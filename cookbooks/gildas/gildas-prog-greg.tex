\chapter{GreG Programming Manual}

\textbf{Partially updated in Oct 2008. Need more details about loading GreG}.

This section describes how \greg\, normally used as an interactive plot
utility, can also be used as a high level plot library.  \greg\ can be used
exactly as a standard graphic library, but because of the possibilities of
the command line monitor, many other possibilities are accessible.

Before presenting in detail the ``Library Version'' of \greg\, we should
distinguish between three different possible applications of the \greg\
Library :
\begin{enumerate}
\item The occasional user who has a single repetitive graphic problem which
  is part of another complex program. Interactive control is not wanted.
  This case can often be solved using command procedures and \sic\ images
  as data format when formatted I/O is definitely too slow.  This is very
  efficient and flexible.  If the user already discarded this possibility,
  he (she) most likely wants the simplest programming ever possible, will
  be satisfied by standard default values, and is not really worried about
  optimum efficiency.
\item A programmer wanting to solve a single repetitive graphic problem for
  use by other people. Interactive action is not wanted.  Simple
  programming is of little importance, but efficiency is a major problem.
\item A programmer wishing to integrate elaborate and flexible graphic
  applications as part of a more complete data analysis system. Interactive
  control by the user and error recovery are necessary. Then all \greg\
  capabilities are wanted, and in addition this programmer may well be
  interested to use the possibilities of the \sic\ monitor to ``supervise''
  the data analysis system.
\end{enumerate}

The library version of \greg\ allows all three cases to be solved
adequately by offering three different ways to call \greg\ services :
\begin{enumerate}
\item Passing a command to \greg\ using the routine {\tt GR\_EXEC}\\
  {\tt GR\_EXEC('Command Argument/Option')} will execute the command line
  exactly as if you had typed it interactively. Instead of {\tt GR\_EXEC},
  it is recommended to use {\tt GR\_EXEC1, GR\_EXEC2} and {\tt GR\_EXECL}
  for commands of languages GREG1, GREG2 and GTVL respectively.

\item Calling an intermediate formatting routine which generates the
  appropriate call to {\tt GR\_EXEC} from its own arguments.  Special
  entries are used to process possible options.
  The advantage of this mode is to provide a more standard program
  interface. Not all commands will be accessible in that way, and it is
  marginally slower than the previous mode.

\item Calling subroutines which do not correspond to \greg\ commands but
  directly generates plot actions. This way is the most optimized access,
  since it bypasses the command line interpreter.  However, only standard
  things can be done like this, and it requires some precautions because of
  the segmented nature of the graphic library.
\end{enumerate}

All three ways can be used within a single program, and the choice between
one or the other is just a matter of convenience and/or efficiency.

In addition, the \greg\ plot library can be used either as a classic
package of subroutines (``Library Only''), or as an interactive facility
allowing user control at run time by means of the \sic\ monitor
possibilities (``Interactive''). The two modes can be mixed in a
subroutine, with the important restriction that the ``Library Only'' mode
is just a subset of the ``Interactive'' mode. It is not possible to change
the mode during program execution. A program using \greg\ plot library in
``Interactive'' mode in fact appears as a superset of the \greg\
interactive utility.

\section{Interaction  with  FORTRAN programs}

For a normal user, it can be said that \greg\ does not interfere at all
with a {\tt FORTRAN} program. All interactions with a program concern the
system or \sic\ facilities as detailed below.

\begin{itemize}
\item {I/O Units :}\\
  All logical units used by the \greg\ system and the associated \sic\
  monitor are FORTRAN units between 50 and 99. Programs using \sic\ should
  avoid opening such units, or should get available units through calls to
  {\tt SIC\_GETLUN}.
\item{Work Space :}\\
  \greg\ uses as much as possible the concept of virtual memory. This means
  that work space, when required, is allocated dynamically at run time.
  Hence, \greg\ does not overload a small program. There is currently one
  exception, the X,Y and Z buffers which have a fixed size allocation of
  10000 long-words each. This may change at some time. On large
  applications, be sure that your virtual memory quota is large enough. Be
  sure also to run \greg\ in a large enough working set to reduce page
  faults.
\item{Special Handler :}\\
  The \sic\ monitor always traps the {\tt \^{}C} action to provide a
  facility to interrupt procedures at any time.  You can bypass this action
  by adequate programming (see {\tt SIC\_CTRLC} routine in the \sic\
  manual).
% The \greg\ system when used in interactive uses a {\tt VMS} Handler to recover
% from errors in Logarithms. By default, this is disabled in the library mode
% but you might be interested in establishing this possibility. Just add the
% following instruction in your main program:
% {\tt CALL LIB\$ESTABLISH (GR\_HANDLER)}
\end{itemize}

Programmers using \sic\ as command monitor together with \greg\, either in
interactive or in library mode, should be aware of the interaction between
\greg\ and \sic\ command parsing facility.  Each call to {\tt GR\_EXEC,
  GR\_EXEC1, GR\_EXEC2, GR\_EXECL or EXEC\_GREG} parses at least one \greg\
command line, and thus modify the pointers accessed by \sic\ argument
retrieving routines.  Accordingly, any subroutine implementing a user
command should retrieve all its arguments {\bf before} calling a \greg\
subroutine.

\section{Basic Routines}

\begin{enumerate}
\item{\tt LOAD\_GREG (Mode)}\\
  \textbf{To be updated since the change of SIC initialization mode}
  This subroutine must be called before any other reference to \greg\
  services. It is used to define the operating mode and to initialize the
  \greg\ vocabulary into the \sic\ monitor.  The argument is of type
  Character and can be {\tt 'LIBRARY'} to initialize \greg\ in the
  ``Library Only'' (no interactive \sic\ monitor) mode, or {\tt
    'INTERACTIVE'} to initialize \greg\ with all \sic\ monitor
  capabilities. Strings like {\tt 'LIBRARY GREG1'} or {\tt 'INTERACTIVE
    GREG2'} can be used to load only one language, although the
  GREG2$\backslash$ language requires GREG1$\backslash$ to function.  This
  routine does not set the \sic\ prompt, neither the Log File which you may
  specify by calling routine {\tt SIC\_OPT}. Please refer to the \sic\
  manual.
\item{\tt GR\_EXEC(LINE)}\\
  This is the basic routine for all plot actions. It is able to activate
  any \greg\ command exactly in the same way as if you had typed it during
  an interactive session. Use {\tt GR\_EXEC} if you do not know to which of
  the \greg\ languages it pertains, {\tt GR\_EXEC1,2 or L} according to the
  language if you know (and you should).  The command line must not include
  the language field.  The command is not written to the \sic\ stack,
  neither to the Log File.
\item{\tt GR\_ERROR()}\\
  This logical function allows error recovery. It returns the internal
  error status of \greg\ program, and clears it. If another \greg\
  subroutine is called while an error status exist, program execution
  aborts.
\item{\tt EXEC\_GREG(LINE)}\\
  \textbf{Obsolescent, use EXEC\_COMMAND or EXEC\_PROGRAM instead}
  execute a command line which can be either a \greg\ command or a \sic\
  command (like {\tt '@ PROC'} for example). Control returns to the calling
  program when the command is completed successfully. If an error occurs,
  the current \sic\ error recovery action is activated before. This is
  usually a {\tt PAUSE} which gives interactive control to the user ;
  control will then return to the calling program only after command {\tt
    EXIT} (or {\tt \^{}Z}) has been typed.  Contrary to {\tt GR\_EXEC}, the
  command is written to the \sic\ stack, and to the Log File.
  ``Interactive'' mode is required.
\end{enumerate}
{\em Only {\tt GR\_EXEC} (or its variant) and {\tt GR\_ERROR} are allowed
  in Library Mode.}

\section{Linking}

\subsection{UNIX systems}

\textbf{To be updated with current information on architecture under Linux}

To access \greg\ from your Fortran Program, you need to link to several
libraries. All \greg\ libraries are located in {\tt \$GAG\_ROOT/lib} where
{\tt \$GAG\_ROOT} is an environment variable defining where the \gildas\
software is located. See your local \gildas\ expert for that.

Then you should use the following link command to link your program
\begin{verbatim}
        f77 -o Program Program.f -L/usr/lib/X11 -L$GAG_ROOT/lib \
        -lgdf -lGREG -lcontour -lgtlang -lchar -lsic -limage -lgag -lrary \
        -lX11 -lm -lc

where
        f77              is the Fortran compiler/linker
        Program.f        is your Fortran program.
        /usr/lib/X11     indicates where the X11 libraries are located
        -lm -lc          are required on some systems
\end{verbatim}
On some systems, the \greg\ libraries are shareable, thus avoiding code to
be included in your own programs.

\section{Running}

If you are using \greg\ in ``Library Only'' mode, you can just run your
program normally.

If you are using the ``Interactive'' mode you need to provide the
assignments of \greg\ help files and working files to your program.  All
compulsory assignments are made system wide, so you can just run the
program. You will obtain help for the \sic\ command monitor and a log
file will be written in \texttt{.gag/logs} .


\section{Example}

\textbf{No simple working example is available as of today.}
See the \mapping\ program for a more exhaustive example. 

\section{Array Transfer}

Subroutines with data arrays transferred to or obtained from \greg\ have a
Real*4 and a Real*8 version.  The simple precision has a name beginning by
{\tt GR4\_}, the double precision a name beginning by {\tt GR8\_}. In the
argument list description, {\tt REAL} will mean Real(KIND=4) for the {\tt GR4\_}
version, Real(KIND=8) for the {\tt GR8\_}. These routine work by copying the
data. For really big arrays, it is more efficient to transfer the
information by reference, though SIC variables (see the SIC manual).

\subsection{GR4\_GIVE - GR8\_GIVE}

{\tt GR4\_GIVE(NAME,NXY,ARRAY)}

This subroutine passes One-Dimensional array {\tt ARRAY} to \greg\ as the
X, Y, or Z array according to the given {\tt NAME}. It is the optimal way
to initialize the One-Dimensional arrays of \greg\ with the data you have
computed within your application program.
\begin{verbatim}
        NAME    is CHARACTER(LEN=1) and may be X, Y or Z
        NXY     is the number of values set in ARRAY
        ARRAY   is a REAL array of size NXY.
\end{verbatim}

\subsection{GR4\_GET - GR8\_GET}

{\tt GR4\_GET(NAME,NXY,ARRAY)}

This subroutine is the reverse of the {\tt GR4(8)\_GIVE} routines.  It
passes One-Dimensional array {\tt ARRAY} from \greg\ to your program as the
X, Y, or Z array according to the given {\tt NAME}. It allows to benefit in
your program of the flexible input formats of \greg\ .
\begin{verbatim}
        NAME    is CHARACTER(LEN=1) and may be X, Y or Z     Input
        NXY     dimension of ARRAY                      Input
                number of values returned in ARRAY      Output
        ARRAY   is a REAL array of size NXY             Output
\end{verbatim}

\subsection{GR4\_RGIVE - GR8\_RGIVE}

{\tt GR4\_RGIVE(NX,NY,CONV,R)}

This subroutine passes a two-Dimensional array to \greg\ as the Regular
Grid array used for mapping.
\begin{verbatim}
        NX,NY   Integers, array dimensions
        CONV    Real(KIND=8) array of dimension 6
                It contains respectively
                CONV(1) Reference pixel in X (First dimension of R)
                CONV(2) X User coordinate value at CONV(1)
                CONV(3) X User coordinate increment per pixel (may be negative)
                CONV(4)-CONV(6) same as above for Y (Second dimension of R)
        R       REAL   array (NX,NY) to be transferred.
\end{verbatim}
If R is Real(KIND=4), the array is not physically copied but its address is
computed by \greg\ for later use. \greg\ will not modify anything in it. If
of type Real(KIND=8), virtual memory is allocated to create a Real(KIND=4) array of
same dimensions.

\subsection{GR4\_LEVELS - GR8\_LEVELS}

{\tt GR4\_LEVELS(NL,LEVELS)}

This subroutine initializes a set of contour levels for mapping by \greg\ .
{\tt LEVELS} is a {\tt REAL} array of dimension {\tt NL}.

\section{Immediate Routines}

The following routines do not use the \sic\ command line interpretor.  They
are essentially internal \greg\ routines made available to the user, and
are thus very efficient to use. As above, they have a KIND=4 and a KIND=8
version.

For optimization purposes, these routines do not include any explicit
segmentation of the plot. In order to be consistent with the philosophy of
the interactions between \greg\ and the {\tt GTVIRT} graphic library, you
must enclose a set of calls to the immediate routines between a call to
{\tt GR\_SEGM} and {\tt GR\_OUT}. Ex:
\begin{verbatim}
     CALL GR_SEGM('Nom',ERROR)  ! Close current graphic segment and open
                                ! a new one
     CALL GR4_CONNECT (...)     ! Fill this segment with plot coordinates
     CALL GR4_MARKER (...)      ! etc..
     CALL GR_OUT                ! Make the segment visible on screen
\end{verbatim}
\subsection{GR\_SEGM}

{\tt GR\_SEGM(NAME,ERROR}

Close the current graphic segment and open a new graphic segment with the
current plotting attributes selected by {\tt PENCIL}. All the commands of
\greg\ open at least one new segment, but the immediate routines do not.
This routine must be called prior to calling a series of immediate routines
to ensure that the plot will appear with the currently selected graphic
attributes, and to allow a selective erasure of the plot. NAME is a
character string indicating the desired name of the segment, and ERROR a
logical error flag. All plot request issued between two successive calls to
{\tt GR\_SEGM} will make a single graphic segment. Note however that every
\greg\ command having an effective plot action creates one or more graphic
segments.

\subsection{GR\_OUT}
{\tt GR\_OUT}

Updates the graphic output. This routine must be called when you wish to
make visible a series of calls to the immediate routines.

\subsection{DRAW - RELOCATE}
{\tt DRAW(XU,YU) RELOCATE(XU,YU)}

Basic pen down or up movement in User coordinates (Real(KIND=8) values).  There
is no Real(KIND=4) version.

\subsection{GDRAW - GRELOCATE}
{\tt GDRAW(X4,Y4) GRELOCATE(X4,Y4)}

Basic pen down or up movement in Physical coordinates (Real(KIND=4) values).
There is no Real(KIND=8) version.

\subsection{GR4\_PHYS\_USER - GR8\_PHYS\_USER}
{\tt GR4\_PHYS\_USER (XP,YP,XU,YU,NXY)}

Convert Physical coordinates ({\tt XP,YP} always Real(KIND=4) values) to User
coordinates ({\tt XU,YU, REAL} values). {\tt NXY} is the number of values.

\subsection{GR4\_USER\_PHYS - GR8\_USER\_PHYS}
{\tt GR4\_USER\_PHYS(XU,YU,XP,YP,NXY)}

Convert User coordinates ({\tt XU,YU, REAL} values) to Physical coordinates
({\tt XP,YP}, always Real(KIND=4) values). {\tt NXY} is the number of values.


\subsection{GR4\_CONNECT - GR8\_CONNECT}

{\tt GR4\_CONNECT(NXY,X,Y,BVAL,EVAL)}

This subroutine connects all data points represented by the X and Y arrays
passed in arguments. {\tt BVAL} and {\tt EVAL} are used for blanked values.
{\tt EVAL} negative means no blanking.
\begin{verbatim}
Arguments :
        NXY     INTEGER         Input
        X       REAL (NXY)      Input
        Y       REAL (NXY)      Input
        BVAL    REAL            Input
        EVAL    REAL            Input
\end{verbatim}

\subsection{GR4\_HISTO - GR8\_HISTO}

{\tt GR4\_HISTO(NXY,X,Y,BVAL,EVAL)}
\begin{verbatim}
Arguments :
        NXY     INTEGER         Input
        X       REAL (NXY)      Input
        Y       REAL (NXY)      Input
        BVAL    REAL            Input
        EVAL    REAL            Input
\end{verbatim}

\subsection{GR4\_MARKER - GR8\_MARKER}
{\tt GR4\_MARKER(NXY,X,Y,BVAL,EVAL)}

Markers of current size are plotted at each data point.
\begin{verbatim}
Arguments :
        NXY     INTEGER         Input
        X       REAL (NXY)      Input
        Y       REAL (NXY)      Input
        BVAL    REAL            Input
        EVAL    REAL            Input
\end{verbatim}

\subsection{GR4\_CURVE - GR8\_CURVE}

{\tt GR4\_CURVE(NXY,X,Y,Z,VAR,PER,BVAL,EVAL,ERROR)}

Plots a smooth curve from the (X,Y) values using the requested interpolant.
Z is either a dummy argument or a parameter for the curve representation
depending on the {\tt VAR} value. {\tt VAR} indicates which is the variable
to use for interpolation. {\tt PER} indicates whether the curve is periodic
or not. {\tt BVAL} and {\tt EVAL} define the blanking value and the
blanking tolerance (set {\tt EVAL} negative to disable blanking checking).
{\tt ERROR} is an error flag set if the curve could not be produced.  The
current accuracy is used for the interpolation.
\begin{verbatim}
Arguments :
        NXY     INTEGER         Input
        X       REAL (NXY)      Input
        Y       REAL (NXY)      Input
        VAR     CHARACTER*(*)   Input
        PER     LOGICAL         Input
        BVAL    REAL            Input
        EVAL    REAL            Input
        ERROR   LOGICAL         Output
\end{verbatim}

\subsection{GR4\_EXTREMA - GR8\_EXTREMA}
{\tt GR4\_EXTREMA(NXY,X,BVAL,EVAL,XMIN,XMAX,NMIN,NMAX)}

Compute the extrema of the input array avoiding blanked values.
\begin{verbatim}
Arguments :
        NXY     INTEGER         Input   Number of points
        X       REAL (NXY)      Input   Array
        BVAL    REAL            Input   Blanking value
        EVAL    REAL            Input   Tolerance on blanking
        XMIN    REAL            Output  Minimum value
        XMAX    REAL            Output  Maximum value
        NMIN    INTEGER         Output  Pixel of the minimum X(NMIN) = XMIN
        NMAX    INTEGER         Output  Pixel of the maximum
\end{verbatim}

\subsection{GR8\_BLANKING}
{\tt GR8\_BLANKING(BVAL,EVAL)}

Define the blanking value to be used later. It is equivalent to call {\tt
  GR\_EXEC('SET BLANKING BVAL EVAL')}, but the later form requires
formatting of values.
\begin{verbatim}
Arguments :
        BVAL    REAL            Input   Blanking value
        EVAL    REAL            Input   Blanking precision
\end{verbatim}

\subsection{GR8\_SYSTEM - GR8\_PROJEC}

{\tt GR8\_SYSTEM (ICODE)}

{\tt GR8\_PROJEC (X,Y,A,ICODE)}

Define respectively the coordinate projection system and the projection
constants. The {\tt SYSTEM} code can be 1 for {\tt UNKNOWN}, 2 for {\tt
  EQUATORIAL (1950.0)}, 3 for {\tt GALACTIC}. The projection code is 0 for
{\tt NONE}, 1 for {\tt GNOMONIC}, 2 for {\tt ORTHOGRAPHIC}, 3 for {\tt
  AZIMUTHAL}, 4 for {\tt STEREOGRAPHIC}, 5 for {\tt LAMBERT} cylindrical, 6
for {\tt AITOFF} equal area and 7 for {\tt RADIO} (also known as global
{SINUSOIDAL}) projection.
\begin{verbatim}
Arguments :
        ICODE   INTEGER         Input
        X       REAL            Input        Projection center
        Y       REAL            Input        Projection center
        A       REAL            Input        Projection Angle
\end{verbatim}

\subsection{GR4\_RVAL}

{\tt GR4\_RVAL(XU,YU,Z4)}

Returns a map value at a given point in user coordinates.
\begin{verbatim}

Arguments :
        XU      REAL*8          Input
        YU      REAL*8          Input
        Z4      REAL*4          Output
\end{verbatim}

\subsection{GR\_WHERE}

{\tt GR\_WHERE(XU,YU,X4,Y4)}

Returns the pen position with the same conventions as {\tt GR\_CURS}
\begin{verbatim}
Arguments :
        XU      R*8     X User coordinates         Output
        YU      R*8     Y User coordinates         Output
        X4      R*4     X Plot coordinates         Output
        Y4      R*4     Y Plot coordinates         Output
\end{verbatim}

\subsection{GR8\_TRI}
{\tt GR8\_TRI(X,INDEX,N,*)}

Sorting program that uses a quicksort algorithm.  Applies for an input
array of Real(KIND=8) values which is reordered.  It also returns an array of
indexes sorted for increasing order of X. You can use {\tt GR8\_SORT} to
reorder other arrays associated with X.
\begin{verbatim}
Arguments :
        X       R*8(*)  Unsorted/Sorted array           Input/Output
        INDEX   I(*)    Integer array of sorted indexes Output
        N       I       Length of arrays                Input
        *       Label   Error return
\end{verbatim}

\subsection{GR8\_SORT}

{\tt GR8\_SORT(X,XSORT,INDEX,N)}

Reorder a real*8 array using the sort indexes computed by {\tt GR8\_TRI}.
Note that {\tt X} and {\tt XSORT} must be different (i.e. sorting cannot
take place within the same array).
\begin{verbatim}
Arguments :
        X       R*8(*)  Unsorted/Sorted array           Input/Output
        XSORT   R*8(*)  Sorted array                    Work space
        INDEX   I(*)    Integer array of sorted indexes Input
                        (obtained by GR8_TRI)
        N       I       Length of arrays                Input
\end{verbatim}

\subsection{GR\_CLIP}

{\tt LOGICAL FUNCTION GR\_CLIP(clip)}

Turn on {\tt (CLIP = .TRUE.)} or off {\tt (CLIP = .FALSE.)} clipping of
lines, and return current status in {\tt GR\_CLIP}. By default, clipping is
on.  Caution : some \greg\ subroutines force clipping off and reset it on
upon exit.

% \subsection{GR\_HANDLER}

% {\tt GR\_HANDLER}

% The \greg\ system when used in interactive uses a Handler to recover
% from errors in Logarithms. By default, this is disabled in the library mode
% but you might be interested in establishing this possibility. Just add the
% following instruction in your main program:

% {\tt CALL LIB\$ESTABLISH (GR\_HANDLER)}


\section{The cursor routine}
{\tt GR\_CURS (XU,YU,X4,Y4,CODE)}

Calls the interactive graphic cursor and returns its position when you hit
any alphanumeric key on the keyboard.
\begin{verbatim}
Arguments :
        XU      REAL*8       X User coordinates    Output
        YU      REAL*8       Y User coordinates    Output
        X4      REAL*4       X Plot coordinates    Output
        Y4      REAL*4       Y Plot coordinates    Output
        CODE    CHARACTER*1  Character struck      Output
\end{verbatim}
The subroutine returns {\tt CODE = 'E'} when an error occurs.

For X-Window terminals, button 1 returns \^\ , 2 returns \& and 3 returns
$*$.

\section{\greg\ High-Level Subroutines}

These routines essentially format the command line to pass it later to the
{\tt GR\_EXEC} subroutine, and are thus the less efficient routines of the
\greg\ library. They are provided essentially for user convenience because
the formatting needed to use {\tt GR\_EXEC} might be tedious. Note all
\greg\ commands have the high level equivalent, but all can be used with
{\tt GR\_EXEC}.

Each subroutine corresponds to a command, and each entry corresponds to an
option of that command. The subroutine and entry names are built from the 4
first characters of the corresponding command and option names. The entries
must be called before the subroutine, since it is this one which
effectively transmit the command to {\tt GR\_EXEC}.

The conventional type of arguments are
\begin{verbatim}
        NAME            Character
        ARG,ARG1,...    Real*8
        IARG,IARG1,...  Integer*4
        NARG            Integer*4
\end{verbatim}

When {\tt NARG} is present, all arguments need not be passed : trailing
arguments may be omitted. {\tt NARG} is used to give the actual number of
arguments passed after {\tt NARG} itself and it must be set precisely. All
arguments before {\tt NARG}, usually the character string {\tt NAME}, and
the argument {\tt NARG} must always be present (even if {\tt NAME='\ '}).
Note that if {\tt ARG1} is missing, {\tt ARG2} cannot be present. Note also
that the double quotes should not be passed for character strings. For
instance, the interactive command {\tt LABEL ``A little toy''} should be
replaced by {\tt CALL GR\_LABE('A little toy')}.

Example : {\tt CALL GR\_DRAW ('MARKER', 2, 0.3D0, 0.8D0)}

Note that the {\tt ARG1} and {\tt ARG2} arguments are here expressed as
constants, and the D exponent is required as they must be Real*8 values.

The high level routines are not yet fully guaranteed as up to date.  In the
following description, each high level subroutine is preceeded by the
assumed syntax for the equivalent command. If this syntax differs from the
current internal help description, using the discrepant parts of the high
level routine will cause a fatal error, but common parts of the syntax can
be used safely.

For the GTVL language, the following routines are available
\begin{verbatim}
CLEAR [Argument]
        SUBROUTINE GR_CLEA(NAME)

DEVICE [Type [Descriptor]] [/OUTPUT Logical_Name]
        SUBROUTINE GR_DEVI(NAME)
        All arguments of command DEVICE being character strings, it is
        easy to concatenate them into NAME.
\end{verbatim}

For the GREG1 language, the routines are
\begin{verbatim}

AXIS Name [A1 A2] [/LOCATION X Y] [/TICK Orientation [BIG SMALL]]
        [/LABEL Position] [/[NO]LOG] [/ABSOLUTE]
        SUBROUTINE GR_AXIS(NAME,NARG,ARG1,ARG2)         ! 28-Sep-1986
        ENTRY GR_AXIS_LOCA(NARG,ARG1,ARG2)
        ENTRY GR_AXIS_TICK(NAME,NARG,ARG1,ARG2)
        ENTRY GR_AXIS_LABE(NAME)
        ENTRY GR_AXIS_LOG
        ENTRY GR_AXIS_NOLO
        ENTRY GR_AXIS_ABSO

BOX [Arguments]
        SUBROUTINE GR_BOX(NAME)                         ! 28-Sep-1986
        ENTRY GR_BOX_ABSO

COLUMN [X Nx] [Y Ny] [E Ne] [/FILE File] [/LINES Lmin [Lmax]]
        [/TABLE TableName]
        SUBROUTINE GR_COLU(NAME)                        ! 28-Sep-1986
        As this routine is normally of no use in the library version,
        it requires you to code explicitly the remaining part of the
        command line. Hence, it should be equivalent to use
        either GR_EXEC('GREG1\COLUMN '//NAME)
        or GR_EXEC1('COLUMN '//NAME)

CONNECT [/BLANKING Bval Eval]                           ! 28-Sep-1986
        SUBROUTINE GR_CONN
        ENTRY GR_CONN_BLAN(NARG,ARG1,ARG2)

DRAW [Action [X Y]] [/USER] [/BOX N] [/CHARACTER N] [/CLIP]
        SUBROUTINE GR_DRAW(NAME,NARG,ARG1,ARG2)
        ENTRY GR_DRAW_CHAR(NARG,IARG)
        ENTRY GR_DRAW_USER
        ENTRY GR_DRAW_BOX(NARG,IARG)
        ENTRY GR_DRAW_CLIP

        Note that no support is given for DRAW TEXT and DRAW FILL_AREA,
        unless you specify in NAME all the arguments and set NARG to 0.
        DRAW TEXT can easily be replaced by GR_DRAW('RELOCATE'...)
        followed by a call to GR_LABE.

ERRORBAR NAME
        SUBROUTINE GR_ERRO(NAME)                        ! 28-Sep-1986

HISTOGRAM [/BASE [Ybase]] [/BLANKING Bval Eval]
        SUBROUTINE GR_HIST                              ! 28-Sep-1986
        ENTRY GR_HIST_BASE(ARG)
        ENTRY GR_HIST_BLAN(NARG,ARG1,ARG2)

LABEL "String" [/X] [/Y] [CENTERING N] [/APPEND]
        SUBROUTINE GR_LABE(NAME)                        ! 28-Sep-1986
        ENTRY GR_LABE_X
        ENTRY GR_LABE_Y
        ENTRY GR_LABE_CENT(IARG)
        ENTRY GR_LABE_APPE

LIMITS [Xmin Xmax Ymin Ymax] [/XLOG] [/YLOG] [/RGDATA] [/REVERSE [X] [Y]]
        [/BLANKING Bval Eval]
        SUBROUTINE GR_LIMI(NARG,ARG1,ARG2,ARG3,ARG4)    ! 28-Sep-1986
        ENTRY GR_LIMI_XLOG
        ENTRY GR_LIMI_YLOG
        ENTRY GR_LIMI_RGDA
        ENTRY GR_LIMI_REVE(NAME)
        ENTRY GR_LIMI_BLAN(NARG,ARG1,ARG2)
        Arguments of GR_LIMI can be omitted with the standard VAX
        convention to indicate that automatic limit must be computed
        for this one. For example
                CALL GR_LIMI_XLOG
                CALL GR_LIMI(4,0.d0, ,-10.d0,10.d0)
        is equivalent to
                CALL GR_EXEC1('LIMITS 0 * -10 10/XLOG')
        Presently, there is no support for the fifth argument (Angular
        unit or Absolute coordinates) and for the =, < and > possibilities
        of command LIMITS in this High Level routine. To use these
        possibilities, you should use directly GR_EXEC1('LIMITS ...').

PENCIL [N] [/COLOUR C] [/DASHED D] [/WEIGHT W]
        SUBROUTINE GR_PEN(IPEN,ICOLOUR,COLOUR,IDASH,IWEIGHT,ERROR)

POINTS [Size] [/BLANKING Bval Eval]
        SUBROUTINE GR_POIN(NARG,ARG1)
        SUBROUTINE GR_POIN_BLAN(NARG,ARG1,ARG2)

RULE [X] [Y] [/MAJOR [/MINOR]
        SUBROUTINE GR_RULE(NAME)                        ! 28-Sep-1986
        ENTRY GR_RULE_MAJO
        ENTRY GR_RULE_MINO

SET Something [Value1 [Value2]...]]]
        SUBROUTINE GR_SET(NAME,NARG,ARG1,ARG2,ARG3,ARG4) ! 28-Sep-1986

SHOW
        SUBROUTINE GR_SHOW(NAME)                        ! 28-Sep-1986

TICKSPACE SmallX BigX SmallY BigY                       ! 28-Sep-1986
        SUBROUTINE GR_TICK(NARG,ARG1,ARG2,ARG3,ARG4)

\end{verbatim}

The following commands correspond to the GREG2$\backslash$ Language.
\begin{verbatim}

EXTREMA [/BLANKING Bval Eval] [/PLOT]
        SUBROUTINE GR_EXTR                              ! 28-Sep-1986
        ENTRY GR_EXTR_BLAN(NARG,ARG1,ARG2)
        ENTRY GR_EXTR_PLOT

LEVELS List
        SUBROUTINE GR_LEVE(NAME)
        Coding the list in the required format may be funny.
        Use GR4_LEVELS or GR8_LEVELS instead, they are more convenient.

RGDATA File_Name [/SUBSET IX1 IY1 IX2 IY2]
        SUBROUTINE GR_RGDA(NAME)                        ! 28-Sep-1986
        ENTRY GR_RGDA_SUBS(NARG,IARG1,IARG2,IARG3,IARG4)

RGMAP [/ABSOLUTE Value] [/PERCENT Value] [/KEEP] [/BLANKING Bval Eval]
        [/GREY Colour Ntry] [/PENS Pos Neg]
        SUBROUTINE GR_RGMA                              ! 28-Sep-1986
        ENTRY GR_RGMA_ABSO(ARG)
        ENTRY GR_RGMA_PERC(ARG)
        ENTRY GR_RGMA_BLAN(NARG,ARG1,ARG2)
        ENTRY GR_RGMA_KEEP
        ENTRY GR_RGMA_PENS(NARG,IARG1,IARG2)
        No support is yet given for option /GREY because it is still
        rather experimental.

\end{verbatim}

Remember that if anything available in interactive seems to be missing in
the previous list, you can always use {\tt GR\_EXEC} to access it.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "gildas-prog"
%%% End:
