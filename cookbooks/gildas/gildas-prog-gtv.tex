\chapter{GTV Programming Manual}

\section{Concept}

The GTVIRT is a fast low-level Graphic library, allowing to develop Graphic
programs in a completely device independent way.  The GTVIRT library
involves the following concepts:
\begin{itemize}
\item{\em the Plot Page} \\
  The Plot Page is a virtual workspace on which all graphic items will be
  (virtually) drawn.  The Plot Page units are ``virtual centimeters''.
  Actual drawing on a graphic device is usually done with an automatic
  scaling factor to match the Plot Page to the device.  For hardcopy
  outputs, an exact matching between the Plot Page unit and centimeters is
  possible.
\item{\em Segments} \\
  Graphic segments are the smallest separable entities in a drawing.
  Segments are named, and can be edited to change their aspects.  Although
  segments are usually associated with a single, easily identified, part of
  the drawing (e.g. a curve, or a label, or a bitmap image), they can
  contain anything.  The segmentation is not defined by the GTVIRT, but
  left to the calling program.  Segments are named uniquely to help
  identifying them.
\item{\em Coordinate Systems} Two coordinate systems are available within
  the GTVIRT: a Page coordinate system (whose units are centimeters), and a
  User coordinate system. Drawing can be done in any of these systems.
\item{\em Directories} \\
  Directories are special graphic segments used to group in a logical way
  ensembles of segments.  A complete directory tree can be specified in a
  drawing.  As for the segments, the structure of the drawing is defined by
  the calling program, not by the GTVIRT library.

  Each directory has its own User coordinate system. This feature can be
  used to map several coordinate systems to different or similar regions of
  the Plot Page.
\item{\em Devices} \\
  The GTVIRT is (completely ?) device independent. A drawing can be
  prepared and visualized separately. The GTVIRT allows display on a large
  variety of graphic devices.
\item{\em Metafiles} \\
  Directories and directory trees of a Plot can be saved on a (binary)
  metafile, complete with all graphic segments and user coordinate systems.
  Metafiles can be imported as Sub-directories in any Plot.
\item{\em Hardcopy} \\
  ``Hard''copy on paper-like Graphic devices such as pen plotters, laser
  printers, etc..., are completely transparent in the GTVIRT. ``Soft''
  hardcopy on files in industry standard graphic languages such as
  PostScript or HPGL is possible too.
\item{\em Windows} \\
  {\em Windows} are available on some devices. This includes X-Window
  terminals, and MS-Windows screen. {\em Windows} can be attached to any
  directory. Several windows can be attached to the same directory. A {\em
    Window} displays all the sub-directories depending on the directory to
  which it is attached.
\item{\em Plotting Depth} \\
  Each segment can be understood as an opaque or transparent plot. The
  order in which opaque segments are plotted can be controlled by their
  plotting depth.  This feature is important when plotting bitmaps or
  drawing colour filled curves.
\item{\em 8-bit images} \\
  are supported on Windows graphic displays (X-Window or MS-Window or MAC).
\end{itemize}

The \gtvirt\ normally operates in a buffered mode. All drawings command a
written into an internal metacode, and only transmitted to the plotting
device when required by the calling program. In addition, for applications
which require an unbuffered plot, an {\em immediate} mode is available.

\section{Programming}

\begin{itemize}
\item{Initialisation}\\
  The GTVIRT library is initialized by a a call to\\
  {\tt SUBROUTINE INIT\_GTVIRT}\\
  followed by a call to
  {\tt SUBROUTINE GTINIT(LX,LY,LUNG,LUNH,NAME,USER\_ROUTINE}\\
  where
\begin{itemize}\itemsep 0pt
\item {\tt LX,LY} are the page dimensions
\item {\tt LUNG,LUNH} are logical unit numbers for graphic output and
  hardcopies, to be supplied by the caller
\item {\tt NAME} is a character string giving the name of the top directory
  of the drawings. {\tt NAME} should start with a $<$ sign.
\item {\tt USER\_ROUTINE} is a user-supplied subroutine called when moving
  from a sub-directory to the other. It must provide to the GTVIRT
  information about coordinate systems. The only suitable routine is
  available in the \greg\ program, and is named {\tt GREG\_USER}
\end{itemize}

\item{The {\tt GTVIEW} subroutine}\\
  
  The {\tt GTVIEW} subroutine is one of the main control routine in the
  library.  It is used to send drawing commands from the internal metacode
  to the graphic device, but also to performs other actions on the
  metacode.
  
  {\tt SUBROUTINE GTVIRT(Mode)}
  
  where {\tt Action} is a character string which can be
\begin{itemize}
\item       'Append'        \\
  Plot all metacode starting from last drawn vector
\item       'Rewind'        \\
  Clear the screen, and plot the whole metacode from first vector
\item       'Update'        \\
  Update the screen (all windows) if needed
\item       'Limits'        \\
  Recompute the plot limits (Bounding Box)
\item       'Purge'         \\
  Delete images associated to all windows
\item       'Delete'        \\
  Delete images associated to the current window
\item       'Zap'           \\
  Delete cache-bitmap associated to images
\item       'Sleep'         \\
  Set screen update off
\item       'Wake'          \\
  Set screen update on
\end{itemize}
\end{itemize}

\section{Basic Sequence}

\greg\ and the \gtvirt\ are intimately related, and it is not recommended
to use the \gtvirt\ without \greg\. Accordingly, the programming example
given below also uses some (primitive) \greg\ subroutines. The basic
drawing sequence is

\begin{verbatim}
        CALL GR_SEGM (Segment_Name,Error)   ! 1
        CALL GTPOLYL (N,Xarray,Yarray)      ! 2
        CALL ...                            ! 3
        CALL GTVIEW('Append')               ! 4
\end{verbatim}
\begin{enumerate}
\item Open a new graphic segment. This routines is an ``intelligent''
  routine checking wether pen attributes have been changed, and calling
  both {\tt GTSEGM} (the basic segment creation routine) and {\tt GTEDIT}
  (which defines the segment attributes like pen colour, dashed pattern,
  thickness).
\item Use any drawing routine you wish, e.g. an polyline. The drawing
  commands go to the internal metacode only at that time.
\item ...
\item Update the graphic screen with all the new drawing commands which
  have been put in the metacode since last call to {\tt GTVIEW('Append')}
\end{enumerate}

Further buffering between graphic segments can be obtained by enclosing a
set of complete sequences like the above one between a call to {\tt
  GTVIEW('Sleep')} and {\tt GTVIEW('Wake\_up')}. Drawing to the screen will
then only happen when the call to {\tt GTVIEW('Wake\_up')} is made. For
compatibility with other subroutines which may also perform their own {\tt
  Sleep / Wake\_up} control, it is recommended to use the logical function
{\tt GTSTAT} instead, e.g.
\begin{verbatim}
      SLEEP = GTSTAT ('Sleep')

        ... calls to GTVIRT ...

      IF (.NOT.SLEEP) CALL GTVIEW('Wake_up')
\end{verbatim}

\section{Plot Structuration and multi-window applications}
For some complex applications, it is useful to be able to create several
graphic windows and display different plots in each of them. Such a control
over the plot structure is preferably done using the {\tt GTVL} command
language, by appropriate calls to subroutine {\tt GR\_EXECL}.

{\tt    SUBROUTINE GR\_EXECL(Command)}\\
execute a command from the {\tt GTVL} command language.

The most relevant commands to structure a plot are
\begin{itemize}\itemsep 0pt
\item{\tt CREATE DIRECTORY $<$NAME} to create new top directory called {\tt
    $<$NAME}
\item{\tt CHANGE DIRECTORY $<$NAME} to move to this (top) directory
\item{\tt CREATE WINDOW} to create a new window associated to the current
  directory
\item{\tt CHANGE POSITION Code} to move the current window a given position
  in the screen.
\item{\tt CLEAR WINDOW} to clear a window
\item{\tt CLEAR TREE} to erase the tree linked to the current directory
  (from the current top directory).
\item{\tt CREATE DIRECTORY NAME} to create new sub directory
\item{\tt CHANGE DIRECTORY NAME} to move to this sub directory
\end{itemize}
Refer to the \greg\ manuals for detailed help on these commands.

In addition, subroutine {\tt GTEXIST} is useful to check whether a given
directory already exists.

\section{Subroutines}
\begin{itemize}
\item gtchar\\
  Draws a character string.
\item gtclal\\
  Clear alphanumeric screen. On Windows applications, transfer focus to the
  (current) graphic window, and raise in on top of others to display it.
\item gtclear\\
  Erase the whole plot, all directory structures, destroy all associated
  windows, etc... An empty top directory is then re-created
\item gtclos\\
  Close the current graphic device
\item gtclpl\\
  Raise alphanumeric window, and returns focus to it.
\item gtcurs\\
  Call the cursor
\item gtdls\\
  Delete the last graphic segment
\item gtdraw\\
  Draw a vector from current pen position to the specified point.
\item gtedit\\
  Edit the current segment properties.
\item gterflag, gterrgto, gterrtst\\
  Control error status of the library
\item gtexist\\
  Controls the existence of a named sub-directory.
\item gthard\\
  Create a hardcopy
\item gtinit\\
  Initialize the GTVIRT drawing space and the GTVL language
\item gtopen\\
  Open a graphic device
%\item gtpaus (internal) Wait for some time...
\item gtpolyl\\
  Draw a set of lines.
\item gtreloc\\
  Move current pen position to specified coordinates.
\item gtsegm\\
  Open a new segment
\item gtview\\
  Activate the drawing
\item gtwhere\\
  Returns current pen position
% \item gtwindow (internal) Define clipping parameters.
\item gtg\_charlen\\
  Computes character string length
\item gtg\_charsiz\\
  Returns character size
\item gtg\_curs\\
  Returns cursor existence
\item gtg\_screen\\
  Returns clipping parameters
\item gtg\_open\\
  Returns device parameters
\item gtstat\\
  Change GTVIRT mode (Sleep or Wake\_up), and returns previous mode.
% \item gtvirt_data (internal) Initialisation data
\item gtv\_newimage\\
  Create a new image slot
\item gtv\_image\\
  Draw a bitmap to an image slot
\item gtv\_numimage\\
  Get a free image slot
\item gtv\_majimage\\
  Define parameters of an image slot
\item gtv\_fillpoly\\
  Fill a closed polygon.
\item init\_gtvirt\\
  Initialize the GTVIRT library
\item exit\_clear\\
  Quick exit, to be used at program completion.
\item run\_gtvl\\
  Dispatch the GTVL commands to appropriate subroutines
\item exec\_gtvl\\
  Execute a GTVL language command or command procedure
%\item gtvoctdf
%\item dicho
\end{itemize}

{\em Immediate} routines are used to produce immediate actions. They use
the immediate pen. These subroutines are
\begin{itemize}
\item gti\_beep: beep
\item gti\_clear: clear the current window
\item gti\_draw: draw line to current
%\item gti\_filz
\item gti\_out: flush the normal drawing buffer.
\item gti\_pen: select the immediate pen
\item gti\_polyl: draw a polyline
\item gti\_reloc: relocate the immediate pen
%\item gti\_ivalp
\item gti\_where: returns immediate pen position
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "gildas-prog"
%%% End: 
