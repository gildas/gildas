\documentclass[11pt]{article}
\include{gildas-def} % GILDAS specific definitions

\makeindex{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\begin{center}
  \huge{}
  \textbf{A quick introduction to GILDAS} \\[2\bigskipamount]
  \Large{}
  Last revised in October 2008\\[\bigskipamount]
  \large{}
  Questions? Comments? Bug reports? Mail to: \texttt{gildas@iram.fr}\\[2\bigskipamount]
  The \gildas{} team welcomes an acknowledgment in publications\\
  using \gildas{} software to reduce and/or analyze data.\\
  Please use the following reference in your publications:\\
  \texttt{http://www.iram.fr/IRAMFR/GILDAS}\\[2\bigskipamount]
  Up-to-date information on credits and responsabilities \\
  may be found on the GILDAS web page
\end{center}

\begin{rawhtml}
  Note: This is the on-line version of the Introduction to GILDAS.
  A <A HREF="../../pdf/gildas-intro.pdf"> PDF version</A>
  is also available.
\end{rawhtml}

\bigskip{} %

Related information are available in
\begin{latexonly}
  \begin{itemize}
  \item SIC: Command Line Interpretor
  \item GREG: Graphical Possibilities
  \item ASTRO: Ephemeris and observation preparation 
  \item MIRA: Calibration of the raw format (IMBFITS) and
    the writing of the CLASS data format of IRAM-30m spectra obtained in
    2006 and after
  \item CLASS: Processing of single-dish spectra
  \item CLIC: Continuum and Line Interferometric Calibration
  \item MAPPING: Imaging and deconvolution of aperture
    synthesis data
  \item MIS: Millimeter Interferometry Simulation Tools
  \item Programming in GILDAS
  \end{itemize}
\end{latexonly}
%
\begin{rawhtml}
  <UL>
  <LI> <A HREF="../sic-html/sic.html">   SIC:  Command Line Interpretor</A>
  <LI> <A HREF="../greg-html/greg.html"> GREG: Graphical Possibilities</A>
  <LI> <A HREF="../astro-html/astro.html"> ASTRO: Ephemeris and observation preparation </A>
  <LI> <A HREF="../mira-html/mira.html"> Calibration of the raw format (IMBFITS) and
    the writing of the CLASS data format of IRAM-30m spectra obtained in
    2006 and after </A>
  <LI> <A HREF="../class-html/class.html"> Processing of single-dish spectra </A>
  <LI> <A HREF="../clic-html/clic.html"> Continuum and Line Interferometric Calibration</A>
  <LI> <A HREF="../mapping-html/mapping.html"> Imaging and deconvolution of aperture
    synthesis data</A>
  <LI> <A HREF="../mis-html/mis.html"> MIS: Millimeter Interferometry Simulation Tools</A>
  <LI> <A HREF="../gildas-prog-html/gildas-prog.html"> Programming in GILDAS</A>
  </UL>
\end{rawhtml}

\newpage
\tableofcontents{}

\newpage
\section{Introduction}

\gildas\ is one of the numerous image processing systems used in Astronomy.
\gildas\ was born sometime ago in the Groupe d'Astrophysique de Grenoble
(now LAOG, Laboratoire d'Astronomie de l'Observatoire de Grenoble), and has
been adopted as the IRAM standard data reduction package. \gildas\ is 
jointly maintained by IRAM \& LAB (Laboratoire d'Astrophysique de Bordeaux).  
\gildas\ contains many facilities,
most of which are oriented towards spectral line mapping, or in fact all
kind of 3-dimensional data.

\gildas\ has grown on top of two pre-existing programs, \class\ and \greg ,
which seemed to please their users.  As many other packages, \gildas\ had
to move out of its original VAX-VMS environment towards the growing Unix
area. The original software was modified in order to run on VMS and Unix
operating systems, with (near) transparent file sharing even between
heterogeneous computers.  The current implementation even allows remote
task submission on a compute server from a workstation.  VMS is no longer supported, but
a Windows version is available and supported.

This guide is a general introduction to the \gildas\ package. It does not
describe any utility, but gives cross-reference to other documentations and
many usefull hints on how to customize your \gildas\ environment.

\section{Organisation}

\gildas\ consists of five major parts :
\begin{itemize}
\item The documentations, a set of PostScript files. An HTML documentation
  is also available.
\item Dedicated utilities, which are programs using the \sic\ (Sympathetic
  Interpretor of Commands) user-friendly interface. Each utility normally
  has its own manual, accessible through the general documentation system.
  \begin{itemize}
  \item \greg\ an all purpose 1-D and 2-D graphic program
  \item \class\ (Continuum and Line Analysis Single-dish Software), for
    single-dish data processing.
  \item {\bf ASTRO}, an astronomical tool, very useful to prepare an
    observing session, or for amateur astronomy...
  \item {\bf CLIC} (Continuum and Line Interferometric Calibration), to
    calibrate interferometer data from Plateau de Bure.
  \item {\bf Mapping}, an interactive imaging and deconvolution package.
  \end{itemize}
  \index{The Tasks}
\item Dedicated small programs to perform non interactive time consuming
  processing : smoothing, transpositions, fitting, etc...  These programs
  will be called ``Tasks''. They are not intended for interactive use
  (although you may do it if you are an expert), but require instead a
  monitor program to activate them.
\item Additional applications programs, such as FLUX or POINT to handle
  pointing and monitoring data for the IRAM PdB array.
\end{itemize}

\section{Concepts and Useful Hints}

\subsection{Images}

{\em Images} and {\em Tables} are the two most useful concepts in \gildas .
Practically all data used by \gildas\ are stored as {\em Images} (or {\em
  Tables}). An {\em Image} is a data file containing an array of up to 4
dimensions, and a small but comprehensive header to store the array
dimensions, associated coordinates, etc\ldots {\em Tables} are just 2-d
{\em Images} with only the dimensions indicated in the header.

{\em Images} are used everywhere in \gildas .  The \sic\ command monitor
directly manipulates {\em Images} through the {\tt DEFINE IMAGE} command.
The \greg\ program is able to display {\em Images} as contour plots with
overlaid bitmaps. \class\ and {\bf CLIC}, after working with their own data
files, produce {\em Images} for further processing and display. And
finally, all {\em Tasks} use {\em Images} for input and output.

Because of this importance of {\em Images}, we recommend the \gildas\ users
to read carefully the corresponding section in the \sic\ manual. Very
efficient use of {\em Images} is possible within \sic , but it is also
possible to do things a 1000 times more slowly. The ability of \sic\ to
perform mathematics on {\em Images} can solve many problems, and avoid many
``on-purpose'' programs.

{\em Images} can even be initialized easily from external files of many
different formats using the {\tt ACCEPT} command of \sic , which allows the
user to read in a totally flexible way data files to set the content of
\sic\ variables.

Conversion from FITS format to {\em Images} is possible using several tools (because
of the inherent complexities of the FITS format). Simple FITS files can even
be automatically translated into {\em Images}.

\subsection{Customizing}

Although your system manager will provide reasonable defaults, you may wish
to customize a few things to your own taste. Customizing the \gildas\
environment can be done at two levels: Logical Names, and Initialization
files for the \sic\ based utilities.

\subsubsection{Logical Names}

All logical names are placed in a file named {\tt .gag.dico} in your home
directory {\tt (\$HOME)}. The format of the {\tt .gag.dico} file is
\begin{verbatim}
LOGICAL_NAME1    equivalent_name1
LOGICAL_NAME2:   Equivalent_Name2

\end{verbatim}
where the logical names should always be in capital letters, while case
matter for equivalent names. The {\tt ``:''} indicates that the logical
name is a directory (pathname). Besides logical names which you can use to
define special files, or directories, a few peculiar names are used to
customize your \gildas\ environment:
\begin{itemize}
\item{\tt GAG\_EDIT}\\
  the name of your preferred text editor (e.g. vi, emacs, vuepad, ved,
  \ldots). With an \& at the end, it will be launched in background.
\item{\tt GAG\_HARDCOPY}\\
  the default type you wish for graphic hardcopies in \greg .  Major
  possibilities are
\begin{verbatim}
EPS FAST      "Fast" grey-scale postscript
EPS GREY      "Nice" grey-scale postscript (clipping more accurate)
EPS COLOR      Color postscript
HPGL           HP-GL language
\end{verbatim}
  See \greg\ command {\tt DEVICE} for details.
\item{\tt GAG\_PRINTER}\\
  The (queue) name of the printer you prefer to use by default. Beware that
  it should be compatible with the description given in {\tt
    GAG\_HARDCOPY}.  Funny results can be obtained sending HPGL commands to
  a PostScript printer.
\item{\tt GILDAS\_LOCAL:}\\
  The path for your own \gildas\ tasks. For advanced users only.
\item{\tt GAG\_TMP:}\\
  A path where you whish to store temporary files which may be created by
  some applications. For optimum performance, use a local disk of the
  computer.
\end{itemize}

\subsubsection{File Names}

Two problems may arise when running \gildas\ under Unix. The first one is
due to the use of the \/ as an option separator in \sic , and as a
tree/subtree separator in pathnames under Unix. The second is that \sic\ is
normally case unsensitive, while Unix is case sensitive. Placing the file
name within double-quote will prevent making case conversion.
\begin{itemize}
\item SIC "Logical names" such as gag\_log: are expanded in filenames.
\begin{verbatim}
    Typed named                    Expanded Filename
  gag_log:toto.log             /users/me/.gag/logs/toto.log
\end{verbatim}
\item DOS-like names can also be typed and will be translated to Unix-like
  names.
\begin{verbatim}
    Typed named                    Expanded Filename
  \users\me\toto.log             /users/me/toto.log
  "sub\DIRE\Toto.Dat"           sub/DIRE/ToTo.dat
\end{verbatim}
\end{itemize}
Filename handling becomes very simple when filenames are kept simple and lower case, as often happens.

\subsubsection{Initialisation Files}

All \sic\ based interactive programs read a initialisation file before
starting. This file is a standard command procedure for the corresponding
program. For example, for \greg\ it could contain:
\begin{verbatim}
SIC\DEFINE DOUBLE SEC /GLOBAL    !
SIC\LET SEC PI/180/3600         ! Second in Radian
SIC\SIC HELP CONTENT            ! Use PostScript file for HELP
GTVL\DEVICE                     ! Prompt for a graphics device
\end{verbatim}
The initialisation file is located in your home directory, and its name is
{\tt ``init.program''} where {\tt ``program''} is the default file
extension for the application; this is usually just the application name.

\section{Getting Started}

The normal starting point is then to use the \greg\ graphic program, which
will give you a first experience with the \sic\ command monitor. It is
recommended to read first the \sic\ and \greg\ {\em Cookbooks} (15 and 5
pages respectively).

To get really started with image processing, read the \gildas\ chapters 2
and 3 ({\em Running Tasks}, and {\em Displaying Images}) trying to run some
of the image processing tasks (8 pages).  However, since \gildas\ is an
image processing system, some practice with the {\tt GREG2$\backslash$}
\greg\ language will be soon necessary. This practice can be done using the
GRAPHIC program, but reading the \greg\ {\em Manual} will help (12 more
pages).

Reference to more specific sections of the \sic\ and \greg\ documentations
become only necessary for advanced users, and obviously for programmers.
While the user become more and more familiar with \gildas\, he (or she)
will find that constant reference to the documentations is hardly ever
necessary. The internal HELP is usually sufficient.

\include{gildas-intro-format}
\include{gildas-intro-comm}

\printindex{}

\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
