\documentclass[11pt]{article}

% GILDAS specific definitions
\include{gildas-def}

\title{Amplitude Calibration \\
  IRAM Plateau de Bure Interferometer}
\author{S. Guilloteau$^{1}$}
\date{Document probably older than you think}
\makeindex{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\maketitle{}

\begin{center}
Version 1.0
\end{center}

\begin{center}
  (1) Institut de Radio Astronomie Millim\'etrique\\
  300 Rue de la Piscine\\
  F-38406 Saint Martin d'H\`eres\\
\end{center}

Calibration compensates for imperfections and unknowns in the instrument
use, including antenna defects (surface quality, focus), pointing errors,
atmospheric transmission and fluctuations, receiver and backend gain and
instabilities, etc...

This paper describes the way \obs\ (30-m or 15-m version) calibrates
spectra, tries to assess the accuracy of the calibration factor, $Tcal$,
under practical observing conditions, and then list precautions to take
when observing.

\begin{rawhtml}
  Note: this is the on-line version of the "Amplitude Calibration".
  A <A HREF="../../pdf/pdbi-amp-cal.pdf"> PDF version</A> is also available.
\end{rawhtml}

Related information is available in
\begin{latexonly}
  \begin{itemize}
  \item{IRAM Plateau de Bure Interferometer: OBS Users Guide}
  \item{CLIC: Continuum and Line Interferometer Calibration}
  \end{itemize}
\end{latexonly}

\begin{rawhtml}
  <UL>
  <LI> IRAM Plateau de Bure Interferometer:
  <A HREF="../obs-html/obs.html"> OBS Users Guide</A>
  <LI> <A HREF="../clic-html/clic.html"> CLIC:</A>  Continuum and Line Interferometer
  Calibration
\end{rawhtml}

\newpage
\tableofcontents{}

\newpage
\section{Goal and Principle}

\subsection{Basic Equations}

The {\tt CALIBRATION} procedure goal is to convert backend counts into
intrinsic surface brightness or flux density of the source. This conversion
can be divided formally in two steps: correction for individual backend
channel gains and receiver bandpass, and correction for atmospheric
absorption and antenna gain.

Apart from putting the telescope into orbit, there is hardly any hardware
fix which may help overcome the atmospheric absorption perfectly. Hence,
the correction of atmospheric effects is difficult. The goal is to estimate
properly the noise contribution from the atmosphere and the atmospheric
transmission. \obs\ uses the standard chopper wheel technique to calibrate
the backends and receiver gains.

This technique assumes the detector (receiver + IF cables + backend) is
linear.  Thus the detector counts, $Mean\_*$, are related to ``effective
temperatures'' by
\begin{equation}
Mean\_load / K = Trec + T_{load}
\end{equation}
when looking at any given load at a known (effective) temperature
$T_{load}$. $Trec$ is the receiver noise temperature. On the sky,
\begin{equation}
Mean\_atm / K = Trec + T_{emi}
\end{equation}

When looking at source of brightness $T_B$, one obtains
\begin{equation}
Mean\_source / K = Trec + T_{emi} + B_s * e^{-\tau} * T_B
\end{equation}
where $B_S$ is the appropriate antenna to source coupling factor, $\tau$
the atmospheric optical depth.  Thus $T_B$ can be derived from the counts
introducing the ``calibration temperature'' $Tcal$ with
\begin{equation}
T_B = Tcal * \frac {(Mean\_source-Mean\_atm)}{(Mean\_load-Mean\_atm)}
\end{equation}
and, in this simple case, Tcal would be given by

\begin{equation}
Tcal =  (T_{load} - T_{emi}) \frac {e^{\tau}}{B_s}
\end{equation}
The basic equations presented above assume a pure single-sideband reception
(or pure double-sideband on a continuum source).  The system noise
temperature is given by
\begin{equation}
Tsys = \frac{Tcal * Mean\_atm}{Mean\_load - Mean\_atm}
\end{equation}

\subsection{Atmospheric Model}

Evaluating $Tcal$ thus requires to determine both $T_{emi}$ and $\tau$.
Assuming the receiver temperature is known, the noise power received on the
sky $T_{emi}$ is given by
\begin{equation}
T_{emi} = \frac{(T_{load} + Trec) * Mean\_atm}{Mean\_load} - Trec
\end{equation}
and is related to the sky emission temperature by
\begin{equation}
T_{sky} = \frac{T_{emi} - (1.-F_{eff}) * T_{cab}}{F_{eff}}
\end{equation}
where the second formula corrects for the antenna coupling to the sky
($F_{eff}$ is the forward efficiency). Strictly speaking, eq. (8) assumes
that the rear sidelobes of the antenna look into the receiver cabin kept at
a constant temperature $T_{cab}$, with low ohmic losses. $T_{cab}$ can
however be chosen as a weighted average of the focal cabin temperature and
the outside temperature for all rearward losses.

In case of a double side-band receiver and single side-band signal,
$T_{sky}$ is a sum of the contribution of the two receiver bands weighted
by the sideband gain ratio (gain in the image band divided by gain in the
signal band).
\begin{equation}
T_{sky} = \frac{T_{sky\_s} + T_{sky\_i} * Gain\_i}
{(1. + Gain\_i)}
\end{equation}
and a similar equation for $T_{emi}$. Note that this assumes $F_{eff}$ is
identical in the two side bands.

To determine the opacity $\tau$, the trick is to {\bf model} the
atmospheric emission to derive the transmission. There is some hope that it
can work (at least in reasonably good conditions) because the transmission
is dominated by a few constituants, among which only the water vapor varies
significantly with time. Hence, if the atmosphere can be modelled by a
small number of layers, it is possible to derive the transmission from the
emission. The atmospheric model used (ATM, J. Cernicharo) is derived from a
``Standard atmosphere'' and the knowledge of the Atmospheric pressure
$P_{amb}$ and outside Temperature $T_{amb}$ (and the altitude of the site).
Together with the season in the year, these parameters give a good
approximation of the physical temperature of the absorbing layers.  By a
minimization routine, with the amount of precipitable water vapor in
millimeters as variable, the best model fitting the measured $T_{sky}$ is
found. Then, for each band (signal and image), the total zenith opacities
are computed by summing the opacities due to Oxygen and Water, with a small
empirical correction factor for other minor constituents.

The zenith opacities are used together with the elevation (number of air
masses) to compute $Tcal$, the SSB scaling factor
\begin{equation}
Tcal  = \frac{(T_{load} * (1. + Gain\_i) - T_{emi\_s} - Gain\_i * T_{emi\_i})}
           {B_s * e^{-Tau\_s * Air\_mass}}
\end{equation}
where $Tau\_s$ is the zenith opacity in the signal sideband.

\section{Calibration mode: COLD, AUTO, or TREC ?}

Based on these equations, four modes of calibration are available: TREC,
COLD, AUTO, and MANUAL.

\subsection{No Nitrogen available : TREC Mode}

This is what happens in \obs\ when used in TREC mode (specified by {\tt
  OBS$\backslash$SET CAL TREC}) and when you do a CAL command without the
argument COLD. In this mode, the receiver temperature is used to determine
the gain (conversion from counts to temperature) of the receiver.

With this method, the parameters which have to be specified are thus
\begin{itemize}
\item{$T_{load}$}, the hot load (also called chopper) temperature.
\item{$T_{rec}$}, the receiver noise (DSB) temperature.
\item{$P_{amb}$}, the external pressure.
\item{$T_{amb}$}, the outside pressure.
\item{$F_{eff}$}, the forward sky coupling efficiency.
\item{$Gain\_i$}, the ratio of the gain in image band to that in the signal
  band.
\end{itemize}
plus a number of (semi) constant parameters ($T_{cab}$, site altitude,
etc...).  The elevation (and hence $Air\_mass$) is read by the control
program, as are the frequencies of the signal and image bands. All other
parameters are derived by the calibration procedure.

The weather parameters may be supplied by the meteo station, specified by
the {\tt OBS$\backslash$SET AMBIENT AUTO} command.  If this automatic
station does not work, you have to supply by hand $P_{amb}, T_{amb}$ and
$Humidity$. This is done by the {\tt OBS$\backslash$SET AMBIENT P\_amb
  T\_amb Humidity} command.  The $Humidity$ is not used for the
calibration, but by the drive program for the refraction correction to the
telescope pointing.

\subsection{No Nitrogen available : AUTO mode}

Another alternative is to assume that you know the water vapor content,
either from a previous measurement and assuming the weather is stable, or
from theoretical curves as a function of pressure, temperature and relative
humidity. With the knowledge of the atmospheric parameters, the model
predicts $T_{sky}$ and $Tau$.  In this mode, instead of $Trec$ above, you
must supply $Water$ the amount of precipitable water vapor in millimeter.
$Trec$ is then given by (from Eq. 7)
\begin{equation}
Trec = \frac{T_{load} * Mean\_atm - T_{emi} * Mean\_load}
                 {Mean\_load - Mean\_atm}
\end{equation}

This is what happens in \obs\ when used in AUTO mode (specified by {\tt
  OBS$\backslash$SET CAL AUTO}) and when you do a CAL command without the
argument COLD.

\subsection{CAL COLD calibration}

This is the most complete calibration procedure, with the minimum number of
assumptions.  The first step is to measure the power detected by the
receiver on the Sky, then on a warm load (the Chopper), then on a cold
(liquid nitrogen) load, averaged over the channels of a specific backend.
For a perfectly linear detector, the output power is proportional to the
total input noise (Receiver + Load), hence the receiver temperature is
simply given by

\begin{equation}
Trec = \frac{T_{load} * Mean\_cold - T_{cold} * Mean\_load}
{Mean\_load - Mean\_cold}
\end{equation}
Then, one can proceed as in TREC mode.

The description above refers to what happens when \obs\ when you use the
CAL COLD command (in either TREC or AUTO mode).

\subsection{MANUAL mode}

The last (and worst) alternative is to specify all the atmospheric
parameters.  This is known as the MANUAL mode (specified by {\tt
  OBS$\backslash$SET CAL MANUAL} command).  The sky emission is then
computed from 4 parameters which are the atmospheric temperatures
($T_{atm\_s}$ and $T_{atm\_i}$) and zenith opacities ($Tau\_s$ and
$Tau\_i$) in signal and image band respectively
\begin{equation}
T_{sky\_s} = T_{atm\_s} * (1. - e^{-Tau\_s*Air\_mass} )
\end{equation}
\begin{equation}
T_{sky\_i} = T_{atm\_i} * (1. - e^{-Tau\_i*Air\_mass} )
\end{equation}
Outside temperature, pressure and humidity are not used for the
calibration, although they are still necessary for the telescope pointing,
and the water vapor content is no longer needed (you replace four
independent parameters by a set of four other independent ones). $Tsys$ and
$Trec$ are computed from Eqs. 6 and 11, so that the forward efficiency and
gain image ratios are still required. For the calibration to be correct, it
is however up to you to ensure that the values supplied are realistic at
the frequency of your observations, and with the weather conditions you
have...

This mode is just an ``educated guess'' for the atmospheric calibration,
but it still corrects properly the relative gains of all the backend
channels.

\section{The temperature scale : ``Beam Efficiency''}

In Eq. 3, $B_s$ is the antenna to source coupling factor, required to
obtain a scale which corresponds to the {\bf brightness} temperature of
your source (which I presume is the only physical parameter which really
interests you).

If you knew your source structure exactly, this factor could in principle
be computed from the antenna radiation pattern. If the source is
unresolved, it is customary to use the so-called ``Main Beam Brightness''
temperature scale, which would be the brightness temperature of a source
just filling the main primary beam, hence using $B_s$ = $B_{eff}$, the Beam
efficiency. If the source extent is unknown, you would use $B_s$ =
$F_{eff}$, the Forward efficiency, pending further correction ($T_A^*$
scale, with no direct relation with the brightness temperature of the
source).

At the 30-m, $B_s$ is called {\tt B\_Eff} in \obs , and it can be set using
the {\tt OBS$\backslash$SET EFFICIENCY B\_Eff F\_Eff} command. ($F_{eff}$
is also noted {\tt F\_Eff} in \obs\ manual). $B_s$ is only a scaling factor
in your temperature scale.

At Plateau de Bure, $B_s$ is assumed equal to {\tt F\_eff}, because a
secondary amplitude calibration is required.

\section{The SKYDIP}

In all the calibration modes, $T_{sky}$ is derived from $T_{emi}$ by
correcting for the forward efficiency. Hence it is important to give
correctly this forward efficiency. This efficiency can be measured using a
skydip (command {\tt OBS$\backslash$SKYDIP}).

In a skydip, the total power on the sky is measured at various elevation.
Since the sky brightness varies with the elevation, while the rearward
losses are essentially constant, it is possible in a skydip to derive
simultaneously the atmospheric transmission (i.e. the water vapor content,
$Water$) and the forward efficiency $F_{eff}$. Provided $Trec$ is known (at
all elevations), a skydip can be done.  $Trec$ can be calculated from the
gains on the Chopper and on the Cold Load when Nitrogen is available, or
given by user input in TREC mode.  In the first case, elevation dependent
$Trec$ can be accounted for, while the second case require constant $Trec$.

\section{Continuum Calibration}

The description above assumes that the source signal comes only from the
(so-called) {\em signal} sideband. This is of course not true for continuum
radiation. If we neglect spectral index effects, one can derive the proper
calibration of a continuum source from the description above. $Tcal$ should
be multiplied by
\begin{equation}
\frac{1}{1 + Gain\_i * e^{(Tau\_s - Tau\_i) * Air\_mass}}
\end{equation}

Similarly for spectral line observations, if you are interested in the
image band, $Tcal$ should be multiplied by
\begin{equation}
e^{ (Tau\_i - Tau\_s) * Air\_mass} / Gain\_i 
\end{equation}

\section{Reduced Chopper Calibration}

With modern sensitive receivers, the system noise under good weather
conditions may be significantly lower than 300 K.  Accordingly, measuring
the total noise when looking at a warm load at 300 K requires a linear
response of the whole detection system over a dynamic range greater than
2-3.  This can be problematic.

There are several ways to bypass this limitation, for example
\begin{enumerate}
\item Using lower temperature (controlled or monitored) loads.  This is
  probably the best, but it requires cooling devices somewhere.
\item If the receiver is linear, but not the backend, it is possible to
  place a switchable attenuator in the IF chain, preferably immediately
  after the conversion to IF frequency.
\item Covering partially the beam by the absorber, the other part still
  looking at the sky.
\end{enumerate}

\subsection{Partial Beam Coverage}
This scheme has been implemented on Plateau de Bure, where the dynamic
range of the detectors is relatively small (only 2 between optimum
sensitivity and saturation).  The only change from the standard equations
presented before is the introduction of the ``beam filling factor'' or
``calibration efficiency'' $C_{eff}$ in the output on the chopper (from Eq.
(1-2))
\begin{equation}
Mean\_load / K = Trec + C_{eff} * T_{load} + (1-C_{eff}) * T_{emi}
\end{equation}

Algebra similar to that already exposed yields
\begin{equation}
Tcal  = C_{eff} * \frac{(T_{load} - T_{emi}) * (1. + Gain\_i)}
           {B_s * e^{-Tau\_s * Air\_mass}}
\end{equation}
just correcting $Tcal$ by an additional $C_{eff}$ scaling factor.

In TREC mode, the sky emission is derived by
\begin{equation}
T_{emi} = \frac{(T_{load} + Trec) * Mean\_atm * C_{eff}}
{Mean\_load - (1-C_{eff})*Mean\_atm} - Trec
\end{equation}
and in AUTO mode, the receiver temperature may be derived by
\begin{equation}
Trec = \frac{C_{eff} * Mean\_atm * T_{load} - (Mean\_load - (1-C_{eff}) 
* Mean\_atm) * T\_emi}{Mean\_load - Mean\_atm}
\end{equation}

\subsection{Attenuator}
Using an attenuator just after the 1st IF conversion may be more
convenient, because it then allows the backend to work at (almost) constant
level. Let us assume an attenuation factor $\eta$ (smaller than 1) is used
on the chopper.  Then
\begin{equation}
Mean\_load / K = \eta * (Trec+T_{load})
\end{equation}
and in TREC mode, the sky emission is given by:
\begin{equation}
T_{emi} =  \frac{\eta * (Trec+T_{load}) * Mean\_atm}{Mean\_load} - Trec
\end{equation}
while with a cold load, $Trec$ is given by
\begin{equation}
Trec = \frac{Mean\_cold * T_{load} - Mean\_load * T_{cold}}
{Mean\_load - \eta * Mean\_cold}
\end{equation}
$T_B$ is now given by
\begin{equation}
T_B = e^{\tau} \frac{\eta}{B_s}(T_{load}+Trec)
\frac{Mean\_sou-Mean\_atm}{Mean\_load}
\end{equation}

\section{Critical parameters, biases and errors}

\subsection{Low opacity approximation}
To outline the relative importance of all parameters, I will now derive an
approximate formula for the calibration factor $Tcal$ in the limiting (but
frequent) case where the opacities are weak and equal in the image and
signal bands.
\begin{equation}
T_{sky} = \tau * T_{atm}
\end{equation}
where $T_{atm}$ is the physical temperature of the absorbing layers.  Thus
\begin{equation}
T_{emi} = F_{eff} * \tau * T_{atm} + (1-F_{eff}) * T_{cab}
\end{equation}
\begin{equation}
\tau     = (T_{emi} - (1-F_{eff}) * T_{cab}) / ( F_{eff} * T_{atm} )
\end{equation}
\begin{equation}
Tcal    = C_{eff} * (T_{load} - T_{emi}) (1+Gain\_i) / ((1-\tau) * B_s)
\end{equation}
Eliminating $\tau$ gives
\begin{equation}
Tcal = C_{eff} \frac{(T_{load}-T_{emi}) * (1+Gain\_i) * T_{atm} * F_{eff}}
          {B_s * (F_{eff} * (T_{atm}-T_{cab}) + T_{cab} - T_{emi})}
\end{equation}

Although $T_{cab}$ is a weighted average of the physical temperature of the
cabin and the outside temperature, it is not very different from
$T_{load}$. Hence we can simplify :
\begin{equation}
Tcal = C_{eff} \frac{F_{eff} * (1+Gain\_i) * T_{atm}}
{B_s * ( 1 - F_{eff} * \frac{T_{cab}-T_{atm}}{T_{cab}-T_{emi}})}
\end{equation}
It is important to note that in this formula, $T_{atm}$ is dependent mostly
on the outside temperature and pressure (and site altitude of course), and
weakly on $\tau$ because $T_{atm}$ is the (physical) temperature of the
absorbing layers.  $T_{emi}$ is the effective temperature seen by the
antenna, and hence small compared to $T_{cab}$. The parameters are
$F_{eff}$, $B_{eff}$ and $Gain\_i$.  Setting reasonable numbers :
\begin{center}
  $T_{cab} = 290 K$, $T_{atm} = 240 K$, $T_{emi} = 50 K$, and $C_{eff} =
  1$,
\end{center}
yield
\begin{equation}  
Tcal = \frac{240 * (1+Gain\_i) * F_{eff}}{B_s * (1 - 0.2*F_{eff})}
\end{equation}

\subsection{SKYDIP effects}

Currently, $T_{amb}$ (the outside temperature) is used instead of $T_{cab}$
in Eq. 8.  The effect of this assumption is intricate, because it is used
also in the SKYDIP in which $F_{eff}$ and $Water$ are both determined. If
the atmospheric opacities are low, the sky brightness $T_{sky}$ is a linear
function of the number of airmass, and thus
\begin{equation}
T_{emi} = (1.-F_{eff})*T_{cab} + F_{eff} * C^{te} * Water * Air\_mass
\end{equation}
In this case $F_{eff}$ is underestimated, because $T_{amb}$ is usually {\em
  lower} than $T_{cab}$. An increase in $Water$ will compensate the lower
value of the forward efficiency. If the opacities are sufficiently high,
the linearity is no longer preserved, and the fit will be bad. However, for
20 K difference between $T_{amb}$ and $T_{cab}$, the effect on $F_{eff}$ is
only 0.01 for $F_{eff}$ around 0.90.

\subsection{Derivatives}
Another way to look at the critical parameters of the calibration is to
analyse the partial derivatives of $Tcal$ with respect to the parameters.

\subsubsection{Relative to $Trec$}
\begin{equation}
\frac{\partial Tcal}{\partial Trec} = 
\frac{\partial Tcal}{\partial T_{emi}} \frac{\partial T_{emi}}{\partial Trec} 
+ \frac{\partial Tcal}{\partial Tau} \frac{\partial Tau}{\partial Trec} 
\end{equation}
From Eq. (18)
\begin{equation}
\frac{\partial Tcal}{\partial T_{emi}} 
= \frac{Tcal}{T_{emi}-T_{load}} 
\end{equation}
and from Eq. (20)
\begin{equation}
\frac{\partial T_{emi}}{\partial Trec} = -1 + \frac{T_{emi}+Trec}
{T_{load}+Trec}
\end{equation}
thus
\begin{equation}
\frac{\partial Tcal}{\partial T_{emi}} \frac{\partial T_{emi}}{\partial Trec} 
= \frac{Tcal}{T_{load}+Trec}
\end{equation}
For the second term, from Eq. (18)
\begin{equation}
\frac{\partial Tcal}{\partial Tau} = Air\_mass * Tcal
\end{equation}
and from Eq. (27)
\begin{equation}
\frac{\partial Tau}{\partial Trec} =
\frac{\partial Tau}{\partial T_{emi}} \frac{\partial T_{emi}}{\partial Trec} 
\end{equation}
\begin{equation}
\frac{\partial Tau}{\partial Trec} =
\frac{1}{Air\_mass * T_{atm} * F_{eff}} \frac{\partial T_{emi}}{\partial Trec} 
\end{equation}
\begin{equation}
\frac{\partial Tcal}{\partial Tau} \frac{\partial Tau}{\partial Trec} =
\frac{Tcal}{T_{atm} * F_{eff}} \frac{T_{emi}-T_{load}}{T_{load}+Trec}
\end{equation}
giving finally
\begin{equation}
\frac{1}{Tcal} \frac{\partial Tcal}{\partial Trec} = 
\frac{T_{atm}*F_{eff} - T_{load} + T_{emi}}
{F_{eff}*T_{atm}*(T_{load} + Trec)}
\end{equation}

The typical numbers mentionned above, with $T_{load} = T_{cab}$, yield
\begin{equation}
\frac{1}{Tcal} \frac{\partial Tcal}{\partial Trec} = 
\frac{F_{eff}-1}{F_{eff}} \frac{1}{T_{load}+Trec}
\end{equation}
of the order of $3$ $10^{-4}$ per K. Note that this could be higher for
higher opacities (hence higher $T_{emi}$).

\subsubsection{Relative to $Tcab$}
Taking Eq. (30), one may compute the derivative with respect to Tcab:
\begin{equation}
\frac{1}{Tcal} \frac{\partial Tcal}{\partial T_{cab}} = 
\frac{(T_{atm} - T_{emi}} 
{(T_{cab}-T_{emi})*(T_{cab}-T_{emi}-F_{eff}*(T_{cab}-T_{atm})}
\end{equation}
or with the numerical values in Eq. (31) about $3.6$ $10^{-3}$ per K.

\subsubsection{Relative to $F_{eff}$}

Again, from Eq. (30), assuming $F_{eff}$ and $B_{eff}$ are independent,
\begin{equation}
\frac{1}{Tcal} \frac{\partial Tcal}{\partial F_{eff}} = 
\frac{1}{F_{eff}} + \frac{1}{\frac{T_{cab}-T_{emi}}{T_{cab}-T_{atm}}-F_{eff}}
\end{equation}
which is of the order of 1.4 for the numerical values in Eq. (31).  The
first term must be omitted if one assumes $B_s$ = $F_{eff}$, and in this
case the derivative is of the order of 0.26.

\subsubsection{Relative to $C_{eff}$}
\begin{equation}
\frac{\partial Tcal}{\partial C_{eff}} = 
\frac{Tcal}{C_{eff}} - \frac{Tcal}{T_{load}-T_{emi}}
\frac{\partial T_{emi}}{\partial C_{eff}} 
\end{equation}
\begin{equation}
\frac{\partial T_{emi}}{\partial C_{eff}} =
\frac{(T_{emi}+Trec)^2}{T_{load}+Trec}
\end{equation}
\begin{equation}
\frac{1}{Tcal} \frac{\partial Tcal}{\partial C_{eff}} = 
\frac{1}{C_{eff}} -
\frac{(T_{emi}+Trec)^2}{C_{eff}^2 * (T_{load}+Trec) * (T_{load}-T_{emi}) }
\end{equation}

\subsubsection{Relative to $\eta$}

$Tcal$ has a slightly different definition in this case, and
\begin{equation}
\frac{\partial Tcal}{\partial \eta} = \frac{Tcal}{\eta} +
\frac{\partial Tau}{\partial \eta} \frac{\partial Tcal}{\partial Tau} +
\frac{\partial Trec}{\partial \eta} \frac{\partial Tcal}{\partial Trec}
\end{equation}
\begin{equation}
\frac{\partial Tau}{\partial \eta} =
\frac{1}{T_{atm} * F_{eff}}\frac{\partial Temi}{\partial \eta} 
\end{equation}
\begin{equation}
\frac{\partial Temi}{\partial \eta} =
-\frac{T_{emi}+Trec}{\eta ^2} +
\frac{\partial Trec}{\partial \eta} \frac{T_{emi}-T_{load}}{T_{load}+Trec}
\end{equation}

In TREC mode, derivative with respect to $Trec$ must be omitted, and
\begin{equation}
\frac{1}{Tcal} \frac{\partial Tcal}{\partial \eta} = 
\frac{\eta * T_{atm} * F{eff} - T_{emi} - Trec}{\eta ^2}
\end{equation}

With a cold load, a corresponding equation could be derived using
\begin{equation}
\frac{\partial Trec}{\partial \eta} =
Trec \frac{Trec-T_{cold}}{T_{load}- \eta * T_{cold} + (1-\eta) * Trec}
\end{equation}

\subsection{Results}
Using the parameters used previously, and $C_{eff}$ = 0.5, $\eta$ = 0.6,
and $Trec$ = 100 K, we can make the following comparison of the variations
of $Tcal$
\\
\begin{tabular}{|l|c|c|c|r|r|}
\hline
Item & $T_{cab}$ & $Trec$ & $F_{eff}$ & $C_{eff}$ & $\eta$\\
\hline
Typical Error & 2 K & 10 K & 0.01 & 0.02 & 0.02 \\
Induced variation (in \%) & 0.7 & 0.3 & 1.3 & 2.0 & -0.5 \\
\hline
\end{tabular}
\\
This shows that the maximum effect is from $C_{eff}$ if this method is
used, from $F_{eff}$ otherwise. It also indicates that the ``reduced
chopper'' calibration is not a good technique, but a switchable attenuator
should be used instead.

\subsection{Consequences}

The most critical parameter of the calibration is the Forward Efficiency
$F_{eff}$. This parameter is a function of frequency (antenna surface
accuracy), but also of the receiver used (illumination) and of the {\em
  Focus} with a tendency to be too low if you are defocussed. If $F_{eff}$
is underestimated, $T_{sky}$ is underestimated (See equation 8 and note
that $T_{cab}$ is always greater than $T_{emi}$ except in very bad
sites...) and you may obtain anomalously low $Water$ vapor content, and
vice-versa.

The sideband gain ratio $Gain\_i$ is also a critical parameter.  $Gain\_i$
is not only a scaling factor as obvious in Eq. 8, but is also involved in
the derivation of the atmospheric model since the contributions from the
atmosphere in image and signal bands are considered. This effect will be
important only if the opacities in both bands are significantly different,
as for the J=1-0 line of CO.

On the 30-m telescope, because of the design of the cold load, $T_{cold}$
is also a function of frequency and receiver (it varies from 87 K at 230
GHz to 116 K at 75 GHz). The values of $T_{cold}$ are fairly well known by
the receiver engineers : ask them...

\section{Recommendations}

Remember that you can measure all calibration parameters only if nitrogen
is available, by the CAL COLD command.

\subsection{30-m}

Check the value of $T_{cold}$ for each receiver used. If the weather is
reasonably good, make a skydip ({\tt OBS$\backslash$SKYDIP} command) to
measure the forward efficiency. In case of scattered clouds, you may prefer
to rely on the preceding measurements of this parameter.  In any case, the
knowledge of the image band gain ratio, $Gain\_i$, is essential. This is
mostly a scale factor when signal and image opacities are similar, but this
is not entirely true close to strong atmospheric absorption bands (e.g. at
115 GHz).

The observing strategy then depends on the weather and (or) receiver
stability. Under good conditions (stable weather), you may calibrate with a
CAL COLD from time to time, and use the derived value of the water vapor
content for intermediate calibrations with a simple CAL AUTO. If the
weather is not stable, but the receiver is, use a CAL TREC instead of the
CAL AUTO.  CAL COLD is always best since you can monitor both water vapor
and receiver fluctuations.

Do not forget to focus ({\tt OBS$\backslash$FOCUS} command) the telescope
for the receiver you are most interested in.  Otherwise, the standard value
of the beam efficiency will be wrong.

\subsection{Interferometer}

No cold load is available, so one has to rely on TREC for at least one
receiver. $Gain\_i$ can be measured with the interferometer (3 antennas
required) using a strong continuum point source ({\tt OBS$\backslash$GAIN
  command}).

This "single-dish" amplitude calibration is only a first step for
interferometry. Additional degradation may come from phase noise.  This
degradation can be estimated from the amplitude of the phase calibrator (a
quasar, usually). However, most quasars are variable sources, with typical
time scales of months. Hence, flux referencing to a source of known flux is
also necessary.  Some quasars are regularly monitored to get the flux by
comparison with planets and/or compact H$_{II}$ regions.  This secondary
amplitude calibration allows the derivation of the true antenna efficiency
(Jy/K conversion factor) at the time of observations.

\subsection{Other problems}

I have not mentionned pointing accuracy. This problem certainly limits the
calibration accuracy in many circumstances.  There is no perfect solution
to that. Remember to set atmospheric parameters precisely for the
refraction correction.  Avoid checking the pointing on sources too far away
from your object as most pointing errors are systematic effects in azimuth
and elevation. The secondary calibration used at Plateau de Bure cancels to
first order this problem, since pointing errors should be very similar on
the phase calibrator and on the source, but correction for primary beam
attenuation can not be applied properly in case of pointing problems.

There are a few circumstances which prevents any good calibration, such as
anomalous refraction, high or time varying phase noise in interferometry,
or gain losses due to thin partial ice or water coating on the dish
surface. The main beam efficiency may drop by a factor 2 in the latter
case, and anomalous refraction of more than 10" has been observed also.
Unless you can monitor these effects by observing simultaneously a line of
known intensity in your source, calibration will be impossible in any of
these cases.

\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
