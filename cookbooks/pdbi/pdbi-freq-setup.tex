\documentclass[11pt]{article}

% GILDAS specific definitions
\include{gildas-def}

\title {Frequency Setup \\
  IRAM Plateau de Bure Interferometer}
\author {S. Guilloteau$^{1}$}
\date{Document probably older than you think}
\makeindex{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\maketitle{}

\begin{center}
Version 2.1
\end{center}

\begin{center}
  (1) Institut de Radio Astronomie Millim\'etrique\\
  300 Rue de la Piscine\\
  F-38406 Saint Martin d'H\`eres
\end{center}

This paper describes the current frequency scheme on Plateau de Bure.

\begin{rawhtml}
  Note: this is the on-line version of the "Frequency Setup"
  A <A HREF="../../pdf/pdbi-freq-setup.pdf"> PDF version</A>
  is also available.
\end{rawhtml}

Related information is available in:
\begin{latexonly}
  \begin{itemize}
  \item{IRAM Plateau de Bure Interferometer: OBS Users Guide}
  \item{IRAM Plateau de Bure Interferometer: Correlator Description}
  \item{ASTRO: Astronomical Software To pRepare Observations}
  \end{itemize}
\end{latexonly}

\begin{rawhtml}
  <UL>
  <LI> IRAM Plateau de Bure Interferometer:
  <A HREF="../obs-html/obs.html"> OBS Users Guide</A>
  <LI> IRAM Plateau de Bure Interferometer:
  <A HREF="../pdbi-correlator-html/pdbi-correlator.html"> Correlator Description</A>
  <LI> <A HREF="../astro-html/astro.html"> ASTRO:</A>  A Software to pRepare
  Observations
\end{rawhtml}

Revision history:\\
2.1 support for the 210-245 GHz receivers\\
2.0 support for the new correlator system.

\tableofcontents

\newpage
\section{Basic Equations}

For the Plateau de Bure interferometer, the conversion from sky frequencies
down to the IF band (100-600 MHz) is done using two local oscillators. The
first frequency conversion (with the LO1 oscillator) is double sideband
from sky frequency down to 1.5 GHz.  The second frequency is a lower
sideband conversion with a second oscillator LO2 around 1.875 GHz.  In this
document, I will use the following frequencies :
\begin{itemize}
\item{\em Fsyn} the synthesizer frequency
\item{\em Flo2} the second LO frequency
\item{\em Flo1} the first LO frequency
\item{\em Fsky} the sky frequency corresponding to the IF band center
\item{\em Fcent} a frequency in the IF band (100-600 $MHz$)
\item{\em Frest} the corresponding rest frequency at the source
\end{itemize}

All frequencies are derived from {\em Fsyn}, with frequency multipliers to
generate the relevant harmonic numbers, according to the following
equations (with frequencies in MHz)
\begin{equation}
   Flo2 = Fsyn - 0.5
\end{equation}
\begin{equation}
   Flo1 = M \times (H \times Flo2 - L \times Eps)
\end{equation}
\begin{equation}
   Fsky = Flo1 + S \times (Flo2-350)
\end{equation}
\begin{equation}
   Frest = \frac{Fsky - S \times (Fcent-350)}{Doppler} 
\end{equation}
\begin{equation}
   Frest = \frac{ ( (M \times H+S) \times Flo2 - M \times L \times Eps ) 
           - S \times Fcent}{Doppler}
\end{equation}

Five parameters introduced in these equations, {\em M}, {\em H}, {\em L},
{\em S}, and {\em Eps}, depend on the phase lock and receiver system, while
{\em Doppler} depends on the source:
\begin{itemize}
\item{\em H} is the harmonic number used to generate the LO1 from the LO2.
  With {\em Flo2} near 1.875 GHz, {\em H} is roughly 50 at 95 GHz.
\item{\em M} is the multiplier order: 1 for the 3mm system, 3 for the 1.3mm
  system.
\item{\em L} is +1 if the LO1 is in ``HIGH lock'', -1 in ``LOW lock''
\item{\em Eps} is equal to (100 + 50/512)
\item{\em S} is +1 for the upper sideband of the receiver (USB), -1 for the
  lower sideband (LSB).
\item{\em Doppler} is the total Doppler correction $(1-v/c)$ for the
  current source.
\end{itemize}
The phase lock system nominal performances are obtained for {\em Flo2} in
the range 1.850 - 1.900 GHz.

\section{Image Frequency Doppler Tracking}

\subsection{Basic Equations}
The LO2 frequency used to track a spectral line at a given frequency {\em
  Frest} centered in the IF band (350 $MHz$) is
\begin{equation}
 Flo2 = \frac{ Frest \times Doppler + S \times 350 + M \times L \times Eps }
 { M \times H + S }
\end{equation}
and, from eq. (5), the image rest frequency at the band center is
\begin{equation}
 F_{I} = \frac{ (M \times H-S) \times Flo2 
         - M \times L \times Eps + S \times 350}
         {Doppler}
\end{equation}
A little algebra follows \\

$ F_{I} = \frac{ \frac{M \times H-S}{M \times H+S} \times ( Frest \times
  Doppler + S \times 350 + M \times L \times Eps ) - M \times L \times Eps
  + S \times 350}
{Doppler} $\\

$ F_{I} = \frac{ (M \times H-S) \times ( Frest \times Doppler + S \times
  350 + M \times L \times Eps ) + (M \times H+S) \times (S \times 350 - M
  \times L \times Eps) }
{(M \times H+S)\times Doppler} $\\

$ F_{I} = \frac{M \times H-S}{M \times H+S} \times Frest + \frac{ (M\times
  H-S+ M\times H+S) \times S \times 350 - (M \times H+S- M \times H+S)
  \times M \times L \times Eps }
{(M \times H+S)\times Doppler} $\\

$ F_{I} = \frac{M \times H-S}{M \times H+S} \times Frest + \frac{ 2 \times
  M \times H \times S \times 350 - 2 \times S \times M \times L \times Eps
  }
{(M \times H+S) \times Doppler} $\\

\vskip 0.4 cm and result in
\begin{equation}
 F_{I} = \frac{M \times H-S}{M \times H+S} \times Frest
         + \frac{ 2 \times S \times M \times ( H \times 350 - L \times Eps)}
           {(M \times H+S) \times Doppler}
\end{equation}
\\
This result is not independent of the Doppler effect. Accordingly, it the
doppler tracking is not exact for the image frequency.  and for 2 different
values, $D1$ and $D2$, corresponding to different velocities $V1$ and $V2$,
we obtain (assuming no change of $H$)
\begin{equation}
 \delta F_{I} = \frac{D2-D1}{ D2 \times D1} \times 
            \frac{ 2 \times S \times M \times ( H \times 350 - L \times Eps) }
            {M \times H+S}
\end{equation}
or, assuming $V1$ and $V2 << c$, and with $H >> 1$, $M > 1$, the frequency
shift in MHz is
\begin{equation}
 \delta F_{I} = \frac{\delta V}{c} \times 700 
\end{equation}
or, in velocity (in km.s$^{-1}$)
\begin{equation}
\delta V_{I} = \delta V \frac{700}{F_I} 
\end{equation}

\subsection{Consequences}

The velocity variations have three slightly different origins
\begin{itemize}
\item the diurnal variation due to the earth rotation. The maximum value is
  $\delta V = 0.90$ $km.s^{-1}$ in 12 hours.
\item the yearly variation due to earth orbital motion. The order of
  magnitude is $\delta V = 15$ $km.s^{-1}$ for two months, with a maximum
  value of $\delta V = 60$ $km.s^{-1}$ for 6 months
\item the source velocity. Typical value is $\delta V = 60$ $km.s^{-1}$ for
  galactic objects.
\end{itemize}

Accordingly, the following frequency and velocity differences are induced
at 95 GHz USB (harmonic number 50) in the three cases :
\begin{itemize}
\item{Earth rotation : maximum $\delta F_I = 0.002$ $MHz, \delta V_I =
    0.007$ $km.s^{-1}$}
\item{Earth orbit : typical $\delta F_I = 0.035$ $MHz, \delta V_I = 0.11$
    $km.s^{-1}$,\\ maximum $\delta F_I = 0.14$ $MHz, \delta V_I = 0.45$
    $km.s^{-1}$}
\item{Source to source : $\delta F = 0.14$ $MHz$}
\end{itemize}
Note that the frequency shifts do not depend on the observing frequency.

\section{Frequency Setup Choice}

\subsection{Choice of Receiver Band: USB or LSB}

Above 114 GHz, only the USB tuning works. Below 114 GHz, if only one line
has to be observed, use the LSB setting. Receivers can be tuned with
sideband rejection, thus improving significantly the system temperature (up
to a factor 2).

When one line must be observed in each band, the Doppler tracking is exact
only for one of the two receiver bands. Presently, no software correction
is implemented to correct for the resulting frequency shift. Since the
elapsed time for any observing project will be typically 2 months, the
observer should take care of this peculiarity if the desired spectral
resolution is higher than 0.3125 MHz (in practice, this happens for the 20
and 40 MHz band with 256 channels). Otherwise, the effect can be neglected.
Source to source variations may be important. In such a case, select the
band according to the line to be observed with the highest resolution, or
according to the most important line if resolutions are identical.

\subsection{Choice of Correlator Setup}

The correlator offers 6 units, of up to 160 MHz width each, with a total of
up to 256 channels per baseline per unit.  Each unit is connected to a
variable frequency local oscillator (LO3) followed by an image rejection
mixer for conversion of the IF band (100-600 MHz) into baseband (0-80 MHz).
The LO3 frequencies are adjustable by step of 0.625 MHz, with a small
offset of 50/512 MHz added because of the general frequency scheme.

40, 80 and 160 MHz bandwidths are obtained by using the USB and LSB outputs
of the LO3 image rejection mixer (separation better than 30 dB), with a
filter of 20, 40 and 80 MHz respectively. Sampling is done at 160 MHz. At
160 and 80 MHz bandwith, correlator boards operate with a time multiplex
factor (TMF) of 4 and 2, producing 64 and 128 channels respectively. At 40
MHz, 256 channels are obtained.  The 20 MHz band is obtained by using the
LSB output of the mixer, and cascading two correlator boards in pass-along
to obtain also 256 channels, with a resolution of 0.078 MHz.

The frequency coverage of the correlator is thus extremely flexible, but
care must be taken of the compromise between bandwidth and resolution.  If
more than one line has to be observed simultaneously, the observer should
be careful to choice the appropriate setup, but many lines can be observed
simultaneously, specially when one takes into account coincidences between
the Upper and Lower sidebands (lines separated by 2.6 to 3.5 GHz). Note
that in practice, 3 or 4 units are left in broad band mode to provide
continuum flux measurement (and accurate amplitude and phase calibration),
so that the setup currently concerns 2 or 3 units.

The {\tt ASTRO$\backslash$LINE} command, in the \astro\ program implements
a graphic display of the frequency setup, with overlaid line frequencies.
The spectral correlator bands are displayed according to the setup
specified in the {\tt /SPECTRAL} option.  The syntax of this option is
identical to that of command {\tt OBS$\backslash$SPECTRAL} in the \obs\ 
program.

Command {\tt ASTRO$\backslash$LINE/SPECTRAL Unit Band LO3} can be used to
overlay a spectral correlator setup on a plot produced by a previous {\tt
  ASTRO$\backslash$LINE/SPECTRAL Unit Band LO3}.

\subsection{{\tt LINE} command help}
\begin{verbatim}
          [ASTRO\]LINE Name Frequency Band [Lock [Center [Harm]]]
          [/MOLECULES File] [/SPECTRAL Unit Bandwidth LO3]


     Plot rest frequency coverage of  Plateau  de  Bure  Interferometer.
The  command syntax is identical to that of the corresponding command in
OBS.
     Name is a line name to label the plot
     Frequency is the center frequency in GHz
     Band should be USB or LSB
     Lock (optional) is LOW or HIGH
     Center (optional), the center IF2 frequency in MHz, default 350.
     Harm (optional), the harmonic number.

     The option /MOLECULES enables  to  plot  the  rest  frequencies  of
several  transitions,  found  in  file  File.   This is a text file with
entries:
Freq    'chain'
where Freq is the rest frequency in GHz and 'chain' the line name to  be
plotted by GreG, between single quotes.  Example:
88.632  'HCN'
89.081  'HCO\u+'

     The option /SPECTRAL  is  used  to  specify  the  desired  spectral
correlator configuration.
- Unit is the unit number (1 to 6)
- Bandwidth is the bandwidth in MHz (20, 40, 80 or 160)
- LO3 is the center frequency (in MHz) of the band center,

          [ASTRO\]LINE/SPECTRAL Unit Bandwidth LO3
     Will overlay the specified correlator on a LINE plot

     Because only one synthesizer is used to generate the  1st  and  2nd
LOs,  the  IF  frequency  is  slightly variable (between 1.500 and 1.550
GHz), and the Doppler tracking is correct only for  the  specified  band
(USB  or  LSB).   The  last  specified source is used to get the Doppler
shift.   The  image  frequency  doppler  shift  is  wrong  by  at   most
0.11 km/s for a two month experiment.

\end{verbatim}

\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
