\documentclass[11pt]{article}

% GILDAS specific definitions
\include{gildas-def}

\title{Practical implementation of the atmospheric phase correction for the
  Plateau de Bure Interferometer}
\author{R.Lucas}
\date{Document probably older than you think}

\makeindex{}

\newcommand{\www}{http://iram.fr}
\newcommand{\doc}{http://iram.fr/doc}
\newcommand{\newsletter}{http://iram.fr/dist/newsletter}
\newcommand{\htwoo}{\mbox{H$_2$O}}
\newcommand{\ww}{\mbox{$w_{\rm \small \htwoo}$}}
\newcommand{\tr}{\mbox{$T_{\rm R}$}}
\newcommand{\pref}{\mbox{$P_{\rm REF}(t)$}}
\newcommand{\tsysmon}{\mbox{$T_{\rm SYS}$}}
\newcommand{\tem}{\mbox{$T_{\rm EM}$}}
\newcommand{\temi}{\mbox{$T_{\rm EM,I}$}}
\newcommand{\tems}{\mbox{$T_{\rm EM,S}$}}
\newcommand{\tchop}{\mbox{$T_{\rm CHOP}$}}
\newcommand{\tcold}{\mbox{$T_{\rm COLD}$}}
\newcommand{\tloss}{\mbox{$T_{\rm LOSS}$}}
\newcommand{\tamb}{\mbox{$T_{\rm AMB}$}}
\newcommand{\csky}{\mbox{$C_{\rm SKY}$}}
\newcommand{\ccold}{\mbox{$C_{\rm COLD}$}}
\newcommand{\cchop}{\mbox{$C_{\rm CHOP}$}}
\newcommand{\etaf}{\mbox{$\eta_{\rm F}$}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\maketitle{}

\section {Introduction}

As soon as the new dual frequency receivers have been installed on the
\htmladdnormallink{Plateau de Bure}{\doc/bure.html}, it has been clear that
the atmospheric phase correction method investigated by Michael Bremer at
IRAM could be implemented in the interferometer, thanks to the good
stability of the receivers: see the \htmladdnormallink{May 1995
  Newsletter}{\newsletter/may95/may95.html}, the \htmladdnormallink{Nov.
  1995 Newsletter}{\newsletter/nov95/nov95.html}, and the technical reports
prepared by M. Bremer.

The purpose of the present document is to describe how the correction
method has been implemented in CLIC, for the continuum data, and how the
real time acquisition system will be modified to apply the correction in
real time to the spectroscopic data.

\section{Background}

The principle of the method is the following: the atmospheric phase
fluctuations are due to different, time-varying water vapor content in the
line of sight of each antenna across the atmosphere; these differences in
water vapor content are measurable by monitoring their emission at radio
frequencies. We thus use the differences in emission, measured by using the
1mm channel of the receivers as an atmospheric monitor, to correct for
phase fluctuations in both the 1mm and 3mm windows.

\subsection {Monitoring the atmospheric emission}

The radiation temperature \footnote{``radiation temperature'' here actually
  stands for ``Rayleigh-Jeans equivalent brightness temperature''} of the
atmosphere \tem\ is determined by using the usual ``chopper wheel''
calibration technique: one compares the emission of the atmosphere, to both
an ambient load of radiation temperature \tchop\ (the absorbing table that
is switched in to the beam), and a cold load of radiation temperature
\tcold\ (an absorber in the receiver dewars towards which the beam is
redirected by switching in a corner mirror).

The three measurements give (\tamb\ is the temperature in the receiver
cabin and \etaf\ the forward efficiency):

$$\csky = K (\tr+ (1-\etaf)\tamb+\etaf \tem) $$
$$\cchop = K (\tr+ \tchop) $$
$$\ccold = K (\tr+ \tcold) $$

These measurements are combined to give \tr\ and \tem. Actually \tr\ is
quite stable and we can avoid too frequent cold load measurements by
assuming \tr\ constant.

The system noise of the atmospheric monitor is given by:

$$
\tsysmon = \frac{\tr + (1 - \etaf) \tamb}{\etaf}+\tem = \tloss + tem $$

Note that this is different from the usual formula for the system
temperature, since our reference plane is {\em below} the atmosphere, not
above.

Thus a variation $\Delta \tem$ of atmospheric emission leads to a variation
$\Delta P$ of the total power $P$, given by:

$$
\Delta P/P = (\Delta \tem + \Delta\tloss)/ \tsysmon$$

where we allowed for a change in \tloss\ due to a variety of possible
causes: variations in receiver noise, ambient temperature, forward
efficiency (or ground noise).

\subsection{Atmosphere model}

The \htmladdnormallink{atmosphere model}{\doc/amplitude/amplitude.html} in
use at Bure has been extended to compute the atmospheric excess pathlength
by integrating the refractive index of wet air along the line of sight
across the atmosphere.

The total precipitable water vapor content is computed, using an iterative
procedure, to verify the condition:

$$
\tem(observed) = (\tems(\ww) + G \temi(\ww))/(1+G) $$
where G is the
image side band gain ratio, and \tems\ and \temi\ are the radiation
temperatures of the atmosphere computed from the atmosphere model in the
signal and image side bands of the monitoring receiver.  The model depends
also on the ambient pressure, the ambient temperature, and the source
elevation, which are known or directly measured.

The atmospheric excess pathlength $l$ at the observing frequency is then
computed by integration when \ww\ is known. One also computes the
derivative $\frac{\partial l}{\partial \tem} $.

\subsection {The phase correction}

The atmospheric phase affecting the observations is given by:

$$
\phi(t) = \frac{2 \pi}{\lambda} l(t) $$

Ideally one would like use \tem\ measured every second for each antenna, to
compute the corresponding $\phi(t)$, and to correct the measured baseline
phases.

Practically this is not feasible, since $\phi(t)$ amounts to many turns,
and instrumental effects affect the measured \tem. The receiver gains, the
forward efficiencies vary with the source elevation. So when the antennas
are moved in elevation by more that a few degrees, like when switching
between observed sources, these effects spoil the measured \tem\ and
prevent the use of the derived pathlength values.

Instead we use a differential procedure: once the antennas track a given
source, one calibrates the atmosphere to calculate $\tem(t_0)$, $l(t_0)$,
and ${\partial l}/{\partial \tem}(t_0)$.

The relative change in total power is:
$$
\Delta \tem(t) = \tsysmon \frac{\Delta P(t)}{P} + \Delta\tloss(t) $$
The
phase correction applied is then:
$$
\Delta \phi(t) = \frac{2 \pi}{\lambda} \frac{\partial l}{\partial \tem}
\left( \tsysmon \frac{\Delta P(t)}{P} + \Delta\tloss(t) \right)$$

which we may rewrite as
$$
\Delta \phi(t) = \frac{2 \pi}{\lambda} \frac{\partial l}{\partial \tem}
\frac{\tsysmon(t_0)}{P(t_0)}\left(P(t)-\pref\right)$$

The choice of \pref\ will be made in order to include as much as possible
all the slow effects that contribute to $\Delta\tloss(t)$. It is not a
problem if long term atmospheric effects are also included in \pref\ ;
these effects will not be removed by the radiometric phase correction, but
by the traditional phase referencing on a nearby calibrator.

Several choices of \pref\ may be used:

\begin{enumerate}
\item Use for a given time interval (e.g. a scan of $ 1-4 $ min. duration)
  the average of $P(t)$ in the same interval.  This ``minimal'' choice has
  the advantage of correcting the amplitude of the decorrelation effect as
  much as possible (of course decorrelation effects occurring inside an
  elementary sampling interval, one second, will not be corrected; but this
  may usually be neglected). The average phase should not be affected. This
  is the scheme we plan to apply in quasi-real time in the correlators, to
  the spectral line data.
\item Use, for a longer time scale (e.g. the on-source time between two
  observations of the phase calibrator), a linear fit to the $P(t)$ data as
  the \pref\ reference. This is basically the same choice, but additionnaly
  allows for a linear drift of $\Delta \tloss$ during the observation. This
  choice is currently implemented in CLIC (see below).
\item Use a smooth curve approximation to $P(t)$ on a longer time scale
  (one or several hours). This approximation should be different for the
  source data and the phase calibrator data. This has not been really tried
  yet.
\item Use a realistic model of the system noise as a function of azimut and
  elevation, and other parameters: although some effects should be easily
  calibrated out (e.g. by monitoring the temperature of the receiver
  cabin), such a detailed knowledge of the ground noise properties of the
  PdB antennas is difficult to reach, and not yet available.
\end{enumerate}

\section{Implementation in the acquisition - data reduction system:}

\subsection{In quasi-real time:}

\begin{itemize}
\item Task {\tt RDI} solves the atmospheric calibration observations (with
  or without cold load measurement), to obtain the correction factors $F$
  in $\Delta \phi(t) = F \left( P(t) - \pref \right)$. There is one such
  coefficient for each side band of each receiver in each antenna.  The
  standard atmospheric model is used, but the data is interpolated out of a
  pre-computed table to save computing time (the table name is the logical
  name {\tt GAG\_ATMOSPHERE} which should translate to {\tt
    COMP\$DISK:[CONTROL.OBS]ATM-BURE-VAX.BIN}). The new parameters are
  stored in the data files in the new header section named ``Atmospheric
  monitor''. {\em This is currently implemented}.
  
  At the same time a default is set for the quantity \pref\ discussed
  above: it is the measured atmosphere emission at the time of the last
  calibration.  This will have to be refined by the command {\tt MONITOR}.
\item The correlators will soon do the correction on the spectral line
  data, using the scan-averaged atmospheric emission as a reference. For
  safety reasons, spectral data will have to be averaged both with and
  without phase correction.  More powerful CPU's will be needed for the
  correlators (which are being installed and tested).
\item The CLIC data format had to be extended to allow storing both
  phase-corrected and uncorrected data. So far the data section of each
  observation in a CLIC file would contain one record of continuum data for
  each second (or longer time records after data compression by {\tt
    COMPRESS}), and a single average record containing scan-averaged
  continuum and spectral line data. In the new format there will
  potentially be two such average records, one with uncorrected, the other
  with phase-corrected data.
  
  In fact three observing modes are possible, selected by a new OBS
  command:
\begin{itemize}
\item {\tt SET PHASE ON}, to record only phase-corrected data.
\item {\tt SET PHASE OFF}, to record only uncorrected data.
\item {\tt SET PHASE BOTH}, to record both uncorrected and phase-corrected
  data.
\end{itemize}
\end{itemize}

\subsection{In the calibration program (CLIC):}

\begin{itemize}
\item {\em Filling in the data parameters of receiver 2:} Receiver 1 (3mm)
  data recorded before Nov. 11 1995 did not contain information on receiver
  2 parameters, on which the phase correction is based.  The current CLIC
  version automatically fills in these parameters when the data is read. Of
  course this only works if at least one correlator unit was connected to
  receiver 2.  An informational message is given when this operation is
  done. This has been effective for some time already (2 months).
\item {\em Solving the atmospheric calibrations} for the atmospheric
  monitor parameters. For the newest data (since Nov 11, 1995) this is done
  in real time.  For older data this must be done manually, using either
  command {\tt ATMOSPHERE}, which does exactly what is currently done by
  RDI at Bure, or command {\tt MONITOR}. To execute these commands, the
  currect index of CLIC should include all data to be processed, including
  the atmospheric calibrations scans (procedure {\tt CALI}).
\item {\em Setting a reasonable value for \pref}. This is done using
  command {\tt MONITOR} which applies the second option described above: a
  linear fit to $P(t)$. The description of the command is:

\noindent{\tt MONITOR delta\_time} 

The scans in the current index are grouped in intervals of maximum duration
`delta\_time' (in seconds); source changes will also be used to separate
intervals. In each interval a straight line is fitted in the variation of
atmospheric emission as a function of time; this line will be the reference
value \pref\ for the atmospheric correction.

The default for `delta\_time' is 3600 s, which usually results in using the
on-source time between two phase calibrator observations as intervals for
evaluation of \pref.

One may also try and extend `delta\_time' and use this command to process
the source observations and the phase calibrator observations separately
(but beware that using very long times will produce long term phase drifts
into the phase correction, which will have to be removed by the usual phase
calibrations anyway).

After using this command, one may plot \pref\ using {\tt SET Y ATM\_REF}
and {\tt SET X TIME}, in antenna mode; $P(t)$ itself is available with {\tt
  SET Y ATM\_EM}.

\item {\em Finally, applying (or not) the correction} is done via {\tt SET
    PHASE ATMOSPHERE} (or {\tt SET PHASE NOATMOSPHERE}).  This should work
  for all plots and $uv$ table construction, focus solving, flux solving,
  .... When the corrected spectral line data is available, it will switch
  to the phase-corrected average records.
  
  After a plot, one may check the $F$ coefficients used by typing {\tt SHOW
    CORR}. The coefficients are given in radians per unit of $P(t)$.

\end{itemize}

\begin{thebibliography}{}
  
\bibitem{} Bremer, M., IRAM working report, April 1994
\bibitem{} Bremer, M., IRAM working report n. 238, Nov. 1995
\end{thebibliography}

\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
