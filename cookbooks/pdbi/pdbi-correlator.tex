\documentclass[11pt]{article}

% GILDAS specific definitions
\include{gildas-def}

\title{Narrow Band Correlator Description \\
  IRAM Plateau de Bure Interferometer}
\author{S.Guilloteau$^{1}$, R.Lucas$^{1}$, R.Neri$^{1}$}
\date{2000}
\makeindex{}

\newcommand{\cam}{\mbox {\tt correlator}} 
\newcommand{\corr}{\mbox {\tt correl}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\maketitle{}

\begin{center}
  Version 1.3
\end{center}

\begin{center}
  (1) Institut de Radio Astronomie Millim\'etrique\\
  300 Rue de la Piscine\\
  F-38406 Saint Martin d'H\`eres
\end{center}

This document is intended for IRAM astronomers to provide information
about the narrow band correlator and the modifications made in CLIC
and OBS to support it. Details of the correlator architecture can be
found in the backend group technical manuals.

\begin{rawhtml}
  Note: this is the on-line version of the "Correlator Description"
  A <A HREF="../../pdf/pdbi-correlator.pdf"> PDF version</A>
  is also available.
\end{rawhtml}

Related information is available in:
\begin{latexonly}
  \begin{itemize}
  \item{IRAM Plateau de Bure Interferometer: Introduction}
  \item{IRAM Plateau de Bure Interferometer: OBS Users Guide}
  \item{IRAM Plateau de Bure Interferometer: Atmospheric Calibration}
  \item{CLIC: Continuum and Line Interferometer Calibration}
  \end{itemize}
\end{latexonly}

\begin{rawhtml}
  <UL>
  <LI> IRAM Plateau de Bure Interferometer:
  <A HREF="../noema-intro-html/noema-intro.html"> Introduction</A>
  <LI> IRAM Plateau de Bure Interferometer:
  <A HREF="../obs-html/obs.html"> OBS Users Guide</A>
  <LI> IRAM Plateau de Bure Interferometer:
  <A HREF="../pdbi-amp-cal-html/pdbi-amp-cal.html"> Atmospheric Calibration</A>
  <LI> <A HREF="../clic-html/clic.html"> CLIC:</A>  Continuum and Line
  Interferometer Calibration
  </UL>
\end{rawhtml}

Revision history:\\
1.3 6 Antenna correlator \\
1.2 5 Antenna version \\
1.1 Introduction of the high resolution mode on unit 5.

\tableofcontents

\newpage
\section{Introduction}

The new six-antenna correlator replaces the previous five-antenna backends
on the Plateau de Bure. The latter will be shipped to Spain and
reconfigured to cope with HERA, the multi-beam heterodyne array in use at
the 30m telescope.

\section{Description}

\subsection{Hardware}

The \cam\ is made of 8 fully independent units, connected to the IF system
through a software configurable distribution system.  Each unit processes 6
antennas and 15 baselines.  The 8 units are totally independent from each
other.

Each unit is divided into three parts: the IF processor, controlled by a
simple micro-controller (IF-micro), the master digital processor,
controlled by a 68040 microprocessor, and the satellite digital processor,
controlled by a 68030 microprocessor. The ``master'' crate contains all
samplers, the main control board, correlator board, and a high speed (1
Gbit) serial link emitter to transmit the samples to the ``satellite''
crate.  The ``satellite'' crate contain a high speed receiver, and
correlator board to process the remaining correlations.

The ``master'' micros communicate to the IF-micro through a serial link.
All 68xxx processors are linked to the main acquisition computer of the
PdBI by a dedicated Ethernet link.  The IF-micro controls all the low-level
functions required in the analog part: frequency setting, low-pass filter
selection, oscillator phase control (value and rate).  The ``master''
microprocessors control all the digital parts: multiplex factor, delay
steps, correlator board readout, Fast Fourier Transform, small delay
correction, bandpass correction, frequency and time averaging, etc\ldots
The ``satellite'' micros do not handle any IF or sampling operation: only
correlator board readout and subsequent calculations are performed here.

\subsection{Functionalities}

Each unit can operate in the following modes:
\begin{center}
\begin{tabular}{|c|r|r|r|}
\hline
Mode & Band & Channels & Spacing \\
     & (MHz) &         & (MHz)   \\
\hline
1    & 320       & $2 \times  64$       & 2.5    \\
2    & 160       & $1 \times 128$       & 1.25  \\
3    & 160       & $2 \times 128$       & 0.625  \\
4    &  80       & $1 \times 256$       & 0.312  \\
5    &  80       & $2 \times 256$       & 0.156  \\
6    &  40       & $1 \times 512$       & 0.078  \\
7    &  20       & $1 \times 512$       & 0.039  \\
\hline
\end{tabular}
\end{center}
The effective resolution is 1.2 or 1.6 times the channel spacing, depending
upon the apodization function.

Each unit can be placed anywhere in the [100,680] MHz IF band, by steps of
0.625 MHz using a local oscillator referred to as the LO3.

%The 40, 80 and 160 MHz bandwidth are synthesized by the upper and
%lower sidebands of an IRM (Image Rejection Mixer) which converts the
%band around the LO3 frequency to baseband. Low pass filters of 20, 40
%and 80 MHz allow to select the desired bandwidth.

%The 20 MHz band is produced by analyzing only the upper sideband of
%the IRM, but with two correlator boards in pass-along to improve
%spectral resolution.

%The 10 MHz bandwidth is available only on unit 5. It is produced by
%by slowing the clock rate to 20 MHz instead of 40 MHz. The input level
%is however somewhat lower than on other bandwidths, so that one should
%avoid placing this subband too close to the receiver band edges.

\subsection{Data Types}

The basic integration step (dump) is 1 second. Transmitting a full spectrum
is impossible at such a rate: there are too many channels... Available
integration times usually range from 15 to 240 seconds, although a few
specific observing procedures have a shorter duration (5 seconds). However,
the correlator produces two types of data:
\begin{itemize}
\item the Time data, which is a frequency averaged data computed every
  second. Each unit produces its time data, by averaging the results of an
  integration step over a user-defined frequency band (to be selected
  within the IF band of the unit, of course). This data is also called the
  ``Continuum'' data.
\item the Spectral data, i.e. the time averaged spectrum for the whole
  integration. This data is also called the ``Line'' data.
\end{itemize}

\subsection{Software}

The whole correlator is controlled from the host computer by a program
named ``\corr '', which communicates with all 8 units every second.

Every second, \corr\ sends to all connected units the desired observing
mode, the rate and phases for the LO3, the delays, etc\ldots, and retrieves
Continuum data from the previous second. This is done by scanning
sequentially the connected units.

Every second, the units perform the FFT of the lag data, apply the ``IF
Pass Band'' correction, correct the phases for small delay offsets, and
compute the Continuum data by averaging over a frequency band specified by
\corr .

At the end of an observation, \corr\ instruct a second program, {\tt
  correl\_spectra} to read sequentially all connected units to recover the
Line data, cross or auto-correlation spectra on all baselines or all
antennas.

After some reordering, \corr\ transmits the data (Continuum and Line) to
``{\tt RDI\_PROGRAM}'' which computes and applies atmospheric calibration
before writing it on disk.

\section{OBS}

\obs\ has been modified to handle the new possibilities of the \cam .  A
number of commands had small changes, mostly concerning integration times.
Major modifications are:
\begin{itemize}
\item Display:\\
  New display layout, to have the correlator configuration
\item {\tt SPECTRAL}\\
  The syntax of the {\tt SPECTRAL} command has been changed:\\
  {\tt SPECTRAL Iunit Band Center/CONTINUUM Band Center}\\
  See the internal help for details
\item {\tt POINT}\\
  Accepts the {\tt /FAST} option even in interferometry mode
\item {\tt POINT PSEUDO}\\
  A new command making a pointing in autocorrelation, with a
  pseudo-continuum mode using the correlator. Analysis is provided by
  \clic\ command SOLVE PSEUDO.
\item Single-Dish mode\\
  Single-dish mode has disappeared.
\end{itemize}

\section{RDI and RED}

Both programs have been extensively modified. RED has disappeared.  RDI now
only computes atmospheric calibration and writes data. An additional
program (named AFF) monitors the running acquisition (subscan number) and
computes the real-time amplitude and phases for display by {\tt STS C}.

In order to save time, RDI returns control to \obs\ immediately after data
is read into memory. It then completes the calibration steps and writes up
the data, while the acquisition of the next command is already started.
This is normally OK because the computation time is less than the minimum
integration time on the correlator (5 seconds + 3 seconds of setup or
delays + 2-3 seconds to read the data) even in the worst cases.

An even faster automatic mode is available for cross-correlation loops,
using the {\tt SET SCAN Nscans}) command. In this mode, acquisition
continues even while RDI reads, calibrates and stores the previous scan.
The minimum integration time is 30 seconds in this case.

\section{CLIC}

\clic\ has been significantly modified to handle the correlator
architecture, and specially because of the introduction of the Time data.
The major modification indeed concerns the content of ``records'' in an
observation: ``records'' now contain only the Time data and associated
parameters. Only one set of spectral data is available per scan.  In
addition, a special record (the ``average record'') containing the average
over the whole scan of the time data is available.

\clic\ can then plot in two modes: Time or Spectral mode.

In a funny way, \clic\ and \cam\ are good examples of the uncertainty
principle: you can have finely sampled data in time {\bf or} in frequency,
but not both at the same time (though $\Delta \nu . \Delta t >> h$)!

\subsection{Time mode}

In Time mode, \clic\ plots by default the ``Continuum'' subbands C01 to C08
(each ``Continuum'' subband being a frequency-averaged data over part of
the spectral band). This can be plotted as a function of time, record by
record ({\tt SET AVER NONE}), or with any integration time shorter than the
scan duration ({\tt SET AVER Atime}), or by scan average ({\tt SET AVER
  SCAN}). In the latter case, \clic\ does not recompute the time-averaged
continuum data, but reads it directly from the ``average record'' in the
input file: this mode is thus specially fast.

Time mode is selected whenever the X coordinate is a time-like variable:
{\tt SET X TIME, SET X HOUR, SET X DEC, SET X RA, SET X SCAN}, etc\ldots
result in time mode display for \clic .

It is possible to plot spectral data (subbands L01 to L08) as a function of
time, but only by scan average of course, since no other information is
available. To do so, the {\tt SET SUBBAND} command must be typed after the
{\tt SET X} command.

\subsection{Spectral Mode}

In Spectral mode, \clic\ plots spectral data, the subbands L01 to L08, as a
function of a frequency variable. Only one spectrum per subband and per
scan is available.

Spectral mode is selected whenever the X coordinate is a frequency-like
variable: {\tt SET X I\_FREQ, SET X CHANNEL, SET X VELOCITY, SET X SKY\_F}
result in spectral mode display.

It is possible to plot Continuum data (C01 to C08) as a function of
frequency, but of course, each continuum subband is only ONE frequency
point.

\subsection{Other Commands}

A few data reduction commands have been modified because of the new
functionnality offered by the Time data. Major commands include

\begin{itemize}
\item{\tt SPECTRUM}\\
  {\tt SPECTRUM} has disappeared, because the \cam\ data is already
  pre-calibrated. There is no longer any raw data.
\item{\tt COMPRESS}\\
  {\tt COMPRESS} now only works on Time data, since the Spectral data is
  always scan averaged.
\item{\tt ATMOSPHERE}\\
  {\tt ATMOSPHERE} is a new command to recompute the atmospheric
  calibration.  This could formerly be done using the old {\tt SPECTRUM}
  command.
\item{\tt SOLVE FOCUS}\\
  The {\tt FOCUS} scans are now much like the {\tt POINTING} scans: 3 scans
  are produced instead of 1 previously. The {\tt SOLVE FOCUS} command
  accepts a {\tt /COMPRESS} option to change the integration time of the
  Time data.
\item{\tt SOLVE POINT}\\
  The basic integration step being now 1 second, the sensitivity may be
  insufficient in such a time to allow the derivation of antenna based
  quantities from baseline visibilities in a pointing. {\tt SOLVE POINT}
  command now accepts a {\tt /COMPRESS Time} option to allow adjustment of
  the integration time of each dump to the source intensity.  The default
  is {\tt /COMPRESS 4}, producing something similar to the old correlator.
\item{\tt SOLVE PSEUDO} \\
  Because Time data can correspond to narrow bands, a pseudo-continuum mode
  is available for total-power pointing on strong line sources (SiO masers
  usually). If one subband group has been selected using command SET SUB,
  the signal is the intensity measured by this subband group.  If two
  subband groups have bean selected (e.g. {\tt SET SUB C01 C02 TO C08} the
  signal from the second group is subtracted from that of the first group
  (which should thus contain the line emission) to produce true
  pseudo-continuum data.
\item{\tt SOLVE DELAY}\\
  The delay is no longer measured by scanning the delay line, but by
  measuring the phase slope as a function of frequency in the line
  subbands.  The {\tt SOLVE DELAY/PLOT} command will fit a straight line to
  the phase versus frequency (allowing for 2$\pi$ ambiguities).
\end{itemize}

\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

