namespace atm{
  
  // must be in the same order as for the enumeration
  string  AtmosphereType::type_[] = { "tropical",
				      "midlatSummer", 
				      "midlatWinter",
				      "subarcticSummer",  
				      "subarcticWinter" };
  
  
}
