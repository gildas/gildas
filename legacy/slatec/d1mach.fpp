*DECK D1MACH
      DOUBLE PRECISION FUNCTION D1MACH (I)
C***BEGIN PROLOGUE  D1MACH
C***PURPOSE  Return floating point machine dependent constants.
C***LIBRARY   SLATEC
C***CATEGORY  R1
C***TYPE      DOUBLE PRECISION (R1MACH-S, D1MACH-D)
C***KEYWORDS  MACHINE CONSTANTS
C***AUTHOR  Fox, P. A., (Bell Labs)
C           Hall, A. D., (Bell Labs)
C           Schryer, N. L., (Bell Labs)
C***DESCRIPTION
C
C   D1MACH can be used to obtain machine-dependent parameters for the
C   local machine environment.  It is a function subprogram with one
C   (input) argument, and can be referenced as follows:
C
C        D = D1MACH(I)
C
C   where I=1,...,5.  The (output) value of D above is determined by
C   the (input) value of I.  The results for various values of I are
C   discussed below.
C
C   D1MACH( 1) = B**(EMIN-1), the smallest positive magnitude.
C   D1MACH( 2) = B**EMAX*(1 - B**(-T)), the largest magnitude.
C   D1MACH( 3) = B**(-T), the smallest relative spacing.
C   D1MACH( 4) = B**(1-T), the largest relative spacing.
C   D1MACH( 5) = LOG10(B)
C
C   Assume double precision numbers are represented in the T-digit,
C   base-B form
C
C              sign (B**E)*( (X(1)/B) + ... + (X(T)/B**T) )
C
C   where 0 .LE. X(I) .LT. B for I=1,...,T, 0 .LT. X(1), and
C   EMIN .LE. E .LE. EMAX.
C
C   The values of B, T, EMIN and EMAX are provided in I1MACH as
C   follows:
C   I1MACH(10) = B, the base.
C   I1MACH(14) = T, the number of base-B digits.
C   I1MACH(15) = EMIN, the smallest exponent E.
C   I1MACH(16) = EMAX, the largest exponent E.
C
C   To alter this function for a particular environment, the desired
C   set of DATA statements should be activated by removing the C from
C   column 1.  Also, the values of D1MACH(1) - D1MACH(4) should be
C   checked for consistency with the local operating system.
C
C***REFERENCES  P. A. Fox, A. D. Hall and N. L. Schryer, Framework for
C                 a portable library, ACM Transactions on Mathematical
C                 Software 4, 2 (June 1978), pp. 177-188.
C***ROUTINES CALLED  XERMSG
C***REVISION HISTORY  (YYMMDD)
C   750101  DATE WRITTEN
C   890213  REVISION DATE from Version 3.2
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900315  CALLs to XERROR changed to CALLs to XERMSG.  (THJ)
C   900618  Added DEC RISC constants.  (WRB)
C   900723  Added IBM RS 6000 constants.  (WRB)
C   900911  Added SUN 386i constants.  (WRB)
C   910710  Added HP 730 constants.  (SMR)
C   911114  Added Convex IEEE constants.  (WRB)
C   920121  Added SUN -r8 compiler option constants.  (WRB)
C   920229  Added Touchstone Delta i860 constants.  (WRB)
C   920501  Reformatted the REFERENCES section.  (WRB)
C   920625  Added CONVEX -p8 and -pd8 compiler option constants.
C           (BKS, WRB)
C   930201  Added DEC Alpha and SGI constants.  (RWC and WRB)
C   071022  Remove obsolete machines (SG)
C***END PROLOGUE  D1MACH
C
      INTEGER I
      INTEGER SMALL(4)
      INTEGER LARGE(4)
      INTEGER RIGHT(4)
      INTEGER DIVER(4)
      INTEGER LOG10(4)
C
      DOUBLE PRECISION DMACH(5)
      SAVE DMACH
C
      LOGICAL FIRST
      SAVE FIRST
C
      DATA FIRST /.TRUE./
#if defined(VAX)
!
! These are the original values
C     MACHINE CONSTANTS FOR THE DEC VAX USING D_FLOATING
      DATA SMALL(1), SMALL(2) /        128,           0 /
      DATA LARGE(1), LARGE(2) /     -32769,          -1 /
      DATA RIGHT(1), RIGHT(2) /       9344,           0 /
      DATA DIVER(1), DIVER(2) /       9472,           0 /
      DATA LOG10(1), LOG10(2) /  546979738,  -805796613 /

C     MACHINE CONSTANTS FOR THE DEC VAX USING G_FLOATING
C
C      DATA SMALL(1), SMALL(2) /         16,           0 /
C      DATA LARGE(1), LARGE(2) /     -32769,          -1 /
C      DATA RIGHT(1), RIGHT(2) /      15552,           0 /
C      DATA DIVER(1), DIVER(2) /      15568,           0 /
C      DATA LOG10(1), LOG10(2) /  1142112243, 2046775455 /
#elif defined(IEEE) 
!
! Note the reversal. This produces "decent" numbers...
      DATA SMALL(2), SMALL(1) / Z'00100000', Z'00000000' /
      DATA LARGE(2), LARGE(1) / Z'7FEFFFFF', Z'FFFFFFFF' /
      DATA RIGHT(2), RIGHT(1) / Z'3CA00000', Z'00000000' /
      DATA DIVER(2), DIVER(1) / Z'3CB00000', Z'00000000' /
      DATA LOG10(2), LOG10(1) / Z'3FD34413', Z'509F79FF' /
#elif defined(EEEI)
!
! This has not been checked 
      DATA SMALL(1), SMALL(2) / Z'00100000', Z'00000000' /
      DATA LARGE(1), LARGE(2) / Z'7FEFFFFF', Z'FFFFFFFF' /
      DATA RIGHT(1), RIGHT(2) / Z'3CA00000', Z'00000000' /
      DATA DIVER(1), DIVER(2) / Z'3CB00000', Z'00000000' / 
      DATA LOG10(1), LOG10(2) / Z'3FD34413', Z'509F79FF' /
#endif
!
! This code is to avoid the problems with Equivalence, in particular
! with the Gfortran (GNU) compiler
      IF (FIRST) THEN
         CALL SCOPY  (2,SMALL,1,DMACH(1),1) 
         CALL SCOPY  (2,LARGE,1,DMACH(2),1) 
         CALL SCOPY  (2,RIGHT,1,DMACH(3),1) 
         CALL SCOPY  (2,DIVER,1,DMACH(4),1) 
         CALL SCOPY  (2,LOG10,1,DMACH(5),1) 
         FIRST = .FALSE.
      ENDIF
!
      IF (I .LT. 1 .OR. I .GT. 5) CALL XERMSG ('SLATEC', 'D1MACH',
     &   'I OUT OF BOUNDS', 1, 2)
!
      D1MACH = DMACH(I)
      END
