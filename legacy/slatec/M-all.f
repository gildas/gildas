*DECK MACON
      SUBROUTINE MACON
C***BEGIN PROLOGUE  MACON
C***SUBSIDIARY
C***PURPOSE  Subsidiary to BVSUP
C***LIBRARY   SLATEC
C***TYPE      SINGLE PRECISION (MACON-S, DMACON-D)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C    Sets up machine constants using R1MACH
C
C***SEE ALSO  BVSUP
C***ROUTINES CALLED  R1MACH
C***COMMON BLOCKS    ML5MCO
C***REVISION HISTORY  (YYMMDD)
C   750601  DATE WRITTEN
C   890531  Changed all specific intrinsics to generic.  (WRB)
C   890921  Realigned order of variables in certain COMMON blocks.
C           (WRB)
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900328  Added TYPE section.  (WRB)
C***END PROLOGUE  MACON
      COMMON /ML5MCO/ URO,SRU,EPS,SQOVFL,TWOU,FOURU,LPAR
C***FIRST EXECUTABLE STATEMENT  MACON
      URO=R1MACH(4)
      SRU=SQRT(URO)
      DD=-LOG10(URO)
      LPAR=0.5*DD
      KE=0.5+0.75*DD
      EPS=10.**(-2*KE)
      SQOVFL=SQRT(R1MACH(2))
      TWOU=2.0*URO
      FOURU=4.0*URO
      RETURN
      END
*DECK MC20AD
      SUBROUTINE MC20AD (NC, MAXA, A, INUM, JPTR, JNUM, JDISP)
C***BEGIN PROLOGUE  MC20AD
C***SUBSIDIARY
C***PURPOSE  Subsidiary to DSPLP
C***LIBRARY   SLATEC
C***TYPE      DOUBLE PRECISION (MC20AS-S, MC20AD-D)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C     THIS SUBPROGRAM IS A SLIGHT MODIFICATION OF A SUBPROGRAM
C     FROM THE C. 1979 AERE HARWELL LIBRARY.  THE NAME OF THE
C     CORRESPONDING HARWELL CODE CAN BE OBTAINED BY DELETING
C     THE FINAL LETTER =D= IN THE NAMES USED HERE.
C     REVISED SEP. 13, 1979.
C
C     ROYALTIES HAVE BEEN PAID TO AERE-UK FOR USE OF THEIR CODES
C     IN THE PACKAGE GIVEN HERE.  ANY PRIMARY USAGE OF THE HARWELL
C     SUBROUTINES REQUIRES A ROYALTY AGREEMENT AND PAYMENT BETWEEN
C     THE USER AND AERE-UK.  ANY USAGE OF THE SANDIA WRITTEN CODES
C     DSPLP( ) (WHICH USES THE HARWELL SUBROUTINES) IS PERMITTED.
C
C***SEE ALSO  DSPLP
C***ROUTINES CALLED  (NONE)
C***REVISION HISTORY  (YYMMDD)
C   811215  DATE WRITTEN
C   890831  Modified array declarations.  (WRB)
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900402  Added TYPE section.  (WRB)
C***END PROLOGUE  MC20AD
      INTEGER INUM(*), JNUM(*)
      DOUBLE PRECISION A(*),ACE,ACEP
      DIMENSION JPTR(NC)
C***FIRST EXECUTABLE STATEMENT  MC20AD
      NULL = -JDISP
C**      CLEAR JPTR
      DO 10 J=1,NC
         JPTR(J) = 0
   10 CONTINUE
C**      COUNT THE NUMBER OF ELEMENTS IN EACH COLUMN.
      DO 20 K=1,MAXA
         J = JNUM(K) + JDISP
         JPTR(J) = JPTR(J) + 1
   20 CONTINUE
C**      SET THE JPTR ARRAY
      K = 1
      DO 30 J=1,NC
         KR = K + JPTR(J)
         JPTR(J) = K
         K = KR
   30 CONTINUE
C
C**      REORDER THE ELEMENTS INTO COLUMN ORDER.  THE ALGORITHM IS AN
C        IN-PLACE SORT AND IS OF ORDER MAXA.
      DO 50 I=1,MAXA
C        ESTABLISH THE CURRENT ENTRY.
         JCE = JNUM(I) + JDISP
         IF (JCE.EQ.0) GO TO 50
         ACE = A(I)
         ICE = INUM(I)
C        CLEAR THE LOCATION VACATED.
         JNUM(I) = NULL
C        CHAIN FROM CURRENT ENTRY TO STORE ITEMS.
         DO 40 J=1,MAXA
C        CURRENT ENTRY NOT IN CORRECT POSITION.  DETERMINE CORRECT
C        POSITION TO STORE ENTRY.
            LOC = JPTR(JCE)
            JPTR(JCE) = JPTR(JCE) + 1
C        SAVE CONTENTS OF THAT LOCATION.
            ACEP = A(LOC)
            ICEP = INUM(LOC)
            JCEP = JNUM(LOC)
C        STORE CURRENT ENTRY.
            A(LOC) = ACE
            INUM(LOC) = ICE
            JNUM(LOC) = NULL
C        CHECK IF NEXT CURRENT ENTRY NEEDS TO BE PROCESSED.
            IF (JCEP.EQ.NULL) GO TO 50
C        IT DOES.  COPY INTO CURRENT ENTRY.
            ACE = ACEP
            ICE = ICEP
            JCE = JCEP + JDISP
   40    CONTINUE
C
   50 CONTINUE
C
C**      RESET JPTR VECTOR.
      JA = 1
      DO 60 J=1,NC
         JB = JPTR(J)
         JPTR(J) = JA
         JA = JB
   60 CONTINUE
      RETURN
      END
*DECK MC20AS
      SUBROUTINE MC20AS (NC, MAXA, A, INUM, JPTR, JNUM, JDISP)
C***BEGIN PROLOGUE  MC20AS
C***SUBSIDIARY
C***PURPOSE  Subsidiary to SPLP
C***LIBRARY   SLATEC
C***TYPE      SINGLE PRECISION (MC20AS-S, MC20AD-D)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C     THIS SUBPROGRAM IS A SLIGHT MODIFICATION OF A SUBPROGRAM
C     FROM THE C. 1979 AERE HARWELL LIBRARY.  THE NAME OF THE
C     CORRESPONDING HARWELL CODE CAN BE OBTAINED BY DELETING
C     THE FINAL LETTER =S= IN THE NAMES USED HERE.
C     REVISED SEP. 13, 1979.
C
C     ROYALTIES HAVE BEEN PAID TO AERE-UK FOR USE OF THEIR CODES
C     IN THE PACKAGE GIVEN HERE.  ANY PRIMARY USAGE OF THE HARWELL
C     SUBROUTINES REQUIRES A ROYALTY AGREEMENT AND PAYMENT BETWEEN
C     THE USER AND AERE-UK.  ANY USAGE OF THE SANDIA WRITTEN CODES
C     SPLP( ) (WHICH USES THE HARWELL SUBROUTINES) IS PERMITTED.
C
C***SEE ALSO  SPLP
C***ROUTINES CALLED  (NONE)
C***REVISION HISTORY  (YYMMDD)
C   811215  DATE WRITTEN
C   890831  Modified array declarations.  (WRB)
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900402  Added TYPE section.  (WRB)
C***END PROLOGUE  MC20AS
      INTEGER INUM(*), JNUM(*)
      REAL A(*)
      DIMENSION JPTR(NC)
C***FIRST EXECUTABLE STATEMENT  MC20AS
      NULL = -JDISP
C**      CLEAR JPTR
      DO 10 J=1,NC
         JPTR(J) = 0
   10 CONTINUE
C**      COUNT THE NUMBER OF ELEMENTS IN EACH COLUMN.
      DO 20 K=1,MAXA
         J = JNUM(K) + JDISP
         JPTR(J) = JPTR(J) + 1
   20 CONTINUE
C**      SET THE JPTR ARRAY
      K = 1
      DO 30 J=1,NC
         KR = K + JPTR(J)
         JPTR(J) = K
         K = KR
   30 CONTINUE
C
C**      REORDER THE ELEMENTS INTO COLUMN ORDER.  THE ALGORITHM IS AN
C        IN-PLACE SORT AND IS OF ORDER MAXA.
      DO 50 I=1,MAXA
C        ESTABLISH THE CURRENT ENTRY.
         JCE = JNUM(I) + JDISP
         IF (JCE.EQ.0) GO TO 50
         ACE = A(I)
         ICE = INUM(I)
C        CLEAR THE LOCATION VACATED.
         JNUM(I) = NULL
C        CHAIN FROM CURRENT ENTRY TO STORE ITEMS.
         DO 40 J=1,MAXA
C        CURRENT ENTRY NOT IN CORRECT POSITION.  DETERMINE CORRECT
C        POSITION TO STORE ENTRY.
            LOC = JPTR(JCE)
            JPTR(JCE) = JPTR(JCE) + 1
C        SAVE CONTENTS OF THAT LOCATION.
            ACEP = A(LOC)
            ICEP = INUM(LOC)
            JCEP = JNUM(LOC)
C        STORE CURRENT ENTRY.
            A(LOC) = ACE
            INUM(LOC) = ICE
            JNUM(LOC) = NULL
C        CHECK IF NEXT CURRENT ENTRY NEEDS TO BE PROCESSED.
            IF (JCEP.EQ.NULL) GO TO 50
C        IT DOES.  COPY INTO CURRENT ENTRY.
            ACE = ACEP
            ICE = ICEP
            JCE = JCEP + JDISP
   40    CONTINUE
C
   50 CONTINUE
C
C**      RESET JPTR VECTOR.
      JA = 1
      DO 60 J=1,NC
         JB = JPTR(J)
         JPTR(J) = JA
         JA = JB
   60 CONTINUE
      RETURN
      END
*DECK MGSBV
      SUBROUTINE MGSBV (M, N, A, IA, NIV, IFLAG, S, P, IP, INHOMO, V, W,
     +   WCND)
C***BEGIN PROLOGUE  MGSBV
C***SUBSIDIARY
C***PURPOSE  Subsidiary to BVSUP
C***LIBRARY   SLATEC
C***TYPE      SINGLE PRECISION (MGSBV-S, DMGSBV-D)
C***AUTHOR  Watts, H. A., (SNLA)
C***DESCRIPTION
C
C **********************************************************************
C Orthogonalize a set of N real vectors and determine their rank
C
C **********************************************************************
C INPUT
C **********************************************************************
C   M = Dimension of vectors
C   N = No. of vectors
C   A = Array whose first N cols contain the vectors
C   IA = First dimension of array A (col length)
C   NIV = Number of independent vectors needed
C   INHOMO = 1 Corresponds to having a non-zero particular solution
C   V = Particular solution vector (not included in the pivoting)
C   INDPVT = 1 Means pivoting will not be used
C
C **********************************************************************
C OUTPUT
C **********************************************************************
C   NIV = No. of linear independent vectors in input set
C     A = Matrix whose first NIV cols. contain NIV orthogonal vectors
C         which span the vector space determined by the input vectors
C   IFLAG
C          = 0 success
C          = 1 incorrect input
C          = 2 rank of new vectors less than N
C   P = Decomposition matrix.  P is upper triangular and
C             (old vectors) = (new vectors) * P.
C         The old vectors will be reordered due to pivoting
C         The dimension of p must be .GE. N*(N+1)/2.
C             (  N*(2*N+1) when N .NE. NFCC )
C   IP = Pivoting vector. The dimension of IP must be .GE. N.
C             (  2*N when N .NE. NFCC )
C   S = Square of norms of incoming vectors
C   V = Vector which is orthogonal to the vectors of A
C   W = Orthogonalization information for the vector V
C   WCND = Worst case (smallest) norm decrement value of the
C          vectors being orthogonalized  (represents a test
C          for linear dependence of the vectors)
C **********************************************************************
C
C***SEE ALSO  BVSUP
C***ROUTINES CALLED  PRVEC, SDOT
C***COMMON BLOCKS    ML18JR, ML5MCO
C***REVISION HISTORY  (YYMMDD)
C   750601  DATE WRITTEN
C   890531  Changed all specific intrinsics to generic.  (WRB)
C   890831  Modified array declarations.  (WRB)
C   890921  Realigned order of variables in certain COMMON blocks.
C           (WRB)
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900328  Added TYPE section.  (WRB)
C   910722  Updated AUTHOR section.  (ALS)
C***END PROLOGUE  MGSBV
C
      DIMENSION A(IA,*),V(*),W(*),P(*),IP(*),S(*)
C
C
      COMMON /ML18JR/ AE,RE,TOL,NXPTS,NIC,NOPG,MXNON,NDISK,NTAPE,NEQ,
     1                INDPVT,INTEG,NPS,NTP,NEQIVP,NUMORT,NFCC,
     2                ICOCO
C
      COMMON /ML5MCO/ URO,SRU,EPS,SQOVFL,TWOU,FOURU,LPAR
C
C***FIRST EXECUTABLE STATEMENT  MGSBV
      IF(M .GT. 0  .AND.  N .GT. 0  .AND.  IA .GE. M) GO TO 10
      IFLAG=1
      RETURN
C
   10 JP=0
      IFLAG=0
      NP1=N+1
      Y=0.0
      M2=M/2
C
C     CALCULATE SQUARE OF NORMS OF INCOMING VECTORS AND SEARCH FOR
C     VECTOR WITH LARGEST MAGNITUDE
C
      J=0
      DO 30 I=1,N
      VL=SDOT(M,A(1,I),1,A(1,I),1)
      S(I)=VL
      IF (N .EQ. NFCC) GO TO 25
      J=2*I-1
      P(J)=VL
      IP(J)=J
   25 J=J+1
      P(J)=VL
      IP(J)=J
      IF(VL .LE. Y) GO TO 30
      Y=VL
      IX=I
   30 CONTINUE
      IF (INDPVT .NE. 1) GO TO 33
      IX=1
      Y=P(1)
   33 LIX=IX
      IF (N .NE. NFCC) LIX=2*IX-1
      P(LIX)=P(1)
      S(NP1)=0.
      IF (INHOMO .EQ. 1) S(NP1)=SDOT(M,V,1,V,1)
      WCND=1.
      NIVN=NIV
      NIV=0
C
      IF(Y .EQ. 0.0) GO TO 170
C **********************************************************************
      DO 140 NR=1,N
      IF (NIVN .EQ. NIV) GO TO 150
      NIV=NR
      IF(IX .EQ. NR) GO TO 80
C
C     PIVOTING OF COLUMNS OF P MATRIX
C
      NN=N
      LIX=IX
      LR=NR
      IF (N .EQ. NFCC) GO TO 40
      NN=NFCC
      LIX=2*IX-1
      LR=2*NR-1
   40 IF(NR .EQ. 1) GO TO 60
      KD=LIX-LR
      KJ=LR
      NRM1=LR-1
      DO 50 J=1,NRM1
      PSAVE=P(KJ)
      JK=KJ+KD
      P(KJ)=P(JK)
      P(JK)=PSAVE
   50 KJ=KJ+NN-J
      JY=JK+NMNR
      JZ=JY-KD
      P(JY)=P(JZ)
   60 IZ=IP(LIX)
      IP(LIX)=IP(LR)
      IP(LR)=IZ
      SV=S(IX)
      S(IX)=S(NR)
      S(NR)=SV
      IF (N .EQ. NFCC) GO TO 69
      IF (NR .EQ. 1) GO TO 67
      KJ=LR+1
      DO 65 K=1,NRM1
      PSAVE=P(KJ)
      JK=KJ+KD
      P(KJ)=P(JK)
      P(JK)=PSAVE
   65 KJ=KJ+NFCC-K
   67 IZ=IP(LIX+1)
      IP(LIX+1)=IP(LR+1)
      IP(LR+1)=IZ
C
C     PIVOTING OF COLUMNS OF VECTORS
C
   69 DO 70 L=1,M
      T=A(L,IX)
      A(L,IX)=A(L,NR)
   70 A(L,NR)=T
C
C     CALCULATE P(NR,NR) AS NORM SQUARED OF PIVOTAL VECTOR
C
   80 JP=JP+1
      P(JP)=Y
      RY=1.0/Y
      NMNR=N-NR
      IF (N .EQ. NFCC) GO TO 85
      NMNR=NFCC-(2*NR-1)
      JP=JP+1
      P(JP)=0.
      KP=JP+NMNR
      P(KP)=Y
   85 IF(NR .EQ. N  .OR.  NIVN .EQ. NIV) GO TO 125
C
C    CALCULATE ORTHOGONAL PROJECTION VECTORS AND SEARCH FOR LARGEST NORM
C
      Y=0.0
      IP1=NR+1
      IX=IP1
C     ****************************************
      DO 120 J=IP1,N
      DOT=SDOT(M,A(1,NR),1,A(1,J),1)
      JP=JP+1
      JQ=JP+NMNR
      IF (N .NE. NFCC) JQ=JQ+NMNR-1
      P(JQ)=P(JP)-DOT*(DOT*RY)
      P(JP)=DOT*RY
      DO 90 I = 1,M
   90 A(I,J)=A(I,J)-P(JP)*A(I,NR)
      IF (N .EQ. NFCC) GO TO 99
      KP=JP+NMNR
      JP=JP+1
      PJP=RY*PRVEC(M,A(1,NR),A(1,J))
      P(JP)=PJP
      P(KP)=-PJP
      KP=KP+1
      P(KP)=RY*DOT
      DO 95 K=1,M2
      L=M2+K
      A(K,J)=A(K,J)-PJP*A(L,NR)
   95 A(L,J)=A(L,J)+PJP*A(K,NR)
      P(JQ)=P(JQ)-PJP*(PJP/RY)
C
C     TEST FOR CANCELLATION IN RECURRENCE RELATION
C
   99 IF(P(JQ) .GT. S(J)*SRU) GO TO 100
      P(JQ)=SDOT(M,A(1,J),1,A(1,J),1)
  100 IF(P(JQ) .LE. Y) GO TO 120
      Y=P(JQ)
      IX=J
  120 CONTINUE
      IF (N .NE. NFCC) JP=KP
C     ****************************************
      IF(INDPVT .EQ. 1) IX=IP1
C
C     RECOMPUTE NORM SQUARED OF PIVOTAL VECTOR WITH SCALAR PRODUCT
C
      Y=SDOT(M,A(1,IX),1,A(1,IX),1)
      IF(Y  .LE.  EPS*S(IX))  GO TO 170
      WCND=MIN(WCND,Y/S(IX))
C
C     COMPUTE ORTHOGONAL PROJECTION OF PARTICULAR SOLUTION
C
  125 IF(INHOMO .NE. 1) GO TO 140
      LR=NR
      IF (N .NE. NFCC) LR=2*NR-1
      W(LR)=SDOT(M,A(1,NR),1,V,1)*RY
      DO 130 I=1,M
  130 V(I)=V(I)-W(LR)*A(I,NR)
      IF (N .EQ. NFCC) GO TO 140
      LR=2*NR
      W(LR)=RY*PRVEC(M,V,A(1,NR))
      DO 135 K=1,M2
      L=M2+K
      V(K)=V(K)+W(LR)*A(L,NR)
  135 V(L)=V(L)-W(LR)*A(K,NR)
  140 CONTINUE
C **********************************************************************
C
C     TEST FOR LINEAR DEPENDENCE OF PARTICULAR SOLUTION
C
  150 IF(INHOMO .NE. 1) RETURN
      IF ((N .GT. 1) .AND. (S(NP1) .LT. 1.0)) RETURN
      VNORM=SDOT(M,V,1,V,1)
      IF (S(NP1) .NE. 0.) WCND=MIN(WCND,VNORM/S(NP1))
      IF(VNORM .GE. EPS*S(NP1)) RETURN
  170 IFLAG=2
      WCND=EPS
      RETURN
      END
*DECK MINFIT
      SUBROUTINE MINFIT (NM, M, N, A, W, IP, B, IERR, RV1)
C***BEGIN PROLOGUE  MINFIT
C***PURPOSE  Compute the singular value decomposition of a rectangular
C            matrix and solve the related linear least squares problem.
C***LIBRARY   SLATEC (EISPACK)
C***CATEGORY  D9
C***TYPE      SINGLE PRECISION (MINFIT-S)
C***KEYWORDS  EIGENVALUES, EIGENVECTORS, EISPACK
C***AUTHOR  Smith, B. T., et al.
C***DESCRIPTION
C
C     This subroutine is a translation of the ALGOL procedure MINFIT,
C     NUM. MATH. 14, 403-420(1970) by Golub and Reinsch.
C     HANDBOOK FOR AUTO. COMP., VOL II-LINEAR ALGEBRA, 134-151(1971).
C
C     This subroutine determines, towards the solution of the linear
C                                                        T
C     system AX=B, the singular value decomposition A=USV  of a real
C                                         T
C     M by N rectangular matrix, forming U B rather than U.  Householder
C     bidiagonalization and a variant of the QR algorithm are used.
C
C     On INPUT
C
C        NM must be set to the row dimension of the two-dimensional
C          array parameters, A and B, as declared in the calling
C          program dimension statement.  Note that NM must be at least
C          as large as the maximum of M and N.  NM is an INTEGER
C          variable.
C
C        M is the number of rows of A and B.  M is an INTEGER variable.
C
C        N is the number of columns of A and the order of V.  N is an
C          INTEGER variable.
C
C        A contains the rectangular coefficient matrix of the system.
C          A is a two-dimensional REAL array, dimensioned A(NM,N).
C
C        IP is the number of columns of B.  IP can be zero.
C
C        B contains the constant column matrix of the system if IP is
C          not zero.  Otherwise, B is not referenced.  B is a two-
C          dimensional REAL array, dimensioned B(NM,IP).
C
C     On OUTPUT
C
C        A has been overwritten by the matrix V (orthogonal) of the
C          decomposition in its first N rows and columns.  If an
C          error exit is made, the columns of V corresponding to
C          indices of correct singular values should be correct.
C
C        W contains the N (non-negative) singular values of A (the
C          diagonal elements of S).  They are unordered.  If an
C          error exit is made, the singular values should be correct
C          for indices IERR+1, IERR+2, ..., N.  W is a one-dimensional
C          REAL array, dimensioned W(N).
C
C                                   T
C        B has been overwritten by U B.  If an error exit is made,
C                       T
C          the rows of U B corresponding to indices of correct singular
C          values should be correct.
C
C        IERR is an INTEGER flag set to
C          Zero       for normal return,
C          K          if the K-th singular value has not been
C                     determined after 30 iterations.
C                     The singular values should be correct for
C                     indices IERR+1, IERR+2, ..., N.
C
C        RV1 is a one-dimensional REAL array used for temporary storage,
C          dimensioned RV1(N).
C
C     Calls PYTHAG(A,B) for sqrt(A**2 + B**2).
C
C     Questions and comments should be directed to B. S. Garbow,
C     APPLIED MATHEMATICS DIVISION, ARGONNE NATIONAL LABORATORY
C     ------------------------------------------------------------------
C
C***REFERENCES  B. T. Smith, J. M. Boyle, J. J. Dongarra, B. S. Garbow,
C                 Y. Ikebe, V. C. Klema and C. B. Moler, Matrix Eigen-
C                 system Routines - EISPACK Guide, Springer-Verlag,
C                 1976.
C***ROUTINES CALLED  PYTHAG
C***REVISION HISTORY  (YYMMDD)
C   760101  DATE WRITTEN
C   890531  Changed all specific intrinsics to generic.  (WRB)
C   890831  Modified array declarations.  (WRB)
C   890831  REVISION DATE from Version 3.2
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   920501  Reformatted the REFERENCES section.  (WRB)
C***END PROLOGUE  MINFIT
C
      INTEGER I,J,K,L,M,N,II,IP,I1,KK,K1,LL,L1,M1,NM,ITS,IERR
      REAL A(NM,*),W(*),B(NM,IP),RV1(*)
      REAL C,F,G,H,S,X,Y,Z,SCALE,S1
      REAL PYTHAG
C
C***FIRST EXECUTABLE STATEMENT  MINFIT
      IERR = 0
C     .......... HOUSEHOLDER REDUCTION TO BIDIAGONAL FORM ..........
      G = 0.0E0
      SCALE = 0.0E0
      S1 = 0.0E0
C
      DO 300 I = 1, N
         L = I + 1
         RV1(I) = SCALE * G
         G = 0.0E0
         S = 0.0E0
         SCALE = 0.0E0
         IF (I .GT. M) GO TO 210
C
         DO 120 K = I, M
  120    SCALE = SCALE + ABS(A(K,I))
C
         IF (SCALE .EQ. 0.0E0) GO TO 210
C
         DO 130 K = I, M
            A(K,I) = A(K,I) / SCALE
            S = S + A(K,I)**2
  130    CONTINUE
C
         F = A(I,I)
         G = -SIGN(SQRT(S),F)
         H = F * G - S
         A(I,I) = F - G
         IF (I .EQ. N) GO TO 160
C
         DO 150 J = L, N
            S = 0.0E0
C
            DO 140 K = I, M
  140       S = S + A(K,I) * A(K,J)
C
            F = S / H
C
            DO 150 K = I, M
               A(K,J) = A(K,J) + F * A(K,I)
  150    CONTINUE
C
  160    IF (IP .EQ. 0) GO TO 190
C
         DO 180 J = 1, IP
            S = 0.0E0
C
            DO 170 K = I, M
  170       S = S + A(K,I) * B(K,J)
C
            F = S / H
C
            DO 180 K = I, M
               B(K,J) = B(K,J) + F * A(K,I)
  180    CONTINUE
C
  190    DO 200 K = I, M
  200    A(K,I) = SCALE * A(K,I)
C
  210    W(I) = SCALE * G
         G = 0.0E0
         S = 0.0E0
         SCALE = 0.0E0
         IF (I .GT. M .OR. I .EQ. N) GO TO 290
C
         DO 220 K = L, N
  220    SCALE = SCALE + ABS(A(I,K))
C
         IF (SCALE .EQ. 0.0E0) GO TO 290
C
         DO 230 K = L, N
            A(I,K) = A(I,K) / SCALE
            S = S + A(I,K)**2
  230    CONTINUE
C
         F = A(I,L)
         G = -SIGN(SQRT(S),F)
         H = F * G - S
         A(I,L) = F - G
C
         DO 240 K = L, N
  240    RV1(K) = A(I,K) / H
C
         IF (I .EQ. M) GO TO 270
C
         DO 260 J = L, M
            S = 0.0E0
C
            DO 250 K = L, N
  250       S = S + A(J,K) * A(I,K)
C
            DO 260 K = L, N
               A(J,K) = A(J,K) + S * RV1(K)
  260    CONTINUE
C
  270    DO 280 K = L, N
  280    A(I,K) = SCALE * A(I,K)
C
  290    S1 = MAX(S1,ABS(W(I))+ABS(RV1(I)))
  300 CONTINUE
C     .......... ACCUMULATION OF RIGHT-HAND TRANSFORMATIONS.
C                FOR I=N STEP -1 UNTIL 1 DO -- ..........
      DO 400 II = 1, N
         I = N + 1 - II
         IF (I .EQ. N) GO TO 390
         IF (G .EQ. 0.0E0) GO TO 360
C
         DO 320 J = L, N
C     .......... DOUBLE DIVISION AVOIDS POSSIBLE UNDERFLOW ..........
  320    A(J,I) = (A(I,J) / A(I,L)) / G
C
         DO 350 J = L, N
            S = 0.0E0
C
            DO 340 K = L, N
  340       S = S + A(I,K) * A(K,J)
C
            DO 350 K = L, N
               A(K,J) = A(K,J) + S * A(K,I)
  350    CONTINUE
C
  360    DO 380 J = L, N
            A(I,J) = 0.0E0
            A(J,I) = 0.0E0
  380    CONTINUE
C
  390    A(I,I) = 1.0E0
         G = RV1(I)
         L = I
  400 CONTINUE
C
      IF (M .GE. N .OR. IP .EQ. 0) GO TO 510
      M1 = M + 1
C
      DO 500 I = M1, N
C
         DO 500 J = 1, IP
            B(I,J) = 0.0E0
  500 CONTINUE
C     .......... DIAGONALIZATION OF THE BIDIAGONAL FORM ..........
  510 CONTINUE
C     .......... FOR K=N STEP -1 UNTIL 1 DO -- ..........
      DO 700 KK = 1, N
         K1 = N - KK
         K = K1 + 1
         ITS = 0
C     .......... TEST FOR SPLITTING.
C                FOR L=K STEP -1 UNTIL 1 DO -- ..........
  520    DO 530 LL = 1, K
            L1 = K - LL
            L = L1 + 1
            IF (S1 + ABS(RV1(L)) .EQ. S1) GO TO 565
C     .......... RV1(1) IS ALWAYS ZERO, SO THERE IS NO EXIT
C                THROUGH THE BOTTOM OF THE LOOP ..........
            IF (S1 + ABS(W(L1)) .EQ. S1) GO TO 540
  530    CONTINUE
C     .......... CANCELLATION OF RV1(L) IF L GREATER THAN 1 ..........
  540    C = 0.0E0
         S = 1.0E0
C
         DO 560 I = L, K
            F = S * RV1(I)
            RV1(I) = C * RV1(I)
            IF (S1 + ABS(F) .EQ. S1) GO TO 565
            G = W(I)
            H = PYTHAG(F,G)
            W(I) = H
            C = G / H
            S = -F / H
            IF (IP .EQ. 0) GO TO 560
C
            DO 550 J = 1, IP
               Y = B(L1,J)
               Z = B(I,J)
               B(L1,J) = Y * C + Z * S
               B(I,J) = -Y * S + Z * C
  550       CONTINUE
C
  560    CONTINUE
C     .......... TEST FOR CONVERGENCE ..........
  565    Z = W(K)
         IF (L .EQ. K) GO TO 650
C     .......... SHIFT FROM BOTTOM 2 BY 2 MINOR ..........
         IF (ITS .EQ. 30) GO TO 1000
         ITS = ITS + 1
         X = W(L)
         Y = W(K1)
         G = RV1(K1)
         H = RV1(K)
         F = 0.5E0 * (((G + Z) / H) * ((G - Z) / Y) + Y / H - H / Y)
         G = PYTHAG(F,1.0E0)
         F = X - (Z / X) * Z + (H / X) * (Y / (F + SIGN(G,F)) - H)
C     .......... NEXT QR TRANSFORMATION ..........
         C = 1.0E0
         S = 1.0E0
C
         DO 600 I1 = L, K1
            I = I1 + 1
            G = RV1(I)
            Y = W(I)
            H = S * G
            G = C * G
            Z = PYTHAG(F,H)
            RV1(I1) = Z
            C = F / Z
            S = H / Z
            F = X * C + G * S
            G = -X * S + G * C
            H = Y * S
            Y = Y * C
C
            DO 570 J = 1, N
               X = A(J,I1)
               Z = A(J,I)
               A(J,I1) = X * C + Z * S
               A(J,I) = -X * S + Z * C
  570       CONTINUE
C
            Z = PYTHAG(F,H)
            W(I1) = Z
C     .......... ROTATION CAN BE ARBITRARY IF Z IS ZERO ..........
            IF (Z .EQ. 0.0E0) GO TO 580
            C = F / Z
            S = H / Z
  580       F = C * G + S * Y
            X = -S * G + C * Y
            IF (IP .EQ. 0) GO TO 600
C
            DO 590 J = 1, IP
               Y = B(I1,J)
               Z = B(I,J)
               B(I1,J) = Y * C + Z * S
               B(I,J) = -Y * S + Z * C
  590       CONTINUE
C
  600    CONTINUE
C
         RV1(L) = 0.0E0
         RV1(K) = F
         W(K) = X
         GO TO 520
C     .......... CONVERGENCE ..........
  650    IF (Z .GE. 0.0E0) GO TO 700
C     .......... W(K) IS MADE NON-NEGATIVE ..........
         W(K) = -Z
C
         DO 690 J = 1, N
  690    A(J,K) = -A(J,K)
C
  700 CONTINUE
C
      GO TO 1001
C     .......... SET ERROR -- NO CONVERGENCE TO A
C                SINGULAR VALUE AFTER 30 ITERATIONS ..........
 1000 IERR = K
 1001 RETURN
      END
*DECK MINSO4
      SUBROUTINE MINSO4 (USOL, IDMN, ZN, ZM, PERTB)
C***BEGIN PROLOGUE  MINSO4
C***SUBSIDIARY
C***PURPOSE  Subsidiary to SEPX4
C***LIBRARY   SLATEC
C***TYPE      SINGLE PRECISION (MINSO4-S)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C     This subroutine orthogonalizes the array USOL with respect to
C     the constant array in a weighted least squares norm.
C
C     Entry at MINSO4 occurs when the final solution is
C     to be minimized with respect to the weighted
C     least squares norm.
C
C***SEE ALSO  SEPX4
C***ROUTINES CALLED  (NONE)
C***COMMON BLOCKS    SPL4
C***REVISION HISTORY  (YYMMDD)
C   801001  DATE WRITTEN
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900402  Added TYPE section.  (WRB)
C***END PROLOGUE  MINSO4
C
      COMMON /SPL4/   KSWX       ,KSWY       ,K          ,L          ,
     1                AIT        ,BIT        ,CIT        ,DIT        ,
     2                MIT        ,NIT        ,IS         ,MS         ,
     3                JS         ,NS         ,DLX        ,DLY        ,
     4                TDLX3      ,TDLY3      ,DLX4       ,DLY4
      DIMENSION       USOL(IDMN,*)           ,ZN(*)      ,ZM(*)
C***FIRST EXECUTABLE STATEMENT  MINSO4
      ISTR = 1
      IFNL = K
      JSTR = 1
      JFNL = L
C
C     COMPUTE WEIGHTED INNER PRODUCTS
C
      UTE = 0.0
      ETE = 0.0
      DO  20 I=IS,MS
         II = I-IS+1
         DO  10 J=JS,NS
            JJ = J-JS+1
            ETE = ETE+ZM(II)*ZN(JJ)
            UTE = UTE+USOL(I,J)*ZM(II)*ZN(JJ)
   10    CONTINUE
   20 CONTINUE
C
C     SET PERTURBATION PARAMETER
C
      PERTRB = UTE/ETE
C
C     SUBTRACT OFF CONSTANT PERTRB
C
      DO  40 I=ISTR,IFNL
         DO  30 J=JSTR,JFNL
            USOL(I,J) = USOL(I,J)-PERTRB
   30    CONTINUE
   40 CONTINUE
      RETURN
      END
*DECK MINSOL
      SUBROUTINE MINSOL (USOL, IDMN, ZN, ZM, PERTB)
C***BEGIN PROLOGUE  MINSOL
C***SUBSIDIARY
C***PURPOSE  Subsidiary to SEPELI
C***LIBRARY   SLATEC
C***TYPE      SINGLE PRECISION (MINSOL-S)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C     This subroutine orthogonalizes the array USOL with respect to
C     the constant array in a weighted least squares norm.
C
C     Entry at MINSOL occurs when the final solution is
C     to be minimized with respect to the weighted
C     least squares norm.
C
C***SEE ALSO  SEPELI
C***ROUTINES CALLED  (NONE)
C***COMMON BLOCKS    SPLPCM
C***REVISION HISTORY  (YYMMDD)
C   801001  DATE WRITTEN
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900402  Added TYPE section.  (WRB)
C***END PROLOGUE  MINSOL
C
      COMMON /SPLPCM/ KSWX       ,KSWY       ,K          ,L          ,
     1                AIT        ,BIT        ,CIT        ,DIT        ,
     2                MIT        ,NIT        ,IS         ,MS         ,
     3                JS         ,NS         ,DLX        ,DLY        ,
     4                TDLX3      ,TDLY3      ,DLX4       ,DLY4
      DIMENSION       USOL(IDMN,*)           ,ZN(*)      ,ZM(*)
C***FIRST EXECUTABLE STATEMENT  MINSOL
      ISTR = 1
      IFNL = K
      JSTR = 1
      JFNL = L
C
C     COMPUTE WEIGHTED INNER PRODUCTS
C
      UTE = 0.0
      ETE = 0.0
      DO  20 I=IS,MS
         II = I-IS+1
         DO  10 J=JS,NS
            JJ = J-JS+1
            ETE = ETE+ZM(II)*ZN(JJ)
            UTE = UTE+USOL(I,J)*ZM(II)*ZN(JJ)
   10    CONTINUE
   20 CONTINUE
C
C     SET PERTURBATION PARAMETER
C
      PERTRB = UTE/ETE
C
C     SUBTRACT OFF CONSTANT PERTRB
C
      DO  40 I=ISTR,IFNL
         DO  30 J=JSTR,JFNL
            USOL(I,J) = USOL(I,J)-PERTRB
   30    CONTINUE
   40 CONTINUE
      RETURN
      END
*DECK MPADD
      SUBROUTINE MPADD (X, Y, Z)
C***BEGIN PROLOGUE  MPADD
C***SUBSIDIARY
C***PURPOSE  Subsidiary to DQDOTA and DQDOTI
C***LIBRARY   SLATEC
C***TYPE      ALL (MPADD-A)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C Adds X and Y, forming result in Z, where X, Y and Z are 'mp'
C  (multiple precision) numbers.  Four guard digits are used,
C  and then R*-rounding.
C
C***SEE ALSO  DQDOTA, DQDOTI
C***ROUTINES CALLED  MPADD2
C***REVISION HISTORY  (YYMMDD)
C   791001  DATE WRITTEN
C   890831  Modified array declarations.  (WRB)
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900402  Added TYPE section.  (WRB)
C***END PROLOGUE  MPADD
      INTEGER X(*), Y(*), Z(*)
C***FIRST EXECUTABLE STATEMENT  MPADD
      CALL MPADD2 (X, Y, Z, Y, 0)
      RETURN
      END
*DECK MPADD2
      SUBROUTINE MPADD2 (X, Y, Z, Y1, TRUNC)
C***BEGIN PROLOGUE  MPADD2
C***SUBSIDIARY
C***PURPOSE  Subsidiary to DQDOTA and DQDOTI
C***LIBRARY   SLATEC
C***TYPE      ALL (MPADD2-A)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C  Called by MPADD, MPSUB etc.
C  X, Y and Z are MP numbers, Y1 and TRUNC are integers.
C  To force call by reference rather than value/result, Y1 is
C  declared as an array, but only Y1(1) is ever used.
C  Sets Z = X + Y1(1)*ABS(Y), where Y1(1) = +- Y(1).
C  If TRUNC .EQ. 0, R*-rounding is used;  otherwise, truncation.
C  R*-rounding is defined in the Kuki and Cody reference.
C
C  The arguments X(*), Y(*), and Z(*) are all INTEGER arrays of size
C  30.  See the comments in the routine MPBLAS for the reason for this
C  choice.
C
C***SEE ALSO  DQDOTA, DQDOTI, MPBLAS
C***REFERENCES  H. Kuki and W. J. Cody, A statistical study of floating
C                 point number systems, Communications of the ACM 16, 4
C                 (April 1973), pp. 223-230.
C               R. P. Brent, On the precision attainable with various
C                 floating-point number systems, IEEE Transactions on
C                 Computers C-22, 6 (June 1973), pp. 601-607.
C               R. P. Brent, A Fortran multiple-precision arithmetic
C                 package, ACM Transactions on Mathematical Software 4,
C                 1 (March 1978), pp. 57-70.
C               R. P. Brent, MP, a Fortran multiple-precision arithmetic
C                 package, Algorithm 524, ACM Transactions on Mathema-
C                 tical Software 4, 1 (March 1978), pp. 71-81.
C***ROUTINES CALLED  MPADD3, MPCHK, MPERR, MPNZR, MPSTR
C***COMMON BLOCKS    MPCOM
C***REVISION HISTORY  (YYMMDD)
C   791001  DATE WRITTEN
C   ??????  Modified for use with BLAS.  Blank COMMON changed to named
C           COMMON.  R given dimension 12.
C   890531  Changed all specific intrinsics to generic.  (WRB)
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900402  Added TYPE section.  (WRB)
C   920528  Added a REFERENCES section revised.  (WRB)
C   930124  Increased Array size in MPCON for SUN -r8.  (RWC)
C***END PROLOGUE  MPADD2
      COMMON /MPCOM/ B, T, M, LUN, MXR, R(30)
      INTEGER B, T, R, X(*), Y(*), Z(*), Y1(*), TRUNC
      INTEGER S, ED, RS, RE
C***FIRST EXECUTABLE STATEMENT  MPADD2
      IF (X(1).NE.0) GO TO 20
   10 CALL MPSTR(Y, Z)
      Z(1) = Y1(1)
      RETURN
   20 IF (Y1(1).NE.0) GO TO 40
   30 CALL MPSTR (X, Z)
      RETURN
C COMPARE SIGNS
   40 S = X(1)*Y1(1)
      IF (ABS(S).LE.1) GO TO 60
      CALL MPCHK (1, 4)
      WRITE (LUN, 50)
   50 FORMAT (' *** SIGN NOT 0, +1 OR -1 IN CALL TO MPADD2,',
     1        ' POSSIBLE OVERWRITING PROBLEM ***')
      CALL MPERR
      Z(1) = 0
      RETURN
C COMPARE EXPONENTS
   60 ED = X(2) - Y(2)
      MED = ABS(ED)
      IF (ED) 90, 70, 120
C EXPONENTS EQUAL SO COMPARE SIGNS, THEN FRACTIONS IF NEC.
   70 IF (S.GT.0) GO TO 100
      DO 80 J = 1, T
      IF (X(J+2) - Y(J+2)) 100, 80, 130
   80 CONTINUE
C RESULT IS ZERO
      Z(1) = 0
      RETURN
C HERE EXPONENT(Y) .GE. EXPONENT(X)
   90 IF (MED.GT.T) GO TO 10
  100 RS = Y1(1)
      RE = Y(2)
      CALL MPADD3 (X, Y, S, MED, RE)
C NORMALIZE, ROUND OR TRUNCATE, AND RETURN
  110 CALL MPNZR (RS, RE, Z, TRUNC)
      RETURN
C ABS(X) .GT. ABS(Y)
  120 IF (MED.GT.T) GO TO 30
  130 RS = X(1)
      RE = X(2)
      CALL MPADD3 (Y, X, S, MED, RE)
      GO TO 110
      END
*DECK MPADD3
      SUBROUTINE MPADD3 (X, Y, S, MED, RE)
C***BEGIN PROLOGUE  MPADD3
C***SUBSIDIARY
C***PURPOSE  Subsidiary to DQDOTA and DQDOTI
C***LIBRARY   SLATEC
C***TYPE      ALL (MPADD3-A)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C   Called by MPADD2; does inner loops of addition
C
C   The arguments X(*) and Y(*) and the variable R in COMMON are all
C   INTEGER arrays of size 30.  See the comments in the routine MPBLAS
C   for the reason for this choice.
C
C***SEE ALSO  DQDOTA, DQDOTI, MPBLAS
C***ROUTINES CALLED  (NONE)
C***COMMON BLOCKS    MPCOM
C***REVISION HISTORY  (YYMMDD)
C   791001  DATE WRITTEN
C   ??????  Modified for use with BLAS.  Blank COMMON changed to named
C           COMMON.  R given dimension 12.
C   890831  Modified array declarations.  (WRB)
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900402  Added TYPE section.  (WRB)
C   930124  Increased Array size in MPCON for SUN -r8.  (RWC)
C***END PROLOGUE  MPADD3
      COMMON /MPCOM/ B, T, M, LUN, MXR, R(30)
      INTEGER B, T, R, X(*), Y(*), S, RE, C, TED
C***FIRST EXECUTABLE STATEMENT  MPADD3
      TED = T + MED
      I2 = T + 4
      I = I2
      C = 0
C CLEAR GUARD DIGITS TO RIGHT OF X DIGITS
   10 IF (I.LE.TED) GO TO 20
      R(I) = 0
      I = I - 1
      GO TO 10
   20 IF (S.LT.0) GO TO 130
C HERE DO ADDITION, EXPONENT(Y) .GE. EXPONENT(X)
      IF (I.LT.T) GO TO 40
   30 J = I - MED
      R(I) = X(J+2)
      I = I - 1
      IF (I.GT.T) GO TO 30
   40 IF (I.LE.MED) GO TO 60
      J = I - MED
      C = Y(I+2) + X(J+2) + C
      IF (C.LT.B) GO TO 50
C CARRY GENERATED HERE
      R(I) = C - B
      C = 1
      I = I - 1
      GO TO 40
C NO CARRY GENERATED HERE
   50 R(I) = C
      C = 0
      I = I - 1
      GO TO 40
   60 IF (I.LE.0) GO TO 90
      C = Y(I+2) + C
      IF (C.LT.B) GO TO 70
      R(I) = 0
      C = 1
      I = I - 1
      GO TO 60
   70 R(I) = C
      I = I - 1
C NO CARRY POSSIBLE HERE
   80 IF (I.LE.0) RETURN
      R(I) = Y(I+2)
      I = I - 1
      GO TO 80
   90 IF (C.EQ.0) RETURN
C MUST SHIFT RIGHT HERE AS CARRY OFF END
      I2P = I2 + 1
      DO 100 J = 2, I2
      I = I2P - J
  100 R(I+1) = R(I)
      R(1) = 1
      RE = RE + 1
      RETURN
C HERE DO SUBTRACTION, ABS(Y) .GT. ABS(X)
  110 J = I - MED
      R(I) = C - X(J+2)
      C = 0
      IF (R(I).GE.0) GO TO 120
C BORROW GENERATED HERE
      C = -1
      R(I) = R(I) + B
  120 I = I - 1
  130 IF (I.GT.T) GO TO 110
  140 IF (I.LE.MED) GO TO 160
      J = I - MED
      C = Y(I+2) + C - X(J+2)
      IF (C.GE.0) GO TO 150
C BORROW GENERATED HERE
      R(I) = C + B
      C = -1
      I = I - 1
      GO TO 140
C NO BORROW GENERATED HERE
  150 R(I) = C
      C = 0
      I = I - 1
      GO TO 140
  160 IF (I.LE.0) RETURN
      C = Y(I+2) + C
      IF (C.GE.0) GO TO 70
      R(I) = C + B
      C = -1
      I = I - 1
      GO TO 160
      END
*DECK MPBLAS
      SUBROUTINE MPBLAS (I1)
C***BEGIN PROLOGUE  MPBLAS
C***SUBSIDIARY
C***PURPOSE  Subsidiary to DQDOTA and DQDOTI
C***LIBRARY   SLATEC
C***TYPE      ALL (MPBLAS-A)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C     This subroutine is called to set up Brent's 'mp' package
C     for use by the extended precision inner products from the BLAS.
C
C     In the SLATEC library we require the Extended Precision MP number
C     to have a mantissa twice as long as Double Precision numbers.
C     The calculation of MPT (and MPMXR which is the actual array size)
C     in this routine will give 2x (or slightly more) on the machine
C     that we are running on.  The INTEGER array size of 30 was chosen
C     to be slightly longer than the longest INTEGER array needed on
C     any machine that we are currently aware of.
C
C***SEE ALSO  DQDOTA, DQDOTI
C***REFERENCES  R. P. Brent, A Fortran multiple-precision arithmetic
C                 package, ACM Transactions on Mathematical Software 4,
C                 1 (March 1978), pp. 57-70.
C               R. P. Brent, MP, a Fortran multiple-precision arithmetic
C                 package, Algorithm 524, ACM Transactions on Mathema-
C                 tical Software 4, 1 (March 1978), pp. 71-81.
C***ROUTINES CALLED  I1MACH, XERMSG
C***COMMON BLOCKS    MPCOM
C***REVISION HISTORY  (YYMMDD)
C   791001  DATE WRITTEN
C   890531  Changed all specific intrinsics to generic.  (WRB)
C   890531  REVISION DATE from Version 3.2
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900402  Added TYPE section.  (WRB)
C   920501  Reformatted the REFERENCES section.  (WRB)
C   930124  Increased Array size in MPCON for SUN -r8, and calculate
C               size for Quad Precision for 2x DP.  (RWC)
C***END PROLOGUE  MPBLAS
      COMMON /MPCOM/ MPB, MPT, MPM, MPLUN, MPMXR, MPR(30)
C***FIRST EXECUTABLE STATEMENT  MPBLAS
      I1 = 1
C
C     For full extended precision accuracy, MPB should be as large as
C     possible, subject to the restrictions in Brent's paper.
C
C     Statements below are for an integer wordlength of  48, 36, 32,
C     24, 18, and 16.  Pick one, or generate a new one.
C       48     MPB = 4194304
C       36     MPB =   65536
C       32     MPB =   16384
C       24     MPB =    1024
C       18     MPB =     128
C       16     MPB =      64
C
      MPBEXP = I1MACH(8)/2-2
      MPB = 2**MPBEXP
C
C     Set up remaining parameters
C                  UNIT FOR ERROR MESSAGES
      MPLUN = I1MACH(4)
C                  NUMBER OF MP DIGITS
      MPT = (2*I1MACH(14)+MPBEXP-1)/MPBEXP
C                  DIMENSION OF R
      MPMXR = MPT+4
C
      if (MPMXR.GT.30) THEN
         CALL XERMSG('SLATEC', 'MPBLAS',
     *      'Array space not sufficient for Quad Precision 2x ' //
     *      'Double Precision, Proceeding.', 1, 1)
         MPT = 26
         MPMXR = 30
      ENDIF
C                  EXPONENT RANGE
      MPM = MIN(32767,I1MACH(9)/4-1)
      RETURN
      END
*DECK MPCDM
      SUBROUTINE MPCDM (DX, Z)
C***BEGIN PROLOGUE  MPCDM
C***SUBSIDIARY
C***PURPOSE  Subsidiary to DQDOTA and DQDOTI
C***LIBRARY   SLATEC
C***TYPE      ALL (MPCDM-A)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C Converts double-precision number DX to multiple-precision Z.
C Some numbers will not convert exactly on machines with base
C other than two, four or sixteen. This routine is not called
C by any other routine in 'mp', so may be omitted if double-
C precision is not available.
C
C The argument Z(*) and the variable R in COMMON are both INTEGER
C arrays of size 30.  See the comments in the routine MPBLAS for the
C for the reason for this choice.
C
C***SEE ALSO  DQDOTA, DQDOTI, MPBLAS
C***ROUTINES CALLED  MPCHK, MPDIVI, MPMULI, MPNZR
C***COMMON BLOCKS    MPCOM
C***REVISION HISTORY  (YYMMDD)
C   791001  DATE WRITTEN
C   ??????  Modified for use with BLAS.  Blank COMMON changed to named
C           COMMON.  R given dimension 12.
C   890531  Changed all specific intrinsics to generic.  (WRB)
C   890831  Modified array declarations.  (WRB)
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900402  Added TYPE section.  (WRB)
C   930124  Increased Array size in MPCON for SUN -r8.  (RWC)
C***END PROLOGUE  MPCDM
      DOUBLE PRECISION DB, DJ, DX
      COMMON /MPCOM/ B, T, M, LUN, MXR, R(30)
      INTEGER B, T, R, Z(*), RS, RE, TP
C***FIRST EXECUTABLE STATEMENT  MPCDM
      CALL MPCHK (1, 4)
      I2 = T + 4
C CHECK SIGN
      IF (DX) 20, 10, 30
C IF DX = 0D0 RETURN 0
   10 Z(1) = 0
      RETURN
C DX .LT. 0D0
   20 RS = -1
      DJ = -DX
      GO TO 40
C DX .GT. 0D0
   30 RS = 1
      DJ = DX
   40 IE = 0
   50 IF (DJ.LT.1D0) GO TO 60
C INCREASE IE AND DIVIDE DJ BY 16.
      IE = IE + 1
      DJ = 0.0625D0*DJ
      GO TO 50
   60 IF (DJ.GE.0.0625D0) GO TO 70
      IE = IE - 1
      DJ = 16D0*DJ
      GO TO 60
C NOW DJ IS DY DIVIDED BY SUITABLE POWER OF 16
C SET EXPONENT TO 0
   70 RE = 0
      DB = DBLE(B)
C CONVERSION LOOP (ASSUME DOUBLE-PRECISION OPS. EXACT)
      DO 80 I = 1, I2
      DJ = DB*DJ
      R(I) = INT(DJ)
   80 DJ = DJ - DBLE(R(I))
C NORMALIZE RESULT
      CALL MPNZR (RS, RE, Z, 0)
      IB = MAX(7*B*B, 32767)/16
      TP = 1
C NOW MULTIPLY BY 16**IE
      IF (IE) 90, 130, 110
   90 K = -IE
      DO 100 I = 1, K
      TP = 16*TP
      IF ((TP.LE.IB).AND.(TP.NE.B).AND.(I.LT.K)) GO TO 100
      CALL MPDIVI (Z, TP, Z)
      TP = 1
  100 CONTINUE
      RETURN
  110 DO 120 I = 1, IE
      TP = 16*TP
      IF ((TP.LE.IB).AND.(TP.NE.B).AND.(I.LT.IE)) GO TO 120
      CALL MPMULI (Z, TP, Z)
      TP = 1
  120 CONTINUE
  130 RETURN
      END
*DECK MPCHK
      SUBROUTINE MPCHK (I, J)
C***BEGIN PROLOGUE  MPCHK
C***SUBSIDIARY
C***PURPOSE  Subsidiary to DQDOTA and DQDOTI
C***LIBRARY   SLATEC
C***TYPE      ALL (MPCHK-A)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C  Checks legality of B, T, M, MXR and LUN which should be set
C  in COMMON. The condition on MXR (the dimension of the EP arrays)
C  is that  MXR .GE. (I*T + J)
C
C***SEE ALSO  DQDOTA, DQDOTI, MPBLAS
C***ROUTINES CALLED  I1MACH, MPERR
C***COMMON BLOCKS    MPCOM
C***REVISION HISTORY  (YYMMDD)
C   791001  DATE WRITTEN
C   ??????  Modified for use with BLAS.  Blank COMMON changed to named
C           COMMON.  R given dimension 12.
C   891009  Removed unreferenced statement label.  (WRB)
C   891009  REVISION DATE from Version 3.2
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900402  Added TYPE section.  (WRB)
C   930124  Increased Array size in MPCON for SUN -r8.  (RWC)
C***END PROLOGUE  MPCHK
      COMMON /MPCOM/ B, T, M, LUN, MXR, R(30)
      INTEGER B, T, R
C***FIRST EXECUTABLE STATEMENT  MPCHK
      LUN = I1MACH(4)
C NOW CHECK LEGALITY OF B, T AND M
      IF (B.GT.1) GO TO 40
      WRITE (LUN, 30) B
   30 FORMAT (' *** B =', I10, ' ILLEGAL IN CALL TO MPCHK,'/
     1 ' PERHAPS NOT SET BEFORE CALL TO AN MP ROUTINE ***')
      CALL MPERR
   40 IF (T.GT.1) GO TO 60
      WRITE (LUN, 50) T
   50 FORMAT (' *** T =', I10, ' ILLEGAL IN CALL TO MPCHK,'/
     1 ' PERHAPS NOT SET BEFORE CALL TO AN MP ROUTINE ***')
      CALL MPERR
   60 IF (M.GT.T) GO TO 80
      WRITE (LUN, 70)
   70 FORMAT (' *** M .LE. T IN CALL TO MPCHK,'/
     1 ' PERHAPS NOT SET BEFORE CALL TO AN MP ROUTINE ***')
      CALL MPERR
C 8*B*B-1 SHOULD BE REPRESENTABLE, IF NOT WILL OVERFLOW
C AND MAY BECOME NEGATIVE, SO CHECK FOR THIS
   80 IB = 4*B*B - 1
      IF ((IB.GT.0).AND.((2*IB+1).GT.0)) GO TO 100
      WRITE (LUN, 90)
   90 FORMAT (' *** B TOO LARGE IN CALL TO MPCHK ***')
      CALL MPERR
C CHECK THAT SPACE IN COMMON IS SUFFICIENT
  100 MX = I*T + J
      IF (MXR.GE.MX) RETURN
C HERE COMMON IS TOO SMALL, SO GIVE ERROR MESSAGE.
      WRITE (LUN, 110) I, J, MX, MXR, T
  110 FORMAT (' *** MXR TOO SMALL OR NOT SET TO DIM(R) BEFORE CALL',
     1 ' TO AN MP ROUTINE *** ' /
     2 ' *** MXR SHOULD BE AT LEAST', I3, '*T +', I4, ' =', I6, '  ***'
     3 / ' *** ACTUALLY MXR =', I10, ', AND T =', I10, '  ***')
      CALL MPERR
      RETURN
      END
*DECK MPCMD
      SUBROUTINE MPCMD (X, DZ)
C***BEGIN PROLOGUE  MPCMD
C***SUBSIDIARY
C***PURPOSE  Subsidiary to DQDOTA and DQDOTI
C***LIBRARY   SLATEC
C***TYPE      ALL (MPCMD-A)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C  Converts multiple-precision X to double-precision DZ. Assumes
C  X is in allowable range for double-precision numbers. There is
C  some loss of accuracy if the exponent is large.
C
C  The argument X(*) is INTEGER array of size 30.  See the comments in
C  the routine MPBLAS for the reason for this choice.
C
C***SEE ALSO  DQDOTA, DQDOTI, MPBLAS
C***ROUTINES CALLED  MPCHK, MPERR
C***COMMON BLOCKS    MPCOM
C***REVISION HISTORY  (YYMMDD)
C   791001  DATE WRITTEN
C   ??????  Modified for use with BLAS.  Blank COMMON changed to named
C           COMMON.  R given dimension 12.
C   890531  Changed all specific intrinsics to generic.  (WRB)
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900402  Added TYPE section.  (WRB)
C   930124  Increased Array size in MPCON for SUN -r8.  (RWC)
C***END PROLOGUE  MPCMD
      DOUBLE PRECISION DB, DZ, DZ2
      COMMON /MPCOM/ B, T, M, LUN, MXR, R(30)
      INTEGER B, T, R, X(*), TM
C***FIRST EXECUTABLE STATEMENT  MPCMD
      CALL MPCHK (1, 4)
      DZ = 0D0
      IF (X(1).EQ.0) RETURN
      DB = DBLE(B)
      DO 10 I = 1, T
      DZ = DB*DZ + DBLE(X(I+2))
      TM = I
C CHECK IF FULL DOUBLE-PRECISION ACCURACY ATTAINED
      DZ2 = DZ + 1D0
C TEST BELOW NOT ALWAYS EQUIVALENT TO - IF (DZ2.LE.DZ) GO TO 20,
C FOR EXAMPLE ON CYBER 76.
      IF ((DZ2-DZ).LE.0D0) GO TO 20
   10 CONTINUE
C NOW ALLOW FOR EXPONENT
   20 DZ = DZ*(DB**(X(2)-TM))
C CHECK REASONABLENESS OF RESULT.
      IF (DZ.LE.0D0) GO TO 30
C LHS SHOULD BE .LE. 0.5 BUT ALLOW FOR SOME ERROR IN LOG
      IF (ABS(DBLE(X(2))-(LOG(DZ)/
     1    LOG(DBLE(B))+0.5D0)).GT.0.6D0) GO TO 30
      IF (X(1).LT.0) DZ = -DZ
      RETURN
C FOLLOWING MESSAGE INDICATES THAT X IS TOO LARGE OR SMALL -
C TRY USING MPCMDE INSTEAD.
   30 WRITE (LUN, 40)
   40 FORMAT (' *** FLOATING-POINT OVER/UNDER-FLOW IN MPCMD ***')
      CALL MPERR
      RETURN
      END
*DECK MPDIVI
      SUBROUTINE MPDIVI (X, IY, Z)
C***BEGIN PROLOGUE  MPDIVI
C***SUBSIDIARY
C***PURPOSE  Subsidiary to DQDOTA and DQDOTI
C***LIBRARY   SLATEC
C***TYPE      ALL (MPDIVI-A)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C  Divides 'mp' X by the single-precision integer IY giving 'mp' Z.
C  This is much faster than division by an 'mp' number.
C
C  The arguments X(*) and Z(*), and the variable R in COMMON are all
C  INTEGER arrays of size 30.  See the comments in the routine MPBLAS
C  for the reason for this choice.
C
C***SEE ALSO  DQDOTA, DQDOTI, MPBLAS
C***ROUTINES CALLED  MPCHK, MPERR, MPNZR, MPSTR, MPUNFL
C***COMMON BLOCKS    MPCOM
C***REVISION HISTORY  (YYMMDD)
C   791001  DATE WRITTEN
C   ??????  Modified for use with BLAS.  Blank COMMON changed to named
C           COMMON.  R given dimension 12.
C   890531  Changed all specific intrinsics to generic.  (WRB)
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900402  Added TYPE section.  (WRB)
C   930124  Increased Array size in MPCON for SUN -r8.  (RWC)
C***END PROLOGUE  MPDIVI
      COMMON /MPCOM/ B, T, M, LUN, MXR, R(30)
      INTEGER B, T, R, X(*), Z(*), RS, RE, R1, C, C2, B2
C***FIRST EXECUTABLE STATEMENT  MPDIVI
      RS = X(1)
      J = IY
      IF (J) 30, 10, 40
   10 WRITE (LUN, 20)
   20 FORMAT (' *** ATTEMPTED DIVISION BY ZERO IN CALL TO MPDIVI ***')
      GO TO 230
   30 J = -J
      RS = -RS
   40 RE = X(2)
C CHECK FOR ZERO DIVIDEND
      IF (RS.EQ.0) GO TO 120
C CHECK FOR DIVISION BY B
      IF (J.NE.B) GO TO 50
      CALL MPSTR (X, Z)
      IF (RE.LE.(-M)) GO TO 240
      Z(1) = RS
      Z(2) = RE - 1
      RETURN
C CHECK FOR DIVISION BY 1 OR -1
   50 IF (J.NE.1) GO TO 60
      CALL MPSTR (X, Z)
      Z(1) = RS
      RETURN
   60 C = 0
      I2 = T + 4
      I = 0
C IF J*B NOT REPRESENTABLE AS AN INTEGER HAVE TO SIMULATE
C LONG DIVISION.   ASSUME AT LEAST 16-BIT WORD.
      B2 = MAX(8*B,32767/B)
      IF (J.GE.B2) GO TO 130
C LOOK FOR FIRST NONZERO DIGIT IN QUOTIENT
   70 I = I + 1
      C = B*C
      IF (I.LE.T) C = C + X(I+2)
      R1 = C/J
      IF (R1) 210, 70, 80
C ADJUST EXPONENT AND GET T+4 DIGITS IN QUOTIENT
   80 RE = RE + 1 - I
      R(1) = R1
      C = B*(C - J*R1)
      KH = 2
      IF (I.GE.T) GO TO 100
      KH = 1 + T - I
      DO 90 K = 2, KH
      I = I + 1
      C = C + X(I+2)
      R(K) = C/J
   90 C = B*(C - J*R(K))
      IF (C.LT.0) GO TO 210
      KH = KH + 1
  100 DO 110 K = KH, I2
      R(K) = C/J
  110 C = B*(C - J*R(K))
      IF (C.LT.0) GO TO 210
C NORMALIZE AND ROUND RESULT
  120 CALL MPNZR (RS, RE, Z, 0)
      RETURN
C HERE NEED SIMULATED DOUBLE-PRECISION DIVISION
  130 C2 = 0
      J1 = J/B
      J2 = J - J1*B
      J11 = J1 + 1
C LOOK FOR FIRST NONZERO DIGIT
  140 I = I + 1
      C = B*C + C2
      C2 = 0
      IF (I.LE.T) C2 = X(I+2)
      IF (C-J1) 140, 150, 160
  150 IF (C2.LT.J2) GO TO 140
C COMPUTE T+4 QUOTIENT DIGITS
  160 RE = RE + 1 - I
      K = 1
      GO TO 180
C MAIN LOOP FOR LARGE ABS(IY) CASE
  170 K = K + 1
      IF (K.GT.I2) GO TO 120
      I = I + 1
C GET APPROXIMATE QUOTIENT FIRST
  180 IR = C/J11
C NOW REDUCE SO OVERFLOW DOES NOT OCCUR
      IQ = C - IR*J1
      IF (IQ.LT.B2) GO TO 190
C HERE IQ*B WOULD POSSIBLY OVERFLOW SO INCREASE IR
      IR = IR + 1
      IQ = IQ - J1
  190 IQ = IQ*B - IR*J2
      IF (IQ.GE.0) GO TO 200
C HERE IQ NEGATIVE SO IR WAS TOO LARGE
      IR = IR - 1
      IQ = IQ + J
  200 IF (I.LE.T) IQ = IQ + X(I+2)
      IQJ = IQ/J
C R(K) = QUOTIENT, C = REMAINDER
      R(K) = IQJ + IR
      C = IQ - J*IQJ
      IF (C.GE.0) GO TO 170
C CARRY NEGATIVE SO OVERFLOW MUST HAVE OCCURRED
  210 CALL MPCHK (1, 4)
      WRITE (LUN, 220)
  220 FORMAT (' *** INTEGER OVERFLOW IN MPDIVI, B TOO LARGE ***')
  230 CALL MPERR
      Z(1) = 0
      RETURN
C UNDERFLOW HERE
  240 CALL MPUNFL(Z)
      RETURN
      END
*DECK MPERR
      SUBROUTINE MPERR
C***BEGIN PROLOGUE  MPERR
C***SUBSIDIARY
C***PURPOSE  Subsidiary to DQDOTA and DQDOTI
C***LIBRARY   SLATEC
C***TYPE      ALL (MPERR-A)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C  This routine is called when a fatal error condition is
C  encountered, and after a message has been written on
C  logical unit LUN.
C
C***SEE ALSO  DQDOTA, DQDOTI, MPBLAS
C***ROUTINES CALLED  (NONE)
C***COMMON BLOCKS    MPCOM
C***REVISION HISTORY  (YYMMDD)
C   791001  DATE WRITTEN
C   ??????  Modified for use with BLAS.  Blank COMMON changed to named
C           COMMON.  R given dimension 12.
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900402  Added TYPE section.  (WRB)
C   930124  Increased Array size in MPCON for SUN -r8.  (RWC)
C***END PROLOGUE  MPERR
      COMMON /MPCOM/ B, T, M, LUN, MXR, R(30)
      INTEGER B, T, R
C***FIRST EXECUTABLE STATEMENT  MPERR
      CALL XERMSG('SLATEC', 'MPERR',
     1   ' *** EXECUTION TERMINATED BY CALL TO MPERR' //
     2   ' IN MP VERSION 770217 ***', 1, 2)
C
C AT PRESENT JUST STOP, BUT COULD DUMP B, T, ETC. HERE.
C ACTION COULD EASILY BE CONTROLLED BY A FLAG IN LABELLED COMMON.
C ANSI VERSION USES STOP, UNIVAC 1108 VERSION USES
C RETURN 0 IN ORDER TO GIVE A TRACE-BACK.
C FOR DEBUGGING PURPOSES IT MAY BE USEFUL SIMPLY TO
C RETURN HERE.  MOST MP ROUTINES RETURN WITH RESULT
C ZERO AFTER CALLING MPERR.
      STOP
      END
*DECK MPMAXR
      SUBROUTINE MPMAXR (X)
C***BEGIN PROLOGUE  MPMAXR
C***SUBSIDIARY
C***PURPOSE  Subsidiary to DQDOTA and DQDOTI
C***LIBRARY   SLATEC
C***TYPE      ALL (MPMAXR-A)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C  Sets X to the largest possible positive 'mp' number.
C
C  The argument X(*) is an INTEGER arrays of size 30.  See the comments
C  in the routine MPBLAS for the reason for this choice.
C
C***SEE ALSO  DQDOTA, DQDOTI, MPBLAS
C***ROUTINES CALLED  MPCHK
C***COMMON BLOCKS    MPCOM
C***REVISION HISTORY  (YYMMDD)
C   791001  DATE WRITTEN
C   ??????  Modified for use with BLAS.  Blank COMMON changed to named
C           COMMON.  R given dimension 12.
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900402  Added TYPE section.  (WRB)
C   930124  Increased Array size in MPCON for SUN -r8.  (RWC)
C***END PROLOGUE  MPMAXR
      COMMON /MPCOM/ B, T, M, LUN, MXR, R(30)
      INTEGER B, T, R, X(*)
C***FIRST EXECUTABLE STATEMENT  MPMAXR
      CALL MPCHK (1, 4)
      IT = B - 1
C SET FRACTION DIGITS TO B-1
      DO 10 I = 1, T
   10 X(I+2) = IT
C SET SIGN AND EXPONENT
      X(1) = 1
      X(2) = M
      RETURN
      END
*DECK MPMLP
      SUBROUTINE MPMLP (U, V, W, J)
C***BEGIN PROLOGUE  MPMLP
C***SUBSIDIARY
C***PURPOSE  Subsidiary to DQDOTA and DQDOTI
C***LIBRARY   SLATEC
C***TYPE      ALL (MPMLP-A)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C Performs inner multiplication loop for MPMUL. Carries are not pro-
C pagated in inner loop, which saves time at the expense of space.
C
C***SEE ALSO  DQDOTA, DQDOTI
C***ROUTINES CALLED  (NONE)
C***REVISION HISTORY  (YYMMDD)
C   791001  DATE WRITTEN
C   890831  Modified array declarations.  (WRB)
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900402  Added TYPE section.  (WRB)
C***END PROLOGUE  MPMLP
      INTEGER U(*), V(*), W
C***FIRST EXECUTABLE STATEMENT  MPMLP
      DO 10 I = 1, J
   10 U(I) = U(I) + W*V(I)
      RETURN
      END
*DECK MPMUL
      SUBROUTINE MPMUL (X, Y, Z)
C***BEGIN PROLOGUE  MPMUL
C***SUBSIDIARY
C***PURPOSE  Subsidiary to DQDOTA and DQDOTI
C***LIBRARY   SLATEC
C***TYPE      ALL (MPMUL-A)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C  Multiplies X and Y, returning result in Z, for 'mp' X, Y and Z.
C  The simple o(t**2) algorithm is used, with four guard digits and
C  R*-rounding. Advantage is taken of zero digits in X, but not in Y.
C  Asymptotically faster algorithms are known (see Knuth, VOL. 2),
C  but are difficult to implement in FORTRAN in an efficient and
C  machine-independent manner. In comments to other 'mp' routines,
C  M(t) is the time to perform t-digit 'mp' multiplication. Thus
C  M(t) = o(t**2) with the present version of MPMUL, but
C  M(t) = o(t.log(t).log(log(t))) is theoretically possible.
C
C  The arguments X(*), Y(*), and Z(*), and the variable R in COMMON are
C  all INTEGER arrays of size 30.  See the comments in the routine
C  MPBLAS for the reason for this choice.
C
C***SEE ALSO  DQDOTA, DQDOTI, MPBLAS
C***ROUTINES CALLED  MPCHK, MPERR, MPMLP, MPNZR
C***COMMON BLOCKS    MPCOM
C***REVISION HISTORY  (YYMMDD)
C   791001  DATE WRITTEN
C   ??????  Modified for use with BLAS.  Blank COMMON changed to named
C           COMMON.  R given dimension 12.
C   890531  Changed all specific intrinsics to generic.  (WRB)
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900402  Added TYPE section.  (WRB)
C   930124  Increased Array size in MPCON for SUN -r8.  (RWC)
C***END PROLOGUE  MPMUL
      COMMON /MPCOM/ B, T, M, LUN, MXR, R(30)
      INTEGER B, T, R, X(*), Y(*), Z(*), RS, RE, XI, C, RI
C***FIRST EXECUTABLE STATEMENT  MPMUL
      CALL MPCHK (1, 4)
      I2 = T + 4
      I2P = I2 + 1
C FORM SIGN OF PRODUCT
      RS = X(1)*Y(1)
      IF (RS.NE.0) GO TO 10
C SET RESULT TO ZERO
      Z(1) = 0
      RETURN
C FORM EXPONENT OF PRODUCT
   10 RE = X(2) + Y(2)
C CLEAR ACCUMULATOR
      DO 20 I = 1, I2
   20 R(I) = 0
C PERFORM MULTIPLICATION
      C = 8
      DO 40 I = 1, T
      XI = X(I+2)
C FOR SPEED, PUT THE NUMBER WITH MANY ZEROS FIRST
      IF (XI.EQ.0) GO TO 40
      CALL MPMLP (R(I+1), Y(3), XI, MIN (T, I2 - I))
      C = C - 1
      IF (C.GT.0) GO TO 40
C CHECK FOR LEGAL BASE B DIGIT
      IF ((XI.LT.0).OR.(XI.GE.B)) GO TO 90
C PROPAGATE CARRIES AT END AND EVERY EIGHTH TIME,
C FASTER THAN DOING IT EVERY TIME.
      DO 30 J = 1, I2
      J1 = I2P - J
      RI = R(J1) + C
      IF (RI.LT.0) GO TO 70
      C = RI/B
   30 R(J1) = RI - B*C
      IF (C.NE.0) GO TO 90
      C = 8
   40 CONTINUE
      IF (C.EQ.8) GO TO 60
      IF ((XI.LT.0).OR.(XI.GE.B)) GO TO 90
      C = 0
      DO 50 J = 1, I2
      J1 = I2P - J
      RI = R(J1) + C
      IF (RI.LT.0) GO TO 70
      C = RI/B
   50 R(J1) = RI - B*C
      IF (C.NE.0) GO TO 90
C NORMALIZE AND ROUND RESULT
   60 CALL MPNZR (RS, RE, Z, 0)
      RETURN
   70 WRITE (LUN, 80)
   80 FORMAT (' *** INTEGER OVERFLOW IN MPMUL, B TOO LARGE ***')
      GO TO 110
   90 WRITE (LUN, 100)
  100 FORMAT (' *** ILLEGAL BASE B DIGIT IN CALL TO MPMUL,',
     1        ' POSSIBLE OVERWRITING PROBLEM ***')
  110 CALL MPERR
      Z(1) = 0
      RETURN
      END
*DECK MPMUL2
      SUBROUTINE MPMUL2 (X, IY, Z, TRUNC)
C***BEGIN PROLOGUE  MPMUL2
C***SUBSIDIARY
C***PURPOSE  Subsidiary to DQDOTA and DQDOTI
C***LIBRARY   SLATEC
C***TYPE      ALL (MPMUL2-A)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C  Multiplies 'mp' X by single-precision integer IY giving 'mp' Z.
C  Multiplication by 1 may be used to normalize a number even if some
C  digits are greater than B-1. Result is rounded if TRUNC.EQ.0,
C  otherwise truncated.
C
C  The arguments X(*) and Z(*), and the variable R in COMMON are all
C  INTEGER arrays of size 30.  See the comments in the routine MPBLAS
C  for the reason for this choice.
C
C***SEE ALSO  DQDOTA, DQDOTI, MPBLAS
C***ROUTINES CALLED  MPCHK, MPERR, MPNZR, MPOVFL, MPSTR
C***COMMON BLOCKS    MPCOM
C***REVISION HISTORY  (YYMMDD)
C   791001  DATE WRITTEN
C   ??????  Modified for use with BLAS.  Blank COMMON changed to named
C           COMMON.  R given dimension 12.
C   890531  Changed all specific intrinsics to generic.  (WRB)
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900402  Added TYPE section.  (WRB)
C   930124  Increased Array size in MPCON for SUN -r8.  (RWC)
C***END PROLOGUE  MPMUL2
      COMMON /MPCOM/ B, T, M, LUN, MXR, R(30)
      INTEGER B, T, R, X(*), Z(*), TRUNC, RE, RS
      INTEGER C, C1, C2, RI, T1, T3, T4
C***FIRST EXECUTABLE STATEMENT  MPMUL2
      RS = X(1)
      IF (RS.EQ.0) GO TO 10
      J = IY
      IF (J) 20, 10, 50
C RESULT ZERO
   10 Z(1) = 0
      RETURN
   20 J = -J
      RS = -RS
C CHECK FOR MULTIPLICATION BY B
      IF (J.NE.B) GO TO 50
      IF (X(2).LT.M) GO TO 40
      CALL MPCHK (1, 4)
      WRITE (LUN, 30)
   30 FORMAT (' *** OVERFLOW OCCURRED IN MPMUL2 ***')
      CALL MPOVFL (Z)
      RETURN
   40 CALL MPSTR (X, Z)
      Z(1) = RS
      Z(2) = X(2) + 1
      RETURN
C SET EXPONENT TO EXPONENT(X) + 4
   50 RE = X(2) + 4
C FORM PRODUCT IN ACCUMULATOR
      C = 0
      T1 = T + 1
      T3 = T + 3
      T4 = T + 4
C IF J*B NOT REPRESENTABLE AS AN INTEGER WE HAVE TO SIMULATE
C DOUBLE-PRECISION MULTIPLICATION.
      IF (J.GE.MAX(8*B, 32767/B)) GO TO 110
      DO 60 IJ = 1, T
      I = T1 - IJ
      RI = J*X(I+2) + C
      C = RI/B
   60 R(I+4) = RI - B*C
C CHECK FOR INTEGER OVERFLOW
      IF (RI.LT.0) GO TO 130
C HAVE TO TREAT FIRST FOUR WORDS OF R SEPARATELY
      DO 70 IJ = 1, 4
      I = 5 - IJ
      RI = C
      C = RI/B
   70 R(I) = RI - B*C
      IF (C.EQ.0) GO TO 100
C HAVE TO SHIFT RIGHT HERE AS CARRY OFF END
   80 DO 90 IJ = 1, T3
      I = T4 - IJ
   90 R(I+1) = R(I)
      RI = C
      C = RI/B
      R(1) = RI - B*C
      RE = RE + 1
      IF (C) 130, 100, 80
C NORMALIZE AND ROUND OR TRUNCATE RESULT
  100 CALL MPNZR (RS, RE, Z, TRUNC)
      RETURN
C HERE J IS TOO LARGE FOR SINGLE-PRECISION MULTIPLICATION
  110 J1 = J/B
      J2 = J - J1*B
C FORM PRODUCT
      DO 120 IJ = 1, T4
      C1 = C/B
      C2 = C - B*C1
      I = T1 - IJ
      IX = 0
      IF (I.GT.0) IX = X(I+2)
      RI = J2*IX + C2
      IS = RI/B
      C = J1*IX + C1 + IS
  120 R(I+4) = RI - B*IS
      IF (C) 130, 100, 80
C CAN ONLY GET HERE IF INTEGER OVERFLOW OCCURRED
  130 CALL MPCHK (1, 4)
      WRITE (LUN, 140)
  140 FORMAT (' *** INTEGER OVERFLOW IN MPMUL2, B TOO LARGE ***')
      CALL MPERR
      GO TO 10
      END
*DECK MPMULI
      SUBROUTINE MPMULI (X, IY, Z)
C***BEGIN PROLOGUE  MPMULI
C***SUBSIDIARY
C***PURPOSE  Subsidiary to DQDOTA and DQDOTI
C***LIBRARY   SLATEC
C***TYPE      ALL (MPMULI-A)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C Multiplies 'mp' X by single-precision integer IY giving 'mp' Z.
C This is faster than using MPMUL.  Result is ROUNDED.
C Multiplication by 1 may be used to normalize a number
C even if the last digit is B.
C
C***SEE ALSO  DQDOTA, DQDOTI
C***ROUTINES CALLED  MPMUL2
C***REVISION HISTORY  (YYMMDD)
C   791001  DATE WRITTEN
C   890831  Modified array declarations.  (WRB)
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900402  Added TYPE section.  (WRB)
C***END PROLOGUE  MPMULI
      INTEGER X(*), Z(*)
C***FIRST EXECUTABLE STATEMENT  MPMULI
      CALL MPMUL2 (X, IY, Z, 0)
      RETURN
      END
*DECK MPNZR
      SUBROUTINE MPNZR (RS, RE, Z, TRUNC)
C***BEGIN PROLOGUE  MPNZR
C***SUBSIDIARY
C***PURPOSE  Subsidiary to DQDOTA and DQDOTI
C***LIBRARY   SLATEC
C***TYPE      ALL (MPNZR-A)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C  Modified for use with BLAS.  Blank COMMON changed to named COMMON.
C  Assumes long (i.e. (t+4)-DIGIT) fraction in R, sign = RS, exponent
C  = RE.  Normalizes, and returns 'mp' result in Z. Integer arguments
C  RS and RE are not preserved. R*-rounding is used if TRUNC.EQ.0
C
C  The argument Z(*) and the variable R in COMMON are INTEGER arrays
C  of size 30.  See the comments in the routine MPBLAS for the reason
C  for this choice.
C
C***SEE ALSO  DQDOTA, DQDOTI, MPBLAS
C***ROUTINES CALLED  MPERR, MPOVFL, MPUNFL
C***COMMON BLOCKS    MPCOM
C***REVISION HISTORY  (YYMMDD)
C   791001  DATE WRITTEN
C   890531  Changed all specific intrinsics to generic.  (WRB)
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900402  Added TYPE section.  (WRB)
C   930124  Increased Array size in MPCON for SUN -r8.  (RWC)
C***END PROLOGUE  MPNZR
      COMMON /MPCOM/ B, T, M, LUN, MXR, R(30)
      INTEGER B, T, R, Z(*), RE, RS, TRUNC, B2
C***FIRST EXECUTABLE STATEMENT  MPNZR
      I2 = T + 4
      IF (RS.NE.0) GO TO 20
C STORE ZERO IN Z
   10 Z(1) = 0
      RETURN
C CHECK THAT SIGN = +-1
   20 IF (ABS(RS).LE.1) GO TO 40
      WRITE (LUN, 30)
   30 FORMAT (' *** SIGN NOT 0, +1 OR -1 IN CALL TO MPNZR,',
     1        ' POSSIBLE OVERWRITING PROBLEM ***')
      CALL MPERR
      GO TO 10
C LOOK FOR FIRST NONZERO DIGIT
   40 DO 50 I = 1, I2
      IS = I - 1
      IF (R(I).GT.0) GO TO 60
   50 CONTINUE
C FRACTION ZERO
      GO TO 10
   60 IF (IS.EQ.0) GO TO 90
C NORMALIZE
      RE = RE - IS
      I2M = I2 - IS
      DO 70 J = 1, I2M
      K = J + IS
   70 R(J) = R(K)
      I2P = I2M + 1
      DO 80 J = I2P, I2
   80 R(J) = 0
C CHECK TO SEE IF TRUNCATION IS DESIRED
   90 IF (TRUNC.NE.0) GO TO 150
C SEE IF ROUNDING NECESSARY
C TREAT EVEN AND ODD BASES DIFFERENTLY
      B2 = B/2
      IF ((2*B2).NE.B) GO TO 130
C B EVEN.  ROUND IF R(T+1).GE.B2 UNLESS R(T) ODD AND ALL ZEROS
C AFTER R(T+2).
      IF (R(T+1) - B2) 150, 100, 110
  100 IF (MOD(R(T),2).EQ.0) GO TO 110
      IF ((R(T+2)+R(T+3)+R(T+4)).EQ.0) GO TO 150
C ROUND
  110 DO 120 J = 1, T
      I = T + 1 - J
      R(I) = R(I) + 1
      IF (R(I).LT.B) GO TO 150
  120 R(I) = 0
C EXCEPTIONAL CASE, ROUNDED UP TO .10000...
      RE = RE + 1
      R(1) = 1
      GO TO 150
C ODD BASE, ROUND IF R(T+1)... .GT. 1/2
  130 DO 140 I = 1, 4
      IT = T + I
      IF (R(IT) - B2) 150, 140, 110
  140 CONTINUE
C CHECK FOR OVERFLOW
  150 IF (RE.LE.M) GO TO 170
      WRITE (LUN, 160)
  160 FORMAT (' *** OVERFLOW OCCURRED IN MPNZR ***')
      CALL MPOVFL (Z)
      RETURN
C CHECK FOR UNDERFLOW
  170 IF (RE.LT.(-M)) GO TO 190
C STORE RESULT IN Z
      Z(1) = RS
      Z(2) = RE
      DO 180 I = 1, T
  180 Z(I+2) = R(I)
      RETURN
C UNDERFLOW HERE
  190 CALL MPUNFL (Z)
      RETURN
      END
*DECK MPOVFL
      SUBROUTINE MPOVFL (X)
C***BEGIN PROLOGUE  MPOVFL
C***SUBSIDIARY
C***PURPOSE  Subsidiary to DQDOTA and DQDOTI
C***LIBRARY   SLATEC
C***TYPE      ALL (MPOVFL-A)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C  Called on multiple-precision overflow, i.e. when the
C  exponent of 'mp' number X would exceed M.  At present execution is
C  terminated with an error message after calling MPMAXR(X), but it
C  would be possible to return, possibly updating a counter and
C  terminating execution after a preset number of overflows.  Action
C  could easily be determined by a flag in labelled common.
C
C  The argument X(*) is an INTEGER array of size 30.  See the comments
C  in the routine MPBLAS for the reason for this choice.
C
C***SEE ALSO  DQDOTA, DQDOTI, MPBLAS
C***ROUTINES CALLED  MPCHK, MPERR, MPMAXR
C***COMMON BLOCKS    MPCOM
C***REVISION HISTORY  (YYMMDD)
C   791001  DATE WRITTEN
C   ??????  Modified for use with BLAS.  Blank COMMON changed to named
C           COMMON.  R given dimension 12.
C   890831  Modified array declarations.  (WRB)
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900402  Added TYPE section.  (WRB)
C   930124  Increased Array size in MPCON for SUN -r8.  (RWC)
C***END PROLOGUE  MPOVFL
      COMMON /MPCOM/ B, T, M, LUN, MXR, R(30)
      INTEGER B, T, R, X(*)
C***FIRST EXECUTABLE STATEMENT  MPOVFL
      CALL MPCHK (1, 4)
C SET X TO LARGEST POSSIBLE POSITIVE NUMBER
      CALL MPMAXR (X)
      WRITE (LUN, 10)
   10 FORMAT (' *** CALL TO MPOVFL, MP OVERFLOW OCCURRED ***')
C TERMINATE EXECUTION BY CALLING MPERR
      CALL MPERR
      RETURN
      END
*DECK MPSTR
      SUBROUTINE MPSTR (X, Y)
C***BEGIN PROLOGUE  MPSTR
C***SUBSIDIARY
C***PURPOSE  Subsidiary to DQDOTA and DQDOTI
C***LIBRARY   SLATEC
C***TYPE      ALL (MPSTR-A)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C  Sets Y = X for 'mp' X and Y.
C
C  The arguments X(*) and Y(*) are INTEGER arrays of size 30.  See the
C  comments in the routine MPBLAS for the reason for this choice.
C
C***SEE ALSO  DQDOTA, DQDOTI, MPBLAS
C***ROUTINES CALLED  (NONE)
C***COMMON BLOCKS    MPCOM
C***REVISION HISTORY  (YYMMDD)
C   791001  DATE WRITTEN
C   ??????  Modified for use with BLAS.  Blank COMMON changed to named
C           COMMON.  R given dimension 12.
C   890206  REVISION DATE from Version 3.2
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900402  Added TYPE section.  (WRB)
C   930124  Increased Array size in MPCON for SUN -r8.  (RWC)
C***END PROLOGUE  MPSTR
      COMMON /MPCOM/ B, T, M, LUN, MXR, R(30)
      INTEGER B, T, R, X(*), Y(*)
C***FIRST EXECUTABLE STATEMENT  MPSTR
      DO 10 I = 1, T+2
         Y(I) = X(I)
   10 CONTINUE
      RETURN
      END
*DECK MPUNFL
      SUBROUTINE MPUNFL (X)
C***BEGIN PROLOGUE  MPUNFL
C***SUBSIDIARY
C***PURPOSE  Subsidiary to DQDOTA and DQDOTI
C***LIBRARY   SLATEC
C***TYPE      ALL (MPUNFL-A)
C***AUTHOR  (UNKNOWN)
C***DESCRIPTION
C
C Called on multiple-precision underflow, i.e.  when the
C exponent of 'mp' number X would be less than -M.
C
C***SEE ALSO  DQDOTA, DQDOTI
C***ROUTINES CALLED  MPCHK
C***REVISION HISTORY  (YYMMDD)
C   791001  DATE WRITTEN
C   890831  Modified array declarations.  (WRB)
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900402  Added TYPE section.  (WRB)
C***END PROLOGUE  MPUNFL
      INTEGER X(*)
C***FIRST EXECUTABLE STATEMENT  MPUNFL
      CALL MPCHK (1, 4)
C THE UNDERFLOWING NUMBER IS SET TO ZERO
C AN ALTERNATIVE WOULD BE TO CALL MPMINR (X) AND RETURN,
C POSSIBLY UPDATING A COUNTER AND TERMINATING EXECUTION
C AFTER A PRESET NUMBER OF UNDERFLOWS.  ACTION COULD EASILY
C BE DETERMINED BY A FLAG IN LABELLED COMMON.
      X(1) = 0
      RETURN
      END
