.ec _
.ll 76
.ad b
.in 4
.ti -4
1 FILE
.ti +4
[POINT_\]FILE Name Type

     Specify the input file name and type of  data  (OPTICAL,  RADIO  or
INTERFEROMETRY).   The file extension is derived according to Type using
the following defaults :  .OPT, .RAD and .INT  respectively.   The  file
header are read, but not the data.

.ti -4
1 IGNORE
.ti +4
[POINT_\]IGNORE [/SCAN List of scans] [/SOURCE List of sources]
[/TIME TimeMin TimeMax]

     Ignore part of the input data for the next SOLVE command.  The data
must  have been read before.  The Scan list, Source list and time ranges
are emptied each time a new READ or FILE command is typed.

.ti -4
1 INCLUDE
.ti +4
 [POINT\]INCLUDE [/SCAN List of scans] [/SOURCE List of source]
[/TIME Tmin Tmax]

     Include part of the input data (presumably previously ignored)  for
the  next  minimization.  The data must have been read before.  The Scan
list or Source list can be replaced by the "wildcard" * to  include  any
scan or source.

.ti -4
1 LIST
.ti +4
[POINT_\]LIST [Scan] [/OUTPUT Filename]

     List the current data or the  specified  scan.   The  bad  data  is
flagged  with  an  exclamation  point.   Option  /OUTPUT  can be used to
redirect the LIST command output (default Terminal).

.ti -4
1 PARAMETER
.ti +4
[POINT_\]PARAMETER NAME [Value] [/FREE]

     Fix a fit parameter at the specified value, or free  the  specified
parameter if option /FREE is present.  Value is in arcsecond.  Available
parameters are
.nf
      IAZ     Zero azimuth
      IEL+COV Zero elevation
      COH     Azimuth collimation
      MVE     East Inclination
      MVN     North Inclination
      NPE     Non perpendicularity
      REF1    1st order refraction
      REF2    2nd order refraction
      ELES    grav.+exc. coder ele. (sin)
      ELEC    grav.+exc. coder ele. (cos)
      AZES    excen. coder az. (sin)
      AZEC    excen. coder az. (cos)
.fi
REF1 and REF2 are preset to 0 and 0 by default, because  on  Plateau  de
Bure  the  refraction  is  computed in real from meteo parameters.  They
should not be freed, except for very specific purposes.

.ti -4
1 PLOT
.ti +4
          PLOT [Argument] [/BAD]

     Plot some aspect of the data and fit results.  Argument may be
.nf
         SUMMARY (Default).
         Residuals in El versus residuals in Az.

         COVERAGE
         Elevation versus Azimuth of data points.

         ERRORS AZIMUTH or ERRORS ELEVATION
         Original pointing errors in Az or El, as a function of  Az  and
         El.

         RESIDUALS AZIMUTH or RESIDUALS ELEVATION
         Residual pointing errors in Az or El, as a function of  Az  and
         El.

         TIME
         Residual pointing errors as a function of time.

         CIRCLE
         General summary of pointing behaviour :  arrows indicating  the
         pointing  error  magnitude  and direction on a (Az,El) circular
         diagram.
.fi
By default, only good data points (those considered  for  the  fit)  are
plotted.    If   option  /BAD  is  present,  rejected  data  points  (as
implicitely specified by commands IGNORE, REJECT, SET ELEVATION and  SET
RMS) are also plotted, using the virtual pen number 2.

.ti -4
1 PRINT
.ti +4
[POINT_\]PRINT [/OUTPUT Filename|PRINTER] [/HEADER]

     List the last  fit  results  computed  by  command  SOLVE.   Option
/HEADER can be used to list all fit parameters; by default only variable
parameters are listed.  Option /OUTPUT can be used to redirect the PRINT
command  output  (default Terminal); if PRINTER is specified as argument
to /OUPUT option, the fit results are printed.

.ti -4
1 READ
.ti +4
[POINT_\]READ Antenna_number [Station_name]

     Read the data of antenna Antenna_number (on station Station_name if
specified).  A file must have been opened using command FILE before.

.ti -4
1 REJECT
.ti +4
[POINT_\]REJECT MaxError

     Reject  all  data  points  with  pointing  residuals  greater  than
MaxError  for  the  next  fits.  A READ command will reset the rejection
threshold to infinity.

.ti -4
1 SET
.ti +4
[POINT_\]SET Item Value1 [Value2]

     Set some selection criterium.  Possible items are
.nf
         ELEVATION Elmin Elmax :
         specify valid elevation range (default 5 90, in degrees)

         RMS rmsmax :
         specify maximum standard deviation (from  gauss  fit)  for  the
         measured pointing errors.
.fi
Optical  pointing  data  has  measured  pointing  errors  with  standard
deviation  set to 0, so that SET RMS although meaningfull only for RADIO
or INTER data, can also be used transparently.

     As for comands  IGNORE,  INCLUDE  and  REJECT,  selection  criteria
specified  by  the  SET command will be considered for the next PLOT and
FIT commands.  However, contrary to commands IGNORE, INCLUDE and REJECT,
the criteria will not be reset by any READ or FILE command.

.ti -4
1 SHOW
.ti +4
[POINT_\]SHOW Item

     shows the specified items :
.nf
     RMS        The maximum standard deviation of original measurements
     IGNORED    Data points ignored based on Scans or Source name
     ELEVATION  The elevation range
     REJECTED   The rejected data scans, based on previous fit model
.fi
.ti -4
1 SOLVE
.ti +4
[POINT_\]SOLVE

     Compute fit results, considering only the valid data points implied
by commands IGNORE, REJECT, SET RMS or SET ELEVATION.
.ti -4
1 ENDOFHELP
