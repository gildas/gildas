\documentclass[11pt]{article}

% GILDAS specific definitions
\include{gildas-def}

\title {Determination of Pointing Parameters \\
  IRAM Plateau de Bure Interferometer}%
\author {S. Guilloteau$^{1}$}%
\date{Document probably older than you think}%
\makeindex{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\maketitle{}

\begin{center}
  Version 2.0
\end{center}

\begin{center}
  (1) Institut de Radio Astronomie Millim\'etrique\\
  300 Rue de la Piscine\\
  F-38406 Saint Martin d'H\`eres
\end{center}

This document describes the current tools for determining the pointing of
the antennas on Plateau de Bure. Good pointing is essential to the
operation of the interferometer. Unfortunately, there are many ways to do
bad or poor pointing, and no simple way to do it well.  Please read this
document carefully . Its contents are important.

\begin{rawhtml}
  Note: this is the on-line version of the "Pointing Measurements" A <A
  HREF="../../pdf/point.pdf"> PDF version</A> is also available.
\end{rawhtml}

This document is a sum up of the steps relevant for pointing.  More details
on the individual programs and command mentionned in this document are
available in:
\begin{latexonly}
\begin{itemize}
\item{IRAM Plateau de Bure Interferometer: Introduction}
\item{IRAM Plateau de Bure Interferometer: OBS Users Guide}
\item{\class\ Users Guide}
\item{\clic\ Users Guide}
\item{\sic\ and \greg\ Users Guide}
\end{itemize}
\end{latexonly}

\begin{rawhtml}
<UL>
<LI> IRAM Plateau de Bure Interferometer:
<A HREF="../noema-intro-html/noema-intro.html"> Introduction</A>
<LI> IRAM Plateau de Bure Interferometer:
<A HREF="../obs-html/obs.html"> OBS Users Guide</A>
<LI> <A HREF="../clic-html/clic.html"> CLIC:</A>  Continuum and Line
Interferometer Calibration
<LI> <A HREF="../class-html/class.html"> CLASS:</A>  Continuum and Line
Analysis Simple Software
<LI> <A HREF="../sic-html/sic.html"> SIC</A>  and 
<A HREF="../greg-html/greg.html"> GreG</A>  Users Guide
</UL>
\end{rawhtml}

\newpage
\tableofcontents

\newpage
\section{Basic pointing parameters}

%\baselineskip=24pt
\subsection{Parameter List}

Let dX and dY be the offsets on the sky parallel to the Azimuth axis and
Elevation axis respectively, produced by a pointing error. The standard
pointing model for the Plateau de Bure antennas has the following
parameters:
\begin{verbatim}
IAZ      azimuth encoder zero  
         dX = IAZ*cos(El)               dY = 0
IEL      elevation encoder zero 
         dX = 0                         dY = IEL
COH      telescope azimuth collimation
         dX = cos(El)*asin(COH/cos(El)) dY = -asin(sin(El)/sqrt(1-COH**2))
         for COH << cos(El), equivalent to
         dX = COH                       dY = 0, 
COV      telescope vertical collimation
         dX = 0                         dY = COV
MVE      Azimuth axis tilt towards East
         dX =  MVE*cos(Az)*sin(El)      dY = -MVE*sin(Az)
MVN      Azimuth axis tilt towards North
         dX = -MVN*sin(Az)*sin(El)      dY = -MVN*cos(Az)
NPE      Elevation axis tilt (axis non perpendicularity)
         dX = -NPE*sin(El)              dY = 0
         (assuming small NPE and COH in practice.)
REF0     First order refraction coefficient
         dX = 0                         dY = -REF0/tan(El)
REF1     Second order refraction coefficient
         dX = 0                         dY = -REF1/tan(El)**3
ELES     gravity+eccentricity of Elevation encoder 
         dX = 0                         dY =  ELES*sin(El)
ELEC     gravity+eccentricity of Elevation encoder
         dX = 0                         dY =  ELEC*cos(El)
AZES     eccentricity of Azimuth encoder
         dX = AZES*sin(Az)*cos(El)      dY = 0
AZEC     eccentricity of Azimuth encoder
         dX = AZEC*cos(Az)*cos(El)      dY = 0
HEL      Homology elevation bending (cos(El))
         dX = 0                         dY = -HEL*cos(El)
\end{verbatim}
The total pointing correction is thus, in the small COH approximation
\begin{verbatim}
    dX = IAZ.cos(El) + COH + sin(El) * (MVE*cos(Az)-MVN*sin(Az)-NPE)
          + cos(El) * (AZES*sin(Az)+AZEC*cos(Az))
    dY = IEL+COV - (MVE*sin(Az)+MVN*cos(Az))
          + (ELES*sin(El)+(ELEC-HEL)*cos(El))
          - REF0/tan(El) - REF1/tan(El)**3 - REF2/tan(El)**5
\end{verbatim}

Since each antenna is equipped with an off-axis optical telescope, there
are two sets of {\tt (COH,COV)} values, one for the radio axis and one for
the optical axis. As only {\tt IEL+COV} is meaningful, the radio {\tt COV}
is set to zero, fixing the {\tt IEL} value.

Five terms dominate the pointing model : {\tt IAZ, IEL+COV, COH, MVE} and
{\tt MVN}. In principle {\tt COH, IEL,} and {\tt COV} depend only on the
antenna, {\tt MVE} and {\tt MVN} depend on the station and antenna (azimuth
bearing), and {\tt IAZ} depends on the antenna (encoder), the station
(footpad positions) and the positioning of the antenna on the station
(repeatability 1 mm).

The refraction coefficients are weather dependant. Accordingly, the
parameters are computed in real time, using temperature, pressure and
humidity. REF0 and REF1 are thus fixed to zero in the pointing fit.

Among the other terms, all of which depend on the antenna, just two have
been found to be of some importance for some antennas: {\tt ELEC} which
enters only in combination with {\tt HEL} in radio (small values on all
antennas), and possibly {\tt NPE} (Antenna 3). These are marginally
different from zero, however, and confirmation will be required. The other
parameters can be set to zero.

\subsection{Parameter Files}

The values for these parameters used in real time are stored in several
parameter files, located under directory INTER\_BASE:. To follow the
logical dependence of the parameters upon antenna and station, several
files exist :
\begin{itemize}
\item{GENERAL.ANk}\\
  where k is an antenna number. These files contain the parameters which
  only depends on the antenna: radio and optical collimations {\tt COH} and
  {\tt COV}, {\tt NPE, AZEC, AZES, ELEC, ELES,} and also a default value
  for {\tt IAZ}. They also contain the homology elevation bending, {\tt
    HEL}, the subreflector parameters: {\tt HTR0} and {\tt HTR1} for
  translation, {\tt HVT0} and {\tt HVT1} for tilt, {\tt HFO0} and {\tt
    HFO1} for focus; and two beam-switch parameters (sign and beam
  separation).
\item{Sij.ANk}\\
  where Sij is a station name (for example W00), and k an antenna number.
  These files contains the Antenna-Station dependent parameters, for
  pointing {\tt IAZ, MVE,} and {\tt MVN}, and for interferometry, the {\tt
    Delay}, and the position {\tt X,Y,Z}.
\end{itemize}
The Sij.ANk should {\em never} be editted, but should only be changed using
the CONFIGURE mode of \obs . The GENERAL.ANk file should be editted if a
major change occur.

\section{The POINT program}

\subsection{Description}

The \point\ program is a pointing parameter fitting program based on the
\sic\ user interface, and \greg\ for graphic display.  It is called by the
command:
\begin{verbatim}
$ POINT 
\end{verbatim}

A typical session with \point\ is:
\begin{itemize}
\item{\tt GREG1$\backslash$DEVICE RETRO}\\
  Define graphic device for display
\item{\tt FILE 16-OCT-1989 RADIO}\\
  Define the input data file and type. Three data types exist: RADIO, for
  single-dish radio pointing results, INTER for interferometric pointing
  results, and OPTICAL for optical pointing data. The file extension is
  derived from the data type using the following defaults : .RAD, .INT and
  .OPT respectively.  .RAD files are produced by the \class\ {\tt
    ANALYSE$\backslash$PRINT POINTING} command, .INT files are produced by
  the \clic\ {\tt CLIC$\backslash$SOLVE POINTING} command, and .OPT files
  are produced by the DCL SAV command on the real-time microVAX BURE01. The
  data type also defines the default variable parameters.
\item{\tt READ 2}\\
  Read the data for Antenna 2 from the input file.
\item{\tt SOLVE}\\
  Solve for the pointing parameters using the currently read data.
\item{\tt PLOT SUMMARY}\\
  Display on the graphic device a summary of residual pointing errors;
  residual in Elevation versus residual in Azimuth.
\item{\tt PLOT COVERAGE}\\
  Display the Azimuth and Elevation coverage to see whether correlated
  parameters, such as {\tt IAZ} and {\tt COH} for example, are well
  determined or not.
\item{\tt PRINT/OUTPUT 16-OCT-1989.RADIO/HEADER}\\
  If both the summary and coverage look good, write the fit results to a
  file for the archive. The {\tt /HEADER} option is used to write out the
  values of the fixed parameter also.
\end{itemize}

If some points appear discrepant in the fit or if the fit residuals are
large, there are several possibilities for rejecting bad data points.
\begin{itemize}
\item{\tt REJECT 8.0}\\
  Reject all data points with pointing residuals higher than $8.0"$ after
  the last {\tt SOLVE} command.
\item{\tt SET RMS 2.0}\\
  Select only those data points for which the formal error on the pointing
  observation is less than $2.0"$.  For INTER and RADIO data types, the
  formal error is computed by the \clic\ and \class\ gauss fit commands,
  and the error is set to zero for OPTICAL data.
\item{\tt SET ELEVATION 10.0 80.0}\\
  Select only data points in the elevation range 10 to 80 degrees.  The
  default elevation range is 5 to 90 degrees.
\item{\tt IGNORE/SCAN s1 s2 s3 TO s4 BY s5/SOURCE JUPITER}\\
  Ignore some scans in the data (the scan numbers are displayed by the {\tt
    PLOT SUMMARY} command), or some sources. Previously ignored scans and
  sources can be reconsidered agin using the {\tt INCLUDE} command (same
  syntax as {\tt IGNORE}).
\end{itemize}

If the coverage is bad, it is interesting to fix some parameter to reduce
the errors on correlated parameters. This is especially true for the radio
COH, because the collimation is essentially a constant that depends only on
the antenna. For example
\begin{itemize}
\item{\tt PARAMETER COH 42.0}\\
  Will fix {\tt COH} to $42"$.
\item{\tt PARAMETER MVE/FREE}\\
  Will free the (previously fixed) parameter {\tt MVE}.
\end{itemize}

\subsection{Optical versus Radio}

The types of data files treated by \point\ differ slightly. For Optical
data, the pointing error is relative to a perfect antenna. Hence the {\tt
  POINT$\backslash$PRINT} output directly indicates the new pointing
parameter values. For Radio (or Interferometer) data, however, the pointing
error is relative to the previous model, {\em except for the collimations}.
Hence {\tt POINT$\backslash$PRINT} yields directly {\tt COH} and {\tt COV},
but for all other parameters the \point\ results should be added to the
previous values.

In particular, the Radio results can be used in the \obs\ {\tt
  SET$\backslash$POINTING} command in an incremental way:
\begin{verbatim}
     SET\POINTING IAZ IAZ+dIaz IEL IEL+dIel/ANTENNA 2
\end{verbatim}
where {\tt dIaz, dIel} are the results given by command {\tt
  POINT$\backslash$PRINT} for the parameters {\tt IAZ, IEL}.

\section{Optical Pointing}
\subsection{Getting data}

Optical pointing is done using the MNU program, and the DCL commands CRE
(CREate the data file), SAV (SAVe telescope coordinates in the data file),
CAT (select a star from the star CATalog), and PLA (select a PLAnet), or
using the automatic searching mode of MNU. Please refer to the document
``Foreign Commands'' by A. Perrigouard for a complete description of these
commands.

Two different approaches can be used for the optical pointing, depending
whether you have an idea of the pointing parameters or not.
\begin{itemize}
\item First, make a crude optical pointing with a few stars, 5-8 at low and
  3-6 at high elevations, with good azimuth coverage. The accuracy need not
  be very good ($5"$). Then, use the newly determined pointing constants to
  make a more accurate pointing session with 40 to 60 stars. All stars
  should be rather well centered already , and now the centering accuracy
  can be made better ($1"$). Uniform coverage is not required: most
  important is to have low and high elevation sources.
\item Or use only a single pass with 60 stars centered within $1"$.  This
  can take somewhat longer because the stars may be far off center.
\end{itemize}

\subsection{Optical Pointing using MNU}

MNU includes a mode for optical pointing, accessible through the {\tt
  REFLECTOR} page:
\begin{center}
  {\tt
\begin{tabular}{|c|c|c|c|}
\hline
GOLD  & HELP    & POINT   & REMOTE \\
/DEF  & /TOP    & /STOP   & MICRO \\
\hline
C.SYS &         & Tracking & \\
/HORI & SPI.    & GUID.    & OFFS.\\
\hline
      & Second. & Mirror   & \\
FOCUS & TRANS   & V TILT   & H TILT \\
\hline
SAVE & NEXT     &          &  \\
     & /DIST    & SLEW     & MNU \\
\hline
INIT & ANT.     & REF. RAD & ANTEN. \\
/SUB & REF.     & /OPTIC   &   \\
\hline
\end{tabular}
}
\end{center}
In this mode, activated by the {\tt POINT} key ({\tt PF3} on the {\tt
  REFLECTOR} page), it tries to find the best candidates to map the sky
uniformly in azimuth and elevation. The density of stars is given by
specifying the minimal distance angle between stars, using the {\tt
  GOLD/DISTANCE (PF1/KP2)} key. To hasten a pointing session, the program
will try to find candidates above 10 degrees in elevation and at the
minimum distance (in time) from the current telescope position.

MNU uses a catalog of bright stars named J2000R.CAT in the
[CONTROL.COMMAND] directory, on disk COMP\$DISK:, and writes the pointing
data in the most recently created {\em Date}.OPT file on directory
[OBSERVER.POINTING], on disk RLTDATA ({\em Date} being a date in format
DD-MMM-YYYY as usual).  The file POINTING.OPT, also in [OBSERVER.POINTING],
contains the name of the last pointing data file created along with its
creation time.  MNU assumes that the pointing concerns the default antenna.
To find out or modify this default use the DCL command DEF [Ant], or use
the {\tt GOLD/DEF (PF1/PF1)} key to change it.

To start an optical pointing session, press the {\tt POINT} key. The
program opens the data file {\em Date}.OPT, reads and processes the records
concerning the default antenna, and then closes the file. It opens the
catalog file J2000R.CAT and processes the star coordinates in order to
prepare and to speed up the calculations next to be performed.  Next, the
file is closed.

Specify a minimum distance between stars using the {\tt GOLD/DISTANCE} key,
for instance 10.0 for 10 degrees. MNU will look for the best candidate,
given the present telescope position, the data already saved in {\em
  Date}.OPT, and the possible stars in the catalog file. To move to the
next star, press the {\tt NEXT (KP2)} key.

When the telescope stops slewing and tracks the star, use MNU to move the
telescope slightly so the TV cross-hair is on the star. Meanwhile, as soon
as the command to send the telescope to the requested position has been
spawned, the catalog is searched for the next star candidate. Once the TV
cross-hair is on the star, press the {\tt SAVE (KP1)} key to append the
star position, the current telescope position, the sidereal time and the
refraction correction to the data file. If an error occurs while opening
the file, the operation can be executed again.

Then go on using the {\tt NEXT} key... Every time you press the {\tt NEXT}
key, a message saying ``{\tt No more candidate}'' may appear. You should
then either suspend your pointing sessions using the {\tt GOLD/STOP
  (PF1/PF3)} key, or decrease the minimal distance between stars using the
{\tt GOLD/DISTANCE (PF1/KP2)} key.

\subsection{Reducing data}

To reduce optical pointing data, work on BURE02, in the [OBS.POINTING]
directory, and use \point , specifying the RLTDATA:[OBSERVER.POINTING]{\em
  Date}.OPT file as input:
\begin{verbatim}
$ SET DEF [OBS.POINTING]
$ POINT 
POINT> FILE Date OPTICAL
POINT> READ Antenna_Number
POINT> SOLVE
POINT> [ Other commands ]
\end{verbatim}
The file is fetch by default from directory RLTDATA:[OBSERVER.POINTING].  A
fit will be made with the free parameters {\tt IAZ IEL+COV COH MVE MVN},
and all other parameters fixed at zero.  A fit with more free parameters
(e.g. ELEC and NPE) can be made using the commands :
\begin{verbatim}
POINT> PARAMETER NPE/FREE
POINT> PARAMETER ELEC/FREE
\end{verbatim}
Optical pointing gives only the sum {\tt IEL+COV}. The zero of the
elevation encoder is an antenna constant.  After a fit by \point\ all
parameters should be replaced in the antenna-station parameter file.

\section{Radio Pointing}

\subsection{Full sky, single-dish radio pointing}

If the radio pointing has to be determined for the first time (new antenna,
new receiver, new subreflector alignment, or other major changes), a full
sky radio pointing is necessary. Using the \obs\ {\tt OBS$\backslash$POINT}
command, observe 30 to 40 positions, preferably clustered at high and low
elevations, but well spaced in azimuth.

Use \class\ on BURE02 to reduce the pointing measurements, working on the
[OBS.SINGLE] directory. A procedure named PR:INIT\_POINT can be used in
\class\ to get started. Open the .BUR input file, get a list of the
pointing scan, using commands FIND and LIST. Then the procedure PR:POINT
can be used to process the data scan by scan. For each pointing scan, it
averages separately the two azimuth and two elevation subscans, fit
gaussians, with two coupled components in case of beam switching, to the
averages, and at each PAUSE, the user can write the result in an output
data file, named {\em Date}.BUR-POINT. This can be done while data
acquisition is still going on. Once all data have been reduced, open the
reduced data file in input, select all data using the {\tt
  LAS$\backslash$FIND} command, and then use {\tt ANALYSE$\backslash$PRINT
  POINT/OUTPUT [-.POINTING]{\em Date}.RAD} to write out the fit results.

Stop \class\ and move to [OBS.POINTING] directory to use \point\ to fit the
pointing parameters. The default adjustable parameters are {\tt IAZ,
  IEL+COV, MVE,} and {\tt MVN}, while {\tt COH} is set to its antenna
dependent value, found from file INTER\_BASE:GENERAL.ANk.

In RADIO mode, \point\ gives {\em variations from the previous parameter
  values, except for {\tt COH} where it gives the absolute value} (note
{\tt COV = dIEL}, the variation of {\tt IEL}). Refraction parameters should
always be fixed to zero, since the radio refraction is computed in real
time.

\subsection{Interferometer pointing}

Pointing sources can be observed with the \obs\ command
\begin{verbatim}
    OBS\POINT /ANTENNA Iant /LENGTH 60
\end{verbatim}
in INTERferometry mode if only antenna {\tt Iant} is to be pointed, or
\begin{verbatim}
    OBS\POINT /LENGTH 40
\end{verbatim}
if all antennas must be pointed.

Data reduction can then be done with the {\tt CLIC$\backslash$SOLVE
  POINTING} command in \clic , working on the [OBS.POINTING] directory.
Select a set of pointing scans with the {\tt CLIC$\backslash$FIND} command
(use {\tt SET PROCEDURE POINT}) and process these scans with {\tt
  CLIC$\backslash$SOLVE POINTING [/PLOT] /OUTPUT {\em Date}.INT}. By
default data is appended to that file, add argument {\tt NEW} after the
{\tt {\em Date}.INT} file name to create a new one.

For continuum sources, you should use {\tt SET SUB C01 TO C10} and {\tt SET
  AVERAGE METHOD VECTOR}.  You might want to do an RF\_PASSBAND calibration
to be able to average both sidebands ({\tt SET RF ON, SET BAND AVERAGE});
this is absolutely necessary at high frequencies to improve signal to noise
ratio.  If observing SiO masers at the standard frequency (86.051 USB LOW),
use {\tt SET SUB C02} and {\tt SET BAND UPPER} to process the data in \clic.

Then, use \point\ the same way as for the RADIO (single-dish) data.

\subsection{Limited radio pointing determination}

Once the optical pointing parameters have been determined and entered in
the Antenna-Station file, only one radio pointing on a strong source is
needed to get the radio {\tt COV} and the radio {\tt IAZ}. The radio {\tt
  COH} should be set to its (antenna-only dependent) value.

If the optical {\tt COV} was already well known, you should find the radio
{\tt COV = 0}.  Radio {\tt IAZ} is determined as follows: measure the
azimuth pointing error, divide it by {\tt cos(Elevation)}, and {\bf
  subtract} the result from {\tt IAZ} (Oui, {\bf soustraire}). You should
then be ready for a radio observing !

\section{Inclinometers}

All antennas are equipped with one inclinometer, that should allow
determination of the inclinations without astronomical pointing. Regular
monitoring of the inclinometer outputs {\em must be made}. It is especially
important to get the inclinometer results before and after each pointing
session.  To use the inclinometers:
\begin{itemize}
\item Send an antenna North at some elevation, chosen to avoid the sun at
  any
  azimuth, for example using antenna {\tt Iant}\\
  {\tt \$ COO ANTE Iant HO 180D 85D NULL}, under DCL, or\\
  {\tt OBS$\backslash$SOURCE INCLINO, OBS$\backslash$LOAD}, within \obs
\item Once the antenna stops, run the inclinometer program \\
  {\tt \$ INC Iant /PLOT}\\
\end{itemize}

The antenna will do a full turn at the current elevation, and a sine curve
will be fit to the inclinations measured every 5 degrees.  From this fit,
the parameters MVE and MVN are derived from the azimuth axis tilt measured
by the inclinometers using three additional corrections:
\begin{itemize}
\item a change of sign due to conventions on the orientation of the
  vertical.
\item a local vertical correction to refer the inclinations to the vertical
  of the array center
\item a global gravity corrections of MVE = +10", MVN = +7", to correct for
  large scale deviations of the gravity field due to the Alps.
\end{itemize}

The resulting {\tt MVE} and {\tt MVN} are displayed and written in the file
{\tt INTER\_BASE:INCLINO.ANi}, where {\tt i = Iant}.  The fit is displayed
by GreG if you specified the {\tt /PLOT} option for the INC command. A
hardcopy possibility is offered.

\section{Cookbook, Tricks, and Traps}

It is difficult to produce a cookbook recipe for getting the pointing
parameters. The following {\bf Traps} should be avoided, however:
\begin{itemize}
\item{Azimuth Elevation coverage}\\
  It MUST be reasonable. Never believe the formal errors on {\tt MVE} or
  {\tt MVN} if the Azimuth coverage is less than 270 degrees. Never believe
  the formal error on {\tt COH} if the elevation coverage is less than 60
  degrees. From the pointing error equations, it can be seen that the best
  coverage consists in sources equally spaced in Azimuth, but clustered at
  high and low elevations.
\item{Radio Collimation {\tt COH}}\\
  This is essentially an antenna constant.  It is better to fix {\tt COH}
  to the previous value, rather than trying to fit it (see the {\tt
    INTER\_BASE:GENERAL.ANi} file to get this number).  Accordingly, this
  is done by default. Do not believe {\tt COH} has changed unless you have
  1) many data points ($>40$) 2) a good sky coverage, and 3) a formal error
  on {\tt COH} less than $0.5"$.
\item{Gauss fit formal error}\\
  For radio pointing, if the formal error on the position from gauss fit is
  higher than $2.0"$, the data may be useless.
\item{Avoid long pointing sessions and systematic Az-El coverage}\\
  At least in OPTICAL pointing. Time dependent effects, such as thermal
  behaviour of the optical telescope mount, may then become inextricably
  mixed with Az or El dependent terms.
\end{itemize}

Accordingly, the following {\bf Tricks} should be used:
\begin{itemize}
\item{Use SiO Masers in Pseudo Continuum}\\
  They will help you get a good Az-El coverage. They are usually stronger
  than continuum sources.
\item{Use the beam switch}\\
  Always for continuum sources, but also for SiO masers if the weather is
  poor.
\item{Fix the beam width for \class\ gauss fits}\\
  This helps to reduce the formal error on the position, specially for weak
  sources. This can be done using the \class\ command {\tt
    ANALYZE$\backslash$METHOD CONTINUUM Width}, where Width is in arc
  seconds. The beam width is 55" at 86 GHz.  The PR:INIT\_POINT.CLASS
  procedure executes such a command if the observing frequency is
  specified.
\item{Use the inclinometers}\\
  At least just before and just after the pointing session, but also later
  from time to time to check any time dependence of {\tt MVE} or {\tt MVN}.
\item{Use the specially designed procedure {\tt POINT\_INTER} and
    {\tt POINT\_MULTI}}\\
  These procedures have been written to produce a reasonable coverage,
  considering the functional form of the pointing model equations.
  Pointing sources have been selected to avoid excessive noise.
\end{itemize}

\newpage
\section{POINT Language Internal Help}
\include{point-help-point}

\newpage 

\printindex{}

\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
