subroutine param (line,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  !
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'setup.inc'
  include 'size.inc'
  include 'data.inc'
  ! Local
  logical :: refrac
  integer :: iant
  !
  character(len=36) :: argum,keywor
  character(len=80) :: chain,dummy
  integer :: nkey,l,i,ier,na
  real*8 :: value
  ! PI is defined with more digits than necessary to avoid losing
  ! the last few bits in the decimal to binary conversion
  real*8 :: pi,sfac
  parameter (pi=3.14159265358979323846d0)
  parameter (sfac = pi/180d0/3600d0)
  integer :: iarg,jarg,narg,ivir,nvir
  !
  narg = sic_narg(0)
  do iarg=1,narg,2
    call sic_ke (line,0,iarg,argum,na,.true.,error)
    if (error) return
    call sic_ambigs('PARAMETER',argum,keywor,nkey,   &
     &      nompar,kvar,error)
    if (error) return
  enddo
!
  do iarg=1,narg,2
     call sic_ke (line,0,iarg,argum,na,.true.,error)
     if (error) return
     call sic_ambigs('PARAMETER',argum,keywor,nkey,   &
          &      nompar,kvar,error)
     jarg = iarg+1
     value = coef(nkey)/sfac
     call sic_ke (line,0,jarg,argum,na,.true.,error)
     if (error) return
     if (argum.eq.'*') then
        free(nkey) = .true.
     else
        free(nkey) = .false.
        ivir = index(argum,',')
        if (ivir.eq.0) then
           call sic_r8 (line,0,jarg,value,.false.,error)
           if (error) return
           coef (nkey) = value*sfac
        else
           call sic_math_dble(argum(1:ivir),ivir-1,value,error)
           if (error) return
           coef (nkey) = value*sfac
           value = valpre(nkey)/sfac
           nvir = lenc(argum)-ivir
           call sic_math_dble(argum(ivir+1:),nvir,value,error)
           if (error) return
           valpre(nkey) = value*sfac
        endif
     endif
  enddo
  fitted = .false.
  goto 50
  !
  entry set_radio(iant)
  !
  do i=1,3
    free(i) = .true.
    coef(i) = 0.0
    valpre(i) = 0.0
  enddo
  do i=4,mvar
    free(i) = .false.
    coef(i) = 0.0
    valpre(i) = 0.0
  enddo
  !
  if (iant.lt.10) then
     write(dummy,'(A,I1)') 'general.an',iant
  else
     write(dummy,'(A,I2)') 'general.an',iant
  endif
  call sic_parsef (dummy,chain,'INTER_BASE:','.obs')
  ier = sic_open(1,chain,'OLD',.true.)
  if (ier.ne.0) then
    write(6,*) 'W-READ,  Error opening '//chain(1:lenc(chain))
    call putios('W-READ,  ',ier)
  else
    read(1,*)                  ! Comment
    read(1,*)                  ! IAZ
    read(1,*)                  ! IEL
    read(1,*) value
    coef(3) = value*sfac
    free(3) = .false.
    write(6,'(1x,A,F6.1,A)') 'I-READ,  COH set to ',value,'"'
  endif
  close(unit=1)
  goto 50
  !
  entry set_optical (refrac)
  do i=1,5
    free(i) = .true.
    coef(i) = 0.0
    valpre(i) = 0.0
  enddo
  do i=6,mvar
    free(i) = .false.
    coef(i) = 0.0
    valpre(i) = 0.0
  enddo
  if (.not.refrac) then
    coef(7) = 42.0*sfac
    coef(8) = 0.04*sfac
  endif
  goto 50
  !
  50    npf =0
  nfix=0
  do l=1,kvar
    if (free(l)) then
      npf=npf+1
      ipar(npf)=l
    else
      nfix=nfix+1
      ifixed(nfix)=l
    endif
  enddo
end
!
blockdata nom_param
  ! Global
  include 'setup.inc'
  include 'size.inc'
  include 'data.inc'
  data nompar /   &
     &    'IAZ     Zero azimuth',   &
     &    'IEL+COV Zero elevation',   &
     &    'COH     Azimuth collimation',   &
     &    'MVE     East Inclination ',   &
     &    'MVN     North Inclination',   &
     &    'NPE     Non perpendicularity',   &
     &    'REF1    1st order refraction',   &
     &    'REF2    2nd order refraction',   &
     &    'ELES    grav.+exc. coder ele. (sin)',   &
     &    'ELEC    grav.+exc. coder ele. (cos)',   &
     &    'AZES    excen. coder az. (sin)',   &
     &    'AZEC    excen. coder az. (cos)',   &
     &    'incl. coder az. (sin)',   &
     &    'incl. coder az. (cos)',   &
     &    'incl. coder ele. (sin)',   &
     &    'incl. coder ele. (cos)'/
end
