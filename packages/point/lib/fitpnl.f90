subroutine fitpnl(x,y,n,nterms,ipar,a,niter,   &
     &    num,yfit,sigma,sigmaa,eps,bavard, good)
  !---------------------------------------------------------------------
  !	Fit non lineaire
  !
  !	N est le nombre de points
  !       NTERMS est le nombre de terms a fitter
  !       A(J),J=1,NTERMS sont les estimations initiales des parametres
  !       X(K,J),J=1,N K=1,NVAR et Y(J),J=1,N sont les valeurs des X et Y
  !       NITER est le nombre maximal d'iterations tolere
  !       NUM est le nombre d'iterations effectuees
  !       YFIT(J),J=1,N  sont les valeurs fittees
  !	SIGMA est le sigma du fit (S(Y(J)-YFIT(J))**2)/N
  !	SIGMAA(J),J=1,NTERMS sont les sigmas sur les parametres
  !	EPS est la tolerance sur SQRT(CHISQ)
  !---------------------------------------------------------------------
  ! Global
  include 'size.inc'
  !
  integer :: n                      ! Number of data points
  real*8 :: x(3,n)                  ! Data points coordinates
  real*8 :: y(n)                    ! Data points values
  integer :: nterms                 ! Number of parameters
  integer :: ipar(mvar)             !
  real*8 :: a(mvar)                 ! Parameters
  integer :: niter                  ! Maximum Number of iterations
  integer :: num                    ! Current Number of iterations
  real*8 :: yfit(n)                 ! Fitted values
  real*8 :: sigma                   ! Rms of residuals
  real*8 :: sigmaa(nterms)          ! Errors on parameters
  real*8 :: eps                     ! Stability of minimum
  logical :: bavard                 ! Write down progress
  logical :: good(n)                !
  ! Global
  real*8 :: funpnl,fchisq
  external :: funpnl,fchisq
  ! Local
  real*8 :: alpha(mvar,mvar),array(mvar,mvar),beta(mvar)
  real*8 :: deriv(mvar),b(mvar),aa(mvar)
  integer :: ntermx,i,j,k,jj,kk
  real*8 :: flamda,rts,chisq1,chisqr
  !
  ntermx=mvar
  flamda=0.001
  rts=57.29577951*3600.
  !
  do num=1,niter
    do j=1,nterms
      beta(j)=0.
      do k=1,j
        alpha(j,k)=0.
      enddo
    enddo
    do i=1,n
      if (good(i)) then
        call dpoinl(x(1,i),a,deriv)
        do  j=1,nterms
          jj=ipar(j)
          beta(j)=beta(j)+(y(i)-funpnl(x(1,i),a))*deriv(jj)
          do k=1,j
            kk=ipar(k)
            alpha(j,k)=alpha(j,k)+deriv(jj)*deriv(kk)
          enddo
        enddo
      endif
    enddo
    do j=1,nterms
      do k=1,j
        alpha(k,j)=alpha(j,k)
      enddo
    enddo
    do i=1,n
      if (good(i)) yfit(i)=funpnl(x(1,i),a)
    enddo
    chisq1=fchisq(y,n,nterms,yfit,good)
    !
    100      continue
    do j=1,nterms
      do k=1,nterms
        array(j,k)=alpha(j,k)/dsqrt(alpha(j,j)*alpha(k,k))
      enddo
      array(j,j)=1.+flamda
    enddo
    call amtinv(array,nterms)
    do j=1,nterms
      jj=ipar(j)
      b(j)=a(jj)
      do  k=1,nterms
        b(j)=b(j)+beta(k)*array(j,k)/dsqrt(alpha(j,j)*alpha(k,k))
      enddo
    enddo
    do j=1,ntermx
      aa(j)=a(j)
    enddo
    do j=1,nterms
      jj=ipar(j)
      aa(jj)=b(j)
    enddo
    do i=1,n
      if (good(i)) yfit(i)=funpnl(x(1,i),aa)
    enddo
    chisqr=fchisq(y,n,nterms,yfit,good)
    if (chisq1.lt.chisqr) then
      flamda = 10*flamda
      goto 100
    endif
    !
    ! Converged ...
    flamda=flamda/10.
    sigma=dsqrt(chisqr)
    do j=1,nterms
      jj=ipar(j)
      a(jj)=b(j)
      sigmaa(j)=dsqrt(array(j,j)/alpha(j,j))*sigma
    enddo
    if (bavard) then
      write(6,1001) num,sigma*rts
      do k=1,nterms
        jj=ipar(k)
        write(6,1000) num,jj,a(jj)*rts,sigmaa(k)*rts
      enddo
    endif
    if ((chisq1-chisqr).lt.eps*chisq1) return
  enddo
  1000  format(' ite = ',i3,' a(',i2,') =',f12.2,' err = ',f12.3)
  1001  format(' ite = ',i3,' sigma = ',f12.2)
end
