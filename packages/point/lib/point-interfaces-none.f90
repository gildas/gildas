module point_interfaces_none
  interface
    subroutine point_pack_set(pack)
      use gpack_def
      !----------------------------------------------------------------------
      ! Public package definition routine.
      ! It defines package methods and dependencies to other packages.
      !----------------------------------------------------------------------
      type(gpack_info_t), intent(out) :: pack ! Package info structure
    end subroutine point_pack_set
  end interface
  !
  interface
    subroutine point_pack_init(gpack_id,error)
      use sic_def
      !----------------------------------------------------------------------
      ! Private routine to set the POINT environment.
      !----------------------------------------------------------------------
      integer :: gpack_id
      logical :: error
    end subroutine point_pack_init
  end interface
  !
  interface
    subroutine point_pack_clean(error)
      !----------------------------------------------------------------------
      ! Private routine to clean the POINT environment
      ! (e.g. Remove temporary buffers, unregister languages...)
      !----------------------------------------------------------------------
      logical :: error
    end subroutine point_pack_clean
  end interface
  !
end module point_interfaces_none
