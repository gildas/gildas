subroutine flag (line,comm,error)
  character(len=*) :: line          !
  character(len=*) :: comm          !
  logical :: error                  !
  !
  write(6,*) 'W-FLAG,  Not yet implemented'
end
!<FF>
subroutine plot_result (line,comm,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  !
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  character(len=*) :: comm          !
  logical :: error                  !
  ! Global
  include 'setup.inc'
  ! Local
  integer :: mvoc1,mvoc2
  parameter (mvoc1=6,mvoc2=2)
  integer :: nkey1,nkey2,na
  character(len=12) :: vocab1(mvoc1),vocab2(mvoc2),argum,keyw1,keyw2
  real*8 :: lim
  !
  data vocab1 /'ERRORS','RESIDUALS','SUMMARY','TIME','CIRCLE',   &
     &    'COVERAGE'/
  data vocab2 /'AZIMUTH','ELEVATION'/
  !
  if (.not.fitted) write(6,*) 'W-PLOT,  No fit was done'
  if (sic_present(0,1)) then
     call sic_ke (line,0,1,argum,na,.true.,error)
     if (error) return
     call sic_ambigs('PLOT',argum,keyw1,nkey1,vocab1,mvoc1,   &
          &      error)
     if (error) return
  else
     nkey1 = 3
     keyw1 = 'SUMMARY'
  endif
  lim = 0.0
  if (sic_present(0,2).and.nkey1.eq.3) call sic_r8 (line,0,2,lim,.true., error)
  !
  dobad = sic_present(1,0)
  !
  if (nkey1.le.2) then
    call sic_ke (line,0,2,argum,na,.true.,error)
    if (error) return
    call sic_ambigs('PLOT',argum,keyw2,nkey2,vocab2,mvoc2,error)
    if (error) return
    if (keyw1.eq.'ERRORS') then
      if (keyw2.eq.'AZIMUTH') then
        call plot_err_az
      else
        call plot_err_el
      endif
    else
      if (keyw2.eq.'AZIMUTH') then
        call plot_res_az
      else
        call plot_res_el
      endif
    endif
  elseif (keyw1.eq.'SUMMARY') then
    call plot_summary (lim)
  elseif (keyw1.eq.'COVERAGE') then
    call plot_coverage
  elseif (keyw1.eq.'TIME') then
    call plot_time
  elseif (keyw1.eq.'CIRCLE') then
    call plot_circle
  endif
end
!<FF>
subroutine plot_summary (lim)
  use gkernel_interfaces
  ! Global
  include 'size.inc'
  include 'setup.inc'
  include 'data.inc'
  ! Local        
  integer :: i
  real*8 :: zmax,lim
  real*4 :: ymax
  character(len=4) :: chain
  logical :: err
  !
  err = .false.
  !
  call gr_exec1('SET FONT SIMPLEX')
  call gr_exec1('SET PLOT_PAGE LANDSCAPE')
  call gr_exec1('SET BOX SQUARE')
  call gr_exec1('SET COORDINATE USER')
  !
  ! Compute limits
  zmax = 0.0
  do i=1,ndata
    if (dobad) then
      zmax = max(zmax,abs(resel(i)),abs(resaz(i)))
    elseif (good(i) .or. good(ndata+i)) then
      zmax = max(zmax,abs(resel(i)),abs(resaz(i)))
    endif
  enddo
  zmax = 1.1*zmax
  if (zmax.eq.0.0) zmax = 0.1
  if (lim.ne.0.0) zmax = lim
  ymax = zmax
  call gr_limi (4,-ymax,ymax,-ymax,ymax)
  call gr_exec1 ('BOX')
  call gr_exec1 ('SET EXPAND 0.6')
  do i=1,ndata
    if (good(i).or.good(ndata+i)) then
      write (chain,'(I4)') scan(i)
      call relocate (resaz(i),resel(i))
      call gr_labe (chain)
    endif
  enddo
  if (dobad) then
    call gr_pen(ipen=2,error=err)
    do i=1,ndata
      if (.not.(good(i).or.good(ndata+i))) then
        write (chain,'(I4)') scan(i)
        call relocate (resaz(i),resel(i))
        call gr_labe (chain)
      endif
    enddo
    call gr_pen(ipen=0,error=err)
  endif
  call gr_exec1 ('SET EXPAND 0.8')
  call gr_exec2 ('ELLIPSE 2 /USER 0 0')
  call gr_exec2 ('ELLIPSE 5 /USER 0 0')
  call gr_exec2 ('ELLIPSE 10 /USER 0 0')
  call gr_exec1 ('DRAW TEXT 0 2.1 "2`" 8')
  call gr_exec1 ('DRAW TEXT 0 5.1 "5`" 8')
  call gr_exec1 ('DRAW TEXT 0.0 -1.5 '//   &
     &    '"Residual pointing errors in Azimuth (`)" 5 /BOX 2')
  call gr_exec1 ('SET ORIENTATION 90')
  call gr_exec1 ('DRAW TEXT -1.5 0.0 '//   &
     &    '"Residual pointing errors in Elevation (`)" 5 /BOX 4')
  call gr_exec1 ('SET ORIENTATION 0')
  call gr_exec1 ('DRAW TEXT  0.0 0.6 '//   &
     &    '"Residual pointing errors" 5 /BOX 8')
  call gr_exec1 ('DRAW TEXT 0.0 1.2 "'//cfile//'" 5 /BOX 8')
  call gr_exec1 ('DRAW TEXT 0.0 0.6 "'//stat_ant//'" 6 /BOX 7')
end
!<FF>
subroutine plot_coverage
  use gkernel_interfaces
  ! Global
  include 'size.inc'
  include 'setup.inc'
  include 'data.inc'
  include 'pi.inc'
  ! Local
  integer :: i
  logical :: err
  !
  err = .false.
  !
  call gr_exec1 ('LIMITS -180 180 0 90')
  call gr_exec1 ('SET PLOT_PAGE LANDSCAPE')
  call gr_exec1 ('BOX')
  call gr_limi (4,-pis,pis,0.0,pis/2.)
  !
  call gtsegm('POINT',err)
  do i=1,ndata
    if (good(i).or.good(ndata+i)) then
      ! call GR8_marker (1,azis(i),eles(i),0.0D0,-1.0D0)
      call gr8_marker (1,x(1,i),x(2,i),0.0d0,-1.0d0)
    endif
  enddo
  call gr_segm_close(err)
  !
  if (dobad) then
    call gr_pen(ipen=2,error=err)
    call gtsegm('BAD',err)
    do i=1,ndata
      if (.not.(good(i).or.good(ndata+i))) then
        call gr8_marker (1,x(1,i),x(2,i),0.0d0,-1.0d0)
      endif
    enddo
    call gr_segm_close(err)
    call gr_pen(ipen=0,error=err)
  endif
  !
  call gr_exec1 ('DRAW TEXT 0 -1.5 "Azimuth" 5 /BOX 2')
  call gr_exec1 ('SET ORIENTATION 90')
  call gr_exec1 ('DRAW TEXT -1.5 0.0 "Elevation" 5 /BOX 4')
  call gr_exec1 ('SET ORIENTATION 0')
  call gr_exec1 ('DRAW TEXT 0.0 0.6 '//   &
     &    '"Pointing sampling in Azimuth and Elevation" 5 /BOX 8')
  call gr_exec1 ('DRAW TEXT 0.0 1.2 "'//cfile//'" 5 /BOX 8')
  call gr_exec1 ('DRAW TEXT 0.0 0.6 "'//stat_ant//'" 6 /BOX 7')
end
!<FF>
subroutine plot_err_az
  use gkernel_interfaces
  ! Global
  include 'size.inc'
  include 'setup.inc'
  include 'data.inc'
  include 'pi.inc'
  ! Local
  real*4 :: xmin,xmax
  integer :: i
  logical :: err
  !
  err = .false.
  !
  call gr_exec1 ('SET PLO POR')
  call gr_exec1 ('SET BOX 4 20 15 26')
  call limipo (ndata,fobs,good,xmin,xmax,dobad)
  call gr_limi (4,-180.0,180.0,xmin*secpi,xmax*secpi)
  call gr_exec1 ('BOX')
  call gr_limi (4,-pis,pis,xmin,xmax)
  !
  call gtsegm('POINT',err)
  do i=1,ndata
    if (good(i).or.good(ndata+i)) then
      ! call GR8_marker (1,azis(i),eraz(i),0.0D0,-1.0D0)
      call gr8_marker (1,x(1,i),fobs(i),0.0d0,-1.0d0)
    endif
  enddo
  call gr_segm_close(err)
  !
  if (dobad) then
    call gr_pen(ipen=2,error=err)
    call gtsegm('BAD',err)
    do i=1,ndata
      if (.not.(good(i).or.good(ndata+i))) then
        call gr8_marker (1,x(1,i),fobs(i),0.0d0,-1.0d0)
      endif
    enddo
    call gr_segm_close(err)
    call gr_pen(ipen=0,error=err)
  endif
  !
  call gr_exec1 ('SET BOX 4 20 2 13')
  call gr_limi (4,0.0,90.0,xmin*secpi,xmax*secpi)
  call gr_exec1 ('BOX')
  call gr_limi (4,0.0,pis/2,xmin,xmax)
  !
  call gtsegm('POINT',err)
  do i=1,ndata
    if (good(i).or.good(ndata+i)) then
      call gr8_marker (1,x(2,i),fobs(i),0.0d0,-1.0d0)
    endif
  enddo
  call gr_segm_close(err)
  !
  if (dobad) then
    call gr_pen(ipen=2,error=err)
    call gtsegm('BAD',err)
    do i=1,ndata
      if (.not.(good(i).or.good(ndata+i))) then
        call gr8_marker (1,x(2,i),fobs(i),0.0d0,-1.0d0)
      endif
    enddo
    call gr_segm_close(err)
    call gr_pen(ipen=0,error=err)
  endif
  !
  call gr_exec1 ('DRAW TEXT 0.0 0.75 "Azimuth" 5 /BOX 8')
  call gr_exec1 ('DRAW TEXT 0.0 -1.2 "Elevation" 5 /BOX 2')
  call gr_exec1 ('DRAW TEXT -0.95 2.2 "\gDA(`)" 5 /BOX 4')
  call gr_exec1 ('DRAW TEXT -0.95 9.8 "\gDA(`)" 5 /BOX 7')
  call gr_exec1 ('DRAW TEXT 0.0 13.5 '//   &
     &    '"Pointing errors in Azimuth " 5 /BOX 8')
  call gr_exec1 ('DRAW TEXT 0.0 1.2 "'//cfile//'" 5 /BOX 8')
  call gr_exec1 ('DRAW TEXT 0.0 13.5 "'//stat_ant//'" 6 /BOX 7')
end
!<FF>
subroutine plot_err_el
  use gkernel_interfaces
  ! Global
  include 'size.inc'
  include 'setup.inc'
  include 'data.inc'
  include 'pi.inc'
  ! Local
  real*4 :: xmin,xmax
  integer :: i
  logical :: err
  !
  err = .false.
  !
  call gr_exec1 ('SET PLO POR')
  call gr_exec1 ('SET BOX 4 20 15 26')
  call limipo(ndata,fobs(ndata+1),good,xmin,xmax,dobad)
  call gr_limi (4,-180.0,180.0,xmin*secpi,xmax*secpi)
  call gr_exec1('BOX')
  call gr_limi (4,-pis,pis,xmin,xmax)
  !
  call gtsegm('POINT',err)
  do i=1,ndata
    if (good(i).or.good(ndata+i)) then
      ! call GR8_marker (1,azis(i),erel(i),0.0D0,-1.0D0)
      call gr8_marker (1,x(1,i),fobs(i+ndata),0.0d0,-1.0d0)
    endif
  enddo
  call gr_segm_close(err)
  !
  if (dobad) then
    call gr_pen(ipen=2,error=err)
    call gtsegm('BAD',err)
    do i=1,ndata
      if (.not.(good(i).or.good(ndata+i))) then
        call gr8_marker (1,x(1,i),fobs(i+ndata),0.0d0,-1.0d0)
      endif
    enddo
    call gr_segm_close(err)
    call gr_pen(ipen=0,error=err)
  endif
  !
  call gr_exec1 ('SET BOX 4 20 2 13')
  call gr_limi (4,0.0,90.0,xmin*secpi,xmax*secpi)
  call gr_exec1 ('BOX')
  call gr_limi (4,0.0,pis/2,xmin,xmax)
  !
  call gtsegm('POINT',err)
  do i=1,ndata
    if (good(i).or.good(ndata+i)) then
      call gr8_marker (1,x(2,i),fobs(i+ndata),0.0d0,-1.0d0)
    endif
  enddo
  call gr_segm_close(err)
  !
  if (dobad) then
    call gr_pen(ipen=2,error=err)
    call gtsegm('BAD',err)
    do i=1,ndata
      if (.not.(good(i).or.good(ndata+i))) then
        call gr8_marker (1,x(2,i),fobs(i+ndata),0.0d0,-1.0d0)
      endif
    enddo
    call gr_segm_close(err)
    call gr_pen(ipen=0,error=err)
  endif
  !
  call gr_exec1 ('DRAW TEXT 0.0 0.75 "Azimuth" 5 /BOX 8')
  call gr_exec1 ('DRAW TEXT 0.0 -1.2 "Elevation" 5 /BOX 2')
  call gr_exec1 ('DRAW TEXT -0.95 2.2 "\gDE(`)" 5 /BOX 4')
  call gr_exec1 ('DRAW TEXT -0.95 9.8 "\gDE(`)" 5 /BOX 7')
  call gr_exec1 ('DRAW TEXT 0.0 13.5 '//   &
     &    '"Pointing errors in Elevation " 5 /BOX 8')
  call gr_exec1 ('DRAW TEXT 0.0 14.2 "'//cfile//'" 5 /BOX 8')
  call gr_exec1 ('DRAW TEXT 0.0 13.5 "'//stat_ant//'" 6 /BOX 7')
end
!<FF>
subroutine plot_res_az
  use gkernel_interfaces
  ! Global
  include 'size.inc'
  include 'setup.inc'
  include 'data.inc'
  include 'pi.inc'
  ! Local
  real*4 :: xmin,xmax
  integer :: i
  logical :: err
  !
  err = .false.
  !
  call gr_exec1 ('SET PLO POR')
  call gr_exec1 ('SET BOX 4 20 15 26')
  call limipo(ndata,resaz,good,xmin,xmax,dobad)
  call gr_limi (4,-180.0,180.0,xmin,xmax)
  call gr_exec1 ('BOX')
  call gr_limi (4,-pis,pis,xmin,xmax)
  !
  call gtsegm('POINT',err)
  do i=1,ndata
    if (good(i).or.good(ndata+i)) then
      ! call GR8_marker (1,azis(i),resaz(i),0.0D0,-1.0D0)
      call gr8_marker (1,x(1,i),resaz(i),0.0d0,-1.0d0)
    endif
  enddo
  call gr_segm_close(err)
  !
  if (dobad) then
     call gtsegm('BAD',err)
     call gr_pen(ipen=2,error=err)
     do i=1,ndata
        if (.not.(good(i).or.good(ndata+i))) then
           call gr8_marker (1,x(1,i),resaz(i),0.0d0,-1.0d0)
        endif
     enddo
     call gr_segm_close(err)
     call gr_pen(ipen=0,error=err)
  endif
  !
  call gr_exec1 ('SET BOX 4 20 2 13')
  call gr_limi  (4,0.0,90.0,xmin,xmax)
  call gr_exec1 ('BOX')
  call gr_limi  (4,0.0,pis/2,xmin,xmax)
  !
  call gtsegm ('POINT',err)
  do i=1,ndata
     if (good(i).or.good(ndata+i)) then
        call gr8_marker (1,x(2,i),resaz(i),0.0d0,-1.0d0)
     endif
  enddo
  call gr_segm_close(err)
  !
  if (dobad) then
    call gtsegm('BAD',err)
    call gr_pen(ipen=2,error=err)
    do i=1,ndata
      if (.not.(good(i).or.good(ndata+i))) then
        call gr8_marker (1,x(2,i),resaz(i),0.0d0,-1.0d0)
      endif
    enddo
    call gr_segm_close(err)
    call gr_pen(ipen=0,error=err)
  endif
  !
  call gr_exec1 ('DRAW TEXT 0.0 0.75 "Azimuth" 5 /BOX 8')
  call gr_exec1 ('DRAW TEXT 0.0 -1.2 "Elevation" 5 /BOX 2')
  call gr_exec1 ('DRAW TEXT -0.95 2.2 "\gDA(`)" 5 /BOX 4')
  call gr_exec1 ('DRAW TEXT -0.95 9.8 "\gDA(`)" 5 /BOX 7')
  call gr_exec1 ('DRAW TEXT 0.0 13.5 '//   &
     &    '"Residual pointing errors in Azimuth" 5 /BOX 8')
  call gr_exec1 ('DRAW TEXT 0.0 14.2 "'//cfile//'" 5 /BOX 8')
  call gr_exec1 ('DRAW TEXT 0.0 13.5 "'//stat_ant//'" 6 /BOX 7')
end
!<FF>
subroutine plot_res_el
  use gkernel_interfaces
  ! Global
  include 'size.inc'
  include 'setup.inc'
  include 'data.inc'
  include 'pi.inc'
  ! Local
  real*4 :: xmin,xmax
  integer :: i
  logical :: err
  !
  err = .false.
  !
  call gr_exec1 ('SET PLO POR')
  call gr_exec1 ('SET BOX 4 20 15 26')
  call limipo (ndata,resel,good,xmin,xmax,dobad)
  call gr_limi (4,-180.0,180.0,xmin,xmax)
  call gr_exec1('BOX')
  call gr_limi (4,-pis,pis,xmin,xmax)
  !
  call gtsegm('POINT',err)
  do i=1,ndata
    if (good(i).or.good(ndata+i)) then
      ! call GR8_marker (1,azis(i),resel(i),0.0D0,-1.0D0)
      call gr8_marker (1,x(1,i),resel(i),0.0d0,-1.0d0)
    endif
  enddo
  call gr_segm_close(err)
  !
  if (dobad) then
    call gr_pen(ipen=2,error=err)
    call gtsegm('BAD',err)
    do i=1,ndata
      if (.not.(good(i).or.good(ndata+i))) then
        call gr8_marker (1,x(1,i),resel(i),0.0d0,-1.0d0)
      endif
    enddo
    call gr_segm_close(err)
    call gr_pen(ipen=0,error=err)
  endif
  !
  call gr_exec1 ('SET BOX 4 20 2 13')
  call gr_limi (4,0.0,90.0,xmin,xmax)
  call gr_exec1 ('BOX')
  call gr_limi (4,0.0,pis/2,xmin,xmax)
  !
  call gtsegm('POINT',err)
  do i=1,ndata
    if (good(i).or.good(ndata+i)) then
      call gr8_marker (1,x(2,i),resel(i),0.0d0,-1.0d0)
    endif
  enddo
  call gr_segm_close(err)
  !
  if (dobad) then
    call gr_pen(ipen=2,error=err)
    call gtsegm('BAD',err)
    do i=1,ndata
      if (.not.(good(i).or.good(ndata+i))) then
        call gr8_marker (1,x(2,i),resel(i),0.0d0,-1.0d0)
      endif
    enddo
    call gr_segm_close(err)
    call gr_pen(ipen=0,error=err)
  endif
  !
  call gr_exec1 ('DRAW TEXT 0.0 0.75 "Azimuth" 5 /BOX 8')
  call gr_exec1 ('DRAW TEXT 0.0 -1.2 "Elevation" 5 /BOX 2')
  call gr_exec1 ('DRAW TEXT -0.95 2.2 "\gDE(`)" 5 /BOX 4')
  call gr_exec1 ('DRAW TEXT -0.95 9.8 "\gDE(`)" 5 /BOX 7')
  call gr_exec1 ('DRAW TEXT 0.0 13.5 '//   &
     &    '"Residual pointing errors in Elevation" 5 /BOX 8')
  call gr_exec1 ('DRAW TEXT 0.0 14.2 "'//cfile//'" 5 /BOX 8')
  call gr_exec1 ('DRAW TEXT 0.0 13.5 "'//stat_ant//'" 6 /BOX 7')
end
!<FF>
subroutine plot_time
  use gkernel_interfaces
  ! Global
  include 'size.inc'
  include 'setup.inc'
  include 'data.inc'
  ! Local
  real*4 :: xmin,xmax,tmin,tmax
  integer :: i
  logical :: err
  !
  err = .false.
  !
  call gr_exec1 ('SET PLO POR')
  call gr_exec1 ('SET BOX 4 20 15 26')
  call limipo (ndata,resaz,good,xmin,xmax,dobad)
  call limipo (ndata,tiem,good,tmin,tmax,dobad)
  call gr_limi (4,tmin,tmax,xmin,xmax)
  !
  call gtsegm('POINT',err)
  do i=1,ndata
    if (good(i).or.good(ndata+i)) then
      call gr8_marker (1,tiem(i),resaz(i),0.0d0,-1.0d0)
    endif
  enddo
  call gr_segm_close(err)
  !
  if (dobad) then
    call gr_pen(ipen=2,error=err)
    call gtsegm('BAD',err)
    do i=1,ndata
      if (.not.(good(i).or.good(ndata+i))) then
        call gr8_marker (1,tiem(i),resaz(i),0.0d0,-1.0d0)
      endif
    enddo
    call gr_segm_close(err)
    call gr_pen(ipen=0,error=err)
  endif
  !
  call gr_exec1 ('BOX')
  call gr_exec1 ('SET BOX 4 20 2 13')
  call limipo (ndata,resel,good,xmin,xmax,dobad)
  call gr_limi (4,tmin,tmax,xmin,xmax)
  !
  call gtsegm('POINT',err)
  do i=1,ndata
    if (good(i).or.good(ndata+i)) then
      call gr8_marker (1,tiem(i),resel(i),0.0d0,-1.0d0)
    endif
  enddo
  call gr_segm_close(err)
  !
  if (dobad) then
    call gr_pen(ipen=2,error=err)
    call gtsegm('BAD',err)
    do i=1,ndata
      if (.not.(good(i).or.good(ndata+i))) then
        call gr8_marker (1,tiem(i),resel(i),0.0d0,-1.0d0)
      endif
    enddo
    call gr_segm_close(err)
    call gr_pen(ipen=0,error=err)
  endif
  call gr_exec1 ('BOX')
  ! call gr_exec1 ('POINT')
  call gr_exec1 ('DRAW TEXT 0.0 0.75 "Time" 5 /BOX 8')
  call gr_exec1 ('DRAW TEXT 0.0 -1.2 "Time" 5 /BOX 2')
  call gr_exec1 ('DRAW TEXT -0.95 2.2 "\gDE(`)" 5 /BOX 4')
  call gr_exec1 ('DRAW TEXT -0.95 9.8 "\gDA(`)" 5 /BOX 7')
  call gr_exec1 ('DRAW TEXT 0.0 13.5 '//   &
     &    '"Residual pointing errors versus Time" 5 /BOX 8')
  call gr_exec1 ('DRAW TEXT 0.0 14.2 "'//cfile//'" 5 /BOX 8')
  call gr_exec1 ('DRAW TEXT 0.0 13.5 "'//stat_ant//'" 6 /BOX 7')
end
!<FF>
subroutine plot_circle
  ! Global
  include 'size.inc'
  include 'setup.inc'
  include 'data.inc'
  ! Local
  integer :: i
  real :: rad,crad,srad,pi,rmax,scal,taill,err,r,sina,cosa
  real :: x0,y0,x1,y1
  !
  character(len=2) :: cij(10)
  character(len=4) :: chain
  data cij/'0','10','20','30','40','50','60','70','80','90'/
  !
  pi = acos(-1.0)
  call gr_exec1 ('SET PLO PORTRAIT')
  call gr_exec1 ('SET BOX_LOCATION 2 19 6 23')
  call gr_exec1 ('SET EXPAND 0.7')
  call gr_exec1 ('LIMITS 90 -90 90 -90')
  call gr_exec1 ('SET COOR USER')
  do i = 1,10
    call relocate(0.0d0,0.0d0)
    call gr_exec2('ELLIPSE '//cij(i))
    if (i.gt.1 .and. i.lt.10) then
      call relocate (-5.0d0,-103.0d0+10.0d0*dble(i))
      call gr_labe  (cij(i))
    endif
  enddo
  do i=-180,150,30
    rad = i*pi/180.0
    srad = sin(rad)
    crad = cos(rad)
    call relocate (0.0d0,0.0d0)
    call draw (dble(90.0*srad),dble(90.0*crad))
    call relocate (dble(100.0*srad),dble(100.0*crad))
    write (chain,'(I4)') i
    call gr_labe (chain)
  enddo
  call gr_exec1 ('DRAW TEXT 0 -120 "'//stat_ant//'" 5')
  call gr_exec1 ('DRAW TEXT 0 -110 '//   &
     &    '"Residual pointing errors in function of Azimuth and Elevation"'   &
     &    //' 5')
  call gr_exec1 ('DRAW MARKER 90 90')
  !
  rmax = 0.0
  do i=1,ndata
    err = sqrt(resaz(i)**2+resel(i)**2)
    rmax= max(rmax,err)
  enddo
  scal  = 25.0/rmax            ! 25 Degrees for maximum error
  taill = 5.0*scal             ! Size of 5" marker
  call gr_draw ('ARROW',2,90.0-taill,90.0)
  call relocate (90d0-0.5d0*taill,90.0d0)
  call gr_labe_cent(8)
  call gr_labe ('5`')
  do i=1,ndata
    r = 90.0-180.0*x(2,i)/pi   ! Degrees
    sina = sin(x(1,i))
    cosa = cos(x(1,i))
    x0 = r*sina
    y0 = r*cosa
    x1 = x0+(resaz(i)*cosa-resel(i)*sina)*scal
    y1 = y0-(resaz(i)*sina+resel(i)*cosa)*scal
    call gr_draw ('MARKER',2,x0,y0)
    call gr_draw ('ARROW',2,x1,y1)
  enddo
end
!<FF>
subroutine limipo (n,x,good,dmin,dmax,bad)
  integer :: n                      !
  real*8 :: x(*)                    !
  logical :: good(*)                !
  real :: dmin                      !
  real :: dmax                      !
  logical :: bad                    !
  ! Local
  integer :: i,k
  real*8 :: xdiff,xmin,xmax
  k = 0
  xmin = 1d38
  xmax = -1d38
  if (bad) then
    do i=1,n
      xmin = min(x(i),xmin)
      xmax = max(x(i),xmax)
    enddo
    k = n
  else
    do i=1,n
      if (good(i).or.good(n+i)) then
        xmin = min(x(i),xmin)
        xmax = max(x(i),xmax)
        k = k+1
      endif
    enddo
  endif
  if (k.eq.0) then
    dmin = -0.1
    dmax =  0.1
    return
  endif
  if (xmin.lt.xmax) then
    xdiff = 0.1*(xmax-xmin)
  elseif (xmin.ne.0.) then
    xdiff = 0.1*abs(xmin)
  else
    xdiff = 0.1
  endif
  dmin = xmin-xdiff
  dmax = xmax+xdiff
end
