      REAL RMSMAX,ELMIN,ELMAX
      REAL SIGMA,SIGMA_AZ,SIGMA_EL
!
      INTEGER NDATA                      ! Number of original points
      INTEGER NPT                        ! Number of points to fit
      INTEGER KP                         ! Number of Good points
      INTEGER NPF, NFIX                  ! Fixed and variable parameters
      LOGICAL DOBAD                      ! Plot bad data too...
      LOGICAL FITTED                     ! Fit done
!
      CHARACTER*80 CFILE
      CHARACTER*1 CTYPE
      CHARACTER*12 STAT_ANT
      COMMON /COMSET/ RMSMAX,ELMIN,ELMAX,                               &
     &SIGMA,SIGMA_AZ,SIGMA_EL,                                          &
     &NDATA,NPT,NPF,NFIX,KP, DOBAD, FITTED, CFILE, CTYPE, STAT_ANT
