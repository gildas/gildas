subroutine listpo(line,error)
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  integer :: sic_open
  logical :: sic_present
  include 'setup.inc'
  include 'size.inc'
  include 'data.inc'
  ! Local
  ! PI is defined with more digits than necessary to avoid losing
  ! the last few bits in the decimal to binary conversion
  real*8 :: pi
  parameter (pi=3.14159265358979323846d0)
  real*4 :: secpi,degpi
  parameter (secpi = 3600.*180./pi)
  parameter (degpi = 180./pi)
  integer :: i,lun,iscan,ier
  character(len=80) :: ofile,name
  !
  if (ndata.eq.0 .or. npt.eq.0) return
  lun = 6
  if (sic_present(1,0)) then
    call sic_ch (line,1,1,ofile,i,.true.,error)
    if (error) return
    lun = 1
    name = ofile
    call sic_parsef(name,ofile,' ','.lis')
    ier = sic_open (lun,ofile,'NEW',.false.)
  endif
  !
  iscan = 0
  call sic_i4 (line,0,1,iscan,.false.,error)
  if (error) return
  call get_good (npt,kp)
  write(lun,102)
  do i=1,npt
    if (iscan.eq.0 .or. scan(i).eq.iscan) then
      if (good(i)) then
        write(lun,100) scan(i),nint(x(3,i)),   &
     &          x(1,i)*degpi,x(2,i)*degpi,tiem(i),   &
     &          fobs(i)*secpi,rms(i),source(i)
      else
        write(lun,101) scan(i),nint(x(3,i)),   &
     &          x(1,i)*degpi,x(2,i)*degpi,tiem(i),   &
     &          fobs(i)*secpi,rms(i),source(i)
      endif
    endif
  enddo
  if (lun.ne.6) close(lun)
  !
  100   format(1x,i6,i3,5(1x,f10.3),1x,a)
  101   format(1x,'!',i5,i3,5(1x,f10.3),1x,a)
  102   format(1x,'  Scan Code',   &
     &    '    Az         El         Time       Error      Rms')
end
