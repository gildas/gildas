module point_interfaces
  !---------------------------------------------------------------------
  ! POINT interfaces, all kinds (private or public). Do not use directly
  ! out of the library.
  !---------------------------------------------------------------------
  !
  use point_interfaces_private
  !
end module point_interfaces
