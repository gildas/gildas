subroutine solve(line,comm,error)
  character(len=*) :: line          !
  character(len=*) :: comm          !
  logical :: error                  !
  ! Global
  logical :: sic_present
  include 'setup.inc'
  include 'size.inc'
  include 'data.inc'
  ! Local
  integer :: niter,num,l,naz,nel,i
  character(len=2) :: arg
  real*8 :: eps, funpnl, sigm
  logical :: bavard
  real*8 :: pi,fac,sfac,rts
  parameter (pi=3.14159265358979323846d0)
  !
  sfac = pi/180d0/3600d0
  fac  = 180d0/pi
  rts  = 1.d0/sfac
  !
  bavard = sic_present(0,1)
  eps = 1d-2
  niter=15
  !
  call get_good (npt,kp)
  arg = ' '
  call sic_ke (line,0,1,arg,i,.false.,error)
  if (error) return
  if (arg.eq.'EL') then
    do i=1,ndata
      if (good(i)) then
        good(i) = .false.
        kp = kp-1
      endif
    enddo
  elseif (arg.eq.'AZ') then
    do i=ndata,npt
      if (good(i)) then
        good(i) = .false.
        kp = kp-1
      endif
    enddo
  endif
  write(6,'(A,I4,A,I4)') ' I-POINTING, ',kp,   &
     &    ' good points among ',npt
  if (kp.le.npf) then
    write(6,*) 'E-POINTING,  Not enough data points'
    error = .true.
    return
  endif
  !
  sigco(3) = sigco(3)*cos(coef(3))
  coef(3)  = sin(coef(3))
  valpre(3)= sin(valpre(3))
  if (npf.ne.0) then
    write(6,*) 'I-POINTING,  Calling FITPNL'
    call fitpnl(x,fobs,npt,npf,ipar,coef,niter,num,ffit,   &
     &      sigm,sigco,eps,bavard,good)
    sigma = sigm
  else
    write(6,*) 'I-POINTING,  No variable parameter'
  endif
  !
  do l=1,npt
    ffit(l)=funpnl(x(1,l),coef)
    resd(l)=fobs(l)-ffit(l)
  enddo
  !
  sigma_az = 0
  sigma_el = 0
  naz = 0
  nel = 0
  do l=1,ndata
    fitaz(l)=ffit(l)*rts
    resaz(l)=resd(l)*rts
    fitel(l)=ffit(ndata+l)*rts
    resel(l)=resd(ndata+l)*rts
    if (good(l)) then
      naz = naz+1
      sigma_az = sigma_az+resaz(l)**2
    endif
    if (good(ndata+l)) then
      sigma_el = sigma_el+resel(l)**2
      nel = nel+1
    endif
  enddo
  sigma = sigma_az+sigma_el
  sigma = sqrt(2.0*sigma/max(1,(naz+nel-npf)))
  sigma_az = sqrt(sigma_az/max(1,(naz-npf)))
  sigma_el = sqrt(sigma_el/max(1,(nel-npf)))
  write(6,'(1X,A,F8.2,A,F8.2,A,F8.2,A)')   &
     &    'I-POINTING,  Sigma ',sigma,'  ( Azimuth',sigma_az   &
     &    ,'  Elevation',sigma_el,' ) '
  fitted = .true.
  coef(3)  = asin(coef(3))
  sigco(3) = sigco(3)/cos(coef(3))
  valpre(3)= asin(valpre(3))
end
!<FF>
subroutine print (line,comm,error)
  character(len=*) :: line          !
  character(len=*) :: comm          !
  logical :: error                  !
  ! Global
  integer :: lenc,sic_open
  logical :: sic_present
  include 'setup.inc'
  include 'size.inc'
  include 'data.inc'
  ! Local
  integer :: lun,l,kk,i
  character(len=80) :: ofile,name
  real*8 :: pi,fac,sfac,rts
  parameter (pi=3.14159265358979323846d0)
  character(len=42) :: par30m(mvar)
  integer :: signe(mvar),ier
  !
  data par30m/   &
     &    'P1      Zero azimuth (-IAZ or NULA)',   &
     &    'P7      Zero elevation (-IEL-COV or NULE)',   &
     &    'P2      Azimuth collimation (-COH or COL*)',   &
     &    'P4      East Inclination (-MVE)',   &
     &    'P5      North Inclination (MVN)',   &
     &    'P3      Non perpendicularity (NPE)',   &
     &    'REF1    1st order refraction',   &
     &    'REF2    2nd order refraction',   &
     &    'ELES    grav.+exc. coder ele. (sin)',   &
     &    'ELEC    grav.+exc. coder ele. (cos)',   &
     &    'AZES    excen. coder az. (sin)',   &
     &    'AZEC    excen. coder az. (cos)',   &
     &    'incl. coder az. (sin)',   &
     &    'incl. coder az. (cos)',   &
     &    'incl. coder ele. (sin)',   &
     &    'incl. coder ele. (cos)'/
  data signe/-1,-1,-1,-1,1,1,4*1,1,1,4*1/
  !
  if (.not.fitted) then
    write(6,*) 'E-PRINT,  No fit done'
    error = .true.
    return
  endif
  sfac = pi/180d0/3600d0
  fac  = 180d0/pi
  rts  = 1.d0/sfac
  !
  lun = 6
  if (sic_present(1,0)) then
    call sic_ch (line,1,1,ofile,i,.true.,error)
    if (error) return
    lun = 1
    name = ofile
    call sic_parsef(name,ofile,' ','.result')
    ier = sic_open (lun,ofile,'NEW',.false.)
  endif
  !
  if (sic_present(2,0)) then
    write(lun,100)
    do l=1,kvar
      if (free(l)) then
        write(lun,101) l,valpre(l)/sfac,nompar(l)
      else
        write(lun,102) l,valpre(l)/sfac,coef(l)/sfac,nompar(l)
      endif
    enddo
    write(lun,'(/)')
  endif
  write(lun,'(1X,A,A)') 'POINTING results for file ',   &
     &    cfile(1:lenc(cfile))
  write(lun,'(1X,A,A)') '   ',stat_ant
  write(lun,'(1X,A,I4,A,I4,A)') '  Number of points used ',kp,   &
     &    ', among ',npt,' data points'
  write(lun,103)   &
     &    '  Sigma ',sigma,'  ( Azimuth',sigma_az   &
     &    ,'  Elevation',sigma_el,' ) '
  !
  ! If no 30-m Format
  if (.not.sic_present(3,0)) then
    do l=1,npf
      kk=ipar(l)
      write(lun,104) kk,nompar(kk),coef(kk)/sfac,sigco(l)/sfac
    enddo
  else
    do l=1,npf
      kk=ipar(l)
      write(lun,104) kk,par30m(kk),signe(kk)*coef(kk)/sfac,   &
     &        sigco(l)/sfac
    enddo
  endif
  !
  if (lun.ne.6) close(unit=lun)
  !
  100   format(/,' Par.   Previous',23x,'Name')
  101   format(1x,i3,2x,f10.2,'   ** Variable **   ',3x,a,a)
  102   format(1x,i3,2x,f10.2,' Fixed at ',f10.2,3x,a,a)
  103   format(1x,a,f8.2,a,f8.2,a,f8.2,a,/)
  104   format(1x,i4,1x,a,1x,f12.2,' +/- ',f6.2)
end
