subroutine input_file (line,comm,error)
  use gkernel_interfaces
  character(len=*) :: line          !
  character(len=*) :: comm          !
  logical :: error                  !
  ! Global
  include 'setup.inc'
  include 'size.inc'
  include 'data.inc'
  ! Local
  character(len=4) :: stat
  character(len=60) :: extension,name
  integer :: ier,n
  !
  call sic_ke (line,0,2,ctype,n,.true.,error)
  if (error) return
  call sic_ch (line,0,1,cfile,n,.true.,error)
  if (error) return
  if (ctype.eq.'O') then
    extension = '.opt'
  elseif (ctype.eq.'R') then
    extension = '.rad'
  elseif (ctype.eq.'I') then
    extension = '.int'
  else
    write(6,*) 'E-FILE,  Unknown type '//ctype
    error = .true.
    return
  endif
  !
  npt = 0
  ndata = 0
  fitted  = .false.
  !
  name= cfile
  call sic_parsef(name,cfile,' ',extension)
  open (unit=2,file=cfile,status='OLD',iostat=ier)
  if (ier.ne.0) then
    n = min (lenc(cfile),50)
    write(6,*) 'E-FILE, Error opening file '//cfile(1:n)
    call putios ('E-FILE,  ',ier)
    error = .true.
    return
  endif
  inquire (unit=2,name=cfile)
  !
  if (ctype.eq.'O') then
    call header_op (stat)
  elseif (ctype.eq.'R') then
    call header_ra
  elseif (ctype.eq.'I') then
    call header_in
  endif
  close (unit=2)
end
