block data setupdata
  include 'setup.inc'
  data elmin, elmax /5.0, 90.0/
  data rmsmax /5.0/
end
!
subroutine set (line,comm,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  !
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  character(len=*) :: comm          !
  logical :: error                  !
  ! Global
  include 'setup.inc'
  include 'size.inc'
  ! Local
  integer :: mvocab
  parameter (mvocab=2)
  integer :: nkey,i
  character(len=12) :: vocab(mvocab),argum,keywor
  data vocab /'ELEVATION', 'RMS'/
  !
  call sic_ke (line,0,1,argum,i,.true.,error)
  if (error) return
  call sic_ambigs('SET',argum,keywor,nkey,vocab,mvocab,error)
  if (error) return
  !
  goto (1,2) nkey
  !
  ! Elevation
  1     elmax = 90.0
  elmin = 5.0
  call sic_r4 (line,0,3,elmax,.false.,error)
  if (error) return
  call sic_r4 (line,0,2,elmin,.false.,error)
  if (error) return
  call flag_el(npt,elmin,elmax)
  goto 50
  !
  ! RMS
  2     rmsmax = 5.0
  call sic_r4 (line,0,2,rmsmax,.false.,error)
  if (error) return
  call flag_rm(npt,rmsmax)
  goto 50
  !
  50    return
end
!<FF>
subroutine show (line,comm,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  !
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  character(len=*) :: comm          !
  logical :: error                  !
  ! Global
  include 'setup.inc'
  include 'size.inc'
  include 'data.inc'
  ! Local
  integer :: mvocab
  parameter (mvocab=4)
  integer :: nkey,i,k
  character(len=80) :: chain
  character(len=12) :: vocab(mvocab),argum,keywor
  data vocab /'ELEVATION', 'RMS', 'IGNORED', 'REJECTED'/
  !
  call sic_ke (line,0,1,argum,i,.true.,error)
  if (error) return
  call sic_ambigs('SHOW',argum,keywor,nkey,vocab,mvocab,error)
  if (error) return
  !
  goto (1,2,3,4) nkey
  !
  1     write(6,*) 'Elevation range ',elmin,elmax
  goto 50
  2     write(6,*) 'Maximum rms of measured errors ',rmsmax
  goto 50
  3     k = 1
  write(6,*) 'Ignored data points'
  do i=1,npt
    if (badus(i)) then
      write (chain(k:),'(I4)') scan(i)
      k = k+5
      if (k.gt.70) then
        write(6,*) chain(1:k)
        k = 1
      endif
    endif
  enddo
  if (k.gt.1) write(6,*) chain(1:k)
  goto 50
  !
  4     k = 1
  write(6,*) 'Rejected data points'
  do i=1,npt
    if (badre(i)) then
      write (chain(k:),'(I4)') scan(i)
      k = k+5
      if (k.gt.70) then
        write(6,*) chain(1:k)
        k = 1
      endif
    endif
  enddo
  if (k.gt.1) write(6,*) chain(1:k)
  goto 50
  !
  50    return
end
