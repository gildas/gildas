subroutine gauss(matju,resol,lvar)
  !---------------------------------------------------------------------
  ! POINT
  !	Gauss pivot inversion for a matrix
  !---------------------------------------------------------------------
  ! Global
  include 'size.inc'
  !
  real*8 :: matju(mvar,mvar+1)      !
  real*8 :: resol(mvar)             !
  integer :: lvar                   !
  ! Local
  real*8 :: a(mvar,mvar+1),ck(mvar)
  integer :: loc(mvar),j,l,k,ji
  real*8 ::  amax,f
  !
  do j=1,mvar
    ck(j)=0.0d0
  enddo
  do j=1,mvar
    do l=1,mvar+1
      a(j,l)=matju(j,l)
    enddo
  enddo
  !
  do ji=1,lvar
    amax=0.0d0
    do k=1,lvar
      if (amax.lt.abs(a(k,ji))) then
        if (ck(k).le.0.d0) then
          loc(ji) = k
          amax = abs(a(k,ji))
        endif
      endif
    enddo
    !
    if (amax.le.1.0d-15) then
      write(6,1000)
      do j=1,lvar
        write(6,1001) (matju(j,k),k=1,lvar+1)
      enddo
      stop 'E-POINTING,  No solution'
    endif
    !
    l=loc(ji)
    ck(l)=1.0d0
    do j=1,lvar
      if (l.ne.j) then
        f=-a(j,ji)/a(l,ji)
        do k=ji+1,lvar+1
          a(j,k)=a(j,k)+f*a(l,k)
        enddo
      endif
    enddo
  enddo
  !
  do ji=1,lvar
    l=loc(ji)
    resol(ji)=a(l,lvar+1)/a(l,ji)
  enddo
  !
  1000  format(/,'E-GAUSS,  Dependant equations',//)
  1001  format(7(1x,1pg10.2))
end
