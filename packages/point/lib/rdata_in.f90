subroutine rdata_in (azis,eles,eraz,erel,tiem,np,valpre,   &
     &    icode,scan,jant,stat,source,rms)
  !---------------------------------------------------------------------
  ! POINTING
  !	Read CLIC CLIC\SOLVE POINTING output files
  !---------------------------------------------------------------------
  real*8 :: azis(*)                 !
  real*8 :: eles(*)                 !
  real*8 :: eraz(*)                 !
  real*8 :: erel(*)                 !
  real*8 :: tiem(*)                 !
  integer :: np                     !
  real*8 :: valpre(*)               !
  integer :: icode(*)               !
  integer :: scan(*)                !
  integer :: jant                   !
  character(len=*) :: stat          !
  character(len=*) :: source(*)     !
  real*8 :: rms(*)                  !
  ! Local
  real*8 :: fac,rts,pi,azi,ele,temp,pos,dpos,wid,dwi,err
  ! note: err is actually the source intensity (peak).
  integer :: iaz,iel,icount,ic,jscan,ier,iobs,iscan,jcode,jndex
  integer :: iant,ista,jsta
  character(len=12) :: name
  character(len=3) :: arm(3)
  data arm/'N','W','E'/
  !
  pi = acos(-1.0d0)
  fac = pi/180.d0
  rts = 3600.0d0/fac
  !
  ! Skip header lines (1)
  read(2,*)
  !
  iaz = 0
  iel = 0
  icount = 0
  write(6,*) 'I-POINTING,  Reading INTER data'
  ic = 0
  jscan=-5
  jsta = 0
  !
  do while (.true.)
    read(2,*,iostat=ier,end=110) iobs,iscan,jcode,azi,ele,   &
     &      temp,iant,ista,pos,dpos,wid,dwi,err,name
    if (ier.ne.0) then
      write(6,*) 'W-RDATA_RA,  Read error in input file'
      goto 30
    endif
    if (iant.ne.jant) goto 30
    if (jsta.ne.0) then
      if (ista.ne.jsta) then
        write(6,*) 'E-POINT, Station change at ',iscan
        goto 30
      endif
    else
      jsta = ista
    endif
    !
    ! check whether scan number change
    if (iscan.ne.jscan) then
      ! check whether first scan or last scan complete
      if (iaz.ne.0 .and. iel.ne.0) then
        icount=icount+2
      elseif (jscan.ne.-5) then
        write(6,*) 'W-RDATA_RA,  Scan ignored',   &
     &          jscan
      endif
      jscan = iscan
      iaz=0
      iel=0
    endif
    !
    ! Azimuth drift
    if (jcode.eq.1) then
      if (iaz.eq.0) then
        iaz=1
        jndex=icount+iaz+iel
        azis(jndex)=azi
        eles(jndex)=ele
        tiem(jndex)=temp
        eraz(jndex)=pos
        erel(jndex)=0.
        icode(jndex)=jcode
        scan(jndex) =jscan
        rms(jndex) = dpos
        source(jndex) = name
      endif
      !
      ! Elevation drift
    elseif (jcode.eq.0) then
      if (iel.eq.0) then
        iel=1
        jndex=icount+iaz+iel
        azis(jndex)=azi
        eles(jndex)=ele
        tiem(jndex)=temp
        erel(jndex)=pos
        eraz(jndex)=0.
        icode(jndex)=jcode
        scan(jndex) =jscan
        rms(jndex) = dpos
        source(jndex) = name
      endif
    else
      write(6,*) 'W-RDATA_RA,  Scan ',iscan,   &
     &        ' is neither Azimuth nor Elevation'
    endif
    30    enddo
    110   np = icount
    ! Last scan complete
    if (iaz.eq.1 .and.iel.eq.1) np=np+2
    write(stat,'(1X,I2.2)') mod(jsta,100)
    jsta = jsta/100
    if (jsta.gt.0 .and. jsta.le.3) stat(1:1) = arm(jsta)
  end
