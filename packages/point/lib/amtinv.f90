subroutine amtinv(cmat,npar)
  !---------------------------------------------------------------------
  ! POINT
  ! 	Matrix inversion through Gauss maximum pivot
  !	Dimensionned for size MVAR, MVAR
  ! J.Cernicharo, S.Guilloteau
  !---------------------------------------------------------------------
  ! Global
  include 'size.inc'
  !
  real*8 :: cmat(mvar,mvar)         !
  integer :: npar                   !
  ! Local
  real*8 :: amat(mvar,mvar+1),resol(mvar),amax
  integer :: j,l
  !
  if (npar.eq.1) then
    cmat(1,1)=1./cmat(1,1)
    return
  endif
  amax=-1.0d10
  do j=1,npar
    do l=1,npar
      amax = max(cmat(j,l),amax)
    enddo
  enddo
  do j=1,npar
    do l=1,npar
      amat(j,l)=cmat(j,l)/amax
    enddo
  enddo
  do j=1,npar+1
    do l=1,npar
      amat(l,npar+1)=0.0d0
    enddo
    amat(j,npar+1)=1.0d0
    call gauss(amat,resol,npar)
    do l=1,npar
      cmat(l,j)=resol(l)/amax
    enddo
  enddo
end
