function funpnl(xv,a)
  !---------------------------------------------------------------------
  ! POINT
  !	Compute the pointing function
  ! V-2.0
  !	Changed sign of excentricity corrections
  !---------------------------------------------------------------------
  ! Global
  include 'size.inc'
  !
  real*8 :: funpnl                  !
  real*8 :: xv(3)                   !
  real*8 :: a(mvar)                 !
  ! Local
  real*8 :: az,el,cosel,sinel,tanel,cosaz,sinaz
  !
  az=xv(1)
  el=xv(2)
  cosel=cos(el)
  sinel=sin(el)
  cosaz=cos(az)
  sinaz=sin(az)
  if (xv(3).eq.0) then
    funpnl=-a( 1)*cosel   &
     &      +cosel*asin(max(-1d0,min(1d0,-a(3)/cosel)))   &
     &      -a( 4)*cosaz*sinel   &
     &      +a( 5)*sinaz*sinel   &
     &      +a( 6)*sinel   &
     &      -a(11)*sinaz*cosel   &
     &      -a(12)*cosaz*cosel
    if (kvar.le.12) return
    funpnl = funpnl   &
     &      +a(13)*cosel*sin(2.d0*az)   &
     &      +a(14)*cosel*cos(2.d0*az)
  else
    tanel = sinel/cosel
    funpnl=-a( 2)   &
     &      -  el+   &
     &      asin(max(-1d0,min(1d0,sinel/sqrt(1d0-a(3)**2))))   &
     &      +a( 4)*sinaz   &
     &      +a( 5)*cosaz   &
     &      +a( 7)/tanel   &
     &      +a( 8)/tanel**3   &
     &      -a( 9)*sinel   &
     &      -a(10)*cosel
    if (kvar.le.12) return
    funpnl = funpnl   &
     &      +a(15)*sin(2.d0*el)   &
     &      +a(16)*cos(2.d0*el)
  endif
end
