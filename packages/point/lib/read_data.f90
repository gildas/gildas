subroutine read_data (line,comm,error)
  use gkernel_interfaces
  character(len=*) :: line          !
  character(len=*) :: comm          !
  logical :: error                  !
  ! Global
  include 'setup.inc'
  include 'size.inc'
  include 'data.inc'
  ! Local
  real*8 :: ftie(nmax)            ! Work array for time
  integer :: iscan(nmax)          ! Work array for scan numbers
  integer :: icode(nmax)          ! Work array for code (1 Az - 0 El)
  real*8 :: frms(nmax)
  character(len=12) :: sou(nmax)       ! Work array for sources
  integer :: i1,l1,l,n,ier,jant
  character(len=4) :: stat
  real*8 :: pi,fac,sfac,rts
  logical :: refrac, no_data, no_file
  parameter (pi=3.14159265358979323846d0)
  data no_data /.true./, no_file /.true./
  !
  if (.not.sic_varexist('nodata')) then
     call sic_def_logi ('nodata',no_data,.true.,error) 
     call sic_def_logi ('nofile',no_file,.true.,error)
  endif
  sfac = pi/180d0/3600d0
  fac  = 180d0/pi
  rts  = 1.d0/sfac
  !
  fitted = .false.
  call sic_i4 (line,0,1,jant,.true.,error)
  if (error) return
  !
  ier = sic_open (2,cfile,'OLD',.true.)
  if (ier.ne.0) then
   n = min (lenc(cfile),50)
    write(6,*) 'E-READ_DATA,  Error opening file '//cfile(1:n)
    call putios ('E-READ_DATA,  ',ier)
    goto 99
  endif
  no_file = .false.
  !
  if (ctype.eq.'O') then
    call rdata_op(azis,eles,eraz,erel,tiem,ndata,valpre,   &
     &      jant,stat,refrac)
    call set_optical (refrac)
    npt = 2*ndata
    i1 = ndata
    do l=1,ndata
      x(1,l)=azis(l)/fac
      x(2,l)=eles(l)/fac
      x(3,l)=0.
      i1=i1+1
      x(1,i1)=azis(l)/fac
      x(2,i1)=eles(l)/fac
      x(3,i1)=1.
      fobs(l)=eraz(l)*sfac
      fobs(i1)=erel(l)*sfac
      scan(l)=l
      scan(i1)=l
      source(l) = ' '
      source(i1) = ' '
      rms(l) = 0.0
      rms(i1) = 0.0
      azis(i1) = azis(l)
      eles(i1) = eles(l)
      tiem(i1) = tiem(l)
    enddo
  elseif (ctype.eq.'R' .or. ctype.eq.'I') then
    if (ctype.eq.'R') then
      call rdata_ra (azis,eles,eraz,erel,ftie,ndata,valpre,   &
     &        icode,iscan,jant,stat,sou,frms)
    else
      call rdata_in (azis,eles,eraz,erel,ftie,ndata,valpre,   &
     &        icode,iscan,jant,stat,sou,frms)
    endif
    ndata=ndata/2
    npt = 2*ndata
    call set_radio(jant)
    i1=ndata
    l1=0
    do l=1,npt
      if (icode(l).eq.1) then
        l1=l1+1
        x(1,l1)=azis(l)/fac    !radian
        x(2,l1)=eles(l)/fac    !radian
        fobs(l1)=eraz(l)*sfac  !radian
        tiem(l1)=ftie(l)
        x(3,l1)=0.
        scan(l1) = iscan(l)
        source(l1) = sou(l)
        rms(l1) = frms(l)
      else
        i1=i1+1
        x(1,i1)=azis(l)/fac
        x(2,i1)=eles(l)/fac
        x(3,i1)=1.
        fobs(i1)=erel(l)*sfac
        scan(i1)=iscan(l)
        tiem(i1)=ftie(l)
        source(i1) = sou(l)
        rms(i1) = frms(l)
      endif
    enddo
    !
    ! Reorder AZ and EL values
    do l=1,npt
      azis(l) = x(1,l)*fac
      eles(l) = x(2,l)*fac
    enddo
  endif
  do l=1,npt
    badre(l) = .false.
    badus(l) = .false.
    badti(l) = .false.
  enddo
  call flag_el(npt,elmin,elmax)
  call flag_rm(npt,rmsmax)
  close (unit=2)
  no_data = .false.
  if (ndata.eq.0) then
    write (6,*) 'E-READ,  No data in file'
    no_data = .true.
    return
  endif
  call get_good (npt,kp)
  if (jant.lt.10) then
     write (stat_ant,'(a,i1,a)') 'A',jant,' on '//stat
  else
     write (stat_ant,'(a,i2,a)') 'A',jant,' on '//stat
  endif
  return
  99    close (unit=2)
  no_file = .true.
end
