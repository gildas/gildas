subroutine run_point (line,comm,error)
  character(len=*) :: line          !
  character(len=*) :: comm          !
  logical :: error                  !
  !
  if (comm.eq.'IGNORE') then
    call ignore (line,comm,error)
  elseif (comm.eq.'INCLUDE') then
    call include (line,comm,error)
  elseif (comm.eq.'PARAMETER') then
    call param (line,comm,error)
  elseif (comm.eq.'PLOT') then
    call plot_result (line,comm,error)
  elseif (comm.eq.'PRINT') then
    call print (line,comm,error)
  elseif (comm.eq.'FILE') then
    call input_file (line,comm,error)
  elseif (comm.eq.'LIST') then
    call listpo (line,error)
  elseif (comm.eq.'READ') then
    call read_data (line,comm,error)
  elseif (comm.eq.'REJECT') then
    call reject (line,comm,error)
  elseif (comm.eq.'SET') then
    call set  (line,comm,error)
  elseif (comm.eq.'SHOW') then
    call show (line,comm,error)
  elseif (comm.eq.'SOLVE') then
    call solve (line,comm,error)
  elseif (comm.eq.'FLAG') then
    call flag(line,comm,error)
  else
    write(6,*) 'E-POINT,  No code for ',comm
  endif
end
!<FF>
subroutine load_point
  ! Global
  logical, external :: gr_error
  external :: run_point
  ! Local
  integer :: mpoint
  logical :: error
  !
  parameter (mpoint=25)
  character(len=12) :: cpoint(mpoint)
  data cpoint/   &
     &    ' IGNORE', '/SCAN', '/SOURCE', '/TIME',   &
     &    ' INCLUDE', '/SCAN', '/SOURCE', '/TIME',   &
     &    ' FILE', ' FLAG', ' LIST', '/OUTPUT',   &
     &    ' PARAMETER', '/FREE', ' PLOT', '/BAD',   &
     &    ' PRINT', '/OUTPUT', '/HEADER', '/30M',   &
     &    ' READ', ' REJECT',   &
     &    ' SET', ' SHOW', ' SOLVE'/
  !
  error = gr_error()
  call sic_begin('POINT','GAG_HELP_POINT',mpoint,cpoint,   &
     &    '1.0-00  15-JAN-1991  S.G.',run_point,gr_error)
  call gr_exec1 ('SET MARKER 4 0 .15')
  call gr_exec1 ('SET EXPAND 0.8')
end
