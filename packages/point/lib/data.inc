!
      REAL*8 X(NCOL,2*NMAX)          ! Function to fit
      REAL*8 ERAZ(NMAX)              ! Azimuth error
      REAL*8 EREL(NMAX)              ! Elevation error
      REAL*8 AZIS(NMAX)              ! Azimuth values
      REAL*8 ELES(NMAX)              ! Elevation values
      REAL*8 FOBS(2*NMAX)            ! Observed values (Az or El)
      REAL*8 FFIT(2*NMAX)            ! Fitted values (Az or El)
      REAL*8 RESD(2*NMAX)            ! Residuals
      REAL*8 RESAZ(NMAX)             ! Residual azimuth
      REAL*8 RESEL(NMAX)             ! Residual elevations
      REAL*8 FITAZ(NMAX)             ! Fitted values azimuth
      REAL*8 FITEL(NMAX)             ! Fitted values elevation
      REAL*8 COV(MVAR,MVAR)          ! Covariance matrix
      REAL*8 SIGCO(MVAR)             ! Sigma on parameters
      REAL*8 COEF(MVAR)              ! Parameters to fit
      REAL*8 VALPRE(MVAR)            ! Previous parameter values
      REAL*8 TIEM(NMAX)              ! Observing time
      REAL*8 RMS(NMAX)               ! Rms error on measurement
!
      INTEGER SCAN(NMAX)             ! Scan number
      INTEGER IFIXED(MVAR)           ! Fixed parameters
      INTEGER IPAR(MVAR)             ! Variable parameters
!
      LOGICAL BADUS(NMAX)            ! User declared bad data
      LOGICAL BADEL(NMAX)            ! Elevation declared bad data
      LOGICAL BADRM(NMAX)            ! RMS declared bad data
      LOGICAL BADRE(NMAX)            ! Fit rejected bad data
      LOGICAL BADTI(NMAX)            ! Time declared bad
      LOGICAL GOOD(NMAX)             ! Good data (.AND. of all other .NOT.BADs)
      LOGICAL FREE(MVAR)
!
      CHARACTER*36 NOMPAR(MVAR)
      CHARACTER*12 SOURCE(NMAX)
      COMMON /DATA/ X, ERAZ, EREL, AZIS, ELES, FOBS, FFIT,              &
     &RESD, RESAZ, RESEL, FITAZ, FITEL, COV, SIGCO, COEF,               &
     &VALPRE, TIEM, RMS,                                                &
     &SCAN, IFIXED, IPAR,                                               &
     &BADUS, BADEL, BADRM, BADRE, BADTI, GOOD, FREE
      COMMON /CDATA/ SOURCE, NOMPAR
