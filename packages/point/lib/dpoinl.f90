subroutine dpoinl(xv,a,deriv)
  !---------------------------------------------------------------------
  ! POINT
  !	Compute the derivatives of the pointing function
  !	Dimensionned for MVAR or KVAR
  ! V2.0
  !	Changed sign of excentricity corrections
  !---------------------------------------------------------------------
  ! Global
  include 'size.inc'
  !
  real*8 :: xv(3)                   !
  real*8 :: a(mvar)                 !
  real*8 :: deriv(mvar)             !
  ! Local
  real*8 :: az,el,cosel,sinel,cosaz,sinaz
  !
  az=xv(1)
  el=xv(2)
  cosel=cos(el)
  sinel=sin(el)
  cosaz=cos(az)
  sinaz=sin(az)
  if (xv(3).eq.0) then
    deriv( 1)=-cosel
    deriv( 2)= 0
    deriv( 3)=-1.0/sqrt(max(1d-37,1.0d0-(a(3)/cosel)**2))
    deriv( 4)= -cosaz*sinel
    deriv( 5)= sinaz*sinel
    deriv( 6)= sinel
    deriv( 7)= 0
    deriv( 8)= 0
    deriv( 9)= 0
    deriv(10)= 0
    deriv(11)= -sinaz*cosel
    deriv(12)= -cosaz*cosel
    if (kvar.le.12) return
    deriv(13)= sin(2.d0*az)*cosel
    deriv(14)= cos(2.d0*az)*cosel
    deriv(15)= 0
    deriv(16)= 0
  else
    deriv( 1)= 0
    deriv( 2)=-1
    deriv( 3)= -sinel*a(3)/   &
     &      sqrt(max(1d-30,1.0d0-sinel**2/(1.0d0-a(3)**2)))/   &
     &      sqrt(max(1d-30,(1.0d0-a(3)**2)**3))
    deriv( 4)=sinaz
    deriv( 5)=+cosaz
    deriv( 6)= 0
    deriv( 7)= 1.d0/tan(el)
    deriv( 8)= deriv(7)**3
    deriv( 9)= -sinel
    deriv(10)= -cosel
    deriv(11)= 0
    deriv(12)= 0
    if (kvar.le.12) return
    deriv(13)= 0
    deriv(14)= 0
    deriv(15)= sin(2.d0*el)
    deriv(16)= cos(2.d0*el)
  endif
end
