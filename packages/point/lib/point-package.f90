!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage the POINT package
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine point_pack_set(pack)
  use gpack_def
  use gkernel_interfaces
  !----------------------------------------------------------------------
  ! Public package definition routine.
  ! It defines package methods and dependencies to other packages.
  !----------------------------------------------------------------------
  type(gpack_info_t), intent(out) :: pack ! Package info structure
  !
  external :: point_pack_init
  external :: point_pack_clean
  !
  pack%name='point'
  pack%ext='.point'
  pack%depend(1:1) = (/ locwrd(greg_pack_set) /)
  pack%init=locwrd(point_pack_init)
  pack%clean=locwrd(point_pack_clean)
  !
end subroutine point_pack_set
!
subroutine point_pack_init(gpack_id,error)
  use sic_def
  !----------------------------------------------------------------------
  ! Private routine to set the POINT environment.
  !----------------------------------------------------------------------
  integer :: gpack_id
  logical :: error
  !
  ! Set libraries id's
  !call point_message_set_id(gpack_id)
  !
  ! Local language
  call load_point
  call sic_setsymbol('set','point'//backslash//'set',error)
  call sic_setsymbol('sh*ow','point'//backslash//'show',error)
  !
end subroutine point_pack_init
!
subroutine point_pack_clean(error)
  !----------------------------------------------------------------------
  ! Private routine to clean the POINT environment
  ! (e.g. Remove temporary buffers, unregister languages...)
  !----------------------------------------------------------------------
  logical :: error
  !
end subroutine point_pack_clean
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
