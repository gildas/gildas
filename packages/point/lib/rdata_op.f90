subroutine rdata_op(azis,eles,eraz,erel,tiem,np,valpre,   &
     &    jant,stat,refrac)
  !---------------------------------------------------------------------
  ! POINTING
  !	Read optical data files.
  !	List directed format can be used, except for sign...
  !---------------------------------------------------------------------
  real*8 :: azis(*)                 !
  real*8 :: eles(*)                 !
  real*8 :: eraz(*)                 !
  real*8 :: erel(*)                 !
  real*8 :: tiem(*)                 !
  integer :: np                     !
  real*8 :: valpre(*)               !
  integer :: jant                   !
  character(len=*) :: stat          !
  logical :: refrac                 !
  ! Local
  real*8 :: as(3),ds(3),ar(3),dr(3),lst(3),x(3)
  real*8 :: fac,rts,pi,tsd,cor1,cor2,funpnl
  real*8 :: alp,alpr,del,delr,tiemm,era,ere
  real*8 :: ref(3),tanel,tane2,rfra
  !
  real :: iaz, iel, coh, cov, mve, mvn
  real :: npe, azec, azes, elec, elsi, refr
  integer :: ic,l,iant, i
  external :: funpnl
  character(len=132) :: chain
  character(len=3) :: station(4)
  !
  pi = acos(-1.0d0)
  fac = pi/180.d0
  rts = 3600.0d0/fac
  refrac = .false.
  !
  read(2,'(A)') chain          ! Antenna and Station
  call sic_upper(chain)
  !
  ! Single-dish format
  write(6,*) 'I-POINTING,  Reading optical data file'
  if (index(chain,'ANT').ne.0) then
    read(2,*)                  ! Plateau de Bure coordinates
    read(2,*)                  ! IAZ IEL
    read(2,'(1X,6F9.0)') iaz, iel, coh, cov, mve, mvn
    read(2,*)                  ! etc...
    read(2,'(1X,6F9.0)') npe, azec, azes, elec, elsi, refr
    read(2,*)                  ! etc...
    ic=0
    do l=1,1000
      read (2,'(A)',err=100,end=10) chain
      read (chain,*,err=100) as,ds,ar,dr,lst
      alp=dabs(as(1))+dabs(as(2)/60.)+dabs(as(3)/3600.)
      alp=alp*fac
      if (index(chain(1:14),'-').ne.0) alp = -alp
      del=dabs(ds(1))+dabs(ds(2)/60.)+dabs(ds(3)/3600.)
      del=del*fac
      if (index(chain(16:28),'-').ne.0) del = -del
      alpr=dabs(ar(1))+dabs(ar(2)/60.)+dabs(ar(3)/3600.)
      alpr=alpr*fac
      if (index(chain(30:42),'-').ne.0) alpr = -alpr
      delr=dabs(dr(1))+dabs(dr(2)/60.)+dabs(dr(3)/3600.)
      delr=delr*fac
      if (index(chain(44:56),'-').ne.0) delr = -delr
      tsd = lst(1)+lst(2)/60.+lst(3)/3600.
      if (tsd.gt.24) then
        tsd=tsd-24
      elseif (tsd.lt.0) then
        tsd=tsd+24
      endif
      tiemm=tsd
      !
      ic=ic+1
      azis(ic)=alp
      eles(ic)=del
      tiem(ic)=tiemm
      era=-(alp-alpr)*dcos(del)
      ere=-(del-delr)
      x(1)=azis(ic)
      x(2)=eles(ic)
      x(3)=0
      cor1=funpnl(x,valpre)
      x(3)=1
      cor2=funpnl(x,valpre)
      eraz(ic)= (era+cor1)*rts
      erel(ic)= (ere+cor2)*rts
      azis(ic) = azis(ic)/fac
      eles(ic) = eles(ic)/fac
    enddo
    valpre(1) = iaz/rts
    valpre(2) = (iel+cov)/rts
    valpre(3) = coh/rts
    valpre(4) = mve/rts
    valpre(5) = mvn/rts
    valpre(6) = npe/rts
    !	    VALPRE(7) = REFR
    !	    VALPRE(8) = REF2
    valpre(9) = elsi/rts
    valpre(10) = elec/rts
    valpre(11) = azes/rts
    valpre(12) = azec/rts
  else
    read(2,*)                  ! Plateau de Bure coordinates
    do i=1,4
      read(2,'(A)') chain
      station(i) = chain(12:14)
      read(2,*)                ! IAZ IEL
      read(2,*)                ! etc...
      read(2,*)                ! etc...
      read(2,*)                ! etc...
    enddo
    iant = 0
    do i=1,4
      if (station(i).ne.'0  ') then
        iant = iant+1
      endif
    enddo
    if (iant.eq.0) then
      write(6,*) 'E-POINTING,  No antenna on station'
      return
    elseif (iant.eq.1) then
      do i=1,4
        if (station(i).ne.'0  '.and.station(i).ne.'$$$') then
          if (jant.ne.0) then
            if (jant.ne.i) then
              write(6,'(1X,A,I1,A)')   &
     &                'W-POINTING,  Antenna ',jant,   &
     &                'not on station'
            endif
          endif
          jant = i
        endif
      enddo
      write(6,'(1X,A,I1,A,A)') 'I-POINTING,  Selected Antenna ',   &
     &        jant,' on station ',station(jant)
    else
      do i=1,4
        if (i.eq.jant) then
          if (station(i).ne.'0  ') then
            write(6,'(1X,A,I1,A,A)')   &
     &              'I-POINTING,  Antenna ',   &
     &              i,' on station ',station(i)
          else
            write(6,'(1X,A,I1,A,A)')   &
     &              'E-POINTING,  Antenna ',   &
     &              i,' not on station '
            return
          endif
        endif
      enddo
    endif
    stat = station(jant)
    !
    ic=0
    do l=1,1000
      read (2,'(A)',err=100,end=10) chain
      read (chain,*,err=100) iant,as,ds,ar,dr,lst
      if (l.eq.1 .and. chain(74:).ne.' ') refrac = .true.
      ! Select only the right antenna
      if (iant.eq.jant) then
        alp=dabs(as(1))+dabs(as(2)/60.)+dabs(as(3)/3600.)
        alp=alp*fac
        if (index(chain(3:16),'-').ne.0) alp = -alp
        del=dabs(ds(1))+dabs(ds(2)/60.)+dabs(ds(3)/3600.)
        del=del*fac
        if (index(chain(18:30),'-').ne.0) del = -del
        alpr=dabs(ar(1))+dabs(ar(2)/60.)+dabs(ar(3)/3600.)
        alpr=alpr*fac
        if (index(chain(32:44),'-').ne.0) alpr = -alpr
        delr=dabs(dr(1))+dabs(dr(2)/60.)+dabs(dr(3)/3600.)
        delr=delr*fac
        if (index(chain(46:58),'-').ne.0) delr = -delr
        tsd = lst(1)+lst(2)/60.+lst(3)/3600.
        if (tsd.gt.24) then
          tsd=tsd-24
        elseif (tsd.lt.0) then
          tsd=tsd+24
        endif
        tiemm=tsd
        !
        ! Refraction
        if (refrac) then
          read(chain(74:),*,end=101) ref
          tanel = 1.0/tan(del)
          tane2 = tanel**2
          rfra = - tanel*   &
     &            (((ref(3)*tane2)+ref(2))*tane2+ref(1))
          delr = delr+rfra
        endif
        !
        ic=ic+1
        eles(ic)=del
        tiem(ic)=tiemm
        era=-(alp-alpr)
        ! Beware of Azimut determination
        do while (era.lt.-pi)
          era = era+2d0*pi
        enddo
        do while (era.gt.pi)
          era = era-2d0*pi
        enddo
        era= era*cos(del)
        ere=-(del-delr)
        if (alp.le.-pi) then
          alp = alp+2d0*pi
        elseif (alp.gt.pi) then
          alp = alp-2d0*pi
        endif
        azis(ic)=alp
        x(1)=azis(ic)
        x(2)=eles(ic)
        x(3)=0
        cor1=funpnl(x,valpre)
        x(3)=1
        cor2=funpnl(x,valpre)
        eraz(ic)= (era+cor1)*rts
        erel(ic)= (ere+cor2)*rts
        azis(ic) = azis(ic)/fac
        eles(ic) = eles(ic)/fac
      endif
    enddo
  endif
  !
  10    np = ic
  return
  !
  101   write(6,*) 'E-POINTING,  Inconsistent refraction log'
  return
  100   write(6,*) 'E-POINTING,  Error at line ',l,' of data file'
  return
end
