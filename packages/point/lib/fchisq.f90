function fchisq(x,n,nfree,xfit,good)
  !---------------------------------------------------------------------
  !	calcul de la dispersion quadratique moyenne
  !---------------------------------------------------------------------
  real*8 :: fchisq                  !
  real*8 :: x(1)                    !
  integer :: n                      !
  integer :: nfree                  !
  real*8 :: xfit(1)                 !
  logical :: good(1)                !
  ! Local
  real*8 :: res
  integer :: j,kp
  !
  fchisq=0.
  kp = 0
  do j=1,n
    if (good(j)) then
      res = x(j)-xfit(j)
      fchisq=fchisq+res**2
      kp = kp+1
    endif
  enddo
  fchisq = fchisq/(kp-nfree)
end
