subroutine header_ra
  !---------------------------------------------------------------------
  ! POINTING
  !	Read CLASS ANALYSE\PRINT POINTING output files
  !---------------------------------------------------------------------
  character(len=80) :: chain
  !
  ! Skip header lines (4)
  read(2,*)
  read(2,*)
  read(2,*)
  read(2,'(A)') chain
end
!<FF>
subroutine header_op
  !---------------------------------------------------------------------
  ! POINTING
  !	Read optical data files.
  !---------------------------------------------------------------------
  ! Global
  integer :: lenc
  ! Local
  character(len=80) :: chain
  character(len=3) :: station(4)
  integer :: i,iant
  !
  read(2,'(A)') chain          ! Antenna and Station
  call sic_upper (chain)
  !
  ! Single-dish format
  write(6,*) 'I-POINTING,  Reading optical data file'
  if (index(chain,'ANT').ne.0) then
    write(6,'(1x,a,a)') 'I-POINTING,  Found single antenna data '   &
     &      ,chain(1:lenc(chain))
  else
    write(6,'(1x,a,a)') 'I-POINTING,  Found multi-antenna data '   &
     &      ,chain(1:lenc(chain))
    read(2,*)                  ! Plateau de Bure coordinates
    do i=1,4
      read(2,'(A)') chain
      station(i) = chain(12:14)
      read(2,*)                ! IAZ IEL
      read(2,*)                ! etc...
      read(2,*)                ! etc...
      read(2,*)                ! etc...
    enddo
    iant = 0
    do i=1,4
      if (station(i).ne.'0  ') then
        iant = iant+1
      endif
    enddo
    if (iant.eq.0) then
      write(6,*) 'E-POINTING,  No antenna on station'
    else
      do i=1,4
        if (station(i).ne.'0  ') then
          write(6,'(1X,A,I1,A,A)') 'I-POINTING,  Antenna ',   &
     &            i,' on station ',station(i)
        endif
      enddo
    endif
  endif
end
!<FF>
subroutine header_in
  !---------------------------------------------------------------------
  ! POINTING
  !	Read CLIC SOLVE POINTING /OUTPUT output files
  !---------------------------------------------------------------------
end
