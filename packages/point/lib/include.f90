subroutine include (line,comm,error)
  use gkernel_interfaces
  use point_interfaces, except_this=>include
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   !
  character(len=*)                :: comm   !
  logical,          intent(inout) :: error  !
  ! Global
  include 'setup.inc'
  ! Local
  character(len=*), parameter :: rname='INCLUDE'
  character(len=12) :: argum
  real(kind=4) :: tmin,tmax
  integer(kind=4) :: na
  !
  if (sic_present(1,0)) then  ! /SCAN
    call sic_ch (line,1,1,argum,na,.false.,error)
    if (error) return
    if (argum.eq.'*') then
      call get_all(npt)
      fitted = .false.
    else
      call do_list (rname,line,1,error,get_scans)
      fitted = .false.
    endif
  endif
  !
  if (sic_present(2,0)) then  ! /SOURCE
    call sic_ch (line,2,1,argum,na,.false.,error)
    if (error) return
    if (argum.eq.'*') then
      call get_all(npt)
      fitted = .false.
    else
      call do_list (rname,line,2,error,get_sources)
      fitted = .false.
    endif
  endif
  !
  if (sic_present(3,0)) then  ! /TIME
    call sic_r4 (line,3,2,tmax,.true.,error)
    if (error) return
    call sic_r4 (line,3,1,tmin,.true.,error)
    if (error) return
    call get_ti (npt,tmin,tmax)
    fitted = .false.
  endif
  !
end subroutine include
!
subroutine ignore (line,comm,error)
  use gkernel_interfaces
  use point_interfaces, except_this=>ignore
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   !
  character(len=*)                :: comm   !
  logical,          intent(inout) :: error  !
  ! Global
  include 'setup.inc'
  ! Local
  character(len=*), parameter :: rname='IGNORE'
  real(kind=4) :: tmin,tmax
  !
  if (sic_present(1,0)) then  ! /SCAN
    call do_list (rname,line,1,error,flag_scans)
    fitted = .false.
  endif
  !
  if (sic_present(2,0)) then  ! /SOURCE
    call do_list (rname,line,2,error,flag_sources)
    fitted = .false.
  endif
  !
  if (sic_present(3,0)) then  ! /TIME
    call sic_r4 (line,3,2,tmax,.true.,error)
    if (error) return
    call sic_r4 (line,3,1,tmin,.true.,error)
    if (error) return
    call flag_ti (npt,tmin,tmax)
    fitted = .false.
  endif
  !
end subroutine ignore
!
subroutine reject (line,comm,error)
  use gkernel_interfaces
  use point_interfaces, except_this=>reject
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   !
  character(len=*)                :: comm   !
  logical,          intent(inout) :: error  !
  ! Global
  include 'setup.inc'
  ! Local
  real(kind=4) :: rmserr
  !
  call sic_r4 (line,0,1,rmserr,.true.,error)
  if (error) return
  fitted = .false.
  call flag_re (ndata,rmserr)
end subroutine reject
!
subroutine do_list (rname,line,iopt,error,func)
  use gkernel_interfaces
  use gkernel_types
  use point_interfaces, except_this=>do_list
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: rname  !
  character(len=*), intent(in)  :: line   !
  integer(kind=4),  intent(in)  :: iopt   !
  logical,          intent(out) :: error  !
  external                      :: func   !
  ! Local
  integer(kind=4) :: narg,first,last
  integer(kind=4), parameter :: mloop=1
  type(sic_listi4_t) :: list
  integer(kind=4) :: loop(3)
  !
  error = .false.
  narg = sic_narg(iopt)
  if (narg.eq.0)  return
  !
  first = sic_start(iopt,1)
  last = sic_end(iopt,narg)
  call sic_parse_listi4(rname,line(first:last),list,mloop,error)
  if (error)  return
  !
  loop(1) = list%i1(1)
  loop(2) = list%i2(1)
  loop(3) = list%i3(1)
  call func(loop)
  !
end subroutine do_list
!
subroutine get_scans (loop)
  use point_interfaces, except_this=>get_scans
  !---------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  !---------------------------------------------------------------------
  integer(kind=4), intent(in) :: loop(3)  !
  ! Global
  include 'setup.inc'
  ! Local
  integer(kind=4) :: i
  !
  do i=loop(1),loop(2),loop(3)
    call get_scan (npt,i)
  enddo
end subroutine get_scans
!
subroutine get_sources (loop)
  use point_interfaces, except_this=>get_sources
  !---------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  !---------------------------------------------------------------------
  integer(kind=4), intent(in) :: loop(3)  !
  ! Global
  include 'setup.inc'
  ! Local
  integer(kind=4) :: i
  !
  do i=loop(1),loop(2),loop(3)
    call get_source (npt,i)
  enddo
end subroutine get_sources
!
subroutine flag_scans (loop)
  use point_interfaces, except_this=>flag_scans
  !---------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  !---------------------------------------------------------------------
  integer(kind=4), intent(in) :: loop(3)  !
  ! Global
  include 'setup.inc'
  ! Local
  integer(kind=4) :: i
  !
  do i=loop(1),loop(2),loop(3)
    call flag_scan (npt,i)
  enddo
end subroutine flag_scans
!
subroutine flag_sources (loop)
  use point_interfaces, except_this=>flag_sources
  !---------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  !---------------------------------------------------------------------
  integer(kind=4), intent(in) :: loop(3)  !
  ! Global
  include 'setup.inc'
  ! Local
  integer(kind=4) :: i
  !
  do i=loop(1),loop(2),loop(3)
    call flag_source (npt,i)
  enddo
end subroutine flag_sources
