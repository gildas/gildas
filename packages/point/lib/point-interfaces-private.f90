module point_interfaces_private
  interface
    subroutine include (line,comm,error)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      character(len=*)                :: comm   !
      logical,          intent(inout) :: error  !
    end subroutine include
  end interface
  !
  interface
    subroutine ignore (line,comm,error)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      character(len=*)                :: comm   !
      logical,          intent(inout) :: error  !
    end subroutine ignore
  end interface
  !
  interface
    subroutine reject (line,comm,error)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      character(len=*)                :: comm   !
      logical,          intent(inout) :: error  !
    end subroutine reject
  end interface
  !
  interface
    subroutine do_list (rname,line,iopt,error,func)
      use gkernel_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: rname  !
      character(len=*), intent(in)  :: line   !
      integer(kind=4),  intent(in)  :: iopt   !
      logical,          intent(out) :: error  !
      external                      :: func   !
    end subroutine do_list
  end interface
  !
  interface
    subroutine get_scans (loop)
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: loop(3)  !
    end subroutine get_scans
  end interface
  !
  interface
    subroutine get_sources (loop)
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: loop(3)  !
    end subroutine get_sources
  end interface
  !
  interface
    subroutine flag_scans (loop)
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: loop(3)  !
    end subroutine flag_scans
  end interface
  !
  interface
    subroutine flag_sources (loop)
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: loop(3)  !
    end subroutine flag_sources
  end interface
  !
end module point_interfaces_private
