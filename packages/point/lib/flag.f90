subroutine flag_ti(n,timin,timax)
  integer :: n                      !
  real :: timin                     !
  real :: timax                     !
  ! Global
  include 'size.inc'
  include 'data.inc'
  ! Local
  integer :: i
  !
  do i=1,n
    if (tiem(i).ge.timin .and. tiem(i).le.timax) then
      badti(i) = .true.
    endif
  enddo
end
!<FF>
subroutine get_ti(n,timin,timax)
  integer :: n                      !
  real :: timin                     !
  real :: timax                     !
  ! Global
  include 'size.inc'
  include 'data.inc'
  ! Local
  integer :: i
  !
  do i=1,n
    if (tiem(i).ge.timin .and. tiem(i).le.timax) then
      badti(i) = .false.
    endif
  enddo
end
!<FF>
subroutine get_all(n)
  integer :: n                      !
  ! Global
  include 'size.inc'
  include 'data.inc'
  ! Local
  integer :: i
  !
  do i=1,n
    badus(i) = .false.
  enddo
end
!<FF>
subroutine flag_el(n,elmin,elmax)
  integer :: n                      !
  real :: elmin                     !
  real :: elmax                     !
  ! Global
  include 'size.inc'
  include 'data.inc'
  ! Local
  integer :: i
  !
  do i=1,n
    if (eles(i).ge.elmin .and. eles(i).le.elmax) then
      badel(i) = .false.
    else
      badel(i) = .true.
    endif
  enddo
end
!<FF>
subroutine flag_rm (n,rmsmax)
  integer :: n                      !
  real :: rmsmax                    !
  ! Global
  include 'size.inc'
  include 'data.inc'
  ! Local
  integer :: i
  !
  do i=1,n
    if (rms(i).ge.rmsmax) then
      badrm(i) = .true.
    else
      badrm(i) = .false.
    endif
  enddo
end
!<FF>
subroutine flag_re (n,res)
  integer :: n                      !
  real :: res                       !
  ! Global
  include 'size.inc'
  include 'data.inc'
  ! Local
  integer :: i
  real :: r2
  !
  r2 = res**2
  do i=1,n
    if (resaz(i)**2+resel(i)**2 .gt. r2) then
      badre(i) = .true.
      badre(i+n) = .true.
    else
      badre(i) = .false.
      badre(i+n) = .false.
    endif
  enddo
end
!<FF>
subroutine flag_source (n,object)
  integer :: n                      !
  character(len=*) :: object        !
  ! Global
  include 'size.inc'
  include 'data.inc'
  ! Local
  integer :: i
  !
  do i=1,n
    if (source(i).eq.object) then
      badus(i) = .true.
    endif
  enddo
end
!<FF>
subroutine flag_scan (n,jscan)
  integer :: n                      !
  integer :: jscan                  !
  ! Global
  include 'size.inc'
  include 'data.inc'
  ! Local
  integer :: i
  !
  do i=1,n
    if (scan(i).eq.jscan) then
      badus(i) = .true.
    endif
  enddo
end
!<FF>
subroutine get_source (n,object)
  integer :: n                      !
  character(len=*) :: object        !
  ! Global
  include 'size.inc'
  include 'data.inc'
  ! Local
  integer :: i
  !
  do i=1,n
    if (source(i).eq.object) then
      badus(i) = .false.
    endif
  enddo
end
!<FF>
subroutine get_scan (n,jscan)
  integer :: n                      !
  integer :: jscan                  !
  ! Global
  include 'size.inc'
  include 'data.inc'
  ! Local
  integer :: i
  !
  do i=1,n
    if (scan(i).eq.jscan) then
      badus(i) = .false.
    endif
  enddo
end
!<FF>
subroutine get_good (n,ngood)
  integer :: n                      !
  integer :: ngood                  !
  ! Global
  include 'size.inc'
  include 'data.inc'
  ! Local
  integer :: i
  !
  ngood = 0
  do i=1,n
    if (badus(i).or.badel(i).or.badrm(i).or.badre(i)   &
     &      .or.badti(i) ) then
      good(i) = .false.
    else
      good(i) = .true.
      ngood = ngood+1
    endif
  enddo
end
