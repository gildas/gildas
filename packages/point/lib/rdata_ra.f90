subroutine rdata_ra (azis,eles,eraz,erel,tiem,np,valpre,   &
     &    icode,scan,jant,stat,source,rms)
  !---------------------------------------------------------------------
  ! POINTING
  !	Read CLASS ANALYSE\PRINT POINTING output files
  !
  !----------------------------------------------------------------------
  ! Call
  !---------------------------------------------------------------------
  real*8 :: azis(1)                 !
  real*8 :: eles(1)                 !
  real*8 :: eraz(1)                 !
  real*8 :: erel(1)                 !
  real*8 :: tiem(1)                 !
  integer :: np                     !
  real*8 :: valpre(1)               !
  integer :: icode(1)               !
  integer :: scan(1)                !
  integer :: jant                   !
  character(len=*) :: stat          !
  character(len=*) :: source(1)     !
  real*8 :: rms(1)                  !
  ! Local
  real*8 :: fac,rts,pi,azi,ele,temp,pos,dpos,wid,dwi,err
  integer :: iaz,iel,icount,ic,jscan,ier,iobs,iscan,jcode,jndex
  integer :: iant,ista,jsta
  character(len=132) :: chain
  character(len=3) :: arm(3)
  character(len=12) :: name
  logical :: new
  data arm/'N','W','E'/
  !
  pi = acos(-1.0d0)
  fac = pi/180.d0
  rts = 3600.0d0/fac
  !
  ! Skip header lines (4)
  read(2,*)
  read(2,*)
  read(2,*)
  read(2,'(A)') chain
  new = index(chain,'A#').ne.0
  !
  if (new .and. jant.eq.0) then
    write(6,'(1X,A,$)') 'Enter selected antenna '
    read(6,*) jant
  endif
  !
  iaz = 0
  iel = 0
  icount = 0
  write(6,*) 'I-POINTING,  Reading RADIO data'
  ic = 0
  jscan=-5
  jsta = 0
  !
  do while (.true.)
    if (new) then
      read(2,*,iostat=ier,end=110) iobs,iscan,jcode,azi,ele,   &
     &        temp,pos,dpos,iant,ista, wid,dwi,err,err,name
      if (ier.ne.0) then
        write(6,*) 'W-RDATA_RA,  Read error in input file'
        goto 30
      endif
      if (iant.ne.jant) goto 30
      if (jsta.ne.0) then
        if (ista.ne.jsta) then
          write(6,*) 'E-POINT, Station change at ',iscan
          goto 30
        endif
      else
        jsta = ista
      endif
    else
      read(2,*,iostat=ier,end=110) iobs,iscan,jcode,azi,ele,   &
     &        temp,pos
      if (ier.ne.0) then
        write(6,*) 'W-RDATA_RA,  Read error in input file'
        goto 30
      endif
    endif
    !
    ! check whether scan number change
    if (iscan.ne.jscan) then
      ! check whether first scan or last scan complete
      if (iaz.ne.0 .and. iel.ne.0) then
        icount=icount+2
      elseif (jscan.ne.-5) then
        write(6,*) 'W-RDATA_RA,  Scan ignored',   &
     &          jscan
      endif
      jscan = iscan
      iaz=0
      iel=0
    endif
    !
    ! Azimuth drift
    if (jcode.eq.1) then
      if (iaz.eq.0) then
        iaz=1
        jndex=icount+iaz+iel
        azis(jndex)=azi
        eles(jndex)=ele
        tiem(jndex)=temp
        eraz(jndex)=pos
        erel(jndex)=0.
        icode(jndex)=jcode
        scan(jndex) =jscan
        rms(jndex) = dpos
        source(jndex) = name
      endif
      !
      ! Elevation drift
    elseif (jcode.eq.0) then
      if (iel.eq.0) then
        iel=1
        jndex=icount+iaz+iel
        azis(jndex)=azi
        eles(jndex)=ele
        tiem(jndex)=temp
        erel(jndex)=pos
        eraz(jndex)=0.
        icode(jndex)=jcode
        scan(jndex) =jscan
        rms(jndex) = dpos
        source(jndex) = name
      endif
    else
      write(6,*) 'W-RDATA_RA,  Scan ',iscan,   &
     &        ' is neither Azimuth nor Elevation'
    endif
    30    enddo
    110   np = icount
    ! Last scan complete
    if (iaz.eq.1 .and.iel.eq.1) np=np+2
    write(stat,'(1X,I2.2)') mod(jsta,100)
    jsta = jsta/100
    if (jsta.gt.0 .and. jsta.le.3) stat(1:1) = arm(jsta)
  end
