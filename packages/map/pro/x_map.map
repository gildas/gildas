!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
begin data gag_scratch:x-map.hlp

1 Imaging and deconvolution widget

     This is the main interface to image and deconvolve interferometric
     data. Actions are logically grouped in menu at the top of the widget.
  SETUP: Check the consistency of the interferometric and
               single-dish data and setup internal parameters accordingly.
  UVCOVER|UVSHOW: Display the content of the uv table.
  UVSHORT(all|setup|pseudo|merge): Process the short-spacings
               from the corresponding single-dish data.
        UVMAP|UVSTAT: Image the visibility data.
        DIRTY|BEAM|PRIMARY|WEIGHTS: Display the dirty beam or the dirty
         image. The display of the primary beams and weight image is
         also available for a mosaic.
  SUPPORT(inte|poly|rect|elli): Define a mask inside which the CLEAN
               components will be searched. The mask can be a simple polygon,
               rectangle or ellipse or a complex per channel user definition
               through the use of a cursor.
  HOGBOM|CLARK|SDI|MRC|MULTI: Deconvolve data using different
               algorithms.
  RESIDUALS|CLEAN|CCT: Display the deconvolution results: the
               residuals, the clean image or the table of clean components.
        VIEW|BIT: Toggle the default display between an interactive one
         (VIEW) and a standard channel map one (BIT).
     The actions are ordered from left to right in the typical time
     sequence of an imaging and deconvolution session.

     Actions are also hooked (in a less flexible way) on the buttons
     stacked on the left column of the widget (UVSHOW | UVSHORT | UVSTAT |
     UVMAP | SUPPORT | CLEAN | VIEW).  Parameters generic to all the
     actions are shown in the main display.  While parameters specific to a
     single action are accessible through the PARAMETER button in the
     middel column. The HELP buttons in the right column will display
     additional information on each action.

2 NAME
     Name of the data files without extension. All the files must be in the
     current directory.

2 FIRST
     First velocity/frequency channel to act on (default value is 0
     meaning the smallest velocity/frequency).

2 LAST
     Last velocity/frequency channel to act on (default value is 0
     meaning the largest velocity/frequency).

end data gag_scratch:x-map.hlp
!
begin data gag_scratch:uvshow.hlp

1 Tuning of the visualization of the visibility data

2 XTYPE
     Type of visibility information displayed on the X axis.

2 YTYPE
     Type of visibility information displayed on the Y axis.

2 BOX_LIMITS
     greg1\limits of the box where the visibility points are plotted. This string
     is defined as in the LIMITS command. An empty string implies automatic
     limits.

2 UVSHOW%TRACK
     linedb\use a different color for visibilities belonging to the same track. A
     track is defined as a collection of visibilities observed contiguously
     in time.

2 UVSHOW%STEP
     Minimum time step separated two contiguous visibility which starts the
     definition of a new track when UVSHOW%TRACK is YES.

2 UVSHOW%MARKER
     Definition of the symbol used to plot each visibility point. It is
     defined as in the SET MARKER command.

2 UVSHOW%ZERO
     Toggle the display of a vertical and a horizontal line showing the
     zero levels.

2 UVSHOW%FIT
     Toggle the display of the model curve fitted through UV_FIT (in the UV
     actions control panel).

end data gag_scratch:uvshow.hlp

begin data gag_scratch:uvshort.hlp

1 Tuning of the production of the short-spacings from single-dish data

     The single-dish input data is assumed to be a table (in GDF format) of
     name 'name'.tab in the same directory as the interferometric uv
     tables. For 30m data, this table is produced in CLASS in the right
     format by the TABLE command.

     Details of the processing useful to understand this help is available
     in the MAPPING manual.

2 UVSHORT%ACTION
     Action selected when pushing the GO button. The default is ALL meaning
     the pipelining of all others actions in order:
        1. SETUP: Check of the consistency of interferometric and
                  single-dish data and setup internal parameters
                  accordingly. The single-dish coverage and
                  interferometric field-of-view are displayed.
        2. PSEUDO: Production of short-spacing uv tables from the
                  single-dish data. The relative weight between single-dish
                  and interferometric data is computed at this step. As a
                  check, the density of weight as a function of the radius
                  is displayed.
        3. MERGE: Merging of the short-spacing and interferometric uv
                  tables. As a check dirty beam images and cuts
                  (horizontal, vertical and azimuthal averages) of the
                  dirty beam without and with the short-spacings are
                  displayed.
        4. CLEAN: Cleaning of intermediate file products.

2 UVSHORT%UNIT
     Unit of single dish data. "*" implies a guess from the single-dish
     table header during the SETUP step.

2 UVSHORT%DATFAC
     Multiplicative factor applied to the single-dish data in addition to
     the single-dish calibration factor automatically computed during the
     SETUP step.

2 UVSHORT%WEIFAC
     Multiplicative factor applied to the single-dish weight in addition to
     the single-dish calibration factor automatically computed during the
     SETUP step.

All the following variables are automatically setup during the SETUP
step. They can be precautiously modified after this step if the user knows
better...

2 UVSHORT%POSTOL
     Tolerance on the X-Y offsets as a fraction of the single dish beam.

2 UVSHORT%SPETOL
     Tolerance on the spectral axis as a fraction of channel size.

2 UVSHORT%UVTRUNC
     uv radius up to which the single-dish pseudo-visibilities are
     computed.

2 UVSHORT%BEAM%INT
     clean\primary half power beam width of the interferometer antennas.

2 UVSHORT%BEAM%SD
     Half power beam width of the single-dish antenna.

2 UVSHORT%DIAM%INT
     Diameter of the interferometer antennas.

2 UVSHORT%DIAM%SD
     Diameter of the single-dish antennas.

end data gag_scratch:uvshort.hlp

begin data gag_scratch:uvstat.hlp

1 Tuning of the synthesized beam properties (Weighting and tapering)

     Please refer to the MAPPING manual for a discussion of the concepts of
     weighting and tapering.

2 UVSTAT%MODE
     Mode (weighting or tapering) for which the synthesized beam is
     computed as a function of the tuning parameter (robust weighting
     threshold or Gaussian HPBW).

2 WCOL
     Column number of the weights in the input uv table used for the
     computation.

end data gag_scratch:uvstat.hlp
!
begin data gag_scratch:uvmap.hlp

1 Tuning of the production of the dirty beam and image

     Please refer to the MAPPING manual for a discussion of the imaging
     concepts.

2 MCOL
     First and last velocity/frequency channels to map (default value is 0
     meaning all the channels).

2 MAP_FIELD
     Map size in arcseconds, Default is 0, meaning a size which in pixels
     is a power of two that is about twice the size of the primary beam size.

2 MAP_SIZE
     Map size in pixels. Default is 0, meaning a power of two which gives
     about twice the size of the primary beam size.  For efficiency of FFT,
     it is better to pick a power of two.

2 UV_SHIFT
     Toggle the change of phase center and position angle (see MAP_RA,
     MAP_DEC and MAP_ANGLE). This may be used to align the map axes with a
     feature of interest of the source, by rotation of the uv coordinates.

2 MAP_RA
     Right Ascension of the phase center. Default is an empty string,
     meaning keep the value used at time of the observation.

2 MAP_DEC
     Declination of the phase center. Default is an empty string, meaning
     keep the value used at time of the observation.

2 MAP_ANGLE
     Map position angle in degree. Default is 0, meaning do not rotate the
     map.

2 WEIGHT_MODE
     Kind of weighting. Default is NATURAL weighting to maximize the point
     source sensitivity.

2 UV_TAPER
     Properties of the Gaussian used to apodize the weights of the uv
     visibilities (minor and major axes in meters and position angle in
     degree). Default is 0, meaning no tapering.

2 UV_CELL
     Size of the uv plane cell and threshold of the robust weighting.
     Default are 7.5 m (half the diameter of the PdBI antenna) and 1.

2 MAP_TRUNCATE
     Truncation level [0-100%] of the primary beam used in the mosaicing
     step of the imaging process.

2 MAP_BEAM_STEP
     Number of channels having the same dirty beam. If set to 0, there
     will be a single dirty beam for all channels. If set to -1, the number
     of channels per beam will be chosen as the best choice possible
     without violating the tolerance given in MAP_PRECIS.

2 MAP_PRECIS
     Tolerance in the position of sidelobes in pixels to define the number
     of channels per dirty beam.

2 WCOL
     Column number of the weight used to compute the dirty beam. The
     default is to compute only one dirty beam for the whole data
     cube. This default may be incorrect when adding data obtain at
     different rest frequencies.

2 CONVOLUTION
     Convolution kernel function for the gridding of the visibilities in
     the UV plane prior to the fourrier transform.

end data gag_scratch:uvmap.hlp
!
begin data gag_scratch:support.hlp

1 Definition of a support where to search for clean components in the deconvolution

     This widget enables the definition of a support.
     By default, the support is stored in a mask file called NAME.mask.
     Supports can be defined interactively for each channel, or colectively
     for all the channels between FIRST and LAST as either a polygon,
     a rectangle or an ellipse, in such a case the integrated image of
     the signal between the FIRST and LAST channels is displayed. This
     mechanisms enables to (relatively) easily define supports for ranges
     of channels.

2 GOSUPPORT%KIND
     Kind of support to be defined: polygon, rectangle, ellipse, or interactively
     defined through the use of the cursor.

2 GOSUPPORT%CENTER
     Center of the rectangle or the ellipse.

2 GOSUPPORT%MAJOR
     Major axis size of the rectangle or the ellipse.

2 GOSUPPORT%MINOR
     Minor axis size of the rectangle or the ellipse.

2 GOSUPPORT%PA
     Position angle of the rectangle or the ellipse.

end data gag_scratch:support.hlp
!
begin data gag_scratch:clean.hlp

1 Tuning of the CLEAN deconvolution

     Please refer to the MAPPING manual for a discussion of the
     deconvolution concepts.

2 METHOD
     Selection of the CLEAN algorithm used for the deconvolution. HOGBOM
     works even in the presence of strong sidelobes. CLARK is faster than
     HOGBOM. MULTI gives better results on extended sources. Only HOGBOM,
     clean\clark or SDI can be used on mosaics.

2 MYCLEAN%SUPPORT
     Toggle the use of a support in the deconvolution. The support must be
     defined through the SUPPORT panel. A first deconvolution must have
     already been done without support.

2 ARES
     Maximum amplitude of the absolute value of the residual image. This
     maximum is expressed in the image units (Jy/Beam).

2 FRES
     Maximum amplitude of the absolute value of the residual image. This
     maximum is expressed as a fraction of the peak intensity of the dirty
     image.

2 NITER
     Maximum number of clean components. The meaning of Clean Component
     depends on the method.

2 NMAJOR
     Maximum number of major cycles in all algorithms using this notion
     (CLARK, MX, SDI).

2 SEARCH_W
     Minimum relative weight to limit the region where the mosaic
     deconvolution algorithm is allowed to search for CLEAN components.

2 RESTORE_W
     Minimum relative weight to define the region where the mosaic
     deconvolution algorithm performs the final image restoration from the
     linedb\list of CLEAN components.

2 GAIN
     Loop gain. Default is 0.2, good compromise between stability and speed.

2 MYCLEAN%RATIO
     Smoothing factor used in the MRC algorithm.

2 SMOOTH
     Smoothing factor between the 3 images used in the MULTI resolution
     clean\clean algorithm.

2 BLC
     Bottom Left Corner of a square support in pixel units. Default is 0,
     meaning do not use such a square support.

2 TRC
     Top Right Corner of a square support in pixel units. Default is 0,
     meaning do not use such a square support.

2 BEAM_PATCH
     Size of the patch used to fit the dirty beam in order to derive the
     clean\clean beam parameters. 0 means automatically defined.

2 MAJOR
     Forcibly define the major axis of the clean beam.

2 MINOR
     Forcibly define the minor axis of the clean beam.

2 ANGLE
     Forcibly define the position angle of the clean beam.


end data gag_scratch:clean.hlp
!
begin data gag_scratch:view.hlp

1 Tuning of the display of spectra cubes or continuum images

2 SCALE
     Intensity scale. 0 implies an automatic computation.

2 SIZE
     Size of the plotted region. 0 implies the display of the whole region.

2 CENTER
     Center of the plotted region.

2 RANGE
     Velocity range of the plotted spectra. 0 implies the display of the
     whole spectra.

2 CROSS
     Size of cross displayed at the phase center position.

2 MARK
     Type of box marking when displaying channel maps.

2 DO_BIT
     Toggle the display of the image.

2 DO_CONTOUR
     Toggle the display of the contours.

2 SPACING
     Values of the intensity separation between two contours when using
     regularly spaced contours. 0 implies an automatic computation to get a
     sensible number of contours.

end data gag_scratch:view.hlp
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
gui\panel "Imaging and deconvolution" gag_scratch:x-map.hlp
  let name 'name' /prompt "Generic file name (without extension)"
  let first 'first' /prompt "First channel"
  let last 'last' /prompt "Last channel"
!
gui\button "go setup"   "SETUP"
gui\menu "UVSHOW" /choices
   gui\button "go uvcov"   "UV COVER"
   gui\button "go uvshow"  "UV SHOW"
gui\menu /close
gui\menu "UVSHORT" /choices
   gui\button "go uvshort all"    "UVSHORT (all)"
   gui\button "go uvshort setup"  "UVSHORT (setup)"
   gui\button "go uvshort pseudo" "UVSHORT (pseudo)"
   gui\button "go uvshort merge"  "UVSHORT (merge)"
gui\menu /close
gui\menu "UVMAP" /choices
   gui\button "go uvmap"         "UVMAP"
   gui\button "go uvstat weight" "UVSTAT (weight)"
   gui\button "go uvstat taper"  "UVSTAT (taper)"
   gui\button "go uvstat header" "UVSTAT (header)"
gui\menu /close
gui\menu "UVMAP" /choices
   gui\button "go plot lmv"    "DIRTY"
   gui\button "go plot beam"   "BEAM"
   gui\button "go plot lobe"   "PRIMARY (mosaics)"
   gui\button "go plot fields" "FIELDS (mosaics)"
gui\menu /close
gui\menu "SUPPORT" /choices
   gui\button "go support interactive" "SUPPORT (inte)"
   gui\button "go support polygon"     "SUPPORT (poly)"
   gui\button "go support rectangle"   "SUPPORT (rect)"
   gui\button "go support ellipse"     "SUPPORT (elli)"
gui\menu /close
gui\menu "CLEAN" /choices
   gui\button "go clean hogbom" "HOGBOM"
   gui\button "go clean clark"  "CLARK"
   gui\button "go clean sdi"    "SDI"
   gui\button "go clean mrc"    "MRC"
   gui\button "go clean multi"  "MULTI"
gui\menu /close
gui\menu "CLEAN" /choices
   gui\button "go plot res"   "RESIDUALS"
   gui\button "go plot clean" "CLEAN"
   gui\button "go plot cct"   "CCT"
gui\menu /close
gui\menu "DEFAULTS" /choices
   gui\button "go plot view" "VIEW"
   gui\button "go plot bit"  "BIT"
gui\menu /close
!
gui\button "go uvshow" "UVSHOW" "UV visualization" gag_scratch:uvshow.hlp "Parameters"
  say "X and Y axis definition"
  let xtype 'xtype' /prompt "X data" /choice "u" "v" "angle" "radius" "time" "date" "scan" "number" "amp" "phase" "real" "imag" "weight"
  let ytype 'ytype' /prompt "Y data" /choice "u" "v" "angle" "radius" "time" "date" "scan" "number" "amp" "phase" "real" "imag" "weight"
  let box_limits 'box_limits' /prompt "Plot limits"
  say ""
  say "Colorization"
  let uvshow%track 'uvshow%track' /prompt "Use one color per track?"
  let uvshow%step 'uvshow%step' /prompt "Typical time separating 2 tracks [hrs]"
  say ""
  say "Other plot characteristics"
  let uvshow%marker 'uvshow%marker' /prompt "Marker definition"
  let uvshow%zero 'uvshow%zero' /prompt "Display zero level?"
  let uvshow%fit 'uvshow%fit' /prompt "Plot model fit?"
  say ""
!
gui\button "go uvshort" "UVSHORT" "Short-space processing" gag_scratch:uvshort.hlp "Parameters"
  !
  say "Main control parameters"
  let uvshort%action 'uvshort%action' /prompt "Action(s) to be taken" /choice "all" "setup" "pseudo" "merge" "clean"
  let uvshort%unit 'uvshort%unit' /prompt "Single-Dish data unit" /choice "*"  "K (Ta*)" "K (Tmb)" "Jy/Beam"
  let uvshort%datfac 'uvshort%datfac' /prompt "Additional SD amplitudes scaling factor"
  let uvshort%weifac 'uvshort%weifac' /prompt "Additional SD weights scaling factor"
  !
  say " "
  say "Additional parameters for SPECIALISTS (Be careful...)"
  say "Parameters initialized at MAPPING start => Changeable anytime"
  let uvshort%postol 'uvshort%postol' /prompt "Position tolerance [fraction of SD beam]"
  let uvshort%spetol 'uvshort%spetol' /prompt "Spectral tolerance [fraction of channel]"
  let uvshort%uvtrunc 'uvshort%uvtrunc'  /prompt "Single dish uv truncation radius [m]"
  let uvshort%beam%int 'uvshort%beam%int' /prompt "Interferometer primary beam size [arcsec]"
  let uvshort%beam%sd  'uvshort%beam%sd'  /prompt "Single Dish beam size [arcsec]"
  let uvshort%diam%int 'uvshort%diam%int' /prompt "Interferometer antenna diameter [m]"
  let uvshort%diam%sd  'uvshort%diam%sd'  /prompt "Single Dish antenna diameter [m]"
  !
gui\button "go uvstat" "UVSTAT" "Weight polling" gag_scratch:uvstat.hlp "Parameters"
  let uvstat%mode 'uvstat%mode' /prompt "Polled mode" /choice "taper" "weight" "header"
  say ""
  say "Additional parameters for SPECIALISTS (Be careful...)"
  let wcol 'wcol' /prompt "Weight channel"
  say ""
!
gui\button "go uvmap" "UVMAP" "Imaging" gag_scratch:uvmap.hlp "Parameters"
  if (map%newnames) then
     let mcol 'mcol[1]' 'mcol[2]' /prompt "First and last channel to image"
     say ""
     say "Image grid definition"
     let map_cell  'map_cell[1]' 'map_cell[2]' /prompt "Pixel size [arcsec]"
     let map_field 'map_field[1]' 'map_field[2]' /prompt "Field size [arcsec]"
     let map_size 'map_size[1]' 'map_size[2]' /prompt "Map size [pixels]"
     let map_shift 'map_shift' /prompt "Shift and rotate map on specified center ?"
     let map_ra 'map_ra' /prompt "Right Ascension"
     let map_dec 'map_dec' /prompt "Declination"
     let map_angle 'map_angle' /prompt "Angle from North to East"
     say ""
     say "Weighting definition"
     let map_weight 'map_weight' /prompt "Weighting mode" /choice "natural" "robust"
     let map_uvtaper 'map_uvtaper[1]' 'map_uvtaper[2]' 'map_uvtaper[3]' /prompt "UV Taper"
     let map_robust 'map_robust' /prompt "Robust weighting threshold"
     let map_uvcell 'map_uvcell' /prompt "Robust UV cell size"
     say ""
     say "Additional parameters for MOSAICS"
     let map_truncate 'map_truncate' /prompt "Truncation level of primary beam [0-100%]"
     say ""
     say "Additional parameters for SPECIALISTS (Be careful...)"
     let map_beam_step 'map_beam_step' /prompt "Number of channels per single dirty beam"
     let map_precis    'map_precis' /prompt "Fraction of pixel tolerance when previous variable is set to -1"
     let wcol 'wcol' /prompt "Weight channel"
     let map_convolution 'map_convolution' /prompt "Convolution function" /index box sinc expo expo_sinc spheroidal
     say ""
  else
     let mcol 'mcol[1]' 'mcol[2]' /prompt "First and last channel to image"
     say ""
     say "Image grid definition"
     let map_cell 'map_cell[1]' 'map_cell[2]' /prompt "Pixel size [arcsec]"
     let map_field 'map_field[1]' 'map_field[2]' /prompt "Field size [arcsec]"
     let map_size 'map_size[1]' 'map_size[2]' /prompt "Map size [pixels]"
     let uv_shift 'uv_shift' /prompt "Shift and rotate map on specified center ?"
     let map_ra 'map_ra' /prompt "Right Ascension"
     let map_dec 'map_dec' /prompt "Declination"
     let map_angle 'map_angle' /prompt "Angle from North to East"
     say ""
     say "Weighting definition"
     let weight_mode 'weight_mode' /prompt "Weighting mode" /choice "natural" "robust" *
     let uv_taper 'uv_taper[1]' 'uv_taper[2]' 'uv_taper[3]' /prompt "UV Taper"
     let uv_cell 'uv_cell[1]' 'uv_cell[2]' /prompt "UV Cell size and robust weighting threshold"
     say ""
     say "Additional parameters for MOSAICS"
     let map_truncate 'map_truncate' /prompt "Truncation level of primary beam [0-100%]"
     say ""
     say "Additional parameters for SPECIALISTS (Be careful...)"
     let map_beam_step 'map_beam_step' /prompt "Number of channels per single dirty beam"
     let map_precis    'map_precis' /prompt "Fraction of pixel tolerance when previous variable is set to -1"
     let wcol 'wcol' /prompt "Weight channel"
     let convolution 'convolution' /prompt "Convolution function" /index box sinc expo expo_sinc spheroidal
     say ""
  endif
!
gui\button "go support" "SUPPORT" "Support definition" gag_scratch:support.hlp "Parameters"
  let gosupport%kind 'gosupport%kind' /prompt "Support kind" /choice "interactive" "polygon" "rectangle" "ellipse"
  say ""
  say "Rectangle or ellipse parameters"
  let gosupport%center 'gosupport%center[1]' 'gosupport%center[2]' /prompt "Center position offsets [arcsec]"
  let gosupport%major 'gosupport%major' /prompt "Major axis [arcsec]"
  let gosupport%minor 'gosupport%minor' /prompt "Minor axis [arcsec]"
  let gosupport%pa 'gosupport%pa' /prompt "Position angle (from North to East) [degree]"
  say ""
!
gui\button "go clean" "CLEAN" "Deconvolution" gag_scratch:clean.hlp "Parameters"
  let method 'method' /prompt "Deconvolution method" /choice "hogbom" "clark" "sdi" "mrc" "multi"
  let myclean%support 'myclean%support' /prompt "Use the support previously defined by SUPPORT?"
  say ""
  say "Stopping criteria"
  let ares 'ares' /prompt "Max abs. residual"
  let fres 'fres' /prompt "Frac. abs. residual"
  let niter 'niter' /prompt "Max. number of iterations"
  let nmajor 'nmajor' /prompt "Max. number of major cycles"
  say ""
  say "Additional parameters for MOSAICS"
  let search_w 'search_w' /prompt "Min. weight for search [0-1]"
  let restore_w 'restore_w' /prompt "Min. weight for restore [0-1]"
  say ""
  say "Additional parameters for SPECIALISTS (Be careful...)"
  let gain 'gain' /prompt "Loop gain"
  let myclean%ratio 'myclean%ratio' /prompt "MRC smoothing factor"
  let smooth 'smooth' /prompt "MULTI smoothing factor"
  let blc 'blc[1]' 'blc[2]' /prompt "Bottom Left corner"
  let trc 'trc[1]' 'trc[2]' /prompt "Top Right corner"
  let beam_patch 'beam_patch[1]' 'beam_patch[2]' /prompt "Beam Patch"
  let major 'major' /prompt "Clean Beam major axis (sec)"
  let minor 'minor' /prompt "Clean Beam minor axis (sec)"
  let angle 'angle' /prompt "Clean Beam PA (deg E from N)"
  say ""
!
gui\button "go view" "VIEW" "Image visualization" gag_scratch:view.hlp "Parameters"
  say "Part of data cube to be plotted"
  let scale    'scale[1]' 'scale[2]'   /prompt "Intensity scale"
  let size     'size[1]' 'size[2]'     /prompt "Size of Plotted Region [arcsec]"
  let center   'center[1]' 'center[2]' /prompt "Center of Plotted Region [arcsec]"
  let range    'range[1]' 'range[2]'   /prompt "Velocity Range [km/s]"
  say ""
  say "Other plot parameters"
  let cross 'cross' /prompt "Phase center cross size [arcsec]"
  let mark 'mark' /prompt "Box marking type" /choice "velocity" "frequency" "channel" "none"
  let do_bit 'do_bit' /prompt "Show bitmap images?"
  let do_contour 'do_contour' /prompt "Show contour?"
  let spacing 'spacing' /prompt "Contour steps"
  say ""
!
gui\go "go image"
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
