!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module gridding_types
  public :: gridding
  private
  !
  type gridding
     real(kind=4) :: ubias       ! Bias of U coordinates
     real(kind=4) :: ubuff(4096) ! Grid of values of the convolution
     real(kind=4) :: vbias       ! Bias of V coordinates
     real(kind=4) :: vbuff(4096) ! Grid of values of the convolution
  end type gridding
end module gridding_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
