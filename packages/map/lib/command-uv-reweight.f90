!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module uv_reweight
  use gbl_message
  !
  public :: uv_reweight_comm
  private
  !
contains
  !
  subroutine uv_reweight_comm(line,error)
    use gkernel_interfaces
    use uv_buffers
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    integer(kind=index_length) :: iv
    integer :: ic
    real(4) :: scale
    character(len=*), parameter :: rname='UV_REWEIGHT'
    !
    if (huv%loca%size.eq.0) then
      call map_message(seve%e,rname,'No UV data loaded')
      error = .true.
      return
    endif
    !
    error = .false.
    call sic_r4(line,0,1,scale,.true.,error)
    if (error) return
    !
    do iv=1,huv%gil%nvisi
      do ic=1,huv%gil%nchan
        duv(3*ic+huv%gil%fcol-1,iv) = scale * duv(3*ic+huv%gil%fcol-1,iv)
      enddo
    enddo
    do_weig = .true.
  end subroutine uv_reweight_comm
end module uv_reweight
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
