module mapping_interfaces_none
  interface
    subroutine big_wait(n,b)
      real, intent(inout) :: b
      integer, intent(in) :: n
    end subroutine big_wait
  end interface
  !
  interface
    subroutine map_message_set_id(id)
      use gbl_message
      !---------------------------------------------------------------------
      ! Alter library id into input id. Should be called by the library
      ! which wants to share its id with the current one.
      !---------------------------------------------------------------------
      integer, intent(in) :: id
    end subroutine map_message_set_id
  end interface
  !
  interface
    subroutine mapping_pack_set(pack)
      use gpack_def
      !----------------------------------------------------------------------
      ! Can not be in a module as it is called by mapping-pyimport.c and
      ! mapping-sicimport.c
      !----------------------------------------------------------------------
      type(gpack_info_t), intent(out) :: pack
    end subroutine mapping_pack_set
  end interface
  !
end module mapping_interfaces_none
