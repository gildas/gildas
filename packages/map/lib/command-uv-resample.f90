!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module uv_resample
  use gbl_message
  !
  public :: uv_resample_comm
  private
  !
contains
  !
  subroutine uv_resample_comm(line,comm,error)
    use phys_const
    use image_def
    use gkernel_interfaces
    use file_buffers
    use uv_buffers
    !---------------------------------------------------------------------
    ! Resample in velocity the UV Table or compress it
    ! Support for commands
    !     UV_RESAMPLE NC [Ref Val Inc]
    !     UV_COMPRESS NC
    !---------------------------------------------------------------------
    character(len=*), intent(in) :: line  ! Command line
    character(len=*), intent(in) :: comm  ! Calling command
    logical, intent(out) :: error         ! Error flag
    !
    character(len=11) :: rname = 'UV_RESAMPLE'
    real, pointer :: duv_previous(:,:), duv_next(:,:)
    character(len=80) :: chain
    character(len=4) :: arg(4)
    real(8) :: convert(3), velo
    integer :: nt, nc, nchan, na
    type (gildas) :: uvout
    integer :: nu, nv, ni, i
    !
    if (huv%loca%size.eq.0) then
       call map_message(seve%e,'UV_MAP','No UV data loaded')
       error = .true.
       return
    endif
    !
    error = .false.
    ! No argument: get back to the original UV data
    if (sic_narg(0).eq.0) then
       call uv_reset_buffer(rname)
       do_weig = .true.
       return
    endif
    !
    ! Compute the Frequency Conversion from the Velocity Conversion
    call gildas_null(uvout, type = 'UVT')
    call gdf_copy_header(huv,uvout,error)
    !
    if (comm.eq.'UV_RESAMPLE') then
       nc = 0
       !    SIC__CH would complain if argument is too long, SIC_KE is silent
       call sic_ke(line,0,1,arg(1),na,.true.,error)
       error = .false.
       if (arg(1).ne.'*') call sic_i4(line,0,1,nc,.true.,error)
       if (error) return
       !
       ! Set default resampling in Velocity
       convert(1) = huv%gil%ref(1)
       convert(2) = huv%gil%voff
       convert(3) = huv%gil%vres
       ! Get new one
       do i=2,sic_narg(0)
          call sic_ke(line,0,i,arg(i),na,.false.,error)
          error = .false.
          if (arg(i).ne.'*') call sic_r8(line,0,i,convert(i-1),.false.,error)
          if (error) return
       enddo
       !
       ! Fill missing arguments
       if (nc.eq.0) then
          nc = nint(abs(huv%gil%nchan*huv%gil%vres/convert(3)))
          if (arg(2).eq.'*') then
             ! Keep the middle channel in the middle
             velo = (0.5d0*huv%gil%nchan-huv%gil%ref(1))*huv%gil%vres + huv%gil%voff
             convert(1) = 0.5d0*nc - (velo-convert(2))/convert(3)
          endif
       endif
       !
       call getuv_conversion(uvout,nc,convert)
       !
    else if (comm.eq.'UV_COMPRESS') then
       call sic_i4(line,0,1,nc,.true.,error)
       if (error) return
       if (nc.gt.huv%gil%nchan) nc = huv%gil%nchan
       write(chain,'(A,I6,A)') 'Averaging by chunks of ',nc,' channels'
       call map_message(seve%i,comm,chain)
       uvout%gil%inc(1) = uvout%gil%inc(1)*nc
       uvout%gil%ref(1) = (2.0d0*uvout%gil%ref(1)+nc-1.0)/(2*nc)
       uvout%gil%vres = nc*uvout%gil%vres
       uvout%gil%fres = nc*uvout%gil%fres
       ! Change the number of channels
       uvout%gil%nchan = uvout%gil%nchan/nc
    else
       call map_message(seve%e,rname,'Unrecognized choice '//comm)
       error = .true.
       return
    endif
    !
    ! Work with complete structure
    uvout%gil%dim(1) = 3*uvout%gil%nchan + 7 + uvout%gil%ntrail ! and nothing more ?...
    if (uvout%gil%ntrail.ne.0) then
       do i=1,code_uvt_last
          if (uvout%gil%column_pointer(i).gt.uvout%gil%lcol) uvout%gil%column_pointer(i) =  &
               & uvout%gil%column_pointer(i) + uvout%gil%dim(1) - huv%gil%dim(1)
       enddo
    endif
    !
    ni = huv%gil%dim(1)
    nu = uvout%gil%dim(1)
    nv = uvout%gil%dim(2)
    nt = uvout%gil%ntrail
    nchan = uvout%gil%nchan
    !
    ! Prepare appropriate array...
    nullify (duv_previous, duv_next)
    call uv_find_buffers (comm,nu,nv,duv_previous, duv_next,error)
    if (error) then
       call map_message(seve%e,comm,'Cannot set buffer pointers')
       return
    endif
    !
    ! Now resample
    if (comm.eq.'UV_RESAMPLE') then
       call resample_uv (huv,uvout,duv_previous,duv_next,nt)
    else if (comm.eq.'UV_COMPRESS') then
       call compress_uv (duv_next,nu,nv,nchan,duv_previous,ni,nc,nt)
    endif
    !
    ! Set header
    call gdf_copy_header(uvout,huv,error)
    !
    ! Reset proper pointers
    call uv_clean_buffers (duv_previous, duv_next,error)
    if (error) return
    !
    if (allocated(uvtb%data)) deallocate (uvtb%data)            ! UV data not plotted
    uv_plotted = .false.
    !
    ! Indicate optimization and save status
    optimize(code_save_uv)%change = optimize(code_save_uv)%change + 1
    save_data(code_save_uv) = .true.
    do_weig = .true.  ! Recompute weight
    !
    ! Redefine SIC variables 
    call sic_delvariable ('UV',.false.,error)
    call sic_mapgildas ('UV',huv,error,duv) 
  end subroutine uv_resample_comm
  !
  !---------------------------------------------------------------------
  !
  subroutine getuv_conversion(uvout,nc,convert)
    use gildas_def
    use image_def
    use phys_const
    !---------------------------------------------------------------------
    !   Derive the frequency axis corresponding to a specified
    !   Velocity sampling.
    !---------------------------------------------------------------------
    type (gildas), intent(inout) :: uvout   ! Output UV table header
    integer, intent(in) :: nc               ! Number of desired channels
    real(8), intent(inout) :: convert(3)    ! Velocity conversion formula
    !
    real(8) :: n_iref
    !
    ! Convert(3) is initially on a Velocity Scale.
    !
    ! Compute the Reference pixel at which VOFF will occur
    ! in this new sampling, to preserve the VOFF / FREQ / IREF 
    ! correspondance
    !! voff = (n_iref - convert(1))*convert(3) + convert(2)
    n_iref = (uvout%gil%voff - convert(2)) / convert(3) + convert(1)
    !
    convert(1) = n_iref
    convert(2) = uvout%gil%voff
    uvout%gil%vres = convert(3)
    uvout%gil%fres = - uvout%gil%vres * 1d3 / clight * uvout%gil%freq
    uvout%gil%ref(1) = convert(1)
    uvout%gil%val(1) = uvout%gil%freq  ! This is already the case
    uvout%gil%inc(1) = uvout%gil%fres  
    uvout%gil%nchan = nc
    !
    uvout%gil%nchan = nc
    uvout%gil%dim(1) = uvout%gil%nlead+uvout%gil%ntrail + uvout%gil%natom*nc
  end subroutine getuv_conversion
  !
  subroutine resample_uv (uvint,uvout,uvint_data,uvout_data,nt)
    use image_def
    !------------------------------------------------------------------------
    ! Support for command UV_RESAMPLE
    ! Resample (in memory) a UV data set
    !------------------------------------------------------------------------
    type (gildas), intent(in) :: uvint  ! Input UV header
    type (gildas), intent(in) :: uvout  ! Output UV header
    real(4), intent(in) :: uvint_data(uvint%gil%dim(1),uvint%gil%dim(2))
    real(4), intent(out) :: uvout_data(uvout%gil%dim(1),uvout%gil%dim(2))
    integer, intent(in) :: nt         ! Number of trailing informations
    !
    integer(kind=index_length) :: iv, nv
    integer :: nin, nou
    real(8) :: xref,xval,xinc
    real(8) :: yref,yval,yinc
    !
    nv = uvint%gil%nvisi 
    xref = uvout%gil%ref(1)
    yref = uvint%gil%ref(1)
    xval =   uvout%gil%voff
    xinc =   uvout%gil%vres
    yval =   uvint%gil%voff
    yinc =   uvint%gil%vres
    !
    nou = uvout%gil%dim(1)
    nin = uvint%gil%dim(1)
    !
    ! Add test here
    call uv_resample_test_overlap(uvout%gil%nchan,xinc,xref,xval,uvint%gil%nchan,yinc,yref,yval)
    !
    !$OMP PARALLEL &
    !$OMP    & SHARED(uvout_data,uvint_data, uvout, uvint) &
    !$OMP    & SHARED(xinc,xref,xval, yinc,yref,yval, nv,nt,nou,nin) & 
    !$OMP    & PRIVATE(iv)
    !$OMP DO SCHEDULE(STATIC)
    do iv = 1,nv  						  
       uvout_data(1:7,iv) = uvint_data(1:7,iv)
       call interpolate_uv (uvout_data(8,iv), uvout%gil%nchan, xinc,xref,xval, &
            uvint_data(8,iv),uvint%gil%nchan,yinc,yref,yval)
       if (nt.ne.0) then
          uvout_data(nou-nt+1:nou,iv) = uvint_data(nin-nt+1:nin,iv)
       endif
    enddo
    !$OMP END DO
    !$OMP END PARALLEL
  end subroutine resample_uv
  !
  subroutine interpolate_uv(x,xdim,xinc,xref,xval, y,ydim,yinc,yref,yval)
    !-----------------------------------------------------------------------
    ! Support for UV_RESAMPLE
    ! Performs the linear interpolation/integration for visibilities
    !
    ! 1) if actual interpolation (XINC <= YINC)
    !     this is a 2-point linear interpolation formula.
    !     the data is untouched if the resolution is unchanged and the shift
    !     is an integer number of channels.
    !
    ! 2) if not (XINC > YINC)
    !     boxcar convolution (width xinc-yinc) followed by linear interpolation
    !
    !-----------------------------------------------------------------------------
    integer, intent(in) :: ydim             ! Input axis size
    integer, intent(in) :: xdim             ! Output axis size
    real(8), intent(in) :: yref,yval,yinc   ! Input conversion formula
    real(8), intent(in) :: xref,xval,xinc   ! Output conversion formula
    real(4), intent(in) :: y(3,xdim)        ! Input values
    real(4), intent(out) :: x(3,ydim)       ! Output values
    ! Local:
    integer i,imax,imin, j
    real(8) :: pix, val, expand
    real scale, ww
    !-----------------------------------------------------------------------
    !
    expand = abs(xinc/yinc)
    do i = 1, xdim
       !
       ! Compute interval
       val = xval + (i-xref)*xinc
       pix = (val-yval)/yinc + yref
       if (expand.gt.1.) then
          imin = int(pix-expand/2d0+0.5d0)
          imax = int(pix+expand/2d0+0.5d0)
          if (imin.gt.ydim .or. imax.lt.1) then
             cycle
          endif
          if (imin.lt.1) then
             imin = 1
             ww = 1.0  ! the whole channel is in...
          else
             ww = imin-(pix-expand/2d0-0.5d0)
          endif
          x(1:3,i) = y(1:3,imin)*ww
          scale = ww
          !
          if (imax.gt.ydim) then
             imax = ydim
             ww = 1.0
          else
             ww = pix+expand/2d0+0.5d0-imax
          endif
          x(1:3,i) = x(1:3,i) + y(1:3,imax)*ww
          scale = scale + ww
          !
          do j=imin+1, imax-1
             x(1:3,i) = x(1:3,i) + y(1:3,j)
             scale = scale+1.
          enddo
          ! Normalize
          !!            w(i) = scale/expand  ! This is the fraction of channel covered
          x(1:2,i) = x(1:2,i)/scale
       else      
          imin = int(pix)
          imax = imin+1
          if ((imin.lt.1).or.(imax.gt.ydim)) then
             x(1:3,i) = 0.
          else
             x(1:3,i) = y(1:3,imin)*(imin+1-pix) + y(1:3,imin+1)*(pix-imin)
          endif
          ! Re-scale the weight
          x(3,i) = x(3,i)*expand
       endif
    enddo
  end subroutine interpolate_uv
  !
  subroutine average_uv (out,nx,nv,inp,ny,nc,num,nt)
    !------------------------------------------------------------------------
    !   Support for command UV_AVERAGE
    !   Average (in memory) a series of channels of a UV data set
    !------------------------------------------------------------------------
    integer, intent(in) :: num        ! Number of selected channels
    integer, intent(in) :: nx         ! Output Visibility size
    integer, intent(in) :: nv         ! Number of visibilities
    real, intent(out) :: out(nx,nv)   ! Output Visibilities
    integer, intent(in) :: ny         ! Input visibility size
    real, intent(in) :: inp(ny,nv)    ! Input visibilities
    integer, intent(in) :: nc(num)    ! Selected Channels, first & last pairs
    integer, intent(in) :: nt         ! Number of trailing informations
    ! Local
    integer :: j,k,kk,l
    real :: a,b,c
    !
    do j=1,nv
       out(1:7,j) = inp(1:7,j)
       a = 0.0
       b = 0.0
       c = 0.0
       do l=2,num,2
          do k=nc(l-1),nc(l)
             kk = 7+3*k
             if (inp(kk,j).gt.0) then
                a = a+inp(kk-2,j)*inp(kk,j)
                b = b+inp(kk-1,j)*inp(kk,j)
                c = c+inp(kk  ,j)
             endif
          enddo
       enddo
       if (c.ne.0.0) then
          out(8,j) =a/c
          out(9,j) =b/c
          out(10,j)=c              ! time*band
       else
          out(8,j) =0.0
          out(9,j) =0.0
          out(10,j)=0.0
       endif
       !
       if (nt.ne.0) then
          out(nx-nt+1:nx,j) = inp(ny-nt+1:ny,j)
       endif
    enddo
  end subroutine average_uv
  !
  subroutine compress_uv(out,nx,nv,mx,inp,ny,nc,nt)
    !------------------------------------------------------------------------
    !   Support for command UV_COMPRESS
    !   Compress (in memory) by NC channels a UV data set
    !------------------------------------------------------------------------
    integer, intent(in) :: nx         ! Output Visibility size
    integer, intent(in) :: nv         ! Number of visibilities
    integer, intent(in) :: mx         ! Number of Output Channels
    real, intent(out) :: out(nx,nv)   ! Output Visibilities
    integer, intent(in) :: ny         ! Input visibility size
    real, intent(in) :: inp(ny,nv)    ! Input visibilities
    integer, intent(in) :: nc         ! Number of Channels to average
    integer, intent(in) :: nt         ! Number of trailing informations
    ! Local
    integer :: i,j,k,kk,ifi
    real :: a,b,c
    !
    do j=1,nv
       out(1:7,j) = inp(1:7,j)
       ifi = 1
       do i=1,mx
          a = 0.
          b = 0.
          c = 0.
          do k=ifi,ifi+nc-1
             kk = 7+3*k
             if (inp(kk,j).gt.0) then
                a = a+inp(kk-2,j)*inp(kk,j)
                b = b+inp(kk-1,j)*inp(kk,j)
                c = c+inp(kk  ,j)
             endif
          enddo
          ifi = ifi+nc
          kk = 7+3*i
          if (c.ne.0) then
             out(kk-2,j) = a/c
             out(kk-1,j) = b/c
             out(kk  ,j) = c         ! time*band
          else
             out(kk-2,j) = 0.
             out(kk-1,j) = 0.
             out(kk  ,j) = 0.
          endif
       enddo
       !
       if (nt.ne.0) then
          out(nx-nt+1:nx,j) = inp(ny-nt+1:ny,j)
       endif
    enddo
  end subroutine compress_uv
  !
  subroutine uv_resample_test_overlap(nchanout,incout,refout,valout,nchanin,incin,refin,valin)
    !---------------------------------------------------------------------
    ! Tests if a resampled UV table is going to be empty
    !---------------------------------------------------------------------
    integer, intent(in)      :: nchanin              ! Input number of channels
    integer, intent(in)      :: nchanout             ! Output number of channels
    real(kind=8), intent(in) :: refin,valin,incin    ! Input conversion formula
    real(kind=8), intent(in) :: refout,valout,incout ! Output conversion formula
    ! Locals
    integer(kind=4)          :: nzerochans ! Number of zeroed channels on output
    integer(kind=4)          :: ichan ! index for channel iterations
    real(kind=8)             :: velout ! velocity for chan in output
    real(kind=8)             :: channelin ! channel in input
    !
    ! Code
    nzerochans = 0
    do ichan = 1, nchanout
       ! Compute interval
       velout = valout + (ichan-refout)*incout ! velocity on output
       channelin = (velout-valin)/incin + refin ! channel on input
       if (channelin.gt.nchanin.or.channelin.lt.1) then
          nzerochans = nzerochans+1
       endif
    enddo
    if (nzerochans.eq.nchanout) then
       call map_message(seve%w,'UV_RESAMPLE',"Requested velocity range does not intersect the input velocity range")
       call map_message(seve%w,'UV_RESAMPLE',"Output UV table will be zero valued")
    else if (nzerochans.lt.nchanout.and.nzerochans.gt.0) then
       call map_message(seve%w,'UV_RESAMPLE',"Requested velocity range partially intersect the input velocity range")
       call map_message(seve%w,'UV_RESAMPLE',"Output UV table will contain zero valued channels")
    end if
  end subroutine uv_resample_test_overlap
end module uv_resample
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
