subroutine dofft_quick_debug (np,nv,visi,jx,jy,jo   &
     &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
     &    ubias,vbias,ubuff,vbuff)
  use gkernel_interfaces
  !$  use omp_lib
  !----------------------------------------------------------------------
  ! @ private
  !
  ! GILDAS  MAP_FAST
  !   Compute FFT of image by gridding UV data
  !   - Taper before gridding
  !   - Gridding with pre-computed support
  !   - Uses symmetry
  !----------------------------------------------------------------------
  integer, intent(in) :: nv                   ! number of values
  integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
  real, intent(in) :: visi(np,nv)             ! values
  integer, intent(in) :: nc                   ! number of channels
  integer, intent(in) :: jx                   ! X coord location in VISI
  integer, intent(in) :: jy                   ! Y coord location in VISI
  integer, intent(in) :: jo                   ! first channel to map
  integer, intent(in) :: nx                   ! X map size
  integer, intent(in) :: ny                   ! Y map size
  real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
  real, intent(in) :: mapx(nx)                ! X Coordinates of grid
  real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
  real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
  real, intent(in) :: cell(2)                 ! cell size in Meters
  real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
  real, intent(in) :: we(nv)                  ! Weight array
  real, intent(in) :: vv(nv)                  ! V Values
  real, intent(in) :: ubias                   ! U gridding offset
  real, intent(in) :: vbias                   ! V gridding offset
  real, intent(in) :: ubuff(4096)             ! U gridding buffer
  real, intent(in) :: vbuff(4096)             ! V gridding buffer
  ! Global
  real(8), parameter :: pi=3.141592653589793d0
  ! Local
  integer ix,iy,ic,i,iin,iou,io
  integer ixm,ixp,iym,iyp,iu,iv
  real result,resima,staper,etaper,res
  real u,v,du,dv,ufac,vfac
  real cx,cy,sx,sy
  real(8) xinc,xref,yinc,yref
  logical do_taper
  !
  integer my,kx,ky, noper
  integer, parameter :: moper=50000  ! 0 Threshold for parallel computation
  integer ier, othread, ithread, nthread, lchunk, nchunk
  integer iy0, iy1, iyb, iyf, k
  integer, allocatable :: iy0_a(:), iy1_a(:)
  logical start_thread
  !
  ! Compute IO from first channel number
  io = 7+3*jo-2
  ufac = 100.d0/cell(1)
  vfac = 100.d0/cell(2)
  !
  if (taper(1).ne.0. .and. taper(2).ne.0.) then
    do_taper = .true.
    staper = taper(3)*pi/180.0
    if (taper(1).ne.0) then
      cx = cos(staper)/taper(1)
      sy = sin(staper)/taper(1)
    else
      cx = 0.0
      sy = 0.0
    endif
    if (taper(2).ne.0) then
      cy = cos(staper)/taper(2)
      sx = sin(staper)/taper(2)
    else
      cy = 0.0
      sx = 0.0
    endif
    if (taper(4).ne.0.) then
      etaper = taper(4)/2.0
    else
      etaper = 1
    endif
  else
    do_taper = .false.
  endif
  xinc = mapx(2)-mapx(1)
  xref = nx/2+1
  yinc = mapy(2)-mapy(1)
  yref = ny/2+1
  staper = 1.0
  !
  ! Use symmetry
  my = ny/2+1
  !
  !
  ! The scheduling in Open-MP mode is made with large chunks,
  ! to avoid writing at the same location from two different threads.
  ! The u,v data being sorted in increasing v, a thread writes
  ! its largest iy values in its last visibilities, while the
  ! the next thread has written its smallest iy values (which could
  ! overlap with the previous ones) in its first visibilites.
  !
  !   With large chunks, collisions are in practice avoided.
  !   For small numbers of Visibilities, only one thread is used.
  !
  ! The proper algorithm would
  !   - determine the minimum and maximum pixel (including
  !     support) for each thread IT
  !   - or even better, the visibility VS(IT) at which it starts
  !     to touch the next thread IT+1, or the visibility VF(IT)
  !     where it finishes VF(IT) to touch the previous thread IT-1
  !   - Each thread would have two locks: a backward lock LB(IT)
  !     (used by the previous thread) and a forward lock LF(IT)
  !     (use by the next one).
  !   - At the beginning, all threads are not waiting for locks
  !     All locks are released.
  !   - Thread IT
  !       - Waits for lock FL(IT-1)  (which is normally cleared)
  !       - set the backward lock LB(IT)
  !       - when reaching VF(IT), releases LB(IT)
  !       - when reaching VS(IT), waits for LB(IT+1)
  !       - sets FL(IT)
  !       - when finishing, releases FL(IT)
  !
  ! However, the problem is that adjacent threads overlap twice
  ! Thread IT-1   !           !
  !             -----------------
  ! Thread IT                 !           !
  !                         -----[-----]-----
  ! Thread IT+1                           !          !
  !                                     ----------------
  ! so that the Clean region is only the one within brackets.
  ! This means we should check a double-support guard
  ! to define the locks...
  !
  ! The problem is that we do not know in advance where
  ! a thread finishes, unless we explicitely split our
  ! work in advance into chunks...
  noper = nv*(20+nc)
  othread = 1
  !$  othread = omp_get_max_threads()
  lchunk = (nv+othread-1)/othread
  nchunk = (nv+lchunk-1)/lchunk
  !$  print *,'OMP -- Nthread ',othread,' Chunk ',lchunk,nchunk
  allocate (iy0_a(nchunk),iy1_a(nchunk),stat=ier)
  k =  0
  do i=1,nv,lchunk
    k = k+1
    iy0_a(k) = int((visi(jy,i)+2*sup(2))/yinc+yref)
    iy1_a(k) = int((visi(jy,min(i+lchunk-1,nv))-2*sup(2))/yinc+yref)
  enddo
  !
  print *,'Support ',sup(2)/yinc
  print *,'iy0 ',iy0_a
  print *,'iy1 ',iy1_a
  !
  ! Are chunks large enough ?
  nchunk = othread
  print *,' enter an integer < ',othread
!!  read(5,*) nchunk
  nchunk = max(1,nchunk)
  !
!!  if (noper.lt.moper) then
  if (nchunk.lt.othread) then
    !$  call omp_set_num_threads(nchunk)
    print *,'commuting from ',othread,' to ',nchunk,' thread'
  else
    print *,'Using ',othread,' threads ',noper,' < ',moper
  endif
  !$OMP PARALLEL DEFAULT(none) &
  !$OMP   & SHARED(mapx,mapy,nc,nv,nx,ny,my) &
  !$OMP   & SHARED(jx,jy,cx,sx,cy,sy,etaper,do_taper,io) &
  !$OMP   & SHARED(xinc,xref,yinc,yref,sup,ubias,vbias,ufac,vfac) &
  !$OMP   & PRIVATE(i,u,v,staper,result,resima,ixp,ixm,iyp,iym) &
  !$OMP   & PRIVATE(dv,iv,du,iu,res,iin,iou,iy,ix,ic) &
  !$OMP   & SHARED(map) PRIVATE(ithread,lchunk,nthread) &
  !$OMP   & SHARED(ubuff,vbuff,visi,we) &
  !$OMP   & PRIVATE(iy0,iy1,iyb,iyf,start_thread)
  nthread = 1
  ithread = 1
  !$  nthread = omp_get_num_threads()
  !$  ithread = omp_get_thread_num() + 1
  lchunk = (nv+nthread-1)/nthread
  start_thread = .true.
  !$OMP DO SCHEDULE(STATIC,lchunk)
  ! Start with loop on observed visibilities
  do i=1,nv
    u = visi(jx,i)
    v = visi(jy,i)
    if (start_thread) then
      iy0 = int(v/yinc+yref)
      iy1 = int(visi(jy,min(i+lchunk-1,nv))/yinc+yref)
      print *,'thread ',ithread,' iy0 ',iy0,' iy1 ',iy1
      start_thread = .false.
    endif
    !
    if (do_taper) then
      staper = (u*cx + v*sy)**2 + (-u*sx + v*cy)**2
      if (etaper.ne.1) staper = staper**etaper
      if (staper.gt.64.0) then
        staper = 0.0
      else
        staper = exp(-staper)
      endif
      result = staper*we(i)
    else
      result = we(i)
    endif
    !
    ! Weight and taper
    if (v.gt.0) then
      u = -u
      v = - v
      resima = -result
    else
      resima = result
    endif
    ! Channels
    ! Define map cell
    ixp = int((u-sup(1))/xinc+xref+1.0)
    ixm = int((u+sup(1))/xinc+xref)
    iym = int((v-sup(2))/yinc+yref)
    iyp = min(my,int((v+sup(2))/yinc+yref+1.0))
    !
    ! Lock management
    iyb = int((v-2*sup(2))/yinc+yref)
    if (iyb.gt.iy0) then
      print *,ithread,'Releasing lock for ',ithread-1,'; at ',i,iym,iyp
      iy0 = ny
    endif
    iyf = int((v+2*sup(2))/yinc+yref)
    if (iyf.gt.iy1) then
      print *,ithread,'   Waiting for lock from ',ithread+1,'; at ',i,iym,iyp
      iy1 = 2*ny
    endif
    !
    if (ixm.lt.1.or.ixp.gt.nx.or.iym.lt.1.or.iyp.gt.ny) then
      continue
      !            PRINT *,'Visi ',I,' pixels ',IXM,IXP,IYM,IYP
    else
      do iy=iym,iyp
        dv = v-mapy(iy)
        if (abs(dv).le.sup(2)) then
          iv = nint(dv*vfac+vbias)
          do ix=ixm,ixp
            du = u-mapx(ix)
            if (abs(du).le.sup(1)) then
              iu = nint(du*ufac+ubias)
              res = ubuff(iu)*vbuff(iv)
              iou = 1
              iin = io
              do ic=1,nc
                !$OMP ATOMIC
                map (iou,ix,iy) = map (iou,ix,iy) +   &
     &                  visi(iin,i)*result*res
                iou = iou+1
                iin = iin+1
                !$OMP ATOMIC
                map (iou,ix,iy) = map (iou,ix,iy) +   &
     &                  visi(iin,i)*resima*res
                iou = iou+1
                iin = iin+2
              enddo
              ! Beam
              !$OMP ATOMIC
              map (iou,ix,iy) = map(iou,ix,iy) +   &
     &                res*result
            endif
          enddo
        endif
      enddo
    endif
    !
    ! Borderline case: use symmetry
    u = -u
    v = -v
    resima = -resima
    if (v.le.sup(2)) then
      ! Lock management
      iyb = int((v-2*sup(2))/yinc+yref)
      if (iyb.gt.iy0) then
        print *,ithread,'Releasing lock for ',ithread-1,'; at ',i,iym,iyp
        iy0 = ny
      endif
      iyf = int((v+2*sup(2))/yinc+yref)
      if (iyf.gt.iy1) then
        print *,ithread,'   Waiting for lock from ',ithread+1,'; at ',i,iym,iyp
        iy1 = 2*ny
      endif
      !
      ixp = int((u-sup(1))/xinc+xref+1.0)
      ixm = int((u+sup(1))/xinc+xref)
      iym = int((v-sup(2))/yinc+yref)
      iyp = min(my,int((v+sup(2))/yinc+yref+1.0))
      if (ixm.lt.1.or.ixp.gt.nx.or.iym.lt.1.or.iyp.gt.ny) then
        continue
        !               PRINT *,'Visi ',I,' pixels ',IXM,IXP,IYM,IYP
      else
        do iy=iym,iyp
          dv = v-mapy(iy)
          if (abs(dv).le.sup(2)) then
            iv = nint(dv*vfac+vbias)
            do ix=ixm,ixp
              du = u-mapx(ix)
              if (abs(du).le.sup(1)) then
                iu = nint(du*ufac+ubias)
                res = ubuff(iu)*vbuff(iv)
                iou = 1
                iin = io
                do ic=1,nc
                  !$OMP ATOMIC
                  map (iou,ix,iy) = map (iou,ix,iy) +   &
     &                    visi(iin,i)*result*res
                  iou = iou+1
                  iin = iin+1
                  !$OMP ATOMIC
                  map (iou,ix,iy) = map (iou,ix,iy) +   &
     &                    visi(iin,i)*resima*res
                  iou = iou+1
                  iin = iin+2
                enddo
                ! Beam
                !$OMP ATOMIC
                map (iou,ix,iy) = map(iou,ix,iy) +   &
     &                  res*result
              endif
            enddo
          endif
        enddo
      endif
    endif
  enddo
  !$OMP END DO
  !$OMP END PARALLEL
  !
  ! Apply symmetry  !$OMP DO
  do iy=my+1,ny
    ky = ny+2-iy
    do ix=2,nx
      kx = nx+2-ix
      iou = 1
      do i=1,nc
        map(iou,ix,iy) = map(iou,kx,ky)
        iou = iou+1
        map(iou,ix,iy) = -map(iou,kx,ky)
        iou = iou+1
      enddo
      map(iou,ix,iy) = map(iou,kx,ky)  ! Beam
    enddo
  enddo
  !
  ! Missing row is left empty (assume proper coverage of the UV plane)
  do iy = 1,ny
    if (map(2*nc-1,1,iy).ne.0) then
      print *,'Invalid beam ',iy
    endif
  enddo
  !
  if (noper.lt.moper) then
    !$  call omp_set_num_threads(othread)
    print *,'commuting from 1 to ',othread,' thread'
  endif
end subroutine dofft_quick_debug
!
subroutine dofft_quick_para (np,nv,visi,jx,jy,jo   &
     &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
     &    ubias,vbias,ubuff,vbuff)
  use gkernel_interfaces
  !$  use omp_lib
  !----------------------------------------------------------------------
  ! @ private
  !
  ! GILDAS  MAP_FAST
  !   Compute FFT of image by gridding UV data
  !   - Taper before gridding
  !   - Gridding with pre-computed support
  !   - Uses symmetry
  !----------------------------------------------------------------------
  integer, intent(in) :: nv                   ! number of values
  integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
  real, intent(in) :: visi(np,nv)             ! values
  integer, intent(in) :: nc                   ! number of channels
  integer, intent(in) :: jx                   ! X coord location in VISI
  integer, intent(in) :: jy                   ! Y coord location in VISI
  integer, intent(in) :: jo                   ! first channel to map
  integer, intent(in) :: nx                   ! X map size
  integer, intent(in) :: ny                   ! Y map size
  real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
  real, intent(in) :: mapx(nx)                ! X Coordinates of grid
  real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
  real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
  real, intent(in) :: cell(2)                 ! cell size in Meters
  real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
  real, intent(in) :: we(nv)                  ! Weight array
  real, intent(in) :: vv(nv)                  ! V Values
  real, intent(in) :: ubias                   ! U gridding offset
  real, intent(in) :: vbias                   ! V gridding offset
  real, intent(in) :: ubuff(4096)             ! U gridding buffer
  real, intent(in) :: vbuff(4096)             ! V gridding buffer
  ! Global
  real(8), parameter :: pi=3.141592653589793d0
  ! Local
  integer ix,iy,ic,i,iin,iou,io
  integer ixm,ixp,iym,iyp,iu,iv
  real result,resima,staper,etaper,res
  real u,v,du,dv,ufac,vfac
  real cx,cy,sx,sy
  real(8) xinc,xref,yinc,yref
  logical do_taper
  !
  integer my,kx,ky, noper
  integer, parameter :: moper=50000  ! 0 Threshold for parallel computation
  integer othread,ithread,nthread,lchunk,nchunk
  integer iy0,iy1,iyb,iyf
  logical start_thread
  !
  ! Compute IO from first channel number
  io = 7+3*jo-2
  ufac = 100.d0/cell(1)
  vfac = 100.d0/cell(2)
  !
  if (taper(1).ne.0. .and. taper(2).ne.0.) then
    do_taper = .true.
    staper = taper(3)*pi/180.0
    if (taper(1).ne.0) then
      cx = cos(staper)/taper(1)
      sy = sin(staper)/taper(1)
    else
      cx = 0.0
      sy = 0.0
    endif
    if (taper(2).ne.0) then
      cy = cos(staper)/taper(2)
      sx = sin(staper)/taper(2)
    else
      cy = 0.0
      sx = 0.0
    endif
    if (taper(4).ne.0.) then
      etaper = taper(4)/2.0
    else
      etaper = 1
    endif
  else
    do_taper = .false.
  endif
  xinc = mapx(2)-mapx(1)
  xref = nx/2+1
  yinc = mapy(2)-mapy(1)
  yref = ny/2+1
  staper = 1.0
  !
  ! Use symmetry
  my = ny/2+1
  !
  !
  ! The scheduling in Open-MP mode is made with large chunks,
  ! to avoid writing at the same location from two different threads.
  ! The u,v data being sorted in increasing v, a thread writes
  ! its largest iy values in its last visibilities, while the
  ! the next thread has written its smallest iy values (which could
  ! overlap with the previous ones) in its first visibilites.
  !
  !   With large chunks, collisions are in practice avoided.
  !   For small numbers of Visibilities, only one thread is used.
  !
  ! The proper algorithm would
  !   - determine the minimum and maximum pixel (including
  !     support) for each thread IT
  !   - or even better, the visibility VS(IT) at which it starts
  !     to touch the next thread IT+1, or the visibility VF(IT)
  !     where it finishes VF(IT) to touch the previous thread IT-1
  !   - Each thread would have two locks: a backward lock LB(IT)
  !     (used by the previous thread) and a forward lock LF(IT)
  !     (use by the next one).
  !   - At the beginning, all threads are not waiting for locks
  !     All locks are released.
  !   - Thread IT
  !       - Waits for lock FL(IT-1)  (which is normally cleared)
  !       - set the backward lock LB(IT)
  !       - when reaching VF(IT), releases LB(IT)
  !       - when reaching VS(IT), waits for LB(IT+1)
  !       - sets FL(IT)
  !       - when finishing, releases FL(IT)
  !
  ! However, the problem is that adjacent threads overlap twice
  ! Thread IT-1   !           !
  !             -----------------
  ! Thread IT                 !           !
  !                         -----[-----]-----
  ! Thread IT+1                           !          !
  !                                     ----------------
  ! so that the Clean region is only the one within brackets.
  ! This means we should check a double-support guard
  ! to define the locks...
  !
  ! The problem is that we do not know in advance where
  ! a thread finishes, unless we explicitely split our
  ! work in advance into chunks...
  noper = nv*(20+nc)
  othread = 1
  !$  othread = omp_get_max_threads()
  lchunk = (nv+othread-1)/othread
  nchunk = (nv+lchunk-1)/lchunk
  !$  print *,'Nthread ',othread,' Chunk ',lchunk,nchunk
  !$OMP PARALLEL DEFAULT(none) &
  !$OMP   & SHARED(mapx,mapy,nc,nv,nx,ny,my) &
  !$OMP   & SHARED(jx,jy,cx,sx,cy,sy,etaper,do_taper,io) &
  !$OMP   & SHARED(xinc,xref,yinc,yref,sup,ubias,vbias,ufac,vfac) &
  !$OMP   & PRIVATE(i,u,v,staper,result,resima,ixp,ixm,iyp,iym) &
  !$OMP   & PRIVATE(dv,iv,du,iu,res,iin,iou,iy,ix,ic) &
  !$OMP   & SHARED(map) PRIVATE(ithread,lchunk,nthread) &
  !$OMP   & SHARED(ubuff,vbuff,visi,we) &
  !$OMP   & PRIVATE(iy0,iy1,iyb,iyf,start_thread)
  nthread = 1
  ithread = 1
  !$  nthread = omp_get_num_threads()
  !$  ithread = omp_get_thread_num() + 1
  lchunk = (nv+nthread-1)/nthread
  !$OMP DO SCHEDULE(STATIC,lchunk)
  ! Start with loop on observed visibilities
  do i=1,nv
    u = visi(jx,i)
    v = visi(jy,i)
    !
    if (do_taper) then
      staper = (u*cx + v*sy)**2 + (-u*sx + v*cy)**2
      if (etaper.ne.1) staper = staper**etaper
      if (staper.gt.64.0) then
        staper = 0.0
      else
        staper = exp(-staper)
      endif
      result = staper*we(i)
    else
      result = we(i)
    endif
    !
    ! Weight and taper
    if (v.gt.0) then
      u = -u
      v = - v
      resima = -result
    else
      resima = result
    endif
    ! Channels
    ! Define map cell
    ixp = int((u-sup(1))/xinc+xref+1.0)
    ixm = int((u+sup(1))/xinc+xref)
    iym = int((v-sup(2))/yinc+yref)
    iyp = min(my,int((v+sup(2))/yinc+yref+1.0))
    !
    if (ixm.lt.1.or.ixp.gt.nx.or.iym.lt.1.or.iyp.gt.ny) then
      continue
      !            PRINT *,'Visi ',I,' pixels ',IXM,IXP,IYM,IYP
    else
      do iy=iym,iyp
        dv = v-mapy(iy)
        if (abs(dv).le.sup(2)) then
          iv = nint(dv*vfac+vbias)
          do ix=ixm,ixp
            du = u-mapx(ix)
            if (abs(du).le.sup(1)) then
              iu = nint(du*ufac+ubias)
              res = ubuff(iu)*vbuff(iv)
              iou = 1
              iin = io
              do ic=1,nc
                !$OMP ATOMIC
                map (iou,ix,iy) = map (iou,ix,iy) +   &
     &                  visi(iin,i)*result*res
                iou = iou+1
                iin = iin+1
                !$OMP ATOMIC
                map (iou,ix,iy) = map (iou,ix,iy) +   &
     &                  visi(iin,i)*resima*res
                iou = iou+1
                iin = iin+2
              enddo
              ! Beam
              !$OMP ATOMIC
              map (iou,ix,iy) = map(iou,ix,iy) +   &
     &                res*result
            endif
          enddo
        endif
      enddo
    endif
    !
    ! Borderline case: use symmetry
    u = -u
    v = -v
    resima = -resima
    if (v.le.sup(2)) then
      !
      ixp = int((u-sup(1))/xinc+xref+1.0)
      ixm = int((u+sup(1))/xinc+xref)
      iym = int((v-sup(2))/yinc+yref)
      iyp = min(my,int((v+sup(2))/yinc+yref+1.0))
      if (ixm.lt.1.or.ixp.gt.nx.or.iym.lt.1.or.iyp.gt.ny) then
        continue
        !               PRINT *,'Visi ',I,' pixels ',IXM,IXP,IYM,IYP
      else
        do iy=iym,iyp
          dv = v-mapy(iy)
          if (abs(dv).le.sup(2)) then
            iv = nint(dv*vfac+vbias)
            do ix=ixm,ixp
              du = u-mapx(ix)
              if (abs(du).le.sup(1)) then
                iu = nint(du*ufac+ubias)
                res = ubuff(iu)*vbuff(iv)
                iou = 1
                iin = io
                do ic=1,nc
                  !$OMP ATOMIC
                  map (iou,ix,iy) = map (iou,ix,iy) +   &
     &                    visi(iin,i)*result*res
                  iou = iou+1
                  iin = iin+1
                  !$OMP ATOMIC
                  map (iou,ix,iy) = map (iou,ix,iy) +   &
     &                    visi(iin,i)*resima*res
                  iou = iou+1
                  iin = iin+2
                enddo
                ! Beam
                !$OMP ATOMIC
                map (iou,ix,iy) = map(iou,ix,iy) +   &
     &                  res*result
              endif
            enddo
          endif
        enddo
      endif
    endif
  enddo
  !$OMP END DO
  !$OMP END PARALLEL
  !
  ! Apply symmetry  !$OMP DO
  do iy=my+1,ny
    ky = ny+2-iy
    do ix=2,nx
      kx = nx+2-ix
      iou = 1
      do i=1,nc
        map(iou,ix,iy) = map(iou,kx,ky)
        iou = iou+1
        map(iou,ix,iy) = -map(iou,kx,ky)
        iou = iou+1
      enddo
      map(iou,ix,iy) = map(iou,kx,ky)  ! Beam
    enddo
  enddo
  !
  ! Missing row is left empty (assume proper coverage of the UV plane)
  do iy = 1,ny
    if (map(2*nc-1,1,iy).ne.0) then
      print *,'Invalid beam ',iy
    endif
  enddo
  !
  if (noper.lt.moper) then
    !$  call omp_set_num_threads(othread)
    print *,'commuting from 1 to ',othread,' thread'
  endif
end subroutine dofft_quick_para
