!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module uv_stack
  use gbl_message
  !
  public :: uv_stack_comm
  private
  !
contains
  !
  subroutine uv_stack_comm(line,error)
    use gkernel_interfaces
    use image_def
    use file_buffers
    use uv_buffers
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(gildas) :: ouuvh
    real, pointer :: inuvd(:,:),ouuvd(:,:)
    integer(kind=4), parameter :: one=1
    character(len=*), parameter :: rname='UV_STACK_COMM'
    !
    ! Get information on input uv table
    if (huv%loca%size.eq.0) then
      call map_message(seve%e,rname,'No UV data loaded')
      error = .true.
      return
    endif
    !
    ! Update the header of the output uv table
    call gildas_null(ouuvh,type='UVT')
    call gdf_copy_header(huv,ouuvh,error)
    ouuvh%gil%dim(2) = one
    ouuvh%gil%nvisi  = one
    !
    ! User feedback
    call map_message(seve%i,rname,'Stacking visibilities')
    !
    ! Prepare appropriate array
    nullify(inuvd,ouuvd)
    call uv_find_buffers(rname,int(ouuvh%gil%dim(1),kind=4),int(ouuvh%gil%dim(2),kind=4),inuvd,ouuvd,error)
    if (error) return
    !
    ! Actually stack
    call uv_stack_data(huv,inuvd,ouuvh,ouuvd,error)
    if (error) return
    !
    ! Everything went fine => copy output header in global buffer that contains input header up to now
    call gdf_copy_header(ouuvh,huv,error)
    ! Set global UV pointer to the output data and free the input data
    call uv_clean_buffers(inuvd,ouuvd,error)
    if (error) return
    ! UV data not plotted
    if (allocated(uvtb%data)) deallocate(uvtb%data)
    uv_plotted = .false.
    ! Indicate optimization and save status
    optimize(code_save_uv)%change = optimize(code_save_uv)%change + 1
    save_data(code_save_uv) = .true.
    ! Recompute weight
    do_weig = .true.
    !
    ! Redefine SIC variables 
    call sic_delvariable('UV',.false.,error)
    call sic_mapgildas('UV',huv,error,duv) 
  end subroutine uv_stack_comm
  !
  subroutine uv_stack_data(inuvh,inuvd,ouuvh,ouuvd,error)
    use gbl_message
    use image_def
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(gildas),         intent(in)    :: inuvh
    real(kind=4), target, intent(in)    :: inuvd(:,:)
    type(gildas),         intent(in)    :: ouuvh
    real(kind=4), target, intent(inout) :: ouuvd(:,:)
    logical,              intent(inout) :: error
    !
    integer(kind=4) :: ic,nc
    integer(kind=4) :: iv,nv
    real(kind=4), pointer :: weight
    integer(kind=4), parameter :: one=1
    character(len=*), parameter :: rname='UV_STACK_DATA'
    !
    call map_message(seve%t,rname,'Welcome')
    !
    nv = inuvh%gil%dim(2)
    nc = inuvh%gil%nchan
    !
    ! Sum visilibities
    ouuvd(:,one) = 0.0
    do iv=1,nv
       do ic=1,nc
          weight => inuvd(7+3*ic,iv)
          ! Real part
          ouuvd(5+3*ic,one) = ouuvd(5+3*ic,one) + weight*inuvd(5+3*ic,iv)
          ! Imaginary part
          ouuvd(6+3*ic,one) = ouuvd(6+3*ic,one) + weight*inuvd(6+3*ic,iv)
          ! Weight
          ouuvd(7+3*ic,one) = ouuvd(7+3*ic,one) + weight
       enddo ! ic
    enddo ! iv
    ! Normalize the result
    do ic=1,nc
       weight => ouuvd(7+3*ic,one)
       ouuvd(5+3*ic,one) = ouuvd(5+3*ic,one)/weight
       ouuvd(6+3*ic,one) = ouuvd(6+3*ic,one)/weight
    enddo ! ic
  end subroutine uv_stack_data
end module uv_stack
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
