!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module clean_clark
  use gbl_message
  !
  public :: clark_comm
  private
  !
contains
  !
  subroutine clark_comm(line,error)
    use gkernel_interfaces
    use mapping_interfaces
    use clean_tool, only: sub_clean,clean_data
    use clean_types, only: beam_unit_conversion
    use clean_buffers, only: clean_user,clean_prog
    !----------------------------------------------------------------------
    ! Implementation of Barry Clark Clean
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    integer :: iv,na
    character(len=8) :: name,argum,voc1(2)
    data voc1/'CLEAN','RESIDUAL'/
    !
    ! Data checkup
    clean_user%method = 'CLARK'
    call clean_data(error)
    if (error) return
    !
    ! Parameter Definitions
    call beam_unit_conversion(clean_user)
    call clean_user%copyto(clean_prog)
    clean_prog%pflux = sic_present(1,0)
    clean_prog%pcycle = sic_present(2,0)
    clean_prog%qcycle = sic_present(3,0)
    if (clean_prog%pcycle) then
       argum = 'RESIDUAL'
       call sic_ke(line,2,1,argum,na,.false.,error)
       if (error) return
       call sic_ambigs('PLOT',argum,name,iv,voc1,2,error)
       if (error) return
       clean_prog%pclean = iv.eq.1
    else
       clean_prog%pclean = .false.
    endif
    call sub_clean(line,error)
  end subroutine clark_comm
end module clean_clark
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
