!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module uv_stokes
  use gbl_message
  !
  public :: uv_stokes_comm
  private
  !
contains
  !
  subroutine uv_stokes_comm(line,error)
    use gildas_def
    use gkernel_interfaces
    !---------------------------------------------------------------------
    ! UV_STOKES Type FileIn [FileOut] 
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    character(len=filename_length) :: namei, nameo
    character(len=commandline_length) :: chain
    character(len=4) :: the_stokes,stokes(9)
    character(len=12) :: argu
    integer :: i,n,ni,no
    character(len=*), parameter :: rname='UV_STOKES'
    !
    data stokes/'NONE', 'I', 'Q', 'U', 'V', 'LL', 'RR', 'HH', 'VV'/
    !
    call sic_ke(line,0,1,argu,n,.true.,error)
    call sic_ambigs (rname,argu,the_stokes,i,stokes,9,error)
    if (error) return
    !
    n = sic_narg(0)
    if (n.eq.3) then
       call sic_ch(line,0,2,namei,ni,.true.,error)
       call sic_ch(line,0,3,nameo,no,.true.,error)
       chain = '@ nostokes '//trim(the_stokes)//' '//namei(1:ni)//' '//nameo(1:no)
    else if (n.eq.2) then
       call sic_ch(line,0,2,namei,ni,.true.,error)
       chain = '@ nostokes '//trim(the_stokes)//' '//namei(1:ni)
    else
       call gag_message(seve%e,rname,'Invalid number of arguments')
       error = .true.
       return
    endif
    !
    call exec_program(chain)
  end subroutine uv_stokes_comm
end module uv_stokes
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
