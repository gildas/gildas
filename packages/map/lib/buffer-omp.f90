!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module omp_buffers
  !------------------------------------------------------------------------
  ! OPEN-MP control parameters
  !------------------------------------------------------------------------
  !
  public :: omp
  private
  !
  type omp_buffer_user_t
     logical, public  :: debug = .false.
     integer, public  :: grid = 0
     logical, public  :: uvmap_lock = .false.
     integer, public  :: single_beam = 0
   contains
     procedure, public :: init   => omp_buffer_user_init
     procedure, public :: sicdef => omp_buffer_user_sicdef
  end type omp_buffer_user_t
  type(omp_buffer_user_t) :: omp
  !
contains
  !
  subroutine omp_buffer_user_init(user,error)
    !-----------------------------------------------------------------------
    ! Initialize by setting the intent to out
    !-----------------------------------------------------------------------
    class(omp_buffer_user_t), intent(out)   :: user
    logical,                  intent(inout) :: error
  end subroutine omp_buffer_user_init
  !
  subroutine omp_buffer_user_sicdef(user,error)
    use gkernel_interfaces
    !----------------------------------------------------------------------
    ! OMP variables
    !----------------------------------------------------------------------
    class(omp_buffer_user_t), intent(out)   :: user
    logical,                  intent(inout) :: error
    !
    call sic_defstructure('OMP_MAP',.true.,error)
    if (error) return
    call sic_def_logi('OMP_MAP%DEBUG',omp%debug,.false.,error)
    if (error) return
    call sic_def_inte('OMP_MAP%GRID',omp%grid,0,0,.false.,error)
    if (error) return
    call sic_def_logi('OMP_MAP%UV_MAP_LOCK',omp%uvmap_lock,.false.,error)
    if (error) return
    call sic_def_inte('OMP_MAP%SINGLE_BEAM_CODE',omp%single_beam,0,0,.false.,error)
    if (error) return
  end subroutine omp_buffer_user_sicdef
end module omp_buffers
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
