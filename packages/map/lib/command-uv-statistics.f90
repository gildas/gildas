!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module uv_statistics
  use gbl_message
  !
  public :: uv_statistics_comm
  private
  !
contains
  !
  subroutine uv_statistics_comm(line,error)
    use gkernel_interfaces
    use mapping_interfaces, only: uvgmax
    use uvstat_tool, only: uv_listheader,uniform_beam
    use uvmap_tool, only: map_prepare
    use uv_rotate_shift_and_sort_tool, only: uv_sort_main
    use uv_continuum, only: map_beams,map_parameters
    use uvmap_types
    use uv_buffers
    use uvmap_buffers
    !------------------------------------------------------------------------
    ! Analyse a UV data set to define approximate beam size,
    ! field of view, expected "best beam", etc...
    !
    ! Input :
    !     a precessed UV table
    ! Output :
    !     a precessed, rotated, shifted UV table, sorted in V,
    !     ordered in (U,V,W,D,T,iant,jant,nchan(real,imag,weig))
    !     a beam image ?
    !------------------------------------------------------------------------
    character(len=*), intent(inout) :: line
    logical,          intent(inout) :: error
    !
    real(kind=8), parameter :: pi=3.14159265358979323846d0
    real(kind=8), parameter :: f_to_k = 2.d0*pi/299792458.d-6
    !
    integer, parameter :: mt=100
    integer datelist(mt)
    !
    type (uvmap_par), save :: amap  ! To get the New variable names
    !
    integer ndates
    character(len=8) mode, argum
    real, allocatable :: beams(:,:), fft(:)
    real uv_taper(3),map_cell(2),uniform(2),uvmax,uvmin,myuvm
    real start,step,taper
    integer map_size(2),wcol,ctype,n,mcol(2),ier
    integer(kind=index_length) :: dim(4)
    logical sorted, shift
    real(kind=8) :: freq, new(3)
    real(4), save :: bmax, bmin
    logical, save :: first = .true.
    real, save :: result(10,8)
    logical :: print
    character(len=message_length) :: chain
    integer, parameter :: mmode=8
    character(len=8) :: smode(mmode)
    data smode /'ADVISE','ALL','BEAM','CELL','HEADER','SETUP','TAPER','WEIGHT'/
    character(len=*), parameter :: rname = 'UV_STATISTICS'
    !
    if (sic_present(0,1)) then
       call sic_ke(line,0,1,argum,n,.false.,error)
       if (error) return
       call sic_ambigs (rname,argum,mode,n,smode,mmode,error)
       if (error) return
    else
       mode = 'ALL'
    endif
    !
    freq = gdf_uv_frequency(huv)
    !
    select case (mode)
    case ('CELL','TAPER','WEIGHT')
       !
       start = 0.0
       call sic_r4(line,0,2,start,.false.,error)
       start = abs(start)
       step = 0.0
       call sic_r4(line,0,3,step,.false.,error)
       step = abs(step)
       !
       ctype = 1
       call map_prepare(rname,amap,error)
       uniform(:) = amap%uniform(:)
       uv_taper(:) = amap%taper(1:3) 
       ! Unclear ...
       !    call sic_get_inte('WCOL',wcol,error)
       ! This were useless anyway...
       !    call sic_get_inte('MCOL[1]',mcol(1),error)
       !    call sic_get_inte('MCOL[2]',mcol(2),error)
       !    !
       if (error) return
       !
       ! First sort the input UV Table, leaving UV Table in UV_*
       shift = .false.
       call uv_sort_main(error,sorted,shift,new,uvmax,uvmin)
       if (error) return
    case ('ADVISE','ALL','HEADER','SETUP','BEAM')
       ! For HEADERS, do the minimum work for speed...
       ! The UV table is available in HUV%
       if (huv%loca%size.eq.0) then
          call map_message(seve%e,rname,'No UV data loaded')
          error = .true.
          return
       endif
       call uvgmax(huv,duv,uvmax,uvmin)
       ! Now transform UVMAX in kiloWavelength (including 2 pi factor)
       uvmax = uvmax*freq*f_to_k
       uvmin = uvmin*freq*f_to_k
       error = .false.
    end select
    !
    ! New code: For the time being, this is somewhat inconsistent
    ! with the use of uv_sort_main which uses a different frequency
    !
    bmax = uvmax/(freq*f_to_k)
    bmin = uvmin/(freq*f_to_k)
    if (first) then
       !    call sic_defstructure('UVSTAT',.true.,error)
       !    call sic_def_real('UVSTAT%BMAX',bmax,0,dim,.false.,error)
       !    call sic_def_real('UVSTAT%BMIN',bmin,0,dim,.false.,error)
       call sic_def_real('UV_BMAX',bmax,0,dim,.false.,error)
       call sic_def_real('UV_BMIN',bmin,0,dim,.false.,error)
       first = .false.
    endif
    ! 
    call uvmap_default%copyto(uvmap_prog)
    call map_parameters(rname,uvmap_prog,freq,uvmax,uvmin,error,print)
    if(error) return
    select case (mode)
    case ('BEAM') 
       call map_beamsize(huv,duv,bmax,error)
    case ('HEADER') ! That's all for HEADER
       call gdf_print_header(huv)
       call uv_listheader(huv,duv,mt,datelist,ndates,freq)
       call uv_printoffset('HEADER',uvmap_prog)
    case ('ADVISE')
       error = .false.
    case ('ALL')
       call gdf_print_header(huv)
       call uv_listheader(huv,duv,mt,datelist,ndates,freq)
       call uv_printoffset('HEADER',uvmap_prog)
    case ('SETUP')
       call uvmap_prog%copyto(uvmap_default) ! Copy Back ...
       error = .false.
    case default
       !
       ! back to usual code
       myuvm = uvmax/(freq*f_to_k)
       !
       if (mode.eq.'TAPER') then
          if (step.eq.0.) step = sqrt(2.0)
          if (start.eq.0.) start = 10*nint(myuvm/160.0)
          taper = 16*start
          if (trim(uvmap_prog%mode).eq.'NATURAL') then
             uniform(1) = 0.
             uniform(2) = 0.
          endif
       else
          taper = sqrt(uv_taper(1)*uv_taper(2))
          if (taper.ne.0) then
             taper = min(taper,myuvm)
          else
             taper = myuvm
          endif
       endif
       !
       ! Define MAP_CELL and MAP_SIZE
       taper = myuvm*freq*f_to_k
       map_cell(1) = 0.02*nint(180.0*3600.0*50.0/taper/4.0)
       if (map_cell(1).le.0.02) then
          map_cell(1) = 0.002*nint(180.0*3600.0*500.0/taper/4.0)
       endif
       map_cell(2) = map_cell(1)    ! In ", rounded to 0.01"
       if (mode.eq.'TAPER') then
          map_size(1) = 256
          map_size(2) = 256
       else
          map_size(1) = 64
          map_size(2) = 64
       endif
       !
       write(chain,102) myuvm,uvmax*1e-3/2.0/pi
       call map_message(seve%i,rname,chain)
       write(chain,103) map_size
       call map_message(seve%i,rname,chain)
       write(chain,104) map_cell
       call map_message(seve%i,rname,chain)
       !
       ! Redefine some parameters
       map_cell(1) = map_cell(1)*pi/180.0/3600.0   ! In radians
       map_cell(2) = map_cell(2)*pi/180.0/3600.0
       !
       ! Process sorted UV Table according to the type of beam produced
       n = 2*max(map_size(1),map_size(2))
       allocate (beams(map_size(1),map_size(2)),fft(n),stat=ier)
       if (ier.ne.0) then
          call map_message(seve%e,rname,'Memory allocation failure')
          error = .true.
          return
       endif
       call uniform_beam(' ',uv_taper,   &
            &    map_size,map_cell,uniform,wcol,mcol,fft,   &
            &    error,mode,beams,1,start,step,myuvm,result,huv,duv)
       deallocate (beams,fft)
       !
       call sic_delvariable('BEAM_SHAPE',.false.,error)
       dim(1) = 10
       dim(2) =  8
       call sic_def_real('BEAM_SHAPE',result,2,dim,.true.,error)
       error = .false.
    end select
    !
102 format('Maximum baseline is   ',f8.1,' m,  ',f8.1,' kWavelength')
103 format('Map size is   ',i4,' by ',i4)
104 format('Pixel size is ',f6.3,' by ',f5.3,'"')
  end subroutine uv_statistics_comm
  !
  subroutine uv_printoffset(rname,map)
    use uvmap_types
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    character(len=*), intent(in) :: rname
    type(uvmap_par),  intent(in) :: map
    !
    real(8), parameter :: pi=3.14159265358979323846d0
    real(4), parameter :: rad_to_sec = 180.0*3600.0/pi
    integer :: n,j
    character(len=60) :: chain
    !
    if (map%nfields.eq.0) return
    !
    n = abs(map%nfields)
    if (map%nfields.lt.0) then
       write(chain,'(I5,A)') n, ' Phase offsets in mosaic'
    else
       write(chain,'(I5,A)') n, ' Pointing offsets in mosaic'
    endif
    call map_message(seve%i,rname,chain)
    do j=1,n-1,2
       write(*,'(2(A,F12.4,A,F12.4,A))') &
            &   '(',map%offxy(1,j)*rad_to_sec,' ,',map%offxy(2,j)*rad_to_sec,' )', &
            &   '(',map%offxy(1,j+1)*rad_to_sec,' ,',map%offxy(2,j+1)*rad_to_sec,' )'
    enddo
    if (mod(n,2).ne.0)     write(*,'(A,F12.2,A,F12.2,A)') &
         &   '(',map%offxy(1,n)*rad_to_sec,' ,',map%offxy(2,n)*rad_to_sec,' )'
  end subroutine uv_printoffset
  !
  subroutine map_beamsize(huv,duv,uvmax,error)
    use image_def
    use gkernel_interfaces
    use fit_beam_tool
    use uvstat_tool, only: doqfft
    !------------------------------------------------------------------------
    ! Get the natural synthesized beam size
    !------------------------------------------------------------------------
    type(gildas), intent(in)    :: huv      ! UV Header
    real,         intent(in)    :: duv(:,:) ! UV data
    real,         intent(in)    :: uvmax    ! Maximum UV value
    logical,      intent(inout) :: error
    !
    character(len=*), parameter :: rname='MAP_BEAMSIZE'
    real(8), parameter :: pi=3.14159265358979323846d0
    real(8), parameter :: f_to_k = 2.d0*pi/299792458.d-6
    !
    ! Customized values
    real(4) :: pixel_per_beam=4.0     ! A good compromise
    integer, parameter :: nsize=128   ! Large enough for most cases
    real, save :: abeam(nsize,nsize), work(2*nsize)
    ! FFT arrays
    complex :: ipft(nsize*nsize), ipf(nsize*nsize)
    integer :: lx,ly,lw, nu,nv, nx,ny, ix_patch, iy_patch
    integer :: ndim, nn(2)
    real :: uvcell, map_cell, thre, rtmp
    real(kind=8) :: convert(6)
    real :: major_axis, minor_axis, pos_angle 
    !
    nu = huv%gil%dim(1)
    nv = huv%gil%nvisi
    lx = nsize  ! Grid size
    ly = nsize
    nx = nsize  ! Map size
    ny = nsize
    !
    ! Compute FFT's
    lw = max(10,7 + 3*(2*huv%gil%nchan/3))   ! Weight channel
    !
    ! Make sure all baselines are accounted for
    ! and beam is properly sampled
    uvcell = 2.0*uvmax/(lx-2)  * pixel_per_beam  
    !
    call doqfft (nu,nv,   &      ! Size of visibility array
         &    duv,          &      ! Visibilities
         &    1,2,lw,       &      ! U, V pointers
         &    lx,ly,        &      ! Cube size
         &    ipft,         &      ! FFT cube
         &    uvcell)              ! U and V grid coordinates
    !
    ! Make beam, not normalized
    ndim = 2
    nn(1) = nx
    nn(2) = ny
    call fourt_plan(ipf,nn,ndim,-1,1)
    call extracs(1,nx,ny,1,ipft,ipf,lx,ly)
    call fourt  (ipf,nn,ndim,-1,1,work)
    call cmtore (ipf,abeam,nx,ny)
    !
    ! Normalize beam - no grid correction needed here
    rtmp = abeam(nx/2+1,ny/2+1)
    abeam = abeam/rtmp
    !
    ! Define MAP_CELL
    rtmp = uvmax*huv%gil%freq*f_to_k
    map_cell = 0.002*nint(180.0*3600.0*500.0/rtmp/pixel_per_beam)
    map_cell = map_cell*pi/180.0/3600.0
    convert = (/dble(nx/2+1),0.d0,-dble(map_cell),   &
         &    dble(ny/2+1),0.d0,dble(map_cell)/)
    !
    ! Get Fitting Threshold 
    rtmp  = minval(abeam(:,:)) 
    thre = max(min(abs(-1.5*rtmp),0.7),0.3)
    !
    ! Fit beam
    ix_patch = nx/2
    iy_patch = ny/2
    major_axis = 0.
    minor_axis = 0.
    pos_angle  = 0.
    call fibeam (rname,abeam,nx,ny,   &
         &      ix_patch,iy_patch,thre,   &
         &      major_axis,minor_axis,pos_angle,   &
         &      convert,error)
    !
    if (sic_present(0,2)) then
       call gr_exec('CLEAR')
       call gr4_rgive(nx,ny,convert,abeam)
       call gr_exec('LIMITS /RG')
       call gr_exec('SET BOX SQUARE')
       call gr_exec('PLOT')
       call gr_exec('BOX /UNIT SEC')
       call gr_exec('WEDGE')
    endif
  end subroutine map_beamsize
end module uv_statistics
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
