!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module primary_buffers
  use mapping_types
  !------------------------------------------------------------------------
  ! Primary beam correction
  !------------------------------------------------------------------------
  !
  public :: primary_buffer
  public :: sky
  private
  !
  type(mapping_3d_t), target, save :: sky ! Primary beam corrected cube
  !
  type primary_buffer_user_t
   contains
     procedure, public :: init   => primary_buffer_user_init
     procedure, public :: sicdef => primary_buffer_user_sicdef
  end type primary_buffer_user_t
  type(primary_buffer_user_t) :: primary_buffer
  !
contains
  !
  subroutine primary_buffer_user_init(user,error)
    use gkernel_interfaces
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(primary_buffer_user_t), intent(out)   :: user
    logical,                      intent(inout) :: error
    !
    call gildas_null(sky%head)
  end subroutine primary_buffer_user_init
  !
  subroutine primary_buffer_user_sicdef(user,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(primary_buffer_user_t), intent(inout) :: user
    logical,                      intent(inout) :: error
    !
    ! Placeholder => Do nothing for the moment
  end subroutine primary_buffer_user_sicdef
end module primary_buffers
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
