!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module mapping_mosaic
  use gbl_message
  !
  public :: mosaic_comm,mosaic_main
  private
  !
contains
  !
  subroutine mosaic_comm(line,error)
    use gkernel_interfaces
    !----------------------------------------------------------------------
    ! MOSAIC ON|OFF
    ! Activate or desactivate the mosaic mode
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    integer :: na,iv
    character(len=8) :: name,argum,voc1(2)
    data voc1/'OFF','ON'/
    !
    ! Mosaic mode. Default = .true.
    argum = 'ON'
    call sic_ke(line,0,1,argum,na,.false.,error)
    if (error) return
    call sic_ambigs('MOSAIC',argum,name,iv,voc1,2,error)
    if (error) return
    call mosaic_main(name,error)
  end subroutine mosaic_comm
  !
  subroutine mosaic_main(name,error)
    use phys_const
    use gkernel_interfaces
    use uvmap_buffers
    use clean_buffers, only: clean_user
    !----------------------------------------------------------------------
    ! Activate or desactivate the mosaic mode
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: name
    logical,          intent(inout) :: error
    !
    integer :: nf
    logical :: mosaic, old_mosaic
    real :: prim_beam
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='MOSAIC'
    !
    old_mosaic = clean_user%mosaic
    mosaic = name.eq.'ON'
    !
    if (mosaic) then
       if (old_mosaic) then
          call map_message(seve%i,rname,'Already in MOSAIC mode')
       else
          call map_message(seve%i,rname,'Switch to MOSAIC mode')
          call gprompt_set('MOSAIC')
       endif
       if (clean_user%trunca.ne.0.) then
          nf = primbeam%head%gil%dim(1)
          ! Should that be replaced by something else ? e.g. in the Telescope Section ?
          prim_beam = primbeam%head%gil%convert(3,4)   ! convention
          write(mess,100) 'Last mosaic loaded: ',nf,' fields'
          call map_message(seve%i,rname,mess)
          write(mess,101) 'Primary beam (arcsec) = ',prim_beam*180*3600/pi
          call map_message(seve%i,rname,mess)
          write(mess,101) 'Truncation level B_MIN = ',clean_user%trunca
          call map_message(seve%i,rname,mess)
       else
          call map_message(seve%w,rname,'No mosaic loaded so far')
       endif
       write(mess,101) 'Current value: SEARCH_W = ',clean_user%search
       call map_message(seve%i,rname,mess)
       write(mess,101) 'Current value: RESTORE_W = ',clean_user%restor
       call map_message(seve%i,rname,mess)
       clean_user%mosaic = .true.
    else
       if (.not.old_mosaic) then
          call map_message(seve%i,rname,'Already in NORMAL mode')
       else
          call map_message(seve%i,rname,'Switch to NORMAL mode')
          call gprompt_set('MAPPING')
          clean_user%trunca = 0.0
          call sic_delvariable('PRIMARY',.false.,error)
          primbeam%head%gil%dim(1) = 1
       endif
       clean_user%mosaic = .false.
    endif
    !
100 format(a,i3,a)
101 format(a,f5.2)
  end subroutine mosaic_main
end module mapping_mosaic
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
