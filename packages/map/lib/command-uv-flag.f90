!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module uv_flag
  use gbl_message
  !
  public :: uv_flag_comm
  private
  !
contains
  !
  subroutine uv_flag_comm(line,error)
    use gkernel_interfaces
    use mapping_show_or_view
    use file_buffers
    use clean_support_tool
    use uv_buffers
    !----------------------------------------------------------------------
    ! UV_FLAG [/ANTENNA] [/RESET]
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    integer :: iant
    integer, parameter :: O_RESET=2
    integer, parameter :: O_ANT=1
    !
    ! Parse input line
    if (sic_present(O_RESET,0)) then  ! /RESET
       ! Reset flag array
       call reset_uvflag(uvtb%data,uvtb%head%gil%dim(1),uvtb%head%gil%dim(2))
    else if (sic_present(O_ANT,0) ) then  ! /ANTENNA
       call sic_i4(line,O_ANT,1,iant,.true.,error)
       if (error) return
       ! Plot data
       call sic_let_logi('uvshow%flag',.true.,error)
       call show_or_view_main('UV_FLAG',2,line,error)
       if (error) return
       call get_uvflag_ant(uvtb%data,uvtb%head%gil%dim(1),uvtb%head%gil%dim(2),iant)
    else
       ! Plot data
       call sic_let_logi('uvshow%flag',.true.,error)
       call show_or_view_main('UV_FLAG',2,line,error)
       if (error) return
       ! Make user to define a polygon from cursor
       call greg_poly_define('UV_FLAG','',.false.,supportpol,supportvar,error)
       if (error)  return
       ! Get flag array
       call get_uvflag(uvtb%data,uvtb%head%gil%dim(1),uvtb%head%gil%dim(2))
       call sic_let_logi('uvshow%flag',.false.,error)
    endif
    ! Apply flag array to buffer
    call apply_uvflag(uvtb%data,uvtb%head%gil%dim(1),uvtb%head%gil%dim(2)-3,duv)
    do_weig = .true.
    optimize(code_save_uv)%lastnc = -1 !  means UV data.
  end subroutine uv_flag_comm
  !
  subroutine get_uvflag_ant(uvs,nv,nd,iant)
    use gildas_def
    !----------------------------------------------------------------------
    ! Set uvflag array from antenna number
    !----------------------------------------------------------------------
    integer(kind=index_length), intent(in)    :: nv,nd
    real,                       intent(inout) :: uvs(nv,nd)
    integer,                    intent(in)    :: iant
    !
    ! Local variables
    integer ne,nf,i
    !
    ne = nd-1
    nf = nd-2
    do i=1,nv
       if (uvs(i,6).eq.iant .or. uvs(i,7).eq.iant) then
          uvs(i,nf) = 0.0
       endif
    enddo
  end subroutine get_uvflag_ant
  !
  subroutine get_uvflag(uvs,nv,nd)
    use gildas_def
    use clean_support_tool
    !----------------------------------------------------------------------
    ! Get uvflag array from polygon (0 => Flag, 1 => Keep)
    !----------------------------------------------------------------------
    integer(kind=index_length), intent(in)    :: nv,nd
    real,                       intent(inout) :: uvs(nv,nd)
    !
    integer ne,nf
    !
    ne = nd-1
    nf = nd-2
    !
    ! Oh gosh, all that logic assumes the plotted stuff is in
    ! ne, nd, nf ...
    call gr4_inout(supportpol,uvs(1,ne),uvs(1,nd),uvs(1,nf),nv,.true.)
  end subroutine get_uvflag
  !
  subroutine reset_uvflag(uvs,nv,nd)
    use gildas_def
    !----------------------------------------------------------------------
    ! Reset uvflag array to 1
    !----------------------------------------------------------------------
    integer(kind=index_length), intent(in)    :: nv,nd
    real,                       intent(inout) :: uvs(nv,nd)
    !
    integer :: nf,i
    !
    nf = nd-2
    do i=1,nv
       uvs(i,nf) = 1.0
    enddo
  end subroutine reset_uvflag
  !
  subroutine apply_uvflag(uvs,nv,nd,uv)
    use gildas_def
    !----------------------------------------------------------------------
    ! Apply uvflag array by making negative the weight of the flagged data
    !----------------------------------------------------------------------
    integer(kind=index_length), intent(in)    :: nv,nd
    real,                       intent(inout) :: uvs(nv,nd)
    real,                       intent(inout) :: uv(nd,nv)
    !
    real :: factor
    integer :: i,j
    !
    ! Loop on visibilities
    do i=1,nv
       ! Apply uvflag array
       factor = 2*uvs(i,nd+1)-1    ! 1 or -1 depending on selected option
       do j=10,nd,3
          uvs(i,j) = abs(uvs(i,j))*factor
       enddo
       ! Transpose uvs completely because uv and uvs may be ordered differently...
       do j=1,nd
          uv(j,i) = uvs(i,j)
       enddo
    enddo
  end subroutine apply_uvflag
end module uv_flag
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
