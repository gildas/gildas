!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage the MAPPING package
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module mapping_package
  use gkernel_interfaces ! Absolutely needs to be here
  !
  public :: mapping_pack_init,mapping_pack_on_exit
  private
  !
contains
  !
  subroutine mapping_pack_init(gpack_id,error)
    use sic_def ! Definition of backslash
    !
    use clean_language
    use clean_support_tool
    use file_buffers
    use map_buffers
    use omp_buffers
    use plot_buffers
    use uv_buffers
    use uvmap_buffers
    use clean_buffers
    use primary_buffers
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    integer(kind=4), intent(in)    :: gpack_id
    logical,         intent(inout) :: error
    !
    ! Library initializations
    call omp%init(error)
    if (error) return
    call omp%sicdef(error)
    if (error) return
    !
    call map_buffer%init(error)
    if (error) return
    call map_buffer%sicdef(error)
    if (error) return
    !
    call file_buffer%init(error)
    if (error) return
    call file_buffer%sicdef(error)
    if (error) return
    !
    call plot_buffer%init(error)
    if (error) return
    call plot_buffer%sicdef(error)
    if (error) return
    !
    call uv_buffer%init(error)
    if (error) return
    call uv_buffer%sicdef(error)
    if (error) return
    !
    call uvmap_buffer%init(error)
    if (error) return
    call uvmap_buffer%sicdef(error)
    if (error) return
    !
    call clean_buffer%init(error)
    if (error) return
    call clean_buffer%sicdef(error)
    if (error) return
    !
    call primary_buffer%init(error)
    if (error) return
    call primary_buffer%sicdef(error)
    if (error) return
    !
    call clean_support%init(error)
    if (error) return
    call clean_support%sicdef(error)
    if (error) return
    !
    ! Language initializations (must come after the library initializations)
    call clean_language_register(error)
    if (error) return
    !
    ! One time initialization by alphabetical order
    call map_message_set_id(gpack_id)
    !
    ! Language priorities
    call exec_program('sic'//backslash//'sic priority 1 clean')
  end subroutine mapping_pack_init
  !
  subroutine mapping_pack_on_exit(error)
    use file_buffers
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    call file_buffer%save(error)
    if (error) return
  end subroutine mapping_pack_on_exit
end module mapping_package
!
subroutine mapping_pack_set(pack)
  use gpack_def
  use gkernel_interfaces
  use mapping_package
  !----------------------------------------------------------------------
  ! Can not be in a module as it is called by mapping-pyimport.c and
  ! mapping-sicimport.c
  !----------------------------------------------------------------------
  type(gpack_info_t), intent(out) :: pack
  external :: cube_pack_set
  character(len=12) :: status
  !
  pack%name='mapping'
  pack%ext = '.map'
  pack%depend(1:1) = (/ locwrd(greg_pack_set) /)
  pack%init=locwrd(mapping_pack_init)
  pack%on_exit=locwrd(mapping_pack_on_exit)
  pack%authors="J.Pety, S.Guilloteau, F.Gueth, N.Rodriguez-Fernandez"
  !
  ! Implicit import of CUBE in MAPPING?
  call sic_getenv('GAG_MAPPING_CUBE',status)
  if (status.eq."YES" .or. status.eq."yes") then
    pack%depend(2:2) = (/ locwrd(cube_pack_set) /)
  endif
end subroutine mapping_pack_set
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
