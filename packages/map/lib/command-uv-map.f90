!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module uv_map
  use gbl_message
  !
  public :: uv_map_comm
  private
  !
contains
  !
  subroutine uv_map_comm(line,error)
    use uvmap_tool
    use uv_shift
    use uvmosaic_tool, only: uvmosaic_main
    use uvmap_buffers, only: uvmap_prog
    !-------------------------------------------------------------------
    ! Support routine for command
    !     UV_MAP
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    if (uvmap_prog%nfields.ne.0) then
       call map_message(seve%i,'UV_MOSAIC','UV data is a Mosaic')
       call uv_shift_comm(line,error)
       call uvmosaic_main('UV_MOSAIC',line,error)
    else
       call uvmap_main('UV_MAP',line,error)
    endif
  end subroutine uv_map_comm
end module uv_map
! 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
