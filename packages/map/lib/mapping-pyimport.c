
/* Define entry point "initpymapping" for command "import pymapping" in Python */

#include "sic/gpackage-pyimport.h"

GPACK_DEFINE_PYTHON_IMPORT(mapping);
