module mapping_interfaces_private
  interface
    subroutine uv_spectral_frequency_sel(frequency, width, unit, channels, error)
      use gbl_message
      use uv_buffers
      !--------------------------------------------------------
      ! @ private
      !
      ! Mapping
      ! returns spectral channels based on a frequency and width given as input
      !--------------------------------------------------------
      real(kind=8), intent(in)      :: frequency
      real(kind=4), intent(in)      :: width
      character(len=10), intent(in) :: unit
      integer(kind=4), intent(out)  :: channels(2)
      logical, intent(inout)        :: error
    end subroutine uv_spectral_frequency_sel
  end interface
  !
  interface
    subroutine uv_spectral_velocity_sel(velocity, width, unit, channels, error)
      use gbl_message
      use uv_buffers
      !--------------------------------------------------------
      ! @ private
      !
      ! Mapping
      ! returns spectral channels based on a velocity and width given as input
      !--------------------------------------------------------
      real(kind=8), intent(in)      :: velocity
      real(kind=4), intent(in)      :: width
      character(len=10), intent(in) :: unit
      integer(kind=4), intent(out)  :: channels(2)
      logical, intent(inout)        :: error
    end subroutine uv_spectral_velocity_sel
  end interface
  !
  interface
    subroutine uv_spectral_range_sel(range,unit,channels, error)
      use gbl_message
      use uv_buffers
      !--------------------------------------------------------
      ! @ private
      !
      ! Mapping
      ! returns spectral channels based on a range at a known unit
      !--------------------------------------------------------
      real(kind=4), intent(in)      :: range(2)
      character(len=10), intent(in) :: unit
      integer(kind=4), intent(out)  :: channels(2)
      logical, intent(inout)        :: error
    end subroutine uv_spectral_range_sel
  end interface
  !
  interface
    subroutine uv_spectral_getcols(channel,columns)
      use image_def, only: index_length
      use uv_buffers
      !--------------------------------------------------------
      ! @ private
      !
      ! Mapping
      ! returns real, imaginary and weight columns for a given channel
      ! Returns -1 for all columns if channel outside table
      !--------------------------------------------------------
      integer(kind=index_length),intent(in)  :: channel
      integer(kind=index_length),intent(out) :: columns(3)
    end subroutine uv_spectral_getcols
  end interface
  !
  interface
    subroutine uv_spectral_flag(channel,error)
      use image_def, only: index_length,size_length
      use uv_buffers
      !--------------------------------------------------------
      ! @ private
      !
      ! Mapping
      ! Flags a UV channel i.e. makes its weight negative.
      ! Returns error if channel is outside UV table range.
      !--------------------------------------------------------
      integer(kind=index_length),intent(in)  :: channel
      logical,intent(inout)                  :: error
    end subroutine uv_spectral_flag
  end interface
  !
  interface
    subroutine uv_spectral_zero(channel,error)
      use image_def, only: index_length
      use uv_buffers
      !--------------------------------------------------------
      ! @ private
      !
      ! Mapping
      ! zeros a UV channel
      ! Returns error if channel is outside UV table range.
      !--------------------------------------------------------
      integer(kind=index_length),intent(in)  :: channel
      logical,intent(inout)                  :: error
    end subroutine uv_spectral_zero
  end interface
  !
  interface map_reallocate
    subroutine map_reallocate_inte_1d(name,array,n,reallocated,error)
      !---------------------------------------------------------------------
      ! @ private-generic map_reallocate
      ! (Re)allocation an integer*4 array to the desired size.
      ! subroutine returns .true. if array was actually reallocated.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name         ! Array name for feedback
      integer(kind=4),  allocatable   :: array(:)     !
      integer(kind=4),  intent(in)    :: n            ! Desired size
      logical,          intent(out)   :: reallocated  !
      logical,          intent(inout) :: error        !
    end subroutine map_reallocate_inte_1d
    subroutine map_reallocate_real_1d(name,array,n,reallocated,error)
      !---------------------------------------------------------------------
      ! @ private-generic map_reallocate
      ! (Re)allocation a real*4 array to the desired size.
      ! subroutine returns .true. if array was actually reallocated.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name         ! Array name for feedback
      real(kind=4),     allocatable   :: array(:)     !
      integer(kind=4),  intent(in)    :: n            ! Desired size
      logical,          intent(out)   :: reallocated  !
      logical,          intent(inout) :: error        !
    end subroutine map_reallocate_real_1d
    subroutine map_reallocate_real_2d(name,array,n1,n2,reallocated,error)
      !---------------------------------------------------------------------
      ! @ private-generic map_reallocate
      ! (Re)allocation a real*4 array to the desired size.
      ! subroutine returns .true. if array was actually reallocated.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name         ! Array name for feedback
      real(kind=4),     allocatable   :: array(:,:)   !
      integer(kind=4),  intent(in)    :: n1,n2        ! Desired size
      logical,          intent(out)   :: reallocated  !
      logical,          intent(inout) :: error        !
    end subroutine map_reallocate_real_2d
    subroutine map_reallocate_real_3d(name,array,n1,n2,n3,reallocated,error)
      !---------------------------------------------------------------------
      ! @ private-generic map_reallocate
      ! (Re)allocation a real*4 array to the desired size.
      ! subroutine returns .true. if array was actually reallocated.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name         ! Array name for feedback
      real(kind=4),     allocatable   :: array(:,:,:) !
      integer(kind=4),  intent(in)    :: n1,n2,n3     ! Desired size
      logical,          intent(out)   :: reallocated  !
      logical,          intent(inout) :: error        !
    end subroutine map_reallocate_real_3d
    subroutine map_reallocate_logi_2d(name,array,n1,n2,reallocated,error)
      !---------------------------------------------------------------------
      ! @ private-generic map_reallocate
      ! (Re)allocation a real*4 array to the desired size.
      ! subroutine returns .true. if array was actually reallocated.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name         ! Array name for feedback
      logical,          allocatable   :: array(:,:)   !
      integer(kind=4),  intent(in)    :: n1,n2        ! Desired size
      logical,          intent(out)   :: reallocated  !
      logical,          intent(inout) :: error        !
    end subroutine map_reallocate_logi_2d
  end interface map_reallocate
  !
  interface
    subroutine dofft_quick_debug (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      !$  use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  MAP_FAST
      !   Compute FFT of image by gridding UV data
      !   - Taper before gridding
      !   - Gridding with pre-computed support
      !   - Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_quick_debug
  end interface
  !
  interface
    subroutine dofft_quick_para (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      !$  use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  MAP_FAST
      !   Compute FFT of image by gridding UV data
      !   - Taper before gridding
      !   - Gridding with pre-computed support
      !   - Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_quick_para
  end interface
  !
  interface
    subroutine dofft_parallel_v_pseudo_out (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      !$  use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  MAP_FAST
      !   Compute FFT of image by gridding UV data
      !   - Taper before gridding
      !   - Gridding with pre-computed support
      !   - Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_parallel_v_pseudo_out
  end interface
  !
  interface
    subroutine dofft_parallel_v_true_out (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      !$  use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  MAP_FAST
      !   Compute FFT of image by gridding UV data
      !   - Taper before gridding
      !   - Gridding with pre-computed support
      !   - Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_parallel_v_true_out
  end interface
  !
  interface
    subroutine dofft_parallel_v_pseudo (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      !$  use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  MAP_FAST
      !   Compute FFT of image by gridding UV data
      !   - Taper before gridding
      !   - Gridding with pre-computed support
      !   - Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_parallel_v_pseudo
  end interface
  !
  interface
    subroutine dofft_parallel_v_true (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      !$  use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  MAP_FAST
      !   Compute FFT of image by gridding UV data
      !   - Taper before gridding
      !   - Gridding with pre-computed support
      !   - Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_parallel_v_true
  end interface
  !
  interface
    subroutine dofft_parallel_v_true2 (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      !$  use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  MAP_FAST
      !   Compute FFT of image by gridding UV data
      !   - Taper before gridding
      !   - Gridding with pre-computed support
      !   - Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_parallel_v_true2
  end interface
  !
  interface
    subroutine dofft_parallel_y (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      use gbl_message
      !$  use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  UVMAP
      !   Compute FFT of image by gridding UV data
      !   Taper after gridding
      !   Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_parallel_y
  end interface
  !
  interface
    subroutine dofft_parallel_x (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      use gbl_message
      !$  use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  UVMAP
      !   Compute FFT of image by gridding UV data
      !   Taper after gridding
      !   Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_parallel_x
  end interface
  !
  interface
    subroutine map_resample_comm(line,comm,error)
      use phys_const
      use gbl_message
      use image_def
      use mapping_read, only: out_range
      use clean_types
      use file_buffers
      use uvmap_buffers
      use clean_buffers
      use primary_buffers
      !---------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Resample in velocity the Images, or Compress them 
      !   Support for commands
      !     MAP_RESAMPLE WhichOne NC [Ref Val Inc]
      !     MAP_COMPRESS WhichOne NC
      !     MAP_INTEGRATE WhichOne Rmin Rmax RType
      !
      ! WhichOne    Name of map to be processed
      !             Allowed values are DIRTY, CLEAN, SKY
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      character(len=*), intent(in)    :: comm
      logical,          intent(inout) :: error
    end subroutine map_resample_comm
  end interface
  !
  interface
    subroutine dofft_test (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv   &
         &    ,ubias,vbias,ubuff,vbuff,ctype)
      !----------------------------------------------------------------------
      ! @  private
      !
      ! GILDAS  UV_MAP
      !   Compute FFT of image by gridding UV data
      !   Test version to compare speed of various methods
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
      integer, intent(in) :: ctype                ! type of gridding
    end subroutine dofft_test
  end interface
  !
  interface
    subroutine dofft_quick (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  MAP_FAST
      !   Compute FFT of image by gridding UV data
      !   - Taper before gridding
      !   - Gridding with pre-computed support
      !   - Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_quick
  end interface
  !
  interface
    subroutine dofft_fast (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv)
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  MAP_FAST
      !   Compute FFT of image by gridding UV data
      !   Taper before gridding
      !   Only for "visibility in cell" gridding.
      !   Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
    end subroutine dofft_fast
  end interface
  !
  interface
    subroutine dofft_slow (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  UVMAP
      !   Compute FFT of image by gridding UV data
      !   Taper after gridding
      !   Uses symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_slow
  end interface
  !
  interface
    subroutine dofft_quick1 (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
         &    ubias,vbias,ubuff,vbuff)
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  MAP_FAST
      !   Compute FFT of image by gridding UV data
      !   For any gridding support
      !   Taper before gridding
      !   Does not use symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
      real, intent(in) :: ubias                   ! U gridding offset
      real, intent(in) :: vbias                   ! V gridding offset
      real, intent(in) :: ubuff(4096)             ! U gridding buffer
      real, intent(in) :: vbuff(4096)             ! V gridding buffer
    end subroutine dofft_quick1
  end interface
  !
  interface
    subroutine dofft_fast1 (np,nv,visi,jx,jy,jo   &
         &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv)
      !----------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS  MAP_FAST
      !   Compute FFT of image by gridding UV data
      !   Only for "visibility in cell"
      !   Taper before gridding
      !   Do not use symmetry
      !----------------------------------------------------------------------
      integer, intent(in) :: nv                   ! number of values
      integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
      real, intent(in) :: visi(np,nv)             ! values
      integer, intent(in) :: nc                   ! number of channels
      integer, intent(in) :: jx                   ! X coord location in VISI
      integer, intent(in) :: jy                   ! Y coord location in VISI
      integer, intent(in) :: jo                   ! first channel to map
      integer, intent(in) :: nx                   ! X map size
      integer, intent(in) :: ny                   ! Y map size
      real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
      real, intent(in) :: mapx(nx)                ! X Coordinates of grid
      real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
      real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
      real, intent(in) :: cell(2)                 ! cell size in Meters
      real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
      real, intent(in) :: we(nv)                  ! Weight array
      real, intent(in) :: vv(nv)                  ! V Values
    end subroutine dofft_fast1
  end interface
  !
  interface
    subroutine sphfn (ialf, im, iflag, eta, psi, ier)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      !     SPHFN is a subroutine to evaluate rational approximations to se-
      !  lected zero-order spheroidal functions, psi(c,eta), which are, in a
      !  sense defined in VLA Scientific Memorandum No. 132, optimal for
      !  gridding interferometer data.  The approximations are taken from
      !  VLA Computer Memorandum No. 156.  The parameter c is related to the
      !  support width, m, of the convoluting function according to c=
      !  pi*m/2.  The parameter alpha determines a weight function in the
      !  definition of the criterion by which the function is optimal.
      !  SPHFN incorporates approximations to 25 of the spheroidal func-
      !  tions, corresponding to 5 choices of m (4, 5, 6, 7, or 8 cells)
      !  and 5 choices of the weighting exponent (0, 1/2, 1, 3/2, or 2).
      !
      !  Input:
      !    IALF    I*4   Selects the weighting exponent, alpha.  IALF =
      !                  1, 2, 3, 4, and 5 correspond, respectively, to
      !                  alpha = 0, 1/2, 1, 3/2, and 2.
      !    IM      I*4   Selects the support width m, (=IM) and, correspond-
      !                  ingly, the parameter c of the spheroidal function.
      !                  Only the choices 4, 5, 6, 7, and 8 are allowed.
      !    IFLAG   I*4   Chooses whether the spheroidal function itself, or
      !                  its Fourier transform, is to be approximated.  The
      !                  latter is appropriate for gridding, and the former
      !                  for the u-v plane convolution.  The two differ on-
      !                  by a factor (1-eta**2)**alpha.  IFLAG less than or
      !                  equal to zero chooses the function appropriate for
      !                  gridding, and IFLAG positive chooses its F.T.
      !    ETA     R*4   Eta, as the argument of the spheroidal function, is
      !                  a variable which ranges from 0 at the center of the
      !                  convoluting function to 1 at its edge (also from 0
      !                  at the center of the gridding correction function
      !                  to unity at the edge of the map).
      !
      !  Output:
      !    PSI      R*4  The function value which, on entry to the subrou-
      !                  tine, was to have been computed.
      !    IER      I*4  An error flag whose meaning is as follows:
      !                     IER = 0  =>  No evident problem.
      !                           1  =>  IALF is outside the allowed range.
      !                           2  =>  IM is outside of the allowed range.
      !                           3  =>  ETA is larger than 1 in absolute
      !                                     value.
      !                          12  =>  IALF and IM are out of bounds.
      !                          13  =>  IALF and ETA are both illegal.
      !                          23  =>  IM and ETA are both illegal.
      !                         123  =>  IALF, IM, and ETA all are illegal.
      !
      !---------------------------------------------------------------------
      integer(4), intent(in)  :: ialf     ! Exponent
      integer(4), intent(in)  :: im       ! Width of support
      integer(4), intent(in)  :: iflag    ! Gridding function
      real(4), intent(in)     :: eta      ! For spheroidals only
      real(4), intent(out)    :: psi      ! Result
      integer(4), intent(out) :: ier      ! Error code
    end subroutine sphfn
  end interface
  !
  interface
    subroutine gridless_density (npts,sizecell,distmax,evex,evey,eveweight,&
      & evesumweight,xmin,xmax,ymin,ymax,error)
      use gildas_def
      !----------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Support routine for Robust weighting
      !
      ! Stephane Paulin   OASU CNRS / U.Bordeaux  2016
      !
      ! Algorithm to compute the weighted neighborhood of each point in a large
      ! catalog. This algorithm was described, among other sources, by Daniel Briggs in
      ! his thesis. The idea is to avoid npts**2 calculations of the distance between
      ! each pair of points (npts is the # points). For that, one defines a grid only used
      ! to speed-up computation (result is independent of the grid).
      ! Step 1: In a given cell of the grid, all events are linked and the total weight
      !   of the cell is computed and kept in memory.
      ! Step 2: a pattern is computed, that is the same all over the field, whose radius
      !   is the maximum distance. Grid boxes can have 3 states: entirely inside the
      !   maximum distance, entirely outside, or in between (boxes noted 'intermediate'
      !   or 'mixed').
      ! The idea is to compute the distances only for events in these intermediate boxes.
      ! For other events, or the total weight of the box is added (if the box is inside),
      ! or the box is ignored (if outside). Next, the main loop consists on a loop on every
      ! event. For a given event, all boxes in the pattern are checked and treated accordingly.
      ! With this algorithm, one can have CPU times prop. to factor*npts*log(npts) with
      ! factor~2 (ie. when npts is multiplied by 10, the CPU is multiplied by ~45-50)
      ! Remarks/notes:
      ! - this is a preliminary approach for testing, where symmetries are not
      !   taken into account !!! ==> a given couple of coordinates (evex,evey) corresponds
      !   to one event
      !
      !----------------------------------------------------------------------
      real, intent(in) :: sizecell                ! size of a cell.
      ! this is a control parameter that does not change the result but
      ! change the calculation time: to minimize the CPU, the larger the
      ! event density, the smaller sizecell. ex: sizecell~20 (10, 5 resp.)
      ! for a catalog of 10**5 (10**6, 3x10**7) events
      integer, intent(in) :: npts                 ! total # of events
      real, intent(in) :: distmax                 ! maximum distance
      real(kind=4), intent(in) :: evex(npts)      ! X coordinates
      real(kind=4), intent(in) :: evey(npts)      ! Y coordinates
      real(kind=4), intent(in) :: eveweight(npts) ! Weight of event
      real(kind=4), intent(out) :: evesumweight(npts) ! Sum of the weights of all
      ! the events closer nearer than the maximum distance from the current event.
      real(kind=4), intent(in) :: xmin,ymin,xmax,ymax ! Min Max
      logical, intent(out) :: error               ! Error flag
    end subroutine gridless_density
  end interface
  !
  interface
    function calcdistsq(x1,x2,y1,y2)
      ! @ private
      real(kind=4) calcdistsq ! intent(out)
      real(kind=4), intent(in) :: x1,y1,x2,y2
    end function calcdistsq
  end interface
  !
  interface
    subroutine linkidenticalbox(nb, ind_init, boxtotweight, evesumweight, nextevent)
      use gildas_def
      !----------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Support routine for Robust Weighting
      !
      !   Build the chain list of pixels in the same cell with
      !   the same status
      !
      real, intent(in) :: boxtotweight                    ! Box total weight
      integer(kind=4), intent(in) :: nb                   ! Size of chain
      integer(kind=4), intent(in) :: ind_init             ! Starting index in each box
      real(kind=4), intent(inout) :: evesumweight(:)      ! Sum of weight at each event
      integer(kind=4), intent(in) :: nextevent(:)         ! Chained pointers
    end subroutine linkidenticalbox
  end interface
  !
  interface
    subroutine linkgoodbox(nb1, nb2, ind1_init, ind2_init, &
      & boxtotweight1, boxtotweight2, evesumweight, nextevent)
      use gildas_def
      !----------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Support routine for Robust Weighting
      !
      !   Build the chain list of pixels in the same cell with
      !   the same status
      !
      !----------------------------------------------------------------------
      real, intent(in) :: boxtotweight1, boxtotweight2    ! Total weight of each box
      integer(kind=4), intent(in) :: nb1, nb2             ! Box sizes
      integer(kind=4), intent(in) :: ind1_init, ind2_init ! Starting index in each box
      real(kind=4), intent(inout) :: evesumweight(:)      ! Sum of weight at each event
      integer(kind=4), intent(in) :: nextevent(:)         ! Chained pointers
    end subroutine linkgoodbox
  end interface
  !
  interface
    subroutine linkmixedbox(nb1, nb2, ind1_init, ind2_init, evesumweight, &
      & eveweight, evex, evey, nextevent, distmax, distmaxsq)
      use gildas_def
      !----------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Support routine for Robust Weighting
      !
      !   Build chain list of different types
      !
      !----------------------------------------------------------------------
      integer(kind=4), intent(in) :: nb1, nb2             ! Box sizes
      integer(kind=4), intent(in) :: ind1_init, ind2_init ! Starting index in each box
      real, intent(in) :: distmax, distmaxsq              ! Averaging radius
      real(4), intent(in) :: eveweight(:)                 ! Initial weights
      real(4), intent(in) :: evex(:)                      ! Event X coordinates
      real(4), intent(in) :: evey(:)                      ! Event Y coordinates
      real(kind=4), intent(inout) :: evesumweight(:)      ! Sum of weight at each event
      integer(kind=4), intent(in) :: nextevent(:)         ! Chained pointers
    end subroutine linkmixedbox
  end interface
  !
  interface
    subroutine chkfft (a,nx,ny,error)
      !---------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !   Check if FFT is centered...
      !---------------------------------------------------------
      integer, intent(in)  ::  nx,ny   ! X,Y size
      logical, intent(out)  ::  error  ! Error flag
      real, intent(in)  ::  a(nx,ny)   ! Array
    end subroutine chkfft
  end interface
  !
  interface
    subroutine doweig_quick (jc,nv,visi,jx,jy,jw,unif,we,wm,vv,mv, &
         &     umin,umax,vmin,vmax,nbcv)
      use gildas_def
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !     Compute weights of the visibility points.
      !----------------------------------------------------------------------
      integer, intent(in) ::  nv          ! number of values
      integer, intent(in) ::  jc          ! Number of "visibilities"
      integer, intent(in) ::  jx          ! X coord location in VISI
      integer, intent(in) ::  jy          ! Y coord location in VISI
      integer, intent(in) ::  jw          ! Location of weights. If .LE.0, uniform weight
      integer, intent(in) ::  mv          ! Size of work arrays
      integer, intent(in) ::  nbcv        ! Buffering factor
      real, intent(in) ::  visi(jc,nv)    ! Visibilities
      real, intent(in) ::  unif           ! uniform cell size in Meters
      real, intent(inout) ::  we(nv)      ! Weight array
      real, intent(in) ::  wm             ! on input: % of uniformity
      real, intent(in) ::  vv(nv)         ! V values, pre-sorted
      real, intent(out) :: umin,umax,vmin,vmax
    end subroutine doweig_quick
  end interface
  !
  interface
    subroutine doweig_sub (nv,uu,vv,ww,we,unif)
      !$ use omp_lib
      !----------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !     Compute weights of the visibility points.
      !----------------------------------------------------------------------
      integer, intent(in) :: nv          ! number of values
      real, intent(in) ::  uu(nv)        ! U coordinates
      real, intent(in) ::  vv(nv)        ! V coordinates
      real, intent(in) ::  ww(nv)        ! Input Weights
      real, intent(out) ::  we(nv)       ! Output weights
      real, intent(in) ::  unif          ! Cell size
    end subroutine doweig_sub
  end interface
  !
  interface
    subroutine doweig_slow (jc,nv,visi,jx,jy,jw,unif,we,wm)
      !----------------------------------------------------------------------
      ! @ private
      !
      ! MAPPING
      !     Compute weights of the visibility points.
      !----------------------------------------------------------------------
      integer, intent(in) ::  nv          ! number of values
      integer, intent(in) ::  jc          ! Number of "visibilities"
      integer, intent(in) ::  jx          ! X coord location in VISI
      integer, intent(in) ::  jy          ! Y coord location in VISI
      integer, intent(in) ::  jw          ! Location of weights. If .LE.0, uniform weight
      real, intent(in) ::  visi(jc,nv)    ! Visibilities
      real, intent(in) ::  unif           ! uniform cell size in Meters
      real, intent(inout) ::  we(nv)      ! Weight array
      real, intent(in) ::  wm             ! on input: % of uniformity
    end subroutine doweig_slow
  end interface
  !
end module mapping_interfaces_private
