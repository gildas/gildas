!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module mapping_support
  use gbl_message
  !
  public :: support_comm
  private
  !
contains
  !
  subroutine support_comm(line,error)
    use gkernel_interfaces
    use gildas_def
    use clean_support_tool
    use uvmap_buffers
    use clean_buffers
    !----------------------------------------------------------------------
    ! MAPPING Support routine for command
    !	  SUPPORT
    ! 1         [/PLOT]
    ! 2         [/CURSOR]
    ! 3         [/RESET]
    ! 4         [/MASK]
    ! 5         [/VARIABLE]
    ! 6         [/THRESHOLD  Raw Smooth [SmoothingLength]
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    logical :: fromfile, do_plot
    integer(kind=4) :: ier
    real(kind=4), allocatable, target :: imask(:,:)
    real :: raw,smo,length,noise,margin
    character(len=filename_length) :: file
    !
    integer(kind=4), parameter :: optname=0
    integer(kind=4), parameter :: optcurs=1
    integer(kind=4), parameter :: optmask=2
    integer(kind=4), parameter :: optplot=3
    integer(kind=4), parameter :: optrese=4
    integer(kind=4), parameter :: optthre=5
    integer(kind=4), parameter :: optvari=6
    character(len=*), parameter :: rname='SUPPORT'
    !
    do_plot = sic_present(optplot,0)
    !
    if (sic_present(optcurs,0)) then
       call map_message(seve%i,rname,'Use cursor to define polygon')
       clean_user%do_mask = .true.
       file = ''
       fromfile = .false.
       call sic_delvariable('MASK',.false.,error)
       mask%head%loca%size = 0
    else if (sic_present(optrese,0)) then
       if (allocated(mask%data)) then
          deallocate(mask%data,stat=ier)
       endif
       call greg_poly_reset(supportpol,supportvar,error)
       if (error)  return
       clean_user%do_mask = .true.  ! The mask HAS changed...
       call sic_delvariable('MASK',.false.,error)
       support_type = support_none
       mask%head%loca%size = 0
       return
    else if (sic_present(optmask,0)) then
       if (allocated(mask%data)) then
          call sic_delvariable('MASK',.false.,error)
          call sic_def_real ('MASK',mask%data,mask%head%gil%ndim,   &
               &        mask%head%gil%dim,.true.,error)
          clean_user%do_mask = .true.
          support_type = 1 ! First plane will be used by default
          !
          ! Show it if asked for by /PLOT option
          if (do_plot) then
             allocate (imask(mask%head%gil%dim(1),mask%head%gil%dim(2)),stat=ier)
             imask = 0.0
             where (mask%data(:,:,1).ne.0) imask = 1.0
             call sic_def_real ('BITMAP_MASK',imask,mask%head%gil%ndim,   &
                  &        mask%head%gil%dim,.true.,error)
             call gr_exec2('PLOT BITMAP_MASK /SCALING LIN -0.5 1.5')
             error = gr_error()
             call sic_delvariable('BITMAP_MASK',.false.,error)
             deallocate (imask,stat=ier)
          endif
       else
          call map_message(seve%e,rname,'No mask defined')
          error  = .true.
       endif
       return
    else if (sic_present(optthre,0)) then
       ! SUPPORT /THRESHOLD Raw Smooth [Length]
       !
       !  Raw      Thresholding (in Sigma) of the Clean image
       !  Smooth   Thresholding (in Sigma) after smoothing
       !  Length   Smoothing length: default is Clean beam major axis
       !
       if (clean%head%loca%size.eq.0) then
          call map_message(seve%e,rname,'No CLEAN image')
          error = .true.
          return
       endif
       raw = 5.0
       smo = 2.0
       length = clean%head%gil%majo
       !
       call sic_r4(line,optthre,1,raw,.false.,error)
       if (error) return
       call sic_r4(line,optthre,2,smo,.false.,error)
       if (error) return
       call sic_r4(line,optthre,3,length,.false.,error)
       if (error) return
       if (length.eq.0)  length = clean%head%gil%majo
       margin = 0.18
       call sic_r4(line,optthre,4,margin,.false.,error)
       if (margin.lt.0 .or. margin.gt.0.5) then
          call map_message(seve%e,rname,'Margin must be >0 and <0.5')
          error = .true.
          return
       endif
       !
       noise = max(dirty%head%gil%noise,clean%head%gil%noise,clean%head%gil%rms)
       !
       call sic_delvariable('MASK',.false.,error)
       if (allocated(mask%data)) deallocate(mask%data)    
       call gdf_copy_header(clean%head,mask%head,error)
       allocate(mask%data(mask%head%gil%dim(1),mask%head%gil%dim(2),mask%head%gil%dim(3)),   &
            &        stat=ier)
       !
       call mask_clean(mask%head,mask%data,clean%data,raw*noise,smo*noise,length,margin,error)
       if (error) return
       !
       call sic_def_real ('MASK',mask%data,mask%head%gil%ndim,   &
            &        mask%head%gil%dim,.true.,error)
       clean_user%do_mask = .true.
       support_type = 1 ! First plane will be used by default 
       return   
    else if (do_plot.and.sic_narg(optname).eq.0) then
       ! Just plot
       if (support_type.eq.support_poly) then
          call greg_poly_plot(supportpol,error)
       else if (support_type.ne.support_none) then
          allocate (imask(mask%head%gil%dim(1),mask%head%gil%dim(2)),stat=ier)
          imask = 0.0
          where (mask%data(:,:,1).ne.0) imask = 1.0
          call sic_def_real ('BITMAP_MASK',imask,mask%head%gil%ndim,   &
               &        mask%head%gil%dim,.true.,error)
          call gr_exec2('PLOT BITMAP_MASK /SCALING LIN -0.5 1.5')
          error = gr_error()
          call sic_delvariable('BITMAP_MASK',.false.,error)
          deallocate (imask,stat=ier)
          support_type = 1
       else
          call map_message(seve%w,rname,'No support defined')
          error  = .true.      
       endif
       return
    else
       ! This also re-instate the Polygon as the Clean support
       clean_user%do_mask = .true.
       if (sic_narg(optname).ge.1) then
          call greg_poly_parsename(line,optname,optvari,fromfile,file,error)
          if (error) return
       else  if (do_plot) then  ! /PLOT
          call greg_poly_plot(supportpol,error)
          return
       else
          fromfile = .false.
          file = ''  ! Not from file, not from var, i.e. from cursor
       endif
    endif
    !
    ! Define the polygon from file or cursor
    mask%head%loca%size = 0
    call greg_poly_define(rname,file,fromfile,supportpol,supportvar,error)
    if (error)  return
    support_type = support_poly
    !
    if (do_plot) then
       call greg_poly_plot(supportpol,error)
       if (error)  return
    endif
  end subroutine support_comm
end module mapping_support
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
