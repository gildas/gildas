!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module mapping_types
  use image_def
  !
  public :: mapping_2d_t,mapping_3d_t,mapping_4d_t
  public :: map_minmax
  private
  !
  type mapping_2d_t
     type(gildas) :: head
     real, allocatable :: data(:,:)
  end type mapping_2d_t
  !
  type mapping_3d_t
     type(gildas) :: head
     real, allocatable :: data(:,:,:)
  end type mapping_3d_t
  !
  type mapping_4d_t
     type(gildas) :: head
     real, allocatable :: data(:,:,:,:)
  end type mapping_4d_t
  !
contains
  !
subroutine map_minmax(hmap)
  use image_def
  !------------------------------------------------
  !
  !------------------------------------------------
  type(gildas), intent(inout) :: hmap
  !
  real :: rmi,rma
  !
  hmap%gil%extr_words = def_extr_words  ! extrema computed
  hmap%gil%minloc = 1
  hmap%gil%maxloc = 1
  hmap%gil%minloc(1:3) = minloc(hmap%r3d)
  hmap%gil%maxloc(1:3) = maxloc(hmap%r3d)
  rma = hmap%r3d(hmap%gil%maxloc(1),hmap%gil%maxloc(2),hmap%gil%maxloc(3))
  rmi = hmap%r3d(hmap%gil%minloc(1),hmap%gil%minloc(2),hmap%gil%minloc(3))
  hmap%gil%rmax = rma
  hmap%gil%rmin = rmi
end subroutine map_minmax
end module mapping_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
