!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module uv_filter
  use gbl_message
  !
  public :: uv_filter_comm
  public :: uv_filter_parsing
  private
  !
contains
  !
  subroutine uv_filter_comm(line,error)
    use gildas_def
    !----------------------------------------------------------
    ! Front end for UV_FILTER command
    !----------------------------------------------------------
    character(len=*), intent(inout) :: line  ! Command line
    logical, intent(out)            :: error ! Error flag
    !
    integer, allocatable       :: channellist(:,:)
    integer(kind=size_length)  :: nelem
    logical                    :: dozero
    !
    call uv_filter_parsing(line,channellist,nelem,dozero,'UV_FILTER',error)
    if (error) return
    !
    ! Only go to filtering if anything to filter
    if (nelem.gt.0)  call uv_filter_sub(channellist,nelem,dozero,error)
  end subroutine uv_filter_comm
  !
  subroutine uv_filter_parsing(line,channellist,nelem,dozero,rname,error)
    use gildas_def
    use gkernel_types
    use gkernel_interfaces
    use uv_buffers
    !----------------------------------------------------------
    ! Command line interaction for UV_FILTER and UV_BASELINE commands
    !----------------------------------------------------------
    character(len=*),          intent(inout) :: line             ! Command line
    integer, allocatable,      intent(out)   :: channellist(:,:) ! Array containing the flagging ranges
    integer(kind=size_length), intent(out)   :: nelem            ! Number of elements in descriptor
    logical,                   intent(out)   :: dozero           ! Zero option is present
    character(len=*),          intent(in)    :: rname            ! Who is calling?
    logical,                   intent(inout) :: error            ! Error flag
    !
    integer, parameter                    :: opt_chan=1 ! option for channel list
    integer, parameter                    :: opt_freq=2 ! option for frequency list
    integer, parameter                    :: opt_rang=3 ! option for range list !!! Not implemented
    integer, parameter                    :: opt_widt=4 ! option for width
    integer, parameter                    :: opt_zero=5 ! option for zeroing
    integer, parameter                    :: nunits =3
    integer(kind=index_length), parameter :: one=1      ! 1 in i8    
    character(len=10)                     :: units(3)
    data units/'CHANNEL','FREQUENCY','VELOCITY'/
    !
    real(kind=4)                :: width            ! Width to extract around frequency in unit
    character(len=10)           :: unitin           ! Input unit
    character(len=10)           :: unit             ! Treated unit
    integer                     :: chlength         ! Argument length
    integer                     :: narg             ! Number of arguments of an option
    integer                     :: ier              ! error flag
    real(kind=8)                :: freq             ! Input frequency
    integer(kind=4)             :: channel          ! Input Channel
    integer(kind=4)             :: ilist            ! Input list index
    type(sic_descriptor_t)      :: desc             ! Sic Variable descriptor
    character(len=64)           :: listname         ! Sic variable name
    integer                     :: nopt,iopt        ! to handle options
    real(kind=4)                :: drange(2)        ! First and last values of range
    !
    ! Initialization
    !
    width = 1
    unitin = 'CHANNEL'
    !
    ! Interface
    !
    if (huv%loca%size.eq.0) then
       call map_message(seve%e,rname,'No UV data loaded')
       error = .true.
       return
    endif
    dozero = sic_present(opt_zero,0)
    nopt = 0 
    do iopt=1, 3
       if (sic_present(iopt,0)) nopt = nopt+1
    enddo
    ! Making nelem 0 just in case no option is given
    nelem = 0
    ! Checking for conflicting options
    if (nopt.gt.1) then
       call map_message(seve%e,rname,"Options /CHANNELS, /RANGE and /FREQUENCY are mutually exclusive")
       error = .true.
       return
       ! Option range is present
    else if (sic_present(opt_rang,0)) then
       narg = sic_narg(opt_rang)
       if (narg.eq.1.or.narg.eq.0) then
          call map_message(seve%e,rname,"At least two values must be given")
          error = .true.
          return
       endif
       nelem = narg/2
       allocate(channellist(2,nelem),stat=ier)
       if (failed_allocate(rname,'channel list',ier,error))  return
       !
       if (.not.((narg/2)*2.eq.narg)) then ! Odd, means last arg is UNIT!
          call sic_ke(line, opt_rang, narg, unitin, chlength, .true., error)
       endif
       call sic_ambigs (rname,unitin,unit,chlength,units,nunits,error)
       if (error) return
       do ilist=1,nelem,1
          call sic_r4(line,opt_rang,2*(ilist-1)+1,drange(1),.true.,error)
          call sic_r4(line,opt_rang,2*(ilist-1)+2,drange(2),.true.,error)
          if (error) return
          call uv_spectral_range_sel(drange,unit,channellist(:,ilist),error)
       enddo
       ! Option frequency is present
    else if (sic_present(opt_freq,0)) then
       ! Getting frequency list
       narg = sic_narg(opt_freq)
       if (narg.eq.0) then
          call map_message(seve%e,rname,"At least one frequency must be given.")
          error = .true.
          return
       endif
       ! Getting width
       if (sic_present(opt_widt,0)) then
          call sic_r4(line, opt_widt, 1, width, .true., error)
          call sic_ke(line, opt_widt, 2, unitin, chlength, .false., error)
       endif
       call sic_ambigs (rname,unitin,unit,chlength,units,nunits,error)
       if (error) return
       allocate(channellist(2,narg),stat=ier)
       if (failed_allocate(rname,'channel list',ier,error))  return
       do ilist=1,narg
          call sic_r8(line,opt_freq,ilist,freq,.true.,error)
          if (error) return
          call uv_spectral_frequency_sel(freq,width,unit,channellist(:,ilist),error)
       enddo
       nelem = narg
       ! Option channels is present
    else if (sic_present(opt_chan,0)) then
       narg = sic_narg(opt_chan)
       if (narg.eq.0) then
          call map_message(seve%e,rname,"At least one channel must be given.")
          error = .true.
          return
       endif
       if (narg.gt.1) then
          allocate(channellist(2,narg),stat=ier)
          if (failed_allocate(rname,'channel list',ier,error))  return
          do ilist=1,narg
             call sic_i4(line,opt_chan,ilist,channel,.true.,error)
             if (error) return
             channellist(:,ilist) = channel
          enddo
          nelem = narg
       else
          call sic_de(line,opt_chan,1,desc,.true.,error)
          if (error) then
             call map_message(seve%e,rname,'Variable does not exists.')
             return
          endif
          if (.not.(desc%type.eq.fmt_i8.or.desc%type.eq.fmt_i4)) then
             call map_message(seve%e,rname,'Variable '//trim(listname)//' must be Integer ')
             error = .true.
             return
          endif
          if (desc%ndim.gt.1) then
             call map_message(seve%e,rname,'Variable '//trim(listname)//' must have rank 1')
             error = .true.
             return
          endif
          nelem = desc_nelem(desc)
          if (nelem.lt.2) then
             allocate(channellist(2,1),stat=ier)
             if (failed_allocate(rname,'channel list',ier,error))  return
             call sic_descriptor_getval(desc,one,channel,error)
             if (error) then
                call map_message(seve%e,rname,"Can't retrieve value from SIC variable")
                return
             end if
             channellist = channel
          else
             allocate(channellist(2,nelem),stat=ier)
             if (failed_allocate(rname,'channel list',ier,error))  return
             do ilist=1, nelem
                call sic_descriptor_getval(desc,one*ilist,channel,error)
                if (error) then
                   call map_message(seve%e,rname,"Can't retrieve value from SIC variable")
                   return
                end if
                channellist(:,ilist) = channel
             enddo
          endif
          call sic_volatile(desc)
       endif
    else
       if (trim(rname).eq."UV_FILTER") call map_message(seve%w,rname,"No options given skipping filtering")
       if (trim(rname).eq."UV_BASELINE") call map_message(seve%w,rname,"No windows given, fitting baseline to all channels")
    end if
  end subroutine uv_filter_parsing
  !
  subroutine uv_filter_sub(channellist,nelem,dozero,error)
    use gildas_def, only: index_length,size_length
    use uv_buffers
    !--------------------------------------------------------
    ! Apply flagging to a list of channels given as input
    !--------------------------------------------------------
    integer(kind=4), allocatable, intent(in)    :: channellist(:,:)
    integer(kind=size_length),    intent(in)    :: nelem
    logical,                      intent(in)    :: dozero
    logical,                      intent(inout) :: error
    !
    integer                    :: iranges        ! Index when browsing through ranges of channels
    integer                    :: fchan, lchan   ! First and last channels in a range of channels
    integer(kind=index_length) :: ichan          ! Index when browsing through channels in a range
    character(len=50)          :: warning        ! Warning message
    character(len=*), parameter :: rname='UV_FILTER'
    !
    do iranges=1, nelem
       fchan = channellist(1,iranges)
       lchan = channellist(2,iranges)
       if (lchan.lt.1.or.fchan.gt.huv%gil%nchan) then
          write(warning,1000) iranges,"-th spectral range is outside UV table"
          call map_message(seve%w,rname, trim(warning))
       else
          do ichan=fchan, lchan
             if (ichan.ge.1.and.ichan.le.huv%gil%nchan) then
                if (dozero) then
                   call uv_spectral_zero(ichan,error)
                else
                   call uv_spectral_flag(ichan,error)
                end if
             endif
          enddo
       endif
    enddo
1000 format(i4,a38)
  end subroutine uv_filter_sub
end module uv_filter
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
