!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module mapping_write
  use gbl_message
  !
  public :: write_comm
  private
  !
contains
  !
  subroutine write_comm(line,error)
    use gkernel_interfaces
    use gbl_format
    use mapping_read, only: out_range
    use file_buffers
    use uv_buffers
    !----------------------------------------------------------------------
    ! WRITE Type File [/RANGE Start End Kind] [/TRIM] [/APPEND] [/REPLACE]
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    integer :: ntype,nc(2),mc(2),larg
    character(len=12) :: argu,atype,crange
    character(len=filename_length) :: name
    real(8) :: drange(2)
    logical :: err
    integer, parameter :: o_append=1
    integer, parameter :: o_range=2
    integer, parameter :: o_replace=3
    integer, parameter :: o_trim=4  
    character(len=*), parameter :: rname='WRITE'
    !
    call sic_ke(line,0,1,argu,larg,.true.,error)
    if (error) return
    call sic_ch(line,0,2,name,larg,.true.,error)
    if (error) return
    drange = 0.0
    crange  = 'NONE'
    nc = 0
    !
    if (sic_present(o_range,0)) then
       call sic_r8 (line,o_range,1,drange(1),.true.,error)
       if (error) return
       call sic_r8 (line,o_range,2,drange(2),.true.,error)
       if (error) return
       call sic_ke (line,o_range,3,crange,larg,.true.,error)
       if (error) return
    endif
    !
    if (sic_present(o_replace,0)) then
       call sic_ambigs (rname,argu,atype,ntype,vtype,mtype,error)
       if (error) return
       !
       if (atype.eq.'UV') then
          call map_message(seve%e,rname,'Replacing channels in UV data is not allowed')
          error = .true.
          return
       else
          call map_message(seve%e,rname,'Replacing channels in '//atype) 
          call write_replace(name,ntype,nc,error)
       endif
    else
       if (argu.eq.'*') then
          if (crange.ne.'NONE') then
             call map_message(seve%e,rname,'/RANGE option not supported for ' &
                  & //' WRITE *')
             error = .true.
             return
          endif
          do ntype=1,mtype
             if (save_data(ntype)) then
                call map_message(seve%i,rname,'saving '//vtype(ntype))
                if (vtype(ntype).eq.'UV') then
                   mc =[1,huv%gil%nchan]
                else
                   mc = 0
                endif
                call write_main(name,ntype,mc,err)
                error = error.or.err
             endif
          enddo
       else
          !
          call sic_ambigs (rname,argu,atype,ntype,vtype,mtype,error)
          if (error) return
          !
          if (atype.eq.'UV') then
             ! Find the range
             call out_range(rname,crange,drange,nc,huv,error)
          else if (crange.ne.'NONE') then
             ! if (atype.ne.'CLEAN') then
             call map_message(seve%e,rname,'/RANGE option not supported for ' &
                  & //atype)
             error = .true.
             return
             !  endif
          endif
          !
          call write_main(name,ntype,nc,error)
       endif
    endif
  end subroutine write_comm
  !
  subroutine write_main(name,ntype,nc,error)
    use gbl_format
    use image_def
    use gkernel_interfaces
    use clean_support_tool
    use mapping_show_or_view, only: create_fields
    use file_buffers
    use uv_buffers
    use uvmap_buffers
    use clean_buffers
    use primary_buffers
    !----------------------------------------------------------------------
    ! Dispatch the writing operation to specific subroutines depending on
    ! the Type of data to be written
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: name
    integer,          intent(in)    :: nc(2)
    integer,          intent(in)    :: ntype
    logical,          intent(inout) :: error
    !
    type (gildas) :: htv
    logical :: subset,trim_uv,trim_uvany
    integer, parameter :: o_trim=4
    integer, allocatable :: ivis(:)
    integer :: np,nch,nv,mv,im,iv,ic,ier
    character(len=12) :: atype
    character(len=filename_length) :: file
    character(len=*), parameter :: rname='WRITE'
    !
    subset = .false.
    atype = vtype(ntype)
    !
    if (atype.eq.'SUPPORT') then
       call greg_poly_write(rname,supportpol,name,error)
       return
    endif
    !
    call sic_parse_file (name,' ',etype(ntype),file)
    !
    select case(atype)
    case ('UV')
       if (huv%loca%size.eq.0) then
          call map_message(seve%e,rname,'UV data undefined ')
          error = .true.
          return
       endif
       !
       ! Trim if needed
       trim_uv = sic_present(o_trim,0)
       trim_uvany = sic_present(o_trim,1)
       if (trim_uv) then
          call gildas_null(htv, type = 'UVT')
          np = huv%gil%dim(1)
          nv = huv%gil%dim(2)
          nch = huv%gil%nchan
          allocate(ivis(nv),stat=ier)
          if (ier.ne.0) then
             error = .true.
             return
          endif
          !
          ! First passs to identify all valid visibilities
          ! Caution: assume default model here.
          ! This is guaranteed inside Mapping, since we use gdf_read_uvdataset
          mv = 0
          do iv = 1,nv
             if (trim_uvany) then
                ! If a single channel is Bad, flag data
                im = 1
                do ic=1,nch
                   if (duv(7+3*ic,iv).le.0) then
                      im = 0
                      exit
                   endif
                enddo
             else
                ! If a single channel is Good, use data
                im = 0
                do ic=1,nch
                   if (duv(7+3*ic,iv).gt.0) then
                      im = 1
                      exit
                   endif
                enddo
             endif
             if (im.ne.0) then
                mv = mv+1
                ivis(mv) = iv
             endif
          enddo
          !
          ! Create output image
          call write_uv(file,nc,huv,duv,error, mv, ivis)
       else
          ! Create output image
          call write_uv(file,nc,huv,duv,error)
          call setmodif(file,optimize(code_save_uv),nc)
       endif
       save_data(code_save_uv) = .false.
       return ! This is a special case
    case ('BEAM')
       if (hbeam%loca%size.eq.0) then
          call map_message(seve%e,rname,'BEAM image undefined ')
          error = .true.
          return
       endif
       hbeam%file = file
       do while (hbeam%gil%dim(hbeam%gil%ndim).eq.1)
          hbeam%gil%ndim = hbeam%gil%ndim-1
          if (hbeam%gil%ndim.eq.0) exit
       enddo
       call gdf_write_image(hbeam,dbeam,error)
    case ('DIRTY')
       if (dirty%head%loca%size.eq.0) then
          call map_message(seve%e,rname,'DIRTY image undefined ')
          error = .true.
          return
       endif
       dirty%head%file = file
       call gdf_write_image(dirty%head,dirty%data,error)
    case ('CLEAN')
       if (clean%head%loca%size.eq.0) then
          call map_message(seve%e,rname,'CLEAN image undefined ')
          error = .true.
          return
       endif
       clean%head%file = file
       if (subset) then
          clean%head%blc(3) = nc(1)
          clean%head%trc(3) = nc(2)
          call gdf_update_image(clean%head,clean%data(1,1,nc(1)),error)
          clean%head%blc(3) = 0
          clean%head%trc(3) = 0
       else
          call gdf_write_image(clean%head,clean%data,error)
       endif
    case ('RESIDUAL')
       if (resid%head%loca%size.eq.0) then
          call map_message(seve%e,rname,'RESIDUAL image undefined ')
          error = .true.
          return
       endif
       resid%head%file = file
       call gdf_write_image(resid%head,resid%data,error)
    case ('MASK')
       if (mask%head%loca%size.eq.0) then
          call map_message(seve%e,rname,'MASK image undefined ')
          error = .true.
          return
       endif
       mask%head%file = file
       call gdf_write_image(mask%head,mask%data,error)
    case ('CCT')
       if (cct%head%loca%size.eq.0) then
          call map_message(seve%e,rname,'CC Table undefined ')
          error = .true.
          return
       endif
       call write_cct(file,error)
    case ('PRIMARY')
       if (primbeam%head%loca%size.eq.0) then
          call map_message(seve%e,rname,'Primary BEAM image undefined ')
          error = .true.
          return
       endif
       primbeam%head%file = file
       call gdf_write_image(primbeam%head,primbeam%data,error)
    case ('SKY')
       if (sky%head%loca%size.eq.0) then
          call map_message(seve%e,rname,'No primary beam corrected image')
          error = .true.
          return
       endif
       sky%head%file = file
       call gdf_write_image(sky%head,sky%data,error)
    case ('FIELDS')
       call create_fields(error)
       if (fields%head%loca%size.eq.0) then
          call map_message(seve%e,rname,'Primary FIELDS image undefined ')
          error = .true.
          return
       endif
       fields%head%file = file
       call gdf_write_image(fields%head,fields%data,error)
    case default
       call map_message(seve%e,'WRITE','Unsupported type '//atype)
       return
    end select
    !
    ! Set the Read/Write optimization and Save status
    call setmodif(file,optimize(ntype),nc)
    save_data(ntype) = .false.
  end subroutine write_main
  !
  subroutine setmodif(file,opti,nc)
    use gkernel_types
    use file_buffers
    !---------------------------------------------------------------------
    ! Update the Read / Write optimization status
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: file  ! Filename
    type(readop_t),   intent(inout) :: opti  ! Status of corresponding buffer
    integer,          intent(in)    :: nc(2) ! Range to be read
    !
    logical :: error
    !
    if (any(nc.ne.0)) then
       ! Subset write: the internal buffer is not the written file
       return
    else
       ! Whole write: the buffer and the written file match exactly
       ! so one can reset the corresponding "optimize" structure
       ! with this new file.  However, this does not guarantee a full
       ! read optimization here, as the input file and output file may
       ! have different names (but be identical).
       call gag_filmodif(file,opti%modif,error)
    endif
  end subroutine setmodif
  !
  subroutine write_cct(file,error)
    use image_def
    use gkernel_interfaces
    use uvmap_buffers
    use clean_buffers
    !----------------------------------------------------------------------
    ! Support routine for command
    !   WRITE CCT File [/RANGE Start End Kind]
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: file
    logical,          intent(inout) :: error
    !
    type (gildas) :: htcc
    integer :: mclean,i
    !
    ! Define the image header
    call gildas_null (htcc, type = 'IMAGE')
    call gdf_copy_header (dirty%head,htcc, error)
    htcc%file = file
    htcc%gil%ndim = 3
    htcc%char%unit = 'Jy'
    !
    htcc%gil%dim(1) = 3
    ! Keep the same axis description
    htcc%gil%xaxi = 1
    !
    htcc%gil%convert(:,2) = dirty%head%gil%convert(:,3)
    htcc%gil%convert(:,3) = dirty%head%gil%convert(:,2)
    htcc%gil%dim(2) = dirty%head%gil%dim(3)
    htcc%char%code(2) = dirty%head%char%code(3)
    htcc%gil%faxi = 2
    !
    htcc%gil%dim(3) = ubound(cct%data,3)
    htcc%char%code(3) = 'COMPONENT'
    htcc%gil%yaxi = 3
    !
    mclean = htcc%gil%dim(3)
    do i=1,htcc%gil%dim(3)
       if (all(cct%data(:,:,i).eq.0)) then
          mclean = i-1
          exit
       endif
    enddo
    !
    ! Initialize BLCs...
    htcc%blc = 0
    htcc%trc = 0
    htcc%gil%dim(3) = mclean
    call gdf_write_image(htcc,cct%data(:,:,1:mclean),error)
  end subroutine write_cct
  !
  subroutine write_uv(file,nc,uvin,duv,error,mv,ivis)
    use gkernel_interfaces
    use image_def
    !---------------------------------------------------------------------
    ! Write a subset of the loaded UV data
    !---------------------------------------------------------------------
    character(len=*),  intent(in)    :: file
    integer,           intent(in)    :: nc(2)
    type(gildas),      intent(in)    :: uvin
    real,              intent(in)    :: duv(:,:)
    logical,           intent(inout) :: error
    integer, optional, intent(in)    :: mv
    integer, optional, intent(in)    :: ivis(:)
    !
    type (gildas) :: uvou
    logical :: all
    integer :: blc,trc,ib,iv,nblock,ier
    integer(kind=index_length) :: nvisi
    character(len=message_length) :: mess
    !
    call gildas_null(uvou, type= 'UVT')
    call gdf_copy_header(uvin, uvou, error)
    if (error) return
    call sic_parse_file(file,' ','.uvt',uvou%file)
    !
    uvou%gil%nchan = nc(2)-nc(1)+1
    uvou%gil%ref(1) = uvin%gil%ref(1)-nc(1)+1
    uvou%gil%dim(1) = uvou%gil%nlead + uvou%gil%natom*uvou%gil%nchan + uvou%gil%ntrail
    !
    all = uvou%gil%nchan.eq.uvin%gil%nchan
    if (all .and. .not.present(mv)) then
       call gdf_write_image(uvou,duv,error)
    else
       ! Only write a subset, either by trimming or by channels, or both at once...
       !
       ! Define blocking factor
       if (present(mv)) then
          uvou%gil%nvisi = mv
          uvou%gil%dim(2) = mv
       endif
       call gdf_nitems('SPACE_GILDAS',nblock,uvou%gil%dim(1)) ! Visibilities at once
       nblock = min(nblock,uvou%gil%dim(2))
       ! Allocate space
       allocate (uvou%r2d(uvou%gil%dim(1),nblock), stat=ier)
       if (ier.ne.0) then
          write(mess,*) 'Memory allocation error ',uvou%gil%dim(1), nblock
          call map_message(seve%e,'WRITE',mess)
          error = .true.
          return
       endif
       !
       call gdf_create_image(uvou,error)
       if (error) then
          deallocate(uvou%r2d,stat=ier)
          call gdf_close_image(uvou,error)
          error = .true.
          return
       endif
       ! Loop over UV table
       uvou%blc = 0
       uvou%trc = 0
       do ib = 1,uvou%gil%dim(2),nblock
          write(mess,*) ib,' to ',ib+nblock-1,' / ',uvou%gil%dim(2)
          call map_message(seve%i,'WRITE',mess)
          blc = ib
          trc = min(uvou%gil%dim(2),ib-1+nblock)
          uvou%blc(2) = blc
          uvou%trc(2) = trc
          nvisi = trc-blc+1
          !
          ! Here do the job
          if (present(mv)) then
             do iv=blc,trc
                if (all) then
                   uvou%r2d(:,iv-blc+1) = duv(:,ivis(iv))
                else
                   call extract_block(uvou,uvou%r2d(:,iv-blc+1),uvin,duv(:,ivis(iv)),nvisi,nc)
                endif
             enddo
          else
             call extract_block(uvou,uvou%r2d,uvin,duv(:,blc:trc),nvisi,nc)
          endif
          !
          ! Write
          call gdf_write_data(uvou,uvou%r2d,error)
       enddo
       call gdf_close_image(uvou,error)
       deallocate(uvou%r2d,stat=ier)
    endif
  end subroutine write_uv
  !
  subroutine extract_block(out,dout,in,din,nvisi,nc)
    use image_def
    !---------------------------------------------------------------------
    ! Extract a subset block of UV data
    !---------------------------------------------------------------------
    type(gildas),               intent(in)  :: out
    type(gildas),               intent(in)  :: in
    integer(kind=index_length), intent(in)  :: nvisi
    real,                       intent(in)  :: din(in%gil%dim(1),nvisi)
    real,                       intent(out) :: dout(out%gil%dim(1),nvisi)
    integer,                    intent(in)  :: nc(2)
    !
    integer :: iv,icf,icl,jcf,jcl
    !
    icf = in%gil%nlead+(nc(1)-1)*in%gil%natom+1
    icl = in%gil%nlead+nc(2)*in%gil%natom
    jcf = out%gil%nlead+1
    jcl = out%gil%nlead+out%gil%nchan*out%gil%natom
    !
    do iv=1,nvisi
       dout(1:in%gil%nlead,iv) = din(1:in%gil%nlead,iv)
       dout(jcf:jcl,iv) = din(icf:icl,iv)
       if (out%gil%ntrail.gt.0) then
          dout(out%gil%dim(1)-out%gil%ntrail+1:out%gil%dim(1),iv) = &
               din(in%gil%dim(1)-in%gil%ntrail+1:in%gil%dim(1),iv)
       endif
    enddo
  end subroutine extract_block
  !
  subroutine write_replace(name,ntype,nc,error)
    use gbl_format
    use gkernel_interfaces
    use clean_support_tool
    use file_buffers
    use uvmap_buffers
    use clean_buffers
    use primary_buffers
    !----------------------------------------------------------------------
    ! Support routine for command
    !   WRITE Type File /REPLACE 
    !
    ! Dispatch the writing operation to specific subroutines depending
    ! on the Type of data to be written
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: name
    integer,          intent(in)    :: nc(2)
    integer,          intent(in)    :: ntype
    logical,          intent(inout) :: error
    !
    logical :: subset
    character(len=12) :: atype
    character(len=filename_length) :: file
    character(len=*), parameter :: rname='WRITE>REPLACE'
    !
    subset = .false.
    atype = vtype(ntype)
    !
    call sic_parse_file(name,' ',etype(ntype),file)
    !
    select case(atype)
    case ('BEAM')
       if (hbeam%gil%dim(3).ne.1) then
          if (hbeam%gil%dim(4).ne.1) then
             call map_message(seve%e,rname,'Multi-frequency beams not yet supported for mosaics')
             error = .true.
          else
             call replace_one(atype,file,dbeam(:,:,:,1),hbeam,error)
          endif
       else
          call map_message(seve%w,rname,'Single beam plane only')
       endif
    case ('DIRTY')
       call replace_one(atype,file,dirty%data,dirty%head,error)
    case ('CLEAN')
       call replace_one(atype,file,clean%data,clean%head,error)
    case ('RESIDUAL')
       call replace_one(atype,file,resid%data,resid%head,error)
    case ('MASK')
       call replace_one(atype,file,mask%data,mask%head,error)
    case ('CCT')
       call replace_one(atype,file,cct%data,cct%head,error)
    case ('SKY')
       call replace_one(atype,file,sky%data,sky%head,error)
    case default
       call map_message(seve%e,'WRITE','Unsupported type '//atype//' for /REPLACE')
       error = .true.
       return
    end select
    !
    ! Set the Read/Write optimization and Save status
    call setmodif(file,optimize(ntype),nc)
    save_data(ntype) = .false.
  end subroutine write_replace
  !
  subroutine replace_one(name,file,data,head,error)
    use gkernel_interfaces
    use image_def
    !-------------------------------------------------------
    ! 
    !-------------------------------------------------------
    character(len=*), intent(in)    :: name
    character(len=*), intent(in)    :: file
    type(gildas),     intent(inout) :: head
    real(kind=4),     intent(in)    :: data(:,:,:)
    logical,          intent(inout) :: error
    !
    type(gildas) :: hall
    integer :: imin,imax,itmp
    real(8) :: rval,velo
    real(8), parameter :: epsilon=1d-5
    character(len=64) :: chain
    character(len=*), parameter :: rname='REPLACE>ONE'
    !
    if (head%loca%size.eq.0) then
       call map_message(seve%e,rname,name//' image undefined ')
       error = .true.
       return
    endif
    !
    call gildas_null(hall)
    hall%file = file
    call gdf_read_header(hall,error)
    if (error) return
    !
    ! Basic checks of coordinates
    if ( any(head%gil%dim(1:2).ne.hall%gil%dim(1:2)) ) then
       call map_message(seve%e,rname,'Dimensions do not match')
       error = .true.
    endif
    if ( any(head%gil%convert(:,1:2).ne.hall%gil%convert(:,1:2)) ) then
       call map_message(seve%e,rname,'Positions do not match')
       print *,'Head ',head%gil%convert(:,1:2)
       print *,'Hout ',hall%gil%convert(:,1:2)
       print *,'Difference ',head%gil%convert(:,1:2)-hall%gil%convert(:,1:2)
       error = .true.
    endif
    !
    if ( abs(head%gil%inc(3)).ne.abs(hall%gil%inc(3)) ) then
       call map_message(seve%e,rname,'Frequency resolution do not match')
       error = .true.
    endif
    if (error) return
    !
    ! Check which range in the file is needed - Match the velocities
    velo = (1.d0-head%gil%ref(3))*head%gil%inc(3) + head%gil%val(3)
    rval = (velo-hall%gil%val(3))/hall%gil%inc(3) + hall%gil%ref(3)
    imin = nint(rval)
    !
    if (abs(dble(imin)-rval).gt.epsilon*abs(head%gil%inc(3))) then
       call map_message(seve%e,rname,'Frequency axis does not match')
       error = .true.
    endif
    if (error) return
    !
    velo = (head%gil%dim(3)-head%gil%ref(3))*head%gil%inc(3) + head%gil%val(3)
    rval = (velo-hall%gil%val(3))/hall%gil%inc(3) + hall%gil%ref(3)
    imax = nint(rval)
    !
    if (imin.gt.imax) then
       itmp = imin
       imin = imax
       imax = itmp
    endif
    !
    if (imin.lt.1 .or. imax.gt.hall%gil%dim(3)) then
       write(chain,'(A,A,I0,A,I0)') name,' falls in range ',imin,',',imax
       call map_message(seve%e,rname,chain)
       error = .true.
       return
    endif
    !
    hall%blc(3) = imin
    hall%trc(3) = imax
    !
    call gdf_update_image(hall,data,error)
  end subroutine replace_one
end module mapping_write
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
