!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module uv_truncate
  use gbl_message
  !
  public :: uv_truncate_comm
  private
  !
contains
  !
  subroutine uv_truncate_comm(line,error)
    use image_def
    use gkernel_interfaces
    use file_buffers
    use uv_buffers
    !---------------------------------------------------------------------
    ! Truncate a UV Table, by removing baselines out of
    ! a given range (Min and Max)
    !
    ! UV_TRUNCATE Max [Min]
    !
    ! Works on the "current" UV data set: Uses UVS or UVR as needed.
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    real(4) :: mymax, mymin, rmin, rmax, length
    integer :: iv, kv, ier
    integer :: nu, nv
    type (gildas) :: hcuv
    real, pointer :: duv_previous(:,:), duv_next(:,:)
    integer, allocatable :: point(:)
    character(len=80) :: mess
    character(len=*), parameter :: rname='UV_TRUNCATE'
    !
    call sic_r4(line,0,1,mymax,.true.,error)
    if (error) return
    !
    mymin = 0.
    call sic_r4(line,0,2,mymin,.false.,error)
    if (error) return
    !
    ! The UV table is available in HUV%
    if (huv%loca%size.eq.0) then
       call map_message(seve%e,rname,'No UV data loaded')
       error = .true.
       return
    endif
    nu = huv%gil%dim(1)
    nv = huv%gil%dim(2)
    !
    ! Read the Baseline length information
    allocate (point(nv), stat=ier)
    if (ier.ne.0) then
       call map_message(seve%e,rname,'Memory allocation error')
       error = .true.
       return
    endif
    !
    rmin = mymin**2
    rmax = mymax**2
    !
    kv = 0
    do iv = 1, nv
       length = duv(1,iv)**2 + duv(2,iv)**2
       if (length.ge.rmin .and. length.le.rmax) then
          kv = kv+1
          point(kv) = iv
       endif
    enddo
    !
    ! Prepare header and allocate buffers
    call gildas_null(hcuv,type='UVT')
    call gdf_copy_header(huv,hcuv,error)
    hcuv%gil%dim(2) = kv
    hcuv%gil%nvisi = kv
    nullify (duv_previous, duv_next)
    call uv_find_buffers (rname,nu,kv,duv_previous,duv_next,error)
    if (error) return
    !
    ! Now do the job
    do iv = 1,kv
       duv_next(:,iv) = duv(:,point(iv))
    enddo
    !
    ! Finalize the UV Table
    write(mess,'(i12,a,i12,a)') hcuv%gil%nvisi,' Visibilities created (', &
         & huv%gil%nvisi,' before)'
    call map_message(seve%i,rname,mess)
    call uv_clean_buffers (duv_previous, duv_next,error)
    if (error) return
    !
    ! Copy back to UV data set
    call gdf_copy_header(hcuv,huv,error)
    !
    ! Indicate Weights have changed, optimization and save status
    do_weig = .true.
    optimize(code_save_uv)%change = optimize(code_save_uv)%change + 1
    save_data(code_save_uv) = .true.
    ! Redefine SIC variables (mandatory)
    call sic_delvariable ('UV',.false.,error)
    call sic_mapgildas ('UV',huv,error,duv)
  end subroutine uv_truncate_comm
end module uv_truncate
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
