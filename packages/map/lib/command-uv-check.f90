!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module uv_check
  use gbl_message
  !
  public :: uv_check_comm
  private
  !
contains
  !
  subroutine uv_check_comm(line,error)
    use gkernel_interfaces
    use uv_buffers
    !----------------------------------------------------------------------
    ! UV_CHECK [Beams|Null]
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    integer :: nu,nv,nc,na,ncase, ier, i, iv, ic, kv
    integer, allocatable :: cases(:), bad(:)
    character(len=*), parameter :: rname='UV_CHECK'
    character(len=12) :: argum,cmode
    integer, parameter :: mmode=2
    character(len=8) :: smode(2)
    data smode /'BEAMS','NULLS'/
    !
    if (huv%loca%size.eq.0) then
       call map_message(seve%e,rname,'No UV data loaded')
       error = .true.
       return
    endif
    nu = huv%gil%dim(1)
    nv = huv%gil%dim(2)
    nc = huv%gil%nchan
    !
    argum = 'BEAMS'
    call sic_ke(line,0,1,argum,na,.false.,error)
    call sic_ambigs (rname,argum,cmode,na,smode,mmode,error)
    if (error) return
    !
    select case (cmode)
    case('BEAMS')
       allocate (cases(nc),stat=ier)
       !
       call howmany_beams(huv,nv,duv,cases,ncase,error)
       !
       ! Write the result
       if (ncase.eq.1) then
          call map_message(seve%i,rname,'Only one beam needed')
       else
          if (ncase.eq.nc) then
             call map_message(seve%w,rname,'Need one beam per channel')
          else
             call map_message(seve%w,rname,'Beams needed for the following channel ranges:')
             cases(ncase+1) = nc
             do i=1, ncase
                write(6,'(a,i6,a,i6,a)') '[',cases(i),'-',cases(i+1),']'
             enddo
          endif
       endif
    case ('NULLS')
       call map_message(seve%i,rname,'Checking for null visibilities')
       allocate (bad(0:nv),stat=ier)
       bad(0) = 0
       kv = 0
       do iv=1,nv
          do ic=1,nc
             if (duv(7+3*ic,iv).gt.0) then
                if (duv(5+3*ic,iv).eq.0 .and. duv(6+3*ic,iv).eq.0) then
                   if (bad(kv).ne.iv) then
                      kv = kv+1
                      bad(kv) = iv
                   endif
                   duv(7+3*ic,iv) = 0
                endif
             endif
          enddo
       enddo
       if (kv.ne.0) then
          write(6,*) 'Flagged ',kv,' null visibilities'
          write(6,*) (bad(iv),iv=1,kv)
       else
          call map_message(seve%i,rname,'No null visibilities left unflagged')
       endif
       deallocate (bad,stat=ier)
    end select
  end subroutine uv_check_comm
  !
  subroutine howmany_beams(uvin, nvisi, din, cases, ncase, error)
    use image_def
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(gildas), intent(in)    :: uvin                       ! UV table header
    integer,      intent(in)    :: nvisi                      ! Number of visibilities
    real, target, intent(in)    :: din(uvin%gil%dim(1),nvisi) ! UV data
    integer,      intent(inout) :: cases(uvin%gil%nchan)      ! Possible cases for beams
    integer,      intent(inout) :: ncase                      ! Number of cases
    logical,      intent(inout) :: error                      ! flag
    !
    real, pointer :: wlast(:), wcur(:)
    logical, allocatable :: mask(:)
    !
    integer :: k,ic,ier
    !
    allocate (mask(nvisi),stat=ier)
    error = ier.ne.0
    if (error) return
    !
    wlast => din(uvin%gil%fcol+2,:)
    ncase = 1
    cases(1) = 1
    !
    k = uvin%gil%fcol+2
    do ic = 2,uvin%gil%nchan
       k = k+3
       wcur => din(k,:)
       mask(:) = wcur.ne.wlast
       if (any(mask)) then
          ncase = ncase+1
          cases(ncase) = ic ! Which channel
          wlast => din(k,:)
       endif
    enddo
    deallocate (mask)
  end subroutine howmany_beams
end module uv_check
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
