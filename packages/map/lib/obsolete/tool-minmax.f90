subroutine maxvec (x,n,nomin,rmin,nomax,rmax)
  !----------------------------------------------------------------------
  ! @ public
  !
  ! GILDAS:	Utility routine
  ! 	Compute position and values of Min and Max of array X
  !----------------------------------------------------------------------
  integer, intent(in) :: n             ! taille du vecteur
  real, intent(in) :: x(n)             ! Vecteur donne
  integer, intent(out) :: nomin,nomax   ! Resultat de la recherche:indices
  real, intent(out) :: rmin,rmax        ! et valeurs
  !
  integer no
  real val
  !
  no = 1
  rmax=x(no)
  nomax=no
  rmin=x(no)
  nomin=no
  do no=2,n
    val=x(no)
    if (val.gt.rmax) then
      nomax=no
      rmax=val
    elseif (val.lt.rmin) then
      nomin=no
      rmin=val
    endif
  enddo
end subroutine maxvec
!
subroutine maxmsk(ary,nx,ny,msk,box,   &
     &    amax,imax,jmax,amin,imin,jmin)
  !--------------------------------------------------------------------
  ! @ public
  !
  ! GILDAS:	Utility routine
  !	  Find max and min in specified region of real array
  !   Version with mask
  !--------------------------------------------------------------------
  integer, intent(in) :: nx, ny         ! X,Y size
  real, intent(in) :: ary(nx,ny)        ! Array of values
  logical, intent(in) ::  msk(nx,ny)    ! Search mask
  integer, intent(in) :: box(4)         ! Search box
  integer, intent(out) :: imax, jmax    ! Position of Max
  integer, intent(out) :: imin, jmin    ! Position of Min
  real, intent(out) :: amax, amin       ! Max and Min
  !
  integer i, j
  real a
  !
  amax = -1e38
  amin = 1e38
  !
  do j = box(2), box(4)
    do i = box(1), box(3)
      if (msk(i,j)) then
        a = ary(i,j)
        if (a .gt. amax) then
          amax = a
          imax = i
          jmax = j
        endif
        if (a .lt. amin) then
          amin = a
          imin = i
          jmin = j
        endif
      endif
    enddo
  enddo
end subroutine maxmsk
