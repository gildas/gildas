module clean_arrays
  use image_def
  use uvmap_types
  use clean_types
  use cct_types
  !
  ! Static variables first, allocatable next.
  !
  type (uvmap_par), save :: themap
  type (clean_par), target :: method, user_method
  type (gildas), save, target :: hbeam, hdirty, hclean, hresid
  type (gildas), save, target :: hprim, huvi, huv, huvt, hfields
  type (gildas), save, target :: hcct, hmask, hsky
  type (gildas), save :: huvm
  !
  ! Support variable for the SIC variable 'W':
  type (gildas), save :: w_plot
  type (gildas), save :: b_plot
  !
  ! These are UV data
  real, pointer :: duv(:,:)
  logical, save :: uv_plotted
  ! For the Multi-Primary beams Mosaic deconvolution
  ! C_ stands for "Compact Array"
  type (clean_par), target :: c_method, cuse_method
  type (gildas), save :: c_hbeam, c_hdirty, c_hresid, c_hclean
  type (gildas), save :: c_hprim
  !
  real, allocatable, save :: g_weight(:)  ! UV weights
  real, allocatable, save :: g_v(:)       ! V values
  logical, save :: do_weig                ! Optimization of weight computing
  !
  ! Imaging and deconvolution arrays
  real, allocatable, target, save :: dbeam(:,:,:,:) ! 4-D
  real, allocatable, target, save :: ddirty(:,:,:)  ! Dirty 3-D 
  real, allocatable, target, save :: dclean(:,:,:)  ! Clean 3-D
  real, allocatable, target, save :: dresid(:,:,:)  ! Residual 3-D
  real, allocatable, target, save :: dprim(:,:,:,:) ! Primary beam 4-D
  real, allocatable, target, save :: weight(:,:,:)  ! Mosaic weights 3-D
  real, allocatable, target, save :: dsky(:,:,:)      ! Primary beam corrected 3-D
  real, allocatable, target, save :: dfields(:,:,:,:) ! Mosaic fields 4-D
  !
  ! Deconvolution arrays
  real, allocatable, target, save :: dmask(:,:,:)   ! Input 3-D 0, #0 mask
  integer, allocatable, target, save :: d_list(:)   ! List of selected pixels
  logical, allocatable, target, save :: d_mask(:,:) ! Logical mask for pixel selection
  real, allocatable, target, save :: dcct(:,:,:)    ! Clean component values
  type (cct_par), allocatable, save :: tcc(:)       ! Clean component list
  !
  ! These are UV data
  real, allocatable, target, save :: duvi(:,:)  ! Original UV data
  real, pointer :: duvr(:,:)                    ! Resampled UV data
  real, pointer :: duvs(:,:)                    ! Sorted UV data
  real, allocatable, target, save :: duvt(:,:)  ! Time-Baseline sorted UV data, for UV_FLAG
  real, allocatable, target, save :: duvm(:,:)  ! Model UV data
  !
  ! For the Multi-Primary beams Mosaic deconvolution
  ! C_ stands for "Compact Array"
  real, allocatable, target, save :: c_dbeam(:,:,:)
  real, allocatable, target, save :: c_ddirty(:,:,:)
  real, allocatable, target, save :: c_dclean(:,:,:)
  real, allocatable, target, save :: c_dresid(:,:,:)
  real, allocatable, target, save :: c_dprim(:,:,:)
  logical, allocatable, target, save :: c_mask(:,:)
  integer, allocatable, target, save :: c_list(:)
  real, allocatable, target, save :: c_weight(:,:,:)  ! 3-D
  !
  integer :: niter_listsize=0
  integer, allocatable :: niter_list(:)
  integer :: ares_listsize=0
  real, allocatable :: ares_list(:)
  ! List of selected fields in UV_MAP (used for UV_RESTORE and
  ! also for SHOW FIELDS)
  integer :: selected_fieldsize=0
  integer, allocatable :: selected_fields(:)
end module clean_arrays
!
module buffer_types
  use gkernel_types
  integer(kind=4), parameter :: mtype=13
  integer(kind=4), parameter :: mtype_read=11  ! Command READ can not read FIELDS nor COVERAGE
  character(len=12) :: vtype(1:mtype) = (/ &
  &  'UV          ',  &
  &  'BEAM        ',  &
  &  'DIRTY       ',  &
  &  'CLEAN       ',  &
  &  'PRIMARY     ',  &
  &  'RESIDUAL    ',  &
  &  'MASK        ',  &
  &  'SUPPORT     ',  &
  &  'CCT         ',  &
  &  'MODEL       ',  &
  &  'SKY         ',  &
  &  'FIELDS      ',  &
  &  'COVERAGE    ' /)   ! Support keyword for SHOW COVERAGE (no associated buffer)
  !
  character(len=12) :: etype(1:mtype) = (/ &
  & '.uvt        ',  &
  & '.beam       ',  &
  & '.lmv        ',  &
  & '.lmv-clean  ',  &
  & '.lobe       ',  &
  & '.lmv-res    ',  &
  & '.msk        ',  &
  & '.pol        ',  &
  & '.cct        ',  &
  & '.uvt        ',  &
  & '.lmv-sky    ',  &
  & '.fields     ',  &
  & '.uvt        ' /)   ! Irrelevant for COVERAGE
  !
  character(len=12) ctype
  !
  integer, parameter :: code_save_uv=1
  integer, parameter :: code_save_beam=2
  integer, parameter :: code_save_dirty=3
  integer, parameter :: code_save_clean=4
  integer, parameter :: code_save_primary=5
  integer, parameter :: code_save_resid=6
  integer, parameter :: code_save_mask=7
  integer, parameter :: code_save_support=8
  integer, parameter :: code_save_cct=9
  integer, parameter :: code_save_model=10
  integer, parameter :: code_save_sky=11
  integer, parameter :: code_save_fields=12
  logical :: save_data(mtype)
  !
  ! Read / Write optimization
  type readop_t
    type(mfile_t) :: modif
    integer :: lastnc(2) = 0
    integer :: change = 0
  end type
  !
  type (readop_t), save :: optimize(mtype)
  !
  integer :: rw_optimize
end module buffer_types
