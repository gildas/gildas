!
subroutine translate (in, nx, ny, trans, ix, iy)
  !-----------------------------------------------------------------------
  ! @ private
  !
  !     Translate the beam to new position IX, IY
  !-----------------------------------------------------------------------
  integer, intent(in) :: nx          ! X size
  integer, intent(in) :: ny          ! Y size
  real, intent(in) :: in(nx,ny)      ! Input beam
  real, intent(out) :: trans(nx,ny)  ! Translated beam
  integer, intent(in) :: ix          ! X shift
  integer, intent(in) :: iy          ! Y shift
  !
  ! Local
  integer i,j
  !
  trans = 0.0  ! That seemed to have been forgotten until 23-Sep-2009
  ! but this was only a side effect, anyway
  !
  do j=max(1,iy+1),min(ny,ny+iy)
    do i=max(1,ix+1),min(nx,nx+ix)
      trans(i,j) = in(i-ix,j-iy)
    enddo
  enddo
end subroutine translate
