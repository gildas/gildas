subroutine cmplx_mul (out,in,n)
  use gildas_def
  !----------------------------------------------
  ! @ no-interface
  !   Complex multiplication
  !-----------------------------------------------
  integer(kind=size_length), intent(in) :: n     !
  real, intent(out) :: out(2,n)                  !
  real, intent(in) :: in(2,n)                    !
  ! Local
  integer(kind=size_length) :: i
  real ar,ai
  do i=1,n
    ar = in(1,i)*out(1,i)-in(2,i)*out(2,i)
    ai = in(1,i)*out(2,i)+in(2,i)*out(1,i)
    out(1,i) = ar
    out(2,i) = ai
  enddo
end subroutine cmplx_mul
