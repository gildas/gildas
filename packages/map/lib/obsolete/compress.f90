subroutine compress(nx,ny,large,wl,mx,my,small,ws,wfft)
  use gkernel_interfaces, only : fourt
  !----------------------------------------------------------------------
  !	@ public
  ! 
  ! MAPPING
  !   Compress an image through FT truncation
  !----------------------------------------------------------------------
  integer, intent(in) :: nx,ny,mx,my
  real, intent(in) :: large(nx,ny)
  complex, intent(inout) :: wl(nx,ny)
  real, intent(out) :: small(mx,my)
  complex, intent(inout) :: ws(mx,my)
  real, intent(inout) :: wfft(*)
  !
  integer :: j,ndim,dim(2)
  real fact
  !
  wl(:,:) = cmplx(large,0.0)
  !
  ndim = 2
  dim(1) = nx
  dim(2) = ny
  call fourt(wl,dim,ndim,-1,0,wfft)
  !
  ! Keep inner quarter
  do j=1,my/2
    ws(1:mx/2,j) = wl(1:mx/2,j)   
    ws(mx/2+1:mx,j) = wl(nx-mx/2+1:nx,j) 
  enddo
  do j=my/2+1,my
    ws(1:mx/2,j) = wl(1:mx/2,j+ny-my) 
    ws(mx/2+1:mx,j) = wl(nx-mx/2+1:nx,j+ny-my) 
  enddo
  dim(1) = mx
  dim(2) = my
  call fourt (ws,dim,ndim,1,1,wfft)
  fact = 1.0/(nx*ny)
  small(:,:) = fact*real(ws)
end subroutine compress
