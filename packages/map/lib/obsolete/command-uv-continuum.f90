!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Old code?
!
!!$subroutine uv_baseline(line,error)
!!$  use gkernel_interfaces
!!$  use gildas_def
!!$  use gkernel_types
!!$  use gbl_format
!!$  use gbl_message
!!$  use file_buffers
!!$  !----------------------------------------------------------
!!$  ! @ private
!!$  !
!!$  ! MAPPING Support for commands
!!$  !   UV_BASELINE [Degree] /CHANNEL ListVariable [/ZERO]
!!$  !   UV_BASELINE [Degree] /FREQUENCIES ListFreq /WIDTH Width
!!$  !   UV_BASELINE [Degree] /VELOCITIES ListVelo /WIDTH Width
!!$  !
!!$  ! Subtract a continuum baseline, ignoring a list of channels
!!$  ! in the current UV data set.
!!$  !----------------------------------------------------------------
!!$  character(len=*), intent(inout) :: line  ! Command line
!!$  logical, intent(out) :: error            ! Error flag
!!$  !
!!$  character(len=*), parameter :: rname='UV_BASELINE'
!!$  external :: t_baseline
!!$  integer :: degree
!!$  !
!!$  degree = 0
!!$  call sic_i4(line,0,1,degree,.false.,error)
!!$  if (error) return
!!$  if (degree.ne.0 .and. degree.ne.1) then
!!$    call map_message(seve%e,rname,'Only degree 0 or 1 supported')
!!$    error = .true.
!!$    return
!!$  endif
!!$  !
!!$  call uv_filter_base (line,error,rname,t_baseline,degree)
!!$end subroutine uv_baseline
!!$!
!!$subroutine uv_filter(line,error)
!!$  use gkernel_interfaces
!!$  use gildas_def
!!$  use gkernel_types
!!$  use gbl_format
!!$  use gbl_message
!!$  use file_buffers
!!$  !----------------------------------------------------------------
!!$  ! @ private
!!$  !
!!$  ! MAPPING  support for
!!$  !   UV_FILTER  /CHANNEL ListVariable [/ZERO]
!!$  !   UV_FILTER  /FREQUENCIES ListFreq /WIDTH Width [UNIT]
!!$  !   UV_FILTER  /VELOCITY ListVelo /WIDTH Width [UNIT]
!!$  !
!!$  ! "Filter", i.e. flag, a list of channels in the current UV
!!$  ! data set. Flagging is reversible, unless the /ZERO option is
!!$  ! present. With /ZERO, the "filtered" visibilities are set to zero.
!!$  !----------------------------------------------------------------
!!$  character(len=*), intent(inout) :: line  ! Command line
!!$  logical, intent(out) :: error            ! Error flag
!!$  !
!!$  character(len=*), parameter :: rname='UV_FILTER'
!!$  external :: t_filter
!!$  !
!!$  integer, parameter :: opt_chan=1
!!$  integer, parameter :: opt_freq=2
!!$  integer, parameter :: opt_velo=3
!!$  integer, parameter :: opt_width=4
!!$  integer, parameter :: opt_zero=5
!!$  integer :: zero
!!$  !
!!$  if (sic_present(opt_zero,0)) then
!!$    zero = 0
!!$  else
!!$    zero = 1
!!$  endif
!!$  !
!!$  call uv_filter_base (line,error,rname,t_filter,zero)
!!$end subroutine uv_filter
!!$!
!!$subroutine uv_filter_base(line,error,rname,t_routine,zero)
!!$  use gkernel_interfaces
!!$  use gildas_def
!!$  use gkernel_types
!!$  use gbl_format
!!$  use gbl_message
!!$  use file_buffers
!!$  !----------------------------------------------------------------
!!$  ! @ private
!!$  !
!!$  ! MAPPING  support for
!!$  !   UV_FILTER or UV_BASELINE  /CHANNEL ListVariable [/ZERO]
!!$  !   UV_FILTER or UV_BASELINE  /FREQUENCIES ListFreq /WIDTH Width [UNIT]
!!$  !   UV_FILTER or UV_BASELINE  /VELOCITY ListVelo /WIDTH Width [UNIT]
!!$  !
!!$  ! "Filter", i.e. flag, a list of channels in the current UV
!!$  ! data set. Flagging is reversible, unless the /ZERO option is
!!$  ! present. With /ZERO, the "filtered" visibilities are set to zero.
!!$  !----------------------------------------------------------------
!!$  character(len=*), intent(inout) :: line  ! Command line
!!$  logical, intent(out) :: error            ! Error flag
!!$  external :: t_routine
!!$  character(len=*), intent(in) :: rname
!!$  integer, intent(in) :: zero
!!$  !
!!$  real(kind=8), parameter :: pi=3.14159265358979323846d0
!!$  real(kind=8), parameter :: f_to_k = 2.d0*pi/299792458.d-6
!!$  !
!!$  include 'gbl_memory.inc'
!!$  !
!!$  character(len=64) :: listname
!!$  logical :: found
!!$  integer(kind=address_length) :: jpd
!!$  type(sic_descriptor_t) :: desc
!!$  integer :: nf, k, i, j, l, m, ichan, jchan, narg, ier
!!$  real(8) :: freq, velo
!!$  real(4) :: width
!!$  integer, allocatable :: channels(:)
!!$  integer :: na, nstyle
!!$  integer, parameter :: mstyle=3
!!$  character(len=12) :: vstyle(mstyle), astyle, argu
!!$  data vstyle/'CHANNEL','FREQUENCY','VELOCITY'/
!!$  integer, parameter :: opt_chan=1
!!$  integer, parameter :: opt_freq=2
!!$  integer, parameter :: opt_velo=3
!!$  integer, parameter :: opt_width=4
!!$  !
!!$  if (sic_present(opt_chan,0)) then
!!$    narg = sic_narg(opt_chan)
!!$    if (narg.gt.1) then
!!$      allocate(channels(narg),stat=ier)
!!$      do i=1,narg
!!$        call sic_i4(line,opt_chan,i,channels(i),.true.,error)
!!$        if (error) return
!!$      enddo
!!$      call t_routine (narg,channels,zero,error)
!!$      !
!!$    else
!!$      call sic_ch(line,opt_chan,1,listname,na,.true.,error)
!!$      call sic_descriptor(listname,desc,found)
!!$      if (.not.found) then
!!$        call sic_i4(line,opt_chan,1,ichan,.true.,error)
!!$        if (error) then
!!$          call map_message(seve%e,rname,'Variable '//trim(listname)//' does not exists.')
!!$          error = .true.
!!$        endif
!!$        call t_routine (1,ichan,zero,error)
!!$      endif
!!$      if (desc%type.ne.fmt_i4) then
!!$        call map_message(seve%e,rname,'Variable '//trim(listname)//' must be Integer ')
!!$        error = .true.
!!$        return
!!$      endif
!!$      if (desc%ndim.ne.1) then
!!$        call map_message(seve%e,rname,'Variable '//trim(listname)//' must have rank 1')
!!$        error = .true.
!!$        return
!!$      endif
!!$      jpd = gag_pointer(desc%addr,memory)
!!$      nf = desc%dims(1)
!!$      call t_routine (nf,memory(jpd),zero,error)
!!$      if (error) call map_message(seve%e,rname,'Memory allocation error')
!!$    endif
!!$    !
!!$  else if (sic_present(opt_freq,0)) then
!!$    width = abs(huv%gil%fres)  ! In MHz
!!$    call sic_r4(line,opt_width,1,width,.false.,error)
!!$    if (sic_present(opt_width,2)) then
!!$      call sic_ke(line,opt_width,2,argu,na,.true.,error)
!!$      call sic_ambigs (rname,argu,astyle,nstyle,vstyle,mstyle,error)
!!$      select case(astyle)
!!$        case ('CHANNEL')
!!$          width = width*abs(huv%gil%fres)
!!$        case ('VELOCITY')
!!$          width = width*abs(huv%gil%fres/huv%gil%vres)
!!$      end select
!!$    endif
!!$    width = 0.5*width
!!$    !
!!$    narg = sic_narg(opt_freq)
!!$    allocate(channels(huv%gil%nchan),stat=ier)
!!$    if (ier.ne.0) then
!!$      call map_message(seve%e,rname,'Channels allocation error')
!!$      error = .true.
!!$      return
!!$    endif
!!$    k = 0
!!$    !
!!$    do i=1,narg
!!$      call sic_r8(line,opt_freq,i,freq,.true.,error)
!!$      if (error) return
!!$      if (huv%gil%fres.gt.0) then
!!$        ichan = (freq-huv%gil%freq-width)/huv%gil%fres + huv%gil%ref(1)
!!$        jchan = (freq-huv%gil%freq+width)/huv%gil%fres + huv%gil%ref(1)
!!$      else
!!$        ichan = (freq-huv%gil%freq+width)/huv%gil%fres + huv%gil%ref(1)
!!$        jchan = (freq-huv%gil%freq-width)/huv%gil%fres + huv%gil%ref(1)
!!$      endif
!!$      !
!!$      ! Set channels only once...
!!$      do j=ichan,jchan
!!$        if (j.lt.0 .or. j.gt.huv%gil%nchan) then
!!$          cycle
!!$        else
!!$          m = 0
!!$          do l=1,k
!!$            if (channels(l).eq.j) then
!!$              m = l
!!$              exit
!!$            endif
!!$          enddo
!!$          if (m.eq.0) then
!!$            k = k+1
!!$            channels(k) = j
!!$          endif
!!$        endif
!!$      enddo
!!$    enddo
!!$    call t_routine(k,channels,zero,error)
!!$    deallocate(channels)
!!$    if (error) call map_message(seve%e,rname,'Memory allocation error')
!!$    !
!!$  else if (sic_present(opt_velo,0)) then
!!$    width = abs(huv%gil%vres)  ! In km/s
!!$    call sic_r4(line,opt_width,1,width,.false.,error)
!!$    if (sic_present(opt_width,2)) then
!!$      call sic_ke(line,opt_width,2,argu,na,.true.,error)
!!$      call sic_ambigs (rname,argu,astyle,nstyle,vstyle,mstyle,error)
!!$      select case(astyle)
!!$        case ('CHANNEL')
!!$          width = width*abs(huv%gil%vres)
!!$        case ('FREQUENCY')
!!$          width = width*abs(huv%gil%vres/huv%gil%fres)
!!$      end select
!!$    endif
!!$    width = 0.5*width
!!$    !
!!$    narg = sic_narg(opt_velo)
!!$    allocate(channels(huv%gil%nchan),stat=ier)
!!$    if (ier.ne.0) then
!!$      call map_message(seve%e,rname,'Channels allocation error')
!!$      error = .true.
!!$      return
!!$    endif
!!$    k = 0
!!$    !
!!$    do i=1,narg
!!$      call sic_r8(line,opt_velo,i,velo,.true.,error)
!!$      if (error) return
!!$      if (huv%gil%vres.gt.0) then
!!$        ichan = (velo-huv%gil%voff-width)/huv%gil%vres + huv%gil%ref(1)
!!$        jchan = (velo-huv%gil%voff+width)/huv%gil%vres + huv%gil%ref(1)
!!$      else
!!$        ichan = (velo-huv%gil%voff+width)/huv%gil%vres + huv%gil%ref(1)
!!$        jchan = (velo-huv%gil%voff-width)/huv%gil%vres + huv%gil%ref(1)
!!$      endif
!!$      !
!!$      ! Set channels only once...
!!$      do j=ichan,jchan
!!$        if (j.lt.0 .or. j.gt.huv%gil%nchan) then
!!$          cycle
!!$        else
!!$          m = 0
!!$          do l=1,k
!!$            if (channels(l).eq.j) then
!!$              m = l
!!$              exit
!!$            endif
!!$          enddo
!!$          if (m.eq.0) then
!!$            k = k+1
!!$            channels(k) = j
!!$          endif
!!$        endif
!!$      enddo
!!$    enddo
!!$    call t_routine(k,channels,zero,error)
!!$    deallocate(channels)
!!$    if (error) call map_message(seve%e,rname,'Memory allocation error')
!!$  else
!!$    call map_message(seve%e,rname,'Missing option /CHANNEL or /FREQUENCY')
!!$    error = .true.
!!$    return
!!$  endif
!!$  !
!!$  save_data(code_save_uv) = .true.
!!$  !
!!$end subroutine uv_filter_base
!!$  !
!!$  subroutine t_baseline(mf,filter,degree,error)
!!$    use gildas_def
!!$    use image_def
!!$    !----------------------------------------------------------
!!$    ! Subtract a baseline with a list of channels to be ignored
!!$    !----------------------------------------------------------
!!$    integer, intent(in)    :: mf          ! Number of values
!!$    integer, intent(in)    :: filter(mf)  ! Channel list
!!$    integer, intent(in)    :: degree      ! Polynomial degree
!!$    logical, intent(inout) :: error
!!$    !
!!$    integer :: nf, nv, nc, iv, i, j, ier
!!$    integer, allocatable :: filtre(:)
!!$    real, allocatable :: wreal(:), wimag(:), wx(:), wxx(:), wxy(:)
!!$    real :: mreal, mimag, sx, sy, sxx, sxy, a, b, delta
!!$    !
!!$    nv = huv%gil%dim(2)
!!$    nc = huv%gil%nchan
!!$    !
!!$    ! FILTER contains the ones to be ignored.
!!$    ! We must build the list of valid ones...
!!$    allocate(filtre(nc),stat=ier)
!!$    error = (ier.ne.0)
!!$    if (error) return
!!$    !
!!$    nf = 0
!!$    do i=1,nc
!!$       nf = nf+1
!!$       filtre(nf) = i
!!$       do j=1,mf
!!$          if (filter(j).eq.i) then
!!$             nf = nf-1
!!$             exit
!!$          endif
!!$       enddo
!!$    enddo
!!$    !
!!$    ! Allocate a work array for Real and Imaginary parts
!!$    !
!!$    if (degree.eq.0) then
!!$       allocate(wreal(nf),wimag(nf),stat=ier)
!!$    else
!!$       allocate(wreal(nf),wimag(nf),wx(nf),wxx(nf),wxy(nf),stat=ier)
!!$       wx = filtre(1:nf)
!!$    endif
!!$    error = (ier.ne.0)
!!$    if (error) return
!!$    !
!!$    do iv=1,nv
!!$       do i=1,nf
!!$          wreal(i) = duv(5+3*filtre(i),iv)
!!$          wimag(i) = duv(6+3*filtre(i),iv)
!!$       enddo
!!$       !
!!$       ! Compute the fitting polynomials.
!!$       ! Filtre(:) is the channel number which can be used as abscissa
!!$       ! For the time being, only mean value...
!!$       if (degree.eq.0) then
!!$          mreal = sum(wreal)/nf
!!$          mimag = sum(wimag)/nf
!!$          !
!!$          ! Subtract
!!$          do i=1,nc
!!$             duv(5+3*i,iv) = duv(5+3*i,iv)-mreal
!!$             duv(6+3*i,iv) = duv(6+3*i,iv)-mimag
!!$          enddo
!!$       else if (degree.eq.1) then
!!$          ! This code actually does a poor job. There is not enough
!!$          ! Signal to noise ratio to fit a linear baseline per Visibility
!!$          sx = sum(wx)
!!$          sy = sum(wreal)
!!$          wxx = wx**2
!!$          sxx = sum(wxx)
!!$          wxy = wx*wreal
!!$          sxy = sum(wxy)
!!$          delta = (nf*sxx-sx**2)
!!$          a = (sxx*sy-sx*sxy)/delta
!!$          b = (nf*sxy-sx*sy)/delta
!!$          do i=1,nc
!!$             duv(5+3*i,iv) = duv(5+3*i,iv)-(a+b*i)
!!$          enddo
!!$          !
!!$          sy = sum(wimag)
!!$          wxx = wx**2
!!$          sxx = sum(wxx)
!!$          wxy = wx*wimag ! Bug correction 15-Dec-2014
!!$          sxy = sum(wxy)
!!$          delta = (nf*sxx-sx**2)
!!$          a = (sxx*sy-sx*sxy)/delta
!!$          b = (nf*sxy-sx*sy)/delta
!!$          do i=1,nc
!!$             duv(6+3*i,iv) = duv(6+3*i,iv)-(a+b*i)
!!$          enddo
!!$       endif
!!$    enddo
!!$    if (degree.eq.0) then
!!$       deallocate (filtre,wreal,wimag)
!!$    else
!!$       deallocate (filtre,wreal,wimag,wx,wxx,wxy)
!!$    endif
!!$    error = .false.
!!$  end subroutine t_baseline
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
