!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module map_buffers
  !
  public :: map_buffer
  private
  !
  type map_buffer_user_t
     integer, public :: version = 0     ! Parameter to select the imaging tool version => 0 means optimized code
     logical, public :: error = .false. ! SIC logical return status
   contains
     procedure, public :: init   => map_buffer_user_init
     procedure, public :: sicdef => map_buffer_user_sicdef
  end type map_buffer_user_t
  type(map_buffer_user_t) :: map_buffer
  !
contains
  !
  subroutine map_buffer_user_init(user,error)
    !-----------------------------------------------------------------------
    ! Initialize by setting the intent to out
    !-----------------------------------------------------------------------
    class(map_buffer_user_t), intent(out)   :: user
    logical,                  intent(inout) :: error
  end subroutine map_buffer_user_init
  !
  subroutine map_buffer_user_sicdef(user,error)
    use gkernel_interfaces
    !----------------------------------------------------------------------
    ! Define associated SIC variables
    !----------------------------------------------------------------------
    class(map_buffer_user_t), intent(out)   :: user
    logical,                  intent(inout) :: error
    !
    call sic_def_logi('MAPPING_ERROR',map_buffer%error,.false.,error)
    if (error) return
    call sic_def_inte('MAP_VERSION',map_buffer%version,0,0,.false.,error)
    if (error) return
  end subroutine map_buffer_user_sicdef
end module map_buffers
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
