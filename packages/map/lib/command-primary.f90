!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module mapping_primary
  use gbl_message
  !
  public :: primary_comm,get_bsize,primary_beam,mos_primary
  private
  !
contains
  !
  subroutine primary_comm(line,error)
    use phys_const
    use image_def, only: def_extr_words
    use gkernel_interfaces
    use file_buffers
    use clean_buffers
    use primary_buffers
    !------------------------------------------------------------------------
    ! PRIMARY [BeamSize] [/TRUNCATE Percent]
    !
    ! Correct for primary beam
    !------------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    integer, parameter :: o_trunc=1
    !
    real, allocatable :: dflat(:,:)
    integer :: i, ier
    real :: btrunc
    real :: bsize
    character(len=80) :: mess
    logical, parameter :: fromressection=.true.
    character(len=*), parameter :: rname='PRIMARY'
    !
    if (sic_present(o_trunc,0)) then
       call get_bsize(clean%head,rname,line,.not.fromressection,bsize,error,OTRUNC=o_trunc,BTRUNC=btrunc)
    else
       call get_bsize(clean%head,rname,line,.not.fromressection,bsize,error,BTRUNC=btrunc)
    endif
    if (error) return
    !
    write(mess,'(a,f10.2,a,f6.0,a)') 'Correcting for a beam size of ',&
         & bsize/pi*180*3600,'" down to ',100*btrunc,'% '
    call map_message(seve%i,rname,mess)
    !
    if (allocated(sky%data)) then
       if (any(sky%head%gil%dim.ne.clean%head%gil%dim)) then
          print *,'Programming error - Sky & Clean mismatch'
          deallocate(sky%data)
       else
          call map_message(seve%i,rname,'Re-using sky memory')
          call sic_delvariable('SKY',.false.,error)
       endif
    endif
    !
    if (.not.allocated(sky%data)) then
       allocate (sky%data(clean%head%gil%dim(1), clean%head%gil%dim(2), clean%head%gil%dim(3)), stat=ier)
       if (ier.ne.0) then
          call map_message(seve%e,rname,'Memory allocation error')
          error = .true.
          return
       endif
    endif
    !
    call gildas_null(sky%head)
    call gdf_copy_header(clean%head,sky%head,error)
    !
    allocate(dflat(clean%head%gil%dim(1),clean%head%gil%dim(2)),stat=ier)
    if (ier.ne.0) then
       call map_message(seve%e,rname,'Memory allocation error')
       error = .true.
       return
    endif
    !
    call mos_primary(sky%head,dflat,bsize)
    where (dflat.lt.btrunc)
       dflat = 0
    else where
       dflat = 1.0/dflat
    end where
    !
    do i=1,clean%head%gil%dim(3)
       sky%data(:,:,i) = clean%data(:,:,i) * dflat
    enddo
    sky%head%loca%addr = locwrd(sky%data)
    call gdf_get_extrema(sky%head,error)
    clean%head%gil%extr_words = def_extr_words
    !
    call sic_def_real('SKY',sky%data,sky%head%gil%ndim,sky%head%gil%dim,.true.,error)
    save_data(code_save_sky) = .true.
  end subroutine primary_comm
  !
  subroutine get_bsize(h,rname,line,fromressection,bsize,error,otrunc,btrunc)
    use phys_const
    use image_def
    use gkernel_interfaces
    !---------------------------------------------------------------------
    ! Support for command
    !   PRIMARY [BeamSize] [/TRUNCATE Percent]
    !   UV_MAP  [BeamSize] [/TRUNCATE Percent]
    ! Check if there is any deconvolved data associated to h.
    ! Return the primary beam size in radian.
    ! Optionally return the primary beam truncation level.
    !---------------------------------------------------------------------
    type(gildas),              intent(in)    :: h               ! UV data header
    character(len=*),          intent(in)    :: rname           ! Caller's name
    character(len=*),          intent(in)    :: line            ! Command line
    logical,                   intent(in)    :: fromressection  !
    real(kind=4),              intent(out)   :: bsize           ! Beam size in radian
    logical,                   intent(inout) :: error           ! Error flag
    integer(kind=4), optional, intent(in)    :: otrunc          ! Truncation option number
    real(kind=4),    optional, intent(out)   :: btrunc          ! Truncation level [0,1]
    !
    character(len=60) :: chain
    integer(kind=4) :: nc
    real(kind=4) :: beamsize
    !
    ! Check if any deconvolved data
    if (h%loca%size.eq.0) then
       call map_message(seve%e,rname,'No input data')
       error = .true.
       return
    endif
    ! Get beamsize and do some sanity checks
    beamsize = primary_beam(rname,h,fromressection)
    if (beamsize.eq.0) then
       nc = len_trim(rname)+6
       chain(1:nc) = ' '
       chain(nc:) = 'Use command "SPECIFY TELESCOPE Name" to add one'
       call map_message(seve%e,rname,'No primary beam from data')
       call map_message(seve%r,rname,chain) 
       error = .true.
       return
    else
       bsize = beamsize
    endif
    if (abs(beamsize-bsize).gt.0.02*bsize) then
       write(chain,'(A,F8.1,A)') 'Specified beam differs from value in data ', &
            & beamsize*180*3600/pi,'"'
       call map_message(seve%w,rname,chain)
    endif
    write(chain,'(A,F8.1,A)') 'Primary beam ',bsize*180*3600/pi,'"'
    call map_message(seve%i,rname,chain)
    !
    ! Beam truncation level
    if (present(btrunc)) then
       call sic_get_real('MAP_TRUNCATE',btrunc,error)
       if (present(otrunc)) call sic_r4(line,otrunc,1,btrunc,.true.,error)
       if (error) return
       btrunc = btrunc*0.01
    else if (present(otrunc)) then
       call map_message(seve%f,rname,'Programming Error: OTRUNC present, but not BTRUNC')
       error = .true.
    endif
  end subroutine get_bsize
  !
  function primary_beam(rname,head,fromressection)
    use phys_const
    use gkernel_interfaces
    use image_def
    !---------------------------------------------------------------------
    ! Get telescope beamsize for the frequency of a GILDAS data set
    !---------------------------------------------------------------------  
    real(kind=4) :: primary_beam
    character(len=*), intent(in) :: rname
    type(gildas),     intent(in) :: head
    logical,          intent(in) :: fromressection
    !
    integer, parameter :: mteles=8
    character(len=12) :: cteles,kteles,vteles(mteles)
    character(len=68) :: chain
    integer(kind=4) :: iteles
    logical :: error
    real(kind=4) :: factors(mteles), diams(mteles), diamet, factor, beamsize, bsize
    !
    data vteles /'ACA','ALMA','ATCA','NOEMA','SMA','JVLA','VELETA','30M'/
    data diams  /7.,12.,20.,15.,6.,25.,30.0,30.0/
    ! These factors depend on dish illumination 
    !   Validated for NOEMA and VELETA
    !   Validated for ALMA
    ! unclear for ATCA, SMA and JVLA
    data factors /1.13,1.13,1.13,1.22,1.13,1.13,1.20,1.20/
    !
    beamsize = 0.0
    !
    if (head%gil%nteles.ne.0) then
       cteles = head%gil%teles(1)%ctele
       diamet = head%gil%teles(1)%diam
       kteles = cteles
       call sic_ambigs(' ',cteles,kteles,iteles,vteles,mteles,error)
       call map_message(seve%i,rname,'Found telescope '//trim(kteles)//' from data')
       if (error) then
          factor = 1.13
          call map_message(seve%w,rname,'Using default Beam size 1.13 Lambda/D')
          error = .false.
       else
          factor = factors(iteles)
       endif
       beamsize = factor*(clight_mhz/head%gil%freq)/diamet
    endif
    !
    if (fromressection) then
       if (head%gil%majo.ne.0) then    
          bsize = head%gil%majo
          if (beamsize.ne.0 .and. abs(beamsize-bsize).gt.0.02*beamsize) then
             write(chain,'(A,F8.1,A,F8.1,A)') 'Using major axis ',bsize*180*3600/pi, &
                  '" instead of Beam size ',beamsize*180*3600/pi,'"'
             call map_message(seve%w,rname,chain)
          endif
          beamsize = bsize
       endif
    endif
    !
    primary_beam = beamsize
  end function primary_beam
  !
  subroutine mos_primary(prim,primary,bsize)
    use image_def
    use gkernel_interfaces
    use gkernel_types
    !---------------------------------------------------------------------
    ! Compute a Gaussian primary beam
    !---------------------------------------------------------------------
    type(gildas), intent(in)    :: prim                                     ! Primary beam header
    real,         intent(inout) :: primary(prim%gil%dim(1),prim%gil%dim(2)) ! Values
    real,         intent(in)    :: bsize                                    ! Primary beam size in radian
    !
    logical :: error
    integer :: j,ier
    real :: offx,offy,beamx,beamy
    real, allocatable :: factorx(:),factory(:)
    real(8) :: doffx,doffy
    type(projection_t) :: proj
    !
    allocate (factory(prim%gil%dim(2)),factorx(prim%gil%dim(1)),stat=ier)
    !
    error = .false.
    call gwcs_projec(prim%gil%a0,prim%gil%d0,prim%gil%pang,prim%gil%ptyp,proj,error)
    call abs_to_rel(proj,prim%gil%ra,prim%gil%dec,doffx,doffy,1)
    offx = doffx/prim%gil%inc(1)
    offy = doffy/prim%gil%inc(2)
    !
    beamx = prim%gil%inc(1)/bsize*2.0*sqrt(log(2.0))
    beamy = prim%gil%inc(2)/bsize*2.0*sqrt(log(2.0))
    !
    do j=1,prim%gil%dim(2)
       factory(j) = exp(-((j-prim%gil%ref(2)-offy)*beamy)**2)
    enddo
    !
    do j=1,prim%gil%dim(1)
       factorx(j) = exp(-((j-prim%gil%ref(1)-offx)*beamx)**2)
    enddo
    !
    do j=1,prim%gil%dim(2)
       primary(:,j) = factorx * factory(j)
    enddo
    deallocate (factorx, factory)
  end subroutine mos_primary
end module mapping_primary
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
