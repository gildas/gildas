!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module uv_continuum
  use gbl_message
  !
  public :: uv_continuum_comm
  public :: map_beams,map_parameters
  private
  !
contains
  !
  subroutine uv_continuum_comm(line,error)
    use phys_const, only: pi ! f_to_k has a different unit here
    use image_def
    use gkernel_interfaces
    use mapping_interfaces, only: uvgmax
    use uvmap_types
    use file_buffers
    use uv_buffers
    use uvmap_buffers
    !----------------------------------------------------------
    ! UV_CONTINUUM /INDEX Alpha [Frequency]
    !   Create a continuum UV table from a Line one
    !----------------------------------------------------------
    character(len=*), intent(out)   :: line
    logical,          intent(inout) :: error
    !
    integer :: channels(3)
    integer :: uvcode
    integer :: msize,nchan,mc,nu,nv,i
    real, pointer :: duv_previous(:,:),duv_next(:,:)
    real :: alpha,uvmax,uvmin
    real(8) :: freq
    type (gildas) :: hcuv
    type (uvmap_par) :: map
    integer(kind=4), parameter :: optindex=1
    logical :: doindex,docopy
    character(len=*), parameter :: rname='UV_CONTINUUM'
    !
    real(kind=8), parameter :: f_to_k = 2.d0*pi/299792458.d-6
    !
    uvcode = 1 ! code_uvt
    !
    msize = maxval(uvmap_default%size)
    if (msize.eq.0) then
       map = uvmap_default
       freq = gdf_uv_frequency(huv)
       call uvgmax (huv,duv,uvmax,uvmin)
       ! Now transform UVMAX in kiloWavelength (including 2 pi factor)
       uvmax = uvmax*freq*f_to_k
       uvmin = uvmin*freq*f_to_k
       error = .false.
       call map_parameters(rname,map,freq,uvmax,uvmin,error)
       if (error) return
       msize = maxval(map%size)
    endif
    channels = 0
    call t_channel_sampling(rname,huv,channels(3),msize)
    !
    call sic_i4(line,0,1,channels(3),.false.,error)
    call sic_i4(line,0,2,channels(1),.false.,error)
    call sic_i4(line,0,3,channels(2),.false.,error)
    !
    nchan = huv%gil%nchan
    ! Use the <0 convention to start from the end
    if (channels(1).lt.0) then
       channels(1) = max(1,nchan+channels(1))
    else
       channels(1) = min(max(1,channels(1)),nchan)
    endif
    if (channels(2).lt.0) then
       channels(2) = max(channels(1),nchan+channels(2))
    else if (channels(2).eq.0) then
       channels(2) = nchan
    else
       channels(2) = max(channels(1),min(nchan,channels(2)))
    endif
    nchan = channels(2)-channels(1)+1
    !
    ! Make sure things remain tractable for "random"
    ! Frequency axis...
    if (huv%gil%nfreq.ne.0 .and. nchan.ne.1) then
       if (channels(3).ne.1) then
          !
          ! Ultimately, one may check here pseudo-linear axes...
          call map_message(seve%e,rname,'UV tables with random Frequency axis ' &
               & //' can only be converted with channel step = 1')
          error = .true.
          return
       endif
    endif
    !
    call gildas_null(hcuv, type = 'UVT')
    call gdf_copy_header(huv,hcuv,error)
    !
    docopy = .false.
    mc = nchan/channels(3)
    if (mc*channels(3).ne.nchan) mc = mc+1
    hcuv%gil%dim(2) = huv%gil%dim(2)*mc
    hcuv%gil%dim(1) = 10+huv%gil%ntrail
    if (huv%gil%column_pointer(code_uvt_freq).gt.0) then
      ! Frequency column already present => nothing to do for this column
      !
      ! The input table has already a frequency colum. If it has only
      ! one channel, it most likely is already a continuum table.
      ! A simple copy is performed (note that we can not recompute
      ! the frequency column, as the original line table is not available).
      docopy = huv%gil%nchan.eq.1
    else
      call map_message(seve%i,rname,'Adding frequency offset column in DAPS')
      ! hcuv%gil%ntrail = huv%gil%ntrail+1  ! Not needed (automatic?)
      hcuv%gil%dim(1) = hcuv%gil%dim(1)+1
      hcuv%gil%column_pointer(code_uvt_freq) = hcuv%gil%dim(1)
      hcuv%gil%column_size(code_uvt_freq) = 1
    endif
    hcuv%gil%nchan = 1
    hcuv%gil%nvisi = hcuv%gil%dim(2)
    hcuv%gil%inc(1) = huv%gil%inc(1)*channels(3)
    hcuv%gil%fres = channels(3)*hcuv%gil%fres
    hcuv%gil%vres = channels(3)*hcuv%gil%vres
    !
    hcuv%gil%nfreq = 0 ! Reset this
    !
    ! Shift trailing columns to handle Mosaicing
    nu = hcuv%gil%dim(1)
    nv = hcuv%gil%dim(2)
    do i=1,code_uvt_last
       if (huv%gil%column_pointer(i).gt.huv%gil%lcol) then
          hcuv%gil%column_pointer(i) = huv%gil%column_pointer(i)-huv%gil%lcol+10
       endif
    enddo
    hcuv%gil%lcol = 10
    !
    nullify (duv_previous, duv_next)
    call uv_find_buffers(rname,nu,nv,duv_previous,duv_next,error)
    if (error) return
    !
    hcuv%r2d => duv_next
    huv%r2d => duv_previous
    doindex = sic_present(optindex,0)
    if (doindex) then
       call sic_r4(line,optindex,1,alpha,.true.,error)
       if (error) return
    endif
    if (docopy) then
      call map_message(seve%w,rname,'Input table is already a continuum table: duplicating')
      ! Sanity
      if (doindex) then
        call map_message(seve%e,rname,'/INDEX is not possible when duplicating a continuum table')
        error = .true.
        return
      endif
      if (huv%gil%dim(1).ne.nu .or. huv%gil%dim(2).ne.nv) then
        call map_message(seve%e,rname,'Dimensions mismatch')
        error = .true.
        return
      endif
      ! Actual copy (note: can be UVT or TUV order)
      hcuv%r2d(1:nu,1:nv) = huv%r2d(1:nu,1:nv)
    else
      ! From a line table to a continuum table:
      if (doindex) then
        call t_continuum(huv,hcuv,channels,uvcode,error,alpha)
      else
        call t_continuum(huv,hcuv,channels,uvcode,error)
      endif
      if (error) return
    endif
    call uv_clean_buffers(duv_previous, duv_next,error)
    if (error) return
    !
    hcuv%gil%ref(1) = 1.D0
    !
    ! Copy back to UV data set
    call gdf_copy_header(hcuv,huv,error)
    !
    ! Indicate Weights have changed, optimization and save status
    do_weig = .true.
    optimize(code_save_uv)%change = optimize(code_save_uv)%change + 1
    save_data(code_save_uv) = .true.
    !
    ! Redefine SIC variables (mandatory)
    call sic_delvariable('UV',.false.,error)
    call sic_mapgildas('UV',huv,error,duv)
  end subroutine uv_continuum_comm
  !
  subroutine map_parameters(rname,map,freq,uvmax,uvmin,error,list)
    use phys_const, only: pi
    use gkernel_interfaces
    use mapping_primary
    use uvmap_types
    use file_buffers
    use uv_buffers
    !----------------------------------------------------------------------
    ! Prepare the MAP parameters for UV_MAP, MX or UV_RESTORE
    ! Reads this from SIC variables
    !-----------------------------------------------------------------------
    character(len=*),  intent(in)    :: rname       ! Input rname name (UV_MAP or MX)
    type (uvmap_par),  intent(inout) :: map         ! Map parameters
    real(8),           intent(inout) :: freq        ! Observing frequency
    real(4),           intent(in)    :: uvmax,uvmin ! Min & Max UV in m
    logical,           intent(inout) :: error
    logical, optional, intent(in)    :: list
    !
    real(8), parameter :: f_to_k = 2.d0*pi/299792458.d-6
    !
    type (uvmap_par), save :: last_map
    logical :: do_list
    logical, parameter :: fromressection=.true.
    integer :: osize(2)
    integer :: i,np
    real :: uvmi,uvma,xp,xm,hppb,bsize
    real(4) :: map_range(2)  ! Range of offsets
    real(4), parameter :: pixel_per_beam=5.0
    character(len=12) :: prefix
    character(len=message_length) :: chain
    !
    do_list = present(list)
    !
    ! For UV_STAT, reset all parameters to Zero
    if (rname.eq.'UV_STAT') then
       map%size = 0
       map%field = 0
       map%xycell = 0
    endif
    !
    freq = gdf_uv_frequency(huv)
    if (do_list) then                                            
       write(chain,'(A,F12.6,A)') "Observed rest frequency       ", &
            & freq*1d-3," GHz"
       call map_message(seve%i,rname,chain)
    endif
    !
    uvma = uvmax/(freq*f_to_k)
    uvmi = uvmin/(freq*f_to_k)
    if (do_list) then
       write(chain,100) huv%gil%dim(2),huv%gil%nchan
       call map_message(seve%i,rname,chain)
       write(chain,101) uvmi,uvma,' meters'
       call map_message(seve%i,rname,chain)
       write(chain,101) uvmin*1e-3/2.0/pi,uvmax*1e-3/2.0/pi,' kiloWavelength'
       call map_message(seve%i,rname,chain)
    endif
    !
    ! Get beam size (in radians)
    bsize = primary_beam(rname,huv,fromressection)
    !
    if (bsize.eq.0) then
       ! Default to IRAM Plateau de Bure case
       hppb = 63.0*(12.0/15.0)*(100.e3/freq) ! ["] Primary beam size
       write(chain,'(A,F12.1,A)') "Half power primary beam defaulted to ", &
            & hppb," arcsec"
       call map_message(seve%w,rname,chain)                         
       bsize = hppb*pi/3600.0/180.0
    else if (do_list .or. map%size(1).eq.0) then                                           
       write(chain,'(A,F12.1,A)') "Half power primary beam       ", &
            & bsize*180*3600/pi," arcsec"
       call map_message(seve%i,rname,chain)                         
    endif
    hppb = bsize*3600.*180./pi                                                        
    !
    ! Only 2 out of the 3 MAP_FIELD, MAP_SIZE and MAP_CELL
    !   can be non-zero. This should be tested in some way
    !
    if (map%xycell(1)*map%size(1)*map%field(1).ne.0) then
       if (abs(map%field(1)-map%size(1)*map%xycell(1)).gt.0.01*map%field(1)) then
          call map_message(seve%w,rname,'User specified MAP_FIELD, MAP_SIZE and MAP_CELL do not match')
       else
          call map_message(seve%w,rname,'MAP_FIELD, MAP_SIZE and MAP_CELL specified by user')    
       endif
    endif
    !
    ! Now get MAP_CELL and MAP_SIZE if Zero.
    if (map%xycell(1).eq.0) then
       if (map%size(1)*map%field(1).eq.0) then
          map%xycell(1) = 0.01*nint(360.0*3600.0*100.0/uvmax/pixel_per_beam)
          if (map%xycell(1).le.0.02) then
             map%xycell(1) = &
                  0.002*nint(360.0*3600.0*500.0/uvmax/pixel_per_beam)
          endif
          map%xycell(2) = map%xycell(1)  ! In ", rounded to 0.02 or 0.002"
       else
          map%xycell(1) = map%field(1)/map%size(1)
          map%xycell(2) = map%xycell(1)
       endif
    endif
    !
    ! Get MAP_SIZE if non Zero
    if (map%size(1).ne.0) then
       if (map%size(2).eq.0) map%size(2) = map%size(1)
       if (map%field(1).eq.0) map%field = map%size * map%xycell
    endif
    !
    ! Get a wide enough field of view (in ")
    if (map%field(1).ne.0) then
       if (map%field(2).eq.0) map%field(2) = map%field(1)
    else
       !
       if (map%nfields.ne.0) then
          ! The factor 2.5 on the primary beam size is to go down 
          ! low enough by default
          map%field = 2.5*hppb
          ! Enlarge the field of view by the Offset range
          np = abs(map%nfields)
          xm = minval(map%offxy(1,1:np))
          xp = maxval(map%offxy(1,1:np))
          map_range(1) = xp-xm
          xm = minval(map%offxy(2,1:np))
          xp = maxval(map%offxy(2,1:np))
          map_range(2) = xp-xm
          map_range = map_range*180*3600/pi
          !
          map%field = map%field + map_range
       else
          ! The factor 2 on the primary beam size is to go down to 6.25 % level by default
          map%field(1) = 2*hppb
          ! Map Field = 1.13*Lambda/Dmin - but round to 1.3
          if (uvmin.ne.0) then
             map%field(2) = 2.6*360*3600/uvmin ! " In arcsec
          else
             map%field(2) = map%field(1)
          endif
          ! The Min is to avoid going to very large images when uvmin is close to 0
          ! e.g. when short or Zero spacings have been added
          map%field = min(map%field(1),map%field(2))    
       endif
    endif
    !
    if (map%size(1).eq.0) then
       !
       map%size = map%field/map%xycell
       !
       ! map%size =   2**nint(log(real(map%size))/log(2.0))
       call gi4_round_forfft(map%size(1),osize(1),error,map_rounding,map_power)
       call gi4_round_forfft(map%size(1),osize(2),error,map_rounding,map_power)
       map%size = osize
    else if ( (mod(map%size(1),2).ne.0) .or. (mod(map%size(2),2).ne.0) ) then
       call map_message(seve%e,rname,'Number of pixels (MAP_SIZE) must be even')
       error = .true.
    endif
    !
    if (rname.eq.'UV_STAT') then
       prefix = 'Recommended '
    else
       if (error) return
       prefix = 'Current '
    endif
    write(chain,'(A,A,F12.1,a,F12.1,A)') prefix,'Field of view / Largest Scale ' &
         & ,map%field(1),' x ',map%field(2),' "'
    call map_message(seve%i,rname,chain)
    write(chain,'(A,A,F11.1,A,F11.1,A)') prefix,'Image size ', &
         & map%size(1)*map%xycell(1),' x ',map%size(2)*map%xycell(2),'" '
    call map_message(seve%i,rname,chain)
    !
    write(chain,'(A,A,I0,A,I0,A)') prefix,'Map size ', &
         & map%size(1),' x ',map%size(2)
    call map_message(seve%i,rname,chain)
    write(chain,'(A,A,F8.3,A,F8.3,A)') prefix,'Pixel size ', &
         & map%xycell(1),' x ',map%xycell(2),' "'
    call map_message(seve%i,rname,chain)
    !
    ! Do NOT adjust the Field while keeping the Cell size 
    !     map%field = map%size * map%xycell
    ! it is useless or incorrect...
    !
    if (rname.eq.'UV_STAT') return
    ! This part is only for UV_MAP / UV_MOSAIC, not for UV_STAT
    !
    ! Check whether Weights have to be recomputed
    if (map%mode.ne.last_map%mode) do_weig = .true.
    if (map%mode.ne.'NATURAL ') then
       if (map%uniform(1).ne.last_map%uniform(1)) do_weig = .true.
       if (map%uniform(2).ne.last_map%uniform(2)) do_weig = .true.
    endif
    if (map%ctype.ne.last_map%ctype) do_weig = .true.
    do i=1,4
       if (map%taper(i).ne.last_map%taper(i)) do_weig = .true.
    enddo
    last_map = map
    !
    ! Subroutine DOWEI uses map%uniform = 0.0 for Natural Weighting
    if (map%mode.eq.'NATURAL ') map%uniform(1) = 0
    !
100 format('Found ',i12,' Visibilities, ',i4,' channels')
101 format('Baselines ',f9.1,' - ',f9.1,a)
  end subroutine map_parameters
  !
  subroutine map_beams(rname,map_beam,huv,nx,ny,nb,nc)
    use image_def
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    character(len=*), intent(in)    :: rname
    integer,          intent(inout) :: map_beam
    type(gildas),     intent(in)    :: huv
    integer,          intent(in)    :: nx
    integer,          intent(in)    :: ny
    integer,          intent(in)    :: nc
    integer,          intent(out)   :: nb ! Number of beams
    !
    character(len=message_length) :: mess
    !
    if (map_beam.eq.-2) then
       nb = 1
       call map_message(seve%i,rname,'Producing a single beam for all channels')
    else
       !
       if (map_beam.eq.-1) then
          call t_channel_sampling(rname,huv,map_beam,min(nx,ny))
          nb = (nc+map_beam-1)/map_beam
          map_beam = (nc+nb-1)/nb
       endif
       if (map_beam.eq.0) then
          nb = 1
          map_beam = nc
       else
          nb = (nc+map_beam-1)/map_beam
          map_beam = (nc+nb-1)/nb
       endif
       write(mess,'(A,I4,A,I6,I6)') 'Producing one beam every ',map_beam, &
            &   ' channels, total ',nb,nc
       call map_message(seve%i,rname,mess)
    endif
  end subroutine map_beams
  !
  subroutine t_channel_sampling(rname,huv,nident,msize)
    use image_def
    use uvmap_buffers
    !----------------------------------------------------------
    ! Check how many channels can have the same beam
    !----------------------------------------------------------
    character(len=*), intent(in)  :: rname  ! Caller's name
    type(gildas),     intent(in)  :: huv    ! UV data header
    integer,          intent(out) :: nident ! returned step
    integer,          intent(in)  :: msize  ! Current map_size
    !
    real :: df, dc
    character(len=80) :: chain
    !
    !
    ! Allow Cell_Precis (default 10 %) pixel difference at map edge
    ! Relative Delta Frequency per channel is
    df = abs(huv%gil%fres/huv%gil%freq)
    !
    ! so scale error at map edge is, in pixel units
    dc = df * msize / 2
    !
    ! so allowed number of channels with same beam is
    nident = max(nint(uvmap_default%precis/dc),1)
    !
    write(chain,'(A,I6,A,F10.1)') 'Maximum number of channels '// &
         & 'for same beam ',nident,' Bandwidth ',nident*huv%gil%fres
    call map_message(seve%i,rname,chain)
    !
    ! With random Frequency axis, use only 1 channel
    if (huv%gil%nfreq.gt.1) nident = 1
  end subroutine t_channel_sampling
  !
  subroutine t_continuum(hluv,hcuv,channels,uvcode,error,alpha)
    use gkernel_interfaces
    use gildas_def
    use image_def
    !$ use omp_lib
    !----------------------------------------------------------
    ! Create a continuum UV table from a Line one
    !----------------------------------------------------------
    type (gildas),  intent(in)    :: hluv        ! Line UV header
    type (gildas),  intent(inout) :: hcuv        ! Continuum UV header
    integer,        intent(in)    :: channels(3) ! Channel selection
    integer,        intent(in)    :: uvcode      ! Type of UV data
    logical,        intent(inout) :: error
    real, optional, intent(in)    :: alpha       ! Spectral index
    !
    real(8) :: freq,fval,scale_uv,scale_flux,scale_w
    real, allocatable :: are(:), aim(:), awe(:), adw(:)
    real :: re, im, we, dw
    integer :: ifi,ila,ic,jc,kc,ier
    integer(kind=4) :: nv,iv,nt,ilcol
    integer(kind=4) ::    ov,   olcol,ofreq
    integer :: othread, nthread, ov_num
    real(8), allocatable :: uv_dfreq(:),uv_scale(:)
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='T>CONTINUUM'
    !
    integer, parameter :: code_uvt=1 ! This MUST NOT be changed ***JP: why only here?
    integer, parameter :: code_tuv=2 ! This MUST NOT be changed ***JP: why only here?
    !
    ! Define number of Visibilities and Channels...
    ! For input table:
    nv    = hluv%gil%nvisi
    nt    = hluv%gil%ntrail
    ilcol = hluv%gil%lcol  ! Last data column in input table (before trailing columns)
    ! For output table:
    olcol = 10             ! Last data column in output table (before trailing columns)
    ofreq = hcuv%gil%column_pointer(code_uvt_freq)
    !
    ov_num = (channels(2)-channels(1))/channels(3)+1  ! Number of output visibilities per input visibility
    !
    allocate(uv_dfreq(channels(2)),uv_scale(channels(2)),stat=ier)
    if (failed_allocate(rname,'uv_dfreq+uv_scale',ier,error))  return
    fval = hluv%gil%val(hluv%gil%faxi)
    do ic=channels(1),channels(2),channels(3)
       ! Frequency at center of the output channel
       ifi = ic
       ila = min(ic+channels(3)-1,channels(2))
       freq = gdf_uv_frequency(hluv,(ifi+ila)/2.d0)
       uv_dfreq(ic) = freq-fval
       !
       ! The angular scale goes as Lambda/B, i.e. 1/(B nu)
       ! so to preserve a constant angular scale, the "apparent"
       ! B must scale as \nu
       !
       ! so
       uv_scale(ic) = freq/fval ! This is the right formula...
       scale_uv = uv_scale(ic)
       write(mess,'(2(A,I0),2(A,F0.6),A,1PG16.9)')  &
           'Channels: ',ifi,'-',ila,  &
         ', fcent=',freq,', freq=',fval,  &
         ', UV scale=',scale_uv
       call map_message(seve%i,rname,mess)
    enddo
    !
    if (present(alpha)) then
       if (uvcode.eq.code_uvt) then  ! UVT order
          !
          !$OMP PARALLEL DEFAULT(none) &
          !$OMP   & SHARED(nv,ov_num,uv_dfreq,uv_scale,hluv,hcuv,channels,alpha,nt,ilcol,olcol,ofreq) &
          !$OMP   & PRIVATE(iv,ov,fval,ic,freq,re,im,we,kc,jc,dw) &
          !$OMP   & PRIVATE(scale_uv,scale_flux,scale_w)
          !
          !$OMP DO
          do iv=1,nv
             ov = (iv-1)*ov_num+1
             !
             ! Fill in, channel after channel
             do ic=channels(1),channels(2),channels(3)
                scale_uv = uv_scale(ic)
                scale_flux = scale_uv**(-alpha)
                scale_w = scale_uv**(2*alpha)
                !
                hcuv%r2d(1:3,ov) = hluv%r2d(1:3,iv)*scale_uv
                hcuv%r2d(4:7,ov) = hluv%r2d(4:7,iv)
                !
                ! Compact the channels first
                if (channels(3).gt.1) then
                   re = 0
                   im = 0
                   we = 0
                   kc = 5+3*ic
                   do jc = ic,min(ic+channels(3)-1,channels(2))
                      dw = max(0.,hluv%r2d(kc+2,iv))
                      re = re + hluv%r2d(kc,iv)*dw
                      kc = kc+1
                      im = im + hluv%r2d(kc,iv)*dw
                      we = we+dw
                      kc = kc+2
                   enddo
                   if (we.ne.0.0) then
                      re = re/we
                      im = im/we
                   end if
                   hcuv%r2d(8,ov) = re*scale_flux
                   hcuv%r2d(9,ov) = im*scale_flux
                   hcuv%r2d(10,ov) = we
                else
                   hcuv%r2d(8:9,ov)  = hluv%r2d(5+3*ic:6+3*ic,iv)*scale_flux
                   hcuv%r2d(10,ov)  = hluv%r2d(7+3*ic,iv)*scale_w
                endif
                if (nt.gt.0) then
                  hcuv%r2d(olcol+1:olcol+nt,ov) = hluv%r2d(ilcol+1:ilcol+nt,iv)  ! Might contain dfreq column
                endif
                hcuv%r2d(ofreq,ov) = uv_dfreq(ic)  ! Create or update value
                ov = ov+1
             enddo
             if (ov.ne.iv*ov_num+1) print *,'Programming error ',iv,ov,ov_num
          enddo
          !$OMP END DO
          !$OMP END PARALLEL
          !
       else if (uvcode.eq.code_tuv) then ! TUV order
          print *,'TUV order '
          if (channels(3).gt.1) then
             allocate (are(nv),aim(nv),awe(nv),adw(nv),stat=ier)
             if (ier.ne.0) then
                call map_message(seve%e,rname,'Channels allocation error')
                error = .true.
                return
             endif
          endif
          !
          ! Fill in, channel after channel
          othread = 1
          nthread = 1
          !$ othread = omp_get_max_threads()
          !$ nthread = min(othread,ov_num)
          !$ call omp_set_num_threads(nthread)
          !
          !$OMP PARALLEL DEFAULT(none) &
          !$OMP   & SHARED(nv,uv_dfreq,uv_scale,hluv,hcuv,channels,alpha,nt,ilcol,olcol,ofreq) &
          !$OMP   & PRIVATE(ic,ifi,ila,freq) &
          !$OMP   & PRIVATE(are,aim,awe,jc,adw) &
          !$OMP   & PRIVATE(scale_uv,scale_flux,scale_w)
          !
          !$OMP DO
          do ic=channels(1),channels(2),channels(3)
             ifi = (ic-1)*nv+1
             ila = ifi+nv-1
             scale_uv = uv_scale(ic)
             scale_flux = scale_uv**(-alpha)
             scale_w = scale_uv**(2*alpha)
             !
             hcuv%r2d(ifi:ila,1:3) = hluv%r2d(1:nv,1:3)*scale_uv
             hcuv%r2d(ifi:ila,4:7) = hluv%r2d(1:nv,4:7)
             !
             ! Compact the channels first
             if (channels(3).gt.1) then
                are = 0
                aim = 0
                awe = 0
                do jc = ic,min(ic+channels(3)-1,channels(2))
                   adw(:) = max(0.,hluv%r2d(1:nv,7+3*jc))
                   are(:) = are + hluv%r2d(1:nv,5+3*jc)*adw
                   aim(:) = aim + hluv%r2d(1:nv,6+3*jc)*adw
                   awe(:) = awe+adw
                enddo
                where (awe.ne.0.)
                   are(:) = are/awe
                   aim(:) = aim/awe
                end where
                hcuv%r2d(ifi:ila,8) = are*scale_flux
                hcuv%r2d(ifi:ila,9) = aim*scale_flux
                hcuv%r2d(ifi:ila,10) = awe*scale_w
             else
                hcuv%r2d(ifi:ila,8:9) = hluv%r2d(1:nv,5+3*ic:6+3*ic)*scale_flux
                hcuv%r2d(ifi:ila,10) = hluv%r2d(1:nv,7+3*ic)*scale_w
             endif
             if (nt.gt.0) then
                hcuv%r2d(ifi:ila,olcol+1:olcol+nt) = hluv%r2d(1:nv,ilcol+1:ilcol+nt)  ! Might contain dfreq column
             endif
             hcuv%r2d(ifi:ila,ofreq) = uv_dfreq(ic) ! Create or update value
          enddo
          !$OMP END DO
          !$OMP END PARALLEL
          !$ call omp_set_num_threads(othread)
          if (channels(3).gt.1) then
             deallocate (are,aim,awe,stat=ier)
          endif
       endif
    else
       ! Simple code with no spectral index
       if (uvcode.eq.code_uvt) then  ! UVT order
          !
          !$OMP PARALLEL DEFAULT(none) &
          !$OMP   & SHARED(nv,ov_num,uv_dfreq,uv_scale,hluv,hcuv,channels,nt,ilcol,olcol,ofreq) &
          !$OMP   & PRIVATE(iv,ov,fval,ic,freq,scale_uv,re,im,we,kc,jc,dw)
          !
          !$OMP DO
          do iv=1,nv
             ov = (iv-1)*ov_num+1
             !
             ! Fill in, channel after channel
             do ic=channels(1),channels(2),channels(3)
                scale_uv = uv_scale(ic)
                !
                hcuv%r2d(1:3,ov) = hluv%r2d(1:3,iv)*scale_uv
                hcuv%r2d(4:7,ov) = hluv%r2d(4:7,iv)
                !
                ! Compact the channels first
                if (channels(3).gt.1) then
                   re = 0
                   im = 0
                   we = 0
                   kc = 5+3*ic
                   do jc = ic,min(ic+channels(3)-1,channels(2))
                      dw = max(0.,hluv%r2d(kc+2,iv))
                      re = re + hluv%r2d(kc,iv)*dw
                      kc = kc+1
                      im = im + hluv%r2d(kc,iv)*dw
                      we = we+dw
                      kc = kc+2
                   enddo
                   if (we.ne.0.0) then
                      re = re/we
                      im = im/we
                   end if
                   hcuv%r2d(8,ov) = re
                   hcuv%r2d(9,ov) = im
                   hcuv%r2d(10,ov) = we
                else
                   hcuv%r2d(8:10,ov)  = hluv%r2d(5+3*ic:7+3*ic,iv)
                endif
                if (nt.gt.0) then
                   hcuv%r2d(olcol+1:olcol+nt,ov) = hluv%r2d(ilcol+1:ilcol+nt,iv)  ! Might contain dfreq column
                endif
                hcuv%r2d(ofreq,ov) = uv_dfreq(ic) ! Create or update value
                ov = ov+1
             enddo
             if (ov.ne.iv*ov_num+1) print *,'Programming error ',iv,ov,ov_num
          enddo
          !$OMP END DO
          !$OMP END PARALLEL
          !
       else if (uvcode.eq.code_tuv) then ! TUV order
          print *,'TUV order '
          if (channels(3).gt.1) then
             allocate (are(nv),aim(nv),awe(nv),adw(nv),stat=ier)
             if (ier.ne.0) then
                call map_message(seve%e,rname,'Channels allocation error')
                error = .true.
                return
             endif
          endif
          !
          ! Fill in, channel after channel
          othread = 1
          nthread = 1
          !$ othread = omp_get_max_threads()
          !$ nthread = min(othread,ov_num)
          !$ call omp_set_num_threads(nthread)
          !
          !$OMP PARALLEL DEFAULT(none) &
          !$OMP   & SHARED(nv,uv_dfreq,uv_scale,hluv,hcuv,channels,nt,ilcol,olcol,ofreq) &
          !$OMP   & PRIVATE(ic,ifi,ila,freq,scale_uv) &
          !$OMP   & PRIVATE(are,aim,awe,jc,adw)
          !
          !$OMP DO
          do ic=channels(1),channels(2),channels(3)
             ifi = (ic-1)*nv+1
             ila = ifi+nv-1
             scale_uv = uv_scale(ic)
             !
             hcuv%r2d(ifi:ila,1:3) = hluv%r2d(1:nv,1:3)*scale_uv
             hcuv%r2d(ifi:ila,4:7) = hluv%r2d(1:nv,4:7)
             !
             ! Compact the channels first
             if (channels(3).gt.1) then
                are = 0
                aim = 0
                awe = 0
                do jc = ic,min(ic+channels(3)-1,channels(2))
                   adw(:) = max(0.,hluv%r2d(1:nv,7+3*jc))
                   are(:) = are + hluv%r2d(1:nv,5+3*jc)*adw
                   aim(:) = aim + hluv%r2d(1:nv,6+3*jc)*adw
                   awe(:) = awe+adw
                enddo
                where (awe.ne.0.)
                   are(:) = are/awe
                   aim(:) = aim/awe
                end where
                hcuv%r2d(ifi:ila,8) = are
                hcuv%r2d(ifi:ila,9) = aim
                hcuv%r2d(ifi:ila,10) = awe
             else
                hcuv%r2d(ifi:ila,8:10) = hluv%r2d(1:nv,5+3*ic:7+3*ic)
             endif
             if (nt.gt.0) then
                hcuv%r2d(ifi:ila,olcol+1:olcol+nt) = hluv%r2d(1:nv,ilcol+1:ilcol+nt)  ! Might contain dfreq column
             endif
             hcuv%r2d(ifi:ila,ofreq) = uv_dfreq(ic)  ! Create or update value
          enddo
          !$OMP END DO
          !$OMP END PARALLEL
          !$ call omp_set_num_threads(othread)
          if (channels(3).gt.1) then
             deallocate (are,aim,awe,stat=ier)
          endif
       endif
    endif
  end subroutine t_continuum
  !
  subroutine t_filter(mf,filter,zero,error)
    use gildas_def
    use image_def
    use uv_buffers
    !----------------------------------------------------------
    ! Filter / Flag a list of channels
    !----------------------------------------------------------
    integer, intent(in)    :: mf          ! Number of values
    integer, intent(in)    :: filter(mf)  ! Channel list
    integer, intent(in)    :: zero        ! Zero or not
    logical, intent(inout) :: error
    !
    integer :: nf, nv, nc, iv, i, ier
    integer, allocatable :: filtre(:)
    !
    nv = huv%gil%dim(2)
    nc = huv%gil%nchan
    !
    allocate(filtre(mf),stat=ier)
    error = ier.ne.0
    if (error) return
    !
    nf = 0
    do i=1,mf
       if (filter(i).gt.0 .and. filter(i).le.nc) then
          nf = nf+1
          filtre(nf) = filter(i)
       endif
    enddo
    !
    if (zero.eq.0) then
       do iv=1,nv
          do i=1,nf
             duv(5+3*filtre(i):7+3*filtre(i),iv) = 0
          enddo
       enddo
    else
       do iv=1,nv
          do i=1,nf
             duv(7+3*filtre(i),iv) = -abs(duv(7+3*filtre(i),iv))
          enddo
       enddo
    endif
    deallocate (filtre)
  end subroutine t_filter
end module uv_continuum
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
