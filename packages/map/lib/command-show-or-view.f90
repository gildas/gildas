!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module mapping_show_or_view
  use gbl_message
  !
  public :: show_or_view_comm,show_or_view_main
  public :: buffer_copy,create_fields
  private
  !
contains
  !
  subroutine show_or_view_comm(comm,line,error)
    use gkernel_interfaces
    use file_buffers
    use uvmap_buffers
    !----------------------------------------------------------------------
    ! SHOW name or VIEW name  
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: comm
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    integer :: ntype,nc
    character :: argu*12,dtype*12
    !
    ! Parse input line
    call sic_ke(line,0,1,argu,nc,.true.,error)
    if (error) return
    if (argu.eq.'?') argu = ' '
    call sic_ambigs(comm,argu,dtype,ntype,vtype,mtype,error)
    if (error) return
    !
    ! Special case for FIELDS
    if (dtype.eq.'FIELDS') then
       if (primbeam%head%loca%size.eq.0 .or. comm.eq.'SHOW') then
          if (uvmap_prog%nfields.eq.0) then
             call map_message(seve%e,comm,'No Mosaic loaded')
             error = .true.
          else
             call exec_program('@ p_plot_fields.map FIELDS')
          endif
          return
       endif
    endif
    !
    call show_or_view_main(comm,ntype,line,error)
    if (sic_lire().eq.0) call sic_insert(line)
  end subroutine show_or_view_comm
  !
  subroutine show_or_view_main(comm,ntype,line,error)
    use gkernel_interfaces
    use file_buffers
    use plot_buffers
    use uv_buffers
    !----------------------------------------------------------------------
    ! SHOW Name
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: comm
    character(len=*), intent(in)    :: line
    integer,          intent(in)    :: ntype ! Buffer type
    logical,          intent(inout) :: error
    !
    character(len=40) :: argu1,argu2,chain,c_more
    integer :: nc
    logical :: do_dirty
    character(len=12) :: xtype,ytype
    !
    ! Load into SIC buffer
    if (comm.eq.'SHOW' .and. vtype(ntype).eq.'COVERAGE') then
       call sic_get_char('XTYPE',xtype,nc,error)
       call sic_get_char('YTYPE',ytype,nc,error)
       call buffer_copy('UV','W',w_plot,error)
       call sic_let_char('XTYPE','u',error)
       call sic_let_char('YTYPE','v',error)
       chain = 'UV'
       call show_uv(chain,error)
       call sic_let_char('XTYPE',xtype,error)
       call sic_let_char('YTYPE',ytype,error)
       return
    endif
    !
    call buffer_copy(vtype(ntype),'W',w_plot,error)
    if (error) return
    ! Plot SIC buffer with adequate plotting procedure
    if (comm.eq.'SHOW' .or. comm.eq.'UV_FLAG') then
       if (vtype(ntype).eq.'UV') then
          argu1 = ' '
          call sic_ch(line,0,2,argu1,nc,.false.,error)
          argu2 = ' '
          call sic_ch(line,0,3,argu2,nc,.false.,error)
          chain = vtype(ntype)//' '//trim(argu1)//' '//trim(argu2)
          call show_uv(chain,error)
       else if (vtype(ntype).eq.'CCT') then
          argu1 = ' '
          call sic_ch(line,0,2,argu1,nc,.false.,error)
          call exec_program('@ p_cct_w.map w '//argu1)
       else
          c_more = ' '
          call sic_get_logi('DO_DIRTY',do_dirty,error)
          if (do_dirty) then
             call buffer_copy('BEAM','BPLOT',b_plot,error)
             c_more = 'BPLOT'
          endif
          !
          argu1 = ' '
          call sic_ch(line,0,2,argu1,nc,.false.,error)
          argu2 = ' '
          call sic_ch(line,0,3,argu2,nc,.false.,error)
          chain = vtype(ntype)//' W '//trim(c_more)//' '//trim(argu1)//' '//trim(argu2)
          call exec_program('@ p_plot_w.map '//chain)
       endif
    else if (comm.eq.'VIEW') then
       if (vtype(ntype).eq.'UV') then
          continue
       else if (vtype(ntype).eq.'CCT') then
          argu1 = ' '
          call sic_ch(line,0,2,argu1,nc,.false.,error)
          argu2 = ' '
          call sic_ch(line,0,3,argu2,nc,.false.,error)
          chain = vtype(ntype)//' W '//trim(argu1)//' '//trim(argu2)
          call exec_program('@ p_cct_vue.map '//chain)
       else
          argu1 = ' '
          call sic_ch(line,0,2,argu1,nc,.false.,error)
          argu2 = ' '
          call sic_ch(line,0,3,argu2,nc,.false.,error)
          chain = vtype(ntype)//' W '//trim(argu1)//' '//trim(argu2)
          call exec_program('@ p_view_w.map '//chain)
       endif
    endif
  end subroutine show_or_view_main
  !
  subroutine show_uv(chain,error)
    use uv_buffers
    use uv_sort_codes
    use uv_sort
    !----------------------------------------------------------------------
    ! TIME-BASE sort the UV table before showing it
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: chain
    logical,          intent(inout) :: error
    !
    ! Check data presence in buffer
    if (.not.associated(duv)) then
       call map_message(seve%e,'SHOW','No UV data')
       error = .true.
       return
    endif
    !
    ! Sort buffer (TIME-BASE) when needed and create the associated SIC buffer
    if (.not.allocated(uvtb%data)) then
       call uv_tri(code_sort_tb,error)
       if (error) return
    endif
    !
    ! Plot SIC buffer
    call exec_program('@ p_uvshow_w.map w '//chain)
  end subroutine show_uv
  !
  !------------------------------------------------------------------------
  !
  subroutine buffer_copy(atype,namew,gw,error)
    use gkernel_interfaces
    use image_def
    use file_buffers
    use uv_buffers
    use uvmap_buffers
    use clean_buffers
    use primary_buffers
    !----------------------------------------------------------------------
    ! Support for LOAD and SHOW Name
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: atype
    character(len=*), intent(in)    :: namew
    type(gildas),     intent(inout) :: gw
    logical,          intent(inout) :: error
    !
    include 'gbl_memory.inc'
    !
    integer(kind=address_length) :: ipw
    integer(kind=index_length) :: dim(4)
    character(len=*), parameter :: rname='BUFFER>COPY'
    !
    ! Read header
    gw%loca%size = 0
    select case (atype)
    case ('UV')
       call gdf_copy_header(huv,gw,error)
       gw%loca%addr = locwrd(duv)
    case ('BEAM')
       call gdf_copy_header(hbeam,gw,error)
       gw%loca%addr = locwrd(dbeam)
    case ('DIRTY')
       call gdf_copy_header(dirty%head,gw,error)
       gw%loca%addr = locwrd(dirty%data)
    case ('CLEAN')
       call gdf_copy_header(clean%head,gw,error)
       gw%loca%addr = locwrd(clean%data)
    case ('MASK')
       call gdf_copy_header(mask%head,gw,error)
       gw%loca%addr = locwrd(mask%data)
    case ('RESIDUAL')
       call gdf_copy_header(resid%head,gw,error)
       gw%loca%addr = locwrd(resid%data)
    case ('CCT')
       call gdf_copy_header(cct%head,gw,error)
       gw%loca%addr = locwrd(cct%data)
    case ('SKY')
       call gdf_copy_header(sky%head,gw,error)
       gw%loca%addr = locwrd(sky%data)
    case ('FIELDS','PRIMARY')
       call create_fields(error)
       if (.not.error) then
          call gdf_copy_header(fields%head,gw,error)
          gw%loca%addr = locwrd(fields%data)
       endif
    case default
       call map_message(seve%e,rname,'Unsupported type '//atype)
       error = .true.
       return
    end select
    !
    ! Pass table address to SIC buffer
    if (gw%loca%size.eq.0) then
       call map_message(seve%e,rname,'Image not defined '//atype)
       error =.true.
       return
    endif
    call sic_delvariable(namew,.false.,error)
    ipw = gag_pointer(gw%loca%addr,memory)
    !
    if (atype.eq.'BEAM') then
       ! Squeeze degenerate dimensions for Beams
       if (gw%gil%dim(3).eq.1) then
          gw%gil%dim(3) = max(1,gw%gil%dim(4))
          gw%gil%dim(4) = 1
          gw%gil%ndim = 3
       endif
    endif
    dim(1:4) = gw%gil%dim(1:4)
    call sic_def_real_addr(namew,memory(ipw),gw%gil%ndim,dim,.true.,error)
  end subroutine buffer_copy
  !
  subroutine create_fields(error)
    use image_def, only: gdf_maxdims
    use gkernel_interfaces
    use file_buffers
    use uv_buffers
    use uvmap_buffers
    !----------------------------------------------------------------------
    ! Create the FIELDS array (transposed version of the PRIMARY array)
    !----------------------------------------------------------------------
    logical, intent(out) :: error
    !
    integer :: ier
    integer(kind=index_length) :: nfirst,nsecon,nmiddl,nelems,nlast,iblock(5)
    character(len=4) :: code
    !
    if (primbeam%head%loca%size.eq.0) then
       error = .true.
       return
    endif
    error = .false.
    !
    ! If it is allocated, it should be the right one, but play safe...
    if (allocated(fields%data)) then
       if (fields%head%loca%size.eq.primbeam%head%loca%size) return
       deallocate(fields%data)
    endif
    call gildas_null(fields%head)
    !
    code = '231'
    call gdf_transpose_header(primbeam%head,fields%head,code,error)
    ! Determine chunk sizes from code and dimensions.
    call transpose_getblock(primbeam%head%gil%dim,gdf_maxdims,code,iblock,error)
    if (error) return
    !
    ! With only one beam per frequency so far
    if (.not.allocated(fields%data)) then
       allocate(&
            fields%data(primbeam%head%gil%dim(2),primbeam%head%gil%dim(3),primbeam%head%gil%dim(1),primbeam%head%gil%dim(4)),&
            stat=ier)
       if (ier.ne.0) then
          error = .true.
          return
       endif
    endif
    !
    nelems = iblock(1)
    nfirst = iblock(2)
    nmiddl = iblock(3)
    nsecon = iblock(4)
    nlast  = iblock(5)
    call trans4all(fields%data,primbeam%data,nelems,nfirst,nmiddl,nsecon,nlast)
    !
    ! Set Type of First Axis
    fields%head%gil%inc(3) = 1.0
    fields%head%char%code(3) = 'FIELD'
  end subroutine create_fields
end module mapping_show_or_view
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
