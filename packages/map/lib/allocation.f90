subroutine map_reallocate_inte_1d(name,array,n,reallocated,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! @ private-generic map_reallocate
  ! (Re)allocation an integer*4 array to the desired size.
  ! subroutine returns .true. if array was actually reallocated.
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: name         ! Array name for feedback
  integer(kind=4),  allocatable   :: array(:)     !
  integer(kind=4),  intent(in)    :: n            ! Desired size
  logical,          intent(out)   :: reallocated  !
  logical,          intent(inout) :: error        !
  !
  integer(kind=4) :: ier
  character(len=*), parameter :: rname='MAP>REALLOCATE>INTE>1D'
  !
  reallocated = .false.
  if (allocated(array)) then
    if (ubound(array,1).eq.n) then
      ! Already allocated to the rigth size
      return
    else
      deallocate(array)
    endif
  endif
  !
  allocate(array(n),stat=ier)
  if (failed_allocate(rname,name,ier,error)) return
  !
  reallocated = .true.
end subroutine map_reallocate_inte_1d
!
subroutine map_reallocate_real_1d(name,array,n,reallocated,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! @ private-generic map_reallocate
  ! (Re)allocation a real*4 array to the desired size.
  ! subroutine returns .true. if array was actually reallocated.
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: name         ! Array name for feedback
  real(kind=4),     allocatable   :: array(:)     !
  integer(kind=4),  intent(in)    :: n            ! Desired size
  logical,          intent(out)   :: reallocated  !
  logical,          intent(inout) :: error        !
  !
  integer(kind=4) :: ier
  character(len=*), parameter :: rname='MAP>REALLOCATE>REAL>1D'
  !
  reallocated = .false.
  if (allocated(array)) then
    if (ubound(array,1).eq.n) then
      ! Already allocated to the rigth size
      return
    else
      deallocate(array)
    endif
  endif
  !
  allocate(array(n),stat=ier)
  if (failed_allocate(rname,name,ier,error)) return
  !
  reallocated = .true.
end subroutine map_reallocate_real_1d
!
subroutine map_reallocate_real_2d(name,array,n1,n2,reallocated,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! @ private-generic map_reallocate
  ! (Re)allocation a real*4 array to the desired size.
  ! subroutine returns .true. if array was actually reallocated.
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: name         ! Array name for feedback
  real(kind=4),     allocatable   :: array(:,:)   !
  integer(kind=4),  intent(in)    :: n1,n2        ! Desired size
  logical,          intent(out)   :: reallocated  !
  logical,          intent(inout) :: error        !
  !
  integer(kind=4) :: ier
  character(len=*), parameter :: rname='MAP>REALLOCATE>REAL>2D'
  !
  reallocated = .false.
  if (allocated(array)) then
    if (ubound(array,1).eq.n1 .and. &
        ubound(array,2).eq.n2) then
      ! Already allocated to the rigth size
      return
    else
      deallocate(array)
    endif
  endif
  !
  allocate(array(n1,n2),stat=ier)
  if (failed_allocate(rname,name,ier,error)) return
  !
  reallocated = .true.
end subroutine map_reallocate_real_2d
!
subroutine map_reallocate_real_3d(name,array,n1,n2,n3,reallocated,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! @ private-generic map_reallocate
  ! (Re)allocation a real*4 array to the desired size.
  ! subroutine returns .true. if array was actually reallocated.
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: name         ! Array name for feedback
  real(kind=4),     allocatable   :: array(:,:,:) !
  integer(kind=4),  intent(in)    :: n1,n2,n3     ! Desired size
  logical,          intent(out)   :: reallocated  !
  logical,          intent(inout) :: error        !
  !
  integer(kind=4) :: ier
  character(len=*), parameter :: rname='MAP>REALLOCATE>REAL>3D'
  !
  reallocated = .false.
  if (allocated(array)) then
    if (ubound(array,1).eq.n1 .and. &
        ubound(array,2).eq.n2 .and. &
        ubound(array,3).eq.n3) then
      ! Already allocated to the rigth size
      return
    else
      deallocate(array)
    endif
  endif
  !
  allocate(array(n1,n2,n3),stat=ier)
  if (failed_allocate(rname,name,ier,error)) return
  !
  reallocated = .true.
end subroutine map_reallocate_real_3d
!
subroutine map_reallocate_logi_2d(name,array,n1,n2,reallocated,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! @ private-generic map_reallocate
  ! (Re)allocation a real*4 array to the desired size.
  ! subroutine returns .true. if array was actually reallocated.
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: name         ! Array name for feedback
  logical,          allocatable   :: array(:,:)   !
  integer(kind=4),  intent(in)    :: n1,n2        ! Desired size
  logical,          intent(out)   :: reallocated  !
  logical,          intent(inout) :: error        !
  !
  integer(kind=4) :: ier
  character(len=*), parameter :: rname='MAP>REALLOCATE>LOGI>2D'
  !
  reallocated = .false.
  if (allocated(array)) then
    if (ubound(array,1).eq.n1 .and. &
        ubound(array,2).eq.n2) then
      ! Already allocated to the rigth size
      return
    else
      deallocate(array)
    endif
  endif
  !
  allocate(array(n1,n2),stat=ier)
  if (failed_allocate(rname,name,ier,error)) return
  !
  reallocated = .true.
end subroutine map_reallocate_logi_2d
