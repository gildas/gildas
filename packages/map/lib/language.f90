!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module clean_language
  use gbl_message
  !
  public :: clean_language_register
  private
  !
contains
  !
  subroutine clean_language_register(error)
    use gkernel_interfaces
!    use map_buffers
    !----------------------------------------------------------------------
    ! Initialize Mapping Language
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    integer :: mclean
    parameter (mclean=99+1)  ! MFS 
    character(len=16) :: vocab(mclean)
    data vocab /' DUMP',  &
         ' CLEAN','/FLUX','/PLOT','/QUERY','/NITER','/ARES',&
         ' CLARK','/FLUX','/PLOT','/QUERY',&
         ' FIT',' FLUX_SCALE',&
         ' HOGBOM','/FLUX',&
         ' LOAD',&
         ' MAP_COMPRESS',' MAP_RESAMPLE',' MAP_INTEGRATE',&
         ' MOSAIC',&
         ' MRC','/FLUX','/PLOT','/QUERY',&
         ' MFS',&
         ' MULTI','/FLUX',&
         ' MX','/FLUX','/PLOT','/QUERY',&
         ' PRIMARY','/TRUNCATE',&
         ' READ','/FREQUENCY','/PLANE','/RANGE',  '/NOTRAIL','/TELESCOPE',&
         ' SHOW',&
         ' SDI','/FLUX','/PLOT','/QUERY',&
         ' SPECIFY',&
         ' STATISTICS','/WHOLE',&
         ' SUPPORT','/CURSOR','/MASK','/PLOT','/RESET','/THRESHOLD','/VARIABLE',&
         ' UV_BASELINE','/CHANNELS','/FREQUENCY','/RANGE','/WIDTH',&
         ' UV_CHECK',' UV_COMPRESS',' UV_CONTINUUM','/INDEX',&
         ' UV_EXTRACT','/RANGE','/FREQUENCY','/WIDTH',&
         ' UV_FLAG','/ANTENNA','/RESET',&
         ' UV_FILTER','/CHANNELS','/FREQUENCY','/RANGE','/WIDTH','/ZERO',&
         ' UV_MAP','/TRUNCATE',  '/FIELDS',&
         ' UV_RESAMPLE',  ' UV_RESIDUAL',' UV_RESTORE','/COPY','/SLOW',' UV_REWEIGHT', ' UV_RMS',&
         ' UV_SHIFT',' UV_SORT',' UV_STACK',  ' UV_STAT',' UV_STOKES',&
         ' UV_TIME','/WEIGHT',' UV_TRUNCATE',&
         ' VIEW',&
         ' WRITE','/APPEND ','/RANGE','/REPLACE','/TRIM'/
    !
    character(len=40) :: version
    version = '21-Oct-2023 J.Pety'
    !
    !***JP: I don't understand!
    !    call sic_begin('CLEAN','GAG_HELP_CLEAN',mclean,vocab,version,clean_language_run,map_buffer%error)
    call sic_begin('CLEAN','GAG_HELP_CLEAN',mclean,vocab,version,clean_language_run,gr_error)
  end subroutine clean_language_register
  !
  subroutine clean_language_run(line,comm,error)
    use mapping_dump
    use mapping_flux_scale
    use mapping_load
    use mapping_mosaic
    use mapping_primary
    use mapping_read
    use mapping_show_or_view
    use mapping_specify
    use mapping_statistics
    use mapping_support
    use mapping_write
    !
    use uv_baseline
    use uv_check
    use uv_continuum
    use uv_extract
    use uv_filter
    use uv_flag
    use uv_resample
    use uv_reweight
    use uv_rms
    use uv_sort
    use uv_stack
    use uv_statistics
    use uv_stokes
    use uv_time
    use uv_truncate
    !
    use imaging_fit
    use uv_map
    use uv_residual
    use uv_restore
    use uv_shift
    !
    use clean_clark
    use clean_clean
    use clean_hogbom
    use clean_mfs
    use clean_mrc
    use clean_multi
    use clean_mx
    use clean_sdi
    !----------------------------------------------------------------------
    ! Call appropriate subroutine according to COMM
    !----------------------------------------------------------------------
    character(len=*), intent(inout) :: line
    character(len=*), intent(in)    :: comm
    logical,          intent(out)   :: error
    !
    integer, save :: icall=0
    integer :: i
    character(len=*), parameter :: rname='CLEAN>LANGUAGE>RUN'
    !
    if (icall.ne.0) then
       print *,'Rentrant call to '//trim(rname)//' ',comm
       read(5,*) i
    endif
    icall = icall+1
    !
    call map_message(seve%c,rname,line)
    !
    ! Analyze command
    select case (comm)
       !
       ! Administration commands by alphabetical order
       !
    case ('DUMP')
       call dump_comm(line,error)
    case ('FLUX_SCALE')
       call flux_scale_comm(line,error)
    case ('LOAD')
       call load_comm(line,error)
    case ('MAP_RESAMPLE','MAP_COMPRESS','MAP_INTEGRATE') 
       call map_resample_comm(line,comm,error)
    case ('MOSAIC')
       call mosaic_comm(line,error)
    case ('PRIMARY') !***JP: unclear position
       call primary_comm(line,error)
    case ('READ')
       call read_comm(line,error)
    case ('SPECIFY')
       call specify_comm(line,error)
    case ('SHOW','VIEW')
       call show_or_view_comm(comm,line,error)
    case ('STATISTICS')
       call statistics_comm(line,error)
    case ('SUPPORT')
       call support_comm(line,error)
    case ('WRITE')
       call write_comm(line,error)
       !
       ! Commands working on UV tables by alphabetical order
       !
    case ('UV_BASELINE')
       call uv_baseline_comm(line,error)
    case ('UV_CHECK')
       call uv_check_comm(line,error)
    case ('UV_COMPRESS')
       call uv_resample_comm(line,comm,error)
    case ('UV_CONTINUUM')
       call uv_continuum_comm(line,error)
    case ('UV_EXTRACT')
       call uv_extract_comm(line,error)
    case ('UV_FILTER')
       call uv_filter_comm(line,error)
    case ('UV_FLAG')
       call uv_flag_comm(line,error)
    case ('UV_RESAMPLE')
       call uv_resample_comm(line,comm,error)
    case ('UV_REWEIGHT')
       call uv_reweight_comm(line,error)
    case ('UV_RMS')
       call uv_rms_comm(line,error)
    case ('UV_SHIFT')
       call uv_shift_comm(line,error)
    case ('UV_SORT')
       call uv_sort_comm(line,error)
    case ('UV_STACK')
       call uv_stack_comm(line,error)
    case ('UV_STAT')
       call uv_statistics_comm(line,error)
    case ('UV_STOKES')
       call uv_stokes_comm(line,error)       
    case ('UV_TIME')
       call uv_time_comm(line,error)
    case ('UV_TRUNCATE')
       call uv_truncate_comm(line,error)
       !
       ! Imaging commands by alphabetical order
       !
    case ('FIT')
       call fit_comm(line,error)
    case ('UV_MAP')
       call uv_map_comm(line,error)
    case ('UV_RESIDUAL')
       call uv_residual_comm(line,error)
    case ('UV_RESTORE')
       call uv_restore_comm(line,error)
       !
       ! Deconvolution commands by alphabetical order
       !
    case ('CLARK')
       call clark_comm(line,error)
    case ('CLEAN')
       call clean_comm(line,error)
    case ('HOGBOM')
       call hogbom_comm(line,error)
    case ('MFS')
       call mfs_comm(line,error)
    case ('MRC')
       call mrc_comm(line,error)
    case ('MULTI')
       call multi_comm(line,error)
    case ('MX')
       call old_mx_comm(line,error)
    case ('SDI')
       call sdi_comm(line,error)
    case default
       call map_message(seve%i,rname,comm//' not yet implemented')
    end select
    icall = icall-1
  end subroutine clean_language_run
end module clean_language
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
