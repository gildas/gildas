!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module uvmap_buffers
  use image_def
  use mapping_types
  use uvmap_types
  !------------------------------------------------------------------------
  ! Imaging buffers
  !------------------------------------------------------------------------
  !
  public :: uvmap_buffer
  public :: uvmap_prog,uvmap_default,uvmap_old,uvmap_saved
  public :: selected_fieldsize,selected_fields
  !
  public :: d_min,d_max
  public :: dirty,primbeam,fields
  public :: hbeam
  public :: dbeam
  public :: weight,g_weight,g_v,do_weig
  private
  !
  ! User part
  type(uvmap_par), save :: uvmap_prog,uvmap_default,uvmap_old,uvmap_saved
  !
  ! List of selected fields in UV_MAP (used for UV_RESTORE and
  ! also for SHOW FIELDS)
  integer :: selected_fieldsize=0
  integer, allocatable :: selected_fields(:)
  !
  type uvmap_buffer_user_t
   contains
     procedure, public :: init   => uvmap_buffer_user_init
     procedure, public :: sicdef => uvmap_buffer_user_sicdef
  end type uvmap_buffer_user_t
  type(uvmap_buffer_user_t) :: uvmap_buffer
  !
  ! Prog part
  real :: d_min,d_max ! min max of dirty image
  !
  type(gildas), target, save :: hbeam
  type(mapping_3d_t), target, save :: dirty    ! Dirty cube
  type(mapping_4d_t), target, save :: primbeam ! Primary beam
  type(mapping_4d_t), target, save :: fields   ! Mosaic fields
  !  
  real, allocatable, target, save :: dbeam(:,:,:,:)   ! 4-D
  real, allocatable, target, save :: weight(:,:,:)    ! Mosaic weights 3-D
  !
  real, allocatable, save :: g_weight(:)  ! UV weights
  real, allocatable, save :: g_v(:)       ! V values
  logical, save :: do_weig                ! Optimization of weight computing
  !
contains
  !
  subroutine uvmap_buffer_user_init(user,error)
    use gkernel_interfaces
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(uvmap_buffer_user_t), intent(out)   :: user
    logical,                    intent(inout) :: error
    !
    call gildas_null(hbeam)
    call gildas_null(dirty%head)
    call gildas_null(primbeam%head)
    call gildas_null(fields%head)
  end subroutine uvmap_buffer_user_init
  !
  subroutine uvmap_buffer_user_sicdef(user,error)
    use gkernel_interfaces
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(uvmap_buffer_user_t), intent(inout) :: user
    logical,                    intent(inout) :: error
    !
    integer(kind=index_length) :: dim(4) = 0
    !
    ! Initialize
    call uvmap_default%init(error)
    if (error) return
    call uvmap_default%sicdef(error)
    if (error) return
    uvmap_old = uvmap_default
    uvmap_saved = uvmap_old
    !
    ! Dirty Min/Max
    call sic_def_real('D_MAX',d_max,0,0,.false.,error)
    if (error) return
    call sic_def_real('D_MIN',d_min,0,0,.false.,error)
    if (error) return
    !
    ! Old names for compatibility
    !   define logical uv_shift /global
    !   define integer convolution /global
    !   define double uv_cell[2] uv_taper[3] taper_expo /global
    call sic_def_inte('CONVOLUTION',uvmap_old%ctype,0,0,.false.,error)
    if (error) return
    call sic_def_logi('UV_SHIFT',uvmap_old%shift,.false.,error)
    if (error) return
    call sic_def_char('WEIGHT_MODE',uvmap_old%mode,.false.,error)
    if (error) return
    dim = [2,0,0,0]
    call sic_def_real('UV_CELL',uvmap_old%uniform(1),1,dim,.false.,error)
    if (error) return
    dim = [3,0,0,0]
    call sic_def_real('UV_TAPER',uvmap_old%taper,1,dim,.false.,error)
    if (error) return
    call sic_def_real('TAPER_EXPO',uvmap_old%taper(4),0,0,.false.,error)    
  end subroutine uvmap_buffer_user_sicdef
end module uvmap_buffers
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
