!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module mapping_specify
  use gbl_message
  !
  public :: specify_comm
  private
  !
contains
  !
  subroutine specify_comm(line,error)
    use gkernel_interfaces
    use phys_const
    use file_buffers
    use uv_buffers
    use uvmap_buffers
    use clean_buffers
    !-----------------------------------------------------
    ! SPECIFY KeyWord Value
    !-----------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    real(8) :: freq,freq_a,freq_b,value(3)
    real(4) :: velo
    integer :: i,iterm,narg,nc
    integer, parameter :: nterm=3
    character(len=12) :: term(nterm),chain,key
    data term/'FREQUENCY','TELESCOPE','VELOCITY'/
    character(len=*), parameter :: rname='SPECIFY'
    !
    if (huv%loca%size.ne.0) then
       freq = huv%gil%freq
       velo = huv%gil%voff
    endif
    if (dirty%head%loca%size.ne.0) then
       freq = dirty%head%gil%freq
       velo = dirty%head%gil%voff
    endif
    if (clean%head%loca%size.ne.0) then
       freq = clean%head%gil%freq
       velo = clean%head%gil%voff
    endif
    !
    narg = sic_narg(0)
    do i = 1,narg,2
       call sic_ke(line,0,i,chain,nc,.true.,error)
       if (error) return
       call sic_ambigs(rname,chain,key,iterm,term,nterm,error)
       if (error) return
       !
       select case (key)
       case ('FREQUENCY')
          ! Modify velocity scale according to new rest frequency [MHz]
          call sic_r8(line,0,i+1,freq,.true.,error)
       case ('VELOCITY')
          ! Modify Frequency scale according to new velocity (km/s)
          call sic_r4(line,0,i+1,velo,.true.,error)
       case ('TELESCOPE')
          call sic_ke(line,0,i+1,chain,nc,.true.,error)
          if (error) return
          !
          ! Update current UV and Initial UV data set
          if (huv%loca%size.ne.0) then
             if (huv%gil%nteles.ge.1) then
                if (huv%gil%teles(1)%ctele .ne. chain(1:nc)) then
                   call map_message(seve%i,rname,'Telescope ' &
                        & //trim(huv%gil%teles(1)%ctele) &
                        & //' in UV data overwritten by /TELESCOPE '//chain(1:nc)) 
                   ! Undefine the telescope so that all characteristics
                   ! are re-defined by gdf_addteles after
                   huv%gil%teles(1)%ctele = ' '
                   uvi%head%gil%teles(1)%ctele = ' '
                endif
             endif
             call gdf_addteles(huv,'TELE',chain(1:nc),value,error)
             call gdf_addteles(uvi%head,'TELE',chain(1:nc),value,error)
          endif
       end select
       if (error) return
    enddo
    !
    ! Now which ones ...
    if (huv%loca%size.ne.0) then
       freq_b = gdf_uv_frequency(huv,1.d0)
       call gdf_modify(huv,velo,freq,error=error)
       freq_a = gdf_uv_frequency(huv,1.d0)
       if (abs(freq_a-freq_b).gt.10.0) then
          print *,'Frequency Before ',freq_b
          print *,'Frequency After ',freq_a
          print *,'Frequency Difference ',freq_a-freq_b
       endif
    endif
    if (dirty%head%loca%size.ne.0) then
       call gdf_modify(dirty%head,velo,freq,error=error)
    endif
    if (clean%head%loca%size.ne.0) then
       call gdf_modify(clean%head,velo,freq,error=error)
    endif
  end subroutine specify_comm
end module mapping_specify
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
