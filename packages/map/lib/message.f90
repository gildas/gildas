!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage MAPPING messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module map_message_private
  use gpack_def
  !
  ! Identifier used for message identification
  integer :: map_message_id = gpack_global_id  ! Default value for startup message
  !
end module map_message_private
!
subroutine map_message_set_id(id)
  use map_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer, intent(in) :: id
  ! Local
  character(len=message_length) :: mess
  !
  map_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',map_message_id
  call map_message(seve%d,'map_message_set_id',mess)
  !
end subroutine map_message_set_id
!
subroutine map_message(mkind,procname,message)
  use gkernel_interfaces
  use map_message_private
  !---------------------------------------------------------------------
  ! @ public
  !
  ! MAPPING
  !   Messaging facility for the current library. Calls the low-level
  !   (internal) messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  !
  call gmessage_write(map_message_id,mkind,procname,message)
  !
end subroutine map_message
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
