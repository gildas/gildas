!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module clean_tool
  use gbl_message
  !
  public :: sub_clean,check_area,check_box,cct_prepare,clean_data
  !
contains
  !
  subroutine sub_clean(line,error)
    use phys_const
    use image_def, only: def_extr_words
    use gkernel_interfaces
    use clean_support_tool, only: check_mask
    use clean_flux_tool, only: init_flux,close_flux,plot_mrc,major_plot,no_major_plot,next_flux,no_next_flux
    use clean_cycle_tool
    use clean_mrc_tool
    use file_buffers
    use uvmap_buffers
    use clean_buffers
    use primary_buffers
    !$ use omp_lib
    !----------------------------------------------------------------------
    ! Implementation of all standard CLEAN deconvolution algorithms
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    logical :: limits
    logical :: clean_extrema=.true.
    integer :: ipen,nx,ny,np
    real :: ylimn,ylimp
    integer :: mthread, plot_case, nc
    character(len=*), parameter :: rname='SUB_CLEAN'
    !
    call sic_get_logi('CLEAN_EXTREMA',clean_extrema,error)
    call sic_get_inte('FIRST',clean_prog%first,error)
    call sic_get_inte('LAST',clean_prog%last,error)
    call sic_i4(line,0,1,clean_prog%first,.false.,error)
    call sic_i4(line,0,2,clean_prog%last,.false.,error)
    clean_prog%thresh = 0.30
    call sic_get_real('THRESHOLD',clean_prog%thresh,error)
    if (clean_prog%first.eq.0) clean_prog%first = 1
    if (clean_prog%last.eq.0) clean_prog%last = dirty%head%gil%dim(3)
    clean_prog%first = max(1,min(clean_prog%first,dirty%head%gil%dim(3)))
    clean_prog%last = max(clean_prog%first,min(clean_prog%last,dirty%head%gil%dim(3)))
    !
    call check_area(clean_prog,dirty%head,.false.)
    call check_mask(clean_prog,dirty%head)
    clean_user%do_mask = clean_prog%do_mask
    !
    nc = dirty%head%gil%dim(3)
    call cct_prepare(line,nc,clean_prog,rname,error)
    !
    ! Set the pointers for use in subroutines
    dirty%head%r3d => dirty%data
    clean%head%r3d => clean%data
    hbeam%r4d  => dbeam
    resid%head%r3d => resid%data
    primbeam%head%r4d  => primbeam%data
    mask%head%r3d => mask%data
    !
    limits = sic_present(1,0)
    if (limits) then
       call sic_r4 (line,1,1,ylimn,.true.,error)
       if (error) return
       call sic_r4 (line,1,2,ylimp,.true.,error)
       if (error) return
    else
       ylimp = sqrt (float(clean_prog%m_iter+200) *   &
            log(float(clean_prog%m_iter+1)) ) * clean_prog%gain
       if (-dirty%head%gil%rmin.gt.1.3*dirty%head%gil%rmax) then
          ! Probably negative
          ylimn = ylimp*dirty%head%gil%rmin
          ylimp = 0.0
       elseif (-1.3*dirty%head%gil%rmin.gt.dirty%head%gil%rmax) then
          ! Probably positive
          ylimn = 0.0
          ylimp = ylimp*dirty%head%gil%rmax
       else
          ! Don't know...,
          ylimn = ylimp*dirty%head%gil%rmin
          ylimp = ylimp*dirty%head%gil%rmax
       endif
    endif
    ! Usefull variables
    nx = dirty%head%gil%dim(1)
    ny = dirty%head%gil%dim(2)
    np = max(1,primbeam%head%gil%dim(1))
    !
    ! Beam patch according to Method
    if (clean_prog%method.eq.'CLARK'.or.   &
         &    clean_prog%method.eq.'MRC') then
       if (clean_user%patch(1).ne.0) then
          clean_prog%patch(1) = min(clean_user%patch(1),nx)
       else
          clean_prog%patch(1) = min(nx,max(32,nx/4))
       endif
       if (clean_user%patch(2).ne.0) then
          clean_prog%patch(2) = min(clean_user%patch(2),ny)
       else
          clean_prog%patch(2) = min(ny,max(32,ny/4))
       endif
    elseif (clean_prog%method.eq.'SDI') then
       if (clean_user%patch(1).ne.0) then
          clean_prog%patch(1) = min(clean_user%patch(1),nx/4)
       else
          clean_prog%patch(1) = min(nx/2,max(16,nx/8))
       endif
       if (clean_user%patch(2).ne.0) then
          clean_prog%patch(2) = min(clean_user%patch(2),ny/4)
       else
          clean_prog%patch(2) = min(ny,max(16,ny/8))
       endif
    endif
    !
    ! Disable Plotting in Parallel mode
    mthread = 1
    !$  mthread = omp_get_max_threads()
    if (mthread.gt.1 .and. clean_prog%last.ne.clean_prog%first) then
       if (clean_prog%pcycle .or. clean_prog%pflux) then
          call map_message(seve%w,rname,'Ignoring /PLOT and /FLUX option in Parallel mode')
          clean_prog%pcycle = .false.
          clean_prog%pflux = .false.
       endif
       plot_case = 1
    else
       plot_case = 0
    endif
    !
    if (clean_prog%pflux) call init_flux(clean_prog,dirty%head,ylimn,ylimp,ipen)
    !
    ! Delete CCT variable
    call sic_delvariable('CCT',.false.,error)
    !
    ! Avoid array constructors (too touchy about Kind)
    clean_prog%bzone(1:2) = 1
    clean_prog%bzone(3:4) = dirty%head%gil%dim(1:2)
    if (clean_prog%method.eq.'MRC') then
       call sub_mrc('MRC',clean_prog,dirty%head,resid%head,clean%head,hbeam,primbeam%head,d_mask, &
            &   error, plot_mrc)
    else
       if (plot_case.eq.1) then
          call sub_major(clean_prog,dirty%head,resid%head,clean%head,hbeam,primbeam%head,mask%head, &
               &   cct%data,d_mask,d_list,error, no_major_plot, no_next_flux)
       else
          call sub_major(clean_prog,dirty%head,resid%head,clean%head,hbeam,primbeam%head,mask%head, &
               &   cct%data,d_mask,d_list,error, major_plot, next_flux)
       endif
    endif
    !
    if (clean_prog%pflux .and. clean_prog%method.ne.'MRC') then
       call close_flux(ipen,error)
    else
       call gr_execl('CHANGE DIRECTORY <GREG')
    endif
    !
    ! Reset extrema
    if (clean_extrema) then
       call map_message(seve%i,rname,'Computing extrema')
       resid%head%loca%addr = locwrd(resid%data)
       call gdf_get_extrema (resid%head,error)
       resid%head%gil%extr_words = def_extr_words
       !
       clean%head%loca%addr = locwrd(clean%data)
       call gdf_get_extrema (clean%head,error)
       clean%head%gil%extr_words = def_extr_words
    else
       resid%head%gil%extr_words = 0
       clean%head%gil%extr_words = 0
    endif
    !
    ! Specify clean beam parameters
    clean%head%gil%reso_words = 3
    clean%head%gil%majo = clean_prog%major
    clean%head%gil%mino = clean_prog%minor
    clean%head%gil%posa = pi*clean_prog%angle/180.0
    save_data(code_save_clean) = .true.
    save_data(code_save_resid) = .true.
    if (clean_prog%method.ne.'MRC') save_data(code_save_cct) = .true.
    !
    ! Save general parameters...
    clean_user%ibeam = clean_prog%ibeam
    clean_user%nlist = clean_prog%nlist
    !
    sky%head%loca%size = 0 ! Reset the Sky (primary beam corrected) image
    call sic_delvariable ('SKY',.false.,error)
    !
    call sic_mapgildas ('CCT',cct%head,error,cct%data)
  end subroutine sub_clean
  !
  subroutine check_area(method,head,quiet)
    use image_def
    use clean_types
    !-----------------------------------------------
    ! Check search area for Cleaning
    !-----------------------------------------------
    type (gildas),    intent(in) :: head
    type (clean_par), intent(inout) :: method
    logical,          intent(in) :: quiet
    !
    integer :: max_iter0,nx,ny
    real :: ares,hmax
    character(len=message_length) :: chain
    character(len=*), parameter :: rname='CHECK>AREA'
    !
    ! BLC & TRC
    nx = head%gil%dim(1)
    ny = head%gil%dim(2)
    call check_box(nx,ny,method%blc,method%trc)
    !
    ! Support for Clean
    method%box = (/method%blc(1),method%blc(2),&
         method%trc(1),method%trc(2)/)
    !
    hmax = max(abs(head%gil%rmax),abs(head%gil%rmin))
    ares = method%ares
    if (ares.eq.0) then
       ares = method%fres*hmax
    endif
    if (ares.eq.0) then
       ares = head%gil%noise
    endif
    !
    if (.not.quiet) then
       write(chain,'(A,1pg11.3)') 'Cleaning down to ',ares
       call map_message(seve%i,rname,chain)
    endif
    !
    if (hmax.lt.ares) then
       max_iter0 = 0
       if (.not.quiet) then
          write(chain,'(A,1pg11.3,A)') 'Peak flux ',hmax,' is smaller, no Cleaning needed'
          call map_message(seve%w,rname,chain)
       endif
    else
       max_iter0 = log(ares/hmax)/log(1.0-method%gain)*  &
            &(method%trc(1)-method%blc(1)+1)*(method%trc(2)-method%blc(2)+1)/50.0
       if (max_iter0.lt.0) then
          print *,'Unable to guess Iteration number - set to 100 '
          print *,'Ares ',ares,'  Hmax',hmax,'  Gain ',method%gain
          max_iter0 = 100
       endif
       !
       if (.not.quiet) then
          if (ares.lt.head%gil%noise) then
             write(chain,'(A,1pg11.3)') 'Expected noise is larger... ',head%gil%noise
             call map_message(seve%w,rname,chain)
          endif
          write(chain,'(A,i12,a)') 'May need ',max_iter0,' iterations'
          call map_message(seve%i,rname,chain)
       endif
    endif
    !
    if (method%m_iter.eq.0) then
       method%m_iter = max_iter0
       method%ares = ares
    endif
  end subroutine check_area
  !
  subroutine check_box(nx,ny,blc,trc)
    !----------------------------------------------
    ! Define default search box corners
    !-----------------------------------------------
    integer, intent(in)  :: nx,ny
    integer, intent(out) :: blc(2)
    integer, intent(out) :: trc(2)
    !
    ! Check inner quarter if not specified
    if (blc(1).eq.0) then
       blc(1) = nx/4+1
    else
       blc(1) = max(blc(1),1)
    endif
    if (blc(2).eq.0) then
       blc(2) = ny/4+1
    else
       blc(2) = max(blc(2),1)
    endif
    if (trc(1).eq.0) then
       trc(1) = 3*nx/4
    else
       trc(1) = min(trc(1),nx)
    endif
    if (trc(2).eq.0) then
       trc(2) = 3*ny/4
    else
       trc(2) = min(trc(2),ny)
    endif
  end subroutine check_box
  ! 
  subroutine cct_prepare(line,nc,a_method,rname,error)
    use gkernel_interfaces
    use mapping_list_tool
    use clean_types
    use file_buffers
    use uvmap_buffers
    use clean_buffers
    !---------------------------------------------------
    ! Prepare the Clean Component Table
    !---------------------------------------------------
    character(len=*), intent(in)    :: line
    integer,          intent(in)    :: nc
    type(clean_par),  intent(in)    :: a_method
    character(len=*), intent(in)    :: rname
    logical,          intent(inout) :: error
    !
    integer(kind=4) :: ier,x_iter
    integer, parameter :: opt_iter=4
    integer, parameter :: opt_ares=5
    !
    error = .false.
    x_iter = a_method%m_iter
    !
    ! Use up to nc = dirty%head%gil%dim(3)
    if (sic_present(opt_iter,0)) then
       niter_listsize = dirty%head%gil%dim(3)
       print *,'Allocating per plane NITER ',niter_listsize
       if (allocated(niter_list)) deallocate(niter_list)
       allocate(niter_list(niter_listsize),stat=ier)
       call get_i4list_fromsic(rname,line,opt_iter,niter_listsize, &
            & niter_list,error)
       if (error) return
       ! 
       x_iter = maxval(niter_list)
    endif
    !
    if (sic_present(opt_ares,0)) then
       ares_listsize = dirty%head%gil%dim(3)
       print *,'Allocating per plane ARES ',ares_listsize
       if (allocated(ares_list)) deallocate(ares_list)
       allocate(ares_list(ares_listsize),stat=ier)
       call get_r4list_fromsic(rname,line,opt_ares,ares_listsize, &
            & ares_list,error)
       if (error) return
    endif
    !
    if (a_method%method.eq.'MULTI') x_iter = a_method%ninflate*x_iter
    !print *,'CCT-Prepare, Method ',method%method,m_iter,x_iter
    !print *,'CCT-Prepare, FIRST ',a_method%first,' LAST ',a_method%last
    call sic_delvariable('CCT',.false.,error)
    if (error)  error = .false.  ! Not an error if no such variable
    !
    x_iter = max(1,x_iter)
    call reallocate_cct(3,nc,x_iter,cct%data,error)
    if (error)  return
    cct%head%loca%size = 3*nc*x_iter
    cct%data(:,a_method%first:a_method%last,:) = 0.  ! Nullify the channels to be used
    !
    ! Define the image header
    call gildas_null(cct%head)
    call gdf_copy_header (dirty%head,cct%head,error)
    cct%head%gil%ndim = 3
    cct%head%char%unit = 'Jy'
    !
    cct%head%gil%dim(1) = 3
    ! Keep the same axis description
    cct%head%gil%xaxi = 1
    !
    cct%head%gil%convert(:,2) = dirty%head%gil%convert(:,3)
    cct%head%gil%convert(:,3) = dirty%head%gil%convert(:,2)
    cct%head%gil%dim(2) = dirty%head%gil%dim(3)
    cct%head%char%code(2) = dirty%head%char%code(3)
    cct%head%gil%faxi = 2
    cct%head%gil%dim(3) = x_iter
    cct%head%char%code(3) = 'COMPONENT'
    cct%head%gil%yaxi = 3
    !
    ! Initialize BLCs...
    cct%head%blc = 0
    cct%head%trc = 0
    !
  contains
    !
    subroutine reallocate_cct(n1,n2,n3,array,error)
      !-------------------------------------------------------------------
      !***JP: This one should probably be moved in the cct_types module
      !***JP: or the one that will be a list of cc
      !
      ! (re)allocate the CCT allocatable array.
      ! If only the last dimension is enlarged, the previous contents are
      ! kept. Otherwise, the data is initialized to 0.
      !-------------------------------------------------------------------
      integer(kind=4),           intent(in)    :: n1,n2,n3      ! New dimensions
      real(kind=4), allocatable, intent(inout) :: array(:,:,:)
      logical,                   intent(inout) :: error
      !
      logical :: keep
      integer(kind=4) :: ier,oldn1,oldn2,oldn3
      real(kind=4), allocatable :: tmp(:,:,:)
      character(len=message_length) :: mess
      !
      if (n1.le.0 .or. n2.le.0 .or. n3.le.0) then
         write(mess,'(A,3(I0,A))') &
              'CCT size can not be zero nor negative (got n1 x n2 x n3 = ',&
              n1,'x',n2,'x',n3,')'
         call map_message(seve%e,rname,mess)
         error = .true.
         return
      endif
      !
      keep = .false.
      if (allocated(array)) then
         oldn1 = ubound(array,1)
         oldn2 = ubound(array,2)
         oldn3 = ubound(array,3)
         if (oldn1.eq.n1 .and.  &  ! Strict equality
             oldn2.eq.n2 .and.  &  ! Strict equality
             oldn3.ge.n3) then     ! Greater or equal
            write(mess,'(a,i0,a,i0,a,i0)')  &
                 'Re-using current CCT table of size ',oldn1,' x ',oldn2,' x ',oldn3
            call map_message(seve%i,rname,mess)
            return
         endif
         !
         write(mess,'(a,i0,a,i0,a,i0,a)')  &
              'Current CCT table has size ',oldn1,' x ',oldn2,' x ',oldn3,', increasing'
         call map_message(seve%i,rname,mess)
         if (oldn1.eq.n1 .and.  &  ! Strict equality
             oldn2.eq.n2) then     ! Strict equality
            ! The 2 first dimensions are not modified, only the third is enlarged.
            ! Data will be kept.
            keep = .true.
            call move_alloc(from=array,to=tmp)
            ! 'array' is now deallocated
         else
            deallocate(array)
         endif
      endif
      !
      ! Allocation
      allocate(array(n1,n2,n3),stat=ier)
      if (failed_allocate(rname,'ARRAY',ier,error))  return
      write(mess,'(a,i0,a,i0,a,i0)')  &
           'Allocated new CCT table of size ',n1,' x ',n2,' x ',n3
      call map_message(seve%i,rname,mess)
      !
      ! Back copy
      if (keep) then
         array(:,:,1:oldn3) = tmp(:,:,1:oldn3)
         array(:,:,oldn3+1:n3) = 0.
         deallocate(tmp)
      else
         array(:,:,:) = 0.
      endif
    end subroutine reallocate_cct
  end subroutine cct_prepare
  !
  subroutine clean_data(error)
    use gkernel_interfaces
    use mapping_interfaces
    use clean_types, only: beam_unit_conversion
    use uvmap_buffers
    use clean_buffers
    !--------------------------------------------------------
    ! Prepare Clean parameters
    !--------------------------------------------------------
    logical, intent(inout) :: error
    !
    integer :: nx,ny,nc,nb,m_iter
    logical :: reallocated
    character(len=*), parameter :: rname='CLEAN'
    !
    error = .false.
    if (dirty%head%loca%size.eq.0) then
       call map_message(seve%e,rname,'No dirty image')
       error = .true.
    endif
    if (hbeam%loca%size.eq.0) then
       call map_message(seve%e,rname,'No dirty beam')
       error = .true.
    endif
    if (clean_user%mosaic) then
       if (primbeam%head%loca%size.eq.0) then
          call map_message(seve%e,rname,'No primary beam')
          error = .true.
       endif
    endif
    if (error) return
    !
    ! Create clean image if needed
    nx = dirty%head%gil%dim(1)
    ny = dirty%head%gil%dim(2)
    nc = dirty%head%gil%dim(3)
    !
    call gildas_null(clean%head)
    call gdf_copy_header(dirty%head,clean%head,error)       ! Define header in all cases...
    if (error)  return
    call map_reallocate('CLEAN',clean%data,nx,ny,nc,reallocated,error)
    if (error)  return
    if (reallocated) then
       call sic_delvariable('CLEAN',.false.,error)
       if (error)  return
       call sic_mapgildas('CLEAN',clean%head,error,clean%data)
       if (error)  return
    endif
    !
    call map_reallocate('RESIDUAL',resid%data,nx,ny,nc,reallocated,error)
    if (error)  return
    if (reallocated) then
       resid%head = dirty%head !***JP: Does this work?
       call sic_delvariable('RESIDUAL',.false.,error)
       if (error)  return
       call sic_mapgildas('RESIDUAL',resid%head,error,resid%data)
       if (error)  return
    endif
    !
    call map_reallocate('LIST',d_list,nx*ny,reallocated,error)
    if (error)  return
    call map_reallocate('MASK',d_mask,nx,ny,reallocated,error)
    if (error)  return
    if (reallocated) then
       call sic_delvariable('THEMASK',.false.,error)
       if (error)  return
       call sic_def_inte_addr('THEMASK',d_mask,2,dirty%head%gil%dim,.true.,error)
       if (error)  return
       clean_user%do_mask = .true.
    endif
    if (clean_user%do_mask) then
       clean_user%nlist = 0
    endif
    !
    ! Clean Component Table
    !
    ! Must define M_ITER before allocating space
    m_iter = clean_user%m_iter
    if (m_iter.eq.0) then
       call beam_unit_conversion(clean_user)
       call clean_user%copyto(clean_prog)
       call check_area(clean_prog,dirty%head,.true.)
       m_iter = clean_prog%m_iter
    else
       call clean_user%copyto(clean_prog)
    endif
    !
    ! Allocate weight and check beam/image compatibility
    !
    if (clean_user%mosaic) then
       !  Mosaic mode check...
       if (hbeam%gil%dim(3).ne.primbeam%head%gil%dim(1)) then
          call map_message(seve%e,rname, &
               & 'MOSAIC mode: Beam and Primary have different pointings')
          error = .true.
       endif
       !
       if (hbeam%gil%dim(4).ne.primbeam%head%gil%dim(4)) then
          call map_message(seve%e,rname, &
               & 'MOSAIC mode: Beam and Primary have different frequencies')
          error = .true.
       endif
       if (error) return
       nb = hbeam%gil%dim(4)
       !
       call map_reallocate('WEIGHT',weight,nx,ny,nb,reallocated,error)
       if (error)  return
       if (reallocated) then
          clean_user%weight => weight
       endif
       !
    elseif (hbeam%gil%dim(3).ne.1) then
       !! Currently, ordering is unclear...
       if (hbeam%gil%dim(3).ne.dirty%head%gil%dim(3)) then
          call map_message(seve%w,rname,'Different beam and image spectral resolution, '// &
               'not fully tested yet')
          !! error = .true.
          ! Single field verification
       endif
       !!
       call map_message(seve%e,rname,'More than 1 pointing, and Mosaic mode OFF')
       error = .true.
    endif
    !
    ! Frequency matching case
    if (hbeam%gil%dim(4).le.1) then
       continue
    elseif (hbeam%gil%dim(4).ne.dirty%head%gil%dim(3)) then
       call map_message(seve%w,rname,'Different beam and image spectral resolution, '// &
            'not fully tested yet')
       !! error = .true.
    endif
  end subroutine clean_data
end module clean_tool
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
