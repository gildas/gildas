!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module clean_multi
  use gbl_message
  !
  public :: multi_comm
  private
  !
contains
  !
  subroutine multi_comm(line,error)
    use gkernel_interfaces
    use mapping_interfaces
    use clean_tool, only: sub_clean,clean_data
    use clean_types, only: beam_unit_conversion
    use clean_buffers, only: clean_user,clean_prog
    !----------------------------------------------------------------------
    ! Implementation of Multi Scale Clean
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    if (clean_user%mosaic) then
       call map_message(seve%e,'MULTI','Not yet implemented for mosaic')
       error = .true.
       return
    endif
    !
    ! Data checkup
    clean_user%method = 'MULTI'
    call clean_data (error)
    if (error) return
    !
    ! Parameter Definitions
    call beam_unit_conversion(clean_user)
    call clean_user%copyto(clean_prog)
    clean_prog%pflux = sic_present(1,0)
    clean_prog%pcycle = .false.
    clean_prog%qcycle = .false.
    !
    clean_prog%gains = clean_prog%gain
    call sic_get_real('GAINS[1]',clean_prog%gains(1),error)
    call sic_get_real('GAINS[2]',clean_prog%gains(2),error)
    call sic_get_real('GAINS[3]',clean_prog%gains(3),error)
    !
    call sub_clean(line,error)
  end subroutine multi_comm
end module clean_multi
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
