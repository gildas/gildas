!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module imaging_fit
  use gbl_message
  !
  public :: fit_comm
  private
  !
contains
  !  
  subroutine fit_comm(line,error)
    use gkernel_interfaces
    use clean_beam_tool, only: beam_for_channel
    use uvmap_buffers
    use clean_buffers, only: clean_prog
    !---------------------------------------------------------------------------
    ! Support routine for command FIT [ifield]
    !---------------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    integer(kind=4) :: ifield
    character(len=message_length) :: chain
    character(len=*), parameter :: rname='FIT>COMM'
    !
    if (hbeam%loca%size.eq.0) then
       call map_message(seve%e,rname,'No Dirty Beam')
       error = .true.
       return
    endif
    clean_prog%ibeam = beam_for_channel(clean_prog%iplane,dirty%head,hbeam)
    ifield = 1
    call sic_i4(line,0,1,ifield,.false.,error)
    if (error) return
    ifield = min(hbeam%gil%dim(3),max(1,ifield))
    if (hbeam%gil%dim(3).gt.1) then
       write(chain,'(A,I4,A,I4)') 'Fitting beam # ',ifield,' /',hbeam%gil%dim(3)
       call map_message(seve%i,rname,chain)
    endif
    if (hbeam%gil%dim(4).gt.1) then
       write(chain,'(A,I4,A,I4)') 'Fitting plane # ',clean_prog%ibeam,' /',hbeam%gil%dim(4)
       call map_message(seve%i,rname,chain)
    endif
    clean_prog%major = 0.0
    clean_prog%minor = 0.0
    clean_prog%angle = 0.0
    clean_prog%thresh = 0.30
    call sic_get_real('THRESHOLD',clean_prog%thresh,error)
    !
    call clean_prog%fit_beam(hbeam,dbeam(:,:,ifield,clean_prog%ibeam),error)
    if (error) return
  end subroutine fit_comm
end module imaging_fit
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
