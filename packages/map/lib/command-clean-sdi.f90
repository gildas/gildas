!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module clean_sdi
  use gbl_message
  !
  public :: sdi_comm
  private
  !
contains
  !
  subroutine sdi_comm(line,error)
    use gkernel_interfaces
    use mapping_interfaces
    use clean_tool, only: sub_clean,clean_data
    use clean_types, only: beam_unit_conversion
    use uvmap_buffers
    use clean_buffers, only: clean_user,clean_prog
    !----------------------------------------------------------------------
    ! Implementation of Steer Dewdney Ito Clean
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    integer :: iv,na,m_iter
    character(len=8) :: name,argum,voc1(2)
    data voc1/'CLEAN','RESIDUAL'/
    !
    ! Save default number of components
    m_iter = clean_user%m_iter
    clean_user%m_iter = dirty%head%gil%dim(1) * dirty%head%gil%dim(2)
    !
    ! Data checkup: method must be defined first ?
    clean_user%method = 'SDI'
    call clean_data (error)
    if (error) return
    !
    ! Parameter Definitions
    call beam_unit_conversion(clean_user)
    call clean_user%copyto(clean_prog)
    clean_prog%pflux = .false.
    clean_prog%pcycle = sic_present(2,0)
    clean_prog%qcycle = sic_present(3,0)
    if (clean_prog%pcycle) then
       argum = 'RESIDUAL'
       call sic_ke (line,2,1,argum,na,.false.,error)
       if (error) return
       call sic_ambigs ('PLOT',argum,name,iv,voc1,2,error)
       if (error) return
       clean_prog%pclean = iv.eq.1
    else
       clean_prog%pclean = .false.
    endif
    call sub_clean(line,error)
    ! Restore default number of components
    clean_user%m_iter = m_iter
  end subroutine sdi_comm
end module clean_sdi
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
