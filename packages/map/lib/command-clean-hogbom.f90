!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module clean_hogbom
  use gbl_message
  !
  public :: hogbom_comm
  private
  !
contains
  !
  subroutine hogbom_comm(line,error)
    use gkernel_interfaces
    use mapping_interfaces
    use clean_tool, only: sub_clean,clean_data
    use clean_types, only: beam_unit_conversion
    use clean_buffers, only: clean_user,clean_prog
    !----------------------------------------------------------------------
    ! Implementation of Hogbom Clean
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    ! Data checkup
    clean_user%method = 'HOGBOM'
    call clean_data(error)
    if (error) return
    !
    ! Parameter Definitions
    call beam_unit_conversion(clean_user)
    call clean_user%copyto(clean_prog)
    clean_prog%pflux = sic_present(1,0)
    clean_prog%pcycle = .false.
    clean_prog%qcycle = .false.
    call sub_clean(line,error)
  end subroutine hogbom_comm
end module clean_hogbom
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
