module mapping_interfaces
  !---------------------------------------------------------------------
  ! MAPPING interfaces, all kinds (private or public).
  !---------------------------------------------------------------------
  !
  use mapping_interfaces_public
  use mapping_interfaces_private
  !
end module mapping_interfaces
!
module mapping_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! MAPPING dependencies public interfaces. Do not use out of the library.
  !---------------------------------------------------------------------
  !
  use gkernel_interfaces
  use astro_interfaces_public
end module mapping_dependencies_interfaces
