!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module mapping_dump
  use gbl_message
  !
  public :: dump_comm
  private
  !
contains
  !
  subroutine dump_comm(line,error)
    use gkernel_interfaces
    use uvmap_tool, only: uv_clean_sizes
    use file_buffers
    use clean_buffers
    use uv_buffers
    !----------------------------------------------------------------------
    ! Dump debug information
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    character(len=4) meth
    integer :: n,nc,ier
    integer, allocatable :: mic(:)
    !
    call sic_ke(line,0,1,meth,n,.false.,error)
    if (error) return
    if (meth.eq.'USER') then
       call clean_user%list()
    else if (meth.eq.'METH') then
       call clean_prog%list()
    else if (meth.eq.'BUFF') then
       call uv_dump_buffers('DUMP')
    else if (meth.eq.'SG') then
       !
       nc = huv%gil%nchan
       if (nc.ne.cct%head%gil%dim(2)) then
          print *,'Mismatch number of channels between HUV ',nc,' and CCT ',cct%head%gil%dim(2)
       endif
       !
       allocate(mic(nc),stat=ier)
       call uv_clean_sizes(cct%head,cct%data,mic,1,nc)
       print *,'DEBUG -- MIC ',mic
       deallocate(mic)
    else
       print *,'DEBUG  BUFFers|METHod|SG|USER'
    endif
  end subroutine dump_comm
end module mapping_dump
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
