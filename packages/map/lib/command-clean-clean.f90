!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module clean_clean
  use gbl_message
  !
  public :: clean_comm
  private
  !
contains
  !
  subroutine clean_comm(line,error)
    use gkernel_interfaces
    use clean_buffers, only: ares_listsize,niter_listsize
    use clean_clark
    use clean_hogbom
    use clean_mrc
    use clean_multi
    use clean_sdi
    !----------------------------------------------------------------------
    ! Call appropriate subroutine according to METHOD%METHOD
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    character(len=12) :: cmethod
    integer :: nm
    !
    call sic_get_char('METHOD',cmethod,nm,error)
    if (nm.eq.0) then
       error = .true.
    else
       call sic_upper(cmethod)
    endif
    if (error) return
    !
    select case (cmethod)
    case ('CLARK')
       call clark_comm(line,error)
    case ('HOGBOM')
       call hogbom_comm(line,error)
    case ('MRC')
       call mrc_comm(line,error)
    case ('MULTI')
       call multi_comm(line,error)
    case ('SDI')
       call sdi_comm(line,error)
    case default
       call map_message(seve%e,'CLEAN','Unsupported method '//cmethod)
       error = .true.
       return
    end select
    ! 
    ! Reset the per-plane stopping criterium
    ares_listsize = 0
    niter_listsize = 0
  end subroutine clean_comm
end module clean_clean
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
