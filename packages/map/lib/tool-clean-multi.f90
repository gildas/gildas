!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module clean_multi_tool
  public :: init_kernel,add_kernel
  public :: smooth_mask,smooth_kernel,smooth_masked
  private
  !
contains
  !
  subroutine init_kernel(ker,mk,nk,smooth)
    !-----------------------------------------------------------------------
    ! Initialize a smoothing Kernel by a Gaussian shape
    !-----------------------------------------------------------------------
    integer, intent(in)    :: mk         ! Kernel size
    integer, intent(in)    :: nk         ! Kernel size
    real(4), intent(inout) :: ker(mk,mk) ! Kernel, centered on (NK+1)/2
    real(4), intent(in)    :: smooth
    !
    integer :: i,j
    real :: lk,r,s2,sker
    !
    if (nk.eq.1) then
       ker(1,1) = 1.
       return
    endif
    if (smooth.eq.0) then
       ker = 1.0/(nk*nk)
       return
    endif
    lk = (nk+1.0)/2.0
    s2 = 1.0/smooth**2
    sker = 0.0
    ker = 0.0
    do j=1,nk
       do i=1,nk
          r = (i-lk)**2 + (j-lk)**2
          r = r*s2
          ker(i,j) = exp(-r)
          sker = sker+ker(i,j)
       enddo
    enddo
    sker = 1.0/sker
    ker = ker * sker
    !! print *,'Kernel ratio ',minval(ker)/maxval(ker)
  end subroutine init_kernel
  !
  subroutine smooth_mask(mask,smask,nx,ny,nk)
    !-----------------------------------------------------------------------
    ! Enlarge a mask using a kernel
    ! Only based on kernel size
    !-----------------------------------------------------------------------
    integer, intent(in)  :: nx
    integer, intent(in)  :: ny
    logical, intent(in)  :: mask(nx,ny)   ! Input array
    logical, intent(out) :: smask(nx,ny)  ! Smoothed array
    integer, intent(in)  :: nk
    !
    integer :: i,j,lk,lj,li
    !
    if (nk.eq.1) then
       smask = mask
    else
       smask = mask
       lk = (nk+1)/2
       do j=lk,ny-lk+1
          do i=lk,nx-lk+1
             if (.not.smask(i,j)) then
                do lj=1,nk
                   do li=1,nk
                      if (mask(i-li+lk,j-lj+lk)) then
                         smask(i,j) = .true.
                      endif
                   enddo
                enddo
             endif
          enddo
       enddo
    endif
  end subroutine smooth_mask
  !
  subroutine smooth_kernel(beam,sbeam,nx,ny,mk,nk,ker)
    !-----------------------------------------------------------------------
    ! Smooth an array using a kernel
    !-----------------------------------------------------------------------
    integer, intent(in)  :: nx
    integer, intent(in)  :: ny
    real,    intent(in)  :: beam(nx,ny)  ! Input array
    real,    intent(out) :: sbeam(nx,ny) ! Smoothed array
    integer, intent(in)  :: nk
    integer, intent(in)  :: mk
    real,    intent(in)  :: ker(mk,mk)   ! Smoothing kernel, centered on (nk+1)/2
    !
    integer :: i,j,lk,lj,li
    !
    if (nk.eq.1) then
       sbeam = beam
    else
       sbeam = 0.      ! Mandatory
       lk = (nk+1)/2
       do j=lk,ny-lk+1
          do i=lk,nx-lk+1
             do lj=1,nk
                do li=1,nk
                   sbeam(i,j) = sbeam(i,j)+ker(li,lj)*beam(i-li+lk,j-lj+lk)
                enddo
             enddo
          enddo
       enddo
    endif
  end subroutine smooth_kernel
  !
  subroutine smooth_masked(nx,ny,sbeam,beam,mask,mk,nk,ker)
    !-----------------------------------------------------------------------
    ! Smooth an array using a kernel, but only in a mask
    !-----------------------------------------------------------------------
    integer, intent(in)  :: nx
    integer, intent(in)  :: ny
    real,    intent(in)  :: beam(nx,ny)  ! Input array
    real,    intent(out) :: sbeam(nx,ny) ! Smoothed array
    logical, intent(in)  :: mask(nx,ny)  ! Valid area
    integer, intent(in)  :: mk
    integer, intent(in)  :: nk
    real,    intent(in)  :: ker(mk,mk)   ! Smoothing kernel, centered on (nk+1)/2
    !
    integer :: i,j,lk,lj,li,ii,jj
    !
    if (nk.eq.1) then
       sbeam = beam
    else
       sbeam = 0.0  ! Mandatory
       !
       lk = (nk+1)/2
       !$OMP PARALLEL PRIVATE(lj,jj,li,ii)
       !$OMP DO COLLAPSE(2)
       do j=lk,ny-lk+1
          do i=lk,nx-lk+1
             if (mask(i,j)) then
                do lj=1,nk
                   jj = j-lj+lk
                   do li=1,nk
                      ii = i-li+lk
                      sbeam(i,j) = sbeam(i,j)+ker(li,lj)*beam(ii,jj)
                   enddo
                enddo
             endif
          enddo
       enddo
       !$OMP END DO
       !$OMP END PARALLEL
    endif
  end subroutine smooth_masked
  !
  subroutine add_kernel(clean,nx,ny,value,kx,ky,mk,nk,ker)
    !-----------------------------------------------------------------------
    ! Spread a value using a Kernel and add it to result
    !-----------------------------------------------------------------------
    integer, intent(in)    :: nx           ! X Image size
    integer, intent(in)    :: ny           ! Y Image size
    real,    intent(inout) :: clean(nx,ny) ! Summed output array
    real,    intent(in)    :: value        ! Input value
    integer, intent(in)    :: kx           ! X Center of value
    integer, intent(in)    :: ky           ! Y Center of value
    integer, intent(in)    :: mk           ! Kernel size
    integer, intent(in)    :: nk           ! Kernel size
    real,    intent(in)    :: ker(mk,mk)   ! Smoothing kernel, centered on (nk+1)/2
    !
    integer :: i,j,lk,li,lj
    !
    if (nk.eq.1) then
       clean(kx,ky) = clean(kx,ky) + value
    else
       lk = (nk-1)/2
       do j=ky-lk,ky+lk
          lj = j-ky+lk+1
          do i=kx-lk,kx+lk
             li = i-kx+lk+1
             clean(i,j) = clean(i,j) + ker (li,lj) * value
          enddo
       enddo
    endif
  end subroutine add_kernel
end module clean_multi_tool
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
