!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module uv_rms
  use gbl_message
  !
  public :: uv_rms_comm
  private
  !
contains
  !
  subroutine uv_rms_comm(line,error)
    use gkernel_interfaces
    use image_def
    use file_buffers
    use uv_buffers
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(gildas) :: ouuvh
    real, pointer :: inuvd(:,:),ouuvd(:,:)
    integer(kind=4), parameter :: one=1
    character(len=*), parameter :: rname='UV>RMS>COMM'
    !
    ! Get information on input uv table
    if (huv%loca%size.eq.0) then
      call map_message(seve%e,rname,'No UV data loaded')
      error = .true.
      return
    endif
    !
    ! Update the header of the output uv table
    call gildas_null(ouuvh,type='UVT')
    call gdf_copy_header(huv,ouuvh,error)
    ouuvh%gil%dim(2) = one
    ouuvh%gil%nvisi  = one
    !
    ! User feedback
    call map_message(seve%i,rname,'Computing visibility RMS spectrum')
    !
    ! Prepare appropriate array
    nullify(inuvd,ouuvd)
    call uv_find_buffers(rname,int(ouuvh%gil%dim(1),kind=4),int(ouuvh%gil%dim(2),kind=4),inuvd,ouuvd,error)
    if (error) return
    !
    ! Actually rms
    call uv_rms_data(huv,inuvd,ouuvh,ouuvd,error)
    if (error) return
    !
    ! Everything went fine => copy output header in global buffer that contains input header up to now
    call gdf_copy_header(ouuvh,huv,error)
    ! Set global UV pointer to the output data and free the input data
    call uv_clean_buffers(inuvd,ouuvd,error)
    if (error) return
    ! UV data not plotted
    if (allocated(uvtb%data)) deallocate(uvtb%data)
    uv_plotted = .false.
    ! Indicate optimization and save status
    optimize(code_save_uv)%change = optimize(code_save_uv)%change + 1
    save_data(code_save_uv) = .true.
    ! Recompute weight
    do_weig = .true.
    !
    ! Redefine SIC variables 
    call sic_delvariable('UV',.false.,error)
    call sic_mapgildas('UV',huv,error,duv) 
  end subroutine uv_rms_comm
  !
  subroutine uv_rms_data(inuvh,inuvd,ouuvh,ouuvd,error)
    use gkernel_interfaces
    use gbl_message
    use image_def
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(gildas),         intent(in)    :: inuvh
    real(kind=4), target, intent(in)    :: inuvd(:,:)
    type(gildas),         intent(in)    :: ouuvh
    real(kind=4), target, intent(inout) :: ouuvd(:,:)
    logical,              intent(inout) :: error
    !
    real(kind=4), allocatable :: mean(:,:)
    integer(kind=4) :: ier
    integer(kind=4) :: ic,nc,ncol
    integer(kind=4) :: iv,nv
    integer(kind=4), parameter :: one=1
    character(len=*), parameter :: rname='UV>RMS>DATA'
    !
    call map_message(seve%t,rname,'Welcome')
    !
    ncol = inuvh%gil%dim(1)
    nv = inuvh%gil%dim(2)
    nc = inuvh%gil%nchan
    !
    if (nv.gt.1) then
       allocate(mean(ncol,one),stat=ier)
       if (failed_allocate(rname,'mean visibility',ier,error)) return
       !
       ! Sum visilibities
       mean(:,one) = 0.0
       do iv=1,nv
          do ic=1,nc
             ! Real part
             mean(5+3*ic,one) = mean(5+3*ic,one) + inuvd(5+3*ic,iv)
             ! Imaginary part
             mean(6+3*ic,one) = mean(6+3*ic,one) + inuvd(6+3*ic,iv)
             ! Weight
             mean(7+3*ic,one) = mean(7+3*ic,one) + inuvd(7+3*ic,iv)
          enddo ! ic
       enddo ! iv
       ! Normalize the result
       do ic=1,nc
          mean(5+3*ic,one) = mean(5+3*ic,one)/nv
          mean(6+3*ic,one) = mean(6+3*ic,one)/nv
       enddo ! ic
       !
       ouuvd(:,one) = 0.0
       do iv=1,nv
          do ic=1,nc
             ! Real part
             ouuvd(5+3*ic,one) = ouuvd(5+3*ic,one) + (inuvd(5+3*ic,iv)-mean(5+3*ic,one))**2
             ! Imaginary part
             ouuvd(6+3*ic,one) = ouuvd(6+3*ic,one) + (inuvd(6+3*ic,iv)-mean(6+3*ic,one))**2
             ! Weight
             ouuvd(7+3*ic,one) = ouuvd(7+3*ic,one) + (inuvd(7+3*ic,iv)-mean(7+3*ic,one))**2
          enddo ! ic
       enddo ! iv
       ! Normalize the result
       do ic=1,nc
          ouuvd(5+3*ic,one) = sqrt(ouuvd(5+3*ic,one))/(nv-1)
          ouuvd(6+3*ic,one) = sqrt(ouuvd(6+3*ic,one))/(nv-1)
          ouuvd(7+3*ic,one) = sqrt(ouuvd(7+3*ic,one))/(nv-1)
       enddo ! ic
    else
       call map_message(seve%w,rname,'Only one visibility => RMS set to 0!')
       ouuvd(:,one) = 0
    endif
  end subroutine uv_rms_data
end module uv_rms
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
