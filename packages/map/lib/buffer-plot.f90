!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module plot_buffers
  use image_def
  !------------------------------------------------------------------------
  ! Interface with user
  !------------------------------------------------------------------------
  !
  public :: plot_buffer
  public :: w_plot,b_plot
  private
  !
  type(gildas), save :: w_plot ! Header mapped on the SIC W     variable
  type(gildas), save :: b_plot ! Header mapped on the SIC BPLOT variable
  !
  type plot_buffer_user_t
   contains
     procedure, public :: init   => plot_buffer_user_init
     procedure, public :: sicdef => plot_buffer_user_sicdef
  end type plot_buffer_user_t
  type(plot_buffer_user_t) :: plot_buffer
  !
contains
  !
  subroutine plot_buffer_user_init(user,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(plot_buffer_user_t), intent(out)   :: user
    logical,                   intent(inout) :: error
    !
    ! Placeholder => Do nothing for the moment
  end subroutine plot_buffer_user_init
  !
  subroutine plot_buffer_user_sicdef(user,error)
    use gkernel_interfaces
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(plot_buffer_user_t), intent(inout) :: user
    logical,                   intent(inout) :: error
    !
    real :: wdata=0.0
    !
    ! Dummy definition of the variable for initialization purpose only
    call sic_def_real('W',wdata,0,0,.true.,error)
    if (error) return
    call sic_def_header('W',w_plot,.false.,error)
    if (error) return
  end subroutine plot_buffer_user_sicdef
end module plot_buffers
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
