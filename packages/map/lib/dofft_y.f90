!
subroutine dofft_parallel_y (np,nv,visi,jx,jy,jo   &
     &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
     &    ubias,vbias,ubuff,vbuff)
  use gbl_message
  !$  use omp_lib
  !----------------------------------------------------------------------
  ! @ private
  !
  ! GILDAS  UVMAP
  !   Compute FFT of image by gridding UV data
  !   Taper after gridding
  !   Uses symmetry
  !----------------------------------------------------------------------
  integer, intent(in) :: nv                   ! number of values
  integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
  real, intent(in) :: visi(np,nv)             ! values
  integer, intent(in) :: nc                   ! number of channels
  integer, intent(in) :: jx                   ! X coord location in VISI
  integer, intent(in) :: jy                   ! Y coord location in VISI
  integer, intent(in) :: jo                   ! first channel to map
  integer, intent(in) :: nx                   ! X map size
  integer, intent(in) :: ny                   ! Y map size
  real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
  real, intent(in) :: mapx(nx)                ! X Coordinates of grid
  real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
  real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
  real, intent(in) :: cell(2)                 ! cell size in Meters
  real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
  real, intent(in) :: we(nv)                  ! Weight array
  real, intent(in) :: vv(nv)                  ! V Values
  real, intent(in) :: ubias                   ! U gridding offset
  real, intent(in) :: vbias                   ! V gridding offset
  real, intent(in) :: ubuff(4096)             ! U gridding buffer
  real, intent(in) :: vbuff(4096)             ! V gridding buffer
  !
  real(8) :: elapsed_s, elapsed_e, elapsed
  ! Much less efficient than the Visibility order, but can be
  ! parallelized without major issue.
  !
  ! Global
  real(8), parameter :: pi=3.141592653589793d0
  ! Local
  real :: atten(nx,ny)
  integer iu,iv
  integer ifirsp,ilastp,ifirsm,ilastm,my
  integer ix,iy,ic,i,iin,iou,io
  integer kx,ky,fy
  real result,staper,vtaper,utaper,etaper
  real u,v,um,up,vm,vp,ufac,vfac
  real cx,cy,sx,sy
  !
  integer ithread,nthread,lchunk
  character(len=60) :: chain
  !
  ! Compute IO from first channel number
  io = 7+3*jo-2
  ufac = 100.d0/cell(1)
  vfac = 100.d0/cell(2)
  !
  ! Initialize
  !     MY = NY       ! Slow version
  my = ny/2+1                  ! Fast version, using symmetry
  ifirsp = 1
  ilastm = nv
  !
  ! Compute the first valid pixel
  fy = int((vv(1)-sup(2))/(mapy(2)-mapy(1))+ny/2+1)
  !
  ! Precompute the taper
  if (taper(3).ne.0.0) then
    staper = taper(3)*pi/180.0
    if (taper(1).ne.0) then
      cx = cos(staper)/taper(1)
      sy = sin(staper)/taper(1)
    else
      cx = 0.0
      sy = 0.0
    endif
    if (taper(2).ne.0) then
      cy = cos(staper)/taper(2)
      sx = sin(staper)/taper(2)
    else
      cy = 0.0
      sx = 0.0
    endif
  endif
  !
  if (taper(4).ne.0) then
    etaper = taper(4)/2.0
  else
    etaper = 1.0
  endif
  !
  atten = 0.0
  !
  do iy=fy,my
    v = mapy(iy)
    if (taper(3).ne.0) then
      vtaper = -1.0
    elseif  (taper(2).eq.0) then
      vtaper = 1.0
    else
      vtaper = abs(v/taper(2))**(etaper*2.0)
      if (vtaper.gt.64) then
        vtaper = 0
      else
        vtaper = exp(-vtaper)
      endif
    endif
    if (vtaper.ne.0) then
      do ix=1,nx
        u = mapx(ix)
        if (taper(3).ne.0) then
          ! General rotated case...
          staper = (u*cx + v*sy)**2 +   &
     &            (-u*sx + v*cy)**2
          if (etaper.ne.1) staper = staper**etaper
          if (staper.gt.64.0) then
            staper = 0.0
          else
            staper = exp(-staper)
          endif
        elseif  (taper(1).eq.0) then
          staper = vtaper
        else
          utaper = abs(u/taper(1))**(2.0*etaper)
          if (utaper.gt.64) then
            utaper = 0
          else
            utaper = exp(-utaper)
          endif
          staper = vtaper*utaper
        endif
        if (taper(1).lt.0) then
          staper = 1.0-staper
          if (staper.ne.1.0) print *,u,v,staper
        endif
        !
        atten(ix,iy) = staper
      enddo
    else
      do ix=1,nx
        atten(ix,iy) = 0.0
      enddo
    endif
  enddo
  !
  write(*,'(A,F9.2)') 'I-DOFFT, Parallel Gridding Y Mode'
  !
  !$OMP PARALLEL DEFAULT (none) &
  !$OMP   & PRIVATE(nthread,lchunk,ithread) &
  !$OMP   & SHARED(fy,my,ny,nx,nc,nv,io,jx,jy) &
  !$OMP   & PRIVATE(iy,ix,vm,vp,um,up,staper) &
  !$OMP   & SHARED(map,mapx,mapy,vv,atten,sup) &
  !$OMP   & PRIVATE(ifirsp,ilastp,ifirsm,ilastm) &
  !$OMP   & PRIVATE(iin,iou,ic,result,u,v,iu,iv) &
  !$OMP   & SHARED(ubuff,vbuff,ubias,vbias,ufac,vfac,visi,we) &
  !$OMP   & PRIVATE(elapsed_s,elapsed_e,elapsed,chain, kx,ky)
  !
  nthread = 1
  !$  nthread = omp_get_num_threads()
  !$  ithread = omp_get_thread_num() + 1
  !$  elapsed_s = omp_get_wtime()
  lchunk = (my+nthread-1)/nthread
  !
  ! IFIRSP always increases, ILASTM always decreases
  ! but with Open-MP, the ordered is not guaranteed, so
  ! reinitialize each time
  ifirsp = 1
  ilastm = nv
  ! Here I tried without, as my version is sequential, but
  ! it did not change computing time, as expected
  !
  !$OMP DO
  do iy=1,ny
    map(:,:,iy) = 0.0
  enddo
  !$OMP END DO
  !
  map = 0.0
  !$OMP DO
  do iy=fy,my
    v = mapy(iy)
    !
    ! Optimized dichotomic search, taking into account the fact that
    !  VV is an ordered array
    !
    ! IFIRSP always increases, ILASTM always decreases
    ! but with Open-MP, the ordered is not guaranteed, so
    ! reinitialize each time
    ifirsp = 1
    ilastm = nv
    !
    vm = v-sup(2)
    vp = v+sup(2)
    call findp (nv,vv,vm,ifirsp)
    ilastp = ifirsp
    call findp (nv,vv,vp,ilastp)
    ilastp = ilastp-1
    !
    ilastm = ilastm+1
    vm = -v-sup(2)
    vp = -v+sup(2)
    call findm (nv,vv,vp,ilastm)
    ifirsm = ilastm
    ilastm = ilastm-1
    call findm (nv,vv,vm,ifirsm)
    !
    ! Loop on X cells
    if (ilastp.ge.ifirsp .or. ilastm.ge.ifirsm) then
      do ix=1,nx
        u = mapx(ix)
        staper = atten(ix,iy)
        !
        ! Do while in X cell for (+U,+V)
        um = u-sup(1)
        up = u+sup(1)
        do i=ifirsp,ilastp
          if (visi(jx,i).ge.um .and. visi(jx,i).le.up) then
            iu = nint((u-visi(jx,i))*ufac+ubias)
            iv = nint((v-visi(jy,i))*vfac+vbias)
            result = ubuff(iu)*vbuff(iv)
            !
            if (result.ne.0.0) then
              if (ix.eq.1) then
                print *,'Unsufficient coverage',iy,ny
              endif
              ! Weight and taper
              result = result*staper*we(i)
              ! Channels
              iou = 1
              iin = io
              do ic=1,nc
                map (iou,ix,iy) = map (iou,ix,iy) +   &
     &                  visi(iin,i)*result
                iou = iou+1
                iin = iin+1
                map (iou,ix,iy) = map (iou,ix,iy) +   &
     &                  visi(iin,i)*result
                iou = iou+1
                iin = iin+2
              enddo
              ! Beam
              map (iou,ix,iy) = map(iou,ix,iy) + result
            endif
          endif
        enddo
        !
        ! Do while in X cell for (-U,-V)
        um = -u-sup(1)
        up = -u+sup(1)
        do i=ifirsm,ilastm
          if (visi(jx,i).ge.um .and. visi(jx,i).le.up) then
            iu = nint(-(u+visi(jx,i))*ufac+ubias)
            iv = nint(-(v+visi(jy,i))*vfac+vbias)
            result = ubuff(iu)*vbuff(iv)
            if (result.ne.0.0) then
              if (ix.eq.1) then
                print *,'Unsufficient coverage',-iy,ny
              endif
              ! Weight and taper
              result = result*staper*we(i)
              ! Channels
              iou = 1
              iin = io
              do ic=1,nc
                map (iou,ix,iy) = map (iou,ix,iy) +   &
     &                  visi(iin,i)*result
                iou = iou+1
                iin = iin+1
                map (iou,ix,iy) = map (iou,ix,iy) -   &
     &                  visi(iin,i)*result
                iou = iou+1
                iin = iin+2
              enddo
              ! Beam
              map (iou,ix,iy) = map(iou,ix,iy) + result
            endif
          endif
        enddo                  ! Visibility loop
      enddo                    ! IX Loop
    endif
  enddo
  !$OMP END DO
!  write(chain,'(A,F9.2)') 'I-DOFFT, Finished Gridding -- Elapsed ',elapsed
!  call map_message(seve%i,rname,chain)
  !
  ! Symmetry
  !$OMP DO
  do iy=my+1,ny
    ky = ny+2-iy
    do ix=2,nx
      kx = nx+2-ix
      iou = 1
      do i=1,nc
        map(iou,ix,iy) = map(iou,kx,ky)
        iou = iou+1
        map(iou,ix,iy) = -map(iou,kx,ky)
        iou = iou+1
      enddo
      map(iou,ix,iy) = map(iou,kx,ky)  ! Beam
    enddo
  enddo
  !$OMP END DO
  !$  elapsed_e = omp_get_wtime()
  elapsed = elapsed_e - elapsed_s
  write(*,'(A,F9.2)') 'I-DOFFT, Finished Gridding -- Elapsed ',elapsed
  !$OMP END PARALLEL
  !
  ! Missing row is left empty (assume proper coverage of the UV plane)
end subroutine dofft_parallel_y
!
subroutine dofft_parallel_x (np,nv,visi,jx,jy,jo   &
     &    ,nc,nx,ny,map,mapx,mapy,sup,cell,taper,we,vv,   &
     &    ubias,vbias,ubuff,vbuff)
  use gbl_message
  !$  use omp_lib
  !----------------------------------------------------------------------
  ! @ private
  !
  ! GILDAS  UVMAP
  !   Compute FFT of image by gridding UV data
  !   Taper after gridding
  !   Uses symmetry
  !----------------------------------------------------------------------
  integer, intent(in) :: nv                   ! number of values
  integer, intent(in) :: np                   ! Number of "visibilities", normally 7+3*nc
  real, intent(in) :: visi(np,nv)             ! values
  integer, intent(in) :: nc                   ! number of channels
  integer, intent(in) :: jx                   ! X coord location in VISI
  integer, intent(in) :: jy                   ! Y coord location in VISI
  integer, intent(in) :: jo                   ! first channel to map
  integer, intent(in) :: nx                   ! X map size
  integer, intent(in) :: ny                   ! Y map size
  real, intent(out) :: map(2*(nc+1),nx,ny)    ! gridded visibilities
  real, intent(in) :: mapx(nx)                ! X Coordinates of grid
  real, intent(in) :: mapy(ny)                ! Y Coordinates of grid
  real, intent(in) :: sup(2)                  ! Support of convolving function in Meters
  real, intent(in) :: cell(2)                 ! cell size in Meters
  real, intent(in) :: taper(4)                ! 1/e taper in Meters + Angle in Radians
  real, intent(in) :: we(nv)                  ! Weight array
  real, intent(in) :: vv(nv)                  ! V Values
  real, intent(in) :: ubias                   ! U gridding offset
  real, intent(in) :: vbias                   ! V gridding offset
  real, intent(in) :: ubuff(4096)             ! U gridding buffer
  real, intent(in) :: vbuff(4096)             ! V gridding buffer
  !
  real(8) :: elapsed_s, elapsed_e, elapsed
  ! Much less efficient than the Visibility order, but can be
  ! parallelized without major issue.
  !
  ! Global
  real(8), parameter :: pi=3.141592653589793d0
  ! Local
  real :: atten(nx,ny)
  integer iu,iv
  integer ifirsp,ilastp,ifirsm,ilastm,my
  integer ix,iy,ic,i,iin,iou,io
  integer kx,ky,fy
  real result,staper,vtaper,utaper,etaper
  real u,v,um,up,vm,vp,ufac,vfac
  real cx,cy,sx,sy
  !
  integer ithread,nthread,lchunk
  !
  ! Compute IO from first channel number
  io = 7+3*jo-2
  ufac = 100.d0/cell(1)
  vfac = 100.d0/cell(2)
  !
  ! Initialize
  !     MY = NY       ! Slow version
  my = ny/2+1                  ! Fast version, using symmetry
  ifirsp = 1
  ilastm = nv
  !
  ! Compute the first valid pixel
  fy = int((vv(1)-sup(2))/(mapy(2)-mapy(1))+ny/2+1)
  !
  ! Precompute the taper
  if (taper(3).ne.0.0) then
    staper = taper(3)*pi/180.0
    if (taper(1).ne.0) then
      cx = cos(staper)/taper(1)
      sy = sin(staper)/taper(1)
    else
      cx = 0.0
      sy = 0.0
    endif
    if (taper(2).ne.0) then
      cy = cos(staper)/taper(2)
      sx = sin(staper)/taper(2)
    else
      cy = 0.0
      sx = 0.0
    endif
  endif
  !
  if (taper(4).ne.0) then
    etaper = taper(4)/2.0
  else
    etaper = 1.0
  endif
  !
  atten = 0.0
  !
  do iy=fy,my
    v = mapy(iy)
    if (taper(3).ne.0) then
      vtaper = -1.0
    elseif  (taper(2).eq.0) then
      vtaper = 1.0
    else
      vtaper = abs(v/taper(2))**(etaper*2.0)
      if (vtaper.gt.64) then
        vtaper = 0
      else
        vtaper = exp(-vtaper)
      endif
    endif
    if (vtaper.ne.0) then
      do ix=1,nx
        u = mapx(ix)
        if (taper(3).ne.0) then
          ! General rotated case...
          staper = (u*cx + v*sy)**2 +   &
     &            (-u*sx + v*cy)**2
          if (etaper.ne.1) staper = staper**etaper
          if (staper.gt.64.0) then
            staper = 0.0
          else
            staper = exp(-staper)
          endif
        elseif  (taper(1).eq.0) then
          staper = vtaper
        else
          utaper = abs(u/taper(1))**(2.0*etaper)
          if (utaper.gt.64) then
            utaper = 0
          else
            utaper = exp(-utaper)
          endif
          staper = vtaper*utaper
        endif
        if (taper(1).lt.0) then
          staper = 1.0-staper
          if (staper.ne.1.0) print *,u,v,staper
        endif
        !
        atten(ix,iy) = staper
      enddo
    else
      do ix=1,nx
        atten(ix,iy) = 0.0
      enddo
    endif
  enddo
  !
  map = 0.0
  write(*,'(A,F9.2)') 'I-DOFFT, Parallel Gridding X Mode'
  !
  !$  elapsed_s = omp_get_wtime()
  !
  ! IFIRSP always increases, ILASTM always decreases
  ifirsp = 1
  ilastm = nv
  !
  do iy=fy,my
    v = mapy(iy)
    !
    ! Optimized dichotomic search, taking into account the fact that
    !  VV is an ordered array
    !
    !
    vm = v-sup(2)
    vp = v+sup(2)
    call findp (nv,vv,vm,ifirsp)
    ilastp = ifirsp
    call findp (nv,vv,vp,ilastp)
    ilastp = ilastp-1
    !
    ilastm = ilastm+1
    vm = -v-sup(2)
    vp = -v+sup(2)
    call findm (nv,vv,vp,ilastm)
    ifirsm = ilastm
    ilastm = ilastm-1
    call findm (nv,vv,vm,ifirsm)
    !
    ! Loop on X cells
    if (ilastp.ge.ifirsp .or. ilastm.ge.ifirsm) then
      !$OMP PARALLEL DEFAULT (none) &
      !$OMP   & PRIVATE(nthread,lchunk,ithread) &
      !$OMP   & SHARED(fy,my,ny,nx,nc,nv,io,jx,jy) &
      !$OMP   & SHARED(iy,vm,vp,v) &
      !$OMP   & PRIVATE(ix,um,up,staper) &
      !$OMP   & SHARED(ifirsp,ilastp,ifirsm,ilastm) &
      !$OMP   & SHARED(map,mapx,mapy,vv,atten,sup) &
      !$OMP   & PRIVATE(iin,iou,ic,result,u,iu,iv) &
      !$OMP   & SHARED(ubuff,vbuff,ubias,vbias,ufac,vfac,visi,we)
      nthread = 1
      !$  nthread = omp_get_num_threads()
      !$  ithread = omp_get_thread_num() + 1
      lchunk = (my+nthread-1)/nthread
      !
      !$OMP DO
      do ix=1,nx
        u = mapx(ix)
        staper = atten(ix,iy)
        !
        ! Do while in X cell for (+U,+V)
        um = u-sup(1)
        up = u+sup(1)
        do i=ifirsp,ilastp
          if (visi(jx,i).ge.um .and. visi(jx,i).le.up) then
            iu = nint((u-visi(jx,i))*ufac+ubias)
            iv = nint((v-visi(jy,i))*vfac+vbias)
            result = ubuff(iu)*vbuff(iv)
            !
            if (result.ne.0.0) then
              if (ix.eq.1) then
                print *,'Unsufficient coverage',iy,ny
              endif
              ! Weight and taper
              result = result*staper*we(i)
              ! Channels
              iou = 1
              iin = io
              do ic=1,nc
                map (iou,ix,iy) = map (iou,ix,iy) +   &
     &                  visi(iin,i)*result
                iou = iou+1
                iin = iin+1
                map (iou,ix,iy) = map (iou,ix,iy) +   &
     &                  visi(iin,i)*result
                iou = iou+1
                iin = iin+2
              enddo
              ! Beam
              map (iou,ix,iy) = map(iou,ix,iy) + result
            endif
          endif
        enddo
        !
        ! Do while in X cell for (-U,-V)
        um = -u-sup(1)
        up = -u+sup(1)
        do i=ifirsm,ilastm
          if (visi(jx,i).ge.um .and. visi(jx,i).le.up) then
            iu = nint(-(u+visi(jx,i))*ufac+ubias)
            iv = nint(-(v+visi(jy,i))*vfac+vbias)
            result = ubuff(iu)*vbuff(iv)
            if (result.ne.0.0) then
              if (ix.eq.1) then
                print *,'Unsufficient coverage',-iy,ny
              endif
              ! Weight and taper
              result = result*staper*we(i)
              ! Channels
              iou = 1
              iin = io
              do ic=1,nc
                map (iou,ix,iy) = map (iou,ix,iy) +   &
     &                  visi(iin,i)*result
                iou = iou+1
                iin = iin+1
                map (iou,ix,iy) = map (iou,ix,iy) -   &
     &                  visi(iin,i)*result
                iou = iou+1
                iin = iin+2
              enddo
              ! Beam
              map (iou,ix,iy) = map(iou,ix,iy) + result
            endif
          endif
        enddo                  ! Visibility loop
      enddo                    ! IX Loop
      !$OMP END DO
      !$OMP END PARALLEL
    endif
  enddo
  !
  !$  elapsed_e = omp_get_wtime()
  elapsed = elapsed_e - elapsed_s
  write(*,'(A,F9.2)') 'I-DOFFT, Finished Gridding -- Elapsed ',elapsed
!  write(chain,'(A,F9.2)') 'I-DOFFT, Finished Gridding -- Elapsed ',elapsed
!  call map_message(seve%i,rname,chain)
  !
  ! Symmetry
  do iy=my+1,ny
    ky = ny+2-iy
    do ix=2,nx
      kx = nx+2-ix
      iou = 1
      do i=1,nc
        map(iou,ix,iy) = map(iou,kx,ky)
        iou = iou+1
        map(iou,ix,iy) = -map(iou,kx,ky)
        iou = iou+1
      enddo
      map(iou,ix,iy) = map(iou,kx,ky)  ! Beam
    enddo
  enddo
  !
  ! Missing row is left empty (assume proper coverage of the UV plane)
end subroutine dofft_parallel_x
!
