!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cct_types
  ! Clean Component definition
  public :: cct_par
  public :: cct_par_minmax,clean_make
  public :: remisajour,soustraire,choice
  private
  !
  type cct_par                 
     real(kind=4) :: influx  ! Current flux
     real(kind=4) :: value   ! Component value
     integer(kind=4) :: ix   ! X pixel
     integer(kind=4) :: iy   ! Y pixel
     integer(kind=4) :: type ! Component kernel number
   contains
!     procedure, public :: minmax => cct_par_minmax !***JP list, ie, not scalar
  end type cct_par
  !
contains
  !
  subroutine cct_par_minmax(x,n,nomin,rmin,nomax,rmax)
    !----------------------------------------------------------------------
    ! Compute position and values of Min and Max of Clean Component Array X
    !----------------------------------------------------------------------
    type(cct_par), intent(in)  :: x(n)        ! Clean components 
    integer,       intent(in)  :: n           ! Size of list
    integer,       intent(out) :: nomin,nomax ! Indices of min and max
    real,          intent(out) :: rmin,rmax   ! Values  of min and max
    !
    integer :: no
    real :: val
    !
    no = 1
    rmax = x(no)%influx
    nomax = no
    rmin = x(no)%influx
    nomin = no
    do no=2,n
       val = x(no)%influx
       if (val.gt.rmax) then
          nomax=no
          rmax=val
       elseif (val.lt.rmin) then
          nomin=no
          rmin=val
       endif
    enddo
  end subroutine cct_par_minmax
  !
  subroutine clean_make(method,hclean,clean,tcc)
    use phys_const
    use image_def
    use gkernel_interfaces
    use mapping_gaussian_tool
    use clean_types
    !----------------------------------------------------------------
    ! Build clean image from Component List
    !----------------------------------------------------------------
    type(clean_par), intent(inout) :: method
    type(gildas),    intent(inout) :: hclean
    real,            intent(inout) :: clean(hclean%gil%dim(1),hclean%gil%dim(2))
    type (cct_par),  intent(in)    :: tcc(method%n_iter)
    !
    integer :: nx,ny,nc,ix,iy,ic,imax,ndim,dim(2),ier
    real :: xinc, yinc,rmax,flux,r,fact
    real, allocatable :: wfft(:)
    complex, allocatable :: ft(:,:)
    !
    nx = hclean%gil%dim(1)
    ny = hclean%gil%dim(2)
    allocate (wfft(2*max(nx,ny)),ft(nx,ny),stat=ier)
    if (ier.ne.0) then
       print *,'F-MAPPING,  Memory allocation error in CLEAN_MAKE'
       STOP
    endif
    xinc = hclean%gil%convert(3,1)
    yinc = hclean%gil%convert(3,2)
    !
    if (method%method.eq.'SDI'.or.method%method.eq.'MULTI') then
       ft(:,:) = cmplx(clean,0.0)
    else
       !
       ! Check minimum flux
       ft = 0.0
       rmax = abs(tcc(1)%value)
       imax = 1
       nc = method%n_iter
       do ic=1,nc
          r = abs(tcc(ic)%value)
          if (r.lt.rmax) then
             imax = ic
             rmax = r
          endif
       enddo
       if (imax.lt.nc-10) then
          !            WRITE(CHAIN,*)
          !     $      'I-CLEAN,  Truncating CLEAN component to '
          !     $      ,IMAX,' / ',NC
          !            CALL GAGOUT(CHAIN)
          !            NC = IMAX
       endif
       !
       ! Convolve source list into residual map ---
       if (method%bshift(3).eq.0) then
          do ic=1,nc
             ix = tcc(ic)%ix
             iy = tcc(ic)%iy
             ft(ix,iy) = ft(ix,iy) + tcc(ic)%value
          enddo
       else
          do ic=1,nc
             flux = 0.5*tcc(ic)%value
             ix = tcc(ic)%ix
             iy = tcc(ic)%iy
             ft(ix,iy) = ft(ix,iy) + flux
             ix = ix+method%bshift(1)
             iy = iy+method%bshift(2)
             ft(ix,iy) = ft(ix,iy) + flux
          enddo
       endif
    endif
    !
    ndim = 2
    dim(1) = nx
    dim(2) = ny
    call fourt(ft,dim,ndim,-1,0,wfft)
    !
    ! Beam Area = PI * BMAJ * BMIN / (4 * LOG(2) ) for flux density
    ! normalisation
    fact = method%major*method%minor*pi/(4.0*log(2.0))   &
         &    /abs(xinc*yinc)/(nx*ny)
    call mulgau(ft,nx,ny,   &
         &    method%major,method%minor,method%angle,   &
         &    fact,xinc,yinc)
    call fourt(ft,dim,ndim,1,1,wfft)
    !
    ! Extract Real part
    clean = real(ft)
    deallocate (wfft,ft)
  end subroutine clean_make
  !
  subroutine remisajour(nxy,clean,resid,tfbeam,fcomp,&
       wcl,ncl,nx,ny,wfft,&
       np,primary,weight,wtrun)
    use gkernel_interfaces, only : fourt
    !----------------------------------------------------------------------
    ! Subtract last major cycle components from residual map.
    !----------------------------------------------------------------------
    integer,       intent(in)    :: nxy               ! Problem size
    integer,       intent(in)    :: nx,ny             ! Problem size
    integer,       intent(in)    :: ncl               ! Number of clean components
    integer,       intent(in)    :: np                ! Number of primary beams
    type(cct_par), intent(in)    :: wcl(ncl)          ! Clean component list
    real,          intent(in)    :: tfbeam(nx,ny,np)  ! TF of dirty beams
    complex,       intent(inout) :: fcomp(nx,ny)      ! Work area
    real,          intent(inout) :: clean(nx,ny)      ! Clean map
    real,          intent(inout) :: resid(nx,ny)      ! Residual map
    real,          intent(in)    :: primary(np,nx,ny) ! Primary beams
    real,          intent(in)    :: weight(nx,ny)     ! Flat field weights
    real,          intent(in)    :: wtrun             ! Truncation of mosaic
    real,          intent(in)    :: wfft(*)           ! work fft area
    !
    integer :: i,j,k,ndim,nn(2),ip
    !
    ndim = 2
    nn(1) = nx
    nn(2) = ny
    if (np.eq.1) then
       fcomp = 0.0
       do k = 1,ncl
          fcomp(wcl(k)%ix,wcl(k)%iy) = cmplx(wcl(k)%value,0.0)
       enddo
       call fourt(fcomp,nn,ndim,-1,0,wfft)
       fcomp = fcomp*tfbeam(:,:,1)
       call fourt(fcomp,nn,ndim,1,1,wfft)
       resid = resid-real(fcomp)
    else
       ! Optimized by using CLEAN to store sum of components before multiplying
       ! by the weights to subtract from the Residuals
       clean = 0.0
       do ip=1,np
          fcomp = 0.0
          do k=1,ncl
             fcomp(wcl(k)%ix,wcl(k)%iy) = wcl(k)%value   &
                  &          * primary(ip,wcl(k)%ix,wcl(k)%iy)
          enddo
          call fourt(fcomp,nn,ndim,-1,0,wfft)
          fcomp = fcomp*tfbeam(:,:,ip)
          call fourt(fcomp,nn,ndim,1,1,wfft)
          do j=1,ny
             do i=1,nx
                if (primary(ip,i,j).gt.wtrun) then
                   clean(i,j) = clean(i,j) + real(fcomp(i,j))   &
                        &              *primary(ip,i,j)
                endif
             enddo
          enddo
       enddo
       resid = resid - clean*weight
    endif
  end subroutine remisajour
  !
  subroutine soustraire(wcl,ncl,&
       dirty,nx,ny,ixbeam,iybeam,&
       ixpatch,iypatch,kcl,gain,&
       nf, primary, weight, wtrun)
    !$ use omp_lib
    !----------------------------------------------------------------------
    ! Subtract the clean component, convolved with dirty beam restricted to
    ! beam patch, from the list of selected points.  Caution : points must be
    ! ordered according I and J
    !----------------------------------------------------------------------
    integer,       intent(in)    :: nf                   ! Number of fields
    integer,       intent(in)    :: ncl                  ! nombre de points retenus
    integer,       intent(in)    :: nx,ny,ixbeam,iybeam  ! dimension et centre du beam
    real,          intent(in)    :: dirty(nx,ny,nf)      ! Dirty Beam
    integer,       intent(in)    :: ixpatch,iypatch      ! taille du centre BEAM retenu
    type(cct_par), intent(inout) :: wcl(ncl)
    integer,       intent(in)    :: kcl                  ! No de la composante introduite
    real,          intent(in)    :: gain                 ! gain de clean
    real,          intent(in)    :: primary (nf,nx,ny)   ! Primary beams
    real,          intent(in)    :: weight (nx,ny)       ! Effective weights on sky
    real,          intent(in)    :: wtrun
    !
    logical :: condi,condj,cond,go_on  ! pour les boucles de soustraction
    integer :: no                      ! No du point courrant
    real :: f                          ! coefficient a retrancher
    integer :: k,l
    ! dans la carte du beam translatee sur la composante
    integer :: l0,k0 ! translation * du beam % a la composante
    integer :: i0,j0,i,j,if,fo,lo,ithread
    !
    ! Subtract current component
    i0 = wcl(kcl)%ix
    j0 = wcl(kcl)%iy
    k0 = i0 - ixbeam
    l0 = j0 - iybeam
    f = gain * wcl(kcl)%influx
    wcl(kcl)%influx = wcl(kcl)%influx - f      ! This property is always true
    if (nf.gt.1) then
       f = f * weight(i0,j0)      ! Convert to Clean Component
    else
       f = f / dirty(ixbeam,iybeam,1)
    endif
    !
    lo = kcl + 1
    fo = kcl - 1
    go_on = .true.
    !$OMP PARALLEL &
    !$OMP &   SHARED(f,wcl,dirty,fo,lo)  FIRSTPRIVATE(go_on) &
    !$OMP &   PRIVATE(no,i,j,k,l,condi,condj,if)
    !$  ithread = omp_get_thread_num()
    !$  !print *,"Number of threads ",omp_get_num_threads(),ithread
    !$OMP DO ORDERED
    ! Subtract from current component to last valid.
    do no = lo,ncl
       if (go_on) then
          i = wcl(no)%ix
          j = wcl(no)%iy
          k = i-k0
          l = j-l0
          condi = abs(k-ixbeam) .lt. ixpatch
          condj = abs(l-iybeam) .lt. iypatch
          cond  = condi .or. condj
          if (.not.cond) then
             go_on = .false.
          else
             if ( (condi.and.condj) .and. (k.gt.0 .and. k.le.nx)   &
                  &      .and. (l.gt.0 .and. l.le.ny) ) then
                if (nf.gt.1) then
                   do if = 1,nf
                      if (primary(if,i,j).gt.wtrun) then
                         wcl(no)%influx = wcl(no)%influx - f*dirty(k,l,if)   &
                              &              *primary(if,i0,j0)*primary(if,i,j)   &
                              &              *weight(i,j)
                      endif
                   enddo
                else
                   wcl(no)%influx = wcl(no)%influx - f*dirty(k,l,1)
                endif
             endif
          endif
       endif
    enddo
    !
    !$OMP END DO
    go_on = .true.
    !$OMP DO ORDERED
    ! Now subtract from first valid to current component
    do no = fo,1,-1
       if (go_on) then
          i = wcl(no)%ix
          j = wcl(no)%iy
          k = i-k0
          l = j-l0
          condi = abs(k-ixbeam) .lt. ixpatch
          condj = abs(l-iybeam) .lt. iypatch
          cond  = condi .or. condj
          if (.not.cond) then
             go_on = .false.
          else
             !
             if ( (condi.and.condj) .and. (k.gt.0 .and. k.le.nx)   &
                  &      .and. (l.gt.0 .and. l.le.ny) ) then
                if (nf.gt.1) then
                   do if = 1,nf
                      if (primary(if,i,j).gt.wtrun) then
                         wcl(no)%influx = wcl(no)%influx - f*dirty(k,l,if)   &
                              &              *primary(if,i0,j0)*primary(if,i,j)   &
                              &              *weight(i,j)
                      endif
                   enddo
                else
                   wcl(no)%influx = wcl(no)%influx - f*dirty(k,l,1)
                endif
             endif
          endif
       endif
    enddo
    !$OMP END DO
    !$OMP END PARALLEL
  end subroutine soustraire
  !
  subroutine choice(r,nx,ny,list,nl,limite,mcl,wcl,ncl,rmax,ngoal)
    !----------------------------------------------------------------------
    ! Select possible clean components in residual map.  Components are
    ! returned ordered in increasing I and J.
    ! 
    ! Search list is built if ngoal>0, re-used otherwise.
    !----------------------------------------------------------------------
    integer,       intent(in)    :: nx         ! X size
    integer,       intent(in)    :: ny         ! Y size
    integer,       intent(in)    :: nl         ! List size
    integer,       intent(in)    :: mcl        ! Maximum number of components
    integer,       intent(out)   :: ncl        ! Number of components found
    real,          intent(in)    :: r(nx*ny)   ! Input array
    type(cct_par), intent(out)   :: wcl(mcl)   ! Components
    integer,       intent(in)    :: list(nl)   ! List of pixels 
    real,          intent(inout) ::  limite    ! selection threshold
    real,          intent(in)    :: rmax       ! Maximum value
    integer,       intent(in)    :: ngoal      ! Intended number   
    !
    integer :: i,k
    integer, parameter :: nh=128
    integer :: hx(nh)
    real :: hmin,hstep
    !
    if (ngoal.gt.0) then
       hmin = 0.0
       hstep = rmax/(nh-2)
       call histos(r,nx,ny,list,nl,hx,nh,hmin,hstep)
       !
       ! Locate a "reasonable" number of components
       hmin = 0
       do i=1,nh
          if (hmin.eq.0) then
             if (hx(i).lt.ngoal) then
                hmin = (i-1)*hstep
                ncl = hx(i)
             endif
          endif
       enddo
       limite = max(limite,hmin)
    endif
    !
    ! Select possible components in Clean Box
    ncl = 0
    do i=1,nl
       k = list(i)
       if (abs(r(k)).ge.limite) then
          ncl=ncl+1
          wcl(ncl)%influx =r(k)
          wcl(ncl)%iy = (k-1)/nx+1
          wcl(ncl)%ix = k-(wcl(ncl)%iy-1)*nx
       endif
    enddo
  end subroutine choice
  !
  subroutine histos(r,nx,ny,list,nl,hx,nh,hmin,hstep)
    !----------------------------------------------------------------------
    ! Computes the histogram of part of an image
    !----------------------------------------------------------------------
    integer, intent(in)  :: nx       ! X size
    integer, intent(in)  :: ny       ! Y size
    real,    intent(in)  :: r(nx*ny) ! Input array 
    integer, intent(in)  :: nl       ! List size
    integer, intent(in)  :: list(nl) ! List of values
    integer, intent(in)  :: nh       ! Histogram size
    integer, intent(out) :: hx(nh)   ! Histogram
    real,    intent(in)  :: hmin     ! Min bin
    real,    intent(in)  :: hstep    ! Bin size
    !
    real :: steph
    integer :: i,k,nin,nout,n
    !
    nin = 0
    nout = 0
    hx = 0
    steph = 1.0/hstep
    !
    do i=1,nl
       k = list(i)
       n = nint((abs(r(k))-hmin)*steph + 1.0)
       if (n.ge.1 .and. n.le.nh) then
          hx(n) = hx(n)+1
          nin = nin+1
       else
          nout= nout+1
       endif
    enddo
    !
    ! Convert to cumulative form
    do n=nh-1,1,-1
       hx(n) = hx(n)+hx(n+1)
    enddo
  end subroutine histos
end module cct_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
