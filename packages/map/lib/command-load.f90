!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module mapping_load
  use gbl_message
  !
  public :: load_comm
  private
  !
contains
  !
  subroutine load_comm(line,error)
    use gkernel_interfaces
    use file_buffers
    use plot_buffers
    use mapping_show_or_view, only: buffer_copy
    !----------------------------------------------------------------------
    ! LOAD Name
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    integer :: ntype,nc
    character(len=12) :: argu, d_type
    !
    ! Parse input line
    call sic_ke(line,0,1,argu,nc,.true.,error)
    if (error) return
    if (argu.eq.'?') argu = ' '
    call sic_ambigs('LOAD',argu,d_type,ntype,vtype,mtype,error)
    if (error) return
    ! Load into SIC variable W
    call buffer_copy(d_type,'W',w_plot,error)
    if (error) return
  end subroutine load_comm
end module mapping_load
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
