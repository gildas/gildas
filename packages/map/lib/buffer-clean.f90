!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module clean_buffers
  use mapping_types
  use clean_types
  use cct_types
  !------------------------------------------------------------------------
  ! Deconvolution arrays
  !------------------------------------------------------------------------
  !
  public :: clean_buffer
  public :: clean_user,clean_prog
  public :: niter_listsize,niter_list
  public :: ares_listsize,ares_list
  !
  public :: clean,resid,mask,cct
  public :: d_list,d_mask,tcc
  private
  !
  ! User part
  type(clean_par), target :: clean_prog,clean_user
  !
  integer :: niter_listsize=0
  integer, allocatable :: niter_list(:)
  !
  integer :: ares_listsize=0
  real, allocatable :: ares_list(:)
  !
  type clean_buffer_user_t
   contains
     procedure, public :: init         => clean_buffer_user_init
     procedure, public :: sicdef       => clean_buffer_user_sicdef
     procedure, public :: get_stopping => clean_buffer_user_get_stopping
  end type clean_buffer_user_t
  type(clean_buffer_user_t) :: clean_buffer
  !
  ! Prog part
  type(mapping_3d_t), target, save :: clean ! Clean cube
  type(mapping_3d_t), target, save :: resid ! Residual cube
  type(mapping_3d_t), target, save :: mask  ! Input mask cube, #0 mask
  type(mapping_3d_t), target, save :: cct   ! Clean component table
  !
  integer, allocatable, target, save :: d_list(:)     ! List of selected pixels
  logical, allocatable, target, save :: d_mask(:,:)   ! Logical mask for pixel selection
  type(cct_par), allocatable,   save :: tcc(:)        ! Clean component list
  !
contains
  !
  subroutine clean_buffer_user_init(user,error)
    use gkernel_interfaces
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(clean_buffer_user_t), intent(out)   :: user
    logical,                    intent(inout) :: error
    !
    call clean_user%init(error)
    if (error) return
    !
    call gildas_null(clean%head)
    call gildas_null(resid%head)  
    call gildas_null(mask%head)
    call gildas_null(cct%head)
  end subroutine clean_buffer_user_init
  !
  subroutine clean_buffer_user_sicdef(user,error)
    use gkernel_interfaces
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(clean_buffer_user_t), intent(inout) :: user
    logical,                    intent(inout) :: error
    !
    call clean_user%sicdef(error)
    if (error) return
    !
    call sic_defstructure('FITTED',.true.,error)
    if (error) return
    call sic_def_real('FITTED%MAJOR',clean_prog%major,0,0,.true.,error)
    if (error) return
    call sic_def_real('FITTED%MINOR',clean_prog%minor,0,0,.true.,error)
    if (error) return
    call sic_def_real('FITTED%ANGLE',clean_prog%angle,0,0,.true.,error)
    if (error) return
  end subroutine clean_buffer_user_sicdef
  !
  subroutine clean_buffer_user_get_stopping(user,miter,ares,iplane)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(clean_buffer_user_t), intent(inout) :: user
    integer,                    intent(inout) :: miter
    real,                       intent(inout) :: ares
    integer,                    intent(in)    :: iplane
    !
    if (iplane.ge.1 .and. iplane.le.niter_listsize) then
       miter = niter_list(iplane)
    endif
    if (iplane.ge.1 .and. iplane.le.ares_listsize) then
       ares = ares_list(iplane)
    endif
  end subroutine clean_buffer_user_get_stopping
end module clean_buffers
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
