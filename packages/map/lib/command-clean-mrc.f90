!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module clean_mrc
  use gbl_message
  !
  public :: mrc_comm
  private
  !
contains
  !
  subroutine mrc_comm(line,error)
    use gkernel_interfaces
    use mapping_interfaces    
    use clean_tool, only: sub_clean,clean_data
    use clean_types, only: beam_unit_conversion
    use uvmap_buffers
    use clean_buffers, only: clean_user,clean_prog
    !----------------------------------------------------------------------
    ! Implementation of Multi Resolution Clean
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    integer :: ratio,nx,ny
    character(len=3) :: rname='MRC'
    !
    if (clean_user%mosaic) then
       call map_message(seve%e,rname,'Not valid for mosaic')
       error = .true.
       return
    endif
    !
    ! Data checkup
    clean_user%method = 'MRC'
    call clean_data (error)
    if (error) return
    !
    ! Parameter Definitions
    call beam_unit_conversion(clean_user)
    call clean_user%copyto(clean_prog)
    !
    ! Smoothing ratio : given by user or 2, 4 or 8 according to image size
    if (clean_prog%ratio.ne.0) then
       ratio = clean_prog%ratio
       if (power_of_two(ratio).eq.-1) then
          call map_message(seve%e,rname,'Smoothing ratio has to be a power of 2')
          error = .true.
          return
       endif
    else
       nx = dirty%head%gil%dim(1)
       ny = dirty%head%gil%dim(2)
       if (nx*ny.gt.512*512) then
          ratio = 8
       elseif (nx*ny.gt.128*128) then
          ratio = 4
       else
          ratio = 2
       endif
    endif
    clean_prog%ratio = ratio
    clean_prog%pflux = sic_present(1,0)
    clean_prog%pcycle = sic_present(2,0)
    clean_prog%qcycle = .false.
    !
    call sub_clean(line,error)
    clean_user%do_mask = .true. ! important sinon CLARK ne marche plus
  end subroutine mrc_comm
end module clean_mrc
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
