! (Copyright IRAM, the GILDAS team)
!
! Author: Stephane Guilloteau
!
! This file contains the full source code to make an Image from a
! UV data file in the "tuv" order (i.e. in time / channels  order, rather
! than in the natural channels / time order).
!
! It is not memory limited, provided one channel fits into Virtual memory.
!
! It will utilize memory as efficiently as possible, reading as many
! channels as possible at once. The memory size should be controlled by the
! SIC logical name GILDAS_SPACE ( defaults to 128 Mbytes).
!
! The code was derived in just a day from the old "UV_MAP", by transposing
! where appropriate.
!
! The new code has been shared with that of a new version of UV_MAP.
!
! An MX version is in progress...
!
program p_invert
  use gkernel_interfaces
  use mapping_interfaces
  use gildas_def
  use uvmap_def_task
  !
  ! Compute Dirty images from .TUV tables
  !
  character(len=filename_length) :: name,uvdata
  logical :: error
  type (par_uvmap) :: map
  !
  error = .false.
  call get_uvmap_par ('UV_INVERT',uvdata,name,map,error)
  if (error) call sysexi(fatale)
  error = .false.
  map%uvcode = code_tuv ! This is 2
  call s_invert (uvdata,name,map,error)
  if (error) call sysexi (fatale)
!

  contains

!<FF>
subroutine s_invert(uvdata,name,map,error)
  use gkernel_interfaces
  use mapping_interfaces
  use gbl_message
  use image_def
  use uvmap_def_task
  use phys_const
  use uv_shift, only: uv_shift_header
  use clean_mx, only: domima
  use uvstat_tool, only: prnoise
  !
  character(len=*), intent(in) :: uvdata   ! UV data file name
  character(len=*), intent(in) :: name     ! Output file name
  type (par_uvmap), intent(inout) :: map   ! Imaging parameters
  logical, intent(out) :: error
  !
  real uvmin,uvmax, cpu1, cpu0
  type (gridding) :: conv
  !
  character(len=*), parameter :: rname='UV_INVERT'
  character(len=60) :: chain
  type (gildas) :: huv, hbeam, hdirty
  real, pointer :: uuv(:), vuv(:), vin(:,:,:)
  real, allocatable, target :: duv(:,:)
  real, allocatable, target :: vall(:,:,:)
  real, allocatable :: visi(:,:,:), wuv(:), ouv(:)
  integer, allocatable :: iuv(:)
  real, allocatable :: work(:)
  logical, allocatable :: suv(:)
  integer nuv, ier, ic, jc, kc
  logical sorted, bychannel
  real, allocatable :: w_mapu(:), w_mapv(:)
  real, allocatable :: w_xgrid(:), w_ygrid(:), w_grid(:,:)
  real, allocatable :: beam(:,:), dirty(:,:,:)
  complex, allocatable :: ftbeam(:,:), tfgrid(:,:,:)
  real wall, rmi, rma, uvmx
  real rms, rc, space_channel
  integer space_gildas
  integer nc,nx,ny,nd,lx,ly,nn(2),ndim,imi,ima
  integer(kind=size_length) :: jmi,jma
  integer lc,ikc,mc,il,iv, nwork
  !
  integer i
  !
  call gag_cpu(cpu0)
  !
  bychannel = .not.map%blocked  ! TEST
  jmi = 1 ! To avoid compiler warnings
  jma = 1
  !
  ! Read Headers
  call gildas_null(huv, type='TUV')
  call sic_parse_file(uvdata,' ','.tuv',huv%file)
  call gdf_read_header (huv,error)
  if (error) return
  !
  ! Correct for new phase center if required
  if (map%shift) then
    if (huv%gil%ptyp.eq.p_none) then
      call map_message(seve%w,rname,'No previous phase center info')
      huv%gil%a0 = huv%gil%ra
      huv%gil%d0 = huv%gil%dec
      huv%gil%pang = 0.
      huv%gil%ptyp = p_azimuthal
    elseif (huv%gil%ptyp.ne.p_azimuthal) then
      call map_message(seve%w,rname,'Previous projection type not SIN')
      huv%gil%ptyp = p_azimuthal
    endif
    call uv_shift_header(map%new,huv%gil%a0,huv%gil%d0,huv%gil%pang,map%off,map%shift)
    huv%gil%posi_words = 12
    huv%gil%proj_words = 0
  endif
  !
  ! Define channels and Center frequency
  call t_channel (huv,map)
  !
  ! Compute observing frequency, and new phase center in wavelengths
  if (map%shift) then
    sorted = .false.
    huv%gil%a0 = map%new(1)
    huv%gil%d0 = map%new(2)
    huv%gil%pang = map%new(3)
    map%cs(1)  =  cos(map%off(3))
    map%cs(2)  = -sin(map%off(3))
    ! Note that the new phase center is counter-rotated because rotations
    ! are applied before phase shift.
    map%xy(1) = - map%freq * f_to_k * ( map%off(1)*map%cs(1) - map%off(2)*map%cs(2) )
    map%xy(2) = - map%freq * f_to_k * ( map%off(2)*map%cs(1) + map%off(1)*map%cs(2) )
  else
    map%xy = 0.0
    map%cs = (/1.0,0.0/)
  endif
  !
  ! Read data
  nuv = huv%gil%dim(1)
  allocate (duv(nuv,2),suv(nuv),iuv(nuv),stat=huv%status)
  if (gildas_error(huv,rname,error)) return
  !
  call gdf_read_uvonly(huv,duv,error)
  if (gildas_error(huv,rname,error)) return
  !
  uuv => duv(:,1)
  vuv => duv(:,2)
  ! Apply the Rotation
  call s_loaduv (nuv,map%cs,uuv,vuv,suv,uvmin,uvmax)
  !
  ! Check if sorted
  call s_chksuv (nuv,vuv,iuv,sorted)
  if (.not.sorted) then
    call gr4_trie (vuv,iuv,nuv,error)
    allocate (work(nuv),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Cannot allocate sorting array')
      error = .true.
      return
    endif
    call gr4_sort (uuv,work,iuv,nuv)
    if (error) return
  endif
  !
  ! Define the Map characteristics
  call t_map ('UV_INVERT', map,huv,uvmin,uvmax,conv)
  call gag_cpu(cpu1)
  write(chain,102) 'Finished weighting CPU ',cpu1-cpu0
  call map_message(seve%r,rname,chain)
  !
  ! Frome here, U and V are sorted...
  !
  if (map%beam.eq.0) then
    print  *,'Case ONE beam'
    ! Case for One Weight channel (a single dirty beam)
    allocate (wuv(nuv),ouv(nuv),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Cannot allocate Weight array')
      error = .true.
      return
    endif
    if (map%channels(3).le.0) then
      wuv = 1.0
    else
      huv%blc(2) = huv%gil%fcol+huv%gil%natom*map%channels(3)-1
      huv%trc(2) = huv%gil%fcol+huv%gil%natom*map%channels(3)-1
    !! print  *,'Reading BLC ',huv%blc(2),' TRC ',huv%trc(2)
      call gdf_read_data(huv,wuv,error)
      if (gildas_error(huv,rname,error)) return
      if (.not.sorted) then
        call gr4_sort (wuv,work,iuv,nuv)
        deallocate(work)
      endif
    endif
    !
    ! Save original weights
    ouv(:) = wuv ! ouv(1:nuv) = wuv(1:nuv)
    !
    ! Compute the weights from this
    call t_doweig (nuv,uuv,vuv,wuv,map%uniform(1),map%uniform(2),error)
    if (error) return
    !
    ! Apply taper if needed
    call t_dotaper(nuv,uuv,vuv,wuv,map%taper)
    !
    ! Compute the noise (after re-weighting and tapering...)
    wall = 0
    do iv=1,nuv
     if (ouv(iv).gt.0) then
        wall = wall + ouv(iv)
     endif
    enddo
    if (wall.eq.0.0) then
      write(chain,'(a,i12,a)') 'Plane ',map%channels(3), &
           & ' has Zero weight'
      call map_message(seve%e,rname,chain)
      error = .true.
      return
    else
      !
      ! Noise definition
      wall = 1e-3/sqrt(wall)
      call prnoise(rname,'Natural',wall,rms)
      !
      ! Re-normalize the weights and re-count the noise
      call scawei (nuv,wuv,ouv,wall)
      wall = 1e-3/sqrt(wall)
      call prnoise(rname,'Expected',wall,rms)
    endif
    !
    ! Then compute the Dirty Beam
    nx = map%size(1)
    ny = map%size(2)
    nd = nx*ny
    allocate(w_mapu(nx),w_mapv(ny),work(2*max(nx,ny)),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Cannot allocate U V axes arrays')
      error = .true.
      return
    endif
    uvmx = uvmax / (map%freq*f_to_k)
    lx = (uvmx+map%support(1))/map%uvcell(1) + 2
    ly = (uvmx+map%support(2))/map%uvcell(2) + 2
    lx = 2*lx
    ly = 2*ly
    if (ly.gt.ny) then
      write(chain,'(a,a,f8.3)') 'Map cell is too large ', &
           & ' Undersampling ratio ',float(ly)/float(ny)
      call map_message(seve%w,rname,chain)
      ly = min(ly,ny)
      lx = min(lx,nx)
    else
      write(chain,'(a,a,f8.3)') 'Map cell ', &
           & ' Oversampling ratio ',float(ny)/float(ly)
      call map_message(seve%i,rname,chain)
    endif
    call docoor (lx,-map%uvcell(1),w_mapu)
    call docoor (ly,map%uvcell(2),w_mapv)
    !
    allocate(w_xgrid(nx),w_ygrid(ny),w_grid(nx,ny),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Cannot allocate gridding arrays')
      error = .true.
      return
    endif
    call grdtab (ny, conv%vbuff, conv%vbias, w_ygrid)
    call grdtab (nx, conv%ubuff, conv%ubias, w_xgrid)
    !
    ! Compute FFT's
    !
    ! TFGRID is allocated as the largest tolerable / required workspace
    !
    nc = map%channels(2)-map%channels(1)+1
    if (bychannel) then
      mc = 1
    else
      !
      ! Compute MC from the Virtual Memory Requirement
      space_gildas = 128
      ier = sic_getlog('SPACE_GILDAS',space_gildas)          ! Mega Bytes
      space_channel = 8*(2*nuv+nx*ny+lx*ly)  ! All these are Complex numbers
      rc = (space_gildas*1024.0*1024.0)/space_channel        !
      mc = min(max(1,nint(rc)),nc)           ! Don't need to allocate more than NC...
      write(chain,'(A,I4)') 'Blocking Factor ',mc
      call map_message(seve%i,rname,chain)
    endif
    allocate(tfgrid(mc,lx,ly),ftbeam(nx,ny),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Cannot allocate Fourier buffers')
      error = .true.
      return
    endif
    !
    ! Beam computation
    allocate(visi(2,1,nuv),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Cannot allocate Visibility buffers')
      error = .true.
      return
    endif
    visi(1,1,:) = 1.0
    visi(2,1,:) = 0.0
    call s_dofft (nuv,uuv,vuv,wuv,visi,lx,ly,1,tfgrid,  &
        &  w_mapu,w_mapv,map%support,map%uvcell,        &
        &  conv%ubias,conv%vbias,conv%ubuff,conv%vbuff)
    !
    ndim = 2
    nn(1) = nx
    nn(2) = ny
    ! Put the small grid in the large one
    call extracs(1,nx,ny,1,tfgrid,ftbeam,lx,ly)
    call fourt  (ftbeam,nn,ndim,-1,1,work)
    allocate(beam(nx,ny),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Cannot allocate Beam')
      error = .true.
      return
    endif
    call cmtore (ftbeam,beam,nx,ny)
    !
    ! Make beam, not normalized
    call dogrid (w_grid,w_xgrid,w_ygrid,nx,ny,beam)  ! grid correction
    deallocate(w_xgrid,w_ygrid)
    !
    ! Normalize and Free beam
    call docorr (beam,w_grid,nx*ny)
    rma = -1e38
    rmi = 1e38
    call domima (beam,rmi,rma,imi,ima,nx*ny)
    !
    ! Define beam
    call gildas_null(hbeam)
    call t_setbeam (huv,hbeam,map,2)   ! Define Beam header
    call sic_parse_file(name,' ','.beam',hbeam%file)
    hbeam%gil%rmax = rma
    hbeam%gil%rmin = rmi
    !
    ! Write beam
    call gdf_write_image(hbeam,beam,error)
    ! Clean-up
    deallocate(visi)
    deallocate(beam)
    call gag_cpu(cpu1)
    cpu1 = cpu1-cpu0
    write(chain,102) 'Finished beam CPU ',cpu1
    call map_message(seve%i,rname,chain)
    ! Done Beam
    !
    ! Prepare image
    rma = -1e38
    rmi = 1e38
    allocate (visi(2,mc,nuv),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Cannot allocate Visibility buffers')
      error = .true.
      return
    endif
    nc = map%channels(2)-map%channels(1)+1
    !
    call gildas_null(hdirty)
    call t_setdirty(huv,hdirty,map,wall)
    call sic_parse_file(name,' ','.lmv',hdirty%file)
    !
    ! Blocking factor could be computed from virtual memory available...
    if (bychannel) then
      print *,'By channel'
      allocate(vall(nuv,3*nc,1),stat=ier)
      if (ier.ne.0) then
        call map_message(seve%e,rname,'Cannot allocate Visibility buffers')
        error = .true.
        return
      endif
      huv%trc(2) = huv%gil%fcol+huv%gil%natom*map%channels(2)-1
      huv%blc(2) = huv%gil%fcol+huv%gil%natom*(map%channels(1)-1)
      call gdf_read_data(huv,vall,error)
      if (gildas_error(huv,rname,error)) return
      allocate(dirty(nx,ny,nc),stat=ier)
    else
      print *,'By block'
      allocate(vall(nuv,2,mc),stat=ier)
      if (ier.ne.0) then
        call map_message(seve%e,rname,'Cannot allocate Visibility buffers')
        error = .true.
        return
      endif
      call gdf_create_image(hdirty,error)
      allocate(dirty(nx,ny,mc),stat=ier)
    endif
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Cannot allocate Dirty buffer')
      error = .true.
      return
    endif
    !
    ! Loop on channels
    hdirty%blc = 0
    hdirty%trc = 0
    !
    do ic=map%channels(1),map%channels(2),mc
      jc = ic-map%channels(1)+1
      if (bychannel) then
        vin => vall(:,3*jc-2:3*jc,:)
        kc = jc
        il = ic
        lc = 1
      else
        lc = mc
        il = min(ic+mc-1,map%channels(2))
        lc = il-ic+1
        ikc = 1
        do kc = ic,il         ! Here KC is the INPUT channel number
          huv%trc(2) = huv%gil%fcol+huv%gil%natom*(kc-1)+1
          huv%blc(2) = huv%gil%fcol+huv%gil%natom*(kc-1)
          call gdf_read_data(huv,vall(:,:,ikc),error)
          if (gildas_error(huv,rname,error)) return
          ikc = ikc+1
        enddo
        vin => vall
      endif
      !
      if (.not.sorted) then
        ! Sort and Transpose the VIN array into the VISI array
        call s_sortuv(nuv,map%xy,uuv,vuv,suv,iuv,lc,vin,visi)  ! Precess and sort Visibilities
      else
        ! Transpose the VIN array into the VISI array
        call s_swapuv(nuv,lc,vin,visi)
      endif
      !
      call s_dofft (nuv,uuv,vuv,wuv,visi,lx,ly,lc,tfgrid, &
        &   w_mapu,w_mapv,map%support,map%uvcell,         &
        &   conv%ubias,conv%vbias,conv%ubuff,conv%vbuff)
      !
      ! Write channel down, if you wish
      if (.not.bychannel) then
        ikc = 1
        do kc = jc,jc+lc-1          ! Here KC is the output channel number
          call extracs(lc,nx,ny,ikc,tfgrid,ftbeam,lx,ly)    ! Plunge in large grid
          call fourt  (ftbeam,nn,ndim,-1,1,work)            ! Fourier Transform
          call cmtore (ftbeam,dirty(:,:,1),nx,ny)           ! Real part extraction
          call docorr (dirty(:,:,1),w_grid,nx*ny)           ! Grid correction
          call domima (dirty(:,:,1),rmi,rma,imi,ima,nx*ny)  ! Compute extrema
          hdirty%blc(3) = kc
          hdirty%trc(3) = kc
          call gdf_write_data(hdirty,dirty,error)
          if (ima.ne.0) jma = ima+(kc-1)*nd
          if (imi.ne.0) jmi = imi+(kc-1)*nd
          ikc = ikc+1
        enddo
        write(chain,'(A,I5,I5)') 'Finished bloc ',ic,il
        call map_message(seve%i,rname,chain)
      else
        call extracs(lc,nx,ny,1,tfgrid,ftbeam,lx,ly)       ! Plunge in large grid
        call fourt  (ftbeam,nn,ndim,-1,1,work)             ! Fourier Transform
        call cmtore (ftbeam,dirty(:,:,kc),nx,ny)           ! Real part extraction
        call docorr (dirty(:,:,kc),w_grid,nx*ny)           ! Grid correction
        call domima (dirty(:,:,kc),rmi,rma,imi,ima,nx*ny)  ! Compute extrema
        if (ima.ne.0) jma = ima+(kc-1)*nd
        if (imi.ne.0) jmi = imi+(kc-1)*nd
      endif
    enddo
    !
    ! Or write them all together
    call t_setextrema(hdirty,rmi,jmi,rma,jma)
    if (bychannel) then
      hdirty%blc = 0
      hdirty%trc = 0
      call gdf_write_image(hdirty,dirty,error)
    else
      call gdf_update_header(hdirty,error)
      call gdf_close_image(hdirty,error)
    endif
    call gag_cpu(cpu1)
    write(chain,102) 'Finished maps CPU ',cpu1-cpu0
    call map_message(seve%i,rname,chain)
    deallocate(dirty)
    !
    deallocate(work)
    deallocate(vall)
    deallocate(visi)
    deallocate(tfgrid,ftbeam)
    deallocate(w_mapu,w_mapv,w_grid)
    !
    ! Many beam case not yet programmed...
  else
    call map_message(seve%e,rname,'Many beams not implemented')
  endif
102 FORMAT(A,F9.2)
end subroutine s_invert
!
subroutine s_loaduv (nuv,cs,u,v,s,uvmin,uvmax)
  !----------------------------------------------------------------------
  ! UV_INVERT    uvsort routines
  !     load new U,V coordinates and sign indicator into work arrays
  !     for sorting.
  !----------------------------------------------------------------------
  integer, intent(in) :: nuv         ! number of visibilities
  real, intent(inout) :: u(nuv)      ! output U coordinates
  real, intent(inout) :: v(nuv)      ! output V coordinates
  real(kind=8), intent(in) :: cs(2)  ! rotation parameter
  logical, intent(out) :: s(nuv)     ! V sign indicator
  real, intent(out) :: uvmax         ! maximum length
  real, intent(out) :: uvmin         ! minimum baseline
  !
  integer iv
  real uv,uu,vv
  !
  uvmax = 0.0
  uvmin = 1e36
  if (cs(2).eq.0.0) then
    do iv=1,nuv
      if (v(iv).gt.0) then
        u(iv) = -u(iv)
        v(iv) = -v(iv)
        s(iv) = .false.
      else
        s(iv) = .true.
      endif
      uv = u(iv)*u(iv)+v(iv)*v(iv)
      if (uv.ne.0) then
        uvmax = max(uvmax,uv)
        uvmin = min(uvmin,uv)
      endif
    enddo
  else
    do iv=1,nuv
      uu = u(iv)
      vv = v(iv)
      u(iv) = cs(1)*uu - cs(2)*vv
      v(iv) = cs(2)*uu + cs(1)*vv
      if (v(iv).gt.0) then
        u(iv) = -u(iv)
        v(iv) = -v(iv)
        s(iv) = .false.
      else
        s(iv) = .true.
      endif
      uv = u(iv)*u(iv)+v(iv)*v(iv)
      if (uv.ne.0) then
        uvmax = max(uvmax,uv)
        uvmin = min(uvmin,uv)
      endif
    enddo
  endif
  uvmax = sqrt(uvmax)
  uvmin = sqrt(uvmin)
end subroutine s_loaduv
!
subroutine s_chksuv (nuv,v,it,sorted)
  !----------------------------------------------------------------------
  ! UV_INVERT    uvsort routines
  !   Check if visibilities are sorted
  !----------------------------------------------------------------------
  integer, intent(in)  :: nuv      ! Number of visibilities
  integer, intent(out) :: it(nuv)  ! Initial order pointer
  real, intent(in) :: v(nuv)       ! V values
  logical, intent(out) :: sorted   ! array is "negative & increasing"
  !
  integer iv
  real vmax
  !
  do iv=1,nuv
    it(iv) = iv
  enddo
  vmax = v(1)
  do iv = 1,nuv
    if (v(iv).gt.0 .or. v(iv).lt.vmax) then
      sorted = .false.
      return
    endif
    vmax = v(iv)
  enddo
  sorted = .true.
end subroutine s_chksuv
!
subroutine s_sortuv (nuv,xy,u,v,s,it,nc,vin,vout)
  !----------------------------------------------------------------------
  ! UV_INVERT    uvsort routines
  !     sort an input UV table into an output one.
  !     the output UV table is a rotated, phase shifted copy of
  !     the input one, with all V negative and increasing
  !     the output is also transposed for further use
  !----------------------------------------------------------------------
  integer, intent(in) :: nuv          ! number of visibilities
  integer, intent(in) :: nc           ! number of channels
  real, intent(in) :: u(nuv)          ! U values
  real, intent(in) :: v(nuv)          ! V values
  real, intent(in) :: vin(nuv,2,nc)   ! input visibilities
  real, intent(out) :: vout(2,nc,nuv) ! output visibilities
  real(kind=8), intent(in) :: xy(2)   ! phase shifting factors
  logical, intent(in) :: s(nuv)       ! sign of new visibility
  integer, intent(in) :: it(nuv)      ! pointer to old visibility
  !
  integer ic,iv,kv
  real phi, cphi, sphi
  !
  ! simple case: no phase shift
  if (xy(1).eq.0 .and. xy(2).eq.0) then
    do iv=1,nuv
      kv = it(iv)
      if (s(kv)) then
        do ic=1,nc
          vout(1,ic,iv) =  vin(kv,1,ic)
          vout(2,ic,iv) =  vin(kv,2,ic)
        enddo
      else
        do ic=1,nc
          vout(1,ic,iv) =  vin(kv,1,ic)
          vout(2,ic,iv) = -vin(kv,2,ic)
        enddo
      endif
    enddo
  else
    !
    ! complex case: phase center has been shifted
    do iv=1,nuv
      kv = it(iv)
      phi = xy(1)*u(iv)+xy(2)*v(iv)
      cphi = cos(phi)
      sphi = sin(phi)
      if (s(kv)) then
        do ic=1,nc
          vout(1,ic,iv) = vin(kv,1,ic)*cphi - vin(kv,2,ic)*sphi
          vout(2,ic,iv) = vin(kv,1,ic)*sphi + vin(kv,2,ic)*cphi
        enddo
      else
        do ic=1,nc
          vout(1,ic,iv) = vin(kv,1,ic)*cphi + vin(kv,2,ic)*sphi
          vout(2,ic,iv) = vin(kv,1,ic)*sphi - vin(kv,2,ic)*cphi
        enddo
      endif
    enddo
  endif
end subroutine s_sortuv
!
subroutine s_swapuv(nuv,nc,vin,vout)
  !----------------------------------------------------------------------
  ! UV_INVERT    uvsort routines
  !     Transpose the input UV table into an output one.
  !----------------------------------------------------------------------
  integer, intent(in) :: nuv          ! number of visibilities
  integer, intent(in) :: nc           ! number of channels
  real, intent(in) :: vin(nuv,2,nc)   ! input visibilities
  real, intent(out) :: vout(2,nc,nuv) ! output visibilities
  !
  integer ic,iv
  !
  do iv=1,nuv
    do ic=1,nc
      vout(1,ic,iv) = vin(iv,1,ic)
      vout(2,ic,iv) = vin(iv,2,ic)
    enddo
  enddo
end subroutine s_swapuv
!
!
subroutine s_dofft (nv,uu,vv,we,visi,nx,ny,nc,image,mapx,mapy,sup,cell,    &
     &ubias,vbias,ubuff,vbuff)
  !----------------------------------------------------------------------
  ! GILDAS  MAP_FAST
  !   Compute FFT of image by gridding UV data
  !----------------------------------------------------------------------
  use phys_const
  !
  integer, intent(in) :: nv                         ! number of values
  real, intent(in) :: uu(nv)                        ! U coordinates
  real, intent(in) :: vv(nv)                        ! V coordinates
  real, intent(in) :: we(nv)                        ! W weights
  integer, intent(in) :: nx,ny                      ! map size
  integer, intent(in) :: nc                         ! number of channels
  real, intent(in) :: visi(2,nc,nv)                 ! values
  complex, intent(out) :: image(nc,nx,ny)           ! gridded visibilities
  real, intent(in) :: mapx(nx),mapy(ny)             ! coordinates of grid
  real, intent(in) :: sup(2)                        ! support of convolving function in meters
  real, intent(in) :: cell(2)                       ! cell size in meters
  !
  real, intent(in) :: ubias,vbias                   ! Gridding parameters
  real, intent(in) :: ubuff(4096),vbuff(4096)       ! Gridding parameters
  !
  ! Local
  integer ix,iy,ic,i,iin,iou,io
  integer ixm,ixp,iym,iyp,iu,iv
  real result,resima,res
  real u,v,du,dv,ufac,vfac
  real cx,cy,sx,sy
  real(kind=8) xinc,xref,yinc,yref
  !
  integer my,kx,ky
  !
  ufac = 100.d0/cell(1)
  vfac = 100.d0/cell(2)
  xinc = mapx(2)-mapx(1)
  xref = nx/2+1
  yinc = mapy(2)-mapy(1)
  yref = ny/2+1
  !
  ! Reset
  image = 0.0
  !
  ! Use symmetry
  my = ny/2+1
  !
  ! Start with loop on observed visibilities
  do i=1,nv
    u = uu(i)
    v = vv(i)
    result = we(i)  ! weight and taper included
    if (v.gt.0) then
      u = -u
      v = -v
      resima = -result
    else
      resima = result
    endif
    !
    ! Define map cell
    ixp = int((u-sup(1))/xinc+xref+1.0)
    ixm = int((u+sup(1))/xinc+xref)
    iym = int((v-sup(2))/yinc+yref)
    iyp = min(my,int((v+sup(2))/yinc+yref+1.0))
    !
    if (ixm.lt.1.or.ixp.gt.nx.or.iym.lt.1.or.iyp.gt.ny) then
      continue
      !!        print *,'visi ',i,' pixels ',ixm,ixp,iym,iyp
    else
      do iy=iym,iyp
        dv = v-mapy(iy)
        if (abs(dv).le.sup(2)) then
          iv = nint(dv*vfac+vbias)
          do ix=ixm,ixp
            du = u-mapx(ix)
            if (abs(du).le.sup(1)) then
              iu = nint(du*ufac+ubias)
              res = ubuff(iu)*vbuff(iv)
              do ic=1,nc
                image(ic,ix,iy) = image(ic,ix,iy) + &
                &   cmplx(visi(1,ic,i)*result*res,visi(2,ic,i)*resima*res)
              enddo
            endif
          enddo
        endif
      enddo
    endif
    !
    ! Borderline case: use symmetry
    u = -u
    v = -v
    resima = -resima
    if (v.le.sup(2)) then
      ixp = int((u-sup(1))/xinc+xref+1.0)
      ixm = int((u+sup(1))/xinc+xref)
      iym = int((v-sup(2))/yinc+yref)
      iyp = min(my,int((v+sup(2))/yinc+yref+1.0))
      if (ixm.lt.1.or.ixp.gt.nx.or.iym.lt.1.or.iyp.gt.ny) then
        continue
        !         print *,'visi ',i,' pixels ',ixm,ixp,iym,iyp
      else
        do iy=iym,iyp
          dv = v-mapy(iy)
          if (abs(dv).le.sup(2)) then
            iv = nint(dv*vfac+vbias)
            do ix=ixm,ixp
              du = u-mapx(ix)
              if (abs(du).le.sup(1)) then
                iu = nint(du*ufac+ubias)
                res = ubuff(iu)*vbuff(iv)
                do ic=1,nc
                  image(ic,ix,iy) = image(ic,ix,iy) + &
                &   cmplx(visi(1,ic,i)*result*res,visi(2,ic,i)*resima*res)
                enddo
              endif
            enddo
          endif
        enddo
      endif
    endif
  enddo                              ! visibility loop
  !
  ! Apply symmetry
  do iy=my+1,ny
    ky = ny+2-iy
    do ix=2,nx
      kx = nx+2-ix
      do ic=1,nc
        image(ic,ix,iy) = conjg(image(ic,kx,ky))
      enddo
    enddo
  enddo
  !
  ! Missing row is left empty (assume proper coverage of the UV plane)
  do iy = 1,ny
    if (real(image(1,1,iy)).ne.0.0) print *,'Invalid beam ',iy,image(1,1,iy),ny
  enddo
end subroutine s_dofft
!
subroutine t_setextrema(h,rmi,jmi,rma,jma)
  use gkernel_interfaces
  use gildas_def
  use image_def
  !------------------------------------------------
  ! Put extrema into the specified header
  !------------------------------------------------
  type (gildas), target,     intent(inout) :: h
  real(kind=4),              intent(in)    :: rmi  ! Value of minimum
  integer(kind=size_length), intent(in)    :: jmi  ! Position of minimum
  real(kind=4),              intent(in)    :: rma  ! Value of maximum
  integer(kind=size_length), intent(in)    :: jma  ! Position of maximum
  !
  integer(kind=8) :: ilong 
  !
  h%gil%rmax = rma
  h%gil%rmin = rmi
  ilong = jmi
  call gdf_index_to_where (ilong,h%gil%ndim,h%gil%dim,h%gil%minloc)  
  ilong = jma
  call gdf_index_to_where (ilong,h%gil%ndim,h%gil%dim,h%gil%maxloc)  
  h%gil%extr_words = def_extr_words                ! extrema computed
end subroutine t_setextrema
end program p_invert

