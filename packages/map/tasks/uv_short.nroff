.ec _
.ll 76
.ad b
.in 4
.ti -4
.nf
1 UV__SHORT Compute short/zero spacing visibilities from single-dish data. 
.fi
.ti +4
UV__SHORT

This  task computes pseudo-visibilities for short or zero spacings 
from a single dish table of spectra (Class table) or LMV data cube. 

These pseudo visibilities are appended added to the output UV table
(-merged.uvt) alongside the original visibilities.

This task computes short spacings, i.e. when the Interferometer dish
diameter is smaller than the Single-dish diameter, otherwise the task
UV__ZERO should be used instead (see HELP UV__ZERO).

The task performs 4 actions:
.nf
  (1) Setup: Initialization of all relevant variables
  (2) Grid: Creation of a map from the spectra over a grid
  (3) Pseudo: Computation of UV visibilities from the gridded spectra
  (4) Merge: Creation of a new UV table (-merged) with the pseudo
      visibilities plus the original visibilities
.fi


.ti -4
2 ACTION$
.ti +4
TASK\CHARACTER "Action to be performed" ACTION$ "all"

There are 5 possible actions:
.nf
  (1) Setup: Initialization of all relevant variables
  (2) Grid: Creation of a map from the spectra over a grid
  (3) Pseudo: Computation of UV visibilities from the gridded spectra
  (4) Merge: Creation of a new UV table (-merged) with the pseudo
      visibilities plus the original visibilities
  (5) All: execute all the actions at once
.fi

.ti -4
2 NAME$
.ti +4
TASK\CHARACTER "Generic name of input data" NAME$

The generic name of the files to be processed by the task.  The task
will look for single dish data in a file called 'name'.tab and
interferometric data in a file called 'name'.uvt.


.ti -4
2 SPETOL$
.ti +4
TASK\REAL "Tolerance for spectral consistency check" SPETOL$ 0.01

Relative spectral tolerance between the single dish and
interferometric data. SPETOL=0.01 means a tolerance of 1% between the
interferometric and single dish data spectral axes.


.ti -4
2 POSTOL$
.ti +4
TASK\REAL "X-Y position tolerance for gridding" POSTOL$ 0.1

The pointing tolerance between the interferometric and single dish
data as a fraction of the single dish beam.


.ti -4
2 IPFAC$
.ti +4
TASK\REAL "Interferometer calibration factor" IPFAC$ 1.0

Multiplicative factor to be applied to the interferometric data. This
parameter can be used to correct for an incorrect flux scale on the
interferometric data.

.ti -4
2 SDUNIT$
.ti +4
TASK\CHARACTER "Single dish unit" SDUNIT$ "*"

Unit of the single dish data. If set to "*" will get it from the
header of the .tab file. Accepted units are: jy/beam, K (Tmb), K (Ta*)
and K. If the unit is K (Ta*) a calibration factor from K (Ta*) to K
(Tmb) is expected (i.e. SDFAC$ < 1.0).


.ti -4
2 SDFAC$
.ti +4
TASK\REAL "Single dish calibration factor" SDFAC$ 1.0

Multiplicative calibration factor. It is used to convert from antenna
temperature (Ta*) to main beam temperature (Tmb).

.ti -4
2 SDWEI$
.ti +4
TASK\REAL "Single dish weighting   factor" SDWEI$ 1.0

Multiplicative scaling factor on the weights of the single dish
pseudo-visibilities. 

.ti -4
2 UVTRUNC$
.ti +4
TASK\REAL "Single dish UV truncation radius [m]" UVTRUNC$ 0.

No pseudo visibility at UV spacings higher than UVTRUNC$ is
generated. Theoretical consideration on the method used in this task
implies that UVTRUNC$ should be at most (SDDIAM$-IPDIAM$).

If set to zero by the user, it will be set to SDDIAM$-IPDIAM$

.ti -4
2 SDBEAM$
.ti +4
TASK\REAL "Single dish beam diameter [sec]" SDBEAM$ 0

Half-power beam width of the single dish antenna, in arc seconds.  The
beam is assumed to be Gaussian.

If set to zero by the user, the task will look for this information on
the header of the .tab file.

.ti -4
2 SDDIAM$
.ti +4
TASK\REAL "Single dish diameter [m]" SDDIAM$ 0

Diameter of the single dish antenna used to produce the input spectra,
in meters.

If set to zero by the user, the task will look for this information on
the header of the .tab file.

.ti -4
2 IPBEAM$
.ti +4
TASK\REAL "Interferometer primary beam [sec]" IPBEAM$ 0

Half-power beam width of the interferometer antennas (Primary beam),
in arc seconds. The beam is assumed to be Gaussian.

If set to zero by the user, the task will look for this information on
the header of the UV table.

.ti -4
2 IPDIAM$
.ti +4
TASK\REAL "Interferometer diameter [m]" IPDIAM$ 0

Diameter of the interferometer antennas used to produce the input UV
table, in meters.

If set to zero by the user, the task will look for this information on
the header of the UV table.

.ti -4
2 UVSTEP$
.ti +4
TASK\REAL "UV Step for binning weight densities [m]" UVSTEP$ 0

Step in the UV plane for binning weight densities. The size of the
step will determine where the weights will be binned to derive a
weight density. The first step is supposed to contain only the single
dish data and the second step the shortest baselines of the
interferometric data. The difference in scale between the first and
second steps is used to determine the weight scaling factor the
single dish pseudo visibilities.

.ti -4
1 ENDOFHELP
