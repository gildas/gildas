program uv_vla_patchif
  use gkernel_interfaces
  !
  ! Assign the proper frequencies to a UV table 
  ! coming from VLA with IFs
  !
  ! Input:
  !   A UV Table with visibilities containing Nstokes x Nchan
  ! Output
  !   A UV Table with visibilities of size Nchan, but potentially different
  !   polarization states.
  !
  character(len=filename_length) :: nami,namo
  integer :: nif
  real(8), allocatable :: myfreqs(:)
  logical error
  !
  call gildas_open
  call gildas_char('INPUT$',nami)
  call gildas_char('OUTPUT$',namo)
  call gildas_inte('NIF$',nif,1)
  allocate(myfreqs(nif))
  call gildas_dble('FREQS$',myfreqs,nif)
  call gildas_close
  call sub_vla_patchif (nami,namo,nif,myfreqs,error)
  call gagout('I-UV_VLA_PATCHIF,  Successful completion')
end program uv_vla_patchif
!
subroutine sub_vla_patchif(nami,namo,nif,freqs,error)
  use image_def
  use gkernel_interfaces
  use gbl_message
  !
  character(len=*), intent(inout) :: nami  ! Input file name
  character(len=*), intent(inout) :: namo  ! Output file name
  integer, intent(in) :: nif
  real(8), intent(in) :: freqs(nif)
  logical, intent(out) :: error
  ! Global
  character(len=*), parameter :: rname='UV_VLA_PATCHIF'
  ! Local
  type (gildas) :: hin, hou
  real(kind=4), allocatable :: din(:,:), dou(:,:)
  integer :: stoke
  integer :: ic, kc, iv, ier
  integer :: iloop, nblock, nvisi
  integer, allocatable :: indx(:) 
  !
  error = .false.
  !
  ! Read Header of the UV table
  call gildas_null(hin, type= 'UVT')
  call sic_parsef (nami,hin%file,' ','.uvt')
  call gdf_read_header (hin,error)
  if (error) return
  !
  if (nif.ne.hin%gil%nchan) then
    call map_message(seve%e,rname,'Frequency array does not match the number of channels')
    error = .true.
    return
  endif
  if (hin%gil%nstokes.gt.1) then
    call map_message(seve%e,rname,'UV table has more than 1 Stoke parameter')
    error = .true.
    return
  endif  
  ! Initialize the output one
  call gildas_null(hou, type='UVT')
  call gdf_copy_header (hin, hou, error)
  call sic_parsef (namo,hou%file,'  ','.uvt')
  !
  stoke = 0
  if (hin%gil%nstokes.eq.1) then
    if (associated(hin%gil%stokes)) stoke = hin%gil%stokes(1)
  endif
  !
  allocate(hou%gil%freqs(nif), hou%gil%stokes(nif), indx(nif), stat=ier)
  !
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Memory allocation error')
    error = .true.
    return
  endif  
  !
  ! Setup and Sort the frequencies by increasing order
  hou%gil%freqs = freqs+hin%gil%freq
  do ic = 1,nif
    indx(ic) = ic
  enddo
  call gr8_trie(hou%gil%freqs,indx,nif,error)
  hou%gil%stokes = stoke
  hou%gil%nstokes = 1
  hou%gil%nfreq = nif  
  !
  ! Reset the mean frequency in the header...
  hou%gil%freq = sum(hou%gil%freqs)/nif
  ! and in the first axis too
  hou%gil%val(1) = hou%gil%freq
  !
  call gdf_setuv(hou,error)
  !
  ! OK copy the whole stuff...
  call gdf_nitems('SPACE_GILDAS',nblock,hin%gil%dim(1))
  nblock = min(nblock,hin%gil%dim(2))
  !
  allocate (din(hin%gil%dim(1),nblock),dou(hin%gil%dim(1),nblock),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Memory allocation error ')
    error = .true.
    return
  endif
  !
  hin%blc = 0
  hin%trc = 0
  hou%blc = 0
  hou%trc = 0
  call gdf_create_image(hou,error)
  if (error) return
  !
  call map_message(seve%i,rname,'Copying UV data ')
  do iloop = 1,hin%gil%dim(2),nblock
    hin%blc(2) = iloop
    hin%trc(2) = min(iloop+nblock-1,hin%gil%dim(2))
    hou%blc(2) = iloop
    hou%trc(2) = hin%trc(2)
    call gdf_read_data (hin,din,error)
    if (error) return
    !
    nvisi = hin%trc(2)-iloop+1
    do iv=1,nvisi
      dou(1:7,iv) = din(1:7,iv)
      do ic=1,nif
        kc = indx(ic)
        dou(5+3*ic:7+3*ic,iv) = din(5+3*kc:7+3*kc,iv)
      enddo
      if (hou%gil%ntrail.gt.0) then
        dou(8+3*nif:,iv) = din(8+3*nif:,iv)
      endif
    enddo
    call gdf_write_data (hou, dou, error)
    if (error) return
  enddo
  call gdf_close_image(hou,error)
  !
end subroutine sub_vla_patchif
