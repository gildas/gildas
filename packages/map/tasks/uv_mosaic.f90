!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
program uv_mosaic
  use gildas_def
  use gkernel_interfaces
  !----------------------------------------------------------------------
  ! Make a mosaic UV Tables from a set of single field UV tables.
  !
  ! The original UV Tables are assumed to have the same number of channels and
  ! 1) either to have the same phase tracking center
  ! 2) or a phase tracking center corresponding to the pointing center
  !
  ! It is assumed that the names of the original field UV tables
  ! the simple sequence 'NAME'-'I'.uvt
  !----------------------------------------------------------------------
  character(len=*), parameter :: rname='UV_MOSAIC'
  integer(kind=4) :: nf
  character(len=filename_length) :: generic
  logical :: error = .false.
  !
  call gildas_open
  call gildas_inte('FIELDS$',nf,1)
  call gildas_char('NAME$',generic)
  call gildas_close
  !
  if (nf.ne.0) then
     call uv_mosaic_group(rname,generic,nf,error)
  else
     call uv_mosaic_split(rname,generic,nf,error)
  endif
  if (error) call sysexi(fatale)
  call gagout('I-'//rname//',  Successful completion')
end program uv_mosaic
!
subroutine uv_mosaic_fieldname(generic,if,head)
  use image_def
  use gkernel_interfaces
  !----------------------------------------------------------------------
  !
  !----------------------------------------------------------------------
  character(len=*), intent(in)    :: generic
  integer(kind=4),  intent(in)    :: if
  type(gildas),     intent(inout) :: head
  !
  character(len=filename_length) :: name
  character(len=12) :: rchain, lchain
  integer(kind=4) :: nn
  !
  name = generic
  nn = len_trim(name)
  write(rchain,'(I12)') if
  lchain = adjustl(rchain)
  name(nn+1:) = '-'//lchain
  call sic_parse_file(name,' ','.uvt',head%file)
end subroutine uv_mosaic_fieldname
!
subroutine uv_mosaic_group(rname,generic,nf,error)
  use phys_const
  use image_def
  use gbl_message
  use gkernel_types
  use gkernel_interfaces
  !----------------------------------------------------------------------
  ! Build a Mosaic from an ensemble of NF initial fields
  ! with a name following the convention name-'i' i=1,nf
  !----------------------------------------------------------------------
  character(len=*), intent(in)    :: rname
  character(len=*), intent(in)    :: generic
  integer,          intent(in)    :: nf
  logical,          intent(inout) :: error
  !
  type(gildas) :: field
  type(gildas) :: mosaic
  real(kind=4), allocatable :: dmos(:,:)
  real(kind=4), allocatable :: dfield(:,:)
  !
  integer(kind=4) :: ier,nvisi,mvisi
  integer(kind=4) :: if,iv,i_xoff,i_yoff,luv
  real(kind=8) :: doffx,doffy
  type(projection_t) :: proj
  !
  integer(kind=4) :: code,code_x,code_y
  integer(kind=4), parameter :: code_mosaic_void=0   ! Mosaic UV table with undecided offsets
  integer(kind=4), parameter :: code_mosaic_phase=1  ! Mosaic UV table with phase offsets
  integer(kind=4), parameter :: code_mosaic_point=2  ! Mosaic UV table with pointing offsets
  real(kind=4), parameter :: point_accuracy=0.1*rad_per_sec   ! Pointing accuracy
  real(kind=4), parameter :: phase_accuracy=0.001*rad_per_sec ! Phase accuracy
  !
  real(kind=8) :: a0,d0
  character(len=15) :: chde,chra
  !
  ! Initialization
  call gildas_null(field,type='UVT')
  call gildas_null(mosaic,type='UVT')
  !
  ! Sanity check
  if (nf.le.0) then
     call map_message(seve%e,rname,'Number of field must be > 1')
     error = .true.
     return
  endif
  !
  ! Get first header and copy it in output image
  if = 1
  call uv_mosaic_fieldname(generic,if,field)
  call gdf_read_header(field,error)
  if (error) then
     call map_message(seve%e,rname,'Cannot open '//trim(field%file))
     return
  endif
  call gdf_close_image(field,error)
  if (error) return
  call gdf_copy_header(field,mosaic,error)
  !
  ! Check consistency of headers
  code = code_mosaic_void
  if (abs(field%gil%ra-field%gil%a0).gt.point_accuracy .or. &
       abs(field%gil%dec-field%gil%d0).gt.point_accuracy) then
     ! Phase & Pointing center differ
     ! => Later on, we'll check that all fields have a common phase center
     code = code_mosaic_point
  endif
  nvisi = 0
  mvisi = 0
  a0 = 0d0
  d0 = 0d0
  do if=1,nf
     ! Read header of field #if
     call uv_mosaic_fieldname(generic,if,field)
     call gdf_read_header(field,error)
     if (error) then
        call map_message(seve%e,rname,'Cannot open '//trim(field%file))
        return
     endif
     call gdf_close_image(field,error)
     if (error) return
     ! Check spectral axis
     if (field%gil%dim(1).ne.mosaic%gil%dim(1) .or. &
          field%gil%nchan.ne.mosaic%gil%nchan) then
        call map_message(seve%e,rname,'Inconsistent channel number at '//trim(field%file))
        print *,'Dim ',field%gil%dim(1),mosaic%gil%dim(1)
        print *,'Dim ',field%gil%nchan,mosaic%gil%nchan
        error = .true.
     endif
     if (field%gil%ref(1).ne.mosaic%gil%ref(1) .or. &
          field%gil%vres.ne.mosaic%gil%vres  .or. &
          field%gil%fres.ne.mosaic%gil%fres  .or. &
          field%gil%freq.ne.mosaic%gil%freq  .or. &
          field%gil%voff.ne.mosaic%gil%voff) then
        call map_message(seve%e,rname,'Inconsistent spectral axis at '//trim(field%file))
        error = .true.
     endif
     ! Check projection
     if (code.eq.code_mosaic_point) then
        if (abs(mosaic%gil%a0-field%gil%a0).gt.phase_accuracy .or. &
             & abs(mosaic%gil%d0-field%gil%d0).gt.phase_accuracy) then
           call map_message(seve%e,rname,'#1 Inconsistent phase center at '//trim(field%file))
           error = .true.
        endif
     else if (code.eq.code_mosaic_phase) then
        if (field%gil%ra.ne.field%gil%a0 .or. field%gil%dec.ne.field%gil%d0) then
           call map_message(seve%e,rname,'Phase & Pointing center mismatch at '//trim(field%file))
           error = .true.
        endif
     else if (abs(field%gil%ra-field%gil%a0).gt.point_accuracy .or. &
          abs(field%gil%dec-field%gil%d0).gt.point_accuracy) then
        ! Previously undefined, but phase center differ from pointing center
        ! - must have common phase center
        code = code_mosaic_point
        if (abs(mosaic%gil%a0-field%gil%a0).gt.phase_accuracy .or. &
             abs(mosaic%gil%d0-field%gil%d0).gt.phase_accuracy) then
           print *,'A0 ',mosaic%gil%a0, field%gil%a0, 180*3600/pi*abs(mosaic%gil%a0-field%gil%a0)
           print *,'D0 ',mosaic%gil%d0, field%gil%d0, 180*3600/pi*abs(mosaic%gil%d0-field%gil%d0)
           call map_message(seve%e,rname,'#2 Inconsistent phase center at '//trim(field%file))
           error = .true.
        endif
     else
        code = code_mosaic_phase
     endif
     a0 = a0+field%gil%ra
     d0 = d0+field%gil%dec
     if (error) return
     ! Iterate visibility sizes
     nvisi = nvisi + field%gil%nvisi
     mvisi = max(mvisi,field%gil%dim(2))
  enddo ! if
  a0 = a0/nf
  d0 = d0/nf
  !
  ! Prepare the mosaic UV table
  mosaic%gil%nvisi = nvisi
  mosaic%gil%dim(2) = nvisi
  ! There may be problems if a column of the other type is already
  ! present in the initial UV table...
  if (code.eq.code_mosaic_point) then
     code_x = code_uvt_xoff
     code_y = code_uvt_yoff
     if ((mosaic%gil%column_pointer(code_uvt_loff).ne.0) .or. &
          (mosaic%gil%column_pointer(code_uvt_moff).ne.0)) then
        error = .true.
     endif
  else
     code_x = code_uvt_loff
     code_y = code_uvt_moff
     if ((mosaic%gil%column_pointer(code_uvt_xoff).ne.0) .or. &
          (mosaic%gil%column_pointer(code_uvt_yoff).ne.0)) then
        error = .true.
     endif
  endif
  if (error) then
     call map_message(seve%e,rname,'Combination of PHASE_OFF and POINT_OFF not allowed')
     return
  endif
  ! Add or reuse the offset columns
  if (mosaic%gil%column_pointer(code_x).eq.0) then
     mosaic%gil%dim(1) = mosaic%gil%dim(1)+1
     mosaic%gil%column_pointer(code_x) = mosaic%gil%dim(1)
     mosaic%gil%column_size(code_x) = 1
  endif
  if (mosaic%gil%column_pointer(code_y).eq.0) then
     mosaic%gil%dim(1) = mosaic%gil%dim(1)+1
     mosaic%gil%column_pointer(code_y) = mosaic%gil%dim(1)
     mosaic%gil%column_size(code_y) = 1
  endif
  i_xoff = mosaic%gil%column_pointer(code_x)
  i_yoff = mosaic%gil%column_pointer(code_y)
  ! Define the projection center for the offset columns
  if (code.eq.code_mosaic_phase) then
     mosaic%gil%a0 = a0
     mosaic%gil%d0 = d0
  endif
  mosaic%gil%ra = mosaic%gil%a0
  mosaic%gil%dec = mosaic%gil%d0
  call gwcs_projec(mosaic%gil%a0,mosaic%gil%d0,mosaic%gil%pang,mosaic%gil%ptyp,proj,error)
  !
  ! Allocate visibility buffers
  allocate(dmos(mosaic%gil%dim(1),nvisi), stat=ier)
  if (failed_allocate(rname,'mosaic uv table',ier,error)) return
  allocate(dfield(field%gil%dim(1),mvisi), stat=ier)
  if (failed_allocate(rname,'Field uv table',ier,error)) return
  !
  ! Create, fill, and write the mosaic UV table
  call sic_parse_file(generic,' ','.uvt',mosaic%file)
  call gdf_create_image(mosaic,error)
  if (error) return
  field%blc = 0
  field%trc = 0
  mosaic%blc = 0
  mosaic%trc = 0
  do if=1,nf
     call uv_mosaic_fieldname(generic,if,field)
     call gdf_read_header(field,error)
     if (error) return
     call abs_to_rel(proj,field%gil%ra,field%gil%dec,doffx,doffy,1)
     call gdf_read_data(field,dfield,error)
     if (error) return
     call gdf_close_image(field,error)
     if (error) return
     luv = field%gil%dim(1)
     !
     do iv=1,field%gil%nvisi
        dmos(1:luv,iv) = dfield(1:luv,iv)
        dmos(i_xoff,iv) = doffx
        dmos(i_yoff,iv) = doffy
     enddo
     !
     mosaic%blc(2) = mosaic%trc(2)+1
     mosaic%trc(2) = mosaic%blc(2)+field%gil%nvisi-1
     !
     call gdf_write_data(mosaic,dmos,error)
     if (error) return
  enddo
  call gdf_close_image(mosaic,error)
  if (error) return
  !
  print *,""
  print *,"New projection center:"
  call rad2sexa (mosaic%gil%a0,24,chra)
  call rad2sexa (mosaic%gil%d0,360,chde)
  print *,'A0: ',chra
  print *,'D0: ',chde
  print *,""
end subroutine uv_mosaic_group
!
subroutine uv_mosaic_split(rname,generic,nf,error)
  use phys_const
  use gkernel_interfaces
  use gkernel_types
  use image_def
  use gbl_message
  !---------------------------------------------------------------------------
  ! Explode a Mosaic to an ensemble of NF single fields
  ! with a name following the convention name-'i'
  !---------------------------------------------------------------------------
  character(len=*), intent(in)    :: rname
  character(len=*), intent(in)    :: generic
  integer(kind=4),  intent(out)   :: nf
  logical,          intent(inout) :: error
  !
  type(gildas) :: field
  type(gildas) :: mosaic
  real(kind=4), allocatable :: dmos(:,:)
  real(kind=4), allocatable :: dfield(:,:)
  !
  integer(kind=4), parameter :: code_mosaic_phase=1  ! Mosaic UV table with phase offsets
  integer(kind=4), parameter :: code_mosaic_point=2  ! Mosaic UV table with pointing offsets
  integer(kind=4) :: code,ier
  integer(kind=4) :: nlast,mvisi
  integer(kind=4) :: jv,iv,ifi,kfi,nfi,mfi
  integer(kind=4) :: loff,moff,xoff,yoff,xcol,ycol
  real(kind=8) :: doffx,doffy,ra,dec
  real(kind=8), allocatable :: doff(:,:),dtmp(:,:)
  character(len=14) :: chra,chde
  type(projection_t) :: proj
  !
  ! Initialization
  call gildas_null(field,type='UVT')
  call gildas_null(mosaic,type='UVT')
  !
  ! Sanity check
  if (nf.ne.0) then
     call map_message(seve%e,rname,'Number of field must be = 0')
     error = .true.
     return
  endif
  !
  ! Get mosaic header and copy it to the field header
  call sic_parse_file(generic,' ','.uvt',mosaic%file)
  call gdf_read_header(mosaic,error)
  if (error) then
     call map_message(seve%e,rname,'Cannot open '//trim(mosaic%file))
     return
  endif
  call gdf_copy_header(mosaic,field,error)
  if (error) return
  !
  ! Check mosaic kind
  loff = mosaic%gil%column_pointer(code_uvt_loff)
  moff = mosaic%gil%column_pointer(code_uvt_moff)
  xoff = mosaic%gil%column_pointer(code_uvt_xoff)
  yoff = mosaic%gil%column_pointer(code_uvt_yoff)
  if ((loff.ne.0).and.(moff.ne.0)) then
     call map_message(seve%i,rname,'Input UV table is a phase offset mosaic')
     xcol = loff
     ycol = moff
     code = code_mosaic_phase
  else if ((xoff.ne.0).and.(yoff.ne.0)) then
     call map_message(seve%i,rname,'Input UV table is a pointing offset mosaic')
     xcol = xoff
     ycol = yoff
     code = code_mosaic_point
  else
     call map_message(seve%e,rname,'Input UV table is not a mosaic (No XY or LM offset columns)')
     error = .true.
     return
  endif
  !
  ! Get rid of the trailing columns if possible. Just verify
  ! that ntrail is 2, and that these are the two appropriate
  ! columns
  if (field%gil%ntrail.eq.2) then
     if (((xcol.eq.field%gil%lcol+1) .and. &
          (ycol.eq.field%gil%lcol+2)) .or. &
         ((xcol.eq.field%gil%lcol+2) .and. &
          (ycol.eq.field%gil%lcol+1))) then
        if (code.eq.code_mosaic_point) then
           field%gil%column_pointer(code_uvt_xoff) = 0
           field%gil%column_pointer(code_uvt_yoff) = 0
           field%gil%column_size(code_uvt_xoff) = 0
           field%gil%column_size(code_uvt_yoff) = 0
        else
           field%gil%column_pointer(code_uvt_loff) = 0
           field%gil%column_pointer(code_uvt_moff) = 0
           field%gil%column_size(code_uvt_loff) = 0
           field%gil%column_size(code_uvt_moff) = 0
        endif
     endif
     field%gil%ntrail = 0
     field%gil%dim(1) = field%gil%lcol
     nlast = field%gil%lcol
  else
     nlast = field%gil%dim(1)
  endif
  !
  ! Read data
  allocate(dmos(mosaic%gil%dim(1),mosaic%gil%dim(2)),stat=ier)
  if (failed_allocate(rname,'Mosaic uv table',ier,error)) return
  call gdf_read_data(mosaic,dmos,error)
  if (error) return
  !
  ! Scan the list of offset
  doffx = dmos(xcol,1)
  doffy = dmos(ycol,1)
  nfi = 1
  mfi = 100
  allocate(doff(3,mfi),stat=ier)
  doff(1,1) = doffx
  doff(2,1) = doffy
  doff(3,1) = 1
  do iv=2,mosaic%gil%nvisi
     kfi = 0
     do ifi=1,nfi
        if (dmos(xcol,iv).eq.doff(1,ifi) .and. &
            dmos(ycol,iv).eq.doff(2,ifi)) then
           kfi = ifi
           doff(3,kfi) = doff(3,kfi)+1
           exit
        endif
     enddo ! ifi
     ! New field => Initialize
     if (kfi.eq.0) then
        if (nfi.eq.mfi) then
           allocate(dtmp(3,2*mfi),stat=ier)
           dtmp(:,1:mfi) = doff(:,:)
           deallocate(doff)
           allocate(doff(3,2*mfi),stat=ier)
           doff(:,:) = dtmp
           deallocate(dtmp)
           mfi = 2*mfi
        endif
        nfi = nfi+1
        doff(1,nfi) = dmos(xcol,iv)
        doff(2,nfi) = dmos(ycol,iv)
        doff(3,nfi) = 1
     endif
  enddo ! iv
  !
  ! Allocate enough space for field uv table
  mvisi = 0
  do ifi=1,nfi
     mvisi = max(nint(doff(3,ifi)),mvisi)
  enddo
  allocate(dfield(nlast,mvisi),stat=ier)
  if (failed_allocate(rname,'Field uv table',ier,error)) return
  !
  ! Create, fill, and write the output UV tables
  call gwcs_projec(mosaic%gil%a0,mosaic%gil%d0,mosaic%gil%pang,mosaic%gil%ptyp,proj,error)
  if (error) return
  do ifi=1,nfi
     call uv_mosaic_fieldname(generic,ifi,field)
     field%gil%nvisi = doff(3,ifi)
     field%gil%dim(2) = field%gil%nvisi
     call rel_to_abs(proj,doff(1,ifi),doff(2,ifi),ra,dec,1)
     field%gil%ra = ra
     field%gil%dec = dec
     if (code.eq.code_mosaic_phase) then
        field%gil%a0 = ra
        field%gil%d0 = dec
     endif
     !
     jv = 0
     do iv=1,mosaic%gil%nvisi
        if ((dmos(xcol,iv).eq.doff(1,ifi)) .and. &
            (dmos(ycol,iv).eq.doff(2,ifi))) then
           jv = jv+1
           dfield(1:nlast,jv) = dmos(1:nlast,iv)
        endif
     enddo
     field%gil%nvisi = jv
     !
     call gdf_create_image(field,error)
     if (error) return
     field%blc = 0
     field%trc = 0
     call gdf_write_data(field,dfield,error)
     if (error) return
     call gdf_close_image(field,error)
     if (error) return
  enddo ! ifi
end subroutine uv_mosaic_split
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
