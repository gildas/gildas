program uv_fit
  use gildas_def
  use gkernel_interfaces
  use image_def
  ! Local
  type(gildas) :: x
  ! PI is defined with more digits than necessary to avoid losing
  ! the last few bits in the decimal to binary conversion
  real(8), parameter :: pi=3.14159265358979323846d0
  real(8), parameter :: f_to_k=1.d0/299792458.d-6    ! not 2*PI

  character(len=80) :: name,resu
  real, allocatable :: ipq(:) ! Q Data Pointer, binned
  real, allocatable :: ipy(:) ! Y Data Pointer, binned
  real, allocatable :: ipw(:) ! Y weights, binned
  real, allocatable :: ipaq(:) ! Q Data Pointer
  real, allocatable :: ipar(:) ! Real Data Pointer
  real, allocatable :: ipai(:) ! Imaginary Data Pointer
  real, allocatable :: ipaw(:) ! Weight Data Pointer
  integer :: nv, ndata, iflag, kc, ku, jpar
  integer :: ifun, nfun, if1, if2, k, i, choixf(3), ic(2), nrep, ier
  integer :: nvs, nvi
  logical :: error, ampl, bin
  real(8) :: a(6), var_a(6), lambda, a0(6), lambda0
  real :: chi_2, fact, para(3), qrange(3), res(12), velo
  parameter (nfun=11)
  character(len=8) :: f, cf, func(nfun)
  !
  data func/'NONE','DISK','RING','PARABOLA','GAUSSIAN','EXPONENT',   &
     &    'POWER-1','POWER-2','POWER-3','POINT','PLANET'/
  !
  call gildas_open
  call gildas_char('UVTABLE$',name)
  call gildas_inte('CHANNEL$',ic,2)
  call gildas_char('FUNCTION_1$',f)
  call gildas_real('PARA_1$',para,3)
  call sic_ambigs('FIT',f,cf,if1,func,nfun,error)
  if (error) then
    write(6,*) 'E-UV_FITC,  Invalid function '//f
    goto 999
  endif
  if1 = if1 - 1
  a(1) = para(1)
  a(2) = para(2)
  a(3) = para(3)
  if (if1 .eq. 2) then
    k = 4
  elseif (if1.eq.9) then
    k = 2
  else
    k = 3
  endif
  !
  call gildas_char('FUNCTION_2$',f)
  call gildas_real('PARA_2$',para,3)
  call sic_ambigs('FIT',f,cf,if2,func,nfun,error)
  if (error) then
    write(6,*) 'E-UV_FITC,  Invalid function '//f
    goto 999
  endif
  if2 = if2 - 1
  if (if1.gt.0 .and. if2.gt.0) then
    choixf(2) = if1
    choixf(3) = if2
    if (para(1)*a(1) .gt. 0) then
      print *,'Selecting SUM '
      choixf(1) = 0            ! Sum
      a(k) = para(1)
      a(k+1) = para(2)
      a(k+2) = para(3)
    endif
  elseif (if1 .gt. 0) then
    choixf(1) = if1
  elseif (if2 .gt. 0) then
    choixf(1) = if2
    do i = 1,3
      a(i) = para(i)
    enddo
  else
    write(6,*) 'E-UV_FITC,  No function to fit'
    goto 999
  endif
  print *,'CHOIX ',choixf
  print *, ' '
  call gildas_char('RESULT$',resu)
  call gildas_real('QRANGE$',qrange,3)
  call gildas_dble('LAMBDA$',lambda,1)
  call gildas_logi('AMPLITUDE$',ampl,1)
  call gildas_logi('BINDATA$',bin,1)
  call gildas_close
  nrep = max(8,nint(qrange(3)))
  !
  write(6,*) 'I-UV_FIT,  Loading data'
  call gildas_null(x, type = 'UVT')
  call gdf_read_gildas (x, name, '.uvt', error)
  if (error) then
    write(6,100) 'F-UV_FITC, Cannot read input table'
    goto 999
  endif
  !
  ndata = x%gil%dim(2)
  ier = gdf_range (ic,x%gil%nchan)
  do i=1,6
    a0(i) = a(i)
  enddo
  lambda0 = lambda
  !
  ! Get virtual memory according to binning option
  if (bin) then
    allocate (ipq(nrep), ipy(nrep), ipw(nrep), stat=ier)
    allocate (ipaq(ndata), ipar(ndata), ipai(ndata), ipaw(ndata), stat=ier)
  else
    nrep = ndata
    allocate (ipq(nrep), ipy(nrep), ipw(nrep), stat=ier)
  endif
  !
  name = resu
  call sic_parsef (name,resu,' ','.fit')
  ku = lenc(resu)
  open(unit=2,file=resu(1:ku),status='UNKNOWN')
  ku = ku+1
  !
  ! Loop on channels
  nvs = x%gil%dim(1)
  nvi = x%gil%dim(2)
  !
  do kc = ic(1),ic(2)
    ! Load (U^2+V^2) in wavelengths, Compute observing frequency for channel KC
    do i=1,6
      a(i) = a0(i)
    enddo  ! i
    lambda = lambda0
    chi_2 = 0.0
    fact = (x%gil%val(1) + x%gil%fres * (x%gil%val(1)/x%gil%freq) * kc) * f_to_k
    if (bin) then
      nv = nrep
    else
      nv = 0
    endif
    call load_data(ndata,x%r2d,nvs,kc,fact,ampl,   &
     &      ipaq,ipar,ipai,ipaw,   &
     &      nv,ipq,ipy,ipw,   &
     &      qrange(1),qrange(2))
    if (nv.eq.0) then
      write(6,*) 'W-UV_FITC,  No data for channel',kc
      cycle !! goto 30
    endif
    !
    print *,'Lambda ',lambda
    if (lambda.gt.1d-7) then
      write(6,*) 'I-UV_FITC,  Starting minimization on channel',kc
      call linear(nv, ipq, ipy, ipw,   &
     &        choixf,a,lambda,var_a,chi_2,iflag)
    endif
    !
    ! Convert variance to sigma, and print result
    write(6,100)
    do i = 1,6
      var_a(i) = sqrt(var_a(i))
      write(6,*) 'Par. ', i, a(i), var_a(i)
    enddo  ! i
    write(6,100)
    write(6,1001) '        UV_FITC results for channel ',kc
    ifun = choixf(1)
    if (ifun.ne.0) then
      ifun = ifun+1
      write(6,1000) 'Source model '//func(ifun)
      write(6,1000) 'Flux density ',a(1)*1e3,var_a(1)*1e3,' mJy'
      if (ifun.eq.3) then
        a(2) = abs(a(2))
        a(3) = abs(a(3))
        write(6,1000) 'Inner Radius  ',a(2)*180*3600/pi,   &
     &          var_a(2)*180*3600/pi,' "'
        write(6,1000) 'Outer Radius  ',a(3)*180*3600/pi,   &
     &          var_a(3)*180*3600/pi,' "'
        jpar = 3
      elseif (ifun.lt.10) then
        a(2) = abs(a(2))
        write(6,1000) 'Radius        ',a(2)*180*3600/pi,   &
     &          var_a(2)*180*3600/pi,' "'
        jpar = 2
      else
        jpar = 1
      endif
    else
      jpar = 0
      do i=2,3
        ifun = choixf(i)+1
        write(6,1000) 'Source component '//func(ifun)
        jpar = jpar+1
        write(6,1000) 'Flux density ',   &
     &          a(jpar)*1e3,var_a(jpar)*1e3,' mJy'
        if (ifun.eq.3) then
          jpar = jpar+1
          a(jpar) = abs(a(jpar))
          write(6,1000) 'Inner Radius ',a(jpar)*180*3600/pi,   &
     &            var_a(jpar)*180*3600/pi,' "'
          jpar = jpar+1
          a(jpar) = abs(a(jpar))
          write(6,1000) 'Outer Radius ',a(jpar)*180*3600/pi,   &
     &            var_a(jpar)*180*3600/pi,' "'
        elseif (ifun.lt.10) then
          jpar = jpar+1
          a(jpar) = abs(a(jpar))
          write(6,1000) 'Radius       ',a(jpar)*180*3600/pi,   &
     &            var_a(jpar)*180*3600/pi,' "'
        endif
      enddo  ! i
    endif
    write(6,100)
    !
    do i=1,jpar
      res(2*i-1) = a(i)
      res(2*i) = var_a(i)
    enddo  ! i
    velo = x%gil%voff+x%gil%vres*(kc-x%gil%ref(1))
    write(2,*) kc,velo,(res(i),i=1,2*jpar)
    !
    write(resu(ku:),'(I3.3)') kc
    write(6,100) 'I-UV_FITC,  Creating model result '//resu(1:ku+2)
    call curve(qrange(1),qrange(2),choixf,a,var_a,nrep,resu,fact,   &
     &      ipq, ipy, ipw ,nv)
  enddo  ! kc
  call gdf_close_image (x,error)
  call gagout('S-UV_FITC,  Successful completion')
  call sysexi(1)
  !
  999   call sysexi(fatale)
  !
  100   format (1x,a)
  1001  format (11x,a,i4)
  1000  format (11x,a,f9.2,' +- ',f9.2,a)
end program uv_fit
!
subroutine load_data(ndata,visi,nx,ic,fact,ampl,aq,ar,ai,aw,   &
   &      nv,q,y,w,qmin,qmax)
  use gildas_def
  use gkernel_interfaces
  !-------------------------------------------------------------------
  ! Load Data (U,V,Visibilities,Weights,etc...)
  !
  !-------------------------------------------------------------------
  integer, intent(in) :: ndata                ! Number of visibilities
  integer, intent(in) :: nx                   ! Size of a visibility
  real, intent(in) :: visi(nx,*)              ! Visibilities
  integer, intent(in) :: ic                   ! Channel number
  real, intent(in) :: fact                    ! Radius scaling factor
  logical, intent(in) :: ampl                 ! Ampl or Real part ?
  real, intent(out) :: aq(ndata)              ! Amplitude 
  real, intent(out) :: ar(ndata)              ! Real 
  real, intent(out) :: ai(ndata)              ! Imaginary  
  real, intent(out) :: aw(ndata)              ! Weights 
  integer, intent(out) :: nv                  ! Number of good points
  real, intent(out) :: q(ndata)               ! Abscissas of visibilities
  real, intent(out) :: y(ndata)               ! Data values
  real, intent(out) :: w(ndata)               ! Weights
  real, intent(in)  :: qmin                   !
  real, intent(in)  :: qmax                   ! Range of abscissas
  ! Global
  ! Local
  integer, allocatable :: id(:)
  real, allocatable :: iw(:)
  integer :: i, j, jv, kv, jf, kf, irc, iic, iwc, ier
  logical :: error
  real :: sigma, sigma_a, rall, iall, qall, wall, xmax, xstep
  !
  real(4), parameter :: pi=3.14159265358979323
  !
  irc = 5 + 3*ic
  iic = irc + 1
  iwc = iic + 1
  !
  ! No average case: select all good data points as they come
  if (nv.eq.0) then
    write(6,*) 'I-UV_FITC,  Fitting raw data'
    jv = 0
    do i = 1,ndata
      if (visi(iwc,i).gt.0) then
        jv = jv+1
        q(jv)  = fact * sqrt(visi(1,i)**2 + visi(2,i)**2)
        if (ampl) then
          y(jv)  = sqrt(visi(irc,i)**2+visi(iic,i)**2)
          sigma = 1. / sqrt(visi(iwc,i))
          if ((y(jv) / sigma) .le. 0.5) then
            sigma_a = sigma * sqrt(2. - pi / 2.) *   &
   &                (1 + (y(jv)/2./sigma)**2)
          elseif  ((y(jv) / sigma) .ge. 2.) then
            sigma_a = sigma * (1 - (sigma/2./y(jv))**2)
          else
            sigma_a = 0.161 + 0.6156 * sigma
          endif
          !			if (visi(irc,i).lt.0) y(jv) = -y(jv)
          w(jv) = 1. / sigma_a**2
        else
          y(jv) = visi(irc,i)
          w(jv) = visi(iwc,i)
        endif
      endif
    enddo
    nv = jv
  else
    write(6,*) 'I-UV_FITC,  Averaging data before fit'
    !
    ! Average case : Load data with good points
    jv = 0
    do i = 1,ndata
      if (visi(iwc,i).gt.0) then
        jv = jv+1
        aq(jv) = sqrt(visi(1,i)**2 + visi(2,i)**2)
        ar(jv) = visi(irc,i)
        ai(jv) = visi(iic,i)
        aw(jv) = visi(iwc,i)
      endif
    enddo
    if (jv.eq.0) then
      nv = 0
      return
    endif
    !
    ! Sort data by Q Order
    allocate (id(jv), iw(jv), stat=ier)
    if (ier.ne.0) then
      call gagout('E_UV_FITC,  Memory allocation failure')
      goto 96
    endif
    error = .false.
    call gr4_trie(aq,id,jv,error)
    print *,'End tri'
    if (error) goto 96
    call gr4_sort(ar,iw,id,jv) 
    call gr4_sort(ai,iw,id,jv) 
    call gr4_sort(aw,iw,id,jv) 
    deallocate (id,iw)
    !
    ! Now bin them
    xstep = (qmax-qmin)/(nv-1)
    xmax  = qmin-0.5*xstep
    jf = 1                   ! Faux: il faut le trouver
    do j=1,jv
      if (aq(j).gt.xmax) then
        jf = j
        exit 
      endif
    enddo
    xmax  = qmin+0.5*xstep
    !
    kv = 0
    do i=1,nv
      !
      ! Compute mean values in [Xmin,Xmax[
      wall = 0.0
      qall = 0.0
      iall = 0.0
      rall = 0.0
      kf = jf
      do j=jf,jv
        if (aq(j).ge.xmax) exit 
        qall = qall+aq(j)*aw(j)
        rall = rall+ar(j)*aw(j)
        iall = iall+ai(j)*aw(j)
        wall = wall+aw(j)
        kf = j+1
      enddo
      !
      jf = kf
      if (wall.ne.0) then
        kv = kv+1
        q(kv) = qall/wall*fact
        if (ampl) then
          y(kv)  = sqrt(rall**2+iall**2)/wall
          sigma = 1. / sqrt(wall)
          if ((y(kv) / sigma) .le. 0.5) then
            sigma_a = sigma * sqrt(2. - pi / 2.) *   &
   &                (1 + (y(kv)/2./sigma)**2)
          elseif  ((y(kv) / sigma) .ge. 2.) then
            sigma_a = sigma * (1 - (sigma/2./y(kv))**2)
          else
            sigma_a = 0.161 + 0.6156 * sigma
          endif
          !			if (rall.lt.0) y(kv) = -y(kv)
          w(kv) = 1. / sigma_a**2
        else
          y(kv) = rall/wall
          w(kv) = wall
        endif
      endif
      xmax = xmax+xstep
    enddo
    ! Set NV to the number of good points
    nv = kv
  endif
  !
  ! Normalize weights by 1E6 to take into account the frequency unit
  do i=1,nv
    w(i) = 1e6*w(i)
  enddo
  return
  96    write(6,*) 'F-UV_FIT,  Cannot sort data'
  call sysexi(fatale)
end subroutine load_data
!
subroutine curve (qmin,qmax,choixf,par,spar,nrep,datafile,fact,   &
   &      q,y,w,nv)
  !-------------------------------------------------------------------
  ! Compute the Data Points representating the Chosen Model F
  !
  ! * INPUT Parameters  :
  !
  !	QMIN	: Lower Bound of Representation
  !	QMAX	: Upper Bound of Representation
  !	CHOIXF	: Vector [3] = Choice of the Model F
  !	PAR	: Vector [6] = Parameters of the Model
  !	SPAR	: Vector [6] = Sigma of the Model
  !	NREP	: Number of Data Points (Fitting Model)
  !	DATAFILE: Output file name
  !	FACT	: Meter to Wavelength conversion factor
  !-------------------------------------------------------------------
  real :: qmin                    !
  real :: qmax                    !
  integer :: choixf(3)            !
  real*8 :: par(6)                !
  real*8 :: spar(6)               !
  integer :: nrep                 !
  character(len=*) :: datafile    !
  real :: fact                    !
  integer :: nv                   !
  real :: q(nv)                   !
  real :: y(nv)                   !
  real :: w(nv)                   !
  ! Local
  integer :: i
  real :: dy(6), step_q, x, f, xw, ff, xx
  !      real*8 dpar(6)
  !
  open (unit=1,file=datafile,   &
   &      recl=255,status='UNKNOWN')
  !
  write(1,100) '! Result of UV_FIT: Number of Waves, '   &
   &      //' Function Value . . . . . .'
  write(1,101) '! ',choixf
  write(1,102) '! ',par(1),par(2),par(3)
  write(1,102) '! ',par(4),par(5),par(6)
  !
  step_q = (qmax - qmin) / (nrep - 1)
  x  = qmin-step_q
  !
  do i = 1,max(nrep,nv)
    if (i.le.nrep) then
      x = x+step_q
      xw = x*fact
      call func (choixf,par,xw,f,dy)
    endif
    !	   do j=1,6
    !	      dpar(j) = par(j)+spar(j)
    !	   enddo
    !	   call func (choixf,dpar,xw,yp,dy,abso)
    !	   do j=1,6
    !	      dpar(j) = par(j)-spar(j)
    !	   enddo
    !	   call func (choixf,dpar,xw,ym,dy,abso)
    if (i.le.nv) then
      xx = q(i)
      call func (choixf,par,xx,ff,dy)
      write(1,103) x,f,xw,q(i)/fact,y(i),1.0/sqrt(w(i)),y(i)-ff
    else
      write(1,103) x,f,xw,0,0,0,0
    endif
  enddo
  close (unit=1)
  100   format(a)
  101   format(a,3i3,' . . . . . . ')
  102   format(a,3(1x,1pg19.7),' . . . . . .')
  103   format(7(1x,1pg19.7))
end subroutine curve
!
subroutine linear(ndata,q,y,wy,choixf,a,lambda,   &
   &      var_a,chi_2,iflag)
  !-------------------------------------------------------------------
  ! Make a Least-Squares Fit to a Non Linear Discrete Function Y(Qi) with a
  ! Linearization of the Fitting Model F(Q), chosen by the user (MARQUARDT's
  ! Method).
  !
  ! Models of functions are contained in UV_FIT2.FOR : "Disk", "Ring",
  ! "Cut Parabola", "Exponential", "Gaussian", "Power -1",  "Power -2",
  ! "Power -3", "Point" and Sums of every Couple of these Functions.
  !
  !  * INPUT Parameters  :
  !
  !	NDATA	:  Number of Data Y(Q)
  !	Q	:  Vector [NDATA]  = Abscissa of Data
  !	Y	:  Vector [NDATA] = Data
  !	WY	:  Vector [NDATA] = Weights of Data
  !	CHOIXF	:  Vector [3] (Choice of the Fitting Continued Model)
  !	A	:  Vector [6] = Starting Values of the Parameters of the chosen
  !		   Model
  ! 	LAMBDA	:  Starting Value of the Parameter of Convergence
  !
  !  * OUTPUT Parameters :
  !
  !	A	:  Vector [6] = Final Values of the Parameters
  !	VAR_A	:  Vector [6] = Variances of the Final Parameters
  !	CHI_2	:  Value of the Error Function for the Final Parameters
  !	IFLAG	:  Integer showing the Accuracy of the Problem (0 = Good)
  !-------------------------------------------------------------------
  integer :: ndata                !
  real :: q(ndata)                !
  real :: y(ndata)                !
  real :: wy(ndata)               !
  integer :: choixf(3)            !
  real*8 :: a(6)                  !
  real*8 :: lambda                !
  real*8 :: var_a(6)              !
  real :: chi_2                   !
  integer :: iflag                !
  ! Local
  integer :: i, j, npar, npar1, npar2
  real :: oldchi_2, newchi_2, epsilon
  real*8 :: par(6), determ, beta(6), alpha(6,6), norm(6,6), delta_a(6)
  character(len=16) :: text(-2:2)
  !
  data text/'Not converged','Approximate','Good','Optimistic',   &
   &      'Bad'/
  !
  ! 0. Initialisations.
  ! -------------------
  epsilon = 1e-7
  !
  if (choixf(1) .eq. 0) then
    if (choixf(2) .eq. 2) then
      npar1 = 3
    elseif (choixf(2).eq.9) then
      npar1 = 1
    else
      npar1 = 2
    endif
    if (choixf(2) .eq. 2) then
      npar2 = 3
    elseif (choixf(2).eq.9) then
      npar2 = 1
    else
      npar2 = 2
    endif
    npar = npar1 + npar2
  elseif (choixf(1) .eq. 2) then
    npar = 3
  elseif (choixf(1) .eq. 9) then
    npar = 1
  else
    npar = 2
  endif
  !
  do j = 1,npar
    par(j) = a(j)
    delta_a(j) = 0.
  enddo
  if (choixf(1) .eq. 10) npar = npar-1
  !
  ! I. Compute Error CHI_2 at Starting Point.
  ! -----------------------------------------
  call chisqr(ndata,q,y,wy,par,choixf,oldchi_2)
  !
  ! II. Compute Elements of BETA and ALPHA Matrices.
  ! ------------------------------------------------
  20    write(6,1001) 'I-UV_FIT,  Chi-2 ',oldchi_2/(ndata-npar),   &
   &      (par(i),i=1,npar)
  10    call cal_alpha_beta(npar,ndata,q,y,wy,par,choixf,   &
   &      lambda,alpha,norm,beta)
  !
  ! III. Invert Matrix ALPHA.
  ! -------------------------
  call mat_inv(alpha,npar,determ)
  !
  ! IV. Compute Parameters DELTA_A(J).
  ! ----------------------------------
  call delta(npar,beta,alpha,norm,delta_a)
  !
  ! V. Compute the New Error NEWCHI_2.
  ! ----------------------------------
  do j = 1,npar
    a(j) = par(j) + delta_a(j)
  enddo
  call chisqr(ndata,q,y,wy,a,choixf,newchi_2)
  !
  ! VI. Test of Convergence.
  ! ------------------------
  if (abs(newchi_2 - oldchi_2) .lt. (1.e-2 * oldchi_2)) then
    chi_2 = min (newchi_2,oldchi_2)
    if (chi_2 .le. ((ndata - npar) / 4.)) then
      iflag = 1
    elseif  (chi_2 .ge. (4. * (ndata - npar))) then
      iflag = 2
    else
      iflag = 0
    endif
    goto 100
  elseif (newchi_2 .lt. oldchi_2) then
    lambda = lambda / 10.
    do j = 1,npar
      par(j) = a(j)
    enddo
    oldchi_2 = newchi_2
    if (lambda .gt. epsilon) goto 20
    write(6,*) 'W-UV_FIT,  Solution not converged : '   &
   &        //' LAMBDA < EPSILON'
    iflag = -1
    goto 100
  elseif  (lambda .ge. 1.e4) then
    write(6,*) 'E-UV_FIT,  No Solution : LAMBDA too large'
    iflag = -2
    goto 100
  else
    lambda = lambda * 10.
    goto 10
  endif
  100   write(6,1000) 'I-UV_FIT,  Final Chi-2 ',chi_2/(ndata-npar),   &
   &      text(iflag)
  call sigma(ndata,npar,chi_2,iflag,alpha,norm,var_a)
  !
  1000  format(1x,a,1pg10.3,' Fit is ',a)
  1001  format(1x,a,7(1x,1pg10.3))
end subroutine linear
!
subroutine chisqr(ndata,q,y,wy,par,choixf,chi_2)
  !-------------------------------------------------------------------
  ! Compute the Error of Interpolation
  !
  ! * INPUT Parameters  :
  !
  !	NDATA	: Number of Data
  !	Q	: Vector [NDATA] = Abscissas of Data
  !	Y	: Vector [NDATA] = Data
  !	WY	: Vector [NDATA] = Weights of the Data
  !	PAR	: Parameters of the Fitting Function F
  !	CHOIXF	: Vector [3] = Choice of the Model F
  !
  !  * OUTPUT Parameters :
  !
  !	CHI_2	:  Value of the Error Function for the Final Parameters.
  !-------------------------------------------------------------------
  integer :: ndata                !
  real :: q(ndata)                !
  real :: y(ndata)                !
  real :: wy(ndata)               !
  real*8 :: par(6)                !
  integer :: choixf(3)            !
  real :: chi_2                   !
  ! Local
  integer :: i
  real :: f, df(6)
  real*8 :: acc
  !
  acc = 0.
  do i = 1,ndata
    call func(choixf,par,q(i),f,df)
    acc = acc + wy(i)* (y(i) - f)**2
  enddo
  chi_2 = acc
end subroutine chisqr
!
subroutine cal_alpha_beta(npar,ndata,q,y,wy,par,choixf,lambda,alpha,norm,beta)
  use gildas_def
  use gkernel_interfaces
  !-------------------------------------------------------------------
  ! Compute Matrices ALPHA and BETA, and Values normalizing the Elements of ALPHA
  !
  ! * INPUT Parametres  :
  !
  ! 	NPAR	: Number of Parameters of the Model F
  !	NDATA	: Number of Data
  !	Q	: Vector [NDATA] = Abscissas of Data
  !	Y	: Vector [NDATA] = Data
  !	WY	: Vector [NDATA] = Weights of Data
  !	PAR	: Parameters of the Fitting Function F
  !	CHOIXF	: Vector [3] = Choice of the Model F
  ! 	LAMBDA	: Parameter weighting Diagonal Elements of ALPHA
  !
  ! * OUTPUT Parameters :
  !
  !	ALPHA	: Array [NPAR x NPAR] = computed Matrix
  ! 	NORM	: Array [NPAR x NPAR] = Values to normalize ALPHA
  !	BETA	: Vector [NPAR] = computed Vector
  !-------------------------------------------------------------------
  integer :: npar                 !
  integer :: ndata                !
  real :: q(ndata)                !
  real :: y(ndata)                !
  real :: wy(ndata)               !
  real*8 :: par(npar)             !
  integer :: choixf(3)            !
  real*8 :: lambda                !
  real*8 :: alpha(npar,npar)      !
  real*8 :: norm(npar,npar)       !
  real*8 :: beta(npar)            !
  ! Local
  integer :: i, j, k
  real :: f, df(6)
  real*8 :: bet, alph, cc
  !
  do k = 1,npar
    bet = 0.
    do j = k,npar
      alph = 0.
      do i = 1,ndata
        call func(choixf,par,q(i),f,df)
        cc = df(j)
        cc = cc * df(k)
        cc = cc * wy(i)
        alph = alph + cc
        if (j .eq. k) then
          cc = df(k)
          cc = cc * (y(i)-f)
          cc = cc * wy(i)
          bet = bet + cc
        endif
      enddo
      alpha(j,k) = alph
      alpha(k,j) = alpha(j,k)
    enddo
    beta(k) = bet
  enddo
  !
  ! Normalize Elements of Matrix ALPHA.
  ! -----------------------------------
  do j = 1,npar
    do k= j,npar
      norm(j,k) = dsqrt(alpha(j,j))*dsqrt(alpha(k,k))
      norm(k,j) = norm(j,k)
    enddo
  enddo
  !
  do j = 1,npar
    do k= j+1,npar
      if (norm(j,k) .eq. 0) then
        write(6,*)   &
   &            'F-UV_FIT,  Second derivative matrix is singular'
        print *, alpha
        call sysexi(fatale)
      endif
      alpha(j,k) = alpha(j,k) / norm(j,k)
      alpha(k,j) = alpha(j,k)
    enddo
    alpha(j,j) = 1. + lambda
  enddo
end subroutine cal_alpha_beta
!
subroutine delta(npar,beta,alpha,norm,delta_a)
  !-------------------------------------------------------------------
  ! Compute the New Parameters DELTA_A of the Problem.
  !
  ! * INPUT Parameters  :
  !
  ! 	NPAR	: Number of Parameters of the Model F
  !	BETA	: Vector [NPAR]
  !  	ALPHA	: Array [NPAR x NPAR]
  ! 	NORM	: Array [NPAR x NPAR]
  !
  ! * OUTPUT Parameters :
  !
  !	DELTA_A	: Vector [NPAR] of the New Parameters
  !-------------------------------------------------------------------
  integer :: npar                 !
  real*8 :: beta(npar)            !
  real*8 :: alpha(npar,npar)      !
  real*8 :: norm(npar,npar)       !
  real*8 :: delta_a(npar)         !
  ! Local
  integer :: j, k
  !
  do j = 1,npar
    delta_a(j) = 0.
    do k= 1,npar
      delta_a(j) = delta_a(j) + beta(k) * alpha(j,k) / norm(j,k)
    enddo
  enddo
end subroutine delta
!
subroutine sigma(ndata,npar,chi_2,iflag,alpha,norm,var_a)
  !-------------------------------------------------------------------
  ! Compute Variances of Parameters.
  !
  ! * INPUT Parameters  :
  !
  !	NDATA	: Number of Measures
  !	NPAR	: Number of Parameters
  !	CHI_2	: Value of the Fitting Error
  !	IFLAG 	: Integer showing the Accuracy of the Fitting Problem (Good = 0)
  !	ALPHA	: Array [NPAR x NPAR]
  !	NORM	: Array [NPAR x NPAR] = Values normalizing ALPHA
  !
  ! * OUTPUT Parameters :
  !
  !	VAR_A	: Vector [NPAR] of Variances of Parameters
  !-------------------------------------------------------------------
  integer :: ndata                !
  integer :: npar                 !
  real :: chi_2                   !
  integer :: iflag                !
  real*8 ::  alpha(npar,npar)     !
  real*8 ::  norm(npar,npar)      !
  real*8 ::  var_a(npar)          !
  ! Local
  integer :: j
  real :: fact
  !
  if (iflag.eq.2) then
    fact = chi_2 / (ndata-npar)
    do j = 1,npar
      var_a(j) = alpha(j,j) / norm(j,j) * fact
    enddo
  else
    do j = 1,npar
      var_a(j) = alpha(j,j) / norm(j,j)
    enddo
  endif
end subroutine sigma
!
subroutine mat_inv(a,n,d)
  use gkernel_interfaces
  use gildas_def
  !-------------------------------------------------------------------
  ! Invert a N - Dimensionnal Symmetric Matrix A (N < 10) and calculate its
  ! Determinant D.
  !
  !  * INPUT Parameters  :
  !
  !	A	: Array [N x N] = Matrix A
  !	N	: Degree of Matrix A
  !
  !  * OUTPUT Parameters :
  !
  !	A	: Array [N x N] contains the Inverse Matrix A-1
  !	D	: Determinant of INPUT Matrix A
  !-------------------------------------------------------------------
  integer :: n                    !
  real*8 :: a(n,n)                !
  real*8 :: d                     !
  ! Local
  integer :: i, j, k, l, ik(10), jk(10)
  real*8 :: amax, save_a
  !
  if (n .ge. 10) goto 100
  d = 1.
  !
  do k = 1,n
    ! I. Find Largest Element A(I,J) in Rest of Matrix A.
    ! ---------------------------------------------------
    amax = 0.
    do i = k,n
      do j = k,n
        if (abs(a(i,j)) .gt. abs(amax)) then
          amax = a(i,j)
          ik(k) = i
          jk(k) = j
        endif
      enddo
    enddo
    !
    ! II. Interchange Rows and Columns to put AMAX in A(K,K).
    ! -------------------------------------------------------
    if (amax .eq. 0) goto 100
    i = ik(k)
    if (i.ne.k) then
      do j = 1,n
        save_a = a(k,j)
        a(k,j) = a(i,j)
        a(i,j) = -save_a
      enddo
    endif
    j = jk(k)
    if (j.ne.k) then
      do i = 1,n
        save_a = a(i,k)
        a(i,k) = a(i,j)
        a(i,j) = -save_a
      enddo
    endif
    !
    ! III. Compute Elements of Inverse Matrix.
    ! ----------------------------------------
    do i = 1,n
      if (i.ne.k) then
        a(i,k) = - a(i,k) / amax
      endif
    enddo
    do i = 1,n
      do j = 1,n
        if (i.ne.k) then
          if (j.ne.k) then
            a(i,j) = a(i,j) + a(i,k)*a(k,j)
          endif
        endif
      enddo
    enddo
    do j = 1,n
      if (j.ne.k) then
        a(k,j) = a(k,j) / amax
      endif
    enddo
    a(k,k) = 1. / amax
    d = d * amax
  enddo
  !
  ! IV. Restore Ordering of Matrix.
  ! -------------------------------
  do l = 1,n
    k = n - l + 1
    j = ik(k)
    if (j.gt.k) then
      do i = 1,n
        save_a = a(i,k)
        a(i,k) = - a(i,j)
        a(i,j) = save_a
      enddo
    endif
    i = jk(k)
    if (i.gt.k) then
      do j = 1,n
        save_a = a(k,j)
        a(k,j) = - a(i,j)
        a(i,j) = save_a
      enddo
    endif
  enddo
  return
  !
  100   write(6,*) 'F-UV_FIT,  Second derivative matrix is singular'
  call sysexi(fatale)
end subroutine mat_inv
!
subroutine func (choixf,par,q,f,df)
  !-------------------------------------------------------------------
  ! Compute Chosen Function F and its Partial Derivatives at the Radius Q :
  !
  !       CHOIXF(1)                 FONCTION
  !
  !	   0             [cf. CHOIXF(2) and CHOIXF(3)]
  !	   1			    DISK
  !	   2			    RING
  !	   3 			    PARA
  !	   4                        GAUS
  !	   5                        EXPO
  !	   6			    POW1
  !	   7	                    POW2
  !	   8			    POW3
  !	   9			    POIN
  !         10                        PLAN
  !
  ! * INPUT Parameters  :
  !
  !	CHOIXF	: Vector [3] = Choice of the Function F.
  ! 	PAR	: Vector [6] of the 6 Parameters of F.
  !	Q	: Radius at which F and its Derivatives are computed.
  !
  ! * OUTPUT Parameters :
  !
  !	F	: Value of F(Q).
  !	DF	: Vector [6] of the Values of F - Partial Derivatives.
  !-------------------------------------------------------------------
  integer :: choixf(3)            !
  real*8 :: par(6)                !
  real :: q                       !
  real :: f                       !
  real :: df(6)                   !
  ! Local
  integer :: i, if, il, ipar
  !
  if (choixf(1) .eq. 0) then
    if = 2
    il = 3
  else
    if = 1
    il = 1
  endif
  !
  ipar = 1
  f = 0.
  do i = if,il
    goto (10,20,30,40,50,60,70,80,90,100) choixf(i)
    10       call disk(par(ipar),q,f,df(ipar))
    goto 200
    20       call ring(par(ipar),q,f,df(ipar))
    ipar = ipar+1
    goto 200
    30       call para(par(ipar),q,f,df(ipar))
    goto 200
    40       call gaus(par(ipar),q,f,df(ipar))
    goto 200
    50       call expo(par(ipar),q,f,df(ipar))
    goto 200
    60       call pow1(par(ipar),q,f,df(ipar))
    goto 200
    70       call pow2(par(ipar),q,f,df(ipar))
    goto 200
    80       call pow3(par(ipar),q,f,df(ipar))
    goto 200
    90       call poin(par(ipar),q,f,df(ipar))
    goto 200
    100      call plan(par(ipar),q,f,df(ipar))
    goto 200
    200      ipar = ipar + 2
  enddo
  !
end subroutine func
!
subroutine disk(par,q,f,df)
  !-------------------------------------------------------------------
  ! Compute the Visibility Function F(q), which corresponds to a " Disk "
  ! Brightness Distribution f(r) and its Partial Derivatives.
  !
  ! * INPUT Parameters  :
  !
  ! 	PAR	: Vector [2] of the 2 Parameters of F.
  !	Q	: Radius at which F and its Partial Derivatives are computed.
  !
  ! * OUTPUT Parameters :
  !
  !	F	: Value of F(q).
  !	DF	: Vector [2] of the Values of F - Partial Derivatives.
  !-------------------------------------------------------------------
  real*8 :: par(2)                !
  real :: q                       !
  real :: f                       !
  real :: df(2)                   !
  ! Global
  real*8 :: mth_bessj0,mth_bessj1
  ! Local
  real :: a, b, da, db
  real :: pi, j0, j1
  real*8 :: ksi
  !
  pi = 3.1415926535897932384626
  a = par(1)
  b = par(2)
  !
  ! I. Compute F(Q).
  ! ----------------
  if (q .eq. 0.) then
    j1 = 1.
  else
    ksi = 2. * b * pi * q
    j1 = 2. * mth_bessj1(ksi) / ksi
  endif
  f = f + a * j1
  !
  ! II. Compute First Partial Derivatives.
  ! --------------------------------------
  if (q .eq. 0.) then
    da = 1.
    db = 0.
  else
    da = j1
    j0 = mth_bessj0(ksi)
    db = a  / b * 2. * (j0 - j1)
  endif
  !
  df(1) = da
  df(2) = db
end subroutine disk
!
subroutine ring(par,q,f,df)
  !-------------------------------------------------------------------
  ! Compute the Visibility Function F(q), which corresponds to a " Ring "
  ! Brightness Distribution f(r) and its Partial Derivatives.
  !
  ! * INPUT Parameters  :
  !
  ! 	PAR	: Vector [3] of the 3 Parameters of F.
  !	Q	: Radius at which F and its Partial Derivatives are computed.
  !
  ! * OUTPUT Parameters :
  !
  !	F	: Value of F(q).
  !	DF	: Vector [3] of the Values of F - Partial Derivatives.
  !-------------------------------------------------------------------
  real*8 :: par(3)                !
  real :: q                       !
  real :: f                       !
  real :: df(3)                   !
  ! Global
  real*8 :: mth_bessj0,mth_bessj1
  ! Local
  real*8 :: a, b, c, da, db, dc
  real*8 :: pi, jb0, jc0, j1, jb1, jc1, den
  real*8 :: ksi, ksib, ksic
  !
  pi = 3.1415926535897932384626
  a = par(1)
  b = par(2)
  c = par(3)
  !
  ! I. Compute F(Q).
  ! ----------------
  if (q .eq. 0.) then
    j1 = 1.
  else
    ksi = 2. * pi * q
    ksib = b * ksi
    ksic = c * ksi
    den = ksib**2 - ksic**2
    jb1 = mth_bessj1(ksib)
    jc1 = mth_bessj1(ksic)
    j1 = 2. * (ksib*jb1 - ksic*jc1) / den
  endif
  f = f + a * j1
  !
  ! II. Compute First Partial Derivatives.
  ! --------------------------------------
  if (q .eq. 0.) then
    da = 1.
    db = 0.
    dc = 0.
  else
    da = j1
    jb0 = mth_bessj0(ksib)
    db = ksib * a / (ksib*b - ksic*c) * 2. * (jb0 - j1)
    jc0 = mth_bessj0(ksic)
    dc = - ksic * a / (ksib*b - ksic*c) * 2. * (jc0 - j1)
  endif
  !
  df(1) = da
  df(2) = db
  df(3) = dc
end subroutine ring
!
subroutine para(par,q,f,df)
  !-------------------------------------------------------------------
  ! Compute the Visibility Function F(q), which corresponds to a " Cut Parabola "
  ! Brightness Distribution f(r) and its Partial Derivatives.
  !
  ! * INPUT Parameters  :
  !
  ! 	PAR	: Vector [2] of the 2 Parameters of F.
  !	Q	: Radius at which F and its Partial Derivatives are computed.
  !
  ! * OUTPUT Parameters :
  !
  !	F	: Value of F(q).
  !	DF	: Vector [2] of the Values of F - Partial Derivatives.
  !-------------------------------------------------------------------
  real*8 :: par(2)                !
  real :: q                       !
  real :: f                       !
  real :: df(2)                   !
  ! Global
  real*8 :: mth_bessj0,mth_bessj1
  ! Local
  real :: a, b, da, db
  real :: pi, j, j0, j1
  real*8 :: ksi
  !
  pi = 3.1415926535897932384626
  a = par(1)
  b = par(2)
  !
  ! I. Compute F(Q).
  ! ----------------
  if (q .eq. 0.) then
    j = 1.
  else
    ksi = 2. * b * pi * q
    j1 = mth_bessj1(ksi)
    j0 = ksi * mth_bessj0(ksi)
    j = 8. / ksi**3 * (2. * j1 - j0)
  endif
  f = f + a * j
  !
  ! II. Compute First Partial Derivatives.
  ! --------------------------------------
  da = j
  if (q .eq. 0.) then
    db = 0.
  else
    db = 4. * j0 + (ksi**2 - 8.) * j1
    db = db * 8. * a / b / ksi**3
  endif
  !
  df(1) = da
  df(2) = db
end subroutine para
!
subroutine expo(par,q,f,df)
  !-------------------------------------------------------------------
  ! Compute the Visibility Function F(q), which corresponds to an " Exponential "
  ! Brightness Distribution f(r) and its Partial Derivatives.
  !
  ! * INPUT Parameters  :
  !
  ! 	PAR	: Vector [2] of the 2 Parameters of F.
  !	Q	: Radius at which F and its Partial Derivatives are computed.
  !
  ! * OUTPUT Parameters :
  !
  !	F	: Value of F(q).
  !	DF	: Vector [2] of the Values of F - Partial Derivatives.
  !-------------------------------------------------------------------
  real*8 :: par(2)                !
  real :: q                       !
  real :: f                       !
  real :: df(2)                   !
  ! Local
  real :: a, b, da, db
  real :: pi, j, k, ksi, y
  !
  pi = 3.1415926535897932384626
  a = par(1)
  b = par(2)
  !
  ! I. Compute F(Q).
  ! ----------------
  ksi = 2. * b * pi * q / log(2.)    ! B = 1/2 power radius RL
  k = ksi**2 + 1.
  j = 1. / k**1.5
  y = a * j
  f = f + y
  !
  ! II. Compute First Partial Derivatives.
  ! --------------------------------------
  da = j
  db = - 3. * ksi**2 * y / k / b
  !
  df(1) = da
  df(2) = db
end subroutine expo
!
subroutine gaus(par,q,f,df)
  !-------------------------------------------------------------------
  ! Compute the Visibility Function F(q), which corresponds to a " Gaussian "
  ! Brightness Distribution f(r) and its Partial Derivatives.
  !
  ! * INPUT Parameters  :
  !
  ! 	PAR	: Vector [2] of the 2 Parameters of F.
  !	Q	: Radius at which F and its Partial Derivatives are computed.
  !
  ! * OUTPUT Parameters :
  !
  !	F	: Value of F(q).
  !	DF	: Vector [2] of the Values of F - Partial Derivatives.
  !-------------------------------------------------------------------
  real*8 :: par(2)                !
  real :: q                       !
  real :: f                       !
  real :: df(2)                   !
  ! Local
  real :: a, b, da, db
  real :: pi, j, ksi2, y
  !
  pi = 3.1415926535897932384626
  a = par(1)
  b = par(2)
  !
  ! I. Compute F(Q).
  ! ----------------
  ksi2 = (b * pi * q)**2 / log(2.)
  j = exp(-ksi2)
  y = a * j
  f = f + y
  !
  ! II. Compute First Partial Derivatives.
  ! --------------------------------------
  da = j
  db = -ksi2 * y / b * 2
  !
  df(1) = da
  df(2) = db
end subroutine gaus
!
subroutine pow1(par,q,f,df)
  !-------------------------------------------------------------------
  ! Compute the Visibility Function F(q), which corresponds to a " Power -1 "
  ! Brightness Distribution f(r) and its Partial Derivatives.
  !
  ! * INPUT Parameters  :
  !
  ! 	PAR	: Vector [2] of the 2 Parameters of F.
  !	Q	: Radius at which F and its Partial Derivatives are computed.
  !
  ! * OUTPUT Parameters :
  !
  !	F	: Value of F(q).
  !	DF	: Vector [2] of the Values of F - Partial Derivatives.
  !-------------------------------------------------------------------
  real*8 :: par(2)                !
  real :: q                       !
  real :: f                       !
  real :: df(2)                   !
  ! Local
  real :: a, b, da, db
  real :: pi, j, ksi, y
  !
  pi = 3.1415926535897932384626
  a = par(1)
  b = par(2)
  !
  ! I. Compute F(Q) [Q > 0].
  ! ------------------------
  ksi = 2. * b * pi * q
  j = exp(-ksi) / ksi
  y = a * j
  f = f + y
  !
  ! II. Compute First Partial Derivatives.
  ! --------------------------------------
  da = j
  db = -y / b * (ksi + 1.)
  !
  df(1) = da
  df(2) = db
end subroutine pow1
!
subroutine pow2(par,q,f,df)
  !-------------------------------------------------------------------
  ! Compute the Visibility Function F(q), which corresponds to a " Power -2 "
  ! Brightness Distribution f(r) and its Partial Derivatives.
  !
  ! * INPUT Parameters  :
  !
  ! 	PAR	: Vector [2] of the 2 Parameters of F.
  !	Q	: Radius at which F and its Partial Derivatives are computed.
  !
  ! * OUTPUT Parameters :
  !
  !	F	: Value of F(q).
  !	DF	: Vector [2] of the Values of F - Partial Derivatives.
  !-------------------------------------------------------------------
  real*8 :: par(2)                !
  real :: q                       !
  real :: f                       !
  real :: df(2)                   !
  ! Global
  real*8 :: mth_bessk0,mth_bessk1
  ! Local
  real :: a, b, da, db
  real :: pi, k0, k1
  real*8 :: ksi
  !
  pi = 3.1415926535897932384626
  a = par(1)
  b = par(2)
  !
  ! I. Compute F(Q).
  ! ----------------
  ksi = 2.* b * pi * q
  k0 = mth_bessk0(ksi)
  f = f + a * k0
  !
  ! II. Compute First Partial Derivatives.
  ! --------------------------------------
  da = k0
  k1 = mth_bessk1(ksi)
  db = - ksi / b * a * k1
  !
  df(1) = da
  df(2) = db
end subroutine pow2
!
subroutine pow3(par,q,f,df)
  !-------------------------------------------------------------------
  ! Compute the Visibility Function F(q), which corresponds to a " Power -3 "
  ! Brightness Distribution f(r) and its Partial Derivatives.
  !
  ! * INPUT Parameters  :
  !
  ! 	PAR	: Vector [2] of the 2 Parameters of F.
  !	Q	: Radius at which F and its Partial Derivatives are computed.
  !
  ! * OUTPUT Parameters :
  !
  !	F	: Value of F(q).
  !	DF	: Vector [2] of the Values of F - Partial Derivatives.
  !-------------------------------------------------------------------
  real*8 :: par(2)                !
  real :: q                       !
  real :: f                       !
  real :: df(2)                   !
  ! Local
  real :: a, b, da, db
  real :: pi, j, ksi, y
  !
  pi = 3.1415926535897932384626
  a = par(1)
  b = par(2)
  !
  ! I. Compute F(Q).
  ! ----------------
  ksi = 2. * b * pi * q
  j = exp(-ksi)
  y = a * j
  f = f + y
  !
  ! II. Compute First Partial Derivatives.
  ! --------------------------------------
  da = j
  db = -y * ksi / b
  !
  df(1) = da
  df(2) = db
end subroutine pow3
!
subroutine poin (par,q,f,df)
  !-------------------------------------------------------------------
  ! Compute the Visibility Function F(q), which corresponds to a " Point "
  ! Brightness Distribution f(r) and its Partial Derivatives.
  !
  ! * INPUT Parameters  :
  !
  ! 	PAR	: Vector [1] of the 1 Parameters of F.
  !	Q	: Radius at which F and its Partial Derivatives are computed.
  !
  ! * OUTPUT Parameters :
  !
  !	F	: Value of F(q).
  !	DF	: Vector [1] of the Values of F - Partial Derivatives.
  !-------------------------------------------------------------------
  real*8 :: par(1)                !
  real :: q                       !
  real :: f                       !
  real :: df(1)                   !
  !
  ! I. Compute F(Q).
  ! ----------------
  f = f + par(1)
  !
  ! II. Compute First Partial Derivatives.
  ! --------------------------------------
  df(1) = 1.0
end subroutine poin
!
subroutine plan(par,q,f,df)
  !-------------------------------------------------------------------
  ! Compute the Visibility Function F(q), which corresponds to a " Disk "
  ! Brightness Distribution f(r) and its Partial Derivatives.
  !
  ! * INPUT Parameters  :
  !
  ! 	PAR	: Vector [2] of the 2 Parameters of F.
  !	Q	: Radius at which F and its Partial Derivatives are computed.
  !
  ! * OUTPUT Parameters :
  !
  !	F	: Value of F(q).
  !	DF	: Vector [2] of the Values of F - Partial Derivatives.
  !-------------------------------------------------------------------
  real*8 :: par(2)                !
  real :: q                       !
  real :: f                       !
  real :: df(2)                   !
  ! Global
  real*8 :: mth_bessj1
  ! Local
  real*8 :: a, b, da, db
  real*8 :: pi, j1
  real*8 :: ksi
  !
  pi = 3.1415926535897932384626
  a = par(1)
  b = par(2)
  !
  ! I. Compute F(Q).
  ! ----------------
  if (q .eq. 0.) then
    j1 = 1.
  else
    ksi = 2.d0 * b * pi * q
    j1 = 2.d0 * mth_bessj1(ksi) / ksi
  endif
  f = f + a * j1
  !
  ! II. Compute First Partial Derivatives.
  ! --------------------------------------
  if (q .eq. 0.) then
    da = 1.
    db = 0.
  else
    da = j1
    !         J0 = MTH_BESSJ0(KSI)
    !         DB = A  / B * 2. * (J0 - J1)
  endif
  !
  df(1) = da
  df(2) = 0.
end subroutine plan
!
!========================================================================
! Special functions
!
function mth_bessj0 (x)
  !-------------------------------------------------------------------
  ! Compute Bessel function J0
  !-------------------------------------------------------------------
  real*8 :: mth_bessj0            !
  real*8 :: x                     !
  ! Local
  real*8 :: ax,z,xx,y
  real*8 :: p1,p2,p3,p4,p5
  real*8 :: q1,q2,q3,q4,q5
  real*8 :: r1,r2,r3,r4,r5,r6
  real*8 :: s1,s2,s3,s4,s5,s6
  !
  data p1,p2,p3,p4,p5/1.d0,-.1098628627d-2,.2734510407d-4,   &
   &      -.2073370639d-5,.2093887211d-6/
  data q1,q2,q3,q4,q5/-.1562499995d-1,.1430488765d-3,   &
   &      -.6911147651d-5,.7621095161d-6,-.934945152d-7/
  data r1,r2,r3,r4,r5,r6/57568490574.d0,-13362590354.d0,   &
   &      651619640.7d0,   &
   &      -11214424.18d0,77392.33017d0,-184.9052456d0/
  data s1,s2,s3,s4,s5,s6/57568490411.d0,1029532985.d0,   &
   &      9494680.718d0,59272.64853d0,267.8532712d0,1.d0/
  !
  if (abs(x).lt.8.d0) then
    y=x*x
    mth_bessj0 = (r1+y*(r2+y*(r3+y*(r4+y*(r5+y*r6)))))   &
   &        /(s1+y*(s2+y*(s3+y*(s4+y*(s5+y*s6)))))
  else
    ax=abs(x)
    z=8.d0/ax
    y=z*z
    xx=ax-.785398164d0
    mth_bessj0 = sqrt(.636619772d0/ax)*(cos(xx)*   &
   &        (p1+y*(p2+y*(p3+y*(p4+y*p5))))   &
   &        -z*sin(xx)*(q1+y*(q2+y*(q3+y*(q4+y*q5)))))
  endif
end function mth_bessj0
!
function mth_bessj1 (x)
  !-------------------------------------------------------------------
  ! Compute Bessel function J1
  !-------------------------------------------------------------------
  real*8 :: mth_bessj1            !
  real*8 :: x                     !
  ! Local
  real*8 :: ax,z,xx
  real*8 :: y,p1,p2,p3,p4,p5,q1,q2,q3,q4,q5,r1,r2,r3,r4,r5,r6
  real*8 :: s1,s2,s3,s4,s5,s6
  data r1,r2,r3,r4,r5,r6/72362614232.d0,-7895059235.d0,   &
   &      242396853.1d0,-2972611.439d0,15704.48260d0,-30.16036606d0/
  data s1,s2,s3,s4,s5,s6/144725228442.d0,2300535178.d0,   &
   &      18583304.74d0,99447.43394d0,376.9991397d0,1.d0/
  data p1,p2,p3,p4,p5/1.d0,.183105d-2,-.3516396496d-4,   &
   &      .2457520174d-5,-.240337019d-6/
  data q1,q2,q3,q4,q5/.04687499995d0,-.2002690873d-3,   &
   &      .8449199096d-5,-.88228987d-6,.105787412d-6/
  !
  if (abs(x).lt.8.d0) then
    y=x*x
    mth_bessj1=x*(r1+y*(r2+y*(r3+y*(r4+y*(r5+y*r6)))))   &
   &        /(s1+y*(s2+y*(s3+y*(s4+y*(s5+y*s6)))))
  else
    ax=abs(x)
    z=8.d0/ax
    y=z*z
    xx=ax-2.356194491d0
    mth_bessj1=sqrt(.636619772d0/ax)*(cos(xx)*   &
   &        (p1+y*(p2+y*(p3+y*(p4+y*p5))))   &
   &        -z*sin(xx)*(q1+y*(q2+y*(q3+y*(q4+y*q5)))))   &
   &        *sign(1.d0,x)
  endif
end function mth_bessj1
!
subroutine mth_bessjn (x,j,n)
  !-------------------------------------------------------------------
  ! Compute Bessels functions from J1 to Jn
  !-------------------------------------------------------------------
  real*8 :: x                     !
  integer :: n                    !
  real*8 :: j(n)                  !
  ! Global
  real*8 :: mth_bessj1,mth_bessj
  ! Local
  integer :: i
  !
  ! Unefficient implementation, if you don't care
  j(1) = mth_bessj1(x)
  do i=2,n
    j(i) = mth_bessj(i,x)
  enddo
end subroutine mth_bessjn
!
function mth_bessk0(x)
  !-------------------------------------------------------------------
  ! Bessel function K0(X)
  !-------------------------------------------------------------------
  real*8 :: mth_bessk0            !
  real*8 :: x                     !
  ! Local
  real*8 ::  egam, g, t, xbig, xvsmal, y
  ! Data
  data xvsmal,egam/3.2d-8,5.772156649015329d-1/
  !
  ! XBIG = largest X such that  EXP(-X)/SQRT(X) .GT. MINREAL (rounded down)
  data xbig /86.4d0/
  !
  if (x.le.0.0d0) then
    mth_bessk0 = 0.0d0
  elseif (x.gt.xbig) then
    mth_bessk0 = 0.0d0
  elseif (x.gt.4.0d0) then
    !
    ! Large X
    t = 10.0d0/(1.0d0+x) - 1.0d0
    !
    ! EXPANSION (0042) EVALUATED AS Y(T)  --PRECISION 17E.18
    y=  +1.23688664769425422d+0+t*(  -1.72683652385321641d-2+   &
   &        t*(  -9.25551464765637133d-4+t*(  -9.02553345187404564d-5+   &
   &        t*(  -6.31692398333746470d-6+t*(  -7.69177622529272933d-7+   &
   &        t*(  -4.16044811174114579d-8+t*(  -9.41555321137176073d-9+   &
   &        t*( +1.75359321273580603d-10+t*( -2.22829582288833265d-10+   &
   &        t*( +3.49564293256545992d-11+t*( -1.11391758572647639d-11+   &
   &        t*( +2.85481235167705907d-12+t*( -7.31344482663931904d-13+   &
   &        t*( +2.06328892562554880d-13+t*( -1.28108310826991616d-13+   &
   &        t*( +4.43741979886551040d-14))))))))))))))))
    mth_bessk0 = dexp(-x)*y/dsqrt(x)
    !
  elseif (x.gt.2.0d0) then
    !
    ! Upper middle X
    t = x - 3.0d0
    !
    ! EXPANSION (0041) EVALUATED AS Y(T)  --PRECISION 17E.18
    y=  +6.97761598043851776d-1+t*(  -1.08801882084935132d-1+   &
   &        t*( +2.56253646031960321d-2+t*(  -6.74459607940169198d-3+   &
   &        t*( +1.87292939725962385d-3+t*(  -5.37145622971910027d-4+   &
   &        t*( +1.57451516235860573d-4+t*(  -4.68936653814896712d-5+   &
   &        t*( +1.41376509343622727d-5+t*(  -4.30373871727268511d-6+   &
   &        t*( +1.32052261058932425d-6+t*(  -4.07851207862189007d-7+   &
   &        t*( +1.26672629417567360d-7+t*(  -3.95403255713518420d-8+   &
   &        t*( +1.23923137898346852d-8+t*(  -3.88349705250555658d-9+   &
   &        t*( +1.22424982779432970d-9+t*( -4.03424607871960089d-10+   &
   &        t*(+1.28905587479980147d-10+t*( -2.97787564633235128d-11+   &
   &        t*(+9.11109430833001267d-12+t*( -7.39672783987933184d-12+   &
   &        t*(+2.43538242247537459d-12))))))))))))))))))))))
    mth_bessk0 = dexp(-x)*y
    !
  elseif (x.gt.1.0d0) then
    !
    ! Lower middle X
    t = 2.0d0*x - 3.0d0
    !
    ! EXPANSION (0040) EVALUATED AS Y(T)  --PRECISION 17E.18
    y=  +9.58210053294896496d-1+t*(  -1.42477910128828254d-1+   &
   &        t*(  +3.23582010649653009d-2+t*(  -8.27780350351692662d-3+   &
   &        t*(  +2.24709729617770471d-3+t*(  -6.32678357460594866d-4+   &
   &        t*(  +1.82652460089342789d-4+t*(  -5.37101208898441760d-5+   &
   &        t*(  +1.60185974149720562d-5+t*(  -4.83134250336922161d-6+   &
   &        t*(  +1.47055796078231691d-6+t*(  -4.51017292375200017d-7+   &
   &        t*(  +1.39217270224614153d-7+t*(  -4.32185089841834127d-8+   &
   &        t*(  +1.34790467361340101d-8+t*(  -4.20597329258249948d-9+   &
   &        t*(  +1.32069362385968867d-9+t*( -4.33326665618780914d-10+   &
   &        t*( +1.37999268074442719d-10+t*( -3.19241059198852137d-11+   &
   &        t*( +9.74410152270679245d-12+t*( -7.83738609108569293d-12+   &
   &        t*( +2.57466288575820595d-12))))))))))))))))))))))
    mth_bessk0 = dexp(-x)*y
    !
  elseif (x.gt.xvsmal) then
    t = 2.0d0*x*x - 1.0d0
    !
    ! EXPANSION (0038) EVALUATED AS G(T)  --PRECISION 17E.18
    g=  +1.12896092945412762d+0+t*( +1.32976966478338191d-1+   &
   &        t*( +4.07157485171389048d-3+t*( +5.59702338227915383d-5+   &
   &        t*( +4.34562671546158210d-7+t*( +2.16382411824721532d-9+   &
   &        t*(+7.49110736894134794d-12+t*(+1.90674197514561280d-14)   &
   &        ))))))
    !
    ! EXPANSION (0039) EVALUATED AS Y(T)  --PRECISION 17E.18
    y=  +2.61841879258687055d-1+t*( +1.52436921799395196d-1+   &
   &        t*( +6.63513979313943827d-3+t*( +1.09534292632401542d-4+   &
   &        t*( +9.57878493265929443d-7+t*( +5.19906865800665633d-9+   &
   &        t*(+1.92405264219706684d-11+t*(+5.16867886946332160d-14+   &
   &        t*(+1.05407718191360000d-16))))))))
    mth_bessk0 = -dlog(x)*g + y
    !
  else
    ! Very small X
    mth_bessk0 = -(dlog(0.5d0*x)+egam)
  endif
end function mth_bessk0
!
function mth_bessk1(x)
  !-------------------------------------------------------------------
  ! Bessel function K1(X)
  !-------------------------------------------------------------------
  real*8 :: mth_bessk1            !
  real*8 :: x                     !
  ! Local
  real*8 :: g, t, xbig, xsest, xsmall, y
  !15   DATA XSMALL /1.3D-8/
  data xsmall /7.9d-10/
  !     XBIG = LARGEST X SUCH THAT  EXP(-X)/SQRT(X) .GT. MINREAL
  !               (ROUNDED DOWN)
  !     XSEST = 1.0/MAXREAL  (ROUNDED UP)
  ! IEEE
  !R5   DATA XBIG,XSEST /707.1D0,1.2D-308/
  ! VAX
  data xbig,xsest /86.4d0,5.9d-39/
  !
  if (x.le.0.0d0) then
    mth_bessk1 = 0.0d0
  elseif (x.le.xsest) then
    mth_bessk1 = 1.0d0/xsest
  elseif (x.ge.xbig) then
    mth_bessk1 = 0.0d0
  elseif (x.le.xsmall) then
    mth_bessk1 = 1.0d0/x
  elseif (x.le.1.0d0) then
    t = 2.0d0*x*x - 1.0d0
    !
    ! EXPANSION (0046) EVALUATED AS G(T)  --PRECISION 17E.18
    g=  +5.31907865913352762d-1+t*( +3.25725988137110495d-2+   &
   &        t*( +6.71642805873498653d-4+t*( +6.95300274548206237d-6+   &
   &        t*( +4.32764823642997753d-8+t*(+1.79784792380155752d-10+   &
   &        t*(+5.33888268665658944d-13+t*(+1.18964962439910400d-15)   &
   &        ))))))
    !
    ! EXPANSION (0047) EVALUATED AS Y(T)  --PRECISION 17E.18
    y=  +3.51825828289325536d-1+t*( +4.50490442966943726d-2+   &
   &        t*( +1.20333585658219028d-3+t*( +1.44612432533006139d-5+   &
   &        t*( +9.96686689273781531d-8+t*(+4.46828628435618679d-10+   &
   &        t*(+1.40917103024514301d-12+t*(+3.29881058019865600d-15)   &
   &        ))))))
    mth_bessk1 = 1.0d0/x + x*(dlog(x)*g-y)
  elseif (x.le.2.0d0) then
    !
    ! LOWER MIDDLE X
    t = 2.0d0*x - 3.0d0
    !
    ! EXPANSION (0048) EVALUATED AS Y(T)  --PRECISION 17E.18
    y=  +1.24316587355255299d+0+t*( -2.71910714388689413d-1+   &
   &        t*( +8.20250220860693888d-2+t*( -2.62545818729427417d-2+   &
   &        t*( +8.57388087067410089d-3+t*( -2.82450787841655951d-3+   &
   &        t*( +9.34594154387642940d-4+t*( -3.10007681013626626d-4+   &
   &        t*( +1.02982746700060730d-4+t*( -3.42424912211942134d-5+   &
   &        t*( +1.13930169202553526d-5+t*( -3.79227698821142908d-6+   &
   &        t*( +1.26265578331941923d-6+t*( -4.20507152338934956d-7+   &
   &        t*( +1.40138351985185509d-7+t*( -4.66928912168020101d-8+   &
   &        t*( +1.54456653909012693d-8+t*( -5.13783508140332214d-9+   &
   &        t*( +1.82808381381205361d-9+t*(-6.15211416898895086d-10+   &
   &        t*(+1.28044023949946257d-10+t*(-4.02591066627023831d-11+   &
   &        t*(+4.27404330568767242d-11+   &
   &        t*(-1.46639291782948454d-11)))))))))))))))))))))))
    mth_bessk1 = dexp(-x)*y
  elseif (x.le.4.0d0) then
    !
    ! UPPER MIDDLE X
    t = x - 3.0d0
    !
    ! EXPANSION (0049) EVALUATED AS Y(T)  --PRECISION 17E.18
    y=  +8.06563480128786903d-1+t*( -1.60052611291327173d-1+   &
   &        t*( +4.58591528414023064d-2+t*( -1.42363136684423646d-2+   &
   &        t*( +4.55865751206724687d-3+t*( -1.48185472032688523d-3+   &
   &        t*( +4.85707174778663652d-4+t*( -1.59994873621599146d-4+   &
   &        t*( +5.28712919123131781d-5+t*( -1.75089594354079944d-5+   &
   &        t*( +5.80692311842296724d-6+t*( -1.92794586996432593d-6+   &
   &        t*( +6.40581814037398274d-7+t*( -2.12969229346310343d-7+   &
   &        t*( +7.08723366696569880d-8+t*( -2.35855618461025265d-8+   &
   &        t*( +7.79421651144832709d-9+t*( -2.59039399308009059d-9+   &
   &        t*(+9.20781685906110546d-10+t*(-3.09667392343245062d-10+   &
   &        t*(+6.44913423545894175d-11+t*(-2.02680401514735862d-11+   &
   &        t*(+2.14736751065133220d-11+   &
   &        t*(-7.36478297050421658d-12)))))))))))))))))))))))
    mth_bessk1 = dexp(-x)*y
  else
    !
    ! LARGE X
    t = 10.0d0/(1.0d0+x) - 1.0d0
    !
    ! EXPANSION (0050) EVALUATED AS Y(T)  --PRECISION 17E.18
    y=  +1.30387573604230402d+0+t*( +5.44845254318931612d-2+   &
   &        t*( +4.31639434283445364d-3+t*( +4.29973970898766831d-4+   &
   &        t*( +4.04720631528495020d-5+t*( +4.32776409784235211d-6+   &
   &        t*( +4.07563856931843484d-7+t*( +4.86651420008153956d-8+   &
   &        t*( +3.82717692121438315d-9+t*(+6.77688943857588882d-10+   &
   &        t*(+6.97075379117731379d-12+t*(+1.72026097285930936d-11+   &
   &        t*(-2.60774502020271104d-12+t*(+8.58211523713560576d-13+   &
   &        t*(-2.19287104441802752d-13+t*(+1.39321122940600320d-13+   &
   &        t*(-4.77850238111580160d-14))))))))))))))))
    mth_bessk1 = dexp(-x)*y/dsqrt(x)
  endif
end function mth_bessk1
!
!
function mth_bessj(n,x)
  !-------------------------------------------------------------------
  ! Compute Bessel function Jn, with n integer
  !-------------------------------------------------------------------
  real*8 :: mth_bessj             !
  integer :: n                    !
  real*8 :: x                     !
  ! Global
  real*8 :: mth_bessj0,mth_bessj1
  ! Local
  real*8 :: bigno,bigni,bessj
  real*8 :: tox, bjm, bj, bjp, sum
  integer :: iacc, j, m, jsum
  parameter (iacc=40,bigno=1.d10,bigni=1.d-10)
  !
  mth_bessj = 0
  if (n.lt.2) return
  tox=2.d0/x
  if (x.gt.dble(n))then
    bjm = mth_bessj0(x)
    bj  = mth_bessj1(x)
    do j=1,n-1
      bjp= j*tox*bj-bjm
      bjm= bj
      bj = bjp
    enddo
    bessj = bj
  else
    bessj = 0 ! To prevent a Gfortran compiler bug...
    m = 2*((n+int(sqrt(float(iacc*n))))/2)
    jsum=0
    sum=0.d0
    bjp=0.d0
    bj =1.d9
    do j=m,1,-1
      bjm= j*tox*bj-bjp
      bjp= bj
      bj = bjm
      if (abs(bj).gt.bigno) then
        bj = bj*bigni
        bjp = bjp*bigni
        bessj = bessj*bigni
        sum = sum*bigni
      endif
      if (jsum.ne.0) sum=sum+bj
      jsum = 1-jsum
      if (j.eq.n) bessj=bjp
    enddo
    sum = sum+sum-bj
    bessj = bessj/sum
  endif
  mth_bessj = bessj
end function mth_bessj
