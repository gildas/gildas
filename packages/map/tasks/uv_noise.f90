program p_uv_noise
  use gildas_def
  use gkernel_interfaces
  use image_def
  !---------------------------------------------------------------------
  ! GILDAS
  !	Compute exact weight for an input UV table
  !---------------------------------------------------------------------
  ! Local
  character(len=filename_length) :: uvdata
  character(len=filename_length) :: uvchannels
  character(len=12) :: ctype
  logical :: error,keep
  integer :: ishow
  !
  ! Code:
  call gildas_open
  call gildas_char('UVDATA$',uvdata)
  call gildas_char('RANGES$',uvchannels)
  call gildas_char('CTYPE$',ctype)
  call gildas_inte('PRINT$',ishow,1)
  call gildas_logi('KEEP$',keep,1)
  call gildas_close
  !
  call sub_uv_noise(uvdata,uvchannels,ctype,ishow,keep, error)
  if (error) call sysexi(fatale)
end program p_uv_noise
!
subroutine sub_uv_noise (cuvin, channels, ctype, ishow, keep, error)
  use gkernel_interfaces
  use image_def
  use gbl_message
  !---------------------------------------------------------------------
  ! GILDAS
  !	  Compute weights for an input UV table based on
  !   statistics over (many) channels
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: cuvin 
  character(len=*), intent(in) :: channels
  character(len=*), intent(in) :: ctype
  integer, intent(in) :: ishow
  logical, intent(in) :: keep  
  logical, intent(out) :: error
  !
  ! Local variables
  character(len=*), parameter :: rname='UV_NOISE'
  character(len=80) :: mess
  character(len=16) :: atype
  type (gildas) :: uvin
  integer :: ier, nblock, ib
  integer, parameter :: mranges=50
  integer :: nc(mranges), numchan, nvisi, nbad
  !
  ! Simple checks 
  error = len_trim(cuvin).eq.0
  if (error) then
    call map_message(seve%e,rname,'No input UV table name')
    return
  endif
  !
  call gildas_null (uvin, type = 'UVT')     ! Define a UVTable gildas header
  call gdf_read_gildas (uvin, cuvin, '.uvt', error, data=.false.)
  if (error) then
    call map_message(seve%e,rname,'Cannot read input UV table')
    return
  endif
  !
  ! Read range of channels
  atype = ctype
  call get_ranges('UV_NOISE',channels,atype,mranges,numchan,nc,uvin,error)
  if (error) return
  !
  ! Define blocking factor, on largest data file, usually the input one
  ! but not always...  
  call gdf_nitems('SPACE_GILDAS',nblock,uvin%gil%dim(1)) ! Visibilities at once
  nblock = min(nblock,uvin%gil%dim(2))
  ! Allocate respective space for each file
  allocate (uvin%r2d(uvin%gil%dim(1),nblock), stat=ier)
  if (ier.ne.0) then
    write(mess,*) 'Memory allocation error ',uvin%gil%dim(1), nblock
    call map_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  ! Loop over line table - The example assumes the same
  ! number of visibilities in Input and Output, which may not
  ! be true...
  uvin%blc = 0
  uvin%trc = 0
  !
  nbad = 0
  do ib = 1,uvin%gil%dim(2),nblock
    write(mess,*) ib,' / ',uvin%gil%dim(2),nblock
    call map_message(seve%d,rname,mess) 
    uvin%blc(2) = ib
    uvin%trc(2) = min(uvin%gil%dim(2),ib-1+nblock) 
    call gdf_read_data(uvin,uvin%r2d,error)
    nvisi = uvin%trc(2)-uvin%blc(2)+1
    !
    ! Here do the job
    !
    call uv_noise(uvin,nvisi,ib,numchan,nc,ishow,keep,nbad,error)
    ! call myjob (uvou, uvou%r2d, uvin, uvin%r2d) 
    !
    call gdf_write_data (uvin,uvin%r2d,error)
    if (error) return
  enddo
  !
  ! Finalize the image
  call gdf_close_image(uvin,error)
  if (error) return
  !
  write(6,*) nbad,'/',uvin%gil%nvisi,' visibilities with non conformant weights'
  !
  call map_message(seve%i,rname,'Successful completion')
end subroutine sub_uv_noise
!
subroutine uv_noise (out,nvisi,ivisi,numchan,nc,ishow,keep,nbad,error)
  use image_def
  type (gildas), intent(inout) :: out    ! Visibilities
  integer, intent(in) :: numchan         ! Number of windows 
  integer, intent(in) :: nvisi           ! Number of visibilities
  integer, intent(in) :: ivisi           ! First visibility
  integer, intent(in) :: nc(2*numchan)   ! Channel windows
  integer, intent(in) :: ishow           ! Show changes
  logical, intent(in) :: keep            ! Keep anomalous re-scale ?
  integer, intent(inout) :: nbad         ! Number of potentially "bad" visibilities
  logical, intent(out) :: error          ! Error flag
  ! Local
  integer :: i,j,k,l,fc,nd,nk,ier
  real :: rms, rmsr, rmsi, wei, reel, imag, arms, awei, scale
  real, allocatable :: original(:), ratio(:)
  integer(kind=size_length) :: ndata
  !
  nk = (nc(1)+nc(2))/2
  if (ishow.gt.0) then
    write(6,*) 'Visibility      New         Original    Valid '
  endif
  fc = out%gil%fcol-1
  error = .false.
  k = ivisi-1
  allocate(original(nvisi),ratio(nvisi),stat=ier)
  if (ier.ne.0) then
    error = .true.
    return
  endif
  !  
  do j=1,nvisi
    !
    ! Compute rms on "Good" channels
    rmsr = 0.0
    rmsi = 0.0
    nd = 0
    do i=1,out%gil%nchan
      do l=1,numchan
        if ( (i.lt.nc(2*l-1) .or. i.gt.nc(2*l)) .and.   &
       &        out%r2d(fc+3*i,j).gt.0)  then
          reel = out%r2d(fc+3*i-2,j)
          imag = out%r2d(fc+3*i-1,j)
          rmsr = rmsr+reel**2
          rmsi = rmsi+imag**2
          nd = nd+1
        endif
      enddo
    enddo
    !
    if (nd.eq.0) cycle
    !
    ! This is the weight on the REAL or IMAGinary part
    k = k+1
    rms = min(rmsr,rmsi)
    if (ishow.gt.0) then
      if (mod(k,ishow).eq.1) then
        arms = sqrt(rms/float(nd-1))
        awei = 1.0/sqrt(out%r2d(fc+3*nk,j)*1e6) 
        write(6,*) j+ivisi-1,arms,awei,nd ! ,out%gil%nvisi
      endif
    endif
    original(j) = out%r2d(fc+3*nk,j)
    ratio(j) = float(nd-1)/rms*1e-6
    ratio(j) = ratio(j)/original(j)
    !
  enddo
  !
  ndata = nvisi
  call gr4_median(ratio,ndata,0.0,-1.0,scale,error)
  if (error) return
  write(6,*) 'Median rescale is ',scale
  !
  do j=1,nvisi
    if (ratio(j).gt.3*scale) then
      if (keep) then
        wei = scale*original(j)
      else
        wei = -scale*original(j)
      endif
      write(6,'(i8,f10.1,f4.0,f4.0,1pg10.3)') j+ivisi-1, &
      &   sqrt(out%r2d(1,j)**2+out%r2d(2,j)**2),out%r2d(6,j),out%r2d(7,j),ratio(j)
      nbad = nbad+1
    else
      wei = scale*original(j)
    endif
    !
    do i=1,out%gil%nchan
      if (out%r2d(fc+3*i,j).gt.0) then
        out%r2d(fc+3*i,j) = wei
      else if (out%r2d(fc+3*i,j).lt.0) then
        out%r2d(fc+3*i,j) = -wei      
      endif
    enddo
  enddo 
end subroutine uv_noise
!
subroutine get_ranges(pname,uvchannel,ctype,mranges,numchan,nc,y,error)
  use image_def
  use gbl_message
  use gkernel_interfaces
  !
  character(len=*), intent(in) :: pname
  character(len=*), intent(in) :: uvchannel
  character(len=*), intent(inout) :: ctype
  integer, intent(in) :: mranges
  integer, intent(out) :: numchan
  integer, intent(out) :: nc(mranges)
  type(gildas), intent(in) :: y
  logical, intent(out) :: error
  !
  integer(4) :: ier, i, ntmp
  real(8), parameter :: rnone=-1D9 ! Impossible velocity, frequency or channel number 
  real(kind=8) :: drange(mranges)
  integer(kind=4), parameter :: mtype=3
  integer :: itype
  character(len=12) :: types(mtype),mytype
  data types /'CHANNEL','VELOCITY','FREQUENCY'/
  !
  call sic_upper(ctype)
  error = .false.
  call sic_ambigs(pname,ctype,mytype,itype,types,mtype,error)
  if (error)  return
  !
  !
  drange = rnone   ! Impossible velocity, frequency or channel number 
  nc = 0                         ! This initialization is essential...
  read (uvchannel,*,iostat=ier) drange
  if (ier.gt.0) then
    call putios('E-'//pname//',  ',ier) 
    write(6,*) uvchannel
    error = .true.
    return
  endif
  ! 
  numchan = 0
  i = 2
  do while (i.lt.mranges)
    if (drange(i).ne.rnone.and.drange(i-1).ne.rnone) numchan = numchan+2
    i = i+2
  enddo
  !
  ! Convert to channels
  !
  if (numchan.eq.0) then
    numchan = 2
    nc(1) = 1
    nc(2) = y%gil%nchan 
  else if (mytype.eq.'CHANNEL') then
    nc(1:numchan) = nint(drange(1:numchan))
    do i=1,numchan
      if (nc(i).lt.0) nc(i) = y%gil%nchan+nc(i)
    enddo
  else if (mytype.eq.'VELOCITY') then
    ! drange = nc - y%gil%ref(y%gil%faxi) ) * y%gil%vres + y%gil%voff 
    nc(1:numchan) = (drange(1:numchan) - y%gil%voff) / y%gil%vres + y%gil%ref(y%gil%faxi)
  else if (mytype.eq.'FREQUENCY') then
    ! drange = nc - y%gil%ref(y%gil%faxi) ) * y%gil%vres + y%gil%voff 
    nc(1:numchan) = (drange(1:numchan) - y%gil%freq) / y%gil%fres + y%gil%ref(y%gil%faxi)
  else
    call map_message(seve%e,pname,'Type of value '''//trim(mytype)//''' not supported')
    error = .true.
    return
  endif
  !
  do i=2,numchan,2
    if (nc(i).lt.nc(i-1)) then
      ntmp = nc(i)
      nc(i) = nc(i-1)
      nc(i-1) = ntmp
    endif
  enddo
end subroutine get_ranges
