module maptasks_interfaces_none
  interface
    subroutine get_weightmode(task,mode,error)
      !---------------------------------------------------------------------
      !
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: task
      character(len=*), intent(inout) :: mode
      logical, intent(out) :: error
    end subroutine get_weightmode
  end interface
  !
  interface
    subroutine get_uvmap_par (task,uvdata,name,map,error)
      !
      ! UV_INVERT / UV_MAP
      !    Define mapping parameters
      !
      use phys_const
      use uvmap_def_task
      !
      character(len=*), intent(in) :: task
      character(len=*), intent(out) :: uvdata
      character(len=*), intent(out) :: name
      type (par_uvmap), intent(inout) :: map
      logical, intent(out) :: error
    end subroutine get_uvmap_par
  end interface
  !
  interface
    subroutine t_doweig (nv,uu,vv,wuv,unif,wm,error)
      !----------------------------------------------------------------------
      ! UV_INVERT / UV_MAP
      !   Compute weights of the visibility points.
      !----------------------------------------------------------------------
      use gildas_def
      integer, intent(in) :: nv  ! number of values
      real, intent(in) :: uu(nv)     ! U values
      real, intent(in) :: vv(nv)     ! V values
      real, intent(inout) :: wuv(nv) ! Weights
      real, intent(in) :: unif   ! uniform cell size in Meters
      real, intent(inout) :: wm      ! on input: % of uniformity
      logical, intent(out) :: error
    end subroutine t_doweig
  end interface
  !
  interface
    subroutine t_doweig_quick (nv,uu,vv,wuv,unif,we,wm,mv,umin,umax,  &
      vmin,vmax,nbcv,error)
      !----------------------------------------------------------------------
      ! UV_INVERT / UV_MAP
      !   Compute weights of the visibility points.
      !     Quick version using sub-grids
      !----------------------------------------------------------------------
      integer, intent(in) :: nv  ! number of values
      real, intent(in) :: uu(nv)     ! U values
      real, intent(in) :: vv(nv)     ! V values
      real, intent(inout) :: wuv(nv) ! Weights
      real, intent(in) :: unif   ! uniform cell size in Meters
      real, intent(in) :: wm         ! Weight scale factor
      real, intent(out) :: we(nv)
      integer, intent(in) :: mv
      integer, intent(in) :: nbcv    ! sub-grid scale factor
      real, intent(in) :: umin
      real, intent(in) :: umax
      real, intent(in) :: vmin
      real, intent(in) :: vmax
      logical, intent(out) :: error
    end subroutine t_doweig_quick
  end interface
  !
  interface
    subroutine t_dotaper(nv,uu,vv,ww,taper)
      !
      ! UV_INVERT / UV_MAP
      !    Apply taper
      !
      use phys_const
      integer, intent(in) :: nv      ! Number of UV points
      real, intent(in) :: uu(nv)     ! U values
      real, intent(in) :: vv(nv)     ! V values
      real, intent(inout) :: ww(nv)  ! Weights
      real, intent(in) :: taper(4)   ! Taper parameters
    end subroutine t_dotaper
  end interface
  !
  interface
    subroutine t_channel(huv,map)
      use gildas_def
      use image_def
      use uvmap_def_task
      !----------------------------------------------------------
      ! UV_INVERT / UV_MAP
      !    Setup the channel associated parameters
      !----------------------------------------------------------
      type (gildas), intent(in) :: huv        ! UV header
      type (par_uvmap), intent(inout) :: map  ! Imaging parameters
    end subroutine t_channel
  end interface
  !
  interface
    subroutine t_map(task,map,huv,uvmin,uvmax,conv)
      !----------------------------------------------------------
      ! UV_INVERT / UV_MAP
      !    Setup the image plane associated parameters
      !----------------------------------------------------------
      use gildas_def
      use image_def
      use uvmap_def_task
      use phys_const
      !
      character(len=*), intent(in) :: task
      type (par_uvmap), intent(inout) :: map
      type (gildas), intent(in) :: huv
      real, intent(inout) :: uvmin  ! Minimum UV length (in meters on input)
      real, intent(inout) :: uvmax  ! Maximum UV length (in Wavelength on output)
      type (gridding), intent(out) :: conv
    end subroutine t_map
  end interface
  !
  interface
    subroutine t_setbeam(huv,hbeam,map,ndim)
      !-----------------------------------------------------
      ! UV_INVERT / UV_MAP
      !     Set Beam Header
      !-----------------------------------------------------
      use image_def
      use uvmap_def_task
      type (par_uvmap), intent(in) :: map
      type (gildas), intent(in), target :: huv
      type (gildas), intent(inout), target :: hbeam
      integer, intent(in) :: ndim  ! Number of dimensions
    end subroutine t_setbeam
  end interface
  !
  interface
    subroutine t_setdirty(huv,hdirty,map,wall)
      use image_def
      use uvmap_def_task
      !-----------------------------------------------------
      ! UV_INVERT / UV_MAP
      !     Set Dirty Header
      !-----------------------------------------------------
      type (par_uvmap), intent(in) :: map    ! Imaging parameters
      type (gildas), intent(in), target :: huv        ! UV Header
      type (gildas), intent(inout), target :: hdirty  ! Dirty Header
      real, intent(in) :: wall    ! Theoretical noise
    end subroutine t_setdirty
  end interface
  !
  interface
    subroutine do_mapslow(np,nv,visi,we,lc,   &
         &    freq,nc,nx,ny,map,beam,mapcox,mapcoy)
      use phys_const
      integer, intent(in) :: np                     ! Size of a visibility
      integer, intent(in) :: nv                     ! Number of visibilities
      real, intent(in) :: visi(np,nv)               ! Visibilities UVT
      real, intent(in) :: we(nv)                    ! Weights
      integer, intent(in) :: lc                     !
      real(8), intent(in) :: freq                   ! Observing frequency
      integer, intent(in) :: nc                     ! Number of channels
      integer, intent(in) :: nx                     ! Number of pixels in X
      integer, intent(in) :: ny                     ! Number of pixels in Y
      real, intent(out) :: map(nc,nx,ny)            ! Dirty cube
      real, intent(out) :: beam(nx,ny)              ! Dirty beam
      real, intent(in) :: mapcox(nx)                ! X coordinates
      real, intent(in) :: mapcoy(ny)                ! Y coordinates
    end subroutine do_mapslow
  end interface
  !
  interface
    subroutine t_uvsort (np,nv,vin,vout,inxy,incs,uvmax,uvmin,sort,error)
      use gildas_def 
      use gbl_message
      use uv_rotate_shift_and_sort_tool, only: loaduv,sortuv,chksuv
      !----------------------------------------------------------------------
      ! UV_MAP
      !     Rotate, Shift and Sort a UV table for map making
      !     Differential precession should have been applied before.
      !----------------------------------------------------------------------
      !
      integer, intent(in) :: np                   ! Size of a visibility
      integer, intent(in) :: nv                   ! Number of visibilities
      real, intent(in) :: vin(np,nv)              ! Input visibilities
      real, intent(out) :: vout(np,nv)            ! Output visibilities
      real(8), intent(in) :: inxy(2)              ! Phase shift
      real(8), intent(in) :: incs(2)              ! Frame Rotation
      real, intent(out) :: uvmax                  ! Max UV value
      real, intent(out) :: uvmin                  ! Min UV value
      logical, intent(inout) :: sort              ! Data has been sorted
      logical, intent(out) :: error
    end subroutine t_uvsort
  end interface
  !
  interface
    subroutine mx_uvmap (u,b,m,grid,mapu,mapv,work,sblock,cpu0,rms)
      use gildas_def
      use image_def
      use gbl_format
      use mx_parameters
      use uvstat_tool, only: prnoise
      !---------------------------------------------------------------------
      ! MX:
      ! UVMAP Compute a map from a CLIC UV Sorted Table
      ! by Gridding and Fast Fourier Transform, with
      ! one single beam for all channels.
      !
      ! Input : (in X)
      ! a precessed UV table, sorted in V, ordered in
      ! (U,V,W,D,T,iant,jant,nchan(real,imag,weig))
      ! Output :
      ! a beam image (in B)
      ! a VLM cube   (in Y)
      ! Work space :
      ! a  VLM complex Fourier cube (first V value is for beam)
      ! a  complex plane
      ! Save results
      ! The grid correction GRID
      ! The UV coordinates  MAPU, MAPV
      ! The UV weight array WE_ADDR
      !---------------------------------------------------------------------
      type(gildas), intent(inout) :: u,b,m    !
      real :: grid(map_size(1),map_size(2))   !
      real :: mapu(map_size(1))                         !
      real :: mapv(map_size(2))                         !
      real :: work(*)                         !
      integer(4) :: sblock                    !
      real :: cpu0                            !
      real :: rms                             !
    end subroutine mx_uvmap
  end interface
  !
  interface
    subroutine mx_uvsort (x,y,uvtable,new,shift,uvmax,uvmin,error)
      use gildas_def
      use gbl_format
      use image_def
      use uv_shift, only: uv_shift_header
      use uv_rotate_shift_and_sort_tool, only: uvsort_uv
      !---------------------------------------------------------------------
      ! GILDAS
      !   Sort an input UV table
      !---------------------------------------------------------------------
      type(gildas), intent(inout) :: x            ! Unsorted UV table
      type(gildas), intent(inout) :: y            ! Sorted UV table
      character(len=*), intent(in) :: uvtable     ! Table name
      real(8), intent(inout) :: new(3)            ! New phase center
      logical, intent(inout) :: shift             ! Shift phase center ?
      real(4), intent(inout) :: uvmax             !
      real(4), intent(inout) :: uvmin             !
      logical, intent(out) :: error               ! Error flag
    end subroutine mx_uvsort
  end interface
  !
  interface
    subroutine mx_local_clean(hu,hb,hr,hc,hcct,grid,mapx,mapy,work,sblock, cpu0)
      use gildas_def
      use image_def
      use cct_types
      use mx_parameters
      use clean_beam_tool, only: find_sidelobe
      use minmax_tool, only: maxmap
      !---------------------------------------------------------------------
      ! GILDAS: CLEAN Internal routine
      ! Implementation of MX CLEAN deconvolution algorithm.
      !---------------------------------------------------------------------
      type(gildas), intent(in) :: hu    ! UV Data  Header
      type(gildas), intent(in) :: hb    ! Beam     Header
      type(gildas), intent(in) :: hr    ! Residual Header
      type(gildas), intent(in) :: hcct  ! Clean Component Table Header
      type(gildas), intent(in) :: hc    ! Clean    Header
      real :: grid(hb%gil%dim(1),hb%gil%dim(2))     ! Gridding correction
      real :: mapx(hb%gil%dim(1))                   ! X coordinates
      real :: mapy(hb%gil%dim(2))                   ! Y coordinates
      real :: work(*)                   ! FFT Work array
      integer :: sblock                 ! Blocking factor
      real :: cpu0                      ! Starting CPU
    end subroutine mx_local_clean
  end interface
  !
  interface
    subroutine mx_make_clean (clean,nx,ny,ft,bmaj,bmin,pa,   &
         &    xinc,yinc,compon,nc,work,resi)
      use mapping_gaussian_tool
      !---------------------------------------------------------------------
      ! GILDAS: CLEAN   Internal routine
      ! Convolve source list into residual map using the Fourier method.
      !---------------------------------------------------------------------
      integer :: nx                     !
      integer :: ny                     !
      real :: clean(nx,ny)              !
      complex :: ft(nx,ny)              !
      real :: bmaj                      !
      real :: bmin                      !
      real :: pa                        !
      real :: xinc                      !
      real :: yinc                      !
      integer :: nc                     !
      real :: compon(3,nc)              !
      real :: work(*)                   !
      real :: resi(nx,ny)               !
    end subroutine mx_make_clean
  end interface
  !
  interface
    subroutine mx_loadxy (x,y,mapux,nx,mapvy,ny,mcol)
      use gildas_def
      use image_def
      type(gildas) :: x,y               !
      integer :: nx                     !
      real :: mapux(nx)                 !
      integer :: ny                     !
      real :: mapvy(ny)                 !
      integer :: mcol(2)                !
    end subroutine mx_loadxy
  end interface
  !
  interface
    subroutine mx_major (u,beam,residu,nx,ny, nz,   &
         &    fwork,wclean,mclean,box,maxiter,compon,niter,   &
         &    grid, mapu, mapv, mapx, mapy, wfft, fft, sblock, cpu0)
      use gildas_def
      use image_def
      use mx_parameters
      use cct_types
      use minmax_tool, only: maxmap
      !---------------------------------------------------------------------
      ! CLEAN
      !	Major cycle loop according to MX idea
      !---------------------------------------------------------------------
      type(gildas) :: u                 !
      integer :: nx                     !
      integer :: ny                     !
      real ::    beam(nx,ny)            !
      integer :: nz                     !
      real ::    residu(nx,ny,nz)       !
      complex :: fwork(nx,ny)           ! Work space for T.F.
      integer :: mclean                 !
      type(cct_par) :: wclean(mclean)   ! Work space for clean components
      integer :: box(4)                 ! Cleaning box
      integer :: maxiter                ! Maximum number of clean components
      real ::  compon(3,maxiter,nz)     ! Clean component table
      integer :: niter(nz)              ! Number of Clean components
      real ::    grid(nx,ny)            ! Grid correction
      real :: mapu(nx)                  ! Coordinates of UV grid
      real :: mapv(ny)                  ! Coordinates of UV grid
      real :: mapx(nx)                  ! Coordinates of XY map in 1/m
      real :: mapy(ny)                  ! Coordinates of XY map in 1/m
      real :: wfft(*)                   ! FFT work space
      complex :: fft(nz+1,nx,ny)        ! FFTs of planes
      integer :: sblock                 !
      real :: cpu0                      !
    end subroutine mx_major
  end interface
  !
  interface
    subroutine mx_uvsub_loc(nx,ny,mapx,mapy,wclean,nclean,flux,jc,nv,visi,io)
      use cct_types
      !-------------------------------------------------------------------
      ! GILDAS:	CLEAN 	Internal routine
      !	Subtract last major cycle components from residual map.
      !-------------------------------------------------------------------
      integer :: nx                   !
      integer :: ny                   !
      real :: mapx(nx)                !
      real :: mapy(ny)                !
      integer :: nclean               !
      type(cct_par) :: wclean(nclean) !
      real :: flux                    !
      integer :: jc                   !
      integer :: nv                   !
      real :: visi(jc,nv)             !
      integer :: io                   !
    end subroutine mx_uvsub_loc
  end interface
  !
  interface
    subroutine mx_minor (wclean,nbpoint,   &
        &      beam,nx,ny,ixbeam,iybeam,ixpatch,iypatch,   &
        &      gain,maxiter,clarkmin,limite,converge,check,   &
        &      compon,niter)
      use cct_types
      !-------------------------------------------------------------------
      ! MX:
      !     CLEAN    Internal routine
      !        B.Clark minor cycles
      !     Deconvolve as in standard clean a list of NBPOINT points
      !     selected in the map until the residuals is less than CLARKMIN
      !-------------------------------------------------------------------
      type(cct_par) :: wclean(*)      ! Clean component
      integer :: nbpoint              ! nombre de points retenus
      integer :: nx                   ! dimension et centre du beam
      integer :: ny                   ! dimension et centre du beam
      real :: beam(nx,ny)             ! beam
      integer :: ixbeam               ! dimension et centre du beam
      integer :: iybeam               ! dimension et centre du beam
      integer :: ixpatch              ! rayon utile du Beam
      integer :: iypatch              ! rayon utile du Beam
      real :: gain                    ! gain de clean
      integer :: maxiter              ! nombre max d'iteration
      real :: clarkmin                ! borne d'arret de clean
      real :: limite                  ! borne d'arret de clean
    !  real :: cclean(*)               ! valeur des composantes
      logical :: converge             ! Controle de la convergence
      logical :: check                ! ibid.
      real :: compon(3,maxiter)       ! Effective clean components
      integer :: niter                ! Number of clean components
    end subroutine mx_minor
  end interface
  !
  interface
    subroutine mx_get_parameters(nx,ny)
      use gildas_def
      use mx_parameters
      use clean_tool, only: check_box
      !---------------------------------------------------------------------
      ! GILDAS:	MX Internal routine
      !	Retrieves input parameters for MX Uvmap & Clean
      !---------------------------------------------------------------------
      integer, intent(out) :: nx,ny
    end subroutine mx_get_parameters
  end interface
  !
  interface
    subroutine mx_open_clean(hd,hc,hcct,rms,error)
      use gildas_def
      use image_def
      use gbl_format
      use mx_parameters
      !---------------------------------------------------------------------
      ! GILDAS:	MX routine
      !	Open missing input maps
      !	Dirty beam has been copied into B area
      !	Clean Component Table created in C area, after creation in Z.
      !	Residual map is in Y, originally loaded with dirty map
      !	Cleaned map in Z, originally undefined
      !---------------------------------------------------------------------
      type(gildas), intent(in) :: hd
      type(gildas), intent(inout) :: hc,hcct
      real, intent(in) :: rms
      logical, intent(out) :: error
    end subroutine mx_open_clean
  end interface
  !
  interface
    subroutine mx_fit_beam(hb,error)
      use image_def
      use mx_parameters
      use fit_beam_tool
      type(gildas), intent(inout) :: hb
      logical, intent(out) :: error
    end subroutine mx_fit_beam
  end interface
  !
end module maptasks_interfaces_none
