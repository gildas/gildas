!
! Definitions used by UV_INVERT / UV_MAP
!

module uvmap_def_task
  type par_uvmap
     !! MAP_CELL(2),UV_CELL(2),MAP_SIZE(2),WCOL,MCOL(2),WCOL(2)
     real taper(4)    ! UV taper
     character(len=4) mode  ! Weighting mode
     real field(2)     ! Field of view
     integer size(2)   ! Image size
     real xycell(2)    ! Image cell
     real uniform(2)   ! Robust weighting parameter
     real support(2)   ! Support size in meters
     real uvcell(2)    ! Cell size in meters
     integer channels(4)  ! First, Last, Weight and Beam channels
     integer ctype     ! Convolution type
     integer beam      ! One beam or one per channel
     logical shift     ! Shift phase center
     logical blocked   ! Use blocking factor, or not (Debug)
     character(len=16) ra_c
     character(len=16) dec_c
     real(kind=8) new(3)     ! New position and angle of shift/rotation
     real(kind=8) off(3)     ! Offset and angle of shift/rotation
     real(kind=8) xy(2)      ! Phase rotation ! R*4 for SortUV
     real(kind=8) cs(2)      ! Angle offsets  ! R*4 for LoadUV
     real(kind=8) freq       ! Frequency for U,V values
     integer uvcode    ! UVT or TUV order ?
  end type
  !
  type gridding
     real ubias            ! Bias of U coordinates
     real ubuff(4096)      ! Grid of values of the convolution
     real vbias            ! Bias of V coordinates
     real vbuff(4096)      ! Grid of values of the convolution
  end type gridding
  integer, parameter :: code_uvt=1  ! This MUST NOT be changed
  integer, parameter :: code_tuv=2  ! This MUST NOT be changed
end module uvmap_def_task
!
module mx_parameters
  use uvmap_def_task
  use gildas_def
!
!------------------------------------------------------------------------------
! Parameters of CLEAN deconvolution.
!------------------------------------------------------------------------------
      character(len=filename_length) :: map_name, &
     &name_dirty,                                 &
     &name_beam,                                  &
     &name_residual,                              &
     &name_list,                                  &
     &name_clean,                                 &
     &uv_table
!
      integer max_iter,                           &
     &n_iter,                                     &
     &c_blc(4),                                   &
     &c_trc(4),                                   &
     &ix_beam,iy_beam,                            &
     &ix_patch,iy_patch
      real(4)             &
     &gain_loop,          &
     &frac_res,           &
     &abs_res,            &
     &major_axis,         &
     &minor_axis,         &
     &pos_angle,          &
     &beam_max,           &
     &beam_min,           &
     &beam_gain,          &
     &smooth_ratio
      logical             &
     &keep_cleaning
      integer cshift(3)
!
! Image parameters
! Cloned from uvmap_def with some name changes
     !! UV_TAPER(4), MAP_CELL(2),UV_CELL(2),MAP_SIZE(2),WCOL,MCOL(2),WCOL(2)
     real uv_taper(4)  ! taper(4)   ! UV taper
     character(len=4) mode  ! Weighting mode
     real field(2)     ! Field of view
     integer map_size(2) ! size(2)   ! Image size
     real map_cell(2)  ! xycell(2)    ! Image cell
     real uniform(2)   ! Robust weighting parameter
     real support(2)   ! Support size in meters
     real uv_cell(2)   ! uvcell(2)    ! Cell size in meters
     integer mcol(2 )  ! First, Last channels
     integer wcol      ! Weight channels
     !! integer channels(3)  ! First, Last and Weight channels
     integer ctype     ! Convolution type
     logical one_beam      ! One beam or one per channel
     logical shift     ! Shift phase center
     logical blocked   ! Use blocking factor, or not (Debug)
     character(len=16) ra_c
     character(len=16) dec_c
     real(kind=8) new(3)     ! New position and angle of shift/rotation
     real(kind=8) off(3)     ! Offset and angle of shift/rotation
     real(kind=8) xy(2)      ! Phase rotation ! R*4 for SortUV
     real(kind=8) cs(2)      ! Angle offsets  ! R*4 for LoadUV
     real(kind=8) freq       ! Frequency for U,V values
     integer uvcode    ! UVT or TUV order ?
!
    real(8), parameter :: epsilon=1.d-8
    !
    ! SAVE them to avoid losing them on return of allocation routine
    real, allocatable, save :: weight(:)
    real, allocatable, save :: vcoor (:)
    complex, allocatable, save :: map_tf (:,:,:)
    !
    type (gridding) :: conv
end module mx_parameters
