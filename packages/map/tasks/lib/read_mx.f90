  !
subroutine mx_get_parameters(nx,ny)
  use gildas_def
  use gkernel_interfaces
  use mx_parameters
  use clean_tool, only: check_box
  !---------------------------------------------------------------------
  ! GILDAS:	MX Internal routine
  !	Retrieves input parameters for MX Uvmap & Clean
  !---------------------------------------------------------------------
  integer, intent(out) :: nx,ny
  ! Global
  real(8), parameter :: pi=3.141592653589793d0
  ! Local
  character(len=4) :: weight_mode
  logical :: error
  !
  call gildas_open
  call gildas_char('UV_TABLE$',uv_table)
  call gildas_char('MAP_NAME$',map_name)
  !
  ! UVMAP part
  call gildas_real('UV_TAPER$',uv_taper,4)
  uv_taper(3) = 0.0
  call gildas_char('WEIGHT_MODE$',weight_mode)
  call gildas_inte('MAP_SIZE$',map_size,2)
  call gildas_real('MAP_CELL$',map_cell,2)
  call gildas_real('UV_CELL$',uniform,2)
  call gildas_inte('WCOL$',wcol,1)
  call gildas_inte('MCOL$',mcol,2)
  call gildas_inte('CONVOLUTION$',ctype,1)
  call gildas_logi('UV_SHIFT$',shift,1)
  if (shift) then
    call gildas_char('RA_CENTER$',ra_c)
    call gildas_char('DEC_CENTER$',dec_c)
    call gildas_dble('ANGLE$',new(3),1)
  endif
  !
  ! Clark Clean Part
  call gildas_real('GAIN$',gain_loop,1)
  call gildas_inte('NITER$',max_iter,1)
  call gildas_real('FRES$',frac_res,1)
  call gildas_real('ARES$',abs_res,1)
  call gildas_inte('BLC$',c_blc,4)
  call gildas_inte('TRC$',c_trc,4)
  call gildas_logi('KEEP$',keep_cleaning,1)
  call gildas_real('MAJOR$',major_axis,1)
  call gildas_real('MINOR$',minor_axis,1)
  call gildas_real('PA$',pos_angle,1)
  call gildas_inte('BEAM_PATCH$',ix_patch,2)
  !
  call gildas_close
  call get_weightmode('MX',weight_mode,error)
  if (error) goto 99
  !
  ! Redefine some parameters
  if (power_of_two(map_size(1)).le.0 .or. power_of_two(map_size(2)).le.0) then
    call gagout('E-MX,  Only powers of two allowed')
    goto 99
  endif
  if (any(map_cell.eq.0)) then
    call gagout('E-MX,  Specify map cell')
    goto 99
  endif  
  !
  if (shift) then
    call sic_decode(ra_c,new(1),24,error)
    if (error) goto 98
    call sic_decode(dec_c,new(2),360,error)
    if (error) goto 98
    new(3) = new(3)*pi/180.0d0
  endif
  !
  if (weight_mode.eq.'NATU') uniform(1) = -1.0
  map_cell(1) = map_cell(1)*pi/180.0/3600.0   ! In radians
  map_cell(2) = map_cell(2)*pi/180.0/3600.0   ! In radians
  !
  if (ctype.eq.0) ctype = 5
  !
  ! Check cleaning box
  call check_box(map_size(1),map_size(2),c_blc,c_trc)
  call gagout('I-MX,  Parameters read successfully')
  nx = map_size(1)
  ny = map_size(2)
  return
  98    call gagout('E-MX,  Input conversion error on phase center')
  99    call sysexi(fatale)
end subroutine mx_get_parameters
!
subroutine mx_open_clean(hd,hc,hcct,rms,error)
  use gildas_def
  use image_def
  use gbl_format
  use gkernel_interfaces
  use mx_parameters
  !---------------------------------------------------------------------
  ! GILDAS:	MX routine
  !	Open missing input maps
  !	Dirty beam has been copied into B area
  !	Clean Component Table created in C area, after creation in Z.
  !	Residual map is in Y, originally loaded with dirty map
  !	Cleaned map in Z, originally undefined
  !---------------------------------------------------------------------
  type(gildas), intent(in) :: hd
  type(gildas), intent(inout) :: hc,hcct
  real, intent(in) :: rms
  logical, intent(out) :: error
  ! Global
  real(8), parameter :: pi=3.141592653589793d0 
  ! Local
  integer :: ier
  character(len=filename_length) :: name
  !
  error = .false.
  !
  call gildas_null (hcct)
  call gdf_copy_header (hd, hcct, error)
  ! Create Clean Component Table 
  hcct%gil%dim(1) = 3
  hcct%gil%dim(2) = max_iter
  !	hcct%gil%dim(3) and hcct%gil%dim(4) as before
  name = map_name
  call sic_parsef(name,hcct%file,' ','.cct')
  hcct%char%code(1) = 'IJV'
  hcct%char%code(2) = 'COMPONENT'
  hcct%gil%convert(:,1) = (/1d0,0d0,1d0/)
  hcct%gil%convert(:,2) = (/1d0,0d0,1d0/)
  hcct%gil%extr_words = 0
  call gdf_create_image(hcct, error)
  if (error) then
    call gagout ('F-MX_CLEAN,  Cannot create clean component table')
    return
  endif
  !
!!  call gdf_allocate (hcct, error)
  allocate (hcct%r3d(hcct%gil%dim(1),hcct%gil%dim(2),hcct%gil%dim(3)), stat=ier)
  error = ier.ne.0
  if (error) then
    call gagout('F-MX_CLEAN,  Cannot allocate Clean Component Table')
    return
  endif
  !
  ! Create clean map 
  call gildas_null(hc)
  call gdf_copy_header (hd, hc, error)
  name = map_name
  call sic_parsef(name,hc%file,' ','.lmv-mx')
  !
  ! Undefine Extrema section
  hc%gil%extr_words = 0
  ! Define resolution
  hc%gil%reso_words = 3
  hc%gil%majo = major_axis
  hc%gil%mino = minor_axis
  hc%gil%posa = pi*pos_angle/180.0
  ! Create noise section
  hc%gil%nois_words = 2
  hc%gil%noise = rms
  !
  call gdf_create_image (hc, error)
  if (error) then
    call gagout('F-MX_CLEAN,  Cannot create clean image')
    return
  endif
  !
  ! Define Clean Image 
!!  call gdf_allocate (hc, error)
  allocate (hc%r3d(hc%gil%dim(1),hc%gil%dim(2),hc%gil%dim(3)), stat=ier)
  error = ier.ne.0
  if (error) then
    call gagout('F-MX_CLEAN,  Cannot allocate clean image')
    return
  endif
end subroutine mx_open_clean
!
subroutine mx_fit_beam(hb,error)
  use image_def
  use mx_parameters
  use fit_beam_tool
  type(gildas), intent(inout) :: hb
  logical, intent(out) :: error
  !
  real :: thre
  !
  if (ix_patch.eq.0) ix_patch = min(map_size(1)/2,128)
  if (iy_patch.eq.0) iy_patch = min(map_size(2)/2,128)
  ix_patch = min(ix_patch,map_size(1)/2)
  iy_patch = min(iy_patch,map_size(2)/2)
  thre = 0.30
  call fibeam ('MX',hb%r2d,map_size(1),map_size(2),   &
     &    ix_patch,iy_patch,thre,major_axis,minor_axis,pos_angle,   &
     &    hb%gil%convert,error)
end subroutine mx_fit_beam
