module maptasks_interfaces_private
  interface
    subroutine absmax (x,n,num,rabs)
      use cct_types
      !-------------------------------------------------------------------
      ! @ private
      !
      ! GILDAS:	Utility routine
      ! 	Compute position and values of Min and Max of array X
      !-------------------------------------------------------------------
      integer, intent(in) :: n                    !
      type(cct_par), intent(in) :: x(n)            !
      integer, intent(out) :: num                  !
      real, intent(out) :: rabs                    !
    end subroutine absmax
  end interface
  !
  interface
    subroutine choice_box (r,nx,ny,box,limite,   &
         &    nclean,wclean,nbpoin,rmax,select)
      use cct_types
      !---------------------------------------------------------------------
      ! @ private
      !
      ! MAP TASKS 	Internal routine
      !	  Select possible clean components in residual map.
      !	  Components are returned ordered in increasing I and J.
      !   Uses a BOX instead of a LIST
      !   Differs from one in map/lib by using a Box 
      !   and a slightly different test to rebuild the histogram.
      !---------------------------------------------------------------------
      integer, intent(in) :: nx          ! X size
      integer, intent(in) :: ny          ! Y size
      integer, intent(in) :: nclean      ! Maximum number of components
      integer, intent(out) :: nbpoin     ! Number of components found
      real, intent(in) :: r(nx,ny)       ! Input array
      integer :: box(4)                  !
      type(cct_par), intent(inout) :: wclean(nclean)            !
      real, intent(inout) ::  limite     ! selection threshold
      real, intent(in) :: rmax           ! Maximum value
      logical, intent(in) :: select                 !
    end subroutine choice_box
  end interface
  !
  interface
    subroutine histos_box (r,nx,ny, box, hx,nh,hmin,hstep)
      !---------------------------------------------------------------------
      ! @ private
      !
      ! MAP TASKS
      !	  Computes the histogram of part of an image
      !   Differs from one in map/lib: uses a box instead
      !   of a list of pixels.
      !---------------------------------------------------------------------
      integer, intent(in) :: nx        ! X size
      integer, intent(in) :: ny        ! Y size
      real, intent(in)  :: r(nx,ny)    ! Input array 
      integer, intent(in)  :: box(4)   ! Search box
      integer, intent(in) :: nh        ! Histogram size
      integer, intent(out) :: hx(nh)   ! Histogram
      real, intent(in) :: hmin         ! Min bin
      real, intent(in) :: hstep        ! Bin size
    end subroutine histos_box
  end interface
  !
end module maptasks_interfaces_private
