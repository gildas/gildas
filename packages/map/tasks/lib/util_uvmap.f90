! (Copyright IRAM, the GILDAS team)
!
! Author: Stephane Guilloteau
!
! Common code used by UV_MAP and UV_INVERT (f90 version)
!
subroutine get_weightmode(task,mode,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  !
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: task
  character(len=*), intent(inout) :: mode
  logical, intent(out) :: error
  !
  integer i
  character(len=8) :: argum
  character(len=8) :: vweight(3)=(/'NATURAL ','UNIFORM ','ROBUST  '/)
  !
  argum = mode
  call sic_upper (argum)
  call sic_ambigs (task,argum,mode,i,vweight,3,error)
  if (error) then
     call gagout('E-'//task//',  Invalid weight mode '//argum)
  else
     call gagout('I-'//task//',  Using '//vweight(i)//' weighting')
  endif
end subroutine get_weightmode
!
subroutine get_uvmap_par (task,uvdata,name,map,error)
  !
  ! UV_INVERT / UV_MAP
  !    Define mapping parameters
  !
  use phys_const
  use gkernel_interfaces
  use mapping_interfaces
  use uvmap_def_task
  !
  character(len=*), intent(in) :: task
  character(len=*), intent(out) :: uvdata
  character(len=*), intent(out) :: name
  type (par_uvmap), intent(inout) :: map
  logical, intent(out) :: error
  !
  character(len=256) :: chain
    !
  call gildas_open
  call gildas_char('UV_TABLE$',uvdata)
  call gildas_char('MAP_NAME$',name)
  call gildas_real('UV_TAPER$',map%taper,4)
  call gildas_char('WEIGHT_MODE$',map%mode)
  call gildas_real('MAP_FIELD$',map%field,2)
  call gildas_inte('MAP_SIZE$',map%size,2)
  call gildas_real('MAP_CELL$',map%xycell,2)
  call gildas_real('UV_CELL$',map%uniform,2)
  call gildas_inte('ONEBEAM$',map%beam,1)
  call gildas_inte('WCOL$',map%channels(3),1)
!  call gildas_inte('BCOL$',map%channels(4),1)
  call gildas_inte('MCOL$',map%channels,2)
  map%ctype = 5
  call gildas_inte('CONVOLUTION$',map%ctype,1)
  if (task.eq.'UV_INVERT') call gildas_logi('BLOCKED$',map%blocked,1)
  !
  call gildas_logi('UV_SHIFT$',map%shift,1)
  if (map%shift) then
    call gildas_char('RA_CENTER$',map%ra_c)
    call gildas_char('DEC_CENTER$',map%dec_c)
    call gildas_dble('ANGLE$',map%new(3),1)
    call sic_decode(map%ra_c,map%new(1),24,error)
    if (error) then
      chain = 'E-'//task//', Input conversion error on phase center '//map%ra_c
      call gagout(chain)
      return
    endif
    call sic_decode(map%dec_c,map%new(2),360,error)
    if (error) then
      chain = 'E-'//task//', Input conversion error on phase center '//map%dec_c
      call gagout(chain)
      return
    endif
    map%new(3) = map%new(3)*pi/180.0d0
  endif
  call gildas_close
  call get_weightmode(task,map%mode,error)
end subroutine get_uvmap_par
!
subroutine t_doweig (nv,uu,vv,wuv,unif,wm,error)
  use gkernel_interfaces
  use mapping_interfaces, only : findp
  !----------------------------------------------------------------------
  ! UV_INVERT / UV_MAP
  !   Compute weights of the visibility points.
  !----------------------------------------------------------------------
  use gildas_def
  integer, intent(in) :: nv  ! number of values
  real, intent(in) :: uu(nv)     ! U values
  real, intent(in) :: vv(nv)     ! V values
  real, intent(inout) :: wuv(nv) ! Weights
  real, intent(in) :: unif   ! uniform cell size in Meters
  real, intent(inout) :: wm      ! on input: % of uniformity
  logical, intent(out) :: error
  !
  integer iv
  real umin,umax,vmin,vmax,vstep,vimin,vimax
  real vcmin, vcmax
  integer nbcv, ivmin, ivmax, icv, nbv, new, nflag
  real, allocatable :: we(:)
  integer ier
  character(len=80) chain
  !
  ! Natural weight
  nflag = 0
  if (unif.le.0.0) then
    do iv=1,nv
      if (wuv(iv).le.0) then
         nflag = nflag+1
         wuv(iv) = 0.0
      endif
    enddo
    if (nflag.ne.0) then
      write(chain,'(a,i12,a)') 'I-DOWEIG,  Natural weights, ', nflag,  &
           & ' flagged visibilities ignored'
      call gagout(chain)
    endif
    return
  endif
  !
  ! Uniform weight
  !
  ! 1) Compute VMIN, VMAX, UMIN, UMAX
  vmin = vv(1)
  vmax = vv(nv)
  umin = 0.0
  umax = 0.0
  do iv=1,nv
    if (uu(iv).lt.umin) then
      umin = uu(iv)
    elseif  (uu(iv).gt.umax) then
      umax = uu(iv)
    endif
  enddo
  if (-umin.gt.umax) then
    umax = -umin
  else
    umin = -umax
  endif
  !
  ! Allow small margin
  vmin = 1.001*vmin
  umax = 1.001*umax
  umin = 1.001*umin
  !
  ! Some speed up factor
  nbcv = 8
  vstep = -vmin/nbcv
  !
  ! Adjust the number of cells. A cell must be > 2*UNIF, and > 4*UNIF is more
  ! than sufficient to get speed up.
  if (vstep.lt.4*unif) then
    nbcv = int((-vmin/(4.*unif)))
    if (nbcv.lt.1) nbcv = 1
    vstep = -vmin/nbcv
  endif
  !
  ! Loop on U,V cells
  nbv = 0
  ivmin = 1
  do icv = 1,nbcv
    vcmin = (icv-1)*vstep+vmin
    vimin = vcmin-unif
    vcmax = icv*vstep+vmin
    vimax = vcmax+unif
    call findp (nv,vv,vimin,ivmin)
    ivmax = ivmin
    call findp (nv,vv,vimax,ivmax)
    ivmax = min(nv,ivmax+1)
    new = ivmax-ivmin+1
    if (icv.eq.nbcv) then
      vimin = -unif
      call findp (nv,vv,vimin,ivmin)
      new = new + (nv-ivmin+1)
    endif
    nbv = max(new,nbv)
  enddo
  !
  ! Allocate memory
  allocate(we(nv),stat=ier)
  if (ier.ne.0) then
    call gagout('E-GRID,  Cannot allocate work arrays')
    error = .true.
    return
  endif
  call t_doweig_quick (nv,uu,vv,wuv,unif,we,wm,  &
      &  nbv,umin,umax,vmin,vmax,nbcv,error)
  !
  ! Copy back the weights
  wuv = we
  deallocate(we)
  error = .false.
end subroutine t_doweig
!
subroutine t_doweig_quick (nv,uu,vv,wuv,unif,we,wm,mv,umin,umax,  &
  vmin,vmax,nbcv,error)
  use gkernel_interfaces
  !----------------------------------------------------------------------
  ! UV_INVERT / UV_MAP
  !   Compute weights of the visibility points.
  !     Quick version using sub-grids
  !----------------------------------------------------------------------
  integer, intent(in) :: nv  ! number of values
  real, intent(in) :: uu(nv)     ! U values
  real, intent(in) :: vv(nv)     ! V values
  real, intent(inout) :: wuv(nv) ! Weights
  real, intent(in) :: unif   ! uniform cell size in Meters
  real, intent(in) :: wm         ! Weight scale factor
  real, intent(out) :: we(nv)
  integer, intent(in) :: mv
  integer, intent(in) :: nbcv    ! sub-grid scale factor
  real, intent(in) :: umin
  real, intent(in) :: umax
  real, intent(in) :: vmin
  real, intent(in) :: vmax
  logical, intent(out) :: error
  !
  integer i
  real u,v,weight,wmax,wmin
  real vstep,ustep,vimin,vimax,uimin,uimax
  real ucmin, ucmax, vcmin, vcmax
  integer nbcu, ivmin, ivmax, icu, icv, nbv, ier
  !
  integer, allocatable :: ipv(:)
  real, allocatable :: utmp(:), vtmp(:), wtmp(:), rtmp(:)
  !
  allocate (ipv(mv),utmp(mv),vtmp(mv),wtmp(mv),rtmp(mv),stat=ier)
  if (ier.ne.0) then
    call gagout('E-GRID,  Cannot allocate work arrays')
    error = .true.
    return
  endif
  we = -1.0
  !
  ! Some speed up factor
  nbcu = 2*nbcv
  vstep = -vmin/nbcv
  ustep = (umax-umin)/nbcu
  !
  ! Adjust the number of cells. A cell must be > 2*UNIF, and > 4*UNIF is more
  ! than sufficient to get speed up.
  !
  if (vstep.lt.4*unif) then
    call gagout('E-GRID,  Vstep trop petit')
    error = .true.
    return
  endif
  !
  ! Loop on U,V cells
  do icv = 1,nbcv
    vcmin = (icv-1)*vstep+vmin
    vimin = vcmin-unif
    vcmax = icv*vstep+vmin
    vimax = vcmax+unif
    !
    ivmin = 1
    ivmax = nv
    call findp (nv,vv,vimin,ivmin)
    ivmax = ivmin
    call findp (nv,vv,vimax,ivmax)
    ivmax = min(nv,ivmax+1)
    !
    do icu = 1,nbcu
      ucmin = (icu-1)*ustep+umin
      uimin = ucmin-unif
      ucmax = icu*ustep+umin
      uimax = ucmax+unif
      !
      ! Select valid visibilities
      nbv = 0
      do i=ivmin,ivmax
        ! Note that V stays ordered by increasing values
        u = uu(i)
        v = vv(i)
        !
        ! Normal case only...
        if ((v.ge.vimin).and.(v.le.vimax)) then
          if ((u.ge.uimin).and.(u.le.uimax)) then
            nbv = nbv+1
            ipv(nbv) = i
            utmp(nbv)= u
            vtmp(nbv)= v
            wtmp(nbv)= wuv(i)
          endif
        endif
        !
        ! Limiting case near V=0, inverse the U test...
        if (-v.le.unif) then
          if ((u.lt.-uimin).and.(u.gt.-uimax)) then
            nbv = nbv+1
            ipv(nbv) = 0  ! Why 0 ?
            utmp(nbv)= u
            vtmp(nbv)= v
            wtmp(nbv)= wuv(i)
          endif
        endif
      enddo
      !
      ! Compute the weights
      if (nbv.gt.0) then
        call doweig_sub (nbv,utmp,vtmp,wtmp, rtmp, unif)
        !
        ! IPV is a back pointer
        do i=1,nbv
          if ( (utmp(i).gt.ucmin.and.utmp(i).le.ucmax) &
               & .and. (vtmp(i).gt.vcmin.and.vtmp(i).le.vcmax) ) &
               & then
            if (ipv(i).ne.0) then
              if (we(ipv(i)).ne.-1.0) then
                 Print *,'Computed ',i,ipv(i),we(ipv(i)),rtmp(i)
              endif
              we(ipv(i)) = rtmp(i)
            endif
          endif
        enddo
      endif
        !
    enddo  ! Icu
  enddo    ! Icv
  !
  ! All weights are computed now, normalize them
  weight = 0.0
  wmax = 0.0
  wmin = 1.e36
  do i=1,nv
    if (we(i).gt.0.0) then
      wmax = max(we(i),wmax)
      wmin = min(we(i),wmin)
      weight = weight+we(i)
    endif
  enddo
  weight = weight/nv
  !!write(6,*) 'Weights ',wm,wmax,wmin,sqrt(wmin*wmax)*wm
  weight = wm*sqrt(wmin*wmax)
  do i=1,nv
    if (we(i).gt.weight) then
      we(i) = wuv(i)/we(i)*weight
    elseif (we(i).gt.0.0) then
      we(i) = wuv(i)
    endif
  enddo
  deallocate (ipv,utmp,vtmp,wtmp,rtmp)
end subroutine t_doweig_quick
!
subroutine t_dotaper(nv,uu,vv,ww,taper)
  !
  ! UV_INVERT / UV_MAP
  !    Apply taper
  !
  use phys_const
  integer, intent(in) :: nv      ! Number of UV points
  real, intent(in) :: uu(nv)     ! U values
  real, intent(in) :: vv(nv)     ! V values
  real, intent(inout) :: ww(nv)  ! Weights
  real, intent(in) :: taper(4)   ! Taper parameters
  !
  real staper, etaper, cx,sy,sx,cy, u,v
  integer i
  !
  if (taper(1).ne.0. .and. taper(2).ne.0.) then
    staper = taper(3)*pi/180.0
    if (taper(1).ne.0) then
      cx = cos(staper)/taper(1)
      sy = sin(staper)/taper(1)
    else
      cx = 0.0
      sy = 0.0
    endif
    if (taper(2).ne.0) then
      cy = cos(staper)/taper(2)
      sx = sin(staper)/taper(2)
    else
      cy = 0.0
      sx = 0.0
    endif
    if (taper(4).ne.0.) then
      etaper = taper(4)/2.0
    else
      etaper = 1
    endif
    !
    ! Modify the weights to compute the tapered ones
    do i=1,nv
      u = uu(i)
      v = vv(i)
      staper = (u*cx + v*sy)**2 + (-u*sx + v*cy)**2
      if (etaper.ne.1) staper = staper**etaper
      if (staper.gt.64.0) then
        staper = 0.0
      else
        staper = exp(-staper)
      endif
      !
      ! Weight and taper
      ww(i) = staper*ww(i)
    enddo
  endif
end subroutine t_dotaper
!
subroutine t_channel(huv,map)
  use gkernel_interfaces, only : gdf_uv_frequency
  use gildas_def
  use image_def
  use uvmap_def_task
  !----------------------------------------------------------
  ! UV_INVERT / UV_MAP
  !    Setup the channel associated parameters
  !----------------------------------------------------------
  type (gildas), intent(in) :: huv        ! UV header
  type (par_uvmap), intent(inout) :: map  ! Imaging parameters
  !
  integer nc,fcol,lcol
  real(8) :: rcol
  !
  ! Check the channel range
  nc = huv%gil%nchan
  if (map%channels(1).eq.0) then
    map%channels(1) = 1
  else
    map%channels(1) = max(1,min(map%channels(1),nc))
  endif
  if (map%channels(2).eq.0) then
    map%channels(2) = nc
  else
    map%channels(2) = max(1,min(map%channels(2),nc))
  endif
  fcol = min(map%channels(1),map%channels(2))
  lcol = max(map%channels(1),map%channels(2))
  map%channels(1) = fcol
  map%channels(2) = lcol
  if (map%channels(3).eq.0) then
    map%channels(3) = (lcol+fcol)/2
  else
    map%channels(3) = max(0,min(map%channels(3),nc))
  endif
  !
  ! Get center frequency
  if (map%channels(4).eq.0) then
    rcol = 0.5d0*(lcol+fcol)
  else
    rcol = max(1,min(map%channels(4),nc))
  endif
  !
  map%freq = gdf_uv_frequency(huv, rcol)
  !
end subroutine t_channel
!
subroutine t_map(task,map,huv,uvmin,uvmax,conv)
  use gkernel_interfaces
  use mapping_interfaces
  !----------------------------------------------------------
  ! UV_INVERT / UV_MAP
  !    Setup the image plane associated parameters
  !----------------------------------------------------------
  use gildas_def
  use image_def
  use uvmap_def_task
  use phys_const
  !
  character(len=*), intent(in) :: task
  type (par_uvmap), intent(inout) :: map
  type (gildas), intent(in) :: huv
  real, intent(inout) :: uvmin  ! Minimum UV length (in meters on input)
  real, intent(inout) :: uvmax  ! Maximum UV length (in Wavelength on output)
  type (gridding), intent(out) :: conv
  !
  ! Locals
  character(len=80) chain
  integer ctypx,ctypy,nc
  real xparm(10),yparm(10)
  real prim,field
  real :: pixel_per_beam=2.5
  !
  ! Initialize Xparm & Yparm to Zero, otherwise lots of trouble...
  xparm = 0
  yparm = 0
  !
  nc = huv%gil%nchan !! (huv%gil%dim(map%uvcode)-7)/3
  write(chain,100) task,huv%gil%dim(3-map%uvcode),nc
  call gagout(chain)
  write(chain,102) task,map%freq
  call gagout(chain)
  write(chain,101) task, uvmin,uvmax,' meters'
  call gagout(chain)
  !
  ! Now transform UVMAX in Wavelengths (including 2 pi factor)
  uvmax = uvmax * (map%freq*f_to_k)
  uvmin = uvmin * (map%freq*f_to_k)
  write(chain,101) task, uvmin*1e-3/(2*pi),uvmax*1e-3/(2*pi),' kiloWavelength'
  call gagout(chain)
  !
  ! Now get MAP_CELL and MAP_SIZE if Zero.
  if (map%xycell(1).eq.0) then
    map%xycell(1) = 0.02*nint(180.0*3600.0*50.0/uvmax/pixel_per_beam)
    if (map%xycell(1).le.0.02) then
      map%xycell(1) = 0.002*nint(180.0*3600.0*500.0/uvmax/pixel_per_beam)
    endif
    map%xycell(2) = map%xycell(1)   ! in ", rounded to 0.02 or 0.002"
  endif
  !
  ! Get a wide enough field of view...
  if (map%field(1).ne.0) then
    if (map%field(2).eq.0) map%field(2) = map%field(1)
    map%size = map%field/map%xycell !
    map%size =   2**nint(log(real(map%size))/log(2.0))
    !
    map%xycell = sqrt(map%field(1)*map%field(2)/(map%size(1)*map%size(2)))
    !
  endif
  !
  if (map%size(1).eq.0) then
    prim = huv%gil%majo*180*3600/pi
    field = 2.0*180*3600/uvmin
    write(chain,105) task,prim,field
    call gagout(chain)
    field = max(field,prim)
    map%size(2) = field/map%xycell(1) !!*pi/180/3600)
    map%size(2) = max(map%size(2),nint(2*pixel_per_beam*uvmax/uvmin))
    map%size(1) =   2**nint(log(real(map%size(2)))/log(2.0))
    if (map%size(1).lt.0.8*map%size(2)) map%size(1) = 2*map%size(1)
    map%size(2) = map%size(1)
  endif
  !
  write(chain,103) task,map%size
  call gagout(chain)
  write(chain,104) task,map%xycell
  call gagout(chain)
  !
  ! Redefine some parameters
  call sic_upper(map%mode)
  if (map%mode.eq.'NATU') map%uniform(1) = -1.0
  if (map%uniform(2).le.0.0) map%uniform(2) =  1.0
  map%xycell = map%xycell*pi/180.0/3600.0 ! in radians
  !
  ! Compute gridding function
  ctypx = map%ctype
  ctypy = map%ctype
  call grdflt (ctypx, ctypy, xparm, yparm)
  call convfn (ctypx, xparm, conv%ubuff, conv%ubias)
  call convfn (ctypy, yparm, conv%vbuff, conv%vbias)
  !
  map%uvcell = clight/(map%freq*1d6)/(map%xycell*map%size)
  map%support(1) = xparm(1)*map%uvcell(1)  ! in meters
  map%support(2) = yparm(1)*map%uvcell(2)  ! in meters
  !
  !
100 FORMAT('I-',A,',  Found ',I12,' Visibilities, ',I4,' channels')
101 FORMAT('I-',A,',  Baselines ',F9.1,' - ',F9.1,A)
102 FORMAT('I-',A,',  Frequency ',F13.6,' MHz')
103 FORMAT('I-',A,',  Map size is   ',I4,' by ',I4)
104 FORMAT('I-',A,',  Pixel size is ',F8.3,' by ',F8.3,'"')
105 FORMAT('I-',A,',  Primary beam ',F8.1,' -- Field ',F8.1,' (")')
  !
end subroutine t_map
!
!
subroutine t_setbeam(huv,hbeam,map,ndim)
  !-----------------------------------------------------
  ! UV_INVERT / UV_MAP
  !     Set Beam Header
  !-----------------------------------------------------
  use gkernel_interfaces
  use image_def
  use uvmap_def_task
  type (par_uvmap), intent(in) :: map
  type (gildas), intent(in), target :: huv
  type (gildas), intent(inout), target :: hbeam
  integer, intent(in) :: ndim  ! Number of dimensions
  !
  logical :: error
  !
  call gdf_copy_header(huv,hbeam,error)
  !
  hbeam%blc = 0
  hbeam%trc = 0
  hbeam%gil%ndim = ndim ! For Mosaics -- May have to be forced to 3 later...
  hbeam%gil%dim(1) = map%size(1)
  hbeam%gil%dim(2) = map%size(2)
  if (ndim.eq.3) then
    hbeam%gil%dim(3) = map%channels(2)-map%channels(1)+1
    if (hbeam%gil%dim(3).eq.1) hbeam%gil%ndim = 2 ! For Mosaics
  else
    if (map%channels(4).ne.0) then
      hbeam%gil%ref(3) = (1-map%channels(4))+huv%gil%ref(map%uvcode)
    else
      hbeam%gil%ref(3) =  0.5*(2-map%channels(1)-map%channels(2)) &
        & +huv%gil%ref(map%uvcode)
    endif
    hbeam%gil%dim(3) = 1
  endif
  hbeam%gil%dim(4) = 1
  hbeam%gil%ref(1) = map%size(1)/2+1
  hbeam%gil%ref(2) = map%size(2)/2+1
  hbeam%gil%val(1) = 0
  hbeam%gil%val(2) = 0
  hbeam%gil%inc(1) = -map%xycell(1)    ! assume equatorial system
  hbeam%gil%inc(2) = map%xycell(2)
  if (map%uvcode.eq.1) then
    hbeam%gil%val(3) = huv%gil%val(1)
    hbeam%gil%inc(3) = huv%gil%inc(1)
  else
    hbeam%gil%val(3) = huv%gil%val(2)
    hbeam%gil%inc(3) = huv%gil%inc(2)
  endif
  hbeam%char%code(1) = 'ANGLE'
  hbeam%char%code(2) = 'ANGLE'
  hbeam%char%code(3) = 'FREQUENCY'
  hbeam%gil%proj_words = 0
  hbeam%gil%extr_words = 0
  hbeam%gil%reso_words = 0
  hbeam%gil%uvda_words = 0
  hbeam%gil%type_gdf = code_gdf_image
  hbeam%loca%size = map%size(1)*map%size(2)
  hbeam%char%type = 'GILDAS_IMAGE'
  !
end subroutine t_setbeam
!
subroutine t_setdirty(huv,hdirty,map,wall)
  use gkernel_interfaces
  use image_def
  use uvmap_def_task
  !-----------------------------------------------------
  ! UV_INVERT / UV_MAP
  !     Set Dirty Header
  !-----------------------------------------------------
  type (par_uvmap), intent(in) :: map    ! Imaging parameters
  type (gildas), intent(in), target :: huv        ! UV Header
  type (gildas), intent(inout), target :: hdirty  ! Dirty Header
  real, intent(in) :: wall    ! Theoretical noise
  !
  real(kind=4) :: loff,boff
  logical :: error
  !
  call gdf_copy_header(huv,hdirty,error)
  !
  hdirty%gil%ndim = 3
  hdirty%gil%dim(1) = map%size(1)
  hdirty%gil%dim(2) = map%size(2)
  hdirty%gil%dim(3) = map%channels(2)-map%channels(1)+1
  hdirty%gil%dim(4) = 1
  hdirty%gil%ref(1) = map%size(1)/2+1
  hdirty%gil%ref(2) = map%size(2)/2+1
  hdirty%gil%val(1) = 0
  hdirty%gil%val(2) = 0
  hdirty%gil%inc(1) = -map%xycell(1)    ! assume equatorial system
  hdirty%gil%inc(2) = map%xycell(2)
  if (map%uvcode.eq.1) then
    hdirty%gil%ref(3) = huv%gil%ref(1) - map%channels(1) +1
  else
    hdirty%gil%ref(3) = huv%gil%ref(2) - map%channels(1) +1
  endif
  hdirty%gil%val(3) = huv%gil%voff
  hdirty%gil%inc(3) = huv%gil%vres
  ! Projection information (assume equatorial system)
  hdirty%char%code(1) = 'RA'
  hdirty%char%code(2) = 'DEC'
  hdirty%char%code(3) = 'VELOCITY'
  call equ_to_gal(hdirty%gil%ra,hdirty%gil%dec,0.0,0.0,  &
                  hdirty%gil%epoc,hdirty%gil%lii,hdirty%gil%bii,loff,boff,error)
  if (huv%gil%ptyp.eq.p_none) then
     hdirty%gil%ptyp = p_azimuthal             ! Azimuthal (Sin)
     !!hdirty%gil%a0 = hdirty%gil%ra
     !!hdirty%gil%d0 = hdirty%gil%dec
     !!print *,'5.2 pos ',hdirty%gil%a0,hdirty%gil%d0
  else
     hdirty%gil%ptyp = p_azimuthal
     hdirty%gil%pang = huv%gil%pang  ! Defined in table.
     hdirty%gil%a0 = huv%gil%a0
     hdirty%gil%d0 = huv%gil%d0
  endif
  hdirty%char%syst = 'EQUATORIAL'
  hdirty%gil%xaxi = 1
  hdirty%gil%yaxi = 2
  hdirty%gil%faxi = 3
  hdirty%gil%proj_words = 9
  hdirty%gil%extr_words = 0                ! extrema not computed
  hdirty%gil%nois_words = 2
  hdirty%gil%reso_words = 0                ! no beam
  hdirty%gil%uvda_words = 0                ! No UV data any more
  hdirty%gil%type_gdf = code_gdf_image
  hdirty%gil%noise = wall
  hdirty%char%unit = 'Jy/beam'
  hdirty%char%type = 'GILDAS_IMAGE'
  hdirty%loca%size = hdirty%gil%dim(1) * hdirty%gil%dim(2) * hdirty%gil%dim(3)
  !
end subroutine t_setdirty
!
subroutine do_mapslow(np,nv,visi,we,lc,   &
     &    freq,nc,nx,ny,map,beam,mapcox,mapcoy)
  use phys_const
  integer, intent(in) :: np                     ! Size of a visibility
  integer, intent(in) :: nv                     ! Number of visibilities
  real, intent(in) :: visi(np,nv)               ! Visibilities UVT
  real, intent(in) :: we(nv)                    ! Weights
  integer, intent(in) :: lc                     !
  real(8), intent(in) :: freq                   ! Observing frequency
  integer, intent(in) :: nc                     ! Number of channels
  integer, intent(in) :: nx                     ! Number of pixels in X
  integer, intent(in) :: ny                     ! Number of pixels in Y
  real, intent(out) :: map(nc,nx,ny)            ! Dirty cube
  real, intent(out) :: beam(nx,ny)              ! Dirty beam
  real, intent(in) :: mapcox(nx)                ! X coordinates
  real, intent(in) :: mapcoy(ny)                ! Y coordinates
  ! Local
  real(8) :: sinu, cosi
  real(8) :: z, kwx, kwy, y, signe(2)
  integer :: ix, iy, ic, iic, irc, i, k
  !------------------------------------------------------------------------
  signe(1) = 1d0
  signe(2) = -1d0
  !
  ! Code:
  beam = 0
  map = 0
  !
  ! This may not be optimal in terms of memory access (bigger array
  ! is in inner loop, and sin + cos are computed twice)
  !
  do i = 1, nv                 ! loop on visibility points
    kwx = freq * f_to_k * visi(1,i)    ! Prepare exponential coefficients
    kwy = freq * f_to_k * visi(2,i)
    !
    ! Take into account conjugate points
    do k=1,2
      if (k.eq.2) then
        kwx = -kwx
        kwy = -kwy
      endif
      do iy=1, ny              ! Loop on pixels
        y = kwy*mapcoy(iy)
        do ix=1, nx
          z = kwx*mapcox(ix) + y
          cosi = cos(z)
          sinu = signe(k)*sin(z) ! Conjugate if needed
          cosi = cosi*we(i)
          sinu = sinu*we(i)
          beam(ix,iy) = beam(ix,iy)+cosi
          do ic = 1, nc
            irc = 2+3*(ic+lc)
            iic = irc+1
            map(ic,ix,iy) = map(ic,ix,iy)   &
     &              + visi(irc,i)*cosi   &
     &              + visi(iic,i)*sinu
          enddo
        enddo
      enddo
    enddo
  enddo
end subroutine do_mapslow
