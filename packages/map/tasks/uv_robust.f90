program uvrobust_main
  use gildas_def
  use image_def
  use gkernel_interfaces
  !----------------------------------------------------------------------
  ! GILDAS
  !       Set weights to "Robust" value 
  !----------------------------------------------------------------------
  character(len=80) :: uvdata,uvtri
  real(4) :: cell,robust
  integer :: wcol
  logical error
  !
  ! Code:
  call gildas_open
  call gildas_char('UVDATA$',uvdata)
  call gildas_char('UVSORT$',uvtri)
  call gildas_real('CELL$',cell,1)
  call gildas_real('ROBUST$',robust,1)
  call gildas_inte('WCOL$',wcol,1)
  call gildas_close
  call sub_uvrobust (uvdata,uvtri,cell,robust,wcol,error)
  if (error) call sysexi(fatale)
!
contains
!
subroutine sub_uvrobust(uvdata,uvtri,cell,robust,wcol,error)
  use uv_rotate_shift_and_sort_tool, only: uvsort_uv
  !----------------------------------------------------------------------
  ! GILDAS
  !       Sort an input UV table
  !       Add CC code to implement precession
  !----------------------------------------------------------------------
  use gildas_def
  use image_def
  use gkernel_interfaces
  !
  character(len=*), intent(in) :: uvdata ! Input UV data set name
  character(len=*), intent(in) :: uvtri  ! Output UV data set name
  real(4), intent(in) :: cell            ! Cell size
  real(4), intent(in) :: robust          ! Robust parameter
  integer, intent(in) :: wcol            ! Weight channel
  logical, intent(out) :: error
  !
  ! Local variables:
  type (gildas) :: in, out
  real, allocatable :: din(:,:), dout(:,:), weight(:), vvalues(:)
  character(len=80) :: name
  real xy(2),cs(2), uvmax, uvmin, wm
  integer :: ier, np, nv, i, j, jw
  !
  data xy/0.0,0.0/, cs/1.0,0.0/
  ! Code:
  !
  error = .false.
  call gildas_null(in, type='UVT')
  call gildas_null(out, type='UVT')
  !
  ! Input file
  call sic_parse_file(uvdata,' ','.uvt',in%file)
  call gdf_read_header(in,error)
  if (error) return
  !
  np = in%gil%dim(1)
  nv = in%gil%dim(2)
  allocate(din(np,nv),dout(np,nv),stat=ier)
  call gdf_read_data(in,din,error)
  if (error) then
     deallocate(din,dout)
     return
  endif
  !
  ! Create output image
  call gdf_copy_header(in,out,error)
  call sic_parse_file(uvtri,' ','.uvt',out%file)
  !
  call uvsort_uv  (np,nv,in%gil%ntrail,din,dout, &
     &      xy,cs,uvmax,uvmin,error)
  write (6,'(A,F10.2,F10.2)') 'Min - Max UV-spacing: ',   &
     &      uvmin,uvmax
  if (error) then
     deallocate(din,dout)
     return
  endif
  !
  ! Now compute the weights
  allocate (weight(nv),vvalues(nv),stat=ier)
  if (wcol.eq.0) then
     jw = (in%gil%nchan+1)/2 
  else
     jw = wcol
  endif
  !
  ! Load V values and Original Weights
  call dovisi (np,nv,dout,vvalues,weight,jw)
  call doweig (np,nv,dout,1,2,jw,cell,weight,robust,vvalues,error)
!!  print *,weight
  do i=1,out%gil%nvisi
     do j=1,out%gil%nchan
        dout(out%gil%fcol+out%gil%natom*j-1,i) = weight(i)
     enddo
  enddo
  call gdf_write_image(out,dout,error)
  deallocate(din,dout,weight)
  ! 
end subroutine sub_uvrobust
!
end program uvrobust_main
