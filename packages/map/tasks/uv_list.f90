program uv_list
  use gildas_def
  use image_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! GILDAS
  !	   LIST data in input UV table
  !---------------------------------------------------------------------
  ! Local
  type(gildas) :: x, y
  character(len=80) :: uvdata
  integer, parameter :: mvocab=7
  character(len=80) :: uvlist, listfile
  character(len=3) :: vocab(mvocab)
  integer, allocatable :: ind(:)
  real(8), allocatable :: time(:)
  integer :: nx, nc, lun, ier, mc(2)
  real :: ut_step
  logical :: error
  !
  data vocab/'ALL','12','13','23','14','24','34'/
  !------------------------------------------------------------------------
  ! Code:
  call gildas_open
  call gildas_char('UVDATA$',uvdata)
  call gildas_char('UVLIST$',uvlist)
  call gildas_real('UT_STEP$',ut_step,1)
  call gildas_inte('CHANNEL$',nc,1)
  call gildas_close
  !
  if (len_trim(uvdata).le.0) goto 999
  if (len_trim(uvlist).le.0) goto 999
  !
  ! Input file
  call gildas_null(x, type = 'UVT')
  call gdf_read_gildas(x, uvdata, '.uvt', error, data = .false.)
  if (error) then
    write(6,*) 'F-UV_FLAG,  Cannot read input UV table'
    goto 999
  endif
  !
  ! Output list
  call sic_parsef(uvlist,listfile,' ','.uvlist')
  ier = sic_getlun(lun)
  open(unit=lun,file=listfile,status='UNKNOWN')
  rewind(lun)
  write(lun,1000) trim(x%file), nc
  !
  nc = max(1,min(nc,x%gil%nchan))
  mc(1:2) = nc 
  call gildas_null(y, type = 'UVT')
  call gdf_copy_header(x,y,error)
  y%gil%dim(1) = x%gil%dim(1) - x%gil%natom*(x%gil%nchan-1) ! Only one channel left...
  call gdf_allocate (y,error)
  call gdf_read_uvdataset (x,y,mc,y%r2d,error)
  !
  allocate (ind(x%gil%dim(2)), time(x%gil%dim(2)), stat=ier)
  if (ier.ne.0) goto 999
  call dolist (y%r2d, y%gil%dim(1),y%gil%dim(2),ind,   &
     &    time, lun,1,ut_step,error)
  if (error) call sysexi(fatale)
  call gagout('S-UV_LIST,  Successful completion')
  call sysexi(1)
  !
  999   call sysexi(fatale)
  !
  1000  format(1x,'File:',t10,a,t40,'Channel: ',i4,/,   &
     &    1x,79('-'),/,   &
     &    t7,'Date',t18,'Time',t25,'Bas',   &
     &    t31,'U',t38,'V',t44,'W',t53,'R',t61,'I',t70,'W',/,   &
     &    1x,79('-'))
end program uv_list
!
subroutine dolist (out,nx,nv,index,t,lun,nc,ut_step,error)
  use gildas_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! List the table visibility points at each ut_step (hours)
  !---------------------------------------------------------------------
  integer(kind=index_length), intent(in) :: nx                     !
  integer(kind=index_length), intent(in) :: nv                     !
  real :: out(nx,nv)                !
  integer :: index(nv)              !
  real(8) :: t(nv)                  !
  integer :: lun                    !
  integer :: nc                     !
  real :: ut_step                   !
  logical :: error                  !
  ! Local
  integer :: mb
  parameter (mb=6)
  integer :: j,is1,ie1,jj, nj1, jj1(mb), nj2, jj2(mb)
  real(8) :: tt, t1, t2
  logical :: end
  !------------------------------------------------------------------------
  ! Code:
  !
  end = .false.
  t1 = -1d10                   ! the last dump written
  t2 = -1d10                   ! the previous dump
  is1 = 0
  ie1 = 0
  do j=1, nv
    t(j) = out(4,j)+out(5,j)/86400.
  enddo
  call gr8_trie (t,index,int(nv,kind=4),error)
  if (error) return
  nj1 = 0
  nj2 = 0
  do jj = 1, nv
    tt = t(jj)
    if (nj1.lt.mb .and. tt-t1.lt.3d-18) then
      nj1 = nj1 + 1
      jj1(nj1) = index(jj)
    endif
    if (nj2.lt.mb .and. tt-t2.lt.3d-18) then
      nj2 = nj2 + 1
      jj2(nj2) = index(jj)
    endif
    !
    ! Dump record of 1 ut_step before
    if (tt.ge.t1+ut_step/24.) then
      if (nj1.gt.0) then
        call dodump(t1,out,nx,nv,jj1,nj1,nc,end,lun)
      endif
      t1 = tt
      nj1 = 1
      jj1(1) = index(jj)
    endif
    ! Dump last record if 1 ut_step has elapsed anyway
    if (tt.ge.t2+ut_step/24.) then
      if (nj2.gt.0) then
        end = .true.
        call dodump(t2,out,nx,nv,jj2,nj2,nc,end,lun)
      endif
    endif
    if (t2.ne.tt) then
      t2 = tt
      nj2 = 1
      jj2(1) = index(jj)
    endif
  enddo
  if (nj2.gt.0) then
    end = .true.
    call dodump(t2,out,nx,nv,jj2,nj2,nc,end,lun)
  endif
end subroutine dolist
!
subroutine dodump(time,out,nx,nv,jjj,nj,nc,end,lun)
  use gkernel_interfaces
  real(8) :: time                    !
  integer(kind=index_length), intent(in) :: nx                     !
  integer(kind=index_length), intent(in) :: nv                     !
  real :: out(nx,nv)                !
  integer :: nj                     !
  integer :: jjj(nj)                !
  integer :: nc                     !
  logical :: end                    !
  integer :: lun                    !
  ! Global
  include 'gbl_pi.inc'
  ! Local
  logical :: error
  integer :: it, ks, ke, mb, kj, is, ie, j, i
  parameter (mb=6)
  character(len=20) :: chd, cht
  !------------------------------------------------------------------------
  ! Code:
  it = int(time+1.d5)-100000
  call gag_todate(it,chd,error)
  if (error) then
    write(6,*) 'I-DOLIST, Date conversion error'
  endif
  call sexag(cht,2*pi*(time-it),24)
  do ks = 1, mb-1
    do ke = ks+1, mb
      do kj = 1, nj
        j = jjj(kj)
        is = nint(out(6,j))
        ie = nint(out(7,j))
        if (is.eq.ks .and. ie.eq.ke) then
          write(lun,1000) chd(1:11),cht(1:9),is,ie,   &
     &            (out(i,j),i=1,3), (out(i,j),i=nc*3+5,nc*3+7)
          chd = ' '
          cht = ' '
        endif
      enddo
    enddo
  enddo
  if (end) then
    write(lun,1001)
    end = .false.
  endif
  return
  1000  format(3x,a,a,1x,2i1,3f7.1,2f8.3,1pg11.2)
  1001  format(1x,78('-'))
end subroutine dodump
