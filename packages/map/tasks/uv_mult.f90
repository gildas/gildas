program uv_mult
  use gildas_def
  use gkernel_interfaces
  use image_def
  use gbl_format
  !---------------------------------------------------------------------
  ! TASK   Modify the U,V length of a table in order to change
  !        the size of the final image.
  !
  ! Input :
  !	UV_TABLE$	The UV table to be modified
  !       MULTA$ Multiplication of the final image in X by factor A
  !       MULTB$ Multiplication of the final image in X by factor B
  !       MULTC$ Intensity Calibration factor  C
  !
  ! R.Moreno Jun-1999
  !---------------------------------------------------------------------
  ! Local
  type(gildas) :: x
  character(len=filename_length) :: uv_table,name
  !
  integer :: sblock=1000
  integer :: i,nc,n,j,k, ier
  real :: multa,multb,multc
  logical :: error
  !
  call gildas_open
  call gildas_char('UV_TABLE$',uv_table)
  call gildas_real('MULTA$',multa,1)
  call gildas_real('MULTB$',multb,1)
  call gildas_real('MULTC$',multc,1)
  if ( (multa.eq.0.0).or.(multb.eq.0.0).or.(multc.eq.0.0) ) then
    multa = 1.0
    multb = 1.0
    multc = 1.0
    call gagout('W-MCAL, A,B or C  = 0, no multiplication applied')
  endif
  call gildas_close
  !
  ! Open line table
  n = lenc(uv_table)
  if (n.le.0) goto 999
  call gildas_null (x, type = 'UVT')
  name = uv_table(1:n)
  call gdf_read_gildas (x, name, '.uvt', error)
  if (error) then
    call gagout('F-MCAL,  Cannot read input/output UV table')
    goto 999
  endif
  nc = x%gil%nchan 
  !
  allocate (x%r2d(x%gil%dim(1), sblock), stat=ier)
  !
  ! Loop over line table
  do i=1,x%gil%dim(2),sblock
    x%blc(2) = i
    x%trc(2) = min(x%gil%dim(2),i-1+sblock)
    call gdf_read_data(x,x%r2d,error)
    k = 0
    do j=x%blc(2),x%trc(2)
      k = k+1
      call domult (nc,x%r2d(:,k),x%r2d(1:2,k),multa,multb,multc)
    enddo
    call gdf_write_data (x, x%r2d, error)
  enddo
  ! end loop
  call gdf_close_image (x, error)
  call gagout('S-UV_MULT,  Successful completion')
  call sysexi(1)
  999   call sysexi(fatale)
end program uv_mult
!
subroutine domult (nc,visi,uv,ma,mb,mc)
  !---------------------------------------------------------------------
  ! TASK  scaling the visibility by a factor
  !
  !---------------------------------------------------------------------
  integer, intent(in) :: nc                     !
  real, intent(inout) :: visi(*)                !
  real, intent(inout) :: uv(2)                  !
  real, intent(in) :: ma                        !
  real, intent(in) :: mb                        !
  real, intent(in) :: mc                        !
  ! Local
  real :: mabc
  !
  integer :: i,ii,ir,iw
  real :: reel,imag
  !
  uv(1) = uv(1)/ma
  uv(2) = uv(2)/mb
  mabc  = abs(ma*mb)*mc
  do i=3,3*nc,3
    ir = i+5
    ii = i+6
    iw = i+7
    reel = visi(ir)
    imag = visi(ii)
    visi(ir) =   reel*mabc
    visi(ii) =   imag*mabc
  enddo
end subroutine domult

