program uv_average
  use gildas_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! GILDAS
  !	Compress an input UV table
  !---------------------------------------------------------------------
  ! Local
  character(len=filename_length) :: uvdata,uvsort,uvchannels
  character(len=12) :: ctype
  logical :: error
  !
  ! Code:
  call gildas_open
  call gildas_char('UV_INPUT$',uvdata)
  call gildas_char('UV_OUTPUT$',uvsort)
  call gildas_char('RANGES$',uvchannels) 
  call gildas_char('CTYPE$',ctype)
  call gildas_close
  !
  call sub_uv_average(uvdata,uvsort,uvchannels,ctype,error)
  if (error) call sysexi(fatale)
end program uv_average
!
subroutine sub_uv_average(uvdata,uvsort,uvchannels,ctype,error)
  use gildas_def
  use gkernel_interfaces
  use image_def
  use gbl_format
  use gbl_message
  !---------------------------------------------------------------------
  ! GILDAS
  !	Compress an input UV table
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: uvdata
  character(len=*), intent(in) :: uvsort
  character(len=*), intent(in) :: uvchannels
  character(len=*), intent(in) :: ctype
  logical, intent(out) :: error
  ! Local
  character(len=*), parameter :: pname= 'UV_AVERAGE'
  character(len=12) :: dtype
  character(len=80) :: mess
  type(gildas) :: x,y
  character(len=filename_length) :: text
  integer, parameter :: mranges=50
  integer :: nc(mranges),nchan,numchan,nsum,i,nblock
  integer(4) :: ier
  !
  error = .false.
  !
  if (len_trim(uvdata).le.0 .or. len_trim(uvsort).le.0) then
    call map_message(seve%e,pname,'Missing file name')
    error = .true.
    return
  endif
  !
  ! Input file
  call gildas_null(y, type= 'UVT' )
  call gdf_read_gildas (y, uvdata, '.uvt', error, data=.false.)
  if (error) then
    call map_message(seve%e,pname,'Cannot read input table')
    return
  endif
  !
  ! Read number of channels
  dtype = ctype
  call get_ranges(pname,uvchannels,dtype,mranges,numchan,nc,y, error)
  if (error) return
  !
  ! Define number of channels being added and effective mean channel
  nchan = 0
  nsum = 0
  do i=2,numchan,2
    ! Sanity: reject ranges off the total range, truncate ranges
    ! overlapping boundary. NB: get_ranges ensures nc(i-1)<=nc(i).
    if (nc(i).lt.1 .or. nc(i-1).gt.y%gil%nchan) then
      write(text,'(3(a,i0))')  &
        'Range #',i/2,' out of boundaries: ',nc(i-1),' to ',nc(i)
      call map_message(seve%e,pname,text)
      error = .true.
      return
    endif
    if (nc(i-1).lt.1 .or. nc(i).gt.y%gil%nchan) then
      write(text,'(3(a,i0))')  &
        'Range #',i/2,' overlaps boundaries: ',nc(i-1),' to ',nc(i)
      call map_message(seve%w,pname,text)
      if (nc(i-1).lt.1)          nc(i-1) = 1
      if (nc(i).gt.y%gil%nchan)  nc(i) = y%gil%nchan
    endif
    !
    nchan = nc(i)-nc(i-1)+1+nchan
    nsum = (nc(i)+nc(i-1))*(nc(i)-nc(i-1)+1)/2+nsum
    write (text,'(2(A,I0))') 'Averaging from ',nc(i-1),' to ',nc(i)
    call map_message(seve%i,pname,text)
  enddo
  !
  ! Define output image
  call gildas_null (x, type = 'UVT')
  call gdf_copy_header (y, x, error)
  !
  ! Do not forget trailing columns
  x%gil%dim(1) = 10+x%gil%ntrail
  x%gil%inc(1) = x%gil%inc(1)*nchan
  x%gil%ref(1) = 1.d0-(float(nsum)/nchan-x%gil%ref(1))/nchan
  x%gil%vres = nchan*x%gil%vres
  x%gil%fres = nchan*x%gil%fres
  x%gil%nchan = 1
  call gdf_uv_shift_columns(y,x)
  call gdf_setuv (x, error)
  if (error) return
  !
  ! Define blocking factor
  call gdf_nitems('SPACE_GILDAS',nblock,y%gil%dim(1)) ! Visibilities at once
  nblock = min(nblock,y%gil%dim(2))
  allocate (y%r2d(y%gil%dim(1),nblock), x%r2d(x%gil%dim(1),nblock), stat=ier)
  if (ier.ne.0) then
    write(mess,*) 'Memory allocation error ',y%gil%dim(1), nblock
    call map_message(seve%e,pname,mess)
    error = .true.
    goto 10
  endif
  !
  ! Now create the file
  call sic_parse_file(uvsort,' ','.uvt',x%file)
  call gdf_create_image(x,error)
  if (error) goto 10
  !
  ! Loop over line table
  x%blc = 0
  x%trc = 0
  do i=1,x%gil%dim(2),nblock
    write(mess,*) i,' / ',x%gil%dim(2),nblock
    call map_message(seve%d,pname,mess) 
    x%blc(2) = i
    x%trc(2) = min(x%gil%dim(2),i-1+nblock) 
    y%blc(2) = i
    y%trc(2) = x%trc(2)
    call gdf_read_data(y,y%r2d,error)
    call average (x%r2d, x%gil%dim(1),x%trc(2)-x%blc(2)+1,   &
     &    x%gil%nlead,  x%gil%ntrail,               &
     &    y%r2d ,y%gil%dim(1),nc,numchan)
    call gdf_write_data (x,x%r2d,error)
    if (error) goto 10
  enddo
  !
  ! Finalize the image
  call gdf_close_image(x,error)
  call gdf_close_image(y,error)
10 deallocate(x%r2d,y%r2d)
end subroutine sub_uv_average
!
subroutine average (out,nx,nv,nlead,ntrail,inp,ny,nc,num)
  use gildas_def
  integer(kind=index_length) :: nx  ! Size of output visibility
  integer(kind=index_length) :: nv  ! Number of visibilities
  integer, intent(in) :: nlead      ! Leading columns
  integer, intent(in) :: ntrail     ! Trailing columns
  real :: out(nx,nv)                ! Output visibilities
  integer(kind=index_length) :: ny  ! Size of input visibility
  real :: inp(ny,nv)                ! Input visibilities
  integer :: num                    ! Number of ranges X 2
  integer :: nc(num)                ! Range boundaries
  ! Local
  integer :: k,kk,l
  integer(kind=index_length) :: j
  real :: a,b,c
  !
  do j=1,nv
    out(1:nlead,j) = inp(1:nlead,j)
    a = 0.0
    b = 0.0
    c = 0.0
    do l=2,num,2
      do k=nc(l-1),nc(l)
        kk = nlead+3*k
        if (inp(kk,j).ne.0) then
          a = a+inp(kk-2,j)*inp(kk,j)
          b = b+inp(kk-1,j)*inp(kk,j)
          c = c+inp(kk  ,j)
        endif
      enddo
    enddo
    if (c.ne.0.0) then
      out(8,j) =a/c
      out(9,j) =b/c
      out(10,j)=c              ! time*band
    else
      out(8,j) =0.0
      out(9,j) =0.0
      out(10,j)=0.0
    endif
    if (ntrail.gt.0) out(11:nx,j) = inp(ny-ntrail+1:ny,j)
  enddo
end subroutine average
!
subroutine get_ranges(pname,uvchannel,ctype,mranges,numchan,nc,y,error)
  use image_def
  use gbl_message
  use gkernel_interfaces
  !
  character(len=*), intent(in) :: pname
  character(len=*), intent(in) :: uvchannel
  character(len=*), intent(inout) :: ctype
  integer, intent(in) :: mranges
  integer, intent(out) :: numchan
  integer, intent(out) :: nc(mranges)
  type(gildas), intent(in) :: y
  logical, intent(out) :: error
  !
  integer(4) :: ier, i, ntmp
  real(8), parameter :: rnone=-1D9 ! Impossible velocity, frequency or channel number 
  real(kind=8) :: drange(mranges)
  integer(kind=4), parameter :: mtype=3
  integer :: itype
  character(len=12) :: types(mtype),mytype
  data types /'CHANNEL','VELOCITY','FREQUENCY'/
  !
  call sic_upper(ctype)
  error = .false.
  call sic_ambigs(pname,ctype,mytype,itype,types,mtype,error)
  if (error)  return
  !
  !
  drange = rnone   ! Impossible velocity, frequency or channel number 
  nc = 0                         ! This initialization is essential...
  read (uvchannel,*,iostat=ier) drange
  if (ier.gt.0) then
    call putios('E-'//pname//',  ',ier) 
    write(6,*) uvchannel
    error = .true.
    return
  endif
  ! 
  numchan = 0
  i = 2
  do while (i.lt.mranges)
    if (drange(i).ne.rnone.and.drange(i-1).ne.rnone) numchan = numchan+2
    i = i+2
  enddo
  !
  ! Convert to channels
  !
  if (numchan.eq.0) then
    numchan = 2
    nc(1) = 1
    nc(2) = y%gil%nchan 
  else if (mytype.eq.'CHANNEL') then
    nc(1:numchan) = nint(drange(1:numchan))
  else if (mytype.eq.'VELOCITY') then
    ! drange = nc - y%gil%ref(y%gil%faxi) ) * y%gil%vres + y%gil%voff 
    nc(1:numchan) = (drange(1:numchan) - y%gil%voff) / y%gil%vres + y%gil%ref(y%gil%faxi)
  else if (mytype.eq.'FREQUENCY') then
    ! drange = nc - y%gil%ref(y%gil%faxi) ) * y%gil%vres + y%gil%voff 
    nc(1:numchan) = (drange(1:numchan) - y%gil%freq) / y%gil%fres + y%gil%ref(y%gil%faxi)
  else
    call map_message(seve%e,pname,'Type of value '''//trim(mytype)//''' not supported')
    error = .true.
    return
  endif
  !
  do i=2,numchan,2
    if (nc(i).lt.nc(i-1)) then
      ntmp = nc(i)
      nc(i) = nc(i-1)
      nc(i-1) = ntmp
    endif
  enddo
end subroutine get_ranges
!
