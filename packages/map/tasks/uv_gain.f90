!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This file starts with the mapping module that wraps the LAPACK routines
! needed in the main task.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module mapping_linear_algebra_tool
  public :: mth_dpotrf
  public :: mth_dpotrs
  public :: code_upper_triangle
  public :: code_lower_triangle
  private
  !
  character(len=*), parameter :: code_upper_triangle='U'
  character(len=*), parameter :: code_lower_triangle='L'
  !
contains
  !
  subroutine mth_fail(pname,rname,info,error)
    use gbl_message
    !---------------------------------------------------------------------
    ! Error handling routine
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: pname
    character(len=*), intent(in)    :: rname
    integer(kind=4),  intent(in)    :: info
    logical,          intent(inout) :: error
    !
    character(len=60) :: chain
    !
    if (info.ne.0) then
       write(chain,'(a,a,a,i4)') 'ERROR in ',trim(rname),', info = ',info
       call map_message(seve%e,pname,chain)
       error = .true.
       return
    endif
  end subroutine mth_fail
  !
  subroutine mth_dpotrf(pname,uplo,n,a,lda,error)
    character(len=*), intent(in)    :: pname
    character(len=*) :: uplo
    integer(kind=4)  :: n
    integer(kind=4)  :: lda
    real(kind=8)     :: a(lda,*)
    logical,         intent(inout) :: error
    !
    integer(kind=4) :: info
    character(len=*), parameter :: rname='MTH>DPOTRF'
    !
    !  Purpose
    !  =======
    !
    !  DPOTRF computes the Cholesky factorization of a real symmetric
    !  positive definite matrix A.
    !
    !  The factorization has the form
    !     A = U**T * U,  if UPLO = 'U', or
    !     A = L  * L**T,  if UPLO = 'L',
    !  where U is an upper triangular matrix and L is lower triangular.
    !
    !  This is the block version of the algorithm, calling Level 3 BLAS.
    !
    !  Arguments
    !  =========
    !
    !  UPLO    (input) CHARACTER*1
    !          = 'U':  Upper triangle of A is stored;
    !          = 'L':  Lower triangle of A is stored.
    !
    !  N       (input) INTEGER
    !          The order of the matrix A.  N >= 0.
    !
    !  A       (input/output) DOUBLE PRECISION array, dimension (LDA,N)
    !          On entry, the symmetric matrix A.  If UPLO = 'U', the leading
    !          N-by-N upper triangular part of A contains the upper
    !          triangular part of the matrix A, and the strictly lower
    !          triangular part of A is not referenced.  If UPLO = 'L', the
    !          leading N-by-N lower triangular part of A contains the lower
    !          triangular part of the matrix A, and the strictly upper
    !          triangular part of A is not referenced.
    !
    !          On exit, if INFO = 0, the factor U or L from the Cholesky
    !          factorization A = U**T*U or A = L*L**T.
    !
    !  LDA     (input) INTEGER
    !          The leading dimension of the array A.  LDA >= max(1,N).
    !
    !  INFO    (output) INTEGER
    !          = 0:  successful exit
    !          < 0:  if INFO = -i, the i-th argument had an illegal value
    !          > 0:  if INFO = i, the leading minor of order i is not
    !                positive definite, and the factorization could not be
    !                completed.
    !
    ! Call LAPACK routine
    call dpotrf(uplo,n,a,lda,info)
    call mth_fail(pname,rname,info,error)
    if (error) return
  end subroutine mth_dpotrf
  !
  subroutine mth_dpotrs(pname,uplo,n,nrhs,a,lda,b,ldb,error)
    character(len=*), intent(in)    :: pname
    character(len=*) :: uplo
    integer(kind=4)  :: n
    integer(kind=4)  :: nrhs
    integer(kind=4)  :: lda
    real(kind=8)     :: a(lda,*)
    integer(kind=4)  :: ldb
    real(kind=8)     :: b(ldb,*)
    logical,         intent(inout) :: error
    !
    integer(kind=4) :: info
    character(len=*), parameter :: rname='MTH>DPOTRS'
    !
    !  Purpose
    !  =======
    !
    !  DPOTRS solves a system of linear equations A*X = B with a symmetric
    !  positive definite matrix A using the Cholesky factorization
    !  A = U**T*U or A = L*L**T computed by DPOTRF.
    !
    !  Arguments
    !  =========
    !
    !  UPLO    (input) CHARACTER*1
    !          = 'U':  Upper triangle of A is stored;
    !          = 'L':  Lower triangle of A is stored.
    !
    !  N       (input) INTEGER
    !          The order of the matrix A.  N >= 0.
    !
    !  NRHS    (input) INTEGER
    !          The number of right hand sides, i.e., the number of columns
    !          of the matrix B.  NRHS >= 0.
    !
    !  A       (input) DOUBLE PRECISION array, dimension (LDA,N)
    !          The triangular factor U or L from the Cholesky factorization
    !          A = U**T*U or A = L*L**T, as computed by DPOTRF.
    !
    !  LDA     (input) INTEGER
    !          The leading dimension of the array A.  LDA >= max(1,N).
    !
    !  B       (input/output) DOUBLE PRECISION array, dimension (LDB,NRHS)
    !          On entry, the right hand side matrix B.
    !          On exit, the solution matrix X.
    !
    !  LDB     (input) INTEGER
    !          The leading dimension of the array B.  LDB >= max(1,N).
    !
    !  INFO    (output) INTEGER
    !          = 0:  successful exit
    !          < 0:  if INFO = -i, the i-th argument had an illegal value
    !
    ! Call LAPACK routine
    call dpotrs(uplo,n,nrhs,a,lda,b,ldb,info)
    call mth_fail(pname,rname,info,error)
    if (error) return
  end subroutine mth_dpotrs
  !
  subroutine mth_dpbtrf(pname,uplo,n,kd,ab,ldab,error)
    !---------------------------------------------------------------------
    ! This one is unused in this task!
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: pname
    character(len=*) :: uplo
    integer(kind=4)  :: n
    integer(kind=4)  :: kd
    integer(kind=4)  :: ldab
    real(kind=8)     :: ab(ldab,*)
    logical          :: error
    !
    integer(kind=4) :: info
    character(len=*), parameter :: rname='MTH>DPBTRF'
    !
    !  Purpose
    !  =======
    !
    !  DPBTRF computes the Cholesky factorization of a real symmetric
    !  positive definite band matrix A.
    !
    !  The factorization has the form
    !     A = U**T * U,  if UPLO = 'U', or
    !     A = L  * L**T,  if UPLO = 'L',
    !  where U is an upper triangular matrix and L is lower triangular.
    !
    !  Arguments
    !  =========
    !
    !  UPLO    (input) CHARACTER*1
    !          = 'U':  Upper triangle of A is stored;
    !          = 'L':  Lower triangle of A is stored.
    !
    !  N       (input) INTEGER
    !          The order of the matrix A.  N >= 0.
    !
    !  KD      (input) INTEGER
    !          The number of superdiagonals of the matrix A if UPLO = 'U',
    !          or the number of subdiagonals if UPLO = 'L'.  KD >= 0.
    !
    !  AB      (input/output) DOUBLE PRECISION array, dimension (LDAB,N)
    !          On entry, the upper or lower triangle of the symmetric band
    !          matrix A, stored in the first KD+1 rows of the array.  The
    !          j-th column of A is stored in the j-th column of the array AB
    !          as follows:
    !          if UPLO = 'U', AB(kd+1+i-j,j) = A(i,j) for max(1,j-kd)<=i<=j;
    !          if UPLO = 'L', AB(1+i-j,j)    = A(i,j) for j<=i<=min(n,j+kd).
    !
    !          On exit, if INFO = 0, the triangular factor U or L from the
    !          Cholesky factorization A = U**T*U or A = L*L**T of the band
    !          matrix A, in the same storage format as A.
    !
    !  LDAB    (input) INTEGER
    !          The leading dimension of the array AB.  LDAB >= KD+1.
    !
    !  INFO    (output) INTEGER
    !          = 0:  successful exit
    !          < 0:  if INFO = -i, the i-th argument had an illegal value
    !          > 0:  if INFO = i, the leading minor of order i is not
    !                positive definite, and the factorization could not be
    !                completed.
    !
    !  Further Details
    !  ===============
    !
    !  The band storage scheme is illustrated by the following example, when
    !  N = 6, KD = 2, and UPLO = 'U':
    !
    !  On entry:                       On exit:
    !
    !      *    *   a13  a24  a35  a46      *    *   u13  u24  u35  u46
    !      *   a12  a23  a34  a45  a56      *   u12  u23  u34  u45  u56
    !     a11  a22  a33  a44  a55  a66     u11  u22  u33  u44  u55  u66
    !
    !  Similarly, if UPLO = 'L' the format of A is as follows:
    !
    !  On entry:                       On exit:
    !
    !     a11  a22  a33  a44  a55  a66     l11  l22  l33  l44  l55  l66
    !     a21  a32  a43  a54  a65   *      l21  l32  l43  l54  l65   *
    !     a31  a42  a53  a64   *    *      l31  l42  l53  l64   *    *
    !
    !  Array elements marked * are not used by the routine.
    !
    ! Call LAPACK routine
    call dpbtrf(uplo,n,kd,ab,ldab,info)
    call mth_fail(pname,rname,info,error)
    if (error) return
  end subroutine mth_dpbtrf
  !
  subroutine mth_dpbtrs(pname,uplo,n,kd,nrhs,ab,ldab,b,ldb,error)
    !---------------------------------------------------------------------
    ! This one is unused in this task!
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: pname
    character(len=*) :: uplo
    integer(kind=4)  :: n
    integer(kind=4)  :: kd
    integer(kind=4)  :: nrhs
    integer(kind=4)  :: ldab
    real(kind=8)     :: ab(ldab,*)
    integer(kind=4)  :: ldb
    real(kind=8)     :: b(ldb,*)
    logical,         intent(inout) :: error
    !
    integer(kind=4) :: info
    character(len=*), parameter :: rname='MTH>DPBTRS'
    !
    !  Purpose
    !  =======
    !
    !  DPBTRS solves a system of linear equations A*X = B with a symmetric
    !  positive definite band matrix A using the Cholesky factorization
    !  A = U**T*U or A = L*L**T computed by DPBTRF.
    !
    !  Arguments
    !  =========
    !
    !  UPLO    (input) CHARACTER*1
    !          = 'U':  Upper triangular factor stored in AB;
    !          = 'L':  Lower triangular factor stored in AB.
    !
    !  N       (input) INTEGER
    !          The order of the matrix A.  N >= 0.
    !
    !  KD      (input) INTEGER
    !          The number of superdiagonals of the matrix A if UPLO = 'U',
    !          or the number of subdiagonals if UPLO = 'L'.  KD >= 0.
    !
    !  NRHS    (input) INTEGER
    !          The number of right hand sides, i.e., the number of columns
    !          of the matrix B.  NRHS >= 0.
    !
    !  AB      (input) DOUBLE PRECISION array, dimension (LDAB,N)
    !          The triangular factor U or L from the Cholesky factorization
    !          A = U**T*U or A = L*L**T of the band matrix A, stored in the
    !          first KD+1 rows of the array.  The j-th column of U or L is
    !          stored in the j-th column of the array AB as follows:
    !          if UPLO ='U', AB(kd+1+i-j,j) = U(i,j) for max(1,j-kd)<=i<=j;
    !          if UPLO ='L', AB(1+i-j,j)    = L(i,j) for j<=i<=min(n,j+kd).
    !
    !  LDAB    (input) INTEGER
    !          The leading dimension of the array AB.  LDAB >= KD+1.
    !
    !  B       (input/output) DOUBLE PRECISION array, dimension (LDB,NRHS)
    !          On entry, the right hand side matrix B.
    !          On exit, the solution matrix X.
    !
    !  LDB     (input) INTEGER
    !          The leading dimension of the array B.  LDB >= max(1,N).
    !
    !  INFO    (output) INTEGER
    !          = 0:  successful exit
    !          < 0:  if INFO = -i, the i-th argument had an illegal value
    !
    ! Call LAPACK routine
    call dpbtrs(uplo,n,kd,nrhs,ab,ldab,b,ldb,info)
    call mth_fail(pname,rname,info,error)
    if (error) return
  end subroutine mth_dpbtrs
end module mapping_linear_algebra_tool
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module mapping_ante_gain_types
  !
  ! Maximum size of problem
  integer(kind=4), parameter :: mant=256  ! ALMA uses stations rather than antennas
  integer(kind=4), parameter :: mbas=mant*(mant-1)/2
  !
  ! 1 date-time antenna gain description
  type :: ante_gain_t
    real(kind=8)    :: time          ! Date-time
    integer(kind=4) :: iref          ! Reference antenna
    integer(kind=4) :: nant          ! Number of antennas for this date-time
    integer(kind=4) :: la(mant)      ! Sequential number of Physical number
    integer(kind=4) :: al(mant)      ! Physical number of Sequential number
    real(kind=8)    :: ca(mant)      ! Amplitude correction
    real(kind=8)    :: cp(mant)      ! [rad] Phase correction
    real(kind=4)    :: ampsnr(mant)  !
    logical         :: isbad(mant)   !
  end type ante_gain_t
  !
  ! All date-time antenna gains description
  type :: all_ante_gain_t
    integer(kind=4)                :: n = 0  ! Number of times in use
    type(ante_gain_t), allocatable :: ag(:)  !
  contains
    procedure :: reallocate => all_ante_gain_reallocate
  end type all_ante_gain_t
  !
contains
  !
  subroutine all_ante_gain_reallocate(aag,n,error)
    !-------------------------------------------------------------------
    ! Reallocate a all_ante_gain_t to store at least 'n' times
    !-------------------------------------------------------------------
    class(all_ante_gain_t), intent(inout) :: aag
    integer(kind=4),        intent(in)    :: n
    logical,                intent(inout) :: error
    ! Local
    integer(kind=4), parameter :: min_alloc=100
    integer(kind=4) :: osize,nsize,i
    type(all_ante_gain_t) :: tmp
    !
    if (allocated(aag%ag)) then
      osize = size(aag%ag)
      if (osize.ge.n)  return
      nsize = osize*2
    else
      osize = 0
      nsize = min_alloc
    endif
    nsize = max(nsize,n)
    !
    if (osize.gt.0) then
      call move_alloc(from=aag%ag,to=tmp%ag)
      tmp%n = osize
    endif
    allocate(aag%ag(nsize))
    if (osize.gt.0) then
      do i=1,tmp%n
        aag%ag(i) = tmp%ag(i)
      enddo
      aag%n = tmp%n
      deallocate(tmp%ag)
    endif
  end subroutine all_ante_gain_reallocate
end module mapping_ante_gain_types
!
module mapping_selfcal_gain_tool
  !
  ! Number of output columns in gain tables: 7 lead + RIW + 1 trail
  integer(kind=4), parameter :: ngcol=11
  !
  integer(kind=4), parameter :: ucol    = 1  ! hx%gil%column_pointer(code_uvt_u)
  integer(kind=4), parameter :: vcol    = 2  ! hx%gil%column_pointer(code_uvt_v)
  integer(kind=4), parameter :: datecol = 4  ! hx%gil%column_pointer(code_uvt_date)
  integer(kind=4), parameter :: timecol = 5  ! hx%gil%column_pointer(code_uvt_time)
  integer(kind=4), parameter :: anticol = 6  ! hx%gil%column_pointer(code_uvt_anti)
  integer(kind=4), parameter :: antjcol = 7  ! hx%gil%column_pointer(code_uvt_antj)
  integer(kind=4), parameter :: rcol    = 8  ! Real value for 1st channel
  integer(kind=4), parameter :: icol    = 9  ! Imaginary value for 1st channel
  integer(kind=4), parameter :: wcol    = 10 ! Weight value for 1st channel
  ! Freq offset column number in gain tables:
  integer(kind=4), parameter :: gfreq   = 11 ! Always last
  !
  type :: gain_dt_t  ! Gain date-times bookkeeping
    integer(kind=4)              :: date0     ! [gag] Arbitrary reference date for 'times'
    real(kind=8),    allocatable :: times(:)  ! [sec] Compact times for sorting and searching
    integer(kind=4), allocatable :: odates(:) ! [gag] Original dates from the table
    real(kind=4),    allocatable :: otimes(:) ! [sec] Orginal times from the table
  end type gain_dt_t
  !
  public :: do_base_gain,do_ante_gain,do_plot_gain,do_apply_cal
  public :: ngcol
  public :: gain_dt_t
  private
  !
contains
  !
  subroutine do_base_gain(do_amp,do_pha,nvis,ichan,  &
    data,ufreqs,model,gain,index,gdt,freqs,error)
    use gkernel_interfaces
    use uv_sort
    use uv_sort_codes
    !---------------------------------------------------------------------
    ! Compute the Baseline-Based Gain by comparing the Data to the Model,
    ! retaining either Amplitude or Phase (or both, but this is not used by
    ! the calling routines so far)
    !
    ! We use channel Ichan from Data table for the Data.  Model and Gain are
    ! assumed to be continuum (i.e. single-channel) tables.  Gain will
    ! contain the observed baseline gains, in an increasing freq-then-time
    ! order.
    !   Times is a real(kind=8) array to sort out date/time
    !   Index is an integer array yielding the ordering
    !     gain(i) = data/model (Index(i))
    !---------------------------------------------------------------------
    logical,         intent(in)    :: do_amp            ! Calibrate amplitude
    logical,         intent(in)    :: do_pha            ! Calibrate phase
    integer(kind=4), intent(in)    :: nvis              ! Number of visibilities
    integer(kind=4), intent(in)    :: ichan             ! Reference column (channel)
    ! Input UV data and arrays:
    real(kind=4),    intent(in)    :: data(:,:)         ! [:,nvis] Unsorted raw visibilities (with possibly trailing columns)
    real(kind=8),    intent(in)    :: ufreqs(nvis)      ! [nvis]   Associated unsorted central frequencies
    real(kind=4),    intent(in)    :: model(:,:)        ! [:,nvis] Unsorted modeled visibilities (10 or more for trailing columns)
    ! Output UV gain and arrays:
    real(kind=4),    intent(out)   :: gain(ngcol,nvis)  ! Sorted gain solution
    integer(kind=4), intent(out)   :: index(nvis)       ! Sorting array
    type(gain_dt_t), intent(inout) :: gdt               ! Sorted times of visibilities
    real(kind=8),    intent(out)   :: freqs(nvis)       ! Sorted frequencies of visibilities
    logical,         intent(inout) :: error
    !
    character(len=*), parameter :: rname='DO>BASE>GAIN'
    integer(kind=4) :: iv,jv,i,k,ncol,ier
    complex(kind=4) :: zgain,zdata,zmodel
    real(kind=4) :: wgain,wdata,again
    ! real(kind=8) :: buff(1)  ! Sorting buffer (unused for code_sort_tf)
    real(kind=8), allocatable :: buff(:)  ! Sorting buffer
    logical :: sorted
    integer(kind=4), allocatable :: rindex(:)  ! Reverse sorting array
    !
    ncol = size(data,dim=1)
    allocate(rindex(nvis),buff(nvis),stat=ier)
    if (failed_allocate(rname,'rindex',ier,error))  return
    ! call uv_findtb(code_sort_ft,&  ! Sort by freq then datetime
    call uv_findtb(code_sort_tb,&    ! Sort by datetime then base
                   data,nvis,ncol,&
                   buff,index,rindex,sorted,error,&
                   freq=ufreqs)
    if (error) return
    !
    ! Compute sorted gain and sorted DAPS as by-products of the subroutine
    gdt%date0 = data(datecol,1)
    do iv=1,nvis
       jv = index(iv)
       ! DAPS
       gdt%odates(iv) = nint(data(datecol,jv))
       gdt%otimes(iv) = data(timecol,jv)
       gdt%times(iv)  = (data(datecol,jv)-gdt%date0)*86400.d0+data(timecol,jv)
       freqs(iv) = ufreqs(jv)
       ! Gain
       do i=1, 7
          gain(i,iv) = data(i,jv)
       enddo
       k = (ichan-1)*3+7
       zdata = cmplx(data(k+1,jv),data(k+2,jv))
       wdata = data(k+3,jv)
       zmodel = cmplx(model(rcol,jv),model(icol,jv))
       if (zmodel.ne.0) then
          zgain = zdata/zmodel
          wgain = wdata*abs(zmodel)**2
          if (.not.do_amp) then
             again = abs(zmodel)
             ! Here Zgain = Zdata modified by a phase factor .
             ! If we have a good model of the data, Zgain will
             ! have module of order Unity
             zgain = zgain*again 
             wgain = wdata
          elseif (.not.do_pha) then
             ! Here Zgain is just an amplitude factor.
             ! Phases are not modified
             ! Again, if the model is good, Zgain will be close to Unity
             zgain = abs(zgain)  ! Zgain = An amplitude factor
          else  
             ! Normally, we do not do Phase and Amplitude at once
             ! so this section of code is normally Idle
             ! 
             ! Zgain has both the Phase and the Amplitude factor
             ! here, so no change needed
          endif
       else
          zgain = 0. ! It can be anything, since Wgain is Zero
          wgain = 0.
       endif
       gain(rcol,iv) = real(zgain)
       gain(icol,iv) = aimag(zgain)
       gain(wcol,iv) = wgain
       gain(gfreq,iv) = 0.  ! Set later
    enddo
  end subroutine do_base_gain
  !
  subroutine do_ante_gain(do_amp,do_pha,nvis,gdt,freqs,tinteg,snr,&
       basegain,antegain,refant,aag,error)
    use gildas_def
    use gbl_message
    use mapping_ante_gain_types
    !---------------------------------------------------------------------
    ! Compute complex gains (antenna-based solution) from a set of initial
    ! Antenna gains, smoothing in time as needed.
    !
    ! The initial Antenna gains must be sorted by increasing time
    !---------------------------------------------------------------------
    logical,         intent(in)    :: do_amp                ! Calibrate amplitude ?
    logical,         intent(in)    :: do_pha                ! Compute phase ?
    integer(kind=4), intent(in)    :: nvis                  ! Number of visibilities
    type(gain_dt_t), intent(in)    :: gdt                   ! Times of sorted visibilities
    real(kind=8),    intent(in)    :: freqs(nvis)           ! Frequencies of sorted visibilities
    real(kind=4),    intent(in)    :: tinteg                ! Integration time
    real(kind=4),    intent(in)    :: snr                   ! requested Signal to Noise 
    real(kind=4),    intent(in)    :: basegain(ngcol,nvis)  ! Input baseline gain array
    real(kind=4),    intent(inout) :: antegain(ngcol,nvis)  ! Output antenna gain array
    integer(kind=4), intent(in)    :: refant                ! Reference antenna
    type(all_ante_gain_t), intent(out), target :: aag       !
    logical,         intent(inout) :: error
    ! 
    integer(kind=4) :: iv,i,nbas,j,k,ia,ja,vgood,vbad,igood,isol
    real(kind=8) :: t,told,f,fold
    real(kind=4) :: ampli,phase
    integer(kind=4) :: iant(mbas),jant(mbas) ! Start & End antenna of baseline
    type(ante_gain_t), pointer :: ag
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='DO>ANTE>GAIN'
    !
    nbas = 0
    igood = 0
    isol = 0
    vgood = 0
    vbad = 0
    do j=2, mant
       do i=1, j-1
          nbas = nbas+1
          iant(nbas) = i
          jant(nbas) = j
       enddo
    enddo
    !
    ! Loop on visibilities
    !
    told = -1d10  ! An impossibly old time...
    fold = -1d10
    aag%n = 0
    do iv=1, nvis
       t = gdt%times(iv)
       f = freqs(iv)
       !
       ! Time change ?
       ! if (told.ne.t .or. fold.ne.f) then  ! Take frequencies into account
       if (told.ne.t) then  ! Do not take frequencies into account
          ! Identify a new time range
          call aag%reallocate(aag%n+1,error)
          if (error)  return
          aag%n = aag%n+1
          ag => aag%ag(aag%n)  ! For code readability
          !print *,'New output time ',iv,told,t,ntimes
          call do_ante_gain_current_time(do_amp,do_pha,nvis,gdt,tinteg,snr,  &
            basegain,refant,iant,jant,iv,igood,isol,ag,error)
          if (error)  return
          told = t
          fold = f
       endif
       !
       ! Fill in the antenna gains in "antegain"
       ! DAPS
       do k=1,7
          antegain(k,iv) = basegain(k,iv)
       enddo
       antegain(gfreq,iv) = 0.  ! Set later
       ! Channel
       ia = ag%la(nint(basegain(anticol,iv)))
       ja = ag%la(nint(basegain(antjcol,iv)))
       ampli = 1./exp(ag%ca(ia)+ag%ca(ja)) ! Not sure for this one...
       phase = - ag%cp(ia) + ag%cp(ja)     ! This is THE solution
       !! print *,'IV ',ampli, 180*phase/3.14159
       antegain(rcol,iv) = ampli * cos(phase)
       antegain(icol,iv) = ampli * sin(phase)
       !
       ! Flag data with no solution
       !! print *,'IV ',iv,' IA ',ia,ag%isbad(ia),' JA ',ja,ag%isbad(ja)
       if (ag%isbad(ia) .or. ag%isbad(ja)) then
          antegain(wcol,iv) = -abs(basegain(wcol,iv))
          vbad = vbad+1
       else
          antegain(wcol,iv) = basegain(wcol,iv)
          vgood = vgood+1
       endif
    enddo
    !
    write(mess,'(A,I0,A,I0,A)') "Found ",igood,"/",isol," good solutions"
    call map_message(seve%i,rname,mess)
    write(mess,'(A,I0,A,I0,A)') "Retained ",vgood," valid visibilities, ", &
         vbad," flagged ones"
    call map_message(seve%i,rname,mess)
  end subroutine do_ante_gain
  !
  subroutine do_ante_gain_current_time(do_amp,do_pha,nvis,gdt,tinteg,snr,  &
    basegain,refant,iant,jant,iv,igood,isol,ag,error)
    use phys_const
    use gbl_message
    use mapping_ante_gain_types
    !-------------------------------------------------------------------
    ! Compute gains for the current date-time. Called only once for
    ! each new date-time.
    !-------------------------------------------------------------------
    logical,           intent(in)    :: do_amp                ! Calibrate amplitude ?
    logical,           intent(in)    :: do_pha                ! Compute phase ?
    integer(kind=4),   intent(in)    :: nvis                  ! Number of visibilities
    type(gain_dt_t),   intent(in)    :: gdt                   ! Times of visibilities
    real(kind=4),      intent(in)    :: tinteg                ! Integration time
    real(kind=4),      intent(in)    :: snr                   ! requested Signal to Noise 
    real(kind=4),      intent(in)    :: basegain(ngcol,nvis)  ! Input baseline gain array
    integer(kind=4),   intent(in)    :: refant                ! Reference antenna
    integer(kind=4),   intent(in)    :: iant(mbas)            !
    integer(kind=4),   intent(in)    :: jant(mbas)            !
    integer(kind=4),   intent(in)    :: iv                    ! Current visibility
    integer(kind=4),   intent(inout) :: igood                 ! Running counter of good solutions
    integer(kind=4),   intent(inout) :: isol                  ! Running counter of solutions
    type(ante_gain_t), intent(out)   :: ag                    ! Antenna gain results
    logical,           intent(inout) :: error                 !
    !
    integer(kind=4) :: nbas
    logical :: ok
    real(kind=8) :: t
    integer(kind=4) :: jv,jvok,ia,ib,ja,lia,lja
    integer(kind=4) :: nantbad,icount,iflag,itest,kbad
    real(kind=4) :: maxsnr,minsnr
    integer(kind=4) :: antbad(mant)
    real(kind=4) :: ua(mant),va(mant)
    real(kind=4) :: aa(mant)
    real(kind=8) :: x(mbas),y(mbas),w(mbas),amp(mbas),pha(mbas),wa(mbas),ss(mbas)
    real(kind=8) :: wk2(mant,mant),wk3(mant),wp(mant)
    integer(kind=4) :: lpairs(mant),lpair(mant,mant)
    logical, parameter :: only_with_closure=.false.
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='DO>ANTE>GAIN'
    !
    ag%time = gdt%times(iv)
    !
    ! Find first visibility >= ag%time-Tinteg/2
    jv = iv
    t = gdt%times(jv)
    jvok = jv
    do while (t.ge.ag%time-tinteg/2)
      jvok = jv
      if (jv.eq.1)  exit
      ! Try previous jv
      jv = jv - 1
      t = gdt%times(jv)
    enddo
    jv = jvok
    t = gdt%times(jvok)
    ag%la(:) = 0
    ag%ca(:) = 0.0
    ag%cp(:) = 0.0
    x = 0.0
    y = 0.0
    w = 0.0
    !
    ! Search how many antennas at this time, and average
    ! over all visibilities which fit the time range
    !
    ok = (jv.lt.nvis .and. t.le.ag%time+tinteg/2.)
    lpairs = 0
    lpair  = 0
    ag%nant = 0
    do while (ok)
      !
      ! Only Select the UN-FLAGGED data
      !
      if (basegain(wcol,jv).gt.0.0) then
        !
        ! Build the antenna pointer list "ag%la" and "ag%al"
        !
        ! In the end, we use this formula...
        !    ia = ag%la(nint(basegain(anticol,iv)))
        !    ja = ag%la(nint(basegain(antjcol,iv)))
        ! which figures out which sequence number is each antenna
        !
        ia = nint(basegain(anticol,jv))
        if (ag%la(ia).eq.0) then
          ag%nant = ag%nant+1
          ag%la(ia) = ag%nant
          ag%al(ag%nant) = ia ! The reverse pointer
        endif
        ja = nint(basegain(antjcol,jv))
        if (ag%la(ja).eq.0) then
          ag%nant = ag%nant+1
          ag%la(ja) = ag%nant
          ag%al(ag%nant) = ja
        endif
        !
        !! Build the antenna pairs (for debug only)
        !! lpairs(ag%la(ia)) = lpairs(ag%la(ia))+1
        !! lpair(lpairs(ag%la(ia)),ag%la(ia)) = ag%la(ja)
        !!
        !! lpairs(ag%la(ja)) = lpairs(ag%la(ja))+1
        !! lpair(lpairs(ag%la(ja)),ag%la(ja)) = ag%la(ia)

        ! Here we set in UA,VA the "Antenna" U,V coordinate
        ! relative to Antenna #1
        !   Note: we do not average UA,VA over time since it
        !   is only used to determine the Reference antenna,
        !   a nearly irrelevant number.
        lia = ag%la(ia)
        lja = ag%la(ja)
        if (lia.eq.1) then
          ua(lja) = basegain(ucol,jv)
          va(lja) = basegain(vcol,jv)
        elseif (lja.eq.1) then
          ua(lia) = -basegain(ucol,jv)
          va(lia) = -basegain(vcol,jv)
        endif
        !
        ! Here we average the visibility into the X,Y,W buffer
        ! we also set start antenna < end antenna
        if (lia.eq.lja) then
            print *,'Ignoring autocorrelation for ',lia,ag%al(lia)
        else
          ib = basenum(lia,lja)
          if (lia.lt.lja) then
            y(ib) = y(ib) + basegain(icol,jv)*basegain(wcol,jv)
          else !! if (lja.lt.lia) then
            y(ib) = y(ib) - basegain(icol,jv)*basegain(wcol,jv)
          endif
          x(ib) = x(ib) + basegain(rcol,jv)*basegain(wcol,jv)
          w(ib) = w(ib) + basegain(wcol,jv)
        endif
        !
      endif  ! End selection of Un-Flagged data
      !
      jv = jv + 1
      t = gdt%times(jv)
      ok = (jv.lt.nvis .and. t.le.ag%time+tinteg/2.)
    enddo
    !
    ! Get the reference antenna
    if (refant.ne.0) then  ! From user input
      ! This is wrong if antenna renumbering occured
      ! It should be ag%la(ag%iref) = refant
      ag%iref = refant
    else  ! Guess
      ag%iref = do_ante_gain_current_refant(ag%nant,ua,va)
    endif
    !
    ! Check if any antenna has no closure 
    nantbad = 0
    do lia=1,ag%nant
      icount = 0
      do lja=1,ag%nant
        if (lia.ne.lja) then
          ib = basenum(lia,lja)
          if (w(ib).ne.0) then
            icount = icount+1
          endif
        endif
      enddo
      if (icount.lt.2) then
        if (nantbad.eq.0) then
          write(mess,100)  gdt%odates(iv),gdt%otimes(iv),ag%iref
100       format('Date ',I0,' Time ',F8.1,' Ref ',i2,':')
          call map_message(seve%w,rname,mess)
        endif
        write(mess,'(3(a,i0))')  '  Antenna Seq. ',lia,' (Phys. ',ag%al(lia),') has no closure'
        call map_message(seve%w,rname,mess)
        nantbad = nantbad+1
        antbad(nantbad) = lia
        ag%isbad(lia) = .true.
      else
        ag%isbad(lia) = .false.
      endif
    enddo
    !
    if (nantbad.eq.ag%nant) then
      call map_message(seve%w,rname,'  No antenna can be calibrated at this time')
    else if (nantbad.ne.0) then
      write(mess,'(2x,i0,a)') nantbad,' antennas with no solution'
      call map_message(seve%w,rname,mess)
    endif
    !
    if (only_with_closure) then
      ! Keep only antennas with closure (and their baselines).
      ! The Cholesky factorization (DPOTRF) requires a real symmetric
      ! positive definite matrix A. If we keep antennas with some null
      ! baselines, it will fail.
      call do_ante_gain_current_compress(ag,x,y,w,error)
      if (error)  return
      nantbad = 0
    endif
    !
    nbas = ag%nant*(ag%nant-1)/2
    !
    ! Normalize the time averaged visibilities
    do ib=1, nbas
      if (w(ib).ne.0) then
        x(ib) = x(ib)/w(ib)
        y(ib) = y(ib)/w(ib)
      endif
    enddo
    !
    ! phases
    if (do_pha) then
      !
      ! Here, normally, each antenna should have at least
      ! 2 baselines, otherwise it is not possible...
      !
      if (nbas.gt.0) then
        ! Solve for amplitude first to estimate S/N ratio
        ! Use an intermediate weight array "wa" for this
        do ib=1, nbas
          if (w(ib).ne.0) then
            amp(ib) = log(x(ib)**2+y(ib)**2)/2.
            wa(ib)  = w(ib)*(x(ib)**2+y(ib)**2)
          else
            wa(ib) = 0.0
          endif
        enddo
        call gain_ant(1,nbas,iant,jant,ag%iref,ag%nant,amp,wa,wk2,wk3,ss,ag%ca,wp,error)
        if (error) then
          error = .false.
          maxsnr = 0.0
          minsnr = 0.0
        else
          do ia = 1, ag%nant
            isol = isol + 1
            aa(ia) = exp(2*ag%ca(ia))
            wa(ia) = wp(ia)/aa(ia)**2
            ag%ampsnr(ia) = aa(ia)*sqrt(wa(ia))*1e3
            if (ag%isbad(ia)) ag%ampsnr(ia) = 0.0 !S.Guilloteau
            !
            ! Reset amplitude to 1 now
            ag%ca(ia) = 0
          enddo
          maxsnr = maxval(ag%ampsnr(1:ag%nant))
          minsnr = maxsnr
          do ia = 1, ag%nant
            if (ag%ampsnr(ia).ne.0) minsnr = min(minsnr,ag%ampsnr(ia))
          enddo
        endif
      else
        maxsnr = 0.0
        minsnr = 0.0
      endif
      !
      if (maxsnr.gt.snr) then
        !
        ! If enough SNR, solve for the Phases for the good
        ! antennas. Use the initial weights "w" here.
        do ib=1, nbas
          if (w(ib).ne.0) then
            pha(ib) = atan2(y(ib),x(ib))
            !
            ! 1-Mar-2018 S.Guilloteau
            ! Normally, the phase error in radian is the Noise to Signal ratio,
            ! so the weight of that phase should be  (Signal/Noise)^2
            ! i.e. wa(ib) = (x(ib)^2+y(ib)^2) * w(ib)
            ! since w(ib) is the 1/Noise^2 for the (real, imaginary) parts,
            ! i.e. like used above for the Amplitude
            !
          else
            pha(ib) = 0.0
          endif
        enddo
        call gain_ant(2,nbas,iant,jant,ag%iref,ag%nant,pha,w,wk2,wk3,ss,ag%cp,wp,error)
        if (error)  error = .false.
        iflag = 0
        do ia =1, ag%nant
          !
          ! Keep only antenna solution with required snr
          if (ag%ampsnr(ia).lt.snr) then
            ag%cp(ia) = 0      ! Set phase correction to Zero
            itest = 0
            !
            ! Check whether this antenna was already expected
            ! to be without solution or not.
            do kbad=1,nantbad
              if (antbad(kbad).eq.ia) then
                !!print *,'** OK ** NO Solution for flagged antenna ',ia,ag%al(ia)
                itest = kbad
                exit
              endif
            enddo
            if (itest.eq.0) then
              print *,'** Warning *** Flagged one more antenna ',ia,ag%al(ia)
              ag%isbad(ia) = .true.
            endif
            iflag = iflag+1
          else if (ag%cp(ia).eq.0) then
             ! print *,'** Warning *** No solution for antenna',ia,ag%cp(ia)
             ! print *,' Amp SNR ',ag%ampsnr(ia),snr,aa(ia),wa(ia)
          else
            ! This was happening when AMPSNR was not set to Zero for
            ! antennas with no closure.
            !do kbad=1,nantbad
            !  if (antbad(kbad).eq.ia) then
            !    print *,'** Warning *** Solution for flagged antenna ',ia,ag%al(ia)
            !    print *,' Amp SNR ',ag%ampsnr(ia),snr,aa(ia),wa(ia)
            !    exit
            !  endif
            !enddo
            igood = igood + 1
          endif
        enddo
      else
        iflag = ag%nant
      endif
      if (iflag.ne.0) then
        write(mess,101) gdt%odates(iv),gdt%otimes(iv),ag%iref,maxsnr,minsnr,iflag
      else
        write(mess,101) gdt%odates(iv),gdt%otimes(iv),ag%iref,maxsnr,minsnr
      endif
101   format('Date ',I0,' Time ',F8.1,' Ref ',i2,' SNR: Max ',1PG13.5,' Min ',1PG13.5,1X,I0,' antennas ignored')
      call map_message(seve%i,rname,mess)
    endif
    !
    ! This is redundant: it has already been done above
    do ia=1,ag%nant
      if (ag%isbad(ia)) then
        ag%cp(ia) = 0.
      endif
    enddo
    !
    ! amplitudes next (because Phase reset CA = 0.0)
    if (do_amp) then
      ! Here, we should check that each antenna should have at least
      ! 3 baselines, otherwise it is not possible...
      !
      ! Here, we use the raw initial weights also, not the SNR weights
      do ib=1, nbas
        if (w(ib).ne.0) then
          amp(ib) = log(x(ib)**2+y(ib)**2)/2.
        endif
      enddo
      call gain_ant(1,nbas,iant,jant,ag%iref,ag%nant,amp,w,wk2,wk3,ss,ag%ca,wp,error)
      if (error) return
    endif
    !
    !!print *,'BAD ',ag%isbad(1:ag%nant)
  end subroutine do_ante_gain_current_time
  !
  function do_ante_gain_current_refant(nant,ua,va) result(refant)
    !-------------------------------------------------------------------
    ! Guess the reference antenna by locating the one with the shortest
    ! baselines.
    !-------------------------------------------------------------------
    integer(kind=4) :: refant
    integer(kind=4), intent(in) :: nant
    real(kind=4),    intent(in) :: ua(:),va(:)
    !
    real(kind=4) :: ug,vg
    integer(kind=4) :: ia
    real(kind=4) :: d2,dmin
    !
    ! Compute the centroid of antenna positions (still relative to Antenna #1)
    ug = 0
    vg = 0
    do ia=1,nant
      ug = ug+ua(ia)
      vg = vg+va(ia)
    enddo
    ug = ug/nant
    vg = vg/nant
    dmin = 1e10
    !
    ! Locate the antenna closest to centroid (this is independent of absolute pos.)
    refant = 1
    do ia=1,nant
      d2 = (ua(ia)-ug)**2 + (va(ia)-vg)**2
      if (d2.lt.dmin) then
        dmin = d2
        refant = ia
      endif
    enddo
  end function do_ante_gain_current_refant
  !
  function basenum(ia,ja)
    !-------------------------------------------------------------------
    ! Return the baseline number
    !-------------------------------------------------------------------
    integer(kind=4) :: basenum
    integer(kind=4), intent(in) :: ia,ja
    if (ia.lt.ja) then
      basenum = (ja-1)*(ja-2)/2+ia
    else !! if (ja.lt.ia) then
      basenum = (ia-1)*(ia-2)/2+ja
    endif
  end function basenum
  !
  subroutine do_ante_gain_current_compress(ag,x,y,w,error)
    use mapping_ante_gain_types
    !-------------------------------------------------------------------
    ! Recompute the buffers by keeping only the 'good' antennas (and the
    ! baselines between good antennas).
    !-------------------------------------------------------------------
    type(ante_gain_t), intent(inout) :: ag
    real(kind=8),      intent(inout) :: x(:)
    real(kind=8),      intent(inout) :: y(:)
    real(kind=8),      intent(inout) :: w(:)
    logical,           intent(inout) :: error
    !
    type(ante_gain_t) :: nag
    real(kind=8) :: nx(mbas),ny(mbas),nw(mbas)
    integer(kind=4) :: oanti,oantj,nanti,nantj,pant,nbas,obas
    !
    nag%nant = 0
    nag%la(:) = 0
    nx(:) = 0.d0
    ny(:) = 0.d0
    nw(:) = 0.d0
    do oantj=1,ag%nant       ! Old antenna sequential number
      if (ag%isbad(oantj))  cycle  ! Discard this antenna
      !
      nag%nant = nag%nant+1  ! New number of antennas
      !
      ! Antenna arrays
      nantj = nag%nant        ! New antenna sequential number
      pant = ag%al(oantj)     ! Physical number (preserved)
      nag%al(nantj) = pant    ! From sequential to physical
      nag%la(pant) = nantj    ! From physical to sequential
      nag%isbad(nantj) = .false.
      !
      ! Baseline arrays
      do nanti=1,nantj-1
        nbas = basenum(nanti,nantj)  ! New baseline number
        oanti = ag%la(nag%al(nanti))
        obas = basenum(oanti,oantj)  ! Old baseline number
        nx(nbas) = x(obas)
        ny(nbas) = y(obas)
        nw(nbas) = w(obas)
      enddo
    enddo
    !
    ! Overwrite arguments
    ag%nant = nag%nant
    ag%al(:) = nag%al(:)
    ag%la(:) = nag%la(:)
    ag%isbad(:) = nag%isbad(:)
    x(:) = nx(:)
    y(:) = ny(:)
    w(:) = nw(:)
  end subroutine do_ante_gain_current_compress
  !
  subroutine gain_ant(iy,nbas,iant,jant,iref,nant,y,w,wk2,wk3,ss,c,wc,error)
    use gkernel_interfaces
    use mapping_linear_algebra_tool
    !---------------------------------------------------------------------
    ! Compute a weighted least-squares approximation to the antenna
    ! amplitudes or phases for an arbitrary set of baseline data points.
    !---------------------------------------------------------------------
    integer(kind=4), intent(in)    :: iy             ! 1 for log(amplitude), 2 for phase
    integer(kind=4), intent(in)    :: nbas           ! the number of baselines
    integer(kind=4), intent(in)    :: iant(nbas)     ! start antenna for each baseline
    integer(kind=4), intent(in)    :: jant(nbas)     ! end  antenna for each baseline
    integer(kind=4), intent(in)    :: iref           ! Reference antenna for phases
    integer(kind=4), intent(in)    :: nant           ! the number of antennas
    real(kind=8),    intent(in)    :: y(nbas)        ! the data values
    real(kind=8),    intent(in)    :: w(nbas)        ! weights
    real(kind=8),    intent(inout) :: wk2(nant,nant) ! work space
    real(kind=8),    intent(inout) :: wk3(nant)      ! work space
    real(kind=8),    intent(out)   :: ss(nbas)       ! rms of fit for each baseline
    real(kind=8),    intent(out)   :: c(nant)        ! the gains
    real(kind=8),    intent(out)   :: wc(nant)       ! the weights
    logical,         intent(inout) :: error
    !
    integer(kind=4) :: i,ia,ja,ib,nantm1,l,iter
    real(kind=8) :: wi,ww,yi
    real(kind=8) :: norm
    real(kind=8), parameter :: tol=1d-14
    character(len=*), parameter :: rname='GAIN>ANT'    
    !
    ss = 0.
    !
    ! Check that the weights are positive.
    !
    do ib=1,nbas
       if (w(ib).lt.0.0d0) then
          call gagout('E-GAIN_ANT,  Weights not positive')
          error = .true.
          return
       endif
    enddo
    !
    if (iy.eq.1) then
       ! Amplitude case is simple...
       wk2 = 0.0d0
       wk3 = 0.0d0
       !
       ! store the upper-triangular part of the normal equations in wk2
       ! and the right-hand side in wk3.
       do ib=1,nbas
          wi = w(ib)
          if (wi.gt.0) then
             ia = iant(ib)
             ja = jant(ib)
             wk2(ia,ia) = wk2(ia,ia)+wi
             wk2(ia,ja) = wk2(ia,ja)+wi
             wk2(ja,ia) = wk2(ja,ia)+wi
             wk2(ja,ja) = wk2(ja,ja)+wi
          endif
       enddo
       do ib=1, nbas
          ia = iant(ib)
          ja = jant(ib)
          wi = w(ib)*y(ib)
          wk3(ia) = wk3(ia) + wi
          wk3(ja) = wk3(ja) + wi
       enddo
       !
       ! Solve the system of normal equations by first computing the Cholesky
       ! factorization
       call mth_dpotrf(rname,code_upper_triangle,nant,wk2,nant,error)
       if (error) return
       call mth_dpotrs(rname,code_upper_triangle,nant,1,wk2,nant,wk3,nant,error)
       if (error) return
       do i=1,nant
          c(i) = wk3(i)
          wc(i) = wk2(i,i)**2
       enddo
    elseif (iy.eq.2) then
       ! Phase is more complicated ...
       nantm1 = nant-1
       do i=1,nant
          c(i) = 0.0d0
          wc(i) = 0.0d0
       enddo
       !
       ! start iterating
       norm = 1e10
       iter = 0
       do while (norm.gt.tol .and. iter.lt.100)
          iter = iter + 1
          do i=1,nantm1
             do l=1,nantm1
                wk2(l,i) = 0.0d0
             enddo
             wk3(i) = 0.0d0
          enddo
          do ib=1,nbas
             wi = w(ib)
             if (wi.gt.0) then
                ia = zant(iant(ib),iref)
                ja = zant(jant(ib),iref)
                if (ia.ne.0) then
                   wk2(ia,ia) = wk2(ia,ia)+wi
                endif
                if (ja.ne.0) then
                   wk2(ja,ja) = wk2(ja,ja)+wi
                endif
                if (ia.ne.0 .and. ja.ne.0) then
                   wk2(ja,ia) = wk2(ja,ia)-wi
                   wk2(ia,ja) = wk2(ia,ja)-wi
                endif
             endif
          enddo
          do ib=1, nbas
             if (w(ib).gt.0) then
                yi = y(ib)
                ia = iant(ib)
                ja = jant(ib)
                yi = yi+(c(ia)-c(ja))
             else
                yi = 0
             endif
             yi = sin(yi)
             ia = zant(iant(ib),iref)
             ja = zant(jant(ib),iref)
             wi = w(ib)*yi
             if (ia.ne.0) then
                wk3(ia) = wk3(ia) - wi
             endif
             if (ja.ne.0) then
                wk3(ja) = wk3(ja) + wi
             endif
          enddo
          !
          ! Solve the system of normal equations by first computing the Cholesky
          ! factorization
          call mth_dpotrf(rname,code_upper_triangle,nantm1,wk2,nant,error)
          if (error) return
          call mth_dpotrs(rname,code_upper_triangle,nantm1,1,wk2,nant,wk3,nantm1,error)
          if (error) return
          !  Add the result to c
          norm = 0
          do ia=1,nant
             i = zant(ia,iref)
             if (i.ne.0) then
                ww = wk3(i)
                c(ia) = c(ia)+ww
                wc(ia) = wk2(i,i)**2
                norm = norm+ww**2
             endif
          enddo
       enddo
    endif
    !
  contains
    !
    function zant(i,r)
      !---------------------------------------------------------------------
      ! Return the apparent number of Antenna #i when reference
      ! antenna is #r. Since #r is not in the list of antennas for
      ! which a solution is to be searched, this number
      ! is #i for #i < #r, and #i-1 for #i > #r
      !---------------------------------------------------------------------
      integer(kind=4)             :: zant ! intent(out)
      integer(kind=4), intent(in) :: i
      integer(kind=4), intent(in) :: r
      !
      if (i.eq.r) then
         zant = 0
      elseif (i.gt.r) then
         zant = i-1
      else
         zant = i
      endif
      return
    end function zant
  end subroutine gain_ant
  !
  subroutine do_plot_gain(aag,uvsol,refant,error)
    use gildas_def
    use phys_const
    use gkernel_interfaces
    use image_def
    use gbl_message
    use mapping_ante_gain_types
    !---------------------------------------------------------------------
    ! Prepare the Solution Table: Amplitude and Phase
    ! corrections per antenna.
    !
    ! Convert the formatted temporary files to a Gildas Table
    !---------------------------------------------------------------------
    type(all_ante_gain_t), intent(in)    :: aag    ! All solutions
    character(len=*),      intent(in)    :: uvsol  ! Solution file name
    integer(kind=4),       intent(in)    :: refant ! Reference antenna
    logical,               intent(inout) :: error
    !
    type(gildas) :: tab
    character(len=filename_length) :: name
    integer(kind=4) :: nant,ier,i,j
    real(kind=8) :: time,ztime,date,secs
    integer(kind=4) :: jant,kant,ntimes,nantes
    real(kind=4) :: rphase
    ! Larger allocatable arrays (one could use re-allocation instead,
    ! but they are still small anyway)
    integer(kind=4), allocatable :: itmp(:),ialways(:)
    character(len=*), parameter :: rname='DO>PLOT>GAIN'
    !
    ntimes = aag%n
    nantes = 0
    do i=1,ntimes
      nantes = max(nantes,aag%ag(i)%nant)
    enddo
    !
    allocate(itmp(ntimes*nantes),ialways(ntimes*nantes),stat=ier)
    if (ier.ne.0) then
       error = .true.
       return
    endif
    !
    call gildas_null(tab)
    !
    name  = uvsol
    call sic_parse_file(name,' ','.tab',tab%file)
    call map_message(seve%i,rname,'Creating solution table '//trim(tab%file))
    !
    tab%gil%ndim = 2
    tab%gil%dim(1) = ntimes
    tab%gil%dim(2) = 4+2*nantes
    !
    ! Find the common antennas
    nant = aag%ag(1)%nant
    ialways(1:nant) = aag%ag(1)%al(1:nant)
    jant = nant
    do i=2,ntimes
       nant = aag%ag(i)%nant
       call union(ialways,jant,aag%ag(i)%al(1:nant),nant,itmp,kant)
       jant = kant
       ialways(1:kant) = itmp(1:kant)
    enddo ! i
    !
    write(*,*) kant,' antennas present:'
    write(*,'(16(1x,I3))') ialways(1:kant)
    !
    ! Normally, here we should use the "absolute" antenna number
    ! and the Table should be of size KANT. To be done...
    !
    allocate(tab%r2d(tab%gil%dim(1),tab%gil%dim(2)), stat=ier)
    !
    ztime = 0.d0
    tab%r2d = 0
    do i=1,ntimes
       if (ztime.eq.0.d0) ztime = aag%ag(i)%time
       time = aag%ag(i)%time-ztime
       secs = mod(time,86400.d0)
       date = (time-secs)/86400.d0 
       tab%r2d(i,1) = secs
       tab%r2d(i,2) = date     
       tab%r2d(i,3) = aag%ag(i)%nant
       tab%r2d(i,4) = aag%ag(i)%iref
       !
       if (refant.eq.0) then
         rphase = 0.
       else
         rphase = aag%ag(i)%cp(refant)
       endif
       do j=1,nant
          tab%r2d(i,3+2*j) = (aag%ag(i)%cp(j) - rphase)*deg_per_rad  ! [deg]
          tab%r2d(i,4+2*j) = aag%ag(i)%ampsnr(j)
       enddo ! j
    enddo ! i
    call gdf_write_image(tab,tab%r2d,error)
    deallocate(tab%r2d)
    !
  contains
    !
    subroutine overlap(a,n,b,m,c,k)
      !---------------------------------------------------------------------
      ! Find the intersection of a and b
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: n
      integer(kind=4), intent(in)    :: a(n)
      integer(kind=4), intent(in)    :: m
      integer(kind=4), intent(in)    :: b(m)
      integer(kind=4), intent(inout) :: k
      integer(kind=4), intent(inout) ::c(*)
      !
      integer(kind=4) :: i,j
      !
      k = 0
      do i=1,n
         do j=1,m
            if (a(i).eq.b(j)) then
               k = k+1
               c(k) = a(i)
               exit
            endif
         enddo ! j
      enddo ! i
      !! print *,'Overlap ',n,m,k,' = ',c(1:K)
    end subroutine overlap
    !
    subroutine union(a,n,b,m,c,k)
      !---------------------------------------------------------------------
      ! Find the union of a and b
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: n
      integer(kind=4), intent(in)    :: a(n)
      integer(kind=4), intent(in)    :: m
      integer(kind=4), intent(in)    :: b(m)
      integer(kind=4), intent(inout) :: k
      integer(kind=4), intent(inout) ::c(*)
      !
      integer(kind=4) :: i,j
      logical :: found
      !
      k = n
      c(1:k) = a(1:n)
      !
      do j=1,m
         found = .false.
         do i=1,n
            if (c(i).eq.b(j)) then
               found = .true.
               exit
            endif
         enddo ! i
         if (.not.found) then
            k = k+1
            c(k) = b(j)
         endif
      enddo ! j
    end subroutine union
  end subroutine do_plot_gain
  !
  subroutine do_apply_cal(ncol,nch,nvis,data,cal,gain,flag,index)
    use gbl_message
    !---------------------------------------------------------------------
    ! Apply Gain Table to input UV data set
    ! Index is the ordering of the gain. Gain(i) applies to Visi(Index(i))
    !---------------------------------------------------------------------
    integer(kind=4), intent(in)  :: ncol              ! Number of column in visi
    integer(kind=4), intent(in)  :: nvis              ! Number of visibilities
    integer(kind=4), intent(in)  :: nch               ! Number of channels
    real(kind=4),    intent(in)  :: data(ncol,nvis)   ! Unsorted raw data
    real(kind=4),    intent(out) :: cal(:,:)          ! [:,nvis] Unsorted calibrated data
    real(kind=4),    intent(in)  :: gain(ngcol,nvis)  ! Sorted gain array
    logical,         intent(in)  :: flag              ! Flag data with no solution?
    integer(kind=4), intent(in)  :: index(nvis)       ! Sorting array
    !
    integer(kind=4) :: iv,jv,k,nflag
    complex(kind=4) :: zdata,zgain,zcal
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='DO>APPLY>CAL'
    !
    nflag = 0
    do iv=1, nvis
       jv = index(iv)
       ! Leading DAPS
       do k=1, 7
          cal(k,jv) = data(k,jv)
       enddo
       ! Data
       zgain = cmplx(gain(rcol,iv),gain(icol,iv))
       if (gain(wcol,iv).lt.0) then
          nflag = nflag+1
          zgain = 0
       endif
       do k=8, 5+3*nch, 3
          if (zgain.ne.0) then
             zdata = cmplx(data(k,jv),data(k+1,jv))
             zcal = zdata / zgain
             cal(k,jv) = real(zcal)
             cal(k+1,jv) = aimag(zcal)
             cal(k+2,jv) = data(k+2,jv)*abs(zgain)**2
          else
             cal(k,jv) = data(k,jv)
             cal(k+1,jv) = data(k+1,jv)
             cal(k+2,jv) = data(k+2,jv)
             if (flag) cal(k+2,jv) = -abs(cal(k+2,jv))
          endif
       enddo
       ! Trailing DAPS. Note: the FREQ column in output table will be
       ! set later on
       if (8+3*nch.le.ncol) then
          cal(8+3*nch:ncol,jv) = data(8+3*nch:ncol,jv)
       endif
    enddo
    !
    write(mess,'(I0,A)') nflag,' flagged visibilities in gain array'
    if (nflag.eq.0) then
       continue
    elseif (flag) then
       mess = trim(mess)//', flagged in calibrated data (FLAG$ = YES)'
    else
       mess = trim(mess)//', not flagged and no calibration applied in calibrated data (FLAG$ = NO)'
    endif
    call map_message(seve%i,rname,mess)
  end subroutine do_apply_cal
end module mapping_selfcal_gain_tool
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
program uv_gain
  use gkernel_interfaces
  use gildas_def
  use image_def
  use gbl_format
  use gbl_message
  use mapping_selfcal_gain_tool
  use mapping_ante_gain_types
  !---------------------------------------------------------------------
  ! Compute Gains by comparing the UVT table and
  !       a UV model file.
  ! Input : a UV table (observed)
  !         a UV model
  ! Output: a UV table, with the gains.
  !         a UV calibrated table
  !---------------------------------------------------------------------
  type(gildas) :: gain,mod,raw,cal
  logical :: error
  logical :: flag           ! Flag data with no solution
  logical :: do_amp         ! Self calibrate amplitude
  logical :: do_pha         ! Self calibrate phase
  logical :: antenna_based  ! Antenna based self calibration
  integer(kind=4) :: ichan,nvis,ncol,ier
  real(kind=4) :: tinteg    ! Integration time
  real(kind=4) :: snr       ! Minimum SNR for a solution
  integer(kind=4) :: refant ! Reference antenna (0 = automatic)
  character(len=filename_length) :: uvraw,uvmod,uvgain,uvcal,uvsol ! File names
  !
  integer(kind=4), allocatable         :: index(:)   ! Sorting index (by time)
  real(kind=8),    allocatable         :: ufreqs(:)  ! Unsorted central frequency of visibilities
  real(kind=8),    allocatable         :: freqs(:)   ! Sorted central frequency of visibilities
  real(kind=4),    allocatable, target :: basegain(:,:),antegain(:,:) ! Gain arrays
  type(gain_dt_t) :: gdt
  type(all_ante_gain_t) :: aag
  character(len=*), parameter :: rname='UV_GAIN'
  !
  error = .false.
  !
  call gildas_open
  call gildas_char('UVDATA$',uvraw)
  call gildas_char('UVMODEL$',uvmod)
  call gildas_char('UVGAIN$',uvgain)
  call gildas_char('UVCAL$',uvcal)
  call gildas_real('TINTEG$',tinteg,1)
  call gildas_real('SNR$',snr,1)
  call gildas_logi('FLAG$',flag,1)
  call gildas_inte('CHANNEL$',ichan,1)
  call gildas_logi('DO_AMP$',do_amp,1)
  call gildas_logi('DO_PHA$',do_pha,1)
  call gildas_logi('ANTENNA_BASED$',antenna_based,1)
  call gildas_inte('REFERENCE_A$',refant,1)
  if (antenna_based) call gildas_char('UVSOL$',uvsol)
  call gildas_close
  !
  ! Sanity check
  if (uvraw.eq.uvcal) then
    call map_message(seve%f,rname,'Input and Self-calibrated table must differ')
    error = .true.
    goto 999
  endif
  !
  ! Read input
  call get_uvt(raw,'input',uvraw,error,ufreqs)
  if (error) goto 999
  call get_uvt(mod,'model',uvmod,error)
  if (error) goto 999
  !
  nvis = raw%gil%nvisi
  ncol = raw%gil%dim(1)
  if (antenna_based) then
    allocate(index(nvis),gdt%times(nvis),gdt%odates(nvis),gdt%otimes(nvis),   &
             freqs(nvis),basegain(ngcol,nvis),antegain(ngcol,nvis),stat=ier)
  else
    allocate(index(nvis),gdt%times(nvis),gdt%odates(nvis),gdt%otimes(nvis),   &
             freqs(nvis),basegain(ngcol,nvis),stat=ier)
  endif
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Cannot allocate gain arrays')
    goto 999
  endif
  !
  ! Get baseline-based gains
  call do_base_gain(do_amp,do_pha,nvis,ichan,  &
       raw%r2d,ufreqs,mod%r2d,basegain,index,gdt,freqs,error)
  if (error) goto 999
  !
  ! Mod table is not needed any more.
  deallocate(mod%r2d,stat=ier)
  call gdf_close_image(mod,error)
  if (error) goto 999
  !
  ! Compute antenna-based gains
  if (antenna_based) then
     call do_ante_gain(do_amp,do_pha,nvis,gdt,freqs,tinteg,snr, &
          basegain,antegain,refant,aag,error)
     if (error) goto 999
     call do_plot_gain(aag,uvsol,refant,error)
     if (error) goto 999
  endif
  !
  ! Prepare output
  call prepare_uvt_as(gain,'gain',uvgain,mod,error)
  if (error) goto 999
  if (antenna_based) then
    gain%r2d => antegain
  else
    gain%r2d => basegain  
  endif
  call prepare_uvt_as(cal,'calibrated',uvcal,raw,error)
  if (error) goto 999
  call gdf_allocate(cal,error)
  if (error) goto 999
  call do_apply_cal(ncol,raw%gil%nchan,nvis,raw%r2d,cal%r2d,gain%r2d,flag,index)
  !
  ! Fill the frequency DAPS column
  call add_uvt_freq(cal,ufreqs,error)  ! Unsorted CAL with unsorted freqs
  if (error) goto 999
  call add_uvt_freq(gain,freqs,error)  ! Sorted GAIN with sorted freqs
  if (error) goto 999
  !
  ! Write ouptut data
  call gdf_write_data(cal,cal%r2d,error)
  if (error) goto 999
  call gdf_write_data(gain,gain%r2d,error)
  if (error) goto 999
  !
  ! Clean
  if (allocated(index))     deallocate(index)
  if (allocated(gdt%times)) deallocate(gdt%times,gdt%odates,gdt%otimes)
  if (allocated(basegain))  deallocate(basegain)
  if (allocated(antegain))  deallocate(antegain)
  !
  call map_message(seve%i,rname,'Successful completion')
  call sysexi(1)
  !
999 call sysexi(fatale)
  !
contains
  !
  subroutine get_uvt(uvt,kind,name,error,freq)
    !-------------------------------------------------------------------
    ! Read the named file in the dedicated structure
    ! Optionally return a 'frequency' column describing the central
    ! frequency of each visibility (useful for pseudo-continuum tables
    ! which are not centered on the header frequency).
    !-------------------------------------------------------------------
    type(gildas),     intent(inout) :: uvt
    character(len=*), intent(in)    :: kind
    character(len=*), intent(in)    :: name
    logical,          intent(inout) :: error
    real(kind=8), optional, allocatable, intent(out) :: freq(:)
    !
    integer(kind=4) :: n,ier,cfreq
    integer(kind=8) :: iv
    character(len=filename_length) :: shortname
    !
    n = len_trim(name)
    if (n.le.0) return
    shortname = name(1:n)
    call gildas_null(uvt,type='UVT')
    call gdf_read_gildas(uvt,shortname,'.uvt',error)
    if (error) then
       call map_message(seve%e,rname,'Cannot read '//trim(kind)//'uv table')
       return
    endif
    !
    if (.not.present(freq))  return
    !
    ! Create the absolute frequency column, either from the offset
    ! frequency DAPS column, or from scratch. Note the column is returned
    ! unsorted (i.e. in the data order); use the 'index' array if sorted
    ! column is needed.
    allocate(freq(uvt%gil%nvisi),stat=ier)
    if (failed_allocate(rname,'freq array',ier,error)) return
    cfreq = uvt%gil%column_pointer(code_uvt_freq)
    if (cfreq.gt.0) then
      do iv=1,uvt%gil%nvisi
        freq(iv) = uvt%gil%val(uvt%gil%faxi) + uvt%r2d(cfreq,iv)  ! From offset freq to absolute freq.
      enddo
    else
      ! Fallback: header frequency is used for all visibilities
      freq(:) = uvt%gil%val(uvt%gil%faxi)
    endif
  end subroutine get_uvt
  !
  subroutine prepare_uvt_as(ou,kind,name,in,error)
    type(gildas),     intent(inout) :: ou
    character(len=*), intent(in)    :: kind
    character(len=*), intent(in)    :: name
    type(gildas),     intent(in)    :: in
    logical,          intent(inout) :: error 
    !
    integer(kind=4) :: n
    character(len=filename_length) :: shortname
    !
    call gildas_null(ou,type='UVT')
    !
    ! Set up header
    call gdf_copy_header(in,ou,error)
    if (error) return
    if (ou%gil%column_pointer(code_uvt_freq).gt.0) then
      ! Frequency column already present => nothing to do
    else
      call map_message(seve%i,rname,'Adding frequency offset column in DAPS')
      ! hcuv%gil%ntrail = huv%gil%ntrail+1  ! Not needed (automatic?)
      ou%gil%dim(1) = ou%gil%dim(1)+1
      ou%gil%column_pointer(code_uvt_freq) = ou%gil%dim(1)
      ou%gil%column_size(code_uvt_freq) = 1
    endif
    !
    ! Set up file
    n = len_trim(name)
    if (n.eq.0) return
    shortname = name(1:n)
    call sic_parsef(shortname,ou%file,' ','.uvt')
    call map_message(seve%i,rname,'Creating '//trim(kind)//' uv table '//trim(ou%file))
    call gdf_create_image(ou,error)
    if (error) then
       call map_message(seve%e,rname,'Cannot create '//trim(kind)//' uv table')
       return
    endif
  end subroutine prepare_uvt_as
  !
  subroutine add_uvt_freq(uvt,freq,error)
    !-------------------------------------------------------------------
    ! Fill the code_uvt_freq DAPS column
    !-------------------------------------------------------------------
    type(gildas), intent(inout) :: uvt      !
    real(kind=8), intent(in)    :: freq(:)  ! Unsorted absolute frequencies (i.e. in 'data' order)
    logical,      intent(inout) :: error    !
    !
    integer(kind=4) :: cfreq
    integer(kind=8) :: iv
    !
    cfreq = uvt%gil%column_pointer(code_uvt_freq)
    if (cfreq.le.0) then
      call map_message(seve%e,rname,'Internal error: no frequency DAPS column')
      error = .true.
      return
    endif
    !
    do iv=1,uvt%gil%nvisi
      uvt%r2d(cfreq,iv) = freq(iv)-uvt%gil%val(uvt%gil%faxi)
    enddo
  end subroutine add_uvt_freq
end program uv_gain
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
