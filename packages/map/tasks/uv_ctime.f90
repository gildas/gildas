program uv_ctime
  use gkernel_interfaces
  use gbl_format
  !
  ! Time - Average a UV Table
  ! usefull for ALMA data
  !
  ! Input:
  !   A UV Table
  !     a time averaging constant
  !     a maximum UV distance (just in case)
  ! Output
  !   A UV Table
  !
  character(len=filename_length) :: p_nami,p_namo
  real(4) :: p_mytime, p_myuv
  logical :: p_error
  integer(4) :: p_wcol
  !
  call gildas_open
  call gildas_char('UV_INPUT$',p_nami)
  call gildas_char('UV_OUTPUT$',p_namo)
  call gildas_real('TIME$',p_mytime,1)
  call gildas_real('UV$',p_myuv,1)
  call gildas_inte('WCOL$',p_wcol,1)
  call gildas_close
  !
  call sub_uv_ctime (p_nami,p_namo,p_mytime,p_myuv,p_wcol,p_error)
  if (p_error) call sysexi(fatale)
  call gagout('I-UV_CTIME,  Successful completion')
  !
contains
!<FF>
subroutine sub_uv_ctime (nami,namo,mytime,myuv,wcol,error)
  use image_def
  use gkernel_interfaces
  use mapping_interfaces, only : map_message
  use gbl_message
  !
  character(len=*), intent(inout) :: nami  ! Input file name
  character(len=*), intent(inout) :: namo  ! Output file name
  real(4), intent(in) :: mytime  ! Desired Integration Time
  real(4), intent(in) :: myuv    ! Maximum UV length change
  integer, intent(inout) :: wcol ! Weight channel
  logical, intent(out) :: error
  !
  character(len=*), parameter :: rname='UV_CTIME'
  character(len=256) :: mess
  type (gildas) :: hin, hout
  real(kind=4), allocatable, target :: din(:,:), dout(:,:)
  integer, allocatable :: the_times(:), ftime(:), ltime(:)
  real(8), allocatable :: times(:), ttime(:)
  integer :: nvis, iv, ntime, first_time, last_time
  real(8) :: old_time, t0
  integer :: mv, nv, jv, it, ier, mvisi, maxant
  integer :: codes(4)
  real :: rmax
  !
  ! Read Header of a sorted, UV table
  call gildas_null(hin, type= 'UVT')
  call sic_parsef (nami,hin%file,' ','.uvt')
  call gdf_read_header (hin,error)
  if (error) return
  !
  if (hin%gil%column_size(code_uvt_xoff).ne.0 .or. &
    & hin%gil%column_size(code_uvt_yoff).ne.0 .or. &
    & hin%gil%column_size(code_uvt_loff).ne.0 .or. &
    & hin%gil%column_size(code_uvt_moff).ne.0) then
    call map_message(seve%e,rname,'Mosaic UV tables not yet supported')
    error = .true.
    return
  endif
  !
  ! Read the Time Baseline information, and verify it is sorted
  nvis = hin%gil%nvisi
  allocate (din(4,nvis),stat=ier)
  codes(1:4) = [code_uvt_date, code_uvt_time, code_uvt_anti, code_uvt_antj]
  call gdf_read_uvonly_codes(hin, din, codes, 4, error)
  !
  allocate (times(nvis), the_times(nvis), ttime(nvis), ftime(nvis), &
     & ltime(nvis), stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Memory allocation error')
    error = .true.
    return
  endif
  !
  ! Get the chronological order:
  old_time = -1d10
  ntime = 0
  t0 = din(1,1)
  maxant = 0
  do iv = 1, nvis
    times(iv) = (din(1,iv)-t0)*86400.d0+din(2,iv)
    rmax = max(din(3,iv),din(4,iv))
    maxant = max(maxant,nint(rmax))
    if (times(iv).lt.old_time) then
      print *,'IV ',iv,' T0 ',t0,' Times ',times(iv),din(1,iv),din(2,iv)
      call map_message(seve%e,rname,'UV table is not Time-Baseline sorted')
      error = .true.
      return
    elseif (times(iv).gt.old_time) then
      if (ntime.gt.0) ltime(ntime) = iv-1
      old_time = times(iv)
      ntime = ntime+1
      ftime(ntime) = iv
      ttime(ntime) = times(iv)
    endif
  enddo
  ltime(ntime) = nvis
  deallocate(din)
  ! ftime is the starting number
  ! ltime is the ending number
  !
  ! So here is the maximum number of visibilities per time range
  mvisi = (maxant*(maxant-1))/2
  print *,'Maximum antenna ',maxant,' # baselines ',mvisi
!  do it=1,ntime
!    mvisi = max(ltime(it)-ftime(it)+1,mvisi)  ! Add one...
!  enddo
!  mvisi = mvisi+1
  allocate (dout(hin%gil%dim(1),mvisi), stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Memory allocation error')
    error = .true.
    return
  endif
  !
  ! Prepare output UV Table
  call gildas_null(hout, type='UVT')
  call gdf_copy_header (hin, hout, error)
  !! call gdf_setuv (hout, error)
  !
  ! Find out how many output visibilities there may be
  old_time = ttime(1)
  first_time = ftime(1)
  last_time = ltime(1)
  !
  jv = 0  ! Initialize properly
  do it=1,ntime-1
    if ((ttime(it)-old_time).gt.mytime) then
      jv = jv+mvisi  ! Count all time changes
    endif
  enddo
  jv = jv+mvisi ! Last time is a time change...
  !
  ! Allocate a suitable output UV Table
  hout%gil%dim(2) = jv
  hout%gil%nvisi = jv
  call sic_parsef (namo,hout%file,' ','.uvt')
  call gdf_create_image(hout,error)
  if (error) return
  !
  ! Now do the job
  old_time = ttime(1)
  first_time = ftime(1)
  last_time = ltime(1)
  jv = 1
  !
  if (wcol.eq.0) wcol = max(3,hin%gil%nchan)/3
  hin%blc = 0
  hin%trc = 0
  hout%trc = 0
  hout%blc = 0
  hout%blc(2) = 1
  mv = hin%gil%dim(1)
  nv = hin%gil%nvisi
  !
  do it=1,ntime
    !
    if ((ttime(it)-old_time).gt.mytime .or. it.eq.ntime) then
      ! write(mess,*) it,' Time ',old_time,' range ',first_time,last_time
      ! call map_message(seve%d,rname,mess)
      hin%blc(2) = first_time
      hin%trc(2) = last_time
      allocate (din(mv,last_time-first_time+1),stat=ier)
      if (ier.ne.0) then
        call map_message(seve%e,rname,'Memory allocation error')
        error = .true.
        return
      endif
      call gdf_read_data(hin,din,error)
      if (error) return
      jv = 1 ! Initialize...
      call time_compress(din,mv,1,last_time-first_time+1, &
          wcol, hout, dout, jv, mvisi, error)
      if (error) then
        write(mess,*) it,' Time ',old_time,' range ',first_time,last_time, ' / ',nv
        call map_message(seve%e,rname,mess)
        write(mess,*) 'Programming error or strange UV table ',jv,' > ',mvisi
        call map_message(seve%e,rname,mess)
        return
      endif
      if (jv.gt.0) then
        hout%trc(2) = hout%blc(2)+jv-1
        call gdf_write_data(hout,dout,error)
        if (error) return
        hout%blc(2) = hout%trc(2)+1
      else
        write(mess,*) 'No valid averaged visibility for ',old_time
        call map_message(seve%w,rname,mess)
      endif
      first_time = ftime(it)
      old_time = ttime(it)
      deallocate (din)
    endif
    last_time = ltime(it)
  enddo
  !
  ! Finalize the UV Table with the right numbers
  hout%gil%dim(2)= hout%trc(2)  ! Do  not know if this is allowed...
  hout%gil%nvisi = hout%trc(2)
  call gdf_update_header(hout, error)
  call gdf_close_image(hout, error)
  !
end subroutine sub_uv_ctime
!
subroutine time_compress(invisi,nd,first,last,wcol, out, ovisi, jv, mvisi, error)
  use gildas_def
  use image_def
  use gkernel_interfaces
  use mapping_interfaces, only : map_message
  use gbl_message
  !
  type(gildas), intent(in) :: out
  integer, intent(in) :: nd                     ! Visibility size
  real, intent(in) :: invisi(nd,*)              ! Input visibilities
  integer, intent(in) :: wcol                   ! Weight column
  real, intent(inout) :: ovisi(:,:)             ! Output visibilities
  integer, intent(in) :: first, last            ! First and last input visibilities
  integer, intent(inout) :: jv                  ! Current output visibility number
  integer, intent(in) :: mvisi
  logical, intent(out) :: error
  !
  character(len=*), parameter :: rname='TIME_COMPRESS'
  real, allocatable, target :: avisi(:,:), bvisi(:,:)
  real, pointer :: my_visi(:,:)
  integer, allocatable :: tb(:), indx(:)
  integer :: vmax, iant, jant
  integer :: iv, kv, nvisi, ier, icol, ic
  logical :: sorted
  real :: w, my_w
  !
  error = .false.
  !
  ! Extract the relevant
  nvisi = last-first+1
  allocate (avisi(nd,nvisi),tb(nvisi),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Memory allocation error')
    error = .true.
    return
  endif
  avisi(1:nd,1:nvisi) = invisi(:,first:last)
  !
  ! Sort it by BASELINE.
  ! We do not care about time here, since we are time averaging
  !
  ! Check the order
  do iv=1,nvisi
    ! Add the baseline number code as major driver, as steps are > nd
    if (avisi(7,iv).lt.avisi(6,iv)) then
      tb(iv) = (256*avisi(6,iv)+avisi(7,iv))
    else
      tb(iv) = (256*avisi(7,iv)+avisi(6,iv))
    endif
  enddo
  sorted = .true.
  vmax = tb(1)
  do iv = 1,nvisi
    if (tb(iv).lt.vmax) then
      sorted = .false.
      exit
    endif
    vmax = tb(iv)
  enddo
  !
  ! Sort it if needed
  if (.not.sorted) then
    allocate(indx(nvisi),bvisi(nd,nvisi),stat=ier)
    if (ier.ne.0) then
      call map_message(seve%e,rname,'Memory allocation error')
      error = .true.
      return
    endif
    do iv = 1,nvisi
      indx(iv) = iv
    enddo
    call gi4_trie(tb,indx,nvisi,error)
    if (error) then
      call gagout('F-UVSORT,  Sorting error')
      return
    else
      !
      ! Simple case
      do iv=1,nvisi
        kv = indx(iv)
        bvisi(:,iv) = avisi(:,kv)
      enddo
    endif
    my_visi => bvisi
  else
    my_visi => avisi
  endif
  !
  ! Now do the real work for each baseline
  icol = out%gil%nlead+out%gil%natom*wcol
  !
  ovisi(:,jv) = 0
  kv = jv ! Output visibility pointer
  w = 0   ! Current running weight
  iant = my_visi(6,1)
  jant = my_visi(7,1)
  do iv=1,nvisi
    if (iant.eq.my_visi(6,iv) .and. jant.eq.my_visi(7,iv)) then
      continue
    else if (iant.eq.my_visi(7,iv) .and. jant.eq.my_visi(6,iv)) then
      ! We take care of reversed visibilities, which could
      ! happen when V is close to zero, and the UV table has been already
      ! sorted in another way before...
      !
      ! This can be done by changing the Imaginary part of my_visi
      ! as well as u,v (and w in principle...)
      !! print *,iv,' visi ',my_visi(:,iv)
      my_visi(1,iv) = -my_visi(1,iv)
      my_visi(2,iv) = -my_visi(2,iv)
      do ic=out%gil%fcol+1,out%gil%lcol,3
        my_visi(ic,iv) = -my_visi(ic,iv)
      enddo
    else
 !!     print *,'KV ',kv, ' -- Visi ',iv,' changing from ',iant,jant, &
 !!     & ' to ',my_visi(6,iv),my_visi(7,iv)
      if (w.ne.0.0) then
        call mean_visiw(out,ovisi(:,kv),w)
        kv = kv+1
        if (kv.gt.mvisi) then
          print *,'MVISI ',mvisi,' First ',first
          print *,'KV ',kv, ' -- Visi ',iv+first-1,' changing from ',iant,jant, &
      & ' to ',my_visi(6,iv),my_visi(7,iv)
          error = .true.
          return
        endif
        ovisi(:,kv) = 0.
      endif
      iant = my_visi(6,iv)
      jant = my_visi(7,iv)
      w = 0.
    endif
    my_w = my_visi(icol,iv)
    call add_visiw(out,ovisi(:,kv),my_w,my_visi(:,iv),w)
  enddo
  !
  if (w.ne.0.0) then
    jv = kv
    call mean_visiw(out,ovisi(:,kv),w)
  else
    jv = kv-1
  endif
end subroutine time_compress
!
subroutine add_visiw(out,ovisi,aw,avisi,sw)
  use image_def
  type(gildas), intent(in) :: out
  real, intent(inout) :: ovisi(:)
  real, intent(in) :: aw
  real, intent(in) :: avisi(:)
  real, intent(inout) :: sw
  !
  integer i
  !
  if (aw.le.0) return
  !
  ovisi(1:3) = ovisi(1:3)+aw*avisi(1:3)
  ovisi(4:out%gil%nlead) = avisi(4:out%gil%nlead)
  do i = out%gil%nlead+1,out%gil%natom*out%gil%nchan+out%gil%nlead,out%gil%natom
    ovisi(i) = ovisi(i) + aw*avisi(i)
    ovisi(i+1) = ovisi(i+1) + aw*avisi(i+1)
    ovisi(i+2) = ovisi(i+2) + avisi(i+2)   ! Sum the weights
  enddo
  do i=out%gil%lcol+1,out%gil%dim(1)
    ovisi(i) = avisi(i)
  enddo
  sw = sw+aw
end subroutine add_visiw
!
subroutine mean_visiw(out,ovisi,sw)
  use image_def
  type(gildas), intent(in) :: out
  real, intent(inout) :: ovisi(:)
  real, intent(in) :: sw
  !
  integer i
  !
  if (sw.le.0) return
  !
  ovisi(1:3) = ovisi(1:3)/sw
  do i = out%gil%nlead+1,out%gil%natom*out%gil%nchan+out%gil%nlead,out%gil%natom
    ovisi(i) = ovisi(i)/sw
    ovisi(i+1) = ovisi(i+1)/sw
  enddo
end subroutine mean_visiw

end program uv_ctime

