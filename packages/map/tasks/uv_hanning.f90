program uv_hanning
  use gildas_def
  use gkernel_interfaces
  character(len=filename_length) :: cuvin, cuvou 
  logical error
  !
  ! Code:
  call gildas_open
  call gildas_char('UV_INPUT$',cuvin)
  call gildas_char('UV_OUTPUT$',cuvou)
  !
  call gildas_close
  !
  call sub_uv_hanning (cuvin,cuvou,error)
  if (error) call sysexi(fatale)
end program uv_hanning
!
subroutine sub_uv_hanning (cuvin, cuvou, error)
  use gkernel_interfaces
  use image_def
  use gbl_message
  !---------------------------------------------------------------------
  ! GILDAS
  !	Hanning Smooth a UV table 
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: cuvin 
  character(len=*), intent(in) :: cuvou 
  logical, intent(out) :: error
  !
  ! Local variables
  character(len=*), parameter :: rname='UV_HANNING'
  character(len=80)  :: mess
  type (gildas) :: uvin
  type (gildas) :: uvou
  integer :: ier, nblock, ib, nvisi
  !
  ! Simple checks 
  error = len_trim(cuvin).eq.0
  if (error) then
    call map_message(seve%e,rname,'No input UV table name')
    return
  endif
  error = len_trim(cuvou).eq.0
  if (error) then
    call map_message(seve%e,rname,'No output UV table name')
    return
  endif
  !
  call gildas_null (uvin, type = 'UVT')     ! Define a UVTable gildas header
  call gdf_read_gildas (uvin, cuvin, '.uvt', error, data=.false.)
  if (error) then
    call map_message(seve%e,rname,'Cannot read input UV table')
    return
  endif
  !
  call gildas_null (uvou, type = 'UVT')     ! Define a UVTable gildas header
  call gdf_copy_header(uvin,uvou,error)
  !
  ! Here modify the header of the output UV table according
  ! to the desired goal
  call sic_parse_file(cuvou,' ','.uvt',uvou%file)
  ! Define number of channels
  uvou%gil%nchan = uvin%gil%nchan-2       ! Ignore edges 
  uvou%gil%ref(1) = uvou%gil%ref(1)-1.d0  ! Shift reference channel by 1
  uvou%gil%dim(1) = uvou%gil%nlead+uvou%gil%nchan*uvou%gil%natom+uvou%gil%ntrail    
  call gdf_uv_shift_columns(uvin,uvou)
  call gdf_setuv(uvou,error)
  if (error) return
  !
  ! Define blocking factor
  call gdf_nitems('SPACE_GILDAS',nblock,uvin%gil%dim(1)) ! Visibilities at once
  nblock = min(nblock,uvin%gil%dim(2))
  ! Allocate respective space for each file
  allocate (uvin%r2d(uvin%gil%dim(1),nblock), uvou%r2d(uvou%gil%dim(1),nblock), stat=ier)
  if (ier.ne.0) then
    write(mess,*) 'Memory allocation error ',uvin%gil%dim(1), nblock
    call map_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  ! create the image
  call gdf_create_image(uvou,error)
  if (error) return
  !
  ! Loop over line table - The example assumes the same
  ! number of visibilities in Input and Output, which may not
  ! be true...
  uvou%blc = 0
  uvou%trc = 0
  do ib = 1,uvou%gil%dim(2),nblock
    write(mess,*) ib,' / ',uvou%gil%dim(2),nblock
    call map_message(seve%d,rname,mess) 
    uvou%blc(2) = ib
    uvou%trc(2) = min(uvou%gil%dim(2),ib-1+nblock) 
    uvin%blc(2) = ib
    uvin%trc(2) = uvou%trc(2)
    call gdf_read_data(uvin,uvin%r2d,error)
    !
    ! Here do the job
    !
    nvisi = uvou%trc(2)-uvou%blc(2)+1
    call hanning (uvou, uvin, nvisi) 
    !
    call gdf_write_data (uvou,uvou%r2d,error)
    if (error) return
  enddo
  !
  ! Finalize the image
  call gdf_close_image(uvin,error)
  call gdf_close_image(uvou,error)
  if (error) return
  !
  call map_message(seve%i,rname,'Successful completion')
end subroutine sub_uv_hanning
!
subroutine hanning (out, in, nvisi) 
  use image_def
  type (gildas), intent(inout) :: out
  type (gildas), intent(in) :: in
  integer, intent(in) :: nvisi
  ! Local
  integer :: i,j,k,kk
  real :: a,b,c,w(3),r
  data w/0.5,1.0,0.5/
  !
  do j=1,nvisi
    out%r2d (1:out%gil%nlead,j) =  in%r2d (1:out%gil%nlead,j)  
    do i=1,out%gil%nchan
      a = 0
      b = 0
      c = 0
      do k=1,3
        kk = in%gil%nlead+3*(i+k-1)
        if (in%r2d(kk,j).ne.0) then
          r = in%r2d(kk,j)*w(k)
          a = a+in%r2d(kk-2,j)*r
          b = b+in%r2d(kk-1,j)*r
          c = c+r
        endif
      enddo
      kk = out%gil%fcol+3*(i-1)
      if (c.ne.0) then
        out%r2d(kk,j) = a/c
        out%r2d(kk+1,j) = b/c
        out%r2d(kk+2,j) = c         ! time*band
      else
        out%r2d(kk,j) = 0
        out%r2d(kk+1,j) = 0
        out%r2d(kk+2,j) = 0
      endif
    enddo
    if (out%gil%ntrail.gt.0) then
      out%r2d(out%gil%dim(1)-out%gil%ntrail+1:out%gil%dim(1),j) &
       = in%r2d(out%gil%dim(1)-out%gil%ntrail+1:out%gil%dim(1),j)
    endif
  enddo
end subroutine hanning
