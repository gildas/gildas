module uvfit_data
  integer, parameter :: mpar=50
  integer, parameter :: mf=10
  integer, parameter :: mpin=7 ! Maximum number of parameters per function
  real(kind=8) :: pars(mpar)
  real(kind=8) :: sw
  real, allocatable :: uvriw(:,:)
  real :: save
  integer nf, ifunc(mf)              ! number of functions, functions
  integer npfunc(mf)                 ! number of parameters per function
  integer np                         ! number of points
  integer npar
  integer nstart(mpar)
end module uvfit_data

program uv_fit
  use gkernel_interfaces
  use gkernel_types
  use image_def
  use gbl_format
  use gbl_message
  use uvfit_data
  !---------------------------------------------------------------------
  !
  !---------------------------------------------------------------------
  ! Global
  include 'gbl_nan.inc'
  ! Local
  integer, allocatable :: iw(:)
  real(8), allocatable :: fvec(:), wa(:), r(:,:)
  real(8), allocatable :: wa1(:), wa2(:), wa3(:), wa4(:)
  type (gildas) :: huv
  type (gildas) :: htab
  type (gildas) :: hres
  type (gildas) :: hcov
  real, allocatable :: duv(:,:)
  real, allocatable :: dtab(:,:)
  real, allocatable :: dcova(:,:,:)
  !
  !
  real(8) :: xx, yy, xa, xd
  real(8), parameter :: pi = 3.14159265358979323846d0
  real(8), parameter :: clight = 299792458.d-6   ! in meter.mhz
  integer :: j, js, jsmin, lwa, ldr, ipar,jpar
  integer :: np2, nc
  integer :: n1, l, js1, if1, nvpar, npstart, if, kstart, ndata,  kc
  integer :: iii, k, i, ki, ic(2), nch, ncol
  integer :: iopt, nprint, info
  integer, parameter :: mstart=1000
  real :: par0(mpar), range(mpar), fact, vit
  real :: uv_range(2), uv_min, uv_max
  real(8) :: parin(mpar,mstart), par2(mpar,mstart), parstep(mpar)
  real(8) :: parout(mpar,mstart), fsumsq, cj(mpar)
  real(8) :: fsumin, epar(mpar), preerr(mpar), rms, tol, denorm, factor, fluxfactor
  character(len=8) :: fluxunit
  logical :: error
  integer, parameter :: mfunc=14
  integer :: mpfunc(mfunc), nstartf(mf,mfunc),nstarts
  real :: parf(mf,mfunc), rangef(mf,mfunc)
  character(len=9) :: cf, ccf, cfunc(mfunc), ccpar(mpin,mfunc), ch
  character(len=20) :: cpar(mpar),errmess
  character(len=80) :: name, resu, resid, chpar
  character(len=15) :: chpos1
  character(len=15) :: chpos2
  character(len=200) :: chain
  logical :: savef(mf)
  external :: fitfcn
  integer :: ier, nvs, nvt
  type(projection_t) :: proj
  !
  data cfunc/'POINT','E_GAUSS','C_GAUSS','C_DISK','RING','EXPO',&
       &    'POWER-2','POWER-3','E_DISK','U_RING','E_RING','SPERGEL',&
       &    'E_SPERGEL', 'E_EXPO'/
  data ccpar/'R.A.','DEC.','FLUX','    ','    ','    ',' ',&               ! point
       &    'R.A.','Dec.','Flux','Major','Minor','Pos.Ang.',' ',&          ! e_gauss
       &    'R.A.','Dec.','Flux','F.W.H.P.','    ','    ',' ',&            ! c_gauss
       &    'R.A.','Dec.','Flux','Diam.','    ','    ',' ',&               ! c_disk
       &    'R.A.','Dec.','Flux','I.Diam.','O.Diam.','    ',' ',&          ! ring
       &    'R.A.','Dec.','Flux','F.W.H.P.','    ','    ',' ',&            ! expo
       &    'R.A.','Dec.','Flux','F.W.H.P.','    ','    ',' ',&            ! power-2
       &    'R.A.','Dec.','Flux','F.W.H.P.','    ','    ',' ',&            ! power-3
       &    'R.A.','Dec.','Flux','Major','Minor','Pos.Ang.',' ',&          ! e_disk
       &    'R.A.','Dec.','Flux','Radius','    ','    ',' ',&              ! u_ring
       &    'R.A.','Dec.','Flux','Outer','Inner','Pos.Ang.','Ratio',&      ! e_ring
       &    'R.A.','Dec.','Flux','H.L.R','nu','','',&                      ! spergel
       &    'R.A.','Dec.','Flux','Maj.H.L.R','Min/Maj','Pos.Ang.','nu',&   ! e_spergel
       &    'R.A.','Dec.','Flux','Major','Minor','Pos.Ang.',' '/           ! e_expo
  data mpfunc /3,6,4,4,5,4,4,4,6,4,7,5,7,6/
  !
  character(len=*), parameter :: rname='UV_FIT'
  !-----------------------------------------------------------------------
  call xsetf(0)
  !     Code:
  !
  call gildas_open
  call gildas_char('UVTABLE$',name)
  call gildas_inte('CHANNEL$',ic,2)
  !
  !     2 Additional parameters to use specific uv range.
  call gildas_real('UV_RANGE$',uv_range,2)
  uv_min = uv_range(1)
  uv_max = uv_range(2)
  if (uv_min.ne.0 .or. uv_max.ne.0) then
    write(chain,*) 'UV data from ',uv_min,   &
     &      ' to ',uv_max,' m.'
    call gag_message(seve%i,rname,chain)
  endif
  call gildas_inte('NF$',nf,1)
  k = 0
  !
  ! Scan all function parameters
  do i=1, nf
    write(ch,'(a,i2.2,a)') 'FUNCT',i,'$'
    call gildas_char(ch,cf)
    call sic_upper(cf)
    call sic_ambigs('FIT',cf,ccf,ifunc(i),cfunc,mfunc,error)
    if (error) then
      write(6,*) 'E-FIT,  Invalid function '//cf
      call sysexi(fatale)
    endif
    write(ch,'(a,i2.2,a)') 'PARAM',i,'$'
    call gildas_real(ch,parf(:,i),mpin)
    write(ch,'(a,i2.2,a)') 'RANGE',i,'$'
    call gildas_real(ch,rangef(:,i),mpin)
    !
    !     start=-1 means fixed parameter.
    write(ch,'(a,i2.2,a)') 'START',i,'$'
    call gildas_inte(ch,nstartf(:,i),mpin)
    npfunc(i) = mpfunc(ifunc(i))
    do j=1, npfunc(i)
      k = k + 1
      par0(k) = parf(j,i)
      range(k) = rangef(j,i)
      nstart(k) = nstartf(j,i)
      cpar(k) = ccf//' '//ccpar(j,ifunc(i))
    enddo
    !
    !     Additional option used to subtract model functions.
    write(ch,'(a,i2.2,a)') 'SUBSF',i,'$'
    call gildas_logi(ch,savef(i),1)
  enddo
  npar = k
  call gildas_char('RESULT$',resu)
  call gildas_char('RESIDUALS$',resid)
  call gildas_close
  !
  !     Create array of starting values
  !
  npstart = 1
  nstarts = 1
  do i=1, npar   
    if (nstart(i).gt.1) then
      nstarts = nstarts*nstart(i)
      n1 = nstart(i)-1
      parstep(i) = range(i)/n1
      parin(i,1) = par0(i)-0.5*parstep(i)*n1
    else
      parin(i,1) = par0(i)
    endif
  enddo
  !
  if (nstarts.gt.mstart) then
     write (chain,'(a,i7,a,i4,a)') "Number of independents starts (",nstarts,&
          ") is too large (max=", mstart, ")"
     call gag_message(seve%f,rname, chain)
     call sysexi(fatale)
  endif
  !
  do i=1, npar
    if (nstart(i).gt.1) then
      kstart = npstart
      do j=1, nstart(i)-1
        do k=kstart+1,kstart+npstart
          do l=1, npar
            parin(l,k)= parin(l,k-npstart)
          enddo
          parin(i,k) = parin(i,k-npstart)+parstep(i)
        enddo
        kstart = kstart+npstart
      enddo
      npstart = kstart
    endif
  enddo
  !
  !     Load input UV Table (HUV -- DUV)
  !
  call gildas_null(huv, type = 'UVT')
  call gdf_read_gildas(huv, name, '.uvt', error, data = .false.)
  if (error) then
    call gag_message(seve%f,rname,'Cannot read input table')
    call sysexi(fatale)
  endif
  allocate (duv(huv%gil%dim(1),huv%gil%dim(2)),stat=ier)
  call gdf_read_data(huv,duv,error)
  if (error) then
     print *,'Error reading data '
     call sysexi(fatale)
  endif
  !
  ndata = huv%gil%dim(2)
  ier = gdf_range(ic, huv%gil%nchan)
  nc = huv%gil%nchan
  !
  np2 = 2*ndata                ! max. data points
  lwa = npar*(np2+5)+np2
  !
  ! For DNLS1E and DCOV
  ier = 0
  allocate (fvec(np2),wa(lwa),r(np2,npar),iw(npar),stat=ier)
  if (ier.ne.0) call sysexi(fatale)
  !
  ! For DCOV only
  allocate(wa1(npar),wa2(npar),wa3(npar),wa4(np2),stat=ier)
  if (ier.ne.0) call sysexi(fatale)
  !
  !     Create table of fitted results HTAB - DTAB
  call gildas_null (htab, type = 'TABLE')
  call gdf_copy_header(huv,htab,error)
  call sic_parsef(resu,htab%file,' ','.uvfit')
  call gag_message(seve%i,rname,'Creating fit table '//   &
     &    trim(htab%file) )
  htab%char%code(2) = 'uv-fit'
  htab%char%code(1) = 'freq'
  htab%gil%dim(1) = nc
  htab%gil%dim(2) = 4 + nf*(3+2*mpin)
  htab%gil%val(2) = 0.
  allocate (dtab(htab%gil%dim(1),htab%gil%dim(2)),stat=ier)
  if (ier.ne.0) then
    call gag_message(seve%e,rname,'Cannot create output table')
    call sysexi(fatale)
  endif
  !
  ! Create cube of covariance matrices HCOV - DCOVA
  nvpar = 0  ! Number of variable parameters?
  do i=1,npar
    if (nstart(i).ge.0) nvpar = nvpar+1
  enddo
  if (nvpar.gt.0) then
    call gildas_null(hcov,type='IMAGE')
    call gdf_copy_header(huv,hcov,error)
    call sic_parse_file(resu,' ','.cov',hcov%file)
    call gag_message(seve%i,rname,'Creating covariance cube '//hcov%file)
    hcov%gil%ndim = 3
    hcov%char%code(1) = 'parameter'
    hcov%char%code(2) = 'parameter'
    hcov%char%code(3) = 'frequency'
    hcov%char%unit = 'COVAR UNIT'
    hcov%gil%dim(1) = nvpar
    hcov%gil%dim(2) = nvpar
    hcov%gil%dim(3) = nc
    hcov%gil%convert(:,1) = 1.d0
    hcov%gil%convert(:,2) = 1.d0
    hcov%gil%convert(1,3) = huv%gil%convert(1,1)-ic(1)+1
    hcov%gil%convert(2,3) = huv%gil%convert(2,1)
    hcov%gil%convert(3,3) = huv%gil%convert(3,1)
    hcov%gil%blan_words = 0
    hcov%gil%extr_words = 0
    hcov%gil%reso_words = 0
    allocate (dcova(hcov%gil%dim(1),  &
                    hcov%gil%dim(2),  &
                    hcov%gil%dim(3)),stat=ier)
    if (ier.ne.0) then
      call gag_message(seve%e,rname,'Cannot create covariance cube')
      call sysexi(fatale)
    endif
    do kc=1,nc
      dcova(:,:,kc) = s_nan  ! For blank channels, failed convergence, etc.
    enddo
  else
    call gag_message(seve%w,rname,'No free parameter, no covariance cube created')
  endif
  !
  !     Loop on channels
  nvs = huv%gil%dim(1)
  !
  allocate (uvriw(5,ndata),stat=ier)
  !
  do kc = ic(1),ic(2)
    write(6,*)
    !     Load (U,V) in arcsec**-1, compute observing frequency for channel KC
    fact = gdf_uv_frequency(huv,dble(kc)) / clight * pi/180.d0/3600d0
    call load_data(ndata,nvs,kc,fact,duv,np,uvriw,uv_min,uv_max)
    if (np.eq.0) then
      write(chain,'(A,I0)') 'No data for channel', kc
      call gag_message(seve%w,rname,chain)
      cycle
    else
      write(chain,'(I0,A,I0)') np,' data points for channel', kc
      call gag_message(seve%i,rname,chain)
    endif
    !
    np2 = 2*np
    vit = huv%gil%voff+(kc-huv%gil%ref(1))*huv%gil%vres
    write(chain,'(A,I0,A,1PG10.3)') 'Starting minimization on channel ', kc,   &
     &      '   Velocity= ',vit
    call gag_message(seve%i,rname,chain)
    !
    fsumin = 1d20
    !
    do js=1, npstart
      !     Get only variable parameters:
      nvpar = 0
      do i=1, npar
        if (nstart(i).ge.0) then
          nvpar = nvpar+1
          par2(nvpar,js)=parin(i,js)
        endif
        pars(i) = parin(i,js)
      enddo
      if (nvpar.eq.0) then
        call gag_message(seve%i,rname,'No free parameter, using input values')
        jsmin = 1
        goto 998
      endif
      write(6,'(a,5(1pg12.5))') 'I-UV_FIT, Starting from ',   &
     &        (par2(iii,js),iii=1, nvpar)
      !
      !     supply full jacobian
      iopt = 2
      !     or only Function (test mode)
      !     iopt = 1
      tol = 1d-8
      nprint = 0
      call dnls1e (fitfcn, iopt, np2, nvpar, par2(1,js),   &
     &        fvec, tol, nprint, info,   &
     &        iw, wa, lwa)
      if (info.eq.0) then
        print *,'F-DNLS1E, improper input parameters, info= ',   &
     &          info
      elseif (info.eq.6 .or. info.eq.7) then
        print *,'F-DNLS1E, TOL too small, info= ',info
      elseif (info.eq.5) then
        print *,'F-DNLS1E, not converging, info= ',info
      elseif (info.eq.4) then
        print *,'W-DNLS1E, FVEC orthog. to Jacob. col., info= ',   &
     &          info
      endif
      fsumsq  = denorm(np2, fvec)
      !
      if (fsumsq.lt.fsumin) then
        fsumin = fsumsq
        jsmin = js
      endif
      js1 = js
    enddo
    if (jsmin.ne.js1) then
      nprint = 0
      call dnls1e (fitfcn, iopt, np2, nvpar, par2(1,jsmin),   &
     &        fvec, tol, nprint, info,   &
     &        iw, wa, lwa)
      if (info.eq.0) then
        print *,'F-DNLS1E, improper input parameters, info= ',   &
     &          info
      elseif (info.eq.6 .or. info.eq.7) then
        print *,'F-DNLS1E, TOL too small, info= ',info
      elseif (info.eq.5) then
        print *,'F-DNLS1E, not converging, info= ',info
      elseif (info.eq.4) then
        print *,'W-DNLS1E, FVEC orthog. to Jacob. col., info= ',   &
     &          info
      endif
      fsumsq  = denorm(np2, fvec)
    endif
    ldr = np2
    call dcov (fitfcn, iopt, np2, nvpar, par2(1,jsmin),   &
     &      fvec, r, ldr, info,   &
     &      wa1, wa2, wa3, wa4)
    if (info.eq.0) then
      print *,'F-DCOV, improper input parameters, info= ',info
    elseif (info.eq.1) then
      do jpar=1,nvpar
        do ipar=1,nvpar
          dcova(ipar,jpar,kc-ic(1)+1) = r(ipar,jpar)
        enddo
      enddo
    elseif (info.eq.2) then
      print *,'W-DCOV, Jacobian singular, info= ',info
    endif
    !
    !     extract the diagonal of r
    call diagonal(np2,nvpar,r,cj)
    rms = fsumsq*dsqrt(np2*1d0)
    write(6,'(a,f10.4,a)') ' r.m.s.= ',rms,' Jy.'
    if1 = info
    !
    998 continue
    k = 0
    preerr(:) = sqrt(abs(cj(:)))
    do i=1, npar
      if (nstart(i).ge.0) then
        k = k+1
        parout(i,jsmin)=par2(k,jsmin)
        epar(i) = preerr(k)
      else
        parout(i,jsmin) = pars(i)
        epar(i) = 0.0000 
      endif
    enddo
    !
    k = 1   
    do j=1, nf
       call outpar(ifunc(j),npfunc(j),parout(k,jsmin),epar(k))
       k = k + npfunc(j)
    enddo
    k = 0
    ki = 0
    call gwcs_projec(huv%gil%a0,huv%gil%d0,-huv%gil%pang,huv%gil%ptyp,proj,error)
    do j=1, nf
      ! Ra/Dec absolute coordinates (1st and 2nd parameters)
      xx = parout(ki+1,jsmin)*pi/180d0/3600d0
      yy = parout(ki+2,jsmin)*pi/180d0/3600d0
      call rel_to_abs(proj,xx,yy,xa,xd,1)
      call rad2sexa(xa,24,chpos1)
      call rad2sexa(xd,360,chpos2)
      ! Flux in best units (3rd parameter)
      if (abs(parout(ki+3,jsmin)).ge.1.d0) then
        fluxfactor = 1.d0
        fluxunit = 'Jy'
      elseif (abs(parout(ki+3,jsmin)).ge.1.d-3) then
        fluxfactor = 1.d3
        fluxunit = 'milliJy'  ! or 'mJy' ?
      else
        fluxfactor = 1.d6
        fluxunit = 'microJy'  ! or 'uJy' ?
      endif
      do i=1, npfunc(j)
        ki = ki+1
        if (i.eq.3) then
          factor = fluxfactor
        else
          factor = 1.d0
        endif
        if (nstart(ki).ge.0) then
          k = k+1
          write(errmess,'(f0.5)')  epar(ki)*factor ! Error can be larger than "999.99999"
          write(chpar,'(2a,f11.5,a,a,a)')  &
               cpar(ki), ' = ',parout(ki,jsmin)*factor,' (',adjustr(errmess(1:9)),')'
        else
          epar(ki) = 0.
          write(chpar,'(2a,f11.5,a)')  &
            cpar(ki), ' = ', parout(ki,jsmin)*factor,' (  fixed  )'
        endif
        nch = len_trim(chpar)
        if (i.eq.1) then
          write(6,*) chpar(1:nch)//'  '//chpos1
        elseif (i.eq.2) then
          write(6,*) chpar(1:nch)//' '//chpos2
        elseif (i.eq.3) then
          write(6,*) chpar(1:nch)//'  '//fluxunit
        else
          write(6,*) chpar(1:nch)
        endif
      enddo
    enddo
    ncol = htab%gil%dim(2)
    call outfit(nc,kc,ncol,dtab,rms,vit,npar,parout(1,jsmin),epar)
  enddo
  deallocate(fvec,wa,r,iw,wa1,wa2,wa3,wa4)
  !
  !     Create residuals table and initialize it with the data.
  !
  call gildas_null(hres, type = 'UVT')
  call gdf_copy_header(huv,hres,error)
  call sic_parsef(resid,hres%file,' ','.uvt')
  !
  ! Subtract using DUV as work space...
  !
  !     Loop on functions for subtracting  model.
  nvt = htab%gil%dim(2)
  do if = 1, nf
    if (savef(if)) then
      write (6,'(a,i2.2,2i8)') 'Removing FUNCT',if,ndata,nc
      call model_data(huv,ndata,nvs,nc,ic(1),ic(2), &
     &        nvt,duv,dtab,if)
    endif
  enddo
  call gdf_write_image(hres,duv,error)
  call gdf_write_image(htab,dtab,error)
  if (nvpar.gt.0)  call gdf_write_image(hcov,dcova,error)
  call gag_message(seve%i,rname,'Successful completion')
  !
end program uv_fit
!
subroutine diagonal(m,n,r,c)
  integer :: m                      !
  integer :: n                      !
  real(kind=8) :: r(m,n)                  !
  real(kind=8) :: c(n)                    !
  ! Local
  integer :: i
  do i=1, n
    c(i) = r(i,i)
  enddo
end subroutine diagonal
!
subroutine outfit(nc,ic,ncol,y,rms,vit,nbpar,par,epar)
  use gildas_def
  use uvfit_data
  !---------------------------------------------------------------------
  ! Store the fitted parameters into the output table Y
  !
  ! Format of table is, for each channel:
  ! 1-4  RMS of fit, number of functions, number of parameters, velocity
  ! then for each function, 3+mpin*2 columns:
  !        Function number, type of function, number of parameters
  !        then 6 times (parameter, error)
  !---------------------------------------------------------------------
  integer, intent(in) :: nc         ! Number of channels
  integer, intent(in) :: ic         ! Current channel
  integer, intent(in) :: ncol       ! Size of Y
  real, intent(out) :: y(nc,ncol)   ! Output table
  real(8), intent(in) :: rms        ! RMS of fit
  real, intent(in) :: vit           ! Velocity
  integer, intent(in) :: nbpar      ! Number of parameters
  real(8), intent(in) :: par(nbpar) ! Parameters
  real(8), intent(in) :: epar(nbpar)! Errors
  ! Local
  integer :: if, ip, k, kcol
  !
  y(ic,1) = rms
  y(ic,2) = nf
  y(ic,3) = nbpar
  y(ic,4) = vit
  kcol = 5
  k = 1
  do if=1, nf
    kcol = 5+(if-1)*(3+2*mpin)
    y(ic,kcol) = if
    y(ic,kcol+1) = ifunc(if)
    y(ic,kcol+2) = npfunc(if)
    kcol = kcol+3
    do ip=1, npfunc(if)
      y(ic,kcol) = par(k)
      y(ic,kcol+1) = epar(k)
      k = k+1
      kcol = kcol+2
    enddo
  enddo
  return
end subroutine outfit
!
subroutine load_data(ndata,nx,ic,fact,visi,np,uvriw,uv_min,uv_max)
  !---------------------------------------------------------------------
  ! Load Data of channel IC from the input UV table into array UVRIW.
  !
  !---------------------------------------------------------------------
  integer, intent(in) :: ndata        ! Number of visibilities
  integer, intent(in) :: nx           ! Size of a visibility
  integer, intent(in) :: ic           ! Channel number
  real, intent(in) :: fact            ! Conversion factor of baselines
  real, intent(in) :: visi(nx,ndata)  ! Visibilities
  integer, intent(out) :: np          ! Number of non zero visibilities
  real, intent(out) :: uvriw(5,ndata) ! Output array
  real, intent(in) :: uv_min          ! Min UV
  real, intent(in) :: uv_max          ! Max UV
  !
  ! Local:
  integer :: i, irc, iic, iwc
  real :: uv1, uv2, uv
  !
  !! pi = 3.14159265358979323
  irc = 5 + 3*ic
  iic = irc + 1
  iwc = iic + 1
  np = 0
  if (uv_min.eq.0.0 .and. uv_max.eq.0.0) then
    do i = 1, ndata
      if (visi(iwc,i).gt.0) then
        np = np + 1
        uvriw(1,np) = visi(1,i) * fact
        uvriw(2,np) = visi(2,i) * fact
        uvriw(3,np) = visi(irc,i)
        uvriw(4,np) = visi(iic,i)
        uvriw(5,np) = visi(iwc,i) * 1e6
      endif
    enddo
  else
    uv1 = uv_min**2
    uv2 = uv_max**2
    do i = 1, ndata
      if (visi(iwc,i).gt.0) then
        uv = visi(1,i)**2+visi(2,i)**2
        if (uv.ge.uv1 .and. uv.lt.uv2) then
          np = np + 1
          uvriw(1,np) = visi(1,i) * fact
          uvriw(2,np) = visi(2,i) * fact
          uvriw(3,np) = visi(irc,i)
          uvriw(4,np) = visi(iic,i)
          uvriw(5,np) = visi(iwc,i) * 1e6
        endif
      endif
    enddo
  endif
  return
end subroutine load_data
!
subroutine model_data(huv,nd,nx,nc,ic1,ic2,ncol,vin,vy,if)
  use image_def
  use uvfit_data
  !---------------------------------------------------------------------
  ! Subtract the model uv data for component function number IF
  ! from the input visibility data.
  !
  !     ND         Integer   number of visibilities                Input
  !     NX         INteger   second dim. of VIN                    Input
  !     NC         INteger   Number of channels                    Input
  !     IC1        Integer   First channel to process              Input
  !     IC2        Integer   Last channel to process               Input
  !     NCOL       Integer   Second dim. of VY                     Input
  !     VIN        Real      Visibilities                   Input/output
  !     VY         Real      Model parameters                      Input
  !     IF         Integer   Function number                       Input
  !---------------------------------------------------------------------
  type (gildas), intent(in) :: huv  ! Inut UV Table header
  integer, intent(in) :: nd         ! Number of visibilities
  integer, intent(in) :: nx         ! Size of a visbility
  integer, intent(in) :: nc         ! Number of channels
  integer, intent(in) :: ic1        ! First channel
  integer, intent(in) :: ic2        ! Last channel
  integer, intent(in) :: ncol       ! Size of VY
  real, intent(inout) :: vin(nx,nd) ! Visibilities
  real, intent(in) :: vy(nc,ncol)   ! Model parameters
  integer, intent(in) :: if         ! Function number
  !
  ! Local
  real :: fact
  integer :: j, i, k, iif, npf, kcol, ic
  real(8) :: y(2), dy(2,7), paris(7), uu, vv
  real(8), parameter :: pi = 3.14159265358979323846d0
  real(8), parameter :: clight = 299792458.d-6   ! in meter.mhz
  !
  ! Code:
  do j=1, nd
    !
    ! Kcol is the first column number in result table VY for function IF
    kcol = 5+(if-1)*(3+2*mpin)
    do ic = ic1, ic2
      fact = huv%gil%val(1) * &
     &        (1.d0+huv%gil%fres/huv%gil%freq*(ic-huv%gil%ref(1)))/clight   &
     &        *pi/180.d0/3600.d0
      uu = vin(1,j)*fact
      vv = vin(2,j)*fact
      !
      ! IIF is the function type and NPF its number of parameters.
      iif = nint(vy(ic,kcol+1))
      npf = nint(vy(ic,kcol+2))
      do i=1, npf
        paris(i) = vy(ic,kcol+3+2*(i-1))
      enddo
      call model(iif,npf,uu,vv,paris,y,dy)
      k = 8 + 3*(ic-1)
      vin(k,j) = vin(k,j) - y(1)
      vin(k+1,j) = vin(k+1,j) - y(2)
    enddo
  enddo
end subroutine model_data
!
subroutine fitfcn(iflag,m,nvpar,x,f,fjac,ljc)
  use gildas_def
  use uvfit_data
  !$ use omp_lib
  !---------------------------------------------------------------------
  ! FCN is called by DNLS1E
  !
  !---------------------------------------------------------------------
  integer, intent(in) :: iflag          ! Code for print out
  integer, intent(in) :: m              ! Number of data points
  integer, intent(in) :: nvpar          ! Number of variables
  real(8), intent(in) :: x(nvpar)       ! Variable parameters
  real(8), intent(out) :: f(m)          ! Function value at each point
  integer, intent(in) :: ljc            ! First dimension of FJAC
  real(8), intent(out) :: fjac(ljc,nvpar) ! Partial derivatives
  !
  real(8), external :: denorm
  !
  ! Local
  integer :: k, i, l, j, kpar, iif, kvpar, kf, kl
  integer :: ithread, nthread
  real(8) :: uu, vv, rr, ii, ww, fact, y(2), dy(2,mpar), fa, fb
  real(8), allocatable :: swi(:)
  real(8), allocatable :: fone(:), ftwo(:)
  !
  k = 1
  sw = 0
  !
  ! Put the variable parameters in PARS (already including the fixed ones)
  kvpar = 1
  do j=1, npar
    if (nstart(j).ge.0) then
      pars(j) = x(kvpar)
      kvpar = kvpar+1
    endif
  enddo
  !
  nthread = 1
  !$ nthread = omp_get_max_threads()
  allocate(swi(nthread),fone(nvpar),ftwo(nvpar))
  swi = 0.0
  !
  !$OMP PARALLEL DEFAULT(none)  &
  !$OMP & SHARED(uvriw) &  ! Input Visibility array
  !$OMP & SHARED(f,fjac) & ! Ouput arrays
  !$OMP & SHARED(np,nf,nvpar,npfunc,iflag,pars,ifunc,nstart, swi) &
  !$OMP & PRIVATE(i,uu,vv,rr,ii,ww,kf,kl,fact,kpar,kvpar,j,l)  &
  !$OMP & PRIVATE(ithread, iif, y, dy, fone, ftwo, fa, fb)
  !
  ithread = 1
  !$ ithread = omp_get_thread_num()+1
  !$OMP DO
  do i=1, np
    kf = 2*i-1
    kl = 2*i
    !
    ! Get real and imaginary part of visibility
    call getvisi(np,uvriw,i,uu,vv,rr,ii,ww)
    !
    ! Initialize F and derivatives.
    ! the weights are 1/sigma**2 (within a factor 2)
    fact = ww
    !
    ! Compute F and FJAC from model
    kpar  = 1
    kvpar = 1
    if (iflag.eq.1) then
      fa = -rr
      fb = -ii
      do j = 1, nf
        iif = ifunc(j)
        call model(iif,npfunc(j),uu,vv,pars(kpar),y,dy)
        fa = fa + y(1)
        fb = fb + y(2)
        kpar = kpar + npfunc(j)
      enddo
      f(kf) = fa*fact
      f(kl) = fb*fact
      !
    else if (iflag.eq.2) then
      fone = 0.d0
      ftwo = 0.d0
      do j = 1, nf
        iif = ifunc(j)
        call model(iif,npfunc(j),uu,vv,pars(kpar),y,dy)
        do l = 1, npfunc(j)
          if (nstart(kpar).ge.0) then
            fone(kvpar) = fone(kvpar) + dy(1,l)
            ftwo(kvpar) = ftwo(kvpar) + dy(2,l)
            kvpar = kvpar+1
          endif
          kpar = kpar + 1
        enddo
      enddo
      do l=1, nvpar
        fjac(kf,l) = fone(l)*fact
        fjac(kl,l) = ftwo(l)*fact
      enddo
      !
    else if (iflag.eq.0) then
      continue
    endif
    !
    ! Weight appropriately:
    swi(ithread) = swi(ithread) + fact
  enddo
  !$OMP END DO
  !$OMP END PARALLEL
  !
  sw = 0.0
  do i=1,nthread
    sw = sw+swi(i)
  enddo
  !
  ! In the end: normalize by the sum of weights.
  k = 1
  if (iflag.eq.1) then
    do k = 1, 2*np
      f(k) = f(k)/sw
    enddo
  else if (iflag.eq.2) then
    do l=1, nvpar
      do k=1,2*np
        fjac(k,l) = fjac(k,l)/sw
      enddo
    enddo
  endif
  if (iflag.eq.0) then
    print 1000, (x(i), i=1, nvpar), denorm(2*np,f)
    1000     format (10(1pg19.12))
  endif
  !!  print *,'Done FITFCN'
end subroutine fitfcn
!
subroutine getvisi(n,uvriw,k,u,v,r,i,w)
  !---------------------------------------------------------------------
  !     Obtain U, V, and the visibility from the array UVRIW.
  !
  !     N      Integer     Number of visibilities     Input
  !     UVRIW  real(8)      Input array                Input
  !     K      Integer     Visibility number          Input
  !     U      real(8)      U coord.                   Output
  !     V      real(8)      V coord.                   Output
  !     R      real(8)      Real part                  Output
  !     I      real(8)      Imaginary part.            Output
  !     W      real(8)      Weight                     Output
  !---------------------------------------------------------------------
  integer :: n                      !
  real :: uvriw(5,n)                !
  integer :: k                      !
  real(8) ::  u                      !
  real(8) ::  v                      !
  real(8) ::  r                      !
  real(8) ::  i                      !
  real(8) ::  w                      !
  !
  u = uvriw(1,k)
  v = uvriw(2,k)
  r = uvriw(3,k)
  i = uvriw(4,k)
  w = uvriw(5,k)
  return
end subroutine getvisi
!
subroutine outpar(id,kfunc,pars,errs)
  !---------------------------------------------------------------------
  ! Process fitted parameters to be saved, according to fitted functions
  ! id        Integer     Function type            Input
  ! KFUNC    Integer     Number of parameters     Input
  ! PAR      real(8)      Parameters               Input / Output
  ! Errs      real(8)      Parameters               Input / Output
  !---------------------------------------------------------------------
  integer, intent(in) :: id     ! Function code
  integer, intent(in) :: kfunc ! Number of parameters
  real(kind=8), intent(inout) :: pars(kfunc)  ! Parameters
  real(kind=8), intent(inout) :: errs(kfunc)  ! Parameters
  ! Local
  real(kind=8) :: dummy
  !
  ! Reorder the axes, and change the P. Angle accordingly
  ! Make sure the axes are positive.
  select case (id)
  case (2,9,14)
     ! E_GAUSS, E_DISK, E_EXPO
     pars(4) = abs(pars(4))
     pars(5) = abs(pars(5))
     if (pars(4).lt.pars(5)) then
        pars(6) = pars(6)+90d0
        dummy = pars(4)
        pars(4) = pars(5)
        pars(5) = dummy
        dummy = errs(4)
        errs(4) = errs(5)
        errs(5) = dummy
     endif
     pars(6) = -90d0+mod(pars(6)+90d0,180d0)
  case (11)
     ! E_RING
     if (pars(5).lt.pars(4)) then
        dummy = pars(4)
        pars(4) = pars(5)
        pars(5) = dummy
        dummy = errs(4)
        errs(4) = errs(5)
        errs(5) = dummy
     endif
     if (pars(7).gt.1.0) then
        pars(4) = pars(4)*pars(7)
        pars(5) = pars(5)*pars(7)
        pars(7) = 1d0/pars(7)
     endif
  case (13)
     ! E_SPERGEL
     print *,'maj,q,pa: ',pars(4:6)
     pars(4) = abs(pars(4))
     pars(5) = abs(pars(5))
     if (pars(5).gt.1.0) then
        pars(6) = pars(6)+90d0
        pars(4) = pars(4)*pars(5)
        errs(4) = errs(4)*pars(5)
        errs(5) = errs(5)/pars(5)**2
        pars(5) = 1/pars(5)
     endif
     pars(6) = -90d0+mod(pars(6)+90d0,180d0)
  case (3,4,6,7,8,10,12)
     ! C_GAUSS, C_DISK, EXPO, POWER-2, POWER-3, U_RING, SPERGEL
     pars(4) = abs(pars(4))     
  case (5)
     ! RING  
     pars(4) = abs(pars(4))
     pars(5) = abs(pars(5))
     if (pars(5).lt.pars(4)) then
        dummy = pars(4)
        pars(4) = pars(5)
        pars(5) = dummy
        dummy = errs(4)
        errs(4) = errs(5)
        errs(5) = dummy
     endif
  end select
end subroutine outpar
!
subroutine  model(ifunc,kfunc,uu,vv,x,y,dy)
  !---------------------------------------------------------------------
  ! Compute a model function.
  !---------------------------------------------------------------------
  integer, intent(in)    :: ifunc       ! Fuction type
  integer, intent(in)    :: kfunc       ! Number of parameters
  real(8), intent(in)    :: uu          ! U coordinate in arcsec^-1
  real(8), intent(in)    :: vv          ! V coordinate in arcsec^-1
  real(8), intent(in)    :: x(kfunc)    ! Parameters
  real(8), intent(inout) :: y(2)        ! Function (real, imag.)
  real(8), intent(inout) :: dy(2,kfunc) ! Partial derivatives.
  !
  ! Global
  real(8) :: z_exp
  !
  ! Local
  integer :: i
  real(kind=8), parameter :: pi  = 3.14159265358979323846d0
  real(kind=8), parameter :: twopi = 2d0*pi
  real(kind=8), parameter :: deg = pi/180d0
  real(kind=8), parameter :: cst  = 3.5597073312469d0  ! pi**2/4d0/log(2d0)
  real(kind=8), parameter :: cte  = 3*pi**2/log(2d0)**2
  real(kind=8), parameter :: acnu = -0.40371257831497309
  real(kind=8), parameter :: bcnu = -0.22810144250066436
  real(kind=8), parameter :: ccnu = +2.4009610601053772
  real(kind=8) :: st,ct,q1,q2,arg,carg,sarg,darg1,darg2
  real(kind=8) :: a,da(7),b,ksi,q,j0,j1,aa,dfac4,dfac5
  real(kind=8) :: dadksi
  real(kind=8) :: dbesj0,dbesj1,dbesk0,dbesk1
  real(kind=8) :: a1,a2,fac,k0,k1,d,daa
  real(kind=8) :: ii,oo,fi,fo,ff,dgo_do,dgi_do,dgi_di,dgo_di,dout2
  real(kind=8) :: flux,dg_dmaj,dg_dmin,dg_dpa,major,minor
  real(kind=8) :: dg_douter,dg_dinner,dg_dratio,dg_drota
  ! Spergel
  real(kind=8) :: cnu,nu,mu,uvdist2,uurot,vvrot,major2,lambda2,aspect,sinpa,cospa,g,factor
  !-----------------------------------------------------------------------
  !
  ! The fitted function is of the form:
  !     F = flux * f(x-x0,y-y0)
  ! corresponding to a visibility model:
  !     G = flux * exp(-2i*pi(u*x0+v*y0)) * g(u,v)
  ! where g is the Fourier transform of f.
  !
  ! The first three parameters 1, 2, 3, are x0, y0, and flux.
  !
  ! First compute A = g(u,v), and DA(k) = dA/dx_k for k=4, ...
  ! depending on model.
  !
  da(:) = 0
  !
  select case (ifunc)
  !
  case (1)  ! Point source
    ! 1- Point source
    ! model = flux * delta(x-x0,y-y0)
    ! g(u,v) = 1
    ! 3 pars (x0, y0, flux)
    a = 1d0
    !
  case (2) ! Gaussian
    ! 2- Gaussian (2-dim)
    !     model = flux*4*ln2/(pi*b1*b2) * exp (-4*ln2*((r1/b1)**2+(r2/b2)**2)
    !     g(u,v) = exp(-pi**2/4/ln2*(b1*u1+b2*u2)**2)
    !       where u1 = u*sin(theta)+v*cos(theta)
    !             u2 = u*cos(theta)-v*sin(theta)
    !     6 pars (x0, y0, flux, theta, b1, b2)
    !
    st = sin(deg*x(6))
    ct = cos(deg*x(6))
    q1 = uu*st+vv*ct
    q2 = uu*ct-vv*st
    a1 = -cst*q1**2
    a2 = -cst*q2**2
    a = z_exp(a1*x(4)**2+a2*x(5)**2)
    da(4) = 2*x(4)*a1*a
    da(5) = 2*x(5)*a2*a
    da(6) = -2d0*cst*q1*q2*(x(4)**2-x(5)**2)*deg*a
    !
  case (3)
    !
    ! 3- Gaussian (1-dim)
    !     model = flux*4*ln2/(pi*b**2) * exp (-4*ln2*(r/b)**2)
    !     g(u,v) = exp(- pi**2/4/ln(2)*(b*q)**2)
    !     where q**2 = u**2+v**2
    !     4 pars (x0, y0, flux, b)
    !
    q2 = uu**2+vv**2
    a1 = -cst*q2
    a = z_exp(a1*x(4)**2)
    da(4) = a1*x(4)*2d0*a
    !
  case (4) ! Disk
    ! 4- Disk
    !     model = flux*4/(pi*b**2)
    !     g(u,v) = J1(pi*b*q)/q
    !     where q**2 = u**2+v**2
    !     4 pars (x0, y0, flux, b)
    !
    q = sqrt(uu**2+vv**2)
    ksi = pi*x(4)*q
    if (ksi.eq.0d0) then
      a = 1
      da(4) = 0
    else
      j1 = dbesj1(ksi)
      j0 = dbesj0(ksi)
      a = j1*2d0/ksi
      da(4) = 2d0*(j0-a)/x(4)
    endif
    !
  case (5) ! Ring
    !
    ! 5- Ring
    q = sqrt(uu**2+vv**2)
    a = 0
    da(4) = 0
    da(5) = 0
    if (x(4).ne.x(5)) then
      d = x(4)**2-x(5)**2
      fac = x(4)**2/d
      dfac4 =-2*x(5)**2*x(4)/d**2
      dfac5 =2*x(4)**2*x(5)/d**2
      ksi = pi*x(4)*q
      if (ksi.eq.0) then
        a = a + fac
        da(4) = da(4) + dfac4
        da(5) = da(5) + dfac5
      else
        j1 = dbesj1(ksi)
        j0 = dbesj0(ksi)
        aa = j1*2d0/ksi
        daa = 2d0*(j0-aa)/x(4)
        a = a + aa*fac
        da(4) = da(4) + daa*fac+aa*dfac4
        da(5) = da(5) + aa*dfac5
      endif
      ksi = pi*x(5)*q
      if (ksi.eq.0) then
        a = a+1d0-fac
        da(4) = da(4)-dfac4
        da(5) = da(5)-dfac5
      else
        j1 = dbesj1(ksi)
        j0 = dbesj0(ksi)
        aa = j1*2d0/ksi
        daa = 2d0*(j0-aa)/x(5)
        a = a + aa*(1d0-fac)
        da(4) = da(4) - aa*dfac4
        da(5) = da(5) + daa*(1d0-fac)-aa*dfac5
      endif
    endif
    !
  case (6) ! Exponential
    ! 6- exponential
    !     model = exp(- 2*ln2*r/b)
    !     g(u,v) = 1 / ( 1 + (q*b*pi/ln2)**2 )**(3/2)
    !
    q2 = uu**2+vv**2
    ksi = q2 * (x(4)*pi/log(2d0))**2
    b = 1d0+ksi
    a = 1d0/b**1.5
    da(4) = -3*a*ksi/b/x(4)
    !
  case (7) ! Power -2
    ! 7- Power -2
    !     model =
    !     g(u,v) =
    !
    q = sqrt(uu**2+vv**2)
    ksi = pi*x(4)*q
    k0 = dbesk0(ksi)
    k1 = dbesk1(ksi)
    a = k0
    da(4) = -ksi/x(4)*k1
    !
  case (8) ! Power -3
    !
    ! 8- Power -3
    !     model =
    !     g(u,v) = exp( -pi*q*b/sqrt(2**(1/3)-1) )
    !
    q = sqrt(uu**2+vv**2)
    ksi = pi*q*x(4)/sqrt(2.**(1./3)-1)
    a = z_exp(-ksi)
    da(4) = -a*ksi/x(4)
    !
  case (9) ! Elliptical disk
    ! 9- Elliptical Disk
    !     model = flux*4/(pi*(b1**2+b2**2)) (inside ellipse, 0 outside)
    !     g(u,v) = 2 J1 (ksi)  / ksi
    !     where u1 = u*sin(theta)+v*cos(theta)
    !           u2 = u*cos(theta)-v*sin(theta)
    !     ksi = pi*sqrt((b1*u1)**2+(b2*u2)**2)
    !     6 pars (x0, y0, flux, b1, b2, theta)
    !
    st = sin(deg*x(6))
    ct = cos(deg*x(6))
    q1 = uu*st+vv*ct
    q2 = uu*ct-vv*st
    ksi = pi*sqrt((x(4)*q1)**2+(x(5)*q2)**2)
    if (ksi.eq.0d0) then
      a = 1
      da(4) = 0
      da(5) = 0
      da(6) = 0
    else
      j1 = dbesj1(ksi)
      a = j1*2d0/ksi
      j0 = dbesj0(ksi)
      dadksi = 2d0*(j0-a)/ksi
      da(4) = dadksi*pi**2*q1**2*x(4)/ksi
      da(5) = dadksi*pi**2*q2**2*x(5)/ksi
      da(6) = dadksi/ksi*pi**2*q1*q2*(x(4)**2-x(5)**2)*deg
    endif
    !
  case (10)  ! Unresolved ring
    !
    ! 10- Unresolved Ring
    !     model = flux/(2*pi*b)*delta(r-b)
    !     g(u,v) = J0(pi*b*q)
    !     where q**2 = u**2+v**2
    !     4 pars (x0, y0, flux, b)
    !
    q = sqrt(uu**2+vv**2)
    ksi = pi*x(4)*q
    if (ksi.eq.0d0) then
      a = 1
      da(4) = 0
    else
      j1 = dbesj1(ksi)
      j0 = dbesj0(ksi)
      a = j0
      da(4) = -j1 * pi*q
    endif
    !
  case (11) ! Elliptical Ring
    ! 11- Elliptical Ring
    !
    !     To be developped more...
    !
    !     7 pars (x0, y0, flux, outer, inner, pa, ratio)
    !
    !     Define a1 = outer
    !     Define a2 = outer*ratio
    !     Define b1 = inner
    !     Define b2 = inner*ratio
    !
    ! Sum of two elliptical disks.
    !     a) a = fa *4/(pi*outer^2*ratio) > 0
    !        ga(u,v) = fa * J1(pi*sqrt((a1*u1)**2+(a2*u2)**2)
    !     b) b = fb *4/(pi*inner^2*ratio) < 0
    !        gb(u,v) = fb * J1(pi*sqrt((b1*u1)**2+(b2*u2)**2)
    !     and
    !        fa + fb = flux
    !        fa / fb = -(outer/inner)^2
    !     where u1 = u*sin(theta)+v*cos(theta)
    !           u2 = u*cos(theta)-v*sin(theta)
    !*     so
    !        fa = flux * outer^2 / (outer^2-inner^2)
    !        fb = - flux * inner^2 / (outer^2-inner^2)
    !
    ! Derivative vs outer
    !
    !     d(fa)/d(out) * J1 + fa * d(J1)/d(out)
    !
    !
    st = sin(deg*x(6))
    ct = cos(deg*x(6))
    q1 = uu*st+vv*ct
    q2 = uu*ct-vv*st
    ii = x(5) ! Inner radius
    oo = x(4) ! Outer radius
    ff = x(7) ! cos (inclination)
    dout2 = (oo**2-ii**2)
    !
    ! First source
    a1 = x(4)
    a2 = x(4)*ff
    !
    ksi = pi*sqrt((a1*q1)**2+(a2*q2)**2)
    if (ksi.eq.0d0) then
      a = 1
      dg_dmaj = 0
      dg_dmin = 0
      dg_dpa = 0
    else
      j1 = dbesj1(ksi)
      j0 = dbesj0(ksi)
      a = j1*2d0/ksi
      dadksi = 2d0*(j0-a)/ksi
      dg_dmaj = dadksi*pi**2*q1**2*a1/ksi    ! d(g)/d(major)
      dg_dmin = dadksi*pi**2*q2**2*a2/ksi    ! d(g)/d(minor)
      dg_dpa = dadksi/ksi*pi**2*q1*q2*(a1**2-a2**2)*deg
    endif
    !
    ! Now it gets a little more complex
    fo = oo**2 / dout2
    !
    dgo_do = 2 * oo * ii**2 / dout2 *  a    ! Strictly speaking
    dgo_do = dgo_do + fo * (dg_dmaj + ff * dg_dmin)
    dgo_di = 2 * oo**2 * ii / dout2 *  a
    !
    flux = a * fo
    dg_dratio = fo * a1 * dg_dmin
    dg_drota = fo * dg_dpa
    !
    ! Second source
    a1 = x(5)
    a2 = x(5)*ff
    !
    ksi = pi*sqrt((a1*q1)**2+(a2*q2)**2)
    if (ksi.eq.0d0) then
      a = 1
      dg_dmaj = 0
      dg_dmin = 0
      dg_dpa = 0
    else
      j1 = dbesj1(ksi)
      j0 = dbesj0(ksi)
      a = j1*2d0/ksi
      dadksi = 2d0*(j0-a)/ksi
      dg_dmaj = dadksi*pi**2*q1**2*a1/ksi    ! d(g)/d(major)
      dg_dmin = dadksi*pi**2*q2**2*a2/ksi    ! d(g)/d(minor)
      dg_dpa = dadksi/ksi*pi**2*q1*q2*(a1**2-a2**2)*deg
    endif
    !
    fi = ii**2 / dout2
    !
    dgi_di = 2 * ii * oo**2 / dout2 *  a    ! Strictly speaking
    dgi_di = dgo_di + fi * (dg_dmaj + ff * dg_dmin)
    dgi_do = 2 * ii**2 * oo / dout2 *  a
    !
    ! Add (or rather subtract...) to previous derivatives
    dg_dinner = dgo_di - dgi_di
    dg_douter  = dgo_do - dgi_do
    dg_dratio = dg_dratio - fi * a1 * dg_dmin
    dg_drota = dg_drota - fi * dg_dpa
    !
    ! and to flux (actually, relative visibility)
    flux = flux - a * fi
    !
    a = flux                     ! Normalized scaling factor now
    da(4) = dg_douter
    da(5) = dg_dinner
    da(6) = dg_drota
    da(7) = dg_dratio
    !
  case (12)
     !
     ! Spergel 2010 profile
     ! model = flux*(r/2)^2*(Knu(r)/(Gamma(nu+1))
     ! g(u,v) = 1/(2*pi)*(1+(u^2+v^2)*(r0/cnu)^2))^(-1-nu)
     !          1   2    3    4   5
     ! 5 pars (x0, y0, flux, r0, nu)
     ! 
     ! cnu = acnu+bcnu*nu+ccnu*log(nu+2)
     ! Approximantion to cnu based on fit to tabulated cnu provided by Spergel 2010.
     ! 10% accuracy for nu < -0.6, for nu > -0.6 accuracy < 1%
     !
     nu = x(5)
     cnu = acnu+bcnu*nu+ccnu*log(nu+2)
     major = x(4)*twopi
     uvdist2 = uu**2+vv**2
     lambda2 = major**2*uvdist2
     g = 1d0 + (lambda2/cnu**2)
     !
     a = g**(-1-nu)                                                          ! a
     da(4) = twopi*(2d0/cnu**2)*(-nu-1)*g**(-nu-2)*major*uvdist2             ! da_drmaj
     da(5) = a*( (nu+1)*(2*lambda2/(g*cnu**3))*(bcnu+ccnu/(nu+2)) - log(g) ) ! da_dnu
     !
  case (13)
     !
     ! Elliptical adaptation to Spergel 2010 profile (for galaxies)
     ! model = flux*(r/2)^2*(Knu(r)/(Gamma(nu+1))
     ! g(u,v) = 1/(2*pi)*(1+rmaj^2(uurot^2+vvrot^2*asp^2)/cnu^2))^(-1-nu)
     !         1  2  3    4    5   6  7
     ! 7 pars (x0,y0,flux,rmaj,asp,pa,nu)
     ! 
     ! cnu = acnu+bcnu*nu+ccnu*log(nu+2)
     ! Approximantion to cnu based on fit to tabulated cnu provided by Spergel 2010.
     ! 10% accuracy for nu < -0.6, for nu > -0.6 accuracy < 1%
     !
     nu = x(7)
     mu = -(nu+1)
     cnu = acnu+bcnu*nu+ccnu*log(nu+2)
     major  = x(4)*twopi
     aspect = x(5)
     cospa = cos(deg*x(6))
     sinpa = sin(deg*x(6))
     uurot = uu*sinpa+vv*cospa
     vvrot = uu*cospa-vv*sinpa
     uvdist2 = (uurot**2 + aspect**2*vvrot**2)
     major2 = major**2
     lambda2 = major2*uvdist2
     g = 1d0 + (lambda2/cnu**2)
     factor = mu*g**(mu-1)*(2d0/cnu**2)
     !
     a = g**mu                                                               ! a
     da(4) = twopi*factor       *major *uvdist2                              ! da_drmaj
     da(5) = twopi*factor*aspect*major2*vvrot**2                             ! da_daspect
     da(6) =   deg*factor*(1-aspect**2)*major2*uurot*vvrot                   ! da_dpa
     da(7) = a*( (nu+1)*(2*lambda2/(g*cnu**3))*(bcnu+ccnu/(nu+2)) - log(g) ) ! da_dnu
     !
  case (14)
     !
     ! Elliptical Exponential
     ! model = exp(- 2*ln2*(r1/b1+r2/b2))
     ! g(u,v) = 1 / ( 1 + (uvd1^2*r1^2+uvd2^2*r2^2)*(pi/ln2)**2 )**(3/2)
     ! 6 pars (x0,y0,flux,size1,size2,theta)
     !
     sinpa = sin(deg*x(6))
     cospa = cos(deg*x(6))
     uurot = uu*sinpa+vv*cospa
     vvrot = uu*cospa-vv*sinpa
     lambda2 = x(4)**2*uurot**2+x(5)**2*vvrot**2
     !
     ksi = lambda2*(pi/log(2d0))**2
     !
     a = 1d0/(1d0+ksi)**1.5
     !
     da(4) = -cte*x(4)*uurot**2/(1d0+ksi)**2.5
     da(5) = -cte*x(5)*vvrot**2/(1d0+ksi)**2.5
     da(6) = -cte*(x(4)**2-x(5)**2)*uurot*vvrot/(1d0+ksi)**2.5
     !
  case default
     print *,'Internal error => Unknown function: ',ifunc
  end select
  !
  ! Compute the phase term exp(-2i*pi(u*x0+v*y0)) and its derivatives
  ! with respect to x_0 and y_0
  !
  ! Sign is correct for X positive towards East.
  darg1 = twopi*uu
  darg2 = twopi*vv
  arg = darg1*x(1)+darg2*x(2)
  carg = cos(arg)
  sarg = sin(arg)
  !
  ! Now compute the real and imaginary part of function.
  y(1) = x(3)*a*carg
  y(2) = x(3)*a*sarg
  !
  ! And its derivatives with respect to x0, y0, flux
  dy(1,1) = -y(2)*darg1
  dy(2,1) =  y(1)*darg1
  dy(1,2) = -y(2)*darg2
  dy(2,2) =  y(1)*darg2
  dy(1,3) = a*carg
  dy(2,3) = a*sarg
  ! And its derivatives with respect to parameters 4, 5, 6, 7
  if (kfunc.ge.4) then
    do i=4, kfunc
      dy(1,i) = x(3)*carg*da(i)
      dy(2,i) = x(3)*sarg*da(i)
    enddo
  endif
end subroutine model
!
function z_exp(x)
  !---------------------------------------------------------------------
  ! protect again underflows in exp(x)
  !---------------------------------------------------------------------
  real(8) :: z_exp                   !
  real(8) :: x                       !
  ! Local
  real(8) :: d1mach, xmin, ymin
  logical :: first
  data first/.true./
  save xmin, ymin, first
  !
  if (first) then
    ymin = 2*d1mach(1)
    xmin = log(ymin)
    first = .false.
  endif
  if (x.lt.xmin) then
    z_exp = ymin
    return
  endif
  z_exp = exp(x)
end function z_exp
