module uvfit_data_proper
  integer, parameter :: mpar=50
  integer, parameter :: mf=10
  real(kind=8) :: pars(mpar)
  real(kind=8) :: sw
  real, allocatable :: uvriw(:,:)
  real :: save
  integer nf, ifunc(mf)              ! number of functions, functions
  integer npfunc(mf)                 ! number of parameters per function
  integer np                         ! number of points
  integer npar
  integer nstart(mpar)
end module uvfit_data_proper

program uv_fit_proper
  use gkernel_interfaces
  use gkernel_types
  use image_def
  use gbl_format
  use uvfit_data_proper
  !---------------------------------------------------------------------
  !     Re-written with Fortran-90 interfaces
  !     on 2011, June 30 S.Guilloteau
  !---------------------------------------------------------------------
  !
  ! Local
  integer, allocatable :: iw(:)
  real(8), allocatable :: fvec(:), wa(:), r(:,:)
  real(8), allocatable :: wa1(:), wa2(:), wa3(:), wa4(:)
  type (gildas) :: huv
  type (gildas) :: htab
  type (gildas) :: hres
  real, pointer :: duv(:,:)
  real, allocatable :: dtab(:,:)
  !
  !
  real(8) :: xx, yy, xa, xd
  real(8), parameter :: pi = 3.14159265358979323846d0
  real(8), parameter :: clight = 299792458.d-6   ! in meter.mhz
  integer :: j, mstart, js, jsmin, lwa, ldr
  integer :: mfunc, np2, mpin, nc
  integer :: n1, l, js1, if1, nvpar, npstart, if, kstart, ndata,  kc
  integer :: iii, k, i, ki, ic(2), nch
  integer :: iopt, nprint, info
  parameter (mstart=1000)
  real :: par0(mpar), range(mpar), fact, vit, uv_range(2), uv_min
  real :: uv_max
  real(8) :: parin(mpar,mstart), par2(mpar,mstart), parstep(mpar)
  real(8) :: parout(mpar,mstart), fsumsq, cj(mpar)
  real(8) :: fsumin, epar(mpar), vpar(mpar), rms, tol, denorm
  real(8) :: dzero, factor, fluxfactor
  character(len=8) :: fluxunit
  logical :: error
  parameter (mfunc=11, mpin=9)
  integer :: mpfunc(mfunc), nstartf(mf,mfunc)
  real :: parf(mf,mfunc), rangef(mf,mfunc)
  character(len=8) :: cf, ccf, cfunc(mfunc), ccpar(mpin,mfunc), ch
  character(len=20) :: cpar(mpar)
  character(len=80) :: name, resu, resid, chpar
  character(len=13) :: chpos1
  character(len=14) :: chpos2
  logical :: savef(mf)
  external :: fitfcn
  integer :: ier, nvs, nvt
  type(projection_t) :: proj
  !
  data cfunc/'POINT','E_GAUSS','C_GAUSS','C_DISK','RING','EXPO',   &
     &    'POWER-2','POWER-3','E_DISK','U_RING','E_RING'/
  data ccpar/'R.A.','Dec.','Mu_a','Mu_b','FLux','    ','    ','    ',' ',   &
     &    'R.A.','Dec.','Mu_a','Mu_b','Flux','Major','Minor','Pos.Ang.',' ',   &
     &    'R.A.','Dec.','Mu_a','Mu_b','Flux','F.W.H.P.','    ','    ',' ',   &
     &    'R.A.','Dec.','Mu_a','Mu_b','Flux','Diam.','    ','    ',' ',   &
     &    'R.A.','Dec.','Mu_a','Mu_b','Flux','I.Diam.','O.Diam.','    ',' ',   &
     &    'R.A.','Dec.','Mu_a','Mu_b','Flux','F.W.H.P.','    ','    ',' ',   &
     &    'R.A.','Dec.','Mu_a','Mu_b','Flux','F.W.H.P.','    ','    ',' ',   &
     &    'R.A.','Dec.','Mu_a','Mu_b','Flux','F.W.H.P.','    ','    ',' ',   &
     &    'R.A.','Dec.','Mu_a','Mu_b','Flux','Major','Minor','Pos.Ang.',' ',   &
     &    'R.A.','Dec.','Mu_a','Mu_b','Flux','Radius','    ','    ',' ',   &
     &    'R.A.','Dec.','Mu_a','Mu_b','Flux','Outer','Inner','Pos.Ang.','Incli'/
  data mpfunc /5,8,6,6,7,5,5,5,8,5,9/
  !-----------------------------------------------------------------------
  call xsetf(0)
  !     Code:
  !
  call gildas_open
  call gildas_char('UVTABLE$',name)
  call gildas_inte('CHANNEL$',ic,2)
  !
  !     2 Additional parameters to use specific uv range.
  call gildas_real('UV_RANGE$',uv_range,2)
  uv_min = uv_range(1)
  uv_max = uv_range(2)
  call gildas_char('RESULT$',resu)
  call gildas_char('RESIDUALS$',resid)
  !
  print *,'UV_FIT Fortran-90 version '
  if (uv_min.ne.0 .and. uv_max.ne.0) then
    write(6,*) 'I-UV_FIT,  UV data from ',uv_min,   &
     &      ' to ',uv_max,' m.'
  endif
  call gildas_inte('NF$',nf,1)
  k = 0
  do i=1, nf
    write(ch,'(a,i2.2,a)') 'FUNCT',i,'$'
    call gildas_char(ch,cf)
    call sic_upper(cf)
    call sic_ambigs('FIT',cf,ccf,ifunc(i),cfunc,mfunc,error)
    if (error) then
      write(6,*) 'E-FIT,  Invalid function '//cf
      goto 999
    endif
    write(ch,'(a,i2.2,a)') 'PARAM',i,'$'
    call gildas_real(ch,parf(:,i),mpin)
    write(ch,'(a,i2.2,a)') 'RANGE',i,'$'
    call gildas_real(ch,rangef(:,i),mpin)
    !
    !     start=-1 means fixed parameter.
    write(ch,'(a,i2.2,a)') 'START',i,'$'
    call gildas_inte(ch,nstartf(:,i),mpin)
    npfunc(i) = mpfunc(ifunc(i))
    do j=1, npfunc(i)
      k = k + 1
      par0(k) = parf(j,i)
      range(k) = rangef(j,i)
      nstart(k) = nstartf(j,i)
      cpar(k) = ccf//' '//ccpar(j,ifunc(i))
    enddo
    !
    !     Additional option used to subtract model functions.
    write(ch,'(a,i2.2,a)') 'SUBSF',i,'$'
    call gildas_logi(ch,savef(i),1)
  enddo
  npar = k
  call gildas_close
  !
  !     Create array of starting values
  npstart = 1
  do i=1, npar
    if (nstart(i).gt.1) then
      n1 = nstart(i)-1
      parstep(i) = range(i)/n1
      parin(i,1) = par0(i)-0.5*parstep(i)*n1
    else
      parin(i,1) = par0(i)
    endif
  enddo
  do i=1, npar
    if (nstart(i).gt.1) then
      kstart = npstart
      do j=1, nstart(i)-1
        do k=kstart+1,kstart+npstart
          do l=1, npar
            parin(l,k)= parin(l,k-npstart)
          enddo
          parin(i,k) = parin(i,k-npstart)+parstep(i)
        enddo
        kstart = kstart+npstart
      enddo
      npstart = kstart
    endif
  enddo
  !
  !     Load input UV Table (HUV -- DUV) 
  !
  call gildas_null(huv, type = 'UVT')
  call sic_parsef(name,huv%file,' ','.uvt')
  call gdf_read_gildas (huv, name, '.uvt', error)
  if (error) then
    write(6,*) 'F-UV_FIT, Cannot read input table'
    goto 999
  endif
  duv => huv%r2d
  !
  ! Use the Equinox of the coordinates
  dzero = dble(huv%gil%epoc)-2000.0d0
  print *,'Dzero ',dzero
  !
  ndata = huv%gil%dim(2)
  k = huv%gil%nchan 
  if (ic(1).eq.0 .and. ic(2).eq.0) then
    ic(1) = 1
    ic(2) = k
  endif
  ic(1) = max(1,ic(1))
  ic(2) = min(ic(2),k)
  nc = k
  !
  np2 = 2*ndata                ! max. data points
  lwa = npar*(np2+5)+np2
  !
  ! For DNLS1E and DCOV
  ier = 0
  allocate (fvec(np2),wa(lwa),r(np2,npar),iw(npar),stat=ier)
  if (ier.ne.0) goto 999
  !
  ! For DCOV only
  allocate(wa1(npar),wa2(npar),wa3(npar),wa4(np2),stat=ier)
  if (ier.ne.0) goto 999
  !
  !     Create table of fitted results HTAB - DTAB
  call gildas_null (htab, type = 'IMAGE')
  call gdf_copy_header(huv,htab,error)
  call sic_parsef(resu,htab%file,' ','.uvfit')
  call gagout('I-UV_FIT,  Creating fit table '//   &
     &    trim(htab%file) )
  htab%char%code(2) = 'uv-fit'
  htab%char%code(1) = 'freq'
  htab%gil%dim(1) = nc
  htab%gil%dim(2) = 4 + nf*21
  htab%gil%val(2) = 0.
  htab%gil%ndim = 2
  allocate (dtab(htab%gil%dim(1),htab%gil%dim(2)),stat=ier)
  if (ier.ne.0) then
    call gagout('F-UV_FIT, Cannot create output table') 
    goto 999
  endif
  !
  !     Loop on channels
  allocate (uvriw(6,ndata),stat=ier)
  !
  nvs = huv%gil%dim(1)
  nvt = htab%gil%dim(2)
  !
  do kc = ic(1),ic(2)
    write(6,*)
    !     Load (U,V) in arcsec**-1, compute observing frequency for channel KC
    fact = huv%gil%val(1) * &
     & (1.d0+huv%gil%fres/huv%gil%freq*(kc-huv%gil%ref(1)))/clight   &
     &      *pi/180./3600.
    call load_data(dzero,ndata,nvs,kc,fact,duv,np,   &
     &     uvriw,uv_min,uv_max)
    if (np.eq.0) then
      write(6,*) 'W-UV_FIT, No data for channel', kc
      goto 30
    else
      write(6,*) 'I-UV_FIT, ',np,' data points for channel', kc
    endif
    !
    np2 = 2*np
    vit = huv%gil%voff+(kc-huv%gil%ref(1))*huv%gil%vres
    write(6,*) 'I-UV_FIT, Starting minimization on channel', kc,   &
     &      '   Velocity= ',vit
    fsumin = 1d20
    do js=1, npstart
      !     Get only variable parameters:
      nvpar = 0
      do i=1, npar
        if (nstart(i).ge.0) then
          nvpar = nvpar+1
          par2(nvpar,js)=parin(i,js)
        endif
        pars(i) = parin(i,js)
      enddo
      if (nvpar.eq.0) then
        write(6,*)   &
     &          'F-UV_FIT, At least one parameter should be free'
        goto 998
      endif
      write(6,'(1x,a,5(1pg12.5))') 'I-UV_FIT, Starting from ',   &
     &        (par2(iii,js),iii=1, nvpar)
      !
      !     supply full jacobian
      iopt = 2
      !     or only Function (test mode)
      !     iopt = 1
      tol = 1d-8
      nprint = 0
      call dnls1e (fitfcn, iopt, np2, nvpar, par2(1,js),   &
     &        fvec, tol, nprint, info,   &
     &        iw, wa, lwa)
      if (info.eq.0) then
        print *,'F-DNLS1E, improper input parameters, info= ',   &
     &          info
      elseif (info.eq.6 .or. info.eq.7) then
        print *,'F-DNLS1E, TOL too small, info= ',info
      elseif (info.eq.5) then
        print *,'F-DNLS1E, not converging, info= ',info
      elseif (info.eq.4) then
        print *,'W-DNLS1E, FVEC orthog. to Jacob. col., info= ',   &
     &          info
      endif
      fsumsq  = denorm(np2, fvec)
      !
      if (fsumsq.lt.fsumin) then
        fsumin = fsumsq
        jsmin = js
      endif
      js1 = js
    enddo
    if (jsmin.ne.js1) then
      nprint = 0
      call dnls1e (fitfcn, iopt, np2, nvpar, par2(1,jsmin),   &
     &        fvec, tol, nprint, info,   &
     &        iw, wa, lwa)
      if (info.eq.0) then
        print *,'F-DNLS1E, improper input parameters, info= ',   &
     &          info
      elseif (info.eq.6 .or. info.eq.7) then
        print *,'F-DNLS1E, TOL too small, info= ',info
      elseif (info.eq.5) then
        print *,'F-DNLS1E, not converging, info= ',info
      elseif (info.eq.4) then
        print *,'W-DNLS1E, FVEC orthog. to Jacob. col., info= ',   &
     &          info
      endif
      fsumsq  = denorm(np2, fvec)
    endif
    ldr = np2
    call dcov (fitfcn, iopt, np2, nvpar, par2(1,jsmin),   &
     &      fvec, r, ldr, info,   &
     &      wa1, wa2, wa3, wa4)  
    if (info.eq.0) then
      print *,'F-DCOV, improper input parameters, info= ',info
    elseif (info.eq.2) then
      print *,'W-DCOV, Jacobian singular, info= ',info
    endif
    !
    !     extract the diagonal of r
    call diagonal(np2,nvpar,r,cj)
    rms = fsumsq*dsqrt(np2*1d0)
    write(6,'(a,f10.4,a)') ' r.m.s.= ',rms,' Jy.'
    if1 = info
    !
    998 continue
    k = 0
    do i=1, npar
      if (nstart(i).ge.0) then
        k = k+1
        parout(i,jsmin)=par2(k,jsmin)
      else
        parout(i,jsmin) = pars(i)
      endif
    enddo
    !
    k = 1
    do j=1, nf
      call outpar(ifunc(j),npfunc(j),parout(k,jsmin))
      k = k + npfunc(j)
    enddo
    k = 0
    ki = 0
    call gwcs_projec(huv%gil%a0,huv%gil%d0,-huv%gil%pang,huv%gil%ptyp,proj,error)
    do j=1, nf
      ! Ra/Dec absolute coordinates (1st and 2nd parameters)
      xx = parout(ki+1,jsmin)*pi/180d0/3600d0
      yy = parout(ki+2,jsmin)*pi/180d0/3600d0
      call rel_to_abs(proj,xx,yy,xa,xd,1)
      call rad2sexa(xa,24,chpos1,4)
      call rad2sexa(xd,360,chpos2,4)
      ! Flux in best units (5th parameter)
      if (abs(parout(ki+5,jsmin)).ge.1.d0) then
        fluxfactor = 1.d0
        fluxunit = 'Jy'
      elseif (abs(parout(ki+5,jsmin)).ge.1.d-3) then
        fluxfactor = 1.d3
        fluxunit = 'milliJy'  ! or 'mJy' ?
      else
        fluxfactor = 1.d6
        fluxunit = 'microJy'  ! or 'uJy' ?
      endif
      do i=1, npfunc(j)
        ki = ki+1
        if (i.eq.5) then
          factor = fluxfactor
        else
          factor = 1.d0
        endif
        if (nstart(ki).ge.0) then
          k = k+1
          epar(ki) = sqrt(abs(cj(k)))
          if (i.eq.3) then
             epar(ki) = 1e3*epar(ki)
             vpar(ki) = parout(ki,jsmin)*1e3 + huv%gil%mura
          else if (i.eq.4) then
             epar(ki) = 1e3*epar(ki)
             vpar(ki) = parout(ki,jsmin)*1e3 + huv%gil%mudec
          else
             vpar(ki) = parout(ki,jsmin)
          endif
          write(chpar,'(2a,f11.5,a,f9.5,a)')  &
            cpar(ki), ' = ', vpar(ki)*factor, ' (',epar(ki)*factor,')'
        else
          epar(ki) = 0.
          write(chpar,'(2a,f11.5,a)')  &
            cpar(ki), ' = ', parout(ki,jsmin)*factor, ' (  fixed  )'
        endif
        nch = len_trim(chpar)
        if (i.eq.1) then
          write(6,*) chpar(1:nch)//'  '//chpos1
        elseif (i.eq.2) then
          write(6,*) chpar(1:nch)//' '//chpos2
        elseif (i.eq.5) then
          write(6,*) chpar(1:nch)//'  '//fluxunit
        else
          write(6,*) chpar(1:nch)
        endif
      enddo
    enddo
    call outfit(nc,kc,nvt,dtab,rms,   &
     &      vit,npar,parout(1,jsmin),epar)
    30      continue
  enddo
  !
  !     Create residuals table (in Z common) and initialize it with the data.
  !
  call gildas_null (hres, type = 'UVT')
  call gdf_copy_header(huv,hres,error)
  call sic_parsef(resid,hres%file,' ','.uvt')
  !
  ! Subtract using DUV as work space...
  !
  !     Loop on functions for subtracting  model.
  do if = 1, nf
    if (savef(if)) then
      write (6,'(a,i2.2,2i8)') 'Removing FUNCT',if,ndata,nc
      call model_data(huv,ndata,nvs,nc,ic(1),ic(2), &
     &        nvt,duv,dtab,if)
    endif
  enddo
  print *,'Writing residual '
  call gdf_write_image(hres,duv,error)
  print *,'Writing TABLE fit '
  call gdf_write_image(htab,dtab,error)
  call gagout('S-UV_FIT,  Successful completion')
  call sysexi(1)
  !
  999  call sysexi(fatale)
  !
end program uv_fit_proper
!
subroutine diagonal(m,n,r,c)
  integer :: m                      !
  integer :: n                      !
  real(kind=8) :: r(m,n)                  !
  real(kind=8) :: c(n)                    !
  ! Local
  integer :: i
  do i=1, n
    c(i) = r(i,i)
  enddo
end subroutine diagonal
!
subroutine outfit(nc,ic,ncol,y,rms,vit,nbpar,par,epar)
  use gildas_def
  use uvfit_data_proper
  !---------------------------------------------------------------------
  ! Store the fitted parameters into the output table Y
  !
  ! Format of table is, for each channel:
  ! 1-4  RMS of fit, number of functions, number of parameters, velocity
  ! then for each function, 3+9*2=21  columns:
  !        Function number, type of function, number of parameters
  !        then 9 times (parameter, error)
  !---------------------------------------------------------------------
  integer, intent(in) :: nc         ! Number of channels
  integer, intent(in) :: ic         ! Current channel
  integer, intent(in) :: ncol       ! Size of Y
  real, intent(out) :: y(nc,ncol)   ! Output table
  real(8), intent(in) :: rms        ! RMS of fit
  real, intent(in) :: vit           ! Velocity
  integer, intent(in) :: nbpar      ! Number of parameters
  real(8), intent(in) :: par(nbpar) ! Parameters
  real(8), intent(in) :: epar(nbpar)! Errors
  ! Local
  integer :: if, ip, k, kcol
  !
  y(ic,1) = rms
  y(ic,2) = nf
  y(ic,3) = nbpar
  y(ic,4) = vit
  kcol = 5
  k = 1
  do if=1, nf
    kcol = 5+(if-1)*21
    y(ic,kcol) = if
    y(ic,kcol+1) = ifunc(if)
    y(ic,kcol+2) = npfunc(if)
    kcol = kcol+3
    do ip=1, npfunc(if)
      y(ic,kcol) = par(k)
      y(ic,kcol+1) = epar(k)
      k = k+1
      kcol = kcol+2
    enddo
  enddo
  return
end subroutine outfit
!
subroutine load_data(dzero,ndata,nx,ic,fact,visi,np,uvriw,uv_min,uv_max)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Load Data of channel IC from the input UV table into array UVRIW.
  !
  !---------------------------------------------------------------------
  real(8), intent(in) :: dzero        ! Reference date
  integer, intent(in) :: ndata        ! Number of visibilities
  integer, intent(in) :: nx           ! Size of a visibility
  integer, intent(in) :: ic           ! Channel number
  real, intent(in) :: fact            ! Conversion factor of baselines
  real, intent(in) :: visi(nx,ndata)  ! Visibilities
  integer, intent(out) :: np          ! Number of non zero visibilities
  real, intent(out) :: uvriw(6,ndata) ! Output array
  real, intent(in) :: uv_min          ! Min UV 
  real, intent(in) :: uv_max          ! Max UV
  ! Local
  ! Local:
  integer :: i, irc, iic, iwc
  real :: pi, uv1, uv2, uv
  integer :: j2000
  character(len=13) :: date
  logical error
  !
  pi = 3.14159265358979323
  !
  ! Get J2000 in CLASS date
  !
  date = '01-JAN-2000'
  call gag_fromdate(date,j2000,error)
  print *,'J2000 ',j2000
  !
  irc = 5 + 3*ic
  iic = irc + 1
  iwc = iic + 1
  np = 0
  if (uv_min.eq.0.0 .and. uv_max.eq.0.0) then
    do i = 1, ndata
      if (visi(iwc,i).gt.0) then
        np = np + 1
        uvriw(1,np) = visi(1,i) * fact
        uvriw(2,np) = visi(2,i) * fact
        uvriw(3,np) = visi(irc,i)
        uvriw(4,np) = visi(iic,i)
        uvriw(5,np) = visi(iwc,i) * 1e6
        uvriw(6,np) = (visi(4,i)-j2000)/365.25d0 - dzero ! In years
      endif
    enddo
  else
    uv1 = uv_min**2
    uv2 = uv_max**2
    do i = 1, ndata
      if (visi(iwc,i).gt.0) then
        uv = visi(1,i)**2+visi(2,i)**2
        if (uv.ge.uv1 .and. uv.lt.uv2) then
          np = np + 1
          uvriw(1,np) = visi(1,i) * fact
          uvriw(2,np) = visi(2,i) * fact
          uvriw(3,np) = visi(irc,i)
          uvriw(4,np) = visi(iic,i)
          uvriw(5,np) = visi(iwc,i) * 1e6
          uvriw(6,np) = (visi(4,i)-j2000)/365.25d0 - dzero ! In years
        endif
      endif
    enddo
  endif
  return
end subroutine load_data
!
subroutine model_data(huv,nd,nx,nc,ic1,ic2,ncol,vin,vy,if)
  use gkernel_interfaces
  use image_def
  !---------------------------------------------------------------------
  ! Subtract the model uv data for component function number IF
  ! from the input visibility data.
  !
  !---------------------------------------------------------------------
  type (gildas), intent(in) :: huv  ! Inut UV Table header
  integer, intent(in) :: nd         ! Number of visibilities
  integer, intent(in) :: nx         ! Size of a visbility
  integer, intent(in) :: nc         ! Number of channels
  integer, intent(in) :: ic1        ! First channel
  integer, intent(in) :: ic2        ! Last channel
  integer, intent(in) :: ncol       ! Size of VY
  real, intent(inout) :: vin(nx,nd) ! Visibilities
  real, intent(in) :: vy(nc,ncol)   ! Model parameters
  integer, intent(in) :: if         ! Function number
  !
  ! Local
  real :: fact
  integer :: j, i, k, iif, npf, kcol, ic
  real(8) :: y(2), dy(2,9), paris(9), uu, vv
  real(8) :: dzero, dtime
  integer :: j2000
  character(len=13) :: date
  real(8) :: pi, clight
  parameter (pi = 3.14159265358979323846d0)
  parameter (clight = 299792458.d-6)   ! in meter.mhz
  logical :: error
  !
  ! Code:
  date = '01-JAN-2000'
  call gag_fromdate(date,j2000,error)
  dzero = dble(huv%gil%epoc)-2000.0d0
  !
  do j=1, nd
    dtime = (vin(4,j)-j2000)/365.25d0 - dzero ! In years
    !
    ! Kcol is the first column number in result table VY for function IF
    kcol = 5+(if-1)*21
    do ic = ic1, ic2
      fact = huv%gil%val(1) * & 
     &        (1.d0+huv%gil%fres/huv%gil%freq*(ic-huv%gil%ref(1)))/clight   &
     &        *pi/180./3600.
      uu = vin(1,j)*fact
      vv = vin(2,j)*fact
      !
      ! IIF is the function type and NPF the number of its parameters.
      iif = nint(vy(ic,kcol+1))
      npf = nint(vy(ic,kcol+2))
      do i=1, npf
        paris(i) = vy(ic,kcol+3+2*(i-1))
      enddo
      call model(iif,npf,uu,vv,paris,y,dy,dtime)
      k = 8 + 3*(ic-1)
      vin(k,j) = vin(k,j) - y(1)
      vin(k+1,j) = vin(k+1,j) - y(2)
    enddo
  enddo
end subroutine model_data
!
subroutine fitfcn(iflag,m,nvpar,x,f,fjac,ljc)
  use gildas_def
  use uvfit_data_proper
  !---------------------------------------------------------------------
  ! FCN is called by DNLS1E
  !
  !---------------------------------------------------------------------
  integer, intent(in) :: iflag          ! Code for print out
  integer, intent(in) :: m              ! Number of data points
  integer, intent(in) :: nvpar          ! Number of variables
  real(8), intent(in) :: x(nvpar)       ! Variable parameters
  real(8), intent(out) :: f(m)          ! Function value at each point
  integer, intent(in) :: ljc            ! First dimension of FJAC
  real(8), intent(out) :: fjac(ljc,nvpar) ! Partial derivatives
  !
  ! Local
  integer :: k, i, l, j, kpar, iif, kvpar
  real(8) :: uu, vv, rr, ii, ww, dd, fact, y(2), dy(2,mpar), denorm
  !
  k = 1
  sw = 0
  !
  ! Put the variable parameters in PARS (already including the fixed ones)
  kvpar = 1
  do j=1, npar
    if (nstart(j).ge.0) then
      pars(j) = x(kvpar)
      kvpar = kvpar+1
    endif
  enddo
  do i=1, np
    !
    ! Get real and imaginary part of visibility
    call getvisi(np,uvriw,i,uu,vv,rr,ii,ww,dd)
    !
    ! Initialize F and derivatives.
    ! the weights are 1/sigma**2 (within a factor 2)
    fact = ww
    if (iflag.eq.1) then
      f(k) = -rr
      f(k+1) = -ii
    elseif (iflag.eq.2) then
      do l=1, nvpar
        fjac(k,l) = 0
        fjac(k+1,l) = 0
      enddo
    endif
    !
    ! Compute F and FJAC from model
    kpar = 1
    kvpar = 1
    do j = 1, nf
      iif = ifunc(j)
      call model(iif,npfunc(j),uu,vv,pars(kpar),y,dy,dd)
      if (iflag.eq.1) then
        f(k) = f(k) + y(1)
        f(k+1) = f(k+1) + y(2)
        kpar = kpar + npfunc(j)
      elseif (iflag.eq.2) then
        do l = 1, npfunc(j)
          if (nstart(kpar).ge.0) then
            fjac(k,kvpar) = fjac(k,kvpar) + dy(1,l)
            fjac(k+1,kvpar) = fjac(k+1,kvpar) + dy(2,l)
            kvpar = kvpar+1
          endif
          kpar = kpar + 1
        enddo
      endif
    enddo
    !
    ! Weight appropriately:
    !
    if (iflag.eq.1) then
      f(k) = f(k)*fact
      f(k+1) = f(k+1)*fact
    elseif (iflag.eq.2) then
      do l=1, nvpar
        fjac(k,l) = fjac(k,l)*fact
        fjac(k+1,l) = fjac(k+1,l)*fact
      enddo
    endif
    k = k + 2
    sw = sw + fact
  enddo
  !
  ! In the end: normalize by the sum of weights.
  k = 1
  do i=1, np
    if (iflag.eq.1) then
      f(k) = f(k)/sw
      f(k+1) = f(k+1)/sw
    elseif (iflag.eq.2) then
      do l=1, nvpar
        fjac(k,l) = fjac(k,l)/sw
        fjac(k+1,l) = fjac(k+1,l)/sw
      enddo
    endif
    k = k + 2
  enddo
  if (iflag.eq.0) then
    print 1000, (x(i), i=1, nvpar), denorm(k-1,f)
    1000     format (10(1pg19.12))
  endif
  !!	print *,'Done FITFCN'
  return
end subroutine fitfcn
!
subroutine getvisi(n,uvriw,k,u,v,r,i,w,d)
  !---------------------------------------------------------------------
  !     Obtain U, V, and the visibility from the array UVRIW.
  !
  !     N      Integer     Number of visibilities     Input
  !     UVRIW  real(8)      Input array                Input
  !     K      Integer     Visibility number          Input
  !     U      real(8)      U coord.                   Output
  !     V      real(8)      V coord.                   Output
  !     R      real(8)      Real part                  Output
  !     I      real(8)      Imaginary part.            Output
  !     W      real(8)      Weight                     Output
  !---------------------------------------------------------------------
  integer :: n                      !
  real :: uvriw(6,n)                !
  integer :: k                      !
  real(8) ::  u                      !
  real(8) ::  v                      !
  real(8) ::  r                      !
  real(8) ::  i                      !
  real(8) ::  w                      !
  real(8) :: d
  !
  u = uvriw(1,k)
  v = uvriw(2,k)
  r = uvriw(3,k)
  i = uvriw(4,k)
  w = uvriw(5,k)
  d = uvriw(6,k)
end subroutine getvisi
!
subroutine outpar(k,kfunc,par)
  use image_def
  !---------------------------------------------------------------------
  ! Process fitted parameters to be saved, according to fitted functions
  !---------------------------------------------------------------------
  integer, intent(in) :: k     ! Function code
  integer, intent(in) :: kfunc ! Number of parameters
  real(8), intent(inout) :: par(kfunc)  ! Parameters
  ! Local
  real(8) :: aa
  integer :: icgauss,iegauss, ipoint, icdisk, iedisk
  parameter (iegauss=2,ipoint=1,icgauss=3,icdisk=4,iedisk=9)
  !
  ! Gaussian:  Reorder the axes, and change the P. Angle accordingly
  ! Make sure the axes are positive.
  !
  if (k.eq.iegauss .or. k.eq.iedisk) then
    par(7) = abs(par(7))
    par(6) = abs(par(6))
    if (par(6).lt.par(7)) then
      par(8) = par(8)+90d0
      aa = par(6)
      par(6) = par(7)
      par(7) = aa
    endif
    par(8) = -90d0+mod(par(8)+3690d0,180d0)
  elseif (k.eq.icgauss .or. k.eq.icdisk) then
    par(6) = abs(par(6))
  endif
end subroutine outpar
!
subroutine  model(ifunc,kfunc,uu,vv,x,y,dy,dtime)
  !---------------------------------------------------------------------
  ! Compute a model function.
  !
  !---------------------------------------------------------------------
  integer, intent(in) :: ifunc       ! Fuction type 
  integer, intent(in) :: kfunc       ! Number of parameters
  real(8), intent(in) :: uu          ! U coordinate in arcsec^-1
  real(8), intent(in) :: vv          ! V coordinate in arcsec^-1
  real(8), intent(in) :: dtime       ! Time offset
  real(8), intent(in) :: x(kfunc)    ! Parameters
  real(8), intent(inout) :: y(2)        ! Function (real, imag.)
  real(8), intent(inout) :: dy(2,kfunc) ! Partial derivatives.
  !
  ! Global
  real(8) :: z_exp
  !
  ! Local
  integer :: i
  real(8) :: st, ct, q1, q2, arg, carg, sarg, darg1, darg2
  real(8) :: a, da(9), b, ksi, q, j0, j1, aa, dfac4, dfac5
  real(8) :: dadksi
  real(8) :: dbesj0, dbesj1, dbesk0, dbesk1
  real(8) :: pi, dpi, deg, a1, a2, cst, fac, k0, k1, d, daa
  parameter (pi = 3.14159265358979323846d0, dpi=2d0*pi,   &
     &    cst=3.5597073312469d0,   &   ! pi**2/4d0/log(2d0)
     &    deg=pi/180d0)
  real(8) fa,flux,dg_dmaj,dg_dmin,dg_dpa,dfa_dinner,dfa_douter
  real(8) dg_douter,dg_dinner,dg_dratio,dg_drota
  !-----------------------------------------------------------------------
  !
  ! The fitted function is of the form:
  !     F = flux * f(x-x0,y-y0)
  ! corresponding to a visibility model:
  !     G = flux * exp(-2i*pi(u*x0+v*y0)) * g(u,v)
  ! where g is the Fourier transform of f.
  !
  ! The first three parameters 1, 2, 3, are x0, y0, and flux.
  !
  ! First compute A = g(u,v), and DA(k) = dA/dx_k for k=4, ...
  ! depending on model.
  !
  goto (10,20,30,40,50,60,70,80,90,100,110) ifunc
  !
  ! 1- Point source
  ! model = flux * delta(x-x0,y-y0)
  ! g(u,v) = 1
  ! 5 pars (x0, y0, mu_a, mu_b, flux)
  10    a = 1d0
  goto 1000
  !
  ! 2- Gaussian (2-dim)
  !     model = flux*4*ln2/(pi*b1*b2) * exp (-4*ln2*((r1/b1)**2+(r2/b2)**2)
  !     g(u,v) = exp(-pi**2/4/ln2*(b1*u1+b2*u2)**2)
  !       where u1 = u*sin(theta)+v*cos(theta)
  !             u2 = u*cos(theta)-v*sin(theta)
  !     8 pars (x0, y0, mu_a, mu_b, flux, theta, b1, b2)
  !
  20    continue
  st = sin(deg*x(8))
  ct = cos(deg*x(8))
  q1 = uu*st+vv*ct
  q2 = uu*ct-vv*st
  a1 = -cst*q1**2
  a2 = -cst*q2**2
  a = z_exp(a1*x(6)**2+a2*x(7)**2)
  da(6) = 2*x(6)*a1*a
  da(7) = 2*x(7)*a2*a
  da(8) = -2d0*cst*q1*q2*(x(6)**2-x(7)**2)*deg*a
  goto 1000
  !
  ! 3- Gaussian (1-dim)
  !     model = flux*4/(ln2*pi*b**2) * exp (-4*ln2*(r/b)**2)
  !     g(u,v) = exp(- pi**2/4/ln(2)*(b*q)**2)
  !     where q**2 = u**2+v**2
  !     6 pars (x0, y0, mu_a, mu_b, flux, b)
  !
  30    continue
  q2 = uu**2+vv**2
  a1 = -cst*q2
  a = z_exp(a1*x(6)**2)
  da(6) = a1*x(6)*2d0*a
  goto 1000
  !
  ! 4- Disk
  !     model = flux*4/(pi*b**2)
  !     g(u,v) = J1(pi*b*q)/q
  !     where q**2 = u**2+v**2
  !     6 pars (x0, y0, mu_a, mu_b, flux, b)
  !
  40    continue
  q = sqrt(uu**2+vv**2)
  ksi = pi*x(6)*q
  if (ksi.eq.0d0) then
    a = 1
    da(6) = 0
  else
    j1 = dbesj1(ksi)
    j0 = dbesj0(ksi)
    a = j1*2d0/ksi
    da(6) = 2d0*(j0-a)/x(6)
  endif
  goto 1000
  !
  ! 5- Ring
  50    continue
  q = sqrt(uu**2+vv**2)
  a = 0
  da(6) = 0
  da(7) = 0
  if (x(6).eq.x(7)) goto 1000
  d = x(6)**2-x(7)**2
  fac = x(6)**2/d
  dfac4 =-2*x(7)**2*x(6)/d**2
  dfac5 =2*x(6)**2*x(7)/d**2
  ksi = pi*x(6)*q
  if (ksi.eq.0) then
    a = a + fac
    da(6) = da(6) + dfac4
    da(7) = da(7) + dfac5
  else
    j1 = dbesj1(ksi)
    j0 = dbesj0(ksi)
    aa = j1*2d0/ksi
    daa = 2d0*(j0-aa)/x(6)
    a = a + aa*fac
    da(6) = da(6) + daa*fac+aa*dfac4
    da(7) = da(7) + aa*dfac5
  endif
  ksi = pi*x(7)*q
  if (ksi.eq.0) then
    a = a+1d0-fac
    da(6) = da(6)-dfac4
    da(7) = da(7)-dfac5
  else
    j1 = dbesj1(ksi)
    j0 = dbesj0(ksi)
    aa = j1*2d0/ksi
    daa = 2d0*(j0-aa)/x(7)
    a = a + aa*(1d0-fac)
    da(6) = da(6) - aa*dfac4
    da(7) = da(7) + daa*(1d0-fac)-aa*dfac5
  endif
  goto 1000
  !
  ! 6- exponential
  !     model = exp(- 2*ln2*r/b)
  !     g(u,v) = 1 / ( 1 + (q*b/ln2)**2 )**(3/2)
  !
  60    continue
  q2 = uu**2+vv**2
  ksi = q2 * (x(6)/log(2d0))**2
  b = 1d0+ksi
  a = 1d0/b**1.5
  da(6) = -3*a*ksi/b/x(6)
  goto 1000
  !
  ! 7- Power -2
  !     model =
  !     g(u,v) =
  !
  70    continue
  q = sqrt(uu**2+vv**2)
  ksi = pi*x(6)*q
  k0 = dbesk0(ksi)
  k1 = dbesk1(ksi)
  a = k0
  da(6) = -ksi/x(6)*k1
  goto 1000
  !
  ! 8- Power -3
  !     model =
  !     g(u,v) = exp( -pi*q*b/sqrt(2**(1/3)-1) )
  !
  80    continue
  q = sqrt(uu**2+vv**2)
  ksi = pi*q*x(6)/sqrt(2.**(1./3)-1)
  a = z_exp(-ksi)
  da(6) = -a*ksi/x(6)
  goto 1000
  !
  ! 9- Elliptical Disk
  !     model = flux*4/(pi*(b1**2+b2**2)) (inside ellipse, 0 outside)
  !     g(u,v) = J1(pi*sqrt((b1*u1)**2+(b2*u2)**2)
  !     where u1 = u*sin(theta)+v*cos(theta)
  !           u2 = u*cos(theta)-v*sin(theta)
  !     6 pars (x0, y0, flux, theta, b1, b2)
  !
  90    continue
  st = sin(deg*x(8))
  ct = cos(deg*x(8))
  q1 = uu*st+vv*ct
  q2 = uu*ct-vv*st
  ksi = pi*sqrt((x(6)*q1)**2+(x(7)*q2)**2)
  if (ksi.eq.0d0) then
    a = 1
    da(6) = 0
    da(7) = 0
    da(8) = 0
  else
    j1 = dbesj1(ksi)
    j0 = dbesj0(ksi)
    a = j1*2d0/ksi
    dadksi = 2d0*(j0-a)/ksi
    da(6) = dadksi*pi**2*q1**2*x(6)/ksi
    da(7) = dadksi*pi**2*q2**2*x(7)/ksi
    da(8) = dadksi/ksi*pi**2*q1*q2*(x(6)**2-x(7)**2)*deg
  endif
  goto 1000
  !
  ! 10- Unresolved Ring
  !     model = flux/(2*pi*b)*delta(r-b)
  !     g(u,v) = J0(pi*b*q)
  !     where q**2 = u**2+v**2
  !     4 pars (x0, y0, flux, b)
  !
  100  continue
  q = sqrt(uu**2+vv**2)
  ksi = pi*x(6)*q
  if (ksi.eq.0d0) then
    a = 1
    da(6) = 0
  else
    j1 = dbesj1(ksi)
    j0 = dbesj0(ksi)
    a = j0
    da(6) = -j1 * pi*q
  endif
  goto 1000
  !
  ! 11- Elliptical Ring
  !
  !     To be developped more...
  !
  !     7 pars (x0, y0, flux, outer, inner, pa, ratio)
  !
  !     Define a1 = outer
  !     Define a2 = outer*ratio
  !     Define b1 = inner
  !     Define b2 = inner*ratio
  !
  ! Sum of two elliptical disks.
  !     a) a = fa *4/(pi*outer^2*ratio) > 0
  !        ga(u,v) = fa * J1(pi*sqrt((a1*u1)**2+(a2*u2)**2)
  !     b) b = fb *4/(pi*inner^2*ratio) < 0
  !        gb(u,v) = fb * J1(pi*sqrt((b1*u1)**2+(b2*u2)**2)
  !     and
  !        fa + fb = flux
  !        fa / fb = -(outer/inner)^2
  !     where u1 = u*sin(theta)+v*cos(theta)
  !           u2 = u*cos(theta)-v*sin(theta)
  !*     so
  !        fa = flux * outer^2 / (outer^2-inner^2)
  !        fb = - flux * inner^2 / (outer^2-inner^2)
  !
  ! Derivative vs outer
  !
  !     d(fa)/d(out) * J1 + fa * d(J1)/d(out)
  !
  110  continue
  st = sin(deg*x(8))
  ct = cos(deg*x(8))
  q1 = uu*st+vv*ct
  q2 = uu*ct-vv*st
  !
  ! First source
  a1 = x(6)
  a2 = x(6)*x(9)
  fa = x(6)**2 / (x(6)**2 - x(7)**2)
  !
  ksi = pi*sqrt((a1*q1)**2+(a2*q2)**2)
  if (ksi.eq.0d0) then
    a = 1
    dg_dmaj = 0
    dg_dmin = 0
    dg_dpa = 0
  else
    j1 = dbesj1(ksi)
    j0 = dbesj0(ksi)
    a = j1*2d0/ksi
    dadksi = 2d0*(j0-a)/ksi
    dg_dmaj = dadksi*pi**2*q1**2*a1/ksi    ! d(g)/d(major)
    dg_dmin = dadksi*pi**2*q2**2*a2/ksi    ! d(g)/d(minor)
    dg_dpa = dadksi/ksi*pi**2*q1*q2*(a1**2-a2**2)*deg
  endif
  !
  dfa_douter = 2*x(6)/(x(6)**2 - x(7)**2) -   &
     &    2*x(6)*x(6)**2/(x(6)**2-x(7)**2)**2   ! Dubious ...
  !
  flux = a*fa
  dg_douter = fa * (dg_dmaj + x(9) * dg_dmin)
  dg_douter = dg_douter + dfa_douter * a
  dg_dratio = fa * a1 * dg_dmin
  dg_drota = fa * dg_dpa
  !
  ! Second source
  a1 = x(7)
  a2 = x(7)*x(9)
  fa = - x(7)**2 / (x(6)**2 - x(7)**2)
  !
  ksi = pi*sqrt((a1*q1)**2+(a2*q2)**2)
  if (ksi.eq.0d0) then
    a = 1
    dg_dmaj = 0
    dg_dmin = 0
    dg_dpa = 0
  else
    j1 = dbesj1(ksi)
    j0 = dbesj0(ksi)
    a = j1*2d0/ksi
    dadksi = 2d0*(j0-a)/ksi
    dg_dmaj = dadksi*pi**2*q1**2*a1/ksi    ! d(g)/d(major)
    dg_dmin = dadksi*pi**2*q2**2*a2/ksi    ! d(g)/d(minor)
    dg_dpa = dadksi/ksi*pi**2*q1*q2*(a1**2-a2**2)*deg
  endif
  dfa_dinner =  2*x(7)*x(6)**2/(x(6)**2-x(7)**2)**2
  !
  flux = flux + a*fa
  dg_dinner = fa * (dg_dmaj + x(9) * dg_dmin)
  dg_dinner = dg_dinner + dfa_dinner * a
  dg_dratio = dg_dratio + fa * a1 * dg_dmin
  dg_drota = dg_drota + fa * dg_dpa
  !
  a = flux                     ! Normalized scaling factor now
  da(6) = dg_douter
  da(7) = dg_dinner
  da(8) = dg_drota
  da(9) = dg_dratio
  goto 1000
  !
  ! Compute the phase term exp(-2i*pi(u*x0+v*y0)) and its derivatives
  ! with respect to x_0 and y_0
  !
  ! correct sign if x is positive towards East.
  1000   continue
  darg1 = dpi*uu
  darg2 = dpi*vv
  arg = darg1*(x(1)+x(3)*dtime)+darg2*(x(2)+x(4)*dtime)
  carg = cos(arg)
  sarg = sin(arg)
  !
  ! Now compute the real and imaginary part of function.
  y(1) = x(5)*a*carg
  y(2) = x(5)*a*sarg
  !
  ! And its derivatives with respect to x0, y0
  dy(1,1) = -y(2)*darg1
  dy(2,1) =  y(1)*darg1
  dy(1,2) = -y(2)*darg2
  dy(2,2) =  y(1)*darg2
  !
  ! with respect to Proper motion
  dy(1,3) = dy(1,1)*dtime
  dy(2,3) = dy(2,1)*dtime
  dy(1,4) = dy(1,2)*dtime
  dy(2,4) = dy(2,2)*dtime
  !
  ! with respect to Flux
  dy(1,5) = a*carg
  dy(2,5) = a*sarg
  ! And its derivatives with respect to parameters 6 to 9 
  if (kfunc.ge.6) then
    do i=6, kfunc
      dy(1,i) = x(5)*carg*da(i)
      dy(2,i) = x(5)*sarg*da(i)
    enddo
  endif
end subroutine model
!
function z_exp(x)
  !---------------------------------------------------------------------
  ! protect again underflows in exp(x)
  !---------------------------------------------------------------------
  real(8) :: z_exp                   !
  real(8) :: x                       !
  ! Local
  real(8) :: d1mach, xmin, ymin
  logical :: first
  data first/.true./
  save xmin, ymin, first
  !
  if (first) then
    ymin = 2*d1mach(1)
    xmin = log(ymin)
    first = .false.
  endif
  if (x.lt.xmin) then
    z_exp = ymin
    return
  endif
  z_exp = exp(x)
end function z_exp
