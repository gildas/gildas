program uv_clip
  use gildas_def
  use gkernel_interfaces
  use image_def
  !---------------------------------------------------------------------
  ! GILDAS
  !	CLIP an input UV table
  !---------------------------------------------------------------------
  ! Local
  character(len=filename_length) :: uvdata
  integer :: nc(2)
  real :: val
  logical :: error, clipall
  !
  ! Code:
  call gildas_open
  call gildas_char('UVDATA$',uvdata)
  call gildas_real('VCLIP$',val,1)
  call gildas_inte('CHANNELS$',nc,2)
  ! CLIPALL .true. means that one clips all channel at bad values
  call gildas_logi('CLIPALL$',clipall,1)
  call gildas_close
  !
  !
  call sub_uv_clip (uvdata, val, nc, clipall, error)
  if (error) call sysexi(fatale)
end program uv_clip
!
subroutine sub_uv_clip (cuvin, val, nc, clipall, error)
  use gkernel_interfaces
  use image_def
  use gbl_message
  !---------------------------------------------------------------------
  ! GILDAS
  !	A template for handling UV tables, with an Input and Output one
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: cuvin 
  real, intent(in) :: val
  integer, intent(in) :: nc(2)
  logical, intent(in) :: clipall
  logical, intent(out) :: error
  !
  ! Local variables
  character(len=*), parameter :: rname='UV_CLIP'
  character(len=80)  :: mess
  type (gildas) :: uvin
  integer :: ier, nblock, ib, my_nc(2)
  integer(kind=index_length) :: nx,nvisi
  !
  ! Simple checks 
  error = len_trim(cuvin).eq.0
  if (error) then
    call map_message(seve%e,rname,'No input UV table name')
    return
  endif
  !
  call gildas_null (uvin, type = 'UVT')     ! Define a UVTable gildas header
  call gdf_read_gildas (uvin, cuvin, '.uvt', error, data=.false.)
  if (error) then
    call map_message(seve%e,rname,'Cannot read input UV table')
    return
  endif
  !
  my_nc = nc  
  ier = gdf_range(my_nc,uvin%gil%nchan)
  if (ier.ne.0) then
    error = .true.
    call map_message(seve%e,rname,'Invalid channel range')
    return
  endif
  !
  ! Define blocking factor, on largest data file, usually the input one
  ! but not always...  
  call gdf_nitems('SPACE_GILDAS',nblock,uvin%gil%dim(1)) ! Visibilities at once
  nblock = min(nblock,uvin%gil%dim(2))
  ! Allocate respective space 
  allocate (uvin%r2d(uvin%gil%dim(1),nblock), stat=ier)
  if (ier.ne.0) then
    write(mess,*) 'Memory allocation error ',uvin%gil%dim(1), nblock
    call map_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  ! Loop over line table - 
  uvin%blc = 0
  uvin%trc = 0
  nx = uvin%gil%dim(1)
  !
  do ib = 1,uvin%gil%dim(2),nblock
    write(mess,*) ib,' / ',uvin%gil%dim(2),nblock
    call map_message(seve%d,rname,mess) 
    uvin%blc(2) = ib
    uvin%trc(2) = min(uvin%gil%dim(2),ib-1+nblock) 
    call gdf_read_data (uvin,uvin%r2d,error)
    !
    ! Here do the job
    nvisi = uvin%trc(2)-uvin%blc(2)+1
    call clip (uvin%r2d, nx, nvisi, uvin%gil%nchan, uvin%gil%nlead, &
         val,my_nc,clipall)
    !
    call gdf_write_data (uvin,uvin%r2d,error)
    if (error) return
  enddo
  !
  ! Finalize the image
  call gdf_close_image(uvin,error)
  if (error) return
  deallocate(uvin%r2d)
  !
  call map_message(seve%i,rname,'Successful completion')
  return
end subroutine sub_uv_clip
!
subroutine clip (out,nx,nv,nchan,nlead,val,nc,clipall)
  use gkernel_interfaces
  integer(kind=index_length), intent(in)    :: nx
  integer(kind=index_length), intent(in)    :: nv
  real(kind=4),               intent(inout) :: out(nx,nv)
  integer(kind=4),            intent(in)    :: nchan
  integer(kind=4),            intent(in)    :: nlead
  real(kind=4),               intent(in)    :: val
  integer(kind=4),            intent(in)    :: nc(2)
  logical,                    intent(in)    :: clipall
  ! Local
  integer(kind=4) :: i,j,ibad,it,itold
  character(len=16) :: chd
  real(kind=4) :: amp,val2
  logical :: bad,error
  !
  itold = -100000
  !
  val2 = val*val
  do j=1,nv
    bad = .false.
    !
    ! Locate bad channels
    do i=3*nc(1),3*nc(2),3
      if (out(nlead+i,j).ne.0) then
        amp = out(nlead+i-2,j)**2 + out(nlead+i-1,j)**2
        if (amp.ne.amp .or. amp.gt.val2) then
          bad = .true.
          ibad = i/3
          it = out(4,j)    ! Assume Date is here...
          if (it.ne.itold) call gag_todate(it,chd,error)
          itold = it
          write (*,100)   &
            'Visi (',j,ibad,')   =   (',out(nlead+i-2,j),',',   &
            out(nlead+i-1,j),'), Scan ',nint(out(3,j)),chd  ! Hum, scan number not adequate..
          out(nlead+i-2:nlead+i-1,j) = 0
        endif
      endif
    enddo
    !
    ! Flag all channels to keep same beam
    if (bad.and.clipall) then
      do i=nlead+3,3*(nchan-1)+10,3
        out(i,j) = 0
      enddo
    endif
  enddo
  100   format(1x,a,i6,i4,a,1pg10.3,a,1pg10.3,a,i4,1x,a)
end subroutine clip
