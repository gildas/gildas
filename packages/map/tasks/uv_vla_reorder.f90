program uv_vla_reorder
  use gkernel_interfaces
  !
  ! Re-order a UV table coming from VLA with IFs
  ! by increasing frequencies
  !
  ! Input:
  !   A UV Table with visibilities containing Nchan, in arbitrary
  !   frequency order
  ! Output
  !   A UV Table with visibilities containing Nchan, ordered in frequency
  !
  character(len=filename_length) :: nami,namo
  integer :: nif
  real(8), allocatable :: myfreqs(:)
  logical error
  !
  call gildas_open
  call gildas_char('INPUT$',nami)
  call gildas_char('OUTPUT$',namo)
  call gildas_close
  call sub_vla_reorder (nami,namo,error)
  call gagout('I-UV_VLA_REORDER,  Successful completion')
end program uv_vla_reorder
!
subroutine sub_vla_reorder(nami,namo,error)
  use image_def
  use gkernel_interfaces
  use gbl_message
  !
  character(len=*), intent(inout) :: nami  ! Input file name
  character(len=*), intent(inout) :: namo  ! Output file name
  logical, intent(out) :: error
  !
  character(len=*), parameter :: rname='UV_VLA_REORDER'
  type (gildas) :: hin, hou
  real(kind=4), allocatable :: din(:,:), dou(:,:)
  integer :: nif
  real(8), allocatable :: freqs(:)
  !
  integer :: stoke
  character(len=message_length) :: mess
  !
  integer i, istoke, ivisi, is, ns, iv, jv, ier
  integer astoke(4)
  integer natom, nlead, nchan, kin, kou, next
  logical extract
  real :: re, im, we
  integer intrail, ontrail, iend, oend
  real :: da, db
  integer :: ip
  integer :: kv, iloop, nblock, nvisi, mblock, multi
  real(8) :: fmin, fmax, df
  real(8), allocatable :: dfreq(:)
  integer, allocatable :: indx(:)
  integer :: ic,kc
  !
  error = .false.
  !
  ! Read Header of the UV table
  call gildas_null(hin, type= 'UVT')
  call sic_parsef (nami,hin%file,' ','.uvt')
  call gdf_read_header (hin,error)
  if (error) return
  !
  nif = hin%gil%nchan
  if (hin%gil%nstokes.gt.1) then
    call map_message(seve%e,rname,'UV table has more than 1 Stoke parameter')
    error = .true.
    return
  endif  
  ! Initialize the output one
  call gildas_null(hou, type='UVT')
  call gdf_copy_header (hin, hou, error)
  call sic_parsef (namo,hou%file,'  ','.uvt')
  !
  stoke = 0
  if (hin%gil%nstokes.eq.1) then
    if (associated(hin%gil%stokes)) stoke = hin%gil%stokes(1)
  endif
  !
  allocate(hou%gil%freqs(nif), hou%gil%stokes(nif), indx(nif), stat=ier)
  !
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Memory allocation error')
    error = .true.
    return
  endif  
  !
  ! Sort the frequencies by increasing order
  hou%gil%freqs = hin%gil%freqs
  do ic = 1,nif
    indx(ic) = ic
  enddo
  call gr8_trie(hou%gil%freqs,indx,nif,error)
  hou%gil%stokes = stoke
  hou%gil%nstokes = 1
  hou%gil%nfreq = nif  
  !
  ! Reset the mean frequency in the header...
  hou%gil%freq = sum(hou%gil%freqs)/nif
  ! and in the first axis too
  hou%gil%val(1) = hou%gil%freq
  !
  call gdf_setuv(hou,error)
  !
  ! OK copy the whole stuff...
  call gdf_nitems('SPACE_GILDAS',nblock,hin%gil%dim(1))
  nblock = min(nblock,hin%gil%dim(2))
  !
  allocate (din(hin%gil%dim(1),nblock),dou(hin%gil%dim(1),nblock),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Memory allocation error ')
    error = .true.
    return
  endif
  !
  hin%blc = 0
  hin%trc = 0
  hou%blc = 0
  hou%trc = 0
  call gdf_create_image(hou,error)
  if (error) return
  !
  call map_message(seve%i,rname,'Copying UV data ')
  do iloop = 1,hin%gil%dim(2),nblock
    hin%blc(2) = iloop
    hin%trc(2) = min(iloop+nblock-1,hin%gil%dim(2))
    hou%blc(2) = iloop
    hou%trc(2) = hin%trc(2)
    call gdf_read_data (hin,din,error)
    if (error) return
    !
    nvisi = hin%trc(2)-iloop+1
    do iv=1,nvisi
      dou(1:7,iv) = din(1:7,iv)
      do ic=1,nif
        kc = indx(ic)
        dou(5+3*ic:7+3*ic,iv) = din(5+3*kc:7+3*kc,iv)
      enddo
      if (hou%gil%ntrail.gt.0) then
        dou(8+3*nif:,iv) = din(8+3*nif:,iv)
      endif
    enddo
    call gdf_write_data (hou, dou, error)
    if (error) return
  enddo
  call gdf_close_image(hou,error)
  !
end subroutine sub_vla_reorder
