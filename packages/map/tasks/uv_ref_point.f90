program uv_ref_point
  use gildas_def
  use gkernel_interfaces
  use mapping_interfaces
  use image_def
  use gbl_format
  !---------------------------------------------------------------------
  ! TASK  Self-Calibration
  !	Applies "Self-Calibration" results to a CLIC UV table.
  ! Input :
  !	TABLE$	The UV table to be self-calibrated
  !	SELF$	The "gain" table, i.e. a UV table used as phase reference
  !	DTIME$	The smoothing time constant
  !	TYPE$	Type of self-cal, Ampli, Phase or Both
  !	SUBT$	Subtract source model from data
  ! S.Guilloteau 3-Avril-1990
  !---------------------------------------------------------------------
  ! Local
  type(gildas) :: x,y
  character(len=filename_length) :: uv_table,self_name,name
  character(len=8) :: type
  real(8) :: time,stime,dtime
  integer :: wcol,itype
  !
  real(8), allocatable :: ipw(:)
  integer, allocatable :: ipi(:)
  integer :: sblock=1000
  integer :: j,n,k,ier, wrange(2)
  integer :: i,nvs,nvi,nsc,nc
  complex :: resul
  real :: resulr,resuli,resulm,factor,reel,imgi,rbase(2),uv(2)
  logical :: error
  !
  call gildas_open
  call gildas_char('UV_TABLE$',uv_table)
  call gildas_char('SELF$',self_name)
  call gildas_dble('DTIME$',dtime,1)
  call gildas_inte('WCOL$',wcol,1)
  call gildas_char('TYPE$',type)
  call gildas_real('SUB$',factor,1)
  if (type.eq.'BOTH') then
    itype = 0
  elseif (type.eq.'AMPLI') then
    itype = -1
  elseif (type.eq.'PHASE') then
    itype = 1
  endif
  call gildas_close
  dtime = 0.5d0*dtime
  !
  ! Open continuum table
  n = lenc(self_name)
  if (n.le.0) goto 999
  name = self_name(1:n)
  call gildas_null (y, type = 'UVT')
  call gdf_read_gildas(y, name, '.uvt', error)
  if (error) then
    call gagout('F-SELF_CAL,  Cannot read input UV table')
    goto 999
  endif
  nsc = y%gil%nchan 
  wcol = max(0,wcol)
  wcol = min(wcol,nsc)
  wrange(1:2) = wcol
  !
  ! Load time variable and sort by time order
  nvs = y%gil%dim(1)
  nvi = y%gil%dim(2)
  allocate (ipw(nvi), ipi(nvi), stat=ier)
  !
  ! Open line table
  n = lenc(uv_table)
  if (n.le.0) goto 999
  name = uv_table(1:n)
  call gildas_null(x, type = 'UVT')
  call gdf_read_gildas(x, name, '.uvt', error, data=.false.)
  if (error) then
    call gagout ('F-SELF_CAL,  Cannot read input/output UV table')
    goto 999
  endif
  nc = x%gil%nchan 
  !
  ! Sort self-cal table
  call dotime (nvs, nvi, y%r2d,   &
     &    ipw, ipi, stime)
  !
  allocate (x%r2d(x%gil%dim(1), sblock), stat=ier)
  !
  ! Loop over line table
  do i=1,x%gil%dim(2),sblock
    x%blc(2) = i
    x%trc(2) = min(x%gil%dim(2),i-1+sblock)
    call gdf_read_data(x,x%r2d,error)
    k = 0
    do j=x%blc(2),x%trc(2)
      k = k+1
      !
      ! Get time and baseline
      call getiba (x%r2d(:,k),stime,time,rbase,uv)
      !
      ! Compute self-cal correction for that baseline
      call geself (nvs, nvi, wrange, y%r2d,   &
     &        time,dtime,ipw,ipi,   &
     &        rbase,resul,uv)
      ! apply self-cal : Amplitude Phase or Phase only
      !
      ! Probably with appropriate model=(1,0)
      !
      ! call doself  (model,resul,itype,self,weight)
      !
      resulr = real(resul)
      resuli = imag(resul)
      resulm = resulr**2+resuli**2
      if (itype.gt.0) then
        resulm = sqrt(resulm)  ! Phase only
        reel = factor*resulm
        imgi = 0.0
      elseif (itype.lt.0) then ! Amplitude
        resulr = 1.0
        resuli = 0.0
        if (resulm.gt.0) then
          reel = factor*resulr/sqrt(resulm)
          imgi = factor*resuli/sqrt(resulm)
        endif
      else                     ! Both
        reel = factor
        imgi = 0.0
      endif
      if (resulm.gt.0) then
        resulr = resulr/resulm
        resuli = resuli/resulm
      endif
      call doscal (nc,x%r2d(:,k),resulr,resuli,reel,imgi,1.0)
    enddo
    call gdf_write_data(x,x%r2d,error)
  enddo
  ! end loop
  call gdf_close_image(x,error)
  call gdf_close_image(y,error)
  call gagout('S-SELF_CAL,  Successful completion')
  call sysexi(1)
  999   call sysexi(fatale)
end program uv_ref_point
