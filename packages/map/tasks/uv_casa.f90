program uv_casa
  use gkernel_interfaces
  !
  ! Make a "CASA to Gildas" preparation for a UV Table
  !
  ! - Split a UV table by Polarization
  ! - run uv_noise
  ! - trim the result
  !
  ! Input:
  !   A UV Table with visibilities containing Nstokes x Nchan
  ! Output
  !   A UV Table with visibilities of size Nchan, but potentially different
  !   polarization states, properly re-weighted stuff, and no flagged data.
  !
  character(len=filename_length) :: nami,namo,namef
  character(len=12) :: mystoke
  character(len=filename_length) :: uvchannels
  character(len=12) :: ctype
  real(kind=4) :: threshold
  logical :: error,keep,dotrim
  integer :: iblock(2), wcol
  !
  call gildas_open
  call gildas_char('INPUT$',nami)
  call gildas_char('OUTPUT$',namo)
  call gildas_char('STOKES$',mystoke)
  call gildas_char('RANGES$',uvchannels)
  call gildas_char('CTYPE$',ctype)
  call gildas_real('THRESHOLD$',threshold,1)
  call gildas_inte('BLOCK$',iblock,2)
  call gildas_logi('KEEP$',keep,1)
  call gildas_logi('TRIM$',dotrim,1)
  call gildas_inte('WCOL$',wcol,1)
  call gildas_close
  !
  call sic_upper(mystoke)
  namef = ' '
  call sub_uv_casa (nami, namo, mystoke, uvchannels, ctype, threshold, &
      & iblock, keep, dotrim, wcol, error, namef)
  if (error) then
    call gagout('I-UV_CASA, Deleting corrupted data file')
    call gag_filrm(namef)
    call sysexi(fatale)
  endif
  call gagout('I-UV_CASA,  Successful completion')
  call sysexi(1)
  !
  !
  contains
!<FF>
subroutine sub_uv_casa(nami,namo,mystoke,channels,ctype,threshold, &
      & iblock,keep,dotrim,wcol,error,namef)
  use image_def
  use gkernel_interfaces
  use gbl_message
  !
  character(len=*), intent(inout) :: nami  ! Input file name
  character(len=*), intent(inout) :: namo  ! Output file name
  character(len=*), intent(in) :: mystoke  ! Desired Stoke parameterr
  character(len=*), intent(in) :: channels ! "channel" range string
  character(len=*), intent(in) :: ctype    ! type of values for ranges
  real, intent(in) :: threshold            ! Maximum tolerable deviation from median
  integer, intent(in) :: iblock(2)         ! Number of visibilities for blocking / printout
  integer, intent(in) :: wcol              ! Weight column for flags
  logical, intent(in) :: keep              ! Keep deviant data ?
  logical, intent(in) :: dotrim            ! Trim the flagged data ?
  logical, intent(out) :: error            ! Logical error flag
  character(len=*), intent(inout) :: namef ! Output file name
  !
  character(len=*), parameter :: rname='UV_CASA'
  type (gildas) :: hin, hou, htrim
  real(kind=4), allocatable, target :: din(:,:), dou(:,:)
  !
  character(len=message_length) :: mess
  character(len=16) :: atype
  !
  integer :: invisi, ouvisi
  integer(kind=index_length) :: mvis
  integer :: ier, ib
  integer, parameter :: mranges=50
  integer :: nc(mranges), numchan, nbad
  integer i, istoke, ivisi, is, ns, iv, jv
  integer astoke(4)
  integer natom, nlead, nchan, kin, kou, next
  logical extract
  real :: re, im, we
  integer intrail, ontrail, iend, oend
  real :: da, db
  integer :: ip
  integer :: kv, iloop, nblock, ishow, kvisi, mblock, multi
  !
  error = .false.
  extract = .false.
  !
  ! Scan the requested polarization code
  if (mystoke.eq.'NONE') then
    istoke = code_stokes_none
  elseif (mystoke.eq.'ALL') then
    istoke = code_stokes_all
  else
    call gdf_stokes_code(mystoke,istoke,error)
    if (error) then
      call map_message(seve%e,rname,'Invalid Stokes '//mystoke)
      return
    endif
  endif
  !
  ! Read Header of a sorted, UV table
  call gildas_null(hin, type= 'UVT')
  call sic_parsef (nami,hin%file,' ','.uvt')
  call gdf_read_header (hin,error)
  if (error) return
  !
  ! Read range of channels
  atype = ctype
  call get_ranges(rname,channels,atype,mranges,numchan,nc,hin,error)
  if (error) return
  !
  call gildas_null(hou, type='UVT')
  call gdf_copy_header (hin, hou, error)
  !
  hou%gil%nstokes = 1
  hou%gil%order = 0
  !
  call gildas_null(htrim, type='UVT')
  call sic_parsef (namo,namef,'  ','.uvt')
  !
  if (dotrim) then
    htrim%file = namef
!    call sic_parsef (namo,htrim%file,'  ','.uvt')
  else
    hou%file = namef
!    call sic_parsef (namo,hou%file,'  ','.uvt')
  endif
  !
  nblock = iblock(1)
  ishow = iblock(2)
  if (nblock.eq.0) then
    call gdf_nitems('SPACE_GILDAS',nblock,hin%gil%dim(1))
    nblock = min(nblock,hin%gil%dim(2))
    print *,'Nblock ',nblock,' > ',ishow
  endif
  !
  ! Check if it has more than 1 polarization
  if (hin%gil%nstokes.eq.1) then
    call map_message(seve%i,rname,'Already only 1 polar per visibility ')
    ! Here need to check if a Polar column is present, through the
    ! extra columns (code_uvt_stok)
    !
    is = hin%gil%column_pointer(code_uvt_stok)
    if (is.eq.0) then
      call map_message(seve%i,rname,'Already only 1 polar in total')
      ns = 1
      astoke(ns) = hin%gil%order
      if (istoke.eq.astoke(1) .or. istoke.eq.0) then
        !
        ! OK copy the whole stuff...
        allocate (din(hin%gil%dim(1),nblock),stat=ier)
        if (ier.ne.0) then
          call map_message(seve%e,rname,'Memory allocation error ')
          error = .true.
          return
        endif
        !
        hin%blc = 0
        hin%trc = 0
        hou%blc = 0
        hou%trc = 0
        if (dotrim) then
          call gdf_copy_header(hou,htrim,error)
          call sic_parsef (namo,htrim%file,'  ','.uvt')
          htrim%trc = 0
          htrim%blc(2) = 1  ! Get started
          !
          ! create the image with smallest possible size
          htrim%gil%dim(2) = nblock
          htrim%gil%nvisi = 0
          call gdf_create_image(htrim,error)
        else
          call gdf_create_image(hou,error)
        endif
        if (error) return
        !
        call map_message(seve%i,rname,'Replicating UV table ')
        do iloop = 1,hin%gil%dim(2),nblock
          hin%blc(2) = iloop
          hin%trc(2) = min(iloop+nblock-1,hin%gil%dim(2))
          hou%blc(2) = iloop
          hou%trc(2) = hin%trc(2)
          call gdf_read_data (hin,din,error)
          if (error) return
          !
          hou%r2d => din
          kvisi = hin%trc(2)-hin%blc(2)+1
          ib = iloop
          call uv_noise(hou,kvisi,ib,numchan,nc,threshold,ishow,keep,nbad,error)
          if (error) return
          !
          if (dotrim) then
            invisi = hou%trc(2)-hou%blc(2)+1
            call my_uvtrim (hou, invisi, htrim, ouvisi, wcol, error)
            if (error) return
            if (ouvisi.gt.0) then
              htrim%trc(2) = htrim%blc(2)+ouvisi-1
              mvis = htrim%trc(2)
              !
              if (mvis.gt.htrim%gil%dim(2)) then
                !!print *,'Replicating -- Extending from ',htrim%gil%dim(2), ' to  ',mvis
                call gdf_close_image (htrim,error)
                call gdf_extend_image (htrim,mvis,error)
              endif
              call gdf_write_data (htrim,hou%r2d,error)
              if (error) return
              htrim%blc(2) = htrim%trc(2)+1
            endif
          else
            call gdf_write_data (hou, din, error)
          endif
          if (error) return
        enddo
        !
        ! Finalize the image
        if (dotrim) then
          htrim%gil%nvisi = htrim%trc(2)
          htrim%gil%dim(2) = htrim%trc(2)
          call gdf_update_header(htrim,error)
          if (error) return
          call gdf_close_image(htrim,error)
          if (error) return
        else
          call gdf_close_image(hou,error)
        endif
      else
        call map_message(seve%i,rname,'Polar '//mystoke// &
        ' does not match '//gdf_stokes_name(astoke(1)) )
        error = .true.
      endif
      !
      return
    else
      call map_message(seve%i,rname,'Perhaps more than 1 polar, scanning...')
      allocate (din(hin%gil%dim(1),nblock),dou(hou%gil%dim(1),nblock),stat=ier)
      if (ier.ne.0) then
        call map_message(seve%i,rname,'Memory allocation error ')
        error = .true.
        return
      endif
      !
      hin%blc = 0
      hin%trc = 0
      hou%blc = 0
      hou%trc = 0
      hou%blc(2) = 1
      jv = 0
      if (dotrim) then
        call gdf_copy_header(hou,htrim,error)
        call sic_parsef (namo,htrim%file,'  ','.uvt')
        htrim%trc = 0
        htrim%blc(2) = 1  ! Get started
        !
        ! create the image with smallest possible size
        htrim%gil%dim(2) = nblock
        htrim%gil%nvisi = 0
        call gdf_create_image(htrim,error)
      else
        call gdf_create_image(hou,error)
      endif
      if (error) return
      !
      ns = 0
      do iloop = 1,hin%gil%dim(2),nblock
        hin%blc(2) = iloop
        hin%trc(2) = min(iloop+nblock-1,hin%gil%dim(2))
        kv = hin%trc(2)-hin%blc(2)+1
        call gdf_read_data (hin,din,error)
        if (error) return
        !
        jv = 0
        do iv = 1,kv
          if (din(is,iv).ne.istoke) cycle
          jv = jv+1
          dou(:,jv) = din(:,iv)
        enddo
        !
        if (jv.gt.0) then
          hou%trc(2) = hou%blc(2)+jv-1
          !
          hou%r2d => dou
          kvisi = hou%trc(2)-hou%blc(2)+1
          ib = hou%blc(2)
          call uv_noise(hou,kvisi,ib,numchan,nc,threshold,ishow,keep,nbad,error)
          if (error) return
          !
          if (dotrim) then
            invisi = hou%trc(2)-hou%blc(2)+1
            call my_uvtrim (hou, invisi, htrim, ouvisi, wcol, error)
            if (error) return
            if (ouvisi.gt.0) then
              htrim%trc(2) = htrim%blc(2)+ouvisi-1
              mvis = htrim%trc(2)
              !
              if (mvis.gt.htrim%gil%dim(2)) then
                call gdf_close_image (htrim,error)
                call gdf_extend_image (htrim,mvis,error)
              endif
              call gdf_write_data (htrim,hou%r2d,error)
              if (error) return
              htrim%blc(2) = htrim%trc(2)+1
            endif
          else
            call gdf_write_data(hou,dou,error)
          endif
          hou%blc(2) = hou%trc(2)+1
        endif
      enddo
      !
      ! Finalize the image
      if (dotrim) then
        htrim%gil%nvisi = htrim%trc(2)
        htrim%gil%dim(2) = htrim%trc(2)
        call gdf_update_header(htrim,error)
        if (error) return
        call gdf_close_image(htrim,error)
        if (error) return
      else
        hou%gil%nvisi = hou%trc(2)
        hou%gil%dim(2) = hou%trc(2)
        call gdf_update_header(hou,error)
        call gdf_close_image(hou,error)
      endif
    endif
    !
  else
    !
    ! This means each Visi has Nstokes x Nchan or Nchan x Nstokes elements
    !
    if (hin%gil%order.eq.code_chan_stok) then
      ! Nchan X Nstokes channels
      !
      call map_message(seve%i,rname,'Nchan channels X Nstokes stokes')
    else if (hin%gil%order.eq.code_stok_chan) then
      call map_message(seve%i,rname,'Nstokes stokes X  Nchan channels')
      ! Nstokes x Nchan channels
    else
      call map_message(seve%e,rname,'Inconsistent UV table state for polarization')
      error = .true.
      return
    endif
    ns = hin%gil%nstokes
    mess = 'Stokes: '
    next = 10
    do i=1,ns
      mess(next:) = gdf_stokes_name(hin%gil%stokes(i))
      next = len_trim(mess)+3
    enddo
    call map_message(seve%i,rname,mess)
    !
    !!print *,'DIN ',(din(1:10,i),i=1,10)
    !
    astoke(1:ns) = hin%gil%stokes(1:ns)
    natom = hin%gil%natom
    nchan = hin%gil%nchan
    nlead = hin%gil%nlead
    intrail = hin%gil%ntrail
    ontrail = intrail
    iend = hin%gil%dim(1)
    !
    ! Shift the trailing columns
    do i=1,code_uvt_last
      if (hou%gil%column_pointer(i).gt.hin%gil%fcol) then
        hou%gil%column_pointer(i) =  hou%gil%column_pointer(i) - &
          & (ns-1) * hin%gil%natom * hin%gil%nchan
      endif
    enddo
    !
    ! The useful cases are
    if (ns.eq.2) then
      if (istoke.eq.code_stokes_i .or. istoke.eq.code_stokes_none &
        .or. istoke.eq.code_stokes_all) then
        ! Input: 2 Stokes, HH+VV or RR+LL, Output: I, NONE or ALL
        ! keep everything, make proper weighting
        if ( (astoke(1).eq.code_stokes_hh.and.astoke(2).eq.code_stokes_vv) &
        .or. (astoke(2).eq.code_stokes_hh.and.astoke(1).eq.code_stokes_vv) &
        .or. (astoke(1).eq.code_stokes_ll.and.astoke(2).eq.code_stokes_rr) &
        .or. (astoke(2).eq.code_stokes_ll.and.astoke(1).eq.code_stokes_rr) &
        .or. (astoke(1).eq.code_stokes_xx.and.astoke(2).eq.code_stokes_yy) &
        .or. (astoke(2).eq.code_stokes_xx.and.astoke(1).eq.code_stokes_yy) &
        ) then
          if (istoke.eq.code_stokes_all) then
            hou%gil%nvisi = 2*hin%gil%nvisi
            multi = 2
            ontrail = ontrail+1
          else
            multi = 1
            hou%gil%nvisi = hin%gil%nvisi
          endif
          call map_message(seve%i,rname,'Deriving '//mystoke// &
          ' from '//gdf_stokes_name(astoke(1))//' and '//gdf_stokes_name(astoke(2)))
        else
          call map_message(seve%e,rname,'Cannot derive '//mystoke// &
          ' from '//gdf_stokes_name(astoke(1))//' and '//gdf_stokes_name(astoke(2)))
          error = .true.
          return
        endif
        !
        hou%gil%dim(2) = hou%gil%nvisi
        hou%gil%dim(1) = hin%gil%dim(1) - hin%gil%nchan*hin%gil%natom &
                       + ontrail-intrail
        oend = hou%gil%dim(1)+intrail-ontrail
        !!print *,'IN ',hin%gil%natom,  hin%gil%nchan,  hin%gil%nvisi, hin%gil%dim(1)
        !!print *,'OUT ',hou%gil%natom,  hou%gil%nchan,  hou%gil%nvisi, hou%gil%dim(1)
        !!print *,'OEND ',oend, intrail, ontrail
        if (istoke.eq.code_stokes_all) then
          hou%gil%column_pointer(code_uvt_stok) = hou%gil%dim(1)
        endif
        !
        ! Here, we make the loop
        ! We make no assumption about the Stokes ordering, so allocate
        ! a block which may handle all input visibilities
        mblock = multi*nblock
        allocate (dou(hou%gil%dim(1),mblock),din(hin%gil%dim(1),nblock),stat=ier)
        if (ier.ne.0) then
          call map_message(seve%e,rname,'Memory allocation error')
          error = .true.
          return
        endif
        !
        hin%blc = 0
        hin%trc = 0
        hou%blc = 0
        hou%trc = 0
        if (dotrim) then
          call gdf_copy_header(hou,htrim,error)
          call sic_parsef (namo,htrim%file,'  ','.uvt')
          !
          htrim%trc = 0
          htrim%blc(2) = 1  ! Get started
          !
          ! create the image with smallest possible size
          htrim%gil%dim(2) = nblock
          htrim%gil%nvisi = 0
          call gdf_create_image(htrim,error)
        else
          call gdf_create_image(hou,error)
        endif
        if (error) return
        !
        hou%blc(2) = 1
        do iloop = 1,hin%gil%dim(2),nblock
          write(mess,*) iloop,' / ',hin%gil%dim(2),nblock
          call map_message(seve%i,rname,mess)
          hin%blc(2) = iloop
          hin%trc(2) = min(iloop+nblock-1,hin%gil%dim(2))
          call gdf_read_data(hin,din,error)
          if (error) return
          kvisi = hin%trc(2)-hin%blc(2)+1
          !
          if (hin%gil%order.eq.code_chan_stok) then
            ! Nchan X Nstokes channels  (ALL)
            !
            if (istoke.eq.code_stokes_all) then
              !
              jv = 0
              do iv = 1,kvisi
                jv = jv+1
                dou(1:nlead,jv) = din(1:nlead,iv)
                dou(nlead+1:nlead+natom*nchan,jv) = din(nlead+1:nlead+natom*nchan,iv)
                if (ontrail.ne.intrail) dou(oend+1,jv) = astoke(1)
                if (intrail.gt.0) dou(1+nlead+natom*nchan:oend,jv) = &
                     din(nlead+1+2*natom*nchan:iend,iv)
                !
                jv = jv+1
                dou(1:nlead,jv) = din(1:nlead,iv)
                dou(nlead+1:nlead+natom*nchan,jv) = din(nlead+1+natom*nchan:nlead+2*natom*nchan,iv)
                if (ontrail.ne.intrail) dou(oend+1,jv) = astoke(2)
                if (intrail.gt.0) dou(1+nlead+natom*nchan:oend,jv) = &
                     din(nlead+1+2*natom*nchan:iend,iv)
              enddo
            else
              call map_message(seve%e,rname, &
              & 'Nchan X Nstokes channel order only supported for code ALL')
              error = .true.
              return
            endif
          else
            !
            ! Nstokes x Nchan channels
            jv = 0
            do iv = 1,kvisi
              jv = jv+1
              dou(1:nlead,jv) = din(1:nlead,iv)
              kin = nlead
              kou = nlead
              !
              if (istoke.eq.code_stokes_none) then  ! Unpolarized case
                ! Stokes NONE
                do i=1,nchan
                  if (natom.eq.3) then ! Real, Image, Weight
                    if (din(kin+3,iv).gt.0) then
                      re = din(kin+1,iv) * din(kin+3,iv)
                      im = din(kin+2,iv) * din(kin+3,iv)
                      we = din(kin+3,iv)
                    else
                      re = 0
                      im = 0
                      we = 0
                    endif
                    kin = kin+natom
                    if (din(kin+3,iv).gt.0) then
                      re = re + din(kin+1,iv) * din(kin+3,iv)
                      im = im + din(kin+2,iv) * din(kin+3,iv)
                      we = we + din(kin+3,iv)
                    endif
                    if (we.ne.0) then
                      dou(kou+1:kou+1,jv) = re/we
                      dou(kou+2:kou+2,jv) = im/we
                      dou(kou+3:kou+3,jv) = we
                    else
                      dou(kou+1:kou+3,jv) = 0
                    endif
                    !
                  else if (natom.eq.2) then
                    ! Only Real Weight
                    if (din(kin+2,iv).gt.0) then
                      re = din(kin+1,iv) * din(kin+2,iv)
                      we = din(kin+2,iv)
                    else
                      re = 0
                      we = 0
                    endif
                    kin = kin+natom
                    if (din(kin+2,iv).gt.0) then
                      re = re + din(kin+1,iv) * din(kin+2,iv)
                      we = we + din(kin+2,iv)
                    endif
                    if (we.ne.0) then
                      dou(kou+1:kou+1,jv) = re/we
                      dou(kou+2:kou+2,jv) = we
                    else
                      dou(kou+1:kou+2,jv) = 0
                    endif
                  endif
                  kou = kou + natom
                  kin = kin + natom
                enddo
                !
              else if (istoke.eq.code_stokes_i) then
                ! Stokes I
                ! Stokes I is strictly Stokes I, i.e.   HH+VV or LL+RR, not
                ! an arbitrary weighted combination.
                ! For unpolarized signals, to maximize sensitivity, use Stokes NONE
                do i=1,nchan
                  if (din(kin+natom,iv).gt.0)  then
                    dou(kou+1:kou+natom,jv) = din(kin+1:kin+natom-1,iv)  ! Visib
                    da = 1.0/sqrt(din(kin+natom,iv)) ! Noise
                    ip = 1
                  else
                    da = 0.0
                    ip = 0
                  endif
                  kin = kin+natom
                  if (din(kin+natom,iv).gt.0)  then
                    dou(kou+1:kou+natom-1,jv) = dou(kou+1:kou+natom-1,jv) + &
                      din(kin+1:kin+natom-1,iv)
                    db = 1.0/sqrt(din(kin+natom,iv))  ! Noise
                    ip = ip+1
                  else
                    db = 0.0
                  endif
                  if (ip.eq.2) then
                    dou(kou+1:kou+natom-1,jv) = dou(kou+1:kou+natom-1,jv) / ip
                    dou(kou+natom,jv) = 4.0/(da+db)**2 ! Weight = 1/noise^2
                  else
                    dou(kou+natom,jv) = 0.0
                  endif
                  kou = kou + natom
                  kin = kin + natom
                enddo
                !
              else
                ! Stokes ALL
                do i=1,nchan
                  dou(kou+1:kou+natom,jv) = din(kin+1:kin+natom,iv)
                  kou = kou + natom
                  kin = kin + 2*natom
                enddo
                if (ontrail.ne.intrail) dou(oend+1,jv) = astoke(1)
                if (intrail.gt.0) dou(1+nlead+natom*nchan:oend,jv) = &
                     din(nlead+1+2*natom*nchan:iend,iv)
                !
                jv = jv+1
                dou(1:nlead,jv) = din(1:nlead,iv)
                kin = nlead+natom
                kou = nlead
                do i=1,nchan
                  dou(kou+1:kou+natom,jv) = din(kin+1:kin+natom,iv)
                  kou = kou + natom
                  kin = kin + 2*natom
                enddo
                if (ontrail.ne.intrail) dou(oend+1,jv) = astoke(2)
                !
              endif
              !
              ! Fill trailing columns
              if (intrail.gt.0) dou(1+nlead+natom*nchan:oend,jv) = &
                   din(nlead+1+2*natom*nchan:iend,iv)
              !
            enddo
          endif
          if (jv.ne.multi*kvisi) then
            write(mess,*) 'Expecting up to ',multi*kvisi, &
            & ' visibilities, found ',jv
            call map_message(seve%w,rname,mess)
          endif
          hou%trc(2) = hou%blc(2)+jv-1
          !
          hou%r2d => dou
          kvisi = hou%trc(2)-hou%blc(2)+1
          ib = hou%blc(2)
          call uv_noise(hou,kvisi,ib,numchan,nc,threshold,ishow,keep,nbad,error)
          if (error) return
          !
          if (dotrim) then
            invisi = hou%trc(2)-hou%blc(2)+1
            ouvisi = invisi
            call my_uvtrim (hou, invisi, htrim, ouvisi, wcol, error)
            print *,'INVISI ',invisi,' OUVISI ',ouvisi,error
            if (error) return
            if (ouvisi.gt.0) then
              htrim%trc(2) = htrim%blc(2)+ouvisi-1
              mvis = htrim%trc(2)
              !
              if (mvis.gt.htrim%gil%dim(2)) then
                call gdf_close_image (htrim,error)
                call gdf_extend_image (htrim,mvis,error)
              endif
              call gdf_write_data (htrim,dou,error)
              if (error) return
              htrim%blc(2) = htrim%trc(2)+1
            endif
          else
            call gdf_write_data(hou,dou,error)
          endif
          hou%blc(2) = hou%trc(2)+1
          !
        enddo ! By block
      else
        ! Input: 2 Stokes, HH+VV or RR+LL, Output: one among these.
        ! keep only that one...
        if (istoke.eq.astoke(1)) then
          ivisi = 1
        else if (istoke.eq.astoke(2)) then
          ivisi = 2
        else
          call map_message(seve%e,rname,'Cannot extract '//mystoke// &
          ' from '//gdf_stokes_name(astoke(1))//' and '//gdf_stokes_name(astoke(2)))
          error = .true.
          return
        endif
        !
        hou%gil%dim(1) = hin%gil%dim(1)-hin%gil%nchan*hin%gil%natom
        extract = .true.
      endif
      !
    else if (ns.eq.4) then
      !
      ! 4 Stokes, extract one of them
      ivisi = 0
      do i=1,ns
        if (istoke.eq.astoke(i)) then
          ivisi = i
          exit
        endif
      enddo
      if (ivisi.eq.0) then
        if (istoke.ge.1 .and. istoke.le.4) then
          ! 4 Stokes, wanted any of I,Q,U,V
          ! apply matrix conversion to compute the desired stuff
          call map_message(seve%e,rname,'Cannot yet handle 4 Stokes ')
          error = .true.
          return
        else
          call map_message(seve%e,rname,'Cannot extract '//mystoke// &
          ' from '//gdf_stokes_name(astoke(1))//gdf_stokes_name(astoke(2))//  &
          gdf_stokes_name(astoke(3))//' and '//gdf_stokes_name(astoke(4)) )
          error = .true.
          return
        endif
      else
        call map_message(seve%i,rname,'Extracting '//mystoke// &
          ' from '//gdf_stokes_name(astoke(1))//gdf_stokes_name(astoke(2))//  &
          gdf_stokes_name(astoke(3))//' and '//gdf_stokes_name(astoke(4)))
        extract = .true.
      endif
    endif
    !
    ! The extraction code makes no assumption about the Stokes order.
    !
    if (extract) then
      !
      allocate (dou(hou%gil%dim(1),nblock),din(hin%gil%dim(1),nblock),stat=ier)
      if (ier.ne.0) then
        call map_message(seve%e,rname,'Memory allocation error ')
        error = .true.
        return
      endif
      hin%blc = 0
      hou%trc = 0
      !
      hou%blc(2) = 1
      do iloop = 1,hin%gil%dim(2),nblock
        hin%blc(2) = iloop
        hin%trc(2) = min(iloop+nblock-1,hin%gil%dim(2))
        kv = hin%trc(2)-hin%blc(2)+1
        call gdf_read_data(hin,din,error)
        if (error) return
        !
        if (hin%gil%order.eq.code_chan_stok) then
          ! Nchan X Nstokes channels
          do iv = 1,hin%gil%nvisi
            dou(1:nlead,iv) = din(1:nlead,iv)
            dou(nlead+1:nlead+natom*nchan,iv) = &
            & din(nlead+1+(ivisi-1)*natom*nchan:nlead+ivisi*natom*nchan,iv)
            if (intrail.gt.0) dou(1+nlead+natom*nchan:,iv) = din(nlead+1+ns*natom*nchan:,iv)
          enddo
        else
          ! Nstokes x Nchan channels
          do iv = 1,hin%gil%nvisi
            dou(1:nlead,iv) = din(1:nlead,iv)
            kin = nlead+(ivisi-1)*natom
            kou = nlead
            do i=1,nchan
              dou(kou+1:kou+natom,iv) = din(kin+1:kin+natom,iv)
              kou = kou + natom
              kin = kin + ns*natom
            enddo
            if (intrail.gt.0) dou(1+nlead+natom*nchan:,iv) = din(nlead+1+ns*natom*nchan:,iv)
            !
          enddo
        endif
        hou%trc(2) = hou%trc(2)+kv-1
        !
        hou%r2d => dou
        kvisi = hou%trc(2)-hou%blc(2)+1
        ib = hou%blc(2)
        call uv_noise(hou,kvisi,ib,numchan,nc,threshold,ishow,keep,nbad,error)
        if (error) return
        !
        if (dotrim) then
          invisi = hou%trc(2)-hou%blc(2)+1
          call my_uvtrim (hou, invisi, htrim, ouvisi, wcol, error)
          if (error) return
          if (ouvisi.gt.0) then
            htrim%trc(2) = htrim%blc(2)+ouvisi-1
            mvis = htrim%trc(2)
            !
            if (mvis.gt.htrim%gil%dim(2)) then
              call gdf_close_image (htrim,error)
              call gdf_extend_image (htrim,mvis,error)
            endif
            call gdf_write_data (htrim,hou%r2d,error)
            if (error) return
            htrim%blc(2) = htrim%trc(2)+1
          endif
        else
          call gdf_write_data(hou,dou,error)
        endif
        hou%blc(2) = hou%trc(2)+1
        !
      enddo
    endif
    !
    ! Finalize the image
    if (dotrim) then
      htrim%gil%nvisi = htrim%trc(2)
      htrim%gil%dim(2) = htrim%trc(2)
      call gdf_update_header(htrim,error)
      if (error) return
      call gdf_close_image(htrim,error)
      if (error) return
    else
      call gdf_close_image(hou,error)
    endif
  endif
  !
end subroutine sub_uv_casa
!
subroutine uv_noise (out,nvisi,ivisi,numchan,nc,factor,ishow,keep,nbad,error)
  use image_def
  !---------------------------------------------------------------------
  ! Make sure the input range is reasonable contiguous in Time
  ! If not, split in different time ranges
  !---------------------------------------------------------------------
  type (gildas), intent(inout) :: out    ! Visibilities
  integer, intent(in) :: numchan         ! Number of windows
  integer, intent(in) :: nvisi           ! Number of visibilities
  integer, intent(in) :: ivisi           ! First visibility
  integer, intent(in) :: nc(2*numchan)   ! Channel windows
  integer, intent(in) :: ishow           ! Show changes
  real, intent(in) :: factor             ! Maximum Deviation factor
  logical, intent(in) :: keep            ! Keep anomalous re-scale ?
  integer, intent(inout) :: nbad         ! Number of potentially "bad" visibilities
  logical, intent(out) :: error          ! Error flag
  ! Local
  integer :: j
  real(8) :: ftime,ltime,ctime
  real(8), parameter :: mtime=3600d0
  !
  ftime = out%r2d(4,1)*86400.d0 + out%r2d(5,1)
  ltime = out%r2d(4,nvisi)*86400.d0 + out%r2d(5,nvisi)
  !
  if ((ltime-ftime).le.mtime) then
    call uv_sub_noise (out,out%r2d,nvisi,ivisi,numchan,nc,factor,ishow,keep,nbad,error)
  else
    print *,'Time discontinuity ',nint((ltime-ftime)/mtime),' hours'
    do j=1,nvisi
      !
      ctime = out%r2d(4,j)*86400.d0 + out%r2d(5,j)
      if ((ctime-ftime).gt.mtime) then
        if ((ltime-ctime).gt.mtime) then
          call gagout('F-UV_CASA, Double time discontinuity, re-try with a BLOCK$[1] value')
          print *,'At visi ',j
          print *,'L-C-F-TIME (sec) ',ltime,ctime,ftime
          error = .true.
          return
        endif
        call uv_sub_noise (out,out%r2d,j-1,ivisi,numchan,nc,factor,ishow,keep,nbad,error)
        !!
        call uv_sub_noise (out,out%r2d(:,j:),nvisi-j+1,ivisi+j-1,numchan,nc,factor,ishow,keep,nbad,error)
        return
      endif
    enddo
  endif
end subroutine uv_noise
!
subroutine uv_sub_noise (out,dout,nvisi,ivisi,numchan,nc,factor,ishow,keep,nbad,error)
  use image_def
  type (gildas), intent(inout) :: out    ! Visibility Header
  real, intent(inout) :: dout(:,:)       ! Visibilities
  integer, intent(in) :: numchan         ! Number of windows
  integer, intent(in) :: nvisi           ! Number of visibilities
  integer, intent(in) :: ivisi           ! First visibility
  integer, intent(in) :: nc(2*numchan)   ! Channel windows
  integer, intent(in) :: ishow           ! Show changes
  real, intent(in) :: factor             ! Maximum Deviation factor
  logical, intent(in) :: keep            ! Keep anomalous re-scale ?
  integer, intent(inout) :: nbad         ! Number of potentially "bad" visibilities
  logical, intent(out) :: error          ! Error flag
  ! Local
  integer :: i,j,k,l,fc,nd,nk,ier
  real :: rms, rmsr, rmsi, wei, reel, imag, arms, awei, scale
  real, allocatable :: original(:), ratio(:)
  integer(kind=size_length) :: ndata
  !
  if (ishow.gt.0) then
    write(6,*) 'Visibility      New            Original             # Channels '
  endif
  nk = (nc(1)+nc(2))/2
  fc = out%gil%fcol-1
  error = .false.
  k = ivisi-1
  allocate(original(nvisi),ratio(nvisi),stat=ier)
  if (ier.ne.0) then
    print *,'Memory allocation error ',ier
    error = .true.
    return
  endif
  !
  do j=1,nvisi
    !
    ! Compute rms on "Good" channels
    rmsr = 0.0
    rmsi = 0.0
    nd = 0
    do i=1,out%gil%nchan
      do l=1,numchan
        if ( (i.lt.nc(2*l-1) .or. i.gt.nc(2*l)) .and.   &
       &        dout(fc+3*i,j).gt.0)  then
          reel = dout(fc+3*i-2,j)
          imag = dout(fc+3*i-1,j)
          rmsr = rmsr+reel**2
          rmsi = rmsi+imag**2
          nd = nd+1
        endif
      enddo
    enddo
    !
    if (nd.eq.0) cycle
    !
    ! This is the weight on the REAL or IMAGinary part
    k = k+1
    rms = min(rmsr,rmsi)
    original(j) = dout(fc+3*nk,j)
    if (ishow.gt.0) then
      if (mod(k,ishow).eq.1) then
        arms = sqrt(rms/float(nd-1))
        if (original(j).gt.0) then
          awei = 1.0/sqrt(original(j)*1e6)
          write(6,*) j+ivisi-1,arms,awei,nd ! ,out%gil%nvisi
        else
          awei = 1.0/sqrt(-original(j)*1e6)
          write(6,*) j+ivisi-1,arms,awei,' Flagged ' ! ,out%gil%nvisi
        endif
      endif
    endif
    ratio(j) = float(nd-1)/rms*1e-6
    if (original(j).gt.0) then
      ratio(j) = ratio(j)/original(j)
    else
      ratio(j) = 0.0
    endif
    !
  enddo
  !
  ndata = nvisi
  call gr4_median(ratio,ndata,0.0,0.0,scale,error)
  write(6,*) 'Median rescale is ',scale,ndata,error
  if (error) return
  !
  do j=1,nvisi
    if (ratio(j).gt.factor*scale) then
      if (keep) then
        wei = scale*original(j)
      else
        wei = -scale*original(j)
      endif
!!      write(6,'(i16,f10.1,f4.0,f4.0,1pg10.3,1pg10.3)') j+ivisi-1, &
!!      &   sqrt(dout(1,j)**2+dout(2,j)**2),dout(6,j),dout(7,j),original(j),ratio(j)
      nbad = nbad+1
    else
      wei = scale*original(j)
    endif
    !
    do i=1,out%gil%nchan
      if (dout(fc+3*i,j).gt.0) then
        dout(fc+3*i,j) = wei
      else if (dout(fc+3*i,j).lt.0) then
        dout(fc+3*i,j) = -wei
      endif
    enddo
  enddo
end subroutine uv_sub_noise
!
subroutine my_uvtrim(in,invis,out,outvis,wcol,error)
  use gildas_def
  use image_def
  use gkernel_interfaces
  use gbl_message
  !----------------------------------------------------------------------
  ! GILDAS
  !       Trim an input UV table: get rid off all flagged visibilities
  !----------------------------------------------------------------------
  type (gildas), intent(inout) :: in, out
  integer, intent(in) :: invis
  integer, intent(out) :: outvis
  integer, intent(in) :: wcol
  logical, intent(out) :: error
  !
  ! Local variables:
  character(len=*), parameter :: rname='UVTRIM'
  integer(kind=index_length), allocatable :: ivis(:)
  integer :: ier, np, nclow, ncup, ic
  integer :: iv, im, mv
  !
  np = in%gil%dim(1)
  if (wcol.lt.0) then
    nclow = 1
    ncup = in%gil%nchan
  else if (wcol.eq.0) then
    nclow = (in%gil%nchan+2)/3
    ncup = nclow
  else
    nclow = max(1,wcol)
    ncup = min(wcol,in%gil%nchan)
    nclow = ncup
  endif
  allocate(ivis(invis),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Memory allocation error')
    error = .true.
    return
  endif
  !
  ! First passs to identifiy all valid visibilities
  mv = 0
  do iv = 1,invis
    im = 1
    do ic=nclow,ncup
      if (in%r2d(in%gil%fcol-1+3*ic,iv).le.0) then
         im = 0
         exit
      endif
    enddo
    if (im.ne.0) then
      mv = mv+1
      ivis(mv) = iv
    endif
  enddo
  !
  ! Second pass to keep the good visibilities only
  ! Memory saved by working in place, as the visibilities are ordered.
  do iv = 1,mv
    if (ivis(iv).gt.iv) in%r2d(:,iv) = in%r2d(:,ivis(iv))
  enddo
  outvis = mv
  deallocate(ivis)
  !
end subroutine my_uvtrim
!
!
subroutine get_ranges(pname,uvchannel,ctype,mranges,numchan,nc,y,error)
  use image_def
  use gbl_message
  use gkernel_interfaces
  !
  character(len=*), intent(in) :: pname
  character(len=*), intent(in) :: uvchannel
  character(len=*), intent(inout) :: ctype
  integer, intent(in) :: mranges
  integer, intent(out) :: numchan
  integer, intent(out) :: nc(mranges)
  type(gildas), intent(in) :: y
  logical, intent(out) :: error
  !
  integer(4) :: ier, i, ntmp
  real(8), parameter :: rnone=-1D9 ! Impossible velocity, frequency or channel number
  real(kind=8) :: drange(mranges)
  integer(kind=4), parameter :: mtype=3
  integer :: itype
  character(len=12) :: types(mtype),mytype
  data types /'CHANNEL','VELOCITY','FREQUENCY'/
  !
  call sic_upper(ctype)
  error = .false.
  call sic_ambigs(pname,ctype,mytype,itype,types,mtype,error)
  if (error)  return
  !
  !
  drange = rnone   ! Impossible velocity, frequency or channel number
  nc = 0                         ! This initialization is essential...
  read (uvchannel,*,iostat=ier) drange
  if (ier.gt.0) then
    call putios('E-'//pname//',  ',ier)
    write(6,*) uvchannel
    error = .true.
    return
  endif
  !
  numchan = 0
  i = 2
  do while (i.lt.mranges)
    if (drange(i).ne.rnone.and.drange(i-1).ne.rnone) numchan = numchan+2
    i = i+2
  enddo
  !
  ! Convert to channels
  !
  if (numchan.eq.0) then
    numchan = 2
    nc(1) = 1
    nc(2) = y%gil%nchan
  else if (mytype.eq.'CHANNEL') then
    nc(1:numchan) = nint(drange(1:numchan))
    do i=1,numchan
      if (nc(i).lt.0) nc(i) = y%gil%nchan+nc(i)
    enddo
  else if (mytype.eq.'VELOCITY') then
    ! drange = nc - y%gil%ref(y%gil%faxi) ) * y%gil%vres + y%gil%voff
    nc(1:numchan) = (drange(1:numchan) - y%gil%voff) / y%gil%vres + y%gil%ref(y%gil%faxi)
  else if (mytype.eq.'FREQUENCY') then
    ! drange = nc - y%gil%ref(y%gil%faxi) ) * y%gil%vres + y%gil%voff
    nc(1:numchan) = (drange(1:numchan) - y%gil%freq) / y%gil%fres + y%gil%ref(y%gil%faxi)
  else
    call map_message(seve%e,pname,'Type of value '''//trim(mytype)//''' not supported')
    error = .true.
    return
  endif
  !
  do i=2,numchan,2
    if (nc(i).lt.nc(i-1)) then
      ntmp = nc(i)
      nc(i) = nc(i-1)
      nc(i-1) = ntmp
    endif
  enddo
end subroutine get_ranges
!
end program uv_casa
