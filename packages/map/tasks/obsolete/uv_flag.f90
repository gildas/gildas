program uv_flag
  use gildas_def
  use gkernel_interfaces
  use image_def
  !---------------------------------------------------------------------
  ! GILDAS
  !	FLAG data in input UV table
  !---------------------------------------------------------------------
  real(8), parameter :: pi=3.14159265358979323846d0
  ! Local
  type(gildas) :: x
  character(len=80) :: uvdata,name, ut_start, ut_end, date_start, baseline
  character(len=80) :: date_end, chain
  integer :: n,nx,nc(2), i, ibase, larg, ier, iant, jant
  real(8) :: ut1, ut2, tt
  real(4) :: riant, rjant, rflag
  logical :: error, flag
  !
  !------------------------------------------------------------------------
  ! Code:
  call gildas_open
  call gildas_char('UVDATA$',uvdata)
  !
  call gildas_char('DATE_START$',date_start)
  larg = len_trim(date_start)
  call gag_fromdate(date_start(1:larg),i,error)
  if (error) then
    write(6,*) 'F-UV_FLAG, error converting ',date_start(1:larg)
    goto 999
  endif
  ut1 = i
  !
  call gildas_char('UT_START$',ut_start)
  larg = len_trim(ut_start)
  call sic_decode(ut_start,tt,24,error)    ! TT in RADIANS
  if (error) goto 999
  ut1 = ut1 + tt/2d0/pi
  !
  call gildas_char('DATE_END$',date_end)
  larg = len_trim(date_end)
  call gag_fromdate(date_end(1:larg),i,error)
  if (error) then
    write(6,*) 'F-UV_FLAG, error converting ',date_end(1:larg)
    goto 999
  endif
  ut2 = i
  !
  call gildas_char('UT_END$',ut_end)
  larg = len_trim(ut_end)
  call sic_decode(ut_end,tt,24,error)  ! TT in RADIANS
  if (error) goto 999
  ut2 = ut2 + tt/2d0/pi
  !
  call gildas_char('BASELINE$',baseline)
  call sic_upper(baseline)
  if (baseline.eq."ALL") then
    ibase = 0
  else
    read(baseline,*,iostat=ier) riant,rjant
    if (ier.ne.0) goto 999
    ibase = basant(riant,rjant)
    iant = 0
    jant = 0
  endif
  !
  call gildas_logi('FLAG$',flag,1)
  if (flag) then
    rflag = -1
  else
    rflag = 1
  endif
  !
  call gildas_char('ANTENNA$',chain)
  read(chain,'(i12)',iostat=ier) iant
  if (ier.ne.0) goto 999
  !
  call gildas_inte('CHANNELS$',nc,2)
  call gildas_close
  !
  ! Input file
  n = len_trim(uvdata)
  if (n.le.0) goto 999
  call gildas_null(x, type = 'UVT')
  call gdf_read_gildas(x, uvdata, '.uvt', error)
  if (error) then
    write(6,100) 'F-UV_FLAG,  Cannot read input UV table'
    goto 999
  endif
  !
  ier = gdf_range(nc,x%gil%nchan)
  call doflag (x%r2d,x%gil%dim(1),x%gil%dim(2),ut1, ut2, ibase,   &
     &    iant, nc, rflag)
  call gdf_write_data(x, x%r2d, error)
  call gagout('S-UV_FLAG,  Successful completion')
  call sysexi(1)
  !
  98    call gagout('F-UV_FLAG, error converting UT string')
  999   call sysexi(fatale)
  100   format(1x,a)
contains
!
subroutine doflag (out,nx,nv,ut1, ut2, ibase, iant, nc, rflag)
  use gildas_def
  !---------------------------------------------------------------------
  ! Conditionally change sign of weights according to RFLAG
  !---------------------------------------------------------------------
  integer(kind=index_length) :: nx                     !
  integer(kind=index_length) :: nv                     !
  real :: out(nx,nv)                !
  real(8) :: ut1                     !
  real(8) :: ut2                     !
  integer :: ibase                  !
  integer :: iant
  integer :: nc(2)                  !
  real :: rflag                  !
  ! Local
  integer :: i,j, i1, i2, ib
  real(8) :: t
  !------------------------------------------------------------------------
  ! Code:
  !
  if (iant.ne.0) then
    do j=1,nv
      t = out(4,j)+out(5,j)/86400d0
      i1 = nint(out(6,j))
      i2 = nint(out(7,j))
      if (t.gt.ut1 .and. t.lt.ut2 .and.   &
       &      (i1.eq.iant .or. i2.eq.iant)) then
        do i = 3*nc(1), 3*nc(2), 3
          out(i+7,j) = sign(out(i+7,j),rflag)
        enddo
      endif
    enddo
  else  
    do j=1,nv
      t = out(4,j)+out(5,j)/86400d0
      ib = basant(out(6,j),out(7,j))
      if (t.gt.ut1 .and. t.lt.ut2 .and.   &
       &      (ibase.le.0 .or. ib.eq.ibase)) then
        do i = 3*nc(1), 3*nc(2), 3
          out(i+7,j) = sign(out(i+7,j),rflag)
        enddo
      endif
    enddo
  endif
end subroutine 
!
integer function basant(ri,rj)
  real, intent(in) :: ri,rj
  !
  integer k,l
  if (ri.gt.rj) then
    basant = 256*nint(rj)+nint(ri)
  else
    basant = 256*nint(ri)+nint(rj)
  endif
end function basant
!
end program uv_flag
