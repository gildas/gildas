.ec _
.ll 76
.ad b
.in 4
.ti -4
.nf
1 MAKE__MOSAIC Build a dirty mosaic from individual fields (suited only to MAPPING)

.fi
This task combines individual dirty images and beams corresponding to
various pointing centers into a "mosaic" which is suited for further
deconvolution using the MAPPING program.

Mosaics are ensemble of overlapping fields, with typically half power 
primary beam overlap, and similar (but not identical) UV coverages 
for each field. Individual images for every field should be created 
with  a common phase tracking center. The UV__SHIFT, MAP__RA
and  MAP__DEC variables in task UV__MAP must be used for this. 
The image names must derive from a generic name as follows 
(e_.g_. for a generic name  MAP)
.br
     MAP-1.LMV, MAP-2.LMV, ... MAP-N.LMV
.br
where N is the number of fields. 

For example
.nf
LET MAP__SIZE 256 512
LET MAP__CELL 0.5
LET UV__SHIFT YES
LET MAP__RA     20:39:10
LET MAP__DEC    68:01:20
!
LET WCOL 1
FOR I 1 TO 10
LET NAME CONT-'I'
GO UV__MAP
NEXT
.fi 

MAKE__MOSAIC produces a combined image (MAP.LMV), a data cube of primary beams
(MAP.LOBE), a data cube of dirty beams (MAP.BEAM), and
a weight image MAP.WEIGHT, which are required for cleaning.

Two parameters are very important in the mosaic creation:
the primary beam width (FWHM), and the truncation threshold of the
primary beam. It is recommended to use a relatively high value
for this threshold, 0.2 to 0.4, to avoid contaminating adjacent field
in case of pointing errors.

.ti -4
2 FIELDS$ 
.ti +4
TASK\INTEGER "Number of fields" FIELDS$ 

The number of fields in the mosaic.

.ti -4
2 NAME$
.ti +4
TASK\CHARACTER "Generic file name (without extension)" NAME$ 

The generic name of the mosaic. The names of the individual field
images is derived by appending the field number, e_.g_. map-1 map-2
etc.., when NAME$ = map.

.ti -4
2 BEAM$
.ti +4
TASK\REAL "Half-Power Primary Beam (in radians)" BEAM$ 

The half power primary beam of the antennas, expressed in radians.

.ti -4
2 BMIN$
.ti +4
TASK\REAL "Truncation Threshold" BMIN$

The minimum relative intensity under which the primary beam of the 
antennas should be set to zero. A value of 0.2 to 0.3 is recommended.
.ti -4
1 ENDOFHELP
