program p_line2cont
  use gkernel_interfaces
  use gildas_def
  use uvmap_def_task
  !
  ! Compute Dirty images from .TUV tables
  !
  character(len=256) line,cont 
  logical error
  type (par_uvmap) :: map
  !
  error = .false.
  map%channels(3) = 1
  call i_line2cont (line,cont,map,error)
  if (error) call sysexi(fatale)
  map%uvcode = code_tuv   ! Must be 2
  call t_line2cont (line,cont,map,error)
  if (error) call sysexi (fatale)
  !
  contains
!
subroutine t_line2cont(line,cont,map,error) 
  use image_def
  use gkernel_interfaces
  use uvmap_def_task
  use phys_const
  !
  character(len=*), intent(inout) :: line     ! Line file name
  character(len=*), intent(inout) :: cont     ! Continuum file name
  type (par_uvmap), intent(inout) :: map   ! Imaging parameters
  logical, intent(inout) :: error
  !
  type (gildas) :: hluv
  type (gildas) :: hcuv
  integer ier,nc,mc
  ! 
  ! Read Headers  
  call gildas_null(hluv, type = 'UVT')
  call sic_parsef (line,hluv%file,' ','.tuv')
  call gdf_read_header (hluv,error)
  if (error) return
  !
  ! Define channels and Center frequency
  call t_channel (hluv,map)
  !! nc = map%channels(2)-map%channels(1)+1 
  !
  ! Allocate & Read appropriate data set
  !
  allocate(hluv%r2d(hluv%gil%dim(1),hluv%gil%dim(2)),stat=ier)
  call gdf_read_data(hluv,hluv%r2d,error)
  !
  call gildas_null (hcuv, type = 'UVT')
  call gdf_copy_header(hluv,hcuv,error) 
  !
  nc = (map%channels(2)-map%channels(1)+1)
  mc = nc/map%channels(3)
  if (mc*map%channels(3).ne.nc) mc = mc+1
  hcuv%gil%dim(1) = hluv%gil%dim(1)*mc
  hcuv%gil%dim(2) = 10
  hcuv%gil%nchan = 1
  allocate(hcuv%r2d(hcuv%gil%dim(1),hcuv%gil%dim(2)),stat=ier)
  call t_continuum1(hluv,hcuv,map,error)
  call sic_parsef (cont,hcuv%file,' ','.tuv')
  call gdf_write_image(hcuv,hcuv%r2d,error)
end subroutine t_line2cont
!
subroutine i_line2cont (line,cont,map,error) 
  use gkernel_interfaces
  !
  ! UV_LINE2CONT 
  !    Define parameters
  !
  use uvmap_def_task
  character(len=*), intent(out) :: line  
  character(len=*), intent(out) :: cont
  type (par_uvmap), intent(out) :: map
  logical, intent(out) :: error
  !
  call gildas_open
  call gildas_char('UV_LINE$',line)
  call gildas_char('UV_CONT$',cont)
  call gildas_inte('CHANNELS$',map%channels,3)
  call gildas_close
end subroutine i_line2cont
!
subroutine t_continuum1(hluv,hcuv,map,error)
  !----------------------------------------------------------
  ! UV_INVERT / UV_MAP
  !    Create a continuum UV table from a Line one
  !----------------------------------------------------------
  use gildas_def
  use image_def
  use uvmap_def_task
  !
  type (gildas), intent(in) :: hluv          ! Line UV header
  type (gildas), intent(inout) :: hcuv          ! Line UV header
  type (par_uvmap), intent(inout) :: map  ! Imaging parameters
  logical, intent(out) :: error
  !
  real(8) :: freq,scale_uv
  real, allocatable :: re(:), im(:), we(:), dw(:)
  integer :: ifi,ila,nc,nv,ic,jc,ier
  !
  error = .false.
  !
  ! Define number of Visibilities and Channels...
  if (map%uvcode.eq.code_uvt) then  ! UVT order
     nv = hluv%gil%dim(2)
  else ! TUV order
     nv = hluv%gil%dim(1)
     allocate (re(nv),im(nv),we(nv),dw(nv),stat=ier)
     if (ier.ne.0) then
        error = .true.
        return
     endif
     !
     ! Fill in, channel after channel
     ifi = 1
     ila = nv
     do ic=map%channels(1),map%channels(2),map%channels(3)
        print *,'ic ',ic,ifi,ila
        freq = hluv%gil%val(2) + hluv%gil%fres * & 
             & (ic+map%channels(3)/2 - hluv%gil%ref(2))
        scale_uv = hluv%gil%val(2)/freq
        !
        hcuv%r2d(ifi:ila,1:3) = hluv%r2d(1:nv,1:3)*scale_uv
        hcuv%r2d(ifi:ila,4:7) = hluv%r2d(1:nv,4:7)
        !
        ! Compact the channels first
        if (map%channels(3).gt.1) then
           re = 0
           im = 0
           we = 0
           do jc = ic,min(ic+map%channels(3)-1,map%channels(2))
              dw(:) = max(0.,hluv%r2d(1:nv,7+3*ic))
              re(:) = re + hluv%r2d(1:nv,5+3*jc)*dw
              im(:) = im + hluv%r2d(1:nv,6+3*jc)*dw
              we(:) = we+dw
           enddo
           where (we.ne.0)
             re = re/we
             im = im/we
           end where
           hcuv%r2d(ifi:ila,8) = re
           hcuv%r2d(ifi:ila,9) = im
           hcuv%r2d(ifi:ila,10) = we
        else
           hcuv%r2d(ifi:ila,8:10) = hluv%r2d(1:nv,5+3*ic:7+3*ic)
        endif
        ifi = ila+1
        ila = ila+nv
     enddo
     map%channels = 1
     map%freq = hcuv%gil%val(2)
     hcuv%gil%ref(2) = 1
     hcuv%gil%val(2) = map%freq
     hcuv%gil%inc(2) = hluv%gil%inc(2)*nc
     deallocate (re,im,we,dw,stat=ier)
  endif
end subroutine t_continuum1
!
include 'lib/util_uvmap.f90'
!
end program p_line2cont

        
