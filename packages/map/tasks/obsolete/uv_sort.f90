program uvsort_main
  use gildas_def
  use image_def
  use gkernel_interfaces
  !----------------------------------------------------------------------
  ! GILDAS
  !       Sort an input UV table
  !       Add CC code to implement precession
  !----------------------------------------------------------------------
  character(len=80) :: uvdata,uvtri
  integer, parameter :: mtypes=7
  character(len=12) :: type
  character(len=2) :: types(mtypes)
  data types/'TB','UV','BT','TF','UF','BF','FI'/
  logical :: error
  integer :: i
  !
  ! Code:
  call gildas_open
  call gildas_char('UVDATA$',uvdata)
  call gildas_char('UVSORT$',uvtri)
  call gildas_char('TYPE$',type)
  call gildas_close
  !
  error = .true.
  do i=1,mtypes
    if (type.eq.types(i)) then
      error = .false.
      exit
    endif
  enddo
  !
  if (error) then
    call gagout ('F-UVSORT,  Invalid sort type')
  else
    call sub_uvsort(uvdata,uvtri,type,error)
  endif
  !
  if (error) call sysexi(fatale)
end program uvsort_main
!
subroutine sub_uvsort(uvdata,uvtri,type,error)
  use image_def
  use gkernel_interfaces
  use gbl_message
  !----------------------------------------------------------------------
  ! GILDAS
  !       Sort an input UV table
  !       Add CC code to implement precession
  !----------------------------------------------------------------------
  !
  character(len=*), intent(in) :: uvdata  ! Name of unsorted UV table
  character(len=*), intent(in) :: uvtri   ! Name of sorted UV table
  character(len=*), intent(in) :: type    ! Kind of sort
  logical, intent(out) :: error
  !
  ! Local variables:
  character(len=*), parameter :: rname='UVSORT'
  type (gildas) :: in, out
  character(len=12) :: xtype
  character(len=filename_length) :: name
  character(len=64) :: mess
  real xy(2),cs(2), uvmax, uvmin
  integer :: ier, np, nv, nblock
  !
  data xy/0.0,0.0/, cs/1.0,0.0/
  data xtype /'UV-DATA'/
  !C      DATA XTYPE /'UV-SORT'/          ! for precessed
  ! Code:
  !
  ! The local code only works for "reasonably" small UV Tables,
  ! say 1 GByte or so (according to SPACE_GILDAS)
  !
  ! For large ones, calls a dedicated routine which is
  ! splits the I/O in small blocks (actually one
  ! visibility at once in output).
  !
  error = .false.
  !
  ! Input file
  call gildas_null(in, type = 'UVT')
  name = trim(uvdata)
  call gdf_read_gildas(in, name, '.uvt', error, data=.false.)
  !
  ! Check image size
  call gdf_nitems('SPACE_GILDAS',nblock,in%gil%dim(1))
  if (in%gil%dim(2).gt.nblock) then
    call my_big_uvsort(uvdata, uvtri, type, error)
    return
  endif
  !
  call map_message(seve%i,rname,'Doing in-memory sort')
  call gdf_read_gildas(in, name, '.uvt', error, data=.true.)
  !
  if (error) then
    call map_message(seve%e,rname,'Cannot read UV data')
    return
  endif
  !
  ! Create output image
  call gildas_null(out, type = 'UVT')
  call gdf_copy_header(in,out,error)
  name = trim(uvtri)
  call sic_parsef(name,out%file,' ','.uvt')
  if (in%file .eq. out%file) then
    call map_message(seve%e,rname,'In place sorting not allowed')
    error = .true.
    return
  endif
  call gdf_create_image (out,error)
  if (error) return
  !
  allocate (out%r2d(out%gil%dim(1),out%gil%dim(2)), stat=ier)
  !
  select case (type)
  case ('TB')
    call uvsort_tb(in,out,error)
  case ('BT')
    call uvsort_bt(in,out,error)
  case ('UV')
    if (in%gil%nvisi.gt.huge(nv)) then
      write(mess,*) 'Cannot handle more than ',huge(nv),' visibilities'
      call map_message(seve%e,rname,mess)
      error = .true.
    else
      nv = in%gil%nvisi
      np = in%gil%dim(1)
      call uvsort_uv (np,nv,in%gil%ntrail,   &
       &      in%r2d,out%r2d,                &
       &      xy,cs,uvmax,uvmin,error)
      write (mess,'(A,F10.2,F10.2)') 'Min - Max UV-spacing: ',   &
       &      uvmin,uvmax
       call map_message(seve%i,rname,mess)
    endif
  case ('FI')
    call uvsort_fi(in,out,error)
  case default
    call map_message(seve%e,rname,'Does not yet handle '//type)
    error = .true.
  end select
  !
  if (.not.error) then
    call gdf_write_image(out,out%r2d,error)
  endif
  deallocate(in%r2d,out%r2d,stat=ier)
  !
end subroutine sub_uvsort
!
subroutine uvsort_tb (hin,hou,error)
  use gkernel_interfaces
  use image_def
  use gbl_message
  !----------------------------------------------------------------------
  ! MAP
  !     Rotate, Shift and Sort a UV table for map making
  !     Differential precession should have been applied before.
  !----------------------------------------------------------------------
  type(gildas), intent(inout) :: hin
  type(gildas), intent(inout) :: hou
  logical, intent(out) :: error
  !
  character(len=*), parameter :: rname='UVSORT_TB'
  character(len=60) :: mess
  integer :: ier
  integer :: nv
  integer :: np
  real(8), allocatable :: times(:)
  integer, allocatable :: indx(:)
  logical :: sorted
  !
  if (hin%gil%nvisi.gt.huge(nv)) then
    write(mess,*) 'Cannot handle more than ',huge(nv),' visibilities'
    call map_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  nv = hin%gil%nvisi
  np = hin%gil%dim(1)
  !
  allocate(times(nv),indx(nv), stat=ier)
  error = ier.ne.0
  if (error) return
  call loadtb (hin%r2d,np,nv,times,indx,sorted,4)
  !
  ! Sort if needed
  if (.not.sorted) then
    call gr8_trie(times,indx,nv,error)
    if (error) then
      call map_message(seve%e,rname,'Sorting error')
    else
      call sortiv (hin%r2d,hou%r2d,np,nv,indx)
    endif
  else
    call map_message(seve%w,rname,'Table is already sorted')
    hou%r2d = hin%r2d
  endif
  deallocate(times,indx)
end subroutine uvsort_tb
!
subroutine sortiv (vin,vout,np,nv,it)
  !----------------------------------------------------------------------
  ! MAP    UVSORT routines
  !     Sort an input UV table into an output one
  !----------------------------------------------------------------------
  integer, intent(in) :: np         ! Size of a visibility
  integer, intent(in) ::  nv        ! Number of visibilities
  real, intent(in) ::  vin(np,nv)   ! Input visibilities
  real, intent(out) ::  vout(np,nv) ! Output visibilities
  integer, intent(in) ::  it(nv)    ! Sorting pointer
  !
  integer iv,kv,ic
  !
  ! Simple case
  do iv=1,nv
    kv = it(iv)
    vout(:,iv) = vin(:,kv)
    if (vout(6,iv).gt.vout(7,iv)) then
      vout(6,iv) = vin(7,kv)
      vout(7,iv) = vin(6,kv)
      do ic=9,np,3
        vout(ic,iv) = -vout(ic,iv)
      enddo
    endif
  enddo
end subroutine sortiv
!
subroutine uvsort_bt (hin,hou,error)
  use gkernel_interfaces
  use image_def
  use gbl_message
  !----------------------------------------------------------------------
  ! MAP
  !     Rotate, Shift and Sort a UV table for map making
  !     Differential precession should have been applied before.
  !----------------------------------------------------------------------
  type(gildas), intent(inout) :: hin
  type(gildas), intent(inout) :: hou
  logical, intent(out) :: error
  !
  character(len=*), parameter :: rname='UVSORT_BT'
  character(len=60) :: mess
  integer :: ier
  integer :: nv
  integer :: np
  real(8), allocatable :: times(:)
  integer, allocatable :: indx(:)
  logical :: sorted
  !
  if (hin%gil%nvisi.gt.huge(nv)) then
    write(mess,*) 'Cannot handle more than ',huge(nv),' visibilities'
    call map_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  nv = hin%gil%nvisi
  np = hin%gil%dim(1)
  !
  allocate(times(nv),indx(nv), stat=ier)
  error = ier.ne.0
  if (error) return
  call loadbt (hin%r2d,np,nv,times,indx,sorted,4)
  !
  ! Sort if needed
  if (.not.sorted) then
    call gr8_trie(times,indx,nv,error)
    if (error) then
      call map_message(seve%e,rname,'Sorting error')
    else
      call sortiv (hin%r2d,hou%r2d,np,nv,indx)
    endif
  else
    call map_message(seve%w,rname,'Table is already sorted')
    hou%r2d = hin%r2d
  endif
  deallocate(times,indx)
end subroutine uvsort_bt
!
subroutine loadbt (visi,np,nv,tb,it,sorted,idate)
  !----------------------------------------------------------------------
  ! MAP    UVSORT routines
  !     Load Time / Baseline combination into work arrays for sorting.
  !----------------------------------------------------------------------
  integer, intent(in)  :: np                   ! Size of a visibility
  integer, intent(in)  :: nv                   ! Number of visibilities
  real, intent(in) :: visi(np,nv)              ! Input visibilities
  real(8), intent(out) :: tb(nv)               ! Output Date+Time
  integer, intent(out) :: it(nv)               ! Indexes
  logical, intent(out) :: sorted               !
  integer, intent(in)  :: idate                ! Date pointer
  !
  integer :: iv, itime, iant, jant, id, jd, nd,ier
  real(8) :: vmax, dd
  integer, allocatable :: dates(:),jdate(:)
  !
  itime = idate+1
  iant = itime+1
  jant = iant+1
  !
  ! Compact the dates
  allocate(dates(nv),jdate(nv),stat=ier)
  if (ier.ne.0) return
  nd = 1
  dates(nd) = visi(idate,1)
  jdate(1) = 0
  do iv=2,nv
    jd = 0
    do id = 1,nd
      if (visi(idate,iv).eq.dates(id)) then
        jd = id
        jdate(iv) = jd-1
        exit
      endif
    enddo
    if (jd.eq.0) then
      nd = nd+1
      dates(nd) = visi(idate,iv)
      jdate(iv) = nd-1
    endif
  enddo
  !
  dd = nd+2
  do iv=1,nv
    ! Time compacted value:
    ! This is a date in Days, with a minimun range
    tb(iv) = dble(jdate(iv))+dble(visi(itime,iv))/86400d0
    ! Add the baseline number code as major driver, as steps are > dd
    tb(iv) = tb(iv) + dd * (100.0*visi(iant,iv)+visi(jant,iv))
  enddo
  !
  do iv=1,nv
    it(iv) = iv
  enddo
  !
  vmax = tb(1)
  do iv = 1,nv
    if (tb(iv).lt.vmax) then
      sorted = .false.
      return
    endif
    vmax = tb(iv)
  enddo
  sorted = .true.
end subroutine loadbt
!
subroutine loadtb (visi,np,nv,tb,it,sorted,idate)
  !----------------------------------------------------------------------
  ! MAP    UVSORT routines
  !     Load new U,V coordinates and sign indicator into work arrays
  !     for sorting.
  !----------------------------------------------------------------------
  integer, intent(in)  :: np                   ! Size of a visibility
  integer, intent(in)  :: nv                   ! Number of visibilities
  real, intent(in) :: visi(np,nv)              ! Input visibilities
  real(8), intent(out) :: tb(nv)               ! Output Date+Time
  integer, intent(out) :: it(nv)               ! Indexes
  logical, intent(out) :: sorted               !
  integer, intent(in)  :: idate                ! Date pointer
  !
  integer :: iv, itime, iant, jant, id, jd, nd,ier
  real(8) :: vmax, dd
  integer, allocatable :: dates(:),jdate(:)
  integer :: imin, imax
  !
  ! Scan how many days
  itime = idate+1
  iant = itime+1
  jant = iant+1
  allocate(dates(nv),jdate(nv),stat=ier)
  if (ier.ne.0) return
  nd = 1
  dates(nd) = visi(idate,1)
  jdate(1) = 0
  do iv=2,nv
    jd = 0
    do id = 1,nd
      if (visi(idate,iv).eq.dates(id)) then
        jd = id
        jdate(iv) = jd-1
        exit
      endif
    enddo
    if (jd.eq.0) then
      nd = nd+1
      dates(nd) = visi(idate,iv)
      jdate(iv) = nd-1
    endif
  enddo
  !
  do iv=1,nv
    ! Time compacted value in seconds
    tb(iv) = dble(jdate(iv))*86400d0+dble(visi(itime,iv))
    ! The baseline is not accounted for into this yet...
    ! Do it by adding the baseline number as a sub-item.
    ! This assumes time differ by more than 1 second here, and there
    ! are less than 256 antennas
    imax = max(visi(iant,iv),visi(jant,iv))
    imin = min(visi(iant,iv),visi(jant,iv))    
    tb(iv) = tb(iv) + (256.0*imin+imax)/(256.0*256.0)
  enddo
  !
  do iv=1,nv
    it(iv) = iv
  enddo
  !
  vmax = tb(1)
  do iv = 1,nv
    if (tb(iv).lt.vmax) then
      sorted = .false.
      return
    endif
    vmax = tb(iv)
  enddo
  sorted = .true.
end subroutine loadtb
!
subroutine my_big_uvsort(uvdata,uvtri,type,error)
  !----------------------------------------------------------------------
  ! GILDAS
  !       Sort an input UV table
  !       Add CC code to implement precession
  !----------------------------------------------------------------------
  use image_def
  use gkernel_interfaces
  use gbl_message
  !
  character(len=*), intent(in) :: type
  character(len=*), intent(in) :: uvdata,uvtri
  logical, intent(out) :: error
  !
  ! Local variables:
  character(len=*), parameter :: rname='BIG_UVSORT'
  character(len=60) :: mess
  type (gildas) :: in, out
  character(len=12) :: xtype
  character(len=filename_length) :: name
  real, allocatable :: din(:,:)
  real(8), allocatable :: times(:)
  integer, allocatable :: indx(:)
  real(4), allocatable :: u(:), v(:)
  logical, allocatable :: s(:)
  integer :: codes(4)
  logical :: sorted
  integer :: iv,jv,kv,mv,ib
  integer :: nblock
  integer :: one(1)
  !
  real xy(2),cs(2), uvmax, uvmin
  integer :: ier, np, nv
  data xy/0.0,0.0/, cs/1.0,0.0/
  data xtype /'UV-DATA'/
  !
  error = .false.
  call map_message(seve%w,rname,'Doing '//trim(type)//' sorting for huge file')
  !
  ! Input file
  call gildas_null(in, type = 'UVT')
  name = trim(uvdata)
  call gdf_read_gildas(in, name, '.uvt', error, data=.false.)
  !
  nv = in%gil%nvisi
  allocate (indx(nv),times(nv),stat=ier)
  error = ier.ne.0
  if (error) return
  !
  sorted = .false.
  if  (type.eq.'TB') then
    allocate (din(4,nv),stat=ier)
    codes(1:4) = [code_uvt_date, code_uvt_time, code_uvt_anti, code_uvt_antj]
    call gdf_read_uvonly_codes(in, din, codes, 4, error)
    call loadtb (din,4,nv,times,indx,sorted,1)
  elseif  (type.eq.'BT') then
    allocate (din(4,nv),stat=ier)
    codes(1:4) = [code_uvt_date, code_uvt_time, code_uvt_anti, code_uvt_antj]
    call gdf_read_uvonly_codes(in, din, codes, 4, error)
    call loadbt (din,4,nv,times,indx,sorted,1)
  elseif (type.eq.'UV') then
    allocate (din(2,in%gil%nvisi),stat=ier)
    codes(1:2) = [code_uvt_u,code_uvt_v]
    call gdf_read_uvonly_codes(in, din, codes, 2, error)
    !
    allocate(u(nv),v(nv),s(nv),stat=ier)
    call loaduv (din,2,nv,cs,u,v,s,uvmax,uvmin)
    call chksuv (nv,v,indx,sorted)
    if (.not.sorted) then
      allocate (in%r2d(in%gil%dim(1),1), stat=ier)
      times(1:nv) = v(1:nv)
    endif
  endif
  !
  ! Sort if needed
  if (sorted) then
    call map_message(seve%e,rname,'Input file is already sorted')
    error = .true.
    return ! Do not even copy the input file ?...
  endif
  !
  call gr8_trie(times,indx,nv,error)
  if (error) return
  !
  ! Simple case
  call gildas_null(out, type = 'UVT')
  call gdf_copy_header(in,out,error)
  name = trim(uvtri)
  call sic_parsef(name,out%file,' ','.uvt')
  if (in%file .eq. out%file) then
    call map_message(seve%e,rname,'In place sorting not allowed')
    error = .true.
    return
  endif
  call gdf_create_image (out,error)
  if (error) return
  !
  call gdf_nitems('SPACE_GILDAS',nblock,out%gil%dim(1))
  nblock = min(out%gil%dim(2),nblock)
  allocate (out%r2d(out%gil%dim(1),nblock), stat=ier)
  !
  ! Loop over line table
  out%blc = 0
  out%trc = 0
  in%blc = 0
  in%trc = 0
  iv = 0
  one(1) = 1
  np = in%gil%dim(1)
  !
  ! Create output by block
  do ib = 1,out%gil%nvisi,nblock
    write(mess,*) ib,' / ',out%gil%nvisi,nblock
    call map_message(seve%d,rname,mess)
    out%blc(2) = ib
    out%trc(2) = min(out%gil%dim(2),ib-1+nblock)
    mv = out%trc(2)-out%blc(2)+1
    !
    ! Build the output only one visibility at once
    ! This can be VERY slow, but no other choice here...
    do jv=1,mv
      iv = iv+1
      kv = indx(iv)
      in%blc(2) = kv
      in%trc(2) = kv
      if (type.eq.'UV') then
        ! Read and modify the visibility because of
        ! the possible phase shift
        call gdf_read_data(in,in%r2d,error)
        call sortuv (in%r2d,out%r2d(:,jv),np,1,in%gil%ntrail, &
          &   xy,u(kv),v(kv),s(kv),one)
      else
        ! Just read in appropriate place
        call gdf_read_data(in,out%r2d(:,jv),error)
      endif
    enddo
    call gdf_write_data (out,out%r2d,error)
    if (error) return
  enddo
  !
  call gdf_close_image(out,error)
  if (error) then
    call map_message(seve%e,rname,'Error writing UV table')
    return
  endif
end subroutine my_big_uvsort
!
subroutine uvsort_fi (hin,hou,error)
  use gkernel_interfaces
  use image_def
  use gbl_message
  !----------------------------------------------------------------------
  ! MAP
  !     Sort a (mosaic) UV table by fields.
  !----------------------------------------------------------------------
  type(gildas), intent(inout) :: hin
  type(gildas), intent(inout) :: hou
  logical, intent(out) :: error
  !
  character(len=*), parameter :: rname='UVSORT_FIELDS'
  character(len=60) :: mess
  integer :: ier, ixoff, iyoff
  integer :: nv
  integer :: np
  integer, allocatable :: times(:)
  integer, allocatable :: indx(:)
  logical :: sorted
  !
  if (hin%gil%column_size(code_uvt_xoff).ne.1 .or.  &
    & hin%gil%column_size(code_uvt_yoff).ne.1) then
    mess = 'Input UV tables has no offset columns'
    call map_message(seve%e,rname,mess)
    error = .true.
    return
  else
    ixoff = hin%gil%column_pointer(code_uvt_xoff)
    iyoff = hin%gil%column_pointer(code_uvt_yoff)
  endif
  !
  if (hin%gil%nvisi.gt.huge(nv)) then
    write(mess,*) 'Cannot handle more than ',huge(nv),' visibilities'
    call map_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  nv = hin%gil%nvisi
  np = hin%gil%dim(1)
  !
  allocate(times(nv),indx(nv), stat=ier)
  error = ier.ne.0
  if (error) return
  call loadfi (hin%r2d,np,nv,times,indx,sorted,ixoff,iyoff)
  !
  ! Sort if needed
  if (.not.sorted) then
    call gi4_trie(times,indx,nv,error)
    if (error) then
      call map_message(seve%e,rname,'Sorting error')
    else
      call sortiv (hin%r2d,hou%r2d,np,nv,indx)
    endif
  else
    call map_message(seve%w,rname,'Table is already sorted')
    hou%r2d = hin%r2d
  endif
  deallocate(times,indx)
end subroutine uvsort_fi
!
subroutine loadfi (visi,np,nv,tb,it,sorted,ixoff,iyoff)
  !----------------------------------------------------------------------
  ! MAP    UVSORT routines
  !     Load field numbers into work arrays for sorting.
  !----------------------------------------------------------------------
  integer, intent(in)  :: np                   ! Size of a visibility
  integer, intent(in)  :: nv                   ! Number of visibilities
  real, intent(in) :: visi(np,nv)              ! Input visibilities
  integer, intent(out) :: tb(nv)               ! Output field number
  integer, intent(out) :: it(nv)               ! Indexes
  logical, intent(out) :: sorted               !
  integer, intent(in)  :: ixoff                ! X pointer
  integer, intent(in)  :: iyoff                ! Y pointer
  !
  integer :: iv, itime, iant, jant
  integer :: ifi, mfi, kfi, nfi, ier
  real(8) :: vmax, d0
  real(kind=8), allocatable :: doff(:,:), dtmp(:,:)
  !
  ! Scan how many fields
  nfi = 1
  mfi = 100
  allocate(doff(2,mfi),stat=ier)
  doff(1,1) = visi(ixoff,1)
  doff(2,1) = visi(iyoff,1)
  tb(1) = 1
  !
  do iv=2,nv
    kfi = 0
    do ifi=1,nfi
      if (visi(ixoff,iv).eq.doff(1,ifi) .and. &
      & visi(iyoff,iv).eq.doff(2,ifi) ) then
        tb(iv) = ifi
        kfi = ifi
        exit
      endif
    enddo
    !
    ! New field
    if (kfi.eq.0) then
      if (nfi.eq.mfi) then
        allocate(dtmp(2,2*mfi),stat=ier)
        dtmp(:,1:mfi) = doff(:,:)
        deallocate(doff)
        allocate(doff(2,2*mfi),stat=ier)
        doff(:,:) = dtmp
        deallocate(dtmp)
        mfi = 2*mfi
      endif
      nfi = nfi+1
      doff(1,nfi) = visi(ixoff,iv)
      doff(2,nfi) = visi(iyoff,iv)
      tb(iv) = nfi
    endif
  enddo
  !
  do iv=1,nv
    it(iv) = iv
  enddo
  !
  vmax = tb(1)
  do iv = 1,nv
    if (tb(iv).lt.vmax) then
      sorted = .false.
      return
    endif
    vmax = tb(iv)
  enddo
  sorted = .true.
end subroutine loadfi

