program uv_continuum
  use gildas_def
  use gkernel_interfaces
  character(len=filename_length) :: cuvin, cuvou 
  character(len=512) :: cfreq
  integer, parameter :: mfreq=32
  real(8) :: width
  real(8), parameter :: rnone=-1D9
  real(8) :: freqs(mfreq)
  integer :: chan(2)
  integer :: i, nf, ier
  logical error
  !
  ! Code:
  call gildas_open
  call gildas_char('UV_INPUT$',cuvin)
  call gildas_char('UV_OUTPUT$',cuvou)
  call gildas_char('FREQUENCIES$',cfreq)
  call gildas_dble('WIDTH$',width,1)
  call gildas_inte('CHANNEL$',chan,2)
  call gildas_close
  !
  freqs = rnone  ! Invalid frequency
  read(cfreq,*,iostat=ier) freqs
  if (ier.gt.0) then
    call putios('E-UV_FILTER',ier) 
    write(6,*) cfreq
    call sysexi(fatale)
  endif
  nf = 0
  do i=1,mfreq
    if (freqs(i).eq.rnone) exit
    nf = nf+1
  enddo
  !
  call sub_uv_filter (cuvin,cuvou,nf,freqs,width,chan,error)
  if (error) call sysexi(fatale)
end program uv_continuum
!
subroutine sub_uv_filter(cuvin,cuvou,mf,freqs,width,chan,error)
  use image_def
  use gkernel_interfaces
  use gbl_message
  !
  character(len=*), intent(in) :: cuvin 
  character(len=*), intent(in) :: cuvou 
  integer, intent(in) :: mf
  real(8), intent(in) :: freqs(mf)
  real(8), intent(in) :: width
  logical, intent(out) :: error
  logical, parameter :: zero=.true.
  integer, intent(inout) :: chan(2)
  !
  ! Local variables
  character(len=*), parameter :: rname='UV_CONTINUUM'
  character(len=80)  :: mess
  type (gildas) :: uvin
  type (gildas) :: uvou
  integer :: ier, nblock, ib
  integer(kind=index_length) :: nd, nv 
  integer :: nf, nc, ichan, jchan, iv, i, j, k, l, m, nsum
  integer, allocatable :: filtre(:), channels(:)
  !
  ! Simple checks 
  error = len_trim(cuvin).eq.0
  if (error) then
    call map_message(seve%e,rname,'No input UV table name')
    return
  endif
  !
  call gildas_null (uvin, type = 'UVT')     ! Define a UVTable gildas header
  call gdf_read_gildas (uvin, cuvin, '.uvt', error, data=.false.)
  if (error) then
    call map_message(seve%e,rname,'Cannot read input UV table')
    return 
  endif
  nc = uvin%gil%nchan 
  nd = uvin%gil%dim(1)
  !
  ier = gdf_range (chan,nc)
  print *,'Channels ',chan
  !
  ! Here modify the header of the output UV table according
  ! to the desired goal
  !
  call gildas_null (uvou, type = 'UVT')     ! Define a UVTable gildas header
  call gdf_copy_header(uvin,uvou,error)
  call sic_parse_file(cuvou,' ','.uvt',uvou%file)
  !
  ! Do not forget trailing columns
  nsum = (chan(1)+chan(2)+1)/2
  uvou%gil%dim(1) = 10+uvou%gil%ntrail
  uvou%gil%inc(1) = uvou%gil%inc(1)*nc
  uvou%gil%ref(1) = 1.d0-(float(nsum)/nc-uvou%gil%ref(1))/nc
  uvou%gil%vres = nc*uvou%gil%vres
  uvou%gil%fres = nc*uvou%gil%fres
  uvou%gil%nchan = 1
  call gdf_uv_shift_columns(uvin,uvou)
  call gdf_setuv (uvou, error)
  if (error) return
  !
  ! Define blocking factor, on largest data file, usually the input one
  ! but not always...  
  call gdf_nitems('SPACE_GILDAS',nblock,uvin%gil%dim(1)) ! Visibilities at once
  nblock = min(nblock,uvin%gil%dim(2))
  ! Allocate respective space for each file
  allocate (uvin%r2d(uvin%gil%dim(1),nblock),uvou%r2d(uvou%gil%dim(1),nblock), stat=ier)
  if (ier.ne.0) then
    write(mess,*) 'Memory allocation error ',uvin%gil%dim(1), nblock
    call map_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  allocate(filtre(nc),channels(nc),stat=ier)
  if (ier.ne.0) then
    call map_message(seve%e,rname,'Memory allocation error')
    error = .true.
    return
  endif    
  !
  k = 0
  do i=1,mf
    ichan = (freqs(i)-uvin%gil%freq-width)/abs(uvin%gil%fres) + uvin%gil%ref(1)  
    jchan = (freqs(i)-uvin%gil%freq+width)/abs(uvin%gil%fres) + uvin%gil%ref(1)   
    !
    ! Set channels only once...
    do j=ichan,jchan
      if (j.lt.0 .or. j.gt.uvin%gil%nchan) then
        cycle
      else
        m = 0
        do l=1,k
          if (channels(l).eq.j) then
            m = l
            exit
          endif
        enddo
        if (m.eq.0) then
          k = k+1
          channels(k) = j
        endif
      endif
    enddo
  enddo
  !
  ! Set the valid one, now
  nf = 0
  do i=1,k
    if (channels(i).gt.0 .and. channels(i).le.nc) then
      nf = nf+1
      filtre(nf) = channels(i)
    endif
  enddo
  !
  ! create the image
  call gdf_create_image(uvou,error)
  if (error) return
  !
  ! Loop per blocks over line table
  uvou%blc = 0
  uvou%trc = 0
  uvin%blc = 0
  uvin%trc= 0
  !
  do i=1,uvou%gil%dim(2),nblock
    write(mess,*) i,' / ',uvou%gil%dim(2),nblock
    call map_message(seve%i,rname,mess) 
    uvou%blc(2) = i
    uvou%trc(2) = min(uvou%gil%dim(2),i-1+nblock) 
    uvin%blc(2) = i
    uvin%trc(2) = uvou%trc(2)
    call gdf_read_data(uvin,uvin%r2d,error)
    if (error) return
    !
    nv = uvin%trc(2)-uvin%blc(2)+1
    call my_uv_filter (uvin%r2d,nd,nv,nc,nf,filtre,zero) 
    !
    call my_uv_average (uvou%r2d, uvou%gil%dim(1),nv,           &
     &    uvou%gil%nlead,  uvou%gil%ntrail,               &
     &    uvin%r2d ,uvin%gil%dim(1),chan,2)
    !
    call gdf_write_data (uvou,uvou%r2d,error)
    if (error) return
  enddo
  !
  ! Finalize the image
  call gdf_close_image(uvou,error)
  call gdf_close_image(uvin,error)
  !  
  call map_message(seve%i,rname,'Successful completion')
end subroutine sub_uv_filter
!
subroutine my_uv_filter(duv,nd,nv,nc,nf,filtre,zero)
  use gildas_def
  !----------------------------------------------------------
  ! @ private
  !
  ! MAPPING
  !   Support routine for UV_FILTER 
  !   Filter / Flag a list of channels 
  !----------------------------------------------------------
  !
  integer(kind=index_length) , intent(in) :: nv          ! Number of visibilities
  integer(kind=index_length) , intent(in) :: nd          ! Visibility size
  integer, intent(in) :: nc          ! Number of channels
  real, intent(inout) :: duv(nd,nv)  ! Visibilities
  !  
  integer, intent(in) :: nf          ! Number of values
  integer, intent(in) :: filtre(nf)  ! Channel list
  logical, intent(in) :: zero        ! Zero or not
  !
  integer :: i,iv
  !
  !
  if (zero) then
    do iv=1,nv
      do i=1,nf
        duv(5+3*filtre(i):7+3*filtre(i),iv) = 0
      enddo
    enddo
  else
    do iv=1,nv
      do i=1,nf
        duv(7+3*filtre(i),iv) = -abs(duv(7+3*filtre(i),iv))
      enddo
    enddo
  endif
end subroutine my_uv_filter
!
subroutine my_uv_average (out,nx,nv,nlead,ntrail,inp,ny,nc,num)
  use gildas_def
  integer(kind=index_length) :: nx  ! Size of output visibility
  integer(kind=index_length) :: nv  ! Number of visibilities
  integer, intent(in) :: nlead      ! Leading columns
  integer, intent(in) :: ntrail     ! Trailing columns
  real :: out(nx,nv)                ! Output visibilities
  integer(kind=index_length) :: ny  ! Size of input visibility
  real :: inp(ny,nv)                ! Input visibilities
  integer :: num                    ! Number of ranges X 2
  integer :: nc(num)                ! Range boundaries
  ! Local
  integer :: k,kk,l
  integer(kind=index_length) :: j
  real :: a,b,c
  !
  do j=1,nv
    out(1:nlead,j) = inp(1:nlead,j)
    a = 0.0
    b = 0.0
    c = 0.0
    do l=2,num,2
      do k=nc(l-1),nc(l)
        kk = nlead+3*k
        if (inp(kk,j).gt.0) then
          a = a+inp(kk-2,j)*inp(kk,j)
          b = b+inp(kk-1,j)*inp(kk,j)
          c = c+inp(kk  ,j)
        endif
      enddo
    enddo
    if (c.ne.0.0) then
      out(8,j) =a/c
      out(9,j) =b/c
      out(10,j)=c              ! time*band
    else
      out(8,j) =0.0
      out(9,j) =0.0
      out(10,j)=0.0
    endif
    if (ntrail.gt.0) out(nx-ntrail+1:nx,j) = inp(ny-ntrail+1:ny,j)
  enddo
end subroutine my_uv_average
