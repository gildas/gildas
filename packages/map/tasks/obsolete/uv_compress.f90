program uv_compress
  use gildas_def
  use gkernel_interfaces
  use gbl_format
  !---------------------------------------------------------------------
  ! GILDAS
  ! Compress an input UV table
  !---------------------------------------------------------------------
  ! Local
  character(len=filename_length) :: uvdata,uvsort
  integer :: nc
  logical :: error
  !
  ! Code:
  call gildas_open
  call gildas_char('UV_INPUT$',uvdata)
  call gildas_char('UV_OUTPUT$',uvsort)
  call gildas_inte('NC$',nc,1)
  call gildas_close
  !
  call sub_uv_compress(uvdata,uvsort,nc,error)
  if (error) call sysexi(fatale)
  call gagout('S-UV_COMPRESS,  Successful completion')
  call sysexi(1)
end program
!  
subroutine sub_uv_compress(uvdata,uvsort,naver,error)
  use gildas_def
  use gkernel_interfaces
  use image_def
  use gbl_message
  !
  character(len=*), intent(in) :: uvdata !
  character(len=*), intent(in) :: uvsort ! 
  integer, intent(in) :: naver           ! Number of channels to average
  logical, intent(out) :: error          ! Error flag
  !
  character(len=*), parameter :: rname='UV_COMPRESS'
  type(gildas) :: out,in
  integer :: n,nc,nchan,ier,i,nblock,nvisi
  character(len=80) :: mess
  !
  error = .true.
  if (naver.lt.0) return
  n = len_trim(uvdata)
  if (n.le.0) return
  n = len_trim(uvsort)
  if (n.le.0) return
  !
  ! Input file
  call gildas_null (in, type= 'UVT')
  call gdf_read_gildas (in, uvdata, '.uvt', error, data=.false.)
  if (error) then
    call map_message(seve%e,rname,'Cannot read input UV table')
    return
  endif
  !
  ! Define number of channels
  if (naver.gt.in%gil%nchan .or. naver.eq.0) then
    nc = in%gil%nchan
    nchan = 1
    call map_message(seve%w,rname,'Compressing all channels')
  else
    nc = naver
    nchan = in%gil%nchan/nc
  endif
  write(mess,*) 'Compressing by ',nc,' channels'
  call map_message(seve%i,rname,mess)
  call gildas_null(out, type= 'UVT')
  call gdf_copy_header(in, out, error)
  if (error) return
  !
  ! Modify header
  out%gil%dim(1) = out%gil%nlead + out%gil%natom*nchan + out%gil%ntrail
  out%gil%nchan = nchan
  out%gil%inc(1) = out%gil%inc(1)*nc
  out%gil%ref(1) = (2.0*out%gil%ref(1)+nc-1.0)/2/nc
  out%gil%vres = nc*out%gil%vres
  out%gil%fres = nc*out%gil%fres
  call gdf_uv_shift_columns(in,out)
  call gdf_setuv(out,error)
  if (error) return
  !
  ! Define blocking factor
  call gdf_nitems('SPACE_GILDAS',nblock,in%gil%dim(1)) ! Visibilities at once
  nblock = min(nblock,in%gil%dim(2))
  allocate (in%r2d(in%gil%dim(1),nblock), out%r2d(out%gil%dim(1),nblock), stat=ier)
  if (ier.ne.0) then
    write(mess,*) 'Memory allocation error ',in%gil%dim(1), nblock
    call map_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  call sic_parse_file(uvsort,' ','.uvt',out%file)
  call gdf_create_image (out,error)
  if (error) return
  !
  ! Loop over line table
  out%blc = 0
  out%trc = 0
  in%blc = 0
  in%trc= 0
  !
  do i=1,out%gil%dim(2),nblock
    write(mess,*) i,' / ',out%gil%dim(2),nblock
    call map_message(seve%i,rname,mess) 
    out%blc(2) = i
    out%trc(2) = min(out%gil%dim(2),i-1+nblock) 
    in%blc(2) = i
    in%trc(2) = out%trc(2)
    call gdf_read_data(in,in%r2d,error)
    nvisi = out%trc(2)-out%blc(2)+1
    call my_compress_uv ( &
     &  out%r2d,out%gil%dim(1),nvisi,out%gil%nchan, & 
     &  in%gil%nlead, in%gil%ntrail, in%r2d,in%gil%dim(1), nc)
    call gdf_write_data (out,out%r2d,error)
    if (error) return
  enddo
  !
  ! Finalize the image
  call gdf_close_image(out,error)
  call gdf_close_image(in,error)
  !
end subroutine sub_uv_compress
!
subroutine my_compress_uv(out,nx,nv,nchan,nlead,ntrail,inp,ny,nc)
  use gildas_def
  integer(kind=index_length), intent(in) :: nx         ! Output Visibility size
  integer, intent(in) :: nv         ! Number of visibilities
  real, intent(out) :: out(nx,nv)   ! Output Visibilities
  integer(kind=index_length), intent(in) :: ny         ! Input visibility size
  real, intent(in) :: inp(ny,nv)    ! Input visibilities
  integer, intent(in) :: nc         ! Number of Channels to average
  integer, intent(in) :: nchan      ! Output number of channels
  integer, intent(in) :: nlead
  integer, intent(in) :: ntrail
  ! Local
  integer :: i,j,k,kk,ic
  real :: a,b,c
  !
  do j=1,nv
    out(1:nlead,j) = inp(1:nlead,j)
    ic = 1
    do i=1,nchan
      a = 0
      b = 0
      c = 0
      do k=ic,ic+nc-1
        kk = nlead+3*k
        if (inp(kk,j).ne.0) then
          a = a+inp(kk-2,j)*inp(kk,j)
          b = b+inp(kk-1,j)*inp(kk,j)
          c = c+inp(kk  ,j)
        endif
      enddo
      ic = ic+nc
      kk = nlead+3*i
      if (c.ne.0) then
        out(kk-2,j) =a/c
        out(kk-1,j) =b/c
        out(kk  ,j) =c         ! time*band
      else
        out(kk-2,j) =0
        out(kk-1,j) =0
        out(kk  ,j) =0
      endif
    enddo
    if (ntrail.gt.0) then
      out(nx-ntrail+1:nx,j) = inp(ny-ntrail+1:ny,j)
    endif
  enddo
end subroutine my_compress_uv
