program uvtrim_main
  use gildas_def
  use image_def
  use gkernel_interfaces
  !----------------------------------------------------------------------
  ! GILDAS
  !       Trim an input UV table: get rid off all flagged visibilities
  !----------------------------------------------------------------------
  character(len=filename_length) :: uvdata,uvtri
  character(len=8) :: code
  integer :: wcol
  logical :: error
  !
  ! Code:
  call gildas_open
  call gildas_char('UVDATA$',uvdata)
  call gildas_char('UVTRIM$',uvtri)
  call gildas_char('CODE$',code)
  call sic_upper(code)
  call gildas_inte('WCOL$',wcol,1)
  call gildas_close
  call sub_uv_trim(uvdata,uvtri,code,wcol,error)
  if (error) call sysexi(fatale)
end program uvtrim_main
!
!
subroutine sub_uv_trim (cuvin, cuvou, code, wcol, error)
  use gkernel_interfaces
  use mapping_interfaces
  use image_def
  use gbl_message
  !---------------------------------------------------------------------
  ! GILDAS
  !   Trim a UV table,
  !      - remove the flagged data
  !   or  - trim trailing columns
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: cuvin
  character(len=*), intent(in) :: cuvou
  character(len=*), intent(in) :: code
  integer, intent(in) :: wcol
  logical, intent(out) :: error
  !
  ! Local variables
  character(len=*), parameter :: rname='UV_TRIM'
  character(len=80)  :: mess
  type (gildas) :: uvin
  type (gildas) :: uvou
  integer :: ier, nblock, ib, invisi, ouvisi
  integer(index_length) :: mvis
  !
  ! Simple checks
  select case (code)
  case ('DATA','TRAIL')
    continue
  case default
    error =.true.
    call map_message(seve%e,rname,'Invalid code '//code)
    return
  end select
  !
  error = len_trim(cuvin).eq.0
  if (error) then
    call map_message(seve%e,rname,'No input UV table name')
    return
  endif
  !
  call gildas_null (uvin, type = 'UVT')     ! Define a UVTable gildas header
  call gdf_read_gildas (uvin, cuvin, '.uvt', error, data=.false.)
  if (error) then
    call map_message(seve%e,rname,'Cannot read input UV table')
    return
  endif
  !
  ! Here modify the header of the output UV table according
  ! to the desired goal
  !
  call gildas_null (uvou, type = 'UVT')     ! Define a UVTable gildas header
  call gdf_copy_header(uvin,uvou,error)
  call sic_parse_file(cuvou,' ','.uvt',uvou%file)
  !
  if (code.eq.'TRAIL') then
    uvou%gil%ntrail = 0
    do ib=1,code_uvt_last
      if (uvou%gil%column_pointer(ib).gt.uvin%gil%fcol) then
        uvou%gil%column_pointer(ib) =  0
        uvou%gil%column_size(ib) = 0
      endif
    enddo
    uvou%gil%dim(1) = uvou%gil%lcol
    call gdf_setuv(uvou,error)
  endif
  !
  ! Define blocking factor, on largest data file, usually the input one
  ! but not always...
  call gdf_nitems('SPACE_GILDAS',nblock,uvin%gil%dim(1)) ! Visibilities at once
  nblock = min(nblock,uvin%gil%dim(2))
  ! Allocate respective space for each file
  allocate (uvin%r2d(uvin%gil%dim(1),nblock), uvou%r2d(uvou%gil%dim(1),nblock), stat=ier)
  if (ier.ne.0) then
    write(mess,*) 'Memory allocation error ',uvin%gil%dim(1), nblock
    call map_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  ! create the image with smallest possible size
  uvou%gil%dim(2) = nblock
  uvou%gil%nvisi = 0
  call gdf_create_image(uvou,error)
  if (error) return
  !
  ! Loop over line table - The example assumes the same
  ! number of visibilities in Input and Output, which may not
  ! be true...
  uvin%blc = 0
  uvin%trc = 0
  uvou%blc = 0
  uvou%trc = 0
  uvou%blc(2) = 1  ! Get started
  !
  if (code.eq.'DATA') then
    do ib = 1,uvin%gil%dim(2),nblock
      write(mess,*) ib,' / ',uvin%gil%dim(2),nblock
      call map_message(seve%d,rname,mess)
      uvin%blc(2) = ib
      uvin%trc(2) = min(uvin%gil%dim(2),ib-1+nblock)
      invisi = uvin%trc(2)-uvin%blc(2)+1
      call gdf_read_data(uvin,uvin%r2d,error)
      !
      call my_uvtrim(uvin, invisi, uvou, ouvisi, wcol, error)
      if (error) return
      !
      if (ouvisi.gt.0) then
        uvou%trc(2) = uvou%blc(2)+ouvisi-1
        mvis = uvou%trc(2)
        !
        if (mvis.gt.uvou%gil%dim(2)) then
          print *,'Extending from ',uvou%gil%dim(2), ' to  ',mvis
          call gdf_close_image(uvou,error)
          call gdf_extend_image (uvou,mvis,error)
        endif
        call gdf_write_data (uvou,uvin%r2d,error)
        if (error) return
        uvou%blc(2) = uvou%trc(2)+1
      endif
    enddo
  else if (code.eq.'TRAIL') then
    do ib = 1,uvin%gil%dim(2),nblock
      write(mess,*) ib,' / ',uvin%gil%dim(2),nblock
      call map_message(seve%d,rname,mess)
      uvin%blc(2) = ib
      uvin%trc(2) = min(uvin%gil%dim(2),ib-1+nblock)
      invisi = uvin%trc(2)-uvin%blc(2)+1
      call gdf_read_data(uvin,uvin%r2d,error)
      !
      ouvisi = invisi
      !
      if (ouvisi.gt.0) then
        uvou%r2d(:,1:invisi) = uvin%r2d(1:uvin%gil%lcol,1:invisi)
        uvou%trc(2) = uvou%blc(2)+ouvisi-1
        mvis = uvou%trc(2)
        !
        if (mvis.gt.uvou%gil%dim(2)) then
          print *,'Extending from ',uvou%gil%dim(2), ' to  ',mvis
          call gdf_close_image(uvou,error)
          call gdf_extend_image (uvou,mvis,error)
        endif
        call gdf_write_data (uvou,uvou%r2d,error)
        if (error) return
        uvou%blc(2) = uvou%trc(2)+1
      endif
    enddo
  endif
  !
  ! Finalize the image
  uvou%gil%nvisi = uvou%trc(2)
  uvou%gil%dim(2) = uvou%trc(2)
  call gdf_update_header(uvou,error)
  if (error) return
  call gdf_close_image(uvou,error)
  if (error) return
  !
  call gdf_close_image(uvin,error)
  call map_message(seve%i,rname,'Successful completion')
  return
end subroutine sub_uv_trim
!
subroutine my_uvtrim(in,invis,out,outvis,wcol,error)
  use gildas_def
  use image_def
  use gkernel_interfaces
  !----------------------------------------------------------------------
  ! GILDAS
  !       Trim an input UV table: get rid off all flagged visibilities
  !----------------------------------------------------------------------
  type (gildas), intent(inout) :: in, out
  integer, intent(in) :: invis
  integer, intent(out) :: outvis
  integer, intent(in) :: wcol
  logical, intent(out) :: error
  !
  ! Local variables:
  integer(kind=index_length), allocatable :: ivis(:)
  integer :: ier, np, nclow, ncup, ic
  integer :: iv, im, mv
  !
  np = in%gil%dim(1)
  if (wcol.lt.0) then
    nclow = 1
    ncup = in%gil%nchan
  else if (wcol.eq.0) then
    nclow = (in%gil%nchan+2)/3
    ncup = nclow
  else
    nclow = max(1,wcol)
    ncup = min(wcol,in%gil%nchan)
    nclow = ncup
  endif
  allocate(ivis(invis),stat=ier)
  if (ier.ne.0) then
    error = .true.
    return
  endif
  !
  ! First passs to identifiy all valid visibilities
  mv = 0
  do iv = 1,invis
    im = 1
    do ic=nclow,ncup
      if (in%r2d(in%gil%fcol-1+3*ic,iv).le.0) then
         im = 0
         exit
      endif
    enddo
    if (im.ne.0) then
      mv = mv+1
      ivis(mv) = iv
    endif
  enddo
  !
  ! Second pass to keep the good visibilities only
  ! Memory saved by working in place, as the visibilities are ordered.
  do iv = 1,mv
    if (ivis(iv).gt.iv) in%r2d(:,iv) = in%r2d(:,ivis(iv))
  enddo
  outvis = mv
  deallocate(ivis)
  !
end subroutine my_uvtrim
