program uv_center
  use gildas_def
  use gkernel_interfaces
  use image_def
  !---------------------------------------------------------------------
  ! GILDAS
  !	Compute position of centroid by fitting directly UV
  !	data to minimize the imaginary part
  !---------------------------------------------------------------------
  ! Local
  type(gildas) :: x
  ! PI is defined with more digits than necessary to avoid losing
  ! the last few bits in the decimal to binary conversion
  real(8), parameter :: pi=3.14159265358979323846d0
  real(8), parameter :: f_to_k=1.d0/299792458.d-6     ! not 2*PI
  character(len=80) :: iname, oname, hname
  real(4) :: limits(4), fact, x_ref0, velo, fsec, weight
  real(4) :: rms_fin, dec_x, dec_y, var_x, var_y, covar
  integer :: i, nc, nv, ic(2), iflag, ngrid, ier
  integer(kind=index_length) :: ipu, ipv, ipre, ipim, ipw
  logical :: error
  !
  ! Input file
  ! -----------
  call gildas_open
  call gildas_char('INPUT$',iname)
  call gildas_real('LIMITS$',limits,4)
  call gildas_inte('CHANNELS$',ic,2)
  call gildas_inte('GRID$',ngrid,1)
  call gildas_char('OUTPUT$',hname)
  call gildas_close
  !
  call gildas_null (x, type = 'TUV')
  call gdf_read_gildas (x, iname, '.tuv', error)
  if (error) then
    print *,'Error reading TUV data set'
    call sysexi(fatale)
  endif
  !
  ! Set Memory Pointers
  nv   = x%gil%nvisi          ! Number of Data
  nc   = x%gil%nchan          ! Number of Channels
  ier = gdf_range (ic, nc)
  ipu  = x%gil%column_pointer(code_uvt_u)       ! U Data Pointer
  ipv  = x%gil%column_pointer(code_uvt_v)       ! V Data Pointer
  !
  ! Convert U,V limits in meters
  fact = (x%gil%val(2)+x%gil%fres*(x%gil%val(2)/x%gil%freq)*0.5*(ic(1)+ic(2)))*f_to_k
  limits = limits*fact
  !
  ! Load Data and Compute Center of Image for each Channel I from [IC(1),IC(2)]
  call sic_parsef(hname,oname,' ','.dat')
  !
  open (unit=2,file=oname(1:lenc(oname)),status='UNKNOWN')
  !
  x_ref0 = (x%gil%freq-x%gil%val(2))/(x%gil%fres*(x%gil%val(2)/x%gil%freq)) + x%gil%ref(2)
  do i = ic(1),ic(2)
    ipre = x%gil%fcol+3*(i-1)  ! Real Part Data Pointer
    ipim = ipre+1    ! Imaginary Part Data Pointer
    ipw  = ipim+1    ! Weights Data Pointer
    call chkwei(nv,x%r2d(:,ipw),weight)
    if (weight.gt.0) then
      call center  (nv, &
     &        x%r2d(:,ipu),x%r2d(:,ipv),x%r2d(:,ipre),x%r2d(:,ipim),   &
     &        x%r2d(:,ipw),limits,rms_fin,dec_x,dec_y,var_x,   &
     &        var_y,covar,iflag,ngrid)
      var_x = sqrt(var_x)
      var_y = sqrt(var_y)
      rms_fin = sqrt(rms_fin)
      write(6,101) i, rms_fin, iflag
      fsec = 180.0d0*3600.0d0/pi/fact
      velo = x%gil%voff+x%gil%vres*(i-x%gil%ref(2))
      write(6,102) velo, dec_x*fsec, var_x*fsec*rms_fin,   &
     &        dec_y*fsec, var_y*fsec*rms_fin
      write(2,104) velo, rms_fin,   &
     &        dec_x/fact, var_x/fact, var_x*rms_fin/fact,   &
     &        dec_y/fact, var_y/fact, var_y*rms_fin/fact
    else
      write(6,103) i
    endif
  enddo
  call gdf_close_image (x,error)
  call gagout('S-UV_CENTER,  Normal successful completion')
  call sysexi(1)
  !
  101   format('I-UV_CENTER,  Channel ',i3,' RMS ',1pg9.2,' Code ',i2)
  102   format('   VLSR ',f10.4,'  X = ',f10.4,' +- ',f10.4,   &
     &    '; Y = ',f10.4,' +- ',f10.4,' (") ')
  103   format('E-UV_CENTER,  Channel ',i3,' has zero weight')
  104   format(8(1x,1pg11.4))
end program uv_center
!
subroutine chkwei(nv,w,wtot)
  integer :: nv                     !
  real :: w(nv)                     !
  real :: wtot                      !
  ! Local
  integer :: i
  !
  wtot = 0
  do i=1,nv
    if (w(i).gt.0) then
      wtot = wtot+w(i)
    endif
  enddo
end subroutine chkwei
!
subroutine center(ndata,u,v,re_dat,im_dat,w_dat,lim_xy,   &
     &    rms_fin,x_shift,y_shift,var_x,var_y,covar,iflag,ngrid)
  !---------------------------------------------------------------------
  ! Subroutine computing Center of Images
  !
  !  * INPUT Parameters  :
  !
  !	NDATA	: Number of Data
  !	U	: Vector [NDATA] = Abscissas of (U,V) Plane
  !	V	: Vector [NDATA] = Ordinates of (U,V) Plane
  !	RE_DAT	: Vector [NDATA] = Real Data
  !	IM_DAT	: Vector [NDATA] = Imaginary Data
  ! 	W_DAT	: Vector [NDATA] = Weights of Data
  !	LIM_XY	: Vector [4] = Bounds of the Field of Investigation
  !	NGRID	: Number of grid points for first search
  !
  !  * OUTPUT Parameters :
  !
  !	RMS_FIN : Final Value of the Error
  !	X_SHIFT	: Computed Value of the Shift in Abscissa
  !	Y_SHIFT	: Computed Value of the Shift in Ordinate
  !	VAR_X	: Variance of X_SHIFT
  !	VAR_Y	: Variance of Y_SHIFT
  !	COVAR	: Covariance of (X_SHIFT,Y_SHIFT)
  !	IFLAG	: Operation Code
  !
  !  * (U,V) Data must be enter in Wavelenghts, and the Bounds of
  !    Investigation LIM_XY in Radian.
  !---------------------------------------------------------------------
  integer :: ndata                  !
  real :: u(ndata)                  !
  real :: v(ndata)                  !
  real :: re_dat(ndata)             !
  real :: im_dat(ndata)             !
  real :: w_dat(ndata)              !
  real :: lim_xy(4)                 !
  real :: rms_fin                   !
  real :: x_shift                   !
  real :: y_shift                   !
  real :: var_x                     !
  real :: var_y                     !
  real :: covar                     !
  integer :: iflag                  !
  integer :: ngrid                  !
  ! Local
  integer :: nxmin, nxgrid, nxgri2
  parameter (nxmin=20,nxgrid=50,nxgri2=2500)
  integer :: j, nx, ny, nxy, nmin, ip_min(nxgri2)
  integer :: ip_sol, nminima
  real :: tab(3,nxgri2), trav_min(nxgri2)
  real :: solution(6,nxmin), rms_2
  real(8) :: lambda, xy(2), var_xy(3)
  !
  if (ngrid.gt.1) then
    !
    !  Compute Function F(X,Y) : Grid [NX x NY] in Area of Investigation.
    nx = min(ngrid,nxgrid)
    ny = nx
    write(6,1000) nx,ny
    nxy = nx * ny
    call fxy_grid(ndata,u,v,re_dat,im_dat,w_dat,lim_xy,   &
     &      nx,ny,nxy,tab)
    ! use 3rd col in tab(3,nxy) to order
    call minima(nxy,tab,trav_min,ip_min,3,3)
    nmin = min(nxmin,nxy)
  else
    !
    ! Start from center of LIM_XY
    nmin = 1
    ip_min(1) = 1
    tab(1,1) = 0.5*(lim_xy(1)+lim_xy(2))
    tab(2,1) = 0.5*(lim_xy(3)+lim_xy(4))
  endif
  !
  !  Cancel Imaginary Part for the NMIN Starting Values (X,Y).
  !  using MARQUARDT's Method (Linearization).
  write(6,*) 'Starting Investigation with MARQUARDT''s Method'
  nminima = 0
  do j = 1,nmin
    xy(1) = tab(1,ip_min(j))
    xy(2) = tab(2,ip_min(j))
    lambda = 1.e-2
    call marquardt(ndata,u,v,re_dat,im_dat,w_dat,xy,   &
     &      lambda,var_xy,rms_2,iflag)
    nminima = nminima + 1
    solution(1,nminima) = rms_2
    solution(2,nminima) = xy(1)
    solution(3,nminima) = xy(2)
    solution(4,nminima) = var_xy(1)
    solution(5,nminima) = var_xy(2)
    solution(6,nminima) = var_xy(3)
  enddo
  !
  ! Find the Smallest Value of RMS_2 and Output Results.
  if (nminima.gt.1) then
    ! use 1st col in solution(6,nminima)
    call minima(nminima,solution,trav_min,ip_min,1,6)
    ip_sol = ip_min(1)
  else
    ip_sol = 1
  endif
  rms_fin = solution(1,ip_sol)
  x_shift = solution(2,ip_sol)
  y_shift = solution(3,ip_sol)
  var_x   = solution(4,ip_sol)
  var_y   = solution(5,ip_sol)
  covar   = solution(6,ip_sol)
  return
  !
  1000  format(1x,'Computing Error Function for a ',i2,' x ',i2,' Grid')
end subroutine center
!
subroutine minima(ndata,tab,trav,point,icol,ncol)
  use gkernel_interfaces
  use gildas_def
  !---------------------------------------------------------------------
  ! Find the N smallest Values of a NDATA Vector
  !
  !  * INPUT Parameters  :
  !
  !	NDATA	: Dimension of the Input Vector
  !	TAB	: Vector [NCOL,NDATA] = Input Vector
  ! 	N	: Required Number of Minima @@@
  !	ICOL	: column for ordering
  !	NCOL	: number of columns
  !
  !  * WORKSPACE         :
  !
  !	TRAV	: Vector [NDATA] = Real Workspace
  !
  !  * OUTPUT Parameters :
  !
  !	POINT	: Vector [NDATA] = Pointers of Minima
  !---------------------------------------------------------------------
  integer :: ndata                  !
  integer :: ncol                   !
  real :: tab(ncol,ndata)           !
  real :: trav(ndata)               !
  integer :: point(ndata)           !
  integer :: icol                   !
  ! Local
  logical :: error
  integer :: j
  !
  do j = 1,ndata
    trav(j) = tab(icol,j)
  enddo
  call gr4_trie(trav,point,ndata,error)
  if (error) then
    write(6,*) 'E-UV_CENTER,  Cannot sort results'
    call sysexi (fatale)
  endif
end subroutine minima
!
subroutine fxy_grid(ndata,u,v,re_dat,im_dat,w_dat,lim,   &
     &    nx,ny,nxy,tab)
  !---------------------------------------------------------------------
  ! Compute the Error Function F(X,Y) for a Grid [NX x NY].
  !
  !  * INPUT Parameters  :
  !
  !	NDATA	: Number of Data
  !	U	: Vector [NDATA] = Abscissas of (U,V) Plane
  !	V	: Vector [NDATA] = Ordinates of (U,V) Plane
  !	RE_DAT	: Vector [NDATA] = Real Data
  !	IM_DAT	: Vector [NDATA] = Imaginary Data
  !	W_DAT   : Vector [NDATA] = Weights of Data
  !	LIM	: Vector [4] = Bounds of the Grid
  !	NX	: Number of X-Axis Values
  !	NY	: Number of Y-Axis Values
  !	NXY	: Total Number of Points of the Grid
  !
  !  * OUTPUT Parameters :
  !
  !	TAB	: Array [3,NXY] = Values X,Y,F(X,Y)
  !---------------------------------------------------------------------
  integer :: ndata                  !
  real :: u(ndata)                  !
  real :: v(ndata)                  !
  real :: re_dat(ndata)             !
  real :: im_dat(ndata)             !
  real :: w_dat(ndata)              !
  real :: lim(4)                    !
  integer :: nx                     !
  integer :: ny                     !
  integer :: nxy                    !
  real :: tab(3,nxy)                !
  ! Local
  integer :: i, j, k, l
  real :: pi, step_x, step_y, c, s, x, y
  real*8 :: f, im
  !
  pi = 3.1415926535897932384626
  step_x = (lim(2) - lim(1)) / (nx - 1)
  step_y = (lim(4) - lim(3)) / (ny - 1)
  l = 0
  do j = 1,ny
    y = lim(3) + step_y * (j - 1)
    do i = 1,nx
      x = lim(1) + step_x * (i - 1)
      f = 0.
      do k = 1,ndata
        if (w_dat(k).gt.0) then
          c  = cos(2. * pi * (x*u(k) + y*v(k)))
          s  = sin(2. * pi * (x*u(k) + y*v(k)))
          im = im_dat(k) * c - re_dat(k) * s
          f  = f + w_dat(k) * im**2 *1e6
        endif
      enddo
      l = l + 1
      tab(1,l) = x
      tab(2,l) = y
      tab(3,l) = f / (ndata - 2)
    enddo
  enddo
end subroutine fxy_grid
!
subroutine marquardt(ndata,u,v,re,im,w,xy,lambda,var_xy,chi_2,   &
     &    iflag)
  !---------------------------------------------------------------------
  ! Use a Method of Linearization (MARQUARDT's One) to find the Minimum of a
  ! Non Linear Function F.
  !
  !  * INPUT Parameters  :
  !
  !	NDATA	: Number of Data
  !	U	: Vector [NDATA] = Abscissas of (U,V) Plane
  !	V	: Vector [NDATA] = Ordinates of (U,V) Plane
  !	RE	: Vector [NDATA] = Real Data
  !	IM	: Vector [NDATA] = Imaginary Data
  !	W 	: Vector [NDATA] = Weights of Data
  !	XY	: Vector [2] = Starting Values of Parameters X,Y
  ! 	LAMBDA	: Starting Value of the Parameter of Convergence
  !
  !  * OUTPUT Parameters :
  !
  !	XY	: Vector [2] = Final Values of Parameters X,Y
  !	VAR_XY	: Vector [3] = Variances and Covariance of the Final Parameters
  !	CHI_2	: Value of the Normalized Error for the Final Parameters
  !	IFLAG	: Integer indicating the Accuracy of the Solutions (0 = Good)
  !---------------------------------------------------------------------
  integer :: ndata                  !
  real :: u(ndata)                  !
  real :: v(ndata)                  !
  real :: re(ndata)                 !
  real :: im(ndata)                 !
  real :: w(ndata)                  !
  real*8 :: xy(2)                   !
  real*8 :: lambda                  !
  real*8 :: var_xy(3)               !
  real :: chi_2                     !
  integer :: iflag                  !
  ! Local
  integer :: j
  real :: oldchi_2, newchi_2, epsilon
  real*8 :: par(2),determ,beta(2),alpha(2,2),norm(2,2),delta_xy(2)
  !
  ! 0. Initialisations.
  ! -------------------
  epsilon = 1e-7
  !
  par(1) = xy(1)
  par(2) = xy(2)
  delta_xy(1) = 0.
  delta_xy(2) = 0.
  !
  ! I. Compute Error CHI_2 at Starting Point.
  ! -----------------------------------------
  call chisqr(ndata,u,v,re,im,w,par,oldchi_2)
  !
  ! II. Compute Elements of BETA and ALPHA Matrices.
  ! ------------------------------------------------
  20    continue
  !!	WRITE(6,1000) 'I-UV_CENTER,  Chi-2 ',OLDCHI_2,PAR
  10    call cal_alpha_beta(ndata,u,v,re,im,w,par,lambda,   &
     &    alpha,norm,beta)
  !
  ! III. Invert Matrix ALPHA.
  ! -------------------------
  call mat_inv(alpha,2,determ)
  !
  ! IV. Compute Parameters DELTA_A(J).
  ! ----------------------------------
  call delta(beta,alpha,norm,delta_xy)
  !
  ! V. Compute the New Error NEWCHI_2.
  ! ----------------------------------
  do j = 1,2
    xy(j) = par(j) + delta_xy(j)
  enddo
  call chisqr(ndata,u,v,re,im,w,xy,newchi_2)
  !
  ! VI. Test of Convergence.
  ! ------------------------
  if (abs(newchi_2 - oldchi_2) .lt. (1.e-3 * oldchi_2)) then
    chi_2 = min (newchi_2,oldchi_2)
    if (chi_2 .le. 0.25) then
      iflag = 1
    elseif  (chi_2 .ge. 4.) then
      iflag = 2
    else
      iflag = 0
    endif
    go to 100
  elseif  (newchi_2 .lt. oldchi_2) then
    lambda = lambda / 10.
    do j = 1,2
      par(j) = xy(j)
    enddo
    oldchi_2 = newchi_2
    if (lambda .gt. epsilon) goto 20
    iflag = -1
    chi_2 = oldchi_2
    go to 100
  elseif  (lambda .ge. 1.e4) then
    iflag = -2
    chi_2 = oldchi_2
    go to 100
  else
    lambda = lambda * 10
    go to 10
  endif
  100   var_xy(1) = alpha(1,1) / norm(1,1)
  var_xy(2) = alpha(2,2) / norm(2,2)
  var_xy(3) = alpha(1,2) / norm(1,2)
  write(6,1000) 'I-UV_CENTER,  Chi-2 ',chi_2,par
  return
  1000  format(1x,a,1pg10.3,2x,1pg10.3,1x,1pg10.3)
end subroutine marquardt
!
subroutine chisqr(ndata,u,v,re,im,w,par,chi_2)
  !---------------------------------------------------------------------
  ! Compute the Normalized Error Function.
  !
  ! * INPUT Parameters  :
  !
  !	NDATA	: Number of Data
  !	U	: Vector [NDATA] = Abscissas of (U,V) Plane
  !	V	: Vector [NDATA] = Ordinates of (U,V) Plane
  !	RE	: Vector [NDATA] = Real Data
  !	IM	: Vector [NDATA] = Imaginary Data
  !	W 	: Vector [NDATA] = Weights of Data
  !	PAR	: Vector [2] = Values of Parameters X,Y
  !
  !  * OUTPUT Parameters :
  !
  !	CHI_2	: Value of the Error Function for the Parameters PAR
  !---------------------------------------------------------------------
  integer :: ndata                  !
  real :: u(ndata)                  !
  real :: v(ndata)                  !
  real :: re(ndata)                 !
  real :: im(ndata)                 !
  real :: w(ndata)                  !
  real*8 :: par(2)                  !
  real :: chi_2                     !
  ! Local
  integer :: i
  real :: f, df(2)
  real*8 :: acc
  !
  acc = 0.
  do i = 1,ndata
    if (w(i).gt.0) then
      call fonction(par,u(i),v(i),re(i),im(i),f,df)
      acc = acc + w(i) * f**2 * 1e6
    endif
  enddo
  chi_2 = acc / (ndata - 2)
end subroutine chisqr
!
subroutine cal_alpha_beta(ndata,u,v,re,im,w,par,lambda,   &
     &    alpha,norm,beta)
  !---------------------------------------------------------------------
  ! Compute Matrices ALPHA and BETA, and Values normalizing the Elements of ALPHA
  !
  ! * INPUT Parameter  :
  !
  !	NDATA	: Number of Data
  !	U	: Vector [NDATA] = Abscissas of (U,V) Plane
  !	V	: Vector [NDATA] = Ordinates of (U,V) Plane
  !	RE	: Vector [NDATA] = Real Data
  !	IM	: Vector [NDATA] = Imaginary Data
  !	W 	: Vector [NDATA] = Weights of Data
  !	PAR	: Vector [2]     = Values of Parameters X,Y
  ! 	LAMBDA	: Value of the Parameter of Convergence
  !
  !  * OUTPUT Parameters :
  !
  !	ALPHA	: Array [2 x 2] = Computed Matrix ALPHA
  ! 	NORM	: Array [2 x 2] = Computed Matrix of Normalization
  !	BETA	: Vector [2]    = Computed Matrix BETA
  !---------------------------------------------------------------------
  integer :: ndata                  !
  real :: u(ndata)                  !
  real :: v(ndata)                  !
  real :: re(ndata)                 !
  real :: im(ndata)                 !
  real :: w(ndata)                  !
  real*8 :: par(2)                  !
  real*8 :: lambda                  !
  real*8 :: alpha(2,2)              !
  real*8 :: norm(2,2)               !
  real*8 :: beta(2)                 !
  ! Local
  integer :: i, j, k
  real :: f, df(2)
  real*8 :: bet, alph
  !
  do k = 1,2
    bet = 0.
    do j = k,2
      alph = 0.
      do i = 1,ndata
        if (w(i).gt.0) then
          call fonction(par,u(i),v(i),re(i),im(i),f,df)
          alph = alph + w(i) * df(j) * df(k) *1e6
          if (j .eq. k) then
            bet = bet - w(i) * f * df(k) *1e6
          endif
        endif
      enddo
      alpha(j,k) = alph
      alpha(k,j) = alpha(j,k)
    enddo
    beta(k) = bet
  enddo
  !
  ! Normalize Elements of Matrix ALPHA.
  ! -----------------------------------
  norm(1,1) = alpha(1,1)
  norm(1,2) = sqrt(alpha(1,1) * alpha(2,2))
  norm(2,1) = norm(1,2)
  norm(2,2) = alpha(2,2)
  !
  alpha(1,2) = alpha(1,2) / norm(1,2)
  alpha(2,1) = alpha(1,2)
  alpha(1,1) = 1. + lambda
  alpha(2,2) = 1. + lambda
  return
end subroutine cal_alpha_beta
!
subroutine delta(beta,alpha,norm,delta_a)
  !---------------------------------------------------------------------
  ! Compute the New Parameters DELTA_A of the Problem.
  !
  ! * INPUT Parameters  :
  !
  !	ALPHA	: Array [2 x 2] = Matrix ALPHA
  ! 	NORM	: Array [2 x 2] = Matrix of Normalization of ALPHA
  !	BETA	: Vector [2]    = Matrix BETA
  !
  ! * OUTPUT Parameters :
  !
  !	DELTA_A	: Vector [2] = New Parameters
  !---------------------------------------------------------------------
  real*8 :: beta(2)                 !
  real*8 :: alpha(2,2)              !
  real*8 :: norm(2,2)               !
  real*8 :: delta_a(2)              !
  ! Local
  integer :: j, k
  !
  do j = 1,2
    delta_a(j) = 0.
    do k= 1,2
      delta_a(j) = delta_a(j) + beta(k) * alpha(j,k) / norm(j,k)
    enddo
  enddo
end subroutine delta
!
subroutine mat_inv(a,n,d)
  !---------------------------------------------------------------------
  ! Invert a N - Dimensionnal Symmetric Matrix A (N < 10) and calculate its
  ! Determinant D.
  !
  !  * INPUT Parameters  :
  !
  !	A	: Array [N x N] = Matrix A
  !	N	: Degree of Matrix A
  !
  !  * OUTPUT Parameters :
  !
  !	A	: Array [N x N] contains the Inverse Matrix A-1
  !	D	: Determinant of Input Matrix A
  !---------------------------------------------------------------------
  integer :: n                      !
  real*8 :: a(n,n)                  !
  real*8 :: d                       !
  ! Local
  integer :: i, j, k, l, ik(10), jk(10)
  logical :: logic
  real*8 :: amax, save_a
  !
  if (n .ge. 10) then
    go to 100
  endif
  d = 1.
  !
  do k = 1,n
    ! I. Find the Largest Element A(I,J) in Rest of Matrix A.
    ! -------------------------------------------------------
    amax = 0.
    do i = k,n
      do j = k,n
        logic = abs(a(i,j)) .gt. abs(amax)
        if (logic) then
          amax = a(i,j)
          ik(k) = i
          jk(k) = j
        endif
      enddo
    enddo
    !
    ! II. Interchange Rows and Columns to put AMAX in A(K,K).
    ! -------------------------------------------------------
    if (amax .eq. 0) then
      d = 0.
      go to 100
    endif
    i = ik(k)
    if ((i - k) .ne. 0) then
      do j = 1,n
        save_a = a(k,j)
        a(k,j) = a(i,j)
        a(i,j) = -save_a
      enddo
    endif
    j = jk(k)
    if ((j - k) .ne. 0) then
      do i = 1,n
        save_a = a(i,k)
        a(i,k) = a(i,j)
        a(i,j) = -save_a
      enddo
    endif
    !
    ! III. Compute Elements of Inverse Matrix.
    ! ----------------------------------------
    do i = 1,n
      if ((i - k) .ne. 0) then
        a(i,k) = - a(i,k) / amax
      endif
    enddo
    do i = 1,n
      do j = 1,n
        if ((i - k) .ne. 0) then
          if ((j - k) .ne. 0) then
            a(i,j) = a(i,j) + a(i,k)*a(k,j)
          endif
        endif
      enddo
    enddo
    do j = 1,n
      if ((j - k) .ne. 0) then
        a(k,j) = a(k,j) / amax
      endif
    enddo
    a(k,k) = 1. / amax
    d = d * amax
  enddo
  !
  ! IV. Restore Ordering of Matrix.
  ! -------------------------------
  do l = 1,n
    k = n - l + 1
    j = ik(k)
    if ((j - k) .gt. 0) then
      do i = 1,n
        save_a = a(i,k)
        a(i,k) = - a(i,j)
        a(i,j) = save_a
      enddo
    endif
    i = jk(k)
    if ((i - k) .gt. 0) then
      do j = 1,n
        save_a = a(k,j)
        a(k,j) = - a(i,j)
        a(i,j) = save_a
      enddo
    endif
  enddo
  100   return
end subroutine mat_inv
!
subroutine fonction(par,u,v,re,im,f,df)
  !---------------------------------------------------------------------
  ! Compute the Function F and its First Partial Derivatives.
  !
  ! * INPUT Parameters  :
  !
  !	PAR	:  Vector [2] = Values of Parameters
  !	U	:  Data U
  !	V	:  Data V
  !	RE	:  Real Data
  !	IM	:  Imaginary Data
  !
  ! * OUTPUT Parameters :
  !
  !	F	:  Value of the Function F
  !	DF	:  Vector [2] = First Partial Derivatives of F
  !---------------------------------------------------------------------
  real*8 :: par(2)                  !
  real :: u                         !
  real :: v                         !
  real :: re                        !
  real :: im                        !
  real :: f                         !
  real :: df(2)                     !
  ! Local
  real :: twopi, c, s, a
  parameter (twopi=2.0*3.1415926535897932384626)
  a = twopi*(u*par(1)+v*par(2))
  c = cos(a)
  s = sin(a)
  f = im * c - re * s
  a = im * s + re * c
  df(1) = -twopi * u * a
  df(2) = -twopi * v * a
end subroutine fonction
