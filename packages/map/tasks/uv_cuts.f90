program uv_cuts
  use image_def
  use gkernel_interfaces
!-----------------------------------------------------------
!  Compute radial visibility profiles averaged on regularly 
!  sampled angle intervals around the UV plane center.  
!  The results is output in a 4-D GDF file ordered as:
!
!  - Axis 1: Radial points;
!  - Axis 2: Azimuth angles;
!  - Axis 3: Frequency channels;
!  - Axis 4: Amplitude, phase and weight.
!
!------------------------------------------------------------
  real, external :: uv_max
  ! Local
  type(gildas) :: x,y
  character(len=filename_length) :: name, resu
  integer :: n, nt, nq, nc, ier
  logical :: error
  real :: qstep, qmax, qmin, tmin, tmax, tstep
  integer :: nvs, nvi
  !
  call gildas_open
  call gildas_char('UVTABLE$',name)
  call gildas_char('OUTIMAGE$',resu)
  call gildas_real('QMIN$',qmin,1)
  call gildas_real('QMAX$',qmax,1)
  call gildas_real('QSTEP$',qstep,1)
  call gildas_real('TMIN$',tmin,1)
  call gildas_real('TMAX$',tmax,1)
  call gildas_real('TSTEP$',tstep,1)
  call gildas_close
  !
  ! Load input UV Table (X common)
  !
  n = len_trim(name)
  if (n.eq.0) goto 999
  n = len_trim(resu)
  if (n.eq.0) goto 999
  !
  error = .false.
  write(6,*) 'I-UV_CUTS,  Loading data'
  call gildas_null (x, type= 'UVT')
  call gdf_read_gildas (x, name, '.uvt', error)
  if (error) then
    write(6,*) 'F-UV_CUTS, Cannot read input UV table'
    goto 999
  endif
  !
  ! Create New Table   (Y common)
  call gildas_null(y, type= 'IMAGE')
  call gdf_copy_header(x,y,error) 
  call sic_parsef(resu,y%file,' ','.gdf')
  call gagout('I-UV_CUTS,  Creating  result image '//   &
     &    trim(y%file) )
  nvs = x%gil%dim(1)
  nvi = x%gil%dim(2)
  if (qmax.eq.0) qmax = uv_max(x%r2d,nvs,nvi)
  !
  y%gil%ndim = 4
  !
  ! dim 1 = number of radial points in visi curve
  nq = (qmax-qmin)/qstep+1
  y%gil%dim(1) = nq
  y%gil%ref(1) = 1.0
  y%gil%val(1) = qmin
  y%gil%inc(1) = qstep
  !
  ! dim 2 = number of azimuths of visi curves
  nt = (tmax-tmin)/tstep+1
  y%gil%dim(2) = nt
  y%gil%ref(2) = 1.0
  y%gil%val(2) = tmin
  y%gil%inc(2) = tstep
  !
  ! dim 3 = number of freq. channels
  y%gil%dim(3) = x%gil%nchan
  !
  ! dim 4 = 3 (real, imag, weight)
  y%gil%dim(4) = 3
  allocate (y%r4d (y%gil%dim(1), y%gil%dim(2), y%gil%dim(3), y%gil%dim(4)), stat=ier)
  call gdf_create_image(y, error)
  if (error) then
    call gagout('F-UV_CUTS,  cannot create residuals UV table')
    goto 999
  endif
  !
  call cuts (x%r2d, nvs, nvi, x%gil%nlead,  &
     &    y%r4d, nq, y%gil%convert(:,1), nt, y%gil%convert(:,2), nc)
  !
  call gdf_write_data(y, y%r2d, error)
  if (error) goto 999
  call gdf_close_image (y,error)
  call gagout('S-UV_CUTS,  Successful completion')
  call sysexi(1)
  !
  999   call sysexi(fatale)
  !
end program uv_cuts
!
function uv_max(visx, n1, nvx)
  real :: uv_max                    !
  integer :: n1                     !
  integer :: nvx                    !
  real :: visx(n1,nvx)              !
  ! Local
  integer :: i
  !------------------------------------------------------------------------------
  uv_max = 0.0
  do i=1, nvx
    uv_max = max (uv_max,visx(1,i)**2+visx(2,i)**2)
  enddo
  uv_max = sqrt(uv_max)
end function uv_max
!
subroutine cuts (vis, n1, nv, nlead, cut, nq, qc,   &
     &    nt, tc, nc)
  integer :: n1                     !
  integer :: nv                     !
  real :: vis(n1,nv)                !
  integer :: nlead                  !
  integer :: nq                     !
  integer :: nt                     !
  integer :: nc                     !
  real ::  cut(nq, nt, nc, 3)       !
  real*8 :: qc(3)                   !
  real*8 :: tc(3)                   !
  ! Global
  real(8), parameter :: pi=3.14159265358979323846d0
  ! Local
  real ::  amp, pha
  integer :: iq, iv, it, ic
  real :: qq, tt, w
  !
  cut = 0.0
  !
  do iv=1, nv
    qq = sqrt(vis(1,iv)**2+vis(2,iv)**2)
    tt = atan2(vis(2,iv),vis(1,iv))
    tt = mod(tt+2d0*pi,pi)
    iq = nint( qc(1)+(qq-qc(2))/qc(3) )
    it = nint( tc(1)+(tt-tc(2))/tc(3) )
    if ((iq.gt.0 .and. iq.le.nq)   &
     &      .and. (it.gt.0 .and. it.le.nt)) then
      do ic=1, nc
        w = vis(3*ic+nlead,iv)
        cut(iq,it,ic,1) = cut(iq,it,ic,1) + vis(3*ic+nlead-2,iv)*w
        cut(iq,it,ic,2) = cut(iq,it,ic,2) + vis(3*ic+nlead-1,iv)*w
        cut(iq,it,ic,3) = cut(iq,it,ic,3) + w
      enddo
    endif
  enddo
  !
  do ic = 1, nc
    do it = 1, nt
      do iq = 1, nq
        w = cut(iq,it,ic,3)
        if (w.gt.0) then
          cut(iq,it,ic,1) = cut(iq,it,ic,1)/w
          cut(iq,it,ic,2) = cut(iq,it,ic,2)/w
          amp = sqrt( cut(iq,it,ic,1)**2+cut(iq,it,ic,2)**2)
          pha = atan2(cut(iq,it,ic,2),cut(iq,it,ic,1))
          cut(iq,it,ic,1) = amp
          cut(iq,it,ic,2) = pha
        endif
      enddo
    enddo
  enddo
end subroutine cuts
