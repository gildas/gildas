subroutine gtemplate_load
  !---------------------------------------------------------------------
  ! Define and load languages of the package.
  !---------------------------------------------------------------------
  ! Global
  external :: gtemplate_run
  logical, external :: gtemplate_error
  ! Local
  ! Define commands of a new language
  integer, parameter :: mcom=4
  character(len=12) :: vocab(mcom)
  data vocab /                      &
    ' DUMMY', '/OPT1', '/OPT2',     &  ! A command with 2 options
    ' VOID' /                          ! A standalone command
  !
  ! Load the new language:
  call sic_begin(           &
    'TEMPLATE',             &  ! Language name
    'GAG_HELP_GTEMPLATE',   &  !
    mcom,                   &  ! Number of commands + options in language
    vocab,                  &  ! Array of commands
    '1.0    27-Oct-2008 ',  &  ! Some version string
    gtemplate_run,          &  ! The routine which handles incoming commands
    gtemplate_error)           ! The error status routine of the library
  !
end subroutine gtemplate_load
!<FF>
subroutine gtemplate_run(line,comm,error)
  !---------------------------------------------------------------------
  ! Support routine for the language GTEMPLATE.
  ! This routine is able to call the routines associated to each
  ! command of the language GTEMPLATE
  !---------------------------------------------------------------------
  use gbl_message
  character(len=*),  intent(in)    :: line   ! Command line to send to support routines
  character(len=12), intent(in)    :: comm   ! Command resolved by the SIC interpreter
  logical,           intent(inout) :: error  ! Logical error flag
  !
  call gtemplate_message(seve%c,'GTEMPLATE',line)
  !
  ! Call appropriate subroutine according to COMM
  select case(comm)
  case('DUMMY')
    call gtemplate_dummy(line,error)
  case('VOID')
    call gtemplate_void(line,error)
  case default
    call gtemplate_message(seve%e,'GTEMPLATE_RUN','Unimplemented command '//comm)
    error = .true.
  end select
  !
end subroutine gtemplate_run
!<FF>
function gtemplate_error()
  !---------------------------------------------------------------------
  ! Support routine for the error status of the library. Does nothing
  ! here.
  !---------------------------------------------------------------------
  logical :: gtemplate_error
  !
  gtemplate_error = .false.
  !
end function gtemplate_error
