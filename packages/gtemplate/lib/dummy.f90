subroutine gtemplate_dummy(line,error)
  use gbl_message
  !---------------------------------------------------------------------
  ! Support routine for command DUMMY
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Command line
  logical,          intent(inout) :: error  ! Error flag
  ! Global
  integer, external :: sic_narg
  ! Local
  character(len=*), parameter :: rname='DUMMY'
  character(len=message_length) :: mess
  !
  call gtemplate_message(seve%i,rname,'Entering DUMMY command')
  !
  write(mess,'(I2,A)') sic_narg(0),' argument(s) passed to command'
  call gtemplate_message(seve%i,rname,mess)
  !
end subroutine gtemplate_dummy
