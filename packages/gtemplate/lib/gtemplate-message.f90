!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage GTEMPLATE messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module gtemplate_message_private
  use gpack_def
  !
  ! Identifier used for message identification
  integer :: gtemplate_message_id = gpack_global_id  ! Default value for startup message
  !
end module gtemplate_message_private
!
subroutine gtemplate_message_set_id(id)
  use gtemplate_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer, intent(in) :: id
  ! Local
  character(len=message_length) :: mess
  !
  gtemplate_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',gtemplate_message_id
  call gtemplate_message(seve%d,'gtemplate_message_set_id',mess)
  !
end subroutine gtemplate_message_set_id
!
subroutine gtemplate_message(mkind,procname,message)
  use gtemplate_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! Messaging facility for the current library. Calls the low-level
  ! (internal) messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  !
  call gmessage_write(gtemplate_message_id,mkind,procname,message)
  !
end subroutine gtemplate_message
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
