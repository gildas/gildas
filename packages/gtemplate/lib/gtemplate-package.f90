!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage the GTEMPLATE package
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine gtemplate_pack_set(pack)
  use gpack_def
  use gkernel_interfaces
  !
  type(gpack_info_t), intent(out) :: pack
  !
  external :: gtemplate_pack_init
  external :: gtemplate_pack_clean
  !
  pack%name='template'
  pack%ext='.gtemplate'
  pack%depend(1:1) = (/ locwrd(sic_pack_set) /) ! List here dependencies
  pack%init=locwrd(gtemplate_pack_init)
  pack%clean=locwrd(gtemplate_pack_clean)
  !
end subroutine gtemplate_pack_set
!
subroutine gtemplate_pack_init(gpack_id,error)
  !----------------------------------------------------------------------
  ! 
  !----------------------------------------------------------------------
  integer :: gpack_id
  logical :: error
  ! Global
  integer :: sic_setlog
  ! Local
  integer :: ier
  !
  call gtemplate_message_set_id(gpack_id)
  !
  ! Specific variables
  ier = sic_setlog('gag_help_gtemplate','gag_doc:hlp/gtemplate-help-template.hlp')
  if (ier.eq.0) then
    error=.true.
    return
  endif
  !
  ! Load local language
  call gtemplate_load
  !
  ! Specific initializations
  !
end subroutine gtemplate_pack_init
!
subroutine gtemplate_pack_clean(error)
  !----------------------------------------------------------------------
  ! Called at end of session. Might clean here for example global buffers
  ! allocated during the session
  !----------------------------------------------------------------------
  logical :: error
  !
  print *,"Called gtemplate_pack_clean"
  !
end subroutine gtemplate_pack_clean
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
