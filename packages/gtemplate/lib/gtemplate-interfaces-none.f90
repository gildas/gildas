module gtemplate_interfaces_none
  interface
    subroutine gtemplate_pack_set(pack)
      use gpack_def
      !
      type(gpack_info_t), intent(out) :: pack
    end subroutine gtemplate_pack_set
  end interface
  !
  interface
    subroutine gtemplate_pack_init(gpack_id,error)
      !----------------------------------------------------------------------
      ! 
      !----------------------------------------------------------------------
      integer :: gpack_id
      logical :: error
    end subroutine gtemplate_pack_init
  end interface
  !
  interface
    subroutine gtemplate_pack_clean(error)
      !----------------------------------------------------------------------
      ! Called at end of session. Might clean here for example global buffers
      ! allocated during the session
      !----------------------------------------------------------------------
      logical :: error
    end subroutine gtemplate_pack_clean
  end interface
  !
  interface
    subroutine gtemplate_message_set_id(id)
      use gbl_message
      !---------------------------------------------------------------------
      ! Alter library id into input id. Should be called by the library
      ! which wants to share its id with the current one.
      !---------------------------------------------------------------------
      integer, intent(in) :: id
    end subroutine gtemplate_message_set_id
  end interface
  !
  interface
    subroutine gtemplate_message(mkind,procname,message)
      use gbl_message
      !---------------------------------------------------------------------
      ! Messaging facility for the current library. Calls the low-level
      ! (internal) messaging routine with its own identifier.
      !---------------------------------------------------------------------
      integer,          intent(in) :: mkind     ! Message kind
      character(len=*), intent(in) :: procname  ! Name of calling procedure
      character(len=*), intent(in) :: message   ! Message string
    end subroutine gtemplate_message
  end interface
  !
  interface
    subroutine gtemplate_load
      !---------------------------------------------------------------------
      ! Define and load languages of the package.
      !---------------------------------------------------------------------
      ! Global
    end subroutine gtemplate_load
  end interface
  !
  interface
    subroutine gtemplate_run(line,comm,error)
      !---------------------------------------------------------------------
      ! Support routine for the language GTEMPLATE.
      ! This routine is able to call the routines associated to each
      ! command of the language GTEMPLATE
      !---------------------------------------------------------------------
      use gbl_message
      character(len=*),  intent(in)    :: line   ! Command line to send to support routines
      character(len=12), intent(in)    :: comm   ! Command resolved by the SIC interpreter
      logical,           intent(inout) :: error  ! Logical error flag
    end subroutine gtemplate_run
  end interface
  !
  interface
    function gtemplate_error()
      !---------------------------------------------------------------------
      ! Support routine for the error status of the library. Does nothing
      ! here.
      !---------------------------------------------------------------------
      logical :: gtemplate_error
    end function gtemplate_error
  end interface
  !
  interface
    subroutine gtemplate_void(line,error)
      !---------------------------------------------------------------------
      ! Does nothing more than returning asap
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Error flag
    end subroutine gtemplate_void
  end interface
  !
  interface
    subroutine gtemplate_dummy(line,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! Support routine for command DUMMY
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Error flag
    end subroutine gtemplate_dummy
  end interface
  !
end module gtemplate_interfaces_none
