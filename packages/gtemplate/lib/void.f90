subroutine gtemplate_void(line,error)
  !---------------------------------------------------------------------
  ! Does nothing more than returning asap
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Command line
  logical,          intent(inout) :: error  ! Error flag
  !
  return
  !
end subroutine gtemplate_void
