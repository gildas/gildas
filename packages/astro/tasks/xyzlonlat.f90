program geo_to_lonlat
  use gkernel_interfaces
  !ASTRO ! real(8), parameter :: a_earth =6367435d0      ! earth radius in meters
  real(8), parameter :: bovera=0.99664719d+00        ! earth b/a
  real(8), parameter :: a_earth=6378140d+00          ! (m) earth radius
  real(8), parameter :: flatten=1.d0-bovera          ! Earth flattening
  real(8), parameter :: pi=3.14159265358979323846d0
  !
  real(8) :: xyz(3)
  real(8) :: plh(3)
  real(8) :: lon,lat,alt
  character(len=14) :: clon,clat
  integer :: ixyz(3)
  logical :: error
  !
  call gildas_open
  call gildas_dble('XYZ$',xyz,3)
  call gildas_char('LONGITUDE$',clon)
  call gildas_char('LATITUDE$',clat)
  call gildas_dble('ALTITUDE$',alt,1)
  call gildas_close
  !
  if (any(xyz.ne.0.d0)) then
    call gwcs_xyz2lla(xyz,plh, a_earth, flatten)
    if (plh(1).gt.pi) then
      plh(1) = 2.d0*pi-plh(1)
      call sexag(clon,plh(1),360)
      Print *,'Longitude ',clon,' West'
    else
      call sexag(clon,plh(1),360)
      Print *,'Longitude ',clon,' East'
    endif
    call sexag(clat,plh(2),360)
    Print *,'Latitude ',clat
    Print *,'Altitude ',plh(3)
  else
    call sic_decode(clon,lon,360,error)
    call sic_decode(clat,lat,360,error)
    plh(1) = lon
    plh(2) = lat
    plh(3) = alt
    call gwcs_lla2xyz(plh, xyz, a_earth, flatten)
    ixyz = nint(xyz)
    Print *,'XYZ ',ixyz
  endif
  !
end program geo_to_lonlat
!
