!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! J.Pety 2004-2014 on an original ideas of F.Gueth
!
! This task is very general on purpose.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
program inter_time_estimator
  !
  use phys_const
  use gkernel_interfaces
  !
  real(4) :: bandwidth,ontime,tsys,anteff,decorrel,freq,rms
  real(4) :: beam(2),lambda,Trms,jyperk,velres
  integer :: npol,nant,nbas
  !
  call gildas_open
  call gildas_real('FREQ$',freq,1)           ! in GHz
  call gildas_real('BANDWIDTH$',bandwidth,1) ! in MHz
  call gildas_inte('NPOL$',npol,1)           !
  call gildas_real('TSYS$',tsys,1)           ! in K
  call gildas_real('DECORREL$',decorrel,1)   !
  call gildas_real('ONTIME$',ontime,1)       ! in hrs
  call gildas_inte('NANT$',nant,1)           !
  call gildas_real('ANTEFF$',anteff,1)       ! 
  call gildas_real('BEAM$',beam,2)           ! in arcsec
  call gildas_close
  !
  velres = 1e-6*(clight*bandwidth/freq) ! in km/s
  lambda = 1e-6*clight/freq             ! in mm
  !
  write (*,'(/,'' Interferometer Sensitivity '')')
  write (*,'( '' --------------------------- '',/)')
  write (*,'( '' Frequency:                    '',f9.3,'' GHz'')') freq
  write (*,'( '' wavelength:                   '',f9.3,'' mm'')') lambda
  write (*,'(/'' Number of polarizations:      '',i5)') npol
  write (*,'( '' Frequency resolution:         '',f9.3,'' MHz'')') bandwidth
  write (*,'( '' Velocity resolution:          '',f9.3,'' km/s'')') velres 
  write (*,'(/'' Tsys:                         '',f9.1,'' K'')') tsys
  write (*,'( '' Decorrelation coefficient:    '',f9.1)') decorrel
  write (*,'( '' On-source integration time:   '',f9.1,'' hrs'')') ontime
  write (*,'(/'' Number of available antennas: '',i5)') nant
  write (*,'( '' Antenna efficiency:           '',f9.1,'' Jy/K '')') anteff
  write (*,'( '' Beam:                         '',f3.1,'' x '',f3.1,'' arcsec'',/)') beam
  !
  bandwidth = bandwidth*1e6   ! in Hz
  freq = freq*1e9             ! in Hz
  ontime = ontime*3600.       ! in s
  beam = beam*rad_per_sec     ! in rad
  nbas = nant*(nant-1)        ! number of baselines
  !
  ! In point source sensitivity, everything except the beam counts while
  ! sensitivity expressed in main beam temperature obviously needs the beam
  ! information.
  rms = anteff*tsys/(decorrel*sqrt(bandwidth*ontime*nbas*npol))
  jyperk = 2.*(kbolt*1e32)*pi*beam(1)*beam(2)/4./log(2.0)/lambda**2
  jyperk = 1./jyperk
  Trms = jyperk*rms
  !
  write (*,'(/'' Conversion factor:            '',f9.1,'' K[Tmb] per Jy/beam'')') jyperk
  if ((rms.lt.1e-1).and.(rms.ge.1e-3)) then
     write (*,'( '' Point source sensitivity:     '',f9.1,'' mJy'')') rms*1e3
  else if ((rms.lt.1e-3).and.(rms.ge.1e-6)) then
     write (*,'( '' Point source sensitivity:     '',f9.1,'' microJy'')') rms*1e6
  else
     write (*,'( '' Point source sensitivity:     '',f9.1,'' Jy'')') rms
  endif
  if ((Trms.lt.1e-1).and.(Trms.ge.1e-3)) then
     write (*,'( '' rms brightness temperature:   '',f9.1,'' mK[Tmb]'',/)') Trms*1e3
  else if ((Trms.lt.1e-3).and.(Trms.ge.1e-6)) then
     write (*,'( '' rms brightness temperature:   '',f9.1,'' microK[Tmb]'',/)') Trms*1e6
  else
     write (*,'( '' rms brightness temperature:   '',f9.1,'' K[Tmb]'',/)') Trms
  endif
  write (*,'( '' ---------------------------   '',/)')
  !
end program inter_time_estimator
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
