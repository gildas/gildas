
subroutine alma_plot_line
  use gbl_message
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  ! Main entry for plotting ALMA Frequency setup
  !---------------------------------------------------------------------
  character(len=*), parameter :: rname='ALMA_PLOT'
  integer(kind=4) :: j, ibb, nch
  real(kind=4) :: df, dv, tmp, rffromif, cfreq
  character(len=132) :: mess
  !
  if (plot_mode.eq.10) then
     call astro_message(seve%i,rname,'Plotting mode: ALMA FREQUENCY coverage')
     call alma_plot_dsb
  elseif (plot_mode.gt.10 .and. plot_mode.lt.15) then
     ibb = plot_mode-10
     if (.not.bb_def(ibb)) then
        call astro_message(seve%e,rname,'Baseband not defined')
        return
     endif
     write(mess,'(a,i1)') 'Plotting mode: ALMA BASEBAND ', ibb
     call astro_message(seve%i,rname,mess)
     call alma_plot_baseband(ibb)
     !
     ! List the current spectral window
     !
     if (bb_nspw(ibb).ne.0) then
        write(mess,'(a,i1)') 'Current spectral windows for baseband ',ibb
        call astro_message(seve%i,rname,mess)

        call astro_message(seve%r,rname,  &
          'Spw   Width   IFcent     RFcent           RFlimits     Npol Nchan    Resolutions')
        call astro_message(seve%r,rname,  &
          '       MHz      MHz        GHz              GHz                     kHz      km/s')
  1000 format(i2,2x,f7.2,2x,f8.2,2x,f10.5,2x,f10.5,a1,f9.5,2x,i1,2x,i4,2x,f7.3,2x,f7.3)
        do j = 1,bb_nspw(ibb)
	         cfreq = spw_cfreq(ibb,j)+bb_cfreq(ibb)
	         nch = 8192/spw_polar(ibb,j)* spw_use(ibb,j) / 100 ! 15/16 counted separately below
	         df = spw_width(ibb,j)/nch*1000.  ! freq. resolution, in kHz
           nch = nch*15/16
           dv = df/1000./flo1*299792.458  ! vel. resolution, computed with FLO1
           tmp = rffromif(cfreq,bb_sideband(ibb))/1000.
           write(mess,1000) j, spw_width(ibb,j)*(15./16.), cfreq, tmp, &
                tmp-(spw_width(ibb,j)*(15./16.)/2000.),'-',tmp+(spw_width(ibb,j)*(15./16.)/2000.), &
                spw_polar(ibb,j), nch, df, dv  ! spw_use(ibb,j)
           call astro_message(seve%r,rname,mess)
        enddo
     else
        write(mess,'(a,i1)') 'No spectral window defined for baseband ',ibb
        call astro_message(seve%i,rname,mess)
     endif
  else
    call astro_message(seve%e,rname,'Initialization problem')
    return
  endif
end subroutine alma_plot_line
!
subroutine alma_plot_dsb
  use gbl_message
  use ast_line
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  ! Plot of USB + LSB, as produced by the LINE command
  !---------------------------------------------------------------------
  real(kind=8) :: bandwidth, center
  integer(kind=4) :: i, ib
  real(kind=8) :: f1, f2, u1, u2
  character(len=132) :: chain, limi
  ! --------------------------------------------------------------------
  !
  call gr_exec('CLEAR DIRECTORY')
  !
  ! IF1 bandwidth
  !
  bandwidth = abs(userlim(2)-userlim(1))/1000.
  center = userlim(1)+(userlim(2)-userlim(1))/2.
  !
  ! LSB plot -----------------------------------------------------------
  !
  call gr_exec1('SET ORIEN 0')
  call gr_exec1('TICK 0 0 0 0')
  call gr_exec1('PEN 0')
  call gr_exec1('SET BOX 3 29 11.5 17')
  !
  ! Frequency boudaries
  !
  f1 = (flo1-center)/1000.d0/fshift+bandwidth/2.
  f2 = f1-bandwidth
  u1 = (flo1+center)/1000.d0/fshift-bandwidth/2.
  u2 = u1+bandwidth
  !
  ! RF axis
  !
  write(chain,1040) f2,f1
  call gr_exec1(chain)
  call gr_exec1('AXIS XU /TICK IN /LABEL P')
  call gr_exec1('DRAW TEXT 0 1 "Rest frequency (GHz)    LSB" /BOX 8')
  !
  ! Plot atmospheric transmission
  !
  if (do_atmplot) then
    ! call alma_receiver(f1,u1)
    call pdbi_line_atmos(f1,f2,u1,u2,plotwater,100)
  endif
  !
  ! Plot molecular lines
  !
  if (nmol.ge.1) then
    call pdbi_line_molecules(f1,f2)
  endif
  !
  ! Indicate requested tuning frequency
  !
  if (sky.eq.-1) then
    call gr_exec1('PEN 5')
    write(chain,1090) freq, 0.0 ! Gfortran is touchy
    call gr_exec1(chain)
    write(chain,1100) freq, 1.5
    call gr_exec1(chain)
    call gr_exec1('PEN 0')
  endif
  !
  ! IF1 limtis (are FLIPPED wrt RF in the LSB)
  !
  write(limi,1040) userlim(2), userlim(1)
  !
  ! IF1 axis
  !
  call gr_exec1(limi)
  call gr_exec1('AXIS XL')
  call gr_exec1('DRAW TEXT 0 -1.5 "Intermediate frequency IF1 (MHz)" 5 /BOX 2')
  !
  ! Y axis (dash on user limits, then continuous on hard limits)
  !
  call gr_exec1('PEN /DASH 3')
  write (chain,1050) userlim(1)
  call gr_exec1(chain)
  write (chain,1060) userlim(1)
  call gr_exec1(chain)
  write (chain,1050) userlim(2)
  call gr_exec1(chain)
  write (chain,1060) userlim(2)
  call gr_exec1(chain)
  call gr_exec1('PEN /DASH 1')
  write (chain,1050) iflim(1)
  call gr_exec1(chain)
  write (chain,1060) iflim(1)
  call gr_exec1(chain)
  write (chain,1050) iflim(2)
  call gr_exec1(chain)
  write (chain,1060) iflim(2)
  call gr_exec1(chain)
  !
  ! Plot basebands
  !
  call alma_plot_dsb_corr(-1)
  !
  ! Label
  !
  ib = len_trim(label)
  do i = 1,len_trim(label)
    if (label(i:i).eq.'[') ib=i-1
  enddo
  call gr_exec1('PEN 1 /WEIGHT 3')
  call gr_exec1('DRAW TEXT 0 3 "'//label(1:12)//'" 6 /BOX 7')
  call gr_exec1('PEN 0')
  call gr_exec1('DRAW TEXT 0 3 "'//label(13:ib)//'" 5 /BOX 8')
  call gr_exec1('DRAW TEXT 0 3 "'//label(ib:len_trim(label))//'" 4 /BOX 9')
  !
  ! USB plot -----------------------------------------------------------
  !
  call gr_exec1('SET ORIEN 0')
  call gr_exec1('PEN 0')
  call gr_exec1('SET BOX 3 29 3 8.5')
  !
  ! Frequency boundaries
  !
  f1 = (flo1+center)/1000.d0/fshift-bandwidth/2.
  f2 = f1+bandwidth
  u1 = (flo1-center)/1000.d0/fshift+bandwidth/2.
  u2 = u1-bandwidth
  !
  ! RF axis
  !
  write(chain,1040) f1,f2
  call gr_exec1(chain)
  call gr_exec1('AXIS XL /TICK IN /LABEL P')
  call gr_exec1('DRAW TEXT 0 -1 "Rest frequency (GHz)    USB" /BOX 2')
  !
  ! Plot atmospheric transmission
  !
  if (do_atmplot) then
    call pdbi_line_atmos(f1,f2,u1,u2,plotwater,100)
  endif
  !
  ! Plot molecular lines
  !
  if (nmol.ge.1) then
    call pdbi_line_molecules(f1,f2)
  endif
  !
  ! Indicate requested tuning frequency
  !
  if (sky.eq.1) then
    call gr_exec1('PEN 5')
    write(chain,1090) freq, 0.0
    call gr_exec1(chain)
    write(chain,1100) freq, 1.5
    call gr_exec1(chain)
    call gr_exec1('PEN 0')
  endif
  !
  ! IF1 limtis
  !
  write(limi,1040) userlim(1), userlim(2)
  !
  ! IF axis
  !
  call gr_exec1(limi)
  call gr_exec1('AXIS XU /TICK IN /LABEL P')
  !
  ! Y axis (dash on user limits, then continuous on hard limits)
  !
  call gr_exec1('PEN /DASH 3')
  write (chain,1050) userlim(1)
  call gr_exec1(chain)
  write (chain,1060) userlim(1)
  call gr_exec1(chain)
  write (chain,1050) userlim(2)
  call gr_exec1(chain)
  write (chain,1060) userlim(2)
  call gr_exec1(chain)
  call gr_exec1('PEN /DASH 1')
  write (chain,1050) iflim(1)
  call gr_exec1(chain)
  write (chain,1060) iflim(1)
  call gr_exec1(chain)
  write (chain,1050) iflim(2)
  call gr_exec1(chain)
  write (chain,1060) iflim(2)
  call gr_exec1(chain)
  !
  ! Plot basebands
  !
  call alma_plot_dsb_corr(1)
  !
  call gr_exec1('PEN 0')
  !
  ! End -------------------------------------------------------------------
  !
  1040 format('LIMITS ',1pg25.16,1x,1pg25.16,' 0 5')
  1050 format('DRAW RELOCATE ',f10.2,' 0 /USER')
  1060 format('DRAW LINE ',f10.2,' 5 /USER')
  1090 format('DRAW RELOCATE ',f10.2,' ',f10.2,' /USER')
  1100 format('DRAW LINE ',f10.2,' ',f10.2,' /USER')
  !
end subroutine alma_plot_dsb
!
! ----------------------------------------------------------------------
!
subroutine alma_plot_dsb_corr(sideband)
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  ! Plot the correlator base bands onto the DSB plot
  !---------------------------------------------------------------------
  integer(kind=4) :: sideband       !
  ! Local
  integer(kind=4) :: i
  real(kind=4) :: yoff
  character(len=256) :: chain
  !
  ! Display four possible basebands
  !
  do i = 1,4
    yoff = 0.1+0.4*i
    if ((bb_def(i)).and.(bb_sideband(i).eq.sideband)) then
      write (chain,1003) i, 1
      if (i.eq.4) write (chain,1003) 6, 1   ! nicer color
      call gr_exec1(chain)
      if (recband.eq.9) then
        call gr_exec1('SET EXPAND 0.7')
      endif
      write(chain,1000) bb_cfreq(i), yoff, i
      call gr_exec1(chain)
      if (recband.eq.9) then
        call gr_exec1('SET EXPAND 1.0')
      endif
      write (chain,1003) i, 3
      if (i.eq.4) write (chain,1003) 6, 3
      call gr_exec1(chain)
      write(chain,1001) bb_cfreq(i)-250, yoff
      call gr_exec1(chain)
      write(chain,1002) bb_cfreq(i)-1000, yoff
      call gr_exec1(chain)
      write(chain,1001) bb_cfreq(i)+250, yoff
      call gr_exec1(chain)
      write(chain,1002) bb_cfreq(i)+1000, yoff
      call gr_exec1(chain)
      !
      ! Overlay the spectral windows
      !
      call alma_plot_spectral(i,0)
    endif
  enddo
  call gr_exec1('PEN 0')
1000 format('DRAW TEXT ',f10.2,f10.2,' "BBAND ',i1,'" /USER')
1001 format('DRAW RELOCATE ',f10.2,f10.2,' /USER')
1002 format('DRAW ARROW ',f10.2,f10.2,' /USER /CLIP')
1003 format('PEN ',i2,' /DASH 1 /WEIGHT ',i2)
end subroutine alma_plot_dsb_corr
!
! ----------------------------------------------------------------------
!
subroutine alma_plot_dsb_corr_tiny(sideband,ibb)
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  ! Plot the correlator basebands onto the DSB plot
  ! sideband = +1 if USB, -1 is LSB
  !---------------------------------------------------------------------
  integer(kind=4) :: sideband       !
  integer(kind=4) :: ibb            !
  ! Local
  integer(kind=4) :: i
  real(kind=4) :: yoff
  character(len=256) :: chain
  !
  ! Display four possible basebands
  !
  do i = 1,4
    yoff = 0.3+0.4*i
    if ((bb_def(i)).and.(bb_sideband(i).eq.sideband)) then
      write (chain,1003) i, 1
      if (i.eq.4) write (chain,1003) 6, 1   ! nicer color
      call gr_exec1(chain)
      call gr_exec1('SET EXPAND 0.5')
      write(chain,1000) bb_cfreq(i), yoff, i
      call gr_exec1(chain)
      call gr_exec1('SET EXPAND 1')
      ! call gr_exec1('PEN 1 /WEIGHT 3')
      write(chain,1001) bb_cfreq(i)-250, yoff
      call gr_exec1(chain)
      write(chain,1002) bb_cfreq(i)-1000, yoff
      call gr_exec1(chain)
      write(chain,1001) bb_cfreq(i)+250, yoff
      call gr_exec1(chain)
      write(chain,1002) bb_cfreq(i)+1000, yoff
      call gr_exec1(chain)
      !
      ! Overlay the spectral windows
      !
      call alma_plot_spectral(i,0)
      !
      ! That baseband is plotted with PLOT BASE command
      !
      if (i.eq.ibb) then
        write (chain,1003) i, 1
        if (i.eq.4) write (chain,1003) 6, 1   ! nicer color
        call gr_exec1(chain)
        write(chain,1001) bb_cfreq(i)-1000, yoff
        call gr_exec1(chain)
        if (sideband.eq.1) then ! USB
          call gr_exec1('DRAW LINE -14 -3 /BOX 1')
        else
          call gr_exec1('DRAW LINE  14 -3 /BOX 3')
        endif
        write(chain,1001) bb_cfreq(i)+1000, yoff
        call gr_exec1(chain)
        if (sideband.eq.1) then ! USB
          call gr_exec1('DRAW LINE 0 -3 /BOX 3')
        else
          call gr_exec1('DRAW LINE 0 -3 /BOX 1')
        endif
      endif
    endif
  enddo
  call gr_exec1('PEN 0')
  1000 format('DRAW TEXT ',f10.2,f10.2,' "BBAND ',i1,'" /USER')
  1001 format('DRAW RELOCATE ',f10.2,f10.2,' /USER')
  1002 format('DRAW ARROW ',f10.2,f10.2,' /USER /CLIP')
  1003 format('PEN ',i2,' /DASH 1 /WEIGHT ',i2)
end subroutine alma_plot_dsb_corr_tiny
!
! -----------------------------------------------------------------------
!
subroutine alma_plot_dsb_tiny(ibb)
  use gbl_message
  use ast_line
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  ! Tiny plot of USB + LSB
  !---------------------------------------------------------------------
  integer(kind=4) :: ibb            !
  ! Local
  real(kind=8) :: bandwidth, center
  real(kind=8) :: f1, f2, u1, u2
  character(len=132) :: chain, limi
  ! --------------------------------------------------------------------
  !
  ! IF1 bandwidth
  !
  bandwidth = abs(userlim(2)-userlim(1))/1000.
  center = userlim(1)+(userlim(2)-userlim(1))/2
  !
  ! LSB plot -----------------------------------------------------------
  !
  call gr_exec1('SET ORIEN 0')
  call gr_exec1('TICK 0 0 0 0')
  call gr_exec1('PEN 0')
  call gr_exec1('SET BOX 3 15 13 16')
  !
  ! LSB frequency boundaries
  !
  f1 = (flo1-center)/1000.d0/fshift+bandwidth/2.
  f2 = f1-bandwidth
  u1 = (flo1+center)/1000.d0/fshift+bandwidth/2.
  u2 = u1+bandwidth
  !
  ! RF axis
  !
  write(chain,1040) f2,f1
  call gr_exec1(chain)
  call gr_exec1('AXIS XL /TICK IN /LABEL NO')
  call gr_exec1('AXIS XU /TICK IN /LABEL P')
  call gr_exec1('DRAW TEXT 0 1 "Rest frequency (GHz) - LSB" /BOX 8')
  !
  ! Plot atmospheric transmission
  !
  if (do_atmplot) then
	  print *,'From 3'
    call pdbi_line_atmos(f1,f2,u1,u2,plotwater,50)
  endif
  call gr_exec1('BOX N N N')
  !
  ! Plot molecular lines
  !
  if (nmol.ge.1) then
     call gr_exec1('SET EXPAND 0.5')
     call pdbi_line_molecules(f1,f2)
     call gr_exec1('SET EXPAND 1')
  endif
  !
  ! Indicate requested tuning frequency
  !
  call gr_exec1('PEN 5')
  write(chain,1090) freq, 0.0
  call gr_exec1(chain)
  write(chain,1100) freq, 1.5
  call gr_exec1(chain)
  call gr_exec1('PEN 0')
  !
  ! IF1 limtis (are FLIPPED wrt RF in the LSB)
  !
  write(limi,1040) userlim(2), userlim(1)
  !
  ! Plot basebands
  !
  call gr_exec1(limi) ! must be in IF frame
  call alma_plot_dsb_corr_tiny(-1,ibb)
  !
  ! USB plot -----------------------------------------------------------
  !
  call gr_exec1('SET ORIEN 0')
  call gr_exec1('PEN 0')
  call gr_exec1('SET BOX 17 29 13 16')
  !
  ! USB frequency boudaries
  !
  f1 = (flo1+center)/1000.d0/fshift-bandwidth/2.
  f2 = f1+bandwidth
  u1 = (flo1-center)/1000.d0/fshift+bandwidth/2.
  u2 = u1-bandwidth
  !
  ! RF axis
  !
  write(chain,1040) f1,f2
  call gr_exec1(chain)
  call gr_exec1('AXIS XL /TICK IN /LABEL NO')
  call gr_exec1('AXIS XU /TICK IN /LABEL P')
  call gr_exec1('DRAW TEXT 0 1 "Rest frequency (GHz) - USB" /BOX 8')
  !
  ! Plot atmospheric transmission
  !
  if (do_atmplot) then
	  print *,'From 4'
    call pdbi_line_atmos(f1,f2,u1,u2,plotwater,50)
  endif
  call gr_exec1('BOX N N N')
  !
  ! Plot molecular lines
  !
  if (nmol.ge.1) then
     call gr_exec1('SET EXPAND 0.5')
     call pdbi_line_molecules(f1,f2)
     call gr_exec1('SET EXPAND 1')
  endif
  !
  ! IF1 limtis
  !
  write(limi,1040) userlim(1), userlim(2)
  !
  ! Plot basebands
  !
  call gr_exec1(limi) ! must be in IF frame
  call alma_plot_dsb_corr_tiny(1,ibb)
  !
  ! End -------------------------------------------------------------------
  !
  1040 format('LIMITS ',1pg25.16,1x,1pg25.16,' 0 5')
  1090 format('DRAW RELOCATE ',f10.2,' ',f10.2,' /USER')
  1100 format('DRAW LINE ',f10.2,' ',f10.2,' /USER')
  !
end subroutine alma_plot_dsb_tiny
!
! -----------------------------------------------------------------------
!
subroutine alma_plot_baseband(ibb)
  use gbl_message
  use ast_line
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  ! Plot of the BASEBANDs
  !---------------------------------------------------------------------
  integer(kind=4) :: ibb            !
  ! Global
  logical :: gr_error
  ! Local
  integer(kind=4) :: i, ib
  real(kind=8) :: bandwidth, center, ifmin, ifmax, tmp
  real(kind=8) :: f1, f2, u1, u2
  real(kind=4) :: rffromif
  character(len=3) :: band
  character(len=132) :: chain, limi
  !
  character(len=6) :: input1(4), input2(4)
  data input1/'Q1 HOR','Q2 HOR','Q3 VER','Q4 VER'/
  data input2/'Q1 VER','Q2 VER','Q3 HOR','Q4 HOR'/
  real(kind=4) :: qlim(2,4)
  data qlim/4200,5200,5000,6000,6000,7000,6800,7800/
  !
  ! ------------------------------------------------------------------------
  !
  call gr_exec('CLEAR DIRECTORY')
  call gr_exec1('PEN 0')
  !
  ! Plot the  full receiver frequency coverage
  !
  call alma_plot_dsb_tiny(ibb)
  !
  ! Labels
  !
  call gr_exec1('SET BOX 3 29 13 16')
  ib = len_trim(label)
  do i = 1,len_trim(label)
    if (label(i:i).eq.'[') ib=i-1
  enddo
  call gr_exec1('PEN 1 /WEIGHT 3')
  call gr_exec1('DRAW TEXT 0 4 "'//label(1:12)//'" 6 /BOX 7')
  call gr_exec1('PEN 0 /WEIGHT 1')
  call gr_exec1('DRAW TEXT 0 4 "'//label(13:ib)//'" 5 /BOX 8')
  call gr_exec1('DRAW TEXT 0 4 "'//label(ib+1:len_trim(label))//'" 4 /BOX 9')
  !
  tmp = rffromif(bb_cfreq(ibb),bb_sideband(ibb))/1000.
  if (bb_sideband(ibb).eq.1) then
    write(chain,1100) ibb, bb_cfreq(ibb), 'USB' , tmp
  else
    write(chain,1100) ibb, bb_cfreq(ibb), 'LSB' , tmp
  endif
  call gr_exec1('DRAW TEXT 0 3 "'//chain(1:len_trim(chain))//'" 6 /BOX 7')
  call gr_exec1('PEN 1 /WEIGHT 3')
  call gr_exec1('DRAW TEXT 0 3 "'//chain(1:10)//'" 6 /BOX 7')
  call gr_exec1('PEN 0')
  !
  ! IF1 limits
  !
  ifmin = bb_cfreq(ibb)-1000.
  ifmax = bb_cfreq(ibb)+1000.
  bandwidth = abs(ifmax-ifmin)/1000.
  center = bb_cfreq(ibb)
  !
  ! Box
  !
  call gr_exec1('SET ORIEN 0')
  call gr_exec1('TICK 0 0 0 0')
  call gr_exec1('SET BOX 3 29 2 10')
  call gr_exec1('BOX N N N')
  if (gr_error()) return
  !
  ! RF axis
  !
  if (bb_sideband(ibb).eq.1) then    ! USB
    f1 = (flo1+center)/1000.d0/fshift-bandwidth/2.
    f2 = f1+bandwidth
    u1 = (flo1-center)/1000.d0/fshift+bandwidth/2.
    u2 = u1-bandwidth
    band = "USB"
  else                         ! LSB
    f2 = (flo1-center)/1000.d0/fshift+bandwidth/2.
    f1 = f2-bandwidth
 !   f1 = (flo1-center)/1000.d0/fshift+bandwidth/2.
 !   f2 = f1-bandwidth
    u1 = (flo1+center)/1000.d0/fshift+bandwidth/2.
    u2 = u1+bandwidth
    band = "LSB"
  endif
  write(chain,1040) f1,f2
  call gr_exec1(chain)
  call gr_exec1('AXIS XU /TICK IN /LABEL P')
  call gr_exec1('PEN 0 /DASH 1')
  call gr_exec1('DRAW TEXT 0  1 "Rest frequency (GHz) - '//band //'" /BOX 8')
  !
  ! Plot atmospheric transmission
  !
  if (do_atmplot) then
	  print *,'From 5'
    call pdbi_line_atmos(f1,f2,u1,u2,plotwater,200)
    call gr_exec1('BOX N N N')
  endif
  !
  ! Plot molecular lines
  !
  if (nmol.ge.1) then
    call pdbi_line_molecules(f1,f2)
  endif
  !
  ! IF axis
  !
  if (bb_sideband(ibb).eq.1) then  ! USB
    write(limi,1040) ifmin, ifmax
  else  ! LSB
    write(limi,1040) ifmax, ifmin
  endif
  call gr_exec1(limi)
  call gr_exec1('AXIS XL /TICK IN /LABEL P')
  call gr_exec1('DRAW TEXT 0 -1.5 "Intermediate frequency (MHz)" 5 /BOX 2')
  !
  ! Spectral units
  !
  call gr_exec1('PEN 1 /WEIGHT 1')
  call alma_plot_spectral(ibb, 0)
  call gr_exec1('PEN 0')
  !
  ! End -------------------------------------------------------------------
  !
  1040 format('LIMITS ',1pg25.16,1x,1pg25.16,' 0 5')
  1100 format('BASEBAND ',i1,' is centered at IF1 = ',f8.2,' MHz (',a3,')   RF = ',f10.5,' GHz')
  !
end subroutine alma_plot_baseband
