subroutine astro_source(line,error)
  use gildas_def
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>astro_source
  use ast_astro
  use ast_constant
  use my_receiver_globals
  !---------------------------------------------------------------------
  ! @ private
  !  Support routine for ASTRO Command
  !    SOURCE [name]
  ! 1    [/DRAW MARKER|LINE|SYMBOL|FULL|BOX]
  ! 2    [/ALTERNATE]
  ! 3    [/FLUX Fmin Fmax]
  ! 4    [/SUN]
  ! 5    [/PRINT]
  ! 6    [/CURSOR]
  ! 7    [/QUIET]
  ! 8    [/VARIABLE VarName]
  ! 9    [/NOCLIP] plotting option
  ! 10   [/RESET] to put all source related values to 0
  ! 11   [/DOPPLER] to define a LSR or z to be taken into account in freq
  !                    computations, without any source defined
  !  Search the source catalog for source 'name'
  !  Parse the line found for coords and velocities
  !  Convert coordinates to internal format
  !---------------------------------------------------------------------
  character(len=*):: line           !
  logical :: error                  !
  ! Local
  character(len=*), parameter :: rname='SOURCE'
  character(len=2) :: coord   ! coordinate system ('EQ','GA','HO'...)
  character(len=2) :: ftype,vtype   ! flux type ('FL' or 'MA') and vel type
  real(kind=4) :: equinox          ! Equinox
  real(kind=4) :: flux             ! flux (Jy) or Magnitude
  real(kind=4) :: spindex          ! spectral index
  real(kind=4) :: mag(9)           ! Magnitudes in bands
  real(kind=4) :: fmin, fmax       ! Min - Max plotted flux or mag.
  real(kind=8) :: lambda, beta      ! coord (rad)
  real(kind=8) :: s_3(3)
  real(kind=4) :: sun_limit
  character(len=sourcename_length) :: name
  logical :: draw,drawclip,lunopened,dodoppler
  integer(kind=4) :: nlu, nlx, npar, lpar, lun, icat, ier, nc, ismin, i, j
  character(len=256) :: c_line, x_line, par
  logical :: found, print, do_print, alternate,doreset,doflux,docursor,dovar,dosun,doquiet,noclip
  character(len=1) :: code
  real(kind=8) :: xu, yu, dsmin, dd, cxu, cyu, sxu, syu, cxs, cys, sys, sxs,velocity,redshift
  real(kind=4) :: x4, y4
  character(len=filename_length) :: catalog_saved
  integer(kind=4) :: mcalib, ncalib
  parameter (mcalib=100)
  character(len=20) :: ccalib(mcalib)
  real(kind=4) :: dsplot(msplot), dcalib(mcalib), fcalib(mcalib)
  integer(kind=4) :: ind(msplot), is(mcalib)
  character(len=1) :: drawargs(5)  ! Support for /DRAW arguments
  character(len=varname_length) :: varname
  integer(kind=4), parameter :: optdraw =1  ! /DRAW
  integer(kind=4), parameter :: optalter=2
  integer(kind=4), parameter :: optflux =3
  integer(kind=4), parameter :: optsun  =4
  integer(kind=4), parameter :: optprint=5
  integer(kind=4), parameter :: optcurso=6
  integer(kind=4), parameter :: optquiet=7
  integer(kind=4), parameter :: optvaria=8
  integer(kind=4), parameter :: optnoclip=9
  integer(kind=4), parameter :: optreset=10
  integer(kind=4), parameter :: optdoppler=11
  !
  save ncalib, ccalib, fcalib, dcalib
  !------------------------------------------------------------------------
  ! Set some logicals
  alternate=sic_present(optalter,0)
  draw=sic_present(optdraw,0)
  doflux=sic_present(optflux,0)
  dosun=sic_present(optsun,0)
  print = sic_present(optprint,0)
  docursor=sic_present(optcurso,0)
  doquiet=sic_present(optquiet,0)
  dovar=sic_present(optvaria,0)
  noclip=sic_present(optnoclip,0)
  doreset=sic_present(optreset,0)
  dodoppler=sic_present(optdoppler,0)
  !
  if (doreset) then
    if (alternate.or.doflux.or.docursor.or.draw.or.dovar) then
      call astro_message(seve%e,rname,'/RESET must be used alone')
    endif
    if (sic_narg(0).gt.0) then
      call astro_message(seve%e,rname,'No argument accepted with option /RESET')
      error=.true.
      return
    endif
  endif
  !Code
  if (alternate) then
    icat = 2
  else
    icat = 1
  endif
  found = .false.
  !
  ! Set /FLUX option
  if (doflux) then
    call sic_r4 (line,optflux,1,fmin,.true.,error)
    if (error) return
    fmax = 1.0e38
    call sic_r4 (line,optflux,2,fmax,.false.,error)
    if (error) return
  else
    fmin = -100.               !enables some star magnitudes also
    fmax = 1.0e38
  endif
  !
  ! SOURCE /CURSOR (opt 6)
  catalog_saved = catalog_name(1)
  if (docursor) then
    if (nsplot.le.0) then
      call astro_message(seve%e,'SOURCE','No source in current plot')
      error = .true.
      return
    endif
    call gr_curs (xu, yu, x4, y4, code)
    dsmin = 1e10
    ismin = 0
    cxu = cos(xu)
    cyu = cos(yu)
    sxu = sin(xu)
    syu = sin(yu)
    do i=1, nsplot
      cxs = cos(xsplot(i))
      cys = cos(ysplot(i))
      sxs = sin(xsplot(i))
      sys = sin(ysplot(i))
      dd = (cxu*cyu-cxs*cys)**2 + (sxu*cyu-sxs*cys)**2 + (syu-sys)**2
      if (dd.lt.dsmin) then
        ismin = i
        dsmin = dd
      endif
    enddo
    name = splot(ismin)
    nc = lenc(name)
    if (catalog_name(1).ne.ccplot(ismin)) then
      catalog_name(1) = ccplot(ismin)
    endif
  else
    !
    ! Source name
    if (sic_present(0,1)) then
      call sic_ch(line,0,1,name,nc,.false.,error)
      if (error) return
      if (nc.gt.0) then
        call sic_blanc(name,nc)
      else
        return
      endif
    else
      name = 'ALL'
    endif
  endif
  !
  ! Set /DRAW type option
  if (draw) then
    call astro_draw_parse(rname,line,optdraw,drawargs,error)
    if (error)  return
  endif
  !
  drawclip=.true.
  if (noclip) then
    if (.not.draw) then
      call astro_message(seve%e,rname,'Option /NOCLIP only valid in combination with /DRAW')
      error=.true.
      return
    endif
    drawclip=.false.
  endif
  !
  ! /SUN option
  do_print = .true.
  sun_limit = slimit
  if (.not.dosun)  slimit = 0.d0
  !
  ! /PRINT option
  do_print = .not. (print .or. alternate)
  !
  ! /QUIET option
  if (doquiet) then
     slimit = 0.d0      ! force no /SUN
     print = .false.
     do_print = .false.
  endif
  !
  ! /VARIABLE VarName
  if (dovar) then
    call sic_ch(line,optvaria,1,varname,nc,.true.,error)
    if (error)  return
    call source_variable(catalog_name(icat),varname,fmin,fmax,error)
    if (error)  return
    return
  endif
  !
  if (doreset) then
    if (soukind.eq.soukind_none) then
      call astro_message(seve%w,rname,'No source defined, nothing to reset')
      goto 999
    endif
    ! null global variables
    call nullify_astro_source(error)
    if (error) return
    !
    ! Reset the FEBE register which stands for a single source
    if (obsname.eq.'NOEMA') then
      call noema%register%clear(error)
      if (error) return
    endif
    !
    soukind=soukind_none
    goto 999
  endif
  !
  ! /DOPPLER CASE
  if (dodoppler) then
    if (draw.or.alternate.or.doflux.or.docursor.or.dovar.or.doreset.or.noclip.or. &
        doquiet.or.print.or.dosun) then
      call astro_message(seve%e,rname,'/DOPPLER must be used alone')
    endif
    if (sic_narg(0).gt.0) then
      call astro_message(seve%e,rname,'No argument accepted with option /DOPPLER')
      error=.true.
      return
    endif
    call source_doppler_only(rname,line,optdoppler,error)
    ! Reset the FEBE register which stands for a single source
    if (obsname.eq.'NOEMA') then
      call noema%register%clear(error)
      if (error) return
    endif
    goto 999
  endif
  !
  ! Source coordinates are in command line
  lunopened=.false.
  if (sic_present(0,2)) then
    icat = 0
    ! Resolve expressions (as decode_line is not meant to resolve expressions
    ! from Sic command line)
    call expand_line(line,c_line,nlu,error)
    if (error)  goto 999
    ! Skip source name
    npar = 1
    call sic_next(c_line(1:nlu),par,lpar,npar)
    ! Source arguments parsing (starting from updating npar)
    call decode_line (c_line, npar, coord, equinox, lambda, beta, vtype,  &
      velocity,redshift, ftype, flux, spindex, mag, error)
    if (.not.error .and. (flux.ge.fmin .and. flux.lt.fmax)) then
      z_axis = flux
      z_axis_type = ftype
      call object (name,coord,equinox,lambda,beta,vtype,velocity,redshift, &
                  draw,drawargs,drawclip,s_3,do_print,icat,error)
      soukind=soukind_full
      if (error) goto 999
      if (print) then
        call astro_message(seve%r,rname,c_line(1:nlu))
      endif
    endif
    found = .true.
    !
    ! else search in catalog
  else
    if (lenc(catalog_name(icat)).eq.0) then
      call astro_message(seve%f,rname,'No catalog')
      error = .true.
      goto 999
    endif
    ier = sic_getlun(lun)
    ier = sic_open(lun,catalog_name(icat),'OLD',.true.)
    if (ier.ne.0) then
      call astro_message(seve%e,rname,'Error opening '//catalog_name(icat))
      call putios('E-ASTRO_SOURCE,  ',ier)
      call sic_frelun(lun)
      error = .true.
      goto 999
    endif
    !
    lunopened=.true.
    do while (.true.)
100   read (lun,'(A)',iostat=ier,end=210,err=910) x_line
      if (x_line(1:1).eq.'!') goto 100 ! ignore comments
      nlx = lenc(x_line)
      if (nlx.eq.0) goto 100   ! ignore empty lines
      nlu = nlx
      c_line = x_line
      call sic_blanc(c_line,nlu)
      npar = 1
      lpar = 80
      call sic_next(c_line(1:nlu),par,lpar,npar)
      if (lpar.eq.0) goto 100
      if (name.eq.'ALL') then
        found = .true.
      else
        found = .false.
      endif
      call check_source(name,lenc(name),par,lpar,found)
      if (found) then
        call decode_line (c_line, npar, coord, equinox, lambda, beta, vtype,  &
        velocity,redshift, ftype, flux, spindex, mag, error)
        if (.not.error .and. (flux.ge.fmin .and. flux.lt.fmax)) then
          z_axis = flux
          z_axis_type = ftype
          call object(par(1:lpar),coord,equinox,lambda,beta,vtype,velocity,redshift,  &
                      draw,drawargs,drawclip,s_3,do_print,icat,error)
          soukind=soukind_full
          if (print) then
            call astro_message(seve%r,rname,x_line(1:nlx))
          endif
        endif
        found = .true.
        if (name.ne.'ALL') goto 990
        if (sic_ctrlc()) goto 990
      endif
    enddo
  endif
  !
  ! Reset the FEBE register which stands for a single source
  if (obsname.eq.'NOEMA') then
    call noema%register%clear(error)
    if (error) return
  endif
  !
  210   continue
  if (.not.found) then
    call astro_message(seve%e,rname,name//' not found')
    goto 920
  endif
  if (draw) then
    do i=1, nsplot
      dsplot(i) = sqrt(xsplot(i)**2+ysplot(i)**2)*360/pi
    enddo
    ncalib = 0
    do i=1, nsplot
      if (ncalib.lt.mcalib .and. dsplot(i).gt.0 .and. dsplot(i).lt.20.) then
        ncalib = ncalib+1
        fcalib(ncalib) = -zsplot(i)
        is(ncalib) = i
      endif
    enddo
    call gr4_trie(fcalib,ind,ncalib,error)
    do j=1, ncalib
      dcalib(j) = dsplot(is(ind(j)))
      ccalib(j) = splot(is(ind(j)))
      fcalib(j) = -fcalib(j)
    enddo
    call sic_delvariable ('CALIB_N',.false.,error)
    call sic_delvariable ('CALIB_DIST',.false.,error)
    call sic_delvariable ('CALIB_NAME',.false.,error)
    call sic_delvariable ('CALIB_FLUX',.false.,error)
    error = .false.
    call sic_def_inte('CALIB_N',ncalib,1,1,.true.,error)
    call sic_def_real('CALIB_DIST',dcalib,1,ncalib,.true.,error)
    call sic_def_real('CALIB_FLUX',fcalib,1,ncalib,.true.,error)
    call sic_def_charn('CALIB_NAME',ccalib,1,ncalib,.true.,error)
  endif
  goto 990
  !
  ! Errors
  910   call astro_message(seve%e,rname,'Error reading catalog')
  call putios('E-ASTRO_SOURCE,  ',ier)
  !
  920   error = .true.
  990   if (lunopened) close (unit=lun)
  call sic_frelun(lun)

  !
  999   continue
  catalog_name(1) = catalog_saved
  slimit = sun_limit

end subroutine astro_source
!
subroutine expand_line(line,oline,oll,error)
  use gkernel_interfaces
  use astro_interfaces, except_this=>expand_line
  !---------------------------------------------------------------------
  ! @ private
  ! Expand the current command line for all its expressions (i.e.
  ! single-quoted arguments). Other arguments are not modified; in
  ! particular we need to avoid translating a random character string as
  ! a valid variable name. Double-quotes are preserved. For example:
  !   Hello PI "PI PI" 'pi' '2*pi'
  ! translates as
  !   Hello PI "PI PI" 3.1415926535898 6.2831853071796
  ! Beware the returned line can not be used with the SIC_R4 et al
  ! subroutines as the command line pointers are not relevant for the
  ! modified version.
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   !
  character(len=*), intent(out)   :: oline  ! Output line
  integer(kind=4),  intent(out)   :: oll    ! Output line length
  logical,          intent(inout) :: error  !
  ! Local
  integer(kind=4), parameter :: iopt=0  ! Analyse command arguments
  integer(kind=4) :: iarg,larg
  !
  oll = 1
  do iarg=1,sic_narg(iopt)  ! Start at 1: command is removed
    call sic_st(line,iopt,iarg,oline(oll:),larg,.true.,error)
    if (error)  return
    oll = oll+larg+1
  enddo
  oll = oll-1
  !
end subroutine expand_line
!
subroutine decode_line(c_line,npar,coord,equinox,lambda,beta,vtyp,velo,redsh,ftype,  &
  flux,spindex,mag,error)
  use gbl_constant
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>decode_line
  use ast_constant
  !---------------------------------------------------------------------
  ! @ private
  !  Decode a standard source catalog line
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: c_line   ! Input line
  integer(kind=4),  intent(inout) :: npar     ! pointer to first data word (coords or type)
  character(len=2), intent(out)   :: coord    ! Coordinate type
  real(kind=4),     intent(out)   :: equinox  ! Equinox
  real(kind=8),     intent(out)   :: lambda   !
  real(kind=8),     intent(out)   :: beta     !
  character(len=2), intent(out)   :: vtyp     ! velocity type
  real(kind=8),     intent(out)   :: velo     ! velocity
  real(kind=8),     intent(out)   :: redsh     ! redshift
  character(len=2), intent(out)   :: ftype    ! flux type
  real(kind=4),     intent(out)   :: flux     ! flux value
  real(kind=4),     intent(out)   :: spindex  ! spectral index
  real(kind=4),     intent(out)   :: mag(9)   ! magnitudes V,R,I,J,H,K,L,M,N
  logical,          intent(inout) :: error    !
  ! Local
  integer(kind=4) :: npar1
  integer(kind=4) :: nlu, lpar
  character(len=80) :: par
  real(kind=4) :: r1, r2 
  real(kind=8) :: vorz
  integer(kind=4) :: nkey,i
  integer(kind=4), parameter :: mvoc=9
  character(len=2) :: vocab(mvoc),keywor
  character(len=11), parameter :: rname='DECODE_LINE'
  ! Data
  data vocab/'MV','MR','MI','MJ','MH','MK','ML','MM','MN'/
  !
  ! Decode coordinates
  ! go to uppercase for keywords.
  nlu = lenc(c_line)
  npar1 = npar                 ! pointer to coords
  lpar = 2
  call sic_next(c_line(npar:),par,lpar,npar)   ! type ?
  if (lpar.eq.0) goto 220
  coord = '  '
  call sic_upper(par)
  equinox = equinox_null  ! Default for non-equatorial systems
  if (par(1:2).eq.'EQ') then
    lpar = 12
    call sic_next(c_line(npar:),par,lpar,npar) ! equinox
    if (lpar.eq.0) goto 220
    call sic_math_real(par,lpar,equinox,error)
    if (error) goto 930
    coord = 'EQ'
  elseif (par(1:1).eq.'G') then
    coord = 'GA'
  elseif (par(1:1).eq.'S') then
    coord = 'SU'
  elseif (par(1:1).eq.'E') then
    coord = 'EC'
  elseif (par(1:1).eq.'H') then
    coord = 'HO'
  elseif (par(1:1).eq.'D') then
    coord = 'DA'
  else
    npar = npar1
    coord = 'EQ'
    equinox = 1950.0
  endif
  lpar = 40
  call sic_next(c_line(npar:),par,lpar,npar)   ! get lambda
  if (lpar.eq.0) goto 220
  call suffix(par,lpar,r1,r2)
  if (r1.ne.0.0) then
    call astro_message(seve%w,rname,'Time derivatives ignored in ASTRO')
  endif
  call sic_sexa(par,lpar,lambda,error)
  if (error) goto 930
  if (coord.eq.'EQ' .or. coord.eq.'DA') then
    lambda = lambda*pi/12.0d0
  else
    lambda = lambda*pi/180.0d0
  endif
  lpar = 40
  call sic_next(c_line(npar:),par,lpar,npar)   ! get delta
  if (lpar.eq.0) goto 220
  call suffix(par,lpar,r1,r2)
  if (r1.ne.0.0) then
    call astro_message(seve%w,rname,'Time derivatives ignored in ASTRO')
  endif
  call sic_sexa(par,lpar,beta,error)
  if (error) goto 930
  beta = beta*pi/180.0d0
  !
  ! Decode validation hour
  npar1 = npar
  lpar = 2
  call sic_next(c_line(npar:),par,lpar,npar)
  call sic_upper(par)
  if (par(1:2).eq.'HO') then
    lpar = 3
    call sic_next(c_line(npar:),par,lpar,npar)
    call astro_message(seve%w,rname,'Validation Hour ignored')
  else
    npar = npar1
  endif
  !
  ! Decode parallax
  npar1 = npar
  lpar = 2
  call sic_next(c_line(npar:),par,lpar,npar)
  call sic_upper(par)
  if (par(1:2).eq.'PA') then
    lpar = 12
    call sic_next(c_line(npar:),par,lpar,npar)
    call astro_message(seve%w,rname,'Parallax ignored')
  else
    npar = npar1
  endif
  !
  ! Decode velocity or Redshift
  npar1 = npar
  lpar = 1
  call sic_next(c_line(npar:),par,lpar,npar)   ! get type (vel frame or z)
  if (lpar.eq.0) par(1:1) = '*'
  call sic_upper(par)
  if (par(1:1).eq.'L') then
    vtyp = 'LS'
  elseif (par(1:1).eq.'H') then
    vtyp = 'HE'
  elseif (par(1:1).eq.'E') then
    vtyp = 'EA'
  elseif (par(1:1).eq.'N') then
    vtyp = 'NU'
  elseif (par(1:1).eq.'R') then
    vtyp = 'RE'
  else                         ! defaults if no type specified
    if (coord.eq.'HO') then
      vtyp = 'EA'
    else
      vtyp = 'LS'
    endif
    npar = npar1
  endif
  !
  lpar = 12
  npar1 = npar
  call sic_next(c_line(npar:),par,lpar,npar)   ! get vel. or z
  if (lpar.gt.0) then
    call sic_upper(par(1:lpar))
    if (par(1:1).gt.'A' .and. par(1:1).le.'Z') then
      velo = 0d0
      redsh = 0d0
      npar = npar1
      goto 80
    endif
    call sic_math_dble(par,lpar,vorz,error)
    if (error) goto 930
    if (vtyp.eq.'RE') then
      if (vorz.lt.0) then
        call astro_message(seve%e,rname,'Redshift should be greater or equal to 0')
        error=.true.
        return
      endif
      redsh = vorz
      velo = 0d0
    else
      velo = vorz
      redsh = 0d0
    endif
  else
    velo = 0d0
    redsh = 0d0
    npar = npar1
  endif
  !
  ! Decode flux
  !
  80    ftype = 'FL'           !default
  flux = 0.
  spindex = 0.
  npar1 = npar
  lpar = 2
  call sic_next(c_line(npar:),par,lpar,npar)   ! get type
  if (lpar.gt.0) then
    call sic_upper(par)
    if (par(1:2).eq.'FL') then
      ftype = 'FL'
    elseif (par(1:2).eq.'MA') then
      ftype = 'MA'
    else                       ! not a flux/mag code
      npar = npar1
      goto 81
    endif
  else
    goto 81
  endif
  ! Decode flux/mag value
  lpar = 12
  npar1 = npar
  call sic_next(c_line(npar:),par,lpar,npar)   ! get flux
  if (lpar.gt.0) then
    call sic_math_real(par,lpar,flux,error)
    if (error) goto 930
  else
    flux = 0.
    npar = npar1
    goto 81
  endif
  !Decode spectral index
  lpar = 12
  npar1 = npar
  call sic_next(c_line(npar:),par,lpar,npar)   ! get spectral index
  if (lpar.gt.0) then
    call sic_math_real (par,lpar,spindex,error)
    if (error) then
      spindex = 0.
      npar = npar1
    endif
  else
    spindex = 0.
    npar = npar1
  endif
  !Decode magnitudes in bands
  !
  ! Decode magnitudes
  !
  81    continue
  do i=1,9
    mag(i)=99.99
  enddo
  !
  do while (.true.)            ! I=1,9  ! Could be an open loop
    lpar = 2
    npar1 = npar
    call sic_next(c_line(npar:),par,lpar,npar)
    if (lpar.gt.0) then
      call sic_upper(par)
      call sic_ambigs(' ',par,keywor,nkey,vocab,mvoc, error)
      if (.not.error) then
        lpar = 6
        call sic_next(c_line(npar:),par,lpar,npar)
        if (lpar.eq.0) goto 220
        call sic_math_real(par,lpar,mag(nkey),error)
        if (error) goto 930
      else
        error = .false.
      endif
    else
      return                   ! No more keyword
    endif
  enddo
  return
  !
  ! Errors
930 continue
  call astro_message(seve%e,rname,'Decoding error')
  call astro_message(seve%r,rname,c_line)
  goto 300
  !
220 continue
  call astro_message(seve%e,rname,'Missing parameters')
  !
300 error = .true.
  return
end subroutine decode_line
!
subroutine source_doppler_only(rname,line,optdoppler,error)
  use gildas_def
  use gkernel_interfaces
  use gbl_message
  use astro_interfaces, except_this=>source_doppler_only
  use ast_astro
  !----------------------------------------------------------------------
  ! @ private
  ! Handle the case where only a redshift or a lsr velocity is provided, 
  ! no precise source
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  character(len=*), intent(in) :: line
  integer(kind=4), intent(in)  :: optdoppler
  logical, intent(inout) :: error
  ! Local
  integer(kind=4), parameter :: mdoptypes=2
  integer(kind=4) :: ndoptype,lpar
  character(len=8), parameter :: doptypes(mdoptypes) = (/ 'LSR     ','REDSHIFT' /)
  character(len=8) :: doptype,par
  character(len=varname_length) :: varname
  real(kind=8) :: dval,vshift,vlsr,velocity,redshift
  character(len=12) :: name
  !
  call sic_ke(line,optdoppler,1,par,lpar,.true.,error)
  if (error) return
  call sic_ambigs(rname,par,doptype,ndoptype,doptypes,mdoptypes,error)
  if (error) return
  call sic_r8(line,optdoppler,2,dval,.true.,error)
  if (error) return
  !
  select case (doptype)
  case ('LSR')
    soukind=soukind_vlsr
    velocity=dval
    vshift=dval
    vlsr=dval
    redshift=0d0
  case ('REDSHIFT')
    soukind=soukind_red
    velocity=0d0
    vshift=0d0
    vlsr=0d0
    if (dval.lt.0) then
      call astro_message(seve%e,rname,'Redshift should be greater or equal to 0')
      error=.true.
      return
    endif
    redshift=dval
  case default
    call astro_message(seve%e,rname,'Doppler type not recognized')
    error=.true.
    return
  end select
  name=soukinds(soukind)
  ! Store output parameters in the SIC structure astro%source
  varname = 'ASTRO'
  if (.not.sic_varexist(varname)) then
      call sic_defstructure(varname,.true.,error)
      if (error) return
  endif
  varname = 'ASTRO%SOURCE'
  call fill_doppler_source(rname,varname,name,vshift,vlsr,redshift, &
                           doptype,velocity,error)
  if (error) return
  !
end subroutine source_doppler_only
!
subroutine suffix(par,lpar,r1,r2)
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*):: par            !
  integer(kind=4) :: lpar           !
  real(kind=4) :: r1                !
  real(kind=4) :: r2                !
  ! Local
  integer(kind=4) :: i,ier
  character(len=80) :: chain
  !
  r1 = 0.0
  r2 = 0.0
  i = index(par(1:lpar),',')
  if (i.eq.0) return
  chain = par(i+1:lpar)
  par(i:) = ' '
  lpar = i-1
  i = index(chain,',')
  if (i.eq.0) then
    read (chain,*,iostat=ier) r1
  else
    read (chain,*,iostat=ier) r1,r2
  endif
end subroutine suffix
!
subroutine check_source(name,lname,par,lpar,found)
  use ast_astro
  use ast_constant
  !---------------------------------------------------------------------
  ! @ private
  ! This subroutine:
  ! 1) * if 'found' is not yet .true., check is the input 'name' matches
  !      one of the fields in 'par' (several values can be separated by
  !      |). Matching is case insensitive.
  !    * if 'found' is already .true., skip the matching part and does
  !      only the following action.
  ! 2) if the name is found, update 'par' according to the ASTRO\SET
  !    NAME tuning:
  !     -1: all the variants are kept in 'par'
  !      0: the variant used for searching ('name') is returned in 'par'
  !    Val: the Val-th variant is returned in 'par'
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: name   ! Name to search for
  integer(kind=4),  intent(in)    :: lname  ! Length of 'name'
  character(len=*), intent(inout) :: par    ! Name to check against
  integer(kind=4),  intent(inout) :: lpar   ! Length of 'par'
  logical,          intent(inout) :: found  ! If already .true., do not search
  ! Local
  integer(kind=4) :: j,k,j1,j2, jk
  character(len=80) :: nom
  character(len=256) :: upar
  !
  ! Syntax is NAME1[|NAME2[|NAME3]]
  if (.not.found) then
    jk = 1
    nom = name
    call sic_upper(nom)
    upar = par
    call sic_upper(upar)
    do while (.not.found .and. jk.le.lpar)
      j = index(upar(jk:lpar),nom(1:lname))
      found = j.ne.0
      if (.not.found) return
      k = jk-1+j-1
      if (k.gt.0 .and. upar(k:k).ne.'|') found = .false.
      k = jk-1+j+lname
      if (k.le.lpar .and. upar(k:k).ne.'|') found = .false.
      jk = k
    enddo
    if (.not.found) return
    ! Use only one name if NAME_OUT is set
    if (name_out.eq.0) then
      par = name(1:lname)  ! The case may be different from the original case in
        ! the catalog, but ASTRO\HELP SET says "print the name of the source
        ! which has been searched for". Is this case problem intended by the HELP?
      lpar = lname
    elseif (name_out.eq.-1) then  ! Use all names
      return
    endif
  elseif (name_out.eq.-1) then
    return
  else
    ! Extract name of order NAME_OUT
    j = 0
    j1 = 1
    do while (.true.)
      j2 = index(par(j1:lpar),'|')
      if (j2.eq.0) then
        nom = par(j1:lpar)
        lpar = lpar-j1+1
        par = nom
        return
      endif
      j2 = j2+j1-2
      if (j.eq.name_out) then
        nom = par(j1:j2)
        lpar = j2-j1+1
        par = nom
        return
      endif
      j1 = j2+2
      j = j+1
    enddo
  endif
  !
end subroutine check_source
!
subroutine source_variable(filename,varname,fmin,fmax,error)
  use gbl_format
  use gbl_message
  use gkernel_interfaces
  use ast_params
  use astro_interfaces, except_this=>source_variable
  !---------------------------------------------------------------------
  ! @ private
  !  Load the catalog in a user structure
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: filename  !
  character(len=*), intent(in)    :: varname   !
  real(kind=4),     intent(in)    :: fmin      !
  real(kind=4),     intent(in)    :: fmax      !
  logical,          intent(inout) :: error     !
  ! Local
  character(len=*), parameter :: rname='SOURCE'
  character(len=256) :: x_line
  character(len=128) :: dummy
  character(len=sourcename_length) :: souname
  real(kind=8) :: lambda,beta
  real(kind=4) :: equinox,flux,spindex,mag(9)
  real(kind=8) :: velocity,redshift
  character(len=2) :: coord,ftype,vtype
  integer(kind=4) :: ier,lpar,lun,nl,npar,nsources
  logical :: dofill,found
  character(len=16) :: dims
  !
  if (filename.eq.'') then
    call astro_message(seve%e,rname,'No catalog')
    error = .true.
    return
  endif
  !
  if (sic_getlun(lun).ne.1) then
    error = .true.
    return
  endif
  !
  dofill = .false.
100 continue
  !
  ier = sic_open(lun,filename,'OLD',.true.)
  if (ier.ne.0) then
    call astro_message(seve%e,rname,'Error opening '//filename)
    call putios('E-SOURCE,  ',ier)
    call sic_frelun(lun)
    error = .true.
    return
  endif
  !
  ! NB: for now the lines are decoded twice (one loop for counting the -valid-
  ! lines, one loop for filling the output variables). This could be improved
  ! by storing the results in temporary arrays (or array of structure of
  ! type(source))
  nsources = 0
  do
    read(lun,'(A)',iostat=ier) x_line
    if (ier.ne.0)            exit   ! <0: EOF, >0: Error
    if (x_line(1:1).eq.'!')  cycle  ! ignore comments
    if (x_line.eq.'')        cycle  ! ignore empty lines
    !
    nl = len_trim(x_line)
    call sic_blanc(x_line,nl)
    !
    npar = 1
    call sic_next(x_line(1:nl),souname,lpar,npar)
    if (lpar.eq.0)  cycle
    !
    dummy = 'ALL'   ! Unused below
    found = .true.  ! Select whatever the name
    ! Patch 'par' according to ASTRO\SET NAME:
    call check_source(dummy,len_trim(dummy),souname,lpar,found)
    !
    call decode_line(x_line,npar,coord,equinox,lambda,beta,vtype,velocity,redshift,  &
      ftype,flux,spindex,mag,error)
    if (error)  exit
    !
    if (flux.lt.fmin .or. flux.gt.fmax)  cycle
    !
    nsources = nsources+1
    !
    if (dofill) then
      write(dims,'(A,I0,A)')  '[',nsources,']'
      call sic_variable_fill(rname,trim(varname)//'%NAME'//dims,souname(1:lpar),error)
      if (error)  return
      call sic_variable_fill(rname,trim(varname)//'%SYSTEM'//dims, coord,error)
      if (error)  return
      call sic_variable_fill(rname,trim(varname)//'%EQUINOX'//dims,equinox,error)
      if (error)  return
      call sic_variable_fill(rname,trim(varname)//'%LAMBDA'//dims,lambda,error)
      if (error)  return
      call sic_variable_fill(rname,trim(varname)//'%BETA'//dims,beta,error)
      if (error)  return
      call sic_variable_fill(rname,trim(varname)//'%VTYPE'//dims,vtype,error)
      if (error)  return
      call sic_variable_fill(rname,trim(varname)//'%VELOCITY'//dims,velocity,error)
      if (error)  return
      call sic_variable_fill(rname,trim(varname)//'%REDSHIFT'//dims,redshift,error)
      if (error)  return
      call sic_variable_fill(rname,trim(varname)//'%ITYPE'//dims,ftype,error)
      if (error)  return
      call sic_variable_fill(rname,trim(varname)//'%INTENSITY'//dims,flux,error)
      if (error)  return
      call sic_variable_fill(rname,trim(varname)//'%SPINDEX'//dims,spindex,error)
      if (error)  return
      write(dims,'(A,I0,A)')  '[,',nsources,']'
      call sic_variable_fill(rname,trim(varname)//'%MAGNITUDE'//dims,mag,int(9,kind=size_length),error)
      if (error)  return
    endif
    !
    if (sic_ctrlc())  exit
  enddo
  !
  ier = sic_close(lun)
  if (.not.dofill) then
    ! Create structure
    call structure_parent(varname,error)
    if (error)  return
    ! Create elements in structure
    call sic_defvariable(fmt_i4,trim(varname)//'%N',.true.,error)
    if (error)  return
    write(dims,'(A,I0,A)')  '[',nsources,']'
    call sic_defvariable(len(souname),trim(varname)//'%NAME'//dims,.true.,error)
    if (error)  return
    call sic_defvariable(2,     trim(varname)//'%SYSTEM'//dims,.true.,error)
    if (error)  return
    call sic_defvariable(fmt_r4,trim(varname)//'%EQUINOX'//dims,.true.,error)
    if (error)  return
    call sic_defvariable(fmt_r8,trim(varname)//'%LAMBDA'//dims,.true.,error)
    if (error)  return
    call sic_defvariable(fmt_r8,trim(varname)//'%BETA'//dims,.true.,error)
    if (error)  return
    call sic_defvariable(2,     trim(varname)//'%VTYPE'//dims,.true.,error)
    if (error)  return
    call sic_defvariable(fmt_r4,trim(varname)//'%VELOCITY'//dims,.true.,error)
    if (error)  return
    call sic_defvariable(fmt_r4,trim(varname)//'%REDSHIFT'//dims,.true.,error)
    if (error)  return
    call sic_defvariable(2,     trim(varname)//'%ITYPE'//dims,.true.,error)
    if (error)  return
    call sic_defvariable(fmt_r4,trim(varname)//'%INTENSITY'//dims,.true.,error)
    if (error)  return
    call sic_defvariable(fmt_r4,trim(varname)//'%SPINDEX'//dims,.true.,error)
    if (error)  return
    write(dims,'(A,I0,A)')  '[9,',nsources,']'
    call sic_defvariable(fmt_r4,trim(varname)//'%MAGNITUDE'//dims,.true.,error)
    if (error)  return
    ! Fill size
    call sic_variable_fill(rname,trim(varname)//'%N',nsources,error)
    if (error)  return
    dofill = .true.
    goto 100
  endif
  !
  call sic_frelun(lun)
  !
contains
  subroutine structure_parent(varname,error)
    use gbl_message
    use gkernel_types
    use gkernel_interfaces
    !---------------------------------------------------------------------
    !  Define the Sic structure (parent structure part)
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: varname  ! Structure name e.g. MYTOC
    logical,          intent(inout) :: error    ! Logical error flag
    ! Local
    type(sic_descriptor_t) :: desc
    logical :: found
    !
    ! Check if structure exist, create if needed
    found = .false.  ! Not verbose
    call sic_descriptor(varname,desc,found)
    if (found) then
      ! Delete it
      if (desc%type.ne.0) then
        call astro_message(seve%e,rname,'Output variable must be a structure')
        error = .true.
  !   elseif (desc%status.ne.user_defined) then
  !     call astro_message(seve%e,rname,'Output variable must be user-defined')
  !     error = .true.
      endif
      if (error)  return
      call sic_delvariable(varname,.true.,error)  ! User request
      if (error)  return
    endif
    ! Create a user-defined structure  ! ZZZ not global?
    call sic_crestructure(varname,.true.,error)
    if (error) then
      call astro_message(seve%e,rname,'Can not define '//trim(varname)//' structure')
      return
    endif
  end subroutine structure_parent
  !
end subroutine source_variable
