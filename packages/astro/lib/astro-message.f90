!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage ASTRO messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module astro_message_private
  use gpack_def
  !---------------------------------------------------------------------
  ! Identifier used for message identification
  !---------------------------------------------------------------------
  integer(kind=4) :: astro_message_id = gpack_global_id  ! Default value for startup message
  !
end module astro_message_private
!
subroutine astro_message_set_id(id)
  use astro_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer(kind=4), intent(in) :: id  !
  ! Local
  character(len=message_length) :: mess
  !
  astro_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',astro_message_id
  call astro_message(seve%d,'astro_message_set_id',mess)
  !
end subroutine astro_message_set_id
!
subroutine astro_message(mkind,procname,message)
  use astro_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Messaging facility for the current library. Calls the low-level
  ! (internal) messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer(kind=4),  intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  !
  call gmessage_write(astro_message_id,mkind,procname,message)
  !
end subroutine astro_message
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
