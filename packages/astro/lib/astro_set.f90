subroutine astro_set_command(line,error)
  use gbl_message
  use gkernel_interfaces
  use ast_line
  use ast_astro
  use plot_molecules_globals
  use frequency_axis_globals
  !---------------------------------------------------------------------
  ! @ private
  ! ASTRO Command SET DUT1|DTDT|AZ|FLUX
  !---------------------------------------------------------------------
  character(len=*):: line           !
  logical :: error                  !
  ! Local
  integer(kind=4) :: nkey
  integer(kind=4), parameter :: mkeys=7
  character(len=12) :: arg, key, keys(mkeys)
  character(len=2) :: arg2
  character(len=64) :: ch
  real*8 :: rarg
  integer*4 :: nc
  character(len=*), parameter :: rname ='ASTRO_SET'
  ! Data
  data keys/'DUT1','DTDT','AZIMUTH','NAME','ATM','LINES','FREQUENCY'/
  !
  !------------------------------------------------------------------------
  ! Code :
  call sic_ke (line,0,1,arg,nc,.true.,error)
  if (error) return
  if (arg.eq.'?') then
  ! to avoid ? to be given to GREG since it is not an ASTRO\SET argument
    call sic_ambigs_list(rname,seve%i,'Choices are:',keys)
    return
  endif
  ! 'sic_ambigs_sub' does not raise an error if no known keyword is
  ! found:
  call sic_ambigs_sub('SET',arg,key,nkey,keys,mkeys,error)
  if (error) return  ! There was an ambiguity
  !
  if (nkey.eq.0) then
    ! Argument not found in ASTRO\SET keywords, try a GREG\SET one:
    line(1:5)='GREG1'
    call exec_command (line,error)
    return
  endif
  !
  ! DUT1
  if (key.eq.'DUT1') then
    call sic_r8 (line,0,2,rarg,.true.,error)
    if (error) return
    if (rarg.lt.-0.9 .or. rarg.gt.0.9) then
      write(ch,*) 'Invalid value ',rarg
      call astro_message(seve%e,rname,ch)
      error = .true.
      return
    else
      d_ut1 = rarg
      write(ch,'(A,F5.3)') 'D_UT1 set to ',d_ut1
      call astro_message(seve%i,rname,ch)
    endif
    !
    ! DTDT
  elseif (key.eq.'DTDT') then
    call sic_r8  (line,0,2,rarg,.true.,error)
    if (error) return
    if (rarg.lt.0 .or. rarg.gt.100.) then
      write(ch,*) 'Invalid value ',rarg
      call astro_message(seve%e,rname,ch)
      error = .true.
      return
    else
      d_tdt = rarg
      write(ch,'(A,F6.3)') 'D_TDT set to ',d_tdt
      call astro_message(seve%i,rname,ch)
    endif
    !
    ! AZimuth reference
  elseif (key.eq.'AZIMUTH') then
    call sic_ke (line,0,2,arg2,nc,.true.,error)
    if (error) return
    if (arg2(1:1).eq.'S' .or. arg2(1:1).eq.'N') then
      azref = arg2(1:1)
    else
      error = .true.
      write(ch,*) 'Invalid Azimuth reference '//arg2
      call astro_message(seve%e,rname,ch)
      return
    endif
    !
    ! NAME convention
  elseif (key.eq.'NAME') then
    call sic_ke (line,0,2,arg2,nc,.true.,error)
    if (error) return
    if (arg2(1:2).eq.'*') then
      name_out = 0
    elseif (arg2(1:2).eq.'AL') then
      name_out = -1
    else
      call sic_i4 (line,0,2,name_out,.true.,error)
      if (error) return
    endif
    !
    ! ATM model
  elseif (key.eq.'ATM') then
    call sic_ch (line,0,2,arg,nc,.true.,error)
    if (error) return
    call sic_upper(arg)  ! 'OLD', 'NEW', '1985', ...
    call atm_setup(arg,error)
    !
  elseif (key.eq.'LINES') then
    call astro_set_lines(line,molecules,error)
    if (error) return
  elseif (key.eq.'FREQUENCY') then
    call astro_set_frequency(line,freq_axis,error)
    if (error) return
  endif
  !
end subroutine astro_set_command
!
subroutine astro_set_lines(line,molec,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>astro_set_lines
  use astro_types
  !---------------------------------------------------------------------
  ! @ private
  ! Define the way the molecular lines will be plotted
  !---------------------------------------------------------------------
  character(len=*), intent(inout):: line           !
  type(plot_molecules_t), intent(inout) :: molec
  logical, intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='ASTRO_SET'
  integer(kind=4), parameter :: mkeys=5
  integer(kind=4)       :: nkey,nc
  character(len=12) :: keys(mkeys),key,arg
  character(len=256) :: mess
  data keys/'ON','OFF','GAUSS','BOXCAR','MARKER'/
  !
  call sic_ke (line,0,2,arg,nc,.true.,error)
  if (error) return
  call sic_ambigs(rname,arg,key,nkey,keys,mkeys,error)
  if (error) return
  if (nkey.le.2) then
    ! Switch plot on/off
    if (key.eq.'ON') molec%doplot=.True.
    if (key.eq.'OFF') molec%doplot=.False.
    if (sic_narg(0).gt.2) then
      call astro_message(seve%e,rname,'Too many arguments')
      error = .true.
      return
    endif
  elseif (nkey.le.4) then
    ! Profile mode
    molec%doplot=.True.
    molec%profile=key
    call sic_r8(line,0,3,molec%width,.true.,error)
    if (error) return
    if (sic_narg(0).gt.3) then
      call astro_message(seve%e,rname,'Too many arguments')
      error = .true.
      return
    endif
  elseif (key.eq.'MARKER') then
    molec%doplot=.True.
    molec%profile='BOXCAR'
    molec%width=0d0
    if (sic_narg(0).gt.2) then
      call astro_message(seve%e,rname,'Too many arguments')
      error = .true.
      return
    endif
  else
    write (mess,'(a,1x,a)') key,'not implemented'
    call astro_message(seve%e,rname,mess)
    error=.true.
    return
  endif
  !
end subroutine astro_set_lines
!
subroutine astro_set_frequency(line,freq_axis,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>astro_set_frequency
  use astro_types
  use ast_astro
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  ! Define the way the molecular lines will be plotted
  !---------------------------------------------------------------------
  character(len=*), intent(inout):: line           !
  type(frequency_axis_t), intent(inout) :: freq_axis
  logical, intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='ASTRO_SET'
  integer(kind=4), parameter :: mkeys=11
  integer(kind=4), parameter :: mkeys1=4
  integer(kind=4), parameter :: mkeys2=11
  character(len=12), parameter :: defmain='REST'
  character(len=12), parameter :: defsecond='NULL'
  integer(kind=4)       :: nkey,nc,ik
  character(len=12) :: keys(mkeys),key,arg
  character(len=256) :: mess
  data keys/'DEFAULT','REST','RF','LSR','IF1','IF2','CHUNKS','IMREST', &
            'IMRF','IMLSR','NULL'/
  ! respect order in astro_types freqax_*
  !
  if (sic_narg(0).gt.3.or.sic_narg(0).lt.1) then
    call astro_message(seve%e,rname,'Wrong number of arguments')
    error=.true.
    return
  endif
  if (sic_narg(0).eq.1) then
    ! show current frequency axis
    call astro_message(seve%i,rname,'Current frequency axis are:')
    write (mess,'(a,1x,a)') 'Main (lower) axis:',trim(freq_axis%main)
    call astro_message(seve%r,rname,mess)
    write (mess,'(a,1x,a)') 'Secondary (upper) axis:',trim(freq_axis%second)
    call astro_message(seve%r,rname,mess)
    return
  endif
  !main axis
  call sic_ke (line,0,2,arg,nc,.true.,error)
  if (error) return
  call sic_ambigs(rname,arg,key,nkey,keys,mkeys,error)
  if (error) return
  if (nkey.gt.mkeys1) then
    ! MAIN axis restricted to REST,RF or LSR
    write (mess,'(a,1x,a)') key,'Axis cannot be selected as main axis'
    call astro_message(seve%e,rname,mess)
    write (mess,'(a)') 'Possible choices are:'
    do ik=2,mkeys1
    write (mess,'(a,1x,a)') trim(mess),trim(keys(ik))
    enddo
    call astro_message(seve%i,rname,mess)
    error=.true.
    return
  else if (obsname.eq.'NOEMA'.and.noema_mode.eq.'ONLINE'.and.nkey.ne.2) then
    call astro_message(seve%e,rname,'MAIN frequency axis must be REST to work with NOEMA ONLINE')
    call astro_message(seve%e,rname,'SECONDARY frequency is free: SET FREQUENCY REST IF1|IF2|RF|...')
    error = .true.
    return
  endif
  if (key.eq.'DEFAULT') then
    freq_axis%main=defmain
    freq_axis%second=defsecond
    return
  endif
  freq_axis%main=key
  if (sic_narg(0).eq.2) return
  !2nd axis
  arg='NULL'
  call sic_ke (line,0,3,arg,nc,.false.,error)
  if (error) return
  call sic_ambigs(rname,arg,key,nkey,keys,mkeys,error)
  if (error) return
  if (obsname.eq.'VELETA'.or.obsname.eq.'PICOVELETA') then
    if (key.eq.'IF2') then
      call astro_message(seve%w,rname,'IF2 frequencies not available for PICO instruments')
      key='NULL'
    endif
  endif
  if (key.eq.'DEFAULT') then
    freq_axis%second=defsecond
    return
  endif
  freq_axis%second=key
  !
end subroutine astro_set_frequency
