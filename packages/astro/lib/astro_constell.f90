subroutine astro_constell(line,error)
  use gildas_def
  use gkernel_interfaces
  use image_def
  use gbl_format
  use gbl_message
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  !	ASTRO Command CONSTELLATIONS /DRAW [L] [M] [S] /NAME /BOUNDARIES
  !	PLOTS the constellations according to current projection, markers,
  !	etc... and the default constellation table GAG_CONSTELLATIONS
  !	format of constellation table is
  !	TABLE[n,6] where columns are:
  !	RA DEC (2000.0) magnitude code_star_name code_constell code_draw
  !	code_draw is 1 for M, 2 for L and 3 for M L
  !	this default overrided by / DRAW
  !	/NAME will plot also constellations names
  !	/BOUNDARIES will draw constellations boundaries
  !---------------------------------------------------------------------
  character(len=*):: line           !
  logical :: error                  !
  ! Local
  character(len=*), parameter :: rname='CONSTELL'
  character(len=1) :: char0,char1,char2
  integer(kind=4) :: draw_code, nc
  logical :: plot_name
  logical :: first_time=.true.
  logical :: bound_nothere=.true.
  logical :: plot_bound=.false.
  !
  type(gildas), save :: x
  type(gildas), save :: y
  !
  !------------------------------------------------------------------------
  ! Code
  ! Set /DRAW type option
  error = .false.
  draw_code = 3
  char0 = '*'
  char1 = '*'
  char2 = '*'
  plot_name = .false.
  if (sic_present(1,0)) then
    draw_code = 0
    call sic_ke (line,1,1,char0,nc,.true.,error)
    if (error) return
    call sic_ke (line,1,2,char1,nc,.false.,error)
    if (error) return
    call sic_ke (line,1,3,char2,nc,.false.,error)
    if (error) return
    if (char0.eq.'M'.or.char1.eq.'M'.or.char2.eq.'M') draw_code=draw_code+1
    if (char0.eq.'L'.or.char1.eq.'L'.or.char2.eq.'L') draw_code=draw_code+2
    if (char0.eq.'S'.or.char1.eq.'S'.or.char2.eq.'S') plot_name=.true.
  endif
  !
  ! Set /NAME option
  if (sic_present(2,0)) then
    call astro_message(seve%f,rname,'Option not yet implemented')
    error = .false.
    return
  endif
  !
  ! lets go
  if (first_time) then
    call gildas_null(y, type = 'TABLE' )
    if (.not.sic_query_file('gag_constell','data#dir:','',y%file)) then
      call astro_message(seve%f,rname,'gag_constell not found')
      error = .true.
      return
    endif
    y%gil%form = fmt_r8
    call gdf_read_gildas(y,y%file,' ',error)
    if (error) then
      call astro_message(seve%f,rname,'Cannot read Constellation data file')
      return
    endif
    first_time = .false.
  endif
  !
  ! Set /BOUNDARIES option
  if (sic_present(3,0)) then
    if (bound_nothere) then
      call gildas_null(x, type = 'TABLE' )
      if (.not.sic_query_file('gag_const_bound','data#dir:','',x%file)) then
        call astro_message(seve%f,rname,'gag_const_bound not found')
        error = .true.
        return
      endif
      x%gil%form = fmt_r8
      call gdf_read_gildas(x,x%file,' ',error)
      if (error) then
        call astro_message(seve%f,rname,'Cannot read Boundaries data file')
        error = .true.
        return
      endif
      bound_nothere=.false.
    endif
    plot_bound=.true.
  else
    plot_bound=.false.
  endif
  !
  ! loop on stars
  call gr_segm('CONSTELL',error)
  if (.not.plot_bound .or. sic_present(1,0)) then
    call subpltcons(y%d2d,y%gil%dim(1),y%gil%dim(2), draw_code, plot_name)
  endif
  if (plot_bound) then
    call subpltbnds(x%d2d,x%gil%dim(1),x%gil%dim(2))
  endif
  call gr_segm_close(error)
  !
  error=.false.
end subroutine astro_constell
!
subroutine subpltbnds(array,n,m)
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4), intent(in) :: n        ! 1st Array dimension
  integer(kind=4), intent(in) :: m        ! 2nd Array dimension
  real(kind=8), intent(in) :: array(n,m)  ! Array
  ! Local
  real(kind=8) :: ra_old,dec_old,ra,dec,ra_start,dec_start
  real(kind=8) :: pi, twopi, halfpi, equinox
  integer(kind=4) :: i
  parameter (pi=3.141592653589793d0, halfpi=pi/2d0, twopi = pi*2d0)
  logical :: error
  !
  equinox=1875.0d0               !  pour les frontieres de constellation
  i=1
  ra=array(i,1)
  dec=array(i,2)
  ra_start=ra
  dec_start=dec
  ra_old=ra
  dec_old=dec
  do while(i.lt.n)
    i = i+1
    if (array(i,1).eq.-10000d0) then   !finish constellation
      if (ra.ne.ra_start.or.dec.ne.dec_start) then
        call slowgrid(ra,ra_start,dec,dec_start,'EQ',equinox,error)
      endif
      i = i+1
      ra_start=array(i,1)
      dec_start=array(i,2)
      ra_old=array(i,1)
      dec_old=array(i,2)
    else
      ra=array(i,1)
      dec=array(i,2)
      call slowgrid(ra_old,ra,dec_old,dec,'EQ',equinox,error)
      ra_old=ra
      dec_old=dec
    endif
  enddo
end subroutine subpltbnds
!
subroutine slowgrid(lambda1,lambda2,delta1,delta2,code,equinox,error)
  use gkernel_interfaces
  use gkernel_types
  use ast_astro
  use ast_constant
  !---------------------------------------------------------------------
  ! @ private
  ! TRACE UN ARC DE (lambda1,delta1) a (lambda2,delta2)
  ! define dans le systeme CODE,
  ! le tout precesse a l'epoque courante si besoin est (CODE='EQ'),
  ! dans le systeme courant (FRAME) et la projection courante.
  !---------------------------------------------------------------------
  real(kind=8)     :: lambda1  !
  real(kind=8)     :: lambda2  !
  real(kind=8)     :: delta1   !
  real(kind=8)     :: delta2   !
  character(len=2) :: code     !
  real(kind=8)     :: equinox  !
  logical          :: error    !
  ! Local
  real(kind=8) :: stepl, stepd, az, el
  integer(kind=4) :: mstep,nstep
  parameter (mstep=50)
  real(kind=8) ::  l1,d1,ll1,dd1,x,y, xin(mstep), yin(mstep), zin(mstep)
  real(kind=8) ::  xmin, xmax, ymin, ymax, x1
  integer(kind=4) :: j, nin
  logical :: in, last_in
  type(projection_t) :: gregproj
  !------------------------------------------------------------------------
  if (lambda2.eq.lambda1.and.delta1.eq.delta2) then
    error=.true.
    return
  endif
  nstep=mstep
  call sic_get_dble('USER_XMIN',xmin,error)
  call sic_get_dble('USER_XMAX',xmax,error)
  call sic_get_dble('USER_YMIN',ymin,error)
  call sic_get_dble('USER_YMAX',ymax,error)
  if (xmax.lt.xmin) then
    x1 = xmin
    xmin = xmax
    xmax = x1
  endif
  if (ymax.lt.ymin) then
    x1 = ymin
    ymin = ymax
    ymax = x1
  endif
  error = .false.
  in = .false.
  last_in = .false.
  nin = 0
  !
  stepl= (lambda2-lambda1)
  if (-stepl.gt.pi) then
    stepl =twopi+stepl
  elseif (stepl.gt.pi) then
    stepl = twopi-stepl
    stepl=-stepl
  endif
  stepl= stepl / (nstep-1)
  stepd= (delta2-delta1)
  if (-stepd.gt.halfpi) then
    stepd =pi+stepd
  elseif (stepd.gt.halfpi) then
    stepd = pi-stepd
    stepd=-stepd
  endif
  stepd= stepd / (nstep-1)
  do j=1, nstep
    l1 = lambda1+ stepl * (j-1)
    d1 = delta1+ stepd * (j-1)
    call inothersystem(code,equinox,l1,d1,ll1,dd1,az,el,error)
    if (ll1.lt.0) ll1 = ll1 + twopi
    if (frame.eq.'HORIZONTAL') then
      ll1 = az
      dd1 = el
    endif
    if (projection) then
      call greg_projec_get(gregproj)  ! Get current Greg projection
      call abs_to_rel(gregproj,ll1,dd1,x,y,1)
    else
      x=ll1
      y=dd1
    endif
    in = x.ge.xmin .and. x.le.xmax .and. y.ge.ymin .and. y.le.ymax
    if (frame.eq.'HORIZONTAL') in = in .and. (y.ge.0d0)
    if (in) then
      nin = nin + 1
      xin (nin) = x
      yin (nin) = y
      zin (nin) = nin
      if (nin.ge.2) then
        if (.not.projection .and. abs(xin(nin)-xin(nin-1)).gt.pi) then
          xin(nin) = xin(nin)-sign(2*pi,xin(nin)-xin(nin-1))
          call gr8_curve (nin,xin,yin,zin,'Z',.false.,0d0,-1d0,error)
          nin = 1
          xin (1) = x
          yin (1) = y
          zin (1) = nin
        endif
      endif
    elseif (.not.last_in) then
      nin = 1
      xin (1) = x
      yin (1) = y
      zin (1) = nin
    elseif (last_in) then
      nin = nin + 1
      xin (nin) = x
      yin (nin) = y
      zin (nin) = nin
      call gr8_curve (nin,xin,yin,zin,'Z',.false.,0d0,-1d0,error)
      nin = 1
    endif
    last_in = in
  enddo
  if (nin.ge.2) then
    call gr8_curve (nin,xin,yin,zin,'Z',.false.,0d0,-1d0,error)
  endif
end subroutine slowgrid
!
subroutine subpltcons(array,n,m, draw_code, plot_name)
  use gkernel_interfaces, no_interface=>gr8_marker
  use gkernel_types
  use ast_astro
  use ast_constant
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4), intent(in) :: n          ! 1st Array dimension
  integer(kind=4), intent(in) :: m          ! 2nd Array dimension
  real(kind=8), intent(in) :: array(n,m)    ! Array
  integer(kind=4), intent(in) :: draw_code  ! Drawing option
  logical, intent(in) :: plot_name          ! Plot constellation names
  ! Local
  character(len=2) :: greek
  character(len=1) :: codename(24)
  character(len=1) :: altcodename(26)
  character(len=6) :: twod
  logical :: in,eqfram,horfram,no_name
  !
  real(kind=8) :: x,y,xmin, xmax, ymin, ymax, x1
  !
  real(kind=8) :: s_3(2)
  real(kind=8) :: s_2(2), x_2(3), v_2(3), dop, lsr, x_3(3), x_0(3), x_1(3)
  real(kind=8) :: degrad, psi, the, phi, jfix, epsm, mat1(9), mat2(9), angles(6)
  real(kind=8) :: trfm_01(9), oblimo, stv
  integer(kind=4) :: lname
  integer(kind=4) :: i,k,l,istar
  parameter (degrad = pi/180.d0, stv = 2d0*pi/86400d0*366.25d0/365.25d0)
  real(kind=8) :: apx1, apx2, apx3
  parameter (apx1 = 0.289977970217382d0, apx2 = -11.9099497383444d0,  &
             apx3 = 16.0645264529100d0)
  real(8) :: code3,code4,code5,code6
  real :: flux
  real(4) :: size                  !reference magnitude for marker size (0.0)
  integer :: ns,is
  real(4), parameter :: zmax=0.0
  logical :: error,clip_state
  type(projection_t) :: gregproj
  ! Data
  data codename/'a','b','c','d','e','f','g','h', 'i','k','l','m',  &
                'n','o','p','q','r','s', 't','u','w','x','y','z'/
  data altcodename/'a','b','c','d','e','f','g','h', 'i','j','k','l','m',  &
                   'n','o','p','q','r','s', 't','u','v','w','x','y','z'/
  !
  error = .false.
  !
  twod = char(92)//char(92)//'d'//char(92)//char(92)//'d'
  greek = char(92)//'g'
  !
  eqfram =  (frame.eq.'EQUATORIAL')
  horfram = (frame.eq.'HORIZONTAL')
  !
  call gr_exec1('SET COORD USER')
  clip_state = gr_clip(.true.)
  call gr_get_marker(ns,is,size)
  call sic_get_dble('USER_XMIN',xmin,error)
  call sic_get_dble('USER_XMAX',xmax,error)
  call sic_get_dble('USER_YMIN',ymin,error)
  call sic_get_dble('USER_YMAX',ymax,error)
  if (xmax.lt.xmin) then
    x1 = xmin
    xmin = xmax
    xmax = x1
  endif
  if (ymax.lt.ymin) then
    x1 = ymin
    ymin = ymax
    ymax = x1
  endif
  !
  !  Init Transformation matrix from equatorial (JFIX) to ecliptic (J2000.0)
  jfix=j2000                   ! fixed equinox (2000.0)
  epsm=oblimo (jfix)           ! mean obliquity at fixed equinox
  psi=0.d0                     ! Euler angles of transformation
  the=epsm
  phi=0.d0
  call eulmat (psi,the,phi,mat1)   ! compute matrix from (R1) to ecl (JFIX
  call qprec (jfix,j2000,angles)   ! compute precession angles
  psi=angles(5)                ! Euler angles of transformation
  the=angles(4)
  phi=-angles(6)-angles(5)
  call eulmat (psi,the,phi,mat2)   ! compute matrix from ecl (JFIX) to (R0
  call mulmat (mat1,mat2,trfm_01)  ! product TRFM01 = MAT2 x MAT1
  do istar = 1,n
    ! our Case is: mean equatorial coordinates at fixed equinox,
    ! transformation (R1) to (R0)
    s_1(1) = array(istar,1)
    s_1(2) = array(istar,2)
    code3 = array(istar,3)
    code4 = array(istar,4)
    code5 = array(istar,5)
    code6 = array(istar,6)
    call rect (s_1, x_1)
    call matvec (x_1, trfm_01, x_0)
    call matvec (x_0, trfm_20, x_2)
    call matvec (x_0, trfm_30, x_3)    ! needed for Horizon command
    call spher (x_3, s_3)      !
    call matvec (vg_0, trfm_20, v_2)
    !
    ! Doppler correction
    v_2(2) = v_2(2) - cos(pi*(lonlat(2))/180.0d0) * stv * (radius+altitude)
    dop = (v_2(1) * x_2(1) + v_2(2) * x_2(2) + v_2(3) * x_2(3))
    lsr = - (apx1 * x_0(1) + apx2 * x_0(2) + apx3 * x_0(3))
    fshift = 1.0d0
    !
    ! Aberration
    do i = 1, 3
      x_2(i) = x_2(i) - v_2(i) / light
    enddo
    call spher(x_2, s_2)
    s_2(1) = - s_2(1) * 180./pi
    s_2(2) = s_2(2) * 180./pi
    !
    azimuth = s_2(1)*pi/180d0
    elevation = s_2(2)*pi/180d0
    ra = s_3(1)                ! in radians
    if (ra.lt.0) ra = ra + 2*pi
    dec = s_3(2)               ! in radians
    !
    no_name = .false.
    if (code4.le.24.999.and.code4.ge.1) then
      k = int(code4)
      if (code4-float(k).eq.0) then
        source_name = greek//codename(k)
        lname = 3
      else
        source_name = greek//codename(k)//twod
        l = 10*(code4-float(k))
        if (l.gt.10) then
          write (source_name(10:),'(I2)') l
        else
          write (source_name(10:),'(I1)') l
        endif
        lname=lenc(source_name)
      endif
    elseif (code4.le.126.999.and.code4.ge.100.999) then
      code4 = code4-100.0
      k = code4
      if (code4-k.le.0.001) then
        source_name = altcodename(k)
        lname = 1
      else
        source_name = altcodename(k)//twod
        l = 10*(code4-float(k))
        if (l.gt.10) then
          write (source_name(8:),'(I2)') l
        else
          write (source_name(8:),'(I1)') l
        endif
        lname=lenc(source_name)
      endif
    else
      no_name = .true.
    endif
    !
    if (projection) then
      call greg_projec_get(gregproj)  ! Get current Greg projection
      if (eqfram) then
        call abs_to_rel(gregproj,ra,dec,x,y,1)
      else
        call abs_to_rel(gregproj,azimuth,elevation,x,y,1)
      endif
    else
      if (eqfram) then
        x = ra
        y = dec
      else
        x = azimuth
        y = elevation
      endif
    endif
    !
    in = x.le.xmax .and. x.ge.xmin .and. y.le.ymax .and. y.ge.ymin
    if (horfram) in = in .and. (elevation.ge.0d0)
    !
    if (draw_code.eq.3) then
      if (code6.eq.3) then
        if (eqfram) then
          call great_circle(ra_old,dec_old,ra,dec,error)
        elseif (horfram) then
          call great_circle(azimuth_old,elevation_old,azimuth,elevation,error)
        endif
        if (in) then
          ! cf A.YOUNG, SKY & TELESCOPE vol 79, no 3 , march 1990, pp311-313
          flux = size*10**(0.16*(zmax-code3))
          call gr_set_marker(ns,is,flux)
          call gr8_marker(1,x,y,0d0,-1d0)
        endif
      elseif (code6.eq.2) then
        if (eqfram) then
          call great_circle(ra_old,dec_old,ra,dec,error)
        elseif (horfram) then
          call great_circle(azimuth_old,elevation_old,azimuth,elevation,error)
        endif
      elseif (code6.eq.1.and.in) then
        flux = size*10**(0.16*(zmax-code3))
        call gr_set_marker(ns,is,flux)
        call gr8_marker(1,x,y,0d0,-1d0)
      endif
    elseif (draw_code.eq.2) then
      if (code6.eq.3.or.code6.eq.2) then
        if (eqfram) then
          call great_circle(ra_old,dec_old,ra,dec,error)
        elseif (horfram) then
          call great_circle(azimuth_old,elevation_old,azimuth,elevation,error)
        endif
      endif
    elseif (draw_code.eq.1) then
      if ((code6.eq.3.or.code6.eq.1).and.in) then
        flux = size*10**(0.16*(zmax-code3))
        call gr_set_marker(ns,is,flux)
        call gr8_marker(1,x,y,0d0,-1d0)
      endif
    endif
    !
    elevation_old=elevation
    azimuth_old=azimuth
    ra_old=ra
    dec_old=dec
    !
    if (in .and. (plot_name.and..not.no_name)) then
      call greg_draw_label(source_name,lname,error)
      if (error) exit
      call relocate(x,y)
    endif
  enddo
  call gr_set_marker(ns,is,size)
end subroutine subpltcons
!
subroutine inothersystem(coord,equinox,lambda,beta,righta,decli,azimu,eleva,error)
  use gbl_message
  use ast_astro
  use ast_constant
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=2) :: coord         ! type of coord
  real(kind=8) :: equinox           ! if EQ
  real(kind=8) :: lambda            !
  real(kind=8) :: beta              !
  real(kind=8) :: righta            !
  real(kind=8) :: decli             !
  real(kind=8) :: azimu             !
  real(kind=8) :: eleva             !
  logical :: error                  !
  ! Local
  real(kind=8) :: s_3(2)                ! ra dec at current equinox
  real(kind=8) :: s_2(2), x_2(3), v_2(3), x_3(3), x_0(3), x_1(3), degrad
  real(kind=8) :: psi, the, phi, jfix, epsm, mat1(9), mat2(9), mat3(9), angles(6)
  real(kind=8) :: trfm_03(9), trfm_01(9), oblimo, stv
  parameter (degrad = pi /180.d0, stv = 2d0* pi / 86400d0*366.25d0 / 365.25d0)
  real(kind=8) :: apx1, apx2, apx3
  parameter (apx1 = 0.289977970217382d0, apx2 = -11.9099497383444d0,  &
             apx3 = 16.0645264529100d0)
  character(len=*), parameter :: rname = 'CONSTELL'
  !
  !------------------------------------------------------------------------
  ! Code :
  if (coord.eq.'HO') then                 ! horizontal coordinates
    s_2(1) = lambda * 180.d0 /pi
    s_2(2) = beta * 180.d0 /pi
    !
    !  Case : present day "true" coordinates, no transformation matrix, (R1) = (R3)
    !  considered as fixed coordinates in fixed frame, but formally maps are
    !  meaningless
  elseif (coord.eq.'DA') then
    s_3(1) = lambda
    s_3(2) = beta
    call rect (s_3, x_3)
    call matvec (x_3, trfm_23, x_2)       ! needed to compute ...
    call transp (trfm_30, trfm_03)        ! The LSR correction.
    call matvec (x_3, trfm_03, x_0)       ! needed for Horizon command
    !
    !  Case : galactic II coordinates, transformation (R1) to (R0)
  elseif (coord.eq.'GA') then             ! galactic coordinates
    s_1(1) = lambda
    s_1(2) = beta
    call rect (s_1, x_1)
    !  transformation matrix from galactic II to ecliptic (J2000.0)
    !  ### Euler angles could be computed once for all ...
    !  Computation in 3 steps
    psi = 33.d0 * degrad                  ! well known Euler angles
    the = -62.6d0 * degrad                ! for transformation
    phi = -282.25d0 * degrad              ! matrix from
    call eulmat (psi,the,phi,mat1)        ! gal. (II) to equ. (1950.0)
    jfix=j2000+(1950.d0-2000.d0)*365.25d0 ! fixed equinox (julian days)
    epsm=oblimo (jfix)                    ! mean obliquity at fixed equinox
    psi=0.d0                              ! Euler angles of transformation
    the=epsm
    phi=0.d0
    call eulmat (psi,the,phi,mat2)        ! equ. (1950.0) to ecl. (1950.0)
    call mulmat (mat1,mat2,mat3)          ! product MAT3 = MAT2 x MAT1
    !                                     ! gal. (II) to ecl. (1950.0)
    call qprec (jfix,j2000,angles)        ! compute precession angles
    psi=angles(5)                         ! Euler angles
    the=angles(4)                         ! of transformation
    phi=-angles(6)-angles(5)              ! matrix from
    call eulmat (psi,the,phi,mat1)        ! ecl. (1950.0) to ecl. (2000.0)
    call mulmat (mat3,mat1,trfm_01)       ! product TRFM01 = MAT1 x MAT3
    !                                     ! gal. (II) to ecl. (1950.0)
    call matvec (x_1, trfm_01, x_0)
    call matvec (x_0, trfm_20, x_2)
    call matvec (x_0, trfm_30, x_3)       ! needed for Horizon command
    call spher (x_3, s_3)
    !
    ! Case: mean equatorial coordinates at fixed equinox, transformation (R1) to (R0)
  elseif (coord.eq.'EQ') then
    s_1(1) = lambda
    s_1(2) = beta
    call rect (s_1, x_1)
    !  Transformation matrix from equatorial (JFIX) to ecliptic (J2000.0)
    jfix=j2000+(equinox-2000.0d0)*365.25d0  ! fixed equinox (julian days)
    epsm=oblimo (jfix)                    ! mean obliquity at fixed equinox
    psi=0.d0                              ! Euler angles of transformation
    the=epsm
    phi=0.d0
    call eulmat (psi,the,phi,mat1)        !computematrix from (R1) to ecl (JFIX)
    call qprec (jfix,j2000,angles)        ! compute precession angles
    psi=angles(5)                         ! Euler angles of transformation
    the=angles(4)
    phi=-angles(6)-angles(5)
    call eulmat (psi,the,phi,mat2)        !computematrix from ecl (JFIX) to (R0)
    call mulmat (mat1,mat2,trfm_01)       ! product TRFM01 = MAT2 x MAT1
    call matvec (x_1, trfm_01, x_0)
    call matvec (x_0, trfm_20, x_2)
    call matvec (x_0, trfm_30, x_3)       ! needed for Horizon command
    call spher (x_3, s_3)
  else
    call astro_message(seve%e,rname,'Unsupported coordinates')
    error=.true.
    return
  endif
  call matvec (vg_0, trfm_20, v_2)
  fshift = 1.0d0
  if (coord.ne.'HO') then
    call spher(x_2, s_2)
    s_2(1) = - s_2(1) * 180.d0/pi
    s_2(2) = s_2(2) * 180.d0/pi
  endif
  if (azref.ne.'S') then
    s_2(1) = s_2(1)+180.d0
  endif
  azimu = s_2(1)*pi/180d0
  eleva = s_2(2)*pi/180d0
  righta = s_3(1)              ! in radians
  if (righta.lt.0) righta = righta + 2d0*pi
  decli = s_3(2)               ! in radians
end subroutine inothersystem
