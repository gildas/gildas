subroutine astro_show(line,error)
  use gbl_message
  use gkernel_interfaces
  use ast_astro
  use ast_line
  use astro_types
  use frequency_axis_globals
  use plot_molecules_globals  
  !---------------------------------------------------------------------
  ! @ private
  ! Show contents of ASTRO variable defined with SET command
  !---------------------------------------------------------------------
  character(len=*):: line           !
  logical :: error                  !
  ! Global
  character(len=4), external :: atm_get_version
  ! Local
  character(len=*), parameter :: rname ='SHOW'
  integer(kind=4) :: nkey
  integer(kind=4), parameter :: mkeys=10
  character(len=12) :: arg, key, keys(mkeys)
  integer(kind=4) :: nc,nt,no,nt2
  character(len=256) :: mess,chain,chain2
  ! Data
  data keys/'DUT1','DTDT','AZIMUTH','NAME','ATM','LINES','FREQUENCY','ALL','TIME','OBSERVATORY'/
  !
  ! Decode the command line
  arg='ALL'
  call sic_ke (line,0,1,arg,nc,.false.,error)
  if (error) return
  if (nc.ne.0) then
    call sic_ambigs(rname,arg,key,nkey,keys,mkeys,error)
    if (error) return
  endif
  !
  if (key.eq.'ALL'.or.key.eq.'TIME') then
    !UTC
    call jdate_to_datetime(jnow_utc,chain,error)
    write(mess,'(a,1x,a)') 'UTC :',trim(chain)
    call astro_message(seve%r,rname,mess)
    ! UT1
    call jdate_to_datetime(jnow_ut1,chain,error)
    write(mess,'(a,1x,a,1x,a,1x,f0.3)') 'UT1 :',trim(chain),'D_UT1 :',d_ut1
    call astro_message(seve%r,rname,mess)
    ! TDT
    call jdate_to_datetime(jnow_tdt,chain,error)
    write(mess,'(a,1x,a,1x,a,1x,f0.3)') 'TDT :',trim(chain),'D_TDT :',d_tdt
    call astro_message(seve%r,rname,mess)
    ! Julian date
    write(mess,'(1x,a,1x,f16.2)') 'Julian date :',jnow_tdt
    call astro_message(seve%r,rname,mess)
    !
    ! LST
    call rad2sexa(lst,24,chain,4,left=.true.)
    nt = len_trim(chain)
    write(mess,'(a,1x,a)') 'LST :            ',chain(1:nt)
    call astro_message(seve%r,rname,mess)
  endif
  if (key.eq.'ALL'.or.key.eq.'OBSERVATORY') then
    call deg2sexa(lonlat(1),360,chain,3,left=.true.)
    nt = len_trim(chain)
    call deg2sexa(lonlat(2),360,chain2,3,left=.true.)
    nt2 = len_trim(chain2)
    call astro_message(seve%r,rname,' ')
    no=lenc(obsname)
    write(mess,'(1x,a,3x,a,3x,a,3x,a)') 'OBS : ',obsname(1:no),chain(1:nt),chain2(1:nt2)
    call astro_message(seve%r,rname,mess)
    write(mess,'(1x,a,3x,f5.3,a,3x,f5.1,a)') 'Alt.: ',altitude,' km     Sun Avoidance:',slimit,' deg.'
    call astro_message(seve%r,rname,mess)
    ! Print mode for NOEMA
    if (obsname.eq.'NOEMA') then
      if (noema_mode.eq.'ONLINE') then
        write (mess,'(a)') ' ONLINE MODE: OBS-like syntax for frequency setups'
      else if (noema_mode.eq.'OFFLINE') then
        write (mess,'(a)') ' OFFLINE MODE: ASTRO syntax for frequency setups'
      else
        call astro_message(seve%e,rname,'NOEMA MODE not understood')
        error = .true.
        return
      endif
      call astro_message(seve%r,rname,mess)
    endif
  endif
  if (key.eq.'DUT1') then
    write (mess,'(a,1x,f0.8,1x,a)') 'DUT1 = UT1-UTC =',d_ut1,'s'
    call astro_message(seve%r,rname,mess)
  endif
  if (key.eq.'DTDT') then
    write (mess,'(a,1x,f0.8,1x,a)') 'DTDT = TDT-UTC =',d_tdt,'s'
    call astro_message(seve%r,rname,mess)
  endif
  if (key.eq.'ALL'.or.key.eq.'AZIMUTH') then
    write (mess,'(a,1x,a)') 'Zero azimuth is',azref
    call astro_message(seve%r,rname,mess)
  endif
  if (key.eq.'ALL'.or.key.eq.'NAME') then
    if (name_out.eq.-1) then
      chain='ALL (all names displayed)'
    else if (name_out.eq.0) then
      chain='*, (i.e. as searched for)'
    else
      write (chain,'(i0,1x,a,i0,a)') name_out,'(print name #',name_out,')'
    endif
    write (mess,'(a,1x,a)') 'Naming convention is',trim(chain)
    call astro_message(seve%r,rname,mess)
  endif
  if (key.eq.'ALL'.or.key.eq.'ATM') then
    write (mess,'(a,1x,a)') 'ATM version is',atm_get_version()
    call astro_message(seve%r,rname,mess)
!     call astro_message(seve%e,rname,'ATM version not yet available')
  endif
  if (key.eq.'ALL'.or.key.eq.'LINES') then
    if (molecules%doplot) then
      chain='ON'
      write (mess,'(a,1x,a)') 'Molecular lines display is',trim(chain)
      call astro_message(seve%r,rname,mess)
      write (mess,'(a,1x,a)') 'Line profile:',trim(molecules%profile)
      call astro_message(seve%r,rname,mess)
      write (mess,'(a,1x,f0.3,1x,a)') 'Line width:',molecules%width,'MHz'
      call astro_message(seve%r,rname,mess)
    else
      chain='OFF'
      write (mess,'(a,1x,a)') 'Molecular lines display is',trim(chain)
      call astro_message(seve%r,rname,mess)
    endif
  endif
  if (key.eq.'ALL'.or.key.eq.'FREQUENCY') then
    write (mess,'(a,1x,a)') 'Main (lower) frequency axis is',trim(freq_axis%main)
    call astro_message(seve%r,rname,mess)
    write (mess,'(a,1x,a)') 'Secondary (upper) frequency axis is',trim(freq_axis%second)
    call astro_message(seve%r,rname,mess)
  endif

  !
end subroutine astro_show
