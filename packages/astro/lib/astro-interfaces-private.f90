module astro_interfaces_private
  interface
    subroutine alma_baseband(line,error)
      use gbl_message
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      ! PDBI internal routine, support for command
      !	BASEBAND i freq sideband
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      logical :: error                  !
    end subroutine alma_baseband
  end interface
  !
  interface
    subroutine check_basebands(iband, error)
      use gbl_message
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: iband  !
      logical, intent(out) :: error         !
    end subroutine check_basebands
  end interface
  !
  interface
    subroutine alma_line(line,error)
      use gildas_def
      use gbl_message
      use ast_line
      use ast_astro
      use atm_params
      !---------------------------------------------------------------------
      ! @ private
      ! ALMA 	Internal routine, support for command
      !	FREQUENCY name Freq Sideband [Center]
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line  !
      logical, intent(out) :: error            !
    end subroutine alma_line
  end interface
  !
  interface
    function rffromif(if,band)
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      real(kind=4) :: rffromif             !
      real(kind=4), intent(in) :: if       !
      integer(kind=4), intent(in) :: band  !
    end function rffromif
  end interface
  !
  interface
    function iffromrf(rf,band)
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      real(kind=4) :: iffromrf             !
      real(kind=4), intent(in) :: rf       !
      integer(kind=4), intent(in) :: band  !
    end function iffromrf
  end interface
  !
  interface
    subroutine decoderf(rf,if,band)
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      real(kind=4), intent(in) :: rf        !
      real(kind=4), intent(out) :: if       !
      integer(kind=4), intent(out) :: band  !
    end subroutine decoderf
  end interface
  !
  interface
    subroutine alma_receiver(f,u,irec)
      use atm_params
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      real(kind=8), intent(in) :: f        ! Signal frequency
      real(kind=8), intent(in) :: u        ! Image frequency
      integer(kind=4), intent(in) :: irec  ! Receiver band
    end subroutine alma_receiver
  end interface
  !
  interface
    subroutine check_receiver(irec, error)
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: irec  !
      logical, intent(out) :: error        !
    end subroutine check_receiver
  end interface
  !
  interface
    subroutine check_flo1(irec, flo, chain, error)
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: irec  !
      real(kind=8), intent(in) :: flo      !
      character(len=*), intent(inout) :: chain
      logical, intent(out) :: error        !
    end subroutine check_flo1
  end interface
  !
  interface
    subroutine alma_spectral(line,error)
      use gbl_message
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      ! ALMA internal routine, support for command SPWINDOW
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      logical :: error                  !
    end subroutine alma_spectral
  end interface
  !
  interface
    subroutine alma_plot_spectral(ibb,specialunit)
      use ast_line
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      ! Plot spectral window on baseband ibb
      ! Window #ispw is plotted in dash
      !---------------------------------------------------------------------
      integer(kind=4) :: ibb            !
      integer(kind=4) :: specialunit    !
    end subroutine alma_plot_spectral
  end interface
  !
  interface
    subroutine explore_alma_modes(rwidth,polar,use,kmode,error)
      use gbl_message
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      ! Check if the requesed mode is available (depending on the ALMA Cycle)
      ! and/or if several sub-modes are possible
      !---------------------------------------------------------------------
      real(kind=4) :: rwidth            !
      integer(kind=4) :: polar          !
      real(kind=4) :: use               !
      integer(kind=4) :: kmode          !
      logical :: error                  !
    end subroutine explore_alma_modes
  end interface
  !
  interface
    subroutine check_spwindow(ispw, ibb, polar, use, rwidth, cfreq, restrict, error)
      use gbl_message
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      ! Check if the required spectral mode is allowed, depending on the ALMA Cycle
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: ispw   !
      integer(kind=4), intent(in) :: ibb    !
      integer(kind=4), intent(in) :: polar  !
      real(kind=4), intent(in) :: use       !
      real(kind=4), intent(in) :: rwidth    !
      real(kind=4), intent(in) :: cfreq     !
      logical, intent(in) :: restrict       !
      logical, intent(out) :: error         !
    end subroutine check_spwindow
  end interface
  !
  interface
    subroutine alma_plot_line
      use gbl_message
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      ! Main entry for plotting ALMA Frequency setup
      !---------------------------------------------------------------------
    end subroutine alma_plot_line
  end interface
  !
  interface
    subroutine alma_plot_dsb
      use gbl_message
      use ast_line
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      ! Plot of USB + LSB, as produced by the LINE command
      !---------------------------------------------------------------------
    end subroutine alma_plot_dsb
  end interface
  !
  interface
    subroutine alma_plot_dsb_corr(sideband)
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      ! Plot the correlator base bands onto the DSB plot
      !---------------------------------------------------------------------
      integer(kind=4) :: sideband       !
    end subroutine alma_plot_dsb_corr
  end interface
  !
  interface
    subroutine alma_plot_dsb_corr_tiny(sideband,ibb)
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      ! Plot the correlator basebands onto the DSB plot
      ! sideband = +1 if USB, -1 is LSB
      !---------------------------------------------------------------------
      integer(kind=4) :: sideband       !
      integer(kind=4) :: ibb            !
    end subroutine alma_plot_dsb_corr_tiny
  end interface
  !
  interface
    subroutine alma_plot_dsb_tiny(ibb)
      use gbl_message
      use ast_line
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      ! Tiny plot of USB + LSB
      !---------------------------------------------------------------------
      integer(kind=4) :: ibb            !
    end subroutine alma_plot_dsb_tiny
  end interface
  !
  interface
    subroutine alma_plot_baseband(ibb)
      use gbl_message
      use ast_line
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      ! Plot of the BASEBANDs
      !---------------------------------------------------------------------
      integer(kind=4) :: ibb            !
    end subroutine alma_plot_baseband
  end interface
  !
  interface
    subroutine atm_init(error)
      use gbl_message
      use atm_params
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      ! ATM
      !
      ! One time instantiation of the ASTRO variables which are used (as
      ! input and output) by the command ASTRO\ATM. These variables and
      ! their startup values are not needed if the programmer does want
      ! to make use of the command ASTRO\ATM.
      !---------------------------------------------------------------------
      logical, intent(inout) :: error   ! Logical error flag
    end subroutine atm_init
  end interface
  !
  interface
    subroutine astro_atm(line,error)
      use gildas_def
      use atm_params
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      ! ATM
      !
      ! Purpose: compute atmospheric properties
      !     from a full set of SIC variables
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine astro_atm
  end interface
  !
  interface
    subroutine astro_atm_print(error)
      use gbl_message
      use atm_params
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      ! ATM
      !
      ! Support routine for command:
      !   ASTRO\ATM /PRINT
      !---------------------------------------------------------------------
      logical, intent(inout) :: error   ! Logical error flag
    end subroutine astro_atm_print
  end interface
  !
  interface
    subroutine astro_atm_compute_0d(line,error)
      use atm_params
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   ASTRO\ATM
      ! i.e. compute (direct call to libatm) the atmosphere according to
      ! user inputs found in Sic variables (scalar version)
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine astro_atm_compute_0d
  end interface
  !
  interface
    subroutine astro_atm_compute_1d(line,error)
      use gildas_def
      use gbl_message
      use gkernel_types
      use atm_params
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   ASTRO\ATM /FREQ SIGNAL IMAGE
      ! i.e. compute (direct call to libatm) the atmosphere according to
      ! user inputs found in Sic variables, and to the frequency arrays
      ! passed to the option (1D version)
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine astro_atm_compute_1d
  end interface
  !
  interface
    subroutine astro_catalog(line,error)
      use gbl_message
      use ast_horizon
      use ast_astro
      use plot_molecules_globals
      !---------------------------------------------------------------------
      ! @ private
      ! ASTRO CATALOG name
      !	Defines the name of the source and molecular line catalog
      !---------------------------------------------------------------------
      character(len=*), intent(inout):: line           !
      logical, intent(inout) :: error                  !
    end subroutine astro_catalog
  end interface
  !
  interface
    subroutine get_catalog_name(file,file2,ext,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Decode the new catalog name
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: file    !
      character(len=*), intent(inout) :: file2   !
      character(len=*), intent(inout) :: ext     !
      logical,          intent(inout) :: error   !
    end subroutine get_catalog_name
  end interface
  !
  interface
    subroutine astro_constell(line,error)
      use gildas_def
      use image_def
      use gbl_format
      use gbl_message
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      !	ASTRO Command CONSTELLATIONS /DRAW [L] [M] [S] /NAME /BOUNDARIES
      !	PLOTS the constellations according to current projection, markers,
      !	etc... and the default constellation table GAG_CONSTELLATIONS
      !	format of constellation table is
      !	TABLE[n,6] where columns are:
      !	RA DEC (2000.0) magnitude code_star_name code_constell code_draw
      !	code_draw is 1 for M, 2 for L and 3 for M L
      !	this default overrided by / DRAW
      !	/NAME will plot also constellations names
      !	/BOUNDARIES will draw constellations boundaries
      !---------------------------------------------------------------------
      character(len=*):: line           !
      logical :: error                  !
    end subroutine astro_constell
  end interface
  !
  interface
    subroutine subpltbnds(array,n,m)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: n        ! 1st Array dimension
      integer(kind=4), intent(in) :: m        ! 2nd Array dimension
      real(kind=8), intent(in) :: array(n,m)  ! Array
    end subroutine subpltbnds
  end interface
  !
  interface
    subroutine slowgrid(lambda1,lambda2,delta1,delta2,code,equinox,error)
      use gkernel_types
      use ast_astro
      use ast_constant
      !---------------------------------------------------------------------
      ! @ private
      ! TRACE UN ARC DE (lambda1,delta1) a (lambda2,delta2)
      ! define dans le systeme CODE,
      ! le tout precesse a l'epoque courante si besoin est (CODE='EQ'),
      ! dans le systeme courant (FRAME) et la projection courante.
      !---------------------------------------------------------------------
      real(kind=8)     :: lambda1  !
      real(kind=8)     :: lambda2  !
      real(kind=8)     :: delta1   !
      real(kind=8)     :: delta2   !
      character(len=2) :: code     !
      real(kind=8)     :: equinox  !
      logical          :: error    !
    end subroutine slowgrid
  end interface
  !
  interface
    subroutine subpltcons(array,n,m, draw_code, plot_name)
      use gkernel_types
      use ast_astro
      use ast_constant
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: n          ! 1st Array dimension
      integer(kind=4), intent(in) :: m          ! 2nd Array dimension
      real(kind=8), intent(in) :: array(n,m)    ! Array
      integer(kind=4), intent(in) :: draw_code  ! Drawing option
      logical, intent(in) :: plot_name          ! Plot constellation names
    end subroutine subpltcons
  end interface
  !
  interface
    subroutine inothersystem(coord,equinox,lambda,beta,righta,decli,azimu,eleva,error)
      use gbl_message
      use ast_astro
      use ast_constant
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=2) :: coord         ! type of coord
      real(kind=8) :: equinox           ! if EQ
      real(kind=8) :: lambda            !
      real(kind=8) :: beta              !
      real(kind=8) :: righta            !
      real(kind=8) :: decli             !
      real(kind=8) :: azimu             !
      real(kind=8) :: eleva             !
      logical :: error                  !
    end subroutine inothersystem
  end interface
  !
  interface
    subroutine astro_draw(name,icat,argum,clip,error)
      use gbl_message
      use gkernel_types
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name
      integer(kind=4),  intent(in)    :: icat
      character(len=*), intent(in)    :: argum(:) ! /Draw arguments
      logical,          intent(in) :: clip          ! clipping of source names
      logical,          intent(inout) :: error
    end subroutine astro_draw
  end interface
  !
  interface
    subroutine astro_draw_parse(caller,line,optdraw,drawargs,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for
      !   SOURCE|PLANET /DRAW Arg1 ... ArgN
      !  Parse the arguments of the option /DRAW
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: caller
      character(len=*), intent(in)    :: line
      integer(kind=4),  intent(in)    :: optdraw
      character(len=*), intent(out)   :: drawargs(:)
      logical,          intent(inout) :: error
    end subroutine astro_draw_parse
  end interface
  !
  interface
    subroutine astro_frame(line,error)
      use gbl_message
      use ast_astro
      use ast_constant
      !---------------------------------------------------------------------
      ! @ private
      !	Define displayed reference frame
      !	FRAME EQ|HO		rectangular plot
      !	FRAME EQ|HO l b size	stereo square plot centered at l,b
      !				size in degrees
      !	FRAME HO  ZENITH|NORTH|EAST|WEST|SOUTH
      !---------------------------------------------------------------------
      character(len=*):: line           !
      logical :: error                  !
    end subroutine astro_frame
  end interface
  !
  interface
    subroutine astro_header (line,error)
      use gbl_message
      use ast_astro
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      !	Prints out current times, and observatory coordinates
      !---------------------------------------------------------------------
      character(len=*):: line           !
      logical :: error                  !
    end subroutine astro_header
  end interface
  !
  interface
    subroutine sup_header
      use gbl_message
      use ast_horizon
      use ast_astro
      use ast_constant
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
    end subroutine sup_header
  end interface
  !
  interface
    subroutine astro_horizon(line, error)
      use gbl_message
      use ast_horizon
      use ast_constant
      use ast_astro
      use ast_planets
      !---------------------------------------------------------------------
      ! @ private
      !	ASTRO command HORIZON
      !
      !     HORIZON h1 h2 h3 ...
      !     [/SOURCE [name]|[NEXT n]]]  #1
      !     [/PLANET [name]] #2
      !     [/ALTERNATE]  #3
      !     [/AIRMASSES] #4
      !     [/PROJECT] #5
      !     [/FLUX Fmin [Fmax]] #6
      !     [/NIGHT_MARKS] #7
      !     [/ELEVATION] #8
      !	Plots observing periods for sources, planets, ...
      ! 	on a time chart.
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line  !
      logical, intent(out) :: error            !
    end subroutine astro_horizon
  end interface
  !
  interface
    subroutine close_catalog(rname,lun,lunopened,mess,error)
      use gbl_message
      !---------------------------------------------------------------------
      !  @ private
      !  close current opened catalog file
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      integer(kind=4), intent(in) :: lun
      logical, intent(inout) :: lunopened
      character(len=*), intent(in) :: mess
      logical, intent(inout) :: error
    end subroutine close_catalog
  end interface
  !
  interface
    subroutine restore_astro_source(rname,asou,error)
      use gbl_message
      use ast_astro
      use ast_horizon
      use ast_constant
      !---------------------------------------------------------------------
      !  @ private
      !  save in a structure the parameters of the ASTRO%SOURCE before HORIZON
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(astro_source_mem), intent(inout) :: asou
      logical, intent(inout) :: error
    end subroutine restore_astro_source
  end interface
  !
  interface
    subroutine save_astro_source(asou,error)
      use gbl_message
      use ast_astro
      use ast_horizon
      use ast_constant
      !---------------------------------------------------------------------
      !  @ private
      !  save in a structure the parameters of the ASTRO%SOURCE before HORIZON
      !---------------------------------------------------------------------
      type(astro_source_mem), intent(inout) :: asou
      logical, intent(inout) :: error
    end subroutine save_astro_source
  end interface
  !
  interface
    subroutine do_horizon(rname,souname,cataline,coord,equinox,lambda,beta,vtyp,velo,redsh,elev,error)
      use gbl_message
      use ast_astro
      use ast_horizon
      !---------------------------------------------------------------------
      !  @ private
      !  Do the computation required before Horizon plot
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      character(len=*), intent(in) :: souname
      character(len=*), intent(in) :: cataline
      character(len=*), intent(in) :: coord
      real(kind=4), intent(in) :: equinox
      real(kind=8), intent(in) :: lambda, beta
      character(len=*), intent(in) ::vtyp
      real(kind=8), intent(in) ::velo
      real(kind=8), intent(in) ::redsh
      logical, intent(in) :: elev ! minimal elevatin (bure case)
      logical, intent(inout) :: error
    end subroutine do_horizon
  end interface
  !
  interface
    subroutine horizon_catalog(rname,lun,s1,s2,fmin,fmax,elev,error)
      use gbl_message
      use ast_astro
      use ast_horizon
      !---------------------------------------------------------------------
      !  @ private
      !  Loop on sources in catalog
      !  prepare horizon plot for a subset or all source
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      integer(kind=4), intent(in) :: lun ! Unit where catalog is opened
      integer(kind=4), intent(in) :: s1,s2  ! 1st and last source to plot
      real(kind=4), intent(in)  :: fmin,fmax ! flux range to be plotted
      logical, intent(in) :: elev ! minimal elevatin (bure case)
      logical, intent(inout) :: error
    end subroutine horizon_catalog
  end interface
  !
  interface
    subroutine horizon_sourcename(rname,souname,asou,lun,elev,error)
      use gbl_message
      use ast_astro
      use ast_horizon
      use ast_constant
      !---------------------------------------------------------------------
      !  @ private
      !  Check if input source is in memory or in the catalog
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      character(len=*), intent(in) :: souname ! source name
      type(astro_source_mem), intent(in) :: asou
      integer(kind=4), intent(in) :: lun      ! Unit where catalog is opened
      logical, intent(in) :: elev             ! minimal elevatin (bure case)
      logical, intent(inout) :: error
    end subroutine horizon_sourcename
  end interface
  !
  interface
    subroutine eq_planet(i_planet, s_3, error)
      use ast_astro
      use ast_constant
      use ast_planets
      !---------------------------------------------------------------------
      !  @ private
      !  Get Apparent RA and DEC of planet I_PLANET
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)  :: i_planet  !
      real(kind=8),    intent(out) :: s_3(3)    !
      logical,         intent(out) :: error     !
    end subroutine eq_planet
  end interface
  !
  interface
    subroutine set_project(line,low_el,error)
      use gildas_def
      use ast_horizon
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      !  ASTRO
      ! Read specific PdBI information in source catalog
      !
      ! Where is the documentation of this ????
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line    !
      integer(kind=4), intent(out) :: low_el  !
      logical, intent(out) :: error           !
    end subroutine set_project
  end interface
  !
  interface
    subroutine set_rise(name,s_3,error)
      use gbl_message
      use ast_horizon
      use ast_astro
      use ast_constant
      !---------------------------------------------------------------------
      ! @ private
      ! ASTRO
      !  Find the rising and setting times of object NAME
      !  Apparent coordinates are in s_3
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name    !
      real(kind=8),     intent(in)    :: s_3(3)  !
      logical,          intent(inout) :: error   !
    end subroutine set_rise
  end interface
  !
  interface
    subroutine sunrise(error)
      use ast_horizon
      use ast_astro
      use ast_constant
      !---------------------------------------------------------------------
      ! @ private
      !  Find the rising and setting times of SUN and the various twilight
      ! times.
      !   [1]:0 deg [2]:-6 deg (civil twilight) [3]:-12 deg (Nautical twilight)
      !   [4]:-18 astronomical twilight. Compute phase of MOON
      !  Updates corresponding values in the common.
      !---------------------------------------------------------------------
      logical, intent(inout) :: error   !
    end subroutine sunrise
  end interface
  !
  interface
    subroutine plot_horizon(project,elev,error)
      use gildas_def
      use ast_horizon
      use ast_astro
      use ast_constant
      !---------------------------------------------------------------------
      ! @ private
      ! 	ASTRO
      !---------------------------------------------------------------------
      logical, intent(in) :: project    !
      logical, intent(in) :: elev       !
      logical, intent(out) :: error     !
    end subroutine plot_horizon
  end interface
  !
  interface
    subroutine astro_init(error)
      use gbl_message
      use ast_line
      use ast_astro
      use ast_constant
      !---------------------------------------------------------------------
      ! @ private
      !	Initialize ASTRO parameters
      !---------------------------------------------------------------------
      logical :: error                  !
    end subroutine astro_init
  end interface
  !
  interface
    subroutine astro_j2000(line,error)
      use gbl_message
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      !	ASTRO Command :
      !	CONVERT out_catalog_name /J2000 obs_equinox ! change ref frame to J2000
      !	/PRECESS new_equinox		! precess to equinox
      !     /FLUX fmin                ! select sources larger than fmin Jy
      !     /NAME ncname iname        ! reserve ncname chars for names;
      !                               ! Put `IAU' standard name in iname^th position
      !     /INPUT POS filename pos
      !     /INPUT FLUX filename
      !---------------------------------------------------------------------
      character(len=*):: line           !
      logical :: error                  !
    end subroutine astro_j2000
  end interface
  !
  interface
    subroutine encode_line (olun, par, lpar, coord, equinox, lambda, beta, vtypx,  &
      velo, ftype, flux, spindex, comment, iname, ncname, aname, error)
      use ast_constant
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      integer(kind=4) :: olun           !
      character(len=*):: par            !
      integer(kind=4) :: lpar           !
      character(len=2) :: coord         !
      real(kind=4) :: equinox           !
      real(kind=8) :: lambda            !
      real(kind=8) :: beta              !
      character(len=2) :: vtypx         !
      real(kind=4) :: velo              !
      character(len=2) :: ftype         !
      real(kind=4) :: flux              !
      real(kind=4) :: spindex           !
      character(len=*):: comment        !
      integer(kind=4) :: iname          !
      integer(kind=4) :: ncname         !
      character(len=12) :: aname        !
      logical :: error                  !
    end subroutine encode_line
  end interface
  !
  interface
    subroutine get_input(key,file,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      character(len=*):: key            !
      character(len=*):: file           !
      logical :: error                  !
    end subroutine get_input
  end interface
  !
  interface
    subroutine astro_line(line,error)
      use gbl_message
      use ast_line
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      ! ASTRO 	Internal routine
      !	support for command
      !	LINE name Freq SB [Lock [Center [Harm]]]
      !	/MOLECULES FileName [Mol1 Mol2 ... Moln]        (1)
      !	/SPECTRAL Bandwidth LO3 Center [LO3 Center]     (2)
      !       /WIDTH  line_width                              (3)
      !       /IF_LIMITS ifmin ifmax                          (4)
      !       /BEST best                                      (5)
      !       /AUTO                                           (6)
      !       /VLBI                                           (7)
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line  !
      logical, intent(inout) :: error          !
    end subroutine astro_line
  end interface
  !
  interface
    subroutine plot_line(iflim, userlim, flo1, flo2, pdbi, fshift, sky, label,  &
      nmol, name, freq, width)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      real(kind=4) :: iflim(2)          !
      real(kind=4) :: userlim(2)        !
      real(kind=8) :: flo1              !
      real(kind=8) :: flo2              !
      logical :: pdbi                   !
      real(kind=8) :: fshift            !
      integer(kind=4) :: sky            !
      character(len=130) :: label       !
      integer(kind=4) :: nmol           !
      character(len=20) :: name(nmol)   !
      real(kind=8) :: freq(nmol)        !
      real(kind=4) :: width             !
    end subroutine plot_line
  end interface
  !
  interface
    subroutine plot_correlator(line, iflim, userlim, error)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*):: line           !
      real(kind=4) :: iflim(2)          !
      real(kind=4) :: userlim(2)        !
      logical :: error                  !
    end subroutine plot_correlator
  end interface
  !
  interface
    subroutine old_berkeley (line,error,berk)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*):: line           !
      logical :: error                  !
      character(len=*):: berk           !
    end subroutine old_berkeley
  end interface
  !
  interface
    subroutine berkeley (line, iflim, berk, error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*):: line           !
      real(kind=4) :: iflim(2)          !
      character(len=128) :: berk        !
      logical :: error                  !
    end subroutine berkeley
  end interface
  !
  interface
    subroutine astro_line_alma(line,error)
      use gbl_message
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*):: line           !
      logical :: error                  !
    end subroutine astro_line_alma
  end interface
  !
  interface
    subroutine astro_line_sma(line,error)
      use gbl_message
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      ! Support for the SMA frequency setup.
      !     This is simple enough (en principle):
      !     The IF is at 5+/-1 GHz
      !     The correlator covers the 2 GHz IF with ? channels
      !---------------------------------------------------------------------
      character(len=*):: line           !
      logical :: error                  !
    end subroutine astro_line_sma
  end interface
  !
  interface
    subroutine astro_message_set_id(id)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Alter library id into input id. Should be called by the library
      ! which wants to share its id with the current one.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: id  !
    end subroutine astro_message_set_id
  end interface
  !
  interface
    subroutine astro_message(mkind,procname,message)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Messaging facility for the current library. Calls the low-level
      ! (internal) messaging routine with its own identifier.
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in) :: mkind     ! Message kind
      character(len=*), intent(in) :: procname  ! Name of calling procedure
      character(len=*), intent(in) :: message   ! Message string
    end subroutine astro_message
  end interface
  !
  interface
    subroutine ephem_pack_init(gpack_id,error)
      !----------------------------------------------------------------------
      ! @ private
      !
      !----------------------------------------------------------------------
      integer(kind=4) :: gpack_id       !
      logical :: error                  !
    end subroutine ephem_pack_init
  end interface
  !
  interface
    subroutine ephem_pack_clean(error)
      !----------------------------------------------------------------------
      ! @ private
      !
      !----------------------------------------------------------------------
      logical, intent(inout) :: error   !
    end subroutine ephem_pack_clean
  end interface
  !
  interface
    subroutine astro_pack_init(gpack_id,error)
      !----------------------------------------------------------------------
      ! @ private
      !
      !----------------------------------------------------------------------
      integer(kind=4) :: gpack_id       !
      logical :: error                  !
    end subroutine astro_pack_init
  end interface
  !
  interface
    subroutine astro_pack_clean(error)
      !----------------------------------------------------------------------
      ! @ private
      !
      !----------------------------------------------------------------------
      logical, intent(inout) :: error   !
    end subroutine astro_pack_clean
  end interface
  !
  interface
    subroutine astro_planet(line,error)
      use gildas_def
      use gbl_message
      use ast_astro
      use ast_constant
      use ast_planets
      !---------------------------------------------------------------------
      ! @ private
      !	ASTRO Command :
      !	PLANET name|ALL SURFACE lon lat /DRAW /QUIET
      !---------------------------------------------------------------------
      character(len=*):: line           !
      logical :: error                  !
    end subroutine astro_planet
  end interface
  !
  interface
    subroutine get_planet_tbright(rname,ip,freq,hdist,tb,error)
      use gbl_message
      use ast_constant
      use ast_planets
      !---------------------------------------------------------------------
      ! @ private
      ! retrieve plant brightness temperature
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: rname
      integer(kind=4), intent(inout) ::ip
      real(kind=8), intent(inout) :: freq
      real(kind=8), intent(inout) :: hdist
      real(kind=8), intent(inout) :: tb
      logical, intent(inout) :: error
    end subroutine get_planet_tbright
  end interface
  !
  interface
    subroutine read_tbright_file(rname,mfile,pmodel,error)
      use gildas_def
      use gbl_message
      use ast_constant
      use ast_planets
      !---------------------------------------------------------------------
      ! @ private
      ! read planet brightness temperature from file
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: rname
      character(len=*), intent(in) :: mfile
      type(planet_spec_t), intent(inout) :: pmodel
      logical, intent(inout) :: error
    end subroutine read_tbright_file
  end interface
  !
  interface
    subroutine interp_tbright_file(rname,pmodel,freq,tb,error)
      use gildas_def
      use gbl_message
      use ast_constant
      use ast_planets
      !---------------------------------------------------------------------
      ! @ private
      ! interpolation of brightness temperature in stored model
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: rname
      type(planet_spec_t), intent(inout) :: pmodel
      real(kind=8), intent(inout) :: freq
      real(kind=8), intent(inout) :: tb
      logical, intent(inout) :: error
    end subroutine interp_tbright_file
  end interface
  !
  interface
    subroutine astro_set_command(line,error)
      use gbl_message
      use ast_line
      use ast_astro
      use plot_molecules_globals
      use frequency_axis_globals
      !---------------------------------------------------------------------
      ! @ private
      ! ASTRO Command SET DUT1|DTDT|AZ|FLUX
      !---------------------------------------------------------------------
      character(len=*):: line           !
      logical :: error                  !
    end subroutine astro_set_command
  end interface
  !
  interface
    subroutine astro_set_lines(line,molec,error)
      use gbl_message
      use astro_types
      !---------------------------------------------------------------------
      ! @ private
      ! Define the way the molecular lines will be plotted
      !---------------------------------------------------------------------
      character(len=*), intent(inout):: line           !
      type(plot_molecules_t), intent(inout) :: molec
      logical, intent(inout) :: error
    end subroutine astro_set_lines
  end interface
  !
  interface
    subroutine astro_set_frequency(line,freq_axis,error)
      use gbl_message
      use astro_types
      use ast_astro
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      ! Define the way the molecular lines will be plotted
      !---------------------------------------------------------------------
      character(len=*), intent(inout):: line           !
      type(frequency_axis_t), intent(inout) :: freq_axis
      logical, intent(inout) :: error
    end subroutine astro_set_frequency
  end interface
  !
  interface
    subroutine astro_source(line,error)
      use gildas_def
      use gbl_message
      use ast_astro
      use ast_constant
      use my_receiver_globals
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for ASTRO Command
      !    SOURCE [name]
      ! 1    [/DRAW MARKER|LINE|SYMBOL|FULL|BOX]
      ! 2    [/ALTERNATE]
      ! 3    [/FLUX Fmin Fmax]
      ! 4    [/SUN]
      ! 5    [/PRINT]
      ! 6    [/CURSOR]
      ! 7    [/QUIET]
      ! 8    [/VARIABLE VarName]
      ! 9    [/NOCLIP] plotting option
      ! 10   [/RESET] to put all source related values to 0
      ! 11   [/DOPPLER] to define a LSR or z to be taken into account in freq
      !                    computations, without any source defined
      !  Search the source catalog for source 'name'
      !  Parse the line found for coords and velocities
      !  Convert coordinates to internal format
      !---------------------------------------------------------------------
      character(len=*):: line           !
      logical :: error                  !
    end subroutine astro_source
  end interface
  !
  interface
    subroutine expand_line(line,oline,oll,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Expand the current command line for all its expressions (i.e.
      ! single-quoted arguments). Other arguments are not modified; in
      ! particular we need to avoid translating a random character string as
      ! a valid variable name. Double-quotes are preserved. For example:
      !   Hello PI "PI PI" 'pi' '2*pi'
      ! translates as
      !   Hello PI "PI PI" 3.1415926535898 6.2831853071796
      ! Beware the returned line can not be used with the SIC_R4 et al
      ! subroutines as the command line pointers are not relevant for the
      ! modified version.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      character(len=*), intent(out)   :: oline  ! Output line
      integer(kind=4),  intent(out)   :: oll    ! Output line length
      logical,          intent(inout) :: error  !
    end subroutine expand_line
  end interface
  !
  interface
    subroutine decode_line(c_line,npar,coord,equinox,lambda,beta,vtyp,velo,redsh,ftype,  &
      flux,spindex,mag,error)
      use gbl_constant
      use gbl_message
      use ast_constant
      !---------------------------------------------------------------------
      ! @ private
      !  Decode a standard source catalog line
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: c_line   ! Input line
      integer(kind=4),  intent(inout) :: npar     ! pointer to first data word (coords or type)
      character(len=2), intent(out)   :: coord    ! Coordinate type
      real(kind=4),     intent(out)   :: equinox  ! Equinox
      real(kind=8),     intent(out)   :: lambda   !
      real(kind=8),     intent(out)   :: beta     !
      character(len=2), intent(out)   :: vtyp     ! velocity type
      real(kind=8),     intent(out)   :: velo     ! velocity
      real(kind=8),     intent(out)   :: redsh     ! redshift
      character(len=2), intent(out)   :: ftype    ! flux type
      real(kind=4),     intent(out)   :: flux     ! flux value
      real(kind=4),     intent(out)   :: spindex  ! spectral index
      real(kind=4),     intent(out)   :: mag(9)   ! magnitudes V,R,I,J,H,K,L,M,N
      logical,          intent(inout) :: error    !
    end subroutine decode_line
  end interface
  !
  interface
    subroutine source_doppler_only(rname,line,optdoppler,error)
      use gildas_def
      use gbl_message
      use ast_astro
      !----------------------------------------------------------------------
      ! @ private
      ! Handle the case where only a redshift or a lsr velocity is provided, 
      ! no precise source
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      character(len=*), intent(in) :: line
      integer(kind=4), intent(in)  :: optdoppler
      logical, intent(inout) :: error
    end subroutine source_doppler_only
  end interface
  !
  interface
    subroutine suffix(par,lpar,r1,r2)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*):: par            !
      integer(kind=4) :: lpar           !
      real(kind=4) :: r1                !
      real(kind=4) :: r2                !
    end subroutine suffix
  end interface
  !
  interface
    subroutine check_source(name,lname,par,lpar,found)
      use ast_astro
      use ast_constant
      !---------------------------------------------------------------------
      ! @ private
      ! This subroutine:
      ! 1) * if 'found' is not yet .true., check is the input 'name' matches
      !      one of the fields in 'par' (several values can be separated by
      !      |). Matching is case insensitive.
      !    * if 'found' is already .true., skip the matching part and does
      !      only the following action.
      ! 2) if the name is found, update 'par' according to the ASTRO\SET
      !    NAME tuning:
      !     -1: all the variants are kept in 'par'
      !      0: the variant used for searching ('name') is returned in 'par'
      !    Val: the Val-th variant is returned in 'par'
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name   ! Name to search for
      integer(kind=4),  intent(in)    :: lname  ! Length of 'name'
      character(len=*), intent(inout) :: par    ! Name to check against
      integer(kind=4),  intent(inout) :: lpar   ! Length of 'par'
      logical,          intent(inout) :: found  ! If already .true., do not search
    end subroutine check_source
  end interface
  !
  interface
    subroutine source_variable(filename,varname,fmin,fmax,error)
      use gbl_format
      use gbl_message
      use ast_params
      !---------------------------------------------------------------------
      ! @ private
      !  Load the catalog in a user structure
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: filename  !
      character(len=*), intent(in)    :: varname   !
      real(kind=4),     intent(in)    :: fmin      !
      real(kind=4),     intent(in)    :: fmax      !
      logical,          intent(inout) :: error     !
    end subroutine source_variable
  end interface
  !
  interface
    subroutine astro_uv(line,error)
      use gildas_def
      use image_def
      use gkernel_types
      use gbl_message
      use ast_astro
      use ast_constant
      use atm_params
      !---------------------------------------------------------------------
      ! @ private
      ! ASTRO Support for command
      ! UV_TRACKS Station1_ .. StationN
      !       [/FRAME Max_U] #1
      !       [/HOUR Hmin Hmax] #2
      !       [/TABLE Name] #3
      !       [/HORIZON Elv] #4
      !       [/INTEGRATION T] #5
      !       [/STATIONS ALL|list] #6
      !       [/SIZE size1 nsize1 size2 nsize2 ...] #7
      !       [/OFFSET offset] #8
      !       [/WEIGHT mode [Jy_per_K Bandwidth]] #9
      !------------------------------------------------------------------------
      ! Notes:
      ! - RL 09-nov-1998 /SIZE must be given to antenna size.
      ! - OFFSET is the position of the fixed delay line which corresponds to the
      ! the first station. It is then necessary to insure that in the following
      ! the tables X(MSTAT) reflect the order in which the stations were entered
      ! in the command line.
      !
      ! Note about XX YY ZZ TT, the positions written in the station's file:
      ! TT is (or should be) the total length (in m) from the center of
      ! the aperture to a reference plane common to all beams (preferably the
      ! entrance plane of delay lines). For XX, YY and ZZ, these are the 3
      ! components of the vector going from the earth centre to the location of
      ! the station on the surface of the earth (local equatorial coordinates),
      ! with XX along this direction, YY in the W-E direction
      ! (positive towards EAST) and ZZ in the N direction. Given local horizontal
      ! (on earth surface) coordinates x,y,z of the station,
      ! x towards Local east, y toward N and z toward Zenith,
      ! the transformation matrix is :
      !
      ! XX      / 0,-sin(lat),  cos(lat) \   x
      ! YY =    | 1,        0,         0 | * y
      ! ZZ      \ 0, cos(lat),  sin(lat) /   z
      !
      ! Where lat is the latitude of the interferometer center
      ! Note about u,v,w:
      ! the transformation from the equatorial coordinates Bx,By,Bz
      ! of a Baseline B to u,v,w is given by the matrix:
      ! u       / sin(H)       , cos(H)       ,      0 \   Bx
      ! v  =    |-sin(D)*cos(H), sin(D)*sin(H), cos(D) | * By
      ! w       \ cos(D)*cos(H),-cos(D)*sin(H), sin(D) /   Bz
      ! where H is the hour angle and D the declination of the source
      !
      ! which give ellipses of centre (0, (B/lambda)*sin(d)*cos(D)), with semi-major
      ! axis (B/lambda)*cos(d) and semi-minor axis (B/lambda)*cos(d)*sin(D)
      !
      ! Taking into account a maximum throw for delay lines may be useful for
      ! RadioInterferometers too (some may have cable length limitations) but
      ! is not desirable by default for them. However, the geometrical delay between
      ! apertures ('aerials'? 'telescopes'?) is used deep in the program. To avoid
      ! problems, I suggest to define the throw of fake 'delay lines' for radio
      ! interferometers to 2 a.u = 3.0E11 m ,with OFFSET to 1 a.u.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine astro_uv
  end interface
  !
  interface
    subroutine uv_do_baseline(rname,uvang,uvsta_i,uvsta_j,uk,vk,wk,error)
      use gbl_message
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      !
      ! compute baseline coordinates
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: rname   ! command name
      type(uv_track_angles_t), intent(in) :: uvang ! current angle to compute distance
      type(uv_track_station_t), intent(in) :: uvsta_i,uvsta_j ! current stations
      real(kind=4), intent(inout) :: uk,vk,wk
      logical, intent(inout) :: error 
    end subroutine uv_do_baseline
  end interface
  !
  interface
    subroutine uv_check_shadow(rname,uvang,uvsta_i,uvsta_j,ok_i,ok_j,error)
      use gbl_message
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      !
      ! check whether the 2 antennas on stations i,j have shadowing
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: rname   ! command name
      type(uv_track_angles_t), intent(in) :: uvang ! current angle to compute distance
      type(uv_track_station_t), intent(in) :: uvsta_i,uvsta_j ! current stations
      logical, intent(inout) :: ok_i,ok_j
      logical, intent(inout) :: error 
    end subroutine uv_check_shadow
  end interface
  !
  interface
    subroutine uv_check_delay(rname,uvang,uvsta_i,uvsta_j,uvsta_ref,throw,delay_ok,error)
      use gbl_message
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      !
      ! check whether the delay is not bigger than the limit (for observatories using fixed delay lines)
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: rname   ! command name
      type(uv_track_angles_t), intent(in) :: uvang ! current angle to compute distance
      type(uv_track_station_t), intent(in) :: uvsta_i,uvsta_j,uvsta_ref ! current stations
      real(kind=4), intent(in) :: throw
      logical, intent(inout) :: delay_ok
      logical, intent(inout) :: error 
    end subroutine uv_check_delay
  end interface
  !
  interface
    subroutine uv_match_station(rname,uvin,fname,istation,match,error)
      use gbl_message
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      !
      ! check if station of the current line in the file matches the requested station
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: rname   ! command name
      type(uv_track_in_t), intent(in) :: uvin
      character(len=*) :: fname ! station name as in station file
      character(len=*) :: istation ! station name as user input
      logical, intent(inout) :: match 
      logical, intent(inout) :: error 
    end subroutine uv_match_station
  end interface
  !
  interface
    subroutine uv_reset_plot(rname,uvin,arr,lextr,ff,error)
      use gbl_message
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      !
      ! reset the plot upon /frame option
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: rname   ! command name
      type(uv_track_in_t), intent(in) :: uvin
      real(kind=4), intent(in) :: arr
      real(kind=4), intent(inout) :: lextr
      real(kind=8), intent(inout) :: ff        !
      logical, intent(inout) :: error 
    end subroutine uv_reset_plot
  end interface
  !
  interface
    subroutine uv_get_arraysize(rname,fsta,arr,nign,off,error)
      use gbl_message
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      !
      ! extract useful info from station file
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: rname   ! command name
      character(len=*), intent(in) :: fsta   ! station file name
      real(kind=4), intent(inout) :: arr        !
      integer(kind=4), intent(inout) :: nign
      real(kind=4), intent(inout) :: off(4) ! offset wrt to origin to plot positions of stations
      logical, intent(inout) :: error 
    end subroutine uv_get_arraysize
  end interface
  !
  interface
    subroutine astro_uv_legend(rname,freq,decs,stalis,oldlis,del_mess,nused, count, nc,nces,error)
      use gbl_message
      use phys_const
      !---------------------------------------------------------------------
      ! @ private
      !
      ! draw the legend (stations used, declination, frequency)
      ! at the right place according to the plot_page orientation
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: rname   ! command name
      real(kind=8), intent(in) :: freq         ! Frequency
      real(kind=8), intent(in) :: decs         ! Declination [radian]
      character(len=*), intent(in) :: stalis   !list of used stations
      integer(kind=4), intent(in) :: nces     !positions to split the displayed line if needed
      character(len=*), intent(in) :: oldlis   !previous list of used stations
      character(len=*), intent(in) :: del_mess   !message about delay if present
      integer(kind=4), intent(in) :: nused     ! number of used stations
      integer(kind=4), intent(inout) :: count  !
      integer(kind=4), intent(in) :: nc        !
      logical, intent(inout) :: error 
    end subroutine astro_uv_legend
  end interface
  !
  interface
    subroutine uvbox(base,freq)
      use phys_const
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      real(kind=4), intent(in) :: base  ! Baseline length
      real(kind=8), intent(in) :: freq  ! Frequency
    end subroutine uvbox
  end interface
  !
  interface
    subroutine astro_close_table(x)
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      ! To be re-written using
      !    gdf_write_image(header,array,error)
      ! AND may be also
      !    gdf_close_image(header,error)
      !---------------------------------------------------------------------
      type(gildas), intent(inout) :: x  !
    end subroutine astro_close_table
  end interface
  !
  interface
    subroutine astro_extend_table(x,name,nvis,dec,freq,error)
      use gildas_def
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Re-written for new Imgage I/O interface
      !---------------------------------------------------------------------
      type(gildas),     intent(inout) :: x      ! Image structure
      character(len=*), intent(in)    :: name   ! File name
      integer(kind=4),  intent(in)    :: nvis   ! Number of visibilities to add
      real(kind=8),     intent(in)    :: dec    ! Declination
      real(kind=8),     intent(in)    :: freq   ! Frequency
      logical,          intent(out)   :: error  ! Logical error flag
    end subroutine astro_extend_table
  end interface
  !
  interface
    subroutine astro_init_table(x,name,nvis,dec,freq,band,obs,error)
      use gildas_def
      use image_def
      use gbl_format
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(gildas),     intent(inout) :: x      ! Image structure
      character(len=*), intent(in)    :: name   ! File name
      integer(kind=4),  intent(in)    :: nvis   ! Number of visibilities to write
      real(kind=8),     intent(in)    :: dec    ! Declination
      real(kind=8),     intent(in)    :: freq   ! Frequency
      real(kind=4),     intent(in)    :: band   ! Frequency resolution
      character(len=*), intent(in)    :: obs    ! Current observatory
      logical,          intent(out)   :: error  ! Logical error flag
    end subroutine astro_init_table
  end interface
  !
  interface
    subroutine fill_table (u,v,w,d,t,ideb,ifin,visi,el,wkey)
      use atm_params
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      real(kind=4),    intent(in)  :: u         ! U coordinate
      real(kind=4),    intent(in)  :: v         ! V
      real(kind=4),    intent(in)  :: w         ! W
      real(kind=4),    intent(in)  :: d         ! Date
      real(kind=4),    intent(in)  :: t         ! Time
      integer(kind=4), intent(in)  :: ideb      ! Start Antenna
      integer(kind=4), intent(in)  :: ifin      ! End Antenna
      real(kind=4),    intent(out) :: visi(10)  ! Visibility
      real(kind=8),    intent(in)  :: el        ! Elevation
      integer(kind=4), intent(in)  :: wkey      ! Code for Weights
    end subroutine fill_table
  end interface
  !
  interface
    subroutine matmul(mat,a,b,n)
      !---------------------------------------------------------------------
      ! @ private
      ! version 1.0  mpifr cyber edition  22 may 1977.  G.Haslam
      ! version 2.0  iram
      !
      ! this routine provides the transformation of vector a to vector b
      ! using the n dimensional direction cosine array, mat.
      !---------------------------------------------------------------------
      real(kind=8),    intent(in)  :: mat(3,3)  !
      real(kind=8),    intent(in)  :: a(3)      !
      real(kind=8),    intent(out) :: b(3)      !
      integer(kind=4), intent(in)  :: n         !
    end subroutine matmul
  end interface
  !
  interface
    subroutine amset(obslat,amat)
      use ast_constant
      !---------------------------------------------------------------------
      ! @ private
      !   subroutine to provide transformation matrix from hour angle - dec
      !       into azimuth - elevation.
      !---------------------------------------------------------------------
      real(kind=8), intent(in)  :: obslat   ! in degrees
      real(kind=8), intent(out) :: amat(9)  !
    end subroutine amset
  end interface
  !
  interface
    subroutine dangle(along,alat,a)
      use ast_constant
      !---------------------------------------------------------------------
      ! @ private
      ! version 1.0  mpifr cyber edition  22 may 1977.
      ! see nod 2 manual page m 9.1
      ! c.g haslam       mar  1972.
      ! this routine recovers the longitude and latitude angles, along
      ! (0 - 360) and alat ( +.- 180) in deg_to_radrees, which correspond to the
      ! vector a.
      !---------------------------------------------------------------------
      real(kind=8), intent(out) :: along  !
      real(kind=8), intent(out) :: alat   !
      real(kind=8), intent(in)  :: a(3)   !
    end subroutine dangle
  end interface
  !
  interface
    subroutine astro_setuv_weight(el)
      use gildas_def
      use atm_params
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      ! ATM - using global astro variables
      !---------------------------------------------------------------------
      real(kind=8), intent(in) :: el
    end subroutine astro_setuv_weight
  end interface
  !
  interface
    subroutine noema_define_pfx(pfx,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! defines polyfix backends
      !-----------------------------------------------------------------------
      type(pfx_t), intent(inout)    :: pfx
      logical, intent(inout)            :: error
    end subroutine noema_define_pfx
  end interface
  !
  interface
    subroutine noema_pfx_mode_message(rname,pfxm,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Display message on correlator mode selection
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname        ! command line
      type(pfx_mode_t), intent(in) :: pfxm
      logical, intent(inout) :: error
    end subroutine noema_pfx_mode_message
  end interface
  !
  interface
    subroutine noema_spw(line,error)
      use gbl_message
      use gkernel_types
      use ast_astro
      use my_receiver_globals
      use plot_molecules_globals
      use frequency_axis_globals
      !-----------------------------------------------------------------------
      ! @ private
      ! Set and show the backend coverage of NOEMA
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(inout) :: line        ! command line
      logical, intent(inout) :: error
    end subroutine noema_spw
  end interface
  !
  interface
    subroutine noema_spw_online(line,error)
      use gbl_message
      use gkernel_types
      use ast_astro
      use my_receiver_globals
      use plot_molecules_globals
      use frequency_axis_globals
      use string_parser_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Set and show the backend coverage of NOEMA
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(inout) :: line        ! command line
      logical, intent(inout) :: error
    end subroutine noema_spw_online
  end interface
  !
  interface
    subroutine noema_sort_spw(spwout,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! sort the list of spw
      !
      !-----------------------------------------------------------------------
      type(spw_output_t), intent(inout) :: spwout
      logical, intent(inout) :: error
    end subroutine noema_sort_spw
  end interface
  !
  interface
    function sort_spw_gt(m,l)
      use my_receiver_globals
      !---------------------------------------------------------------------
      ! @ private
      ! Sorting function for noema_sort_spw (greater than)
      ! ---
      !  NB: THIS SORTS THE GLOBAL SPW STRUCTURE SHARED THROUGH MODULE
      !      MY_RECEIVER_GLOBALS!!!
      !---------------------------------------------------------------------
      logical :: sort_spw_gt
      integer(kind=4), intent(in) :: m,l
    end function sort_spw_gt
  end interface
  !
  interface
    function sort_spw_ge(m,l)
      use my_receiver_globals
      !---------------------------------------------------------------------
      ! @ private
      ! Sorting function for noema_sort_spw (greater than)
      ! ---
      !  NB: THIS SORTS THE GLOBAL SPW STRUCTURE SHARED THROUGH MODULE
      !      MY_RECEIVER_GLOBALS!!!
      !---------------------------------------------------------------------
      logical :: sort_spw_ge
      integer(kind=4), intent(in) :: m,l
    end function sort_spw_ge
  end interface
  !
  interface
    subroutine noema_sort_spw2(spwout,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! sort the list of spw in spw2
      ! NB2: Duplication of noema_sort_spw to be able to work on 2 list of spw in parralel
      !      Could be merged using contains but requires gfortran > 4.4
      !
      !-----------------------------------------------------------------------
      type(spw_output_t), intent(inout) :: spwout
      logical, intent(inout) :: error
    end subroutine noema_sort_spw2
  end interface
  !
  interface
    function sort_spw2_gt(m,l)
      use my_receiver_globals
      !---------------------------------------------------------------------
      ! @ private
      ! Sorting function for noema_sort_spw (greater than)
      ! ---
      !  NB: THIS SORTS THE GLOBAL SPW2 STRUCTURE SHARED THROUGH MODULE
      !      MY_RECEIVER_GLOBALS!!!
      ! NB2: Duplication of sort_spw_ge to be able to work on 2 list of spw in parralel
      !      Could be merged using contains but requires gfortran > 4.4
      !---------------------------------------------------------------------
      logical :: sort_spw2_gt
      integer(kind=4), intent(in) :: m,l
    end function sort_spw2_gt
  end interface
  !
  interface
    function sort_spw2_ge(m,l)
      use my_receiver_globals
      !---------------------------------------------------------------------
      ! @ private
      ! Sorting function for noema_sort_spw (greater than)
      ! ---
      !  NB: THIS SORTS THE GLOBAL SPW2 STRUCTURE SHARED THROUGH MODULE
      !      MY_RECEIVER_GLOBALS!!!
      ! NB2: Duplication of sort_spw_ge to be able to work on 2 list of spw in parralel
      !      Could be merged using contains but requires gfortran > 4.4
      !---------------------------------------------------------------------
      logical :: sort_spw2_ge
      integer(kind=4), intent(in) :: m,l
    end function sort_spw2_ge
  end interface
  !
  interface
    subroutine noema_config_spw_byfreq(noema,error)
      use gbl_message
      use astro_noema_type
      !-----------------------------------------------------------------------
      ! @ private
      ! recognize which mode and type of chunk is being configured
      !
      !-----------------------------------------------------------------------
      type(noema_t), intent(inout) :: noema
      logical, intent(inout) :: error
    end subroutine noema_config_spw_byfreq
  end interface
  !
  interface
    subroutine noema_config_spw_bychunk(spwin,inoema,error)
      use gbl_message
      use gkernel_types
      use astro_noema_type
      !----------------------------------------------------------------------
      ! @ private
      ! configure spw by chunk intervals, in all selected unit
      !
      !-----------------------------------------------------------------------
      type(sic_listi4_t), intent(in) :: spwin
      type(noema_t), intent(inout) :: inoema
      logical, intent(inout) :: error
    end subroutine noema_config_spw_bychunk
  end interface
  !
  interface
    subroutine noema_config_spw_bychunk_unit(spwin,rdesc,rsou,rtune,noema_if,pfxu,spw,error)
      use gbl_message
      use gkernel_types
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! configure spw by chunk intervals for a given unit
      !
      !--------------------------------fit---------------------------------------
      type(sic_listi4_t), intent(in) :: spwin
      type(receiver_desc_t), intent(in) :: rdesc
      type(receiver_source_t), intent(in) :: rsou
      type(receiver_tune_t), intent(in) :: rtune
      type(noema_if_t), intent(in) :: noema_if
      type(pfx_unit_t), intent(inout) :: pfxu
      type(spw_t), intent(inout) :: spw
      logical, intent(inout) :: error
    end subroutine noema_config_spw_bychunk_unit
  end interface
  !
  interface
    subroutine noema_check_chunks(rname,ic1,ic2,spw,pfxunit,needdef,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! check if all chunk of a spw are already defined. 
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      integer(kind=4), intent(in)   :: ic1,ic2
      type(spw_t), intent(in) :: spw
      type(pfx_unit_t), intent(in) :: pfxunit
      logical, intent(inout) :: needdef
      logical, intent(inout) :: error
    end subroutine noema_check_chunks
  end interface
  !
  interface
    subroutine noema_add_spw(rdesc,rsou,rtune,ic1,ic2,pfxunit,spw,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! add the defined window (might be different from input)
      !
      !-----------------------------------------------------------------------
      type(receiver_desc_t), intent(in) :: rdesc
      type(receiver_source_t), intent(in) :: rsou
      type(receiver_tune_t), intent(in) :: rtune
      integer(kind=4), intent(in)   :: ic1,ic2
      type(pfx_unit_t), intent(inout) :: pfxunit
      type(spw_t), intent(inout) :: spw
      logical, intent(inout) :: error
    end subroutine noema_add_spw
  end interface
  !
  interface
    subroutine noema_find_chunks(rdesc,rtune,rsou,spwcomm,pfxunit,ic1,ic2,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! recognize which mode and type of chunk is being configured
      !
      !-----------------------------------------------------------------------
      type(receiver_desc_t), intent(in) :: rdesc
      type(receiver_tune_t), intent(in) :: rtune
      type(receiver_source_t), intent(in) :: rsou
      type(spw_comm_t), intent(inout) :: spwcomm
      type(pfx_unit_t), intent(inout) :: pfxunit
      integer(kind=4), intent(out)  :: ic1,ic2
      logical, intent(inout) :: error
    end subroutine noema_find_chunks
  end interface
  !
  interface
    subroutine noema_chunk_minmax(rdesc,rsou,rtune,bb_code,sb_code,pfxtype,ich,chmin,chmax,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! compute rest frequency limits of a given chunk
      !
      !-----------------------------------------------------------------------
      type(receiver_desc_t), intent(in) :: rdesc
      type(receiver_source_t), intent(in) :: rsou
      type(receiver_tune_t), intent(in) :: rtune
      integer(kind=4), intent(in)   :: bb_code !
      integer(kind=4), intent(in)   :: sb_code !
      type(pfx_type_t), intent(in) :: pfxtype
      integer(kind=4), intent(in)  :: ich
      real(kind=8), intent(out)  :: chmin,chmax
      logical, intent(inout) :: error
    end subroutine noema_chunk_minmax
  end interface
  !
  interface
    subroutine noema_config_chunks(rname,ic1,ic2,pfxunit,spwcomm,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! recognize which mode and type of chunk is being configured
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(in)  :: rname
      integer(kind=4), intent(in)  :: ic1,ic2
      type(pfx_unit_t), intent(inout) :: pfxunit
      type(spw_comm_t),intent(in)      :: spwcomm
      logical, intent(inout) :: error
    end subroutine noema_config_chunks
  end interface
  !
  interface
    subroutine pfx_fixed_spw(rname,rdesc,rsou,rtune,pfxu,spw,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Define coverage of chunks without flexibility
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(receiver_desc_t), intent(inout) :: rdesc
      type(receiver_source_t), intent(inout) :: rsou
      type(receiver_tune_t), intent(inout) :: rtune
      type(pfx_unit_t), intent(inout) :: pfxu
      type(spw_t), intent(inout) :: spw
      logical, intent(inout) :: error
    end subroutine pfx_fixed_spw
  end interface
  !
  interface
    subroutine noema_assign_units(rname,nifproc,pfx,rdesc,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! assign IF cable to polyfix units
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(correlator_input_t), intent(in) :: nifproc
      type(pfx_t), intent(inout) :: pfx
      logical, intent(inout) :: error
      type(receiver_desc_t), intent(in) :: rdesc
    end subroutine noema_assign_units
  end interface
  !
  interface
    subroutine noema_plot_selpfx(rname,inoema,cplot,cata,drawaxis,error)
      use gbl_message
      use astro_noema_type
      !-----------------------------------------------------------------------
      ! @ private
      ! Displays the backend coverage
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(noema_t), intent(in) :: inoema
      type(current_boxes_t), intent(inout) :: cplot
      type(plot_molecules_t), intent(in) :: cata
      type(frequency_axis_t), intent(in) :: drawaxis
      logical, intent(inout) :: error
    end subroutine noema_plot_selpfx
  end interface
  !
  interface
    subroutine rec_def_fbox_chunks(if2min,if2max,pfxu,ubox,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Prepare the box for plotting chunks numbers [input is IF2]
      !-----------------------------------------------------------------------
      real(kind=8), intent(in)                      :: if2min,if2max ! IF2 frequency limits
      type(frequency_box_user_t), intent(inout)     :: ubox ! box parameters user
      type(pfx_unit_t), intent(in)                  :: pfxu ! correlator parameters
      logical, intent(inout)                        :: error
    end subroutine rec_def_fbox_chunks
  end interface
  !
  interface
    subroutine noema_draw_spw(spwout,pfxunit,fbox,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Displays the coverage of the spw in a given correlator unit
      !
      !-----------------------------------------------------------------------
      type(spw_output_t), intent(in) :: spwout
      type(pfx_unit_t), intent(in) :: pfxunit
      type(frequency_box_user_t)   :: fbox
      logical, intent(inout) :: error
    end subroutine noema_draw_spw
  end interface
  !
  interface
    subroutine noema_id_spw(spwout,pfxunit,fbox,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Displays the coverage of the spw in a given correlator unit
      !
      !-----------------------------------------------------------------------
      type(spw_output_t), intent(in) :: spwout
      type(pfx_unit_t), intent(in) :: pfxunit
      type(frequency_box_user_t)   :: fbox
      logical, intent(inout) :: error
    end subroutine noema_id_spw
  end interface
  !
  interface
    subroutine noema_draw_chunks(flo1,flo2,sdop,pfxtype,ib,is,ip,fbox,id,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Displays the chunks limits and highligh conflicting chunks
      !
      !-----------------------------------------------------------------------
      real(kind=8), intent(in)      :: flo1
      real(kind=8), intent(in)      :: flo2
      real(kind=8), intent(in)      :: sdop
      type(pfx_type_t), intent(in) :: pfxtype
      integer(kind=4), intent(in)   :: ib ! baseband
      integer(kind=4), intent(in)   :: is ! sideband
      integer(kind=4), intent(in)   :: ip ! polar
      type(frequency_box_user_t)   :: fbox
      logical, intent(in)     :: id
      logical, intent(inout) :: error
    end subroutine noema_draw_chunks
  end interface
  !
  interface
    subroutine noema_pfx_status(pfx,overload,conflict,chunk1,allisoff,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Check feasability of the setup
      !
      !-----------------------------------------------------------------------
      type(pfx_t), intent(in) :: pfx
      integer(kind=4), intent(inout)     :: overload
      integer(kind=4), intent(inout)     :: conflict
      logical, intent(inout) :: chunk1
      logical, intent(inout) :: allisoff
      logical, intent(inout) :: error
    end subroutine noema_pfx_status
  end interface
  !
  interface
    subroutine pico_backend(line,error)
      use gbl_message
      use astro_types
      use ast_astro
      use my_receiver_globals
      use plot_molecules_globals
      use frequency_axis_globals
      !-----------------------------------------------------------------------
      ! @ private
      ! Set and show the backend coverage for EMIR
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(inout) :: line        ! command line
      logical, intent(inout) :: error
    end subroutine pico_backend
  end interface
  !
  interface
    subroutine define_emir_backends(rname,em,error)
      use gbl_message
      use ast_line
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! defines IF1 frequency coverage of backends
      !-----------------------------------------------------------------------
      character(len=*), intent(in)  :: rname
      type(emir_t), intent(inout)  :: em
      logical, intent(inout)            :: error
    end subroutine define_emir_backends
  end interface
  !
  interface
    subroutine emir_setup_backend(emir,iback,imode,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Set backend coverage from if cables content
      !
      !-----------------------------------------------------------------------
      type(emir_t), intent(inout) :: emir
      integer(kind=4), intent(in) :: iback
      integer(kind=4), intent(in) :: imode
      logical, intent(inout) :: error
    end subroutine emir_setup_backend
  end interface
  !
  interface
    subroutine emir_plot_backend(emir,iback,imode,cplot,cata,drawaxis,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Displays the backend coverage when switchbox setup was given by user
      !
      !-----------------------------------------------------------------------
      type(emir_t), intent(in) :: emir
      integer(kind=4), intent(in) :: iback
      integer(kind=4), intent(in) :: imode
      type(current_boxes_t), intent(inout) :: cplot
      type(plot_molecules_t), intent(in) :: cata
      type(frequency_axis_t), intent(in) :: drawaxis
      logical, intent(inout) :: error
    end subroutine emir_plot_backend
  end interface
  !
  interface
    subroutine ndatec(ndate,cdate,error)
      use gbl_message
      !------------------------------------------------------------------------
      ! @ private
      !  Convert the integers for year, month, and day into date '01-JAN-1984'
      !------------------------------------------------------------------------
      integer(kind=4),  intent(in)    :: ndate(3)  !
      character(len=*), intent(out)   :: cdate     !
      logical,          intent(inout) :: error     !
    end subroutine ndatec
  end interface
  !
  interface
    subroutine ntimec(ntime,ctime,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Converts the integers for Hours, Minutes, Seconds, milliSeconds to
      ! formatted TIME
      !---------------------------------------------------------------------
      character(len=*), intent(out)   :: ctime     !
      integer(kind=4),  intent(in)    :: ntime(4)  !
      logical,          intent(inout) :: error     !
    end subroutine ntimec
  end interface
  !
  interface
    subroutine jdate_to_datetime(jdate,datetime,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Convert input Julian date to Astro's specific date-time formatted
      ! string e.g.:
      !    18-JAN-2018 11:03:33.400
      ! At least 24 characters are needed for the full output string.
      ! However, shorter strings are accepted. Result will be truncated,
      ! this may be useful to get e.g. the leading date part.
      !---------------------------------------------------------------------
      real(kind=8),     intent(in)    :: jdate     !
      character(len=*), intent(out)   :: datetime  !
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine jdate_to_datetime
  end interface
  !
  interface
    subroutine vsop87(tjj,ico,ider,prec,r,error)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Compute object position and velocities from VSOP87 ephemeris.
      !
      ! ICO    1 - 8 = Mercury to Neptune, relative to Sun
      !            9 = Earth/Moon barycenter, relative to Sun
      !           10 = Sun, relative to Solar system barycenter
      ! R(3,0:IDER) position (1-3) vector and derivatives (in au, au/day, au/day**2)
      !---------------------------------------------------------------------
      real(kind=8),    intent(in)    :: tjj          ! Julian date
      integer(kind=4), intent(in)    :: ico          ! Body identifier
      integer(kind=4), intent(in)    :: ider         ! Number of derivatives to compute (0 to 2)
      real(kind=8),    intent(in)    :: prec         ! Precision 0 to 0.01; 0 is maximal precision
      real(kind=8),    intent(out)   :: r(3,0:ider)  ! Position vector and derivatives
      logical,         intent(inout) :: error        !
    end subroutine vsop87
  end interface
  !
  interface
    subroutine readr8(lun,irecord,nw,w,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Read NW words starting at direct access record IRECORD on file LUN
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: lun
      integer(kind=4), intent(inout) :: irecord
      integer(kind=4), intent(in)    :: nw
      real(kind=8),    intent(out)   :: w(nw)
      logical,         intent(inout) :: error
    end subroutine readr8
  end interface
  !
  interface
    subroutine readi4(lun,irecord,nw,w,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Read NW words starting at direct access record IRECORD on file LUN
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: lun
      integer(kind=4), intent(inout) :: irecord
      integer(kind=4), intent(in)    :: nw
      integer(kind=4), intent(out)   :: w(nw)
      logical,         intent(inout) :: error
    end subroutine readi4
  end interface
  !
  interface
    subroutine eph_convert(cfile)
      use gbl_convert
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*):: cfile          !
    end subroutine eph_convert
  end interface
  !
  interface
    subroutine ephsta (vector,planet,error)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Define vector to compute
      ! two separate calls EPHSTA and EPHVEC are kept for compatibility.
      !---------------------------------------------------------------------
      integer(kind=4) :: vector         ! vector description code
      integer(kind=4) :: planet         ! object identification
      logical :: error                  !
    end subroutine ephsta
  end interface
  !
  interface
    subroutine ephvec (jdate,deriv,rvec,error)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Compute position vector and derivatives at given date
      !---------------------------------------------------------------------
      real(kind=8)           :: jdate            ! julian date of computation
      integer(kind=4)        :: deriv            ! number of derivatives needed (if position only, DERIV = 0)
      real(kind=8)           :: rvec(3,0:deriv)  ! resulting vectors
      logical, intent(inout) :: error            !
    end subroutine ephvec
  end interface
  !
  interface
    subroutine format_catalog(line,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !	ASTRO Command CATALOG  name /VELETA
      !---------------------------------------------------------------------
      character(len=*), intent(inout):: line           !
      logical, intent(inout) :: error                  !
    end subroutine format_catalog
  end interface
  !
  interface
    subroutine getpar(line,code,par,lpar)
      !---------------------------------------------------------------------
      ! @ private
      ! Find parameter par for code word CODE
      !---------------------------------------------------------------------
      character(len=*):: line           !
      character(len=*):: code           !
      character(len=*):: par            !
      integer(kind=4) :: lpar           !
    end subroutine getpar
  end interface
  !
  interface
    subroutine nblank(par,lpar)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*):: par            !
      integer(kind=4) :: lpar           !
    end subroutine nblank
  end interface
  !
  interface
    subroutine transform(in,out,u,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*):: in             !
      character(len=*):: out            !
      character(len=1) :: u             !
      logical :: error                  !
    end subroutine transform
  end interface
  !
  interface
    subroutine output_line(olun,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: olun           !
      logical :: error                  !
    end subroutine output_line
  end interface
  !
  interface
    subroutine great_circle(a1,d1,a2,d2,error)
      use gbl_message
      use gkernel_types
      use ast_astro
      use ast_constant
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      real(kind=8) :: a1                !
      real(kind=8) :: d1                !
      real(kind=8) :: a2                !
      real(kind=8) :: d2                !
      logical :: error                  !
    end subroutine great_circle
  end interface
  !
  interface
    function iangin(ch,cdef,dang,iang)
      !---------------------------------------------------------------------
      ! @ private
      !	J.SCHRAML, Sept 81
      !
      ! This function converts an input line into angles. It calls
      ! the function LINSCN for returning individual fields and their
      ! sign and dimensions. The fields are multiplied by 60 and than
      ! added. If a dimension occurs the accumulated value is converted
      ! into arcseconds. The dimension of the last field is cdef unless
      ! specified explicitely. Sign is only allowed for the first
      ! field, otherwise an error will occur.
      ! Input: String CH and Dimension CDEF
      ! Output: Double precision Angle in Radians and integer 10 ms
      !---------------------------------------------------------------------
      integer(kind=4) :: iangin         !
      character(len=*):: ch             !
      character(len=1) :: cdef          !
      real(kind=8) :: dang              !
      integer(kind=4) :: iang           !
    end function iangin
  end interface
  !
  interface
    function linscn(ch,dat,isig,cdim)
      !---------------------------------------------------------------------
      ! @ private
      !	J.SCHRAML, sept 1981
      !
      ! This function decodes an input character string CH, finds the value
      ! DAT, its sign and the information if a sign has been entered in ISIG,
      ! assembles the dimension as a character string CDIM and returns the
      ! actual field lenth as the function value
      ! This function is called by the the routines that interprete the
      ! input of observers as numbers or angles.
      !
      !	Definitions and startup values
      !---------------------------------------------------------------------
      integer(kind=4) :: linscn         !
      character(len=*):: ch             !
      real(kind=8) :: dat               !
      integer(kind=4) :: isig           !
      character(len=*):: cdim           !
    end function linscn
  end interface
  !
  interface
    function tsmg (tjj)
      !---------------------------------------------------------------------
      ! @ private
      !  calcul du temps sideral moyen de Greenwich (radian)
      !  from EPHAUT / BDL / A002 / 82-1
      !  modified 29 november 1984 by Michel Perault
      !  reference Aoki et al. (1982, Astron. Astrophys. 105, 359)
      !  en entree : TJJ date (jours juliens)
      !  remarques : on utilise le formulaire UAI84
      !              l'epoque de reference est J2000.0 (JJ2451545.0)
      !              le temps est compte en siecle julien
      !---------------------------------------------------------------------
      real(kind=8) :: tsmg              !
      real(kind=8) :: tjj               !
    end function tsmg
  end interface
  !
  interface
    function oblimo (tjj)
      !---------------------------------------------------------------------
      ! @ private
      !  calcul de l'obliquite moyenne (radian), precession selon Lieske et al. (1977)
      !  from EPHAUT / BDL / C004 / 82-1
      !  modified 29 november 1984 by Michel Perault
      !  en entree : TJJ date (jours juliens)
      !---------------------------------------------------------------------
      real(kind=8) :: oblimo            !
      real(kind=8) :: tjj               !
    end function oblimo
  end interface
  !
  interface
    subroutine nuta (tjj,dpsi,deps)
      !---------------------------------------------------------------------
      ! @ private
      !  calcul de la nutation en longitude et en obliquite a la date TJJ
      !  d'apres les formules de WAHR (1981)
      !  from EPHAUT / BDL / D003 / 81-1
      !  modified 29 november 1984 by Michel Perault
      !  only the terms with amplitude larger than 0.004 arcsec are kept
      !  en entree :  TJJ date (jours juliens)
      !  en sortie :  DPSI nutation en longitude (radian)
      !		DEPS nutation en obliquite (radian)
      !  remarques :  l'epoque de reference pour le calcul de la nutation
      !		est TF1 : 1 janvier 2000 12h (J2000.0)
      !		l'epoque de reference pour le calcul des arguments
      !		fondamentaux est TF2 : 0 janvier 1900 12h
      !---------------------------------------------------------------------
      real(kind=8) :: tjj               !
      real(kind=8) :: dpsi              !
      real(kind=8) :: deps              !
    end subroutine nuta
  end interface
  !
  interface
    subroutine ctcheb (values,degree,coeff,error)
      !---------------------------------------------------------------------
      ! @ private
      !     Approximation d'une fonction par un developpement en polynomes de
      !     Tchebychev sur un intervalle de variation donne
      !  Loosely inspired from EPHAUT / BDL / J002 / 81-1
      !  Modified 5 October 1985 - Michel Perault - POM Version 2.0
      !  Input :
      !	VALUES(0:DEGREE) function at Tchebychev abscissa
      !	DEGREE	degree of representation
      !  Output :
      !	COEFF(0:DEGREE)	coefficients of Tchebychev polynomes
      !	ERROR	logical, if improper value of DEGREE
      !  Notes :
      !	VALUES et COEFF sont exprimes dans la meme unite.
      !	Les abscisses de tchebychev sur l'intervalle (-1,+1) sont donnees
      !	par la relation suivante :
      !                 Xk = cos((2*k+1)*pi/(2*(DEGREE+1))  pour k = 0,DEGREE+1
      !	Les valeurs de la fonction aux abscisses de Tchebychev ne doivent
      !	pas presenter de discontinuite.
      !	Il est conseille de representer la fonction sur un intervalle ne
      !	contenant pas plus d'un extremum.
      !---------------------------------------------------------------------
      integer(kind=4) :: degree         ! degree of representation
      real(kind=8) :: values(0:degree)  ! function at Tchebychev abscissa
      real(kind=8) :: coeff(0:degree)   ! coeffiecients of Tchebychev polynomes
      logical :: error                  ! set if error
    end subroutine ctcheb
  end interface
  !
  interface
    subroutine ftcheb (degree,coeff,x,deriv,values,error)
      !---------------------------------------------------------------------
      ! @ private
      !  Calcul d'une fonction et de ses derivees a partir de sa representation
      !  en developpement de polynomes de Tchebychev
      !  Loosely inspired from EPHAUT / BDL / J003 / 81-1
      !  Modified 5 October 1985 - Michel Perault - POM Version 2.0
      !  Input :
      !	DEGREE	degree of representation
      !	COEFF(0:DEGREE)	coeffiecients of Tchebychev polynomes
      !	X	abscissa (between -1.d0 and +1.d0)
      !	DERIV	number of derivatives to compute
      !  Output :
      !	VALUES(0:DERIV)	function and derivatives
      !	ERROR	logical, if improper value of DEGREE, X or DERIV
      !  Notes :
      !	FTCHEB est exprimee dans la meme unite que les coefficients
      !  	Si dt est l'intervalle de representation exprime dans une unite donnee,
      !  	les derivees doivent etre multipliees par (2/dt)**DERIV
      !---------------------------------------------------------------------
      integer(kind=4) :: degree         ! degree of representation
      real(kind=8) :: coeff(0:degree)   ! coeffiecients of Tchebychev polynomes
      real(kind=8) :: x                 ! abscissa (between -1.d0 and +1.d0)
      integer(kind=4) :: deriv          ! number of derivatives to compute
      real(kind=8) :: values(0:deriv)   ! function and derivatives
      logical :: error                  ! set if error
    end subroutine ftcheb
  end interface
  !
  interface
    function a_lever(d,d0,el)
      !---------------------------------------------------------------------
      ! @ private
      !	Lever d'une etoile de declinaison D a latitude D0
      !	Ce code est different de "lever" de GreG, donc je l'appelle A_LEVER
      !	(sinon big problemes,...GD, 8-10-1992)
      !	D	Declination in radians
      !	D0	Latitude in degrees
      ! 	EL	elevation limit in degrees
      !	LEVER	Hour angle in Hours
      !
      !	PI/2-D0+EL < D	Circumpolaire
      !	D0-PI/2+EL < D < PI/2-D0+EL H = ACOS (TAN(D)/TAN(D0-PI/2))
      !	D < D0-PI/2+EL	Toujours couchee...
      !
      ! Formule applicable a la partie visible du ciel en projection
      ! sur un plan
      !---------------------------------------------------------------------
      real(kind=4) :: a_lever           !
      real(kind=4) :: d                 !
      real(kind=4) :: d0                !
      real(kind=4) :: el                !
    end function a_lever
  end interface
  !
  interface
    subroutine line_auto(line,error)
      use gbl_message
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*):: line           !
      logical :: error                  !
    end subroutine line_auto
  end interface
  !
  interface
    subroutine name_it(name,i,ch,lch)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*):: name           !
      integer(kind=4) :: i              !
      character(len=*):: ch             !
      integer(kind=4) :: lch            !
    end subroutine name_it
  end interface
  !
  interface
    subroutine load_astro(mode)
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: mode           !
    end subroutine load_astro
  end interface
  !
  interface
    subroutine run_astro(line,comm,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      character(len=12) :: comm         !
      logical :: error                  !
    end subroutine run_astro
  end interface
  !
  interface
    subroutine run_pdbi(line,comm,error)
      use gbl_message
      use ast_line
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      character(len=12) :: comm         !
      logical :: error                  !
    end subroutine run_pdbi
  end interface
  !
  interface
    subroutine run_noemaoffline(line,comm,error)
      use gbl_message
      use ast_line
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      character(len=12) :: comm         !
      logical :: error                  !
    end subroutine run_noemaoffline
  end interface
  !
  interface
    subroutine run_noemaonline(line,comm,error)
      use gbl_message
      use ast_line
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      character(len=12) :: comm         !
      logical :: error                  !
    end subroutine run_noemaonline
  end interface
  !
  interface
    subroutine run_alma(line,comm,error)
      use gbl_message
      use ast_line
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      character(len=12) :: comm         !
      logical :: error                  !
    end subroutine run_alma
  end interface
  !
  interface
    subroutine run_30m(line,comm,error)
      use gbl_message
      use ast_line
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      character(len=12) :: comm         !
      logical :: error                  !
    end subroutine run_30m
  end interface
  !
  interface
    subroutine astro_exec(buffer)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !	Library version of the ASTRO program.
      !	All the ASTRO commands are accessible through calls in the form:
      !
      !	call us_exec ('This_command')
      !---------------------------------------------------------------------
      character(len=*) :: buffer
    end subroutine astro_exec
  end interface
  !
  interface
    subroutine noema_febe(line,error)
      use gbl_message
      use gkernel_types
      use ast_astro
      use my_receiver_globals
      !-----------------------------------------------------------------------
      ! @ private
      ! Set of tools to put in memory some freqsetups and come back to them
      ! set of MULTI ARG commands
      !-----------------------------------------------------------------------
      character(len=*), intent(inout) :: line        ! command line
      logical, intent(inout) :: error
    end subroutine noema_febe
  end interface
  !
  interface
    subroutine noema_plot(line,error)
      use gbl_message
      use astro_noema_type
      use ast_astro
      use my_receiver_globals
      use plot_molecules_globals
      use frequency_axis_globals
      use string_parser_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Plot the current setup of receiver\backend at Bure
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(inout) :: line        ! command line
      logical, intent(inout) :: error
    end subroutine noema_plot
  end interface
  !
  interface
    subroutine draw_dopminmax_spw(spwu,pfx,rdesc,rsou,rtune,cplotb,ry1,ry2,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Show what can be the variation on the HR SPW being plotted 
      ! with the evolution of the Earth doppler
      !
      !-----------------------------------------------------------------------
      type(spw_unit_t), intent(in) :: spwu 
      type(pfx_t), intent(in) :: pfx
      type(receiver_desc_t), intent(in) :: rdesc
      type(receiver_source_t), intent(in) :: rsou
      type(receiver_tune_t), intent(in) :: rtune
      type(frequency_box_t), intent(inout) :: cplotb
      real(kind=8), intent(in) :: ry1,ry2
      logical, intent(inout) :: error
    end subroutine draw_dopminmax_spw
  end interface
  !
  interface
    subroutine noema_oplot_dopminmax(inoema,cplot,drawaxis,error)
      use gbl_message
      use astro_noema_type
      !-----------------------------------------------------------------------
      ! @ private
      ! Show what can be the variation on the HR SPW coverage 
      ! with the evolution of the Earth doppler
      !
      !-----------------------------------------------------------------------
      type(noema_t), intent(inout) :: inoema
      type(current_boxes_t), intent(inout) :: cplot
      type(frequency_axis_t), intent(in) :: drawaxis
      logical, intent(inout) :: error
    end subroutine noema_oplot_dopminmax
  end interface
  !
  interface
    subroutine pfx_spw_minmax_if2(rname,pfx,spwu,if2m,edge,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Get if2 limits of a spw
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(spw_unit_t), intent(in) :: spwu
      type(pfx_t), intent(in) :: pfx
      real(kind=8), intent(inout) :: if2m(2)
      logical, intent(inout) :: edge(2)
      logical, intent(inout) :: error
    end subroutine pfx_spw_minmax_if2
  end interface
  !
  interface
    subroutine noema_default_pms(pms,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Reset the structure containing info to be passed to Proposal Management System
      !-----------------------------------------------------------------------
      type(info_pms_t), intent(inout)   :: pms
      logical, intent(inout) :: error
    end subroutine noema_default_pms
  end interface
  !
  interface
    subroutine noema_get_fcontw20(frep,rs,fcontw20,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Special routine to compute a continuum freq as done in PMS W20 session
      ! so that PMS and SMS give same results
      !-----------------------------------------------------------------------
      real(kind=8), intent(in)   :: frep
      type(receiver_source_t), intent(in)   :: rs
      real(kind=8), intent(inout) :: fcontw20
      logical, intent(inout) :: error
    end subroutine noema_get_fcontw20
  end interface
  !
  interface
    subroutine noema_print_pms(infopms,error)
      use gbl_message
      use astro_types
      use ast_astro
      !-----------------------------------------------------------------------
      ! @ private
      ! Print all info relevant for Proposal Management System
      !-----------------------------------------------------------------------
      type(info_pms_t), intent(in)   :: infopms
      logical, intent(inout) :: error
    end subroutine noema_print_pms
  end interface
  !
  interface
    subroutine noema_draw_frep(freq,spwout,cplot,infopms,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Draw the position of a repreentative freq (might be different form tuning freq)
      ! Needed for PMS
      !-----------------------------------------------------------------------
      real(kind=8), intent(in)      :: freq
      type(spw_output_t), intent(in) :: spwout
      type(current_boxes_t), intent(inout) :: cplot
      type(info_pms_t), intent(inout)   :: infopms
      logical, intent(inout) :: error
    end subroutine noema_draw_frep
  end interface
  !
  interface
    subroutine noema_trackshare(dolsr,ival,rsou,frep,frest,cplot,tsdf,ts_freqout,error)
      use gbl_message
      use astro_types
      use phys_const
      !-----------------------------------------------------------------------
      ! @ private
      ! COmpute offset of trackshared sources with min and max values (LSR or REDSHIFT)
      ! developped for SMS
      !-----------------------------------------------------------------------
      logical, intent(in)  :: dolsr ! if true velo, else redshift
      real(kind=8), intent(in)      :: ival(2) ! vmin and vmax
      type(receiver_source_t), intent(in) :: rsou  ! full receiver parameters
      real(kind=8), intent(in)      :: frep
      real(kind=8), intent(in)      :: frest
      type(current_boxes_t), intent(inout) :: cplot
      real(kind=8), intent(inout) :: tsdf(2)
      logical, intent(inout) :: ts_freqout
      logical, intent(inout) :: error
    end subroutine noema_trackshare
  end interface
  !
  interface
    subroutine noema_draw_trackshare(frf,freqtype,cplot,plotok,error)
      use gbl_message
      use astro_types
      use phys_const
      !-----------------------------------------------------------------------
      ! @ private
      ! Draw the position of sources with min and max velocities in case of tracksharing
      ! Needed for SMS
      !-----------------------------------------------------------------------
      real(kind=8), intent(in)      :: frf ! freq to display in RF
      character(len=*), intent(in) :: freqtype ! rep or tuning freq
      type(current_boxes_t), intent(inout) :: cplot
      logical, intent(inout) :: plotok
      logical, intent(inout) :: error
    end subroutine noema_draw_trackshare
  end interface
  !
  interface
    subroutine noema_draw_flo(rtune,cplot,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Draw the position of the LO and indicate whether the tuning is ON grid or not
      !-----------------------------------------------------------------------
      type(receiver_tune_t), intent(in) :: rtune
      type(current_boxes_t), intent(inout) :: cplot
      logical, intent(inout) :: error
    end subroutine noema_draw_flo
  end interface
  !
  interface
    subroutine noema_draw_summary(inoema,cplot,molecules,drawaxis,error)
      use gbl_message
      use gbl_ansicodes
      use astro_noema_type
      use ast_astro
      !-----------------------------------------------------------------------
      ! @ private
      ! Draw coverage of spectral units independently of correlator units
      ! for summary purpose
      !
      !-----------------------------------------------------------------------
      type(noema_t), intent(in) :: inoema
      type(current_boxes_t), intent(inout) :: cplot
      type(plot_molecules_t), intent(in) :: molecules
      type(frequency_axis_t), intent(in) :: drawaxis
      logical, intent(inout) :: error
    end subroutine noema_draw_summary
  end interface
  !
  interface
    subroutine noema_draw_legend(rname,cplot,nr,r,rcol,badsetup,nflex,mflex,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! draw title and warning message 
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(current_boxes_t), intent(inout)          :: cplot
      integer(kind=4), intent(in) :: nr
      real(kind=4), intent(in) :: r(m_res)
      character(len=*), intent(in) :: rcol(m_res)
      logical, intent(in) :: badsetup
      integer(kind=4), intent(in) :: nflex
      integer(kind=4), intent(in) :: mflex 
      logical, intent(inout) :: error
    end subroutine noema_draw_legend
  end interface
  !
  interface
    subroutine noema_draw_warning(rname,cplot,outlo,chunk1,overload,conflict,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! draw title and warning message 
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(current_boxes_t), intent(inout)          :: cplot
      logical, intent(in) :: outlo
      logical, intent(in) :: chunk1
      integer(kind=4), intent(in) :: overload
      integer(kind=4), intent(in) :: conflict
      logical, intent(inout) :: error
    end subroutine noema_draw_warning
  end interface
  !
  interface
    subroutine noema_spw_getcol(rname,spu,pfx,ctype,color,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      !Get color to be used to draw a SPW - loop on PFX unit
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(spw_unit_t), intent(in) :: spu
      type(pfx_t), intent(in) :: pfx
      integer(kind=4), intent(in) :: ctype ! which color to use (fill rect or hatch or border)
      character(len=*), intent(inout) :: color
      logical, intent(inout) :: error
    end subroutine noema_spw_getcol
  end interface
  !
  interface
    subroutine noema_lsrrange(inoema,cplot,infopms,error)
      use gbl_message
      use astro_noema_type
      use phys_const
      !-----------------------------------------------------------------------
      ! @ private
      ! Message about comparison of SPW width and LSR shift
      ! Useful for people to see if a config is OK for different sources
      !-----------------------------------------------------------------------
      type(noema_t), intent(in) :: inoema
      type(current_boxes_t), intent(in) :: cplot
      type(info_pms_t), intent(inout)   :: infopms
      logical, intent(inout) :: error
    end subroutine noema_lsrrange
  end interface
  !
  interface
    subroutine noema_changevelo(line,error)
      use gbl_message
      use gbl_constant
      use gkernel_types
      use ast_astro
      use astro_noema_type
      use phys_const
      use my_receiver_globals
      use plot_molecules_globals
      !-----------------------------------------------------------------------
      ! @ private
      ! NEWVEL to check what will be the freq coverage for a different source velocity of redshift
      ! Syntax:
      ! NEWVEL LSR|REDSHIFT value[km/s|redshift] [toleranceMHz]
      !                     [/SETUP LineName ReprFreqGHz SpectraResolkHz /LINES]
      ! Does not change antything in the current setup
      !-----------------------------------------------------------------------
      character(len=*), intent(inout) :: line        ! command line
      logical, intent(inout) :: error
    end subroutine noema_changevelo
  end interface
  !
  interface
    subroutine noema_getoffset(spwold,spwnew,offset,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Compare old and new frequencies 
      !
      !-----------------------------------------------------------------------
      type(spw_output_t), intent(in) :: spwold
      type(spw_output_t), intent(in) :: spwnew
      real(kind=8), intent(inout) :: offset
      logical, intent(inout) :: error
    end subroutine noema_getoffset
  end interface
  !
  interface
    subroutine noema_oplot_summary(spwout,pfx,rsou,cplot,molecules,error)
      use gbl_message
      use astro_types
      use ast_astro
      !-----------------------------------------------------------------------
      ! @ private
      ! Overplot coverage of spectral units for comparison purpose (changevelo)
      !
      !-----------------------------------------------------------------------
      type(spw_output_t), intent(in) :: spwout
      type(pfx_t), intent(in) :: pfx
      type(receiver_source_t), intent(in) :: rsou
      type(current_boxes_t), intent(inout) :: cplot
      type(plot_molecules_t), intent(in) :: molecules
      logical, intent(inout) :: error
    end subroutine noema_oplot_summary
  end interface
  !
  interface
    subroutine noema_external_tuning(rname,irec,lnoema,error)
      use gbl_message
      use astro_noema_type
      use phys_const
      !-----------------------------------------------------------------------
      ! @ private
      ! SET tuning  and source information from outside astro
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(astro_noemareceiver_t), intent(in) :: irec  ! import tuning info
      type(noema_t), intent(inout) :: lnoema    ! febe structure
      logical, intent(inout) :: error
    end subroutine noema_external_tuning
  end interface
  !
  interface
    subroutine noema_external_spw(rname,ispw,lnoema,error)
      use gbl_message
      use astro_noema_type
      use my_receiver_globals
      !-----------------------------------------------------------------------
      ! @ private
      ! configure correlator from outside information
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(astro_noemaspw_t), intent(in) :: ispw  ! import spw list
      type(noema_t), intent(inout) :: lnoema     ! setup structure
      logical, intent(inout) :: error
    end subroutine noema_external_spw
  end interface
  !
  interface
    subroutine if2tochunk(rname,chtype,if2,domin,ich,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! get chunk number form if2 value
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(pfx_type_t), intent(in) :: chtype
      real(kind=8), intent(in)     :: if2
      logical, intent(in)          :: domin
      integer(kind=4), intent(out) :: ich
      logical, intent(inout) :: error
    end subroutine if2tochunk
  end interface
  !
  interface
    subroutine noema_tsys(line,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   NOEMA\TSYS TableName [/CONTINUUM] [/DBR]
      ! Compute the Tsys  binary table
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      logical,          intent(inout) :: error
    end subroutine noema_tsys
  end interface
  !
  interface
    subroutine noema_tsys_clean()
      !---------------------------------------------------------------------
      ! @ private
      ! Deallocate the global buffers. There is no need to keep them
      ! allocated after the command has ended
      !---------------------------------------------------------------------
      !
    end subroutine noema_tsys_clean
  end interface
  !
  interface
    subroutine noema_tsys_table(file,dbr,error)
      use gbl_message
      use gkernel_types
      use astro_types
      !---------------------------------------------------------------------
      ! @ private
      ! Compute the Tsys + Opacity table
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: file
      logical,          intent(in)    :: dbr
      logical,          intent(inout) :: error
    end subroutine noema_tsys_table
  end interface
  !
  interface
    subroutine noema_tsys_continuum(file,dbr,error)
      use gbl_message
      use gkernel_types
      use astro_types
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      ! Compute the Tsys + Opacity table
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: file
      logical,          intent(in)    :: dbr
      logical,          intent(inout) :: error
    end subroutine noema_tsys_continuum
  end interface
  !
  interface
    subroutine noema_tsys_write_binarray(lun,array,n,buffer,currec,recpos,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: lun
      integer(kind=4), intent(in)    :: n
      real(kind=4),    intent(in)    :: array(n)
      integer(kind=4), intent(inout) :: buffer(:)
      integer(kind=4), intent(inout) :: currec
      integer(kind=4), intent(inout) :: recpos
      logical,         intent(inout) :: error
    end subroutine noema_tsys_write_binarray
  end interface
  !
  interface
    subroutine noema_spectral_sweep(line,error)
      use gbl_message
      use ast_astro
      use my_receiver_globals
      use astro_specsweep_types
      use plot_molecules_globals
      use frequency_axis_globals
      !-----------------------------------------------------------------------
      ! @ private
      ! Command to prepare multi-tuning spectral observations with NOEMA
      !-----------------------------------------------------------------------
      character(len=*), intent(inout) :: line        ! command line
      logical, intent(inout) :: error
    end subroutine noema_spectral_sweep
  end interface
  !
  interface
    subroutine noema_list(line,error)
      use gbl_message
      use my_receiver_globals
      use frequency_axis_globals
      !-----------------------------------------------------------------------
      ! @ private
      ! List current state of the setup definition
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(inout) :: line        ! command line
      logical, intent(inout) :: error
    end subroutine noema_list
  end interface
  !
  interface
    subroutine noema_show_online(line,error)
      use gbl_message
      use my_receiver_globals
      use frequency_axis_globals
      use plot_molecules_globals
      !-----------------------------------------------------------------------
      ! @ private
      ! SHOW command in OBS
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(inout) :: line        ! command line
      logical, intent(inout) :: error
    end subroutine noema_show_online
  end interface
  !
  interface
    subroutine noema_list_spw(rname,spwout,fr_axis,rsou,byindex,onlyconflict,w1,w2,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! list all the spwctral window already defined
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(spw_output_t), intent(inout) :: spwout ! list of spw
      type(receiver_source_t), intent(in) :: rsou ! source parameters (for freq conversion)
      character(len=*), intent(in) :: fr_axis     ! current main frequency axis
      logical, intent(in) :: byindex         ! list in spectral resolution then freq order
      logical, intent(in) :: onlyconflict         ! list only conflicts
      integer(kind=4)       ::w1,w2               ! sublist limits
      logical, intent(inout) :: error
    end subroutine noema_list_spw
  end interface
  !
  interface
    subroutine noema_list_spw_online(rname,spwout,rsou,pfx,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! list all the spwctral window already defined in ONLINE mode
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(spw_output_t), intent(inout) :: spwout ! list of spw
      type(receiver_source_t), intent(in) :: rsou ! source parameters (for freq conversion)
      type(pfx_t), intent(in) :: pfx
      logical, intent(inout) :: error
    end subroutine noema_list_spw_online
  end interface
  !
  interface
    subroutine noema_spw_obs_index(rname,spwout,pfx,obsindex,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Get the obs index of the currently defined SPW
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(spw_output_t), intent(in) :: spwout ! list of spw
      type(pfx_t), intent(in) :: pfx ! list of spw
      integer(kind=4), intent(inout) :: obsindex(spwout%n_spw)
      logical, intent(inout) :: error
    end subroutine noema_spw_obs_index
  end interface
  !
  interface
    function sort_spw_freq_gt(m,l)
      use my_receiver_globals
      !---------------------------------------------------------------------
      ! @ private
      ! Sorting function for noema_sort_freq_spw (greater than)
      ! ---
      !  NB: THIS SORTS THE GLOBAL SPW STRUCTURE SHARED THROUGH MODULE
      !      MY_RECEIVER_GLOBALS!!!
      !---------------------------------------------------------------------
      logical :: sort_spw_freq_gt
      integer(kind=4), intent(in) :: m,l
    end function sort_spw_freq_gt
  end interface
  !
  interface
    function sort_spw_freq_ge(m,l)
      use my_receiver_globals
      !---------------------------------------------------------------------
      ! @ private
      ! Sorting function for noema_sort_freq_spw (greater or equal)
      ! ---
      !  NB: THIS SORTS THE GLOBAL SPW STRUCTURE SHARED THROUGH MODULE
      !      MY_RECEIVER_GLOBALS!!!
      !---------------------------------------------------------------------
      logical :: sort_spw_freq_ge
      integer(kind=4), intent(in) :: m,l
    end function sort_spw_freq_ge
  end interface
  !
  interface
    subroutine noema_list_pfx(rname,pfx,fr_axis,rsou,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! list all the IFcode and the mode (+usage?) of pfx units
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(in)  :: rname
      type(pfx_t), intent(inout) :: pfx ! pfx description
      type(receiver_source_t), intent(in) :: rsou ! source parameters (for freq conversion)
      character(len=*), intent(in) :: fr_axis     ! current main frequency axis
      logical, intent(inout) :: error
    end subroutine noema_list_pfx
  end interface
  !
  interface
    subroutine noema_list_outputfreq(spwu,fr_axis,rsou,mess,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! convert the frequency limits of the spw unit into the output freq frame
      !
      !-----------------------------------------------------------------------
      type(spw_unit_t), intent(inout) :: spwu ! list of spw
      character(len=*), intent(in) :: fr_axis     ! current main frequency axis
      type(receiver_source_t), intent(in) :: rsou ! source parameters (for freq conversion)
      character(len=*), intent(inout)     :: mess !output message with right frequencies
      logical, intent(inout) :: error
    end subroutine noema_list_outputfreq
  end interface
  !
  interface
    subroutine noema_reset(line,error)
      use gbl_message
      use ast_astro
      use my_receiver_globals
      use plot_molecules_globals
      use frequency_axis_globals
      !-----------------------------------------------------------------------
      ! @ private
      ! Reset one or several spw
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(inout) :: line        ! command line
      logical, intent(inout) :: error
    end subroutine noema_reset
  end interface
  !
  interface
    subroutine noema_last_spw(spwout,n_last,spw_last,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! find the latest spw that have been defined
      !-----------------------------------------------------------------------
      type(spw_output_t), intent(inout)  :: spwout
      integer(kind=4), intent(inout)  :: n_last
      integer(kind=4), intent(inout)  :: spw_last(m_spw)
      logical, intent(inout)            :: error
    end subroutine noema_last_spw
  end interface
  !
  interface
    function sort_spw_time_gt(m,l)
      use my_receiver_globals
      !---------------------------------------------------------------------
      ! @ private
      ! Sorting function for noema_last_spw (greater than) [sort by time]
      ! ---
      !  NB: BASED ON THE GLOBAL SPW STRUCTURE SHARED THROUGH MODULE
      !      MY_RECEIVER_GLOBALS!!!
      !---------------------------------------------------------------------
      logical :: sort_spw_time_gt
      integer(kind=4), intent(in) :: m,l
    end function sort_spw_time_gt
  end interface
  !
  interface
    function sort_spw_time_ge(m,l)
      use my_receiver_globals
      !---------------------------------------------------------------------
      ! @ private
      ! Sorting function for noema_sort_spw (greater or equal)
      ! ---
      !  NB: THIS SORTS THE GLOBAL SPW STRUCTURE SHARED THROUGH MODULE
      !      MY_RECEIVER_GLOBALS!!!
      !---------------------------------------------------------------------
      logical :: sort_spw_time_ge
      integer(kind=4), intent(in) :: m,l
    end function sort_spw_time_ge
  end interface
  !
  interface
    subroutine pfx_reset_unit(pfxu,spwout,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! defines polyfix backends
      !-----------------------------------------------------------------------
      type(pfx_unit_t), intent(inout)    :: pfxu
      type(spw_output_t), intent(inout)  :: spwout
      logical, intent(inout)            :: error
    end subroutine pfx_reset_unit
  end interface
  !
  interface
    subroutine noema_reset_spw(pfx,spwin,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! reset a spw and free the associated chunks in pfx corresponding unit
      !-----------------------------------------------------------------------
      type(pfx_t), intent(inout)    :: pfx
      type(spw_unit_t), intent(inout)  :: spwin
      logical, intent(inout)            :: error
    end subroutine noema_reset_spw
  end interface
  !
  interface
    subroutine noema_null_spw(win,error)
      use astro_types
      !------------------------------------------------------------------------
      ! @ private
      ! put all attributes of a spectral window to undefined
      !------------------------------------------------------------------------
      type(spw_unit_t), intent(inout)       :: win
      logical, intent(inout)                :: error
    end subroutine noema_null_spw
  end interface
  !
  interface
    subroutine noema_reset_backend(pfx,spwout,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! defines polyfix backends
      !-----------------------------------------------------------------------
      type(pfx_t), intent(inout)    :: pfx
      type(spw_output_t)            :: spwout
      logical, intent(inout)            :: error
    end subroutine noema_reset_backend
  end interface
  !
  interface
    subroutine noema_compress_spw(spwout,error)
      use astro_types
      !---------------------------------------------------------------------
      ! @ private
      ! Compress spw list (to remove spwindows that have been reset)
      !---------------------------------------------------------------------
      type(spw_output_t), intent(inout) :: spwout
      logical,            intent(inout) :: error
    end subroutine noema_compress_spw
  end interface
  !
  interface
    subroutine noema_copy_spw(in,out,error)
      use astro_types
      !---------------------------------------------------------------------
      ! @ private
      ! Copy a spw_unit_t into another one
      !---------------------------------------------------------------------
      type(spw_unit_t), intent(in)    :: in
      type(spw_unit_t), intent(out)   :: out
      logical,          intent(inout) :: error
    end subroutine noema_copy_spw
  end interface
  !
  interface
    subroutine noema_check_conflicts(rname,spw,pfx,error)
      use gbl_message
      use gbl_ansicodes
      use astro_types
      !---------------------------------------------------------------------
      ! @ private
      ! Copy a spw_unit_t into another one
      !---------------------------------------------------------------------
      character(len=*), intent(in)          :: rname
      type(spw_output_t), intent(inout)    :: spw
      type(pfx_t), intent(in)       :: pfx
      logical, intent(inout)   :: error
    end subroutine noema_check_conflicts
  end interface
  !
  interface
    subroutine noema_setup_file(rname,line,doonline,error)
      use gbl_message
      use gbl_ansicodes
      use astro_types
      use ast_line
      use ast_astro
      use my_receiver_globals
      use plot_molecules_globals
      !-----------------------------------------------------------------------
      ! @ private
      ! Create a sequence of commands to reach the present state
      ! if calling command is SETUP   -->  ONLINE syntax
      ! if calling command is PROPOSAL--> OFFLINE syntax
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      character(len=*), intent(inout) :: line        ! command line
      logical, intent(inout) :: doonline
      logical, intent(inout) :: error
    end subroutine noema_setup_file
  end interface
  !
  interface
    subroutine noema_multi_script(rname,lnoema,nf,self,cata,doonline,dotime,olun,error)
      use gbl_message
      use astro_noema_type
      use ast_line
      use ast_astro
      !-----------------------------------------------------------------------
      ! @ private
      ! Prepare the wrting of an astro scritpt ith several febe inside (to input for PMS)
      ! keep OFFLINE mode
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname        ! calling command
      type(noema_t), intent(in)  :: lnoema
      integer(kind=4), intent(in) :: nf
      integer(kind=4), intent(in) :: self(nf)
      type(plot_molecules_t), intent(in) :: cata
      logical, intent(in)   :: doonline
      logical, intent(in)   :: dotime
      integer(kind=4), intent(in) :: olun
      logical, intent(inout) :: error
    end subroutine noema_multi_script
  end interface
  !
  interface
    subroutine multi_script_header(rname,lnoema,nf,self,ilun,error)
      use gbl_message
      use astro_noema_type
      use ast_line
      use ast_astro
      !-----------------------------------------------------------------------
      ! @ private
      ! Write the header part of the multi febe script for PMS
      ! 
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname        ! calling command
      type(noema_t), intent(in)  :: lnoema
      integer(kind=4), intent(in) :: nf
      integer(kind=4), intent(in) :: self(nf)
      integer(kind=4), intent(in) :: ilun
      logical, intent(inout) :: error
    end subroutine multi_script_header
  end interface
  !
  interface
    subroutine noema_setup_info(rname,rsou,lfebe,olun,error)
      use gbl_message
      use astro_types
      use ast_line
      use ast_astro
      !-----------------------------------------------------------------------
      ! @ private
      ! get info relative to FEBE and put it in the PMS script
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname        ! calling command
      type(receiver_source_t), intent(in)  :: rsou
      type(noema_febe_t), intent(in)  :: lfebe
      integer(kind=4), intent(in) :: olun
      logical, intent(inout) :: error
    end subroutine noema_setup_info
  end interface
  !
  interface
    subroutine noema_setup_check(rname,rsou,lfebe,doonline,error)
      use gbl_message
      use astro_types
      use ast_line
      use ast_astro
      !-----------------------------------------------------------------------
      ! @ private
      ! Check Febe before writing it
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname        ! calling command
      type(receiver_source_t), intent(in)  :: rsou
      type(noema_febe_t), intent(in)  :: lfebe
      logical, intent(in)   :: doonline
      logical, intent(inout) :: error
    end subroutine noema_setup_check
  end interface
  !
  interface
    subroutine noema_setup_print(rname,rsou,lfebe,cata,doonline,olun,error)
      use gbl_message
      use astro_types
      use ast_line
      use ast_astro
      !-----------------------------------------------------------------------
      ! @ private
      ! Create a sequence of commands to reach the input state based only on SPW /CH
      ! keep OFFLINE mode
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname        ! calling command
      type(receiver_source_t), intent(in)  :: rsou
      type(noema_febe_t), intent(in)  :: lfebe
      type(plot_molecules_t), intent(in) :: cata
      logical, intent(in)   :: doonline
      integer(kind=4), intent(in) :: olun
      logical, intent(inout) :: error
    end subroutine noema_setup_print
  end interface
  !
  interface
    subroutine noema_setup_userpref(rname,cata,chain,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! extract get user configuration for the molecular line plots
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(plot_molecules_t), intent(in)  :: cata
      character(len=*), intent(inout) :: chain
      logical,  intent(inout) :: error
    end subroutine noema_setup_userpref
  end interface
  !
  interface
    subroutine noema_setup_cata(spwout,molecules,mlist,ncm,error)
      use gbl_message
      use astro_types
      use ast_line
      !-----------------------------------------------------------------------
      ! @ private
      ! extract from the line catalog the lines falling in the current spw
      !
      !-----------------------------------------------------------------------
      type(spw_output_t), intent(in)  :: spwout
      type(plot_molecules_t), intent(in)  :: molecules
      character(len=*), intent(inout) :: mlist(:,:)
      integer(kind=4), intent(inout) :: ncm
      logical,  intent(inout) :: error
    end subroutine noema_setup_cata
  end interface
  !
  interface
    subroutine noema_setup_source(rname,recsou,doonline,dotime,olun,error)
      use gbl_message
      use gbl_constant
      use ast_astro
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! create the sourcecommand
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(receiver_source_t), intent(in)  :: recsou
      logical,  intent(in) :: doonline
      logical,  intent(in) :: dotime
      integer(kind=4), intent(in) :: olun
      logical,  intent(inout) :: error
    end subroutine noema_setup_source
  end interface
  !
  interface
    subroutine noema_setup_rec(rsou,rtune,doonline,olun,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! create the tuning command
      !-----------------------------------------------------------------------
      type(receiver_source_t), intent(in)  :: rsou
      type(receiver_tune_t), intent(in)  :: rtune
      logical,  intent(in) :: doonline
      integer(kind=4), intent(in) :: olun
      logical,  intent(inout) :: error
    end subroutine noema_setup_rec
  end interface
  !
  interface
    subroutine noema_setup_pfx(rtune,pfx,spwout,doonline,olun,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! create the BASEBAND and SPW commands
      !
      !-----------------------------------------------------------------------
      type(receiver_tune_t), intent(in)  :: rtune
      type(pfx_t), intent(in) :: pfx
      type(spw_output_t), intent(in) :: spwout
      logical,  intent(in) :: doonline
      integer(kind=4), intent(in) :: olun
      logical,  intent(inout) :: error
    end subroutine noema_setup_pfx
  end interface
  !
  interface
    subroutine noema_setup_bb(rtune,pfx,spwout,doonline,olun,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! create baseband commands
      !
      !-----------------------------------------------------------------------
      type(receiver_tune_t), intent(in)  :: rtune
      type(pfx_t), intent(in) :: pfx
      type(spw_output_t), intent(in) :: spwout
      logical,  intent(in) :: doonline
      integer(kind=4), intent(in) :: olun
      logical,  intent(inout) :: error
    end subroutine noema_setup_bb
  end interface
  !
  interface
    subroutine noema_setup_spw(rtune,pfx,spwout,doonline,olun,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! create spw commands
      !
      !-----------------------------------------------------------------------
      type(receiver_tune_t), intent(in)  :: rtune
      type(pfx_t), intent(in) :: pfx
      type(spw_output_t), intent(in) :: spwout
      logical,  intent(in) :: doonline
      integer(kind=4), intent(in) :: olun
      logical,  intent(inout) :: error
    end subroutine noema_setup_spw
  end interface
  !
  interface
    subroutine noema_print(message,olun)
      use gbl_message
      !-----------------------------------------------------------------------
      ! @ private
      ! send message to the given logical unit
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: message
      integer(kind=4),  intent(in) :: olun
    end subroutine noema_print
  end interface
  !
  interface
    subroutine noema_info_pms(rtune,rsou,pfx,spwout,pms,error)
      use gbl_message
      use astro_types
      use ast_astro
      !-----------------------------------------------------------------------
      ! @ private
      ! Fill the structure containing info
      ! to be passed to Proposal Management System
      !-----------------------------------------------------------------------
      type(receiver_tune_t), intent(in)      :: rtune
      type(receiver_source_t), intent(in)      :: rsou
      type(pfx_t), intent(in)    :: pfx
      type(spw_output_t), intent(in)    :: spwout
      type(info_pms_t), intent(inout)   :: pms
      logical, intent(inout) :: error
    end subroutine noema_info_pms
  end interface
  !
  interface
    subroutine object(name,coord,equinox,lambda,beta,invtype,invelo,inredshift, &
                      draw,drawargs,drawclip,s_3,print,icat,error)
      use gildas_def
      use gbl_message
      use ast_astro
      use ast_constant
      !---------------------------------------------------------------------
      ! @ private
      ! ASTRO	Reduce coordinates of a fixed object
      ! Arguments:
      !	NAME	C*(*)	Object name			Input
      !	COORD	C*2	Coordinate system		Input
      !	EQUINOX	R*4	Equinox of the coordinates	Input
      !	LAMBDA	R*8	Longitude like coordinate	Input
      ! 	DRAW	L	Plot a symbol?			Input
      !	S_3	R*8(3)	RA, Dec, Dsun at current epoch	Output
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name         ! Object name
      character(len=2), intent(in)    :: coord        ! Coordinate system
      real(kind=4),     intent(in)    :: equinox      ! Equinox of system
      real(kind=8),     intent(in)    :: lambda       ! Longitude like coordinate
      real(kind=8),     intent(in)    :: beta         ! Latitude like coordinate
      character(len=*), intent(in)    :: invtype      ! kind of velocity/redshift
      real(kind=8), intent(in)        :: invelo       !input value for velocity
      real(kind=8), intent(in)        :: inredshift   ! input redshift
      logical,          intent(in)    :: draw         ! Plot?
      character(len=*), intent(in)    :: drawargs(:)  ! Which kind of plot(s)?
      logical,          intent(in)    :: drawclip     ! how to clip the source names
      real(kind=8),     intent(out)   :: s_3(3)       ! RA, Dec & Dsun at current epoch
      logical,          intent(in)    :: print        ! Printout flag
      integer(kind=4),  intent(in)    :: icat         !
      logical,          intent(inout) :: error        !
    end subroutine object
  end interface
  !
  interface
    function sun_distance (x_2)
      use ast_constant
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      !	Returns angular distance from SUn
      !	X_2: apparent horizontal coordinates of the source
      !	Xsun_2: apparent horizontal coordinates of the sun
      ! The angle between the 2 directions is derived from the scalar product 
      ! of the vectors in the horizontal coordinate frame:
      ! Vobj.Vsun=(XOXSun+YOYSun+ZOZSun)=||Vobj||*||Vsun||*cos(SunObserverTarget)
      !---------------------------------------------------------------------
      real(kind=8) :: sun_distance      !
      real(kind=8) :: x_2(3)            !
    end function sun_distance
  end interface
  !
  interface
    subroutine fill_astro_source(varname,name,ora,odec,s_2,vshift,vlsr,lsr,dop,redsh, &
                                 coord,equinox,lambda,beta,invtype,invelo,error)
      use gildas_def
      use ast_astro
     !---------------------------------------------------------------------
      ! @ private
      ! fill astro%source% structure
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: varname
      character(len=*),intent(in)   :: name
      real(kind=8), intent(in)      :: ora,odec
      real(kind=8), intent(in)      :: s_2(2)
      real(kind=8), intent(in)      :: vshift,vlsr,lsr,dop,invelo
      real(kind=8), intent(in)      :: redsh
      character(len=*), intent(in)  :: coord,invtype
      real(kind=4), intent(in)  :: equinox
      real(kind=8),     intent(in)    :: lambda,beta   ! Longitude/latitude like coordinate
      logical,        intent(inout) :: error    ! Logical error flag  
    end subroutine fill_astro_source
  end interface
  !
  interface
    subroutine fill_doppler_source(rname,varname,name,vshift,vlsr,redsh,invtype,invelo,error)
      use gildas_def
      use gbl_message
      use ast_astro
      use ast_constant
      !---------------------------------------------------------------------
      ! @ private
      ! fill astro%source% structure in the case of source /doppler option
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: rname
      character(len=*), intent(in)  :: varname
      character(len=*),intent(in)   :: name
      real(kind=8), intent(in)      :: vshift,vlsr
      real(kind=8), intent(in)      :: redsh
      character(len=*),intent(in)   :: invtype
      real(kind=8), intent(in)      :: invelo
      logical,        intent(inout) :: error    ! Logical error flag  
    end subroutine fill_doppler_source
  end interface
  !
  interface
    subroutine nullify_astro_source(error)
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      !  put to 0 all astro%source% values
      !---------------------------------------------------------------------
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine nullify_astro_source
  end interface
  !
  interface
    subroutine delete_astro_source(error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  delete the structure astro%source%
      !---------------------------------------------------------------------
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine delete_astro_source
  end interface
  !
  interface
    subroutine astro_observatory_command(line,error)
      use gbl_message
      use ast_line
      use ast_astro
      use my_receiver_globals
      use frequency_axis_globals
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      ! OBSERVATORY name [version] [on|off] 
      ! or
      ! OBSERVATORY Long Lat Altitude [Sun-Avoidance]
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line   !
      logical,          intent(out) :: error  !
    end subroutine astro_observatory_command
  end interface
  !
  interface
    subroutine pdbi_line(line,error)
      use gildas_def
      !---------------------------------------------------------------------
      !@ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line   ! Input command line
      logical,          intent(out) :: error  ! Logical error flag
    end subroutine pdbi_line
  end interface
  !
  interface
    subroutine pdbi_line_sub(line,error)
      use gildas_def
      use gbl_message
      use ast_line
      use ast_astro
      use phys_const
      !---------------------------------------------------------------------
      !@ private
      ! PDBI  Internal routine, support for command
      ! LINE name Freq SB [Lock [Center [Harm]]]
      !         /RECEIVER rec                                (1)
      !         /BEST best                                   (2)
      !         /FIXED_FREQ in order to allow tuning out of the LO 500MHz grid
      !                         (which is the default for obs_year.ge.2015)
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line   ! Input command line
      logical,          intent(out) :: error  ! Logical error flag
    end subroutine pdbi_line_sub
  end interface
  !
  interface
    subroutine line_check_grid(line,iarg,flo1,fcent,sky,ftune,ongrid,fcentgrid,error)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @private
      ! Check that the proposed tuning corresponds to an LO frequency
      ! on the grid provided by the FrontEnd
      ! If not, a new IF frequency is suggested
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: line  ! command line
      integer(kind=4), intent(in) :: iarg   ! position of fcent in the command line
      real(kind=8), intent(in) :: flo1      !lo1 frequency to check
      real(kind=8), intent(in) :: fcent     !IF center frequency
      integer(kind=4), intent(in) :: sky    !+1 USB, -1 LSB
      real(kind=8), intent(in) :: ftune     !corresponding sky frequency
      logical, intent(in) :: ongrid         ! option /ONGRID present or not
      real(kind=8), intent(out) :: fcentgrid ! proposed IF freq to move the tuning to the grid
      logical, intent(inout) :: error       !
    end subroutine line_check_grid
  end interface
  !
  interface
    subroutine pdbi_plot_grid(oldfcent,fcent,error)
      use gbl_message
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      ! Plot IF frequencies matching the tuning grid (obs_year>=2015) 
      ! and the original Fcent to visualize to change in /ONGRID MODE
      !---------------------------------------------------------------------
      real(kind=8), intent(in) :: oldfcent  ! original IF center frequency in the LINE command
      real(kind=8), intent(in) :: fcent     ! actually used IF center frequency in the LINE command (/ONGRID option)
      logical, intent(inout) :: error       !
    end subroutine pdbi_plot_grid
  end interface
  !
  interface
    subroutine pdbi_narrow(line,error)
      use gbl_message
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      ! PDBI internal routine, support for command
      !	NARROW_INPUT 1|2 Q1|Q2|Q3|Q4 [HOR|VER]
      !---------------------------------------------------------------------
      character(len=*):: line           !
      logical :: error                  !
    end subroutine pdbi_narrow
  end interface
  !
  interface
    subroutine pdbi_widex(line,error)
      use gbl_message
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      ! PDBI internal routine, support for command WIDEX
      ! (only producing a plot)
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      logical :: error                  !
    end subroutine pdbi_widex
  end interface
  !
  interface
    subroutine pdbi_plot(line,error)
      use gildas_def
      use gbl_message
      use atm_params
      use ast_line
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      ! PDBI internal routine, support for command
      !	PLOT [LINE|NARROW|WIDEX]
      !         /LIMITS min max                             (1)
      !         /MOLECULES FileName [Mol1 Mol2 ... Moln]    (2)
      !         /WIDTH width                                (3)
      !         /ATMOSPHERE                                 (4)
      !         /SPURIOUS                                   (5)
      !---------------------------------------------------------------------
      character(len=*):: line           !
      logical :: error                  !
    end subroutine pdbi_plot
  end interface
  !
  interface
    subroutine alma_plot_def(error)
      use gildas_def
      use gbl_message
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      ! Defaults paramaters for the plot
      !---------------------------------------------------------------------
      logical :: error                  !
    end subroutine alma_plot_def
  end interface
  !
  interface
    subroutine read_lines(line,kmol,molfile)
      use gildas_def
      use gbl_message
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*):: line           !
      integer(kind=4) :: kmol           !
      character(len=*):: molfile        !
    end subroutine read_lines
  end interface
  !
  interface
    subroutine pdbi_print(line,error)
      use gildas_def
      use gbl_message
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      ! PDBI internal routine, support for command PRINT
      !---------------------------------------------------------------------
      character(len=*):: line           !
      logical :: error                  !
    end subroutine pdbi_print
  end interface
  !
  interface
    subroutine pdbi_spectral(line,error)
      use gbl_message
      use ast_line
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      ! PDBI internal routine, support for command
      !	SPECTRAL iunit bandwidth frequency /NARROW i
      !---------------------------------------------------------------------
      character(len=*):: line           !
      logical :: error                  !
    end subroutine pdbi_spectral
  end interface
  !
  interface
    subroutine pdbi_plot_spectral(window,specialunit)
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4) :: window         !
      integer(kind=4) :: specialunit    !
    end subroutine pdbi_plot_spectral
  end interface
  !
  interface
    subroutine find_rffreq(i,f)
      use gbl_message
      use ast_line
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      ! PDBI internal routine
      !       Find sky frequency associated to the center of subband i
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: i  !
      real(kind=8), intent(out) :: f    !
    end subroutine find_rffreq
  end interface
  !
  interface
    subroutine if3fromrf(if3,narrow,rf,error)
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      ! Compute IF3 from RF
      !---------------------------------------------------------------------
      real(kind=4), intent(out) :: if3        !
      integer(kind=4), intent(out) :: narrow  !
      real(kind=4), intent(in) :: rf          !
      logical, intent(out) :: error           !
    end subroutine if3fromrf
  end interface
  !
  interface
    subroutine pdbi_plot_full
      use gbl_message
      use ast_line
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      ! Plot of USB + LSB, as produced by the LINE command
      !---------------------------------------------------------------------
    end subroutine pdbi_plot_full
  end interface
  !
  interface
    subroutine pdbi_plot_full_corr
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      ! Plot the correlator bands onto the DSB plot
      !---------------------------------------------------------------------
    end subroutine pdbi_plot_full_corr
  end interface
  !
  interface
    subroutine pdbi_plot_narrow
      use gbl_message
      use ast_line
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      ! Plot of the NARROW-BAND correlator
      !---------------------------------------------------------------------
      ! Global
    end subroutine pdbi_plot_narrow
  end interface
  !
  interface
    subroutine pdbi_plot_widex
      use gbl_message
      use ast_line
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      ! Plot of WIDEX
      !---------------------------------------------------------------------
      ! Global
    end subroutine pdbi_plot_widex
  end interface
  !
  interface
    subroutine pdbi_plot_narrow_widex(wideband)
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      ! Plot the narrow band correlator setup on the WIDEX plot
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: wideband  ! Widex Band indicator
    end subroutine pdbi_plot_narrow_widex
  end interface
  !
  interface
    subroutine pdbi_line_molecules(fmin,fmax)
      use ast_line
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Plot the molecular lines
      !---------------------------------------------------------------------
      real(kind=8) :: fmin              !
      real(kind=8) :: fmax              !
    end subroutine pdbi_line_molecules
  end interface
  !
  interface
    subroutine pdbi_line_atmos(fmin,fmax,umin,umax,wat,npoints)
      use gbl_message
      use gkernel_types
      use atm_params
      use ast_line
      use ast_astro
      !---------------------------------------------------------------------
      ! @ private
      ! Plot atmospheric transmission
      !---------------------------------------------------------------------
      real(kind=8), intent(in) :: fmin        ! Signal frequency range
      real(kind=8), intent(in) :: fmax        ! Signal frequency range
      real(kind=8), intent(in) :: umin        ! Image frequency range
      real(kind=8), intent(in) :: umax        ! Image frequency range
      real(kind=4), intent(in) :: wat         ! Water vapor content
      integer(kind=4), intent(in) :: npoints  ! Number of points in the curve
    end subroutine pdbi_line_atmos
  end interface
  !
  interface
    subroutine pdbi_line_spurious
      use gbl_message
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      ! Plot spurious lines. Box must be in IF1 limits.
      !---------------------------------------------------------------------
    end subroutine pdbi_line_spurious
  end interface
  !
  interface
    subroutine astro_def_receiver(rname,name,rdesc,error)
      use gbl_message
      use astro_types
      use ast_astro
      use ast_line
      !-----------------------------------------------------------------------
      ! @ private
      ! fill in the receiver type according to input name
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      character(len=*), intent(in)          :: name ! receiver name
      type(receiver_desc_t), intent(inout) :: rdesc ! receiver parameters
      logical, intent(inout)        :: error
    end subroutine astro_def_receiver
  end interface
  !
  interface
    subroutine astro_receiver_info(rdesc,rname,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! display in the terminal main information about the receiver properties
      !-----------------------------------------------------------------------
      type(receiver_desc_t), intent(in) :: rdesc ! receiver parameters
      character(len=*), intent(in)   :: rname
      logical, intent(inout)        :: error
    end subroutine astro_receiver_info
  end interface
  !
  interface
    subroutine astro_def_recsource(rname,rdesc,rsou,error)
      use gbl_message
      use gbl_constant
      use ast_astro
      use astro_types
      use phys_const
      !-----------------------------------------------------------------------
      ! @ private
      ! fill in the receiver_source type according to current source parameters
      !-----------------------------------------------------------------------
      character(len=*), intent(in)  :: rname
      type(receiver_desc_t), intent(inout) :: rdesc ! receiver desc parameters
      type(receiver_source_t), intent(inout) :: rsou ! receiver source parameters
      logical, intent(inout)        :: error
    end subroutine astro_def_recsource
  end interface
  !
  interface
    subroutine source_minmax_doppler(rsou,vmin,vmax,error)
      use gbl_message
      use ast_astro
      use ast_constant
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! get min and max doppler for a given source
      !
      !-----------------------------------------------------------------------
      type(receiver_source_t), intent(inout) :: rsou
      real(kind=8), intent(inout) :: vmin
      real(kind=8), intent(inout) :: vmax
      logical, intent(inout) :: error
    end subroutine source_minmax_doppler
  end interface
  !
  interface
    subroutine astro_tune_receiver(rname,rdesc,rsou,rcomm,rtune,error)
      use gildas_def
      use gbl_message
      use gbl_ansicodes
      use astro_types
      use phys_const
      use ast_astro
      !-----------------------------------------------------------------------
      ! @ private
      ! Check input and Set tuning parameters (single band)
      !-----------------------------------------------------------------------
      character(len=*), intent(in)   :: rname
      type(receiver_desc_t), intent(in)  :: rdesc ! receiver description 
      type(receiver_source_t), intent(in)  :: rsou ! source velocity and freq shifts 
      type(receiver_comm_t), intent(inout)  :: rcomm ! receiver command
      type(receiver_tune_t), intent(inout)  :: rtune ! receiver tuning
      logical, intent(inout)                :: error
    end subroutine astro_tune_receiver
  end interface
  !
  interface
    subroutine rec_find_band(rname,rdesc,frf,iband,error)
      use gbl_message
      use astro_types  
      !-----------------------------------------------------------------------
      ! @ private
      ! Find the receiver band to tune based on input frequency
      !-----------------------------------------------------------------------
      character(len=*), intent(in)          :: rname
      type(receiver_desc_t), intent(in)     :: rdesc
      real(kind=8), intent(in)              :: frf
      integer(kind=4), intent(inout)        :: iband
      logical, intent(inout)                :: error
    end subroutine rec_find_band
  end interface
  !
  interface
    subroutine rec_reset_box(fbox,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! reset the defaults values for frequency_box_t that will be defined 
      ! (in order to use safely defaults values)
      !-----------------------------------------------------------------------
      type(frequency_box_t), intent(inout)  :: fbox
      logical, intent(inout)                :: error
    end subroutine rec_reset_box
  end interface
  !
  interface
    subroutine rec_def_legendbox(rname,lbox,cplot,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Prepare the box to display messages around the various plotted boxes
      !-----------------------------------------------------------------------
      character(len=*), intent(in)                  :: rname ! name
      type(frequency_box_phys_t), intent(inout)     :: lbox
      type(current_boxes_t), intent(inout)          :: cplot
      logical, intent(inout)                        :: error
    end subroutine rec_def_legendbox
  end interface
  !
  interface
    subroutine rec_def_fbox(f1,f2,ftype,fbox,rdesc,rsou,flo1,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Prepare the box for plotting ad hoc frequency coverage [input can be if1,rf,rest]
      !-----------------------------------------------------------------------
      real(kind=8), intent(in)                      :: f1,f2 !frequency limits (GHz)
      character(len=*), intent(in)                  :: ftype ! type of frequency limits
      type(frequency_box_t), intent(inout)          :: fbox ! box parameters (physical and user)
      type(receiver_desc_t), intent(in)             :: rdesc ! receiver parameters
      type(receiver_source_t), intent(in)           :: rsou ! receiver source parameters
      real(kind=8), intent(in)                      :: flo1
      logical, intent(inout)                        :: error
    end subroutine rec_def_fbox
  end interface
  !
  interface
    subroutine rec_def_fbox_rf(frf1,frf2,fbox,rdesc,rsou,flo1,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Prepare the box for plotting ad hoc frequency coverage [input is RF]
      !-----------------------------------------------------------------------
      real(kind=8), intent(in)                      :: frf1,frf2 ! RF sky frequency limits (GHz)
      type(frequency_box_t), intent(inout)          :: fbox ! box parameters (physical and user)
      type(receiver_desc_t), intent(in)             :: rdesc ! receiver parameters
      type(receiver_source_t), intent(in)           :: rsou ! receiver source parameters
      real(kind=8), intent(in)                      :: flo1
      logical, intent(inout)                        :: error
    end subroutine rec_def_fbox_rf
  end interface
  !
  interface
    subroutine rec_set_limits(rname,cplot,limtype,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! set the limit of the chosen box
      !-----------------------------------------------------------------------
      character(len=*), intent(in)          :: rname
      type(current_boxes_t), intent(in)     :: cplot ! all boxes
      character(len=*), intent(in)          :: limtype
      logical, intent(inout)                :: error
    end subroutine rec_set_limits
  end interface
  !
  interface
    subroutine rec_set_limits_box(rname,fbox,limtype,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! set the limit of the chosen box
      !-----------------------------------------------------------------------
      character(len=*), intent(in)          :: rname
      type(frequency_box_t), intent(in)     :: fbox ! box parameters (physical and user)
      character(len=*), intent(in)          :: limtype
      logical, intent(inout)                :: error
    end subroutine rec_set_limits_box
  end interface
  !
  interface
    subroutine rec_get_itune(rec,ib,it,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      !find the tuning id of a given band
      !-----------------------------------------------------------------------
      type(receiver_t)                      :: rec
      integer(kind=4), intent(in)           :: ib ! looked for band
      integer(kind=4), intent(out)          :: it ! tuning id of the band
      logical, intent(inout)                :: error
    end subroutine rec_get_itune
  end interface
  !
  interface
    subroutine rec_inputtorest(rname,finput,freqtype,rsou,frest,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! For input frequency in any frame (rest,rf,lsr), return the corresponding rest freq
      !-----------------------------------------------------------------------
      character(len=*), intent(in)                      :: rname ! Name of the calling routine
      real(kind=8), intent(in)              :: finput ! Input freq 
      character(len=16)                     :: freqtype ! kind of input frequency
      type(receiver_source_t), intent(in)        :: rsou ! current source parameters
      real(kind=8), intent(inout)           :: frest ! Rest frequency output
      logical, intent(inout)                :: error
    end subroutine rec_inputtorest
  end interface
  !
  interface
    subroutine rec_resttooutput(rname,frest,freqtype,rsou,foutput,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! for an input rest freq, returns the output frequency in the current frame
      !-----------------------------------------------------------------------
      character(len=*), intent(in)          :: rname ! name of the calling routine
      real(kind=8), intent(in)              :: frest ! Input rest freq 
      character(len=16)                     :: freqtype ! kind of input frequency
      type(receiver_source_t), intent(in)   :: rsou ! current source parameters
      real(kind=8), intent(inout)           :: foutput ! Rest frequency output
      logical, intent(inout)                :: error
    end subroutine rec_resttooutput
  end interface
  !
  interface
    subroutine if1torf(flo,fif,isb,frf,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! compute the RF sky frequency (MHz) corresponding to the input IF frequency (MHz)
      !-----------------------------------------------------------------------
      real(kind=8), intent(in)              :: flo ! LO frequency (MHz)
      real(kind=8), intent(in)              :: fif ! IF frequency input (MHz)
      integer(kind=4), intent(in)           :: isb ! considered sideband
      real(kind=8), intent(out)             :: frf ! RF frequency output (MHz)
      logical, intent(inout)                :: error
    end subroutine if1torf
  end interface
  !
  interface
    subroutine rftoif1(flo,frf,isb,fif,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! compute the RF frequency corresponding to the input IF frequency
      !-----------------------------------------------------------------------
      real(kind=8), intent(in)              :: flo ! LO frequency (MHz)
      real(kind=8), intent(in)              :: frf ! RF frequency input (MHz)
      integer(kind=4), intent(in)           :: isb ! considered sideband
      real(kind=8), intent(out)             :: fif ! IF frequency output (MHz)
      logical, intent(inout)                :: error
    end subroutine rftoif1
  end interface
  !
  interface
    subroutine if1toif2(flo2,fif1,isb,fif2,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! compute the if2 frequency corresponding to the input IF1 frequency
      !-----------------------------------------------------------------------
      real(kind=8), intent(in)              :: flo2 ! LO2 frequency (MHz)
      real(kind=8), intent(in)              :: fif1 ! frequency input (MHz)
      integer(kind=4), intent(in)           :: isb ! considered baseband (inner = lsb, outer = usb)
      real(kind=8), intent(out)             :: fif2 ! IF frequency output (MHz)
      logical, intent(inout)                :: error
    end subroutine if1toif2
  end interface
  !
  interface
    subroutine if2toif1(flo2,fif2,isb,fif1,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! compute the if1 frequency corresponding to the input IF2 frequency
      !-----------------------------------------------------------------------
      real(kind=8), intent(in)              :: flo2 ! LO2 frequency (MHz)
      real(kind=8), intent(in)              :: fif2 ! frequency input (MHz)
      integer(kind=4), intent(in)           :: isb ! considered baseband (inner = lsb, outer = usb)
      real(kind=8), intent(out)             :: fif1 ! IF frequency output (MHz)
      logical, intent(inout)                :: error
    end subroutine if2toif1
  end interface
  !
  interface
    subroutine resttorf(dopshift,frest,frf,error)
      !-----------------------------------------------------------------------
      ! @ private
      ! compute the RF sky frequency corresponding to the input rest frequency
      !-----------------------------------------------------------------------
      real(kind=8), intent(in)              :: dopshift ! doppler shift
      real(kind=8), intent(in)              :: frest ! Rest frequency input
      real(kind=8), intent(out)             :: frf ! RF (Sky) frequency output
      logical, intent(inout)                :: error
    end subroutine resttorf
  end interface
  !
  interface
    subroutine rftorest(dopshift,frf,frest,error)
      !-----------------------------------------------------------------------
      ! @ private
      ! compute the rest frequency corresponding to the input RF frequency
      !-----------------------------------------------------------------------
      real(kind=8), intent(in)              :: dopshift ! LO frequency
      real(kind=8), intent(in)              :: frf ! RF (Sky) frequency input
      real(kind=8), intent(out)             :: frest ! Rest frequency output
      logical, intent(inout)                :: error
    end subroutine rftorest
  end interface
  !
  interface
    subroutine lsrtorest(lsrshift,flsr,frest,error)
      !-----------------------------------------------------------------------
      ! @ private
      ! compute the rest frequency corresponding to the input lsr frequency
      !-----------------------------------------------------------------------
      real(kind=8), intent(in)              :: lsrshift ! LO frequency
      real(kind=8), intent(in)              :: flsr ! lsr frequency input
      real(kind=8), intent(out)             :: frest ! Rest frequency output
      logical, intent(inout)                :: error
    end subroutine lsrtorest
  end interface
  !
  interface
    subroutine resttolsr(lsrshift,frest,flsr,error)
      !-----------------------------------------------------------------------
      ! @ private
      ! compute the rest frequency corresponding to the input lsr frequency
      !-----------------------------------------------------------------------
      real(kind=8), intent(in)              :: lsrshift ! LO frequency
      real(kind=8), intent(in)              :: frest ! rest frequency input
      real(kind=8), intent(out)             :: flsr ! lsr frequency output
      logical, intent(inout)                :: error
    end subroutine resttolsr
  end interface
  !
  interface
    subroutine rec_locate_fbox(fbox,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Draw a box with 2 given axis
      !-----------------------------------------------------------------------
      type(frequency_box_phys_t),intent(in) :: fbox ! structure describing the plotting box and all axis
      logical, intent(inout)           :: error
    end subroutine rec_locate_fbox 
  end interface
  !
  interface
    subroutine rec_draw_fbox(cplot,ibox,drawaxis,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Draw a box with 2 given axis
      !-----------------------------------------------------------------------
      type(current_boxes_t),intent(in) :: cplot ! structure describing the plotting boxes
      integer(kind=4), intent(in)      :: ibox ! current box index
      type(frequency_axis_t), intent(in) :: drawaxis
      logical, intent(inout)           :: error
    end subroutine  rec_draw_fbox
  end interface
  !
  interface
    subroutine rec_draw_molecules(cata,faxis,error)
      use gbl_message
      use astro_types
      use ast_line
      !---------------------------------------------------------------------
      ! @ private
      ! Plot the molecular lines (Rest, GHz)
      !---------------------------------------------------------------------
      type(frequency_box_user_t), intent(in)        :: faxis
      type(plot_molecules_t), intent(in)            :: cata
      logical, intent(inout)                        :: error
    end subroutine rec_draw_molecules
  end interface
  !
  interface
    subroutine rec_draw_linetune(rtune,fbox,error)
      use astro_types
     !-----------------------------------------------------------------------
      ! @ private
      ! Displays the tuned frequency (in rest freq)
      !-----------------------------------------------------------------------
      type(receiver_tune_t), intent(in)        :: rtune !receiver tuning
      type(frequency_box_user_t), intent(in)        :: fbox !axis limits
      logical, intent(inout)        :: error
    end subroutine rec_draw_linetune
  end interface
  !
  interface
    subroutine rec_draw_axis(faxis,paxis,drawname,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Displays an axis with limits given as input.
      !-----------------------------------------------------------------------
      type(frequency_box_user_t), intent(in)        :: faxis !axis limits
      type(draw_axis_t), intent(in)                 :: paxis !axis physical properties
      logical, intent(in)                           :: drawname
      logical, intent(inout)                        :: error
    end subroutine rec_draw_axis
  end interface
  !
  interface
    subroutine rec_draw_rect(rr,faxis,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Displays a rectangle through gr4_connect
      !-----------------------------------------------------------------------
      type(draw_rect_t)                      :: rr ! all attributes of the line to be drawn
      type(frequency_box_user_t), intent(in) :: faxis ! axis limits
      logical, intent(inout)         :: error
    end subroutine rec_draw_rect
  end interface
  !
  interface
    subroutine rec_draw_boxcar(rr,faxis,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Displays a boxcar profile through gr4_connect
      !-----------------------------------------------------------------------
      type(draw_rect_t)                      :: rr ! all attributes of the line to be drawn
      type(frequency_box_user_t), intent(in) :: faxis ! axis limits
      logical, intent(inout)         :: error
    end subroutine rec_draw_boxcar
  end interface
  !
  interface
    subroutine rec_draw_polyline(segname,np,xp,yp,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Connect np points through gr4_connect
      !-----------------------------------------------------------------------
      character(len=*), intent(in)   :: segname
      integer(kind=4),intent(in)     :: np
      real(kind=4),intent(in)        :: xp(np),yp(np)
      logical, intent(inout)         :: error
    end subroutine rec_draw_polyline
  end interface
  !
  interface
    subroutine rec_draw_gauss(gg,faxis,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Displays a gaussian
      !-----------------------------------------------------------------------
      type(draw_gauss_t)                      :: gg ! all attributes of the guassian to be drawn
      type(frequency_box_user_t), intent(in) :: faxis ! axis limits
      logical, intent(inout)         :: error
    end subroutine rec_draw_gauss
  end interface
  !
  interface
    subroutine rec_draw_frect(rr,faxis,error)
      use gkernel_types
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Displays a filled rectangle.
      !-----------------------------------------------------------------------
      type(draw_rect_t)                      :: rr ! all attributes of the line to be drawn
      type(frequency_box_user_t), intent(in) :: faxis ! axis limits
      logical, intent(inout)         :: error
    end subroutine rec_draw_frect
  end interface
  !
  interface
    subroutine rec_draw_hrect(rr,faxis,error)
      use gkernel_types
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Displays a hatched rectangle (color of the current pen)
      !-----------------------------------------------------------------------
      type(draw_rect_t)                      :: rr ! all attributes of the line to be drawn
      type(frequency_box_user_t), intent(in) :: faxis ! axis limits
      logical, intent(inout)         :: error
    end subroutine rec_draw_hrect
  end interface
  !
  interface
    subroutine rec_draw_mark(mark,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Displays a marker.
      !-----------------------------------------------------------------------
      type(draw_mark_t), intent(in)             :: mark ! all attributes of the marker to be drawn
      logical, intent(inout)         :: error
    end subroutine rec_draw_mark
  end interface
  !
  interface
    subroutine rec_draw_line(line,faxis,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Displays a line.
      !-----------------------------------------------------------------------
      type(draw_line_t), intent(in)             :: line ! all attributes of the line to be drawn
      type(frequency_box_user_t), intent(in)    :: faxis ! axis limits
      logical, intent(inout)         :: error
    end subroutine rec_draw_line
  end interface
  !
  interface
    subroutine rec_draw_arrow(line,faxis,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Displays a line.
      !-----------------------------------------------------------------------
      type(draw_line_t), intent(in)             :: line ! all attributes of the line to be drawn
      type(frequency_box_user_t), intent(in)    :: faxis ! axis limits
      logical, intent(inout)                    :: error
    end subroutine rec_draw_arrow
  end interface
  !
  interface
    subroutine rec_draw_physrect(fbox,acol,idash,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Displays a rectangle defined by physical coordinates - in the current GTVL directory
      !-----------------------------------------------------------------------
      type(frequency_box_phys_t), intent(in)    :: fbox ! all attributes of the line to be drawn
      character(len=*), intent(in)              :: acol
      integer(kind=4), intent(in)               :: idash
      logical, intent(inout)                    :: error
    end subroutine rec_draw_physrect
  end interface
  !
  interface
    subroutine rec_draw_interbox_line(rname,ibline,cplot,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Draw a line from 1 box to another
      !-----------------------------------------------------------------------
      character(len=*), intent(in)          :: rname
      type(draw_interbox_line_t), intent(in)        :: ibline ! all attributes of the line to be drawn
      type(current_boxes_t), intent(in)             :: cplot ! current boxes
      logical, intent(inout)         :: error
    end subroutine rec_draw_interbox_line
  end interface
  !
  interface
    subroutine rec_display_error(mess,error)
      use astro_types ! for colors
      !-----------------------------------------------------------------------
      ! @ private
      ! Clear the plot and display an error message
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: mess
      logical, intent(inout)        :: error
    end subroutine rec_display_error
  end interface
  !
  interface
    subroutine rec_check_doppler(rsou,doredshift,dopchanged,error)
      use gildas_def
      use astro_types
      use phys_const
      !-----------------------------------------------------------------------
      ! @ private
      ! checks that astro%source doppler did not change wrt tuning 
      ! (i.e. source command used between tuning and other action (zoom,backend))
      !-----------------------------------------------------------------------
      type(receiver_source_t), intent(in) :: rsou
      logical, intent(in)        :: doredshift  
      logical, intent(out)        :: dopchanged  
      logical, intent(inout)        :: error
    end subroutine  rec_check_doppler
  end interface
  !
  interface
    subroutine rec_draw_source(rname,rsou,error)
      use gbl_message
      use ast_astro
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Displays the current doppler value above the upper right corner of the current box
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(receiver_source_t), intent(in)   :: rsou
      logical, intent(inout)   :: error
    end subroutine rec_draw_source
  end interface
  !
  interface
    subroutine pico_emir(line,error)
      use gbl_message
      use astro_types
      use ast_astro
      use my_receiver_globals
      use plot_molecules_globals
      use frequency_axis_globals
      !-----------------------------------------------------------------------
      ! @ private
      ! EMIR command to define and visualize EMIR coverage
      ! EMIR F1 SB1 F2 SB2 /CATALOG /zoom
      !-----------------------------------------------------------------------
      character(len=*), intent(inout) :: line        ! command line
      logical, intent(inout) :: error
    end subroutine pico_emir
  end interface
  !
  interface
    subroutine rec_define_emir(rname,rdesc,error)
      use gbl_message
      use ast_line
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! fill in the receiver type according to input name
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(receiver_desc_t), intent(inout) :: rdesc ! receiver parameters
      logical, intent(inout)        :: error
    end subroutine rec_define_emir
  end interface
  !
  interface
    subroutine rec_define_emir_switchbox(emirsw,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! switchbox characteristics
      !-----------------------------------------------------------------------
      type(correlator_input_t), intent(inout) :: emirsw ! emir switchbox parameters
      logical, intent(inout)        :: error
    end subroutine rec_define_emir_switchbox
  end interface
  !
  interface
    subroutine pico_reset_emir(emir,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! reset emir specific parameters
      !-----------------------------------------------------------------------
      type(emir_t), intent(inout) :: emir 
      logical, intent(inout)        :: error
    end subroutine pico_reset_emir
  end interface
  !
  interface
    subroutine pico_emir_setup(emir,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Check feasability and tune emir requested bands
      !-----------------------------------------------------------------------
      type(emir_t), intent(inout) :: emir !command line parameters
      logical, intent(inout)        :: error
    end subroutine pico_emir_setup
  end interface
  !
  interface
    subroutine emir_switchbox(line,error)
      use gbl_message
      use astro_types
      use ast_astro
      use my_receiver_globals
      use plot_molecules_globals
      use frequency_axis_globals
      !-----------------------------------------------------------------------
      ! @ private
      ! SWITCHBOX command to define what are the basebands brought to the backends
      ! no arg = defaults =  single band or maximize FTS coverage + dual polar when possible
      ! option SINGLEPOLAR (only dual band) = maximize FTS coverage + single polar when possible
      ! with arguments : sets explicitely the content of the 1-4 if cables
      !-----------------------------------------------------------------------
      character(len=*), intent(inout) :: line        ! command line
      logical, intent(inout)        :: error
    end subroutine emir_switchbox
  end interface
  !
  interface
    subroutine emir_switch_autosetup(emir,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! assign the right baseband to the 8 emir if cables - default with inner and outer
      !-----------------------------------------------------------------------
      type(emir_t), intent(inout)   :: emir
      logical, intent(inout)        :: error
    end subroutine emir_switch_autosetup
  end interface
  !
  interface
    subroutine emir_switch_listsetup(emir,cablecode,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! assign user defined IF cables - user provides 4 codes, we deduce 8 IF cables
      !-----------------------------------------------------------------------
      type(emir_t), intent(inout)   :: emir
      character(len=*), intent(inout)  :: cablecode(emir%switch%m_usercables)
      logical, intent(inout)        :: error
    end subroutine emir_switch_listsetup
  end interface
  !
  interface
    subroutine pico_switch_plot(emir,cata,cplot,drawaxis,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! identifies the polarizations and sideband available with the current emir setup
      !-----------------------------------------------------------------------
      type(emir_t), intent(inout) :: emir
      type(plot_molecules_t), intent(in) :: cata
      type(current_boxes_t), intent(inout) :: cplot
      type(frequency_axis_t), intent(in) :: drawaxis
      logical, intent(inout) :: error
    end subroutine pico_switch_plot
  end interface
  !
  interface
    subroutine rec_define_noema(rdesc,mode,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! fill in the receiver type according to input name
      !-----------------------------------------------------------------------
      type(receiver_desc_t), intent(inout) :: rdesc ! receiver desc parameters
      character(len=*), intent(in) :: mode ! OFFLINE or ONLINE
      logical, intent(inout)        :: error
    end subroutine rec_define_noema
  end interface
  !
  interface
    subroutine rec_define_noema_ifproc(noemaif,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! if proc characteristics
      !-----------------------------------------------------------------------
      type(correlator_input_t), intent(inout) :: noemaif !
      logical, intent(inout)        :: error
    end subroutine rec_define_noema_ifproc
  end interface
  !
  interface
    subroutine noema_reset_setup(tunecomm,noema_if,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! reset noema specific parameters
      !-----------------------------------------------------------------------
      type(noema_tuning_comm_t), intent(inout) :: tunecomm
      type(noema_if_t), intent(inout) :: noema_if
      logical, intent(inout)        :: error
    end subroutine noema_reset_setup
  end interface
  !
  interface
    subroutine rec_noema(line,error)
      use gbl_message
      use ast_astro
      use my_receiver_globals
      use plot_molecules_globals
      use frequency_axis_globals
      !-----------------------------------------------------------------------
      ! @ private
      ! Command to define and visualize NOEMA receiver coverage
      !-----------------------------------------------------------------------
      character(len=*), intent(inout) :: line        ! command line
      logical, intent(inout) :: error
    end subroutine rec_noema
  end interface
  !
  interface
    subroutine noema_default_tuning(rname,rdesc,sdopshift,ncomm,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! reset noema specific parameters
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(receiver_desc_t), intent(in) :: rdesc   ! all receiver info
      real(kind=8), intent(in)      :: sdopshift
      type(noema_tuning_comm_t), intent(inout) :: ncomm ! command-related stuff
      logical, intent(inout)        :: error
    end subroutine noema_default_tuning
  end interface
  !
  interface
    subroutine rec_noema_online(line,error)
      use gbl_message
      use ast_astro
      use my_receiver_globals
      use plot_molecules_globals
      use frequency_axis_globals
      !-----------------------------------------------------------------------
      ! @ private
      ! ASTRO implementation of the tuning command in OBS\LINE
      ! Decode the line and fill noematune structure used as input for true tuning routines
      !-----------------------------------------------------------------------
      character(len=*), intent(inout) :: line        ! command line
      logical, intent(inout) :: error
    end subroutine rec_noema_online
  end interface
  !
  interface
    subroutine noema_setup(rname,noematune,rcomm,rdesc,rsou,rtune,error)
      use gbl_message
      use astro_types
      use frequency_axis_globals
      !-----------------------------------------------------------------------
      ! @ private
      ! Check feasability and tune noema requested bands
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(noema_tuning_comm_t), intent(inout) :: noematune !command line parameters
      type(receiver_desc_t), intent(in) :: rdesc ! receiver description and tuning
      type(receiver_comm_t), intent(inout) :: rcomm ! receiver description and tuning
      type(receiver_source_t), intent(in) :: rsou ! receiver description and tuning
      type(receiver_tune_t), intent(inout) :: rtune ! receiver description and tuning
      logical, intent(inout)        :: error
    end subroutine noema_setup
  end interface
  !
  interface
    subroutine astro_tunegrid(rname,rdesc,rtune,rsou,bandedge,ongrid,newfcent,error)
      use gbl_message
      use astro_types
      use frequency_axis_globals
      !-----------------------------------------------------------------------
      ! @ private
      ! Check that LO is on the tuning grid, propose the right if freq
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(receiver_desc_t), intent(in) :: rdesc ! receiver description 
      type(receiver_tune_t), intent(in) :: rtune ! receiver tuning
      type(receiver_source_t), intent(in) :: rsou ! receiver tuning
      logical, intent(inout)            :: bandedge
      logical, intent(inout)            :: ongrid
      real(kind=8), intent(inout)       :: newfcent
      logical, intent(inout)        :: error
    end subroutine astro_tunegrid
  end interface
  !
  interface
    subroutine noema_ifproc(rname,rdesc,rtune,ifproc,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! define the basebands brought to the backends
      ! no flxibility with noema for the time being
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(receiver_desc_t), intent(inout) :: rdesc
      type(receiver_tune_t), intent(inout) :: rtune
      type(correlator_input_t), intent(inout) :: ifproc
      logical, intent(inout)        :: error
    end subroutine noema_ifproc
  end interface
  !
  interface
    subroutine noema_ifproc_plot(rname,fixed_scale,noema_if,molecules,rdesc,rsou,rtune,cplot,drawaxis,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! identifies the correlator inputs on the plot
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      logical, intent(in) :: fixed_scale
      type(noema_if_t), intent(in) :: noema_if
      type(plot_molecules_t), intent(in) :: molecules
      type(receiver_desc_t), intent(inout) :: rdesc
      type(receiver_source_t), intent(inout) :: rsou
      type(receiver_tune_t), intent(inout) :: rtune
      type(current_boxes_t), intent(inout) :: cplot
      type(frequency_axis_t), intent(in) :: drawaxis
      logical, intent(inout) :: error
    end subroutine noema_ifproc_plot
  end interface
  !
  interface
    subroutine noema_draw_confusion(rname,rdesc,flo1,sdop,bb_code,sb_code,fbox,cpdesc,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! identifies the confusion zone of NOEMA
      ! S18 version: only the 10MHz of the LO2 parasite
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(receiver_desc_t), intent(in) :: rdesc
      real(kind=8), intent(in)   :: flo1
      real(kind=8), intent(in)   :: sdop
      integer(kind=4), intent(in)   :: bb_code
      integer(kind=4), intent(in)   :: sb_code
      type(frequency_box_user_t), intent(in) :: fbox ! must be rest
      type(current_boxes_desc_t), intent(in) :: cpdesc 
      logical, intent(inout) :: error
    end subroutine noema_draw_confusion
  end interface
  !
  interface
    subroutine noema_baseband(line,error)
      use gbl_message
      use ast_astro
      use my_receiver_globals
      use plot_molecules_globals
      use string_parser_types
      use frequency_axis_globals
      !-----------------------------------------------------------------------
      ! @ private
      ! Baseband and mode selection (in preparation for SPW definition)
      !
      !-----------------------------------------------------------------------
      character(len=*), intent(inout) :: line        ! command line
      logical, intent(inout) :: error
    end subroutine noema_baseband
  end interface
  !
  interface
    subroutine noema_baseband_online(line,error)
      use gbl_message
      use ast_astro
      use my_receiver_globals
      use plot_molecules_globals
      use string_parser_types
      use frequency_axis_globals
      !-----------------------------------------------------------------------
      ! @ private
      ! online Baseband and mode selection: as done in OBS
      ! Give a mode to the PFX units
      !-----------------------------------------------------------------------
      character(len=*), intent(inout) :: line        ! command line
      logical, intent(inout) :: error
    end subroutine noema_baseband_online
  end interface
  !
  interface
    subroutine rec_plot_sidebands(rdesc,rsou,rtune,cplot,cata,drawaxis,error)
      use gbl_message
      use gildas_def
      use astro_types
      use ast_line
      !-----------------------------------------------------------------------
      ! @ private
      ! displays frequency coverage of heterodyne receivers (full width for sidebands)
      !-----------------------------------------------------------------------
      type(receiver_desc_t), intent(in)          :: rdesc ! receiver description parameters
      type(receiver_source_t), intent(in)          :: rsou ! receiver source parameters
      type(receiver_tune_t), intent(in)          :: rtune ! receiver tuning parameters
      type(plot_molecules_t), intent(in)         :: cata
      type(current_boxes_t), intent(inout)  :: cplot    ! plot page parameters
      type(frequency_axis_t), intent(in) :: drawaxis
      logical, intent(inout)         :: error
    end subroutine rec_plot_sidebands
  end interface
  !
  interface
    subroutine rec_def_scale(rdesc,fixed,fmin,fmax,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! determine the limits of the bands to plot - 
      ! fixed option means same scale in all boxes
      !-----------------------------------------------------------------------
      type(receiver_desc_t), intent(in) :: rdesc ! receiver description
      logical, intent(in)   :: fixed ! to plot all bands with the same scale
      real(kind=8), intent(out) :: fmin(rdesc%n_rbands),fmax(rdesc%n_rbands)
      logical, intent(inout) :: error
    end subroutine rec_def_scale
  end interface
  !
  interface
    subroutine rec_plot_tuning(rdesc,rsou,rtune,cata,cplot,drawaxis,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Zoom on the tuned area of a Band (Fmin=LSB min, Fmax=USB max)
      !-----------------------------------------------------------------------
      type(receiver_desc_t), intent(inout) :: rdesc ! receiver description and tuning
      type(receiver_source_t), intent(inout) :: rsou ! receiver description and tuning
      type(receiver_tune_t), intent(inout) :: rtune ! receiver description and tuning
      type(plot_molecules_t), intent(in)   :: cata     !plot known lines from current catalog
      type(current_boxes_t), intent(inout) :: cplot
      type(frequency_axis_t), intent(in) :: drawaxis
      logical, intent(inout) :: error
    end subroutine rec_plot_tuning
  end interface
  !
  interface
    subroutine rec_plot_nbands(rec,cata,fixed,cplot,drawaxis,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Displays the frequency coverage of a multi band receiver
      ! Only tuned bands are displayed
      !-----------------------------------------------------------------------
      type(receiver_t), intent(inout) :: rec ! receiver description and tuning
      type(plot_molecules_t), intent(in)   :: cata     !plot known lines from current catalog
      logical, intent(in)   :: fixed ! to plot all bands with the same scale
      type(current_boxes_t), intent(inout) :: cplot
      type(frequency_axis_t), intent(in) :: drawaxis
      logical, intent(inout) :: error
    end subroutine rec_plot_nbands
  end interface
  !
  interface
    subroutine rec_plot_mbands(rdesc,rsou,cata,fixed,cplot,drawaxis,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Displays the frequency coverage of a multi band receiver
      ! All band displayed
      !-----------------------------------------------------------------------
      type(receiver_desc_t), intent(in) :: rdesc ! receiver description and tuning
      type(receiver_source_t), intent(in) :: rsou ! receiver description and tuning
      type(plot_molecules_t), intent(in)   :: cata     !plot known lines from catalog
      logical, intent(in)   :: fixed     !plot axis with the same scale
      type(current_boxes_t), intent(inout) :: cplot
      type(frequency_axis_t), intent(in) :: drawaxis
      logical, intent(inout) :: error
    end subroutine rec_plot_mbands
  end interface
  !
  interface
    subroutine rec_plot_tuned(rdesc,rsou,rtune,cata,cplot,drawaxis,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Displays the sideband of tuned rec bands
      !-----------------------------------------------------------------------
      type(receiver_desc_t), intent(in) :: rdesc ! receiver description and tuning
      type(receiver_source_t), intent(in) :: rsou ! receiver description and tuning
      type(receiver_tune_t), intent(in) :: rtune ! receiver description and tuning
      type(plot_molecules_t), intent(in)   :: cata     !plot known lines from catalog
      type(current_boxes_t), intent(inout) :: cplot
      type(frequency_axis_t), intent(in) :: drawaxis
      logical, intent(inout) :: error
    end subroutine rec_plot_tuned
  end interface
  !
  interface
    subroutine rec_make_title(rtune,mess,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! prepare the message to be written as title
      !-----------------------------------------------------------------------
      type(receiver_tune_t), intent(in) :: rtune
      character(len=*), intent(inout)   :: mess
      logical, intent(inout) :: error
    end subroutine rec_make_title
  end interface
  !
  interface
    subroutine rec_draw_title(cplot,mess,error)
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! print the message title above the upper box
      !-----------------------------------------------------------------------
      type(current_boxes_t), intent(inout)          :: cplot
      character(len=*), intent(inout)   :: mess
      logical, intent(inout) :: error
    end subroutine rec_draw_title
  end interface
  !
  interface
    subroutine rec_zoom(cata,fz1,fz2,rdesc,rsou,cplot,drawaxis,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ private
      ! single frame zoom in a user define range
      !-----------------------------------------------------------------------
      type(plot_molecules_t), intent(in)   :: cata
      real(kind=8), intent(in)      :: fz1, fz2 ! zoom limit, in REST frame
      type(receiver_desc_t), intent(in) :: rdesc
      type(receiver_source_t), intent(in) :: rsou
      type(current_boxes_t), intent(inout) :: cplot
      type(frequency_axis_t), intent(in) :: drawaxis
      logical, intent(inout) :: error
    end subroutine rec_zoom
  end interface
  !
  interface
    subroutine astro_show(line,error)
      use gbl_message
      use ast_astro
      use ast_line
      use astro_types
      use frequency_axis_globals
      use plot_molecules_globals  
      !---------------------------------------------------------------------
      ! @ private
      ! Show contents of ASTRO variable defined with SET command
      !---------------------------------------------------------------------
      character(len=*):: line           !
      logical :: error                  !
    end subroutine astro_show
  end interface
  !
  interface
    subroutine small_circle(a1,d1,r,error)
      use gkernel_types
      use ast_astro
      use ast_constant
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      real(kind=8) :: a1                !
      real(kind=8) :: d1                !
      real(kind=8) :: r                 !
      logical :: error                  !
    end subroutine small_circle
  end interface
  !
  interface
    subroutine string_parser_addlist(sp,position,list,error)
      use gbl_message
      use string_parser_types
      !---------------------------------------------------------------------
      ! @ private
      !  Add a list of possible values for a given substring
      !---------------------------------------------------------------------
      type(string_parser_t), intent(inout) :: sp
      integer(kind=4),       intent(in)    :: position  ! Expected position in the string
      character(len=*),      intent(in)    :: list(:)   ! All possible values for this substring
      logical,               intent(inout) :: error     !
    end subroutine string_parser_addlist
  end interface
  !
  interface
    subroutine string_parser_print(sp,error)
      use gbl_message
      use string_parser_types
      !---------------------------------------------------------------------
      ! @ private
      !  Print the contents of the string_parser_t
      !---------------------------------------------------------------------
      type(string_parser_t), intent(in)    :: sp
      logical,               intent(inout) :: error
    end subroutine string_parser_print
  end interface
  !
  interface
    function string_parser_print_onelist(sp,isub)
      use string_parser_types
      !---------------------------------------------------------------------
      ! @ private
      ! Return the list of possibilities for a given substring in compact
      ! way
      !---------------------------------------------------------------------
      character(len=128) :: string_parser_print_onelist
      type(string_parser_t), intent(in) :: sp
      integer(kind=4),       intent(in) :: isub
    end function string_parser_print_onelist
  end interface
  !
  interface
    subroutine string_parser_parse(caller,sp,string,allpresent,codes,error)
      use gbl_message
      use string_parser_types
      !---------------------------------------------------------------------
      ! @ private
      ! Parse the input string for the substrings described in the
      ! string_parser_t structure. In return the codes(i) is filled with:
      !  - 0 if the i-th substring was not found in the string (if allowed)
      !  - j>0 if the j-th value was found for the i-th substring.
      !---------------------------------------------------------------------
      character(len=*),      intent(in)    :: caller      ! Calling routine name
      type(string_parser_t), intent(in)    :: sp          !
      character(len=*),      intent(in)    :: string      ! Input string to parse
      logical,               intent(in)    :: allpresent  ! All substrings must be present?
      integer(kind=4),       intent(out)   :: codes(:)    ! Returned codes
      logical,               intent(inout) :: error       !
    end subroutine string_parser_parse
  end interface
  !
  interface
    subroutine string_parser_build(sp,codes,string,error)
      use gbl_message
      use string_parser_types
      !---------------------------------------------------------------------
      ! @ private
      ! Build the character string from the codes list (reverse operation
      ! from string_parser_parse)
      !---------------------------------------------------------------------
      type(string_parser_t), intent(in)    :: sp
      integer(kind=4),       intent(in)    :: codes(:)
      character(len=*),      intent(out)   :: string
      logical,               intent(inout) :: error
    end subroutine string_parser_build
  end interface
  !
  interface
    subroutine astro_uv_doppler(line,error)
      use image_def
      use gbl_format
      use gbl_message
      !-----------------------------------------------------------------------
      ! @ private
      !   Support for command UV_DOPPLER
      ! Task to compute the Doppler correction and apply it (by changing
      ! the u,v coordinates) to an UV Table.
      !-----------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      logical,          intent(inout) :: error  !
    end subroutine astro_uv_doppler
  end interface
  !
  interface
    subroutine sub_uv_doppler(vtype,rdate,rtime,doppler, &
            & coord,equinox,lambda,beta,error)
      use image_def
      use gbl_message
      use gbl_constant
      use phys_const
      !---------------------------------------------------------------------
      ! @ private
      !
      ! Compute the Doppler correction, calling ASTRO routine, according to:
      ! * observatory location
      ! * reference frame (LSR, Helio...)
      ! Radio convention is used
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in)  :: vtype    ! Type of referential
      real(kind=4),     intent(in)  :: rdate    ! GAG Date
      real(kind=4),     intent(in)  :: rtime    ! UT Time
      real(kind=4),     intent(out) :: doppler  ! Doppler factor
      character(len=2), intent(in)  :: coord    !
      real(kind=4),     intent(in)  :: equinox  !
      real(kind=8),     intent(in)  :: lambda   !
      real(kind=8),     intent(in)  :: beta     !
      logical,          intent(out) :: error    ! flag
    end subroutine sub_uv_doppler
  end interface
  !
end module astro_interfaces_private
