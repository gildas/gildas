subroutine pico_backend(line,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>pico_backend
  use astro_types
  use ast_astro
  use my_receiver_globals
  use plot_molecules_globals
  use frequency_axis_globals
  !-----------------------------------------------------------------------
  ! @ private
  ! Set and show the backend coverage for EMIR
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(inout) :: line        ! command line
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='BACKEND'
  logical       :: dopchanged
  integer(kind=4) :: iback,lpar,iftsm,imode
  integer(kind=4), parameter :: m_backnames = 3
  integer(kind=4), parameter :: m_ftsmodes = 2
  character(len=20)     :: backnames(m_backnames), backname,par
  character(len=20)     :: backmode,ftsmodes(m_ftsmodes),ftsmode
  data backnames/'FTS','WILMA','VESPA'/
  data ftsmodes/'WIDE','NARROW'/
  !
  if (obsname.ne.'PICOVELETA'.and.obsname.ne.'VELETA') then
      call astro_message(seve%e,rname,'Inconsistency between Receiver Name and Observatory')
      error = .true.
      return
  endif
  !
  if (emir%rec%n_tunings.eq.0) then
    call astro_message(seve%e,rname,'Please define a tuning (EMIR command) before setting up backends')
    error = .true.
    return
  endif
  !
  !check that doppler did not changed since last tuning
  call rec_check_doppler(emir%rec%source,emir%rec%desc%redshift,dopchanged,error)
  if (dopchanged) then
    call astro_message(seve%e,rname,'Source properties changed since last tuning')
    call astro_message(seve%i,rname,'You should set again the tuning (EMIR command) before working with backends')
    call rec_display_error('Source changed since last tuning',error)
    error = .true.
    return
  endif
  !
  !backend selection
  call sic_ke(line,0,1,par,lpar,.true.,error)
  if (error) return
  call sic_ambigs(rname,par,backname,iback,backnames,m_backnames,error)
  if (error) return
  !
  if (emir%rec%desc%name.eq.'EMIR') then
    if (.not.emir%switch%defined) then
      call astro_message(seve%e,rname,'Please configure first the switchbox (BASEBAND command)')
      error = .true.
      return
    endif
    call define_emir_backends(rname,emir,error)
    if (error) return
    if (backname.eq.'FTS') then
      call sic_ke(line,0,2,backmode,lpar,.true.,error)
      if (error) return
      call sic_ambigs(rname,backmode,ftsmode,iftsm,ftsmodes,m_ftsmodes,error)
      if (error) return
      emir%be(iback)%imode = iftsm
    else if (backname.eq.'WILMA') then
      emir%be(iback)%imode = 1
    else if (backname.eq.'VESPA') then
      emir%be(iback)%imode = 1
    else
      call astro_message(seve%e,rname,'NOT YET IMPLEMENTED')
      error = .true.
      return
    endif
    imode=emir%be(iback)%imode
    call emir_setup_backend(emir,iback,imode,error)
    if (error) return
    call emir_plot_backend(emir,iback,imode,cplot,molecules,freq_axis,error)
    if (error) return
  else
    call astro_message(seve%e,rname,'NOT YET IMPLEMENTED')
    error = .true.
    return
  endif
  !
  ! Let the user with the limits in main axis frequency frame
  call rec_set_limits(rname,cplot,freq_axis%main,error)
  if (error) return
  !
end subroutine pico_backend
!
subroutine define_emir_backends(rname,em,error)
  use gbl_message
  use astro_interfaces, except_this=>define_emir_backends
  use ast_line
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! defines IF1 frequency coverage of backends
  !-----------------------------------------------------------------------
  character(len=*), intent(in)  :: rname
  type(emir_t), intent(inout)  :: em
  logical, intent(inout)            :: error
  ! Local
  integer(kind=4)       :: i,j,k
  real(kind=8)          :: fold_offset
  !
  ! Handle the change of frequency implemented in 2021
  if (obs_year.eq.2016) then
    fold_offset=0d0
  else if  (obs_year.eq.2021) then
    fold_offset=320d0
  else
    call astro_message(seve%e,rname,'PICO generation not understood')
    error = .true.
    return
  endif
  !
  ! Backend definition 1: FTS
  em%be(1)%name = 'FTS'
  em%be(1)%n_backmodes = 2
  !Mode 1 = 200
  em%be(1)%mode(1)%backname = 'FTS'
  em%be(1)%mode(1)%modename = 'WIDE'
  em%be(1)%mode(1)%n_backunits = 8 ! [backend unit process 1 baseband]
  em%be(1)%mode(1)%m_chunks = 24
  em%be(1)%mode(1)%iflim(1,1) = 7680d0+fold_offset    !outer
  em%be(1)%mode(1)%iflim(2,1) = 11730d0+fold_offset
  em%be(1)%mode(1)%iflim(1,2) = 3950d0    !inner
  em%be(1)%mode(1)%iflim(2,2) = 8000d0
  !Mode 2 = 50
  em%be(1)%mode(2)%backname = 'FTS'
  em%be(1)%mode(2)%modename = 'NARROW'
  em%be(1)%mode(2)%n_backunits = 8 ! [backend unit process 1 baseband]
  em%be(1)%mode(2)%m_chunks = 24
  em%be(1)%mode(2)%iflim(1,1) = 8570d0+fold_offset    !outer
  em%be(1)%mode(2)%iflim(2,1) = 10390d0+fold_offset
  em%be(1)%mode(2)%iflim(1,2) = 5290d0    !inner
  em%be(1)%mode(2)%iflim(2,2) = 7110d0
  !
  em%be(2)%name = 'WILMA'
  em%be(2)%n_backmodes = 1
  !Mode 1
  em%be(2)%mode(1)%backname = 'WILMA'
  em%be(2)%mode(1)%modename = '2MHz'
  em%be(2)%mode(1)%n_backunits = 4 ! [backend unit process 1 baseband, 4 at most]
  em%be(2)%mode(1)%m_chunks = 16
  em%be(2)%mode(1)%iflim(1,1) = 7835d0+fold_offset    !outer
  em%be(2)%mode(1)%iflim(2,1) = 11555d0+fold_offset
  em%be(2)%mode(1)%iflim(1,2) = 4125d0    !inner
  em%be(2)%mode(1)%iflim(2,2) = 7845d0
  !
  em%be(3)%name = 'VESPA'
  em%be(3)%n_backmodes = 1
  em%be(3)%mode(1)%backname = 'VESPA'
  em%be(3)%mode(1)%modename = ''
  em%be(3)%mode(1)%n_backunits = 4 ! []
  em%be(3)%mode(1)%m_chunks = em%be(2)%mode(1)%n_backunits
  em%be(3)%mode(1)%iflim(1,1) = 9430d0-250d0+fold_offset   !outer
  em%be(3)%mode(1)%iflim(2,1) = 9430d0+250d0+fold_offset
  em%be(3)%mode(1)%iflim(1,2) = 6250d0-250d0    !inner
  em%be(3)%mode(1)%iflim(2,2) = 6250d0+250d0
  !
  do k=1,em%rec%desc%n_backends
    do i=1,em%be(k)%n_backmodes
      do j =1,em%be(k)%mode(i)%n_backunits
        em%be(k)%mode(i)%unit(j)%n_chunks = em%be(k)%mode(i)%m_chunks/em%be(k)%mode(i)%n_backunits
      enddo
    enddo
  enddo
  !
end subroutine define_emir_backends
!
subroutine emir_setup_backend(emir,iback,imode,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>emir_setup_backend
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Set backend coverage from if cables content
  !
  !-----------------------------------------------------------------------
  type(emir_t), intent(inout) :: emir
  integer(kind=4), intent(in) :: iback
  integer(kind=4), intent(in) :: imode
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='BACKEND'
  integer(kind=4)       :: icable
  integer(kind=4), parameter       :: m_cable_wilma = 4
  !
  if (emir%rec%n_tunings.eq.0) then
    call astro_message(seve%e,rname,'Please define a tuning (EMIR command) before setting up backends')
    error = .true.
    return
  endif
  !
  if (.not.emir%switch%defined) then
    call astro_message(seve%e,rname,'Please configure first the switchbox (BASEBAND command)')
    error = .true.
    return
  endif
  !
  icable = 1
  do while (icable.le.emir%be(iback)%mode(imode)%n_backunits) ! correlator units
    emir%be(iback)%mode(imode)%unit(icable)%sb_code = emir%switch%ifc(icable)%sb_code
    emir%be(iback)%mode(imode)%unit(icable)%pol_code = emir%switch%ifc(icable)%pol_code
    emir%be(iback)%mode(imode)%unit(icable)%bb_code = emir%switch%ifc(icable)%bb_code
    emir%be(iback)%mode(imode)%unit(icable)%iband = emir%switch%ifc(icable)%iband
    emir%be(iback)%mode(imode)%unit(icable)%ifmin = &
        emir%be(iback)%mode(imode)%iflim(1,emir%be(iback)%mode(imode)%unit(icable)%bb_code)
    emir%be(iback)%mode(imode)%unit(icable)%ifmax = &
        emir%be(iback)%mode(imode)%iflim(2,emir%be(iback)%mode(imode)%unit(icable)%bb_code)
    emir%be(iback)%mode(imode)%unit(icable)%width = &
        emir%be(iback)%mode(imode)%unit(icable)%ifmax-emir%be(iback)%mode(imode)%unit(icable)%ifmin
    emir%be(iback)%mode(imode)%unit(icable)%ifcenter = &
        (emir%be(iback)%mode(imode)%unit(icable)%ifmax+emir%be(iback)%mode(imode)%unit(icable)%ifmin)/2d0
    emir%be(iback)%mode(imode)%unit(icable)%label = emir%switch%ifc(icable)%label
    icable = icable+1
  enddo
  !
end subroutine emir_setup_backend
!
subroutine emir_plot_backend(emir,iback,imode,cplot,cata,drawaxis,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>emir_plot_backend
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Displays the backend coverage when switchbox setup was given by user
  !
  !-----------------------------------------------------------------------
  type(emir_t), intent(in) :: emir
  integer(kind=4), intent(in) :: iback
  integer(kind=4), intent(in) :: imode
  type(current_boxes_t), intent(inout) :: cplot
  type(plot_molecules_t), intent(in) :: cata
  type(frequency_axis_t), intent(in) :: drawaxis
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='BACKEND'
  integer(kind=4)       :: it,is,ib,sb_code(m_sideband),ibox,iu,ich,ku,irec,ib1,ib2,iinc
  character(len=16)     :: isbcol(m_sideband)
  real(kind=8)          :: px,py,yoff,xt,yt,wch,bandoff
  character(len=200)    :: comm,bbname,boxlabel
  character(len=200)    :: defchar,smallchar,molchar
  type(draw_rect_t)     :: backrect
  type(frequency_box_phys_t) :: recbox
  type(draw_line_t)     :: chline
  integer(kind=4), parameter :: iframedash = 3
  integer(kind=4), parameter :: idefdash = 1
  !
  if (.not.emir%switch%defined) then
    call astro_message(seve%e,rname,'Please configure first the switchbox (BASEBAND command)')
    error = .true.
    return
  endif
  !Determine number of box to plot
  if (emir%rec%n_tunings.eq.0) then
    call astro_message(seve%e,rname,'Please define a tuning (EMIR command) before setting up backends')
    error = .true.
    return
  else if (emir%rec%n_tunings.eq.1) then
    cplot%nbox = emir%rec%desc%n_sbands*emir%rec%desc%n_bbands
  else if  (emir%rec%n_tunings.eq.2) then
    if (emir%setup%sb_mode.eq.'SSB') then
      cplot%nbox = emir%rec%n_tunings*emir%rec%desc%n_bbands
    else if (emir%setup%sb_mode.eq.'2SB'.or.emir%switch%mode.eq.'LIST') then
       cplot%nbox = emir%rec%n_tunings*emir%rec%desc%n_sbands*emir%rec%desc%n_bbands
    else
      call astro_message(seve%e,rname,'Problem with EMIR setup (SSB or 2SB)')
      error = .true.
      return
    endif
  else
    call astro_message(seve%e,rname,'EMIR supports only 1 or 2 tuning bands')
    error = .true.
    return
  endif
  ! Reset boxes
  do ib=1,cplot%nbox
    call rec_reset_box(cplot%box(ib),error)
    if (error) return
  enddo
  !
  ! Clear the current window, and ONLY the current window
  call gr_execl('CHANGE DIRECTORY') ! No arg = go to the top directory
  call gr_execl('CLEAR DIRECTORY') ! No arg = clear the whole window
  ! get page info
  px = 0
  py = 0
  call sic_get_dble('page_x',px,error)
  if (error) return
  call sic_get_dble('page_y',py,error)
  if (error) return
  !
  if (cplot%nbox.ge.5) then
  cplot%desc%defchar = 0.3
  cplot%desc%smallchar = 0.2
  cplot%desc%molchar = 0.2
  else
  cplot%desc%defchar = 0.35
  cplot%desc%smallchar = 0.3
  cplot%desc%molchar = 0.2
  endif
  write (defchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%defchar
  write (smallchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%smallchar
  write (molchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%molchar
  call gr_exec1(defchar)
  call gr_exec1('TICKSPACE 0 0 0 0')
  call gr_pen(colour=adefcol,idash=1,error=error)
  if (error) return
  ! set plot parameters
  if (cplot%nbox.gt.4) then
    yoff = 1.d0
  else
    yoff = 1.5d0
  endif
  ibox = 0
  bandoff = -1
  irec = 0
  do it=1,emir%rec%n_tunings !tunings
    bandoff = bandoff+1
    if (emir%rec%desc%n_sbands.eq.2) then
      sb_code(1) = lsb_code !lsb first to show increasing frequencies
      sb_code(2) = usb_code
      isbcol(1) = sb_col(lsb_code)
      isbcol(2) = sb_col(usb_code)
    else if (emir%rec%desc%n_sbands.eq.1) then
      sb_code(1) = emir%rec%tune(it)%sb_code
      isbcol(1) = sb_col(emir%rec%tune(it)%sb_code)
    else
      call astro_message(seve%e,rname,'Problem with number of sidebands')
      error = .true.
      return
    endif
    do is=1,emir%rec%desc%n_sbands !sidebands
      if (emir%switch%mode.ne.'LIST'.and.               &
          emir%setup%sb_mode.ne.'2SB'.and.              &
          sb_code(is).ne.emir%setup%sb_code(it)) cycle
      !sort basebands by frequency
      if (sb_code(is).eq.lsb_code) then
        ib1 = 1
        ib2 = emir%rec%desc%n_bbands
        iinc = 1
      else if (sb_code(is).eq.usb_code) then
        ib1 = emir%rec%desc%n_bbands
        ib2 = 1
        iinc = -1
      else
        call astro_message(seve%e,rname,'Problem with sideband determination')
        error = .true.
        return
      endif
      do ib=ib1,ib2,iinc
        ibox = ibox+1
        boxlabel = ''
        cplot%box(ibox)%iband = emir%rec%tune(it)%iband
        cplot%box(ibox)%sb_code = sb_code(is)
        write (bbname,'(a,1x,a,a)') emir%setup%polar(it),sideband(cplot%box(ibox)%sb_code)(1:1),          &
                                  emir%rec%desc%bbname(ib)(1:1)
        cplot%box(ibox)%phys%sx = px-4.5d0 ! depends on plot orientation
        cplot%box(ibox)%phys%sy = min((py-3.d0)/cplot%nbox-yoff,4.) ! depends on baseband number
        cplot%box(ibox)%phys%xmin = 3.d0
        cplot%box(ibox)%phys%xmax = cplot%box(ibox)%phys%xmin+cplot%box(ibox)%phys%sx
        cplot%box(ibox)%phys%ymax = py-2.5d0-(cplot%box(ibox)%phys%sy+yoff)*(ibox-1)-bandoff
        cplot%box(ibox)%phys%ymin = cplot%box(ibox)%phys%ymax-cplot%box(ibox)%phys%sy
        if (it.ne.irec) then
          recbox%ymax = cplot%box(ibox)%phys%ymax+1.6*cplot%desc%defchar
          irec = it
          recbox%xmin = cplot%box(ibox)%phys%xmin-1
          recbox%xmax = cplot%box(ibox)%phys%xmax+0.5
        endif
        write (comm,'(a,i0)') 'CREATE DIRECTORY BOX',ibox
        call gr_execl(comm)
        write (comm,'(a,i0)') 'CHANGE DIRECTORY BOX',ibox
        call gr_execl(comm)
!         call rec_def_fbox(cplot%box(ibox)%sb_code,rec%desc%bblim(1,ib),         &
!                           rec%desc%bblim(2,ib),'IF1',cplot%box(ibox),           &
!                           rec%source%dopshift,rec%tune(it),error)
        call rec_def_fbox(emir%rec%desc%bblim(1,ib),emir%rec%desc%bblim(2,ib), &
                          'IF1',cplot%box(ibox),emir%rec%desc,emir%rec%source,emir%rec%tune(it)%flo1,error)
        if (error) return
        ! Locate the box
        call rec_locate_fbox(cplot%box(ibox)%phys,error)
        if (error) return
        ! identify different bands
        if ((is.eq.emir%rec%desc%n_sbands.and.ib.eq.ib2).or.(ib.eq.ib2.and.emir%setup%sb_mode.eq.'SSB')) then
          recbox%ymin = cplot%box(ibox)%phys%ymin-1.6*cplot%desc%defchar
          write (comm,'(a,i0)') 'CHANGE DIRECTORY'
          call gr_execl(comm)
          call rec_draw_physrect(recbox,adefcol,iframedash,error)
          if (error) return
          write (comm,'(a,1x,f0.3)') 'SET CHARACTER',2*cplot%desc%defchar
          call gr_exec1(comm)
          if (emir%rec%source%z.ne.0) then
            call gr_pen(colour=azcol,idash=idefdash,error=error)
            if (error) return
          endif
          write (comm,'(a,a,a)') 'DRAW TEXT -1.8 0 "',                    &
                      trim(emir%rec%desc%bandname(emir%rec%tune(it)%iband)),'" 5 90 /CHARACTER 4'
          call gr_exec1(comm)
          call gr_exec1(defchar)
!          Comment here until we create a short title that fits well in the plot
          if (emir%rec%tune(it)%outlo) then
            call gr_pen(colour=aconflictcol,idash=1,error=error)
            if (error) return
            write (comm,'(a)') 'DRAW TEXT -1.2 0 "LO out of recommended range" 5 90 /CHARACTER 4'
            call gr_exec1(comm)
            call gr_pen(colour=adefcol,idash=1,error=error)
            if (error) return
          endif
!           call rec_make_title(rec%tune(it),mess,error)
!           if (error) return
!           write (comm,'(a,1x,a,1x,a)') 'DRAW TEXT -2.3 0 "',trim(mess),'" 5 90 /CHARACTER 4'
!           call gr_exec1(comm)
          call gr_pen(colour=adefcol,idash=1,error=error)
          if (error) return
          write (comm,'(a,i0)') 'CHANGE DIRECTORY BOX',ibox
          call gr_execl(comm)
          call gr_exec1(defchar)
          call gr_pen(colour=adefcol,idash=1,error=error)
          if (error) return
        endif
        ! draw header
        if (ibox.eq.1) then
          call gr_exec1(defchar)
          call gr_pen(colour=apolcol,idash=1,error=error)
          if (error) return
          if (emir%switch%mode.eq.'LIST') then
            call gr_pen(colour=adefcol,idash=1,error=error)
            if (error) return
            write (comm,'(a,1x,a,1x,a)') 'DRAW TEXT +5 2.5 "USER DEFINED" 5 0 /CHARACTER 7'
            call gr_exec1(comm)
          else
            write (comm,'(a,1x,a,1x,a)') 'DRAW TEXT +3 2.5 "',trim(emir%setup%polmode),'" 5 0 /CHARACTER 7'
            call gr_exec1(comm)
            call gr_pen(colour=adefcol,idash=1,error=error)
            if (error) return
            write (comm,'(a,1x,a,1x,a)') 'DRAW TEXT +6 2.5 " POLAR" 5 0 /CHARACTER 7'
            call gr_exec1(comm)
          endif
          call rec_draw_source(rname,emir%rec%source,error)
          if (error) return
          call gr_exec1(defchar)
        endif
        call gr_pen(colour=adefcol,idash=1,error=error)
        if (error) return
        !fill in default colorize
        backrect%xmin = cplot%box(ibox)%if1%xmin
        backrect%xmax = cplot%box(ibox)%if1%xmax
        backrect%ymin = cplot%box(ibox)%if1%ymin
        backrect%ymax = cplot%box(ibox)%if1%ymax
        backrect%col = anotselcol
        call rec_draw_frect(backrect,cplot%box(ibox)%if1,error)
        if (error) return
        do iu=1,emir%be(iback)%mode(imode)%n_backunits ! loop on backend units
          if (emir%be(iback)%mode(imode)%unit(iu)%iband.ne.emir%rec%tune(it)%iband.or.               &
              emir%be(iback)%mode(imode)%unit(iu)%sb_code.ne.sb_code(is).or.                     &
              emir%be(iback)%mode(imode)%unit(iu)%bb_code.ne.ib.or.            &
              emir%be(iback)%mode(imode)%unit(iu)%ifcenter.eq.0) cycle
          backrect%xmin = emir%be(iback)%mode(imode)%unit(iu)%ifmin
          backrect%xmax = emir%be(iback)%mode(imode)%unit(iu)%ifmax
          backrect%ymin = cplot%box(ibox)%if1%ymin
          backrect%ymax = cplot%box(ibox)%if1%ymax
          backrect%col = ahighrescol
          call rec_draw_frect(backrect,cplot%box(ibox)%if1,error)
          if (error) return
          wch = emir%be(iback)%mode(imode)%unit(iu)%width/emir%be(iback)%mode(imode)%unit(iu)%n_chunks
          do ich=1,emir%be(iback)%mode(imode)%unit(iu)%n_chunks-1
            chline%xmin = emir%be(iback)%mode(imode)%unit(iu)%ifmin+wch*ich
            chline%xmax = emir%be(iback)%mode(imode)%unit(iu)%ifmin+wch*ich
            chline%ymin = cplot%box(ibox)%if1%ymin
            chline%ymax = cplot%box(ibox)%if1%ymax
            chline%col = achunkcol
            chline%dash = 2
            call rec_draw_line(chline,cplot%box(ibox)%if1,error)
            if (error) return
          enddo ! chunks
          write (boxlabel,'(a,1x,a,i0,1x,a)') trim(boxlabel),trim(emir%be(iback)%mode(imode)%backname)  &
                    ,iu,trim(emir%be(iback)%mode(imode)%unit(iu)%label)
          ! check for overlap
          do ku = 1,emir%be(iback)%mode(imode)%n_backunits !overlap
            if (ku.ne.iu) then
              if (emir%be(iback)%mode(imode)%unit(ku)%iband.eq.emir%rec%tune(it)%iband.and.               &
                emir%be(iback)%mode(imode)%unit(ku)%sb_code.eq.sb_code(is).and.                     &
                emir%be(iback)%mode(imode)%unit(ku)%bb_code.ne.ib) then
                backrect%col = aoverlapcol
                backrect%xmin = emir%be(iback)%mode(imode)%unit(ku)%ifmin
                backrect%xmax = emir%be(iback)%mode(imode)%unit(ku)%ifmax
                backrect%ymin = cplot%box(ibox)%if1%ymin
                backrect%ymax = cplot%box(ibox)%if1%ymax/2
                call rec_draw_frect(backrect,cplot%box(ibox)%if1,error)
                if (error) return
              endif
            endif
          enddo !overlap
          call gr_pen(colour=adefcol,idash=1,error=error)
          if (error) return
          !plot  molecules
          call gr_exec1(molchar)
          call rec_draw_molecules(cata,cplot%box(ibox)%rest,error)
          if (error) return
          call gr_exec1(defchar)
        enddo !correl units
        !
        ! show tuning line
        if (cplot%box(ibox)%rest%xmin.le.emir%rec%tune(it)%frest.and.cplot%box(ibox)%rest%xmax.ge.emir%rec%tune(it)%frest) then
          call rec_draw_linetune(emir%rec%tune(it),cplot%box(ibox)%rest,error)
          if (error) return
        endif
        !
        ! Draw unit name
        xt = 0.d0
        yt = 1d0
        call gr_exec1(smallchar)
        write (comm,'(a,1x,f0.3,1x,f0.3,1x,a,a,a)')             &
              'DRAW TEXT',xt,yt,'"',trim(boxlabel),'" 6 0 /CHARACTER 1'
        call gr_exec1(defchar)
        call gr_exec1(comm)
        ! Draw the box
        !
        call rec_draw_fbox(cplot,ibox,drawaxis,error)
        if (error) return
        call gr_execl('CHANGE DIRECTORY')
      enddo !basebands
    enddo !sidebands
  enddo !tunings
  cplot%desc%plotmode = pm_basebands
  !
end subroutine emir_plot_backend
