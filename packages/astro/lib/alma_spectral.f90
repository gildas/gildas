subroutine alma_spectral(line,error)
  use gbl_message
  use gkernel_interfaces
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  ! ALMA internal routine, support for command SPWINDOW
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Local
  character(len=*), parameter :: rname = 'ALMA_SPECTRAL'
  logical :: update, message, okmode, interactive
  logical :: oldspw, restrict, donewplot
  integer(kind=4) :: i, ibb, lun
  integer(kind=4) :: polar, kmode, ispw, sign
  real(kind=4) :: cfreq, rwidth, use, awidth
  real(kind=4) :: x4, y4
  real(kind=8) :: xu, yu,ifmin, ifmax
  integer(kind=4) :: old_nspw, new_nspw, old_spw_polar, old_spw_sideband, iwidth
  real(kind=4) :: old_spw_width, old_spw_cfreq, old_spw_use
  character(len=1) :: code
  character(len=message_length) :: mess
  character(len=3) :: bmode(3)
  data bmode /'LSB','USB','DSB'/
  ! ---------------------------------------------------------------------------
  !
  ! Baseband number : either with option BASEBAND or from the current plot
  !
  donewplot = .false.
  if (.not.sic_present(1,0)) then
    if (plot_mode.gt.10) then
      ibb = plot_mode-10
    else
      call astro_message(seve%e,rname,'Missing /BASEBAND option')
      error = .true.
      return
    endif
  else
    call sic_i4 (line,1,1,ibb,.true.,error)
    if (error) return
    if (ibb.lt.1 .or. ibb.gt.4) then
      call astro_message(seve%e,rname,'Invalid correlator baseband')
      error = .true.
      return
    endif
    donewplot = .true.
  endif
  !
  ! This baseband must have already been defined
  !
  if (.not.bb_def(ibb)) then
    call astro_message(seve%e,rname,'Baseband not defined')
    error = .true.
    return
  endif
  !
  ! /RESET
  !
  if (sic_present(4,0)) then
    bb_nspw(ibb) = 0   ! number of spectral windows defined on baseband ibb
    call astro_message(seve%i,rname,'Resetting spectral window on that baseband')
    return
  endif
  !
  ! Spectral window id
  !
  call sic_i4(line,0,1,ispw,.true.,error)
  if ((ispw.gt.bb_nspw(ibb)+1).or.(ispw.lt.1)) then
    call astro_message(seve%e,rname,'Spectral Window number does not make sense')
    error = .true.
    return
  endif
  !
  ! New spectral window or existing one?
  !
  old_nspw = bb_nspw(ibb)
  if (ispw.gt.bb_nspw(ibb)) then
    bb_nspw(ibb) = bb_nspw(ibb)+1
    oldspw = .false.
    ! defaults
    rwidth = 125 ! why not?
    cfreq = 0    ! spwindow centered in baseband
    polar = 2    ! default
    use = 100
  else
    ! modify an already existing window -> bb_nspw not modified
    oldspw = .true.
    ! defaults are defined from the existing window
    rwidth = spw_width(ibb,ispw)
    cfreq = spw_cfreq(ibb,ispw)+bb_cfreq(ibb)
    polar = spw_polar(ibb,ispw)
    use = spw_use(ibb,ispw)
  endif
  new_nspw = bb_nspw(ibb)
  interactive = .true.
  !
  ! Save parameters in case new definition is not valid
  !
  old_spw_width = spw_width(ibb,ispw)
  old_spw_cfreq = spw_cfreq(ibb,ispw)
  old_spw_polar = spw_polar(ibb,ispw)
  old_spw_use = spw_use(ibb,ispw)
  old_spw_sideband = spw_sideband(ibb,ispw)
  !
  ! Bandwidth
  !
  if (sic_present(0,2)) then
    call sic_r4(line,0,2,rwidth,.true.,error)
    if (error) then
      bb_nspw(ibb) = old_nspw
      return
    endif
    ! Round to nearest available bandwidth
    if (rwidth.le.500) then
      awidth = log(rwidth/31.25)/log(2.0)
      iwidth = max(0,nint(awidth))
      awidth = 31.25*(2**iwidth)
      if (awidth.ne.rwidth) then
        if (abs((rwidth-awidth)/awidth).lt.0.1) then
          write(mess,'(A,F6.2)') awidth
          call astro_message(seve%i,rname,mess)
          rwidth = awidth
        endif
      endif
    endif
    !
    if (rwidth.ne.2000 .and. rwidth.ne.1000  .and.  &
        rwidth.ne.500  .and. rwidth.ne.250   .and.  &
        rwidth.ne.125  .and. rwidth.ne.62.5   .and.  &
        rwidth.ne.31.25) then
      call astro_message(seve%e,rname,'Invalid bandwidth')
      error = .true.
      bb_nspw(ibb) = old_nspw
      return
    endif
  endif
  !
  ! Polar. products /POLAR option
  !
  if (sic_present(2,0)) then
    call sic_i4(line,2,1,polar,.true.,error)
    if (polar.ne.1 .and. polar.ne.2 .and. polar.ne.4) then
      call astro_message(seve%e,rname,'Invalid polarization products')
      error =.true.
      bb_nspw(ibb) = old_nspw
      return
    endif
    if (oldspw) interactive = .false.
  endif
  !
  ! Fraction of correlator /USE option
  !
  if (sic_present(3,0)) then
    call sic_r4(line,3,1,use,.true.,error)
    if (error) return
    if (use.lt.1) use = use*100.
    if (use.eq.3) use = 3.125
    if (use.eq.6) use = 6.25
    if (use.eq.12) use = 12.5
    if (use.ne.100  .and.  use.ne.50  .and.  use.ne.25  .and.  &
        use.ne.12.5 .and.  use.ne.6.25  .and.  use.ne.3.125) then
      call astro_message(seve%e,rname,'Invalid correlator fraction')
      error = .true.
      bb_nspw(ibb) = old_nspw
      return
    endif
    if (oldspw) interactive = .false.
  endif
  !
  ! Center frequency from command line
  !
  if (sic_present(0,3)) then
    call sic_r4(line,0,3,cfreq,.true.,error)
    if (error) return
    if ((cfreq.le.(bb_cfreq(ibb)-1000+rwidth/2.-tolerance_freq)) .or. &
        (cfreq.ge.(bb_cfreq(ibb)+1000-rwidth/2.+tolerance_freq))) then
      call astro_message(seve%e,rname,'Invalid center frequency')
      write (mess,'(A,f10.1,f10.1)') "Allowed range ", &
      bb_cfreq(ibb)-1000+rwidth/2.-tolerance_freq,  &
      bb_cfreq(ibb)+1000-rwidth/2.+tolerance_freq
      call astro_message(seve%i,rname,mess)
      error = .true.
      bb_nspw(ibb) = old_nspw
      return
    endif
    !
    ! A spectral window has now been fully defined
    ! NB: cfreq is the spwindow central frequency, in the IF1 band
    !
    write(mess,'(a,i2,a,i1)') 'Requested spectral window ',ispw,' on baseband ',ibb
    call astro_message(seve%i,rname,mess)
    write(mess,'(a,f7.2,a,f8.2 ,a, i1,a,f6.2,a)') 'Bandwidth ',rwidth,' MHz / IF center ', &
      cfreq,' MHZ / Polar = ',polar,' / Using ',use, '%'
    call astro_message(seve%r,rname,mess)
    interactive = .false.
  endif
  !
  ! /RESTRICT option - to have all possible restrictions
  !
  restrict = sic_present(5,0)
  !
  ! Interactive definition of the spectral window
  !
  if (interactive) then
    !
    ! plot_mode must 11-14
    !
    if ((plot_mode.le.10).or.(donewplot)) then
      bb_nspw(ibb) = old_nspw
      plot_mode = 10+ibb
      call alma_plot_def(error)
      if (error) return
      call alma_plot_line
      bb_nspw(ibb) = new_nspw
    endif
    !
    ! Update spwindow plots, to have selected window in dash
    !
    call gr_exec1('PEN 1 /WEIGHT 1')
    call gt_clear_segment('SPWINDOW',.false.,error)
    call alma_plot_spectral(ibb,ispw)
    call gr_exec1('PEN 0')
    !
    ! Loop on cursor output
    !
    code = ' '
    call astro_message(seve%i,rname,'Interactive definition of the spectral window')
    call astro_message(seve%r,rname,'    Left click to define position / narrow input')
    call astro_message(seve%r,rname,'    + to increase bandwidth')
    call astro_message(seve%r,rname,'    - to decrease bandwidth')
    call astro_message(seve%r,rname,'    > to shift position by 5 MHz (increase RF)')
    call astro_message(seve%r,rname,'    < to shift position by 5 MHz (decrease RF)')
    call astro_message(seve%r,rname,'    P to change polarization product (1-2-4)')
    call astro_message(seve%r,rname,'    U to decrease fraction of correlator')
    call astro_message(seve%r,rname,'    I to increase fraction of correlator')
    call astro_message(seve%r,rname,'    E to exit')
    !
    write(mess,'(a,i2,a,i1)') 'Requested spectral window ',ispw,' on baseband ',ibb
    call astro_message(seve%i,rname,mess)
    if (oldspw) then
      write(mess,'(a,f7.2,a,f8.2 ,a, i1,a,f6.2,a)') 'Bandwidth ',rwidth,' MHz / IF center ', &
               cfreq,' MHZ / Polar = ',polar,' / Using ',use, '%'
      call astro_message(seve%r,rname,mess)
    endif
    !
    do while (code.ne.'E')
      !
      message = .false.
      update = .false.
      call gr_curs (xu, yu, x4, y4, code)
      !
      if (code.eq.'^') then
        !
        ! Left click : define central frequency
        !
        ifmin = min(iflim(1),iflim(2))
        ifmax = max(iflim(1),iflim(2))
        if ((yu.lt.0).or.(yu.gt.5).or.(xu.lt.ifmin).or.(xu.gt.ifmax)) then
          call astro_message(seve%e,rname,'Please click within the baseband plot')
        else
          cfreq = xu
          message =.true.
          update = .true.
        endif
      else
        !
        ! Too lazy to use SHIFT
        !
        if (code.eq.'=') code = '+'
        if (code.eq.',') code = '<'
        if (code.eq.'.') code = '>'
        !
        ! Move/expand/modify unit
        !
        sign = bb_sideband(ibb)
        if ((code.eq.'+').and.(rwidth.ne.2000)) rwidth = rwidth*2
        if ((code.eq.'-').and.(rwidth.ne.31.25)) rwidth = rwidth/2
        if (code.eq.'<') cfreq = cfreq-sign*5
        if (code.eq.'>') cfreq = cfreq+sign*5
        if ((code.eq.'U').and.(use.ne.3.125)) use = use/2.
        if ((code.eq.'I').and.(use.ne.100)) use = use*2.
        if (code.eq.'P') then
          polar = polar*2
          if (polar.eq.8) polar = 1
        endif
        !
        update = (code.eq.'+').or.(code.eq.'-').or.(code.eq.'<').or.(code.eq.'>')
        message = update.or.(code.eq.'U').or.(code.eq.'I').or.(code.eq.'P')
      endif
      !
      ! Message (if something has changed)
      !
      if (message) then
        write(mess,'(a,f7.2,a,f8.2 ,a, i1,a,f6.2,a)') 'Bandwidth ',rwidth,' MHz / IF center ', &
              cfreq,' MHZ / Polar = ',polar,' / Using ',use, '%'
        call astro_message(seve%r,rname,mess)
      endif
      !
      ! Update plot
      !
      if (update) then
        plot_mode = 10+ibb
        spw_width(ibb,ispw) = rwidth
        spw_cfreq(ibb,ispw) = cfreq-bb_cfreq(ibb)
        spw_polar(ibb,ispw) = polar
        spw_use(ibb,ispw) = use
        !
        call gr_exec1('PEN 1 /WEIGHT 1')
        call gt_clear_segment('SPWINDOW',.false.,error)
        call alma_plot_spectral(ibb,ispw)
        call gr_exec1('PEN 0')
      endif
    enddo
  endif
  !
  ! Check if that mode is available (depending on the ALMA Cycle)
  ! and/or if several sub-modes are possible
  !
  call explore_alma_modes(rwidth,polar,use,kmode,error)
  okmode = .not.error
  !
  ! Check other restrictions (depending on the ALMA Cycle)
  !
  ! NB restrict = all possible restrictions. Define earlier in the
  ! routine.
  !
  call check_spwindow(ispw, ibb, polar, use, rwidth, cfreq, restrict, error)
  okmode = okmode.and.(.not.error)
  !
  ! Everything OK, add new spectral window to the list
  !
  if (okmode) then
    i = ispw
    spw_width(ibb,i) = rwidth
    spw_cfreq(ibb,i) = cfreq-bb_cfreq(ibb)
    spw_polar(ibb,i) = polar
    spw_use(ibb,i) = use
    spw_sideband(ibb,i) = bb_sideband(ibb)
    write(mess,'(a,i2,a,i1)') 'Adding/modifying spectral window # ',ispw,' on baseband ',ibb
    call astro_message(seve%i,rname,mess)
  else
    !
    ! Not OK, back to initial values
    !
    bb_nspw(ibb) = old_nspw
    i = ispw
    spw_width(ibb,i) = old_spw_width
    spw_cfreq(ibb,i) = old_spw_cfreq
    spw_polar(ibb,i) = old_spw_polar
    spw_use(ibb,i) = old_spw_use
    spw_sideband(ibb,i) = bb_sideband(ibb)
  endif
  !
  ! Do the new plot
  !
  call astro_message(seve%r,rname,'')
  plot_mode = 10+ibb
  call alma_plot_line
  !
  1000 format(4x,i2,3x,f7.2,3x,f7.2,3x,f10.5,3x,i1,3x,f7.3,'%')
end subroutine alma_spectral
!
subroutine alma_plot_spectral(ibb,specialunit)
  use ast_line
  use gildas_def
  !---------------------------------------------------------------------
  ! @ private
  ! Plot spectral window on baseband ibb
  ! Window #ispw is plotted in dash
  !---------------------------------------------------------------------
  integer(kind=4) :: ibb            !
  integer(kind=4) :: specialunit    !
  ! Local
  integer(kind=size_length) :: nxy
  logical :: error
  integer(kind=4) :: i, j, dash
  real(kind=4) :: x(8),y(8), flo, z(8), bandwidth, edge
  real(kind=8) :: doppler
  character(len=80) :: chain
  data z/0.0,2.0,2.0,0.0,2.5,2.0,2.0,0.0/
  !
  do i=1,bb_nspw(ibb) ! loop on spw defined for baseband ibb
     !
     bandwidth = spw_width(ibb,i)
     flo = spw_cfreq(ibb,i)+bb_cfreq(ibb)
     !
     ! Box size depends on unit (will fail if more than 8 units...)
     !
     do j=1,8
        y(j) = (1.0+0.05*(i-1))*z(j)
     enddo
     !
     ! Actual band width is 15/16 of the nominal width
     !
     bandwidth = bandwidth*15./16.
     !
     ! Rough indication of zone affected by possible doppler shift
     ! (not in cursor mode)
     !
     if (i.ne.specialunit) then
        nxy = 4
        doppler = 1e-4*flo1
        call gr_exec1('PEN 10 /DASH 1 /COL 10')
        call gr_segm('DOPPLER',error)
        x(1) = flo+0.5*bandwidth-doppler
        x(2) = flo+0.5*bandwidth-doppler
        x(3) = flo+0.5*bandwidth
        x(4) = flo+0.5*bandwidth
        call gr4_ufill(nxy,x,y)
        x(1) = flo-0.5*bandwidth+doppler
        x(2) = flo-0.5*bandwidth+doppler
        x(3) = flo-0.5*bandwidth
        x(4) = flo-0.5*bandwidth
        call gr4_ufill(nxy,x,y)
        call gr_segm_close(error)
     endif
     !
     ! Do NOT keep 5% slope
     !
     edge = 0.05*bandwidth
     x(1) = flo+0.5*bandwidth
     x(2) = flo+0.5*bandwidth ! -edge
     x(3) = flo-0.5*bandwidth ! +edge
     x(4) = flo-0.5*bandwidth
     !
     ! Dash pen if i=ispw
     !
     dash = 1
     if (i.eq.specialunit) dash = 3
     write (chain, 200) ibb, dash
     if (ibb.eq.4) write(chain, 200) 6, dash
     call gr_exec1(chain)
     call gr_segm('SPWINDOW',error)
     call gr4_connect(4,x,y,0.0,-1.0)
     call gr_segm_close(error)
     call gr_exec1('PEN 0 /DASH 1')
  enddo
  !
  100 format('DRAW TEXT ',f10.2, f10.2, i2, ' /USER')
  200 format('PEN ',i2,' /WEIGHT 1 /DASH ',i2)
end subroutine alma_plot_spectral
!
subroutine explore_alma_modes(rwidth,polar,use,kmode,error)
  use gbl_message
  use ast_line
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! Check if the requesed mode is available (depending on the ALMA Cycle)
  ! and/or if several sub-modes are possible
  !---------------------------------------------------------------------
  real(kind=4) :: rwidth            !
  integer(kind=4) :: polar          !
  real(kind=4) :: use               !
  integer(kind=4) :: kmode          !
  logical :: error                  !
  ! Local
  integer(kind=4) :: i, lun, ier
  integer(kind=4) :: nmode, maxmode
  parameter (maxmode=120)
  integer(kind=4) :: amode(maxmode), apolar(maxmode), achannels(maxmode)
  integer(kind=4) :: abits(maxmode), anyquist(maxmode)
  real(kind=4) :: ause(maxmode), awidth(maxmode), asensib(maxmode)
  character(len=13) :: rname
  character(len=message_length) :: mess
  character(len=132) :: chain, corrfile, message, corrmodefile
  data rname /'ALMA_SPECTRAL'/
  !
  ! Open file with ALMA correlator modes
  !
  error = .false.
  !
  corrmodefile = 'alma_correlator_modes'
  if (alma_cycle.eq.0) corrmodefile = 'alma_correlator_modes_cycle0'
  if (alma_cycle.eq.1) corrmodefile = 'alma_correlator_modes_cycle1'
  !
  if (.not.sic_query_file(corrmodefile,'data#dir:','.dat',corrfile)) then
     call astro_message(seve%f,rname,'ALMA correlator file not found')
	   Print *, trim(corrmodefile)
     error = .true.
     return
  endif
  ier = sic_getlun (lun)
  ier = sic_open(lun,corrfile,'OLD',.true.)
  if (ier.ne.0) then
    call astro_message(seve%e,rname,'Cannot open file: '//corrfile)
    return
  endif
  !
  ! Read file
  !
  i = 0
  do
    read(lun,'(A)',iostat=ier) chain
    if (ier.lt.0) exit ! EOF
    if (ier.ne.0 .or. lenc(chain).eq.0) cycle
    if (chain(1:1).ne.'!') then
      i = i+1
      read(chain,*,iostat=ier) amode(i), apolar(i), awidth(i), ause(i), &
                    achannels(i), abits(i), anyquist(i), asensib(i)
    endif
  enddo
  !
  close(unit=lun)
  call sic_frelun(lun)
  nmode = i
  !
  ! Now explore the modes
  !
  ! First pass to find if this mode does exist
  !
  kmode = 0
  do i=1,nmode
     if ((apolar(i).eq.polar).and.(awidth(i).eq.rwidth) .and. &
          (ause(i).eq.use)) then
        kmode = kmode+1
     endif
  enddo
  !
  ! If it exists, list the possible submodes
  !
  if (kmode.gt.1) then
     call astro_message(seve%r,rname,'Possible submodes are:')
     call astro_message(seve%r,rname,  &
       '  Mode  Pol   Width      Use      Nchn Bits  Nyq Sensib.')
     do i=1,nmode
        if ((apolar(i).eq.polar).and.(awidth(i).eq.rwidth) .and. &
             (ause(i).eq.use)) then
           message = ''
           if (achannels(i).eq.8192) message = ' --> best resolution'
           if (asensib(i).eq.0.99) message = ' --> best sensitivity'
           write(mess,1000) amode(i), apolar(i), awidth(i), ause(i),&
             achannels(i), abits(i), anyquist(i), asensib(i), &
             message(1:lenc(message))
           call astro_message(seve%r,rname,mess)
        endif
     enddo
  elseif (kmode.eq.1) then
	! call astro_message(seve%i,rnane,'Found correlator mode')
  else
     write (chain,'(a,i1)') 'This correlator mode is not available in Cycle ',alma_cycle
     call astro_message(seve%e,rname,chain)
     error = .true.
  endif
1000 format(4x,i2,3x,i1,3x,f7.2,3x,f7.3,'%',3x,i4,3x,i1,4x,i1,3x,f4.2,a)
end subroutine explore_alma_modes
!
subroutine check_spwindow(ispw, ibb, polar, use, rwidth, cfreq, restrict, error)
  use gbl_message
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  ! Check if the required spectral mode is allowed, depending on the ALMA Cycle
  !---------------------------------------------------------------------
  integer(kind=4), intent(in) :: ispw   !
  integer(kind=4), intent(in) :: ibb    !
  integer(kind=4), intent(in) :: polar  !
  real(kind=4), intent(in) :: use       !
  real(kind=4), intent(in) :: rwidth    !
  real(kind=4), intent(in) :: cfreq     !
  logical, intent(in) :: restrict       !
  logical, intent(out) :: error         !
  ! Local
  integer(kind=4) :: i
  logical :: okmode
  real(kind=4) :: correlator, fullwidth, offset
  !
  error = .false.
  if (alma_cycle.eq.0) then
    !
    ! ALMA CYCLE 0
    !
    if (ispw.gt.1) then
      call astro_message(seve%e,'ALMA_SPECTRAL', &
          'ALMA Cycle 0: only one spectral window per baseband')
      error = .true.
    elseif (polar.eq.4) then
      call astro_message(seve%e,'ALMA_SPECTRAL', &
          'ALMA Cycle 0: only one or two polarization products')
      error = .true.
    elseif (use.ne.100) then
      call astro_message(seve%e,'ALMA_SPECTRAL', &
          'ALMA Cycle 0: cannot split correlator resources')
      error = .true.
    endif
    if (error) return
    !
    ! Check position of spectral window within the baseband
    ! (these are restrictions probably valid for all Cycles)
    !
    offset = abs(cfreq-bb_cfreq(ibb))
    if (offset.gt.950) then
      call astro_message(seve%e,'ALMA_SPECTRAL', &
        'Center frequency must be >50 MHz from baseband edge')
        error = .true.
    elseif ((offset+rwidth/2.).gt.1000) then
      call astro_message(seve%e,'ALMA_SPECTRAL', &
        'Bandwidth must be fully included within baseband')
!**!        error = .true.
    endif
    if (error) return
    !
    ! All four basebands must have the same setup
    !
    call astro_message(seve%i,'ALMA_SPECTRAL',   &
       'ALMA Cycle 0: forcing all spectral windows to same mode')
    do i = 1, 4 ! loop on basebands (already defined or not)
      spw_width(i,1) = rwidth
   	  spw_polar(i,1) = polar
	    spw_use(i,1) = use
      if (restrict) then
        !
        ! Force positions within baseband to be the same
        !
        spw_cfreq(i,1) = cfreq-bb_cfreq(ibb)
      endif
      bb_nspw(i) = 1
    enddo
    okmode = .true.
    !
  elseif (alma_cycle.eq.1) then
    !
    ! ALMA CYCLE 1
    !
    if (ispw.gt.1) then
      call astro_message(seve%e,'ALMA_SPECTRAL', &
        'ALMA Cycle 1: only one spectral window per baseband')
      error = .true.
    elseif (polar.eq.4) then
      call astro_message(seve%e,'ALMA_SPECTRAL', &
        'ALMA Cycle 1: only one or two polarization products')
      error = .true.
    elseif (use.ne.100) then
      call astro_message(seve%e,'ALMA_SPECTRAL', &
        'ALMA Cycle 1: cannot split correlator resources')
      error = .true.
    endif
    if (error) return
    !
    ! Check position of spectral window within the baseband
    ! (these are restrictions probably valid for all Cycles)
    !
    offset = abs(cfreq-bb_cfreq(ibb))
    if (offset.gt.950) then
      call astro_message(seve%e,'ALMA_SPECTRAL', &
        'Center frequency must be >50 MHz from baseband edge')
      error = .true.
    elseif ((offset+rwidth/2.).gt.1000) then
      call astro_message(seve%e,'ALMA_SPECTRAL', &
        'Bandwidth must be fully included within baseband')
!**!        error = .true.
    endif
    if (error) return
    spw_width(ibb,1) = rwidth
    spw_polar(ibb,1) = polar
    spw_use(ibb,1) = use
    okmode = .true.
  else
    !
    ! according to ALMA Memo 556
    !
    ! Test total use of correlator (must be <100%)
    !
    okmode = .true.
    spw_use(ibb,ispw) = use
    correlator = 0.
    do i=1,bb_nspw(ibb)
      correlator = correlator+spw_use(ibb,i)
    enddo
    if (correlator.gt.100) then
      call astro_message(seve%e,'ALMA_SPECTRAL','Correlator resources not available')
      ! write(mess,'(A,F6.2,A)') 'Total setup would use  ',correlator,  &
      !     ' % of the correlator quadrant'
      ! call astro_message(seve%w,rname,mess)
      okmode = .false.
    endif
    !
    ! Test total bandwidth (must be < 1000 MHz if several windows)
    !
    spw_width(ibb,ispw) = rwidth
    fullwidth = 0.
    do i=1,bb_nspw(ibb)
      fullwidth = fullwidth + spw_width(ibb,i)
    enddo
    if ((fullwidth.gt.1000).and.(bb_nspw(ibb).gt.1)) then
      call astro_message(seve%e,'ALMA_SPECTRAL','Total bandwidth cannot be > 1000 MHz in multi-window mode')
      okmode = .false.
    endif
  endif
  if (.not.okmode) error = .true.
end subroutine check_spwindow
