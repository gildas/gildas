subroutine astro_line(line,error)
  use gbl_message
  use gkernel_interfaces
  use ast_line
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  ! ASTRO 	Internal routine
  !	support for command
  !	LINE name Freq SB [Lock [Center [Harm]]]
  !	/MOLECULES FileName [Mol1 Mol2 ... Moln]        (1)
  !	/SPECTRAL Bandwidth LO3 Center [LO3 Center]     (2)
  !       /WIDTH  line_width                              (3)
  !       /IF_LIMITS ifmin ifmax                          (4)
  !       /BEST best                                      (5)
  !       /AUTO                                           (6)
  !       /VLBI                                           (7)
  !---------------------------------------------------------------------
  character(len=*), intent(inout) :: line  !
  logical, intent(inout) :: error          !
  ! Local
  character(len=12) :: name,par
  character(len=256) :: msg,chain,molfile
  integer(kind=4) :: lname, nlu, npar, lpar, kmol
  character(len=4) :: band, lock
  real(kind=8) :: fcent, eps, fsky, best, f1, lo1, lo2, vsys
  integer(kind=4) :: harm, mult, lck, nc, irec, k, pdbi, ier
  parameter (eps=(100.25d0))   ! MHz
  logical :: vlbi
  character(len=9) :: rname
  character(len=message_length) :: mess
  ! Data
  data rname /'PDBI_LINE'/
  !! REAL*8 FLO1(2), FLO2(2)
  ! Code -----------------------------------------------------------------------
  !
  ! Find IF limits to be used depending on receiver used
  ! PDBI is a flag = -1 if ALMA, SMA
  !                = 1  if <2006 system
  !                = 2  if NGRx  --- CODE IS THERE BUT NOT USED
  !
  iflim(1) = 100               ! Default IF2
  if (obs_year.eq.1995) then
    iflim(2) = 600             ! Default IF2
  elseif (obs_year.eq.2000) then
    iflim(2) = 680             ! Default IF2
  endif
  pdbi = 1                     ! Default
  if (obsname.eq.'ALMA') then
    !
    ! ALMA -- VERY OLD CODE !!
    !
    iflim(1) = 4000            ! IF1
    iflim(2) = 8000            ! IF1
    pdbi =-1
  endif
  !
  userlim = iflim
  if (sic_present(4,0)) then
    call sic_r4(line,4,1,userlim(1),.false.,error)
    call sic_r4(line,4,2,userlim(2),.false.,error)
    if (error) return
    if (userlim(1).ge.userlim(2)) then
      call astro_message(seve%e,rname,'Wrong IF limits')
      error = .true.
      return
    endif
    if (userlim(1).lt.iflim(1)) then
      userlim(1) = iflim(1)
    endif
    if (userlim(2).gt.iflim(2)) then
      userlim(2) = iflim(2)
    endif
  endif
  !
  ! /SPECTRAL
  !
  if (sic_present(2,0) .and. .not.sic_present(0,1)) then
    call plot_correlator(line, iflim, userlim, error)
    return
  endif
  !
  ! /MOLECULE filename [molecules]
  !
  if (sic_present(1,1)) then
    call sic_ch (line,1,1,msg,nc,.false.,error)
    if (error) return
    molfile = msg
    call sic_parsef (msg,molfile,' ','.DAT')
  else
    msg = ''
    ier = sic_getlog('GAG_MOLECULES',msg)
    call sic_parsef (msg,molfile,' ','.DAT')
  endif
  kmol = sic_narg(1)
  call read_lines(line,kmol,molfile)
  !
  ! /WIDTH linewidth
  !
  width = 0
  call sic_r4(line,3,1,width,.false.,error)
  if (error) return
  width = abs(width)
  !
  ! PdBI specific options
  !
  if (pdbi.eq.1) then
    !
    ! /VLBI
    !
    vlbi = sic_present(7,0)
    !
    ! /BEST 1860. or 1875.
    !
    best = 0
    call sic_r8(line,5,1,best,.false.,error)
    if (error) return
  endif
  !
  ! Decode line parameters
  ! LINE linename freq lsb|usb [low|high] [if] [harm]
  !
  ! Line name
  !
  lname = 12
  call sic_ch (line,0,1,name,lname,.true.,error)
  if (error) return
  !
  ! Prepare for reading the next parameters
  !
  if (sic_present(0,2)) then
    if (.not.sic_present (0,3)) then
      call astro_message(seve%e,rname,'Missing Sidebands')
      error = .true.
      return
    endif
    msg = line(11:)            ! Skip language and commands
    if (index(msg,'/').ne.0) then
      msg = msg(1:index(msg,'/')-1)
    endif
    nlu = lenc(msg)
    call sic_blanc(msg,nlu)
    npar = 1
    lpar = 12
    call sic_next(msg(1:),par,lpar,npar)   ! Skip line name
  endif
  !
  ! Decode frequency
  !
  if (fshift.eq.0.0d0) fshift = 1.0d0
  lpar = 12
  call sic_next(msg(npar:),par,lpar,npar)
  if (lpar.eq.0) then
    call astro_message(seve%e,rname,'Missing rest frequency')
    goto 99
  endif
  call sic_math_dble(par,lpar,freq,error)
  if (error) goto 930
  fsky = freq*1d3*fshift       ! In MHz
  !
  ! Decode sideband
  !
  lpar = 1
  call sic_next (msg(npar:),par,lpar,npar)
  if (lpar.eq.0) then
    call astro_message(seve%e,rname,'Missing sideband')
    goto 99
  endif
  call sic_upper(par(1:lpar))
  if (par(1:1).eq. 'L') then
    band = 'LSB '
    sky = -1
  elseif (par(1:1).eq. 'U') then
    band = 'USB '
    sky = +1
  else
    goto 930
  endif
  !
  ! Decode further parameters (only for Bure)
  !
  if (pdbi.gt.0) then
    !
    ! Default values for harmonic, lock, and central IF
    !
    harm = 0
    lock = 'LOW'
    lck = -1
    if (obs_year.eq.1995) then
      fcent = 350.d0
    elseif (obs_year.eq.2000) then
      fcent = 390.d0
    endif
    !
    ! Decode lock
    !
    lpar = 4
    call sic_next (msg(npar:),par,lpar,npar)
    if (lpar.eq.0) goto 300
    call sic_upper(par(1:lpar))
    if (par(1:1).eq.'L') then
      lock = 'LOW '
      lck = -1
    elseif (par(1:1).eq.'H') then
      lock = 'HIGH'
      lck = +1
    else
      goto 930
    endif
    !
    ! Decode center IF. Force it to 390 if *
    !
    lpar = 12
    call sic_next (msg(npar:),par,lpar,npar)
    if (lpar.eq.0) goto 300
    if (par(1:1).eq.'*') then
      call astro_message(seve%i,rname,'Using default center freq.')
    else
      call sic_math_dble(par,lpar,fcent,error)
      if (error) goto 930
      if (fcent.lt.110.d0 .or. fcent.gt.590.d0) then
        call astro_message(seve%e,rname,'Invalid center frequency')
        goto 930
      endif
    endif
    !
    ! Decode harmonic number
    !
    lpar = 12
    call sic_next (msg(npar:),par,lpar,npar)
    if (lpar.eq.0) goto 300
    if (par(1:1).eq.'*') then
      harm = 0
    else
      read (par(1:lpar),*,err=930) harm
      if (harm.lt.32 .or. harm.gt.70) then
        call astro_message(seve%e,rname,'Invalid harmonic number')
        goto 930
      endif
    endif
  endif
  !
  300  continue
  !
  ! Now compute and check LO frequencies
  !
  ! (1) PdBI Rx1 and Rx2 (<2006)
  !
  if (pdbi.eq.1) then
    !
    if (fsky.gt.80e3 .and. fsky.lt.120e3) then
      mult = +1
      irec = 1
      if (best.eq.0) best = 1860
    elseif (fsky.gt.200e3 .and. fsky.lt.260e3) then
      mult = 3                 ! Tripler
      irec = 2
      if (best.eq.0) best = 1875
    else
      call astro_message(seve%e,rname,'Frequency not accessible with PdBI '//  &
      'receivers')
      goto 99
    endif
    !
    ! Define harmonic number
    !
    if (harm.eq.0) then
      f1 = ( (fsky + lck*eps*mult + sky*fcent) / best - sky) / dble(mult)
      harm = nint(f1)
    endif
    !
    ! Check FLO2
    !
    lo2 = (fsky + sky*fcent + lck*mult*eps) / (mult*harm+sky)
    if (lo2.lt.1835 .or. lo2.gt.1910) then
      write(chain,'(A,1PG10.3)') 'Invalid LO2 frequency',lo2
      call astro_message(seve%e,rname,chain)
      goto 99
    elseif  (lo2.lt.1845 .or. lo2.gt.1880) then
      write(chain,'(A,1PG10.3,A)') 'LO2 frequency ',lo2,' near limits'
      call astro_message(seve%w,rname,chain)
    endif
    !
    ! Check VLBI case (FLO2 must be a multiple of 0.125)
    !
    if (vlbi) then
      lo2 = (fsky + sky*fcent + lck*mult*eps) / (mult*harm+sky)
      k = nint(lo2/0.125d0)
      if (lo2.ne.k*0.125d0) then
        lo2 = k*0.125d0
        fcent = (lo2*(mult*harm+sky) - fsky - lck*mult*eps)/sky
        write(chain,'(A,1PG12.5,A)') 'Center frequency shifted to ', fcent,  &
        ' for VLBI requirements'
        call astro_message(seve%w,rname,chain)
      endif
    endif
    !
    ! Compute FLO1 and FLO2
    !
    lo2 = (fsky + sky*fcent + lck*mult*eps) / (mult*harm+sky)
    lo1 = mult*(harm*lo2 - lck*eps)
    !! FLO2(IREC) = LO2 ! SIC variable
    !! FLO1(IREC) = LO1 ! SIC variable
    vsys = (1.d0-fshift)*299792.458
    write(mess,'(A,I2)') ' Setup valid for receiver ', irec
    call astro_message(seve%r,rname,mess)
    if (vsys.ne.0) then
      write(mess,*) 'Source velocity = ', vsys,' km/s'
      call astro_message(seve%r,rname,mess)
    endif
    write(mess,*) 'FSKY  = ',fsky/1d3,' GHz'
    call astro_message(seve%r,rname,mess)
    write(mess,*) 'FLO1  = ',lo1,' MHz'
    call astro_message(seve%r,rname,mess)
    write(mess,*) 'FLO2  = ',lo2,' MHz'
    call astro_message(seve%r,rname,mess)
    write(mess,*) 'FSYN  = ',lo2+0.5d0,' MHz'
    call astro_message(seve%r,rname,mess)
    if ((abs(mod(lo1+sky*lo2+0.5d0,1d0)-0.5d0).gt. 0.0000001).or.  &
        nint(8*lo2).ne.8*lo2) then
      write(mess,*) '** Setup invalid for VLBI **'
      call astro_message(seve%r,rname,mess)
    endif
    !
    ! Plot label
    !
    write(label,1030) name(1:lenc(name)), fsky*1d-3/fshift, band, lock,  &
    fcent, harm, '  /RECEIVER',irec,'   [V= ',vsys,' km/s]'
    ! &  'Image ' (FLO1(IREC)-SKY*(FLO2(IREC)-350.))*1D-3/FSHIFT
    call astro_message(seve%r,rname,label)
    !
    ! (2) PdBI NGRx  -- CODE NOT USED, REPLACED BY pdbi_line.f
    !
  elseif (pdbi.eq.2) then
    !
    if (fsky.lt.120e3) then
      irec = 1
    elseif (fsky.lt.200e3) then
      irec = 2
    elseif (fsky.lt.260e3) then
      irec = 3
    else
      irec = 4
    endif
    !
    ! Compute FLO1 and FLO2
    !
    lo2 = 8000
    if (sky.eq.1) then
      lo1 = fsky-6000
    else
      lo1 = fsky+6000
    endif
    !
    vsys = (1.d0-fshift)*299792.458
    write(mess,*) '** Support of NGRX still preliminary **'
    call astro_message(seve%r,rname,mess)
    if (vsys.ne.0) then
      write(mess,*) 'Source velocity = ', vsys,' km/s'
      call astro_message(seve%r,rname,mess)
    endif
    write(mess,*) 'FSKY  = ',fsky/1d3,' GHz'
    call astro_message(seve%r,rname,mess)
    write(mess,*) 'FLO1  = ',lo1,' MHz'
    call astro_message(seve%r,rname,mess)
    write(mess,*) 'FLO2  = ',lo2,' MHz'
    call astro_message(seve%r,rname,mess)
    lo2 = 0                    ! to have plot in IF1
    !
    ! Plot label
    !
    write(label,1040) name(1:lenc(name)), fsky*1d-3/fshift, band,   &
    '   *** PdBI NGRX *** '
    call astro_message(seve%r,rname,label)
    !
    ! (3) ALMA
    !
  else
    !
    ! Compute FLO1 and FLO2
    !
    lo2 = 8000
    if (sky.eq.1) then
      lo1 = fsky-6000
    else
      lo1 = fsky+6000
    endif
    !
    vsys = (1.d0-fshift)*299792.458
    write(mess,*) '** Support of ALMA still preliminary **'
    call astro_message(seve%r,rname,mess)
    if (vsys.ne.0) then
      write(mess,*) 'Source velocity = ', vsys,' km/s'
      call astro_message(seve%r,rname,mess)
    endif
    write(mess,*) 'FSKY  = ',fsky/1d3,' GHz'
    call astro_message(seve%r,rname,mess)
    write(mess,*) 'FLO1  = ',lo1,' MHz'
    call astro_message(seve%r,rname,mess)
    write(mess,*) 'FLO2  = ',lo2,' MHz'
    call astro_message(seve%r,rname,mess)
    lo2 = 0                    ! to have plot in IF1
    !
    ! Plot label
    !
    write(label,1040) name(1:lenc(name)), fsky*1d-3/fshift, band,   &
    '   *** ALMA *** '
    call astro_message(seve%r,rname,label)
  endif
  !
  ! Do the plot
  !
  call plot_line(iflim, userlim, lo1, lo2, pdbi, fshift, sky, label, nmol,  &
  molname, molfreq, width)
  call plot_correlator(line, iflim, userlim, error)
  !
  return
  !
  ! Errors
  !
  930   call astro_message(seve%e,rname,'Error decoding '//par(1:lpar))
  99    error = .true.
  !
  1030  format('LINE ',a,1x,f11.6,1x,a,1x,a,1x,f6.2,1x,i3,a,i2,a,f6.1,a)
  1040  format('LINE ',a,1x,f11.6,1x,a,1x,a,1x,a)
end subroutine astro_line
!
subroutine plot_line(iflim, userlim, flo1, flo2, pdbi, fshift, sky, label,  &
  nmol, name, freq, width)
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  real(kind=4) :: iflim(2)          !
  real(kind=4) :: userlim(2)        !
  real(kind=8) :: flo1              !
  real(kind=8) :: flo2              !
  logical :: pdbi                   !
  real(kind=8) :: fshift            !
  integer(kind=4) :: sky            !
  character(len=130) :: label       !
  integer(kind=4) :: nmol           !
  character(len=20) :: name(nmol)   !
  real(kind=8) :: freq(nmol)        !
  real(kind=4) :: width             !
  ! Global
  integer(kind=4) :: lenc
  logical :: gr_error
  ! Local
  real(kind=4) :: bandwidth, fw, center
  integer(kind=4) :: i
  real(kind=8) :: f1, f2
  character(len=3) :: tmp
  character(len=132) :: chain, limi
  character(len=20) :: aname
  real(kind=4) :: afreq
  logical :: error
  !------------------------------------------------------------------------
  ! Code:
  !
  call gr_exec('CLEAR DIRECTORY')
  !
  ! IF bandwidth
  !
  bandwidth = abs(userlim(2)-userlim(1))/1000.
  center = userlim(1)+(userlim(2)-userlim(1))/2.
  write(limi,1040) userlim(1), userlim(2)
  !
  ! IF1 or IF2?
  !
  if (flo2.eq.0) then
    tmp = 'IF1'
  else
    tmp = 'IF2'
  endif
  !
  ! USB plot ----------------------------------
  !
  call gr_exec1('SET ORIEN 0')
  call gr_exec1('TICK 0 0 0 0')
  call gr_exec1('SET BOX 3 29 13 18')
  if (gr_error()) return
  !
  ! IF axis
  !
  call gr_exec1(limi)
  call gr_exec1('AXIS XL')
  chain = 'DRAW TEXT 0 -1 "Intermediate frequency '//tmp//' (MHz)" /BOX 2'
  call gr_exec1(chain)
  !
  ! Y axis (dash on user limits, then continuous on hard limits)
  !
  call gr_exec1('PEN /DASH 3')
  write (chain,1050) userlim(1)
  call gr_exec1(chain)
  write (chain,1060) userlim(1)
  call gr_exec1(chain)
  write (chain,1050) userlim(2)
  call gr_exec1(chain)
  write (chain,1060) userlim(2)
  call gr_exec1(chain)
  call gr_exec1('PEN /DASH 1')
  write (chain,1050) iflim(1)
  call gr_exec1(chain)
  write (chain,1060) iflim(1)
  call gr_exec1(chain)
  write (chain,1050) iflim(2)
  call gr_exec1(chain)
  write (chain,1060) iflim(2)
  call gr_exec1(chain)
  !
  ! RF axis
  !
  if (flo2.eq.0) then
    !
    ! Plot in IF1
    !
    f1 = (flo1+center)/1000.d0/fshift-bandwidth/2.
    f2 = f1+bandwidth
  else
    !
    ! Plot in IF2
    !
    f1 = (flo1+flo2-center)/1000.d0/fshift+bandwidth/2.
    f2 = f1-bandwidth
  endif
  !
  write(chain,1040) f1,f2
  call gr_exec1(chain)
  call gr_exec1('AXIS XU /TICK IN /LABEL P')
  call gr_exec('PEN /DASH 1')
  if (sky.eq.1) then
    call gr_exec1('DRAW TEXT 0 1 "Rest frequency (GHz)        - Signal" /BOX 8')
  else
    call gr_exec1('DRAW TEXT 0 1 "Rest frequency (GHz)        - Image" /BOX 8')
  endif
  call gr_exec1('PEN /WEIGHT 3')
  call gr_exec1('DRAW TEXT 0 1 "                        USB         " /BOX 8')
  call gr_exec1('PEN /WEIGHT 1')
  !
  ! Lines
  !
  if (nmol.ge.1) then
    fw=min(f1,f2)              ! If plot in IF1, F1<F2
    f1=max(f1,f2)              ! If plot in IF2, F1>F2
    f2=fw                      ! Need here IF2 convention
    call gr_exec1 ('SET ORIEN 50')
    aname = ' '
    afreq = 0.0
    do i=1, nmol
      if (freq(i).le.f1 .and. freq(i).ge.f2) then
        if (name(i).ne.aname .or. abs(afreq-freq(i)).gt.40e-3) then
          write(chain,1000) 'DRAW TEXT ',freq(i),' 4. "'//  &
          name(i)(1:lenc(name(i)))//'" 5 /USER'
          aname = name(i)
          afreq = freq(i)
          call gr_exec1(chain(1:lenc(chain)))
        endif
        call gr_segm('LINE',error)
        if (width.eq.0) then
          call relocate (freq(i),3.d0)
          call draw (freq(i),2.0d0)
        else
          fw = width*freq(i)/299792.458
          call relocate (freq(i)-fw/2.,2.d0)
          call draw(freq(i),3.d0)
          call draw(freq(i)+fw/2.,2.d0)
        endif
        call gr_segm_close(error)
      endif
    enddo
  endif
  !
  ! LSB plot ----------------------------------
  !
  call gr_exec1('SET ORIEN 0')
  call gr_exec1('SET BOX 3 29 3 8')
  if (gr_error()) return
  !
  ! IF axis
  !
  call gr_exec1(limi)
  ! IF (ERROR) RETURN
  call gr_exec1('AXIS XU /TICK IN /LABEL P')
  chain = 'DRAW TEXT 0 1 "Intermediate frequency '//tmp//' (MHz)" /BOX 8'
  call gr_exec1(chain)
  !
  ! Y axis (dash on user limits, then continuous on hard limits)
  !
  call gr_exec1('PEN /DASH 3')
  write (chain,1050) userlim(1)
  call gr_exec1(chain)
  write (chain,1060) userlim(1)
  call gr_exec1(chain)
  write (chain,1050) userlim(2)
  call gr_exec1(chain)
  write (chain,1060) userlim(2)
  call gr_exec1(chain)
  call gr_exec1('PEN /DASH 1')
  write (chain,1050) iflim(1)
  call gr_exec1(chain)
  write (chain,1060) iflim(1)
  call gr_exec1(chain)
  write (chain,1050) iflim(2)
  call gr_exec1(chain)
  write (chain,1060) iflim(2)
  call gr_exec1(chain)
  !
  ! RF axis
  !
  if (flo2.eq.0) then
    !
    ! Plot in IF1
    !
    f1 = (flo1-center)/1000.d0/fshift+bandwidth/2.
    f2 = f1-bandwidth
    tmp = 'IF1'
  else
    !
    ! Plot in IF2
    !
    f1 = (flo1-flo2+center)/1000.d0/fshift-bandwidth/2.
    f2 = f1+bandwidth
    tmp = 'IF2'
  endif
  !
  write(chain,1040) f1,f2
  call gr_exec1(chain)
  call gr_exec1('AXIS XL /TICK IN /LABEL P')
  if (sky.eq.-1) then
    call gr_exec1('DRAW TEXT 0 -1 "Rest frequency (GHz)        - Signal" /BOX 2')
  else
    call gr_exec1('DRAW TEXT 0 -1 "Rest frequency (GHz)        - Image" /BOX 2')
  endif
  call gr_exec1('PEN /WEIGHT 3')
  call gr_exec1('DRAW TEXT 0 -1 "                        LSB        " /BOX 2')
  call gr_exec1('PEN /WEIGHT 1')
  !
  ! Lines
  !
  if (nmol.ge.1) then
    fw=min(f1,f2)              ! If plot in IF1, F2>F2
    f2=max(f1,f2)              ! If plot in IF2, F1<F2
    f1=fw                      ! Need here IF2 convention
    call gr_exec1 ('SET ORIEN 50')
    aname = ' '
    afreq = 0.0
    do i=1, nmol
      if (freq(i).le.f2 .and. freq(i).ge.f1) then
        if (name(i).ne.aname .or. abs(afreq-freq(i)).gt.40e-3) then
          write(chain,1000) 'DRAW TEXT ',freq(i),' 4. "'//  &
          name(i)(1:lenc(name(i)))//'" 5 /USER'
          aname = name(i)
          afreq = freq(i)
          call gr_exec1(chain(1:lenc(chain)))
        endif
        call gr_segm('LINE',error)
        if (width.eq.0) then
          call relocate (freq(i),3.d0)
          call draw (freq(i),2.0d0)
        else
          fw = width*freq(i)/299792.458
          call relocate (freq(i)-fw/2.,2.d0)
          call draw(freq(i),3.d0)
          call draw(freq(i)+fw/2.,2.d0)
        endif
        call gr_segm_close(error)
      endif
    enddo
  endif
  !
  ! Label ----------------------------------
  !
  call gr_exec1 ('SET ORIEN 0')
  call gr_exec1('DRAW TEXT 0 3.1 "'//label(1:lenc(label)) //'" 5 /BOX 8')
  !
  1000 format(a,g14.7,a)
  1030 format(a,1x,f11.6,1x,a,1x,a,1x,f6.2,1x,i4,a,f6.2,a,f11.6)
  1040 format('LIMITS ',1pg25.16,1x,1pg25.16,' 0 5')
  1050 format('DRAW RELOCATE ',f10.2,' 0 /USER')
  1060 format('DRAW LINE ',f10.2,' 5 /USER')
end subroutine plot_line
!
subroutine plot_correlator(line, iflim, userlim, error)
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*):: line           !
  real(kind=4) :: iflim(2)          !
  real(kind=4) :: userlim(2)        !
  logical :: error                  !
  ! Global
  integer(kind=4) :: lenc
  logical :: gr_error
  ! Local
  character(len=256) :: berk, limi
  integer(kind=4) :: n
  !
  ! Initialization label
  !
  berk = ''
  !
  ! IF limits
  !
  write(limi,1040) userlim(1), userlim(2)
  call gr_exec1(limi)
  !
  ! USB plot
  !
  call gr_exec1('SET ORIEN 0')
  call gr_exec1('SET BOX 3 29 13 18')
  if (gr_error()) return
  call berkeley (line, iflim, berk, error)
  !
  ! LSB plot
  !
  call gr_exec1('SET BOX 3 29 3 8')
  if (gr_error()) return
  call berkeley (line, iflim, berk, error)
  !
  ! Label
  !
  n = lenc(berk)
  if (n.gt.0) then
    call gr_exec1('SET EXPAND 0.80')
    call gr_exec1('DRAW TEXT -1.5 2.2 "'//berk(1:n) //'" 6 /BOX 7')
    call gr_exec1('SET EXPAND 1.0')
  endif
  !
  1040 format('LIMITS ',1pg25.16,1x,1pg25.16,' 0 5')
end subroutine plot_correlator
!
subroutine old_berkeley (line,error,berk)
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*):: line           !
  logical :: error                  !
  character(len=*):: berk           !
  ! Global
  integer(kind=4) :: lenc,sic_narg
  ! Local
  integer(kind=4) :: nband,iband,i,j,icent,nline,llo3(2),nc
  character(len=12) :: comm
  character(len=1) :: cband
  character(len=4) :: lband(2)
  real(kind=4) :: x(7),y(7)
  character(len=13) :: rname
  ! Data
  data y/0.0,2.0,2.0,0.0,2.0,2.0,0.0/
  data rname /'LINE/SPECTRAL'/
  !
  nline = lenc(line)
  call sic_analyse (comm,line,nline,error)
  if (error) return
  nband = sic_narg(2)
  if (nband.ge.0) then
    if (nband.eq.5) then
      nband = 2
    elseif (nband.eq.3) then
      nband = 1
    else
      call astro_message(seve%e,rname,'Invalid syntax, see HELP')
      error = .true.
      return
    endif
    call sic_i4 (line,2,1,iband,.true.,error)
    if (iband.ne.10 .and. iband.ne.20 .and. iband.ne.40 .and. iband.ne.80  &
        .and. iband.ne.5) then
      call astro_message(seve%e,rname,'Invalid bandwidth ')
      error = .true.
      return
    endif
    !
    do j=1,nband
      call sic_i4 (line,2,2*j,icent,.true.,error)
      if (icent.ne.150 .and. icent.ne.250 .and. icent .ne. 350 .and.  &
          icent.ne.450 .and. icent.ne.550) then
        call astro_message(seve%e,rname,'Invalid LO3 ')
        error = .true.
        return
      endif
      call sic_ke (line,2,2*j+1,cband,nc,.true.,error)
      if (error) return
      if (iband.ne.80) then
        if (cband.eq.'U') then
          x(1) = icent+iband+0.5
          x(2) = icent+iband-0.5
          x(3) = icent+0.5
          x(4) = icent
          call gr4_connect(4,x,y,0.0,-1.0)
          lband(j) = 'USB'
        elseif (cband.eq.'L') then
          x(1) = icent-iband-0.5
          x(2) = icent-iband+0.5
          x(3) = icent-0.5
          x(4) = icent
          call gr4_connect(4,x,y,0.0,-1.0)
          lband(j) = 'LSB'
        else
          call astro_message(seve%e,rname,'Invalid sideband '//cband)
          error = .true.
          return
        endif
      elseif (cband.eq.'D') then
        x(1) = icent-40.5
        x(2) = icent-39.5
        x(3) = icent-0.5
        x(4) = icent
        x(5) = icent+0.5
        x(6) = icent+39.5
        x(7) = icent+40.5
        call gr4_connect(7,x,y,0.0,-1.0)
        lband(j) = 'DSB'
      else
        call astro_message(seve%e,rname,'Invalid sideband '//cband)
        error = .true.
        return
      endif
      llo3(j) = icent
    enddo
    if (berk.eq.' ') then
      if (nband.eq.1) then
        write(berk,1000) iband,llo3(1),lband(1)
      else
        write(berk,1000) iband,llo3(1),lband(1),llo3(2),lband(2)
      endif
    endif
  else
    do i=150,550,100
      x(1) = i-40.5
      x(2) = i-39.5
      x(3) = i-0.5
      x(4) = i
      x(5) = i+0.5
      x(6) = i+39.5
      x(7) = i+40.5
      call gr4_connect(7,x,y,0.0,-1.0)
    enddo
  endif
  error = .false.
  1000  format('SPECTRAL ',i2,2(1x,i3,1x,a))
end subroutine old_berkeley
!
subroutine berkeley (line, iflim, berk, error)
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*):: line           !
  real(kind=4) :: iflim(2)          !
  character(len=128) :: berk        !
  logical :: error                  !
  ! Global
  integer(kind=4) :: lenc
  logical :: sic_present
  ! Local
  logical :: err
  integer(kind=4) :: iband,nline,iunit,i
  character(len=12) :: comm
  real(kind=4) :: x(8),y(8),flo3,z(8), edge
  character(len=13) :: rname
  ! Data
  data z/0.0,2.0,2.0,0.0,2.5,2.0,2.0,0.0/
  data rname /'LINE/SPECTRAL'/
  !
  nline = lenc(line)
  error = .false.
  call sic_analyse (comm,line,nline,error)
  if (nline.eq.0) return
  if (error) return
  if (.not.sic_present(2,1)) return
  !
  ! Unit
  !
  call sic_i4 (line,2,1,iunit,.true.,error)
  if (error) return
  if (iunit.lt.1 .or. iunit.gt.8) then
    call astro_message(seve%w,rname,'Invalid correlator unit number ')
    error = .true.
    return
  endif
  do i=1,8
    y(i) = (1.0+0.05*(iunit-1))*z(i)
  enddo
  !
  ! Bandwidth
  !
  call sic_i4  (line,2,2,iband,.true.,error)
  if (error) return
  if (iband.ne.20 .and. iband.ne.40 .and. iband.ne.80 .and. iband.ne.160  &
      .and. iband.ne.320) then
    call astro_message(seve%e,rname,'Invalid bandwidth ')
    error = .true.
    return
  endif
  !
  ! Center LO3 frequency
  !
  call sic_r4 (line,2,3,flo3,.true.,error)
  if (error) return
  if (flo3.lt.iflim(1) .or. flo3.gt.iflim(2)) then
    call astro_message(seve%e,rname,'Invalid LO3 ')
    error = .true.
    return
  endif
  flo3 = 0.625*nint(flo3/0.625)
  call gr_segm('SPECTRAL',err)
  !
  ! Consider a 5% loss at the filter's edge
  !
  edge = iband*0.05
  if (iband.ne.20) then
    x(1) = flo3+0.5*iband
    x(2) = flo3+0.5*iband-1.0*edge
    x(3) = flo3
    x(4) = flo3
    x(5) = flo3
    x(6) = flo3
    x(7) = flo3-0.5*iband+1.0*edge
    x(8) = flo3-0.5*iband
    call gr4_connect(8,x,y,0.0,-1.0)
  else
    x(1) = flo3+0.5*iband
    x(2) = flo3+0.5*iband-1.0*edge
    x(3) = flo3-0.5*iband+1.0*edge
    x(4) = flo3-0.5*iband
    call gr4_connect(4,x,y,0.0,-1.0)
  endif
  call gr_segm_close(error)
  !
  ! Label
  !
  berk = 'SPECTRAL '
  do i = 1, 8
    nline = 15*(i-1)+10
    if (i.eq.iunit) then
      write(berk(nline:),'(I1,I4,F7.2,A)') i,iband,flo3,';'
    else
      ! WRITE(BERK(NLINE:),'(I2)') I
      ! WRITE(BERK(NLINE+13:),'(A)') ';'
    endif
  enddo
  !
  error = .false.
end subroutine berkeley
!
subroutine astro_line_alma(line,error)
  use gbl_message
  use gkernel_interfaces
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*):: line           !
  logical :: error                  !
  ! Local
  integer(kind=4) :: nmol,kmol,mmol
  integer(kind=4) :: i, ier, nc, lun
  parameter (mmol=1000)
  real(kind=8) :: f1, f2, freqs(mmol), afreq
  real(kind=4) :: new_width
  character(len=20) :: name(mmol), aname
  character(len=64) :: limi,fname
  character(len=256) :: chain
  character(len=256) :: molfile
  logical :: new
  !
  real(kind=4) :: sky, width, fw, fsky, tole
  character(len=4) :: band
  character(len=12) :: lname
  character(len=4) :: rname='LINE'
  !------------------------------------------------------------------------
  !
  width = 0.0                  ! No line width
  new = sic_present(4,0)       ! Wide band option
  ! Line name
  call sic_ch(line,0,1,fname,nc,.true.,error)
  ! Frequency
  call sic_r4(line,0,2,fsky,.true.,error)
  ! USB / LSB indicator
  call sic_ke(line,0,3,aname,nc,.true.,error)
  if (aname.eq.'USB') then
    sky = 1
  elseif  (aname.eq.'LSB') then
    sky = -1
  else
    ! je pense qu'il s'agit d'une erreur -- RL
    ! CALL GAGOUT('E-LINE, Invalid sideband '//NAME)
    call astro_message(seve%e,rname,'Invalid sideband '//aname)
    error = .true.
    return
  endif
  !
  ! Redshift
  if (fshift.eq.0) fshift = 1.d0
  !
  ! Molecule list
  kmol = sic_narg(1)
  if (kmol.gt.1) then
    kmol = kmol-1
    do i=1,kmol
      call sic_ch (line,1,i+1,name(mmol-i+1),nc,.false.,error)
      if (error) return
    enddo
  else
    kmol = 0
  endif
  !
  chain = 'GAG_MOLECULES'
  call sic_ch(line,1,1,chain,nc,.false.,error)
  molfile = chain
  call sic_parsef (chain,molfile,' ','.dat')
  !
  nmol = 0
  ier = sic_getlun(lun)
  if (ier.ne.1) then
    call astro_message(seve%e,rname,'Cannot allocate LUN')
    call putios('E-LINE,  ',ier)
    return
  endif
  open(unit=lun,file=molfile(1:lenc(molfile)), status='OLD',iostat=ier)
  if (ier.ne.0) then
    call astro_message(seve%e,rname,'Cannot open file: ')
    call astro_message(seve%e,rname,molfile)
    call putios('E-LINE,  ',ier)
    goto 9
  endif
  !
  5     read(lun,'(A)',iostat=ier,end=9) chain
  if (ier.ne.0 .or. lenc(chain).eq.0) goto 5
  if (chain(1:1).ne.'!') then
    read(chain,*,err=5) afreq, aname
    if (kmol.eq.0) then
      nmol = nmol + 1
      freqs(nmol) = afreq
      name(nmol) = aname
      if (nmol.le.mmol) goto 5
    else
      do i=mmol-kmol+1,mmol
        if (name(i).eq.aname) then
          nmol = nmol+1
          name(nmol) = aname
          freqs(nmol) = afreq
        endif
      enddo
      if (nmol.le.mmol-kmol) goto 5
    endif
  else
    goto 5
  endif
  9     close(unit=lun)
  call sic_frelun(lun)
  !
  call gr_exec('CLEAR DIRECTORY')
  tole = 0.1                   ! max(0.1,WIDTH)
  !
  ! Note: WIDTH existait deja et servait bien a quelque chose ... (RL)
  if (new) then
    !
    ! RN, Thu Nov 23, 2000... leaving it at 680 for the time being
    ! NEW_WIDTH = 1.0
    ! LIMI = 'LIMITS 100 1100 0 5'
    new_width = 8.0
    limi = 'LIMITS 4000 12000 0 5'
  else
    new_width = 4.0
    limi = 'LIMITS 4000 8000 0 5'
  endif
  !
  ! USB plot
  call gr_exec1('SET ORIEN 0')
  call gr_exec1('TICK 0 0 0 0')
  call gr_exec1('SET BOX 3 29 13 18')
  if (gr_error()) return
  call gr_exec1(limi)
  ! CALL BERKELEY (LINE,ERROR,BERK)
  ! IF (ERROR) RETURN
  call gr_exec1('AXIS XL')
  call gr_exec1('DRAW TEXT 0 -1 "IF" /BOX 2')
  !
  if (sky.eq.1) then
    f1 = fsky/fshift - new_width/2.0
  else
    f1 = fsky/fshift + new_width/2.0 + 8.0
  endif
  f2 = f1 + new_width
  write(chain,1040) f1,f2
  call gr_exec1(chain)
  call gr_exec1('AXIS YR /TICK NONE /LABEL NONE')
  call gr_exec1('AXIS YL /TICK NONE /LABEL NONE')
  call gr_exec1('AXIS XU /TICK IN /LABEL P')
  if (sky.eq.1) then
    call gr_exec1('DRAW TEXT 0 1 "USB - Signal" /BOX 8')
  else
    call gr_exec1('DRAW TEXT 0 1 "USB - Image " /BOX 8')
  endif
  if (nmol.ge.1) then
    call gr_exec1 ('SET ORIEN 50')
    aname = ' '
    afreq = 0.0
    do i=1, nmol
      if (freqs(i).le.f2 .and. freqs(i).ge.f1) then
        if (name(i).ne.aname .or. abs(afreq-freqs(i)).gt.tole) then
          write(chain,1000) 'DRAW TEXT ',freqs(i),' 4. "'//  &
          name(i)(1:lenc(name(i))+1)//'" 5 /USER'
          aname = name(i)
          afreq = freqs(i)
          call gr_exec1(chain(1:lenc(chain)))
        endif
        call gr_segm('LINE',error)
        if (width.eq.0) then
          call relocate (freqs(i),3.d0)
          call draw (freqs(i),2.0d0)
        else
          fw = width*freqs(i)/299792.458
          call relocate (freqs(i)-fw/2.,2.d0)
          call draw(freqs(i),3.d0)
          call draw(freqs(i)+fw/2.,2.d0)
        endif
        call gr_segm_close(error)
      endif
    enddo
  endif
  !
  ! LSB plot
  call gr_exec1('SET ORIEN 0')
  call gr_exec1('SET BOX 3 29 3 8')
  if (gr_error()) return
  call gr_exec1(limi)
  !      CALL BERKELEY (LINE,ERROR,BERK)
  !      IF (ERROR) RETURN
  call gr_exec1('AXIS XU /TICK IN /LABEL P')
  call gr_exec1('DRAW TEXT 0 1 "IF" /BOX 8')
  if (sky.eq.1) then
    f1 = fsky/fshift - new_width/2.0 - 8.0
  else
    f1 = fsky/fshift + new_width/2.0
  endif
  f2 = f1 - new_width
  write(chain,1040) f1,f2
  call gr_exec1(chain)
  call gr_exec1('AXIS YR /TICK NONE /LABEL NONE')
  call gr_exec1('AXIS YL /TICK NONE /LABEL NONE')
  call gr_exec1('AXIS XL /TICK IN /LABEL P')
  if (sky.eq.-1) then
    call gr_exec1('DRAW TEXT 0 -1 "LSB - Signal" /BOX 2')
  else
    call gr_exec1('DRAW TEXT 0 -1 "LSB - Image " /BOX 2')
  endif
  if (nmol.ge.1) then
    call gr_exec1 ('SET ORIEN 50')
    aname = ' '
    afreq = 0.0
    do i=1, nmol
      if (freqs(i).le.f1 .and. freqs(i).ge.f2) then
        if (name(i).ne.aname .or. abs(afreq-freqs(i)).gt.tole) then
          write(chain,1000) 'DRAW TEXT ',freqs(i),' 4. "'//  &
          name(i)(1:lenc(name(i)))//'" 5 /USER'
          aname = name(i)
          afreq = freqs(i)
          call gr_exec1(chain(1:lenc(chain)))
        endif
        call gr_segm('LINE',error)
        if (width.eq.0) then
          call relocate (freqs(i),3.d0)
          call draw (freqs(i),2.0d0)
        else
          fw = width*freqs(i)/299792.458
          call relocate (freqs(i)-fw/2.,2.d0)
          call draw(freqs(i),3.d0)
          call draw(freqs(i)+fw/2.,2.d0)
        endif
        call gr_segm_close(error)
      endif
    enddo
  endif
  call gr_exec1 ('SET ORIEN 0')
  !
  ! Label
  if (sky.eq.1) then
    band = 'USB'
  else
    band = 'LSB'
  endif
  ! IF (LCK.EQ.-1) THEN
  !   LOCK = 'LOW'
  ! ELSE
  !   LOCK = 'HIGH'
  ! ENDIF
  write(chain,1030) lname(1:lenc(lname)), fsky/fshift,band
  call astro_message(seve%r,rname,chain)
  call gr_exec1('DRAW TEXT 0 3.1 "'//chain(1:lenc(chain))//'" 5 /BOX 8')
  !
  ! SPECTRAL command
  ! N = LENC(BERK)
  ! IF (N.GT.0) THEN
  !   CALL GR_EXEC1('SET EXPAND 0.85')
  !   CALL GR_EXEC1('DRAW TEXT 0 2.2 "'//BERK(1:N)//'" 6 /BOX 7')
  !   CALL GR_EXEC1('SET EXPAND 1.0')
  ! ENDIF
  !
  1000  format(a,g14.7,a)
  1010  format(a,i3,a)
  1020  format(1x,a,f12.6)
  1030  format(a,1x,f11.6,1x,a,1x,a,1x,f6.2,1x,i4,a,f6.2,a,f11.6)
  1040  format('LIMITS ',1pg25.16,1x,1pg25.16,' 0 5')
end subroutine astro_line_alma
!
subroutine astro_line_sma(line,error)
  use gbl_message
  use gkernel_interfaces
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  ! Support for the SMA frequency setup.
  !     This is simple enough (en principle):
  !     The IF is at 5+/-1 GHz
  !     The correlator covers the 2 GHz IF with ? channels
  !---------------------------------------------------------------------
  character(len=*):: line           !
  logical :: error                  !
  ! Local
  integer(kind=4) :: nmol,kmol,mmol
  integer(kind=4) :: i, ier, nc, lun
  parameter (mmol=1000)
  real(kind=8) :: f1, f2, freqs(mmol), afreq
  real(kind=4) :: new_width
  character(len=20) :: name(mmol), aname, quant(mmol), aquant
  character(len=64) :: limi,fname
  character(len=256) :: chain
  character(len=256) :: molfile
  !
  real(kind=4) :: sky, width, fw, fsky, tole
  character(len=4) :: band
  character(len=12) :: lname
  character(len=4) :: rname
  data rname /'LINE'/
  !------------------------------------------------------------------------
  !
  width = 0.0                  ! No line width
  ! NEW = SIC_PRESENT(4,0)     ! Wide band option
  ! Line name
  call sic_ch(line,0,1,fname,nc,.true.,error)
  ! Frequency
  call sic_r4(line,0,2,fsky,.true.,error)
  ! USB / LSB indicator
  call sic_ke(line,0,3,aname,nc,.true.,error)
  if (aname.eq.'USB') then
    sky = 1
  elseif  (aname.eq.'LSB') then
    sky = -1
  else
    call astro_message(seve%e,rname,'Invalid sideband '//aname)
    error = .true.
    return
  endif
  !
  ! Redshift
  if (fshift.eq.0) fshift = 1.d0
  !
  ! Molecule list
  kmol = sic_narg(1)
  if (kmol.gt.1) then
    kmol = kmol-1
    do i=1,kmol
      call sic_ch (line,1,i+1,name(mmol-i+1),nc,.false.,error)
      if (error) return
    enddo
  else
    kmol = 0
  endif
  !
  chain = 'GAG_MOLECULES'
  call sic_ch(line,1,1,chain,nc,.false.,error)
  molfile = chain
  call sic_parsef (chain,molfile,' ','.dat')
  !
  nmol = 0
  ier = sic_getlun(lun)
  if (ier.ne.1) then
    call astro_message(seve%e,rname,'Cannot allocate LUN')
    call putios('E-LINE,  ',ier)
    return
  endif
  open(unit=lun,file=molfile(1:lenc(molfile)), status='OLD',iostat=ier)
  if (ier.ne.0) then
    call astro_message(seve%e,rname,'Cannot open file: ')
    call astro_message(seve%e,rname,molfile)
    call putios('E-LINE,  ',ier)
    goto 9
  endif
  !
  5     read(lun,'(A)',iostat=ier,end=9) chain
  if (ier.ne.0 .or. lenc(chain).eq.0) goto 5
  if (chain(1:1).ne.'!') then
    aquant = ' '
    read(chain,*,iostat=ier) afreq, aname, aquant
    if (kmol.eq.0) then
      nmol = nmol + 1
      freqs(nmol) = afreq
      name(nmol) = aname
      quant(nmol) = aquant
      if (nmol.le.mmol) goto 5
    else
      do i=mmol-kmol+1,mmol
        if (name(i).eq.aname) then
          nmol = nmol+1
          name(nmol) = aname
          freqs(nmol) = afreq
          quant(nmol) = aquant
        endif
      enddo
      if (nmol.le.mmol-kmol) goto 5
    endif
  else
    goto 5
  endif
  9     close(unit=lun)
  call sic_frelun(lun)
  !
  call gr_exec('CLEAR DIRECTORY')
  tole = 0.1                   ! max(0.1,WIDTH)
  !
  new_width = 2.0
  limi = 'LIMITS 4000 6000 0 5'
  !
  ! USB plot
  call gr_exec1('SET ORIEN 0')
  call gr_exec1('TICK 0 0 0 0')
  call gr_exec1('SET BOX 3 29 13 18')
  if (gr_error()) return
  call gr_exec1(limi)
  call gr_exec1('AXIS XL')
  call gr_exec1('DRAW TEXT 0 -1 "IF" /BOX 2')
  !
  if (sky.eq.1) then
    f1 = fsky/fshift - new_width/2.0
  else
    f1 = fsky/fshift + new_width/2.0 + 8.0
  endif
  f2 = f1 + new_width
  write(chain,1040) f1,f2
  call gr_exec1(chain)
  call gr_exec1('AXIS YR /TICK NONE /LABEL NONE')
  call gr_exec1('AXIS YL /TICK NONE /LABEL NONE')
  call gr_exec1('AXIS XU /TICK IN /LABEL P')
  if (sky.eq.1) then
    call gr_exec1('DRAW TEXT 0 1 "USB - Signal" /BOX 8')
  else
    call gr_exec1('DRAW TEXT 0 1 "USB - Image " /BOX 8')
  endif
  if (nmol.ge.1) then
    call gr_exec1 ('SET ORIEN 50')
    aname = ' '
    afreq = 0.0
    do i=1, nmol
      if (freqs(i).le.f2 .and. freqs(i).ge.f1) then
        if (name(i).ne.aname .or. abs(afreq-freqs(i)).gt.tole) then
          write(chain,1000) 'DRAW TEXT ',freqs(i),' 4. "'//  &
          name(i)(1:lenc(name(i))+1) //quant(i)(1:lenc(quant(i))+1)//'" 5 /USER'
          aname = name(i)
          afreq = freqs(i)
          call gr_exec1(chain(1:lenc(chain)))
        endif
        call gr_segm('LINE',error)
        if (width.eq.0) then
          call relocate (freqs(i),3.d0)
          call draw (freqs(i),2.0d0)
        else
          fw = width*freqs(i)/299792.458
          call relocate (freqs(i)-fw/2.,2.d0)
          call draw(freqs(i),3.d0)
          call draw(freqs(i)+fw/2.,2.d0)
        endif
        call gr_segm_close(error)
      endif
    enddo
  endif
  !
  ! LSB plot
  call gr_exec1('SET ORIEN 0')
  call gr_exec1('SET BOX 3 29 3 8')
  if (gr_error()) return
  call gr_exec1(limi)
  ! CALL BERKELEY (LINE,ERROR,BERK)
  ! IF (ERROR) RETURN
  call gr_exec1('AXIS XU /TICK IN /LABEL P')
  call gr_exec1('DRAW TEXT 0 1 "IF" /BOX 8')
  if (sky.eq.1) then
    f1 = fsky/fshift - new_width/2.0 - 8.0
  else
    f1 = fsky/fshift + new_width/2.0
  endif
  f2 = f1 - new_width
  write(chain,1040) f1,f2
  call gr_exec1(chain)
  call gr_exec1('AXIS YR /TICK NONE /LABEL NONE')
  call gr_exec1('AXIS YL /TICK NONE /LABEL NONE')
  call gr_exec1('AXIS XL /TICK IN /LABEL P')
  if (sky.eq.-1) then
    call gr_exec1('DRAW TEXT 0 -1 "LSB - Signal" /BOX 2')
  else
    call gr_exec1('DRAW TEXT 0 -1 "LSB - Image " /BOX 2')
  endif
  if (nmol.ge.1) then
    call gr_exec1 ('SET ORIEN 50')
    aname = ' '
    afreq = 0.0
    do i=1, nmol
      if (freqs(i).le.f1 .and. freqs(i).ge.f2) then
        if (name(i).ne.aname .or. abs(afreq-freqs(i)).gt.tole) then
          write(chain,1000) 'DRAW TEXT ',freqs(i),' 4. "'//  &
          name(i)(1:lenc(name(i)))//'" 5 /USER'
          aname = name(i)
          afreq = freqs(i)
          call gr_exec1(chain(1:lenc(chain)))
        endif
        call gr_segm('LINE',error)
        if (width.eq.0) then
          call relocate (freqs(i),3.d0)
          call draw (freqs(i),2.0d0)
        else
          fw = width*freqs(i)/299792.458
          call relocate (freqs(i)-fw/2.,2.d0)
          call draw(freqs(i),3.d0)
          call draw(freqs(i)+fw/2.,2.d0)
        endif
        call gr_segm_close(error)
      endif
    enddo
  endif
  call gr_exec1 ('SET ORIEN 0')
  !
  ! Label
  if (sky.eq.1) then
    band = 'USB'
  else
    band = 'LSB'
  endif
  write(chain,1030) lname(1:lenc(lname)), fsky/fshift,band
  call astro_message(seve%r,rname,chain)
  call gr_exec1('DRAW TEXT 0 3.1 "'//chain(1:lenc(chain)) //'" 5 /BOX 8')
  !
  ! SPECTRAL command
  ! N = LENC(BERK)
  ! IF (N.GT.0) THEN
  !   CALL GR_EXEC1('SET EXPAND 0.85')
  !   CALL GR_EXEC1('DRAW TEXT 0 2.2 "'//BERK(1:N)//'" 6 /BOX 7')
  !   CALL GR_EXEC1('SET EXPAND 1.0')
  ! ENDIF
  !
  1000  format(a,g14.7,a)
  1010  format(a,i3,a)
  1020  format(1x,a,f12.6)
  1030  format(a,1x,f11.6,1x,a,1x,a,1x,f6.2,1x,i4,a,f6.2,a,f11.6)
  1040  format('LIMITS ',1pg25.16,1x,1pg25.16,' 0 5')
end subroutine astro_line_sma
