subroutine pdbi_line(line,error)
  use gildas_def
  use gkernel_interfaces
  use astro_interfaces, except_this=>pdbi_line
  !---------------------------------------------------------------------
  !@ private
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: line   ! Input command line
  logical,          intent(out) :: error  ! Logical error flag
  ! Local
  character(len=commandline_length) :: comm,mess
  !
  call pdbi_line_sub(line,error)
  if (error) then
    write (mess,'(a)') 'Error executing command line'
    call gr_execl('CLEAR DIRECTORY')
    call gr_exec1('SET ORIEN 0')
    call gr_execl('CLEAR DIRECTORY')
    call gr_exec1('SET BOX 3 29 3 17')
    call gr_exec1('PENCIL /COLO 1')
    write (comm,'(a,a,a)') 'DRAW TEXT 0 0 "',trim(mess),'" /BOX 5'
    call gr_exec1(comm)
    call gr_exec1('PENCIL /COL 0')
  endif
  !
end subroutine pdbi_line
!
subroutine pdbi_line_sub(line,error)
  use gildas_def
  use gkernel_interfaces
  use astro_interfaces, except_this=>pdbi_line_sub
  use gbl_message
  use ast_line
  use ast_astro
  use phys_const
  !---------------------------------------------------------------------
  !@ private
  ! PDBI  Internal routine, support for command
  ! LINE name Freq SB [Lock [Center [Harm]]]
  !         /RECEIVER rec                                (1)
  !         /BEST best                                   (2)
  !         /FIXED_FREQ in order to allow tuning out of the LO 500MHz grid
  !                         (which is the default for obs_year.ge.2015)
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: line   ! Input command line
  logical,          intent(out) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='PDBI_LINE'
  character(len=message_length) :: mess
  character(len=256) :: chain
  character(len=12) :: name,par
  character(len=4) :: band, lock
  integer(kind=4), parameter :: nb_rec=4
  integer(kind=4) :: i,lname,lpar,ipar,ir,iargfcent
  integer(kind=4) :: mult, harm, lck, inputharm
  integer(kind=4) :: irec, userrec
  integer(kind=4) :: tabmult(nb_rec)
  integer(kind=4), parameter :: optfixed=9 !option fixed_frequency
  integer(kind=4), parameter :: optongrid=10 !option ongrid
  real(kind=8) :: fcent,fsky,best,f1,eps,vsys,fsynth,tmpnum,vlsr,lsrshift,ftune,flsr,loobs,lotune,newfcent,oldfcent
  real(kind=8) :: freqmin(nb_rec),freqmax(nb_rec),lolmin(nb_rec),lolmax(nb_rec),loumin(nb_rec),loumax(nb_rec)
  logical :: freqok,sideband_entered,fixed,nosource,ongrid
  data freqmin/74.7,128.2,200.2,275.2/  !RF extreme freq (lsb @7.8ghz if) JMW June 2016
  data freqmax/117.8,179.8,275.8,372.8/ !RF extreme freq (usb @7.8ghz if) JMW June 2016
  data lolmin/82.5,136,208,283/,lolmax/110,172,268,365/  !lo freqs for lsb tunings JMW june 2016
  data loumin/89,139,208,283/,loumax/110,172,268,365/  !lo freqs for usb tunings JMW june 2016
  data tabmult/1,2,3,3/                    !multiplier (pdb 2010)
  !
  ! Reset
  error = .false.
  narrow_def = .false.
  do i = 1,nunit
    unit_def(i) = .false.
  enddo
  !
  ! [PDBI\]LINE linename freq lsb|usb [low|high] [if] [harm]
  !
  ! Line name
  lname = 12
  call sic_ch(line,0,1,name,lname,.true.,error)
  if (error) return
  !
  ! Frequency
  if (fshift.eq.0.0d0) fshift = 1.0d0
  call sic_r8(line,0,2,freq,.true.,error)
  if (error)  return
  if (sic_varexist("ASTRO%SOURCE")) then
    call sic_get_dble('ASTRO%SOURCE%V_SOU_OBS',vsys,error)
    call sic_get_dble('ASTRO%SOURCE%V_SOU_LSR',vlsr,error)
    lsrshift = 1.0d0-vlsr/clight_kms
    nosource = .false.
  else
    vlsr = 0d0
    lsrshift = 1d0
    nosource = .true.
  endif
  fsky = freq*1d3*fshift          ! In MHz
  flsr = freq*1d3*lsrshift       ! In MHz
  !check true frequencies taken from OBS on Fsky
  irec = 0
  do ir=1,nb_rec
    if (fsky.ge.freqmin(ir)*1e3.and.fsky.le.freqmax(ir)*1e3) then
      irec=ir
    endif
  enddo
  if (irec.eq.0) then
    call astro_message(seve%e,rname,'Frequency cannot be observed with NOEMA')
    error = .true.
    return
  endif
  mult=tabmult(irec)
  !
  ! The next parameters are optional and can be entered in any
  ! order
  !
  ! Sideband defaults - USB limits FROM OBS [oct 2015]
  !
  !
  band = 'LSB '
  sky = -1
  if ((fsky.gt.103.55*1e3.and.fsky.lt.120*1e3).or.  &
       (fsky.gt.164*1e3.and.fsky.lt.200*1e3).or.    &
       (fsky.gt.267*1e3.and.fsky.lt.290*1e3)) then
     band = 'USB '
     sky = +1
     call astro_message(seve%i,rname,&
          'Default receiver tuning is USB at that frequency')
  endif
  !
  ! Frequency ofset in the LO system
  ! (changed in Sept. 2013)
  !
  if (obs_year.ge.2013) then
    eps = 100.00d0
  else
    eps = 100.25d0
  endif
  !
  ! Other defaults
  !
  ! Defaults
  harm = 0
  lock = 'LOW'
  lck = -1
  fcent = 6500                 ! Do NOT center at 6000
  oldfcent = 0
  iargfcent = 0
  !
  ! Decode the rest of the command line
  sideband_entered = .false.
  do ipar=3,sic_narg(0)
     call sic_ke(line,0,ipar,par,lpar,.true.,error)
     if (error)  return
     !
     if (par(1:1).eq.'H') then
        !
        ! HIGH
        call astro_message(seve%w,rname,  &
             'LOCK LOW|HIGH is not an input parameter anymore')
        !
     elseif (par(1:1).eq.'L') then
        !
        ! LOW
        if (lpar.ge.2.and.par(1:2).eq.'LO') then
           call astro_message(seve%w,rname, &
                'LOCK LOW|HIGH is not an input parameter anymore')
           cycle
        endif
        !
        ! LSB
        if (sideband_entered) then
           call astro_message(seve%e,rname,'Ambiguous entries: LSB or USB?')
           error = .true.
           return
        endif
        if (band(1:1).eq.'U') then
           call astro_message(seve%w,rname,'Default tuning (USB) forced to LSB')
        endif
        band = 'LSB '
        sky = -1
        sideband_entered = .true.
        !
     elseif (par(1:1).eq.'U') then
        !
        ! USB
        if (sideband_entered) then
           call astro_message(seve%e,rname,'Ambiguous entries: LSB or USB?')
           error = .true.
           return
        endif
        if (band(1:1).eq.'L') then
           call astro_message(seve%w,rname,'Default tuning (LSB) forced to USB')
        endif
        band = 'USB '
        sky = 1
        sideband_entered = .true.
        !
     else
        ! A number...
        call sic_math_dble (par,lpar,tmpnum,error)
        if (error) goto 930
        if (tmpnum.gt.4210 .and. tmpnum.lt.7790) then
           ! Fcent in MHz
           fcent = tmpnum
           iargfcent = ipar
        elseif (tmpnum.ge.32 .and. tmpnum.le.70) then
           ! Harmonic number - old system  (<2010)
           if (obs_year.lt.2010) then
              harm = tmpnum
           else
              call astro_message(seve%e,rname,'Invalid harmonic number 1')
              error = .true.
              return
           endif
        elseif (tmpnum.ge.5 .and. tmpnum.le.9) then
           ! Harmonic number - old system  (<2010)
           if (obs_year.ge.2010) then
              harm = tmpnum
           else
              call astro_message(seve%e,rname,'Invalid harmonic number 2')
              error = .true.
              return
           endif
        else
           error = .true.
           goto 930
        endif
     endif
  enddo
  !
  ! LINE /BEST bestvalue  [this option #5 of LINE command]
  !
  best = 0
  call sic_r8(line,5,1,best,.false.,error)
  if (error) return
  if (best.eq.0) best=1850
  !
  ! /RECEIVER option #8
  !
  userrec = irec
  call sic_i4(line,8,1,userrec,.false.,error)
  if (error) return
  if ((userrec.lt.1).or.(userrec.gt.4)) then
    call astro_message(seve%e,rname,'Only 4 receivers available')
    error = .true.
    return
  endif
  if (irec.ne.userrec) then
    call astro_message(seve%e,rname,'Frequency/Receiver mismatch')
    error = .true.
    return
  endif
  !
  ! /FIXED_FREQ option #optfixed - if fixed freq is given, we don't check the tuning grid
  !
  fixed = sic_present(optfixed,0)
  if (fixed.and.obs_year.lt.2015) then
    call astro_message(seve%w,rname,'Option /FIXED_FREQ ignored. Valid only with PdBI 2015')
  endif
  !
  ! /ONGRID option #optongrid
  ! With this option the system will choose the IF centrer freq so that the tuning LO is on the tuning grid
  !
  ongrid = sic_present(optongrid,0)
  if (ongrid.and.obs_year.lt.2015) then
    call astro_message(seve%w,rname,'Option /ONGRID ignored. Valid only with PdBI 2015')
  endif
  if (ongrid.and.fixed.and.obs_year.ge.2015) then
    call astro_message(seve%e,rname,'Options /FIXED_FREQ and /ONGRID are exclusive from each other')
    error = .true.
    return
  endif
  !
  ! Define harmonic number - LO system until Sept. 2010
  !
98 continue
  harm=0
  if (obs_year.lt.2010) then
     if (harm.eq.0) then
        f1 =  (fsky - sky*fcent + lck*eps*mult) / (best*mult)
        harm = int(f1)
     endif
     !
     ! Check resulting FLO1REF
     !
     flo1ref = (fsky - sky*fcent + lck*mult*eps) / (harm*mult)
     if (flo1ref.lt.1850 .or. flo1ref.gt.1900) then
        write(chain,'(A,1PG10.3)') 'Invalid LO1REF frequency',flo1ref
        call astro_message(seve%e,rname,chain)
        goto 99
     endif
     !
     ! Compute FLO1 and FLO1REF
     !
     flo1ref = (fsky - sky*fcent + lck*mult*eps) / (mult*harm)
     flo1 = mult*(harm*flo1ref - lck*eps)
     fsynth = flo1ref+0.5d0
     !
     !! Check that other formulas give same results
     !!      PRINT *,flo1,flo1ref
     !!      FLO1 = FSKY - SKY*FCENT
     !!      FLO1REF = FLO1/MULT/HARM + LCK*EPS/HARM
     !!      PRINT *,flo1,flo1ref
     !
  else
     !
     ! New LO 3G (Sept. 2010)
     !
     inputharm = harm
     freqok = .false.
     harm = 5
     if (fixed) then
       ftune = fsky
     else
       ftune = flsr
     endif
     do while ((.not.freqok).and.(harm.le.8))
        flo1ref =  (ftune - sky*fcent + lck*eps*mult) / (harm*mult)
        if ((flo1ref.gt.13200d0).and.(flo1ref.lt.16400d0)) then
           freqok = .true.
        else
           harm = harm+1
        endif
     enddo
     if (.not.freqok) then
        call astro_message(seve%e,rname,'Impossible to find harmonic number')
        error = .true.
        return
     endif
     !
     ! Give priority to harmonic number in LINE command
     !
     if ((inputharm.ne.0).and.(inputharm.ne.harm)) then
        harm = inputharm
        flo1ref = (ftune - sky*fcent + lck*eps*mult) / (harm*mult)
        if ((flo1ref.gt.13400d0).and.(flo1ref.lt.16100d0)) then
           call astro_message (seve%w,rname, &
                'A lower harmonic number may be preferable')
        else
           call astro_message (seve%e,rname,'Invalid harmonic number')
           error = .true.
           return
        endif
     endif
     !
     ! Compute FLO1 and FLO1REF
     !
     flo1ref = (ftune - sky*fcent + lck*mult*eps) / (mult*harm)
     flo1 = mult*(harm*flo1ref - lck*eps)
     fsynth = flo1ref/8d0+0.5d0
  endif
  !CHECK that FLO1 is in the right range given the USB/LSB band
  if (band.eq.'LSB'.and.(flo1.lt.lolmin(irec)*1e3.or.flo1.gt.lolmax(irec)*1e3)) then
    call astro_message(seve%e,rname,'LO frequency out of range. Tuning impossible in LSB')
    error = .true.
    return
  endif
  if (band.eq.'USB'.and.(flo1.lt.loumin(irec)*1e3.or.flo1.gt.loumax(irec)*1e3)) then
    call astro_message(seve%e,rname,'LO frequency out of range. Tuning impossible in USB')
    error = .true.
    return
  endif
  !
  ! Check that flo is on the grid provided by  FE group
  !
  if (obs_year.ge.2015) then
    if (.not.fixed) then
      call line_check_grid(line,iargfcent,flo1,fcent,sky,ftune,ongrid,newfcent,error)
      if (error) then
        if (.not.ongrid) return
        oldfcent = fcent
        fcent = newfcent
        error = .false.
        goto 98
      endif
      loobs = fsky-sky*fcent
      lotune=flo1
      flo1=loobs
    endif
  endif
  !
  ! Output message
  !
  write(mess,'(A,I2,A,A,A)') 'Setup valid for receiver ', irec,' (',trim(band),')'
  call astro_message(seve%i,rname,mess)
  if (irec.eq.4) then
    call astro_message(seve%w,rname,'----------------------------------------------------------------')
    call astro_message(seve%w,rname,'Note that Band 4 is presently NOT offered for NOEMA Observations')
    call astro_message(seve%w,rname,'----------------------------------------------------------------')
  endif
  if (nosource) then
    call astro_message(seve%i,rname,'No Source in memory, velocity 0 km/s used')
  else
    write(mess,'(a,f0.2,a)') '  Source velocity = ', vsys,' km/s'
    call astro_message(seve%r,rname,mess)
  endif
  write(mess,'(a,f10.5,a)') '  FSKY  = ',fsky/1d3,' GHz'
  call astro_message(seve%r,rname,mess)
  write(mess,'(a,i0,a)')      '  IF1 center frequency           FCENT  = ',nint(fcent),' MHz'
  call astro_message(seve%r,rname,mess)
  write(mess,'(a,f10.5,a)')   '  LO frequency for observations   FLO1  = ',flo1/1d3,' GHz'
  call astro_message(seve%r,rname,mess)
  if (obs_year.ge.2015.and.(.not.fixed)) then
    write(mess,'(a,f10.5,a)') '  LO frequency for tuning    FLO1_TUNE  = ',lotune/1d3,' GHz'
    call astro_message(seve%r,rname,mess)
  endif
  write(mess,*) ' FLO1REF  = ',flo1ref,' MHz'
  call astro_message(seve%r,rname,mess)
  write(mess,'(a,i2)') '  HARM     =    ',harm
  call astro_message(seve%r,rname,mess)
  write(mess,*) ' FSYN     = ',fsynth,' MHz'
  call astro_message(seve%r,rname,mess)
  !
  ! Define plot label
  !
  write(linecomm,1000) trim(name), freq, band,   &
  fcent, harm
  write(label,1010) trim(name), freq,  band, fcent,  &
  harm, '  /RECEIVER',irec,'   [V= ',vsys,' km/s]'
  recband = irec
  !
  ! Now do the plot
  !
  plot_mode = 1
  call pdbi_plot_def(error)
  if (error) return
  call pdbi_plot_line(error)
  if (obs_year.ge.2015.and.(.not.fixed)) then
    call pdbi_plot_grid(oldfcent,fcent,error)
  endif
  !
  ! Info re. Quarters definition
  !
  call astro_message(seve%i,rname,'Bandwidth quarters definition:')
  call astro_message(seve%r,rname,' Q1 from 4.2 GHz to 5.2 GHz')
  call astro_message(seve%r,rname,' Q2 from 5.0 GHz to 6.0 GHz')
  call astro_message(seve%r,rname,' Q3 from 6.0 GHz to 7.0 GHz')
  call astro_message(seve%r,rname,' Q4 from 6.8 GHz to 7.8 GHz')
  call astro_message(seve%i,rname,'Polarizations possibilities:')
  call astro_message(seve%r,rname,  &
    ' Correlator input 1: Q1 HOR, Q2 HOR, Q3 VER, Q4 VER')
  call astro_message(seve%r,rname,  &
    ' Correlator input 2: Q1 VER, Q2 VER, Q3 HOR, Q4 HOR')
  !
  ! End
  !
  return
  !
  ! Errors
  !
  930  call astro_message(seve%e,rname,'Error decoding '//par(1:lpar))
  99   error = .true.
  return
  !
  ! Format
  !
  1000 format('LINE ',a,1x,f10.5,1x,a,1x,f7.0,1x,i3)
  1010 format('LINE ',a,1x,f10.5,1x,a,1x,f7.0,1x,i3,a,i2,a,f6.1,a)
  !
end subroutine pdbi_line_sub
!
subroutine line_check_grid(line,iarg,flo1,fcent,sky,ftune,ongrid,fcentgrid,error)
  use gildas_def
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>line_check_grid
  !---------------------------------------------------------------------
  ! @private
  ! Check that the proposed tuning corresponds to an LO frequency
  ! on the grid provided by the FrontEnd
  ! If not, a new IF frequency is suggested
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: line  ! command line
  integer(kind=4), intent(in) :: iarg   ! position of fcent in the command line
  real(kind=8), intent(in) :: flo1      !lo1 frequency to check
  real(kind=8), intent(in) :: fcent     !IF center frequency
  integer(kind=4), intent(in) :: sky    !+1 USB, -1 LSB
  real(kind=8), intent(in) :: ftune     !corresponding sky frequency
  logical, intent(in) :: ongrid         ! option /ONGRID present or not
  real(kind=8), intent(out) :: fcentgrid ! proposed IF freq to move the tuning to the grid
  logical, intent(inout) :: error       !
  ! Local
  character(len=*), parameter :: rname='PDBI_LINE'
  real(kind=8) :: fgrid,gridbin,gridtol
  character(len=message_length) :: mess
  integer(kind=4) :: lastarg
  character(len=commandline_length) :: rline
  integer(kind=4), parameter :: optongrid=10 !option ongrid
  data gridbin/500/      ! bins in the LO tuning grid (500MHz - Nov 2015)
  data gridtol/1/        !tolerance on the LO grid : 1MHz
  !
  fgrid = nint(flo1/gridbin)*gridbin
  if (abs(fgrid-flo1).lt.gridtol) return
  fcentgrid = (ftune-fgrid)/sky
  !avoid well known if parasites and 6000MHz (limit of the 2 widex)
  if (abs(fcentgrid-4500).lt.50.or.abs(fcentgrid-6000).lt.50.or.abs(fcentgrid-6300).lt.50) then
    fcentgrid = fcentgrid+500
  endif
  !
  if (.not.ongrid) then
    write(mess,'(a,f0.3,a)') '  Tuning frequency out of the grid:       ',flo1,' MHz'
    call astro_message(seve%e,rname,mess)
    write(mess,'(a,f0.3,a)') '  Closest grid point (with optimized Trec)',fgrid,' MHz'
    call astro_message(seve%i,rname,mess)
    write(mess,'(a,i0,a)')   '  Recommended IF frequency                ',nint(fcentgrid),' MHz'
    call astro_message(seve%i,rname,mess)
    write(mess,'(a,f0.3,a)') '  Corresponding shift wrt original setup  ',fcentgrid-fcent,' MHz'
    call astro_message(seve%i,rname,mess)
  endif
  if (.not.ongrid) then
    ! Build the recommended command line
    if (iarg.le.0) then
      ! Fcent was not present, put it right after the last argument of command
      lastarg = sic_narg(0)
      write(rline,'(A,1X,I0,A)')  line(1:sic_end(0,lastarg)),  &
                                  nint(fcentgrid),  &
                                  trim(line(sic_end(0,lastarg)+1:))
    else
      ! Replace Fcent in place
      write(rline,'(A,I0,A)')  line(1:sic_start(0,iarg)-1),  &
                              nint(fcentgrid),  &
                              trim(line(sic_end(0,iarg)+1:))
    endif
    call astro_message(seve%i,rname,"  You may try the following command line:")
    call astro_message(seve%r,rname,rline)
    call astro_message(seve%i,rname,'With the option /ONGRID, the LINE command will shift the tuning automatically')
    call astro_message(seve%i,rname,'With the option /FIXED_FREQ, the tuning will be forced out of the grid')
  else
    !prepare the effective command line
    if (iarg.le.0) then
      ! Fcent was not present, put it right after the last argument of command
      lastarg = sic_narg(0)
      write(rline,'(A,1X,I0,A,A)')  line(1:sic_end(0,lastarg)),  &
                                  nint(fcentgrid),  &
                                  line(sic_end(0,lastarg)+1:sic_start(optongrid,0)-1), &
                                  trim(line(sic_end(optongrid,0)+1:))
    else
      ! Replace Fcent in place
      write(rline,'(A,I0,A,A)')  line(1:sic_start(0,iarg)-1),  &
                              nint(fcentgrid),  &
                              line(sic_end(0,iarg)+1:sic_start(optongrid,0)-1), &
                              trim(line(sic_end(optongrid,0)+1:))
    endif
!    print *,trim(rline)
    write(mess,'(a)') '  IF Frequency automatically shifted to match tuning grid'
    call astro_message(seve%i,rname,mess)
    write(mess,'(a,i0,a)') '  New IF center Frequency = ',nint(fcentgrid),' MHz'
    call astro_message(seve%i,rname,mess)
    write(mess,'(a,f0.3,a)') '  Corresponding shift wrt original setup  ',fcentgrid-fcent,' MHz'
    call astro_message(seve%i,rname,mess)
    call astro_message(seve%i,rname,"  Illustrated by the black, dashed arrow")
    call astro_message(seve%i,rname,"  Equivalent command line:")
    call astro_message(seve%r,rname,rline)
  endif
  error = .true.
  !
end subroutine line_check_grid
!
subroutine pdbi_plot_grid(oldfcent,fcent,error)
  use gbl_message
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  ! Plot IF frequencies matching the tuning grid (obs_year>=2015) 
  ! and the original Fcent to visualize to change in /ONGRID MODE
  !---------------------------------------------------------------------
  real(kind=8), intent(in) :: oldfcent  ! original IF center frequency in the LINE command
  real(kind=8), intent(in) :: fcent     ! actually used IF center frequency in the LINE command (/ONGRID option)
  logical, intent(inout) :: error       !
  integer(kind=4) :: i,nposs
  real(kind=8)  :: fposs
  character(len=*), parameter :: rname='PDBI_PLOT'
  character(len=132) :: chain,flist
  call gr_exec1('SET BOX_LOCATION 3 29 9 17')
  ! show when fcent was changed (/ONGRID OPTION)
  if (fcent.ne.oldfcent.and.oldfcent.gt.4000.and.oldfcent.lt.8000) then
    call astro_message(seve%i,rname,"Plotting the original IF center frequency (dashed black line)")
    call gr_exec1('PEN /COL 0 /DASH 2')
    write(chain,1010) oldfcent,-0.2
    call gr_exec1(chain)
    write(chain,1020) oldfcent,1.5
    call gr_exec1(chain)
    write(chain,1010) oldfcent,-0.2
    call gr_exec1(chain)
    write(chain,1030) fcent,-.2
    call gr_exec1(chain)
    call gr_exec1('PEN /COL 0 /DASH 1')
  endif
  !
  ! show other possible FCent
  flist = ''
  nposs = 0
  do i = 1,17
    fposs = fcent+(i-9)*500
    call gr_exec1('PEN /COL 6 /DASH 2')
    if (fposs.gt.4250.and.fposs.lt.7750) then
      if (abs(fposs-6300).gt.50.and.abs(fposs-4500).gt.50.and.abs(fposs-6000).gt.50) then
        nposs = nposs+1
        write(chain,1010) fposs,0.0
        call gr_exec1(chain)
        write(chain,1020) fposs,1.0
        call gr_exec1(chain)
        write(flist,'(a,1x,i0)') trim(flist),nint(fposs)
      endif
    endif
    call gr_exec1('PEN /COL 0 /DASH 1')
  enddo
  if (nposs.ge.1) then
    write(flist,'(a,a)') trim(flist),' MHz'
    call astro_message(seve%i,rname,"IF frequencies respecting the tuning grid (dashed pink lines):")
    call astro_message(seve%i,rname,flist)
  endif
  !
  1010 format('DRAW RELOCATE ',f15.3,1x,f15.3,' /USER')
  1020 format('DRAW LINE ',f15.3,1x,f15.3' /USER')
  1030 format('DRAW ARROW ',f15.3,1x,f15.3' /USER')
  1040 format(f10.2,1x,'MHz')
  !
end subroutine pdbi_plot_grid
