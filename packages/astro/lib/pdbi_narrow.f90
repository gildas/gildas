subroutine pdbi_narrow(line,error)
  use gkernel_interfaces
  use gbl_message
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  ! PDBI internal routine, support for command
  !	NARROW_INPUT 1|2 Q1|Q2|Q3|Q4 [HOR|VER]
  !---------------------------------------------------------------------
  character(len=*):: line           !
  logical :: error                  !
  ! Local
  integer(kind=4) :: i,n,nkey,nc
  character(len=2) :: qq
  character(len=6) :: entry(2)
  character(len=6) :: input1(4), input2(4)
  data input1/'Q1 HOR','Q2 HOR','Q3 VER','Q4 VER'/
  data input2/'Q1 VER','Q2 VER','Q3 HOR','Q4 HOR'/
  character(len=11) :: rname
  data rname /'PDBI_NARROW'/
  ! ---------------------------------------------------------------------------
  !
  ! LINE command must have been run (find a better test?)
  !
  if (flo1.eq.0) then
     call astro_message(seve%e,rname,'LINE command not yet executed')
     error = .true.
     return
  endif
  !
  narrow_def = .false.
  do i=1,nunit
    unit_def(i) = .false.
  enddo
  !
  n = sic_narg(0)
  if (n.ne.2) then
    call astro_message(seve%e,rname,'Command needs two arguments')
    error = .true.
    return
  endif
  !
  do i = 1,2
    call sic_ch(line,0,i,qq,nc,.false.,error)
    if (error) return
    call sic_upper(qq)
    if (i.eq.1) then
      call sic_ambigs( 'PDBI_NARROW',qq,entry(1),nkey,input1,4,error)
    elseif (i.eq.2) then
      call sic_ambigs( 'PDBI_NARROW',qq,entry(2),nkey,input2,4,error)
    endif
    if (error) then
      call astro_message(seve%e,rname,'Wrong syntax')
      return
    endif
    narrow_input(i) = nkey
  enddo
  !
  ! Now do the plot
  !
  narrow_def = .true.
  plot_mode = 2
  call pdbi_plot_def(error)
  if (error) return
  call pdbi_plot_line(error)
  !
end subroutine pdbi_narrow
!
subroutine pdbi_widex(line,error)
  use gbl_message
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  ! PDBI internal routine, support for command WIDEX
  ! (only producing a plot)
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  !
  plot_mode = 3
  call pdbi_plot_def(error)
  if (error) return
  call pdbi_plot_line(error)
  !
end subroutine pdbi_widex
