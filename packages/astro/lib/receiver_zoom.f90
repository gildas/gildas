subroutine rec_plot_sidebands(rdesc,rsou,rtune,cplot,cata,drawaxis,error)
  use gbl_message
  use gildas_def
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_plot_sidebands
  use astro_types
  use ast_line
  !-----------------------------------------------------------------------
  ! @ private
  ! displays frequency coverage of heterodyne receivers (full width for sidebands)
  !-----------------------------------------------------------------------
  type(receiver_desc_t), intent(in)          :: rdesc ! receiver description parameters
  type(receiver_source_t), intent(in)          :: rsou ! receiver source parameters
  type(receiver_tune_t), intent(in)          :: rtune ! receiver tuning parameters
  type(plot_molecules_t), intent(in)         :: cata
  type(current_boxes_t), intent(inout)  :: cplot    ! plot page parameters
  type(frequency_axis_t), intent(in) :: drawaxis
  logical, intent(inout)         :: error
  ! local
  character(len=*), parameter   :: rname='PLOT'
  character(len=200)            :: comm,defchar,molchar,mess,smallchar
  integer(kind=4)               :: i,j,is(m_sideband)
  integer(kind=4)               :: ibox,ib1,ibcap,boxid(m_boxes)
  real(kind=8)                  :: boxymax(m_boxes),cappos
  type(draw_rect_t) :: sbrect
  type(draw_interbox_line_t)    :: zoomline
  real(kind=8)                  :: px,py,frf,xt,yt
  real(kind=4)                  :: yzoom
  !
  ! get page info
  call sic_get_dble('page_x',px,error)
  if (error) return
  call sic_get_dble('page_y',py,error)
  if (error) return
  if (px.lt.29) then
    call astro_message(seve%w,rname,'PLOT is optimized for LANDSCAPE orientation of the display')
  endif
  !
  cplot%desc%defchar = 0.4
  write (defchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%defchar
  cplot%desc%molchar = 0.3
  write (molchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%molchar
  if (px.gt.25) then
    cplot%desc%smallchar = 0.3
  else if (px.le.25) then
    cplot%desc%smallchar = 0.2
  else
    call astro_message(seve%e,rname,'Problem with page size')
    error = .true.
    return
  endif
  write (smallchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%smallchar
  !
  cplot%nbox = rdesc%n_sbands+1 ! USB,LSB + overview
  do j=1,cplot%nbox
    call rec_reset_box(cplot%box(j),error)
    cplot%box(j)%phys%sy = 4d0 ! fixed
    cplot%box(j)%phys%xmin = 3.d0
    cplot%box(j)%phys%sx = px-4.5d0 ! depends on plot orientation
    cplot%box(j)%phys%xmax = cplot%box(j)%phys%xmin+cplot%box(j)%phys%sx
  enddo
  ! Clear the current window, and ONLY the current window
  call gr_execl('CHANGE DIRECTORY') ! No arg = go to the top directory
  call gr_execl('CLEAR DIRECTORY') ! No arg = clear the whole window
  call gr_exec1('TICKSPACE 0 0 0 0')
  call gr_pen(colour=adefcol,error=error)
  if (error) return
  call gr_exec1(defchar)
  !
  ! sidebands defaults and plot organisation
  if (rdesc%n_sbands.eq.2) then
    cplot%box(1)%sb_code=usb_code !usb in upper frame
    cplot%box(3)%sb_code=lsb_code
    is(1)=usb_code
    is(2)=lsb_code
    boxid(2)=1 ! USB UP
    boxymax(2)=py-3d0
    !
    boxid(3)=3 ! LSB LOW
    boxymax(3)=py-3d0-2*(cplot%box(1)%phys%sy+2.d0)
    !
    boxid(1)=2 ! GLOBAL [id=1] at CENTER [Pos=2]
    boxymax(1)=py-3d0-cplot%box(1)%phys%sy-2.d0
    !
  else if (rdesc%n_sbands.eq.1) then
    cplot%box(2)%sb_code=rtune%sb_code !only 1 sideband
    is(1)=rtune%sb_code
  else
    call astro_message(seve%e,rname,'We should have 1 or 2 sidebands')
    error = .true.
    return
  endif
  !
  cappos=-1
  do i=1,cplot%nbox
    if (boxymax(i).le.cappos) cycle
    ibcap=boxid(i)
    cappos=boxymax(i)
  enddo
  !
  ! global receiver band view
  ! physical box
  ibox=1
  ib1=boxid(ibox)
  write (comm,'(a,i0)') 'CREATE DIRECTORY BOX',ib1
  call gr_execl(comm)
  write (comm,'(a,i0)') 'CHANGE DIRECTORY BOX',ib1
  call gr_execl(comm)
  cplot%box(ib1)%iband=rtune%iband
  yt = cplot%box(ib1)%phys%ymax
  cplot%box(ib1)%phys%ymax=boxymax(ibox)
  cplot%box(ib1)%phys%ymin=cplot%box(ib1)%phys%ymax-cplot%box(ib1)%phys%sy
  ! user box
  ! Define the freq box (whole band)
  call rec_def_fbox(rdesc%restlim(1,rtune%iband), &
                    rdesc%restlim(2,rtune%iband), &
                    'REST',cplot%box(ib1),rdesc,rsou,rtune%flo1,error)
  if (error) return
  call rec_locate_fbox(cplot%box(ib1)%phys,error)
  if (error) return
  sbrect%xmin = cplot%box(ib1)%rest%xmin
  sbrect%xmax = cplot%box(ib1)%rest%xmax
  sbrect%ymin = cplot%box(ib1)%rest%ymin
  sbrect%ymax = cplot%box(ib1)%rest%ymax
  sbrect%col = aavailcol
  call rec_draw_frect(sbrect,cplot%box(ib1)%rest,error)
  if (error) return
  !
  ! displays USB and LSB over the whole band
  do j=1,rdesc%n_sbands
    call if1torf(rtune%flo1,rdesc%iflim(2),is(j),frf,error)
    if (error) return
    call rftorest(rsou%dopshift,frf,sbrect%xmax,error)
    if (error) return
    call if1torf(rtune%flo1,rdesc%iflim(1),is(j),frf,error)
    if (error) return
    call rftorest(rsou%dopshift,frf,sbrect%xmin,error)
    if (error) return
    sbrect%ymin = cplot%box(ib1)%rest%ymin
    sbrect%ymax = cplot%box(ib1)%rest%ymax
    sbrect%col = atunedcol
    call rec_draw_frect(sbrect,cplot%box(ib1)%rest,error)
    if (error) return
    call gr_exec1(smallchar)
    write (comm,'(a,1x,f0.3,1x,f0.3,1x,a,1x,a)') 'DRAW TEXT', &
                sbrect%xmax-(sbrect%xmax-sbrect%xmin)/20, &
                cplot%box(ib1)%rest%ymin+(cplot%box(ib1)%rest%ymax-cplot%box(ib1)%rest%ymin)/4d0, &
                sideband(is(j)),' 5 90 /USER' 
    call gr_exec1(comm)
    call gr_exec1(defchar)
  enddo
  !ID areas outside call for proposal
  sbrect%xmin = rdesc%restlim(1,rtune%iband)
  sbrect%xmax = rdesc%restcall(1,rtune%iband)
  sbrect%ymin = cplot%box(ib1)%rest%ymin
  sbrect%ymax = cplot%box(ib1)%rest%ymax
  sbrect%col = adefcol
  call rec_draw_hrect(sbrect,cplot%box(ib1)%rest,error)
  if (error) return
  sbrect%xmax = rdesc%restlim(2,rtune%iband)
  sbrect%xmin = rdesc%restcall(2,rtune%iband)
  sbrect%ymin = cplot%box(ib1)%rest%ymin
  sbrect%ymax = cplot%box(ib1)%rest%ymax
  sbrect%col = adefcol
  call rec_draw_hrect(sbrect,cplot%box(ib1)%rest,error)
  if (error) return
  call rec_draw_fbox(cplot,ib1,drawaxis,error)
  if (error) return
  if (rsou%z.ne.0) then
    call gr_pen(colour=azcol,error=error)
    if (error) return
  else
    call gr_pen(colour=adefcol,error=error)
    if (error) return
  endif
  write (comm,'(a,1x,a,1x,a)') 'DRAW TEXT -1.5 0', &
        trim(rdesc%bandname(rtune%iband)),'5 90 /CHARACTER 4'
  call gr_exec1(comm)
  call gr_pen(colour=adefcol,error=error)
  if (error) return
  call gr_exec1(molchar)
  call rec_draw_molecules(cata,cplot%box(ib1)%rest,error)
  if (error) return
  call gr_exec1(defchar)
  !
  !ZOOM on LSB and USB
  ! loop on the sidebands to zoom
  call gr_execl('CHANGE DIRECTORY') ! No arg = go to the top directory
  do i=2,cplot%nbox ! start at 2 because 1 is global view
    ibox=boxid(i)
    cplot%box(ibox)%iband=rtune%iband
    cplot%box(ibox)%phys%ymax = boxymax(i)
    cplot%box(ibox)%phys%ymin = cplot%box(ibox)%phys%ymax-cplot%box(ibox)%phys%sy
    if (cplot%box(ibox)%phys%ymax.gt.yt) then
      yt = cplot%box(ibox)%phys%ymax
    endif
    write (comm,'(a,i0)') 'CREATE DIRECTORY BOX',ibox
    call gr_execl(comm)
    write (comm,'(a,i0)') 'CHANGE DIRECTORY BOX',ibox
    call gr_execl(comm)
!     call rec_def_fbox(cplot%box(i)%sb_code,rdesc%iflim(1),rdesc%iflim(2),'IF1', &
!                       cplot%box(i),rsou%dopshift,rtune,error)
    call rec_def_fbox(rdesc%iflim(1),rdesc%iflim(2),'IF1',cplot%box(ibox),rdesc,rsou, &
                      rtune%flo1,error)
    if (error) return
    cplot%box(ibox)%iband = rtune%iband
    ! Now locate the box
    call rec_locate_fbox(cplot%box(ibox)%phys,error)
    if (error) return
    !
    if (i.eq.2) then
      call rec_draw_source(rname,rsou,error)
      if (error) return
    endif
    !Display tuning frequency
    if (cplot%box(ibox)%sb_code.eq.rtune%sb_code) then
      call rec_draw_linetune(rtune,cplot%box(ibox)%rest,error)
      if (error) return
    endif
    ! Draw molecules
    call gr_exec1(molchar)
    call rec_draw_molecules(cata,cplot%box(ibox)%rest,error)
    if (error) return
    call gr_exec1(defchar)
    write (comm,'(a,1x,f0.3,1x,f0.3,1x,f0.3,1x,f0.3)') &
          'LIMITS',cplot%box(ibox)%rest%xmin,cplot%box(ibox)%rest%xmax,cplot%box(ibox)%rest%ymin,cplot%box(ibox)%rest%ymax
    call gr_exec1(comm)
    ! ID the sideband on the global view
    zoomline%col = sb_col(cplot%box(ibox)%sb_code)
    do j=1,2 !1=xmin, 2=xmax
      zoomline%ibox(1)=ibox
      zoomline%ibox(2)=ib1
      if (j.eq.1) then
        zoomline%xpos(1) = cplot%box(ibox)%rest%xmin
      else if (j.eq.2) then
        zoomline%xpos(1) = cplot%box(ibox)%rest%xmax
      endif
      zoomline%xpos(2) = zoomline%xpos(1)
      zoomline%frame(1) = 'REST'
      zoomline%frame(2) = 'REST'
      if (cplot%box(ibox)%phys%ymin.lt.cplot%box(ib1)%phys%ymin) then
        zoomline%ypos(1) = cplot%box(ibox)%rest%ymax
        zoomline%ypos(2) = cplot%box(ib1)%rest%ymin
        yzoom=cplot%box(1)%rest%ymax
      else if (cplot%box(ibox)%phys%ymin.gt.cplot%box(ib1)%phys%ymin) then
        zoomline%ypos(1) = cplot%box(ibox)%rest%ymin
        zoomline%ypos(2) = cplot%box(ib1)%rest%ymax
        yzoom=cplot%box(1)%rest%ymin
      else
        call astro_message(seve%e,rname,'Problem with box positions')
        error = .true.
        return
      endif
      call rec_draw_interbox_line(rname,zoomline,cplot,error)
      if (error) return
      zoomline%ypos(1) = yzoom
      zoomline%ibox(1) = zoomline%ibox(2)
      call rec_draw_interbox_line(rname,zoomline,cplot,error)
      if (error) return
      write (comm,'(a,i0)') 'CHANGE DIRECTORY BOX',ibox
      call gr_execl(comm)
    enddo
    !
    ! Draw the box
    call rec_draw_fbox(cplot,ibox,drawaxis,error)
    if (error) return
    !
    call gr_execl('CHANGE DIRECTORY')
    call gr_pen(colour=adefcol,error=error)
    if (error) return
  enddo
  !
  ! Caption
  write (comm,'(a,i0)') 'CHANGE DIRECTORY BOX',ibcap
  call gr_execl(comm)
  yt = yt-cplot%box(ibcap)%phys%ymin+2
  call gr_exec1(defchar)
  xt = (cplot%box(ibcap)%phys%xmin+cplot%box(ibcap)%phys%xmax)/2d0-cplot%box(ibcap)%phys%xmin
  if (rtune%outlo) then
    call gr_pen(colour=aconflictcol,error=error)
    if (error) return
    write (comm,'(a,1x,f0.3,1x,f0.3,1x,a,f0.3,a)') &
      'DRAW TEXT',xt,yt,'"LO (',rtune%flo1*ghzpermhz,'GHz) out of recommended range" 5 0'
    call gr_exec1(comm)
  else
    call gr_pen(colour=adefcol,error=error)
    if (error) return
    write (comm,'(a,1x,f0.3,1x,f0.3,1x,a,1x,f0.3,1x,a)') &
      'DRAW TEXT',xt,yt,'"LO:',rtune%flo1*ghzpermhz,'GHz" 5 0'
    call gr_exec1(comm)
  endif
  call rec_make_title(rtune,mess,error)
  if (error) return
  call rec_draw_title(cplot,mess,error)
  if (error) return
  call gr_pen(colour=adefcol,error=error)
  if (error) return
  call gr_exec1(defchar)
  call gr_execl('CHANGE DIRECTORY')
  cplot%desc%plotmode = pm_receiver
  !
end subroutine rec_plot_sidebands
!
!
subroutine rec_def_scale(rdesc,fixed,fmin,fmax,error)
  use astro_interfaces, except_this=>rec_def_scale
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! determine the limits of the bands to plot - 
  ! fixed option means same scale in all boxes
  !-----------------------------------------------------------------------
  type(receiver_desc_t), intent(in) :: rdesc ! receiver description
  logical, intent(in)   :: fixed ! to plot all bands with the same scale
  real(kind=8), intent(out) :: fmin(rdesc%n_rbands),fmax(rdesc%n_rbands)
  logical, intent(inout) :: error
  ! Local
  integer(kind=4) :: i
  real(kind=8) :: bwidth(rdesc%n_rbands),bmax,fcent(rdesc%n_rbands)
  !compute band width
  if (.not.fixed) then
    do i=1,rdesc%n_rbands
      fmin(i) = rdesc%restlim(1,i)
      fmax(i) = rdesc%restlim(2,i)
    enddo
  else
    do i=1,rdesc%n_rbands
      bwidth(i) = rdesc%restlim(2,i)-rdesc%restlim(1,i)
      fcent(i) = (rdesc%restlim(2,i)+rdesc%restlim(1,i))/2d0
    enddo
    bmax = maxval(bwidth)
    do i=1,rdesc%n_rbands
      fmin(i) = fcent(i)-bmax/2d0
      fmax(i) = fcent(i)+bmax/2d0
    enddo
  endif
  !
end subroutine rec_def_scale
!
subroutine rec_plot_tuning(rdesc,rsou,rtune,cata,cplot,drawaxis,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_plot_tuning
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Zoom on the tuned area of a Band (Fmin=LSB min, Fmax=USB max)
  !-----------------------------------------------------------------------
  type(receiver_desc_t), intent(inout) :: rdesc ! receiver description and tuning
  type(receiver_source_t), intent(inout) :: rsou ! receiver description and tuning
  type(receiver_tune_t), intent(inout) :: rtune ! receiver description and tuning
  type(plot_molecules_t), intent(in)   :: cata     !plot known lines from current catalog
  type(current_boxes_t), intent(inout) :: cplot
  type(frequency_axis_t), intent(in) :: drawaxis
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter      :: rname='PLOT'
  integer(kind=4)       :: ibox,j,iband,sb_code(m_sideband)
  real(kind=8)          :: px,py,yoff,boxsize,frf,xt,yt
  real(kind=8)          :: fmin,fmax,frfmin,frfmax
  character(len=200)    :: comm,defchar,molchar,mess,mess2
  type(draw_rect_t)     :: sbrect
  !
  ! Clear the current window, and ONLY the current window
  call gr_execl('CHANGE DIRECTORY') ! No arg = go to the top directory
  call gr_execl('CLEAR DIRECTORY') ! No arg = clear the whole window
  call gr_exec1('TICKSPACE 0 0 0 0')
  call gr_pen(colour=adefcol,error=error)
  if (error) return
  ! get page info
  call sic_get_dble('page_x',px,error)
  if (error) return
  call sic_get_dble('page_y',py,error)
  if (error) return
  if (px.lt.29) then
    call astro_message(seve%w,rname,'PLOT is optimized for LANDSCAPE orientation of the display')
  endif
  ! set plot parameters
  cplot%nbox = 1
  do j=1,cplot%nbox
    call rec_reset_box(cplot%box(j),error)
  enddo
  cplot%desc%defchar = 0.4
  write (defchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%defchar
  cplot%desc%molchar = 0.2
  write (molchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%molchar
  if (px.gt.25) then
    cplot%desc%smallchar = 0.3
  else if (px.le.25) then
    cplot%desc%smallchar = 0.2
  else
    call astro_message(seve%e,rname,'Problem with page size')
    error = .true.
    return
  endif
  cplot%desc%hugechar = 1
  call gr_exec1(defchar)
  yoff = 4d0
  boxsize = 4.
  !Go for the plot
  iband = rtune%iband
  ibox=1
  !Prepare the limits of the bands in REST
  frfmin=rtune%flo1-rdesc%iflim(2)
  frfmax=rtune%flo1+rdesc%iflim(2)
  call rftorest(rsou%dopshift,frfmin,fmin,error)
  if (error) return
  call rftorest(rsou%dopshift,frfmax,fmax,error)
  if (error) return
  !
  ! Physical description of the box
  cplot%box(ibox)%phys%sx = px-4.5d0
  cplot%box(ibox)%phys%sy = boxsize
  cplot%box(ibox)%phys%xmin = 3d0
  cplot%box(ibox)%phys%xmax = cplot%box(ibox)%phys%xmin+cplot%box(ibox)%phys%sx
  cplot%box(ibox)%phys%ymax = py-yoff-(ibox-1)*(boxsize+yoff)
  cplot%box(ibox)%phys%ymin = cplot%box(ibox)%phys%ymax-boxsize
  write (comm,'(a,i0)') 'CREATE DIRECTORY BOX',ibox
  call gr_execl(comm)
  write (comm,'(a,i0)') 'CHANGE DIRECTORY BOX',ibox
  call gr_execl(comm)
  cplot%box(ibox)%iband = iband
  !define the box limits in all freq frames
  call rec_def_fbox(fmin,fmax,'REST',cplot%box(ibox),rdesc,rsou,rtune%flo1,error)
  if (error) return
  !locate the box
  call rec_locate_fbox(cplot%box(ibox)%phys,error)
  if (error) return
  if (ibox.eq.1) then
    call rec_draw_source(rname,rsou,error)
    if (error) return
  endif
  sbrect%xmin = cplot%box(ibox)%rest%xmin
  sbrect%xmax = cplot%box(ibox)%rest%xmax
  sbrect%ymin = cplot%box(ibox)%rest%ymin
  sbrect%ymax = cplot%box(ibox)%rest%ymax
  sbrect%col = aavailcol
  call rec_draw_frect(sbrect,cplot%box(ibox)%rest,error)
  if (error) return
  !ID sidebands
  if (rdesc%n_sbands.eq.2) then
    sb_code(1) = usb_code !usb first
    sb_code(2) = lsb_code !lsb
  else if (rdesc%n_sbands.eq.1) then
    sb_code(1) = rtune%sb_code ! tuned side band for single sideband
  else
    call astro_message(seve%e,rname,'We should have 1 or 2 sidebands')
    error = .true.
    return
  endif
  do j=1,rdesc%n_sbands
    call if1torf(rtune%flo1,rdesc%iflim(2),sb_code(j),frf,error)
    if (error) return
    call rftorest(rsou%dopshift,frf,sbrect%xmax,error)
    if (error) return
    call if1torf(rtune%flo1,rdesc%iflim(1),sb_code(j),frf,error)
    if (error) return
    call rftorest(rsou%dopshift,frf,sbrect%xmin,error)
    if (error) return
    sbrect%ymin = cplot%box(ibox)%rest%ymin
    sbrect%ymax = cplot%box(ibox)%rest%ymax
    sbrect%col = atunedcol
    call rec_draw_frect(sbrect,cplot%box(ibox)%rest,error)
    if (error) return
  enddo
  !Draw the box
  call rec_draw_fbox(cplot,ibox,drawaxis,error)
  if (error) return
!   ! show tuning line
!   call rec_draw_linetune(rtune,cplot%box(ibox)%rest,error)
!   if (error) return
  if (rsou%z.ne.0) then
    call gr_pen(colour=azcol,error=error)
    if (error) return
  else
    call gr_pen(colour=adefcol,error=error)
    if (error) return
  endif
  write (comm,'(a,1x,a,1x,a)') 'DRAW TEXT -1.5 0 "',trim(rdesc%bandname(iband)),'" 5 90 /CHARACTER 4'
  call gr_exec1(comm)
  ! Draw tuning name
  xt=0d0
  yt=2d0
  call rec_make_title(rtune,mess,error)
  if (error) return
  if (rtune%outlo) then
    call gr_pen(colour=aconflictcol,error=error)
    if (error) return
    write (mess2,'(a,1x,a)') trim(mess),'(out of LO range)'
    call gr_exec1(comm)
  else
    mess2=trim(mess)
    call gr_pen(colour=altunecol,error=error)
    if (error) return
  endif
  call rec_draw_title(cplot,mess2,error)
  if (error) return
  call gr_pen(colour=adefcol,error=error)
  if (error) return
  call gr_execl('CHANGE DIRECTORY')
  cplot%desc%plotmode = pm_proposal
  !
end subroutine rec_plot_tuning
!
subroutine rec_plot_nbands(rec,cata,fixed,cplot,drawaxis,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_plot_nbands
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Displays the frequency coverage of a multi band receiver
  ! Only tuned bands are displayed
  !-----------------------------------------------------------------------
  type(receiver_t), intent(inout) :: rec ! receiver description and tuning
  type(plot_molecules_t), intent(in)   :: cata     !plot known lines from current catalog
  logical, intent(in)   :: fixed ! to plot all bands with the same scale
  type(current_boxes_t), intent(inout) :: cplot
  type(frequency_axis_t), intent(in) :: drawaxis
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter      :: rname='PLOT'
  integer(kind=4)       :: i,j,iband,sb_code(m_sideband)
  real(kind=8)          :: px,py,yoff,boxsize,frf,xt,yt
  real(kind=8) ::fmin(rec%desc%n_rbands),fmax(rec%desc%n_rbands)
  character(len=200)    :: comm,defchar,molchar,mess,mess2
  type(draw_rect_t)     :: sbrect
  !
  !Prepare the limits of the bands
  call rec_def_scale(rec%desc,fixed,fmin,fmax,error)
  if (error) return
  !
  ! Clear the current window, and ONLY the current window
  call gr_execl('CHANGE DIRECTORY') ! No arg = go to the top directory
  call gr_execl('CLEAR DIRECTORY') ! No arg = clear the whole window
  call gr_exec1('TICKSPACE 0 0 0 0')
  call gr_pen(colour=adefcol,error=error)
  if (error) return
  ! get page info
  call sic_get_dble('page_x',px,error)
  if (error) return
  call sic_get_dble('page_y',py,error)
  if (error) return
  if (px.lt.29) then
    call astro_message(seve%w,rname,'PLOT is optimized for LANDSCAPE orientation of the display')
  endif
  ! set plot parameters
  cplot%nbox = rec%n_tunings
  do i=1,cplot%nbox
    call rec_reset_box(cplot%box(i),error)
  enddo
  cplot%desc%defchar = 0.4
  write (defchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%defchar
  cplot%desc%molchar = 0.2
  write (molchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%molchar
  if (px.gt.25) then
    cplot%desc%smallchar = 0.3
  else if (px.le.25) then
    cplot%desc%smallchar = 0.2
  else
    call astro_message(seve%e,rname,'Problem with page size')
    error = .true.
    return
  endif
  call gr_exec1(defchar)
  yoff = 2d0
  boxsize = min((py-3.d0)/cplot%nbox-yoff,4.)
  !Loop on tunings
  do i=1,cplot%nbox
    iband = rec%tune(i)%iband
    ! Physical description of the box
    cplot%box(i)%phys%sx = px-4.5d0
    cplot%box(i)%phys%sy = boxsize
    cplot%box(i)%phys%xmin = 3d0
    cplot%box(i)%phys%xmax = cplot%box(i)%phys%xmin+cplot%box(i)%phys%sx
    cplot%box(i)%phys%ymax = py-yoff-(i-1)*(boxsize+yoff)
    cplot%box(i)%phys%ymin = cplot%box(i)%phys%ymax-boxsize
    write (comm,'(a,i0)') 'CREATE DIRECTORY BOX',i
    call gr_execl(comm)
    write (comm,'(a,i0)') 'CHANGE DIRECTORY BOX',i
    call gr_execl(comm)
    cplot%box(i)%iband = iband
    !define the box limits in all freq frames
    call rec_def_fbox(fmin(iband),fmax(iband),'REST',cplot%box(i),rec%desc,rec%source,rec%tune(i)%flo1,error)
    if (error) return
    !locate the box
    call rec_locate_fbox(cplot%box(i)%phys,error)
    if (error) return
    if (i.eq.1) then
      call rec_draw_source(rname,rec%source,error)
      if (error) return
    endif
    sbrect%xmin = cplot%box(i)%rest%xmin
    sbrect%xmax = cplot%box(i)%rest%xmax
    sbrect%ymin = cplot%box(i)%rest%ymin
    sbrect%ymax = cplot%box(i)%rest%ymax
    sbrect%col = aavailcol
    call rec_draw_frect(sbrect,cplot%box(i)%rest,error)
    if (error) return
    !ID areas outside receiver range
    if  (cplot%box(i)%rest%xmin.lt.rec%desc%restlim(1,iband)) then
      sbrect%xmin = cplot%box(i)%rest%xmin
      sbrect%xmax = rec%desc%restlim(1,iband)
      sbrect%ymin = cplot%box(i)%rest%ymin
      sbrect%ymax = cplot%box(i)%rest%ymax
      sbrect%col = aoutcol
      call rec_draw_frect(sbrect,cplot%box(i)%rest,error)
      if (error) return
    endif
    if  (cplot%box(i)%rest%xmax.gt.rec%desc%restlim(2,iband)) then
      sbrect%xmin = cplot%box(i)%rest%xmax
      sbrect%xmax = rec%desc%restlim(2,iband)
      sbrect%ymin = cplot%box(i)%rest%ymin
      sbrect%ymax = cplot%box(i)%rest%ymax
      sbrect%col = aoutcol
      call rec_draw_frect(sbrect,cplot%box(i)%rest,error)
      if (error) return
    endif
    !ID sidebands
    if (rec%desc%n_sbands.eq.2) then
      sb_code(1) = usb_code !usb first
      sb_code(2) = lsb_code !lsb
    else if (rec%desc%n_sbands.eq.1) then
      sb_code(1) = rec%tune(i)%sb_code ! tuned side band for single sideband
    else
      call astro_message(seve%e,rname,'We should have 1 or 2 sidebands')
      error = .true.
      return
    endif
    do j=1,rec%desc%n_sbands
      call if1torf(rec%tune(i)%flo1,rec%desc%iflim(2),sb_code(j),frf,error)
      if (error) return
      call rftorest(rec%source%dopshift,frf,sbrect%xmax,error)
      if (error) return
      call if1torf(rec%tune(i)%flo1,rec%desc%iflim(1),sb_code(j),frf,error)
      if (error) return
      call rftorest(rec%source%dopshift,frf,sbrect%xmin,error)
      if (error) return
      sbrect%ymin = cplot%box(i)%rest%ymin
      sbrect%ymax = cplot%box(i)%rest%ymax
      sbrect%col = atunedcol
      call rec_draw_frect(sbrect,cplot%box(i)%rest,error)
      if (error) return
    enddo
    !ID areas outside call for proposal
    sbrect%xmin = rec%desc%restlim(1,iband)
    sbrect%xmax = rec%desc%restcall(1,iband)
    sbrect%ymin = cplot%box(i)%rest%ymin
    sbrect%ymax = cplot%box(i)%rest%ymax
    sbrect%col = adefcol
    call rec_draw_hrect(sbrect,cplot%box(i)%rest,error)
    if (error) return
    sbrect%xmax = rec%desc%restlim(2,iband)
    sbrect%xmin = rec%desc%restcall(2,iband)
    sbrect%ymin = cplot%box(i)%rest%ymin
    sbrect%ymax = cplot%box(i)%rest%ymax
    sbrect%col = adefcol
    call rec_draw_hrect(sbrect,cplot%box(i)%rest,error)
    if (error) return
    !Draw the box
    call rec_draw_fbox(cplot,i,drawaxis,error)
    if (error) return
    ! show tuning line
    call rec_draw_linetune(rec%tune(i),cplot%box(i)%rest,error)
    if (error) return
    if (rec%source%z.ne.0) then
      call gr_pen(colour=azcol,error=error)
      if (error) return
    else
      call gr_pen(colour=adefcol,error=error)
      if (error) return
    endif
    write (comm,'(a,1x,a,1x,a)') 'DRAW TEXT -1.5 0 "',trim(rec%desc%bandname(iband)),'" 5 90 /CHARACTER 4'
    call gr_exec1(comm)
    ! Draw tuning name
    xt=0d0
    yt=2d0
    call rec_make_title(rec%tune(i),mess,error)
    if (error) return
    if (rec%tune(i)%outlo) then
      call gr_pen(colour=aconflictcol,error=error)
      if (error) return
      write (mess2,'(a,1x,a)') trim(mess),'(out of LO range)'
      call gr_exec1(comm)
    else
      mess2=trim(mess)
      call gr_pen(colour=adefcol,error=error)
      if (error) return
    endif
    write (comm,'(a,1x,f0.3,1x,f0.3,1x,a,1x,a,1x,a)') &
          'DRAW TEXT',xt,yt,'"',trim(mess2),'" 6 0 /CHARACTER 7'
    call gr_exec1(comm)
    call gr_pen(colour=adefcol,error=error)
    if (error) return
    ! plot molecular lines
    call gr_exec1(molchar)
    call rec_draw_molecules(cata,cplot%box(i)%rest,error)
    if (error) return
    call gr_exec1(defchar)
    call gr_execl('CHANGE DIRECTORY')
    cplot%desc%plotmode = pm_tunedbands
  enddo
  !
end subroutine rec_plot_nbands
!
subroutine rec_plot_mbands(rdesc,rsou,cata,fixed,cplot,drawaxis,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_plot_mbands
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Displays the frequency coverage of a multi band receiver
  ! All band displayed
  !-----------------------------------------------------------------------
  type(receiver_desc_t), intent(in) :: rdesc ! receiver description and tuning
  type(receiver_source_t), intent(in) :: rsou ! receiver description and tuning
  type(plot_molecules_t), intent(in)   :: cata     !plot known lines from catalog
  logical, intent(in)   :: fixed     !plot axis with the same scale
  type(current_boxes_t), intent(inout) :: cplot
  type(frequency_axis_t), intent(in) :: drawaxis
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter      :: rname='PLOT'
  integer(kind=4)       :: i
  real(kind=8)          :: px,py,yoff,ytitle,boxsize
  real(kind=8)          :: fmin(rdesc%n_rbands),fmax(rdesc%n_rbands),flo1
  character(len=200)    :: comm,defchar,molchar
  type(draw_rect_t)     :: sbrect
  !
  !Prepare the limits of the bands
  call rec_def_scale(rdesc,fixed,fmin,fmax,error)
  if (error) return
  !
  ! Clear the current window, and ONLY the current window
  call gr_execl('CHANGE DIRECTORY') ! No arg = go to the top directory
  call gr_execl('CLEAR DIRECTORY') ! No arg = clear the whole window
  call gr_exec1('TICKSPACE 0 0 0 0')
  call gr_pen(colour=adefcol,error=error)
  if (error) return
  ! get page info
  call sic_get_dble('page_x',px,error)
  if (error) return
  call sic_get_dble('page_y',py,error)
  if (error) return
  if (px.lt.29) then
      call astro_message(seve%w,rname,'PLOT is optimized for LANDSCAPE orientation of the display')
  endif
  ! set plot parameters
  cplot%nbox = rdesc%n_rbands
  do i=1,cplot%nbox
    call rec_reset_box(cplot%box(i),error)
  enddo
  cplot%desc%defchar = 0.4
  write (defchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%defchar
  cplot%desc%molchar = 0.2
  write (molchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%molchar
  if (px.gt.25) then
    cplot%desc%smallchar = 0.3
  else if (px.le.25) then
    cplot%desc%smallchar = 0.2
  else
    call astro_message(seve%e,rname,'Problem with page size')
    error = .true.
    return
  endif
  call gr_exec1(defchar)
  yoff = 2d0
  ytitle = 1d0
  boxsize = min((py-3.d0)/cplot%nbox-yoff,4.)
  !Loop on receiver bands
  do i=1,cplot%nbox
    !physical properties of the box
    cplot%box(i)%phys%sx = px-4.5d0
    cplot%box(i)%phys%sy = boxsize
    cplot%box(i)%phys%xmin = 3d0
    cplot%box(i)%phys%xmax = cplot%box(i)%phys%xmin+cplot%box(i)%phys%sx
    cplot%box(i)%phys%ymax = py-ytitle-yoff-(i-1)*(boxsize+yoff)
    cplot%box(i)%phys%ymin = cplot%box(i)%phys%ymax-boxsize
    write (comm,'(a,i0)') 'CREATE DIRECTORY BOX',i
    call gr_execl(comm)
    write (comm,'(a,i0)') 'CHANGE DIRECTORY BOX',i
    call gr_execl(comm)
    !
    !define the box limits in all freq frames
    cplot%box(i)%iband = i
    ! Do not plot tuned band here
    flo1=0d0
    !
    call rec_def_fbox(fmin(i),fmax(i),'REST',cplot%box(i),rdesc,rsou,flo1,error)
    if (error) return
    ! Locate the box
    call rec_locate_fbox(cplot%box(i)%phys,error)
    if (error) return
    !
    if (i.eq.1) then
      call rec_draw_source(rname,rsou,error)
      if (error) return
    endif
    !
    ! id what is not tuned
    call setdepth(3)  ! background rectangles
    sbrect%xmin = cplot%box(i)%rest%xmin
    sbrect%xmax = cplot%box(i)%rest%xmax
    sbrect%ymin = cplot%box(i)%rest%ymin
    sbrect%ymax = cplot%box(i)%rest%ymax
    sbrect%col = aavailcol
    call rec_draw_frect(sbrect,cplot%box(i)%rest,error)
    if (error) return
    !ID areas outside receiver range
    if  (cplot%box(i)%rest%xmin.lt.rdesc%restlim(1,i)) then
      sbrect%xmin = cplot%box(i)%rest%xmin
      sbrect%xmax = rdesc%restlim(1,i)
      sbrect%ymin = cplot%box(i)%rest%ymin
      sbrect%ymax = cplot%box(i)%rest%ymax
      sbrect%col = aoutcol
      call rec_draw_frect(sbrect,cplot%box(i)%rest,error)
      if (error) return
    endif
    if  (cplot%box(i)%rest%xmax.gt.rdesc%restlim(2,i)) then
      sbrect%xmin = cplot%box(i)%rest%xmax
      sbrect%xmax = rdesc%restlim(2,i)
      sbrect%ymin = cplot%box(i)%rest%ymin
      sbrect%ymax = cplot%box(i)%rest%ymax
      sbrect%col = aoutcol
      call rec_draw_frect(sbrect,cplot%box(i)%rest,error)
      if (error) return
    endif
    call setdepth(1)
    !
    !ID areas outside call for proposal
    sbrect%xmin = rdesc%restlim(1,i)
    sbrect%xmax = rdesc%restcall(1,i)
    sbrect%ymin = cplot%box(i)%rest%ymin
    sbrect%ymax = cplot%box(i)%rest%ymax
    sbrect%col = adefcol
    call rec_draw_hrect(sbrect,cplot%box(i)%rest,error)
    if (error) return
    sbrect%xmax = rdesc%restlim(2,i)
    sbrect%xmin = rdesc%restcall(2,i)
    sbrect%ymin = cplot%box(i)%rest%ymin
    sbrect%ymax = cplot%box(i)%rest%ymax
    sbrect%col = adefcol
    call rec_draw_hrect(sbrect,cplot%box(i)%rest,error)
    if (error) return
    if (rsou%z.ne.0) then
      call gr_pen(colour=azcol,error=error)
      if (error) return
    else
      call gr_pen(colour=adefcol,error=error)
      if (error) return
    endif
    write (comm,'(a,1x,a,1x,a)') 'DRAW TEXT -1.5 0 "',trim(rdesc%bandname(i)),'" 5 90 /CHARACTER 4'
    call gr_exec1(comm)
    call gr_pen(colour=adefcol,error=error)
    if (error) return
    ! draw the box
    call rec_draw_fbox(cplot,i,drawaxis,error)
    if (error) return
    ! plot molecular lines
    call gr_exec1(molchar)
    call rec_draw_molecules(cata,cplot%box(i)%rest,error)
    if (error) return
    call gr_exec1(defchar)
    call gr_execl('CHANGE DIRECTORY')
    cplot%desc%plotmode = pm_allbands
  enddo
  !
end subroutine rec_plot_mbands
!
subroutine rec_plot_tuned(rdesc,rsou,rtune,cata,cplot,drawaxis,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_plot_tuned
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Displays the sideband of tuned rec bands
  !-----------------------------------------------------------------------
  type(receiver_desc_t), intent(in) :: rdesc ! receiver description and tuning
  type(receiver_source_t), intent(in) :: rsou ! receiver description and tuning
  type(receiver_tune_t), intent(in) :: rtune ! receiver description and tuning
  type(plot_molecules_t), intent(in)   :: cata     !plot known lines from catalog
  type(current_boxes_t), intent(inout) :: cplot
  type(frequency_axis_t), intent(in) :: drawaxis
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter      :: rname='PLOT'
  integer(kind=4)       :: i,j,sb_code(m_sideband)
  real(kind=8)          :: frf,xt,yt
  character(len=200)    :: comm,mess1,mess2
  type(draw_rect_t)     :: sbrect
  !
  do i=1,cplot%nbox
    ! Cycle if box has an band that does not match the rtune
    if (cplot%box(i)%iband.ne.0.and.rtune%iband.ne.cplot%box(i)%iband) cycle
    write (comm,'(a,i0)') 'CHANGE DIRECTORY BOX',i
    call gr_execl(comm)
    !ID sidebands
    if (rdesc%n_sbands.eq.2) then
      sb_code(1) = usb_code !usb first
      sb_code(2) = lsb_code !lsb
    else if (rdesc%n_sbands.eq.1) then
      sb_code(1) = rtune%sb_code ! tuned side band for single sideband
    else
      call astro_message(seve%e,rname,'We should have 1 or 2 sidebands')
      error = .true.
      return
    endif
    call setdepth(2) ! between background and front
    do j=1,rdesc%n_sbands
      call if1torf(rtune%flo1,rdesc%iflim(2),sb_code(j),frf,error)
      if (error) return
      call rftorest(rsou%dopshift,frf,sbrect%xmax,error)
      if (error) return
      call if1torf(rtune%flo1,rdesc%iflim(1),sb_code(j),frf,error)
      if (error) return
      call rftorest(rsou%dopshift,frf,sbrect%xmin,error)
      if (error) return
      sbrect%ymin = cplot%box(i)%rest%ymin
      sbrect%ymax = cplot%box(i)%rest%ymax
      sbrect%col = atunedcol
      call rec_draw_frect(sbrect,cplot%box(i)%rest,error)
      if (error) return
    enddo
    call setdepth(1)
    ! show tuning line
    call rec_draw_linetune(rtune,cplot%box(i)%rest,error)
    if (error) return
    if (cplot%box(i)%iband.ne.0) then
    ! Plot tuning legend in box is within a rec band
      !       xt=cplot%box(i)%phys%sx/4d0
      xt=0
      !       yt=2d0*cplot%desc%defchar
      yt=2
      call rec_make_title(rtune,mess1,error)
      if (error) return
      if (rtune%outlo) then
        call gr_pen(colour=aconflictcol,error=error)
        if (error) return
        write (mess2,'(a,1x,a)') trim(mess1),'(out of LO range)'
      else
        mess2=trim(mess1)
        call gr_pen(colour=adefcol,error=error)
        if (error) return
      endif
      write (comm,'(a,1x,f0.3,1x,f0.3,1x,a,a,a)') &
            'DRAW TEXT',xt,yt,'"',trim(mess2),'" 6 0 /CHARACTER 7'
      call gr_exec1(comm)
    endif
    call gr_pen(colour=adefcol,error=error)
    if (error) return
    write (comm,'(a)') 'CHANGE DIRECTORY'
    call gr_execl(comm)
  enddo ! i plot box
  !
end subroutine rec_plot_tuned
!
subroutine rec_make_title(rtune,mess,error)
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_make_title
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! prepare the message to be written as title
  !-----------------------------------------------------------------------
  type(receiver_tune_t), intent(in) :: rtune
  character(len=*), intent(inout)   :: mess
  logical, intent(inout) :: error
  ! local
  character(len=256) :: m0,m1,m2
  !
  write (m0,'(a,1x,f0.3,1x,a,1x,f0.3,a,1x,f0.3,a)')  &
         'REST: ',rtune%frest*ghzpermhz,&
         'GHZ (LSR: ',rtune%flsr*ghzpermhz, &
         ', RF: ',rtune%frf*ghzpermhz,')'
  if (len(rtune%name).gt.0) then
    write (m1,'(a,1x,a)') trim(rtune%name),trim(m0)
  endif
  write (m2,'(a,1x,i0,1x,a,1x,a)') &
        'IF1:',nint(rtune%fcent),'MHz',sideband(rtune%sb_code)
  write (mess,'(a,1x,a)') trim(m1),trim(m2)
  !
end subroutine rec_make_title
!
subroutine rec_draw_title(cplot,mess,error)
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_draw_title
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! print the message title above the upper box
  !-----------------------------------------------------------------------
  type(current_boxes_t), intent(inout)          :: cplot
  character(len=*), intent(inout)   :: mess
  logical, intent(inout) :: error
  ! local
  type(frequency_box_phys_t) :: lbox
  character(len=256), parameter :: rname='PLOT'
  character(len=512) :: comm
  real(kind=4) :: xt,yt
  !
  if (.not.gtexist('<GREG<BOXLEGEND')) then
    call rec_def_legendbox(rname,lbox,cplot,error)
    if (error) return
    write (comm,'(a,i0)') 'CREATE DIRECTORY BOXLEGEND'
    call gr_execl(comm)
    write (comm,'(a,i0)') 'CHANGE DIRECTORY BOXLEGEND'
    call gr_execl(comm)
    ! Locate the box
    call rec_locate_fbox(lbox,error)
  else
    write (comm,'(a,i0)') 'CHANGE DIRECTORY BOXLEGEND'
    call gr_execl(comm)
  endif
  xt=0
  yt=6
  write (comm,'(a,1x,f0.3,1x,f0.3,1x,a,a,a)') &
    'DRAW TEXT',xt,yt,'"',trim(mess),'" 5 0 /CHARACTER 8'
  call gr_exec1(comm)
  write (comm,'(a,i0)') 'CHANGE DIRECTORY'
  call gr_execl(comm)
  !
end subroutine rec_draw_title
!
subroutine rec_zoom(cata,fz1,fz2,rdesc,rsou,cplot,drawaxis,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_zoom
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! single frame zoom in a user define range
  !-----------------------------------------------------------------------
  type(plot_molecules_t), intent(in)   :: cata
  real(kind=8), intent(in)      :: fz1, fz2 ! zoom limit, in REST frame
  type(receiver_desc_t), intent(in) :: rdesc
  type(receiver_source_t), intent(in) :: rsou
  type(current_boxes_t), intent(inout) :: cplot
  type(frequency_axis_t), intent(in) :: drawaxis
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='PLOT'
  integer(kind=4) :: i,j,k,sb_code(m_sideband)
  real(kind=8)         :: px,py,yoff,fmin(rdesc%n_rbands),fmax(rdesc%n_rbands)
  real(kind=8), parameter :: flo1=0d0
  character(len=200)    :: comm,defchar,molchar
  type(draw_rect_t) :: drect
  !
  if (fz1.ge.fz2) then
    call astro_message(seve%e,rname,'Fmin should be lower than Fmax')
    error = .true.
    return
  endif
  !
  cplot%nbox = 1
  call rec_reset_box(cplot%box(1),error)
  if (error) return
  ! get page info
  call sic_get_dble('page_x',px,error)
  if (error) return
  call sic_get_dble('page_y',py,error)
  if (error) return
  ! set plot parameters
  yoff = 5.5d0
  ! Clear the current window, and ONLY the current window
  call gr_execl('CHANGE DIRECTORY') ! No arg = go to the top directory
  call gr_execl('CLEAR DIRECTORY') ! No arg = clear the whole window
  call gr_exec1('TICKSPACE 0 0 0 0')
  call gr_pen(colour=adefcol,error=error)
  if (error) return
  !
  cplot%desc%defchar = 0.4
  write (defchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%defchar
  cplot%desc%molchar = 0.2
  write (molchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%molchar
  if (px.gt.25) then
    cplot%desc%smallchar = 0.3
  else if (px.le.25) then
    cplot%desc%smallchar = 0.2
  else
    call astro_message(seve%e,rname,'Problem with page size')
    error = .true.
    return
  endif
  call gr_exec1(defchar)
  !
  cplot%box(1)%phys%sx = px-4.5d0
  cplot%box(1)%phys%sy = 4d0
  cplot%box(1)%phys%xmin = 3d0
  cplot%box(1)%phys%xmax = cplot%box(1)%phys%xmin+cplot%box(1)%phys%sx
  cplot%box(1)%phys%ymax = py-yoff
  cplot%box(1)%phys%ymin = cplot%box(1)%phys%ymax-cplot%box(1)%phys%sy
  !
  call rec_def_fbox(fz1,fz2,'REST',cplot%box(1),rdesc,rsou,flo1,error)
  if (error) return
  !
  !
  write (comm,'(a,i0)') 'CREATE DIRECTORY BOX',1
  call gr_execl(comm)
  write (comm,'(a,i0)') 'CHANGE DIRECTORY BOX',1
  call gr_execl(comm)
  !
  call rec_locate_fbox(cplot%box(1)%phys,error)
  if (error) return
  !
  call rec_draw_source(rname,rsou,error)
  if (error) return
  !
  ! Draw full window in black
  drect%xmin = cplot%box(1)%rest%xmin
  drect%xmax = cplot%box(1)%rest%xmax
  drect%ymin = cplot%box(1)%rest%ymin
  drect%ymax = cplot%box(1)%rest%ymax
  drect%col = aoutcol
  call setdepth(4) ! background
  call rec_draw_frect(drect,cplot%box(1)%rest,error)
  call setdepth(1) ! background
  if (error) return
  if (rdesc%n_sbands.eq.2) then
    sb_code(1) = usb_code !usb first
    sb_code(2) = lsb_code !lsb
  else if (rdesc%n_sbands.eq.1) then
    call astro_message(seve%e,rname,'Problem with single sideband')
    error = .true.
    return
!    sb_code(1) = rec%tune(i)%sb_code ! tuned side band for single sideband
!       JB to be corrected
  else
    call astro_message(seve%e,rname,'We should have 1 or 2 sidebands')
    error = .true.
    return
  endif  
  !
  do i=1,rdesc%n_rbands
    j = i-1
    k = i+1
    if (j.eq.0) then
      fmin(i) = 0d0
    else
      fmin(i) = rdesc%restlim(2,j)
    endif
    if (k.gt.rdesc%n_rbands) then
      fmax(i) = rdesc%restlim(2,j)+100d3
    else
      fmax(i) = rdesc%restlim(1,k)
    endif
  enddo
  !
  ! ID observable ranges
  ! Plot in grey observable ranges
  do i=1,rdesc%n_rbands
    drect%xmin = rdesc%restlim(1,i)
    drect%xmax = rdesc%restlim(2,i)
    drect%ymin = cplot%box(1)%rest%ymin
    drect%ymax = cplot%box(1)%rest%ymax
    drect%col = aavailcol
    call setdepth(3) ! background
    call rec_draw_frect(drect,cplot%box(1)%rest,error)
    call setdepth(1) !
    if (error) return
    if (cplot%box(1)%rest%xmax.lt.fmax(i).and.cplot%box(1)%rest%xmin.gt.fmin(i)) then
      cplot%box(1)%iband = i
      write (comm,'(a,1x,a,1x,a)') 'DRAW TEXT -1.5 0 "',trim(rdesc%bandname(cplot%box(1)%iband)),  &
              '" 5 90 /CHARACTER 4'
      call gr_exec1(comm)
      exit
    endif
  enddo
  if (cplot%box(1)%iband.eq.0) then
    call astro_message(seve%i,rname,'Could not attribute a receiver band to the current zoom window')
  endif
  !
  call rec_draw_fbox(cplot,1,drawaxis,error)
  if (error) return
  !
  call gr_exec1(molchar)
  call rec_draw_molecules(cata,cplot%box(1)%rest,error)
  if (error) return
  call gr_exec1('BOX N N N')
  call gr_execl('CHANGE DIRECTORY')
  cplot%desc%plotmode = pm_zoom
  !
end subroutine rec_zoom
