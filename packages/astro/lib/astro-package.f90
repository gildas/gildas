!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage the EPHEM package
! This is a trick to solve the fact that we may want to load only part of
! a package as in the case of ASTRO. The EPHEM name is probably not
! adequate...
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine ephem_pack_set(pack)
  use gpack_def
  use gkernel_interfaces
  !----------------------------------------------------------------------
  ! @ public-mandatory
  !----------------------------------------------------------------------
  type(gpack_info_t), intent(out) :: pack  !
  !
  external :: atm_pack_set
  external :: ephem_pack_init
  external :: ephem_pack_clean
  !
  pack%name='ephem'
  pack%depend(1:2) = (/ locwrd(atm_pack_set), locwrd(greg_pack_set) /)
  pack%init=locwrd(ephem_pack_init)
  pack%clean=locwrd(ephem_pack_clean)
  pack%authors="J.Boissier, F.Gueth, J.Pety"
  !
end subroutine ephem_pack_set
!
subroutine ephem_pack_init(gpack_id,error)
  !----------------------------------------------------------------------
  ! @ private
  !
  !----------------------------------------------------------------------
  integer(kind=4) :: gpack_id       !
  logical :: error                  !
  !
  call atm_message_set_id(gpack_id)
#if defined(ATM2009)
  call atm_c_message_set_id(gpack_id)
#endif
#if defined(ATM2016)
  call atm_c_message_set_id(gpack_id)
#endif
  call astro_message_set_id(gpack_id)
  !
  ! Specific initializations
  call ephini(error)
  if (error) return
  !
end subroutine ephem_pack_init
!
subroutine ephem_pack_clean(error)
  !----------------------------------------------------------------------
  ! @ private
  !
  !----------------------------------------------------------------------
  logical, intent(inout) :: error   !
  !
  call ephclose(error)
  !
end subroutine ephem_pack_clean
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage the ASTRO package
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine astro_pack_set(pack)
  use gpack_def
  use gkernel_interfaces
  !----------------------------------------------------------------------
  ! @ public
  !----------------------------------------------------------------------
  type(gpack_info_t), intent(out) :: pack  !
  !
  external :: astro_pack_init
  external :: astro_pack_clean
  !
  pack%name='astro'
  pack%ext='.astro'
  pack%depend(1:1) = (/ locwrd(greg_pack_set) /)
  pack%init=locwrd(astro_pack_init)
  pack%clean=locwrd(astro_pack_clean)
  pack%authors="J.Boissier, F.Gueth, J.Pety"
  !
end subroutine astro_pack_set
!
subroutine astro_pack_init(gpack_id,error)
  !----------------------------------------------------------------------
  ! @ private
  !
  !----------------------------------------------------------------------
  integer(kind=4) :: gpack_id       !
  logical :: error                  !
  !
  call atm_message_set_id(gpack_id)
#if defined(ATM2009)
  call atm_c_message_set_id(gpack_id)
#endif
#if defined(ATM2016)
  call atm_c_message_set_id(gpack_id)
#endif
  call astro_message_set_id(gpack_id)
  !
  call load_astro('INTERACTIVE')
  call exec_program('SIC'//char(92)//'SIC PRIORITY 1 ASTRO')
  !
  ! Specific initializations
  call atm_init(error)
  if (error) return
  call astro_init(error)
  if (error) return
  !
end subroutine astro_pack_init
!
subroutine astro_pack_clean(error)
  !----------------------------------------------------------------------
  ! @ private
  !
  !----------------------------------------------------------------------
  logical, intent(inout) :: error   !
  !
  call ephclose(error)
  !
end subroutine astro_pack_clean
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
