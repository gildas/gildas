subroutine noema_list(line,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_list
  use my_receiver_globals
  use frequency_axis_globals
  !-----------------------------------------------------------------------
  ! @ private
  ! List current state of the setup definition
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(inout) :: line        ! command line
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='LIST'
  integer(kind=4), parameter :: optconflict = 1  ! option /CONFLICT
  integer(kind=4), parameter :: optindex = 2  ! option /INDEX
  integer(kind=4) :: is1,is2
  logical :: doindex,dopchanged,doconflict
  !
  if (.not.noema%cfebe%defined) then
    call astro_message(seve%e,rname,'No tuning found')
    error = .true.
    return
  endif
  !
  !check that doppler did not changed since last tuning
  call rec_check_doppler(noema%source,noema%recdesc%redshift,dopchanged,error)
  if (dopchanged) then
    call astro_message(seve%e,rname,'Source properties changed since last action')
    call rec_display_error('Source changed since last action',error)
    error = .true.
    return
  endif
  !
  if (noema%cfebe%spw%out%n_spw.eq.0) then
    call astro_message(seve%e,rname,'No SPW defined')
    error = .true.
    return
  endif
  !
  doconflict = sic_present(optconflict,0)
  doindex = sic_present(optindex,0)
  !
  !   LIST the spw, order is the hardware one (LowRes First, Hpol first, the frequency)
  if (sic_narg(0).eq.0) then
    is1=1
    is2=noema%cfebe%spw%out%n_spw
  else if (sic_narg(0).eq.2) then
    call sic_i0(line,0,1,is1,.true.,error)
    if (error) return
    call sic_i0(line,0,2,is2,.true.,error)
    if (error) return
  else
    call astro_message(seve%e,rname,'LIST accepts only 0 or 2 (ispw1 and ispw2) arguments')
    error=.true.
    return
  endif
  call noema_list_spw(rname,noema%cfebe%spw%out,freq_axis%main,noema%source,doindex,doconflict,is1,is2,error)
  if (error) return
  !
end subroutine noema_list
!
subroutine noema_show_online(line,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_show_online
  use my_receiver_globals
  use frequency_axis_globals
  use plot_molecules_globals
  !-----------------------------------------------------------------------
  ! @ private
  ! SHOW command in OBS
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(inout) :: line        ! command line
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='SHOW'
  integer(kind=4), parameter :: optplot = 1  ! option /PLOT
  logical :: doplot,dopchanged
  integer(kind=4), parameter :: mkeys=2
  integer(kind=4) :: nkey,is1,is2,lpar,iu
  character(len=16) :: keys(mkeys),key,par
  data keys/'SPW','BASEBANDS'/
  !
  if (.not.noema%cfebe%defined) then
    call astro_message(seve%e,rname,'No tuning found')
    error = .true.
    return
  endif
  !
  !check that doppler did not changed since last tuning
  call rec_check_doppler(noema%source,noema%recdesc%redshift,dopchanged,error)
  if (dopchanged) then
    call astro_message(seve%e,rname,'Source properties changed since last action')
    call rec_display_error('Source changed since last action',error)
    error = .true.
    return
  endif
  !
  ! /PLOT option
  doplot = sic_present(optplot,0)
  !
  call sic_ke(line,0,1,par,lpar,.true.,error)
  if (error) return
  call sic_ambigs(rname,par,key,nkey,keys,mkeys,error)
  if (error) return
  !
  if (doplot) then
    if (noema%cfebe%i_f%selunit%n_ifsel.le.0) then
      ! Select all basebands and plot all
      noema%cfebe%i_f%selunit%n_ifsel=0
      noema%cfebe%i_f%selunit%polmode='B'
      do iu=1,noema%cfebe%pfx%n_units
        noema%cfebe%i_f%selunit%n_ifsel=noema%cfebe%i_f%selunit%n_ifsel+1
        noema%cfebe%i_f%selunit%usel(iu)=iu
      enddo
    endif
    call noema_plot_selpfx(rname,noema,cplot,molecules,freq_axis,error)
    if (error) return
  endif
  !
  select case (key)
  case ('SPW') ! LIST the spw
    if (noema%cfebe%spw%out%n_spw.eq.0) then
      call astro_message(seve%e,rname,'No SPW defined')
      error = .true.
      return
    endif
    is1=1
    is2=noema%cfebe%spw%out%n_spw
    call noema_list_spw_online(rname,noema%cfebe%spw%out,noema%source,noema%cfebe%pfx,error)
    if (error) return
  case ('BASEBANDS') ! LIST the chunks
    call noema_list_pfx(rname,noema%cfebe%pfx,freq_axis%main,noema%source,error)
    if (error) return
  end select
  !
end subroutine noema_show_online
!
subroutine noema_list_spw(rname,spwout,fr_axis,rsou,byindex,onlyconflict,w1,w2,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_list_spw
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! list all the spwctral window already defined
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(spw_output_t), intent(inout) :: spwout ! list of spw
  type(receiver_source_t), intent(in) :: rsou ! source parameters (for freq conversion)
  character(len=*), intent(in) :: fr_axis     ! current main frequency axis
  logical, intent(in) :: byindex         ! list in spectral resolution then freq order
  logical, intent(in) :: onlyconflict         ! list only conflicts
  integer(kind=4)       ::w1,w2               ! sublist limits
  logical, intent(inout) :: error
  ! Local
  integer(kind=4)       :: iw,nch,ic1,ic2,jw
  integer(kind=4) :: sort(spwout%n_spw)  ! Automatic array
  character(len=64)    :: mess1,mess2,mess3,mess4,mess5,mess6
  character(len=512)    :: mess
  !
  if (onlyconflict) then
    write (mess,'(a)') 'Spectral windows in conflict are:'
  else
    write (mess,'(i0,1x,a)') spwout%n_spw,'spectral windows defined:'
  endif
  call astro_message(seve%i,rname,mess)
  !
  if (w1.le.0.or.w2.le.0.or.w2.lt.w1) then
    call astro_message(seve%e,rname,'Problem with index of spectral windows to list')
    error=.true.
    return
  endif
  !
  ! Define the order for the list
  do iw=1,spwout%n_spw
    sort(iw) = iw
  enddo
  if (.not.(byindex)) then
    call gi0_quicksort_index_with_user_gtge(sort,spwout%n_spw,  &
      sort_spw_freq_gt,sort_spw_freq_ge,error)
    if (error)  return
  endif
  !
  do jw=w1,w2
    iw=sort(jw)
    if (onlyconflict.and. &
        .not.spwout%win(iw)%conflict.and. & 
        .not.spwout%win(iw)%overload) cycle
    ic1=min(spwout%win(iw)%ich2,spwout%win(iw)%ich1)
    ic2=max(spwout%win(iw)%ich2,spwout%win(iw)%ich1)
    nch = abs(spwout%win(iw)%ich2-spwout%win(iw)%ich1)+1
    write (mess1,'(a,1x,i3,1x,a)')  'SPW',iw,trim(spwout%win(iw)%label)
    write (mess2,'(f8.1,1x,a)') spwout%win(iw)%resol,'kHz,'
    write (mess3,'(a,1x,i2,1x,a,1x,i2)') 'Chunks',ic1,'to',ic2
    ! get output freq in the current main frequency axis
    call noema_list_outputfreq(spwout%win(iw),fr_axis,rsou,mess4,error)
    if (error) return
    write (mess,'(a,1x,a,1x,a,a)') trim(mess1),trim(mess2),trim(mess3),trim(mess4)
    if (len_trim(spwout%win(iw)%user_label).gt.0) then
      write (mess5,'(a,a,a)') '(' , trim(spwout%win(iw)%user_label) , ')'
      write (mess,'(a,1x,a)') trim(mess),trim(mess5)
    endif
    if (spwout%win(iw)%conflict.or.spwout%win(iw)%overload) then
      write (mess6,'(a)') '!Conflict!'
      write (mess,'(a,1x,a)') trim(mess),trim(mess6)
    endif
    call astro_message(seve%r,rname,mess)
  enddo
  !
end subroutine noema_list_spw
!
subroutine noema_list_spw_online(rname,spwout,rsou,pfx,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_list_spw_online
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! list all the spwctral window already defined in ONLINE mode
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(spw_output_t), intent(inout) :: spwout ! list of spw
  type(receiver_source_t), intent(in) :: rsou ! source parameters (for freq conversion)
  type(pfx_t), intent(in) :: pfx
  logical, intent(inout) :: error
  ! Local
  integer(kind=4)       :: ic1,ic2,jw,iu,it,nb_chunks,ic,imode
  integer(kind=4) :: obsindex(spwout%n_spw)  ! Automatic array
  character(len=512)    :: mess,mess1,mess2,mess3
  !
  ! Define the order for the list
  call noema_spw_obs_index(rname,spwout,pfx,obsindex,error)
  if (error) return
  !
  do iu=1,pfx%n_units
    imode=pfx%unit(iu)%imode
    write (mess1,'(a,i0,1x,a,a,a)')  'BBAND#',iu,'(',trim(pfx%unit(iu)%label(3:5)),')'
    if (imode.ne.-1) then
      do it=1,pfx%unit(iu)%mode(imode)%n_types
        if (.not.pfx%unit(iu)%mode(imode)%chtype(it)%move_chunk) cycle
        nb_chunks=pfx%unit(iu)%mode(imode)%chtype(it)%usage
        exit
      enddo ! pfx type
      if (nb_chunks.eq.0) then
        write (mess,'(a,i0,1x,a,a,a)') 'No Chunks selected for BBAND#',iu, &
                      '(',trim(pfx%unit(iu)%label(3:5)),')'
      else
        write (mess,'(a,1x,a,1x,i0)') trim(mess1),'NB_CHUNKS=',nb_chunks
      endif
      call astro_message(seve%r,rname,mess)
      !
    else ! unit not configured
      write (mess,'(a,1x,a)') trim(mess1),'Not configured'
      call astro_message(seve%r,rname,mess)
    endif
    !
    do jw=1,spwout%n_spw
      if (spwout%win(jw)%label.ne.pfx%unit(iu)%label) cycle
      if (.not.spwout%win(jw)%flexible) cycle
      ic1=min(spwout%win(jw)%ich2,spwout%win(jw)%ich1)
      ic2=max(spwout%win(jw)%ich2,spwout%win(jw)%ich1)
      write (mess2,'(a,1x,a,1x,i0,1x,a,i0,a)') trim(mess1),'- SPW#',obsindex(jw), &
                    '(L',jw,') - CHUNK#'
      do ic=ic1,ic2
        write (mess2,'(a,1x,i0)') trim(mess2),ic
      enddo
      if (len_trim(spwout%win(jw)%user_label).gt.0) then
        write (mess3,'(a,a,a)') '(' , trim(spwout%win(jw)%user_label) , ')'
        write (mess2,'(a,1x,a)') trim(mess2),trim(mess3)
      endif
      call astro_message(seve%r,rname,mess2)
    enddo ! SPW
    call astro_message(seve%r,rname,'')
  enddo ! PFX units
  !
end subroutine noema_list_spw_online
!
subroutine noema_spw_obs_index(rname,spwout,pfx,obsindex,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_spw_obs_index
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Get the obs index of the currently defined SPW
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(spw_output_t), intent(in) :: spwout ! list of spw
  type(pfx_t), intent(in) :: pfx ! list of spw
  integer(kind=4), intent(inout) :: obsindex(spwout%n_spw)
  logical, intent(inout) :: error
  ! Local
  integer(kind=4)       :: iw,iu,it,imode,iobs
  !
  do iw=1,spwout%n_spw
    obsindex(iw)=-1
  enddo
  do iu=1,pfx%n_units
    iobs = 0
    do iw=1,spwout%n_spw
      if (spwout%win(iw)%label.ne.pfx%unit(iu)%label) cycle
      if (.not.spwout%win(iw)%flexible) then
        obsindex(iw)=0
      else
        imode=pfx%unit(iu)%imode
        do it=1,pfx%unit(iu)%mode(imode)%n_types
          if (.not.pfx%unit(iu)%mode(imode)%chtype(it)%move_chunk) cycle
          if (spwout%win(iw)%resol.eq.pfx%unit(iu)%mode(imode)%chtype(it)%df_chunks*1d3) then
            iobs=iobs+1
            obsindex(iw)=iobs
          else
            call astro_message(seve%e,rname,'Problem with SPW resolution')
            error=.true.
            return
          endif
        enddo
      endif
    enddo
  enddo
  !
end subroutine noema_spw_obs_index
!
function sort_spw_freq_gt(m,l)
  use my_receiver_globals
  !---------------------------------------------------------------------
  ! @ private
  ! Sorting function for noema_sort_freq_spw (greater than)
  ! ---
  !  NB: THIS SORTS THE GLOBAL SPW STRUCTURE SHARED THROUGH MODULE
  !      MY_RECEIVER_GLOBALS!!!
  !---------------------------------------------------------------------
  logical :: sort_spw_freq_gt
  integer(kind=4), intent(in) :: m,l
  !
  if (noema%cfebe%spw%out%win(m)%resol.ne.noema%cfebe%spw%out%win(l)%resol) then
    ! Sort by resolution (low res first)
    sort_spw_freq_gt = noema%cfebe%spw%out%win(m)%resol.lt.noema%cfebe%spw%out%win(l)%resol
    return
  endif
  if (noema%cfebe%spw%out%win(m)%restmin.ne.noema%cfebe%spw%out%win(l)%restmin) then
    ! Sort by min freq
    sort_spw_freq_gt = noema%cfebe%spw%out%win(m)%restmin.gt.noema%cfebe%spw%out%win(l)%restmin
    return
  endif
  ! Sort by H or V (H=1, V=2)
  sort_spw_freq_gt = noema%cfebe%spw%out%win(m)%pol_code.gt.noema%cfebe%spw%out%win(l)%pol_code
  !
end function sort_spw_freq_gt
!
function sort_spw_freq_ge(m,l)
  use my_receiver_globals
  !---------------------------------------------------------------------
  ! @ private
  ! Sorting function for noema_sort_freq_spw (greater or equal)
  ! ---
  !  NB: THIS SORTS THE GLOBAL SPW STRUCTURE SHARED THROUGH MODULE
  !      MY_RECEIVER_GLOBALS!!!
  !---------------------------------------------------------------------
  logical :: sort_spw_freq_ge
  integer(kind=4), intent(in) :: m,l
  !
  if (noema%cfebe%spw%out%win(m)%resol.ne.noema%cfebe%spw%out%win(l)%resol) then
    ! Sort by resol (low res first)
    sort_spw_freq_ge = noema%cfebe%spw%out%win(m)%resol.le.noema%cfebe%spw%out%win(l)%resol
    return
  endif
  if (noema%cfebe%spw%out%win(m)%restmin.ne.noema%cfebe%spw%out%win(l)%restmin) then
    ! Sort by min freq
    sort_spw_freq_ge = noema%cfebe%spw%out%win(m)%restmin.ge.noema%cfebe%spw%out%win(l)%restmin
    return
  endif
  ! Sort by H or V (H=1, V=2)
  sort_spw_freq_ge = noema%cfebe%spw%out%win(m)%pol_code.ge.noema%cfebe%spw%out%win(l)%pol_code
  !
end function sort_spw_freq_ge
!
subroutine noema_list_pfx(rname,pfx,fr_axis,rsou,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_list_pfx
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! list all the IFcode and the mode (+usage?) of pfx units
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(in)  :: rname
  type(pfx_t), intent(inout) :: pfx ! pfx description
  type(receiver_source_t), intent(in) :: rsou ! source parameters (for freq conversion)
  character(len=*), intent(in) :: fr_axis     ! current main frequency axis
  logical, intent(inout) :: error
  ! Local
  integer(kind=4)       :: iu,imode,it,percentuse
  character(len=64)    :: mess1,mess2
  character(len=512)    :: mess
  !
  do iu=1,pfx%n_units
    write (mess1,'(a,1x,i3,1x,a,1x,a,a)')  'UNIT',iu,'Baseband',trim(pfx%unit(iu)%label),':'
    imode=pfx%unit(iu)%imode
    if (imode.gt.0) then
      write (mess2,'(a,1x,a)') 'Mode:',trim(pfx%unit(iu)%mode(imode)%modename)
    else if (imode.eq.-1) then
      write (mess2,'(a)') 'Mode not configured'
    else
      call astro_message(seve%e,rname,'Problem with correlator unit mode')
      error=.true.
      return
    endif
    write (mess,'(a,1x,a,1x,a,a)') trim(mess1),trim(mess2)
    call astro_message(seve%r,rname,mess)
    if (imode.eq.-1) cycle
    ! Loop on chunktypes for configured units
    do it=1,pfx%unit(iu)%mode(imode)%n_types
      percentuse=nint(100.0*pfx%unit(iu)%mode(imode)%chtype(it)%usage/ &
                 pfx%unit(iu)%mode(imode)%chtype(it)%use_chunks)
      write (mess,'(a,a,1x,a,1x,i3)')  &
             '    ',trim(pfx%unit(iu)%mode(imode)%chtype(it)%typename),'usage:',percentuse
      call astro_message(seve%r,rname,mess)
    enddo
  enddo
  !
end subroutine noema_list_pfx
!
subroutine noema_list_outputfreq(spwu,fr_axis,rsou,mess,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_list_outputfreq
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! convert the frequency limits of the spw unit into the output freq frame
  !
  !-----------------------------------------------------------------------
  type(spw_unit_t), intent(inout) :: spwu ! list of spw
  character(len=*), intent(in) :: fr_axis     ! current main frequency axis
  type(receiver_source_t), intent(in) :: rsou ! source parameters (for freq conversion)
  character(len=*), intent(inout)     :: mess !output message with right frequencies
  logical, intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='LIST'
  real(kind=8)  :: fmin,fmax
  !
  call rec_resttooutput(rname,spwu%restmin,fr_axis,rsou,fmin,error)
  if (error) return
  call rec_resttooutput(rname,spwu%restmax,fr_axis,rsou,fmax,error)
  if (error) return
  write (mess,'(a,1x,a,1x,f10.2,1x,a,1x,f10.2,1x,a)') &
              ',',trim(fr_axis),fmin,'to',fmax,'MHz'
  !
end subroutine noema_list_outputfreq
!
subroutine noema_reset(line,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_reset
  use ast_astro
  use my_receiver_globals
  use plot_molecules_globals
  use frequency_axis_globals
  !-----------------------------------------------------------------------
  ! @ private
  ! Reset one or several spw
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(inout) :: line        ! command line
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='RESET'
  integer(kind=4) :: is,ir,nkey,nc,n_to_reset,iarg,i1,iu
  integer(kind=4) :: spw_to_reset(m_spw)
  logical :: dopchanged,dolast,dolist
  integer(kind=4), parameter :: mkeys=2
  character(len=16) :: keys(mkeys),arg,key
  character(len=256) :: mess,spwstring
  data keys/'LAST','*'/
  !
  if (.not.noema%cfebe%defined) then
    call astro_message(seve%e,rname,'No tuning found. Nothing to reset')
    error = .true.
    return
  endif
  !
  !check that doppler did not changed since last tuning
  call rec_check_doppler(noema%source,noema%recdesc%redshift,dopchanged,error)
  if (dopchanged) then
    call astro_message(seve%e,rname,'Source properties changed since last action')
    call rec_display_error('Source changed since last action',error)
    error = .true.
    return
  endif
  !
  if (noema%cfebe%spw%out%n_spw.eq.0) then
    call astro_message(seve%e,rname,'Nothing to reset (no SPW defined)')
    error = .true.
    return
  endif
  !
  dolist=.false.
  dolast=.false.
  n_to_reset=0
  ! First parse the command line
  ! 1- Look for known keywords
  arg=''
  call sic_ke(line,0,1,arg,nc,.false.,error)
  if (error) return
  !
  if (nc.ne.0) then
    call sic_ambigs_sub(rname,arg,key,nkey,keys,mkeys,error)
    if (error) return
    if (nkey.eq.0) dolist=.true.
  else
    ! no arg
    dolast=.true.
  endif
  !
  if (dolist) then
  ! not a recognized keyword --> a sic list
    i1=1
    ! Put list in a string
    do iarg=1,sic_narg(0)
      ! Translate the arguments (as string, because of "TO" in "12 TO 34") and
      ! save them in a string
      call sic_ke(line,0,iarg,spwstring(i1:),nc,.true.,error)
      if (error)  return
      i1 = i1+nc+1
    enddo
    ! Build the list of each individual element selected
    call sic_build_listi4(spw_to_reset,n_to_reset,m_spw,spwstring,rname,error)
    if (error)  return
    !
  else if (key.eq.'LAST'.or.dolast) then
    ! reset the spectral windows created by the last spw command
    call noema_last_spw(noema%cfebe%spw%out,n_to_reset,spw_to_reset,error)
    if (error)  return
    if (n_to_reset.eq.0) return
  else if (key.eq.'*') then
    !reset all flexible spectral windows
    n_to_reset=0
    do is=1,noema%cfebe%spw%out%n_spw
      if (.not.(noema%cfebe%spw%out%win(is)%flexible)) cycle
      n_to_reset=n_to_reset+1
      spw_to_reset(n_to_reset)=is
    enddo
  else
    call astro_message(seve%e,rname,'Could not understand what to do')
    error=.true.
    return
  endif
  !
  !Check that all spw to reset exist and can be reset before starting to reset
  do ir=1,n_to_reset
    if (spw_to_reset(ir).gt.noema%cfebe%spw%out%n_spw.or.spw_to_reset(ir).le.0) then
      write (mess,'(a,i0,1x,a)') 'SPW #',spw_to_reset(ir),'is not defined'
      call astro_message(seve%e,rname,mess)
      error=.true.
    endif
    if (.not.(noema%cfebe%spw%out%win(spw_to_reset(ir))%flexible)) then
      write (mess,'(a,i0,1x,a)') 'Fixed SPW #',spw_to_reset(ir),'cannot be reset'
      call astro_message(seve%e,rname,mess)
      error=.true.
    endif
  enddo
  if (error) then
    call astro_message(seve%e,rname,'Nothing done')
    return
  endif
  !
  !Do the reset
  do ir=1,n_to_reset
    write (mess,'(a,1x,i0)') 'Resetting Spectral Window #',spw_to_reset(ir)
    call astro_message(seve%i,rname,mess)
    call noema_reset_spw(noema%cfebe%pfx,noema%cfebe%spw%out%win(spw_to_reset(ir)),error)
    if (error) exit
  enddo
  ! Remove empty windows from spw%out type
  call noema_compress_spw(noema%cfebe%spw%out,error)
  if (error) return
  ! look for conflicts
  call noema_check_conflicts(rname,noema%cfebe%spw%out,noema%cfebe%pfx,error)
  if (error) return
  ! Display the spw list again (since numbers changed)
  call noema_list_spw(rname,noema%cfebe%spw%out,freq_axis%main,noema%source, & 
                        .false.,.false.,1,noema%cfebe%spw%out%n_spw,error)
  if (error) return
  ! Redo the plot
  if (cplot%desc%plotmode.eq.pm_receiver) then
    call rec_plot_sidebands(noema%recdesc,noema%source,noema%cfebe%rectune,cplot,molecules,freq_axis,error)
    if (error) return
    call noema_draw_summary(noema,cplot,molecules,freq_axis,error)
    if (error) return
  else 
    if (noema%cfebe%i_f%selunit%n_ifsel.eq.0) then
      ! Select all basebands and plot all
      do iu=1,noema%cfebe%pfx%n_units
        noema%cfebe%i_f%selunit%n_ifsel=noema%cfebe%i_f%selunit%n_ifsel+1
        noema%cfebe%i_f%selunit%usel(iu)=iu
      enddo
    endif
    call noema_plot_selpfx(rname,noema,cplot,molecules,freq_axis,error)
    if (error) return
    if (noema%source%sourcetype.eq.soukind_full) then
      call noema_oplot_dopminmax(noema,cplot,freq_axis,error)
      if (error) return
    endif
  endif
  !
  ! Let the user with the limits in main axis frequency frame
  call rec_set_limits(rname,cplot,freq_axis%main,error)
  if (error) return
  !
end subroutine noema_reset
!
subroutine noema_last_spw(spwout,n_last,spw_last,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_last_spw
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! find the latest spw that have been defined
  !-----------------------------------------------------------------------
  type(spw_output_t), intent(inout)  :: spwout
  integer(kind=4), intent(inout)  :: n_last
  integer(kind=4), intent(inout)  :: spw_last(m_spw)
  logical, intent(inout)            :: error
  ! Local
  integer(kind=4)       :: iw,ir
  integer(kind=4)       :: sort(spwout%n_spw)
  integer(kind=8)       :: lasttime
  !
  !Sort spw by time
  !
  do iw=1,spwout%n_spw
    sort(iw) = iw
  enddo
  !
  call gi0_quicksort_index_with_user_gtge(sort,spwout%n_spw,  &
    sort_spw_time_gt,sort_spw_time_ge,error)
  if (error)  return
  !
  lasttime=spwout%win(sort(spwout%n_spw))%ctime
  !
  n_last=0
  do iw=spwout%n_spw,1,-1
    ir=sort(iw)
    if (spwout%win(ir)%ctime.ne.lasttime.or..not.spwout%win(ir)%flexible) exit
    n_last=n_last+1
    spw_last(n_last)=ir
  enddo
  if (n_last.eq.0) then
    call astro_message(seve%i,'RESET','No more flexible SPW to reset')
    return
  endif
  !
end subroutine noema_last_spw
!
function sort_spw_time_gt(m,l)
  use my_receiver_globals
  !---------------------------------------------------------------------
  ! @ private
  ! Sorting function for noema_last_spw (greater than) [sort by time]
  ! ---
  !  NB: BASED ON THE GLOBAL SPW STRUCTURE SHARED THROUGH MODULE
  !      MY_RECEIVER_GLOBALS!!!
  !---------------------------------------------------------------------
  logical :: sort_spw_time_gt
  integer(kind=4), intent(in) :: m,l
  !
  ! Sort by creation time
  sort_spw_time_gt = noema%cfebe%spw%out%win(m)%ctime.gt.noema%cfebe%spw%out%win(l)%ctime
  !
end function sort_spw_time_gt
!
function sort_spw_time_ge(m,l)
  use my_receiver_globals
  !---------------------------------------------------------------------
  ! @ private
  ! Sorting function for noema_sort_spw (greater or equal)
  ! ---
  !  NB: THIS SORTS THE GLOBAL SPW STRUCTURE SHARED THROUGH MODULE
  !      MY_RECEIVER_GLOBALS!!!
  !---------------------------------------------------------------------
  logical :: sort_spw_time_ge
  integer(kind=4), intent(in) :: m,l
  !
  ! Sort by creation time
  sort_spw_time_ge = noema%cfebe%spw%out%win(m)%ctime.ge.noema%cfebe%spw%out%win(l)%ctime
  !
end function sort_spw_time_ge
!
subroutine pfx_reset_unit(pfxu,spwout,error)
  use astro_interfaces, except_this=>pfx_reset_unit
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! defines polyfix backends
  !-----------------------------------------------------------------------
  type(pfx_unit_t), intent(inout)    :: pfxu
  type(spw_output_t), intent(inout)  :: spwout
  logical, intent(inout)            :: error
  ! Local
  integer(kind=4)       :: j,k,ich
  !
  ! First pfx unit characteristics
  do j=1,pfxu%n_modes
    do k=1,pfxu%mode(j)%n_types
      pfxu%mode(j)%chtype(k)%usage = 0
      do ich=1,m_ch
        pfxu%mode(j)%chtype(k)%chunks(ich) = 0
      enddo
    enddo
  enddo
  ! Then associated spectral windows
  do j=1,m_spw
    if (spwout%win(j)%label.ne.pfxu%label) cycle
    call noema_null_spw(spwout%win(j),error)
    if (error) return
  enddo
  !
end subroutine pfx_reset_unit
!
subroutine noema_reset_spw(pfx,spwin,error)
  use astro_interfaces, except_this=>noema_reset_spw
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! reset a spw and free the associated chunks in pfx corresponding unit
  !-----------------------------------------------------------------------
  type(pfx_t), intent(inout)    :: pfx
  type(spw_unit_t), intent(inout)  :: spwin
  logical, intent(inout)            :: error
  ! Local
  character(len=*), parameter :: rname='RESET'
  integer(kind=4)       :: i,k,ich,ic1,ic2,imode
  !
  ic1=spwin%ich1
  ic2=spwin%ich2
  !
  ! Find and free the chunks related to the resetted spw
  do i=1,pfx%n_units
    if (pfx%unit(i)%label.ne.spwin%label) cycle
    imode=pfx%unit(i)%imode
    do k=1,pfx%unit(i)%mode(imode)%n_types
      if (.not.(pfx%unit(i)%mode(imode)%chtype(k)%move_chunk)) cycle
      do ich=ic1,ic2
        pfx%unit(i)%mode(imode)%chtype(k)%chunks(ich)=pfx%unit(i)%mode(imode)%chtype(k)%chunks(ich)-1
        if (pfx%unit(i)%mode(imode)%chtype(k)%chunks(ich).eq.0) then
          pfx%unit(i)%mode(imode)%chtype(k)%usage = pfx%unit(i)%mode(imode)%chtype(k)%usage-1
        endif
      enddo !chunks
    enddo  !types
  enddo !pfx units
  !
  !Then reset spw attributes
  call noema_null_spw(spwin,error)
  if (error) return
  !
end subroutine noema_reset_spw
!
subroutine noema_null_spw(win,error)
  use astro_interfaces, except_this=>noema_null_spw
  use astro_types
  !------------------------------------------------------------------------
  ! @ private
  ! put all attributes of a spectral window to undefined
  !------------------------------------------------------------------------
  type(spw_unit_t), intent(inout)       :: win
  logical, intent(inout)                :: error
  !
  win%restmin = undef_freq
  win%restmax = -1d0
  win%ich2 = -1
  win%ich1 = -1
  win%resol = -1.0
  win%iband = -1
  win%sb_code = -1
  win%bb_code = -1
  win%pol_code = -1
  win%label = ''
  win%user_label = ''
  win%flexible = .false.
  win%ctime = -1
 !
end subroutine noema_null_spw
!
subroutine noema_reset_backend(pfx,spwout,error)
  use astro_interfaces, except_this=>noema_reset_backend
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! defines polyfix backends
  !-----------------------------------------------------------------------
  type(pfx_t), intent(inout)    :: pfx
  type(spw_output_t)            :: spwout
  logical, intent(inout)            :: error
  ! Local
  integer(kind=4)       :: i
  !
  ! Reset polyfix type settings
  do i=1,pfx%n_units
    pfx%unit(i)%iband = 0
    pfx%unit(i)%sb_code = 0
    pfx%unit(i)%bb_code = 0
    pfx%unit(i)%pol_code = 0
    pfx%unit(i)%label = ''
    pfx%unit(i)%imode = -1
    call pfx_reset_unit(pfx%unit(i),spwout,error)
  enddo
  !
  ! Reset spectral window output information
  spwout%n_spw=0
  !
end subroutine noema_reset_backend
!
subroutine noema_compress_spw(spwout,error)
  use astro_interfaces, except_this=>noema_compress_spw
  use astro_types
  !---------------------------------------------------------------------
  ! @ private
  ! Compress spw list (to remove spwindows that have been reset)
  !---------------------------------------------------------------------
  type(spw_output_t), intent(inout) :: spwout
  logical,            intent(inout) :: error
  ! Local
  integer(kind=4) :: ispw,jspw
  !
  jspw = 0
  do ispw=1,spwout%n_spw
    if (spwout%win(ispw)%restmin.eq.undef_freq) then
      cycle
    else
      jspw = jspw+1
    endif
    if (ispw.ne.jspw) then
      call noema_copy_spw(spwout%win(ispw),spwout%win(jspw),error)
      if (error)  return
    endif
  enddo ! ispw
  !
  spwout%n_spw = jspw
  !
end subroutine noema_compress_spw
!
subroutine noema_copy_spw(in,out,error)
  use astro_interfaces, except_this=>noema_copy_spw
  use astro_types
  !---------------------------------------------------------------------
  ! @ private
  ! Copy a spw_unit_t into another one
  !---------------------------------------------------------------------
  type(spw_unit_t), intent(in)    :: in
  type(spw_unit_t), intent(out)   :: out
  logical,          intent(inout) :: error
  !
  out = in
  !
end subroutine noema_copy_spw
!
subroutine noema_check_conflicts(rname,spw,pfx,error)
  use gbl_message
  use gbl_ansicodes
  use astro_interfaces, except_this=>noema_check_conflicts
  use astro_types
  !---------------------------------------------------------------------
  ! @ private
  ! Copy a spw_unit_t into another one
  !---------------------------------------------------------------------
  character(len=*), intent(in)          :: rname
  type(spw_output_t), intent(inout)    :: spw
  type(pfx_t), intent(in)       :: pfx
  logical, intent(inout)   :: error
  ! Local
  integer(kind=4)        :: ispw,iu,imode,it,ic1,ic2
  character(len=256)    :: mess
  !
  do ispw=1,spw%n_spw ! look for spw involved in the conflict
    do iu=1,pfx%n_units ! correlator units
      imode=pfx%unit(iu)%imode
      if (spw%win(ispw)%label.ne.pfx%unit(iu)%label) cycle ! spw is not in the current unit
      ic1=spw%win(ispw)%ich1
      ic2=spw%win(ispw)%ich2
      do it=1,pfx%unit(iu)%mode(imode)%n_types ! correlator types
        if (pfx%unit(iu)%mode(imode)%chtype(it)%df_chunks.ne.spw%win(ispw)%resol/1d3) cycle
        ! Chunk used more than once
        if (any(pfx%unit(iu)%mode(imode)%chtype(it)%chunks(ic1:ic2).gt.1)) then
          spw%win(ispw)%conflict = .true.
          write (mess,'(a,i0,1x,a)') 'SPW #',ispw,'uses conflicting chunk(s)'
          call astro_message(seve%w,rname,mess)
        else
          spw%win(ispw)%conflict = .false.
        endif
        ! Warning Chunk 1
        if (pfx%unit(iu)%mode(imode)%chtype(it)%move_chunk.and.ic1.eq.1) then
          write (mess,'(a,i0,a)') 'SPW ',ispw, &
                  ' uses CHUNK 1 which might not be usable'
          call astro_message(seve%w,rname,mess)
          spw%win(ispw)%chunk1 = .true.
        else
          spw%win(ispw)%chunk1 = .false.          
        endif
        ! Look for overload
        if (pfx%unit(iu)%mode(imode)%chtype(it)%move_chunk .and. &
            pfx%unit(iu)%mode(imode)%chtype(it)%usage.gt. &
            pfx%unit(iu)%mode(imode)%chtype(it)%use_chunks) then
          spw%win(ispw)%overload = .true.
        else
          spw%win(ispw)%overload = .false.
        endif
      enddo ! chunk types (i.e. resolutions of same mode)
    enddo ! pfx units
  enddo ! defined spectral windows
  !
end subroutine noema_check_conflicts
!
subroutine noema_setup_file(rname,line,doonline,error)
  use gbl_message
  use gbl_ansicodes
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_setup_file
  use astro_types
  use ast_line
  use ast_astro
  use my_receiver_globals
  use plot_molecules_globals
  !-----------------------------------------------------------------------
  ! @ private
  ! Create a sequence of commands to reach the present state
  ! if calling command is SETUP   -->  ONLINE syntax
  ! if calling command is PROPOSAL--> OFFLINE syntax
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  character(len=*), intent(inout) :: line        ! command line
  logical, intent(inout) :: doonline
  logical, intent(inout) :: error
  ! local
  integer(kind=4), parameter :: optfile=1 ! to write output in a file rather than on the screen
  integer(kind=4), parameter :: opttime=2 ! to include computation time in output script
  integer(kind=4), parameter :: optfebe=3 ! to extract setups from the FEBE register
  logical :: dopchanged,dofebe,dofile,dotime,doall
  integer(kind=4) :: nchar,olun,ier,iarg,nfebe,is,ifebe
  integer(kind=4), allocatable :: febe_sel(:)
  character(len=filename_length) :: file,argum
  !
  dofile=sic_present(optfile,0)
  dofebe=sic_present(optfebe,0)
  dotime=sic_present(opttime,0)
  !
  !check that doppler did not changed since last tuning
  call rec_check_doppler(noema%source,noema%recdesc%redshift,dopchanged,error)
  if (dopchanged) then
    call astro_message(seve%e,rname,'Source properties changed since last tuning')
    call astro_message(seve%i,rname,'Nothing done')
    call rec_display_error('Source changed since last tuning',error)
    error = .true.
    return
  endif
  !
  ! Output to STDOUT or file?
  if (dofile) then  ! /FILE
    call sic_ch(line,optfile,1,argum,nchar,.true.,error)
    if (error)  return
    call sic_parse_file(argum,' ','.astro',file)
    !
    ier = sic_getlun(olun) !find a unit number
    if (ier.ne.1) then
      call astro_message(seve%e,rname,'No logical unit left')
      error = .true.
      return
    endif
    ier = sic_open(olun,file,'NEW',.false.)
    if (ier.ne.0) then
      call astro_message(seve%e,rname,'Cannot open file '//file)
      error = .true.
      call sic_frelun(olun)
      return
    endif
  else
    olun = 6  ! STDOUT
  endif
  !
  ! FEBE
  if (dofebe) then
    if (noema%register%n_saved.eq.0) then
      call astro_message(seve%e,rname,'No saved FEBE, no script to create')
      goto 10
    endif
    doall=sic_narg(optfebe).eq.0
    if (doall) then
      !no arg = all febes
      nfebe=noema%register%n_saved
    else
      nfebe=sic_narg(optfebe)
    endif
    allocate(febe_sel(nfebe),stat=ier)
    if (ier.ne.0) then
      call astro_message(seve%e,rname,'Problem allocating febe_sel structure dimensions')
      error=.true.
      return
    endif
    if (doall) then
      ! fill the selection with all the defined febes
      ifebe=0
      do is=1,noema%register%m_saved
        if (.not.noema%register%saved_febe(is)%defined) cycle
        ifebe=ifebe+1
        febe_sel(ifebe)=is
      enddo
    else
      do iarg=1,nfebe
        call sic_i0(line,optfebe,iarg,febe_sel(iarg),.true.,error)
        if (error) return
      enddo
    endif
    ! Run a dedicated subroutine
    call noema_multi_script(rname,noema,nfebe,febe_sel, & 
                          molecules,doonline,dotime,olun,error)
    deallocate(febe_sel)
    if (error) goto 10
  else
    !
    call noema_setup_check(rname,noema%source,noema%cfebe & 
                          ,doonline,error)
    if (error) then
      call astro_message(seve%e,rname,'Problem with current FEBE, script not written')
      error = .true.
      return
    endif
    call noema_setup_source(rname,noema%source,doonline,dotime,olun,error)
    if (error) goto 10
    call noema_setup_print(rname,noema%source,noema%cfebe, & 
                          molecules,doonline,olun,error)
    if (error) goto 10
  endif
  !
  10 continue
  if (olun.ne.6) then
    ier = sic_close(olun)
    call sic_frelun(olun)
  endif
  !
end subroutine noema_setup_file
!
subroutine noema_multi_script(rname,lnoema,nf,self,cata,doonline,dotime,olun,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_multi_script
  use astro_noema_type
  use ast_line
  use ast_astro
  !-----------------------------------------------------------------------
  ! @ private
  ! Prepare the wrting of an astro scritpt ith several febe inside (to input for PMS)
  ! keep OFFLINE mode
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname        ! calling command
  type(noema_t), intent(in)  :: lnoema
  integer(kind=4), intent(in) :: nf
  integer(kind=4), intent(in) :: self(nf)
  type(plot_molecules_t), intent(in) :: cata
  logical, intent(in)   :: doonline
  logical, intent(in)   :: dotime
  integer(kind=4), intent(in) :: olun
  logical, intent(inout) :: error
  ! local
  integer(kind=4) :: is,ifebe
  character(len=256) :: mess
  !
  !
  ! Feasibilitiy + Print header in the file
  call multi_script_header(rname,lnoema,nf,self,olun,error)
  if (error) return
  !
  call noema_setup_source(rname,lnoema%source,doonline,dotime,olun,error)
  if (error) return
  !
  ! Now loop on the selected FEBEs
  do is=1,nf
    ifebe=self(is)
    call noema_setup_check(rname,lnoema%source,lnoema%register%saved_febe(ifebe), & 
                          doonline,error)
    if (error) then
      write (mess,'(a,i0)')   'Problem with FEBE #',ifebe
      call astro_message(seve%e,rname,mess)
      call astro_message(seve%e,rname,'Output script might be uncomplete')
      error = .true.
      return
    endif
    call noema_print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!',olun)
    call noema_print('! BEGIN FEBE',olun)
    call noema_setup_info(rname,lnoema%source,lnoema%register%saved_febe(ifebe),olun,error)
    if (error) return
    call noema_setup_print(rname,lnoema%source,lnoema%register%saved_febe(ifebe), &
                     cata,doonline,olun,error)
    if (error) return
    call noema_print('! END FEBE',olun)
    call noema_print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!',olun)
    call noema_print('FEBE ADD ! Add FEBE to register',olun)
  enddo
  !
end subroutine noema_multi_script
!
subroutine multi_script_header(rname,lnoema,nf,self,ilun,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>multi_script_header
  use astro_noema_type
  use ast_line
  use ast_astro
  !-----------------------------------------------------------------------
  ! @ private
  ! Write the header part of the multi febe script for PMS
  ! 
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname        ! calling command
  type(noema_t), intent(in)  :: lnoema
  integer(kind=4), intent(in) :: nf
  integer(kind=4), intent(in) :: self(nf)
  integer(kind=4), intent(in) :: ilun
  logical, intent(inout) :: error
  ! local
  character(len=256) :: mess
  integer(kind=4) :: is,ib,nbands,bandsel(lnoema%recdesc%n_rbands),nmodes,iu,prevmode,imode
  integer(kind=4) :: tuning_count(lnoema%recdesc%n_rbands)
  logical         :: cycle_multi_mode, dual_band
  ! Feasibility 
  ! Count nb tuning per band
  nbands=0
  do ib=1,lnoema%recdesc%n_rbands
    bandsel(ib)=0
  enddo
  do ib=1,lnoema%recdesc%n_rbands
    tuning_count(ib)=0
    do is=1,nf
      if (.not.lnoema%register%saved_febe(self(is))%defined) cycle
      if (lnoema%register%saved_febe(self(is))%rectune%iband.eq.ib) then
        tuning_count(ib)=tuning_count(ib)+1
      endif
    enddo
    if (tuning_count(ib).gt.0) then
      nbands=nbands+1
      bandsel(nbands)=ib
    endif
  enddo
  !
  ! Check DualBand limitations (only B1 + B3)
  dual_band=.false.
  if (nbands.eq.0) then
    call astro_message(seve%e,rname,'No tuning found !')
    error=.true.
    return    
  else if (nbands.gt.2) then
    call astro_message(seve%e,rname,'At most 2 frequency bands can be used simultaneously')
    error=.true.
    return
  else if (nbands.eq.1) then
    call astro_message(seve%i,rname,'SingleBand')
  else if (nbands.eq.2) then
    if (bandsel(1).eq.2.or.bandsel(2).eq.2) then
      call astro_message(seve%e,rname,'DualBand is available for B1 and B3 combination')
      error=.true.
      return
    endif
    dual_band=.true.
    call astro_message(seve%i,rname,'DualBand')
   endif
  !
  ! Check freq cycling limitation: only in B1 + not combined with dualband +same correlator mode
  do ib=1,nbands
    if (bandsel(ib).ne.1.and.tuning_count(bandsel(ib)).gt.1) then
      call astro_message(seve%e,rname,'Frequency cycling is only available in Band 1')
      error=.true.
      return
    endif
  enddo
  cycle_multi_mode=.false.
  if (tuning_count(1).gt.1) then
    ! Forbid cycling + dual band
    if (dual_band) then
      call astro_message(seve%e,rname,'Cycling cannot be combined with Dual Band')
      error=.true.
      return
    endif
    ! Cycling, check that all units have same mode
    prevmode=-1
    do is=1,nf
      if (.not.lnoema%register%saved_febe(self(is))%defined) cycle
      if (lnoema%register%saved_febe(self(is))%rectune%iband.ne.1) cycle
      do iu=1,lnoema%register%saved_febe(self(is))%pfx%n_units
        imode=lnoema%register%saved_febe(self(is))%pfx%unit(iu)%imode
        if (prevmode.eq.-1.and.imode.gt.0) then
          prevmode=imode
        endif
        if (imode.gt.0.and.prevmode.ne.imode) then
          cycle_multi_mode=.true.
        endif
      enddo
    enddo
  endif
  if (cycle_multi_mode) then
    call astro_message(seve%e,rname,'Frequency cycling must use same mode for all tunings')
    error=.true.
    return
  endif
  !
  ! Print
  !
  call noema_print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!',ilun)
  call noema_print('! BEGIN HEADER',ilun)
  mess='! Selected bands:'
  do ib=1,nbands
    write (mess,'(a,1x,i0)') trim(mess),bandsel(ib)
  enddo
  call noema_print(trim(mess),ilun)
  mess='! Number of tunings per band:'
  do ib=1,nbands
    write (mess,'(a,1x,i0)') trim(mess),tuning_count(bandsel(ib))
  enddo
  call noema_print(trim(mess),ilun)
  call noema_print('! END HEADER',ilun)
  !
end subroutine multi_script_header
!
subroutine noema_setup_info(rname,rsou,lfebe,olun,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_setup_info
  use astro_types
  use ast_line
  use ast_astro
  !-----------------------------------------------------------------------
  ! @ private
  ! get info relative to FEBE and put it in the PMS script
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname        ! calling command
  type(receiver_source_t), intent(in)  :: rsou
  type(noema_febe_t), intent(in)  :: lfebe
  integer(kind=4), intent(in) :: olun
  logical, intent(inout) :: error
  ! local
  type(info_pms_t) :: infopms
  character(len=256) :: mess1,mess2
  integer(kind=4) :: ir
  !
  infopms%frep=lfebe%rectune%frest
  infopms%freplsr=lfebe%rectune%flsr
  infopms%resrep=0
  infopms%tracksharing=.false. ! Not Done yes
  infopms%nresol=0 ! Not done yet
  call noema_default_pms(infopms,error)
  if (error) return
  call noema_info_pms(lfebe%rectune,rsou,lfebe%pfx,lfebe%spw%out,infopms,error)
  if (error) return
  ! 
  !
  call noema_print('! BEGIN INCLUDE_INFO',olun)
    ! Tuning parameters
  write (mess1,'(a,1x,i0)')  '! Tuning Band =',infopms%tuning_band
  call noema_print(trim(mess1),olun)
  write (mess1,'(a,1x,f0.6,1x,a)')  '! Tuning REST Frequency =',infopms%tuning_freq,'GHz'
  call noema_print(trim(mess1),olun)
  write (mess1,'(a,1x,f0.6,1x,a)')  '! Tuning LO Frequency =',infopms%tuning_flo1,'GHz'
  call noema_print(trim(mess1),olun)
  write (mess1,'(a,1x,f0.6,1x,a)')  '! Tuning MIN RF Frequency =',infopms%tuning_minrf,'GHz'
  call noema_print(trim(mess1),olun)
  write (mess1,'(a,1x,f0.6,1x,a)')  '! Tuning MAX RF Frequency =',infopms%tuning_maxrf,'GHz'
  call noema_print(trim(mess1),olun)
  !
  if (infopms%tuning_ongrid) then
    mess1='! Tuning ON grid'
  else
    mess1='! Tuning OFF grid'
  endif
  call noema_print(trim(mess1),olun)
  !
  if (infopms%tuning_outlo) then
    mess1='! W-OMS LO tuning frequency is out of recommended range'
    call noema_print(trim(mess1),olun)
  endif
   ! Warning if Chunk 1 is used
  if (infopms%chunk1) then
    mess1='! W-OMS Setup uses chunk #1. Might not be possible'
    call noema_print(trim(mess1),olun)
  endif
  ! LSR/z ranges
  if (infopms%redshift) then
    write (mess1,'(a,1x,f0.3,1x,a)') '! Half the most narrow SPW is equivalent to an offset of', &
                        infopms%zrange,'in redshift'
  else
    write (mess1,'(a,1x,f0.3,1x,a)') '! Half the most narrow SPW is equivalent to an offset of', &
                        infopms%lsrrange,'km/s in LSR velocity'
  endif
  call noema_print(mess1,olun)
  ! Correlator usage
  if (infopms%mflex.ne.0) then
    write (mess1,'(a,1x,i0,a)') '! Flexible part of the correlator is used at',nint(100.*infopms%nflex/infopms%mflex),'%'
    call noema_print(trim(mess1),olun)
  endif
 
  ! Continuum part
  write (mess1,'(a,1x,f0.6,1x,a)')  &
                '! Representative REST frequency for continuum =',infopms%fcont*ghzpermhz,'GHz'
  call noema_print(trim(mess1),olun)
  write (mess1,'(a,1x,f0.6,1x,a)')  &
                '! Representative LSR frequency for continuum =',infopms%fcontlsr*ghzpermhz,'GHz'
  call noema_print(trim(mess1),olun)
  if (oms_bugw20) then
    ! Special case for SMS W20 session - special continuum frequency
    ! used in the W20 (v6) engine of PMS
    write (mess1,'(a,1x,f0.6,1x,a)')  &
                  '! SMSW20 Representative LSR frequency for continuumSMSW20 =',infopms%fcontw20*ghzpermhz,'GHz'
    call noema_print(trim(mess1),olun)
  endif
  write (mess1,'(a,i0,1x,a,1x,f0.6,1x,a)') &
                '! Total bandwidth for continuum (',infopms%npolcont,' polar) =',infopms%dfcont,'MHz'
  call noema_print(trim(mess1),olun)
  write (mess1,'(a,1x,f0.6,1x,a)') &
               '! Channel spacing for continuum =',infopms%rescont,'MHz'
  call noema_print(trim(mess1),olun)

  ! Representative frequency
  ! XX do we have it at this stage
  if (infopms%nresol.gt.0) then
    ! XX NOT TESTED YET
    write (mess1,'(a,1x,f0.6,1x,a,1x,i0,1x,a)') &
                  '! Representative Frequency =',infopms%frep, &
                  'GHz covered by',infopms%nspw,' spectral windows'
    call noema_print(mess1,olun)
    do ir=1,infopms%nresol
      write (mess1,'(a,i0,1x,a,1x,f0.1,1x,a)') &
          '! ',infopms%npol(ir), &
          'polarizations at hardware df=',infopms%resol(ir),'kHz'
      call noema_print(trim(mess1),olun)
    enddo
  endif
  if (infopms%resrep.ne.0) then
    write (mess1,'(a,i0,1x,a,1x,f0.1,1x,a)') &
          '! ',infopms%npolrep, &
          'polarizations at requested df=',infopms%resrep,'kHz'
    call noema_print(trim(mess1),olun)
  endif
  ! LSR freq for Tsys and sensitivity
  write (mess1,'(a,1x,f0.3,1x,a)') &
                '! LSR Representative Frequency =',infopms%freplsr,'GHz'
  call noema_print(mess1,olun)
  ! Sideband limits
  do ir=1,m_sideband
    write (mess1,'(a,a,1x,a,1x,f0.3,1x,a,1x,f0.3,1x,a)') &
                  '! ',sideband(ir),'LSR Min Max =',infopms%lsrlim(ir,1),'-',infopms%lsrlim(ir,2),'GHz'
    call noema_print(mess1,olun)
  enddo
  ! Tracksharing case
  if (infopms%tracksharing) then
    ! NOT TESTED - MIGHT NOT WORK
    ! Min
    if (infopms%redshift) then
      write (mess1,'(a)')  'redshift'
    else
      write (mess1,'(a)')  'VLSR'
    endif
    write (mess2,'(a,1x,a,1x,a,1x,i0,1x,a)') & 
          '! Track-sharing: Freq offset for source with lowest', &
          trim(mess1),'=',nint(infopms%tsdf(1)),'MHz'
    call noema_print(mess2,olun)
    ! Max
    write (mess2,'(a,1x,a,1x,a,1x,i0,1x,a)') & 
          '! Track-sharing: Freq offset for source with highest', & 
          trim(mess1),'=',nint(infopms%tsdf(2)),'MHz'
    call noema_print(mess2,olun)
    ! Case when TUNING or REPRESENTATIV gets out of ranges
    if (infopms%ts_freqout) then
      write (mess1,'(a)') "! Track-sharing: offset between sources might push lines out of range"
      call noema_print(mess1,olun)
    endif
  endif
  call noema_print('! END INCLUDE_INFO',olun)
  !
end subroutine noema_setup_info
!
subroutine noema_setup_check(rname,rsou,lfebe,doonline,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_setup_check
  use astro_types
  use ast_line
  use ast_astro
  !-----------------------------------------------------------------------
  ! @ private
  ! Check Febe before writing it
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname        ! calling command
  type(receiver_source_t), intent(in)  :: rsou
  type(noema_febe_t), intent(in)  :: lfebe
  logical, intent(in)   :: doonline
  logical, intent(inout) :: error
  ! local
  logical :: badsetup,chunk1,modedef,alloff,do250,do2000
  integer(kind=4) :: conflict,overload,iu
  character(len=256) :: mess
  !
  !Preliminary checks
  if (.not.lfebe%defined) then
    call astro_message(seve%e,rname,'No tuning found')
    error = .true.
    return
  endif
  if (.not.lfebe%i_f%ifproc%defined) then
    call astro_message(seve%e,rname,'Problem with IF Processor part')
    error = .true.
    return
  endif
  ! Check and stop if 250kHz mode requested (warning)
  do250=.false.
  do2000=.false.
  do iu=1,lfebe%pfx%n_units
    if (lfebe%pfx%unit(iu)%imode.eq.3) then
      do250=.true.
    else if (lfebe%pfx%unit(iu)%imode.eq.1) then
      do2000=.true.
    endif
  enddo
  if (do250.and.do2000) then
    call astro_message(seve%e,rname,"First implementation of 250kHz mode does not allow mixed correlator configuration")
    call astro_message(seve%e,rname,"Same mode should be used for all basebands")
    error=.true. 
    return
  endif
  !
  if (.not.doonline) then
    ! Checks:
    ! if no unit ON do not create the script
    modedef=.false.
    do iu=1,lfebe%pfx%n_units
      if (lfebe%pfx%unit(iu)%imode.eq.-1) cycle
      modedef=.true.
    enddo
    if (.not.modedef) then
      call astro_message(seve%e,rname,'PolyFix Unit Modes are not defined: use BASEBAND command first')
      error = .true.
      return
    endif
  endif
  ! Check status of pfx (overload or conflict)
  chunk1=.false.
  call noema_pfx_status(lfebe%pfx,overload,conflict,chunk1,alloff,error)
  if (error) return
  if (alloff) then
    call astro_message(seve%e,rname,'Backend is not defined')
    error=.true.
    return
  endif
  badsetup=.false.
  if (overload.gt.0) then
    badsetup=.true.
    call astro_message(seve%e,rname,'Setup requires more chunks than available')
  endif
  if (conflict.gt.0) then
    badsetup=.true.
    write (mess,'(i0,1x,a)') conflict,'Chunks used by more than one spectral window'
    call astro_message(seve%e,rname,mess)
  endif
  if (badsetup) then
    call astro_message(seve%e,rname,'Please solve conflicts before creating the procedure')
    error=.true.
    return
  endif
  !
  if (chunk1) then
    call astro_message(seve%w,rname, & 
        'Configuration uses Chunk 1. Might not be feasible.')
  endif
  !
  !
  if (doonline.and. &
     (rsou%sourcetype.eq.soukind_vlsr.or.rsou%sourcetype.eq.soukind_red)) then
    write (mess,'(a,1x,a,1x,a)') & 
          trim(rname),'command needs a fully defined source - incompatible with SOURCE /DOPPLER option'
    call astro_message(seve%e,rname,mess)
    error=.true.
    return
  endif
  !
  if (lfebe%rectune%outlo) then
    call astro_message(seve%w,rname,'LO out of recommended range. Might not be feasible.')
  endif
  !
end subroutine noema_setup_check
!
subroutine noema_setup_print(rname,rsou,lfebe,cata,doonline,olun,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_setup_print
  use astro_types
  use ast_line
  use ast_astro
  !-----------------------------------------------------------------------
  ! @ private
  ! Create a sequence of commands to reach the input state based only on SPW /CH
  ! keep OFFLINE mode
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname        ! calling command
  type(receiver_source_t), intent(in)  :: rsou
  type(noema_febe_t), intent(in)  :: lfebe
  type(plot_molecules_t), intent(in) :: cata
  logical, intent(in)   :: doonline
  integer(kind=4), intent(in) :: olun
  logical, intent(inout) :: error
  ! local
  integer(kind=4) :: ncm,im,kmol
  character(len=256) :: mess
  character(len=128) :: molfile,molfile2
  character(len=32), allocatable  :: curr_mol(:,:)
  character(len=1)      :: dummy
  !
  call noema_print('! BEGIN INCLUDE_SETUP',olun)
  call noema_setup_userpref(rname,cata,mess,error)
  if (error) return
  call noema_print(mess,olun)
  ! TUNING command
  call noema_setup_rec(rsou,lfebe%rectune,doonline,olun,error)
  if (error) return
  ! BASEBANDS and SPW  commands
  call noema_setup_pfx(lfebe%rectune,lfebe%pfx,lfebe%spw%out,doonline,olun,error)
  if (error) return
  call noema_print('! END INCLUDE_SETUP',olun)
  !
  ! Extract current lines from catalog
  ! Load molecular line [taken from older line versions]
  if (.not.sic_query_file(cata%catalog,'data#dir:','.dat',molfile)) then
    call astro_message(seve%e,rname,'line catalog file not found')
    error=.true.
    return
  endif
  kmol = 0
  call read_lines(dummy,kmol,molfile)
  allocate(curr_mol(2,nmol))
  !
  call noema_setup_cata(lfebe%spw%out,cata,curr_mol,ncm,error)
  if (error) return
  !
  ! Write lines in data section of setup file
  molfile2='setup_molecules.lin'
  write (mess,'(a,a)') 'BEGIN DATA GAG_SCRATCH:',trim(molfile2)
  call noema_print(mess,olun)
  call noema_print('! Known spectral lines in the covered frequency ranges',olun)
  do im=1,ncm
    write (mess,'(a,1x,a,a,a)') trim(curr_mol(1,im)),"'",trim(curr_mol(2,im)),"'"
    call noema_print(mess,olun)
  enddo
  write (mess,'(a,a)') 'END DATA GAG_SCRATCH:',trim(molfile2)
  call noema_print(mess,olun)
  !
  deallocate(curr_mol)
  !
  !
end subroutine noema_setup_print
!
subroutine noema_setup_userpref(rname,cata,chain,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_setup_userpref
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! extract get user configuration for the molecular line plots
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(plot_molecules_t), intent(in)  :: cata
  character(len=*), intent(inout) :: chain
  logical,  intent(inout) :: error
  !
  if (cata%width.gt.1d-6) then
    write (chain,'(a,1x,a,1x,f0.3)') 'SET LINES',trim(cata%profile),cata%width
  else
    write (chain,'(a)') 'SET LINES MARKER'
  endif

  !
end subroutine noema_setup_userpref
!
subroutine noema_setup_cata(spwout,molecules,mlist,ncm,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_setup_cata
  use astro_types
  use ast_line
  !-----------------------------------------------------------------------
  ! @ private
  ! extract from the line catalog the lines falling in the current spw
  !
  !-----------------------------------------------------------------------
  type(spw_output_t), intent(in)  :: spwout
  type(plot_molecules_t), intent(in)  :: molecules
  character(len=*), intent(inout) :: mlist(:,:)
  integer(kind=4), intent(inout) :: ncm
  logical,  intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SETUP'
  integer(kind=4) :: im,iw,ic
  real(kind=8) :: mfreq
  !
  ic=0
  do im=1,nmol
    mfreq=molfreq(im)*mhzperghz
    do iw=1,spwout%n_spw
      if (mfreq.lt.spwout%win(iw)%restmin-molecules%width.or. &
          mfreq.gt.spwout%win(iw)%restmax+molecules%width) cycle
      ic=ic+1
      write (mlist(1,ic),'(f0.6)') molfreq(im)
      mlist(2,ic)=molname(im)
      exit ! molecule in list
    enddo ! spw
  enddo ! molecules
  ncm=ic
  !
end subroutine noema_setup_cata
!
subroutine noema_setup_source(rname,recsou,doonline,dotime,olun,error)
  use gbl_message
  use gbl_constant
  use astro_interfaces, except_this=>noema_setup_source
  use ast_astro
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! create the sourcecommand
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(receiver_source_t), intent(in)  :: recsou
  logical,  intent(in) :: doonline
  logical,  intent(in) :: dotime
  integer(kind=4), intent(in) :: olun
  logical,  intent(inout) :: error
  ! Local
  character(len=24) :: datechain
  character(len=128) :: mess1,mess
  !
  ! Inlcude tag for OMS parsing
  call noema_print('! BEGIN INCLUDE_INIT',olun)
  !
  if (doonline.and.recsou%z.ne.0) then
    call noema_print('! Source defined without z and use redshifted tuning frequency',olun)
  endif
  if (dotime) then
    if (doonline) then
      call astro_message(seve%e,rname,'/TIME option should not be used in online mode')
      error=.true.
      return
    endif
    call jdate_to_datetime(jnow_utc,datechain,error)
    if (error) return
    write (mess,'(a,1x,a,1x,a)') 'TIME',datechain(13:24),datechain(1:11)
    call noema_print(mess,olun)
  endif
  !
  if (recsou%sourcetype.eq.soukind_full) then
    write (mess1,'(a,1x,a,1x,a)') 'SOURCE',trim(recsou%name),trim(recsou%coord)
    if (recsou%eq.ne.equinox_null) then
      write (mess1,'(a,1x,f0.3)') trim(mess1),recsou%eq
    endif
    write (mess1,'(a,1x,a,1x,a)') trim(mess1),trim(recsou%lambda),trim(recsou%beta)
    if (recsou%invtype.eq.'RE'.and.(.not.doonline)) then
      write (mess,'(a,1x,a,1x,f0.6)') trim(mess1),'RED',recsou%z
    else
      write (mess,'(a,1x,a,1x,f0.3)') trim(mess1),'LSR',recsou%vlsr
    endif
  else if (recsou%sourcetype.eq.soukind_red.or.recsou%sourcetype.eq.soukind_vlsr) then
    if (doonline) then
      write (mess,'(a)') '! Command SETUP requires fully defined source'
      call astro_message(seve%e,rname,'mess')
      error=.true.
      return
    else
      if (recsou%sourcetype.eq.soukind_vlsr) then
        write (mess,'(a,1x,a,1x,f0.3)') 'SOURCE /DOPPLER',trim(soukinds(recsou%sourcetype)),recsou%vlsr
      else if (recsou%sourcetype.eq.soukind_red) then
        write (mess,'(a,1x,a,1x,f0.6)') 'SOURCE /DOPPLER',trim(soukinds(recsou%sourcetype)),recsou%z
      endif
    endif
  else if (recsou%sourcetype.eq.soukind_none) then
    write (mess,'(a)') 'SOURCE /RESET ! No source entered'
  else
    write (mess1,'(a,1x,i0)') 'Unvalid source type:',recsou%sourcetype
    call astro_message(seve%e,rname,mess1)
    error=.true.
    return
  endif
  !
  call noema_print(mess,olun)
  !
  ! Inlcude tag for OMS parsing
  call noema_print('! END INCLUDE_INIT',olun)
  !
end subroutine noema_setup_source
!
subroutine noema_setup_rec(rsou,rtune,doonline,olun,error)
  use gbl_message
  use astro_interfaces, except_this=>noema_setup_rec
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! create the tuning command
  !-----------------------------------------------------------------------
  type(receiver_source_t), intent(in)  :: rsou
  type(receiver_tune_t), intent(in)  :: rtune
  logical,  intent(in) :: doonline
  integer(kind=4), intent(in) :: olun
  logical,  intent(inout) :: error
  ! Local
  real(kind=8) :: fcomm
  character(len=256) :: mess,tname*12
  !
  if (doonline) then
    ! ONLINE
    if (rsou%z.ne.0) then
      fcomm=rtune%flsr*ghzpermhz
    else
      fcomm=rtune%frest*ghzpermhz    
    endif
    !     rtune%frest/(1+rsou%z)/1d3
    if (rtune%name.eq.'') then
      write (tname,'(i0,a)') nint(fcomm),sideband(rtune%sb_code)
    else
      tname=rtune%name
    endif
    write (mess,'(a,1x,a,1x,f0.6,1x,a,1x,f0.3,1x,a,1x,i0)') &
      'LINE',trim(tname),fcomm,trim(sideband(rtune%sb_code)), &
      rtune%fcent,'/RECEIVER',rtune%iband
    ! Add redshift info
    if (rsou%z.ne.0) then
      write (mess,'(a,a,f0.6)') trim(mess),' ! z=',rsou%z
    endif
  else
    ! OFFLINE
    write (mess,'(a)') 'SET FREQUENCY REST LSR'
    call noema_print(mess,olun)
    fcomm=rtune%frest/1d3
    write (mess,'(a,1x,f0.6,1x,a,1x,f0.3)') &
      'TUNING',fcomm,trim(sideband(rtune%sb_code)),rtune%fcent
    if (.not.rtune%ongrid) then
       write (mess,'(a,1x,a)') trim(mess),'/FIXED_FREQ'
    endif
  endif
  ! print
  call noema_print(mess,olun)
  !
end subroutine noema_setup_rec
!
subroutine noema_setup_pfx(rtune,pfx,spwout,doonline,olun,error)
  use gbl_message
  use astro_interfaces, except_this=>noema_setup_pfx
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! create the BASEBAND and SPW commands
  !
  !-----------------------------------------------------------------------
  type(receiver_tune_t), intent(in)  :: rtune
  type(pfx_t), intent(in) :: pfx
  type(spw_output_t), intent(in) :: spwout
  logical,  intent(in) :: doonline
  integer(kind=4), intent(in) :: olun
  logical,  intent(inout) :: error
  ! Local
  integer(kind=8) :: iu,ilab,ntyp,imode,ispw,it
  character(len=256) :: mess,messres
  !
  if (doonline) then
    ! ONLINE syntax: first all BB then all SPW
    call noema_setup_bb(rtune,pfx,spwout,doonline,olun,error)
    if (error) return
    ! SPW commands
    call noema_setup_spw(rtune,pfx,spwout,doonline,olun,error)
    if (error) return
  else
    ! OFFLINE SYNTAX
    ! BASEBAND and SPW grouped by pfx units
    ilab=1
    do iu=1,pfx%n_units
      if (pfx%unit(iu)%iband.ne.rtune%iband) cycle
      if (.not.(any(spwout%win(1:spwout%n_spw)%label.eq.pfx%unit(iu)%label))) then
        ! Baseband OFF
        write (mess,'(a,1x,a,1x,a)') 'BASEBAND',pfx%unit(iu)%label(ilab:5),'OFF'
        call noema_print(mess,olun)
      else
        ! Baseband mode
        imode=pfx%unit(iu)%imode
        ntyp=pfx%unit(iu)%mode(imode)%n_types
        messres=''
        do it=1,ntyp
          if (imode.eq.3.and.it.eq.1) cycle ! exception for 250kHz typ 1 is pseudo 2MHz
          write (messres,'(a,1x,f0.2)') trim(messres),pfx%unit(iu)%mode(imode)%chtype(it)%df_chunks*1d3
        enddo
        write (mess,'(a,1x,a,1x,a,1x,a)') 'BASEBAND',pfx%unit(iu)%label(ilab:5),'/MODE',trim(messres)
        call noema_print(mess,olun)
        ! ALL SPW
        do ispw=1,spwout%n_spw
          if (spwout%win(ispw)%label.ne.pfx%unit(iu)%label) cycle
          if (.not.spwout%win(ispw)%flexible) cycle
          write (mess,'(a,1x,i0,1x,a,1x,i0)') 'SPW /CHUNK', &
              spwout%win(ispw)%ich1,'TO',spwout%win(ispw)%ich2
          if (spwout%win(ispw)%user_label.ne.'') then
            write (mess,'(a,1x,a,1x,a)') trim(mess),'/LABEL',trim(spwout%win(ispw)%user_label)
          endif
          call noema_print(mess,olun)
        enddo !spw
      endif
    enddo
  endif
  !
end subroutine noema_setup_pfx
!
subroutine noema_setup_bb(rtune,pfx,spwout,doonline,olun,error)
  use gbl_message
  use astro_interfaces, except_this=>noema_setup_bb
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! create baseband commands
  !
  !-----------------------------------------------------------------------
  type(receiver_tune_t), intent(in)  :: rtune
  type(pfx_t), intent(in) :: pfx
  type(spw_output_t), intent(in) :: spwout
  logical,  intent(in) :: doonline
  integer(kind=4), intent(in) :: olun
  logical,  intent(inout) :: error
  ! Local
  integer(kind=8) :: iu,ilab,ntyp,imode
  character(len=256) :: mess
  !
  if (doonline) then
    ! ONLINE syntax
    ilab=3
    do iu=1,pfx%n_units
      if (pfx%unit(iu)%iband.ne.rtune%iband) cycle
      if (.not.(any(spwout%win(1:spwout%n_spw)%label.eq.pfx%unit(iu)%label))) then
        ! Baseband OFF
        write (mess,'(a,1x,a,1x,a,1x,i0)') 'BASEBAND',pfx%unit(iu)%label(ilab:5), &
                                           'OFF /RECEIVER',pfx%unit(iu)%iband
      else
        ! Baseband mode
        write (mess,'(a,1x,a,1x,i0,1x,a,1x,i0)') 'BASEBAND',pfx%unit(iu)%label(ilab:5),pfx%unit(iu)%imode, &
              '/RECEIVER',pfx%unit(iu)%iband
      endif
      call noema_print(mess,olun)
    enddo
  else
    ! OFFLINE SYNTAX
    ilab=1
    do iu=1,pfx%n_units
      if (pfx%unit(iu)%iband.ne.rtune%iband) cycle
      if (.not.(any(spwout%win(1:spwout%n_spw)%label.eq.pfx%unit(iu)%label))) then
        ! Baseband OFF
        write (mess,'(a,1x,a,1x,a)') 'BASEBAND',pfx%unit(iu)%label(ilab:5),'OFF'
      else
        ! Baseband mode
        imode=pfx%unit(iu)%imode
        ntyp=pfx%unit(iu)%mode(imode)%n_types
        write (mess,'(a,1x,a,1x,f0.1)') 'BASEBAND',pfx%unit(iu)%label(ilab:5),pfx%unit(iu)%mode(imode)%chtype(ntyp)%df_chunks*1d3
      endif
      call noema_print(mess,olun)
    enddo
  endif
  !
end subroutine noema_setup_bb
!
subroutine noema_setup_spw(rtune,pfx,spwout,doonline,olun,error)
  use gbl_message
  use astro_interfaces, except_this=>noema_setup_spw
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! create spw commands
  !
  !-----------------------------------------------------------------------
  type(receiver_tune_t), intent(in)  :: rtune
  type(pfx_t), intent(in) :: pfx
  type(spw_output_t), intent(in) :: spwout
  logical,  intent(in) :: doonline
  integer(kind=4), intent(in) :: olun
  logical,  intent(inout) :: error
  ! Local
  integer(kind=8) :: iu,ispw
  character(len=256) :: mess
  !
  if (doonline) then
    ! ONLINE SYNTAX
    do iu=1,pfx%n_units
      if (pfx%unit(iu)%iband.ne.rtune%iband) cycle
      if (.not.(any(spwout%win(1:spwout%n_spw)%label.eq.pfx%unit(iu)%label))) cycle
      do ispw=1,spwout%n_spw
        !SPW COMMAND
        if (spwout%win(ispw)%label.ne.pfx%unit(iu)%label) cycle
        if (.not.spwout%win(ispw)%flexible) cycle
        if (spwout%win(ispw)%ich1.eq.spwout%win(ispw)%ich2) then
          write (mess,'(a,1x,i0,1x,a,1x,a,1x,a,1x,i0)') & 
                        'SPW /CHUNK', spwout%win(ispw)%ich1,'/BASEBAND', &
                        spwout%win(ispw)%label(3:5),'/RECEIVER',pfx%unit(iu)%iband
        else
          write (mess,'(a,1x,i0,1x,a,1x,i0,1x,a,1x,a,1x,a,1x,i0)') 'SPW /CHUNK', &
              spwout%win(ispw)%ich1,'TO',spwout%win(ispw)%ich2, &
              '/BASEBAND',spwout%win(ispw)%label(3:5),'/RECEIVER',pfx%unit(iu)%iband
        endif
        if (spwout%win(ispw)%user_label.ne.'') then
          write (mess,'(a,1x,a,1x,a)') trim(mess),'/LABEL',trim(spwout%win(ispw)%user_label)
        endif
        call noema_print(mess,olun)
      enddo !spw
    enddo !basebands
  else
    ! OFFLINE SYNTAX
    do iu=1,pfx%n_units
      if (pfx%unit(iu)%iband.ne.rtune%iband) cycle
      if (.not.(any(spwout%win(1:spwout%n_spw)%label.eq.pfx%unit(iu)%label))) cycle
      do ispw=1,spwout%n_spw
        !SPW COMMAND
        if (spwout%win(ispw)%label.ne.pfx%unit(iu)%label) cycle
        if (.not.spwout%win(ispw)%flexible) cycle
        write (mess,'(a,1x,i0,1x,a,1x,i0)') 'SPW /CHUNK', &
            spwout%win(ispw)%ich1,'TO',spwout%win(ispw)%ich2
        if (spwout%win(ispw)%user_label.ne.'') then
          write (mess,'(a,1x,a,1x,a)') trim(mess),'/LABEL',trim(spwout%win(ispw)%user_label)
        endif
        call noema_print(mess,olun)
      enddo !spw
    enddo !basebands
  endif
  !
end subroutine noema_setup_spw
!
subroutine noema_print(message,olun)
  use gbl_message
  use astro_interfaces, except_this=>noema_print
  !-----------------------------------------------------------------------
  ! @ private
  ! send message to the given logical unit
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: message
  integer(kind=4),  intent(in) :: olun
  ! Local
  character(len=*), parameter :: rname='SETUP'
  !
  if (olun.eq.6) then
    call astro_message(seve%r,rname,message)
  else
    write(olun,'(A)') trim(message)
  endif
  !
end subroutine noema_print
!
subroutine noema_info_pms(rtune,rsou,pfx,spwout,pms,error)
  use gbl_message
  use astro_interfaces, except_this=>noema_info_pms
  use astro_types
  use ast_astro
  !-----------------------------------------------------------------------
  ! @ private
  ! Fill the structure containing info
  ! to be passed to Proposal Management System
  !-----------------------------------------------------------------------
  type(receiver_tune_t), intent(in)      :: rtune
  type(receiver_source_t), intent(in)      :: rsou
  type(pfx_t), intent(in)    :: pfx
  type(spw_output_t), intent(in)    :: spwout
  type(info_pms_t), intent(inout)   :: pms
  logical, intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='PLOT'
  integer(kind=4) :: iu,imode,it,iw,is
  integer(kind=4), parameter :: lowrestype=1 ! low res always in type=1
  real(kind=8) :: dfconth,dfcontv,flsr,maxrest,minrest
  !
  !Tuning
  pms%tuning_band=rtune%iband
  pms%tuning_freq=rtune%frest*ghzpermhz
  pms%tuning_flo1=rtune%flo1*ghzpermhz
  pms%tuning_ongrid=rtune%ongrid
  pms%tuning_outlo=rtune%outlo
  if (rsou%z.ne.0) then
    pms%redshift=.true.
  endif
  !
  ! Get min max in LSR
  pms%lsrlim(1,1) = 1d11
  pms%lsrlim(1,2) = 0d0
  pms%lsrlim(2,1) = 1d11
  pms%lsrlim(2,2) = 0d0
  maxrest=-1d0
  minrest=1d20
  do iw=1,spwout%n_spw
    if (spwout%win(iw)%flexible) cycle ! donot consider HR
    do is=1,m_sideband
      if (spwout%win(iw)%sb_code.ne.is) cycle
      if (spwout%win(iw)%restmin.lt.minrest) then
        minrest=spwout%win(iw)%restmin
      endif
      if (spwout%win(iw)%restmax.gt.maxrest) then
        maxrest=spwout%win(iw)%restmax
      endif
      call resttolsr(rsou%lsrshift,spwout%win(iw)%restmax,flsr,error)
      if (error) return
      if (flsr.gt.pms%lsrlim(is,2)) then
        pms%lsrlim(is,2)=flsr
      endif
      call resttolsr(rsou%lsrshift,spwout%win(iw)%restmin,flsr,error)
      if (error) return
      if (flsr.lt.pms%lsrlim(is,1)) then
        pms%lsrlim(is,1)=flsr
      endif
    enddo ! is sideband
  enddo ! iw SPW
  call resttorf(rsou%dopshift,maxrest,pms%tuning_maxrf,error)
  if (error) return
  pms%tuning_maxrf=pms%tuning_maxrf*ghzpermhz
  call resttorf(rsou%dopshift,minrest,pms%tuning_minrf,error)
  if (error) return
  pms%tuning_minrf=pms%tuning_minrf*ghzpermhz
  pms%lsrlim(1,1) = pms%lsrlim(1,1)*ghzpermhz
  pms%lsrlim(1,2) = pms%lsrlim(1,2)*ghzpermhz
  pms%lsrlim(2,1) = pms%lsrlim(2,1)*ghzpermhz
  pms%lsrlim(2,2) = pms%lsrlim(2,2)*ghzpermhz
  !Continuum + Correlator usage
  pms%fcontlsr=rtune%flotune ! LSR frame do not take into account earth doppler 
  call lsrtorest(rsou%lsrshift,pms%fcontlsr,pms%fcont,error) ! REST frame
  if (error) return
  if (pms%frep.ne.0.and.oms_bugw20) then
    ! Special case of W20 semester - OMS needs a continuum freq which is not the true LO
    ! But the LO we would have had if the tuning was the default one (i.e. IF=6000 when possible)
     call noema_get_fcontw20(pms%frep,rsou,pms%fcontw20,error)
    if (error) return
  endif
  !
  pms%dfcont = 0d0
  pms%rescont=-1d0
  dfconth = 0d0
  dfcontv = 0d0
  pms%chunk1=.false.
  
  do iu=1,pfx%n_units
    imode=pfx%unit(iu)%imode
    if (imode.le.0) cycle ! does not consider units that are OFF
    !correlator usage
    do it=1,pfx%unit(iu)%mode(imode)%n_types
      if (.not.(pfx%unit(iu)%mode(imode)%chtype(it)%move_chunk)) cycle
      pms%mflex=pms%mflex+pfx%unit(iu)%mode(imode)%chtype(it)%use_chunks
      pms%nflex=pms%nflex+pfx%unit(iu)%mode(imode)%chtype(it)%usage
      ! get if chunk1 is used (warning later)
      if (pfx%unit(iu)%mode(imode)%chtype(it)%chunks(1).gt.0) pms%chunk1=.true.
    enddo ! it
    ! continuum
    if (pfx%unit(iu)%pol_code.eq.1) then ! H polar
      dfconth=dfconth+pfx%unit(iu)%mode(imode)%chtype(lowrestype)%use_chunks* &
               pfx%unit(iu)%mode(imode)%chtype(lowrestype)%width_chunk- &
                pfx%unit(iu)%mode(imode)%chtype(lowrestype)%width_chunk/2d0
    else if (pfx%unit(iu)%pol_code.eq.2) then ! V polar
      dfcontv=dfcontv+pfx%unit(iu)%mode(imode)%chtype(lowrestype)%use_chunks* &
               pfx%unit(iu)%mode(imode)%chtype(lowrestype)%width_chunk- &
                pfx%unit(iu)%mode(imode)%chtype(lowrestype)%width_chunk/2d0 ! chunk #1 is 32 instead of 64
    endif
    if (pfx%unit(iu)%mode(imode)%chtype(lowrestype)%df_chunks.le.pms%rescont) cycle
    pms%rescont=pfx%unit(iu)%mode(imode)%chtype(lowrestype)%df_chunks
  enddo ! iu
  if (dfconth.lt.0.or.dfcontv.lt.0) then
    call astro_message(seve%e,rname,'Problem with continuum and polarizations')
    error=.true.
    return
  endif
  !
  if (dfconth.eq.dfcontv) then ! 2 polar
    pms%dfcont=dfconth
    pms%npolcont=2
  else if (dfconth.eq.0.and.dfcontv.gt.0) then ! full V
    pms%dfcont=dfcontv
    pms%npolcont=1
  else if (dfcontv.eq.0.and.dfconth.gt.0) then ! full H
    pms%dfcont=dfconth
    pms%npolcont=1
  else  ! mixed case
    pms%dfcont=(dfconth+dfcontv)/2d0
    pms%npolcont=2
  endif
  !
end subroutine noema_info_pms

