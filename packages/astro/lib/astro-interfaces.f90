module astro_interfaces
  !---------------------------------------------------------------------
  ! ASTRO interfaces, all kinds (private or public). Do not use directly
  ! out of the library.
  !---------------------------------------------------------------------
  !
  use astro_interfaces_public
  use astro_interfaces_private
  !
end module astro_interfaces
