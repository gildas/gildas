subroutine astro_header (line,error)
  use gbl_message
  use gkernel_interfaces
  use ast_astro
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  !	Prints out current times, and observatory coordinates
  !---------------------------------------------------------------------
  character(len=*):: line           !
  logical :: error                  !
  ! Local
  character(len=*), parameter :: rname='HEADER'
  character(len=message_length) :: mess
  real(kind=4) :: xmax,ymax
  logical :: land
  integer(kind=4) :: nd, nt, no
  character(len=40) :: d_chain, t_chain
  character(len=256) :: string
  !
  call sic_get_real('PAGE_X',xmax,error)
  call sic_get_real('PAGE_Y',ymax,error)
  land = xmax.ge.ymax
  !
  ! UTC
  call jdate_to_datetime(jnow_utc,d_chain,error)
  write(mess,1000) 'UTC :',d_chain
  call astro_message(seve%r,rname,mess)
  if (.not.projection) then
    string = 'DRAW TEXT 0 0.1 "UTC: '//trim(d_chain)//'" 9 /BOX 7'
  elseif  (land) then
    string = 'DRAW TEXT -0.3 0 "UTC: '//trim(d_chain)//'" 1 /BOX 7'
  else
    string = 'DRAW TEXT  0  3.7 "UTC: '//trim(d_chain)//'" 9 /BOX 7'
  endif
  call gr_exec1 (string)
  !
  ! UT1
  call jdate_to_datetime(jnow_ut1,d_chain,error)
  write(mess,1000) 'UT1 :',trim(d_chain),'D_UT1 :',d_ut1
  call astro_message(seve%r,rname,mess)
  !
  ! TDT
  call jdate_to_datetime(jnow_tdt,d_chain,error)
  write(mess,1000) 'TDT :',trim(d_chain),'D_TDT :',d_tdt
  call astro_message(seve%r,rname,mess)
  !
  ! Julian date
  write(mess,1010) 'Julian date :',jnow_tdt
  call astro_message(seve%r,rname,mess)
  !
  ! LST
  call rad2sexa(lst,24,t_chain,4,left=.true.)
  nt = len_trim(t_chain)
  write(mess,1000) 'LST :            ',t_chain(1:nt)
  call astro_message(seve%r,rname,mess)
  if (.not.projection) then
    string = 'DRAW TEXT 0 0.1 "LST: '//t_chain(1:nt)//'" 7 /BOX 9'
  elseif  (land) then
    string = 'DRAW TEXT -0.3 -1 "LST: '//t_chain(1:nt)//'" 1 /BOX 7'
    call gr_exec1(string)
    string = 'DRAW TEXT -0.3 -2 "FRAME : '//frame(1:lenc(frame))//'" 1 /BOX 7'
  else
    string = 'DRAW TEXT  0 2.7 "LST: '//t_chain(1:nt)//'" 9 /BOX 7'
    call gr_exec1(string)
    string = 'DRAW TEXT  0 1.7 "FRAME : '//frame(1:lenc(frame))//'" 9 /BOX 7'
  endif
  call gr_exec1 (string)
  !
  ! Observatory
  call deg2sexa(lonlat(1),360,t_chain,3,left=.true.)
  nt = len_trim(t_chain)
  call deg2sexa(lonlat(2),360,d_chain,3,left=.true.)
  nd = len_trim(d_chain)
  call astro_message(seve%r,rname,' ')
  no=lenc(obsname)
  write(mess,1020) 'OBS : ',obsname(1:no),t_chain(1:nt),d_chain(1:nd)
  call astro_message(seve%r,rname,mess)
  write(mess,1030) 'Alt.: ',altitude,' km     Sun Avoidance:',slimit,' deg.'
  call astro_message(seve%r,rname,mess)
  !
  call astro_message(seve%r,rname,' ')
  if (.not.projection) then
    string = 'DRAW TEXT 0 0.7 "OBS: '//t_chain(1:nt)//' '//d_chain(1:nd)//  &
    '" 9 /BOX 7'
  elseif  (land) then
    string = 'DRAW TEXT -0.3 -3 "OBS: '//t_chain(1:nt)//' '//d_chain(1:nd)//  &
    '" 1 /BOX 7'
  else
    string = 'DRAW TEXT 0 0.7 "OBS: '//t_chain(1:nt)//' '//d_chain(1:nd)//  &
    '" 9 /BOX 7'
  endif
  call gr_exec1(string)
  !
  ! Misc info
  call sup_header
  !
1000 format (1x,a,1x,a,4x,a,1x,f7.3)
1010 format (1x,a,1x,f16.2)
1020 format (1x,a,3x,a,3x,a,3x,a)
1030 format (1x,a,3x,f5.3,a,3x,f5.1,a)
end subroutine astro_header
!
subroutine sup_header
  use gbl_message
  use gkernel_interfaces
  use ast_horizon
  use ast_astro
  use ast_constant
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*), parameter :: rname='SUP_HEADER'
  character(len=message_length) :: mess
  logical :: error
  integer(kind=4) :: n1,n2,i
  integer(kind=4) :: nt(4)
  real(kind=8) ::  d1,d2,x0,utc
  character(len=12) :: cr1,cr2
  character(len=16) :: what(4)
  data what/'SunRise/Set :','Civil :','Nautical :','Astronomical :'/
  !
  error = .false.
  call sunrise(error)
  !
  ! Will convert to UTC
  utc = mod(jnow_utc+0.5d0,1d0)*twopi
  x0 = utc-lst
  do i=1,4
    d1=sunriz(i)+x0
    d1=d1*12.d0/pi
    d1=mod(d1+48.d0,24.d0)
    nt(1)=int(d1)
    nt(2)=int((d1-float(nt(1)))*60.0)
    nt(3)=int(((d1-float(nt(1)))*60.0-float(nt(2)))*60.0)
    nt(4)=int(((d1-float(nt(1)))*3600-float(nt(2))*60.0-float(nt(3)))*1000.0)
    call ntimec(nt, cr1, error)
    n1 = lenc(cr1)
    call sic_blanc(cr1, n1)
    d2=sunset(i)+x0
    d2=d2*12.d0/pi
    d2=mod(d2+48.d0,24.d0)
    nt(1)=int(d2)
    nt(2)=int((d2-float(nt(1)))*60.0)
    nt(3)=int(((d2-float(nt(1)))*60.0-float(nt(2)))*60.0)
    nt(4)=int(((d2-float(nt(1)))*3600-float(nt(2))*60.0-float(nt(3)))*1000.0)
    call ntimec(nt, cr2, error)
    n2 = lenc(cr2)
    call sic_blanc(cr2, n2)
    ! always up!
    if (i.eq.2) then
      write(mess,1000) 'Twilight              Start          End'
      call astro_message(seve%r,rname,mess)
    endif
    if (sunriz(i).lt.0) then
      if(i.eq.1) then
        write(mess,1001) what(i),'-always-up--','-always-up--'
      else
        write(mess,1001) what(i),'----none----','----none----'
      endif
      call astro_message(seve%r,rname,mess)
      ! always down!
    elseif (sunriz(i).gt.twopi) then
      if(i.eq.1) then
        write(mess,1001) what(i),'---no--sun--','---no-sun---'
      else
        write(mess,1001) what(i),'----none----','----none----'
      endif
      call astro_message(seve%r,rname,mess)
      !
    elseif (d1.le.d2) then
      if(i.eq.1) then
        write(mess,1001) what(i),cr1,cr2
      else
        write(mess,1001) what(i),cr2,cr1
      endif
      call astro_message(seve%r,rname,mess)
      !
    elseif (d1.gt.d2) then
      if(i.eq.1) then
        write(mess,1001) what(i),cr2,cr1
      else
        write(mess,1001) what(i),cr1,cr2
      endif
      call astro_message(seve%r,rname,mess)
    endif
  enddo
  return
  1000  format (1x,a)
  1001  format (1x,a,3x,a,3x,a)
end subroutine sup_header
