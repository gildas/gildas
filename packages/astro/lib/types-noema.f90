module astro_noema_type
  use astro_register_type ! register and all types from there
  !
  public
  !
  type noema_t
    ! Some global things
    type(receiver_source_t) :: source
    type(receiver_desc_t) :: recdesc
    ! Current FEBE configuration
    type(noema_febe_t) :: cfebe
    ! Register of saved febes
    type(noema_febe_register_t) :: register
    !
  end type noema_t
end module astro_noema_type
