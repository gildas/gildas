subroutine alma_line(line,error)
  use gildas_def
  use gbl_message
  use gkernel_interfaces
  use ast_line
  use ast_astro
  use atm_params
  !---------------------------------------------------------------------
  ! @ private
  ! ALMA 	Internal routine, support for command
  !	FREQUENCY name Freq Sideband [Center]
  !---------------------------------------------------------------------
  character(len=*), intent(inout) :: line  !
  logical, intent(out) :: error            !
  ! Local
  character(len=*), parameter :: rname='ALMA_LINE'
  character(len=32) :: name,par
  integer(kind=4) :: lname, lpar
  character(len=4) :: band
  real(kind=8) :: fcent, fsky,  vsys
  integer(kind=4) :: irec, userrec
!!  real(8), parameter :: eps=(100.25d0)   ! MHz, where does that come from ?...
  character(len=message_length) :: mess
  integer, parameter :: mkeys=2
  integer ::nkey
  character(len=4) :: keys(mkeys), key
  data keys/'LSB ','USB '/
  ! ---------------------------------------------------------------------------
  !
  ! Reset
  !
  error = .false.
  bb_def(:) = .false.
  bb_nspw(:) = 0
  !
  ! FREQ linename freq lsb|usb [if]
  !
  ! Line name
  !
  lname = 12
  call sic_ch (line,0,1,name,lname,.true.,error)
  if (error) return
  !
  ! Frequency
  !
  call sic_ch (line,0,2,par,lpar,.true.,error)
  if (error) goto 99
  call sic_math_dble(par,lpar,freq,error)
  if (error) goto 930
  ! Apply the redshift correction
  if (fshift.eq.0.0d0) fshift = 1.0d0
  fsky = freq*1d3*fshift       ! In MHz
  !
  ! Sideband
  !
  call sic_ch (line,0,3,par,lpar,.true.,error)
  if (error) goto 99
  call sic_upper(par(1:lpar))
  call sic_ambigs(rname,par,key,nkey,keys,mkeys,error)
  if (error) goto 99
  if (key.eq. 'LSB') then
    band = 'LSB '
    sky = -1
  elseif (key.eq. 'USB') then
    band = 'USB '
    sky = +1
  endif
  !
  ! Find receiver band irec
  ! fcent is the default IF frequency for the requested tuning
  ! = the middle of the IF band
  !
  ! Note: the frequency boundaries below are approximative
  if (fsky.gt.80e3 .and. fsky.lt.120e3) then
    irec = 3
    iflim(1) = 4000
    iflim(2) = 8000
    fcent = 6000  ! B3 4-8 GHz 2SB
  elseif (fsky.gt.121e3 .and. fsky.lt.163e3) then
    irec = 4
    iflim(1) = 4000
    iflim(2) = 8000
    fcent = 6000  ! B4 4-8 GHz 2SB
  elseif (fsky.gt.163e3 .and. fsky.lt.211e3) then
    irec = 5
    iflim(1) = 4000
    iflim(2) = 8000
    fcent = 6000  ! B5 4-8 GHz 2SB
  elseif (fsky.gt.211e3 .and. fsky.lt.275e3) then
    irec = 6
    iflim(1) = 4500
    iflim(2) = 10000
    fcent = 8000  ! B6 5-10 GHz 2SB
  elseif (fsky.gt.275e3 .and. fsky.lt.373e3) then
    irec = 7
    iflim(1) = 4000
    iflim(2) = 8000
    fcent = 6000  ! B7 4-8 GHz 2SB
  elseif (fsky.gt.385e3 .and. fsky.lt.500e3) then
    irec = 8
    iflim(1) = 4000
    iflim(2) = 8000
    fcent = 6000  ! B8 4-8 GHz 2SB
  elseif (fsky.gt.602e3 .and. fsky.lt.720e3) then
    irec = 9
    iflim(1) = 4000
    iflim(2) = 12000
    fcent = 8000  ! B9 4-12 GHz DSB
  elseif (fsky.gt.787e3 .and. fsky.lt.950e3) then
    irec = 10
    iflim(1) = 4000
    iflim(2) = 12000
    fcent = 8000  ! B10 4-12 GHz DSB
  else
    call astro_message(seve%e,rname,'Frequency cannot be observed with ALMA')
    error = .true.
    return
  endif
  !
  call check_receiver(irec, error)
  if (error) then
    call astro_message(seve%e, rname,'Receiver band not available in selected Cycle')
    return
  endif
  !
  userrec = irec
  call sic_i4(line,8,1,userrec,.false.,error)
  if (error) return
  if (irec.ne.userrec) then
    call astro_message(seve%e,rname,'Frequency/Receiver mismatch')
    error = .true.
    return
  endif
  !
  ! Center IF
  if (sic_present(0,4)) then
    call sic_ch (line,0,4,par,lpar,.true.,error)
    if (error) goto 99
    if (par(1:1).eq.'*') then
      call astro_message(seve%i,rname,'Using default center freq.')
    else
      call sic_math_dble(par,lpar,fcent,error)
      if (error) goto 930
      if (fcent.lt.iflim(1) .or. fcent.gt.iflim(2)) then
        call astro_message(seve%e,rname,'Invalid center frequency')
        goto 930
      endif
    endif
  endif
  !
  ! Compute FLO1
  !
  flo1 = fsky-sky*fcent
  !
  ! Final check on setup feasibility
  !
  call check_flo1(irec, flo1, mess, error)
  if (error) then
    call astro_message(seve%e, rname, mess) 
    return
  endif
  ! Output message
  !
  write(mess,'(A,I2)') 'Setup valid for receiver band ', irec
  call astro_message(seve%i,rname,mess)
  if (irec.eq.6) then
     write(mess,'(A,I2,A)') 'Band ',irec,' is a 2SB receiver with IF = 5 to 10 GHz'
  elseif (irec.gt.8) then
     write(mess,'(A,I2,A)') 'Band ',irec,' is a DSB receiver with IF = 4 to 12 GHz'
  else
     write(mess,'(A,I2,A)') 'Band ',irec,' is a 2SB receiver with IF = 4 to 8 GHz'
  endif
  call astro_message(seve%i,rname,mess)
  !
  ! Trec
  !
  call alma_receiver(flo1,flo1,irec)
  !
  vsys = (1.d0-fshift)*299792.458
  if (vsys.ne.0) then
    write(mess,*) 'Source velocity = ', vsys,' km/s'
    call astro_message(seve%r,rname,mess)
  endif
  write(mess,'(a,f10.5,a)') ' FSKY  = ',fsky/1d3,' GHz'
  call astro_message(seve%r,rname,mess)
  write(mess,'(a,f10.5,a)') ' FLO1  = ',flo1/1d3,' GHz'
  call astro_message(seve%r,rname,mess)
  write(mess,'(a,f6.1,a)') ' TREC  = ',trec,' K'
  call astro_message(seve%r,rname,mess)
  !
  ! Plot label
  !
  write(label,1010) 'ALMA BAND ', irec, name(1:lenc(name)), fsky*1d-3/fshift, band, fcent,  &
  '   [V= ',vsys,' km/s]'
  recband = irec
  !
  ! Now do the plot
  !
  plot_mode = 10
  call alma_plot_def(error)
  if (error) return
  call alma_plot_line
  !
  ! End
  !
  return
  !
  ! Errors
  !
  930  call astro_message(seve%e,rname,'Error decoding '//par(1:lpar))
  99   error = .true.
  return
  !
  ! Format
  !
  ! 1010 format(a,i2,' FREQ ',a,1x,f10.5,1x,a,1x,f7.2,a,f6.1,a)
  1010 format(a,i2,a,1x,f10.5,1x,a,1x,f7.2,a,f6.1,a)
  !
end subroutine alma_line
!
function rffromif(if,band)
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  real(kind=4) :: rffromif             !
  real(kind=4), intent(in) :: if       !
  integer(kind=4), intent(in) :: band  !
  rffromif = flo1+band*if
end function rffromif
!
function iffromrf(rf,band)
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  real(kind=4) :: iffromrf             !
  real(kind=4), intent(in) :: rf       !
  integer(kind=4), intent(in) :: band  !
  iffromrf = (rf-flo1)/band
end function iffromrf
!
subroutine decoderf(rf,if,band)
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  real(kind=4), intent(in) :: rf        !
  real(kind=4), intent(out) :: if       !
  integer(kind=4), intent(out) :: band  !
  band = 1
  if (rf.lt.flo1) band = -1
  if = (rf-flo1)/band
end subroutine decoderf
!
subroutine alma_receiver(f,u,irec)
  use atm_params
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  real(kind=8), intent(in) :: f        ! Signal frequency
  real(kind=8), intent(in) :: u        ! Image frequency
  integer(kind=4), intent(in) :: irec  ! Receiver band
  ! Local
  real(kind=4), parameter :: h_sur_k = 6.62e-34/1.38e-23*1e9
  real(kind=4) :: ff
  !
  ! These are the Trec used by the OT (version Cycle 0)
  real(kind=4) :: ottrec(10)
  data ottrec/17.0,30.0,45.0,51.0,65.0,55.0,75.0,196.0,110.0,230.0/
  !
  ff = f*1e-3
  !
  feff = 0.95-0.05*(ff/900.0)**2  ! Forward efficiency guess
  if (f.lt.390.0) then
    gim = 0.1
    if (f.lt.300.0) then
        trec = 4.0*h_sur_k*ff + 4.0
    else
        trec = 6.0*h_sur_k*ff + 4.0
    endif
    trec = max(trec,25.0)
  else
    gim = 1.0
    trec = 5.0*h_sur_k*ff
  endif
  trec = ottrec(irec)
end subroutine alma_receiver
!
subroutine check_receiver(irec, error)
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4), intent(in) :: irec  !
  logical, intent(out) :: error        !
  !
  error = .false.
  if (alma_cycle.eq.0) then
    if (irec.ne.3 .and. irec.ne.6 .and. irec.ne.7 .and. irec.ne.9) then
      error = .true.
    endif
  else
    if (irec.lt.3) then
      error = .true.
    endif
  endif
end subroutine check_receiver
!
subroutine check_flo1(irec, flo, chain, error)
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4), intent(in) :: irec  !
  real(kind=8), intent(in) :: flo      !
  character(len=*), intent(inout) :: chain
  logical, intent(out) :: error        !
  ! Local
  logical :: ok
  real(kind=8) :: lo
  real(8) :: flo_limits(2,3:10)
  data flo_limits /92,108, &
  & 133, 155, &
  & 171, 203, &
  & 221, 265, &
  & 283, 365, &
  & 393, 492, &
  & 610, 712, &
  & 795, 942 /
  !
  ! Check limits of FLO1. Should be independant from Cycle,
  ! though here are Cycle 0 values (Tech. Handbook Table 1)
  !
  lo = flo/1d3
  if (irec.lt.3 .or. irec.gt.10) then
    write(chain,'(A,I0)') 'Invalid receiver ',irec
    error = .true.
    return
  endif
  ok = (lo.ge.flo_limits(1,irec)) .and. (lo.le.flo_limits(2,irec))
  if (.not.ok) then
    write(chain,'(A,F6.1,A,2F6.1)') 'LO1 ',lo,' not in range ', &
    & flo_limits(1,irec),flo_limits(2,irec)
    error = .true.
  else
    error = .false.
  endif
end subroutine check_flo1
