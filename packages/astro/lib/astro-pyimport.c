
/* Define entry point "initpyastro" for command "import pyastro" in Python */

#include "sic/gpackage-pyimport.h"

GPACK_DEFINE_PYTHON_IMPORT(astro);
