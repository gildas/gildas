subroutine pdbi_plot_line(error)
  use gbl_message
  use ast_line
  !---------------------------------------------------------------------
  ! @ public
  ! Main entry for plotting
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Local
  character(len=*), parameter :: rname='PDBI_PLOT'
  logical :: ok
  integer(kind=4) :: i, nch
  real(kind=4) :: df, dv
  real(kind=8) :: f
  character(len=6) :: input1(4), input2(4)
  character(len=10) :: dsb
  data input1/'Q1 HOR','Q2 HOR','Q3 VER','Q4 VER'/
  data input2/'Q1 VER','Q2 VER','Q3 HOR','Q4 HOR'/
  character(len=message_length) :: mess
  !
  if (flo1.eq.0) then
     call astro_message(seve%e,rname,'LINE command not yet executed')
     error = .true.
     return
  endif
  !
  if (plot_mode.eq.1) then
     call astro_message(seve%i,rname,'Plotting mode: LINE coverage')
     call pdbi_plot_full
  elseif ((plot_mode.eq.2).and.(narrow_def)) then
     call astro_message(seve%i,rname,'Plotting mode: NARROW correlator')
     call pdbi_plot_narrow
  elseif (plot_mode.eq.3) then
     call astro_message(seve%i,rname,'Plotting mode: WIDEX correlator')
     call pdbi_plot_widex
  else
    call astro_message(seve%e,rname,'Initilization problem')
    return
  endif
  !
  ! Spurious message on spurious lines
  ! Message is here so that it is output with PLOT command *and*
  ! with LINE/NARROW/SPECTRAL
  !
  if (do_spurious) then
     write(mess,'(A,F8.2,A)') 'Plotting possible spurious lines '// &
             ' (FLO1REF = ',flo1ref,' MHz)'
     call astro_message(seve%i,'PDBI_PLOT',trim(mess))
  endif
  !
  ! Print receiver and correlator setups
  !
  ! call astro_message(seve%i,rname,'Receiver tuning:')
  ! call astro_message(seve%r,rname,' '//label)
  !
  if (narrow_def) then
    call astro_message(seve%r,rname,'')
    call astro_message(seve%i,rname,'Current narrow-band correlator setup:')
    call astro_message(seve%r,rname,' Entry 1: '//input1(narrow_input(1)))
    call astro_message(seve%r,rname,' Entry 2: '//input2(narrow_input(2)))
    ok = .false.
    do i=1,nunit
      ok = ok.or.unit_def(i)
    enddo
    if (ok) then
      do i=1,nunit
        if (unit_def(i)) then
          !
          ! Compute Nchannel, df, dv
          !
          if (unit_band(i).eq.20) nch = 512    ! SSB mode
          if (unit_band(i).eq.40) nch = 512    ! SSB mode
          if (unit_band(i).eq.80) then
            if (unit_bmode(i).le.3) nch = 256  ! SSB mode
            if (unit_bmode(i).gt.3) nch = 512  ! DSB mode
          endif
          if (unit_band(i).eq.160) then
            if (unit_bmode(i).le.3) nch = 128  ! SSB mode
            if (unit_bmode(i).gt.3) nch = 256  ! DSB mode
          endif
          if (unit_band(i).eq.320) nch = 128   ! DSB mode
          if (unit_bmode(i).le.3) then
            dsb = '(SSB mode)'
          else
            dsb = '(DSB mode)'
          endif
          df = float(unit_band(i))/float(nch)
          call find_rffreq(i,f)  ! RF frequency at center of subband i
          dv = df/f*299792.458
          !
          ! Ouptut message
          !
          write(mess,1010) i,unit_band(i),unit_cent(i),unit_wind(i), nch,  &
            df, dv, dsb
        else
          !
          ! Unit not defined
          !
          write(mess,1020) i
        endif
        call astro_message(seve%r,rname,mess)
      enddo
    else
      call astro_message(seve%r,rname,' No spectral units defined')
    endif
    call astro_message(seve%r,rname,'')
  else
     ! call astro_message(seve%i,rname,'Narrow-band correlator setup: '// &
     !     'not yet defined')
  endif
  1010 format('  Unit ',i1,i4,f8.2,' on entry ',i1,' -- ',i3,' ch. of ',f5.3,  &
              ' MHz = ',f4.2,' km/s ',a)
  1020 format('  Unit ',i1,' OFF')
end subroutine pdbi_plot_line
!
! ------------------------------------------------------------------------
!
subroutine pdbi_plot_full
  use gbl_message
  use ast_line
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  ! Plot of USB + LSB, as produced by the LINE command
  !---------------------------------------------------------------------
  real(kind=8) :: bandwidth, center
  real(kind=8) :: f1, f2, u1, u2
  character(len=*), parameter :: rname='PDBI_PLOT'
  character(len=132) :: chain, limi
  !
  call gr_exec('CLEAR DIRECTORY')
  call gr_exec1('SET PLOT LANDSCAPE')
  !
  ! IF1 bandwidth
  !
  bandwidth = abs(userlim(2)-userlim(1))/1000.
  center = userlim(1)+(userlim(2)-userlim(1))/2.
  write(limi,1040) userlim(1), userlim(2)
  !
  ! SIGNAL plot ------------------------------------------------------------
  !
  call gr_exec1('SET ORIEN 0')
  call gr_exec1('TICK 0 0 0 0')
  call gr_exec1('PEN 0')
  call gr_exec1('SET BOX 3 29 9 17')
  !
  ! USB or LSB?
  !
  if (sky.eq.1) then ! Signal = USB, IMAGE = LSB
     f1 = (flo1+center)/1000.d0/fshift-bandwidth/2.
     f2 = f1+bandwidth
     u1 = (flo1-center)/1000.d0/fshift+bandwidth/2.
     u2 = u1-bandwidth
  else               ! Signal = LSB, IMAGE = USB
     f1 = (flo1-center)/1000.d0/fshift+bandwidth/2.
     f2 = f1-bandwidth
     u1 = (flo1+center)/1000.d0/fshift-bandwidth/2.
     u2 = u1+bandwidth
  endif
  !
  ! RF axis
  !
  write(chain,1040) f1,f2
  call gr_exec1(chain)
  call gr_exec1('AXIS XU /TICK IN /LABEL P')
  if (sky.eq.1) then
    call gr_exec1('DRAW TEXT 0 1 "Rest frequency (GHz)    USB - Signal" /BOX 8')
  else
    call gr_exec1('DRAW TEXT 0 1 "Rest frequency (GHz)    LSB - Signal" /BOX 8')
  endif
  !
  ! Plot atmospheric transmission
  !
  if (do_atmplot) then
     call pdbi_line_atmos(f1,f2,u1,u2,plotwater,200)
  endif
  !
  ! Plot molecular lines
  !
  if (nmol.ge.1) then
     call pdbi_line_molecules(f1,f2)
  endif
  !
  ! Indicate requested tuning frequency
  !
  call gr_exec1('PEN 6')
  write(chain,1090) freq, 0.
  call gr_exec1(chain)
  write(chain,1100) freq, 1.5
  call gr_exec1(chain)
  call gr_exec1('PEN 0')
  !
  ! IF1 axis
  !
  call gr_exec1(limi)
  call gr_exec1('AXIS XL')
  call gr_exec1('DRAW TEXT 0 -1.5 "Intermediate frequency IF1 (MHz)" 5 /BOX 2')
  !
  ! Y axis (dash on user limits, then continuous on hard limits)
  !
  call gr_exec1('PEN /DASH 3')
  write (chain,1050) userlim(1)
  call gr_exec1(chain)
  write (chain,1060) userlim(1)
  call gr_exec1(chain)
  write (chain,1050) userlim(2)
  call gr_exec1(chain)
  write (chain,1060) userlim(2)
  call gr_exec1(chain)
  call gr_exec1('PEN /DASH 1')
  write (chain,1050) iflim(1)
  call gr_exec1(chain)
  write (chain,1060) iflim(1)
  call gr_exec1(chain)
  write (chain,1050) iflim(2)
  call gr_exec1(chain)
  write (chain,1060) iflim(2)
  call gr_exec1(chain)
  !
  ! Spurious lines
  !
  if (do_spurious) then
     call pdbi_line_spurious
  endif
  !
  ! Plot correlator quarters (narrow-band and widex)
  !
  call pdbi_plot_full_corr
  !
  ! Label = LINE command
  !
  call gr_exec1 ('SET ORIEN 0')
  call gr_exec1('DRAW TEXT 0 3 "'//trim(label)//'" 5 /BOX 8')
  !
  ! IMAGE plot -----------------------------------------------------------
  !
  call gr_exec1('SET ORIEN 0')
  call gr_exec1('PEN 14 /COL 14')
  call gr_exec1('SET BOX 3 29 3 6')
  !
  ! USB or LSB?
  !
  if (sky.eq.-1) then ! IMAGE = USB
     f1 = (flo1+center)/1000.d0/fshift-bandwidth/2.
     f2 = f1+bandwidth
     u1 = (flo1-center)/1000.d0/fshift+bandwidth/2.
     u2 = u1-bandwidth
  else                ! IMAGE = LSB
     f1 = (flo1-center)/1000.d0/fshift+bandwidth/2.
     f2 = f1-bandwidth
     u1 = (flo1+center)/1000.d0/fshift-bandwidth/2.
     u2 = u1+bandwidth
  endif
  !
  ! RF axis
  !
  write(chain,1040) f1,f2
  call gr_exec1(chain)
  call gr_exec1('AXIS XL /TICK IN /LABEL P')
  if (sky.eq.-1) then
    call gr_exec1('DRAW TEXT 0 -1 "Rest frequency (GHz)    USB - Image" /BOX 2')
  else
    call gr_exec1('DRAW TEXT 0 -1 "Rest frequency (GHz)    LSB - Image" /BOX 2')
  endif
  !
  ! Plot atmospheric transmission
  !
  ! if (do_atmplot) then
  !   call pdbi_line_atmos(f1,f2,u1,u2,plotwater,100)
  ! endif
  !
  ! Plot molecular lines
  !
  if (nmol.ge.1) then
     call pdbi_line_molecules(f1,f2)
  endif
  !
  ! IF axis
  !
  call gr_exec1(limi)
  call gr_exec1('AXIS XU /TICK IN /LABEL P')
  !
  ! Y axis (dash on user limits, then continuous on hard limits)
  !
  call gr_exec1('PEN /DASH 3')
  write (chain,1050) userlim(1)
  call gr_exec1(chain)
  write (chain,1060) userlim(1)
  call gr_exec1(chain)
  write (chain,1050) userlim(2)
  call gr_exec1(chain)
  write (chain,1060) userlim(2)
  call gr_exec1(chain)
  call gr_exec1('PEN /DASH 1')
  write (chain,1050) iflim(1)
  call gr_exec1(chain)
  write (chain,1060) iflim(1)
  call gr_exec1(chain)
  write (chain,1050) iflim(2)
  call gr_exec1(chain)
  write (chain,1060) iflim(2)
  call gr_exec1(chain)
  !
  ! Spurious lines -- REMOVED
  !
  ! if (do_spurious) then
  !   call pdbi_line_spurious
  ! endif
  !
  ! Plot correlator quarters -- REMOVED
  !
  ! call pdbi_plot_full_corr
  !
  call gr_exec1('PEN 0')
  !
  ! End -------------------------------------------------------------------
  !
  1040 format('LIMITS ',1pg25.16,1x,1pg25.16,' 0 5')
  1050 format('DRAW RELOCATE ',f10.2,' 0 /USER')
  1060 format('DRAW LINE ',f10.2,' 5 /USER')
  1090 format('DRAW RELOCATE ',f15.6,' ',f15.6,' /USER')
  1100 format('DRAW LINE ',f15.6,' ',f15.6,' /USER')
  !
end subroutine pdbi_plot_full
!
! -----------------------------------------------------------------------
!
subroutine pdbi_plot_full_corr
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  ! Plot the correlator bands onto the DSB plot
  !---------------------------------------------------------------------
  integer(kind=4) :: i
  character(len=14) :: colortext(4)
  !
  ! Color for the four quarters
  !
  do i = 1,4
     if (narrow_def) then
        if (narrow_input(1).eq.i) then
           colortext(i) = 'PEN 1'  ! NARROW 1 in red
        elseif (narrow_input(2).eq.i) then
           colortext(i) = 'PEN 3'  ! NARROW 2 in blue
        else
           colortext(i) = 'PEN 14 /COL 14'  ! Not used
        endif
     else
        colortext(i) = 'PEN 14 /COL 14'  ! No NARROW defined yet
     endif
  enddo
  !
  ! Plot the NARROW quarters
  !
  call gr_exec1(colortext(1))
  call gr_exec1('DRAW TEXT 4700 0.7 "Q1" 5 /USER /CLIP')
  call gr_exec1(colortext(1)//' /WEIGHT 5')
  call gr_exec1('DRAW RELOCATE 4760 0.7 /USER /CLIP')
  call gr_exec1('DRAW ARROW 5200 0.7 /USER /CLIP')
  call gr_exec1('DRAW RELOCATE 4635 0.7 /USER /CLIP')
  call gr_exec1('DRAW ARROW 4200 0.7 /USER /CLIP')
  call gr_exec1(colortext(1)//' /WEIGHT 1')
  !
  call gr_exec1(colortext(2))
  call gr_exec1('DRAW TEXT 5500 1.1 "Q2" 5 /USER /CLIP ')
  call gr_exec1(colortext(2)//' /WEIGHT 5')
  call gr_exec1('DRAW RELOCATE 5560 1.1 /USER /CLIP')
  call gr_exec1('DRAW ARROW 6000 1.1 /USER /CLIP ')
  call gr_exec1('DRAW RELOCATE 5435 1.1 /USER /CLIP')
  call gr_exec1('DRAW ARROW 5000 1.1 /USER /CLIP ')
  call gr_exec1(colortext(2)//' /WEIGHT 1')
  !
  call gr_exec1(colortext(3))
  call gr_exec1('DRAW TEXT 6500 0.7 "Q3" 5 /USER /CLIP ')
  call gr_exec1(colortext(3)//' /WEIGHT 5')
  call gr_exec1('DRAW RELOCATE 6565 0.7 /USER /CLIP ')
  call gr_exec1('DRAW ARROW 7000 0.7 /USER /CLIP ')
  call gr_exec1('DRAW RELOCATE 6435 0.7 /USER /CLIP ')
  call gr_exec1('DRAW ARROW 6000 0.7 /USER /CLIP ')
  call gr_exec1(colortext(3)//' /WEIGHT 1')
  !
  call gr_exec1(colortext(4))
  call gr_exec1('DRAW TEXT 7300 1.1 "Q4" 5 /USER /CLIP')
  call gr_exec1(colortext(4)//' /WEIGHT 5')
  call gr_exec1('DRAW RELOCATE 7365 1.1 /USER /CLIP')
  call gr_exec1('DRAW ARROW 7800 1.1 /USER /CLIP')
  call gr_exec1('DRAW RELOCATE 7235 1.1 /USER /CLIP')
  call gr_exec1('DRAW ARROW 6800 1.1 /USER /CLIP')
  call gr_exec1(colortext(4)//' /WEIGHT 1')
  !
  ! Now draw the WIDEX coverage
  !
  call gr_exec1('PEN 0 /WEIGHT 1')
  call gr_exec1('DRAW TEXT 5100 0.3 "WIDEX LOW" 5 /USER /CLIP')
  call gr_exec1('PEN 0 /WEIGHT 5')
  call gr_exec1('DRAW RELOCATE 5350 0.3 /USER /CLIP')
  call gr_exec1('DRAW ARROW 6000 0.3 /USER /CLIP')
  call gr_exec1('DRAW RELOCATE 4850 0.3 /USER /CLIP')
  call gr_exec1('DRAW ARROW 4200 0.3 /USER /CLIP')
  !
  call gr_exec1('PEN 0 /WEIGHT 1')
  call gr_exec1('DRAW TEXT 6900 0.3 "WIDEX HIGH" 5 /USER /CLIP')
  call gr_exec1('PEN 0 /WEIGHT 5')
  call gr_exec1('DRAW RELOCATE 7165 0.3 /USER /CLIP')
  call gr_exec1('DRAW ARROW 7800 0.3 /USER /CLIP')
  call gr_exec1('DRAW RELOCATE 6635 0.3 /USER /CLIP')
  call gr_exec1('DRAW ARROW 6000 0.3 /USER /CLIP')
  !
  call gr_exec1('PEN 0 /WEIGHT 1')
  !
end subroutine pdbi_plot_full_corr
!
! -----------------------------------------------------------------------
!
subroutine pdbi_plot_narrow
  use gbl_message
  use ast_line
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  ! Plot of the NARROW-BAND correlator
  !---------------------------------------------------------------------
  ! Global
  logical :: gr_error
  ! Local
  real(kind=8) :: bandwidth, center, ifmin, ifmax, tmp
  real(kind=8) :: f1, f2, u1, u2
  character(len=*), parameter :: rname='PDBI_PLOT'
  character(len=3) :: band
  character(len=132) :: chain, limi
  !
  character(len=6) :: input1(4), input2(4)
  data input1/'Q1 HOR','Q2 HOR','Q3 VER','Q4 VER'/
  data input2/'Q1 VER','Q2 VER','Q3 HOR','Q4 HOR'/
  real(kind=4) :: qlim(2,4)
  data qlim/4200,5200,5000,6000,6000,7000,6800,7800/
  !
  ! ------------------------------------------------------------------------
  !
  call gr_exec('CLEAR DIRECTORY')
  call gr_exec1('PEN 0')
  !
  ! NARROW 1 ---------------------------------------------------------------
  !
  ! IF1 limits
  !
  ifmin = qlim(1,narrow_input(1))
  ifmax = qlim(2,narrow_input(1))
  bandwidth = abs(ifmax-ifmin)/1000.
  center = ifmin+(ifmax-ifmin)/2.
  !
  ! Box
  !
  call gr_exec1('SET ORIEN 0')
  call gr_exec1('TICK 0 0 0 0')
  call gr_exec1('SET BOX 4 29 11.5 17')
  call gr_exec1('BOX N N N')
  if (gr_error()) return
  !
  ! RF axis
  !
  ! USB or LSB?
  !
  if (sky.eq.1) then           ! USB
    f1 = (flo1+center)/1000.d0/fshift-bandwidth/2.
    f2 = f1+bandwidth
    u1 = (flo1-center)/1000.d0/fshift+bandwidth/2.
    u2 = u1-bandwidth
    band = "USB"
  else                         ! LSB
    f1 = (flo1-center)/1000.d0/fshift+bandwidth/2.
    f2 = f1-bandwidth
    u1 = (flo1+center)/1000.d0/fshift-bandwidth/2.
    u2 = u1+bandwidth
    band = "LSB"
  endif
  ! IF in Q2 and Q4 is flipped wrt the RF frequency
  if ((narrow_input(1).eq.2).or.(narrow_input(1).eq.4)) then
    tmp = f1
    f1 = f2
    f2 = tmp
	tmp = u1
	u1 = u2
	u2 = tmp
  endif
  write(chain,1040) f1,f2
  call gr_exec1(chain)
  call gr_exec1('AXIS XU /TICK IN /LABEL P')
  call gr_exec1('PEN /DASH 1')
  ! call gr_exec1('DRAW TEXT 0  1 "Rest frequency (GHz) - '//band //'" /BOX 8')
  call gr_exec1('DRAW TEXT 0  1 "Rest frequency (GHz)" /BOX 8')
  !
  ! Plot atmospheric transmission
  !
  if (do_atmplot) then
     call pdbi_line_atmos(f1,f2,u1,u2,plotwater,100)
  endif
  !
  ! Plot molecular lines
  !
  if (nmol.ge.1) then
     call pdbi_line_molecules(f1,f2)
  endif
  !
  ! IF axis at correlator entry (100-1100 MHz)
  !
  call gr_exec1('LIMITS 100 1100 0 5')
  call gr_exec1('AXIS XL')
  call gr_exec1('DRAW TEXT 0 -1.5 "Intermediate frequency at correlator '//  &
  'input (MHz)" 5 /BOX 2')
  !
  ! Label (correlator input)
  !
  call gr_exec1('PEN 1')
  call gr_exec1('DRAW TEXT -2 0 "NARROW 1 = '//input1(narrow_input(1)) //'" 5 90 '//  &
  '/BOX 4')
  write(chain,1070) ifmin/1e3, ifmax/1e3
  call gr_exec1('DRAW TEXT -1 0 "'//trim(chain)//'" 5 90 /BOX 4')
  call gr_exec1('BOX N N N')
  !
  ! Spectral units
  !
  call pdbi_plot_spectral(1,0)
  !
  ! Spurious lines (need to switch to IF1 limits)
  !
  if (do_spurious) then
     if ((narrow_input(1).eq.1).or.(narrow_input(1).eq.3)) then
        write(limi,1040) ifmin, ifmax
        call gr_exec1(limi)
     else
        write(limi,1040) ifmax, ifmin  ! Q2 and Q4 are flipped
        call gr_exec1(limi)
     endif
     call pdbi_line_spurious
  endif
  call gr_exec1('PEN 0')
  !
  ! Label (LINE command)  -------------------------------------------------
  !
  call gr_exec1 ('SET ORIEN 0')
  call gr_exec1('DRAW TEXT 0 3 "'//trim(label)//'" 5 /BOX 8')
  !
  ! NARROW 2 --------------------------------------------------------------
  !
  ! IF1 limits
  !
  ifmin = qlim(1,narrow_input(2))
  ifmax = qlim(2,narrow_input(2))
  bandwidth = abs(ifmax-ifmin)/1000.
  center = ifmin+(ifmax-ifmin)/2.

  !
  write(limi,1040) ifmin, ifmax
  call gr_exec1('SET ORIEN 0')
  call gr_exec1('SET BOX 4 29 3 8.5')
  call gr_exec1('BOX N N N')
  if (gr_error()) return
  !
  ! RF axis
  !
  if (sky.eq.1) then           ! USB
    f1 = (flo1+center)/1000.d0/fshift-bandwidth/2.
    f2 = f1+bandwidth
    u1 = (flo1-center)/1000.d0/fshift+bandwidth/2.
    u2 = u1-bandwidth
    band = "USB"
  else                         ! LSB
    f1 = (flo1-center)/1000.d0/fshift+bandwidth/2.
    f2 = f1-bandwidth
    u1 = (flo1+center)/1000.d0/fshift-bandwidth/2.
    u2 = u1+bandwidth
    band = "LSB"
  endif

  ! IF in Q2 and Q4 is flipped wrt the RF frequency
  if ((narrow_input(2).eq.2).or.(narrow_input(2).eq.4)) then
    tmp = f1
    f1 = f2
    f2 = tmp
	tmp = u1
	u1 = u2
	u2 = tmp
  endif
  write(chain,1040) f1,f2
  call gr_exec1(chain)
  call gr_exec1('AXIS XL /TICK IN /LABEL P')
  call gr_exec('PEN /DASH 1')
  ! call gr_exec1('DRAW TEXT 0 -1 "Rest frequency (GHz) - '//band //'" /BOX 2')
  call gr_exec1('DRAW TEXT 0 -1 "Rest frequency (GHz)" /BOX 2')
  !
  ! Plot atmospheric transmission
  !
  if (do_atmplot) then
     call pdbi_line_atmos(f1,f2,u1,u2,plotwater,100)
  endif
  !
  ! Plot molecular lines
  !
  if (nmol.ge.1) then
     call pdbi_line_molecules(f1,f2)
  endif
  !
  ! IF axis at correlator entry (100-1100 MHz)
  !
  call gr_exec1('LIMITS 100 1100 0 5')
  call gr_exec1('AXIS XU /TICK IN /LABEL P')
  ! call gr_exec1('DRAW TEXT 0 1.5 "Intermediate frequency at correlator '//  &
  ! 'input (MHz)" 5 /BOX 8')
  !
  ! Label (correlator input)
  !
  call gr_exec1('PEN 3')
  call gr_exec1('DRAW TEXT -2 0 "NARROW 2 = '//input2(narrow_input(2))//'" 5 90 '//  &
  '/BOX 4')
  write(chain,1070) ifmin/1e3, ifmax/1e3
  call gr_exec1('DRAW TEXT -1 0 "'//trim(chain)//'" 5 90 '//  &
  '/BOX 4')
  call gr_exec1('BOX N N N')
  !
  ! Spectral units
  !
  call pdbi_plot_spectral(2,0)
  !
  ! Spurious lines
  !
  if (do_spurious) then
     if ((narrow_input(2).eq.1).or.(narrow_input(2).eq.3)) then
        write(limi,1040) ifmin, ifmax
        call gr_exec1(limi)
     else
        write(limi,1040) ifmax, ifmin  ! Q2 and Q4 are flipped
        call gr_exec1(limi)
     endif
     call pdbi_line_spurious
  endif
  call gr_exec1('PEN 0')
  !
  ! End -------------------------------------------------------------------
  !
  1040 format('LIMITS ',1pg25.16,1x,1pg25.16,' 0 5')
  1070 format('IF1 = ',f3.1,' to ',f3.1,' GHz')
  !
end subroutine pdbi_plot_narrow
!
! -----------------------------------------------------------------------
!
subroutine pdbi_plot_widex
  use gbl_message
  use ast_line
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  ! Plot of WIDEX
  !---------------------------------------------------------------------
  ! Global
  logical :: gr_error
  ! Local
  real(kind=8) :: bandwidth, center, ifmin, ifmax
  real(kind=8) :: f1, f2, u1, u2
  character(len=*), parameter :: rname='PDBI_PLOT'
  character(len=3) :: band
  character(len=132) :: chain, limi
  integer(kind=4) :: lowband, highband
  data lowband/1/
  data highband/2/
  !
  ! ------------------------------------------------------------------------
  !
  call gr_exec('CLEAR DIRECTORY')
  call gr_exec1('PEN 0')
  !
  ! WIDEX LOW -------------------------------------------------------------
  !
  ! IF1 limits
  !
  ifmin = 4200
  ifmax = 6000
  bandwidth = abs(ifmax-ifmin)/1000.
  center = ifmin+(ifmax-ifmin)/2.
  !
  write(limi,1040) ifmin, ifmax
  call gr_exec1('SET ORIEN 0')
  call gr_exec1('SET BOX 4 29 11.5 17')
  call gr_exec1('PEN 0')
  ! call gr_exec1('BOX N N N')
  if (gr_error()) return
  !
  ! RF axis
  !
  if (sky.eq.1) then           ! USB
    f1 = (flo1+center)/1000.d0/fshift-bandwidth/2.
    f2 = f1+bandwidth
    u1 = (flo1-center)/1000.d0/fshift+bandwidth/2.
    u2 = u1-bandwidth
    band = "USB"
  else                         ! LSB
    f1 = (flo1-center)/1000.d0/fshift+bandwidth/2.
    f2 = f1-bandwidth
    u1 = (flo1+center)/1000.d0/fshift-bandwidth/2.
    u2 = u1+bandwidth
    band = "LSB"
  endif
  !
  write(chain,1040) f1,f2
  call gr_exec1(chain)
  call gr_exec1('AXIS XU /TICK IN /LABEL P')
  call gr_exec1('PEN /DASH 1')
  ! call gr_exec1('DRAW TEXT 0  1 "Rest frequency (GHz) - '//band //'" /BOX 8')
  call gr_exec1('DRAW TEXT 0  1 "Rest frequency (GHz)" /BOX 8')
  !
  ! Plot atmospheric transmission
  !
  if (do_atmplot) then
     call pdbi_line_atmos(f1,f2,u1,u2,plotwater,100)
  endif
  !
  ! Plot molecular lines
  !
  if (nmol.ge.1) then
     call pdbi_line_molecules(f1,f2)
  endif
  !
  ! IF1 axis
  !
  write(chain,1040) ifmin, ifmax
  call gr_exec1(chain)
  call gr_exec1('AXIS XL')
  call gr_exec1('AXIS YL /TICK NO')
  call gr_exec1('PEN 0 /DASH 3')
  call gr_exec1('DRAW RELOCATE 6000 0 /USER')
  call gr_exec1('DRAW LINE 6000 5 /USER')
  call gr_exec1('PEN 0 /DASH 1')
  call gr_exec1('DRAW TEXT 0 -1.5 "Intermediate frequency IF1 (MHz)" 5 /BOX 2')
  !
  ! Spectral units
  !
  call pdbi_plot_narrow_widex(lowband)
  !
  ! Spurious lines
  !
  if (do_spurious) then
     call pdbi_line_spurious
  endif
  !
  ! Label
  !
  call gr_exec1('PEN 0')
  call gr_exec1('DRAW TEXT -2 0 "WIDEX LOW" 5 90 /BOX 4')
  write(chain,1080) ifmin/1e3, ifmax/1e3
  call gr_exec1('DRAW TEXT -1 0 "'//trim(chain)//'" 5 90 '//  &
  '/BOX 4')
  call gr_exec1('LIMITS 100 1900 0 5')
  ! call gr_exec1('BOX N N N')
  !
  ! Label (LINE command)  -------------------------------------------------
  !
  call gr_exec1('PEN 0')
  call gr_exec1 ('SET ORIEN 0')
  call gr_exec1('DRAW TEXT 0 3 "'//trim(label)//'" 5 /BOX 8')
  !
  ! WIDEX HIGH -------------------------------------------------------------
  !
  ! IF1 limits
  !
  ifmin = 6000
  ifmax = 7800
  bandwidth = abs(ifmax-ifmin)/1000.
  center = ifmin+(ifmax-ifmin)/2.
  !
  ! Box
  !
  call gr_exec1('SET ORIEN 0')
  call gr_exec1('TICK 0 0 0 0')
  call gr_exec1('SET BOX 4 29 3 8.5')
  call gr_exec1('PEN 0')
  ! call gr_exec1('BOX N N N')
  if (gr_error()) return
  !
  ! RF axis
  !
  if (sky.eq.1) then           ! USB
    f1 = (flo1+center)/1000.d0/fshift-bandwidth/2.
    f2 = f1+bandwidth
    u1 = (flo1-center)/1000.d0/fshift+bandwidth/2.
    u2 = u1-bandwidth
    band = "USB"
  else                         ! LSB
    f1 = (flo1-center)/1000.d0/fshift+bandwidth/2.
    f2 = f1-bandwidth
    u1 = (flo1+center)/1000.d0/fshift-bandwidth/2.
    u2 = u1+bandwidth
    band = "LSB"
  endif
  !
  write(chain,1040) f1,f2
  call gr_exec1(chain)
  call gr_exec1('AXIS XL /TICK IN /LABEL P')
  call gr_exec('PEN /DASH 1')
  ! call gr_exec1('DRAW TEXT 0 -1 "Rest frequency (GHz) - '//band //'" /BOX 2')
  call gr_exec1('DRAW TEXT 0 -1 "Rest frequency (GHz)" /BOX 2')
  !
  ! Plot atmospheric transmission
  !
  if (do_atmplot) then
     call pdbi_line_atmos(f1,f2,u1,u2,plotwater,100)
  endif
  !
  ! Plot molecular lines
  !
  if (nmol.ge.1) then
     call pdbi_line_molecules(f1,f2)
  endif
  !
  ! IF1 axis
  !
  write (chain,1040) ifmin, ifmax
  call gr_exec1(chain)
  call gr_exec1('AXIS XU /TICK IN /LABEL P')
  call gr_exec1('AXIS YR /TICK NO')
  call gr_exec1('PEN 0 /DASH 3')
  call gr_exec1('DRAW RELOCATE 6000 0 /USER')
  call gr_exec1('DRAW LINE 6000 5 /USER')
  call gr_exec1('PEN 0 /DASH 1')
  !
  ! Narrow-band correlator
  !
  call pdbi_plot_narrow_widex(highband)
  !
  ! Spurious lines
  !
  if (do_spurious) then
     call pdbi_line_spurious
  endif
  !
  ! Label
  !
  call gr_exec1('PEN 0')
  call gr_exec1('DRAW TEXT -2 0 "WIDEX HIGH" 5 90 /BOX 4')
  write(chain,1080) ifmin/1e3, ifmax/1e3
  call gr_exec1('DRAW TEXT -1 0 "'//trim(chain)//'" 5 90 '//  &
  '/BOX 4')
  !
  ! End -------------------------------------------------------------------
  !
  1040 format('LIMITS ',1pg25.16,1x,1pg25.16,' 0 5')
  1080 format('IF1 = ',f3.1,' to ',f3.1,' GHz')
  !
end subroutine pdbi_plot_widex
!
subroutine pdbi_plot_narrow_widex(wideband)
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  ! Plot the narrow band correlator setup on the WIDEX plot
  !---------------------------------------------------------------------
  integer(kind=4), intent(in) :: wideband  ! Widex Band indicator
  ! Local
  integer(kind=4) :: i, old_pen, gr_spen, xoffset(5)
  real(kind=4) :: yoffset
  logical :: doplot
  character(len=132) :: chain, iflimits
  character(len=17) :: drawtext, colortext
  character(len=6) :: input1(4), input2(4)
  data input1/'Q1 HOR','Q2 HOR','Q3 VER','Q4 VER'/
  data input2/'Q1 VER','Q2 VER','Q3 HOR','Q4 HOR'/
  real(kind=4) :: qlim(2,4)
  data qlim/4200,5200,5000,6000,6000,7000,6800,7800/
  !
  if (narrow_def) then
     !
     do i = 1,2
        !
        doplot = .false.
        !
        ! Loop on narrow band entries.
        !
        if (i.eq.1) then
           colortext = 'PEN 1'   ! narrow 1 in red
           drawtext = 'NARROW 1 = '//input1(narrow_input(1))
           yoffset = 0.7
        else
           colortext = 'PEN 3'   ! narrow 2 in blue
           drawtext = 'NARROW 2 = '//input2(narrow_input(2))
           yoffset = 1.1
        endif
        if (narrow_input(i).eq.1 .and. wideband.eq.1) then ! Q1 in WIDEX LOW
           xoffset(1) = 4200
           xoffset(2) = 4470
           xoffset(3) = 4930
           xoffset(4) = 5200
           xoffset(5) = 4700
           iflimits = 'LIMITS 100 1900 0 5' ! Q1 in WIDEX LOW
           doplot = .true.
        elseif (narrow_input(i).eq.2 .and. wideband.eq.1) then ! Q2 in WIDEX LOW
           xoffset(1) = 5000
           xoffset(2) = 5270
           xoffset(3) = 5730
           xoffset(4) = 6000
           xoffset(5) = 5500
           iflimits = 'LIMITS 1900 100 0 5' ! Q2 in WIDEX LOW
           doplot = .true.
        elseif(narrow_input(i).eq.3 .and. wideband.eq.2) then ! Q3 in WIDEX HIGH
           xoffset(1) = 6000
           xoffset(2) = 6270
           xoffset(3) = 6730
           xoffset(4) = 7000
           xoffset(5) = 6500
           iflimits = 'LIMITS 100 1900 0 5' ! Q3 in WIDEX HIGH
           doplot = .true.
        elseif (narrow_input(i).eq.4 .and. wideband.eq.2) then ! Q4 in WIDEX HIGH
           xoffset(1) = 6800
           xoffset(2) = 7070
           xoffset(3) = 7530
           xoffset(4) = 7800
           xoffset(5) = 7300
           iflimits = 'LIMITS 1900 100 0 5' ! Q4 in WIDEX HIGH
           doplot = .true.
        endif
        !
        ! Draw arrows & text
        !
        if (doplot) then
           call gr_exec1(colortext//' /WEIGHT 5')
           write(chain,1050) xoffset(3), yoffset
           call gr_exec1(chain)
           write(chain,1060) xoffset(4), yoffset
           call gr_exec1(chain)
           write(chain,1050) xoffset(2), yoffset
           call gr_exec1(chain)
           write(chain,1060) xoffset(1), yoffset
           call gr_exec1(chain)
           call gr_exec1(colortext//' /WEIGHT 1 /DASH 1')
           write(chain,1070) xoffset(5), yoffset, trim(drawtext)
           call gr_exec1(chain)
           !
           ! Plot narrow band spectral units
           !
           call gr_exec1(iflimits)
           call gr_exec1(colortext//' /WEIGHT 1 /DASH 1')
           old_pen = gr_spen(1)        ! --
           old_pen = gr_spen(old_pen)  ! select current pen
           call pdbi_plot_spectral(wideband,0)
           call gr_exec1(colortext//' /WEIGHT 1 /DASH 1')
           !
           ! Back to the IF1 limits
           !
           if (wideband.eq.1) then
              call gr_exec1('LIMITS 4200 6000 0 5')
           else
              call gr_exec1('LIMITS 6000 7800 0 5')
           endif
        endif
     enddo
  else
     call gr_exec1('PEN 14 /COL 14 /WEIGHT 1')
     !
     if (wideband.eq.1) then ! WIDEX LOW: Q1 and Q2
        call gr_exec1('PEN 14 /COL 14 /WEIGHT 1')
        call gr_exec1('DRAW TEXT 4700 0.7 "Q1" 5 /USER /CLIP')
        call gr_exec1('PEN 14 /COL 14 /WEIGHT 5')
        call gr_exec1('DRAW RELOCATE 4760 0.7 /USER /CLIP')
        call gr_exec1('DRAW ARROW 5200 0.7 /USER /CLIP')
        call gr_exec1('DRAW RELOCATE 4635 0.7 /USER /CLIP')
        call gr_exec1('DRAW ARROW 4200 0.7 /USER /CLIP')
        !
        call gr_exec1('PEN 14 /COL 14 /WEIGHT 1')
        call gr_exec1('DRAW TEXT 5500 1.1 "Q2" 5 /USER /CLIP ')
        call gr_exec1('PEN 14 /COL 14 /WEIGHT 5')
        call gr_exec1('DRAW RELOCATE 5560 1.1 /USER /CLIP')
        call gr_exec1('DRAW ARROW 6000 1.1 /USER /CLIP ')
        call gr_exec1('DRAW RELOCATE 5435 1.1 /USER /CLIP')
        call gr_exec1('DRAW ARROW 5000 1.1 /USER /CLIP ')
        !
     else ! WIDEX HIGH: Q3 and Q4
        call gr_exec1('PEN 14 /COL 14 /WEIGHT 1')
	call gr_exec1('DRAW TEXT 6500 0.7 "Q3" 5 /USER /CLIP ')
        call gr_exec1('PEN 14 /COL 14 /WEIGHT 5')
        call gr_exec1('DRAW RELOCATE 6565 0.7 /USER /CLIP ')
        call gr_exec1('DRAW ARROW 7000 0.7 /USER /CLIP ')
        call gr_exec1('DRAW RELOCATE 6435 0.7 /USER /CLIP ')
        call gr_exec1('DRAW ARROW 6000 0.7 /USER /CLIP ')
        !
        call gr_exec1('PEN 14 /COL 14 /WEIGHT 1')
        call gr_exec1('DRAW TEXT 7300 1.1 "Q4" 5 /USER /CLIP')
        call gr_exec1('PEN 14 /COl 14 /WEIGHT 5')
        call gr_exec1('DRAW RELOCATE 7365 1.1 /USER /CLIP')
        call gr_exec1('DRAW ARROW 7800 1.1 /USER /CLIP')
        call gr_exec1('DRAW RELOCATE 7235 1.1 /USER /CLIP')
        call gr_exec1('DRAW ARROW 6800 1.1 /USER /CLIP')
     endif
  endif
  1050 format('DRAW RELOCATE ',i8, f10.2,' /USER /CLIP')
  1060 format('DRAW ARROW ',i8, f10.2,' /USER /CLIP')
  1070 format('DRAW TEXT 'i8, f10.2,' "',a,'" 5 /USER /CLIP')
end subroutine pdbi_plot_narrow_widex
!
subroutine pdbi_line_molecules(fmin,fmax)
  use ast_line
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Plot the molecular lines
  !---------------------------------------------------------------------
  real(kind=8) :: fmin              !
  real(kind=8) :: fmax              !
  ! Local
  logical :: error
  real(kind=8) :: fw
  integer(kind=4) :: i
  real(kind=4) :: afreq
  character(len=132) :: chain
  character(len=20) :: aname
  !
  fw=max(fmin,fmax)              ! Make sure Fmin<Fmax (depends on LSB
  fmin=min(fmin,fmax)            ! or USB)
  fmax=fw
  call gr_exec1 ('SET ORIEN 50')
  ! call gr_exec1 ('SET EXPAND 0.7')
  aname = ' '
  afreq = 0.0
  do i=1, nmol
     if (molfreq(i).ge.fmin .and. molfreq(i).le.fmax) then
        if (molname(i).ne.aname .or. abs(afreq-molfreq(i)).gt.40e-3) then
           write(chain,1000) 'DRAW TEXT ',molfreq(i),' 4. "' //  &
                trim(molname(i))//'" 5 /USER'
           aname = molname(i)
           afreq = molfreq(i)
           call gr_exec1(trim(chain))
        endif
        call gr_segm('LINE',error)
        if (width.eq.0) then
           call relocate (molfreq(i),3.d0)
           call draw (molfreq(i),2.0d0)
        else
           fw = width*molfreq(i)/299792.458
           call relocate (molfreq(i)-fw/2.,2.d0)
           call draw(molfreq(i),3.d0)
           call draw(molfreq(i)+fw/2.,2.d0)
        endif
        call gr_segm_close(error)
     endif
  enddo
  call gr_exec1 ('SET ORIEN 0')
  call gr_exec1 ('SET EXPAND 1')
  !
  1000 format(a,g14.7,a)
end subroutine pdbi_line_molecules
!
subroutine pdbi_line_atmos(fmin,fmax,umin,umax,wat,npoints)
  use gbl_message
  use gkernel_types
  use gkernel_interfaces
  use atm_interfaces_public
  use astro_interfaces, except_this=>pdbi_line_atmos
  use atm_params
  use ast_line
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  ! Plot atmospheric transmission
  !---------------------------------------------------------------------
  real(kind=8), intent(in) :: fmin        ! Signal frequency range
  real(kind=8), intent(in) :: fmax        ! Signal frequency range
  real(kind=8), intent(in) :: umin        ! Image frequency range
  real(kind=8), intent(in) :: umax        ! Image frequency range
  real(kind=4), intent(in) :: wat         ! Water vapor content
  integer(kind=4), intent(in) :: npoints  ! Number of points in the curve
  ! Local
  character(len=*), parameter :: rname='PDBI_LINE_ATMOS'
  logical :: error
  integer(kind=4) :: i, ier, old_pen
  character(len=message_length) :: mess
  real(kind=8) :: ff(npoints), transmi(npoints), tsystem(npoints)
  real(kind=8) :: tsysmax
  type(polygon_drawing_t) :: histo
  integer(kind=size_length), parameter :: nbase=1
  real(kind=8) :: base(nbase)
  !
  water = wat      ! user defined water, 1 mm as default
!!  freqs = fmin     ! frequency
  !
  ! Why, ???... does not match Umin Umax either
!  fw=max(fmin,fmax)              ! Make sure Fmin<Fmax (depends on LSB
!  fmin=min(fmin,fmax)            ! or USB)
!  fmax=fw
  !
  ! Do the job, just like a call to ATM command (cf astro_atm.f90)
  !
  do i=1,npoints
     ff(i) = fmin + (fmax-fmin)*(i-1)/(npoints-1)
     freqs = ff(i)
     call atm_transm(water,1.0,freqs,temis,tatms,tauox,tauw,taut,ier)
     transmi(i) = 5*(1-taut) ! factor 5 to use plot Y limits = [0,5]
	 freqi = (umax-umin)*(freqs-fmin)/(fmax-fmin) + umin
     call atm_transm(water,airmass,freqi,temii,tatmi,tauox,tauw,taut,ier)
     call atm_transm(water,airmass,freqs,temis,tatms,tauox,tauw,taut,ier)
     !
     ! Compute Tsys
     !
     tsystem(i) = exp(taut*airmass) * (gim * (feff*temii+(1.0-feff)*t0 + trec) +  &
           (feff*temis+(1.0-feff)*t0 + trec) ) / feff
  enddo
  tsysmax = 1.1d0*maxval(tsystem)
  tsysmax = nint(tsysmax)
  tsystem = 5D0*tsystem/tsysmax
  !
  ! Plot atmosphere
  !
  call gr_exec('PEN 10 /COL 10')
  old_pen = gr_spen(10) ! select pen 10
  call gr_segm('ATMOSFILL',error)
  ! Set up the polygon description
  histo%contoured = .false.  ! Contour will be done with another color hereafter
  histo%hatched = .false.
  histo%filled = .true.
  base(:) = 4.98d0
  call gr8_histo_hatchfill(int(npoints,kind=size_length),ff,transmi,  &
                           nbase,base,0.d0,-1.d0,histo,error)
  call gr_segm_close(error)
  !
  call gr_exec('PEN 0 /COL 0 /DASH 3')
  old_pen = gr_spen(0) ! select pen 0
  call gr_segm('ATMOS',error)
  call gr8_connect(npoints,ff,transmi,0.0,-1.0)
  call gr_segm_close(error)
  !
  ! Plot Tsys
  !
  call gr_exec1('PEN 3 /COL 3 /DASH 1')
  i = gr_spen(3)   ! Select pen 3 (blue)
  call gr_segm('TSYS',error)
  call gr8_connect(npoints,ff,tsystem,0.0,-1.0)
  call gr_segm_close(error)
  write(mess,*) tsysmax
  call gr_exec('AXIS YL 0 '//trim(mess))
  call gr_exec('DRAW TEXT -2 0 "T\\dsys\\u (K)" 5 90 /BOX 4')
  ! call gr_exec('LABEL "T\\dsys\\u (K)" /Y')
  !
  ! Back to old pen
  !
  call gr_exec('PEN 0 /COL 0 /DASH 1')
  i = gr_spen(old_pen)
  !
end subroutine pdbi_line_atmos
!
subroutine pdbi_line_spurious
  use gbl_message
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  ! Plot spurious lines. Box must be in IF1 limits.
  !---------------------------------------------------------------------
  character(len=*), parameter :: rname='PDBI_PLOT'
  character(len=132) :: chain
  ! character(len=message_length) :: mess
  !
  ! write(mess,'(A,F6.1,A)') 'Plotting possible spurious lines '// &
  !     ' (FLO1REF = ',flo1ref,' MHz)'
  ! call astro_message(seve%i,rname,mess)
  !
  call gr_exec1('PEN 2 /DASH 3')
  write(chain,1050) 3*flo1ref
  call gr_exec1(chain)
  write(chain,1060) 3*flo1ref
  call gr_exec1(chain)
  write(chain,1080) 3*flo1ref, '3*FLO1REF'
  call gr_exec1(chain)
  write(chain,1050) 4*flo1ref
  call gr_exec1(chain)
  write(chain,1060) 4*flo1ref
  call gr_exec1(chain)
  write(chain,1080) 4*flo1ref, '4*FLO1REF'
  call gr_exec1(chain)
  write(chain,1050) 6300.
  call gr_exec1(chain)
  write(chain,1060) 6300.
  call gr_exec1(chain)
  write(chain,1080) 6300., '"6300 MHz"'
  call gr_exec1(chain)
  write(chain,1050) 4500.
  call gr_exec1(chain)
  write(chain,1060) 4500.
  call gr_exec1(chain)
  write(chain,1080) 4500., '"4500 MHz"'
  call gr_exec1(chain)
  call gr_exec1('PEN 0')
  1050 format('DRAW RELOCATE ',f10.2,' 0 /USER')
  1060 format('DRAW LINE ',f10.2,' 5 /USER /CLIP')
  1080 format('DRAW TEXT 'f10.2,' 2.5 ',a,' 8 90 /USER /CLIP')
end subroutine pdbi_line_spurious
