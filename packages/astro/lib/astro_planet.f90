subroutine astro_planet(line,error)
  use gildas_def
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>astro_planet
  use ast_astro
  use ast_constant
  use ast_planets
  !---------------------------------------------------------------------
  ! @ private
  !	ASTRO Command :
  !	PLANET name|ALL SURFACE lon lat /DRAW /QUIET
  !---------------------------------------------------------------------
  character(len=*):: line           !
  logical :: error                  !
  ! Local
  character(len=*), parameter :: rname='PLANET'
  character(len=message_length) :: mess
  character(len=3) :: patch
  character(len=12) :: arg2,arg
  character(len=60) :: alpha, delta
  logical :: draw,drawclip, found, surf, visible
  real(kind=8) :: lola(2),eq(2),ho(2),lola_e(2),eqoff(2),vr, vroff, distance
  real(kind=8) :: helio_dist, pl_axes(3), tbright, flux, sunel, eqtopo(2)
  integer(kind=4) ::  na, nd, ip, nc, nf, ieph, k, nkey, mvoc1
  parameter (mvoc1=2)
  character(len=12) :: name, voc1(mvoc1), k1
  real(kind=8) :: degrad, secrad, tmb, fbeam, size, save_tdt, eqeph(2,3)
  real(kind=8) :: eq1(2),eq2(2),vreph(3),vr1,vr2
  parameter (degrad=pi/180d0,secrad=degrad/3600d0)
  logical :: ephem , dowrite,dobeam,donoclip
  real(kind=8) :: dteph(3), dte
  ! 1 hour interval for produced ephemeris
  parameter (dte=1.d0)
  integer(kind=4) :: neph, nch, ier, lun, nchp, nchv
  character(len=40) :: namef, ephfile, d_chain, ch, chp, chv
  character(len=16) :: varname
  integer(kind=4) :: nv
  real(kind=8) :: tmpde, tmpds, tmpmaj, tmpmin, tmppa, tmpsize,beam
  integer(kind=4), parameter :: optdraw=1  ! /DRAW
  integer(kind=4), parameter :: optephem=2  ! /ephem
  integer(kind=4), parameter :: optquiet=3  ! /quiet
  integer(kind=4), parameter :: optbeam=4  ! /BEAM
    integer(kind=4), parameter :: optnoclip=5  ! /NOCLIP
  character(len=1) :: drawargs(5)  ! Support for /DRAW arguments
  !
  data voc1/'CENTER','SURFACE'/
  !
  ! SAVE variables that are mapped to a SIC variable
  save ho, sunel, vr, tbright, flux, tmb, fbeam
  save tmpde, tmpds, tmpmaj, tmpmin, tmppa, tmpsize
  save alpha, delta
  !------------------------------------------------------------------------
  !
  found = .false.
  arg = 'ALL'
  call sic_ke (line,0,1,arg,nc,.false.,error)
  if (error) return
  call sic_ambigs(rname,arg,name,nkey,body,m_body+1,error)
  if (error) return
  arg = 'CENTER'
  nc = 6
  call sic_ke (line,0,2,arg,nc,.false.,error)
  if (error) return
  call sic_ambigs(rname,arg,k1,nkey,voc1,mvoc1,error)
  if (error) return
  if (k1.eq.'SURFACE') then
    surf = .true.
    call sic_r8 (line,0,3,lola(1),.true.,error)
    if (error) return
    call sic_r8 (line,0,4,lola(2),.true.,error)
    if (error) return
    lola(1) = lola(1)*degrad
    lola(2) = lola(2)*degrad
  else
    surf = .false.
    lola(1) = 0.d0
    lola(2) = 0.d0
  endif
  !
  dowrite = .not.sic_present(optquiet,0)
  ephem = sic_present(optephem,0)
  draw = sic_present(optdraw,0)
  dobeam = sic_present(optbeam,0)
  donoclip = sic_present(optnoclip,0)
  !
  ! Set /DRAW type option
  if (draw) then
    call astro_draw_parse(rname,line,optdraw,drawargs,error)
    if (error)  return
  endif
  drawclip=.true.
  if (donoclip) then
    if (.not.draw) then
      call astro_message(seve%e,rname,'Option /NOCLIP only valid in combination with /DRAW')
      error=.true.
      return
    endif
    drawclip=.false.
  endif
  !
  ! Ephemeris option to write a file with RA/DEC/Vr at the time of the computation
  if (ephem) then
    ier = sic_getlun (lun)
    ! Read file name
    namef = 'BODY'
    nc = 4
    call sic_ch(line,optephem,1,namef,nc,.false.,error)
    if (error) return
    ! Read write type: NEW = create file, APPEND = add to existing file
    arg2 = 'APPEND'
    call sic_ke(line,optephem,2,arg2,nc,.false.,error)
    ! File handling
    call sic_parsef(namef,ephfile,' ','.sou')
    ier = sic_getlun (lun)
    nf = lenc(ephfile)
    if (arg2(1:3).eq.'NEW') then
      ier = sic_open(lun,ephfile(1:nf),'NEW',.false.)
    else
      ier = sic_open(lun,ephfile(1:nf),'APPEND',.false.)
    endif
    if (ier.ne.0) goto 98
    ! need 3 times to compute drivative
    neph = 3
    dteph(1) = -dte/24d0
    dteph(2) = dte/24d0
    dteph(3) = 0d0
  else
    neph = 1
    dteph(1) = 0d0
  endif
  !
  ! Get beam size
  beam = 0d0
  if (dobeam) then
    ! to replace the default beam (from observatory) by the user's value
    call sic_r8(line,optbeam,1,beam,.true.,error)
    if (error) return
  endif
  call do_tele_beam(primbeam,beam,freq,obsname(1:4))
  !
  ! Loop on possible objects
  save_tdt = jnow_tdt
  do ip = 1, m_body
    if (name .eq. 'ALL' .or. name.eq.body(ip)) then
      do ieph = 1, neph  ! loop over times to be able to compute ra,dec,v derivative
        jnow_tdt = save_tdt + dteph(ieph)
        call do_astro_planet(rname,ip,surf,lola,eq,eqtopo, ho,sunel,vr,distance,  &
        helio_dist,pl_axes,tbright,freq,flux, visible,lola_e,eqoff,vroff,   &
        primbeam*secrad,tmb,fbeam, size,error)
        ! we use the GEOCENTRIC coordinates here ...
        ! EQEPH(1,IEPH) = EQTOPO(1)
        ! EQEPH(2,IEPH) = EQTOPO(2)
        eqeph(1,ieph) = eq(1)
        eqeph(2,ieph) = eq(2)
        vreph(ieph) = vr
      enddo
      if (ephem) then
        ! Get the first and second derivative to be written in ephem file
        do k=1,2
          eq1(k) = (eqeph(k,2)-eqeph(k,1))/2d0/dte
          eq2(k) = (eqeph(k,2)+eqeph(k,1)-2*eqeph(k,3))/dte**2
        enddo
        vr1 = (vreph(2)-vreph(1))/2d0/dte
        vr2 = (vreph(2)+vreph(1)-2*vreph(3))/dte**2
      endif
      !
      if (surf) then
        lola_e(1) =mod(540.d0+lola_e(1)/degrad,360.d0)
        lola_e(2) =lola_e(2)/degrad
        write(mess,1001) '_Sub-earth point long,lat=', lola_e,' degrees.'
        call astro_message(seve%r,rname,mess)
        eqoff(1) = eqoff(1)/secrad
        eqoff(2) = eqoff(2)/secrad
        if (visible) then
          write(mess,1001) '_Surface point offsets: ',eqoff,  &
            ' arc sec.; vel. ',vroff,'  km/s; visible.'
        else
          write(mess,1001) '_Surface point offsets: ',eqoff,  &
            ' arc sec.; vel. ',vroff,'  km/s; hidden.'
        endif
        call astro_message(seve%r,rname,mess)
      endif
      !
      if (ephem) then
        ! Prepare to write output file
        call rad2sexa(eq(1),24,alpha,4,left=.true.)
        na = len_trim(alpha)
        call rad2sexa(eq(2),360,delta,3,left=.true.)
        nd = len_trim(delta)
        eq1(1) = eq1(1)/pi*3600*12
        eq1(2) = eq1(2)/pi*3600*180
        eq2(1) = eq2(1)/pi*3600*12
        eq2(2) = eq2(2)/pi*3600*180
        call jdate_to_datetime(jnow_utc,d_chain,error)
        ! Prepare the line to print
        write(ch,'(f20.3)') eq1(1)
        nch = 20
        call sic_blanc(ch,nch)
        alpha = alpha(1:na)//','//ch(1:nch)
        na = na+nch+1
        write(ch,'(f20.3)') eq2(1)
        nch = 20
        call sic_blanc(ch,nch)
        alpha = alpha(1:na)//','//ch(1:nch)
        na = na+nch+1
        !
        write(ch,'(f20.3)') eq1(2)
        nch = 20
        call sic_blanc(ch,nch)
        delta = delta(1:nd)//','//ch(1:nch)
        nd = nd+nch+1
        write(ch,'(f20.3)') eq2(2)
        nch = 20
        call sic_blanc(ch,nch)
        delta = delta(1:nd)//','//ch(1:nch)
        nd = nd+nch+1
        !
        ! Geocentric parallax (horizontal parallax)
        write(chp,'(F20.3)') 8.794/distance*au_km
        nchp = 20
        ! topocentric coordinates: set parallax to zero (for a test)
        ! CH='0'
        ! NCH = 1
        write(ch(nch:),'(F20.3)') vr
        call sic_blanc(chp,nchp)
        !
        write(chv,'(f20.3)') vr
        nchv = 20
        call sic_blanc(chv,nchv)
        write(ch,'(f20.3)') vr1
        nch = 20
        call sic_blanc(ch,nch)
        chv = chv(1:nchv)//','//ch(1:nch)
        nchv = nchv+1+nch
        if (abs(vr2).gt.0.001) then
          write(ch,'(f20.3)') vr2
          nch = 20
          call sic_blanc(ch,nch)
          chv = chv(1:nchv)//','//ch(1:nch)
          nchv = nchv+1+nch
        endif
        !
        ! Actually write the line
        write(lun,1005) body(ip), trim(d_chain), dte*3600.,  &
        alpha(1:na),delta(1:nd), chp(1:nchp), chv(1:nchv)
      else
        call rad2sexa(eq(1),24,alpha,4,left=.true.)
        na = len_trim(alpha)
        call rad2sexa(eq(2),360,delta,3,left=.true.)
        nd = len_trim(delta)
      endif
      ho(1) = ho(1) /degrad
      ho(2) = ho(2) /degrad
      if (dowrite) then
        ! Print on screen
        if (azref.eq.'S') then
          write(mess,1000) body(ip),alpha(1:na), delta(1:nd), ho, sunel, vr
          azimuth = ho(1) * degrad
        else
          write(mess,1000) body(ip),alpha(1:na), delta(1:nd), ho(1)+180.d0,  &
          ho(2), sunel, vr
          azimuth = ho(1) * degrad + pi
        endif
        call astro_message(seve%r,rname,mess)
        if (sunel.lt.slimit) then
          write(mess,1003) body(ip),sunel
          call astro_message(seve%r,rname,mess)
        endif
        if (ip.gt.2) then
          write(mess,1002) distance/au_km, helio_dist/au_km, pl_axes(1)/secrad,  &
          pl_axes(2)/secrad, pl_axes(3)/degrad, tbright, freq, flux
          call astro_message(seve%r,rname,mess)
          write(mess,1004) freq, primbeam, tmb, fbeam, size/secrad
          call astro_message(seve%r,rname,mess)
        endif
        call astro_message(seve%r,rname,' ')
        !
        ! Store all output parameters in a SIC structure astro%planet
        !
        varname = 'ASTRO'
        if (.not.sic_varexist(varname)) then
          call sic_defstructure(varname,.true.,error)
          if (error) return
        endif
        varname = 'ASTRO%PLANET'
        if (sic_varexist(varname)) then
          call sic_delvariable(varname,.false.,error)
          if (error) return
        endif
        call sic_defstructure(varname,.true.,error)
        if (error) return
        nv = lenc(varname)
        !
        call sic_def_char(varname(1:nv)//'%NAME',body(ip),.true.,error)
        call sic_def_char(varname(1:nv)//'%RA',alpha(1:na),.true.,error)
        call sic_def_char(varname(1:nv)//'%DEC',delta(1:nd),.true.,error)
        if (azref.eq.'S') then
          call sic_def_dble(varname(1:nv)//'%AZ',ho(1),0,0,.true.,error)
        else
          ho(1) = ho(1)+180.
          call sic_def_dble(varname(1:nv)//'%AZ',ho(1),0,0,.true.,error)
        endif
        call sic_def_dble(varname(1:nv)//'%EL',ho(2),0,0,.true.,error)
        call sic_def_dble(varname(1:nv)//'%SUND',sunel,0,0,.true.,error)
        call sic_def_dble(varname(1:nv)//'%VEL',vr,0,0,.true.,error)
        tmpde = distance/au_km
        call sic_def_dble(varname(1:nv)//'%DE',tmpde,0,0,.true.,error)
        tmpds = helio_dist/au_km
        call sic_def_dble(varname(1:nv)//'%DS',tmpds,0,0,.true.,error)
        tmpmaj= pl_axes(1)/secrad
        call sic_def_dble(varname(1:nv)//'%MAJ',tmpmaj,0,0,.true.,error)
        tmpmin = pl_axes(2)/secrad
        call sic_def_dble(varname(1:nv)//'%MIN',tmpmin,0,0,.true.,error)
        tmppa = pl_axes(3)/degrad
        call sic_def_dble(varname(1:nv)//'%PA',tmppa,0,0,.true.,error)
        call sic_def_dble(varname(1:nv)//'%TB',tbright,0,0,.true.,error)
        call sic_def_dble(varname(1:nv)//'%S',flux,0,0,.true.,error)
        call sic_def_dble(varname(1:nv)//'%FREQ',freq,0,0,.true.,error)
        call sic_def_dble(varname(1:nv)//'%BEAM',primbeam,0,0,.true.,error)
        call sic_def_dble(varname(1:nv)//'%TMB',tmb,0,0,.true.,error)
        call sic_def_dble(varname(1:nv)//'%FLUX',fbeam,0,0,.true.,error)
        tmpsize = size/secrad
        call sic_def_dble(varname(1:nv)//'%SIZE',tmpsize,0,0,.true.,error)
        if (error) return
        !
      endif
      !
      ! SIC global variables
      !
      elevation = ho(2) * degrad
      ra = eq(1)
      if (ra.lt.0d0) ra = ra + 2.0d0*pi
      dec = eq(2)
      source_name = body (ip)
      s_1(1) = ra   ! LAMBDA
      s_1(2) = dec  ! BETA
      !
      z_axis = 0
      z_axis_type = 'MA'
      if (draw) then
        patch=char(92)//'g'//c_symbol(ip)
        call astro_draw(patch,-1,drawargs,drawclip,error)
      endif
    endif
  enddo
  jnow_tdt = save_tdt
  if (ephem) then
    close(lun)
    call sic_frelun (lun)
  endif
  planet_tmb = tmb
  planet_flux = fbeam
  planet_size(1) = pl_axes(1)
  planet_size(2) = pl_axes(2)
  planet_size(3) = pl_axes(3)
  return
  !
  98    call astro_message(seve%e,'PLANET','Error opening '//ephfile(1:nf))
  call putios ('E-PLANET, ',ier)
  error = .true.
  call sic_frelun (lun)
  return
  !
  1000 format (1x,a,' RA ',a,' Dec ',a,' Az ',f12.5,' El ',f12.5,' Sun.D. ',  &
       f6.1,' Vel. ',f8.3)
  1001 format (a,2f7.2,a,f7.3,a)
  1002 format(' DE=',f5.2,' DS=',f5.2,' Maj=',f6.2,' Min=',f6.2,' PA=',f6.2,  &
       ' TB=',f6.2,' S(',f6.1,')=',f8.2)
  1003 format(1x,a,' is within Sun avoidance (',f6.1,' deg)')
  1004 format(' Frequency ',f6.1,': Beam ',f6.1,'  Tmb ',f8.2,'  Flux ',f9.2,  &
       '  Size ',f6.1)
  1005 format(1x,a,' DAY ',a,1x,f7.0,1x,a,1x,a,' PA ',a,' EA ',a)
end subroutine astro_planet
!
subroutine do_astro_planet(rname,ip,surf,lola,eq,eqtopo,ho,sunel,vr,distance,  &
  helio_dist,pl_axes,tbright,fr,flux, visible,lola_e,eqoff, vroff,pbeam,tmb,  &
  fbeam,size,error)
  use astro_interfaces, except_this=>do_astro_planet
  use ast_astro
  use ast_constant
  use ast_planets
  !---------------------------------------------------------------------
  ! @ public
  !	ASTRO Command :
  !	PLANET name|ALL /DRAW
  !---------------------------------------------------------------------
  character(len=*) :: rname
  integer(kind=4) :: ip             !
  logical :: surf                   !
  real(kind=8) :: lola(2)           !
  real(kind=8) :: eq(2)             !
  real(kind=8) :: eqtopo(2)         !
  real(kind=8) :: ho(2)             !
  real(kind=8) :: sunel             !
  real(kind=8) :: vr                !
  real(kind=8) :: distance          !
  real(kind=8) :: helio_dist        !
  real(kind=8) :: pl_axes(3)        !
  real(kind=8) :: tbright           !
  real(kind=8) :: fr                !
  real(kind=8) :: flux              !
  logical :: visible                !
  real(kind=8) :: lola_e(2)         !
  real(kind=8) :: eqoff(2)          !
  real(kind=8) :: vroff             !
  real(kind=8) :: pbeam             !
  real(kind=8) :: tmb               !
  real(kind=8) :: fbeam             !
  real(kind=8) :: size              !
  logical :: error                  !
  ! Local
!   real(kind=8) :: sun_distance
  integer(kind=4) ::  i
  real(kind=8) :: x_0(3), x_2(3), v_0(3), v_2(3), result(6), trfm_32(9), x_3(3)
  real(kind=8) :: r3, s_p(2), x_p(3), x_5(3), x_4(3), mat1(9), s_4(2), psi
  real(kind=8) :: the, phi, w1, v_4(3), v_5(3), x_6(3), s_6(2), mat2(9), x, y
  real(kind=8) :: n_2(3), n_3(3), den, y_2(3), norm
  equivalence (result(1), x_0(1)), (result(4),v_0(1))
  real(kind=8) :: degrad
  parameter (degrad=pi/180d0)
  !
  !------------------------------------------------------------------------
  ! Code :
  call ephsta(vector(ip), ip, error)
  if (error) return
  !
  ! Get ecliptic J2000 coordinates
  call ephvec(jnow_tdt, 1, result, error)
  !
  ! Get Apparent Horizontal coordinates
  call matvec(x_0, trfm_20, x_2)
  call matvec(v_0, trfm_20, v_2)
  !
  ! Geocentric distance
  distance = dsqrt(x_0(1)**2 + x_0(2)**2 + x_0(3)**2)
  vr = (x_0(1)*v_0(1)+x_0(2)*v_0(2)+x_0(3)*v_0(3))/distance
  ! Heliocentric distance
  helio_dist = dsqrt((xsun_0(1)-x_0(1))**2 +(xsun_0(2)-x_0(2))**2 +  &
  (xsun_0(3)-x_0(3))**2)
  ! Light-time correction
  do i = 1, 3
    x_2(i) = x_2(i) - distance / light * v_2(i)
  enddo
  !
  ! Apparent equatorial coordinates (without parallax)
  call transp(trfm_23, trfm_32)
  call matvec(x_2, trfm_32, x_3)
  call spher(x_3, eq)
  ! half diameter, angle
  if (ip.gt.ibody_sun) then
    pl_axes(1) = 2d0*plrad(ip)/distance    ! radians,diam
    pl_axes(2) = pl_axes(1)*(1.d0-aratio(ip))
    s_p(1) = rap(ip)*pi/180.d0
    s_p(2) = decp(ip)*pi/180.d0
    call rect(s_p, x_p)
    r3 = dsqrt(x_3(1)**2+x_3(2)**2+x_3(3)**2)
    pl_axes(3) = atan2( -x_3(2)*x_p(1)+x_3(1)*x_p(2), (x_p(3)*  &
    (x_3(1)**2+x_3(2)**2) - x_3(3)*(x_p(1)*x_3(1)+x_p(2)*x_3(2)))/r3)
    pl_axes(3) = mod(pl_axes(3)+2d0*pi,2d0*pi)
    ! Brightness temperature
    if (fr.gt.0) then
      call get_planet_tbright(rname,ip,fr,helio_dist,tbright,error)
      if (error) return
      ! Flux
      flux = pl_axes(1)*pl_axes(2)*1158.1d0*fr**3 *  &
             (1.d0/(exp(0.04801d0*fr/tbright)-1.d0) -  &
              1.d0/(exp(0.04801d0*fr/2.7d0)-1.d0))
    endif
    if (surf) then
      ! x_5 = planetocentric rectangular coordinates
      ! the angular rotation is degrad*w1/86400. (rad/sec)
      w1 = (w0(ip)+wp(ip) * (jnow_utc-jzero-distance/light/86400.))
      x_5(1) = plrad(ip)*cos(lola(1))*cos(lola(2))
      x_5(2) = -plrad(ip)*sin(lola(1))*cos(lola(2))
      x_5(3) = plrad(ip)*(1.d0-aratio(ip))*sin(lola(2))
      v_5(1) = -x_5(2)*degrad*wp(ip)/86400.
      v_5(2) = x_5(1)*degrad*wp(ip)/86400.
      v_5(3) = 0.
      if (wp(ip).lt.0)  then
        x_5(2) = - x_5(2)
        v_5(2) = - v_5(2)
      endif
      ! x_4 = same, but in equatorial system of date
      psi = pi-w1*degrad
      the = pi/2d0-s_p(2)
      phi = pi/2d0-s_p(1)      !(90d0-rap(ip)) * DEGRAD
      call eulmat (psi,the,phi,mat1)
      call eulmat (-phi,-the,-psi,mat2)
      call matvec (x_5, mat1, x_4)
      call matvec (v_5, mat1, v_4)
      call matvec(x_3,mat2,x_6)
      call spher(x_6,s_6)
      lola_e(1)=-s_6(1)
      lola_e(2)=-s_6(2)
      ! radial velocity:
      vroff = (v_4(1)*x_3(1)+v_4(2)*x_3(2)+v_4(3)*x_3(3)) /sqrt(x_3(1)**2+  &
      x_3(2)**2+x_3(3)**2)
      visible = x_4(1)*x_3(1)+x_4(2)*x_3(2)+x_4(3)*x_3(3).lt.0
      ! position offsets
      do i=1, 3
        x_4(i) = x_4(i)+x_3(i)
      enddo
      call spher(x_4,s_4)
      eqoff(1) = (s_4(1)-eq(1))/cos(eq(2))
      eqoff(2) = (s_4(2)-eq(2))
    endif
    !
  endif
  !
  ! Parallax correction
  x_2(3) = x_2(3) - (radius + altitude)
  !
  ! Apparent equatorial coordinates (with parallax = topocentric)
  call matvec(x_2, trfm_32, x_3)
  call spher(x_3, eqtopo)
  !
  ! Spherical horizontal coordinates
  sunel = sun_distance (x_2)   ! Sun Angular Distance
  !
  call spher(x_2, ho)          ! Back to spherical
  ho(1) = -ho(1)
  do i=1, 2
    ho(i) = mod(ho(i)+3d0*pi,2d0*pi)-pi
  enddo
  !
  if (ip.gt.ibody_sun) then
    ! Rayleigh Jeans Brightness
    tmb = 6.62d-34/1.38d-23*fr*1e9 *(1.d0/(exp(0.04801d0*fr/tbright)-1.d0)  &
         -1.d0/(exp(0.04801d0*fr/2.7d0)-1.d0))
    ! Primary beam correction
    if (pbeam.ne.0.0d0) then
      x = (pl_axes(1)*pl_axes(2))/(pbeam/sqrt(log(2d0)))**2
      y = (1.0d0-exp(-x))
      fbeam = flux*y/x
      tmb = tmb*y
      size = sqrt(log(2d0)/2.0d0*pl_axes(1)*pl_axes(2)+pbeam**2)
    else
      fbeam = flux
      size = 0d0
    endif
  endif
  !
  ! Compute parallactic angle
  ! parallactic angle
  ! x_3 is the source vector in eq coordinates (3)
  ! n_3 is the normal to the source meridian plane
  ! TRFM_23 goes from (3) to (2)
  ! n_2 is the normal to the source meridian plane, in horiz coords.(2)
  ! x_2 is the source vector in in horiz coords.
  ! y_2 is the intersection of the vertical plane of the source and
  ! the plane of the sky
  den = sqrt(x_3(1)**2+x_3(2)**2)
  n_3(1) = -x_3(2)/den
  n_3(2) = x_3(1)/den
  n_3(3) = 0
  call matvec (n_3, trfm_23, n_2)
  den = sqrt(x_2(1)**2+x_2(2)**2)
  y_2(1) = x_2(1)*x_2(3) / den
  y_2(2) = x_2(2)*x_2(3) / den
  y_2(3) = -den
  norm = sqrt(y_2(1)**2+y_2(2)**2+y_2(3)**2)
  parallactic_angle = pi/2-acos((y_2(1)*n_2(1)+y_2(2)*n_2(2)+y_2(3)*n_2(3))/norm)
end subroutine do_astro_planet
!
subroutine get_planet_tbright(rname,ip,freq,hdist,tb,error)
  use gbl_message
  use astro_interfaces, except_this=>get_planet_tbright
  use ast_constant
  use ast_planets
  !---------------------------------------------------------------------
  ! @ private
  ! retrieve plant brightness temperature
  !---------------------------------------------------------------------
  character(len=*), intent(inout) :: rname
  integer(kind=4), intent(inout) ::ip
  real(kind=8), intent(inout) :: freq
  real(kind=8), intent(inout) :: hdist
  real(kind=8), intent(inout) :: tb
  logical, intent(inout) :: error
  !
  if (tmode(ip).eq.itmode_spidx) then
    ! old style, 1 T [+ spectral index]
    tb = temp(ip)*(freq/100.d0)**etemp(ip)
    ! Martian specials
    if (ip.eq.ibody_mar) then
      tb = tb*dsqrt(1.5236*au_km/hdist)
    endif
  else if (tmode(ip).eq.itmode_file) then
    ! Use brightness model in files
    if (.not.planet_model(ip)%reading_done) then
      call read_tbright_file(rname,tmodel_file(ip),planet_model(ip),error)
      if (error) return
    endif
    call interp_tbright_file(rname,planet_model(ip),freq,tb,error)
    if (error) return
  else
    call astro_message(seve%e,rname,'Problem with determination of the method to get brightness T')
    error=.true.
    return
  endif
  !
end subroutine get_planet_tbright
!
subroutine read_tbright_file(rname,mfile,pmodel,error)
  use gildas_def
  use gbl_message
  use astro_interfaces, except_this=>read_tbright_file
  use gkernel_interfaces
  use ast_constant
  use ast_planets
  !---------------------------------------------------------------------
  ! @ private
  ! read planet brightness temperature from file
  !---------------------------------------------------------------------
  character(len=*), intent(inout) :: rname
  character(len=*), intent(in) :: mfile
  type(planet_spec_t), intent(inout) :: pmodel
  logical, intent(inout) :: error
  !
  character(len=message_length) :: mess,mline
  character(len=filename_length) :: file
  integer(kind=4) :: lun,ier,ifr,iline
  real(kind=4) :: mflux,mtrj
  !
  !
  if (.not.sic_query_file(mfile,'data#dir:','.dat',file)) then
    write (mess,'(a,1x,a,1x,a)') 'Model file',trim(mfile),'not found'
    call astro_message(seve%e,'rname',trim(mess))
    error=.true.
    return
  endif
  !
  ier=sic_getlun(lun)
  ier=sic_open(lun,file,'OLD',.true.)
  if (ier.ne.0) then
    write (mess,'(a,1x,a)') 'Troubles opening model file:',file
    call astro_message(seve%e,rname,trim(mess))
    close(unit=lun)
    call sic_frelun (lun)
    return
  endif
  !
  ! Allocate the spectrum variables
  allocate (pmodel%freq(tmodel_mlines),pmodel%tb(tmodel_mlines),stat=ier)
  if (ier.ne.0) then
    call astro_message(seve%e,rname,'Problem allocating variables')
    error=.true.
    close(unit=lun)
    call sic_frelun (lun)
    return
  endif
  ! read the file
  open (unit=lun,file=file,status='old')  
  ifr=0
  do while(ier.eq.0)
    read(lun,'(a)',iostat=ier) mline
    if (ier.ne.0.or.mline(1:1).eq."!") cycle
    ifr=ifr+1
    read(mline,*) iline,pmodel%freq(ifr),pmodel%tb(ifr),mflux,mtrj
  enddo
  close(lun)
  call sic_frelun (lun)
  pmodel%n_freq=ifr
  pmodel%reading_done=.true.
  !
end subroutine read_tbright_file
!
subroutine interp_tbright_file(rname,pmodel,freq,tb,error)
  use gildas_def
  use gbl_message
  use astro_interfaces, except_this=>interp_tbright_file
  use gkernel_interfaces
  use ast_constant
  use ast_planets
  !---------------------------------------------------------------------
  ! @ private
  ! interpolation of brightness temperature in stored model
  !---------------------------------------------------------------------
  character(len=*), intent(inout) :: rname
  type(planet_spec_t), intent(inout) :: pmodel
  real(kind=8), intent(inout) :: freq
  real(kind=8), intent(inout) :: tb
  logical, intent(inout) :: error
  !
  character(len=message_length) :: mess
  character(len=filename_length) :: file
  integer(kind=8) :: ifr
  logical :: found
  !
  if (freq.ge.pmodel%freq(pmodel%n_freq)) then
    ! beyond max frequency
    if (freq.gt.pmodel%freq(pmodel%n_freq)) then
      call astro_message(seve%w,rname,'Frequency greater than model limit. Use last model value')
    endif
    tb=pmodel%tb(pmodel%n_freq)
  else if (freq.le.pmodel%freq(1)) then
    ! before 1st frequency
     if (freq.lt.pmodel%freq(1)) then
      call astro_message(seve%w,rname,'Frequency lower than model limit. Use first model value')
    endif
    tb=pmodel%tb(1)
  else
    ! get position of freq in the model table 
    call gr8_dicho(pmodel%n_freq,pmodel%freq(1:pmodel%n_freq),freq,.true.,ifr,error)
    if (error) return
    ! (.true. means ceiling mode. ifr is the range above the searched freq) 
    ! do the linear interpolation
    tb=pmodel%tb(ifr-1)+(freq-pmodel%freq(ifr-1))*(pmodel%tb(ifr)-pmodel%tb(ifr-1))/(pmodel%freq(ifr)-pmodel%freq(ifr-1))
  endif
  !
end subroutine interp_tbright_file
