module ast_horizon
! Old horizon.inc
      integer(kind=4), parameter :: m_horiz=9
      integer(kind=4), parameter :: m_source=1000
!
      real(kind=8) :: horiz(m_horiz), rise(m_source, m_horiz)
      real(kind=8) :: set (m_source, m_horiz), transit(m_source)
      real(kind=8) :: declin(m_source), righta(m_source),dsun(m_source)
      real(kind=8) :: sunriz(4),sunset(4),moonphse, moonmag
      integer(kind=4) :: n_horiz, next_source(2), n_rise
      logical :: airmass, night_mark
!
      character(len=36) :: c_rise(m_source)
      integer(kind=4) :: min_elev(m_source)
!
! Old projec.inc
      character(len=4) :: c_proj(m_source)
      character(len=8) :: c_freq(m_source)
      character(len=4) :: c_weig(m_source)
end module ast_horizon
!
module ast_line
! PDBI/ALMA/PICO receiver/correlator setup
      integer(kind=4) :: obs_year
      integer(kind=4) :: alma_cycle
! NOEMA mode (ONLINE or OFFLINE)
      character(len=8) :: noema_mode
!
! spurious lines
      real(kind=8) :: flo1ref
      logical :: do_spurious
!
! correlator setup
      logical :: narrow_def
      integer(kind=4) :: narrow_input(2)
      integer(kind=4), parameter :: nunit=8
      logical :: unit_def(nunit)
      integer(kind=4) :: unit_band(nunit)
      real(kind=4) :: unit_cent(nunit)
      integer(kind=4) :: unit_wind(nunit)
      integer(kind=4) :: unit_bmode(nunit)
!
! ALMA correlator setup
      logical :: bb_def(4)
      real(kind=4) :: bb_cfreq(4)
      integer(kind=4) :: bb_sideband(4)
      integer(kind=4) :: bb_nspw(4)
      real(kind=4) :: tolerance_freq
!
      integer(kind=4) :: maxsp
      parameter(maxsp=40)
      real spw_width(4,maxsp)
      real spw_cfreq(4,maxsp)
      integer spw_polar(4,maxsp)
      real spw_use(4,maxsp)
      integer spw_sideband(4,maxsp)
!---------------------------------------------
!
! Molecules
      integer nmol
    integer, parameter :: mmol=1000
      character(len=20) molname(mmol)
      real(8) molfreq(mmol)
      real(4) width
!------------------------------------
!
! plot
      real(8) flo1
      integer plot_mode
      real(4) iflim(2), userlim(2)
      integer sky
      character(len=132) label
!
! for plot /atmosphere
      logical do_atmplot
      real plotwater
!
! for print command
      character(len=80) linecomm
      integer recband
end module ast_line

module ast_constant
    real(kind=8),  parameter :: pi=3.141592653589793d0
    real(kind=8),  parameter ::  halfpi=pi/2d0
    real(kind=8),  parameter :: twopi = pi*2d0
    real(kind=8),  parameter :: j2000 = 2451545.0d0
    real(kind=8),  parameter :: light = 299792.458d0   ! km/s
    real(kind=8),  parameter :: radius=6367.435d0      ! earth radius in kilometers
    real(kind=8), parameter :: au_m=149597870700d0 !astronomical unit in m
    real(kind=8), parameter :: au_km=149597870.700d0 !astronomical unit in m
    real(kind=8), parameter :: astro_nullr8=-1.2345d36
    real(kind=4), parameter :: astro_nullr4=-1.2345e36
    real(kind=8), parameter :: astro_nulli8=-12345678
    real(kind=4), parameter :: astro_nulli4=-12345678
    character(len=4), parameter :: astro_nulla='NULL'
end module ast_constant
!
module ast_params
  integer(kind=8), parameter :: sourcename_length=128
  !
end module ast_params
!
module ast_planets
  use gildas_def
  use ast_ephem
  !
  integer(kind=4), parameter :: m_body = 9
  character(len=12), parameter :: body(m_body+1)=(/ 'MOON        ', 'SUN         ', &
                    'MERCURY     ', 'VENUS       ', 'MARS        ', 'JUPITER     ', &
                    'SATURN      ', 'URANUS      ', 'NEPTUNE     ', 'ALL         '/)
  ! Body id
  !-----------
  integer(kind=4), parameter :: ibody_moo=1
  integer(kind=4), parameter :: ibody_sun=2
  integer(kind=4), parameter :: ibody_mer=3
  integer(kind=4), parameter :: ibody_ven=4
  integer(kind=4), parameter :: ibody_mar=5
  integer(kind=4), parameter :: ibody_jup=6
  integer(kind=4), parameter :: ibody_sat=7
  integer(kind=4), parameter :: ibody_ura=8
  integer(kind=4), parameter :: ibody_nep=9
  integer(kind=4), parameter :: ibody_all=10
  ! Plot Symbol 
  !-------------
  character(len=1), parameter :: c_symbol(m_body)=(/':', '0', '1', '2', '4', '5', '6', '7', '8'/)
  ! Body properties
  !------------------
  ! Planet Radius from Astronomical Almanac 2015 (page E6):
  real(kind=8), parameter :: plrad(m_body) = (/1737.4d0,0.d0,2439.7d0,6051.8d0,&
                                 3396.2d0,71492.d0,60268.d0,25559.d0,24764.d0/)    ! in km
  ! Aspect ratio = oblateness=flatening from Ast. Almanac 2015 (page E6):
  ! aratio=(Feq-Fpol)/Feq)
  real(kind=8), parameter :: aratio(m_body) = (/0d0,0d0,0d0,0d0, 0.005886d0, &
                                    0.064874d0,0.097962d0,0.022927d0,0.017081d0/) 
  ! Pole coordinates from Astronomical Almanac 2015 (page E5):
  real(kind=8), parameter :: rap(m_body) = (/0.d0, 0.d0, 281.00d0, 272.76d0, 317.67d0, &
                                              268.06d0, 40.58d0,257.31d0, 299.43d0/)
  real(kind=8), parameter :: decp(m_body) = (/0.d0, 0.d0, 61.41d0, 67.16d0, 52.88d0, &
                                            64.50d0,83.54d0,-15.18d0, 42.95d0/)
  ! origin of time                                          
  real(kind=8), parameter :: jzero=2449352.4993381481d0 ! 1994 January 0, 0h tdt.
  !Argument of prime meridian (Ast. Almanac 1994 (page E87)) -
  ! Go along with jzero defined above
  real(kind=8), parameter :: w0(m_body) = (/0d0, 0d0, 191.04d0, 168.12d0, 166.20d0, &
                                            354.77d0,53.27d0,277.31d0, 147.30d0/)
                                            
  real(kind=8), parameter :: wp(m_body) = (/0d0, 0d0, 6.1385025d0, -1.4813675d0, 350.8919830d0, &
                                          870.536d0,810.7939024d0, -501.1600928d0, 536.3128492d0/)
  !Variable for britghness temperature
  ! Comparison of results of the 2 methods decribed in IRAM report 2024-01
  integer(kind=4), parameter :: itmode_spidx=1 ! computation legacy method
  integer(kind=4), parameter :: itmode_file=2  ! tabulated model values
  integer(kind=4), parameter :: tmode(m_body) = (/itmode_spidx,itmode_spidx,itmode_spidx,itmode_spidx,itmode_spidx, &
                                                itmode_file,itmode_file,itmode_file,itmode_file/)
  !Brightness temperatures @ 100GHz (3mm) - legacy method
  real(kind=8), parameter :: temp(m_body) = (/200.d0,5700.d0,450.d0,350.d0, &
                                          207.d0,170.d0,150.d0,132.d0,125.d0/) ! Avant Marten
                                    ! 207.D0,170.D0,150.D0,118.D0,119.D0/       ! D'apres A.Marten
  ! spectral index for T - legacy method
  real(kind=8), parameter :: etemp(m_body) = (/0.d0,0.d0,0.d0,-0.35d0, &
                                          0.d0,0.d0,0.d0,-0.35d0,-0.35d0/)  ! Avant Marten
  !                                       0.D0,0.D0,0.D0,-0.22D0,-0.20D0/        ! D'apres A.Marte
  ! Model files
  ! Files provided in 2017 by R. MORENO (LESIA - OBS PARIS)
  character(len=filename_length), parameter :: tmodel_file(m_body)=(/'NONE               ','NONE               ',   &
                                                                     'NONE               ','NONE               ',  &
                                                                     'NONE               ', &            
                                                         'jup_esa2_2_i_1000  ','sat_esa2_2_i_1000  ', &
                                                         'ura_esa2_2_i_h_1000','nep_esa5_2_i_1000  '/)
  ! Number of lines max in the model (can be adpated later)
  integer(kind=8), parameter :: tmodel_mlines = 1024
  !vector (to compute positions, vsop, etc)
  integer(kind=4), parameter :: vector(m_body)=(/ ivect_moon, ivect_sun, ivect_pla, ivect_pla, &
                                         ivect_pla, ivect_pla, ivect_pla, ivect_pla, ivect_pla/)
  ! Variables
  type :: planet_spec_t
    ! planet spectra read from files
    logical :: reading_done=.false.
    integer(kind=8) :: n_freq
    real(kind=8), allocatable :: freq(:)
    real(kind=8), allocatable :: tb(:)
  end type planet_spec_t
  !
  type(planet_spec_t) :: planet_model(m_body)
  !
end module ast_planets
!
module ast_astro
      use gildas_def
      use ast_params
      !
      logical :: library_mode  ! Astro loaded in Library or Interactive mode?
      !
! astro.inc
      real(kind=8) :: jnow_utc              ! date julienne utc
      real(kind=8) :: jnow_ut1              ! date julienne ut1
      real(kind=8) :: jnow_tdt              ! date julienne tdt
      real(kind=8) :: d_ut1                 ! ut1 - utc (seconds)
      real(kind=8) :: d_tdt                 ! tdt - utc (seconds)
      real(kind=8) :: lst                   ! local "true" sidereal time
      real(kind=8) :: lonlat(2)             ! e-longitude, latitude in degrees
      real(kind=8) :: altitude              ! altitude in kilometers
      real(kind=8) :: xsun_0(3)             ! earth_sun vector, in r0 , in km
      real(kind=8) :: xsun_2(3)             ! sun az-el at current time
      real(kind=8) :: xg_0(3)               ! earth_g vector, in r0 , in km
      real(kind=8) :: vg_0(3)               ! earth_g vector derivative, in r0 , in km/s
      real(kind=8) :: trfm_30(9)            ! matrix r0 to r3
      real(kind=8) :: trfm_20(9)            ! matrix r0 to r2
      real(kind=8) :: trfm_23(9)            ! matrix r3 to r2
      real(kind=8) :: trfm_43(9)            ! matrix r3 to r4
      real(kind=8) :: trfm_05(9)            ! matrix r5 (day ecliptic) to r0 (j2000 ecliptic)
      real(kind=8) :: s_1(2)                ! source reference coords.
      real(kind=8) :: azimuth, elevation    ! horizontal coords
      real(kind=8) :: ra, dec               ! present_day equatorial coords
      real(kind=8) :: parallactic_angle     ! parallactic angle
      real(kind=8) :: azimuth_old, elevation_old  ! horizontal coords
      real(kind=8) :: ra_old, dec_old       ! present_day equatorial coords
      real(kind=8) :: slimit                ! sun avoidance radius
      real(kind=8) :: fshift                ! frequency ratio due to doppler effect
      real(kind=8) :: freq                  ! frequency (rest) in ghz
      real(kind=8) :: primbeam              ! telescope primary beam width in radian (value used in computations)
      real(kind=4) :: z_axis
      logical :: projection
      integer(kind=4) :: name_out
      character(len=sourcename_length) :: source_name
      ! Possible values for sourcetype
      integer(kind=4), parameter :: msoukind=4
      integer(kind=4), parameter :: soukind_none=1      ! no source defined
      integer(kind=4), parameter :: soukind_full=2      ! source defined, full computations
      integer(kind=4), parameter :: soukind_vlsr=3      ! use only LSR
      integer(kind=4), parameter :: soukind_red=4      ! use only Redshift
      character(len=12), parameter ::soukinds(msoukind)= (/ 'NO SOURCE   ','ASTRO SOURCE', &
                                                            'LSR         ','REDSHIFT    ' /)
      integer(kind=4) :: soukind=soukind_none
!
! Support for ASTRO%SOURCE% variable
      character(len=60) :: source_alpha, source_delta
      real(kind=8):: source_az, source_el
      real(kind=8):: source_ra, source_dec
      real(kind=8) :: source_jutc
      character(len=sourcename_length) :: astro_source_name, source_date_chain
      real(kind=8) :: source_vlsr, source_dop, source_vshift, source_lsr
      real(kind=8) :: source_redshift
! Support for ASTRO%SOURCE%IN variable
      character(len=2)  :: source_incoord
      character(len=2)  :: source_invtype
      character(len=14) :: source_inbetasexa,source_inlambdasexa
      real(kind=4)      :: source_ineq
      real(kind=8)      :: source_invelocity,source_inredshift     
! To save the current astro source      
      type :: astro_source_mem
        integer(kind=4)   :: soukind
        character(len=sourcename_length) :: name
        character(len=2)  :: coord
        real(kind=4)   :: equinox
        real(kind=8)      :: lambda  ! radians
        real(kind=8)      :: beta  ! radians
        character(len=2)  :: vtype
        real(kind=8) :: velocity
        real(kind=8) :: redshift
      end type astro_source_mem

!       Planet stuff
      real(kind=4) :: planet_tmb            ! planet main beam brightness
      real(kind=4) :: planet_flux           ! planet flux
      real(kind=4) :: planet_size(3)        ! planet size
!
      character(len=2) :: z_axis_type
      character(len=12) :: frame           ! 'horizontal','equatorial' display
      character(len=filename_length) :: catalog_name(2) ! for sources
      character(len=1) :: azref
!       character(len=2) :: vtype           ! velocity system ('ls', 'ea', 'he','re')
      character(len=16) :: obsname         ! observatory name
!
! For OMS interaction
      character(len=16) :: oms_caller="NULL"  ! oms or pms or sms or dms...
      logical :: oms_bugw20=.false.   ! to let ASTRO know we are dealing with the special semester w20
! For plots
      integer(kind=4) :: msplot, nsplot
      parameter (msplot=2000)
      real(kind=8) :: xsplot(msplot), ysplot(msplot), zsplot(msplot)
      character(len=sourcename_length) :: splot(msplot)
      character(len=filename_length) :: ccplot(msplot)
      ! For UV tracks
      integer(kind=4), parameter :: msize=20  ! max number of differnt sizes for an observatory
      integer(kind=4), parameter :: msta=100  ! max number of selected stations
      !
      type :: uv_track_in_t
        ! Describe input of uvtrack command
        logical :: three_digit_name_compatible
        logical :: do_frame,do_hour,do_table,do_ho,do_int,do_stations,do_size,do_offset,do_weight
        logical :: do_all
        real(kind=4) :: in_h1,in_h2 !  /HOUR_ANGLE option user input
        real(kind=4) :: h1,h2 ! /HOUR_ANGLE option actually used value
        real(kind=4) :: horizon !  /HOUR_ANGLE option
        real(kind=4) :: integ_time ! /INT option
        real(kind=4) :: user_array ! if argument of /FRAME option
        ! For / WEight
        integer(kind=4) :: wkey
        ! For /SIZE
        integer(kind=4) :: nsize
        real(kind=4) :: sizes(msize)      ! size
        integer(kind=4) :: nant_size(msize)  ! number of antennas of this size
        real(kind=4) :: offset            ! option /OFFSET (delay line)
        ! For the stations
      end type uv_track_in_t
      !
      type:: uv_track_station_t
        character(len=12) :: name
        real(kind=4) :: x,y,z ! station coordinates
        real(kind=4) :: t ! alma case
        real(kind=4) :: asize ! antenna size
        logical :: found
      end type uv_track_station_t
      !
      type :: uv_track_angles_t
        real(kind=4) :: sd ! sine of declination
        real(kind=4) :: cd ! cosine of dec
        real(kind=4) :: sp ! sine of lat
        real(kind=4) :: cp ! cosine of lat
        real(kind=4) :: zsh ! depends on h
        real(kind=4) :: zch ! depends on h
      end type uv_track_angles_t
      !
      type :: uv_track_t
        type(uv_track_in_t) :: in
        type(uv_track_angles_t) :: ang
        integer(kind=4)   :: n_stations
        type(uv_track_station_t) :: stations(msta)
      end type uv_track_t
      !
  !
end module ast_astro
!
module atm_params
  !---------------------------------------------------------------------
  ! ATM input and output parameters.
  ! For portability, SAVE Fortran variables which are target of SIC
  ! variables.
  !---------------------------------------------------------------------
  real(kind=4), save :: t0,p0,p1,h0,water,airmass
  real(kind=4), save :: freqs,freqi,feff,trec,gim,tsys,tant
  real(kind=4), save :: tauox,tauw,taut,temis,temii,paths,pathi,tatms,tatmi
  logical, save :: atm_initdone=.false.
  !
  ! Noise information
  real(kind=4) :: bandwidth
  real(kind=4) :: jy_per_k
  real(kind=4) :: inttime
  real(kind=4) :: uvweight
end module atm_params
