subroutine pdbi_plot(line,error)
  use gildas_def
  use gkernel_interfaces
  use gbl_message
  use atm_params
  use ast_line
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  ! PDBI internal routine, support for command
  !	PLOT [LINE|NARROW|WIDEX]
  !         /LIMITS min max                             (1)
  !         /MOLECULES FileName [Mol1 Mol2 ... Moln]    (2)
  !         /WIDTH width                                (3)
  !         /ATMOSPHERE                                 (4)
  !         /SPURIOUS                                   (5)
  !---------------------------------------------------------------------
  character(len=*):: line           !
  logical :: error                  !
  ! Local
  integer(kind=4) :: kmol, nc, mkeys, nkey, bb
  character(len=256) :: molfile
  character(len=12) :: mode
  character(len=9) :: rname1, rname2, rname
  ! Data
  data rname1 /'PDBI_PLOT'/
  data rname2 /'ALMA_PLOT'/
  data rname /'PLOT'/
  parameter(mkeys=5)
  character(len=12) :: key, keys(mkeys)
  character(len=128) :: mess
  ! Data
  data keys/'LINE','NARROW','WIDEX','FREQUENCY','BASEBAND'/
  ! ---------------------------------------------------------------------------
  !
  ! PDBI: PLOT LINE | NARROW | WIDEX
  ! ALMA: PLOT FREQ | BASE i
  !
  if (flo1.eq.0) then
     call astro_message(seve%e,rname,'LINE command not yet executed')
     error = .true.
     return
  endif
  !
  if (sic_present(0,1)) then
     call sic_ke (line,0,1,mode,nc,.true.,error)
     if (error) return
     call sic_ambigs('rname1',mode,key,nkey,keys,mkeys,error)
     if (error) return
     if (key.eq.'LINE') then
        plot_mode = 1
     elseif (key.eq.'NARROW') then
        if (.not.narrow_def) then
           call astro_message(seve%e,rname1,'NARROW entries not defined')
           error = .true.
           return
        else
           plot_mode = 2
        endif
     elseif (key.eq.'WIDEX') then
        plot_mode = 3
     elseif (key.eq.'FREQUENCY') then
        plot_mode = 10
     elseif (key.eq.'BASEBAND') then
        call sic_i4(line,0,2,bb,.true.,error)
        if (error) return
        if (bb.eq.1) then
           plot_mode = 11
        elseif (bb.eq.2) then
           plot_mode = 12
        elseif (bb.eq.3) then
           plot_mode = 13
        elseif (bb.eq.4) then
           plot_mode = 14
        else
           call astro_message(seve%e,rname2,'Wrong BASEBAND number')
           error = .true.
           return
        endif
     else
        call astro_message(seve%e,rname1,'Unknown plotting mode')
        error = .true.
        return
     endif
  endif
  if (plot_mode.lt.10) then !PDBI
	  rname = rname1
  else if (plot_mode.lt.20) then !ALMA
  	rname = rname2
  else
    call astro_message(seve%e,rname,'Plot mode not understood (1)')
    error=.true.
    return
  endif
  !
  ! PLOT /LIMITS min max
  !
  userlim(1) = iflim(1)
  userlim(2) = iflim(2)
  if (sic_present(1,0)) then
    if ((plot_mode.ne.1).and.(plot_mode.ne.10)) then
      call astro_message(seve%w,rname,'/LIMITS option ignored')
      goto 100
    endif
    call sic_r4(line,1,1,userlim(1),.false.,error)
    call sic_r4(line,1,2,userlim(2),.false.,error)
    if (error) return
    if (userlim(1).ge.userlim(2)) then
      call astro_message(seve%e,rname,'Wrong IF limits')
      error = .true.
      return
    endif
    if (userlim(1).lt.iflim(1)) then
      userlim(1) = iflim(1)
    endif
    if (userlim(2).gt.iflim(2)) then
      userlim(2) = iflim(2)
    endif
  endif
  100  continue
  !
  ! PLOT /MOLECULE filename [molecules]
  !
  if (sic_present(2,1)) then
    call sic_ch (line,2,1,molfile,nc,.false.,error)
    if (error) return
  else
    molfile = 'gag_molecules'
  endif
  !
  ! Not convinced that it must be a SIC_QUERY_FILE ! SG
  if (.not.sic_query_file(molfile,'data#dir:','.dat',molfile)) then
    call astro_message(seve%e,rname,molfile(1:lenc(molfile))//  &
    ' not found')
    error = .true.
    return
  endif
  kmol = sic_narg(2)
  call read_lines(line,kmol,molfile)
  !
  ! PLOT /WIDTH linewidth
  !
  width = 0
  call sic_r4(line,3,1,width,.false.,error)
  if (error) return
  width = abs(width)
  !
  ! PLOT /ATMOSPHERE water
  !
  plotwater = 1
  do_atmplot = .false.
  if (sic_present(4,0)) then
     do_atmplot = .true.
     if (sic_present(4,1)) then
        call sic_r4(line,4,1,plotwater,.false.,error)
        if (error) return
        plotwater = abs(plotwater)
     endif
     write(mess,'(A,F4.1,A)') 'Plotting atmospheric transmission for ', &
          plotwater,' mm water'
     call astro_message(seve%i,rname,mess)
     write(mess,'(A,F4.1,A)') 'Plotting system temperature for ', &
          plotwater,' mm water'
     call astro_message(seve%i,rname,mess)
     write(mess, '(A)') 'Use command ATMOSPHERE /PRINT to list' // &
          ' all input parameters'
     call astro_message(seve%i,rname,mess)
     h0 = altitude
     p1 = p0*2.0**(-h0/5.5)     ! Pressure at altitude H0
     call atm_atmosp(t0,p1,h0)
  endif
  !
  ! PLOT /SPURIOUS NO
  !
  if (plot_mode.lt.10) then
     do_spurious = .true. ! default is plotting the spurious lines
     if (sic_present(5,0)) then
        if (sic_present(5,1)) then
           do_spurious = .false.
        endif
     endif
     if (do_spurious) then
       ! Message moved to pdbi_plot_line subroutine
       ! write(mess,'(A,F8.2,A)') 'Plotting possible spurious lines '// &
       !      ' (FLO1REF = ',flo1ref,' MHz)'
       ! call astro_message(seve%i,rname,mess)
     endif
  endif
  !
  ! Now produce the plot
  !
  if (plot_mode.lt.10) then
     call pdbi_plot_line(error)
  else if (plot_mode.lt.20) then
     call alma_plot_line
  else
    call astro_message(seve%e,rname,'Plot mode not understood (2)')
    error=.true.
    return
  endif
  !
end subroutine pdbi_plot
!
! ---------------------------------------------------------------------------
!
subroutine pdbi_plot_def(error)
  use gildas_def
  use gbl_message
  use gkernel_interfaces
  use ast_line
  !---------------------------------------------------------------------
  ! @ public
  ! Defaults paramaters for the plot
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Local
  character(len=*), parameter:: rname='PLOT'
  integer(kind=4) :: kmol
  character(len=1) :: dummy
  character(len=256) :: molfile
  !
  if (plot_mode.lt.10) then ! Bure Case
    iflim(1) = 4000
    iflim(2) = 8000
  else
    call astro_message(seve%e,rname,'Plot mode not understood (3)')
    error=.true.
    return
  endif
  !
  userlim = iflim
  width = 0
  plotwater = 1
  do_atmplot = .false.
  do_spurious = .true.
  !
  if (.not.sic_query_file('gag_molecules','data#dir:','.dat',molfile)) then
    call astro_message(seve%e,'PDBI_PLOT','gag_molecules not found')
    error=.true.
    return
  endif
  kmol = 0
  call read_lines(dummy,kmol,molfile)
  !
end subroutine pdbi_plot_def
!
subroutine alma_plot_def(error)
  use gildas_def
  use gbl_message
  use gkernel_interfaces
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  ! Defaults paramaters for the plot
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Local
  integer(kind=4) :: kmol
  character(len=1) :: dummy
  character(len=256) :: molfile
  !
  userlim = iflim ! iflim depends on receiver
  width = 0
  plotwater = 1
  do_atmplot = .false.
  do_spurious = .false.
  !
  if (.not.sic_query_file('gag_molecules','data#dir:','.dat',molfile)) then
    call astro_message(seve%e,'ALMA_PLOT_DEF','gag_molecules not found')
    error=.true.
    return
  endif
  kmol = 0
  call read_lines(dummy,kmol,molfile)
  !
end subroutine alma_plot_def
!
!
subroutine read_lines(line,kmol,molfile)
  use gildas_def
  use gkernel_interfaces
  use gbl_message
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*):: line           !
  integer(kind=4) :: kmol           !
  character(len=*):: molfile        !
  ! Local
  character(len=*), parameter :: rname='LINE'
  character(len=256) :: chain
  character(len=20) :: aname
  real(kind=8) :: afreq
  integer(kind=4) :: i,lun,nc,nv,ier
  integer(kind=4) :: icsv, iend
  logical :: error
  character(len=16) :: varname
  !
  error = .false.
  !
  ! Molecule list - if any
  !
  if (kmol.gt.1) then
    kmol = kmol-1
    do i=1,kmol
      call sic_ch (line,2,i+1,molname(mmol-i+1),nc,.false.,error)
      if (error) return
    enddo
  else
    kmol = 0
  endif
  !
  ! Open file
  !
  nmol = 0
  ier = sic_getlun(lun)
  if (ier.ne.1) then
    call astro_message(seve%e,rname,'Cannot allocate LUN')
    call putios('E-LINE,  ',ier)
    return
  endif
  open(unit=lun,file=molfile(1:lenc(molfile)), status='OLD',iostat=ier)
  if (ier.ne.0) then
    call astro_message(seve%e,rname,'Cannot open file: ')
    call astro_message(seve%e,rname,molfile)
    call putios('E-LINE,  ',ier)
    goto 9
  endif
  !
  ! Read file
  !
5 read(lun,'(A)',iostat=ier,end=9) chain
  if (ier.ne.0 .or. lenc(chain).eq.0) goto 5
  if (chain(1:1).ne.'!') then
    icsv = index(chain,';')
	if (icsv.gt.1) then
		read(chain(1:icsv-1),*,err=5) afreq
		aname = chain(icsv+1:)
		iend = index(aname,';')
		aname(iend:) = ' '
	else if (icsv.eq.1) then
		goto 5
	else
		read(chain,*,err=5) afreq, aname
	endif
    if (kmol.eq.0) then
      nmol = nmol + 1
      molfreq(nmol) = afreq
      molname(nmol) = aname
      if (nmol.le.mmol) goto 5
    else
      do i=mmol-kmol+1,mmol
        if (molname(i).eq.aname) then
          nmol = nmol+1
          molname(nmol) = aname
          molfreq(nmol) = afreq
        endif
      enddo
      if (nmol.le.mmol-kmol) goto 5
    endif
  else
    goto 5
  endif
9 close(unit=lun)
  call sic_frelun(lun)
  !
  ! Create the SIC substructure (Delete it first if needed)
  !
  varname = 'ASTRO'
  if (.not.sic_varexist(varname)) then
    call sic_defstructure(varname,.true.,error)
    if (error) return
  endif
  varname = 'ASTRO%LINE'
  if (sic_varexist(varname)) then
    call sic_delvariable(varname,.false.,error)
    if (error) return
  endif
  call sic_defstructure(varname,.true.,error)
  if (error) return
  nv = lenc(varname)
  call sic_def_inte(varname(1:nv)//'%N',nmol,0,0,.true.,error)
  call sic_def_dble(varname(1:nv)//'%FREQ',molfreq,1,nmol,.true.,error)
  call sic_def_charn(varname(1:nv)//'%NAME',molname,1,nmol,.true.,error)
  if (error) return
  !
end subroutine read_lines
!
! ---------------------------------------------------------------------------
!
subroutine pdbi_plot_sync(c_flo1,c_plot_mode,c_sky, c_narrow_def,           &
  c_narrow_input,c_unit_def,c_unit_band, c_unit_cent,c_unit_wind,c_fshift,  &
  c_label)
  use ast_line
  use ast_astro
  !---------------------------------------------------------------------
  ! @ public
  ! This subroutine can be called to synchronize astro-global variables
  ! with ones local to a module. This prevents sharing the astro-global
  ! variables with other modules.
  !---------------------------------------------------------------------
  real(kind=8) :: c_flo1                 !
  integer(kind=4) :: c_plot_mode         !
  integer(kind=4) :: c_sky               !
  logical :: c_narrow_def                !
  integer(kind=4) :: c_narrow_input(2)   !
  logical :: c_unit_def(nunit)           !
  integer(kind=4) :: c_unit_band(nunit)  !
  real(kind=4) :: c_unit_cent(nunit)     !
  integer(kind=4) :: c_unit_wind(nunit)  !
  real(kind=8) :: c_fshift               !
  character(len=132) :: c_label          !
  !
  flo1 = c_flo1
  plot_mode = c_plot_mode
  sky = c_sky
  narrow_def = c_narrow_def
  narrow_input = c_narrow_input
  unit_def = c_unit_def
  unit_band = c_unit_band
  unit_cent = c_unit_cent
  unit_wind = c_unit_wind
  label = c_label
  fshift = c_fshift
  !
end subroutine pdbi_plot_sync
