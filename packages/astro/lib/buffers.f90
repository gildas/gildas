module plot_molecules_globals
  use astro_types
  !
  type(plot_molecules_t), save :: molecules
  !
end module plot_molecules_globals
!
module frequency_axis_globals
  use astro_types
  !
  type(frequency_axis_t), save :: freq_axis
  !
end module frequency_axis_globals
!
!
module my_receiver_globals
  use astro_noema_type
  !
  type(noema_t), save          :: noema          ! everything about NOEMA
  type(emir_t),  save          :: emir           ! emir global
  type(current_boxes_t),  save :: cplot          ! current plot
  type(spw_t)                  :: spw2           ! spw collection 2nd list (when comparison are done)
  !
end module my_receiver_globals
