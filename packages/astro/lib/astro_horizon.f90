subroutine astro_horizon(line, error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>astro_horizon
  use ast_horizon
  use ast_constant
  use ast_astro
  use ast_planets
  !---------------------------------------------------------------------
  ! @ private
  !	ASTRO command HORIZON
  !
  !     HORIZON h1 h2 h3 ...
  !     [/SOURCE [name]|[NEXT n]]]  #1
  !     [/PLANET [name]] #2
  !     [/ALTERNATE]  #3
  !     [/AIRMASSES] #4
  !     [/PROJECT] #5
  !     [/FLUX Fmin [Fmax]] #6
  !     [/NIGHT_MARKS] #7
  !     [/ELEVATION] #8
  !	Plots observing periods for sources, planets, ...
  ! 	on a time chart.
  !---------------------------------------------------------------------
  character(len=*), intent(inout) :: line  !
  logical, intent(out) :: error            !
  ! Local
  character(len=*), parameter :: rname='HORIZON'
  integer(kind=4) :: i_source, n_source, lun,is
  integer(kind=4) ::  ier
  logical :: do_all_source, found, do_source,doflux,doplanet
  logical :: lunopened
  logical :: project, elev, bure
  real(kind=4) :: fmin, fmax ! for /FLUX option
  character(len=12) :: argum
  character(len=sourcename_length) :: name
  integer(kind=4) :: icat,last_source  
  real(kind=8) :: s_3(3)                ! apparent RA and DEC, DSUN
  ! To save the current Astro%source and set it back after the HORIZON command
  type(astro_source_mem) :: current_source
  ! For Planets
  integer(kind=4) :: i_planet, i, nc
  integer(kind=4) :: n_low,n_names
  logical :: selected(m_body)
  character(len=256) :: mess
  integer(kind=4), parameter :: optsource=1
  integer(kind=4), parameter :: optplanet=2
  integer(kind=4), parameter :: optalternate=3
  integer(kind=4), parameter :: optairmasses=4
  integer(kind=4), parameter :: optproject=5
  integer(kind=4), parameter :: optflux=6
  integer(kind=4), parameter :: optnightmarks=7
  integer(kind=4), parameter :: optelevation=8
  !
  ! Save current astro%source
  call save_astro_source(current_source,error)
  if (error) return
  ! Code ------------------------------------------------------------------
  !
  n_rise = 0
  !
  ! Options
  !
  lunopened=.false.
  airmass = sic_present(optairmasses,0)
  do_source = sic_present(optsource,0)
  doplanet =sic_present(optplanet,0)
  project = sic_present(optproject,0)
  elev = sic_present(optelevation,0)
  bure = (project.or.elev)
  night_mark = sic_present(optnightmarks,0)
  doflux=sic_present(optflux,0)
  !
  ! Consistency Checks
  !
  if (sic_present(optalternate,0)) then
    icat = 2
  else
    icat = 1
  endif
  if (.not.do_source .and. .not.sic_present(optplanet,0)) then
    call astro_message(seve%e,rname,'/SOURCE or /PLANET must be given')
    error = .true.
    return
  endif
  if ((elev).and.(airmass)) then
    call astro_message(seve%w,rname,'/ELEVATION incompatible with '//  &
    '/AIRMASS - ignored')
    elev = .false.
  endif
  if ((elev).and.(sic_present(optplanet,0))) then
    call astro_message(seve%w,rname,'/ELEVATION incompatible with '//  &
    '/PLANET - ignored')
    elev = .false.
  endif
  !
  ! Get Horizon values
  !
  n_low = 0
  if (elev) n_low = 1          ! Add one low-elevation point
  if (sic_narg(0).gt.0) then
    n_horiz = sic_narg(0)
    do i=1, n_horiz
      call sic_r8 (line,0,i,horiz(i+n_low),.true.,error)
      if (error) return
    enddo
    n_horiz = n_horiz + n_low
  elseif (airmass) then
    n_horiz = 3
    horiz(1) = 4
    horiz(2) = 3
    horiz(3) = 2
  else
    n_horiz = 4+n_low
    horiz(n_low+1) = 20
    horiz(n_low+2) = 40
    horiz(n_low+3) = 60
    horiz(n_low+4) = 80
  endif
  if (elev) horiz(1) = horiz(2)    ! Default
  !
  ! Get flux range
  !
  if (doflux) then
    call sic_r4 (line,optflux,1,fmin,.true.,error)
    if (error) return
    fmax = 1.0e38
    call sic_r4 (line,optflux,2,fmax,.false.,error)
    if (error) return
  else
    fmin = -100.               !enables some star magnitudes also
    fmax = 1.0e38
  endif
  !
  ! Sources
  !  
  if (do_source) then
    ! Check and open catalog
    if (len_trim(catalog_name(icat)).eq.0) then
      call astro_message(seve%e,rname,'No catalog')
      error = .true.
      return
    endif
    !
    ier = sic_getlun(lun)
    ier = sic_open(lun,catalog_name(icat),'OLD',.true.)
    if (ier.ne.0) then
      call astro_message(seve%e,rname,'Error opening catalog')
      error=.true.
      return
    endif
    lunopened=.true.
    !
    found  = .false.
    if (.not.sic_present(optsource,1)) then
      ! No arguments = all sources in catalog
      next_source(icat) = 1
      last_source=3*m_source
      call horizon_catalog(rname,lun,next_source(icat),last_source,fmin,fmax,elev,error)
      if (error) then
        call close_catalog(rname,lun,lunopened,'Error reading catalog',error)
        return
      endif
    else
      call sic_ke (line,optsource,1,argum,nc,.true.,error)
      if (error) return
      if (argum(1:4).eq.'NEXT') then
        ! NEXT: plot a number of sources
        call sic_i4 (line,optsource,2,n_source,.true.,error)
        if (error) then
          call close_catalog(rname,lun,lunopened,'Error reading number of sources',error)
          return
        endif
        n_source = max(n_source,1)
        last_source = next_source(icat)+n_source-1
        call horizon_catalog(rname,lun,next_source(icat),last_source,fmin,fmax,elev,error)
        if (error) then
          call close_catalog(rname,lun,lunopened,'Error reading catalog',error)
          return
        endif
        next_source(icat)=last_source+1
      else
        ! Source names are specified
        if (doflux) then
          call astro_message(seve%w,rname,'/FLUX option ignored when a source list is given')
        endif
        n_names=sic_narg(optsource)
        do is=1,n_names
          call sic_ch(line,optsource,is,name,nc,.true.,error)
          if (error) then
            call close_catalog(rname,lun,lunopened,'Error decoding source name',error)
            return
          endif
          call horizon_sourcename(rname,name,current_source,lun,elev,error)
          if (error) then
            call close_catalog(rname,lun,lunopened,'Error when looking for the source',error)
            return
          endif
        enddo
      endif
    endif
    if (lunopened) close(unit=lun)
    call sic_frelun(lun)
  endif
  ! Planets
  if (doplanet) then
    if (n_rise.ge.m_source) then
      call astro_message(seve%w,rname,'Already too many sources. Planets not drawn')
    else
      if (.not.sic_present(optplanet,1)) then
        do_all_source = .true.
      else
        selected(1:m_body) = .false.
        do_all_source = .false.
        do i_source = 1, sic_narg(optplanet)
          call sic_ke(line,optplanet,i_source,argum,nc,.true.,error)
          if (error) then
            call astro_message(seve%e,rname,'Error decoding planet name')
            error=.true.
            return
          endif
          call sic_ambigs(rname,argum,name,nc,body,m_body,error)
          if (error) then
            write (mess,'(a,1x,a,1x,a)') 'Planet name',argum,'not recognized'
            call astro_message(seve%e,rname,mess)
            error=.true.
            return
          endif
          selected(nc) = .true.
        enddo
      endif
      do i_planet = 1, m_body
        if (do_all_source .or. selected(i_planet)) then
          call eq_planet(i_planet,s_3,error)
          if (n_rise.ge.m_source) then
            call astro_message(seve%w,rname,'Too many sources')
            exit
          endif
          n_rise = n_rise+1
          call set_rise(body(i_planet), s_3, error)
          if (error) then
            call astro_message(seve%e,rname,'Problem computing Planet visibility')
            error=.true.
            return
          endif
          found = .true.
        endif
      enddo
    endif
  endif
  !
  ! Now do the plotting
  !
  if (night_mark) call sunrise(error)
  call plot_horizon(project,elev,error)
  ! Close files if any
  if (lunopened) close(unit=lun)
  call sic_frelun(lun)
  ! Restore astro%source
  call restore_astro_source(rname,current_source,error)
  if (error) return
  !
end subroutine astro_horizon
!
subroutine close_catalog(rname,lun,lunopened,mess,error)
  use gkernel_interfaces
  use gbl_message
  use astro_interfaces, except_this=>close_catalog
  !---------------------------------------------------------------------
  !  @ private
  !  close current opened catalog file
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  integer(kind=4), intent(in) :: lun
  logical, intent(inout) :: lunopened
  character(len=*), intent(in) :: mess
  logical, intent(inout) :: error
  !
  if (lunopened) close(unit=lun)
  call sic_frelun(lun)
  lunopened=.false.
  call astro_message(seve%e,rname,mess)
  return
  !
end subroutine close_catalog
!
subroutine restore_astro_source(rname,asou,error)
  use gkernel_interfaces
  use gbl_message
  use astro_interfaces, except_this=>restore_astro_source
  use ast_astro
  use ast_horizon
  use ast_constant
  !---------------------------------------------------------------------
  !  @ private
  !  save in a structure the parameters of the ASTRO%SOURCE before HORIZON
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(astro_source_mem), intent(inout) :: asou
  logical, intent(inout) :: error
  !
  real(kind=8) :: s_3(3)
  logical, parameter :: draw=.false.            !needed by object() but unused
  logical, parameter :: drawclip=.false.        !needed by object() but unused
  character(len=1) :: drawargs(1)               !needed by object() but unused
  character(len=256) ::varname,name
  real(kind=8) :: vlsr,vshift,velocity,redshift
  !
  call nullify_astro_source(error)
  if (error) return
  soukind=asou%soukind
  if (asou%soukind.eq.soukind_full) then
    ! ASTRO SOURCE FULLY DEFINED
    call object(asou%name,asou%coord,asou%equinox,asou%lambda,asou%beta, &
                asou%vtype,asou%velocity,asou%redshift, &
                draw,drawargs,drawclip,s_3,.false.,0,error)
    if (error) return
  else if (asou%soukind.ne.soukind_none) then
    !LSR or REDSHIFT mode
    name=soukinds(asou%soukind)
    if (asou%soukind.eq.soukind_vlsr) then
      velocity=asou%velocity
      vshift=asou%velocity
      vlsr=asou%velocity
      redshift=0d0
    else if (asou%soukind.eq.soukind_red) then
      name=soukinds(asou%soukind)
      velocity=0d0
      vshift=0d0
      vlsr=0d0
      redshift=asou%redshift
    endif
    ! Store output parameters in the SIC structure astro%source
    varname = 'ASTRO'
    if (.not.sic_varexist(varname)) then
      call sic_defstructure(varname,.true.,error)
      if (error) return
    endif
    varname = 'ASTRO%SOURCE'
    call fill_doppler_source(rname,varname,name,vshift,vlsr,redshift,asou%vtype,velocity,error)
    if (error) return
  endif
  !
end subroutine restore_astro_source
!
subroutine save_astro_source(asou,error)
  use gkernel_interfaces
  use gbl_message
  use astro_interfaces, except_this=>save_astro_source
  use ast_astro
  use ast_horizon
  use ast_constant
  !---------------------------------------------------------------------
  !  @ private
  !  save in a structure the parameters of the ASTRO%SOURCE before HORIZON
  !---------------------------------------------------------------------
  type(astro_source_mem), intent(inout) :: asou
  logical, intent(inout) :: error
  !
  real(kind=8) :: inbeta, inlambda
  !
  asou%soukind=soukind
  if (soukind.eq.soukind_full) then
    asou%name=astro_source_name
    asou%coord=source_incoord
    asou%equinox=source_ineq
    ! Convert coordinates to prepare for object
    call sic_sexa(source_inlambdasexa,len(source_inlambdasexa),inlambda,error)
    asou%lambda=inlambda*pi/12d0 !rad
    call sic_sexa(source_inbetasexa,len(source_inbetasexa),inbeta,error)
    asou%beta=inbeta*pi/180d0 !rad
    asou%vtype=source_invtype
    asou%velocity=source_invelocity
    asou%redshift=source_inredshift
  else if (soukind.eq.soukind_vlsr) then
    asou%velocity=source_invelocity
    asou%redshift=0d0
  else if (soukind.eq.soukind_red) then
    asou%velocity=0d0
    asou%redshift=source_inredshift
  endif
  !
end subroutine save_astro_source
!
subroutine do_horizon(rname,souname,cataline,coord,equinox,lambda,beta,vtyp,velo,redsh,elev,error)
  use gkernel_interfaces
  use gbl_message
  use astro_interfaces, except_this=>do_horizon
  use ast_astro
  use ast_horizon
  !---------------------------------------------------------------------
  !  @ private
  !  Do the computation required before Horizon plot
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  character(len=*), intent(in) :: souname
  character(len=*), intent(in) :: cataline
  character(len=*), intent(in) :: coord
  real(kind=4), intent(in) :: equinox
  real(kind=8), intent(in) :: lambda, beta
  character(len=*), intent(in) ::vtyp
  real(kind=8), intent(in) ::velo
  real(kind=8), intent(in) ::redsh

  logical, intent(in) :: elev ! minimal elevatin (bure case)
  logical, intent(inout) :: error
  ! Local
  integer(kind=4) :: low_el ! for bure projects
  real(kind=8) :: s_3(3)                ! apparent RA and DEC, DSUN
  logical, parameter :: draw=.false.            !needed by object() but unused
  logical, parameter :: drawclip=.false.        !needed by object() but unused
  character(len=1) :: drawargs(1)               !needed by object() but unused
  !
  ! Astrometry
  call object(souname,coord,equinox,lambda,beta,vtyp,velo,redsh, &
              draw,drawargs,drawclip,s_3,.false.,0,error)
  if (error) return
  ! drawargs and drawclip are not used since draw is FALSE. Object will not do any plot
  ! One more source
  if (n_rise.ge.m_source) then
    call astro_message(seve%w,rname,'Too many source')
    return
  endif
  n_rise = n_rise+1
  ! Bure projects
  ! Looks for special format in catalog to add information
  call set_project(cataline,low_el,error)
  if (error) return
  if (elev) then
    horiz(1) = horiz(2)
    if (low_el.lt.horiz(1)) then
      horiz(1) = low_el
    endif
    min_elev(n_rise) = horiz(1)  ! Save for plot
  endif
  !
  call set_rise(souname, s_3, error)
  if (error) return
  !
end subroutine do_horizon
!
subroutine horizon_catalog(rname,lun,s1,s2,fmin,fmax,elev,error)
  use gkernel_interfaces
  use gbl_message
  use astro_interfaces, except_this=>horizon_catalog
  use ast_astro
  use ast_horizon
  !---------------------------------------------------------------------
  !  @ private
  !  Loop on sources in catalog
  !  prepare horizon plot for a subset or all source
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  integer(kind=4), intent(in) :: lun ! Unit where catalog is opened
  integer(kind=4), intent(in) :: s1,s2  ! 1st and last source to plot
  real(kind=4), intent(in)  :: fmin,fmax ! flux range to be plotted
  logical, intent(in) :: elev ! minimal elevatin (bure case)
  logical, intent(inout) :: error
  ! local
  integer(kind=4) :: isou,ier,nlu,npar,lpar
  character(len=256) :: c_line,p_line,par
  ! For decode_line and object
  character(len=2) :: coord       ! coordinate system ('EQ','GA','HO')
  character(len=2) :: ftype,vtype       ! flux type  'MA', 'JY' and vtype (ea, re,...)
  real(kind=4) :: equinox, flux, spindex, mag(9)
  real(kind=8) :: lambda, beta ,velocity,redshift         ! coord (rad)
  ! For check_source
  logical :: found
  character(len=16) :: dummy
  !
  isou=0
  do
    read(lun,'(A)',iostat=ier) c_line
    if (ier.lt.0) exit ! end of file
    if (ier.ne.0) then 
      call astro_message(seve%e,rname,'Error reading catalog line')
      error=.true.
      return ! problem when reading
    endif
    if (c_line(1:1).eq.'!')  cycle  ! Comment line
    nlu = len_trim(c_line)
    if (nlu.eq.0)  cycle  ! Blank line
    !    
    p_line = c_line ! save original line to get special info in case of bure project
    call sic_blanc(c_line,nlu) ! get to standard format to be able to decode line    !
    ! Get source name
    lpar=12
    npar = 1
    call sic_next(c_line,par,lpar,npar)   
    dummy=''
    found=.true. ! always true in catalog context
    call check_source(dummy,len_trim(dummy),par,lpar,found) ! reduce source name
    ! Decode source definition line
    call decode_line (c_line, npar, coord, equinox, lambda, beta, vtype,  &
    velocity,redshift, ftype, flux, spindex, mag, error)
    if (error) return
    if (coord.ne.'HO' .and. (flux.ge.fmin.and.flux.le.fmax)) then
      isou=isou+1
      if (isou.lt.s1.or.isou.gt.s2) cycle ! plot only in the requested range
      if (n_rise.ge.m_source) then
        call astro_message(seve%w,rname,'Too many sources')
        exit ! reached the maximum number of sources
      endif
      call do_horizon(rname,par(1:lpar),p_line,coord,equinox,lambda,beta, &
                      vtype,velocity,redshift,elev,error)
      if (error) exit
    endif
  enddo
  !
end subroutine horizon_catalog
!
subroutine horizon_sourcename(rname,souname,asou,lun,elev,error)
  use gkernel_interfaces
  use gbl_message
  use astro_interfaces, except_this=>horizon_sourcename
  use ast_astro
  use ast_horizon
  use ast_constant
  !---------------------------------------------------------------------
  !  @ private
  !  Check if input source is in memory or in the catalog
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  character(len=*), intent(in) :: souname ! source name
  type(astro_source_mem), intent(in) :: asou
  integer(kind=4), intent(in) :: lun      ! Unit where catalog is opened
  logical, intent(in) :: elev             ! minimal elevatin (bure case)
  logical, intent(inout) :: error
  ! local
  integer(kind=4) :: ier,nlu,npar,lpar
  character(len=256) :: c_line,p_line,par,mess
  ! For decode_line and object
  character(len=2) :: coord       ! coordinate system ('EQ','GA','HO')
  character(len=2) :: ftype, vtype   ! flux type  'MA', 'JY' and vel type
  real(kind=4) :: equinox, flux, spindex, mag(9)
  real(kind=8) :: lambda,beta,velocity,redshift         ! coord (rad)
  ! For check_source
  logical :: found
  logical :: gotsource
  !
  gotsource=.false.
  ! Check if the required source is in memory
  if (asou%soukind.eq.soukind_full.and.souname.eq.asou%name) then
    write (mess,'(a,1x,a,1x,a)') 'Source',trim(souname),'is the current source in memory'
    gotsource=.true.
    p_line=souname
    call astro_message(seve%i,rname,mess)
    ! Convert coordinates to prepare for object
    call do_horizon(rname,souname,p_line,asou%coord,asou%equinox,asou%lambda,asou%beta, &
                    asou%vtype,asou%velocity,asou%redshift,elev,error)
    if (error) return
  else
    !Look for source in catalog
    do
      read(lun,'(A)',iostat=ier) c_line
      if (ier.lt.0) exit ! end of file
      if (ier.ne.0) exit ! problem when reading
      if (c_line(1:1).eq.'!')  cycle  ! Comment line
      nlu = len_trim(c_line)
      if (nlu.eq.0)  cycle  ! Blank line
      !    
      p_line = c_line ! save original line to get special info in case of bure project
      call sic_blanc(c_line,nlu) ! get to standard format to be able to decode line    !
      ! Get source name
      lpar=12
      npar = 1
      call sic_next(c_line,par,lpar,npar)   
      found=.false. ! always false in source name context
      ! check if source in catalog and reduce source name
      call check_source(souname,len_trim(souname),par,lpar,found) 
      if (.not.found) cycle 
      gotsource=.true.
      ! Decode source definition line
      call decode_line (c_line, npar, coord, equinox, lambda, beta, vtype,  &
      velocity,redshift, ftype, flux, spindex, mag, error)
      if (error) return
      if (coord.ne.'HO') then
        call do_horizon(rname,souname,p_line,coord,equinox,lambda,beta, & 
                        vtype,velocity,redshift,elev,error)
        if (error) return
      endif
      exit ! here we found the source name, no need to go on with the loop
    enddo
    rewind(lun)
  endif
  if (gotsource) return
  !
  write (mess,'(a,1x,a,1x,a)') 'Source',trim(souname),'not found'
  call astro_message(seve%e,rname,mess)
  error=.true.
  !
end subroutine horizon_sourcename
!
subroutine eq_planet(i_planet, s_3, error)
  use ast_astro
  use ast_constant
  use ast_planets
  !---------------------------------------------------------------------
  !  @ private
  !  Get Apparent RA and DEC of planet I_PLANET
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)  :: i_planet  !
  real(kind=8),    intent(out) :: s_3(3)    !
  logical,         intent(out) :: error     !
  ! Local
  real(kind=8) :: sun_distance
  integer(kind=4) :: i
  real(kind=8) :: x_0(3), x_2(3), v_0(3), v_2(3), result(6), trfm_32(9)
  real(kind=8) :: x_3(3), distance
  equivalence (result(1), x_0(1)), (result(4),v_0(1))
  !------------------------------------------------------------------------
  ! Code :
  ! Save Current RA & DEC in case
  ra_old=ra
  dec_old=dec
  ! Compute new values
  error = .false.
  call ephsta(vector(i_planet), i_planet, error)
  if (error) return
  !
  ! Get ecliptic J2000 coordinates
  call ephvec(jnow_tdt, 1, result, error)
  !
  ! Get Apparent Horizontal coordinates
  call matvec(x_0, trfm_20, x_2)
  call matvec(v_0, trfm_20, v_2)
  !
  ! Light-time correction
  distance = dsqrt(x_2(1)**2 + x_2(2)**2 + x_2(3)**2)
  do i = 1, 3
    x_2(i) = x_2(i) - distance / light*v_2(i)
  enddo
  s_3(3) = sun_distance(x_2)
  !
  ! Apparent equatorial coordinates (without parallax)
  call transp(trfm_23, trfm_32)
  call matvec(x_2, trfm_32, x_3)
  call spher(x_3, s_3)
  ra = s_3(1)
  dec = s_3(2)
end subroutine eq_planet
!
subroutine set_project(line,low_el,error)
  use gildas_def
  use gkernel_interfaces
  use ast_horizon
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  !  ASTRO
  ! Read specific PdBI information in source catalog
  !
  ! Where is the documentation of this ????
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: line    !
  integer(kind=4), intent(out) :: low_el  !
  logical, intent(out) :: error           !
  ! Local
  integer(kind=4) :: ia,ja,ka,la,ma,na
  character(len=5) :: c_low_elev
  !
  low_el = 100
  c_weig(n_rise) = ' '
  ia = index(line,'[')+1
  if (ia.eq.1) return          ! No PdBI additional information
  !
  ja = index(line(ia:),'/')+ia-1
  c_proj(n_rise) = line(ia:ja-1)
  !
  ka = index(line(ja:),'//')+ja-1
  c_freq(n_rise) = line(ja+1:ka-1)
  !
  if (line(ka+2:ka+6).eq.'FIRST') then
    c_weig(n_rise) = ' 4'
  else
    c_weig(n_rise) = ' 1'
  endif
  !
  la = index(line(ka:),'///')
  if (la.eq.0) return
  ma = index(line(la:),'////')
  if (ma.eq.0) return
  ma = ma+la-1
  na = index(line,']')
  !
  c_low_elev = line(ma+5:na-1)
  if (len_trim(c_low_elev).ne.0) then
    call sic_math_inte(c_low_elev,len_trim(c_low_elev),low_el,error)
    if (error) return
  endif
  !
end subroutine set_project
!
subroutine set_rise(name,s_3,error)
  use gbl_message
  use ast_horizon
  use ast_astro
  use ast_constant
  !---------------------------------------------------------------------
  ! @ private
  ! ASTRO
  !  Find the rising and setting times of object NAME
  !  Apparent coordinates are in s_3
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: name    !
  real(kind=8),     intent(in)    :: s_3(3)  !
  logical,          intent(inout) :: error   !
  ! Local
  real(kind=8) :: sdelsphi, cdelcphi, cosh, h, sh
  integer(kind=4) :: i
  !
  if (n_rise.gt.m_source) then
    call astro_message(seve%e,'HORIZON','Too many sources')
    error = .true.
    return
  endif
  !
  c_rise(n_rise) = trim(name)
  transit(n_rise) = mod(s_3(1)+4*twopi, twopi)
  declin(n_rise)=dec
  righta(n_rise)=ra
  dsun(n_rise) = s_3(3)
  sdelsphi = sin(s_3(2)) * sin(pi/180.d0*lonlat(2))
  cdelcphi = cos(s_3(2)) * cos(pi/180.d0*lonlat(2))
  do i=1, n_horiz
    if (airmass) then
      sh = 1./horiz(i)
    else
      sh = sin( horiz(i)*pi/180. )
    endif
    cosh = (sh - sdelsphi) / cdelcphi
    if (cosh .le. -1) then
      rise(n_rise,i) = - pi    ! always up
      set (n_rise,i) = 3 * pi
    elseif (cosh.ge.1) then    ! always down
      rise (n_rise,i) = 3 * pi
      set(n_rise,i) = - pi
    else
      h = acos(cosh)
      rise(n_rise,i) = mod(s_3(1)-h+4*twopi, twopi)
      set (n_rise,i) = mod(s_3(1)+h+4*twopi, twopi)
    endif
  enddo
end subroutine set_rise
!
subroutine sunrise(error)
  use astro_interfaces, except_this=>sunrise
  use ast_horizon
  use ast_astro
  use ast_constant
  !---------------------------------------------------------------------
  ! @ private
  !  Find the rising and setting times of SUN and the various twilight
  ! times.
  !   [1]:0 deg [2]:-6 deg (civil twilight) [3]:-12 deg (Nautical twilight)
  !   [4]:-18 astronomical twilight. Compute phase of MOON
  !  Updates corresponding values in the common.
  !---------------------------------------------------------------------
  logical, intent(inout) :: error   !
  ! Local
  real(kind=8) :: s_3(3), elong
  real(kind=8) :: sdelsphi, cdelcphi, cosh, h, sh
  integer(kind=4) :: i
  ! It is necessary to save the current source RA & DEC since SUNRISE use
  ! common values of st_horizon module. Define a copy of variables related to
  ! source position:
  real(kind=8) :: save_s_1(2),save_s_3(3)
  real(kind=8) :: save_azimuth, save_elevation
  real(kind=8) :: save_ra, save_dec
  real(kind=8) :: save_pa
  real(kind=8) :: save_azimuth_old, save_elevation_old
  ! Code ----------------------------------------------------------------
  ! update S_3
  do i=1,3
    save_s_3(i)=s_3(i)
  enddo
  do i=1,2
    save_s_1(i)=s_1(i)
  enddo
  save_azimuth=azimuth
  save_elevation=elevation
  save_ra=ra
  save_dec=dec
  save_pa=parallactic_angle
  save_azimuth_old=azimuth_old
  save_elevation_old=elevation_old
  !     Do the job
  call eq_planet(2,s_3,error)  ! 2 = SUN
  sdelsphi = sin(s_3(2)) * sin(pi/180.d0*lonlat(2))
  cdelcphi = cos(s_3(2)) * cos(pi/180.d0*lonlat(2))
  do i=1,4
    sh = sin(-(i-1)*6.0d0*pi/180.0d0)  ! 0 -6 -12 -18 deg (various twilights)
    cosh = (sh - sdelsphi) / cdelcphi
    if (cosh .le. -1) then
      sunriz(i) = - pi         ! always up
      sunset(i) = 3 * pi
    elseif (cosh.ge.1) then    ! always down
      sunriz(i) = 3 * pi
      sunset(i) = - pi
    else
      h = acos(cosh)
      sunriz(i) = mod(s_3(1)-h+4*twopi, twopi)
      sunset(i) = mod(s_3(1)+h+4*twopi, twopi)
    endif
  enddo
  call eq_planet(1,s_3,error)  ! 1 = MOON
  elong = pi/180.d0*s_3(3)     ! so it seems...
  ! stolen idea in xephem. not as accurate yet (no corrective terms).
  moonphse = (1+cos(pi-elong))/2*100
  moonmag  = -12.7+2.5*(log10(pi)-log10(pi/2*(1+1.e-6-cos(elong))))
  ! put back values
  do i=1,3
    s_3(i)=save_s_3(i)
  enddo
  do i=1,2
    s_1(i)=save_s_1(i)
  enddo
  azimuth=save_azimuth
  elevation=save_elevation
  ra=save_ra
  dec=save_dec
  parallactic_angle=save_pa
  azimuth_old=save_azimuth_old
  elevation_old=save_elevation_old
end subroutine sunrise
!
subroutine plot_horizon(project,elev,error)
  use gildas_def
  use gkernel_interfaces
  use ast_horizon
  use ast_astro
  use ast_constant
  !---------------------------------------------------------------------
  ! @ private
  ! 	ASTRO
  !---------------------------------------------------------------------
  logical, intent(in) :: project    !
  logical, intent(in) :: elev       !
  logical, intent(out) :: error     !
  ! Local
  integer(kind=4) :: i,j, ii
  character(len=80) :: command
  character(len=3) :: chain
  real(kind=8) :: yc, yu, yl, utc, x1, x2, xt, factor, chsize, t
  real(kind=4) :: character_size, tick_size, box_ymax, box_ymin, box_xmin
  real(kind=4) :: box_xmax
  real(kind=4) :: poly(4,2)               !internal POLY array
  integer(kind=index_length) :: ipol(4)  ! its trace (to be passed)
  integer(kind=4) :: dash(4)
  integer(kind=4) :: jj
  ! DATA
  data ipol/4,2,0,0/           !trace of POLY
  data dash/3,4,2,1/
  ! Code ----------------------------------------------------------------
  call gr_exec1('SET BOX /DEFAULT')
  ! get current parameters
  call sic_get_real('CHARACTER_SIZE',character_size,error)
  call sic_get_real('TICK_SIZE',tick_size,error)
  call sic_get_real('BOX_YMIN',box_ymin,error)
  call sic_get_real('BOX_YMAX',box_ymax,error)
  call sic_get_real('BOX_XMIN',box_xmin,error)
  call sic_get_real('BOX_XMAX',box_xmax,error)
  !
  call gr_exec('CLEAR DIRECTORY')
  if (project) then
    box_xmax = box_xmax-3
    box_ymax = box_ymax-3
    write(command,'(A,4(1X,F8.2))') 'SET BOX ', box_xmin,box_xmax,box_ymin,  &
    box_ymax
    call gr_exec1(command)
  endif
  ! background grey boxes if night_marks
  if (night_mark) then
    ! pass array
    call sic_def_real('MYPOLYTMP',poly,2,ipol,.true.,error)
    ! set user limits to something useful
    write (command,'(A,F10.8,1X,I3,A)') 'LIMITS 0 ',twopi, 0, ' 1 '
    call gr_exec1(command)
    call gr_exec1('SET COORD USER')
    do i=1,4
      ! Sun always up!
      if (sunriz(i).eq.-pi) then
        ! Sun always down!
      elseif (sunriz(i).eq.3*pi) then
        poly(1,1)=0.0
        poly(1,2)=0.0
        poly(2,1)=twopi
        poly(2,2)=0.0
        poly(3,1)=twopi
        poly(3,2)=1.0
        poly(4,1)=0.0
        poly(4,2)=1.0
        write (command,110) i+8
        call gr_exec2(command)
        ! Sun Rises before set: 2 lines, Night outside
      elseif (sunriz(i).le.sunset(i)) then
        poly(1,1)=0.0
        poly(1,2)=0.0
        poly(2,1)=sunriz(i)
        poly(2,2)=0.0
        poly(3,1)=sunriz(i)
        poly(3,2)=1.0
        poly(4,1)=0.0
        poly(4,2)=1.0
        write (command,110) i+8
        call gr_exec2(command)
        poly(1,1)=sunset(i)
        poly(1,2)=0.0
        poly(2,1)=sunset(i)
        poly(2,2)=1.0
        poly(3,1)=twopi
        poly(3,2)=1.0
        poly(4,1)=twopi
        poly(4,2)=0.0
        call gr_exec2(command)
        ! Sun Rises after set: 2 lines, Night inside
      elseif (sunriz(i).gt.sunset(i)) then
        poly(1,1)=sunriz(i)
        poly(1,2)=0.0
        poly(2,1)=sunriz(i)
        poly(2,2)=1.0
        poly(3,1)=sunset(i)
        poly(3,2)=1.0
        poly(4,1)=sunset(i)
        poly(4,2)=0.0
        write (command,110) i+8
        call gr_exec2(command)
      endif
    enddo
    call sic_delvariable ('MYPOLYTMP',.false.,error)
  endif
  error = .false.
  factor = (box_ymax - box_ymin) / (n_rise + 1)
  chsize = min(character_size*1.2d0, character_size*factor*0.8d0)
  !
  ! Plot the Frame : mark with date, latitude, longitude
  call jdate_to_datetime(jnow_utc,command,error)
  if (error)  return
  call gr_exec1('DRAW TEXT 0 0.9 "Day: '//command(1:11)//'" 9 /BOX 7')
  if (project) then
    call gr_exec1('DRAW TEXT -0.5 0.9 "Project" 7 /BOX 7')
    call gr_exec1('DRAW TEXT +0.5 0.9 "Frequency" 9 /BOX 9')
  endif
  call deg2sexa(lonlat(1),360,command(1:),3,left=.true.)
  call deg2sexa(lonlat(2),360,command(15:),3,left=.true.)
  call gr_exec1('DRAW TEXT 0 0.9 "Obs: '//trim(command)//'" 7 /BOX 9')
  ! low axis (LST)
  write (command,'(A,I0,A)') 'LIMITS 0 24 ', n_rise+1, ' 0 '
  call gr_exec1(command)
  call gr_exec1('TICKSPACE 1 3 1 1')
  call gr_exec1('AXIS XL /TICK IN /LABEL P')
  call gr_exec1('LABEL "L.S.T." /X')
  !
  ! upper axis (UTC)
  utc = mod(jnow_utc+0.5d0,1d0)*twopi
  x1 = (utc - lst) *12./pi
  x1 = mod (x1+48.d0, 24.d0)
  x2 = x1 + 24.
  write (command,'(A,2(1X,F6.3),A)') 'AXIS XU',x1, x2,' /TICK IN /LABEL N'
  call gr_exec1(command)
  call gr_exec1('DRAW TEXT 0 0.9 "U.T.C." /BOX 8')
  do i=0,21,3
    xt = mod(i+24.d0-x1,24.d0) * (box_xmax-box_xmin)/24.
    write (command,'(A,2(1X,F5.2),A,I2,A)') 'DRAW TEXT',xt,tick_size,' "',i,  &
    '" 8 /BOX 7'
    call gr_exec1(command)
  enddo
  ! side axes
  call gr_exec1('AXIS YL /TICK N')
  call gr_exec1('AXIS YR /TICK N')
  write (command,'(A,F10.5)') 'SET CHARACTER ',chsize
  call gr_exec1(command)
  ! now in radians
  write (command,'(A,F10.8,1X,I0,A)') 'LIMITS 0 ',twopi, n_rise+1,' 0 '
  call gr_exec1(command)
  call gr_exec1('SET COORD USER')
  ! draw sunrise and sunset rules if NIGHT_MARK
  if (night_mark) then
    do i=1,4
      write (command,'(A,I1)') 'PENCIL /DASH ',dash(i)
      call gr_exec1(command)
      ! Sun always up!
      if (sunriz(i).eq.-pi) then
        ! Sun always down!
      elseif (sunriz(i).eq.3*pi) then
        ! Sun Rises before set: 2 lines, Night outside
      elseif (sunriz(i).le.sunset(i)) then
        write (command,100) sunriz(i), 0.0
        call gr_exec1(command)
        write (command,101) sunriz(i), float(n_rise+1)
        call gr_exec1(command)
        write (command,100) sunset(i), 0.0
        call gr_exec1(command)
        write (command,101) sunset(i), float(n_rise+1)
        call gr_exec1(command)
        ! Sun Rises after set: 2 lines, Night inside
      elseif (sunriz(i).gt.sunset(i)) then
        write (command,100) sunriz(i), 0.0
        call gr_exec1(command)
        write (command,101) sunriz(i), float(n_rise+1)
        call gr_exec1(command)
        write (command,100) sunset(i), 0.0
        call gr_exec1(command)
        write (command,101) sunset(i), float(n_rise+1)
        call gr_exec1(command)
      endif
    enddo
    call gr_exec1('PENCIL /DASH 1')
    write(command,106) int(moonphse)
    call gr_exec1(command)
    write(command,107) moonmag
    call gr_exec1(command)
  endif
  ! Now for the objects
  do i=1, n_rise
    yc = i
    yu = i-0.1
    yl = i+0.1
    jj = 1
    !
    ! Transit marks
    ! central transit tick below (Y increases)
    if (rise(i,jj).lt.0.d0) then
      t = mod(transit(i)+5*pi, twopi)
      write(command,100) t,yc
      call gr_exec1(command)
      write(command,101) t, yl
      call gr_exec1(command)
    elseif (rise(i,jj).le.twopi) then
      write(command,100) transit(i),yc
      call gr_exec1(command)
      write(command,101) transit(i), yl
      call gr_exec1(command)
    endif
    if (rise(i,jj).lt.0.d0) then  ! always up. one line.
      write (command,100) 0., yc
      call gr_exec1(command)
      write (command,101) twopi,yc
      call gr_exec1(command)
      if (dsun(i).lt.slimit) then
        write (command,103) pi, yu, c_rise(i)(1:lenc(c_rise(i))),8
      else
        write (command,102) pi, yu, c_rise(i)(1:lenc(c_rise(i))),8
      endif
      call gr_exec1(command)
    elseif (rise(i,jj).gt.twopi) then  ! always down. only name (with single or double parenthesis).
      if (dsun(i).lt.slimit) then
        write (command,103) pi, yu, '('//c_rise(i)(1:lenc(c_rise(i)))//')', 8
      else
        write (command,102) pi, yu, '('//c_rise(i)(1:lenc(c_rise(i)))//')', 8
      endif
      call gr_exec1(command)
    elseif (rise(i,jj).lt.set(i,jj)) then  ! rise before set: 1 line
      write (command,100) rise(i,jj), yc
      call gr_exec1(command)
      write (command,101) set(i,jj),yc
      call gr_exec1(command)
      if (dsun(i).lt.slimit) then
        write (command,103) (rise(i,jj)+set(i,jj))/2, yu,  &
        c_rise(i)(1:lenc(c_rise(i))), 8
      else
        write (command,102) (rise(i,jj)+set(i,jj))/2, yu,  &
        c_rise(i)(1:lenc(c_rise(i))), 8
      endif
      call gr_exec1(command)
    elseif (rise(i,jj).gt.set(i,jj)) then  ! rise after set: two segments
      write (command,100) 0., yc
      call gr_exec1(command)
      write (command,101) set(i,jj),yc
      call gr_exec1(command)
      if (dsun(i).lt.slimit) then
        write (command,103) 0., yu, c_rise(i)(1:lenc(c_rise(i))), 9
      else
        write (command,102) 0., yu, c_rise(i)(1:lenc(c_rise(i))), 9
      endif
      call gr_exec1(command)
      write (command,100) rise(i,jj), yc
      call gr_exec1(command)
      write (command,101) twopi,yc
      call gr_exec1(command)
      if (dsun(i).lt.slimit) then
        write (command,103) twopi, yu, c_rise(i)(1:lenc(c_rise(i))), 7
      else
        write (command,102) twopi, yu, c_rise(i)(1:lenc(c_rise(i))), 7
      endif
      call gr_exec1(command)
    endif
    !
    ! Project
    if (project) then
      if (c_weig(i).ne.' ') then
        command = 'PENCIL /WEIGHT '//c_weig(i)
        call gr_exec1(command)
        write(command,104) -3.*pi/180.0,yu,c_proj(i)
        call gr_exec1(command)
        write(command,105) 363.*pi/180.0,yu,c_freq(i)
        call gr_exec1(command)
        call gr_exec1('PENCIL /WEIGHT 1')
      endif
    endif
    !
    ! Tick marks
    call gr_exec1('SET EXPAND 0.7')
    if (elev) horiz(1) = min_elev(i)
    do j = jj, n_horiz
      if (rise(i,j).ge.0.d0 .and. rise(i,j).le.twopi) then
        if (airmass) then
          write (chain,'(F3.1)') horiz(j)
        else
          ii = nint(horiz(j))
          if (ii.ge.0) then
            write (chain,'(I2)') ii
          else
            write (chain,'(I3)') ii
          endif
        endif
        write (command,100) rise(i,j), yc
        call gr_exec1(command)
        write (command,101) rise(i,j), yu
        call gr_exec1(command)
        write (command,102) rise(i,j), yl, chain, 2
        call gr_exec1(command)
        write (command,100) set(i,j), yc
        call gr_exec1(command)
        write (command,101) set(i,j), yu
        call gr_exec1(command)
        write (command,102) set(i,j), yl, chain, 2
        call gr_exec1(command)
      endif
    enddo
    call gr_exec1('SET EXPAND 1.0')
  enddo
  !
  ! Restore defaults
  write (command,'(A,F10.5)') 'SET CHARACTER ',character_size
  call gr_exec1(command)
  call gr_exec1('LIMITS -180. 180. 0. 90.')
  call gr_exec1('TICKSPACE 10. 30. 5. 15.')
  !
100 format('DRAW RELOCATE ',f8.3,1x,f8.3)
101 format('DRAW LINE ',f8.3,1x,f8.3)
102 format('DRAW TEXT ',f8.3,1x,f8.3,' "',a,'" ',i1)
103 format('DRAW TEXT ',f8.3,1x,f8.3,' "(\\i',a,'\\i)" ',i1)
104 format('DRAW TEXT ',f8.3,1x,f8.3,' "',a,'" 4 /USER')
105 format('DRAW TEXT ',f8.3,1x,f8.3,' "',a,'" 6 /USER')
106 format('DRAW TEXT -0.05 -0.6 "\\gF\d:\\r=',i2.2,'%" 4 /BOX 7')
107 format('DRAW TEXT -0.05 -1.6 "m\d\g:\\r=',f5.1,'" 4 /BOX 7')
110 format('POLYGON MYPOLYTMP /VAR /FILL',1x,i2.2)
end subroutine plot_horizon
