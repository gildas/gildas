module astro_interfaces_public
  interface
    subroutine ephem_pack_set(pack)
      use gpack_def
      !----------------------------------------------------------------------
      ! @ public-mandatory
      !----------------------------------------------------------------------
      type(gpack_info_t), intent(out) :: pack  !
    end subroutine ephem_pack_set
  end interface
  !
  interface
    subroutine astro_pack_set(pack)
      use gpack_def
      !----------------------------------------------------------------------
      ! @ public
      !----------------------------------------------------------------------
      type(gpack_info_t), intent(out) :: pack  !
    end subroutine astro_pack_set
  end interface
  !
  interface
    subroutine do_astro_planet(rname,ip,surf,lola,eq,eqtopo,ho,sunel,vr,distance,  &
      helio_dist,pl_axes,tbright,fr,flux, visible,lola_e,eqoff, vroff,pbeam,tmb,  &
      fbeam,size,error)
      use ast_astro
      use ast_constant
      use ast_planets
      !---------------------------------------------------------------------
      ! @ public
      !	ASTRO Command :
      !	PLANET name|ALL /DRAW
      !---------------------------------------------------------------------
      character(len=*) :: rname
      integer(kind=4) :: ip             !
      logical :: surf                   !
      real(kind=8) :: lola(2)           !
      real(kind=8) :: eq(2)             !
      real(kind=8) :: eqtopo(2)         !
      real(kind=8) :: ho(2)             !
      real(kind=8) :: sunel             !
      real(kind=8) :: vr                !
      real(kind=8) :: distance          !
      real(kind=8) :: helio_dist        !
      real(kind=8) :: pl_axes(3)        !
      real(kind=8) :: tbright           !
      real(kind=8) :: fr                !
      real(kind=8) :: flux              !
      logical :: visible                !
      real(kind=8) :: lola_e(2)         !
      real(kind=8) :: eqoff(2)          !
      real(kind=8) :: vroff             !
      real(kind=8) :: pbeam             !
      real(kind=8) :: tmb               !
      real(kind=8) :: fbeam             !
      real(kind=8) :: size              !
      logical :: error                  !
    end subroutine do_astro_planet
  end interface
  !
  interface
    subroutine astro_time(line,error)
      use ast_astro
      use ast_constant
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !	Command TIME [date_time]
      !	Perform time dependent calculations at "date_time"
      !	default : current system time
      !  Compute Earth rotation parameters and related quantities giving
      !  - orientation of Earth rotation axis in space (i.e. precession and nutation)
      !  - position angle around this axis (i.e. sidereal time)
      !	Earth rotation velocity (J2000.0) = 7.2921151467e-5 rd/s
      !	see Aoki et al. (1982) or Astron. Almanach Suppl. (1984)
      !	if considered constant : error 1.e-8 (i.e. 0.01" after 24 hours)
      !  - orientation of the CIO (Conventional International Origin) with respect
      !	to the Celestial Ephemeris frame (i.e. pole motion)
      !---------------------------------------------------------------------
      character(len=*):: line           !
      logical :: error                  !
    end subroutine astro_time
  end interface
  !
  interface
    subroutine do_astro_time(jutc,jut1,jtdt,error)
      use gbl_message
      use ast_horizon
      use ast_astro
      use ast_constant
      !---------------------------------------------------------------------
      ! @ public
      !  Compute Earth rotation parameters and related quantities giving
      !  - orientation of Earth rotation axis in space (i.e. precession and nutation)
      !  - position angle around this axis (i.e. sidereal time)
      !	Earth rotation velocity (J2000.0) = 7.2921151467e-5 rd/s
      !	see Aoki et al. (1982) or Astron. Almanach Suppl. (1984)
      !	if considered constant : error 1.e-8 (i.e. 0.01" after 24 hours)
      !  - orientation of the CIO (Conventional International Origin) with respect
      !	to the Celestial Ephemeris frame (i.e. pole motion)
      !---------------------------------------------------------------------
      real(kind=8), intent(in) :: jutc              !
      real(kind=8), intent(in) :: jut1              !
      real(kind=8), intent(in) :: jtdt              !
      logical, intent(inout) :: error                  !
    end subroutine do_astro_time
  end interface
  !
  interface
    subroutine cdaten(cdate,ndate,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !  Convert the date '01-JAN-1984' in integers for year, month, and day
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: cdate     ! String date
      integer(kind=4),  intent(out)   :: ndate(3)  ! Integer values
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine cdaten
  end interface
  !
  interface
    subroutine ctimen(ctime,ntime,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Converts the TIME '12:34:56.1234' in integers for Hours, Minutes,
      ! Seconds, and milliSeconds
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: ctime     !
      integer(kind=4),  intent(out)   :: ntime(4)  !
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine ctimen
  end interface
  !
  interface
    subroutine utc(date)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      !  Lecture de l'heure du systeme par appel des routines systeme
      ! SIC_DATE en sortie : DATE date gregorienne (year, month, day, hour,
      ! minute, second)
      !---------------------------------------------------------------------
      integer(kind=4), intent(out) :: date(7)  !
    end subroutine utc
  end interface
  !
  interface
    subroutine ephini(error)
      use gildas_def
      use gbl_format
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Open direct access file of VSOP87 ephemeris.
      ! R. Lucas 29-apr-1994
      !---------------------------------------------------------------------
      logical, intent(out) :: error     !
    end subroutine ephini
  end interface
  !
  interface
    subroutine ephclose(error)
      !---------------------------------------------------------------------
      ! @ public
      ! Close the ephemeris file and free its logical unit
      !---------------------------------------------------------------------
      logical, intent(inout) :: error   !
    end subroutine ephclose
  end interface
  !
  interface
    subroutine jjdate (tjj,date)
      !---------------------------------------------------------------------
      ! @ public
      !  Conversion d'une date julienne en date du calendrier gregorien
      !  from EPHAUT / BDL / ? / 82-1
      !  modified 29 november 1984 by Michel Perault
      !  en entree : TJJ date julienne (jours juliens)
      !  en sortie : DATE date gregorienne (year, month, day, hour, minute, second)
      !  remarque  : le calcul couvre la periode julienne de l'an
      !              4713 avant notre ere a l'an 3267 de notre ere.
      !              les annees sont comptees en millesimes astronomiques
      !              l'annee qui precede l'an 1 de notre ere est l'an 0
      !              ex : l'an 80 avant notre ere a le millesime -79.
      !  rappel    : la periode du 5 octobre 1582 0h au 14 octobre 1582
      !              24 h n'existe pas
      !---------------------------------------------------------------------
      real(kind=8) :: tjj               !
      integer(kind=4) :: date(7)        !
    end subroutine jjdate
  end interface
  !
  interface
    subroutine datejj (date,tjj)
      !---------------------------------------------------------------------
      ! @ public
      !  conversion d'une date du calendrier gregorien en date julienne
      !  from EPHAUT / BDL / ? / 82-1
      !  modified 29 november 1984 by Michel Perault
      !  en entree : DATE date gregorienne (year, month, day, hour, minute, second
      !		, millisecond)
      !  en sortie : TJJ date julienne (jours juliens)
      !  remarque  : le calcul couvre la periode julienne de l'an
      !              4713 avant notre ere a l'an 3267 de notre ere.
      !              les annees sont comptees en millesimes astronomiques
      !              l'annee qui precede l'an 1 de notre ere est l'an 0
      !              ex : l'an 80 avant notre ere a le millesime -79.
      !  rappel    : la periode du 5 octobre 1582 0h au 14 octobre 1582
      !              24 h n'existe pas
      !---------------------------------------------------------------------
      integer(kind=4) :: date(7)        !
      real(kind=8) :: tjj               !
    end subroutine datejj
  end interface
  !
  interface
    subroutine astro_noemasetup_plot(rname,isetup,iplotmode,iupperaxis,showlines,error)
      use gbl_message
      use astro_noema_type
      !-----------------------------------------------------------------------
      ! @ public
      ! Plot a NOEMA receiver\backend setup as done in ASTRO
      !
      ! input:
      ! isetup: import structure filled in by 2 public subroutines:
      !       astro_noemasetup_receiver to transfer tuning/source information (1 call)
      !       astro_noemasetup_spw to transfer polyfix configuration (1 call / SPW)
      !
      ! Plot can be configured with:
      ! plotmode (integer*4):  pm+* defined in astro_types module
      !    pm_basebands=1= BASEBANDS = 4 boxes (H/V)LO, (H/V)LI, (H/V)UI, (H/V)UO
      !                               chunk limits are drawn
      !    pm_receiver=2= RECEIVER = 3 boxes (H/V)-USB, (H/V)-LSB, Global receiver band
      !                               chunk limits are not drawn
      ! iupperaxis (integer*4): choice for upper axis (lower axis is always REST)
      !     freqax_* defined in astro_types
      !     freqax_rest,freqax_rf, _lsr, _if1, _if2,_chunks,_imrest, _imrf, _imrf,_imlsr, _null
      !
      ! showline (logical): if .true. the catalog lines are indicated on the plot
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(astro_noemasetup_t), intent(in) :: isetup    ! import structure
      integer(kind=4), intent(in) :: iplotmode     ! BASEBANDS or RECEIVER
      integer(kind=4), intent(in) :: iupperaxis
      logical, intent(in) :: showlines
      logical, intent(inout) :: error
    end subroutine astro_noemasetup_plot
  end interface
  !
  interface
    subroutine astro_noemasetup_list(rname,isetup,doindex,error)
      use gbl_message
      use astro_noema_type
      !-----------------------------------------------------------------------
      ! @ public
      ! Plot/List a NOEMA receiver\backend setup as done in ASTRO
      !
      ! input:
      ! isetup: import structure filled in by 2 public subroutines:
      !       astro_noemasetup_receiver to transfer tuning/source information (1 call)
      !       astro_noemasetup_spw to transfer polyfix configuration (1 call / SPW)
      ! doindex .true. to list SPW according to their number in the data instead of frequency
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(astro_noemasetup_t), intent(in) :: isetup    ! import structure
      logical, intent(in) :: doindex
      logical, intent(inout) :: error
    end subroutine astro_noemasetup_list
  end interface
  !
  interface
    subroutine astro_noemasetup_receiver(rname,irec,flo1,fcent,sb,tuningname,sourcename,doppler,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ public
      ! API to fill in receiver section of the ASTRO import structure
      ! irec = isetup%rec ! irec = astro_noemareceiver_t
      !        isetup = astro_noemasetup
      ! flo1 (real*8) = LO1 tuning frequency (REST frame) in MHz
      ! fcent (real*8) = IF1 center frequency in MHz
      ! sb (integer*4) = sideband code where the tuning frequency is placed (1=USB,2=LSB)
      ! tuning name (character*12) = name of the LINE command at the observatory
      ! source name (character*128) = name of the source
      ! doppler (real*8) = doppler of the observation
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(astro_noemareceiver_t), intent(inout) :: irec
      real(kind=8), intent(in)  :: flo1   ! MHz, rest
      real(kind=8), intent(in)  :: fcent    !MHz, if1
      integer(kind=4), intent(in) :: sb ! sideband
      character(len=*), intent(in) :: tuningname ! tuning name
      character(len=*), intent(in) :: sourcename ! source name
      real(kind=8), intent(in)  :: doppler    ! as defined in obs/clic
      logical, intent(inout) :: error
    end subroutine astro_noemasetup_receiver
  end interface
  !
  interface astro_noemasetup_spw
    subroutine astro_noemasetup_spw_bychunk(rname,ispw,ch1,ch2,resol,label,user_label,corrmode,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ public-generic astro_noemasetup_spw
      ! API to fill in SPW section of the ASTRO import structure 
      ! 1 call per SPW: the list of SPW is incremented internally
      !
      ! Definition by chunk:
      ! For each spectral window input is:
      ! ch1 (integer*4) = 1st chunk of the SPW
      ! ch2 (integer*4) = last chunk of the SPW
      ! real (real*8) = resolution of the SPW (in MHz)
      ! label (character*3) = label to identify the baseband where the SPW is placed. Format: HLO
      ! user_label (character*32) = in case the used attached a name to the SPW
      ! corrmode (integer*4) = correlator mode used for the baseband
      !
      ! NB: When a HighResolution SPW is defined, the Low SPW of the current baseband is
      ! also defined if it does not exist yet
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(astro_noemaspw_t), intent(inout) :: ispw
      integer(kind=4), intent(in) :: ch1
      integer(kind=4), intent(in) :: ch2
      real(kind=8), intent(in)  :: resol
      character(len=*), intent(in) :: label
      character(len=*), intent(in) :: user_label
      integer(kind=4), intent(in) :: corrmode
      logical, intent(inout) :: error
    end subroutine astro_noemasetup_spw_bychunk
    subroutine astro_noemasetup_spw_byfreq(rname,ispw,f1,f2,resol,label,user_label,corrmode,error)
      use gbl_message
      use astro_types
      !-----------------------------------------------------------------------
      ! @ public-generic astro_noemasetup_spw
      ! API to fill in SPW section of the ASTRO import structure 
      ! 1 call per SPW: the list of SPW is incremented internally
      !
      ! Definition by frequency:
      ! For each spectral window input is:
      ! f1 (real*8) = IF2 min of the SPW
      ! f2 (real*8) = IF2 max of the SPW
      ! real (real*8) = resolution of the SPW (in MHz)
      ! label (character*3) = label to identify the baseband where the SPW is placed. Format: HLO
      ! user_label (character*32) = in case the used attached a name to the SPW
      ! corrmode (integer*4) = correlator mode used for the baseband
      !
      ! NB: When a HighResolution SPW is defined, the Low SPW of the current baseband is
      ! also defined if it does not exist yet
      !-----------------------------------------------------------------------
      character(len=*), intent(in) :: rname
      type(astro_noemaspw_t), intent(inout) :: ispw
      real(kind=8), intent(in) :: f1
      real(kind=8), intent(in) :: f2
      real(kind=8), intent(in)  :: resol
      character(len=*), intent(in) :: label
      character(len=*), intent(in) :: user_label
      integer(kind=4), intent(in) :: corrmode
      logical, intent(inout) :: error
    end subroutine astro_noemasetup_spw_byfreq
  end interface astro_noemasetup_spw
  !
  interface
    subroutine do_object(coord,equinox,lambda,beta,s_2,s_3,dop,lsr,svec,x_0,  &
      parang,error)
      use gbl_message
      use ast_astro
      use ast_constant
      !---------------------------------------------------------------------
      ! @ public
      !  Reduce coordinates of a fixed object
      !---------------------------------------------------------------------
      character(len=2), intent(in)    :: coord    ! Coordinate system
      real(kind=4),     intent(in)    :: equinox  ! Equinox (ignored if not EQ)
      real(kind=8),     intent(in)    :: lambda   ! Longitude like coordinate
      real(kind=8),     intent(in)    :: beta     !
      real(kind=8),     intent(out)   :: s_2(2)   !
      real(kind=8),     intent(out)   :: s_3(3)   ! ra, dec & distance of Sun at current epoch
      real(kind=8),     intent(out)   :: dop      !
      real(kind=8),     intent(out)   :: lsr      !
      real(kind=8),     intent(out)   :: svec(3)  !
      real(kind=8),     intent(out)   :: x_0(3)   !
      real(kind=8),     intent(out)   :: parang   ! Parallactic angle
      logical,          intent(inout) :: error    !
    end subroutine do_object
  end interface
  !
  interface
    subroutine do_tele_beam(primbeam,beam,freq,arg)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Default beam size according to Telescope
      ! Specific values for PICO and EFFELSBERG, otherwise just scale the
      ! BURE value.
      !---------------------------------------------------------------------
      real(kind=8),     intent(out) :: primbeam  ! Computed value
      real(kind=8),     intent(in)  :: beam      ! If not 0, return this value
      real(kind=8),     intent(in)  :: freq      ! Else, use freq+telescope
      character(len=*), intent(in)  :: arg       ! Telescope name
    end subroutine do_tele_beam
  end interface
  !
  interface astro_observatory
    subroutine astro_observatory_byname(arg,error)
      use gbl_message
      use ast_astro
      !---------------------------------------------------------------------
      ! @ public-generic astro_observatory
      !  Set the Astro observatory given its name
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: arg    ! Telescope name
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine astro_observatory_byname
    subroutine astro_observatory_bychcoords(clon,clat,alt,slim,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public-generic astro_observatory
      !  Set Astro observatory given its longitude/latitude (sexagesimal
      ! strings), altitude and avoidance
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: clon   ! Telescope longitude (sexagesimal string)
      character(len=*), intent(in)    :: clat   ! Telescope latitude (sexagesimal string)
      real(kind=8),     intent(in)    :: alt    ! Altitude [km]
      real(kind=8),     intent(in)    :: slim   ! Sun avoidance
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine astro_observatory_bychcoords
    subroutine astro_observatory_byr8coords(lon,lat,alt,slim,error)
      use gbl_message
      use ast_astro
      !---------------------------------------------------------------------
      ! @ public-generic astro_observatory
      !  Set Astro observatory given its longitude/latitude (float values),
      ! altitude and avoidance
      !---------------------------------------------------------------------
      real(kind=8), intent(in)    :: lon    ! Telescope longitude [deg]
      real(kind=8), intent(in)    :: lat    ! Telescope latitude [deg]
      real(kind=8), intent(in)    :: alt    ! Altitude [km]
      real(kind=8), intent(in)    :: slim   ! Sun avoidance
      logical,      intent(inout) :: error  ! Logical error flag
    end subroutine astro_observatory_byr8coords
  end interface astro_observatory
  !
  interface
    subroutine pdbi_plot_def(error)
      use gildas_def
      use gbl_message
      use ast_line
      !---------------------------------------------------------------------
      ! @ public
      ! Defaults paramaters for the plot
      !---------------------------------------------------------------------
      logical :: error                  !
    end subroutine pdbi_plot_def
  end interface
  !
  interface
    subroutine pdbi_plot_sync(c_flo1,c_plot_mode,c_sky, c_narrow_def,           &
      c_narrow_input,c_unit_def,c_unit_band, c_unit_cent,c_unit_wind,c_fshift,  &
      c_label)
      use ast_line
      use ast_astro
      !---------------------------------------------------------------------
      ! @ public
      ! This subroutine can be called to synchronize astro-global variables
      ! with ones local to a module. This prevents sharing the astro-global
      ! variables with other modules.
      !---------------------------------------------------------------------
      real(kind=8) :: c_flo1                 !
      integer(kind=4) :: c_plot_mode         !
      integer(kind=4) :: c_sky               !
      logical :: c_narrow_def                !
      integer(kind=4) :: c_narrow_input(2)   !
      logical :: c_unit_def(nunit)           !
      integer(kind=4) :: c_unit_band(nunit)  !
      real(kind=4) :: c_unit_cent(nunit)     !
      integer(kind=4) :: c_unit_wind(nunit)  !
      real(kind=8) :: c_fshift               !
      character(len=132) :: c_label          !
    end subroutine pdbi_plot_sync
  end interface
  !
  interface
    subroutine pdbi_plot_line(error)
      use gbl_message
      use ast_line
      !---------------------------------------------------------------------
      ! @ public
      ! Main entry for plotting
      !---------------------------------------------------------------------
      logical :: error                  !
    end subroutine pdbi_plot_line
  end interface
  !
end module astro_interfaces_public
