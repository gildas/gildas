subroutine astro_uv_doppler(line,error)
  use image_def
  use gkernel_interfaces
  use astro_interfaces, except_this=>astro_uv_doppler
  use gbl_format
  use gbl_message
  !-----------------------------------------------------------------------
  ! @ private
  !   Support for command UV_DOPPLER
  ! Task to compute the Doppler correction and apply it (by changing
  ! the u,v coordinates) to an UV Table.
  !-----------------------------------------------------------------------
  character(len=*), intent(in)    :: line   !
  logical,          intent(inout) :: error  !
  ! Local
  character(len=filename_length) :: file
  character(len=12) :: telescope
  character(len=*), parameter :: rname='UV_DOPPLER'
  type (gildas) :: uvin, uvou
  integer(kind=4) :: nf, i, ier, ib
  real(kind=4) :: rdate, rtime, doppler
  real(kind=8), parameter :: clight_kms=299792.458d0
  character(len=2) :: coord
  character(len=80) :: mess
  real(kind=4) :: equinox
  real(kind=8) :: lambda, beta
  integer(kind=4) :: nblock, nvisi, idate, itime
  logical :: output
  integer(kind=4) :: mode
  integer(kind=4) :: idopp
  !
  call sic_ke(line,0,1,telescope,nf,.true.,error)
  if (error)  return
  call astro_observatory(telescope,error)
  if (error) return
  !
  call sic_ch(line,0,2,file,nf,.true.,error)
  if (error)  return
  call gildas_null (uvin, type = 'UVT')     ! Define a UVTable gildas header
  call gdf_read_gildas (uvin, file, '.uvt', error, data=.false.)
  if (error) then
    call astro_message(seve%e,rname,'Cannot read input UV table')
    return
  endif
  !
  ! Define blocking factor, on largest data file, usually the input one
  ! but not always...
  call gdf_nitems('SPACE_GILDAS',nblock,uvin%gil%dim(1)) ! Visibilities at once
  nblock = min(nblock,uvin%gil%dim(2))
  ! Allocate respective space for each file
  allocate (uvin%r2d(uvin%gil%dim(1),nblock),stat=ier)
  if (failed_allocate(rname,'UV input data',ier,error))  return
  !
  ! Here modify the header of the output UV table according
  ! to the desired goal
  !
  output = sic_present(0,3)
  call gildas_null (uvou, type = 'UVT')     ! Define a UVTable gildas header
  call gdf_copy_header(uvin,uvou,error)
  !
  mode = 0
  call sic_i4(line,0,4,mode,.false.,error)
  if (error)  return
  if (mode.ne.0) then
    idopp = 3
    do i = 1,code_uvt_last
      if (uvou%gil%column_pointer(i).eq.idopp)  then
        uvou%gil%column_size(i) = 0
        uvou%gil%column_pointer(i) = 0
      endif
    enddo
    uvou%gil%column_pointer(code_uvt_topo) = idopp
    uvou%gil%column_size(code_uvt_topo) = 1
  endif
  !
  if (output) then
    call sic_ch(line,0,3,file,nf,.true.,error)
    call sic_parse_file(file,' ','.uvt',uvou%file)
    !
    ! create the image
    uvou%gil%dopp = 0.d0
    uvou%gil%version_uv = code_version_uvt_syst
    call gdf_create_image(uvou,error)
    if (error) then
      call gdf_close_image(uvin,error)
      deallocate(uvin%r2d,stat=ier)
      error = .true.
      return
    endif
  endif
  !
  coord = uvin%char%syst
  if (coord.eq.'EQ') then
    equinox = uvin%gil%epoc
    lambda = uvin%gil%ra
    beta   = uvin%gil%dec
  else if (coord.eq.'GA') then
    equinox = uvin%gil%epoc
    lambda = uvin%gil%lii
    beta   = uvin%gil%bii
  else
    coord = 'EQ'
    call astro_message(seve%w,rname,'Unsupported system '//coord)
    call astro_message(seve%w,rname,'Assuming EQUATORIAL')
    equinox = 2000.0
    lambda = uvin%gil%ra
    beta   = uvin%gil%dec
  endif
  !
  ! Loop over line table
  uvou%blc = 0
  uvou%trc = 0
  uvin%blc = 0
  uvin%trc = 0
  rdate = -1e38
  rtime = -1e38
  idate = uvin%gil%column_pointer(code_uvt_date)
  itime = uvin%gil%column_pointer(code_uvt_time)
  !
  do ib = 1,uvou%gil%dim(2),nblock
    write(mess,*) ib,' / ',uvou%gil%dim(2),nblock
    call astro_message(seve%d,rname,mess)
    uvin%blc(2) = ib
    uvin%trc(2) = min(uvin%gil%dim(2),ib-1+nblock)
    uvou%blc(2) = ib
    uvou%trc(2) = uvin%trc(2)
    call gdf_read_data(uvin,uvin%r2d,error)
    nvisi = uvin%trc(2)-uvin%blc(2)+1
    !
    ! Here do the job
    do i=1,nvisi
      !
      if (uvin%r2d(idate,i).ne.rdate .or. uvin%r2d(itime,i).ne.rtime) then
        rdate = uvin%r2d(idate,i)
        rtime = uvin%r2d(itime,i)
        call sub_uv_doppler(uvin%gil%vtyp,rdate,rtime,doppler,&
          coord,equinox,lambda,beta,error)
        !! factor = (1.d0 + doppler)
        if (.not.output) then
          Print *,' Visi ',i+nblock*(ib-1),rdate,rtime,clight_kms*doppler
        endif
      endif
      if (idopp.ne.0) uvin%r2d(idopp,i) = doppler
    enddo
    !
    if (output) then
      call gdf_write_data (uvou,uvin%r2d,error)
      if (error) then
        call gdf_close_image(uvou,error)
        call gdf_close_image(uvin,error)
        deallocate(uvin%r2d,stat=ier)
        error = .true.
        return
      endif
    endif
  enddo
  !
  call gdf_close_image(uvin,error)
  if (output) call gdf_close_image(uvou,error)
  deallocate(uvin%r2d,stat=ier)
end subroutine astro_uv_doppler
!
subroutine sub_uv_doppler(vtype,rdate,rtime,doppler, &
        & coord,equinox,lambda,beta,error)
  use image_def
  use astro_interfaces, except_this => sub_uv_doppler
  use gbl_message
  use gbl_constant
  use phys_const
  !---------------------------------------------------------------------
  ! @ private
  !
  ! Compute the Doppler correction, calling ASTRO routine, according to:
  ! * observatory location
  ! * reference frame (LSR, Helio...)
  ! Radio convention is used
  !---------------------------------------------------------------------
  integer(kind=4),  intent(in)  :: vtype    ! Type of referential
  real(kind=4),     intent(in)  :: rdate    ! GAG Date
  real(kind=4),     intent(in)  :: rtime    ! UT Time
  real(kind=4),     intent(out) :: doppler  ! Doppler factor
  character(len=2), intent(in)  :: coord    !
  real(kind=4),     intent(in)  :: equinox  !
  real(kind=8),     intent(in)  :: lambda   !
  real(kind=8),     intent(in)  :: beta     !
  logical,          intent(out) :: error    ! flag
  ! Local
  real(kind=8) :: vshift
  real(kind=8) :: s_2(2), s_3(3), dop, lsr, svec(3), x_0(3), parang
  real(kind=8) :: jtdt, jut1, jutc
  !
  doppler = -1d0
  !
  ! Reset the time
  jutc = rdate + rtime/86400.d0 +  2460549.5d0
  !
  ! For what we want to do, these are negligible
  jut1 = 0.d0
  Jtdt = 0.
  call do_astro_time(jutc,jut1,jtdt,error)
  if (error) return
  !
  ! Call ASTRO
  call do_object(coord,equinox,lambda,beta,s_2,s_3,dop,lsr,svec, &
      &   x_0,parang,error)
  if (error) return
  !
  ! Now retrieve the Doppler value
  !
  select case (vtype)
  case (vel_lsr)
     vshift = dop+lsr
  case (vel_hel)
     vshift = dop
  case (vel_ear)
     vshift = 0.0d0
  case default
     vshift = 0.0d0
  end select
  !
  ! This follows the CLASS convention, but it seems that the
  ! CLIC convention is WRONG ...
  doppler = - vshift / clight * 1d3
end subroutine sub_uv_doppler
