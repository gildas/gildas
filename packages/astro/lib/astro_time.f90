subroutine astro_time(line,error)
  use ast_astro
  use ast_constant
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>astro_time
  !---------------------------------------------------------------------
  ! @ public
  !	Command TIME [date_time]
  !	Perform time dependent calculations at "date_time"
  !	default : current system time
  !  Compute Earth rotation parameters and related quantities giving
  !  - orientation of Earth rotation axis in space (i.e. precession and nutation)
  !  - position angle around this axis (i.e. sidereal time)
  !	Earth rotation velocity (J2000.0) = 7.2921151467e-5 rd/s
  !	see Aoki et al. (1982) or Astron. Almanach Suppl. (1984)
  !	if considered constant : error 1.e-8 (i.e. 0.01" after 24 hours)
  !  - orientation of the CIO (Conventional International Origin) with respect
  !	to the Celestial Ephemeris frame (i.e. pole motion)
  !---------------------------------------------------------------------
  character(len=*):: line           !
  logical :: error                  !
  ! Local
  character(len=4), parameter :: rname='TIME'
  integer(kind=4) :: date(7), nc
  real(kind=8) :: jutc, jnow, jut1, jtdt, ssun(2), w(3)
  real(kind=4) :: zone0
  character(len=12) :: time_chain, date_chain
  !------------------------------------------------------------------------
  ! Code
  !  transformation matrix from ecliptic (J2000.0) to true equatorial (JNOW):
  !  precession and nutation _ COMPUTE M30
  if (sic_present(0,1)) then
    time_chain = '*'
    date_chain = '*'
    call sic_ch (line,0,1,time_chain,nc,.true.,error)
    if (error) return
    call sic_ch (line,0,2,date_chain,nc,.false.,error)
    if (error) return
    call ctimen(time_chain,date(4:7),error)
    if (error) return
    call cdaten(date_chain,date(1:3),error)
    if (error) return
  else
    call utc (date)            ! read UTC from system clock
  endif
  call datejj (date,jnow)      ! transform it to julian date
  if (sic_present(1,0)) then
    jutc = jnow - d_ut1/86400.d0
  elseif (sic_present(2,0)) then
    jutc = jnow - d_tdt/86400.d0
  else
    jutc = jnow
  endif
  if (sic_present(4,0)) then
    call sic_r4 (line,4,1,zone0,.true.,error)
    if (error) return
    jutc = jnow - zone0/24.d0
  endif
  jut1 = d_ut1
  jtdt = d_tdt
  call do_astro_time(jutc,jut1,jtdt,error)
  if (error) return
  !
  if (sic_varexist('ASTRO%SOURCE')) then
    ! Warning message if source exists
    call astro_message(seve%w,rname,'TIME changed. Need to redo SOURCE command &
                        to compute positions and velocities for the new time')
  endif
  !
  ! Sun-avoidance limit
  if (sic_present(3,0)) then
    return
  endif
  !
  ! Do not plot if sun elevation lower than about 2 degrees
  if (xsun_2(3).lt.-pi/90) then
    return
  elseif (frame.eq.'HORIZONTAL') then
    call gr_segm('TIME',error)
    call spher(xsun_2,ssun)
    if (azref.eq.'S') then
      call small_circle(-ssun(1),ssun(2),slimit*pi/180.d0,error)
    else
      call small_circle(-ssun(1)+pi,ssun(2),slimit*pi/180.d0,error)
    endif
    call gr_segm_close(error)
  elseif (frame.eq.'EQUATORIAL') then
    call gr_segm('TIME',error)
    call matvec(xsun_0, trfm_30, w)
    call spher(w,ssun)
    call small_circle(ssun(1),ssun(2),slimit*pi/180.d0,error)
    call gr_segm_close(error)
  endif
end subroutine astro_time
!
subroutine do_astro_time(jutc,jut1,jtdt,error)
  use gbl_message
  use gkernel_interfaces
  use ast_horizon
  use ast_astro
  use ast_constant
  use astro_interfaces, except_this=>do_astro_time
  !---------------------------------------------------------------------
  ! @ public
  !  Compute Earth rotation parameters and related quantities giving
  !  - orientation of Earth rotation axis in space (i.e. precession and nutation)
  !  - position angle around this axis (i.e. sidereal time)
  !	Earth rotation velocity (J2000.0) = 7.2921151467e-5 rd/s
  !	see Aoki et al. (1982) or Astron. Almanach Suppl. (1984)
  !	if considered constant : error 1.e-8 (i.e. 0.01" after 24 hours)
  !  - orientation of the CIO (Conventional International Origin) with respect
  !	to the Celestial Ephemeris frame (i.e. pole motion)
  !---------------------------------------------------------------------
  real(kind=8), intent(in) :: jutc              !
  real(kind=8), intent(in) :: jut1              !
  real(kind=8), intent(in) :: jtdt              !
  logical, intent(inout) :: error                  !
  ! Local
  real(kind=8) :: dpsi,deps             ! nutation parameters
  real(kind=8) :: gst                   ! Greenwich mean sidereal time (radians)
  real(kind=8) :: epsm
  real(kind=8) :: result(6), angles(6), psi, the, phi, ct, st, mat1(9), mat2(9)
  real(kind=8) :: mat3(9), trfm_24(9), l(2), g(2), x_5(3), v_5(3), s_5(2)
  real(kind=8) :: distance, n
  integer(kind=4) :: i, j
  real(kind=8) :: ua
  parameter (ua=149597870.d0)
  real(kind=8) :: sunrise_ut, sunset_ut
  save sunrise_ut, sunset_ut
  logical :: first
  character(len=10) :: rname
  ! Data
  data rname /'ASTRO_TIME'/
  data first/.true./
  !------------------------------------------------------------------------
  jnow_utc = jutc
  d_tdt = jtdt
  d_ut1 = jut1
  jnow_tdt = jnow_utc + d_tdt/86400.d0
  jnow_ut1 = jnow_utc + d_ut1/86400.d0
  call qprec (j2000,jnow_utc,angles)  ! compute precession angles
  psi=angles(5)                       ! Euler angles of transformation
  the=angles(4)                       ! mean ecliptic J2000.0 to JNOW
  phi=-angles(6)-angles(5)
  call eulmat (psi,the,phi,mat1)      ! compute matrix
  epsm=oblimo (jnow_utc)              ! mean obliquity at JNOW
  call nuta (jnow_utc,dpsi,deps)      ! compute nutation at JNOW
  psi=-dpsi                           ! Euler angles of transformation
  the=0.d0                            ! mean ecliptic to "true" ecliptic
  phi=0.d0                            ! reference system
  call eulmat (psi,the,phi,mat2)      ! compute matrix
  call mulmat (mat1,mat2,mat3)        ! product MAT2 x MAT1
  psi=0.d0                            ! Euler angles of transformation
  the=-epsm-deps                      ! "true" ecliptic to true equatorial
  phi=0.d0                            ! reference system
  call eulmat (psi,the,phi,mat1)      ! compute matrix
  call mulmat (mat3,mat1,trfm_30)     ! product MAT1 x MAT3
  !
  !  Compute sidereal time
  !  The true sidereal time is equal to the mean sidereal time plus a nutation
  !  correction DPSI * cos (EPSM+DEPS) + terms smaller than 0.003"
  !  (see Guinot's BIH internal note and Aoki + al 1983, Celest. mech. 29, 335).
  !  According to G. Francou one should use DPSI * ( cosEPSM - 1/2 DEPS sinEPSM)
  !  This point has to be checked.
  !  %%% Take DUT1 = UT1 - UTC from TO$DUT	RL 21-oct-85
  !
  gst=tsmg(jnow_ut1)                  ! Greenwich mean ST at UTC=0. (radians)
  gst=gst+dpsi*dcos(epsm+deps)        ! nutation correction (already computed)
  ! East longitude is positive
  lst=gst+ pi/180d0*lonlat(1)         ! Local "true" sidereal time
  if (lst .lt. 0d0) lst = lst + twopi
  if (lst .ge. twopi) lst = lst - twopi
  !
  ! Earth rotation matrix (M43)
  do i=1, 3
    do j=1,3
      trfm_43(i+3*(j-1)) = 0
    enddo
  enddo
  ct = cos(lst)
  st = sin(lst)
  trfm_43(1) = ct
  trfm_43(5) = ct
  trfm_43(4) = st
  trfm_43(2) = - st
  trfm_43(9) = 1.d0
  !
  !  Compute Earth orientation, with respect to the instantaneous axis of rotation
  !  ### Pole motion should be included in TRFM24
  psi=halfpi                   ! 90 degrees
  the=halfpi-pi/180d0*lonlat(2)    ! 90 degrees minus latitude
  phi=-halfpi                  ! -90 degrees
  call eulmat (psi,the,phi,trfm_24)    ! compute transformation matrix
  call mulmat(trfm_43, trfm_24, trfm_23)   ! (M23) = M24 * M43
  call mulmat(trfm_30, trfm_23, trfm_20)   ! M20 = (M23) * M30
  ! Default G and Sun ephemeris ... (Page C24)
  do i=1,2
    n = jnow_tdt-j2000+i-1
    l(i) = mod(280.460d0 + 0.9856474*n + 36000.d0,360.d0)
    g(i) = mod(357.528d0 + 0.9856003*n + 36000.d0,360.d0)
    s_5(1) = (l(i) + 1.915*sin(pi*(g(i))/180.d0) + 0.020*sin(pi*(2*g(i))/  &
             180.0d0))*pi/180.
    s_5(2) = 0.0
    if (i.eq.1) then
      call rect(s_5, x_5)
    else
      call rect(s_5, v_5)
    endif
  enddo
  distance = (1.00014d0-0.01671d0*cos(pi*(g(1))/180.0d0)- 0.00014d0*   &
             cos(pi*(2*g(1))/180.0d0))*ua
  do i=1,3
    v_5(i) = (v_5(i)-x_5(i)) / 86400.d0 * distance
    x_5(i) = x_5(i) * distance
  enddo
  call qprec (jnow_tdt,j2000,angles)   ! compute precession angles
  psi=angles(5)                ! Euler angles of transformation
  the=angles(4)
  phi=-angles(6)-angles(5)
  ! compute matrix from ecl (now) to (R0)
  call eulmat (psi,the,phi,trfm_05)
  call matvec (x_5, trfm_05, xsun_0)
  call matvec (v_5, trfm_05, vg_0) ! use for velocity determination
  do i=1, 3
    xg_0(i) = xsun_0(i)
  enddo
  !
  ! Start ephemeride
  error = .false.
  !
  ! Get Earth motion (/ Sol Sys Barycenter)
  call ephsta (0, 0, error)
  if (error) then
    call astro_message(seve%f,rname,'Error in EPHSTA')
    return
  endif
  call ephvec (jnow_tdt, 1, result, error)
  if (.not.error) then
    do i=1, 3
      xg_0(i) = result(i)
      vg_0(i) = result(i+3)
    enddo
  else
    error = .false.
    call astro_message(seve%w,rname,'Using approximate Earth motion')
  endif
  !
  ! Get Sun position
  call ephsta( 2, 2, error)
  if (error) then
    call astro_message(seve%f,rname,'Error in EPHSTA')
    return
  endif
  !
  ! Get ecliptic J2000 coordinates
  call ephvec(jnow_tdt, 1, result, error)
  if (error) then
    error = .false.
    call astro_message(seve%w,rname,'Using approximate Sun position')
  else
    do i=1, 3
      xsun_0(i) = result(i)
    enddo
  endif
  call matvec(xsun_0, trfm_20, xsun_2)
  nsplot = 0
  !
  call sunrise(error)
  sunrise_ut= (mod(jnow_utc+0.5d0,1d0)+(sunriz(1)-lst)/twopi)*24.
  sunset_ut= (mod(jnow_utc+0.5d0,1d0)+(sunset(1)-lst)/twopi)*24.
  if (first) then
    call sic_def_dble ('SUNSET',sunset_ut,1,1,.true.,error)
    call sic_def_dble ('SUNRISE',sunrise_ut,1,1,.true.,error)
    first = .false.
  endif
  return
end subroutine do_astro_time
