subroutine astro_catalog(line,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>astro_catalog
  use ast_horizon
  use ast_astro
  use plot_molecules_globals
  !---------------------------------------------------------------------
  ! @ private
  ! ASTRO CATALOG name
  !	Defines the name of the source and molecular line catalog
  !---------------------------------------------------------------------
  character(len=*), intent(inout):: line           !
  logical, intent(inout) :: error                  !
  ! Local
  character(len=128) :: name,fbasename
  character(len=32) :: ext
  character(len=filename_length) :: file,file2,fdir,fsou,flin
  integer(kind=4) :: icat,l1,iext
  character(len=*), parameter :: rname='CATALOG'
  integer(kind=4), parameter :: optveleta=1
  integer(kind=4), parameter :: optalter=2
  integer(kind=4), parameter :: optline=3
  integer(kind=4), parameter :: optsource=4
  logical :: dosource
  !
  ! No arg = displays current catalogs
  if (.not.sic_present(0,1)) then
    l1 = lenc(catalog_name(1))
    if (l1.ne.0) then
      call astro_message(seve%i,rname,'Current source catalog is '//catalog_name(1))
    else
      call astro_message(seve%i,rname,'No Current source catalog opened')
    endif
    l1 = lenc(catalog_name(2))
    if (l1.ne.0) then
      call astro_message(seve%i,rname,'Alternate source catalog is '//catalog_name(2))
    else
      call astro_message(seve%i,rname,'No Alternate source catalog opened')
    endif
    l1 = lenc(molecules%catalog)
    if (l1.ne.0) then
      call astro_message(seve%i,rname,'Current line catalog is '//molecules%catalog)
    else
      call astro_message(seve%i,rname,'No Current line catalog opened')
    endif
    return
  endif 
  !
  call sic_ch (line,0,1,name,l1,.true.,error)
  if (error) return
  !
  !Resolve input file name
  call sic_parse_file(name,' ','',file)
  !Split file name to extract extension
  call sic_parse_name(file,fbasename,ext,fdir)
  !
  ! /VELETA Option
  if (sic_present(optveleta,0)) then
    if (sic_present(optline,0)) then
      call astro_message(seve%e,rname,'Options /VELETA and /LINE are exclusive from each other')
      error=.true.
      return
    endif
    if (ext.eq.'cat') then
      call astro_message(seve%w,rname,'/VELETA requires the file name without extension .CAT')
      file=file(1:iext-1)
    else if (len_trim(ext).ne.0) then
      call astro_message(seve%e,rname,'/VELETA requires a file name without extension')
      error=.true.
      return
    endif
    call format_catalog(line,error)
    if (error) return
    dosource=.true.
  endif
  !
  if (sic_present(optsource,0)) then !Option source
    dosource=.true.
  else if (sic_present(optline,0)) then !Option line
    dosource=.false.
  else if (ext.eq.'lin') then !Mode deduced from extension
    dosource=.false.
  else if (ext.eq.'sou') then !!Mode deduced from extension
    dosource=.true.
  else if (ext.eq.'') then
    write (fsou,'(a,a)') trim(file),'.sou'
    write (flin,'(a,a)') trim(file),'.lin'
    if (gag_inquire(fsou,len_trim(fsou)).eq.0) then !No extension and file.sou found
      dosource=.true.
    else if (gag_inquire(flin,len_trim(flin)).eq.0) then !!No extension and file.lin found
      dosource=.false.
    else
      call astro_message(seve%e,rname,'Could not find any catalog file')
      error=.true.
      return
    endif
  else !Extension given but neither .sou nor .lin
    dosource=.true.
  endif
  !
  if (dosource) then
    ext='.sou'
    if (sic_present(optalter,0)) then
      icat = 2
    else
      icat = 1
    endif  
    call get_catalog_name(file,file2,ext,error)
    if (error) return
    catalog_name(icat) = file2
    next_source(icat) = 1
    if (icat.eq.1) then
      call astro_message(seve%i,rname,'New source catalog is '//catalog_name(icat))
    else
      call astro_message(seve%i,rname,'New alternate source catalog is '//catalog_name(icat))
    endif
  else 
    ext='.lin'
    call get_catalog_name(file,file2,ext,error)
    if (error) return
    molecules%catalog = file2
    call astro_message(seve%i,rname,'New line catalog is '//molecules%catalog)
  endif
  !
end subroutine astro_catalog
!
subroutine get_catalog_name(file,file2,ext,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>get_catalog_name
  !---------------------------------------------------------------------
  ! @ private
  ! Decode the new catalog name
  !---------------------------------------------------------------------
  character(len=*), intent(inout) :: file    !
  character(len=*), intent(inout) :: file2   !
  character(len=*), intent(inout) :: ext     !
  logical,          intent(inout) :: error   !
  ! LOCal
  character(len=*), parameter :: rname='CATALOG'
  !
  call sic_parse_file(file,' ',ext,file2)
  if (gag_inquire(file2,len_trim(file2)).ne.0) then
    call astro_message(seve%e,rname,'Error opening '//trim(file2))
    error = .true.
    return
  endif
  !
end subroutine get_catalog_name
