subroutine noema_febe(line,error)
  use gbl_message
  use gkernel_interfaces
  use gkernel_types
  use astro_interfaces, except_this=>noema_febe
  use ast_astro
  use my_receiver_globals
  !-----------------------------------------------------------------------
  ! @ private
  ! Set of tools to put in memory some freqsetups and come back to them
  ! set of MULTI ARG commands
  !-----------------------------------------------------------------------
  character(len=*), intent(inout) :: line        ! command line
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='FEBE'
  integer(kind=4), parameter :: n_keys=6
  integer(kind=4) :: nc,nkey,iload,iremove
!   real(kind=8)  :: 
!   logical :: 
  character(len=256) :: keys(n_keys),key,arg
  !
  data keys/'ADD','LOAD','SAVE','LIST','REMOVE','CLEAR'/
  !
  if (obsname.ne.'NOEMA') then
    call astro_message(seve%e,rname,'Inconsistency between Receiver Name and Observatory')
    error = .true.
    return
  endif
  !
  if (noema%recdesc%name.ne.'NOEMA') then
    call astro_message(seve%e,rname,'We should be working with NOEMA receivers')
    error = .true.
    return
  endif
  !
  if (sic_narg(0).eq.0) then
    call astro_message(seve%i,rname,'Nothing to do')
    return
  endif
  !
  ! Allocate the register
  call noema%register%reallocate(m_register_alloc,error)
  if (error) return
  !
  ! Read the first
  call sic_ke(line,0,1,arg,nc,.true.,error)
  if (error) return
  ! Understand the key
  call sic_ambigs(rname,arg,key,nkey,keys,n_keys,error)
  if (error) return
  !
  select case (key)
  case ('ADD')
    if (sic_narg(0).gt.1) then
      call astro_message(seve%i,rname,'ADD option do not accept any argument')
      return
    endif
    call noema%register%add(noema%cfebe,error)
    if (error) return
  case ('LIST')
      if (sic_narg(0).gt.1) then
      call astro_message(seve%i,rname,'LIST option do not accept any argument')
      return
    endif
    call noema%register%list(noema%recdesc,error)
    if (error) return
  case ('LOAD')
    if (sic_narg(0).lt.2) then
      call astro_message(seve%i,rname,'Missing number of the FEBE to load')
      return
    endif
    if (sic_narg(0).gt.2) then
      call astro_message(seve%i,rname,'Only 1 FEBE can be loaded at a time')
      return
    endif
    ! Read the number of the setup to load
    call sic_i4(line,0,2,iload,.true.,error)
    if (error) return
    call  noema%register%load(iload,noema%cfebe,error)
    if (error) return
  case ('REMOVE')
    if (sic_narg(0).lt.2) then
      call astro_message(seve%i,rname,'Missing number of the FEBE to remove')
      return
    endif
    if (sic_narg(0).gt.2) then
      call astro_message(seve%i,rname,'Only 1 FEBE can be removed at a time')
      return
    endif
    ! Read the number of the setup to load
    call sic_i4(line,0,2,iremove,.true.,error)
    if (error) return
    call noema%register%remove(iremove,error)
    if (error) return
  case ('SAVE')
    ! save loaded settings
    call noema%register%save(noema%cfebe,error)
    if (error) return
  case ('CLEAR')
    ! empty the list of saved febes
    if (sic_narg(0).gt.1) then
      call astro_message(seve%i,rname,'CLEAR option do not accept any argument')
      return
    endif
    call noema%register%clear(error)
    if (error) return
  case default
    call astro_message(seve%e,rname,'Keyword not understood')
    error = .true.
    return
  end select
  !
end subroutine noema_febe
