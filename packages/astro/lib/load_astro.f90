subroutine load_astro(mode)
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*) :: mode           !
  ! Global
  external :: run_astro, run_pdbi, run_alma, run_30m, run_noemaoffline, run_noemaonline
  logical, external :: gr_error
  ! Local
  integer(kind=4),  parameter :: mcom_astro=68
  character(len=12) :: vocab_astro(mcom_astro)
  integer(kind=4),  parameter :: mcom_pdbi=27
  character(len=12) :: vocab_pdbi(mcom_pdbi)
  integer(kind=4),  parameter :: mcom_noemaoffline=44
  character(len=12) :: vocab_noemaoffline(mcom_noemaoffline)
  integer(kind=4),  parameter :: mcom_noemaonline=15
  character(len=12) :: vocab_noemaonline(mcom_noemaonline)
  integer(kind=4),  parameter :: mcom_alma=14
  character(len=12) :: vocab_alma(mcom_alma)
  integer(kind=4),  parameter :: mcom_30m=8
  character(len=12) :: vocab_30m(mcom_30m)
  !
  character(len=1) :: backslash
  logical :: error
  !
  ! Commandes ASTRO
  data vocab_astro /                                                       &
    ' ATMOSPHERE', '/PRINT','/FREQUENCIES',                                &
    ' CATALOG', '/VELETA','/ALTERNATE','/LINE','/SOURCE',                  &
    ' CONVERT', '/ALTERNATE','/J2000','/PRECESSION',              &
                '/GALACTIC','/FLUX','/NAME','/INPUT',                      &
    ' CONSTELL', '/DRAW','/NAME','/BOUNDARIES ',                           &
    ' FRAME',                                                              &
    ' HORIZON', '/SOURCE','/PLANET','/ALTERNATE','/AIRMASSES','/PROJECT',  &
                '/FLUX','/NIGHT_MARKS','/ELEVATION',                       &
    ' HEADER',                                                             &
    ' OBSERVATORY',                                                        &
    ' PLANET', '/DRAW','/EPHEMERIS','/QUIET','/BEAM','/NOCLIP',            &
    ' SET',                                                                &
    ' SHOW',                                                               &
    ' SOURCE', '/DRAW','/ALTERNATE','/FLUX','/SUN','/PRINT','/CURSOR',     &
               '/QUIET','/VARIABLE','/NOCLIP','/RESET','/DOPPLER',         &
    ' TIME', '/UT1','/TDT','/NODRAW','/ZONE',                              &
    ' UV_DOPPLER',                                                         &
    ' UV_TRACKS', '/FRAME','/HOUR_ANGLE','/TABLE','/HORIZON',              &
                  '/INTEGRATION','/STATIONS','/SIZES','/OFFSET','/WEIGHT'/
  !
  ! Commandes PDBI
  data vocab_pdbi /                                                     &
    ! Line : old receivers
    ' LINE', '/MOLECULES', '/SPECTRAL', '/WIDTH', '/IF_LIMITS',         &
    '/BEST', '/AUTO', '/VLBI',                                          &
    ! Line : new receivers
    '/RECEIVER', '/FIXED_FREQ', '/ONGRID',                              &
    ' PLOT','/LIMITS','/MOLECULES','/WIDTH','/ATMOSPHERE',              &
    '/SPURIOUS',                                                        &
    ' NARROW_INPU','/RECEIVER',                                         &
    ' PRINT',                                                           &
    ' SPECTRAL','/NARROW_INPU', '/BAND', '/RECEIVER','/RF',             &
    ' WIDEX ','/RECEIVER'/
  ! Commandes NOEMA
  data vocab_noemaoffline /                                             &
    ' BASEBANDS','/RESET','/MODE',                                      &
    ' FEBE',                                                            &
    ' LIST','/CONFLICT','/INDEX',                                       &
    ' NEWVEL','/FIXED_FREQ','/SETUP','/LINES','/TRACKSH',               &
    ' PLOT','/RECEIVER','/PROPOSAL','/LINES','/CHECKBAND',              &
    ' PROPOSAL','/FILE','/TIME','/FEBE',                                &
    ' RESET',                                                           &
    ' SETUP','/FILE',                                                   &
    ' SPW','/RANGE','/FREQUENCY','/CHUNK','/LABEL',                     &
    ' SPECSWEEP','/RANGE','/BANDMAX','/NTUNING','/ZOOM','/FILE',        &
                 '/ADD_FEBE',                                           &
    ' TSYS', '/CONTINUUM','/DBR',                                       &
    ' TUNING','/ZOOM','/PAGEWIDTH','/FIXED_FREQ','/INFO'/
  !Command OBS pour noema
  data vocab_noemaonline /                                              &
    ' BASEBANDS','/RECEIVER',                                           &
    ' LINE','/RECEIVER',                                                &
    ' PROPOSAL','/FILE',                                                &
    ' SHOW','/PLOT',                                                    &
    ' SPW','/RANGE','/FREQUENCY','/CHUNK','/LABEL',                     &
    '/BASEBAND','/RECEIVER'/
  data vocab_alma /           &
    ' PLOT','/LIMITS','/MOLECULES','/WIDTH','/ATMOSPsHERE',             &
    '/SPURIOUS',                                                        &
    ! ALMA
    ' FREQUENCY ',                                                      &
    ' BASEBAND',                                                        &
    ' SPWINDOW','/BASEBAND/','/POLAR','/USE','/RESET', '/RESTRICT' /
  data vocab_30m /                                                      &
    '/SPURIOUS',                                                        &
    ! 30-m
    ' BACKEND',                                                         &
    ' EMIR','/ZOOM','/PAGEWIDTH','/INFO',                               &
    ' BASEBANDS','/SINGLEPOLAR' /
  !------------------------------------------------------------------------
  error = gr_error()
  error = .false.
  !
  backslash = char(92)
  freq = 100.0d0
  library_mode = mode.eq.'LIBRARY'
  if (library_mode) then
    call sic_begin('ASTRO','GAG_HELP_ASTRO',mcom_astro,vocab_astro,' ',  &
    run_astro,gr_error)
  else
    ! Load ASTRO language
    call sic_begin('ASTRO','GAG_HELP_ASTRO',mcom_astro,vocab_astro,  &
    '3.1    30-Nov-2006 ', run_astro,gr_error)
    ! Load PDBI language
    call sic_begin('PDBI','GAG_HELP_PDBI',mcom_pdbi,vocab_pdbi,  &
    '1.0   30-Jun-2006',run_pdbi,gr_error)
    ! Load NOEMA language
    call sic_begin('NOEMAOFFLINE','GAG_HELP_NOEMAOFFLINE',mcom_noemaoffline,vocab_noemaoffline,  &
    '1.0',run_noemaoffline,gr_error)
    call sic_begin('NOEMAONLINE','GAG_HELP_NOEMAONLINE',mcom_noemaonline,vocab_noemaonline,  &
    '1.0',run_noemaonline,gr_error)
    call sic_begin('ALMA','GAG_HELP_PDBI',mcom_alma,vocab_alma,  &
    '1.0   30-Jun-2012',run_alma,gr_error)
    call sic_begin('PICO','GAG_HELP_PICO',mcom_30m,vocab_30m,  &
    '1.0   10-Aug-2013',run_30m,gr_error)
    ! Set is by default ASTRO\SET, but will be given to greg or flux if possible
!     call sic_setsymbol('SET','ASTRO'//backslash//'SET',error)
    ! Default marker: a star
    call gr_exec1('SET MARKER 5 2 0.5')
  endif
end subroutine load_astro
!
subroutine run_astro(line,comm,error)
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  character(len=12) :: comm         !
  logical :: error                  !
  ! Local
  call astro_message(seve%c,'ASTRO',line)
  !
  ! Call appropriate subroutine according to COMM
  if (comm.eq.'ATMOSPHERE') then
    call astro_atm(line,error)
  elseif (comm.eq.'CATALOG') then
    call astro_catalog(line,error)
  elseif (comm.eq.'CONVERT') then
    call astro_j2000(line,error)    ! B1950 -> J2000 or reverse
  elseif (comm.eq.'CONSTELL') then
    call astro_constell(line,error)
  elseif (comm.eq.'HEADER') then
    call astro_header(line,error)
  elseif (comm.eq.'FRAME') then
    call astro_frame(line,error)
! ELSEIF (COMM.EQ.'LINE') THEN
!   IF (SIC_PRESENT(6,0)) THEN
!     CALL LINE_AUTO(LINE,ERROR)
!   ELSE
!     CALL ASTRO_LINE(LINE,ERROR)
!   ENDIF
  elseif (comm.eq.'HORIZON') then
    call astro_horizon(line,error)
  elseif (comm.eq.'OBSERVATORY') then
    call astro_observatory_command(line,error)
  elseif (comm.eq.'PLANET') then
    call astro_planet(line,error)
  elseif (comm.eq.'SET') then
    call astro_set_command(line,error)
  elseif (comm.eq.'SHOW') then
    call astro_show(line,error)
  elseif (comm.eq.'SOURCE') then
    call astro_source(line,error)
  elseif (comm.eq.'TIME') then
    call astro_time(line,error)
  elseif (comm.eq.'UV_TRACKS') then
    call astro_uv(line,error)
  elseif (comm.eq.'UV_DOPPLER') then
    call astro_uv_doppler(line,error)
  else
    call astro_message(seve%e,'RUN_ASTRO','Unimplemented command '// comm)
    error = .true.
  endif
end subroutine run_astro
!
subroutine run_pdbi(line,comm,error)
  use gbl_message
  use ast_line
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  character(len=12) :: comm         !
  logical :: error                  !
  ! Global
  logical :: sic_present
  ! Local
  character(len=128) :: ch
  character(len=4), parameter :: rname='PDBI'
  !------------------------------------------------------------------------
  !
  call astro_message(seve%c,rname,line)
  !
  if (obsname.ne.'BURE') then
    write(ch,'(a,1x,a)') 'Command invalid with current observatory',obsname
    call astro_message(seve%e,rname,ch)
    error=.true.
    return
  endif
  !
  ! PLOT used for PDBI and ALMA
  if (comm.eq.'PLOT') then
    if (plot_mode.lt.10) then ! Means this is indeed PdBI, not ALMA
      if (obs_year.lt.2006) then
        call astro_message(seve%i,rname,'Command invalid in this context')
        call astro_message(seve%i,rname,'You have selected the old  &
                           generation receivers')
        error = .true.
        return
      endif
    endif
    call pdbi_plot(line,error)
  !
  ! PdBI commands
  !
  else
    if (obs_year.lt.2006) then
      !
      ! Old generation receivers
      !
      write(ch,'(a,I4)') 'Using PdBI status as of ', obs_year
      call astro_message(seve%i,rname,ch)
      if (comm.eq.'LINE') then
        if (sic_present(6,0)) then
          call line_auto(line,error)
        else
          call astro_line(line,error)
        endif
      else
        call astro_message(seve%i,rname,'Command invalid in this context')
      endif
    else
      !
      ! New generation receivers
      !
      if (comm.eq.'LINE') then
        call pdbi_line(line,error)
      elseif (comm.eq.'NARROW_INPU') then
        call pdbi_narrow(line,error)
      elseif (comm.eq.'PRINT') then
        call pdbi_print(line,error)
      elseif (comm.eq.'SPECTRAL') then
        call pdbi_spectral(line,error)
      elseif (comm.eq.'WIDEX') then
        call pdbi_widex(line,error)
      endif
        !
    endif
  endif
  !
end subroutine run_pdbi
!
subroutine run_noemaoffline(line,comm,error)
  use gbl_message
  use astro_interfaces, except_this=>run_noemaoffline
  use ast_line
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  character(len=12) :: comm         !
  logical :: error                  !
  ! Local
  character(len=12), parameter :: rname='NOEMAOFFLINE'
  character(len=128) :: ch
  logical :: converttoonline
  !------------------------------------------------------------------------
  !
  call astro_message(seve%c,rname,line)
  !
  if (obsname.ne.'NOEMA') then
      write(ch,'(a,1x,a)') 'Command invalid with current observatory:',obsname
      call astro_message(seve%e,rname,ch)
      error=.true.
      return
  endif
  if (noema_mode.ne.'OFFLINE') then
      write(ch,'(a,1x,a)') 'Command invalid with current NOEMA mode:',noema_mode
      call astro_message(seve%e,rname,ch)
      error=.true.
      return
  endif
  !
  if (comm.eq.'BASEBANDS') then
     call noema_baseband(line,error)
  else if (comm.eq.'LIST') then
     call noema_list(line,error)
  else if (comm.eq.'FEBE') then
     call noema_febe(line,error)     
  else if (comm.eq.'NEWVEL') then
     call noema_changevelo(line,error)
  else if (comm.eq.'PLOT') then
     call noema_plot(line,error)
  else if (comm.eq.'PROPOSAL') then
     converttoonline=.false.
     call noema_setup_file(comm,line,converttoonline,error)
  else if (comm.eq.'RESET') then
     call noema_reset(line,error)
  else if (comm.eq.'SETUP') then
     converttoonline=.true.
     call noema_setup_file(comm,line,converttoonline,error)
  else if (comm.eq.'SPW') then
     call noema_spw(line,error)
  else if (comm.eq.'SPECSWEEP') then
     call noema_spectral_sweep(line,error)
  else if (comm.eq.'TSYS') then
     call noema_tsys(line,error)
  else if (comm.eq.'TUNING') then
     call rec_noema(line,error)
  endif
  !
end subroutine run_noemaoffline
!
subroutine run_noemaonline(line,comm,error)
  use gbl_message
  use astro_interfaces, except_this=>run_noemaonline
  use ast_line
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  character(len=12) :: comm         !
  logical :: error                  !
  ! Local
  character(len=11), parameter :: rname='NOEMAONLINE'
  character(len=128) :: ch
  logical :: converttoonline
  !------------------------------------------------------------------------
  !
  call astro_message(seve%c,rname,line)
  !
  if (obsname.ne.'NOEMA') then
      write(ch,'(a,1x,a)') 'Command invalid with current observatory:',obsname
      call astro_message(seve%e,rname,ch)
      error=.true.
      return
  endif
  if (noema_mode.ne.'ONLINE') then
      write(ch,'(a,1x,a)') 'Command invalid with current NOEMA mode:',noema_mode
      call astro_message(seve%e,rname,ch)
      error=.true.
      return
  endif
  !
  if (comm.eq.'BASEBANDS') then
    call noema_baseband_online(line,error)
  else if (comm.eq.'LINE') then
    call rec_noema_online(line,error)
  else if (comm.eq.'PROPOSAL') then
     converttoonline=.false.
     call noema_setup_file(comm,line,converttoonline,error)
  else if (comm.eq.'RESET') then
    call noema_reset(line,error)
  else if (comm.eq.'SHOW') then
    call noema_show_online(line,error)
  else if (comm.eq.'SPW') then
     call noema_spw_online(line,error)
  endif
  !
end subroutine run_noemaonline
!
subroutine run_alma(line,comm,error)
  use gbl_message
  use ast_line
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  character(len=12) :: comm         !
  logical :: error                  !
  ! Local
  character(len=4),parameter :: rname='ALMA'
  character(len=128) :: ch
  !------------------------------------------------------------------------
  !
  call astro_message(seve%c,rname,line)
  !
  if (obsname.ne.'ALMA') then
    write(ch,'(a,1x,a)') 'Command invalid with current observatory',obsname
    call astro_message(seve%e,rname,ch)
    error=.true.
    return
  endif
  !
  ! PLOT used for PDBI and ALMA
  !
  if (comm.eq.'PLOT') then
    if (plot_mode.lt.10) then ! Means this is indeed PdBI, not ALMA
      if (obs_year.lt.2006) then
        call astro_message(seve%i,rname,'Command invalid in this context')
        call astro_message(seve%i,rname,'You have selected the old  &
                              generation receivers')
        error = .true.
        return
      endif
    endif
  call pdbi_plot(line,error)
  !
  ! ALMA commands
  !
  elseif (comm.eq.'FREQUENCY') then
     !if (alma_cycle.eq.0) then
     !   call astro_message(seve%i,'ALMA','ALMA Cycle 0')
     !else
     !   call astro_message(seve%i,'ALMA','Assuming FULL ALMA capabilities')
     !endif
     call alma_line(line,error)
  elseif (comm.eq.'BASEBAND') then
     call alma_baseband(line,error)
  elseif (comm.eq.'SPWINDOW') then
     call alma_spectral(line,error)
  endif
  !
end subroutine run_alma
!
subroutine run_30m(line,comm,error)
  use gbl_message
  use ast_line
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  character(len=12) :: comm         !
  logical :: error                  !
  ! Local
  character(len=4), parameter :: rname='30M '
  character(len=128) :: ch
  !------------------------------------------------------------------------
  !
  call astro_message(seve%c,rname,line)
  !
  if (obsname.ne.'VELETA') then
    write(ch,'(a,1x,a)') 'Command invalid with current observatory',obsname
    call astro_message(seve%e,rname,ch)
    error=.true.
    return
  endif
  ! 30M commands
  if (comm.eq.'BACKEND') then
    call pico_backend(line,error)
  elseif (comm.eq."EMIR") then
    call pico_emir(line,error)
  elseif (comm.eq.'BASEBANDS') then
    call emir_switchbox(line,error)
  endif
  !
end subroutine run_30m
!
subroutine astro_exec(buffer)
  use gildas_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !	Library version of the ASTRO program.
  !	All the ASTRO commands are accessible through calls in the form:
  !
  !	call us_exec ('This_command')
  !---------------------------------------------------------------------
  character(len=*) :: buffer
  ! Global
  integer(kind=4) :: lenc
  ! Local
  logical :: error
  character(len=255) :: line
  character(len=12) :: comm,lang
  integer(kind=4) :: nl
  character(len=10) :: rname
  ! Data
  data rname /'ASTRO_EXEC'/
  !------------------------------------------------------------------------
  !
  ! Copy the command line in argument to the local buffer LINE
  ! Analyse it
  line = buffer
  nl=lenc(line)
  call sic_format (line,nl)
  call sic_analyse(comm,line,nl,error)
  if (error) then
    call astro_message(seve%f,rname,'Error Interpreting Line')
    goto 1000
  endif
  !
  ! If your application is multi-language, then make sure that the
  ! command belongs to your language.
  call sic_lang(lang)
  if (lang .ne. 'ASTRO') then
    call astro_message(seve%f,rname,'Language Mismatch Line')
    goto 1000
  endif
  !
  ! Execute it
  call run_astro(line,comm,error)
  if (error) then
    call astro_message(seve%f,rname,'Error Executing Line')
    goto 1000
  endif
  !
  ! Job done, Return
  return
  !
  ! No error recovery for Library Version
1000  call astro_message(seve%f,rname,line(1:nl))
  call sysexi(fatale)
end subroutine astro_exec
