subroutine alma_baseband(line,error)
  use gkernel_interfaces
  use gbl_message
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  ! PDBI internal routine, support for command
  !	BASEBAND i freq sideband
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Local
  integer(kind=4) :: iband,n,nkey,nc,mkeys, firstbase, lastbase
  real(kind=4) :: cfreq, x4, y4, tmp, rffromif
  real(kind=8) :: xu, yu
  character(len=1) :: code
  character(len=3) :: qq
  character(len=13) :: rname
  !
  parameter(mkeys=2)
  character(len=3) :: key, keys(mkeys)
  character(len=128) :: chain
  ! Data
  data keys/'LSB','USB'/
  data rname /'ALMA_BASEBAND'/
  ! ---------------------------------------------------------------------------
  !
  n = sic_narg(0)
  if (n.gt.3) then
     call astro_message(seve%e,rname,'Wrong number of arguments')
     error = .true.
     return
  endif
  !
  if (n.eq.0) then
     !
     ! Command BASE alone means a loop on all four basebands
     !
     firstbase = 1
     lastbase = 4
     bb_def(:) = .false.  ! reset all basebands
  else
     !
     ! First argument is the baseband number
     !
     call sic_i4(line,0,1,iband,.false.,error)
     if ((iband.lt.1).or.(iband.gt.4)) then
        call astro_message(seve%e,rname,'Only four basebands')
        error =.true.
        return
     endif
     firstbase = iband
     lastbase = iband
     !
     ! Possibly switch to "plot baseband iband"
     !
     if (plot_mode.gt.10 .and. plot_mode.lt.15) then
        plot_mode = 10+iband
     endif
  endif
  !
  ! Loop on basebands
  !
  do iband = firstbase, lastbase
     !
     ! CURSOR option
     !
     if (n.le.1) then
        !
        ! plot_mode must be 10
        !
        if (plot_mode.ne.10) then
           plot_mode = 10
           call alma_plot_def(error)
           if (error) return
           call alma_plot_line
        endif
        !
        call gr_curs (xu, yu, x4, y4, code)
        if ((yu.ge.0).and.(yu.le.5)) then
           bb_sideband(iband) = 1
           key = 'USB'
        elseif ((yu.ge.7.75).and.(yu.le.12.75)) then
           bb_sideband(iband) = -1
           key = 'LSB'
        else
           call astro_message(seve%e,rname,'Please select LSB or USB')
           error =.true.
           return
        endif
        !
        if (bb_sideband(iband).eq.-1) then
           xu = iflim(2)-(xu-iflim(1)) ! LSB: IF limits are flipped on the plot
        endif
        if (xu.lt.(iflim(1)+1000)) then
           call astro_message(seve%w,rname,'Baseband must fit into IF band')
           xu = iflim(1)+1000
        elseif (xu.gt.(iflim(2)-1000)) then
           call astro_message(seve%w,rname,'Baseband must fit into IF band')
           xu = iflim(2)-1000
        endif
        bb_cfreq(iband) = xu
     else
        !
        ! No CURSOR (in that case the loop firstband/lastband is useless)
        !
        ! Central frequency
        !
        call sic_r4(line,0,2,cfreq,.false.,error)
        !
        if (cfreq.lt.1000) then
           !
           ! Defined in RF instead of IF?
           !
           cfreq = cfreq*1000.  ! in MHz
           call decoderf(cfreq, tmp, bb_sideband(iband))
           if ((tmp.lt.(iflim(1)+1000)).or.(tmp.gt.(iflim(2)-1000))) then
              call astro_message(seve%e,rname,'Baseband must fit into IF band')
!**!              error =.true.
!**!              return
           endif
           bb_cfreq(iband) = tmp
           key = "LSB"
           if (bb_sideband(iband).eq.1) key = "USB"
        else
           !
           ! Defined as IF, BAND
           if ((cfreq.lt.(iflim(1)+1000)).or.(cfreq.gt.(iflim(2)-1000))) then
              call astro_message(seve%e,rname,'Baseband must fit into IF band')
              error =.true.
              return
           endif
           bb_cfreq(iband) = cfreq
           !
           ! Sideband
           !
           call sic_ke(line,0,3,qq,nc,.false.,error)
           call sic_upper(qq)
           call sic_ambigs(rname,qq,key,nkey,keys,mkeys,error)
           if (error) return
           if (key.eq.'LSB') then
              bb_sideband(iband) = -1
           elseif (key.eq.'USB') then
              bb_sideband(iband) = 1
           endif
        endif
     endif
     !
     ! Check if this is possible
     !
     call check_basebands(iband, error)
     if (error) return
     bb_def(iband) = .true.
     !
     ! Reset spectral windows associated to that baseband
     ! NO - dont do it
     !  bb_nspw(iband) = 0
     !
     ! Output message
     !
     tmp = rffromif(bb_cfreq(iband),bb_sideband(iband))/1000.
     write(chain,1000) iband, bb_cfreq(iband), key
     call astro_message(seve%i,rname,chain)
     write(chain,1010) iband, tmp
     call astro_message(seve%i,rname,chain)
     !
     ! Now update the current plot
     !
     call alma_plot_line
     !
     ! End of loop on basebands
     !
  enddo
  !
  1000 format('BASEBAND ',i1,' is centered at IF1 = ',f8.2,' MHz (',a3,')')
  1010 format('BASEBAND ',i1,' is centered at RF = ',f10.5,' GHz')
end subroutine alma_baseband
!
subroutine check_basebands(iband, error)
  use gbl_message
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4), intent(in) :: iband  !
  logical, intent(out) :: error         !
  ! Local
  integer(kind=4) :: i, usb_count, lsb_count
  !
  error = .false.
  usb_count = 0
  lsb_count = 0
  do i = 1,4 ! Loop on basebands
    if (bb_def(i).or.(i.eq.iband)) then  ! bb_def(iband) not yet updated
        if (bb_sideband(i).eq.1) then
          usb_count = usb_count+1
        else
          lsb_count = lsb_count+1
        endif
    endif
  enddo
  if ((recband.ne.9).and.   &
      ((usb_count.eq.3 .and. lsb_count.eq.1)  &
      .or. (usb_count.eq.1 .and. lsb_count.eq.3) )) then
    call astro_message(seve%e,'ALMA_BASEBAND','Cannot have 3+1 basebands combination')
    error = .true.
  endif
end subroutine check_basebands
