subroutine astro_observatory_command(line,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>astro_observatory_command
  use ast_line
  use ast_astro
  use my_receiver_globals
  use frequency_axis_globals
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  ! OBSERVATORY name [version] [on|off] 
  ! or
  ! OBSERVATORY Long Lat Altitude [Sun-Avoidance]
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: line   !
  logical,          intent(out) :: error  !
  ! Local
  character(len=*), parameter :: rname='OBSERVATORY'
  character(len=12) :: argum
  integer(kind=4) :: nc,ier,nk,year,acycle
  character(len=24) :: clon,clat
  real(kind=8) :: alti,slim
  integer(kind=4), parameter :: mobs=45
  character(len=12) :: knownobs(mobs),previous
  ! Data
  data knownobs /  &
    'BURE','PDBI','NOEMA','PICOVELETA','VELETA','30M',  &  ! IRAM ones first
    'ACA','ALMA','APEX','ATF','CARMA','CSO','EFFELSBERG','FAST','FCRAO','GBT',  &
    'GI2T','GLT','HAYSTACK','HHT','IOTA','JCMT','KITTPEAK','KOSMA','LASILLA',   &
    'LMT','MAUNAKEA','MEDICINA','MRO','NRO','NOBEYAMA','NOTO','OSO','PARANAL',  &
    'PTI','SEST','SMA','SMT','SPT','SRT','TRAO','VLA','VLT','WESTFORD','YEBES' /
  !
  previous=obsname
  error = .false.
  if (sic_narg(0).eq.0) then
    !
    ! No argument == decode GAG_ASTRO_OBS
    !
    ier = sic_getlog('GAG_ASTRO_OBS',argum)
    if (ier.ne.0) then
      call astro_message(seve%e,rname,  &
        'Failure in translation of GAG_ASTRO_OBS logical')
      error = .true.
      return
    endif
  endif
  call sic_ke (line,0,1,argum,nc,.false.,error)
  if (error) return
  if (argum.eq.'?') then
    ! sic_ambigs_list required to keep "observatory ?" working with the use of sic_ambigs_sub
    call sic_ambigs_list(rname,seve%i,'Choices are:',knownobs)
    return
  endif
  call sic_ambigs_sub(rname,argum,obsname,nk,knownobs,mobs,error)
  if (error) return
  if (nk.ne.0) then
    !name of the observatory is recognized
    call astro_observatory(obsname,error)
    if (error)  return
    call astro_message(seve%i,rname,'Selected '//trim(obsname)//' observatory')
    if (.not.library_mode) then
      ! Astro was loaded in INTERACTIVE mode, including PDBI\ and ALMA\
      ! languages.
      select case (obsname)
      case ('NOEMA')
        call parse_obs_noema(line,year,noema_mode,error)
        if (error) return
        obs_year=year
        call noema_message(year)
        ! NOEMA : language NOEMA
        call exec_string ('SIC  PICO\ OFF',error)
        call exec_string ('SIC  ALMA\ OFF',error)
        call exec_string ('SIC  PDBI\ OFF',error)
        if (noema_mode.eq.'OFFLINE') then
          call exec_string ('SIC  NOEMAONLINE\ OFF',error)
          call exec_string ('SIC  NOEMAOFFLINE\ ON',error)
          call exec_string ('SIC  PRIO 1 NOEMAOFFLINE',error)
          if (previous.ne.'NOEMA') then
            call noema_reset_setup(noema%cfebe%tune,noema%cfebe%i_f,error)
            if (error) return
          endif
        else if (noema_mode.eq.'ONLINE') then
          call astro_message(seve%i,rname,'Selecting ONLINE (i.e. OBS-like) language')
          ! Check that main freq axis is REST:
          if (freq_axis%main.ne.'REST') then
            call astro_message(seve%e,rname,'Main frequency axis must be REST to work with NOEMA ONLINE')
            call astro_message(seve%e,rname,'Please type SET FREQUENCY REST [IF1,IF2,...] and try again')
            error = .true.
            return
          endif
          call exec_string ('SIC  NOEMAOFFLINE\ OFF',error)
          call exec_string ('SIC  NOEMAONLINE\ ON',error)
          call exec_string ('SIC  PRIO 1 NOEMAONLINE',error)
          if (previous.ne.'NOEMA') then
            call noema_reset_setup(noema%cfebe%tune,noema%cfebe%i_f,error)
          endif
        endif
      case ('BURE','PDBI')
        call parse_obs_pdbi(line,year,error)
        if (error) return
        obs_year=year
        call pdbi_message(obs_year)
        call exec_string ('SIC  PICO\ OFF',error)
        call exec_string ('SIC  ALMA\ OFF',error)
        call exec_string ('SIC  NOEMAOFFLINE\ OFF',error)
        call exec_string ('SIC  NOEMAONLINE\ OFF',error)
        call exec_string ('SIC  PDBI\ ON',error)
        call exec_string ('SIC  PRIO 1 PDBI',error)
        obsname='BURE'
      case ('ALMA','ACA')
        call parse_obs_alma(line,acycle,error)
        if (error) return
        alma_cycle=acycle
        call alma_message(alma_cycle)
        call exec_string ('SIC  PDBI\ OFF',error)
        call exec_string ('SIC  PICO\ OFF',error)
        call exec_string ('SIC  NOEMAOFFLINE\ OFF',error)
        call exec_string ('SIC  NOEMAONLINE\ OFF',error)
        call exec_string ('SIC  ALMA\ ON',error)
        call exec_string ('SIC  PRIO 1 ALMA',error)
      case('VELETA','PICOVELETA','30M')
        call parse_obs_pico(line,year,error)
        if (error) return
        obs_year=year
        call pico_message(obs_year)
        call exec_string ('SIC  PDBI\ OFF',error)
        call exec_string ('SIC  ALMA\ OFF',error)
        call exec_string ('SIC  NOEMAOFFLINE\ OFF',error)
        call exec_string ('SIC  NOEMAONLINE\ OFF',error)
        call exec_string ('SIC  PICO\ ON',error)
        call exec_string ('SIC  PRIO 1 PICO',error)
        obsname='VELETA'
        if (previous.ne.'PICOVELETA'.and.previous.ne.'VELETA') then
          call pico_reset_emir(emir,error)
        endif
      end select
    endif
    if (previous.ne.obsname) then
      ! null global variables about source
      call astro_message(seve%d,rname,'Reset source information')
      call nullify_astro_source(error)
      if (error) return
      soukind=soukind_none
      ! Recompute the matrices filled by TIME
      call astro_message(seve%d,rname,'Redo Time')
      call do_astro_time(jnow_utc,d_ut1,d_tdt,error)
      if (error) return
    else
       call astro_message(seve%d,rname,'Projection matrix already ok')
    endif
  else
    !
    ! Do not recognize the observatory name == decode coordinates
    call sic_ch(line,0,1,clon,nc,.true.,error)
    if (error)  return
    call sic_ch(line,0,2,clat,nc,.true.,error)
    if (error)  return
    call sic_r8(line,0,3,alti,.true.,error)
    if (error) return
    slim = 30.d0
    call sic_r8(line,0,4,slim,.false.,error)
    if (error) return
    call astro_observatory(clon,clat,alti,slim,error)
    if (error)  return
    ! Recompute the matrices filled by TIME
    call do_astro_time(jnow_utc,d_ut1,d_tdt,error)
    if (error) return
  endif
  !
contains
  !
  subroutine parse_obs_noema(line,yr,cmode,error)
    !---------------------------------------------------------------------
    ! Decode the OBSERVATORY command line in the NOEMA case
    !---------------------------------------------------------------------
    character(len=*), intent(in) :: line
    integer(kind=4), intent(inout) :: yr
    character(len=*), intent(inout) :: cmode ! command mode (ON or OFFline)
    logical, intent(inout) :: error
    ! Local
    character(len=128) :: ch
    character(len=12) :: argum
    integer(kind=4) :: nc,nv,nm,iarg
    integer(kind=4), parameter :: mnoema=1
    integer(kind=4), parameter :: mmodes=2 
    character(len=12) :: version
    character(len=12) :: noemaversions(mnoema)
    integer(kind=4) :: inoemaversions(mnoema)
    character(len=12) :: noemamodes(mmodes)
    logical :: foundversion,foundmode,foundany
    ! Data
    data noemaversions / '2017'/
    data inoemaversions / 2017 /
    data noemamodes / 'OFFLINE','ONLINE' /
    !
    ! Find NOEMA version and mode
    yr=2017
    cmode='OFFLINE'
    if (sic_narg(0).gt.3) then
      call astro_message(seve%e,rname,'Too many arguments. Aborted')
      error=.true.
      return
    endif
!       if (sic_narg(0).gt.1) then !! USELESS
    foundversion=.false.
    foundmode=.false.
    do iarg=2,sic_narg(0)
      foundany=.false.
      argum='NULL'
      call sic_ke(line,0,iarg,argum,nc,.true.,error)
      if (error) return
      ! Look if arg is a version
      call sic_ambigs_sub(rname,argum,version,nv,noemaversions,mnoema,error)
      if (error) return
      if (nv.gt.0) then
        foundany=.true.
        if (foundversion)  then
          call astro_message(seve%e,rname,'NOEMA VERSION already found !')
          error=.true.
          return
        else
          foundversion=.true.
!               if (nv.eq.mnoema) nv=mnoema-1 ! not needed before polyfix
          yr=inoemaversions(nv)
        endif
      endif
      ! Look if arg is a mode
      call sic_ambigs_sub(rname,argum,cmode,nm,noemamodes,mmodes,error)
      if (error) return
      if (nm.gt.0) then
        foundany=.true.
        if (foundmode) then
          call astro_message(seve%e,rname,'NOEMA MODE already found !')
          error=.true.
          return
        else
          foundmode=.true.
        endif
      endif
      if (.not.foundany) then
        write (ch,'(a,1x,a,1x,a)') 'Keyword',argum,'not understood'
        call astro_message(seve%e,rname,ch)
        call sic_ambigs_list(rname,seve%e,'Choices for NOEMA are:',noemaversions)
        call astro_message(seve%e,rname,'You might be looking for PdBI Versions ? Try:')
        call astro_message(seve%e,rname,'OBSERVATORY PDBI Version')
        error=.true.
        return
      endif
    enddo
  end subroutine parse_obs_noema
  !
  subroutine noema_message(yr)
    !---------------------------------------------------------------------
    ! Describes the receivers/correlator capabilities for selected period
    !---------------------------------------------------------------------
    integer(kind=4), intent(in):: yr
    ! Local
    character(len=128) :: ch
    !
    write(ch,'(A,I4)') 'NOEMA status is set to year ',yr
    call astro_message(seve%i,rname,ch)
    !
    if (yr.eq.2017) then
      call astro_message(seve%r,rname,'   >> NOEMA receivers with Polyfix backend')
    endif
  end subroutine noema_message
  !
  subroutine parse_obs_pdbi(line,yr,error)
    !---------------------------------------------------------------------
    ! Decode the OBSERVATORY command line in the BURE/PDBI case
    !---------------------------------------------------------------------
    character(len=*), intent(in) :: line
    integer(kind=4), intent(inout) :: yr
    logical, intent(inout) :: error
    ! Local
    character(len=128) :: ch
    character(len=12) :: argum
    integer(kind=4) :: nc,nv
    character(len=12) :: version
    integer(kind=4), parameter :: mpdbi=7
    character(len=12) :: pdbiversions(mpdbi)
    integer(kind=4) :: ipdbiversions(mpdbi-1)
    ! Data
    data pdbiversions / '1995','2000','2006','2010','2013','2015','NOW'/
    data ipdbiversions / 1995 , 2000 , 2006 , 2010 , 2013 , 2015 /
    !
    !PDBI versions (year)
    if (sic_narg(0).gt.2) then
      call astro_message(seve%e,rname,'Too many arguments for this observatory')
      error=.true.
      return
    endif
    argum='2015'
    call sic_ke(line,0,2,argum,nc,.false.,error)
    if (error) return
    call sic_ambigs_sub(rname,argum,version,nv,pdbiversions,mpdbi,error)
    if (error) return
    if (nv.gt.0) then
      if (nv.eq.mpdbi) nv=mpdbi-1
      year=ipdbiversions(nv)
    else
      write (ch,'(a,1x,a,1x,a)') 'Keyword',argum,'not understood'
      call astro_message(seve%e,rname,ch)
      call sic_ambigs_list(rname,seve%e,'Choices for PDBI are:',pdbiversions)
      call astro_message(seve%e,rname,'You might be looking for NOEMA Versions. Try:')
      call astro_message(seve%e,rname,'OBSERVATORY NOEMA Version')
      error = .true.
      return
    endif
  end subroutine parse_obs_pdbi
  !
  subroutine pdbi_message(yr)
    !---------------------------------------------------------------------
    ! Describes the receivers/correlator capabilities for selected period
    !---------------------------------------------------------------------
    integer(kind=4), intent(in):: yr  !
    ! Local
    character(len=128) :: ch
    !
    write(ch,'(A,I4)') 'PdBI status is set to year ',yr
    call astro_message(seve%i,rname,ch)
    !
    if (yr.eq.1995) then
      call astro_message(seve%r,rname,'   >> Old generation receivers')
      call astro_message(seve%r,rname,'   >> Usable IF = 100-600 MHz')
    elseif (yr.eq.2000) then
      call astro_message(seve%r,rname,'   >> Old generation receivers')
      call astro_message(seve%r,rname,'   >> Usable IF = 100-680 MHz')
    elseif (yr.eq.2006) then
      call astro_message(seve%r,rname,'   >> New generation receivers (2006-2017)')
    elseif (yr.eq.2010) then
      call astro_message(seve%r,rname,'   >> New generation receivers (2006-2017)')
      call astro_message(seve%r,rname,'   >> New LO system (2010)')
    elseif (yr.eq.2013) then
      call astro_message(seve%r,rname,'   >> New generation receivers (2006-2017)')
      call astro_message(seve%r,rname,'   >> New LO system (2010) with 2013 update')
    elseif (yr.eq.2015) then
      call astro_message(seve%r,rname,'   >> New generation receivers (2006-2017)')
      call astro_message(seve%r,rname,'   >> New LO system (2010) with 2013 update')
      call astro_message(seve%r,rname,'   >> Tuning optimized on a grid of LO frequencies')
    endif
  end subroutine pdbi_message
  !
  subroutine parse_obs_pico(line,yr,error)
    !---------------------------------------------------------------------
    ! Decode the OBSERVATORY command line in the PICO case
    !---------------------------------------------------------------------
    character(len=*), intent(in) :: line
    integer(kind=4), intent(inout) :: yr
    logical, intent(inout) :: error
    ! Local
    character(len=128) :: ch
    character(len=12) :: argum
    integer(kind=4) :: nc,nv
    character(len=12) :: version
    integer(kind=4), parameter :: mpico=2
    character(len=12) :: picoversions(mpico)
    integer(kind=4) :: ipicoversions(mpico)
    ! Data
    data picoversions / '2016','2021' /
    data ipicoversions / 2016 , 2021  /
    !
    !PDBI versions (year)
    if (sic_narg(0).gt.2) then
      call astro_message(seve%e,rname,'Too many arguments for this observatory')
      error=.true.
      return
    endif
    argum='2021'
    call sic_ke(line,0,2,argum,nc,.false.,error)
    if (error) return
    call sic_ambigs_sub(rname,argum,version,nv,picoversions,mpico,error)
    if (error) return
    if (nv.gt.0) then
      year=ipicoversions(nv)
    else
      write (ch,'(a,1x,a,1x,a)') 'Keyword',argum,'not understood'
      call astro_message(seve%e,rname,ch)
      call sic_ambigs_list(rname,seve%e,'Choices for PICO are:',picoversions)
      error = .true.
      return
    endif
  end subroutine parse_obs_pico
  !
  subroutine pico_message(yr)
    !---------------------------------------------------------------------
    ! Describes the capabilities for selected period
    !---------------------------------------------------------------------
    integer(kind=4), intent(in):: yr  !
    ! Local
    character(len=128) :: ch
    !
    write(ch,'(A,I4)') 'PICO status is set to year ',yr
    call astro_message(seve%i,rname,ch)
    !
    if (yr.eq.2016) then
      call astro_message(seve%r,rname,'   >> Original Outer baseband frequency range (up to 2021)')
      call astro_message(seve%r,rname,'   >> Folding frequency = 15.68 GHz')
    elseif (yr.eq.2021) then
      call astro_message(seve%r,rname,'   >> Current Outer baseband frequency range (as of 2021)')
      call astro_message(seve%r,rname,'   >> Folding frequency = 16.00 GHz')
    endif
  end subroutine pico_message
  !
  subroutine parse_obs_alma(line,ac,error)
    !---------------------------------------------------------------------
    ! Decode the OBSERVATORY command line in the ALMA case
    !---------------------------------------------------------------------
    character(len=*), intent(in) :: line
    integer(kind=4), intent(inout) :: ac ! ALMA cycle
    logical, intent(inout) :: error
    ! Local
    character(len=128) :: ch
    character(len=12) :: argum
    integer(kind=4) :: nv
    integer(kind=4), parameter :: malma=1
    character(len=12) :: version
    character(len=12) :: almaversions(malma)
    logical :: foundversion
    ! Data
    data almaversions / 'FULL' /
    !
    foundversion=.false.
    if (sic_narg(0).gt.2) then
      call astro_message(seve%e,rname,'Too many arguments for this observatory')
      error=.true.
      return
    endif
    argum='FULL'
    call sic_ke(line,0,2,argum,nc,.false.,error)
    if (error) return
    call sic_ambigs_sub(rname,argum,version,nv,almaversions,malma,error)
    if (error) return
    if (nv.gt.0) then
      if (version.eq.'FULL') then
        foundversion=.true.
        ac=666
        call astro_message(seve%i,rname,'Using Full ALMA capabilities')
      else
        call astro_message(seve%e,rname,'Problem with Cycle')
        error=.true.
        return
      endif
    else
      call sic_i4(line,0,2,ac,.true.,error)
      if (error) return
      if (ac.lt.0) then
        call astro_message(seve%e,rname,'ALMA Cycles start with 0')
        error = .true.
        return
      endif
      foundversion=.true.
      write (ch,'(a,1x,i0)') 'Found ALMA cycle',ac
      call astro_message(seve%i,rname,ch)
    endif
    if (.not.(foundversion)) then
      write (ch,'(a,1x,a,1x,a)') 'Keyword',argum,'not understood'
      call astro_message(seve%e,rname,ch)
      error=.true.
      return
    endif
    !
  end subroutine parse_obs_alma
  !
  subroutine alma_message(acycle)
    !---------------------------------------------------------------------
    ! Displays the selected ALMA cycle
    !---------------------------------------------------------------------
    integer(kind=4), intent(in):: acycle  !
    ! Local
    character(len=128) :: ch
    !
    if (acycle.eq.666) then
      write(ch,'(A)') 'Using FULL ALMA capabilities'
    else
      write(ch,'(A,i2,A)') 'Using ALMA Cycle ',acycle,' capabilities'
    endif
    call astro_message(seve%i,rname,ch)
  end subroutine alma_message
  !
end subroutine astro_observatory_command
!
subroutine astro_observatory_byname(arg,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>astro_observatory_byname
  use ast_astro
  !---------------------------------------------------------------------
  ! @ public-generic astro_observatory
  !  Set the Astro observatory given its name
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: arg    ! Telescope name
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='OBSERVATORY'
  character(len=4) :: teles  ! Comparison is based on 4 characters
                             ! Hide this "detail" here
  real(4) :: diam
  !
  teles = arg  ! arg can be smaller or larger than 4 characters!
  freq = 100.d0
  !
  call gwcs_observatory(arg,lonlat,altitude,slimit,diam,error)
  select case (teles)
  case ('PARA','VLT ','VLTI')
    call astro_message(seve%i,rname,  &
      &  'Frequency set to 299.792458 THz = 1 micron')
    freq = 299792.458d0
  case ('IOTA')
    call astro_message(seve%i,rname,  &
      &  'Frequency set to 187.370286 THz = 1.6 micron')
    freq = 187370.286d0
  case ('PTI ')
    call astro_message(seve%i,rname,'Frequency set to 136.269299 THz = '//  &
       & '2.2 micron')
    freq = 136269.299d0
  case ('GI2T')
    call astro_message(seve%i,rname,'Frequency set to 500 THz = '//  &
       & '0.6 micron')
    freq = 499653.8397d0
  end select
end subroutine astro_observatory_byname
!
subroutine astro_observatory_bychcoords(clon,clat,alt,slim,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>astro_observatory_bychcoords
  !---------------------------------------------------------------------
  ! @ public-generic astro_observatory
  !  Set Astro observatory given its longitude/latitude (sexagesimal
  ! strings), altitude and avoidance
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: clon   ! Telescope longitude (sexagesimal string)
  character(len=*), intent(in)    :: clat   ! Telescope latitude (sexagesimal string)
  real(kind=8),     intent(in)    :: alt    ! Altitude [km]
  real(kind=8),     intent(in)    :: slim   ! Sun avoidance
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  integer(kind=4) :: llon,llat
  real(kind=8) :: lon,lat
  !
  llon = lenc(clon)
  call sic_sexa(clon(1:llon),llon,lon,error)
  if (error) return
  !
  llat = lenc(clat)
  call sic_sexa(clat(1:llat),llat,lat,error)
  if (error) return
  !
  call astro_observatory_byr8coords(lon,lat,alt,slim,error)
  if (error)  return
  !
end subroutine astro_observatory_bychcoords
!
subroutine astro_observatory_byr8coords(lon,lat,alt,slim,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>astro_observatory_byr8coords
  use ast_astro
  !---------------------------------------------------------------------
  ! @ public-generic astro_observatory
  !  Set Astro observatory given its longitude/latitude (float values),
  ! altitude and avoidance
  !---------------------------------------------------------------------
  real(kind=8), intent(in)    :: lon    ! Telescope longitude [deg]
  real(kind=8), intent(in)    :: lat    ! Telescope latitude [deg]
  real(kind=8), intent(in)    :: alt    ! Altitude [km]
  real(kind=8), intent(in)    :: slim   ! Sun avoidance
  logical,      intent(inout) :: error  ! Logical error flag
  !
  real(8) :: xyz(3)
  character(len=message_length) :: mess
  !
  if (abs(lat).gt.90.d0) then
    call astro_message(seve%e,'OBSERVATORY','Latitude must be < 90 degrees')
    error = .true.
    return
  endif
  !
  lonlat(1) = lon
  lonlat(2) = lat
  altitude = alt
  slimit = slim
  obsname = "User defined"
  !
  call gwcs_lonlat2geo(lon,lat,alt,xyz)
  !
  write(mess,'(A,3(1X,F0.8))') 'XYZ',xyz
  call astro_message(seve%i,'OBSERVATORY',mess)
end subroutine astro_observatory_byr8coords
!
subroutine do_tele_beam(primbeam,beam,freq,arg)
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  ! Default beam size according to Telescope
  ! Specific values for PICO and EFFELSBERG, otherwise just scale the
  ! BURE value.
  !---------------------------------------------------------------------
  real(kind=8),     intent(out) :: primbeam  ! Computed value
  real(kind=8),     intent(in)  :: beam      ! If not 0, return this value
  real(kind=8),     intent(in)  :: freq      ! Else, use freq+telescope
  character(len=*), intent(in)  :: arg       ! Telescope name
  !
  if (beam.eq.0.d0) then
    select case (arg)
    case ('BURE','PDBI','NOEM')
      primbeam = 56d0*(88d0/freq)
    case ('PICO','VELE')
      primbeam = 2398d0/freq   ! Value from C.Kramer
    case ('ALMA')
      primbeam = 15d0/12d0*56d0*(88d0/freq)
    case ('ACA')
      primbeam = 15d0/7d0*56d0*(88d0/freq)
    case ('ATF')
      primbeam = 15d0/12d0*56d0*(88d0/freq)
    case ('CARM')
      ! Consider here the 10.4m antennas
      primbeam = 15d0/10.4d0*56d0*(88d0/freq)
    case ('CSO ')
      primbeam = 15d0/10.4d0*56d0*(88d0/freq)
    case ('EFFE')
      primbeam = 43d0*(22d0/freq)
    case ('FCRA')
      primbeam = 15d0/14d0*56d0*(88d0/freq)
    case ('JCMT')
      primbeam = 56d0*(88d0/freq)
    case ('KITT')
      primbeam = 15d0/12d0*56d0*(88d0/freq)
    case ('SEST')
      primbeam = 56d0*(88d0/freq)
    case ('SMA ')
      primbeam = 15d0/6d0*56d0*(88d0/freq)
    case ('SMT ')
      primbeam = 15d0/10d0*56d0*(88d0/freq)
    case ('OVRO')    ! OBS NOT DEFINED !
      primbeam = 15d0/10.4d0*56d0*(88d0/freq)
    case ('45-M')    ! OBS NOT DEFINED !
      primbeam = 15d0/45d0*56d0*(88d0/freq)
    case ('NRO ')    ! OBS NOT DEFINED !
      primbeam = 15d0/10d0*56d0*(88d0/freq)
    case ('BIMA')    ! OBS NOT DEFINED !
      primbeam = 15d0/6.1d0*56d0*(88d0/freq)
    case default
      call astro_message(seve%w,'ASTRO','No default primary beam '//  &
      'available for observatory '//arg)
      primbeam = 0.d0
    end select
  else
    primbeam = beam
  endif
end subroutine do_tele_beam
