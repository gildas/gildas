!-----------------------------------------------------------------------
! The following API provides a way to parse substring in a string. This
! assumes the string is a collection a concatenated substrings, and each
! substring can only be found in a limited list of choices.
! For example:
!    XXYYYZ
! is a collection of 3 substrings:
!    XX  (2 chars)
!    YYY (3 chars)
!    Z   (1 char)
! With the API, the programmer declares the list of possibilities for
! each of them. The parser will find which values were used, and will
! raise an error if something goes wrong.
! Remarks:
!  - order matters here, i.e. YYYXXZ is not supported
!  - 2 modes are supported: 1) all substrings must be present, or 2)
!    some substrings may be absent (e.g. XXZ)
!-----------------------------------------------------------------------
subroutine string_parser_addlist(sp,position,list,error)
  use gbl_message
  use string_parser_types
  !---------------------------------------------------------------------
  ! @ private
  !  Add a list of possible values for a given substring
  !---------------------------------------------------------------------
  type(string_parser_t), intent(inout) :: sp
  integer(kind=4),       intent(in)    :: position  ! Expected position in the string
  character(len=*),      intent(in)    :: list(:)   ! All possible values for this substring
  logical,               intent(inout) :: error     !
  ! Local
  character(len=*), parameter :: rname='STRING>PARSER>ADDLIST'
  integer(kind=4) :: i
  character(len=message_length) :: mess
  !
  ! Sanity checks
  if (position.gt.string_parser_msub) then
    ! Enlarge string_parser_msub and recompile
    write(mess,'(A,I0,A)')  &
      'String parser supports at most ',string_parser_msub,' substrings'
    call astro_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  if (size(list).gt.substring_parser_mval) then
    ! Enlarge substring_parser_mval and recompile
    write(mess,'(A,I0,A)')  &
      'Substring list can not have more than ',substring_parser_mval,' values'
    call astro_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  if (len(list(1)).gt.substring_parser_mchar) then
    ! Enlarge substring_parser_mchar and recompile
    write(mess,'(A,I0,A)')  'Substring list values values can not have more than ',  &
      substring_parser_mchar,' characters'
    call astro_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  if (sp%active(position)) then
    call astro_message(seve%e,rname,'Position-th element already declared')
    error = .true.
    return
  endif
  !
  sp%active(position) = .true.
  sp%substring(position)%nval = size(list)
  do i=1,size(list)
    sp%substring(position)%val(i) = list(i)
  enddo
  !
end subroutine string_parser_addlist
!
subroutine string_parser_print(sp,error)
  use gbl_message
  use astro_interfaces, except_this=>string_parser_print
  use string_parser_types
  !---------------------------------------------------------------------
  ! @ private
  !  Print the contents of the string_parser_t
  !---------------------------------------------------------------------
  type(string_parser_t), intent(in)    :: sp
  logical,               intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='STRING>PARSER>PRINT'
  integer(kind=4) :: i
  character(len=message_length) :: mess
  !
  do i=1,string_parser_msub
    if (.not.sp%active(i))  cycle
    write(mess,'(A,I0,A,I0,A,A)')  &
      'Element #',i,' (',sp%substring(i)%nval,' values): ',trim(string_parser_print_onelist(sp,i))
    call astro_message(seve%r,rname,mess)
  enddo
  !
end subroutine string_parser_print
!
function string_parser_print_onelist(sp,isub)
  use string_parser_types
  !---------------------------------------------------------------------
  ! @ private
  ! Return the list of possibilities for a given substring in compact
  ! way
  !---------------------------------------------------------------------
  character(len=128) :: string_parser_print_onelist
  type(string_parser_t), intent(in) :: sp
  integer(kind=4),       intent(in) :: isub
  ! Local
  integer(kind=4) :: ic,ival
  !
  if (isub.gt.string_parser_msub) then
    string_parser_print_onelist = '(internal error)'
    return
  endif
  !
  string_parser_print_onelist = '('
  ic = 1
  do ival=1,sp%substring(isub)%nval
    ic = ic + 1
    string_parser_print_onelist(ic:) = sp%substring(isub)%val(ival)
    ic = ic + len_trim(sp%substring(isub)%val(ival))
    string_parser_print_onelist(ic:ic) = ','
  enddo
  string_parser_print_onelist(ic:ic) = ')'
  !
end function string_parser_print_onelist
!
subroutine string_parser_parse(caller,sp,string,allpresent,codes,error)
  use gbl_message
  use string_parser_types
  use astro_interfaces, except_this=>string_parser_parse
  !---------------------------------------------------------------------
  ! @ private
  ! Parse the input string for the substrings described in the
  ! string_parser_t structure. In return the codes(i) is filled with:
  !  - 0 if the i-th substring was not found in the string (if allowed)
  !  - j>0 if the j-th value was found for the i-th substring.
  !---------------------------------------------------------------------
  character(len=*),      intent(in)    :: caller      ! Calling routine name
  type(string_parser_t), intent(in)    :: sp          !
  character(len=*),      intent(in)    :: string      ! Input string to parse
  logical,               intent(in)    :: allpresent  ! All substrings must be present?
  integer(kind=4),       intent(out)   :: codes(:)    ! Returned codes
  logical,               intent(inout) :: error       !
  ! Local
  integer(kind=4) :: ic,ielem,ival
  character(len=message_length) :: mess
  !
  ! Sanity check
  if (size(codes).lt.string_parser_msub) then
    call astro_message(seve%e,caller,'Can not parse string to codes')
    error = .true.
    return
  endif
  !
  ic = 1
elem: do ielem=1,string_parser_msub
    if (sp%active(ielem)) then
      do ival=1,sp%substring(ielem)%nval
        if (index(string(ic:),trim(sp%substring(ielem)%val(ival))).eq.1) then
          codes(ielem) = ival
          ic = ic+len_trim(sp%substring(ielem)%val(ival))
          cycle elem
        endif
      enddo
    endif
    !
    codes(ielem) = 0
    if (allpresent) then
      call astro_message(seve%e,caller,'Missing one of '//string_parser_print_onelist(sp,ielem))
      error = .true.
      return
    endif
  enddo elem
  !
  if (string(ic:).ne.'') then
    call astro_message(seve%e,caller,'Argument not understood near '//string(ic:))
    ic = 1
    do ielem=1,string_parser_msub
      if (sp%active(ielem)) then
        write(mess(ic:),'(A)')  string_parser_print_onelist(sp,ielem)
        ic = len_trim(mess)+2
      endif
    enddo
    call astro_message(seve%e,caller,'Argument must be a combination of '//mess)
    error = .true.
    return
  endif
  !
end subroutine string_parser_parse
!
subroutine string_parser_build(sp,codes,string,error)
  use gbl_message
  use string_parser_types
  use astro_interfaces, except_this=>string_parser_build
  !---------------------------------------------------------------------
  ! @ private
  ! Build the character string from the codes list (reverse operation
  ! from string_parser_parse)
  !---------------------------------------------------------------------
  type(string_parser_t), intent(in)    :: sp
  integer(kind=4),       intent(in)    :: codes(:)
  character(len=*),      intent(out)   :: string
  logical,               intent(inout) :: error
  ! Local
  integer(kind=4) :: ic,ielem
  !
  ic = 1
  do ielem=1,string_parser_msub
    if (.not.sp%active(ielem))  cycle
    if (codes(ielem).eq.0) then
      string(ic:ic) = '*'
    else
      string(ic:) = sp%substring(ielem)%val(codes(ielem))
    endif
    ic = len_trim(string)+1
  enddo
  !
end subroutine string_parser_build
