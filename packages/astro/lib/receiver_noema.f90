subroutine rec_define_noema(rdesc,mode,error)
  use gbl_message
  use astro_interfaces, except_this=>rec_define_noema
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! fill in the receiver type according to input name
  !-----------------------------------------------------------------------
  type(receiver_desc_t), intent(inout) :: rdesc ! receiver desc parameters
  character(len=*), intent(in) :: mode ! OFFLINE or ONLINE
  logical, intent(inout)        :: error
  ! local
  integer(kind=4)       :: i
  !
  rdesc%name = 'NOEMA'
  rdesc%defined = .true.
  if (mode.eq.'OFFLINE') then
    rdesc%n_rbands = 3
  else if (mode.eq.'ONLINE') then
    rdesc%n_rbands = 4
  endif
  rdesc%bandname(1) = 'Band_1'
  rdesc%bandname(2) = 'Band_2'
  rdesc%bandname(3) = 'Band_3'
  rdesc%bandname(4) = 'Band_4'
  rdesc%rejection(1) = 30
  rdesc%rejection(2) = 30
  rdesc%rejection(3) = 30
  rdesc%rejection(4) = 30
  rdesc%n_sbands = 2
  rdesc%n_bbands = 2
  rdesc%n_polar = 2
  rdesc%n_backends = 1
  rdesc%iflim(1) = 3872d0
  rdesc%iflim(2) = 11616d0
  rdesc%ifband = rdesc%iflim(2)-rdesc%iflim(1)
  !basebands
  rdesc%flo2 = 7744d0
  ! LO2 separation zone: (from commissioning output)
  ! Mark only the strong parasite
  !Attenuation 1.5chunk in INNER and OUTER defined in IF1 - not yet plotted
  rdesc%if1conf(1) = rdesc%flo2-10d0
  rdesc%if1conf(2) = rdesc%flo2+10d0
  rdesc%bbname(1) = 'OUTER'
  rdesc%bblim(1,1) = 7744d0
  rdesc%bblim(2,1) = 11616d0
  rdesc%bbref(1) = 10000d0
  rdesc%bbname(2) = 'INNER'
  rdesc%bblim(1,2) = 3872d0
  rdesc%bblim(2,2) = 7744d0
  rdesc%bbref(2) = 6000d0
  ! Polars
  rdesc%polname(1) = 'HORIZONTAL'
  rdesc%polname(2) = 'VERTICAL'
  !
  ! Limits as stated in the tuning tables  (Jan2017)
  rdesc%lohard(1,1) = 82.0d3
  rdesc%lohard(2,1) = 110d3
  rdesc%lohard(1,2) = 136d3
  rdesc%lohard(2,2) = 172d3
  rdesc%lohard(1,3) = 207.744d3
  rdesc%lohard(2,3) = 268d3
  rdesc%lohard(1,4) = 280d3
  rdesc%lohard(2,4) = 370d3
  !
  ! Limits from ACC = Specs from SVN noema (may2018)
  rdesc%locall(1,1) = 82.0d3 ! to match the specs 20dec2017 (was 82.5)
  rdesc%locall(2,1) = 107.700d3 ! update W20 (from MK)
  rdesc%locall(1,2) = 138.616d3
  rdesc%locall(2,2) = 171.256d3
  rdesc%locall(1,3) = 207.744d3
  rdesc%locall(2,3) = 264.384d3
  rdesc%locall(1,4) = 280d3
  rdesc%locall(2,4) = 370d3
  !
  rdesc%lotol = 100d0
  !
  ! Wider limits to let some margin to explore the edge of the bands
  do i=1,rdesc%n_rbands
      rdesc%lolim(1,i) = min(rdesc%locall(1,i),rdesc%lohard(1,i))
      rdesc%lolim(2,i) = max(rdesc%locall(2,i),rdesc%lohard(2,i))
  enddo
  !
  ! RF limits from LOLIM +- IFmax
  do i=1,rdesc%n_rbands
      rdesc%rflim(1,i) = rdesc%lolim(1,i)-rdesc%iflim(2)
      rdesc%rflim(2,i) = rdesc%lolim(2,i)+rdesc%iflim(2)
  enddo
  ! RF limits from LOCALL +- IFmax
  do i=1,rdesc%n_rbands
      rdesc%rfcall(1,i) = rdesc%locall(1,i)-rdesc%iflim(2)
      rdesc%rfcall(2,i) = rdesc%locall(2,i)+rdesc%iflim(2)
  enddo
  !
  rdesc%tuninggrid = .true.
  rdesc%gridbin = 500d0 ! MHz
  rdesc%gridtol = 1d0     ! MHz
  !
end subroutine rec_define_noema
!
subroutine rec_define_noema_ifproc(noemaif,error)
  use gbl_message
  use astro_interfaces, except_this=>rec_define_noema_ifproc
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! if proc characteristics
  !-----------------------------------------------------------------------
  type(correlator_input_t), intent(inout) :: noemaif !
  logical, intent(inout)        :: error
  ! local
  integer(kind=4) :: i
  ! Ifproc
  noemaif%n_ifcables = 8
  noemaif%m_usercables = 0
  noemaif%mode = ''
  noemaif%defined = .false.
  do i=1,m_ifcables
    noemaif%ifc(i)%label = ''
    noemaif%ifc(i)%iband = 0
    noemaif%ifc(i)%sb_code = 0
    noemaif%ifc(i)%bb_code = 0
    noemaif%ifc(i)%pol_code = 0
  enddo
  !
end subroutine rec_define_noema_ifproc
!
subroutine noema_reset_setup(tunecomm,noema_if,error)
  use astro_interfaces, except_this=>noema_reset_setup
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! reset noema specific parameters
  !-----------------------------------------------------------------------
  type(noema_tuning_comm_t), intent(inout) :: tunecomm
  type(noema_if_t), intent(inout) :: noema_if
  logical, intent(inout)        :: error
  ! Local
  integer(kind=4) :: i
  !
  !Command parameters
  tunecomm%dotune=.false.
  tunecomm%frest = 0
  tunecomm%fcent = 0
  tunecomm%zoommode = ''
  tunecomm%fz1 = 0
  tunecomm%fz2 = 0
  tunecomm%fixed_scale = .false. 
  !
  !IF PROCESSOR
  noema_if%ifproc%mode = ''
  noema_if%ifproc%defined = .false.
  do i=1,m_ifcables
    noema_if%ifproc%ifc(i)%label = ''
    noema_if%ifproc%ifc(i)%iband = 0
    noema_if%ifproc%ifc(i)%sb_code = 0
    noema_if%ifproc%ifc(i)%bb_code = 0
    noema_if%ifproc%ifc(i)%pol_code = 0
  enddo
  !
  !Baseband selection
  noema_if%selunit%n_ifsel=-1
  noema_if%selunit%polmode='NULL'
  do i=1,m_backunits
    noema_if%selunit%usel(i)=-1
  enddo
  !
end subroutine noema_reset_setup
!
subroutine rec_noema(line,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_noema
  use ast_astro
  use my_receiver_globals
  use plot_molecules_globals
  use frequency_axis_globals
  !-----------------------------------------------------------------------
  ! @ private
  ! Command to define and visualize NOEMA receiver coverage
  !-----------------------------------------------------------------------
  character(len=*), intent(inout) :: line        ! command line
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='TUNING'
  integer(kind=4),parameter :: optzoom=1 ! option /ZOOM
  integer(kind=4),parameter :: optpagewidth=2 ! option /PAGEWIDTH
  integer(kind=4),parameter :: optfixfreq=3 ! option /FIXED_FREQ
  integer(kind=4),parameter :: optinfo=4 ! option /INFO
  integer(kind=4) :: lpar,izoom
  integer(kind=4), parameter :: m_zmodes=2
  integer(kind=4), parameter :: marg=3
  real(kind=8) :: fcomm
  character(len=20) ::  zmodes(m_zmodes),zmode
  logical :: noarg,doinfo,doauto
  integer(kind=4) :: it
  data zmodes/'ID','BOTH'/
  !
  error = .false.
  !
  if (obsname.ne.'NOEMA') then
    call astro_message(seve%e,rname,'Inconsistency between Receiver Name and Observatory')
    error = .true.
    return
  endif
  !
  !Define receiver parameters
  call astro_def_receiver(rname,'NOEMA',noema%recdesc,error)
  if (error) return
  !Define ifproc parameters
  call rec_define_noema_ifproc(noema%cfebe%i_f%ifproc,error)
  if (error) return
  ! Put in memory the current ASTRO%SOURCE parameters
  noema%recdesc%redshift = .true. ! tuning takes into accound source redshift
  call astro_def_recsource(rname,noema%recdesc,noema%source,error)
  if (error) return
  !
  !Print Receiver info in the terminal
  doinfo=sic_present(optinfo,0)
  !
  !Decoding Line Command
  ! Tuning parameters
  zmode='ID'
  izoom=0
  it=0
  noema%cfebe%tune%dotune=.false.
  doauto=.false.
  if (sic_narg(0).gt.marg) then
    call astro_message(seve%e,rname,'Too many arguments')
    error=.true.
    return
  else  if (sic_narg(0).eq.0) then
    noarg=.true.
    call astro_message(seve%i,rname,'Showing the coverage of NOEMA receiver bands')
  else
    noarg=.false.
    call astro_message(seve%i,rname,'Resetting tuning')
    call noema_reset_setup(noema%cfebe%tune,noema%cfebe%i_f,error)
    if (error) return
    it=1 ! to be removed with reorgnization
    noema%cfebe%tune%dotune=.true.
    noema%cfebe%tune%name=''
    ! Get the user value
    call sic_r8(line,0,1,fcomm,.true.,error)
    if (error) return
    ! Convert input freq to rest
    call rec_inputtorest(rname,fcomm,freq_axis%main,noema%source,noema%cfebe%tune%frest,error)
    if (error) return
    if (sic_narg(0).eq.marg) then
      ! full command
      call sic_ke (line,0,2,noema%cfebe%tune%sideband,lpar,.true.,error)
      if (error) return
      call sic_r8(line,0,3,noema%cfebe%tune%fcent,.true.,error)
      if (error) return
    else  if (sic_narg(0).eq.2) then
      call astro_message(seve%e,rname,'Mode not implemented. Command accepts 0,1 or 3 arguments')
      error=.true.
      return
    else 
      ! use default tuning
      doauto=.true.
    endif
  endif
  ! Options
  ! Zoom parameter
  if (sic_present(optzoom,0)) then
    if (sic_narg(optzoom).eq.0) then
      ! No arg = zoom on tuned band
      if (.not.noema%cfebe%tune%dotune) then
        call astro_message(seve%e,rname,'Try to zoom on a not tune band')
        error = .true.
        return
      endif
      zmode='SINGLE'
      izoom=1
    else if (sic_narg(optzoom).eq.2) then
      ! Freq limits explicitely given
      zmode='FREQ'
      call sic_r8(line,optzoom,1,fcomm,.true.,error)
      if (error) return
      fcomm=fcomm*mhzperghz
      call rec_inputtorest(rname,fcomm,freq_axis%main,noema%source,noema%cfebe%tune%fz1,error)
      if (error) return
      call sic_r8(line,optzoom,2,fcomm,.true.,error)
      if (error) return
      fcomm=fcomm*mhzperghz
      call rec_inputtorest(rname,fcomm,freq_axis%main,noema%source,noema%cfebe%tune%fz2,error)
        if (error) return
    else
      call astro_message(seve%e,rname,'/ZOOM option accepts 0 or 2 arguments')
      error = .true.
      return
    endif  
  endif
  ! /pagewidth option
  if (sic_present(optpagewidth,0)) then
    noema%cfebe%tune%fixed_scale = .false.
  else
    noema%cfebe%tune%fixed_scale = .true.
  endif
  !
  ! /fixed_freq option
  if (sic_present(optfixfreq,0)) then
    noema%cfebe%tune%fixedfreq = .true.
  else
    noema%cfebe%tune%fixedfreq = .false.
  endif
  !
  if (doauto) then
    call noema_default_tuning(rname,noema%recdesc,noema%source%dopshift,noema%cfebe%tune,error)
    if (error) return
  endif
  !Prepare for the plot and tune  
  if (noema%cfebe%tune%dotune) then
    call noema_setup(rname,noema%cfebe%tune,noema%cfebe%reccomm,noema%recdesc,noema%source, &
                      noema%cfebe%rectune,error)
    if (error) return
    noema%cfebe%defined=.true.
  endif  
  !
  if (zmode.eq.'ID') then
    call rec_plot_mbands(noema%recdesc,noema%source, &
                        molecules,noema%cfebe%tune%fixed_scale,cplot,freq_axis,error)
    if (error) return
    if (noema%cfebe%defined) then
      call rec_plot_tuned(noema%recdesc,noema%source,noema%cfebe%rectune, &
                      molecules,cplot,freq_axis,error)
      if (error) return
    endif
  else if (zmode.eq.'SINGLE') then
    if (izoom.le.0.or.(.not.noema%cfebe%tune%dotune)) then
      call astro_message(seve%e,rname,'Tried to zoom on a not tuned band')
      error = .true.
      return
    endif
    call rec_plot_sidebands(noema%recdesc,noema%source,noema%cfebe%rectune, &
                            cplot,molecules,freq_axis,error)
    if (error) return
  else if (zmode.eq.'FREQ') then
    call rec_zoom(molecules,noema%cfebe%tune%fz1,noema%cfebe%tune%fz2, &
                      noema%recdesc,noema%source,cplot,freq_axis,error)
    if (error) return
    
    if (noema%cfebe%defined) then
      call rec_plot_tuned(noema%recdesc,noema%source,noema%cfebe%rectune, &
                      molecules,cplot,freq_axis,error)
      if (error) return
    endif
  endif
  !
  if (noarg) then
    ! if no tuning was entered, reset the rec%n_tuning to 0
    ! like this ifproc/backend command will not work (need at least 1 tuning)
    noema%cfebe%defined=.false.
  else
    ! define if ranges transported
    call noema_ifproc(rname,noema%recdesc,noema%cfebe%rectune,noema%cfebe%i_f%ifproc,error)
    if (error) return
    ! Update the plot
    call noema_ifproc_plot(rname,noema%cfebe%tune%fixed_scale,noema%cfebe%i_f,molecules, &
          noema%recdesc,noema%source,noema%cfebe%rectune,cplot,freq_axis,error)
    if (error) return
    ! define polyfix and assign if "cables" to correlator units [only input, no mode yet]
    call noema_define_pfx(noema%cfebe%pfx,error)
    if (error) return
    call noema_reset_backend(noema%cfebe%pfx,noema%cfebe%spw%out,error)
    noema%cfebe%spw%comm%itype=-1
    if (error) return
    call noema_assign_units(rname,noema%cfebe%i_f%ifproc,noema%cfebe%pfx,noema%recdesc,error)
    if (error) return
  endif
  !
  if (doinfo) then
    call astro_receiver_info(noema%recdesc,rname,error)
    if (error) return
  endif
  !
  ! Let the user with the limits in main axis frequency frame
  call rec_set_limits(rname,cplot,freq_axis%main,error)
  if (error) return
  call gtview('U')
  !
end subroutine rec_noema
!
subroutine noema_default_tuning(rname,rdesc,sdopshift,ncomm,error)
  use gbl_message
  use astro_interfaces, except_this=>noema_default_tuning
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! reset noema specific parameters
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(receiver_desc_t), intent(in) :: rdesc   ! all receiver info
  real(kind=8), intent(in)      :: sdopshift
  type(noema_tuning_comm_t), intent(inout) :: ncomm ! command-related stuff
  logical, intent(inout)        :: error
  ! Local
  integer(kind=4)       :: iband,sbcode
  real(kind=8)          :: frf,fumin,limit(2)
  real(kind=8), parameter :: ifmargin=600d0 !to avoid placing the requested freq too close to the band edge
  !
  !
  ! Find Band
  frf = ncomm%frest*sdopshift*mhzperghz
  call rec_find_band(rname,rdesc,frf,iband,error)
  if (error) return
  !Check whether the requested frequency is in the call for proposal limits 
  ! and chose the right LO range to consider
  if (frf.ge.rdesc%rfcall(1,iband)+ifmargin.and.frf.le.rdesc%rfcall(2,iband)-ifmargin) then
    limit(1) = rdesc%locall(1,iband)
    limit(2) = rdesc%locall(2,iband)
  else
    limit(1) = rdesc%lohard(1,iband)
    limit(2) = rdesc%lohard(2,iband)
  endif
  !
  !Find SideBand
  fumin=rdesc%locall(1,iband)+rdesc%iflim(1)+rdesc%ifband/4d0
  !Choose to switch from LSB to USB at the center of the band
  sbcode=usb_code
  if (frf.lt.fumin) then
    sbcode=lsb_code
  endif
  ncomm%sideband=sideband(sbcode)
  !
  !Find FIF
  ncomm%fcent=rdesc%bbref(2) ! Inner baseband by default
  ! Check band edge
  ! LSB case
  if (sbcode.eq.lsb_code) then
    if ((frf+ncomm%fcent).lt.limit(1)) then
      ncomm%fcent=limit(1)-frf
    else if ((frf+ncomm%fcent).gt.limit(2)) then
      ncomm%fcent=limit(2)-frf
    endif
  ! USB case
  else if (sbcode.eq.usb_code) then
    if ((frf-ncomm%fcent).lt.limit(1)) then
      ncomm%fcent=frf-limit(1)
    else if ((frf-ncomm%fcent).gt.limit(2)) then
      ncomm%fcent=frf-limit(2)
    endif
  endif
  !
end subroutine noema_default_tuning
!
subroutine rec_noema_online(line,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_noema_online
  use ast_astro
  use my_receiver_globals
  use plot_molecules_globals
  use frequency_axis_globals
  !-----------------------------------------------------------------------
  ! @ private
  ! ASTRO implementation of the tuning command in OBS\LINE
  ! Decode the line and fill noematune structure used as input for true tuning routines
  !-----------------------------------------------------------------------
  character(len=*), intent(inout) :: line        ! command line
  logical, intent(inout) :: error
  ! local
  character(len=*),parameter :: rname='LINE'
  integer(kind=4), parameter :: optrec=1        ! /RECEIVER option
  integer(kind=4) :: inputrec,izoom,lname
  character(len=128) :: mess
  ! taken form obs:
  logical :: sideband_entered,fcent_entered
  integer :: ipar,lpar,harm
  real(kind=8) :: fcent,tmpnum
  character(len=12) :: par
  character(len=4) :: band
  !
  error = .false.
  !
  if (obsname.ne.'NOEMA') then
    call astro_message(seve%e,rname,'Inconsistency between Receiver Name and Observatory')
    error = .true.
    return
  endif
  !
  !Define receiver parameters
  call astro_def_receiver(rname,'NOEMA',noema%recdesc,error)
  if (error) return
  !Define ifproc parameters
  call rec_define_noema_ifproc(noema%cfebe%i_f%ifproc,error)
  if (error) return
  ! Put in memory the current ASTRO%SOURCE parameters
  noema%recdesc%redshift=.false.           ! source redshift not taken into account
  call astro_def_recsource(rname,noema%recdesc,noema%source,error)
  if (error) return
  ! Warning in redshift because line does not handle this
  if (noema%source%sourcetype.ne.soukind_none.and.noema%source%z.ne.0) then
    call astro_message(seve%e,rname,'LINE command does not take into account source redshift')
    call astro_message(seve%e,rname,'Change your source and try again')
    error=.true.
    return
  endif
  !
  ! Defaults for some parameters in the NOEMA_OBS case:
  call noema_reset_setup(noema%cfebe%tune,noema%cfebe%i_f,error)
  if (error) return
  noema%cfebe%tune%dotune=.true.
  noema%cfebe%tune%fixedfreq=.true.           ! no concept of tuning grid in OBS
  noema%cfebe%tune%zoommode='SINGLE'
  izoom=1
  !
  ! Now parse the command line
  ! Line name
  call sic_ch(line,0,1,noema%cfebe%tune%name,lname,.true.,error)
  if (error) return
  ! Frequency
  call sic_r8(line,0,2,noema%cfebe%tune%frest,.true.,error)
  if (error) return
  !Option /RECEIVER
  inputrec=0
  call sic_i4(line,optrec,1,inputrec,.false.,error)
  if (error) return
  !
  noema%cfebe%tune%sideband='USB '
  noema%cfebe%tune%fcent=noema%recdesc%bbref(2) !center of inner baseband
  ! OBS variables - some will not be used but must be parsed
  harm = 0
  sideband_entered = .false.
  fcent_entered =  .false.
  do ipar=3,sic_narg(0)
    call sic_ke(line,0,ipar,par,lpar,.true.,error)
    if (error) return
    !
    if (par(1:1).eq.'H') then
      !
      ! HIGH
      call astro_message(seve%w,rname,  &
            'LOCK LOW|HIGH is not an input parameter anymore')
      !
    elseif (par(1:1).eq.'L') then
      !
      ! LOW
      if (lpar.ge.2.and.par(1:2).eq.'LO') then
          call astro_message(seve%w,rname, &
              'LOCK LOW|HIGH is not an input parameter anymore')
          cycle
      endif
      !
      ! LSB
      if (sideband_entered) then
        call astro_message(seve%e,rname,'Ambiguous entries: LSB or USB?')
        error = .true.
        return
      endif
! TO BE CHECKED AFTER TUNING (LIKE /RECE and found rec)
!         if (band(1:1).eq.'U') then
!            call astro_message(seve%w,rname,'Default tuning (USB) forced to LSB')
!         endif
      band = 'LSB '
      sideband_entered = .true.
      !
    elseif (par(1:1).eq.'U') then
       !
       ! USB
      if (sideband_entered) then
        call astro_message(seve%e,rname,'Ambiguous entries: LSB or USB?')
        error = .true.
        return
      endif
!         if (band(1:1).eq.'L') then
!            call astro_message(seve%w,rname,'Default tuning (LSB) forced to USB')
!         endif
      band = 'USB '
      sideband_entered = .true.
        !
    else
      ! A number...
      call sic_math_dble (par,lpar,tmpnum,error)
      if (error) then
        write (mess,'(a,1x,a)') 'Error decoding input:',par(1:lpar)
        call astro_message(seve%e,rname,mess)
        error=.true.
        return
      endif
      if (tmpnum.gt.noema%recdesc%iflim(1).and.tmpnum.lt.noema%recdesc%iflim(2)) then
        if (fcent_entered) then
          call astro_message(seve%e,rname,'IF center frequency already entered')
          error = .true.
          return
        else
          ! Fcent in MHz
          fcent = tmpnum
          fcent_entered = .true.
        endif
      elseif (tmpnum.ge.32 .and. tmpnum.le.70) then
        ! Harmonic number - old system  (<2010)
        call astro_message(seve%e,rname,'Invalid harmonic number 1')
        error = .true.
        return
      elseif (tmpnum.ge.5 .and. tmpnum.le.9) then
        ! Harmonic number - new system  (>2010)
        harm = tmpnum
      else
        write (mess,'(a,1x,a)') 'Error decoding input:',par(1:lpar)
        call astro_message(seve%e,rname,mess)
        error=.true.
        return
      endif
    endif
  enddo
  if (sideband_entered) then
    noema%cfebe%tune%sideband=band
  endif
  if (fcent_entered) then
    noema%cfebe%tune%fcent=fcent
  endif
  !
  !Do the tuning
  call noema_setup(rname,noema%cfebe%tune,noema%cfebe%reccomm,noema%recdesc, &
                  noema%source,noema%cfebe%rectune,error)
  if (error) return
  noema%cfebe%defined=.true.
  !
  !Check that receiver band is OK with /RECEIVER option
  if (inputrec.ne.0.and.inputrec.ne.noema%cfebe%rectune%iband) then
    call astro_message(seve%e,rname,'Frequency/ReceiverBand mismatch:')
    write (mess,'(f0.3,1x,a,1x,i0)') noema%cfebe%tune%frest,'is not in Band',inputrec
    call astro_message(seve%e,rname,mess)
    error=.true.
    return
  endif
  !
  ! Now the plotting
  !
  if (noema%cfebe%tune%zoommode.eq.'SINGLE') then
    if (izoom.le.0.or.(.not.noema%cfebe%tune%dotune)) then
      call astro_message(seve%e,rname,'Tried to zoom on a not tuned band')
      error = .true.
      return
    endif
    call rec_plot_sidebands(noema%recdesc,noema%source,noema%cfebe%rectune, &
                            cplot,molecules,freq_axis,error)
    if (error) return
  else 
    call astro_message(seve%e,rname,'Plot mode not understood')
    error = .true.
    return
  endif
  !
  ! IF transport
  ! define if ranges transported
  call noema_ifproc(rname,noema%recdesc,noema%CFEBE%rectune,noema%CFEBE%i_f%ifproc,error)
  if (error) return
  ! Update the plot
  call noema_ifproc_plot(rname,noema%CFEBE%tune%fixed_scale,noema%CFEBE%i_f,molecules, &
          noema%recdesc,noema%source,noema%CFEBE%rectune,cplot,freq_axis,error)
  if (error) return
  ! define polyfix and assign if "cables" to correlator units [only input, no mode yet]
  call noema_define_pfx(noema%cfebe%pfx,error)
  if (error) return
  call noema_reset_backend(noema%cfebe%pfx,noema%cfebe%spw%out,error)
  if (error) return
  noema%cfebe%spw%comm%itype=-1
  call noema_assign_units(rname,noema%cfebe%i_f%ifproc,noema%cfebe%pfx,noema%recdesc,error)
  if (error) return
  !
  ! Let the user with the limits in main axis frequency frame
  call rec_set_limits(rname,cplot,freq_axis%main,error)
  if (error) return
  !
end subroutine rec_noema_online
!
subroutine noema_setup(rname,noematune,rcomm,rdesc,rsou,rtune,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_setup
  use astro_types
  use frequency_axis_globals
  !-----------------------------------------------------------------------
  ! @ private
  ! Check feasability and tune noema requested bands
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(noema_tuning_comm_t), intent(inout) :: noematune !command line parameters
  type(receiver_desc_t), intent(in) :: rdesc ! receiver description and tuning
  type(receiver_comm_t), intent(inout) :: rcomm ! receiver description and tuning
  type(receiver_source_t), intent(in) :: rsou ! receiver description and tuning
  type(receiver_tune_t), intent(inout) :: rtune ! receiver description and tuning
  logical, intent(inout)        :: error
  ! Local
  logical       :: ongrid,bandedge
  type(receiver_comm_t) :: gridcomm
  real(kind=8)  :: newfcent,ftune
  character(len=200) :: mess
  !
  rcomm%rec_name = 'NOEMA'
  rcomm%frest = noematune%frest
  call sic_upper(noematune%sideband)
  if (noematune%sideband(1:1).eq.'U') then
    rcomm%sideband = 'USB'
  else if (noematune%sideband(1:1).eq.'L') then
    rcomm%sideband = 'LSB'
  else
    call astro_message(seve%e,rname,'Problem decoding tuning sideband code (LSB or USB)')
    error = .true.
    return
  endif
  rcomm%fcent = noematune%fcent
  rcomm%name = noematune%name
  ! tune the bands
  call rec_resttooutput(rname,noematune%frest,freq_axis%main,rsou,ftune,error)
  if (error) return
  if (noematune%fixedfreq) then
    ! ignore the grid
    rtune%ongrid=.false.
    call astro_tune_receiver(rname,rdesc,rsou,rcomm,rtune,error)
    if (error) return
  else ! we shift the tuning to the grid
    ongrid = .false.
    bandedge = .false.
    gridcomm = rcomm
    do while (.not.ongrid.and..not.bandedge)
      call astro_tune_receiver(rname,rdesc,rsou,gridcomm,rtune,error)
      if (error) return
      write(mess,'(a,f10.5,a)') ' FLOTUNE = ',rtune%flotune*ghzpermhz,' GHz'
      call astro_message(seve%i,rname,mess)
      call astro_tunegrid(rname,rdesc,rtune,rsou, &
                          bandedge,ongrid,newfcent,error)
      if (error) return
      gridcomm%fcent = newfcent
    enddo
    rtune%ongrid=ongrid
  endif
  !
end subroutine noema_setup
!
subroutine astro_tunegrid(rname,rdesc,rtune,rsou,bandedge,ongrid,newfcent,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>astro_tunegrid
  use astro_types
  use frequency_axis_globals
  !-----------------------------------------------------------------------
  ! @ private
  ! Check that LO is on the tuning grid, propose the right if freq
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(receiver_desc_t), intent(in) :: rdesc ! receiver description 
  type(receiver_tune_t), intent(in) :: rtune ! receiver tuning
  type(receiver_source_t), intent(in) :: rsou ! receiver tuning
  logical, intent(inout)            :: bandedge
  logical, intent(inout)            :: ongrid
  real(kind=8), intent(inout)       :: newfcent
  logical, intent(inout)        :: error
  character(len=32) :: cname
  ! Local
  real(kind=8)  :: fgrid,foutput
  character(len=200) :: mess
  !
  fgrid = nint(rtune%flotune/rdesc%gridbin)*rdesc%gridbin
  if ((fgrid.lt.rdesc%locall(1,rtune%iband).and.rtune%flotune.ge.rdesc%locall(1,rtune%iband)) &
       .or.(fgrid.gt.rdesc%locall(2,rtune%iband).and.rtune%flotune.le.rdesc%locall(2,rtune%iband)) &
       .or.(fgrid.lt.rdesc%lohard(1,rtune%iband).or.fgrid.gt.rdesc%locall(2,rtune%iband))) then
    ! don't shift to the grid if this push the LO out of the recommended or possible ranges
    bandedge=.true.
    newfcent = rtune%fcent
    call astro_message(seve%i,rname,'Tuning close to band edge. Not shifted to the grid')
    return
  endif
  if (abs(fgrid-rtune%flotune).lt.rdesc%gridtol) then
    ! Tuning is on the grid
    newfcent = rtune%fcent
    ongrid = .true.
    return
  endif
  ! Compute new IF freq to be on the tuning grid
  call astro_message(seve%i,rname,'Original tuning does not match the grid')
  newfcent = (rtune%flsr-fgrid)/sb_sign(rtune%sb_code)
  ! Check that new IF stays in the IF range
  if (newfcent.lt.rdesc%iflim(1).or.newfcent.gt.rdesc%iflim(2)) then
    ! Too close to band edge to move
    bandedge=.true.
    newfcent = rtune%fcent
  endif
  ! Write information
  write(mess,'(a,f0.3,a)') 'Tuning automatically shifted to the IF Frequency = ',newfcent,' MHz'
  call astro_message(seve%i,rname,mess)
  write(mess,'(a,f0.3,a)') 'This corresponds to a shift of ',abs(rtune%fcent-newfcent),' MHz'
  call astro_message(seve%i,rname,mess)
  call astro_message(seve%i,rname,'Actual command:')
  if (rname.eq.'NEWVEL') then
    cname='TUNING'
  else
    cname=rname
  endif
  call rec_resttooutput(rname,rtune%frest,freq_axis%main,rsou,foutput,error)
  if (error) return
  write (mess,'(a,1x,f0.3,1x,a,1x,f0.3)') trim(cname),foutput/1d3,sideband(rtune%sb_code),newfcent
  call astro_message(seve%r,rname,mess)
  !
end subroutine astro_tunegrid
!
subroutine noema_ifproc(rname,rdesc,rtune,ifproc,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_ifproc
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! define the basebands brought to the backends
  ! no flxibility with noema for the time being
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(receiver_desc_t), intent(inout) :: rdesc
  type(receiver_tune_t), intent(inout) :: rtune
  type(correlator_input_t), intent(inout) :: ifproc
  logical, intent(inout)        :: error
  ! local
  integer(kind=4)       :: is,ip,icable
  integer(kind=4)       :: polcode(rdesc%n_polar),sb_code(rdesc%n_sbands)
  character(len=200)    :: comm
  !
  polcode(1) = hpol_code
  polcode(2) = vpol_code
  !
  icable = 0
  do ip = 1,rdesc%n_polar
    if (rdesc%n_sbands.eq.2) then
      sb_code(1) = lsb_code !lsb first to match the agreement about ordering (VP 2017)
      sb_code(2) = usb_code
    else if (rdesc%n_sbands.eq.1) then
      sb_code(1) = rtune%sb_code
    else
      call astro_message(seve%e,rname,'Problem with number of sidebands')
      error = .true.
      return
    endif
    do is = 1,rdesc%n_sbands
      icable = icable+1
      if (icable.gt.ifproc%n_ifcables) then
        call astro_message(seve%e,rname,'More IF than available cables')
        error=.true.
        return
      endif
      ifproc%ifc(icable)%iband = rtune%iband
      ifproc%ifc(icable)%sb_code = sb_code(is)
      ifproc%ifc(icable)%pol_code = polcode(ip)
      write (ifproc%ifc(icable)%label,'(a,i0,a,a,a)')                 &
              rdesc%bandname(rtune%iband)(1:1),                   &
              rtune%iband,rdesc%polname(ip)(1:1),                 &
              sideband(sb_code(is))(1:1)
    enddo ! sidebands
  enddo ! polar
  !
  ifproc%defined = .true.
  !
  do icable=1,ifproc%n_ifcables
    write (comm,'(a,1x,i0,1x,a,1x,a)') 'Correlator input #',icable,'contains',ifproc%ifc(icable)%label
    call astro_message(seve%d,rname,comm)
  enddo
  !
end subroutine noema_ifproc
!
subroutine noema_ifproc_plot(rname,fixed_scale,noema_if,molecules,rdesc,rsou,rtune,cplot,drawaxis,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_ifproc_plot
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! identifies the correlator inputs on the plot
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  logical, intent(in) :: fixed_scale
  type(noema_if_t), intent(in) :: noema_if
  type(plot_molecules_t), intent(in) :: molecules
  type(receiver_desc_t), intent(inout) :: rdesc
  type(receiver_source_t), intent(inout) :: rsou
  type(receiver_tune_t), intent(inout) :: rtune
  type(current_boxes_t), intent(inout) :: cplot
  type(frequency_axis_t), intent(in) :: drawaxis
  logical, intent(inout) :: error
  ! local
  integer(kind=4) :: i,k,sb_code(m_sideband),ip,ib,ic
  character(len=200) :: comm,defchar,smallchar,molchar
  character(len=5) :: cablecode
  real(kind=8) :: fif1,frf,frest,yt,l1,l2,l3,l4,b1,b2,b3,b4
  type(draw_line_t)     :: linebb
  !
  sb_code(1) = usb_code
  sb_code(2) = lsb_code
  !
  write (defchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%defchar
  write (smallchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%smallchar
  write (molchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%molchar
  !
  call gr_exec1(defchar)
  call gr_pen(colour=adefcol,error=error)
  if (error) return
  !
  write (comm,'(a)') 'CHANGE DIRECTORY'
  call gr_execl(comm)
  if (gtexist('IFPROC')) then
    write (comm,'(a)') 'DESTROY DIRECTORY IFPROC'
    call gr_execl(comm)
  endif
  write (comm,'(a)') 'CREATE DIRECTORY IFPROC'
  call gr_execl(comm)
  do i=1,cplot%nbox
    write (comm,'(a,i0)') 'CHANGE DIRECTORY <GREG<BOX',i
    call gr_execl(comm)
    l1 = cplot%box(i)%rest%xmin
    l2 = cplot%box(i)%rest%xmax
    l3 = cplot%box(i)%rest%ymin
    l4 = cplot%box(i)%rest%ymax
    b1 = cplot%box(i)%phys%xmin
    b2 = cplot%box(i)%phys%xmax
    b3 = cplot%box(i)%phys%ymin
    b4 = cplot%box(i)%phys%ymax
    write (comm,'(a)') 'CHANGE DIRECTORY'
    call gr_execl(comm)
    write (comm,'(a)') 'CHANGE DIRECTORY IFPROC'
    call gr_execl(comm)
    write (comm,'(a,4(1x,f0.3))') 'SET BOX_LOCATION',b1,b2,b3,b4
    call gr_exec1(comm)
    write (comm,'(a,4(1x,f0.3))') 'LIMITS',l1,l2,l3,l4
    call gr_exec1(comm)
    if (rtune%iband.ne.cplot%box(i)%iband) cycle
    do k=1,rdesc%n_sbands
        do ib=1,rdesc%n_bbands
          do ip=1,rdesc%n_polar
            write (cablecode,'(a,i0,a,a,a,a)') rdesc%bandname(rtune%iband)(1:1),            &
                rtune%iband,rdesc%polname(ip)(1:1),sideband(sb_code(k))(1:1),rdesc%bbname(ib)(1:1)
            do ic=1,noema_if%ifproc%n_ifcables
              if (noema_if%ifproc%ifc(ic)%label.ne.cablecode(1:4)) cycle
              fif1 = (rdesc%bblim(2,ib)+rdesc%bblim(1,ib))/2d0
              yt = l3+((l4-l3)/6)*ip
              call if1torf(rtune%flo1,fif1,sb_code(k),frf,error)
              if (error) return
              call rftorest(rsou%dopshift,frf,frest,error)
              if (error) return
              call gr_pen(colour=abbifproccol,error=error)
              if (error) return
              call gr_exec1(smallchar)
              write (comm,'(a,1x,f0.6,1x,f0.3,1x,a,a,1x,a)')               &
                'DRAW TEXT',frest,yt,'"',trim(cablecode),'" 5 0 /USER /CLIP'
              call gr_exec1(comm)
              call gr_exec(defchar)
              call noema_draw_confusion(rname,rdesc,rtune%flo1,rsou%dopshift, &
                                        ib,sb_code(k),cplot%box(i)%rest,cplot%desc,error)
              if (error) return
              call gr_pen(colour=adefcol,error=error)
              if (error) return
              !draw the limit of the baseband
              call if1torf(rtune%flo1,rdesc%bblim(1,ib),sb_code(k),frf,error)
              if (error) return
              call rftorest(rsou%dopshift,frf,linebb%xmin,error)
              if (error) return
              call if1torf(rtune%flo1,rdesc%bblim(2,ib),sb_code(k),frf,error)
              if (error) return
              call rftorest(rsou%dopshift,frf,linebb%xmax,error)
              if (error) return
              linebb%ymin = cplot%box(i)%rest%ymin+(cplot%box(i)%rest%ymax-cplot%box(i)%rest%ymin)/10
              linebb%ymax = linebb%ymin
              linebb%col   = abbifproccol
              linebb%dash  = 1
              call gr_exec1(smallchar)
              call rec_draw_arrow(linebb,cplot%box(i)%rest,error)
              if (error) return
              call gr_exec1(defchar)
              frest = linebb%xmin
              linebb%xmin = linebb%xmax
              linebb%xmax = frest
              call rec_draw_arrow(linebb,cplot%box(i)%rest,error)
              if (error) return
              call gr_pen(colour=adefcol,error=error)
              if (error) return
              call gr_exec1(defchar)
            enddo ! cable
          enddo ! polar
          !
        enddo !baseband
!        endif
    enddo !sideband
    !plot again molecules
    call gr_exec1(molchar)
    call rec_draw_molecules(molecules,cplot%box(i)%rest,error)
    if (error) return
    call gr_exec1(defchar)
    ! plot tuning frequency
    call rec_draw_linetune(rtune,cplot%box(i)%rest,error)
    if (error) return
    !
      write (comm,'(a)') 'CHANGE DIRECTORY'
    call gr_execl(comm)
    write (comm,'(a)') 'CHANGE DIRECTORY'
    call gr_execl(comm)
  enddo ! boxes
  call gr_pen(colour=adefcol,error=error)
  if (error) return
  !
end subroutine noema_ifproc_plot
!
subroutine noema_draw_confusion(rname,rdesc,flo1,sdop,bb_code,sb_code,fbox,cpdesc,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_draw_confusion
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! identifies the confusion zone of NOEMA
  ! S18 version: only the 10MHz of the LO2 parasite
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(receiver_desc_t), intent(in) :: rdesc
  real(kind=8), intent(in)   :: flo1
  real(kind=8), intent(in)   :: sdop
  integer(kind=4), intent(in)   :: bb_code
  integer(kind=4), intent(in)   :: sb_code
  type(frequency_box_user_t), intent(in) :: fbox ! must be rest
  type(current_boxes_desc_t), intent(in) :: cpdesc 
  logical, intent(inout) :: error
  ! Local
  real(kind=8)  :: frf,xt,yt
  type(draw_rect_t) :: confrect
  character(len=200) :: comm,smallchar,defchar
  integer(kind=4) :: iplotmode,ipos
  !
  iplotmode=cpdesc%plotmode
  !Compute Confusion zone in rest frame to draw rectangle
  call if1torf(flo1,rdesc%if1conf(1),sb_code,frf,error)
  if (error) return
  call rftorest(sdop,frf,confrect%xmin,error)
  if (error) return
  call if1torf(flo1,rdesc%if1conf(2),sb_code,frf,error)
  if (error) return
  call rftorest(sdop,frf,confrect%xmax,error)
  if (error) return
  confrect%ymin = fbox%ymin
  confrect%ymax = fbox%ymax
  confrect%col=aconfucol
  ! Draw rectangle
  call gr_pen(colour=aconfucol,error=error)
  if (error) return
  call rec_draw_frect(confrect,fbox,error)
  if (error) return
  ! Write label (depends on plotmode)
  write (defchar,'(a,1x,f0.3)') 'SET CHARACTER',cpdesc%defchar
  write (smallchar,'(a,1x,f0.3)') 'SET CHARACTER',cpdesc%smallchar
  !
  call gr_exec1(smallchar)
  if (iplotmode.eq.pm_basebands) then
    if (sb_code.eq.lsb_code.and.bb_code.eq.outer_code) ipos = 3
    if (sb_code.eq.lsb_code.and.bb_code.eq.inner_code) ipos = 1
    if (sb_code.eq.usb_code.and.bb_code.eq.inner_code) ipos = 3
    if (sb_code.eq.usb_code.and.bb_code.eq.outer_code) ipos = 1
    write (comm,'(a,1x,i0)') 'DRAW TEXT 0 -0.5 "LO2" 5 0 /character',ipos
    call gr_exec1(comm)
  else if (iplotmode.eq.pm_receiver) then
    write (comm,'(a)') 'DRAW TEXT 0 -0.75 "LO2" 5 0 /character 2'
    call gr_exec1(comm)
  else if (iplotmode.eq.pm_allbands.or.iplotmode.eq.pm_tunedbands) then
    xt=(confrect%xmax+confrect%xmin)/2d0
    yt=confrect%ymin-(confrect%ymax-confrect%ymin)/20d0
    write (comm,'(a,1x,f0.3,1x,f0.3,1x,a)') 'DRAW TEXT',xt,yt,'"LO2" 5 0 /user'
    call gr_exec1(comm)
  else if (iplotmode.eq.pm_proposal) then
    if (bb_code.eq.1) then
      xt=(confrect%xmax+confrect%xmin)/2d0
      yt=confrect%ymin-(confrect%ymax-confrect%ymin)/20d0
      write (comm,'(a,1x,f0.3,1x,f0.3,1x,a)') 'DRAW TEXT',xt,yt,'"LO2" 5 0 /user'
      call gr_exec1(comm)
    endif
  else
    call astro_message(seve%e,rname,'Problem with plot mode')
    error=.true.
    return
  endif
  call gr_pen(colour=adefcol,error=error)
  if (error) return
  call gr_exec1(defchar)
  !
end subroutine noema_draw_confusion
!
subroutine noema_baseband(line,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_baseband
  use ast_astro
  use my_receiver_globals
  use plot_molecules_globals
  use string_parser_types
  use frequency_axis_globals
  !-----------------------------------------------------------------------
  ! @ private
  ! Baseband and mode selection (in preparation for SPW definition)
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(inout) :: line        ! command line
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='BASEBAND'
  integer(kind=4), parameter :: optreset=1
  integer(kind=4), parameter :: optmode=2
  character(len=200) :: mess
  type(string_parser_t) :: sp
  integer(kind=4) :: lpar,narg,selmode
  integer(kind=4) :: pol_sel,bb_sel,sb_sel,selband,isel,iu
  character(len=12) :: selcode
  character(len=argument_length) :: par
  character(len=argument_length), parameter :: mode_key="OFF"
  integer(kind=4) :: iunit
  real(kind=4) :: res1,res2,resmin,resmax
  real(kind=4), parameter :: resmed=250d0
  real(kind=4), parameter :: reslow=2000d0
  real(kind=4), parameter :: reshigh1=62.5d0
  logical       :: doreset,dopchanged,dooff,domode
  integer(kind=4), parameter :: ipband=1
  integer(kind=4), parameter :: ippol=2
  integer(kind=4), parameter :: ipsb=3
  integer(kind=4), parameter :: ipbb=4
  integer(kind=4), parameter :: nband=3
  integer(kind=4), parameter :: npol=2
  integer(kind=4), parameter :: nsb=2
  integer(kind=4), parameter :: nbb=2
  integer(kind=4)       :: parsecode(4)
  character(len=2) :: bb_band(nband)
  character(len=1) :: bb_pol(npol),bb_sb(nsb),bb_bb(nbb)
  data bb_band/'B1','B2','B3'/
  data bb_pol/'H','V'/
  data bb_sb/'U','L'/
  data bb_bb/'O','I'/
  !
  !Preliminary checks
  if (obsname.ne.'NOEMA') then
    call astro_message(seve%e,rname,'Observatory changed since last tuning - Please define a new tuning')
    error = .true.
    return
  endif
  !
  if (.not.noema%cfebe%defined) then
    call astro_message(seve%e,rname,'Please define a tuning (TUNING command) before setting up backends')
    error = .true.
    return
  endif
  if (.not.noema%cfebe%i_f%ifproc%defined) then
    call astro_message(seve%e,rname,'Problem with IF Processor part')
    error = .true.
    return
  endif
  !
  !check that doppler did not changed since last tuning
  call rec_check_doppler(noema%source,noema%recdesc%redshift,dopchanged,error)
  if (dopchanged) then
    call astro_message(seve%e,rname,'Source changed since last tuning')
    call astro_message(seve%i,rname,'You should set again the tuning before working with backends')
    call rec_display_error('Source changed since last tuning',error)
    error = .true.
    return
  endif
  !
  narg=sic_narg(0)
  doreset=sic_present(optreset,0)
  domode=sic_present(optmode,0)
  !
  noema%cfebe%i_f%selunit%n_ifsel=0
  do iu=1,m_backunits
    noema%cfebe%i_f%selunit%usel(iu)=-1
  enddo
  !
  !Read correl mode
  selmode=-1
  dooff=.false.
  if (domode) then
    call sic_ke(line,optmode,1,par,lpar,.true.,error)
    if (error) return    
    if (par(1:lpar).eq.mode_key(1:lpar)) then
      selmode=-1
      dooff=.true.
    else
      ! Mode entered through resolution
      res1=undef_freq
      res2=undef_freq
      call sic_r4(line,optmode,1,res1,.true.,error)
      if (error) return
      call sic_r4(line,optmode,2,res2,.false.,error)
      if (error) return
      if (res2.eq.undef_freq) then 
        if (res1.eq.reshigh1.or.res1.eq.reslow) then
          call astro_message(seve%w,rname,'Channel spacing is ambiguous. Please enter 2 values')
          call astro_message(seve%w,rname,'Correlator mode unchanged')
          domode=.false.
        else
          res2=res1
        endif
      endif
      !
      resmin=min(res1,res2)
      resmax=max(res1,res2)
      !
      if (resmin.eq.resmed.and.resmax.eq.resmed) then
        selmode=3
      else if (resmin.eq.reshigh1.and.resmax.eq.reslow) then
        selmode=1
      else
        call astro_message(seve%w,rname,'Channel spacing values do not match any correlator mode')
        call astro_message(seve%w,rname,'Correlator mode unchanged')
        domode=.false.
      endif        
    endif
  endif
  !Reading arguments
  selcode=''
  if (narg.gt.2) then
    call astro_message(seve%e,rname,'Too many arguments')
    error=.true.
    return
  else if (narg.eq.2) then
    ! Handle PMS syntax for first polyfix deadlines
    call astro_message(seve%w,rname,'2 Arguments syntax is obsolescent, please use /MODE option instead')
    if (sic_present(optmode,0)) then
      call astro_message(seve%w,rname,'2 arguments syntax cannot be combined with /MODE option.')
      call astro_message(seve%w,rname,'Correlator mode unchanged')
      domode=.false.
    else
      call sic_ke(line,0,1,selcode,lpar,.true.,error)
      if (error) return
      call sic_r4(line,0,2,res1,.true.,error)
      if (error) return
      if (res1.ne.reshigh1) then
        call astro_message(seve%w,rname,'Only 62.5 accepted as second argument. Use /MODE option.')
        call astro_message(seve%w,rname,'Correlator mode unchanged')
        domode=.false.
      else
        selmode=1
        domode=.true.
      endif
    endif
  else if (narg.eq.1) then
    call sic_ke(line,0,1,selcode,lpar,.true.,error)
    if (error) return
  endif 
  !
  ! Check mode selection
  if (selmode.gt.3) then
    call astro_message(seve%w,rname,'Mode not yet implemented (only modes 1 and 2)')
    call astro_message(seve%w,rname,'Correlator mode unchanged')
    domode=.false.
  endif
  !
  !Defaults codes
  pol_sel=0
  bb_sel=0
  sb_sel=0
  selband=0
  !Parse the user code
  if (len_trim(selcode).ne.0) then
    call string_parser_addlist(sp,ipband,bb_band,error)
    if (error) return
    call string_parser_addlist(sp,ippol,bb_pol,error)
    if (error) return
    call string_parser_addlist(sp,ipsb,bb_sb,error)
    if (error) return
    call string_parser_addlist(sp,ipbb,bb_bb,error)
    if (error) return
    call string_parser_parse('BASEBAND',sp,selcode,.false.,parsecode,error)
    if (error) return
  else 
    parsecode(ipband)=0
    parsecode(ippol)=0
    parsecode(ipsb)=0
    parsecode(ipbb)=0
  endif
  !
  if (parsecode(ippol).eq.0) then
    noema%cfebe%i_f%selunit%polmode = "B" ! BOTH polar same config
  else
    noema%cfebe%i_f%selunit%polmode = "S"
  endif
  !
  ! Loop over pfx%units to put the right numbers in ifsel() and define the mode when needed
  isel=0
  iunit=0
  do iu=1,noema%cfebe%pfx%n_units
    if (noema%cfebe%pfx%unit(iu)%iband.ne.parsecode(ipband).and.parsecode(ipband).ne.0) cycle
    if (noema%cfebe%pfx%unit(iu)%pol_code.ne.parsecode(ippol).and.parsecode(ippol).ne.0) cycle
    if (noema%cfebe%pfx%unit(iu)%sb_code.ne.parsecode(ipsb).and.parsecode(ipsb).ne.0) cycle
    if (noema%cfebe%pfx%unit(iu)%bb_code.ne.parsecode(ipbb).and.parsecode(ipbb).ne.0) cycle
    isel=isel+1
    noema%cfebe%i_f%selunit%usel(isel)=iu
    if (dooff.or.doreset.or.(domode.and.selmode.ne.noema%cfebe%pfx%unit(iu)%imode)) then
      write (mess,'(a,1x,i0)') 'Resetting unit ',iu
      call astro_message(seve%i,rname,mess)
      call pfx_reset_unit(noema%cfebe%pfx%unit(iu),noema%cfebe%spw%out,error)
      if (error) return
      ! Change mode when needed
      if (domode.or.dooff) noema%cfebe%pfx%unit(iu)%imode=selmode
      iunit=iu
      if (.not.dooff) then
        call pfx_fixed_spw(rname,noema%recdesc,noema%source,noema%cfebe%rectune,  &
                            noema%cfebe%pfx%unit(iu),noema%cfebe%spw,error)
        if (error) return
      endif
    endif
  enddo !iu
  if (isel.eq.0) then
    call astro_message(seve%e,rname,'The selection returns nothing')
    error = .true.
    return
  endif
  ! remove empty spw (if there was a reset)
  call noema_compress_spw(noema%cfebe%spw%out,error)
  if (error) return
  ! sort spw
  call noema_sort_spw(noema%cfebe%spw%out,error)
  if (error) return
  ! look for conflicts
  call noema_check_conflicts(rname,noema%cfebe%spw%out,noema%cfebe%pfx,error)
  if (error) return
  !
  noema%cfebe%i_f%selunit%n_ifsel=isel
  write (mess,'(a)') 'Selected Basebands are:'
  do iu=1,m_backunits
    if (noema%cfebe%i_f%selunit%usel(iu).ne.-1) then
!      write (mess,'(a,1x,a)') trim(mess),trim(noema%ifproc%ifc(noema%selunit%usel(iu))%label)
      write (mess,'(a,1x,a)') trim(mess),  &
          trim(noema%cfebe%pfx%unit(noema%cfebe%i_f%selunit%usel(iu))%label)
    endif
  enddo !iu
  call astro_message(seve%i,rname,mess)
  if (iunit.ne.0.and.selmode.gt.0) then
    call noema_pfx_mode_message(rname,noema%cfebe%pfx%unit(iunit)%mode(selmode),error)
    if (error) return
  endif
  !
  call noema_plot_selpfx(rname,noema,cplot,molecules,freq_axis,error)
  if (error) return
  !
  if (noema%source%sourcetype.eq.soukind_full) then
    call noema_oplot_dopminmax(noema,cplot,freq_axis,error)
    if (error) return
  endif
  !
  ! Let the user with the limits in main axis frequency frame
  call rec_set_limits(rname,cplot,freq_axis%main,error)
  if (error) return
  !
end subroutine noema_baseband
!
subroutine noema_baseband_online(line,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_baseband_online
  use ast_astro
  use my_receiver_globals
  use plot_molecules_globals
  use string_parser_types
  use frequency_axis_globals
  !-----------------------------------------------------------------------
  ! @ private
  ! online Baseband and mode selection: as done in OBS
  ! Give a mode to the PFX units
  !-----------------------------------------------------------------------
  character(len=*), intent(inout) :: line        ! command line
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='BASEBAND'
  type(string_parser_t) :: sp
  integer(kind=4) :: iarg,lpar,narg,ncorrmode,selmode
  integer(kind=4) :: pol_sel,bb_sel,sb_sel,selband,isel,iu,inputrec
  integer(kind=4),parameter :: m_corrmodes = 4
  character(len=12) :: par,corrmode,selcode
  character(len=12) :: corrmodes(m_corrmodes)
  character(len=200) :: mess
  integer(kind=4), parameter :: optrec=1
  logical       :: dopchanged,gotcode,gotmode,dorec
  data corrmodes/'1','2','3','OFF'/
  integer(kind=4), parameter :: ipband=1
  integer(kind=4), parameter :: ippol=2
  integer(kind=4), parameter :: ipsb=3
  integer(kind=4), parameter :: ipbb=4
  integer(kind=4), parameter :: nband=3
  integer(kind=4), parameter :: npol=2
  integer(kind=4), parameter :: nsb=2
  integer(kind=4), parameter :: nbb=2
  integer(kind=4), parameter :: m_keys=1
  integer(kind=4)       :: parsecode(4),nkey
  character(len=2) :: bb_band(nband)
  character(len=1) :: bb_pol(npol),bb_sb(nsb),bb_bb(nbb)
  character(len=1) :: keys(m_keys),key
  data keys/'*'/
  data bb_band/'B1','B2','B3'/
  data bb_pol/'H','V'/
  data bb_sb/'U','L'/
  data bb_bb/'O','I'/
  !
  !Preliminary checks
  if (obsname.ne.'NOEMA') then
    call astro_message(seve%e,rname,'Observatory changed since last tuning - Please define a new tuning')
    error = .true.
    return
  endif
  !
  if (.not.noema%cfebe%defined) then
    call astro_message(seve%e,rname,'Please define a tuning (TUNING command) before setting up backends')
    error = .true.
    return
  endif
  if (.not.noema%cfebe%i_f%ifproc%defined) then
    call astro_message(seve%e,rname,'Problem with IF Processor part')
    error = .true.
    return
  endif
  !
  !check that doppler did not changed since last tuning ! SHOULD DISAPPEAR in the FUTURE
  call rec_check_doppler(noema%source,noema%recdesc%redshift,dopchanged,error)
  if (dopchanged) then
    call astro_message(seve%e,rname,'Source changed since last tuning')
    call astro_message(seve%i,rname,'You should set again the tuning before working with backends')
    call rec_display_error('Source changed since last tuning',error)
    error = .true.
    return
  endif
  !
  narg=sic_narg(0)
  dorec=sic_present(optrec,0)
  inputrec=0
  call sic_i4(line,optrec,1,inputrec,.false.,error)
  if (error) return
  !
  !Defaults codes
  pol_sel=0
  bb_sel=0
  sb_sel=0
  selband=0
  selcode=''
  selmode=1 ! defaults is mode 1
  corrmode='1'
  do iu=1,m_backunits
    noema%cfebe%i_f%selunit%usel(iu)=-1
  enddo
  !
  ! Input checks
  if (narg.ne.2) then
    call astro_message(seve%i,rname,'BASEBAND needs 2 arguments. Current state:')
    call noema_list_pfx(rname,noema%cfebe%pfx,freq_axis%main,noema%source,error)
    if (error) return
    return
  endif
  !Reading arguments
  gotmode=.false.
  gotcode=.false.
  do iarg=1,sic_narg(0)
    call sic_ke(line,0,iarg,par,lpar,.true.,error)
    if (error) return
    ! First try to recognize a correlator mode
    call sic_ambigs_sub(rname,par,corrmode,ncorrmode,corrmodes,m_corrmodes,error)
    if (error) return
    if (ncorrmode.eq.0) then ! correlator mode not recognized
      if (gotcode) then
        call astro_message(seve%e,rname,'Selection Code already entered. Problem with correlator mode choice')
        error=.true.
        return
      endif
      call sic_ambigs_sub(rname,par,key,nkey,keys,m_keys,error)
      if (error) return
      if (nkey.eq.0) then !key '*' not recognized
        selcode=par ! parameter is thus a selection code 
      else
        selcode=''
      endif
      gotcode=.true.
    else
      ! correlator mode recognized
      if (gotmode) then
        call astro_message(seve%e,rname,'Only one mode at a once !')
        error=.true.
        return
      endif
      selmode=ncorrmode
      gotmode=.true.
    endif
  enddo
  !
  ! Check mode selection
  corrmode=corrmodes(selmode)
  if (corrmode.eq.'OFF') then
    ! OFF CASE
    selmode=-1
  else if (corrmode.eq.'2')then
    ! VLBI: nothing in ASTRO
    call astro_message(seve%e,rname,'VLBI mode not supported in ASTRO')
    error=.true.
    return
  endif
  !
  !Parse the user code
  if (len_trim(selcode).ne.0) then
    call string_parser_addlist(sp,ipband,bb_band,error)
    if (error) return
    call string_parser_addlist(sp,ippol,bb_pol,error)
    if (error) return
    call string_parser_addlist(sp,ipsb,bb_sb,error)
    if (error) return
    call string_parser_addlist(sp,ipbb,bb_bb,error)
    if (error) return
    call string_parser_parse('BASEBAND',sp,selcode,.false.,parsecode,error)
    if (error) return
  else 
    parsecode(ipband)=0
    parsecode(ippol)=0
    parsecode(ipsb)=0
    parsecode(ipbb)=0
  endif
  !
  ! Case with /RECEIVER option
  if (dorec) then
    if (parsecode(ipband).ne.0) then
      if (parsecode(ipband).ne.inputrec) then
        call astro_message(seve%e,rname,'Inconsistency in Band selection')
        error=.true.
        return
      endif
    else if (parsecode(ipband).eq.0) then
      ! use the option input instead of the code
      parsecode(ipband)=inputrec
    endif
  endif
  !
  ! Loop over pfx%units to put the right numbers in ifsel() and define the mode when needed
  isel=0
  do iu=1,noema%cfebe%pfx%n_units
    if (noema%cfebe%pfx%unit(iu)%iband.ne.parsecode(ipband).and.parsecode(ipband).ne.0) cycle
    if (noema%cfebe%pfx%unit(iu)%pol_code.ne.parsecode(ippol).and.parsecode(ippol).ne.0) cycle
    if (noema%cfebe%pfx%unit(iu)%sb_code.ne.parsecode(ipsb).and.parsecode(ipsb).ne.0) cycle
    if (noema%cfebe%pfx%unit(iu)%bb_code.ne.parsecode(ipbb).and.parsecode(ipbb).ne.0) cycle
    isel=isel+1
    write (mess,'(a,1x,i0)') 'Resetting unit ',iu
    call astro_message(seve%i,rname,mess)
    ! Reset unit
    call pfx_reset_unit(noema%cfebe%pfx%unit(iu),noema%cfebe%spw%out,error)
    if (error) return
    if (selmode.ne.noema%cfebe%pfx%unit(iu)%imode) then
      noema%cfebe%pfx%unit(iu)%imode=selmode
    endif
    if (selmode.ne.-1) then
      call pfx_fixed_spw(rname,noema%recdesc,noema%source,noema%cfebe%rectune, &
                          noema%cfebe%pfx%unit(iu),noema%cfebe%spw,error)
      if (error) return
    endif
  enddo !iu
  if (isel.eq.0) then
    call astro_message(seve%e,rname,'The selection returns nothing')
    error = .true.
    return
  endif
  ! remove empty spw (if there was a reset)
  call noema_compress_spw(noema%cfebe%spw%out,error)
  if (error) return
  ! sort spw
  call noema_sort_spw(noema%cfebe%spw%out,error)
  if (error) return
  ! look for conflicts
  call noema_check_conflicts(rname,noema%cfebe%spw%out,noema%cfebe%pfx,error)
  if (error) return
  !
  ! Now the plotting
  ! PLOT
  !Select all the units for the plot
  noema%cfebe%i_f%selunit%n_ifsel=noema%cfebe%pfx%n_units
  noema%cfebe%i_f%selunit%polmode='B'
  do iu=1,noema%cfebe%pfx%n_units
    noema%cfebe%i_f%selunit%usel(iu)=iu
  enddo
  !
  call noema_plot_selpfx(rname,noema,cplot,molecules,freq_axis,error)
  if (error) return
  ! Let the user with the limits in main axis frequency frame
  call rec_set_limits(rname,cplot,freq_axis%main,error)
  if (error) return
  !
end subroutine noema_baseband_online
