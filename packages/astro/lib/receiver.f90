subroutine astro_def_receiver(rname,name,rdesc,error)
  use gbl_message
  use astro_interfaces, except_this=>astro_def_receiver
  use astro_types
  use ast_astro
  use ast_line
  !-----------------------------------------------------------------------
  ! @ private
  ! fill in the receiver type according to input name
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  character(len=*), intent(in)          :: name ! receiver name
  type(receiver_desc_t), intent(inout) :: rdesc ! receiver parameters
  logical, intent(inout)        :: error
  !
  !Define receiver properties in %desc
  if (name.eq."EMIR") then
    call rec_define_emir(rname,rdesc,error)
    if (error) return
  else if (name.eq.'NOEMA') then
    call rec_define_noema(rdesc,noema_mode,error)
    if (error) return
  else if (name.eq.'HERA') then
    rdesc%name = 'HERA'
    rdesc%defined=.true.
    rdesc%n_rbands = 2
    rdesc%bandname(1) = 'HERA1'
    rdesc%bandname(2) = 'HERA2'
    rdesc%rejection(1) = 10
    rdesc%rejection(2) = 10
    rdesc%n_sbands = 1
    rdesc%n_bbands = 1
    rdesc%n_polar = 1
    rdesc%n_backends = 3
    rdesc%iflim(1) = 3500d0
    rdesc%iflim(2) = 4500d0
    rdesc%ifband = rdesc%iflim(2)-rdesc%iflim(1)
    !True frequency limits: Call for Proposal+/-BandWidth/2
    rdesc%rflim(1,1) = 215d3-rdesc%ifband/2
    rdesc%rflim(2,1) = 272d3+rdesc%ifband/2
    rdesc%rflim(1,2) = 215d3-rdesc%ifband/2
    rdesc%rflim(2,2) = 241d3+rdesc%ifband/2
    !LO limits (deduced from observable frequencies)
    rdesc%lolim(1,1) = rdesc%rflim(1,1)+rdesc%iflim(2)
    rdesc%lolim(2,1) = rdesc%rflim(2,1)-rdesc%iflim(2)
    rdesc%lolim(1,2) = rdesc%rflim(1,2)+rdesc%iflim(2)
    rdesc%lolim(2,2) = rdesc%rflim(2,2)-rdesc%iflim(2)
    !basebands
    rdesc%bbname(1) = ''
    rdesc%bblim(1,1) = rdesc%iflim(1)
    rdesc%bblim(2,1) = rdesc%iflim(2)
    !polar
    rdesc%polname = '-'
  else
    call astro_message(seve%e,rname,"NOT IMPLEMENTED")
    error = .true.
    return
  endif
  !
end subroutine astro_def_receiver
!
subroutine astro_receiver_info(rdesc,rname,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>astro_receiver_info
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! display in the terminal main information about the receiver properties
  !-----------------------------------------------------------------------
  type(receiver_desc_t), intent(in) :: rdesc ! receiver parameters
  character(len=*), intent(in)   :: rname
  logical, intent(inout)        :: error
  ! local
  integer(kind=4)       :: i
  character(len=256)    :: mess
  !
  call astro_message(seve%r,rname,'-------------------------------------------------------')
  write (mess,'(a,1x,a,1x,a)') 'Info about',trim(rdesc%name),'Receiver'
  call astro_message(seve%r,rname,mess)
  !
  ! Receiver Bands
  write (mess,'(a,1x,i0,1x,a)') '+',rdesc%n_rbands,'frequency bands:'
  call astro_message(seve%r,rname,mess)
  !
  write (mess,'(a,1x,i0,1x,a)') 'Standard limits:'
  call astro_message(seve%r,rname,mess)
  do i=1,rdesc%n_rbands
    write (mess,'(a,a,a,1x,f7.3,1x,a,1x,f7.3,1x,a)') & 
           '  ',trim(rdesc%bandname(i)),': RF=',rdesc%rfcall(1,i)*ghzpermhz,'-',rdesc%rfcall(2,i)*ghzpermhz,'GHz'
    call astro_message(seve%r,rname,mess)
  enddo
  write (mess,'(a,1x,i0,1x,a)') 'Extended limits (not guaranteed):'
  call astro_message(seve%r,rname,mess)
  do i=1,rdesc%n_rbands
    write (mess,'(a,a,a,1x,f7.3,1x,a,1x,f7.3,1x,a)') & 
           '  ',trim(rdesc%bandname(i)),': RF=',rdesc%rflim(1,i)*ghzpermhz,'-',rdesc%rflim(2,i)*ghzpermhz,'GHz'
    call astro_message(seve%r,rname,mess)
  enddo
  !
  ! IF
  write (mess,'(a,1x,f8.1,a,1x,f8.1,1x,a)') &
         '+ IF=',rdesc%iflim(1),'-',rdesc%iflim(2),'MHz'
  call astro_message(seve%r,rname,mess)
  write (mess,'(a,1x,f8.1,1x,a)') &
         '  Bandwidth=',rdesc%iflim(2)-rdesc%iflim(1),'MHz'
  call astro_message(seve%r,rname,mess)
  !
  write (mess,'(a,1x,i0,1x,a)') '+',rdesc%n_sbands,'sidebands'
  call astro_message(seve%r,rname,mess)
  if (rdesc%n_polar.eq.1) then
    write (mess,'(a,1x,i0,1x,a)') '+',rdesc%n_polar,'polarization:',rdesc%polname(1)
  else if (rdesc%n_polar.eq.2) then
    write (mess,'(a,1x,i0,1x,a,1x,a,a,1x,a)')  &
           '+',rdesc%n_polar,'polarizations:',trim(rdesc%polname(1)),',',trim(rdesc%polname(2))
  else
    call astro_message(seve%e,rname,'What kind of receiver is this (polars) ?')
    error=.true.
    return
  endif
  call astro_message(seve%r,rname,mess)
  call astro_message(seve%r,rname,'-------------------------------------------------------')
  !
end subroutine astro_receiver_info
!
subroutine astro_def_recsource(rname,rdesc,rsou,error)
  use gbl_message
  use gbl_constant
  use gkernel_interfaces
  use astro_interfaces, except_this=>astro_def_recsource
  use ast_astro
  use astro_types
  use phys_const
  !-----------------------------------------------------------------------
  ! @ private
  ! fill in the receiver_source type according to current source parameters
  !-----------------------------------------------------------------------
  character(len=*), intent(in)  :: rname
  type(receiver_desc_t), intent(inout) :: rdesc ! receiver desc parameters
  type(receiver_source_t), intent(inout) :: rsou ! receiver source parameters
  logical, intent(inout)        :: error
  ! local
  integer(kind=4)       :: i,j,nc
  real(kind=8)          :: vsys,vlsr,z,vmin,vmax,eshift,eshiftmin,eshiftmax,jutc_source
  !
  !get current source info and Set source dependant values
  if (soukind.eq.soukind_none) then
    ! No source defined
    vsys = 0d0
    vlsr = 0d0
    z = 0d0
    rsou%name = ''
  else
    call sic_get_char('ASTRO%SOURCE%NAME',rsou%name,nc,error)
    if (error) then
      call astro_message(seve%e,rname,'Error reading source information')
      return
    endif
    call sic_get_dble('ASTRO%SOURCE%V_SOU_OBS',vsys,error)
    if (error) then
      call astro_message(seve%e,rname,'Error reading source information')
      return
    endif
    call sic_get_dble('ASTRO%SOURCE%V_SOU_LSR',vlsr,error)
    if (error) then
      call astro_message(seve%e,rname,'Error reading source information')
      return
    endif
    call sic_get_dble('ASTRO%SOURCE%REDSHIFT',z,error)
    if (error) then
      call astro_message(seve%e,rname,'Error reading source information')
      return
    endif
    call sic_get_char('ASTRO%SOURCE%IN%VTYPE',rsou%invtype,nc,error)
    if (error) then
      call astro_message(seve%e,rname,'Error reading veltype (LSR or RED) information')
      return
    endif
    if (soukind.eq.soukind_full) then
      call sic_get_dble('ASTRO%SOURCE%JUTC',jutc_source,error)
      if (error) then
        call astro_message(seve%e,rname,'Error reading source/time information')
        return
      endif
      call sic_get_char('ASTRO%SOURCE%IN%COORD',rsou%coord,nc,error)
      if (error) then
        call astro_message(seve%e,rname,'Error reading source information')
        return
      endif
      rsou%eq = equinox_null
      call sic_get_real('ASTRO%SOURCE%IN%EQUINOX',rsou%eq,error)
      if (error) then
        call astro_message(seve%e,rname,'Error reading source information')
        return
      endif
      call sic_get_char('ASTRO%SOURCE%IN%LAMBDA',rsou%lambda,nc,error)
      if (error) then
        call astro_message(seve%e,rname,'Error reading source information')
        return
      endif
      call sic_get_char('ASTRO%SOURCE%IN%BETA',rsou%beta,nc,error)
      if (error) then
        call astro_message(seve%e,rname,'Error reading source information')
        return
      endif
    endif
  endif
  rsou%sourcetype = soukind
  rsou%jutc=jutc_source
  rsou%z = z
  rsou%vdop = vsys
  rsou%vlsr = vlsr
  rsou%lsrdefined = .true.
  rsou%zshift = 1d0/(1d0+rsou%z)    ! from rest to LSR frame
  rsou%dopshift = (1d0-vsys/clight_kms)   ! from rest to topo frame
  rsou%lsrshift = (1d0-vlsr/clight_kms)   ! from rest to LSR frame
  eshift = (1d0-(vsys-vlsr)/clight_kms)         ! from LSR frame to topo frame
  if (rdesc%redshift.and.rsou%z.ne.0) then
    rsou%dopshift = rsou%zshift+eshift-1d0 ! combine earth and z
    rsou%lsrshift = rsou%zshift
  endif
  ! Compute doppler min and max
  if (soukind.eq.soukind_full) then
    call source_minmax_doppler(rsou,vmin,vmax,error)   
    if (error) return
    rsou%vdopmin=vmin
    rsou%vdopmax=vmax
    rsou%dopmin=(1.0d0-vmin/clight_kms)
    rsou%dopmax=(1.0d0-vmax/clight_kms)
    if (rdesc%redshift.and.rsou%z.ne.0) then
      eshiftmin=(1d0-(vmin-vlsr)/clight_kms)
      eshiftmax=(1d0-(vmax-vlsr)/clight_kms)
      rsou%dopmin = rsou%zshift+eshiftmin-1d0
      rsou%dopmax = rsou%zshift+eshiftmax-1d0
    endif
  endif
  !Set rest limits of the receiver
  if (rsou%vlsr.ne.0.and.rsou%z.ne.0) then
    call astro_message(seve%e,rname,'A source should not have a redshift AND a LSR velocity')
    error = .true.
    return
  endif
  do i = 1,rdesc%n_rbands
    do j=1,2
      rdesc%restlim(j,i) = rdesc%rflim(j,i)/rsou%dopshift
      rdesc%restcall(j,i) = rdesc%rfcall(j,i)/rsou%dopshift
    enddo
  enddo
  rsou%defined = .true.
  !
end subroutine astro_def_recsource
!
subroutine source_minmax_doppler(rsou,vmin,vmax,error)
  use gbl_message
  use ast_astro
  use ast_constant
  use astro_types
  use astro_interfaces, except_this=>source_minmax_doppler
  !-----------------------------------------------------------------------
  ! @ private
  ! get min and max doppler for a given source
  !
  !-----------------------------------------------------------------------
  type(receiver_source_t), intent(inout) :: rsou
  real(kind=8), intent(inout) :: vmin
  real(kind=8), intent(inout) :: vmax
  logical, intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DOPMINMAX'
  real(kind=8) :: savetime,savedut1,savedutc,tt,tdutc,tdut1
  integer(kind=4),parameter :: ntimes = 732 ! 2 yrs
  integer(kind=4),parameter :: tstep = 10 ! 
  integer(kind=4) :: it,lpar
  real(kind=8) :: s_2(2),s_3(3),dop,lsr,svec(3),x_0(3),parang,bb,ll,vv
  !
  !\translate coordinates to radians
  lpar=len_trim(rsou%lambda)
  call sic_sexa(rsou%lambda,lpar,ll,error)
  if (error) return
  if (rsou%coord.eq.'EQ' .or. rsou%coord.eq.'DA') then
    ll = ll*pi/12.0d0
  else
    ll = ll*pi/180.0d0
  endif
  lpar=len_trim(rsou%beta)
  call sic_sexa(rsou%beta,lpar,bb,error)
  if (error) return
  bb = bb*pi/180.0d0
  !
  !Save current parameters before looping on times
  savetime=jnow_utc
  savedut1=d_ut1
  savedutc=d_tdt
  !Define times to be used
  tdutc=0
  tdut1=0
  vmin=1e6
  vmax=-1e6
  !loop on times and get min max
  do it=1,ntimes,tstep
    tt=savetime+it-ntimes/2+tstep/2
    call do_astro_time(tt,tdut1,tdutc,error)
    if (error) return
!     call object(rsou%name,rsou%coord,rsou%eq,rsou%lambda,rsou%beta, &
!       .false.,'',.false.,s_3,.false.,0,error)
    call do_object(rsou%coord,rsou%eq,ll,bb, &
                   s_2,s_3,dop,lsr,svec,x_0,parang,error)
    if (error) return
    vv=dop+lsr+rsou%vlsr
    if (vv.gt.vmax) then
      vmax=vv
    endif
    if (vv.lt.vmin) then
      vmin=vv
    endif    
  enddo
  !Back to previous situation
  call do_astro_time(savetime,savedut1,savedutc,error)
  if (error) return
  call do_object(rsou%coord,rsou%eq,ll,bb, &
                 s_2,s_3,dop,lsr,svec,x_0,parang,error)
  if (error) return
  !
end subroutine source_minmax_doppler
!
subroutine astro_tune_receiver(rname,rdesc,rsou,rcomm,rtune,error)
  use gildas_def
  use gkernel_interfaces
  use gbl_message
  use gbl_ansicodes
  use astro_interfaces, except_this=>astro_tune_receiver
  use astro_types
  use phys_const
  use ast_astro
  !-----------------------------------------------------------------------
  ! @ private
  ! Check input and Set tuning parameters (single band)
  !-----------------------------------------------------------------------
  character(len=*), intent(in)   :: rname
  type(receiver_desc_t), intent(in)  :: rdesc ! receiver description 
  type(receiver_source_t), intent(in)  :: rsou ! source velocity and freq shifts 
  type(receiver_comm_t), intent(inout)  :: rcomm ! receiver command
  type(receiver_tune_t), intent(inout)  :: rtune ! receiver tuning
  logical, intent(inout)                :: error
  !local
  real(kind=8)          :: frf
  character(len=4)      ::  key
  character(len=200)    :: mess
  real(kind=8), parameter :: loeps=1d-3 ! epsilon of 1kHz when checking band edges
  !
  ! Implement user input
  rtune%frest = rcomm%frest*mhzperghz
  rtune%name = rcomm%name
  rtune%fcent = rcomm%fcent
  if (rcomm%fcent.lt.rdesc%iflim(1).or.rcomm%fcent.gt.rdesc%iflim(2)) then
    error = .true.
    write (mess,'(a,f0.0,a,f0.0,a)') 'IF frequency should be between ',   &
    rdesc%iflim(1),' and ',rdesc%iflim(2),' MHz'
    call astro_message(seve%e,rname,mess)
    return
  endif
  call sic_upper(rcomm%sideband)
  call sic_ambigs(rname,rcomm%sideband,key,rtune%sb_code,sideband,m_sideband,error)
  if (error) return
  rtune%frf = rtune%frest*rsou%dopshift   !MHz
  rtune%flsr = rtune%frest*rsou%lsrshift   !MHz
  !
  !Check rf frequency
  call rec_find_band(rname,rdesc,rtune%frf,rtune%iband,error)
  if (error) return
  !
  !Check LSB/USB
  if ((rtune%frf.lt.rdesc%lolim(1,rtune%iband)  .and.                 &
       sideband(rtune%sb_code).eq.'USB')               .or.                  &
      (rtune%frf.gt.rdesc%lolim(2,rtune%iband)  .and.                 &
       sideband(rtune%sb_code).eq.'LSB')             ) then
    write (mess,'(a,f0.3,a,a)') 'Impossible to tune ',rtune%frf,' in ',sideband(rtune%sb_code)
    call astro_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !Compute LO1 (including doppler)
  rtune%flo1 = rtune%frf-sb_sign(rtune%sb_code)*rtune%fcent
  frf=rtune%frest*rsou%dopmin
  rtune%flo1dmin = frf-sb_sign(rtune%sb_code)*rtune%fcent
  frf=rtune%frest*rsou%dopmax
  rtune%flo1dmax = frf-sb_sign(rtune%sb_code)*rtune%fcent
  rtune%flotune = rtune%flsr-sb_sign(rtune%sb_code)*rtune%fcent
  !check that LO is in the possible range (hard limits)
  if (rtune%flotune.lt.rdesc%lolim(1,rtune%iband)-loeps.or. &
      rtune%flotune.gt.rdesc%lolim(2,rtune%iband)+loeps) then
    write (mess,'(a,1x,f0.0,1x,a)') 'LO',rtune%flotune, &
                'MHz is out of range, you may try a different IF center frequency'
    call astro_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  write (rtune%label,1010) trim(rdesc%name),                       &
          trim(rdesc%bandname(rtune%iband)),'BAND - Tuning:',trim(rtune%name),         &
          rtune%frest,sideband(rtune%sb_code),rtune%fcent
  !
  write(mess,'(a,f10.5,a)') ' FRF     = ',rtune%frf*ghzpermhz,' GHz'
  call astro_message(seve%i,rname,mess)
  write(mess,'(a,f10.5,a)') ' FLO1    = ',rtune%flo1*ghzpermhz,' GHz'
  call astro_message(seve%i,rname,mess)
  ! check that LO is in the call for proposal range to issue a warning if needed
  if (rtune%flotune.lt.rdesc%locall(1,rtune%iband)-loeps.or. &
      rtune%flotune.gt.rdesc%locall(2,rtune%iband)+loeps) then
    write (mess,'(a,1x,f0.0,1x,a)') 'LO',rtune%flo1, &
                'MHz is out of recommended range. This tuning might not be possible'
    call astro_message(seve%w,rname,mess)
    rtune%outlo = .true.
  else
    rtune%outlo = .false.
  endif
  !
  1010  format(a,1x,a,1x,a,1x,a,1x,f0.3,1x,a,1x,f0.3)
  !
end subroutine astro_tune_receiver
!
subroutine rec_find_band(rname,rdesc,frf,iband,error)
  use gbl_message
  use astro_interfaces, except_this=>rec_find_band
  use astro_types  
  !-----------------------------------------------------------------------
  ! @ private
  ! Find the receiver band to tune based on input frequency
  !-----------------------------------------------------------------------
  character(len=*), intent(in)          :: rname
  type(receiver_desc_t), intent(in)     :: rdesc
  real(kind=8), intent(in)              :: frf
  integer(kind=4), intent(inout)        :: iband
  logical, intent(inout)                :: error
  ! Local
  integer(kind=4)       :: i
  character(len=256)    :: mess
  !
  iband=0
  do i = 1,rdesc%n_rbands
    if (frf.ge.rdesc%rflim(1,i)-rdesc%lotol.and.frf.le.rdesc%rflim(2,i)+rdesc%lotol) then
      iband = i
      write (mess,'(a,a,a,a,a)') 'Selecting the ',trim(rdesc%bandname(iband)),  &
     ' band of the ',trim(rdesc%name),' receiver'
      call astro_message(seve%i,rname,mess)
      exit
    endif
  enddo
  if (iband.eq.0) then
    error = .true.
    write (mess,'(a,f0.3,a,a)') 'RF Frequency ',frf,'MHz is out of range for ',rdesc%name
    call astro_message(seve%e,rname,mess)
    return
  endif
  !
end subroutine rec_find_band
!
subroutine rec_reset_box(fbox,error)
  use astro_interfaces, except_this=>rec_reset_box
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! reset the defaults values for frequency_box_t that will be defined 
  ! (in order to use safely defaults values)
  !-----------------------------------------------------------------------
  type(frequency_box_t), intent(inout)  :: fbox
  logical, intent(inout)                :: error
  !
  fbox%sb_code = 0
  fbox%iband = 0
  !
  fbox%phys%xmin = 0
  fbox%phys%xmax = 0
  fbox%phys%ymin = 0
  fbox%phys%ymax = 0
  fbox%phys%sx = 0
  fbox%phys%sy = 0
  !
  fbox%rest%xmin = 0
  fbox%rest%xmax = 0
  fbox%rest%ymin = 0
  fbox%rest%ymax = 5
  fbox%rest%name = ''
  fbox%rest%defined=.false.
  !
  fbox%rf%xmin = 0
  fbox%rf%xmax = 0
  fbox%rf%ymin = 0
  fbox%rf%ymax = 5
  fbox%rf%name = ''
  fbox%rf%defined=.false.
  !
  fbox%imrest%xmin = 0
  fbox%imrest%xmax = 0
  fbox%imrest%ymin = 0
  fbox%imrest%ymax = 5
  fbox%imrest%name = ''
  fbox%imrest%defined=.false.
  !
  fbox%imrf%xmin = 0
  fbox%imrf%xmax = 0
  fbox%imrf%ymin = 0
  fbox%imrf%ymax = 5
  fbox%imrf%name = ''
  fbox%imrf%defined=.false.
  !
  fbox%if1%xmin = 0
  fbox%if1%xmax = 0
  fbox%if1%ymin = 0
  fbox%if1%ymax = 5
  fbox%if1%name = ''
  fbox%if1%defined=.false.
  !
  fbox%if2%xmin = 0
  fbox%if2%xmax = 0
  fbox%if2%ymin = 0
  fbox%if2%ymax = 5
  fbox%if2%name = ''
  fbox%if2%defined=.false.
  !
  fbox%chunks%xmin = 0
  fbox%chunks%xmax = 0
  fbox%chunks%ymin = 0
  fbox%chunks%ymax = 5
  fbox%chunks%name = ''
  fbox%chunks%defined=.false.
  !
end subroutine rec_reset_box
!
subroutine rec_def_legendbox(rname,lbox,cplot,error)
  use gbl_message
  use astro_interfaces, except_this=>rec_def_legendbox
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Prepare the box to display messages around the various plotted boxes
  !-----------------------------------------------------------------------
  character(len=*), intent(in)                  :: rname ! name
  type(frequency_box_phys_t), intent(inout)     :: lbox
  type(current_boxes_t), intent(inout)          :: cplot
  logical, intent(inout)                        :: error
  !local
  integer(kind=4) :: i
  real(kind=4) :: bxmin,bxmax,bymin,bymax
  !
  bxmin=1e6
  bymin=1e6
  bxmax=-1
  bymax=-1
  do i=1,cplot%nbox
    if (cplot%box(i)%phys%xmin.lt.bxmin) bxmin=cplot%box(i)%phys%xmin
    if (cplot%box(i)%phys%xmax.gt.bxmax) bxmax=cplot%box(i)%phys%xmax
    if (cplot%box(i)%phys%ymin.lt.bymin) bymin=cplot%box(i)%phys%ymin
    if (cplot%box(i)%phys%ymax.gt.bymax) bymax=cplot%box(i)%phys%ymax    
  enddo
  lbox%xmin=bxmin
  lbox%xmax=bxmax
  lbox%ymin=bymin
  lbox%ymax=bymax
  lbox%sy=bxmax-bxmin
  lbox%sy=bymax-bymin
  !
end subroutine rec_def_legendbox
!
subroutine rec_def_fbox(f1,f2,ftype,fbox,rdesc,rsou,flo1,error)
  use gbl_message
  use astro_interfaces, except_this=>rec_def_fbox
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Prepare the box for plotting ad hoc frequency coverage [input can be if1,rf,rest]
  !-----------------------------------------------------------------------
  real(kind=8), intent(in)                      :: f1,f2 !frequency limits (GHz)
  character(len=*), intent(in)                  :: ftype ! type of frequency limits
  type(frequency_box_t), intent(inout)          :: fbox ! box parameters (physical and user)
  type(receiver_desc_t), intent(in)             :: rdesc ! receiver parameters
  type(receiver_source_t), intent(in)           :: rsou ! receiver source parameters
  real(kind=8), intent(in)                      :: flo1
  logical, intent(inout)                        :: error
  !local
  character(len=*), parameter   :: rname='PLOT'
  real(kind=8)                  :: sk1,sk2
  !
  if (trim(ftype).eq.'REST') then
    call resttorf(rsou%dopshift,f1,sk1,error)
    if (error) return
    call resttorf(rsou%dopshift,f2,sk2,error)
    if (error) return
  else if (trim(ftype).eq.'RF') then
    sk1 = f1
    sk2 = f2
  else if (trim(ftype).eq.'IF1') then
    if (flo1.le.1d0) then
      call astro_message(seve%e,rname,'IF1 cannot be used to define a not tuned band')
      error=.true.
      return
    endif
    call if1torf(flo1,f1,fbox%sb_code,sk1,error)
    if (error) return
    call if1torf(flo1,f2,fbox%sb_code,sk2,error)
    if (error) return
  else
    call astro_message(seve%e,rname,'This frequency type is not supported')
    error = .true.
    return
  endif
  !
  call rec_def_fbox_rf(sk1,sk2,fbox,rdesc,rsou,flo1,error)
  if (error) return
  !
end subroutine rec_def_fbox
!
subroutine rec_def_fbox_rf(frf1,frf2,fbox,rdesc,rsou,flo1,error)
  use gbl_message
  use astro_interfaces, except_this=>rec_def_fbox_rf
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Prepare the box for plotting ad hoc frequency coverage [input is RF]
  !-----------------------------------------------------------------------
  real(kind=8), intent(in)                      :: frf1,frf2 ! RF sky frequency limits (GHz)
  type(frequency_box_t), intent(inout)          :: fbox ! box parameters (physical and user)
  type(receiver_desc_t), intent(in)             :: rdesc ! receiver parameters
  type(receiver_source_t), intent(in)           :: rsou ! receiver source parameters
  real(kind=8), intent(in)                      :: flo1
  logical, intent(inout)                        :: error
  !local
  character(len=*), parameter   :: rname='PLOT'
  real(kind=8)                  :: sk1,sk2
  integer(kind=4)               :: imisb
  !
  ! Sky frequency decides the orientation of the axis (rf increases from left to right)
  fbox%rf%xmin=min(frf1,frf2)
  fbox%rf%xmax=max(frf1,frf2)
  fbox%rf%name = 'RF Frequency'
  fbox%rf%defined=.true.
  !
  !Rest axis
  call rftorest(rsou%dopshift,fbox%rf%xmin,fbox%rest%xmin,error)
  if (error) return
  call rftorest(rsou%dopshift,fbox%rf%xmax,fbox%rest%xmax,error)
  if (error) return
  fbox%rest%name = 'Rest Frequency'
  fbox%rest%defined=.true.
  !
  !LSR axis
  call resttolsr(rsou%lsrshift,fbox%rest%xmin,fbox%lsr%xmin,error)
  if (error) return
  call resttolsr(rsou%lsrshift,fbox%rest%xmax,fbox%lsr%xmax,error)
  if (error) return
  fbox%lsr%name = 'LSR Frequency'
  fbox%lsr%defined=.true.
  !
  ! IF1 axis limits
  if (fbox%sb_code.ne.0) then
    call rftoif1(flo1,fbox%rf%xmin,fbox%sb_code,fbox%if1%xmin,error)
    if (error) return
    call rftoif1(flo1,fbox%rf%xmax,fbox%sb_code,fbox%if1%xmax,error)
    if (error) return
    fbox%if1%name = 'Intermediate Frequency IF1'
    fbox%if1%defined=.true.
  endif
  !
  ! IF2 axis limits
  if (fbox%bb_code.ne.0.and.rdesc%flo2.ne.0) then
    call if1toif2(rdesc%flo2,fbox%if1%xmin,fbox%bb_code,fbox%if2%xmin,error)
    if (error) return
    call if1toif2(rdesc%flo2,fbox%if1%xmax,fbox%bb_code,fbox%if2%xmax,error)
    if (error) return
    fbox%if2%name = 'Intermediate Frequency IF2'
    fbox%if2%defined=.true.
  endif
  !
  ! Image axis
  !===========
  if (fbox%sb_code.eq.0) then
!     call astro_message(seve%i,rname,'Image frequencies cannot be defined for the current box')
    return
  else if (fbox%sb_code.eq.1) then
    imisb = 2
  else if (fbox%sb_code.eq.2) then
    imisb = 1
  else
    error = .true.
    call astro_message(seve%e,rname,'There is something wrong with the image side band determination')
    return
  endif
  !
  ! RF
  call if1torf(flo1,fbox%if1%xmin,imisb,sk1,error)
  if (error) return
  call if1torf(flo1,fbox%if1%xmax,imisb,sk2,error)
  if (error) return
  fbox%imrf%xmin = sk1
  fbox%imrf%xmax = sk2
  fbox%imrf%name = 'IMAGE RF frequency'
  fbox%imrf%defined=.true.
  !
  ! Rest
  call rftorest(rsou%dopshift,fbox%imrf%xmin,fbox%imrest%xmin,error)
  if (error) return
  call rftorest(rsou%dopshift,fbox%imrf%xmax,fbox%imrest%xmax,error)
  if (error) return
  fbox%imrest%name = 'IMAGE Rest frequency'
  fbox%imrest%defined=.true.
  !
  ! LSR
  call resttolsr(rsou%lsrshift,fbox%imrest%xmin,fbox%imlsr%xmin,error)
  if (error) return
  call resttolsr(rsou%lsrshift,fbox%imrest%xmax,fbox%imlsr%xmax,error)
  if (error) return
  fbox%imlsr%name = 'IMAGE LSR frequency'
  fbox%imlsr%defined=.true.
  !
end subroutine rec_def_fbox_rf
!
subroutine rec_set_limits(rname,cplot,limtype,error)
  use gbl_message
  use astro_interfaces, except_this=>rec_set_limits
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! set the limit of the chosen box
  !-----------------------------------------------------------------------
  character(len=*), intent(in)          :: rname
  type(current_boxes_t), intent(in)     :: cplot ! all boxes
  character(len=*), intent(in)          :: limtype
  logical, intent(inout)                :: error
  !local
  character(len=256) :: comm
  integer(kind=4) :: ib
  !
  write (comm,'(a)') 'CHANGE DIRECTORY'
  call gr_execl(comm)
  do ib=1,cplot%nbox
    write (comm,'(a,i0)') 'CHANGE DIRECTORY <GREG<BOX',ib
    call gr_execl(comm)
    call rec_set_limits_box(rname,cplot%box(ib),limtype,error)
    if (error) return
  enddo
  write (comm,'(a)') 'CHANGE DIRECTORY'
  call gr_execl(comm)
  !
end subroutine rec_set_limits
!
subroutine rec_set_limits_box(rname,fbox,limtype,error)
  use gbl_message
  use astro_interfaces, except_this=>rec_set_limits_box
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! set the limit of the chosen box
  !-----------------------------------------------------------------------
  character(len=*), intent(in)          :: rname
  type(frequency_box_t), intent(in)     :: fbox ! box parameters (physical and user)
  character(len=*), intent(in)          :: limtype
  logical, intent(inout)                :: error
  !local
  character(len=256) :: comm
  !
  select case (limtype)
  case ('REST')
    write (comm,'(a,1x,f0.3,1x,f0.3,1x,f0.3,1x,f0.3)') &
          'LIMITS',fbox%rest%xmin,fbox%rest%xmax,fbox%rest%ymin,fbox%rest%ymax
  case ('RF')
    write (comm,'(a,1x,f0.3,1x,f0.3,1x,f0.3,1x,f0.3)') &
          'LIMITS',fbox%rf%xmin,fbox%rf%xmax,fbox%rf%ymin,fbox%rf%ymax
  case ('LSR')
    write (comm,'(a,1x,f0.3,1x,f0.3,1x,f0.3,1x,f0.3)') &
          'LIMITS',fbox%lsr%xmin,fbox%lsr%xmax,fbox%lsr%ymin,fbox%lsr%ymax
  case default
    call astro_message(seve%e,rname,'Problem with frequency axis when setting the limits')
    error=.true.
    return
  end select
  !
  call gr_exec1(comm)
  !
end subroutine rec_set_limits_box
!
subroutine rec_get_itune(rec,ib,it,error)
  use gbl_message
  use astro_interfaces, except_this=>rec_get_itune
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  !find the tuning id of a given band
  !-----------------------------------------------------------------------
  type(receiver_t)                      :: rec
  integer(kind=4), intent(in)           :: ib ! looked for band
  integer(kind=4), intent(out)          :: it ! tuning id of the band
  logical, intent(inout)                :: error
  ! local
  integer(kind=4) :: i
  !
  it=0
  do i=1,rec%n_tunings
    if (rec%tune(i)%iband.eq.ib) then
      it = i
    endif
  enddo
  !
end subroutine rec_get_itune
!
subroutine rec_inputtorest(rname,finput,freqtype,rsou,frest,error)
  use gbl_message
  use astro_interfaces, except_this=>rec_inputtorest
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! For input frequency in any frame (rest,rf,lsr), return the corresponding rest freq
  !-----------------------------------------------------------------------
  character(len=*), intent(in)                      :: rname ! Name of the calling routine
  real(kind=8), intent(in)              :: finput ! Input freq 
  character(len=16)                     :: freqtype ! kind of input frequency
  type(receiver_source_t), intent(in)        :: rsou ! current source parameters
  real(kind=8), intent(inout)           :: frest ! Rest frequency output
  logical, intent(inout)                :: error
  !
  if (freqtype.eq.'REST') then
    ! nothing to do
    frest = finput
  else if (freqtype.eq.'RF') then
    call rftorest(rsou%dopshift,finput,frest,error)
    if (error) return
  else if (freqtype.eq.'LSR') then
    call lsrtorest(rsou%lsrshift,finput,frest,error)
    if (error) return
  else
    call astro_message(seve%e,rname,'Could not understand the frequency type (should be REST, RF or LSR)')
    error=.true.
    return
  endif
  !
end subroutine rec_inputtorest
!
subroutine rec_resttooutput(rname,frest,freqtype,rsou,foutput,error)
  use gbl_message
  use astro_interfaces, except_this=>rec_resttooutput
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! for an input rest freq, returns the output frequency in the current frame
  !-----------------------------------------------------------------------
  character(len=*), intent(in)          :: rname ! name of the calling routine
  real(kind=8), intent(in)              :: frest ! Input rest freq 
  character(len=16)                     :: freqtype ! kind of input frequency
  type(receiver_source_t), intent(in)   :: rsou ! current source parameters
  real(kind=8), intent(inout)           :: foutput ! Rest frequency output
  logical, intent(inout)                :: error
  !
  if (freqtype.eq.'REST') then
    ! nothing to do
    foutput = frest
  else if (freqtype.eq.'RF') then
    call resttorf(rsou%dopshift,frest,foutput,error)
    if (error) return
  else if (freqtype.eq.'LSR') then
    call resttolsr(rsou%lsrshift,frest,foutput,error)
    if (error) return
  else
    call astro_message(seve%e,rname,'Could not understand the frequency type (should be REST, RF or LSR)')
    error=.true.
    return
  endif
  !
end subroutine rec_resttooutput
!
subroutine if1torf(flo,fif,isb,frf,error)
  use astro_interfaces, except_this=>if1torf
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! compute the RF sky frequency (MHz) corresponding to the input IF frequency (MHz)
  !-----------------------------------------------------------------------
  real(kind=8), intent(in)              :: flo ! LO frequency (MHz)
  real(kind=8), intent(in)              :: fif ! IF frequency input (MHz)
  integer(kind=4), intent(in)           :: isb ! considered sideband
  real(kind=8), intent(out)             :: frf ! RF frequency output (MHz)
  logical, intent(inout)                :: error
  !
  frf = (flo+sb_sign(isb)*fif)
  !
end subroutine if1torf
!
subroutine rftoif1(flo,frf,isb,fif,error)
  use astro_interfaces, except_this=>rftoif1
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! compute the RF frequency corresponding to the input IF frequency
  !-----------------------------------------------------------------------
  real(kind=8), intent(in)              :: flo ! LO frequency (MHz)
  real(kind=8), intent(in)              :: frf ! RF frequency input (MHz)
  integer(kind=4), intent(in)           :: isb ! considered sideband
  real(kind=8), intent(out)             :: fif ! IF frequency output (MHz)
  logical, intent(inout)                :: error
  !
  fif = sb_sign(isb)*(frf-flo)
  !
end subroutine rftoif1
!
subroutine if1toif2(flo2,fif1,isb,fif2,error)
  use astro_interfaces, except_this=>if1toif2
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! compute the if2 frequency corresponding to the input IF1 frequency
  !-----------------------------------------------------------------------
  real(kind=8), intent(in)              :: flo2 ! LO2 frequency (MHz)
  real(kind=8), intent(in)              :: fif1 ! frequency input (MHz)
  integer(kind=4), intent(in)           :: isb ! considered baseband (inner = lsb, outer = usb)
  real(kind=8), intent(out)             :: fif2 ! IF frequency output (MHz)
  logical, intent(inout)                :: error
  !
  fif2 = sb_sign(isb)*(fif1-flo2)
  !
end subroutine if1toif2
!
subroutine if2toif1(flo2,fif2,isb,fif1,error)
  use astro_interfaces, except_this=>if2toif1
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! compute the if1 frequency corresponding to the input IF2 frequency
  !-----------------------------------------------------------------------
  real(kind=8), intent(in)              :: flo2 ! LO2 frequency (MHz)
  real(kind=8), intent(in)              :: fif2 ! frequency input (MHz)
  integer(kind=4), intent(in)           :: isb ! considered baseband (inner = lsb, outer = usb)
  real(kind=8), intent(out)             :: fif1 ! IF frequency output (MHz)
  logical, intent(inout)                :: error
  !
  fif1 = flo2+sb_sign(isb)*fif2
  !
end subroutine if2toif1
!
subroutine resttorf(dopshift,frest,frf,error)
  use astro_interfaces, except_this=>resttorf
  !-----------------------------------------------------------------------
  ! @ private
  ! compute the RF sky frequency corresponding to the input rest frequency
  !-----------------------------------------------------------------------
  real(kind=8), intent(in)              :: dopshift ! doppler shift
  real(kind=8), intent(in)              :: frest ! Rest frequency input
  real(kind=8), intent(out)             :: frf ! RF (Sky) frequency output
  logical, intent(inout)                :: error
  !
  frf = frest*dopshift
  !
end subroutine resttorf
!
subroutine rftorest(dopshift,frf,frest,error)
  use astro_interfaces, except_this=>rftorest
  !-----------------------------------------------------------------------
  ! @ private
  ! compute the rest frequency corresponding to the input RF frequency
  !-----------------------------------------------------------------------
  real(kind=8), intent(in)              :: dopshift ! LO frequency
  real(kind=8), intent(in)              :: frf ! RF (Sky) frequency input
  real(kind=8), intent(out)             :: frest ! Rest frequency output
  logical, intent(inout)                :: error
  !
  frest = frf/dopshift
  !
end subroutine rftorest
!
subroutine lsrtorest(lsrshift,flsr,frest,error)
  use astro_interfaces, except_this=>lsrtorest
  !-----------------------------------------------------------------------
  ! @ private
  ! compute the rest frequency corresponding to the input lsr frequency
  !-----------------------------------------------------------------------
  real(kind=8), intent(in)              :: lsrshift ! LO frequency
  real(kind=8), intent(in)              :: flsr ! lsr frequency input
  real(kind=8), intent(out)             :: frest ! Rest frequency output
  logical, intent(inout)                :: error
  !
  frest = flsr/lsrshift
  !
end subroutine lsrtorest
!
subroutine resttolsr(lsrshift,frest,flsr,error)
  use astro_interfaces, except_this=>resttolsr
  !-----------------------------------------------------------------------
  ! @ private
  ! compute the rest frequency corresponding to the input lsr frequency
  !-----------------------------------------------------------------------
  real(kind=8), intent(in)              :: lsrshift ! LO frequency
  real(kind=8), intent(in)              :: frest ! rest frequency input
  real(kind=8), intent(out)             :: flsr ! lsr frequency output
  logical, intent(inout)                :: error
  !
  flsr = frest*lsrshift
  !
end subroutine resttolsr
!
subroutine rec_locate_fbox(fbox,error)
  use gbl_message
  use astro_interfaces, except_this=>rec_locate_fbox
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Draw a box with 2 given axis
  !-----------------------------------------------------------------------
  type(frequency_box_phys_t),intent(in) :: fbox ! structure describing the plotting box and all axis
  logical, intent(inout)           :: error
  ! Local
  character(len=200)    :: comm
  !
  write (comm,'(a,4(1x,f0.3))') 'SET BOX_LOCATION',fbox%xmin,fbox%xmax,fbox%ymin,fbox%ymax
  call gr_exec1(comm)
  !
end subroutine rec_locate_fbox 
!
subroutine rec_draw_fbox(cplot,ibox,drawaxis,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_draw_fbox
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Draw a box with 2 given axis
  !-----------------------------------------------------------------------
  type(current_boxes_t),intent(in) :: cplot ! structure describing the plotting boxes
  integer(kind=4), intent(in)      :: ibox ! current box index
  type(frequency_axis_t), intent(in) :: drawaxis
  logical, intent(inout)           :: error
  ! Local
  character(len=*), parameter   :: rname='PLOT'
  type(frequency_box_user_t) :: plotbox ! frequency axis to plot
  character(len=200)    :: comm
  type(draw_axis_t)     :: paxis
  integer(kind=4)       :: ip
  integer(kind=4), parameter :: mpos=2
  character(len=16) :: axis(mpos) ! name of the axis to plot
  character(len=16) :: abcol ! box color
  character(len=4)      :: position(mpos)
  logical :: drawname
  !
  axis(1)=drawaxis%main
  axis(2)=drawaxis%second
  position(1)='LOW'
  position(2)='UP'
  !
  write (comm,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%defchar
  call gr_exec1(comm)
  !
  !Define box color
  if (cplot%box(ibox)%sb_code.eq.0) then
    abcol=adefcol
  else if (cplot%box(ibox)%sb_code.le.2) then
    abcol=sb_col(cplot%box(ibox)%sb_code)
  else 
    call astro_message(seve%e,rname,'Problem with sideband determination')
    error=.true.
    return
  endif
  call gr_pen(colour=abcol,idash=1,error=error)
  if (error) return
  !
  ! Indicate the sideband when defined
  if (cplot%box(ibox)%sb_code.ne.0) then
    write (comm,'(a,1x,a,1x,a)') 'DRAW TEXT 1 0',trim(sideband(cplot%box(ibox)%sb_code)),'4 90 /CHARACTER 9'
    call gr_exec1(comm)
  endif
  !
  !Draw box
  call gr_exec1('BOX N N N')
  !
  do ip=1,mpos
    plotbox%xmin=-1d0
    paxis%pos=position(ip)
    if (trim(axis(ip)).eq.'IF1') then
      if (cplot%box(ibox)%sb_code.ne.0) then
        plotbox=cplot%box(ibox)%if1
      else
        write (comm,'(a,1x,a,1x,a)') 'Axis',trim(axis(ip)),'cannot be drawn (undefined sideband)'
        call astro_message(seve%w,rname,comm)
        if (position(ip).eq.'LOW') then
          write (comm,'(a,1x,a,1x,a)') 'Displaying Rest frequency instead'
          call astro_message(seve%w,rname,comm)
          plotbox=cplot%box(ibox)%rest
        endif
      endif
    else  if (trim(axis(ip)).eq.'IF2') then
      if (cplot%box(ibox)%sb_code.ne.0.and.cplot%box(ibox)%bb_code.ne.0) then
        plotbox=cplot%box(ibox)%if2
      else
        write (comm,'(a,1x,a,1x,a)') 'Axis',trim(axis(ip)),'cannot be drawn (undefined side/base band)'
        call astro_message(seve%w,rname,comm)
        if (position(ip).eq.'LOW') then
          write (comm,'(a,1x,a,1x,a)') 'Displaying Rest frequency instead'
          call astro_message(seve%w,rname,comm)
          plotbox=cplot%box(ibox)%rest
        endif
      endif
    else  if (trim(axis(ip)).eq.'CHUNKS') then
      if (cplot%box(ibox)%chunks%defined) then
        plotbox=cplot%box(ibox)%chunks
      else
        write (comm,'(a,1x,a,1x,a)') 'Axis',trim(axis(ip)),'cannot be drawn (undefined side/base band)'
        call astro_message(seve%w,rname,comm)
        if (position(ip).eq.'LOW') then
          write (comm,'(a,1x,a,1x,a)') 'Displaying Rest frequency instead'
          call astro_message(seve%w,rname,comm)
          plotbox=cplot%box(ibox)%rest
        endif
      endif
    else if (trim(axis(ip)).eq.'REST') then
      plotbox=cplot%box(ibox)%rest
    else if (trim(axis(ip)).eq.'IMREST') then
      plotbox=cplot%box(ibox)%imrest
    else if (trim(axis(ip)).eq.'RF') then
      plotbox=cplot%box(ibox)%rf
    else if (trim(axis(ip)).eq.'LSR') then
      plotbox=cplot%box(ibox)%lsr
    else if (trim(axis(ip)).eq.'NULL') then
      ! do nothing
    else
      write (comm,'(a,1x,a,1x,a)') 'Axis',trim(axis(ip)),'not yet implemented'
      call astro_message(seve%w,rname,comm)
    endif
    paxis%aboxcol=abcol
    !
    ! To plot axis name when needed
    drawname=.true.
    if (ibox.ne.1.and.ip.eq.2) then
      plotbox%name=''
      drawname=.false.
    endif
    if (ibox.ne.cplot%nbox.and.ip.eq.1) then
      plotbox%name=''
      drawname=.false.
    endif
    !
    ! Draw the axis
    if (plotbox%xmin.ne.-1d0) then
      call rec_draw_axis(plotbox,paxis,drawname,error)
      if (error) return
    endif
  enddo
  !
  call gr_pen(colour=adefcol,idash=1,error=error)
  if (error) return
  !
end subroutine  rec_draw_fbox
!
subroutine rec_draw_molecules(cata,faxis,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_draw_molecules
  use astro_types
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  ! Plot the molecular lines (Rest, GHz)
  !---------------------------------------------------------------------
  type(frequency_box_user_t), intent(in)        :: faxis
  type(plot_molecules_t), intent(in)            :: cata
  logical, intent(inout)                        :: error
  !local
  character(len=*), parameter :: rname='PLOT'
  integer(kind=4)          :: i,kmol
  type(draw_line_t)        :: lmol
  type(draw_rect_t)        :: rmol
  type(draw_gauss_t)        :: gmol
  real(kind=8)             :: afreq,fmin,fmax,yt,mfreq,lmin,lmax
  character(len=20)        :: aname
  character(len=200)       :: comm,molfile
  character(len=1)         :: dummy
  !
  ! Plot only if required
  if (.not.cata%doplot) return
  ! Read lines
  if (.not.sic_query_file(cata%catalog,'data#dir:','.dat',molfile)) then
    call astro_message(seve%e,rname,'line catalog file not found')
    error=.true.
    return
  endif
  kmol = 0
  call read_lines(dummy,kmol,molfile)
  ! set the limits
  write (comm,'(a,1x,f0.3,1x,f0.3,1x,f0.3,1x,f0.3)') 'LIMITS',faxis%xmin,faxis%xmax,faxis%ymin,faxis%ymax
  call gr_exec1(comm)
  !
  call gr_exec1('SET ORIENTATION 50')
  aname = ''
  afreq = 0d0
  fmin = faxis%xmin
  fmax = faxis%xmax
  lmin = (faxis%ymax+faxis%ymin)/2d0
  lmax = faxis%ymax-(faxis%ymax-faxis%ymin)/3d0
  call gr_pen(colour=amolcol,error=error)
  if (error) return
  do i=1, nmol
    mfreq = molfreq(i)*mhzperghz
    if (mfreq.lt.fmin-cata%width.or.mfreq.gt.fmax+cata%width) cycle
    if (cata%width.eq.0d0) then
      lmol%dash = 1
      lmol%col = amolcol
      lmol%ymin = lmin
      lmol%ymax = lmax
      lmol%xmin = mfreq
      lmol%xmax = mfreq
      call rec_draw_line(lmol,faxis,error)
      if (error) return
    else if (cata%profile.eq.'BOXCAR') then
      rmol%dash = 1
      rmol%col = amolcol
      rmol%ymin = lmin
      rmol%ymax = lmax
      rmol%xmin = mfreq-cata%width/2d0
      rmol%xmax = mfreq+cata%width/2d0
      call rec_draw_boxcar(rmol,faxis,error)
      if (error) return
    else if (cata%profile.eq.'GAUSS') then
      gmol%dash = 1
      gmol%col = amolcol
      gmol%ymin = lmin
      gmol%ymax = lmax
      gmol%xcent = mfreq
      gmol%fwhm = cata%width
      call rec_draw_gauss(gmol,faxis,error)
      if (error) return
    endif
    if (molname(i).ne.aname .or. abs(afreq-mfreq).gt.40) then
      call gr_pen(colour=amolcol,error=error)
      if (error) return
      yt = faxis%ymax-(faxis%ymax-faxis%ymin)/6
      write(comm,1000) 'DRAW TEXT',mfreq,yt,'"',trim(molname(i)),'" 5 /USER /CLIP'
          aname = molname(i)
          afreq = mfreq
          call gr_exec1(comm)
    endif
  enddo
  call gr_exec1('SET ORIENTATION 0')
  call gr_pen(colour=adefcol,error=error)
  if (error) return
  !
  1000 format(a,1x,f0.3,1x,f0.3,1x,a,a,a)
  !
end subroutine rec_draw_molecules
!
subroutine rec_draw_linetune(rtune,fbox,error)
  use astro_types
 !-----------------------------------------------------------------------
  ! @ private
  ! Displays the tuned frequency (in rest freq)
  !-----------------------------------------------------------------------
  type(receiver_tune_t), intent(in)        :: rtune !receiver tuning
  type(frequency_box_user_t), intent(in)        :: fbox !axis limits
  logical, intent(inout)        :: error
  ! Local
  type(draw_line_t) :: linetune
  !
  linetune%xmin = rtune%frest
  linetune%xmax = rtune%frest
  linetune%ymin = fbox%ymin
  linetune%ymax = fbox%ymin+(fbox%ymax-fbox%ymin)*ltuneheight
  linetune%col   = altunecol
  if (rtune%outlo) then
    linetune%col   = aconflictcol
  endif
  linetune%dash = 1
  if (linetune%xmin.gt.fbox%xmin.and.linetune%xmin.lt.fbox%xmax) then
    call rec_draw_arrow(linetune,fbox,error)
    if (error) return
  endif
  !
end subroutine rec_draw_linetune
!
subroutine rec_draw_axis(faxis,paxis,drawname,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_draw_axis
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Displays an axis with limits given as input.
  !-----------------------------------------------------------------------
  type(frequency_box_user_t), intent(in)        :: faxis !axis limits
  type(draw_axis_t), intent(in)                 :: paxis !axis physical properties
  logical, intent(in)                           :: drawname
  logical, intent(inout)                        :: error
  !local
  character(len=200)            :: comm
  real(kind=4)                  :: xt,yt
  integer(kind=4)               :: bt ! box point
  real(kind=8), parameter       :: ghzlim = 20d3
  character(len=3)              :: unit
  !
  unit=faxis%unit
  if (faxis%unit.eq.'MHz'.and.faxis%xmin.gt.ghzlim.and.faxis%xmax.gt.ghzlim) then
    write (comm,'(a,1x,f0.3,1x,f0.3,1x,f0.3,1x,f0.3)')                                               &
        'LIMITS',faxis%xmin*ghzpermhz,faxis%xmax*ghzpermhz,faxis%ymin,faxis%ymax
    unit = 'GHz'
  else
    write (comm,'(a,1x,f0.3,1x,f0.3,1x,f0.3,1x,f0.3)')                                               &
        'LIMITS',faxis%xmin,faxis%xmax,faxis%ymin,faxis%ymax
  endif
  call gr_exec1(comm)
  call gr_pen(colour=paxis%aboxcol,idash=1,error=error)
  if (error) return
  write (comm,'(a,a,1x,a,1x,a,1x,a,1x,a)') 'AXIS X',paxis%pos,'/TICK',paxis%tick,'/LABEL',paxis%label
  call gr_exec1(comm)
  xt = 0
  if (paxis%pos.eq.'UP') then
    yt = 3
    bt = 8
  else
    yt = -2
    bt = 2
  endif
  if (drawname) then
    call gr_pen(colour=paxis%alabcol,idash=1,error=error)
    if (error) return
    write (comm,'(a,1x,f0.3,1x,f0.3,1x,a,a,1x,a,a,a,1x,i0)')                                       &
            'DRAW TEXT',xt,yt,'"',trim(faxis%name),'(',trim(unit),')" /CHARACTER',bt
    call gr_exec1(comm)
  endif
  ! back to MHz limits, whatever the plot
  write (comm,'(a,1x,f0.3,1x,f0.3,1x,f0.3,1x,f0.3)')                                               &
        'LIMITS',faxis%xmin,faxis%xmax,faxis%ymin,faxis%ymax
  unit = 'MHz'
  call gr_exec1(comm)
  call gr_pen(colour=adefcol,idash=1,error=error)
  if (error) return
  !
end subroutine rec_draw_axis
!
subroutine rec_draw_rect(rr,faxis,error)
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_draw_rect
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Displays a rectangle through gr4_connect
  !-----------------------------------------------------------------------
  type(draw_rect_t)                      :: rr ! all attributes of the line to be drawn
  type(frequency_box_user_t), intent(in) :: faxis ! axis limits
  logical, intent(inout)         :: error
  !local
  character(len=200)            :: comm
  real(kind=4)                  :: x(5),y(5)
  !
  write (comm,'(a,1x,f0.3,1x,f0.3,1x,f0.3,1x,f0.3)') 'LIMITS',faxis%xmin,faxis%xmax,faxis%ymin,faxis%ymax
  call gr_exec1(comm)
  !
  call gr_pen(colour=rr%col,idash=rr%dash,error=error)
  if (error) return
  x(:) = (/rr%xmin,rr%xmin,rr%xmax,rr%xmax,rr%xmin/)
  y(:) = (/rr%ymin,rr%ymax,rr%ymax,rr%ymin,rr%ymin/)
  !
  call rec_draw_polyline('RECT',5,x,y,error)
  if (error) return
  call gr_pen(colour=adefcol,idash=1,error=error)
  if (error) return
  !
end subroutine rec_draw_rect
!
subroutine rec_draw_boxcar(rr,faxis,error)
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_draw_boxcar
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Displays a boxcar profile through gr4_connect
  !-----------------------------------------------------------------------
  type(draw_rect_t)                      :: rr ! all attributes of the line to be drawn
  type(frequency_box_user_t), intent(in) :: faxis ! axis limits
  logical, intent(inout)         :: error
  !local
  integer(kind=4), parameter :: np=4
  real(kind=4)  :: x(np),y(np)
  character(len=256) :: comm
  !
  write (comm,'(a,1x,f0.3,1x,f0.3,1x,f0.3,1x,f0.3)') 'LIMITS',faxis%xmin,faxis%xmax,faxis%ymin,faxis%ymax
  call gr_exec1(comm)
  !
  call gr_pen(colour=rr%col,idash=rr%dash,error=error)
  if (error) return
  !
  x(:) = (/rr%xmin,rr%xmin,rr%xmax,rr%xmax/)
  y(:) = (/rr%ymin,rr%ymax,rr%ymax,rr%ymin/)
  !
  call rec_draw_polyline('BOXCAR',4,x,y,error)
  if (error) return
  call gr_pen(colour=adefcol,idash=1,error=error)
  if (error) return
  !
end subroutine rec_draw_boxcar
!
subroutine rec_draw_polyline(segname,np,xp,yp,error)
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_draw_polyline
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Connect np points through gr4_connect
  !-----------------------------------------------------------------------
  character(len=*), intent(in)   :: segname
  integer(kind=4),intent(in)     :: np
  real(kind=4),intent(in)        :: xp(np),yp(np)
  logical, intent(inout)         :: error
  !
  call gr_segm(segname,error)
  if (error) return
  call gr4_connect(np,xp,yp,0.,-1.) ! no blanking
  call gr_segm_close(error)
  if (error) return
  !
end subroutine rec_draw_polyline
!
subroutine rec_draw_gauss(gg,faxis,error)
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_draw_gauss
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Displays a gaussian
  !-----------------------------------------------------------------------
  type(draw_gauss_t)                      :: gg ! all attributes of the guassian to be drawn
  type(frequency_box_user_t), intent(in) :: faxis ! axis limits
  logical, intent(inout)         :: error
  !local
  character(len=200)            :: comm
  integer(kind=4)     :: ig
  integer(kind=4), parameter  :: ng=50
  real(kind=4)        :: xg(ng),yg(ng),sig,dx
  !
  write (comm,'(a,1x,f0.3,1x,f0.3,1x,f0.3,1x,f0.3)') 'LIMITS',faxis%xmin,faxis%xmax,faxis%ymin,faxis%ymax
  call gr_exec1(comm)
  !
  sig=gg%fwhm/(2.0*sqrt(2.0*log(2.0)))
  !
  dx=4.0*gg%fwhm/ng
  call gr_pen(colour=gg%col,idash=gg%dash,error=error)
  if (error) return
  do ig=1,ng
    xg(ig)=gg%xcent-2.0*gg%fwhm+dx*(ig-1)
    yg(ig)=gg%ymin+(gg%ymax-gg%ymin)*exp(-(xg(ig)-gg%xcent)**2/(2.0*sig**2.0))
  enddo
  call rec_draw_polyline('GAUSS',ng,xg,yg,error)
  if (error) return
  call gr_pen(colour=adefcol,idash=1,error=error)
  if (error) return
  !
end subroutine rec_draw_gauss
!
subroutine rec_draw_frect(rr,faxis,error)
  use gkernel_types
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_draw_frect
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Displays a filled rectangle.
  !-----------------------------------------------------------------------
  type(draw_rect_t)                      :: rr ! all attributes of the line to be drawn
  type(frequency_box_user_t), intent(in) :: faxis ! axis limits
  logical, intent(inout)         :: error
  !local
  type(polygon_t)               :: poly
  type(polygon_drawing_t)   :: filling
  character(len=200)            :: comm
  !
  write (comm,'(a,1x,f0.3,1x,f0.3,1x,f0.3,1x,f0.3)') 'LIMITS',faxis%xmin,faxis%xmax,faxis%ymin,faxis%ymax
  call gr_exec1(comm)
  poly%ngon = 4 ! rectangle has 4 summits
  poly%xgon(1:5) = (/rr%xmin,rr%xmax,rr%xmax,rr%xmin,rr%xmin/)
  poly%ygon(1:5) = (/rr%ymin,rr%ymin,rr%ymax,rr%ymax,rr%ymin/)
  filling%contoured = .false.
  filling%cpen = 0
  filling%filled = .true.
  call gtv_pencol_name2id('ASTRO',rr%col,filling%fcolor,error)
  if (error) return
  filling%hatched = .false.
  filling%hpen = 0
  filling%hangle = 0
  filling%hsepar = 0
  filling%hphase = 0
  call greg_poly_plot(poly,filling,error)
  if (error) return
  !
end subroutine rec_draw_frect
!
subroutine rec_draw_hrect(rr,faxis,error)
  use gkernel_types
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_draw_hrect
  use astro_types
  
  !-----------------------------------------------------------------------
  ! @ private
  ! Displays a hatched rectangle (color of the current pen)
  !-----------------------------------------------------------------------
  type(draw_rect_t)                      :: rr ! all attributes of the line to be drawn
  type(frequency_box_user_t), intent(in) :: faxis ! axis limits
  logical, intent(inout)         :: error
  !local
  type(polygon_t)               :: poly
  type(polygon_drawing_t)   :: filling
  character(len=200)            :: comm
  !
  write (comm,'(a,1x,f0.3,1x,f0.3,1x,f0.3,1x,f0.3)') 'LIMITS',faxis%xmin,faxis%xmax,faxis%ymin,faxis%ymax
  call gr_exec1(comm)
  poly%ngon = 4 ! rectangle has 4 summits
  poly%xgon(1:5) = (/rr%xmin,rr%xmax,rr%xmax,rr%xmin,rr%xmin/)
  poly%ygon(1:5) = (/rr%ymin,rr%ymin,rr%ymax,rr%ymax,rr%ymin/)
  filling%contoured = .true.
  filling%cpen = 0
  filling%filled = .false.
  filling%fcolor = 0
  filling%hatched = .true.
  filling%hpen = 0
  filling%hangle = 45
  filling%hsepar = 0.3
  filling%hphase = 0
  call greg_poly_plot(poly,filling,error)
  if (error) return
  !
end subroutine rec_draw_hrect
!
subroutine rec_draw_mark(mark,error)
  use gkernel_interfaces
  use gbl_message
  use astro_interfaces, except_this=>rec_draw_mark
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Displays a marker.
  !-----------------------------------------------------------------------
  type(draw_mark_t), intent(in)             :: mark ! all attributes of the marker to be drawn
  logical, intent(inout)         :: error
  !local
  character(len=*), parameter :: rname="DRAW"
  character(len=200)           :: comm
  !
  if ((mark%ref.ne."USER").and. &
     (mark%ref.ne."BOX").and. &
     (mark%ref.ne."PHYSICAL").and. &
     (mark%ref.ne."CHARACTER")) then
    error=.true.
    call astro_message(seve%e,rname,'Problem with coordinate frame')
    return
  endif
  ! Define the marker
  call gr_set_marker(mark%nside,mark%style,mark%s)
  ! Change Color
  call gr_pen(colour=mark%col,idash=1,error=error)
  if (error) return
  ! Draw
  write (comm,'(a,1x,f0.3,1x,f0.3,1x,a,a)') 'DRAW MARKER',mark%x,mark%y,'/',trim(mark%ref)
  if (mark%ref.eq."BOX".or.mark%ref.eq."CHARACTER") then
    write (comm,'(a,1x,i0)') trim(comm),mark%iref
  endif
  if (mark%clip) then
    write (comm,'(a,1x,a)') trim(comm),'/CLIP'
  endif
  !
  call gr_exec1(comm)
  !
end subroutine rec_draw_mark
!
subroutine rec_draw_line(line,faxis,error)
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_draw_line
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Displays a line.
  !-----------------------------------------------------------------------
  type(draw_line_t), intent(in)             :: line ! all attributes of the line to be drawn
  type(frequency_box_user_t), intent(in)    :: faxis ! axis limits
  logical, intent(inout)         :: error
  !local
  character(len=200)            :: comm
  !
  write (comm,'(a,1x,f0.3,1x,f0.3,1x,f0.3,1x,f0.3)') 'LIMITS',faxis%xmin,faxis%xmax,faxis%ymin,faxis%ymax
  call gr_exec1(comm)
  !
  call gr_pen(colour=line%col,idash=line%dash,error=error)
  if (error) return
  write (comm,'(a,1x,f0.3,1x,f0.3,1x,a)') 'DRAW RELOCATE',line%xmin,line%ymin,'/USER'
  call gr_exec1(comm)
  write (comm,'(a,1x,f0.3,1x,f0.3,1x,a)') 'DRAW LINE',line%xmax,line%ymax,'/USER /CLIP'
  call gr_exec1(comm)
  call gr_pen(colour=adefcol,idash=1,error=error)
  if (error) return
  !
end subroutine rec_draw_line
!
subroutine rec_draw_arrow(line,faxis,error)
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_draw_arrow
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Displays a line.
  !-----------------------------------------------------------------------
  type(draw_line_t), intent(in)             :: line ! all attributes of the line to be drawn
  type(frequency_box_user_t), intent(in)    :: faxis ! axis limits
  logical, intent(inout)                    :: error
  !local
  character(len=200)            :: comm
  !
  write (comm,'(a,1x,f0.3,1x,f0.3,1x,f0.3,1x,f0.3)') 'LIMITS',faxis%xmin,faxis%xmax,faxis%ymin,faxis%ymax
  call gr_exec1(comm)
  !
  call gr_pen(colour=line%col,idash=line%dash,error=error)
  if (error) return
  write (comm,'(a,1x,f0.6,1x,f0.6,1x,a)') 'DRAW RELOCATE',line%xmin,line%ymin,'/USER'
  call gr_exec1(comm)
  write (comm,'(a,1x,f0.6,1x,f0.6,1x,a)') 'DRAW ARROW',line%xmax,line%ymax,'/USER /CLIP'
  call gr_exec1(comm)
  call gr_pen(colour=adefcol,idash=1,error=error)
  if (error) return
  !
end subroutine rec_draw_arrow
!
subroutine rec_draw_physrect(fbox,acol,idash,error)
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_draw_physrect
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Displays a rectangle defined by physical coordinates - in the current GTVL directory
  !-----------------------------------------------------------------------
  type(frequency_box_phys_t), intent(in)    :: fbox ! all attributes of the line to be drawn
  character(len=*), intent(in)              :: acol
  integer(kind=4), intent(in)               :: idash
  logical, intent(inout)                    :: error
  !local
  character(len=200)            :: comm
  type(draw_rect_t)             :: rr
  !
  write (comm,'(a,4(1x,f0.3))') 'SET BOX_LOCATION',fbox%xmin,fbox%xmax,fbox%ymin,fbox%ymax
  call gr_exec1(comm)
  call gr_pen(colour=acol,idash=idash,error=error)
  if (error) return
  rr%xmin = -1
  rr%xmax = 1
  rr%ymin = -1
  rr%ymax = 1
  write (comm,'(a,1x,f0.3,1x,f0.3,1x,f0.3,1x,f0.3)') 'LIMITS',rr%xmin,rr%xmax,rr%ymin,rr%ymax
  call gr_exec1(comm)
  write (comm,'(a,1x,f0.3,1x,f0.3,1x,a)') 'DRAW RELOCATE',rr%xmin,rr%ymin,'/USER'
  call gr_exec1(comm)
  write (comm,'(a,1x,f0.3,1x,f0.3,1x,a)') 'DRAW LINE',rr%xmin,rr%ymax,'/USER'
  call gr_exec1(comm)
  write (comm,'(a,1x,f0.3,1x,f0.3,1x,a)') 'DRAW LINE',rr%xmax,rr%ymax,'/USER'
  call gr_exec1(comm)
  write (comm,'(a,1x,f0.3,1x,f0.3,1x,a)') 'DRAW LINE',rr%xmax,rr%ymin,'/USER'
  call gr_exec1(comm)
  write (comm,'(a,1x,f0.3,1x,f0.3,1x,a)') 'DRAW LINE',rr%xmin,rr%ymin,'/USER'
  call gr_exec1(comm)
  !
  call gr_pen(colour=adefcol,idash=1,error=error)
  if (error) return
  !
end subroutine rec_draw_physrect
!
subroutine rec_draw_interbox_line(rname,ibline,cplot,error)
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_draw_interbox_line
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Draw a line from 1 box to another
  !-----------------------------------------------------------------------
  character(len=*), intent(in)          :: rname
  type(draw_interbox_line_t), intent(in)        :: ibline ! all attributes of the line to be drawn
  type(current_boxes_t), intent(in)             :: cplot ! current boxes
  logical, intent(inout)         :: error
  !local
  character(len=200)            :: comm
  !
  call gr_pen(colour=ibline%col,idash=ibline%dash,error=error)
  if (error) return
  !
  ! First point
  call gr_execl('CHANGE DIRECTORY')
  write (comm,'(a,i0)') 'CHANGE DIRECTORY BOX',ibline%ibox(1)
  call gr_execl(comm)
  call rec_set_limits_box(rname,cplot%box(ibline%ibox(1)),ibline%frame(1),error)
  if (error) return
  write (comm,'(a,1x,f0.3,1x,f0.3,1x,a)') 'DRAW RELOCATE',ibline%xpos(1),ibline%ypos(1),'/USER'
  call gr_exec1(comm)
  !
  ! second point
  call gr_execl('CHANGE DIRECTORY')
  write (comm,'(a,i0)') 'CHANGE DIRECTORY BOX',ibline%ibox(2)
  call gr_execl(comm)
  call rec_set_limits_box(rname,cplot%box(ibline%ibox(2)),ibline%frame(2),error)
  if (error) return
  write (comm,'(a,1x,f0.3,1x,f0.3,1x,a)') 'DRAW LINE',ibline%xpos(2),ibline%ypos(2),'/USER'
  call gr_exec1(comm)
  !
  call gr_execl('CHANGE DIRECTORY')
  !
  call gr_pen(colour=adefcol,idash=1,error=error)
  if (error) return
  !
end subroutine rec_draw_interbox_line
!
subroutine rec_display_error(mess,error)
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_display_error
  use astro_types ! for colors
  !-----------------------------------------------------------------------
  ! @ private
  ! Clear the plot and display an error message
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: mess
  logical, intent(inout)        :: error
  !local
  real(kind=8)  :: px,py
  character(len=200) :: comm
  !
  ! get page info
  call sic_get_dble('page_x',px,error)
  if (error) return
  call sic_get_dble('page_y',py,error)
  if (error) return
  !
  call gr_execl('CHANGE DIRECTORY')
  call gr_execl('CLEAR DIRECTORY')
  call gr_exec1('SET CHARACTER 0.6')
  write (comm,'(a,4(1x,f0.3))') 'SET BOX_LOCATION',0d0,px,0d0,py
  call gr_exec1(comm)
  call gr_pen(colour=aconflictcol,idash=1,error=error)
  if (error) return
  write (comm,'(a,a,a)') 'DRAW TEXT 0 -1 "',mess,'" /CHARACTER 5'
  call gr_exec1(comm)
  call gr_pen(colour=adefcol,idash=1,error=error)
  if (error) return
  !
end subroutine rec_display_error
!
subroutine rec_check_doppler(rsou,doredshift,dopchanged,error)
  use gildas_def
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_check_doppler
  use astro_types
  use phys_const
  !-----------------------------------------------------------------------
  ! @ private
  ! checks that astro%source doppler did not change wrt tuning 
  ! (i.e. source command used between tuning and other action (zoom,backend))
  !-----------------------------------------------------------------------
  type(receiver_source_t), intent(in) :: rsou
  logical, intent(in)        :: doredshift  
  logical, intent(out)        :: dopchanged  
  logical, intent(inout)        :: error
  ! local
  real(kind=8) :: currentdop,vsys,vlsr,redsh,eshift,zshift
  !
  dopchanged = .false.
  !get current source values
  if (sic_varexist("ASTRO%SOURCE")) then
    call sic_get_dble('ASTRO%SOURCE%V_SOU_OBS',vsys,error)
    call sic_get_dble('ASTRO%SOURCE%V_SOU_LSR',vlsr,error)
    call sic_get_dble('ASTRO%SOURCE%REDSHIFT',redsh,error)
  else
    vlsr = 0d0
    vsys = 0d0
    redsh=0d0
  endif
  currentdop = (1.0d0-vsys/clight_kms)
  eshift = (1d0-(vsys-vlsr)/clight_kms)         ! from LSR frame to topo frame
  if (doredshift.and.redsh.ne.0) then
    ! when a redshift is given, we set the lsr velocity to 0 but we keep the earth doppler
    zshift = (1d0/(1d0+redsh))
    currentdop = zshift+eshift-1d0 
  endif
  !
  !compare to tuning 
  if (abs(currentdop-rsou%dopshift).gt.toldoppler.or.abs(redsh-rsou%z).gt.toldoppler) then
    dopchanged = .true.
  endif
  !
end subroutine  rec_check_doppler
!
subroutine rec_draw_source(rname,rsou,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>rec_draw_source
  use ast_astro
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Displays the current doppler value above the upper right corner of the current box
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(receiver_source_t), intent(in)   :: rsou
  logical, intent(inout)   :: error
  ! Local
  character(len=200)    :: comm
  character(len=22) :: datechain
  character(len=sourcename_length) :: lname
  real(kind=4) :: cw, nw
  real(kind=4), parameter :: dw=1.5e-2
  !
  if (rsou%sourcetype.eq.soukind_none) then
    ! No source
    lname='NO SOURCE'
  else
    lname=trim(rsou%name)
  endif
!     write (comm,'(a)')                               &
!                 'DRAW TEXT 0 5 "NO SOURCE" 4 0 /CHARACTER 9'
!     call gr_exec1(comm)
!     write (comm,'(a,1x,f0.1,1x,a)')                           &
!                'DRAW TEXT 0 3 "V\\dDop\\u =',                 &
!                 rsou%vdop,'km s\\u-1\\d" 4 0 /CHARACTER 9'
!     call gr_exec1(comm)
!     return
  !
  ! Source NAME (BoldFace)
  call inqwei(cw)
  nw=cw+dw
  call gr_pen(weight=nw,error=error)
  if (error) return
  write (comm,'(a,1x,a,1x,a)')                               &
              'DRAW TEXT 0 5 "',                             &
              trim(lname),'" 4 0 /CHARACTER 9'
  call gr_exec1(comm)
  ! back to original weught
  call gr_pen(weight=cw,error=error)
  if (error) return
  ! That's it for NO SOURCE
  if (rsou%sourcetype.eq.soukind_none) return
  !
  !Write Doppler velocity and date
  if (rsou%sourcetype.eq.soukind_full) then
    write (comm,'(a,1x,f0.1,1x,a)')                           &
              'DRAW TEXT 0 3 "V\\dDop\\u =',                 &
                rsou%vdop,'km s\\u-1\\d" 4 0 /CHARACTER 9'
    call gr_exec1(comm)
    ! Computation time
    call jdate_to_datetime(rsou%jutc,datechain,error)
    if (error) return
    write (comm,'(a,a,a)') 'DRAW TEXT 0 6 "',datechain,'" 4 0 /CHARACTER 9'
    call gr_exec1(comm)
  endif
  !
  ! IF LSR/Z are defined, write them
  if (rsou%lsrdefined) then
    if (rsou%invtype.ne.'RE') then
      write (comm,'(a,1x,f0.1,1x,a)')                        &
              'DRAW TEXT 0 4 "V\\dLSR\\u =',                 &
                rsou%vlsr,'km s\\u-1\\d" 4 0 /CHARACTER 9'
      call gr_exec1(comm)
    else
      call gr_pen(colour=azcol,idash=1,error=error)
      if (error) return
      write (comm,'(a,1x,f0.6,1x,a)')                        &
              'DRAW TEXT 0 4 "z =',                          &
                rsou%z,'" 4 0 /CHARACTER 9'
      call gr_exec1(comm)
      call gr_pen(colour=adefcol,idash=1,error=error)
      if (error) return
    endif
  endif
  !
!   write (mess,'(a,1x,i0)') 'Unvalid source type:',rsou%sourcetype
!   call astro_message(seve%e,rname,mess)
!   error=.true.
!   return
  !
end subroutine rec_draw_source
