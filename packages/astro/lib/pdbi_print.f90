subroutine pdbi_print(line,error)
  use gildas_def
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>pdbi_print
  use ast_line
  !---------------------------------------------------------------------
  ! @ private
  ! PDBI internal routine, support for command PRINT
  !---------------------------------------------------------------------
  character(len=*):: line           !
  logical :: error                  !
  ! Local
  integer(kind=4) :: i, ier, lun,nc
  character(len=80) :: name, file, chain
  character(len=10) :: rname
  character(len=6) :: input1(4), input2(4), tmp1, tmp2
  character(len=3) :: bmode(4)
  ! Data
  data rname /'PDBI_PRINT'/
  data input1/'Q1 HOR','Q2 HOR','Q3 VER','Q4 VER'/
  data input2/'Q1 VER','Q2 VER','Q3 HOR','Q4 HOR'/
  data bmode/'LSB','USB','SSB','DSB'/
  ! ---------------------------------------------------------------------------
  !
  ! Sanity check (should be more elaborated)
  !
  if (.not.narrow_def) then
    call astro_message(seve%e,rname, 'LINE and NARROW not defined')
    error=.true.
    return
  endif
  !
  ! Open ouput file
  !
  name = 'setup'
  call sic_ch(line,0,1,name,nc,.false.,error)
  if (error)  return
  call sic_parsef(name,file,'','.obs')
  ier = sic_getlun(lun)
  ier = sic_open(lun,file,'NEW',.false.)
  if (ier.ne.0) then
    call astro_message(seve%e,rname,'Error opening file '//file)
    close (unit=lun)
    call sic_frelun (lun)
    error=.true.
    return
  endif
  rewind(lun)
  !
  ! Current date-time
  call sic_date(chain)
  !
  ! Print receiver & backend setup
  !
  write(lun,1000) '!'
  write(lun,1000) '! Plateau de Bure receiver and backend setup'
  write(lun,1000) '! Written by ASTRO command PRINT on '//chain(1:11)  ! Date only
! write(lun,1000) '! '//chain(1:lenc(chain))
  write(lun,1000) '!'
  !
  write(lun,1000) '! ----- Receiver -----'
  write(lun,1000) '!'
  write(lun,1010) recband
  chain = linecomm
  write(lun,1020) chain(1:lenc(chain)),recband
  write(lun,1000) '!'
  !
  write(lun,1000) '! ----- IF processor -----'
  tmp1=input1(narrow_input(1))
  tmp2=input2(narrow_input(2))
  write(lun,1000) '! Selected quarters are '//tmp1//' and '//tmp2
  write(lun,1000) '!'
  write(lun,1030) tmp1(1:2),tmp2(1:2),recband
  write(lun,1000) '!'
  !
  write(lun,1000) '! ----- Narrow-band correlator -----'
  write(lun,1000) '!'
  do i=1,nunit
    if (unit_def(i)) then
      write(lun,1040) i,unit_band(i),unit_cent(i),unit_wind(i),  &
      bmode(unit_bmode(i)), recband
    else
      write(lun,1050) i
    endif
  enddo
  !
  ! Close file
  !
  close (unit=lun)
  call sic_frelun (lun)
  !
1000 format(a)
1010 format('LET RECEIVER',i2)
1020 format(a,' /RECEIVER',i2)
1030 format('NARROW ',a,1x,a,' /RECEIVER',i2)
1040 format('SPECTRAL ',i1,i4,f8.2,' /NARROW',i2,' /BAND ',a,' /RECEIVER',i2)
1050 format('SPECTRAL ',i1,' OFF')
end subroutine pdbi_print
