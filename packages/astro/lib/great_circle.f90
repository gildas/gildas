subroutine great_circle(a1,d1,a2,d2,error)
  use gbl_message
  use gkernel_interfaces
  use gkernel_types
  use ast_astro
  use ast_constant
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  real(kind=8) :: a1                !
  real(kind=8) :: d1                !
  real(kind=8) :: a2                !
  real(kind=8) :: d2                !
  logical :: error                  !
  ! Local
  character(len=*), parameter :: rname='GREAT_CIRCLE'
  integer(kind=4) :: mstep
  parameter (mstep=21)
  real(kind=8) :: s1(2), s2(2), v1(3), v2(3), v(3), s(2), w(3), xin(mstep)
  real(kind=8) :: yin(mstep), zin(mstep), xmin, xmax, ymin, ymax, x1, cs, angle
  real(kind=8) :: ss, t, c1, c2, x, y
  integer(kind=4) :: i, j, nin
  logical :: in, last_in
  type(projection_t) :: gregproj
  !------------------------------------------------------------------------
  s1(1) = a1
  s1(2) = d1
  s2(1) = a2
  s2(2) = d2
  call sic_get_dble('USER_XMIN',xmin,error)
  call sic_get_dble('USER_XMAX',xmax,error)
  call sic_get_dble('USER_YMIN',ymin,error)
  call sic_get_dble('USER_YMAX',ymax,error)
  if (xmax.lt.xmin) then
    x1 = xmin
    xmin = xmax
    xmax = x1
  endif
  if (ymax.lt.ymin) then
    x1 = ymin
    ymin = ymax
    ymax = x1
  endif
  error = .false.
  !
  call rect(s1,v1)
  call rect(s2,v2)
  cs = v1(1)*v2(1) + v1(2)*v2(2) + v1(3)*v2(3)
  !
  if (cs.gt.1.d0 .or. cs.lt.-1.d0) then
    call astro_message(seve%w,rname,'Coincident or opposite positions')
    return
  endif
  angle = acos(cs)
  ss = sin(angle)
  if (ss.le.0d0) then
    call astro_message(seve%w,rname,'Coincident or opposite positions')
    return
  endif
  do i=1,3
    w(i) = (v2(i)-cs*v1(i))/ss
  enddo
  in = .false.
  last_in = .false.
  nin = 0
  do j=1, mstep
    t = angle * (j-1) / (mstep-1)
    c1 = cos(t)
    c2 = sin(t)
    do i= 1, 3
      v(i) = v1(i)*c1 + w(i)*c2
    enddo
    call spher(v,s)
    if (projection) then
      call greg_projec_get(gregproj)  ! Get current Greg projection
      call abs_to_rel(gregproj,s(1),s(2),x,y,1)
    else
      x = s(1)
      y = s(2)
      if (frame.eq.'EQUATORIAL' .and. x.lt.0) x = x + 2d0*pi
      if (frame.eq.'HORIZONTAL' .and. azref.eq.'N' .and. x.lt.0) x = x + 2d0*pi
    endif
    in = x.ge.xmin .and. x.le.xmax .and. y.ge.ymin .and. y.le.ymax
    if (frame.eq.'HORIZONTAL') in = in .and. (s(2).ge.0d0)
    if (in) then
      nin = nin + 1
      xin (nin) = x
      yin (nin) = y
      zin (nin) = t
      if (.not.projection .and. abs(xin(nin)-xin(nin-1)).gt.pi) then
        xin(nin) = xin(nin)-sign(2*pi,xin(nin)-xin(nin-1))
        if (nin.ge.2)  &
          call gr8_curve (nin,xin,yin,zin,'Z',.false.,0d0,-1d0,error)
        nin = 1
        xin (1) = x
        yin (1) = y
        zin (1) = t
      endif
    elseif (.not.last_in) then
      nin = 1
      xin (1) = x
      yin (1) = y
      zin (1) = t
    elseif (last_in) then
      nin = nin + 1
      xin (nin) = x
      yin (nin) = y
      zin (nin) = t
      call gr8_curve (nin,xin,yin,zin,'Z',.false.,0d0,-1d0,error)
      nin = 1
    endif
    last_in = in
  enddo
  if (nin.ge.2) then
    call gr8_curve (nin,xin,yin,zin,'Z',.false.,0d0,-1d0,error)
  endif
  !
end subroutine great_circle
