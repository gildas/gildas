subroutine small_circle(a1,d1,r,error)
  use gkernel_interfaces
  use gkernel_types
  use ast_astro
  use ast_constant
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  real(kind=8) :: a1                !
  real(kind=8) :: d1                !
  real(kind=8) :: r                 !
  logical :: error                  !
  ! Local
  integer(kind=4) :: mstep
  parameter (mstep=50)
  real(kind=8) :: s1(2), u(3), v(3), s(2), w(3), vv(3)
  real(kind=8) :: xin(mstep), yin(mstep), zin(mstep)
  real(kind=8) :: xmin, xmax, ymin, ymax, x1, cs, ss, t, cp, sp, x, y, g
  integer(kind=4) :: i, j, nin
  logical :: in, last_in
  type(projection_t) :: gregproj
  !------------------------------------------------------------------------
  s1(1) = a1
  s1(2) = d1
  call sic_get_dble('USER_XMIN',xmin,error)
  call sic_get_dble('USER_XMAX',xmax,error)
  call sic_get_dble('USER_YMIN',ymin,error)
  call sic_get_dble('USER_YMAX',ymax,error)
  if (xmax.lt.xmin) then
    x1 = xmin
    xmin = xmax
    xmax = x1
  endif
  if (ymax.lt.ymin) then
    x1 = ymin
    ymin = ymax
    ymax = x1
  endif
  error = .false.
  !
  call rect(s1,w)
  v (3) = 0
  g = sqrt(1 - w(3)**2)
  u (3) = - g
  if (g.ne.0) then
    u (1) = w(1) * w(3) / g
    u (2) = w(2) * w(3) / g
    v (1) = w(2) / g
    v (2) = - w(1) / g
  else
    u (1) = 1
    u (2) = 0
    v (1) = 0
    v (2) = 1
  endif
  cs = cos(r)
  ss = sin(r)
  in = .false.
  last_in = .false.
  nin = 0
  do j=1, mstep
    t = 2*pi * (j-1) / (mstep-1)
    cp = cos(t)
    sp = sin(t)
    do i= 1, 3
      vv(i) = ss * (cp * u(i) + sp * v(i)) + cs * w(i)
    enddo
    call spher(vv,s)
    if (projection) then
      call greg_projec_get(gregproj)  ! Get current Greg projection
      call abs_to_rel(gregproj,s(1),s(2),x,y,1)
    else
      x = s(1)
      if (frame.eq.'EQUATORIAL' .and. x.lt.0) x = x + 2d0*pi
      if (frame.eq.'HORIZONTAL' .and. azref.eq.'N' .and. x.lt.0) x = x + 2d0*pi
      y = s(2)
    endif
    in = x.ge.xmin .and. x.le.xmax .and. y.ge.ymin .and. y.le.ymax
    if (frame.eq.'HORIZONTAL') in = in .and. (s(2).ge.0d0)
    if (in) then
      nin = nin + 1
      xin (nin) = x
      yin (nin) = y
      zin (nin) = t
      if (nin.ge.2) then
        if (.not.projection .and. abs(xin(nin)-xin(nin-1)).gt.pi) then
          xin(nin) = xin(nin)-sign(2*pi,xin(nin)-xin(nin-1))
          call gr8_curve(nin,xin,yin,zin,'Z',.false.,0d0,-1d0,error)
          nin = 1
          xin (1) = x
          yin (1) = y
          zin (1) = t
        endif
      endif
    elseif (.not.last_in) then
      nin = 1
      xin (1) = x
      yin (1) = y
      zin (1) = t
    elseif (last_in) then
      nin = nin + 1
      xin (nin) = x
      yin (nin) = y
      zin (nin) = t
      call gr8_curve (nin,xin,yin,zin,'Z',.false.,0d0,-1d0,error)
      nin = 1
    endif
    last_in = in
  enddo
  if (nin.ge.2) then
    call gr8_curve (nin,xin,yin,zin,'Z',.false.,0d0,-1d0,error)
  endif
end subroutine small_circle
