function a_lever(d,d0,el)
  !---------------------------------------------------------------------
  ! @ private
  !	Lever d'une etoile de declinaison D a latitude D0
  !	Ce code est different de "lever" de GreG, donc je l'appelle A_LEVER
  !	(sinon big problemes,...GD, 8-10-1992)
  !	D	Declination in radians
  !	D0	Latitude in degrees
  ! 	EL	elevation limit in degrees
  !	LEVER	Hour angle in Hours
  !
  !	PI/2-D0+EL < D	Circumpolaire
  !	D0-PI/2+EL < D < PI/2-D0+EL H = ACOS (TAN(D)/TAN(D0-PI/2))
  !	D < D0-PI/2+EL	Toujours couchee...
  !
  ! Formule applicable a la partie visible du ciel en projection
  ! sur un plan
  !---------------------------------------------------------------------
  real(kind=4) :: a_lever           !
  real(kind=4) :: d                 !
  real(kind=4) :: d0                !
  real(kind=4) :: el                !
  ! Local
  real(kind=4) :: dd,d1,d2,ee
  real(kind=8) :: pi
  parameter (pi=3.14159265358979323846d0)
  !
  if (d0.lt.0.) then
    d2 = -d
    d1 = -d0*pi/180.0
  else
    d2 = d
    d1 = d0*pi/180.0
  endif
  dd = d1-pi*0.5d0
  ee = el*pi/180.
  a_lever = (-sin(ee)+sin(d2)*cos(dd))/(cos(d2)*sin(dd))
  if (a_lever.le.-1.0) then
    a_lever = -12.0            ! Circumpolar
  elseif (a_lever.ge.1.0) then
    a_lever = 0.0              ! Not visible
  else
    a_lever = -12.0/pi* acos(a_lever)
  endif
end function a_lever
