function iangin(ch,cdef,dang,iang)
  !---------------------------------------------------------------------
  ! @ private
  !	J.SCHRAML, Sept 81
  !
  ! This function converts an input line into angles. It calls
  ! the function LINSCN for returning individual fields and their
  ! sign and dimensions. The fields are multiplied by 60 and than
  ! added. If a dimension occurs the accumulated value is converted
  ! into arcseconds. The dimension of the last field is cdef unless
  ! specified explicitely. Sign is only allowed for the first
  ! field, otherwise an error will occur.
  ! Input: String CH and Dimension CDEF
  ! Output: Double precision Angle in Radians and integer 10 ms
  !---------------------------------------------------------------------
  integer(kind=4) :: iangin         !
  character(len=*):: ch             !
  character(len=1) :: cdef          !
  real(kind=8) :: dang              !
  integer(kind=4) :: iang           !
  ! Global
  integer(kind=4) :: lenc,linscn
  ! Local
  integer(kind=4) :: ilen, istart, nfield, i2, isig, isign
  integer(kind=4) :: i
  real(kind=8) :: d, dacc
  character(len=1) :: cdim,comp(6),comb(6)
  real(kind=8) :: dfac(6),dangl
  data comp/'h','m','s','d','''','"'/
  data comb/'H','M','S','D','''','"'/
  data dfac/5.4d4,9d2,15d0,36d2,6d1,1d0/
  !
  dangl=0.0d0
  !
  ilen = lenc(ch)
  istart=1
  nfield=0
  300   continue
  i2=linscn(ch(istart:ilen),d,isig,cdim)   ! Scan one field
  if (nfield.eq.0) then
    isign=isig                 ! Save sign, and only 1
    d=dabs(d)                  ! Positive value
    dacc=0.0d0
    isig=0                     ! For test
  endif
  dacc=dacc*60.0d0+d           ! Accumulate
  if (isig.ne.0) go to 2000    ! another sign=ERROR
  if(cdim.eq.' ') go to 500    ! Test various space characters
  if(cdim.eq.',') go to 500
  if(cdim.eq.':') go to 500
  do 400 i=1,6
    if(cdim.eq.comp(i) .or. cdim.eq.comb(i)) then
      dangl=dangl+dacc*dfac(i)   ! Add the value to angle
      dacc=0.0d0                 ! and new fields
      go to 500
    endif
  400   continue
  iangin=4
  go to 2100                     ! unwanted dimension
  !
  500   nfield=nfield+1
  ! IF(NFIELD.EQ.3) GO TO 800    ! Stop after 3 fields
  istart=istart+i2               ! next field
  if (istart.le.ilen)  go to 300 ! and all fields
  !
  800   continue
  cdim = cdef
  do 900 i=1,6
    if (cdim.eq.comp(i).or.cdim.eq.comb(i)) then
      dangl=dangl+dacc*dfac(i) ! default
    endif
  900   continue
  dang=dmod(dangl,1.296d6)
  if(dang.eq.0.0d0.and.dangl.ne.0.0d0) dang=1.296d6    !exact 360
  iang=1000d0*dang+.5d0
  if (isign.lt.0) then
    dang=-dang
    iang=-iang                 ! Negativ
  endif
  dang=dang*6.283185307179586d0/1.296d6    !Radian
  iangin=1                     !ok
  return
  2000  iangin=2
  2100  dang=0.0d0
  iang=0
  return
end function iangin
!
function linscn(ch,dat,isig,cdim)
  !---------------------------------------------------------------------
  ! @ private
  !	J.SCHRAML, sept 1981
  !
  ! This function decodes an input character string CH, finds the value
  ! DAT, its sign and the information if a sign has been entered in ISIG,
  ! assembles the dimension as a character string CDIM and returns the
  ! actual field lenth as the function value
  ! This function is called by the the routines that interprete the
  ! input of observers as numbers or angles.
  !
  !	Definitions and startup values
  !---------------------------------------------------------------------
  integer(kind=4) :: linscn         !
  character(len=*):: ch             !
  real(kind=8) :: dat               !
  integer(kind=4) :: isig           !
  character(len=*):: cdim           !
  ! Local
  integer(kind=4) :: i, ic, ndim, ldim
  real(kind=8) :: d10
  logical :: fieldstart,fieldend,dims
  !
  dat=0.0d0
  d10=0.0d0                    ! Is used for the decimal point
  isig=0                       ! No sign typed thus far
  linscn=len(ch)               ! Test the whole string at most
  cdim=' '                     ! No dimension was typed
  !
  i=0                          ! Counts the character position
  fieldstart=.false.
  fieldend  =.false.
  dims =.false.
  !
  ! Everytime a character is interpreted the return is to this point and
  ! the flow is modified by the logical variables
  !
  100   i=i+1                      ! next character
  if (i.gt.linscn) go to 1000      ! The end of the string is reached
  !
  if (ch(i:i).eq.' ') then         ! Blanks are important seperators
    if (dims) go to 900            ! This is a start of a new field after
    !                              ! terminating a dimension field= finish
    if (.not.fieldstart) go to 100 ! Leading blanks
    fieldend =.true.               ! Field terminated with blank
    go to 100                      ! Read over terminating blanks
  endif
  !
  ic=ichar(ch(i:i))-44             ! Position in the ASCII table, makes +
  !                                ! a-1 and - to +1; 0=4,9=13
  if (iabs(ic).eq.1) then          ! It is a sign
    if (dims) go to 900            ! This is part of the next field
    if (isig.ne.0.or.fieldstart) then
      go to 900                    ! must be new value, sinc it is new sign
    else
      isig=-ic                     ! remember the sign
      go to 100                    ! and continue interpreting
    endif
  endif
  !
  if (ch(i:i).eq.'.') then     ! next the period
    if (dims) go to 900        ! starts a new field
    if (fieldend)  go to 900   ! also after blanks
    if (d10.eq.0.0d0) then     ! no other period thus far
      d10=1.0d0                ! start with the fraction
      fieldstart=.true.        !a new field has started
      go to 100                ! and continue reading
    else
      go to 900                ! must be next period
    endif
  endif
  !
  ! only numbers or dimensions left
  if (ic.lt.4.or.ic.gt.13) go to 500   ! must be dimensions
  if (dims) go to 900          ! new field
  if (fieldend ) go to 900     ! was no dimension at all
  fieldstart=.true.            ! Obviously a new field
  dat=dat*10.0d0+(ic-4)        ! accumulating the decimal number
  d10=d10*10.0d0               ! and the fraction
  go to 100
  !
  500   if (.not.dims) then    ! first indication of dim
    dims =.true.               ! from now on
    ndim=1
    ldim=len(cdim)             ! for safety
    cdim(1:1)=ch(i:i)          ! first character
    go to 100
  else
    ndim=ndim+1                ! Number of characters
    if (ndim.gt.ldim) go to 900    ! finish here, end dimensions
    cdim(ndim:ndim)=ch(i:i)    ! append string
    go to 100                  ! Till the end
  endif
  900   linscn=i-1             ! so many without the test next
  1000  if (d10.ne.0.0d0) dat=dat/d10  ! fraction, if any
  if (isig.lt.0)  dat=-dat     ! negative sign
  return
end function linscn
