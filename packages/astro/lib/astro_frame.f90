subroutine astro_frame(line,error)
  use gbl_message
  use ast_astro
  use ast_constant
  !---------------------------------------------------------------------
  ! @ private
  !	Define displayed reference frame
  !	FRAME EQ|HO		rectangular plot
  !	FRAME EQ|HO l b size	stereo square plot centered at l,b
  !				size in degrees
  !	FRAME HO  ZENITH|NORTH|EAST|WEST|SOUTH
  !---------------------------------------------------------------------
  character(len=*):: line           !
  logical :: error                  !
  ! Global
  logical :: sic_present,gr_error
  ! Local
  character(len=*), parameter :: rname='FRAME'
  character(len=message_length) :: mess
  character(len=12) :: arg1, arg2, arg3
  character(len=255) :: command
  real(kind=8) :: size
  integer(kind=4) :: nc
  !------------------------------------------------------------------------
  ! Code :
  arg1 = 'HORIZONTAL'
  arg2 = ' '
  arg3 = ' '
  call sic_ke (line,0,1,arg1,nc,.false.,error)
  if (error) return
  call sic_ke (line,0,2,arg2,nc,.false.,error)
  if (error) return
  call sic_ke (line,0,3,arg3,nc,.false.,error)
  if (error) return
  size = 180.d0
  call sic_r8 (line,0,4,size,.false.,error)
  if (error) return
  if (size.ne.180d0) then
    write(mess,*) 'Size= ',size
    call astro_message(seve%r,rname,mess)
    size = tan(pi*(size/4d0)/180.0d0)
    write(mess,*) 'Tan = ',size
    call astro_message(seve%r,rname,mess)
  else
    size= 1d0
  endif
  !
  if (arg1(1:2).eq.'EQ') then
    frame = 'EQUATORIAL'
    projection = sic_present(0,2).and.sic_present(0,3)
    call gr_exec('SET SYSTEM EQUATORIAL')
    call gr_exec('CLEAR DIRECTORY')
    if (projection) then
      call gr_exec('PROJECTION '//arg2//' '//arg3//' /TYPE STEREO')
      if (gr_error()) return
      write(command,'(A,4(1X,1PG13.6))') 'LIMITS ', size,-size,-size,size
      call gr_exec1(command)
      call gr_exec('SET BOX SQUARE')
      call gr_exec1('TICKSPACE 5 15 5 15')
      call gr_exec1('BOX N N N')
      call gr_exec('GRID 15 15')
    else
      call gr_exec('SET BOX /DEF')
      call gr_exec('PROJECTION 0 0 /TYPE NONE')
      call gr_exec1('LIMITS 24. 0. -90. 90.')
      call gr_exec1('TICKSPACE 1 3 10 30')
      call gr_exec1('BOX')
      call gr_exec1('LIMITS 2*pi 0 -pi|2 pi|2')
      call gr_exec1('LABEL "Right Ascension (hours)" /X')
      call gr_exec1('LABEL "Declination (degrees)" /Y')
    endif
  elseif (arg1(1:2).eq.'HO') then
    projection = sic_present(0,2)
    call gr_exec('SET SYSTEM UNKNOWN')
    call gr_exec('CLEAR DIRECTORY')
    frame = 'HORIZONTAL'
    if (projection) then
      if (arg2(1:1).eq.'Z') then
        if (azref.eq.'S') then
          arg2 = '0'
        else
          arg2 = '180'
        endif
        arg3 = '90'
        size = 1.0d0
      elseif (arg2(1:1).eq.'N') then
        if (azref.eq.'S') then
          arg2 = '-180'
        else
          arg2 = '0'
        endif
        arg3 = '60'
        size = tan(pi*(30d0)/180.0d0)
      elseif (arg2(1:1).eq.'S') then
        if (azref.eq.'S') then
          arg2 = '0'
        else
          arg2 = '180'
        endif
        arg3 = '60'
        size = tan(pi*(30d0)/180.0d0)
      elseif (arg2(1:1).eq.'E') then
        if (azref.eq.'S') then
          arg2 = '-90'
        else
          arg2 = '90'
        endif
        arg3 = '60'
        size = tan(pi*(30d0)/180.0d0)
      elseif (arg2(1:1).eq.'W') then
        if (azref.eq.'S') then
          arg2 = '90'
        else
          arg2 = '270'
        endif
        arg3 = '60'
        size = tan(pi*(30d0)/180.0d0)
      elseif (arg3(1:1).eq.' ') then
        call astro_message(seve%f,'ASTRO','Uncomplete Frame definition')
        error=.true.
        return
      endif
      call gr_exec('PROJECTION '//arg2//' '//arg3//' /TYPE STEREO')
      if (gr_error()) return
      write(command,'(A,4(1X,1PG13.6))') 'LIMITS ', -size,size,-size,size
      call gr_exec1(command)
      call gr_exec('SET BOX SQUARE')
      call gr_exec1('TICKSPACE 5 15 5 15')
      call gr_exec1('BOX N N')
      call gr_exec('GRID 15 15')
    else
      call gr_exec('SET BOX /DEF')
      call gr_exec('PROJECTION 0 0 /TYPE NONE')
      if (azref.eq.'S') then
        call gr_exec1('LIMITS -180 180 0. 90.')
      else
        call gr_exec1('LIMITS 0 360 0. 90.')
      endif
      call gr_exec1('TICKSPACE 10. 30. 5. 15.')
      call gr_exec1('BOX')
      if (azref.eq.'S') then
        call gr_exec1('LIMITS -PI PI 0. PI|2')
      else
        call gr_exec1('LIMITS 0 2*PI 0. PI|2')
      endif
      call gr_exec1('LABEL "Azimuth (degrees)" /X')
      call gr_exec1('LABEL "Elevation  (degrees)" /Y')
    endif
  else
    call astro_message(seve%e,'ASTRO_FRAME','Unsupported display frame '//arg1)
  endif
  nsplot = 0
end subroutine astro_frame
