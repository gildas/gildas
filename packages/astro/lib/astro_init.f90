subroutine astro_init(error)
  use gbl_message
  use gkernel_interfaces
  use ast_line
  use ast_astro
  use ast_constant
  !---------------------------------------------------------------------
  ! @ private
  !	Initialize ASTRO parameters
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Local
  character(len=10) :: chain
  character(len=16) :: varname
  character(len=10) :: rname= 'ASTRO_INIT'
  integer(kind=4) :: nv
  !
  ! Code :
  nsplot = 0
  jnow_utc = 2451545.0d0
  ! mjd = jul.date. - 2400000.5
  ! d_ut1 = 0.3290 - 0.00140*(mjd - 47184)
  d_ut1 = 0.0d0
  d_tdt = 56.184d0
  altitude = 2.560             ! in kilometers
  chain = '05:54:26.0'
  call sic_sexa (chain,10,lonlat(1),error)
  chain = '44:38:01.2'
  call sic_sexa (chain,10,lonlat(2),error)
  obs_year = 2015            ! default
  alma_cycle = 1              ! default
  !
  jnow_utc = j2000 - 12 * 365.25d0
  jnow_ut1 = jnow_utc + d_ut1 / 86400.d0
  jnow_tdt = jnow_utc + d_tdt / 86400.d0
  call ephini(error)
  if (error) then
    call astro_message(seve%f,rname,'Error in EPHINI')
    return
  else
    call astro_message(seve%i,rname,'Ephemeris opened')
  endif
  call gr_exec('SET SYSTEM UNKNOWN')
  projection = .false.
  frame = 'HORIZONTAL'
  azref = 'S'
  call gr_exec('PROJECTION 0 0 /TYPE NONE')
  call gr_exec1('LIMITS -180 180 0. 90.')
  call gr_exec1('TICKSPACE 10. 30. 5. 15.')
  call gr_exec1('BOX')
  call gr_exec1('LIMITS -pi pi 0 pi|2')
  call gr_exec1('LABEL "Azimuth (degrees)" /X')
  call gr_exec1('LABEL "Elevation  (degrees)" /Y')
  !
  ! SIC variables
  !
  ! The 4 following variables duplicate the CLASS variables
  ! and are only used for display. They may be advantageously
  ! replaced by the astro%source%  similar variables defined in object.f90
  !
  call sic_def_dble('AZIMUTH',azimuth,0,1,.true.,error)
  !! if (error) return
  call sic_def_dble('ELEVATION',elevation,0,1,.true.,error)
  !! if (error) return
  call sic_def_dble('LAMBDA',s_1(1),0,1,.true.,error)
  !! if (error) return
  call sic_def_dble('BETA',s_1(2),0,1,.true.,error)
  !! if (error) return
  !
  call sic_def_dble('RA',ra,0,1,.true.,error)
  if (error) return
  call sic_def_dble('DEC',dec,0,1,.true.,error)
  if (error) return
  call sic_def_dble('PAR_ANGLE',parallactic_angle,0,1,.true.,error)
  if (error) return
  call sic_def_dble('DOPPLER',fshift,0,1,.false.,error)
  if (error) return
  call sic_def_dble('FREQUENCY',freq,0,1,.false.,error)
  !! if (error) return
  ! No longer needed. Use Astro%Source%v_sou_lsr
  !  call sic_def_real('ASTRO_VELOCITY',velocity,0,1,.true.,error)
  !  if (error) return
  call sic_def_dble('SUN_LIMIT',slimit,0,1,.false.,error)
  if (error) return
  call sic_def_char('FRAME',frame,.true.,error)
  if (error) return
  ! No longer needed. Use Astro%Source%Name
  ! call sic_def_char('ASTRO_SOURCE',source_name,.true.,error
  ! if (error) return
  call sic_def_char('CATALOG',catalog_name(1),.true.,error)
  if (error) return
  call sic_def_char('CATALOG_ALT',catalog_name(2),.true.,error)
  if (error) return
  !
  !      CALL SIC_DEF_DBLE ('FLO1',FLO1,1,2,.TRUE.,ERROR)
  !      IF (ERROR) RETURN
  !      CALL SIC_DEF_DBLE ('FLO2',FLO2,1,2,.TRUE.,ERROR)
  !      IF (ERROR) RETURN
  !
  call sic_def_char('OBSERVATORY',obsname,.true.,error)
  if (error) return
  call sic_def_dble('OBS_LONLAT',lonlat,1,2,.true.,error)
  call sic_def_dble('OBS_ALTITUDE',altitude,0,1,.true.,error)
  !   if (error) return
  ! No longer needed. Use Astro%Planet%TMB
  !   call sic_def_real('PLANET_TMB',planet_tmb,0,1,.true.,error)
  !   if (error) return
  ! No longer needed. Use Astro%Planet%FLUX
  !   call sic_def_real('PLANET_FLUX',planet_flux,0,1,.true.,error)
  !   if (error) return
  ! No longer needed. Use Astro%Planet%SIZE
  !   call sic_def_real('PLANET_SIZE',planet_size,1,3,.true.,error)
  !   if (error) return
  call sic_def_inte('RECEIVER',recband,0,1,.false.,error)
  if (error) return
  tolerance_freq = 1 ! 1 MHz default frequency tolerance on ALMA\SPWINDOW command
  call sic_def_real('TOLERANCE_FREQ',tolerance_freq,0,1,.false.,error)
  !
  ! For OMS communication
  varname = 'OMS'
  if (.not.sic_varexist(varname)) then
      call sic_defstructure(varname,.false.,error)
      if (error) return
  endif
  nv=lenc(varname)
  call sic_def_char(varname(1:nv)//'%CALLER',oms_caller(1:lenc(oms_caller)),.false.,error)
  call sic_def_logi(varname(1:nv)//'%BUGW20',oms_bugw20,.false.,error)
  !
end subroutine astro_init
