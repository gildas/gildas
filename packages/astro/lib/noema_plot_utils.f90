subroutine noema_plot(line,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_plot
  use astro_noema_type
  use ast_astro
  use my_receiver_globals
  use plot_molecules_globals
  use frequency_axis_globals
  use string_parser_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Plot the current setup of receiver\backend at Bure
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(inout) :: line        ! command line
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='PLOT'
  integer(kind=4), parameter :: optrec=1        ! /RECEIVER
  integer(kind=4), parameter :: optpms=2        ! /PROPOSAL option
  integer(kind=4), parameter :: optline=3        ! /LINES option
  integer(kind=4), parameter :: optband=4        ! /CHECKBAND option
  type(plot_molecules_t) :: cata
  type(string_parser_t) :: sp
  character(len=5)      :: selcode
  logical :: dopchanged,dorec,dopms,domol,dofreq,dodf,badsetup,chunk1,allisoff,do250,do2000,doband
  integer(kind=4)       :: lpar,iu,isel,overload,conflict,ib,is,bandin
  real(kind=8)  :: frepin,frep
  character(len=256) :: mess,comm
  integer(kind=4), parameter :: ipband=1
  integer(kind=4), parameter :: ippol=2
  integer(kind=4), parameter :: ipsb=3
  integer(kind=4), parameter :: ipbb=4
  integer(kind=4), parameter :: nband=3
  integer(kind=4), parameter :: npol=2
  integer(kind=4), parameter :: nsb=2
  integer(kind=4), parameter :: nbb=2
  integer(kind=4)       :: parsecode(4)
  character(len=2) :: bb_band(nband)
  character(len=1) :: bb_pol(npol),bb_sb(nsb),bb_bb(nbb)
  type(info_pms_t) :: infopms
  data bb_band/'B1','B2','B3'/
  data bb_pol/'H','V'/
  data bb_sb/'U','L'/
  data bb_bb/'O','I'/
  !
  !Preliminary checks
  if (.not.noema%cfebe%defined) then
    call astro_message(seve%e,rname,'No tuning found')
    error = .true.
    return
  endif
  if (.not.noema%cfebe%i_f%ifproc%defined) then
    call astro_message(seve%e,rname,'Problem with IF Processor part')
    error = .true.
    return
  endif
  !
  !check that doppler did not changed since last tuning
  call rec_check_doppler(noema%source,noema%recdesc%redshift,dopchanged,error)
  if (dopchanged) then
    call astro_message(seve%e,rname,'Source properties changed since last tuning')
    call astro_message(seve%i,rname,'Nothing done')
    call rec_display_error('Source changed since last tuning',error)
    error = .true.
    return
  endif
  !
  dorec=sic_present(optrec,0)
  dopms=sic_present(optpms,0)
  dofreq=sic_present(optpms,1)
  dodf=sic_present(optpms,2)
  domol=sic_present(optline,0)
  doband=sic_present(optband,0)
  !
  if (dorec.and.dopms) then
    call astro_message(seve%e,rname,'Options /RECEIVER and /PROPOSAL are exculsive from each other')
    error = .true.
    return
  endif
  if (domol.and..not.(dopms)) then
    call astro_message(seve%e,rname,'Options /LINE only available in combination with /PROPOSAL')
    error = .true.
    return
  endif
  if (doband.and..not.(dopms)) then
    call astro_message(seve%e,rname,'Options /CHECKBAND only available in combination with /PROPOSAL')
    error = .true.
    return
  endif
  !
  if (dorec) then
    ! Global view of the receiver
    call rec_plot_sidebands(noema%recdesc,noema%source,noema%cfebe%rectune, &
                            cplot,molecules,freq_axis,error)
    if (error) return
    call noema_draw_summary(noema,cplot,molecules,freq_axis,error)
    if (error) return
!     if (rec%source%sourcetype.eq.soukind_full) then
!       call noema_oplot_dopminmax(spw%out,pfx,rec,cplot,freq_axis,error)
!       if (error) return
!     endif
  else if (dopms) then
    ! PROPOSAL PLOT (AS DONE IN PMS)
    do250=.false.
    do2000=.false.
    do iu=1,noema%cfebe%pfx%n_units
      ! check if 250kHz mode is requested and issue a warning
      if (noema%cfebe%pfx%unit(iu)%imode.eq.2) then
        do250=.true.
      else if (noema%cfebe%pfx%unit(iu)%imode.eq.1) then
        do2000=.true.
      endif
    enddo
    if (do250.and.do2000) then
      call astro_message(seve%e,rname,"First implementation of 250kHz mode does not allow mixed correlator configuration")
      call astro_message(seve%e,rname,"Same mode should be used for all basebands")
      error=.true. 
      return
    endif
    ! Check the band if option present (PMS use)
    if (doband) then
      if (sic_narg(optband).gt.1) then
        call astro_message(seve%e,rname,'Too many arguments')
        error=.true.
        return
      endif
      call sic_i4(line,optband,1,bandin,.true.,error)
      if (error) return
      if (noema%cfebe%rectune%iband.ne.bandin) then
        call astro_message(seve%e,rname,'Input frequency and Band do not match')
        error=.true.
        return
      endif
    endif
    ! Check status of pfx (overload or conflict)
    chunk1=.false.
    call noema_pfx_status(noema%cfebe%pfx,overload,conflict,chunk1,allisoff,error)
    if (error) return
    badsetup=.false.
    if (allisoff) then
      call astro_message(seve%e,'OMS','All polyfix unit are OFF')    
      error=.true.
      return
    endif
    if (overload.gt.0) then
      badsetup=.true.
      call astro_message(seve%e,'OMS','Setup requires more chunks than available')
    endif
    if (conflict.gt.0) then
      badsetup=.true.
      write (mess,'(i0,1x,a)') conflict,'Chunks used by more than one spectral window'
      call astro_message(seve%e,'OMS',mess)
    endif
    if (badsetup) then
      call astro_message(seve%e,'OMS','Please solve conflicts and try again')
      error=.true.
      return
    endif
    !
    call astro_message(seve%i,'OMS','Switching Frequency Axis to Rest and LSR')
    freq_axis%main=trim(freqax_names(freqax_rest))
    freq_axis%second=trim(freqax_names(freqax_lsr))
    ! plot on a single horizontal line to fit well in PMS pages
    ! prepare for molecules
    if (domol) then
      cata = molecules
      cata%doplot=.true.
    else
      cata%doplot=.false.
    endif
    call noema_default_pms(infopms,error)
    if (error) return
    ! Indicate a representative frequency
    if (dofreq) then
      call sic_r8(line,optpms,1,frepin,.true.,error)
      if (error) return
      call rec_inputtorest(rname,frepin,freq_axis%main,noema%source,frep,error)
      if (error) return
      infopms%frep=frep ! GHz
      call resttolsr(noema%source%lsrshift,infopms%frep,infopms%freplsr,error)
      if (error) return
      frep=frep*mhzperghz ! in MHz for further computations
      if (dodf) then
        call sic_r8(line,optpms,2,infopms%resrep,.true.,error)
        if (error) return
      endif
    endif
    call noema_info_pms(noema%cfebe%rectune,noema%source,noema%cfebe%pfx,noema%cfebe%spw%out,infopms,error)
    if (error) return
    call rec_plot_tuning(noema%recdesc,noema%source,noema%cfebe%rectune,cata,cplot,freq_axis,error)
    if (error) return
    ! Add confusion zone
    write (comm,'(a)') 'CHANGE DIRECTORY BOX1'
    call gr_execl(comm)
    do is=1,noema%recdesc%n_sbands
      do ib=1,noema%recdesc%n_bbands        
        call noema_draw_confusion(rname,noema%recdesc,noema%cfebe%rectune%flo1,noema%source%dopshift, &
                                  ib,is,cplot%box(1)%rest,cplot%desc,error)
        if (error) return
      enddo
    enddo
    write (comm,'(a)') 'CHANGE DIRECTORY'
    call gr_execl(comm)
    call gr_pen(colour=adefcol,error=error)
    ! Add SPW
    call noema_draw_summary(noema,cplot,cata,freq_axis,error)
    if (error) return
!     if (rec%source%sourcetype.eq.soukind_full) then
!       call noema_oplot_dopminmax(spw%out,pfx,rec,cplot,freq_axis,error)
!       if (error) return
!     endif
! Draw LO freq
    call noema_draw_flo(noema%cfebe%rectune,cplot,error)
    if (error) return
    ! Print lsr range validity
    call noema_lsrrange(noema,cplot,infopms,error)
    if (error) return
    ! Add representative frequency marker
    if (dofreq) then
      call noema_draw_frep(frep,noema%cfebe%spw%out,cplot,infopms,error)
      if (error) return
    endif
    ! Output text to be read by OMS
    call noema_print_pms(infopms,error)
    if (error) return
    !
  else
    ! Baseband selection
    selcode=''
    call sic_ke(line,0,1,selcode,lpar,.false.,error)
    if (error) return
    !Parse the user code
    if (len_trim(selcode).ne.0) then
      call string_parser_addlist(sp,ipband,bb_band,error)
      if (error) return
      call string_parser_addlist(sp,ippol,bb_pol,error)
      if (error) return
      call string_parser_addlist(sp,ipsb,bb_sb,error)
      if (error) return
      call string_parser_addlist(sp,ipbb,bb_bb,error)
      if (error) return
      call string_parser_parse('BASEBAND',sp,selcode,.false.,parsecode,error)
      if (error) return
    else 
      parsecode(ipband)=0
      parsecode(ippol)=0
      parsecode(ipsb)=0
      parsecode(ipbb)=0
    endif
    !
    if (parsecode(ippol).eq.0) then
      noema%cfebe%i_f%selunit%polmode = "B" ! BOTH polar same config
    else
      noema%cfebe%i_f%selunit%polmode = "S"
    endif
    !
    ! Loop over pfx%units to put the right numbers in ifsel() and define the mode when needed
    isel=0
    do iu=1,noema%cfebe%pfx%n_units
      if (noema%cfebe%pfx%unit(iu)%iband.ne.parsecode(ipband).and.parsecode(ipband).ne.0) cycle
      if (noema%cfebe%pfx%unit(iu)%pol_code.ne.parsecode(ippol).and.parsecode(ippol).ne.0) cycle
      if (noema%cfebe%pfx%unit(iu)%sb_code.ne.parsecode(ipsb).and.parsecode(ipsb).ne.0) cycle
      if (noema%cfebe%pfx%unit(iu)%bb_code.ne.parsecode(ipbb).and.parsecode(ipbb).ne.0) cycle
      isel=isel+1
      noema%cfebe%i_f%selunit%usel(isel)=iu
    enddo !iu
    if (isel.eq.0) then
      call astro_message(seve%e,rname,'The selection returns nothing')
      error = .true.
      return
    endif
    !
    noema%cfebe%i_f%selunit%n_ifsel=isel
    write (mess,'(a)') 'Selected Basebands are:'
    do iu=1,isel
      if (noema%cfebe%i_f%selunit%usel(iu).ne.-1) then
         write (mess,'(a,1x,a)') trim(mess), & 
         trim(noema%cfebe%pfx%unit(noema%cfebe%i_f%selunit%usel(iu))%label)
      endif
    enddo !iu
    call astro_message(seve%i,rname,mess)
    !
    call noema_plot_selpfx(rname,noema,cplot,molecules,freq_axis,error)
    if (error) return
    !
    if (noema%source%sourcetype.eq.soukind_full) then
      call noema_oplot_dopminmax(noema,cplot,freq_axis,error)
      if (error) return
    endif
    !
  endif
  !
  ! Let the user with the limits in main axis frequency frame
  call rec_set_limits(rname,cplot,freq_axis%main,error)
  if (error) return
  !
  ! Empty the selection, user must do a BASEBAND before defining new SPW
  noema%cfebe%i_f%selunit%n_ifsel=0
  !
end subroutine noema_plot
!
subroutine draw_dopminmax_spw(spwu,pfx,rdesc,rsou,rtune,cplotb,ry1,ry2,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>draw_dopminmax_spw
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Show what can be the variation on the HR SPW being plotted 
  ! with the evolution of the Earth doppler
  !
  !-----------------------------------------------------------------------
  type(spw_unit_t), intent(in) :: spwu 
  type(pfx_t), intent(in) :: pfx
  type(receiver_desc_t), intent(in) :: rdesc
  type(receiver_source_t), intent(in) :: rsou
  type(receiver_tune_t), intent(in) :: rtune
  type(frequency_box_t), intent(inout) :: cplotb
  real(kind=8), intent(in) :: ry1,ry2
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='PLOT'
  integer(kind=4) :: il
  real(kind=8)  :: soff,spos
  type(draw_rect_t) :: sprec
  real(kind=8) :: frf,fmin,fmax,if2m(2),fif1
  logical :: bandedge(2)
  !
  soff=0.
  spos=0.
  ! get if2 limits of the spw
  call pfx_spw_minmax_if2(rname,pfx,spwu,if2m,bandedge,error)
  if (error) return
  ! loop on both extremities of the SPW
  do il=1,2
    ! Don't do if band edge - can't plot doppler out of range
    if (bandedge(il)) cycle
    ! get value for doppler min and max of the if2 lower limit of spw
    call if2toif1(rdesc%flo2,if2m(il),spwu%bb_code,fif1,error)
    if (error) return
    ! Dopmin
    call if1torf(rtune%flo1dmin,fif1,spwu%sb_code,frf,error)
    if (error) return
    call rftorest(rsou%dopmin,frf,fmin,error)
    if (error) return
    ! Dopmax
    call if1torf(rtune%flo1dmax,fif1,spwu%sb_code,frf,error)
    if (error) return
    call rftorest(rsou%dopmax,frf,fmax,error)
    if (error) return
    sprec%xmin=fmin
    sprec%xmax=fmax
    sprec%ymin=ry1
    sprec%ymax=ry2
    call rec_draw_hrect(sprec,cplotb%rest,error)
    if (error) return
    call gr_pen(colour=adefcol,error=error)
    if (error) return
  enddo ! il
  !
end subroutine draw_dopminmax_spw
!
subroutine noema_oplot_dopminmax(inoema,cplot,drawaxis,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_oplot_dopminmax
  use astro_noema_type
  !-----------------------------------------------------------------------
  ! @ private
  ! Show what can be the variation on the HR SPW coverage 
  ! with the evolution of the Earth doppler
  !
  !-----------------------------------------------------------------------
  type(noema_t), intent(inout) :: inoema
  type(current_boxes_t), intent(inout) :: cplot
  type(frequency_axis_t), intent(in) :: drawaxis
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='PLOT'
  integer(kind=4) :: ispw,ib
  real(kind=8)  :: srect,sbox,soff,spos,yh,yv
  type(draw_rect_t) :: sprec
  logical :: yesh,yesv
  character(len=256) :: comm
  !
  call gr_execl('CHANGE DIRECTORY')
  !
  !
  soff=0.
  spos=0.
  do ib=1,cplot%nbox
    if (cplot%box(ib)%sb_code.eq.0.and.cplot%desc%plotmode.eq.pm_receiver) cycle
    sbox=cplot%box(ib)%rest%ymax-cplot%box(ib)%rest%ymin
    write (comm,'(a,i0)') 'CHANGE DIRECTORY BOX',ib
    call gr_execl(comm)
    yesh=.false.
    yesv=.false.
    do ispw=1,inoema%cfebe%spw%out%n_spw
      if (.not.inoema%cfebe%spw%out%win(ispw)%flexible) cycle
      if ((inoema%cfebe%spw%out%win(ispw)%restmin.ge.cplot%box(ib)%rest%xmin) &
        .and. (inoema%cfebe%spw%out%win(ispw)%restmax.le.cplot%box(ib)%rest%xmax)) then
        call noema_spw_getcol(rname,inoema%cfebe%spw%out%win(ispw),inoema%cfebe%pfx,icol_hatch,sprec%col,error)
        ! define position of the drawings
        if (cplot%desc%plotmode.eq.pm_receiver) then
          spos = sbox/2d0
          soff=3*sbox/50d0
        else if (cplot%desc%plotmode.eq.pm_proposal) then
          spos=sbox
          soff=3d0*sbox/20d0
        else if (cplot%desc%plotmode.eq.pm_basebands) then
          spos=sbox
          soff=0
        endif
        srect=(spos-4d0*soff)/2d0
        if (inoema%cfebe%spw%out%win(ispw)%conflict.or.inoema%cfebe%spw%out%win(ispw)%overload) then
          sprec%col=aconflictcolh
        endif
        if (inoema%cfebe%spw%out%win(ispw)%pol_code.eq.hpol_code) then
          sprec%ymax=spos-soff
          sprec%ymin=sprec%ymax-srect
          yh=(sprec%ymax+sprec%ymin)/2d0
          yesh=.true.
        else if (inoema%cfebe%spw%out%win(ispw)%pol_code.eq.vpol_code) then
          sprec%ymin=cplot%box(ib)%rest%ymin+soff
          sprec%ymax=sprec%ymin+srect
          yv=(sprec%ymax+sprec%ymin)/2d0
          yesv=.true.
        else
          call astro_message(seve%e,rname,'Problem with polarizations')
          error=.true.
          return
        endif
        !
        call draw_dopminmax_spw(inoema%cfebe%spw%out%win(ispw),inoema%cfebe%pfx,inoema%recdesc,  & 
                   inoema%source,inoema%cfebe%rectune,cplot%box(ib),sprec%ymin,sprec%ymax,error)
        if (error) return
      endif
    enddo ! ispw
    !redraw the box
    call rec_draw_fbox(cplot,ib,drawaxis,error)
    if (error) return
    call gr_execl('CHANGE DIRECTORY')
  enddo ! ib
  !
end subroutine noema_oplot_dopminmax
!
subroutine pfx_spw_minmax_if2(rname,pfx,spwu,if2m,edge,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>pfx_spw_minmax_if2
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Get if2 limits of a spw
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(spw_unit_t), intent(in) :: spwu
  type(pfx_t), intent(in) :: pfx
  real(kind=8), intent(inout) :: if2m(2)
  logical, intent(inout) :: edge(2)
  logical, intent(inout) :: error
  ! local
  type(pfx_type_t) :: pfxtype
  integer(kind=4) :: iu,im,it
  !
  !Get the unit, mode and type number for the current spw
  do iu=1,pfx%n_units
    if (spwu%label.ne.pfx%unit(iu)%label) cycle
    im=pfx%unit(iu)%imode
    do it=1,pfx%unit(iu)%mode(im)%n_types
      pfxtype=pfx%unit(iu)%mode(im)%chtype(it)
      edge(1)=spwu%ich1.eq.1
      edge(2)=spwu%ich2.eq.pfxtype%n_chunks
      ! Compute if2min and if2max
      if2m(1)=pfxtype%if2ch0+(spwu%ich1-1)*pfxtype%width_chunk-pfxtype%width_chunk/2d0
      if2m(2)=pfxtype%if2ch0+(spwu%ich2-1)*pfxtype%width_chunk+pfxtype%width_chunk/2d0
    enddo
  enddo
  !
end subroutine pfx_spw_minmax_if2
!
subroutine noema_default_pms(pms,error)
  use astro_interfaces, except_this=>noema_default_pms
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Reset the structure containing info to be passed to Proposal Management System
  !-----------------------------------------------------------------------
  type(info_pms_t), intent(inout)   :: pms
  logical, intent(inout) :: error
  ! Local
  integer(kind=4) :: ir
  !
  pms%redshift=.false.
  pms%tuning_freq=0d0
  pms%tuning_ongrid=.false.
  pms%lsrlim(1,1) = 0d0
  pms%lsrlim(1,2) = 0d0
  pms%lsrlim(2,1) = 0d0
  pms%lsrlim(2,2) = 0d0
  pms%frep=0d0
  pms%freplsr=0d0
  pms%npolrep=0d0
  pms%resrep=0d0
  pms%lsrrange=0d0
  pms%zrange=0d0
  pms%mflex=0d0
  pms%nflex=0d0
  pms%nspw=0
  pms%nresol=0
  pms%fcont=0d0
  pms%dfcont=0d0
  pms%rescont=0d0
  do ir=1,m_chtypes
    pms%resol(ir)=-1d0
    pms%npol(ir)=-1
  enddo !ir
  pms%tracksharing=.false.
  pms%tsdf(1)=0d0
  pms%tsdf(2)=0d0
  !
end subroutine noema_default_pms
!
subroutine noema_get_fcontw20(frep,rs,fcontw20,error)
  use gbl_message
  use astro_interfaces, except_this=>noema_get_fcontw20
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Special routine to compute a continuum freq as done in PMS W20 session
  ! so that PMS and SMS give same results
  !-----------------------------------------------------------------------
  real(kind=8), intent(in)   :: frep
  type(receiver_source_t), intent(in)   :: rs
  real(kind=8), intent(inout) :: fcontw20
  logical, intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='OMS'
  type(noema_tuning_comm_t) :: w20noematune
  type(noema_if_t) :: w20noema_if
  type(receiver_desc_t) :: w20rdesc
  type(receiver_source_t) :: w20rsou
  type(receiver_comm_t) :: w20rcomm
  type(receiver_tune_t) :: w20rtune
  !
  !Define receiver parameters
  call astro_def_receiver(rname,'NOEMA',w20rdesc,error)
  if (error) return
  w20rsou = rs
  !
  call noema_reset_setup(w20noematune,w20noema_if,error)
  if (error) return
  !
  w20noematune%frest=frep
  w20noematune%dotune=.true.
  w20noematune%fixedfreq=.true.
  !
  call noema_default_tuning(rname,w20rdesc,w20rsou%dopshift,w20noematune,error)
  if (error) return
  call noema_setup(rname,w20noematune,w20rcomm,w20rdesc,w20rsou,w20rtune,error)
  if (error) return
  !
  fcontw20=w20rtune%flotune
  !
end subroutine noema_get_fcontw20
!
subroutine noema_print_pms(infopms,error)
  use gbl_message
  use astro_interfaces, except_this=>noema_print_pms
  use astro_types
  use ast_astro
  !-----------------------------------------------------------------------
  ! @ private
  ! Print all info relevant for Proposal Management System
  !-----------------------------------------------------------------------
  type(info_pms_t), intent(in)   :: infopms
  logical, intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='OMS'
  integer(kind=4) :: ir
  character(len=256) :: mess1,mess2
  !
  ! Tuning parameters
  write (mess1,'(a,1x,i0)')  'Tuning Band =',infopms%tuning_band
  call astro_message(seve%i,rname,trim(mess1))
  write (mess1,'(a,1x,f0.6,1x,a)')  'Tuning REST Frequency =',infopms%tuning_freq,'GHz'
  call astro_message(seve%i,rname,trim(mess1))
  write (mess1,'(a,1x,f0.6,1x,a)')  'Tuning LO Frequency =',infopms%tuning_flo1,'GHz'
  call astro_message(seve%i,rname,trim(mess1))
  write (mess1,'(a,1x,f0.6,1x,a)')  'Tuning MIN RF Frequency =',infopms%tuning_minrf,'GHz'
  call astro_message(seve%i,rname,trim(mess1))
  write (mess1,'(a,1x,f0.6,1x,a)')  'Tuning MAX RF Frequency =',infopms%tuning_maxrf,'GHz'
  call astro_message(seve%i,rname,trim(mess1))
  !
  if (infopms%tuning_ongrid) then
    mess1='Tuning ON grid'
  else
    mess1='Tuning OFF grid'
  endif
  call astro_message(seve%i,rname,trim(mess1))
  !
  if (infopms%tuning_outlo) then
    mess1='LO tuning frequency is out of recommended range'
    call astro_message(seve%w,rname,trim(mess1))
  endif
  ! LSR/z ranges
  if (infopms%redshift) then
    write (mess1,'(a,1x,f0.3,1x,a)') 'Half the most narrow SPW is equivalent to an offset of', &
                        infopms%zrange,'in redshift'
  else
    write (mess1,'(a,1x,f0.3,1x,a)') 'Half the most narrow SPW is equivalent to an offset of', &
                        infopms%lsrrange,'km/s in LSR velocity'
  endif
  call astro_message(seve%i,rname,mess1)
  ! Correlator usage
  if (infopms%mflex.ne.0) then
    write (mess1,'(a,1x,i0,a)') 'Flexible part of the correlator is used at',nint(100.*infopms%nflex/infopms%mflex),'%'
    call astro_message(seve%i,rname,trim(mess1))
  endif
  ! Warning if Chunk 1 is used
  if (infopms%chunk1) then
    mess1='Setup uses chunk #1. Might not be possible'
    call astro_message(seve%w,rname,trim(mess1))
  endif
  ! Continuum part
  write (mess1,'(a,1x,f0.6,1x,a)')  &
                'Representative REST frequency for continuum =',infopms%fcont*ghzpermhz,'GHz'
  call astro_message(seve%i,rname,trim(mess1))
  write (mess1,'(a,1x,f0.6,1x,a)')  &
                'Representative LSR frequency for continuum =',infopms%fcontlsr*ghzpermhz,'GHz'
  call astro_message(seve%i,rname,trim(mess1))
  if (oms_bugw20) then
    ! Special case for SMS W20 session - special continuum frequency
    ! used in the W20 (v6) engine of PMS
    write (mess1,'(a,1x,f0.6,1x,a)')  &
                  'SMSW20 Representative LSR frequency for continuumSMSW20 =',infopms%fcontw20*ghzpermhz,'GHz'
    call astro_message(seve%i,rname,trim(mess1))
  endif
  write (mess1,'(a,i0,1x,a,1x,f0.6,1x,a)') &
                'Total bandwidth for continuum (',infopms%npolcont,' polar) =',infopms%dfcont,'MHz'
  call astro_message(seve%i,rname,trim(mess1))
  write (mess1,'(a,1x,f0.6,1x,a)') &
               'Channel spacing for continuum =',infopms%rescont,'MHz'
  call astro_message(seve%i,rname,trim(mess1))
  ! Representative frequency
  if (infopms%nresol.le.0) return
  write (mess1,'(a,1x,f0.6,1x,a,1x,i0,1x,a)') &
                'Representative Frequency =',infopms%frep, &
                'GHz covered by',infopms%nspw,' spectral windows'
  call astro_message(seve%i,rname,mess1)
  do ir=1,infopms%nresol
    write (mess1,'(i0,1x,a,1x,f0.1,1x,a)') &
        infopms%npol(ir), &
        'polarizations at hardware df=',infopms%resol(ir),'kHz'
    call astro_message(seve%i,rname,trim(mess1))
  enddo
  if (infopms%resrep.ne.0) then
    write (mess1,'(i0,1x,a,1x,f0.1,1x,a)') &
          infopms%npolrep, &
          'polarizations at requested df=',infopms%resrep,'kHz'
    call astro_message(seve%i,rname,trim(mess1))
  endif
  ! LSR freq for Tsys and sensitivity
  write (mess1,'(a,1x,f0.3,1x,a)') &
                'LSR Representative Frequency =',infopms%freplsr,'GHz'
  call astro_message(seve%i,rname,mess1)
  do ir=1,m_sideband
    write (mess1,'(a,1x,a,1x,f0.3,1x,a,1x,f0.3,1x,a)') &
                  sideband(ir),'LSR Min Max =',infopms%lsrlim(ir,1),'-',infopms%lsrlim(ir,2),'GHz'
    call astro_message(seve%i,rname,mess1)
  enddo
  ! Tracksharing case
  if (infopms%tracksharing) then
    ! Min
    if (infopms%redshift) then
      write (mess1,'(a)')  'redshift'
    else
      write (mess1,'(a)')  'VLSR'
    endif
    write (mess2,'(a,1x,a,1x,a,1x,i0,1x,a)') & 
          'Track-sharing: Freq offset for source with lowest', &
          trim(mess1),'=',nint(infopms%tsdf(1)),'MHz'
    call astro_message(seve%i,rname,mess2)
    ! Max
    write (mess2,'(a,1x,a,1x,a,1x,i0,1x,a)') & 
          'Track-sharing: Freq offset for source with highest', & 
          trim(mess1),'=',nint(infopms%tsdf(2)),'MHz'
    call astro_message(seve%i,rname,mess2)
    ! Case when TUNING or REPRESENTATIV gets out of ranges
    if (infopms%ts_freqout) then
      write (mess1,'(a)') "Track-sharing: offset between sources might push lines out of range"
      call astro_message(seve%i,rname,mess1)
    endif
  endif
  !
end subroutine noema_print_pms
!
subroutine noema_draw_frep(freq,spwout,cplot,infopms,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_draw_frep
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Draw the position of a repreentative freq (might be different form tuning freq)
  ! Needed for PMS
  !-----------------------------------------------------------------------
  real(kind=8), intent(in)      :: freq
  type(spw_output_t), intent(in) :: spwout
  type(current_boxes_t), intent(inout) :: cplot
  type(info_pms_t), intent(inout)   :: infopms
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='PLOT'
  integer(kind=4), parameter :: mres=2
  integer(kind=4) :: ib,iw,nspw,nres,ir,npol(mres)
  integer(kind=4) :: repspw(spwout%n_spw),reppol(spwout%n_spw)
  character(len=5) :: replabel(spwout%n_spw)
  real(kind=8)  :: repres(spwout%n_spw),res(mres)
  type(draw_line_t) :: fline
  character(len=256) :: comm,mess,mess1
  !
  !Search the spw covering the representative frequency
  do iw=1,spwout%n_spw
    repspw(iw)=0
    repres(iw)=0
    reppol(iw)=0
  enddo
  do ir=1,mres
    npol(ir)=0
  enddo
  nspw=0
  nres=0
  do iw=1,spwout%n_spw
    if (freq.lt.spwout%win(iw)%restmin.or.freq.ge.spwout%win(iw)%restmax) cycle
    nspw=nspw+1
    repspw(nspw)=iw
    repres(nspw)=spwout%win(iw)%resol
    reppol(nspw)=spwout%win(iw)%pol_code
    replabel(nspw)=spwout%win(iw)%label
    if (nspw.eq.1.or.spwout%win(iw)%resol.ne.repres(nspw-1)) then
      nres=nres+1
      res(nres)=spwout%win(iw)%resol
      infopms%resol(nres)=spwout%win(iw)%resol
    endif
  enddo
  infopms%nresol=nres
  infopms%nspw=nspw
  do ir=1,nres
    do iw=1,nspw
      if (repres(iw).eq.res(ir)) then
        npol(ir)=npol(ir)+1
        infopms%npol(ir)=npol(ir)
      endif
    enddo
  enddo
  if (nspw.eq.0) then
    call noema_default_pms(infopms,error)
    if (error) return
    write (mess,'(a,1x,f0.3,1x,a)')  'Representative frequency',freq,'MHz is not in the observed range'
    call astro_message(seve%e,rname,mess)
    error=.true.
    return
  endif
  if (infopms%resrep.ne.0) then
    if (infopms%nresol.gt.1) then
      ! Required resolution wider than widest channel
      if (infopms%resrep.ge.maxval(infopms%resol(1:infopms%nresol))) then
        infopms%npolrep=maxval(infopms%npol)
      ! Required resolution narrower than narrowest channel
      else if (infopms%resrep.lt.minval(infopms%resol(1:infopms%nresol))) then
        call astro_message(seve%e,rname,'Resolution is not achievable at representative frequency')
        ! Error is not set to .true. to let the code run and give other available info to PMS
        infopms%npolrep=0
      ! Required resolution is intermediate
      else
        infopms%npolrep=0
        do ir=1,infopms%nresol
          if (infopms%resol(ir).le.infopms%resrep) then
            if (infopms%npol(ir).gt.infopms%npolrep) then
              infopms%npolrep=infopms%npol(ir)
            endif
          endif
        enddo
      endif
    else if (infopms%nresol.eq.1) then
      if (infopms%resrep.lt.infopms%resol(1)) then
        call astro_message(seve%e,rname,'Resolution not achievable at representative frequency')
        ! Error is not set to .true. to let the code run and give other available info to PMS
        infopms%npolrep=0
      else
        infopms%npolrep=infopms%npol(1)
      endif
    else
      call astro_message(seve%e,rname,'Problem with resolution at representative frequency')
      ! Error is not set to .true. to let the code run and give other available info to PMS
      infopms%npolrep=0
    endif
  endif
  write (mess,'(a,1x,i0,1x,a)') 'Representative frequency is covered by',nspw,'spectral windows'
!  call astro_message(seve%i,rname,mess)
!   do iw=1,nspw
!     write (mess,'(a,i3,1x,a,1x,a,a,1x,f8.1,a)') &
!             'SPW #',repspw(iw),'in',replabel(iw),', df=',repres(iw),'kHz'
!     call astro_message(seve%i,rname,mess)
!   enddo
  write (mess1,'(a,1x,i0,1x,a,1x,f0.1,1x,a)') &
      'Representative frequency is covered by',npol(1),'polarizations at df=',res(1),'kHz'
  if (nres.eq.2) then
    write (mess1,'(a,1x,a,1x,i0,1x,a,1x,f0.1,1x,a)') &
      trim(mess1),'and',npol(2),'polarizations at df=',res(2),'kHz'
  endif
!  call astro_message(seve%i,rname,mess1)
  ! Do the plot
  do ib=1,cplot%nbox
    if (freq.lt.cplot%box(ib)%rest%xmin.or.freq.gt.cplot%box(ib)%rest%xmax) cycle
    write (comm,'(a,i0)') 'CHANGE DIRECTORY BOX',ib
    !Draw the arrow
    call gr_execl(comm)
    fline%xmin=freq
    fline%xmax=freq
    fline%ymin=cplot%box(ib)%rest%ymin
    fline%ymax=cplot%box(ib)%rest%ymin+(cplot%box(ib)%rest%ymax-cplot%box(ib)%rest%ymin)*frepheight
    fline%col=afrepcol
    fline%dash=1
    call rec_draw_arrow(fline,cplot%box(ib)%rest,error)
    if (error) return
    !Write the information
!    write (comm,'(a,a,a)') 'DRAW TEXT 0 -6 "',trim(mess1),'" 6 0 /CHARACTER 1'
!     call gr_pen(colour=afrepcol,error=error)
!     call gr_exec1(comm)
    call gr_pen(colour=adefcol,error=error)
    call gr_execl('CHANGE DIRECTORY')
  enddo
  !
end subroutine noema_draw_frep
!
subroutine noema_trackshare(dolsr,ival,rsou,frep,frest,cplot,tsdf,ts_freqout,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_trackshare
  use astro_types
  use phys_const
  !-----------------------------------------------------------------------
  ! @ private
  ! COmpute offset of trackshared sources with min and max values (LSR or REDSHIFT)
  ! developped for SMS
  !-----------------------------------------------------------------------
  logical, intent(in)  :: dolsr ! if true velo, else redshift
  real(kind=8), intent(in)      :: ival(2) ! vmin and vmax
  type(receiver_source_t), intent(in) :: rsou  ! full receiver parameters
  real(kind=8), intent(in)      :: frep
  real(kind=8), intent(in)      :: frest
  type(current_boxes_t), intent(inout) :: cplot
  real(kind=8), intent(inout) :: tsdf(2)
  logical, intent(inout) :: ts_freqout
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='NEWVEL'
  integer(kind=4) :: iv
  logical :: arrow_plot
  real(kind=8) :: freq,dshift,val(2),eshift,zshift
  character(len=256) :: mess
  !
  val(1)=minval(ival)
  val(2)=maxval(ival)
  ts_freqout=.false.
  do iv=1,2
    if (dolsr) then
      ! source type is VLSR
      dshift=rsou%dopshift-(val(iv)-rsou%vlsr)/clight_kms
    else
      ! Source type is REDSHIFT
      eshift=(1d0-(rsou%vdop-rsou%vlsr)/clight_kms)
      zshift= 1d0/(1d0+val(iv)) 
      dshift=zshift+eshift-1d0 
    endif
    ! Tuning freq
    freq=frest*dshift ! FRF
    call noema_draw_trackshare(freq,'TUNING',cplot,arrow_plot,error)
    tsdf(iv) = freq-(frest*rsou%dopshift)
    if (error) return
    if (.not.arrow_plot) then
      ! Case when arrow out of boxes
      ts_freqout=.true.
      if (dolsr) then
        write (mess,'(a,1x,f0.3,1x,a)') "Tuning Frequency out of box for vLSR =",val(iv),"km/s"
      else
        write (mess,'(a,1x,f0.6)') "Tuning Frequency out of box for z =",val(iv)
      endif
      call astro_message(seve%w,rname,mess)
    endif
    ! Repr freq
    if (frep.eq.undef_freq) cycle
    freq=frep*dshift ! FRF
    call noema_draw_trackshare(freq,'REP',cplot,arrow_plot,error)
    if (error) return    
    if (.not.arrow_plot) then
      ! Case when arrow out of boxes
      ts_freqout=.true.
      if (dolsr) then
        write (mess,'(a,1x,f0.3,1x,a)') "Representative Frequency out of box for vLSR =",val(iv),"km/s"
      else
        write (mess,'(a,1x,f0.6)') "Representative Frequency out of box for z =",val(iv)
      endif
      call astro_message(seve%w,rname,mess)
    endif
    !
  enddo
  !
end subroutine noema_trackshare
!
subroutine noema_draw_trackshare(frf,freqtype,cplot,plotok,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_draw_trackshare
  use astro_types
  use phys_const
  !-----------------------------------------------------------------------
  ! @ private
  ! Draw the position of sources with min and max velocities in case of tracksharing
  ! Needed for SMS
  !-----------------------------------------------------------------------
  real(kind=8), intent(in)      :: frf ! freq to display in RF
  character(len=*), intent(in) :: freqtype ! rep or tuning freq
  type(current_boxes_t), intent(inout) :: cplot
  logical, intent(inout) :: plotok
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='NEWVEL'
  integer(kind=4) :: ib
  type(draw_line_t) :: fline
  character(len=256) :: comm
  character(len=16) :: acol
  real(kind=4) :: arrheight
  !
  plotok = .false. ! number of plotted arrow
  if (freqtype.eq.'TUNING') then
    acol=altunecol
    arrheight=ltuneheight*3./4.
  else if (freqtype.eq.'REP') then
    acol=afrepcol
    arrheight=frepheight*3./4.
  else
    call astro_message(seve%e,rname,'FREQUENCY TYPE NOT UNDERSTOOD')
    error=.true.
    return
  endif
  do ib=1,cplot%nbox
    if (frf.lt.cplot%box(ib)%rf%xmin.or.frf.gt.cplot%box(ib)%rf%xmax) cycle
    write (comm,'(a,i0)') 'CHANGE DIRECTORY BOX',ib
    !Draw the arrow
    call gr_execl(comm)
    fline%xmin=frf
    fline%xmax=frf
    fline%ymin=cplot%box(ib)%rf%ymin
    fline%ymax=cplot%box(ib)%rf%ymin+(cplot%box(ib)%rf%ymax-cplot%box(ib)%rf%ymin)*arrheight
    fline%col=acol
    fline%dash=2
    call rec_draw_arrow(fline,cplot%box(ib)%rf,error)
    if (error) return
    plotok=.True.  
    call gr_pen(colour=adefcol,error=error)
    call gr_execl('CHANGE DIRECTORY')
  enddo
  !
end subroutine noema_draw_trackshare
!
subroutine noema_draw_flo(rtune,cplot,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_draw_flo
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Draw the position of the LO and indicate whether the tuning is ON grid or not
  !-----------------------------------------------------------------------
  type(receiver_tune_t), intent(in) :: rtune
  type(current_boxes_t), intent(inout) :: cplot
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='PLOT'
  type(draw_line_t) :: fline
  character(len=256) :: comm,mess,mess1,mess2,locol,hugechar,defchar
  integer(kind=4) :: ib
  !
  write (hugechar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%hugechar
  write (defchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%defchar
  !Prepare the message
  write (mess,'(a,1x,f8.3,1x,a)') 'Tuning LO frequency =',rtune%flotune*ghzpermhz,'GHz'
  locol=adefcol
  mess1=''
  mess2=''
  if (.not.rtune%ongrid) then
    mess1='(Not on tuning grid)'
  endif
  if (rtune%outlo) then
    locol=aconflictcol
  endif
  ! Do the plot
  do ib=1,cplot%nbox
    if (rtune%flotune.lt.cplot%box(ib)%lsr%xmin.or.rtune%flotune.gt.cplot%box(ib)%lsr%xmax) cycle
    write (comm,'(a,i0)') 'CHANGE DIRECTORY BOX',ib
    !Draw the arrow
    call gr_execl(comm)
    fline%xmin=rtune%flotune
    fline%xmax=rtune%flotune
    fline%ymin=cplot%box(ib)%lsr%ymin
    fline%ymax=cplot%box(ib)%lsr%ymin+(cplot%box(ib)%lsr%ymax-cplot%box(ib)%lsr%ymin)*9./16.
    fline%dash=1
    fline%col=locol
    call rec_draw_arrow(fline,cplot%box(ib)%lsr,error)
    if (error) return
    !   
    !Write the information
    call gr_pen(colour=locol,error=error)
    write (comm,'(a,a,a)') 'DRAW TEXT 0 -1.5 "',trim(mess),'" /character 8'
    call gr_exec1(comm)
    if (.not.rtune%ongrid) then
      write (comm,'(a,a,a)') 'DRAW TEXT 0 -2.5 "',trim(mess1),'" /character 8'
      call gr_exec1(comm)
    endif
    call gr_pen(colour=adefcol,iweight=1,error=error)
    call gr_execl('CHANGE DIRECTORY')
  enddo
  !
end subroutine noema_draw_flo
!
subroutine noema_draw_summary(inoema,cplot,molecules,drawaxis,error)
  use gbl_message
  use gbl_ansicodes
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_draw_summary
  use astro_noema_type
  use ast_astro
  !-----------------------------------------------------------------------
  ! @ private
  ! Draw coverage of spectral units independently of correlator units
  ! for summary purpose
  !
  !-----------------------------------------------------------------------
  type(noema_t), intent(in) :: inoema
  type(current_boxes_t), intent(inout) :: cplot
  type(plot_molecules_t), intent(in) :: molecules
  type(frequency_axis_t), intent(in) :: drawaxis
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='PLOT'
  integer(kind=4) :: ispw,ib,ibb,overload,conflict
  integer(kind=4) ::iu,ir,imode,it,mflex,nflex
  real(kind=8)  :: srect,sbox,soff,spos,xt,yt,yh,yv,sw
  real(kind=4)  :: maxybox
  type(draw_rect_t) :: sprec
  integer(kind=4) :: nres
  real(kind=4) :: res(m_res)
  character(len=12) :: rescol(m_res)
  logical :: yesh,yesv,chunk1,alloff,outlo,badsetup
  character(len=256) :: comm,molchar,defchar
  !
  write (defchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%defchar
  write (molchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%molchar
  call gr_execl('CHANGE DIRECTORY')
  !
  ! check whether more resource than available are used or if chunks are used by more than 1 spw
  chunk1=.false.
  call noema_pfx_status(inoema%cfebe%pfx,overload,conflict,chunk1,alloff,error)
  if (error) return
  if (alloff) then
    call astro_message(seve%e,rname,'Backend not defined')
    error=.true.
    return
  endif
  !
  ! Get usage of correlator (flexible SPW + number of Resolutions)
  mflex=0
  nflex=0
  spos=0.
  soff=0.
  nres=0
  do ir=1,m_res
    res(ir)=undef_freq
  enddo
  do iu=1,inoema%cfebe%pfx%n_units
    imode=inoema%cfebe%pfx%unit(iu)%imode
    if (imode.le.0) cycle ! does not plot unused baseband
    do it=1,inoema%cfebe%pfx%unit(iu)%mode(imode)%n_types
      if (.not.(any((res(1:m_res)).eq.inoema%cfebe%pfx%unit(iu)%mode(imode)%chtype(it)%df_chunks))) then
        nres=nres+1
        res(nres)=inoema%cfebe%pfx%unit(iu)%mode(imode)%chtype(it)%df_chunks
        rescol(nres)=inoema%cfebe%pfx%unit(iu)%mode(imode)%chtype(it)%itypecol(icol_fill)
      endif
      if (.not.(inoema%cfebe%pfx%unit(iu)%mode(imode)%chtype(it)%move_chunk)) cycle
      mflex=mflex+inoema%cfebe%pfx%unit(iu)%mode(imode)%chtype(it)%use_chunks
      nflex=nflex+inoema%cfebe%pfx%unit(iu)%mode(imode)%chtype(it)%usage
    enddo ! it
  enddo  
  !
  maxybox=-1.
  do ib=1,cplot%nbox
    if (cplot%box(ib)%sb_code.eq.0.and.cplot%desc%plotmode.eq.pm_receiver) cycle
    sbox=cplot%box(ib)%rest%ymax-cplot%box(ib)%rest%ymin
    write (comm,'(a,i0)') 'CHANGE DIRECTORY BOX',ib
    call gr_execl(comm)
    yesh=.false.
    yesv=.false.
    do ispw=1,inoema%cfebe%spw%out%n_spw
      if ((inoema%cfebe%spw%out%win(ispw)%restmin.ge.cplot%box(ib)%rest%xmin) &
        .and. (inoema%cfebe%spw%out%win(ispw)%restmax.le.cplot%box(ib)%rest%xmax)) then
        ! get color according to SPW mode/resolution
        call noema_spw_getcol(rname,inoema%cfebe%spw%out%win(ispw),inoema%cfebe%pfx,icol_fill,sprec%col,error)
        if (error) return
        if (cplot%desc%plotmode.eq.pm_receiver) then
          spos = sbox/2d0
          sw = spos/6d0
          soff = sw*(inoema%cfebe%spw%out%win(ispw)%itype-1)
        else if (cplot%desc%plotmode.eq.pm_proposal) then
          spos=3d0*sbox/4
          sw =spos/6d0
          soff = sw*(inoema%cfebe%spw%out%win(ispw)%itype-1)
        else
          call astro_message(seve%e,rname,'Problem with plot mode')
          error=.true.
          return
        endif
        srect=(spos-4d0*soff)/2d0
        if (inoema%cfebe%spw%out%win(ispw)%conflict.or.inoema%cfebe%spw%out%win(ispw)%overload &
            .or.inoema%cfebe%spw%out%win(ispw)%chunk1) then
          sprec%col=aconflictcol
        endif
        sprec%xmin=inoema%cfebe%spw%out%win(ispw)%restmin
        sprec%xmax=inoema%cfebe%spw%out%win(ispw)%restmax
        if (inoema%cfebe%spw%out%win(ispw)%pol_code.eq.hpol_code) then
          sprec%ymax=spos-soff
          sprec%ymin=sprec%ymax-sw
          yh=(sprec%ymax+sprec%ymin)/2d0
          yesh=.true.
        else if (inoema%cfebe%spw%out%win(ispw)%pol_code.eq.vpol_code) then
          sprec%ymax=spos-3d0*sw-soff
          sprec%ymin=sprec%ymax-sw
          yv=(sprec%ymax+sprec%ymin)/2d0
          yesv=.true.
        else
          call astro_message(seve%e,rname,'Problem with polarizations')
          error=.true.
          return
        endif
        call rec_draw_frect(sprec,cplot%box(ib)%rest,error)
        if (error) return
        call noema_spw_getcol(rname,inoema%cfebe%spw%out%win(ispw),inoema%cfebe%pfx,icol_border,sprec%col,error)
        if (error) return
        sprec%dash=1
        call rec_draw_rect(sprec,cplot%box(ib)%rest,error)
        if (error) return
        xt=sprec%xmin
        yt=sprec%ymin
        call gr_pen(colour=sprec%col,error=error)
        if (error) return
        write (comm,'(a,1x,f0.3,1x,f0.3,1x,a,i0,a)') 'DRAW TEXT',xt,yt,'"L',ispw,'" 9 0 /USER'
        call gr_exec(molchar)
        call gr_exec1(comm)
        call gr_exec(defchar)
        if (inoema%source%sourcetype.eq.soukind_full) then
          call draw_dopminmax_spw(inoema%cfebe%spw%out%win(ispw),inoema%cfebe%pfx,inoema%recdesc, & 
                    inoema%source,inoema%cfebe%rectune,cplot%box(ib),sprec%ymin,sprec%ymax,error)
          if (error) return
        endif
      endif   
    enddo ! ispw
    if (cplot%box(ib)%sb_code.ne.0) then ! Plot confusion zone when possible
      do ibb=1,inoema%recdesc%n_bbands
        call noema_draw_confusion(rname,inoema%recdesc,inoema%cfebe%rectune%flo1,  & 
                inoema%source%dopshift,ibb,cplot%box(ib)%sb_code,cplot%box(ib)%rest,cplot%desc,error)
        if (error) return
      enddo
      call gr_pen(colour=adefcol,error=error)
      if (error) return
    endif
    ! Draw molecules
    call gr_exec1(molchar)
    call rec_draw_molecules(molecules,cplot%box(ib)%rest,error)
    if (error) return
    call gr_exec1(defchar)
    ! Display tuning frequency
    if ((cplot%box(ib)%sb_code.eq.inoema%cfebe%rectune%sb_code).or.(cplot%desc%plotmode.eq.pm_proposal)) then
      call rec_draw_linetune(inoema%cfebe%rectune,cplot%box(ib)%rest,error)
      if (error) return
    endif
    ! caption
    xt=cplot%box(ib)%rest%xmax
    write (comm,'(a,1x,f0.3,1x,f0.3,1x,a)') 'DRAW TEXT',xt,yh,'" H" 6 0 /USER'
    if (yesh) call gr_exec1(comm)
    write (comm,'(a,1x,f0.3,1x,f0.3,1x,a)') 'DRAW TEXT',xt,yv,'" V" 6 0 /USER'
    if (yesv) call gr_exec1(comm)
    xt=cplot%box(ib)%rest%xmin
    write (comm,'(a,1x,f0.3,1x,f0.3,1x,a)') 'DRAW TEXT',xt,yh,'"H " 4 0 /USER'
    if (yesh) call gr_exec1(comm)
    write (comm,'(a,1x,f0.3,1x,f0.3,1x,a)') 'DRAW TEXT',xt,yv,'"V " 4 0 /USER'
    if (yesv) call gr_exec1(comm)
    !redraw the box
    call rec_draw_fbox(cplot,ib,drawaxis,error)
    if (error) return
    call gr_execl('CHANGE DIRECTORY')
  enddo ! ib
  !
  ! Draw legend and warnings
  outlo=inoema%cfebe%rectune%outlo
  call noema_draw_warning(rname,cplot,outlo,chunk1,overload,conflict,error)
  if (error) return
  ! Then other things
  badsetup=(overload.gt.0.or.conflict.gt.0)
  call noema_draw_legend(rname,cplot,nres,res,rescol,badsetup,nflex,mflex,error)
  if (error) return
  !
  call gr_execl('CHANGE DIRECTORY')
  call gr_pen(colour=adefcol,error=error)
  if (error) return
  !
end subroutine noema_draw_summary
!
subroutine noema_draw_legend(rname,cplot,nr,r,rcol,badsetup,nflex,mflex,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_draw_legend
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! draw title and warning message 
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(current_boxes_t), intent(inout)          :: cplot
  integer(kind=4), intent(in) :: nr
  real(kind=4), intent(in) :: r(m_res)
  character(len=*), intent(in) :: rcol(m_res)
  logical, intent(in) :: badsetup
  integer(kind=4), intent(in) :: nflex
  integer(kind=4), intent(in) :: mflex 
  logical, intent(inout) :: error
  ! local
  type(frequency_box_phys_t)     :: lbox
  type(draw_mark_t) :: mark
  character(len=512) :: comm
  real(kind=4) :: resoff
  integer(kind=4) :: ir
  !    
  if (.not.gtexist('<GREG<BOXLEGEND')) then
    call rec_def_legendbox(rname,lbox,cplot,error)
    if (error) return
    write (comm,'(a,i0)') 'CREATE DIRECTORY BOXLEGEND'
    call gr_execl(comm)
    write (comm,'(a,i0)') 'CHANGE DIRECTORY BOXLEGEND'
    call gr_execl(comm)
    ! Locate the box
    call rec_locate_fbox(lbox,error)
  else
    write (comm,'(a,i0)') 'CHANGE DIRECTORY BOXLEGEND'
    call gr_execl(comm)
  endif
  !
  do ir=1,nr ! loop on number of resolution used
    resoff = -4+(ir-1)
    mark%col=rcol(ir)
    mark%nside=4
    mark%style=3
    mark%s=cplot%desc%defchar
    mark%x=0.5
    mark%y=resoff
    mark%ref='CHARACTER'
    mark%iref=1
    call rec_draw_mark(mark,error)
    if (error) return
    mark%col=adefcol
    mark%style=0
    call rec_draw_mark(mark,error)
    if (error) return
    call gr_pen(colour=adefcol,error=error)
    if (error) return
    write (comm,'(a,f0.0,a,1x,f0.1,1x,a)') 'DRAW TEXT 1 ',resoff,' "df =', & 
                                           r(ir)*khzpermhz,'kHz" 6 0 /CHARACTER 1'
    call gr_exec1(comm)
  enddo
  ! write usage of flexible chunks only for valid setups
  if (.not.(badsetup)) then
    if (mflex.gt.0) then
      write (comm,'(a,1x,i0,a,i0,1x,a)') 'DRAW TEXT 0 5 "',nflex,'/',mflex,'flexible chunks used" 6 0 /CHARACTER 7'
      call gr_exec1(comm)
    endif
  endif

  !
end subroutine noema_draw_legend
!
subroutine noema_draw_warning(rname,cplot,outlo,chunk1,overload,conflict,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_draw_warning
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! draw title and warning message 
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(current_boxes_t), intent(inout)          :: cplot
  logical, intent(in) :: outlo
  logical, intent(in) :: chunk1
  integer(kind=4), intent(in) :: overload
  integer(kind=4), intent(in) :: conflict
  logical, intent(inout) :: error
  ! local
  type(frequency_box_phys_t)     :: lbox
  character(len=512) :: comm,hugechar,defchar
  real(kind=4) :: xt,yt
  logical :: badsetup
  !    
  write (hugechar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%hugechar
  write (defchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%defchar
  !
  if (.not.gtexist('<GREG<BOXLEGEND')) then
    call rec_def_legendbox(rname,lbox,cplot,error)
    if (error) return
    write (comm,'(a,i0)') 'CREATE DIRECTORY BOXLEGEND'
    call gr_execl(comm)
    write (comm,'(a,i0)') 'CHANGE DIRECTORY BOXLEGEND'
    call gr_execl(comm)
    ! Locate the box
    call rec_locate_fbox(lbox,error)
  else
    write (comm,'(a,i0)') 'CHANGE DIRECTORY BOXLEGEND'
    call gr_execl(comm)
  endif
  !
  badsetup=(overload.gt.0.or.conflict.gt.0)
  ! Draw  warnings/errors
  ! tuning out of lo range
  if (outlo) then
    call gr_pen(colour=aconflictcolplot,iweight=3,error=error)
    if (error) return
    call gr_exec1(hugechar)
    write (comm,'(a)') &
      'DRAW TEXT ''(box_xmin+box_xmax)/2-box_xmin'' ''(box_ymax+box_ymin)/2-box_ymin'' &
      "LO out of recommended range" 5 ''180/pi*atan2((box_ymax-box_ymin),(box_xmax-box_xmin))'''
    call gr_exec1(comm)
    call gr_exec1(defchar)
    call gr_pen(colour=aconflictcol,iweight=1,error=error)
    if (error) return
    xt=-1
    yt=-4
    write (comm,'(a,1x,f0.3,1x,f0.3,1x,a)') &
      'DRAW TEXT',xt,yt,'"LO out of recommended range" 4 0 /CHARACTER 3'
    call gr_exec1(comm)
  endif
  !
  if (chunk1) then
    call gr_pen(colour=aconflictcol,error=error)
    if (error) return
    write (comm,'(a)') &
      'DRAW TEXT -1 -3 "Configuration using chunk #1. Might not be feasible" 4 0 /CHARACTER 3'
    call gr_exec1(comm)
  endif
  !
  if (.not.(badsetup)) then
    call gr_execl('CHANGE DIRECTORY')
    call gr_pen(colour=adefcol,error=error)
    return
  endif
  ! Add error message when setup is not feasible
  call gr_pen(colour=aconflictcol,error=error)
  if (error) return
  xt=1
  yt=4
  write (comm,'(a,1x,f0.3,1x,f0.3,1x,a)') &
  'DRAW TEXT',xt,yt,'"Setup not feasible:" 6 0 /CHARACTER 7'
  call gr_exec1(comm)
  if (overload.gt.0) then
    xt=1
    yt=2
    write (comm,'(a,1x,f0.3,1x,f0.3,1x,a,i0,1x,a)') & 
    'DRAW TEXT',xt,yt,'"',overload,'units in overload" 6 0 /CHARACTER 7'
    call gr_exec1(comm)
  endif
  if (conflict.gt.0) then
    xt=1
    yt=3
    write (comm,'(a,1x,f0.3,1x,f0.3,1x,a,i0,1x,a)') &
    'DRAW TEXT',xt,yt,'"',conflict,'chunks in conflict (C)" 6 0 /CHARACTER 7'
    call gr_exec1(comm)
  endif
  !
  call gr_execl('CHANGE DIRECTORY')
  !
end subroutine noema_draw_warning
!
subroutine noema_spw_getcol(rname,spu,pfx,ctype,color,error)
  use gbl_message
  use astro_interfaces, except_this=>noema_spw_getcol
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  !Get color to be used to draw a SPW - loop on PFX unit
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(spw_unit_t), intent(in) :: spu
  type(pfx_t), intent(in) :: pfx
  integer(kind=4), intent(in) :: ctype ! which color to use (fill rect or hatch or border)
  character(len=*), intent(inout) :: color
  logical, intent(inout) :: error
  ! local
  integer(kind=4) :: iu
  logical :: foundcol
  !
  foundcol=.false.
  do iu=1,pfx%n_units
    if (spu%label.ne.pfx%unit(iu)%label) cycle
      color=pfx%unit(iu)%mode(pfx%unit(iu)%imode)%chtype(spu%itype)%itypecol(ctype)
      foundcol=.true.
  enddo
  if (.not.foundcol) then
    call astro_message(seve%e,rname,'Could not define plot color')
    error=.true.
    return
  endif
  !
end subroutine noema_spw_getcol
!
subroutine noema_lsrrange(inoema,cplot,infopms,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_lsrrange
  use astro_noema_type
  use phys_const
  !-----------------------------------------------------------------------
  ! @ private
  ! Message about comparison of SPW width and LSR shift
  ! Useful for people to see if a config is OK for different sources
  !-----------------------------------------------------------------------
  type(noema_t), intent(in) :: inoema
  type(current_boxes_t), intent(in) :: cplot
  type(info_pms_t), intent(inout)   :: infopms
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='PLOT'
  !for chunk way
  integer(kind=4) :: iband
  real(kind=8) :: hires,ds2,v2,z2
  character(len=5) :: spwlab
  character(len=200) :: mess,comm
  ! for spw way
  real(kind=8) :: wmin,ww
  integer(kind=4) :: iw,imin
  !
  hires=inoema%cfebe%spw%out%win(inoema%cfebe%spw%out%n_spw)%resol/1d3
  spwlab=inoema%cfebe%spw%out%win(inoema%cfebe%spw%out%n_spw)%label
  iband=inoema%cfebe%spw%out%win(inoema%cfebe%spw%out%n_spw)%iband
  !
!   chwidth=0d0
!   do iu=1,pfx%n_units
!     if (pfx%unit(iu)%label.ne.spwlab) cycle
!     imode=pfx%unit(iu)%imode
!     do it=1,pfx%unit(iu)%mode(imode)%n_types
!       if (pfx%unit(iu)%mode(imode)%chtype(it)%df_chunks.ne.hires) cycle
!       itype=it
!       chwidth=pfx%unit(iu)%mode(imode)%chtype(it)%width_chunk
!     enddo
!   enddo
!   !
!   if (chwidth.eq.0) then
!     call astro_message(seve%e,rname,'Could not determine chunk width')
!     error=.true.
!     return
!   endif
!   !
!   lsrwidth=clight_kms*(chwidth/rec%source%lsrshift)/rec%tune(itune)%frest ! km/s
!   write (mess,'(a,1x,f0.3,1x,a)') 'Method A: 1 chunk is equivalent to',lsrwidth,'km/s in source frame'
!   call astro_message(seve%r,'rname',mess)
  !
!   !2nd method
!   ds2=(rec%tune(itune)%flsr+chwidth)/rec%tune(itune)%flsr*rec%source%lsrshift
!   if (rec%source%z.ne.0) then
!     z2=1d0/ds2-1
!     write (mess,'(a,1x,f0.6,1x,a)') 'Method B: 1 chunk is equivalent to',z2-rec%source%z,'redshift offset'
!   else
!     v2=clight_kms*(1-ds2)
!     write (mess,'(a,1x,f0.3,1x,a)') 'Method B: 1 chunk is equivalent to',v2-rec%source%vlsr,'km/s in source frame'
!   endif
!   call astro_message(seve%r,'rname',mess)
!   !
  ! Other way: width of the narrowest spw
  wmin=1e9
  do iw=1,inoema%cfebe%spw%out%n_spw
    ww=inoema%cfebe%spw%out%win(iw)%restmax-inoema%cfebe%spw%out%win(iw)%restmin
    if (ww.ge.wmin) cycle
    wmin=ww
    imin=iw
  enddo
  ds2=(inoema%cfebe%rectune%flsr+wmin/2d0)/inoema%cfebe%rectune%flsr*inoema%source%lsrshift
  if (inoema%source%z.ne.0) then
    z2=1d0/ds2-1
    write (mess,'(a,1x,f0.6,1x,a)') &
    'Half the most narrow SPW is equivalent to an offset of',abs(z2-inoema%source%z),'in redshift'
    infopms%zrange=abs(z2-inoema%source%z)
    infopms%redshift=.true.
  else
    v2=clight_kms*(1-ds2)
    write (mess,'(a,1x,f0.3,1x,a)') &
    'Half the most narrow SPW is equivalent to an offset of',abs(v2-inoema%source%vlsr),'km/s in source LSR velocity'
    infopms%lsrrange=abs(v2-inoema%source%vlsr)
    infopms%redshift=.false.
  endif
!   call astro_message(seve%r,'rname',mess)
  !
  if (cplot%nbox.ne.1) then
    call astro_message(seve%e,rname,'Could not determine the current plot nothing is drawn')
    error = .true.
    return
  endif
  !Write the information
  call gr_execl('CHANGE DIRECTORY BOX1')
  write (comm,'(a,a,a)') 'DRAW TEXT 0 -5 "',trim(mess),'" 6 0 /CHARACTER 1'
  call gr_pen(colour=adefcol,error=error)
  call gr_exec1(comm)
  call gr_execl('CHANGE DIRECTORY')
  !
end subroutine noema_lsrrange
!
subroutine noema_changevelo(line,error)
  use gbl_message
  use gbl_constant
  use gkernel_interfaces
  use gkernel_types
  use astro_interfaces, except_this=>noema_changevelo
  use ast_astro
  use astro_noema_type
  use phys_const
  use my_receiver_globals
  use plot_molecules_globals
  !-----------------------------------------------------------------------
  ! @ private
  ! NEWVEL to check what will be the freq coverage for a different source velocity of redshift
  ! Syntax:
  ! NEWVEL LSR|REDSHIFT value[km/s|redshift] [toleranceMHz]
  !                     [/SETUP LineName ReprFreqGHz SpectraResolkHz /LINES]
  ! Does not change antything in the current setup
  !-----------------------------------------------------------------------
  character(len=*), intent(inout) :: line        ! command line
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='NEWVEL'
  integer(kind=4), parameter :: optfixed=1
  integer(kind=4), parameter :: optsetup=2
  integer(kind=4), parameter :: optline=3
  integer(kind=4), parameter :: opttrackshare=4
  type(noema_t) :: newnoema
  type(plot_molecules_t) :: cata
  type(info_pms_t) :: infopms
  type(frequency_axis_t) :: drawaxis
  real(kind=8) :: newv,tolf,offset,frep,dfrep,frepin,tsvel(2),tsdf(2)
  integer(kind=4) :: i,j,iu,ispw,imode,it,ic1,ic2,olun,conflict,overload,lpar
  character(len=256) :: mess
  character(len=12) :: sname
  logical :: dopchanged,gottype,needdef,doobs,badsetup,dohr,soulsr
  logical :: fixedfreq,dosetup,domol,dotrackshare,chunk1,ts_freqout,alloff
  integer(kind=4), parameter :: mdoptypes=2
  integer(kind=4), parameter :: doplsr=1
  integer(kind=4), parameter :: dopred=2
  integer(kind=4) :: ndoptype
  character(len=8), parameter :: doptypes(mdoptypes) = (/ 'LSR     ','REDSHIFT' /)
  character(len=8) :: doptype,par
  ! 
  if (.not.noema%cfebe%defined) then
    call astro_message(seve%e,rname,'No tuning defined, nothing to do')
    error = .true.
    return
  endif
  !
  !check that doppler did not changed since last tuning
  call rec_check_doppler(noema%source,noema%recdesc%redshift,dopchanged,error)
  if (dopchanged) then
    call astro_message(seve%e,rname,'Source properties changed since last action')
    call rec_display_error('Source changed since last action',error)
    error = .true.
    return
  endif
  !
  ! Check status of pfx (overload or conflict)
  chunk1=.false.
  call noema_pfx_status(noema%cfebe%pfx,overload,conflict,chunk1,alloff,error)
  if (error) return
  if (alloff) then
    call astro_message(seve%e,rname,'Backend not defined')
    error=.true.
    return
  endif
  badsetup=.false.
  if (overload.gt.0) then
    badsetup=.true.
    call astro_message(seve%e,rname,'Setup requires more chunks than available')
  endif
  if (conflict.gt.0) then
    badsetup=.true.
    write (mess,'(i0,1x,a)') conflict,'Chunks used by more than one spectral window'
    call astro_message(seve%e,rname,mess)
  endif
  if (badsetup) then
    call astro_message(seve%e,rname,'Please solve conflicts and try again')
    error=.true.
    return
  endif
  !
  drawaxis%main=trim(freqax_names(freqax_rest))
  drawaxis%second=trim(freqax_names(freqax_lsr))
  !
  fixedfreq=sic_present(optfixed,0)
  dosetup=sic_present(optsetup,0)
  domol=sic_present(optline,0)
  dotrackshare=sic_present(opttrackshare,0)
  !
  if (domol.and..not.dosetup) then
    call astro_message(seve%e,rname,'/LINES option only possible in combination with /SETUP')
    error=.true.
    return
  endif
  !
  ! Parse the line
  call sic_ke(line,0,1,par,lpar,.true.,error)
  if (error) return
  call sic_ambigs(rname,par,doptype,ndoptype,doptypes,mdoptypes,error)
  if (error) return
  if (ndoptype.eq.doplsr) then
    soulsr=.true.
    if (noema%source%z.ne.0) then
      call astro_message(seve%e,rname,'Inconsitency between source type and command argument')
      error=.true.
      return
    endif
  else
    soulsr=.false.
    if (noema%source%vlsr.ne.0) then
      call astro_message(seve%e,rname,'Inconsitency between source type and command argument')
      error=.true.
      return
    endif
  endif
  call sic_r8(line,0,2,newv,.true.,error)
  if (error) return
  !
  tolf = 0d0
  call sic_r8(line,0,3,tolf,.false.,error)
  if (error) return
  !
  ! Parse /SETUP arguments
  if (dosetup) then
    sname=''
    call sic_ch(line,optsetup,1,sname,lpar,.true.,error)
    if (error) return
    call sic_r8(line,optsetup,2,frepin,.true.,error)
    if (error) return
    dfrep=0d0
    call sic_r8(line,optsetup,3,dfrep,.false.,error)
    if (error) return
  else
    frep=undef_freq
    frepin=undef_freq
  endif
  !
  ! prepare for molecules
  if (domol) then
    cata%catalog=molecules%catalog
    cata%doplot=.true.
    cata%profile=molecules%profile
    cata%width=molecules%width
  else
    cata = molecules
  endif
  !
  ! Parse /TRACKSHARING option
  if (dotrackshare) then
    do i=1,2
      call sic_r8(line,opttrackshare,i,tsvel(i),.true.,error)
      if (error) return
    enddo
  endif
  !
  !Duplicate structure
  newnoema%source=noema%source
  newnoema%recdesc=noema%recdesc
  newnoema%cfebe%i_f=noema%cfebe%i_f
  newnoema%cfebe%tune=noema%cfebe%tune
  newnoema%cfebe%pfx=noema%cfebe%pfx
  !If /FIXED option is present:
  newnoema%cfebe%tune%fixedfreq=fixedfreq
  newnoema%cfebe%tune%fcent=noema%cfebe%rectune%fcent
  print *,'newnoema tune',noema%cfebe%tune%fcent
  ! Duplicate spw
  spw2=noema%cfebe%spw
  if (soulsr) then
  !  Change source information in LSR case
  if (newnoema%source%sourcetype.eq.soukind_none) then
    newnoema%source%name='V shift'
    newnoema%source%sourcetype = soukind_full
    else if (newnoema%source%sourcetype.eq.soukind_full.or.newnoema%source%sourcetype.eq.soukind_vlsr) then
      newnoema%source%name='New Velocity'
    else if (newnoema%source%sourcetype.eq.soukind_red) then
      write (mess,'(a,1x,a)') 'Source type not supported in this context:',soukinds(newnoema%source%sourcetype)
      call astro_message(seve%e,rname,mess)
      error=.true.
      return
   else
     write (mess,'(a,1x,i0)') 'Unvalid source type:',newnoema%source%sourcetype
     call astro_message(seve%e,rname,mess)
     error=.true.
     return
   endif
   ! Compute new coverage
    newnoema%source%vlsr=newv
    newnoema%source%z=0d0
    newnoema%source%vdop=noema%source%vdop+newv-noema%source%vlsr
  else 
    ! Change source information in REDSHIFT case
    if (newnoema%source%sourcetype.eq.soukind_none) then
      newnoema%source%name='Z shift'
      newnoema%source%sourcetype = soukind_full
    else if (newnoema%source%sourcetype.eq.soukind_full.or.newnoema%source%sourcetype.eq.soukind_red) then
      newnoema%source%name='New redshift'
    else if (newnoema%source%sourcetype.eq.soukind_vlsr) then
      write (mess,'(a,1x,a)') 'Source type not supported in this context:',soukinds(newnoema%source%sourcetype)
      call astro_message(seve%e,rname,mess)
      error=.true.
      return
    else
      write (mess,'(a,1x,i0)') 'Unvalid source type:',newnoema%source%sourcetype
      call astro_message(seve%e,rname,mess)
      error=.true.
      return
    endif
    !Compute new coverage
    newnoema%source%vlsr=0d0
    newnoema%source%z=newv
  endif
  newnoema%source%zshift = 1d0/(1d0+newnoema%source%z)
  newnoema%source%dopshift = (1.0d0-newnoema%source%vdop/clight_kms)*newnoema%source%zshift
  newnoema%source%lsrshift = (1.0d0-newnoema%source%vlsr/clight_kms)*newnoema%source%zshift
  do i = 1,noema%recdesc%n_rbands
    do j=1,2
      newnoema%recdesc%restlim(j,i) = newnoema%recdesc%rflim(j,i)/newnoema%source%dopshift
    enddo
  enddo
  ! New tuning done
  call noema_setup(rname,newnoema%cfebe%tune,newnoema%cfebe%reccomm,newnoema%recdesc, &
                  newnoema%source,newnoema%cfebe%rectune,error)
  if (error) return
  !
  ! Reset spw2 global
  do ispw=1,noema%cfebe%spw%out%n_spw
    call noema_null_spw(spw2%out%win(ispw),error)
    if (error) return
  enddo
  ! Remove empty windows from spw2%out type 
  call noema_compress_spw(spw2%out,error)
  if (error) return
  !
  ! Select all units and recompute spw in spw2
  newnoema%cfebe%i_f%selunit%n_ifsel=0
  newnoema%cfebe%i_f%selunit%polmode="B"
  dohr=.false.
  do iu=1,newnoema%cfebe%pfx%n_units
    newnoema%cfebe%i_f%selunit%n_ifsel=newnoema%cfebe%i_f%selunit%n_ifsel+1
    newnoema%cfebe%i_f%selunit%usel(iu)=iu
    if (newnoema%cfebe%pfx%unit(iu)%imode.eq.-1) cycle
    call pfx_reset_unit(newnoema%cfebe%pfx%unit(iu),spw2%out,error)
    if (error) return
    newnoema%cfebe%i_f%selunit%usel(iu)=iu
    imode=newnoema%cfebe%pfx%unit(iu)%imode
    ! Fixed spw    
    call pfx_fixed_spw(rname,newnoema%recdesc,newnoema%source,newnoema%cfebe%rectune,newnoema%cfebe%pfx%unit(iu),spw2,error)
    if (error) return
    ! Flexible spw
    gottype=.false.
    do it=1,newnoema%cfebe%pfx%unit(iu)%mode(imode)%n_types
      if (newnoema%cfebe%pfx%unit(iu)%mode(imode)%chtype(it)%move_chunk) then
        spw2%comm%itype=it
        gottype=.true.
      endif
    enddo
    if (.not.gottype) then
      write (mess,'(a,1x,a)') 'No flexible SPW in',newnoema%cfebe%pfx%unit(iu)%label
      call astro_message(seve%i,rname,mess)
      cycle
    endif
    ! Define flexible spw when present
    spw2%comm%resol=newnoema%cfebe%pfx%unit(iu)%mode(imode)%chtype(spw2%comm%itype)%df_chunks*1000d0 ! in kHz
    do ispw=1,noema%cfebe%spw%out%n_spw
      if (noema%cfebe%spw%out%win(ispw)%label.ne.newnoema%cfebe%pfx%unit(iu)%label) cycle
      if (.not.noema%cfebe%spw%out%win(ispw)%flexible) cycle
      spw2%comm%user_label=noema%cfebe%spw%out%win(ispw)%user_label
      dohr=.true.
      ic1=noema%cfebe%spw%out%win(ispw)%ich1
      ic2=noema%cfebe%spw%out%win(ispw)%ich2
      !1:check that chunks need to be configured
      call noema_check_chunks(rname,ic1,ic2,spw2,newnoema%cfebe%pfx%unit(iu),needdef,error)
      if (error) return
      if (needdef) then
        !2: configure the chunks
        call noema_config_chunks(rname,ic1,ic2,newnoema%cfebe%pfx%unit(iu),spw2%comm,error)
        if (error) return
        !3: update the list of spw
        call noema_add_spw(newnoema%recdesc,newnoema%source,newnoema%cfebe%rectune,ic1,ic2,newnoema%cfebe%pfx%unit(iu),spw2,error)
        if (error) return
      else
        call astro_message(seve%w,rname,'All chunks are already defined, no SPW added')
      endif
    enddo ! ispw
  enddo ! iu
  !
  ! Sort the SPW list
  call noema_sort_spw2(spw2%out,error)
  if (error)  return  
  ! Copy result to newnoema big structure
  newnoema%cfebe%spw=spw2
  ! Look for conflict (i.e. same chunk used several times)
  call noema_check_conflicts(rname,newnoema%cfebe%spw%out,newnoema%cfebe%pfx,error)
  if (error)  return
  !
  ! 
  if (dosetup) then
    ! Prepare structure for output information
    call noema_default_pms(infopms,error)
    if (error) return
    ! Do some conversion and start to fill PMS/SMS info structure
    call rec_inputtorest(rname,frepin,drawaxis%main,newnoema%source,frep,error)
    if (error) return
    infopms%frep=frep
    call resttolsr(newnoema%source%lsrshift,infopms%frep,infopms%freplsr,error)
    if (error) return
    infopms%resrep=dfrep
    frep=frep*mhzperghz ! for later computation
    !
    call noema_info_pms(newnoema%cfebe%rectune,newnoema%source,newnoema%cfebe%pfx,newnoema%cfebe%spw%out,infopms,error)
    if (error) return
    if (dotrackshare) infopms%tracksharing=.true.
  endif
  !
  ! Do the PLOT
  call noema_plot_selpfx(rname,newnoema,cplot,cata,drawaxis,error)
  if (error) return
  !
  ! Overplot HR SPW as configured in the original setup
  if (dohr) then
    call noema_oplot_summary(noema%cfebe%spw%out,newnoema%cfebe%pfx,noema%source,cplot,cata,error)
    if (error) return
  endif
  !
  if (dotrackshare) then
    ! Show position of sources with vmin and vmax
    call noema_trackshare(soulsr,tsvel,newnoema%source,frep,newnoema%cfebe%rectune%frest, & 
                          cplot,tsdf,ts_freqout,error)
    if (error) return
    if (dosetup) then
      infopms%tsdf(1) = tsdf(1)
      infopms%tsdf(2) = tsdf(2)
      infopms%ts_freqout = ts_freqout
    endif
  endif
  !
  if (dosetup) then
    ! Show representative frequency if needed
    call noema_draw_frep(frep,newnoema%cfebe%spw%out,cplot,infopms,error)
    if (error) return
    ! Output info pms structure
    call noema_print_pms(infopms,error)
    if (error) return
  endif
  !
  ! Let the user with the limits in main axis frequency frame
  call rec_set_limits(rname,cplot,drawaxis%main,error)
  if (error) return
  !
  ! Compare old and new frequencies and compare offset to user tolerance
  call noema_getoffset(noema%cfebe%spw%out,newnoema%cfebe%spw%out,offset,error)
  if (error) return
  if (offset.gt.tolf) then
    write (mess,'(a,i0,1x,a,i0,1x,a)') 'SPW offset (',nint(offset), &
          'MHz) greater than tolerance (',nint(tolf),'MHz)'
    call astro_message(seve%e,'OMS',mess)
  else
    write (mess,'(a,i0,1x,a,i0,1x,a)') 'SPW offset (',nint(offset), &
          'MHz) within tolerance (',nint(tolf),'MHz)'
    call astro_message(seve%i,'OMS',mess)
    !
    if (dosetup) then
      ! Write on screen the corresponding setup
      olun=6
      doobs=.true.
      call noema_print('! BEGIN INCLUDE_SETUP',olun)
        ! TUNING command
        if (sname.eq."*") then
          ! Default name is LO freq
          write (newnoema%cfebe%rectune%name,'(a,i3.3)') 'LO',nint(newnoema%cfebe%rectune%flo1*ghzpermhz)
        else if (sname.ne.'') then
          ! Name as given by user
          newnoema%cfebe%rectune%name=sname
        endif
        call noema_setup_rec(newnoema%source,newnoema%cfebe%rectune,doobs,olun,error)
        if (error) return
        ! BASEBANDS commands
        call noema_setup_bb(newnoema%cfebe%rectune,newnoema%cfebe%pfx,newnoema%cfebe%spw%out,doobs,olun,error)
        if (error) return
        ! SPW commands
        call noema_setup_spw(newnoema%cfebe%rectune,newnoema%cfebe%pfx,newnoema%cfebe%spw%out,doobs,olun,error)
        if (error) return
      call noema_print('! END INCLUDE_SETUP',olun)
    endif
  endif
  !
end subroutine noema_changevelo
!
subroutine noema_getoffset(spwold,spwnew,offset,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_getoffset
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Compare old and new frequencies 
  !
  !-----------------------------------------------------------------------
  type(spw_output_t), intent(in) :: spwold
  type(spw_output_t), intent(in) :: spwnew
  real(kind=8), intent(inout) :: offset
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='NEWVEL'
  integer(kind=4) :: ispw,nspw
  !
  if (spwold%n_spw.ne.spwnew%n_spw) then
    call astro_message(seve%e,rname,"Problem with number of SPW")
    error=.true.
    return
  endif
  !
  offset=0d0
  nspw=0
  do ispw=1,spwold%n_spw
    if (.not.spwnew%win(ispw)%flexible) cycle
    offset=offset+abs(spwold%win(ispw)%restmin-spwnew%win(ispw)%restmin)
    nspw=nspw+1
  enddo
  if (nspw.eq.0) return
  offset=nint(offset/nspw)
  !
end subroutine noema_getoffset
!
subroutine noema_oplot_summary(spwout,pfx,rsou,cplot,molecules,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_oplot_summary
  use astro_types
  use ast_astro
  !-----------------------------------------------------------------------
  ! @ private
  ! Overplot coverage of spectral units for comparison purpose (changevelo)
  !
  !-----------------------------------------------------------------------
  type(spw_output_t), intent(in) :: spwout
  type(pfx_t), intent(in) :: pfx
  type(receiver_source_t), intent(in) :: rsou
  type(current_boxes_t), intent(inout) :: cplot
  type(plot_molecules_t), intent(in) :: molecules
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='NEWVEL'
  integer(kind=4) :: ispw,ib,overload,conflict
  real(kind=8)  :: sbox
  type(draw_rect_t) :: sprec
  logical :: badsetup,yesh,yesv,chunk1,alloff
  character(len=256) :: comm,molchar,defchar,mess
  !
  write (defchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%defchar
  write (molchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%molchar
  call gr_execl('CHANGE DIRECTORY')
  !
  ! check whether more resource than available are used or if chunks are used by more than 1 spw
  chunk1=.false.
  call noema_pfx_status(pfx,overload,conflict,chunk1,alloff,error)
  if (error) return
  if (alloff) then
    call astro_message(seve%e,rname,'Backend not defined')
    error=.true.
    return
  endif
  badsetup=(overload.gt.0).or.(conflict.gt.0)
  !
  !
  do ib=1,cplot%nbox
    sbox=cplot%box(ib)%rest%ymax-cplot%box(ib)%rest%ymin
    write (comm,'(a,i0)') 'CHANGE DIRECTORY BOX',ib
    call gr_execl(comm)
    yesh=.false.
    yesv=.false.
    do ispw=1,spwout%n_spw
      if (.not.spwout%win(ispw)%flexible) cycle
      if ((spwout%win(ispw)%restmin.ge.cplot%box(ib)%rest%xmin) &
        .or. (spwout%win(ispw)%restmax.le.cplot%box(ib)%rest%xmax)) then
        if (cplot%desc%plotmode.eq.pm_basebands) then
          sprec%xmin=spwout%win(ispw)%restmin
          sprec%xmax=spwout%win(ispw)%restmax
          call noema_spw_getcol(rname,spwout%win(ispw),pfx,icol_hatch,sprec%col,error)
          if (spwout%win(ispw)%pol_code.eq.1) then
            sprec%ymax=cplot%box(ib)%rest%ymax
            sprec%ymin=(cplot%box(ib)%rest%ymax+cplot%box(ib)%rest%ymin)/2d0
          else if (spwout%win(ispw)%pol_code.eq.2) then
            sprec%ymax=(cplot%box(ib)%rest%ymax+cplot%box(ib)%rest%ymin)/2d0
            sprec%ymin=cplot%box(ib)%rest%ymin
          else
            call astro_message(seve%e,rname,'Problem with polarizations')
            error=.true.
            return
          endif
          call gr_pen(colour=sprec%col,error=error)
          if (error) return
          call rec_draw_hrect(sprec,cplot%box(ib)%rest,error)
          if (error) return
          call gr_pen(colour=adefcol,error=error)
          if (error) return
        else
          call astro_message(seve%e,rname,'Problem with plot mode')
          error=.true.
          return
        endif
      endif
    enddo ! ispw
    ! Draw molecules
    call gr_exec1(molchar)
    call rec_draw_molecules(molecules,cplot%box(ib)%rest,error)
    if (error) return
    call gr_exec1(defchar)
    ! Draw caption at the top
    if (ib.eq.1) then
      call gr_pen(colour=sprec%col,error=error)
      if (error) return
      if (rsou%sourcetype.eq.soukind_full) then
        if (rsou%z.ne.0d0) then
          write (mess,'(a,1x,a,1x,a,1x,f0.4,1x,a)') 'SPW coverage for',trim(rsou%name), &
                '(z=',rsou%z,')'
        else
          write (mess,'(a,1x,a,1x,a,1x,f0.3,1x,a)') 'SPW coverage for',trim(rsou%name), &
                '(V\\dLSR\\u=',rsou%vlsr,'km s\\u-1\\d)'
        endif
      else
        if (rsou%z.ne.0d0) then
          write (mess,'(a,1x,f0.4)') 'SPW coverage for z =',rsou%z
        else
          write (mess,'(a,1x,f0.3,1x,a)') 'SPW coverage for V\\dLSR\\u=',rsou%vlsr,'km s\\u-1\\d'
        endif
      endif
      write (comm,'(a,a,a)') 'DRAW TEXT 0 2 "',trim(mess),'" 4 0 /CHARACTER 9'
      call gr_exec1(comm)
      call gr_pen(colour=adefcol,error=error)
      if (error) return
    endif
    !redraw the box
    call gr_execl('CHANGE DIRECTORY')
  enddo ! ib
  call gr_execl('CHANGE DIRECTORY')
  call gr_pen(colour=adefcol,error=error)
  if (error) return
  !
end subroutine noema_oplot_summary
!
subroutine noema_external_tuning(rname,irec,lnoema,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_external_tuning
  use astro_noema_type
  use phys_const
  !-----------------------------------------------------------------------
  ! @ private
  ! SET tuning  and source information from outside astro
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(astro_noemareceiver_t), intent(in) :: irec  ! import tuning info
  type(noema_t), intent(inout) :: lnoema    ! febe structure
  logical, intent(inout) :: error
  ! Local
  integer(kind=4) :: i,j
  real(kind=8) :: frf
  !
  ! Source part
  ! Defined with doppler and no LSR
  lnoema%source%z=0d0
  lnoema%source%zshift=0d0
  lnoema%source%name=irec%sourcename
  lnoema%source%vdop=-clight_kms*irec%obsdoppler
  lnoema%source%dopshift=1.0d0+irec%obsdoppler
  lnoema%source%vlsr=0d0
  lnoema%source%lsrdefined=.false. ! to avoid plotting LSR on figures
  lnoema%source%lsrshift=lnoema%source%dopshift
  ! Receiver limits
  do i = 1,lnoema%recdesc%n_rbands
    do j=1,2
      lnoema%recdesc%restlim(j,i) = lnoema%recdesc%rflim(j,i)/lnoema%source%dopshift
      lnoema%recdesc%restcall(j,i) = lnoema%recdesc%rfcall(j,i)/lnoema%source%dopshift
    enddo
  enddo
  ! Actual tuning part  - 1 tuning plotted at a time
  lnoema%cfebe%reccomm%rec_name='NOEMA'
  frf=irec%flo1+sb_sign(irec%sbcode)*irec%fcent
  lnoema%cfebe%reccomm%frest=frf/lnoema%source%dopshift*ghzpermhz
  lnoema%cfebe%reccomm%sideband=sideband(irec%sbcode)
  lnoema%cfebe%reccomm%name = irec%tuningname
  lnoema%cfebe%reccomm%fcent = irec%fcent
  call astro_tune_receiver(rname,lnoema%recdesc,lnoema%source,lnoema%cfebe%reccomm,lnoema%cfebe%rectune,error)
  if (error) return
  !
  ! define if ranges transported
  call noema_ifproc(rname,lnoema%recdesc,lnoema%cfebe%rectune,lnoema%cfebe%i_f%ifproc,error)
  if (error) return
  !
end subroutine noema_external_tuning
!
subroutine noema_external_spw(rname,ispw,lnoema,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_external_spw
  use astro_noema_type
  use my_receiver_globals
  !-----------------------------------------------------------------------
  ! @ private
  ! configure correlator from outside information
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(astro_noemaspw_t), intent(in) :: ispw  ! import spw list
  type(noema_t), intent(inout) :: lnoema     ! setup structure
  logical, intent(inout) :: error
  ! Local
  integer(kind=4) :: iw,iu,it,imode,ic1,ic2
  logical :: gottype,needdef
  character(len=5) :: wlabel
  !
  ! define polyfix and assign if "cables" to correlator units [only input, no mode yet]
  call noema_define_pfx(lnoema%cfebe%pfx,error)
  if (error) return
  call noema_reset_backend(lnoema%cfebe%pfx,lnoema%cfebe%spw%out,error)
  lnoema%cfebe%spw%comm%itype=-1
  if (error) return
  call noema_assign_units(rname,lnoema%cfebe%i_f%ifproc,lnoema%cfebe%pfx,lnoema%recdesc,error)
  if (error) return
  !
  lnoema%cfebe%i_f%selunit%n_ifsel=0
  lnoema%cfebe%i_f%selunit%polmode="B"
  do iu=1,lnoema%cfebe%pfx%n_units
    lnoema%cfebe%i_f%selunit%n_ifsel=lnoema%cfebe%i_f%selunit%n_ifsel+1
    lnoema%cfebe%i_f%selunit%usel(iu)=iu
    do iw=1,ispw%n_spw
      !create the 5character label
      write (wlabel,'(a,i0,a)') 'B',lnoema%cfebe%rectune%iband,trim(ispw%label(iw))
      if (wlabel.ne.lnoema%cfebe%pfx%unit(iu)%label) cycle
      ! Assign mode and compute fixed SPW when needed
      if (lnoema%cfebe%pfx%unit(iu)%imode.eq.-1) then
        call pfx_reset_unit(lnoema%cfebe%pfx%unit(iu),lnoema%cfebe%spw%out,error)
        if (error) return
        lnoema%cfebe%pfx%unit(iu)%imode=ispw%corrmode(iw)
        call pfx_fixed_spw(rname,lnoema%recdesc,lnoema%source,lnoema%cfebe%rectune, &
                  lnoema%cfebe%pfx%unit(iu),lnoema%cfebe%spw,error)
        if (error) return
      else if (lnoema%cfebe%pfx%unit(iu)%imode.ne.ispw%corrmode(iw)) then
        call astro_message(seve%e,rname,'2 windows in the same unit have different modes !!')
        error=.true.
        return
      endif
      ! Flexible spw
      imode=ispw%corrmode(iw)
      gottype=.false.
      do it=1,lnoema%cfebe%pfx%unit(iu)%mode(imode)%n_types
        if (lnoema%cfebe%pfx%unit(iu)%mode(imode)%chtype(it)%df_chunks.eq.ispw%resol(iw)) then
          lnoema%cfebe%spw%comm%itype=it
          gottype=.true.
        endif
      enddo !it
      if (.not.gottype) then
        call astro_message(seve%e,rname,'Problem with chunk types')
        error=.true.
        return
      endif
      if (.not.lnoema%cfebe%pfx%unit(iu)%mode(imode)%chtype(lnoema%cfebe%spw%comm%itype)%move_chunk) cycle
      lnoema%cfebe%spw%comm%resol=  & 
          lnoema%cfebe%pfx%unit(iu)%mode(imode)%chtype(lnoema%cfebe%spw%comm%itype)%df_chunks*1000d0 ! in kHz
      if (ispw%chunkmin(iw).eq.0.or.ispw%chunkmax(iw).eq.0) then
        call astro_message(seve%e,rname,'Problem with SPW limits')
        error=.true.
        return
      endif
      ! Define by chunk
      ic1=ispw%chunkmin(iw)
      ic2=ispw%chunkmax(iw)
      !1:check that chunks need to be configured
      call noema_check_chunks(rname,ic1,ic2,lnoema%cfebe%spw,lnoema%cfebe%pfx%unit(iu),needdef,error)
      if (error) return
      if (needdef) then
        !2: configure the chunks
        call noema_config_chunks(rname,ic1,ic2,lnoema%cfebe%pfx%unit(iu),lnoema%cfebe%spw%comm,error)
        if (error) return
        !3: update the list of spw
        call noema_add_spw(lnoema%recdesc,lnoema%source,lnoema%cfebe%rectune,ic1,ic2, &
                            lnoema%cfebe%pfx%unit(iu),lnoema%cfebe%spw,error)
        if (error) return
      else
        call astro_message(seve%w,rname,'All chunks are already defined, no SPW added')
      endif
    enddo !iw
  enddo ! iu
  !
  spw2=lnoema%cfebe%spw ! use a temporary global for sorting... to be seen with SB
  ! Sort the SPW list
  call noema_sort_spw2(spw2%out,error)
  if (error)  return  
  !
  lnoema%cfebe%spw=spw2  ! back to original name
  ! Look for conflict (i.e. same chunk used several times)
  call noema_check_conflicts(rname,lnoema%cfebe%spw%out,lnoema%cfebe%pfx,error)
  if (error)  return
  !
end subroutine noema_external_spw
!
subroutine if2tochunk(rname,chtype,if2,domin,ich,error)
  use gbl_message
  use astro_interfaces, except_this=>if2tochunk
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! get chunk number form if2 value
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(pfx_type_t), intent(in) :: chtype
  real(kind=8), intent(in)     :: if2
  logical, intent(in)          :: domin
  integer(kind=4), intent(out) :: ich
  logical, intent(inout) :: error
  ! Local
  real(kind=4) :: dch
  !
  dch=(1+(if2-chtype%if2ch0)/chtype%width_chunk)
  !
  if (domin) then
    ich=ceiling(dch)
  else
    ich=floor(dch)
  endif
  !
end subroutine if2tochunk
!
