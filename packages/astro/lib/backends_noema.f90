subroutine noema_define_pfx(pfx,error)
  use astro_interfaces, except_this=>noema_define_pfx
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! defines polyfix backends
  !-----------------------------------------------------------------------
  type(pfx_t), intent(inout)    :: pfx
  logical, intent(inout)            :: error
  ! Local
  integer(kind=4)       :: i,j,k,ic
  !pfx general
  pfx%name = 'POLYFIX'
  ! Define number of units
  call pfx%reallocate(8,error)
  if (error) return
  pfx%if2lim(1)=0d0
  pfx%if2lim(2)=3872d0
  ! pfx units
  do i=1,pfx%n_units
    pfx%unit(i)%n_modes = 3
    pfx%unit(i)%imode = -1
    ! Modes
    ! Mode 1 = Full band 2MHz + 62.6kHz chunks
    pfx%unit(i)%mode(1)%backname = 'POLYFIX'
    pfx%unit(i)%mode(1)%modename = '2000, 62.5kHz'
    pfx%unit(i)%mode(1)%n_types = 2
    ! Mode 1 Type 1 = low res
    pfx%unit(i)%mode(1)%chtype(1)%typename = 'Low_Res'
    pfx%unit(i)%mode(1)%chtype(1)%itypecol(icol_fill) = alowrescol ! filled rect
    pfx%unit(i)%mode(1)%chtype(1)%itypecol(icol_hatch) = alowrescolh ! hatch rect
    pfx%unit(i)%mode(1)%chtype(1)%itypecol(icol_border) = alowrescolr ! border rect
    pfx%unit(i)%mode(1)%chtype(1)%move_chunk = .false.
    pfx%unit(i)%mode(1)%chtype(1)%n_chunks = 61
    pfx%unit(i)%mode(1)%chtype(1)%use_chunks = 61
    pfx%unit(i)%mode(1)%chtype(1)%df_chunks = 2d0
    pfx%unit(i)%mode(1)%chtype(1)%width_chunk = 64d0
    pfx%unit(i)%mode(1)%chtype(1)%if2ch0 = 0d0
    ! Mode 1 Type 2 = high res
    pfx%unit(i)%mode(1)%chtype(2)%typename = 'High_Res'
    pfx%unit(i)%mode(1)%chtype(2)%itypecol(icol_fill) = ahighrescol  ! filled rect
    pfx%unit(i)%mode(1)%chtype(2)%itypecol(icol_hatch) = ahighrescolh ! hatch rect
    pfx%unit(i)%mode(1)%chtype(2)%itypecol(icol_border) = ahighrescolr  ! border rect
    pfx%unit(i)%mode(1)%chtype(2)%move_chunk = .true.
    pfx%unit(i)%mode(1)%chtype(2)%n_chunks = 61
    pfx%unit(i)%mode(1)%chtype(2)%use_chunks = 16
    pfx%unit(i)%mode(1)%chtype(2)%df_chunks = 0.0625d0
    pfx%unit(i)%mode(1)%chtype(2)%width_chunk = 64d0
    pfx%unit(i)%mode(1)%chtype(2)%if2ch0 = 0d0
    ! Mode 2 = VLBI    
    pfx%unit(i)%mode(2)%backname = 'POLYFIX'
    pfx%unit(i)%mode(2)%modename = 'VLBI'
    pfx%unit(i)%mode(2)%n_types = 0    
    !
    ! Mode 3 = Full band 250kHz (+ pseudi 2MHz)
    pfx%unit(i)%mode(3)%backname = 'POLYFIX'
    pfx%unit(i)%mode(3)%modename = '2000, 250kHz'
    pfx%unit(i)%mode(3)%n_types = 2
    ! Mode 3 Type 1 = low res
    pfx%unit(i)%mode(3)%chtype(1)%typename = 'Low_Res'
    pfx%unit(i)%mode(3)%chtype(1)%itypecol(icol_fill) = alowrescol ! filled rect
    pfx%unit(i)%mode(3)%chtype(1)%itypecol(icol_hatch) = alowrescolh ! hatch rect
    pfx%unit(i)%mode(3)%chtype(1)%itypecol(icol_border) = alowrescolr ! border rect
    pfx%unit(i)%mode(3)%chtype(1)%move_chunk = .false.
    pfx%unit(i)%mode(3)%chtype(1)%n_chunks = 61
    pfx%unit(i)%mode(3)%chtype(1)%use_chunks = 61
    pfx%unit(i)%mode(3)%chtype(1)%df_chunks = 2d0
    pfx%unit(i)%mode(3)%chtype(1)%width_chunk = 64d0
    pfx%unit(i)%mode(3)%chtype(1)%if2ch0 = 0d0
    !
    ! Mode 3 Type 2 = 250 res
    pfx%unit(i)%mode(3)%chtype(2)%typename = 'Med_Res'
    pfx%unit(i)%mode(3)%chtype(2)%itypecol(icol_fill) = a250col  ! filled rect
    pfx%unit(i)%mode(3)%chtype(2)%itypecol(icol_hatch) = a250colh ! hatch rect
    pfx%unit(i)%mode(3)%chtype(2)%itypecol(icol_border) = a250colr  ! border rect
    pfx%unit(i)%mode(3)%chtype(2)%move_chunk = .false.
    pfx%unit(i)%mode(3)%chtype(2)%n_chunks = 61
    pfx%unit(i)%mode(3)%chtype(2)%use_chunks = 61
    pfx%unit(i)%mode(3)%chtype(2)%df_chunks = 0.250d0
    pfx%unit(i)%mode(3)%chtype(2)%width_chunk = 64d0
    pfx%unit(i)%mode(3)%chtype(2)%if2ch0 = 0d0
    ! Init chunks
    do j=1,pfx%unit(i)%n_modes
      do k=1,pfx%unit(i)%mode(j)%n_types
        do ic=1,pfx%unit(i)%mode(j)%chtype(k)%n_chunks
          pfx%unit(i)%mode(j)%chtype(k)%chunks(ic) = 0
        enddo
      enddo
    enddo
  enddo
  !
end subroutine noema_define_pfx
!
subroutine noema_pfx_mode_message(rname,pfxm,error)
  use gbl_message
  use astro_interfaces, except_this=>noema_pfx_mode_message
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Display message on correlator mode selection
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname        ! command line
  type(pfx_mode_t), intent(in) :: pfxm
  logical, intent(inout) :: error
  ! local
  integer(kind=4) :: it
  character(len=256) :: mess,messband,messres,messflex
  !
  write (mess,'(a,1x,a,1x,a,1x,a)') 'Selected',trim(pfxm%modename), &
                                'of',trim(pfxm%backname)
  call astro_message(seve%i,rname,mess)
  !
  do it=1,pfxm%n_types
    ! Flexible chunks ?
    if (pfxm%chtype(it)%move_chunk) then
      messflex='to be selected'
    else
      messflex=''
    endif
    ! Width
    if (pfxm%chtype(it)%use_chunks.eq.pfxm%chtype(it)%n_chunks) then
      messband='Full baseband'
    else
      write (messband,'(i0,1x,a,1x,i0,1x,a,1x,a)') pfxm%chtype(it)%use_chunks,'out of',    &
                                            pfxm%chtype(it)%n_chunks,'chunks',trim(messflex)
    endif
    ! Channel spacing
    write (messres,'(a,f0.2,1x,a)') '(',pfxm%chtype(it)%df_chunks*khzpermhz,'kHz Channels)'
    ! Global message
    write (mess,'(a,a,1x,a,1x,a)') trim(pfxm%chtype(it)%typename),':', & 
    trim(messband),trim(messres)
    call astro_message(seve%i,rname,mess)
  enddo ! it
  !
end subroutine noema_pfx_mode_message
!
subroutine noema_spw(line,error)
  use gbl_message
  use gkernel_interfaces
  use gkernel_types
  use astro_interfaces, except_this=>noema_spw
  use ast_astro
  use my_receiver_globals
  use plot_molecules_globals
  use frequency_axis_globals
  !-----------------------------------------------------------------------
  ! @ private
  ! Set and show the backend coverage of NOEMA
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(inout) :: line        ! command line
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='SPW'
  logical       :: dopchanged
  integer(kind=4), parameter :: optrange = 1 ! option /RANGE
  integer(kind=4), parameter :: optfreq = 2 ! option /FREQUENCY
  integer(kind=4), parameter :: optchunk = 3 ! option /CHUNK
  integer(kind=4), parameter :: optlabel = 4 ! option /CHUNK
  integer(kind=4) :: i1,iarg,nc,ier
  real(kind=8)  :: f1,fwidth,f2
  logical :: dorange,dofreq,dochunk,dolabel
  character(len=256) :: spwstring,mess
  type(sic_listi4_t) :: spwlist
  !
  if (obsname.ne.'NOEMA') then
      call astro_message(seve%e,rname,'Inconsistency between Receiver Name and Observatory')
      error = .true.
      return
  endif
  !
  if (noema%recdesc%name.ne.'NOEMA') then
    call astro_message(seve%e,rname,'SPW Works only with NOEMA receivers')
    error = .true.
    return
  endif
  if (.not.noema%cfebe%defined) then
    call astro_message(seve%e,rname,'Please define a tuning before setting up backends')
    error = .true.
    return
  endif
  if (noema%cfebe%i_f%selunit%n_ifsel.le.0) then
    call astro_message(seve%e,rname,'Please do a BASEBAND selection before defining the backends')
    error = .true.
    return
  endif
  !
  !check that doppler did not changed since last tuning
  call rec_check_doppler(noema%source,noema%recdesc%redshift,dopchanged,error)
  if (dopchanged) then
    call astro_message(seve%e,rname,'Source properties changed since last tuning')
    call astro_message(seve%i,rname,'You should set again the tuning before working with backends')
    call rec_display_error('Source changed since last tuning',error)
    error = .true.
    return
  endif
  !
  dolabel = sic_present(optlabel,0)
  dofreq = sic_present(optfreq,0)
  dorange = sic_present(optrange,0)
  dochunk = sic_present(optchunk,0)
  !
  noema%cfebe%spw%comm%allowconflict=.true. ! conflict authorized in offline mode
  !
  if (.not.dorange.and..not.dofreq.and..not.dochunk) then
    call astro_message(seve%e,rname,'Nothing to do')
    error = .true.
    return
  endif
  !
  if (sic_narg(0).ne.0) then
    call astro_message(seve%e,rname,'SPW command accepts only options, no arguments')
    error = .true.
    return
  endif
  !
  if (dolabel) then
    call sic_ch(line,optlabel,1,noema%cfebe%spw%comm%user_label,nc,.true.,error)
    if (error) return
  else
    noema%cfebe%spw%comm%user_label=''
  endif
  !
  ier=gag_time(noema%cfebe%spw%comm%time)
  if (ier.ne.0) then
    call astro_message(seve%e,rname,'Problem finding time')
    error=.true.
    return
  endif
  !
  if (dorange) then
    call sic_r8(line,optrange,1,f1,.true.,error)
    if (error) return
    call rec_inputtorest(rname,f1,freq_axis%main,noema%source,noema%cfebe%spw%comm%fmin,error)
    if (error) return
    call sic_r8(line,optrange,2,f2,.true.,error)
    if (error) return
    call rec_inputtorest(rname,f2,freq_axis%main,noema%source,noema%cfebe%spw%comm%fmax,error)
    if (error) return
    call noema_config_spw_byfreq(noema,error)
    if (error) return
  endif
  if (dofreq) then
    call sic_r8(line,optfreq,1,f1,.true.,error)
    if (error) return
    call sic_r8(line,optfreq,2,fwidth,.true.,error)
    if (error) return
    call rec_inputtorest(rname,f1-fwidth/2d0,freq_axis%main,noema%source,noema%cfebe%spw%comm%fmin,error)
    if (error) return
    call rec_inputtorest(rname,f1+fwidth/2d0,freq_axis%main,noema%source,noema%cfebe%spw%comm%fmax,error)
    if (error) return
    call noema_config_spw_byfreq(noema,error)
    if (error) return
  endif
  ! Chunks
  if (dochunk) then
    if (sic_narg(optchunk).eq.0) then
      call astro_message(seve%e,rname,'Please provide a chunk list to define SPW')
      error=.true.
      return
    endif
    ! Check that we can use chunk to configure spw
    if (cplot%nbox.ne.1) then
      write (mess,'(a)') & 
        'SPW defined by chunks only when a single baseband is selected (can be dual polar)'
      call astro_message(seve%e,rname,mess)
      error=.true.
      return
    endif
    !
    !Decode the list of chunks
    i1 = 1
    do iarg=1,sic_narg(optchunk)
      ! Translate the arguments (as string, because of "TO" in "12 TO 34") and
      ! save them in a string
      call sic_ke(line,optchunk,iarg,spwstring(i1:),nc,.true.,error)
      if (error)  return
      i1 = i1+nc+1
    enddo
    !
    ! 2) Parse the string into the dedicated type
    !    mspw = max size of problem, no internal limit, your choice
    call sic_parse_listi4(rname,spwstring,spwlist,m_spw,error)
    if (error)  return
    !
    ! 3) Forbid step different than 1
    if (any(abs(spwlist%i3(1:spwlist%nlist)).ne.1)) then
      call astro_message(seve%e,rname,'Step must be 1 or -1 == Chunks in a spw must be contiguous')
      error = .true.
      return
    endif
    !
    call noema_config_spw_bychunk(spwlist,noema,error)
    if (error) return
  endif
  ! Sort the SPW list
  call noema_sort_spw(noema%cfebe%spw%out,error)
  if (error)  return  
  ! Look for conflict (i.e. same chunk used several times)
  call noema_check_conflicts(rname,noema%cfebe%spw%out,noema%cfebe%pfx,error)
  if (error)  return
  !
  call noema_plot_selpfx(rname,noema,cplot,molecules,freq_axis,error)
  if (error) return
  !
  if (noema%source%sourcetype.eq.soukind_full) then
    call noema_oplot_dopminmax(noema,cplot,freq_axis,error)
    if (error) return
  endif
  !
  ! Let the user with the limits in main axis frequency frame
  call rec_set_limits(rname,cplot,freq_axis%main,error)
  if (error) return
  !
end subroutine noema_spw
!
subroutine noema_spw_online(line,error)
  use gbl_message
  use gkernel_interfaces
  use gkernel_types
  use astro_interfaces, except_this=>noema_spw_online
  use ast_astro
  use my_receiver_globals
  use plot_molecules_globals
  use frequency_axis_globals
  use string_parser_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Set and show the backend coverage of NOEMA
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(inout) :: line        ! command line
  logical, intent(inout) :: error
  ! local
  character(len=*), parameter :: rname='SPW'
  logical       :: dopchanged
  integer(kind=4), parameter :: optrange = 1 ! option /RANGE
  integer(kind=4), parameter :: optfreq = 2 ! option /FREQUENCY
  integer(kind=4), parameter :: optchunk = 3 ! option /CHUNK
  integer(kind=4), parameter :: optlabel = 4 ! option /LABEL
  integer(kind=4), parameter :: optbase = 5 ! option /BASEBAND
  integer(kind=4), parameter :: optrec = 6 ! option /RECEIVER
  integer(kind=4) :: i1,iarg,nc,ier,inputrec,it,iu,iunit,imode,isel,ir,is,iw
  logical :: dorange,dofreq,dochunk,dolabel,dorec,dobase,gottype,dooff,doall,dolist
  character(len=256) :: spwstring,mess
  integer(kind=4), parameter:: mkeys=1
  integer(kind=4) :: nkey
  character(len=16) :: firstarg,arg,keys(mkeys),key
  type(sic_listi4_t) :: spwlist
  integer(kind=4) :: spw_to_reset(m_spw),n_to_reset,nbefore
  integer(kind=4) :: obsindex(noema%cfebe%spw%out%n_spw)
  data keys/'*'/
  ! Related to baseband parsing:
  character(len=12) :: selcode
  integer(kind=4) :: lscode
  type(string_parser_t) :: sp
  integer(kind=4), parameter :: ipband=1 ! position in the chain
  integer(kind=4), parameter :: ippol=2
  integer(kind=4), parameter :: ipsb=3
  integer(kind=4), parameter :: ipbb=4
  integer(kind=4), parameter :: nband=3 ! number of choices
  integer(kind=4), parameter :: npol=2
  integer(kind=4), parameter :: nsb=2
  integer(kind=4), parameter :: nbb=2
  integer(kind=4)       :: parsecode(4)
  character(len=2) :: bb_band(nband)
  character(len=1) :: bb_pol(npol),bb_sb(nsb),bb_bb(nbb)
  data bb_band/'B1','B2','B3'/
  data bb_pol/'H','V'/
  data bb_sb/'U','L'/
  data bb_bb/'O','I'/
  !
  if (obsname.ne.'NOEMA') then
      call astro_message(seve%e,rname,'Inconsistency between Receiver Name and Observatory')
      error = .true.
      return
  endif
  !
  if (noema%recdesc%name.ne.'NOEMA') then
    call astro_message(seve%e,rname,'SPW Works only with NOEMA receivers')
    error = .true.
    return
  endif
  if (.not.noema%cfebe%defined) then
    call astro_message(seve%e,rname,'Please define a tuning before setting up backends')
    error = .true.
    return
  endif
!   if (noema%selunit%n_ifsel.le.0) then
!     call astro_message(seve%e,rname,'Please do a BASEBAND selection before defining the backends')
!     error = .true.
!     return
!   endif
  !
  !check that doppler did not changed since last tuning
  call rec_check_doppler(noema%source,noema%recdesc%redshift,dopchanged,error)
  if (dopchanged) then
    call astro_message(seve%e,rname,'Source properties changed since last tuning')
    call astro_message(seve%i,rname,'You should set again the tuning before working with backends')
    call rec_display_error('Source changed since last tuning',error)
    error = .true.
    return
  endif
  !
  dolabel = sic_present(optlabel,0)
  dofreq = sic_present(optfreq,0)
  dorange = sic_present(optrange,0)
  dochunk = sic_present(optchunk,0)
  dorec = sic_present(optrec,0)
  dobase = sic_present(optbase,0)
  !
  noema%cfebe%spw%comm%allowconflict=.false. ! conflict NOT authorized in ONLINE mode
  !
  ! Decode the /RECEIVER option
  inputrec=0
  call sic_i4(line,optrec,1,inputrec,.false.,error)
  if (error) return
  !
  ! Decode the BASEBAND selection
  if (.not.dobase) then
    call astro_message(seve%e,rname,'You must select at least a Baseband (HUO,VUO,VLI,...)')
    error=.true.
    return
  endif
  call sic_ke(line,optbase,1,selcode,lscode,.true.,error)
  if (error) return
  !Parse the user code (in that case HUO are mandatory ?!)
  call sic_ambigs_sub(rname,selcode,key,nkey,keys,mkeys,error)
  if (error) return
  if (nkey.eq.0) then
    call string_parser_addlist(sp,ipband,bb_band,error)
    if (error) return
    call string_parser_addlist(sp,ippol,bb_pol,error)
    if (error) return
    call string_parser_addlist(sp,ipsb,bb_sb,error)
    if (error) return
    call string_parser_addlist(sp,ipbb,bb_bb,error)
    if (error) return
    call string_parser_parse('BASEBAND',sp,selcode,.false.,parsecode,error)
    if (error) return
  else if (key.eq.'*') then
    parsecode(ipband)=0
    parsecode(ippol)=0
    parsecode(ipsb)=0
    parsecode(ipbb)=0
  else
    call astro_message(seve%e,rname,'Problem with baseband selection')
    error=.true.
    return
  endif
  ! Case with /RECEIVER option
  if (dorec) then
    if (parsecode(ipband).ne.0) then
      if (parsecode(ipband).ne.inputrec) then
        call astro_message(seve%e,rname,'Inconsistency in Band selection')
        error=.true.
        return
      endif
    else if (parsecode(ipband).eq.0) then
      ! use the option input instead of the code
      parsecode(ipband)=inputrec
    endif
  endif
  ! Find the right units
  iunit=0
  isel=0
  do iu=1,noema%cfebe%pfx%n_units
    if (noema%cfebe%pfx%unit(iu)%iband.ne.parsecode(ipband).and.parsecode(ipband).ne.0) cycle
    if (noema%cfebe%pfx%unit(iu)%pol_code.ne.parsecode(ippol).and.parsecode(ippol).ne.0) cycle
    if (noema%cfebe%pfx%unit(iu)%sb_code.ne.parsecode(ipsb).and.parsecode(ipsb).ne.0) cycle
    if (noema%cfebe%pfx%unit(iu)%bb_code.ne.parsecode(ipbb).and.parsecode(ipbb).ne.0) cycle
    isel=isel+1
    noema%cfebe%i_f%selunit%usel(isel)=iu
  enddo
  noema%cfebe%i_f%selunit%n_ifsel=isel
  !
  dooff=.false.
  if (sic_narg(0).ne.0) then
    ! Read the first
    call sic_ke(line,0,1,firstarg,nc,.false.,error)
    dooff = (firstarg.eq.'OFF')
    if (.not.dooff) then
      call astro_message(seve%e,rname,'Only choice for first argument is OFF, followed by the SPW to switch OFF')
      error = .true.
      return
    else
      if (sic_narg(0).eq.1) then
        call astro_message(seve%e,rname,'Please enter as argument the SPW you want to switch OFF')
        error = .true.
        return
      endif
    endif
  endif
  !
  if (dooff) then
    if (dorange.or.dofreq.or.dochunk.or.dorec.or.dolabel) then
      call astro_message(seve%e,rname,'Switching off SPW can be combined with /BASEBAND option only')
      error=.true.
      return
    endif
  endif
  if (.not.dorange.and..not.dofreq.and..not.dochunk.and..not.dooff) then
    call astro_message(seve%e,rname,'Nothing to do')
    error = .true.
    return
  endif
  !
  if (dolabel) then
    call sic_ch(line,optlabel,1,noema%cfebe%spw%comm%user_label,nc,.true.,error)
    if (error) return
  else
    noema%cfebe%spw%comm%user_label=''
  endif
  !
  if (dorange.or.dofreq) then
    call astro_message(seve%e,rname,'Not yet implemented')
    error=.true.
    return
  endif
  !
  ier=gag_time(noema%cfebe%spw%comm%time)
  if (ier.ne.0) then
    call astro_message(seve%e,rname,'Problem finding time')
    error=.true.
    return
  endif
  !
  if (dooff)  then
    ! SWITCH OFF SOME SPECTRAL WINDOW
    ! check if * was the argument
    dolist=.true.
    if (sic_narg(0).eq.2) then
      dolist=.false.
      doall=.false.
      call sic_ke(line,0,2,arg,nc,.true.,error)
      if (error)  return
      call sic_ambigs_sub(rname,arg,key,nkey,keys,mkeys,error)
      if (error) return
      if (nkey.eq.0) then
        dolist=.true.
      else
        if (key.eq.'*') doall=.true.
      endif
    endif
    !
    !Compute the OBS index of SPW
    call noema_spw_obs_index(rname,noema%cfebe%spw%out,noema%cfebe%pfx,obsindex,error)
    if (error) return
    !
    if (dolist) then
      ! Decode a list of spw which is NOT a sic list
      n_to_reset=0
      do iarg=2,sic_narg(0) ! start at 2 because first is "OFF"
        call sic_i4(line,0,iarg,i1,.true.,error)
        if (error) return
        do iw=1,noema%cfebe%spw%out%n_spw
          do is=1,noema%cfebe%i_f%selunit%n_ifsel
            iunit=noema%cfebe%i_f%selunit%usel(is)
            if (noema%cfebe%spw%out%win(iw)%label.ne.noema%cfebe%pfx%unit(iunit)%label) cycle
            if (obsindex(iw).ne.i1) cycle
            n_to_reset=n_to_reset+1
            spw_to_reset(n_to_reset)=iw
          enddo
        enddo
      enddo
      if (n_to_reset.eq.0) then
        call astro_message(seve%e,rname,'Selection report 0 SPW')
        error=.true.
      endif
    else if (doall) then
      !reset all flexible spectral windows
      n_to_reset=0
      do iw=1,noema%cfebe%spw%out%n_spw
        if (.not.(noema%cfebe%spw%out%win(iw)%flexible)) cycle
        do is=1,noema%cfebe%i_f%selunit%n_ifsel
          iunit=noema%cfebe%i_f%selunit%usel(is)
          if (noema%cfebe%spw%out%win(iw)%label.ne.noema%cfebe%pfx%unit(iunit)%label) cycle
          n_to_reset=n_to_reset+1
          spw_to_reset(n_to_reset)=iw
        enddo
      enddo
    else 
      call astro_message(seve%e,rname,'Problem deconding SPW to reset')
      error=.true.
      return
    endif
    !Check that all spw to reset exist and can be reset before starting to reset
    do ir=1,n_to_reset
      if (spw_to_reset(ir).gt.noema%cfebe%spw%out%n_spw.or.spw_to_reset(ir).le.0) then
        write (mess,'(a,i0,1x,a)') 'SPW #',spw_to_reset(ir),'is not defined'
        call astro_message(seve%e,rname,mess)
        error=.true.
      else if (.not.(noema%cfebe%spw%out%win(spw_to_reset(ir))%flexible)) then
        write (mess,'(a,i0,1x,a)') 'Fixed SPW #',spw_to_reset(ir),'cannot be reset'
        call astro_message(seve%e,rname,mess)
        error=.true.
      endif
    enddo
    if (error) then
      call astro_message(seve%e,rname,'Nothing done')
      return
    endif
    !
    !Do the reset
    do ir=1,n_to_reset
      write (mess,'(a,1x,i0)') 'Resetting Spectral Window #',spw_to_reset(ir)
      call astro_message(seve%i,rname,mess)
      call noema_reset_spw(noema%cfebe%pfx,noema%cfebe%spw%out%win(spw_to_reset(ir)),error)
      if (error) exit
    enddo
    ! Remove empty windows from spw%out type
    call noema_compress_spw(noema%cfebe%spw%out,error)
    if (error) return
  else if (dochunk) then
    !
    ! DEFINE SOME SPECTRAL WINDOWS
    !
    ! Decode the baseband(s) where the SPW will be placed
    ! Find if flexible spw can be configured
    nbefore=noema%cfebe%spw%out%n_spw
    do is=1,noema%cfebe%i_f%selunit%n_ifsel
      iunit=noema%cfebe%i_f%selunit%usel(is)
      imode=noema%cfebe%pfx%unit(iunit)%imode
      !
      if (imode.eq.-1) then
        call astro_message(seve%e,rname,'SPW cannot be placed in a Baseband not configured yet')
        call astro_message(seve%e,rname,'Use BASEBAND command to do it')
        error=.true.
        exit
      else if (imode.le.0.or.imode.gt.noema%cfebe%pfx%unit(iunit)%n_modes) then
        call astro_message(seve%e,rname,'Problem with PolyFix unit mode')
        error=.true.
        exit
      endif
      !
      ! Get the type and the spectral resolution
      gottype=.false.
      do it=1,noema%cfebe%pfx%unit(iunit)%mode(imode)%n_types
        if (.not.(noema%cfebe%pfx%unit(iunit)%mode(imode)%chtype(it)%move_chunk)) cycle
        if (gottype) then
          call astro_message(seve%e,rname,'Several kinds of flexible chunks ??')
          error=.true.
          exit
        endif
        noema%cfebe%spw%comm%itype=it 
        gottype = .true.
      enddo
      if (.not.gottype) then
        call astro_message(seve%e,rname,'This baseband cannot host user defined SPW')
          error=.true.
          exit
      endif
      noema%cfebe%spw%comm%resol= &
        noema%cfebe%pfx%unit(iunit)%mode(imode)%chtype(noema%cfebe%spw%comm%itype)%df_chunks*1000d0 ! in kHz
      ! Chunks
      if (sic_narg(optchunk).eq.0) then
        call astro_message(seve%e,rname,'Please provide a chunk list to define SPW')
        error=.true.
        exit
      endif
      !Decode the list of chunks
      i1 = 1
      do iarg=1,sic_narg(optchunk)
        ! Translate the arguments (as string, because of "TO" in "12 TO 34") and
        ! save them in a string
        call sic_ke(line,optchunk,iarg,spwstring(i1:),nc,.true.,error)
        if (error)  exit
        i1 = i1+nc+1
      enddo
      !
      ! 2) Parse the string into the dedicated type
      !    mspw = max size of problem, no internal limit, your choice
      call sic_parse_listi4(rname,spwstring,spwlist,m_spw,error)
      if (error)  exit
      !
      ! 3) Forbid step different than 1
      if (any(abs(spwlist%i3(1:spwlist%nlist)).ne.1)) then
        call astro_message(seve%e,rname,'Step must be 1 or -1 == Chunks in a spw must be contiguous')
        error = .true.
        exit
      endif
      !
      call noema_config_spw_bychunk_unit(spwlist,noema%recdesc,noema%source,noema%cfebe%rectune, & 
                          noema%cfebe%i_f,noema%cfebe%pfx%unit(iunit),noema%cfebe%spw,error)
      if (error) exit
      !
    enddo
    !
  else
    call astro_message(seve%e,rname,'How did you get there ?')
    error=.true.
    return
  endif
  !
  !
  if (error) then
    !If an error occured we cancel all the SPW we already created
    if (noema%cfebe%spw%out%n_spw.eq.nbefore) then
      error=.true.
      return
    elseif (noema%cfebe%spw%out%n_spw.gt.nbefore) then
      error=.false.
      call astro_message(seve%e,rname,'An error occured no SPW created')
      call noema_last_spw(noema%cfebe%spw%out,n_to_reset,spw_to_reset,error)
      if (error) then
        call astro_message(seve%e,rname,'Could not find the SPW to cancel')
        call astro_message(seve%e,rname,'Check your list of SPW')
        ! Remove empty windows from spw%out type
        call noema_compress_spw(noema%cfebe%spw%out,error)
        if (error) return
        ! look for conflicts
        call noema_check_conflicts(rname,noema%cfebe%spw%out,noema%cfebe%pfx,error)
        if (error) return
        ! Display the spw list again (since numbers changed)
        call noema_list_spw_online(rname,noema%cfebe%spw%out,noema%source, & 
                                    noema%cfebe%pfx,error)
        if (error) return
        return
      endif
      !Check that all spw to reset exist and can be reset before starting to reset
      do ir=1,n_to_reset
        if (spw_to_reset(ir).gt.noema%cfebe%spw%out%n_spw.or.spw_to_reset(ir).le.0) then
          write (mess,'(a,i0,1x,a)') 'SPW #',spw_to_reset(ir),'is not defined'
          call astro_message(seve%e,rname,mess)
          error=.true.
        endif
        if (.not.(noema%cfebe%spw%out%win(spw_to_reset(ir))%flexible)) then
          write (mess,'(a,i0,1x,a)') 'Fixed SPW #',spw_to_reset(ir),'cannot be reset'
          call astro_message(seve%e,rname,mess)
          error=.true.
        endif
        !Do the reset
        call noema_reset_spw(noema%cfebe%pfx,noema%cfebe%spw%out%win(spw_to_reset(ir)),error)
        call noema_compress_spw(noema%cfebe%spw%out,error)
        if (error) exit
      enddo
      if (error) then
        call astro_message(seve%e,rname,'Problem with the reset of SPW')
        call astro_message(seve%e,rname,'Check your list of SPW')
        ! Remove empty windows from spw%out type
          call noema_compress_spw(noema%cfebe%spw%out,error)
          if (error) return
          ! look for conflicts
          call noema_check_conflicts(rname,noema%cfebe%spw%out,noema%cfebe%pfx,error)
          if (error) return
          ! Display the spw list again (since numbers changed)
          call noema_list_spw_online(rname,noema%cfebe%spw%out,noema%source,noema%cfebe%pfx,error)
          if (error) return
!         return
      endif
    endif
  endif
  ! Sort the SPW list
  call noema_sort_spw(noema%cfebe%spw%out,error)
  if (error)  return  
  !
  ! Look for conflict (i.e. same chunk used several times, or chunk 1)
  call noema_check_conflicts(rname,noema%cfebe%spw%out,noema%cfebe%pfx,error)
  if (error)  return
  !
  ! LIST SPW
  call noema_list_spw_online(rname,noema%cfebe%spw%out,noema%source,noema%cfebe%pfx,error)
  if (error) return
  !
  ! PLOT
  !Select all the units for the plot
  noema%cfebe%i_f%selunit%n_ifsel=noema%cfebe%pfx%n_units
  noema%cfebe%i_f%selunit%polmode='B'
  do iu=1,noema%cfebe%pfx%n_units
    noema%cfebe%i_f%selunit%usel(iu)=iu
  enddo
  !
  call noema_plot_selpfx(rname,noema,cplot,molecules,freq_axis,error)
  if (error) return
  ! Let the user with the limits in main axis frequency frame
  call rec_set_limits(rname,cplot,freq_axis%main,error)
  if (error) return
  !
end subroutine noema_spw_online
!
subroutine noema_sort_spw(spwout,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_sort_spw
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! sort the list of spw
  !
  !-----------------------------------------------------------------------
  type(spw_output_t), intent(inout) :: spwout
  logical, intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SPW'
  integer(kind=4) :: iw,first
  integer(kind=4) :: sort(spwout%n_spw)  ! Automatic array
  type(spw_unit_t) :: tmp(m_spw)
  !
!   write (mess,'(i0,1x,a)') spwout%n_spw,'spectral windows in input'
!   call astro_message(seve%i,rname,mess)
  !
  if (spwout%n_spw.eq.0) return ! nothing to sort
  do iw=1,spwout%n_spw
    sort(iw) = iw
  enddo
  call gi0_quicksort_index_with_user_gtge(sort,spwout%n_spw,  &
    sort_spw_gt,sort_spw_ge,error)
  if (error)  return
  !
  ! Half-optimized sorting: duplicate only from point where spw are
  ! not sorted anymore
  do iw=1,spwout%n_spw
    first = iw
    if (sort(iw).ne.iw)  exit
  enddo
  if (first.eq.spwout%n_spw) then
!     call astro_message(seve%i,rname,"List is already sorted, nothing done")
    return
  endif
  !
  ! Duplicate from 'first' to end of list
  do iw=first,spwout%n_spw
    call noema_copy_spw(spwout%win(iw),tmp(iw),error)
    if (error)  return
  enddo
  !
  ! Sort unsorted part of spwout
  do iw=first,spwout%n_spw
    !if iw ne sort(iw) ??? ZZZ
    call noema_copy_spw(tmp(sort(iw)),spwout%win(iw),error)
    if (error)  return
  enddo
  !
end subroutine noema_sort_spw
!
function sort_spw_gt(m,l)
  use my_receiver_globals
  !---------------------------------------------------------------------
  ! @ private
  ! Sorting function for noema_sort_spw (greater than)
  ! ---
  !  NB: THIS SORTS THE GLOBAL SPW STRUCTURE SHARED THROUGH MODULE
  !      MY_RECEIVER_GLOBALS!!!
  !---------------------------------------------------------------------
  logical :: sort_spw_gt
  integer(kind=4), intent(in) :: m,l
  !
  if (noema%cfebe%spw%out%win(m)%resol.ne.noema%cfebe%spw%out%win(l)%resol) then
    ! Sort by resolution (low res first)
    sort_spw_gt = noema%cfebe%spw%out%win(m)%resol.lt.noema%cfebe%spw%out%win(l)%resol
    return
  endif
  if (noema%cfebe%spw%out%win(m)%pol_code.ne.noema%cfebe%spw%out%win(l)%pol_code) then
    ! Sort by polar (H=1, V=2)
    sort_spw_gt = noema%cfebe%spw%out%win(m)%pol_code.gt.noema%cfebe%spw%out%win(l)%pol_code
    return
  endif
  ! Sort by increasing RF/REST frequency
  sort_spw_gt = noema%cfebe%spw%out%win(m)%restmin.gt.noema%cfebe%spw%out%win(l)%restmin
  !
end function sort_spw_gt
!
function sort_spw_ge(m,l)
  use my_receiver_globals
  !---------------------------------------------------------------------
  ! @ private
  ! Sorting function for noema_sort_spw (greater than)
  ! ---
  !  NB: THIS SORTS THE GLOBAL SPW STRUCTURE SHARED THROUGH MODULE
  !      MY_RECEIVER_GLOBALS!!!
  !---------------------------------------------------------------------
  logical :: sort_spw_ge
  integer(kind=4), intent(in) :: m,l
  !
  if (noema%cfebe%spw%out%win(m)%resol.ne.noema%cfebe%spw%out%win(l)%resol) then
    ! Sort by resolution (low res first)
    sort_spw_ge = noema%cfebe%spw%out%win(m)%resol.le.noema%cfebe%spw%out%win(l)%resol
    return
  endif
  if (noema%cfebe%spw%out%win(m)%pol_code.ne.noema%cfebe%spw%out%win(l)%pol_code) then
    ! Sort by polar (H=1, V=2)
    sort_spw_ge = noema%cfebe%spw%out%win(m)%pol_code.ge.noema%cfebe%spw%out%win(l)%pol_code
    return
  endif
  ! Sort by increasing RF/REST frequency
  sort_spw_ge = noema%cfebe%spw%out%win(m)%restmin.ge.noema%cfebe%spw%out%win(l)%restmin
  !
end function sort_spw_ge
!
subroutine noema_sort_spw2(spwout,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_sort_spw2
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! sort the list of spw in spw2
  ! NB2: Duplication of noema_sort_spw to be able to work on 2 list of spw in parralel
  !      Could be merged using contains but requires gfortran > 4.4
  !
  !-----------------------------------------------------------------------
  type(spw_output_t), intent(inout) :: spwout
  logical, intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SPW'
  integer(kind=4) :: iw,first
  integer(kind=4) :: sort(spwout%n_spw)  ! Automatic array
  type(spw_unit_t) :: tmp(m_spw)
  !
!   write (mess,'(i0,1x,a)') spwout%n_spw,'spectral windows in input'
!   call astro_message(seve%i,rname,mess)
  !
  do iw=1,spwout%n_spw
    sort(iw) = iw
  enddo
  call gi0_quicksort_index_with_user_gtge(sort,spwout%n_spw,  &
    sort_spw2_gt,sort_spw2_ge,error)
  if (error)  return
  !
  ! Half-optimized sorting: duplicate only from point where spw are
  ! not sorted anymore
  do iw=1,spwout%n_spw
    first = iw
    if (sort(iw).ne.iw)  exit
  enddo
  if (first.eq.spwout%n_spw) then
!     call astro_message(seve%i,rname,"List is already sorted, nothing done")
    return
  endif
  !
  ! Duplicate from 'first' to end of list
  do iw=first,spwout%n_spw
    call noema_copy_spw(spwout%win(iw),tmp(iw),error)
    if (error)  return
  enddo
  !
  ! Sort unsorted part of spwout
  do iw=first,spwout%n_spw
    !if iw ne sort(iw) ??? ZZZ
    call noema_copy_spw(tmp(sort(iw)),spwout%win(iw),error)
    if (error)  return
  enddo
  !
end subroutine noema_sort_spw2
!
function sort_spw2_gt(m,l)
  use my_receiver_globals
  !---------------------------------------------------------------------
  ! @ private
  ! Sorting function for noema_sort_spw (greater than)
  ! ---
  !  NB: THIS SORTS THE GLOBAL SPW2 STRUCTURE SHARED THROUGH MODULE
  !      MY_RECEIVER_GLOBALS!!!
  ! NB2: Duplication of sort_spw_ge to be able to work on 2 list of spw in parralel
  !      Could be merged using contains but requires gfortran > 4.4
  !---------------------------------------------------------------------
  logical :: sort_spw2_gt
  integer(kind=4), intent(in) :: m,l
  !
  if (spw2%out%win(m)%resol.ne.spw2%out%win(l)%resol) then
    ! Sort by resolution (low res first)
    sort_spw2_gt = spw2%out%win(m)%resol.lt.spw2%out%win(l)%resol
    return
  endif
  if (spw2%out%win(m)%pol_code.ne.spw2%out%win(l)%pol_code) then
    ! Sort by polar (H=1, V=2)
    sort_spw2_gt = spw2%out%win(m)%pol_code.gt.spw2%out%win(l)%pol_code
    return
  endif
  ! Sort by increasing RF/REST frequency
  sort_spw2_gt = spw2%out%win(m)%restmin.gt.spw2%out%win(l)%restmin
  !
end function sort_spw2_gt
!
function sort_spw2_ge(m,l)
  use my_receiver_globals
  !---------------------------------------------------------------------
  ! @ private
  ! Sorting function for noema_sort_spw (greater than)
  ! ---
  !  NB: THIS SORTS THE GLOBAL SPW2 STRUCTURE SHARED THROUGH MODULE
  !      MY_RECEIVER_GLOBALS!!!
  ! NB2: Duplication of sort_spw_ge to be able to work on 2 list of spw in parralel
  !      Could be merged using contains but requires gfortran > 4.4
  !---------------------------------------------------------------------
  logical :: sort_spw2_ge
  integer(kind=4), intent(in) :: m,l
  !
  if (spw2%out%win(m)%resol.ne.spw2%out%win(l)%resol) then
    ! Sort by resolution (low res first)
    sort_spw2_ge = spw2%out%win(m)%resol.le.spw2%out%win(l)%resol
    return
  endif
  if (spw2%out%win(m)%pol_code.ne.spw2%out%win(l)%pol_code) then
    ! Sort by polar (H=1, V=2)
    sort_spw2_ge = spw2%out%win(m)%pol_code.ge.spw2%out%win(l)%pol_code
    return
  endif
  ! Sort by increasing RF/REST frequency
  sort_spw2_ge = spw2%out%win(m)%restmin.ge.spw2%out%win(l)%restmin
  !
end function sort_spw2_ge
!
subroutine noema_config_spw_byfreq(noema,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_config_spw_byfreq
  use astro_noema_type
  !-----------------------------------------------------------------------
  ! @ private
  ! recognize which mode and type of chunk is being configured
  !
  !-----------------------------------------------------------------------
  type(noema_t), intent(inout) :: noema
  logical, intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SPW'
  integer(kind=4)       :: is,ich1,ich2,imode,ity,iunit,ibb,isb
  real(kind=8)  :: fmin,fmax,frf,f1,f2
  logical       :: found,needdef,gottype
  character(len=200) :: mess
  !
  if (.not.noema%cfebe%defined) then
    call astro_message(seve%e,rname,'Please define a tuning before setting up backends')
    error = .true.
    return
  endif
  !
  if (noema%cfebe%spw%comm%fmin.ge.noema%cfebe%spw%comm%fmax) then
    call astro_message(seve%e,rname,'Please define a spectral window with fmin < fmax')
    error = .true.
    return
  endif
  found = .false.
  !check that spw falls in the units
  do is=1,noema%cfebe%i_f%selunit%n_ifsel
    iunit=noema%cfebe%i_f%selunit%usel(is)
    ibb=noema%cfebe%pfx%unit(iunit)%bb_code
    isb=noema%cfebe%pfx%unit(iunit)%sb_code
    ! Get min and max freq for the current pfx unit
    call if1torf(noema%cfebe%rectune%flo1,noema%recdesc%bblim(1,ibb),isb,frf,error)
    if (error) return
    call rftorest(noema%source%dopshift,frf,f1,error)
    if (error) return
    call if1torf(noema%cfebe%rectune%flo1,noema%recdesc%bblim(2,ibb),isb,frf,error)
    if (error) return
    call rftorest(noema%source%dopshift,frf,f2,error)
    if (error) return
    fmin = min(f1,f2)
    fmax = max(f1,f2)
    if (noema%cfebe%spw%comm%fmin*mhzperghz.gt.fmax.or. &
          noema%cfebe%spw%comm%fmax*mhzperghz.lt.fmin) then
      ! spw command is out of the current unit
      cycle
    else if (noema%cfebe%spw%comm%fmax*mhzperghz.le.fmax.and. &
              noema%cfebe%spw%comm%fmin*mhzperghz.ge.fmin) then
      found = .true.
      write (mess,'(a,1x,i0,1x,a)') 'SPW fits in unit',iunit,noema%cfebe%pfx%unit(iunit)%label
      call astro_message(seve%i,rname,mess)
      ! Check that SPW can be defined here
      if (noema%cfebe%pfx%unit(iunit)%imode.eq.-1) then
        call astro_message(seve%w,rname,'Unit mode is not defined. Use command BASEBAND to select one.')
        cycle ! go to next baseband
      endif
      imode=noema%cfebe%pfx%unit(iunit)%imode
      gottype=.false.
      do ity=1,noema%cfebe%pfx%unit(iunit)%mode(imode)%n_types
        if (.not.(noema%cfebe%pfx%unit(iunit)%mode(imode)%chtype(ity)%move_chunk)) cycle
        noema%cfebe%spw%comm%itype=ity 
        noema%cfebe%spw%comm%resol=noema%cfebe%pfx%unit(iunit)%mode(imode)%chtype(ity)%df_chunks*1000d0 ! in kHz
        gottype=.true.
      enddo
      if (.not.gottype) then
        call astro_message(seve%w,rname,'Selected mode for the current unit does not allow SPW')
        cycle ! go to next baseband
      endif
      !Define the spw: 
      !1: find the involved chunks
      call noema_find_chunks(noema%recdesc,noema%cfebe%rectune,noema%source,noema%cfebe%spw%comm, &
                              noema%cfebe%pfx%unit(iunit),ich1,ich2,error)
      if (error) return
      !2: If all chunks already covered then no need to configure them
      call noema_check_chunks(rname,ich1,ich2,noema%cfebe%spw,noema%cfebe%pfx%unit(iunit),needdef,error)
      if (error) return
      if (needdef) then
        !3: configure the chunks
        call noema_config_chunks(rname,ich1,ich2,noema%cfebe%pfx%unit(iunit),noema%cfebe%spw%comm,error)
        if (error) return
        !4: update the list of spw
        call noema_add_spw(noema%recdesc,noema%source,noema%cfebe%rectune,ich1,ich2, &
                                noema%cfebe%pfx%unit(iunit),noema%cfebe%spw,error)
        if (error) return
      else
        ! nothing done if all chunks already used
        call astro_message(seve%w,rname,'All needed chunks are already configured, no new SPW created')
      endif
      cycle
    else
      call astro_message(seve%e,rname,'The defined spectral window does not fit in a baseband')
      call astro_message(seve%e,rname,'Please split your command line')
      error = .true.
      return
    endif
  enddo ! selected basebands
  if (.not.found) then
    call astro_message(seve%e,rname,'The defined spectral window is out of the available ranges')
    error = .true.
    return
  endif
  !
end subroutine noema_config_spw_byfreq
!
subroutine noema_config_spw_bychunk(spwin,inoema,error)
  use gbl_message
  use gkernel_interfaces
  use gkernel_types
  use astro_interfaces, except_this=>noema_config_spw_bychunk
  use astro_noema_type
  !----------------------------------------------------------------------
  ! @ private
  ! configure spw by chunk intervals, in all selected unit
  !
  !-----------------------------------------------------------------------
  type(sic_listi4_t), intent(in) :: spwin
  type(noema_t), intent(inout) :: inoema
  logical, intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SPW'
  integer(kind=4)       :: iunit,is
  !
  if (.not.inoema%cfebe%defined) then
    call astro_message(seve%e,rname,'Please define a tuning before setting up backends')
    error = .true.
    return
  endif
  !
  !check that spw falls in the units
  do is=1,inoema%cfebe%i_f%selunit%n_ifsel
    iunit=inoema%cfebe%i_f%selunit%usel(is)
    call noema_config_spw_bychunk_unit(spwin,inoema%recdesc,inoema%source,inoema%cfebe%rectune, &
                                        inoema%cfebe%i_f,inoema%cfebe%pfx%unit(iunit),inoema%cfebe%spw,error)
    if (error) return
  enddo ! selected basebands
  !
end subroutine noema_config_spw_bychunk
!
subroutine noema_config_spw_bychunk_unit(spwin,rdesc,rsou,rtune,noema_if,pfxu,spw,error)
  use gbl_message
  use gkernel_interfaces
  use gkernel_types
  use astro_interfaces, except_this=>noema_config_spw_bychunk_unit
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! configure spw by chunk intervals for a given unit
  !
  !--------------------------------fit---------------------------------------
  type(sic_listi4_t), intent(in) :: spwin
  type(receiver_desc_t), intent(in) :: rdesc
  type(receiver_source_t), intent(in) :: rsou
  type(receiver_tune_t), intent(in) :: rtune
  type(noema_if_t), intent(in) :: noema_if
  type(pfx_unit_t), intent(inout) :: pfxu
  type(spw_t), intent(inout) :: spw
  logical, intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SPW'
  integer(kind=4)       :: ispw,max_chunk,ic1,ic2,imode,ity
  logical :: needdef,gottype
  character(len=128) :: mess
  !
  ! SPW characteristics come from pfx unit
  if (pfxu%imode.eq.-1) then
    write (mess,'(a,1x,a,1x,a)') 'Unit',trim(pfxu%label),'has no mode defined. Use command BASEBAND to select one'
    call astro_message(seve%w,rname,mess)
    return
  endif
  imode=pfxu%imode
  gottype=.false.
  do ity=1,pfxu%mode(imode)%n_types
    if (.not.(pfxu%mode(imode)%chtype(ity)%move_chunk)) cycle
    spw%comm%itype=ity 
    spw%comm%resol=pfxu%mode(imode)%chtype(ity)%df_chunks*1000d0 ! in kHz
    gottype=.true.
  enddo
  if (.not.gottype) then
    call astro_message(seve%w,rname,'Selected mode for the current unit does not allow SPW')
    return
  endif
  ! Check that chunks are not out of range 1-MaxChunk
  max_chunk=pfxu%mode(pfxu%imode)%chtype(spw%comm%itype)%n_chunks
  if (any(spwin%i1(1:spwin%nlist).le.0).or. &
      any(spwin%i2(1:spwin%nlist).le.0).or. &
      any(spwin%i1(1:spwin%nlist).gt.max_chunk).or. &
      any(spwin%i2(1:spwin%nlist).gt.max_chunk)) then
    call astro_message(seve%e,rname,'Try to configure non existing chunks - aborted')
    error = .true.
    return
  endif
  ! configure the spw
  do ispw=1,spwin%nlist
    ic1=min(spwin%i1(ispw),spwin%i2(ispw))
    ic2=max(spwin%i1(ispw),spwin%i2(ispw))
    !Define the spw: 
    !1: if all chunks are already defined then nothing is done
    call noema_check_chunks(rname,ic1,ic2,spw,pfxu,needdef,error)
    if (error) return
    if (needdef) then
      !2: configure the chunks
      call noema_config_chunks(rname,ic1,ic2,pfxu,spw%comm,error)
      if (error) return
      !3: update the list of spw
      call noema_add_spw(rdesc,rsou,rtune,ic1,ic2,pfxu,spw,error)
      if (error) return
    else
      call astro_message(seve%w,rname,'All chunks are already defined, no SPW added')
    endif
  enddo
  !
end subroutine noema_config_spw_bychunk_unit
!
subroutine noema_check_chunks(rname,ic1,ic2,spw,pfxunit,needdef,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_check_chunks
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! check if all chunk of a spw are already defined. 
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  integer(kind=4), intent(in)   :: ic1,ic2
  type(spw_t), intent(in) :: spw
  type(pfx_unit_t), intent(in) :: pfxunit
  logical, intent(inout) :: needdef
  logical, intent(inout) :: error
  ! Local
  integer(kind=4) :: ic,imode,itype
  character(len=128) :: mess
  !
  if (pfxunit%imode.eq.-1) then
    write (mess,'(a,1x,a,1x,a)') 'Unit',trim(pfxunit%label),'has no mode defined. Use command BASEBAND to select one'
    call astro_message(seve%w,rname,mess)
    return
  endif
  imode=pfxunit%imode
  itype=spw%comm%itype
  needdef=.false.
  do ic=ic1,ic2
    if (pfxunit%mode(imode)%chtype(itype)%chunks(ic).ne.0) cycle
    needdef=.true.
  enddo
  !
end subroutine noema_check_chunks
!
subroutine noema_add_spw(rdesc,rsou,rtune,ic1,ic2,pfxunit,spw,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_add_spw
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! add the defined window (might be different from input)
  !
  !-----------------------------------------------------------------------
  type(receiver_desc_t), intent(in) :: rdesc
  type(receiver_source_t), intent(in) :: rsou
  type(receiver_tune_t), intent(in) :: rtune
  integer(kind=4), intent(in)   :: ic1,ic2
  type(pfx_unit_t), intent(inout) :: pfxunit
  type(spw_t), intent(inout) :: spw
  logical, intent(inout) :: error
  ! Local
  integer(kind=4) :: iw,imode,itype
  real(kind=8) :: ch1min,ch1max,ch2min,ch2max
  !
  imode=pfxunit%imode
  itype=spw%comm%itype
  !
  spw%out%n_spw = spw%out%n_spw+1
  iw = spw%out%n_spw
  spw%out%win(iw)%ich1 = ic1
  spw%out%win(iw)%ich2 = ic2
  spw%out%win(iw)%itype = itype
  spw%out%win(iw)%resol = spw%comm%resol
  spw%out%win(iw)%user_label = trim(spw%comm%user_label)
  spw%out%win(iw)%ctime = spw%comm%time                 !creation time (nanos since 1970)
  spw%out%win(iw)%iband = pfxunit%iband
  spw%out%win(iw)%sb_code = pfxunit%sb_code
  spw%out%win(iw)%pol_code = pfxunit%pol_code
  spw%out%win(iw)%bb_code= pfxunit%bb_code
  spw%out%win(iw)%label= pfxunit%label
  spw%out%win(iw)%flexible=pfxunit%mode(imode)%chtype(itype)%move_chunk
  !
  call noema_chunk_minmax(rdesc,rsou,rtune,pfxunit%bb_code,pfxunit%sb_code, &
        pfxunit%mode(pfxunit%imode)%chtype(spw%comm%itype),ic1,ch1min,ch1max,error)
  if (error) return
  call noema_chunk_minmax(rdesc,rsou,rtune,pfxunit%bb_code,pfxunit%sb_code, &
          pfxunit%mode(imode)%chtype(itype),ic2,ch2min,ch2max,error)
  if (error) return
  spw%out%win(iw)%restmin=min(ch1min,ch2min)
  spw%out%win(iw)%restmax=max(ch1max,ch2max)
  !
end subroutine noema_add_spw
!
subroutine noema_find_chunks(rdesc,rtune,rsou,spwcomm,pfxunit,ic1,ic2,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_find_chunks
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! recognize which mode and type of chunk is being configured
  !
  !-----------------------------------------------------------------------
  type(receiver_desc_t), intent(in) :: rdesc
  type(receiver_tune_t), intent(in) :: rtune
  type(receiver_source_t), intent(in) :: rsou
  type(spw_comm_t), intent(inout) :: spwcomm
  type(pfx_unit_t), intent(inout) :: pfxunit
  integer(kind=4), intent(out)  :: ic1,ic2
  logical, intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SPW'
  integer(kind=4) :: ich,ich1,ich2
  real(kind=8) :: chmin,chmax
  character(len=200) :: mess
  !
  if (pfxunit%imode.eq.-1) then
    write (mess,'(a,1x,a,1x,a)') 'Unit',trim(pfxunit%label),'has no mode defined. Use command BASEBAND to select one'
    call astro_message(seve%w,rname,mess)
    return
  endif
  !
  ich1=-1
  ich2=-1
  do ich=1,pfxunit%mode(pfxunit%imode)%chtype(spwcomm%itype)%n_chunks
    call noema_chunk_minmax(rdesc,rsou,rtune,pfxunit%bb_code,pfxunit%sb_code, &
          pfxunit%mode(pfxunit%imode)%chtype(spwcomm%itype),ich,chmin,chmax,error)
    if (error) return
    if (spwcomm%fmin*1d3.ge.chmin.and.spwcomm%fmin*1d3.lt.chmax) then
      ich1 = ich
    endif
    if (spwcomm%fmax*1d3.ge.chmin.and.spwcomm%fmax*1d3.lt.chmax) then
      ich2 = ich
    endif
  enddo
  ic1=min(ich1,ich2)
  ic2=max(ich1,ich2)
  write (mess,'(a,1x,i0,1x,a,1x,i0)') 'Spectral window covers chunks',ic1,'to',ic2
  call astro_message(seve%i,rname,mess)
  !
end subroutine noema_find_chunks
!
subroutine noema_chunk_minmax(rdesc,rsou,rtune,bb_code,sb_code,pfxtype,ich,chmin,chmax,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_chunk_minmax
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! compute rest frequency limits of a given chunk
  !
  !-----------------------------------------------------------------------
  type(receiver_desc_t), intent(in) :: rdesc
  type(receiver_source_t), intent(in) :: rsou
  type(receiver_tune_t), intent(in) :: rtune
  integer(kind=4), intent(in)   :: bb_code !
  integer(kind=4), intent(in)   :: sb_code !
  type(pfx_type_t), intent(in) :: pfxtype
  integer(kind=4), intent(in)  :: ich
  real(kind=8), intent(out)  :: chmin,chmax
  logical, intent(inout) :: error
  ! Local
  real(kind=8) :: fch,fif2,fif1,f1,f2,frf
  !
  fch = pfxtype%if2ch0+(ich-1)*pfxtype%width_chunk
  fif2 = fch-pfxtype%width_chunk/2d0
  fif2=max(fif2,0d0)
  call if2toif1(rdesc%flo2,fif2,bb_code,fif1,error)
  if (error) return
  call if1torf(rtune%flo1,fif1,sb_code,frf,error)
  if (error) return
  call rftorest(rsou%dopshift,frf,f1,error)
  if (error) return
  fif2 = fch+pfxtype%width_chunk/2d0
  call if2toif1(rdesc%flo2,fif2,bb_code,fif1,error)
  if (error) return
  call if1torf(rtune%flo1,fif1,sb_code,frf,error)
  if (error) return
  call rftorest(rsou%dopshift,frf,f2,error)
  if (error) return
  chmin = min(f1,f2)
  chmax = max(f1,f2)
  !
end subroutine noema_chunk_minmax
!
subroutine noema_config_chunks(rname,ic1,ic2,pfxunit,spwcomm,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_config_chunks
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! recognize which mode and type of chunk is being configured
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(in)  :: rname
  integer(kind=4), intent(in)  :: ic1,ic2
  type(pfx_unit_t), intent(inout) :: pfxunit
  type(spw_comm_t),intent(in)      :: spwcomm
  logical, intent(inout) :: error
  ! Local
  integer(kind=4) :: ic,imode,itype
  logical :: allowconflict
  character(len=200) :: mess
  integer(kind=4) :: next_usage,nch
  !
  if (pfxunit%imode.eq.-1) then
    write (mess,'(a,1x,a,1x,a)') 'Unit',trim(pfxunit%label),'has no mode defined. Use command BASEBAND to select one'
    call astro_message(seve%w,rname,mess)
    return
  endif
  imode=pfxunit%imode
  itype=spwcomm%itype
  allowconflict=spwcomm%allowconflict
  nch = ic2-ic1+1
  next_usage = pfxunit%mode(imode)%chtype(itype)%usage+nch
  if (.not.allowconflict.and.next_usage.gt.pfxunit%mode(imode)%chtype(itype)%use_chunks) then
    call astro_message(seve%e,rname,'SPW requiring more resources than available')
    call astro_message(seve%e,rname,'SPW not configured')
    error = .true.
   return
 endif
!
  do ic=ic1,ic2
    pfxunit%mode(imode)%chtype(itype)%chunks(ic) = pfxunit%mode(imode)%chtype(itype)%chunks(ic)+1
    if (pfxunit%mode(imode)%chtype(itype)%chunks(ic).eq.1) then
      pfxunit%mode(imode)%chtype(itype)%usage =  &
        pfxunit%mode(imode)%chtype(itype)%usage+1
    else if (pfxunit%mode(imode)%chtype(itype)%chunks(ic).gt.1.and..not.allowconflict) then
      call astro_message(seve%e,rname,'SPW requires a chunk already used')
      call astro_message(seve%e,rname,'SPW not configured')
      pfxunit%mode(imode)%chtype(itype)%chunks(ic) = pfxunit%mode(imode)%chtype(itype)%chunks(ic)-1
      error = .true.
   return
    endif 
  enddo
  if (.not.(pfxunit%mode(imode)%chtype(itype)%move_chunk)) return
  write (mess,'(a,1x,a,1x,a,1x,a,1x,i0,a)') 'Unit',pfxunit%label,    &
              trim(pfxunit%mode(imode)%chtype(itype)%typename),'is used at', &
              nint(100.0*pfxunit%mode(imode)%chtype(itype)%usage/ &
              pfxunit%mode(imode)%chtype(itype)%use_chunks),'%'
  call astro_message(seve%i,rname,mess)
  if (allowconflict.and.pfxunit%mode(imode)%chtype(itype)%usage.gt. &
        pfxunit%mode(imode)%chtype(itype)%use_chunks) then
      call astro_message(seve%w,rname,'You are using more resources than available')
  endif
  !
end subroutine noema_config_chunks
!
subroutine pfx_fixed_spw(rname,rdesc,rsou,rtune,pfxu,spw,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>pfx_fixed_spw
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Define coverage of chunks without flexibility
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(receiver_desc_t), intent(inout) :: rdesc
  type(receiver_source_t), intent(inout) :: rsou
  type(receiver_tune_t), intent(inout) :: rtune
  type(pfx_unit_t), intent(inout) :: pfxu
  type(spw_t), intent(inout) :: spw
  logical, intent(inout) :: error
  ! local
  integer(kind=4)       :: imode,itype
  character(len=128) :: mess
  !
  if (pfxu%imode.eq.-1) then
    write (mess,'(a,1x,a,1x,a)') 'Unit',trim(pfxu%label),'has no mode defined. Use command BASEBAND to select one'
    call astro_message(seve%w,rname,mess)
    return
  endif
  imode = pfxu%imode
  do itype=1,pfxu%mode(imode)%n_types
    if (pfxu%mode(imode)%chtype(itype)%move_chunk) cycle
    spw%comm%itype=itype
    spw%comm%resol=pfxu%mode(imode)%chtype(itype)%df_chunks*1d3
    spw%comm%time=-1 ! no time for auto spw
    !all chunks are used ich1=1, ich2 = use_chunks
    call noema_config_chunks(rname,1,pfxu%mode(imode)%chtype(itype)%use_chunks, &
                              pfxu,spw%comm,error)
    if (error) return
    call noema_add_spw(rdesc,rsou,rtune,1,pfxu%mode(imode)%chtype(itype)%use_chunks,pfxu,spw,error)
    if (error) return
  enddo
  !
end subroutine pfx_fixed_spw
!
subroutine noema_assign_units(rname,nifproc,pfx,rdesc,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_assign_units
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! assign IF cable to polyfix units
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(correlator_input_t), intent(in) :: nifproc
  type(pfx_t), intent(inout) :: pfx
  logical, intent(inout) :: error
  type(receiver_desc_t), intent(in) :: rdesc
  ! local
  integer(kind=4)       :: icable,iunit,ib
  integer(kind=4)       :: bbcode(rdesc%n_bbands)
  !
  iunit=0
  do icable=1,nifproc%n_ifcables ! loop on if_cables
    ! ignore unused cables
    if (nifproc%ifc(icable)%iband.eq.0) cycle
    ! Introduce the basebands (IF split in 2)
    ! Define the order to respect the nomenclature (pietu memo 2017)
    if (nifproc%ifc(icable)%sb_code.eq.lsb_code) then
      bbcode(1) = outer_code
      bbcode(2) = inner_code
    else if (nifproc%ifc(icable)%sb_code.eq.usb_code) then
      bbcode(1) = inner_code
      bbcode(2) = outer_code
    else
      call astro_message(seve%e,rname,'Problem with sideband determination')
      error = .true.
      return
    endif
    do ib = 1,rdesc%n_bbands
      iunit=iunit+1
      if (iunit.gt.pfx%n_units) then
        call astro_message(seve%e,rname,'More basebands than units')
        error = .true.
        return
      endif
      pfx%unit(iunit)%iband = nifproc%ifc(icable)%iband
      pfx%unit(iunit)%pol_code = nifproc%ifc(icable)%pol_code
      pfx%unit(iunit)%sb_code = nifproc%ifc(icable)%sb_code
      pfx%unit(iunit)%bb_code = bbcode(ib)
      pfx%unit(iunit)%label = nifproc%ifc(icable)%label
      ! Add bband to label
      write (pfx%unit(iunit)%label,'(a,a)')  trim(nifproc%ifc(icable)%label),rdesc%bbname(bbcode(ib))(1:1)
    enddo ! ib
  enddo ! icable
  !
end subroutine noema_assign_units
!
subroutine noema_plot_selpfx(rname,inoema,cplot,cata,drawaxis,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_plot_selpfx
  use astro_noema_type
  !-----------------------------------------------------------------------
  ! @ private
  ! Displays the backend coverage
  !
  !-----------------------------------------------------------------------
  character(len=*), intent(in) :: rname
  type(noema_t), intent(in) :: inoema
  type(current_boxes_t), intent(inout) :: cplot
  type(plot_molecules_t), intent(in) :: cata
  type(frequency_axis_t), intent(in) :: drawaxis
  logical, intent(inout) :: error
  ! local
  integer(kind=4), parameter :: it = 1 ! only 1 tuning at a time
  integer(kind=4)       :: is,ib,sb_code(m_sideband),ibox,iu,irec,ib1,ib2,iinc,ity,isel
  integer(kind=4)       :: ipol,lmess
  integer(kind=4)       :: selband,selbb,selsb,imode,selpol,overload,conflict,n_modebox,prevmode,iunit
  real(kind=8)          :: px,py,yoff,xt,yt,prevxt
  real(kind=4)          :: smess
  character(len=256)    :: boxlabel,messtitle,mess
  character(len=256)    :: defchar,smallchar,molchar,hugechar
  character(len=512)    :: comm
!   type(frequency_box_phys_t) :: recbox
  type(draw_mark_t)     :: mark
  type(draw_rect_t)     :: recnot
  logical               :: do_idchunk,chunk1,samemode,alloff,outlo
!  integer(kind=4), parameter :: iframedash = 3
!   integer(kind=4), parameter :: idefdash = 1
  !
  !
  if (.not.inoema%cfebe%i_f%ifproc%defined) then
    call astro_message(seve%e,rname,'Please configure first the correlator inputs and settings (BASEBAND command)')
    error = .true.
    return
  endif
  !
  ! check whether more resource than available are used or if chunks are used by more than 1 spw
  chunk1=.false.
  call noema_pfx_status(inoema%cfebe%pfx,overload,conflict,chunk1,alloff,error)
  if (error) return
  !Determine number of box to plot
  if (inoema%cfebe%i_f%selunit%polmode.eq.'B') then
    cplot%nbox = inoema%cfebe%i_f%selunit%n_ifsel/2
  else
    cplot%nbox = inoema%cfebe%i_f%selunit%n_ifsel
  endif
  if (cplot%nbox.eq.1) then
    do_idchunk=.true.
  else
    do_idchunk=.false.
  endif
  !
  ! Reset boxes
  do ib=1,cplot%nbox
    call rec_reset_box(cplot%box(ib),error)
    if (error) return
  enddo
  !
  ! Clear the current window, and ONLY the current window
  call gr_execl('CHANGE DIRECTORY') ! No arg = go to the top directory
  call gr_execl('CLEAR DIRECTORY') ! No arg = clear the whole window
  !
  ! get page info
  px = 0
  py = 0
  call sic_get_dble('page_x',px,error)
  if (error) return
  call sic_get_dble('page_y',py,error)
  if (error) return
  !
  cplot%desc%plotmode = pm_basebands
  cplot%desc%hugechar = 1.5
  if (cplot%nbox.eq.1) then
    cplot%desc%defchar = 0.4
    cplot%desc%smallchar = 0.3
    cplot%desc%molchar = 0.2
  else if (cplot%nbox.gt.4) then
    cplot%desc%defchar = 0.3
    cplot%desc%smallchar = 0.2
    cplot%desc%molchar = 0.2
  else
    cplot%desc%defchar = 0.4
    cplot%desc%smallchar = 0.3
    cplot%desc%molchar = 0.2
  endif
  write (defchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%defchar
  write (smallchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%smallchar
  write (molchar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%molchar
  write (hugechar,'(a,1x,f0.3)') 'SET CHARACTER',cplot%desc%hugechar
  !
  call gr_exec1('TICKSPACE 0 0 0 0')
  call gr_pen(colour=adefcol,idash=1,error=error)
  if (error) return
  call gr_exec1(defchar)
  ! set plot parameters
  if (cplot%nbox.gt.4) then
    yoff = 1.d0
  else
    yoff = 1.5d0
  endif
  ibox = 0
  irec = 0
  if (inoema%recdesc%n_sbands.eq.2) then
    sb_code(1) = lsb_code !lsb first to show increasing frequencies
    sb_code(2) = usb_code
  else if (inoema%recdesc%n_sbands.eq.1) then
    sb_code(1) = inoema%cfebe%rectune%sb_code
  else
    call astro_message(seve%e,rname,'Problem with number of sidebands')
    error = .true.
    return
  endif
  do is=1,inoema%recdesc%n_sbands !sidebands
    !sort basebands by frequency
    if (sb_code(is).eq.lsb_code) then
      ib1 = 1
      ib2 = inoema%recdesc%n_bbands
      iinc = 1
    else if (sb_code(is).eq.usb_code) then
      ib1 = inoema%recdesc%n_bbands
      ib2 = 1
      iinc = -1
    else
      call astro_message(seve%e,rname,'Problem with sideband determination')
      error = .true.
      return
    endif
    do ib=ib1,ib2,iinc
      do isel=1,inoema%cfebe%i_f%selunit%n_ifsel
        iunit=inoema%cfebe%i_f%selunit%usel(isel)
        if (inoema%cfebe%i_f%selunit%polmode.eq.'B') then
          ! do not duplicate box in dual polar mode
          if (inoema%cfebe%pfx%unit(iunit)%pol_code.eq.2) cycle
        endif
        selband=inoema%cfebe%pfx%unit(iunit)%iband
        selbb=inoema%cfebe%pfx%unit(iunit)%bb_code
        selsb=inoema%cfebe%pfx%unit(iunit)%sb_code
        selpol=inoema%cfebe%pfx%unit(iunit)%pol_code
        if (inoema%cfebe%rectune%iband.ne.selband) cycle
        if (sb_code(is).ne.selsb) cycle
        if (ib.ne.selbb) cycle
!           if (noema_if%selunit%polmode.ne.'B'.or.noema_if%ifproc%ifc(icab)%pol_code.eq.1) then
        ibox = ibox+1
        boxlabel = ''
        prevmode=-1
        n_modebox=0
        cplot%box(ibox)%iband = inoema%cfebe%rectune%iband
        cplot%box(ibox)%sb_code = sb_code(is)
        cplot%box(ibox)%bb_code = ib
        cplot%box(ibox)%phys%sx = px-4.5d0 ! depends on plot orientation
        if (do_idchunk) then
          cplot%box(ibox)%phys%sy = 1d0*py/3
          cplot%box(ibox)%phys%ymax = 2d0*py/3
        else
          cplot%box(ibox)%phys%sy = min((py-4.d0)/cplot%nbox-yoff,4.) ! depends on baseband number
          cplot%box(ibox)%phys%ymax = py-3.d0-(cplot%box(ibox)%phys%sy+yoff)*(ibox-1)
        endif
        cplot%box(ibox)%phys%xmin = 3.d0
        cplot%box(ibox)%phys%xmax = cplot%box(ibox)%phys%xmin+cplot%box(ibox)%phys%sx
        cplot%box(ibox)%phys%ymin = cplot%box(ibox)%phys%ymax-cplot%box(ibox)%phys%sy
!           if (it.ne.irec) then
!             recbox%ymax = cplot%box(ibox)%phys%ymax+1.2*cplot%desc%defchar
!             irec = it
!             recbox%xmin = cplot%box(ibox)%phys%xmin-1
!             recbox%xmax = cplot%box(ibox)%phys%xmax+0.5
!           endif
        write (comm,'(a,i0)') 'CREATE DIRECTORY BOX',ibox
        call gr_execl(comm)
        write (comm,'(a,i0)') 'CHANGE DIRECTORY BOX',ibox
        call gr_execl(comm)
        call rec_def_fbox(inoema%recdesc%bblim(1,ib),inoema%recdesc%bblim(2,ib),'IF1',cplot%box(ibox), &
                            inoema%recdesc,inoema%source,inoema%cfebe%rectune%flo1,error)
        if (error) return
        ! Locate the box
        call rec_locate_fbox(cplot%box(ibox)%phys,error)
        if (error) return
        if (ibox.eq.1) then
          call gr_exec1(defchar)
          call rec_draw_source(rname,inoema%source,error)
          if (error) return
          call gr_exec1(defchar)
        endif
        ! background for areas not covered
        recnot%xmin = cplot%box(ibox)%rest%xmin
        recnot%xmax = cplot%box(ibox)%rest%xmax
        recnot%ymin = cplot%box(ibox)%rest%ymin
        recnot%ymax = cplot%box(ibox)%rest%ymax
        recnot%col = anotselcol
        call rec_draw_frect(recnot,cplot%box(ibox)%rest,error)
        if (error) return
        do iu=1,inoema%cfebe%pfx%n_units ! loop on backend units
          if (inoema%cfebe%pfx%unit(iu)%iband.ne.selband.or.   &
              inoema%cfebe%pfx%unit(iu)%sb_code.ne.selsb.or.   &
              inoema%cfebe%pfx%unit(iu)%bb_code.ne.selbb) cycle
          if (inoema%cfebe%pfx%unit(iu)%imode.ne.-1) then
            call rec_def_fbox_chunks(cplot%box(ibox)%if2%xmin,cplot%box(ibox)%if2%xmax, &
                                      inoema%cfebe%pfx%unit(iu),cplot%box(ibox)%chunks,error)
            if (error) return
          endif
          if (inoema%cfebe%pfx%unit(iu)%pol_code.ne.selpol.and. &
              inoema%cfebe%i_f%selunit%polmode.ne.'B') cycle
          imode=inoema%cfebe%pfx%unit(iu)%imode
          xt=-0.5
          if (inoema%cfebe%pfx%unit(iu)%pol_code.eq.1) then !H polar
            yt=0
            write (boxlabel,'(a,1x,f0.3,1x,f0.3,1x,a,a,a)') 'DRAW TEXT', &
                  xt,yt,'"',trim(inoema%cfebe%pfx%unit(iu)%label),'" 4 90 /CHARACTER 7'
          else if (inoema%cfebe%pfx%unit(iu)%pol_code.eq.2) then !V polar
            yt=0
            write (boxlabel,'(a,1x,f0.3,1x,f0.3,1x,a,a,a)') 'DRAW TEXT', &
                  xt,yt,'"',trim(inoema%cfebe%pfx%unit(iu)%label),'" 6 90 /CHARACTER 1'
          else
            call astro_message(seve%e,rname,'Problem with polars')
            error = .true.
            return
          endif
          call gr_exec1(boxlabel)
          if (imode.eq.-1) cycle
          if (imode.ne.prevmode) then
            n_modebox=n_modebox+1
            prevmode=imode
            samemode=.false.
          else
            samemode=.true.
          endif
          if (n_modebox.eq.1) then
            prevxt=0
          endif
          ! draw spw coverage
          call noema_draw_spw(inoema%cfebe%spw%out,inoema%cfebe%pfx%unit(iu), &
                              cplot%box(ibox)%rest,error)
          if (error) return
          
          yt=-2*cplot%desc%defchar
          do ity=1,inoema%cfebe%pfx%unit(iu)%mode(imode)%n_types
            xt=prevxt
            ipol=inoema%cfebe%pfx%unit(iu)%pol_code
            call gr_exec1(smallchar)
            ! id chunks
            call noema_draw_chunks(inoema%cfebe%rectune%flo1,inoema%recdesc%flo2,inoema%source%dopshift, &
                 inoema%cfebe%pfx%unit(iu)%mode(imode)%chtype(ity),ib,sb_code(is),ipol,  &
                 cplot%box(ibox)%rest,do_idchunk,error)
            if (error) return
            !
            !caption
            if (samemode) cycle ! write only 1 if 2 polar are the same
            write (mess,'(a,1x,f0.1,1x,a)') 'df =',  &
                    inoema%cfebe%pfx%unit(iu)%mode(imode)%chtype(ity)%df_chunks*1d3,'kHz'
            call gr_pen(colour=adefcol,error=error)
            if (error) return
            write (comm,'(a,1x,f0.3,1x,f0.3,1x,a,a,a)') 'DRAW TEXT', &
                  xt,yt,'"',trim(mess),'" 4 0 /BOX 3'
            call gr_exec1(comm)
            call gr_pen(colour=adefcol,error=error)
            if (error) return
            ! draw the marker
            lmess=len(trim(mess))
            call gtg_charlen(lmess,trim(mess),cplot%desc%smallchar,smess,0)
            xt=xt-smess-cplot%desc%smallchar
            mark%col=inoema%cfebe%pfx%unit(iu)%mode(imode)%chtype(ity)%itypecol(icol_fill)
            mark%nside=4
            mark%style=3
            mark%s=cplot%desc%smallchar
            mark%x=xt
            mark%y=-2*cplot%desc%defchar
            mark%ref='BOX'
            mark%iref=3
            call rec_draw_mark(mark,error)
            if (error) return
            mark%col=adefcol
            mark%style=0
            call rec_draw_mark(mark,error)
            if (error) return
            prevxt=xt-cplot%desc%smallchar
          enddo ! pfx types
          call gr_exec1(smallchar)
          call noema_id_spw(inoema%cfebe%spw%out,inoema%cfebe%pfx%unit(iu),cplot%box(ibox)%rest,error)
          if (error) return
          call gr_exec1(defchar)
        enddo !correl units
        ! show tuning line
        if (cplot%box(ibox)%rest%xmin.le.inoema%cfebe%rectune%frest.and. &
            cplot%box(ibox)%rest%xmax.ge.inoema%cfebe%rectune%frest) then
          call rec_draw_linetune(inoema%cfebe%rectune,cplot%box(ibox)%rest,error)
          if (error) return
        endif
        !draw confusion zone
        call noema_draw_confusion(rname,inoema%recdesc,inoema%cfebe%rectune%flo1,inoema%source%dopshift, &
                                  ib,sb_code(is),cplot%box(ibox)%rest,cplot%desc,error)
        if (error) return
        !plot  molecules
        call gr_exec1(molchar)
        call rec_draw_molecules(cata,cplot%box(ibox)%rest,error)
        if (error) return
        call gr_exec1(defchar)
        ! Draw the axis
        call rec_draw_fbox(cplot,ibox,drawaxis,error)
        if (error) return
        ! ID the polar
        xt=cplot%desc%defchar
        yt=-cplot%box(ibox)%phys%sy/2d0+cplot%desc%smallchar
        write (comm,'(a,1x,f0.3,1x,f0.3,1x,a)') 'DRAW TEXT',xt,yt,'"H" 5 0 /BOX 9'
        call gr_exec1(comm)
        yt=-cplot%box(ibox)%phys%sy+cplot%desc%smallchar
        write (comm,'(a,1x,f0.3,1x,f0.3,1x,a)') 'DRAW TEXT',xt,yt,'"V" 5 0 /BOX 9'
        call gr_exec1(comm)
        call gr_execl('CHANGE DIRECTORY')
      enddo !selected basebands (BASEBAND command)
    enddo !basebands
  enddo !sidebands
  !
  ! Now draw info on the plot
  ! Prepare the title
  call rec_make_title(inoema%cfebe%rectune,mess,error)
  if (error) return
  messtitle=''
  write (messtitle,'(a,1x,a,1x,a)') &
      trim(messtitle),trim(inoema%recdesc%bandname(inoema%cfebe%rectune%iband)),trim(mess)
  ! Write title
  call rec_draw_title(cplot,messtitle,error)
  if (error) return
  ! Then deal with warnings
  outlo=inoema%cfebe%rectune%outlo
  call noema_draw_warning(rname,cplot,outlo,chunk1,overload,conflict,error)
  if (error) return
  call gr_execl('CHANGE DIRECTORY')
  call gr_pen(colour=adefcol,idash=1,iweight=1,error=error)
  if (error) return
  !
end subroutine noema_plot_selpfx
!
subroutine rec_def_fbox_chunks(if2min,if2max,pfxu,ubox,error)
  use gbl_message
  use astro_interfaces, except_this=>rec_def_fbox_chunks
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Prepare the box for plotting chunks numbers [input is IF2]
  !-----------------------------------------------------------------------
  real(kind=8), intent(in)                      :: if2min,if2max ! IF2 frequency limits
  type(frequency_box_user_t), intent(inout)     :: ubox ! box parameters user
  type(pfx_unit_t), intent(in)                  :: pfxu ! correlator parameters
  logical, intent(inout)                        :: error
  !local
  character(len=*), parameter   :: rname='PLOT'
  real(kind=8) :: acoeff, bcoeff
  integer(kind=4) :: imode,itype
  character(len=128) :: mess
  !
  if (pfxu%imode.eq.-1) then
    write (mess,'(a,1x,a,1x,a)') 'Unit',trim(pfxu%label),'has no mode defined.'
    call astro_message(seve%w,rname,mess)
    return
  endif
  imode=pfxu%imode
  itype=1
  !
  acoeff=(pfxu%mode(imode)%chtype(itype)%n_chunks-0.5d0)/max(if2max,if2min)
  bcoeff=1d0
  !
  ubox%xmin=if2min*acoeff+bcoeff
  ubox%xmax=if2max*acoeff+bcoeff
  ubox%name='Chunks'
  ubox%unit='#'
  ubox%defined=.true.
  !
end subroutine rec_def_fbox_chunks
!
subroutine noema_draw_spw(spwout,pfxunit,fbox,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_draw_spw
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Displays the coverage of the spw in a given correlator unit
  !
  !-----------------------------------------------------------------------
  type(spw_output_t), intent(in) :: spwout
  type(pfx_unit_t), intent(in) :: pfxunit
  type(frequency_box_user_t)   :: fbox
  logical, intent(inout) :: error
  ! Local
  character(len=200), parameter :: rname = 'PLOT'
  integer(kind=4) :: iw,imode,it
  real(kind=8) :: pfxres
  type(draw_rect_t) :: sprec
  logical :: foundcol
  character(len=128) :: mess
  !
  if (pfxunit%imode.eq.-1) then
    write (mess,'(a,1x,a,1x,a)') 'Unit',trim(pfxunit%label),'has no mode defined. Use command BASEBAND to select one'
    call astro_message(seve%w,rname,mess)
    return
  endif
  !
  do iw=1,spwout%n_spw
    if (spwout%win(iw)%label.ne.pfxunit%label) cycle
    imode=pfxunit%imode
    sprec%xmin=spwout%win(iw)%restmin
    sprec%xmax=spwout%win(iw)%restmax
    foundcol = .false.
    do it=1,pfxunit%mode(imode)%n_types
      pfxres=pfxunit%mode(imode)%chtype(it)%df_chunks*khzpermhz
      if (spwout%win(iw)%resol.eq.pfxres) then
        sprec%col=pfxunit%mode(imode)%chtype(it)%itypecol(icol_fill)
        foundcol = .true.
      endif
    enddo
    if (.not.foundcol) then
      error = .true.
      call astro_message(seve%e,rname,'SPW resolution does not match PFX one')
      return
    endif
    if (spwout%win(iw)%pol_code.eq.1) then
      sprec%ymax=fbox%ymax
      sprec%ymin=(fbox%ymax+fbox%ymin)/2d0
    else if (spwout%win(iw)%pol_code.eq.2) then
      sprec%ymax=(fbox%ymax+fbox%ymin)/2d0
      sprec%ymin=fbox%ymin
    else
      call astro_message(seve%e,rname,'Problem with polarizations')
      error=.true.
      return
    endif
    call rec_draw_frect(sprec,fbox,error)
    if (error) return
  enddo
  !
end subroutine noema_draw_spw
!
subroutine noema_id_spw(spwout,pfxunit,fbox,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_id_spw
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Displays the coverage of the spw in a given correlator unit
  !
  !-----------------------------------------------------------------------
  type(spw_output_t), intent(in) :: spwout
  type(pfx_unit_t), intent(in) :: pfxunit
  type(frequency_box_user_t)   :: fbox
  logical, intent(inout) :: error
  ! Local
  character(len=200), parameter :: rname = 'PLOT'
  integer(kind=4) :: iw
  real(kind=4) :: yt
  type(draw_rect_t) :: sprec
  character(len=256) :: comm
  !
  do iw=1,spwout%n_spw
    if (spwout%win(iw)%label.ne.pfxunit%label) cycle
    if (spwout%win(iw)%itype.eq.1) cycle
    sprec%xmin=spwout%win(iw)%restmin
    sprec%xmax=spwout%win(iw)%restmax
    sprec%col=pfxunit%mode(pfxunit%imode)%chtype(spwout%win(iw)%itype)%itypecol(icol_border)
    if (spwout%win(iw)%pol_code.eq.1) then
      sprec%ymax=fbox%ymax
      sprec%ymin=(fbox%ymax+fbox%ymin)/2d0
    else if (spwout%win(iw)%pol_code.eq.2) then
      sprec%ymax=(fbox%ymax+fbox%ymin)/2d0
      sprec%ymin=fbox%ymin
    else
      call astro_message(seve%e,rname,'Problem with polarizations')
      error=.true.
      return
    endif
    call rec_draw_rect(sprec,fbox,error)
    if (error) return
    call gr_pen(colour=sprec%col,error=error)
    if (error) return
    yt=sprec%ymin !+(sprec%ymax-sprec%ymin)/4d0
    write (comm,'(a,1x,f0.3,1x,f0.3,1x,a,i0,a)') 'DRAW TEXT',sprec%xmin,yt,'" L',iw,'" 3 90 /USER'
    call gr_exec1(comm)
    call gr_pen(colour=adefcol,error=error)
    if (error) return
  enddo
  !
end subroutine noema_id_spw
!
subroutine noema_draw_chunks(flo1,flo2,sdop,pfxtype,ib,is,ip,fbox,id,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_draw_chunks
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Displays the chunks limits and highligh conflicting chunks
  !
  !-----------------------------------------------------------------------
  real(kind=8), intent(in)      :: flo1
  real(kind=8), intent(in)      :: flo2
  real(kind=8), intent(in)      :: sdop
  type(pfx_type_t), intent(in) :: pfxtype
  integer(kind=4), intent(in)   :: ib ! baseband
  integer(kind=4), intent(in)   :: is ! sideband
  integer(kind=4), intent(in)   :: ip ! polar
  type(frequency_box_user_t)   :: fbox
  logical, intent(in)     :: id
  logical, intent(inout) :: error
  ! Local
  character(len=200), parameter :: rname = 'PLOT'
  real(kind=8)  :: fif2,fif1,frf,if2ch,xid,yid
  type(draw_rect_t)     :: backrect
  integer(kind=4)       :: ich
  character(len=200) :: comm,usage
  !
  do ich=1,pfxtype%n_chunks
    !skip undefined chunks
    if (pfxtype%chunks(ich).eq.0) cycle
    if2ch=pfxtype%if2ch0+(ich-1)*pfxtype%width_chunk
    fif2 = if2ch-pfxtype%width_chunk/2d0
    if (fif2.lt.0) then 
      fif2 = 0
    endif
    call if2toif1(flo2,fif2,ib,fif1,error)
    if (error) return
    call if1torf(flo1,fif1,is,frf,error)
    if (error) return
    call rftorest(sdop,frf,backrect%xmin,error)
    if (error) return
    fif2 = if2ch+pfxtype%width_chunk/2d0
    call if2toif1(flo2,fif2,ib,fif1,error)
    if (error) return
    call if1torf(flo1,fif1,is,frf,error)
    if (error) return
    call rftorest(sdop,frf,backrect%xmax,error)
    if (error) return
    if (ip.eq.1) then
      backrect%ymin = fbox%ymin+(fbox%ymax-fbox%ymin)/2
      backrect%ymax = fbox%ymax
    else if (ip.eq.2) then
      backrect%ymin = fbox%ymin
      backrect%ymax = fbox%ymin+(fbox%ymax-fbox%ymin)/2
    else
      call astro_message(seve%e,rname,'Problem with polars')
    endif
    ! Put color on conflicting chunks
    if (pfxtype%usage.gt.pfxtype%use_chunks.or.pfxtype%chunks(ich).gt.1) then
      backrect%col = aconflictcol
      call rec_draw_frect(backrect,fbox,error)
      if (error) return
    endif
    if (ich.eq.1.and.pfxtype%move_chunk) then
      backrect%col = aconflictcol
      call rec_draw_frect(backrect,fbox,error)
      if (error) return
    endif
    ! ID chunks
    backrect%col = achunkcol
    backrect%dash = 3
    call rec_draw_rect(backrect,fbox,error)
    if (error) return
    if (id) then
      if (nint(ich/5.)*5.ne.ich) cycle
      if (ich.eq.1) then  ! 1st chunk is only 32MHz wide
        if2ch=pfxtype%if2ch0+(pfxtype%width_chunk/4d0)
      endif
      call if2toif1(flo2,if2ch,ib,fif1,error)
      if (error) return
      call if1torf(flo1,fif1,is,frf,error)
      if (error) return
      call rftorest(sdop,frf,xid,error)
      if (error) return
      yid=fbox%ymin+(fbox%ymax-fbox%ymin)/5
      call gr_exec1('SET CHARACTER 0.8*CHARACTER_SIZE')
      write (comm,'(a,1x,f0.3,1x,f0.3,1x,a,i0,a)') 'DRAW TEXT ',xid,yid,'"',ich,'" 5 0 /USER /CLIP'
      call gr_exec1(comm)
      call gr_exec1('SET CHARACTER CHARACTER_SIZE/0.8')
    endif
  enddo
  if (.not.(pfxtype%move_chunk)) return
  write (comm,'(a)') 'DRAW TEXT -3 0 "Used Chunks" 5 90 /CHARACTER 4'
  call gr_exec1(comm)
  if (pfxtype%usage.gt.pfxtype%use_chunks) then
    call gr_pen(colour=aconflictcol,idash=1,error=error)
    if (error) return
  else
    call gr_pen(colour=adefcol,idash=1,error=error)
    if (error) return
  endif
  write (usage,'(i0,a,i0)')  pfxtype%usage,'/',pfxtype%use_chunks
  if (ip.eq.0) then
    write (comm,'(a,a,a)') 'DRAW TEXT -2 0 "',trim(usage),'" 5 90 /CHARACTER 4'
  else if (ip.eq.1) then
    write (comm,'(a,a,a)') 'DRAW TEXT -2 -0.5 "',trim(usage),'" 4 90 /CHARACTER 7'
  else if (ip.eq.2) then
    write (comm,'(a,a,a)') 'DRAW TEXT -2 0.5 "',trim(usage),'" 6 90 /CHARACTER 1'
  endif
  call gr_exec1(comm)
  call gr_pen(colour=adefcol,idash=1,error=error)
  if (error) return
  !
end subroutine noema_draw_chunks
!
subroutine noema_pfx_status(pfx,overload,conflict,chunk1,allisoff,error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>noema_pfx_status
  use astro_types
  !-----------------------------------------------------------------------
  ! @ private
  ! Check feasability of the setup
  !
  !-----------------------------------------------------------------------
  type(pfx_t), intent(in) :: pfx
  integer(kind=4), intent(inout)     :: overload
  integer(kind=4), intent(inout)     :: conflict
  logical, intent(inout) :: chunk1
  logical, intent(inout) :: allisoff
  logical, intent(inout) :: error
  ! Local
  character(len=200), parameter :: rname = 'SPW'
  integer(kind=4)   :: ic,iu,it,imode,ndef
  !
  overload=0
  conflict=0
  ndef=0
  do iu=1,pfx%n_units
    imode=pfx%unit(iu)%imode
    if (imode.le.0) cycle ! does not consider basebands that are not configured
    ndef=ndef+1
    do it=1,pfx%unit(iu)%mode(imode)%n_types
      if (.not.(pfx%unit(iu)%mode(imode)%chtype(it)%move_chunk)) cycle
      ! Check chunk1
      if (pfx%unit(iu)%mode(imode)%chtype(it)%chunks(1).eq.1) then
        chunk1=.true.
      endif
      ! Check overload
      if (pfx%unit(iu)%mode(imode)%chtype(it)%usage.gt.pfx%unit(iu)%mode(imode)%chtype(it)%use_chunks) then
        overload=overload+1
      endif
      ! Check chunk conflict
      do ic=1,pfx%unit(iu)%mode(imode)%chtype(it)%n_chunks
        if (pfx%unit(iu)%mode(imode)%chtype(it)%chunks(ic).le.1) cycle
        conflict=conflict+1
      enddo
    enddo
  enddo
  if (ndef.eq.0) then
    allisoff=.true.
  else
    allisoff=.false.
  endif
  !
end subroutine noema_pfx_status
