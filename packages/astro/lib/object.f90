subroutine object(name,coord,equinox,lambda,beta,invtype,invelo,inredshift, &
                  draw,drawargs,drawclip,s_3,print,icat,error)
  use gildas_def
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>object
  use ast_astro
  use ast_constant
  !---------------------------------------------------------------------
  ! @ private
  ! ASTRO	Reduce coordinates of a fixed object
  ! Arguments:
  !	NAME	C*(*)	Object name			Input
  !	COORD	C*2	Coordinate system		Input
  !	EQUINOX	R*4	Equinox of the coordinates	Input
  !	LAMBDA	R*8	Longitude like coordinate	Input
  ! 	DRAW	L	Plot a symbol?			Input
  !	S_3	R*8(3)	RA, Dec, Dsun at current epoch	Output
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: name         ! Object name
  character(len=2), intent(in)    :: coord        ! Coordinate system
  real(kind=4),     intent(in)    :: equinox      ! Equinox of system
  real(kind=8),     intent(in)    :: lambda       ! Longitude like coordinate
  real(kind=8),     intent(in)    :: beta         ! Latitude like coordinate
  character(len=*), intent(in)    :: invtype      ! kind of velocity/redshift
  real(kind=8), intent(in)        :: invelo       !input value for velocity
  real(kind=8), intent(in)        :: inredshift   ! input redshift
  logical,          intent(in)    :: draw         ! Plot?
  character(len=*), intent(in)    :: drawargs(:)  ! Which kind of plot(s)?
  logical,          intent(in)    :: drawclip     ! how to clip the source names
  real(kind=8),     intent(out)   :: s_3(3)       ! RA, Dec & Dsun at current epoch
  logical,          intent(in)    :: print        ! Printout flag
  integer(kind=4),  intent(in)    :: icat         !
  logical,          intent(inout) :: error        !
  ! Local
  character(len=*), parameter :: rname='OBJECT'
  character(len=message_length) :: mess
  real(kind=8) :: parang, s_2(2), s_5(2), x_5(3), trfm_50(9), sl
  real(kind=8) :: limit(2), jlim(2), n, n0, n1, g, lmean, long, x_0(3), svec(3)
  real(kind=8) :: vlsr, dop, vshift, lsr
  character(len=80), save :: sun_avoid
  integer(kind=4) :: i
  character(len=12) :: clim(3)
  character(len=sourcename_length) :: sname
  real(kind=8), parameter :: degrad=pi/180.d0
  character(len=16) :: varname
  logical, save :: avoided=.false.
  !
  !------------------------------------------------------------------------
  ! Code :
  !
  if (.not.avoided) then
    call sic_def_char('SUN_AVOID',sun_avoid,.true.,error)
    error = .false.
    avoided = .true.
    sun_avoid = 'not computed'
  endif
  !
  ! Do the actual work
  !
  call do_object(coord, equinox, lambda, beta, s_2, s_3, dop, lsr, &
    & svec, x_0, parang, error)
  !
  ! Check Sun distance
  !
  if (coord .ne. 'HO') then
    call transp (trfm_05, trfm_50)
    call matvec (x_0, trfm_50, x_5)    ! Get current ecliptic coords.
    call spher (x_5, s_5)
    sl = slimit*pi/180.d0
    write(clim(3),'(F6.1)') s_3(3)
    if (abs(s_5(2)).lt.sl) then
      limit(1) = s_5(1)+acos(cos(sl)/cos(s_5(2)))
      limit(2) = s_5(1)-acos(cos(sl)/cos(s_5(2)))
      do i=1,2
        long  = (mod(limit(i)+2d0*pi,2d0*pi))*180.d0/pi
        n1 = (long-280.460d0)/0.9856474d0
        if (i.eq.1) then
          n0 = jnow_utc-j2000
        else
          n0 = n-365.25d0
        endif
        n = n0+mod(n1-n0+36525.d0,365.25d0)
        do while (abs(n-n1).gt.0.05d0)
          n1 = n
          g = 357.528d0 + 0.9856003d0*n
          lmean = long - 1.915d0*sin(pi*(g)/180.0d0)-0.020d0*   &
                  sin(pi*(2d0*g)/180.0d0)
          n = (lmean-280.460d0)/0.9856474d0
          n = n0+mod(n-n0+36525.d0,365.25d0)
        enddo
        n = n - 2d0*(i-1.5d0)  ! 1 day safety margin ...
        jlim(i) = j2000+nint(n)
        call jdate_to_datetime(jlim(i),clim(i),error)
        if (error)  return
      enddo
      sname = name
      if (len_trim(sname).le.12) then
        write (mess,'(a12,1x,a,1x,a,1x,a,1x,a,1x,a,1x,a)') sname, &
                  'Sun distance',trim(clim(3)),'Avoidance',trim(clim(2)),'to',trim(clim(1))
      else
        write (mess,'(a,1x,a,1x,a,1x,a,1x,a,1x,a,1x,a)') trim(sname), &
                  'Sun distance',trim(clim(3)),'Avoidance',trim(clim(2)),'to',trim(clim(1))
      endif
!       call astro_message(seve%r,rname,sname//  &
!         '    Sun distance '//trim(clim(3))//  &
!         '   Avoidance '//trim(clim(2))//' to '//trim(clim(1)))
      call astro_message(seve%r,rname,mess)
      sun_avoid = trim(clim(2))//' to ' //trim(clim(1))
    elseif (sl.ne.0.0) then
      sname = name
      if (len_trim(sname).le.12) then
        write (mess,'(a12,1x,a,1x,a,1x,a)') sname,'Sun distance',trim(clim(3)),'No Avoidance'
      else
        write (mess,'(a,1x,a,1x,a,1x,a)') trim(sname),'Sun distance',trim(clim(3)),'No Avoidance'
      endif
      call astro_message(seve%r,rname,mess)
!       call astro_message(seve%r,rname,sname//  &
!         '    Sun distance '//trim(clim(3))//'   No Avoidance')
      sun_avoid = 'NONE'
    endif
    !
    ! Doppler correction: compute the equivalent VLSR velocity
    ! for display purpose
    !
    if (invtype.eq.'LS'.or.invtype.eq.'RE') then      ! velocity is S/LSR
      vshift = dop+lsr+invelo
      vlsr = invelo
    elseif (invtype.eq.'HE') then  ! velocity is S/G
      vshift = dop+invelo
      vlsr = invelo-lsr
    elseif (invtype.eq.'EA') then  ! velocity is S/OBS
      vshift = invelo
      vlsr = invelo-lsr-dop
    else
      vshift = 0.0d0
      vlsr = -lsr-dop
    endif
  else
    vshift = 0.0d0
  endif
  fshift = (1.0d0 - vshift/light)
  !
  ! Write output message
  !
  if (azref.ne.'S') then
    s_2(1) = s_2(1)+180.d0
  endif
  if (print) then
    if (coord .eq. 'HO') then
      write(mess,1000) name(1:lenc(name)), s_2, dop, lsr
      call astro_message(seve%r,rname,mess)
    else
      sname=name
      if (len_trim(name).le.12) then
        write(mess,1011) sname, s_2
        call astro_message(seve%r,rname,mess)
        write(mess,1012) sname, vshift, vlsr, lsr, dop
        call astro_message(seve%r,rname,mess)
        write(mess,1013) sname, inredshift
        call astro_message(seve%r,rname,mess)      
      else
        write(mess,1001) trim(sname), s_2
        call astro_message(seve%r,rname,mess)
        write(mess,1002) trim(sname), vshift, vlsr, lsr, dop
        call astro_message(seve%r,rname,mess)
        write(mess,1003) trim(sname), inredshift
        call astro_message(seve%r,rname,mess)
      endif
    endif
  endif
  !
  ! Update global SIC variables
  !
  azimuth = s_2(1)*pi/180d0
  elevation = s_2(2)*pi/180d0
  ra = s_3(1)                  ! in radians
  if (ra.lt.0) ra = ra + 2d0*pi
  dec = s_3(2)                 ! in radians
!   source_name = name(1:lenc(name)) ! useless now (to be confirmed)
  !
  ! SOURCE /DRAW
  if (draw) then
    call astro_draw(name,icat,drawargs,drawclip,error)
    if (error)  return
  endif
  parallactic_angle = parang
  !
  ! Store output parameters in the SIC structure astro%sourc
  varname = 'ASTRO'
  if (.not.sic_varexist(varname)) then
      call sic_defstructure(varname,.true.,error)
      if (error) return
  endif
  varname = 'ASTRO%SOURCE'
  call fill_astro_source(varname,name,ra,dec,s_2,vshift,vlsr,lsr,dop,inredshift, &
                         coord,equinox,lambda,beta,invtype,invelo,error)
  if (error) return
  ! End
  !
  return
  1000 format(a,16x,'Az ',f10.5,' El ',f9.5, ' Dop ',f7.3,' Lsr ',f7.3)
  1001 format(a,1x,'Azimuth ',f10.5,4x,'Elevation ',f9.5)
  1011 format(a12,1x,'Azimuth ',f10.5,4x,'Elevation ',f9.5)
  1002 format(a,1x,'V(S/OBS) = ',f9.3, ' [S/LSR=',  &
              f7.3,',LSR/G=',f7.3,',G/OBS=',f7.3,']')
  1012 format(a12,1x,'V(S/OBS) = ',f9.3, ' [S/LSR=',  &
              f7.3,',LSR/G=',f7.3,',G/OBS=',f7.3,']')
  1003 format(a,1x,'Redshift ',f10.3)
  1013 format(a12,1x,'Redshift ',f10.3)
  !
end subroutine object
!
subroutine do_object(coord,equinox,lambda,beta,s_2,s_3,dop,lsr,svec,x_0,  &
  parang,error)
  use gbl_message
  use ast_astro
  use ast_constant
  !---------------------------------------------------------------------
  ! @ public
  !  Reduce coordinates of a fixed object
  !---------------------------------------------------------------------
  character(len=2), intent(in)    :: coord    ! Coordinate system
  real(kind=4),     intent(in)    :: equinox  ! Equinox (ignored if not EQ)
  real(kind=8),     intent(in)    :: lambda   ! Longitude like coordinate
  real(kind=8),     intent(in)    :: beta     !
  real(kind=8),     intent(out)   :: s_2(2)   !
  real(kind=8),     intent(out)   :: s_3(3)   ! ra, dec & distance of Sun at current epoch
  real(kind=8),     intent(out)   :: dop      !
  real(kind=8),     intent(out)   :: lsr      !
  real(kind=8),     intent(out)   :: svec(3)  !
  real(kind=8),     intent(out)   :: x_0(3)   !
  real(kind=8),     intent(out)   :: parang   ! Parallactic angle
  logical,          intent(inout) :: error    !
  ! Global
  real(kind=8) :: sun_distance
  ! Local
  real(kind=8) :: x_2(3), v_2(3), x_3(3), x_1(3)
  real(kind=8) :: degrad, psi, the, phi, jfix, epsm, mat1(9), mat2(9), mat3(9)
  real(kind=8) :: angles(6), trfm_03(9), trfm_01(9), oblimo, stv, y_2(3), n_3(3),sp_2,vp_2(3)
  real(kind=8) :: n_2(3), den
  integer(kind=4) :: i
  parameter (degrad = pi /180.d0, stv = 2d0* pi / 86400d0*366.25d0 / 365.25d0)
  real(kind=8) :: apx1, apx2, apx3
  parameter (apx1 = 0.289977970217382d0, apx2 = -11.9099497383444d0,  &
             apx3 = 16.0645264529100d0)
  !
  !      SAVE
  !------------------------------------------------------------------------
  ! Code :
  if (coord.eq.'HO') then           ! horizontal coordinates
    s_2(1) = lambda * 180.d0 /pi
    s_2(2) = beta * 180.d0 /pi
    call rect(s_2,x_2)              ! To prepare the SUN_DISTANCE
    s_3(3) = sun_distance(x_2)      ! This is the sun distance
    return                          ! Nothing else there
    !
    !  Case : present day "true" coordinates, no transformation matrix, (R1) = (R3)
    !  considered as fixed coordinates in fixed frame, but formally maps
    !  are meaningless
  elseif (coord.eq.'DA') then
    s_3(1) = lambda
    s_3(2) = beta
    call rect (s_3, x_3)
    call matvec (x_3, trfm_23, x_2) ! needed to compute ...
    call transp (trfm_30, trfm_03)  ! The LSR correction.
    call matvec (x_3, trfm_03, x_0) ! needed for Horizon command
    !
    !  Case : galactic II coordinates, transformation (R1) to (R0)
  elseif (coord.eq.'GA') then       ! galactic coordinates
    s_1(1) = lambda
    s_1(2) = beta
    call rect (s_1, x_1)
    !  transformation matrix from galactic II to ecliptic (J2000.0)
    !  ### Euler angles could be computed once for all ...
    !  Computation in 3 steps
    psi = 33.d0 * degrad            ! well known Euler angles
    the = -62.6d0 * degrad          ! for transformation
    phi = -282.25d0 * degrad        ! matrix from
    call eulmat (psi,the,phi,mat1)  ! gal. (II) to equ. (1950.0)
    jfix=j2000+(1950.d0-2000.d0)*365.25d0  ! fixed equinox (julian days)
    epsm=oblimo (jfix)              ! mean obliquity at fixed equinox
    psi=0.d0                        ! Euler angles of transformation
    the=epsm
    phi=0.d0
    call eulmat (psi,the,phi,mat2)  ! equ. (1950.0) to ecl. (1950.0)
    call mulmat (mat1,mat2,mat3)    ! product MAT3 = MAT2 x MAT1
    !                               ! gal. (II) to ecl. (1950.0)
    call qprec (jfix,j2000,angles)  ! compute precession angles
    psi=angles(5)                   ! Euler angles
    the=angles(4)                   ! of transformation
    phi=-angles(6)-angles(5)        ! matrix from
    call eulmat (psi,the,phi,mat1)  ! ecl. (1950.0) to ecl. (2000.0)
    call mulmat (mat3,mat1,trfm_01) ! product TRFM01 = MAT1 x MAT3
    !                               ! gal. (II) to ecl. (1950.0)
    call matvec (x_1, trfm_01, x_0)
    call matvec (x_0, trfm_20, x_2)
    call matvec (x_0, trfm_30, x_3) ! needed for Horizon command
    call spher (x_3, s_3)
    !
    ! Case: mean equatorial coordinates at fixed equinox, transformation (R1) to (R0)
  elseif (coord.eq.'EQ') then
    s_1(1) = lambda
    s_1(2) = beta
    call rect (s_1, x_1)
    !  Transformation matrix from equatorial (JFIX) to ecliptic (J2000.0)
    jfix=j2000+(equinox-2000.0d0)*365.25d0   ! fixed equinox (julian days)
    epsm=oblimo (jfix)              ! mean obliquity at fixed equinox
    psi=0.d0                        ! Euler angles of transformation
    the=epsm
    phi=0.d0
    call eulmat (psi,the,phi,mat1)  ! compute matrix from (R1) to ecl (JFIX)
    call qprec (jfix,j2000,angles)  ! compute precession angles
    psi=angles(5)                   ! Euler angles of transformation
    the=angles(4)
    phi=-angles(6)-angles(5)
    call eulmat (psi,the,phi,mat2)  ! compute matrix from ecl (JFIX) to (R0)
    call mulmat (mat1,mat2,trfm_01) ! product TRFM01 = MAT2 x MAT1
    call matvec (x_1, trfm_01, x_0)
    call matvec (x_0, trfm_20, x_2)
    call matvec (x_0, trfm_30, x_3) ! needed for Horizon command
    call spher (x_3, s_3)
  else
    call astro_message(seve%e,'OBJECT','Unsupported coordinates')
    return
  endif
  call matvec (vg_0, trfm_20, v_2)
  !
  ! Check Sun Distance
  s_3(3) = sun_distance(x_2)        ! This is the sun distance
  !
  ! Doppler correction
  v_2(2) = v_2(2) - cos(pi*(lonlat(2))/180.0d0) * stv * (radius + altitude)
  dop = (v_2(1) * x_2(1) + v_2(2) * x_2(2) + v_2(3) * x_2(3))
  lsr = - (apx1 * x_0(1) + apx2 * x_0(2) + apx3 * x_0(3))
  !
  ! Aberration
  do i = 1, 3
    x_2(i) = x_2(i) - v_2(i) / light
  enddo
  call spher(x_2, s_2)
  s_2(1) = - s_2(1) * 180.d0/pi
  s_2(2) = s_2(2) * 180.d0/pi
  !
  ! XYZ
  call matvec (x_3, trfm_43, svec)
  !
  ! Compute parallactic angle
  ! parallactic angle
  ! x_3 is the source vector in eq coordinates (3)
  ! n_3 is the normal to the source meridian plane
  ! TRFM_23 goes from (3) to (2)
  ! n_2 is the normal to the source meridian plane, in horiz coords.(2)
  ! x_2 is the source vector in in horiz coords.
  ! y_2 is the intersection of the vertical plane of the source and
  ! the plane of the sky
  ! vp_2 is the vector product of y_2 and n_2
  ! sp_2 is the scalar product of vp_2 and x_2
  den = sqrt(x_3(1)**2+x_3(2)**2)
  n_3(1) = -x_3(2)/den
  n_3(2) = x_3(1)/den
  n_3(3) = 0
  call matvec (n_3, trfm_23, n_2)
  den = sqrt(x_2(1)**2+x_2(2)**2)
  y_2(1) = x_2(1)*x_2(3) / den
  y_2(2) = x_2(2)*x_2(3) / den
  y_2(3) = -den
  ! vector produc y_2xn_2
  vp_2(1) = y_2(2)*n_2(3)-y_2(3)*n_2(2)
  vp_2(2) = y_2(3)*n_2(1)-y_2(1)*n_2(3)
  vp_2(3) = y_2(1)*n_2(2)-y_2(2)*n_2(1)
  ! scalar product
  sp_2 = vp_2(1)*x_2(1)+vp_2(2)*x_2(2)+vp_2(3)*x_2(3)
  !
  parang = -pi/2+sign(acos(y_2(1)*n_2(1)+y_2(2)*n_2(2)+y_2(3)*n_2(3)),sp_2)
  return
  !
end subroutine do_object
!
function sun_distance (x_2)
  use ast_constant
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  !	Returns angular distance from SUn
  !	X_2: apparent horizontal coordinates of the source
  !	Xsun_2: apparent horizontal coordinates of the sun
  ! The angle between the 2 directions is derived from the scalar product 
  ! of the vectors in the horizontal coordinate frame:
  ! Vobj.Vsun=(XOXSun+YOYSun+ZOZSun)=||Vobj||*||Vsun||*cos(SunObserverTarget)
  !---------------------------------------------------------------------
  real(kind=8) :: sun_distance      !
  real(kind=8) :: x_2(3)            !
  sun_distance = 180.d0/pi*acos( (x_2(1)*xsun_2(1)+x_2(2)*xsun_2(2)+  &
    x_2(3)*xsun_2(3)) /sqrt(x_2(1)**2+x_2(2)**2+x_2(3)**2) /          &
    sqrt(xsun_2(1)**2+xsun_2(2)**2+xsun_2(3)**2) )
end function sun_distance
!
subroutine fill_astro_source(varname,name,ora,odec,s_2,vshift,vlsr,lsr,dop,redsh, &
                             coord,equinox,lambda,beta,invtype,invelo,error)
  use gildas_def
  use gkernel_interfaces
  use astro_interfaces, except_this=>fill_astro_source
  use ast_astro
 !---------------------------------------------------------------------
  ! @ private
  ! fill astro%source% structure
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: varname
  character(len=*),intent(in)   :: name
  real(kind=8), intent(in)      :: ora,odec
  real(kind=8), intent(in)      :: s_2(2)
  real(kind=8), intent(in)      :: vshift,vlsr,lsr,dop,invelo
  real(kind=8), intent(in)      :: redsh
  character(len=*), intent(in)  :: coord,invtype
  real(kind=4), intent(in)  :: equinox
  real(kind=8),     intent(in)    :: lambda,beta   ! Longitude/latitude like coordinate
  logical,        intent(inout) :: error    ! Logical error flag  
  ! Local
  character(len=varname_length)   :: varnamein
  character(len=40) :: d_chain
  integer(kind=4) :: nv
  !
  if (sic_varexist(varname)) then
    call sic_delvariable(varname,.false.,error)
    if (error) return
  endif
  call sic_defstructure(varname,.true.,error)
  if (error) return
  nv = lenc(varname)
  ! alpha, delta, az, ele
  call rad2sexa(ora,24,source_alpha,4,left=.true.)
  call rad2sexa(odec,360,source_delta,3,left=.true.)
  source_az = s_2(1)   ! define local variables (az, ele used
  source_el = s_2(2)   ! in other routines)
  ! Date for which the computation has been done
  call jdate_to_datetime(jnow_utc,d_chain,error)
  ! Fill variable dedicated to the astro%source structure
  source_ra=ra
  source_dec=dec
  source_jutc=jnow_utc
  astro_source_name=name
  source_vshift=vshift
  source_vlsr=vlsr
  source_lsr=lsr
  source_dop=dop
  source_redshift=redsh
  source_date_chain=d_chain
  !
  !
  call sic_def_dble(varname(1:nv)//'%JUTC',source_jutc,0,0,.true.,error)
  call sic_def_char(varname(1:nv)//'%TIME',source_date_chain(1:lenc(source_date_chain)),.true.,error)
  call sic_def_char(varname(1:nv)//'%NAME',astro_source_name(1:lenc(astro_source_name)),.true.,error)
  call sic_def_char(varname(1:nv)//'%C_RA',source_alpha(1:lenc(source_alpha)),.true.,error)
  call sic_def_char(varname(1:nv)//'%C_DEC',source_delta(1:lenc(source_delta)),.true.,error)
  call sic_def_dble(varname(1:nv)//'%RA',source_ra,0,0,.true.,error)
  call sic_def_dble(varname(1:nv)//'%DEC',source_dec,0,0,.true.,error)
  call sic_def_dble(varname(1:nv)//'%AZ',source_az,0,0,.true.,error)
  call sic_def_dble(varname(1:nv)//'%EL',source_el,0,0,.true.,error)
  call sic_def_dble(varname(1:nv)//'%V_SOU_OBS',source_vshift,0,0,.true.,error)
  call sic_def_dble(varname(1:nv)//'%V_SOU_LSR',source_vlsr,0,0,.true.,error)
  call sic_def_dble(varname(1:nv)//'%V_LSR_G',source_lsr,0,0,.true.,error)
  call sic_def_dble(varname(1:nv)//'%V_G_OBS',source_dop,0,0,.true.,error)
  call sic_def_dble(varname(1:nv)//'%REDSHIFT',source_redshift,0,0,.true.,error)
  !
  ! Input section
  source_incoord = coord
  source_ineq = equinox
  source_invtype=invtype
  source_invelocity=invelo
  source_inredshift=redsh
  ! Coord in sexa
  if (coord.eq.'EQ'.or.coord.eq.'DA') then
    call rad2sexa(lambda,24,source_inlambdasexa,3,left=.true.)
  else
    call rad2sexa(lambda,360,source_inlambdasexa,3,left=.true.)
  endif
  call rad2sexa(beta,360,source_inbetasexa,3,left=.true.)
  write (varnamein,'(a,a)') trim(varname),'%IN'
  if (sic_varexist(varnamein)) then
      call sic_delvariable(varnamein,.false.,error)
      if (error) return
  endif
  call sic_defstructure(varnamein,.true.,error)
  if (error) return
  nv = lenc(varnamein)
  call sic_def_char(varnamein(1:nv)//'%COORD',source_incoord(1:lenc(source_incoord)),.true.,error)
  call sic_def_real(varnamein(1:nv)//'%EQUINOX',source_ineq,0,0,.true.,error)
  call sic_def_char(varnamein(1:nv)//'%LAMBDA',source_inlambdasexa(1:lenc(source_inlambdasexa)),.true.,error)
  call sic_def_char(varnamein(1:nv)//'%BETA',source_inbetasexa(1:lenc(source_inbetasexa)),.true.,error)
  call sic_def_char(varnamein(1:nv)//'%VTYPE',source_invtype(1:lenc(source_invtype)),.true.,error)
  call sic_def_dble(varnamein(1:nv)//'%VELOCITY',source_invelocity,0,0,.true.,error)
  call sic_def_dble(varnamein(1:nv)//'%REDSHIFT',source_inredshift,0,0,.true.,error)
  !
end subroutine fill_astro_source
!
subroutine fill_doppler_source(rname,varname,name,vshift,vlsr,redsh,invtype,invelo,error)
  use gildas_def
  use gkernel_interfaces
  use gbl_message
  use astro_interfaces, except_this=>fill_doppler_source
  use ast_astro
  use ast_constant
  !---------------------------------------------------------------------
  ! @ private
  ! fill astro%source% structure in the case of source /doppler option
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: rname
  character(len=*), intent(in)  :: varname
  character(len=*),intent(in)   :: name
  real(kind=8), intent(in)      :: vshift,vlsr
  real(kind=8), intent(in)      :: redsh
  character(len=*),intent(in)   :: invtype
  real(kind=8), intent(in)      :: invelo
  logical,        intent(inout) :: error    ! Logical error flag  
  ! Local
  integer(kind=4) :: nv
  character(len=varname_length)   :: varnamein
  !
  if (vlsr.ne.0.and.redsh.ne.0) then
    call astro_message(seve%e,rname,'Source should not be defined with both LSR and Redshift')
    error=.true.
    return
  endif
  if (sic_varexist(varname)) then
    call sic_delvariable(varname,.false.,error)
    if (error) return
  endif
  call sic_defstructure(varname,.true.,error)
  if (error) return
  nv = lenc(varname)
  ! Fill variable dedicated to the astro%source structure
  source_alpha=astro_nulla
  source_delta=astro_nulla
  source_az = astro_nullr8
  source_el = astro_nullr8
  source_ra=astro_nullr8
  source_dec=astro_nullr8
  astro_source_name=name
  source_vshift=vshift
  source_vlsr=vlsr
  source_redshift=redsh
  source_dop=0d0
  source_lsr=0d0
  !
  call sic_def_char(varname(1:nv)//'%NAME',astro_source_name(1:lenc(astro_source_name)),.true.,error)
  call sic_def_dble(varname(1:nv)//'%V_SOU_OBS',source_vshift,0,0,.true.,error)
  call sic_def_dble(varname(1:nv)//'%V_SOU_LSR',source_vlsr,0,0,.true.,error)
  call sic_def_dble(varname(1:nv)//'%V_LSR_G',source_lsr,0,0,.true.,error)
  call sic_def_dble(varname(1:nv)//'%V_G_OBS',source_dop,0,0,.true.,error)
  call sic_def_dble(varname(1:nv)//'%REDSHIFT',source_redshift,0,0,.true.,error)
  ! Input section
  source_incoord = astro_nulla
  source_ineq = astro_nullr4
  source_invtype=invtype
  source_invelocity=invelo
  source_inredshift=redsh
  ! Coord in sexa
  write (varnamein,'(a,a)') trim(varname),'%IN'
  if (sic_varexist(varnamein)) then
      call sic_delvariable(varnamein,.false.,error)
      if (error) return
  endif
  call sic_defstructure(varnamein,.true.,error)
  if (error) return
  nv = lenc(varnamein)
  call sic_def_char(varnamein(1:nv)//'%VTYPE',source_invtype(1:lenc(source_invtype)),.true.,error)
  call sic_def_dble(varnamein(1:nv)//'%VELOCITY',source_invelocity,0,0,.true.,error)
  call sic_def_dble(varnamein(1:nv)//'%REDSHIFT',source_inredshift,0,0,.true.,error)
  !
end subroutine fill_doppler_source
!
subroutine nullify_astro_source(error)
  use gkernel_interfaces
  use astro_interfaces, except_this=>nullify_astro_source
  use ast_astro
  !---------------------------------------------------------------------
  ! @ private
  !  put to 0 all astro%source% values
  !---------------------------------------------------------------------
  logical,          intent(inout) :: error    ! Logical error flag
  !
  astro_source_name=''
  source_alpha=''
  source_delta=''
  source_ra=0d0
  source_dec=0d0
  source_az=0d0
  source_el=0d0
  source_vshift=0d0
  source_vlsr=0d0
  source_dop=0d0
  source_redshift=0.
  parallactic_angle=0d0
  source_invelocity=0.0
  source_inredshift=0.0
  source_invtype=''
  soukind=soukind_none
  !
  source_incoord=''
  source_ineq=0
  source_inbetasexa=''
  source_inlambdasexa=''
  !
  call delete_astro_source(error)
  if (error) return
  !
end subroutine nullify_astro_source
!
subroutine delete_astro_source(error)
  use gbl_message
  use gkernel_interfaces
  use astro_interfaces, except_this=>delete_astro_source
  !---------------------------------------------------------------------
  ! @ private
  !  delete the structure astro%source%
  !---------------------------------------------------------------------
  logical,          intent(inout) :: error    ! Logical error flag
  !Local
  character(len=*), parameter :: rname='SOURCE'
  character(len=varname_length) :: varname
  character(len=256) :: mess
  !
  varname = 'ASTRO%SOURCE'
  if (sic_varexist(varname)) then
    ! delete the sic variable
    call sic_delvariable(varname,.false.,error)
    if (error) return
    write (mess,'(a,1x,a,1x,a)') 'Variable',trim(varname),'deleted'
    call astro_message(seve%d,rname,mess)
  else
    call astro_message(seve%w,rname,'Variable allready deleted, nothing done')
    return
  endif
  !
end subroutine delete_astro_source
