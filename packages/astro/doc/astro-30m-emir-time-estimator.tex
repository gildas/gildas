\documentclass{article}
\usepackage{graphicx}
\usepackage{fancyhdr}
\usepackage{natbib}
\makeindex{}

\makeatletter
\textheight 625pt
\textwidth 460pt
\oddsidemargin 0pt
\evensidemargin 0pt
\marginparwidth 50pt
\topmargin 0pt

% Float Positionning Behaviour Correction.
\renewcommand{\textfraction}{0.0}
\renewcommand{\topfraction}{1.0}
\renewcommand{\bottomfraction}{1.0}
\renewcommand{\floatpagefraction}{0.35}
\setcounter{totalnumber}{5}
\setcounter{topnumber}{4}

\newcommand{\ie}{{\em i.e.}}
\newcommand{\eg}{{\em e.g.}}
\newcommand{\cf}{cf.}

\newcommand{\emm}[1]{\ensuremath{#1}}   % Ensures math mode.
\newcommand{\emr}[1]{\emm{\mathrm{#1}}} % Uses math roman fonts.
\newcommand{\nds}[1]{\emm{\displaystyle#1}} % Normal display size for array and tabular environment.
\newcommand{\paren}[1]{\nds{\left(  #1 \right) }} % Parenthesis. 
\newcommand{\cbrace}[1]{\nds{\left\{ #1 \right\}}} % Curly Braces.
\newcommand{\bracket}[1]{\nds{\left[  #1 \right] }} % Brackets.
\newcommand{\abs}[1]{\emm{\left| #1 \right|}} % Absolute value.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\tint}[1][]{\emm{t_\emr{#1}}}
\newcommand{\tonoff}{\tint[onoff]}
\newcommand{\ton}{\tint[on]}
\newcommand{\toff}{\tint[off]}
\newcommand{\tsig}{\tint[sig]}
\newcommand{\ttel}{\tint[tel]}
\newcommand{\npol}{\emm{n_\emr{pol}}}


\newcommand{\noise}[1][]{\emm{\sigma_\emr{#1}}}
\newcommand{\Tsys}{\emm{T_\emr{sys}}}
\newcommand{\speceff}{\emm{\eta_\emr{spec}}}
\newcommand{\obseff}{\emm{\eta_\emr{tel}}}

\newcommand{\dnu}{\emm{d\nu}}

\newcommand{\Tas}{\emm{T_\emr{a}^\star}}
\newcommand{\Tatm}{\emm{T_\emr{atm}}}
\newcommand{\Tcab}{\emm{T_\emr{cab}}}
\newcommand{\Trec}{\emm{T_\emr{rec}}}
\newcommand{\taus}{\emm{\tau_\emr{s}}}
\newcommand{\gim}{\emm{G_\emr{im}}}
\newcommand{\Feff}{\emm{F_\emr{eff}}}
\newcommand{\A}{\emm{A}}

\newcommand{\Amap}{\emm{A_\emr{map}}}
\newcommand{\Abeam}{\emm{A_\emr{beam}}}
\newcommand{\beam}{\emm{\theta}}
\newcommand{\grideff}{\emm{\eta_\emr{grid}}}
\newcommand{\nbeam}{\emm{n_\emr{beam}}}

\newcommand{\tontot} {\emm{\ton^\emr{tot}}}
\newcommand{\tofftot}{\emm{\toff^\emr{tot}}}

\newcommand{\tonbeam}{\emm{\ton^\emr{beam}}}
\newcommand{\toncover}{\emm{\ton^\emr{cover}}}
\newcommand{\toffbeam}{\emm{\toff^\emr{beam}}}
\newcommand{\toffcover}{\emm{\toff^\emr{cover}}}
\newcommand{\tsigbeam}{\emm{\tsig^\emr{beam}}}
\newcommand{\tsigcover}{\emm{\tsig^\emr{cover}}}

\newcommand{\fdump}[1][]{\emm{f_\emr{dump}}}
\newcommand{\vlin}[1][]{\emm{v_\emr{linear}^\emr{#1}}}
\newcommand{\varea}[1][]{\emm{v_\emr{area}^\emr{#1}}}
\newcommand{\vlinmax}{\vlin[max]}
\newcommand{\vareamax}{\varea[max]}
\newcommand{\rowseparation}{\emm{\Delta\theta}}

\newcommand{\nonperoff}{\emm{n_\emr{on/off}}}
\newcommand{\tstable}{\tint[stable]}
\newcommand{\Asubmap}{\emm{A_\emr{submap}}}
\newcommand{\tsubmap}{\tint[submap]}
\newcommand{\nsubmap}{\emm{n_\emr{submap}}}
\newcommand{\ncover}{\emm{n_\emr{cover}}}

\newcommand{\cint}[1]{\texttt{int(}#1\texttt{)}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Makes fancy headings and footers.
\pagestyle{fancy}

\fancyhf{} % Clears all fields.

\lhead{\scshape IRAM-30m EMIR time/sensitivity estimator}
\rhead{\scshape \nouppercase{\rightmark}}   %
\fancyfoot[C]{\bfseries \thepage{}}

\renewcommand{\headrulewidth}{0pt}    %
\renewcommand{\footrulewidth}{0pt}    %

\renewcommand{\sectionmark}[1]{%
  \markright{\thesection. \MakeLowercase{#1}}}
\renewcommand{\subsectionmark}[1]{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\title{IRAM Memo 2009-1\\[3\bigskipamount]
  IRAM-30m EMIR time/sensitivity estimator}%
\author{J. Pety$^{1,2}$, S. Bardeau$^{1}$, E. Reynier$^{1}$\\[0.5cm]
  1. IRAM (Grenoble) \\
  2. Observatoire de Paris}%
\date{Feb, 18th 2010\\
  Version 1.1}

\maketitle

\begin{abstract}
  This memo describes the equations used in the IRAM-30m EMIR
  time/sensitivity estimator available in the \texttt{GILDAS/ASTRO}
  program. A large part of the memo aims at deriving sensitivity
  estimate for the case of On-The-Fly observations, which is not clearly
  documented elsewhere (to our knowledge). Numerical values of the
  different parameters used in the time/sensitivity estimator are grouped
  in appendix A.
\end{abstract}

\vspace*{3cm}

\noindent{}History:
\begin{description}
\item[Version 1.0] (Feb, 04th 2009).
\item[Version 1.1] (Feb, 18th 2010) Simplified.
\end{description}

\newpage{}

\tableofcontents{}
\newpage{}

\section{Generalities}

\subsection{The radiometer equation}

The radiometer equation for a total power measurement reads
\begin{equation}
  \noise = \frac{\Tsys}{\speceff\sqrt{\dnu\,\tint}},
  \label{eq:radiometer:1}
\end{equation}
where \noise{} is the rms noise obtained by integration during \tint{} in a
frequency resolution \dnu{} with a system whose system temperature is given
by \Tsys{} and spectrometer efficiency is \speceff{}. However, total power
measurement includes other contributions (\eg{} the atmosphere emission) in
addition to the astronomical signal. The usual way to remove most of the
unwanted contributions is to switch, \ie{} to measure alternatively
on-source and off-source and then to subtract the off-source from the
on-source measurements. It is easy to show that the rms noise of the
obtained measurement is
\begin{equation}
  \noise = \sqrt{\noise[on]^2+\noise[off]^2} = \frac{\Tsys}{\speceff\sqrt{\dnu\,\tsig}}
  \quad \mbox{with} \quad
  \tsig = \frac{\ton\,\toff}{\ton+\toff},
  \label{eq:radiometer:2}
\end{equation}
where \noise[on] and \noise[off] are the noise of the on and off
measurement observed respectively during the \ton{} and \toff{} integration
time. \tsig{} is just a useful intermediate quantity. 

\subsection{System temperature}

The system temperature is a summary of the noise added by the system. This
noise comes from 1) the receiver and the optics, 2) the emission of the
sky, and 3) the emission picked up by the secondary side lobes of the
telescope. It is usual to approximate it (in the \Tas{} scale) with
\begin{equation}
  \Tsys = \frac{\paren{1+\gim}\exp\cbrace{\taus\,\A}}{\Feff}
          \bracket{\Feff\,\Tatm\,\paren{1-\exp\cbrace{-\taus\,\A}}+\paren{1-\Feff}\Tcab+\Trec},
\end{equation}
where \gim{} is the receiver image gain, \Feff{} the telescope forward
efficiency, $\A = 1/\sin(\emr{elevation})$ the airmass, \taus{} the
atmospheric opacity in the signal band, \Tatm{} the mean physical
atmospheric temperature, \Tcab{} the ambient temperature in the receiver
cabine and \Trec{} the noise equivalent temperature of the receiver and the
optics. All those parameters are easily measured, except \taus{}, which is
depends on the amount of water vapor in the atmosphere and which is
estimated by complex atmospheric models.

\subsection{Elapsed telescope time}

The goal of a time estimator is to find the elapsed telescope time
(\ttel{}) needed to obtain a given rms noise, while a sensitivity estimator
aims at finding the rms noise obtained when observing during \ttel{}. If
\tonoff{} is the total integration time spent both on the on-source and
off-source observations, then
\begin{equation}
  \tonoff = \obseff \, \ttel,
  \label{eq:tel-vs-onoff}
\end{equation}
where \obseff{} is the efficiency of the observing mode, \ie{} the time
needed 1) to do calibrations (\eg{} pointing, focus, temperature scale
calibration), and 2) to slew the telescope between useful integrations.

The tuning of the receivers is not proportional to the total integration
time but it should be added to the elapsed telescope time. A time estimator
can hardly anticipate the total tuning time for a project. Indeed, one
project (\eg{} faint line detection) can request only one tuning to be used
during many hours and another (\eg{} line survey) can request a tuning
every few minutes. In our case, we thus request that the estimator user add
by hand the tuning time to the elapsed telescope time estimation.

\subsection{The number of polarizations}

Heterodyne mixers are coupled to a single linear polarization of the
signal. Hence, heterodyne receivers have at least two mixers, each one
sensitive to one of the two linear polarization of the incoming signal.
Both mixers are looking at the same sky position. This implies that we have
to distinguish between the time spent on a given position of sky and the
human elapsed time. Indeed, we will use the time spent on a given position
of the sky when estimating the sensitivity, while we will give human
elapsed time for the telescope and the on and off times. 

If the mixers are tuned at the same frequency, the times spent on and off
in the same direction of the sky will be twice the human elapsed time.  We
thus have to introduce the number of polarization simultaneously tuned at
the same frequency, \npol{}, which can be set to 1 or 2. It happens that
for EMIR, the two polarizations are always tuned at the same frequency,
\ie{} $\npol=2$. The simplest way to take into account the distinction
between human time and sky time is to slightly modify the radiometer
equation to take into account the number of polarization
\begin{equation}
  \noise = \frac{\Tsys}{\speceff\sqrt{\dnu\,\npol\,\tsig}}
  \quad \mbox{with} \quad
  \tsig = \frac{\ton\,\toff}{\ton+\toff}.
  \label{eq:radiometer:3}
\end{equation}
This equation implies that \ton{}, \toff{}, \tonoff{} and \ttel{} will be
human times.

\subsection{Switching modes and observation kinds}

Switching is done in two main ways.
\begin{description}
\item[Position switch] where the off-measurement is done on a close-by sky
  position devoid of signal. Wobbler switching is a particular case.
\item[Frequency switch] where the telescope always points towards the the
  source and the switching is done in the frequency (velocity) space.
\end{description}
Moreover, there are two main observation kinds.
 \begin{description}
\item[Tracked observations] where the telescope track the source, \ie{} it
  always observes the same position in the source referential. The result
  is a single spectra.
\item[On-The-Fly observations] where the telescope continuously slew
  through the source with time to map it. The result is a cube of spectra.
\end{description}

In the following, we will work out the equations needed by the
time/sensitivity estimator for each combination.

\section{Tracked observations}
\label{sec:tracked}

\subsection{Frequency switched}
\label{sec:tracked:fsw}

In this case, all the time is spent in the direction of the source.
However, the frequency switching also implies that all this times can be
counted as on-source and off-source times. Thus
\begin{equation}
  \tonoff = \ton = \toff,
\end{equation}
\begin{equation}
  \tsig = \frac{\ton}{2} = \frac{\toff}{2} = \frac{\tonoff}{2},
\end{equation}
and 
\begin{equation}
  \noise[fsw] = \frac{\sqrt{2}\,\Tsys}{\speceff\sqrt{\dnu\,\npol\,\obseff\,\ttel}}.
\end{equation}

\subsection{Position switched}
\label{sec:tracked:psw}

In this case, only half of the time is spent in the direction of the
source. Thus
\begin{equation}
  \ton = \toff = \frac{\tonoff}{2},
\end{equation}
\begin{equation}
  \tsig = \frac{\ton}{2} = \frac{\toff}{2} = \frac{\tonoff}{4},
\end{equation}
and 
\begin{equation}
  \noise[psw] = \frac{2\,\Tsys}{\speceff\sqrt{\dnu\,\npol\,\obseff\,\ttel}}.
\end{equation}

\subsection{Comparison}
\label{sec:tracked:comp}

For tracked observations, position switched observations results in a
noise rms $\sqrt{2}$ larger than frequency switched observations for the
same elapsed telescope time. In other words, frequency switched
observations are twice as efficient in time to reach the same rms noise
than position switched observations.

However, time efficiency is not the only criteria of choice. Indeed, with
the current generation of receivers (before march 2009), the IF bandpass is
much cleaner in position switched than in frequency switched
observations. Frequency switched is thus really useful only when the lines
are narrow so that the IF bandpass can be easily cleaned out through
baselining with low order polynomials.

\section{On-The-Fly observations}
\label{sec:otf}

\subsection{Additional notions and notations}

The On-The-Fly (OTF) observing mode is used to map a given region of the
sky. The time/sensitivity estimator will have to link the elapsed telescope
time to cover the whole mapped region to the sensitivity in each
independent resolution element. To do this, we need to introduce
\begin{itemize}
\item \Amap{} and \Abeam{}, which are respectively the area of the map and
  the area of the resolution element. The map area is a user input while
  the resolution area is linked to the telescope full width at half maximum
  (\beam{}) by
  \begin{equation}
    \Abeam = \frac{\grideff\,\pi\,\beam^2}{4\,\ln(2)}
  \end{equation}
  where \grideff{} comes from the fact that the OTF data is gridded by
  convolution. When the convolution kernel is a Gaussian of FWHM equal to
  $\beam/3$ (the default inside the \texttt{GILDAS/CLASS} software), it is
  easy to show that
  \begin{equation}
    \grideff = 1+\frac{1}{9} \simeq 1.11.
  \end{equation}
\item The number of independent measurement (\nbeam{}) in the final map
  which is given by
  \begin{equation}
    \nbeam = \frac{\Amap}{\Abeam}.
    \label{eq:nbeam:1}
  \end{equation}
\item The on and off time spent per independent measurement, \tonbeam{} and
  \toffbeam{}. The associated \tsigbeam{} can then be written
  \begin{equation}
    \tsigbeam = \frac{\tonbeam\,\toffbeam}{\tonbeam+\toffbeam}
  \end{equation}
\item The on and off time spent to map the whole map,
  \tontot{} and \tofftot{}. \tonoff{} is deduced from \tontot{} and
  \tofftot{} in a way which depends on the switching scheme.
\end{itemize}
In addition, we must ensure that the user does not try to scan faster than
the telescope can slew. To do this, we need to introduce
\begin{itemize}
\item The linear scanning speed, \vlin{}, and its maximum value, \vlinmax{}.
\item The area scanning speed, \varea{}, and its maximum value,
  \vareamax{}. When the scanning pattern is linear, then \varea{} and
  \vlin{} are linked through
  \begin{equation}
    \varea = \vlin \, \rowseparation,
  \end{equation}
  where \rowseparation{} is the separation between consecutive rows. To
  avoid nasty signal and noise aliasing problems, we must ensure a Nyquist
  sampling, \ie{}
  \begin{equation}
    \rowseparation = \frac{\beam}{2.5}.
  \end{equation}
\end{itemize}

\subsection{Frequency switched}
\label{sec:otf:fsw}

In frequency switched observations, the switching happens as the telescope
is slewed. This is correct as long as the switching time is much smaller
than the time needed to slew a significant fraction of the telescope beam.

It is easy to understand that
\begin{equation}
  \tonoff = \tontot = \tofftot,
\end{equation}
\begin{equation}
  \tonbeam = \toffbeam = \frac{\tonoff}{\nbeam},
\end{equation}
\begin{equation}
  \tsigbeam = \frac{\tonbeam}{2} = \frac{\toffbeam}{2} = \frac{\tonoff}{2\nbeam},
\end{equation}
and 
\begin{equation}
  \noise[fsw] = \frac{\sqrt{2\,\nbeam}\,\Tsys}{\speceff\sqrt{\dnu\,\npol\,\obseff\,\ttel}}.
\end{equation}
The velocity check can then be written as
\begin{equation}
  \frac{\Amap}{\tonoff} \le \vareamax.
\end{equation}

\subsection{Position switched}
\label{sec:otf:psw}

\subsubsection{Two key points: 1) Sharing OFF among many ONs and 2) system
  stability timescale}

When the stability of the system is long enough, we can share the same off
for several \emph{independent} on-positions measured in a row (\eg{}
ON-ON-ON-OFF-ON-ON-ON-OFF...). The first key point here is the fact that
the on-positions must be independent. The OTF is an observing mode where
the sharing of the off can be used because the goal is to map a given
region of the sky made of independent positions or resolution elements.
When sharing the off-position between several on, \citet{ball76} showed
that the optimal off integration time is
\begin{equation}
  \toff^\emr{optimal} = \sqrt{\nonperoff} \, \ton
  \label{eq:toff:optimal}
\end{equation}
where \nonperoff{} is the number of on measurements per off. Replacing
\toff{} by its optimal value in eq.~\ref{eq:radiometer:3}, we obtain
\begin{equation}
  \tsig = \frac{\ton}{1+\frac{1}{\sqrt{\nonperoff}}}
  \quad \mbox{and} \quad
  \noise =
  \frac{\Tsys}{\speceff\sqrt{\dnu\,\npol\,\ton}}\sqrt{1+\frac{1}{\sqrt{\nonperoff}}}.
  \label{eq:tsig:onperoff}
\end{equation}
We thus see that the rms noise decreases when the number of independent on
per off increases. It seems tempting to have only one off for all the on
positions of the OTF map. However, the second key point of the method is
that the system must be stable between the first and last on
measurement. To take this point into account we must introduce
\begin{itemize}
\item The concept of submap, which is a part of a map observed between two
  successive off measurements. 
\item \Asubmap{}, which is the area covered by the
  telescope in each submap.
\item \nsubmap{} the number of such submaps needed to cover the whole map
  area. 
\item \tstable{}, the typical time where the system is stable. This time
  will be the maximum time between two off measurements, which is noted
  \tsubmap{}.
\item \ncover{}, the number of coverages needed either to reach a given
  sensitivity or to exhaust the acquisition time.
\item \toncover{} and \toffcover{} are the times spent respectively on and
  off per independent measurement and per coverage.
\end{itemize}
We note that the number of on per off (\nonperoff{}) is a purely
geometrical quantity. This implies that the time spent off is linked to the
time spent on by Eq.~\ref{eq:toff:optimal} both in each individual coverage
and when averaging all the coverages.

\subsubsection{Relation between \tonoff{} and \tsigbeam{}}

By construction
\begin{itemize}
\item The number of submaps is the area of the map divided by the area of a
  submap
  \begin{equation}
    \nsubmap = \frac{\Amap}{\Asubmap}.
    \label{eq:nsubmap}
  \end{equation}
\item The number of on per off is the number of independent resolution
  elements in each submap
  \begin{equation}
    \nonperoff = \frac{\Asubmap}{\Abeam}.
    \label{eq:nonperoff}
  \end{equation}
\item The number of independent resolution elements in the map is the
  product of number of submaps by the number of on per off
  \begin{equation}
    \nbeam = \nsubmap\,\nonperoff.
    \label{eq:nbeam:2}
  \end{equation}
\item The submap area is the product of the area velocity by the time to
  cover it
  \begin{equation}
    \Asubmap = \varea \, \tsubmap.
    \label{eq:asubmap}
  \end{equation}
\item The time to scan a submap is the sum of the \nonperoff{} independent
  on integration time
  \begin{equation}
    \tsubmap = \nonperoff\,\toncover.
    \label{eq:tsubmap}
  \end{equation}
\item The relations between times per coverage and times integrated over
  all the coverages are
  \begin{equation}
    \tonbeam = \ncover\,\toncover 
    \quad \mbox{and} \quad
    \toffbeam = \ncover\,\toffcover
    \quad \mbox{with} \quad
    \toffcover = \sqrt{\nonperoff}\,\toncover.
  \end{equation}
\item Using the last two points, it is easy to derive
  \begin{equation}
    \tsigbeam = \ncover\,\tsigcover 
              = \frac{\ncover\,\tsubmap}{\nonperoff+\sqrt{\nonperoff}}.
    \label{eq:otf:psw:tsigbeam}
  \end{equation}
\item Finally, the total time spent on and off is given by
  \begin{equation}
    \tonoff = \ncover\,\nsubmap\,(\nonperoff\,\toncover + \toffcover).
  \end{equation}
  Using Eqs.~\ref{eq:toff:optimal} and~\ref{eq:tsubmap}, we derive
  \begin{equation}
    \tonoff =
    \ncover\,\tsubmap\,\nsubmap\,\paren{1+\frac{1}{\sqrt{\nonperoff}}}.
    \label{eq:otf:psw:tonoff}
  \end{equation}  
\end{itemize}
Both \tsigbeam{} and \tonoff{} are proportional to $\ncover\,\tsubmap$
(\cf{} Eqs.~\ref{eq:otf:psw:tsigbeam} and~\ref{eq:otf:psw:tonoff}). It is
thus easy to derive that
\begin{equation}
  \tonoff = \tsigbeam\,\nsubmap\,\paren{1+\sqrt{\nonperoff}}^2.
\end{equation}
Using Eq.~\ref{eq:nbeam:2}, we can replace \nonperoff{} and obtain
\begin{equation}
  \tonoff = \tsigbeam\,\paren{\sqrt{\nsubmap}+\sqrt{\nbeam}}^2.
  \label{eq:otf:psw:tonoffvstsig}
\end{equation}
Using Eqs.~\ref{eq:radiometer:3}, \ref{eq:tel-vs-onoff}
and~\ref{eq:otf:psw:tonoffvstsig}, we obtain
\begin{equation}
  \noise[psw] =
  \frac{\paren{\sqrt{\nbeam}+\sqrt{\nsubmap}}\,\Tsys}{\speceff\,\sqrt{\dnu\,\npol\,\obseff\,\ttel}}.
\end{equation}
The last equation in theory enables us to find the rms noise as a function
of the elasped telescope time (sensitivity estimation) and vice-versa (time
estimation). However, it is not fully straightforward because we must
enforce that \ncover{} and \nsubmap{} have an integer value. 

\subsubsection{Time/Sensitivity estimation}

This paragraph describes the algorithm to do a time/sensitivity estimation
for a position-switched On-The-Fly observation.
\begin{description}
\item[Step \#1: Computation of \nbeam{} and \nsubmap{}] \mbox{}\\
  \nbeam{} is just computed as the ratio $\Amap/\Abeam$.  Using
  Eqs.~\ref{eq:asubmap} and~\ref{eq:nsubmap}, we obtain
  \begin{equation}
    \nsubmap = \frac{\Amap}{\varea \, \tsubmap}.
    \label{eq:algo:nsubmap}
  \end{equation}
  Using this equation, we start to compute \nsubmap{} for $\tsubmap =
  \tstable$ and $\varea = \vareamax$. We want to enforce the integer
  character of \nsubmap{} in a way which decreases the product
  $\tsubmap\,\varea$. To do this, we use
  \begin{equation}
    \nsubmap = 1+\cint{\nsubmap}.
    \label{eq:nsubmap:int}
  \end{equation}
  Eq.~\ref{eq:nsubmap:int} ensures that $\tsubmap\,\varea <
  \tstable\,\varea[max]$. The value of \varea{} must be decreased so that
  Eq.~\ref{eq:algo:nsubmap} is enforced.
\item[Step \#2: Computation of \ttel{} or \noise{}] We use the following
  equations in descending order to compute the elapsed telescope time and
  in ascending order to compute the rms noise level:
  \begin{eqnarray}
    1. & & \tsigbeam = \frac{\Tsys^2}{\speceff^2\,\noise^2\,\dnu\,\npol},\\
    2. & & \tonoff = \tsigbeam\,\paren{\sqrt{\nsubmap}+\sqrt{\nbeam}}^2,\\
    3. & & \obseff\,\ttel = \tonoff.
  \end{eqnarray}
\item[Step \#3: Computation of derived quantities] \mbox{}\\
  \begin{eqnarray}
    1. & & \nonperoff = \frac{\nbeam}{\nsubmap},\\
    2. & & \ncover = \frac{\tsigbeam}{\tsubmap}\paren{\nonperoff+\sqrt{\nonperoff}},\\
    3. & & \tonbeam = \frac{\ncover\,\tsubmap}{\nonperoff}\\
    3. & & \toffbeam = \tonbeam\,\sqrt{\nonperoff}.
  \end{eqnarray}
\item[Step \#4: Is \ncover{} an integer number?]  The interpretation of the
  above equation to compute \ncover{} has two cases.
  \begin{enumerate}
  \item $\ncover < 1$. This means that either the user tries to cover a too
    large sky area in the given telescope elasped time (sensitivity
    estimation) or the telescope need a minimum time to cover \Amap{} at
    the maximum velocity possible with the telescope and this minimum time
    implies a more sensitive observation than requested by the user (time
    estimation).
  \item $\ncover \ge 1$. \ncover{} will generally not be an integer, we can
    think to decrease \tsubmap{} from \tstable{} to obtain an integer
    value. However, this must be done at constant $\Asubmap (= \varea \,
    \tsubmap)$.  Decreasing \tsubmap{} thus implies increasing \varea{}.
    It is not clear that this is possible because of the constraint $\varea
    < \vareamax$.  Another way to deal with this is to keep \tsubmap{} to
    its maximum value and to adjust \ttel{} (sensitivity estimation) or
    \tsig{} and thus \noise{} (time estimation) to obtain an integer value
    of \ncover{}. However, this implies a change in the wishes of the user.
    The program can not make such a decision alone and we will only warn
    the user. Indeed, the worst case is when \ncover{} is changing from 1
    to 2 because this can decrease the sensitivity by a factor 1.4
    (sensitivity estimation) or double the elapsed telescope time (time
    estimation). The larger the value of \ncover{} the less harm it is to
    enforce the integer character of \ncover.
  \end{enumerate}
\end{description}

\subsection{Comparison}
\label{sec:otf:comp}

Contrary to tracked observations, the position switched observing mode can
be more efficient than the frequency switched observing mode. Indeed, in
frequency switch, the same time is spent in the on and off spectrum. When
subtracting them, the off brings as much noise as the on. In position
switch, the same off can be shared between many ons, in which case the
optimal integration time on the off is much larger than on each independent
on spectrum. Hence, the noise brought by the off spectrum can be much
smaller than the noise brought by the on spectrum.

For frequency switched observations, 
\begin{equation}
  \noise[fsw] = \frac{\sqrt{2\,\nbeam}\,\Tsys}{\speceff\sqrt{\dnu\,\npol\,\obseff\,\ttel}},
\end{equation}
while for position switched observations,
\begin{equation}
  \noise[psw] =
  \frac{\paren{\sqrt{\nbeam}+\sqrt{\nsubmap}}\,\Tsys}{\speceff\,\sqrt{\dnu\,\npol\,\obseff\,\ttel}}.
\end{equation}
We thus have
\begin{equation}
  \frac{\noise[psw]}{\noise[fsw]} = \frac{1}{\sqrt{2}}\,\paren{1+\sqrt{\frac{\nsubmap}{\nbeam}}}.
\end{equation}
Position switched OTF is more efficient than frequency switched OTF for
\begin{equation}
  \frac{\nbeam}{\nsubmap} = \nonperoff \ge \frac{1}{3-2\sqrt{2}} \sim 6.
\end{equation}
Moreover, $\noise[psw]/\noise[fsw] \simeq 0.84$ for $\nonperoff = 30$, and
$\simeq 0.78$ for $\nonperoff = 100$. Using eqs.~\ref{eq:asubmap}
and~\ref{eq:nonperoff}, we see that the limit on the maximum number of on
per off is set by
\begin{equation}
  \nonperoff = \frac{\tstable}{\Abeam/\vareamax},
\end{equation}
\ie{} the ratio of the maximum system stability time by the minimum time
required to map a telescope beam.

As for tracked observations, there are other considerations to be taken
into account. For extra-galactic observations, the lines are large which
excludes the use of frequency switched observations. For Galactic
observations, the intrinsic sensitivity of the receivers may make it
difficult to find a closeby position devoid of signal. We can still use the
position switched OTF observing mode. But we then have to observe the off
position in frequency switched track observing mode long enough to be able
to add back the off astronomical signal.

\section{Acknowledgement}

The author thanks M.~Gu\'elin, S.~Guilloteau, C.~Kramer and C.~Thum for
useful discussions.

\begin{thebibliography}{9}
  \expandafter\ifx\csname natexlab\endcsname\relax\def\natexlab#1{#1}\fi
  
\bibitem[{Ball (1976)}]{ball76} Ball, J. A., in Meth. Exper. Phys.,
  Vol. 12, Part C, Astrophysics: Radio Observations, Chap. 4.3, p. 46,
  ed. M. L. Meeks

\end{thebibliography}

\newpage{}
\appendix{}

\section{Numerical values}

This appendix groups all the numerical values used in the time/sensitivity
estimator. We made conservative choices for two reasons: 1)
time/sensitivity estimators tend to be too optimistics and 2) EMIR is a new
generation of receivers which had not yet been tested at the telescope.

\subsection{Overheads}

\begin{itemize}
\item $\obseff = 0.5.$
\item After estimating the number of tunings needed to complete the
  project, the user has to add to the telescope time 30 minutes per tuning
  (this includes the observation of a line calibrator).
\end{itemize}

\subsection{Atmosphere}

\begin{itemize}
\item $\Tatm = 250$~K.
\item The opacities at signal frequencies are computed with a recent
  version of the ATM program (maintained by J.R.~Pardo). 
\item They are computed for 3 different amount of water vapor per season
  (1, 2 and 4~mm for the winter season and 2, 4 and 7~mm for the summer
  season).
\end{itemize}

\subsection{Telescope}

\begin{itemize}
\item $\Tcab = 290$~K.
\item $\Feff = 0.95$ at 3~mm, 0.93 at 2~mm, 0.91 at 1~mm and 0.88 at
  0.8~mm.
\end{itemize}

\subsection{Frontends}

Warning: Please do not quote these values in your papers. You should refer
to the publications which fully describe the receivers.
\begin{itemize}
\item The receiver temperature is the sum of
  \begin{itemize}
  \item The mixer temperature: Typically 50~K below 260~GHz and 70~K above;
  \item The mirror losses: Typically 10~K;
  \item The dichroic losses: Typically 15~K. Nota Bene: Dichroics enable dual
    frequency observation by frequency separation of the sky signal.
  \end{itemize}
  We end up with $\Trec = 75$~K below 260~GHz and $Trec = 95$~K above
  260~GHz.
\item $\gim = 0.1$.
\end{itemize}

\subsection{Backends}

\begin{itemize}
\item $\speceff = 0.87$ because of the 2 bit quantization at the input of
  the correlators.
\item The noise equivalent bandwidth of our correlators is almost equal to
  the channel spacing. So we do not take this into account in our
  estimation.
\end{itemize}

\subsection{On-The-Fly}

\begin{itemize}
\item $\tstable = 2$~minutes.
\item $\beam = \,\frac{2460''}{\nu\,\emr{GHz}}$.
\item The maximum linear velocity is limited by the maximum dumping rate of
  $\fdump = 2$~Hz. We know that in order to avoid beam elongation along the
  scanning direction, we need to sample at least 4 points per beam in the
  scanning direction. We thus end up with
  \begin{equation}
    \vlinmax = \fdump\,\frac{\beam}{4}\,\emr{arcsec/s}
  \end{equation}
  and
  \begin{equation}
    \vareamax = \fdump\,\frac{\beam}{2.5}\,\frac{\beam}{4}\,\emr{arcsec^2/s}
    \quad \mbox{or} \quad
    \vareamax = \fdump\,\frac{\beam^2}{10}\,\emr{arcsec^2/s}
  \end{equation}

\end{itemize}

\newpage{}
\section{Optimal number of ON per OFF measurements}

This section is just a reformulation of the original demonstration
by~\citet{ball76}.

Let's assume that we are measuring \nonperoff{} \emph{independent}
on-positions for a single off. The same integration time (\ton{}) is spent
on each on-position and the off integration time is
\begin{equation}
  \toff = \alpha \, \ton,
\end{equation}
where $\alpha$ can be varied. Using eq.~\ref{eq:radiometer:2} and $\tonoff
= \nonperoff \ton + \toff = (\nonperoff+\alpha)\,\ton$, it can be shown
than
\begin{equation}
  \tonoff = \frac{\Tsys^2}{\speceff^2\,\sigma^2\,\dnu} \paren{1+\nonperoff+\alpha+\frac{\nonperoff}{\alpha}}.
\end{equation}
Differenciating with respect to $\alpha$, we obtain
\begin{equation}
  \frac{d\tonoff}{d\alpha} \propto 1-\frac{\nonperoff}{\alpha^2}
\end{equation}
Setting the result to zero then gives that
the minimum elapsed time to reach a given rms noise is obtained for
\begin{equation}
  \alpha = \sqrt{\nonperoff} \quad \mbox{or} \quad
  \toff^\emr{optimal} = \sqrt{\nonperoff} \, \ton.
\end{equation}

\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
