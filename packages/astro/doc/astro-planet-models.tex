\documentclass{article}
\usepackage{graphicx}
\usepackage{fancyhdr}
\usepackage{natbib}
\makeindex{}

\makeatletter
\textheight 625pt
\textwidth 460pt
\oddsidemargin 0pt
\evensidemargin 0pt
\marginparwidth 50pt
\topmargin 0pt

% Float Positionning Behaviour Correction.
\renewcommand{\textfraction}{0.0}
\renewcommand{\topfraction}{1.0}
\renewcommand{\bottomfraction}{1.0}
\renewcommand{\floatpagefraction}{0.35}
\setcounter{totalnumber}{5}
\setcounter{topnumber}{4}

\newcommand{\emm}[1]{\ensuremath{#1}}   % Ensures math mode.
\newcommand{\emr}[1]{\emm{\mathrm{#1}}} % Uses math roman fonts.
\newcommand{\nds}[1]{\emm{\displaystyle#1}} % Normal display size for array and tabular environment.
\newcommand{\paren}[1]{\nds{\left(  #1 \right) }} % Parenthesis. 
\newcommand{\cbrace}[1]{\nds{\left\{ #1 \right\}}} % Curly Braces.
\newcommand{\bracket}[1]{\nds{\left[  #1 \right] }} % Brackets.
\newcommand{\abs}[1]{\emm{\left| #1 \right|}} % Absolute value.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% your macros

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Makes fancy headings and footers.
\pagestyle{fancy}

\fancyhf{} % Clears all fields.

\lhead{\scshape Planet brightness models}
\rhead{\scshape Boissier, Pety and Bardeau}   %
\fancyfoot[C]{\bfseries \thepage{}}

\renewcommand{\headrulewidth}{0pt}    %
\renewcommand{\footrulewidth}{0pt}    %

\renewcommand{\sectionmark}[1]{%
  \markright{\thesection. \MakeLowercase{#1}}}
\renewcommand{\subsectionmark}[1]{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

%\title{\includegraphics[height=3cm]{logo/iram} \\[3\bigskipamount]
%  IRAM Memo 2024-?? \\[3\bigskipamount]
%  Title}

\title{IRAM Memo 2024-01 \\[3\bigskipamount]
  Planet brightness temperatures in ASTRO }
\author{J.~Boissier$^{1}$, J.~Pety$^{1,2}$, S.~Bardeau$^{1,}$\\[0.5cm]
  1. IRAM (Grenoble); \\2. Observatoire de Paris}
\date{Dec 06$^\emr{th}$, 2024\\
  Version 1.0}
\maketitle{}

\begin{abstract}
  Planets are used as primary flux calibrator for both IRAM observatories.
  Planet brightness temperatures in ASTRO were not updated since their first implementation.
  The accuracy of the planet models have
greatly improved over the past years to about 5\% absolute accuracy,
rendering it important to update ASTRO.
We present in  this report the comparison of the current implementation of planet brightness temperature in ASTRO to the results of more advanced models.
We describe how we implement new models in ASTRO and what could be the impact on IRAM instrument calibration.

\end{abstract}

\newpage{}
\tableofcontents{}

\newpage{}

\section{Introduction}


Solar System bodies are commonly used as primary flux calibrators in radio astronomy.


Calibration of 30m data relies on good models of the primary
calibrators. Main primary calibrators are Uranus and Mars. For
heterodyne observations (with EMIR), antenna temperatures (TA* scale)
are determined from regular hot/cold/sky calibrations done about every
15min. The scaling to main beam temperatures is done offline using
existing tables of main beam and forward efficiencies. The staff is
observing primary and secondary calibrators a few times per year.  The
main beam efficiencies (together with aperture efficiencies, Jy/K
factors, HPBWs) are derived from night time observations of primary
calibrators. Observers are informed about the current efficiencies via
a public wiki page: https://publicwiki.iram.es/Iram30mEfficiencies.
Updated numbers are in preparation.  The flux stability of primary
and secondary calibrators for 1985-2005 is discussed in Kramer, Moreno and Greve (2008, 
https://arxiv.org/abs/0801.4452).
On the other hand, NIKA2 data are
calibrated to the mJy/beam scale by regular observations of primary
and secondary calibrators every few hours (see
https://www.iram.fr/~gildas/dist/piic-dafs.pdf and Perotto et
al. 2020). ASTRO has been used for the planetary brightness
temperatures and source fluxes.


For NOEMA, antenna efficiencies (Jy/K) during each individual track are determined through observations of secondary calibrators (mostly MWC349 and LKH$\alpha$101).  The fluxes of the primary calibrators are measured during dedicated sessions in which they are observed together with planets (Uranus, Neptune, Mars). Also, planetary observations were used to assess the single-dish efficiencies of the NOEMA antennas (mostly Mars).

In the framework of the EHT collaboration, planet observations are also used to define the Jy/K scales of the stations.

The flux of planets is computed by ASTRO when using the PLANET command. The present (\emph{a.k.a. Legacy}) implementation was implemented more than 30 years ago and never updated since then.


 The accuracy of models of the primary calibrators have
greatly improved over the past years to about 5\% absolute accuracy,
rendering it important to update ASTRO.

In the present document we present a new implementation we plan de distribute in  2025 and its impact on the planet fluxes computed by ASTRO. We also compare the proposed ASTRO implementation to what is done in CASA, for ALMA flux calibration.


\section{Definitions}

\subsection{Planet flux computation}
In ASTRO the planets are defined as ellipses. 
The planet flux computation is based on a surface averaged brightness
temperature at the considered frequency.  The flux is then obtained
using this brightness temperature $T_B$ in the Planck function and
integrating this brightness over the planet apparent surface (which
depends on the distance to the Earth) according to the equation:

\begin{equation}
 F(\nu) = \pi\,\theta_{maj}\,\theta_{min} \, \frac{2\,h\,\nu}{c^2} \, \left[\frac{1}{e^{\frac{h \nu}{k T_B}}-1} - \frac{1}{e^{\frac{h \nu}{k T_{BG}}}-1}\right]
\end{equation}

$\theta_{maj} = \frac{D_{Maj}}{\Delta}$ and $\theta_{min} =
\frac{D_{Min}}{\Delta}$ are the apparent major and minor axis of the
planet ($D$ being the planet diameter and $\Delta$ the geocentric
distance in km).
$T_{BG}$ is the cosmic background radiation (2.7~K)

We list in Table~\ref{TabMod} the brightness models used in the
original ASTRO implementation (\emph{Legacy} models), the models we propose to implement (2024), and the models used in CASA.

\begin{table}
  \begin{center}
  \caption{Planet brightness models in ASTRO and CASA}
  \label{TabMod}
  \begin{tabular}{c c c c}
    \hline
    Planet & ASTRO \emph{Legacy} & ASTRO 2024 & CASA \\
    \hline
    Mercury & Flat Spectrum & Flat Spectrum & No model \\
    & & & (too close to the Sun)\\
    Venus   & Spectral Index& Spectral Index & Physical model \\
    & & & Clancy et al. 2012 \\
    Mars & Flat spectrum, $r_h$ scaling & Physical model & Physical model \\
    & & Lellouch et al. & Butler et al. \\
    Juptier & Flat spectrum & Physical model & Physical model \\
    & & ESA2 (R. Moreno) & ESA4 (G. Orton and R. Moreno) \\
    Saturn & Flat spectrum & Physical model & No model \\
    & & ESA2 (R. Moreno) & \\
    Uranus & Spectral index & Physical model & Physical model \\
    & & ESA2 (R. Moreno) & ESA4 (G. Orton and R. Moreno) \\
    Neptune & Spectral index & Physical model & Physical model \\
    & & ESA5 (R. Moreno) & ESA3 (G. Orton and R. Moreno) \\
    \hline
  \end{tabular}
  \end{center}
\end{table}


\subsection{\emph{Legacy} implementation in ASTRO}

In the \emph{Legacy} implementation of the planet flux computation, the basis
is a reference brightness temperature at 100GHz, derived from radio
observations.

For Mercury, Jupiter, and Saturn the brightness temperature is assumed
to be constant over the spectrum

For Venus, Uranus and Neptune, the variation of the brightness
temperature with frequency is accounted for using a spectral index.

For Mars, the brightness temperature is scaled according to the square
root of the heliocentric distance of the planet.


\subsection{2024 implementation in ASTRO}
\paragraph{Unchanged} The models of Mercury and Venus are not changed.

\paragraph{Mars} The brightness temperature of Mars depends on the frequency and on the time of the observations. 
E. Lellouch et al. in Observatoire de Paris developped an implementation of a full physical and radiative transfer model to compute the surface-averaged brightness temperature of Mars. \\
 (\underline{https://www.lesia.obspm.fr/perso/emmanuel-lellouch/mars/}))\\
Such a tool is used by some observers of the IRAM community, especially for 30m observations.
Lellouch et al. agreed to provide us with the Mars model sources so that we can directly use their model in ASTRO.
We are now in the process of finding the best way to implement this.


\paragraph{Giant planets}
Planetary science experts developped over the years some more advanced
modelling of the brightness temperature of the giant planets (e.g. in
the framework of Herschel flux calibration).  The team in
LESIA-Observatoire de Paris provided the Gildas team with the output
of such models for the giant planet (model series ESA*).  The format
is a table of surface-averaged brightness temperatures between 2 and
1000~GHz.

\subsection{Planet brightness temperatures in CASA}
\label{SecCASA}
For comparison, we describe here the planet brightness models used in CASA.
The planet models used in CASA are described in the ALMA report 784, by B. Butler.

In Summary:
\begin{itemize}
\item{\bf Mercury:}  No model for Mercury
\item{\bf Venus:} Table based on model by Clancy et al. 2012. 
\item{\bf Mars:}
CASA makes use of tabulated brightness temperature values computed by
a full implementation of a physical and radiative transfer model (based on Rudy et al. 1987), for which an online implementation was made by B. Butler from NRAO. \\
 (\underline{http://www.aoc.nrao.edu/~bbutler/work/mars/model/})\\
\item{\bf Jupiter:} Table based on G. Orton model
\item{\bf Saturn:}  No model for Saturn
\item{\bf Uranus:} Table based on G. Orton and R. Moreno models (ESA4)
\item{\bf Neptune:} Table based on G. Orton and R. Moreno models (ESA3)
\end{itemize}


\section{Results}

We compare in Figure~\ref{FigAll} the brightness temperature spectrum
(from 70~GHz to 400~GHz) as found in the \emph{Legacy} implementation in ASTRO (black
lines) with the  2024 values (blue lines) and the
CASA models (red lines).  Figures~\ref{FigB1}--\ref{FigB4} show the
same thing, zooming on the 4 IRAM bands.
The time used for Mars computation (the only model for which time variations are taken into account) is 01-Jan-2010.

\begin{figure}
\includegraphics[width=14cm]{TbPlanet-ZOOM.eps}
\caption{Comparison of brightness temperature spectra of planets as
  present in the ASTRO \emph{Legacy} version (black lines) with the 2024
  models (blue lines) and CASA models (red lines). The colored areas represent the IRAM bands 1--4.}
\label{FigAll}
\end{figure}


\begin{figure}
\includegraphics[width=14cm]{TbPlanet-B1.eps}
\caption{Same as Fig.~\ref{FigAll} but zoom on the IRAM B1 (70--120~GHz); Comparison of brightness temperature spectra of planets as
  present in the ASTRO \emph{Legacy} version (black lines) with the 2024
  models (blue lines) and CASA models (red lines).}
\label{FigB1}
\end{figure}

\begin{figure}
\includegraphics[width=14cm]{TbPlanet-B2.eps}
\caption{Same as Fig.~\ref{FigAll} but zoom on the IRAM B2 (120--180~GHz); Comparison of brightness temperature spectra of planets as
  present in the ASTRO \emph{Legacy} version (black lines) with the 2024
  models (blue lines) and CASA models (red lines)}
\label{FigB2}
\end{figure}

\begin{figure}
\includegraphics[width=14cm]{TbPlanet-B3.eps}
\caption{Same as Fig.~\ref{FigAll} but zoom on the IRAM B3 (200--280~GHz); Comparison of brightness temperature spectra of planets as
  present in the ASTRO \emph{Legacy} version (black lines) with the 2024
  models (blue lines) and CASA models (red lines)}
\label{FigB3}
\end{figure}

\begin{figure}
\includegraphics[width=14cm]{TbPlanet-B4.eps}
\caption{Same as Fig.~\ref{FigAll} but zoom on the IRAM B4 (280--360~GHz); Comparison of brightness temperature spectra of planets as
  present in the ASTRO \emph{Legacy} version (black lines) with the 2024
  models (blue lines) and CASA models (red lines)}
\label{FigB4}
\end{figure}


\subsection{Telluric planets}


\subsubsection{Mars}

Mars is one of the most observed primary flux calibrators at NOEMA.

%The flat spectrum and heliocentric-distance scaling of Mars brightness temperature is by definition not as precise as the Mars physical models are.

In Fig.~\ref{FigAll}--\ref{FigB4} we compare the Mars spectrum provided by the ASTRO \emph{Legacy} implementation and by the CASA model on a single date (01-Jan-2010).

%The time variation of the Mars physical donot depend only on the heliocentric distance.
%The use of Global circulation models
In the Mars physical models time variations are not driven by heliocentric distance only, but also
by atmospherical circulation processes.

In Fig.~\ref{FigMarsT80}--\ref{FigMarsT360} we compare the time variations of the Mars surface average brightness temperature as computed by ASTRO \emph{Legacy} and CASA at different frequencies between 80 and 360~GHz.

We cannot show the output of the Lellouch model because the implementation scheme is not yet defined. However we expect it to be very similar to the CASA model, since it is another implementation of similar physical and radiative transfer model.


There are several obvious differences:
\begin{itemize}
\item The long scale (period of 2 years) variations in the physical models do not follow a sine-like curve as in the ASTRO \emph{Legacy} model. The temperature increase occurs in two phases separated by a plateau, while the decrease is quicker and steady.
\item The amplitude of the long scale variation is also larger in CASA than in  ASTRO \emph{Legacy}.
\item In addition to the long term variation there is in the physical model a daily variation of the temperature, with an amplitude of $\sim$2--3\%.
\item As seen in Fig.~\ref{FigAll} the temperature spectra is not flat in the physical model.
\end{itemize}


In summary the CASA model (i.e. physical model) produces  brightness temperatures 10\% lower than the ASTRO \emph{Legacy} values around the minimum of the \emph{long term} period for frequencies between 80 and 230~GHz.  For the rest of the period, the difference is much lower.
At higher frequencies (260--360~GHz) the minima and maxima of the CASA time variations are respectively 5\% lower and 5\% higher than the ASTRO \emph{Legacy} values.

{\bf The irregular time variations of Mars brightness temperature might affect the flux monitoring of the secondary calibrators, especially at 3mm.}




\subsubsection{Mercury}
With its small elongation Mercury is generally not observed in the mm range, and is not used as a flux calibrator.
No new model introduced in ASTRO, and CASA does not have a model for Mercury.

\subsubsection{Venus}

Venus is not a common calibrator for IRAM, so it was not considered for an update in 2024 despite  ASTRO and CASA brightness temperature spectra for Venus are significantly different.




\subsection{Giant Planets}

For the giant planets, the main differences of the 2024 models are a
different behavior at low frequencies ($<$100~GHz) and the presence of
absorption lines in the planet atmospheres. We detail the differences
for each planet individually hereafter.

\subsubsection{Uranus}
The 2024 brightness temperature spectrum gives lower values than the \emph{Legacy} model.
Above 130~GHz the 2024 temperatures are 4\% lower than the \emph{Legacy} values.
The offset increases towards lower frequencies, with $\sim$5\% at 120~GHz and $\sim$10\% below 80GHz.

The CASA model shows a different profile, with values in very good agreement with the \emph{Legacy} values below 100~GHz and above 220~GHz, and 5\% lower temperatures between 110 and 200~GHz.

{\bf Uranus is commonly used as a primary flux calibrator at NOEMA. Since the 2024 brightness temperatures are lower than the \emph{Legacy} values, the secondary calibrator fluxes and the antenna efficiencies based on Uranus measurements will be lower. The amplitude of this effect will be of 4\% above 130GHz, and in the 5--10\% range between 80 and 115GHz.}


\subsubsection{Neptune}
The \emph{Legacy} and 2024 temperatures are in good agreement above
130~GHz (less than 2\% offset), except around 230 and 345~GHz, where
the CO atmospheric lines are taken into account in the 2024 models.
The brightness temperature in the 2024 spectrum is $\sim$10\% lower
than the \emph{Legacy} values in these lines.

Below 80~GHz the 2024 temperature values are more than 5\% lower than the \emph{Legacy} values.

The CASA model is very similar to the 2024 spectrum, with offset lower than 1\%.


{\bf The use 2024 brightness temperature spectrum of Neptune will have a small impact on the NOEMA flux calibration, except in the vicinity of atmospheric lines.}



\subsubsection{Jupiter}
Jupiter is the giant planet for which the new models induce the smallest change: the 2024 brightness temperature is slightly lower at frequencies below 100~GHz and above 300~GHz, but the offset remains smaller than 3\%.
We note that the CASA model has a similar shape, but is always 2-3\% lower than the ASTRO (\emph{Legacy} or 2024) values.

Given this small difference and the fact that Jupiter is not used frequently for flux calibration, we can say that the switch to new brightness temperature spectrum in ASTRO will have no impact for the users.



\subsubsection{Saturn}
The 2024 brightness temperature spectra gives lower values than the \emph{Legacy} model.
The difference is on the order of 5\% from 70 to 210~GHz but increases to $\sim$10\% at from 210 to 230~GHz and above 300~GHz.
A Saturn's atmospheric line makes the offset bigger in the 230-300~GHz range (30\% offset around 270GHz).


In CASA the choice was made not to use Saturn as a flux calibrator (because the rings make the modelling of an average brightness temperature difficult)

Like Jupiter, Saturn is in general not used as a flux calibrator, so the impact of the new model should remain limited.





\begin{figure}
\includegraphics[width=14cm]{Mars-evol-80.eps}
\caption{Comparison of the  brightness temperature of Mars at 80~GHz as a found by the ASTRO \emph{Legacy} (black line) and the CASA models (red line). Top panel shows the expected temperature behavior over a decade, while middle and bottom panels show zoom on a period of one year and one month, respectively.}
\label{FigMarsT80}
\end{figure}

\begin{figure}
\includegraphics[width=14cm]{Mars-evol-150.eps}
\caption{Same as Fig.~\Ref{FigMarsT80} but for 150~GHz.}
\label{FigMarsT150}
\end{figure}

\begin{figure}
\includegraphics[width=14cm]{Mars-evol-230.eps}
\caption{Same as Fig.~\Ref{FigMarsT80} but for 230~GHz.}
\label{FigMarsT230}
\end{figure}

\begin{figure}
\includegraphics[width=14cm]{Mars-evol-360.eps}
\caption{Same as Fig.~\Ref{FigMarsT80} but for 360~GHz.}
\label{FigMarsT360}
\end{figure}










\section{Summary of the impact on instrument calibration}

\begin{figure}
\includegraphics[width=14cm]{Summary.eps}
\caption{
Brightness temperature ratio as a function of frequency for Mars  ($Tb_{CASA}/Tb_{Legacy}$), Uranus  ($Tb_{2024}/Tb_{Legacy}$) and Neptune ($Tb_{2024}/Tb_{Legacy}$), the planets that are the most used for IRAM flux calibration. Mars is representend with two lines in order to represent the minimum and maximum values reached during the time variation of its brightness temperature.
}
\label{FigSum}
\end{figure}

The use of 2024 planet models for data calibration will have its strongest effect at  lower end of the IRAM frequency ranges (below 100~GHz), where the brightness temperatures of the 3 mostly used planets are on the order of 5--10\% lower in the 2024 model than in the \emph{Legacy} version (see Fig.~\ref{FigSum}).

The difference between the 2024 and \emph{Legacy} models is also large in the presence of planet atmospheric lines. This concerns mainly the CO lines in Neptune, where the 2024 model give lower brightness temperatures than the \emph{Legacy} one. However, the presence of these lines is well known and in general taken into account.

In these two  cases,  2024 brightness temperatures are lower than the \emph{Legacy} values, so the instruments are more performant than it was deduced using the \emph{Legacy} ASTRO models (higher beam efficiency $B_{Eff}$ at 30m and lower secondary calibrator flux, and so Jy/K efficiencies, for NOEMA).
This will have to be taken into account into calibration software like MRTCAL ($B_{Eff}$) and CLIC PIPELINE (lightcurves of secondary calibrators).

The Mars brightness light curve is globally below the \emph{Legacy} Mars brightness up to almost 300~GHz, where it gets symmetric around the \emph{Legacy} values ($\pm$5\%).

Above 120~GHz Netpune and Uranus 2024 models give temperatures  closer  to the \emph{Legacy} values, with offset of $\sim$2--4\%, which is smaller than the model absolute accuracy ($\sim$5\%).


There are two cases in which the brightness temperature is higher in the 2024 models: i/ for Mars, at frequencies above 230~GHz, during half of the long term variation, where the offset can reach 5\%; ii/ for Neptune, at frequencies above 130~GHz, but with a very small effect of 2\%. 





\section{Recommendation}
The availability of precise brightness models for planets makes the
original implementation in ASTRO obsolete.

We propose to update ASTRO using recent models for giant planets and Mars, the most commonly used planets in the framework of flux calibration of IRAM observatories.

We will use state-of-the-art models developped by experts of planetary science and radio astronomy and already used for the calibration of world-class facilities like Herschel, ALMA, or the EHT.

After the update, the planet fluxes as computed by ASTRO will be significantly different than the ones used before, especially at low frequencies.

To avoid confusion and make sure the most precise models are used, the 2024 scheme will replace the original one.
If some users want to make a comparison of the \emph{Legacy} vs 2024 results, they will have to use different gildas releases.

The update will be done in two steps: 1st the giant planets, then Mars, once the integration of the full physical model in ASTRO will be ready.


\section*{Acknowlegements}

We thank Raphael Moreno and Emmanuel Lellouch for providing us with the state-of-the-art planet models, and IRAM colleagues for their useful inputs, especially Carsten Kramer for the description of the 30m data calibration.

  

  

%
% COMMENTS FROM ASTRONOMERS 2024
%
%To check this,
%would it be possible to test the changes in the flux model of Mars to
%see how the light curves of MWC and LkHa at 86 GHz would compare over
%the past 10 years ?
%--> NOT MY JOB
%
%
%%Figure 8 shows that a) the ASTRO Legacy 2020-2030 brightness
%%temperatures of Mars at 230 GHz are on average above 200 K, and
%%slightly higher than CASA.
%%This contrasts with what is shown in Figure
%%4 i.e. what time period do the brightness temperatures shown in
%%Figures 1 to 5 apply to?
%--> Fig 1 to 5  01-jan-2010
%
%%Are you sure that Figure 10 shows TB(2024)/TB(ASTRO) for Mars, and not
%%TB(CASA)/TB(ASTRO) ?
%-->right, corrected
%
%%No news about the asteroids ? 
%--> NO
%
%
%- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
%CK comments
%
%A list of reference is yet missing, in particular for the Orton and
%Moreno models.
%--> R.Moreno did not privde references
%

%%Sec.1: Calibration of 30m data relies on good models of the primary
%%calibrators. Main primary calibrators are Uranus and Mars. For
%%heterodyne observations (with EMIR), antenna temperatures (TA* scale)
%%are determined from regular hot/cold/sky calibrations done about every
%%15min. The scaling to main beam temperatures is done offline using
%%existing tables of main beam and forward efficiencies. The staff is
%%observing primary and secondary calibrators a few times per year.  The
%%main beam efficiencies (together with aperture efficiencies, Jy/K
%%factors, HPBWs) are derived from night time observations of primary
%%calibrators. Observers are informed about the current efficiencies via
%%a public wiki page: https://publicwiki.iram.es/Iram30mEfficiencies
%%. Updated numbers are in preparation.  The flux stability of primary
%%and secondary calibrators for 1985-2005 is discussed in
%%https://arxiv.org/abs/0801.4452 . On the other hand, NIKA2 data are
%%calibrated to the mJy/beam scale by regular observations of primary
%%and secondary calibrators every few hours (see
%%https://www.iram.fr/~gildas/dist/piic-dafs.pdf and Perotto et
%%al. 2020). ASTRO has been used for the planetary brightness
%%temperatures and source fluxes.
%%

%%“With the progress of instrumentation….” I would phrase this
%%differently: The accuracy of models of the primary calibrators have
%%greatly improved over the past years to about 5% absolute accuracy,
%%rendering it important to update ASTRO.

%%Sec.2: what is the background temperature Tbg in eq.1 ?

%%Figure 10: For Mars, I suggest to separate the variation of brightness
%%temperature with heliocentric distance from its intrinsic variability
%%by introducing a mean T_B for a mean distance of 1.524: T_B = <T_B>
%%sqrt (1.524au/R) with R in au (Ulich 1981) and then plot the mean
%%T_B. This way we will see the intrinsic variability of Mars, to
%%compare with the Rudy model used by CASA, and we will also see the
%%difference with the legacy temperatures as function of frequency.
%%--> a whole bunch of computation/plot -- maybe later
%%
%%
%%Beam coupling needs to be taken into account when Mars becomes large (up to
%%25”).
%%--> for flux, here we compare models
%%
%%
%%-----------------------------
%%JB Dec24
%%- Tbg --> use more recent value ?
%%- Find better references
%%- Do Carsten's suggested plot ?
%%- Synchroniser implementation avec MK 
%
%
%



%%\begin{itemize}
%%\item One.
%%\item Two.
%%\item Three.
%%\end{itemize}
%%
\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
