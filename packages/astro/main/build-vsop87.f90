!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
program binary
  use gildas_def
  use gkernel_interfaces
  use gbl_format
  !---------------------------------------------------------------------
  !  Create Ephemeris file from original ASCII files.
  !     The output file is a direct access file of blocksize 512 Bytes
  !     The first block contains an information about the system on
  !     which the binary file was created
  !---------------------------------------------------------------------
  ! Global
  integer mfr,mterm,mbodies,mcoord,malpha
  parameter (mfr=10000,mterm=8000,mbodies=10,mcoord=3,malpha=5)
  ! number of frequencies
  integer n_freq
  ! number of terms for each series, and pointer
  integer n_term(mbodies,mcoord,0:malpha), i_rec_n_term
  ! record number for each series, and pointer
  integer i_rec(mbodies,mcoord,0:malpha), i_rec_i_rec
  ! record number for the list of frequencies
  integer i_rec_fr
  ! frequency pointer for each term
  integer i_freq(mterm)
  ! Logical unit number
  integer lun
  ! frequencies
  real*8 fr(mfr)
  ! cosine and sine term for each term
  real*8 f(mterm), g(mterm), cs(mterm), ss(mterm), oldtjj
  !
  common /vsop/ fr, f, g, cs, ss, oldtjj, n_freq, n_term, i_rec,    &
     & i_rec_fr,i_freq, i_rec_i_rec, i_rec_n_term, lun
  save /vsop/
  ! Local
  integer :: lun1, lun2, if, n3, n4, n5, nseries, ir, i, irecord
  integer :: nterms, ntt, nss, i_file, nf, ier, nl
  logical :: used
  real*8 :: a, b, c
  character(len=8) :: source
  character(len=4) :: cfile
  character(len=132) :: chain
  character(len=7) :: check
  character(len=filename_length) :: argum, vfile
  character(len=20) :: file(mbodies)
  data file /'vsop87/merc.eph','vsop87/venu.eph','vsop87/eart.eph',  &
             'vsop87/mars.eph','vsop87/jupi.eph','vsop87/satu.eph',  &
             'vsop87/uran.eph','vsop87/nept.eph','vsop87/emb.eph',   &
             'vsop87/sun.eph'/
  !
  select case (command_argument_count())
  case (0)
    file = 'vsop87.bin'
  case (1)
    call get_command_argument(1,argum,nl,ier)
    if (ier.ne.0)  call sysexi(fatale)
    ! Parse input pattern
    call sic_parse_file(argum,' ','.bin',vfile)
  case default
    print *,'F-VSOP87, Program expects 0 or 1 argument'
    call sysexi(fatale)
  end select
  !
  ! Delete file if it already exists
  if (gag_inquire(vfile,lenc(vfile)).eq.0) call gag_delete(vfile)
  !
  ! Get logical units and open file
  ier = sic_getlun (lun1)
  if (mod(ier,2).eq.0) then
    call gagout ("E-VSOP87, Error-1")
    call sysexi(ier)
  endif
  ier = sic_getlun (lun2)
  if (mod(ier,2).eq.0) then
    call gagout ("E-VSOP87, Error-2")
    call sysexi(ier)
  endif
  open(unit=lun2,file=vfile,form='unformatted',status='new',access='direct',  &
       recl=128*facunf,iostat=ier)
  if (ier.ne.0) then
    call gagout ("E-VSOP87, An error occured while creating file "//vfile)
    call sysexi(ier)
  endif
  !
  nss = 0
  ntt = 0
  n_freq = 0
  i_rec_fr = 117
  irecord = i_rec_fr
  !
  ! Loop on files for each "Body"
  do if=1,mbodies
    nf = lenc(file(if))
    open(unit=lun1,file=file(if)(1:nf),form='formatted',status='old')  ! access='read')
    print '(2A)', 'Opened file ', file(if)(1:nf)
    nseries = 0
    nterms = 0
    ier = 0
    do while (.true.)
      read(lun1,'(A)',end=800) chain
      check = chain(2:7)
      source = chain(23:29)
      read(chain(42:42),'(I1)') n3
      read(chain(60:60),'(I1)') n4
      read(chain(61:67),'(I7)') n5
      !!! G95 Compiler Bug
      !!!            read (chain,1000,iostat=ier) check,source,n3,n4,n5
      !!! 1000       format(1x,A6,10x,i1,4x,A7,12x,I1,17x,I1,I7)
      !         1         2         3         4         5         6
      !123456789 123456789 123456789 123456789 123456789 123456789 123456789
      !1X, A6,      10X,I,4X,    A7,        12X,I,            17X,I,   I7,
      ! VSOP87 VERSION A1    MERCURY   VARIABLE 1 (XYZ)       *T**0   1449 TERMS
      if (check.ne.'VSOP87') goto 800
      nseries = nseries+1
      ! save the number of terms
      n_term(if,n3,n4) = n5
      do ir=1,n5
        read(lun1,1001) a,b,c
        1001           format(79x,f18.11,f14.11,f20.11)
        nterms = nterms+1
        f(ir) = a*cos(b)
        g(ir) = -a*sin(b)
        used = .false.
        i = 0
        do while (.not.used .and. i.lt.n_freq)
          i = i+1
          used = c.eq.fr(i)
        enddo
        if (used) then
          i_freq(ir) = i
        else
          n_freq = n_freq+1
          fr(n_freq) = c
          i_freq(ir) = n_freq
        endif
      enddo
      i_rec(if,n3,n4) = irecord
      call wseries (lun2,irecord,n5,i_freq,ier)
      if (ier.ne.0) goto 999
      call wseries (lun2,irecord,2*n5,f,ier)
      if (ier.ne.0) goto 999
      call wseries (lun2,irecord,2*n5,g,ier)
      if (ier.ne.0) goto 999
    enddo
    800     close(lun1)
    print '(2A, I4, A, 2(I6, A))', 'End of file ',file(if),  &
                                   nseries,' series,',       &
                                   nterms, ' terms,',        &
                                   n_freq, ' frequencies'
    ntt = ntt+nterms
    nss = nss+nseries
  enddo
  irecord = 2
  i_rec_n_term = irecord
  call wseries(lun2,irecord,180,n_term,ier)
  if (ier.ne.0) goto 999
  i_rec_i_rec = irecord
  call wseries(lun2,irecord,180,i_rec,ier)
  if (ier.ne.0) goto 999
  i_rec_fr = irecord
  call wseries(lun2,irecord,n_freq*2,fr,ier)
  if (ier.ne.0) goto 999
  write(6,*) 'irecord = ', irecord
  !
  ! Write first record
  chain = 'VSOP87 ephemeris in binary format'
  !
  ! Get Hardware of the machine, and transfer it to an integer
  call gdf_getcod(cfile)
  call chtoby(cfile,i_file,4)
  write(unit=lun2,rec=1,iostat=ier) i_file, i_rec_n_term, i_rec_i_rec,  &
                                    i_rec_fr, n_freq, chain
  if (ier.ne.0) then
    print *,'Ier ',ier
  endif
  print '(A, A20, I4, A, 2(I6, A))', 'Total of    ','',  &
                                      nss, ' series,',   &
                                      ntt, ' terms,',    &
                                      n_freq, ' different frequencies'
  close(lun2)
  stop 'Success'
  999  print *,'Ier = ',ier
  stop 'PROBLEM '
end program binary
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wseries(lun,irecord,nw,w,ier)
  integer :: lun                    !
  integer :: irecord                !
  integer :: nw                     !
  integer :: w(nw)                  !
  integer :: ier                    !
  ! Local
  integer :: buffer(128), iw, nrec, i
  !
  iw = 1
  nrec = nw/128
  if (nrec.ge.1) then
    do i=1, nrec
      call r4tor4(w(iw),buffer,128)
      write(lun,rec=irecord,iostat=ier) buffer
      if (ier.ne.0) return
      iw = iw + 128
      irecord = irecord+1
    enddo
  endif
  call r4tor4(w(iw),buffer,nw-iw+1)
  do i=nw-iw+2,128
    buffer(i) = 0
  enddo
  write(lun,rec=irecord,iostat=ier) buffer
  if (ier.ne.0) return
  irecord = irecord+1
end
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

