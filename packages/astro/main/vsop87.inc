      integer mfr,mterm,mbodies,mcoord,malpha
      parameter (mfr=10000,mterm=8000,mbodies=10,mcoord=3,malpha=5)
! number of frequencies
      integer n_freq
! number of terms for each series, and pointer
      integer n_term(mbodies,mcoord,0:malpha), i_rec_n_term
! record number for each series, and pointer
      integer i_rec(mbodies,mcoord,0:malpha), i_rec_i_rec
! record number for the list of frequencies
      integer i_rec_fr
! frequency pointer for each term
      integer i_freq(mterm)
! Logcal unit number
      integer lun
! frequencies
      real*8 fr(mfr)
! cosine and sine term for each term
      real*8 f(mterm), g(mterm), cs(mterm), ss(mterm), oldtjj
!
      common /vsop/ fr, f, g, cs, ss, oldtjj, n_freq, n_term, i_rec,    &
     & i_rec_fr,i_freq, i_rec_i_rec, i_rec_n_term, lun
      save /vsop/
