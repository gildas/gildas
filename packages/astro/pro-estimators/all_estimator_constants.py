########################################################################
# 30m time and/or sensitivity estimators.
# Constants and parameters shared by all receivers.
########################################################################

import numpy

###############
class Constant:
  def __init__(self):
    self.clight = 2.99792458e8         # [m/s] Light velocity
    self.kbolt = 1.38e-23              # [J/K] Boltzman constant
    self.sec2rad = numpy.pi/180./3600. # 1 arcsec in radians
    self.min2rad = numpy.pi/180./60.   # 1 arcmin in radians
    self.deg2rad = numpy.pi/180.       # 1 degree in radians
    self.min2sec = 60.                 # 1 minute in seconds
    self.hr2sec  = 3600.               # 1 hour in seconds
    self.mjy2jy  = 1./1000.            # 1 mJy in Jy

# Instantiate
constant = Constant()

############
class Units:
  def __init__(self):
    # Here arrays of units (as strings) are defined. Associated value
    # must be the index where to find the units in the array.
    self.winter = 0
    self.summer = 1
    self.season = ["winter","summer"]
    #
    self.kms = 0
    self.mhz = 1
    self.khz = 2
    self.reso = ["km/s","MHz","kHz"]
    #
    self.hr = 0
    self.min = 1
    self.sec = 2
    self.timefactor = [3600.,60.,1.]  # Multiplicative factor from timeunit to sec
    self.time = ["hr","min","sec"]
    #
    self.mkelvin = 0
    self.kelvin = 1
    self.krmsfactor = [.001,1.]  # Multiplicative factor from rmsunit to K
    self.krmsname = ["mK","K"]
    #
    self.mjy_beam = 0
    self.jy_beam = 1
    self.jyrmsfactor = [.001,1.]  # Multiplicative factor from rmsunit to Jy/beam
    self.jyrmsname = ["mJy/beam","Jy/beam"]
    #
    self.arcsec = 0
    self.arcmin = 1
    self.degree = 2
    self.anglefactor = [constant.sec2rad,constant.min2rad,constant.deg2rad]  # Multiplicative factor from angleunit to radians
    self.anglename = ["arcsec","arcmin","degree"]
    #
    self.arcsec2 = 0
    self.arcmin2 = 1
    self.degree2 = 2
    self.areafactor = [constant.sec2rad**2,constant.min2rad**2,constant.deg2rad**2]  # Multiplicative factor from areaunit to radians**2
    self.areaname = ["arcsec^2","arcmin^2","degree^2"]

# Instantiate
units = Units()

#############
class Switch:
  def __init__(self):
    self.fre = 0
    self.pos = 1
    self.wob = 2
    self.mode = ["fsw","psw","wsw"]

# Instantiate
switch = Switch()

###########
class Cst:
  # FixMe: Move in Telescope()?
  def __init__(self):
    self.lsb = +1
    self.usb = -1
