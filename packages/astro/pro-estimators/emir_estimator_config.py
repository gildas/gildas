########################################################################
# 30m time and/or sensitivity estimator for EMIR.
# Configuration file. Customize here the parameters and defaults
# for all values used in the estimator.
########################################################################

from hetero_estimator_constants import hobskinds,polars
from all_estimator_constants import units,switch
from all_estimator_config import season,deadline,pwvval,pwvfrac

# === Identifier ===
name = "emir"  # String. Estimator identifier, do not change!

# === Defaults ===
# Customize here the default parameters which appear in the form on the
# webpage
verbose_tsys = False     # Bool  [True|False]
verbose_otf = False      # Bool  [True|False]
sensitivity = True       # Bool  [True|False], compute sensitivity (True) or time (False)
obskind = hobskinds.tra  # Code  [tra|otf], heterodyne observing kind
area = 8.                # Float [arcmin^2], map area for OTF observations
areaunit = units.arcmin2 # Code  [arcsec2,arcmin2,degree2], area unit
el = 45.                 # Float [deg]
res = 0.25               # Float [resunit], resolution in 'resunit'
resunit = units.kms      # Code  [kms|mhz|khz], resolution unit
teltime = 1.             # Float [timeunit], integration time
timeunit = units.hr      # Code  [hr|min|sec], time unit
rms = 10.                # Float [rmsunit], RMS sensitivity
rmsunit = units.mkelvin  # Code  [mkelvin|kelvin], rms unit
freq = 110.              # Float [GHz], observing frequency

# === Telescope parameters ===
teleff = 0.5       # Float [], global telescope efficiency
el_min = 10.       # Float [deg], minimum observable elevation (CK, 25.7.09)
el_max = 90.       # Float [deg], maximum observable elevation
temp = 290.        # Float [K], cabin temperature
speceff = 1./1.15  # Float [], spectrometer efficiency

# === OTF parameters ===
sampling = [4.,2.5]  # 2 floats [dump per beam, row per beam], Parallel and
                     # perpendicular sampling
maxrate = 2.         # Float [Hz], Maximum dumping rate
maxdtoff = 120.      # Float [sec], Maximum time between 2 OFFs (system stability)

# === Back-end parameters ===
fresmin = 3.3e-3  # Float [MHz]. Lowest resolution allowed. VESPA goes down
                  # to 3.3 kHz for EMIR.
vresmax = 500.    # Float [km/s]. Highest resolution allowed.

# === Front-end parameters ===
# Customize here the EMIR parameters
nband = 4                              # Int [], number of bands
rname = ["e090","e150","e230","e330"]  # n Strings, band names
freqs = [70.,120.,190.,275.,380.]      # n+1 Floats [GHz], rough limits of each bands (??? CK, 25.7.09) 
ifreq = [7.84,7.84,7.84,7.84]          # n Floats [GHz], Intermediate Frequencies
# Exact frequency ranges: see CK email to gildas-30m@iram.fr on 09-feb-2015
#                H            V           H+V
frange = [ [[ 71., 119.],[ 71., 119.],[ 71., 119.]], \
           [[122., 186.],[122., 186.],[122., 186.]], \
           [[200., 275.],[200., 275.],[200., 275.]], \
           [[275., 377.],[275., 377.],[275., 377.]] ]
                                       # n*3*2 Floats [GHz], frequency ranges:
                                       # - per band (horizontal), and
                                       # - per polar -2 individual and 1 combined- (vertical),
                                       # e.g. frange[0][1] is the range for 1st band and
                                       # 2nd polar.
feff = [0.95,0.93,0.92,0.86]           # n Floats [], forward efficiencies (CK, 25.7.09 Ref.: wiki)
gain = [0.05,0.05,0.05,0.05]           # n Floats [], image gains (CK, 25.7.09 Ref.: wiki)
#
# Warning above 177 GHz
e150_info  = "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n" + \
            " !!!  WARNING: the e150 frequency range can be extended  !!!\n" + \
            " !!!  above 177 GHz with good performances but the       !!!\n" + \
            " !!!  calibration around the atmospheric water line at   !!!\n" + \
            " !!!  183.31 GHz would require special care.             !!!\n" + \
            " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
# Warning below 202 GHz
e230_info  = "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n" + \
            " !!!  WARNING: when using simultaneously E0 and E2,     !!!\n" + \
            " !!!  dichroic losses lead to strongly increased noise  !!!\n" + \
            " !!!  temperatures for E2 frequencies below about 202   !!!\n" + \
            " !!!  GHz (cf. EMIR homepage).                          !!!\n" + \
            " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
# Warning above 350 GHz
e330_info  = "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n" + \
            " !!!  WARNING: the standard frequency range of the   !!!\n" + \
            " !!!  e330 band is extended to 377 GHz with the YIG  !!!\n" + \
            " !!!  Local Oscillator. Observations above 350 GHz   !!!\n" + \
            " !!!  are run on a shared-risk basis.                !!!\n" + \
            " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
freq_info = [ [], \
              [ [177.,186.,e150_info] ], \
              [ [200.,202.,e230_info] ], \
              [ [350.,377.,e330_info] ] ]  # n lists (one per receiver): each list
                    # contains 0 or more lists, each being the frequency range + the
                    # associated message.
#
# Polarizations
ipol = polars.hv                       # Code [h|v|hv|h_v|all] ("h_v" for h, v,
                                       #                        "all" for h, v, and h+v)
npixperpol = 1                         # Int [], number of pixels per polarization
# Below the names for each polarization and each band. The single polarizarion names
# will never be used, unless you set the above 'ipol' to another value.
#                H       V     H+V
polnames = [ ["e090H","e090V","e090"], \
             ["e150H","e150V","e150"], \
             ["e230H","e230V","e230"], \
             ["e330H","e330V","e330"] ]  # n*3 strings, polarization names

# Switching modes
swmodes = [ switch.fre,switch.pos ]
