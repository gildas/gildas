
import numpy
import all_estimator_utils as utils
from all_estimator_constants import Cst,Switch,switch,units,constant
from hetero_estimator_constants import HeteroObsKinds,polars,grid_factor

########################################
# Functions for heterodyne computations

def hetero_revision():
  string = "$Revision$"
  return string[1:-1]

def list_polars(inconf):
  # Return a list of all the combination of polarizations
  # we may want. This is thinked to be independent of the
  # declaration of the global object "polars".
  ifront = 0  # FixMe! Must be the band identifier!
  #
  l = list(polars.name)  # Just a copy (of good size)
  l[polars.h]   = inconf.polnames[ifront][polars.h]
  l[polars.v]   = inconf.polnames[ifront][polars.v]
  l[polars.hv]  = inconf.polnames[ifront][polars.hv]
  l[polars.h_v] = inconf.polnames[ifront][polars.h]+", "+ \
                  inconf.polnames[ifront][polars.v]
  l[polars.all] = inconf.polnames[ifront][polars.h]+", "+ \
                  inconf.polnames[ifront][polars.v]+", "+ \
                  inconf.polnames[ifront][polars.hv]
  return l


#####################################
# Classes for Heterodyne estimations

cst = Cst()

class Time:
  # Compute the estimations simulteaneously for the different combinations
  # of H, V and H+V polarizations (1, several, or all)
  def __init__(self,inconf):
    self.do   = Do()    # Inputs
    self.done = Done(inconf.swmodes)  # Outputs
    self.conf = inconf
    self.tel  = Telescope(inconf)  # Attach the Telescope parameters
    self.warnings = Warnings()
    # Instance
    self.estimates = None
    # Set defaults
    self.default()
  #
  def default(self):
    # Default a Time instance.
    self.do.default(self.conf)  # Default the parameters
  #
  def compute(self):
    self.warnings.reset()
    self.do2done()
    self.thirtym_parameters()
    #
    if (self.done.front.ipol==polars.h):  # H only
      self.estimates = [Estimator_1polar(self.done,polars.h)]
      #
    elif (self.done.front.ipol==polars.v):  # V only
      self.estimates = [Estimator_1polar(self.done,polars.v)]
      #
    elif (self.done.front.ipol==polars.hv): # H+V only
      self.estimates = [Estimator_1polar(self.done,polars.hv)]
      #
    elif (self.done.front.ipol==polars.h_v):  # H or V
      self.estimates = [Estimator_1polar(self.done,polars.h), \
                        Estimator_1polar(self.done,polars.v)]
      #
    elif (self.done.front.ipol==polars.all): # H or V or H+V
      self.estimates = [Estimator_1polar(self.done,polars.h), \
                        Estimator_1polar(self.done,polars.v), \
                        Estimator_1polar(self.done,polars.hv)]
    #
    for inst in self.estimates:
      inst.compute(self.tel)
  #
  def do2done(self):
    #
    # Basic checks
    if (self.do.sensitivity and self.do.teltime <= 0.):
      raise ValueError, "Telescope time must be positive (is "+repr(self.do.teltime)+")"
    if (not self.do.sensitivity and self.do.rms <= 0.):
      raise ValueError, "RMS sensitivity must be positive (is "+repr(self.do.rms)+")"
    if (self.do.obskind==self.tel.obskinds.otf and self.do.area <= 0.):
      raise ValueError, "Map area must be positive (is "+repr(self.do.area)+")"
    if (self.do.el<self.conf.el_min or self.do.el>self.conf.el_max):
      raise ValueError, "Elevation out of range ["+repr(self.conf.el_min)+"-"+repr(self.conf.el_max)+"]: "+repr(self.do.el)
    if (self.do.season != self.conf.season):
      self.warnings.add("The season doesn't match the next deadline")
    #
    self.done.get(self.do)
    #
    # Convert input resolution into fres and vres
    if (self.do.resunit == units.kms):
      self.done.vres = self.do.res
      self.done.fres_computation()
    elif (self.do.resunit == units.mhz):
      self.done.fres = self.do.res
      self.done.vres_computation()
    elif (self.do.resunit == units.khz):
      self.done.fres = self.do.res/1000.
      self.done.vres_computation()
    else:
      raise ValueError, "Unknown spectrometer resolution unit: "+repr(self.do.resunit)
    if (self.done.fres<self.tel.back.fresmin):
      raise ValueError, "The frequency resolution must be > "+repr(self.tel.back.fresmin)+" MHz"
    if (self.done.vres>self.tel.back.vresmax):
      self.warnings.add("The velocity resolution must be < "+repr(self.tel.back.vresmax)+" km/s")
    #
    # Convert input map area into arcsec^2
    self.done.otf.area.target[:,:] = self.do.area*units.areafactor[self.do.areaunit]/constant.sec2rad**2
    #
    # Convert input time or rms into internal units i.e. seconds or Kelvin resp.
    if (self.done.sensitivity):
      self.done.time.tel[:,:] = self.do.teltime*units.timefactor[self.do.timeunit] # Convert to seconds
    else:
      self.done.rms.k[:,:] = self.do.rms*units.krmsfactor[self.do.rmsunit] # Convert to K
    #
  def thirtym_parameters(self):
    # Antenna specifications
    self.done.airmass = utils.airmass(self.done.el)
    self.done.beam = 2460./self.done.freq
    self.done.lambdaa = 1.e-6*constant.clight/self.done.freq  # in mm
    self.done.jyperk = 2*(constant.kbolt*1.e32)*numpy.pi*(self.done.beam*constant.sec2rad)**2/(4.*numpy.log(2.)*self.done.lambdaa**2)
    self.done.jyperk = 1./self.done.jyperk
    #
    # Weather specifications
    self.done.temp = self.conf.temp
    self.done.pwvval[:] = self.conf.pwvval[self.done.season]
    self.done.pwvfrac[:] = self.conf.pwvfrac[self.done.season]
    #
    # Backend specifications
    self.done.speceff = self.conf.speceff
    #
  def display(self):
    # Print formatted results to STDOUT
    for l in self.format_results():
      print l
  #
  def print_tofile(self,outfile):
    # Print formatted results to the input file pointer
    if (type(outfile)==file):
      for l in self.format_results():
        outfile.write(l+"\n")
    else:
      raise TypeError,"Invalid output type "+repr(type(outfile))+" in Time.print_tofile()"
  #
  def format_results(self):
    # Format the results and return a list of strings
    lines = []
    #
    # Debug mode
    if (self.done.debug):
      lines += [" *** DEBUG DISPLAY *** "]
      lines += [" *** Estimator heterodyne engine "+hetero_revision()+" ***" ]
    #
    # Title
    title = "IRAM-30m (" + units.season[self.do.season] + "|" + self.tel.obskinds.mode[self.do.obskind]
    if (self.do.sensitivity):
      title += "|sensitivity)"
    else:
      title += "|time)"
    title += " for "+self.conf.deadline+" deadline"
    lines += [title]
    #
    lines += ["------------------------------"]
    lines += ["   User-defined parameters:"]
    lines += ["------------------------------"]
    #
    # Reminder of user inputs
    lines += ["%-27s%9.3f %s" % ("Frequency:", self.done.freq, "GHz")]  # Receiver not resolved at this point
    lines += ["%-27s%9.3f %s" % ("Wavelength:", self.done.lambdaa, "mm")]
    lines += [""]
    if (self.done.debug):
      lines += ["%-27s%9.3f %s" % ("Conversion factor:", self.done.jyperk, "K per Jy/beam")]
    lines += ["%-27s%9.3f %s" % ("Frequency resolution:", self.done.fres, "MHz")]
    lines += ["%-27s%9.3f %s" % ("Velocity resolution:", self.done.vres, "km/s")]
    lines += [""]
    lines += ["%-27s%9.1f"    % ("Spectrometer efficiency:", self.done.speceff)]
    if (self.do.obskind==self.tel.obskinds.tra):
      lines += ["%-27s%9.1f %s" % ("Spatial resolution:", self.done.beam, "arcsec")]
    else:
      lines += ["%-27s%9.1f %s" % ("Spatial resolution:", self.done.beam*numpy.sqrt(grid_factor), "arcsec")]
    lines += ["%-27s%9.1f %s" % ("Typical elevation:", self.done.el, "deg")]
    #
    # HTML text
    if (self.do.html):
      html_text  = "We propose to observe during the "
      html_text += units.season[self.do.season] + " season with "
      html_text += self.tel.front.name.upper() + " using the "
      html_text += self.tel.obskinds.mode[self.do.obskind]
      html_text += " OBSMODE observing mode"
      if (self.do.obskind==self.tel.obskinds.otf):
        html_text += " to cover a field of view of "
        html_text += repr(self.do.area) + " " + units.areaname[self.do.areaunit]
      html_text += ". For a typical elevation of "
      html_text += repr(self.do.el) + " degrees, an observing frequency of "
      html_text += repr(self.do.freq) + " GHz and a spectrometer resolution of "
      html_text += repr(self.do.res) + " " + units.reso[self.do.resunit] + ", the "
      if (self.do.sensitivity):
        html_text += "sensitivity estimator tells us that we will reach a sensitivity "
        html_text += "of RESULTVAL RESULTUNIT in "
        html_text += repr(self.do.teltime) + " " + units.time[self.do.timeunit]
      else:
        html_text += "time estimator tells us that we will need a telescope time of "
        html_text += "RESULTVAL RESULTUNIT to reach a sensitivity of "
        html_text += repr(self.do.rms) + " " + units.krmsname[self.do.rmsunit]
      html_text += " with PWVNAME conditions (PWVVAL mm of pwv,"
      html_text += " Tsys TSYSVAL K[Ta*] mean per pixel)."
    else:
      html_text = None
    #
    # Results for each Estimator_1polar instance
    lines += [""]
    lines += ["------------------------------"]
    lines += ["   Computed results:"]
    lines += ["------------------------------"]
    for inst in self.estimates:
      lines += inst.format_results_1polar(self.tel,html_text)
      lines += ["------------------------------"]
    #
    # Add the prefix
    prefix = " "
    for i in range(len(lines)):
      # if (out[i]) != ""):
      lines[i] = prefix+lines[i]
    #
    return lines

#######################
class Estimator_1polar:
  # Class which handles the N pixels for only 1 of the cases H, V or H+V
  # polarizations
  def __init__(self,indone,inpol):
    self.done = indone.copy()
    self.done.front.ipol = inpol
    # Instances
    self.estimates = None  # All pixels
    self.estimate  = None  # 1 equivalent pixel
  #
  def compute(self,tel):
    if (self.done.front.ipol==polars.h):
      npix = tel.front.npixperpol
      polperpix = [polars.h for i in xrange(npix)]
      numperpix = [i+1      for i in xrange(npix)]
    elif (self.done.front.ipol==polars.v):
      npix = tel.front.npixperpol
      polperpix = [polars.v for i in xrange(npix)]
      numperpix = [i+1      for i in xrange(npix)]
    elif (self.done.front.ipol==polars.hv):
      npix = 2*tel.front.npixperpol
      polperpix = numpy.empty(npix,dtype=numpy.int)
      polperpix[:tel.front.npixperpol] = polars.h
      polperpix[tel.front.npixperpol:] = polars.v
      numperpix = numpy.empty(npix,dtype=numpy.int)
      numperpix[:tel.front.npixperpol] = [i+1 for i in xrange(tel.front.npixperpol)]
      numperpix[tel.front.npixperpol:] = [i+1 for i in xrange(tel.front.npixperpol)]
    else:
      raise ValueError,"Polarization can be H, V or H+V only (was "+repr(self.done.front.ipol)+")"
    self.estimates = [Estimator_1(self.done,polperpix[i],numperpix[i]) \
                      for i in xrange(npix)]  # 1 per pixel
    self.estimate = Estimator_1(self.done,self.done.front.ipol,0)
    #
    self.compute_mean_instance(tel)
    self.estimate.compute(tel)
  #
  def compute_mean_instance(self,tel):
    # Read the opacity table once (if required):
    if (work.table and work.opacities is None):
      f = open(work.taufile,'rb')  # Binary file
      a = numpy.fromfile(f,dtype=numpy.float32)
      # A is now a flat array of Nfreq x (1 freq + 4 opacities). Reshape
      # as 2D:
      nfreq = a.shape[0]/5
      work.opacities = a.reshape(nfreq,5)  # This is an alias, not a copy in memory
      f.close()
    #
    # Compute Tsys for each pixel
    for inst in self.estimates:
      inst.done.front.frontend_parameters(inst.done.freq,tel)
      inst.done.tsys_computation(work.opacities)
    #
    # Compute average Tsys
    self.estimate.done.front.frontend_parameters(self.estimate.done.freq,tel)
    self.compute_average_tsys(work.opacities)
  #
  def compute_average_tsys(self,taus):
    # For equivalent (mean) pixels only!
    if (taus is not None):
      if (self.done.season==units.summer):
        first = 2
        last = 5  # Upper bound is excluded
      else:
        first = 1
        last = 4  # Upper bound is excluded
      #
      iloc = utils.compute_location(taus[:,0],self.done.front.fre.sig)
      self.estimate.done.taus[:] = taus[iloc[0],first:last]
      #
      self.estimate.done.tsky[:] = 250.*(1.-numpy.exp(-self.estimate.done.taus*self.estimate.done.airmass))
      for iwater in xrange(work.mwater):
        sum=0.
        for inst in self.estimates:
          sum += 1./(inst.done.tsys[iwater]**2)
        self.estimate.done.tsys[iwater] = numpy.sqrt(len(self.estimates)/sum)
    else:
      raise NotImplementedError,"Tau computation not available, table mode only"
  #
  def format_results_1polar(self,tel,html):
    edone = self.estimate.done
    lines = [ utils.format_string("Receiver:",edone.front.pname,right=True) ]
    #
    if (not edone.front.available):
      # Receiver not available at this frequency. Skipped.
      lines += ["Frequency: "+repr(edone.freq)+" GHz out of range of "+edone.front.pname]
      return lines
    if (edone.front.freq_info is not None):
      lines += [edone.front.freq_info]
    #
    # Number of polarizations
    lines += [ utils.format_integer_9d("Number of polarizations:",polars.npol[self.done.front.ipol]) ]
    lines += [ utils.format_integer_9d("Number of pixels per pol.:",tel.front.npixperpol) ]
    #
    if (edone.verb.tsys):
      trecs = [ inst.done.front.pixel.trec for inst in self.estimates ]
      lines += [utils.format_float_91f_mmm("Trec (min/med/max):",trecs,"K")]
      lines += ["%-27s%9.3f %s" % ("Cabin temperature:", edone.temp, "K")]
      lines += ["%-27s%9.3f %s" % ("Signal frequency:", edone.front.fre.sig, "GHz")]
      lines += ["%-27s%9.3f %s" % ("Image frequency:", edone.front.fre.ima, "GHz")]
      lines += ["%-27s%9.3f"    % ("Image gain:", edone.front.gain)]
      lines += ["%-27s%9.3f"    % ("Forward efficiency:", edone.front.feff)]
      lines += ["%-27s%9.3f"    % ("Airmass:", edone.airmass)]
    #
    lines += self.estimate.format_results_1(tel,html)
    return lines

##################
class Estimator_1:
  # Elemental class which computes 1 value from 1 set of input
  # parameters.
  def __init__(self,indone,ipol,pnum):
    self.done = indone.copy()
    self.done.front.pixel.pol = ipol
    self.done.front.pixel.num = pnum
  #
  def compute(self,tel):
    if (self.done.sensitivity):
      self.done.tel2sig(tel)
    self.done.estimation()
    if (not self.done.sensitivity):
      self.done.sig2tel(tel)
  #
  def format_results_1(self,tel,html):
    out = []
    ifsw = self.done.ifsw
    ipsw = self.done.ipsw
    nppp = tel.front.npixperpol
    isarray = nppp>1
    isotf = self.done.obskind==tel.obskinds.otf
    #
    if (isotf):
      out += [""]
      out += ["%-29s%7.1f %s" % ("Map area:", self.done.otf.area.target[0,0]/60.**2, "arcmin^2")]
      #
      if (isarray):
        if (numpy.any(self.done.otf.prob.northo) or numpy.any(self.done.otf.prob.tchunk)):
          out += [""]
          out += ["Area too small"]
          out += ["  => Increase map size or use tracked observations"]
          return out
      #
      if (self.done.verb.otf):
        out += self.format_results_1_otf(tel)
      #
      if (self.done.otf.nbeam[0,0]<6):
        out += [""]
        out += ["Less than 6 independent beams per pixel in covered area"]
        out += ["  => Increase map size or use tracked observations"]
        return out
    #
    out += [""]
    if (self.done.sensitivity):
      out += ["Time repartition:            Telescope   On       Off"]
      if (isarray and isotf):
        out[-1] += "     Edges"
      # Frequency switching
      if (ifsw is not None):
        values = [ self.done.time.tel[ifsw,0]   /units.timefactor[self.done.timeunit],
                   self.done.time.tot.on[ifsw,0]/units.timefactor[self.done.timeunit],
                   " " ]
        if (isarray and isotf):
          values += [ self.done.time.edge[ifsw,0]/units.timefactor[self.done.timeunit] ]
        out += [ utils.format_float_93f("  - Frequency switching (fsw)",
                values,units.time[self.done.timeunit])]
      # Position switching
      if (ipsw is not None):
        values = [ self.done.time.tel[ipsw,0]    /units.timefactor[self.done.timeunit],
                   self.done.time.tot.on[ipsw,0] /units.timefactor[self.done.timeunit],
                   self.done.time.tot.off[ipsw,0]/units.timefactor[self.done.timeunit] ]
        if (isarray and isotf):
          values += [ self.done.time.edge[ipsw,0]/units.timefactor[self.done.timeunit] ]
        out += [utils.format_float_93f("  - Position  switching (psw)",
                values,units.time[self.done.timeunit])]
      # Wobbler switching
      if (ipsw is not None and not isotf):
        values = [ self.done.time.tel[ipsw,0]    /units.timefactor[self.done.timeunit],
                   self.done.time.tot.on[ipsw,0] /units.timefactor[self.done.timeunit],
                   self.done.time.tot.off[ipsw,0]/units.timefactor[self.done.timeunit] ]
        out += [utils.format_float_93f("  - Wobbler   switching (wsw)",
                values,units.time[self.done.timeunit])]
    else:
      out += ["%-27s%9.3f %s" % ("rms noise:", self.done.rms.k[0,0]/units.krmsfactor[self.done.rmsunit], units.krmsname[self.done.rmsunit]+"[Ta*]")]
    #
    # Computed outputs
    out += [""]
    out += [units.season[self.done.season].capitalize()+" conditions:           Excellent   Good   Average"]
    out += [utils.format_float_91f("Precipitable water:",self.done.pwvval,"mm")]
    out += [utils.format_float_91f("Tsys (mean per pixel):",self.done.tsys,"K[Ta*]")]
    if (self.done.sensitivity):
      if (ifsw is not None):
        if (isotf and self.done.otf.prob.speed[ifsw,0]):
          out += ["FSW: Telescope speed limit reached"]
          out += ["FSW:    => Increase telescope time or decrease map size"]
          if (self.done.debug):
            out += [self.format_rms("FSW",self.done.rms.k[ifsw,:],False,html)]
        else:
          out += [self.format_rms("FSW",self.done.rms.k[ifsw,:],False,html)]
      #
      if (ipsw is not None):
        if (isotf and self.done.otf.prob.speed[ipsw,0]):
          out += ["PSW: Telescope speed limit reached"]
          out += ["PSW:    => Increase telescope time or decrease map size"]
          if (self.done.debug):
            out += [self.format_rms("PSW",self.done.rms.k[ipsw,:],False,html)]
            if (not isotf):
              out += [self.format_rms("WSW",self.done.rms.k[ipsw,:],False,html)]
        elif (isotf and self.done.otf.prob.cover[ipsw,0]):
          out += ["PSW: Can not reach a full coverage"]
          out += ["PSW:    => Increase telescope time or decrease map size"]
          if (self.done.debug):
            out += [self.format_rms("PSW",self.done.rms.k[ipsw,:],False,html)]
            if (not isotf):
              out += [self.format_rms("WSW",self.done.rms.k[ipsw,:],False,html)]
        else:
          out += [self.format_rms("PSW",self.done.rms.k[ipsw,:],True,html)]
          if (not isotf):
            out += [self.format_rms("WSW",self.done.rms.k[ipsw,:],False,html)]
    #
    else:
      if (ifsw is not None):
        out += ["Frequency switching times:"]
        if (isotf and self.done.otf.prob.speed[ifsw,0]):
          out += ["FSW: Telescope speed limit reached"]
          out += ["FSW:    => Increase sensitivity and/or decrease map size"]
          if (self.done.debug):
            out += [utils.format_time("  - On source:",self.done.time.tot.on[ifsw])]
            if (isarray and isotf):
              out += [utils.format_time("  - Edges:",self.done.time.edge[ifsw])]
            out += [self.format_time("FSW","  - Telescope:",self.done.time.tel[ifsw],False,html)]
        else:
          out += [utils.format_time("  - On source:",self.done.time.tot.on[ifsw])]
          if (isarray and isotf):
            out += [utils.format_time("  - Edges:",self.done.time.edge[ifsw])]
          out += [self.format_time("FSW","  - Telescope:",self.done.time.tel[ifsw],False,html)]
      #
      if (ipsw is not None):
        if (isotf):
          out += ["Position switching times:"]
          obsmode = "PSW"
        else:
          out += ["Position/Wobbler switching times:"]
          obsmode = "PSW/WSW"
        #
        if (isotf and self.done.otf.prob.speed[ipsw,0]):
          out += ["PSW: Telescope speed limit reached (i.e. number of coverages < 1)"]
          out += ["PSW:    => Increase sensitivity and/or decrease map size"]
          if (self.done.debug):
            out += [utils.format_time("  - On  source:",self.done.time.tot.on[ipsw])]
            out += [utils.format_time("  - Off source:",self.done.time.tot.off[ipsw])]
            if (isarray and isotf):
              out += [utils.format_time("  - Edges:",self.done.time.edge[ipsw])]
            out += [self.format_time(obsmode,"  - Telescope:",self.done.time.tel[ipsw],False,html)]
        else:
          out += [utils.format_time("  - On  source:",self.done.time.tot.on[ipsw])]
          out += [utils.format_time("  - Off source:",self.done.time.tot.off[ipsw])]
          if (isarray and isotf):
            out += [utils.format_time("  - Edges:",self.done.time.edge[ipsw])]
          out += [self.format_time(obsmode,"  - Telescope:",self.done.time.tel[ipsw],True,html)]
    #
    return out
    #
  def format_html(self,obsmode,val,unit,isdefault,html_text):
    #
    if (html_text is None):
      r0 = "%9.1f" % (val[0])
      r1 = "%9.1f" % (val[1])
      r2 = "%9.1f" % (val[2])
    else:
      #
      warning  = "Requesting PWVNAME weather conditions in "
      warning += units.season[self.done.season]
      warning += " reduces the chance of being scheduled at the telescope "
      warning += "as only PWVFRAC% of the semester is dedicated to such "
      warning += "projects. A clear justification of the good weather "
      warning += "conditions is required in the proposal."
      #
      # Excellent
      t0 = html_text.replace("OBSMODE",obsmode)
      t0 = t0.replace("RESULTVAL","%.1f" % (val[0]))
      t0 = t0.replace("RESULTUNIT",unit)
      t0 = t0.replace("PWVNAME","excellent")
      t0 = t0.replace("PWVVAL",str(self.done.pwvval[0]))
      t0 = t0.replace("TSYSVAL","%.1f" % (self.done.tsys[0]))
      w0 = warning.replace("PWVNAME","excellent")
      w0 = w0.replace("PWVFRAC","%.0f" % (self.done.pwvfrac[0]))
      r0 = '<result text="%s" warning="%s">%9.1f</result>' % (t0,w0,val[0])
      #
      # Good
      t1 = html_text.replace("OBSMODE",obsmode)
      t1 = t1.replace("RESULTVAL","%.1f" % (val[1]))
      t1 = t1.replace("RESULTUNIT",unit)
      t1 = t1.replace("PWVNAME","good")
      t1 = t1.replace("PWVVAL",str(self.done.pwvval[1]))
      t1 = t1.replace("TSYSVAL","%.1f" % (self.done.tsys[1]))
      w1 = warning.replace("PWVNAME","good")
      w1 = w1.replace("PWVFRAC","%.0f" % (self.done.pwvfrac[1]))
      r1 = '<result text="%s" warning="%s">%9.1f</result>' % (t1,w1,val[1])
      #
      # Average
      t2 = html_text.replace("OBSMODE",obsmode)
      t2 = t2.replace("RESULTVAL","%.1f" % (val[2]))
      t2 = t2.replace("RESULTUNIT",unit)
      t2 = t2.replace("PWVNAME","average")
      t2 = t2.replace("PWVVAL",str(self.done.pwvval[2]))
      t2 = t2.replace("TSYSVAL","%.1f" % (self.done.tsys[2]))
      if (isdefault):
        # Always chose average conditions as default.
        r2 = '<result text="%s" default="1">%9.1f</result>' % (t2,val[2])
      else:
        r2 = '<result text="%s">%9.1f</result>' % (t2,val[2])
    #
    return (r0,r1,r2)
    #
  def format_rms(self,obsmode,d,isdefault,html_text):
    #
    if (html_text is None):
      micro = "micro"
      larrow = " rms noises:        --->"
      rarrow = " <---"
    else:
      micro = "&#181"
      larrow = " rms noises:            "
      rarrow = ""
    #
    minrms = min(d)
    if (minrms<1.e-3):
      factor = 1.e6
      unit = micro+"K[Ta*]"
    elif (minrms<1.):
      factor = 1.e3
      unit = "mK[Ta*]"
    else:
      factor = 1.
      unit = " K[Ta*]"
    #
    (r0,r1,r2) = self.format_html(obsmode,d*factor,unit,isdefault,html_text)
    return "%-27s%s%s%s %s" % (obsmode+larrow, r0, r1, r2, unit+rarrow)
  #
  def format_time(self,obsmode,prefix,t_in,isdefault,html_text):
    t = utils.format_tolist(t_in)
    #
    if (html_text is None):
      larrow = "         --->"
      rarrow = " <---"
    else:
      larrow = "             "
      rarrow = ""
    #
    mintime = min(t)
    if (mintime<60.):
      factor = 1.
      unit = "sec"
    elif (mintime<3600.):
      factor = 60.
      unit = "min"
    else:
      factor = 3600.
      unit = "hr"
    #
    t_scaled = [x/factor for x in t]
    (r0,r1,r2) = self.format_html(obsmode,t_scaled,unit,isdefault,html_text)
    #
    if (utils.python_check_version_26()):  # Use new formatting scheme
      listformat = "{0:27s}{1:s}{2:s}{3:s} {4}"
      return listformat.format(prefix+larrow,r0,r1,r2,unit+rarrow)
    else:                                  # Use old formatting scheme
      return "%-27s%s%s%s %s" % (prefix+larrow,r0,r1,r2,unit+rarrow)
    #
  def format_results_1_otf(self,tel):
    # Verbose OTF
    out = []
    nppp = tel.front.npixperpol
    isarray = nppp>1
    #
    out += ["%-29s%7.1f %s" % ("Maximum scanning speed:", self.done.otf.speedmax[0,0], "arcsec^2/s per pixel")]
    if (self.done.sensitivity):
      if (isarray):
        out += self.format_results_1_otf_tel2sig_array(tel)
      else:
        out += self.format_results_1_otf_tel2sig_single(tel)
    else:
      if (isarray):
        out += self.format_results_1_otf_sig2tel_array(tel)
      else:
        out += self.format_results_1_otf_sig2tel_single(tel)
    #
    return out
    #
  def format_results_1_otf_tel2sig_array(self,tel):
    out = []
    ifsw = self.done.ifsw
    ipsw = self.done.ipsw
    nppp = tel.front.npixperpol
    #
    if (ifsw is not None):
      out += ["Frequency switching parameters:"]
      out += ["%-29s%7.1f %s" % ("  - Chunk duration:", self.done.otf.tchunk[ifsw,0], "seconds")]
      out += self.format_check_achunk(ifsw,0)
      out += ["%-29s%7.1f %s" % ("  - Chunk area:", self.done.otf.area.chunk[ifsw,0]/60.**2, "arcmin^2")]
      out += ["%-29s%.1f%s%.1f%s%.1f%s%.0f%s%.1f%s%.1f%s" % ("  - Chunk geometry:",
              self.done.otf.northo[ifsw,0]*self.done.otf.dortho[ifsw,0]," x ",
              self.done.otf.northo[ifsw,0]*self.done.otf.dortho[ifsw,0]*self.done.otf.aspect[ifsw,0],
              " arcsec^2 (",self.done.otf.northo[ifsw,0]*self.done.otf.dortho[ifsw,0],
                        "=",self.done.otf.northo[ifsw,0],
                        "x",self.done.otf.dortho[ifsw,0],
                        ", a=",self.done.otf.aspect[ifsw,0],")"  )]
      out += ["%-29s%7.2f"    % ("  - Edge efficiency:", self.done.otf.edge_eff[ifsw,0])]
      out += ["%-29s%7.1f %s" % ("  - Edge area:", self.done.otf.area.edge[ifsw,0]/60.**2, "arcmin^2")]
      out += ["%-29s%7.1f %s" % ("  - Total area:", self.done.otf.area.total[ifsw,0]/60.**2, "arcmin^2")]
      out += ["%-29s%7.1f %s%d%s%.1f%s" % ("  - Number of indep. beams:", self.done.otf.nbeam[ifsw,0]*nppp, "(= ",nppp," x ",self.done.otf.nbeam[ifsw,0],")")]
      out += ["%-29s%7.1f %s" % ("  - Actual scanning speed:", self.done.otf.speedlin[ifsw,0], "arcsec/s")]
      out += ["%-29s%7.1f %s" % ("",                           self.done.otf.speed[ifsw,0], "arcsec^2/s per pixel")]
      out += ["%-29s%7.1f %s" % ("",                      nppp*self.done.otf.speed[ifsw,0], "arcsec^2/s per array")]
      out += ["%-29s%7.1f %s" % ("  - ON  duration per beam:", self.done.time.bea.on[ifsw,0], "seconds")]
      if (self.done.debug):
        out += ["%-29s%7.1f %s" % ("  - OFF duration per beam:", self.done.time.bea.off[ifsw,0], "seconds")]
        out += ["%-29s%7.1f"    % ("  - Number of ON per OFF:", self.done.otf.nonperoff[ifsw,0])]
    #
    if (ipsw is not None):
      out += ["Position switching parameters:"]
      out += ["%-29s%7.1f %s" % ("  - Chunk duration:", self.done.otf.tchunk[ipsw,0], "seconds")]
      out += self.format_check_achunk(ipsw,0)
      out += ["%-29s%7.1f %s" % ("  - Chunk area:", self.done.otf.area.chunk[ipsw,0]/60.**2, "arcmin^2")]
      out += ["%-29s%.1f%s%.1f%s%.1f%s%.0f%s%.1f%s%.1f%s" % ("  - Chunk geometry:",
              self.done.otf.northo[ipsw,0]*self.done.otf.dortho[ipsw,0]," x ",
              self.done.otf.northo[ipsw,0]*self.done.otf.dortho[ipsw,0]*self.done.otf.aspect[ipsw,0],
              " arcsec^2 (",self.done.otf.northo[ipsw,0]*self.done.otf.dortho[ipsw,0],
                        "=",self.done.otf.northo[ipsw,0],
                        "x",self.done.otf.dortho[ipsw,0],
                        ", a=",self.done.otf.aspect[ipsw,0],")"  )]
      out += ["%-29s%7.2f"    % ("  - Edge efficiency:", self.done.otf.edge_eff[ipsw,0])]
      out += ["%-29s%7.1f %s" % ("  - Edge area:", self.done.otf.area.edge[ipsw,0]/60.**2, "arcmin^2")]
      out += ["%-29s%7.1f %s" % ("  - Total area:", self.done.otf.area.total[ipsw,0]/60.**2, "arcmin^2")]
      out += ["%-29s%7.1f %s%d%s%.1f%s" % ("  - Number of indep. beams:", self.done.otf.nbeam[ipsw,0]*nppp, "(= ",nppp," x ",self.done.otf.nbeam[ipsw,0],")")]
      out += ["%-29s%7.1f %s" % ("  - Actual scanning speed:", self.done.otf.speedlin[ipsw,0], "arcsec/s")]
      out += ["%-29s%7.1f %s" % ("",                           self.done.otf.speed[ipsw,0], "arcsec^2/s per pixel")]
      out += ["%-29s%7.1f %s" % ("",                      nppp*self.done.otf.speed[ipsw,0], "arcsec^2/s per array")]
      out += ["%-29s%7.1f%12s%.1f%s%.1f %s" % ("  - ON  duration per beam:", self.done.time.bea.on[ipsw,0],  " seconds (= ", self.done.otf.ncover[ipsw,0], " x ", self.done.time.cov.on[ipsw,0],  "seconds)")]
      out += ["%-29s%7.1f%12s%.1f%s%.1f %s" % ("  - OFF duration per beam:", self.done.time.bea.off[ipsw,0], " seconds (= ", self.done.otf.ncover[ipsw,0], " x ", self.done.time.cov.off[ipsw,0], "seconds)")]
      out += self.format_check_toff(ipsw,0)
      out += ["%-29s%7.1f %s" % ("  - Submap duration:", self.done.otf.tsubmap[ipsw,0], "seconds")]
      out += ["%-29s%7.1f %s" % ("  - Submap area:", self.done.otf.area.submap[ipsw,0]/60.**2, "arcmin^2 per pixel")]
      out += ["%-29s%7.1f"    % ("  - Number of submaps:", self.done.otf.nsubmap[ipsw,0])]
      out += ["%-29s%7.1f"    % ("  - Number of coverages:", self.done.otf.ncover[ipsw,0])]
      out += ["%-29s%7.1f"    % ("  - Number of ON per OFF:", self.done.otf.nonperoff[ipsw,0])]
    #
    return out
    #
  def format_results_1_otf_tel2sig_single(self,tel):
    out = []
    ifsw = self.done.ifsw
    ipsw = self.done.ipsw
    nppp = tel.front.npixperpol
    #
    out += ["%-29s%7.1f" % ("Number of independent beams:", self.done.otf.nbeam[0,0]*nppp)]
    #
    if (ifsw is not None):
      out += ["Frequency switching parameters:"]
      out += ["%-29s%7.1f %s" % ("  - Actual scanning speed:", self.done.otf.speedlin[ifsw,0], "arcsec/s")]
      out += ["%-29s%7.1f %s" % ("",                           self.done.otf.speed[ifsw,0], "arcsec^2/s")]
      out += ["%-29s%7.1f %s" % ("  - ON  duration per beam:", self.done.time.bea.on[ifsw,0], "seconds")]
      if (self.done.debug):
        out += ["%-29s%7.1f %s" % ("  - OFF duration per beam:", self.done.time.bea.off[ifsw,0], "seconds")]
        out += ["%-29s%7.1f"    % ("  - Number of ON per OFF:", self.done.otf.nonperoff[ifsw,0])]
    #
    if (ipsw is not None):
      out += ["Position switching parameters:"]
      out += ["%-29s%7.1f %s" % ("  - Actual scanning speed:", self.done.otf.speedlin[ipsw,0], "arcsec/s")]
      out += ["%-29s%7.1f %s" % ("",                           self.done.otf.speed[ipsw,0], "arcsec^2/s")]
      out += ["%-29s%7.1f%12s%.1f%s%.1f %s" % ("  - ON  duration per beam:", self.done.time.bea.on[ipsw,0],  " seconds (= ", self.done.otf.ncover[ipsw,0], " x ", self.done.time.cov.on[ipsw,0],  "seconds)")]
      out += ["%-29s%7.1f%12s%.1f%s%.1f %s" % ("  - OFF duration per beam:", self.done.time.bea.off[ipsw,0], " seconds (= ", self.done.otf.ncover[ipsw,0], " x ", self.done.time.cov.off[ipsw,0], "seconds)")]
      out += self.format_check_toff(ipsw,0)
      out += ["%-29s%7.1f %s" % ("  - Submap duration:", self.done.otf.tsubmap[ipsw,0], "seconds")]
      out += ["%-29s%7.1f %s" % ("  - Submap area:", self.done.otf.area.submap[ipsw,0]/60.**2, "arcmin^2")]
      out += ["%-29s%7.1f"    % ("  - Number of submaps:", self.done.otf.nsubmap[ipsw,0])]
      out += ["%-29s%7.1f"    % ("  - Number of coverages:", self.done.otf.ncover[ipsw,0])]
      out += ["%-29s%7.1f"    % ("  - Number of ON per OFF:", self.done.otf.nonperoff[ipsw,0])]
    #
    return out
    #
  def format_results_1_otf_sig2tel_array(self,tel):
    out = []
    ifsw = self.done.ifsw
    ipsw = self.done.ipsw
    nppp = tel.front.npixperpol
    #
    if (ifsw is not None):
      out += ["Frequency switching parameters:"]
      out += ["%-29s%7.1f %s" % ("  - Chunk duration:", self.done.otf.tchunk[ifsw,0], "seconds")]
      out += self.format_check_achunk(ifsw,0)
      out += ["%-29s%7.1f %s" % ("  - Chunk area:", self.done.otf.area.chunk[ifsw,0]/60.**2, "arcmin^2")]
      out += ["%-29s%.1f%s%.1f%s%.1f%s%.0f%s%.1f%s%.1f%s" % ("  - Chunk geometry:",
              self.done.otf.northo[ifsw,0]*self.done.otf.dortho[ifsw,0]," x ",
              self.done.otf.northo[ifsw,0]*self.done.otf.dortho[ifsw,0]*self.done.otf.aspect[ifsw,0],
              " arcsec^2 (",self.done.otf.northo[ifsw,0]*self.done.otf.dortho[ifsw,0],
                        "=",self.done.otf.northo[ifsw,0],
                        "x",self.done.otf.dortho[ifsw,0],
                        ", a=",self.done.otf.aspect[ifsw,0],")"  )]
      out += ["%-29s%7.2f"    % ("  - Edge efficiency:", self.done.otf.edge_eff[ifsw,0])]
      out += ["%-29s%7.1f %s" % ("  - Edge area:", self.done.otf.area.edge[ifsw,0]/60.**2, "arcmin^2")]
      out += ["%-29s%7.1f %s" % ("  - Total area:", self.done.otf.area.total[ifsw,0]/60.**2, "arcmin^2")]
      out += ["%-29s%7.1f %s%d%s%.1f%s" % ("  - Number of indep. beams:", self.done.otf.nbeam[ifsw,0]*nppp, "(= ",nppp," x ",self.done.otf.nbeam[ifsw,0],")")]
      out += [utils.format_float_91f("  - Actual scanning speed:",self.done.otf.speedlin[ifsw],"arcsec/s")]
      out += [utils.format_float_91f("",                          self.done.otf.speed[ifsw],"arcsec^2/s per pixel")]
      out += [utils.format_float_91f("",                     nppp*self.done.otf.speed[ifsw],"arcsec^2/s per array")]
      out += [utils.format_time("  - ON  duration per beam:", self.done.time.bea.on[ifsw])]
      if (self.done.debug):
        out += [utils.format_time("  - OFF duration per beam:", self.done.time.bea.off[ifsw])]
        out += [utils.format_float_91f("  - Number of ON per OFF:",self.done.otf.nonperoff[ifsw])]
    #
    if (ipsw is not None):
      out += ["Position switching parameters:"]
      out += ["%-29s%7.1f %s" % ("  - Chunk duration:", self.done.otf.tchunk[ipsw,0], "seconds")]
      out += self.format_check_achunk(ipsw,0)
      out += ["%-29s%7.1f %s" % ("  - Chunk area:", self.done.otf.area.chunk[ipsw,0]/60.**2, "arcmin^2")]
      out += ["%-29s%.1f%s%.1f%s%.1f%s%.0f%s%.1f%s%.1f%s" % ("  - Chunk geometry:",
              self.done.otf.northo[ipsw,0]*self.done.otf.dortho[ipsw,0]," x ",
              self.done.otf.northo[ipsw,0]*self.done.otf.dortho[ipsw,0]*self.done.otf.aspect[ipsw,0],
              " arcsec^2 (",self.done.otf.northo[ipsw,0]*self.done.otf.dortho[ipsw,0],
                        "=",self.done.otf.northo[ipsw,0],
                        "x",self.done.otf.dortho[ipsw,0],
                        ", a=",self.done.otf.aspect[ipsw,0],")"  )]
      out += ["%-29s%7.2f"    % ("  - Edge efficiency:", self.done.otf.edge_eff[ipsw,0])]
      out += ["%-29s%7.1f %s" % ("  - Edge area:", self.done.otf.area.edge[ipsw,0]/60.**2, "arcmin^2")]
      out += ["%-29s%7.1f %s" % ("  - Total area:", self.done.otf.area.total[ipsw,0]/60.**2, "arcmin^2")]
      out += ["%-29s%7.1f %s%d%s%.1f%s" % ("  - Number of indep. beams:", self.done.otf.nbeam[ipsw,0]*nppp, "(= ",nppp," x ",self.done.otf.nbeam[ipsw,0],")")]
      out += [utils.format_float_91f("  - Actual scanning speed:",self.done.otf.speedlin[ipsw],"arcsec/s")]
      out += [utils.format_float_91f("",                          self.done.otf.speed[ipsw],"arcsec^2/s per pixel")]
      out += [utils.format_float_91f("",                     nppp*self.done.otf.speed[ipsw],"arcsec^2/s per array")]
      out += [utils.format_time("  - ON  duration per beam:", self.done.time.bea.on[ipsw])]
      out += [utils.format_time("  - OFF duration per beam:", self.done.time.bea.off[ipsw])]
      out += self.format_check_toff(ipsw,0)  # Should check the 3 values!
      out += [utils.format_time("  - Submap duration:",          self.done.otf.tsubmap[ipsw])]
      out += [utils.format_float_91f("  - Submap area:",         self.done.otf.area.submap[ipsw]/60.**2,"arcmin^2 per pixel")]
      out += [utils.format_float_91f("  - Number of submaps:",   self.done.otf.nsubmap[ipsw])]
      out += [utils.format_float_91f("  - Number of coverages:", self.done.otf.ncover[ipsw])]
      out += [utils.format_float_91f("  - Number of ON per OFF:",self.done.otf.nonperoff[ipsw])]
    #
    return out
    #
  def format_results_1_otf_sig2tel_single(self,tel):
    out = []
    ifsw = self.done.ifsw
    ipsw = self.done.ipsw
    nppp = tel.front.npixperpol
    #
    out += ["%-29s%7.1f" % ("Number of independent beams:", self.done.otf.nbeam[0,0]*nppp)]
    #
    if (ifsw is not None):
      out += ["Frequency switching parameters:"]
      out += [utils.format_float_91f("  - Actual scanning speed:",self.done.otf.speedlin[ifsw],"arcsec/s")]
      out += [utils.format_float_91f("",                          self.done.otf.speed[ifsw],"arcsec^2/s")]
      out += [utils.format_time("  - ON  duration per beam:", self.done.time.bea.on[ifsw])]
      if (self.done.debug):
        out += [utils.format_time("  - OFF duration per beam:", self.done.time.bea.off[ifsw])]
        out += [utils.format_float_91f("  - Number of ON per OFF:",self.done.otf.nonperoff[ifsw])]
    #
    if (ifsw is not None):
      out += ["Position switching parameters:"]
      out += [utils.format_float_91f("  - Actual scanning speed:",self.done.otf.speedlin[ipsw],"arcsec/s")]
      out += [utils.format_float_91f("",                          self.done.otf.speed[ipsw],"arcsec^2/s")]
      out += [utils.format_time("  - ON  duration per beam:", self.done.time.bea.on[ipsw])]
      out += [utils.format_time("  - OFF duration per beam:", self.done.time.bea.off[ipsw])]
      out += self.format_check_toff(ipsw,0)  # Should check the 3 values!
      out += [utils.format_time("  - Submap duration:",          self.done.otf.tsubmap[ipsw])]
      out += [utils.format_float_91f("  - Submap area:",         self.done.otf.area.submap[ipsw]/60.**2,"arcmin^2")]
      out += [utils.format_float_91f("  - Number of submaps:",   self.done.otf.nsubmap[ipsw])]
      out += [utils.format_float_91f("  - Number of coverages:", self.done.otf.ncover[ipsw])]
      out += [utils.format_float_91f("  - Number of ON per OFF:",self.done.otf.nonperoff[ipsw])]
    #
    return out
    #
  def format_check_toff(self,iswitch,iwater):
    if (self.done.otf.prob.toff[iswitch,iwater] and self.done.debug):
      return ["    WARNING: OFF duration per beam < 10.0 sec"]
    else:
      return []
    #
  def format_check_achunk(self,iswitch,iwater):
    if (self.done.otf.prob.achunk[iswitch,iwater] and self.done.debug):
      return ["    WARNING: Chunk duration decreased to match target area."]
    else:
      return []

#########
class Do:
  def __init__(self):
    self.verb = Verb()
    self.sensitivity = False
    self.season = 0
    self.obskind = 0
    self.resunit = 0
    self.timeunit = 0
    self.rmsunit = 0
    self.el = 0.
    self.freq = 0.
    self.res = 0.
    self.teltime = 0.
    self.rms = 0.
    self.area = 0.
    self.areaunit = 0
    self.ipol = 0
    self.debug = False  # Debug mode for developpers
    self.html = True  # HTML mode for the web page
  #
  def default(self,inconf):
    # Default a Do instance. Read from configuration file.
    self.ipol        = inconf.ipol
    self.verb.tsys   = inconf.verbose_tsys
    self.verb.otf    = inconf.verbose_otf
    self.sensitivity = inconf.sensitivity
    self.season      = inconf.season
    self.obskind     = inconf.obskind
    self.area        = inconf.area
    self.areaunit    = inconf.areaunit
    self.el          = inconf.el
    self.res         = inconf.res
    self.resunit     = inconf.resunit
    self.teltime     = inconf.teltime
    self.timeunit    = inconf.timeunit
    self.rms         = inconf.rms
    self.rmsunit     = inconf.rmsunit
    self.freq        = inconf.freq
    # self.debug not customizable through the config file.
    # OTF optional parameters:
    if (hasattr(inconf,"nsubscan")):  self.nsubscan = inconf.nsubscan
    if (hasattr(inconf,"delta")):     self.delta    = inconf.delta
    if (hasattr(inconf,"edge_eff_min")): self.edge_eff_min = inconf.edge_eff_min
    if (hasattr(inconf,"tchunk_min")): self.tchunk_min = inconf.tchunk_min

###########
class Verb:
  def __init__(self):
    self.tsys = False
    self.otf = False
    #
  def copy(self):
    outverb = Verb()
    outverb.tsys = self.tsys
    outverb.otf  = self.otf
    return outverb

###########
class Work:
  def __init__(self):
    self.interpolate = True
    self.mwater = 3
    # Opacities
    self.table = False  # Reading the opacities from table file?
    self.taufile = utils.make_abs_path(__file__,'tau_table.bin')
    self.opacities = None  # The opacity table read in memory

work = Work()  # Instantiate as a global
work.table = True
work.opacities = None

###########
class Done:
  def __init__(self,swmodes):
    mwater = work.mwater
    # Switching modes
    self.swmodes = list(swmodes)
    self.mswitch = len(swmodes)
    try:
      self.ifsw = swmodes.index(switch.fre)
    except:
      self.ifsw = None  # Not activated
    try:
      self.ipsw = swmodes.index(switch.pos)
    except:
      self.ipsw = None  # Not activated
    self.verb = Verb()
    self.sensitivity = False
    self.otf = DoneOtf(self.mswitch)
    self.season = 0
    self.obskind = 0
    self.pwvval = numpy.empty((mwater),dtype=numpy.float64)
    self.pwvfrac = numpy.empty((mwater),dtype=numpy.float64)
    self.tsys = numpy.empty((mwater),dtype=numpy.float64)
    self.taus = numpy.empty((mwater),dtype=numpy.float64)
    self.tsky = numpy.empty((mwater),dtype=numpy.float64)
    self.time = DoneTime(self.mswitch)
    self.rms = DoneRms(self.mswitch)
    self.freq = 0.
    self.vres = 0.
    self.fres = 0.
    self.beam = 0.
    self.front = DoneFront()
    self.temp = 0.
    self.speceff = 0.
    self.lambdaa = 0.
    self.jyperk = 0.
    self.el = 0.
    self.airmass = 0.
    self.timeunit = 0
    self.rmsunit = 0
    self.debug = False
    #
  def copy(self):
    outdone = Done(self.swmodes)
    outdone.verb        = self.verb.copy()
    outdone.sensitivity = self.sensitivity
    outdone.otf         = self.otf.copy(self.mswitch)
    outdone.season      = self.season
    outdone.obskind     = self.obskind
    outdone.pwvval      = self.pwvval.copy()
    outdone.pwvfrac     = self.pwvfrac.copy()
    outdone.tsys        = self.tsys.copy()
    outdone.taus        = self.taus.copy()
    outdone.tsky        = self.tsky.copy()
    outdone.time        = self.time.copy(self.mswitch)
    outdone.rms         = self.rms.copy(self.mswitch)
    outdone.freq        = self.freq
    outdone.vres        = self.vres
    outdone.fres        = self.fres
    outdone.beam        = self.beam
    outdone.front       = self.front.copy()
    outdone.swmodes     = list(self.swmodes)  # Copy, not alias
    outdone.mswitch     = self.mswitch
    outdone.ifsw        = self.ifsw
    outdone.ipsw        = self.ipsw
    outdone.temp        = self.temp
    outdone.speceff     = self.speceff
    outdone.lambdaa     = self.lambdaa
    outdone.jyperk      = self.jyperk
    outdone.el          = self.el
    outdone.airmass     = self.airmass
    outdone.timeunit    = self.timeunit
    outdone.rmsunit     = self.rmsunit
    outdone.debug       = self.debug
    return outdone
    #
  def get(self,do_instance):
    # Copy a 'Do()' instance into a 'Done()'
    self.front.ipol  = do_instance.ipol
    self.verb        = do_instance.verb
    self.sensitivity = do_instance.sensitivity
    self.season      = do_instance.season
    self.obskind     = do_instance.obskind
    self.resunit     = do_instance.resunit
    self.timeunit    = do_instance.timeunit
    self.rmsunit     = do_instance.rmsunit
    self.el          = do_instance.el
    self.freq        = do_instance.freq
    self.res         = do_instance.res
    self.teltime     = do_instance.teltime
    self.area        = do_instance.area
    self.areaunit    = do_instance.areaunit
    self.debug       = do_instance.debug
    # OTF optional parameters
    if (hasattr(do_instance,"nsubscan")):  self.otf.nsubscan = do_instance.nsubscan
    if (hasattr(do_instance,"delta")):     self.otf.delta = do_instance.delta
    if (hasattr(do_instance,"edge_eff_min")): self.otf.edge_eff_min = do_instance.edge_eff_min
    if (hasattr(do_instance,"tchunk_min")): self.otf.tchunk_min = do_instance.tchunk_min
    #
  def fres_computation(self):
    if (self.vres<0.):
      raise ValueError,"The velocity resolution must be > 0"
    self.fres = 1.e6*self.vres*self.freq/constant.clight # in MHz
    #
  def vres_computation(self):
    self.vres = 1.e-6*constant.clight*self.fres/self.freq # in km/s
    #
  def tsys_computation(self,taus):
    # For real pixels only!
    if (taus is not None):
      if (self.season==units.summer):
        first = 2
        last = 5  # Upper bound is excluded
      else:
        first = 1
        last = 4  # Upper bound is excluded
      #
      iloc = utils.compute_location(taus[:,0],self.front.fre.sig)
      self.taus[:] = [ numpy.interp(self.front.fre.sig,taus[iloc,0],taus[iloc,ipwv])
                       for ipwv in xrange(first,last) ]
      #
      self.tsky[:] = 250.*(1.-numpy.exp(-self.taus*self.airmass))
      self.tsys[:] = numpy.exp(self.taus[:]*self.airmass)*(1+self.front.gain)/self.front.feff
      self.tsys[:] *= self.front.feff*self.tsky[:]+(1-self.front.feff)*self.temp+self.front.pixel.trec
    else:
      raise NotImplementedError,"Tau computation not available, table mode only"
    #
  def tel2sig(self,tel):
    # Generic
    npol = polars.npol[self.front.ipol]
    ifsw = self.ifsw
    ipsw = self.ipsw
    # On-The-Fly ===============================================================
    if (self.obskind == tel.obskinds.otf):
      # OTF generic
      self.otf.otf_init(self.beam,tel,ifsw,ipsw)
      self.time.onoff = self.time.tel*tel.eff
      # OTF frequency switch ---------------------------------------------------
      if (ifsw is not None):
        self.time.tot.on[ifsw]  = self.time.onoff[ifsw]
        self.time.tot.off[ifsw] = self.time.onoff[ifsw]
        self.time.bea.on[ifsw]  = self.time.onoff[ifsw]/self.otf.nbeam[ifsw]
        self.time.bea.off[ifsw] = self.time.onoff[ifsw]/self.otf.nbeam[ifsw]
        self.time.sig[ifsw]     = self.time.onoff[ifsw]/self.otf.nbeam[ifsw]/2.
        self.otf.speed[ifsw]    = self.otf.area.mappix[ifsw]/self.time.onoff[ifsw]
      # OTF position switch ----------------------------------------------------
      if (ipsw is not None):
        # Readjust otf.speed * otf.tsubmap according to nsubmap rounding:
        # self.otf.tsubmap[ipsw]: keep
        self.otf.speed[ipsw] = self.otf.speed[ipsw]*self.otf.nsubmap[ipsw]/numpy.ceil(self.otf.nsubmap[ipsw])
        self.otf.nsubmap[ipsw] = numpy.ceil(self.otf.nsubmap[ipsw])
        self.otf.area.submap[ipsw] = self.otf.speed[ipsw]*self.otf.tsubmap[ipsw]
        # Sigma
        self.time.sig[ipsw] = self.time.onoff[ipsw] / \
                              numpy.square(numpy.sqrt(self.otf.nbeam[ipsw]) + \
                                          numpy.sqrt(self.otf.nsubmap[ipsw]))
        # Derived quantities
        self.otf.nonperoff[ipsw] = self.otf.nbeam[ipsw]/self.otf.nsubmap[ipsw]
        self.otf.ncover[ipsw] = self.time.sig[ipsw]/self.otf.tsubmap[ipsw] * \
                (self.otf.nonperoff[ipsw] + numpy.sqrt(self.otf.nonperoff[ipsw]))
        self.time.cov.on[ipsw] = self.otf.tsubmap[ipsw]/self.otf.nonperoff[ipsw]
        self.time.bea.on[ipsw] = self.otf.ncover[ipsw]*self.time.cov.on[ipsw]
        self.time.tot.on[ipsw] = self.otf.nsubmap[ipsw]*self.otf.nonperoff[ipsw]*self.time.bea.on[ipsw]
        self.time.cov.off[ipsw] = numpy.sqrt(self.otf.nonperoff[ipsw])*self.time.cov.on[ipsw]
        self.time.bea.off[ipsw] = self.otf.ncover[ipsw]*self.time.cov.off[ipsw]
        self.time.tot.off[ipsw] = self.otf.nsubmap[ipsw]*self.time.bea.off[ipsw]
      # OTF generic checks -----------------------------------------------------
      # Only for display
      self.time.edge = (1.-self.otf.edge_eff)*self.time.tel*tel.eff
      self.time.tot.on  *= self.otf.edge_eff
      self.time.tot.off *= self.otf.edge_eff
      # Checks
      self.otf.prob.speed = numpy.where(self.otf.speed>self.otf.speedmax,
                                        True,
                                        self.otf.prob.speed)
      self.otf.prob.cover = numpy.where(self.otf.ncover<1.,
                                        True,
                                        self.otf.prob.cover)
      self.otf.prob.toff  = numpy.where(self.time.bea.off<10.,
                                        True,
                                        self.otf.prob.toff)
      self.otf.finalize(self.beam,tel)
    # Tracked ==================================================================
    elif (self.obskind==tel.obskinds.tra):
      self.time.onoff = self.time.tel*tel.eff
      # Track frequency switch -------------------------------------------------
      if (ifsw is not None):
        self.time.tot.on[ifsw]  = self.time.onoff[ifsw]
        self.time.tot.off[ifsw] = self.time.onoff[ifsw]
        self.time.sig[ifsw]     = self.time.onoff[ifsw]/2.
      # Track position switch --------------------------------------------------
      if (ipsw is not None):
        self.time.tot.on[ipsw]  = self.time.onoff[ipsw]/2.
        self.time.tot.off[ipsw] = self.time.onoff[ipsw]/2.
        self.time.sig[ipsw]     = self.time.onoff[ipsw]/4.
    else:
      raise ValueError,"Unknown observation kind: "+repr(self.obskind)
    #
  def sig2tel(self,tel):
    npol = polars.npol[self.front.ipol]
    ifsw = self.ifsw
    ipsw = self.ipsw
    # On-The-Fly ===============================================================
    if (self.obskind==tel.obskinds.otf):
      # OTF generic
      self.otf.otf_init(self.beam,tel,ifsw,ipsw)
      # OTF frequency switch ---------------------------------------------------
      if (ifsw is not None):
        self.time.onoff[ifsw] = self.time.sig[ifsw]*2.*self.otf.nbeam[ifsw]
        self.otf.speed[ifsw]  = self.otf.area.mappix[ifsw]/self.time.onoff[ifsw]
        # Derived quantities
        self.time.bea.on[ifsw]  = 2.*self.time.sig[ifsw]
        self.time.bea.off[ifsw] = 2.*self.time.sig[ifsw]
        self.time.tot.on[ifsw]  = self.time.onoff[ifsw]
        self.time.tot.off[ifsw] = self.time.onoff[ifsw]
      # OTF position switch ----------------------------------------------------
      if (ipsw is not None):
        # Readjust otf.speed * otf.tsubmap according to nsubmap rounding:
        # self.otf.tsubmap[ipsw]: keep
        self.otf.speed[ipsw] = self.otf.speed[ipsw]*self.otf.nsubmap[ipsw]/numpy.ceil(self.otf.nsubmap[ipsw])
        self.otf.nsubmap[ipsw] = numpy.ceil(self.otf.nsubmap[ipsw])
        self.otf.area.submap[ipsw] = self.otf.speed[ipsw]*self.otf.tsubmap[ipsw]
        # Time
        self.time.onoff[ipsw] = numpy.square(numpy.sqrt(self.otf.nbeam[ipsw]) + \
                                            numpy.sqrt(self.otf.nsubmap[ipsw])) * \
                                self.time.sig[ipsw]
        # Derived quantities
        self.otf.nonperoff[ipsw] = self.otf.nbeam[ipsw]/self.otf.nsubmap[ipsw]
        self.otf.ncover[ipsw] = self.time.sig[ipsw]/self.otf.tsubmap[ipsw] * \
                (self.otf.nonperoff[ipsw] + numpy.sqrt(self.otf.nonperoff[ipsw]))
        self.time.cov.on[ipsw] = self.otf.tsubmap[ipsw]/self.otf.nonperoff[ipsw]
        self.time.bea.on[ipsw] = self.otf.ncover[ipsw]*self.time.cov.on[ipsw]
        self.time.tot.on[ipsw] = self.otf.nsubmap[ipsw]*self.otf.nonperoff[ipsw]*self.time.bea.on[ipsw]
        self.time.cov.off[ipsw] = numpy.sqrt(self.otf.nonperoff[ipsw])*self.time.cov.on[ipsw]
        self.time.bea.off[ipsw] = self.otf.ncover[ipsw]*self.time.cov.off[ipsw]
        self.time.tot.off[ipsw] = self.otf.nsubmap[ipsw]*self.time.bea.off[ipsw]
      # OTF generic ------------------------------------------------------------
      self.time.tel = self.time.onoff/tel.eff
      self.time.tot.on  *= self.otf.edge_eff
      self.time.tot.off *= self.otf.edge_eff
      self.time.edge = (1.-self.otf.edge_eff)*self.time.tel*tel.eff
      self.otf.prob.speed = numpy.where(self.otf.speed>self.otf.speedmax,
                                        True,
                                        self.otf.prob.speed)
      self.otf.prob.speed[ipsw] = numpy.where(self.otf.ncover[ipsw]<1.,
                                              True,
                                              self.otf.prob.speed[ipsw])
      self.otf.prob.cover = numpy.where(self.otf.ncover<1.,
                                        True,
                                        self.otf.prob.cover)
      self.otf.prob.toff  = numpy.where(self.time.bea.off<10.,
                                        True,
                                        self.otf.prob.toff)
      self.otf.finalize(self.beam,tel)
    # Tracked ==================================================================
    elif (self.obskind==tel.obskinds.tra):
      # Generic ----------------------------------------------------------------
      self.time.tot.on  = 2.*self.time.sig
      self.time.tot.off = 2.*self.time.sig
      # Frequency switch -------------------------------------------------------
      if (ifsw is not None):
        self.time.onoff[ifsw] = 2.*self.time.sig[ifsw]
      # Position switch --------------------------------------------------------
      if (ipsw is not None):
        self.time.onoff[ipsw] = 4.*self.time.sig[ipsw]
      # Generic ----------------------------------------------------------------
      self.time.tel = self.time.onoff/tel.eff
    else:
      raise ValueError,"Unknown observation kind: "+repr(self.obskind)
    #
  def estimation(self):
    npol = polars.npol[self.front.ipol]
    #
    if (self.sensitivity):
      for iswitch in range(self.mswitch):
        self.rms.k[iswitch] = self.tsys / (self.speceff * \
                 numpy.sqrt(self.fres*1.e6 * npol * self.time.sig[iswitch]))
        self.rms.k[iswitch] = numpy.where(self.time.sig[iswitch]==0.,
                                          0.,
                                          self.rms.k[iswitch])
    else:
      for iswitch in range(self.mswitch):
        self.time.sig[iswitch] = (self.tsys/(self.speceff*self.rms.k[iswitch]))**2 / \
                 (self.fres*1.e6 * npol)
    #
    # K to Jy      ZZZ 'iswitch' only???
    self.rms.jy[iswitch] = self.rms.k[iswitch]*self.jyperk

##############
class DoneOtf:
  def __init__(self,mswitch):
    mwater = work.mwater
    self.area = Area(mswitch)
    self.prob = Prob(mswitch)
    self.tsubmap   = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.nsubmap   = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.tchunk    = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.ncover    = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.nbeam     = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.nonperoff = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.speed     = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.speedlin  = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.speedmax  = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.edge_eff  = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.northo    = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.dortho    = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.aspect    = numpy.empty((mswitch,mwater),dtype=numpy.float64)
  #
  def otf_init(self,beam,tel,ifsw,ipsw):
    self.area.beam[:,:] = grid_factor*numpy.pi*beam**2/(4.*numpy.log(2.))
    #
    nppp = tel.front.npixperpol
    isarray = nppp>1
    if (isarray):
      # Edge related computations
      if (ifsw is not None):
        self.tchunk[ifsw] = tel.otf.maxdtcal
      if (ipsw is not None):
        self.tchunk[ipsw] = tel.otf.maxdtoff
      self.dortho[:,:] = self.nsubscan*nppp*self.delta
      d_edge = (numpy.sqrt(nppp)-1.)*(1.+self.nsubscan*numpy.sqrt(nppp))*self.delta
      factor = beam*tel.otf.maxrate*self.dortho/self.nsubscan/tel.otf.sampling[0]
      self.area.chunk = factor*self.tchunk
      # Check if Atarget > Achunk, else correct tchunk
      self.prob.achunk = numpy.where(self.area.target<self.edge_eff_min*self.area.chunk,
                                     True,
                                     False)
      self.northo = numpy.where(self.prob.achunk,
                                numpy.floor(numpy.sqrt(self.area.target)/self.dortho),
                                numpy.floor(d_edge/2./self.dortho * (numpy.sqrt(1.+4.*self.area.chunk/d_edge**2) - 1.)))
      self.prob.northo = numpy.where(self.northo==0.,
                                     True,
                                     False)
      self.aspect = numpy.where(self.prob.achunk,
                                self.area.target/numpy.square(self.northo*self.dortho),
                                self.area.chunk/numpy.square(self.northo*self.dortho) - d_edge/(self.northo*self.dortho))
      self.edge_eff = 1./(1.+d_edge/(self.aspect*self.northo*self.dortho))
      area_chunk_new = self.area.target/self.edge_eff
      self.tchunk = numpy.where(self.prob.achunk,
                                self.tchunk*area_chunk_new/self.area.chunk,
                                self.tchunk)
      self.area.chunk = numpy.where(self.prob.achunk,
                                    area_chunk_new,
                                    self.area.chunk)
      self.prob.tchunk = numpy.where(self.tchunk<self.tchunk_min,
                                     True,
                                     False)
      #
      # Submap related computations
      self.tsubmap = self.tchunk
      self.speedmax[:,:] = tel.otf.maxrate*beam*self.delta/tel.otf.sampling[0]
    else:
      self.speedmax[:,:] = tel.otf.maxrate*beam**2/(tel.otf.sampling[0]*tel.otf.sampling[1])
      self.tsubmap[:,:] = tel.otf.maxdtoff
      self.edge_eff[:,:] = 1.
    #
    # Areas
    self.area.edge = self.area.target*(1./self.edge_eff - 1.)
    self.area.total = self.area.target/self.edge_eff
    self.area.mappix = self.area.total/nppp
    self.area.submap = self.speedmax*self.tsubmap
    self.nbeam = self.area.target/(self.edge_eff*nppp*self.area.beam)
    self.nsubmap = self.area.target/(self.edge_eff*nppp*self.area.submap)
    #
    self.speed = self.speedmax.copy()
    self.prob.speed[:,:] = False
    self.prob.cover[:,:] = False
    self.prob.toff[:,:]  = False
    #
    self.ncover[:,:] = 1.
    self.nonperoff[:,:] = 1.
    #
  def copy(self,mswitch):
    outotf = DoneOtf(mswitch)
    outotf.area      = self.area.copy(mswitch)
    outotf.prob      = self.prob.copy(mswitch)
    outotf.tsubmap   = self.tsubmap.copy()
    outotf.nsubmap   = self.nsubmap.copy()
    outotf.tchunk    = self.tchunk.copy()
    outotf.ncover    = self.ncover.copy()
    outotf.nbeam     = self.nbeam.copy()
    outotf.nonperoff = self.nonperoff.copy()
    outotf.speed     = self.speed.copy()
    outotf.speedlin  = self.speedlin.copy()
    outotf.speedmax  = self.speedmax.copy()
    outotf.edge_eff  = self.edge_eff.copy()
    outotf.northo    = self.northo.copy()
    outotf.dortho    = self.dortho.copy()
    outotf.aspect    = self.aspect.copy()
    # OTF optional parameters
    if (hasattr(self,"nsubscan")):  outotf.nsubscan = self.nsubscan
    if (hasattr(self,"delta")):     outotf.delta = self.delta
    if (hasattr(self,"edge_eff_min")): outotf.edge_eff_min = self.edge_eff_min
    if (hasattr(self,"tchunk_min")): outotf.tchunk_min = self.tchunk_min
    return outotf
    #
  def finalize(self,beam,tel):
    isarray = tel.front.npixperpol>1
    if (isarray):
      self.speedlin = self.speed/self.delta
    else:
      self.speedlin = self.speed/(beam/tel.otf.sampling[1])


###########
class Area:
  def __init__(self,mswitch):
    mwater = work.mwater
    self.target = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.mappix = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.edge   = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.total  = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.submap = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.chunk  = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.beam   = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    #
  def copy(self,mswitch):
    outarea = Area(mswitch)
    outarea.target = self.target.copy()
    outarea.mappix = self.mappix.copy()
    outarea.edge   = self.edge.copy()
    outarea.total  = self.total.copy()
    outarea.submap = self.submap.copy()
    outarea.chunk  = self.chunk.copy()
    outarea.beam   = self.beam.copy()
    return outarea

###########
class Prob:
  def __init__(self,mswitch):
    mwater = work.mwater
    self.speed   = numpy.empty((mswitch,mwater),dtype=numpy.bool)
    self.cover   = numpy.empty((mswitch,mwater),dtype=numpy.bool)
    self.toff    = numpy.empty((mswitch,mwater),dtype=numpy.bool)
    self.achunk  = numpy.empty((mswitch,mwater),dtype=numpy.bool)
    self.northo  = numpy.empty((mswitch,mwater),dtype=numpy.bool)
    self.tchunk  = numpy.empty((mswitch,mwater),dtype=numpy.bool)
    #
  def copy(self,mswitch):
    outprob = Prob(mswitch)
    outprob.speed   = self.speed.copy()
    outprob.cover   = self.cover.copy()
    outprob.toff    = self.toff.copy()
    outprob.achunk  = self.achunk.copy()
    outprob.northo  = self.northo.copy()
    outprob.tchunk  = self.tchunk.copy()
    return outprob

###############
class DoneTime:
  def __init__(self,mswitch):
    mwater = work.mwater
    self.sig   = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.tel   = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.onoff = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.edge  = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.tot = TimeTot(mswitch) # Total times
    self.bea = TimeTot(mswitch) # Times per beam for all covers
    self.cov = TimeTot(mswitch) # Times per beam and per cover
    #
  def copy(self,mswitch):
    outtime = DoneTime(mswitch)
    outtime.sig   = self.sig.copy()
    outtime.tel   = self.tel.copy()
    outtime.onoff = self.onoff.copy()
    outtime.edge  = self.edge.copy()
    outtime.tot   = self.tot.copy(mswitch)
    outtime.bea   = self.bea.copy(mswitch)
    outtime.cov   = self.cov.copy(mswitch)
    return outtime

##############
class TimeTot:
  def __init__(self,mswitch):
    mwater = work.mwater
    self.on = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.off = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    #
  def get(self,in_instance):
    self.on = in_instance.on.copy()
    self.off = in_instance.off.copy()
    #
  def copy(self,mswitch):
    outtimetot = TimeTot(mswitch)
    outtimetot.on  = self.on.copy()
    outtimetot.off = self.off.copy()
    return outtimetot

##############
class DoneRms:
  def __init__(self,mswitch):
    mwater = work.mwater
    self.jy = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    self.k = numpy.empty((mswitch,mwater),dtype=numpy.float64)
    #
  def copy(self,mswitch):
    outrms = DoneRms(mswitch)
    outrms.jy = self.jy.copy()
    outrms.k  = self.k.copy()
    return outrms

##############
class DoneFre:
  def __init__(self):
    self.sig = 0.
    self.ima = 0.
    self.ifreq = 0.
    #
  def copy(self):
    outfre = DoneFre()
    outfre.sig   = self.sig
    outfre.ima   = self.ima
    outfre.ifreq = self.ifreq
    return outfre

#############
class Pixel:
  def __init__(self):
    self.pol = 0
    self.num = 0
    self.trec = 0
    #
  def copy(self):
    outpix = Pixel()
    outpix.pol = self.pol
    outpix.num = self.num
    outpix.trec = self.trec
    return outpix

################
class DoneFront:
  def __init__(self):
    self.available = True  # Is the receiver available for the input frequency?
    self.freq_info = None  # Some additional info at the input frequency.
    self.rname     = ""    # Receiver name
    # Polarization:
    self.ipol  = 0    # Identifier
    self.pname = ""   # Name
    # Pixel identification:
    self.pixel = Pixel()
    # Others
    self.feff = 0.
    self.gain = 0.
    self.fre  = DoneFre()
    self.side = 0
    #
  def copy(self):
    outfront = DoneFront()
    outfront.available = self.available
    outfront.freq_info = self.freq_info
    outfront.rname     = self.rname
    outfront.ipol      = self.ipol
    outfront.pname     = self.pname
    outfront.pixel     = self.pixel.copy()
    outfront.feff      = self.feff
    outfront.gain      = self.gain
    outfront.fre       = self.fre.copy()
    outfront.side      = self.side
    return outfront
    #
  def frontend_parameters(self,infreq,tel):
    loc = utils.compute_location(tel.front.freq,infreq)
    if (loc[0] == loc[1]):
      raise ValueError,"Out of range frequency: "+repr(infreq)+" GHz"
    #
    ifront = loc[0]
    if ((infreq<tel.front.frange[ifront,self.pixel.pol,0]) or
        (infreq>tel.front.frange[ifront,self.pixel.pol,1])):
      # Do not raise an error, just skip
      # raise ValueError,"Frequency: "+repr(infreq)+" GHz out of range of "+repr(tel.front.pname[ifront,self.pixel.pol])
      self.available = False
    for info in tel.front.freq_info[ifront]:
      if (infreq>=info[0] and infreq<=info[1]):
        self.freq_info = info[2]  # Load the message
    self.rname = tel.front.rname[ifront]
    self.pname = tel.front.pname[ifront,self.ipol]
    self.feff = tel.front.feff[ifront]
    self.gain = tel.front.gain[ifront]
    self.fre.ifreq = tel.front.ifreq[ifront]
    if (infreq+2*self.fre.ifreq > tel.front.frange[ifront,self.pixel.pol,1]):
      self.side = cst.usb
    else:
      self.side = cst.lsb
    #
    if (self.pixel.num == 0):
      # Not a real pixel: nothing set
      self.pixel.trec = numpy.nan  # Non-sense, unused. NaN ensures we do not use it later on
    else:
      # tel.front.trec[self.pixel.pol] is an array with several lines of Trec,
      # with each frequency in the 1st column (number 0)
      if (work.interpolate):
        self.pixel.trec = numpy.interp(infreq, \
                          tel.front.trec[self.pixel.pol][:,0], \
                          tel.front.trec[self.pixel.pol][:,self.pixel.num])
      else:
        loc = utils.compute_location(tel.front.trec[self.pixel.pol][:,0],infreq)
        self.pixel.trec = tel.front.trec[self.pixel.pol][loc[0],self.pixel.num]
    #
    self.fre.sig = infreq
    self.fre.ima = infreq+2*self.side*self.fre.ifreq


#######################################################
# Classes which describe the telescope characteristics

class Telescope:
  def __init__(self,inconf):
    self.eff      = inconf.teleff
    self.back     = TelBack(inconf)
    self.front    = TelFront(inconf)
    self.switch   = Switch()
    self.obskinds = HeteroObsKinds()
    self.otf      = Otf(inconf)

class TelBack:
  def __init__(self,inconf):
    self.fresmin = inconf.fresmin  # Minimum frequency resolution
    self.vresmax = inconf.vresmax  # Maximum velocity resolution

class TelFront:
  def __init__(self,inconf):
    self.name = inconf.name                      # Receiver name
    self.n = inconf.nband
    self.rname = inconf.rname                    # Receiver band names
    self.pname = numpy.asarray(inconf.polnames)  # Polarizations names
    # Here we assume that one and only one kind of receiver can reach a
    # given frequency. I could add frequency ranges that two different
    # kinds of receiver reach. I would then have a dedicated treatment
    # for them.
    self.freq = numpy.asarray(inconf.freqs,dtype=numpy.float64)
    self.ifreq = inconf.ifreq
    self.frange = numpy.asarray(inconf.frange,dtype=numpy.float64)
    self.freq_info = inconf.freq_info
    self.feff = inconf.feff
    self.gain = inconf.gain
    self.npixperpol = inconf.npixperpol
    # Trec values
    self.trecfile = utils.make_abs_path(__file__,inconf.name+"_estimator_trec.dat")
    self.trec = [numpy.empty((0,1+inconf.npixperpol),dtype=numpy.float64), \
                 numpy.empty((0,1+inconf.npixperpol),dtype=numpy.float64)]
                # A list of 2 arrays ensures we can give a different number
                # of frequencies for each polarization
    self.load_trec()
  #
  def load_trec(self):
    for line in open(self.trecfile,'r').readlines():
      ihash = line.find("#")
      if (ihash>=0):
        # Remove comments
        line = line[:ihash]
      if (line.strip() == ""):
        # Skip empty lines
        continue  # Next iteration
      values = [eval(v) for v in line.split()]
      self.trec[values[0]] = numpy.append(self.trec[values[0]],[values[1:]],axis=0)

class Otf:
  def __init__(self,inconf):
    self.sampling = inconf.sampling
    self.maxrate  = inconf.maxrate
    self.maxdtoff = inconf.maxdtoff
    if (hasattr(inconf,"maxdtcal")):  self.maxdtcal = inconf.maxdtcal

class Warnings:
  def __init__(self):
    self.reset()
    #
  def reset(self):
    self.text = []
    #
  def add(self,more):
    self.text.append(more)
    #
  def get(self):
    return self.text
