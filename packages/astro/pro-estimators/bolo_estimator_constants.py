########################################################################
# 30m time and/or sensitivity estimators.
# Constants and parameters shared by all receivers.
########################################################################

class BoloObsKinds:
  def __init__(self):
    self.onoff = 0
    self.map = 1
    self.mode = ["onoff","map"]

# Instantiate
bobskinds = BoloObsKinds()

class BoloMapRestKinds:
  def __init__(self):
    self.shiftadd = 0
    self.ekh = 1
    self.names = ["Shift-and-Add","EKH"]

# Instantiate
map_restkinds = BoloMapRestKinds()
