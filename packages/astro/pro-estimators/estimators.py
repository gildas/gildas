
import hetero_estimator_utils
import all_estimator_config
import emir_estimator_config
import hera_estimator_config
import noema_estimator_config
import mambo_estimator
import all_estimator_constants
import hetero_estimator_constants
import bolo_estimator_constants

class psp_estimator(object):
    def __init__(self,name):
        self.name = name
        self.prefix = name + "_"
        self.deadline = all_estimator_config.deadline

class hetero(hetero_estimator_utils.Time, psp_estimator):
    def __init__(self, name, conf):
        hetero_estimator_utils.Time.__init__(self, conf)
        psp_estimator.__init__(self, name)
        self.obskinds = hetero_estimator_constants.hobskinds.mode
        self.rms_names = all_estimator_constants.units.krmsname

class emir(hetero):
    def __init__(self):
        hetero.__init__(self, "emir", emir_estimator_config)

class hera(hetero):
    def __init__(self):
        hetero.__init__(self, "hera", hera_estimator_config)
        self.polar_names = hetero_estimator_utils.list_polars(hera_estimator_config)

class noema(hetero):
    def __init__(self):
        hetero.__init__(self, "noema", noema_estimator_config)

class mambo(mambo_estimator.Time, psp_estimator):
    def __init__(self):
        mambo_estimator.Time.__init__(self)
        psp_estimator.__init__(self, "mambo")
        self.obskinds = bolo_estimator_constants.bobskinds.mode
        self.restkinds = bolo_estimator_constants.map_restkinds.names
        self.rms_names = all_estimator_constants.units.jyrmsname

def reload_modules():
    # reload all imported modules
    import types
    import re
    g = globals()
    ret = []
    for k in g.keys():
        # reload(hetero_estimator_utils) is buggy
        if type(g[k]) == types.ModuleType and re.search("(_config|_constants)$", g[k].__name__):
            ret.append(g[k].__name__)
            reload(g[k])
    return ret

