########################################################################
# 30m time and/or sensitivity estimators.
# Constants and parameters shared by all receivers.
########################################################################

class HeteroObsKinds:
  def __init__(self):
    self.tra = 0
    self.otf = 1
    self.mode = ["tracked","otf"]

# Instantiate
hobskinds = HeteroObsKinds()

#############
class Polars:
  def __init__(self):
    self.h = 0
    self.v = 1
    self.hv = 2
    self.h_v = 3
    self.all = 4
    self.name = ["H","V","H+V","H,V","H,V,H+V"]
    self.npol = [1,1,2]  # Only. Non-sense for the 2 lasts.

# Instantiate
polars = Polars()

# Grid smoothing factor
grid_factor = 1.11  # Convolution by a gaussian of FWHM = beam/3
