
from estimators import hera

t = hera()
t.do.season = 1  # summer
t.do.html = False

areas = [10.,100.]
sensitivities = [True,False]

t.do.obskind = 1  # OTF
t.do.rms = 100.
t.do.teltime = 10.
for area in areas:
  t.do.area = area
  for sensitivity in sensitivities:
    t.do.sensitivity = sensitivity
    #
    t.compute()
    t.display()
    #
    print 60*"="
    print "Done: obskind =",t.do.obskind,", sensitivity =",t.do.sensitivity
    print 60*"="

t.do.obskind = 0  # Tracked
for sensitivity in sensitivities:
  t.do.sensitivity = sensitivity
  #
  t.compute()
  t.display()
  #
  print 60*"="
  print "Done: obskind =",t.do.obskind,", sensitivity =",t.do.sensitivity
  print 60*"="
