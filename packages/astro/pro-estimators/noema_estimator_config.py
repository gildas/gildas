########################################################################
# NOEMA time and/or sensitivity estimator for 2017 receivers.
# Configuration file. Customize here the parameters and defaults
# for all values used in the estimator.
########################################################################

# Constant
clight = 2.99792458e8  # [m/s]
burelat = 44.63        # [deg] Bure latitude
ant_diameter = 15

# Parameters
nantdef         = 10          # actual number of antennas at NOEMA site
declmin         = -30.        # minimal accepted declination [deg]
mos_field2point = (7./4.)**2  # conversion from number of fields to number of pointing for mosaic (assuming standard sampling)
mos_tintstd     = 45.         # [sec] Standard integration time at bure
mos_tintmin     = 10.         # [sec] Minimum integration time authorized to stay efficient enough
mos_tslew       = 11.         # [sec] Time to slew from one pointing to the other one [RN/NC 2019]
mos_tcycle      = 45*60.      # [sec] Maximum time to cycle over all pointings of the mosaic. Twice the typical calibration time
mos_eta         = 2.          # []
obseffref       = 1.6         # Observations efficiency during observing cycles
ttune           = 40./60.     # [h] Tuning time for a project is 40min
pfx_iffreq      = 7.744       # [GHz] if frequency
pfx_gim         = 0.05        # Image gain
pfx_contbw      = 15488       # [MHz] continuum bandwitdh in 1 polar
pfx_resmin      = 0.0625      # [MHz] Spectral resolution range of correlator
pfx_resmax      = 15488.      # [MHz] Spectral resolution range of correlator

# Band setup
pfx_nrec            = 4                                 # number of receiver bands
pfx_fmin            = [70.384, 124.384, 196.128,275.2]  # [GHz] Lower limit - for lines
pfx_fmax            = [121.616,183.616, 279.616,372.8]  # [GHz] Upper limit - for lines
pfx_flomin          = [82,     136,     207.744,275]    # [GHz] Lower limit GHz - for continuum
pfx_flomax          = [110,    172,     268,    373]    # [GHz] Upper limit GHz - for continuum
pfx_summerpwv       = [7,        5,       4,      4]    # pwv per band in summer conditions MK S18
pfx_winterpwv       = [5,        4,       3,      2.5]  # pwv per band in winter conditions [MK W18]
pfx_trec            = [35,      45,      55,      0]    # trec as provided by Nichol - Band 4 still hardcoded
pfx_anteff          = [22,      26,      33,     45]    # Jy/K
pfx_decorrel_winter = [0.9,      0.85,    0.8,    0.7]  # decorrelation
pfx_decorrel_summer = [0.9,      0.8,     0.6,    0.5]  # decorrelation
pfx_feff            = [0.96,     0.94,    0.9,    0.9]  # forward efficiency

# User inputs: reasonnable defaults
rx_kind   = "PolyFiX"
season    = "winter"
sideband  = "lsb"
obs_kind  = "line"
bw_kind   = "velocity"
npol      = 2      # 2 polars because most of the time, both polars share the same setup
temp      = 0      # [K]  0 => clever default
water     = 0      # [mm] 0 => clever default
decl      = 25     # [deg]
vres      = 0.25   # [km/s]
teltime   = 8.0    # [hrs]
freq      = 230    # [GHz] List of frequencies for estimation
synbeam   = [1,1]  # [arcsec] Major and minor
mosaic    = False  # no = single field
area      = 0      # [arcmin^2]
trsh      = False  # no = single field
nsou      = 1      #
twinter   = 273    # [K]
tsummer   = 283    # [K]
pwvwinter = 3      # [mm]
pwvsummer = 7      # [mm]
