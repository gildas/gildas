########################################################################
# 30m time and/or sensitivity estimator for ALL ESTIMATORS.
# Configuration file. Customize here the parameters and defaults
# for all values used in the estimators.
########################################################################

from all_estimator_constants import units

# === Weather conditions (do not change!) ===
pwvval = [ [1.,2.,4.] ,
           [2.,4.,7.] ]     # 2x3 floats [mm], pwv (3 first = winter, 3 last =
                            # summer)
pwvfrac = [ [3.,16.,40.] ,
            [5.,23.,50.] ]  # 2x3 floats [%], fraction of total time with at most
                            # such pwv (3 first = winter, 3 last = summer)

# === Next deadline ===
# Customize here the next deadline
deadline = "September 14th, 2010"  # String, appears in the webpage title
season   = units.winter        # Code [summer|winter]. Affect the default season
                               #      and the pwv conditions

