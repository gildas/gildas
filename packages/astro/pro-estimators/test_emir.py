
from estimators import emir

t = emir()
t.do.season = 1  # summer
t.do.html = False

obskinds = [0,1]
sensitivities = [True,False]
freqs = [110.,150.,230.,300.]

for obskind in obskinds:
  t.do.obskind = obskind
  for sensitivity in sensitivities:
    t.do.sensitivity = sensitivity
    for freq in freqs:
      t.do.freq = freq
      #
      t.compute()
      t.display()
      #
      print 60*"="
      print "Done: obskind =",t.do.obskind,", sensitivity =",t.do.sensitivity,", freq =",t.do.freq
      print 60*"="
