########################################################################
# 30m time and/or sensitivity estimator for HERA.
# Configuration file. Customize here the parameters and defaults
# for all values used in the estimator.
########################################################################

from hetero_estimator_constants import hobskinds,polars
from all_estimator_constants import units,switch
from all_estimator_config import season,deadline,pwvval,pwvfrac

# === Identifier ===
name = "hera"  # String. Estimator identifier, do not change!

# === Defaults ===
# Customize here the default parameters which appear in the form on the
# webpage
# FixMe: use the class Do()
verbose_tsys = False     # Bool  [True|False]
verbose_otf = False      # Bool  [True|False]
sensitivity = True       # Bool  [True|False], compute sensitivity (True) or time (False)
obskind = hobskinds.tra  # Code  [tra|otf], heterodyne observing kind
area = 8.                # Float [arcmin^2], map area for OTF observations
areaunit = units.arcmin2 # Code  [arcsec2,arcmin2,degree2], area unit
el = 45.                 # Float [deg]
res = 0.25               # Float [resunit], resolution in 'resunit'
resunit = units.kms      # Code  [kms|mhz|khz], resolution unit
teltime = 1.             # Float [timeunit], integration time
timeunit = units.hr      # Code  [hr|min|sec], time unit
rms = 10.                # Float [rmsunit], RMS sensitivity
rmsunit = units.mkelvin  # Code  [mkelvin|kelvin], rms unit
freq = 230.              # Float [GHz], observing frequency

# === Telescope parameters ===
teleff = 0.5       # Float [], global telescope efficiency
el_min = 10.       # Float [deg], minimum observable elevation (CK, 25.7.09)
el_max = 90.       # Float [deg], maximum observable elevation
temp = 290.        # Float [K], cabin temperature
speceff = 1./1.15  # Float [], spectrometer efficiency

# === OTF parameters ===
# FixMe: use the class Otf():
sampling = [4.,2.5]  # 2 floats [dump per beam, row per beam], Parallel and
                     # perpendicular sampling
maxrate = 2.         # Float [Hz], Maximum dumping rate
maxdtoff = 120.      # Float [sec], Maximum time between 2 OFFs (system stability)
maxdtcal = 600.      # Float [sec], Maximum time between 2 calibrations
nsubscan = 2         # Integer [], 1 = single, 2 = zigzag
delta = 4.           # Float [arcsec], row separation
edge_eff_min = .8    # Float [], used for small map area
tchunk_min = 40.     # Float [sec], minimum chunk duration

# === Back-end parameters ===
fresmin = 20.0e-3  # Float [MHz]. Lowest resolution allowed. VESPA goes down
                   # to 20.0 kHz for HERA.
vresmax = 500.     # Float [km/s]. Highest resolution allowed.

# === Front-end parameters ===
# Customize here the HERA parameters
nband = 1                  # Int [], number of bands
rname = ["HERA"]           # n Strings, band names
freqs = [215.,272.]        # n+1 Floats [GHz], rough limits of each bands
ifreq = [4.]               # n Floats [GHz], Intermediate Frequencies
#                H           V          H+V
frange = [[ [215.,272.],[215.,241.],[215.,241.] ]]
                           # n*3*2 Floats [GHz], frequency ranges (per band,
                           # then per polar -2 individual and 1 combined-,
                           # e.g. frange[0][1] is the range for 1st band and
                           # 2nd polar)
feff = [0.90]              # n Floats [], forward efficiencies (from HERA Manual)
gain = [0.1]               # n Floats [], image gains
#
freq_info = [ [], [], [], [] ]  # n tuples (one per receiver): each list contains
                                # 0 or more lists, each being the frequency range
                                # + the associated message.
#
# Polarizations
ipol = polars.hv           # Code [h|v|hv|h_v|all] ("h_v" for h, v,
                           #                        "all" for h, v, and h+v)
npixperpol = 9             # Int [], number of pixels per polarization
#               H        V        H+V
polnames = [["HERA 1","HERA 2","HERA 1+2"]]
                           # n*3 strings, polarization names (H, V, H+V, for
                           # each band)

# Switching modes
swmodes = [ switch.fre,switch.pos ]
