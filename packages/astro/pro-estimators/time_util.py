
import all_estimator_constants

def build_radio(time,id,checked,value,text):
    ret = '<label><input type="radio" id="' + time.prefix + id + str(value) + \
     '" name="' + time.prefix + id + '" value="' + str(value) + '"'
    if (checked):
        ret += ' checked="checked"'
    ret += ' onclick="on_change_option()" />'
    ret += '<span id="' + time.prefix + 'span_' + id + str(value) + '">'
    ret += text
    ret += '</span></label>'
    return ret

def build_sensitivity_radio(time,text,s):
    return build_radio(time,"time_sensitivity",s,s,text)

def build_obskind_radios(time):
    ret = ''
    for i in xrange(len(time.obskinds)):
        ret += '<td nowrap="nowrap">'
        ret += build_radio(time, "time_obskind", time.do.obskind == i, i, \
         time.obskinds[i])
        ret += '</td>'
    return ret

def build_restkind_radios(time):
    ret = ''
    for i in xrange(len(time.restkinds)):
        ret += '<td nowrap="nowrap">'
        ret += build_radio(time, "time_map_restkind", time.do.map_restkind == i, \
         i, time.restkinds[i])
        ret += '</td>'
    return ret

def build_season_radios(time):
    ret = ''
    for i in (all_estimator_constants.units.winter, \
     all_estimator_constants.units.summer):
        ret += '<td nowrap="nowrap">'
        ret += build_radio(time, "time_season", time.do.season == i, i, \
         all_estimator_constants.units.season[i])
        ret += '</td>'
    return ret

def build_checkbox(time,id,v,text):
    ret = '<span id="' + time.prefix + 'span_' + id + '"><label for="' + \
     time.prefix + id + '"><input type="checkbox" name="' + time.prefix + id \
     + '" id="' + time.prefix + id + '" value="True"'
    if (v):
        ret += ' checked="checked"'
    ret += ' onclick="on_change_option()" />'
    ret += text
    ret += '</label></span>'
    return ret

def build_option(v,selected,text):
    ret = '<option value="' + str(v) + '"'
    if (selected):
        ret += ' selected="selected"'
    ret += '>';
    ret += text
    ret += '</option>';
    return ret

def build_options(time,name,list,selected_index):
    ret = '<select name="'+time.prefix+name+'" onclick="on_change_value()">'
    i = 0
    for c in list:
        ret += build_option(i,selected_index == i,list[i])
        i += 1
    ret += '</select>'
    return ret

def build_resunit_options(time):
    return build_options(time, "time_resunit",
     all_estimator_constants.units.reso, time.do.resunit)

def build_teltimeunit_options(time):
    return build_options(time, "time_teltimeunit",
     all_estimator_constants.units.time, time.do.timeunit)

def build_rmsunit_options(time):
    return build_options(time, "time_rmsunit", time.rms_names, time.do.rmsunit)

def build_sizeazunit_options(time):
    return build_options(time, "time_map_sizeazunit",
     all_estimator_constants.units.anglename, time.do.map_sizeazunit)

def build_sizeelunit_options(time):
    return build_options(time, "time_map_sizeelunit",
     all_estimator_constants.units.anglename, time.do.map_sizeelunit)

def build_areaunit_options(time):
    return build_options(time, "time_areaunit",
     all_estimator_constants.units.areaname, time.do.areaunit)

def build_polars_options(time):
    ret = '<select name="'+time.prefix+'time_ipol" onclick="on_change_value()">'
    i = 0
    for c in time.polar_names:
        ret += build_option(i,time.do.ipol == i,time.polar_names[i])
        i += 1
    ret += '</select>'
    return ret

def build_input_text(time,text, value):
    ret = '<input type="text" name="'+time.prefix+text+'"';
    ret += ' value="'+str(value)+'"';
    ret += ' onfocus="on_change_value()" />';
    return ret;

