########################################################################
# 30m time and/or sensitivity estimator for MAMBO.
# Configuration file. Customize here the parameters and defaults
# for all values used in the estimator.
########################################################################

from bolo_estimator_constants import bobskinds,map_restkinds
from all_estimator_constants import units
from all_estimator_constants import constant as cst
from all_estimator_config import season,deadline

# === Defaults ===
# Customize here the default parameters which appear in the form on the
# webpage
sensitivity = False       # Bool    [True|False], compute sensitivity (True) or time (False)
obskind = bobskinds.onoff # Code    [otf|onoff], bolometer observing kind
verbose = False           # Bool    [True|False], display more info in the output
el = 45.                  # Float   [deg]
teltime = 3.              # Float   [timeunit], integration time
timeunit = units.hr       # Code    [hr|min|sec], time unit
rms = 1.                  # Float   [rmsunit], RMS sensitivity
rmsunit = units.mjy_beam  # Code    [mjy_beam], RMS unit
nsources = 1              # Integer [], number of sources
# Map mode
map_restkind    = map_restkinds.shiftadd  # [shiftadd|ekh]
map_sizeaz      = 10.           # Float [sizeazunit], azimuthal map (shift-and-add) or source (EKH) size
map_sizeazunit  = units.arcmin  # Code  [arcsec|arcmin|degree], sizeaz unit
map_sizeel      = 4.            # Float [sizeelunit], elevation map (shift-and-add) or source (EKH) size
map_sizeelunit  = units.arcmin  # Code  [arcsec|arcmin|degree], sizeel unit
map_vaz         = 8.            # Float [arcsec/s]
map_del         = 8.            # Float [arcsec]
map_Delta_throw = 60.           # Float [arcsec]

# === Map mode parameters ===
map_sizeazmax = 0.5       * cst.deg2rad # Float [degrees], maximum azimuthal source size (EKH only)
map_sizeelmax = 0.5       * cst.deg2rad # Float [degrees], maximum elevation source size (EKH only)
map_vazmin    = 4.        * cst.sec2rad # Float [arcsec/s]
map_vazmax    = 10.       * cst.sec2rad # Float [arcsec/s]
map_delmin    = 5.        * cst.sec2rad # Float [arcsec]
map_delmax    = 24.       * cst.sec2rad # Float [arcsec]
map_Delta_throwmin = 30.  * cst.sec2rad # Float [arcsec]
map_Delta_throwmax = 90.  * cst.sec2rad # Float [arcsec]
map_Delta_array    = 245. * cst.sec2rad # Float [arcsec]
map_Delta_base     = 30.  * cst.sec2rad # Float [arcsec]

# === Maximum durations ===
map_dsubmap = 1. * cst.hr2sec  # Float [hr], submap duration
map_dscan   = 1. * cst.hr2sec  # Float [hr], scan duration

# === Hardware parameters ===
onoff_NEFD = 30. * cst.mjy2jy  # Float [mJy/beam.sqrt(sec)]
alpha_xrange = [40. * cst.sec2rad, \
                 2. * cst.min2rad]  # 2 Floats [arcsec,arcmin]
alpha_yrange = [0.2 * cst.mjy2jy/cst.sec2rad, \
                0.6 * cst.mjy2jy/cst.sec2rad]  # 2 Floats [mJy/beam.sqrt(sec)/arcsec]

tsubscan       = 60.  # Float   [sec]  Subscan duration
tsubscan_ohead =  4.  # Float   [sec]  Subscan start and stop
tscan_ohead    = 15.  # Float   [sec]  Scan start and stop
nsubscanmax    = 20   # Integer []     Maximum number of subscan per scan

dcal      = 1. * cst.hr2sec   # Float [hr], Duration between "calibration" procedures
tpointing = 3. * cst.min2sec  # Float [min], Time for 1 pointing
tfocus    = 2. * cst.min2sec  # Float [min], Time for 1 focus
tskydips  = 5. * cst.min2sec  # Float [min], Time for 1 skydips
tcalonoff = 4. * cst.min2sec  # Float [min], Time for 1 On-Off on a calibrator
eff_cal   = 1.5               # Float [],  Overhead factor during the "calibration" procedures
tcal = (tpointing+tfocus+tskydips+tcalonoff)*eff_cal

eff_tel = 1.0  # Float []  Telescope efficiency (between 0. and 1.)

# === Weather conditions ===
opacities = [[.10,.30,.50], \
             [.20,.35,.50]]    # 2x3 Floats []  Winter and Summer opacities
