
import sys,os,numpy
from all_estimator_constants import constant

def python_check_version_26():
  # Check if Python is 2.6 or newer
  if (sys.version_info[0] < 2):
    return False
  elif (sys.version_info[1] < 6):
    return False
  else:
    return True

#######################
# Formatting functions

def format_tolist(values_in):
  # Convert input values into an (iterable) list. 'values_in' may be
  # of any length. Scalars are converted to list.
  if (type(values_in) == list):
    return values_in
  elif (type(values_in) == numpy.ndarray):
    return values_in.tolist()
  else:
    return [values_in]

def format_rms_jyb(prefix,rms_in,suffix=""):
  rms = format_tolist(rms_in)
  #
  minrms = min(rms)
  if (minrms>1.):
    factor = 1.
    unit = "Jy/beam"
  elif (minrms>1.e-3):
    factor = 1.e3
    unit = "mJy/beam"
  else:
    factor = 1.e6
    unit = "microJy/beam"
  if (suffix != ""):
    unit += " "+suffix
  rms_scaled = [x*factor for x in rms]
  #
  if (python_check_version_26()):
    # Use new formatting scheme
    listformat = "{0:27s}"
    for i in xrange(len(rms_scaled)):
      listformat += "{1["+repr(i)+"]:9.1f}"
    listformat += " {2}"
    return listformat.format(prefix,rms_scaled,unit)
  else:
    # Use old formatting scheme
    listformat = "%-27s"
    listvalues = [prefix]
    for i in xrange(len(rms_scaled)):
      listformat += "%9.1f"
      listvalues.append(rms_scaled[i])
    listformat += " %s"
    listvalues.append(unit)
    #
    return listformat % tuple(listvalues)

def format_integer_9d(prefix,values_in,unit=""):
  values = [int(v) for v in format_tolist(values_in)]
  #
  if (python_check_version_26()):
    # Use new formatting scheme
    listformat = "{0:27s}"
    for i in xrange(len(values)):
      listformat += "{1["+repr(i)+"]:9d}"
    listformat += " {2}"
    #
    return listformat.format(prefix,values,unit)
  else:
    # Use old formatting scheme
    listformat = "%-27s"
    listvalues = [prefix]
    for i in xrange(len(values)):
      listformat += "%9d"
      listvalues.append(values[i])
    listformat += " %s"
    listvalues.append(unit)
    #
    return listformat % tuple(listvalues)

def format_string(prefix,values_in,unit="",right=False):
  values = format_tolist(values_in)
  #
  if (python_check_version_26()):
    # Use new formatting scheme
    #
    if (right):
      sformat = ">9s"
    else:
      sformat = "9s"
    listformat = "{0:27s}"
    for i in xrange(len(values)):
      listformat += "{1["+repr(i)+"]:"+sformat+"}"
    listformat += " {2}"
    #
    return listformat.format(prefix,values,unit)
  else:
    # Use old formatting scheme
    if (right):
      sformat = "9s"
    else:
      sformat = "-9s"
    listformat = "%-27s"
    listvalues = [prefix]
    for i in xrange(len(values)):
      listformat += "%"+sformat
      listvalues.append(values[i])
    listformat += " %s"
    listvalues.append(unit)
    #
    return listformat % tuple(listvalues)

def format_float_91f(prefix,values_in,unit=""):
  values = format_tolist(values_in)
  #
  if (python_check_version_26()):
    # Use new formatting scheme
    listformat = "{0:27s}"
    for i in xrange(len(values)):
      listformat += "{1["+repr(i)+"]:9.1f}"
    listformat += " {2}"
    #
    return listformat.format(prefix,values,unit)
  else:
    # Use old formatting scheme
    listformat = "%-27s"
    listvalues = [prefix]
    for i in xrange(len(values)):
      listformat += "%9.1f"
      listvalues.append(values[i])
    listformat += " %s"
    listvalues.append(unit)
    #
    return listformat % tuple(listvalues)

def format_float_92f(prefix,values_in,unit=""):
  values = format_tolist(values_in)
  #
  if (python_check_version_26()):
    # Use new formatting scheme
    listformat = "{0:27s}"
    for i in xrange(len(values)):
      listformat += "{1["+repr(i)+"]:9.2f}"
    listformat += " {2}"
    #
    return listformat.format(prefix,values,unit)
  else:
    # Use old formatting scheme
    listformat = "%-27s"
    listvalues = [prefix]
    for i in xrange(len(values)):
      listformat += "%9.2f"
      listvalues.append(values[i])
    listformat += " %s"
    listvalues.append(unit)
    #
    return listformat % tuple(listvalues)

def format_float_93f(prefix,values_in,unit=""):
  values = format_tolist(values_in)
  #
  if (python_check_version_26()):
    # Use new formatting scheme
    listformat = "{0:27s}"
    for i in xrange(len(values)):
      if (type(values[i]) == str):
        listformat += "{1["+repr(i)+"]:9s}"
      else:
        listformat += "{1["+repr(i)+"]:9.3f}"
    listformat += " {2}"
    #
    return listformat.format(prefix,values,unit)
  else:
    # Use old formatting scheme
    listformat = "%-27s"
    listvalues = [prefix]
    for i in xrange(len(values)):
      if (type(values[i]) == str):
        listformat += "%9s"
      else:
        listformat += "%9.3f"
      listvalues.append(values[i])
    listformat += " %s"
    listvalues.append(unit)
    #
    return listformat % tuple(listvalues)

def format_float_91f_mmm(prefix,values_in,unit=""):
  # Min, Median, Max
  values = [numpy.min(values_in),numpy.median(values_in),numpy.max(values_in)]
  return format_float_91f(prefix,values,unit)

def format_time_1(t):
  # Format a single time value
  if (t>3600.):
    return "%.1f hr" % (t/3600.)
  elif (t>60.):
    return "%.1f min" % (t/60.)
  else:
    return "%.1f sec" % (t)

def format_time(prefix,t_in):
  t = format_tolist(t_in)
  #
  mintime = min(t)
  if (mintime<60.):
    factor = 1.
    unit = "sec"
  elif (mintime<3600.):
    factor = 60.
    unit = "min"
  else:
    factor = 3600.
    unit = "hr"
  t_scaled = [x/factor for x in t]
  #
  if (python_check_version_26()):
    # Use new formatting scheme
    listformat = "{0:27s}"
    for i in xrange(len(t_scaled)):
      listformat += "{1["+repr(i)+"]:9.1f}"
    listformat += " {2}"
    return listformat.format(prefix,t_scaled,unit)
  else:
    # Use old formatting scheme
    listformat = "%-27s"
    listvalues = [prefix]
    for i in xrange(len(t_scaled)):
      listformat += "%9.1f"
      listvalues.append(t_scaled[i])
    listformat += " %s"
    listvalues.append(unit)
    #
    return listformat % tuple(listvalues)

def format_angle(prefix,angle_in,suffix=""):
  angle = format_tolist(angle_in)
  #
  minangle = min(angle)
  if (minangle>constant.deg2rad):
    factor = constant.deg2rad
    unit = "deg"
  elif (minangle>constant.min2rad):
    factor = constant.min2rad
    unit = "arcmin"
  else:
    factor = constant.sec2rad
    unit = "arcsec"
  if (suffix != ""):
    unit += " "+suffix
  angle_scaled = [x/factor for x in angle]
  #
  if (python_check_version_26()):
    # Use new formatting scheme
    listformat = "{0:27s}"
    for i in xrange(len(angle_scaled)):
      listformat += "{1["+repr(i)+"]:9.1f}"
    listformat += " {2}"
    return listformat.format(prefix,angle_scaled,unit)
  else:
    # Use old formatting scheme
    listformat = "%-27s"
    listvalues = [prefix]
    for i in xrange(len(angle_scaled)):
      listformat += "%9.1f"
      listvalues.append(angle_scaled[i])
    listformat += " %s"
    listvalues.append(unit)
    #
    return listformat % tuple(listvalues)

##########################
# Miscellaneous functions

def make_abs_path(path, file):
  return os.path.join(os.path.dirname(path), file)

def compute_location(arr,x):
  # Try to mimic COMPUTE LOCATION
  # Assume 1D-array sorted in ascending order
  if (x<arr[0]):
    return (0,0)
  if (x>arr[-1]):
    l = len(arr)-1
    return (l,l)
  #
  ind = arr.searchsorted(x)
  if (ind==0):  # Special left boundary case
    ind += 1
  return (ind-1,ind)

def airmass(elevation):
  # Input elevation in degrees
  return 1./numpy.sin(numpy.radians(elevation))

def rad2sec(angle):
  return angle/constant.sec2rad
