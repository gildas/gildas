
var _dynamic_load = 0; /* don't work properly with firefox and IE */
var _estimator;
var _prefix;
var _parameters;

function on_tab_selected(tab_name)
{
    _estimator = tab_name;
    _prefix = _estimator + "_";
    _parameters = document.forms[_prefix + 'parameters'];
    /* dynamic version */
    if (_dynamic_load)
        load_tab();
    else
        on_change_option();
}

function $N(name)
{
    return document.getElementsByName(_prefix + name)[0];
}

function $I(name)
{
    return $(_prefix + name);
}

function on_change_value()
{
    $I("time_compute_button").disabled = false;
}

function on_change_option()
{
    var sensitivity = get_radio_value("time_sensitivity");
    var obskind = get_radio_value("time_obskind");
    var normal_color = $N("time_teltimeunit").style.color;
    var disabled_color = "gray";

    //alert('on_change_option');
    if (sensitivity == "True") {
        $N("time_teltime").disabled = false;
        $N("time_teltimeunit").disabled = false;
        $N("time_rms").disabled = true;
        $N("time_rmsunit").disabled = true;
    } else {
        $N("time_teltime").disabled = true;
        $N("time_teltimeunit").disabled = true;
        $N("time_rms").disabled = false;
        $N("time_rmsunit").disabled = false;
    }
    if ($N("time_area") != null) {
        if (obskind == obskind_otf) {
            $N("time_area").disabled = false;
            $N("time_areaunit").disabled = false;
        } else {
            $N("time_area").disabled = true;
            $N("time_areaunit").disabled = true;
        }
    }
    if ($N("time_verbose_otf") != null) {
        if (obskind == obskind_otf) {
            $N("time_verbose_otf").disabled = false;
            $I("span_time_verbose_otf").style.color = normal_color;
        } else {
            $N("time_verbose_otf").disabled = true;
            $I("span_time_verbose_otf").style.color = disabled_color;
        }
    }
    if ($N("time_map_restkind") != null) {
        var restkind = get_radio_value("time_map_restkind");

        $N("time_nsources").disabled = false;
        if (obskind == obskind_map) {
            $I("time_map_restkind0").disabled = false;
            $I("span_time_map_restkind0").style.color = normal_color;
            $I("time_map_restkind1").disabled = false;
            $I("span_time_map_restkind1").style.color = normal_color;
            $N("time_map_sizeaz").disabled = false;
            $N("time_map_sizeazunit").disabled = false;
            $N("time_map_sizeel").disabled = false;
            $N("time_map_sizeelunit").disabled = false;
            if (restkind == restkind_shiftadd)
                $N("time_nsources").disabled = true;
            $I("expert_parameters").style.display = "block";
        } else {
            $I("time_map_restkind0").disabled = true;
            $I("span_time_map_restkind0").style.color = disabled_color;
            $I("time_map_restkind1").disabled = true;
            $I("span_time_map_restkind1").style.color = disabled_color;
            $N("time_map_sizeaz").disabled = true;
            $N("time_map_sizeazunit").disabled = true;
            $N("time_map_sizeel").disabled = true;
            $N("time_map_sizeelunit").disabled = true;
            $I("expert_parameters").style.display = "none";
        }
        var size_type;
        if (restkind == restkind_shiftadd)
            size_type = "Mosaic";
        else
            size_type = "Source";
        $I("time_map_sizeaz_text").innerHTML = size_type + " size (scanned dir.)";
        $I("time_map_sizeel_text").innerHTML = size_type + " size (perpen.  dir.)";
    }
    time_compute();
}

function get_radio_value(name)
{
    var radio_button = _parameters.elements[_prefix+name];
    for (i=0;i<radio_button.length;i++)
    {
        if (radio_button[i].checked==true)
            return radio_button[i].value;
    }
    return "";
}

function on_complete_load(t)
{
    on_change_option();
}

function load_tab()
{
    var params = "";
    params += "&estimator="+_estimator;
    var req=new Ajax.Updater(_prefix+'form',"time_form.psp",{method:"get",
     parameters:params, onComplete:on_complete_load});
}

function on_complete_compute(t)
{
    //alert('on_complete_compute');
    time_set_result_warning($I("textarea").getAttribute('warning'));
    //firstElement = $I("textarea").getElementsBySelector('.result_field')[0];
    firstElement = $I("textarea").getElementsBySelector('[default="1"]')[0];
    if (firstElement)
        firstElement.onclick();
    else
        $I("result_text").value = '';
}

function time_compute()
{
    var params;

    //alert('time_compute');

    // clear textarea to alert user for a new result
    $I("textarea").value = "";
    // clear warnings
    time_set_selection_warning('');
    time_clear_result_warning('');
    $I("time_compute_button").disabled = true;
    params = "estimator=" + _estimator;
    params += "&" + Form.serialize(_parameters);
    if (time_debug)
        params += "&debug=1";
    var req=new Ajax.Updater(_prefix + 'results', "time_compute.psp",
     {method:"post", parameters:params,
     onComplete:on_complete_compute});
    return 0;
}

function reset_form()
{
    new Ajax.Updater(_estimator,"time_form.psp",{method:"get",
     asynchronous: false,
     parameters:'&estimator='+_estimator});
    on_tab_selected(_estimator);
}

function time_set_result_text(node, text, warning)
{
    node.parentNode.childElements().invoke('removeClassName', 'active');
    node.addClassName('active');
    $I("result_text").value = text;
    //$I("result_text").select();
    time_set_selection_warning(warning);
}

function time_set_selection_warning(warning)
{
    if (warning != '')
        warning = '<p>Warning: ' + warning + '</p>';
    $I("selection_warning").innerHTML = warning;
}

function time_clear_result_warning()
{
    $I("result_warning").innerHTML = '';
}

function time_set_result_warning(warning)
{
    if (warning != '') {
        warnings = warning.split('<|>');
        len = warnings.length;
        for (var i = 0; i < len; i++) {
            warning = '<p>Warning: ' + warnings[i] + '</p>';
            $I("result_warning").innerHTML += warning;
        }
    }
}

