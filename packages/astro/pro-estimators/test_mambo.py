from estimators import mambo

t = mambo()
t.do.season = 1  # summer
t.do.verbose = True

sensitivities = [True,False]
map_restkinds = [0,1]

t.do.obskind = 0  # On-Off
for sensitivity in sensitivities:
  t.do.sensitivity = sensitivity
  #
  t.compute()
  t.display()
  #
  print 60*"="
  print "Done: obskind =",t.do.obskind,", sensitivity =",t.do.sensitivity
  print 60*"="


t.do.obskind = 1  # OTF
t.do.teltime = 10.
for map_restkind in map_restkinds:
  t.do.map_restkind = map_restkind
  for sensitivity in sensitivities:
    t.do.sensitivity = sensitivity
    #
    t.compute()
    t.display()
    #
    print 60*"="
    print "Done: obskind =",t.do.obskind,", restkind =",t.do.map_restkind,", sensitivity =",t.do.sensitivity
    print 60*"="

