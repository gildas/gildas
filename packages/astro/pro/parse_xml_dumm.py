import sys
import pyastro
from gxml.parser import Parser

pyastro.get(verbose=False)
g = pyastro.gdict

if len(sys.argv):
   file = sys.argv[1]
else:
   file = "ObsProject.xml"
fp = open(file,"r")
p = Parser(fp)

#
# get SchedBlockEntity
tree = ["sbl:SchedBlock","sbl:SchedBlockEntity"]
(laddress,lmeta,ldata) = p.do(tree)
g.dumm.entityid=[str(toto['entityId']) for toto in lmeta]
#
# get SchedBblock name
tree = ["sbl:SchedBlock","prj:name"]
(laddress,lmeta,ldata) = p.do(tree)
g.dumm.name=[str(toto) for toto in ldata]
#
# and array
tree = ["sbl:SchedBlock","prj:ObsUnitControl"]
(laddress,lmeta,ldata) = p.do(tree)
g.dumm.array=[str(toto['arrayRequested']) for toto in lmeta]

fp.close()
