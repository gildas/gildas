import sys
import pyastro
from gxml.parser import Parser

pyastro.get(verbose=False)
g = pyastro.gdict

if len(sys.argv):
   file = sys.argv[1]
else:
   file = "SchedBlock0.xml"
fp = open(file,"r")
p = Parser(fp)
#
# Verify Schedblock type ---------------------------------------------------
#
tree=["sbl:SchedBlock","prj:ObsUnitControl"]
(laddress,lmeta,ldata) = p.do(tree)
if (lmeta[0]["arrayRequested"]  == "TWELVE-M"): 
  correlator = "BL"
elif (lmeta[0]["arrayRequested"]  == "SEVEN-M"):
  correlator = "ACA"
else:
  raise Exception("Do not know how to treat "+lmeta[0]["arrayRequested"])
#
# Spectral setup ------------------------------------------------------------
# Name
tree = ["sbl:SchedBlock","sbl:SpectralSpec","sbl:name"]
(laddress,lmeta,ldata) = p.do(tree)
g.sb.freq.name_ss=[str(toto) for toto in ldata]
# 
# + Sky frequency and unit
tree = ["sbl:SchedBlock","sbl:SpectralSpec","sbl:FrequencySetup","sbl:restFrequency"]
(laddress,lmeta,ldata) = p.do(tree)
g.sb.freq.rest_f_unit=[str(toto['unit']) for toto in lmeta]
g.sb.freq.rest_f=[eval(i) for i in ldata]
#
# + LO1 frequency and unit
tree = ["sbl:SchedBlock","sbl:SpectralSpec","sbl:FrequencySetup","sbl:lO1Frequency"]
(laddress,lmeta,ldata) = p.do(tree)
g.sb.freq.lo1_f_unit=[str(toto['unit']) for toto in lmeta]
g.sb.freq.lo1_f=[eval(i) for i in ldata]
#
# Basebands ----------------------------------------------------------------
# + Baseband ID (for correspondance with SpW later)
tree = ["sbl:SchedBlock","sbl:SpectralSpec","sbl:FrequencySetup","sbl:BaseBandSpecification"]
(laddress,lmeta,ldata) = p.do(tree)
g.sb.bb.id=[str(toto['entityPartId']) for toto in lmeta]
#
# + Center Frequency and unit
tree = ["sbl:SchedBlock","sbl:SpectralSpec","sbl:FrequencySetup","sbl:BaseBandSpecification","sbl:centerFrequency"]
(laddress,lmeta,ldata) = p.do(tree)
g.sb.bb.address = laddress
g.sb.bb.center_f_unit=[str(toto['unit']) for toto in lmeta]
g.sb.bb.center_f=[eval(i) for i in ldata]
#
# + LO2 frequency and unit
tree = ["sbl:SchedBlock","sbl:SpectralSpec","sbl:FrequencySetup","sbl:BaseBandSpecification","sbl:lO2Frequency"]
(laddress,lmeta,ldata) = p.do(tree)
g.sb.bb.lo2_f_unit=[str(toto['unit']) for toto in lmeta]
g.sb.bb.lo2_f=[eval(i) for i in ldata]
(laddress,lmeta,ldata) = p.do(tree)
#
# Spectral windows ---------------------------------------------------------
# + partId for correspondance with Basebands
tree = ["sbl:SchedBlock","sbl:SpectralSpec","sbl:"+correlator+"CorrelatorConfiguration","sbl:"+correlator+"BaseBandConfig","sbl:BaseBandSpecificationRef"]
(laddress,lmeta,ldata) = p.do(tree)
g.sb.spw.bb_ref=[str(toto['partId']) for toto in lmeta]
#
# Now we have the correspondance in num_bb
#
# .. Sideband, number of pol.
tree = ["sbl:SchedBlock","sbl:SpectralSpec","sbl:"+correlator+"CorrelatorConfiguration","sbl:"+correlator+"BaseBandConfig","sbl:"+correlator+"SpectralWindow"]
(laddress,lmeta,ldata) = p.do(tree)
g.sb.spw.address = laddress
g.sb.spw.sb=[str(toto['sideBand']) for toto in lmeta]
g.sb.spw.pol=[str(toto['polnProducts']) for toto in lmeta]
#
# SpW centerFreq
tree = ["sbl:SchedBlock","sbl:SpectralSpec","sbl:"+correlator+"CorrelatorConfiguration","sbl:"+correlator+"BaseBandConfig","sbl:"+correlator+"SpectralWindow","sbl:centerFrequency"]
(laddress,lmeta,ldata) = p.do(tree)
g.sb.spw.center_f=[eval(i) for i in ldata]
g.sb.spw.center_f_unit=[str(toto['unit']) for toto in lmeta]
#
# SpW effective width
tree = ["sbl:SchedBlock","sbl:SpectralSpec","sbl:"+correlator+"CorrelatorConfiguration","sbl:"+correlator+"BaseBandConfig","sbl:"+correlator+"SpectralWindow","sbl:effectiveBandwidth"]
(laddress,lmeta,ldata) = p.do(tree)
g.sb.spw.eff_width=[eval(i) for i in ldata]
g.sb.spw.eff_width_unit=[str(toto['unit']) for toto in lmeta]
#
# Effective number of channels
tree = ["sbl:SchedBlock","sbl:SpectralSpec","sbl:"+correlator+"CorrelatorConfiguration","sbl:"+correlator+"BaseBandConfig","sbl:"+correlator+"SpectralWindow","sbl:effectiveNumberOfChannels"]
(laddress,lmeta,ldata) = p.do(tree)
g.sb.spw.num_chan_eff=[eval(i) for i in ldata]

fp.close()
