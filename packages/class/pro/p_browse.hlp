1 BROWSE
        GO BROWSE

    Display an interactive two-panel view of the current R spectrum.

    Bottom panel is the full spectrum (all the frequency range), while
    the to panel is a subset (zoom) of the spectrum. Interactions with
    the mouse and the keyboard allows to zoom, unzoom, and browse the
    spectrum. Type HELP GO BROWSE KEYS for details.

    The Y intensity scale is ruled by the current SET MODE Y mode.
    There are 2 possibilities:
    - SET MODE Y TOTAL: both panels will use the best Y scale. The
      result may be different betwen the top and bottom panels.
    - SET MODE Y Min Max: both panels will use the same Y scale.

    This procedure also displays in the top panel the position of
    spectral lines. These lines are read from a custom catalog given
    in the variable BROWSE%CAT. If not defined, the default ASTRO
    catalog (gag_data:molecules.dat) is used. 

    KEYS

    Left  clic: Center zoom at cursor location 
    Right clic: Exit 
    Press SPACE key: Center zoom at cursor location 
    Press   Z   key: Zoom at cursor location 
    Press   U   key: Unzoom at cursor location 
    Press   B   key: Reset zoom factor to default 
    Press   D   key: Default zoom width and location 
    Press   V   key: Values at cursor location 
    Press   N   key: Shift forward  the zoom window by half its size 
    Press   P   key: Shift backward the zoom window by half its size 
    Press   Y   key: Zoom Y axis scale (Intensity) 
    Press   T   key: Back to full scale for Y axis 
    Press   E   key: Exit loop 
    Press   H   key: Help display


    CATALOG
    
    The GO BROWSE catalog is an ascii file with 2 or 3 columns:
      1 - the line frequency (GHz)
      2 - the line name, with possible escape character for
          display in Greg plots,
      3 - Optional column with extra information on the line.
    Lines starting with an exclamation mark (!) are ignored. A
    typical example is:

!----------------------------------------------------------------
      80.57828  'HDO'                   '1\\D1,0\\U-0\\D0,0\\U'
      80.99316  'CH\d3OH'               '7\D2-8\D1 A-'
!----------------------------------------------------------------
      81.505208 'CCS'                   '6,7-5,6'
      81.65308  'CH\d3OH'               '18\D4-19\D3 E'
      81.881469 'HC\d3N'                '9-8'
!----------------------------------------------------------------
