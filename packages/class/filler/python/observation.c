/*****************************************************************************
 *                              Dependencies                                 *
 *****************************************************************************/

#include <string.h>
#include <Python.h>
#include "gsys/cfc.h"

#define pyclassfiller_obs_reset  CFC_EXPORT_NAME( pyclassfiller_obs_reset )
#define pyclassfiller_obs_write  CFC_EXPORT_NAME( pyclassfiller_obs_write )

void pyclassfiller_obs_reset (int *, int *);
void pyclassfiller_obs_write (int *);

/*****************************************************************************
 *                             Function bodies                               *
 *****************************************************************************/

/**
 * C-Python overlay to Fortran subroutine pyclassfiller_obs_reset.
 *
 * @param[in] self.
 * @param[in] args is a PyObject with input params values.
 * @param[in] kwds is a PyObject with input params optional keywords.
 * @return nothing
 */
PyObject* pyclassfiller_obs_reset_C (PyObject *self, PyObject *args, PyObject *kwds) {
  int nchan,error;

  static char *kwlist[] = {"nchan",NULL};

  if (! PyArg_ParseTupleAndKeywords(args,kwds,"i",kwlist,&nchan))
      return NULL;

  error = 0;
  pyclassfiller_obs_reset(&nchan,&error);

  if (error) {
    PyErr_SetString(PyExc_Exception,"Error while executing pyclassfiller_obs_reset");
    return NULL;
  }

  Py_RETURN_NONE;
}

/**
 * C-Python overlay to Fortran subroutine pyclassfiller_obs_write.
 *
 * @param[in] self.
 * @param[in] args is a PyObject with input params values.
 * @param[in] kwds is a PyObject with input params optional keywords.
 * @return nothing
 */
PyObject* pyclassfiller_obs_write_C (PyObject *self, PyObject *args, PyObject *kwds) {
  int error;

  static char *kwlist[] = {NULL};

  if (! PyArg_ParseTupleAndKeywords(args,kwds,"",kwlist))
      return NULL;

  error = 0;
  pyclassfiller_obs_write(&error);

  if (error) {
    PyErr_SetString(PyExc_Exception,"Error while executing pyclassfiller_obs_write");
    return NULL;
  }

  Py_RETURN_NONE;
}
