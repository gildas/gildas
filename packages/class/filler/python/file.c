/*****************************************************************************
 *                              Dependencies                                 *
 *****************************************************************************/

#include <string.h>
#include <Python.h>
#include <inttypes.h>
#include "gsys/cfc.h"

#define class_fileout_open  CFC_EXPORT_NAME( class_fileout_open )
#define class_fileout_close CFC_EXPORT_NAME( class_fileout_close )

void class_fileout_open  (char *, int *, int *, int64_t *, int *, int *, int);
void class_fileout_close (int *);

/*****************************************************************************
 *                             Function bodies                               *
 *****************************************************************************/

/**
 * C-Python overlay to Fortran subroutine class_fileout_open.
 *
 * @param[in] self.
 * @param[in] args is a PyObject with input params values.
 * @param[in] kwds is a PyObject with input params optional keywords.
 * @return nothing
 */
PyObject* pyclassfiller_fileout_open_C (PyObject *self, PyObject *args, PyObject *kwds) {
  char          *file;
  PyObject      *new,*over,*single;
  int           lnew,lover,lsingle,lerror;
  PY_LONG_LONG  psize;  /* The one read by Python */
  int64_t       fsize;  /* The one sent to Fortran */

  static char *kwlist[] = {"file","new","over","size","single",NULL};

  if (! PyArg_ParseTupleAndKeywords(args,kwds,"sOOLO",kwlist,&file,&new,&over,&psize,&single))
      return NULL;

  lnew = PyObject_IsTrue(new);
  lover = PyObject_IsTrue(over);
  fsize = (int64_t)psize;
  lsingle = PyObject_IsTrue(single);
  lerror = 0;
  class_fileout_open(file,&lnew,&lover,&fsize,&lsingle,&lerror,strlen(file));

  if (lerror) {
    PyErr_SetString(PyExc_Exception,"Error while executing fileout_open");
    return NULL;
  }

  Py_RETURN_NONE;
}


/**
 * C-Python overlay to Fortran subroutine class_fileout_close.
 *
 * @param[in] self.
 * @param[in] args is a PyObject with input params values.
 * @param[in] kwds is a PyObject with input params optional keywords.
 * @return nothing
 */
PyObject* pyclassfiller_fileout_close_C (PyObject *self, PyObject *args, PyObject *kwds) {
  int lerror;

  static char *kwlist[] = {NULL};

  if (! PyArg_ParseTupleAndKeywords(args,kwds,"",kwlist))
      return NULL;

  lerror = 0;
  class_fileout_close(&lerror);

  if (lerror) {
    PyErr_SetString(PyExc_Exception,"Error while executing fileout_close");
    return NULL;
  }

  Py_RETURN_NONE;
}
