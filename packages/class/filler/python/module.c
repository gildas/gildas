
#include <Python.h>
#define PY_ARRAY_UNIQUE_SYMBOL pyclassfiller_ARRAY_API
#define NPY_NO_DEPRECATED_API NPY_API_VERSION
#include "numpy/arrayobject.h"
#include "gsys/cfc.h"
#include "sic/gpackage-pyimport.h"

#define class_write_init           CFC_EXPORT_NAME( class_write_init )
#define class_write_clean          CFC_EXPORT_NAME( class_write_clean )
#define pyclassfiller_obs_init     CFC_EXPORT_NAME( pyclassfiller_obs_init )
#define pyclassfiller_obs_clean    CFC_EXPORT_NAME( pyclassfiller_obs_clean )
#define pyclassfiller_code_coord   CFC_EXPORT_NAME( pyclassfiller_code_coord )
#define pyclassfiller_code_switch  CFC_EXPORT_NAME( pyclassfiller_code_switch )
#define pyclassfiller_code_kind    CFC_EXPORT_NAME( pyclassfiller_code_kind )
#define pyclassfiller_code_xunit   CFC_EXPORT_NAME( pyclassfiller_code_xunit )
#define pyclassfiller_code_proj    CFC_EXPORT_NAME( pyclassfiller_code_proj )
#define pyclassfiller_code_velo    CFC_EXPORT_NAME( pyclassfiller_code_velo )
#define pyclassfiller_code_conv    CFC_EXPORT_NAME( pyclassfiller_code_conv )
#define pyclassfiller_code_section CFC_EXPORT_NAME( pyclassfiller_code_section )
#define pyclassfiller_code_qual    CFC_EXPORT_NAME( pyclassfiller_code_qual )
#define pyclassfiller_code_calib   CFC_EXPORT_NAME( pyclassfiller_code_calib )

void class_write_init (int *);
void class_write_clean (int *);
void pyclassfiller_obs_init (int *);
void pyclassfiller_obs_clean (int *);
void clean_core( void );
void pyclassfiller_code_coord   (int *, int *, int *, int *);
void pyclassfiller_code_switch  (int *, int *, int *);
void pyclassfiller_code_kind    (int *, int *, int *, int *, int *);
void pyclassfiller_code_xunit   (int *, int *, int *);
void pyclassfiller_code_proj    (int *, int *, int *, int *, int *, int *, int *, int *, int *, int *, int *, int *);
void pyclassfiller_code_velo    (int *, int *, int *, int *, int *, int *);
void pyclassfiller_code_conv    (int *, int *, int *, int *);
void pyclassfiller_code_section (int *, int *, int *, int *, int *, int *, int *, int *, int *, int *, int *, int *, int *, int *, int *, int *, int *);
void pyclassfiller_code_qual    (int *, int *, int *, int *, int *, int *, int *, int *, int *, int*);
void pyclassfiller_code_calib   (int *, int *, int *, int *);

PyObject* pyclassfiller_fileout_open_C  (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_fileout_close_C (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_obs_reset_C     (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_obs_write_C     (PyObject *self, PyObject *args, PyObject *kwds);

PyObject* pyclassfiller_set_presec_C    (PyObject *self, PyObject *args, PyObject *kwds);

PyObject* pyclassfiller_set_gen_num_C     (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_gen_ver_C     (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_gen_teles_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_gen_dobs_C    (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_gen_dred_C    (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_gen_kind_C    (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_gen_qual_C    (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_gen_scan_C    (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_gen_subscan_C (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_gen_ut_C      (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_gen_st_C      (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_gen_az_C      (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_gen_el_C      (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_gen_tau_C     (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_gen_tsys_C    (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_gen_time_C    (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_gen_xunit_C   (PyObject *self, PyObject *args, PyObject *kwds);

PyObject* pyclassfiller_set_pos_sourc_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_pos_system_C  (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_pos_equinox_C (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_pos_proj_C    (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_pos_lam_C     (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_pos_bet_C     (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_pos_projang_C (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_pos_lamof_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_pos_betof_C   (PyObject *self, PyObject *args, PyObject *kwds);

PyObject* pyclassfiller_set_spe_line_C    (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_spe_nchan_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_spe_restf_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_spe_image_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_spe_doppler_C (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_spe_rchan_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_spe_fres_C    (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_spe_vres_C    (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_spe_voff_C    (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_spe_bad_C     (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_spe_vtype_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_spe_vconv_C   (PyObject *self, PyObject *args, PyObject *kwds);

PyObject* pyclassfiller_set_swi_nphas_C  (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_swi_decal_C  (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_swi_duree_C  (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_swi_poids_C  (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_swi_swmod_C  (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_swi_ldecal_C (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_swi_bdecal_C (PyObject *self, PyObject *args, PyObject *kwds);

PyObject* pyclassfiller_set_cal_beeff_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_cal_foeff_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_cal_gaini_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_cal_h2omm_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_cal_pamb_C    (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_cal_tamb_C    (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_cal_tatms_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_cal_tchop_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_cal_tcold_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_cal_taus_C    (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_cal_taui_C    (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_cal_tatmi_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_cal_trec_C    (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_cal_cmode_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_cal_atfac_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_cal_alti_C    (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_cal_count_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_cal_lcalof_C  (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_cal_bcalof_C  (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_cal_geolong_C (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_cal_geolat_C  (PyObject *self, PyObject *args, PyObject *kwds);

PyObject* pyclassfiller_set_dri_freq_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_dri_width_C  (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_dri_npoin_C  (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_dri_rpoin_C  (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_dri_tref_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_dri_aref_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_dri_apos_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_dri_tres_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_dri_ares_C   (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_dri_bad_C    (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_dri_ctype_C  (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_dri_cimag_C  (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_dri_colla_C  (PyObject *self, PyObject *args, PyObject *kwds);
PyObject* pyclassfiller_set_dri_colle_C  (PyObject *self, PyObject *args, PyObject *kwds);

PyObject* pyclassfiller_set_datay_C (PyObject *self, PyObject *args, PyObject *kwds);


/**
 * Define the methods table associated to pyclassfiller +Python module+.
 */
static PyMethodDef pyclassfiller_methods[] = {
  {"fileout_open",  (PyCFunction)pyclassfiller_fileout_open_C,  METH_VARARGS|METH_KEYWORDS, "Some description"},
  {"fileout_close", (PyCFunction)pyclassfiller_fileout_close_C, METH_VARARGS|METH_KEYWORDS, "Some description"},
  {"obs_reset",     (PyCFunction)pyclassfiller_obs_reset_C,     METH_VARARGS|METH_KEYWORDS, "Some description"},
  {"obs_write",     (PyCFunction)pyclassfiller_obs_write_C,     METH_VARARGS|METH_KEYWORDS, "Some description"},
  /* Present section */
  {"set_presec",    (PyCFunction)pyclassfiller_set_presec_C,    METH_VARARGS, "Some description"},
  /* Section General */
  {"set_gen_num",     (PyCFunction)pyclassfiller_set_gen_num_C,     METH_VARARGS, "Some description"},
  {"set_gen_ver",     (PyCFunction)pyclassfiller_set_gen_ver_C,     METH_VARARGS, "Some description"},
  {"set_gen_teles",   (PyCFunction)pyclassfiller_set_gen_teles_C,   METH_VARARGS, "Some description"},
  {"set_gen_dobs",    (PyCFunction)pyclassfiller_set_gen_dobs_C,    METH_VARARGS, "Some description"},
  {"set_gen_dred",    (PyCFunction)pyclassfiller_set_gen_dred_C,    METH_VARARGS, "Some description"},
  {"set_gen_kind",    (PyCFunction)pyclassfiller_set_gen_kind_C,    METH_VARARGS, "Some description"},
  {"set_gen_qual",    (PyCFunction)pyclassfiller_set_gen_qual_C,    METH_VARARGS, "Some description"},
  {"set_gen_scan",    (PyCFunction)pyclassfiller_set_gen_scan_C,    METH_VARARGS, "Some description"},
  {"set_gen_subscan", (PyCFunction)pyclassfiller_set_gen_subscan_C, METH_VARARGS, "Some description"},
  {"set_gen_ut",      (PyCFunction)pyclassfiller_set_gen_ut_C,      METH_VARARGS, "Some description"},
  {"set_gen_st",      (PyCFunction)pyclassfiller_set_gen_st_C,      METH_VARARGS, "Some description"},
  {"set_gen_az",      (PyCFunction)pyclassfiller_set_gen_az_C,      METH_VARARGS, "Some description"},
  {"set_gen_el",      (PyCFunction)pyclassfiller_set_gen_el_C,      METH_VARARGS, "Some description"},
  {"set_gen_tau",     (PyCFunction)pyclassfiller_set_gen_tau_C,     METH_VARARGS, "Some description"},
  {"set_gen_tsys",    (PyCFunction)pyclassfiller_set_gen_tsys_C,    METH_VARARGS, "Some description"},
  {"set_gen_time",    (PyCFunction)pyclassfiller_set_gen_time_C,    METH_VARARGS, "Some description"},
  {"set_gen_xunit",   (PyCFunction)pyclassfiller_set_gen_xunit_C,   METH_VARARGS, "Some description"},
  /* Section Position */
  {"set_pos_sourc",   (PyCFunction)pyclassfiller_set_pos_sourc_C,   METH_VARARGS, "Some description"},
  {"set_pos_system",  (PyCFunction)pyclassfiller_set_pos_system_C,  METH_VARARGS, "Some description"},
  {"set_pos_equinox", (PyCFunction)pyclassfiller_set_pos_equinox_C, METH_VARARGS, "Some description"},
  {"set_pos_proj",    (PyCFunction)pyclassfiller_set_pos_proj_C,    METH_VARARGS, "Some description"},
  {"set_pos_lam",     (PyCFunction)pyclassfiller_set_pos_lam_C,     METH_VARARGS, "Some description"},
  {"set_pos_bet",     (PyCFunction)pyclassfiller_set_pos_bet_C,     METH_VARARGS, "Some description"},
  {"set_pos_projang", (PyCFunction)pyclassfiller_set_pos_projang_C, METH_VARARGS, "Some description"},
  {"set_pos_lamof",   (PyCFunction)pyclassfiller_set_pos_lamof_C,   METH_VARARGS, "Some description"},
  {"set_pos_betof",   (PyCFunction)pyclassfiller_set_pos_betof_C,   METH_VARARGS, "Some description"},
  /* Section Spectroscopy */
  {"set_spe_line",    (PyCFunction)pyclassfiller_set_spe_line_C,    METH_VARARGS, "Some description"},
  {"set_spe_nchan",   (PyCFunction)pyclassfiller_set_spe_nchan_C,   METH_VARARGS, "Some description"},
  {"set_spe_restf",   (PyCFunction)pyclassfiller_set_spe_restf_C,   METH_VARARGS, "Some description"},
  {"set_spe_image",   (PyCFunction)pyclassfiller_set_spe_image_C,   METH_VARARGS, "Some description"},
  {"set_spe_doppler", (PyCFunction)pyclassfiller_set_spe_doppler_C, METH_VARARGS, "Some description"},
  {"set_spe_rchan",   (PyCFunction)pyclassfiller_set_spe_rchan_C,   METH_VARARGS, "Some description"},
  {"set_spe_fres",    (PyCFunction)pyclassfiller_set_spe_fres_C,    METH_VARARGS, "Some description"},
  {"set_spe_vres",    (PyCFunction)pyclassfiller_set_spe_vres_C,    METH_VARARGS, "Some description"},
  {"set_spe_voff",    (PyCFunction)pyclassfiller_set_spe_voff_C,    METH_VARARGS, "Some description"},
  {"set_spe_bad",     (PyCFunction)pyclassfiller_set_spe_bad_C,     METH_VARARGS, "Some description"},
  {"set_spe_vtype",   (PyCFunction)pyclassfiller_set_spe_vtype_C,   METH_VARARGS, "Some description"},
  {"set_spe_vconv",   (PyCFunction)pyclassfiller_set_spe_vconv_C,   METH_VARARGS, "Some description"},
  /* Section Frequency Switch */
  {"set_fsw_nphas",   (PyCFunction)pyclassfiller_set_swi_nphas_C,   METH_VARARGS, "Some description"},
  {"set_fsw_decal",   (PyCFunction)pyclassfiller_set_swi_decal_C,   METH_VARARGS, "Some description"},
  {"set_fsw_duree",   (PyCFunction)pyclassfiller_set_swi_duree_C,   METH_VARARGS, "Some description"},
  {"set_fsw_poids",   (PyCFunction)pyclassfiller_set_swi_poids_C,   METH_VARARGS, "Some description"},
  {"set_fsw_swmod",   (PyCFunction)pyclassfiller_set_swi_swmod_C,   METH_VARARGS, "Some description"},
  {"set_fsw_ldecal",  (PyCFunction)pyclassfiller_set_swi_ldecal_C,  METH_VARARGS, "Some description"},
  {"set_fsw_bdecal",  (PyCFunction)pyclassfiller_set_swi_bdecal_C,  METH_VARARGS, "Some description"},
  /* Section Calibration */
  {"set_cal_beeff",   (PyCFunction)pyclassfiller_set_cal_beeff_C,   METH_VARARGS, "Some description"},
  {"set_cal_foeff",   (PyCFunction)pyclassfiller_set_cal_foeff_C,   METH_VARARGS, "Some description"},
  {"set_cal_gaini",   (PyCFunction)pyclassfiller_set_cal_gaini_C,   METH_VARARGS, "Some description"},
  {"set_cal_h2omm",   (PyCFunction)pyclassfiller_set_cal_h2omm_C,   METH_VARARGS, "Some description"},
  {"set_cal_pamb",    (PyCFunction)pyclassfiller_set_cal_pamb_C,    METH_VARARGS, "Some description"},
  {"set_cal_tamb",    (PyCFunction)pyclassfiller_set_cal_tamb_C,    METH_VARARGS, "Some description"},
  {"set_cal_tatms",   (PyCFunction)pyclassfiller_set_cal_tatms_C,   METH_VARARGS, "Some description"},
  {"set_cal_tchop",   (PyCFunction)pyclassfiller_set_cal_tchop_C,   METH_VARARGS, "Some description"},
  {"set_cal_tcold",   (PyCFunction)pyclassfiller_set_cal_tcold_C,   METH_VARARGS, "Some description"},
  {"set_cal_taus",    (PyCFunction)pyclassfiller_set_cal_taus_C,    METH_VARARGS, "Some description"},
  {"set_cal_taui",    (PyCFunction)pyclassfiller_set_cal_taui_C,    METH_VARARGS, "Some description"},
  {"set_cal_tatmi",   (PyCFunction)pyclassfiller_set_cal_tatmi_C,   METH_VARARGS, "Some description"},
  {"set_cal_trec",    (PyCFunction)pyclassfiller_set_cal_trec_C,    METH_VARARGS, "Some description"},
  {"set_cal_cmode",   (PyCFunction)pyclassfiller_set_cal_cmode_C,   METH_VARARGS, "Some description"},
  {"set_cal_atfac",   (PyCFunction)pyclassfiller_set_cal_atfac_C,   METH_VARARGS, "Some description"},
  {"set_cal_alti",    (PyCFunction)pyclassfiller_set_cal_alti_C,    METH_VARARGS, "Some description"},
  {"set_cal_count",   (PyCFunction)pyclassfiller_set_cal_count_C,   METH_VARARGS, "Some description"},
  {"set_cal_lcalof",  (PyCFunction)pyclassfiller_set_cal_lcalof_C,  METH_VARARGS, "Some description"},
  {"set_cal_bcalof",  (PyCFunction)pyclassfiller_set_cal_bcalof_C,  METH_VARARGS, "Some description"},
  {"set_cal_geolong", (PyCFunction)pyclassfiller_set_cal_geolong_C, METH_VARARGS, "Some description"},
  {"set_cal_geolat",  (PyCFunction)pyclassfiller_set_cal_geolat_C,  METH_VARARGS, "Some description"},
  /* Section Continuum Drift */
  {"set_dri_freq",  (PyCFunction)pyclassfiller_set_dri_freq_C,  METH_VARARGS, "Some description"},
  {"set_dri_width", (PyCFunction)pyclassfiller_set_dri_width_C, METH_VARARGS, "Some description"},
  {"set_dri_npoin", (PyCFunction)pyclassfiller_set_dri_npoin_C, METH_VARARGS, "Some description"},
  {"set_dri_rpoin", (PyCFunction)pyclassfiller_set_dri_rpoin_C, METH_VARARGS, "Some description"},
  {"set_dri_tref",  (PyCFunction)pyclassfiller_set_dri_tref_C,  METH_VARARGS, "Some description"},
  {"set_dri_aref",  (PyCFunction)pyclassfiller_set_dri_aref_C,  METH_VARARGS, "Some description"},
  {"set_dri_apos",  (PyCFunction)pyclassfiller_set_dri_apos_C,  METH_VARARGS, "Some description"},
  {"set_dri_tres",  (PyCFunction)pyclassfiller_set_dri_tres_C,  METH_VARARGS, "Some description"},
  {"set_dri_ares",  (PyCFunction)pyclassfiller_set_dri_ares_C,  METH_VARARGS, "Some description"},
  {"set_dri_bad",   (PyCFunction)pyclassfiller_set_dri_bad_C,   METH_VARARGS, "Some description"},
  {"set_dri_ctype", (PyCFunction)pyclassfiller_set_dri_ctype_C, METH_VARARGS, "Some description"},
  {"set_dri_cimag", (PyCFunction)pyclassfiller_set_dri_cimag_C, METH_VARARGS, "Some description"},
  {"set_dri_colla", (PyCFunction)pyclassfiller_set_dri_colla_C, METH_VARARGS, "Some description"},
  {"set_dri_colle", (PyCFunction)pyclassfiller_set_dri_colle_C, METH_VARARGS, "Some description"},
  /* Data */
  {"set_datay", (PyCFunction)pyclassfiller_set_datay_C, METH_VARARGS, "Some description"},
  {NULL}  /* Sentinel */
};


/**
 * _core.so is an internal binary module of the package 'pyclassfiller'
 * Here is its initialization routine
 *
 * @return nothing.
 */
/* Macros loaded from gpackage-pyimport.h */
MOD_INIT(_core) {
  int error;
  PyObject *m;
  char *module_doc;
  /* Codes: */
  int section_user, section_gen, section_pos, section_spe, section_bas, section_his,
      section_plo, section_swi, section_gau, section_dri, section_bea, section_she,
      section_nh3, section_abs, section_cal, section_poi, section_sky;
  int coord_un, coord_eq, coord_ga, coord_ho;
  int kind_spec, kind_cont, kind_sky, kind_onoff, kind_focus;
  int xunit_velo, xunit_freq, xunit_wave;
  int proj_none, proj_gnomonic, proj_ortho, proj_azimuthal, proj_stereo,
      proj_lambert, proj_aitoff, proj_radio, proj_sfl, proj_mollweide, proj_ncp,
      proj_cartesian;
  int velo_unk, velo_lsr, velo_hel, velo_obs, velo_ear, velo_aut;
  int conv_unk, conv_rad, conv_opt, conv_30m;
  int qual_unk, qual_exc, qual_goo, qual_fai, qual_ave, qual_poo, qual_bad,
      qual_awf, qual_wor, qual_del;
  int switch_freq, switch_posi, switch_fold;
  int calib_auto, calib_trec, calib_humi, calib_manu;
  MOD_DEF_DECL("_core", pyclassfiller_methods)

  /* Initialize the libclass (for writing only) */
  error = 0;
  class_write_init(&error);
  if (error) {
    PyErr_SetString(PyExc_Exception,"Error while initializing Class for writing");
    return MOD_ERROR_VAL;
  }

  /* Initialize the observation buffer */
  error = 0;
  pyclassfiller_obs_init(&error);
  if (error) {
    PyErr_SetString(PyExc_Exception,"Error while initializing the observation buffer");
    return MOD_ERROR_VAL;
  }

  /* Register cleaning function (executed when leaving Python) */
  error = Py_AtExit(clean_core);
  if (error) {
    PyErr_SetString(PyExc_Exception,"Error while registering cleaning function");
    return MOD_ERROR_VAL;
  }

  /* Creates the module and add the its methods */
  module_doc = "Some doc";

  MOD_DEF_CODE(m, "_core", module_doc, pyclassfiller_methods)

  /* Checks for errors */
  if (PyErr_Occurred()) {
      PyErr_Print();
      Py_FatalError("E-PYTHON,  Could not initialize '_core' module");
  }

  /* Imports the array (and ufunc) objects */
  import_array();

  /* Gildas codes */
  pyclassfiller_code_coord(&coord_un,&coord_eq,&coord_ga,&coord_ho);
  PyModule_AddIntConstant(m,"code_coord_unk",(long) coord_un);
  PyModule_AddIntConstant(m,"code_coord_equ",(long) coord_eq);
  PyModule_AddIntConstant(m,"code_coord_gal",(long) coord_ga);
  PyModule_AddIntConstant(m,"code_coord_hor",(long) coord_ho);

  pyclassfiller_code_kind(&kind_spec,&kind_cont,&kind_sky,&kind_onoff,&kind_focus);
  PyModule_AddIntConstant(m,"code_kind_spec",(long) kind_spec);
  PyModule_AddIntConstant(m,"code_kind_cont",(long) kind_cont);
  PyModule_AddIntConstant(m,"code_kind_sky",(long) kind_sky);
  PyModule_AddIntConstant(m,"code_kind_onoff",(long) kind_onoff);
  PyModule_AddIntConstant(m,"code_kind_focus",(long) kind_focus);

  pyclassfiller_code_switch(&switch_freq,&switch_posi,&switch_fold);
  PyModule_AddIntConstant(m,"code_switch_freq",(long) switch_freq);
  PyModule_AddIntConstant(m,"code_switch_posi",(long) switch_posi);
  PyModule_AddIntConstant(m,"code_switch_fold",(long) switch_fold);

  pyclassfiller_code_xunit(&xunit_velo,&xunit_freq,&xunit_wave);
  PyModule_AddIntConstant(m,"code_xunit_velo",(long) xunit_velo);
  PyModule_AddIntConstant(m,"code_xunit_freq",(long) xunit_freq);
  PyModule_AddIntConstant(m,"code_xunit_wave",(long) xunit_wave);

  pyclassfiller_code_proj(&proj_none,&proj_gnomonic,&proj_ortho,&proj_azimuthal,
    &proj_stereo,&proj_lambert,&proj_aitoff,&proj_radio,&proj_sfl,
    &proj_mollweide,&proj_ncp,&proj_cartesian);
  PyModule_AddIntConstant(m,"code_proj_none",(long) proj_none);
  PyModule_AddIntConstant(m,"code_proj_gnomonic",(long) proj_gnomonic);
  PyModule_AddIntConstant(m,"code_proj_ortho",(long) proj_ortho);
  PyModule_AddIntConstant(m,"code_proj_azimuthal",(long) proj_azimuthal);
  PyModule_AddIntConstant(m,"code_proj_stereo",(long) proj_stereo);
  PyModule_AddIntConstant(m,"code_proj_lambert",(long) proj_lambert);
  PyModule_AddIntConstant(m,"code_proj_aitoff",(long) proj_aitoff);
  PyModule_AddIntConstant(m,"code_proj_radio",(long) proj_radio);
  PyModule_AddIntConstant(m,"code_proj_sfl",(long) proj_sfl);
  PyModule_AddIntConstant(m,"code_proj_mollweide",(long) proj_mollweide);
  PyModule_AddIntConstant(m,"code_proj_ncp",(long) proj_ncp);
  PyModule_AddIntConstant(m,"code_proj_cartesian",(long) proj_cartesian);

  pyclassfiller_code_velo(&velo_unk,&velo_lsr,&velo_hel,&velo_obs,&velo_ear,&velo_aut);
  PyModule_AddIntConstant(m,"code_velo_unk",(long) velo_unk);
  PyModule_AddIntConstant(m,"code_velo_lsr",(long) velo_lsr);
  PyModule_AddIntConstant(m,"code_velo_helio",(long) velo_hel);
  PyModule_AddIntConstant(m,"code_velo_obs",(long) velo_obs);
  PyModule_AddIntConstant(m,"code_velo_earth",(long) velo_ear);
  PyModule_AddIntConstant(m,"code_velo_auto",(long) velo_aut);

  pyclassfiller_code_conv(&conv_unk,&conv_rad,&conv_opt,&conv_30m);
  PyModule_AddIntConstant(m,"code_conv_unk",(long) conv_unk);
  PyModule_AddIntConstant(m,"code_conv_rad",(long) conv_rad);
  PyModule_AddIntConstant(m,"code_conv_opt",(long) conv_opt);
  PyModule_AddIntConstant(m,"code_conv_30m",(long) conv_30m);

  /* Class codes */
  pyclassfiller_code_section(&section_user,&section_gen,&section_pos,&section_spe,
    &section_bas,&section_his,&section_plo,&section_swi,&section_gau,&section_dri,
    &section_bea,&section_she,&section_nh3,&section_abs,&section_cal,&section_poi,
    &section_sky);
  PyModule_AddIntConstant(m,"code_section_user",(long) section_user);
  PyModule_AddIntConstant(m,"code_section_gen", (long) section_gen);
  PyModule_AddIntConstant(m,"code_section_pos", (long) section_pos);
  PyModule_AddIntConstant(m,"code_section_spe", (long) section_spe);
  PyModule_AddIntConstant(m,"code_section_bas", (long) section_bas);
  PyModule_AddIntConstant(m,"code_section_his", (long) section_his);
  PyModule_AddIntConstant(m,"code_section_plo", (long) section_plo);
  PyModule_AddIntConstant(m,"code_section_fsw", (long) section_swi);
  PyModule_AddIntConstant(m,"code_section_gau", (long) section_gau);
  PyModule_AddIntConstant(m,"code_section_dri", (long) section_dri);
  PyModule_AddIntConstant(m,"code_section_bea", (long) section_bea);
  PyModule_AddIntConstant(m,"code_section_she", (long) section_she);
  PyModule_AddIntConstant(m,"code_section_nh3", (long) section_nh3);
  PyModule_AddIntConstant(m,"code_section_abs", (long) section_abs);
  PyModule_AddIntConstant(m,"code_section_cal", (long) section_cal);
  PyModule_AddIntConstant(m,"code_section_poi", (long) section_poi);
  PyModule_AddIntConstant(m,"code_section_sky", (long) section_sky);

  pyclassfiller_code_qual(&qual_unk,&qual_exc,&qual_goo,&qual_fai,&qual_ave,
                          &qual_poo,&qual_bad,&qual_awf,&qual_wor,&qual_del);
  PyModule_AddIntConstant(m,"code_qual_unknown",(long) qual_unk);
  PyModule_AddIntConstant(m,"code_qual_excellent",(long) qual_exc);
  PyModule_AddIntConstant(m,"code_qual_good",(long) qual_goo);
  PyModule_AddIntConstant(m,"code_qual_fair",(long) qual_fai);
  PyModule_AddIntConstant(m,"code_qual_average",(long) qual_ave);
  PyModule_AddIntConstant(m,"code_qual_poor",(long) qual_poo);
  PyModule_AddIntConstant(m,"code_qual_bad",(long) qual_bad);
  PyModule_AddIntConstant(m,"code_qual_awful",(long) qual_awf);
  PyModule_AddIntConstant(m,"code_qual_worst",(long) qual_wor);
  PyModule_AddIntConstant(m,"code_qual_deleted",(long) qual_del);

  pyclassfiller_code_calib(&calib_auto,&calib_trec,&calib_humi,&calib_manu);
  PyModule_AddIntConstant(m,"code_calib_auto",(long) calib_auto);
  PyModule_AddIntConstant(m,"code_calib_trec",(long) calib_trec);
  PyModule_AddIntConstant(m,"code_calib_humi",(long) calib_humi);
  PyModule_AddIntConstant(m,"code_calib_manu",(long) calib_manu);

  return MOD_SUCCESS_VAL(m);
}

void clean_core( void ) {
  int error;

  /* Tell the module to call the obs buffer cleaning subroutine at exit time */
  error = 0;
  pyclassfiller_obs_clean(&error);
  if (error) {
    PyErr_SetString(PyExc_Exception,"Error while cleaning the observation buffer");
    return;
  }

  error = 0;
  class_write_clean(&error);
  if (error) {
    PyErr_SetString(PyExc_Exception,"Error while cleaning Class");
    return;
  }

}
