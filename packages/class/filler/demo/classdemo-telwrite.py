import pyclassfiller
from pyclassfiller import code  # The various codes needed by the Class Data Format
import numpy   # The Y data (and CAL%COUNT) must be numpy arrays

fileout = pyclassfiller.ClassFileOut()
fileout.open(file='classdemo.30m',new=True,over=True,size=999999,single=True)

obs = pyclassfiller.ClassObservation()
obs.head.presec[:]            = False  # Disable all sections except...
obs.head.presec[code.sec.gen] = True   # General
obs.head.presec[code.sec.pos] = True   # Position
obs.head.presec[code.sec.spe] = True   # Spectroscopy
nchan = 128

for i in range(100):
    # Fill the observation with dummy values
    #
    obs.head.gen.num = 0
    obs.head.gen.ver = 0
    obs.head.gen.teles = "MYTELES%s" % (i+1)
    obs.head.gen.dobs = 0
    obs.head.gen.dred = 0
    obs.head.gen.kind = code.kind.spec
    obs.head.gen.qual = code.qual.unknown
    obs.head.gen.scan = 1
    obs.head.gen.subscan = 1
    obs.head.gen.ut = 0.
    obs.head.gen.st = 0.
    obs.head.gen.az = 0.
    obs.head.gen.el = 0.
    obs.head.gen.tau = 0.
    obs.head.gen.tsys = 100.
    obs.head.gen.time = 100.
    obs.head.gen.xunit = code.xunit.velo  # Unused here
    #
    obs.head.pos.sourc = "MYSOURCE%s" % (i+1)
    obs.head.pos.system = code.coord.equ
    obs.head.pos.equinox = 2000.0
    obs.head.pos.proj = code.proj.none
    obs.head.pos.lam = numpy.pi/2.
    obs.head.pos.bet = numpy.pi/2.
    obs.head.pos.projang = 0.
    obs.head.pos.lamof = 0.
    obs.head.pos.betof = 0.
    #
    obs.head.spe.line  = "MYLINE%s" % (i+1)
    obs.head.spe.restf = 123456.7
    obs.head.spe.nchan = nchan
    obs.head.spe.rchan = 1.
    obs.head.spe.fres = 1.
    obs.head.spe.vres = -1.
    obs.head.spe.voff = 0.
    obs.head.spe.bad = -1000.
    obs.head.spe.image = 98765.
    obs.head.spe.vtype = code.velo.obs
    obs.head.spe.vconv = code.conv.rad
    obs.head.spe.doppler = 0.
    #
    obs.datay = numpy.sin(2.*numpy.pi / nchan *
                  numpy.array(range(nchan),dtype=numpy.float32))
    #
    obs.write()

fileout.close()

del obs,fileout
