module classfiller_interfaces_none
  interface
    subroutine pyclassfiller_code_coord(un,eq,ga,ho)
      use gbl_constant
      integer(kind=4), intent(out) :: un
      integer(kind=4), intent(out) :: eq
      integer(kind=4), intent(out) :: ga
      integer(kind=4), intent(out) :: ho
    end subroutine pyclassfiller_code_coord
  end interface
  !
  interface
    subroutine pyclassfiller_code_kind(spec,cont,sky,onoff,focus)
      use gbl_constant
      integer(kind=4), intent(out) :: spec
      integer(kind=4), intent(out) :: cont
      integer(kind=4), intent(out) :: sky
      integer(kind=4), intent(out) :: onoff
      integer(kind=4), intent(out) :: focus
    end subroutine pyclassfiller_code_kind
  end interface
  !
  interface
    subroutine pyclassfiller_code_xunit(velo,freq,wave)
      use gbl_constant
      integer(kind=4), intent(out) :: velo
      integer(kind=4), intent(out) :: freq
      integer(kind=4), intent(out) :: wave
    end subroutine pyclassfiller_code_xunit
  end interface
  !
  interface
    subroutine pyclassfiller_code_switch(freq,posi,fold)
      use gbl_constant
      integer(kind=4), intent(out) :: freq
      integer(kind=4), intent(out) :: posi
      integer(kind=4), intent(out) :: fold
    end subroutine pyclassfiller_code_switch
  end interface
  !
  interface
    subroutine pyclassfiller_code_proj(none,gnomonic,ortho,azimuthal,stereo,lambert,  &
      aitoff,radio,sfl,mollweide,ncp,cartesian)
      use gbl_constant
      integer(kind=4), intent(out) :: none
      integer(kind=4), intent(out) :: gnomonic
      integer(kind=4), intent(out) :: ortho
      integer(kind=4), intent(out) :: azimuthal
      integer(kind=4), intent(out) :: stereo
      integer(kind=4), intent(out) :: lambert
      integer(kind=4), intent(out) :: aitoff
      integer(kind=4), intent(out) :: radio
      integer(kind=4), intent(out) :: sfl
      integer(kind=4), intent(out) :: mollweide
      integer(kind=4), intent(out) :: ncp
      integer(kind=4), intent(out) :: cartesian
    end subroutine pyclassfiller_code_proj
  end interface
  !
  interface
    subroutine pyclassfiller_code_velo(unk,lsr,hel,obs,ear,aut)
      use gbl_constant
      integer(kind=4), intent(out) :: unk
      integer(kind=4), intent(out) :: lsr
      integer(kind=4), intent(out) :: hel
      integer(kind=4), intent(out) :: obs
      integer(kind=4), intent(out) :: ear
      integer(kind=4), intent(out) :: aut
    end subroutine pyclassfiller_code_velo
  end interface
  !
  interface
    subroutine pyclassfiller_code_conv(unk,rad,opt,thi)
      use gbl_constant
      integer(kind=4), intent(out) :: unk
      integer(kind=4), intent(out) :: rad
      integer(kind=4), intent(out) :: opt
      integer(kind=4), intent(out) :: thi
    end subroutine pyclassfiller_code_conv
  end interface
  !
  interface
    subroutine pyclassfiller_code_section(user,gen,pos,spe,bas,his,plo,swi,  &
      gau,dri,bea,she,nh3,abs,cal,poi,sky)
      use class_parameter
      integer(kind=4), intent(out) :: user
      integer(kind=4), intent(out) :: gen
      integer(kind=4), intent(out) :: pos
      integer(kind=4), intent(out) :: spe
      integer(kind=4), intent(out) :: bas
      integer(kind=4), intent(out) :: his
      integer(kind=4), intent(out) :: plo
      integer(kind=4), intent(out) :: swi
      integer(kind=4), intent(out) :: gau
      integer(kind=4), intent(out) :: dri
      integer(kind=4), intent(out) :: bea
      integer(kind=4), intent(out) :: she
      integer(kind=4), intent(out) :: nh3
      integer(kind=4), intent(out) :: abs
      integer(kind=4), intent(out) :: cal
      integer(kind=4), intent(out) :: poi
      integer(kind=4), intent(out) :: sky
    end subroutine pyclassfiller_code_section
  end interface
  !
  interface
    subroutine pyclassfiller_code_qual(unk,exc,goo,fai,ave,poo,bad,awf,wor,del)
      use class_parameter
      integer(kind=4), intent(out) :: unk
      integer(kind=4), intent(out) :: exc
      integer(kind=4), intent(out) :: goo
      integer(kind=4), intent(out) :: fai
      integer(kind=4), intent(out) :: ave
      integer(kind=4), intent(out) :: poo
      integer(kind=4), intent(out) :: bad
      integer(kind=4), intent(out) :: awf
      integer(kind=4), intent(out) :: wor
      integer(kind=4), intent(out) :: del
    end subroutine pyclassfiller_code_qual
  end interface
  !
  interface
    subroutine pyclassfiller_code_calib(auto,trec,humi,manu)
      use class_calibr
      integer(kind=4), intent(out) :: auto
      integer(kind=4), intent(out) :: trec
      integer(kind=4), intent(out) :: humi
      integer(kind=4), intent(out) :: manu
    end subroutine pyclassfiller_code_calib
  end interface
  !
  interface
    subroutine pyclassfiller_obs_init(error)
      use class_api
      use pyclassfiller_data
      !---------------------------------------------------------------------
      !
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  !
    end subroutine pyclassfiller_obs_init
  end interface
  !
  interface
    subroutine pyclassfiller_obs_reset(nchan,error)
      use class_api
      use pyclassfiller_data
      !---------------------------------------------------------------------
      !
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: nchan  !
      logical,         intent(inout) :: error  !
    end subroutine pyclassfiller_obs_reset
  end interface
  !
  interface
    subroutine pyclassfiller_obs_write(error)
      use class_api
      use pyclassfiller_data
      !---------------------------------------------------------------------
      !
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  !
    end subroutine pyclassfiller_obs_write
  end interface
  !
  interface
    subroutine pyclassfiller_obs_clean(error)
      use class_api
      use pyclassfiller_data
      !---------------------------------------------------------------------
      !
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  !
    end subroutine pyclassfiller_obs_clean
  end interface
  !
  interface
    subroutine pyclassfiller_set_presec(code,error)
      use class_api
      use pyclassfiller_data
      integer(kind=4), intent(in)    :: code
      logical,         intent(inout) :: error
    end subroutine pyclassfiller_set_presec
  end interface
  !
  interface
    subroutine pyclassfiller_set_gen_num(num,error)
      use pyclassfiller_data
      integer(kind=obsnum_length), intent(in) :: num
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_gen_num
  end interface
  !
  interface
    subroutine pyclassfiller_set_gen_ver(ver,error)
      use pyclassfiller_data
      integer(kind=4), intent(in) :: ver
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_gen_ver
  end interface
  !
  interface
    subroutine pyclassfiller_set_gen_teles(teles,error)
      use pyclassfiller_data
      character(len=*), intent(in) :: teles
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_gen_teles
  end interface
  !
  interface
    subroutine pyclassfiller_set_gen_dobs(dobs,error)
      use pyclassfiller_data
      integer(kind=4), intent(in) :: dobs
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_gen_dobs
  end interface
  !
  interface
    subroutine pyclassfiller_set_gen_dred(dred,error)
      use pyclassfiller_data
      integer(kind=4), intent(in) :: dred
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_gen_dred
  end interface
  !
  interface
    subroutine pyclassfiller_set_gen_kind(kind,error)
      use pyclassfiller_data
      integer(kind=4), intent(in) :: kind
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_gen_kind
  end interface
  !
  interface
    subroutine pyclassfiller_set_gen_qual(qual,error)
      use pyclassfiller_data
      integer(kind=4), intent(in) :: qual
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_gen_qual
  end interface
  !
  interface
    subroutine pyclassfiller_set_gen_scan(scan,error)
      use pyclassfiller_data
      integer(kind=obsnum_length), intent(in) :: scan
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_gen_scan
  end interface
  !
  interface
    subroutine pyclassfiller_set_gen_subscan(subscan,error)
      use pyclassfiller_data
      integer(kind=4), intent(in) :: subscan
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_gen_subscan
  end interface
  !
  interface
    subroutine pyclassfiller_set_gen_ut(ut,error)
      use pyclassfiller_data
      real(kind=8), intent(in) :: ut
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_gen_ut
  end interface
  !
  interface
    subroutine pyclassfiller_set_gen_st(st,error)
      use pyclassfiller_data
      real(kind=8), intent(in) :: st
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_gen_st
  end interface
  !
  interface
    subroutine pyclassfiller_set_gen_az(az,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: az
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_gen_az
  end interface
  !
  interface
    subroutine pyclassfiller_set_gen_el(el,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: el
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_gen_el
  end interface
  !
  interface
    subroutine pyclassfiller_set_gen_tau(tau,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: tau
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_gen_tau
  end interface
  !
  interface
    subroutine pyclassfiller_set_gen_tsys(tsys,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: tsys
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_gen_tsys
  end interface
  !
  interface
    subroutine pyclassfiller_set_gen_time(time,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: time
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_gen_time
  end interface
  !
  interface
    subroutine pyclassfiller_set_gen_xunit(xunit,error)
      use pyclassfiller_data
      integer(kind=4), intent(in) :: xunit
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_gen_xunit
  end interface
  !
  interface
    subroutine pyclassfiller_set_pos_sourc(sourc,error)
      use pyclassfiller_data
      character(len=*), intent(in) :: sourc
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_pos_sourc
  end interface
  !
  interface
    subroutine pyclassfiller_set_pos_system(system,error)
      use pyclassfiller_data
      integer(kind=4), intent(in) :: system
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_pos_system
  end interface
  !
  interface
    subroutine pyclassfiller_set_pos_equinox(equinox,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: equinox
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_pos_equinox
  end interface
  !
  interface
    subroutine pyclassfiller_set_pos_proj(proj,error)
      use pyclassfiller_data
      integer(kind=4), intent(in) :: proj
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_pos_proj
  end interface
  !
  interface
    subroutine pyclassfiller_set_pos_lam(lam,error)
      use pyclassfiller_data
      real(kind=8), intent(in) :: lam
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_pos_lam
  end interface
  !
  interface
    subroutine pyclassfiller_set_pos_bet(bet,error)
      use pyclassfiller_data
      real(kind=8), intent(in) :: bet
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_pos_bet
  end interface
  !
  interface
    subroutine pyclassfiller_set_pos_projang(projang,error)
      use pyclassfiller_data
      real(kind=8), intent(in) :: projang
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_pos_projang
  end interface
  !
  interface
    subroutine pyclassfiller_set_pos_lamof(lamof,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: lamof
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_pos_lamof
  end interface
  !
  interface
    subroutine pyclassfiller_set_pos_betof(betof,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: betof
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_pos_betof
  end interface
  !
  interface
    subroutine pyclassfiller_set_spe_line(line,error)
      use pyclassfiller_data
      character(len=*), intent(in) :: line
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_spe_line
  end interface
  !
  interface
    subroutine pyclassfiller_set_spe_nchan(nchan,error)
      use pyclassfiller_data
      integer(kind=4), intent(in) :: nchan
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_spe_nchan
  end interface
  !
  interface
    subroutine pyclassfiller_set_spe_restf(restf,error)
      use pyclassfiller_data
      real(kind=8), intent(in) :: restf
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_spe_restf
  end interface
  !
  interface
    subroutine pyclassfiller_set_spe_image(image,error)
      use pyclassfiller_data
      real(kind=8), intent(in) :: image
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_spe_image
  end interface
  !
  interface
    subroutine pyclassfiller_set_spe_doppler(doppler,error)
      use pyclassfiller_data
      real(kind=8), intent(in) :: doppler
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_spe_doppler
  end interface
  !
  interface
    subroutine pyclassfiller_set_spe_rchan(rchan,error)
      use pyclassfiller_data
      real(kind=8), intent(in) :: rchan
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_spe_rchan
  end interface
  !
  interface
    subroutine pyclassfiller_set_spe_fres(fres,error)
      use pyclassfiller_data
      real(kind=8), intent(in) :: fres
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_spe_fres
  end interface
  !
  interface
    subroutine pyclassfiller_set_spe_vres(vres,error)
      use pyclassfiller_data
      real(kind=8), intent(in) :: vres
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_spe_vres
  end interface
  !
  interface
    subroutine pyclassfiller_set_spe_voff(voff,error)
      use pyclassfiller_data
      real(kind=8), intent(in) :: voff
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_spe_voff
  end interface
  !
  interface
    subroutine pyclassfiller_set_spe_bad(bad,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: bad
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_spe_bad
  end interface
  !
  interface
    subroutine pyclassfiller_set_spe_vtype(vtype,error)
      use pyclassfiller_data
      integer(kind=4), intent(in) :: vtype
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_spe_vtype
  end interface
  !
  interface
    subroutine pyclassfiller_set_spe_vconv(vconv,error)
      use pyclassfiller_data
      integer(kind=4), intent(in) :: vconv
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_spe_vconv
  end interface
  !
  interface
    subroutine pyclassfiller_set_swi_nphas(nphas,error)
      use pyclassfiller_data
      integer(kind=4), intent(in) :: nphas
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_swi_nphas
  end interface
  !
  interface
    subroutine pyclassfiller_set_swi_decal(decal,nelem,error)
      use class_api
      use pyclassfiller_data
      integer(kind=4), intent(in) :: nelem
      real(kind=8), intent(in) :: decal(nelem)
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_swi_decal
  end interface
  !
  interface
    subroutine pyclassfiller_set_swi_duree(duree,nelem,error)
      use class_api
      use pyclassfiller_data
      integer(kind=4), intent(in) :: nelem
      real(kind=4), intent(in) :: duree(nelem)
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_swi_duree
  end interface
  !
  interface
    subroutine pyclassfiller_set_swi_poids(poids,nelem,error)
      use class_api
      use pyclassfiller_data
      integer(kind=4), intent(in) :: nelem
      real(kind=4), intent(in) :: poids(nelem)
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_swi_poids
  end interface
  !
  interface
    subroutine pyclassfiller_set_swi_swmod(swmod,error)
      use pyclassfiller_data
      integer(kind=4), intent(in) :: swmod
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_swi_swmod
  end interface
  !
  interface
    subroutine pyclassfiller_set_swi_ldecal(ldecal,nelem,error)
      use class_api
      use pyclassfiller_data
      integer(kind=4), intent(in) :: nelem
      real(kind=4), intent(in) :: ldecal(nelem)
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_swi_ldecal
  end interface
  !
  interface
    subroutine pyclassfiller_set_swi_bdecal(bdecal,nelem,error)
      use class_api
      use pyclassfiller_data
      integer(kind=4), intent(in) :: nelem
      real(kind=4), intent(in) :: bdecal(nelem)
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_swi_bdecal
  end interface
  !
  interface
    subroutine pyclassfiller_set_cal_beeff(beeff,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: beeff
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_cal_beeff
  end interface
  !
  interface
    subroutine pyclassfiller_set_cal_foeff(foeff,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: foeff
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_cal_foeff
  end interface
  !
  interface
    subroutine pyclassfiller_set_cal_gaini(gaini,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: gaini
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_cal_gaini
  end interface
  !
  interface
    subroutine pyclassfiller_set_cal_h2omm(h2omm,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: h2omm
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_cal_h2omm
  end interface
  !
  interface
    subroutine pyclassfiller_set_cal_pamb(pamb,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: pamb
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_cal_pamb
  end interface
  !
  interface
    subroutine pyclassfiller_set_cal_tamb(tamb,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: tamb
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_cal_tamb
  end interface
  !
  interface
    subroutine pyclassfiller_set_cal_tatms(tatms,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: tatms
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_cal_tatms
  end interface
  !
  interface
    subroutine pyclassfiller_set_cal_tchop(tchop,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: tchop
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_cal_tchop
  end interface
  !
  interface
    subroutine pyclassfiller_set_cal_tcold(tcold,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: tcold
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_cal_tcold
  end interface
  !
  interface
    subroutine pyclassfiller_set_cal_taus(taus,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: taus
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_cal_taus
  end interface
  !
  interface
    subroutine pyclassfiller_set_cal_taui(taui,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: taui
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_cal_taui
  end interface
  !
  interface
    subroutine pyclassfiller_set_cal_tatmi(tatmi,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: tatmi
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_cal_tatmi
  end interface
  !
  interface
    subroutine pyclassfiller_set_cal_trec(trec,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: trec
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_cal_trec
  end interface
  !
  interface
    subroutine pyclassfiller_set_cal_cmode(cmode,error)
      use pyclassfiller_data
      integer(kind=4), intent(in) :: cmode
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_cal_cmode
  end interface
  !
  interface
    subroutine pyclassfiller_set_cal_atfac(atfac,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: atfac
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_cal_atfac
  end interface
  !
  interface
    subroutine pyclassfiller_set_cal_alti(alti,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: alti
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_cal_alti
  end interface
  !
  interface
    subroutine pyclassfiller_set_cal_count(count,nelem,error)
      use class_api
      use pyclassfiller_data
      integer(kind=4), intent(in) :: nelem
      real(kind=4), intent(in) :: count(nelem)
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_cal_count
  end interface
  !
  interface
    subroutine pyclassfiller_set_cal_lcalof(lcalof,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: lcalof
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_cal_lcalof
  end interface
  !
  interface
    subroutine pyclassfiller_set_cal_bcalof(bcalof,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: bcalof
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_cal_bcalof
  end interface
  !
  interface
    subroutine pyclassfiller_set_cal_geolong(geolong,error)
      use pyclassfiller_data
      real(kind=8), intent(in) :: geolong
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_cal_geolong
  end interface
  !
  interface
    subroutine pyclassfiller_set_cal_geolat(geolat,error)
      use pyclassfiller_data
      real(kind=8), intent(in) :: geolat
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_cal_geolat
  end interface
  !
  interface
    subroutine pyclassfiller_set_dri_freq(freq,error)
      use pyclassfiller_data
      real(kind=8), intent(in) :: freq
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_dri_freq
  end interface
  !
  interface
    subroutine pyclassfiller_set_dri_width(width,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: width
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_dri_width
  end interface
  !
  interface
    subroutine pyclassfiller_set_dri_npoin(npoin,error)
      use pyclassfiller_data
      integer(kind=4), intent(in) :: npoin
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_dri_npoin
  end interface
  !
  interface
    subroutine pyclassfiller_set_dri_rpoin(rpoin,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: rpoin
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_dri_rpoin
  end interface
  !
  interface
    subroutine pyclassfiller_set_dri_tref(tref,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: tref
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_dri_tref
  end interface
  !
  interface
    subroutine pyclassfiller_set_dri_aref(aref,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: aref
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_dri_aref
  end interface
  !
  interface
    subroutine pyclassfiller_set_dri_apos(apos,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: apos
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_dri_apos
  end interface
  !
  interface
    subroutine pyclassfiller_set_dri_tres(tres,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: tres
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_dri_tres
  end interface
  !
  interface
    subroutine pyclassfiller_set_dri_ares(ares,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: ares
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_dri_ares
  end interface
  !
  interface
    subroutine pyclassfiller_set_dri_bad(bad,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: bad
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_dri_bad
  end interface
  !
  interface
    subroutine pyclassfiller_set_dri_ctype(ctype,error)
      use pyclassfiller_data
      integer(kind=4), intent(in) :: ctype
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_dri_ctype
  end interface
  !
  interface
    subroutine pyclassfiller_set_dri_cimag(cimag,error)
      use pyclassfiller_data
      real(kind=8), intent(in) :: cimag
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_dri_cimag
  end interface
  !
  interface
    subroutine pyclassfiller_set_dri_colla(colla,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: colla
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_dri_colla
  end interface
  !
  interface
    subroutine pyclassfiller_set_dri_colle(colle,error)
      use pyclassfiller_data
      real(kind=4), intent(in) :: colle
      logical, intent(inout) :: error
    end subroutine pyclassfiller_set_dri_colle
  end interface
  !
  interface
    subroutine pyclassfiller_set_datay(datay,n,error)
      use pyclassfiller_data
      integer(kind=4), intent(in)    :: n
      real(kind=4),    intent(in)    :: datay(n)
      logical,         intent(inout) :: error
    end subroutine pyclassfiller_set_datay
  end interface
  !
end module classfiller_interfaces_none
