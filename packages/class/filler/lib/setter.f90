!--- Present sections --------------------------------------------------
!
subroutine pyclassfiller_set_presec(code,error)
  use class_api
  use pyclassfiller_data
  integer(kind=4), intent(in)    :: code
  logical,         intent(inout) :: error
  ! Local
  character(len=128) :: mess
  !
  if (code.lt.-mx_sec .or. code.gt.0) then
    write(mess,'(A,I0,A)')  'E-FILLER,  Invalid section code (got ',code,')'
    call gagout(mess)
    error = .true.
    return
  endif
  obs%head%presec(code) = .true.
end subroutine pyclassfiller_set_presec
!
!--- General -----------------------------------------------------------
!
subroutine pyclassfiller_set_gen_num(num,error)
  use pyclassfiller_data
  integer(kind=obsnum_length), intent(in) :: num
  logical, intent(inout) :: error
  obs%head%gen%num = num
end subroutine pyclassfiller_set_gen_num
!
subroutine pyclassfiller_set_gen_ver(ver,error)
  use pyclassfiller_data
  integer(kind=4), intent(in) :: ver
  logical, intent(inout) :: error
  obs%head%gen%ver = ver
end subroutine pyclassfiller_set_gen_ver
!
subroutine pyclassfiller_set_gen_teles(teles,error)
  use pyclassfiller_data
  character(len=*), intent(in) :: teles
  logical, intent(inout) :: error
  obs%head%gen%teles = teles
end subroutine pyclassfiller_set_gen_teles
!
subroutine pyclassfiller_set_gen_dobs(dobs,error)
  use pyclassfiller_data
  integer(kind=4), intent(in) :: dobs
  logical, intent(inout) :: error
  obs%head%gen%dobs = dobs
end subroutine pyclassfiller_set_gen_dobs
!
subroutine pyclassfiller_set_gen_dred(dred,error)
  use pyclassfiller_data
  integer(kind=4), intent(in) :: dred
  logical, intent(inout) :: error
  obs%head%gen%dred = dred
end subroutine pyclassfiller_set_gen_dred
!
subroutine pyclassfiller_set_gen_kind(kind,error)
  use pyclassfiller_data
  integer(kind=4), intent(in) :: kind
  logical, intent(inout) :: error
  obs%head%gen%kind = kind
end subroutine pyclassfiller_set_gen_kind
!
subroutine pyclassfiller_set_gen_qual(qual,error)
  use pyclassfiller_data
  integer(kind=4), intent(in) :: qual
  logical, intent(inout) :: error
  obs%head%gen%qual = qual
end subroutine pyclassfiller_set_gen_qual
!
subroutine pyclassfiller_set_gen_scan(scan,error)
  use pyclassfiller_data
  integer(kind=obsnum_length), intent(in) :: scan
  logical, intent(inout) :: error
  obs%head%gen%scan = scan
end subroutine pyclassfiller_set_gen_scan
!
subroutine pyclassfiller_set_gen_subscan(subscan,error)
  use pyclassfiller_data
  integer(kind=4), intent(in) :: subscan
  logical, intent(inout) :: error
  obs%head%gen%subscan = subscan
end subroutine pyclassfiller_set_gen_subscan
!
subroutine pyclassfiller_set_gen_ut(ut,error)
  use pyclassfiller_data
  real(kind=8), intent(in) :: ut
  logical, intent(inout) :: error
  obs%head%gen%ut = ut
end subroutine pyclassfiller_set_gen_ut
!
subroutine pyclassfiller_set_gen_st(st,error)
  use pyclassfiller_data
  real(kind=8), intent(in) :: st
  logical, intent(inout) :: error
  obs%head%gen%st = st
end subroutine pyclassfiller_set_gen_st
!
subroutine pyclassfiller_set_gen_az(az,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: az
  logical, intent(inout) :: error
  obs%head%gen%az = az
end subroutine pyclassfiller_set_gen_az
!
subroutine pyclassfiller_set_gen_el(el,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: el
  logical, intent(inout) :: error
  obs%head%gen%el = el
end subroutine pyclassfiller_set_gen_el
!
subroutine pyclassfiller_set_gen_tau(tau,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: tau
  logical, intent(inout) :: error
  obs%head%gen%tau = tau
end subroutine pyclassfiller_set_gen_tau
!
subroutine pyclassfiller_set_gen_tsys(tsys,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: tsys
  logical, intent(inout) :: error
  obs%head%gen%tsys = tsys
end subroutine pyclassfiller_set_gen_tsys
!
subroutine pyclassfiller_set_gen_time(time,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: time
  logical, intent(inout) :: error
  obs%head%gen%time = time
end subroutine pyclassfiller_set_gen_time
!
subroutine pyclassfiller_set_gen_xunit(xunit,error)
  use pyclassfiller_data
  integer(kind=4), intent(in) :: xunit
  logical, intent(inout) :: error
  obs%head%gen%xunit = xunit
end subroutine pyclassfiller_set_gen_xunit
!
!--- Position ----------------------------------------------------------
!
subroutine pyclassfiller_set_pos_sourc(sourc,error)
  use pyclassfiller_data
  character(len=*), intent(in) :: sourc
  logical, intent(inout) :: error
  obs%head%pos%sourc = sourc
end subroutine pyclassfiller_set_pos_sourc
!
subroutine pyclassfiller_set_pos_system(system,error)
  use pyclassfiller_data
  integer(kind=4), intent(in) :: system
  logical, intent(inout) :: error
  obs%head%pos%system = system
end subroutine pyclassfiller_set_pos_system
!
subroutine pyclassfiller_set_pos_equinox(equinox,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: equinox
  logical, intent(inout) :: error
  obs%head%pos%equinox = equinox
end subroutine pyclassfiller_set_pos_equinox
!
subroutine pyclassfiller_set_pos_proj(proj,error)
  use pyclassfiller_data
  integer(kind=4), intent(in) :: proj
  logical, intent(inout) :: error
  obs%head%pos%proj = proj
end subroutine pyclassfiller_set_pos_proj
!
subroutine pyclassfiller_set_pos_lam(lam,error)
  use pyclassfiller_data
  real(kind=8), intent(in) :: lam
  logical, intent(inout) :: error
  obs%head%pos%lam = lam
end subroutine pyclassfiller_set_pos_lam
!
subroutine pyclassfiller_set_pos_bet(bet,error)
  use pyclassfiller_data
  real(kind=8), intent(in) :: bet
  logical, intent(inout) :: error
  obs%head%pos%bet = bet
end subroutine pyclassfiller_set_pos_bet
!
subroutine pyclassfiller_set_pos_projang(projang,error)
  use pyclassfiller_data
  real(kind=8), intent(in) :: projang
  logical, intent(inout) :: error
  obs%head%pos%projang = projang
end subroutine pyclassfiller_set_pos_projang
!
subroutine pyclassfiller_set_pos_lamof(lamof,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: lamof
  logical, intent(inout) :: error
  obs%head%pos%lamof = lamof
end subroutine pyclassfiller_set_pos_lamof
!
subroutine pyclassfiller_set_pos_betof(betof,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: betof
  logical, intent(inout) :: error
  obs%head%pos%betof = betof
end subroutine pyclassfiller_set_pos_betof
!
!--- Spectroscopy ------------------------------------------------------
!
subroutine pyclassfiller_set_spe_line(line,error)
  use pyclassfiller_data
  character(len=*), intent(in) :: line
  logical, intent(inout) :: error
  obs%head%spe%line = line
end subroutine pyclassfiller_set_spe_line
!
subroutine pyclassfiller_set_spe_nchan(nchan,error)
  use pyclassfiller_data
  integer(kind=4), intent(in) :: nchan
  logical, intent(inout) :: error
  obs%head%spe%nchan = nchan
end subroutine pyclassfiller_set_spe_nchan
!
subroutine pyclassfiller_set_spe_restf(restf,error)
  use pyclassfiller_data
  real(kind=8), intent(in) :: restf
  logical, intent(inout) :: error
  obs%head%spe%restf = restf
end subroutine pyclassfiller_set_spe_restf
!
subroutine pyclassfiller_set_spe_image(image,error)
  use pyclassfiller_data
  real(kind=8), intent(in) :: image
  logical, intent(inout) :: error
  obs%head%spe%image = image
end subroutine pyclassfiller_set_spe_image
!
subroutine pyclassfiller_set_spe_doppler(doppler,error)
  use pyclassfiller_data
  real(kind=8), intent(in) :: doppler
  logical, intent(inout) :: error
  obs%head%spe%doppler = doppler
end subroutine pyclassfiller_set_spe_doppler
!
subroutine pyclassfiller_set_spe_rchan(rchan,error)
  use pyclassfiller_data
  real(kind=8), intent(in) :: rchan
  logical, intent(inout) :: error
  obs%head%spe%rchan = rchan
end subroutine pyclassfiller_set_spe_rchan
!
subroutine pyclassfiller_set_spe_fres(fres,error)
  use pyclassfiller_data
  real(kind=8), intent(in) :: fres
  logical, intent(inout) :: error
  obs%head%spe%fres = fres
end subroutine pyclassfiller_set_spe_fres
!
subroutine pyclassfiller_set_spe_vres(vres,error)
  use pyclassfiller_data
  real(kind=8), intent(in) :: vres
  logical, intent(inout) :: error
  obs%head%spe%vres = vres
end subroutine pyclassfiller_set_spe_vres
!
subroutine pyclassfiller_set_spe_voff(voff,error)
  use pyclassfiller_data
  real(kind=8), intent(in) :: voff
  logical, intent(inout) :: error
  obs%head%spe%voff = voff
end subroutine pyclassfiller_set_spe_voff
!
subroutine pyclassfiller_set_spe_bad(bad,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: bad
  logical, intent(inout) :: error
  obs%head%spe%bad = bad
end subroutine pyclassfiller_set_spe_bad
!
subroutine pyclassfiller_set_spe_vtype(vtype,error)
  use pyclassfiller_data
  integer(kind=4), intent(in) :: vtype
  logical, intent(inout) :: error
  obs%head%spe%vtype = vtype
end subroutine pyclassfiller_set_spe_vtype
!
subroutine pyclassfiller_set_spe_vconv(vconv,error)
  use pyclassfiller_data
  integer(kind=4), intent(in) :: vconv
  logical, intent(inout) :: error
  obs%head%spe%vconv = vconv
end subroutine pyclassfiller_set_spe_vconv
!
!--- Switching ---------------------------------------------------------
!
subroutine pyclassfiller_set_swi_nphas(nphas,error)
  use pyclassfiller_data
  integer(kind=4), intent(in) :: nphas
  logical, intent(inout) :: error
  obs%head%swi%nphas = nphas
end subroutine pyclassfiller_set_swi_nphas
!
subroutine pyclassfiller_set_swi_decal(decal,nelem,error)
  use class_api
  use pyclassfiller_data
  integer(kind=4), intent(in) :: nelem
  real(kind=8), intent(in) :: decal(nelem)
  logical, intent(inout) :: error
  ! Local
  character(len=128) :: mess
  !
  if (nelem.gt.mxphas) then
    write(mess,'(A,I0,A,I0,A)')  &
      'E-FILLER,  SWI%DECAL must have at most ',mxphas,' values (got ',nelem,')'
    call gagout(mess)
    error = .true.
    return
  endif
  obs%head%swi%decal(:) = 0
  obs%head%swi%decal(1:nelem) = decal(1:nelem)
end subroutine pyclassfiller_set_swi_decal
!
subroutine pyclassfiller_set_swi_duree(duree,nelem,error)
  use class_api
  use pyclassfiller_data
  integer(kind=4), intent(in) :: nelem
  real(kind=4), intent(in) :: duree(nelem)
  logical, intent(inout) :: error
  ! Local
  character(len=128) :: mess
  !
  if (nelem.gt.mxphas) then
    write(mess,'(A,I0,A,I0,A)')  &
      'E-FILLER,  SWI%DUREE must have at most ',mxphas,' values (got ',nelem,')'
    call gagout(mess)
    error = .true.
    return
  endif
  obs%head%swi%duree(:) = 0
  obs%head%swi%duree(1:nelem) = duree(1:nelem)
end subroutine pyclassfiller_set_swi_duree
!
subroutine pyclassfiller_set_swi_poids(poids,nelem,error)
  use class_api
  use pyclassfiller_data
  integer(kind=4), intent(in) :: nelem
  real(kind=4), intent(in) :: poids(nelem)
  logical, intent(inout) :: error
  ! Local
  character(len=128) :: mess
  !
  if (nelem.gt.mxphas) then
    write(mess,'(A,I0,A,I0,A)')  &
      'E-FILLER,  SWI%POIDS must have at most ',mxphas,' values (got ',nelem,')'
    call gagout(mess)
    error = .true.
    return
  endif
  obs%head%swi%poids(:) = 0
  obs%head%swi%poids(1:nelem) = poids(1:nelem)
end subroutine pyclassfiller_set_swi_poids
!
subroutine pyclassfiller_set_swi_swmod(swmod,error)
  use pyclassfiller_data
  integer(kind=4), intent(in) :: swmod
  logical, intent(inout) :: error
  obs%head%swi%swmod = swmod
end subroutine pyclassfiller_set_swi_swmod
!
subroutine pyclassfiller_set_swi_ldecal(ldecal,nelem,error)
  use class_api
  use pyclassfiller_data
  integer(kind=4), intent(in) :: nelem
  real(kind=4), intent(in) :: ldecal(nelem)
  logical, intent(inout) :: error
  ! Local
  character(len=128) :: mess
  !
  if (nelem.gt.mxphas) then
    write(mess,'(A,I0,A,I0,A)')  &
      'E-FILLER,  SWI%LDECAL must have at most ',mxphas,' values (got ',nelem,')'
    call gagout(mess)
    error = .true.
    return
  endif
  obs%head%swi%ldecal(:) = 0
  obs%head%swi%ldecal(1:nelem) = ldecal(1:nelem)
end subroutine pyclassfiller_set_swi_ldecal
!
subroutine pyclassfiller_set_swi_bdecal(bdecal,nelem,error)
  use class_api
  use pyclassfiller_data
  integer(kind=4), intent(in) :: nelem
  real(kind=4), intent(in) :: bdecal(nelem)
  logical, intent(inout) :: error
  ! Local
  character(len=128) :: mess
  !
  if (nelem.gt.mxphas) then
    write(mess,'(A,I0,A,I0,A)')  &
      'E-FILLER,  SWI%BDECAL must have at most ',mxphas,' values (got ',nelem,')'
    call gagout(mess)
    error = .true.
    return
  endif
  obs%head%swi%bdecal(:) = 0
  obs%head%swi%bdecal(1:nelem) = bdecal(1:nelem)
end subroutine pyclassfiller_set_swi_bdecal
!
!--- Calibration -------------------------------------------------------
!
subroutine pyclassfiller_set_cal_beeff(beeff,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: beeff
  logical, intent(inout) :: error
  obs%head%cal%beeff = beeff
end subroutine pyclassfiller_set_cal_beeff
!
subroutine pyclassfiller_set_cal_foeff(foeff,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: foeff
  logical, intent(inout) :: error
  obs%head%cal%foeff = foeff
end subroutine pyclassfiller_set_cal_foeff
!
subroutine pyclassfiller_set_cal_gaini(gaini,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: gaini
  logical, intent(inout) :: error
  obs%head%cal%gaini = gaini
end subroutine pyclassfiller_set_cal_gaini
!
subroutine pyclassfiller_set_cal_h2omm(h2omm,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: h2omm
  logical, intent(inout) :: error
  obs%head%cal%h2omm = h2omm
end subroutine pyclassfiller_set_cal_h2omm
!
subroutine pyclassfiller_set_cal_pamb(pamb,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: pamb
  logical, intent(inout) :: error
  obs%head%cal%pamb = pamb
end subroutine pyclassfiller_set_cal_pamb
!
subroutine pyclassfiller_set_cal_tamb(tamb,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: tamb
  logical, intent(inout) :: error
  obs%head%cal%tamb = tamb
end subroutine pyclassfiller_set_cal_tamb
!
subroutine pyclassfiller_set_cal_tatms(tatms,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: tatms
  logical, intent(inout) :: error
  obs%head%cal%tatms = tatms
end subroutine pyclassfiller_set_cal_tatms
!
subroutine pyclassfiller_set_cal_tchop(tchop,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: tchop
  logical, intent(inout) :: error
  obs%head%cal%tchop = tchop
end subroutine pyclassfiller_set_cal_tchop
!
subroutine pyclassfiller_set_cal_tcold(tcold,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: tcold
  logical, intent(inout) :: error
  obs%head%cal%tcold = tcold
end subroutine pyclassfiller_set_cal_tcold
!
subroutine pyclassfiller_set_cal_taus(taus,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: taus
  logical, intent(inout) :: error
  obs%head%cal%taus = taus
end subroutine pyclassfiller_set_cal_taus
!
subroutine pyclassfiller_set_cal_taui(taui,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: taui
  logical, intent(inout) :: error
  obs%head%cal%taui = taui
end subroutine pyclassfiller_set_cal_taui
!
subroutine pyclassfiller_set_cal_tatmi(tatmi,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: tatmi
  logical, intent(inout) :: error
  obs%head%cal%tatmi = tatmi
end subroutine pyclassfiller_set_cal_tatmi
!
subroutine pyclassfiller_set_cal_trec(trec,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: trec
  logical, intent(inout) :: error
  obs%head%cal%trec = trec
end subroutine pyclassfiller_set_cal_trec
!
subroutine pyclassfiller_set_cal_cmode(cmode,error)
  use pyclassfiller_data
  integer(kind=4), intent(in) :: cmode
  logical, intent(inout) :: error
  obs%head%cal%cmode = cmode
end subroutine pyclassfiller_set_cal_cmode
!
subroutine pyclassfiller_set_cal_atfac(atfac,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: atfac
  logical, intent(inout) :: error
  obs%head%cal%atfac = atfac
end subroutine pyclassfiller_set_cal_atfac
!
subroutine pyclassfiller_set_cal_alti(alti,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: alti
  logical, intent(inout) :: error
  obs%head%cal%alti = alti
end subroutine pyclassfiller_set_cal_alti
!
subroutine pyclassfiller_set_cal_count(count,nelem,error)
  use class_api
  use pyclassfiller_data
  integer(kind=4), intent(in) :: nelem
  real(kind=4), intent(in) :: count(nelem)
  logical, intent(inout) :: error
  ! Local
  character(len=128) :: mess
  !
  if (nelem.ne.3) then
    write(mess,'(A,I0,A,I0,A)')  &
      'E-FILLER,  CAL%COUNT must have exactly ',3,' values (got ',nelem,')'
    call gagout(mess)
    error = .true.
    return
  endif
  obs%head%cal%count(:) = 0
  obs%head%cal%count(1:nelem) = count(1:nelem)
end subroutine pyclassfiller_set_cal_count
!
subroutine pyclassfiller_set_cal_lcalof(lcalof,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: lcalof
  logical, intent(inout) :: error
  obs%head%cal%lcalof = lcalof
end subroutine pyclassfiller_set_cal_lcalof
!
subroutine pyclassfiller_set_cal_bcalof(bcalof,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: bcalof
  logical, intent(inout) :: error
  obs%head%cal%bcalof = bcalof
end subroutine pyclassfiller_set_cal_bcalof
!
subroutine pyclassfiller_set_cal_geolong(geolong,error)
  use pyclassfiller_data
  real(kind=8), intent(in) :: geolong
  logical, intent(inout) :: error
  obs%head%cal%geolong = geolong
end subroutine pyclassfiller_set_cal_geolong
!
subroutine pyclassfiller_set_cal_geolat(geolat,error)
  use pyclassfiller_data
  real(kind=8), intent(in) :: geolat
  logical, intent(inout) :: error
  obs%head%cal%geolat = geolat
end subroutine pyclassfiller_set_cal_geolat
!
!--- Continuum Drift ---------------------------------------------------
!
subroutine pyclassfiller_set_dri_freq(freq,error)
  use pyclassfiller_data
  real(kind=8), intent(in) :: freq
  logical, intent(inout) :: error
  obs%head%dri%freq = freq
end subroutine pyclassfiller_set_dri_freq
!
subroutine pyclassfiller_set_dri_width(width,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: width
  logical, intent(inout) :: error
  obs%head%dri%width = width
end subroutine pyclassfiller_set_dri_width
!
subroutine pyclassfiller_set_dri_npoin(npoin,error)
  use pyclassfiller_data
  integer(kind=4), intent(in) :: npoin
  logical, intent(inout) :: error
  obs%head%dri%npoin = npoin
end subroutine pyclassfiller_set_dri_npoin
!
subroutine pyclassfiller_set_dri_rpoin(rpoin,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: rpoin
  logical, intent(inout) :: error
  obs%head%dri%rpoin = rpoin
end subroutine pyclassfiller_set_dri_rpoin
!
subroutine pyclassfiller_set_dri_tref(tref,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: tref
  logical, intent(inout) :: error
  obs%head%dri%tref = tref
end subroutine pyclassfiller_set_dri_tref
!
subroutine pyclassfiller_set_dri_aref(aref,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: aref
  logical, intent(inout) :: error
  obs%head%dri%aref = aref
end subroutine pyclassfiller_set_dri_aref
!
subroutine pyclassfiller_set_dri_apos(apos,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: apos
  logical, intent(inout) :: error
  obs%head%dri%apos = apos
end subroutine pyclassfiller_set_dri_apos
!
subroutine pyclassfiller_set_dri_tres(tres,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: tres
  logical, intent(inout) :: error
  obs%head%dri%tres = tres
end subroutine pyclassfiller_set_dri_tres
!
subroutine pyclassfiller_set_dri_ares(ares,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: ares
  logical, intent(inout) :: error
  obs%head%dri%ares = ares
end subroutine pyclassfiller_set_dri_ares
!
subroutine pyclassfiller_set_dri_bad(bad,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: bad
  logical, intent(inout) :: error
  obs%head%dri%bad = bad
end subroutine pyclassfiller_set_dri_bad
!
subroutine pyclassfiller_set_dri_ctype(ctype,error)
  use pyclassfiller_data
  integer(kind=4), intent(in) :: ctype
  logical, intent(inout) :: error
  obs%head%dri%ctype = ctype
end subroutine pyclassfiller_set_dri_ctype
!
subroutine pyclassfiller_set_dri_cimag(cimag,error)
  use pyclassfiller_data
  real(kind=8), intent(in) :: cimag
  logical, intent(inout) :: error
  obs%head%dri%cimag = cimag
end subroutine pyclassfiller_set_dri_cimag
!
subroutine pyclassfiller_set_dri_colla(colla,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: colla
  logical, intent(inout) :: error
  obs%head%dri%colla = colla
end subroutine pyclassfiller_set_dri_colla
!
subroutine pyclassfiller_set_dri_colle(colle,error)
  use pyclassfiller_data
  real(kind=4), intent(in) :: colle
  logical, intent(inout) :: error
  obs%head%dri%colle = colle
end subroutine pyclassfiller_set_dri_colle
!
!--- Data --------------------------------------------------------------
!
subroutine pyclassfiller_set_datay(datay,n,error)
  use pyclassfiller_data
  integer(kind=4), intent(in)    :: n
  real(kind=4),    intent(in)    :: datay(n)
  logical,         intent(inout) :: error
  !
  call reallocate_obs(obs,n,error)
  if (error)  return
  !
  obs%data1(1:n) = datay(1:n)
  !
end subroutine pyclassfiller_set_datay
