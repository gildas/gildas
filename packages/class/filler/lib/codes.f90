!-----------------------------------------------------------------------
! Gildas codes
!-----------------------------------------------------------------------
subroutine pyclassfiller_code_coord(un,eq,ga,ho)
  use gbl_constant
  integer(kind=4), intent(out) :: un
  integer(kind=4), intent(out) :: eq
  integer(kind=4), intent(out) :: ga
  integer(kind=4), intent(out) :: ho
  un = type_un
  eq = type_eq
  ga = type_ga
  ho = type_ho
end subroutine pyclassfiller_code_coord
!
subroutine pyclassfiller_code_kind(spec,cont,sky,onoff,focus)
  use gbl_constant
  integer(kind=4), intent(out) :: spec
  integer(kind=4), intent(out) :: cont
  integer(kind=4), intent(out) :: sky
  integer(kind=4), intent(out) :: onoff
  integer(kind=4), intent(out) :: focus
  spec = kind_spec
  cont = kind_cont
  sky = kind_sky
  onoff = kind_onoff
  focus = kind_focus
end subroutine pyclassfiller_code_kind
!
subroutine pyclassfiller_code_xunit(velo,freq,wave)
  use gbl_constant
  integer(kind=4), intent(out) :: velo
  integer(kind=4), intent(out) :: freq
  integer(kind=4), intent(out) :: wave
  velo = a_velo
  freq = a_freq
  wave = a_wave
end subroutine pyclassfiller_code_xunit
!
subroutine pyclassfiller_code_switch(freq,posi,fold)
  use gbl_constant
  integer(kind=4), intent(out) :: freq
  integer(kind=4), intent(out) :: posi
  integer(kind=4), intent(out) :: fold
  freq = mod_freq
  posi = mod_pos
  fold = mod_fold
end subroutine pyclassfiller_code_switch
!
subroutine pyclassfiller_code_proj(none,gnomonic,ortho,azimuthal,stereo,lambert,  &
  aitoff,radio,sfl,mollweide,ncp,cartesian)
  use gbl_constant
  integer(kind=4), intent(out) :: none
  integer(kind=4), intent(out) :: gnomonic
  integer(kind=4), intent(out) :: ortho
  integer(kind=4), intent(out) :: azimuthal
  integer(kind=4), intent(out) :: stereo
  integer(kind=4), intent(out) :: lambert
  integer(kind=4), intent(out) :: aitoff
  integer(kind=4), intent(out) :: radio
  integer(kind=4), intent(out) :: sfl
  integer(kind=4), intent(out) :: mollweide
  integer(kind=4), intent(out) :: ncp
  integer(kind=4), intent(out) :: cartesian
  none = p_none
  gnomonic = p_gnomonic
  ortho = p_ortho
  azimuthal = p_azimuthal
  stereo = p_stereo
  lambert = p_lambert
  aitoff = p_aitoff
  radio = p_radio
  sfl = p_sfl
  mollweide = p_mollweide
  ncp = p_ncp
  cartesian = p_cartesian
end subroutine pyclassfiller_code_proj
!
subroutine pyclassfiller_code_velo(unk,lsr,hel,obs,ear,aut)
  use gbl_constant
  integer(kind=4), intent(out) :: unk
  integer(kind=4), intent(out) :: lsr
  integer(kind=4), intent(out) :: hel
  integer(kind=4), intent(out) :: obs
  integer(kind=4), intent(out) :: ear
  integer(kind=4), intent(out) :: aut
  unk = vel_unk
  lsr = vel_lsr
  hel = vel_hel
  obs = vel_obs
  ear = vel_ear
  aut = vel_aut
end subroutine pyclassfiller_code_velo
!
subroutine pyclassfiller_code_conv(unk,rad,opt,thi)
  use gbl_constant
  integer(kind=4), intent(out) :: unk
  integer(kind=4), intent(out) :: rad
  integer(kind=4), intent(out) :: opt
  integer(kind=4), intent(out) :: thi
  unk = vconv_unk
  rad = vconv_rad
  opt = vconv_opt
  thi = vconv_30m
end subroutine pyclassfiller_code_conv
!
!-----------------------------------------------------------------------
! Class codes
!-----------------------------------------------------------------------
subroutine pyclassfiller_code_section(user,gen,pos,spe,bas,his,plo,swi,  &
  gau,dri,bea,she,nh3,abs,cal,poi,sky)
  use class_parameter
  integer(kind=4), intent(out) :: user
  integer(kind=4), intent(out) :: gen
  integer(kind=4), intent(out) :: pos
  integer(kind=4), intent(out) :: spe
  integer(kind=4), intent(out) :: bas
  integer(kind=4), intent(out) :: his
  integer(kind=4), intent(out) :: plo
  integer(kind=4), intent(out) :: swi
  integer(kind=4), intent(out) :: gau
  integer(kind=4), intent(out) :: dri
  integer(kind=4), intent(out) :: bea
  integer(kind=4), intent(out) :: she
  integer(kind=4), intent(out) :: nh3
  integer(kind=4), intent(out) :: abs
  integer(kind=4), intent(out) :: cal
  integer(kind=4), intent(out) :: poi
  integer(kind=4), intent(out) :: sky
  user = class_sec_user_id
  gen  = class_sec_gen_id
  pos  = class_sec_pos_id
  spe  = class_sec_spe_id
  bas  = class_sec_bas_id
  his  = class_sec_his_id
  plo  = class_sec_plo_id
  swi  = class_sec_swi_id
  gau  = class_sec_gau_id
  dri  = class_sec_dri_id
  bea  = class_sec_bea_id
  she  = class_sec_she_id
  nh3  = class_sec_hfs_id
  abs  = class_sec_abs_id
  cal  = class_sec_cal_id
  poi  = class_sec_poi_id
  sky  = class_sec_sky_id
end subroutine pyclassfiller_code_section
!
subroutine pyclassfiller_code_qual(unk,exc,goo,fai,ave,poo,bad,awf,wor,del)
  use class_parameter
  integer(kind=4), intent(out) :: unk
  integer(kind=4), intent(out) :: exc
  integer(kind=4), intent(out) :: goo
  integer(kind=4), intent(out) :: fai
  integer(kind=4), intent(out) :: ave
  integer(kind=4), intent(out) :: poo
  integer(kind=4), intent(out) :: bad
  integer(kind=4), intent(out) :: awf
  integer(kind=4), intent(out) :: wor
  integer(kind=4), intent(out) :: del
  unk = qual_unknown
  exc = qual_excellent
  goo = qual_good
  fai = qual_fair
  ave = qual_average
  poo = qual_poor
  bad = qual_bad
  awf = qual_awful
  wor = qual_worst
  del = qual_deleted
end subroutine pyclassfiller_code_qual
!
subroutine pyclassfiller_code_calib(auto,trec,humi,manu)
  use class_calibr
  integer(kind=4), intent(out) :: auto
  integer(kind=4), intent(out) :: trec
  integer(kind=4), intent(out) :: humi
  integer(kind=4), intent(out) :: manu
  auto = mode_auto
  trec = mode_trec
  humi = mode_humi
  manu = mode_manu
end subroutine pyclassfiller_code_calib
