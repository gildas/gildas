subroutine pyclassfiller_obs_init(error)
  use class_api
  use pyclassfiller_data
  !---------------------------------------------------------------------
  !
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  !
  !
  call class_obs_init(obs,error)
  !
end subroutine pyclassfiller_obs_init
!
subroutine pyclassfiller_obs_reset(nchan,error)
  use class_api
  use pyclassfiller_data
  !---------------------------------------------------------------------
  !
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)    :: nchan  !
  logical,         intent(inout) :: error  !
  !
  call class_obs_reset(obs,nchan,error)
  !
end subroutine pyclassfiller_obs_reset
!
subroutine pyclassfiller_obs_write(error)
  use class_api
  use pyclassfiller_data
  !---------------------------------------------------------------------
  !
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  !
  !
  call class_obs_write(obs,error)
  !
end subroutine pyclassfiller_obs_write
!
subroutine pyclassfiller_obs_clean(error)
  use class_api
  use pyclassfiller_data
  !---------------------------------------------------------------------
  !
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  !
  !
  call class_obs_clean(obs,error)
  !
end subroutine pyclassfiller_obs_clean
