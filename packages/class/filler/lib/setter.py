#!/usr/bin/env python3

# Generator for file "setter.f90"


### Functions ##########################################################

def pyclassfiller_set_numeric(sect,attr,typ):
  print("subroutine pyclassfiller_set_%s_%s(%s,error)" % (sect,attr,attr))
  print("  use pyclassfiller_data")
  print("  %s, intent(in) :: %s" % (typ,attr))
  print("  logical, intent(inout) :: error")
  print("  obs%%head%%%s%%%s = %s" % (sect,attr,attr))
  print("end subroutine pyclassfiller_set_%s_%s" % (sect,attr))
  print("!")

def pyclassfiller_set_string(sect,attr):
  print("subroutine pyclassfiller_set_%s_%s(%s,error)" % (sect,attr,attr))
  print("  use pyclassfiller_data")
  print("  character(len=*), intent(in) :: %s" % (attr))
  print("  logical, intent(inout) :: error")
  print("  obs%%head%%%s%%%s = %s" % (sect,attr,attr))
  print("end subroutine pyclassfiller_set_%s_%s" % (sect,attr))
  print("!")

def pyclassfiller_set_array(sect,attr,typ,equ,melem):
  # sect: section abbreviation
  # attr: attribute name
  # typ: Fortran type
  # equ: (in)equality kind (-1: <=, 0: ==, +1: >=)
  # melem: reference number of elements
  print("subroutine pyclassfiller_set_%s_%s(%s,nelem,error)" % (sect,attr,attr))
  print("  use class_api")
  print("  use pyclassfiller_data")
  print("  integer(kind=4), intent(in) :: nelem")
  print("  %s, intent(in) :: %s(nelem)" % (typ,attr))
  print("  logical, intent(inout) :: error")
  print("  ! Local")
  print("  character(len=128) :: mess")
  print("  !")
  if (equ<0):
    print("  if (nelem.gt.%s) then" % (melem))
    print("    write(mess,'(A,I0,A,I0,A)')  &")
    print("      'E-FILLER,  %s%%%s must have at most ',%s,' values (got ',nelem,')'" % (sect.upper(),attr.upper(),melem))
  elif (equ==0):
    print("  if (nelem.ne.%s) then" % (melem))
    print("    write(mess,'(A,I0,A,I0,A)')  &")
    print("      'E-FILLER,  %s%%%s must have exactly ',%s,' values (got ',nelem,')'" % (sect.upper(),attr.upper(),melem))
  print("    call gagout(mess)")
  print("    error = .true.")
  print("    return")
  print("  endif")
  print("  obs%%head%%%s%%%s(:) = 0" % (sect,attr))
  print("  obs%%head%%%s%%%s(1:nelem) = %s(1:nelem)" % (sect,attr,attr))
  print("end subroutine pyclassfiller_set_%s_%s" % (sect,attr))
  print("!")


### Execution ##########################################################

# Present sections (no particular function)
print("!--- Present sections --------------------------------------------------")
print("!")
print("""subroutine pyclassfiller_set_presec(code,error)
  use class_api
  use pyclassfiller_data
  integer(kind=4), intent(in)    :: code
  logical,         intent(inout) :: error
  ! Local
  character(len=128) :: mess
  !
  if (code.lt.-mx_sec .or. code.gt.0) then
    write(mess,'(A,I0,A)')  'E-FILLER,  Invalid section code (got ',code,')'
    call gagout(mess)
    error = .true.
    return
  endif
  obs%head%presec(code) = .true.
end subroutine pyclassfiller_set_presec
!""")

# General
print("!--- General -----------------------------------------------------------")
print("!")
pyclassfiller_set_numeric("gen","num",    "integer(kind=obsnum_length)")
pyclassfiller_set_numeric("gen","ver",    "integer(kind=4)")
pyclassfiller_set_string ("gen","teles")
pyclassfiller_set_numeric("gen","dobs",   "integer(kind=4)")
pyclassfiller_set_numeric("gen","dred",   "integer(kind=4)")
pyclassfiller_set_numeric("gen","kind",   "integer(kind=4)")
pyclassfiller_set_numeric("gen","qual",   "integer(kind=4)")
pyclassfiller_set_numeric("gen","scan",   "integer(kind=obsnum_length)")
pyclassfiller_set_numeric("gen","subscan","integer(kind=4)")
pyclassfiller_set_numeric("gen","ut",     "real(kind=8)")
pyclassfiller_set_numeric("gen","st",     "real(kind=8)")
pyclassfiller_set_numeric("gen","az",     "real(kind=4)")
pyclassfiller_set_numeric("gen","el",     "real(kind=4)")
pyclassfiller_set_numeric("gen","tau",    "real(kind=4)")
pyclassfiller_set_numeric("gen","tsys",   "real(kind=4)")
pyclassfiller_set_numeric("gen","time",   "real(kind=4)")
pyclassfiller_set_numeric("gen","xunit",  "integer(kind=4)")

# Position
print("!--- Position ----------------------------------------------------------")
print("!")
pyclassfiller_set_string ("pos","sourc")
pyclassfiller_set_numeric("pos","system", "integer(kind=4)")
pyclassfiller_set_numeric("pos","equinox","real(kind=4)")
pyclassfiller_set_numeric("pos","proj",   "integer(kind=4)")
pyclassfiller_set_numeric("pos","lam",    "real(kind=8)")
pyclassfiller_set_numeric("pos","bet",    "real(kind=8)")
pyclassfiller_set_numeric("pos","projang","real(kind=8)")
pyclassfiller_set_numeric("pos","lamof",  "real(kind=4)")
pyclassfiller_set_numeric("pos","betof",  "real(kind=4)")

# Spectroscopy
print("!--- Spectroscopy ------------------------------------------------------")
print("!")
pyclassfiller_set_string ("spe","line")
pyclassfiller_set_numeric("spe","nchan",  "integer(kind=4)")
pyclassfiller_set_numeric("spe","restf",  "real(kind=8)")
pyclassfiller_set_numeric("spe","image",  "real(kind=8)")
pyclassfiller_set_numeric("spe","doppler","real(kind=8)")
pyclassfiller_set_numeric("spe","rchan",  "real(kind=8)")
pyclassfiller_set_numeric("spe","fres",   "real(kind=8)")
pyclassfiller_set_numeric("spe","vres",   "real(kind=8)")
pyclassfiller_set_numeric("spe","voff",   "real(kind=8)")
pyclassfiller_set_numeric("spe","bad",    "real(kind=4)")
pyclassfiller_set_numeric("spe","vtype",  "integer(kind=4)")
pyclassfiller_set_numeric("spe","vconv",  "integer(kind=4)")

# Frequency Switch
print("!--- Switching ---------------------------------------------------------")
print("!")
pyclassfiller_set_numeric("swi","nphas", "integer(kind=4)")
pyclassfiller_set_array  ("swi","decal", "real(kind=8)",-1,"mxphas")
pyclassfiller_set_array  ("swi","duree", "real(kind=4)",-1,"mxphas")
pyclassfiller_set_array  ("swi","poids", "real(kind=4)",-1,"mxphas")
pyclassfiller_set_numeric("swi","swmod", "integer(kind=4)")
pyclassfiller_set_array  ("swi","ldecal","real(kind=4)",-1,"mxphas")
pyclassfiller_set_array  ("swi","bdecal","real(kind=4)",-1,"mxphas")

# Calibration
print("!--- Calibration -------------------------------------------------------")
print("!")
pyclassfiller_set_numeric("cal","beeff",  "real(kind=4)")
pyclassfiller_set_numeric("cal","foeff",  "real(kind=4)")
pyclassfiller_set_numeric("cal","gaini",  "real(kind=4)")
pyclassfiller_set_numeric("cal","h2omm",  "real(kind=4)")
pyclassfiller_set_numeric("cal","pamb",   "real(kind=4)")
pyclassfiller_set_numeric("cal","tamb",   "real(kind=4)")
pyclassfiller_set_numeric("cal","tatms",  "real(kind=4)")
pyclassfiller_set_numeric("cal","tchop",  "real(kind=4)")
pyclassfiller_set_numeric("cal","tcold",  "real(kind=4)")
pyclassfiller_set_numeric("cal","taus",   "real(kind=4)")
pyclassfiller_set_numeric("cal","taui",   "real(kind=4)")
pyclassfiller_set_numeric("cal","tatmi",  "real(kind=4)")
pyclassfiller_set_numeric("cal","trec",   "real(kind=4)")
pyclassfiller_set_numeric("cal","cmode",  "integer(kind=4)")
pyclassfiller_set_numeric("cal","atfac",  "real(kind=4)")
pyclassfiller_set_numeric("cal","alti",   "real(kind=4)")
pyclassfiller_set_array  ("cal","count",  "real(kind=4)",0,"3")
pyclassfiller_set_numeric("cal","lcalof", "real(kind=4)")
pyclassfiller_set_numeric("cal","bcalof", "real(kind=4)")
pyclassfiller_set_numeric("cal","geolong","real(kind=8)")
pyclassfiller_set_numeric("cal","geolat", "real(kind=8)")

# Drift
print("!--- Continuum Drift ---------------------------------------------------")
print("!")
pyclassfiller_set_numeric("dri","freq", "real(kind=8)")
pyclassfiller_set_numeric("dri","width","real(kind=4)")
pyclassfiller_set_numeric("dri","npoin","integer(kind=4)")
pyclassfiller_set_numeric("dri","rpoin","real(kind=4)")
pyclassfiller_set_numeric("dri","tref", "real(kind=4)")
pyclassfiller_set_numeric("dri","aref", "real(kind=4)")
pyclassfiller_set_numeric("dri","apos", "real(kind=4)")
pyclassfiller_set_numeric("dri","tres", "real(kind=4)")
pyclassfiller_set_numeric("dri","ares", "real(kind=4)")
pyclassfiller_set_numeric("dri","bad",  "real(kind=4)")
pyclassfiller_set_numeric("dri","ctype","integer(kind=4)")
pyclassfiller_set_numeric("dri","cimag","real(kind=8)")
pyclassfiller_set_numeric("dri","colla","real(kind=4)")
pyclassfiller_set_numeric("dri","colle","real(kind=4)")

# Data (no particular function)
print("!--- Data --------------------------------------------------------------")
print("!")
print("""subroutine pyclassfiller_set_datay(datay,n,error)
  use pyclassfiller_data
  integer(kind=4), intent(in)    :: n
  real(kind=4),    intent(in)    :: datay(n)
  logical,         intent(inout) :: error
  !
  call reallocate_obs(obs,n,error)
  if (error)  return
  !
  obs%data1(1:n) = datay(1:n)
  !
end subroutine pyclassfiller_set_datay""")
