module pyclassfiller_data
  use class_types
  ! The module needs a lifetime observation in memory
  type(observation) :: obs
end module pyclassfiller_data
