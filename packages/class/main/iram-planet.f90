!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Name: iram-planet.f90  Version for the IRAM 30m
! Purpose: Calculation of efficiencies from planetary measurements
!          with the IRAM 30m without estimate of the error.
! Planetary temperatures described in Kramer (1994)
! First Version: 12.2.90
! Last Change: 20.10.98  <CKr>
!              Now work also with 4 receivers
! Last Change: 28.4.95  <CKr>
!              Create and append to planet.dat even when called from CLASS\@ TEL_CHECK
! Last Change: 25.9.94  <CKr>
!              Input from file, lines beginnning with 99
!              Better interactive input of date
!              Call by class
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Input:   Interactively or from file
! Output:  Onto a file planet.dat.
! All parameters including the derived HPBW, eta_mb, eta_aperture
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
program planet
  use gkernel_interfaces
  use gildas_def
  integer*4 nlen,ipoint6
  integer*2 i,iinput,ifile,idumm
  integer*2 irec,nrec
  integer ier
  real*8 lambda
  real*4 ageom,a_eff,a_eff_poin,bolt,clight,d,diameter,eta_a,eta_mb,&
       eta_a_gold,eta_mb_gold,fluss,fluss_jans,fluss_jans_pb,frequenz,hpbw_gold,&
       freq_ghz,tb_uranus,tb_neptune
  real*4 omega_a,omega_mb,omega_source,pi,radius,rk
  real*4 tastrich_planet,tb_planet,hpbw,trj_planet,fwhm_planet,x
  real*4 tastern_planet,tb_mean,tb_lin,ratio,ratio_th,el,feff,vor
  real*4 rjot
  character*80 name
  character*75 line
  character*11 indate,today
  character*3 cmonth(12),month 
  integer*4 iplanet,iday,iyear,imonth
  logical first
  !
  data cmonth/'JAN','FEB','MAR','APR','MAY','JUN',&
       'JUL','AUG','SEP','OCT','NOV','DEC'/
  !     
  first = .true.
  pi     = 3.1415629535
  clight = 2.9979e+10       ! [cm/s]
  bolt   = 1.38054e-23
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! This part is telescope-specific:
  d = 3000                  ! D = 3000cm IRAM 30m
  ageom = pi/4*d**2.        ! 7068583.47 [cm^2] IRAM 30m   
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  ipoint6 = 0  !  Called interactively
  if (command_argument_count().ge.1) then
    call get_command_argument(1,name,nlen,ier)
    if (ier.ne.0)  call sysexi(fatale)
    if (name(1:6).eq.'POINT6') then
      print *,' Calling sequence: iram-planet POINT6'
      ipoint6 = 1
    endif
  endif
  !
  ! Input kind 
  iinput = 1
  if (ipoint6.eq.0) then    ! Call by hand 
     write (5,2)
2    format (1x,' Interactive Input or Input from Input File ? [1/0] ',$)
     read (5,*) iinput
  else
     iinput = 0             ! Call from CLASS and read from input file 
  endif
  !
111 continue
  if (iinput.eq.0) then     ! Read from input file 
     if (first) then
      if (ipoint6.eq.0) then ! Call by hand
        write (5,4)
4       format(1x,' Inputfile: ',$)
        read (5,'(a)') line
        nlen = lenc(line)
        ier = sic_open(1,line(1:nlen),'OLD',.false.)
        if (ier.ne.0) then
           print *, " Error opening: "//line(1:nlen)
           call sysexi(fatale)
        endif
        !     
        write (5,3)
3       format(1x,' Write output to old planet.dat [1] or create a new' &
             ' file [0] ? ',$)
        ifile = 1
        read (5,*) ifile
        if (ifile.eq.0) then
           ier = sic_open(2,'planet.dat','NEW',.false.)
           if (ier.ne.0) then
              print *, " Error opening: planet.dat"
              call sysexi(fatale)
           endif
           close(unit=2)
        endif
      else
        ! Call from CLASS => Update data file
        ier = sic_open(1,'point_corr_temp.dat','OLD',.false.)
        if (ier.ne.0) then
           print *, " Error opening: point_corr_temp.dat"
           call sysexi(fatale)
        endif
       endif
     endif
     !     
     if (ipoint6.eq.0) then
        print *, '----------------------------------------------------'
     endif
     line(1:2) = '  '
     do while (line(1:2).ne.'99') ! Skip lines not starting with 99
        read (1,'(a)',err=997,end=999) line
        nlen = lenc(line)
     enddo
     if (line(1:2).eq.'99') then
        read (line(1:nlen),*,err=997,end=999) &
             idumm, iplanet, iday, imonth, iyear, freq_ghz, el, &
             tastern_planet, fwhm_planet, feff, irec, nrec
     endif
     ! Test:
     ! write (5,*) 
     ! &    idumm, iplanet, iday, imonth, iyear, freq_ghz, el,
     ! &    tastern_planet, fwhm_planet, feff
     if (iyear.lt.1900) iyear = 1900 + iyear ! suppose iyear=94 e.g.
     tastrich_planet = tastern_planet * feff
  else
     ! Interactive input:
     iplanet = 0
     do while ((iplanet .gt. 9) .or. (iplanet .lt. 2))
        write(5,10)
10      format(1x,' Venus [3], Mars [4], Jupiter [5], Saturn [6],' & 
             ' Uranus [7], Neptune [8]: ',$)
        read(5,*) iplanet
     enddo
     call sic_c_date(today)
     write(5,20) today
20   format(1x,' Date of the observation   (def.:',A,') : ',$)
     read(5,22) indate
22   format(a,$)
     if (indate.eq."           ") then
        indate = today
        print *,indate
     endif
     !
     ! Date translation
     read(indate,'(i2,1x,a,1x,i4)',iostat=ier) iday,month,iyear
     if (ier.ne.0) then
        write(6,*) 'E-DATE,  Date conversion error'
        call sysexi(fatale)
     endif
     !     
     call sic_upper(month)
     imonth = -1
     do i=1,12
        if (cmonth(i).eq.month) then
           imonth=i
           exit
        endif
     enddo
     if (imonth.eq.-1) then
        write(6,*) 'E-DATE,  Wrong month name: '//month
        call sysexi(fatale)
     endif
     write (5,30)
30   format (1x,' Frequency [GHz] ........................... : ',$)
     read (5,*) freq_ghz
     write (5,40)
40   format (1x,' Corrected Antenna Temperature Ta` [K] ..... : ',$)
     read (5,*) tastrich_planet
     write (5,50)
50   format (1x,' Planet FWHM ["] ........................... : ',$)
     read (5,*) fwhm_planet
  endif ! (IINPUT.ne.0) 
  !     
  ! Calculation of the mean diameter ["] of the specified planet:
  call angdiam(iplanet,iyear,imonth,iday,diameter)
  frequenz = freq_ghz * 1.e9   ! [Hz]
  lambda   = clight / frequenz ! [cm]
  !
  ! Planet temperatures 
  tb_planet = 999.  ! Dummy-wert
  if (iplanet.eq.4) then 
     ! Mars:
     !  TB_MEAN is the mean Mars Brightness Temperature at a 
     !  distance of 1.5237 a.u. (look within subroutine MARS).
     !  The true temperature must be corrected for the radial
     !  distance to the sun:
     !  207 K at 90 GHz, 210 K at 150 GHz, 213 K at 227 GHz, 215 K at337GHz
     !  inter- and extrapolate linearily:
     if (freq_ghz.le.150.) then 
        tb_mean = tb_lin(freq_ghz, 90.,207.,  150.,210.) 
     else if (freq_ghz.le.227.) then
        tb_mean = tb_lin(freq_ghz, 150.,210., 227.,213.) 
     else if (freq_ghz.gt.227.) then
        tb_mean = tb_lin(freq_ghz, 227.,213., 337.,215.) 
     endif
     if (iinput.eq.1) then  ! interactive
        ! Too many questions: (ckr, 2/2/95)
        ! write (5,56) TB_MEAN
        ! 56        format (1x,
        ! &      'Mars T_B at the mean helioc. distance'
        ! &      ' of 1.5237 a.u.: [Default: ',f5.1,' K] ? ',$)
        ! read (5,*) TB_MEAN
     endif
     call mars(iyear,imonth,iday,diameter,tb_mean,tb_planet,ipoint6)
  else if (iplanet.eq.5) then
     ! Jupiter:
     !   179 K at 90 GHz, 173 K at 150 GHz, 171 K at 227 GHz, 174 K at337GHz
     !   inter- and extrapolate linearily:
     if (freq_ghz.le.150.) then 
        tb_planet = tb_lin(freq_ghz, 90.,179.,  150.,173.) 
     else if (freq_ghz.le.227.) then
        tb_planet = tb_lin(freq_ghz, 150.,173., 227.,171.) 
     else if (freq_ghz.gt.227.) then
        tb_planet = tb_lin(freq_ghz, 227.,171., 337.,174.) 
     endif
  else if (iplanet.eq.6) then 
     ! Saturn:
     !   153 K at 90 GHz, 135 K at 310 GHz, OTHER TEMPERATURES NOT KNOWN!!
     if (freq_ghz.le.100.) then
        tb_planet = 153.  
     else ! if ((freq_ghz.gt.300.).and.(freq_ghz.lt.320.)) then 
        tb_planet = 135.
        ! else
        !    tb_planet = 999.
        !    print *, 'We only know: 153 K at 90 GHz and 135 K at 310 GHz'
     endif
  else if (iplanet.eq.7) then 
     ! Uranus:
     tb_planet = tb_uranus(freq_ghz)
  else if (iplanet.eq.8) then 
     ! Neptune:
     tb_planet = tb_neptune(freq_ghz)
  endif
  if (iinput.eq.1) then
     ! Interactive use
     if (iplanet.ne.4) then 
        ! Do not ask for Mars again
        write (5,60) tb_planet
60      format (1x,' Actual Brightness Temperature of Planet: T_B  [Default: ',f7.2,' K] ? ',$)
        read (5,*) tb_planet
     endif
  endif
  !
  if ((iinput.eq.0).and.(ipoint6.eq.0)) then
     ! read from file, interactive
     write (5,6) iplanet, iday, imonth, iyear, freq_ghz, el, &
          tastern_planet, fwhm_planet, diameter, tb_planet, &
          feff, tastrich_planet
6    format (1x,i1,1x,i2,1x,i2,1x,i4,1x,f5.1,1x,f3.0,1x, &
          f6.2,1x,f6.2,1x,f6.2,1x,f6.1,1x, &
          f4.2,1x,f6.2)
  endif
  !     
  frequenz   = freq_ghz * 1.e9 ! [hz]
  trj_planet = rjot(tb_planet,frequenz) ! rj-korrektur
  !     
  if (ipoint6.eq.0) then    ! interactive
     write (5,70) trj_planet, tb_planet
70   format (1x,' Rayleigh Jeans Temperature: ',f7.2,', Brightness Temperature T_B: ',f7.2)
  endif
  !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! Theoretical HPBW (see Goldsmith):
  !   T_EDGE = 14.  ! 14dB Edgetaper
  !   ALPHA = 1.28  ! Nach Goldsmith
  !   HPBW_GOLD  = 0.8*sqrt(T_EDGE)/PI*ALPHA*LAMBDA/D  ! [radian]
  !   HPBW_GOLD  = HPBW_GOLD*180./PI                   ! [Grad]
  !   HPBW_GOLD  = HPBW_GOLD*60.*60.                   ! ["]
  ! HPBw nach Fit an gemessene HPBW zw. 85 und 250 GHz (s. 30m-Manual):
  hpbw_gold  = 2398./freq_ghz
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  ! Flux of the planet, independent of the telescope or the atmosphere:
  ! Derived from the Rayleigh-Jeans corrected Brightness Temperatures!
  radius       = diameter / 2.0                                         ! ["]
  omega_source = pi*(radius*pi/(180.*3600.))**2.                        ! [sterad]
  fluss        = 2*bolt/((lambda/100.)**2.) * trj_planet * omega_source ! [Joule]
  fluss_jans = fluss * 1.e26                                            ! [Jansky]
  !
  if (IPOINT6.eq.0) then
     ! Interactive use
     write (5,80) DIAMETER, FLUSS_JANS
80   format (1x,' Mean Diameter: ',f6.2,' arcsec, Total Flux: ',f7.2,' Jy')
  endif
  !
  ! Calculation for the derived HPBW:
  hpbw = sqrt(fwhm_planet**2. - &
       alog(2.)/2.*(2*radius)**2.) ! ["]
  ratio      = 2.*radius / hpbw
  ! x          = radius / 0.6 / hpbw ! etwas genauer:
  x          = sqrt(alog(2.))*2.*radius/hpbw
  rk         = x**2. / (1.-exp(-1.*x**2.))
  fluss_jans_pb = fluss_jans/rk ! flux per beam
  a_eff_poin = 2.*bolt * tastrich_planet / fluss ! [m**2]
  a_eff      = rk * a_eff_poin * 10000. ! [cm**2]
  eta_a      = a_eff / ageom ! aperture efficiency
  vor        = pi/(4.*alog(2.)) ! = 1.133
  omega_mb   = vor*((hpbw/3600.)*pi/180.)**2. ! [sterad]
  omega_a    = (clight / frequenz)**2. / a_eff ! effektiver raumwinkel
  eta_mb     = omega_mb / omega_a ! main beam efficiency
  a_eff      = a_eff / 1.e4 ! [m^2]
  ! eta_mb2    = tastrich_planet / (trj_planet*(1.-exp(-1.*x**2.)))
  if (ipoint6.eq.0) then 
     ! interactive use
     write (5,100,err=105) HPBW,FLUSS_JANS_PB,RATIO,(ETA_A*100.),(ETA_MB*100.)
100  format (1X,'Der.HPBW: ',f4.1,' arcsec, Flux pb: ',f7.2,' Jy, R: ',f4.1,&
          ' ,Ap.eff: ',f4.1,'%, Beff: ',f4.1,'% ')
  endif
105 continue
  !
  ! Same computation for the theoretical HPBW:
  ratio_th   = 2.*radius / hpbw_gold
  x          = radius / 0.6 / hpbw_gold  
  rk         = x**2. / (1.-exp(-1.*x**2.))
  fluss_jans_pb = fluss_jans/rk
  a_eff_poin = 2.*bolt * tastrich_planet / fluss ! [m**2]
  a_eff      = rk * a_eff_poin * 10000. ! [cm**2]
  eta_a_gold = a_eff / ageom ! aperture efficiency
  omega_mb   = 1.133*((hpbw_gold/3600.)*pi/180.)**2. ! [sterad]
  omega_a    = (clight / frequenz)**2. / a_eff ! effektiver raumwinkel
  eta_mb_gold = omega_mb / omega_a ! main beam efficiency
  a_eff      = a_eff / 1.e4 ! [m^2] effective antenna area
  if (ipoint6.eq.0) then    ! interactive
     write (5,110) hpbw_gold,fluss_jans_pb,ratio_th,(eta_a_gold*100.),(eta_mb_gold*100.)
110  format (1X,'Th. HPBW: ',f4.1,' arcsec, Flux pb: ',f7.2,' Jy, R: ',f4.1,&
          ' ,Ap.eff: ',f4.1,'%, Beff: ',f4.1,'%')
  endif
  !     
  !
  if (ipoint6.eq.0) then
     ! Interactive use
     ier = sic_open(2,'planet.dat','APPEND',.false.)
     if (ier.ne.0) then
        print *, " Error opening: planet.dat"
        call sysexi(fatale)
     endif
     write (2,120) &
          iplanet,iday,imonth,iyear,freq_ghz,el,feff,ratio,hpbw, &
          eta_a,eta_mb,hpbw_gold, eta_a_gold, eta_mb_gold
120  format(1x,i1,1x,i2,1x,i2,1x,i4,1x,f5.1,1x,f3.0,1x,f4.2,1x,f5.1,1x,&
          f6.1,1x,f4.2,1x,f4.2,1x,f6.1,1x,f4.2,1x,f4.2)
     print *,' Appended to file planet.dat '
     close (unit=2)
  else
     ! For use in CLASS
     ier = sic_open(3,'iram-planet.dat','APPEND',.false.)
     if (ier.ne.0) then
        print *, " Error opening: iram-planet.dat"
        call sysexi(fatale)
     endif
     write(3,'(f6.2)') diameter
     write(3,'(f7.1)') tb_planet
     write(3,'(f6.1)') freq_ghz
     write(3,'(f4.1)') hpbw
     write(3,'(f4.2)') eta_a
     write(3,'(f4.2)') eta_mb
     close (unit=3)
     ier = sic_open(3,'planet.dat','APPEND',.false.)
     if (ier.ne.0) then
        print *, " Error opening: planet.dat"
        call sysexi(fatale)
     endif
     write (3,120) &
          iplanet,iday,imonth,iyear,freq_ghz,el,feff,ratio,hpbw,&
          eta_a,eta_mb,hpbw_gold,eta_a_gold,eta_mb_gold
     close (unit=3)
  endif
  !     
  if (iinput.eq.0) then
     first = .false.
     goto 111               ! Loop over all input lines
  endif
  stop
997 continue                  ! error
  print *,' Error in reading from file '
  stop
999 continue
  close (unit=1)
  if (ipoint6.eq.0) then
     print *,' End of File detected '
  endif
  stop
end program planet
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  
function tb_lin(freq_ghz,freq1,tb1,freq2,tb2)
  ! Derive a linear equation from two points and calculate the y (=mx+b)
  ! corresponding to a x
  real*4 tb_lin,freq_ghz,freq1,freq2,tb1,tb2,xslope,yaxis
  xslope = (tb1-tb2)/(freq1-freq2)
  yaxis  = tb1-xslope*freq1
  tb_lin = xslope * freq_ghz + yaxis
  return 
end function tb_lin
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  
function rjot(t,freq)
  ! Calculation of the Brightness-Temperature from the 
  ! Physical Planck Temperature:
  real*4 rjot,t,freq,bolt,plan,a
  bolt  = 1.38054e-23       ! J/K
  plan  = 6.6256e-34        ! W s**2
  a     = plan*freq/bolt
  rjot  = a/(exp(a/t)-1)    ! = T_b [K]
  return
end function rjot
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  
function rjot_inv(t,freq)
  ! Calculation of the Physical Planck Temperature from
  ! the Brightness-Temperature:
  real*4 rjot_inv,t,freq,bolt,plan,a
  bolt  = 1.38054e-23       ! J/K
  plan  = 6.6256e-34        ! W s**2
  a     = plan*freq/bolt
  rjot_inv = a/alog(a/t+1.) ! = T_p [K]
  return
end function rjot_inv
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  
function tb_uranus(freq_ghz)
  ! Griffin & Orton 1993, ICARUS, 105, 537 
  real*4 tb_uranus,freq_ghz,clight,frequenz,xlambda,a0,a1,a2,a3
  clight   = 2.9979e+10         ! [cm/s]
  frequenz = freq_ghz * 1.e9    ! [Hz]
  xlambda  = clight / frequenz  ! [cm]
  xlambda  = xlambda * 1.e4     ! [micrometers]
  !
  a0 = -795.694
  a1 =  845.179
  a2 = -288.946
  a3 = 35.200
  tb_uranus = a0 + a1 * alog10(xlambda) &
       + a2 * (alog10(xlambda))**2. &
       + a3 * (alog10(xlambda))**3.
  return
end function tb_uranus
!     
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  
function tb_neptune(freq_ghz)
  ! Griffin & Orton 1993, ICARUS, 105, 537 
  real*4 tb_neptune,freq_ghz,clight,frequenz,xlambda,aa0,aa1,aa2,aa3
  clight   = 2.9979e+10        ! [cm/s]
  frequenz = freq_ghz * 1.e9   ! [Hz]
  xlambda  = clight / frequenz ! [cm]
  xlambda  = xlambda * 1.e4    ! [micrometers]
  !     
  aa0 = -598.901 
  aa1 =  655.681
  aa2 = -229.545
  aa3 =   28.994
  tb_neptune = aa0 + aa1 * alog10(xlambda) &
       + aa2 * (alog10(xlambda))**2. &
       + aa3 * (alog10(xlambda))**3.
  return
end function tb_neptune
!     
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  
subroutine angdiam(iplanet,iyear,imonth,iday,diameter)
  !--------------------------------------------------------------------------
  ! Hier ist ein Fortran-Programm, das die scheinbaren Winkeldurchmesser
  ! der Planeten ausrechnet. <Martin Miller, UNIVERSITY OF COLOGNE>
  ! Bei JUPITER und SATURN sind es die gemittelten Durchmesser (a+b)/2.
  ! 
  ! Merkur:  2
  ! Venus:   3
  ! Mars:    4
  ! Jupiter: 5
  ! Saturn:  6
  ! Uranus:  7
  ! Neptun:  8
  ! Pluto:   9
  ! 
  ! EPHEMERIDENPROGRAMM FUER DIE PLANETEN
  ! MIT EINER GENAUIGKEIT INNERHALB VON 1'.5
  ! 
  ! Iyear, Imonth, Iday: Zeitpunkt (INTEGER)
  ! diameter: scheinbarer Planetendurchmesser in "
  !--------------------------------------------------------------------------
  integer*4 iplanet,iyear,imonth,iday,nplanet
  real*4 diameter
  logical test
  integer n
  double precision w,dist,r,x,y,z,x1,y1,z1,xx,yy,zz
  double precision ec,convs,g,g1,g5,g6,g7,ge,gg,gj,gp,gpp,gn,gt,tb,th,tl,tp
  double precision t,an1(9),xjd,djl,conv,pi2,tt
  double precision ec0(9),ec1(9),an0(9),p0(9),p1(9),&
       th0(9),th1(9),xi(9),a(9),dim(9)
  !     
  ! ARG(P,Q)=P-Q*INT(P/Q)-Q*INT(SIGN(.5,P-Q*INT(P/Q))-.5)
  ! HLP=.FALSE.
  !     
  ! PLUTO ELEMENTS FROM SHARAF (1964)
  data a/1.00000023,.387098599,.723331619,1.523688395,&
       5.202802875,9.53884320,19.19097811,30.0706724,39.672599/
  data an0/358.475833,102.279381,212.603222,319.529425,&
       225.444651,175.758444,74.313628,41.269550,231.002308/
  data an1/35999.049750d0,149472.515289d0,58517.803875d0,&
       19139.858500d0,3034.906654d0,1222.116782d0,428.502578d0,&
       218.466783d0,144.072477d0/
  data ec0/.01675104,.20561421,.00682069,.09331290,&
       .04825382,.05606075,.04704433,.00853341,.24706226/
  data ec1/-.00004180,.00002046,-.00004774,.00009206,&
       .0,.0,.0,.0,.0/
  data p0/101.220833,75.899697,130.163833,334.218203,&
       11.907422,90.110981,169.048778,43.755611,221.592475/
  data p1/1.719175,1.555489,1.408036,1.840758,&
       .0,.0,.0,.0,1.388888/
  data th0/0.,47.145944,75.779647,48.786442,&
       98.932822,112.347606,73.490250,130.678889,108.937165/
  data th1/0.,1.185208,.899850,.770992,&
       .0,.0,.509667,1.100972,1.358056/
  data xi/0.,7.002881,3.393630,1.850333,&
       1.311614,2.494239,.7726658,1.779256,17.109816/
  !     
  ! Durchmesser der Planeten ["] in der Entfernung 1 AU
  ! (aus Astronomical Almanac, 1989, E43)
  data dim/1919.3, 6.72, 16.68, 9.36, 190.5, 156.55, 70.04, &
       67.0, 4.1/
  !     
  pi2=6.283185307179586476d0
  conv=pi2/360.d0
  convs=conv
  !     
  xjd=djl(iyear,imonth,iday)
  t=(xjd-2415020.d0)/36525.d0
  !     
  ! Planetary ephemerides computed below (newcomb/hill,1898)
  ! all values within 1.5' except pluto --- uranus/neptune
  ! suffer for very ancient dates (error up to 3-4 degrees)
  ! comments after perturbations (below) give maximum
  ! effects of terms on geocentric longitude
  tt=t
  tp=tt+18262.d0/36525.d0
  w=tt
  test=.false.
  !     
  ! Sonnenephemeride
  if(iplanet.eq.1) goto 170
  test=.true.
  nplanet=iplanet
  iplanet=1
  goto 170
100 iplanet=nplanet
  test=.false.
170 ec=ec0(iplanet)+ec1(iplanet)
  n=an1(iplanet)*tt/360.d0
  g =(an1(iplanet)*tt-n*360.d0+dble(an0(iplanet)))*conv
  g5=(an1(5)*tt-n*360.d0+dble(an0(5)))*conv
  g6=(an1(6)*tt-n*360.d0+dble(an0(6)))*conv
  g7=(220.169542+428.49311*tp)*convs
  goto (175,175,175,175,171,172,173,174,175),iplanet
171 g=g+(.6506*sin(2*g6-2*g5+336.9*convs)&
       +(3.9987-.002213*36525./4332.58*tp)&
       *sin(5*g6-2*g5+(67.15-8197.0/3600.*tp)*convs)&
       +.5380*sin(5*g6-3*g5+176.5*convs)&
       +.4112*sin(2*g6-g5+1.4*convs)&
       +.0399278*36525./4332.58*tp*sin(-g5+227.46*convs)&
       +.2763*sin(3*g6-2*g5+127.4*convs)&
       +.2669*sin(g6-g5+79.2*convs)  )*299.12837/3600.*convs
  goto 175
  ! 250",1500"-7"T,202",154",126"T(PROBLEM?),104",100",
  ! (21",20",19",18",14",14",11",9",6"T)
172 g=g+(24.153*sin(5*g6-2*g5+(247.11-2.277*tp)*convs)&
       +5.679*sin(4*g6-2*g5+277.39*convs)&
       +3.505*sin(2*g6-g5+181.43*convs)&
       +.657765*36525./10759.20*tp*sin(g6+238.0*convs)&
       +.278*sin(3*g6-g5+121.2*convs)&
       +.266*sin(2*g6-2*g5+157.0*convs)&
       +.238*sin(6*g6-2*g5-3*g7+6.9*convs)&
       +.223*sin(10*g6-4*g5+(133.6-14814.5/3600.*w)*convs)&
       +.234*sin(3*g7-g6+321.7*convs)  )*120.455/3600.*convs
  goto 175
  ! 3270",770",473",300"T(PROBLEM?),37",36",32",30",32",
  ! (29",26",19"T,16"T,14",13",10",9",9",8",7")
173 g=g+33.086*w*w/3600.*convs
  goto 175
174 g=g-22.401/3600.*w*w*convs
175 ge=g+ec*sin(g+ec*sin(g+ec*sin(g+ec*sin(g+ec*sin(g)))))
  x=2.*atan(sqrt((1.+ec)/(1.-ec))*tan(.5*ge))&
       +(p0(iplanet)+p1(iplanet)*w)*convs + (iplanet/7-iplanet/9)*w*5025.3/3600.*convs
  goto (179,179,179,176,179,179,177,178,179),iplanet
176 gt=(358.415+35998.928*w)*convs
  gj=(225.209+3034.462*w+.332*sin((134.4+38.5*w)*convs))&
       *convs
  x=x+(25.384*cos(gj-g-48.9*convs)&
       +52.490*sin((47.48+19.771*w)*convs)-37.05-13.50*w&
       +21.869*cos(2*gj-g-188.3*convs)&
       +16.035*cos(2*gj-2*g-191.9*convs)&
       +13.966*cos(-gt+2*g-20.5*convs)&
       +8.559*cos(-gt+g-35.1*convs)  )/3600.*convs
  ! 92",191",80",58",51",31",(27",23",18",14",12")
  goto 179
177 gn=(225.417+3034.904*w)*convs
  gg=(175.753+1222.113*w)*convs
  g1=(74.412+428.498*w)*convs
  gp=(74.320+428.498*w)*convs
  gpp=(41.339+218.467*w)*convs
  x=x+(142.938*sin(gg-2*g1)+19.508*cos(gg-2*g1)&
       +75.70*cos(3*g1-gg)-102.30*sin(3*g1-gg)&
       -48.623*sin(gn-g1)-21.320*cos(gn-g1)&
       -27.871*cos(gp-gpp)+19.869*sin(gp-gpp)&
       +28.793*cos(2*gp-2*gpp)+10.035*sin(2*gp-2*gpp)&
       +(18.37*sin(3*gp-3*gpp)+8.91*cos(3*gp-3*gpp))*cos(g)&
       +(8.35*sin(3*gp-3*gpp)-16.44*cos(3*gp-3*gpp))*sin(g)&
       -18.585*cos(gg-g1)+12.603*sin(gg-g1)&
       +4.327*cos(3*gp-3*gpp)+14.280*sin(3*gp-3*gpp)&
       )/3600.*convs
  ! 154",135",56",36",32",29"(2),24",15",(9",7",6")
  x=x+((112.317*w-1.551*w*w-.516*w*w*w)*sin(g)&
       -(68.339*w+9.721*w*w)*cos(g)+6.605*w*sin(2*g)&
       -(29.44-.410*w)*sin((20.45-22.61*w)*convs) )/3600.*convs
  goto 179
  ! JUP/SAT TERM & QUADRATURES OF LONG PERIOD URA/NEP TERMS
178 x=x+(  33.972*w*cos(g)&
       +18.553*sin((180.966+1004.034*w+.1403*w*w)*convs)&
       +34.138*sin((153.267+2816.296*w-.0573*w*w)*convs)&
       +(26.50*w+3.92*w*w)*sin(g)   )/3600.*convs
  ! 36"T,19",35",25"T+4"T*T,(15",9",6")
  ! FIRST/LAST TERMS (QUADRATURES) IMPROVE 1600-2000 VALUES
  ! BUT FOR VERY ANCIENT DATES ARE INVALID
179 th=(th0(iplanet)+th1(iplanet)*w)*convs
  tl=th+atan2(sin(x-th)*cos(real(xi(iplanet))*convs),&
       cos(x-th))+(iplanet/5-iplanet/7)*tp*5026.1/3600.*convs
  tb=asin(sin(xi(iplanet)*convs)*sin(x-th))
  r=a(iplanet)*(1.-ec*cos(ge))
  if(iplanet.ne.5) goto 186
  tb=tb+4.37431*36525./4332.58*sin(x+23.62*convs)&
       *tp/3600.*convs
  ! TB(5) EFFECTS LESS THAN 35"T, TB(6) LESS THAN 84"T
  r=r*10.**(.0002303*cos(2*g6-2*g5+336.9*convs)&
       +.0001679*cos(5*g6-3*g5+176.4*convs)&
       +.0000125634*36525./4332.58*tp*cos(-g5+227.4*convs)  )
  ! 22",16",10"T,(7",5",3")
186 if(iplanet.ne.6) goto 190
  tb=tb+24.266*36525./10759.2*sin(x-13.05*convs)&
       *tp/3600.*convs
  r=r*10.**(.0007005*cos(4*g6-2*g5+277.3*convs)&
       +.0003783*cos(g6-g5+79.8*convs)&
       +.000083491*36525./10759.20*tp*cos(g6+58.0*convs)&
       +.0002443*cos(2*g6-g5+176.0*convs)  )
  ! 37",20",15"T,13",(10",6",3")
  ! R(7) TERMS EFFECT LESS THAN 5",3"T,2"T,.3"T*T
  ! R(8) TERMS ONLY EFFECT .02' MAXIMUM
190 x=r*cos(tb)*cos(tl)
  y=r*cos(tb)*sin(tl)
  z=r*sin(tb)
  if(iplanet.ne.1) goto 195
  x1=x
  y1=y
  z1=z
  if(iplanet.eq.1 .and. .not. test) goto 210
  if(iplanet.eq.1 .and.       test) goto 100
195 xx=x-x1
  yy=y-y1
  zz=z-z1
  goto 215
210 xx=-x1
  yy=-y1
  zz=-z1
215 dist=sqrt(xx*xx+yy*yy+zz*zz)
  diameter=dim(iplanet)/dist
  return
end subroutine angdiam
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!     
function djl(iy,im,id)
  ! Berechnung des Julianischen Dates
  ! Eingabe: iyear, imonth, iday (jeweils integer)
  ! Ergebnis ist doppelt genau
  integer*4 iy,im,id
  double precision djl
  !
  djl=(im*3057)/100+id+((5-im/3)/5)*(2-(4-mod(iy,4))/4+&
       (100-mod(iy,100))/100-(400-mod(iy,400))/400)+1721028+&
       (iy*365+iy/4-iy/100+iy/400)
  djl=djl-0.5d0
  return
end function djl
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!     
subroutine mars(iyear,imonth,iday,diameter,tb_mean,tb_planet,ipoint6)
  ! Calculation of the Mars Temperature at the actual radius vector
  ! of mars using the temperature TB_MEAN at the mean distance R_V_DEF a.u..
  ! Subroutines within MARS() are taken from the program planets (planetsubs)
  ! which was written by C. Thum.
  ! 
  ! Input:  IYEAR, IMONTH, IDAY, DIAMETER, TB_MEAN, 
  ! Output: TB_PLANET
  integer*4 iyear,iday,imonth,ipoint6
  real*4    diameter,tb_mean,tb_planet
  real*4    r_v_def,r_v
  integer*4 daep
  real*4    elt(0:9),ebt(0:9),rht(0:9) ! Planets are numbered from 1 to 9 
  !     
  ! These two routines are needed to calculate the actual radius vector of Mars:
  call dayep(iyear,imonth,iday,daep) ! Days since the epoch 1980.0
  call ecp_mean(daep,elt,ebt,rht)    ! Ecliptic coordinates
  r_v= rht(4)       ! Mars 
  r_v_def = 1.5237  ! From the astronomical almanac 1989 e43
  tb_planet = tb_mean * sqrt(r_v_def / r_v)
  !     
  if (ipoint6.eq.0) then ! Interactive run
     write (5,10) r_v_def, tb_mean
10   format(1x,' t_b of mars at a mean distance from the sun of ',f7.4,' a.u.: ',f5.1)
     write (5,20) r_v, tb_planet
20   format(1x,' t_b of mars at the actual dist.from the sun of ',f7.4,' a.u.: ',f5.1)
  endif
  return
end subroutine mars
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!     
subroutine dayep(y,m,d,de)
  ! Subroutine to calculate DAYs since the EPoch 1980.0.0 (722813)
  integer*4 y,m,d,de
  integer*4 k
  real*4    yy
  !
  k=1
  if (m.le.2) k=0
  yy=y+k-1
  de=365*(y-1)-722813+int(yy/4.)-int(yy/100.)+int(yy/400.)+31*(m-1)+d-1-k*int(.4*float(m)+2.3)
end subroutine dayep
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!     
subroutine helora(de,iplanet,phlo,prv,fi)
  use gkernel_interfaces
  ! Subroutine to calculate the Projected Heliocentric LOngitude (PHLO) and the
  ! Projected Radius Vector (PRV) of the planet numbered by I at the Day since
  ! the Epoch DE, FI = heliocentric latitude of the planet
  use gildas_def
  real*4 phlo,prv,fi
  integer*4 de,iplanet
  real*4 m,nu,co,lo,slo,jlo,ra,awn
  real*4 a(9,7)
  ! Period (tropical years)
  a(1,1)=.24085
  a(2,1)=.61521
  a(3,1)=1.00004
  a(4,1)=1.88089
  a(5,1)=11.86224
  a(6,1)=29.45771
  a(7,1)=84.01247
  a(8,1)=164.79558
  a(9,1)=250.9
  ! Longitude at epoch (degrees):
  a(1,2)=231.2973
  a(2,2)=355.73352
  a(3,2)=98.83354
  a(4,2)=126.30783
  a(5,2)=146.966365
  a(6,2)=165.322242
  a(7,2)=228.0708551
  a(8,2)=260.3578998
  a(9,2)=209.439
  ! Longitude of perihelion (degrees):
  a(1,3)=77.1442128
  a(2,3)=131.2895792
  a(3,3)=102.596403
  a(4,3)=335.6908166
  a(5,3)=14.0095493
  a(6,3)=92.6653974
  a(7,3)=172.7363288
  a(8,3)=47.8672148
  a(9,3)=222.972
  ! Eccentricity of orbit:
  a(1,4)=.2056306
  a(2,4)=.0067826
  a(3,4)=.016718
  a(4,4)=.0933865
  a(5,4)=.0484658
  a(6,4)=.0556155
  a(7,4)=.0463232
  a(8,4)=.0090021
  a(9,4)=.25387
  ! Semi-major axis of orbit referred to Earth's orbit:
  a(1,5)=.3870986
  a(2,5)=.7233316
  a(3,5)=1.
  a(4,5)=1.5236883
  a(5,5)=5.202561
  a(6,5)=9.554747
  a(7,5)=19.21814
  a(8,5)=30.10957
  a(9,5)=39.78459
  ! Inclination of orbit (degrees):
  a(1,6)=7.0043579
  a(2,6)=3.394435
  a(3,6)=0.0
  a(4,6)=1.8498011
  a(5,6)=1.3041819
  a(6,6)=2.4893741
  a(7,6)=.7729895
  a(8,6)=1.7716017
  a(9,6)=17.137
  ! Longitude of ascending node (degrees):
  a(1,7)=48.0941733
  a(2,7)=76.4997524
  a(3,7)=0.0
  a(4,7)=49.4032001
  a(5,7)=100.2520175
  a(6,7)=113.4888341
  a(7,7)=73.8768642
  a(8,7)=131.5606494
  a(9,7)=109.941
  co=57.29578
  m=.9856473*float(de)/a(iplanet,1)+a(iplanet,2)-a(iplanet,3)
  call anor(m)
  call ta(m,a(iplanet,4),nu)
  lo=nu+a(iplanet,3) ! Heliocentric longitude
  if (iplanet.eq.5.or.iplanet.eq.6) then
     ! Corrections for Jupiter and Saturn orbits
     call perturb(de,jlo,slo)
     if (iplanet.eq.5) lo=lo+jlo
     if (iplanet.eq.6) lo=lo+slo  
  end if
  if (lo.ge.360.) lo=lo-360.
  ra=a(iplanet,5)*(1.-a(iplanet,4)**2)/(1.+a(iplanet,4)*cos(nu/co)) ! helioc. radius
  fi=asin(sin((lo-a(iplanet,7))/co)*sin(a(iplanet,6)/co))           ! helioc. latitud
  awn=lo-a(iplanet,7) ! Angle without normalization
  call anor(awn)
  phlo=atan(tan(awn/co)*cos(a(iplanet,6)/co))*co+a(iplanet,7)
  if (awn.gt.90..and.awn.lt.270.) phlo=phlo+180.
  if (awn.eq.90..or.awn.eq.270.) then
     print *, ' Error because atan(90 or 180)'
     call sysexi(fatale)
  end if
  ! PHLO=Projected Heliocentric LOngitude
  call anor(phlo)
  prv=ra*cos(fi) ! PRV = Projected Radius Vector 
end subroutine helora
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! 
subroutine ta(m,e,nu)
  ! Subroutine to calculate the True Anomaly
  ! M = Mean anomaly in degrees (0 <= M < 360)
  ! E = Eccentricity of the orbit
  ! NU = true anomaly
  real*4 m,e,nu
  real*4 e0,e1,pi
  !
  pi=3.1415927
  m=m/180.*pi   ! To convert in radians
  e0=m          ! To initialize Newton-Raphson rule
101 e1=e0-(e0-e*sin(e0)-m)/(1.-e*cos(e0))
  if (abs(e1-e0).le.1e-6) go to 100
  e0=e1
  go to 101
100 nu=180./pi*2.*atan(sqrt((1.+e)/(1.-e))*tan(e1/2.))
  ! nu = true anomaly in degrees
  if (nu.lt.0.) nu=360.+nu  ! 0 <= nu < 360
end subroutine ta
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! 
subroutine anor(m)
  ! Subroutine to NORmalize the Angle M (degrees) between 0 <= M <360
  real*4 m
  if (m.lt.0) m=m-float((int(m/360.)-1)*360)
  m=m-float(int(m/360.)*360) ! 0 <= m < 360
end subroutine anor
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! 
subroutine perturb(de,jlo,slo)
  ! Subroutine to estimate PERTURBations in Jupiter and Saturn orbits
  ! DE = Days since Epoch 1980 JAN 0.0
  ! JLO = perturbation to add in Jupiter LOngitude (degrees)
  ! SLO = perturbation to add in Saturn LOngitude (degrees)
  integer*4 de
  real*4 jlo,slo
  !
  real*4 a,b,p,q,t,v
  real*4 co,jd
  !
  co=57.29578
  jd=44238.5+float(de)         ! really jd = jd + 2400000
  t=(jd-15020.)/36525.
  a=t/5.+.1
  p=(237.47555+3034.9061*t)/co ! in radians
  q=(265.9165+1222.1139*t)/co  ! in radians
  v=5.*q-2.*p
  b=q-p
  jlo=(.3314-.0103*a)*sin(v)-.0644*a*cos(v)
  slo=(.1609*a-.0105)*cos(v)+(.0182*a-.8142)*sin(v)-.1488*sin(b)- &
       .0408*sin(2.*b)+.0856*sin(b)*cos(q)+.0813*cos(b)*sin(q)
end subroutine perturb
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! 
subroutine ecp_mean(dse,elt,ebt,rht)
  ! Computes the Coordinates of the Planets refered to the MEAN Ecliptic and
  ! equinox of the date ; rather low precision ( a few arcmin - cf below)
  ! 
  ! DSE       [i] : integer number of days since Epoch 1980 Jan 0.
  ! ELT       [o] : array of Ecliptic Longitudes (degrees)
  ! EBT       [o] : array of Ecliptic Latitudes  (   "   )
  ! RHT       [o] : array of heliocentric radii (a.u.)
  ! 
  ! Planets are numbered from 1 to 9 ; Sun is "planet" 0
  ! 
  ! Remarks on the precision:
  ! This routine uses routines developped by J.Penalver according to the booklet
  ! "Practical Astronomy with your calculator", P.Duffett-Smith, Cambridge U.Press
  ! 
  ! - single precision calculations
  ! - orbital elements: osculating orbit at Epoch 1980 Jan 0.
  ! - perturbations included for Jupiter and Saturn, but not for the other planets
  ! - Kepler equation fully solved (Newton-Raphson iteration)
  ! - precession: included in the elements themselves, as they are referred to
  !               the mean planes of the date
  ! - no nutation
  ! - no planetary aberation correction
  ! - no parallax correction
  !
  integer*4 dse,iplanet
  real*4 elt(0:9),ebt(0:9),rht(0:9)
  real*4 el,eb_rd,rho,co
  data co/57.29578/
  !
  elt(0)=0. ! Sun
  ebt(0)=0.
  rht(0)=0.
  !
  do iplanet=1,9
     call helora(dse,iplanet,el,rho,eb_rd)
     elt(iplanet)=el
     ebt(iplanet)=eb_rd*co
     rht(iplanet)=rho/cos(eb_rd)
  enddo
  !
  return
end subroutine ecp_mean
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

