\chapter{Cookbook}

This part is a list of recipes enabling the beginner or the occasional user
the get on the air very quickly, without losing his time searching the
system's on-line {\tt HELP} facility.

\section{A {\tt CLASS}ic Session}

\begin{verbatim}
   1    device image white
   2    set angle seconds                    
   3    set coordinates equatorial           
   4    file in raw.30m                      
   5    set line 13co(1-0)                   
   6    set source ic348                     
   7    set telescope iram-30m-b30           
   8    set observed 15-aug-1984             
   9    find /offset 0  25                   
  10    set weight time                      
  11    average                                  
  12    set unit v f                         
  13    set mode x -1 14                     
  14    set mode y -0.5 7.5                  
  15    set plot histogram                   
  16    plot                                 
  17    hardcopy /print
  18    hardcopy spectrum.ps  /dev ps fast        
  19    hardcopy spectrum.eps /dev eps color
  20    set window 3 6 8 10                  
  21    base 4 /plot                         
  22    plot                                 
  23    lines 0                              
  24    gauss                                
  25    fit                                  
  26    residual                             
  27    plot                                 
  28    sic delete reduced.30m               
  29    file out reduced.30m new             
  30    swap                                 
  31    write                                
  32    save ic348                           
  33    exit                                 
\end{verbatim}
\begin{itemize}
\item[1] Define the output device.
\item[2-3] Select the coordinate system and angle unit.
\item[4] Open the input file.
\item[5-9] Build the index according to various criteria.
\item[10-11] Averaged all spectra with weights $w_i=\Delta t\Delta
  \nu/T_{\rm sys}^2$.
\item[12-16] Plot the averaged spectrum in a given velocity interval, with
  velocity for the lower axis and rest frequency for the upper axis.
\item[17-20] Make hardcopies (directly to the printer, in a ps or eps
  file).
\item[21-23] Subtract a polynomial baseline. Plot the fitted baseline.
  Plot the baseline subtracted spectrum. The baseline does not take into
  account channels corresponding to $x$ in the two specified windows, in
  current units (here velocity).
\item[24-26] Perform single Gaussian fitting and plot the result.
\item[27-28] Compute the residuals and plot them.
\item[29-30] Open a new output file.
\item[31] Recover the baseline subtracted spectrum.
\item[32] Write it.
\item[33] Write the input commands in file ic348.class.
\end{itemize}

\section{Reading a spectrum from a formatted input file}

\begin{verbatim}
  dev xl w
  greg1\column x 1 y 2 /file ''filename''
  model y x
  plot
\end{verbatim}

\section{Exporting a spectrum to a formatted file}

\begin{verbatim}
  file in toto
  find
  get f
  sic output toto.dat
  for i 1 to channels
    say 'rx[i]' 'ry[i]' /format g12.4 g12.4
  next
  sic output
\end{verbatim}


%% \section{How to produce an integrated intensity map ?}

%% \begin{verbatim}
%%    1    define real v1 v2
%%    2    define integer np
%%    3      let v1 &1
%%    4      let v2 &2
%%    5      let np &3
%%    6    file in map.30m
%%    7    find /num 1 169
%%    8    set angle sec
%%    9    print area 'v1' 'v2' /output area.dat
%%   10    greg1\column x 2 y 3 z 4 /file area.dat
%%   11    greg2\random_map 'np' /blankin -1000
%%   12    greg1\limit /rg
%%   13    clear
%%   14    clear alpha
%%   15    dev im w
%%   16    greg1\set box match 4 3 1
%%   17    greg2\pl
%%   18    greg1\box
%%   19    greg2\wedge
%%   20    greg2\levels 1 to 4 by 0.5
%%   21    set font duplex
%%   22    pen /c 7 /w 4 /das 1
%%   23    greg2\rgmap q /pen 
%%   24    pen /c 0 /w 2 /das 1
%%   25    greg2\rgmap q /pen 
%%   26    greg1\label "\GD\Ga ['']" /X
%%   27    greg1\label "\GD\Gd ['']" /Y
%%   28    greg1\draw text 0 1 "\s( T\dA\b\u* dv  -  v \s'["'v1'":"'v2'"] km/s" 5 0 /box 8
%% \end{verbatim}
%% \begin{itemize}
%% \item[1-5] Define input parameters (velocity range, number of points)
%% \item[6-7] Open input file and build the index
%% \item[9] Compute the integrated area in the velocity range
%%   \verb|[v1:v2]| and write the results in a file \verb|area.dat|
%% \item[10] Read the integrated area with offsets
%% \item[11] Build a map by triangulation interpolation
%% \item[12] Compute the limits of the plot
%% \item[13-16] Initialize device and box
%% \item[17-19] Plot the image with axes and color wedge
%% \item[20-21] Overplot contours
%% \end{itemize}
%% \begin{figure}
%%   \begin{center}
%%         \includegraphics[width=0.5\textwidth,angle=-90]{test-map}
%%         \caption{Example of a map built with the
%%           \texttt{RANDOM\_MAP} command.}
%%         \label{fig:cook-map}
%%   \end{center}
%% \end{figure}

\section{Building a Datacube from a \class{} File}

The building of a regular grid in \class{} is done in two
steps, in a similar fashion as the production of a PdBI
regular grid from UV tables. The first step involves the
building of a table: the spectra are written as rows in the
Gildas internal data format for efficiency (command
\verb?table?). When creating the TABLE, the spectra are
\emph{not} resampled on a grid. The second step is the
resampling of the non-regularly spaced spectra on a regular
grid (command \verb?xy_map?).

A simple example is given below where the cube is produced
from a single .30m file. Default parameters are used by the
\verb?xy_map? command to define both the convolution kernel
(1/3 of the $HPBW$) and the output grid (1/2 of the
$HPBW$).

\begin{verbatim}
file in map       ! Open the input file
find              ! Build the index
consistency       ! Check first that the index is consistent
let name thecube  ! Use global SIC variables to define output cube name
let type lmv      ! The output cube is named ``thecube.lmv''
table 'name' new  ! Build the non-gridded table
xy_map 'name'     ! Grid the data from the table
go view           ! Check the quality of the data reduction with the VIEW tool
\end{verbatim}

The following example illustrates the way to build a cube
from several .30m files. This is typically the case when the
map has been repeated several times and the data have been
calibrated and reduced in different files.

\begin{verbatim}
file in map1      ! Open the input file
find              ! Build the index
consistency       ! Check first that the index is consistent
let name thecube  ! Use global SIC variables to define output cube name
let type lmv      ! 
table 'name' new  ! Build the non-gridded table as a new table
file in map2      ! Open the input file
find              ! Build the index
table 'name'      ! Append the index to the current table
file in map3      ! Open the input file
find              ! Build the index
table 'name'      ! Append the index to the current table
...
xy_map 'name'     ! Grid the data from the global table
go view           ! Check the quality of the data reduction with the VIEW tool
\end{verbatim}


\section{Fitting an HyperFine Structure}
\subsection{Assumptions}
\begin{enumerate}
\item[A1:] same excitation temperature for all the components of
  the multiplet
\item[A2:] Gaussian profiles for the opacity as a function of
  frequency
\item[A3:] the lines all have the same width
\item[A4:] the multiplet components do not overlap
\item[A5:] the main beam temperature is well suited for your source
\end{enumerate}


\subsection{Parameters of the multiplet}
When selecting the HFS method, you must give the name of a
file that contains the relative positions and intensities of
the components of your multiplet:\\
\verb|LAS> method hfs hfs-n2hp.dat|\\
Relative intensities may be normalized or not. Here follow
two examples, for the N$_2$H$^+$ multiplet.
\begin{verbatim}
  7               ! 1st line: Number of components
   6.9360  1.     ! 1st column: Velocity shift, for each component,
   5.9841  5.     !             (in km/s) with respect to the reference
   5.5452  3.     !             component you choose (here the 5th one).
   0.9560  5.     ! 2nd column: The relative strength of each component.
   0.0000  7.     !             Here, they are not normalized.
  -0.6109  3.     !
  -8.0064  3.     !
\end{verbatim}
\begin{verbatim}
  7                  ! As before.
  14.9424 1./27.     ! The reference velocity is now that of the last
  13.9906 5./27.     ! component.
  13.5516 3./27.     ! The intensities are normalized (you could also
  8.9624  5./27.     ! put numerical values rather that fractions).
  8.0064  7./27.     !
  7.3955  3./27.     !
  0.      3./27.     !
\end{verbatim}

\def\vi{\ensuremath{v_i}}
\def\ri{\ensuremath{r_i}}

Let us call \vi{} and \ri{} the positions and relative
intensities of the $N$ components of the multiplet. We
define $S=\sum\ri$. In the first case, we thus have $N=7$
and $S=27$.
%\begin{eqnarray*}
%  N &=& 7\\
%  {\vi}_{i=1,N} &=& {6.9360,5.9841,5.5452,0.9560,0,-0.6109,-8.0064}\\
%  {\ri}_{i=1,N} &=& {1,5,3,5,7,3,3}\\
%  S &=& 27\{}  
%\end{eqnarray*}


\subsection{The fitting procedure}
According to assumptions A3 and A4, the opacity of the $i$th
component is written:
\begin{equation}
  \tau_i(v) = \tau_i\cdot\exp\left[
  -4\ln{2}\left(
  \frac{v-v_{0,i}}{p_3}
  \right)^2
  \right]
\end{equation}
where $p_3$ is the common FWHM of all components. The
central velocity of component $i$ is $v_{0,i}=v_i+p_2$,
where $p_2$ is the velocity of the reference component
(\textit{i.e.} the one with $v_i=0$).

The opacity of the multiplet is the sum of the $N$
opacities:
\begin{equation}
  \tau(v) = p_4 \displaystyle\sum_{i=1}^{N}
  r_i\cdot\exp\left[
  -4\ln{2}\left(
  \frac{v-v_i-p_2}{p_3}
  \right)^2
  \right]
\end{equation}
Given the opacity $\tau(v)$, the antena temperature is given by
\begin{equation}
  T_{\rm ant}(v) = \frac{p_1}{p_4}
  \left(
  1 - e^{-\tau(v)}
  \right)
\end{equation}
From these equations, we deduce:
\begin{eqnarray*}
  \tau(v_i+p_2) &=& p_4 \cdot r_i\qquad (\rm assumption\, A4)\\
  \displaystyle\sum_i \tau_i &=& p_4 \cdot S\\
  T_{\rm ant}(v_i+p_2) &=& \frac{p_1}{p_4}\left(1 - e^{-p_4 r_i}\right)\\
  T_{\rm ant}(v_i+p_2) &\approx& p_1 \cdot r_i
\end{eqnarray*}
where the last equality holds in the optically thin regime.

This implies that the physical meaning of $p_4$ depends on
the value of $S$: If the relative intensities are normalized
to unity, then $S=1$ and $p_4$ equals the sum of all
centerline opacities.

The results of the HFS fitting procedure are:
\begin{verbatim}
   Line     T ant * Tau           V lsr          Delta V            Tau main

   1    1.313 ( 0.018)    3.783 ( 0.001)    0.589 ( 0.003)    0.230 ( 0.006)
\end{verbatim}
where \verb|T ant * Tau|=$p_1$, \verb|V lsr|=$p_2$, \verb|Delta V|=$p_3$
and \verb|Tau main|=$p_4$.

According to assumptions A1 and A5, the hyperfine structure
fitting procedure allows you to deduce the excitation
temperature, since (assuming the Rayleigh-Jeans regime is
valid, which is not true at $\lambda<3$mm...):
\begin{equation}
  T_{\rm ant}(v) = T_A^*(v) = \frac{B_{\rm eff}}{F_{\rm eff}}
        [T_{\rm ex} - T_{\rm bg}] (1-e^{-\tau(v)})
\end{equation}
Finally, the excitation temperature is given by:
\begin{equation}
  T_{\rm ex} = T_{\rm bg} + \frac{F_{\rm eff}}{B_{\rm eff}} \frac{p_1}{p_4}
\end{equation}

\noindent{\bf Note}: the main group opacity is limited to
the range $0.1-30$ since outside these limits, the problem
becomes degenerate because the opacity no longer appears in
the equations (in the optically thin limit, line ratio no
longer depend on the opacity, and in the thick limit,
$\exp(-\tau)\ll 1$).

\subsection{Typical Analysis Sequence}
\label{proc:hfs}
This routine produces the figure \ref{fig:hfs}.
\begin{verbatim}
   1  set plot histo
   2  set format brief
   3  clear
   4  clear alpha
   5  file in prov.30m
   6  find
   7  get f
   8  modify source TOTO
   9  set unit v c
  10  method hfs hfs-n2hp.lin
  11  minimize
  12  set viewport 0.2 0.9 0.4 0.8
  13  set mode x 0 30
  14  box n o i p
  15  spec
  16  title
  17  pen /c 0 /w 2 /das 2
  18  visualize /pen
  19  pen /def
  20  display 1.5
  21  set viewport 0.2 0.9 0.2 0.4
  22  box p o i n /unit v upper
  23  residual
  24  spectr
  25  define real rms
  26  compute rms RMS ry
  27  greg1\draw test 1 -0.5 "\gs = "'nint(rms*100)/100.'" K" 6 0 /box 7
\end{verbatim}
\begin{figure}
  \begin{center}
    \includegraphics[width=0.6\textwidth,angle=-90]{class-hfs}
    \caption{Result of the HFS fitting method (see \ref{proc:hfs}).}
    \label{fig:hfs}
  \end{center}
\end{figure}

\begin{verbatim}
\end{verbatim}

\section{Subtracting Baseline on Large Datasets}
\label{cook6}

\begin{verbatim}
   1  set verbose off
   2  set level 10
   3  dev im w
   4  greg1\set plot portrait
   5  set format brief
   6  set angle sec
   7  file in co21.30m
   8  find /range -6 6 -6 6
   9  load
  10  plot /index
  11  set window /polygon 1
  12  base 1 /index
  13  plot /index
  14  sic rename window-1.pol my-window-1.pol
  15  sic delete co21-base.30m
  16  file out co21-base.30m new
  17  for i 1 to found
  18    get n
  19    base 1
  20    write
  21  next
  22  file in co21.bas
  23  find
  24  load
  25  plot /index
\end{verbatim}
\begin{enumerate}
\item Makes \class{} quiet.
\item Turn off nearly all output messages.
\item Open an image window (necessary to plot 2D images).
\item Change the orientation to portrait.
\item Title in short format.
\item Set arcsecond units.
\item Open the file.
\item Build an index according to offsets.
\item Build a 2D array.
\item Display the whole index.
\item Define a polygon interactively to set spectral windows. By default
  the polygon is stored in a file window-1.pol.  
%%   A global variable \verb|WINDOW[found,1+2*nwind]| contains the spectral
%%   window(s) for each record. For each record, \textit{e.g.} record
%%   \verb|ir|, \verb|WINDOW[ir,1]|=number of spectral windows, while
%%   \verb|WINDOW[ir,2:]|= the boudaries of the spectral window(s). In the
%%   present case, \verb|WINDOW[ir,1]|=1 and \verb|WINDOW[ir,2:3]| gives the
%%   spectral window.
\item Subtract a 1st order polynom to all records.
\item Display the result.
\item Change the polygon filename (to avoid overwritting it next time).
\item Delete output file if it exists.
\item Open the output file as new.
\item[17-21] Make a loop over the index to subtract the baseline and write
  the result.
\item[22-25] Display the result from the output file.
\end{enumerate}

\section{Reading/Writing FITS file}
\label{cook-fits}

\subsection{Exporting \class{} Spectra through FITS}

\begin{verbatim}
   1  set angle sec
   2  file in co21.30m
   3  find /range -2 2 -2 2
   4  get f
   5  set fits mode spectrum
   6  fits write single.fits
   7  find /range -2 2 -2 2
   8  fits write index.fits /mode index
\end{verbatim}
\begin{enumerate}
\item Angles are expressed in arcseconds.
\item Open the .30m file.
\item Build the index.
\item Store the first spectrum in \verb|R| memory.
\item Default mode for writing \verb|FITS| is set to single spectrum.
\item Write the \verb|R| memory in \verb|FITS| format file 'single.fits'.
\item Re-build the index.
\item Write the whole index in \verb|FITS| format file 'index.fits'.
\end{enumerate}

\subsection{Importing FITS spectra into \class{}}

\begin{verbatim}
   1  set angle sec
   2  fits read single
   3  file out single.30m single /overwrite
   4  write
   5  file in single.30m
   6  find
   7  get f
   8  plot
\end{verbatim}
\begin{enumerate}
\item Angles are expressed in arcseconds.
\item Read a single \verb|FITS| spectrum.
\item[3-4] Open a new .30m file.
\item Write the \verb|FITS| spectrum in the .30m file.
\item[6-9] Check the result by plotting the spectrum from the .30m file.
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "class"
%%% End: 
