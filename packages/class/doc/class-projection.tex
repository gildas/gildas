\documentclass{article}

\usepackage{graphicx}
\usepackage{multirow}

\makeindex{}

\makeatletter
\textheight 625pt
\textwidth 460pt
\oddsidemargin 0pt
\evensidemargin 0pt
\marginparwidth 50pt
\topmargin 0pt
\brokenpenalty=10000

\newcommand{\ie} {{\em i.e.}}
\newcommand{\eg} {{\em e.g.}}
\newcommand{\cf} {cf.}

\newcommand{\gildas}    {\mbox {\bf GILDAS}}
\newcommand{\sic}       {\mbox {\bf SIC}}
\newcommand{\greg}      {\mbox {\bf GREG}}
\newcommand{\class}     {\mbox {\bf CLASS}}
\newcommand{\classic}   {\mbox {\bf CLASSIC}}

\newcommand{\emm}[1]{\ensuremath{#1}}   % Ensures math mode
\newcommand{\emr}[1]{\emm{\mathrm{#1}}} % Uses math roman fonts

\def\degr{\hbox{$^\circ$}}
\def\arcmin{\hbox{$^\prime$}}
\def\arcsec{\hbox{$^{\prime\prime}$}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\title{IRAM Memo 2015-1\\[3\bigskipamount]
  Extended support of sky spherical coordinates in \class}%
\author{S. Bardeau$^1$, J. Pety$^{1,2}$ \\[0.5cm]
  1. IRAM (Grenoble)\\
  2. LERMA, Observatoire de Paris}
\date{March, 26$^{th}$ 2015\\
      Version 1.0}

\maketitle

\begin{abstract}

  Up to now, \class\ only supported natively the radio projection of
  the sky spherical coordinates. The main limitation of the radio
  projection is the absence of support of a projection angle. This
  could imply some approximation in the handling of On-The-Fly data
  with rotated scanning directions. \class\ now supports all the
  projections already supported in the \gildas\ kernel, i.e., none
  (unprojected spherical coordinates), gnomonic, orthographic,
  azimuthal, stereographic, lambert, aitoff, radio, and sfl. This
  required the introduction of the {\tt MODIFY PROJECTION} command,
  the modification of the {\tt MODIFY POSITION} command, and the
  modification of the position header section in the \class\ Data
  Format. This memo describes all this in details.\\

  Keywords: coordinates, (re)projection, \class\ Data Format

  Related documents: \class\ documentation, \classic\ Data Container
\end{abstract}

\newpage{}
\tableofcontents{}

\newpage{}

\section{Goal}
\label{txt:goal}

The goal of the present \class\ (data format and library) improvements
is to provide a (much) less restrictive list of supported
projections. This revision relies on the \greg\ projection engines
which support:
\begin{itemize}
\item more projection systems, namely: none (unprojected spherical
  coordinates), gnomonic, orthographic, azimuthal, stereographic,
  lambert, aitoff, radio, and sfl.
\item non-zero projection angles (when allowed by the projection
  system). Before this revision, this support was not possible in
  \class\ since there was no provision of such angle in the \class\
  Data Format.
\end{itemize}

With these improvements, a direct application is for example the
native support of the rotated (i.e. scanning is not performed along
azimuth or elevation) On-The-Fly mapping when observing at the
telescope, using the azimuthal projection with a non-zero angle.

\section{The new Position section in the \class\ Data Format}

In order to achieve the goal described in section~\ref{txt:goal}, the
section Position in the \class\ files is redesigned as described in
table~\ref{tab:section}. This section is delivered in the version 2 of
the observations\footnote{See the \classic\ Data Container memo about
  versioning of the observations.}. The modifications are:
\begin{itemize}
\item the projection angle has been added,
\item the coordinate system is duplicated from the index\footnote{Note
    that the General section is not affected ON THE DISK. See
    section~\ref{txt:api} for the effects on the General section IN
    MEMORY.} into the Position section,
\item the \emph{descriptive system} has been removed; it had no known
  application up to the present day,
\item the items have been reordered for clarity.
\end{itemize}

\begin{table}[htb]
\centering
\caption{New Position section in observations version 2 (since
  26-mar-2015 in the \gildas\ development branch). The elements are
  ordered this way, contiguously in the file. The section length is
  now 14 words (1 word is 4 bytes).}
\vspace{1ex}
\begin{tabular}{llcl}
Fortran type & Parameter & Position & Description \\
\hline
C*12 & SOURC   &   1-3 & Source name                 \\
I*4  & SYSTEM  &     4 & Code for coordinate system  \\
R*4  & EQUINOX &     5 & Equinox of coordinates      \\
I*4  & PROJ    &     6 & Code for projection system  \\
R*8  & LAM     &   7-8 & Lambda of projection center \\
R*8  & BET     &  9-10 & Beta of projection center   \\
R*8  & PROJANG & 11-12 & Projection angle            \\
R*4  & LAMOF   &    13 & Offset in Lambda            \\
R*4  & BETOF   &    14 & Offset in Beta
\end{tabular}
\label{tab:section}
\end{table}

As a reminder and for comparison, the description of the old Position
section is available in the Appendix~\ref{app:oldsection}.

\section{Support in \class}

\subsection{Fillers and variables}
\label{txt:api}

The standard \class\ fillers reflect the changes in the Position
section. At the Fortran level, the {\tt type(observation)} is modified
as follows:
\begin{itemize}
\item {\tt obs\%head\%pos\%projang} (angle in radians) is added,
\item {\tt obs\%head\%pos\%system} replaces {\tt
    obs\%head\%gen\%typec}. Its value remains the special \gildas\
  code for coordinate systems. Note that {\tt obs\%head\%gen\%typec}
  was a convenient duplicate in memory of the corresponding index
  element, so this affects only the General section in memory, not on
  disk.
\item {\tt obs\%head\%pos\%sl0p}, {\tt obs\%head\%pos\%sb0p} and {\tt
    obs\%head\%pos\%sk0p} are removed.
\end{itemize}

At the end-user level, the following \sic\ variables are affected:
\begin{itemize}
\item {\tt R\%HEAD\%POS\%PROJANG} is added,
\item {\tt R\%HEAD\%POS\%SYSTEM} replaces {\tt R\%HEAD\%GEN\%TYPEC}
  and {\tt TYPEC} which are removed,
\item {\tt R\%HEAD\%POS\%SL0P}, {\tt R\%HEAD\%POS\%SB0P} and {\tt
    R\%HEAD\%POS\%SK0P} are removed,
\end{itemize}

Finally, for Python-based fillers, the attributes are changed in the
same way:
\begin{itemize}
\item {\tt obs.head.pos.projang} is added,
\item {\tt obs.head.pos.system} replaces {\tt obs.head.gen.typec},
\item {\tt obs.head.pos.sl0p}, {\tt obs.head.pos.sb0p} and {\tt
    obs.head.pos.sk0p} are removed.
\end{itemize}


\subsection{Commands}

The \class\ commands dealing with the position of the spectrum are not
affected by the renaming of the coordinate system, nor by the
reordering of the variables. On the other hand, several commands are
added or improved to fully benefit the new projection kinds and
possible non-zero angle.

\begin{itemize}

\item {\tt MODIFY PROJECTION} is added. It allows to modify the
  current R spectrum projection, including setting a new projection
  and a non-zero angle. It uses the \greg\ reprojection engines from
  any projection to another.

\item {\tt MODIFY POSITION} is now a shortcut for {\tt MODIFY
    PROJECTION}, in the particular case when the projection system and
  angle are left unchanged. Thanks to this, it supports now any kind
  of projection.

\item {\tt CONSISTENCY} now checks in details the projection of all
  observations in index, i.e. the projection systems, centers, and
  angles.

\item {\tt TABLE} benefits the {\tt CONSISTENCY} improvements, in both
  {\tt NEW} or {\tt OLD} modes. In the latter case, {\tt TABLE}
  verifies that the appended spectra have a projection consistent with
  the old ones. The projection description of the spectra (including
  their angle) is saved in the table header.

\item {\tt XY\_MAP} benefits the {\tt TABLE} improvements, including
  the possible non-zero projection angle in the table header.

\end{itemize}

\newpage
\appendix

\section{Old Position section}
\label{app:oldsection}

\begin{table}[h]
\centering
\caption{Old Position section in observations version 1 (before 26-mar-2015
  in \gildas\ development branch). The elements are ordered this way,
  contiguously in the file. The section length is 17 words.}
\vspace{1ex}
\begin{tabular}{llcl}
Fortran type & Parameter & Position & Description \\
\hline
C*12 & SOURC   &   1-3 & Source name                  \\
R*4  & EQUINOX &     4 & Equinox of coordinates       \\
R*8  & LAM     &   5-6 & Lambda of projection center  \\
R*8  & BET     &   7-8 & Beta of projection center    \\
R*4  & LAMOF   &     9 & Offset in Lambda             \\
R*4  & BETOF   &    10 & Offset in Beta               \\
I*4  & PROJ    &    11 & Code for projection system   \\
R*8  & SL0P    & 12-13 & Lambda of descriptive system \\
R*8  & SB0P    & 14-15 & Beta of descriptive system   \\
R*8  & SK0P    & 16-17 & Angle of descriptive system
\end{tabular}
\end{table}

\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
