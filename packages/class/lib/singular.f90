!	PROGRAM TEST
!	REAL X(256),Y(256),WEIGHT(256),A(32)
!	REAL U(256,32),V(32,32),W(32)
!	INTEGER I,MA,NP
!	REAL CHISQ
!	LOGICAL ERROR
!	EXTERNAL FPOLY,FCHEB
!*
!	NP = 24
!	DO I=1,NP
!	    X(I) = (2*I-(1+NP))/FLOAT(NP-1)	! Normalize range
!!	    Y(I) = x(i)**2+12*X(I)-13
!	    Y(I) = SIN(X(I))**3+SIN(X(I))**2
!	    WEIGHT(I) = 1.
!	ENDDO
!	write(6,*) 'MA?'
!	read (5,*) MA
!	CALL SVDFIT(X,Y,WEIGHT,NP,A,MA,U,V,W,256,32,CHISQ,FPOLY,ERROR)
!	WRITE(6,*) 'Chi square: ',CHISQ
!	WRITE(6,*) (A(I),I=1,MA)
!	CALL SVDFIT(X,Y,WEIGHT,NP,A,MA,U,V,W,256,32,CHISQ,FCHEB,ERROR)
!	WRITE(6,*) 'Chi square: ',CHISQ
!	WRITE(6,*) (A(I),I=1,MA)
!	END
!
subroutine svdfit(x,y,weight,ndata,a,ma,u,v,w,mp,np,chisq,funcs,error)
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  ! Singular value decomposition
  ! Given a set of NDATA points X(I), Y(I) with individual weights
  ! WEIGHT(I), use Chi2 minimization to determine the MA coefficients A of the
  ! fitting function. Here we solve the fitting
  ! equations using singular value decomposition of the NDATA by MA matrix.
  ! Arrays U,V,W provide workspace on input. On output they define the singular
  ! value decomposition and can be used to obtain the covariance matrix. MP, NP
  ! are the physical dimensions of the matrices U, V, W, as indicated below. It
  ! is necessary that MP>=NDATA, NP>=MA. The programs returns values for the MA
  ! fit parameters and Chi2, CHISQ. The user supplies a subroutine
  ! FUNCS(X,AFUNC,MA) that returns the MA basis functions evaluated at x=X in the
  ! array AFUNC.
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)    :: ndata          !
  real(kind=4),    intent(in)    :: x(ndata)       !
  real(kind=4),    intent(in)    :: y(ndata)       !
  real(kind=4),    intent(in)    :: weight(ndata)  !
  integer(kind=4), intent(in)    :: ma             !
  real(kind=4),    intent(out)   :: a(ma)          !
  integer(kind=4), intent(in)    :: mp             !
  integer(kind=4), intent(in)    :: np             !
  real(kind=4),    intent(inout) :: u(mp,np)       !
  real(kind=4),    intent(inout) :: v(np,np)       !
  real(kind=4),    intent(inout) :: w(np)          !
  real(kind=4),    intent(out)   :: chisq          ! Chi^2
  external                       :: funcs          !
  logical,         intent(out)  :: error           !
  ! Local
  real, parameter :: tol=1.e-5
  real :: b(ndata)   ! Automatic array
  real :: afunc(ma)  ! Automatic array
  integer :: i,j
  real :: wmax,thresh,sum,tmp
  !
  error = .false.
  do i=1,ndata
     call funcs(x(i),afunc,ma)
     tmp = sqrt(weight(i))
     do j=1,ma
        u(i,j) = afunc(j)*tmp
     enddo
     b(i) = y(i)*tmp
  enddo
  call svdcmp(u,ndata,ma,mp,np,w,v,error)
  if (error) return
  ! Edit out the (nearly) singular values
  wmax = 0.
  do j=1,ma
     if (w(j).gt.wmax) wmax=w(j)
  enddo
  thresh = tol*wmax
  do j=1,ma
     if (w(j).lt.thresh) w(j) = 0
  enddo
  ! Solve the equations
  call svbksb(u,w,v,ndata,ma,mp,np,b,a,error)
  if (error) return
  ! Evaluate chi-square
  chisq = 0.
  do i=1,ndata
     call funcs(x(i),afunc,ma)
     sum=0.
     do j=1,ma
        sum = sum+a(j)*afunc(j)
     enddo
     chisq = chisq+(y(i)-sum)**2*weight(i)
  enddo
end subroutine svdfit
!
subroutine svdcmp(a,m,n,mp,np,w,v,error)
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  ! Singular value decomposition
  ! Given a matrix A with logical dimensions M by N and physical dimensions MP
  ! by NP, this routine computes its singular value decomposition
  !	A = U.W.Vt
  ! The matrix U replaces A on output. The diagonal matrix of singular values W
  ! is a output as a vector W. The matrix V (not the transpose Vt) is output as
  ! V. M must be greater or equal to N. If it is smaller, then A should be filled
  ! up to square with zero rows.
  ! Typed from "numerical recipes".  TF
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)    :: mp        !
  integer(kind=4), intent(in)    :: np        !
  real(kind=4),    intent(inout) :: a(mp,np)  !
  integer(kind=4), intent(in)    :: m         !
  integer(kind=4), intent(in)    :: n         !
  real(kind=4),    intent(out)   :: w(np)     !
  real(kind=4),    intent(out)   :: v(np,np)  !
  logical,         intent(out)   :: error     !
  ! Local
  integer(kind=4), parameter :: nmax=100 ! Maximum anticipated value of N
  real ::  rv1(nmax)
  integer :: i,j,k,l,nm,its,jj
  real :: scale,anorm,c,s,x,y,z
  real :: f,g,h
  !
  if (n.gt.nmax) then
     call class_message(seve%e,'SVDCMP','NMAX dimension too small. Will need to recompile.')
     error = .true.
     return
  elseif (m.lt.n) then
     call class_message(seve%e,'SVDCMP','You must augment A with extra zero rows.')
     error = .true.
     return
  endif
  ! Householder reduction to diagonal form
  g = 0.0
  scale = 0.0
  anorm = 0.0
  do i=1,n
     l=i+1
     rv1(i) = scale*g
     g = 0.0
     s = 0.0
     scale = 0.0
     if (i.le.m) then
        do k=i,m
           scale = scale+abs(a(k,i))
        enddo
        if (scale.ne.0.0) then
           do k=i,m
              a(k,i) = a(k,i)/scale
              s = s+a(k,i)*a(k,i)
           enddo
           f = a(i,i)
           g = -sign(sqrt(s),f)
           h = f*g-s
           a(i,i) = f-g
           if(i.ne.n) then
              do j=l,n
                 s=0.0
                 do k=i,m
                    s = s+a(k,i)*a(k,j)
                 enddo
                 f = s/h
                 do k=i,m
                    a(k,j) = a(k,j)+f*a(k,i)
                 enddo
              enddo
           endif
           do k=i,m
              a(k,i) = scale*a(k,i)
           enddo
        endif
     endif
     w(i) = scale*g
     g = 0.0
     s = 0.0
     scale = 0.0
     if ((i.le.m).and.(i.ne.n)) then
        do k=l,n
           scale = scale+abs(a(i,k))
        enddo
        if (scale.ne.0) then
           do k=l,n
              a(i,k)=a(i,k)/scale
              s=s+a(i,k)*a(i,k)
           enddo
           f=a(i,l)
           g=-sign(sqrt(s),f)
           h=f*g-s
           a(i,l)=f-g
           do k=l,n
              rv1(k)=a(i,k)/h
           enddo
           if (i.ne.m) then
              do j=l,m
                 s=0.0
                 do k=l,n
                    s=s+a(j,k)*a(i,k)
                 enddo
                 do k=l,n
                    a(j,k)=a(j,k)+s*rv1(k)
                 enddo
              enddo
           endif
           do k=l,n
              a(i,k)=scale*a(i,k)
           enddo
        endif
     endif
     anorm = max(anorm,(abs(w(i))+abs(rv1(i))))
  enddo
  ! Accumulation of right hand transformation
  do i=n,1,-1
     if (i.lt.n) then
        if (g.ne.0.0) then
           do j=l,n               ! Double division avoids possible underflow
              v(j,i) = (a(i,j)/a(i,l))/g
           enddo
           do j=l,n
              s=0.0
              do k=l,n
                 s=s+a(i,k)*v(k,j)
              enddo
              do k=l,n
                 v(k,j)=v(k,j)+s*v(k,i)
              enddo
           enddo
        endif
        do j=l,n
           v(i,j) = 0.0
           v(j,i) = 0.0
        enddo
     endif
     v(i,i) = 1.0
     g = rv1(i)
     l = i
  enddo
  ! Accumulation of left hand transformations.
  do i=n,1,-1
     l = i+1
     g = w(i)
     if (i.lt.n) then
        do j=l,n
           a(i,j) = 0.0
        enddo
     endif
     if (g.ne.0.0) then
        g = 1.0/g
        if (i.ne.n) then
           do j=l,n
              s = 0.0
              do k=l,m
                 s = s+a(k,i)*a(k,j)
              enddo
              f = (s/a(i,i))*g
              do k=i,m
                 a(k,j)=a(k,j)+f*a(k,i)
              enddo
           enddo
        endif
        do j=i,m
           a(j,i) = a(j,i)*g
        enddo
     else
        do j=i,m
           a(j,i) = 0.0
        enddo
     endif
     a(i,i) = a(i,i)+1.0
  enddo
  ! Diagonalization of the bidiagonal form
  do k=n,1,-1                  ! Loop over singular values
     do its=1,30                ! Loop over allowed iterations
        do l=k,1,-1              ! Test for splitting:
           nm=l-1                 ! Note that RV1(1) is always zero
           if ((abs(rv1(l))+anorm).eq.anorm) goto 2
           if ((abs(w(nm))+anorm).eq.anorm) goto 1
        enddo
1       continue
        c=0.0        ! Cancellation of RV1(L), if L>1
        s=1.0
        do i=l,k
           f=s*rv1(i)
           if ((abs(f)+anorm).ne.anorm) then
              g = w(i)
              h = sqrt(f*f+g*g)
              w(i) = h
              h = 1.0/h
              c = (g*h)
              s = -f*h
              do j=1,m
                 y = a(j,nm)
                 z = a(j,i)
                 a(j,nm)=(y*c)+(z*s)
                 a(j,i)=-(y*s)+(z*c)
              enddo
           endif
        enddo
2       continue
        z=w(k)
        if (l.eq.k) then         ! Convergence
           if (z.lt.0.0) then     ! Singular value is made non negative
              w(k)=-z
              do j=1,n
                 v(j,k)=-v(j,k)
              enddo
           endif
           goto 3
        endif
        if (its.eq.30) then
           call class_message(seve%e,'SVDCMP','No convergence in 30 iterations.')
           error = .true.
           return
        endif
        x=w(l)                   ! Shift from bottom 2-by-2 minor
        nm = k-1
        y = w(nm)
        g = rv1(nm)
        h = rv1(k)
        f = ((y-z)*(y+z)+(g-h)*(g+h))/(2.0*h*y)
        g = sqrt(f*f+1.0)
        f = ((x-z)*(x+z)+h*((y/(f+sign(g,f)))-h))/x
        ! Next QR transformation
        c=1.0
        s=1.0
        do j=l,nm
           i=j+1
           g=rv1(i)
           y=w(i)
           h=s*g
           g=c*g
           z=sqrt(f*f+h*h)
           rv1(j) = z
           c=f/z
           s=h/z
           f= (x*c)+(g*s)
           g=-(x*s)+(g*c)
           h=y*s
           y=y*c
           do jj=1,n
              x=v(jj,j)
              z=v(jj,i)
              v(jj,j)= (x*c)+(z*s)
              v(jj,i)=-(x*s)+(z*c)
           enddo
           z=sqrt(f*f+h*h)
           w(j) = z
           if (z.ne.0) then       ! Rotation can be arbitrary if Z=0
              z=1.0/z
              c=f*z
              s=h*z
           endif
           f= (c*g)+(s*y)
           x=-(s*g)+(c*y)
           do jj=1,m
              y=a(jj,j)
              z=a(jj,i)
              a(jj,j) = (y*c)+(z*s)
              a(jj,i) =-(y*s)+(z*c)
           enddo
        enddo
        rv1(l)=0.0
        rv1(k)=f
        w(k)=x
     enddo
3    continue
  enddo
end subroutine svdcmp
!
subroutine svbksb(u,w,v,m,n,mp,np,b,x,error)
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  ! Singular value decomposition
  ! Solves A.X = B for a vector X, where A is specified by the arrays U,W,V as
  ! returned by SVDCMP. M and N are the logical dimensions of A, and will be
  ! equal for square matrices. MP and NP are the physical dimensions of A. B
  ! is the input right hand side. X is the output soolution vector. No input
  ! quantities are destroyed, so the routine may be called sequentially with
  ! different B's.
  ! Typed from "numerical recipes".  TF
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)  :: mp        !
  integer(kind=4), intent(in)  :: np        !
  real(kind=4),    intent(in)  :: u(mp,np)  !
  real(kind=4),    intent(in)  :: w(np)     !
  real(kind=4),    intent(in)  :: v(np,np)  !
  integer(kind=4), intent(in)  :: m         !
  integer(kind=4), intent(in)  :: n         !
  real(kind=4),    intent(in)  :: b(mp)     !
  real(kind=4),    intent(out) :: x(np)     !
  logical,         intent(out) :: error     !
  ! Local
  integer(kind=4), parameter :: nmax=100
  real(kind=4) :: tmp(nmax),s
  integer(kind=4) :: i,j,jj
  !
  if (n.gt.nmax) then
     call class_message(seve%f,'SVDCMP','NMAX dimension too small -- Will need to recompile.')
     error = .true.
     return
  endif
  ! Calculate UtB
  do j=1,n
     s = 0.
     if (w(j).ne.0.) then
        do i=1,m
           s=s+u(i,j)*b(i)
        enddo
        s = s/w(j)
     endif
     tmp(j) = s
  enddo
  ! Matrix multiply by V to get answer
  do j=1,n
     s = 0.0
     do jj=1,n
        s = s+v(j,jj)*tmp(jj)
     enddo
     x(j) = s
  enddo
end subroutine svbksb
!
subroutine fpoly(x,p,np)
  !---------------------------------------------------------------------
  ! @ private
  ! Fitting routine for a polynomial of degree NP-1.
  !---------------------------------------------------------------------
  real(kind=4),    intent(in)  :: x      ! Variable value
  integer(kind=4), intent(in)  :: np     ! Polynom degree
  real(kind=4),    intent(out) :: p(np)  ! NP values of Chebychev polynoms
  ! Local
  integer :: j
  !
  p = (/(x**(j-1),j=1,np)/)
end subroutine fpoly
!
subroutine fcheb(x,p,np)
  !---------------------------------------------------------------------
  ! @ private
  ! Compute the first NP-1 Chebychev polynom of first kind at x
  !    T_n(x), n=0...np-1
  ! Recurrence relation : T_(n+1)(x) - 2xT_(n)(x) + T_(n-1)(x) = 0
  ! Initialization      : T_0 = 1
  !                     : T_1 = x
  !---------------------------------------------------------------------
  real(kind=4),    intent(in)  :: x      ! Variable value
  integer(kind=4), intent(in)  :: np     ! Polynom degree
  real(kind=4),    intent(out) :: p(np)  ! NP values of Chebychev polynoms
  ! Local
  integer :: j
  !
  p(1) = 1.
  if (np.gt.1) then
     p(2) = x
     do j=3,np
        p(j)=2*p(j-1)*x-p(j-2)
     enddo
  endif
end subroutine fcheb
