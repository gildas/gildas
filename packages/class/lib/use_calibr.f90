module class_calibr
  integer(kind=4), parameter :: mode_auto=1
  integer(kind=4), parameter :: mode_trec=2
  integer(kind=4), parameter :: mode_humi=3
  integer(kind=4), parameter :: mode_manu=4
  !
  integer(kind=4) :: cal_mode
  real(kind=4) :: cal_tcol
  real(kind=4) :: cal_tcho
  real(kind=4) :: cal_tsys
  real(kind=4) :: cal_trec
  real(kind=4) :: cal_gain
  real(kind=4) :: cal_taon
  real(kind=4) :: cal_feff
  real(kind=4) :: cal_wh2o
  real(kind=4) :: cal_atms
  real(kind=4) :: cal_atmi
  real(kind=4) :: cal_taus
  real(kind=4) :: cal_taui
  real(kind=4) :: cal_ceff  ! chopper efficiency
  !
  integer(kind=4), parameter :: mxrec=1
  real(kind=4) :: tauoxs(mxrec)
  real(kind=4) :: tauws(mxrec)
  real(kind=4) :: tauoxi(mxrec)
  real(kind=4) :: tauwi(mxrec)
  real(kind=4) :: tcal(mxrec)
  !
  common /comcal/ tauoxs, tauws, tauoxi, tauwi, tcal,  & 
  &cal_mode,cal_tcol,cal_tcho,cal_tsys,cal_trec,        &
  &cal_gain,cal_taon,cal_feff,cal_wh2o,cal_atms,        &
  &cal_atmi,cal_taus,cal_taui,cal_ceff
end module class_calibr
