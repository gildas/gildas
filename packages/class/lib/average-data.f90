module sumlin_mod_second
  use class_types
  !---------------------------------------------------------------------
  !  Support variables for second run through index, in order to
  ! support data addition
  !---------------------------------------------------------------------
  !
  ! Observation which stores the resampled versions of the input spectra
  type(observation) :: obs_resampled
  logical, parameter :: doassoc=.true.
  !
end module sumlin_mod_second
!
subroutine sumlin_data_prepro(aver,io,assoc,error)
  use classcore_interfaces, except_this=>sumlin_data_prepro
  use class_averaging
  use class_types
  use sumlin_mod_second
  !---------------------------------------------------------------------
  ! @ private
  !  Data pre-preprocessing: initialize data before computing
  !---------------------------------------------------------------------
  type(average_t),     intent(in)    :: aver   !
  type(observation),   intent(inout) :: io     ! I/O sum
  type(class_assoc_t), intent(in)    :: assoc  ! Associated Array section (can be empty)
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  real(kind=4) :: val
  !
  ! Prepare 'io' to store its data
  call prepare_obs(io,aver%done%axis%nchan,assoc,error)
  if (error)  return
  !
  ! Prepare the sum value
  ! data1(:):
  !  - avoid NaN (uninitialized values)
  !  - avoid bad, because they would pollute the whole sum if SET BAD OR
  val = 0.0
  if (aver%done%kind_is_spec) then
    if (val.eq.io%head%spe%bad) val = -1.0
  else
    if (val.eq.io%head%dri%bad) val = -1.0
  endif
  io%data1 = val  ! Dummy data (0 weighted)
  io%dataw = 0.0  ! No data available yet in the sum
  !
  if (aver%done%resample) then
    ! Prepare 'obs_resampled' to store the resampled input spectra
    call init_obs(obs_resampled)
    obs_resampled%head = io%head  ! We need a decent header for class_assoc_add
                                  ! to work properly
    call prepare_obs(obs_resampled,aver%done%axis%nchan,assoc,error)
    if (error)  return
  endif
  !
contains
  subroutine prepare_obs(obs,nchan,ref,error)
    type(observation),   intent(inout) :: obs
    integer(kind=4),     intent(in)    :: nchan
    type(class_assoc_t), intent(in)    :: ref  ! Reference section
    logical,             intent(inout) :: error
    ! Local
    integer(kind=4) :: iarray
    real(kind=4), pointer :: r4(:)
    character(len=12) :: aaname
    !
    call reallocate_obs(obs,nchan,error)
    if (error)  return
    !
    if (doassoc) then
      ! Also prepare an Associated Arrays section. All arrays are set as R*4
      ! because the running sum does not work with integers.
      ! Do not name them as the original (some of these names may be reserved
      ! and can not be R*4). Prefix with a T (temporary) so that we can make
      ! basic consistency checks afterwards.
      do iarray=1,ref%n
        ! Skip array 'W'. This is the weight array, non-sense averaging it!
        if (ref%array(iarray)%name.eq.'W')  cycle
        !
        write(aaname,'(A,A)')  'T',ref%array(iarray)%name(1:11)
        call class_assoc_add(obs,aaname,'',fmt_r4,0,-1000.0,r4,error)
        if (error)  return
        r4(:) = 0.0   ! No NaN nor bad
      enddo
      obs%head%presec(class_sec_assoc_id) = obs%assoc%n.gt.0
    endif
    !
  end subroutine prepare_obs
  !
end subroutine sumlin_data_prepro
!
subroutine sumlin_wadd_new(set,obs,aver,sumio,error)
  use gildas_def
  use gbl_message
  use classcore_interfaces, except_this=>sumlin_wadd_new
  use class_averaging
  use class_types
  use sumlin_mod_second
  !---------------------------------------------------------------------
  ! @ private
  !   Weighted addition. Average 'obs' and 'sumin' observations
  ! together into an output observation. 'sumin' can not be handled as
  ! a simple observation since its weight (which may be different for
  ! each channel, because of the history of all previous observations
  ! accumulated) must not be touched. On the contrary a single weight
  ! value for all channels is computed for 'obs'.
  !   This routine is totally generic for both COMPOSITE and INTERSECT
  ! cases. The only difference is the range of X values for ouput sum,
  ! but it is handled in subroutine 'sumout_param'.
  !   Alignment is performed with respect to:
  !   signal frequency if set%align = 'F'
  !   velocity         if set%align = 'V'
  !   channel number   if set%align = 'C' (default)
  ! Called by ACCUMULATE and AVERAGE.
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(observation),   intent(inout) :: obs    ! Spectrum to add. Inout because obs%dataw is set here
  type(average_t),     intent(in)    :: aver   !
  type(observation),   intent(inout) :: sumio  ! I/O sum
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  real(kind=4) :: rbad,sbad,weight,badr4
  integer(kind=4) :: snchan,schanmin,schanmax
  logical :: contaminate
  type(class_assoc_sub_t) :: warray
  !
  ! Set the weight for the input observation. It is a single value for an
  ! input spectrum. Header values already checked.
  select case (aver%done%weight)
  case (weight_sigma)  ! Sigma weighting
    call obs_weight_sigma(aver%do%rname,obs,weight,error)
    if (error)  return
    obs%dataw(:) = weight
    !
  case (weight_time)  ! Time weighting
    call obs_weight_time(aver%do%rname,obs,weight,error)
    if (error)  return
    obs%dataw(:) = weight
    !
  case (weight_equal)  ! Equal weighting
      obs%dataw(:) = 1.
    !
  case (weight_assoc)  ! Take from 'W' Associated Array
    if (class_assoc_exists(obs,'W',warray)) then
      call copy_assoc_sub_aator4(aver%do%rname,warray,obs%dataw,badr4,error)
    else
      ! Already checked in sumlin_header_check
      call class_message(seve%e,aver%do%rname,  &
        'No Associated Array ''W'' while SET WEIGHT is ASSOC')
      error = .true.
      return
    endif
    !
  end select
  !
  contaminate = set%bad.eq.'O'  ! Bad channels contaminate output or not?
  !
  if (aver%done%resample)  then
    ! Remark: in theory 'bad' value can be different from an input spectrum
    ! to another, and addition routine is able to deal with this. In other
    ! words, you cannot factorize the lines below.
    if (aver%done%kind_is_spec) then
      obs_resampled%head%spe%nchan = sumio%head%spe%nchan
      obs_resampled%head%spe%bad   = obs%head%spe%bad
      obs_resampled%head%spe%restf = sumio%head%spe%restf
      obs_resampled%head%spe%image = sumio%head%spe%image
      rbad = obs_resampled%head%spe%bad
      sbad = sumio%head%spe%bad
    else
      obs_resampled%head%dri%npoin = sumio%head%dri%npoin
      obs_resampled%head%dri%bad   = obs%head%dri%bad
      rbad = obs_resampled%head%dri%bad
      sbad = sumio%head%dri%bad
    endif
    call sumlin_resample(set,aver,obs,obs_resampled,schanmin,schanmax,error)
    if (error)  return
    !
    !
    ! First Associated Arrays (because sumio%dataw will be modified when
    ! dealing with RY)
    call waverage_assoc(aver%do%rname,  &
                        obs_resampled%assoc,obs_resampled%dataw,  &
                        sumio%assoc,sumio%dataw,  &
                        schanmin,schanmax,contaminate,error)
    if (error)  return
    !
    call simple_waverage(obs_resampled%data1,obs_resampled%dataw,rbad,  &
                         sumio%data1,sumio%dataw,sbad,                  &
                         schanmin,schanmax,contaminate,.true.)
    !
  else
    ! No resampling needed, only simple weighted average
    !
    if (aver%done%kind_is_spec) then
      rbad = obs%head%spe%bad
      sbad = sumio%head%spe%bad
      snchan = sumio%head%spe%nchan
    else
      rbad = obs%head%dri%bad
      sbad = sumio%head%dri%bad
      snchan = sumio%head%dri%npoin
    endif
    !
    ! First Associated Arrays (because sumio%dataw will be modified when
    ! dealing with RY)
    call waverage_assoc(aver%do%rname,  &
                        obs%assoc,obs%dataw,  &
                        sumio%assoc,sumio%dataw,  &
                        1,snchan,contaminate,error)
    if (error)  return
    !
    ! Then RY + update sumio%dataw
    call simple_waverage(obs%data1,obs%dataw,rbad,  &
                         sumio%data1,sumio%dataw,sbad,  &
                         1,snchan,contaminate,.true.)
  endif
  !
end subroutine sumlin_wadd_new
!
subroutine simple_waverage(rdata1,rdataw,rbad,  &
                           sdata1,sdataw,sbad,  &
                           schanmin,schanmax,contaminate,wup)
  !---------------------------------------------------------------------
  ! @ public
  ! Simple summation in the case SET ALIGN is CHANNEL. For efficiency
  ! purpose, addition is performed only in a given range were we know
  ! the input data array contributes.
  ! Weighting :
  ! * All channels are assumed to be aligned
  ! * Input weights should have been computed according to SET WEIGHT.
  !   Output weights (possibly non-uniform) are computed with special
  !   care.
  ! * Blanked channels propagates or not in output sum according to
  !   input parameter 'contaminate'
  !---------------------------------------------------------------------
  real(kind=4),    intent(in)    :: rdata1(:)    ! Data to be added
  real(kind=4),    intent(in)    :: rdataw(:)    ! Weigth of data to be added
  real(kind=4),    intent(in)    :: rbad         ! Bad value for the data to be added
  real(kind=4),    intent(inout) :: sdata1(:)    ! Current sum
  real(kind=4),    intent(inout) :: sdataw(:)    ! Weight of current sum
  real(kind=4),    intent(in)    :: sbad         ! Bad value for the current sum
  integer(kind=4), intent(in)    :: schanmin     ! First channel
  integer(kind=4), intent(in)    :: schanmax     ! Last channel
  logical,         intent(in)    :: contaminate  ! Bad channels contaminate output or not?
  logical,         intent(in)    :: wup          ! Update weight in return?
  ! Local
  integer(kind=4) :: is
  real(kind=4) :: vr,wr,vt,wt
  !
  ! Quick addition
  do is = schanmin,schanmax
     !
     ! Compute value and weight for input obs
     if (rdata1(is).ne.rbad) then
        ! Channel of new obs is not bad
        vr = rdata1(is)*rdataw(is)
        wr =            rdataw(is)
     elseif (contaminate) then
        ! Channel of new obs is bad and contamination is enabled => sum is bad
        sdata1(is) = sbad
        if (wup) sdataw(is) = 0.0
        cycle  ! Next channel
     else
        ! Channel of new obs is bad and no contamination => zero-weight this channel
        vr = 0.
        wr = 0.
     endif
     !
     ! Compute value and weight for old sum
     if (sdata1(is).ne.sbad) then
        ! Channel of old sum is not bad
        vt = sdata1(is)*sdataw(is)
        wt =            sdataw(is)
     elseif (contaminate) then
        ! Channel of old sum is bad and contamination is enabled => sum is bad
        ! sdata1(is) = sbad  (well, it is already bad)
        if (wup) sdataw(is) = 0.0
        cycle  ! Next channel
     else
        ! Channel of old sum is bad and no contamination => zero-weight this channel
        vt = 0.
        wt = 0.
     endif
     !
     ! Store new sum
     if (wr.ne.0.0 .or. wt.ne.0.0) then
        sdata1(is) = (vr+vt)/(wr+wt)
        if (wup) sdataw(is) = wr+wt
     else
        ! Data missing (do *not* set value as 'bad' here!)
        if (wup) sdataw(is) = 0.0
     endif
  enddo
  !
end subroutine simple_waverage
!
subroutine sumlin_wrms(set,obs,aver,sumio,rmsio,error)
  use gildas_def
  use gbl_message
  use classcore_interfaces, except_this=>sumlin_wrms
  use class_averaging
  use class_types
  use sumlin_mod_second
  !---------------------------------------------------------------------
  ! @ private
  !   Weighted rms.
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(observation),   intent(in)    :: obs    ! Spectrum to add
  type(average_t),     intent(in)    :: aver   !
  type(observation),   intent(in)    :: sumio  ! The previous sum
  type(observation),   intent(inout) :: rmsio  ! The running RMS
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  real(kind=4) :: obad,abad,rbad
  integer(kind=4) :: snchan,schanmin,schanmax
  logical :: contaminate
  !
  ! In the context of RMS, dataw is a counter of the number of values per
  ! channel. The new spectrum to be added counts for 1
  obs%dataw(:) = 1.0
  !
  contaminate = set%bad.eq.'O'  ! Bad channels contaminate output or not?
  !
  if (aver%done%resample)  then
    ! Remark: in theory 'bad' value can be different from an input spectrum
    ! to another, and addition routine is able to deal with this. In other
    ! words, you cannot factorize the lines below.
    if (aver%done%kind_is_spec) then
      obs_resampled%head%spe%nchan = sumio%head%spe%nchan
      obs_resampled%head%spe%bad   = obs%head%spe%bad
      obs_resampled%head%spe%restf = sumio%head%spe%restf
      obs_resampled%head%spe%image = sumio%head%spe%image
      obad = obs_resampled%head%spe%bad
      abad = sumio%head%spe%bad
      rbad = rmsio%head%spe%bad
    else
      obs_resampled%head%dri%npoin = sumio%head%dri%npoin
      obs_resampled%head%dri%bad   = obs%head%dri%bad
      obad = obs_resampled%head%dri%bad
      abad = sumio%head%dri%bad
      rbad = rmsio%head%dri%bad
    endif
    call sumlin_resample(set,aver,obs,obs_resampled,schanmin,schanmax,error)
    if (error)  return
    !
    ! First Associated Arrays (because rms%dataw will be modified when
    ! dealing with RY)
    call wrms_assoc(aver%do%rname,            &
                    obs_resampled%assoc,obs_resampled%dataw,  &
                    sumio%assoc,sumio%dataw,  &
                    rmsio%assoc,rmsio%dataw,  &
                    schanmin,schanmax,contaminate,error)
    if (error)  return
    !
    call simple_wrms(obs_resampled%data1,obs_resampled%dataw,obad,  &
                     sumio%data1,sumio%dataw,abad,  &
                     rmsio%data1,rmsio%dataw,rbad,  &
                     schanmin,schanmax,contaminate,.true.)
  else
    ! No resampling needed, only simple weighted RMS
    !
    if (aver%done%kind_is_spec) then
      obad = obs%head%spe%bad
      abad = sumio%head%spe%bad
      rbad = rmsio%head%spe%bad
      snchan = sumio%head%spe%nchan
    else
      obad = obs%head%dri%bad
      abad = sumio%head%dri%bad
      rbad = rmsio%head%dri%bad
      snchan = sumio%head%dri%npoin
    endif
    !
    ! First Associated Arrays (because rms%dataw will be modified when
    ! dealing with RY)
    call wrms_assoc(aver%do%rname,            &
                    obs%assoc,obs%dataw,      &
                    sumio%assoc,sumio%dataw,  &
                    rmsio%assoc,rmsio%dataw,  &
                    1,snchan,contaminate,error)
    if (error)  return
    !
    call simple_wrms(obs%data1,  obs%dataw,  obad,  &
                     sumio%data1,sumio%dataw,abad,  &
                     rmsio%data1,rmsio%dataw,rbad,  &
                     1,snchan,contaminate,.true.)
  endif
  !
end subroutine sumlin_wrms
!
subroutine simple_wrms(odata1,odataw,obad,  &
                       adata1,adataw,abad,  &
                       rdata1,rdataw,rbad,  &
                       schanmin,schanmax,contaminate,wup)
  !---------------------------------------------------------------------
  ! @ private
  !  RMS - RMS - RMS - RMS - RMS - RMS - RMS - RMS - RMS - RMS - RMS
  !---------------------------------------------------------------------
  real(kind=4),    intent(in)    :: odata1(:)    ! Data to be RMS'd
  real(kind=4),    intent(in)    :: odataw(:)    ! Weight of data to be RMS'd
  real(kind=4),    intent(in)    :: obad         ! Bad channel value for input data
  real(kind=4),    intent(in)    :: adata1(:)    ! Average
  real(kind=4),    intent(in)    :: adataw(:)    ! Average weights (unused)
  real(kind=4),    intent(in)    :: abad         ! Bad channel value for average
  real(kind=4),    intent(inout) :: rdata1(:)    ! The RMS
  real(kind=4),    intent(inout) :: rdataw(:)    ! Weight of RMS
  real(kind=4),    intent(in)    :: rbad         ! Bad channel value for RMS
  integer(kind=4), intent(in)    :: schanmin     ! First channel
  integer(kind=4), intent(in)    :: schanmax     ! Last channel
  logical,         intent(in)    :: contaminate  ! Bad channels contaminate output or not?
  logical,         intent(in)    :: wup          ! Update weight in return?
  ! Local
  integer(kind=4) :: is
  real(kind=4) :: vr,wr,aver
  !
  ! Quick addition
  do is = schanmin,schanmax
    !
    ! Computation can be done only if the average exists
    aver = adata1(is)
    vr = odata1(is)
    if (aver.eq.abad) then
      wr = 0.0
    elseif (vr.eq.obad) then
      wr = 0.0
    else
      wr = odataw(is)
    endif
    !
    if (wr.ne.0.0) then
      rdata1(is) = rdata1(is) + (vr-aver)*(vr-aver)
      if (wup) rdataw(is) = rdataw(is) + wr
    elseif (contaminate) then
      ! Channel of new obs is bad and contamination is enabled => rms is bad
      rdata1(is) = rbad
      if (wup) rdataw(is) = 0.0
    else
      ! Channel of new obs is bad and no contamination => Nothing done
      continue
    endif
  enddo
  !
end subroutine simple_wrms
!
subroutine sumlin_resample(set,aver,obsin,obsout,ismin,ismax,error)
  use gildas_def
  use gbl_constant
  use classcore_interfaces, except_this=>sumlin_resample
  use class_averaging
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set     !
  type(average_t),     intent(in)    :: aver    !
  type(observation),   intent(in)    :: obsin   ! Input observation
  type(observation),   intent(inout) :: obsout  ! Resampled observation
  integer(kind=4),     intent(out)   :: ismin   !
  integer(kind=4),     intent(out)   :: ismax   !
  logical,             intent(inout) :: error   ! Logical error flag
  ! Local
  real(kind=8) :: dfreq
  real(kind=4) :: rbad,sbad
  type(resampling) :: raxis
  !
  raxis%ref = 0.d0  ! Define axis from 0th channel
  select case (aver%done%align)
  case (align_freq)
    raxis%nchan = obsin%head%spe%nchan
    raxis%inc = obsin%head%spe%fres/(1.d0+obsin%head%spe%doppler)
    ! => This looks like a MODIFY FREQUENCY! (SB, 28-oct-2011)
    dfreq = obsin%head%spe%restf - obsout%head%spe%restf
    raxis%val = (-obsin%head%spe%rchan)*raxis%inc + dfreq
  case (align_imag)
    raxis%nchan = obsin%head%spe%nchan
    raxis%inc = -obsin%head%spe%fres/(1.d0+obsin%head%spe%doppler)
    ! => This looks like a MODIFY FREQUENCY! (SB, 28-oct-2011)
    dfreq = obsin%head%spe%image - obsout%head%spe%image
    raxis%val = (-obsin%head%spe%rchan)*raxis%inc + dfreq
  case (align_velo)
    raxis%nchan = obsin%head%spe%nchan
    raxis%inc = obsin%head%spe%vres
    call abscissa_chan2velo(obsin%head,raxis%ref,raxis%val)
  case (align_posi)
    raxis%nchan = obsin%head%dri%npoin
    raxis%inc = dble(obsin%head%dri%ares)
    call abscissa_chan2angl(obsin%head,raxis%ref,raxis%val)
  end select
  !
  ! Associated Arrays first
  call resample_interpolate_regul_assoc(set,aver%do%rname,  &
                                        obsin%assoc,obsin%dataw,raxis,  &
                                        obsout%assoc,obsout%dataw,aver%done%axis,error)
  if (error)  return
  !
  ! Then RY
  if (obsin%head%gen%kind.eq.kind_spec) then
    rbad = obsin%head%spe%bad
    sbad = obsout%head%spe%bad
  else
    rbad = obsin%head%dri%bad
    sbad = obsout%head%dri%bad
  endif
  !
  call resample_interpolate_regul(set,  &
                                  obsin%data1, obsin%dataw, rbad,raxis,  &
                                  obsout%data1,obsout%dataw,sbad,aver%done%axis,  &
                                  ismin,ismax,error)
  if (error)  return
  !
end subroutine sumlin_resample
!
subroutine sumlin_data_postpro_waverage(aver,sumio,error)
  use gbl_constant
  use classcore_interfaces, except_this=>sumlin_data_postpro_waverage
  use class_averaging
  use class_types
  use sumlin_mod_second
  !---------------------------------------------------------------------
  ! @ private
  !  Data post-preprocessing for weighted average
  !---------------------------------------------------------------------
  type(average_t),   intent(in)    :: aver   !
  type(observation), intent(inout) :: sumio  !
  logical,           intent(inout) :: error  ! Logical error flag
  ! Local
  real(kind=4) :: bad
  real(kind=4), pointer :: dataw(:)
  integer(kind=4) :: iarray
  !
  bad = obs_bad(sumio%head)
  where (sumio%dataw.eq.0.0)
    ! Some data missing in all spectra -> blank these channels
    sumio%data1 = bad
  end where
  !
  ! Same patch for Associated Arrays
  do iarray=1,sumio%assoc%n
    where (sumio%dataw.eq.0.0)
      sumio%assoc%array(iarray)%r4(:,1) = sumio%assoc%array(iarray)%badr4
    end where
  enddo
  !
  if (aver%done%resample)  call free_obs(obs_resampled)
  !
  ! Associate the array 'W' if SET WEIGHT is ASSOC
  if (aver%done%weight.eq.weight_assoc) then
    call class_assoc_add(sumio,'W',dataw,error)
    if (error)  return
    dataw(:) = sumio%dataw(:)
  endif
  !
end subroutine sumlin_data_postpro_waverage
!
subroutine sumlin_data_postpro_wrms(aver,rmsio,error)
  use gbl_constant
  use classcore_interfaces, except_this=>sumlin_data_postpro_wrms
  use class_averaging
  use class_types
  use sumlin_mod_second
  !---------------------------------------------------------------------
  ! @ private
  !  Data post-preprocessing for weighted rms
  !---------------------------------------------------------------------
  type(average_t),   intent(in)    :: aver
  type(observation), intent(inout) :: rmsio
  logical,           intent(inout) :: error
  ! Local
  integer(kind=4) :: iarray
  real(kind=4) :: bad
  !
  bad = obs_bad(rmsio%head)
  where (rmsio%dataw.eq.0.0)
    ! Some data missing in all spectra -> blank these channels
    rmsio%data1 = bad
  elsewhere (rmsio%dataw.eq.1.0)
    ! Only 1 value is this channel -> RMS can not be computed
    rmsio%data1 = 0.0
  elsewhere
    rmsio%data1 = sqrt(rmsio%data1/(rmsio%dataw-1.))
  end where
  !
  ! Same for Associated Arrays (by construction they are all R*4)
  do iarray=1,rmsio%assoc%n
    where (rmsio%dataw.eq.0.0)
      ! 0 weight => no data available in RY
      rmsio%assoc%array(iarray)%r4(:,1) = rmsio%assoc%array(iarray)%badr4
    elsewhere (rmsio%dataw.eq.1.0)
      ! weight 1 => only 1 value => RMS can not be computed
      rmsio%assoc%array(iarray)%r4(:,1) = 0.0
    elsewhere (rmsio%assoc%array(iarray)%r4(:,1).ne.rmsio%assoc%array(iarray)%badr4)
      ! Non bad value => compute RMS
      rmsio%assoc%array(iarray)%r4(:,1) = sqrt(rmsio%assoc%array(iarray)%r4(:,1)/(rmsio%dataw-1.))
  ! elsewhere
  !   Bad value => leave as bad
    end where
  enddo
  !
  if (aver%done%resample)  call free_obs(obs_resampled)
  !
end subroutine sumlin_data_postpro_wrms
