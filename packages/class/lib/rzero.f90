function sas_function(action)
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  logical :: sas_function                 ! intent(out)
  character(len=*), intent(in) :: action  !
  ! Too much verbose when working with thousands of spectra...
  ! print *,'User function ',action
  sas_function = .false.
end function sas_function
!
subroutine init_obs(obs)
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  ! Initialize an observation
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  !
  ! Initialize pointers
  nullify(obs%datax,obs%datas,obs%datai,obs%datav,obs%data1,obs%dataw)
  nullify(obs%data2,obs%dap)
  !
  obs%is_R = .false.  ! This is not the R buffer (by default)
  obs%head%xnum = 0   ! Not yet in memory
  !
  ! Not OTF by default
  obs%head%des%ndump = 1
  obs%head%presec(class_sec_desc_id) = .false.
  obs%is_otf = .false.
  !
  ! No user section
  obs%user%n = 0
  !
  ! No associated array
  obs%assoc%n = 0
end subroutine init_obs
!
subroutine free_obs(obs)
  use classcore_interfaces, except_this=>free_obs
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  ! Free an Observation
  ! Must not touch the Header, which may have been previously defined
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  ! Local
  integer(kind=4) :: iuser
  logical :: error
  !
  error = .false.
  !
  if (associated(obs%datax)) deallocate(obs%datax)
  if (associated(obs%datas)) deallocate(obs%datas)
  if (associated(obs%datai)) deallocate(obs%datai)
  if (associated(obs%datav)) deallocate(obs%datav)
  if (associated(obs%data1)) deallocate(obs%data1)
  if (associated(obs%dataw)) deallocate(obs%dataw)
  ! User Section
  obs%user%n = 0
  if (allocated(obs%user%sub)) then
    do iuser=1,size(obs%user%sub)
      if (associated(obs%user%sub(iuser)%data))  &
        deallocate(obs%user%sub(iuser)%data)
    enddo
    deallocate(obs%user%sub)
  endif
  ! Associated arrays
  call deallocate_assoc(obs%assoc,error)
  ! OTF data if any
  if (associated(obs%data2)) deallocate(obs%data2)
  if (associated(obs%dap)) deallocate(obs%dap)
end subroutine free_obs
!
subroutine rzero(obs,code,user_function)
  use gbl_constant
  use gbl_message
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  ! Reset an observation to default values
  !----------------------------------------------------------------------
  type(observation), intent(inout) :: obs            !
  character(len=*),  intent(in)    :: code           ! KEEP or FREE or NULL
  logical,           external      :: user_function  !
  ! Local
  logical :: notok
  !
  ! General
  obs%head%gen%num     = 0
  obs%head%gen%ver     = 0
  obs%head%gen%dobs    = -32768
  obs%head%gen%dred    = -32768
  obs%head%gen%kind    = kind_spec
  obs%head%gen%qual    = qual_unknown
  obs%head%gen%subscan = 0
  obs%head%gen%scan    = 0
  obs%head%gen%ut      = 0.d0
  obs%head%gen%st      = 0.d0
  obs%head%gen%az      = 0.
  obs%head%gen%el      = 0.
  obs%head%gen%tau     = 0.
  obs%head%gen%tsys    = 0.
  obs%head%gen%time    = 0.
  obs%head%gen%parang  = parang_null
  obs%head%gen%yunit   = yunit_unknown
  obs%head%gen%xunit   = 0
  !
  ! Resolution
  obs%head%res%major  = 0.
  obs%head%res%minor  = 0.
  obs%head%res%posang = 0.
  !
  ! Position
  obs%head%pos%sourc   = ' '
  obs%head%pos%system  = type_un
  obs%head%pos%equinox = 0.
  obs%head%pos%proj    = p_none
  obs%head%pos%lam     = 0.d0
  obs%head%pos%bet     = 0.d0
  obs%head%pos%projang = 0.d0
  obs%head%pos%lamof   = 0.
  obs%head%pos%betof   = 0.
  !
  ! Base
  obs%head%bas%deg      = 0
  obs%head%bas%sigfi    = 0.
  obs%head%bas%aire     = 0.
  obs%head%bas%nwind    = 0
  obs%head%bas%w1(:)    = 0.
  obs%head%bas%w2(:)    = 0.
  obs%head%bas%sinus(:) = 0.
  !
  ! Gauss fit
  obs%head%gau%nline   = 0
  obs%head%gau%sigba   = 0.
  obs%head%gau%sigra   = 0.
  obs%head%gau%nfit(:) = 0.
  obs%head%gau%nerr(:) = 0.
  !
  ! Shell fit
  obs%head%she%nline   = 0
  obs%head%she%sigba   = 0.
  obs%head%she%sigra   = 0.
  obs%head%she%nfit(:) = 0.
  obs%head%she%nerr(:) = 0.
  !
  ! NH3/HFS fit
  obs%head%hfs%nline   = 0
  obs%head%hfs%sigba   = 0.
  obs%head%hfs%sigra   = 0.
  obs%head%hfs%nfit(:) = 0.
  obs%head%hfs%nerr(:) = 0.
  !
  ! Absorption fit
  obs%head%abs%nline   = 0
  obs%head%abs%sigba   = 0.
  obs%head%abs%sigra   = 0.
  obs%head%abs%nfit(:) = 0.
  obs%head%abs%nerr(:) = 0.
  !
  ! Pointing fit
  obs%head%poi%nline   = 0
  obs%head%poi%sigba   = 0.
  obs%head%poi%sigra   = 0.
  obs%head%poi%nfit(:) = 0.
  obs%head%poi%nerr(:) = 0.
  !
  ! Spectro
  obs%head%spe%nchan   = 0
  obs%head%spe%restf   = 0.d0
  obs%head%spe%image   = 0.d0
  obs%head%spe%doppler = -1.d0
  obs%head%spe%rchan   = 0.d0
  obs%head%spe%voff    = 0.d0
  obs%head%spe%bad     = class_bad
  obs%head%spe%vtype   = vel_unk
  obs%head%spe%vconv   = vconv_unk
  obs%head%spe%vdire   = vdire_unk
  obs%head%spe%line    = ' '
  ! Even though the following ones can be divider, they must be set to 0
  ! because a value of 1 can happen
  obs%head%spe%vres = 0.d0
  obs%head%spe%fres = 0.d0
  !
  ! Calibration
  obs%head%cal%beeff    = 0.
  obs%head%cal%foeff    = 0.
  obs%head%cal%gaini    = 0.
  obs%head%cal%h2omm    = 0.
  obs%head%cal%pamb     = 0.
  obs%head%cal%tamb     = 0.
  obs%head%cal%tatms    = 0.
  obs%head%cal%tchop    = 0.
  obs%head%cal%tcold    = 0.
  obs%head%cal%taus     = 0.
  obs%head%cal%taui     = 0.
  obs%head%cal%tatmi    = 0.
  obs%head%cal%trec     = 0.
  obs%head%cal%cmode    = 0
  obs%head%cal%atfac    = 0.
  obs%head%cal%alti     = 0.
  obs%head%cal%count(:) = 0.
  obs%head%cal%lcalof   = 0.
  obs%head%cal%bcalof   = 0.
  obs%head%cal%geolong  = 0.d0
  obs%head%cal%geolat   = 0.d0
  !
  ! Misc
  obs%head%plo%amin=0
  obs%head%plo%amax=0
  obs%head%plo%vmin=0
  obs%head%plo%vmax=0
  obs%head%his%nseq=0
  obs%head%dri%npoin=0
  !
  ! Set some of the ones likely to be used as a divider to a non zero value
  obs%head%dri%ares  = 1.
  obs%head%dri%tres  = 1.
  obs%head%dri%width = 1.
  !
  notok = user_function(code)
  if (notok) then
     call class_message(seve%w,'GET','Error freeing user sections')
  endif
  !
  ! OTF data if any
  if (associated(obs%data2)) deallocate(obs%data2)
  if (associated(obs%dap)) deallocate(obs%dap)
  !
  ! User Section
  call rzero_user(obs)
  !
  ! Associated array section
  call rzero_assoc(obs)
  !
end subroutine rzero
!
subroutine class_obs_init(obs,error)
  use classcore_interfaces, except_this=>class_obs_init
  use class_types
  !---------------------------------------------------------------------
  ! @ public (available to external programs)
  !  Initialize the input observation for later use
  !  See demo program classdemo-telwrite.f90
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs    !
  logical,           intent(inout) :: error  !
  !
  call init_obs(obs)
  !
end subroutine class_obs_init
!
subroutine class_obs_reset(obs,ndata,error)
  use classcore_interfaces, except_this=>class_obs_reset
  use class_types
  !---------------------------------------------------------------------
  ! @ public (available to external programs)
  !  Zero-ify and resize the input observation for later use
  !  See demo program classdemo-telwrite.f90
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs    !
  integer(kind=4),   intent(in)    :: ndata  ! Number of values the observation can accept in return
  logical,           intent(inout) :: error  ! Logical error flag
  !
  call rzero(obs,'FREE',sas_function)
  ! NB: no way to use a custom 'user_function', use class dummy one instead
  !
  ! Disable all sections by default. Should be merged in rzero? => probably much
  ! work to avoid breaking everything...
  obs%head%presec(:) = .false.
  !
  call reallocate_obs(obs,ndata,error)
  !
end subroutine class_obs_reset
!
subroutine class_obs_clean(obs,error)
  use classcore_interfaces, except_this=>class_obs_clean
  use class_types
  !---------------------------------------------------------------------
  ! @ public (available to external programs)
  !  Clean/free the input observation before deleting it
  !  See demo program classdemo-telwrite.f90
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs    !
  logical,           intent(inout) :: error  !
  !
  call free_obs(obs)
  !
end subroutine class_obs_clean
!
function obs_nchan(head)
  use gbl_constant
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  !  Return the 'nchan' value of the observation, taking care of the
  ! observation kind
  !---------------------------------------------------------------------
  integer(kind=4) :: obs_nchan  ! Function value on return
  type(header), intent(in) :: head   ! Current observation header
  !
  if (head%gen%kind.eq.kind_spec) then
     obs_nchan = head%spe%nchan
  else if (head%gen%kind.eq.kind_cont) then
     obs_nchan = head%dri%npoin
  else
     obs_nchan = 0
  endif
end function obs_nchan
!
function obs_bad(head)
  use gbl_constant
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  !  Return the 'bad' value of the observation, taking care of the
  ! observation kind
  !---------------------------------------------------------------------
  real(kind=4) :: obs_bad  !
  type(header), intent(in) :: head  ! Current observation header
  !
  if (head%gen%kind.eq.kind_spec) then
    obs_bad = head%spe%bad
  else if (head%gen%kind.eq.kind_cont) then
    obs_bad = head%dri%bad
  else
    obs_bad = class_bad
  endif
end function obs_bad
!
function obs_good_obs(obs,ichan)
  use classcore_interfaces, except_this=>obs_good_obs
  use class_types
  !-------------------------------------------------------------------
  ! @ public-generic obs_good
  ! Return a "good" (i.e. non blanked) value for the specified channel
  ! ---
  !  Obs version
  !-------------------------------------------------------------------
  real(kind=4) :: obs_good_obs  ! Function value on return
  type(observation), intent(in) :: obs
  integer(kind=4),   intent(in) :: ichan
  !
  if (obs%spectre(ichan).ne.obs%cbad) then
    obs_good_obs = obs%spectre(ichan)
  else
    obs_good_obs = obs_fillin(obs%spectre,ichan,obs%cimin,obs%cimax,obs%cbad)
  endif
end function obs_good_obs
!
function obs_good_r4(datay,bad,imin,imax,ichan)
  use classcore_interfaces, except_this=>obs_good_r4
  !-------------------------------------------------------------------
  ! @ public-generic obs_good
  ! Return a "good" (i.e. non blanked) value for the specified channel
  ! ---
  ! R*4 version
  !-------------------------------------------------------------------
  real(kind=4) :: obs_good_r4  ! Function value on return
  real(kind=4),    intent(in) :: datay(:)
  real(kind=4),    intent(in) :: bad
  integer(kind=4), intent(in) :: imin
  integer(kind=4), intent(in) :: imax
  integer(kind=4), intent(in) :: ichan
  !
  if (datay(ichan).ne.bad) then
    obs_good_r4 = datay(ichan)
  else
    obs_good_r4 = obs_fillin(datay,ichan,imin,imax,bad)
  endif
end function obs_good_r4
!
function obs_firstgood(data,nc,bad)
  !---------------------------------------------------------------------
  ! @ private
  !  Return the first non-bad channel
  !---------------------------------------------------------------------
  integer(kind=4) :: obs_firstgood  ! Function value on return
  real(kind=4),    intent(in) :: data(*)  ! Data array
  integer(kind=4), intent(in) :: nc       ! Size of data
  real(kind=4),    intent(in) :: bad      ! Bad value
  ! Local
  integer(kind=4) :: ic
  !
  obs_firstgood = nc
  do ic=1,nc
    if (data(ic).ne.bad) then
      obs_firstgood = ic
      exit
    endif
  enddo
  !
end function obs_firstgood
!
function obs_lastgood(data,nc,bad)
  !---------------------------------------------------------------------
  ! @ private
  !  Return the last non-bad channel
  !---------------------------------------------------------------------
  integer(kind=4) :: obs_lastgood  ! Function value on return
  real(kind=4),    intent(in) :: data(*)  ! Data array
  integer(kind=4), intent(in) :: nc       ! Size of data
  real(kind=4),    intent(in) :: bad      ! Bad value
  ! Local
  integer(kind=4) :: ic
  !
  obs_lastgood = 1
  do ic=nc,1,-1
    if (data(ic).ne.bad) then
      obs_lastgood = ic
      exit
    endif
  enddo
  !
end function obs_lastgood
!
function obs_fillin(r,ival,imin,imax,bad)
  !---------------------------------------------------------------------
  ! @ private
  !  Interpolate bad channel (if possible)
  !---------------------------------------------------------------------
  real(kind=4) :: obs_fillin  ! Function value on return
  integer(kind=4), intent(in) :: ival    ! Channel number
  integer(kind=4), intent(in) :: imin    ! First Channel number
  integer(kind=4), intent(in) :: imax    ! Last Channel number
  real(kind=4),    intent(in) :: r(imax) !
  real(kind=4),    intent(in) :: bad     !
  ! Local
  integer(kind=4) :: i,ifi,ila
  !
  do i=ival-1,imin,-1
    if (r(i).ne.bad) goto 10
  enddo
  !  All channels before 'ival' are bad
  do i=ival+1,imax-1
    if (r(i).ne.bad) goto 10
  enddo
  !  All channels above 'ival' are bad
  !  All channels up to 'imax-1' are bad
  !  Use last channel value
  obs_fillin = r(imax)
  return
10 continue
  !  At least one good channel before or after
  ifi = i
  do i=max(ifi+1,ival+1),imax
    if (r(i).ne.bad) goto 20
  enddo
  !  Only one good channel, not 'imax'
  if (ifi.gt.ival .or. ifi.eq.imin) then
    obs_fillin = r(ifi)
    return
  endif
  do i=ifi-1,imin,-1
    if (r(i).ne.bad) goto 20
  enddo
  obs_fillin = r(ifi)
  return
  !  Two good channels
20 continue
  ila = i
  obs_fillin = ( (ival-ifi)*r(ila) + (ila-ival)*r(ifi) ) / float(ila-ifi)
  return
end function obs_fillin
!
function obs_system(itype)
  use gbl_constant
  !---------------------------------------------------------------------
  ! @ private
  !  Return the Type of Coordinates as a character*2 string
  !---------------------------------------------------------------------
  character(len=4) :: obs_system
  integer(kind=4), intent(in) :: itype
  !
  select case (itype)
  case (type_un)
    obs_system = 'Un'
  case (type_eq)
    obs_system = 'Eq'
  case (type_ga)
    obs_system = 'Ga'
  case (type_ho)
    obs_system = 'Ho'
  case (type_ic)
    obs_system = 'ICRS'
  case default
    obs_system = '??'
  end select
  !
end function obs_system
!
function obs_typev(itype)
  use gbl_constant
  !---------------------------------------------------------------------
  ! @ private
  !  Return the Type of Velocity as a character*4 string
  !---------------------------------------------------------------------
  character(len=4) :: obs_typev
  integer(kind=4), intent(in) :: itype
  !
  select case (itype)
  case (vel_unk)
    obs_typev = 'Unkn'
  case (vel_lsr)
    obs_typev = 'LSR '
  case (vel_hel)
    obs_typev = 'Hel.'
  case (vel_obs)
    obs_typev = 'Obs.'
  case (vel_ear)
    obs_typev = 'Ear.'
  case default
    ! Warning: 'titout' used to display 'Unkn' i.e. code vel_unk in case
    ! of value not understood
    obs_typev = 'Unkn'
  end select
  !
end function obs_typev
!
function obs_projection(iproj)
  use gbl_constant
  !---------------------------------------------------------------------
  ! @ private
  !  Return the Type of Projection as a character*4 string
  !---------------------------------------------------------------------
  character(len=4) :: obs_projection
  integer(kind=4), intent(in) :: iproj
  !
  select case (iproj)
  case (p_none)
    obs_projection = 'None'
  case (p_gnomonic)
    obs_projection = 'Gnom'
  case (p_ortho)
    obs_projection = 'Orth'
  case (p_azimuthal)
    obs_projection = 'Az. '
  case (p_stereo)
    obs_projection = 'Ster'
  case (p_lambert)
    obs_projection = 'Lamb'
  case (p_aitoff)
    obs_projection = 'Aito'
  case (p_radio)
    obs_projection = 'Rad.'
  case (p_sfl)
    obs_projection = 'SFL '
  case default
    obs_projection = '??  '
  end select
end function obs_projection
!
function obs_swmod(swmod)
  use gbl_constant
  !---------------------------------------------------------------------
  ! @ private
  !  Return the switching mode as a character*17 string
  !---------------------------------------------------------------------
  character(len=17) :: obs_swmod
  integer(kind=4), intent(in) :: swmod
  !
  select case (swmod)
  case (mod_freq)
    obs_swmod = 'unfolded FSW'
  case (mod_pos)
    obs_swmod = 'PSW'
  case (mod_fold)
    obs_swmod = 'folded FSW'
  case (mod_wob)
    obs_swmod = 'WSW'
  case (mod_mix)
    obs_swmod = 'mixed switching'
  case default
    obs_swmod = 'unknown switching'
  end select
  !
end function obs_swmod
!
function obs_yunit_tostr(code)
  use class_parameter
  !---------------------------------------------------------------------
  ! @ private-generic obs_yunit
  !  Convert a Y unit code to a human readable string
  !---------------------------------------------------------------------
  character(len=8) :: obs_yunit_tostr  ! Function value on return
  integer(kind=4), intent(in) :: code
  !
  select case (code)
  case (yunit_K_Tas,yunit_K_Tmb,yunit_Jyperbeam,yunit_mJyperbeam,  &
        yunit_Jypersr,yunit_mJypersr)
    obs_yunit_tostr = yunit_strings(code)
  case default
    obs_yunit_tostr = yunit_strings(yunit_unknown)
  end select
  !
end function obs_yunit_tostr
!
function obs_yunit_fromstr(string)
  use gbl_message
  use class_parameter
  !---------------------------------------------------------------------
  ! @ private-generic obs_yunit
  !  Convert a Y unit string to Class internal code
  !---------------------------------------------------------------------
  integer(kind=4) :: obs_yunit_fromstr  ! Function value on return
  character(len=*), intent(in) :: string
  ! Local
  character(len=*), parameter :: rname='YUNIT'
  !
  ! We can support several variants per case
  select case (string)
  case (yunit_strings(yunit_K_Tas))  ! ,'K(Ta*)','K [Ta*]')
    obs_yunit_fromstr = yunit_K_Tas
  case (yunit_strings(yunit_K_Tmb))
    obs_yunit_fromstr = yunit_K_Tmb
  case (yunit_strings(yunit_Jyperbeam))
    obs_yunit_fromstr = yunit_Jyperbeam
  case (yunit_strings(yunit_mJyperbeam))
    obs_yunit_fromstr = yunit_mJyperbeam
  case (yunit_strings(yunit_Jypersr))
    obs_yunit_fromstr = yunit_Jypersr
  case (yunit_strings(yunit_mJypersr))
    obs_yunit_fromstr = yunit_mJypersr
  case (yunit_strings(yunit_unknown))
    obs_yunit_fromstr = yunit_unknown
  case default
    ! Too verbose: it is the responsibility of the caller to warn user
    ! or not.
    ! call class_message(seve%w,rname,'String '//trim(string)//  &
    !  ' not understood, defaults to '//yunit_strings(yunit_unknown))
    obs_yunit_fromstr = yunit_unknown
  end select
  !
end function obs_yunit_fromstr
!
subroutine obs_weight_sigma(rname,obs,w,error,verbose)
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>obs_weight_sigma
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Compute the spectrum weight from sigma (baseline rms), with the
  ! appropriate sanity checks
  !  Note the 1e-6 extra factor which is added to match the [s.MHz/K^2]
  ! units of the time weight. obs%head%bas%sigfi is the RY RMS, i.e.
  ! same unit as RY (usually K), and 1 MHz is 1e6 s^-1.
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: rname    ! Calling routine name
  type(observation), intent(in)    :: obs      !
  real(kind=4),      intent(out)   :: w        ! [1-e6/K^2]
  logical,           intent(inout) :: error    !
  logical, optional, intent(in)    :: verbose  ! Default true
  ! Local
  character(len=message_length) :: mess
  character(len=4) :: string
  integer(kind=4) :: ns
  logical :: verbos
  !
  if (present(verbose)) then
    verbos = verbose
  else
    verbos = .true.
  endif
  !
  ! Sanity checks
  if (obs%head%bas%sigfi.le.0.) then  ! Trap negative SIGMA
    if (verbos)  call class_message(seve%e,rname,  &
      'R%HEAD%BAS%SIGFI must be greater than zero for SIGMA weighting')
    error = .true.
  endif
  if (error) then
    if (verbos)  call class_message(seve%e,rname,  &
      'Try TIME or EQUAL weighting instead')
    return
  endif
  !
  w = 1.e-6/obs%head%bas%sigfi**2
  !
  ! Post computing checks
  if (gag_isreal(w).ne.0) then
    call gag_infini(w,string,ns)
    if (verbos) then
      write(mess,'(3A)')  &
        'WEIGHT = 1/R%HEAD%BAS%SIGFI**2 overflows to ',string,' for SIGMA weighting'
      call class_message(seve%e,rname,mess)
    endif
    error = .true.
    return
  endif
  !
end subroutine obs_weight_sigma
!
subroutine obs_weight_time(rname,obs,w,error,verbose)
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>obs_weight_time
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Compute the spectrum weight from time, fres, and Tsys, with the
  ! appropriate sanity checks
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: rname    ! Calling routine name
  type(observation), intent(in)    :: obs      !
  real(kind=4),      intent(out)   :: w        ! [s.MHz/K^2]
  logical,           intent(inout) :: error    ! Logical error flag
  logical, optional, intent(in)    :: verbose  ! Default true
  ! Local
  character(len=message_length) :: mess
  character(len=4) :: string
  integer(kind=4) :: ns
  logical :: verbos
  !
  if (present(verbose)) then
    verbos = verbose
  else
    verbos = .true.
  endif
  !
  ! Sanity checks
  if (obs%head%gen%time.le.0.) then  ! Trap negative TIME
    if (verbos)  call class_message(seve%e,rname,  &
      'R%HEAD%GEN%TIME must be greater than zero for TIME weighting')
    error = .true.
  endif
  if (obs%head%gen%tsys.le.0.) then  ! Trap negative TSYS
    if (verbos)  call class_message(seve%e,rname,  &
      'R%HEAD%GEN%TSYS must be greater than zero for TIME weighting')
    error = .true.
  endif
  if (error) then
    if (verbos)  call class_message(seve%e,rname,  &
      'Try SIGMA or EQUAL weighting instead')
    return
  endif
  !
  if (obs%head%gen%kind.eq.kind_spec) then
    w = obs%head%gen%time * abs(obs%head%spe%fres)/obs%head%gen%tsys**2
    ! After folding, integration time is left unchanged (we do not want to
    ! change this to remain consistant with the actual scan duration) while
    ! noise has decreased by sqrt(2) => add a factor 2 on time to reflect
    ! this property.
    if (obs%head%swi%swmod.eq.mod_fold)  w = w*2
  else
    w = obs%head%gen%time * abs(obs%head%dri%width)/obs%head%gen%tsys**2
  endif
  !
  ! Post computing checks
  if (gag_isreal(w).ne.0) then
    call gag_infini(w,string,ns)
    if (verbos) then
      if (obs%head%gen%kind.eq.kind_spec) then
        write(mess,'(4A)')  &
          'WEIGHT = R%HEAD%GEN%TIME*|R%HEAD%SPE%FRES|/R%HEAD%GEN%TSYS**2',  &
          ' overflows to ',string,' for TIME weighting'
      else
        write(mess,'(4A)')  &
          'WEIGHT = R%HEAD%GEN%TIME*|R%HEAD%DRI%WIDTH|/R%HEAD%GEN%TSYS**2',  &
          ' overflows to ',string,' for TIME weighting'
      endif
      call class_message(seve%e,rname,mess)
    endif
    error = .true.
    return
  endif
  !
end subroutine obs_weight_time
!
subroutine obs_tsys_time(rname,obs,w,error,verbose)
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>obs_tsys_time
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Compute the spectrum Tsys from time, fres, and input weight, with
  ! the appropriate sanity checks.
  ! ---
  !  This is the obs_weight_time inverse function.
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: rname    ! Calling routine name
  type(observation), intent(inout) :: obs      ! Tsys changed inplace
  real(kind=8),      intent(in)    :: w        ! [s.MHz/K^2]
  logical,           intent(inout) :: error    ! Logical error flag
  logical, optional, intent(in)    :: verbose  ! Default true
  ! Local
  logical :: verbos
  !
  if (present(verbose)) then
    verbos = verbose
  else
    verbos = .true.
  endif
  !
  ! Sanity checks
  ! This test is disabled as the AVERAGE engine always goes through this,
  ! even when coming from VLM cubes where time is null.
  ! if (obs%head%gen%time.le.0.) then  ! Trap negative TIME
  !   if (verbos)  call class_message(seve%e,rname,  &
  !     'R%HEAD%GEN%TIME must be greater than zero for TIME weighting')
  !   error = .true.
  !   return
  ! endif
  !
  if (w.le.0.) then
    obs%head%gen%tsys = 0.
  elseif (obs%head%gen%kind.eq.kind_spec) then
    obs%head%gen%tsys = sqrt(obs%head%gen%time*abs(obs%head%spe%fres)/w)
    ! Folded FSW: see comments in obs_weight_time
    if (obs%head%swi%swmod.eq.mod_fold)  &
      obs%head%gen%tsys = obs%head%gen%tsys*sqrt(2.)
  else
    obs%head%gen%tsys = sqrt(obs%head%gen%time*abs(obs%head%dri%width)/w)
  endif
  !
end subroutine obs_tsys_time
