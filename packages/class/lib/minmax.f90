subroutine class_minmax_r4_1d(rmin,rmax,a,na,rbad)
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private-generic class_minmax
  ! Compute MIN and MAX from a (section of) given array taking into
  ! account blanking values and NaN
  ! ---
  ! R*4 1D version
  !---------------------------------------------------------------------
  real(kind=4),    intent(out) :: rmin   ! Min value plus lower margin
  real(kind=4),    intent(out) :: rmax   ! Max value plus upper margin
  integer(kind=4), intent(in)  :: na     ! Number of values
  real(kind=4),    intent(in)  :: a(na)  ! Input array
  real(kind=4),    intent(in)  :: rbad   ! Bad channel value
  ! Local
  character(len=*), parameter :: rname='MINMAX'
  real(kind=4) :: adiff
  integer(kind=4) :: j,jstart
  real(kind=4), parameter :: margin=0.05
  !
  rmin = 1.
  rmax = -1.
  !
  if (na.ge.1) then
    do j = 1,na
      if (a(j).eq.a(j) .and. a(j).ne.rbad) then
        rmin = a(j)
        rmax = a(j)
        exit
      endif
    enddo
    !
    jstart = j+1
    do j = jstart,na
      if (a(j).eq.a(j) .and. a(j).ne.rbad) then
        rmin = min(rmin,a(j))
        rmax = max(rmax,a(j))
      endif
    enddo
  endif
  !
  if (rmin.gt.rmax) then
    call class_message(seve%w,rname,  &
      'No valid data found in the current X range (SET MODE X). Min-Max set to 0.')
    rmin = 0.
    rmax = 0.
  endif
  !
  adiff = rmax - rmin
  if (adiff.eq.0.) adiff = max(abs(rmin),0.1)
  rmin = rmin - margin*adiff
  rmax = rmax + margin*adiff
  !
end subroutine class_minmax_r4_1d
!
subroutine class_minmax_i4_1d(rmin,rmax,a,na,ibad)
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private-generic class_minmax
  ! Compute MIN and MAX from a (section of) given array taking into
  ! account blanking values
  ! ---
  ! I*4 1D version
  !---------------------------------------------------------------------
  real(kind=4),    intent(out) :: rmin   ! Min value plus lower margin
  real(kind=4),    intent(out) :: rmax   ! Max value plus upper margin
  integer(kind=4), intent(in)  :: na     ! Number of values
  integer(kind=4), intent(in)  :: a(na)  ! Input array
  integer(kind=4), intent(in)  :: ibad   ! Bad channel value
  ! Local
  character(len=*), parameter :: rname='MINMAX'
  integer(kind=4) :: j,jstart,imin,imax,adiff
  real(kind=4), parameter :: margin=0.05
  !
  imin = 1
  imax = -1
  !
  if (na.ge.1) then
    do j = 1,na
      if (a(j).ne.ibad) then
        imin = a(j)
        imax = a(j)
        exit
      endif
    enddo
    !
    jstart = j+1
    do j = jstart,na
      if (a(j).ne.ibad) then
        imin = min(imin,a(j))
        imax = max(imax,a(j))
      endif
    enddo
  endif
  !
  if (imin.gt.imax) then
    call class_message(seve%w,rname,  &
      'No valid data found in the current X range (SET MODE X). Min-Max set to 0')
    imin = 0
    imax = 0
  endif
  !
  adiff = imax - imin
  if (adiff.eq.0) adiff = max(abs(imin),1)
  rmin = imin - margin*adiff
  rmax = imax + margin*adiff
  !
end subroutine class_minmax_i4_1d
!
subroutine class_minmax_r4_2d(rmin,rmax,a,n1,n2,rbad)
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private-generic class_minmax
  ! Compute MIN and MAX from a (section of) given array taking into
  ! account blanking values and NaN
  ! ---
  ! R*4 2D version
  !---------------------------------------------------------------------
  real(kind=4),    intent(out) :: rmin      ! Min value plus lower margin
  real(kind=4),    intent(out) :: rmax      ! Max value plus upper margin
  integer(kind=4), intent(in)  :: n1,n2     ! Number of values
  real(kind=4),    intent(in)  :: a(n1,n2)  ! Input array
  real(kind=4),    intent(in)  :: rbad      ! Bad channel value
  !
  call class_minmax_r4_1d(rmin,rmax,a,n1*n2,rbad)
  !
end subroutine class_minmax_r4_2d
