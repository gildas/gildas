!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! 1D X axes computations, single or double precision
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine abscissa_velo_r4(head,velo,c1,c2)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_velo
  !    Given the velocity axis parameters in the observation header,
  ! fill the array 'velo' with appropriate velocities in the channel
  ! range c1 to c2 (assume c1<=c2)
  ! ---
  !   Single precision version. Use generic entry point 'abscissa_velo'
  ! to access this routine.
  !---------------------------------------------------------------------
  type(header),    intent(in)    :: head     !
  real(kind=4),    intent(inout) :: velo(*)  ! Array to be filled/updated
  integer(kind=4), intent(in)    :: c1,c2    ! Channel range
  ! Local
  integer(kind=4) :: ichan
  !
  do ichan=c1,c2
    velo(ichan) = head%spe%voff + (ichan-head%spe%rchan)*head%spe%vres
  enddo
  !
end subroutine abscissa_velo_r4
!
subroutine abscissa_velo_r8(head,velo,c1,c2)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_velo
  !    Given the velocity axis parameters in the observation header,
  ! fill the array 'velo' with appropriate velocities in the channel
  ! range c1 to c2 (assume c1<=c2)
  ! ---
  !   Double precision version. Use generic entry point 'abscissa_velo'
  ! to access this routine.
  !---------------------------------------------------------------------
  type(header),    intent(in)    :: head     !
  real(kind=8),    intent(inout) :: velo(*)  ! Array to be filled/updated
  integer(kind=4), intent(in)    :: c1,c2    ! Channel range
  ! Local
  integer(kind=4) :: ichan
  !
  do ichan=c1,c2
    velo(ichan) = head%spe%voff + (ichan-head%spe%rchan)*head%spe%vres
  enddo
  !
end subroutine abscissa_velo_r8
!
subroutine abscissa_sigoff_r4(head,soff,c1,c2)
  use gbl_message
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_sigoff
  !    Given the signal frequency axis parameters in the observation
  ! header, fill the array 'soff' with appropriate signal frequency
  ! offsets in the channel range c1 to c2 (assume c1<=c2)
  ! ---
  !   Single precision version. Use generic entry point 'abscissa_sigoff'
  ! to access this routine.
  !---------------------------------------------------------------------
  type(header),    intent(in)    :: head     !
  real(kind=4),    intent(inout) :: soff(*)  ! Array to be filled/updated
  integer(kind=4), intent(in)    :: c1,c2    ! Channel range
  ! Local
  character(len=*), parameter :: rname='ABSCISSA'
  real(kind=8) :: dnu
  integer(kind=4) :: ichan
  !
  ! Doppler correction
  if (head%spe%doppler.ne.-1.d0) then
    dnu = head%spe%fres/(1.d0+head%spe%doppler)
  else
    call class_message(seve%w,rname,'No Doppler correction applied')
    dnu = head%spe%fres
  endif
  !
  do ichan=c1,c2
    soff(ichan) = (ichan-head%spe%rchan)*dnu
  enddo
  !
end subroutine abscissa_sigoff_r4
!
subroutine abscissa_sigoff_r8(head,soff,c1,c2)
  use gbl_message
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_sigoff
  !    Given the signal frequency axis parameters in the observation
  ! header, fill the array 'soff' with appropriate signal frequency
  ! offsets in the channel range c1 to c2 (assume c1<=c2)
  ! ---
  !   Double precision version. Use generic entry point 'abscissa_sigoff'
  ! to access this routine.
  !---------------------------------------------------------------------
  type(header),    intent(in)    :: head     !
  real(kind=8),    intent(inout) :: soff(*)  ! Array to be filled/updated
  integer(kind=4), intent(in)    :: c1,c2    ! Channel range
  ! Local
  character(len=*), parameter :: rname='ABSCISSA'
  real(kind=8) :: dnu
  integer(kind=4) :: ichan
  !
  ! Doppler correction
  if (head%spe%doppler.ne.-1.d0) then
    dnu = head%spe%fres/(1.d0+head%spe%doppler)
  else
    call class_message(seve%w,rname,'No Doppler correction applied')
    dnu = head%spe%fres
  endif
  !
  do ichan=c1,c2
    soff(ichan) = (ichan-head%spe%rchan)*dnu
  enddo
  !
end subroutine abscissa_sigoff_r8
!
subroutine abscissa_imaoff_r4(head,ioff,c1,c2)
  use gbl_message
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_imaoff
  !    Given the image frequency axis parameters in the observation
  ! header, fill the array 'ioff' with appropriate image frequency
  ! offsets in the channel range c1 to c2 (assume c1<=c2)
  ! ---
  !   Single precision version. Use generic entry point 'abscissa_imaoff'
  ! to access this routine.
  !---------------------------------------------------------------------
  type(header),    intent(in)    :: head     !
  real(kind=4),    intent(inout) :: ioff(*)  ! Array to be filled/updated
  integer(kind=4), intent(in)    :: c1,c2    ! Channel range
  ! Local
  character(len=*), parameter :: rname='ABSCISSA'
  real(kind=8) :: dnu
  integer(kind=4) :: ichan
  !
  ! Doppler correction
  if (head%spe%doppler.ne.-1.d0) then
    dnu = head%spe%fres/(1.d0+head%spe%doppler)
  else
    call class_message(seve%w,rname,'No Doppler correction applied')
    dnu = head%spe%fres
  endif
  !
  do ichan=c1,c2
    ioff(ichan) = -(ichan-head%spe%rchan)*dnu
  enddo
  !
end subroutine abscissa_imaoff_r4
!
subroutine abscissa_imaoff_r8(head,ioff,c1,c2)
  use gbl_message
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_imaoff
  !    Given the image frequency axis parameters in the observation
  ! header, fill the array 'ioff' with appropriate image frequency
  ! offsets in the channel range c1 to c2 (assume c1<=c2)
  ! ---
  !   Single precision version. Use generic entry point 'abscissa_imaoff'
  ! to access this routine.
  !---------------------------------------------------------------------
  type(header),    intent(in)    :: head     !
  real(kind=8),    intent(inout) :: ioff(*)  ! Array to be filled/updated
  integer(kind=4), intent(in)    :: c1,c2    ! Channel range
  ! Local
  character(len=*), parameter :: rname='ABSCISSA'
  real(kind=8) :: dnu
  integer(kind=4) :: ichan
  !
  ! Doppler correction
  if (head%spe%doppler.ne.-1.d0) then
    dnu = head%spe%fres/(1.d0+head%spe%doppler)
  else
    call class_message(seve%w,rname,'No Doppler correction applied')
    dnu = head%spe%fres
  endif
  !
  do ichan=c1,c2
    ioff(ichan) = -(ichan-head%spe%rchan)*dnu
  enddo
  !
end subroutine abscissa_imaoff_r8
!
subroutine abscissa_sigabs_r4(head,sabs,c1,c2)
  use gbl_message
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_sigabs
  !    Given the signal frequency axis parameters in the observation
  ! header, fill the array 'sabs' with appropriate absolute signal
  ! frequencies in the channel range c1 to c2 (assume c1<=c2)
  ! ---
  !   Single precision version. Use generic entry point 'abscissa_sigabs'
  ! to access this routine.
  !---------------------------------------------------------------------
  type(header),    intent(in)    :: head     !
  real(kind=4),    intent(inout) :: sabs(*)  ! Array to be filled/updated
  integer(kind=4), intent(in)    :: c1,c2    ! Channel range
  ! Local
  character(len=*), parameter :: rname='ABSCISSA'
  real(kind=8) :: dnu
  integer(kind=4) :: ichan
  !
  ! Doppler correction
  if (head%spe%doppler.ne.-1.d0) then
    dnu = head%spe%fres/(1.d0+head%spe%doppler)
  else
    call class_message(seve%w,rname,'No Doppler correction applied')
    dnu = head%spe%fres
  endif
  !
  do ichan=c1,c2
    sabs(ichan) = head%spe%restf+(ichan-head%spe%rchan)*dnu
  enddo
  !
end subroutine abscissa_sigabs_r4
!
subroutine abscissa_sigabs_r8(head,sabs,c1,c2)
  use gbl_message
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_sigabs
  !    Given the signal frequency axis parameters in the observation
  ! header, fill the array 'sabs' with appropriate absolute signal
  ! frequencies in the channel range c1 to c2 (assume c1<=c2)
  ! ---
  !   Double precision version. Use generic entry point 'abscissa_sigabs'
  ! to access this routine.
  !---------------------------------------------------------------------
  type(header),    intent(in)    :: head     !
  real(kind=8),    intent(inout) :: sabs(*)  ! Array to be filled/updated
  integer(kind=4), intent(in)    :: c1,c2    ! Channel range
  ! Local
  character(len=*), parameter :: rname='ABSCISSA'
  real(kind=8) :: dnu
  integer(kind=4) :: ichan
  !
  ! Doppler correction
  if (head%spe%doppler.ne.-1.d0) then
    dnu = head%spe%fres/(1.d0+head%spe%doppler)
  else
    call class_message(seve%w,rname,'No Doppler correction applied')
    dnu = head%spe%fres
  endif
  !
  do ichan=c1,c2
    sabs(ichan) = head%spe%restf+(ichan-head%spe%rchan)*dnu
  enddo
  !
end subroutine abscissa_sigabs_r8
!
subroutine abscissa_imaabs_r4(head,iabs,c1,c2)
  use gbl_message
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_imaabs
  !    Given the image frequency axis parameters in the observation
  ! header, fill the array 'iabs' with appropriate absolute image
  ! frequencies in the channel range c1 to c2 (assume c1<=c2)
  ! ---
  !   Single precision version. Use generic entry point 'abscissa_imaabs'
  ! to access this routine.
  !---------------------------------------------------------------------
  type(header),    intent(in)    :: head     !
  real(kind=4),    intent(inout) :: iabs(*)  ! Array to be filled/updated
  integer(kind=4), intent(in)    :: c1,c2    ! Channel range
  ! Local
  character(len=*), parameter :: rname='ABSCISSA'
  real(kind=8) :: dnu
  integer(kind=4) :: ichan
  !
  ! Doppler correction
  if (head%spe%doppler.ne.-1.d0) then
    dnu = head%spe%fres/(1.d0+head%spe%doppler)
  else
    call class_message(seve%w,rname,'No Doppler correction applied')
    dnu = head%spe%fres
  endif
  !
  do ichan=c1,c2
    iabs(ichan) = head%spe%image-(ichan-head%spe%rchan)*dnu
  enddo
  !
end subroutine abscissa_imaabs_r4
!
subroutine abscissa_imaabs_r8(head,iabs,c1,c2)
  use gbl_message
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_imaabs
  !    Given the image frequency axis parameters in the observation
  ! header, fill the array 'iabs' with appropriate absolute image
  ! frequencies in the channel range c1 to c2 (assume c1<=c2)
  ! ---
  !   Double precision version. Use generic entry point 'abscissa_imaabs'
  ! to access this routine.
  !---------------------------------------------------------------------
  type(header),    intent(in)    :: head     !
  real(kind=8),    intent(inout) :: iabs(*)  ! Array to be filled/updated
  integer(kind=4), intent(in)    :: c1,c2    ! Channel range
  ! Local
  character(len=*), parameter :: rname='ABSCISSA'
  real(kind=8) :: dnu
  integer(kind=4) :: ichan
  !
  ! Doppler correction
  if (head%spe%doppler.ne.-1.d0) then
    dnu = head%spe%fres/(1.d0+head%spe%doppler)
  else
    call class_message(seve%w,rname,'No Doppler correction applied')
    dnu = head%spe%fres
  endif
  !
  do ichan=c1,c2
    iabs(ichan) = head%spe%image-(ichan-head%spe%rchan)*dnu
  enddo
  !
end subroutine abscissa_imaabs_r8
!
subroutine abscissa_time_r4(head,time,c1,c2)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_time
  !    Given the time axis parameters in the observation header,
  ! fill the array 'time' with appropriate times in the channel
  ! range c1 to c2 (assume c1<=c2)
  ! ---
  !   Single precision version. Use generic entry point 'abscissa_time'
  ! to access this routine.
  !---------------------------------------------------------------------
  type(header),    intent(in)    :: head     !
  real(kind=4),    intent(inout) :: time(*)  ! Array to be filled/updated
  integer(kind=4), intent(in)    :: c1,c2    ! Channel range
  ! Local
  integer(kind=4) :: ichan
  !
  do ichan=c1,c2
    time(ichan) = head%dri%tref + (ichan-head%dri%rpoin)*head%dri%tres
  enddo
  !
end subroutine abscissa_time_r4
!
subroutine abscissa_time_r8(head,time,c1,c2)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_time
  !    Given the time axis parameters in the observation header,
  ! fill the array 'time' with appropriate times in the channel
  ! range c1 to c2 (assume c1<=c2)
  ! ---
  !   Double precision version. Use generic entry point 'abscissa_time'
  ! to access this routine.
  !---------------------------------------------------------------------
  type(header),    intent(in)    :: head     !
  real(kind=8),    intent(inout) :: time(*)  ! Array to be filled/updated
  integer(kind=4), intent(in)    :: c1,c2    ! Channel range
  ! Local
  integer(kind=4) :: ichan
  !
  do ichan=c1,c2
    time(ichan) = head%dri%tref + (ichan-head%dri%rpoin)*head%dri%tres
  enddo
  !
end subroutine abscissa_time_r8
!
subroutine abscissa_angl_r4(head,angl,c1,c2)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_angl
  !    Given the angle axis parameters in the observation header,
  ! fill the array 'angl' with appropriate angles in the channel
  ! range c1 to c2 (assume c1<=c2)
  ! ---
  !   Single precision version. Use generic entry point 'abscissa_angl'
  ! to access this routine.
  !---------------------------------------------------------------------
  type(header),    intent(in)    :: head     !
  real(kind=4),    intent(inout) :: angl(*)  ! Array to be filled/updated
  integer(kind=4), intent(in)    :: c1,c2    ! Channel range
  ! Local
  integer(kind=4) :: ichan
  !
  do ichan=c1,c2
    angl(ichan) = head%dri%aref + (ichan-head%dri%rpoin)*head%dri%ares
  enddo
  !
end subroutine abscissa_angl_r4
!
subroutine abscissa_angl_r8(head,angl,c1,c2)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_angl
  !    Given the angle axis parameters in the observation header,
  ! fill the array 'angl' with appropriate angles in the channel
  ! range c1 to c2 (assume c1<=c2)
  ! ---
  !   Double precision version. Use generic entry point 'abscissa_angl'
  ! to access this routine.
  !---------------------------------------------------------------------
  type(header),    intent(in)    :: head     !
  real(kind=8),    intent(inout) :: angl(*)  ! Array to be filled/updated
  integer(kind=4), intent(in)    :: c1,c2    ! Channel range
  ! Local
  integer(kind=4) :: ichan
  !
  do ichan=c1,c2
    angl(ichan) = head%dri%aref + (ichan-head%dri%rpoin)*head%dri%ares
  enddo
  !
end subroutine abscissa_angl_r8
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Scalar X value computations, single or double precision
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine abscissa_chan2velo_r4(head,chan,velo)
  use class_types
  !--------------------------------------------------------------------
  ! @ private-generic abscissa_chan2velo
  !--------------------------------------------------------------------
  type(header), intent(in)  :: head  !
  real(kind=4), intent(in)  :: chan  !
  real(kind=4), intent(out) :: velo  !
  !
  velo = (chan-head%spe%rchan)*head%spe%vres+head%spe%voff
  !
end subroutine abscissa_chan2velo_r4
!
subroutine abscissa_chan2velo_r8(head,chan,velo)
  use class_types
  !--------------------------------------------------------------------
  ! @ private-generic abscissa_chan2velo
  !--------------------------------------------------------------------
  type(header), intent(in)  :: head  !
  real(kind=8), intent(in)  :: chan  !
  real(kind=8), intent(out) :: velo  !
  !
  velo = (chan-head%spe%rchan)*head%spe%vres+head%spe%voff
  !
end subroutine abscissa_chan2velo_r8
!
subroutine abscissa_velo2chan_r4(head,velo,chan)
  use class_types
  !--------------------------------------------------------------------
  ! @ private-generic abscissa_velo2chan
  !--------------------------------------------------------------------
  type(header), intent(in)  :: head  !
  real(kind=4), intent(in)  :: velo  !
  real(kind=4), intent(out) :: chan  !
  !
  chan = head%spe%rchan+(velo-head%spe%voff)/head%spe%vres
  !
end subroutine abscissa_velo2chan_r4
!
subroutine abscissa_velo2chan_r8(head,velo,chan)
  use class_types
  !--------------------------------------------------------------------
  ! @ private-generic abscissa_velo2chan
  !--------------------------------------------------------------------
  type(header), intent(in)  :: head  !
  real(kind=8), intent(in)  :: velo  !
  real(kind=8), intent(out) :: chan  !
  !
  chan = head%spe%rchan+(velo-head%spe%voff)/head%spe%vres
  !
end subroutine abscissa_velo2chan_r8
!
subroutine abscissa_chan2sigabs_r4(head,chan,sigabs)
  use class_types
  !--------------------------------------------------------------------
  ! @ public-generic abscissa_chan2sigabs
  !--------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=4), intent(in)  :: chan    !
  real(kind=4), intent(out) :: sigabs  !
  ! Local
  real(kind=8) :: dnu
  !
  ! Doppler correction
  if (head%spe%doppler.ne.-1.d0) then
    dnu = head%spe%fres/(1.d0+head%spe%doppler)
  else
    dnu = head%spe%fres
  endif
  !
  sigabs = head%spe%restf+(chan-head%spe%rchan)*dnu
  !
end subroutine abscissa_chan2sigabs_r4
!
subroutine abscissa_chan2sigabs_r8_head(head,chan,sigabs)
  use class_types
  !--------------------------------------------------------------------
  ! @ public-generic abscissa_chan2sigabs
  !  *** type(header) as argument ***
  !--------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=8), intent(in)  :: chan    !
  real(kind=8), intent(out) :: sigabs  !
  !
  call abscissa_chan2sigabs_r8_spe(head%spe,chan,sigabs)
  !
end subroutine abscissa_chan2sigabs_r8_head
!
subroutine abscissa_chan2sigabs_r8_spe(spe,chan,sigabs)
  use class_types
  !--------------------------------------------------------------------
  ! @ public-generic abscissa_chan2sigabs
  !  *** type(class_spectro_t) as argument ***
  !--------------------------------------------------------------------
  type(class_spectro_t), intent(in)  :: spe     !
  real(kind=8),          intent(in)  :: chan    !
  real(kind=8),          intent(out) :: sigabs  !
  ! Local
  real(kind=8) :: dnu
  !
  ! Doppler correction
  if (spe%doppler.ne.-1.d0) then
    dnu = spe%fres/(1.d0+spe%doppler)
  else
    dnu = spe%fres
  endif
  !
  sigabs = spe%restf+(chan-spe%rchan)*dnu
  !
end subroutine abscissa_chan2sigabs_r8_spe
!
subroutine abscissa_sigabs2chan_r4(head,sigabs,chan)
  use class_types
  !--------------------------------------------------------------------
  ! @ public-generic abscissa_sigabs2chan
  !--------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=4), intent(in)  :: sigabs  !
  real(kind=4), intent(out) :: chan    !
  ! Local
  real(kind=8) :: dnu
  !
  ! Doppler correction
  if (head%spe%doppler.ne.-1.d0) then
    dnu = head%spe%fres/(1.d0+head%spe%doppler)
  else
    dnu = head%spe%fres
  endif
  !
  chan = head%spe%rchan+(sigabs-head%spe%restf)/dnu
  !
end subroutine abscissa_sigabs2chan_r4
!
subroutine abscissa_sigabs2chan_r8_head(head,sigabs,chan)
  use class_types
  !--------------------------------------------------------------------
  ! @ public-generic abscissa_sigabs2chan
  !  *** type(header) as argument ***
  !--------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=8), intent(in)  :: sigabs  !
  real(kind=8), intent(out) :: chan    !
  !
  call abscissa_sigabs2chan_r8_spe(head%spe,sigabs,chan)
  !
end subroutine abscissa_sigabs2chan_r8_head
!
subroutine abscissa_sigabs2chan_r8_spe(spe,sigabs,chan)
  use class_types
  !--------------------------------------------------------------------
  ! @ public-generic abscissa_sigabs2chan
  !  *** type(class_spectro_t) as argument ***
  !--------------------------------------------------------------------
  type(class_spectro_t), intent(in)  :: spe     !
  real(kind=8),          intent(in)  :: sigabs  !
  real(kind=8),          intent(out) :: chan    !
  ! Local
  real(kind=8) :: dnu
  !
  ! Doppler correction
  if (spe%doppler.ne.-1.d0) then
    dnu = spe%fres/(1.d0+spe%doppler)
  else
    dnu = spe%fres
  endif
  !
  chan = spe%rchan+(sigabs-spe%restf)/dnu
  !
end subroutine abscissa_sigabs2chan_r8_spe
!
subroutine abscissa_chan2sigoff_r4(head,chan,sigoff)
  use class_types
  !--------------------------------------------------------------------
  ! @ private-generic abscissa_chan2sigoff
  !--------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=4), intent(in)  :: chan    !
  real(kind=4), intent(out) :: sigoff  !
  ! Local
  real(kind=8) :: dnu
  !
  ! Doppler correction
  if (head%spe%doppler.ne.-1.d0) then
    dnu = head%spe%fres/(1.d0+head%spe%doppler)
  else
    dnu = head%spe%fres
  endif
  !
  sigoff = (chan-head%spe%rchan)*dnu
  !
end subroutine abscissa_chan2sigoff_r4
!
subroutine abscissa_chan2sigoff_r8(head,chan,sigoff)
  use class_types
  !--------------------------------------------------------------------
  ! @ private-generic abscissa_chan2sigoff
  !--------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=8), intent(in)  :: chan    !
  real(kind=8), intent(out) :: sigoff  !
  ! Local
  real(kind=8) :: dnu
  !
  ! Doppler correction
  if (head%spe%doppler.ne.-1.d0) then
    dnu = head%spe%fres/(1.d0+head%spe%doppler)
  else
    dnu = head%spe%fres
  endif
  !
  sigoff = (chan-head%spe%rchan)*dnu
  !
end subroutine abscissa_chan2sigoff_r8
!
subroutine abscissa_sigoff2chan_r4(head,sigoff,chan)
  use class_types
  !--------------------------------------------------------------------
  ! @ private-generic abscissa_sigoff2chan
  !--------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=4), intent(in)  :: sigoff  !
  real(kind=4), intent(out) :: chan    !
  ! Local
  real(kind=8) :: dnu
  !
  ! Doppler correction
  if (head%spe%doppler.ne.-1.d0) then
    dnu = head%spe%fres/(1.d0+head%spe%doppler)
  else
    dnu = head%spe%fres
  endif
  !
  chan = head%spe%rchan+sigoff/dnu
  !
end subroutine abscissa_sigoff2chan_r4
!
subroutine abscissa_sigoff2chan_r8(head,sigoff,chan)
  use class_types
  !--------------------------------------------------------------------
  ! @ private-generic abscissa_sigoff2chan
  !--------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=8), intent(in)  :: sigoff  !
  real(kind=8), intent(out) :: chan    !
  ! Local
  real(kind=8) :: dnu
  !
  ! Doppler correction
  if (head%spe%doppler.ne.-1.d0) then
    dnu = head%spe%fres/(1.d0+head%spe%doppler)
  else
    dnu = head%spe%fres
  endif
  !
  chan = head%spe%rchan+sigoff/dnu
  !
end subroutine abscissa_sigoff2chan_r8
!
subroutine abscissa_chan2imaabs_r4(head,chan,imaabs)
  use class_types
  !--------------------------------------------------------------------
  ! @ public-generic abscissa_chan2imaabs
  !--------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=4), intent(in)  :: chan    !
  real(kind=4), intent(out) :: imaabs  !
  ! Local
  real(kind=8) :: dnu
  !
  ! Doppler correction
  if (head%spe%doppler.ne.-1.d0) then
    dnu = head%spe%fres/(1.d0+head%spe%doppler)
  else
    dnu = head%spe%fres
  endif
  !
  imaabs = head%spe%image-(chan-head%spe%rchan)*dnu
  !
end subroutine abscissa_chan2imaabs_r4
!
subroutine abscissa_chan2imaabs_r8_head(head,chan,imaabs)
  use class_types
  !--------------------------------------------------------------------
  ! @ public-generic abscissa_chan2imaabs
  !  *** type(header) as argument ***
  !--------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=8), intent(in)  :: chan    !
  real(kind=8), intent(out) :: imaabs  !
  !
  call abscissa_chan2imaabs_r8_spe(head%spe,chan,imaabs)
  !
end subroutine abscissa_chan2imaabs_r8_head
!
subroutine abscissa_chan2imaabs_r8_spe(spe,chan,imaabs)
  use class_types
  !--------------------------------------------------------------------
  ! @ public-generic abscissa_chan2imaabs
  !  *** type(class_spectro_t) as argument ***
  !--------------------------------------------------------------------
  type(class_spectro_t), intent(in)  :: spe     !
  real(kind=8),          intent(in)  :: chan    !
  real(kind=8),          intent(out) :: imaabs  !
  ! Local
  real(kind=8) :: dnu
  !
  ! Doppler correction
  if (spe%doppler.ne.-1.d0) then
    dnu = spe%fres/(1.d0+spe%doppler)
  else
    dnu = spe%fres
  endif
  !
  imaabs = spe%image-(chan-spe%rchan)*dnu
  !
end subroutine abscissa_chan2imaabs_r8_spe
!
subroutine abscissa_imaabs2chan_r4(head,imaabs,chan)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_imaabs2chan
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=4), intent(in)  :: imaabs  !
  real(kind=4), intent(out) :: chan    !
  ! Local
  real(kind=8) :: dnu
  !
  ! Doppler correction
  if (head%spe%doppler.ne.-1.d0) then
    dnu = head%spe%fres/(1.d0+head%spe%doppler)
  else
    dnu = head%spe%fres
  endif
  !
  chan = head%spe%rchan-(imaabs-head%spe%image)/dnu
  !
end subroutine abscissa_imaabs2chan_r4
!
subroutine abscissa_imaabs2chan_r8(head,imaabs,chan)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_imaabs2chan
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=8), intent(in)  :: imaabs  !
  real(kind=8), intent(out) :: chan    !
  ! Local
  real(kind=8) :: dnu
  !
  ! Doppler correction
  if (head%spe%doppler.ne.-1.d0) then
    dnu = head%spe%fres/(1.d0+head%spe%doppler)
  else
    dnu = head%spe%fres
  endif
  !
  chan = head%spe%rchan-(imaabs-head%spe%image)/dnu
  !
end subroutine abscissa_imaabs2chan_r8
!
subroutine abscissa_chan2imaoff_r4(head,chan,imaoff)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_chan2imaoff
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=4), intent(in)  :: chan    !
  real(kind=4), intent(out) :: imaoff  !
  ! Local
  real(kind=8) :: dnu
  !
  ! Doppler correction
  if (head%spe%doppler.ne.-1.d0) then
    dnu = head%spe%fres/(1.d0+head%spe%doppler)
  else
    dnu = head%spe%fres
  endif
  !
  imaoff = -(chan-head%spe%rchan)*dnu
  !
end subroutine abscissa_chan2imaoff_r4
!
subroutine abscissa_chan2imaoff_r8(head,chan,imaoff)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_chan2imaoff
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=8), intent(in)  :: chan    !
  real(kind=8), intent(out) :: imaoff  !
  ! Local
  real(kind=8) :: dnu
  !
  ! Doppler correction
  if (head%spe%doppler.ne.-1.d0) then
    dnu = head%spe%fres/(1.d0+head%spe%doppler)
  else
    dnu = head%spe%fres
  endif
  !
  imaoff = -(chan-head%spe%rchan)*dnu
  !
end subroutine abscissa_chan2imaoff_r8
!
subroutine abscissa_imaoff2chan_r4(head,imaoff,chan)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_imaoff2chan
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=4), intent(in)  :: imaoff  !
  real(kind=4), intent(out) :: chan    !
  ! Local
  real(kind=8) :: dnu
  !
  ! Doppler correction
  if (head%spe%doppler.ne.-1.d0) then
    dnu = head%spe%fres/(1.d0+head%spe%doppler)
  else
    dnu = head%spe%fres
  endif
  !
  chan = head%spe%rchan-imaoff/dnu
  !
end subroutine abscissa_imaoff2chan_r4
!
subroutine abscissa_imaoff2chan_r8(head,imaoff,chan)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_imaoff2chan
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=8), intent(in)  :: imaoff  !
  real(kind=8), intent(out) :: chan    !
  ! Local
  real(kind=8) :: dnu
  !
  ! Doppler correction
  if (head%spe%doppler.ne.-1.d0) then
    dnu = head%spe%fres/(1.d0+head%spe%doppler)
  else
    dnu = head%spe%fres
  endif
  !
  chan = head%spe%rchan-imaoff/dnu
  !
end subroutine abscissa_imaoff2chan_r8
!
subroutine abscissa_chan2obsfre(head,chan,obsfre)
  use class_types
  !--------------------------------------------------------------------
  ! @ private
  ! Compute absolute observatory frequency at channel #chan
  !--------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=8), intent(in)  :: chan    !
  real(kind=8), intent(out) :: obsfre  !
  ! Local
  real(kind=8) :: rf
  !
  ! fres: not Doppler corrected = in observatory frame
  ! restf: shifted to observatory frame
  rf = head%spe%restf*(1.d0+head%spe%doppler)
  !
  obsfre = rf+(chan-head%spe%rchan)*head%spe%fres
  !
end subroutine abscissa_chan2obsfre
!
subroutine abscissa_chan2angl_r4(head,chan,angl)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_chan2angl
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head  !
  real(kind=4), intent(in)  :: chan  !
  real(kind=4), intent(out) :: angl  !
  !
  angl = (chan-head%dri%rpoin)*head%dri%ares+head%dri%aref
  !
end subroutine abscissa_chan2angl_r4
!
subroutine abscissa_chan2angl_r8(head,chan,angl)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_chan2angl
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head  !
  real(kind=8), intent(in)  :: chan  !
  real(kind=8), intent(out) :: angl  !
  !
  angl = (chan-head%dri%rpoin)*head%dri%ares+head%dri%aref
  !
end subroutine abscissa_chan2angl_r8
!
subroutine abscissa_angl2chan_r4(head,angl,chan)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_angl2chan
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head  !
  real(kind=4), intent(in)  :: angl  !
  real(kind=4), intent(out) :: chan  !
  !
  chan = head%dri%rpoin+(angl-head%dri%aref)/head%dri%ares
  !
end subroutine abscissa_angl2chan_r4
!
subroutine abscissa_angl2chan_r8(head,angl,chan)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_angl2chan
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head  !
  real(kind=8), intent(in)  :: angl  !
  real(kind=8), intent(out) :: chan  !
  !
  chan = head%dri%rpoin+(angl-head%dri%aref)/head%dri%ares
  !
end subroutine abscissa_angl2chan_r8
!
subroutine abscissa_chan2time_r4(head,chan,time)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_chan2time
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head  !
  real(kind=4), intent(in)  :: chan  !
  real(kind=4), intent(out) :: time  !
  !
  time = (chan-head%dri%rpoin)*head%dri%tres+head%dri%tref
  !
end subroutine abscissa_chan2time_r4
!
subroutine abscissa_chan2time_r8(head,chan,time)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_chan2time
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head  !
  real(kind=8), intent(in)  :: chan  !
  real(kind=8), intent(out) :: time  !
  !
  time = (chan-head%dri%rpoin)*head%dri%tres+head%dri%tref
  !
end subroutine abscissa_chan2time_r8
!
subroutine abscissa_time2chan_r4(head,time,chan)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_time2chan
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head  !
  real(kind=4), intent(in)  :: time  !
  real(kind=4), intent(out) :: chan  !
  !
  chan = head%dri%rpoin+(time-head%dri%tref)/head%dri%tres
  !
end subroutine abscissa_time2chan_r4
!
subroutine abscissa_time2chan_r8(head,time,chan)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic abscissa_time2chan
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head  !
  real(kind=8), intent(in)  :: time  !
  real(kind=8), intent(out) :: chan  !
  !
  chan = head%dri%rpoin+(time-head%dri%tref)/head%dri%tres
  !
end subroutine abscissa_time2chan_r8
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine abscissa_any2chan(set,obs,val,chan)
  use gbl_constant
  use classcore_interfaces, except_this=>abscissa_any2chan
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Convert a X coordinate in current unit to channel value.
  ! Signal (F) and Image (I) frequencies are assumed to be offset
  ! frequencies. Support for spectro and continuum, and also for
  ! irregular axes.
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)  :: set   !
  type(observation),   intent(in)  :: obs   !
  real(kind=8),        intent(in)  :: val   ! The value to be converted
  real(kind=8),        intent(out) :: chan  ! The corresponding channel
  ! Local
  integer(kind=4) :: ichan
  !
  if (obs%head%presec(class_sec_xcoo_id)) then
    ! Irregularly sampled X axis
    ! Interpolate: this must exist somewhere else...
    if (obs%datax(1).lt.obs%datax(2)) then
      if (val.lt.obs%datax(1)) then
        chan = 0.d0
      elseif (val.gt.obs%datax(obs%cnchan)) then
        chan = obs%cnchan+1.d0
      else
        do ichan=2,obs%cnchan
          if (val.lt.obs%datax(ichan)) exit
        enddo
        chan = real(ichan,kind=8) - ((obs%datax(ichan)-val)/(obs%datax(ichan)-obs%datax(ichan-1)))
      endif
    else
      if (val.lt.obs%datax(obs%cnchan)) then
        chan = obs%cnchan+1.d0
      elseif (val.gt.obs%datax(1)) then
        chan = 0.d0
      else
        do ichan=2,obs%cnchan
          if (val.gt.obs%datax(ichan)) exit
        enddo
        chan = real(ichan,kind=8) - ((obs%datax(ichan)-val)/(obs%datax(ichan)-obs%datax(ichan-1)))
      endif
    endif
  else
    ! Regularly sampled X axis
    if (obs%head%gen%kind.eq.kind_spec) then
      ! Spectroscopic data
      if (set%unitx(1).eq.'C') then
        chan = val
      elseif (set%unitx(1).eq.'V') then
        call abscissa_velo2chan(obs%head,val,chan)
      elseif (set%unitx(1).eq.'F') then
        call abscissa_sigoff2chan(obs%head,val,chan)
      elseif (set%unitx(1).eq.'I') then
        call abscissa_imaoff2chan(obs%head,val,chan)
      endif
    else
      ! Continuum data
      if (set%unitx(1).eq.'C') then
        chan = val
      elseif (set%unitx(1).eq.'T') then
        call abscissa_time2chan(obs%head,val,chan)
      elseif (set%unitx(1).eq.'A') then
        call abscissa_angl2chan(obs%head,val,chan)
      endif
    endif
  endif
  !
end subroutine abscissa_any2chan
!
subroutine abscissa_chan2any(set,obs,chan,val)
  use gbl_constant
  use classcore_interfaces, except_this=>abscissa_chan2any
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Convert a X coordinate from channel value to current unit.
  ! Signal (F) and Image (I) frequencies are assumed to be offset
  ! frequencies. Support for spectro and continuum, and also for
  ! irregular axes.
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)  :: set   !
  type(observation),   intent(in)  :: obs   !
  real(kind=8),        intent(in)  :: chan  ! The channel to be converted
  real(kind=8),        intent(out) :: val   ! The corresponding value
  ! Local
  integer(kind=4) :: ichan
  !
  if (obs%head%presec(class_sec_xcoo_id)) then
    ! Irregularly sampled X axis
    ! Interpolate: this must exist somewhere else...
    if (chan.lt.1d0) then
      val = obs%datax(1)
    elseif (chan.gt.obs%cnchan) then
      val = obs%datax(obs%cnchan)
    else
      ichan = floor(chan)
      val = obs%datax(ichan) + (obs%datax(ichan+1)-obs%datax(ichan))*(ichan-chan)
    endif
  else
    ! Regularly sampled X axis
    if (obs%head%gen%kind.eq.kind_spec) then
      ! Spectroscopic data
      if (set%unitx(1).eq.'C') then
        val = chan
      elseif (set%unitx(1).eq.'V') then
        call abscissa_chan2velo(obs%head,chan,val)
      elseif (set%unitx(1).eq.'F') then
        call abscissa_chan2sigoff(obs%head,chan,val)
      elseif (set%unitx(1).eq.'I') then
        call abscissa_chan2imaoff(obs%head,chan,val)
      endif
    else
      ! Continuum data
      if (set%unitx(1).eq.'C') then
        val = chan
      elseif (set%unitx(1).eq.'T') then
        call abscissa_chan2time(obs%head,chan,val)
      elseif (set%unitx(1).eq.'A') then
        call abscissa_chan2angl(obs%head,chan,val)
      endif
    endif
  endif
  !
end subroutine abscissa_chan2any
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine abscissa_velo_left(head,velo)
  use classcore_interfaces, except_this=>abscissa_velo_left
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! Compute the leftmost (channelwise) velocity of input spectrum
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head  !
  real(kind=8), intent(out) :: velo  !
  !
  call abscissa_chan2velo(head,.5d0,velo)
  !
end subroutine abscissa_velo_left
!
subroutine abscissa_velo_right(head,velo)
  use classcore_interfaces, except_this=>abscissa_velo_right
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! Compute the rightmost (channelwise) velocity of input spectrum
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head  !
  real(kind=8), intent(out) :: velo  !
  !
  call abscissa_chan2velo(head,dble(head%spe%nchan)+.5d0,velo)
  !
end subroutine abscissa_velo_right
!
subroutine abscissa_sigabs_left_head(head,sigabs)
  use classcore_interfaces, except_this=>abscissa_sigabs_left_head
  use class_types
  !---------------------------------------------------------------------
  ! @ public-generic abscissa_sigabs_left
  ! Compute the leftmost (channelwise) absolute signal frequency of
  ! input spectrum
  !  *** type(header) as argument ***
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=8), intent(out) :: sigabs  !
  !
  call abscissa_chan2sigabs(head,.5d0,sigabs)
  !
end subroutine abscissa_sigabs_left_head
!
subroutine abscissa_sigabs_left_spe(spe,sigabs)
  use classcore_interfaces, except_this=>abscissa_sigabs_left_spe
  use class_types
  !---------------------------------------------------------------------
  ! @ public-generic abscissa_sigabs_left
  ! Compute the leftmost (channelwise) absolute signal frequency of
  ! input spectrum
  !  *** type(class_spectro_t) as argument ***
  !---------------------------------------------------------------------
  type(class_spectro_t), intent(in)  :: spe     !
  real(kind=8),          intent(out) :: sigabs  !
  !
  call abscissa_chan2sigabs(spe,.5d0,sigabs)
  !
end subroutine abscissa_sigabs_left_spe
!
subroutine abscissa_sigabs_middle_head(head,sigabs)
  use classcore_interfaces, except_this=>abscissa_sigabs_middle_head
  use class_types
  !---------------------------------------------------------------------
  ! @ public-generic abscissa_sigabs_middle
  ! Compute the middle absolute signal frequency of input spectrum
  !  *** type(header) as argument ***
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=8), intent(out) :: sigabs  !
  !
  call abscissa_chan2sigabs(head,(dble(head%spe%nchan)+1.d0)/2.d0,sigabs)
  !
end subroutine abscissa_sigabs_middle_head
!
subroutine abscissa_sigabs_middle_spe(spe,sigabs)
  use classcore_interfaces, except_this=>abscissa_sigabs_middle_spe
  use class_types
  !---------------------------------------------------------------------
  ! @ public-generic abscissa_sigabs_middle
  ! Compute the middle absolute signal frequency of input spectrum
  !  *** type(class_spectro_t) as argument ***
  !---------------------------------------------------------------------
  type(class_spectro_t), intent(in)  :: spe     !
  real(kind=8),          intent(out) :: sigabs  !
  !
  call abscissa_chan2sigabs(spe,(dble(spe%nchan)+1.d0)/2.d0,sigabs)
  !
end subroutine abscissa_sigabs_middle_spe
!
subroutine abscissa_sigabs_right_head(head,sigabs)
  use classcore_interfaces, except_this=>abscissa_sigabs_right_head
  use class_types
  !---------------------------------------------------------------------
  ! @ public-generic abscissa_sigabs_right
  ! Compute the rightmost (channelwise) absolute signal frequency of
  ! input spectrum
  !  *** type(header) as argument ***
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=8), intent(out) :: sigabs  !
  !
  call abscissa_chan2sigabs(head,dble(head%spe%nchan)+.5d0,sigabs)
  !
end subroutine abscissa_sigabs_right_head
!
subroutine abscissa_sigabs_right_spe(spe,sigabs)
  use classcore_interfaces, except_this=>abscissa_sigabs_right_spe
  use class_types
  !---------------------------------------------------------------------
  ! @ public-generic abscissa_sigabs_right
  ! Compute the rightmost (channelwise) absolute signal frequency of
  ! input spectrum
  !  *** type(class_spectro_t) as argument ***
  !---------------------------------------------------------------------
  type(class_spectro_t), intent(in)  :: spe     !
  real(kind=8),          intent(out) :: sigabs  !
  !
  call abscissa_chan2sigabs(spe,dble(spe%nchan)+.5d0,sigabs)
  !
end subroutine abscissa_sigabs_right_spe
!
subroutine abscissa_imaabs_left(head,imaabs)
  use classcore_interfaces, except_this=>abscissa_imaabs_left
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! Compute the leftmost (channelwise) absolute image frequency of
  ! input spectrum
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=8), intent(out) :: imaabs  !
  !
  call abscissa_chan2imaabs(head,.5d0,imaabs)
  !
end subroutine abscissa_imaabs_left
!
subroutine abscissa_imaabs_middle_head(head,imaabs)
  use classcore_interfaces, except_this=>abscissa_imaabs_middle_head
  use class_types
  !---------------------------------------------------------------------
  ! @ public-generic abscissa_imaabs_middle
  ! Compute the middle absolute image frequency of input spectrum
  !  *** type(header) as argument ***
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=8), intent(out) :: imaabs  !
  !
  call abscissa_chan2imaabs(head,(dble(head%spe%nchan)+1.d0)/2.d0,imaabs)
  !
end subroutine abscissa_imaabs_middle_head
!
subroutine abscissa_imaabs_middle_spe(spe,imaabs)
  use classcore_interfaces, except_this=>abscissa_imaabs_middle_spe
  use class_types
  !---------------------------------------------------------------------
  ! @ public-generic abscissa_imaabs_middle
  ! Compute the middle absolute image frequency of input spectrum
  !  *** type(class_spectro_t) as argument ***
  !---------------------------------------------------------------------
  type(class_spectro_t), intent(in)  :: spe     !
  real(kind=8),          intent(out) :: imaabs  !
  !
  call abscissa_chan2imaabs(spe,(dble(spe%nchan)+1.d0)/2.d0,imaabs)
  !
end subroutine abscissa_imaabs_middle_spe
!
subroutine abscissa_imaabs_right(head,imaabs)
  use classcore_interfaces, except_this=>abscissa_imaabs_right
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! Compute the rightmost (channelwise) absolute image frequency of
  ! input spectrum
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=8), intent(out) :: imaabs  !
  !
  call abscissa_chan2imaabs(head,dble(head%spe%nchan)+.5d0,imaabs)
  !
end subroutine abscissa_imaabs_right
!
subroutine abscissa_sigoff_left(head,sigoff)
  use classcore_interfaces, except_this=>abscissa_sigoff_left
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Compute the leftmost (channelwise) offset signal frequency of
  ! input spectrum
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=8), intent(out) :: sigoff  !
  !
  call abscissa_chan2sigoff(head,.5d0,sigoff)
  !
end subroutine abscissa_sigoff_left
!
subroutine abscissa_sigoff_right(head,sigoff)
  use classcore_interfaces, except_this=>abscissa_sigoff_right
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Compute the rightmost (channelwise) offset signal frequency of
  ! input spectrum
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=8), intent(out) :: sigoff  !
  !
  call abscissa_chan2sigoff(head,dble(head%spe%nchan)+.5d0,sigoff)
  !
end subroutine abscissa_sigoff_right
!
subroutine abscissa_imaoff_left(head,imaoff)
  use classcore_interfaces, except_this=>abscissa_imaoff_left
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Compute the leftmost (channelwise) offset image frequency of
  ! input spectrum
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=8), intent(out) :: imaoff  !
  !
  call abscissa_chan2imaoff(head,.5d0,imaoff)
  !
end subroutine abscissa_imaoff_left
!
subroutine abscissa_imaoff_right(head,imaoff)
  use classcore_interfaces, except_this=>abscissa_imaoff_right
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Compute the rightmost (channelwise) offset image frequency of
  ! input spectrum
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=8), intent(out) :: imaoff  !
  !
  call abscissa_chan2imaoff(head,dble(head%spe%nchan)+.5d0,imaoff)
  !
end subroutine abscissa_imaoff_right
!
subroutine abscissa_obsfre_left(head,obsfre)
  use classcore_interfaces, except_this=>abscissa_obsfre_left
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Compute the leftmost (channelwise) absolute observatory frequency of
  ! input spectrum
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=8), intent(out) :: obsfre  !
  !
  call abscissa_chan2obsfre(head,.5d0,obsfre)
  !
end subroutine abscissa_obsfre_left
!
subroutine abscissa_obsfre_middle(head,obsfre)
  use classcore_interfaces, except_this=>abscissa_obsfre_middle
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Compute the middle absolute observatory frequency of input spectrum
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=8), intent(out) :: obsfre  !
  !
  call abscissa_chan2obsfre(head,(dble(head%spe%nchan)+1.d0)/2.d0,obsfre)
  !
end subroutine abscissa_obsfre_middle
!
subroutine abscissa_obsfre_right(head,obsfre)
  use classcore_interfaces, except_this=>abscissa_obsfre_right
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Compute the rightmost (channelwise) absolute observatory frequency
  ! of input spectrum
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head    !
  real(kind=8), intent(out) :: obsfre  !
  !
  call abscissa_chan2obsfre(head,dble(head%spe%nchan)+.5d0,obsfre)
  !
end subroutine abscissa_obsfre_right
!
subroutine abscissa_angl_left(head,angl)
  use classcore_interfaces, except_this=>abscissa_angl_left
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Compute the leftmost (channelwise) position angle of input drift
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head  !
  real(kind=8), intent(out) :: angl  !
  !
  call abscissa_chan2angl(head,.5d0,angl)
  !
end subroutine abscissa_angl_left
!
subroutine abscissa_angl_right(head,angl)
  use classcore_interfaces, except_this=>abscissa_angl_right
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Compute the rightmost (channelwise) position angle of input drift
  !---------------------------------------------------------------------
  type(header), intent(in)  :: head  !
  real(kind=8), intent(out) :: angl  !
  !
  call abscissa_chan2angl(head,dble(head%dri%npoin)+.5d0,angl)
  !
end subroutine abscissa_angl_right
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
