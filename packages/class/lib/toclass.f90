!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Code structure. Convention:
!   fits_read_*:    read the FITS into a type(classfits_t) or type(observation)
!   fits_convert_*: read the type(classfits_t) into the type(observation)
!
! loop on each HDU:
!
!   fits_read_hdu
!     fits_reset  (reset columns and so on)
!     fits_read_header_desc  (read basic header elements)
!
!   1 fits_read_basicnodata
!       fits_read_header
!       fits_convert_header  (discarded because no data)
!
!   2 fits_read_basic
!       fits_read_header
!       fits_convert_header
!       fits_head2obs
!           fits_read_header_metacard
!       fits_read_basicdata
!
!   3 fits_convert_bintable
!       fits_read_header
!       fits_convert_header
!       fits_read_bintable_header
!       1 fits_convert_bintable_bycolumn
!           fits_head2obs
!               fits_read_header_metacard
!           fits_read_bintable_hifi_datav2spectro
!           fits_check_head
!           class_write
!       2 fits_convert_bintable_byrow (loop on each row):
!           1 fits_convert_bintable_matrix
!               fits_head2obs
!                  fits_read_header_metacard
!               fits_read_bintable_wave
!               fits_check_head
!               class_write
!           2 fits_convert_bintable_byrow_hifi (repeated for each spectrum in row)
!               fits_head2obs
!                   fits_read_header_metacard
!               fits_read_bintable_hifi
!                   fits_read_bintable_hifi_datav2spectro
!               fits_check_head
!               class_write
!
!   fits_next_hdu
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine toclass(set,r,check,user_function,error)
  use gildas_def
  use gbl_format
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>toclass
  use class_types
  use class_fits
  !----------------------------------------------------------------------
  ! @ private
  ! CLASS-FITS Internal routine
  !
  ! Read a FITS file from tape or disk and conwvert it to CLASS format
  ! in virtual memory.
  ! From the header, we have NX*NY integer pixel values of NBITS
  ! each to read. The blocksize is 2880 Bytes.
  ! The conversion to REAL*4 is done using BSCAL and BZERO
  !
  ! Supports Binary Tables, and Continuum data
  !
  ! Still missing:
  ! * Coordinate Description matrix
  !   RADECSYS, MJD-OBS (the offset between Julian day numbers and
  !   internal code is 2460549.5d0).
  ! * Support for all projections
  !
  !----------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  type(observation),   intent(inout) :: r              !
  logical,             intent(in)    :: check          !
  logical,             intent(inout) :: error          ! Error status
  logical,             external      :: user_function  !
  ! Local
  character(len=*), parameter :: proc='FITS'
  logical :: doexit,nomore
  !
  fits%ishcss = .false.
  fits%version = ''
  fits%head%desc%num = -1
  fits%warn%n = 0
  !
  ! Loop over headers.
  do while (.true.)
    !
    fits%head%desc%num = fits%head%desc%num+1
    !
    call fits_read_hdu(set,fits,r,check,doexit,user_function,error)
    if (error)  return
    if (doexit) then
      call class_message(seve%r,proc,'')
      call class_message(seve%w,proc,'Skipping remaining extensions')
      exit
    endif
    !
    ! Go to the next header
    call fits_next_hdu(fits,nomore,error)
    if (error) return
    if (nomore)  exit
    !
    ! A blank line between each table
    call class_message(seve%r,proc,"")
  enddo
  !
  if (fits%ishcss) then
    call fits_warning_hifi(fits,error)
    if (error)  return
  endif
  !
  call fits_warning_dump(fits%warn,error)
  if (error)  return
  !
end subroutine toclass
!
subroutine fits_next_hdu(fits,nomore,error)
  use gildas_def
  use gkernel_interfaces
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Move the gio library to the beginning of next header
  !---------------------------------------------------------------------
  type(classfits_t), intent(in)    :: fits    !
  logical,           intent(out)   :: nomore  !
  logical,           intent(inout) :: error   !
  ! Local
  integer(kind=record_length) :: istartrec,iendrec
  integer(kind=4) :: istartoff,iendoff
  !
  call gfits_getrecnum(istartrec)
  call gfits_getrecoffset(istartoff)
  !
  ! print *,istartrec,istartoff,lheap
  iendrec = istartrec + fits%head%desc%lheap / 2880 ! theap to take into account too
  iendoff = istartoff + mod(fits%head%desc%lheap, 2880)
  if (iendoff.gt.2880) then
    iendrec = iendrec + 1
    iendoff = iendoff - 2880
  endif
  !
  ! print *,'Iendrec ',iendrec
  call gfits_flush_data(error)
  call gfits_skirec(iendrec-istartrec,error)
  !
  ! End of file ?
  nomore = gfits_iseof()
  !
end subroutine fits_next_hdu
!
subroutine fits_read_hdu(set,fits,r,check,doexit,user_function,error)
  use gkernel_interfaces
  use classcore_interfaces, except_this=>fits_read_hdu
  use class_types
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  type(classfits_t),   intent(inout) :: fits           ! Reading buffer
  type(observation),   intent(inout) :: r              !
  logical,             intent(in)    :: check          !
  logical,             intent(out)   :: doexit         ! No more to be done
  logical,             external      :: user_function  !
  logical,             intent(inout) :: error          !
  ! Local
  logical :: invalid
  !
  doexit = .false.
  !
  ! Reset columns position and so on...
  call fits_reset(fits)
  !
  ! Read the mandatory keywords.
  ! Return the fits kind ('BASIC' or 'BINTABLE'), and axis and table sizes.
  call fits_read_header_desc(fits,check,invalid,error)
  if (error)  return
  !
  ! Unsupported XTENSION: just abort
  if (invalid) then
    doexit = .true.
    return
  endif
  !
  ! Retrieve really vital information.
  !
  if (fits%head%desc%kind.eq.'BASICNODATA') then ! Primary HDU with no data.
    ! We still want to read that because we need to reach the 'end'
    ! keyword. Failing to do so results in assuming that the first header
    ! can not be longer than 2880 bytes long.  And, well, it can.
    call fits_convert_basicnodata(fits,check,error,user_function)
    if (error)  return
    !
  elseif (fits%head%desc%kind.eq.'BASIC') then    ! Basic FITS file. Read in R
    call fits_convert_basic(set,fits,r,check,error,user_function)
    if (error)  return
    ! My guess is that you MUST EXIT here, as no BINTABLE can follow
    ! a non empty HDU
    !
  elseif (fits%head%desc%kind.eq.'BINTABLE') then   ! Binary table extension.
    call fits_convert_bintable(set,check,invalid,user_function,error)
    if (error)  return
    doexit = invalid
    !
  endif
  !
end subroutine fits_read_hdu
!
subroutine fits_convert_basicnodata(fits,check,error,user_function)
  use classcore_interfaces, except_this=>fits_convert_basicnodata
  use classfits_types
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Skip the no-data Primary HDU
  !---------------------------------------------------------------------
  type(classfits_t), intent(inout) :: fits           !
  logical,           intent(in)    :: check          !
  logical,           intent(inout) :: error          !
  logical,           external      :: user_function  !
  ! Local
  type(observation) :: obs
  !
  ! Read in a dummy observation (not R), because there is no purpose
  ! reading such an empty header. Plus, BASICNODATA always comes
  ! before the BINTABLE (index mode, modifies the output file), so
  ! again I don't want to modify R while the purpose is to fill the
  ! output file.
  !
  call init_obs(obs)
  !
  call fits_read_header(fits,check,error)
  if (error)  return
  !
  call fits_convert_header(fits,obs,error,user_function)
  if (error)  return
  !
  call free_obs(obs)
  !
end subroutine fits_convert_basicnodata
!
subroutine fits_convert_basic(set,fits,obs,check,error,user_function)
  use classcore_interfaces, except_this=>fits_convert_basic
  use classfits_types
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Read FITS header into work arrays and CLASS observation
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  type(classfits_t),   intent(inout) :: fits           !
  type(observation),   intent(inout) :: obs            !
  logical,             intent(in)    :: check          !
  logical,             intent(inout) :: error          !
  logical,             external      :: user_function  !
  !
  call fits_read_header(fits,check,error)
  if (error)  return
  !
  call reallocate_obs(obs,fits%head%desc%ndata,error)
  if (error)  return
  call fits_convert_header(fits,obs,error,user_function)
  if (error)  return
  !
  ! Decode FITS header into CLASS parameters.
  call fits_head2obs(set,fits,obs,error)
  if (error)  return
  !
  ! Set default value if not present.
  call fits_check_head(obs,error)
  if (error)  return
  !
  obs%head%gen%num = 0
  obs%head%gen%ver = 0
  !
  ! Read the data
  call fits_read_basicdata(fits,obs,error)
  if (error)  return
  !
  call newdat(set,obs,error)  ! Determine plotting limits.
  if (error)  return
  !
end subroutine fits_convert_basic
!
subroutine fits_convert_bintable(set,check,invalid,user_function,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>fits_convert_bintable
  use class_fits
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  logical,             intent(in)    :: check          !
  logical,             intent(out)   :: invalid        ! Invalid bintable to be skipped?
  logical,             external      :: user_function  !
  logical,             intent(inout) :: error          !
  ! Local
  character(len=*), parameter :: rname='TOCLASS'
  integer(kind=1), allocatable :: row_buffer(:)
  type(observation) :: obs
  integer(kind=4) :: ier
  !
  invalid = .false.
  !
  ! Read FITS header into work arrays and CLASS variables.
  call fits_read_header(fits,check,error)
  if (error)  return
  !
  call init_obs(obs)
  call fits_convert_header(fits,obs,error,user_function)
  if (error)  goto 99
  !
  ! Binary table's columns' analysis.
  call fits_read_bintable_header(fits,error)
  if (error) then
    call class_message(seve%e,rname,'Error decoding binary table parameters.')
    goto 99
  endif
  !
  allocate(row_buffer(fits%cols%desc%lrow),stat=ier)
  if (failed_allocate(rname,'row_buffer',ier,error))  goto 99
  !
  call gfits_flush_data(error)   ! Reset pointer to start of next record.
  if (error) goto 99
  !
  ! Find the location of the beginning of the heap.
  !
  ! The VarArrays data are stored in the "heap".
  if (fits%head%desc%theap.eq.-1) then
    ! Default : the Heap begins just after the table data ends.
    fits%head%desc%theap = fits%head%desc%axis(1) * fits%head%desc%axis(2)
  endif
  call gfits_getrecnum(heaprec)
  call gfits_getrecoffset(heapb)
  heaprec = heaprec+1+fits%head%desc%theap/2880     ! record number.
  heapb   = fits%head%desc%theap-(fits%head%desc%theap/2880)*2880  ! byte offset in this record.
  !
  if (fits%ishcss) then
    call fits_convert_bintable_hifi(set,fits,row_buffer,obs,invalid,  &
      user_function,error)
  else
    if (fits%cols%posi%matrix.ne.0) then
      call fits_convert_bintable_byrow(set,fits,row_buffer,obs,user_function,error)
    else
      call class_message(seve%e,rname,'No valuable data found')
      error = .true.
    endif
  endif
  if (error)  goto 99
  !
  call classcore_fileout_flush(error)
  ! if (error)  continue
  !
99 continue
  ! Free the memory
  if (allocated(row_buffer))  deallocate(row_buffer)
  call free_obs(obs)
  !
  !
end subroutine fits_convert_bintable
!
subroutine fits_convert_bintable_byrow(set,fits,row_buffer,obs,  &
  user_function,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>fits_convert_bintable_byrow
  use classfits_types
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Extract 1 or more spectra found in each row, iterate on all rows
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  type(classfits_t),   intent(inout) :: fits           !
  integer(kind=1),     intent(inout) :: row_buffer(:)  !
  type(observation),   intent(inout) :: obs            ! Observation buffer
  logical,             external      :: user_function  !
  logical,             intent(inout) :: error          !
  ! Local
  integer(kind=4) :: irow,iband
  type(header) :: head
  integer(kind=size_length) :: nbyt
  !
  ! In order to avoid troubles when iterating each row, we have to
  ! re-nullify parameters which were null when entering here (i.e.
  ! same behavior for first and next rows). Typical use is:
  ! - spe%fres is a constant from FITS header
  ! - spe%restf is different from 1 row to another
  ! - spe%vres is computed from spe%restf and spe%fres in fits_check_head
  ! This needs spe%vres to be re-nullified before decoding each row.
  head = obs%head  ! Should we make a full copy_obs (data and special
                   ! sections included)?
  !
  ! Loop on table rows (irow.e. on spectra or continuum drifts).
  do irow=1,fits%cols%desc%nrows
    ! Revert to original header
    obs%head = head
    !
    ! Determine the number of channels for the spectrum.
    ! Three possibilities:
    ! Case 1: The number of channels is equal to the column width ;
    ! Case 2: The number of channels is provided in the CHANNELS column ;
    ! Case 3: The number of channels is specified in the vararray pointer.
    call check_axis(fits%head%desc%axis,fits%head%desc%naxis,  &
      fits%head%desc%ndata,fits%head%desc%main_axis,error) ! Case 1.
    if (error)  return
    !
    ! Read the row into buffer.
    nbyt = fits%cols%desc%lrow
    call gfits_getbuf(row_buffer,nbyt,error)
    if (error)  return
    !
    ! Chop the row buffer into useable header information.
    call fits_chopbuf_1header(fits,row_buffer,fits%cols%desc%lrow,obs,error)
    if (error)  return
    !
    if (fits%cols%posi%matrix.gt.0) then
      call fits_convert_bintable_matrix(set,fits,row_buffer,obs,user_function,error)
      if (error)  return
    endif
    !
    if (fits%ishcss) then
      do iband=1,mhifibands
        call fits_convert_bintable_byrow_hifi(set,row_buffer,fits,iband,  &
          obs,user_function,error)
        if (error)  return
      enddo
    endif
    !
    ! Abort if <^C>
    if (sic_ctrlc()) then
      call class_message(seve%w,'FITS','Aborted by ^C')
      error = .true.
      return
    endif
  enddo
  !
end subroutine fits_convert_bintable_byrow
!
subroutine fits_convert_bintable_matrix(set,fits,row_buffer,obs,user_function,error)
  use gbl_format
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>fits_convert_bintable_matrix
  use classfits_types
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Read data from MATRIX or SPECTRUM or SERIES or DATA column,
  ! and write it as a Class spectrum
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  type(classfits_t),   intent(inout) :: fits           !
  integer(kind=1),     intent(in)    :: row_buffer(:)  !
  type(observation),   intent(inout) :: obs            !
  logical,             external      :: user_function  !
  logical,             intent(inout) :: error          !
  ! Local
  character(len=*), parameter :: rname='FITS>READ>BINTABLE'
  integer(kind=1), allocatable :: matbuf(:)
  integer(kind=4) :: matpos, matchan
  integer(kind=size_length) :: matlen
  logical, parameter :: debug = .false.
  integer(kind=4) :: ier
  character(len=12) :: mess
  !
  if (fits%cols%posi%matrix.eq.0) then
    call class_message(seve%e,rname,'No MATRIX column!')
    error = .true.
    return
  endif
  !
  if (fits%cols%desc%vararray(fits%cols%posi%matrix)) then
    ! Case 3: Variable length array.
    ! Get the length and the position of the VarArray.
    ! The column contains a position and a length. Both are stored
    ! in the same cell: Length (4 bytes) then Position (4 bytes).
    call get_item(matchan,1,fmt_i4,row_buffer(fits%cols%desc%addr(fits%cols%posi%matrix)),eei_i4,error)
    if (error)  return
    call get_item(matpos,1,fmt_i4,row_buffer(fits%cols%desc%addr(fits%cols%posi%matrix)+4),eei_i4,error)
    if (error)  return
    !
    ! Set the number of channels. Number of useful data is patched here:
    fits%head%desc%ndata = matchan
    !
    ! Computes the VarArray physical size, ie in bytes.
    ! It depends on the precision set by TFORM.
    if (fits%cols%desc%fmt(fits%cols%posi%matrix).eq.eei_r8) then
      matlen = matchan*8
    elseif (fits%cols%desc%fmt(fits%cols%posi%matrix).eq.eei_r4 .or.  &
            fits%cols%desc%fmt(fits%cols%posi%matrix).eq.eei_i4) then
      matlen = matchan*4
    elseif (fits%cols%desc%fmt(fits%cols%posi%matrix).eq.eei_i2) then
      matlen = matchan*2
    endif
  else
    if (fits%cols%posi%channels.ne.0) then
      ! Case 2: Number of channels read from the CHANNELS column.
      fits%head%desc%ndata = obs%cnchan  ! CHANNELS[irow] was stored here
    else ! recover from format of DATA array
      fits%head%desc%ndata = fits%cols%desc%nitem(fits%cols%posi%matrix)
    endif
  endif
  !
  if (fits%head%desc%ndata.le.0) then
    call class_message(seve%w,rname,'No data in MATRIX column, skip this row')
    ! No error
    return
  endif
  !
  ! Decode FITS header into CLASS parameters.
  call fits_head2obs(set,fits,obs,error)
  if (error)  return
  !
  ! Reallocate memory for obs if more needed.
  call reallocate_obs(obs,fits%head%desc%ndata,error)
  if (error)  return
  !
  ! Finally, get the data.
  if (fits%cols%desc%vararray(fits%cols%posi%matrix)) then
    ! Variable length array.
    ! Channels are stored in a variable length array, in the heap.
    ! VarArrayPos and VarArrayLen will be used here.
    ! * First : allocate a buffer which will contain the VarArray.
    allocate(matbuf(matlen),stat=ier)
    if (failed_allocate(rname,'matbuf',ier,error))  return
    !
    ! * Second : fill this buffer with data read from the FITS file.
    call vararrayread(matpos,matbuf,matlen,error)
    if (error) then
      if (allocated(matbuf)) deallocate(matbuf)
      return
    endif
    !
    ! * Third : translate this raw byte buffer into Real*4 or whatever.
    call get_item(obs%data1,fits%head%desc%ndata,fmt_r4,matbuf,  &
      fits%cols%desc%fmt(fits%cols%posi%matrix),error)
    if (error) then
      if (allocated(matbuf)) deallocate(matbuf)
      return
    endif
    !
    ! Deallocate the buffer.
    if (allocated(matbuf)) deallocate(matbuf)
    !
  else
    ! Channels are directly written in the column.
    call get_item(obs%data1,fits%head%desc%ndata,fmt_r4,  &
      row_buffer(fits%cols%desc%addr(fits%cols%posi%matrix)),  &
      fits%cols%desc%fmt(fits%cols%posi%matrix),error)
    if (error)  return
  endif
  ! By construction, bad values are set as NaN in the column (whatever
  ! BITPIX is). Set obs%head%spe%bad to the usual Class value, and patch
  ! NaN with it.
  call modify_blanking(obs,class_bad)
  !
  if (fits%cols%posi%wave.gt.0) then
    ! Get the irregular X axis
    call fits_read_bintable_wave(row_buffer,fits%head%desc%ndata,fits%cols,  &
      obs,error)
    if (error)  return
  endif
  !
  ! Set default value if not present.
  call fits_check_head(obs,error)
  if (error)  return
  !
  ! Write to output file: single or multiple, set obs num to 0
  ! in order to give a new (unused) number in the output file.
  ! No reason there should be several versions of the same spectrum
  ! in this context (reading a FITS file)
  obs%head%gen%num = 0  ! Automatic in class_write()
  obs%head%gen%ver = 0
  if (debug) then
    call rdump(obs,"ALL",error)
    read(5, '(A)') mess
  endif
  call class_write(set,obs,error,user_function)
  if (error)  return
  !
end subroutine fits_convert_bintable_matrix
!
subroutine fits_read_bintable_wave(row_buffer,ndata,cols,obs,error)
  use gbl_constant
  use gbl_format
  use gbl_message
  use classcore_interfaces, except_this=>fits_read_bintable_wave
  use classfits_types
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Read X irregular axis from WAVE column
  !---------------------------------------------------------------------
  integer(kind=1),           intent(in)    :: row_buffer(:)  !
  integer(kind=4),           intent(in)    :: ndata          !
  type(classfits_columns_t), intent(in)    :: cols           ! Columns description
  type(observation),         intent(inout) :: obs            !
  logical,                   intent(inout) :: error          !
  ! Local
  character(len=*), parameter :: rname='FITS>READ>BINTABLE'
  integer(kind=1), allocatable :: wavbuf(:)
  integer(kind=4) :: wavpos, wavchan
  integer(kind=size_length) :: wavlen
  !
  if (cols%posi%wave.eq.0) then
    call class_message(seve%e,rname,'No WAVE column!')
    error = .true.
    return
  endif
  !
  ! Determine the number of channels for the wave.
  if (cols%desc%vararray(cols%posi%wave)) then
    ! Variable length array.
    ! Get the length and the position of the VarArray.
    ! The column contains a position and a length. Both are stored
    ! in the same cell: Length (4 bytes) then Position (4 bytes).
    call get_item(wavchan,1,fmt_i4,row_buffer(cols%desc%addr(cols%posi%wave)),eei_i4,error)
    call get_item(wavpos,1,fmt_i4,row_buffer(cols%desc%addr(cols%posi%wave)+4),eei_i4,error)
    !
    ! Computes the VarArray physical size, ie in bytes.
    !
    ! It depends on the precision set by TFORM.
    if ((cols%desc%fmt(cols%posi%wave).eq.eei_r8)) then
      WavLen = WavChan * 8
    elseif ((cols%desc%fmt(cols%posi%wave).eq.eei_r4).or.(cols%desc%fmt(cols%posi%wave).eq.eei_i4)) then
      WavLen = WavChan * 4
    elseif ((cols%desc%fmt(cols%posi%wave).eq.eei_i2)) then
      WavLen = WavChan * 2
    endif
  else
    WavChan = cols%desc%nitem(cols%posi%wave)
  endif
  if (error)  return
  !
  ! Get the irregular frequency axis.
  if (WavChan.ne.ndata) then
    call class_message(seve%e,rname, 'Wrong irregular freq axis length.')
    error = .true.
    return
  endif
  !
  if (cols%desc%vararray(cols%posi%wave)) then
    ! Variable length array.
    !
    ! First : allocate a buffer which will contain the VarArray.
    allocate(WavBuf(WavLen))
    !
    ! Second : fill this buffer with data read from the FITS file.
    call VarArrayRead(WavPos, WavBuf, WavLen, error)
    if (error) then
      if (allocated(WavBuf)) deallocate(WavBuf)
      return
    endif
    !
    ! Third : translate this raw byte buffer into Real*4 or whatever.
    if (xdata_kind.eq.4) then
      call get_item(obs%datav,ndata,fmt_r4,WavBuf,cols%desc%fmt(cols%posi%wave),error)
    else
      call get_item(obs%datav,ndata,fmt_r8,WavBuf,cols%desc%fmt(cols%posi%wave),error)
    endif
    if (error) then
      if (allocated(WavBuf))  deallocate(WavBuf)
      return
    endif
    !
    ! Deallocate the buffer.
    if (allocated(WavBuf))  deallocate(WavBuf)
    !
  else
    ! Channels are directly written in the column.
    if (xdata_kind.eq.4) then
      call get_item(obs%datav, ndata, fmt_r4,   &
                    row_buffer(cols%desc%addr(cols%posi%wave)),   &
                    cols%desc%fmt(cols%posi%wave), error)
    else
      call get_item(obs%datav, ndata, fmt_r8,   &
                    row_buffer(cols%desc%addr(cols%posi%wave)),   &
                    cols%desc%fmt(cols%posi%wave), error)
    endif
    if (error)  return
  endif
  !
  obs%cnchan = ndata
  obs%head%presec(class_sec_xcoo_id) = .true.
  obs%head%gen%xunit = a_freq  ! a_wave a_velo
  !
end subroutine fits_read_bintable_wave
!
subroutine fits_reset_column_positions(pos)
  use classfits_types
  !----------------------------------------------------------------------
  ! @ private
  !   Reset the pointers to the bintable columns. 0 means 'I don't know
  ! this column (yet).'
  !----------------------------------------------------------------------
  type(classfits_column_positions_t), intent(inout) :: pos
  !
  pos%naxis      = 0
  pos%axis(:)    = 0
  pos%crval(:)   = 0
  pos%cdelt(:)   = 0
  pos%crpix(:)   = 0
  pos%crota(:)   = 0
  pos%axtyp(:)   = 0
  pos%wave       = 0
  pos%channels   = 0 !  DEPRECATED, use MAXIS1 instead.
  pos%scan       = 0
  pos%subscan    = 0
  pos%line       = 0
  pos%object     = 0
  pos%telesc     = 0
  pos%backend    = 0  ! UNUSED.
  pos%matrix     = 0
  pos%tsys       = 0
  pos%restfreq   = 0
  pos%imagfreq   = 0
  pos%velocity   = 0
  pos%specsys    = 0
  pos%velref     = 0
  pos%veldef     = 0
  pos%deltav     = 0
  pos%nphase     = 0
  pos%deltaf(:)  = 0
  pos%ptime(:)   = 0
  pos%weight(:)  = 0
  pos%tau_atm    = 0
  pos%tauo2      = 0 ! UNUSED, but should be implemented.
  pos%tauh2o     = 0 ! UNUSED, but should be implemented.
  pos%h2omm      = 0
  pos%tamb       = 0
  pos%pamb       = 0
  pos%tchop      = 0
  pos%tcold      = 0
  pos%elevatio   = 0
  pos%azimuth    = 0
  pos%gainimag   = 0
  pos%beameff    = 0
  pos%forweff    = 0
  pos%equinox    = 0
  pos%dobs       = 0
  pos%dred       = 0
  pos%ut         = 0
  pos%lst        = 0
  pos%datamax    = 0
  pos%datamin    = 0
  pos%time       = 0
  pos%bmaj       = 0
  pos%bmin       = 0
  pos%bpa        = 0
  !
end subroutine fits_reset_column_positions
!
subroutine fits_reset_column_desc(desc)
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(classfits_column_desc_t), intent(inout) :: desc
  !
  desc%ncols = 0
  desc%ttype(:)    = ''
  desc%tform(:)    = ''
  desc%tunit(:)    = ''
  desc%vararray(:) = .false.
  desc%addr(:)     = 0
  desc%leng(:)     = 0
  desc%fmt(:)      = 0
  desc%nitem(:)    = 0
  !
end subroutine fits_reset_column_desc
!
subroutine fits_reset(fits)
  use classcore_interfaces, except_this=>fits_reset
  use classfits_types
  !----------------------------------------------------------------------
  ! @ private
  !  Resets everything before reading a new file (or a new binary table
  ! extension)
  !----------------------------------------------------------------------
  type(classfits_t), intent(inout) :: fits
  !
  ! Initialise all important FITS variables here.
  !
  fits%head%bscal = 1.d0
  fits%head%bzero = 0.d0
  fits%head%blank = 2147483647.d0
  fits%head%rbad  = -1.e38
  fits%head%desc%theap = -1
  !
  fits%head%desc%axval(:)  = 0.d0
  fits%head%desc%axref(:)  = 0.d0
  fits%head%desc%axinc(:)  = 0.d0
  fits%head%desc%rota(:) = 0.d0
  fits%head%desc%axtype(:) = ' '
  fits%head%desc%kind = ' '
  !
  ! Reset the columns description
  call fits_reset_column_desc(fits%cols%desc)
  call fits_reset_column_positions(fits%cols%posi)
end subroutine fits_reset
!
subroutine fits_read_basicdata(fits,obs,error)
  use gildas_def
  use gkernel_interfaces
  use classcore_interfaces, except_this=>fits_read_basicdata
  use classfits_types
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  ! Read all the data
  !----------------------------------------------------------------------
  type(classfits_t), intent(in)    :: fits   !
  type(observation), intent(inout) :: obs    !
  logical,           intent(inout) :: error  ! Error status
  ! Local
  integer(kind=size_length) :: nfill,kdata
  integer(kind=size_length), parameter :: bufsize1=2880
  integer(kind=size_length), parameter :: bufsize2=1440
  integer(kind=size_length), parameter :: bufsize4=720
  integer(kind=1) :: buffer(bufsize1)
  !
  call gfits_flush_data(error)       ! Reset pointer to start of next record
  if (error) return
  nfill = 0
  kdata = fits%head%desc%ndata
  do while (nfill.lt.fits%head%desc%ndata)
    call gfits_getbuf(buffer,bufsize1,error)
    if (error) return
    if (fits%head%desc%nbit.eq.16) then
      call int2_to_real(buffer,bufsize2,obs%data1,kdata,nfill,fits%head%bscal,fits%head%bzero)
      ! See below
      call modify_blanking_head(obs,fits%head%rbad)
      !
    elseif (fits%head%desc%nbit.eq.32) then
      call int4_to_real(buffer,bufsize4,obs%data1,kdata,nfill,fits%head%bscal,fits%head%bzero)
      ! Note that bad values were encoded as FITS%HEAD%BLANK in the FITS, and
      ! they are rescaled into a R*4 float as any other in the spectrum.
      ! OBS%HEAD%SPE%BAD is set accordingly, so all is fine to 1st order, but
      ! this uses a relatively valid value as bad. We can patch them (e.g.
      ! call modify_blank(obs,class_bad)), but what about efficiency?
      call modify_blanking_head(obs,fits%head%rbad)
      !
    elseif (fits%head%desc%nbit.eq.-32) then
      call ieee32_to_real(buffer,bufsize4,obs%data1,kdata,nfill,fits%head%bscal,  &
        fits%head%bzero,class_bad)
      ! Note that bad values were encoded as NaN in the FITS. Set obs%head%spe%bad
      ! to the usual Class value, and patch NaN with it.
      call modify_blanking(obs,class_bad)
    endif
  enddo
  obs%spectre => obs%data1
end subroutine fits_read_basicdata
!
subroutine fits_check_head(obs,error)
  use gbl_constant
  use phys_const
  use gbl_message
  use classcore_interfaces, except_this=>fits_check_head
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  ! CLASS-FITS Internal routine
  !
  ! Check that no fundamental information is missing, and provide
  ! defaults otherwise.
  !----------------------------------------------------------------------
  type(observation), intent(inout) :: obs    !
  logical,           intent(out)   :: error  !
  ! Local
  character(len=*), parameter :: proc='fits_check_head'
  !
  if (obs%head%spe%vres.eq.0.d0) then
     if (obs%head%spe%restf.ne.0.d0 .and. obs%head%spe%fres.ne.0.d0) then
        obs%head%spe%vres = -clight_kms/obs%head%spe%restf*obs%head%spe%fres
     else
        call class_message(seve%w,proc,'No frequency information')
        obs%head%spe%vres = 1.d0
     endif
  endif
  if (obs%head%spe%fres.eq.0.d0) then
     if (obs%head%spe%restf.ne.0.d0 .and. obs%head%spe%vres.ne.0.d0) then
        obs%head%spe%fres = -obs%head%spe%restf/clight_kms*obs%head%spe%vres
     else
        call class_message(seve%w,proc,'No frequency information')
        obs%head%spe%fres = 1.d0
     endif
  endif
  if (obs%head%gen%kind.eq.kind_cont) then
     if (obs%head%dri%width.eq.0) then
        call class_message(seve%w,proc,'No Bandwidth available, 1.0 used')
        obs%head%dri%width = 1.
     endif
  endif
  !
  ! Initialize PRESEC ?
  ! Bertrand Delforge 16/08/06 : no. Otherwise, can never be reset and the
  ! next observations read from the fits file will have useless sections
  ! activated.
  !
  error = .false.
end subroutine fits_check_head
!
subroutine fits_head2obs(set,fits,obs,error)
  use gbl_constant
  use classcore_interfaces, except_this=>fits_head2obs
  use class_types
  use classfits_types
  !----------------------------------------------------------------------
  ! @ private
  ! Try to translate the FITS header into sensible CLASS parameters.
  !----------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(classfits_t),   intent(inout) :: fits   !
  type(observation),   intent(inout) :: obs    !
  logical,             intent(inout) :: error  !
  !
  ! HIFI or not HIFI specific
  if (fits%ishcss) then
    continue
  else
    call fits_head2obs_class(set,fits,obs,error)
    if (error)  return
  endif
  !
end subroutine fits_head2obs
!
subroutine fits_head2obs_class(set,fits,obs,error)
  use gbl_constant
  use phys_const
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>fits_head2obs_class
  use class_setup_new
  use classfits_types
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  FITS header to observation header conversion
  !  Specific to the CLASS FITS format
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(classfits_t),   intent(inout) :: fits   ! Unit header
  type(observation),   intent(inout) :: obs    !
  logical,             intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='TOCLASS'
  integer(kind=4) :: ivel,ibet,ilam,vtype,axistype,lamtype,bettype,n,i,iutc
  real(kind=8) :: foff
  real(kind=8) :: xxx,yyy,angle
  character(len=message_length) :: mess
  character(len=20) :: atype,origin
  character(len=80) :: backend
  logical :: found
  character(len=4), parameter :: pcode(0:mproj) =  &
    (/ '    ','-TAN','-SIN','-ARC','-STG','lamb',  &
       '-ATF','-GLS','-SFL','-MOL','-NCP','-CAR' /)
  !
  ! Decode header information: identifies Lambda, Beta and spectroscopic axes.
  ilam = 0
  ibet = 0
  iutc = 0
  ivel = 0
  vtype = vel_unk
  axistype = 0
  lamtype = type_un
  bettype = type_un
  !
  do i= 1,fits%head%desc%naxis
     atype = fits%head%desc%axtype(i)
     n = len(atype)
     call sic_black(atype,n)
   ! if (index(atype,'FREQ').ne.0) then
     if (atype.eq.'FREQ' .or. atype.eq.'FREQUENCY') then
        if (ivel.ne.0) then
           call class_message(seve%e,rname,'More than one frequency axis')
           error = .true.
           return
        endif
        ivel = i
        axistype = a_freq
     elseif (atype.eq.'LAMBDA'.or.atype.eq.'WAVELENG') then
        if (ivel.ne.0) then
           call class_message(seve%e,rname,'More than one frequency axis')
           error = .true.
           return
        endif
        ivel = i
        axistype = a_wave
     elseif (atype(1:4).eq.'VELO'  .or.  &
             atype(1:4).eq.'VRAD'  .or.  &
             atype(1:4).eq.'FELO') then
        if (ivel.ne.0) then
           call class_message(seve%e,rname,'More than one frequency axis')
           error = .true.
           return
        endif
        ivel = i
        axistype = a_velo
        if (atype(5:8).eq.'-LSR' ) then
           vtype = vel_lsr
        elseif (atype(5:8).eq.'-HEL' ) then
           vtype = vel_hel
        elseif (atype(5:8).eq.'-OBS'.or.atype(5:8).eq.'-TOP' ) then
           vtype = vel_obs
        elseif (atype(5:8).eq.'-EAR'.or.atype(5:8).eq.'-GEO' ) then
           vtype = vel_ear
        endif
     elseif (atype(1:4).eq.'RA--' .or. atype.eq.'RA') then
        if (ilam.ne.0) then
           call class_message(seve%e,rname,'More than one Lamda-like axis')
           error = .true.
           return
        endif
        ilam = i
        lamtype = type_eq  ! Default if RADESYS missing
     elseif (atype(1:4).eq.'DEC-' .or. atype.eq.'DEC') then
        if (ibet.ne.0) then
           call class_message(seve%e,rname,'More than one Beta-like axis')
           error = .true.
           return
        endif
        ibet = i
        bettype = type_eq  ! Default if RADESYS missing
     elseif (atype(1:4).eq.'GLON') then
        if (ilam.ne.0) then
           call class_message(seve%e,rname,'More than one Lamda-like axis')
           error = .true.
           return
        endif
        ilam = i
        lamtype = type_ga
     elseif (atype(1:4).eq.'GLAT') then
        if (ibet.ne.0) then
           call class_message(seve%e,rname,'More than one Beta-like axis')
           error = .true.
           return
        endif
        ibet = i
        bettype = type_ga
        ! Assume time means UTC...
     elseif (atype.eq.'TIME'.or.atype.eq.'UT'.or.atype.eq.'UTC') then
        iutc = i
        ! Add ecliptic and horizontal coordinates here one day...
     elseif (atype.ne.' ' .and. atype.ne.'STOKES') then
        write(mess,'(a,a,i2)') 'Unsupported axis type ',atype,i
        call class_message(seve%w,rname,mess)
     endif
  enddo
  !
  ! Coordinate system
  if (lamtype.ne.bettype)  call class_message(seve%w,rname,'Inconsistent coordinate system')
  if (obs%head%pos%system.eq.type_un)  obs%head%pos%system = lamtype
  !
  ! Decode projection information.
  if (fits%head%desc%axtype(ilam)(5:8).ne.fits%head%desc%axtype(ibet)(5:8)) then
     call class_message(seve%w,rname,'Inconsistent projection definition: '//  &
       fits%head%desc%axtype(ilam)(5:8)//' '//fits%head%desc%axtype(ibet)(5:8))
  endif
  obs%head%pos%proj = -1
  obs%head%pos%projang = 0.d0
  do i=0,mproj
     if (fits%head%desc%axtype(ilam)(5:8).eq.pcode(i)) obs%head%pos%proj = i
  enddo
  if (obs%head%pos%proj.eq.-1) then
     call class_message(seve%w,rname,'Unsupported projection, Radio assumed')
     obs%head%pos%proj = p_radio
  endif
  !
  ! Check for unsupported rotated axes
  do i=1,fits%head%desc%naxis
     if (fits%head%desc%rota(i).ne.0.d0) then
        if (i.ne.ibet .and.  &
           (i.ne.ilam .or. fits%head%desc%rota(i).ne.fits%head%desc%rota(ibet))) then
           write(mess,*) 'Axis ',i,' rotation ignored'
           call class_message(seve%w,rname,mess)
        endif
     endif
  enddo
  !
  ! Main axis determines data type (spectrum or continuum drift).
  ! If no axis has a recognized data type (and most likely any datatype),
  ! then default to spectroscopy and try to nonetheless decode whatever
  ! axis information might be present into accessible header space.
  if (ivel.eq.0 .and. ibet.eq.0 .and. ilam.eq.0) then
     call class_message(seve%w,rname,'Unsupported data type. Spectroscopy assumed.')
     ivel = fits%head%desc%main_axis
  endif
  if (fits%head%desc%main_axis.eq.ivel) then
     obs%head%gen%kind = kind_spec
     foff = 0.d0
     if (axistype.eq.a_velo) then
        obs%head%spe%voff = fits%head%desc%axval(ivel)*1.d-3
        obs%head%spe%vres = fits%head%desc%axinc(ivel)*1.d-3
        if (vtype.ne.vel_unk)  obs%head%spe%vtype = vtype
        if (obs%head%spe%vtype.eq.vel_unk) then
          ! Note: might come from SPECSYS, VELREF, or velocity axis name (typically CTYPE3)
          call class_message(seve%w,rname,'Unknown velocity referential')
        endif
     elseif (axistype.eq.a_freq) then
        !* To be checked in detail... CHECKED, WAS WRONG!
        ! RFOFF = AXVAL(IVEL)*1.D-6-RRESTF
        foff = fits%head%desc%axval(ivel)*1.d-6
        if (foff.ne.0.d0) then
          call gfits_get_value(fits%head%dict,'ORIGIN',found,origin,error)
          if (error)  error = .false.
          if (found .and. origin.eq.'CLASS-Grenoble') then
            ! Only CLASS-FITS stores a possible frequency offset in CRVALi.
            call rspec_classic_foffpatch(obs%head%spe,real(foff,kind=4))
          else
            ! Standard FITS cubes usually duplicate the rest frequency
            ! information in both RESTFREQ and CRVAL3. If they are different,
            ! CRVALi is to be taken, and the RESTFREQ informative value is
            ! lost.
            obs%head%spe%restf = foff
          endif
        endif
        obs%head%spe%fres = fits%head%desc%axinc(ivel)*1.d-6
     elseif (axistype.eq.a_wave) then
        ! RRESTF = CLIGHT/AXVAL(IVEL)*1.D-6
        ! RFRES = -(CLIGHT/AXVAL(IVEL))**2*AXINC(IVEL)*1.D-6
        obs%head%spe%restf = fits%head%desc%axval(ivel)
        obs%head%spe%fres  = fits%head%desc%axinc(ivel)
     else                       ! Unrecognized type, default to velocity
        obs%head%spe%voff = fits%head%desc%axval(ivel)*1.d-3
        obs%head%spe%vres = fits%head%desc%axinc(ivel)*1.d-3
     endif
     obs%head%spe%rchan = fits%head%desc%axref(fits%head%desc%main_axis)
     obs%head%spe%nchan = fits%head%desc%ndata
     !
     ! Continuum drift
  elseif (fits%head%desc%main_axis.eq.ilam.or.fits%head%desc%main_axis.eq.ibet) then
     obs%head%gen%kind = kind_cont
     obs%head%dri%npoin = fits%head%desc%ndata
     obs%head%dri%ctype = obs%head%pos%system
     ! Determine position angle of the drift from coordinate of main axis.
     if (fits%head%desc%main_axis.eq.ilam) then
        obs%head%dri%apos = 0
     else
        obs%head%dri%apos = 0.5*pi
     endif
     obs%head%dri%apos = obs%head%dri%apos+(fits%head%desc%rota(ibet)*pi/180.d0)
     obs%head%dri%aref  = 0.0               ! CRVAL is used for RLAM
     obs%head%dri%rpoin = fits%head%desc%axref(fits%head%desc%main_axis)
     obs%head%dri%ares  = fits%head%desc%axinc(fits%head%desc%main_axis)*pi/180.d0
     if (axistype.eq.a_velo) then
        obs%head%dri%freq  = obs%head%spe%restf*(1-(1.d0-fits%head%desc%axref(ivel))*fits%head%desc%axinc(ivel)/clight)
      ! obs%head%dri%freq  = obs%head%dri%freq*1.d-6
        obs%head%dri%width = -fits%head%desc%axinc(ivel)/clight*obs%head%dri%freq
     elseif (axistype.eq.a_freq) then
        obs%head%dri%freq  = fits%head%desc%axval(ivel)*1.d-6
        obs%head%dri%width = fits%head%desc%axinc(ivel)*1.d-6
     elseif (axistype.eq.a_wave) then
        obs%head%dri%freq  = clight/fits%head%desc%axval(ivel)*1.d-6
        obs%head%dri%width = -(clight/fits%head%desc%axval(ivel))**2*fits%head%desc%axinc(ivel)*1.d-6
     endif
  else
     call class_message(seve%w,rname,'Unsupported data type. Spectroscopy assumed.')
     obs%head%gen%kind = kind_spec
     obs%head%spe%nchan = fits%head%desc%ndata
  endif
  !
  ! Decode time axis if present
  if (iutc.ne.0) then
     obs%head%gen%ut = fits%head%desc%axval(iutc)+(1.d0-fits%head%desc%axref(iutc))*fits%head%desc%axinc(iutc)
  endif
  !
  if (obs%head%gen%tau.eq.-10) then
     obs%head%gen%tau = fits%head%tauo2+fits%head%tauh2o
  endif
  !
  ! Telescope
  backend = ''
  call fits_get_header_card(fits,'BACKEND',backend,found,error)
  if (error)  return
  obs%head%gen%teles = trim(obs%head%gen%teles)//backend
  !
  ! Offsets correspond to pixel (1,1) for spectroscopic data,
  !                    to pixel (Xref,1) for continuum.
  if (fits%head%desc%main_axis.eq.ilam) then
    xxx = 0.d0
  else
    xxx = (1.d0-fits%head%desc%axref(ilam))*fits%head%desc%axinc(ilam)*pi/180.d0
  endif
  if (fits%head%desc%main_axis.eq.ibet) then
    yyy = 0.d0
  else
    yyy = (1.d0-fits%head%desc%axref(ibet))*fits%head%desc%axinc(ibet)*pi/180.d0
  endif
  angle = fits%head%desc%rota(ibet)*pi/180.d0
  obs%head%pos%lamof  = xxx*cos(angle)-yyy*sin(angle)
  obs%head%pos%betof  = xxx*sin(angle)+yyy*cos(angle)
  obs%head%pos%lam    = fits%head%desc%axval(ilam)*pi/180.d0
  obs%head%pos%bet    = fits%head%desc%axval(ibet)*pi/180.d0
  obs%head%pos%rx_off = obs%head%pos%lamof*class_setup_get_fangle()  ! Convert to current angular unit
  obs%head%pos%ry_off = obs%head%pos%betof*class_setup_get_fangle()
  !
end subroutine fits_head2obs_class
!
subroutine fits_read_header(fits,check,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>fits_read_header
  use classfits_types
  !----------------------------------------------------------------------
  ! @ private
  ! Read header information into FITS work arrays
  !----------------------------------------------------------------------
  type(classfits_t), intent(inout) :: fits   !
  logical,           intent(in)    :: check  ! Echo card images on terminal?
  logical,           intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='FITS>READ>HEADER'
  integer(kind=4) :: errcount,icard
  character(len=key_length) :: key,trans
  character(len=message_length) :: mess
  real(kind=4), parameter :: epsr4=1.e-7
  !
  ! Load the header in a dictionnary
  call gfits_load_header(fits%head%dict,check,sic_getsymbol,error)
  if (error)  return
  !
  ! Now translate the dictionnary to some key components
  errcount = 0
  do icard=1,fits%head%dict%ncard
    !
    ! Look for a possible translation
    key = fits%head%dict%card(icard)%key
    call sic_getsymbol(key,trans,error)
    if (error) then
      error = .false.
    else
      key = trans
    endif
    !
    call fits_read_header_card(key,fits%head%dict%card(icard)%val,fits,errcount,error)
    if (error)  return
    !
  enddo
  !
  if (errcount.gt.0) then
     write(mess,'(I0,A)')  &
       errcount,' errors while decoding header. Proceed at your own risks!'
     call class_message(seve%w,rname,mess)
  endif
  !
  ! Post header reading
  ! Determine blanking value
  if (fits%head%desc%nbit.gt.0) then
    ! This must be done after BSCALE, BZERO, and BLANK have been read
    ! In the standard, BLANK is the INTEGER blanking value, but CFITS used to
    ! (incorrectly) consider it as its REAL equivalent.
    if (abs(fits%head%blank-nint(fits%head%blank)).gt.epsr4) then
      call class_message(seve%w,rname,'BLANK not an integer. Interpreted as Real blanking value')
      fits%head%rbad = fits%head%blank
    else
      fits%head%rbad = fits%head%bscal*fits%head%blank+fits%head%bzero
    endif
  else
    ! If BITPIX is negative, bad values are encoded as NaN. Leave default
    ! value unchanged
    continue
  endif
  !
end subroutine fits_read_header
!
subroutine fits_convert_header(fits,obs,error,user_function)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>fits_convert_header
  use classfits_types
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  !   Read header information into CLASS variables
  !----------------------------------------------------------------------
  type(classfits_t), intent(inout) :: fits           !
  type(observation), intent(inout) :: obs            !
  logical,           intent(inout) :: error          !
  logical,           external      :: user_function  !
  ! Local
  character(len=*), parameter :: rname='FITS>READ>HEADER'
  integer(kind=4) :: errcount,icard
  character(len=key_length) :: key,trans
  character(len=message_length) :: mess
  !
  call rzero(obs,'NULL',user_function)
  obs%head%xnum = -1  ! Does not come from a Class file or buffer
  !
  obs%head%presec(:) = .false.
  obs%head%presec(class_sec_gen_id) = .true.
  obs%head%presec(class_sec_pos_id) = .true.
  obs%head%presec(class_sec_spe_id) = .true.
  !
  obs%head%spe%line  = "UNKNOWN"
  obs%head%gen%teles = "UNKNOWN"
  obs%head%pos%sourc = "UNKNOWN"
  !
  ! File uses HCSS format? Yes if Primary header. Same for the 'version'
  ! field.
  if (fits%head%desc%num.eq.0) then  ! Primary HDU
    call fits_parse_ishcss_hifi(fits,fits%ishcss,error)
    if (error)  return
    call fits_parse_version_hifi(fits,fits%version,error)
    if (error)  return
  endif
  !
  ! Now find the real information.
  errcount = 0
  do icard=1,fits%head%dict%ncard
    !
    ! Look for a possible translation
    key = fits%head%dict%card(icard)%key
    call sic_getsymbol(key,trans,error)
    if (error) then
      error = .false.
    else
      key = trans
    endif
    !
    call fits_convert_header_card(key,fits%head%dict%card(icard)%val,obs,errcount,error)
    if (error)  return
    !
  enddo
  !
  if (errcount.gt.0) then
     write(mess,'(I0,A)')  &
       errcount,' errors while decoding header. Proceed at your own risks!'
     call class_message(seve%w,rname,mess)
  endif
  !
end subroutine fits_convert_header
!
subroutine fits_read_header_card(comm,argu,fits,inerror,error)
  use phys_const
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>fits_read_header_card
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Decode the card (name+value) into the fits header structure
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: comm     !
  character(len=*),  intent(in)    :: argu     !
  type(classfits_t), intent(inout) :: fits     ! FITS header
  integer(kind=4),   intent(inout) :: inerror  ! Minor errors to be counted
  logical,           intent(inout) :: error    ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='FITS>READ>HEADER>CARD'
  real(kind=8) :: xxx
  logical, parameter :: debug=.true.
  integer(kind=4) :: i,ier
  character(len=message_length) :: mess
  logical :: found
  !
  found = .true.
  if (comm.eq.'BSCALE') then       ! Tape scaling factor
     read(argu,1002,iostat=ier) fits%head%bscal
     if (ier.ne.0) call werror(comm,inerror,debug)
  elseif (comm.eq.'BZERO') then    ! Tape offset
     read(argu,1002,iostat=ier) fits%head%bzero
     if (ier.ne.0) call werror(comm,inerror,debug)
  elseif (comm.eq.'BLANK') then    ! Blanking value
     read(argu,1002,iostat=ier) fits%head%blank
     if (ier.ne.0) call werror(comm,inerror,debug)
! elseif (comm.eq.'BUNIT') then  ! Array units
!    rtunit = gfits_unquote(argu)
     !
  ! Axis information
  elseif (comm(1:5).eq.'CRVAL') then   ! Value at Ref pixel
     read(comm(6:8),'(BN,I3.1)',iostat=ier) i
     if (ier.eq.0) read(argu,1002,iostat=ier) xxx
     if (ier.ne.0) call werror(comm,inerror,debug)
     fits%head%desc%axval(i) = xxx
  elseif (comm(1:5).eq.'CRPIX') then   ! Reference pixel
     read(comm(6:8),'(BN,I3.1)',iostat=ier) i
     if (ier.eq.0) read(argu,1002,iostat=ier) xxx
     if (ier.ne.0) call werror(comm,inerror,debug)
     fits%head%desc%axref(i) = xxx
  elseif (comm(1:5).eq.'CDELT') then   ! Sampling step
     read(comm(6:8),'(BN,I3.1)',iostat=ier) i
     if (ier.eq.0) read(argu,1002,iostat=ier) xxx
     if (ier.ne.0) call werror(comm,inerror,debug)
     fits%head%desc%axinc(i) = xxx
  elseif (comm(1:5).eq.'CTYPE') then   ! Type of coordinate
     read(comm(6:8),'(BN,I3.1)',iostat=ier) i
     if (ier.ne.0) then
       call werror(comm,inerror,debug)
     else
       fits%head%desc%axtype(i) = gfits_unquote(argu)
     endif
  elseif (comm(1:5).eq.'CROTA') then
     read(comm(6:8),'(BN,I3.1)',iostat=ier) i
     if (ier.eq.0) read(argu,1002,iostat=ier)  xxx
     if (ier.ne.0) call werror(comm,inerror,debug)
     fits%head%desc%rota(i) = xxx
     !
     !
  ! Miscellaneous
  elseif (comm.eq.'INSTRUME') then
     fits%head%instrume = gfits_unquote(argu)
     call sic_upper(fits%head%instrume)
     !
     !
  ! Calibration
  elseif (comm.eq.'TAUO2') then
     read(argu,1002,iostat=ier) fits%head%tauo2
     if (ier.ne.0) call werror(comm,inerror,debug)
  elseif (comm.eq.'TAUH2O') then
     read(argu,1002,iostat=ier) fits%head%tauh2o
     if (ier.ne.0) call werror(comm,inerror,debug)
     !
  ! Miscellaneous unused.
  elseif (comm.eq.'DATAMAX') then
     read(argu,1002,iostat=ier)  fits%head%datamax
     if (ier.ne.0) call werror(comm,inerror,debug)
  elseif (comm.eq.'DATAMIN') then
     read(argu,1002,iostat=ier)  fits%head%datamin
     if (ier.ne.0) call werror(comm,inerror,debug)
  elseif (comm.eq.'HISTORY') then
     continue
     !
  elseif (comm.eq.'GROUP') then
     ! Just in case, check for UVFITS...
     call class_message(seve%e,rname,'UVFITS format not supported.')
     error = .true.
     return
  else
     found = .false.
  endif
  if (found)  return
  !
  if (fits%head%desc%kind.ne.'BINTABLE')  return
  !
  ! Binary table specific
  if (comm(1:5).eq.'TFORM') then
    read(comm(6:8),'(BN,I3.1)',iostat=ier) i
    if (ier.ne.0) then
      call werror(comm,inerror,debug)
    elseif (i.le.fits%cols%desc%ncols) then
      fits%cols%desc%tform(i) = gfits_unquote(argu)
    endif
    !
  elseif (comm(1:5).eq.'TTYPE') then
    read(comm(6:8),'(BN,I3.1)',iostat=ier) i
    if (ier.ne.0) then
      call werror(comm,inerror,debug)
    elseif (i.le.fits%cols%desc%ncols) then
      fits%cols%desc%ttype(i) = gfits_unquote(argu)
    endif
    !
  elseif (comm(1:5).eq.'TUNIT') then
    read(comm(6:8),'(BN,I3.1)',iostat=ier) i
    if (ier.ne.0) then
      call werror(comm,inerror,debug)
    elseif (i.le.fits%cols%desc%ncols) then
      fits%cols%desc%tunit(i) = gfits_unquote(argu)
    endif
    !
  elseif (comm.eq.'MAXIS') then
    read(argu,1002,iostat=ier) xxx
    if (ier.ne.0) call werror(comm,inerror,debug)
    fits%head%desc%naxis = nint(xxx)
    if (fits%head%desc%naxis.gt.maxis) then
      write(mess,'(2(A,I0))') 'Too many axes in FITS file: ',  &
        fits%head%desc%naxis,', truncated to ',maxis
      call class_message(seve%w,rname,mess)
      fits%head%desc%naxis = maxis
    endif
    !
  elseif (comm(1:5).eq.'MAXIS') then
    read(comm(6:8),'(BN,I3.1)',iostat=ier) i
    if (ier.eq.0) read(argu,1002,iostat=ier) xxx
    if (ier.ne.0) call werror(comm,inerror,debug)
    fits%head%desc%axis(i) = nint(xxx)
    !
  elseif (comm.eq.'THEAP') then
    read(argu, 1002, iostat = ier) xxx
    fits%head%desc%theap = nint(xxx)
    !
  endif
  !
  ! 20 digits is less than the intrinsic precision of Vax or IEEE REAL*8, but
  ! the FITS standard specifies that real number should be right (?) justified
  ! in columns 11 to 30.
1002 format(f20.0)
end subroutine fits_read_header_card
!
subroutine fits_convert_header_card(comm,argu,obs,inerror,error)
  use phys_const
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>fits_convert_header_card
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Decode the card (name+value) into the obs structure
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: comm     !
  character(len=*),  intent(in)    :: argu     !
  type(observation), intent(inout) :: obs      !
  integer(kind=4),   intent(inout) :: inerror  ! Minor errors to be counted
  logical,           intent(inout) :: error    ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='FITS>READ>HEADER>CARD'
  real(kind=8) :: xxx
  logical, parameter :: debug=.true.
  logical :: done_instrume
  integer(kind=4) :: i,i1,i2,id,im,iy,ier,iind
  !
  done_instrume = .false.
  if (comm.eq.'RADESYS') then
     select case (gfits_unquote(argu))
     case ('ICRS')
       obs%head%pos%system  = type_ic
       obs%head%pos%equinox = equinox_null
     case ('FK5')
       obs%head%pos%system  = type_eq
       obs%head%pos%equinox = 2000.
     case ('FK4')
       obs%head%pos%system  = type_eq
       obs%head%pos%equinox = 1950.
     case default
       call class_message(seve%w,rname,'RADESYS = '//gfits_unquote(argu)//' ignored')
     end select
  elseif (comm.eq.'OBSTIME'.or.comm.eq.'EXPOSURE') then
     read(argu,1002,iostat=ier) obs%head%gen%time
     if (ier.ne.0) call werror(comm,inerror,debug)
  elseif (comm.eq.'TSYS') then
     read(argu,1002,iostat=ier) obs%head%gen%tsys
     if (ier.ne.0) call werror(comm,inerror,debug)
  elseif (comm.eq.'RESTFREQ' .or. comm.eq.'RESTFRQ') then
     read(argu,1002,iostat=ier) xxx
     if (ier.ne.0) call werror(comm,inerror,debug)
     obs%head%spe%restf = xxx*1.d-6
  elseif (comm.eq.'IMAGFREQ') then
     read(argu,1002,iostat=ier) xxx
     if (ier.ne.0) call werror(comm,inerror,debug)
     obs%head%spe%image = xxx*1.d-6
  elseif (comm.eq.'VLSR') then
     read(argu,1002,iostat=ier) xxx
     if (ier.ne.0) call werror(comm,inerror,debug)
     obs%head%spe%voff = xxx*1.d-3
     obs%head%spe%vtype = vel_lsr
  elseif (comm.eq.'SPECSYS') then
     call fits_decode_specsys(gfits_unquote(argu),obs%head%spe%vtype)
  elseif (comm.eq.'VELREF') then
     read(argu,1002,iostat=ier) xxx
     call fits_decode_velref(nint(xxx),obs%head%spe%vconv,obs%head%spe%vtype)
  elseif (comm.eq.'VELOCITY') then
     read(argu,1002,iostat=ier) xxx
     if (ier.ne.0) call werror(comm,inerror,debug)
     obs%head%spe%voff = xxx*1.d-3
  elseif (comm(1:4).eq.'VELO') then
     read(argu,1002,iostat=ier) xxx
     if (ier.ne.0) call werror(comm,inerror,debug)
     obs%head%spe%voff = xxx*1.d-3
     if (comm(5:8).eq.'-LSR') then
        obs%head%spe%vtype = vel_lsr
     elseif (comm(5:8).eq.'-HEL') then
        obs%head%spe%vtype = vel_hel
     elseif (comm(5:8).eq.'-OBS'.or.comm(5:8).eq.'-TOP') then
        obs%head%spe%vtype = vel_obs
     elseif (comm(5:8).eq.'-EAR'.or.comm(5:8).eq.'-GEO') then
        obs%head%spe%vtype = vel_ear
     else
        call class_message(seve%w,rname,'Unknown velocity referential '//comm(5:8))
     endif
  elseif (comm.eq.'DELTAV') then
     read(argu,1002,iostat=ier) xxx
     if (ier.ne.0) call werror(comm,inerror,debug)
     obs%head%spe%vres = xxx*1.d-3
     !
     !
  ! Calibration
  elseif (comm.eq.'TAU-ATM') then
     read(argu,1002,iostat=ier) obs%head%gen%tau
     if (ier.ne.0) call werror(comm,inerror,debug)
  elseif (comm.eq.'MH2O') then
     read(argu,1002,iostat=ier) obs%head%cal%h2omm
     if (ier.ne.0) call werror(comm,inerror,debug)
  elseif (comm.eq.'TOUTSIDE'.or.comm.eq.'TAMBIENT') then
     read(argu,1002,iostat=ier) obs%head%cal%tamb
     if (ier.ne.0) call werror(comm,inerror,debug)
  elseif (comm.eq.'PRESSURE') then
     read(argu,1002,iostat=ier) obs%head%cal%pamb
     if (ier.ne.0) call werror(comm,inerror,debug)
  elseif (comm.eq.'TCHOP'.or.comm.eq.'THOT') then
     read(argu,1002,iostat=ier) obs%head%cal%tchop
     if (ier.ne.0) call werror(comm,inerror,debug)
  elseif (comm.eq.'TCOLD') then
     read(argu,1002,iostat=ier) obs%head%cal%tcold
     if (ier.ne.0) call werror(comm,inerror,debug)
  elseif (comm.eq.'ELEVATIO') then
     read(argu,1002,iostat=ier) obs%head%gen%el
     if (ier.ne.0) call werror(comm,inerror,debug)
     obs%head%gen%el = obs%head%gen%el*pi/180.d0
  elseif (comm.eq.'AZIMUTH') then
     read(argu,1002,iostat=ier) obs%head%gen%az
     if (ier.ne.0) call werror(comm,inerror,debug)
     obs%head%gen%az = obs%head%gen%az*pi/180.d0
  elseif (comm.eq.'GAINIMAG') then
     read(argu,1002,iostat=ier) obs%head%cal%gaini
     if (ier.ne.0) call werror(comm,inerror,debug)
     obs%head%presec(class_sec_cal_id) = .true.
  elseif (comm.eq.'BEAMEFF') then
     read(argu,1002,iostat=ier) obs%head%cal%beeff
     if (ier.ne.0) call werror(comm,inerror,debug)
     obs%head%presec(class_sec_cal_id) = .true.
  elseif (comm.eq.'FORWEFF'.or.comm.eq.'ETAFSS') then
     read(argu,1002,iostat=ier) obs%head%cal%foeff
     if (ier.ne.0) call werror(comm,inerror,debug)
     obs%head%presec(class_sec_cal_id) = .true.
     !
  ! Resolution
  elseif (comm.eq.'BMAJ') then
     read(argu,1002,iostat=ier) obs%head%res%major
     if (ier.ne.0) call werror(comm,inerror,debug)
     obs%head%res%major = obs%head%res%major*rad_per_deg
     obs%head%presec(class_sec_res_id) = .true.
  elseif (comm.eq.'BMIN') then
     read(argu,1002,iostat=ier) obs%head%res%minor
     if (ier.ne.0) call werror(comm,inerror,debug)
     obs%head%res%minor = obs%head%res%minor*rad_per_deg
     obs%head%presec(class_sec_res_id) = .true.
  elseif (comm.eq.'BPA') then
     read(argu,1002,iostat=ier) obs%head%res%posang
     if (ier.ne.0) call werror(comm,inerror,debug)
     obs%head%res%posang = obs%head%res%posang*rad_per_deg
     obs%head%presec(class_sec_res_id) = .true.
     !
  ! Miscellaneous useful
  elseif (comm.eq.'SCAN-NUM'.or.comm.eq.'SCAN_NUM'.or.comm.eq.'SCAN') then
     read(argu,1002,iostat=ier) xxx
     if (ier.ne.0) call werror(comm,inerror,debug)
     obs%head%gen%scan = nint(xxx,kind=8)
  elseif (comm.eq.'SUBSCAN') then
     read(argu,1002,iostat=ier) xxx
     if (ier.ne.0) call werror(comm,inerror,debug)
     obs%head%gen%subscan = nint(xxx)
  elseif (comm.eq.'LINE') then
     obs%head%spe%line = gfits_unquote(argu)
     call sic_upper(obs%head%spe%line)
  elseif (comm.eq.'OBJECT') then
     obs%head%pos%sourc = gfits_unquote(argu)
     call sic_upper(obs%head%pos%sourc)
  elseif (comm.eq.'INSTRUME') then  ! Has precedence over TELESCOP
     obs%head%gen%teles = gfits_unquote(argu)
     call sic_upper(obs%head%gen%teles)
     done_instrume = .true.
  elseif (comm.eq.'TELESCOP' .and. .not.done_instrume) then  ! Fallback if INSTRUME missing
     obs%head%gen%teles = gfits_unquote(argu)
     call sic_upper(obs%head%gen%teles)
  elseif (comm.eq.'EPOCH'.or.comm.eq.'EQUINOX') then
     read(argu,1002,iostat=ier) xxx
     if (ier.ne.0) call werror(comm,inerror,debug)
     obs%head%pos%equinox=xxx
  elseif (comm.eq.'DATE-OBS'.or.comm.eq.'DATE_OBS') then
     i1 = index(argu,'''')+1
     i2 = index(argu(i1:),'''')-2+i1
     ! make provision for old DATE-OBS format:
     iind = index(argu(i1:i2),'/')
     if (iind.eq.0) then         ! new ISO date format
        read(argu(i1:i2),1004,iostat=ier) iy,im,id
        if (ier.ne.0) call werror(comm,inerror,debug)
        ! In that case, decode also what should be the UT OBS at end:
        iind=index(argu(i1:i2),'T')
        if (iind.ne.0)then
           iind=iind+i1
           call sic_decode(argu(iind:i2),obs%head%gen%ut,24,error)
           if (error)then
              call werror(comm,inerror,debug)
              error=.false.
           endif
        endif
     else
        read(argu(i1:i2),1003,iostat=ier) id,im,iy
        if (ier.ne.0) call werror(comm,inerror,debug)
        iy = 1900+iy
     endif
     call gag_datj(id,im,iy,obs%head%gen%dobs)
  elseif (comm.eq.'DATE-RED'.or.comm.eq.'DATE'.or.comm.eq.'DATE_RED') then
     i1 = index(argu,'''')+1
     i2 = index(argu(i1:),'''')-2+i1
     ! make provision for old DATE-OBS format:
     iind = index(argu(i1:i2),'/')
     if (iind.eq.0) then         ! new ISO date format
        read(argu(i1:i2),1004,iostat=ier) iy,im,id
        if (ier.ne.0) call werror(comm,inerror,debug)
        ! (we ignore the trailing time)
     else
        read(argu(i1:i2),1003,iostat=ier) id,im,iy
        if (ier.ne.0) call werror(comm,inerror,debug)
        iy = 1900+iy
     endif
     call gag_datj(id,im,iy,obs%head%gen%dred)
  elseif (comm.eq.'UT'.or.comm.eq.'UTC') then
     i1 = index(argu,'''')+1
     if (i1.gt.1) then          ! HH:MM:SS.S format
        i2 = index(argu(i1:),'''')-2+i1
        call sic_decode(argu(i1:i2),obs%head%gen%ut,24,error)
        if (error)  return
     else                       ! Time in seconds
        read(argu,1002,iostat=ier) xxx
        if (ier.ne.0) call werror(comm,inerror,debug)
        obs%head%gen%ut = xxx*pi/12.d0/3600.d0
     endif
  elseif (comm.eq.'LST') then
     i1 = index(argu,'''')+1
     if (i1.gt.1) then          ! HH:MM:SS.S format
        i2 = index(argu(i1:),'''')-2+i1
        call sic_decode(argu(i1:i2),obs%head%gen%st,24,error)
        if (error)  return
     else
        read(argu,1002,iostat=ier) xxx
        if (ier.ne.0) call werror(comm,inerror,debug)
        obs%head%gen%st = xxx*pi/12.d0/3600.d0
     endif
     !
     !
  ! Frequency switched information
  elseif (comm.eq.'NPHASE') then
     read(argu,1002,iostat=ier) xxx
     if (ier.ne.0) call werror(comm,inerror,debug)
     obs%head%swi%nphas = nint(xxx)
     if (obs%head%swi%nphas.ge.1) then
        obs%head%swi%swmod = mod_freq
        obs%head%presec(class_sec_swi_id) = .true.
     endif
  elseif (comm(1:6).eq.'DELTAF') then
     read(comm(7:7),'(I1)',iostat=ier) i
     if (ier.ne.0) then
       call werror(comm,inerror,debug)
     elseif (i.gt.0) then  ! sometimes DELTAF is used as AXINC.
        read(argu,1002,iostat=ier) xxx
        if (ier.ne.0) call werror(comm,inerror,debug)
        obs%head%swi%decal(i) = xxx*1.d-6
     endif
  elseif (comm(1:5).eq.'PTIME') then
     read(comm(6:6),'(I1)',iostat=ier) i
     if (ier.eq.0) read(argu,1002,iostat=ier) xxx
     if (ier.ne.0) call werror(comm,inerror,debug)
     obs%head%swi%duree(i) = xxx
  elseif (comm(1:6).eq.'WEIGHT') then
     read(comm(7:7),'(I1)',iostat=ier) i
     if (ier.eq.0) read(argu,1002,iostat=ier) xxx
     if (ier.ne.0) call werror(comm,inerror,debug)
     obs%head%swi%poids(i) = xxx
     !
     !
  ! HIFI-specific (not used)
  elseif (comm(1:6).eq.'SEQNUMBE') then
     read(argu, 1002, iostat = ier) xxx
     if(ier.ne.0) call werror(comm, inerror, debug)
     !
  elseif (comm.eq.'GROUP') then
     ! Just in case, check for UVFITS...
     call class_message(seve%e,rname,'UVFITS format not supported.')
     error = .true.
     return
  endif
  !
  ! 20 digits is less than the intrinsic precision of Vax or IEEE REAL*8, but
  ! the FITS standard specifies that real number should be right (?) justified
  ! in columns 11 to 30.
1002 format(f20.0)
1003 format(i2,1x,i2,1x,i2)
1004 format(i4,1x,i2,1x,i2)
end subroutine fits_convert_header_card
!
subroutine werror(comm,count,warn)
  use gbl_message
  use classcore_interfaces, except_this=>werror
  !-------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  ! Print an error message and increment error counter
  !-------------------------------------------------------------------
  character(len=*), intent(in)    :: comm
  integer,          intent(inout) :: count
  logical,          intent(in)    :: warn
  !
  count = count+1
  if (warn) call class_message(seve%e,'TOCLASS','Error decoding '//comm)
end subroutine werror
!
subroutine fits_read_header_desc(fits,check,invalid,error)
  use gbl_message
  use gkernel_interfaces
  use classcore_interfaces, except_this=>fits_read_header_desc
  use classfits_types
  !----------------------------------------------------------------------
  ! @ private
  ! Retrieve really essential information from a header:
  !   data size, and type of FITS format (basic FITS, or binary table).
  !----------------------------------------------------------------------
  type(classfits_t), intent(inout) :: fits     !
  logical,           intent(in)    :: check    ! Echo card images on terminal.
  logical,           intent(out)   :: invalid  ! Validity status.
  logical,           intent(inout) :: error    ! Error status.
  ! Local
  character(len=*), parameter :: proc='FITS'
  integer(kind=4) :: i
  real(kind=8) :: xxx
  character(len=8) :: comm
  character(len=70) :: argu
  character(len=message_length) :: chain
  !
  ! Basic or Binary Table ?
  !
  ! The very first line of a header must contain either the keyword 'SIMPLE'
  ! (primary header) or the keyword 'XTENSION' (additional headers).
  ! If SIMPLE has the value 'T', then we may have a BASIC FITS. If XTENSION has
  ! the value 'BINTABLE', then we may have data in a Binary Table.
  !
  invalid = .false.
  fits%head%desc%kind = ' '
  call gfits_get(comm, argu, check, error)
  if (error) goto 99
  !
  if (comm.eq.'SIMPLE') then
     ! Primary HDU
     if (argu(1:20).eq.'                   T') then
        fits%head%desc%kind = 'BASIC'
        call class_message(seve%i,proc,'Found the Basic HDU.')
     else
        call class_message(seve%e,proc,'Not a standard FITS file.')
        goto 99
     endif
     !
  elseif (comm.eq.'XTENSION') then
     ! Extension
     !
     if (argu(2:9).eq.'BINTABLE'.or.argu(2:9).eq.'A3DTABLE') then
        ! Binary table found.
        fits%head%desc%kind = 'BINTABLE'
        write(chain,'(A,I0)')  'Found a Binary Table in extension #',fits%head%desc%num
        call class_message(seve%i,proc,chain)
        if (argu(2:9).eq.'A3DTABLE') then
           call class_message(seve%w,proc,'Extension name A3DTABLE is obsolete.')
        endif
!!     else if (argu(2:6).eq.'IMAGE') then 
!!       fits%head%desc%kind = 'BASIC'
!!       call class_message(seve%i,proc,'Found an Image extension.')
     else
        call class_message(seve%w,proc,'Unknown extension: '//trim(argu)//', Skipping')
        invalid = .true.
        error = .false.
        return
     endif
  else
     call class_message(seve%w,proc,'Bad positioning '//COMM//', Quitting')
     invalid = .true.
     error = .true.
     return
  endif
  !
  ! Read the mandatory keywords.
  !
  ! First: BITPIX.
  call gfits_get(comm,argu,check,error)
  if (error) goto 99
  if (comm.ne.'BITPIX') then
     write(chain,*) 'Unexpected keyword ',comm,'. BITPIX expected.'
     call class_message(seve%e,proc,chain)
     goto 99
  endif
  read(argu,1002) xxx
  fits%head%desc%nbit = int(xxx)
  !
  ! Second: NAXIS.
  call gfits_get(comm,argu,check,error)
  if (error) goto 99
  if (comm.ne.'NAXIS') then
     write(chain,*) 'Unexpected keyword ',comm,'. NAXIS expected.'
     call class_message(seve%e,proc,chain)
     goto 99
  endif
  read(argu,1002) xxx
  fits%head%desc%naxis = int(xxx)
  !
  if (fits%head%desc%naxis.gt.maxis) then
     ! maxis is a parameter declared and defined in tofits.f90.
     write(chain,*) 'Too many dimensions: ',fits%head%desc%naxis,' >',maxis
     call class_message(seve%e,proc,chain)
     goto 99
  endif
  !
  if (fits%head%desc%kind.eq.'BINTABLE' .and. fits%head%desc%naxis.ne.2) then
     write(chain,*) 'NAXIS forced to 2 (instead of ',fits%head%desc%naxis,  &
       ') as required by the BINTABLE standard.'
     call class_message(seve%i,proc,chain)
     fits%head%desc%naxis = 2
  endif
  !
  ! Check the BITPIX value, only if there are data.
  if (fits%head%desc%naxis.gt.0) then
     if (fits%head%desc%kind.eq.'BASIC') then
        if (fits%head%desc%nbit.ne.16 .and.  &
            fits%head%desc%nbit.ne.32 .and.  &
            fits%head%desc%nbit.ne.-32) then
           write(chain,'(A,I0,A)') 'Cannot handle ',fits%head%desc%nbit,' bits'
           call class_message(seve%e,proc,chain)
           goto 99
        endif
     elseif (fits%head%desc%kind.eq.'BINTABLE') then
        if (fits%head%desc%nbit.ne.8) then
           call class_message(seve%e,proc,'BITPIX must equal 8 for Binary Tables.')
           goto 99
        endif
     endif
  endif
  !
  ! Next NAXIS lines: read axisn, axis dimensions.
  do i=1,fits%head%desc%naxis
     call gfits_get(comm,argu,check,error)
     if (error) goto 99
     read(argu,1002) xxx
     fits%head%desc%axis(i) = nint(xxx)
  enddo
  !
  ! Extract and compute some information from the values of AXISn.
  if (fits%head%desc%naxis.gt.0) then
     if (fits%head%desc%kind.eq.'BASIC') then
        call check_axis(fits%head%desc%axis,fits%head%desc%naxis,  &
          fits%head%desc%ndata,fits%head%desc%main_axis,error)
        if (error) goto 99
        ! Check axis must not be done on the std axis for a Bintable, but
        ! on the width of columns. fits_chopbuf_1header reads these widths.
     elseif (fits%head%desc%kind.eq.'BINTABLE') then
        ! None of the 2 Bintable AXIS represent the number of channels, so
        ! we put naxis to 0 in order to avoid check_axis to try to find
        ! an axis corresponding to the spectrum.
        fits%head%desc%naxis = 0
     endif
  else
     fits%head%desc%ndata = 0
     fits%head%desc%main_axis = 0
     fits%head%desc%kind = 'BASICNODATA' ! No data at all.
  endif
  !
  ! Specific to Binary Tables.
  !
  if (fits%head%desc%kind.eq.'BINTABLE') then
     !
     ! Table size.
     fits%cols%desc%lrow  = fits%head%desc%axis(1) ! Length of rows.
     fits%cols%desc%nrows = fits%head%desc%axis(2) ! Number of rows.
     if (fits%cols%desc%lrow.le.0) then
        call class_message(seve%e,proc,'Rows are empty.')
        goto 99
     endif
     if (fits%cols%desc%nrows.le.0) then
        call class_message(seve%e,proc,'No row in table.')
        goto 99
     endif
     !
     ! PCOUNT.
     call gfits_get(comm,argu,check,error)
     if (error) goto 99
     if (comm.ne.'PCOUNT') then
        write(chain,*) 'Unexpected keyword ',comm,'. PCOUNT expected.'
        call class_message(seve%e,proc,chain)
        goto 99
     endif
     read(argu,1002) xxx
     fits%head%desc%lheap = nint(xxx) ! PCOUNT = length of Heap area
     !
     ! GCOUNT.
     call gfits_get(comm,argu,check,error)
     if (error) goto 99
     if (comm.ne.'GCOUNT') then
        write(chain,*) 'Unexpected keyword ',comm,'. GCOUNT expected.'
        call class_message(seve%e,proc,chain)
        goto 99
     endif
     read(argu,1002) xxx
     if (xxx.ne.1) then
        call class_message(seve%i,proc,'GCOUNT forced to 1, as required by std.')
     endif
     !
     ! Number of columns.
     call gfits_get(comm,argu,check,error)
     if (error) goto 99
     if (comm.ne.'TFIELDS') then
        write(chain,*) 'Unexpected keyword f',comm,'. TFIELDS expected.'
        call class_message(seve%e,proc,chain)
        goto 99
     endif
     read(argu,1002) xxx
     fits%cols%desc%ncols = nint(xxx)
     if (fits%cols%desc%ncols.gt.mcols) then
        write(chain,'(A,I0,A,I0)') 'Too many columns ',fits%cols%desc%ncols,' truncated to ',mcols
        call class_message(seve%w,proc,chain)
        fits%cols%desc%ncols = mcols
     endif
     !
     ! Xtension name
     call gfits_get(comm,argu,check,error)
     if (error) goto 99
     if (comm.ne.'EXTNAME') then
       ! Is it en error?
       continue
       ! write(chain,*) 'Unexpected keyword ',comm,'. EXTNAME expected.'
       ! call class_message(seve%e,proc,chain)
       ! goto 99
     else
       call class_message(seve%i,proc,'Reading xtension '//trim(argu))
     endif
  endif
  !
  ! Basic FITS file.
  !

  !  if (ndata.gt.0) then
  !fits%head%desc%kind = 'BASIC'
  !call reallocate_obs(r,ndata,error)  ! Should be put outside this subR.
  !if (error) goto 99



  ! Binary table extensions.
  !
  ! No data in basic FITS file. Look for binary  table extensions.

  !  else
  !    call class_message(seve%i,'FITS','No data in FITS file. Looking for a binary table extension')
  !    comm = ' '
  !    argu = ' '
  !    ! Reading all headers and skipping the appropriate number of records would be
  !    ! more efficient. Obviously depends on what is on the tape though...
  !    do while (comm.ne.'XTENSION'.or.   &
  !        &     (argu(2:9).ne.'BINTABLE'.and.argu(2:9).ne.'A3DTABLE'))
  !      call gfits_flush_data(error)
  !      if (error) goto 99
  !      call gfits_get(comm,argu,check,error)
  !      if (error) goto 99
  !    enddo
  !    fits%head%desc%kind = 'BINTABLE'
  !    call class_message(seve%i,'fits_read_header_desc','Found a binary  table extension.')
  !    if (argu(2:9).eq.'A3DTABLE') then
  !      call class_message(seve%i,'FITS','Extension name A3DTABLE is obsolete')
  !    endif
  !  endif
  return
99 error = .true.
  return
  !
1002 format(f20.0)
end subroutine fits_read_header_desc
!
subroutine fits_read_bintable_header(fits,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use gkernel_interfaces
  use classcore_interfaces, except_this=>fits_read_bintable_header
  use classfits_types
  !----------------------------------------------------------------------
  ! @ private
  ! Try to decode the column description (for binary  tables). Decode
  ! FITS format of each column into a column width, a repeat count and
  ! a SIC format code.
  !----------------------------------------------------------------------
  type(classfits_t), intent(inout) :: fits
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: proc='TOCLASS'
  integer            :: icol
  character(len=100) :: smess
  character(len=20)  :: sformat
  integer            :: iformatlen
  integer            :: itypeposition
  character(len=1)   :: sdatatype
  character(len=20)  :: srepeatcount
  integer            :: irepeatcount
  integer            :: icolwidth
  !
  ! First offset for first column : 1.
  fits%cols%desc%addr(1) = 1
  !
  ! Process each column.
  do iCol=1,fits%cols%desc%ncols
     ! Process the format.
     ! Pick up the format string from fits%cols%desc%tform.
     sformat = fits%cols%desc%tform(icol)
     iformatlen = len(sformat)
     call sic_black(sformat,iformatlen) ! remove tabs and spaces.
     !
     ! Has a format been specified?
     if (iformatlen.eq.0) then
        write(smess,'(a,i3)') 'Format not specified for column',icol
        call class_message(seve%e,proc,smess)
        call class_message(seve%e,proc,'Unable to determine alignment of further columns')
        goto 99
     endif
     !
     ! Find the data type.
     itypeposition = scan(sformat,'LIJKAEDP') ! LXBIJAEDCMP
     if (itypeposition.gt.0) then
        ! Data type is found.
        ! Extract the data type from the format string.
        sdatatype = sformat(itypeposition : itypeposition)
        if (sdatatype.eq.'P') then
           sdatatype = sformat(itypeposition+1:itypeposition+1)
           fits%cols%desc%vararray(icol) = .true.
        else
           fits%cols%desc%vararray(icol) = .false.
        endif
        !
        ! Extract the repeat count.
        if (itypeposition.gt.1) then
           srepeatcount = sformat(1:itypeposition-1)
           read (srepeatcount,*,err=10) irepeatcount
        else
           srepeatcount = '1'
           irepeatcount =  1
        endif
        !
        fits%cols%desc%nitem(icol) = irepeatcount
        !
        ! Determine the column width and column datatype.
        if (sdatatype.eq.'A') then
           icolwidth    = fits%cols%desc%nitem(icol)
           fits%cols%desc%fmt(icol) = fits%cols%desc%nitem(icol)
           fits%cols%desc%nitem(icol)  = 1
        elseif (sdatatype.eq.'D') then  ! EEEI Real*8
           icolwidth    = fits%cols%desc%nitem(icol) * 8
           fits%cols%desc%fmt(icol) = eei_r8
        elseif (sdatatype.eq.'E') then  ! EEEI Real*4
           icolwidth    = fits%cols%desc%nitem(icol) * 4
           fits%cols%desc%fmt(icol) = eei_r4
        elseif (sdatatype.eq.'K') then  ! EEEI Integer*8
           icolwidth    = fits%cols%desc%nitem(icol) * 8
           fits%cols%desc%fmt(icol) = eei_i8
        elseif (sdatatype.eq.'J') then  ! EEEI Integer*4
           icolwidth    = fits%cols%desc%nitem(icol) * 4
           fits%cols%desc%fmt(icol) = eei_i4
        elseif (sdatatype.eq.'I') then  ! EEEI Integer*2
           icolwidth    = fits%cols%desc%nitem(icol) * 2
           fits%cols%desc%fmt(icol) = eei_i2
        elseif (sdatatype.eq.'L') then  ! Logical*1
           icolwidth    = fits%cols%desc%nitem(icol)
           fits%cols%desc%fmt(icol) = eei_l
        elseif (sdatatype.eq.'L') then
           ! we never come here (PHB ??????)
           icolwidth    = (fits%cols%desc%nitem(icol) + 7) / 8
           fits%cols%desc%fmt(icol) = fmt_un
        else
           write(smess,'(A,I0,2A)')  'TFORM of column #',icol,' is not supported: ',sformat
           call class_message(seve%e,proc,smess)
           call class_message(seve%e,proc,'Unable to determine alignment of further columns')
           goto 99
        endif
        !
        ! Special case: columns with variable size arrays.
        if (fits%cols%desc%vararray(iCol)) then
           iColWidth = fits%cols%desc%nitem(iCol) * 8 ! Always 8 bytes for a P column.
        endif
     else
        write(smess,'(A,I0,2A)')  'TFORM of column #',icol,' is not supported: ',sformat
        call class_message(seve%e,proc,smess)
        call class_message(seve%e,proc,'Unable to determine alignment of further columns')
        goto 99
     endif
     !
     ! Determine next column's offset into the data block.
     fits%cols%desc%addr(iCol + 1) = fits%cols%desc%addr(iCol) + iColWidth
     !
     ! Process the type.
     !
     ! Read the column type (ie its name) from fits%cols%desc%ttype.
     !
     ! Try to interpret column's type. Should probably be replaced with a
     ! hashing search through a vocabulary in the production version.
     ! Vocabulary production can probably be automated (from STRUCTURE.RNO).
     !
     call class_fits_decode_colpos(fits%cols%desc%ttype(icol),icol,  &
       fits%ishcss,fits%cols%posi,error)
     if (error)  return
  enddo
  return
99 error = .true.
  return
10 write(6,*) 'Error decoding ', sFormat(1:iTypePosition-1)
  return
end subroutine fits_read_bintable_header
!
subroutine class_fits_decode_colpos(stype_in,icol,hifi,colpos,error)
  use gbl_message
  use classcore_interfaces, except_this=>class_fits_decode_colpos
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Set column position in the input structure
  !---------------------------------------------------------------------
  character(len=*),                   intent(in)    :: stype_in  ! Column TTYPE string
  integer(kind=4),                    intent(in)    :: icol      ! Associated column number
  logical,                            intent(in)    :: hifi      ! Search also for HIFI columns?
  type(classfits_column_positions_t), intent(inout) :: colpos    ! Updated column positions list
  logical,                            intent(inout) :: error     ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='FITS'
  integer(kind=4) :: itypelen,j
  character(len=20) :: stype
  logical :: found
  character(len=message_length) :: mess
  !
  stype = stype_in
  itypelen = len(stype)
  call sic_black(stype,itypelen)  ! Suppress all blanks
  call sic_upper(stype)
  !
  ! Has a type been specified?
  if (itypelen.eq.0) then
    write(mess,'(A,I0,A)')  'TYPE not specified for column #', iCol,', ignored'
    call class_message(seve%w,rname,mess)
    return
  endif
  !
  found = .true.
  if (stype.eq.'MAXIS') then
    colpos%naxis = iCol
    !
  elseif (stype(1:5).eq.'MAXIS') then
    read(stype(6:8),'(BN,I3.1)',err=20) j
    colpos%axis(j) = iCol
    !
  elseif (sType.eq.'MATRIX') then
    colpos%matrix = iCol
    !
  elseif (sType(1:6).eq.'MATRIX') then
    read(sType(7:8),'(BN,I2.1)',err=20) j
    if (j.eq.1.or.j.eq.0) then
      colpos%matrix = iCol
    else
      call class_message(seve%e,rname,'Only one data matrix supported. Others ignored.')
    endif
    !
  elseif (sType.eq.'SPECTRUM' .or.  &
          sType.eq.'SERIES'   .or.  &
          sType.eq.'DATA') then
    colpos%matrix = iCol
    !
  elseif (sType.eq.'CHANNELS') then
    colpos%channels = iCol
    !
  elseif (sType(1:5).eq.'CRVAL') then
    read(sType(6:8),'(BN,I3.1)',err=20) j
    colpos%crval(j) = iCol
    !
  elseif (sType(1:5).eq.'CDELT') then
    read(sType(6:8),'(BN,I3.1)',err=20) j
    colpos%cdelt(j) = iCol
    !
  elseif (sType(1:5).eq.'CRPIX') then
    read(sType(6:8),'(BN,I3.1)',err=20) j
    colpos%crpix(j) = iCol
    !
  elseif (sType(1:5).eq.'CROTA') then
    read(sType(6:8),'(BN,I3.1)',err=20) j
    colpos%crota(j) = iCol
    !
  elseif (sType(1:5).eq.'CTYPE') then
    read(sType(6:8),'(BN,I3.1)',err=20) j
    colpos%axtyp(j) = iCol
    !
  elseif (stype.eq.'SCAN-NUM'  .or.  &
          sType.eq.'SCAN'      .or.  &
          sType.eq.'SCAN_NUM') then
    colpos%scan = iCol
    !
  elseif (sType.eq.'SUBSCAN') then
    colpos%subscan = iCol
    !
  elseif (sType.eq.'OBJECT') then
    colpos%object = iCol
    !
  elseif (sType.eq.'LINE') then   ! Missing in UNIPOPS output??
    colpos%line = iCol
    !
  elseif (sType.eq.'TELESCOP') then
    colpos%telesc = iCol
    !
  elseif (sType.eq.'OBSTIME' .or.  &
          sType.eq.'EXPOSURE') then
    colpos%time = iCol
    !
  elseif (sType.eq.'TSYS') then
    colpos%tsys = iCol
    !
  elseif (sType.eq.'WAVE') then
    colpos%wave = iCol
    !
  elseif (sType.eq.'RESTFREQ' .or.  &
          sType.eq.'RESTFRQ') then
    colpos%restfreq = iCol
    !
  elseif (sType.eq.'IMAGFREQ') then
    colpos%imagfreq = iCol
    !
  elseif (sType.eq.'VELOCITY') then
    colpos%velocity = iCol
    !
  elseif (sType.eq.'SPECSYS') then
    colpos%specsys = iCol
    !
  elseif (sType.eq.'VELREF') then
    colpos%velref = iCol
    !
  elseif (sType.eq.'VELDEF') then
    colpos%veldef = iCol
    !
  elseif (sType.eq.'DELTAV' .or.  &
          sType.eq.'DELTAVEL') then
    colpos%deltav = iCol
    !
  elseif (sType.eq.'NPHASE') then
    colpos%nphase = iCol
    !
  elseif (sType(1:6).eq.'DELTAF') then
    read(sType(7:8),'(BN,I2.1)',err=20) j
    colpos%deltaf(j) = iCol
    !
  elseif (sType(1:5).eq.'PTIME') then
    read(sType(7:8),'(BN,I3.1)',err=20) j
    colpos%ptime(j) = iCol
    !
  elseif (sType(1:7).eq.'WEIGHT_') then
    ! Avoid troubles between the one below and the HIFI specific one.
    ! Decoded at bottom.
    found = .false.
    !
  elseif (sType(1:6).eq.'WEIGHT') then
    read(sType(7:8),'(BN,I2.1)',err=20) j
    colpos%weight(j) = iCol
    !
  elseif (sType.eq.'TAU-ATM' .or.  &
          sType.eq.'TAU_ATM') then
    colpos%tau_atm = iCol
    !
  elseif (sType.eq.'MH2O') then
    colpos%H2Omm = iCol
    !
  elseif (sType.eq.'PRESSURE') then
    colpos%pamb = iCol
    !
  elseif (sType.eq.'TOUTSIDE' .or.  &
          stype.eq.'TAMBIENT') then
    colpos%tamb = iCol
    !
  elseif (sType.eq.'TCHOP'.or.sType.eq.'THOT') then
    colpos%tchop = iCol
    !
  elseif (sType.eq.'TCOLD') then
    colpos%tcold = iCol
    !
  elseif (sType.eq.'ELEVATIO') then
    colpos%elevatio = iCol
    !
  elseif (sType.eq.'AZIMUTH') then
    colpos%azimuth = iCol
    !
  elseif (sType.eq.'GAINIMAG') then
    colpos%gainimag = iCol
    !
  elseif (sType.eq.'BEAMEFF') then
    colpos%beameff = iCol
    !
  elseif (sType.eq.'FORWEFF' .or.  &
          sType.eq.'ETAFSS') then
    colpos%forweff = iCol
    !
  elseif (sType.eq.'EPOCH' .or.  &
          sType.eq.'EQUINOX') then
    colpos%equinox = iCol
    !
  elseif (sType.eq.'DATE-OBS' .or.  &
          sType.eq.'DATE_OBS') then
    colpos%dobs = iCol
    !
  elseif (sType.eq.'DATE-RED' .or.  &
          sType.eq.'DATE'     .or.  &
          sType.eq.'DATE_RED') then
    colpos%dred = iCol
    !
  elseif (sType.eq.'UT' .or.  &
          sType.eq.'UTC') then
    colpos%ut = iCol
    !
  elseif (sType.eq.'LST') then
    colpos%lst = iCol
    !
  elseif (sType.eq.'DATAMAX') then
    colpos%datamax = iCol
    !
  elseif (sType.eq.'DATAMIN') then
    colpos%datamin = iCol
    !
  elseif (sType.eq.'BMAJ') then
    colpos%bmaj = iCol
    !
  elseif (sType.eq.'BMIN') then
    colpos%bmin = iCol
    !
  elseif (sType.eq.'BPA') then
    colpos%bpa = iCol
    !
  else
    found = .false.
  endif
  if (found)  return
  !
  ! If FITS is HCSS-FITS, no warning. There are many columns we do not
  ! expect to read in there.
  if (hifi)  return
  !
  write(mess,'(A,I0,2A)')  'TTYPE of column #',icol,' is not supported: ',stype
  call class_message(seve%w,rname,mess)
  return
  !
  ! Errors
20 write(mess,'(A,I0,2A)') 'Error decoding TTYPE of colum #',icol,': ',trim(stype)
  call class_message(seve%e,rname,mess)
  error = .true.
  return
end subroutine class_fits_decode_colpos
!
subroutine check_axis(axis,naxis,ndata,main,error)
  use gbl_message
  use classcore_interfaces, except_this=>check_axis
  !----------------------------------------------------------------------
  ! @ private
  ! CLASS-FITS Internal routine
  !
  ! Try to decode the column description (for binary  tables).
  ! recall: naxis=0 for binary table
  !----------------------------------------------------------------------
  integer, intent(in)  :: naxis       ! Number of axes.
  integer, intent(in)  :: axis(naxis) ! Axes dimensions.
  integer, intent(out) :: ndata       ! Number of data points.
  integer, intent(out) :: main        ! Main axis number.
  logical, intent(out) :: error       ! Logical error flag.
  ! Local
  character(len=*), parameter :: proc='CHECK_AXIS'
  integer :: i
  !
  error = .false.
  ndata = 0
  main  = 0
  do i=1,naxis
     if (axis(i).eq.0) then     ! No data, break.
        main = 0
        ndata = 0
        return
     elseif (axis(i).gt.1) then
        if (main.eq.0) then
           main = i
           ndata = axis(i)
        else
           call class_message(seve%e,proc,'Too many non dummy dimensions.')
!!           error = .true.
           return
        endif
     endif
  enddo
end subroutine check_axis
!
subroutine fits_chopbuf_1header(fits,row,lr,obs,error)
  use gildas_def
  use gbl_message
  use gbl_format
  use gbl_constant
  use phys_const
  use gkernel_interfaces
  use classcore_interfaces, except_this=>fits_chopbuf_1header
  use classfits_types
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  ! Chop the row buffer into individual header elements.
  ! Granularity is at the header level.
  !----------------------------------------------------------------------
  type(classfits_t), intent(inout) :: fits     !
  integer(kind=4),   intent(in)    :: lr       ! Row buffer length
  integer(kind=1),   intent(in)    :: row(lr)  ! Row buffer
  type(observation), intent(inout) :: obs      !
  logical,           intent(out)   :: error    ! Logical error flag
  ! Local
  character(len=*), parameter :: proc='FITS_CHOPBUF'
  integer(kind=address_length) iadd,ipnt
  character :: chain*12,mess*80
  integer(kind=4), parameter :: strlen=23
  character(len=strlen) :: date
  integer :: j,ichain(3),iind,ier,iy,im,id,lench,len
  real(kind=4) :: r4
  integer(kind=1) :: tmpbytes(strlen)
  integer(kind=4) :: scan
  !
  ! Axis information.
  if (fits%cols%posi%naxis.ne.0) then
     call get_item(fits%head%desc%naxis,1,fmt_i4,  &
       row(fits%cols%desc%addr(fits%cols%posi%naxis)),  &
       fits%cols%desc%fmt(fits%cols%posi%naxis),error)
     if (error) goto 99
  endif
  do j=1,fits%head%desc%naxis
     if (fits%cols%posi%axis(j).ne.0) then
        call get_item(fits%head%desc%axis(j),1,fmt_i4,  &
          row(fits%cols%desc%addr(fits%cols%posi%axis(j))),  &
          fits%cols%desc%fmt(fits%cols%posi%axis(j)),error)
        if (error) goto 99
        if (j.eq.fits%head%desc%main_axis) then
          ! Set the number of channel of this spectrum.  If MAXIS1 is
          ! in a column then it contains the number of channels to
          ! read from the MATRIX column, and that number of channels
          ! can be smaller than the width of MATRIX.  It's a way to
          ! store spectra of different sized in a rectangular array.
          fits%cols%desc%nitem(fits%cols%posi%matrix) = fits%head%desc%axis(j)
        endif
     endif
     if (fits%cols%posi%crval(j).ne.0) then
        !            type*,'crval ',j,' in column ',fits%cols%posi%crval(j)
        !            type*,'coladr: ',COLADDR(I_crval(J))
        call get_item(fits%head%desc%axval(j),1,fmt_r8,  &
          row(fits%cols%desc%addr(fits%cols%posi%crval(j))),  &
          fits%cols%desc%fmt(fits%cols%posi%crval(j)),error)
        if (error) goto 99
     endif
     if (fits%cols%posi%cdelt(j).ne.0) then
        !            type*,'cdelt',j,' in column ',fits%cols%posi%cdelt(j)
        !            type*,'coladr: ',COLADDR(I_cdelt(J))
        call get_item(fits%head%desc%axinc(j),1,fmt_r8,  &
          row(fits%cols%desc%addr(fits%cols%posi%cdelt(j))),  &
          fits%cols%desc%fmt(fits%cols%posi%cdelt(j)),error)
        if (error) goto 99
     endif
     if (fits%cols%posi%crpix(j).ne.0) then
        !            type*,'crpix',j,' in column ',fits%cols%posi%crpix(j)
        !            type*,'coladr: ',COLADDR(I_crpix(J))
        call get_item(fits%head%desc%axref(j),1,fmt_r8,  &
          row(fits%cols%desc%addr(fits%cols%posi%crpix(j))),  &
          fits%cols%desc%fmt(fits%cols%posi%crpix(j)),error)
        if (error) goto 99
     endif
     if (fits%cols%posi%crota(j).ne.0) then
        call get_item(fits%head%desc%rota(j),1,fmt_r8,  &
          row(fits%cols%desc%addr(fits%cols%posi%crota(j))),  &
          fits%cols%desc%fmt(fits%cols%posi%crota(j)),error)
        if (error) goto 99
     endif
     if (fits%cols%posi%axtyp(j).ne.0) then
        iadd = locstr(fits%head%desc%axtype(j))
        ipnt = bytpnt(iadd,row)
        call get_item(row(ipnt),1,len(fits%head%desc%axtype(j)),  &
          row(fits%cols%desc%addr(fits%cols%posi%axtyp(j))),  &
          fits%cols%desc%fmt(fits%cols%posi%axtyp(j)),error)
        if (error) goto 99
     endif
  enddo
  ! Other header information.
  if (fits%cols%posi%channels.ne.0) then
     call get_item(obs%cnchan,1,fmt_i4,row(fits%cols%desc%addr(fits%cols%posi%channels)),  &
       fits%cols%desc%fmt(fits%cols%posi%channels), error)
     if (error) goto 99
  endif
  if (fits%cols%posi%scan.ne.0) then
     ! NB: column SCAN still uses format J (I*4) in the writter. Use intermediate
     ! variable
     call get_item(scan,1,fmt_i4,row(fits%cols%desc%addr(fits%cols%posi%scan)),  &
       fits%cols%desc%fmt(fits%cols%posi%scan),error)
     if (error) goto 99
     obs%head%gen%scan = scan
     ! obs%head%gen%num = obs%head%gen%scan  ! Could also renumber them?
     ! Bertrand: we could but it makes everything messy since we can have
     ! several observations with the same scan and different subscan. They
     ! would not become several versions of a single observation.
  endif
  if (fits%cols%posi%subscan.ne.0) then
     call get_item(obs%head%gen%subscan,1,fmt_i4,row(fits%cols%desc%addr(fits%cols%posi%subscan)),  &
       fits%cols%desc%fmt(fits%cols%posi%subscan),error)
     if (error) goto 99
  endif
  if (fits%cols%posi%line.ne.0) then
     call chtoby('            ',tmpbytes,12)
     call get_item(tmpbytes,1,12,row(fits%cols%desc%addr(fits%cols%posi%line)),  &
       fits%cols%desc%fmt(fits%cols%posi%line),error)
     if (error) goto 99
     call bytoch(tmpbytes,obs%head%spe%line,12)
  endif
  if (fits%cols%posi%object.ne.0) then
     call chtoby('            ',tmpbytes,12)
     call get_item(tmpbytes,1,12,row(fits%cols%desc%addr(fits%cols%posi%object)),  &
       fits%cols%desc%fmt(fits%cols%posi%object),error)
     if (error) goto 99
     call bytoch(tmpbytes,obs%head%pos%sourc,12)
  endif
  if (fits%cols%posi%telesc.ne.0) then
     call chtoby('            ',tmpbytes,12)
     call get_item(tmpbytes,1,12,row(fits%cols%desc%addr(fits%cols%posi%telesc)),  &
       fits%cols%desc%fmt(fits%cols%posi%telesc),error)
     if (error) goto 99
     call bytoch(tmpbytes,obs%head%gen%teles,12)
  endif
  if (fits%cols%posi%time.ne.0) then
     call get_item(obs%head%gen%time,1,fmt_r4,row(fits%cols%desc%addr(fits%cols%posi%time)),  &
       fits%cols%desc%fmt(fits%cols%posi%time),error)
     if (error) goto 99
  endif
  if (fits%cols%posi%tsys.ne.0) then
     call get_item(obs%head%gen%tsys,1,fmt_r4,row(fits%cols%desc%addr(fits%cols%posi%tsys)),  &
       fits%cols%desc%fmt(fits%cols%posi%tsys),error)
     if (error) goto 99
  endif
  if (fits%cols%posi%restfreq.ne.0) then
     call get_item(obs%head%spe%restf,1,fmt_r8,row(fits%cols%desc%addr(fits%cols%posi%restfreq)),  &
       fits%cols%desc%fmt(fits%cols%posi%restfreq),error)
     if (error) goto 99
     obs%head%spe%restf = obs%head%spe%restf*1.d-6
  endif
  if (fits%cols%posi%imagfreq.ne.0) then
     call get_item(obs%head%spe%image,1,fmt_r8,row(fits%cols%desc%addr(fits%cols%posi%imagfreq)),  &
       fits%cols%desc%fmt(fits%cols%posi%imagfreq),error)
     if (error) goto 99
     obs%head%spe%image = obs%head%spe%image*1.d-6
  endif
  if (fits%cols%posi%velocity.ne.0) then
     call get_item(r4,1,fmt_r4,row(fits%cols%desc%addr(fits%cols%posi%velocity)),  &
       fits%cols%desc%fmt(fits%cols%posi%velocity),error)
     if (error) goto 99
     obs%head%spe%voff = r4*1.d-3
  endif
  if (fits%cols%posi%specsys.ne.0) then
     chain = ' '
     lench = 8
     call get_item(ichain,1,lench,row(fits%cols%desc%addr(fits%cols%posi%specsys)),  &
       fits%cols%desc%fmt(fits%cols%posi%specsys),error)
     if (error) goto 99
     call bytoch(ichain,chain,lench)
     call fits_decode_specsys(chain(1:lench),obs%head%spe%vtype)
  endif
  if (fits%cols%posi%velref.ne.0) then
     call get_item(j,1,fmt_i4,row(fits%cols%desc%addr(fits%cols%posi%velref)),  &
       fits%cols%desc%fmt(fits%cols%posi%velref),error)
     if (error) goto 99
     call fits_decode_velref(j,obs%head%spe%vconv,obs%head%spe%vtype)
  endif
  if (fits%cols%posi%veldef.ne.0) then
     chain = ' '
     lench = 12
     call get_item(ichain,1,lench,row(fits%cols%desc%addr(fits%cols%posi%veldef)),  &
       fits%cols%desc%fmt(fits%cols%posi%veldef),error)
     if (error) goto 99
     call bytoch(ichain,chain,lench)
!!$     if (index(chain,"LSR").ne.0) then
!!$        obs%head%spe%vtype = vel_lsr
!!$     elseif (index(chain,"HEL").ne.0) then
!!$        obs%head%spe%vtype = vel_hel
!!$     elseif (index(chain,"OBS").ne.0.or.index(chain,"TOP").ne.0) then
!!$        obs%head%spe%vtype = vel_obs
!!$     elseif (index(chain,"EAR").ne.0.or.index(chain,"GEO").ne.0) then
!!$        obs%head%spe%vtype = vel_ear
!!$     else
!!$        write(mess,*) 'Unknown velocity referential: ',chain
!!$        call class_message(seve%w,proc,mess)
!!$     endif
     if (chain(1:lench).eq.'LSR'.or.chain(1:lench).eq.'RADI-LSR') then
        obs%head%spe%vtype = vel_lsr
     elseif (chain(1:lench).eq.'HEL'.or.chain(1:lench).eq.'RADI-HEL') then
        obs%head%spe%vtype = vel_hel
     elseif (chain(1:lench).eq.'OBS'.or.chain(1:lench).eq.'RADI-OBS') then
        obs%head%spe%vtype = vel_obs
     elseif (chain(1:lench).eq.'EAR'.or.chain(1:lench).eq.'RADI-EAR') then
        obs%head%spe%vtype = vel_ear
        ! Should add support for the optical and relativistic convention one day
     else
        write(mess,*) 'Unknown velocity referential: ',chain
        call class_message(seve%w,proc,mess)
     endif
  endif
  if (fits%cols%posi%deltav.ne.0) then
     call get_item(r4,1,fmt_r4,row(fits%cols%desc%addr(fits%cols%posi%deltav)),  &
       fits%cols%desc%fmt(fits%cols%posi%deltav),error)
     if (error) goto 99
     obs%head%spe%vres = r4*1.d-3
  endif
  if (fits%cols%posi%nphase.ne.0) then
     call get_item(obs%head%swi%nphas,1,fmt_i4,row(fits%cols%desc%addr(fits%cols%posi%nphase)),  &
       fits%cols%desc%fmt(fits%cols%posi%nphase),error)
     obs%head%swi%swmod = mod_freq
     obs%head%presec(class_sec_swi_id) = .true.
     if (error) goto 99
  endif
  do j=1, mxphas
     if (fits%cols%posi%deltaf(j).ne.0) then
        call get_item(obs%head%swi%decal,1,fmt_r8,row(fits%cols%desc%addr(fits%cols%posi%deltaf(j))),  &
          fits%cols%desc%fmt(fits%cols%posi%deltaf(j)),error)
        obs%head%swi%decal = obs%head%swi%decal * 1.d-6 ! From Hz to MHz.
        if (error) goto 99     
     endif
     if (fits%cols%posi%ptime(j).ne.0) then
        call get_item(obs%head%swi%duree,1,fmt_r4,row(fits%cols%desc%addr(fits%cols%posi%ptime(j))),  &
          fits%cols%desc%fmt(fits%cols%posi%ptime(j)),error)
        if (error) goto 99     
     endif
     if (fits%cols%posi%weight(j).ne.0) then
        call get_item(obs%head%swi%poids,1,fmt_r4,row(fits%cols%desc%addr(fits%cols%posi%weight(j))),  &
          fits%cols%desc%fmt(fits%cols%posi%weight(j)),error)
        if (error) goto 99     
     endif
  enddo
  if (fits%cols%posi%tau_atm.ne.0) then
     call get_item(obs%head%gen%tau,1,fmt_r4,row(fits%cols%desc%addr(fits%cols%posi%tau_atm)),  &
       fits%cols%desc%fmt(fits%cols%posi%tau_atm),error)
     if (error) goto 99
  endif
  if (fits%cols%posi%h2omm.ne.0) then
     call get_item(obs%head%cal%h2omm,1,fmt_r4,row(fits%cols%desc%addr(fits%cols%posi%h2omm)),  &
       fits%cols%desc%fmt(fits%cols%posi%h2omm),error)
     if (error) goto 99
  endif
  if (fits%cols%posi%pamb.ne.0) then
     call get_item(obs%head%cal%pamb,1,fmt_r4,row(fits%cols%desc%addr(fits%cols%posi%pamb)),  &
       fits%cols%desc%fmt(fits%cols%posi%pamb),error)
     if (error) goto 99
  endif
  if (fits%cols%posi%tamb.ne.0) then
     call get_item(obs%head%cal%tamb,1,fmt_r4,row(fits%cols%desc%addr(fits%cols%posi%tamb)),  &
       fits%cols%desc%fmt(fits%cols%posi%tamb),error)
     if (error) goto 99
  endif
  if (fits%cols%posi%tchop.ne.0) then
     call get_item(obs%head%cal%tchop,1,fmt_r4,row(fits%cols%desc%addr(fits%cols%posi%tchop)),  &
       fits%cols%desc%fmt(fits%cols%posi%tchop),error)
     if (error) goto 99
  endif
  if (fits%cols%posi%tcold.ne.0) then
     call get_item(obs%head%cal%tcold,1,fmt_r4,row(fits%cols%desc%addr(fits%cols%posi%tcold)),  &
       fits%cols%desc%fmt(fits%cols%posi%tcold),error)
     if (error) goto 99
  endif
  if (fits%cols%posi%elevatio.ne.0) then
     call get_item(obs%head%gen%el,1,fmt_r4,row(fits%cols%desc%addr(fits%cols%posi%elevatio)),  &
       fits%cols%desc%fmt(fits%cols%posi%elevatio),error)
     if (error) goto 99
     obs%head%gen%el = obs%head%gen%el*pi/180.d0
  endif
  if (fits%cols%posi%azimuth.ne.0) then
     call get_item(obs%head%gen%az,1,fmt_r4,row(fits%cols%desc%addr(fits%cols%posi%azimuth)),  &
       fits%cols%desc%fmt(fits%cols%posi%azimuth),error)
     if (error) goto 99
     obs%head%gen%az = obs%head%gen%az*pi/180.d0
  endif
  if (fits%cols%posi%gainimag.ne.0) then
     call get_item(obs%head%cal%gaini,1,fmt_r4,row(fits%cols%desc%addr(fits%cols%posi%gainimag)),  &
       fits%cols%desc%fmt(fits%cols%posi%gainimag),error)
     if (error) goto 99
  endif
  if (fits%cols%posi%beameff.ne.0) then
     call get_item(obs%head%cal%beeff,1,fmt_r4,row(fits%cols%desc%addr(fits%cols%posi%beameff)),  &
       fits%cols%desc%fmt(fits%cols%posi%beameff),error)
     if (error) goto 99
  endif
  if (fits%cols%posi%forweff.ne.0) then
     call get_item(obs%head%cal%foeff,1,fmt_r4,row(fits%cols%desc%addr(fits%cols%posi%forweff)),  &
       fits%cols%desc%fmt(fits%cols%posi%forweff),error)
     if (error) goto 99
  endif
  if (fits%cols%posi%equinox.ne.0) then
     call get_item(obs%head%pos%equinox,1,fmt_r4,row(fits%cols%desc%addr(fits%cols%posi%equinox)),  &
       fits%cols%desc%fmt(fits%cols%posi%equinox),error)
     if (error) goto 99
  endif
  ! Needs a modification of the basic common: should store a string, and move
  ! decoding to MJD to fits_head2obs. Should introduce support for
  ! MJD-OBS at the same time.
  ! IF (I_DOBS.NE.0) THEN
  !    CALL GET_ITEM(DATE_OBS,1,FMT_R8,ROW(COLADDR(I_DOBS)),COLFMT(I_DOBS),ERROR)
  !    IF (ERROR) GOTO 99
  ! ENDIF
  ! IF (I_DRED.NE.0) THEN
  !    CALL GET_ITEM(DATE_RED,1,FMT_R8,ROW(COLADDR(I_DRED)),COLFMT(I_DRED),ERROR)
  !    IF (ERROR) GOTO 99
  ! ENDIF
  if (fits%cols%posi%dobs.ne.0) then
     date = ' '  ! Ensure trailing blanks in case item is too short
     call chtoby(date,tmpbytes,strlen)
     call get_item(tmpbytes,1,strlen,  &
                   row(fits%cols%desc%addr(fits%cols%posi%dobs)),  &
                   fits%cols%desc%fmt(fits%cols%posi%dobs), &
                   error)
     if (error) goto 99
     call bytoch(tmpbytes,date,strlen)
     ! make provision for old date format:
     iind = index(date, '/')
     if (iind.eq.0) then         ! new ISO date format
       read(date, 1004, iostat=ier) iy, im, id
       !if (ier.ne.0) call werror(comm,inerror,debug)
       !! In that case, decode also what should be the UT OBS at end:
       !!Bertrand: nope, trailing time is discarded, use UT instead.
       !iind=index(date, 'T')
       !if (iind.ne.0)then
       !   iind=iind+i1
       !   call sic_decode(date, obs%head%gen%ut, 24, error)
       !   if (error)then
       !      call werror(comm,inerror,debug)
       !      error=.false.
       !   endif
       !endif
     else
        read(date,1003,iostat=ier) id,im,iy
        !if (ier.ne.0) call werror(comm, inerror, debug)
        iy = 1900+iy
     endif
     call gag_datj(id,im,iy,obs%head%gen%dobs)
  endif
  if (fits%cols%posi%dred.ne.0) then
     date = ' '  ! Ensure trailing blanks in case item is too short
     call chtoby(date,tmpbytes,strlen)
     call get_item(tmpbytes,1,strlen,  &
                   row(fits%cols%desc%addr(fits%cols%posi%dred)),  &
                   fits%cols%desc%fmt(fits%cols%posi%dred),error)
     if (error) goto 99
     call bytoch(tmpbytes,date,strlen)
     ! make provision for old date format:
     iind = index(date, '/')
     if (iind.eq.0) then         ! new ISO date format
       read(date, 1004, iostat=ier) iy, im, id
       !if (ier.ne.0) call werror(comm,inerror,debug)
       !! In that case, decode also what should be the UT OBS at end:
       !!Bertrand: nope, trailing time is discarded, use UT instead.
       !iind=index(date, 'T')
       !if (iind.ne.0)then
       !   iind=iind+i1
       !   call sic_decode(date, obs%head%gen%ut, 24, error)
       !   if (error)then
       !      call werror(comm,inerror,debug)
       !      error=.false.
       !   endif
       !endif
     else
        read(date, 1003, iostat=ier) id, im, iy
        !if (ier.ne.0) call werror(comm, inerror, debug)
        iy = 1900+iy
     endif
     call gag_datj(id, im, iy, obs%head%gen%dred)
  endif
  if (fits%cols%posi%ut.ne.0) then
     call get_item(obs%head%gen%ut,1,fmt_r8,row(fits%cols%desc%addr(fits%cols%posi%ut)),  &
       fits%cols%desc%fmt(fits%cols%posi%ut),error)
     if (error) goto 99
     obs%head%gen%ut = obs%head%gen%ut*pi/43200.d0  ! Time-seconds to radians
  endif
  if (fits%cols%posi%lst.ne.0) then
     call get_item(obs%head%gen%st,1,fmt_r8,row(fits%cols%desc%addr(fits%cols%posi%lst)),  &
       fits%cols%desc%fmt(fits%cols%posi%lst),error)
     if (error) goto 99
     obs%head%gen%st = obs%head%gen%st*pi/43200.d0  ! Time-seconds to radians
  endif
  if (fits%cols%posi%bmaj.ne.0) then
     call get_item(obs%head%res%major,1,fmt_r4,row(fits%cols%desc%addr(fits%cols%posi%bmaj)),  &
       fits%cols%desc%fmt(fits%cols%posi%bmaj),error)
     if (error) goto 99
     obs%head%res%major = obs%head%res%major*rad_per_deg  ! Deg to radians
  endif
  if (fits%cols%posi%bmin.ne.0) then
     call get_item(obs%head%res%minor,1,fmt_r4,row(fits%cols%desc%addr(fits%cols%posi%bmin)),  &
       fits%cols%desc%fmt(fits%cols%posi%bmin),error)
     if (error) goto 99
     obs%head%res%minor = obs%head%res%minor*rad_per_deg  ! Deg to radians
  endif
  if (fits%cols%posi%bpa.ne.0) then
     call get_item(obs%head%res%posang,1,fmt_r4,row(fits%cols%desc%addr(fits%cols%posi%bpa)),  &
       fits%cols%desc%fmt(fits%cols%posi%bpa),error)
     if (error) goto 99
     obs%head%res%posang = obs%head%res%posang*rad_per_deg  ! Deg to radians
  endif
  !
  return
  !
99 error = .true.
  return
1003 format(i2,1x,i2,1x,i2)
1004 format(i4,1x,i2,1x,i2)
end subroutine fits_chopbuf_1header
!
subroutine fits_decode_specsys(specsys,vtype)
  use gbl_constant
  !---------------------------------------------------------------------
  ! @ private
  ! Decode SPECSYS value as velocity type.
  ! Note: values are not modified if information is missing (might be
  ! read from another keyword).
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: specsys
  integer(kind=4),  intent(inout) :: vtype
  !
  select case (specsys)
  case ('LSRK')
    vtype = vel_lsr
  case ('BARYCENT')
    vtype = vel_hel
  case ('TOPOCENT')
    vtype = vel_obs
  end select
end subroutine fits_decode_specsys
!
subroutine fits_decode_velref(velref,vconv,vtype)
  use gbl_constant
  !---------------------------------------------------------------------
  ! @ private
  ! Decode VELREF value as velocity convention + velocity type.
  ! Note: values are not modified if information is missing (might be
  ! read from another keyword).
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)    :: velref
  integer(kind=4), intent(inout) :: vconv
  integer(kind=4), intent(inout) :: vtype
  ! Local
  integer(kind=4) :: i
  !
  if (velref.gt.256) then
    vconv = vconv_rad
    i = velref-256
  elseif (velref.gt.0) then
    vconv = vconv_opt
    i = velref
  else
    ! Not supported (in particular 0 is not a valid value)
    return
  endif
  !
  select case (i)
  case (1)
    vtype = vel_lsr
  case (2)
    vtype = vel_hel
  case (3)
    vtype = vel_obs
  end select
  !
end subroutine fits_decode_velref
