subroutine class_write_comm(set,line,r,error,user_function)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_write_comm
  use class_common
  use class_types
  use class_index
  use class_parameter
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! CLASS Support routine for command
  !     WRITE [Obs_Number]
  ! Write the current R observation to the output file
  ! If this observation is a old OTF data, splits it into separate
  ! spectra, one per dump.
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  character(len=*),    intent(in)    :: line           ! Command Line
  type(observation),   intent(inout) :: r              !
  logical,             intent(inout) :: error          ! Return error code
  logical,             external      :: user_function  ! handling of user defined sections intent(in)
  ! Local
  character(len=*), parameter :: rname='WRITE'
  integer(kind=4) :: ir,jr,lastsub
  integer(kind=obsnum_length) :: obsnum,lastscan
  character(len=message_length) :: mess
  !
  if (r%head%xnum.eq.0) then
    call class_message(seve%e,rname,'No R spectrum in memory')
    error = .true.
    return
  endif
  !
  ! Observation kind
  if (r%is_otf) then
     ! Old OTF format
     obsnum = fileout%desc%xnext
     call sic_i8 (line,0,1,obsnum,.false.,error)
     jr = r%head%des%rec       ! Current record number
     ! Split them all at once: increment the Obs number automatically
     if (knext.eq.1) then
        lastscan = -1
        lastsub  = 0
     else if (knext.ge.2) then
        lastscan = cx%scan(knext-1)
        lastsub  = cx%subscan(knext-1)
     else
        write(mess,*) 'Wrong value for knext: ',knext
        call class_message(seve%e,rname,mess)
        error = .true.
        return
     endif
     if (cx%scan(knext).ne.lastscan) then
       r%head%gen%subscan = 1
     else
       r%head%gen%subscan = lastsub+1
     endif
     cx%subscan(knext) = r%head%gen%subscan
     ! This has always been in use in the writing subroutine:
     r%head%presec(class_sec_cal_id) = set%beamt.ne.0.
     do ir=1,r%head%des%ndump
        call get_rec(r,ir,error)
        r%head%gen%num = obsnum
        r%head%presec(class_sec_desc_id) = .false.
        r%is_otf = .false.
        call class_write(set,r,error,user_function)
        r%is_otf = .true.
        r%head%presec(class_sec_desc_id) = .true.
        obsnum = obsnum + 1
     enddo
     write(mess,*) 'Splitting an OTF scan into ',r%head%des%ndump,' observations'
     call class_message(seve%i,rname,mess)
     call get_rec(r,jr,error) ! Put first record number into R buffer
     r%is_otf = .true.
     r%head%presec(class_sec_desc_id) = .true.
     !
  else
     ! Just a simple observation => Test its number
     if (sic_present(0,1)) then
        call sic_i8 (line,0,1,obsnum,.true.,error)
        if (error) return
     else if (fileout%desc%single) then
        if (fileout%update) then
           call class_message(seve%e,rname, &
             'Writing not allowed on file opened for UPDATE')
           error = .true.
           return
        endif
        obsnum = 0  ! Reset (ignore) r%head%gen%num, use automatic numbering
     else
        obsnum = r%head%gen%num  ! Use current value (which can be 0 = automatic
                                 ! numbering)
     endif
     !
     ! Set observation number as requested
     r%head%gen%num = obsnum
     if (r%head%gen%subscan.eq.0) then
       ! subscan == 0 <=> Old OTF format
       r%head%gen%subscan = 1
     endif
     !
     ! This has always been in use in the writing subroutine:
     r%head%presec(class_sec_cal_id) = set%beamt.ne.0.
     !
     call class_write(set,r,error,user_function)
     if (error)  return
     !
     write(mess,'(A,I0,A,I0,A)')  &
       'Observation #',r%head%gen%num,';',r%head%gen%ver,' successfully written'
     call class_message(seve%i,rname,mess)
     !
  endif
  !
  call classcore_fileout_flush(error)
  if (error)  return
  !
end subroutine class_write_comm
!
subroutine class_write(set,obs,error,user_function)
  use gbl_message
  use classcore_interfaces, except_this=>class_write
  use class_common
  use class_types
  use plot_formula
  !---------------------------------------------------------------------
  ! @ public
  !  Writes the input observation onto the output file. "write" means:
  ! open the observation in the output file, transfer, close in the
  ! output file. Output file is not flushed, it is up to the calling
  ! routine to flush it or not for efficiency purpose.
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  type(observation),   intent(inout) :: obs            !
  logical,             intent(inout) :: error          ! Return error code
  logical,             external      :: user_function  ! Handling of user defined sections
  ! Local
  character(len=*), parameter :: rname='WRITE'
  integer(kind=entry_length) :: num
  integer(kind=4) :: isec,nsec
  !
  if (fileout%update) then
    call class_message(seve%e,rname, &
      'Writing not allowed on file opened for UPDATE')
    error = .true.
    return
  endif
  !
  if (set%modex.eq.'F') then
    obs%head%plo%vmin = gvx1
    obs%head%plo%vmax = gvx2
    obs%head%presec(class_sec_plo_id) = .true.
  endif
  if (set%modey.eq.'F') then
    obs%head%plo%amin = guy1
    obs%head%plo%amax = guy2
    obs%head%presec(class_sec_plo_id) = .true.
  endif
  !
  ! Number of activated sections:
  nsec = 0
  do isec=-mx_sec,0
    if (obs%head%presec(isec))  nsec = nsec+1
  enddo
  !
  ! Start writing an observation
  call class_write_open(set,obs,nsec,num,error)
  if (error)  return
  !
  call class_update(set,rname,obs,error,user_function)
  if (error)  return
  !
end subroutine class_write
!
subroutine class_update_comm(set,r,error,user_function)
  use gbl_message
  use classcore_interfaces, except_this=>class_update_comm
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! CLASS Support routine for command
  !  UPDATE
  ! Update the observation in R in the output file
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  type(observation),   intent(inout) :: r              !
  logical,             intent(inout) :: error          ! Return error code
  logical,             external      :: user_function  ! Handling of user defined sections
  ! Local
  character(len=*), parameter :: rname='UPDATE'
  character(len=message_length) :: mess
  !
  if (r%head%xnum.eq.0) then
    call class_message(seve%e,rname,'No R spectrum in memory')
    error = .true.
  elseif (r%head%xnum.eq.-1) then
    call class_message(seve%e,rname,'R spectrum is in memory only')
    error = .true.
  endif
  if (.not.filein_is_fileout()) then
    call class_message(seve%e,rname,'Input file must equal Output file')
    error = .true.
  endif
  if (error)  return
  !
  ! This has always been in use in the writing subroutine:
  r%head%presec(class_sec_cal_id) = set%beamt.ne.0.
  !
  call mobs(r,error)
  if (error)  return
  !
  call class_update(set,rname,r,error,user_function)
  if (error)  return
  !
  call classcore_fileout_flush(error)
  if (error)  return
  !
  write(mess,'(A,I0,A,I0,A)')  &
    'Observation #',r%head%gen%num,';',r%head%gen%ver,' successfully updated'
  call class_message(seve%i,rname,mess)
  !
end subroutine class_update_comm
!
subroutine class_update(set,rname,obs,error,user_function)
  use gbl_message
  use classcore_interfaces, except_this=>class_update
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS Internal routine for commands
  !   UPDATE
  !   WRITE
  !  Update the input observation in the output file. Output file is not
  ! flushed, it must be done (or not for efficiency purpose) by the
  ! calling routine.
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  character(len=*),    intent(in)    :: rname          ! Caller name
  type(observation),   intent(inout) :: obs            !
  logical,             intent(inout) :: error          ! Return error code
  logical,             external      :: user_function  ! Handling of user defined sections
  ! Local
  character(len=message_length) :: mess
  !
  if (obs%desc%version.lt.vobs_stable) then
    write(mess,'(A,I0,A)')  &
      'Writing an observation version #',obs%desc%version,' is obsolescent'
    call class_message(seve%w,rname,mess)
    call class_message(seve%w,rname,  &
      'You should use an ouput file with the latest Class Data Format')
  endif
  !
  call class_write_transfer(set,obs,error,user_function)
  if (error) return
  !
  call class_write_close(set,obs,error)
  if (error) return
  !
end subroutine class_update
!
subroutine class_write_open(set,obs,maxsec,entry_num,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_write_open
  use class_common
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  ! Starts the writing of a new observation.
  !----------------------------------------------------------------------
  type(class_setup_t),        intent(in)    :: set        !
  type(observation),          intent(inout) :: obs        !
  integer(kind=4),            intent(in)    :: maxsec     ! Max num of sections to be written (in addition of the data)
  integer(kind=entry_length), intent(out)   :: entry_num  ! Entry number of the observation in the output file
  logical,                    intent(inout) :: error      ! Error flag
  ! Local
  character(len=*), parameter :: rname='WRITE'
  integer(kind=4) :: vobs,ier
  logical :: full
  !
  if (.not.fileout_opened(rname,error))  return
  !
  entry_num = fileout%desc%xnext
  outobs_modify = .false.
  if (fileout%desc%version.eq.1) then
    ! File V1 can only store observation V1. Our own section writing routines
    ! (cwall) will complain if they can not backport a section back into obs v1
    vobs = 1
  else
    ! File V2 can store any observation version. At read time, old versions
    ! of Class will complain if they do not know them, which is fine
    vobs = vobs_stable
    ier = sic_getlog('CLASS_OBS_VERSION',vobs)
  endif
  !
  ! Make sure there is room for a new entry, and initialize its descriptor
  call classic_entry_init(fileout,entry_num,maxsec,vobs,full,obs%desc,error)
  if (full) then
    call class_message(seve%e,rname,  &
      'Set the logical variable CLASS_IDX_SIZE in $HOME/.gag.dico '// &
      'to a value larger than what you need before restarting CLASS')
    error = .true.
  endif
  if (error)  return
  !
  ! Open the working buffer
  call classic_recordbuf_open(fileout,fileout%desc%nextrec,  &
    fileout%desc%nextword,obufobs,error)
  if (error)  return
  !
end subroutine class_write_open
!
subroutine class_write_transfer(set,obs,error,user_function)
  use gildas_def
  use gbl_format
  use gbl_constant
  use gbl_message
  use classcore_interfaces, except_this=>class_write_transfer
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS External routine (available to other programs)
  !  Perform all the necessary operations to transfer the observation
  ! once it has been defined in 'obs' and opened in the output file.
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  type(observation),   intent(inout) :: obs            ! or intent(in) only?
  logical,             intent(inout) :: error          ! Return error code
  logical,             external      :: user_function  ! Handling of user defined sections
  ! Local
  character(len=*), parameter :: rname='WRITE'
  integer(kind=4) :: ndata
  !
  if (error) return
  !
  ! Note that you can NOT activate sections here, it's too late (because
  ! room for the exact number of sections has been preallocated in
  ! class_write_open)
  !
  if (.not.obs%head%presec(class_sec_gen_id)) then
    call class_message(seve%e,rname,'Missing General section')
    error = .true.
    return
  endif
  call wgen(set,obs,error) ! General section
  if (error) return
  !
  if (obs%head%gen%kind.eq.kind_sky) then
     ! Skydip
     call wcal(set,obs,error)
     error = .false.
     call wsky(set,obs,error)
     ndata = 0
     return  ! Nothing more
  endif
  !
  if (.not.obs%head%presec(class_sec_pos_id)) then
    call class_message(seve%e,rname,'Missing Position section')
    error = .true.
    return
  endif
  call wpos(set,obs,error)           ! position
  if (error) return
  !
  if (obs%head%gen%kind.eq.kind_spec) then
    if (.not.obs%head%presec(class_sec_spe_id)) then
      call class_message(seve%e,rname,'Missing Spectroscopy section')
      error = .true.
      return
    endif
    call wspec(set,obs,error)        ! Spectroscopy
    if (error) return
    ndata = obs%head%spe%nchan
  else
    if (.not.obs%head%presec(class_sec_dri_id)) then
      call class_message(seve%e,rname,'Missing Continuum section')
      error = .true.
      return
    endif
    call wcont(set,obs,error)        ! Continuum Data
    if (error) return
    ndata = obs%head%dri%npoin
    if (obs%head%presec(class_sec_bea_id)) then
      call wbeam(set,obs,error)
      if (error) return
    endif
  endif
  if (obs%head%presec(class_sec_res_id)) then
    call wres(set,obs,error)        ! Resolution description
    if (error) return
  endif
  if (obs%head%presec(class_sec_plo_id)) then
    call wplot(set,obs,error)        ! Plot limits
    if (error) return
  endif
  if (obs%head%presec(class_sec_bas_id)) then
    call wbase(set,obs,error)        ! Baseline subtraction parameters (degree,window)
    if (error) return
  endif
  if (obs%head%presec(class_sec_his_id)) then
    call worig(set,obs,error)        ! Origin (number of the spectra)
    if (error) return
  endif
  if (obs%head%presec(class_sec_swi_id)) then
    call wswi(set,obs,error)         ! Phase parameters
    if (error) return
  endif
  !
  if (obs%head%gen%kind.eq.kind_spec) then
    if (obs%head%presec(class_sec_gau_id)) then
        call wgaus(set,obs,error)      ! Gaussian fitting parameters for heterodyne
        if (error) return
    endif
    if (obs%head%presec(class_sec_hfs_id)) then    ! NH3 Fit
        call wnh3(set,obs,error)
        if (error) return
    endif
    if (obs%head%presec(class_sec_abs_id)) then    ! ABS Fit
        call wabs(set,obs,error)
        if (error) return
    endif
    if (obs%head%presec(class_sec_she_id)) then  ! Shell Profile Fit
        call wshel(set,obs,error)
        if (error) return
    endif
  elseif (obs%head%gen%kind.eq.kind_cont) then
    if (obs%head%presec(class_sec_gau_id)) then
        call wgaus(set,obs,error)      ! parametres fit de gaussiennes
        if (error) return
    endif
    if (obs%head%presec(class_sec_poi_id)) then  ! Pointing fit
        call wpoint(set,obs,error)
        if (error) return
    endif
  endif
  if (obs%head%presec(class_sec_cal_id)) then      ! Calibration parameters
    call wcal(set,obs,error)
    if (error) return
  endif
  if (obs%head%presec(class_sec_her_id)) then      ! Calibration parameters
    call wherschel(set,obs,error)
    if (error) return
  endif
  if (obs%head%presec(class_sec_com_id)) then  ! Comment Section
    call wcom(set,obs,error)
    if (error) return
  endif
  if (obs%head%presec(class_sec_xcoo_id)) then   ! X coordinates (irregularly spaced)
    call wxcoo(set,obs,error)
    if (error) return
  endif
  !
  if (obs%head%presec(class_sec_user_id)) then
    call wuser(set,obs,error)
    if (error)  return
  endif
  !
  if (obs%head%presec(class_sec_assoc_id)) then
    call wassoc(set,obs,error)
    if (error)  return
  endif
  !
  error = user_function('WRITE')
  if (error) return
  !
  if (.not.obs%head%presec(class_sec_desc_id)) then
    call wdata(obs,ndata,obs%spectre,error)   ! donnees
  else
    call class_message(seve%e,rname,'OTF data no longer supported')
    error = .true.
  endif
  !
end subroutine class_write_transfer
!
subroutine class_write_flush(set,obs,error)
  use classcore_interfaces, except_this=>class_write_flush
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! CLASS External routine (available to other programs)
  ! Close an observation and reopen the output file
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(observation),   intent(inout) :: obs    !
  logical,             intent(inout) :: error  ! Error status
  !
  call class_write_close(set,obs,error)
  if (error)  return
  !
  call classcore_fileout_flush(error)
  if (error)  return
  !
end subroutine class_write_flush
!
subroutine class_write_close(set,obs,error)
  use classcore_interfaces, except_this=>class_write_close
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! Close an observation, but not the file
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(observation),   intent(inout) :: obs    !
  logical,             intent(inout) :: error  ! Error status
  !
  call cobs(set,obs,error)  ! Close observation
  !
end subroutine class_write_close
