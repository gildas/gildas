subroutine minsinus(npar,g,f,x,iflag,obs)
  use gildas_def
  use phys_const
  use class_types
  use sinus_parameter
  !----------------------------------------------------------------------
  ! @ private-mandatory
  !  Function to be minimized in the SINUS fit.
  !  Basic parameters are Intensity, Period and Phase.
  !----------------------------------------------------------------------
  integer(kind=4),   intent(in)  :: npar     ! Number of parameters
  real(kind=8),      intent(out) :: g(npar)  ! Array of derivatives
  real(kind=8),      intent(out) :: f        ! Function values
  real(kind=8),      intent(in)  :: x(npar)  ! Input parameter values
  integer(kind=4),   intent(in)  :: iflag    ! Code operation
  type(observation), intent(in)  :: obs      !
  ! Local
  integer(kind=4) :: i,kbas,krai
  real(kind=4) :: tt,oo,pp,gt,go,gp,arg,sarg,carg,xvel,ta,ybas,yrai,ff,fs,aa,bb,ga,gb
  !
  ! final computations
  tt=x(1)
  oo=x(2)
  pp=x(3)
  aa=x(4)
  bb=x(5)
  if (iflag.ne.3) then
     fs=0.
     gt=0.
     go=0.
     ga=0.
     gb=0.
     gp=0.
     !
     !	g1 derivee / intensity
     !	g2 derivee / period
     !	g3 derivee / phase
     !
     do i=obs%cimin,obs%cimax
        if (wfit(i).eq.0) cycle
        xvel = real(obs%datax(i),4)
        arg = 2.0*pis*(xvel-pp)/oo
        sarg = sin(arg)
        ff = tt*sarg + aa*xvel + bb
        !
        ! compute chi-2 (no ponderation, wfit is ignored)
        ff =  (ff - obs%spectre(i))
        fs = fs + ff**2
        !
        ! compute gradients
        if (iflag.eq.2) then
           ff = 2.*ff
           gt = gt + ff*sarg
           carg = 2.0*pis*tt*ff*cos(arg)
           go = go - (xvel-pp)*carg/oo**2
           gp = gp - carg/oo
           ga = ga + ff*xvel
           gb = gb + ff
        endif
     enddo
     !
     ! setup values and return
     f = fs
     g(1)=gt
     g(2)=go
     g(3)=gp
     g(4)=ga
     g(5)=gb
  else
     !
     ! compute sigma after minimisation
     kbas=0
     ybas=0.
     krai=0
     yrai=0.
     do i=obs%cimin,obs%cimax
        if (obs%spectre(i).eq.obs%cbad) cycle
        ta = ( &
             tt*sin(2.0*pis*(obs%datax(i)-pp)/oo)   &
             + aa*obs%datax(i) + bb - obs%spectre(i) &
             )**2
        if (wfit(i).ne.0) then
           kbas=kbas+1
           ybas=ybas+ta
        else
           krai=krai+1
           yrai=yrai+ta
        endif
     enddo
     if (kbas.ne.0) then
        sigbas=sqrt(ybas/kbas)
     else
        sigbas=0.d0
     endif
     if (krai.ne.0) then
        sigrai=sqrt(yrai/krai)
     else
        sigrai=0.d0
     endif
  endif
end subroutine minsinus
