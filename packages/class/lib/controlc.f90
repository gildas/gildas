subroutine class_controlc(com,error)
  use gkernel_interfaces
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS Internal routine
  ! Interruption by ^C: behavior depends on keybord input
  ! - blank: continue execution
  ! - carriage return: continue execution
  ! - q: abort execution
  ! _ any other character: ask for input
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: com    ! calling command
  logical,          intent(out) :: error  ! logical flag
  ! Local
  integer :: nans
  character(len=4) :: answer,quit
  data quit/'QUIT'/
  !
  error = .false.
  if (sic_ctrlc()) then
     do while (.true.)
        call sic_wprn('W-'//com//',  <^C> pressed, '//   &
             'type Q to abort, RETURN to continue',answer,nans)
        call sic_blanc(answer,nans)
        if (nans.eq.0) then
           error = .false.
           return
        endif
        nans = min(nans,4)
        call sic_upper(answer)
        if (answer(1:nans).eq.quit(1:nans)) then
           call class_message(seve%w,com,' interrupted by <^C>')
           error = .true.
           return
        endif
     enddo
  endif
end subroutine class_controlc
