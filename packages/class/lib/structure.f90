subroutine allocate_classcore(error)
  use gbl_message
  use gkernel_interfaces
  use classcore_interfaces, except_this=>allocate_classcore
  use class_buffer
  use class_data
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! CLASS internal routine
  !---------------------------------------------------------------------
  logical, intent(out) :: error     ! Error flag
  ! Local
  character(len=*), parameter :: rname='ALLOCATE_CLASSCORE'
  integer(kind=4) :: ier
  type (header) :: head
  !
!  print *,'Size of general ',sizeof(head%gen)*0.125
!  print *,'Size of position ',sizeof(head%pos)*0.125
!  print *,'Size of spectro ',sizeof(head%spe)*0.125
!  print *,'Size of base ',sizeof(head%bas)*0.125
!  print *,'Size of history ',sizeof(head%his)*0.125
!  print *,'Size of plot ',sizeof(head%plo)*0.125
!  print *,'Size of switch ',sizeof(head%swi)*0.125
!  print *,'Size of calibration ',sizeof(head%cal)*0.125
!  print *,'Size of skydip ',sizeof(head%sky)*0.125
!  print *,'Size of gauss ',sizeof(head%gau)*0.125
!  print *,'Size of shell ',sizeof(head%she)*0.125
!  print *,'Size of hfs ',sizeof(head%hfs)*0.125
!  print *,'Size of abs ',sizeof(head%abs)*0.125
!  print *,'Size of drift ',sizeof(head%dri)*0.125
!  print *,'Size of beam ',sizeof(head%bea)*0.125
!  print *,'Size of continuum ',sizeof(head%poi)*0.125
!  print *,'Size of descriptor ',sizeof(head%des)*0.125
!  !
!  print *,'Size of HEADER ',sizeof(head)*0.25
!  print *,'Size of Observation ',sizeof(obs)*0.25
!  print *,'Size of Index',sizeof(ind)*0.25
!  print *,'File ',ispec
  !
  ! Initialization
  error = .false.
  !
  ! Index size is always read in the CLASS_IDX_SIZE logical variable
  ! Default is in gag.dico.gbl, and customized in $HOME/.gag.dico
  ier = sic_getlog('CLASS_IDX_SIZE',class_idx_size)
  if (ier.ne.0) then
    call class_message(seve%f,rname,'Could not find index size through the CLASS_IDX_SIZE logical variable')
    call sysexi(fatale)
  endif
  !
  call class_luns_get(error)  ! Support LUNS for FILE IN|OUT
  if (error) then
    call class_message(seve%f,rname,'Major memory initialization problem')
    return
  endif
  !
  ! Working buffers for read/write sections in IN/OUT file
  header_length = (locwrd(head%ends) - locwrd(head%start) + 8 ) / 4
  allocate (iwork(header_length),stat=ier)
  if (failed_allocate(rname,'Section buffer',ier,error))  return
  !
end subroutine allocate_classcore
!
subroutine deallocate_classcore(error)
  use gbl_message
  use gkernel_interfaces
  use classcore_interfaces, except_this=>deallocate_classcore
  use class_buffer
  use class_common
  use class_data
  use class_index
  use class_popup
  !-------------------------------------------------------------------
  ! @ public (for libclass only)
  ! Deallocate Class buffers
  !-------------------------------------------------------------------
  logical, intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DEALLOCATE_CLASSCORE'
  integer(kind=4) :: ier
  logical :: readsec(-mx_sec:0)
  !
  ! Indexes
  call class_message(seve%d,rname,'Deallocate cx index')
  call deallocate_optimize(cx,error)
  call class_message(seve%d,rname,'Deallocate ix index')
  call deallocate_optimize(ix,error)
  call class_message(seve%d,rname,'Deallocate ox index')
  call deallocate_optimize(ox,error)
  if (error)  &
     call class_message(seve%f,rname,'Could not deallocate memory 1')
  !
  ! P & Q Buffers
  call class_message(seve%d,rname,'Deallocate P & Q buffers')
  if (allocated(pdatai)) then
     deallocate(pdatai,pdatas,pdatav,pdata2,pdataw,pdatax,stat=ier)
     if (ier.ne.0) then
        call class_message(seve%f,rname,'Could not deallocate memory 2')
     endif
  endif
  !
  ! IDX buffers
  readsec(:) = .true.  ! Select all sections
  call class_variable_index_reallocate(.true.,readsec,0_8,error)
  ! if (error)  continue
  !
  ! Popup Region
  ier = 0
  call class_message(seve%d,rname,'Deallocate Popup')
  if (allocated(ipop)) deallocate(ipop,xpop,ypop,stat=ier)
  if (ier.ne.0) then
     call class_message(seve%f,rname,'Could not deallocate memory 3')
  endif
  !
  ! Working buffer for read/write in IN/OUT file
  deallocate (iwork,stat=ier)
  if (ier.ne.0)  call class_message(seve%f,rname,'Could not deallocate iwork')
  if (allocated(uwork))  deallocate(uwork)
  !
  ! Classic record buffers
  call deallocate_recordbuf(ibufbi,error)
  call deallocate_recordbuf(ibufobs,error)
  call deallocate_recordbuf(obufbi,error)
  call deallocate_recordbuf(obufobs,error)
  !
end subroutine deallocate_classcore
!
subroutine reallocate_optimize(optx,mobs,long,keep,error)
  use gbl_message
  use gkernel_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Allocate the 'optimize' type arrays.
  !  'long' allocation: all arrays are allocated
  !  'short' allocation: only ind(:), num(:), bloc(:), word(:), ver(:),
  !                      ut(:)
  !---------------------------------------------------------------------
  type(optimize),             intent(inout) :: optx   !
  integer(kind=entry_length), intent(in)    :: mobs   ! Requested size
  logical,                    intent(in)    :: long   ! Long allocation?
  logical,                    intent(in)    :: keep   ! Keep previous data?
  logical,                    intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='ALLOCATE'
  integer(kind=4) :: ier
  integer(kind=entry_length) :: nobs
  integer(kind=4), allocatable :: bufi4(:)
  integer(kind=8), allocatable :: bufi8(:)
  real(kind=4), allocatable :: bufr4(:)
  real(kind=8), allocatable :: bufr8(:)
  character(len=12), allocatable :: bufc12(:)
  !
  if (associated(optx%ind)) then
    nobs = size(optx%ind,kind=8)  ! Size of allocation
    if (nobs.ge.mobs) then
      ! Index is already allocated with a larger size. Keep it like this.
      ! Shouldn't we deallocate huge allocations if user requests a small one?
      optx%mobs = mobs  ! Actual size in use (NOT size of allocation)
      return
    endif
  elseif (mobs.eq.0) then
    ! No problem: can occur when dealing with empty files (e.g. nothing
    ! was written in an output file)
    optx%mobs = 0
    return
  elseif (mobs.lt.0) then
    call class_message(seve%e,rname,'Can not allocate empty indexes')
    error = .true.
    return
  endif
  !
  ! Allocate temporary buffers
  if (keep) then
    nobs = optx%next-1  ! Only the used part of the arrays has to be kept
    allocate(bufi4(nobs),stat=ier)
    allocate(bufi8(nobs),stat=ier)
    allocate(bufr8(nobs),stat=ier)
    allocate(bufc12(nobs),stat=ier)
    if (long) then
      allocate(bufr4(nobs),stat=ier)
    endif
    if (failed_allocate(rname,'buf arrays',ier,error)) then
      error = .true.
      return
    endif
  endif
  !
  call reallocate_optimize_i8('ind array',optx%ind,error)
  if (error)  return
  call reallocate_optimize_i8('num array',optx%num,error)
  if (error)  return
  call reallocate_optimize_i8('bloc array',optx%bloc,error)
  if (error)  return
  call reallocate_optimize_i4('word array',optx%word,error)
  if (error)  return
  call reallocate_optimize_i4('ver array',optx%ver,error)
  if (error)  return
  call reallocate_optimize_i4('dobs array',optx%dobs,error)
  if (error)  return
  call reallocate_optimize_r8('ut array',optx%ut,error)
  if (error)  return
  call reallocate_optimize_c12('ctele array',optx%ctele,error)
  if (error)  return
  call reallocate_optimize_i8('scan array',optx%scan,error)
  if (error)  return
  call reallocate_optimize_i4('subscan array',optx%subscan,error)
  if (error)  return
  if (long) then
    call reallocate_optimize_i4('kind array',optx%kind,error)
    if (error)  return
    call reallocate_optimize_i4('qual array',optx%qual,error)
    if (error)  return
    call reallocate_optimize_r4('off1 array',optx%off1,error)
    if (error)  return
    call reallocate_optimize_r4('off2 array',optx%off2,error)
    if (error)  return
    call reallocate_optimize_c12('csour array',optx%csour,error)
    if (error)  return
    call reallocate_optimize_c12('cline array',optx%cline,error)
    if (error)  return
  else
    call reallocate_optimize_i8('number array',optx%sort%number,error)
    if (error)  return
    call reallocate_optimize_i8('entry array',optx%sort%entry,error)
    if (error)  return
    call reallocate_optimize_i8('dtt array',optx%sort%dtt,error)
    if (error)  return
  endif
  !
  optx%mobs = mobs  ! Actual size in use
  !
  if (keep) then
    if (allocated(bufi4))   deallocate(bufi4)
    if (allocated(bufi8))   deallocate(bufi8)
    if (allocated(bufr4))   deallocate(bufr4)
    if (allocated(bufr8))   deallocate(bufr8)
    if (allocated(bufc12))  deallocate(bufc12)
  endif
  !
contains
  subroutine reallocate_optimize_i4(name,val,error)
    character(len=*), intent(in)    :: name
    integer(kind=4),  pointer       :: val(:)
    logical,          intent(inout) :: error
    ! Local
    integer(kind=4) :: ier
    !
    if (keep)  bufi4(:) = val(1:nobs)
    if (associated(val)) deallocate(val)
    allocate(val(mobs),stat=ier)
    if (failed_allocate(rname,name,ier,error))  return
    if (keep) val(1:nobs) = bufi4(:)
  end subroutine reallocate_optimize_i4
  !
  subroutine reallocate_optimize_i8(name,val,error)
    character(len=*), intent(in)    :: name
    integer(kind=8),  pointer       :: val(:)
    logical,          intent(inout) :: error
    ! Local
    integer(kind=4) :: ier
    !
    if (keep)  bufi8(:) = val(1:nobs)
    if (associated(val)) deallocate(val)
    allocate(val(mobs),stat=ier)
    if (failed_allocate(rname,name,ier,error))  return
    if (keep) val(1:nobs) = bufi8(:)
  end subroutine reallocate_optimize_i8
  !
  subroutine reallocate_optimize_r4(name,val,error)
    character(len=*), intent(in)    :: name
    real(kind=4),     pointer       :: val(:)
    logical,          intent(inout) :: error
    ! Local
    integer(kind=4) :: ier
    !
    if (keep)  bufr4(:) = val(1:nobs)
    if (associated(val)) deallocate(val)
    allocate(val(mobs),stat=ier)
    if (failed_allocate(rname,name,ier,error))  return
    if (keep) val(1:nobs) = bufr4(:)
  end subroutine reallocate_optimize_r4
  !
  subroutine reallocate_optimize_r8(name,val,error)
    character(len=*), intent(in)    :: name
    real(kind=8),     pointer       :: val(:)
    logical,          intent(inout) :: error
    ! Local
    integer(kind=4) :: ier
    !
    if (keep)  bufr8(:) = val(1:nobs)
    if (associated(val)) deallocate(val)
    allocate(val(mobs),stat=ier)
    if (failed_allocate(rname,name,ier,error))  return
    if (keep) val(1:nobs) = bufr8(:)
  end subroutine reallocate_optimize_r8
  !
  subroutine reallocate_optimize_c12(name,val,error)
    character(len=*),  intent(in)    :: name
    character(len=12), pointer       :: val(:)
    logical,           intent(inout) :: error
    ! Local
    integer(kind=4) :: ier
    !
    if (keep)  bufc12(:) = val(1:nobs)
    if (associated(val)) deallocate(val)
    allocate(val(mobs),stat=ier)
    if (failed_allocate(rname,name,ier,error))  return
    if (keep) val(1:nobs) = bufc12(:)
  end subroutine reallocate_optimize_c12
  !
end subroutine reallocate_optimize
!
subroutine deallocate_optimize(optx,error)
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Deallocate the 'optimize' type arrays.
  !---------------------------------------------------------------------
  type(optimize),  intent(inout) :: optx   !
  logical,         intent(inout) :: error  ! Logical error flag
  ! Local
  integer(kind=4) :: ier
  !
  optx%mobs = 0
  ier = 0
  !
  if (associated(optx%ind))      deallocate(optx%ind,stat=ier)
  if (associated(optx%num))      deallocate(optx%num,stat=ier)
  if (associated(optx%bloc))     deallocate(optx%bloc,stat=ier)
  if (associated(optx%word))     deallocate(optx%word,stat=ier)
  if (associated(optx%ver))      deallocate(optx%ver,stat=ier)
  if (associated(optx%kind))     deallocate(optx%kind,stat=ier)
  if (associated(optx%qual))     deallocate(optx%qual,stat=ier)
  if (associated(optx%scan))     deallocate(optx%scan,stat=ier)
  if (associated(optx%dobs))     deallocate(optx%dobs,stat=ier)
  if (associated(optx%off1))     deallocate(optx%off1,stat=ier)
  if (associated(optx%off2))     deallocate(optx%off2,stat=ier)
  if (associated(optx%subscan))  deallocate(optx%subscan,stat=ier)
  if (associated(optx%csour))    deallocate(optx%csour,stat=ier)
  if (associated(optx%cline))    deallocate(optx%cline,stat=ier)
  if (associated(optx%ctele))    deallocate(optx%ctele,stat=ier)
  if (associated(optx%ut))       deallocate(optx%ut,stat=ier)
  !
  if (associated(optx%sort%number))  deallocate(optx%sort%number,stat=ier)
  if (associated(optx%sort%entry))   deallocate(optx%sort%entry,stat=ier)
  if (associated(optx%sort%dtt))     deallocate(optx%sort%dtt,stat=ier)
  error = ier.ne.0
  !
end subroutine deallocate_optimize
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine reallocate_pq(new_nchan,new_nobs,error)
  use gkernel_interfaces
  use class_data
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  !   (Re)allocate pointers and targets for P and Q buffers
  !   The headers are not modified at all
  !---------------------------------------------------------------------
  integer(kind=4),            intent(in)  :: new_nchan  ! Size of new P & Q arrays
  integer(kind=entry_length), intent(in)  :: new_nobs   ! Size of new P & Q arrays
  logical,                    intent(out) :: error      ! Error flag
  ! Local
  character(len=*), parameter :: rname='REALLOCATE_PQ'
  character(len=100) :: mess
  integer :: old_nchan,ier
  integer(kind=entry_length) :: old_nobs
  logical :: reallocate
  !
  ! Sanity check
  if ((new_nchan.le.0).or.(new_nobs.le.0)) then
     call class_message(seve%f,rname,'Array size is null or negative!')
     return
  endif
  !
  ! Allocation or reallocation?
  if (allocated(pdata2)) then
     ! Reallocation
     old_nchan = ubound(pdata2,1)
     old_nobs  = ubound(pdata2,2)
     if ((new_nchan.eq.old_nchan).and.(new_nobs.eq.old_nobs)) then
        ! Same size => Nothing to be done!
        write(mess,'(a,i0,i0)') &
             'P & Q arrays already allocated at the right size: ',new_nchan,new_nobs
        call class_message(seve%d,rname,mess)
        reallocate = .false.
     else
        reallocate = .true.
     endif
  else
     ! Allocation
     write(mess,'(a,i0,i0)') 'Creating P & Q arrays of size: ',new_nchan,new_nobs
     call class_message(seve%d,rname,mess)
     reallocate = .true.
  endif
  !
  ! (Re)allocate when needed
  if (reallocate) then
     ! Free memory when needed
     if (allocated(pdatai)) then
        call free_obs(p)
        deallocate(pdatai,pdatas,pdatav,pdata2,pdataw,pdatax)
     endif
     ! Reallocate memory of the right size
     allocate(pdatai(new_nchan),         pdatas(new_nchan),pdatav(new_nchan),  &
              pdata2(new_nchan,new_nobs),pdataw(new_nchan),pdatax(new_nchan),  &
              stat=ier)
     if (failed_allocate(rname,'P arrays',ier,error)) return
  endif
  !
  ! Associate P and Q observations to buffers
  p%datax => pdatax
  p%datas => pdatas
  p%datai => pdatai
  p%datav => pdatav
  p%dataw => pdataw
  p%data2 => pdata2
  p%spectre => p%data2(:,1)
  !
end subroutine reallocate_pq
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine reallocate_obs(obs,new_ndata,error)
  use gbl_message
  use gkernel_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  !   (Re)allocate obs. buffers
  !   Keep contents when enlarging the buffers
  !   The headers are not modified at all
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs        ! Current observation
  integer(kind=4),   intent(in)    :: new_ndata  ! Size of new obs arrays
  logical,           intent(out)   :: error      ! Error flag
  ! Local
  character(len=*), parameter :: rname='REALLOCATE_OBS'
  character(len=message_length) :: mess
  logical :: keep
  integer(kind=4) :: old_ndata,ier
  real(kind=xdata_kind), allocatable :: obsx(:),obss(:),obsi(:),obsv(:)
  real(kind=4), allocatable :: obs1(:),obsw(:)
  !
  error = .false.
  !
  ! Sanity check
  if (new_ndata.le.0) then
     call class_message(seve%e,rname,'Array size is null or negative!')
     error = .true.
     return
  endif
  !
  ! Allocation or reallocation?
  if (associated(obs%data1)) then
    ! Reallocation
    old_ndata = ubound(obs%data1,1)
    if (new_ndata.gt.old_ndata) then
      ! Enlargement => Data is kept
      write(mess,'(a,i12,a,i12)') 'Enlarging obs. arrays from ', &
          old_ndata,' to ',new_ndata
      call class_message(seve%d,rname,mess)
      keep = .true.
    else
      ! Reduction or same size => Nothing to be done!
      ! write(mess,'(a,i12)') &
      !   'Obs. arrays already allocated at size larger than: ',new_ndata
      ! call class_message(seve%i,rname,mess)
      return
    endif
  else
    ! Allocation
    write(mess,'(a,i12)') 'Creating obs. arrays of size: ',new_ndata
    call class_message(seve%d,rname,mess)
    keep = .false.
  endif
  !
  ! Save obs. arrays when needed
  if (keep) then
    allocate(obsx(old_ndata),obss(old_ndata),obsi(old_ndata),  &
             obsv(old_ndata),obs1(old_ndata),obsw(old_ndata),stat=ier)
    if (failed_allocate(rname,'Temporary obs. arrays',ier,error)) return
    obsx(:) = obs%datax(1:old_ndata)
    obss(:) = obs%datas(1:old_ndata)
    obsi(:) = obs%datai(1:old_ndata)
    obsv(:) = obs%datav(1:old_ndata)
    obs1(:) = obs%data1(1:old_ndata)
    obsw(:) = obs%dataw(1:old_ndata)
    deallocate(obs%datax,obs%datas,obs%datai,obs%datav,obs%data1,obs%dataw)
  endif
  !
  ! Reallocate memory of the right size
  allocate(obs%datax(new_ndata),obs%datas(new_ndata),obs%datai(new_ndata),  &
           obs%datav(new_ndata),obs%data1(new_ndata),obs%dataw(new_ndata),stat=ier)
  if (failed_allocate(rname,'obs% arrays',ier,error)) return
  !
  ! Restore obs. arrays
  if (keep) then
    obs%datax(1:old_ndata) = obsx(:)
    obs%datas(1:old_ndata) = obss(:)
    obs%datai(1:old_ndata) = obsi(:)
    obs%datav(1:old_ndata) = obsv(:)
    obs%data1(1:old_ndata) = obs1(:)
    obs%dataw(1:old_ndata) = obsw(:)
    deallocate(obsx,obss,obsi,obsv,obs1,obsw)
  endif
  obs%spectre => obs%data1
  !
end subroutine reallocate_obs
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
