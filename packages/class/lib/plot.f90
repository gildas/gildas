subroutine conne1(x,y,nxy,func)
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS  Internal routine
  !  Connect a set of points.
  !---------------------------------------------------------------------
  integer, intent(in) :: nxy     ! Number of points
  real,    intent(in) :: x(nxy)  ! X values
  real,    intent(in) :: y(nxy)  ! Y values
  external            :: func    ! Plotting function
  !
  integer j
  !
  if (nxy.lt.2) return
  call func(x(1),y(1),3)
  do j = 2,nxy
     call func(x(j),y(j),2)
  enddo
  !
end subroutine conne1
!
subroutine conne2(xval,xref,xinc,y,nxy,func)
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS  Internal routine
  !  Connect a set of points regularly spaced along the X axis.
  !---------------------------------------------------------------------
  real,    intent(in) :: xval    ! X value at reference
  real,    intent(in) :: xref    ! X reference pixel
  real,    intent(in) :: xinc    ! X increment
  integer, intent(in) :: nxy     ! Number of points
  real,    intent(in) :: y(nxy)  ! Y values
  external            :: func    ! Plotting function
  ! Local
  integer :: j
  real :: x
  !
  if (nxy.lt.2) return
  x = xval+(1.-xref)*xinc
  call func(x,y(1),3)
  do j=2,nxy
     x = x+xinc
     call func(x,y(j),2)
  enddo
  !
end subroutine conne2
!
subroutine conne3(x,y,nxy,func,rbad)
  !----------------------------------------------------------------------
  ! @ private
  ! CLASS  Internal routine
  !   Connect a set of points with blanking.
  !----------------------------------------------------------------------
  integer, intent(in) :: nxy     ! Number of points
  real,    intent(in) :: x(nxy)  ! X values
  real,    intent(in) :: y(nxy)  ! Y values
  real,    intent(in) :: rbad    ! Blanking value
  external            :: func    ! Plotting function
  ! Local
  integer :: jcode, j
  !
  if (nxy.lt.2) return
  if (y(1).ne.rbad) then
     call func(x(1),y(1),3)
     jcode=2
  else
     jcode=3
  endif
  do j = 2,nxy
     if (y(j).ne.rbad) then
        call func(x(j),y(j),jcode)
        jcode=2
     else
        jcode=3
     endif
  enddo
  !
end subroutine conne3
!
subroutine conne4(xval,xref,xinc,y,nxy,func,rbad)
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS  Internal routine
  !  Connect a set of points regularly spaced along the X axis with
  ! blanking
  !---------------------------------------------------------------------
  integer, intent(in) :: nxy     ! Number of points
  real,    intent(in) :: xref    ! X reference pixel
  real,    intent(in) :: xval    ! X value
  real,    intent(in) :: xinc    ! X increment
  real,    intent(in) :: y(nxy)  ! Y values
  external            :: func    ! Plotting function
  real,    intent(in) :: rbad    ! Blanking value
  ! Local
  integer :: j,jcode
  real :: x
  !
  if (nxy.lt.2) return
  x = xval+(1.-xref)*xinc
  if (y(1).ne.rbad) then
     call func(x,y(1),3)
     jcode = 2
  else
     jcode = 3
  endif
  do j=2,nxy
     x = x+xinc
     if (y(j).ne.rbad) then
        call func(x,y(j),jcode)
        jcode = 2
     else
        jcode = 3
     endif
  enddo
  !
end subroutine conne4
!
subroutine histo2(xval,xref,xinc,y,nxy,func)
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS Internal routine
  !   Trace d'Histogramme a partir de donnees X regulierement espacees
  ! et du tableau Y
  !---------------------------------------------------------------------
  integer, intent(in) :: nxy     ! Number of points
  real,    intent(in) :: xref    ! X reference pixel
  real,    intent(in) :: xval    ! X value
  real,    intent(in) :: xinc    ! X increment
  real,    intent(in) :: y(nxy)  ! Y values
  external            :: func    ! Plotting function
  ! Local
  integer :: j
  real :: x
  !
  if (nxy.lt.2) return
  x = xval + (0.5-xref)*xinc
  call func(x,y(1),3)
  do j=2,nxy
     x = x + xinc
     call func(x,y(j-1),2)
     call func(x,y(j),2)
  enddo
  x = x + xinc
  call func(x,y(nxy),2)
  !
end subroutine histo2
