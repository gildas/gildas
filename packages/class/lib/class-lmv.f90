!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine class_lmv(set,line,error,user_function)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_lmv
  use class_parameter
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! CLASS  Support routine for command
  ! LMV CubeName
  !     [/SCAN LAMBDA|BETA]
  !     [/STEP N]
  !     [/LINE LineCube]
  !     [/FORCE What Value]
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  character(len=*),    intent(in)    :: line           ! Command line
  logical,             intent(inout) :: error          ! Logical error flag
  logical,             external      :: user_function  !
  ! Local
  character(len=*), parameter :: rname='LMV'
  character(len=7) :: argum
  integer(kind=4) :: nc
  character(len=filename_length) :: infile,linefile
  integer(kind=4), parameter :: optscan=1
  integer(kind=4), parameter :: optstep=2
  integer(kind=4), parameter :: optline=3
  integer(kind=4), parameter :: optforc=4
  ! /SCAN option
  integer, parameter :: mways=2
  character(len=*), parameter :: lmv_ways(mways) = (/ 'LAMBDA','BETA  ' /)
  character(len=10) :: way
  integer(kind=4) :: nway,lstep,bstep
  ! /FORCE option
  integer, parameter :: mforces=1
  character(len=*), parameter :: lmv_forces(mforces) = (/ 'UNIT' /)
  character(len=10) :: force,unit
  logical :: dounit
  integer(kind=4) :: iforce,yunit
  !
  ! Parse input line
  call sic_ch(line,0,1,infile,nc,.true.,error)  ! Filename
  if (error) return
  !
  ! /SCAN
  nway = 1  ! Default /SCAN LAMBDA
  if (sic_present(optscan,0)) then
    call sic_ke(line,optscan,1,argum,nc,.true.,error)
    if (error) return
    call sic_ambigs(rname,argum,way,nway,lmv_ways,mways,error)
    if (error) return
  endif
  !
  ! /STEP N | Dlambda Dbeta
  lstep = 1  ! Default step 1
  bstep = 1
  if (sic_present(optstep,0)) then
    call sic_i4(line,optstep,1,lstep,.true.,error)
    if (error) return
    !
    bstep = lstep
    call sic_i4(line,optstep,2,lstep,.false.,error)
    if (error) return
    !
    if (bstep.le.0 .or. lstep.le.0) then
      call class_message(seve%e,rname,'Step must be positive')
      error = .true.
      return
    endif
  endif
  !
  ! /FORCE What Value
  dounit = .false.
  if (sic_present(optforc,0)) then
    call sic_ke(line,optforc,1,argum,nc,.true.,error)
    if (error) return
    call sic_ambigs(rname,argum,force,iforce,lmv_forces,mforces,error)
    if (error) return
    select case (iforce)
    case (1)
      dounit = .true.
      call sic_ke(line,optforc,2,argum,nc,.true.,error)
      if (error) return
      call sic_ambigs(rname,argum,unit,yunit,yunit_keys,1+myunit,error)
      if (error)  return
      yunit = yunit-1
    end select
  endif
  !
  ! /LINE
  linefile = ''
  call sic_ch(line,optline,1,linefile,nc,.false.,error)
  if (error)  return
  !
  ! Do the job
  call lmv_read(set,infile,linefile,nway,lstep,bstep,dounit,yunit,  &
    error,user_function)
  !
end subroutine class_lmv
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine lmv_read(set,inname,linename,nway,lstep,bstep,dounit,yunit,  &
  error,user_function)
  use gildas_def
  use image_def
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>lmv_read
  use class_parameter
  use class_types
  use class_common
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS  Support routine for command
  !    LMV DataCube
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  character(len=*),    intent(in)    :: inname         ! Input LMV table
  character(len=*),    intent(in)    :: linename       ! LINE cube (blank if none)
  integer(kind=4),     intent(in)    :: nway           ! Scan through which direction?
  integer(kind=4),     intent(in)    :: lstep          ! Take 1 value other 'lstep' in lambda direction
  integer(kind=4),     intent(in)    :: bstep          ! Take 1 value other 'bstep' in beta direction
  logical,             intent(in)    :: dounit         ! /FORCE UNIT?
  integer(kind=4),     intent(in)    :: yunit          ! Desired unit if /FORCE UNIT
  logical,             intent(inout) :: error          ! Error flag
  logical,             external      :: user_function  !
  ! Local
  character(len=*), parameter :: rname = 'LMV'
  ! Input
  type(gildas) :: hin,hline
  real(kind=4), allocatable :: din(:,:,:),dline(:,:,:)
  integer(kind=4), pointer :: line(:)
  ! Other variables
  character(len=message_length) :: mess
  integer(kind=obsnum_length) :: nobs,nblank,nwritten
  integer(kind=entry_length) :: ient
  type(indx_t) :: ind
  integer(kind=4) :: ier,nscan,nsubscan
  integer(kind=4) :: xdim,ydim,nchan
  integer(kind=4) :: major,mmajor,majorstep, minor,mminor,minorstep, cell,cellstep
  integer(kind=4) :: xaxi,yaxi,faxi,majoraxi,minoraxi
  real(kind=8) :: xref,xval,xinc,yref,yval,yinc
  real(kind=8) :: bval,eval
  integer(kind=4) :: bounds(3,2)
  logical :: continuous,doline
  type(observation) :: obs
  logical :: error2, exist
  character(len=filename_length) :: filename
  !
  ! Check that an output file is connected
  if (.not.fileout_opened(rname,error))  return
  !
  doline = linename.ne.''
  !
  ! Read header (data cube)
  call sic_parse_file(inname,' ','.lmv-clean',filename)
  inquire (file=filename,exist=exist)
  if (.not.exist) then
    call sic_parse_file(inname,' ','.lmv',filename)
  endif
  call class_message(seve%i,rname,'Reading '//trim(filename))
  call class_file_read_gdfhead(rname,filename,hin,.false.,error)
  if (error)  goto 98
  !
  ! Read header (line cube)
  if (doline) then
    call sic_parse_file(linename,' ','.lmv',filename)
    call class_message(seve%i,rname,'Reading '//trim(filename))
    call class_file_read_gdfhead(rname,filename,hline,.false.,error)
    if (error)  goto 98
    if (.not.class_lmv_match(rname,hin,hline)) then
       call class_message(seve%e,rname,'Data cube and line cube mismatch')
       error = .true.
       return
    endif
  endif
  !
  xaxi = hin%gil%xaxi
  yaxi = hin%gil%yaxi
  faxi = hin%gil%faxi
  !
  ! Read data (data cube)
  allocate(din(hin%gil%dim(1),hin%gil%dim(2),hin%gil%dim(3)),stat=ier)
  if (failed_allocate(rname,"input cube",ier,error))  goto 98
  call gdf_read_data (hin,din,error)
  if (gildas_error(hin,rname,error)) goto 98
  !
  ! Read data (line cube)
  if (doline) then
    allocate(dline(hline%gil%dim(1),hline%gil%dim(2),hline%gil%dim(3)),stat=ier)
    if (failed_allocate(rname,"line cube",ier,error))  goto 98
    call gdf_read_data(hline,dline,error)
    if (gildas_error(hline,rname,error)) goto 98
  endif
  !
  ! Blanked values in gildas cube will become bad values in class.
  bval = hin%gil%bval
  eval = hin%gil%eval
  write(mess,*) 'Blanking values are ',bval,eval
  call class_message(seve%d,rname,mess)
  !
  ! Prepare the observation which will be used to write each spectrum
  call init_obs(obs)
  nchan = hin%gil%dim(faxi)
  call reallocate_obs(obs,nchan,error)
  if (error) goto 99
  !
  ! Set the output header constants. Initialize by hand.
  obs%head%xnum = -1  ! Origin of data (here nothing comes from class buffers)
  obs%head%presec(:) = .false.
  obs%head%presec(class_sec_gen_id) = .true.
  obs%head%presec(class_sec_pos_id) = .true.
  obs%head%presec(class_sec_spe_id) = .true.
  obs%head%presec(class_sec_res_id) = hin%gil%reso_words.gt.0
  !
  ! Use the FILE IN VLM API (this avoids duplicating the conversion)
  ! Get a dummy index and fill obs from it
  ient = 1 ! Arbitrary
  call index_fromgdf(hin,ient,ind,error)
  if (error)  goto 99
  call index_toobs(ind,obs%head,error)
  if (error)  goto 99
  !
  if (error)  goto 99
  ! SECTION: Resolution
  if (obs%head%presec(class_sec_res_id)) then
    call rres_gdf(hin,obs,error)
    if (error)  goto 99
  endif
  ! SECTION: Position (remaining part)
  call rpos_gdf(hin,obs,error)
  if (error)  goto 99
  ! SECTION: Spectroscopy (remaining part)
  call rspec_gdf(hin,obs,error)
  if (error)  goto 99
  call abscissa(set,obs,error)  ! Compute X axis. Is this needed?
  if (error)  goto 99
  !
  ! Loop on data
  xdim = hin%gil%dim(xaxi)
  call lmv_get_axis(hin,xaxi,xref,xval,xinc,error)
  if (error) goto 99
  !
  ydim = hin%gil%dim(yaxi)
  call lmv_get_axis(hin,yaxi,yref,yval,yinc,error)
  if (error) goto 99
  !
  ! To be fully generic with LMV, VLM,... cubes, keep track of bounds
  ! of the different axes:
  ! bounds(xaxi,1), bounds(xaxi,2): current lower and upper bounds in X-axis
  ! (and so on).
  bounds(faxi,1) = 1      ! Take allways the whole
  bounds(faxi,2) = nchan  ! range in freq axis
  !
  nobs=0  ! Number of observations encountered
  nblank=0  ! Number of fully blank observations encountered
  nwritten=0  ! Number of obs. written. nobs=nblank+nwritten
  nscan=1
  if (nway.eq.1) then  ! /SCAN LAMBDA
    majoraxi = yaxi  ! Major axis is in the scanning direction
    mmajor = ydim
    majorstep = lstep
    !
    minoraxi = xaxi  ! Minor axis is perpendicular to the scaning direction
    mminor = xdim
    minorstep = bstep
    !
  else                 ! /SCAN BETA
    majoraxi = xaxi
    mmajor = xdim
    majorstep = bstep
    !
    minoraxi = yaxi
    mminor = ydim
    minorstep = lstep
  endif
  cell=1
  cellstep=minorstep
  !
  continuous = .true.  ! Use continuous numbering. .false. means that
                       ! the numbering may be discontinuous e.g. when
                       ! skipping 1 row over N
  !
majorloop: do major=1,mmajor,majorstep
    bounds(majoraxi,1:2) = major
    !
    nsubscan = 1
    do minor=1,mminor,minorstep
      nobs = nobs+1
      bounds(minoraxi,1:2) = cell
      !
      ! --- Read the data ----------------------------------------------
      !
      obs%spectre(:) = reshape (                      &  ! Use reshape() because
                       din(bounds(1,1):bounds(1,2),   &  ! compilers do not know
                           bounds(2,1):bounds(2,2),   &  ! that 2 dimensions over
                           bounds(3,1):bounds(3,2)),  &  ! 3 are degenerated in
                       (/ nchan /) )                     ! this array.
      !         = din(x,y,:) for a LMV
      !
      ! Blanking
      where (abs(obs%spectre-bval).le.eval)
        obs%spectre = bval
      end where
      !
      cell = cell+cellstep
      if (all(obs%spectre.eq.obs%head%spe%bad)) then
        nblank = nblank+1
        cycle  ! All blanked, skip
      endif
      !
      if (doline) then
        ! Beware: the Class standard says the LINE array encodes as:
        !  +1 = signal
        !   0 = no signal
        !  -1 = blanked value
        call class_assoc_add(obs,'LINE',line,error)
        if (error)  goto 99
        ! R*4 to I*4 implicit conversion:
        line(:) = reshape (                        &
                  dline(bounds(1,1):bounds(1,2),   &
                        bounds(2,1):bounds(2,2),   &
                        bounds(3,1):bounds(3,2)),  &
                  (/ nchan /) )
        where (abs(line-hline%gil%bval).le.hline%gil%eval)
          line(:) = -1
        end where
      endif
      !
      ! --- Read and setup the sections --------------------------------
      !
      ! GENERAL: must "reread" from scratch for each iteration because
      ! of convert_scale() which modifies obs%head%gen%yunit
      call rgen_gdf(hin,obs,error)
      if (error)  return
      ! /FORCE UNIT Arg
      ! This patch is specific to command LMV => not in rgen_gdf
      if (dounit) then
        if (obs%head%gen%yunit.ne.yunit_unknown .and.  &
            obs%head%gen%yunit.ne.yunit         .and.  &
            nwritten.eq.0) then
          ! Cube unit was recognized and valid, but user forces it to
          ! something else. Warn (only once):
          call class_message(seve%w,rname,'Unit '//trim(hin%char%unit)//  &
            ' forced to '//obs_yunit(yunit))
        endif
        obs%head%gen%yunit = yunit
      else
        if (obs%head%gen%yunit.eq.yunit_unknown) then
          call class_message(seve%e,rname,'Unknown unit '//hin%char%unit)
          call class_message(seve%e,rname,  &
            'Use option /FORCE UNIT Arg to force unit to the desired value')
          error = .true.
          goto 99
        endif
      endif
      !
      if (.not.fileout%desc%single) then
        ! Output file is multiple: set the observation number by hand
        obs%head%gen%num = nwritten+1
      else
        obs%head%gen%num = 0  ! Automatic in class_write()
      endif
      obs%head%gen%ver   = 0  ! Incremented to 1 hereafter
      if (continuous) then
        obs%head%gen%scan = nscan     ! 1 scan number per column/row
        obs%head%gen%subscan = nscan  ! Same for subscan
      else
        obs%head%gen%scan = major
        obs%head%gen%subscan = minor
      endif
      ! Rescale the Y data to desired unit. Verbose only for first spectrum.
      call convert_scale(set,obs,nwritten.eq.0,error)
      if (error)  goto 99
      !
      ! POSITION: (ZZZ this is actually computed by the index reader)
      obs%head%pos%lamof = (bounds(xaxi,1)-xref)*xinc+xval
      obs%head%pos%betof = (bounds(yaxi,1)-yref)*yinc+yval
      !
      ! --- Write header and data --------------------------------------
      !
      call class_write(set,obs,error,user_function)
      if (error) exit majorloop
      nwritten = nwritten+1
      !
      nsubscan = nsubscan+1
      if (sic_ctrlc()) then
        call class_message(seve%w,rname,'Aborted by ^C')
        error = .true.
        exit majorloop
      endif
      !
      call class_assoc_delete(obs,'LINE',error)
      if (error)  goto 99
      !
    enddo  ! minor
    !
    cell = cell-cellstep  ! Prepare for next scan: reuse last valid
                          ! value because we alternate the scanning path
    cellstep = -cellstep  ! Alternate the scanning path
    !
    if (nsubscan.gt.1)  nscan=nscan+1
    !
  enddo majorloop ! major
  !
  if (error) then
    write(mess,*) 'Error during transfer. Wrote only',nwritten,' observations.'
    call class_message(seve%e,rname,mess)
  elseif (nblank.eq.0) then
    write(mess,100) nwritten,' observations written'
    call class_message(seve%i,rname,mess)
  else
    write(mess,100) nobs,' observations found, ',nwritten,' written, ',  &
      nblank,' fully blank ignored'
    call class_message(seve%i,rname,mess)
  endif
  !
  ! Cleaning
99 call free_obs(obs)
98 if (allocated(din))  deallocate(din)
  error2 = .false.
  call gdf_close_image(hin,error2)
  if (error2)  error = .true.
  if (doline) then
    if (allocated(dline))  deallocate(dline)
    error2 = .false.
    call gdf_close_image(hline,error2)
    if (error2)  error = .true.
  endif
  !
100 format(3(i0,a))
end subroutine lmv_read
!
function class_lmv_match(rname,h1,h2)
  use image_def
  use gbl_message
  use classcore_interfaces, except_this=>class_lmv_match
  !---------------------------------------------------------------------
  ! @ private
  ! Check if the 2 lmv header match
  !---------------------------------------------------------------------
  logical :: class_lmv_match
  character(len=*), intent(in) :: rname
  type(gildas),     intent(in) :: h1,h2
  !
  class_lmv_match = .true.
  !
  if (h1%gil%ndim.ne.h2%gil%ndim) then
    call class_message(seve%e,rname,'Number of dimensions differ')
    class_lmv_match = .false.
    return
  endif
  if (any(h1%gil%dim(1:h1%gil%ndim).ne.  &
          h2%gil%dim(1:h2%gil%ndim))) then
    call class_message(seve%e,rname,'Dimensions differ')
    class_lmv_match = .false.
    return
  endif
  if (any(h1%gil%convert(1:3,1:h1%gil%ndim).ne.  &
          h2%gil%convert(1:3,1:h2%gil%ndim))) then
    call class_message(seve%e,rname,'Ref/val/inc differ')
    class_lmv_match = .false.
    return
  endif
  if (h1%char%name.ne.h2%char%name) then
    call class_message(seve%e,rname,'Source names differ')
    class_lmv_match = .false.
    return
  endif
  ! And more...?
  !
end function class_lmv_match
!
subroutine lmv_get_axis(hin,iaxis,ref,val,inc,error)
  use image_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Return the ref, val, and inc value of i-th axis in input header.
  !---------------------------------------------------------------------
  type(gildas),    intent(in)    :: hin    ! Get axis from this header
  integer(kind=4), intent(in)    :: iaxis  ! Axis number to get
  real(kind=8),    intent(out)   :: ref    ! Reference pixel
  real(kind=8),    intent(out)   :: val    ! Value at reference pixel
  real(kind=8),    intent(out)   :: inc    ! Increment per pixel
  logical,         intent(inout) :: error  ! Error flag
  ! Local
  character(len=message_length) :: mess
  !
  if (iaxis.le.4) then
    ref = hin%gil%ref(iaxis)
    val = hin%gil%val(iaxis)
    inc = hin%gil%inc(iaxis)
  else
    write(mess,*) 'Internal error: no such axis #',iaxis
    call class_message(seve%e,'LMV',mess)
    error = .true.
    return
  endif
  !
end subroutine lmv_get_axis
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine class_file_read_gdfhead(rname,filename,head,velofirst,error)
  use image_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_file_read_gdfhead
  !---------------------------------------------------------------------
  ! @ private
  ! Read a GDF header, check it is suited for use by Class
  ! (position-position-velocity cube or variants), and modify the
  ! header so that it can be imported into the Class spectroscopic
  ! section in rspec_gdf
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: rname      ! Calling routine name
  character(len=*), intent(in)    :: filename   !
  type(gildas),     intent(inout) :: head       !
  logical,          intent(in)    :: velofirst  ! Should the 1st dim be freq/velo?
  logical,          intent(inout) :: error      !
  !
  call gildas_null(head)
  !
  ! Read header
  head%file = filename
  call gdf_read_header(head,error)
  if (gildas_error(head,rname,error))  return
  !
  ! Tolerate 2D images, mimic 3D
  if (head%gil%ndim.eq.2) then
    call class_file_2to3_gdfhead(rname,head,velofirst,error)
    if (error)  return
  endif
  !
  ! Check the file is suited for Class
  call class_file_check_gdfhead(rname,head,velofirst,error)
  if (error)  return
  !
  ! Ensure that the velocity and rest frequency are aligned on the
  ! reference channel (no FOFF in Class)
  call gdf_modify(head,head%gil%voff,head%gil%freq,error=error)
  if (error)  return
  !
end subroutine class_file_read_gdfhead
!
subroutine class_file_2to3_gdfhead(rname,head,velofirst,error)
  use image_def
  use gbl_message
  use classcore_interfaces, except_this=>class_file_2to3_gdfhead
  !---------------------------------------------------------------------
  ! @ private
  !  Extend a 2D spatial to a pseudo 3D cube with Velocity as 3rd
  ! dimension
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: rname      ! Calling routine name
  type(gildas),     intent(inout) :: head       ! Header to be extended
  logical,          intent(in)    :: velofirst  ! Should the 1st dim be freq/velo?
  logical,          intent(inout) :: error      ! Logical error flag
  ! Local
  integer(kind=4) :: faxi
  !
  if (head%gil%ndim.eq.3)  return
  !
  if (head%gil%xaxi*head%gil%yaxi.ne.2) then
    call class_message(seve%e,rname,'GDF has 2 dimensions but they are not spatial dimensions')
    call class_message(seve%e,rname,'Can not import as a pseudo-cube')
    error = .true.
    return
  endif
  !
  if (head%gil%spec_words.le.0) then
    call class_message(seve%e,rname,  &
      'Input image has no spectroscopic section. See HELP LMV FAXIS for solutions.')
    error = .true.
    return
  endif
  !
  call class_message(seve%w,rname,'Importing 2D image as single channel spectra')
  call class_message(seve%w,rname,  &
    'Assuming Voff and rest frequency are aligned at reference channel 1.0')
  !
  head%gil%ndim = 3
  if (velofirst) then
    ! Push the 2 first dimensions as 2nd and 3rd. No assumption on which
    ! one is X and which one is Y.
    call copy_dim(2,3)
    call copy_dim(1,2)
    head%gil%xaxi = head%gil%xaxi+1
    head%gil%yaxi = head%gil%yaxi+1
    faxi = 1
  else
    ! No need to modify the 2 first dimensions
    faxi = 3
  endif
  head%gil%dim(faxi) = 1
  head%gil%faxi = faxi
  head%char%code(faxi) = 'VELOCITY'
  head%gil%ref(faxi) = 1.d0
  head%gil%val(faxi) = head%gil%voff
  head%gil%inc(faxi) = head%gil%vres
  !
contains
  subroutine copy_dim(a,b)
    !-------------------------------------------------------------------
    ! Copy dimension A into B
    !-------------------------------------------------------------------
    integer(kind=4), intent(in) :: a,b
    head%gil%dim(b)   = head%gil%dim(a)
    head%char%code(b) = head%char%code(a)
    head%gil%ref(b)   = head%gil%ref(a)
    head%gil%val(b)   = head%gil%val(a)
    head%gil%inc(b)   = head%gil%inc(a)
  end subroutine copy_dim
  !
end subroutine class_file_2to3_gdfhead
!
subroutine class_file_check_gdfhead(rname,hin,velofirst,error)
  use image_def
  use gbl_message
  use classcore_interfaces, except_this=>class_file_check_gdfhead
  !---------------------------------------------------------------------
  ! @ private
  ! Check if the GDF file is suited for FILE IN in Class
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: rname      ! Calling routine name
  type(gildas),     intent(in)    :: hin        !
  logical,          intent(in)    :: velofirst  ! Should the 1st dim be freq/velo?
  logical,          intent(inout) :: error      !
  ! Local
  character(len=message_length) :: mess
  logical :: onemissing
  !
  if (hin%gil%ndim.gt.3) then
    ! Tolerate trailing degenerate dimensions
    if (product(hin%gil%dim(4:hin%gil%ndim)).ne.1) then
      call class_message(seve%e,rname,  &
        'Input cube has more than 3 non-degenerate dimensions')
      error=.true.
      return
    endif
  elseif (hin%gil%ndim.eq.2) then
    ! We want 3D. Tolerate 1D (can be a 1 x 1 x Nchan transposed cube with
    ! degenerate dimensions stripped). Reject 2D.
    call class_message(seve%e,rname,'GDF file must have 3 dimensions')
    error = .true.
    return
  endif
  !
  if (hin%gil%spec_words.le.0) then
    call class_message(seve%e,rname,  &
      'Input cube has no spectroscopic section. See HELP LMV FAXIS for solutions.')
    error = .true.
    return
  endif
  !
  if (velofirst .and. hin%gil%faxi.ne.1) then
    call class_message(seve%e,rname,'First dimension must be Velocity/Frequency (VLM)')
    error = .true.
    return
  endif
  !
  if (hin%char%code(hin%gil%faxi).ne.'VELOCITY' .and.  &
      hin%char%code(hin%gil%faxi).ne.'FREQUENCY') then
    call class_message(seve%e,rname,'Only VELOCITY or FREQUENCY axes are supported')
    error = .true.
    return
  endif
  !
  if (hin%gil%xaxi*hin%gil%yaxi*hin%gil%faxi.ne.6) then
    write (mess,'(4(A,I0))')  &
      'Axes order not recognized (X axis: ',hin%gil%xaxi,  &
                               ', Y axis: ',hin%gil%yaxi,  &
                               ', F axis: ',hin%gil%faxi,')'
    call class_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  ! Finally sanity check of the spectroscopic section
  onemissing = .false.
  if (hin%gil%freq.le.0.d0) then
    call class_message(seve%e,rname,'Input cube has no rest frequency defined')
    onemissing = .true.
  endif
  if (hin%gil%fres.eq.0.d0) then
    call class_message(seve%e,rname,'Input cube has no frequency resolution')
    onemissing = .true.
  endif
  if (hin%gil%vres.eq.0.) then
    call class_message(seve%e,rname,'Input cube has no velocity resolution')
    onemissing = .true.
  endif
  if (onemissing) then
    call class_message(seve%e,rname,'See HELP LMV FAXIS for solutions')
    error = .true.
    return
  endif
  !
  if (hin%gil%dopp.ne.0.) then
    ! Need a correct conversion from the GDF axes description to Class axes
    ! (they do not use the same Doppler definition). See rspec_gdf where the
    ! conversion is done.
    write(mess,'(A,A,A,1PG12.6,A)')  &
      'File ',trim(hin%file),' has a non-zero Doppler factor (',hin%gil%dopp,')'
    call class_message(seve%e,rname,mess)
    call class_message(seve%e,rname,'Conversion to Class not implemented. Abort.')
    call class_message(seve%e,rname,'Contact gildas@iram.fr for more information')
    error = .true.
    return
  endif
  !
end subroutine class_file_check_gdfhead
!
subroutine index_fromgdf(h,entry_num,ind,error)
  use gildas_def
  use gbl_constant
  use gbl_message
  use image_def
  use classic_api
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>index_fromgdf
  use class_parameter
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Get the index from the header of the input GDF file
  !---------------------------------------------------------------------
  type(gildas),               intent(in)    :: h          !
  integer(kind=entry_length), intent(in)    :: entry_num  ! Entry number
  type(indx_t),               intent(out)   :: ind        !
  logical,                    intent(inout) :: error      ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='RIX'
  integer(kind=4) :: xaxi,yaxi
  integer(kind=index_length) :: xchan,ychan,dims(3),posi(3)
  integer(kind=8) :: flat
  !
  ind%bloc  = 0  ! Not relevant
  ind%word  = 0  ! Not relevant
  ind%num   = entry_num  ! Kind of arbitrary but makes sense
  ind%ver   = 1
  ind%csour = h%char%name
  ind%cline = h%char%line
  ind%ctele = "UNKNOWN"
  if (h%gil%tele_words.gt.0) then
    ! Let "UNKNOWN" if more than 1 telescope declared in the section
    if (h%gil%nteles.eq.1) then
      call my_set_teles(rname,h%gil%teles(1)%ctele,.false.,ind%ctele,error)
      if (error)  return
    endif
  endif
  call sic_gagdate(ind%dobs)
  ind%dred = ind%dobs
  if (h%char%syst.eq.'EQUATORIAL') then
    ind%type = type_eq
  elseif (h%char%syst.eq.'GALACTIC') then
    ind%type = type_ga
  elseif (h%char%syst.eq.'ICRS') then
    ind%type = type_ic
  else
    call class_message(seve%e,rname,'Unknown coordinate system '//h%char%syst)
    error = .true.
    return
  endif
  ind%kind = kind_spec
  ind%qual = qual_unknown
  ind%posa = 0.d0
  ind%ut   = ut_null
  !
  ! Position-dependent values:
  ! 'entry_num' is the position of the spectrum in the file. For a GDF cube,
  ! it is the flat position in the position-position dimensions. This code is
  ! generic for any combinations (VLM, LMV, or other subtle like MVL)
  dims(1:3) = h%gil%dim(1:3)
  dims(h%gil%faxi) = 1  ! Degenerate the Freq/Velo dimension
  ! Translate the flat position-position dimension into pseudo 3D:
  flat = entry_num
  call gdf_index_to_where(flat,3,dims,posi)
  xaxi = h%gil%xaxi
  yaxi = h%gil%yaxi
  xchan = posi(xaxi)
  ychan = posi(yaxi)
  !
  ind%scan = posi(max(xaxi,yaxi))  ! Outter dimension varies slowly
  ind%subscan = posi(min(xaxi,yaxi))  ! Inner dimension varies faster
  ind%off1 = (xchan-h%gil%ref(xaxi))*h%gil%inc(xaxi)+h%gil%val(xaxi)
  ind%off2 = (ychan-h%gil%ref(yaxi))*h%gil%inc(yaxi)+h%gil%val(yaxi)
  !
end subroutine index_fromgdf
!
subroutine rgen_gdf(h,obs,error)
  use classcore_interfaces, except_this=>rgen_gdf
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! "Read" (in this case, mimic) the GENeral section from the GDF
  ! header
  !---------------------------------------------------------------------
  type(gildas),      intent(in)    :: h       ! GDF header
  type(observation), intent(inout) :: obs     ! Header
  logical,           intent(inout) :: error   ! Logical error flag
  !
! obs%head%gen%num     = from index
! obs%head%gen%ver     = from index
! obs%head%gen%teles   = from index
! obs%head%gen%dobs    = from index
! obs%head%gen%dred    = from index
! obs%head%gen%kind    = from index
! obs%head%gen%qual    = from index
! obs%head%gen%scan    = from index
! obs%head%gen%subscan = from index
! obs%head%gen%st      = ???
! obs%head%gen%az      = ???
! obs%head%gen%el      = ???
! obs%head%gen%tau     = ???
  obs%head%gen%tsys    = 0.0  ! Some default
  obs%head%gen%time    = 0.0  ! Some default
  obs%head%gen%parang  = parang_null
  obs%head%gen%yunit = obs_yunit(h%char%unit)
! obs%head%gen%xunit   = ???
!
end subroutine rgen_gdf
!
subroutine rpos_gdf(h,obs,error)
  use gbl_constant
  use image_def
  use gbl_message
  use classcore_interfaces, except_this=>rpos_gdf
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! "Read" (in this case, mimic) the POSition section from the GDF
  ! header
  !---------------------------------------------------------------------
  type(gildas),      intent(in)    :: h      ! GDF header
  type(observation), intent(inout) :: obs    ! Header
  logical,           intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='RPOS'
  !
! obs%head%pos%sourc   = from index
  if (h%char%syst.eq.'EQUATORIAL') then
    obs%head%pos%system = type_eq
  elseif (h%char%syst.eq.'GALACTIC') then
    obs%head%pos%system = type_ga
  elseif (h%char%syst.eq.'ICRS') then
    obs%head%pos%system = type_ic
  else
    call class_message(seve%e,rname,'Unknown coordinate system '//h%char%syst)
    error = .true.
    return
  endif
  obs%head%pos%equinox = h%gil%epoc
  obs%head%pos%lam     = h%gil%a0
  obs%head%pos%bet     = h%gil%d0
! obs%head%pos%lamof   = from index
! obs%head%pos%betof   = from index
  obs%head%pos%projang = h%gil%pang
  obs%head%pos%proj    = h%gil%ptyp
  !
end subroutine rpos_gdf
!
subroutine rspec_gdf(h,obs,error)
  use gbl_message
  use image_def
  use classcore_interfaces, except_this=>rspec_gdf
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! "Read" (in this case, mimic) the SPECtroscopic section from the GDF
  ! header
  !---------------------------------------------------------------------
  type(gildas),      intent(in)    :: h      ! GDF header
  type(observation), intent(inout) :: obs    ! Header
  logical,           intent(inout) :: error  ! Logical error flag
  !
  call gdf2class_spectro(h,obs%head,error)
  if (error)  return
  !
end subroutine rspec_gdf
!
subroutine rres_gdf(h,obs,error)
  use gbl_message
  use classcore_interfaces, except_this=>rres_gdf
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! "Read" (in this case, mimic) the RESolution section from the GDF
  ! header
  !---------------------------------------------------------------------
  type(gildas),      intent(in)    :: h      ! GDF header
  type(observation), intent(inout) :: obs    ! Header
  logical,           intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='RRES'
  !
  ! Should we care here about null/unset beam while this section is
  ! enabled in the GDF header?
  obs%head%res%major  = h%gil%majo
  obs%head%res%minor  = h%gil%mino
  obs%head%res%posang = h%gil%posa
  !
end subroutine rres_gdf
!
subroutine rdata_sub_vlm(entry_num,first,last,nv,values,error)
  use classic_api
  use classcore_dependencies_interfaces
  use class_common
  !---------------------------------------------------------------------
  ! @ private
  !  Read a (subset of) a spectrum from an entry of a GDF file
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in)    :: entry_num   !
  integer(kind=data_length),  intent(in)    :: first       ! First data word to read
  integer(kind=data_length),  intent(in)    :: last        ! Last data word to read
  integer(kind=data_length),  intent(inout) :: nv          ! Input and actual number of values read
  real(kind=4),               intent(out)   :: values(nv)  ! Data array
  logical,                    intent(out)   :: error       ! Logical error flag
  ! Local
  integer(kind=data_length) :: posi(3)
  !
  ! 'entry_num' is the position of the spectrum in the file. For a VLM file,
  ! it is the flat position in the position-position dimensions (2 and 3).
  posi(3) = (entry_num-1)/filein_vlmhead%gil%dim(2) + 1
  posi(2) = entry_num - (posi(3)-1)*filein_vlmhead%gil%dim(2)
  posi(1) = 0  ! Unused
  !
  filein_vlmhead%blc(1:3) = (/ first,posi(2),posi(3) /)
  filein_vlmhead%trc(1:3) = (/ last, posi(2),posi(3) /)
  call gdf_read_data(filein_vlmhead,values,error)
  !
  nv = last-first+1
  !
end subroutine rdata_sub_vlm
!
!-----------------------------------------------------------------------
!  GDF <-> Class filler
!-----------------------------------------------------------------------
!
subroutine gdf2class_spectro(hgdf,hclass,error)
  use gbl_constant
  use image_def
  use classcore_interfaces, except_this=>gdf2class_spectro
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Convert a GDF header to a Class spectroscopic section +
  ! port the measurement frame to the rest frame.
  !---------------------------------------------------------------------
  type(gildas), intent(in)    :: hgdf
  type(header), intent(inout) :: hclass
  logical,      intent(inout) :: error
  !
  hclass%presec(class_sec_spe_id) = .true.
  !
  hclass%spe%line    = hgdf%char%line
  if (hgdf%gil%type_gdf.eq.code_gdf_uvt) then
    hclass%spe%nchan = hgdf%gil%nchan
  else
    hclass%spe%nchan = hgdf%gil%dim(hgdf%gil%faxi)
  endif
  hclass%spe%restf   = hgdf%gil%freq
  if (hgdf%gil%fima.le.0.d0) then
    hclass%spe%image = image_null
  else
    hclass%spe%image = hgdf%gil%fima
  endif
  hclass%spe%doppler = 0.d0 ! Assumes that the measurement frame is the rest frame
  ! Rchan: caller must ensure that Voff and Restf are aligned on the reference
  ! channel (which means in particular that there is no frequency offset between
  ! the convert(:) array and the rest frequency).
  hclass%spe%rchan   = hgdf%gil%ref(hgdf%gil%faxi)
  hclass%spe%fres    = hgdf%gil%fres
  hclass%spe%vres    = dble(hgdf%gil%vres)
  hclass%spe%voff    = dble(hgdf%gil%voff)
  hclass%spe%bad     = hgdf%gil%bval
  hclass%spe%vtype   = hgdf%gil%vtyp
  hclass%spe%vconv   = vconv_unk  ! *** JP It should be radio when we will have converged on this issue.
  hclass%spe%vdire   = vdire_unk
  !                  = hgdf%gil%dopp  ignored / information lost
  !
end subroutine gdf2class_spectro
!
subroutine class2gdf_spectro(hclass,faxi,hgdf,error)
  use gbl_constant
  use image_def
  use classcore_interfaces, except_this=>class2gdf_spectro
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Convert a Class spectroscopic section to (part of) a GDF header
  !---------------------------------------------------------------------
  type(header),    intent(in)    :: hclass
  integer(kind=4), intent(in)    :: faxi
  type(gildas),    intent(inout) :: hgdf
  logical,         intent(inout) :: error
  !
  hgdf%gil%spec_words = 14
  !
  ! Set up a VELOCITY header (no option for something else)
  hgdf%gil%faxi = faxi
  hgdf%char%code(faxi) = 'VELOCITY'
  hgdf%gil%dim(faxi) = hclass%spe%nchan
  hgdf%gil%ref(faxi) = hclass%spe%rchan
  hgdf%gil%val(faxi) = hclass%spe%voff
  hgdf%gil%inc(faxi) = hclass%spe%vres
  !
  ! Generic header elements
  hgdf%char%line = hclass%spe%line
  hgdf%gil%freq  = hclass%spe%restf
  hgdf%gil%fima  = hclass%spe%image  ! Can be image_null
  hgdf%gil%dopp  = 0.d0 ! Doppler from Earth to Inertial frame defined with hclass%spe%vtype
  hgdf%gil%fres  = hclass%spe%fres/(1.d0+hclass%spe%doppler)  ! Always in source frame
  hgdf%gil%vres  = hclass%spe%vres
  hgdf%gil%voff  = hclass%spe%voff
  hgdf%gil%vtyp  = hclass%spe%vtype
  !
end subroutine class2gdf_spectro
