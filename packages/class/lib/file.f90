subroutine class_file(set,line,error)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_file
  use class_index
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! LAS Support routine for command
  ! FILE IN|OUT|BOTH|UPDATE Filename [SINGLE|MULTIPLE]
  !      [/OVERWRITE]
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)  :: set    !
  character(len=*),    intent(in)  :: line   ! Command line
  logical,             intent(out) :: error  ! Error status
  ! Local
  character(len=*), parameter :: rname='FILE'
  logical :: new,single,overwri
  character(len=8) :: ch
  character(len=filename_length) :: chain,fich
  integer(kind=4), parameter :: mvoc1=4
  integer(kind=4), parameter :: mvoc2=4
  character(len=8) :: voc1(mvoc1),voc2(mvoc2),key1,key2
  integer(kind=4) :: nc,nfile,n1,n2
  integer(kind=4), parameter :: optover=1  ! /OVERWRITE
  ! Data
  data voc1/'IN','OUT','BOTH','UPDATE'/
  data voc2/'MULTIPLE','SINGLE','OLD','NEW'/
  !
  call sic_ke(line,0,1,ch,nc,.true.,error)
  if (error) return
  call sic_ambigs(rname,ch,key1,n1,voc1,mvoc1,error)
  if (error) return
  !
  if (.not.sic_present(0,2)) then
    call class_message(seve%e,rname,'No default is provided for the file name')
    error = .true.
    return
  endif
  call sic_ch(line,0,2,chain,nc,.true.,error)
  if (error) return
  call sic_parse_file(chain,' ',set%defext,fich)
  nfile = lenc(fich)
  !
  ch = 'OLD'  ! Default if no Type provided
  call sic_ke(line,0,3,ch,nc,.false.,error)
  if (error) return
  call sic_ambigs(rname,ch,key2,n2,voc2,mvoc2,error)
  if (error) return
  if (key2.eq.'NEW') then
    call class_message(seve%e,rname,'Syntax FILE OUT File NEW is obsolete. '//  &
      'See HELP FILE for details.')
    error=.true.
    return
  endif
  new = key2.ne.'OLD'
  single = key2.eq.'SINGLE'
  !
  overwri = sic_present(optover,0)
  !
  if (key1.eq.'IN') then
    if (overwri) then
      call class_message(seve%e,rname,'Option /OVERWRITE invalid in this context')
      error = .true.
      return
    endif
    call classcore_filein_open(fich,nfile,error)
    if (error) return
    ! Input file has changed: reset ranges flag to false
    last_xnum = 0
    ix%ranges%done = .false.
    !
  elseif (key1.eq.'OUT') then
    if (new) then
      call classcore_fileout_new(set,fich,nfile,class_idx_size,single,overwri,error)
      if (error) return
    else
      if (gag_inquire(fich,nfile).ne.0 .and. .not.sic_present(0,3)) then
        call class_message(seve%e,rname,'Missing Type argument for new '//  &
        'file '//fich)
        error=.true.
        return
      endif
      call classcore_fileout_old(set,fich,nfile,.false.,error)
      if (error) return
    endif
    ! Output file has changed: reset ranges flag to false
    ox%ranges%done = .false.
    !
  else  ! key1.eq.'BOTH' .or. key1.eq.'UDPATE'
    if (overwri) then
      call class_message(seve%e,rname,'Option /OVERWRITE invalid in this context')
      error = .true.
    endif
    if (new) then
      call class_message(seve%e,rname,'Argument '//key2//' invalid in this context')
      error = .true.
    endif
    if (error) return
    call classcore_filein_open(fich,nfile,error)
    if (error) return
    ! Files have changed: reset ranges flag to false
    last_xnum = 0
    ix%ranges%done = .false.
    call classcore_fileout_old(set,fich,nfile,key1.eq.'UPDATE',error)
    if (error) return
    ! Files have changed: reset ranges flag to false
    ox%ranges%done = .false.
  endif
  !
end subroutine class_file
