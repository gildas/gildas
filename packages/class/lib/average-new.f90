subroutine class_average(set,line,obs,error,user_function)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_average
  use class_averaging
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! CLASS Support routine for command AVERAGE
  ! AVERAGE
  !   1  [/NOMATCH]
  !   2  [/RESAMPLE]
  !   3  [/NOCHECK]
  !   4  [/WEIGHT]
  ! * Average scans of current index together with various weights and
  !   options.
  ! * Update the origin information and reset the plotting constants
  !---------------------------------------------------------------------
  type(class_setup_t), intent(inout) :: set            !
  character(len=*),    intent(in)    :: line           ! Input command line
  type(observation),   intent(inout) :: obs            !
  logical,             intent(inout) :: error          ! Logical error flag
  logical,             external      :: user_function  ! Routine to handle the User sections
  ! Local
  integer(kind=4), parameter :: optnomatch=1
  integer(kind=4), parameter :: optresample=2
  integer(kind=4), parameter :: optnocheck=3
  integer(kind=4), parameter :: optweight=4
  type(average_t) :: aver
  !
  aver%comm%rname = 'AVERAGE'
  aver%comm%nomatch = sic_present(optnomatch,0)  ! /NOMATCH
  aver%comm%resample = sic_present(optresample,0)  ! /RESAMPLE
  aver%comm%optresample = optresample
  aver%comm%nocheck = sic_present(optnocheck,0)  ! /NOCHECK
  aver%comm%optnocheck = optnocheck
  aver%comm%optweight = optweight
  aver%comm%stitch = .false.
  aver%comm%stitch_image = .false.
  aver%comm%rms = .false.
  call average_generic(set,aver,line,obs,error,user_function)
  !
end subroutine class_average
!
subroutine class_stitch(set,line,obs,error,user_function)
  use gbl_message
  use gbl_constant
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_stitch
  use class_averaging
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! CLASS Support routine for command STITCH
  ! STITCH
  !   1  [/NOMATCH]
  !   2  [/RESAMPLE]
  !   3  [/IMAGE]
  !   4  [/LINE]
  !   5  [/TELESCOPE]
  !   6  [/NOCHECK]
  !   7  [/WEIGHT]
  !---------------------------------------------------------------------
  type(class_setup_t), intent(inout) :: set            !
  character(len=*),    intent(in)    :: line           ! Input command line
  type(observation),   intent(inout) :: obs            !
  logical,             intent(inout) :: error          ! Logical error flag
  logical,             external      :: user_function  ! Routine to handle the User sections
  ! Local
  integer(kind=4), parameter :: optnomatch=1
  integer(kind=4), parameter :: optresample=2
  integer(kind=4), parameter :: optimage=3
  integer(kind=4), parameter :: optline=4
  integer(kind=4), parameter :: opttelescope=5
  integer(kind=4), parameter :: optnocheck=6
  integer(kind=4), parameter :: optweight=7
  type(average_t) :: aver
  integer(kind=4) :: nchain
  character(len=12) :: newline,newtele
  !
  if (set%kind.ne.kind_spec) then
     call class_message(seve%w,'STITCH','Unsupported kind of data')
     error = .true.
     return
  endif
  !
  aver%comm%rname = 'STITCH'
  aver%comm%nomatch = sic_present(optnomatch,0)  ! /NOMATCH
  aver%comm%resample = sic_present(optresample,0)  ! /RESAMPLE
  aver%comm%optresample = optresample
  aver%comm%nocheck = sic_present(optnocheck,0)  ! /NOCHECK
  aver%comm%optnocheck = optnocheck
  aver%comm%optweight = optweight
  aver%comm%stitch = .true.
  aver%comm%stitch_image = sic_present(optimage,0) ! /IMAGE
  aver%comm%rms = .false.
  call average_generic(set,aver,line,obs,error,user_function)
  if (error)  return
  !
  ! Post processing header tuning
  if (sic_present(optline,0)) then
     ! Parse the new line name
     call sic_ch(line,optline,1,newline,nchain,.true.,error)
     if (error) return
     call sic_upper(newline)
     obs%head%spe%line = newline
  endif
  if (sic_present(opttelescope,0)) then
     ! Parse the new telescope name
     call sic_ch(line,opttelescope,1,newtele,nchain,.true.,error)
     if (error) return
     call sic_upper(newtele)
     obs%head%gen%teles = newtele
  endif
  !
end subroutine class_stitch
!
subroutine class_rms(set,line,obs,error,user_function)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_rms
  use class_averaging
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! CLASS Support routine for command RMS
  ! RMS
  !   1  [/NOCHECK]
  !   2  [/RESAMPLE]
  !   3  [/WEIGHT]
  ! * Average and rms scans of current index together with various
  !   weights and options.
  ! * Update the origin information and reset the plotting constants
  !---------------------------------------------------------------------
  type(class_setup_t), intent(inout) :: set            !
  character(len=*),    intent(in)    :: line           ! Input command line
  type(observation),   intent(inout) :: obs            !
  logical,             intent(inout) :: error          ! Logical error flag
  logical,             external      :: user_function  ! Routine to handle the User sections
  ! Local
  integer(kind=4), parameter :: optnocheck=1
  integer(kind=4), parameter :: optresample=2
  integer(kind=4), parameter :: optweight=3
  type(average_t) :: aver
  !
  aver%comm%rname = 'RMS'
  aver%comm%nomatch = .false.
  aver%comm%resample = sic_present(optresample,0)  ! /RESAMPLE
  aver%comm%optresample = optresample
  aver%comm%nocheck = sic_present(optnocheck,0)  ! /NOCHECK
  aver%comm%optnocheck = optnocheck
  aver%comm%optweight = optweight
  aver%comm%stitch = .false.
  aver%comm%stitch_image = .false.
  aver%comm%rms = .true.
  call average_generic(set,aver,line,obs,error,user_function)
  !
end subroutine class_rms
!
subroutine average_generic(set,aver,line,obs,error,user_function)
  use gildas_def
  use gbl_message
  use gbl_constant
  use classcore_interfaces, except_this=>average_generic
  use class_averaging
  use class_data
  use class_index
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS Internal routine
  !    AVERAGE all scans of current index together with
  !    various weights and options. Update the origin information and
  !    reset the plotting constants
  !---------------------------------------------------------------------
  type(class_setup_t), intent(inout) :: set            !
  type(average_t),     intent(inout) :: aver           !
  character(len=*),    intent(in)    :: line           ! Input command line
  type(observation),   intent(inout) :: obs            ! Output observation
  logical,             intent(inout) :: error          ! Logical error flag
  logical,             external      :: user_function  !
  ! Local
  type(observation) :: sumio
  !
  ! Verify if any input file
  if (.not.filein_opened(aver%comm%rname,error))  return
  if (cx%next.le.1) then
     call class_message(seve%e,aver%comm%rname,'Index is empty')
     error = .true.
     return
  endif
  !
  ! Work in a local 'observation' (instead of the output one) so that we
  ! don't leave the output in inconsistent shape in case of intermediate
  ! error.
  call init_obs(sumio)
  !
  if (cx%next.eq.2) then
    ! Only one spectrum in the index
     call average_one(set,aver,sumio,error,user_function)
  else
     call average_many(set,aver,line,sumio,error,user_function)
  endif
  if (error)  goto 10
  !
  ! Post processing
  call abscissa(set,sumio,error)
  if (error)  goto 10
  !
  ! Now that 'sumio' is fully ready, copy it in output observation
  call copy_obs(sumio,obs,error)
  if (error)  goto 10
  !
  call newdat(set,obs,error)
  call newdat_assoc(set,obs,error)
  call newdat_user(set,obs,error)
  if (error)  goto 10
  !
10 continue
  !
  ! Free memory
  call free_obs(sumio)
  !
end subroutine average_generic
!
subroutine average_one(set,aver,sumio,error,user_function)
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>average_one
  use class_averaging
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  type(average_t),     intent(in)    :: aver           !
  type(observation),   intent(inout) :: sumio          !
  logical,             intent(inout) :: error          ! Logical error flag
  logical,             external      :: user_function  !
  ! Local
  character(len=message_length) :: mess
  type(observation) :: obs
  character(len=15) :: kind_str
  !
  ! Read the first (and unique) observation into OBS
  call init_obs(obs)
  call get_first(set,obs,user_function,error)
  if (error)  goto 10
  !
  ! Reject wrong kinds
  if (obs%head%gen%kind.ne.set%kind) then
    if (set%kind.eq.kind_spec) then
      kind_str = 'SPECTROSCOPY'
    elseif (set%kind.eq.kind_cont) then
      kind_str = 'CONTINUUM'
    else
      kind_str = 'UNKNOWN'
    endif
    write (mess,'(A,I0,A)')  &
      'Observation #',obs%head%gen%num,' has wrong type (expect ',  &
      trim(kind_str),')'
    call class_message(seve%e,aver%comm%rname,mess)
    error = .true.
    goto 10
  endif
  !
  call class_message(seve%w,aver%comm%rname,'Only one spectrum in index!')
  if (aver%comm%resample)  &
    call class_message(seve%w,aver%comm%rname,'/RESAMPLE ignored, single spectrum not modified')
  !
  call copy_obs(obs,sumio,error)
  if (error) then
    call class_message(seve%e,aver%comm%rname,'Could not copy input spectrum')
    goto 10
  endif
  !
  if (aver%comm%rms)  sumio%spectre(1:sumio%cnchan) = 0.
  !
10 continue
  !
  call free_obs(obs)
  !
end subroutine average_one
!
subroutine average_many(set,aver,line,sumio,error,user_function)
  use gbl_constant
  use gbl_message
  use gkernel_types
  use gkernel_interfaces
  use classcore_interfaces, except_this=>average_many
  use class_averaging
  use class_index
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(class_setup_t), intent(inout) :: set            !
  type(average_t),     intent(inout) :: aver           !
  character(len=*),    intent(in)    :: line           ! Input command line
  type(observation),   intent(inout) :: sumio          !
  logical,             intent(inout) :: error          ! Logical error flag
  logical,             external      :: user_function  !
  ! Local
  type(observation) :: obs,rmsio
  integer(kind=entry_length) :: nobs,iobs
  integer(kind=4) :: save_vtype,nchan,nc
  logical :: kind_is_spec
  type(time_t) :: time
  !
  if (aver%comm%nomatch) then   ! /NOMATCH
    ! Obsolete for STITCH and AVERAGE /NEW since 07-mar-2012
    call class_message(seve%e,aver%comm%rname,'Option /NOMATCH is obsolete (ignored). Use instead:')
    call class_message(seve%e,aver%comm%rname,'SET NOMATCH to disable match of position offsets, or')
    call class_message(seve%e,aver%comm%rname,'/NOCHECK POSITION to disable check of position information')
    error = .true.
    return
  endif
  !
  kind_is_spec = set%kind.eq.kind_spec
  save_vtype = set%veloc
  !
  ! Read the first observation into OBS
  call init_obs(obs)
  ! Fill header structure from buffer. Data not needed at this stage.
  call rheader(set,obs,cx%ind(1),user_function,error)
  if (error)  goto 10
  !
  ! Temporarily change some default settings
  if (kind_is_spec) then
    if (set%veloc.eq.vel_aut)  set%veloc = obs%head%spe%vtype
  endif
  !
  ! 1) Prepare the addition
  if (aver%comm%resample) then
    if (sic_present(aver%comm%optresample,1)) then
      ! /RESAMPLE Nchan Xref Xval Xinc Unit
      aver%do%resampling = resample_user
      call resample_parse_command(line,aver%comm%optresample,aver%comm%rname,obs%head,aver%do%axis,error)
      if (error)  return
      !
      if (aver%comm%stitch) then
        if (aver%do%axis%unit.ne.'F' .and. aver%do%axis%unit.ne.'I') then
          call class_message(seve%e,aver%comm%rname,  &
            'Custom X-axis must be in signal or image frequency unit')
          error = .true.
          return
        endif
      endif
    else
      ! /RESAMPLE
      aver%do%resampling = resample_auto
    endif
    !
  elseif (aver%comm%stitch) then
    ! /STITCH: automatic resampling
    aver%do%resampling = resample_auto
    !
  else
    ! No /RESAMPLE option
    aver%do%resampling = resample_no
  endif
  !
  aver%do%rname = aver%comm%rname
  aver%do%kind = set%kind
  aver%do%weight = set%weigh
  if (sic_present(aver%comm%optweight,0)) then
    call sic_ke(line,aver%comm%optweight,1,aver%do%weight,nc,.true.,error)
    if (error)  return
  endif
  if (aver%comm%stitch_image) then
    aver%do%alignment = align_imag
  elseif (aver%comm%stitch) then
    aver%do%alignment = align_freq
  else
    aver%do%alignment = align_auto
  endif
  if (aver%comm%stitch) then
    aver%do%composition = composite_union  ! Force union
  else
    aver%do%composition = composite_user  ! User's choice
  endif
  aver%do%range = .false.  ! No option /RANGE
  ! If you plan to enable the following tunings in AVERAGE/ACCUMULATE/STITCH
  ! please think twice because the historical behavior of these commands is
  ! to realign the frequency by hand, i.e. change the restf
  aver%do%modfreq = .false. !
  aver%do%restf   = 0.d0    ! Not used
  aver%do%modvelo = .false. !
  aver%do%voff    = 0.d0    ! Not used
  !
  ! 2) Select what will be checked according to user input
  call consistency_check_selection(set,line,aver%comm%optnocheck,cx%cons,error)
  if (error) return
  if (aver%comm%stitch)  cx%cons%lin%check = .false.
  if (set%kind.eq.kind_spec) then
    ! Consistency of the spectroscopic axes is enabled by default (but the user
    ! can deactivate it, to his own risk). Note that this check is useful if
    ! /RESAMPLE is present (resampling automatically disabled if useless) or
    ! not (request for enabling resampling in case of inconsistency).
    ! No need for inconsistency messages if /RESAMPLE is enabled: user
    ! already knows the axes can be inconsistent!
    cx%cons%spe%mess = aver%do%resampling.eq.resample_no
  elseif (set%kind.eq.kind_cont) then
    cx%cons%dri%mess = aver%do%resampling.eq.resample_no
  else
    call class_message(seve%e,aver%comm%rname,'Unsupported kind of data')
    error = .true.
    return
  endif
  ! Set checking tolerances
  call consistency_tole(obs%head,cx%cons)
  ! Give user feedback on what will be done
  call consistency_print(set,obs%head,cx%cons)
  !
  ! 3) Run through the index and set the sum header.
  call sumlin_header(set,aver,obs%head,cx%cons,sumio,error,user_function)
  if (error)  goto 10
  !
  ! --- AVERAGE start --------------------------------------------------
  !
  ! 4) Now, the data. Prepare.
  call sumlin_data_prepro(aver,sumio,obs%assoc,error)
  if (error)  goto 10
  !
  ! 5) Loop on the observations to average them
  ! Prepare 'obs' to store any of the spectra data (may be of various
  ! length, use the maximum)
  call reallocate_obs(obs,aver%done%nchanmax,error)
  if (error)  goto 10
  !
  nobs = cx%next-1
  !
  call gtime_init(time,nobs,error)
  if (error)  return
  !
  do iobs = 1,nobs
    call gtime_current(time)
    !
    ! Fill header structure from buffer.
    call rheader(set,obs,cx%ind(iobs),user_function,error)
    if (error)  goto 10
    !
    ! if (aver%do%modfreq) ...
    ! if (aver%do%modvelo) ...
    !
    if (kind_is_spec) then
      nchan = obs%head%spe%nchan
    else
      nchan = obs%head%dri%npoin
    endif
    ! Read data
    call rdata(set,obs,nchan,obs%data1,error)
    if (error)  goto 10
    !
    call sumlin_wadd_new(set,obs,aver,sumio,error)
    if (error)  goto 10
    !
    call class_controlc(aver%comm%rname,error)
    if (error)  goto 10
  enddo
  !
  call sumlin_data_postpro_waverage(aver,sumio,error)
  if (error)  continue
  sumio%spectre => sumio%data1
  !
  ! --- AVERAGE end ----------------------------------------------------
  !
  if (aver%comm%rms) then
  !
  ! --- RMS start ------------------------------------------------------
  !
  ! Header
  call init_obs(rmsio)
  rmsio%head = sumio%head
  !
  call sumlin_data_prepro(aver,rmsio,obs%assoc,error)
  if (error)  goto 20
  !
  call gtime_init(time,nobs,error)
  if (error)  goto 20
  !
  do iobs = 1,nobs
    call gtime_current(time)
    !
    ! Fill header structure from buffer.
    call rheader(set,obs,cx%ind(iobs),user_function,error)
    if (error)  goto 20
    !
    ! if (aver%do%modfreq) ...
    ! if (aver%do%modvelo) ...
    !
    if (kind_is_spec) then
      nchan = obs%head%spe%nchan
    else
      nchan = obs%head%dri%npoin
    endif
    ! Read data
    call rdata(set,obs,nchan,obs%data1,error)
    if (error)  goto 20
    !
    call sumlin_wrms(set,obs,aver,sumio,rmsio,error)
    if (error)  goto 20
    !
    call class_controlc(aver%comm%rname,error)
    if (error)  goto 20
  enddo
  !
  call sumlin_data_postpro_wrms(aver,rmsio,error)
  if (error) goto 20
  rmsio%spectre => rmsio%data1
  !
  call copy_obs(rmsio,sumio,error)
  if (error)  goto 20
  !
20 continue
  call free_obs(rmsio)
  !
  ! --- RMS end --------------------------------------------------------
  !
  endif  ! Aver%comm%rms
  !
  ! Convert temporary R*4 Associated Arrays into correct format
  call copy_assoc_r4toaa(aver%comm%rname,obs%assoc,sumio%assoc,error)
  if (error)  continue
  !
10 continue
  !
  call free_obs(obs)
  !
  set%veloc = save_vtype
  !
end subroutine average_many
