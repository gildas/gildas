subroutine minpoi(npar,g,f,x,iflag,obs)
  use classfit_interfaces, except_this=>minpoi
  use class_types
  !----------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  ! Function to be minimized in the gaussian fit.
  ! By using 3 hidden parameters, the method allows dependent and
  ! independent gaussians to be fitted. The computation is highly
  ! optimised, but take care of R*4 and R*8 values !... Basic parameters
  ! are Area, Position and Width.
  !----------------------------------------------------------------------
  integer(kind=4),   intent(in)  :: npar     ! Number of parameters
  real(kind=8),      intent(out) :: g(npar)  ! Array of derivatives
  real(kind=8),      intent(out) :: f        ! Function value
  real(kind=8),      intent(in)  :: x(npar)  ! Input parameter values
  integer(kind=4),   intent(in)  :: iflag    ! Code operation
  type(observation), intent(in)  :: obs      !
  ! Local
  integer(kind=4) :: nxy
  !
  nxy = obs%cimax-obs%cimin+1
  call minspo(obs,npar,g,f,x,iflag,obs%datax(obs%cimin:obs%cimax),  &
    obs%spectre(obs%cimin:obs%cimax),nxy)
end subroutine minpoi
!
subroutine minspo(obs,npar,g,f,x,iflag,tx,ty,ndata)
  use classfit_interfaces, except_this=>minspo
  use class_types
  use gauss_parameter
  !----------------------------------------------------------------------
  ! @ private
  !  Function to be minimized in the gaussian fit.
  !  By using 3 hidden parameters, the method allows dependent and
  ! independent gaussians to be fitted. The computation is highly
  ! optimised, but take care of R*4 and R*8 values !... Basic parameters
  ! are Area, Position and Width.
  !----------------------------------------------------------------------
  type(observation),     intent(in)  :: obs        !
  integer(kind=4),       intent(in)  :: npar       ! Number of parameters
  real(kind=8),          intent(out) :: g(npar)    ! Array of derivatives
  real(kind=8),          intent(out) :: f          ! Function value
  real(kind=8),          intent(in)  :: x(npar)    ! Parameter values
  integer(kind=4),       intent(in)  :: iflag      ! Code operation
  integer(kind=4),       intent(in)  :: ndata      ! Number of channels
  real(kind=xdata_kind), intent(in)  :: tx(ndata)  ! Abscissa
  real(kind=4),          intent(in)  :: ty(ndata)  ! Ordinate
  ! Local
  logical :: dograd,dobase
  real(kind=4) :: tt,vv,dd,gt,gv,gd,t1,v1,d1,g1,g2,g3,t2,v2,d2,g4,g5,g6
  real(kind=4) :: xvel,arg1,f1,ff,arg2,f2,arg,ybas,yrai,ta,gs,go,ss,oo
  integer(kind=4) :: i,kbas,krai
  !
  ! Final computations
  if (iflag.eq.3) then
     kbas=0
     ybas=0.
     krai=0
     yrai=0.
     !
     do i=1,ndata
        if (wfit(i).ne.0) then
           ta = propoi(obs,real(tx(i),4),0,dobase)
           if (dobase) then
              kbas=kbas+1
              ybas=ybas+ty(i)**2
           else
              krai=krai+1
              yrai=yrai+(ta-ty(i))**2
           endif
        endif
     enddo

     if (kbas.gt.5) then
        sigbas=sqrt(ybas/kbas)
     else
        sigbas=0.d0
     endif
     if (krai.ne.0) then
        sigrai=sqrt(yrai/krai)
     else
        sigrai=sigbas
     endif
     if (sigbas.eq.0) sigbas = sigrai
     return
  endif
  !
  dograd = iflag.eq.2
  !
  f=0.
  oo=x(4)                    ! Offset
  ss=x(5)                    ! Slope
  tt=x(1)                    ! Area
  vv=x(2)                    ! Position
  dd=x(3)                    ! Width
  go=0.
  gs=0.
  gt=0.
  gv=0.
  gd=0.
  t1=x(6)*tt
  v1=x(7)+vv
  d1=x(8)*dd
  if (nline.eq.2) then
     t2=x(9)*tt
     v2=x(10)+vv
     d2=x(11)*dd
     g4=0.
     g5=0.
     g6=0.
  endif
  g1=0.
  g2=0.
  g3=0.
  !
  !	g1 derivee / aire
  !	g2 derivee / vlsr
  !	g3 derivee / delt
  !
  ! Compute gaussians
  do i=1,ndata
     if (wfit(i).eq.0) goto 600
     xvel = tx(i)
     ! First
     arg1 = (xvel - v1) / d1
     if (abs(arg1).gt.4.) then
        f1 = 0.
        ff = 0.
     else
        f1 = exp(-arg1**2)
        ff = f1 * t1 / d1
     endif
     if (nline.le.1) goto 610
     ! Second
     arg2 = (xvel - v2) / d2
     if (abs(arg2).gt.4.) then
        f2=0.
     else
        f2 = exp(-arg2**2)
        ff = ff + f2 * t2 / d2
     endif
     !
610  continue
     !
     ! Compute Chi-2
     ff =  (ff + oo + xvel*ss - ty(i))
     f = f + ff**2
     !
     ! Compute Gradients
     if (.not.dograd) goto 620
     ff = 2.*ff
     ! Offset and Slope
     gs = gs + xvel * ff
     go = go + ff
     ! First
     if (f1.ne.0.) then
        arg = f1*ff/d1
        g1  = g1 + arg
        gt  = gt + arg*t1
        arg = t1/d1*arg
        g3  = g3 - arg
        gd  = gd - arg*d1
        arg = arg*arg1*2.
        g2  = g2 + arg
        gv  = gv + arg
        g3  = g3 + arg*arg1
        gd  = gd + arg*arg1*d1
     endif
     if (nline.le.1) goto 620
     ! Second
     if (f2.ne.0.) then
        arg = f2*ff/d2
        g4  = g4 + arg
        gt  = gt + arg*t2
        arg = arg*t2/d2
        g6  = g6 - arg
        gd  = gd - arg*d2
        arg = arg*arg2*2.
        g5  = g5 + arg
        gv  = gv + arg
        g6  = g6 + arg*arg2
        gd  = gd + arg*arg2*d2
     endif
     !
620  continue
     !
600  continue
  enddo
  !
  ! Setup values and return
  f = f
  g(4)=go
  g(5)=gs
  g(1)=gt/tt
  g(2)=gv
  g(3)=gd/dd
  !
  g(6)=g1*tt
  g(7)=g2
  g(8)=g3*dd
  if (nline.lt.2) return
  g(9)=g4*tt
  g(10)=g5
  g(11)=g6*dd
end subroutine minspo
