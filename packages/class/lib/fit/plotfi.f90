subroutine plotfit(set,line,obs,error)
  use gbl_message
  use classfit_dependencies_interfaces
  use classfit_interfaces, except_this=>plotfit
  use class_types
  !----------------------------------------------------------------------
  ! @ public
  ! FIT\ Support routine for command
  ! VISUALIZE [M]
  ! 1         [/PEN [i]]
  !----------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  character(len=*),    intent(in)    :: line
  type(observation),   intent(in)    :: obs
  logical,             intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='VISUALIZE'
  integer :: m,oldpen,local,i
  logical :: local_pen
  character(len=50) :: chain
  !
  ! Change pen to Green if needed
  local_pen = sic_present(1,1)
  if (sic_present(1,0)) then
     if (local_pen) then
        ! get pen color from option arguments
        call sic_i4(line,1,1,local,.false.,error)
        if (error) return
        oldpen = gr_spen(local)
        if (gr_error()) then
           error = .true.
           return
        endif
     else
        ! restore pen color set by user
        oldpen = gr_spen(2)
        i = gr_spen(oldpen)
        if (gr_error()) then
           error = .true.
           return
        endif
     endif
  else
     oldpen = gr_spen(2)
     if (gr_error()) then
        error = .true.
        return
     endif
  endif
  !
  ! Select fit component
  if (sic_present(0,1)) then
     call sic_i4 (line,0,1,m,.false.,error)
     if (error) return
     m = max(m,0)
  else
     m = 0
  endif
  !
  call gr_segm('VISUALIZE',error)
  !
  select case (set%method)
  case ('CONTINUUM')
    if (obs%head%presec(class_sec_poi_id)) then
      call plot_curr(propoi,obs,m)
    else
      write(chain,100) 'Missing section ',set%method
      call class_message(seve%w,rname,chain)
    endif
  case ('GAUSS')
    if (obs%head%presec(class_sec_gau_id)) then
      call plot_curr(progauss,obs,m)
    else
      write(chain,100) 'Missing section ',set%method
      call class_message(seve%w,rname,chain)
    endif
  case ('NH3','HFS')
    if (obs%head%presec(class_sec_hfs_id)) then
      call plot_curr(pronh3,obs,m)
    else
      write(chain,100) 'Missing section ',set%method
      call class_message(seve%w,rname,chain)
    endif
  case ('ABSORPTION')
    if (obs%head%presec(class_sec_abs_id)) then
      call plot_curr(proabs,obs,m)
    else
      write(chain,100) 'Missing section ',set%method
      call class_message(seve%w,rname,chain)
    endif
  case ('SHELL')
    if (obs%head%presec(class_sec_she_id)) then
      call plot_freq(proshell,obs,m)
    else
      write(chain,100) 'Missing section ',set%method
      call class_message(seve%w,rname,chain)
    endif
  end select
  !
  call gr_segm_close(error)
  !
  ! Restore former pen
  i=gr_spen(oldpen)
  !
100 format (1x,a,a,a,a)
end subroutine plotfit
!
subroutine plot_curr(profil,obs,m)
  use classfit_interfaces, except_this=>plot_curr
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  ! FIT\ Internal routine for command
  !   VISUALIZE [M]
  ! Plot the sum of all components of a fitted profile (M = 0) or
  ! the Mth component only (M .ne. 0)
  !----------------------------------------------------------------------
  interface
    function profil(obs,x0,m,dobase)
    use class_types
    real(kind=4) :: profil
    type(observation), intent(in)  :: obs
    real(kind=4),      intent(in)  :: x0
    integer(kind=4),   intent(in)  :: m
    logical,           intent(out) :: dobase
    end function profil
  end interface
  type(observation), intent(in) :: obs  !
  integer(kind=4),   intent(in) :: m    ! Function number (0 = all functions)
  ! Local
  real(kind=4) :: dvit,vit,vma,zzz,zz0, ux1, ux2, ux
  logical :: pen_up,dobase
  ! 'sampling' is the number of points to be drawn in the X plot range (you
  ! do not want to use 1 point per channel because if you display only a few
  ! channels you will draw an ugly profile). You want enough points to avoid
  ! missing the critical part of the profile (e.g. the peak), especially when
  ! displaying a thin line w/t the X range. You do not want too much points
  ! to avoid an heavy plot (note however that the code below is optimized to
  ! remove useless points far from the central part).
  integer(kind=4), parameter :: sampling=2048
  !
  call gulimx(ux1,ux2,ux)
  dvit = (ux2-ux1)/sampling
  if (dvit.lt.0) then
     dvit = -dvit
     vit = ux2
     vma = ux1
  else
     vit = ux1
     vma = ux2
  endif
  zzz = profil(obs,vit,m,dobase)
  call uplot(vit,zzz,3)
  zz0 = zzz
  pen_up = .true.
  !
  do while (vit.le.vma)
     vit = vit+dvit
     zzz = profil(obs,vit,m,dobase)
     if (zzz.eq.zz0 .and. vit.lt.vma) then
        pen_up = .true.
     else
        !
        ! Something changed till last value, Plot Last Value if not done
        if (pen_up) call uplot(vit-dvit,zz0,2)
        call uplot(vit,zzz,2)
        zz0 = zzz
        pen_up = .false.
     endif
  enddo
  call uplot(vit,zzz,2)
end subroutine plot_curr
!
subroutine plot_freq(profil,obs,m)
  use classfit_interfaces, except_this=>plot_freq
  use class_parameter
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  ! FIT\ Internal routine for command
  ! VISUALIZE [M]
  ! Plot the sum of all components of a fitted profile (M = 0) or
  ! the Mth component only (M .ne. 0)
  !----------------------------------------------------------------------
  interface
    function profil(obs,a,m,error)
    use class_types
    real(kind=4) :: profil
    type(observation), intent(in)  :: obs
    real(kind=4),      intent(in)  :: a
    integer(kind=4),   intent(in)  :: m
    logical,           intent(out) :: error
    end function profil
  end interface
  type(observation), intent(in) :: obs  !
  integer(kind=4),   intent(in) :: m    ! Function number (0 = all functions)
  ! Local
  real(kind=4) :: dvit,vit,vma,zzz,zz0
  real(kind=8) :: gfxo
  real(kind=plot_length) :: gfx1,gfx2,dx
  logical :: pen_up,dobase
  !
  call gelimx(gfxo,gfx1,gfx2,dx,'F')
  dvit = (gfx2-gfx1)/512.
  if (dvit.lt.0) then
     dvit = -dvit
     vit = gfx2
     vma = gfx1
  else
     vit = gfx1
     vma = gfx2
  endif
  zzz = profil(obs,vit,m,dobase)
  call fplot(vit,zzz,3)
  zz0 = zzz
  pen_up = .true.
  !
  do while (vit.le.vma)
     vit = vit+dvit
     zzz = profil(obs,vit,m,dobase)
     if (zzz.eq.zz0 .and. vit.lt.vma) then
        pen_up = .true.
     else
        !
        ! Something changed till last value, Plot Last Value if not done
        if (pen_up) call fplot(vit-dvit,zz0,2)
        call fplot(vit,zzz,2)
        zz0 = zzz
        pen_up = .false.
     endif
  enddo
  call fplot(vit,zzz,2)
end subroutine plot_freq
