subroutine display(set,line,obs,error)
  use gbl_message
  use classfit_dependencies_interfaces
  use classfit_interfaces, except_this=>display
  use class_setup_new
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! FIT support routine for command DISPLAY for spectroscopic data
  ! DISPLAY [YOFF]
  ! 1       [/NOPLOT]
  ! 2       [/METHOD]
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  character(len=*),    intent(in)    :: line   ! Command line
  type(observation),   intent(in)    :: obs    !
  logical,             intent(inout) :: error  ! Flag
  ! Local
  character(len=*), parameter :: rname='DISPLAY'
  integer(kind=4) :: nkey,nc
  real(kind=4) :: yoff
  character(len=80) :: chain
  logical :: doplot,domethod
  integer(kind=4), parameter :: nvocab=6
  character(len=10) :: argum,keyword
  character(len=10) :: vocab(nvocab)
  data vocab /'GAUSS','NH3','HFS','ABSORPTION','SHELL','CONTINUUM'/
  !
  yoff = 0.
  if (sic_present(0,1)) then
    call sic_r4(line,0,1,yoff,.false.,error)
    if (error)  return
  endif
  doplot = .not.sic_present(1,0)
  domethod = sic_present(2,0)
  if (domethod) then
     call sic_ke(line,2,1,argum,nc,.true.,error)
     call sic_ambigs(rname,argum,keyword,nkey,vocab,nvocab,error)
     if (error) return
  else
     keyword = set%method
  endif
  select case (keyword)
  case ('GAUSS')
     call disgau(set,obs%head,class_setup_get_fangle(),yoff,doplot,error)
  case ('NH3')
     call disnh3(set,obs%head,yoff,doplot,error)
  case ('HFS')
     call disnh3(set,obs%head,yoff,doplot,error)
  case ('ABSORPTION')
     call disabs(set,obs%head,yoff,doplot,error)
  case ('SHELL')
     call dishel(set,obs%head,yoff,doplot,error)
  case ('CONTINUUM')
     call dispoi(set,obs%head,class_setup_get_fangle(),yoff,doplot,error)
  end select
  !
  if (error) then
     write(chain,*) "No ",keyword," fit for observation ",obs%head%gen%num
     call class_message(seve%w,rname,chain)
  endif
  !
end subroutine display
!
subroutine disgau(set,obs,fangle,yoff,doplot,error)
  use gbl_constant
  use gbl_message
  use classfit_interfaces, except_this=>disgau
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !   DISPLAY for GAUSS
  ! Retrieves and writes gauss fit results for current observation
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set     !
  type(header),        intent(in)    :: obs     ! Observation header
  real(kind=8),        intent(in)    :: fangle  !
  real(kind=4),        intent(in)    :: yoff    ! Optionnal Y offset
  logical,             intent(in)    :: doplot  ! on the graphic device
  logical,             intent(inout) :: error   ! Flag
  ! Local
  character(len=*), parameter :: rname='DISPLAY'
  character(len=message_length) :: chain
  integer(kind=4) :: k,k1,k2,i,lc
  real(kind=8) :: ang
  !
  if (.not.obs%presec(class_sec_gau_id)) then
     error = .true.
     return
  endif
  !
  ! Print the results
  call class_message(seve%r,rname,'')
  write(chain,1001)  obs%gen%num,obs%gau%sigba,obs%gau%sigra
  call class_message(seve%r,rname,chain)
  call class_message(seve%r,rname,'')
  if (obs%gau%sigba.gt.1.5*obs%gau%sigra) then
     write(chain,'(10X,A)')  'Optimistic fit'
  else if (obs%gau%sigba.lt.obs%gau%sigra/1.5) then
     write(chain,'(10X,A)')  'Bad fit'
  else
     write(chain,'(10X,A)')  'Fit results'
  endif
  call class_message(seve%r,rname,chain)
  call class_message(seve%r,rname,'')
  !
  k=1
  if (obs%gen%kind.eq.kind_spec) then ! Spectroscopic type
     ang = 1.d0
  else                                ! Continuum drift
     ang = fangle
  endif
  !
  write(chain,1000)
  call class_message(seve%r,rname,chain)
  do i=1,max(obs%gau%nline,1)
     k1=k+1
     k2=k+2
     if (obs%gau%nfit(k2).ne.0.0) then
        ! Width.ne.0 : compute peak temperature (4th param.)
        write (chain,1004) i,&
             obs%gau%nfit(k) *ang,obs%gau%nerr(k) *ang,   &
             obs%gau%nfit(k1)*ang,obs%gau%nerr(k1)*ang,   &
             obs%gau%nfit(k2)*ang,obs%gau%nerr(k2)*ang,   &
             obs%gau%nfit(k)/obs%gau%nfit(k2)/1.064467
     else
        ! Width.eq.0 : peak temperature set to '****'
        write (chain,1005) i,&
             obs%gau%nfit(k) *ang,obs%gau%nerr(k) *ang,   &
             obs%gau%nfit(k1)*ang,obs%gau%nerr(k1)*ang,   &
             obs%gau%nfit(k2)*ang,obs%gau%nerr(k2)*ang
     endif
     lc = len_trim(chain)
     call class_message(seve%r,rname,chain)
     if (doplot) call displo(set,obs,chain,lc,i,yoff)
     k=k+3
  enddo
  !
1001 format (1x,'Observation ',i0,' RMS of Residuals :  Base = ',1pg9.2,'  Line = ',1pg9.2)
1000 format (' Line',8x,'Area',19x,'Position',12x,'Width',13x,'Tpeak')
1004 format (i2,3x,1pg12.5,' (',1pg9.3,')',2(1x,0pf9.3,' (',0pf7.3,')'),1x,1pg12.5)
1005 format (i5,3x,1pg12.5,' (',1pg9.3,')',2(1x,0pf9.3,' (',0pf7.3,')'),1x,'  ****')
end subroutine disgau
!
subroutine disnh3(set,obs,yoff,doplot,error)
  use gbl_message
  use classfit_interfaces, except_this=>disnh3
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !   DISPLAY for NH3
  ! Retrieves and writes line fit results for NH3
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set     !
  type(header),        intent(in)    :: obs     ! Observation header
  real(kind=4),        intent(in)    :: yoff    ! Optionnal Y offset
  logical,             intent(in)    :: doplot  ! on the graphic device
  logical,             intent(inout) :: error   ! Flag
  ! Local
  character(len=*), parameter :: rname='DISPLAY'
  character(len=message_length) :: chain
  integer(kind=4) :: k,i,lc
  !
  if (.not.obs%presec(class_sec_hfs_id)) then
     error = .true.
     return
  endif
  !
  ! Print the results
  call class_message(seve%r,rname,'')
  write(chain,1001)  obs%gen%num,obs%hfs%sigba,obs%hfs%sigra
  call class_message(seve%r,rname,chain)
  call class_message(seve%r,rname,'')
  if (obs%hfs%sigba.gt.1.5*obs%hfs%sigra) then
     write(chain,'(10X,A)')  'Optimistic fit'
  else if (obs%hfs%sigba.lt.obs%hfs%sigra/1.5) then
     write(chain,'(10X,A)')  'Bad fit'
  else
     write(chain,'(10X,A)')  'Fit results'
  endif
  call class_message(seve%r,rname,chain)
  call class_message(seve%r,rname,'')
  !
  write(chain,1000)
  call class_message(seve%r,rname,chain)
  k=1
  do i=1,max(obs%hfs%nline,1)
     write (chain,1004) i, &
          obs%hfs%nfit(k),  obs%hfs%nerr(k),   &
          obs%hfs%nfit(k+1),obs%hfs%nerr(k+1), &
          obs%hfs%nfit(k+2),obs%hfs%nerr(k+2), &
          obs%hfs%nfit(k+3),obs%hfs%nerr(k+3)
     k=k+4
     lc = len_trim(chain)
     call class_message(seve%r,rname,chain)
     if (doplot)  call displo(set,obs,chain,lc,i,yoff)
  enddo
  !
1001 format (1x,'Observation ',i0,' RMS of Residuals :  Base = ',1pg9.2,'  Line = ',1pg9.2)
1000 format (' Line     T ant * Tau           V lsr          delta V            Tau main')
1004 format (i3,4(2x,1pg8.3e1,'(',g8.3e1,')'))
! 1004 format (i3,4(2x,f8.3,' (',f6.3,')'))
end subroutine disnh3
!
subroutine disabs(set,obs,yoff,doplot,error)
  use gbl_message
  use classfit_interfaces, except_this=>disabs
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !   DISPLAY for ABS
  ! Retrieves and writes line fit results for ABS
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set     !
  type(header),        intent(in)    :: obs     ! Observation header
  real(kind=4),        intent(in)    :: yoff    ! Optionnal Y offset
  logical,             intent(in)    :: doplot  ! on the graphic device
  logical,             intent(inout) :: error   ! Flag
  ! Local
  character(len=*), parameter :: rname='DISPLAY'
  character(len=message_length) :: chain
  integer(kind=4) :: k,i,lc
  !
  if (.not.obs%presec(class_sec_abs_id)) then
     error = .true.
     return
  endif
  !
  ! Print the results
  call class_message(seve%r,rname,'')
  write(chain,1001)  obs%gen%num,obs%abs%sigba,obs%abs%sigra
  call class_message(seve%r,rname,chain)
  call class_message(seve%r,rname,'')
  if (obs%abs%sigba.gt.1.5*obs%abs%sigra) then
     write(chain,'(10X,A)')  'Optimistic fit'
  else if (obs%abs%sigba.lt.obs%abs%sigra/1.5) then
     write(chain,'(10X,A)')  'Bad fit'
  else
     write(chain,'(10X,A)')  'Fit results'
  endif
  call class_message(seve%r,rname,chain)
  write(chain,1010)  obs%abs%nfit(1),obs%abs%nerr(1)
  call class_message(seve%r,rname,chain)
  call class_message(seve%r,rname,'')
  !
  write(chain,1000)
  call class_message(seve%r,rname,chain)
  k=2
  do i=1,max(obs%abs%nline,1)
     write (chain,1004) i,&
          obs%abs%nfit(k),  obs%abs%nerr(k),     &
          obs%abs%nfit(k+1),obs%abs%nerr(k+1),   &
          obs%abs%nfit(k+2),obs%abs%nerr(k+2)
     k=k+3
     lc = len_trim(chain)
     call class_message(seve%r,rname,chain)
     if (doplot) call displo(set,obs,chain,lc,i,yoff)
  enddo
  !
1001 format (1x,'Observation ',i0,' RMS of Residuals :  Base = ',1pg9.2,'  Line = ',1pg9.2)
1000 format (' Line',t15,'Tau',t34,'V lsr',t53,'Delta V ')
1004 format ('   ',i1,3(2x,f7.3,' (',f6.3,')'))
1010 format (' Continuum ',f7.3,' (',f6.3,')')
end subroutine disabs
!
subroutine dishel(set,obs,yoff,doplot,error)
  use phys_const
  use gbl_message
  use classfit_interfaces, except_this=>dishel
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !   DISPLAY for SHELL
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set     !
  type(header),        intent(in)    :: obs     ! Observation header
  real(kind=4),        intent(in)    :: yoff    ! Optionnal Y offset
  logical,             intent(in)    :: doplot  ! on the graphic device
  logical,             intent(inout) :: error   ! Flag
  ! Local
  character(len=*), parameter :: rname='DISPLAY'
  character(len=message_length) :: chain
  integer(kind=4) :: k,i,lc
  real(kind=8) :: f,fi
  real(kind=4) :: d,ed,x
  !
  if (.not.obs%presec(class_sec_she_id)) then
     error = .true.
     return
  endif
  !
  ! Print the results
  call class_message(seve%r,rname,'')
  write(chain,1001)  obs%gen%num,obs%she%sigba,obs%she%sigra
  call class_message(seve%r,rname,chain)
  call class_message(seve%r,rname,'')
  if (obs%she%sigba.gt.1.5*obs%she%sigra) then
     write(chain,'(10X,A)')  'Optimistic fit'
  elseif (obs%she%sigba.lt.obs%she%sigra/1.5) then
     write(chain,'(10X,A)')  'Bad fit'
  else
     write(chain,'(10X,A)')  'Fit results'
  endif
  call class_message(seve%r,rname,chain)
  call class_message(seve%r,rname,'')
  !
  write(chain,1000)
  call class_message(seve%r,rname,chain)
  k = 1
  do i=1,max(obs%she%nline,1)
     f = obs%she%nfit(k+1)+obs%spe%restf
     fi=-obs%she%nfit(k+1)+obs%spe%image
     d = obs%she%nfit(k+2)*clight_kms/obs%spe%restf
     ed= obs%she%nerr(k+2)*clight_kms/obs%spe%restf
     x = 1.5*obs%she%nfit(k)/obs%she%nfit(k+2)/(3.0+obs%she%nfit(k+3))
     call class_message(seve%r,rname,'')
     write (chain,1003)  i,obs%she%nfit(k),f,fi,d,obs%she%nfit(k+3),x
     call class_message(seve%r,rname,chain)
     write(chain,1004)  obs%she%nerr(k),obs%she%nerr(k+1),obs%she%nerr(k+1),ed,obs%she%nerr(k+3)
     call class_message(seve%r,rname,chain)
     if (doplot) then
        write (chain,1005) i                  &
             ,obs%she%nfit(k),obs%she%nerr(k),f,&
             obs%she%nerr(k+1),fi,obs%she%nerr(k+1),d,ed,obs%she%nfit(k+3),obs%she%nerr(k+3),x
        lc = len_trim(chain)
        call displo(set,obs,chain(1:lc),lc,i,yoff)
     endif
     !
     k=k+4
  enddo
  !
1001 format (1x,'Observation ',i0,' RMS of Residuals :  Base = ',1pg9.2,'  Line = ',1pg9.2)
1000 format (' Line',3x,'Area K.MHz',4x,'Sig Freq',5x,'Im Freq',6x,'Vexp',1x,'Horn/Center',6x,'Temp')
1003 format (' Line',i2,f11.3,2f12.3,f10.3,f12.3,f10.3)
1004 format (' Errors',1pg11.3,0p,2f12.3,f10.3,f12.3)
! 1000 format (1x,'Line',5x,'Area K.MHz',5x,'Sig Freq',12x,'Im Freq',14x,'Vexp',11x,'Horn/Center',4x,'Temp')
1005 format (i1,1pg8.3e1,'(',1pg7.2e1,0p,')',f12.3,'(',1pg7.2e1,0p,')', &
             f12.3,'(',1pg7.2e1,0p,')',f8.2,'(',f5.2,')',f7.2,'(',f4.2,')',f8.1)
end subroutine dishel
!
subroutine dispoi(set,obs,fangle,yoff,doplot,error)
  use gbl_constant
  use gbl_message
  use classfit_interfaces, except_this=>dispoi
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Retrieves and writes gauss fit results for current observation
  ! Continuum method
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set     !
  type(header),        intent(in)    :: obs     ! Observation header
  real(kind=8),        intent(in)    :: fangle  !
  real(kind=4),        intent(in)    :: yoff    ! Optionnal Y offset
  logical,             intent(in)    :: doplot  ! also on X device
  logical,             intent(inout) :: error   ! Flag
  ! Local
  character(len=*), parameter :: rname='DISPLAY'
  character(len=message_length) :: chain
  integer(kind=4) :: k,i,lc
  real(kind=4) :: ang
  !
  if (.not.obs%presec(class_sec_poi_id)) then
     error = .true.
     return
  endif
  !
  ! Print the results
  call class_message(seve%r,rname,'')
  write(chain,1001)  obs%gen%num,obs%poi%sigba,obs%poi%sigra
  call class_message(seve%r,rname,chain)
  call class_message(seve%r,rname,'')
  if (obs%poi%sigba.gt.1.5*obs%poi%sigra) then
     write(chain,'(10X,A)')  'Optimistic fit'
  else if (obs%poi%sigba.lt.obs%poi%sigra/1.5) then
     write(chain,'(10X,A)')  'Bad fit'
  else
     write(chain,'(10X,A)')  'Fit results'
  endif
  call class_message(seve%r,rname,chain)
  call class_message(seve%r,rname,'')
  !
  ! Print only Gaussian parameters
  ! Skip baseline slope
  if (obs%gen%kind.eq.kind_spec) then ! Spectroscopic type
     ang = 1.d0
  else                                ! Continuum drift
     ang = fangle
  endif
  !
  k=3
  write(chain,1000)
  call class_message(seve%r,rname,chain)
  do i=1,max(obs%poi%nline,1)
     if (obs%poi%nfit(k+2).ne.0.0) then
        write (chain,910) i,  &
             obs%poi%nfit(k)  *ang,obs%poi%nerr(k)  *ang,&
             obs%poi%nfit(k+1)*ang,obs%poi%nerr(k+1)*ang,&
             obs%poi%nfit(k+2)*ang,obs%poi%nerr(k+2)*ang,&
             obs%poi%nfit(k)/obs%poi%nfit(k+2)/1.064467
        call class_message(seve%r,rname,chain)
        lc = len_trim(chain)
        if (doplot) call displo(set,obs,chain,lc,i,yoff)
     else
        write (chain,920) i,&
             obs%poi%nfit(k)  *ang,obs%poi%nerr(k)  *ang,&
             obs%poi%nfit(k+1)*ang,obs%poi%nerr(k+1)*ang,&
             obs%poi%nfit(k+2)*ang,obs%poi%nerr(k+2)*ang
        call class_message(seve%r,rname,chain)
     endif
     k=k+3
  enddo
  !
910 format (i5,3x,1pg12.5,' (',0pg8.3,')',2(1x,0pf8.3,' (',0pf7.3,')'),1x,1pg12.5)
920 format (i5,3x,1pg12.5,' (',0pg8.3,')',2(1x,0pf8.3,' (',0pf7.3,')'),1x,'  ****')
1000 format (11x,' Line',6x,'Area',19x,'Position',12x,'Width'13x,'Tpeak')
1001 format (1x,'Observation ',i0,' RMS of Residuals :  Base = ',1pg10.3,'  Source = ',1pg10.3)
end subroutine dispoi
!
subroutine displo(set,head,text,ltext,ifit,yoff)
  use gbl_message
  use classfit_dependencies_interfaces
  use classfit_interfaces, except_this=>displo
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !    DISPLAY
  ! Writes the output fit results in the PLOT window, for ONE line
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in) :: set    !
  type(header),        intent(in) :: head   !
  character(len=*),    intent(in) :: text   ! Text to draw
  integer(kind=4),     intent(in) :: ltext  ! Length of text
  integer(kind=4),     intent(in) :: ifit   ! Line number
  real(kind=4),        intent(in) :: yoff   ! Optionnal Y offset
  ! Local
  real(kind=4) :: cdef,expa,yoff_loc
  character(len=135) :: ch
  character(len=1) :: unit_up,unit_low
  logical :: error
  !
  if (ltext+35.gt.135) then
     call class_message(seve%e,'DISPLAY','String too long')
     return
  endif
  !
  call sic_get_real('CHARACTER_SIZE',cdef,error)
  expa = 1.0
  !
  !  Adapt the absolute positioning depending if the upper axis is
  ! labeled or not:
  call geunit(set,head,unit_low,unit_up)
  if (unit_low.eq.unit_up) then
    yoff_loc = -cdef*expa/2.
  else
    yoff_loc =  cdef*expa/2.
  endif
  !
  write(ch,'(A,F4.1,A2,A,A)')  &
    'DRAW TEXT 0 ',yoff_loc+yoff+ifit*cdef*expa,' "',text(1:ltext),'" 8 0 /BOX 8'
  call gr_exec(ch)
  error=gr_error()
  !
end subroutine displo
