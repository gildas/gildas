subroutine minnh3(npar,g,chi2,x,iflag,obs)
  use classfit_interfaces, except_this=>minnh3
  use class_types
  use gauss_parameter
  use hyperfine_structure
  !----------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  ! Function to be minimised for NH3 profile fitting
  ! Caution : R*8 and R*4 values are carefully mixed for speed
  !----------------------------------------------------------------------
  integer(kind=4),   intent(in)  :: npar     ! Number of parameters
  real(kind=8),      intent(out) :: g(npar)  ! Array of derivatives
  real(kind=8),      intent(out) :: chi2     ! Chi^2
  real(kind=8),      intent(in)  :: x(npar)  ! Input parameter values
  integer(kind=4),   intent(in)  :: iflag    ! Code operation
  type(observation), intent(in)  :: obs      !
  ! Local
  logical :: dobase
  real(kind=4) :: seuil,ybas,yrai,ta
  real(kind=4) :: fi,gg1,gg2,gg3,gg4,aux,arg,vi4
  real(kind=4), dimension(mhfslin*nhfspar) :: xg
  real(kind=4), dimension(mhfslin) :: xt,xv,xd,xo,xtoi,xetoi
  integer(kind=4) :: kline,i,j,k,kbas,krai
  !
  kline = max(nline,1)
  !
  ! Final computations
  if (iflag.eq.3) goto 20
  xg = 0.
  do i=1,kline
    xt(i) = x(1+4*(i-1)) ! Tant * Tau [K]
    xv(i) = x(2+4*(i-1)) ! Position   [km/s]
    xd(i) = x(3+4*(i-1)) ! FWHM       [km/s]
    xo(i) = x(4+4*(i-1)) ! tau_main   []
  enddo
  chi2 = 0.
  !
  ! Fit of line NH3(1,1)
  do i=obs%cimin,obs%cimax
    !
    ! Skip over masked area or bad channels
    if (wfit(i).ne.0) then
      vi4 = real(obs%datax(i),4)
      !
      ! Chi-2
      xtoi = 0.
      do k=1,kline
        do j=1,nhyp
          arg = (vi4-vhfs(j)-xv(k))/xd(k)
          if (abs(arg).lt.4.) then
            xtoi(k) = xtoi(k) + xo(k)*rhfs(j)*exp(-arg**2)
          endif
        enddo
      enddo
      xetoi(1:kline) = exp(-xtoi(1:kline))
      fi             = sum( xt(1:kline)*(1-xetoi(1:kline))/xo(1:kline) ) ! p1/p4*(1-exp(-tau))
      fi = fi - obs%spectre(i)
      chi2 = chi2 + fi**2
      if (iflag.ne.2) cycle
      !
      ! Gradients only when needed
      fi = 2.*fi
      do k=1,kline ! Loop on lines
        gg1 = (1. - xetoi(k)) / xo(k)  
        gg2 = 0.
        gg3 = 0.
        gg4 = 0.
        do j=1,nhyp
          arg = (vi4-vhfs(j)-xv(k))/xd(k)
          if (abs(arg).lt.4.) then
            aux = rhfs(j)*exp(-arg**2)
            gg4 = gg4 + aux        ! / Tau
            aux = 2.*xo(k)*arg/xd(k)*aux
            gg2 = gg2 + aux        ! / V lsr
            gg3 = gg3 + aux*arg    ! / Delta V
          endif
        enddo
        aux = xt(k)*xetoi(k)/xo(k)
        xg(1+4*(k-1)) = xg(1+4*(k-1)) + fi*gg1      ! derivative wrt p1
        xg(2+4*(k-1)) = xg(2+4*(k-1)) + fi*gg2*aux  ! derivative wrt p2
        xg(3+4*(k-1)) = xg(3+4*(k-1)) + fi*gg3*aux  ! derivative wrt p3
        xg(4+4*(k-1)) = xg(4+4*(k-1)) + &           ! derivative wrt p4
        fi*( gg4*aux - xt(k)*(1.-xetoi(k))/xo(k)/xo(k) )
      enddo
    endif
  enddo
  !
  ! Normalize
  do k=1,kline
    g(1+4*(k-1)) = xg(1+4*(k-1))
    g(2+4*(k-1)) = xg(2+4*(k-1))
    g(3+4*(k-1)) = xg(3+4*(k-1))
    g(4+4*(k-1)) = xg(4+4*(k-1))
  enddo
  !
  ! Residuals if needed
  !
  if (iflag.ne.1) return
  !
  ! Sigma after Minimisation
20 kbas=0
  ybas=0.
  krai=0
  yrai=0.
  seuil= sigbas/3.
  !
  do i=obs%cimin,obs%cimax
    if (wfit(i).ne.0) then
      vi4 = real(obs%datax(i),4)
      ta = pronh3(obs,vi4,0,dobase)
      if (abs(ta).lt.seuil) then
        kbas=kbas+1
        ybas=ybas+obs%spectre(i)**2
      else
        krai=krai+1
        yrai=yrai+(ta-obs%spectre(i))**2
      endif
    endif
  enddo
  !
  if (krai.ne.0) then
    sigrai=sqrt(yrai/krai)
  else
    sigrai=0.
  endif
  if (kbas.ne.0) then
    sigbas=sqrt(ybas/kbas)
  else
    sigbas=sigrai
  endif
  !
end subroutine minnh3
