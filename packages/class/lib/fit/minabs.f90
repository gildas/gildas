subroutine minabs(npar,g,f,x,iflag,obs)
  use classfit_interfaces, except_this=>minabs
  use class_types
  use gauss_parameter
  use hyperfine_structure
  !----------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  ! Function to be minimised for Absorption profile fitting
  ! Caution: R*8 and R*4 values are carefully mixed for speed
  !----------------------------------------------------------------------
  integer(kind=4),   intent(in)  :: npar
  real(kind=8),      intent(out) :: g(npar)
  real(kind=8),      intent(out) :: f
  real(kind=8),      intent(in)  :: x(npar)
  integer(kind=4),   intent(in)  :: iflag
  type(observation), intent(in)  :: obs
  ! Local
  logical :: dobase
  real(kind=4) :: gg(nabspar*mabslin)
  real(kind=4) :: al,bl,cl,ti,fi,aux,arg,ta,seuil,ybas,yrai,vi,vv,tt,dv
  integer(kind=4) :: i,j,kbas,krai,il,it,iv,id,ip
  !
  ! Final computations
  if (iflag.eq.3) goto 20
  !
  g = 0
  f = 0.
  !
  do i=obs%cimin,obs%cimax
     !
     ! Skip over masked area or bad channels
     if (wfit(i).gt.0) then
        vi = obs%datax(i)
        ti = 0
        do il = 1, nline
           al = 0
           bl = 0
           cl = 0
           it = (il-1)*3+2
           iv = it+1
           id = iv+1
           tt = x(it)
           vv = x(iv)
           dv = x(id)
           do j=1, nhyp
              arg = (vi-vhfs(j)-vv)/dv
              if (abs(arg).lt.4.) then
                 aux = rhfs(j)*exp(-arg**2)
                 al = al + aux
                 aux = aux*2.*arg/dv
                 bl = bl + aux
                 cl = cl + aux*arg
              endif
           enddo
           ti = ti+al*tt
           gg(it) = al
           gg(iv) = bl*tt
           gg(id) = cl*tt
        enddo
        ti = exp(-ti)
        fi = obs%spectre(i) - ti*x(1)
        f = f + fi**2
        ti = 2*ti*fi
        g(1) = g(1) - ti
        do ip =2, 3*nline+1
           g(ip) = g(ip)+ti*gg(ip)
        enddo
     endif
  enddo
  !
  ! Residuals if needed
  !
  if (iflag.ne.1) return
  !
20 continue
  ! Sigma after Minimisation
  kbas=0
  ybas=0.
  krai=0
  yrai=0.
  seuil= sigbas/3.
  !
  do i=obs%cimin,obs%cimax
     if (wfit(i).gt.0) then
        vi = real(obs%datax(i),4)
        ta = proabs(obs,vi,0,dobase)
        if (abs(ta).lt.seuil) then
           kbas=kbas+1
           ybas=ybas+obs%spectre(i)**2
        else
           krai=krai+1
           yrai=yrai+(ta-obs%spectre(i))**2
        endif
     endif
  enddo
  !
  if (krai.ne.0) then
     sigrai=sqrt(yrai/krai)
  else
     sigrai=0.
  endif
  if (kbas.ne.0) then
     sigbas=sqrt(ybas/kbas)
  else
     sigbas=sigrai
  endif
  !
end subroutine minabs
