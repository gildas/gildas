subroutine result_comm(set,line,obs,user_function,error)
  use gbl_message
  use classfit_dependencies_interfaces
  use classfit_interfaces, except_this=>result_comm
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! Support routine for command FIT\RESULT [N]
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  character(len=*),    intent(in)    :: line           ! Input command line
  type(observation),   intent(inout) :: obs            !
  logical,             external      :: user_function  !
  logical,             intent(inout) :: error          ! Error flag
  ! Local
  character(len=*), parameter :: rname='RESULT'
  !
  call fit_res_comm(set,line,rname,obs,.true.,user_function,error)
  if (error)  return
  !
end subroutine result_comm
!
subroutine residu_comm(set,line,obs,user_function,error)
  use gbl_message
  use classfit_dependencies_interfaces
  use classfit_interfaces, except_this=>residu_comm
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! Support routine for command FIT\RESIDUALS [N]
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  character(len=*),    intent(in)    :: line           ! Input command line
  type(observation),   intent(inout) :: obs            !
  logical,             external      :: user_function  !
  logical,             intent(inout) :: error          ! Error flag
  ! Local
  character(len=*), parameter :: rname='RESIDUAL'
  !
  call fit_res_comm(set,line,rname,obs,.false.,user_function,error)
  if (error)  return
  !
end subroutine residu_comm
!
subroutine fit_res_comm(set,line,rname,obs,doresu,user_function,error)
  use gbl_message
  use classfit_dependencies_interfaces
  use classfit_interfaces, except_this=>fit_res_comm
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for commands:
  !   FIT\RESULT [N]
  !   FIT\RESIDUAL [N]
  ! Redirection relying on SET METHOD
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  character(len=*),    intent(in)    :: line           ! Input command line
  character(len=*),    intent(in)    :: rname          ! Calling routine name
  type(observation),   intent(inout) :: obs            !
  logical,             intent(in)    :: doresu         ! result or residual?
  logical,             external      :: user_function  !
  logical,             intent(inout) :: error          ! Error flag
  ! Local
  integer(kind=4) :: m
  !
  m = 0
  call sic_i4 (line,0,1,m,.false.,error)
  if (error) return
  m = max(m,0)
  !
  select case (set%method)
  case ('CONTINUUM')
    call fit_res(set,obs,doresu,propoi,m,user_function,error)
  case ('GAUSS')
    call fit_res(set,obs,doresu,progauss,m,user_function,error)
  case ('NH3','HFS')
    call fit_res(set,obs,doresu,pronh3,m,user_function,error)
  case ('ABSORPTION')
    call fit_res(set,obs,doresu,proabs,m,user_function,error)
  case ('SHELL')
    call fit_res(set,obs,doresu,proshell,m,user_function,error)
  case default
    call class_message(seve%e,rname,'Not implemented for method '//set%method)
    error = .true.
    return
  end select
  !
end subroutine fit_res_comm
!
subroutine fit_res(set,obs,doresu,fnc,m,user_function,error)
  use gildas_def
  use classfit_interfaces, except_this=>fit_res
  use class_types
  !--------------------------------------------------------------------
  ! @ private
  ! Support routine for commands
  !  FIT\RESULT [N]
  !  FIT\RESIDUAL [N]
  !
  ! Compute the residuals from last fit
  !  M indicates which line must be subtracted. If M = 0 (default), all
  ! lines must be subtracted.
  !  Copy R into T
  !  Compute R = T - Fit(N)
  !--------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  type(observation),   intent(inout) :: obs            !
  logical,             intent(in)    :: doresu         ! result or residual?
  integer(kind=4),     intent(in)    :: m              ! Line number
  logical,             external      :: user_function  !
  real(kind=4),        external      :: fnc            ! Function used for the fit
  logical,             intent(inout) :: error          ! Error flag
  ! Local
  real(kind=4) :: a,b,resul
  integer(kind=4) :: i
  logical :: dobase
  !
  if (set%method.eq.'SHELL') then
    b = - obs%head%spe%rchan * obs%head%spe%fres
    a = obs%head%spe%fres
    do i=1,obs%cnchan
      if (obs%spectre(i).ne.obs%head%spe%bad) then
        resul = fnc(obs,a*float(i)+b,m,dobase)
        if (doresu) then
          obs%spectre(i) = resul
        else
          obs%spectre(i) = obs%spectre(i) - resul
        endif
      endif
    enddo
    !
  else
    do i=1,obs%cnchan
      if (obs%spectre(i).ne.obs%head%spe%bad) then
        a = real(obs%datax(i),kind=4)  ! data type conversion
        resul = fnc(obs,a,m,dobase)
        if (doresu) then
          obs%spectre(i) = resul
        else
          obs%spectre(i) = obs%spectre(i)-resul
        endif
      endif
    enddo
  endif
  !
  call newlimy(set,obs,error)
  !
end subroutine fit_res
