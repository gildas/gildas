module gauss_parameter
  integer(kind=4), parameter :: mxpar=4  ! 3 for Gauss, 4 for HFS/NH3
  integer(kind=4), parameter :: mxline=10
  !
  ! Initial values and Dependency codes
  real(kind=4)    :: spar(mxpar*mxline)
  real(kind=4)    :: deltav
  integer(kind=4) :: kt0,kt(mxline)
  integer(kind=4) :: kv0,kv(mxline)
  integer(kind=4) :: kd0,kd(mxline)
  integer(kind=4) :: ko0,ko(mxline)
  integer(kind=4) :: kh0,kh(mxline)
  integer(kind=4) :: nline
  !
  ! Weights for minimization
  integer(kind=4) :: mxcan
  integer(kind=4), allocatable :: wfit(:)
  !
  ! Common containing results and errors
  integer(kind=4) :: ngline               ! Number of lines
  real(kind=4)    :: par(4*mxline)   ! Parameters
  real(kind=4)    :: err(4*mxline)   ! Errors
  real(kind=4)    :: sigbas          ! rms on baseline
  real(kind=4)    :: sigrai          ! rms on line
end module gauss_parameter
!
module hyperfine_structure
  ! HFS and NH3 line fitting
  integer(kind=4), parameter    :: mhyp=40
  integer(kind=4)               :: nhyp
  real(kind=4), dimension(mhyp) :: rhfs
  real(kind=4), dimension(mhyp) :: vhfs
  real(kind=4), save            :: hfs_tau_min=0.1
  real(kind=4), save            :: hfs_tau_max=100.0
end module hyperfine_structure
!
module pointing_fit
  real(kind=4) :: width
  real(kind=4) :: aratio
  real(kind=4) :: wratio
end module pointing_fit
