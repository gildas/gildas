subroutine midnh3(obs,fit,ifatal,liter)
  use gildas_def
  use gbl_message
  use fit_minuit
  use classfit_dependencies_interfaces
  use classfit_interfaces, except_this=>midnh3
  use class_types
  use gauss_parameter
  use hyperfine_structure
  !----------------------------------------------------------------------
  ! @ private
  ! Set up the starting parameters for NH3 fit.
  !----------------------------------------------------------------------
  type(observation),  intent(in)    :: obs    !
  type(fit_minuit_t), intent(inout) :: fit     ! Fitting variables
  integer(kind=4),    intent(out)   :: ifatal  ! Nb of errors
  logical,            intent(in)    :: liter   ! Iteration ?
  ! Local
  character(len=*), parameter :: rname='MIDNH3'
  integer(kind=4) :: i,ninte,k
  real(kind=4) :: ymax,ymin,aire,vsup,vinf,yy
  real(kind=8) :: sav,sav2,vplu,vminu
  character(len=100) :: mess
  !
  fit%isw(1:7) = 0
  fit%sigma = 0.d0
  fit%npfix = 0
  ninte = 0
  fit%nu = 0
  fit%npar = 0
  ifatal = 0
  fit%u(1:fit%maxext)      = 0.0d0
  fit%lcode(1:fit%maxext)  = 0
  fit%lcorsp(1:fit%maxext) = 0
  fit%isw(5) = 1
  fit%data = locwrd(obs)
  !
  deltav = abs(obs%datax(obs%cimin+1)-obs%datax(obs%cimin))
  !
  ! Initial guesses
  !
  if (nline.eq.0) then
     ymax=0.
     ymin=0.
     aire=0.
     do i=obs%cimin+1,obs%cimax-1
        if (wfit(i).ne.0) then
           yy = ( obs%spectre(i) +  &
                wfit(i-1)*obs%spectre(i-1) +  &
                wfit(i+1)*obs%spectre(i+1) )  &
                / (1+wfit(i-1)+wfit(i+1))
           if (yy.ge.ymax) then
              ymax = yy
              vsup = obs%datax(i)
           endif
           if (yy.le.ymin) then
              ymin = yy
              vinf = obs%datax(i)
           endif
           aire=aire+yy*abs((obs%datax(i+1)-obs%datax(i-1)))
        endif
     enddo
     aire = aire*0.5
     if (abs(ymin).lt.abs(ymax)) then
        par(2)=vsup
        par(1)=ymax
     else
        par(2)=vinf
        par(1)=ymin
     endif
     par(3)=abs(aire/par(1)/1.064467)/2.    ! Take care of satellites
     par(4)=1.0                 ! Standard Optical depth
     par(1)=1.5*par(1)*par(4)
     fit%nu=4
     !
     ! Take initial Guesses
  else
     fit%nu=4*nline
     do i=1,fit%nu
        par(i)=spar(i)
     enddo
  endif
  !
  k=1
  write(6,1001)
  do i=1,max(nline,1)
     write (6,1002) par(k),par(k+1),par(k+2),par(k+3)
     k=k+4
  enddo
  !
  ! Set Up Parameters
  k=1
  do i=1,max(nline,1)
     !
     ! Temperature * Optical depth
     fit%u(k)=par(k)
     fit%werr(k)=sigbas
     if ( mod(kt(i),2).ne.0 .and. nline.ne.0) fit%werr(k)=0.
     if (fit%u(k).ne.0.) then
        fit%alim(k)=min(0.d0,8.d0*fit%u(k))
        fit%blim(k)=max(0.d0,8.d0*fit%u(k))
     else
        fit%lcode(k)=1
     endif
     k=k+1
     !
     ! Velocity
     fit%u(k)=par(k)
     fit%werr(k)=deltav
     if ( mod(kv(i),2).ne.0 .and. nline.ne.0) fit%werr(k)=0.
     fit%alim(k)=fit%u(k)-0.15*obs%cnchan*deltav
     fit%blim(k)=fit%u(k)+0.15*obs%cnchan*deltav
     k=k+1
     !
     ! Line Widths
     fit%u(k)=abs(par(k))/1.665109d0
     fit%werr(k)=2*deltav
     if ( mod(kd(i),2).ne.0 .and. nline.ne.0 ) fit%werr(k)=0.
     fit%alim(k)=deltav
     fit%blim(k)=0.5*obs%cnchan*deltav
     k=k+1
     !
     ! Optical depth
     fit%u(k)=par(k)
     fit%werr(k)=0.1
     if ( mod(ko(i),2).ne.0 .and. nline.ne.0 ) fit%werr(k)=0.
     fit%alim(k)=hfs_tau_min
     fit%blim(k)=hfs_tau_max
     k=k+1
  enddo
  !
  ! Various checks
  do k=1,fit%nu
     if (k.gt.fit%maxext) then
        ifatal = ifatal+1
     else if (fit%werr(k).le.0.d0) then
        ! Fixed parameter
        fit%lcode(k) = 0
        write(mess,'(a,i3,a)') "Parameter",k," is fixed"
        call class_message(seve%w,rname,mess)
     else
        ! Variable parameter
        ninte = ninte + 1
        ! Parameter with limits
        if (fit%lcode(k).ne.1) then
           fit%lcode(k) = 4
           sav = (fit%blim(k)-fit%u(k))*(fit%u(k)-fit%alim(k))
           if (sav.lt.0d0) then
              ifatal = ifatal + 1
              write(mess,'(a,i0,a,f0.3,a,f0.3)')  &
                'Parameter ',k,' outside limits ',fit%alim(k),' to ',fit%blim(k)
              call class_message(seve%e,rname,mess)
           else if (sav.eq.0d0) then
              write(mess,'(a,i3,a)') "Parameter",k," is at limit"
              call class_message(seve%w,rname,mess)
           endif
        endif
     endif
  enddo
  !
  ! End parameter cards
  ! Stop if fatal error
  if (ninte .gt. fit%maxint)  then
     write(mess,1008) ninte,fit%maxint
     call class_message(seve%e,rname,mess)
     ifatal = ifatal + 1
  endif
  if (ninte .eq. 0) then
     call class_message(seve%w,rname,'All input parameters are fixed')
     ifatal = ifatal + 1
  endif
  if (ifatal.gt.0)  then
    write(mess,'(I0,A)')  ifatal,' errors on input parameters, abort.'
    call class_message(seve%e,rname,mess)
    return
  endif
  !
  ! Calculate step sizes DIRIN
  fit%npar = 0
  do k= 1, fit%nu
     if (fit%lcode(k) .gt. 0)  then
        fit%npar = fit%npar + 1
        fit%lcorsp(k) = fit%npar
        sav = fit%u(k)
        fit%x(fit%npar) = pintf(fit,sav,k)
        fit%xt(fit%npar) = fit%x(fit%npar)
        sav2 = sav + fit%werr(k)
        vplu = pintf(fit,sav2,k) - fit%x(fit%npar)
        sav2 = sav - fit%werr(k)
        vminu = pintf(fit,sav2,k) - fit%x(fit%npar)
        fit%dirin(fit%npar) = 0.5d0 * (dabs(vplu) +dabs(vminu))
     endif
  enddo
  return
  !
1001 format('Input Parameters',/,   &
          3x,'T_ant * tau',6x,'Position',9x,'Width',4x,'Tau (main)')
1002 format((4(f14.3)))
1008 format (' Too many variable parameters.  You request ',i5/,   &
       &    ' This version of MINUIT is only dimensioned for ',i4)
end subroutine midnh3
