module classfit_interfaces_public
  interface
    subroutine display(set,line,obs,error)
      use gbl_message
      use class_setup_new
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! FIT support routine for command DISPLAY for spectroscopic data
      ! DISPLAY [YOFF]
      ! 1       [/NOPLOT]
      ! 2       [/METHOD]
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      character(len=*),    intent(in)    :: line   ! Command line
      type(observation),   intent(in)    :: obs    !
      logical,             intent(inout) :: error  ! Flag
    end subroutine display
  end interface
  !
  interface
    subroutine iterate_fit(set,line,obs,error)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! Support routine for command
      !   FIT\ITERATE
      ! 1     /NOCHECK BASELINE
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      character(len=*),    intent(in)    :: line   ! Input command line
      type(observation),   intent(inout) :: obs    !
      logical,             intent(inout) :: error  ! Flag
    end subroutine iterate_fit
  end interface
  !
  interface
    subroutine keepfi(set,obs,error)
      use gildas_def
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! CLASS support routine for command KEEP
      ! Save profile fit results in the output file.
      ! (Modifies an observation in R onto the output file)
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      type(observation),   intent(inout) :: obs    !
      logical,             intent(inout) :: error  ! Error flag
    end subroutine keepfi
  end interface
  !
  interface
    subroutine fitlines(set,line,obs,error)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! Support routine for command
      !  FIT\LINES [N] [/NOCURSOR] [/INPUT File_Name] [/SHOW]
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      character(len=*),    intent(in)    :: line   ! Input command line
      type(observation),   intent(in)    :: obs    !
      logical,             intent(inout) :: error  ! Logical error flag
    end subroutine fitlines
  end interface
  !
  interface
    subroutine class_method(set,line,error)
      use gildas_def
      use gbl_message
      use class_types
      use hyperfine_structure
      !----------------------------------------------------------------------
      ! @ public
      ! ANALYSE Support routine for command
      ! METHOD   GAUSS
      !          SHELL
      !          NH3(1,1) or NH3(2,2) or NH3(3,3)
      !          CONTINUUM
      !          HFS Filename
      !          ABSORPTION (undocumented)
      ! Select the method for fitting
      !----------------------------------------------------------------------
      type(class_setup_t), intent(inout) :: set    !
      character(len=*),    intent(in)    :: line   ! Command line
      logical,             intent(inout) :: error  ! Error flag
    end subroutine class_method
  end interface
  !
  interface
    subroutine minimize(set,line,obs,error)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! Support routine for command:
      !    FIT\MINIMIZE
      ! 1     /NOCHECK BASELINE
      ! Redirection relying on SET METHOD
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      character(len=*),    intent(in)    :: line   ! Input command line
      type(observation),   intent(inout) :: obs    !
      logical,             intent(inout) :: error  ! Error flag
    end subroutine minimize
  end interface
  !
  interface
    subroutine plotfit(set,line,obs,error)
      use gbl_message
      use class_types
      !----------------------------------------------------------------------
      ! @ public
      ! FIT\ Support routine for command
      ! VISUALIZE [M]
      ! 1         [/PEN [i]]
      !----------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set
      character(len=*),    intent(in)    :: line
      type(observation),   intent(in)    :: obs
      logical,             intent(inout) :: error
    end subroutine plotfit
  end interface
  !
  interface
    function progauss (obs,x0,m,dobase)
      use class_types
      !----------------------------------------------------------------------
      ! @ public (for libclass only)
      ! FIT\ Internal routine
      ! M.eq.0:  Computes the composite profile from sum of all gaussians.
      ! M.ne.0:  Computes the profile from gaussian number M
      !----------------------------------------------------------------------
      real(kind=4) :: progauss  ! Function value on return
      type(observation), intent(in)  :: obs     ! Input observation
      real(kind=4),      intent(in)  :: x0      ! Abscissa
      integer(kind=4),   intent(in)  :: m       ! Sum of components or component num.
      logical,           intent(out) :: dobase  ! Dummy argument for compatibility with PROPOI
    end function progauss
  end interface
  !
  interface
    function pronh3(obs,vi,m,dobase)
      use class_types
      use hyperfine_structure
      !----------------------------------------------------------------------
      ! @ public (for libclass only)
      ! FIT\ Internal routine
      ! M.eq.0:  Computes the composite profile from sum of all NH3 profiles
      ! M.ne.0:  Computes the profile from gaussian number M
      !----------------------------------------------------------------------
      real(kind=4) :: pronh3  ! Function value on return
      type(observation), intent(in)  :: obs     ! Input observation
      real(kind=4),      intent(in)  :: vi      ! Velocity
      integer(kind=4),   intent(in)  :: m       ! Line number
      logical,           intent(out) :: dobase  ! Dummy argument for compatibility with PROPOI
    end function pronh3
  end interface
  !
  interface
    function proabs(obs,vi,m,dobase)
      use class_types
      !----------------------------------------------------------------------
      ! @ public (for libclass only)
      ! FIT\ Internal routine
      ! M.eq.0:  Computes the composite profile from sum of all ABS profiles
      ! M.ne.0:  Computes the profile from gaussian number M
      !----------------------------------------------------------------------
      real(kind=4) :: proabs  ! Function value on return
      type(observation), intent(in)  :: obs    ! Input observation
      real(kind=4),      intent(in)  :: vi     ! Velocity
      integer,           intent(in)  :: m      ! Line number
      logical,           intent(out) :: dobase ! Dummy argument for compatibility with PROPOI
    end function proabs
  end interface
  !
  interface
    function proshell(obs,a,m,error)
      use class_types
      !----------------------------------------------------------------------
      ! @ public (for libclass only)
      ! FIT\ Internal routine
      ! M.eq.0:  Computes the composite profile from sum of all NH3 profiles
      ! M.ne.0:  Computes the profile from gaussian number M
      !
      ! Note: X unit is FREQUENCY
      !----------------------------------------------------------------------
      real(kind=4) :: proshell  ! Function value on return
      type(observation), intent(in) :: obs      ! Input observation
      real(kind=4),      intent(in)  :: a       ! Velocity
      integer(kind=4),   intent(in)  :: m       ! Line number
      logical,           intent(out) :: error   ! Logical error flag
    end function proshell
  end interface
  !
  interface
    subroutine result_comm(set,line,obs,user_function,error)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! Support routine for command FIT\RESULT [N]
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      character(len=*),    intent(in)    :: line           ! Input command line
      type(observation),   intent(inout) :: obs            !
      logical,             external      :: user_function  !
      logical,             intent(inout) :: error          ! Error flag
    end subroutine result_comm
  end interface
  !
  interface
    subroutine residu_comm(set,line,obs,user_function,error)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! Support routine for command FIT\RESIDUALS [N]
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      character(len=*),    intent(in)    :: line           ! Input command line
      type(observation),   intent(inout) :: obs            !
      logical,             external      :: user_function  !
      logical,             intent(inout) :: error          ! Error flag
    end subroutine residu_comm
  end interface
  !
end module classfit_interfaces_public
