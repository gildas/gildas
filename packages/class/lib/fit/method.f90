subroutine class_method(set,line,error)
  use gildas_def
  use gbl_message
  use classfit_dependencies_interfaces
  use classfit_interfaces, except_this=>class_method
  use class_types
  use hyperfine_structure
  !----------------------------------------------------------------------
  ! @ public
  ! ANALYSE Support routine for command
  ! METHOD   GAUSS
  !          SHELL
  !          NH3(1,1) or NH3(2,2) or NH3(3,3)
  !          CONTINUUM
  !          HFS Filename
  !          ABSORPTION (undocumented)
  ! Select the method for fitting
  !----------------------------------------------------------------------
  type(class_setup_t), intent(inout) :: set    !
  character(len=*),    intent(in)    :: line   ! Command line
  logical,             intent(inout) :: error  ! Error flag
  ! Local
  integer(kind=4), parameter :: nmeth=8
  character(len=10) :: amethod(nmeth),argum
  character(len=message_length) :: mess
  character(len=filename_length) :: name
  integer(kind=4) :: n,nc
  integer(kind=index_length) :: dims(4)
  ! Data
  data amethod /'GAUSS','SHELL','NH3(1,1)','NH3(2,2)','NH3(3,3)','CONTINUUM','HFS','ABSORPTION'/
  !
  call sic_ke (line,0,1,argum,nc,.true.,error)
  if (error) return
  call sic_ambigs('METHOD',argum,set%method,n,amethod,nmeth,error)
  if (error) return
  write(mess,*) set%method,' selected'
  !
  if (set%method(1:3).eq.'NH3') then
     call rainh3 (set%method)
     set%method = set%method(1:3)
  elseif (set%method.eq.'HFS'.or.set%method.eq.'ABSORPTION') then
     if (sic_present(0,2)) then
        call sic_ch (line,0,2,name,nc,.false.,error)
        if (error) return
        write(mess,'(3A)') trim(set%method),' selected: input file ',name(1:nc)
        call raihfs(name,error)
     else
        write(mess,'(2A)') trim(set%method),' selected (no input file, single component assumed)'
        nhyp = 1
        vhfs(1) = 0.0
        rhfs(1) = 1.0
     endif
  elseif (set%method.eq.'CONTINUUM') then
     call raicon(set,line,error)
  endif
  if (error)  return
  !
  call sic_delvariable('HFS',.false.,error)
  if (error)  error = .false.  ! Not an error if HFS% does not exist
  if (nhyp.gt.1) then
    call sic_defstructure('HFS',.true.,error)
    call sic_def_inte('HFS%NHYP',nhyp,0,0,.true.,error)
    dims(1) = nhyp
    call sic_def_real('HFS%VELOCITY',vhfs,1,dims,.true.,error)
    call sic_def_real('HFS%RATIO',rhfs,1,dims,.true.,error)
    call sic_def_real('HFS%TAU_MIN',hfs_tau_min,0,0,.false.,error)
    call sic_def_real('HFS%TAU_MAX',hfs_tau_max,0,0,.false.,error)
  endif
  !
  call class_message(seve%i,'METHOD',mess)
  !
end subroutine class_method
!
subroutine rainh3(craie)
  use hyperfine_structure
  !-------------------------------------------------------------------
  ! @ private
  !-------------------------------------------------------------------
  character(len=*), intent(in) :: craie
  !
  if (craie.eq.'NH3(1,1)') then
     nhyp=18
     vhfs( 1) =     19.851279
     vhfs( 2) =     19.315905
     vhfs( 3) =      7.886691
     vhfs( 4) =      7.469667
     vhfs( 5) =      7.351317
     vhfs( 6) =      0.460409
     vhfs( 7) =      0.322042
     vhfs( 8) =     -0.075168
     vhfs( 9) =     -0.213003
     vhfs(10) =      0.311034
     vhfs(11) =      0.192266
     vhfs(12) =     -0.132382
     vhfs(13) =     -0.250923
     vhfs(14) =     -7.233485
     vhfs(15) =     -7.372800
     vhfs(16) =     -7.815255
     vhfs(17) =    -19.411734
     vhfs(18) =    -19.549987
     rhfs( 1) =      0.074074
     rhfs( 2) =      0.148148
     rhfs( 3) =      0.092593
     rhfs( 4) =      0.166667
     rhfs( 5) =      0.018519
     rhfs( 6) =      0.037037
     rhfs( 7) =      0.018519
     rhfs( 8) =      0.018519
     rhfs( 9) =      0.092593
     rhfs(10) =      0.033333
     rhfs(11) =      0.300000
     rhfs(12) =      0.466667
     rhfs(13) =      0.033333
     rhfs(14) =      0.092593
     rhfs(15) =      0.018519
     rhfs(16) =      0.166667
     rhfs(17) =      0.074074
     rhfs(18) =      0.148148
  elseif (craie.eq.'NH3(2,2)') then
     nhyp=21
     vhfs( 1) =     26.526252
     vhfs( 2) =     26.011126
     vhfs( 3) =     25.950454
     vhfs( 4) =     16.391711
     vhfs( 5) =     16.379289
     vhfs( 6) =     15.864175
     vhfs( 7) =      0.562503
     vhfs( 8) =      0.528408
     vhfs( 9) =      0.523745
     vhfs(10) =      0.013282
     vhfs(11) =     -0.003791
     vhfs(12) =     -0.013282
     vhfs(13) =     -0.501831
     vhfs(14) =     -0.531340
     vhfs(15) =     -0.589080
     vhfs(16) =    -15.854685
     vhfs(17) =    -16.369798
     vhfs(18) =    -16.382221
     vhfs(19) =    -25.950454
     vhfs(20) =    -26.011126
     vhfs(21) =    -26.526252
     rhfs( 1) =      0.004186
     rhfs( 2) =      0.037674
     rhfs( 3) =      0.020930
     rhfs( 4) =      0.037209
     rhfs( 5) =      0.026047
     rhfs( 6) =      0.001860
     rhfs( 7) =      0.020930
     rhfs( 8) =      0.011628
     rhfs( 9) =      0.010631
     rhfs(10) =      0.267442
     rhfs(11) =      0.499668
     rhfs(12) =      0.146512
     rhfs(13) =      0.011628
     rhfs(14) =      0.010631
     rhfs(15) =      0.020930
     rhfs(16) =      0.001860
     rhfs(17) =      0.026047
     rhfs(18) =      0.037209
     rhfs(19) =      0.020930
     rhfs(20) =      0.037674
     rhfs(21) =      0.004186
     !
  elseif (craie.eq.'NH3(3,3)') then
     nhyp=26
     vhfs( 1) =     29.195098
     vhfs( 2) =     29.044147
     vhfs( 3) =     28.941877
     vhfs( 4) =     28.911408
     vhfs( 5) =     21.234827
     vhfs( 6) =     21.214619
     vhfs( 7) =     21.136387
     vhfs( 8) =     21.087456
     vhfs( 9) =      1.005122
     vhfs(10) =      0.806082
     vhfs(11) =      0.778062
     vhfs(12) =      0.628569
     vhfs(13) =      0.016754
     vhfs(14) =     -0.005589
     vhfs(15) =     -0.013401
     vhfs(16) =     -0.639734
     vhfs(17) =     -0.744554
     vhfs(18) =     -1.031924
     vhfs(19) =    -21.125222
     vhfs(20) =    -21.203441
     vhfs(21) =    -21.223649
     vhfs(22) =    -21.076291
     vhfs(23) =    -28.908067
     vhfs(24) =    -28.938523
     vhfs(25) =    -29.040794
     vhfs(26) =    -29.191744
     rhfs( 1) =      0.012263
     rhfs( 2) =      0.008409
     rhfs( 3) =      0.003434
     rhfs( 4) =      0.005494
     rhfs( 5) =      0.006652
     rhfs( 6) =      0.008852
     rhfs( 7) =      0.004967
     rhfs( 8) =      0.011589
     rhfs( 9) =      0.019228
     rhfs(10) =      0.010387
     rhfs(11) =      0.010820
     rhfs(12) =      0.009482
     rhfs(13) =      0.293302
     rhfs(14) =      0.459109
     rhfs(15) =      0.177372
     rhfs(16) =      0.009482
     rhfs(17) =      0.010820
     rhfs(18) =      0.019228
     rhfs(19) =      0.004967
     rhfs(20) =      0.008852
     rhfs(21) =      0.006652
     rhfs(22) =      0.011589
     rhfs(23) =      0.005494
     rhfs(24) =      0.003434
     rhfs(25) =      0.008409
     rhfs(26) =      0.012263
  endif
end subroutine rainh3
!
subroutine raihfs(chain,error)
  use gildas_def
  use gbl_message
  use classfit_dependencies_interfaces
  use classfit_interfaces, except_this=>raihfs
  use hyperfine_structure
  !-------------------------------------------------------------------
  ! @ private
  ! Read parameters of hyperfine multiplet
  !-------------------------------------------------------------------
  character(len=*), intent(in)    :: chain
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: proc='METHOD'
  character(len=filename_length) :: name
  integer(kind=4) :: jtmp,n,ier,npar,lpar,nl
  character(len=128) :: local_line
  character(len=24) :: cpar
  !
  ier = sic_getlun(jtmp)
  if (ier.ne.1) then
     call class_message(seve%e,proc,'Cannot open HFS description file')
     call class_message(seve%e,proc,'No logical unit left')
     error = .true.
     return
  endif
  call sic_parse_file(chain,' ','.hfs',name)
  ier = sic_open(jtmp,name,'OLD',.true.)
  if (ier.ne.0) then
     call class_message(seve%e,proc,'Cannot open HFS description file')
     call class_iostat(seve%e,proc,ier)
     call sic_frelun(jtmp)
     error = .true.
     return
  endif
  !
  ! Read number of components
  read (jtmp,*,err=99,iostat=ier) n
  if (n.gt.mhyp) then
     call class_message(seve%e,proc,'Too many HFS components')
     close (unit=jtmp)
     call sic_frelun(jtmp)
     error = .true.
     return
  endif
  nhyp = n
  do n=1,nhyp
     ! Read in local buffer
     read(jtmp,'(A)',err=99,iostat=ier) local_line
     local_line=trim(adjustl(local_line))
     if (ier.ne.0) goto 99
     nl = len_trim(local_line)
     !
     ! Class documentation promises that a fraction can be used for
     ! ratios. Use the math decoding function for this.
     npar = 1
     ! Get and decode 1st arg
     call sic_next(local_line(npar:nl),cpar,lpar,npar,dotab=.true.)
     call sic_math_real(cpar(1:lpar),lpar,vhfs(n),error)
     if (error) goto 99
     ! Get and decode 2nd arg
     call sic_next(local_line(npar:nl),cpar,lpar,npar,dotab=.true.)
     call sic_math_real(cpar(1:lpar),lpar,rhfs(n),error)
     if (error) goto 99
  enddo
  close(unit=jtmp)
  call sic_frelun(jtmp)
  return
  !
99 close(unit=jtmp)
  call class_message(seve%e,proc,'Error reading HFS description file')
  call class_iostat(seve%e,proc,ier)
  call sic_frelun(jtmp)
  error = .true.
end subroutine raihfs
!
subroutine raicon(set,line,error)
  use classfit_dependencies_interfaces
  use classfit_interfaces, except_this=>raicon
  use class_setup_new
  use class_types
  use pointing_fit
  !----------------------------------------------------------------------
  ! @ private
  ! ANALYSE Support routine for command
  ! METHOD CONTINUUM width area-ratio width-ratio
  !----------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  character(len=*),    intent(in)    :: line   ! Command line
  logical,             intent(inout) :: error  ! Error flag
  ! Local
  character(len=12) :: name
  real(kind=4) :: reel
  integer(kind=4) :: narg,nc
  !
  ! Width
  narg = sic_narg(0)
  if (narg.lt.2) return
  call sic_ke (line,0,2,name,nc,.false.,error)
  if (error) return
  if (name.ne.'*') then
     call sic_r4 (line,0,2,reel,.true.,error)
     if (error) return
     width = reel/class_setup_get_fangle()
  else
     width = 0.0
  endif
  !
  ! area ratio
  if (narg.lt.3) return
  call sic_ch (line,0,3,name,nc,.false.,error)
  if (error) return
  if (name.ne.'*') then
     call sic_r4 (line,0,3,reel,.true.,error)
     if (error) return
     aratio = reel
  else
     aratio = 0.0
  endif
  !
  ! Width ratio
  if (narg.lt.4) return
  call sic_ke (line,0,4,name,nc,.false.,error)
  if (error) return
  if (name.ne.'*') then
     call sic_r4 (line,0,4,reel,.true.,error)
     if (error) return
     wratio = reel
  else
     wratio = 0.0
  endif
end subroutine raicon
