module classfit_interfaces_private
  interface
    subroutine disgau(set,obs,fangle,yoff,doplot,error)
      use gbl_constant
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ private
      !   DISPLAY for GAUSS
      ! Retrieves and writes gauss fit results for current observation
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set     !
      type(header),        intent(in)    :: obs     ! Observation header
      real(kind=8),        intent(in)    :: fangle  !
      real(kind=4),        intent(in)    :: yoff    ! Optionnal Y offset
      logical,             intent(in)    :: doplot  ! on the graphic device
      logical,             intent(inout) :: error   ! Flag
    end subroutine disgau
  end interface
  !
  interface
    subroutine disnh3(set,obs,yoff,doplot,error)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ private
      !   DISPLAY for NH3
      ! Retrieves and writes line fit results for NH3
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set     !
      type(header),        intent(in)    :: obs     ! Observation header
      real(kind=4),        intent(in)    :: yoff    ! Optionnal Y offset
      logical,             intent(in)    :: doplot  ! on the graphic device
      logical,             intent(inout) :: error   ! Flag
    end subroutine disnh3
  end interface
  !
  interface
    subroutine disabs(set,obs,yoff,doplot,error)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ private
      !   DISPLAY for ABS
      ! Retrieves and writes line fit results for ABS
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set     !
      type(header),        intent(in)    :: obs     ! Observation header
      real(kind=4),        intent(in)    :: yoff    ! Optionnal Y offset
      logical,             intent(in)    :: doplot  ! on the graphic device
      logical,             intent(inout) :: error   ! Flag
    end subroutine disabs
  end interface
  !
  interface
    subroutine dishel(set,obs,yoff,doplot,error)
      use phys_const
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ private
      !   DISPLAY for SHELL
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set     !
      type(header),        intent(in)    :: obs     ! Observation header
      real(kind=4),        intent(in)    :: yoff    ! Optionnal Y offset
      logical,             intent(in)    :: doplot  ! on the graphic device
      logical,             intent(inout) :: error   ! Flag
    end subroutine dishel
  end interface
  !
  interface
    subroutine dispoi(set,obs,fangle,yoff,doplot,error)
      use gbl_constant
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ private
      ! Retrieves and writes gauss fit results for current observation
      ! Continuum method
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set     !
      type(header),        intent(in)    :: obs     ! Observation header
      real(kind=8),        intent(in)    :: fangle  !
      real(kind=4),        intent(in)    :: yoff    ! Optionnal Y offset
      logical,             intent(in)    :: doplot  ! also on X device
      logical,             intent(inout) :: error   ! Flag
    end subroutine dispoi
  end interface
  !
  interface
    subroutine displo(set,head,text,ltext,ifit,yoff)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ private
      !    DISPLAY
      ! Writes the output fit results in the PLOT window, for ONE line
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in) :: set    !
      type(header),        intent(in) :: head   !
      character(len=*),    intent(in) :: text   ! Text to draw
      integer(kind=4),     intent(in) :: ltext  ! Length of text
      integer(kind=4),     intent(in) :: ifit   ! Line number
      real(kind=4),        intent(in) :: yoff   ! Optionnal Y offset
    end subroutine displo
  end interface
  !
  interface
    subroutine iteabs(set,obs,checkbase,error)
      use class_types
      use gauss_parameter
      !---------------------------------------------------------------------
      ! @ private
      ! LAS Support routine for command
      !   FIT\ITERATE
      ! Iterates last ABS spectrum fitting
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set        !
      type(observation),   intent(inout) :: obs        !
      logical,             intent(in)    :: checkbase  ! Check if baseline removed?
      logical,             intent(inout) :: error      ! Error flag
    end subroutine iteabs
  end interface
  !
  interface
    subroutine itegauss(set,obs,checkbase,error)
      use gbl_message
      use class_types
      use gauss_parameter
      !---------------------------------------------------------------------
      ! @ private
      ! LAS Support routine for command
      !   FIT\ITERATE
      ! Case of gaussian profiles
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set        !
      type(observation),   intent(inout) :: obs        !
      logical,             intent(in)    :: checkbase  ! Check if baseline removed?
      logical,             intent(inout) :: error      ! Error flag
    end subroutine itegauss
  end interface
  !
  interface
    subroutine itenh3(set,obs,checkbase,error)
      use class_types
      use gauss_parameter
      !---------------------------------------------------------------------
      ! @ private
      ! LAS Support routine for command
      !    FIT\ITERATE
      ! Iterates last NH3 spectrum fitting
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set        !
      type(observation),   intent(inout) :: obs        !
      logical,             intent(in)    :: checkbase  ! Check if baseline removed?
      logical,             intent(inout) :: error      ! Error flag
    end subroutine itenh3
  end interface
  !
  interface
    subroutine iteshell(set,obs,checkbase,signal,image,error)
      use class_types
      use gauss_parameter
      !---------------------------------------------------------------------
      ! @ private
      ! LAS Support routine for command
      !    FIT\ITERATE
      ! Iterative shell-like profile fitting
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set        !
      type(observation),   intent(inout) :: obs        !
      logical,             intent(in)    :: checkbase  ! Check if baseline removed?
      real(kind=8),        intent(in)    :: signal     ! Signal band frequency
      real(kind=8),        intent(in)    :: image      ! Image band frequency
      logical,             intent(inout) :: error      ! Error status
    end subroutine iteshell
  end interface
  !
  interface
    subroutine guegauss(line,obs,set,error)
      use gildas_def
      use gbl_message
      use gbl_constant
      use class_setup_new
      use class_types
      use gauss_parameter
      !---------------------------------------------------------------------
      ! @ private
      ! LINE [Nline  [" Line1 Parameters"   ... ["LineN parameters"] ] ]
      !   [/INPUT Filename]
      !   [/NOCURSOR]
      !
      ! Nline is an optional argument
      !   If not present, the current number of lines is used
      !   If present, it indicates the number of lines to be fitted
      !   It can also be set to * or AUTOMATIC to use the current number of
      !     lines in the current spectrum
      !
      ! Define starting values for gaussian fits. These values are stored
      ! in array SPAR and are associated with an integer code defining the
      ! parameter class. The values can be estimated using the cursor if it
      ! is active. One then need to bracket the line, and the program gets
      ! its guesses by evaluating the moment of the spectrum in the
      ! delimited range. Codes cannot be changed in this mode. Conventions
      ! for these codes are:
      !  code = 0  variable parameter
      !  code = 1  fixed parameter
      !  code = 2  variable reference parameter
      !  code = 3  dependent parameter offset or ratio
      !  code = 4  fixed reference parameter
      !---------------------------------------------------------------------
      character(len=*),    intent(in)    :: line   ! Input command line
      type(observation),   intent(in)    :: obs    !
      type(class_setup_t), intent(in)    :: set    !
      logical,             intent(inout) :: error  ! Error flag
    end subroutine guegauss
  end interface
  !
  interface
    subroutine guenh3(line,obs,error)
      use gildas_def
      use gbl_message
      use class_types
      use gauss_parameter
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   LINES [N]
      ! 1       [/NOCURSOR]
      ! 2       [/INPUT File_Name]
      !
      !  Get Starting values for Fit on NH3 profiles
      !---------------------------------------------------------------------
      character(len=*),  intent(in)    :: line   ! Input command line
      type(observation), intent(in)    :: obs    !
      logical,           intent(inout) :: error  ! Error flag
    end subroutine guenh3
  end interface
  !
  interface
    subroutine gueabs(line,obs,error)
      use gildas_def
      use gbl_constant
      use gbl_message
      use class_types
      use gauss_parameter
      !---------------------------------------------------------------------
      ! @ private
      !  Define starting values for absorption fits. These values are stored
      ! in array SPAR and are associated with an integer code defining the
      ! parameter class. The values can be estimated using the cursor if it
      ! is active. One then need to bracket the line, and the program gets
      ! its guesses by evaluating the moment of the spectrum in the
      ! delimited range. Codes cannot be changed in this mode. Conventions
      ! for these codes are:
      !  code = 0  variable parameter
      !  code = 1  fixed parameter
      !  code = 2  variable reference parameter
      !  code = 3  dependent parameter offset or ratio
      !  code = 4  fixed reference parameter
      !---------------------------------------------------------------------
      character(len=*),  intent(in)    :: line   ! Input command line
      type(observation), intent(in)    :: obs    !
      logical,           intent(inout) :: error  ! Error flag
    end subroutine gueabs
  end interface
  !
  interface
    subroutine gueshell(line,obs,error)
      use gildas_def
      use gbl_message
      use gbl_constant
      use class_types
      use gauss_parameter
      !---------------------------------------------------------------------
      ! @ private
      ! Define starting values for gaussian fits. These values are stored
      ! in array SPAR and are associated with an integer code defining the
      ! parameter class. Conventions for these codes are:
      !  code = 0  variable parameter
      !  code = 1  fixed parameter
      !  code = 2  variable reference parameter
      !  code = 3  dependent parameter offset or ratio
      !  code = 4  fixed reference parameter
      !---------------------------------------------------------------------
      character(len=*),  intent(in)    :: line   ! Input command line
      type(observation), intent(in)    :: obs    !
      logical,           intent(inout) :: error  ! Error flag
    end subroutine gueshell
  end interface
  !
  interface
    subroutine guepoi(line,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! LAS Support routine for command
      !   LINES N
      ! for continuum data type
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line   ! Input command line
      logical,          intent(out) :: error  ! Error flag
    end subroutine guepoi
  end interface
  !
  interface
    subroutine check_line(kflag,nline,khead,ivar,ierr)
      use gbl_message
      use gauss_parameter, only: mxline
      !---------------------------------------------------------------------
      ! @ private
      !  Control the gauss fit parameter flags
      !---------------------------------------------------------------------
      integer(kind=4), intent(inout) :: kflag(mxline)  ! Flags to be checked
      integer(kind=4), intent(in)    :: nline          ! Number of lines
      integer(kind=4), intent(out)   :: khead          ! The independant line in the group, if relevant
      integer(kind=4), intent(in)    :: ivar           ! Variable code (fixed, adjustable)
      integer(kind=4), intent(out)   :: ierr           ! Error code
    end subroutine check_line
  end interface
  !
  interface
    subroutine rainh3(craie)
      use hyperfine_structure
      !-------------------------------------------------------------------
      ! @ private
      !-------------------------------------------------------------------
      character(len=*), intent(in) :: craie
    end subroutine rainh3
  end interface
  !
  interface
    subroutine raihfs(chain,error)
      use gildas_def
      use gbl_message
      use hyperfine_structure
      !-------------------------------------------------------------------
      ! @ private
      ! Read parameters of hyperfine multiplet
      !-------------------------------------------------------------------
      character(len=*), intent(in)    :: chain
      logical,          intent(inout) :: error
    end subroutine raihfs
  end interface
  !
  interface
    subroutine raicon(set,line,error)
      use class_setup_new
      use class_types
      use pointing_fit
      !----------------------------------------------------------------------
      ! @ private
      ! ANALYSE Support routine for command
      ! METHOD CONTINUUM width area-ratio width-ratio
      !----------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      character(len=*),    intent(in)    :: line   ! Command line
      logical,             intent(inout) :: error  ! Error flag
    end subroutine raicon
  end interface
  !
  interface
    subroutine midabs(obs,fit,ifatal,liter)
      use gildas_def
      use gbl_message
      use fit_minuit
      use class_types
      use gauss_parameter
      !----------------------------------------------------------------------
      ! @ private
      ! Set up the starting parameters for ABS fit
      !----------------------------------------------------------------------
      type(observation),  intent(in)    :: obs     !
      type(fit_minuit_t), intent(inout) :: fit     ! Fitting variables
      integer(kind=4),    intent(out)   :: ifatal  ! Number of errors
      logical,            intent(in)    :: liter   ! Dummy ?
    end subroutine midabs
  end interface
  !
  interface
    subroutine midgauss(obs,fit,ifatal,liter)
      use gildas_def
      use gbl_message
      use fit_minuit
      use class_types
      use gauss_parameter
      !----------------------------------------------------------------------
      ! @ private
      !  Start a gaussian fit by building the PAR array and internal variable
      ! used by Minuit
      !----------------------------------------------------------------------
      type(observation),  intent(in)    :: obs    !
      type(fit_minuit_t), intent(inout) :: fit    ! Fitting variables
      integer(kind=4),    intent(out)   :: ifatal ! Number of fatal errors
      logical,            intent(in)    :: liter  ! Iterate a fit
    end subroutine midgauss
  end interface
  !
  interface
    subroutine midnh3(obs,fit,ifatal,liter)
      use gildas_def
      use gbl_message
      use fit_minuit
      use class_types
      use gauss_parameter
      use hyperfine_structure
      !----------------------------------------------------------------------
      ! @ private
      ! Set up the starting parameters for NH3 fit.
      !----------------------------------------------------------------------
      type(observation),  intent(in)    :: obs    !
      type(fit_minuit_t), intent(inout) :: fit     ! Fitting variables
      integer(kind=4),    intent(out)   :: ifatal  ! Nb of errors
      logical,            intent(in)    :: liter   ! Iteration ?
    end subroutine midnh3
  end interface
  !
  interface
    subroutine begpoi(obs,datax,datay,nxy,vpar,wfit)
      use phys_const
      use gbl_message
      use class_types
      !----------------------------------------------------------------------
      ! @ private
      !  Compute guesses for the gaussian line fit to pointing results PAR(1)
      ! and PAR(2) have already been derived by another subroutine (INIPOI)
      !----------------------------------------------------------------------
      type(observation),     intent(inout) :: obs        !
      integer(kind=4),       intent(in)    :: nxy        ! Nb of data points
      integer(kind=4),       intent(in)    :: wfit(nxy)  ! Weights
      real(kind=xdata_kind), intent(in)    :: datax(nxy) ! Input X-data
      real(kind=4),          intent(in)    :: datay(nxy) ! Input Y-data
      real(kind=4),          intent(inout) :: vpar(8)    ! Guesses
    end subroutine begpoi
  end interface
  !
  interface
    subroutine midshell(obs,fit,ifatal,liter)
      use gildas_def
      use gbl_message
      use fit_minuit
      use class_types
      use gauss_parameter
      !----------------------------------------------------------------------
      ! @ private
      ! Sets up the starting parameter lists.
      !----------------------------------------------------------------------
      type(observation),  intent(in)    :: obs    !
      type(fit_minuit_t), intent(inout) :: fit     ! Fitting variables
      integer(kind=4),    intent(out)   :: ifatal  ! Nb of errors
      logical,            intent(in)    :: liter   ! Iteration ?
    end subroutine midshell
  end interface
  !
  interface
    subroutine minabs(npar,g,f,x,iflag,obs)
      use class_types
      use gauss_parameter
      use hyperfine_structure
      !----------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Function to be minimised for Absorption profile fitting
      ! Caution: R*8 and R*4 values are carefully mixed for speed
      !----------------------------------------------------------------------
      integer(kind=4),   intent(in)  :: npar
      real(kind=8),      intent(out) :: g(npar)
      real(kind=8),      intent(out) :: f
      real(kind=8),      intent(in)  :: x(npar)
      integer(kind=4),   intent(in)  :: iflag
      type(observation), intent(in)  :: obs
    end subroutine minabs
  end interface
  !
  interface
    subroutine mingauss(npar,g,f,x,iflag,obs)
      use class_types
      use gauss_parameter
      !----------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Function to be minimized in the gaussian fit. By using 3 hidden
      ! parameters, the method allows dependent and independent gaussians to
      ! be fitted.
      ! The computation is highly optimised, but take care of R*4 and
      ! R*8 values !... Basic parameters are Area, Position and Width.
      !----------------------------------------------------------------------
      integer(kind=4),   intent(in)  :: npar     ! Number of parameters
      real(kind=8),      intent(out) :: g(npar)  ! Array of derivatives, size 3+3*Nline (see below)
      real(kind=8),      intent(out) :: f        ! Function values
      real(kind=8),      intent(in)  :: x(npar)  ! Input parameter values, size 3+3*Nline (see midgauss)
      integer(kind=4),   intent(in)  :: iflag    ! Code operation
      type(observation), intent(in)  :: obs      !
    end subroutine mingauss
  end interface
  !
  interface
    subroutine fit_nocheck_parse(caller,line,iopt,set,checkbase,error)
      use class_types
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for parsing option
      !    /NOCHECK BASELINE
      !  of commands MINIMIZE and ITERATE
      !---------------------------------------------------------------------
      character(len=*),    intent(in)    :: caller
      character(len=*),    intent(in)    :: line
      integer(kind=4),     intent(in)    :: iopt
      type(class_setup_t), intent(in)    :: set
      logical,             intent(out)   :: checkbase
      logical,             intent(inout) :: error
    end subroutine fit_nocheck_parse
  end interface
  !
  interface
    subroutine fitgauss(fcn,set,obs,checkbase,liter,error)
      use gildas_def
      use gbl_constant
      use gbl_message
      use fit_minuit
      use class_setup_new
      use class_types
      use gauss_parameter
      !---------------------------------------------------------------------
      ! @ private
      ! FIT\ support routine for command
      !   MINIMIZE
      ! Setup and starts a GAUSS fit minimisation using MINUIT
      !---------------------------------------------------------------------
      interface
        subroutine fcn(npar,g,f,x,iflag,obs)  ! Function to be mininized
        use class_types
        integer(kind=4),   intent(in)  :: npar
        real(kind=8),      intent(out) :: g(npar)
        real(kind=8),      intent(out) :: f
        real(kind=8),      intent(in)  :: x(npar)
        integer(kind=4),   intent(in)  :: iflag
        type(observation), intent(in)  :: obs
        end subroutine fcn
      end interface
      type(class_setup_t), intent(in)            :: set        !
      type(observation),   intent(inout), target :: obs        ! 'target' because subject to a locwrd during minimization
      logical,             intent(in)            :: checkbase  ! Check if baseline removed?
      logical,             intent(in)            :: liter      ! Logical of iterate fit
      logical,             intent(inout)         :: error      ! Error status
    end subroutine fitgauss
  end interface
  !
  interface
    subroutine fitnh3(fcn,set,obs,checkbase,liter,error)
      use gildas_def
      use gbl_message
      use fit_minuit
      use class_types
      use gauss_parameter
      !---------------------------------------------------------------------
      ! @ private
      ! FIT\ support routine for command
      !   MINIMIZE
      ! Setup and starts a line minimisation using MINUIT and the hyperfine
      ! structure of the NH3 lines
      !---------------------------------------------------------------------
      interface
        subroutine fcn(npar,g,f,x,iflag,obs)  ! Function to be mininized
        use class_types
        integer(kind=4),   intent(in)  :: npar
        real(kind=8),      intent(out) :: g(npar)
        real(kind=8),      intent(out) :: f
        real(kind=8),      intent(in)  :: x(npar)
        integer(kind=4),   intent(in)  :: iflag
        type(observation), intent(in)  :: obs
        end subroutine fcn
      end interface
      type(class_setup_t), intent(in)            :: set        !
      type(observation),   intent(inout), target :: obs        ! 'target' because subject to a locwrd during minimization
      logical,             intent(in)            :: checkbase  ! Check if baseline removed?
      logical,             intent(in)            :: liter      ! Logical of iterate fit
      logical,             intent(inout)         :: error      ! Error status
    end subroutine fitnh3
  end interface
  !
  interface
    subroutine fitabs(fcn,set,obs,checkbase,liter,error)
      use gildas_def
      use gbl_constant
      use gbl_message
      use fit_minuit
      use class_types
      use gauss_parameter
      !---------------------------------------------------------------------
      ! @ private
      ! FIT\ support routine for command
      !   MINIMIZE
      ! Setup and starts a line minimisation using MINUIT and the hyperfine
      ! structure of the ABS lines
      !---------------------------------------------------------------------
      interface
        subroutine fcn(npar,g,f,x,iflag,obs)  ! Function to be minimized
        use class_types
        integer(kind=4),   intent(in)  :: npar
        real(kind=8),      intent(out) :: g(npar)
        real(kind=8),      intent(out) :: f
        real(kind=8),      intent(in)  :: x(npar)
        integer(kind=4),   intent(in)  :: iflag
        type(observation), intent(in)  :: obs
        end subroutine fcn
      end interface
      type(class_setup_t), intent(in)            :: set        !
      type(observation),   intent(inout), target :: obs        ! 'target' because subject to a locwrd during minimization
      logical,             intent(in)            :: checkbase  ! Check if baseline removed?
      logical,             intent(in)            :: liter      ! Logical of iterate fit
      logical,             intent(inout)         :: error      ! Error status
    end subroutine fitabs
  end interface
  !
  interface
    subroutine fitshell(fcn,set,obs,checkbase,liter,signal,image,error)
      use gildas_def
      use gbl_constant
      use phys_const
      use gbl_message
      use fit_minuit
      use class_types
      use gauss_parameter
      !---------------------------------------------------------------------
      ! @ private
      ! FIT\ support routine for command
      !   MINIMIZE
      ! Setup and starts a line minimisation using MINUIT and the structure
      ! of the SHELL lines
      !---------------------------------------------------------------------
      interface
        subroutine fcn(npar,g,f,x,iflag,obs)  ! Function to be mininized
        use class_types
        integer(kind=4),   intent(in)  :: npar
        real(kind=8),      intent(out) :: g(npar)
        real(kind=8),      intent(out) :: f
        real(kind=8),      intent(in)  :: x(npar)
        integer(kind=4),   intent(in)  :: iflag
        type(observation), intent(in)  :: obs
        end subroutine fcn
      end interface
      type(class_setup_t), intent(in)            :: set        !
      type(observation),   intent(inout), target :: obs        ! 'target' because subject to a locwrd during minimization
      logical,             intent(in)            :: checkbase  ! Check if baseline removed?
      logical,             intent(in)            :: liter      ! Logical of iterate fit
      real(kind=8),        intent(in)            :: signal     ! Signal band frequency
      real(kind=8),        intent(in)            :: image      ! Image band frequency
      logical,             intent(inout)         :: error      ! Error status
    end subroutine fitshell
  end interface
  !
  interface
    subroutine fitpoi(fcn,set,obs,checkbase,liter,error)
      use gildas_def
      use gbl_message
      use fit_minuit
      use class_setup_new
      use class_types
      use gauss_parameter
      !---------------------------------------------------------------------
      ! @ private
      ! FIT\ support routine for command
      !   MINIMIZE
      ! Setup and starts a GAUSS fit minimisation using MINUIT. Adapted
      ! from basic routine, by addition of a linear baseline removal.
      !---------------------------------------------------------------------
      interface
        subroutine fcn(npar,g,f,x,iflag,obs)  ! Function to be mininized
        use class_types
        integer(kind=4),   intent(in)  :: npar
        real(kind=8),      intent(out) :: g(npar)
        real(kind=8),      intent(out) :: f
        real(kind=8),      intent(in)  :: x(npar)
        integer(kind=4),   intent(in)  :: iflag
        type(observation), intent(in)  :: obs
        end subroutine fcn
      end interface
      type(class_setup_t), intent(in)            :: set        !
      type(observation),   intent(inout), target :: obs        ! 'target' because subject to a locwrd during minimization
      logical,             intent(in)            :: checkbase  ! Check if baseline removed?
      logical,             intent(in)            :: liter      ! Logical of iterate fit
      logical,             intent(inout)         :: error      ! Error status
    end subroutine fitpoi
  end interface
  !
  interface
    subroutine initva(set,obs,error)
      use gildas_def
      use class_types
      use gauss_parameter
      !---------------------------------------------------------------------
      ! @ private
      ! ANALYSE Internal routine
      !  Initialize the parameters for a gaussian fit using Minuit
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set
      type(observation),   intent(in)    :: obs
      logical,             intent(inout) :: error
    end subroutine initva
  end interface
  !
  interface
    subroutine inipoi(sigbas,sigrai,rdatax,rdatay,n,par,wfit,cbad,error)
      use gbl_message
      use class_parameter
      !---------------------------------------------------------------------
      ! @ private
      ! Initializes the parameters for a gaussian fit using Minuit
      !---------------------------------------------------------------------
      real(kind=4),          intent(out)    :: sigbas     ! Sigma on baseline
      real(kind=4),          intent(out)    :: sigrai     ! Sigma on area under line
      integer(kind=4),       intent(in)     :: n          ! Size of data array
      real(kind=xdata_kind), intent(in)     :: rdatax(n)  ! X values
      real(kind=4),          intent(inout)  :: rdatay(n)  ! Y-values
      real(kind=4),          intent(out)    :: par(2)     ! Input guesses (offset and slope)
      integer(kind=4),       intent(out)    :: wfit(n)    ! Weight (0 if bad, 1 if good)
      real(kind=4),          intent(in)     :: cbad       ! Blanking value
      logical,               intent(inout)  :: error      ! Error flag (too few data points)
    end subroutine inipoi
  end interface
  !
  interface
    subroutine minnh3(npar,g,chi2,x,iflag,obs)
      use class_types
      use gauss_parameter
      use hyperfine_structure
      !----------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Function to be minimised for NH3 profile fitting
      ! Caution : R*8 and R*4 values are carefully mixed for speed
      !----------------------------------------------------------------------
      integer(kind=4),   intent(in)  :: npar     ! Number of parameters
      real(kind=8),      intent(out) :: g(npar)  ! Array of derivatives
      real(kind=8),      intent(out) :: chi2     ! Chi^2
      real(kind=8),      intent(in)  :: x(npar)  ! Input parameter values
      integer(kind=4),   intent(in)  :: iflag    ! Code operation
      type(observation), intent(in)  :: obs      !
    end subroutine minnh3
  end interface
  !
  interface
    subroutine minpoi(npar,g,f,x,iflag,obs)
      use class_types
      !----------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Function to be minimized in the gaussian fit.
      ! By using 3 hidden parameters, the method allows dependent and
      ! independent gaussians to be fitted. The computation is highly
      ! optimised, but take care of R*4 and R*8 values !... Basic parameters
      ! are Area, Position and Width.
      !----------------------------------------------------------------------
      integer(kind=4),   intent(in)  :: npar     ! Number of parameters
      real(kind=8),      intent(out) :: g(npar)  ! Array of derivatives
      real(kind=8),      intent(out) :: f        ! Function value
      real(kind=8),      intent(in)  :: x(npar)  ! Input parameter values
      integer(kind=4),   intent(in)  :: iflag    ! Code operation
      type(observation), intent(in)  :: obs      !
    end subroutine minpoi
  end interface
  !
  interface
    subroutine minspo(obs,npar,g,f,x,iflag,tx,ty,ndata)
      use class_types
      use gauss_parameter
      !----------------------------------------------------------------------
      ! @ private
      !  Function to be minimized in the gaussian fit.
      !  By using 3 hidden parameters, the method allows dependent and
      ! independent gaussians to be fitted. The computation is highly
      ! optimised, but take care of R*4 and R*8 values !... Basic parameters
      ! are Area, Position and Width.
      !----------------------------------------------------------------------
      type(observation),     intent(in)  :: obs        !
      integer(kind=4),       intent(in)  :: npar       ! Number of parameters
      real(kind=8),          intent(out) :: g(npar)    ! Array of derivatives
      real(kind=8),          intent(out) :: f          ! Function value
      real(kind=8),          intent(in)  :: x(npar)    ! Parameter values
      integer(kind=4),       intent(in)  :: iflag      ! Code operation
      integer(kind=4),       intent(in)  :: ndata      ! Number of channels
      real(kind=xdata_kind), intent(in)  :: tx(ndata)  ! Abscissa
      real(kind=4),          intent(in)  :: ty(ndata)  ! Ordinate
    end subroutine minspo
  end interface
  !
  interface
    subroutine minshell(npar,g,f,x,iflag,obs)
      use class_types
      use gauss_parameter
      !----------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Function to be minimized in the Shell line fit.
      ! Basic parameters are Area, Width, Position, and Horn/Center ratio.
      ! Allows dependent lines to be fitted.
      !----------------------------------------------------------------------
      integer(kind=4),   intent(in)  :: npar     ! Number of parameters
      real(kind=8),      intent(out) :: g(npar)  ! Array of derivatives
      real(kind=8),      intent(out) :: f        ! Function value
      real(kind=8),      intent(in)  :: x(npar)  ! Input parameter values
      integer(kind=4),   intent(in)  :: iflag    ! Code operation
      type(observation), intent(in)  :: obs      !
    end subroutine minshell
  end interface
  !
  interface
    subroutine fshell(x0,dx,xp,fout,gg,dograd,error)
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      ! Computes the contribution of a Shell-like profile in the current
      ! channel to the square sum, and to the gradients relative to the
      ! parameters also if DOGRAD is .true.. Highly optimised ?
      !----------------------------------------------------------------------
      real(kind=4), intent(in)  :: x0      ! Abscissa
      real(kind=4), intent(in)  :: dx      ! Freq resolution
      real(kind=4), intent(in)  :: xp(4)   ! Input parameters
      real(kind=4), intent(out) :: fout    ! Output value of Shell function
      real(kind=4), intent(out) :: gg(4)   ! Gradients
      logical,      intent(in)  :: dograd  ! Compute contribution to gradients
      logical,      intent(out) :: error   ! Logical error flag
    end subroutine fshell
  end interface
  !
  interface
    subroutine plot_curr(profil,obs,m)
      use class_types
      !----------------------------------------------------------------------
      ! @ private
      ! FIT\ Internal routine for command
      !   VISUALIZE [M]
      ! Plot the sum of all components of a fitted profile (M = 0) or
      ! the Mth component only (M .ne. 0)
      !----------------------------------------------------------------------
      interface
        function profil(obs,x0,m,dobase)
        use class_types
        real(kind=4) :: profil
        type(observation), intent(in)  :: obs
        real(kind=4),      intent(in)  :: x0
        integer(kind=4),   intent(in)  :: m
        logical,           intent(out) :: dobase
        end function profil
      end interface
      type(observation), intent(in) :: obs  !
      integer(kind=4),   intent(in) :: m    ! Function number (0 = all functions)
    end subroutine plot_curr
  end interface
  !
  interface
    subroutine plot_freq(profil,obs,m)
      use class_parameter
      use class_types
      !----------------------------------------------------------------------
      ! @ private
      ! FIT\ Internal routine for command
      ! VISUALIZE [M]
      ! Plot the sum of all components of a fitted profile (M = 0) or
      ! the Mth component only (M .ne. 0)
      !----------------------------------------------------------------------
      interface
        function profil(obs,a,m,error)
        use class_types
        real(kind=4) :: profil
        type(observation), intent(in)  :: obs
        real(kind=4),      intent(in)  :: a
        integer(kind=4),   intent(in)  :: m
        logical,           intent(out) :: error
        end function profil
      end interface
      type(observation), intent(in) :: obs  !
      integer(kind=4),   intent(in) :: m    ! Function number (0 = all functions)
    end subroutine plot_freq
  end interface
  !
  interface
    function proabs1(obs,vi,m)
      use class_types
      use hyperfine_structure
      !----------------------------------------------------------------------
      ! @ private
      ! FIT\ internal routine
      !----------------------------------------------------------------------
      real(kind=4) :: proabs1  ! Function value on return
      type(observation), intent(in) :: obs  ! Input observation
      real(kind=4),      intent(in) :: vi   ! abscissa
      integer(kind=4),   intent(in) :: m    ! which component
    end function proabs1
  end interface
  !
  interface
    function propoi(obs,x0,m,dobase)
      use class_types
      !----------------------------------------------------------------------
      ! @ private
      ! FIT\ Internal routine
      ! M.eq.0:  Computes the composite profile from sum of all ABS profiles
      ! M.ne.0:  Computes the profile from gaussian number M
      ! Computes the profile from gaussian number M
      ! Modified version with a linear baseline. Note that it is called
      ! by GAUSSFIT with a dummy argument DOBASE after M.
      !----------------------------------------------------------------------
      real(kind=4) :: propoi  ! Function value on return
      type(observation), intent(in)  :: obs     ! Input observation
      real(kind=4),      intent(in)  :: x0      ! Value at which the profile should be computed 
      integer(kind=4),   intent(in)  :: m       ! Component number (0 = all)
      logical,           intent(out) :: dobase  ! Is A on the line or on the baseline ?
    end function propoi
  end interface
  !
  interface
    subroutine fit_res_comm(set,line,rname,obs,doresu,user_function,error)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for commands:
      !   FIT\RESULT [N]
      !   FIT\RESIDUAL [N]
      ! Redirection relying on SET METHOD
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      character(len=*),    intent(in)    :: line           ! Input command line
      character(len=*),    intent(in)    :: rname          ! Calling routine name
      type(observation),   intent(inout) :: obs            !
      logical,             intent(in)    :: doresu         ! result or residual?
      logical,             external      :: user_function  !
      logical,             intent(inout) :: error          ! Error flag
    end subroutine fit_res_comm
  end interface
  !
  interface
    subroutine fit_res(set,obs,doresu,fnc,m,user_function,error)
      use gildas_def
      use class_types
      !--------------------------------------------------------------------
      ! @ private
      ! Support routine for commands
      !  FIT\RESULT [N]
      !  FIT\RESIDUAL [N]
      !
      ! Compute the residuals from last fit
      !  M indicates which line must be subtracted. If M = 0 (default), all
      ! lines must be subtracted.
      !  Copy R into T
      !  Compute R = T - Fit(N)
      !--------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      type(observation),   intent(inout) :: obs            !
      logical,             intent(in)    :: doresu         ! result or residual?
      integer(kind=4),     intent(in)    :: m              ! Line number
      logical,             external      :: user_function  !
      real(kind=4),        external      :: fnc            ! Function used for the fit
      logical,             intent(inout) :: error          ! Error flag
    end subroutine fit_res
  end interface
  !
end module classfit_interfaces_private
