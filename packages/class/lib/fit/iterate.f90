subroutine iterate_fit(set,line,obs,error)
  use gbl_message
  use classfit_dependencies_interfaces
  use classfit_interfaces, except_this=>iterate_fit
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! Support routine for command
  !   FIT\ITERATE
  ! 1     /NOCHECK BASELINE
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  character(len=*),    intent(in)    :: line   ! Input command line
  type(observation),   intent(inout) :: obs    !
  logical,             intent(inout) :: error  ! Flag
  ! Local
  character(len=*), parameter :: proc='ITERATE'
  integer(kind=4), parameter :: optnocheck=1
  logical :: checkbase
  integer(kind=4) :: i,niter
  character(len=message_length) :: mess
  !
  ! Decode line
  niter = 1
  call sic_i4(line,0,1,niter,.false.,error)
  if (error) then
    call class_message(seve%e,proc,'Error reading number of iterations')
    return
  endif
  !
  call fit_nocheck_parse(proc,line,optnocheck,set,checkbase,error)
  if (error)  return
  !
  ! Iterate
  do i=1,niter
    write(mess,'(a,i3)') 'Starting iteration #',i
    call class_message(seve%i,proc,mess)
    select case (set%method)
    case ('GAUSS')
      call itegauss(set,obs,checkbase,error)
    case ('NH3','HFS')
      call itenh3(set,obs,checkbase,error)
    case ('ABSORPTION')
      call iteabs(set,obs,checkbase,error)
    case ('SHELL')
      call iteshell(set,obs,checkbase,obs%head%spe%restf,obs%head%spe%image,error)
    case default
      call class_message(seve%e,proc,'Not implement for '//trim(set%method)//' data')
      error = .true.
      return
    end select
    if (error) return
  enddo
  !
end subroutine iterate_fit
!
subroutine iteabs(set,obs,checkbase,error)
  use classfit_interfaces, except_this=>iteabs
  use class_types
  use gauss_parameter
  !---------------------------------------------------------------------
  ! @ private
  ! LAS Support routine for command
  !   FIT\ITERATE
  ! Iterates last ABS spectrum fitting
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set        !
  type(observation),   intent(inout) :: obs        !
  logical,             intent(in)    :: checkbase  ! Check if baseline removed?
  logical,             intent(inout) :: error      ! Error flag
  ! Local
  real(kind=4) :: zpar(4*mxline)
  integer(kind=4) :: i,j,i1
  !
  i1=nline
  nline=max(nline,1)
  ! Save previous parameters
  do i=1,3*mxline+1
    zpar(i)=spar(i)
  enddo
  !
  ! Retrieve Last Fit
  do j=1,3*nline+1
    spar(j)=par(j)
  enddo
  call fitabs(minabs,set,obs,checkbase,.true.,error)
  nline=i1
  ! Restore Last Parameters
  do i=1,3*mxline+1
    spar(i)=zpar(i)
  enddo
end subroutine iteabs
!
subroutine itegauss(set,obs,checkbase,error)
  use gbl_message
  use classfit_interfaces, except_this=>itegauss
  use class_types
  use gauss_parameter
  !---------------------------------------------------------------------
  ! @ private
  ! LAS Support routine for command
  !   FIT\ITERATE
  ! Case of gaussian profiles
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set        !
  type(observation),   intent(inout) :: obs        !
  logical,             intent(in)    :: checkbase  ! Check if baseline removed?
  logical,             intent(inout) :: error      ! Error flag
  ! Local
  integer(kind=4) :: i,j,i1
  real(kind=4) :: zpar(4*mxline)
  ! equivalence (kt(1),kt1), (kv(1),kv1), (kd(1),kd1)
  !
  i1=nline
  if (nline.eq.-1) then
     nline = ngline
  endif
  nline=max(nline,1)
  j=1
  ! Save Last Parameters
  zpar = spar
  !
  ! Retrieve Last Fit
  do i=1,nline
     if (par(j+2).ne.0.0) then
        spar(j) = par(j)/par(j+2)/1.064467
     else
        error = .true.
        call class_message(seve%e,'ITERA','Bad input parameters')
        spar = zpar
        return
     endif
     if (kt(i).eq.3) spar(j)=par(j)/par(3*kt0-2)
     j=j+1
     spar(j)=par(j)
     if (kv(i).eq.3) spar(j)=par(j)-par(3*kv0-1)
     j=j+1
     spar(j)=par(j)
     if (kd(i).eq.3) spar(j)=par(j)/par(3*kd0)
     j=j+1
  enddo
  call fitgauss(mingauss,set,obs,checkbase,.true.,error)
  nline=i1
  !
  ! Restore Last Parameters
  spar = zpar
end subroutine itegauss
!
subroutine itenh3(set,obs,checkbase,error)
  use classfit_interfaces, except_this=>itenh3
  use class_types
  use gauss_parameter
  !---------------------------------------------------------------------
  ! @ private
  ! LAS Support routine for command
  !    FIT\ITERATE
  ! Iterates last NH3 spectrum fitting
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set        !
  type(observation),   intent(inout) :: obs        !
  logical,             intent(in)    :: checkbase  ! Check if baseline removed?
  logical,             intent(inout) :: error      ! Error flag
  ! Local
  real(kind=4) :: zpar(4*mxline)
  integer(kind=4) :: i,i1
  !
  i1=nline
  nline=max(nline,1)
  !
  ! Save Last Parameters
  zpar = spar
  !
  ! Retrieve Last Fit
  do i=1,4*nline
     spar(i)=par(i)
  enddo
  call fitnh3(minnh3,set,obs,checkbase,.true.,error)
  nline=i1
  !
  ! Restore Last Parameters
  spar = zpar
end subroutine itenh3
!
subroutine iteshell(set,obs,checkbase,signal,image,error)
  use classfit_interfaces, except_this=>iteshell
  use class_types
  use gauss_parameter
  !---------------------------------------------------------------------
  ! @ private
  ! LAS Support routine for command
  !    FIT\ITERATE
  ! Iterative shell-like profile fitting
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set        !
  type(observation),   intent(inout) :: obs        !
  logical,             intent(in)    :: checkbase  ! Check if baseline removed?
  real(kind=8),        intent(in)    :: signal     ! Signal band frequency
  real(kind=8),        intent(in)    :: image      ! Image band frequency
  logical,             intent(inout) :: error      ! Error status
  ! Local
  integer(kind=4) :: i1,j,i
  real(kind=4) :: zpar(4*mxline)
  !
  i1=nline
  ! Automatic mode
  if (nline.eq.-1) then
     nline=ngline
  endif
  nline=max(nline,1)
  !
  ! Save Last Parameters
  zpar = spar
  !
  ! Retrieve Last Fit
  ! code = 3 : parameter fixed with respect to parameter coded 2 or 4
  j = 1
  do i=1,nline
     spar(j)=par(j)
     if (kt(i).eq.3) spar(j)=par(j)/par(4*kt0-3)
     j=j+1
     spar(j)=par(j)
     if (kv(i).eq.3) spar(j)=par(j)-par(4*kv0-2)
     j=j+1
     spar(j)=par(j)
     if (kd(i).eq.3) spar(j)=par(j)/par(4*kd0-1)
     j=j+1
     spar(j)=par(j)
     if (kh(i).eq.3) then
        if (par(4*kh0).ne.0) then
           spar(j)=par(j)/par(4*kh0)
        else
           spar(j)=1.0
        endif
     endif
     j=j+1
  enddo
  call fitshell(minshell,set,obs,checkbase,.true.,signal,image,error)
  nline=i1
  !
  ! Restore Last Parameters
  spar = zpar
end subroutine iteshell
