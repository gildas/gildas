!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module classfit_interfaces
  !---------------------------------------------------------------------
  ! CLASSFIT interfaces, all kinds (private or public). Do not use
  ! directly out of the library.
  !---------------------------------------------------------------------
  !
  use classfit_interfaces_public
  use classfit_interfaces_private
  !
end module classfit_interfaces
!
module classfit_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! CLASSFIT dependencies public interfaces. Do not use out of the
  ! library.
  !---------------------------------------------------------------------
  !
  use gkernel_interfaces
  use classcore_interfaces_public
end module classfit_dependencies_interfaces
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
