subroutine minimize(set,line,obs,error)
  use gbl_message
  use classfit_interfaces, except_this=>minimize
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! Support routine for command:
  !    FIT\MINIMIZE
  ! 1     /NOCHECK BASELINE
  ! Redirection relying on SET METHOD
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  character(len=*),    intent(in)    :: line   ! Input command line
  type(observation),   intent(inout) :: obs    !
  logical,             intent(inout) :: error  ! Error flag
  ! Local
  character(len=*), parameter :: rname='MINIMIZE'
  integer(kind=4), parameter :: optnocheck=1
  logical :: checkbase
  !
  call fit_nocheck_parse(rname,line,optnocheck,set,checkbase,error)
  if (error)  return
  !
  select case (set%method)
  case ('CONTINUUM')
    call fitpoi(minpoi,set,obs,checkbase,.false.,error)
  case ('GAUSS')
    call fitgauss(mingauss,set,obs,checkbase,.false.,error)
  case ('NH3','HFS')
    call fitnh3(minnh3,set,obs,checkbase,.false.,error)
  case ('ABSORPTION')
    call fitabs(minabs,set,obs,checkbase,.false.,error)
  case ('SHELL')
    call fitshell(minshell,set,obs,checkbase,.false.,obs%head%spe%restf,obs%head%spe%image,error)
  case default
    call class_message(seve%e,rname,'Not implemented for method '//set%method)
    error = .true.
    return
  end select
  !
end subroutine minimize
!
subroutine fit_nocheck_parse(caller,line,iopt,set,checkbase,error)
  use classfit_dependencies_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Support routine for parsing option
  !    /NOCHECK BASELINE
  !  of commands MINIMIZE and ITERATE
  !---------------------------------------------------------------------
  character(len=*),    intent(in)    :: caller
  character(len=*),    intent(in)    :: line
  integer(kind=4),     intent(in)    :: iopt
  type(class_setup_t), intent(in)    :: set
  logical,             intent(out)   :: checkbase
  logical,             intent(inout) :: error
  ! Local
  integer(kind=4), parameter :: mnocheck=1
  character(len=8), parameter :: nocheck(mnocheck) = (/ 'BASELINE' /)
  character(len=9) :: argum,key
  integer(kind=4) :: nc,ikey
  !
  ! Default is to check if the baseline was removed for all models
  ! (which tend to 0 at edges) except for CONTINUUM (which also fits
  ! a linear background) and ABSORPTION (Which also fits a continuum
  ! level)
  checkbase = set%method.ne.'CONTINUUM' .and.  &
              set%method.ne.'ABSORPTION'
  !
  if (.not.sic_present(iopt,0))  return
  !
  call sic_ke(line,iopt,1,argum,nc,.true.,error)
  if (error) return
  !
  call sic_ambigs(caller,argum,key,ikey,nocheck,mnocheck,error)
  if (error) return
  !
  checkbase = .not.(ikey.eq.1)
  !
end subroutine fit_nocheck_parse
!
subroutine fitgauss(fcn,set,obs,checkbase,liter,error)
  use gildas_def
  use gbl_constant
  use gbl_message
  use fit_minuit
  use classfit_dependencies_interfaces
  use classfit_interfaces, except_this=>fitgauss
  use class_setup_new
  use class_types
  use gauss_parameter
  !---------------------------------------------------------------------
  ! @ private
  ! FIT\ support routine for command
  !   MINIMIZE
  ! Setup and starts a GAUSS fit minimisation using MINUIT
  !---------------------------------------------------------------------
  interface
    subroutine fcn(npar,g,f,x,iflag,obs)  ! Function to be mininized
    use class_types
    integer(kind=4),   intent(in)  :: npar
    real(kind=8),      intent(out) :: g(npar)
    real(kind=8),      intent(out) :: f
    real(kind=8),      intent(in)  :: x(npar)
    integer(kind=4),   intent(in)  :: iflag
    type(observation), intent(in)  :: obs
    end subroutine fcn
  end interface
  type(class_setup_t), intent(in)            :: set        !
  type(observation),   intent(inout), target :: obs        ! 'target' because subject to a locwrd during minimization
  logical,             intent(in)            :: checkbase  ! Check if baseline removed?
  logical,             intent(in)            :: liter      ! Logical of iterate fit
  logical,             intent(inout)         :: error      ! Error status
  ! Local
  character(len=*), parameter :: proc='FITGAUSS'
  integer(kind=4) :: i,k,l,k1,k2,ier
  real(kind=8) :: dx,al,ba,du1,du2
  character(len=message_length) :: mess
  type(fit_minuit_t) :: fit
  !
  ! Check that a Baseline has been removed
  if (checkbase .and. .not.obs%head%presec(class_sec_bas_id)) then
     call class_message(seve%e,proc,'No baseline removed')
     error = .true.
     return
  endif
  !
  ! Added error Patch for Initialisation
  ngline = nline
  error = .false.
  fit%maxext=ntot
  fit%maxint=nvar
  fit%verbose=.true.
  fit%owner = gpack_get_id('class',.true.,error)
  if (error)  return
  !
  ! Initialise values
  call initva(set,obs,error)
  if (error) return
  obs%head%gau%nline = max(nline,1)
  !
  ! Compute deltav (Assume x array is sorted)
  ! Note: this solution will NOT work properly in special
  ! cases (e.g. everal sub-bands with big spaces)
  if (obs%head%presec(class_sec_xcoo_id)) then
     deltav = maxval(abs(obs%datax(obs%cimin+1:obs%cimax)-obs%datax(obs%cimin:obs%cimax-1)))
  else
     deltav = abs(obs%datax(obs%cimin+1)-obs%datax(obs%cimin))
  endif
  !
  call midgauss(obs,fit,ier,liter)
  if (ier.ne.0) then
     error = .true.
     return
  endif
  call intoex(fit,fit%x)
  fit%up = 0.5*(sigbas**2+sigrai**2)
  fit%nfcnmx  = 1000
  fit%epsi  = 0.1d0 * fit%up
  fit%newmin  = 0
  fit%itaur  = 0
  fit%isw(1)  = 0
  fit%isw(3)  = 1
  fit%nfcn = 1
  fit%vtest  = 0.04
  call fcn(fit%npar,fit%g,fit%amin,fit%u,1,obs)
  !
  ! Simplex Minimization
  ! Update obs%head%gau%nfit: used by progauss to compute rms
  if (.not.liter) then
     call simplx(fit,fcn,ier)
     if (ier.ne.0) then
        error =.true.
        return
     endif
     k=1
     do i=1,max(nline,1)
        par(k)  =fit%u(k+3)*fit%u(1)*1.7724538
        par(k+1)=fit%u(k+4)+fit%u(2)
        par(k+2)=fit%u(k+5)*1.665109*fit%u(3)
        obs%head%gau%nfit(k:k+2) = par(k:k+2)
        k=k+3
     enddo
  endif
  !
  ! Gradient Minimization
  call intoex(fit,fit%x)
  call fcn(fit%npar,fit%g,fit%amin,fit%u,3,obs) ! flag=3 --> estimate rms with progauss
  write(mess,1002) sigbas,sigrai
  call class_message(seve%i,proc,mess)
  fit%up = sigbas**2
  fit%epsi  = 0.1d0 * fit%up
  fit%apsi  = fit%epsi
  call hesse(fit,fcn)
  call migrad(fit,fcn,ier)
  if (ier.eq.1) then
     call hesse(fit,fcn)
     ier = 0
  elseif (ier.eq.3) then
     call class_message(seve%w,proc,'Solution not converged')
     ier = 0
  endif
  !
  ! Print Results
  ! Update obs%head%gau%nfit: used by progauss to compute rms
  k=1
  do i=1,max(nline,1)
     par(k)  =fit%u(k+3)*fit%u(1)*1.7724538
     par(k+1)=fit%u(k+4)+fit%u(2)
     par(k+2)=fit%u(k+5)*fit%u(3)*1.665109
     obs%head%gau%nfit(k:k+2) = par(k:k+2)
     k=k+3
  enddo
  call intoex(fit,fit%x)
  !
  ! flag.eq.3: Final computations
  call fcn(fit%npar,fit%g,fit%amin,fit%u,3,obs)
  write(mess,1002) sigbas,sigrai
  call class_message(seve%i,proc,mess)
  fit%up  = sigbas**2
  !
  ! Calculate External Errors
  do i=1,fit%nu
     l  = fit%lcorsp(i)
     if (l .eq. 0)  then
        fit%werr(i)=0.
     else
        if (fit%isw(2) .ge. 1)  then
           dx = sqrt(abs(fit%v(l,l)*fit%up))
           if (fit%lcode(i) .gt. 1) then
              al = fit%alim(i)
              ba = fit%blim(i) - al
              du1 = al + 0.5d0 *(sin(fit%x(l)+dx) +1.0d0) * ba - fit%u(i)
              du2 = al + 0.5d0 *(sin(fit%x(l)-dx) +1.0d0) * ba - fit%u(i)
              if (dx .gt. 1.0d0)  du1 = ba
              dx = 0.5d0 * (abs(du1) + abs(du2))
           endif
           fit%werr(i) = dx
        endif
     endif
  enddo
  !
  k=1
  do i=1,max(nline,1)
     k1=k+1
     k2=k+2
     err(k)=fit%werr(k+3)
     if (i.eq.kt0) err(k)=fit%werr(1)
     err(k1)=fit%werr(k1+3)
     if (i.eq.kv0) err(k1)=fit%werr(2)
     err(k2)=fit%werr(k2+3)
     if (i.eq.kd0) err(k2)=fit%werr(3)
     k=k+3
  enddo
  !
  ! Copy results
  obs%head%gau%sigba = sigbas
  obs%head%gau%sigra = sigrai
  obs%head%gau%nfit(:) = 0.
  obs%head%gau%nfit(1:ngauspar*obs%head%gau%nline) = par(1:ngauspar*obs%head%gau%nline)
  obs%head%gau%nerr(:) = 0.
  obs%head%gau%nerr(1:ngauspar*obs%head%gau%nline) = err(1:ngauspar*obs%head%gau%nline)
  obs%head%presec(class_sec_gau_id) = .true.
  !
  ! Print results
  write(mess,*) 'Number of calls: ',fit%nfcn
  call class_message(seve%i,proc,mess)
  call disgau(set,obs%head,class_setup_get_fangle(),0.,.false.,error)
  !
  !
1002 format (' RMS of Residuals :  Base = ',1pg9.2,'  Line = ',1pg9.2)
end subroutine fitgauss
!
subroutine fitnh3(fcn,set,obs,checkbase,liter,error)
  use gildas_def
  use gbl_message
  use fit_minuit
  use classfit_dependencies_interfaces
  use classfit_interfaces, except_this=>fitnh3
  use class_types
  use gauss_parameter
  !---------------------------------------------------------------------
  ! @ private
  ! FIT\ support routine for command
  !   MINIMIZE
  ! Setup and starts a line minimisation using MINUIT and the hyperfine
  ! structure of the NH3 lines
  !---------------------------------------------------------------------
  interface
    subroutine fcn(npar,g,f,x,iflag,obs)  ! Function to be mininized
    use class_types
    integer(kind=4),   intent(in)  :: npar
    real(kind=8),      intent(out) :: g(npar)
    real(kind=8),      intent(out) :: f
    real(kind=8),      intent(in)  :: x(npar)
    integer(kind=4),   intent(in)  :: iflag
    type(observation), intent(in)  :: obs
    end subroutine fcn
  end interface
  type(class_setup_t), intent(in)            :: set        !
  type(observation),   intent(inout), target :: obs        ! 'target' because subject to a locwrd during minimization
  logical,             intent(in)            :: checkbase  ! Check if baseline removed?
  logical,             intent(in)            :: liter      ! Logical of iterate fit
  logical,             intent(inout)         :: error      ! Error status
  ! Local
  character(len=*), parameter :: proc='FITNH3'
  character(len=1) :: unit,unit_low
  integer(kind=4) :: i,k,l,ier
  real(kind=8) :: dx, al, ba, du1, du2
  character(len=message_length) :: mess
  type(fit_minuit_t) :: fit
  !
  call geunit(set,obs%head,unit,unit_low)
  if (unit.ne.'V') then
     call class_message(seve%e,proc,'Unit X must be Velocity')
     error = .true.
     return
  endif
  !
  ! Check that a Baseline has been removed
  if (checkbase .and. .not.obs%head%presec(class_sec_bas_id)) then
     call class_message(seve%e,proc,'No baseline removed')
     error = .true.
     return
  endif
  !
  fit%maxext=ntot
  fit%maxint=nvar
  fit%verbose = .true.
  fit%owner = gpack_get_id('class',.true.,error)
  if (error)  return
  !
  ! Initialise values
  ngline = nline
  call initva(set,obs,error)
  if (error) return
  call midnh3(obs,fit,ier,liter)
  if (ier.ne.0) then
     error = .true.
     return
  endif
  obs%head%hfs%nline = max(nline,1)
  !
  ! Transforms from internal coordinates (PINT) to external parameters (U)
  call intoex(fit,fit%x)
  fit%up=sigbas**2
  fit%nfcnmx = 1000
  fit%epsi = 0.1d0 * fit%up
  fit%newmin = 0
  fit%itaur = 0
  fit%isw(1) = 0
  fit%isw(3) = 1
  fit%nfcn=1
  call fcn(fit%npar,fit%g,fit%amin,fit%u,1,obs)
  fit%vtest = 0.04d0
  !
  ! Simplex minimization
  ! Update obs%head%nh3%nfit: used by pronh3 to compute rms
  if (.not.liter) then
     ier = 0
     call simplx(fit,fcn,ier)
     if (ier.ne.0) then
        error = .true.
        return
     endif
     k=1
     do i=1,max(nline,1)
        par(k)=fit%u(k)
        par(k+1)=fit%u(k+1)
        par(k+2)=fit%u(k+2)*1.665109
        par(k+3)=fit%u(k+3)
        obs%head%hfs%nfit(k:k+3) = par(k:k+3)
        k=k+4
     enddo
  endif
  !
  ! Gradient minimization
  call fcn(fit%npar,fit%g,fit%amin,fit%u,3,obs)
  write(mess,1002) sigbas,sigrai
  call class_message(seve%i,proc,mess)
  fit%up   = sigbas**2
  fit%epsi = 0.1d0 * fit%up
  fit%apsi = fit%epsi
  call hesse(fit,fcn)
  call migrad(fit,fcn,ier)
  if (ier.eq.1) then
     call hesse(fit,fcn)
     ier = 0
  elseif (ier.eq.3) then
     call class_message(seve%w,proc,'Solution not converged')
     ier = 0
  endif
  !
  ! Print results
  ! Update obs%head%nh3%nfit: used by pronh3 to compute rms
  k=1
  do i=1,max(nline,1)
     par(k)=fit%u(k)
     par(k+1)=fit%u(k+1)
     par(k+2)=fit%u(k+2)*1.665109
     par(k+3)=fit%u(k+3)
     obs%head%hfs%nfit(k:k+3) = par(k:k+3)
     k=k+4
  enddo
  call fcn(fit%npar,fit%g,fit%amin,fit%u,3,obs)
  write(mess,1002) sigbas,sigrai
  call class_message(seve%i,proc,mess)
  !
  fit%up=sigbas**2
  do i=1,fit%nu
     l = fit%lcorsp(i)
     if (l .eq. 0)  then
        fit%werr(i)=0.
     else
        if (fit%isw(2) .ge. 1)  then
           dx = sqrt(abs(fit%v(l,l)*fit%up))
           if (fit%lcode(i) .gt. 1) then
              al = fit%alim(i)
              ba = fit%blim(i) - al
              du1 = al + 0.5d0 *(sin(fit%x(l)+dx) +1.0d0) * ba - fit%u(i)
              du2 = al + 0.5d0 *(sin(fit%x(l)-dx) +1.0d0) * ba - fit%u(i)
              if (dx .gt. 1.0d0)  du1 = ba
              dx = 0.5d0 * (abs(du1) + abs(du2))
           endif
           fit%werr(i) = dx
        endif
     endif
  enddo
  do k=1,4*max(nline,1)
     err(k)=fit%werr(k)
     if (mod(k,4).eq.3) err(k)=err(k)*1.665109
  enddo
  !
  ! Copy results
  obs%head%hfs%sigba = sigbas
  obs%head%hfs%sigra = sigrai
  obs%head%hfs%nfit(:) = 0.
  obs%head%hfs%nfit(1:nhfspar*obs%head%hfs%nline) = par(1:nhfspar*obs%head%hfs%nline)
  obs%head%hfs%nerr(:) = 0.
  obs%head%hfs%nerr(1:nhfspar*obs%head%hfs%nline) = err(1:nhfspar*obs%head%hfs%nline)
  obs%head%presec(class_sec_hfs_id) = .true.
  !
  ! Print results
  write(mess,*) 'Number of calls: ',fit%nfcn
  call class_message(seve%i,proc,mess)
  call disnh3(set,obs%head,0.,.false.,error)
  !
  !
1002 format (' RMS of Residuals :  Base = ',1pg9.2,'  Line = ',1pg9.2)
end subroutine fitnh3
!
subroutine fitabs(fcn,set,obs,checkbase,liter,error)
  use gildas_def
  use gbl_constant
  use gbl_message
  use fit_minuit
  use classfit_dependencies_interfaces
  use classfit_interfaces, except_this=>fitabs
  use class_types
  use gauss_parameter
  !---------------------------------------------------------------------
  ! @ private
  ! FIT\ support routine for command
  !   MINIMIZE
  ! Setup and starts a line minimisation using MINUIT and the hyperfine
  ! structure of the ABS lines
  !---------------------------------------------------------------------
  interface
    subroutine fcn(npar,g,f,x,iflag,obs)  ! Function to be minimized
    use class_types
    integer(kind=4),   intent(in)  :: npar
    real(kind=8),      intent(out) :: g(npar)
    real(kind=8),      intent(out) :: f
    real(kind=8),      intent(in)  :: x(npar)
    integer(kind=4),   intent(in)  :: iflag
    type(observation), intent(in)  :: obs
    end subroutine fcn
  end interface
  type(class_setup_t), intent(in)            :: set        !
  type(observation),   intent(inout), target :: obs        ! 'target' because subject to a locwrd during minimization
  logical,             intent(in)            :: checkbase  ! Check if baseline removed?
  logical,             intent(in)            :: liter      ! Logical of iterate fit
  logical,             intent(inout)         :: error      ! Error status
  ! Local
  character(len=*), parameter :: proc='FITABS'
  character(kind=1) :: unit,unit_low
  integer(kind=4) :: i,k,l,ier
  real(kind=8) :: dx, al, ba, du1, du2
  character(len=message_length) :: mess
  type(fit_minuit_t) :: fit
  !
  call geunit(set,obs%head,unit,unit_low)
  if (unit.ne.'V') then
    call class_message(seve%e,proc,'Unit X must be Velocity')
    error = .true.
    return
  endif
  !
  ! Check that a Baseline has been removed. NB: the default it NOT
  ! to check this, as the method fits a continuum level.
  if (checkbase .and. .not.obs%head%presec(class_sec_bas_id)) then
     call class_message(seve%e,proc,'No baseline removed')
     error = .true.
     return
  endif
  !
  fit%maxext=ntot
  fit%maxint=nvar
  fit%verbose = .true.
  fit%owner = gpack_get_id('class',.true.,error)
  if (error)  return
  !
  ! Initialise values
  ngline = nline
  call initva(set,obs,error)
  if (error) return
  call midabs(obs,fit,ier,liter)
  if (ier.ne.0) then
    error = .true.
    return
  endif
  obs%head%abs%nline = max(nline,1)
  !
  !
  call intoex(fit,fit%x)
  fit%up=sigbas**2
  fit%nfcnmx = 1000
  fit%epsi = 0.1d0 * fit%up
  fit%newmin = 0
  fit%itaur = 0
  fit%isw(1) = 0
  fit%isw(3) = 1
  fit%nfcn=1
  call fcn(fit%npar,fit%g,fit%amin,fit%u,1,obs)
  fit%vtest = 0.04d0
  !
  ! Simplex minimization
  ! Update obs%head%abs%nfit: used by proabs to compute rms
  if (.not.liter) then
    ier = 0
    call simplx(fit,fcn,ier)
    if (ier.ne.0) then
      error = .true.
      return
    endif
    par(1) = fit%u(1)
    obs%head%abs%nfit(1) = par(1)
    k=2
    do i=1,max(nline,1)
      par(k)=fit%u(k)
      par(k+1)=fit%u(k+1)
      par(k+2)=fit%u(k+2)*1.665109
      obs%head%abs%nfit(k:k+2) = par(k:k+2)
      k=k+3
    enddo
  endif
  !
  ! Gradient minimization
  call fcn(fit%npar,fit%g,fit%amin,fit%u,3,obs)
  fit%up = sigbas**2
  fit%epsi = 0.1d0 * fit%up
  fit%apsi = fit%epsi
  call hesse(fit,fcn)
  call migrad(fit,fcn,ier)
  if (ier.eq.1) then
    call hesse(fit,fcn)
    ier = 0
  elseif (ier.eq.3) then
    call class_message(seve%e,proc,'Solution not converged')
    ier = 0
  endif
  !
  ! Print results
  par(1) = fit%u(1)
  obs%head%abs%nfit(1) = par(1)
  k=2
  do i=1,max(nline,1)
    par(k)=fit%u(k)
    par(k+1)=fit%u(k+1)
    par(k+2)=fit%u(k+2)*1.665109
    obs%head%abs%nfit(k:k+2) = par(k:k+2)
    k=k+3
  enddo
  call fcn(fit%npar,fit%g,fit%amin,fit%u,3,obs)
  write(mess,1002) sigbas,sigrai
  call class_message(seve%i,proc,mess)
  !
  fit%up=sigbas**2
  do i=1,fit%nu
    l  = fit%lcorsp(i)
    if (l .eq. 0)  then
      fit%werr(i)=0.
    else
      if (fit%isw(2) .ge. 1)  then
        dx = sqrt(abs(fit%v(l,l)*fit%up))
        if (fit%lcode(i) .gt. 1) then
          al = fit%alim(i)
          ba = fit%blim(i) - al
          du1 = al + 0.5d0 *(sin(fit%x(l)+dx) +1.0d0) * ba - fit%u(i)
          du2 = al + 0.5d0 *(sin(fit%x(l)-dx) +1.0d0) * ba - fit%u(i)
          if (dx .gt. 1.0d0)  du1 = ba
          dx = 0.5d0 * (abs(du1) + abs(du2))
        endif
        fit%werr(i) = dx
      endif
    endif
  enddo
  !
  err(1)=fit%werr(1)
  do k=2,3*max(nline,1)+1
    err(k)=fit%werr(k)
    if (mod(k,3).eq.1) err(k)=err(k)*1.665109
  enddo
  !
  ! Copy results
  obs%head%abs%sigba = sigbas
  obs%head%abs%sigra = sigrai
  obs%head%abs%nfit(:) = 0.
  ! Don't forget continuum here
  obs%head%abs%nfit(1:1+nabspar*obs%head%abs%nline) = par(1:1+nabspar*obs%head%abs%nline)
  obs%head%abs%nerr(:) = 0.
  obs%head%abs%nerr(1:1+nabspar*obs%head%abs%nline) = err(1:1+nabspar*obs%head%abs%nline)
  obs%head%presec(class_sec_abs_id) = .true.
  !
  ! Print results
  write(mess,*) 'Number of calls: ',fit%nfcn
  call class_message(seve%i,proc,mess)
  call disabs(set,obs%head,0.,.false.,error)
  !
1002 format (' RMS of Residuals :  Base = ',1pg9.2,'  Line = ',1pg9.2)
end subroutine fitabs
!
subroutine fitshell(fcn,set,obs,checkbase,liter,signal,image,error)
  use gildas_def
  use gbl_constant
  use phys_const
  use gbl_message
  use fit_minuit
  use classfit_dependencies_interfaces
  use classfit_interfaces, except_this=>fitshell
  use class_types
  use gauss_parameter
  !---------------------------------------------------------------------
  ! @ private
  ! FIT\ support routine for command
  !   MINIMIZE
  ! Setup and starts a line minimisation using MINUIT and the structure
  ! of the SHELL lines
  !---------------------------------------------------------------------
  interface
    subroutine fcn(npar,g,f,x,iflag,obs)  ! Function to be mininized
    use class_types
    integer(kind=4),   intent(in)  :: npar
    real(kind=8),      intent(out) :: g(npar)
    real(kind=8),      intent(out) :: f
    real(kind=8),      intent(in)  :: x(npar)
    integer(kind=4),   intent(in)  :: iflag
    type(observation), intent(in)  :: obs
    end subroutine fcn
  end interface
  type(class_setup_t), intent(in)            :: set        !
  type(observation),   intent(inout), target :: obs        ! 'target' because subject to a locwrd during minimization
  logical,             intent(in)            :: checkbase  ! Check if baseline removed?
  logical,             intent(in)            :: liter      ! Logical of iterate fit
  real(kind=8),        intent(in)            :: signal     ! Signal band frequency
  real(kind=8),        intent(in)            :: image      ! Image band frequency
  logical,             intent(inout)         :: error      ! Error status
  ! Local
  character(len=*), parameter :: proc='FITSHELL'
  character(len=1) :: unit,unit_low
  integer(kind=4) :: i,k,l,oline,ier
  real(kind=8) :: dx,al,ba,du1,du2
  real(kind=8) :: f,fi,tt,d,ed
  character(len=message_length) :: mess
  type(fit_minuit_t) :: fit
  !
  call geunit(set,obs%head,unit,unit_low)
  if (unit.ne.'F') then
     call class_message(seve%e,proc,'Unit X must be Frequency')
     error = .true.
     return
  endif
  !
  ! Check that a baseline has been removed
  if (checkbase .and. .not.obs%head%presec(class_sec_bas_id)) then
     call class_message(seve%e,proc,'No baseline removed')
     error = .true.
     return
  endif
  !
  ! Added error Patch for Initialisation
  ngline = nline
  error  = .false.
  fit%maxext = ntot
  fit%maxint = nvar
  fit%verbose = .true.
  fit%owner = gpack_get_id('class',.true.,error)
  if (error)  return
  !
  ! Initialise values
  call initva(set,obs,error)
  if (error) return
  deltav = abs(obs%datax(obs%cimin+1)-obs%datax(obs%cimin))
  write(mess,1002) sigbas,sigrai
  call class_message(seve%i,proc,mess)
  call midshell(obs,fit,ier,liter)
  if (ier.ne.0) then
     error = .true.
     return
  endif
  obs%head%she%nline = max(ngline,1)
  !
  call intoex(fit,fit%x)
  fit%up = sigbas**2
  fit%nfcnmx  = 1000
  fit%epsi  = 0.1d0 * fit%up
  fit%newmin  = 0
  fit%itaur  = 0
  fit%isw(1)  = 0
  fit%isw(3)  = 1
  fit%nfcn = 1
  fit%vtest  = 0.04d0
  !
  ! Set at least ONE line
  oline   = nline
  if (nline.eq.0) nline=1
  call fcn(fit%npar,fit%g,fit%amin,fit%u,1,obs)
  !
  ! Simplex minimization
  if (.not.liter) then
     ier = 0
     call simplx(fit,fcn,ier)
     if (ier.ne.0) then
        nline = oline
        error = .true.
        return
     endif
     k=1
     ! Update %nfit: used by proshell to compute rms
     do i=1,nline
        par(k)=fit%u(k+4)*fit%u(1)
        par(k+1)=fit%u(k+5)+fit%u(2)
        par(k+2)=fit%u(k+6)*fit%u(3)
        par(k+3)=fit%u(k+7)*fit%u(4)
        obs%head%she%nfit(k:k+3) = par(k:k+3)
        k=k+4
     enddo
  endif
  !
  ! Gradient minimization
  call fcn(fit%npar,fit%g,fit%amin,fit%u,3,obs)
  write(mess,1002) sigbas,sigrai
  call class_message(seve%i,proc,mess)
  fit%up   = sigbas**2
  fit%epsi = 0.1d0 * fit%up
  fit%apsi = fit%epsi
  call hesse(fit,fcn)
  call migrad(fit,fcn,ier)
  if (ier.eq.1) then
     call hesse(fit,fcn)
     ier = 0
  elseif (ier.eq.3) then
     call class_message(seve%e,proc,'Solution not converged')
     ier = 0
  endif
  !
  ! Print results
  ! Update %nfit: used by proshell to compute rms
  k=1
  do i=1,nline
     par(k)=fit%u(k+4)*fit%u(1)
     par(k+1)=fit%u(k+5)+fit%u(2)
     par(k+2)=fit%u(k+6)*fit%u(3)
     par(k+3)=fit%u(k+7)*fit%u(4)
     obs%head%she%nfit(k:k+3) = par(k:k+3)
     k=k+4
  enddo
  call fcn(fit%npar,fit%g,fit%amin,fit%u,3,obs)
  write(mess,1002) sigbas,sigrai
  call class_message(seve%i,proc,mess)
  fit%up=sigbas**2
  !
  ! Calculate External Errors
  do i=1,fit%nu
     l  = fit%lcorsp(i)
     if (l .eq. 0)  then
        fit%werr(i)=0.
     else
        if (fit%isw(2) .ge. 1)  then
           dx = sqrt(abs(fit%v(l,l)*fit%up))
           if (fit%lcode(i) .gt. 1) then
              al = fit%alim(i)
              ba = fit%blim(i) - al
              du1 = al + 0.5d0 *(sin(fit%x(l)+dx) +1.0d0) * ba - fit%u(i)
              du2 = al + 0.5d0 *(sin(fit%x(l)-dx) +1.0d0) * ba - fit%u(i)
              if (dx .gt. 1.0d0)  du1 = ba
              dx = 0.5d0 * (abs(du1) + abs(du2))
           endif
           fit%werr(i) = dx
        endif
     endif
  enddo
  !
  k=1
  do i=1,nline
     tt = 0.5*par(k) / ( par(k+2) * (1.0+par(k+3)/3.0) )
     f = par(k+1)+signal
     fi=-par(k+1)+image
     d = par(k+2)*clight_kms/signal
     err(k) = fit%werr(k+4)
     if (i.eq.kt0) err(k) = fit%werr(1)
     err(k+1) = fit%werr(k+5)
     if (i.eq.kv0) err(k+1) = fit%werr(2)
     err(k+2) = fit%werr(k+6)
     if (i.eq.kd0) err(k+2) = fit%werr(3)
     err(k+3) = fit%werr(k+7)
     if (i.eq.kh0) err(k+3) = fit%werr(4)
     ed = err(k+2)*clight_kms/signal
     k=k+4
  enddo
  nline = oline
  !
  ! Copy results
  obs%head%she%sigba = sigbas
  obs%head%she%sigra = sigrai
  obs%head%she%nfit(:) = 0.
  obs%head%she%nfit(1:nshellpar*obs%head%she%nline) = par(1:nshellpar*obs%head%she%nline)
  obs%head%she%nerr(:) = 0.
  obs%head%she%nerr(1:nshellpar*obs%head%she%nline) = err(1:nshellpar*obs%head%she%nline)
  obs%head%presec(class_sec_she_id) = .true.
  !
  ! Print results
  write(mess,*) 'Number of calls: ',fit%nfcn
  call class_message(seve%i,proc,mess)
  call dishel(set,obs%head,0.,.false.,error)
  return
  !
1002 format (' RMS of Residuals :  Base = ',1pg9.2,'  Line = ',1pg9.2)
end subroutine fitshell
!
subroutine fitpoi(fcn,set,obs,checkbase,liter,error)
  use gildas_def
  use gbl_message
  use fit_minuit
  use classfit_dependencies_interfaces
  use classfit_interfaces, except_this=>fitpoi
  use class_setup_new
  use class_types
  use gauss_parameter
  !---------------------------------------------------------------------
  ! @ private
  ! FIT\ support routine for command
  !   MINIMIZE
  ! Setup and starts a GAUSS fit minimisation using MINUIT. Adapted
  ! from basic routine, by addition of a linear baseline removal.
  !---------------------------------------------------------------------
  interface
    subroutine fcn(npar,g,f,x,iflag,obs)  ! Function to be mininized
    use class_types
    integer(kind=4),   intent(in)  :: npar
    real(kind=8),      intent(out) :: g(npar)
    real(kind=8),      intent(out) :: f
    real(kind=8),      intent(in)  :: x(npar)
    integer(kind=4),   intent(in)  :: iflag
    type(observation), intent(in)  :: obs
    end subroutine fcn
  end interface
  type(class_setup_t), intent(in)            :: set        !
  type(observation),   intent(inout), target :: obs        ! 'target' because subject to a locwrd during minimization
  logical,             intent(in)            :: checkbase  ! Check if baseline removed?
  logical,             intent(in)            :: liter      ! Logical of iterate fit
  logical,             intent(inout)         :: error      ! Error status
  ! Local
  character(len=*), parameter :: proc='FITPOI'
  integer(kind=4) :: i,k,l,k1,k2, ifatal, ier, nxy
  real(kind=8) :: dx, al, ba, du1, du2
  real(kind=4) :: offset
  character(len=message_length) :: chain
  type(fit_minuit_t) :: fit
  !
  ! Check that a Baseline has been removed. NB: the default is not
  ! to check this because the model also fits a linear background
  if (checkbase .and. .not.obs%head%presec(class_sec_bas_id)) then
     call class_message(seve%e,proc,'No baseline removed')
     error = .true.
     return
  endif
  !
  ! Added error Patch for Initialisation
  ngline = nline
  error = .false.
  fit%maxext=ntot
  fit%maxint=nvar
  fit%verbose = .true.
  fit%owner = gpack_get_id('class',.true.,error)
  if (error)  return
  !
  ! Initialise values
  nxy = obs%cimax-obs%cimin+1
  if (mxcan.lt.nxy) then
     if (allocated(wfit)) deallocate(wfit)
  endif
  if (.not.allocated(wfit)) then
     mxcan = nxy
     allocate(wfit(mxcan),stat=ier)
  endif
  !
  ! Compute deltav (Assume x array is sorted)
  ! Note: this solution will NOT work properly in special
  ! cases (e.g. everal sub-bands with big spaces)
  if (obs%head%presec(class_sec_xcoo_id)) then
     deltav = maxval(abs(obs%datax(obs%cimin+1:obs%cimax)-obs%datax(obs%cimin:obs%cimax-1)))
  else
     deltav = abs(obs%datax(obs%cimin+1)-obs%datax(obs%cimin))
  endif
  !
  call inipoi (sigbas,sigrai,obs%datax(obs%cimin:),obs%spectre(obs%cimin:),nxy,par,wfit,obs%cbad,error)
  if (error) return
  !
  offset = par(1)
  par(1) = 0.0
  call midpoi(set,obs,fit,ifatal,liter)
  if (ifatal.ne.0) then
     ! Should restore the original spectrum...
     error = .true.
     return
  endif
  call intoex(fit,fit%x)
  fit%up = sigbas**2
  fit%nfcnmx  = 5000
  fit%epsi  = 0.1d0 * fit%up
  fit%newmin  = 0
  fit%itaur  = 0
  fit%isw(1)  = 0
  fit%isw(3)  = 1
  fit%nfcn = 1
  fit%vtest  = 0.04
  call fcn(fit%npar,fit%g,fit%amin,fit%u,1,obs)
  !
  ! Simplex Minimization
  if (.not.liter) then
     ier = 0
     call simplx(fit,fcn,ier)
     if (ier.ne.0) then
        write(chain,*) 'Spectre ',obs%spectre(obs%cimin)
        call class_message(seve%e,proc,chain)
        ! Should restore the original spectrum...
        error = .true.
        return
     endif
     par(1) = fit%u(4)
     par(2) = fit%u(5)
     ! Update %nfit: used by propoint to compute rms
     obs%head%poi%nfit(1:2) = par(1:2)
     k=3
     do i=1,max(nline,1)
        par(k)  =fit%u(k+3)*fit%u(1)*1.7724538
        par(k+1)=fit%u(k+4)+fit%u(2)
        par(k+2)=fit%u(k+5)*1.665109*fit%u(3)
        obs%head%poi%nfit(k:k+2) = par(k:k+2)
        k=k+3
     enddo
  endif
  !
  ! Gradient Minimization
  call intoex(fit,fit%x)
  call fcn(fit%npar,fit%g,fit%amin,fit%u,3,obs)
  write(chain,1002) sigbas,sigrai
  call class_message(seve%i,proc,chain)
  fit%up = sigbas**2
  fit%epsi = 0.1d0 * fit%up
  fit%apsi = fit%epsi
  call hesse(fit,fcn)
  ier = 0
  call migrad(fit,fcn,ier)
  if (ier.eq.1) then
     call hesse(fit,fcn)
     ier = 0
  elseif (ier.eq.3) then
     ier = 0
     call migrad(fit,fcn,ier)
     if (ier.eq.1) call hesse(fit,fcn)
     ier = 0
  endif
  !
  ! Print Results
  ! Update %nfit: used by propoint to compute rms
  k=3
  par(1) = fit%u(4)
  par(2) = fit%u(5)
  obs%head%poi%nfit(1:2) = par(1:2)
  do i=1,max(nline,1)
     par(k)  =fit%u(k+3)*fit%u(1)*1.7724538
     par(k+1)=fit%u(k+4)+fit%u(2)
     par(k+2)=fit%u(k+5)*fit%u(3)*1.665109
     obs%head%poi%nfit(k:k+2) = par(k:k+2)
     k=k+3
  enddo
  call intoex(fit,fit%x)
  call fcn(fit%npar,fit%g,fit%amin,fit%u,3,obs)
  write(chain,1002) sigbas,sigrai
  call class_message(seve%i,proc,chain)
  fit%up = sigbas**2
  !
  ! Calculate External Errors
  where (obs%spectre(obs%cimin:obs%cimax).ne.obs%cbad) &
       & obs%spectre(obs%cimin:obs%cimax)  = obs%spectre(obs%cimin:obs%cimax)+offset
  !
  par(1) = par(1)+offset
  do i=1,fit%nu
     l  = fit%lcorsp(i)
     if (l .eq. 0)  then
        fit%werr(i)=0.
     else
        if (fit%isw(2) .ge. 1)  then
           dx = sqrt(abs(fit%v(l,l)*fit%up))
           if (fit%lcode(i) .gt. 1) then
              al = fit%alim(i)
              ba = fit%blim(i) - al
              du1 = al + 0.5d0*(sin(fit%x(l)+dx) +1.0d0)*ba - fit%u(i)
              du2 = al + 0.5d0*(sin(fit%x(l)-dx) +1.0d0)*ba - fit%u(i)
              if (dx .gt. 1.0d0)  du1 = ba
              dx = 0.5d0*(abs(du1) + abs(du2))
           endif
           fit%werr(i) = dx
        endif
     endif
  enddo
  !
  k=3                          ! Skip the baseline parameters
  do i=1,max(nline,1)
     k1=k+1
     k2=k+2
     err(k)=fit%werr(k+3)
     if (i.eq.kt0) err(k)=fit%werr(1)
     err(k1)=fit%werr(k1+3)
     if (i.eq.kv0) err(k1)=fit%werr(2)
     err(k2)=fit%werr(k2+3)
     if (i.eq.kd0) err(k2)=fit%werr(3)
     k=k+3
  enddo
  !
  ! Copy results
  obs%head%poi%nline = max(ngline,1)
  obs%head%poi%sigba = sigbas
  obs%head%poi%sigra = sigrai
  call r4tor4(par,obs%head%poi%nfit,2+3*ngline)  ! fit results
  call r4tor4(err,obs%head%poi%nerr,2+3*ngline)  ! fit errors
  obs%head%presec(class_sec_poi_id) = .true.
  !
  ! Display the results on the screen only
  call dispoi(set,obs%head,class_setup_get_fangle(),0.,.false.,error)
  return
  !
  !
1002 format (' RMS of Residuals :  Base = ',1pg9.2,'  Line = ',1pg9.2)
end subroutine fitpoi
!
subroutine initva(set,obs,error)
  use gildas_def
  use classfit_dependencies_interfaces
  use class_types
  use gauss_parameter
  !---------------------------------------------------------------------
  ! @ private
  ! ANALYSE Internal routine
  !  Initialize the parameters for a gaussian fit using Minuit
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(in)    :: obs
  logical,             intent(inout) :: error
  ! Local
  integer(kind=4) :: i,kbas,krai,iw,ier
  integer(kind=4) :: i1,i2
  real(kind=4) :: v1,v2
  real(kind=4) :: aega,asup,ainf! ,sigbas,sigrai
  !
  if (mxcan.lt.obs%cnchan) then
     if (allocated(wfit)) deallocate(wfit)
  endif
  if (.not.allocated(wfit)) then
     mxcan = obs%cnchan
     allocate(wfit(mxcan),stat=ier)
     if (failed_allocate('INITVA','WFIT',ier,error)) return
  endif
  !
  i1 = obs%cimin
  i2 = obs%cimax
  ! wfit(i) = 1 if channel is good AND not in masked velocity window
  ! i in [i1:i2]
  wfit = 0.
  where (obs%spectre(i1:i2).ne.obs%head%spe%bad) wfit(i1:i2) = 1
  if (set%nmask.gt.0) then
     do iw = 1, set%nmask
        v1 = set%mask1(iw)
        v2 = set%mask2(iw)
        where((obs%datax(i1:i2)-v1)*(obs%datax(i1:i2)-v2).le.0) wfit(i1:i2)=0
     enddo
  endif
  !
  ! Compute the Sigma
  kbas   = 0
  krai   = 0
  sigbas = 0.
  sigrai = 0.
  aega   = obs%spectre(i1)*wfit(i1)
  asup   = aega
  do i=obs%cimin+1,obs%cimax
     ainf=aega
     aega=asup
     asup=obs%spectre(i)*wfit(i)
     ! Check weather in/out the line
     ! In the line: at least 2 consecutive points have the same sign
     if ( ainf*aega.lt.0. .and. aega*asup.lt.0. ) then
        sigbas=sigbas+aega**2
        kbas=kbas+1
     else
        sigrai=sigrai+aega**2
        krai=krai+wfit(i-1)
     endif
  enddo
  !
  ! Try to avoid zero sigmas
  if (kbas.ne.0) sigbas=sqrt(sigbas/kbas)
  if (krai.ne.0) then
     sigrai=sqrt(sigrai/krai)
     if (sigbas.eq.0.) sigbas=sigrai
  else
     sigrai=sigbas
  endif
end subroutine initva
!
subroutine inipoi(sigbas,sigrai,rdatax,rdatay,n,par,wfit,cbad,error)
  use gbl_message
  use classfit_interfaces, except_this=>inipoi
  use class_parameter
  !---------------------------------------------------------------------
  ! @ private
  ! Initializes the parameters for a gaussian fit using Minuit
  !---------------------------------------------------------------------
  real(kind=4),          intent(out)    :: sigbas     ! Sigma on baseline
  real(kind=4),          intent(out)    :: sigrai     ! Sigma on area under line
  integer(kind=4),       intent(in)     :: n          ! Size of data array
  real(kind=xdata_kind), intent(in)     :: rdatax(n)  ! X values
  real(kind=4),          intent(inout)  :: rdatay(n)  ! Y-values
  real(kind=4),          intent(out)    :: par(2)     ! Input guesses (offset and slope)
  integer(kind=4),       intent(out)    :: wfit(n)    ! Weight (0 if bad, 1 if good)
  real(kind=4),          intent(in)     :: cbad       ! Blanking value
  logical,               intent(inout)  :: error      ! Error flag (too few data points)
  ! Local
  character(len=*), parameter :: proc='INIPOI'
  real(kind=4) :: aega,asup,ainf,xleft,yleft,xright,yright
  integer(kind=4) :: i,j,kbas,krai,nleft,nright
  integer(kind=4), parameter :: ms=10
  logical :: start
  !
  ! Compute starting values
  !
  ! 1st order baseline input parameters are guessed from the ms first and
  ! ms last points of the continuum drift
  if (n.lt.2*ms) then
     call class_message(seve%e,proc,'Too few data points')
     error = .true.
     return
  endif
  !
  ! Find the good data points in the ms first and ms last points
  xright = 0
  yright = 0
  nright = 0
  xleft = 0
  yleft = 0
  nleft = 0
  do i=1, ms
     if (rdatay(i).ne.cbad) then
        xleft = xleft+rdatax(i)
        yleft = yleft+rdatay(i)
        nleft = nleft+1
     endif
     j = n-i+1
     if (rdatay(j).ne.cbad) then
        xright = xright+ rdatax(j)
        yright = yright+ rdatay(j)
        nright = nright+ 1
     endif
  enddo
  !
  ! Analyse results
  if (nleft.eq.0 .and. nright.eq.0) then
     ! No good data point in the ms first and ms last points
     call class_message(seve%e,proc,'No baseline data')
     error = .true.
     return
  elseif (nleft.eq.0) then
     ! Only good data points in the ms last points => 0 order baseline
     par(1) = yright/nright
     par(2) = 0.0
  elseif (nright.eq.0) then
     ! Only good data points in the ms first points => 0 order baseline
     par(1) = yleft/nleft
     par(2) = 0.0
  else
     ! Some good data points at both ends => 1st order baseline
     !
     ! Baseline offset and slope is computed from the averages of good data
     ! points at both ends
     xleft = xleft/nleft
     yleft = yleft/nleft
     xright = xright/nright
     yright = yright/nright
     if (xright.eq.xleft) then
        ! Problem => Fall back to 0 order baseline
        par(1) = 0.5*(yleft+yright)
        par(2) = 0.0
     else
        ! OK => Compute offset and slope
        par(1) = (xright*yleft - xleft*yright) / (xright - xleft)
        par(2) = (yright-yleft)/(xright-xleft)
     endif
  endif
  !
  ! Compute the Sigma
  kbas = 0
  krai = 0
  sigbas = 0.
  sigrai = 0.
  start = .true.
  do i=1,n
     if (rdatay(i).ne.cbad) then
        wfit(i) = 1
        if (start) then
          aega = rdatay(i) - (par(1)+par(2)*rdatax(i))
          asup = aega
          start = .false.
        else
          ainf = aega
          aega = asup
          asup = rdatay(i) - (par(1)+par(2)*rdatax(i))
          if ( ainf*aega.lt.0. .and. aega*asup.lt.0. ) then
            sigbas = sigbas+aega**2
            kbas = kbas+1
          else
            sigrai = sigrai+aega**2
            krai = krai+1
          endif
        endif
     else
        wfit(i) = 0
     endif
  enddo
  !
  ! Try to avoid zero sigmas
  if (kbas.ne.0) sigbas = sqrt(sigbas/kbas)
  if (krai.ne.0) then
     sigrai = sqrt(sigrai/krai)
     if (sigbas.eq.0.) sigbas = sigrai
  else
     sigrai = sigbas
  endif
  i = 1
end subroutine inipoi
