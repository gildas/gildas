subroutine fitlines(set,line,obs,error)
  use gbl_message
  use classfit_interfaces, except_this=>fitlines
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! Support routine for command
  !  FIT\LINES [N] [/NOCURSOR] [/INPUT File_Name] [/SHOW]
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  character(len=*),    intent(in)    :: line   ! Input command line
  type(observation),   intent(in)    :: obs    !
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='LINES'
  !
  select case (set%method)
  case ('GAUSS')
    call guegauss(line,obs,set,error)
  case ('NH3','HFS')
    call guenh3(line,obs,error)
  case ('ABSORPTION')
    call gueabs(line,obs,error)
  case ('SHELL')
    call gueshell(line,obs,error)
  case default
    call class_message(seve%e,rname,'Not implemented for '//trim(set%method)//' data')
    error = .true.
    return
  end select
  !
end subroutine fitlines
!
subroutine guegauss(line,obs,set,error)
  use gildas_def
  use gbl_message
  use gbl_constant
  use classfit_dependencies_interfaces
  use classfit_interfaces, except_this=>guegauss
  use class_setup_new
  use class_types
  use gauss_parameter
  !---------------------------------------------------------------------
  ! @ private
  ! LINE [Nline  [" Line1 Parameters"   ... ["LineN parameters"] ] ]
  !   [/INPUT Filename]
  !   [/NOCURSOR]
  !
  ! Nline is an optional argument
  !   If not present, the current number of lines is used
  !   If present, it indicates the number of lines to be fitted
  !   It can also be set to * or AUTOMATIC to use the current number of
  !     lines in the current spectrum
  !
  ! Define starting values for gaussian fits. These values are stored
  ! in array SPAR and are associated with an integer code defining the
  ! parameter class. The values can be estimated using the cursor if it
  ! is active. One then need to bracket the line, and the program gets
  ! its guesses by evaluating the moment of the spectrum in the
  ! delimited range. Codes cannot be changed in this mode. Conventions
  ! for these codes are:
  !  code = 0  variable parameter
  !  code = 1  fixed parameter
  !  code = 2  variable reference parameter
  !  code = 3  dependent parameter offset or ratio
  !  code = 4  fixed reference parameter
  !---------------------------------------------------------------------
  character(len=*),    intent(in)    :: line   ! Input command line
  type(observation),   intent(in)    :: obs    !
  type(class_setup_t), intent(in)    :: set    !
  logical,             intent(inout) :: error  ! Error flag
  ! Local
  character(len=*), parameter :: proc='LINES'
  character(len=*), parameter :: auto='AUTOMATIC'
  character(len=9) :: argum,prompt
  character(len=filename_length) :: filnam,name
  real(kind=8) :: dbl
  integer(kind=4) :: nl,jtmp,ier,i,j,ifatal,ios
  logical :: use_file,use_cursor,use_command
  character(len=80) :: answer
  integer(kind=4) :: lpar,npar
  character(len=20) :: cpar
  character(len=12) :: from
  character(len=message_length) :: mess
  !
  use_file = sic_present(2,0)
  use_cursor = .not.sic_present(1,0) .and. .not.use_file
  use_command = .false.
  if (.not.use_file) filnam = "interactive input"
  !
  ! Read from an input file
  if (use_file) then
    call sic_ch (line,2,1,name,nl,.true.,error)
    if (error) return
    ier = sic_getlun(jtmp)
    if (ier.ne.1) then
      call class_message(seve%e,proc,'No logical unit left')
      error = .true.
      return
    endif
    call sic_parse_file(name,' ','.line',filnam)
    ier = sic_open(jtmp,filnam,'OLD',.true.)
    if (ier.ne.0) then
      call class_message(seve%e,proc,'Error opening guess file '//filnam)
      call putios('        ',ier)
      error = .true.
      call sic_frelun(jtmp)
      return
    endif
    read (jtmp,*,err=97,iostat=ios) nline
    if (ios.ne.0) goto 97
    if (nline.eq.0) then
      close (jtmp)             ! RL 22-jun-89
      call sic_frelun(jtmp)    ! RL 22-jun-89
      return
    elseif (nline.lt.0 .or. nline.gt.mxline) then
      write(mess,'(a,i0,a)')  'Invalid number of lines (max. is ',mxline,' lines)'
      call class_message(seve%e,proc,mess)
      close (jtmp)
      call sic_frelun(jtmp)
      error = .true.
      return
    endif
  endif
  !
  ! Get number of lines
  if (sic_present(0,1)) then
    ! Keyword "AUTOMATIC" ?
    call sic_ke (line,0,1,argum,nl,.true.,error)
    if (error)  return
    nl = min(nl,9)
    if (argum(1:nl).eq.auto(1:nl)) then
      nline = -1
    else
      ! Non-automatic: get the exact number
      call sic_i4(line,0,1,nl,.true.,error)
      if (error) return
      if (use_file .and. nl.ne.nline) then
        call class_message(seve%w,proc,'Number of lines differ in Input file and Command line')
      endif
      nline = nl
    endif
  endif
  if (nline.le.0)  return  ! Automatic
  if (nline.gt.mxline) then
    write(mess,'(A,I0)')  'Number of lines is limited to ',mxline
    call class_message(seve%e,proc,mess)
    error = .true.
    return
  endif
  !
  ! Check if input parameters are in Command line
  if ((sic_narg(0)-1).eq.nline) then
    use_command = .true.
    use_cursor = .false.
  endif
  !
  if (use_cursor) then
    if (.not.gtg_curs()) then
      call class_message(seve%e,proc,'No cursor available')
      error = .true.
      return
    endif
    write (6,*) 'Using the cursor, type / to keep last values '
    write (6,*) '      any other key to set a line boundary'
  endif
  !
  ! Convert angles to current units for display
  if (set%kind.eq.kind_cont .and. .not.use_cursor) then
    do i =1,nline
      spar(3*i-1) = spar(3*i-1)*class_setup_get_fangle()
      spar(3*i) = spar(3*i)*class_setup_get_fangle()
    enddo
  endif
  !
  ! Get starting values
  do i=1,nline
    j=3*i
    if (use_cursor) then
      write (6,101) i
      call cursor(obs,spar(j-2),0,error)
      if (error) then
        call class_message(seve%e,proc,'Error while reading input parameter')
        return
      endif
      kt(i) = 0
      kv(i) = 0
      kd(i) = 0
    else
      if (use_file) then
        read(jtmp,*,err=97,iostat=ios)  &
        kt(i),spar(j-2),kv(i),spar(j-1),kd(i),spar(j)
        if (ios.ne.0) goto 97
      else 
        if (use_command) then
          call sic_ch(line,0,i+1,answer,nl,.true.,error)
        else
          write(6,102) i,kt(i),spar(j-2),kv(i),spar(j-1),kd(i),spar(j)
          write (prompt,103) i
          call sic_wprn (prompt,answer,nl)
        endif
        if (nl.eq.0) then
          cycle
        else
          call sic_blanc(answer(1:nl),nl)
          npar = 1
          do while (npar.lt.nl)
            call sic_next(answer(npar:nl),cpar,lpar,npar)
            call sic_math_dble(cpar(1:lpar),lpar,dbl,error)
            if (error) goto 97
          enddo
          error = .true.
          read(answer(1:nl),*,end=100,iostat=ios)  &
          kt(i),spar(j-2),kv(i),spar(j-1),kd(i),spar(j)
          if (ios.ne.0) goto 97
          error = .false.
        endif
      endif
    endif
  enddo
100 continue
  if (error) goto 97
  !
  ! Copy first guesses to internal variables
  ! and convert angles to internal units
  if (set%kind.eq.kind_cont .and. .not.use_cursor) then
    do i =1,nline
      spar(3*i-1) = spar(3*i-1)/class_setup_get_fangle()
      if (mod(kd(i),2).eq.0) spar(3*i) = spar(3*i)/class_setup_get_fangle()
    enddo
  endif
  !
  if (use_file) then
    close (jtmp)
    call sic_frelun(jtmp)
  endif
  ! Check flags'consistency
  call check_line(kt,nline,kt0,1,ifatal)
  call check_line(kv,nline,kv0,2,ier)
  ifatal=ifatal+ier
  call check_line(kd,nline,kd0,3,ier)
  ifatal=ifatal+ier
  if (kt0.ne.0 .and. kd0.ne.0 .and. kt0.ne.kd0) ifatal=ifatal+1
  if (ifatal.ne.0) then
    write(6,104) ifatal
    error = .true.
    return
  endif
  !
  if (sic_present(3,0)) then  ! /SHOW
    ! User feeback
    if (use_cursor) then
      from = 'cursor'
    elseif (use_file) then
      from = 'file'
    elseif (use_command) then
      from = 'command line'
    else
      from = 'prompt'
    endif
    call class_message(seve%i,proc,  &
      'Input values read successfully from '//trim(from)//' (method GAUSS):')
    do i=1,nline
      j=3*i
      write(mess,102) i,kt(i),spar(j-2),kv(i),spar(j-1),kd(i),spar(j)
      call class_message(seve%i,proc,mess)
    enddo
  endif
  !
  return
  !
97 continue
  call class_message(seve%e,proc,'Error reading guesses from '//filnam)
  close (jtmp)
  call sic_frelun(jtmp)
  error = .true.
  return
  !
104 format(' * * * ',i3,' Fatal Errors on Parameters. Try Again ')
103 format('Line ',i0,' :')
102 format(' Line ',i0,' :',3(2x,i1,2x,1pg11.4))
101 format('      Setting line ',i0)
end subroutine guegauss
!
subroutine guenh3(line,obs,error)
  use gildas_def
  use gbl_message
  use classfit_dependencies_interfaces
  use classfit_interfaces, except_this=>guenh3
  use class_types
  use gauss_parameter
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !   LINES [N]
  ! 1       [/NOCURSOR]
  ! 2       [/INPUT File_Name]
  !
  !  Get Starting values for Fit on NH3 profiles
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: line   ! Input command line
  type(observation), intent(in)    :: obs    !
  logical,           intent(inout) :: error  ! Error flag
  ! Local
  character(len=*), parameter :: proc='LINES'
  character(len=*), parameter :: auto='AUTOMATIC'
  real(kind=8) ::  dbl
  logical :: use_file,use_cursor,use_command
  character(len=filename_length) :: filnam,name
  integer(kind=4) :: ier,jtmp,j,i,nl,ios
  character(len=80) :: answer
  character(len=9) :: argum,prompt
  integer(kind=4) :: lpar,npar
  character(len=20) :: cpar
  character(len=12) :: from
  character(len=message_length) :: mess
  !
  use_file = sic_present(2,0)
  use_cursor = .not.sic_present(1,0) .and. .not.use_file
  use_command = .false.
  if (.not.use_file) filnam = "interactive input"
  !
  ! Read from an input file
  if (use_file) then
    call sic_ch (line,2,1,name,nl,.true.,error)
    if (error) return
    ier = sic_getlun(jtmp)
    if (ier.ne.1) then
      call class_message(seve%e,proc,'No logical unit left')
      error = .true.
      return
    endif
    call sic_parse_file(name,' ','.line',filnam)
    ier = sic_open(jtmp,filnam,'OLD',.true.)
    if (ier.ne.0) then
      call class_message(seve%e,proc,'Error opening guess file '//filnam)
      call putios('        ',ier)
      error = .true.
      call sic_frelun(jtmp)
      return
    endif
    read (jtmp,*,err=97,iostat=ios) nline
    if (ios.ne.0) goto 97
    if (nline.lt.0 .or. nline.gt.mxline) then
      call class_message(seve%e,proc,'Invalid number of lines')
      error = .true.
    else
      do i=1,nline
        j=4*i
        read(jtmp,*,err=97,iostat=ios) kt(i),spar(j-3),kv(i),spar(j-2),kd(i),spar(j-1),ko(i),spar(j)
        if (ios.ne.0) goto 97
      enddo
    endif
    close (jtmp)
    call sic_frelun(jtmp)
    if (.not.error) goto 100
  endif
  !
  ! Get number of lines
  if (sic_present(0,1)) then
    ! Keyword "AUTOMATIC" ?
    call sic_ke (line,0,1,argum,nl,.true.,error)
    if (error)  return
    nl = min(nl,9)
    if (argum(1:nl).eq.auto(1:nl)) then
      nline = -1
    else
      ! Non-automatic: get the exact number
      call sic_i4(line,0,1,nl,.true.,error)
      if (error) return
      if (use_file .and. nl.ne.nline) then
        call class_message(seve%w,proc,'Number of lines differ in Input file and Command line')
      endif
      nline = nl
    endif
  endif
  if (nline.le.0)  return  ! Automatic
  if (nline.gt.mxline) then
    write(mess,'(A,I0)')  'Number of lines is limited to ',mxline
    call class_message(seve%e,proc,mess)
    error = .true.
    return
  endif
  !
  ! Check if input parameters are in Command line
  if ((sic_narg(0)-1).eq.nline) then
    use_command = .true.
    use_cursor = .false.
  endif
  !
  if (use_cursor) then
    if (.not.gtg_curs()) then
      call class_message(seve%e,proc,'No cursor available')
      error = .true.
      return
    endif
    write (6,*) 'Using the cursor, type / to keep last values '
    write (6,*) '      any other key to set a line boundary'
  endif
  !
  ! Interactive definition
  !
  ! Get starting values
  do i=1,nline
    j=4*i
    if (use_cursor) then
      write (6,9) i
      call cursor(obs,spar(1+4*(i-1)),0,error)
      if (error) then
        call class_message(seve%e,proc,'Error while reading input parameter')
        return
      endif
      if (spar(4+4*(i-1)).eq.0.) spar(4+4*(i-1)) = 1.
      kt(i) = 0
      kv(i) = 0
      kd(i) = 0
      ko(i) = 0
    else
      if (use_file) then
        read(jtmp,*,err=97,iostat=ios)  &
        kt(i),spar(j-2),kv(i),spar(j-1),kd(i),spar(j)
        if (ios.ne.0) goto 97
      else 
        if (use_command) then
          call sic_ch(line,0,i+1,answer,nl,.true.,error)
        else
          write(6,5) i,&
                kt(i),spar(1+4*(i-1)),&
                kv(i),spar(2+4*(i-1)),&
                kd(i),spar(3+4*(i-1)),&
                ko(i),spar(4+4*(i-1))
          write (prompt,103) i
          call sic_wprn (prompt,answer,nl)
        endif
        if (nl.eq.0) then
          cycle
        else
          call sic_blanc(answer(1:nl),nl)
          npar = 1
          do while (npar.lt.nl)
            call sic_next(answer(npar:nl),cpar,lpar,npar)
            call sic_math_dble(cpar(1:lpar),lpar,dbl,error)
            if (error) goto 97
          enddo
          error = .true.
          read(answer(1:nl),*,end=100,iostat=ios) &
               kt(i),spar(1+4*(i-1)),  &
               kv(i),spar(2+4*(i-1)),  &
               kd(i),spar(3+4*(i-1)),  &
               ko(i),spar(4+4*(i-1))
          if (ios.ne.0) goto 97
          error = .false.
        endif
      endif
    endif
  enddo
  !
  ! Check Flags
100 continue
  if (error) goto 97
  do i=1,mxline
    kt(i)=mod(kt(i),2)
    kv(i)=mod(kv(i),2)
    kd(i)=mod(kd(i),2)
    ko(i)=mod(ko(i),2)
  enddo
  !
  if (sic_present(3,0)) then  ! /SHOW
    ! User feeback
    if (use_cursor) then
      from = 'cursor'
    elseif (use_file) then
      from = 'file'
    elseif (use_command) then
      from = 'command line'
    else
      from = 'prompt'
    endif
    call class_message(seve%i,proc,  &
      'Input values read successfully from '//trim(from)//' (method NH3/HFS):')
    do i=1,nline
      write(mess,5) i,                  &
                kt(i),spar(1+4*(i-1)),  &
                kv(i),spar(2+4*(i-1)),  &
                kd(i),spar(3+4*(i-1)),  &
                ko(i),spar(4+4*(i-1))
      call class_message(seve%i,proc,mess)
    enddo
  endif
  !
  return
  !
103 format('Line ',i0,' :')
5 format(' Line ',i0,' :',4(2x,i1,2x,1pg11.4))
9 format('      Setting line ',i0)
  !
97 continue
  call class_message(seve%e,proc,'Error reading guesses from '//filnam)
  close (jtmp)
  call sic_frelun(jtmp)
  error = .true.
  return
end subroutine guenh3
!
subroutine gueabs(line,obs,error)
  use gildas_def
  use gbl_constant
  use gbl_message
  use classfit_dependencies_interfaces
  use classfit_interfaces, except_this=>gueabs
  use class_types
  use gauss_parameter
  !---------------------------------------------------------------------
  ! @ private
  !  Define starting values for absorption fits. These values are stored
  ! in array SPAR and are associated with an integer code defining the
  ! parameter class. The values can be estimated using the cursor if it
  ! is active. One then need to bracket the line, and the program gets
  ! its guesses by evaluating the moment of the spectrum in the
  ! delimited range. Codes cannot be changed in this mode. Conventions
  ! for these codes are:
  !  code = 0  variable parameter
  !  code = 1  fixed parameter
  !  code = 2  variable reference parameter
  !  code = 3  dependent parameter offset or ratio
  !  code = 4  fixed reference parameter
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: line   ! Input command line
  type(observation), intent(in)    :: obs    !
  logical,           intent(inout) :: error  ! Error flag
  ! Local
  character(len=*), parameter :: proc='LINES'
  character(len=*), parameter :: auto='AUTOMATIC'
  character(len=9) :: argum,prompt
  character(len=filename_length) :: filnam
  character(len=message_length) :: mess
  character(len=80) :: answer,chain
  real(kind=8) :: dbl
  integer(kind=4) :: nl,jtmp,ier,i,j,ifatal,ios
  logical :: use_file,use_cursor,use_command
  integer(kind=4) :: lpar,npar
  character(len=20) :: cpar
  character(len=12) :: from
  !
  use_file = sic_present(2,0)
  use_cursor = .not.sic_present(1,0).and..not.use_file
  if (.not.use_file) filnam = "interactive input"
  !
  ! Read from an input file
  if (use_file) then
     call sic_ch (line,2,1,chain,nl,.true.,error)
     if (error) return
     ier = sic_getlun(jtmp)
     if (ier.ne.1) then
        call class_message(seve%e,proc,'No logical unit left')
        error = .true.
        return
     endif
     call sic_parse_file(chain,' ','.line',filnam)
     ier = sic_open(jtmp,filnam,'OLD',.true.)
     if (ier.ne.0) then
        mess = 'Error opening guess file '//filnam
        call class_message(seve%e,proc,mess)
        call putios('        ',ier)
        call sic_frelun(jtmp)
        error = .true.
        return
     endif
     read (jtmp,*,err=97,iostat=ios) nline
     if (ios.ne.0) goto 97
     if (nline.eq.0) then
        close (jtmp)
        call sic_frelun(jtmp)
        return
     elseif (nline.lt.0 .or. nline.gt.mxline) then
        write(mess,'(a,i0,a)') 'Invalid number of lines (max. ',mxline,')'
        call class_message(seve%e,proc,mess)
        close (jtmp)
        call sic_frelun(jtmp)
        error = .true.
        return
     endif
  endif
  !
  ! Get number of lines
  if (sic_present(0,1)) then
    ! Keyword "AUTOMATIC" ?
    call sic_ke (line,0,1,argum,nl,.true.,error)
    if (error) return
    nl = min(nl,9)
    if (argum(1:nl).eq.auto(1:nl)) then
      nline = -1
    else
      ! Non-automatic: get the exact number
      call sic_i4 (line,0,1,nl,.false.,error)
      if (error) return
      nline = nl
    endif
  endif
  if (nline.le.0)  return  ! Automatic
  if (nline.gt.mxline) then
    write(mess,'(A,I0)')  'Number of lines is limited to ',mxline
    call class_message(seve%e,proc,mess)
    error = .true.
    return
  endif
  !
  ! Check if input parameters are in Command line
  if ((sic_narg(0)-1).eq.nline) then
    use_command = .true.
    use_cursor = .false.
    call class_message(seve%e,proc,  &
      'Reading inputs from command line is not implemented for method ABSORPTION')
    goto 97
  endif
  !
  if (use_cursor) then
     if (.not.gtg_curs()) then
       call class_message(seve%e,proc,'No cursor available')
       error = .true.
       return
     endif
     write (6,*) 'Using the cursor, type / to keep last values '
     write (6,*) '      any other key to set a line boundary'
  endif
  !
  ! Get starting values
  spar(1) = 1.0
  do i=1,nline
     j=1+3*i
     if (use_cursor) then
        write (6,101) i
        call cursor(obs,spar(j-2),0,error)
        kt(i) = 0
        kv(i) = 0
        kd(i) = 0
     else
        if (use_file) then
           read(jtmp,*,err=97,iostat=ios)  &
             kt(i),spar(j-2),kv(i),spar(j-1),kd(i),spar(j)
           if (ios.ne.0) goto 97
        else
           write(6,102) i,kt(i),spar(j-2),kv(i),spar(j-1),kd(i),spar(j)
           write (prompt,103) i
           call sic_wprn (prompt,answer,nl)
           if (nl.eq.0) then
              cycle
           else
              call sic_blanc(answer(1:nl),nl)
              npar = 1
              do while (npar.lt.nl)
                 call sic_next(answer(npar:nl),cpar,lpar,npar)
                 call sic_math_dble(cpar,lpar,dbl,error)
                 if (error) goto 97
              enddo
              error = .true.
              read(answer(1:nl),*,end=100,iostat=ios)  &
                kt(i),spar(j-2),kv(i),spar(j-1),kd(i),spar(j)
              if (ios.ne.0) goto 97
              error = .false.
           endif
        endif
     endif
  enddo
100 continue
  if (error) goto 97
  !
  if (use_file) then
     close (jtmp)
     call sic_frelun(jtmp)
  endif
  ! Check flags'consistency
  call check_line(kt,nline,kt0,1,ifatal)
  call check_line(kv,nline,kv0,2,ier)
  ifatal=ifatal+ier
  call check_line(kd,nline,kd0,3,ier)
  ifatal=ifatal+ier
  if (kt0.ne.0 .and. kd0.ne.0 .and. kt0.ne.kd0) ifatal=ifatal+1
  if (ifatal.ne.0) then
    write(6,104) ifatal
    error = .true.
    return
  endif
  !
  if (sic_present(3,0)) then  ! /SHOW
    ! User feeback
    if (use_cursor) then
      from = 'cursor'
    elseif (use_file) then
      from = 'file'
    elseif (use_command) then
      from = 'command line'
    else
      from = 'prompt'
    endif
    call class_message(seve%i,proc,  &
      'Input values read successfully from '//trim(from)//' (method ABSORPTION):')
    do i=1,nline
      j=1+3*i
      write(mess,102) i,kt(i),spar(j-2),kv(i),spar(j-1),kd(i),spar(j)
      call class_message(seve%i,proc,mess)
    enddo
  endif
  !
  return
  !
97 mess = 'Error reading guesses from '//filnam
  call class_message(seve%e,proc,mess)
  close (jtmp)
  call sic_frelun(jtmp)
  error = .true.
  return
  !
104 format(' * * * ',i3,' Fatal Errors on Parameters. Try Again ')
103 format('Line ',i0,' :')
102 format(' Line ',i0,' :',3(2x,i1,2x,1pg11.4))
101 format('      Setting line ',i0)
end subroutine gueabs
!
subroutine gueshell(line,obs,error)
  use gildas_def
  use gbl_message
  use gbl_constant
  use classfit_dependencies_interfaces
  use classfit_interfaces, except_this=>gueshell
  use class_types
  use gauss_parameter
  !---------------------------------------------------------------------
  ! @ private
  ! Define starting values for gaussian fits. These values are stored
  ! in array SPAR and are associated with an integer code defining the
  ! parameter class. Conventions for these codes are:
  !  code = 0  variable parameter
  !  code = 1  fixed parameter
  !  code = 2  variable reference parameter
  !  code = 3  dependent parameter offset or ratio
  !  code = 4  fixed reference parameter
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: line   ! Input command line
  type(observation), intent(in)    :: obs    !
  logical,           intent(inout) :: error  ! Error flag
  ! Local
  character(len=*), parameter :: proc='LINES'
  character(len=*), parameter :: auto='AUTOMATIC'
  real(kind=8) :: dbl
  character(len=9) :: argum,prompt
  character(len=filename_length) :: filnam,name
  character(len=80) :: answer
  integer(kind=4) :: nl, jtmp, i,j,ifatal,ier,ios
  logical :: use_cursor,use_file,use_command
  integer(kind=4) :: lpar,npar
  character(len=20) :: cpar
  character(len=12) :: from
  character(len=message_length) :: mess
  !
  use_file = sic_present(2,0)
  use_cursor = .not.sic_present(1,0) .and. .not.use_file
  if (.not.use_file) filnam = "interactive input"
  !
  ! Read from an input file
  if (use_file) then
     call sic_ch (line,2,1,name,nl,.true.,error)
     if (error) return
     ier = sic_getlun(jtmp)
     if (ier.ne.1) then
        call class_message(seve%e,proc,'No logical unit left')
        error = .true.
        call sic_frelun(jtmp)
        return
     endif
     call sic_parse_file(name,' ','.line',filnam)
     ier = sic_open (jtmp,filnam,'OLD',.true.)
     if (ier.ne.0) then
        call class_message(seve%e,proc,'Error opening guess file '//filnam)
        call putios('        ',ier)
        error = .true.
        call sic_frelun(jtmp)
        return
     endif
     read (jtmp,*,err=97,iostat=ios) nline
     if (ios.ne.0) goto 97
     if (nline.eq.0) then
        close (jtmp)
        call sic_frelun(jtmp)
        return
     else if (nline.lt.0 .or. nline.gt.mxline) then
        write(mess,'(a,i0,a)')  'Invalid number of lines (max. is ',mxline,' lines)'
        call class_message(seve%e,proc,mess)
        error = .true.
        close (jtmp)
        call sic_frelun(jtmp)
        return
     endif
     !
     ! read the parameters
     do i=1,nline
        j=4*i
        read(jtmp,*,err=97,iostat=ios) &
             kt(i),spar(j-3),kv(i),spar(j-2),   &
             kd(i),spar(j-1),kh(i),spar(j)
        if (ios.ne.0) goto 97
     enddo
     close (jtmp)
     call sic_frelun(jtmp)
  else
     ! Interactive initialization
     ! Get number of lines
     if (sic_present(0,1)) then
        ! Keyword "AUTOMATIC" ?
        call sic_ke (line,0,1,argum,nl,.true.,error)
        if (error) return
        nl = min(nl,9)
        if (argum(1:nl).eq.auto(1:nl)) then
           nline = -1
        else
           ! Non-automatic: get the exact number
           call sic_i4 (line,0,1,nl,.true.,error)
           if (error) return
           nline = nl
        endif
     endif
     if (nline.le.0)  return  ! Automatic
     if (nline.gt.mxline) then
       write(mess,'(A,I0)')  'Number of lines is limited to ',mxline
       call class_message(seve%e,proc,mess)
       error = .true.
       return
     endif
     !
     ! Check if input parameters are in Command line
     if ((sic_narg(0)-1).eq.nline) then
        use_command = .true.
        use_cursor = .false.
        call class_message(seve%e,proc,  &
          'Reading inputs from command line is not implemented for method SHELL')
        goto 97
     endif
     !
     ! Starting values for SHELL fit
     if (use_cursor) then
        if (.not.gtg_curs()) then
           call class_message(seve%e,proc,'No cursor available')
           error = .true.
           return
        endif
        write (6,*) 'Using the cursor, type / to keep last values '
        write (6,*) '      any other key to set a line boundary'
     endif
     !
     do i=1,nline
        if (use_cursor) then
           write (6,9) i
           call cursor(obs,spar(1+4*(i-1)),1,error)
           if (error) then
              call class_message(seve%e,proc,'Error while reading input parameter')
              return
           endif
           kt(i) = 0
           kv(i) = 0
           kd(i) = 0
           ko(i) = 0
        else
           write(6,5) i,&
                kt(i),spar(1+4*(i-1)),&
                kv(i),spar(2+4*(i-1)),&
                kd(i),spar(3+4*(i-1)),&
                ko(i),spar(4+4*(i-1))
           write (prompt,103) i
           call sic_wprn (prompt,answer,nl)
           if (nl.eq.0) then
              cycle
           else
              call sic_blanc(answer(1:nl),nl)
              npar = 1
              do while (npar.lt.nl)
                 call sic_next(answer(npar:nl),cpar,lpar,npar)
                 call sic_math_dble(cpar,lpar,dbl,error)
                 if (error) goto 97
              enddo
              error = .true.
              read(answer(1:nl),*,end=100,iostat=ios) &
                   kt(i),spar(1+4*(i-1)),&
                   kv(i),spar(2+4*(i-1)),&
                   kd(i),spar(3+4*(i-1)),&
                   ko(i),spar(4+4*(i-1))
              if (ios.ne.0) goto 97
              error = .false.
           endif
        endif ! loop on lines
     enddo
  endif ! interactive initialization
100 continue
  if (error) goto 97
  !
  ! Check flags'consistency
  call check_line(kt,nline,kt0,1,ifatal)
  call check_line(kv,nline,kv0,2,ier)
  ifatal=ifatal+ier
  call check_line(kd,nline,kd0,3,ier)
  ifatal=ifatal+ier
  call check_line(kh,nline,kh0,4,ier)
  ifatal=ifatal+ier
  if (kt0.ne.0 .and. kd0.ne.0 .and. kt0.ne.kd0) ifatal=ifatal+1
  if (kt0.ne.0 .and. kh0.ne.0 .and. kt0.ne.kh0) ifatal=ifatal+1
  if (kt0.ne.0 .and. kv0.ne.0 .and. kt0.ne.kv0) ifatal=ifatal+1
  if (kd0.ne.0 .and. kv0.ne.0 .and. kd0.ne.kv0) ifatal=ifatal+1
  if (kh0.ne.0 .and. kv0.ne.0 .and. kh0.ne.kv0) ifatal=ifatal+1
  if (kh0.ne.0 .and. kd0.ne.0 .and. kh0.ne.kd0) ifatal=ifatal+1
  if (ifatal.ne.0) then
    write(6,4) ifatal
    error = .true.
    return
  endif
  !
  if (sic_present(3,0)) then  ! /SHOW
    ! User feeback
    if (use_cursor) then
      from = 'cursor'
    elseif (use_file) then
      from = 'file'
    elseif (use_command) then
      from = 'command line'
    else
      from = 'prompt'
    endif
    call class_message(seve%i,proc,  &
      'Input values read successfully from '//trim(from)//' (method SHELL):')
    do i=1,nline
      write(mess,5) i,                  &
                kt(i),spar(1+4*(i-1)),  &
                kv(i),spar(2+4*(i-1)),  &
                kd(i),spar(3+4*(i-1)),  &
                ko(i),spar(4+4*(i-1))
      call class_message(seve%i,proc,mess)
    enddo
  endif
  !
  return
  !
97 continue
  call class_message(seve%e,proc,'Error reading guesses from '//filnam)
  close (jtmp)
  call sic_frelun(jtmp)
  error = .true.
  return
  !
4 format(' * * * ',i3,' Fatal Errors on Parameters. Try Again ')
5 format(' Line ',i0,' :',4(2x,i1,2x,1pg11.4))
9 format('      Setting line ',i0)
103 format('Line ',i0,' :')
end subroutine gueshell
!
subroutine guepoi(line,error)
  use gbl_message
  use classfit_interfaces, except_this=>guepoi
  !---------------------------------------------------------------------
  ! @ private
  ! LAS Support routine for command
  !   LINES N
  ! for continuum data type
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: line   ! Input command line
  logical,          intent(out) :: error  ! Error flag
  call class_message(seve%e,'LINES','Not implement for continuum data')
  error = .true.
end subroutine guepoi
!
subroutine check_line(kflag,nline,khead,ivar,ierr)
  use gbl_message
  use classfit_interfaces, except_this=>check_line
  use gauss_parameter, only: mxline
  !---------------------------------------------------------------------
  ! @ private
  !  Control the gauss fit parameter flags
  !---------------------------------------------------------------------
  integer(kind=4), intent(inout) :: kflag(mxline)  ! Flags to be checked
  integer(kind=4), intent(in)    :: nline          ! Number of lines
  integer(kind=4), intent(out)   :: khead          ! The independant line in the group, if relevant
  integer(kind=4), intent(in)    :: ivar           ! Variable code (fixed, adjustable)
  integer(kind=4), intent(out)   :: ierr           ! Error code
  ! Local
  character(len=*), parameter :: rname='LINES'
  character(len=*), parameter :: what(3) = (/'Intensity',' Position','   Width '/)
  character(len=1) :: ch
  integer(kind=4) :: nfree,nhead,ndep,i
  !
  ierr=0
  if (nline.eq.0) then
     kflag(1)=mod(kflag(1),2)
     khead=0
     return
  endif
  !
  ! Sanity check
  if (any(kflag(1:nline).lt.0) .or. any(kflag(1:nline).gt.4))then
     call class_message(seve%e,rname,'Flag should be in range 0-4')
     ierr=1
     return
  endif
  !
  ! Find free (out of group), dependant (in group), and independant (head of group) lines
  nfree=0
  nhead=0
  khead=0
  ndep=0
  !
  do i=1,nline
     if (kflag(i).eq.0 .or. kflag(i).eq.1) nfree = nfree+1
     if (kflag(i).eq.2 .or. kflag(i).eq.4) then
       nhead = nhead+1
       khead=i
     endif
     if (kflag(i).eq.3) ndep = ndep+1
  enddo
  !
  if (nhead.gt.1) then
     call class_message(seve%e,rname,'Several groups in '//what(ivar))
     ierr = 1
  endif
  if (ndep.eq.0 .and. khead.ne.0) then
     write(ch,'(I1)') khead
     call class_message(seve%w,rname,  &
       'Reference line '//ch//' alone in a '//what(ivar)//' group')
  endif
  if (ndep.ne.0 .and. khead.eq.0) then
     call class_message(seve%e,rname,'No independent '//what(ivar))
     ierr=1
  endif
  if (nfree.gt.0 .and. khead.ne.0) then
    ! In case of "out-of-group" lines, the current implementation mimics a
    ! group with a fixed head with neutral parameters (e.g. reference
    ! velocity=0). This obviously can not be mixed with a real group.
    ! See how the "major variables" are set in midauss and reused in mingauss.
    call class_message(seve%e,rname,'Invalid mixture of free and grouped lines')
    ierr = 1
  endif
  !
end subroutine check_line
