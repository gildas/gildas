subroutine midpoi(set,obs,fit,ifatal,liter)
  use gildas_def
  use gbl_message
  use fit_minuit
  use classfit_dependencies_interfaces
  use classfit_interfaces
  use class_setup_new
  use class_types
  use gauss_parameter
  use pointing_fit
  !----------------------------------------------------------------------
  ! @ no-interface (because of module problems)
  !  Start a gaussian fit by building the PAR array and internal variable
  ! used by Minuit.
  !  This is a modified version of the LAS minimization routine which
  ! includes a linear baseline removal. Baseline parameters are set in
  ! parameters PAR(1) (Offset) and PAR(2) (Slope). Initial values for
  ! both parameters are set by INITVA. A maximum of two Gaussian lines is
  ! fitted.
  !----------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set     !
  type(observation),   intent(inout) :: obs     !
  type(fit_minuit_t),  intent(inout) :: fit     ! Fitting variables
  integer(kind=4),     intent(out)   :: ifatal  ! Nb of errors
  logical,             intent(in)    :: liter   ! Iterate a fit
  ! Local
  character(len=*), parameter :: rname='MIDPOINT'
  integer(kind=4) :: i, nxy, ninte, k
  real(kind=8) :: sav, sav2, vplu, vminu
  character(len=80) :: mess
  !
  nxy = obs%cimax-obs%cimin+1
  fit%isw(1:7) = 0
  fit%sigma = 0.d0
  fit%npfix = 0
  ninte = 0
  fit%nu = 0
  fit%npar = 0
  ifatal = 0
  fit%u(1:fit%maxext)      = 0.0d0
  fit%lcode(1:fit%maxext)  = 0
  fit%lcorsp(1:fit%maxext) = 0
  fit%isw(5) = 1
  fit%data = locwrd(obs)
  !
  ! Starting values
  call begpoi(obs,obs%datax(obs%cimin:obs%cimax),  &
    obs%spectre(obs%cimin:obs%cimax),nxy,par,wfit)
  if (ngline.eq.2) then
     kt0 = 1
     kv0 = 1
     kd0 = 1
     kt(1) = 2
     kv(1) = 2
     kd(1) = 2
     if (.not.sic_present(0,1) .and. width.ne.0.0) then
        kd(kd0) = 4
        par(5) = width
     endif
     kt(2) = 3
     kv(2) = 3
     kd(2) = 3
     nline = 2
     if (sic_present(0,1)) then
        kv0 = 0
        kv(1) = 0
        kv(2) = 0
     endif
     if (sic_present(0,2)) then
        par(3*kt(1)) = par(3*kt(1))*par(3*kt0)
        kt0 = 0
        kt(1) = 0
        kt(2) = 0
     elseif (aratio.ne.0.0) then
        par(3*kt(2)-3) = aratio    ! 3*KT2-3 in this case...
     endif
     if (sic_present(0,3)) then
        par(3*kd(1)+2) = par(3*kd(1)+2)*par(3*kd0+2)
        kd0 = 0
        kd(1) = 0
        kd(2) = 0
     elseif (wratio.ne.0.0) then
        par(3*kd(2)+2-3) = wratio  ! 3*KD2+2-3 in this case...
     endif
  else
     ngline = 1
     nline = 0
     kt0 = 0
     kv0 = 0
     kd0 = 0
     kt(1) = 0
     kv(1) = 0
     kd(1) = 0
     if (.not.sic_present(0,1) .and. width.ne.0.0) then
        par(5) = width
        kd0 = 0
        kd(1) = 1
        nline = 1
     endif
  endif
  !
  fit%nu = 2+3*max(nline,1)
  !
  ! Type initial guesses
  fit%nu=fit%nu+3
  call class_message(seve%w,rname,'Input Parameters:  Temperature  Position  Width')
  k = 3
  do i=1,max(nline,1)
     write(mess,1002) par(k)*class_setup_get_fangle(),  &
                      par(k+1)*class_setup_get_fangle(),  &
                      par(k+2)*class_setup_get_fangle()
     call class_message(seve%w,rname,mess)
     k=k+3
  enddo
  !
  ! Set up Parameters for Major Variables
  !
  ! Position
  if (kv0.eq.0) then
     fit%u(2)=0.
     fit%werr(2)=0.
  else
     fit%u(2)=par(3*kv0+1)
     if (kv(kv0).eq.4) then
        fit%werr(2)=0.
     else
        fit%werr(2)=deltav
        ! if (liter) fit%werr(2)=err(3*kv0+1)
        fit%alim(2)=fit%u(2)-0.25*nxy*deltav
        fit%blim(2)=fit%u(2)+0.25*nxy*deltav
     endif
  endif
  !
  ! Line Widths
  if (kd0.eq.0) then
     fit%u(3)=1./1.665109           ! 1 / (2*SQRT(LN(2)))
     fit%werr(3)=0.
  else
     fit%u(3)=abs(par(3*kd0+2))/1.665109
     if (kd(kd0).eq.4) then
        fit%werr(3)=0.
     else
        fit%werr(3)=2.0*deltav
        ! if (liter) fit%werr(3)=err(3*kd0+2)
        fit%alim(3)=deltav
        fit%blim(3)=nxy*deltav
     endif
  endif
  !
  ! Areas
  if (kt0.eq.0) then
     fit%u(1)=0.564189584           ! 1 / SQRT(PI)
     fit%werr(1)=0.
  else
     fit%u(1)=par(3*kt0)
     if (kt(kt0).eq.4) then
        fit%werr(1)=0.
     else
        fit%werr(1)=max(sigbas,sigrai)*deltav
        ! if (liter) fit%werr(1)=err(3*kt0)
        if (fit%u(1).ne.0.) then
           fit%alim(1)=min(0.d0,4*fit%u(1))
           fit%blim(1)=max(0.d0,4*fit%u(1))
        else
           fit%lcode(1)=1
        endif
     endif
  endif
  !
  ! Offset
  fit%u(4) = par(1)
  fit%werr(4) = 0.5*sigbas*sqrt(float(nxy))
  if (fit%u(1).ne.0) then
     fit%alim(4) = par(1) - 2*max(fit%u(1),5*fit%werr(4))
     fit%blim(4) = par(1) + 2*max(fit%u(1),5*fit%werr(4))
  else
     fit%lcode(4) = 1
  endif
  ! Slope
  fit%u(5) = par(2)
  fit%werr(5) = (sigbas+sigrai)/abs(deltav)/nxy/4  !
  ! The division by 4 is to take into account the fact that the slope is actually derived
  ! from about 20 channels or so.
  fit%alim(5) = par(2) - 10*fit%werr(5)
  fit%blim(5) = par(2) + 10*fit%werr(5)
  !
  ! Set up parameters for Secondary Variables
  !
  k=6
  do i=1,max(nline,1)
     ! Area
     fit%u(k)=par(k-3)
     if (kt(i).eq.0 .or. nline.eq.0) then
        fit%werr(k)=sigbas*deltav
        ! if (liter) fit%werr(k)=err(k-3)
        if (fit%u(k).ne.0.) then
           fit%alim(k)=min(0.d0,4*fit%u(k))
           fit%blim(k)=max(0.d0,4*fit%u(k))
        else
           fit%lcode(k)=1             ! No Boundaries if fit%u(k)=0.
        endif
     else
        fit%werr(k)=0.
        if (i.eq.kt0) then
           fit%u(k)=0.564189584
        else
           fit%u(k)=fit%u(k)*0.564189584
        endif
     endif
     k=k+1
     !
     ! Position
     fit%u(k)=par(k-3)
     if (kv(i).eq.0 .or. nline.eq.0) then
        fit%werr(k)=2.*deltav
        ! if (liter) fit%werr(k)=err(k-3)
        fit%alim(k)=fit%u(k)-0.25*nxy*deltav
        fit%blim(k)=fit%u(k)+0.25*nxy*deltav
     else
        fit%werr(k)=0.
        if (i.eq.kv0) fit%u(k)=0.
     endif
     k=k+1
     !
     ! Width
     fit%u(k)=abs(par(k-3))
     if (kd(i).eq.0 .or. nline.eq.0) then
        fit%werr(k)=2.*deltav
        ! if (liter) fit%werr(k)=err(k-3)
        fit%alim(k)=deltav
        fit%blim(k)=nxy*deltav
     else
        fit%werr(k)=0.
        if (i.eq.kd0) fit%u(k)=1.
     endif
     k=k+1
  enddo
  !
  ! Various checks
  do k= 1, fit%nu
     if (k .gt. fit%maxext) then
        ifatal = ifatal+1
     else if (fit%werr(k).le.0.d0) then
        ! Fixed parameter
        fit%lcode(k) = 0
        if (k.gt.3) then
           write(mess,'(a,i3,a)') "Parameter",k-3," is fixed"
           call class_message(seve%w,rname,mess)
        endif
     else
        ! Variable parameter
        ninte = ninte + 1
        ! Parameter with limits
        if (fit%lcode(k).ne.1) then
           fit%lcode(k) = 4
           sav = (fit%blim(k)-fit%u(k))*(fit%u(k)-fit%alim(k))
           if (sav.lt.0d0) then
             write(mess,'(a,i0,a,f0.3,a,f0.3)')  &
               'Parameter ',k-3,' outside limits ',fit%alim(k),' to ',fit%blim(k)
             call class_message(seve%e,rname,mess)
             ifatal = ifatal + 1
           else if (sav.eq.0d0) then
              if (k.gt.3) then
                 write(mess,'(a,i3,a)') "Parameter",k-3," is at limit"
                 call class_message(seve%w,rname,mess)
              endif
           endif
        endif
     endif
  enddo
  !
  ! End parameter cards
  ! Stop if fatal error
  if (ninte.gt.fit%maxint)  then
    write(mess,'(A,I0,A,I0)') 'Too many variable parameters: ',ninte,' > ',fit%maxint
    call class_message(seve%e,rname,mess)
    ifatal = ifatal + 1
  endif
  if (ninte.eq.0) then
     call class_message(seve%e,rname,'All input parameters are fixed')
     ifatal = ifatal + 1
  endif
  if (ifatal.gt.0)  then
    write(mess,'(I0,A)')  ifatal,' errors on input parameters, abort.'
    call class_message(seve%e,rname,mess)
    return
  endif
  !
  ! O.K. Start
  ! Calculate step sizes DIRIN
  fit%npar = 0
  do k= 1, fit%nu
     if (fit%lcode(k) .gt. 0)  then
        fit%npar = fit%npar + 1
        fit%lcorsp(k) = fit%npar
        sav = fit%u(k)
        fit%x(fit%npar) = pintf(fit,sav,k)
        fit%xt(fit%npar) = fit%x(fit%npar)
        sav2 = sav + fit%werr(k)
        vplu = pintf(fit,sav2,k) - fit%x(fit%npar)
        sav2 = sav - fit%werr(k)
        vminu = pintf(fit,sav2,k) - fit%x(fit%npar)
        fit%dirin(fit%npar) = 0.5d0 * (dabs(vplu) +dabs(vminu))
     endif
  enddo
  return
  !
1002 format((3(f14.3)))
end subroutine midpoi
!
subroutine begpoi(obs,datax,datay,nxy,vpar,wfit)
  use phys_const
  use gbl_message
  use classfit_interfaces, except_this=>begpoi
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  !  Compute guesses for the gaussian line fit to pointing results PAR(1)
  ! and PAR(2) have already been derived by another subroutine (INIPOI)
  !----------------------------------------------------------------------
  type(observation),     intent(inout) :: obs        !
  integer(kind=4),       intent(in)    :: nxy        ! Nb of data points
  integer(kind=4),       intent(in)    :: wfit(nxy)  ! Weights
  real(kind=xdata_kind), intent(in)    :: datax(nxy) ! Input X-data
  real(kind=4),          intent(in)    :: datay(nxy) ! Input Y-data
  real(kind=4),          intent(inout) :: vpar(8)    ! Guesses
  ! Local
  real(kind=4) :: ymax,aire,yy,vsup,ss
  integer(kind=4) :: i
  !
  ! Automatic guess
  ymax=0.
  aire=0.
  do i=2,nxy-1
     if (wfit(i).ne.0) then
        ss = 1+wfit(i-1)+wfit(i)
        yy = (datay(i) + wfit(i-1)*datay(i-1) +   &
             &          wfit(i+1)*datay(i+1) ) /ss - vpar(2)*datax(i)
        if (yy.ge.ymax) then
           ymax = yy
           vsup = datax(i)
        endif
     endif
     !!     aire=aire+yy*abs((datax(i+1)-datax(i-1)))
     if (yy.gt.0.) aire=aire+yy*abs((datax(i+1)-datax(i-1)))
  enddo
  !
  aire  = aire*0.5
  vpar(4)= vsup
  vpar(5)= abs(aire/ymax/1.064467)
  vpar(3)= aire
  !
  if (obs%head%presec(class_sec_bea_id)) then
     if (mod(abs(obs%head%bea%bpos-obs%head%dri%apos),pis).lt.1e-3) then
        call class_message(seve%i,'MIDPOINT','Setting dual-beam fit')
        vpar(6) = -1.0
        vpar(7) = obs%head%bea%space
        vpar(8) = 1.0
        obs%head%poi%nline = 2
        return
     endif
  endif
  obs%head%poi%nline = 0
end subroutine begpoi
