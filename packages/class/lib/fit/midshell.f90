subroutine midshell(obs,fit,ifatal,liter)
  use gildas_def
  use gbl_message
  use fit_minuit
  use classfit_dependencies_interfaces
  use classfit_interfaces, except_this=>midshell
  use class_types
  use gauss_parameter
  !----------------------------------------------------------------------
  ! @ private
  ! Sets up the starting parameter lists.
  !----------------------------------------------------------------------
  type(observation),  intent(in)    :: obs    !
  type(fit_minuit_t), intent(inout) :: fit     ! Fitting variables
  integer(kind=4),    intent(out)   :: ifatal  ! Nb of errors
  logical,            intent(in)    :: liter   ! Iteration ?
  ! Local
  character(len=*), parameter :: rname='MIDSHELL'
  integer(kind=4) :: i, ninte, k, j
  real(kind=8) :: sav, sav2, vplu, vminu
  real(kind=4) :: s0,s1,s2,yy
  character(len=message_length) :: mess
  !
  do i= 1, 7
     fit%isw(i) = 0
  enddo
  fit%sigma = 0.d0
  fit%npfix = 0
  ninte = 0
  fit%nu = 0
  fit%npar = 0
  ifatal = 0
  do i= 1, fit%maxext
     fit%u(i) = 0.0d0
     fit%lcode(i) = 0
     fit%lcorsp (i) = 0
  enddo
  fit%isw(5) = 1
  fit%data = locwrd(obs)
  !
  deltav = abs(obs%datax(obs%cimin+1)-obs%datax(obs%cimin))
  !
  ! Initial guesses
  !
  if (nline.eq.0) then
     s0=0.
     s1=0.
     s2=0.
     do i=obs%cimin+1,obs%cimax-1
        if (wfit(i).ne.0 .and. abs(obs%spectre(i)).gt.sigbas) then
           yy = obs%spectre(i)
           s0 = s0 + yy
           s1 = s1 + yy * obs%datax(i)
           s2 = s2 + yy * obs%datax(i)**2
        endif
     enddo
     if (s0.ne.0) then
        s1 = s1 / s0
        s2 = s2 / s0
        yy = deltav
        par(3) = yy * sqrt (abs(s2-s1**2)*8.*alog(2.))
        par(2) = s1
        par(1) = s0 * yy
     else
        call class_message(seve%w,'MIDSHELL','Null area found, use manual mode')
        ifatal = 1
        return
     endif
     fit%nu= 4
  else
     !
     ! Take initial Guesses from SPAR array
     fit%nu= 4*nline
     do j=1,4*nline
        par(j) = spar(j)
     enddo
  endif
  !
  fit%nu = fit%nu+4
  k  = 1
  write(6,1001)
  do i=1,max(nline,1)
     write (6,'(5x,4(5x,1pg11.4))')   &
          &      par(k),par(k+1),par(k+2),par(k+3)
     k=k+4
  enddo
  !
  ! Set Up Parameters
  ! Areas
  if (kt0.eq.0) then
     fit%u(1)   = 1.0
     fit%werr(1)= 0.0
  else
     fit%u(1)=par(4*kt0-3)
     if (kt(kt0).eq.4) then
        fit%werr(1)=0.
     else
        ! fit%werr(K) = sigbas*sqrt(abs(par(k+2))*deltav)
        fit%werr(1)=sigbas*deltav*3.d0
        if (liter .and. err(4*kt0-3).ne.0) fit%werr(1)=err(4*kt0-3)
        if (fit%u(1).ne.0.d0) then
           fit%alim(1)=min(0.d0,8.d0*fit%u(1))
           fit%blim(1)=max(0.d0,8.d0*fit%u(1))
        else
           fit%lcode(1)=1
        endif
     endif
  endif
  !
  ! Velocities
  if (kv0.eq.0) then
     fit%u(2)=0.
     fit%werr(2)=0.
  else
     fit%u(2)=par(4*kv0-2)
     if (kv(kv0).eq.4) then
        fit%werr(2)=0.
     else
        fit%werr(2)=deltav
        if (liter.and.err(4*kv0-2).ne.0) fit%werr(2)=err(4*kv0-2)
        fit%alim(2)=fit%u(2)-0.1*obs%cnchan*deltav
        fit%blim(2)=fit%u(2)+0.1*obs%cnchan*deltav
     endif
  endif
  !
  ! Line Widths
  if (kd0.eq.0) then
     fit%u(3)=1.
     fit%werr(3)=0.
  else
     fit%u(3)=abs(par(4*kd0-1))
     if (kd(kd0).eq.4) then
        fit%werr(3)=0.
     else
        fit%werr(3)=deltav
        if (liter.and.err(4*kd0-1).ne.0) fit%werr(3)=err(4*kd0-1)
        fit%alim(3)=deltav
        fit%blim(3)=0.5*obs%cnchan*deltav
     endif
  endif
  !
  ! Horn to Center
  if (kh0.eq.0) then
     fit%u(4)=1.
     fit%werr(4)=0.d0
  else
     fit%u(4)=par(4*kh0)
     if (kh(kh0).eq.4) then
        fit%werr(4)=0.d0
     else
        fit%werr(4)=0.05
        if (liter.and.err(4*kh0).ne.0) fit%werr(4)=err(4*kh0)
        fit%alim(4)=-1.0
        fit%blim(4)=100.
     endif
  endif
  ! Set up parameters for Secondary Variables
  !
  k=5
  do i=1,max(nline,1)
     ! Area
     fit%u(k)=par(k-4)
     if (kt(i).eq.0 .or. nline.eq.0) then
        fit%werr(k)=sigbas*deltav*3.0
        if (liter.and.err(k-4).ne.0) fit%werr(k)=err(k-4)
        if (fit%u(k).ne.0.) then
           fit%alim(k)=min(0.d0,8.d0*fit%u(k))
           fit%blim(k)=max(0.d0,8.d0*fit%u(k))
        else
           fit%lcode(k)=1             ! No Boundaries if fit%u(k)=0.
        endif
     else
        fit%werr(k)=0.d0
        if (i.eq.kt0) fit%u(k)=1.d0
     endif
     k=k+1
     !
     ! Velocity
     fit%u(k)=par(k-4)
     if (kv(i).eq.0 .or. nline.eq.0) then
        fit%werr(k)=deltav
        if (liter.and.err(k-4).ne.0) fit%werr(k)=err(k-4)
        fit%alim(k)=fit%u(k)-0.1*obs%cnchan*deltav
        fit%blim(k)=fit%u(k)+0.1*obs%cnchan*deltav
     else
        fit%werr(k)=0.d0
        if (i.eq.kv0) fit%u(k)=0.
     endif
     k=k+1
     !
     ! Line Width
     fit%u(k)=abs(par(k-4))
     if (kd(i).eq.0 .or. nline.eq.0) then
        fit%werr(k)=deltav
        if (liter.and.err(k-4).ne.0) fit%werr(k)=err(k-4)
        fit%alim(k)=deltav
        fit%blim(k)=0.5*obs%cnchan*deltav
     else
        fit%werr(k)=0.d0
        if (i.eq.kd0) fit%u(k)=1.
     endif
     k=k+1
     !
     ! Horn / Center ratio
     fit%u(k)=par(k-4)
     if (kh(i).eq.0 .or. nline.eq.0) then
        fit%werr(k)=0.05
        if (liter.and.err(k-4).ne.0) fit%werr(k)=err(k-4)
        fit%alim(k)=-1.0
        fit%blim(k)=100.
     else
        fit%werr(k)=0.d0
        if (i.eq.kh0) fit%u(k)=1.
     endif
     k=k+1
  enddo
  !
  ! Various checks
  do k= 1, fit%nu
     if (k .gt. fit%maxext) then
        ifatal = ifatal+1
     else if (fit%werr(k).le.0.d0) then
        ! Fixed parameter
        fit%lcode(k) = 0
        if (k.gt.4) then
          write(mess,'(A,I0,A)') 'Parameter ',k-4,' is fixed'
          call class_message(seve%i,rname,mess)
        endif
     else
        ! Variable parameter
        ninte = ninte + 1
        ! Parameter with limits
        if (fit%lcode(k).ne.1) then
           fit%lcode(k) = 4
           sav = (fit%blim(k)-fit%u(k))*(fit%u(k)-fit%alim(k))
           if (sav.lt.0d0) then
             write(mess,'(a,i0,a,f0.3,a,f0.3)')  &
               'Parameter ',k-4,' outside limits ',fit%alim(k),' to ',fit%blim(k)
             call class_message(seve%e,rname,mess)
             ifatal = ifatal + 1
           else if (sav.eq.0d0) then
              if (k.gt.4) then
                write(mess,'(A,I0,A)') 'Parameter ',k-4,' is at limit'
                call class_message(seve%w,rname,mess)
              endif
           endif
        endif
     endif
  enddo
  !
  ! End parameter cards
  ! Stop if fatal error
  if (ninte.gt.fit%maxint) then
    write(mess,'(A,I0,A,I0)') 'Too many variable parameters: ',ninte,' > ',fit%maxint
    call class_message(seve%e,rname,mess)
    ifatal = ifatal + 1
  endif
  if (ninte.le.0) then
     call class_message(seve%e,rname,'All input parameters are fixed')
     ifatal = ifatal + 1
  endif
  if (ifatal.gt.0)  then
    write(mess,'(I0,A)')  ifatal,' errors on input parameters, abort.'
    call class_message(seve%e,rname,mess)
    return
  endif
  !
  ! Calculate step sizes DIRIN
  fit%npar = 0
  do k= 1, fit%nu
     if (fit%lcode(k) .gt. 0)  then
        fit%npar = fit%npar + 1
        fit%lcorsp(k) = fit%npar
        sav = fit%u(k)
        fit%x(fit%npar) = pintf(fit,sav,k)
        fit%xt(fit%npar) = fit%x(fit%npar)
        sav2 = sav + fit%werr(k)
        vplu = pintf(fit,sav2,k) - fit%x(fit%npar)
        sav2 = sav - fit%werr(k)
        vminu = pintf(fit,sav2,k) - fit%x(fit%npar)
        fit%dirin(fit%npar) = 0.5d0 * (dabs(vplu) +dabs(vminu))
     endif
  enddo
  return
  !
1001 format(' Guesses:   Area         Position        ',   &
       &    'Width        Horn to Center')
end subroutine midshell
