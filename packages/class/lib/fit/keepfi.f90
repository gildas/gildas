subroutine keepfi(set,obs,error)
  use gildas_def
  use gbl_message
  use classfit_dependencies_interfaces
  use classfit_interfaces, except_this=>keepfi
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! CLASS support routine for command KEEP
  ! Save profile fit results in the output file.
  ! (Modifies an observation in R onto the output file)
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(observation),   intent(inout) :: obs    !
  logical,             intent(inout) :: error  ! Error flag
  ! Local
  character(len=*), parameter :: rname='KEEP'
  character(len=message_length) :: mess
  !
  if (.not.filein_is_fileout()) then
     call class_message(seve%e,rname,'Input file must equal output file')
     error = .true.
     return
  endif
  !
  call mobs(obs,error)   ! Open an obs. of output file to modify it
  if(error) return
  !
  call wgaus(set,obs,error)  ! Save Gaussian fit parameters
  if (error) return
  !
  call class_write_close(set,obs,error)
  if (error) return
  !
  write(mess,'(A,I0,A,I0,A)')  &
    'Observation #',obs%head%gen%num,';',obs%head%gen%ver,' successfully updated'
  call class_message(seve%i,rname,mess)
  !
end subroutine keepfi
