subroutine cwsec(set,obs,scode,error)
  use gbl_message
  use classcore_interfaces, except_this=>cwsec
  use class_common
  use class_parameter
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !   Write the appropriate section from the input observation to the
  ! associated entry in the output file, taking care of conversion of
  ! the section data from System data type to File data type.
  !  OBS%DESC: updated
  !  OBS%HEAD: input
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(observation),   intent(inout) :: obs    ! Header
  integer(kind=4),     intent(in)    :: scode  ! Section code
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='CWSEC'
  !
  if (error) return
  !
  if (fileout_isvlm) then
    call class_message(seve%e,rname,  &
      'Writing a section is not relevant for a VLM output file')
    error = .true.
    return
  else
    select case (scode)
    case (class_sec_gen_id)  ! General section
      call wgen_classic(obs,error)
    case (class_sec_res_id)  ! Resolution description
      call wres_classic(obs,error)
    case (class_sec_pos_id)  ! Position section
      call wpos_classic(obs,error)
    case (class_sec_spe_id)  ! Spectroscopy section
      call wspec_classic(obs,error)
    case (class_sec_bas_id)  ! Base section
      call wbas_classic(obs,error)
    case (class_sec_plo_id)  ! Plotting section
      call wplo_classic(obs,error)
    case (class_sec_swi_id)  ! Switching section
      call wswi_classic(obs,error)
    case (class_sec_gau_id)  ! Gauss fit
      call wgau_classic(obs,error)
    case (class_sec_she_id)  ! Shell fit
      call wshe_classic(obs,error)
    case (class_sec_hfs_id)  ! HFS fit
      call whfs_classic(obs,error)
    case (class_sec_abs_id)  ! Absorption fit
      call wabs_classic(obs,error)
    case (class_sec_dri_id)  ! Drift Continuum
      call wdri_classic(obs,error)
    case (class_sec_bea_id)  ! Beam-switching section (for spectra or drifts)
      call wbea_classic(obs,error)
    case (class_sec_user_id)  ! User section
      call wuser_classic(obs,error)
    case (class_sec_assoc_id)  ! Associated Arrays section
      call wassoc_classic(obs,error)
    case (class_sec_her_id)  ! Herchel section
      call wherschel_classic(obs,error)
    case default
      ! Same to be done for the other sections
      call cwsec_classic(obs,scode,error)
    end select
  endif
  !
end subroutine cwsec
!
subroutine wgen_classic(obs,error)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>wgen_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the GENeral section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  integer(kind=data_length) :: slen
  integer(kind=4) :: iwork(class_sec_gen_len_max)
  !
  call fileout%conv%writ%r8(obs%head%gen%ut,iwork(1),2)  ! 1-4
  call fileout%conv%writ%r4(obs%head%gen%az,iwork(5),5)  ! 5-9
  if (obs%desc%version.ge.2) then
    slen = class_sec_gen_len_v2_2
    call fileout%conv%writ%r8(obs%head%gen%parang,iwork(10),1)  ! 10-11
  else
    slen = class_sec_gen_len_v2_1
  endif
  !
  call wsec(obs%desc,class_sec_gen_id,slen,iwork,error)
  if (error)  return
  !
end subroutine wgen_classic
!
subroutine wres_classic(obs,error)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>wres_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the RESolution section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  integer(kind=data_length) :: slen
  integer(kind=4) :: iwork(class_sec_res_len)
  !
  slen = class_sec_res_len
  call fileout%conv%writ%r4(obs%head%res%major,iwork(1),3)  !  1- 3
  !
  call wsec(obs%desc,class_sec_res_id,slen,iwork,error)
  if (error)  return
  !
end subroutine wres_classic
!
subroutine wpos_classic(obs,error)
  use gildas_def
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>wpos_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the POSition section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='WPOS'
  integer(kind=data_length) :: slen
  integer(kind=4) :: iwork(class_sec_pos_len_max)  ! Largest possible
  character(len=message_length) :: mess
  !
  if (obs%head%pos%system.lt.type_un .or.  &
      obs%head%pos%system.gt.type_ic) then
    write(mess,'(A,I0,A)')  'Type of coordinates (code ',  &
      obs%head%pos%system,') not supported'
    call class_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  if (obs%desc%version.ge.2) then
    slen = class_sec_pos_len_v2
    call fileout%conv%writ%cc(obs%head%pos%sourc, iwork(1), 3)
    call fileout%conv%writ%r4(obs%head%pos%system,iwork(4), 3)
    call fileout%conv%writ%r8(obs%head%pos%lam,   iwork(7), 3)
    call fileout%conv%writ%r4(obs%head%pos%lamof, iwork(13),2)
  else
    if (obs%head%pos%projang.ne.0.d0) then
      call class_message(seve%e,rname,  &
        'Can not export a non-zero projection angle into an observation version 1')
      error = .true.
      return
    endif
    slen = class_sec_pos_len_v1
    call fileout%conv%writ%cc(obs%head%pos%sourc,  iwork(1), 3)
    ! V1: obs%head%pos%system written in the index
    call fileout%conv%writ%r4(obs%head%pos%equinox,iwork(4), 1)
    call fileout%conv%writ%r8(obs%head%pos%lam,    iwork(5), 2)
    call fileout%conv%writ%r4(obs%head%pos%lamof,  iwork(9), 2)
    call fileout%conv%writ%i4(obs%head%pos%proj,   iwork(11),1)
    call fileout%conv%writ%r8(0.d0,                iwork(12),1)  ! sl0p
    call fileout%conv%writ%r8(0.d0,                iwork(14),1)  ! sb0p
    call fileout%conv%writ%r8(0.d0,                iwork(16),1)  ! sk0p
  endif
  !
  call wsec(obs%desc,class_sec_pos_id,slen,iwork,error)
  if (error)  return
  !
end subroutine wpos_classic
!
subroutine wspec_classic(obs,error)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>wspec_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the SPECtroscopic Section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='WSPEC'
  integer(kind=data_length) :: slen
  integer(kind=4) :: iwork(class_sec_spe_len_max)  ! Largest possible
  real(kind=4) :: r4value(5)
  if (obs%desc%version.ge.3) then  ! Version 3
    slen = class_sec_spe_len_v3
    call filein%conv%writ%cc(obs%head%spe%line, iwork( 1),3)  !  1-3
    call filein%conv%writ%i4(obs%head%spe%nchan,iwork( 4),1)  !  4
    call filein%conv%writ%r8(obs%head%spe%restf,iwork( 5),7)  !  5-18
    call filein%conv%writ%r4(obs%head%spe%bad,  iwork(19),1)  ! 19
    call filein%conv%writ%i4(obs%head%spe%vtype,iwork(20),3)  ! 20-22
  else                             ! Version 1.3
    slen = class_sec_spe_len_v1_3
    call fileout%conv%writ%cc(obs%head%spe%line,   iwork(1), 3)  ! 1-3
    call fileout%conv%writ%r8(obs%head%spe%restf,  iwork(4), 1)  ! 4-5
    call fileout%conv%writ%i4(obs%head%spe%nchan,  iwork(6), 1)  ! 6
    ! R*4 on disk, R*8 in memory
    r4value(1) = real(obs%head%spe%rchan,kind=4)
    r4value(2) = real(obs%head%spe%fres, kind=4)
    r4value(3) = 0.  ! foff is always 0
    r4value(4) = real(obs%head%spe%vres, kind=4)
    r4value(5) = real(obs%head%spe%voff, kind=4)
    call fileout%conv%writ%r4(r4value,             iwork(7), 5)  ! 7-11
    call fileout%conv%writ%r4(obs%head%spe%bad,    iwork(12),1)  ! 12
    call fileout%conv%writ%r8(obs%head%spe%image,  iwork(13),1)  ! 13-14
    call fileout%conv%writ%i4(obs%head%spe%vtype,  iwork(15),1)  ! 15
    call fileout%conv%writ%r8(obs%head%spe%doppler,iwork(16),1)  ! 16-17
  endif
  !
  call wsec(obs%desc,class_sec_spe_id,slen,iwork,error)
  if (error)  return
  !
end subroutine wspec_classic
!
subroutine wbas_classic(obs,error)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>wbas_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the BASe Section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='WBAS'
  integer(kind=data_length) :: slen
  integer(kind=4) :: iwork(class_sec_bas_len)  ! Largest possible
  slen = class_sec_bas_len
  call fileout%conv%writ%i4(obs%head%bas%deg,  iwork(1),1)
  call fileout%conv%writ%r4(obs%head%bas%sigfi,iwork(2),2)
  call fileout%conv%writ%i4(obs%head%bas%nwind,iwork(4),1)
  ! Valid Nwind values can be:
  !  -1 = basnwind_assoc for LINE associated array,
  !   0 for no windows
  !  >0 for 1 or more windows
  if (obs%head%bas%nwind.gt.0) then
    call fileout%conv%writ%r4(obs%head%bas%w1,iwork(5),                   obs%head%bas%nwind)
    call fileout%conv%writ%r4(obs%head%bas%w2,iwork(5+obs%head%bas%nwind),obs%head%bas%nwind)
  endif
  !
  call wsec(obs%desc,class_sec_bas_id,slen,iwork,error)
  if (error)  return
  !
end subroutine wbas_classic
!
subroutine wplo_classic(obs,error)
  use gildas_def
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>wplo_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the Plotting section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  integer(kind=data_length) :: slen
  integer(kind=4) :: iwork(class_sec_plo_len)
  !
  slen = class_sec_plo_len
  call fileout%conv%writ%r4(obs%head%plo%amin,iwork,class_sec_plo_len)
  !
  call wsec(obs%desc,class_sec_plo_id,slen,iwork,error)
  if (error)  return
  !
end subroutine wplo_classic
!
subroutine wswi_classic(obs,error)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>wswi_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the Switching section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='WSWI'
  integer(kind=data_length) :: slen
  integer(kind=4) :: i,iwork(class_sec_swi_len)
  character(len=message_length) :: mess
  integer(kind=4) :: jx(4)
  real(kind=8) :: xx(2)
  equivalence (xx,jx)
  !
  ! Sanity check
  if (obs%head%swi%swmod.eq.mod_mix) then  ! Mixed mode
    if (obs%head%swi%nphas.ne.0) then
      write(mess,'(A,I0,A)')  &
        'Number of switching phases must be 0 for mixed switching mode (got ',  &
        obs%head%swi%nphas,')'
      call class_message(seve%e,rname,mess)
      error = .true.
      return
    endif
  else  ! Other switching modes
    if (obs%head%swi%nphas.le.0) then
      write(mess,'(A,I0,A)')  'Number of switching phases must be positive (got ',  &
        obs%head%swi%nphas,')'
      call class_message(seve%e,rname,mess)
      error = .true.
      return
    elseif (obs%head%swi%nphas.gt.mxphas) then
      write(mess,'(A,I0,A,I0,A)')  'Number of switching phases must be lower than ',  &
        mxphas,' (got ',obs%head%swi%nphas,')'
      call class_message(seve%e,rname,mess)
      error = .true.
      return
    endif
  endif
  !
  slen = 2 + 6*obs%head%swi%nphas
  !
  call fileout%conv%writ%i4(obs%head%swi%nphas,iwork(1),1)
  ! call fileout%conv%writ%r8(obs%head%swi%decal,iwork(2),rnphas)
  do i=1,obs%head%swi%nphas
    call fileout%conv%writ%r8(obs%head%swi%decal(i),xx,1)
    iwork(2*i)   = jx(1)
    iwork(2*i+1) = jx(2)
  enddo
  call fileout%conv%writ%r4(obs%head%swi%duree,iwork(2+2*obs%head%swi%nphas),obs%head%swi%nphas)
  call fileout%conv%writ%r4(obs%head%swi%poids,iwork(2+3*obs%head%swi%nphas),obs%head%swi%nphas)
  call fileout%conv%writ%i4(obs%head%swi%swmod,iwork(2+4*obs%head%swi%nphas),1)
  call fileout%conv%writ%r4(obs%head%swi%ldecal,iwork(3+4*obs%head%swi%nphas),obs%head%swi%nphas)
  call fileout%conv%writ%r4(obs%head%swi%bdecal,iwork(3+5*obs%head%swi%nphas),obs%head%swi%nphas)
  !
  call wsec(obs%desc,class_sec_swi_id,slen,iwork,error)
  if (error)  return
  !
end subroutine wswi_classic
!
subroutine wgau_classic(obs,error)
  use gildas_def
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>wgau_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the Gauss fit section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='WGAU'
  integer(kind=4) :: dline,dw,mline,mw
  integer(kind=data_length) :: dlen
  integer(kind=4), allocatable :: iwork(:)
  !
  dline = max(5,obs%head%gau%nline)  ! Max number of lines on disk. At least 5 preserves forward compatibility of old CLASS versions
  dw = ngauspar * dline              ! Max number of words per line on disk
  dlen = 3+2*dw                      ! Section length on disk
  mline = obs%head%gau%nline         ! Actual number of used lines in memory
  mw = ngauspar * mline              ! Actual number of words per used line in memory
  !
  ! Fill unused parameters with zeroes before writing to disk
  obs%head%gau%nfit(mw+1:) = 0.
  obs%head%gau%nerr(mw+1:) = 0.
  !
  allocate(iwork(dlen))
  call fileout%conv%writ%i4(obs%head%gau%nline,iwork(1),   1)
  call fileout%conv%writ%r4(obs%head%gau%sigba,iwork(2),   2)
  call fileout%conv%writ%r4(obs%head%gau%nfit, iwork(4),   dw)
  call fileout%conv%writ%r4(obs%head%gau%nerr, iwork(4+dw),dw)
  !
  call wsec(obs%desc,class_sec_gau_id,dlen,iwork,error)
  if (error)  return
  !
end subroutine wgau_classic
!
subroutine wshe_classic(obs,error)
  use gildas_def
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>wshe_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the Shell fit section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='WSHE'
  integer(kind=4) :: dline,dw,mline,mw
  integer(kind=data_length) :: dlen
  integer(kind=4), allocatable :: iwork(:)
  !
  dline = max(5,obs%head%she%nline)  ! Max number of lines on disk. At least 5 preserves forward compatibility of old CLASS versions
  dw = nshellpar * dline             ! Max number of words per line on disk
  dlen = 3+2*dw                      ! Section length on disk
  mline = obs%head%she%nline         ! Actual number of used lines in memory
  mw = nshellpar * mline             ! Actual number of words per used line in memory
  !
  ! Fill unused parameters with zeroes before writing to disk
  obs%head%she%nfit(mw+1:) = 0.
  obs%head%she%nerr(mw+1:) = 0.
  !
  allocate(iwork(dlen))
  call fileout%conv%writ%i4(obs%head%she%nline,iwork(1),   1)
  call fileout%conv%writ%r4(obs%head%she%sigba,iwork(2),   2)
  call fileout%conv%writ%r4(obs%head%she%nfit, iwork(4),   dw)
  call fileout%conv%writ%r4(obs%head%she%nerr, iwork(4+dw),dw)
  !
  call wsec(obs%desc,class_sec_she_id,dlen,iwork,error)
  if (error)  return
  !
end subroutine wshe_classic
!
subroutine whfs_classic(obs,error)
  use gildas_def
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>whfs_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the HFS fit section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='WHFS'
  integer(kind=4) :: dline,dw,mline,mw
  integer(kind=data_length) :: dlen
  integer(kind=4), allocatable :: iwork(:)
  !
  dline = max(3,obs%head%hfs%nline)  ! Max number of lines on disk. At least 3 preserves forward compatibility of old CLASS versions
  dw = nhfspar * dline               ! Max number of words per line on disk
  dlen = 3+2*dw                      ! Section length on disk
  mline = obs%head%hfs%nline         ! Actual number of used lines in memory
  mw = nhfspar * mline               ! Actual number of words per used line in memory
  !
  ! Fill unused parameters with zeroes before writing to disk
  obs%head%hfs%nfit(mw+1:) = 0.
  obs%head%hfs%nerr(mw+1:) = 0.
  !
  allocate(iwork(dlen))
  call fileout%conv%writ%i4(obs%head%hfs%nline,iwork(1),   1)
  call fileout%conv%writ%r4(obs%head%hfs%sigba,iwork(2),   2)
  call fileout%conv%writ%r4(obs%head%hfs%nfit, iwork(4),   dw)
  call fileout%conv%writ%r4(obs%head%hfs%nerr, iwork(4+dw),dw)
  !
  call wsec(obs%desc,class_sec_hfs_id,dlen,iwork,error)
  if (error)  return
  !
end subroutine whfs_classic
!
subroutine wabs_classic(obs,error)
  use gildas_def
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>wabs_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the Absorption fit section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='WABS'
  integer(kind=4) :: dline,dw,mline,mw
  integer(kind=data_length) :: dlen
  integer(kind=4), allocatable :: iwork(:)
  !
  dline = max(5,obs%head%abs%nline)  ! Max number of lines on disk. At least 5 preserves forward compatibility of old CLASS versions
  dw = 1 + nabspar * dline           ! Max number of words per line on disk
  dlen = 3+2*dw                      ! Section length on disk
  mline = obs%head%abs%nline         ! Actual number of used lines in memory
  mw = 1 + nabspar * mline           ! Actual number of words per used line in memory
  !
  ! Fill unused parameters with zeroes before writing to disk
  obs%head%abs%nfit(mw+1:) = 0.
  obs%head%abs%nerr(mw+1:) = 0.
  !
  allocate(iwork(dlen))
  call fileout%conv%writ%i4(obs%head%abs%nline,iwork(1),   1)
  call fileout%conv%writ%r4(obs%head%abs%sigba,iwork(2),   2)
  call fileout%conv%writ%r4(obs%head%abs%nfit, iwork(4),   dw)
  call fileout%conv%writ%r4(obs%head%abs%nerr, iwork(4+dw),dw)
  !
  call wsec(obs%desc,class_sec_abs_id,dlen,iwork,error)
  if (error)  return
  !
end subroutine wabs_classic
!
subroutine wdri_classic(obs,error)
  use gildas_def
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>wdri_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the Drift Continuum section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='WDRI'
  integer(kind=data_length) :: slen
  integer(kind=4) :: iwork(class_sec_dri_len)
  !
  call fileout%conv%writ%r8(obs%head%dri%freq,iwork(1),1)
  call fileout%conv%writ%r4(obs%head%dri%width,iwork(3),1)
  call fileout%conv%writ%i4(obs%head%dri%npoin,iwork(4),1)
  call fileout%conv%writ%r4(obs%head%dri%rpoin,iwork(5),7)
  call fileout%conv%writ%i4(obs%head%dri%ctype,iwork(12),1)
  call fileout%conv%writ%r8(obs%head%dri%cimag,iwork(13),1)
  call fileout%conv%writ%r4(obs%head%dri%colla,iwork(15),2)
  !
  slen = class_sec_dri_len
  call wsec(obs%desc,class_sec_dri_id,slen,iwork,error)
  if (error)  return
  !
end subroutine wdri_classic
!
subroutine wbea_classic(obs,error)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>wbea_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the BEAm-switching section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  integer(kind=data_length) :: slen
  integer(kind=4) :: iwork(class_sec_bea_len)
  !
  slen = class_sec_bea_len
  call fileout%conv%writ%r4(obs%head%bea%cazim,iwork(1),4)
  call fileout%conv%writ%i4(obs%head%bea%btype,iwork(5),1)
  !
  call wsec(obs%desc,class_sec_bea_id,slen,iwork,error)
  if (error)  return
  !
end subroutine wbea_classic
!
subroutine wuser_classic(obs,error)
  use gbl_message
  use classic_api
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>wuser_classic
  use class_buffer
  use class_common
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Transfer the User Section to buffer 'uwork'
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs    !
  logical,           intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='WUSER'
  integer(kind=data_length) :: len  ! Length of section
  integer(kind=4) :: iuser
  !
  if (obs%user%n.le.0)  return
  !
  if (fileout%conv%code.ne.0) then
    call class_message(seve%w,rname,  &
      'Output file is not in native format: skipping User Section')
    return
  endif
  !
  len = 1  ! nuser: 1 word
  do iuser=1,obs%user%n
    ! owner: 3 words
    ! title: 3 words
    ! version: 1 word
    ! ndata: 1 word
    ! data: 'ndata' words
    len = len+8+obs%user%sub(iuser)%ndata
  enddo
  !
  call reallocate_uwork(len,.false.,error)
  if (error)  return
  !
  call fileout%conv%writ%i4 (obs%user%n,uwork(1),1)
  unext = 2
  do iuser=1,obs%user%n
    call fileout%conv%writ%cc(obs%user%sub(iuser)%owner,uwork(unext),3)
    unext = unext+3
    call fileout%conv%writ%cc(obs%user%sub(iuser)%title,uwork(unext),3)
    unext = unext+3
    call fileout%conv%writ%i4(obs%user%sub(iuser)%version,uwork(unext),1)
    unext = unext+1
    call fileout%conv%writ%i4(obs%user%sub(iuser)%ndata,uwork(unext),1)
    unext = unext+1
    ! ZZZ Should skip the following step if the output format is not the
    ! input format.
    call bytoby(obs%user%sub(iuser)%data,  &
                uwork(unext),  &
                obs%user%sub(iuser)%ndata*4)
    unext = unext+obs%user%sub(iuser)%ndata
  enddo
  !
  ! Now write the section
  call wsec(obs%desc,class_sec_user_id,len,uwork,error)
  !
end subroutine wuser_classic
!
subroutine wassoc_classic(obs,error)
  use gbl_format
  use gbl_message
  use classic_api
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>wassoc_classic
  use class_common
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  ! Write the ASSOCiated array section
  !----------------------------------------------------------------------
  type(observation), intent(inout), target :: obs    ! Inout: obs%desc is updated
  logical,           intent(inout)         :: error  !
  ! Local
  character(len=*), parameter :: rname='WASSOC'
  integer(kind=data_length) :: slen,inext
  integer(kind=4), allocatable :: iwork(:)
  integer(kind=1), allocatable :: by(:)
  integer(kind=4) :: ier,nchan,iarray,ibyte,idata,ndata,nbytes
  integer(kind=4) :: pos,nwords,dim2,idime
  character(len=message_length) :: mess
  !
  if (obs%assoc%n.le.0)  return
  !
  nchan = obs_nchan(obs%head)
  slen = 1  ! nassoc: 1 word
  do iarray=1,obs%assoc%n
    ! Sanity check
    if (obs%assoc%array(iarray)%dim1.ne.nchan) then
      write(mess,'(3A,I0,A,I0)')  &
        'Associated Array ''',trim(obs%assoc%array(iarray)%name),  &
        ''' has invalid Nchan: ',obs%assoc%array(iarray)%dim1,' instead of ',nchan
      call class_message(seve%e,rname,mess)
      error = .true.
      return
    endif
    ! fmt: 1 word,  dim2: 1 word, name: 3 words, unit: 3 words
    slen = slen+8
    ! bad + data = ndata * number of words per item
    dim2 = max(1,obs%assoc%array(iarray)%dim2)
    select case (obs%assoc%array(iarray)%fmt)
    case (fmt_r4,fmt_i4)
      nwords = 1+nchan*dim2
    case (fmt_by)
      nbytes = 1+nchan*dim2
      nwords = (nbytes+3)/4
    case (fmt_b2)
      nbytes = (1+nchan*dim2+3)/4
      nwords = (nbytes+3)/4
    case default
      call class_message(seve%e,rname,'Kind of data not yet implemented')
      error = .true.
      return
    end select
    slen = slen+nwords
  enddo
  !
  allocate(iwork(slen),stat=ier)
  if (failed_allocate(rname,'iwork',ier,error))  return
  !
  call fileout%conv%writ%i4(obs%assoc%n,iwork(1),1)
  inext = 2
  do iarray=1,obs%assoc%n
    call fileout%conv%writ%cc(obs%assoc%array(iarray)%name,iwork(inext),3)
    inext = inext+3
    call fileout%conv%writ%cc(obs%assoc%array(iarray)%unit,iwork(inext),3)
    inext = inext+3
    call fileout%conv%writ%i4(obs%assoc%array(iarray)%dim2,iwork(inext),1)
    inext = inext+1
    call fileout%conv%writ%i4(obs%assoc%array(iarray)%fmt, iwork(inext),1)
    inext = inext+1
    !
    dim2 = max(1,obs%assoc%array(iarray)%dim2)
    ndata = nchan*dim2
    select case (obs%assoc%array(iarray)%fmt)
    case (fmt_r4)
      call fileout%conv%writ%r4(obs%assoc%array(iarray)%badr4,iwork(inext),1)
      inext = inext+1
      call fileout%conv%writ%r4(obs%assoc%array(iarray)%r4,iwork(inext),ndata)
      inext = inext+ndata
      !
    case (fmt_i4)
      call fileout%conv%writ%i4(obs%assoc%array(iarray)%badi4,iwork(inext),1)
      inext = inext+1
      call fileout%conv%writ%i4(obs%assoc%array(iarray)%i4,iwork(inext),ndata)
      inext = inext+ndata
      !
    case (fmt_by)  ! 1 byte = 1 value per byte
      nbytes = 1+ndata
      allocate(by(nbytes))
      ! ZZZ use i4toi1_fini in order to trap overflows
      by(1) = obs%assoc%array(iarray)%badi4
      do idime=1,dim2
        by(2+nchan*(idime-1):1+nchan*idime) =  &
          obs%assoc%array(iarray)%i4(:,idime)  ! I*4 in memory to single byte on disk
      enddo
      call bytoby(by,iwork(inext),nbytes)
      deallocate(by)
      nwords = (nbytes+3)/4
      inext = inext+nwords
      !
    case (fmt_b2)  ! 2 bits per value = 4 values per byte
      nbytes = (1+ndata+3)/4
      allocate(by(nbytes))  ! Allocate a byte buffer
      ! NB: buffer is not initialized e.g. with zeroes
      ! Encode the bytes buffer
      ibyte = 1
      pos = 1
      call i4_to_twobit(obs%assoc%array(iarray)%badi4,by(ibyte),pos)
      do idime=1,dim2
        do idata=1,nchan
          if (pos.eq.4) then
            ibyte = ibyte+1
            pos = 1
          else
            pos = pos+1
          endif
          call i4_to_twobit(obs%assoc%array(iarray)%i4(idata,idime),by(ibyte),pos)
        enddo
      enddo
      call bytoby(by,iwork(inext),nbytes)
      deallocate(by)
      nwords = (nbytes+3)/4
      inext = inext+nwords
      !
    case default
      call class_message(seve%e,rname,'Kind of data not yet implemented')
      error = .true.
      return
    end select
  enddo
  !
  ! Now write the section
  call wsec(obs%desc,class_sec_assoc_id,slen,iwork,error)
  ! if (error)  continue
  !
  deallocate(iwork)
  !
contains
  !
  subroutine i4_to_twobit(i4,by,pos)
    integer(kind=4), intent(in)    :: i4   ! I*4 value
    integer(kind=1), intent(inout) :: by   ! 8-bits buffer
    integer(kind=4), intent(in)    :: pos  ! The 2-bits position in the 8-bits buffer (1 ... 4)
    ! Local
    integer(kind=4), parameter :: bi4val=0    ! Position of value bit (we use only one)
    integer(kind=4) :: bb2sign,bb2val
    !
    ! How signed integers work (I*1 example):
    !  I*1 range is -128 to +127  (256 values):
    !  bit num 76543210
    !   -128   10000000
    !   -127   10000001
    !   -126   10000010
    !       ...
    !     -2   11111110
    !     -1   11111111
    !      0   00000000
    !      1   00000001
    !       ...
    !    127   01111111
    ! i.e. the highest bit encodes the sign (for a signed integer), and
    ! the remaining bits encode the value as a power of 2. By symetry for
    ! our 2 bits signed integers:
    !   Range is -2 to +1 (4 values)
    !   bit num 10
    !     -2    10
    !     -1    11
    !      0    00
    !      1    01
    !
    bb2val = 2*(pos-1)
    bb2sign = bb2val+1
    !
    if (i4.lt.0) then
      by = ibset(by,bb2sign)
    else
      by = ibclr(by,bb2sign)
    endif
    if (btest(i4,bi4val)) then
      by = ibset(by,bb2val)
    else
      by = ibclr(by,bb2val)
    endif
  end subroutine i4_to_twobit
  !
end subroutine wassoc_classic
!
subroutine wherschel_classic(obs,error)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>wherschel_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the HERSCHEL Section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='WHERSCHEL'
  integer(kind=data_length) :: slen
  integer(kind=4) :: iwork(class_sec_her_len)
  call fileout%conv%writ%i8(obs%head%her%obsid,     iwork( 1), 1)  !  1- 2
  call fileout%conv%writ%cc(obs%head%her%instrument,iwork( 3), 2)  !  3- 4
  call fileout%conv%writ%cc(obs%head%her%proposal,  iwork( 5), 6)  !  5-10
  call fileout%conv%writ%cc(obs%head%her%aor,       iwork(11),17)  ! 11-27
  call fileout%conv%writ%i4(obs%head%her%operday,   iwork(28), 1)  ! 28-28
  call fileout%conv%writ%cc(obs%head%her%dateobs,   iwork(29), 7)  ! 29-35
  call fileout%conv%writ%cc(obs%head%her%dateend,   iwork(36), 7)  ! 36-42
  call fileout%conv%writ%cc(obs%head%her%obsmode,   iwork(43),10)  ! 43-52
  call fileout%conv%writ%r4(obs%head%her%vinfo,     iwork(53), 2)  ! 53-54
  call fileout%conv%writ%r8(obs%head%her%posangle,  iwork(55), 5)  ! 55-64
  call fileout%conv%writ%r4(obs%head%her%etamb,     iwork(65), 4)  ! 65-68
  call fileout%conv%writ%cc(obs%head%her%tempscal,  iwork(69), 2)  ! 69-70
  call fileout%conv%writ%r8(obs%head%her%lodopave,  iwork(71), 1)  ! 71-72
  call fileout%conv%writ%r4(obs%head%her%gim0,      iwork(73), 6)  ! 73-78
  call fileout%conv%writ%cc(obs%head%her%datehcss,  iwork(79), 7)  ! 79-85
  call fileout%conv%writ%cc(obs%head%her%hcssver,   iwork(86), 6)  ! 86-91
  call fileout%conv%writ%cc(obs%head%her%calver,    iwork(92), 4)  ! 92-95
  call fileout%conv%writ%r4(obs%head%her%level,     iwork(96), 1)  ! 96-96
  !
  slen = class_sec_her_len
  call wsec(obs%desc,class_sec_her_id,slen,iwork,error)
  if (error)  return
  !
end subroutine wherschel_classic
!
subroutine cwsec_classic(obs,scode,error)
  use gildas_def
  use gbl_constant
  use gbl_message
  use classic_api
  use classcore_interfaces, except_this=>cwsec_classic
  use class_types
  use class_buffer
  use class_common
  !---------------------------------------------------------------------
  ! @ private
  !   Write the appropriate section from the input observation to the
  ! associated entry in the output file, taking care of conversion of
  ! the section data from System data type to File data type.
  !  OBS%DESC: updated
  !  OBS%HEAD: input
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs    ! Header to be written
  integer(kind=4),   intent(in)    :: scode  ! Code of header section
  logical,           intent(out)   :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='CWSEC'
  integer(kind=data_length) :: len  ! Length of section
  integer(kind=4) :: len4
  integer(kind=4) :: jx(4)
  real(kind=8) :: xx(2)
  equivalence (xx,jx)
  !
  error = .false.
  !
  select case(scode)
! case (class_sec_gen_id)
    ! Moved to wgen_classic. Same to be done for the other sections
    !
! case (class_sec_pos_id)
    ! Moved to wpos_classic. Same to be done for the other sections
    !
! case (class_sec_spe_id)
    ! Moved to wspec_classic. Same to be done for the other sections
    !
  case (class_sec_com_id)
    len = (obs%head%com%ltext+3)/4  ! next longword align
    len4 = len
    call fileout%conv%writ%cc (obs%head%com%ctext,iwork(1),len4)
    !
! case (class_sec_bas_id)
    ! Moved to wbas_classic. Same to be done for the other sections
    !
  case (class_sec_his_id)
    len = 1+2*obs%head%his%nseq
    ! All integers
    call fileout%conv%writ%i4 (obs%head%his%nseq,iwork(1),1)
    if (obs%head%his%nseq.eq.0) return
    call fileout%conv%writ%i4 (obs%head%his%start,iwork(2),obs%head%his%nseq)
    call fileout%conv%writ%i4 (obs%head%his%end,iwork(2+obs%head%his%nseq),obs%head%his%nseq)
    !
! case (class_sec_plo_id)
    ! Moved to wplo_classic. Same to be done for the other sections
    !
! case (class_sec_swi_id)
    ! Moved to wswi_classic. Same to be done for the other sections
    !
! case (class_sec_gau_id)
    ! Moved to wgau_classic. Same to be done for the other sections
    !
! case (class_sec_dri_id)
    ! Moved to wdri_classic. Same to be done for the other sections
    !
! case (class_sec_bea_id)
    ! Moved to wbea_classic. Same to be done for the other sections
    !
! case (class_sec_she_id)
    ! Moved to wshe_classic. Same to be done for the other sections
    !
! case (class_sec_hfs_id)
    ! Moved to whfs_classic. Same to be done for the other sections
    !
! case (class_sec_abs_id)
    ! Moved to wabs_classic. Same to be done for the other sections
    !
  case (class_sec_cal_id)
    len = class_sec_cal_len
    call fileout%conv%writ%r4 (obs%head%cal%beeff,iwork,class_sec_cal_len)
    call fileout%conv%writ%i4 (obs%head%cal%cmode,iwork(14),1)
    !
  case (class_sec_poi_id)
    len = class_sec_poi_len
    call fileout%conv%writ%i4 (obs%head%poi%nline,iwork(1),1)
    call fileout%conv%writ%r4 (obs%head%poi%sigba,iwork(2),class_sec_poi_len-1)
    !
  case (class_sec_sky_id)
    len = 10 + 2*obs%head%sky%nsky+obs%head%sky%nchop+obs%head%sky%ncold
    call fileout%conv%writ%cc (obs%head%sky%line,iwork(1),3)
    call fileout%conv%writ%r8 (obs%head%sky%restf,xx,2)
    iwork(4) = jx(1)
    iwork(5) = jx(2)
    iwork(6) = jx(3)
    iwork(7) = jx(4)
    call fileout%conv%writ%i4 (obs%head%sky%nsky,iwork(8),3)
    if (obs%head%sky%nsky.gt.0) call fileout%conv%writ%r4 (obs%head%sky%elev,iwork(11),obs%head%sky%nsky)
    if (obs%head%sky%nsky.gt.0) call fileout%conv%writ%r4 (obs%head%sky%emiss,iwork(11+obs%head%sky%nsky),obs%head%sky%nsky)
    if (obs%head%sky%nchop.gt.0) &
        call fileout%conv%writ%r4 (obs%head%sky%chopp,iwork(11+2*obs%head%sky%nsky),obs%head%sky%nchop)
    if (obs%head%sky%ncold.gt.0) &
        call fileout%conv%writ%r4 (obs%head%sky%cold,iwork(11+2*obs%head%sky%nsky+obs%head%sky%nchop),   &
        &      obs%head%sky%ncold)
    !
  case (class_sec_desc_id)  ! Data Descriptor Section
    len = class_sec_desc_len
    len4 = class_sec_desc_len
    call fileout%conv%writ%i4 (obs%head%des%ndump,iwork,len4)
    !
  case default
    call class_message(seve%e,rname,'Unknown section')
    error = .true.
    return
  end select
  !
  ! Now write the section
  call wsec(obs%desc,scode,len,iwork,error)
end subroutine cwsec_classic
!
subroutine cwsec_xcoo(set,obs,error)
  use gildas_def
  use gbl_constant
  use classic_api
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>cwsec_xcoo
  use class_types
  use class_buffer
  use class_common
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  !  Convert section from System data type to File data type
  ! Note that the external functions do not allow data conversion
  ! => call directly r8tor4 !! This WAS A BUG... MUST CALL THROUGH AN
  !  INTERMEDIATE ARRAY
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(observation),   intent(inout) :: obs    !
  logical,             intent(out)   :: error  ! Logical error flag
  ! Local
  integer(kind=data_length) :: len  ! Length of section
  real(kind=4), allocatable :: aaa4(:)
  real(kind=8), allocatable :: aaa8(:)
  integer(kind=4) :: ier,nelem
  !
  if (obs%head%gen%kind.eq.kind_spec) then
    nelem = obs%head%spe%nchan
  elseif (obs%head%gen%kind.eq.kind_cont) then
    nelem = obs%head%dri%npoin
  endif
  ! Put a test on Real*8 values...
  if (set%write_r8) then
    len = 2*nelem+1
  else
    len = nelem+1
  endif
  !
  if (len.gt.jlen) then
    if (jlen.ne.0) deallocate(jwork,stat=ier)
    allocate (jwork(len),stat=ier)
    jlen = len
  endif
  !
  ! Write XUNIT
  call fileout%conv%writ%i4(obs%head%gen%xunit,jwork,1)
  !
  ! Write DATAV content to the file. Note that DATAV can be Velocities,
  ! Frequencies, and so on, depending on XUNIT and what user has put in.
  ! DATAX is not used because it relies on SET UNIT. See crsec_xcoo for
  ! symetry.
  if (set%write_r8) then
    if (set%verbose) write(*,*) 'CWSEC_XCOO: Writing REAL*8 abscissa'
    if (xdata_kind.eq.4) then
      allocate(aaa8(nelem))
      aaa8(1:nelem) = obs%datav(1:nelem)  ! Upcast to R*8
      call fileout%conv%writ%r8(aaa8,jwork(2),nelem)
      deallocate(aaa8)
    else
      call fileout%conv%writ%r8(obs%datav,jwork(2),nelem)
    endif
  else
    if (set%verbose) write(*,*) 'CWSEC_XCOO: Writing REAL*4 abscissa'
    if (xdata_kind.eq.4) then
      call fileout%conv%writ%r4(obs%datav,jwork(2),nelem)
    else
      allocate(aaa4(nelem))
      aaa4(1:nelem) = obs%datav(1:nelem)  ! Downcast to R*4
      call fileout%conv%writ%r4(aaa4,jwork(2),nelem)
      deallocate(aaa4)
    endif
  endif
  !
  ! Now write the section
  error = .false.
  call wsec(obs%desc,class_sec_xcoo_id,len,jwork,error)
end subroutine cwsec_xcoo
