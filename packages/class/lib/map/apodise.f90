!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine dosmoo(raw,gwe,nc,nx,ny,map,mapx,mapy,sup,cell)
  !---------------------------------------------------------------------
  ! @ private
  ! Smooth an input data cube in VLM along L and M by convolution
  ! with a gaussian...
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)  :: nc,nx,ny       ! Cube size
  real(kind=4),    intent(in)  :: raw(nc,nx,ny)  ! Raw cube
  real(kind=4),    intent(in)  :: gwe(nx,ny)     ! Gridded weights Not used ???? JP
  real(kind=4),    intent(out) :: map(nc,nx,ny)  ! Smoothed cube
  real(kind=4),    intent(in)  :: mapx(nx)       ! Arrays of gridded coordinates
  real(kind=4),    intent(in)  :: mapy(ny)       ! Arrays of gridded coordinates
  real(kind=4),    intent(in)  :: sup(2)         ! Convolving function support size in user unit
  real(kind=4),    intent(in)  :: cell(2)        ! Pixel size in user unit
  ! Local
  integer :: yfirs,ylast          ! Range to be considered
  integer :: xfirs,xlast          ! Range to be considered
  integer :: ix,iy,ic
  integer :: jx,jy                ! X coord, Y coord location in RAW
  real :: result,weight
  real :: u,v,du,dv,um,up,vm,vp,dx,dy
  !
  dx = abs(mapx(2)-mapx(1))
  dy = abs(mapy(2)-mapy(1))
  !
  ! Loop on Y rows
  do iy=1,ny
     v = mapy(iy)
     vm = v-sup(2)
     vp = v+sup(2)
     yfirs = max(1,nint((iy-sup(2)/dy)))
     ylast = min(ny,nint((iy+sup(2)/dy)))
     !
     ! Initialize X column
     do ix=1,nx
        do ic=1,nc
           map(ic,ix,iy) = 0.0
        enddo
     enddo
     !
     ! Loop on X cells
     if (yfirs.le.ylast) then
        do ix=1,nx
           u = mapx(ix)
           um = u-sup(1)
           up = u+sup(1)
           weight = 0.0
           xfirs = max(1,nint(ix-sup(1)/dx))
           xlast = min(nx,nint(ix+sup(1)/dx))
           !
           ! Do while in X cell
           if (xfirs.le.xlast) then
              do jy=yfirs,ylast
                 dv = (v-mapy(jy))/cell(2)
                 do jx=xfirs,xlast
                    du = (u-mapx(jx))/cell(1)
                    call convol(du,dv,result)
                    if (result.ne.0.0) then
                       weight = weight + result
                       do ic=1,nc
                          map(ic,ix,iy) = map(ic,ix,iy) + raw(ic,jx,jy)*result
                       enddo
                    endif
                 enddo
              enddo
              !
              ! Normalize weight only in cells where some data exists...
              if (weight.ne.0.0) then
                 weight = 1.0/weight
                 do ic=1,nc
                    map (ic,ix,iy) = map(ic,ix,iy)*weight
                 enddo
              endif
           endif
        enddo
     endif
  enddo
end subroutine dosmoo
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine doapod(xmin,xmax,ymin,ymax,tole,beam, &
     nc,nx,ny,map,raw,mapx,mapy,gweight,wmin)
  !---------------------------------------------------------------------
  ! @ private
  ! Replace the original (RAW) data by an apodized version
  !   - outside of the sampled region (map edges to smoothly go to zero)
  !   - and where the weight is below the minimum value (bad data points)
  !---------------------------------------------------------------------
  real(kind=4),    intent(in)    :: xmin,xmax       ! Boundary of sampled region
  real(kind=4),    intent(in)    :: ymin,ymax       ! Boundary of sampled region
  real(kind=4),    intent(in)    :: tole            ! X-Y position tolerance
  real(kind=4),    intent(in)    :: beam            ! Beam size
  integer(kind=4), intent(in)    :: nc,nx,ny        ! Cube size
  real(kind=4),    intent(in)    :: map(nc,nx,ny)   ! Smoothed cube
  real(kind=4),    intent(inout) :: raw(nc,nx,ny)   ! Original cube
  real(kind=4),    intent(in)    :: mapx(nx)        ! Arrays of gridded coordinates
  real(kind=4),    intent(in)    :: mapy(ny)        ! Arrays of gridded coordinates
  real(kind=4),    intent(in)    :: gweight(nx,ny)  ! Gridded weights
  real(kind=4),    intent(in)    :: wmin            ! Minimum weight
  ! Local
  integer :: ic,ix,iy
  real :: lobe,apod,disty,distx,pi
  !
  pi = acos(-1.0)
  write(6,*) 'Min-Max ',xmin,xmax,ymin,ymax
  write(6,*) 'Beam, Tolerance, Increment ',beam*180*3600/pi,tole*180*3600/pi   &
       &    ,(mapx(1)-mapx(2))*180*3600/pi,'"'
  ! Use a beam size twice larger
  lobe = log(2.0)/beam**2
  do iy=1,ny
     if (mapy(iy).le.ymin-tole) then
        disty = ymin-mapy(iy)
     elseif (mapy(iy).ge.ymax+tole) then
        disty = mapy(iy)-ymax
     else
        disty = 0.0
     endif
     do ix=1,nx
        if (mapx(ix).le.xmin-tole) then
           distx = xmin-mapx(ix)
        elseif (mapx(ix).ge.xmax+tole) then
           distx = mapx(ix)-xmax
        else
           distx = 0.0
        endif
        apod = (distx**2+disty**2)*lobe
        if (apod.gt.80) then
           do ic=1,nc
              raw(ic,ix,iy) = 0.0
           enddo
        elseif  (apod.ne.0.0) then
           apod = exp(-apod)
           do ic=1,nc
              raw(ic,ix,iy) = map(ic,ix,iy)*apod
           enddo
        elseif (gweight(ix,iy).lt.wmin) then
           do ic=1,nc
              raw(ic,ix,iy) = map(ic,ix,iy)
           enddo
        endif
     enddo
  enddo
end subroutine doapod
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
function nearest_power_of_two(n)
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  integer(kind=4) :: nearest_power_of_two  ! Function value on return
  integer(kind=4), intent(in) :: n          !
  ! Local
  real(kind=4) :: r
  integer(kind=4) :: i
  !
  r = real(n)
  r = log(r)/log(2.0)
  i = int(r)
  if (i.ne.r) i = i+1
  nearest_power_of_two = 2**i
end function nearest_power_of_two
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
