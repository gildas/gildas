module classmap_interfaces_public
  interface
    subroutine load_map(error)
      use xymap_def
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! MAP initialization routine:
      ! MAP language is loaded.
      !---------------------------------------------------------------------
      logical, intent(out) :: error     ! Error status
    end subroutine load_map
  end interface
  !
end module classmap_interfaces_public
