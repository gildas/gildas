subroutine dosdft(hout,beam,diam,f,nx,ny,dx,dy)
  use gildas_def
  use image_def
  use phys_const
  !---------------------------------------------------------------------
  ! @ private
  ! Computes Inverse of FT of single-dish beam
  ! (uses a Gaussian truncated at dish size)
  ! Not used in this version, since we have the explicit convolution
  ! kernel in a sampled way
  !---------------------------------------------------------------------
  type (gildas)                  :: hout      ! Header of output cube
  real(kind=4),    intent(in)    :: beam      ! Beam size in radian
  real(kind=4),    intent(in)    :: diam      ! Diameter in meter
  integer(kind=4), intent(in)    :: nx,ny     ! Map size
  real(kind=4),    intent(out)   :: f(nx,ny)  ! Multiplication factor
  real(kind=8),    intent(inout) :: dx,dy     !
  ! Local
  integer :: i,j, ii, jj
  real :: a, b, xx, yy
  !
  dx = clight_mhz/hout%gil%freq/(hout%gil%convert(3,1)*hout%gil%dim(1))
  dy = clight_mhz/hout%gil%freq/(hout%gil%convert(3,2)*hout%gil%dim(2))
  ! equivalent beam area in square pixels ...
  a = abs(4*alog(2.)/pi/beam**2*hout%gil%convert(3,1)*hout%gil%convert(3,2))
  !
  b = (pi*beam/2/clight_mhz*hout%gil%freq)**2/alog(2.)
  do j = 1, ny
     jj = mod(j-1+ny/2,ny)-ny/2
     yy = ( jj*dy )**2
     do i = 1, nx
        ii = mod(i-1+nx/2,nx)-nx/2
        xx = ( ii*dx )**2
        if (xx+yy.le.diam**2) then
           f(i,j) = exp(b*(xx+yy))*a
        else
           f(i,j) = 0.0
        endif
     enddo
  enddo
end subroutine dosdft
!
subroutine sdcorr(z,f,nxy)
  !---------------------------------------------------------------------
  ! @ private
  ! Correct for single dish beam
  !---------------------------------------------------------------------
  complex,         intent(inout) :: z(*)  !
  real(kind=4),    intent(in)    :: f(*)  !
  integer(kind=4), intent(in)    :: nxy   ! Problem size
  ! Local
  integer :: i
  !
  do i = 1, nxy
     z(i) = z(i)*f(i)
  enddo
end subroutine sdcorr
!
subroutine retocm(r,z,nx,ny)
  !---------------------------------------------------------------------
  ! @ private
  ! Real to complex conversion
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)  :: nx,ny     !
  real(kind=4),    intent(in)  :: r(nx,ny)  !
  complex,         intent(out) :: z(nx,ny)  !
  ! Local
  integer :: i, j, ii, jj
  !
  do i=1, nx
     ii = mod(i+nx/2-1,nx)+1
     do j=1, ny
        jj = mod(j+ny/2-1,ny)+1
        z(ii,jj) = r(i,j)
     enddo
  enddo
end subroutine retocm
!
subroutine cmtore(z,r,nx,ny)
  !---------------------------------------------------------------------
  ! @ private
  ! Complex to real conversion
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)  :: nx,ny     !
  complex,         intent(in)  :: z(nx,ny)  !
  real(kind=4),    intent(out) :: r(nx,ny)  !
  ! Local
  integer :: i, j, ii, jj
  !
  do i=1, nx
     ii = mod(i+nx/2-1,nx)+1
     do j=1, ny
        jj = mod(j+ny/2-1,ny)+1
        r(ii,jj) = z(i,j)
     enddo
  enddo
end subroutine cmtore
!
! UNUSED, NAME TOO GENERIC:
! subroutine mask(a,w,n,wm,fact)
!   !---------------------------------------------------------------------
!   ! @ private
!   !
!   ! Rescale data and truncate below the minimum weight
!   !---------------------------------------------------------------------
!   integer(kind=4), intent(in)    :: n     ! Number of values
!   real(kind=4),    intent(inout) :: a(n)  ! Data values
!   real(kind=4),    intent(in)    :: w(n)  ! Input weights
!   real(kind=4),    intent(in)    :: wm    ! Minimum weight
!   real(kind=4),    intent(in)    :: fact  ! Scale factor
!   ! Local
!   integer :: i
!   !
!   do i=1,n
!      if (w(i).lt.wm) then
!         a(i) = 0.0
!      else
!         a(i) = fact*a(i)
!      endif
!   enddo
! end subroutine mask
!
subroutine grdtab(n,buff,bias,corr)
  use phys_const
  !---------------------------------------------------------------------
  ! @ private
  ! Compute fourier transform of gridding function
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)  :: n        ! Number of pixels, assuming center on N/2+1
  real(kind=4),    intent(in)  :: buff(*)  ! Gridding function, tabulated every 1/100th of a pixel
  real(kind=4),    intent(in)  :: bias     ! Center of gridding function
  real(kind=4),    intent(out) :: corr(n)  ! Gridding correction FT
  ! Local
  integer :: i,j,m,l
  real :: kw,kx
  !
  do j=1,n
     corr(j) = 0.0
  enddo
  m = n/2+1
  kw = 0.01 * pi / m
  l = 2*bias+1
  do i=1,l
     if (buff(i).ne.0.0) then
        kx  = kw*(float(i)-bias)
        do j=1,n
           corr(j) = corr(j) + buff(i) * cos(kx*float(j-m))
        enddo
     endif
  enddo
end subroutine grdtab
!
subroutine dogrid(corr,corx,cory,nx,ny,beam)
  !---------------------------------------------------------------------
  ! @ private
  ! Compute grid correction array, with normalisation of beam to 1.0 at
  ! maximum pixel
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)  :: nx,ny        ! Map size
  real(kind=4),    intent(out) :: corr(nx,ny)  ! Final grid correction and beam normalization
  real(kind=4),    intent(in)  :: corx(nx)     ! X Grid correction
  real(kind=4),    intent(in)  :: cory(ny)     ! Y Grid correction
  real(kind=4),    intent(in)  :: beam(nx,ny)  ! Denormalized Beam
  ! Local
  real :: x
  integer :: i,j
  !
  x = corx(nx/2+1)*cory(ny/2+1)/beam(nx/2+1,ny/2+1)
  do j=1,ny
     do i=1,nx
        corr(i,j) = x/(corx(i)*cory(j))
     enddo
  enddo
end subroutine dogrid
!
subroutine docorr(map,corr,nd)
  !---------------------------------------------------------------------
  ! @ private
  ! Apply grid correction to map
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)    :: nd        ! Map size
  real(kind=4),    intent(inout) :: map(nd)   ! Map to be corrected
  real(kind=4),    intent(in)    :: corr(nd)  ! Grid correction
  ! Local
  integer :: i
  !
  do i=1,nd
     map(i) = map(i)*corr(i)
  enddo
end subroutine docorr
!
subroutine dotrunc(hout,diam,f,nx,ny)
  use gildas_def
  use image_def
  use phys_const
  !---------------------------------------------------------------------
  ! @ private
  ! Truncate the Fourier Transform beyond the physical disk size
  !---------------------------------------------------------------------
  type (gildas)                :: hout      ! Header of output data cube
  real(kind=4),    intent(in)  :: diam      ! Diameter in meter
  integer(kind=4), intent(in)  :: nx,ny     ! Map size
  real(kind=4),    intent(out) :: f(nx,ny)  ! Multiplication factor
  ! Local
  real(kind=8) :: dx,dy
  integer :: i,j, ii, jj
  real :: xx, yy
  !
  dx = clight_mhz/hout%gil%freq/(hout%gil%convert(3,1)*hout%gil%dim(1))
  dy = clight_mhz/hout%gil%freq/(hout%gil%convert(3,2)*hout%gil%dim(2))
  do j = 1, ny
     jj = mod(j-1+ny/2,ny)-ny/2
     yy = ( jj*dy )**2
     do i = 1, nx
        ii = mod(i-1+nx/2,nx)-nx/2
        xx = ( ii*dx )**2
        if (xx+yy.gt.diam**2) then
           f(i,j) = 0.0
        endif
     enddo
  enddo
end subroutine dotrunc
