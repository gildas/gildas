!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module classmap_interfaces
  !---------------------------------------------------------------------
  ! CLASSMAP interfaces, all kinds (private or public). Do not use directly
  ! out of the library.
  !---------------------------------------------------------------------
  !
  use classmap_interfaces_public
  use classmap_interfaces_private
  !
end module classmap_interfaces
!
module classmap_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! CLASSMAP dependencies public interfaces. Do not use out of the library.
  !---------------------------------------------------------------------
  !
  use gkernel_interfaces
!  use astro_interfaces_public
!  use classic_interfaces_public
end module classmap_dependencies_interfaces
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
