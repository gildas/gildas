!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine doconv(ncol,nlin,table,xp,yp,we,gwe,nx,ny,map,mapx,mapy,sup,  &
  cell,conv,time,error)
  use gildas_def
  use gbl_message
  use classmap_dependencies_interfaces
  use classmap_interfaces, except_this=>doconv
  use xymap_types
  !---------------------------------------------------------------------
  ! @ private
  !   Grid (i.e. convolve data from input table columns by a gridding
  ! function)
  !---------------------------------------------------------------------
  integer(kind=index_length), intent(in)  :: ncol      ! Number of table column / Nchan
  integer(kind=index_length), intent(in)  :: nlin      ! Number of table line
  real(kind=4),         intent(in)    :: table(ncol,nlin)  ! The table array
  real(kind=4),         intent(in)    :: xp(nlin)          ! X coordinates
  real(kind=4),         intent(in)    :: yp(nlin)          ! Y coordinates
  real(kind=4),         intent(in)    :: we(nlin)          ! The table weights
  integer(kind=index_length), intent(in)  :: nx,ny         ! Map size
  real(kind=4),         intent(out)   :: gwe(nx,ny)        ! Gridded weights
  real(kind=4),         intent(out)   :: map(ncol,nx,ny)   ! Gridded cube
  real(kind=4),         intent(in)    :: mapx(nx)          ! Arrays of gridded coordinates
  real(kind=4),         intent(in)    :: mapy(ny)          ! Arrays of gridded coordinates
  real(kind=4),         intent(in)    :: sup(2)            ! Convolving function support size in user unit
  real(kind=4),         intent(in)    :: cell(2)           ! Pixel size in user unit
  type(xymap_conv2d_t), intent(in)    :: conv              ! Buffer for the convolution kernel
  type(time_t),         intent(inout) :: time              ! Timer
  logical,              intent(inout) :: error             !
  ! Local
  integer(kind=index_length) :: i,ifirs,ilast  ! Range to be considered
  integer(kind=index_length) :: ix,iy,ic
  real(kind=4) :: result,weight
  real(kind=4) :: u,v,du,dv,um,up,vm,vp
  character(len=*), parameter :: rname='XY_MAP'
  !
  call class_message(seve%d,rname,'Convolving')
  !
  ! Loop on Y rows
  ifirs = 1
  do iy=1,ny
     call class_controlc(rname,error)
     if (error)  return
     !
     v = mapy(iy)
     vm = v-sup(2)
     vp = v+sup(2)
     !
     ! Optimized dichotomic search, taking into account the fact that
     ! mapy is an ordered array
     call findr(yp,nlin,vm,ifirs)
     ilast = ifirs
     call findr(yp,nlin,vp,ilast)
     ilast = ilast-1
     !
     ! Initialize X column
     do ix=1,nx
        do ic=1,ncol
           map(ic,ix,iy) = 0.0
        enddo
     enddo
     !
     ! Loop on X cells
     if (ilast.ge.ifirs) then
        do ix=1,nx
           u = mapx(ix)
           um = u-sup(1)
           up = u+sup(1)
           weight = 0.0
           !
           ! Do while in X cell
           do i=ifirs,ilast
              if (xp(i).ge.um .and. xp(i).le.up) then
                 du = (u-xp(i))/cell(1)
                 dv = (v-yp(i))/cell(2)
                 call convol(conv,du,dv,result)
                 if (result.ne.0.0) then
                    result = result*we(i)
                    weight = weight + result
                    do ic=1,ncol
                       map (ic,ix,iy) = map (ic,ix,iy) + table(ic,i)*result
                    enddo
                 endif
              endif
           enddo
           !
           ! Normalize weight only in cells where some data exists...
           gwe(ix,iy) = weight
           if (weight.ne.0.0) then
              weight = 1.0/weight
              do ic=1,ncol
                 map (ic,ix,iy) = map(ic,ix,iy)*weight
              enddo
           endif
        enddo  ! IX
     endif
     !
     call gtime_current(time)
  enddo  ! IY
end subroutine doconv
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine findr(xx,nv,xlim,nlim)
  use gildas_def
  !---------------------------------------------------------------------
  ! @ private
  !  Find nlim such as
  !    xx(nlim-1) < xlim < xx(nlim)
  !  for input data ordered, retrieved from memory
  !  Assume that nlim is already preset so that xx(ic,nlim-1) < xlim
  !---------------------------------------------------------------------
  integer(kind=index_length), intent(in)    :: nv
  real(kind=4),               intent(in)    :: xx(nv)
  real(kind=4),               intent(in)    :: xlim
  integer(kind=index_length), intent(inout) :: nlim
  ! Local
  integer(kind=index_length) :: ninf,nsup,nmid
  !
  if (nlim.gt.nv) return
  if (xx(nlim).gt.xlim) then
     return
  elseif (xx(nv).lt.xlim) then
     nlim = nv+1
     return
  endif
  !
  ninf = nlim
  nsup = nv
  do while(nsup.gt.ninf+1)
     nmid = (nsup + ninf)/2
     if (xx(nmid).lt.xlim) then
        ninf = nmid
     else
        nsup = nmid
     endif
  enddo
  nlim = nsup
end subroutine findr
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine convol(conv,dx,dy,resu)
  use xymap_types
  !---------------------------------------------------------------------
  ! @ private
  ! Compute value of bi-dimensional convolving function in (dx,dy) point
  ! Takes into account the fact that the 1-D convolving functions are
  ! tabulated every 1/100th of pixel
  !---------------------------------------------------------------------
  type(xymap_conv2d_t), intent(in)  :: conv   !
  real(kind=4),         intent(in)  :: dx,dy  ! Point where to compute the convolving function
  real(kind=4),         intent(out) :: resu   ! Resulting value
  ! Local
  integer(kind=4) :: ix,iy
  !
  ix = nint(100.0*dx+conv%x%bias)
  iy = nint(100.0*dy+conv%y%bias)
  resu = conv%x%buff(ix) * conv%y%buff(iy)
  if (resu.lt.1e-10) resu = 0.0
end subroutine convol
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine conv_fn_computation(conv,error)
  use phys_const
  use classmap_dependencies_interfaces
  use classmap_interfaces, except_this=>conv_fn_computation
  use xymap_types
  !---------------------------------------------------------------------
  ! @ private
  ! CONVFN computes the convolving functions and stores them in the
  ! supplied buffer. Values are tabulated every 1/100th of pixel
  ! CONV%PARM(1) = radius of support in pixel unit
  !---------------------------------------------------------------------
  type(xymap_conv1d_t), intent(inout) :: conv   ! Convolution description
  logical,              intent(out)   :: error  !
  ! Local
  character(len=*), parameter :: rname='XY_MAP'
  integer(kind=4) :: lim,i,im,ialf,ier,ibias
  real(kind=4) :: p1,p2,u,umax,absu,eta,psi
  !
  ! Initialization
  error = .false.
  !
  ! Number of rows: need 100 values per pixel, times the support diameter
  i = int( max (conv%parm(1)+0.995 , 1.0) )
  i = i * 2 + 1
  lim = i * 100 + 1
  call conv_fn_reallocation(conv,lim,error)
  if (error)  return
  conv%bias = 50.0 * i + 1.0
  umax = conv%parm(1)
  ! Check function types
  select case (conv%ctype)
  case (1) ! Pill box
     do i = 1,lim
        u = (i-conv%bias) * 0.01
        absu = abs (u)
        if (absu.lt.umax) then
           conv%buff(i) = 1.0
        elseif (absu.eq.umax) then
           conv%buff(i) = 0.5
        else
           conv%buff(i) = 0.0
        endif
     enddo
     return
  case (2) ! Exponential function
     p1 = 1.0 / conv%parm(2)
     do i = 1,lim
        u = (i-conv%bias) * 0.01
        absu = abs (u)
        if (absu.gt.umax) then
           conv%buff(i) = 0.0
        else
           conv%buff(i) = exp (-((p1*absu) ** conv%parm(3)))
        endif
     enddo
     return
  case (3) ! Sinc function.
     p1 = pi / conv%parm(2)
     do i = 1,lim
        u = (i-conv%bias)*0.01
        absu = abs (u)
        if (absu.gt.umax) then
           conv%buff(i) = 0.0
        elseif (absu.eq.0.0) then
           conv%buff(i) = 1.0
        else
           conv%buff(i) = sin (p1*absu) / (p1*absu)
        endif
     enddo
     return
  case (4) ! EXP * SINC convolving function
     p1 = pi / conv%parm(2)
     p2 = 1.0 / conv%parm(3)
     do i = 1,lim
        u = (i-conv%bias)*0.01
        absu = abs (u)
        if (absu.gt.umax) then
           conv%buff(i) = 0.0
        elseif (absu.lt.0.01) then
           conv%buff(i) = 1.0
        else
           conv%buff(i) = sin(u*p1) / (u*p1) *   &
     &                 exp (-((absu * p2) ** conv%parm(4)))
        endif
     enddo
     return
  case (5) ! Spheroidal
     do i = 1,lim
        conv%buff(i) = 0.0
     enddo
     ialf = 2.0 * conv%parm(2) + 1.1
     im = 2.0 * conv%parm(1) + 0.1
     ialf = max (1, min (5, ialf))
     im = max (4, min (8, im))
     lim = conv%parm(1) * 100.0 + 0.1
     ibias = conv%bias
     do i = 1,lim
        eta = float (i-1) / float (lim-1)
        call sphfn (ialf, im, 0, eta, psi, ier)
        conv%buff(ibias+i-1) = psi
     enddo
     lim = ibias-1
     do i = 1,lim
        conv%buff(ibias-i) = conv%buff(ibias+i)
     enddo
     return
  case default ! Type defaulted or not implemented, use Default = EXP * SINC
     conv%ctype = 4
     conv%parm(1) = 3.0
     conv%parm(2) = 1.55
     conv%parm(3) = 2.52
     conv%parm(4) = 2.00
     p1 = pi / conv%parm(2)
     p2 = 1.0 / conv%parm(3)
     do i = 1,lim
        u = (i-conv%bias)*0.01
        absu = abs (u)
        if (absu.gt.umax) then
           conv%buff(i) = 0.0
        elseif (absu.lt.0.01) then
           conv%buff(i) = 1.0
        else
           conv%buff(i) = sin(u*p1) / (u*p1) *   &
     &                 exp (-((absu * p2) ** conv%parm(4)))
        endif
     enddo
  end select
  !
contains
  subroutine conv_fn_reallocation(conv,n,error)
    !-------------------------------------------------------------------
    ! (Re)allocate the convolution buffer
    !-------------------------------------------------------------------
    type(xymap_conv1d_t), intent(inout) :: conv
    integer(kind=4),      intent(in)    :: n
    logical,              intent(inout) :: error
    !
    if (allocated(conv%buff)) then
      if (size(conv%buff).ge.n)  return
      deallocate(conv%buff)
    endif
    !
    allocate(conv%buff(n),stat=ier)
    if (failed_allocate(rname,'conv buffer',ier,error))  return
    !
  end subroutine conv_fn_reallocation
  !
end subroutine conv_fn_computation
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine sphfn(ialf,im,iflag,eta,psi,ier)
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !  SPHFN is a subroutine to evaluate rational approximations to selected
  !  zero-order spheroidal functions, psi(c,eta), which are, in a
  !  sense defined in VLA Scientific Memorandum No. 132, optimal for
  !  gridding interferometer data.  The approximations are taken from
  !  VLA Computer Memorandum No. 156.  The parameter c is related to the
  !  support width, m, of the convoluting function according to c=
  !  pi*m/2.  The parameter alpha determines a weight function in the
  !  definition of the criterion by which the function is optimal.
  !  SPHFN incorporates approximations to 25 of the spheroidal functions,
  !  corresponding to 5 choices of m (4, 5, 6, 7, or 8 cells)
  !  and 5 choices of the weighting exponent (0, 1/2, 1, 3/2, or 2).
  !
  !  Input:
  !    IALF    I*4   Selects the weighting exponent, alpha.  IALF =
  !                  1, 2, 3, 4, and 5 correspond, respectively, to
  !                  alpha = 0, 1/2, 1, 3/2, and 2.
  !    IM      I*4   Selects the support width m, (=IM) and, correspond-
  !                  ingly, the parameter c of the spheroidal function.
  !                  Only the choices 4, 5, 6, 7, and 8 are allowed.
  !    IFLAG   I*4   Chooses whether the spheroidal function itself, or
  !                  its Fourier transform, is to be approximated.  The
  !                  latter is appropriate for gridding, and the former
  !                  for the u-v plane convolution.  The two differ on-
  !                  by a factor (1-eta**2)**alpha.  IFLAG less than or
  !                  equal to zero chooses the function appropriate for
  !                  gridding, and IFLAG positive chooses its F.T.
  !    ETA     R*4   Eta, as the argument of the spheroidal function, is
  !                  a variable which ranges from 0 at the center of the
  !                  convoluting function to 1 at its edge (also from 0
  !                  at the center of the gridding correction function
  !                  to unity at the edge of the map).
  !
  !  Output:
  !    PSI      R*4  The function value which, on entry to the subrou-
  !                  tine, was to have been computed.
  !    IER      I*4  An error flag whose meaning is as follows:
  !                     IER = 0  =>  No evident problem.
  !                           1  =>  IALF is outside the allowed range.
  !                           2  =>  IM is outside of the allowed range.
  !                           3  =>  ETA is larger than 1 in absolute
  !                                     value.
  !                          12  =>  IALF and IM are out of bounds.
  !                          13  =>  IALF and ETA are both illegal.
  !                          23  =>  IM and ETA are both illegal.
  !                         123  =>  IALF, IM, and ETA all are illegal.
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)  :: ialf   !
  integer(kind=4), intent(in)  :: im     !
  integer(kind=4), intent(in)  :: iflag  !
  real(kind=4)                 :: eta    !
  real(kind=4),    intent(out) :: psi    !
  integer(kind=4), intent(out) :: ier    !
  ! Local
  character(len=*), parameter :: rname='SPHEROIDAL'
  real(kind=4) :: alpha(5), eta2, x
  real(kind=4) :: p4(5,5), q4(2,5), p5(7,5), q5(5), p6l(5,5), q6l(2,5)
  real(kind=4) :: p6u(5,5), q6u(2,5), p7l(5,5), q7l(2,5), p7u(5,5)
  real(kind=4) :: q7u(2,5), p8l(6,5), q8l(2,5), p8u(6,5), q8u(2,5)
  character(len=80) :: mess
  integer(kind=4) :: j
  !
  data alpha / 0., .5, 1., 1.5, 2. /
  data p4 /   &
       &    1.584774e-2, -1.269612e-1,  2.333851e-1, -1.636744e-1,   &
       &    5.014648e-2,  3.101855e-2, -1.641253e-1,  2.385500e-1,   &
       &   -1.417069e-1,  3.773226e-2,  5.007900e-2, -1.971357e-1,   &
       &    2.363775e-1, -1.215569e-1,  2.853104e-2,  7.201260e-2,   &
       &   -2.251580e-1,  2.293715e-1, -1.038359e-1,  2.174211e-2,   &
       &    9.585932e-2, -2.481381e-1,  2.194469e-1, -8.862132e-2,   &
       &    1.672243e-2 /
  data q4 /   &
       &    4.845581e-1,  7.457381e-2,  4.514531e-1,  6.458640e-2,   &
       &    4.228767e-1,  5.655715e-2,  3.978515e-1,  4.997164e-2,   &
       &    3.756999e-1,  4.448800e-2 /
  data p5 /   &
       &    3.722238e-3, -4.991683e-2,  1.658905e-1, -2.387240e-1,   &
       &    1.877469e-1, -8.159855e-2,  3.051959e-2,  8.182649e-3,   &
       &   -7.325459e-2,  1.945697e-1, -2.396387e-1,  1.667832e-1,   &
       &   -6.620786e-2,  2.224041e-2,  1.466325e-2, -9.858686e-2,   &
       &    2.180684e-1, -2.347118e-1,  1.464354e-1, -5.350728e-2,   &
       &    1.624782e-2,  2.314317e-2, -1.246383e-1,  2.362036e-1,   &
       &   -2.257366e-1,  1.275895e-1, -4.317874e-2,  1.193168e-2,   &
       &    3.346886e-2, -1.503778e-1,  2.492826e-1, -2.142055e-1,   &
       &    1.106482e-1, -3.486024e-2,  8.821107e-3 /
  data q5 /   &
       &    2.418820e-1,  2.291233e-1,  2.177793e-1,  2.075784e-1,   &
       &    1.983358e-1 /
  data p6l /   &
       &    5.613913e-2, -3.019847e-1,  6.256387e-1, -6.324887e-1,   &
       &    3.303194e-1,  6.843713e-2, -3.342119e-1,  6.302307e-1,   &
       &   -5.829747e-1,  2.765700e-1,  8.203343e-2, -3.644705e-1,   &
       &    6.278660e-1, -5.335581e-1,  2.312756e-1,  9.675562e-2,   &
       &   -3.922489e-1,  6.197133e-1, -4.857470e-1,  1.934013e-1,   &
       &    1.124069e-1, -4.172349e-1,  6.069622e-1, -4.405326e-1,   &
       &    1.618978e-1 /
  data q6l /   &
       &    9.077644e-1,  2.535284e-1,  8.626056e-1,  2.291400e-1,   &
       &    8.212018e-1,  2.078043e-1,  7.831755e-1,  1.890848e-1,   &
       &    7.481828e-1,  1.726085e-1 /
  data p6u /   &
       &    8.531865e-4, -1.616105e-2,  6.888533e-2, -1.109391e-1,   &
       &    7.747182e-2,  2.060760e-3, -2.558954e-2,  8.595213e-2,   &
       &   -1.170228e-1,  7.094106e-2,  4.028559e-3, -3.697768e-2,   &
       &    1.021332e-1, -1.201436e-1,  6.412774e-2,  6.887946e-3,   &
       &   -4.994202e-2,  1.168451e-1, -1.207733e-1,  5.744210e-2,   &
       &    1.071895e-2, -6.404749e-2,  1.297386e-1, -1.194208e-1,   &
       &    5.112822e-2 /
  data q6u /   &
       &    1.101270e+0,  3.858544e-1,  1.025431e+0,  3.337648e-1,   &
       &    9.599102e-1,  2.918724e-1,  9.025276e-1,  2.575336e-1,   &
       &    8.517470e-1,  2.289667e-1 /
  data p7l /   &
       &    2.460495e-2, -1.640964e-1,  4.340110e-1, -5.705516e-1,   &
       &    4.418614e-1,  3.070261e-2, -1.879546e-1,  4.565902e-1,   &
       &   -5.544891e-1,  3.892790e-1,  3.770526e-2, -2.121608e-1,   &
       &    4.746423e-1, -5.338058e-1,  3.417026e-1,  4.559398e-2,   &
       &   -2.362670e-1,  4.881998e-1, -5.098448e-1,  2.991635e-1,   &
       &    5.432500e-2, -2.598752e-1,  4.974791e-1, -4.837861e-1,   &
       &    2.614838e-1 /
  data q7l /   &
       &    1.124957e+0,  3.784976e-1,  1.075420e+0,  3.466086e-1,   &
       &    1.029374e+0,  3.181219e-1,  9.865496e-1,  2.926441e-1,   &
       &    9.466891e-1,  2.698218e-1 /
  data p7u /   &
       &    1.924318e-4, -5.044864e-3,  2.979803e-2, -6.660688e-2,   &
       &    6.792268e-2,  5.030909e-4, -8.639332e-3,  4.018472e-2,   &
       &   -7.595456e-2,  6.696215e-2,  1.059406e-3, -1.343605e-2,   &
       &    5.135360e-2, -8.386588e-2,  6.484517e-2,  1.941904e-3,   &
       &   -1.943727e-2,  6.288221e-2, -9.021607e-2,  6.193000e-2,   &
       &    3.224785e-3, -2.657664e-2,  7.438627e-2, -9.500554e-2,   &
       &    5.850884e-2 /
  data q7u /   &
       &    1.450730e+0,  6.578685e-1,  1.353872e+0,  5.724332e-1,   &
       &    1.269924e+0,  5.032139e-1,  1.196177e+0,  4.460948e-1,   &
       &    1.130719e+0,  3.982785e-1 /
  data p8l /   &
       &    1.378030e-2, -1.097846e-1,  3.625283e-1, -6.522477e-1,   &
       &    6.684458e-1, -4.703556e-1,  1.721632e-2, -1.274981e-1,   &
       &    3.917226e-1, -6.562264e-1,  6.305859e-1, -4.067119e-1,   &
       &    2.121871e-2, -1.461891e-1,  4.185427e-1, -6.543539e-1,   &
       &    5.904660e-1, -3.507098e-1,  2.580565e-2, -1.656048e-1,   &
       &    4.426283e-1, -6.473472e-1,  5.494752e-1, -3.018936e-1,   &
       &    3.098251e-2, -1.854823e-1,  4.637398e-1, -6.359482e-1,   &
       &    5.086794e-1, -2.595588e-1 /
  data q8l /   &
       &    1.076975e+0,  3.394154e-1,  1.036132e+0,  3.145673e-1,   &
       &    9.978025e-1,  2.920529e-1,  9.617584e-1,  2.715949e-1,   &
       &    9.278774e-1,  2.530051e-1 /
  data p8u /   &
       &    4.290460e-5, -1.508077e-3,  1.233763e-2, -4.091270e-2,   &
       &    6.547454e-2, -5.664203e-2,  1.201008e-4, -2.778372e-3,   &
       &    1.797999e-2, -5.055048e-2,  7.125083e-2, -5.469912e-2,   &
       &    2.698511e-4, -4.628815e-3,  2.470890e-2, -6.017759e-2,   &
       &    7.566434e-2, -5.202678e-2,  5.259595e-4, -7.144198e-3,   &
       &    3.238633e-2, -6.946769e-2,  7.873067e-2, -4.889490e-2,   &
       &    9.255826e-4, -1.038126e-2,  4.083176e-2, -7.815954e-2,   &
       &    8.054087e-2, -4.552077e-2 /
  data q8u /   &
       &    1.379457e+0,  5.786953e-1,  1.300303e+0,  5.135748e-1,   &
       &    1.230436e+0,  4.593779e-1,  1.168075e+0,  4.135871e-1,   &
       &    1.111893e+0,  3.744076e-1 /
  !
  ! Code
  ier = 0
  if (ialf.lt.1 .or. ialf.gt.5) ier = 1
  if (im.lt.4 .or. im.gt.8) ier = 2+10*ier
  if (abs(eta).gt.1.) ier = 3+10*ier
  if (ier.ne.0) then
     write(mess,*) 'Error #',ier
     call class_message(seve%f,rname,mess)
     return
  endif
  eta2 = eta**2
  j = ialf
  !
  ! Support width = 4 cells:
  if (im.eq.4) then
     x = eta2-1.
     psi = (p4(1,j)+x*(p4(2,j)+x*(p4(3,j)+x*(p4(4,j)+x*p4(5,j)))))   &
          &      / (1.+x*(q4(1,j)+x*q4(2,j)))
     !
     ! Support width = 5 cells:
  elseif (im.eq.5) then
     x = eta2-1.
     psi = (p5(1,j)+x*(p5(2,j)+x*(p5(3,j)+x*(p5(4,j)+x*(p5(5,j)   &
          &      +x*(p5(6,j)+x*p5(7,j)))))))   &
          &      / (1.+x*q5(j))
     !
     ! Support width = 6 cells:
  elseif (im.eq.6) then
     if (abs(eta).le..75) then
        x = eta2-.5625
        psi = (p6l(1,j)+x*(p6l(2,j)+x*(p6l(3,j)+x*(p6l(4,j)   &
             &        +x*p6l(5,j))))) / (1.+x*(q6l(1,j)+x*q6l(2,j)))
     else
        x = eta2-1.
        psi = (p6u(1,j)+x*(p6u(2,j)+x*(p6u(3,j)+x*(p6u(4,j)   &
             &        +x*p6u(5,j))))) / (1.+x*(q6u(1,j)+x*q6u(2,j)))
     endif
     !
     ! Support width = 7 cells:
  elseif (im.eq.7) then
     if (abs(eta).le..775) then
        x = eta2-.600625
        psi = (p7l(1,j)+x*(p7l(2,j)+x*(p7l(3,j)+x*(p7l(4,j)   &
             &        +x*p7l(5,j))))) / (1.+x*(q7l(1,j)+x*q7l(2,j)))
     else
        x = eta2-1.
        psi = (p7u(1,j)+x*(p7u(2,j)+x*(p7u(3,j)+x*(p7u(4,j)   &
             &        +x*p7u(5,j))))) / (1.+x*(q7u(1,j)+x*q7u(2,j)))
     endif
     !
     ! Support width = 8 cells:
  elseif (im.eq.8) then
     if (abs(eta).le..775) then
        x = eta2-.600625
        psi = (p8l(1,j)+x*(p8l(2,j)+x*(p8l(3,j)+x*(p8l(4,j)   &
             &        +x*(p8l(5,j)+x*p8l(6,j)))))) / (1.+x*(q8l(1,j)+x*q8l(2,j)))
     else
        x = eta2-1.
        psi = (p8u(1,j)+x*(p8u(2,j)+x*(p8u(3,j)+x*(p8u(4,j)   &
             &        +x*(p8u(5,j)+x*p8u(6,j)))))) / (1.+x*(q8u(1,j)+x*q8u(2,j)))
     endif
  endif
  !
  ! Normal return:
  if (iflag.gt.0 .or. ialf.eq.1 .or. eta.eq.0.) return
  if (abs(eta).eq.1.) then
     psi = 0.0
  else
     psi = (1.-eta2)**alpha(ialf)*psi
  endif
end subroutine sphfn
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine conv_fn_default(ctypx,ctypy,xparm,yparm)
  !---------------------------------------------------------------------
  ! @ private
  !   Determine default parameters for the convolution functions when
  ! they are not specified. Default convolving type is a spheroidal
  ! function. Any other unspecified values (= 0.0) will be set to
  ! some value.
  !---------------------------------------------------------------------
  integer(kind=4), intent(inout) :: ctypx      ! Convolution types for X direction
  integer(kind=4), intent(inout) :: ctypy      ! Convolution types for Y direction
  real(kind=4),    intent(inout) :: xparm(10)  ! Parameters for the convolution fns (1) = support radius (cells)
  real(kind=4),    intent(inout) :: yparm(10)  ! Parameters for the convolution fns (1) = support radius (cells)
  ! Local
  character(len=12) :: chtyps(5)
  integer(kind=4) :: numprm(5),k
  logical :: verbose
  data numprm /1, 3, 2, 4, 2/
  data chtyps /'Pillbox','Exponential','Sin(X)/(X)',   &
       &    'Exp*Sinc','Spheroidal'/
  !
  verbose = .false.
  ! Default = type 5
  if ((ctypx.le.0) .or. (ctypx.gt.5)) ctypx = 5
  select case (ctypx)
     !
     ! Pillbox
  case (1)
     if (xparm(1).le.0.0) xparm(1) = 0.5
     ! Exponential.
  case (2)
     if (xparm(1).le.0.0) xparm(1) = 3.0
     if (xparm(2).le.0.0) xparm(2) = 1.00
     if (xparm(3).le.0.0) xparm(3) = 2.00
     ! Sinc.
  case (3)
     if (xparm(1).le.0.0) xparm(1) = 3.0
     if (xparm(2).le.0.0) xparm(2) = 1.14
     ! Exponential * sinc
  case (4)
     if (xparm(1).le.0.0) xparm(1) = 3.0
     if (xparm(2).le.0.0) xparm(2) = 1.55
     if (xparm(3).le.0.0) xparm(3) = 2.52
     if (xparm(4).le.0.0) xparm(4) = 2.00
     ! Spheroidal function
  case (5)
     if (xparm(1).le.0.0) xparm(1) = 3.0
     if (xparm(2).le.0.0) xparm(2) = 1.0
  end select
  if (verbose) then
     write(6,1001) 'X',chtyps(ctypx),(xparm(k),k=1,numprm(ctypx))
  endif
  !
  ! Put default cheking here for further function types
  ! Check Y convolution defaults
  ! Use X values if not specified
  if ((ctypy.le.0) .and. (ctypy.gt.5)) then
     ctypy = ctypx
     yparm = xparm
     return
  endif
  !
  select case (ctypy)
  case (1)
     if (yparm(1).le.0.0) yparm(1) = 0.5
     ! Exponential
  case (2)
     if (yparm(1).le.0.0) yparm(1) = 3.0
     if (yparm(2).le.0.0) yparm(2) = 1.0
     if (yparm(3).le.0.0) yparm(3) = 2.0
     ! Sinc
  case (3)
     if (yparm(1).le.0.0) yparm(1) = 3.0
     if (yparm(2).le.0.0) yparm(2) = 1.14
     ! Exponential * sinc
  case (4)
     if (yparm(1).le.0.0) yparm(1) = 3.0
     if (yparm(2).le.0.0) yparm(2) = 1.55
     if (yparm(3).le.0.0) yparm(3) = 2.52
     if (yparm(4).le.0.0) yparm(4) = 2.00
  case (5)
     ! Spheroidal function
     if (yparm(1).le.0.0) yparm(1) = 3.0
     if (yparm(2).le.0.0) yparm(2) = 1.0
  end select
  !
  ! Put default checking for new types here.
  if (verbose) then
     write(6,1001) 'Y',chtyps(ctypy),(yparm(k),k=1,numprm(ctypy))
  endif
1001 format(1x,a,' Convolution ',a,' Par.=',5f8.4)
end subroutine conv_fn_default
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
