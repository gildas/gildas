!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine sort_xyw(tab,error)
  use gbl_message
  use classmap_dependencies_interfaces
  use classmap_interfaces, except_this=>sort_xyw
  use xymap_types
  !---------------------------------------------------------------------
  ! @ private
  !  Sort the X Y W buffers by ascending Y, and fill the sorting array
  !---------------------------------------------------------------------
  type(xymap_tab_t), intent(inout) :: tab
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='XY_MAP'
  integer(kind=4) :: ilin,ier,n4
  real(kind=4), allocatable :: work(:)
  !
  if (allocated(tab%sort))  deallocate(tab%sort)
  !
  ! Check if sorting is needed
  tab%dosort = .false.
  do ilin=1,tab%nposi-1
    if (tab%y(ilin).gt.tab%y(ilin+1)) then
      tab%dosort = .true.
      exit
    endif
  enddo
  !
  if (.not.tab%dosort) then
    call class_message(seve%i,rname,'Input table is already sorted')
    return
  endif
  !
  allocate(tab%sort(tab%nposi),stat=ier)
  if (failed_allocate(rname,'sort array',ier,error))  return
  !
  ! Sort Y
  n4 = tab%nposi
  call gr4_trie(tab%y,tab%sort,n4,error)
  if (error)  return
  !
  ! Now sort X and W
  allocate(work(tab%nposi),stat=ier)
  if (failed_allocate(rname,'sorting buffer',ier,error))  return
  !
  call gr4_sort(tab%x,work,tab%sort,n4)
  call gr4_sort(tab%w,work,tab%sort,n4)
  !
  deallocate(work)
  !
end subroutine sort_xyw
!
subroutine sort_tab(table,ncol,nlin,key,work,error)
  use gildas_def
  use gbl_message
  use classmap_dependencies_interfaces
  use classmap_interfaces, except_this=>sort_tab
  !---------------------------------------------------------------------
  ! @ private
  !  Sort the input table according to the sorting array
  !---------------------------------------------------------------------
  integer(kind=index_length), intent(in)    :: ncol              ! Table column number
  integer(kind=index_length), intent(in)    :: nlin              ! Table line number
  real(kind=4),               intent(inout) :: table(ncol,nlin)  ! Table to be sorted
  integer(kind=4),            intent(in)    :: key(nlin)         ! Sorting array
  real(kind=4),               allocatable   :: work(:,:)         ! Working buffer
  logical,                    intent(inout) :: error             ! Returned status
  ! Local
  character(len=*), parameter :: rname='XY_MAP'
  !
  call class_message(seve%d,rname,'Sorting table...')
  !
  ! ZZZ Which sorting strategy is best???
  ! 1) use the sorting array and a working buffer of the same size as
  !    the table. BUT this doubles the needed memory
  ! 2) can we use the sorting array and permute the lines 2 by 2? No
  !    permutations can be highly complicated, and this is more or
  !    less the usual quicksort algorithm
  ! 3) use the quicksort algorithm with a to-be-sorted column and
  !    to-be-sorted table, and repeat this as many times as there are
  !    piece of tables. Note also that if quicksort is less memory
  !    consuming, the cost is that it can involve a huge amount of
  !    permutations...
  !
  call reallocate_tab(work,ncol,nlin,error)
  if (error)  return
  !
  call gr4_sort2d(table,work,key,ncol,nlin)
  !
end subroutine sort_tab
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine gr4_sort2d(x,xwork,key,nc,nl)
  use gildas_def
  use classmap_interfaces, except_this=>gr4_sort2d
  !---------------------------------------------------------------------
  ! @ private
  !  Reorder a real*4 2D array by increasing order using the sorted
  ! indexes computed by a G*_TRIE subroutine
  !---------------------------------------------------------------------
  integer(kind=index_length), intent(in)    :: nl,nc         ! N lines and N columns
  real(kind=4),               intent(inout) :: x(nc,nl)      ! The data to be sorted
  real(kind=4)                              :: xwork(nc,nl)  ! Working buffer
  integer(kind=4),            intent(in)    :: key(nl)       ! Sorting array
  ! Local
  integer(kind=index_length) :: il
  !
  if (nl.le.1) return
  !
  do il=1,nl
    xwork(:,il) = x(:,key(il))
  enddo
  do il=1,nl
    x(:,il) = xwork(:,il)
  enddo
end subroutine gr4_sort2d
!
function trione(x,nd,n,ix,work)
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! 	Sorting an input table (i.e. 2D array) according to column ix in
  !     increasing order. The quicksort algorithm is used.
  !---------------------------------------------------------------------
  integer(kind=4) :: trione                   !
  integer(kind=4), intent(in)    :: nd        ! Table column number
  integer(kind=4), intent(in)    :: n         ! Table line number
  real(kind=4),    intent(inout) :: x(nd,n)   ! Unsorted table
  integer(kind=4), intent(in)    :: ix        ! Column serving as key for sorting
  real(kind=4),    intent(inout) :: work(nd)  ! Work space for exchange
  ! Local
  character(len=*), parameter :: rname='SORT'
  integer :: maxstack,nstop
  parameter (maxstack=1000,nstop=15)
  integer :: i, j, k, l1, r1
  integer :: l, r, m, lstack(maxstack), rstack(maxstack), sp
  real :: key
  logical :: mgtl, lgtr, rgtm
  character(len=message_length) :: mess
  !
  trione = 1
  if (n.le.nstop) goto 50
  sp = 0
  sp = sp + 1
  lstack(sp) = 1
  rstack(sp) = n
  !
  ! Sort a subrecord off the stack
  ! Set KEY = median of X(L), X(M), X(R)
  ! No! This is not reasonable, as systematic very inequal partitioning will
  ! occur in some cases (especially for nearly already sorted files)
  ! To fix this problem, I found (but I cannot prove it) that it is best to
  ! select the estimation of the median value from intermediate records. P.V.
1 l = lstack(sp)
  r = rstack(sp)
  sp = sp - 1
  m = (l + r) / 2
  l1=(2*l+r)/3
  r1=(l+2*r)/3
  !
  mgtl = x(ix,m) .gt. x(ix,l)
  rgtm = x(ix,r) .gt. x(ix,m)
  !
  ! Algorithm to select the median key
  !
  !		   	MGTL	RGTM	LGTR	MGTL.EQV.LGTR	MEDIAN_KEY
  !
  !	KL < KM < KR	T	T	*	*		KM
  !	KL > KM > KR	F	F	*	*		KM
  !
  !	KL < KM > KR	T	F	F	F		KR
  !	KL < KM > KR	T	F	T	T		KL
  !
  !	KL > KM < KR	F	T	F	T		KL
  !	KL > KM < KR	F	T	T	F		KR
  !
  if (mgtl .eqv. rgtm) then
     key = x(ix,m)
  else
     lgtr = x(ix,l) .gt. x(ix,r)
     if (mgtl .eqv. lgtr) then
        key = x(ix,l)
     else
        key = x(ix,r)
     endif
  endif
  i = l
  j = r
  !
  ! Find a big record on the left
10 if (x(ix,i).ge.key) goto 11
  i = i + 1
  goto 10
11 continue
  ! Find a small record on the right
20 if (x(ix,j).le.key) goto 21
  j = j - 1
  goto 20
21 continue
  if (i.ge.j) goto 2
  !
  ! Exchange records
  call r4tor4 (x(1,i),work,nd)
  call r4tor4 (x(1,j),x(1,i),nd)
  call r4tor4 (work,x(1,j),nd)
  i = i + 1
  j = j - 1
  goto 10
  !
  ! Subfile is partitioned into two halves, left .le. right
  ! Push the two halves on the stack
2 continue
  if (j-l+1 .gt. nstop) then
     sp = sp + 1
     if (sp.gt.maxstack) then
        write(mess,*) 'Stack overflow ',sp
        call class_message(seve%e,rname,mess)
        trione = 0
        return
     endif
     lstack(sp) = l
     rstack(sp) = j
  endif
  if (r-j .gt. nstop) then
     sp = sp + 1
     if (sp.gt.maxstack) then
        write(mess,*) 'Stack overflow ',sp
        call class_message(seve%e,rname,mess)
        trione = 0
        return
     endif
     lstack(sp) = j+1
     rstack(sp) = r
  endif
  !
  ! Anything left to process?
  if (sp.gt.0) goto 1
  !
50 continue
  !
  do j = n-1,1,-1
     k = j
     do i = j+1,n
        if (x(ix,j).le.x(ix,i)) goto 121
        k = i
     enddo
121  continue
     if (k.eq.j) goto 110
     call r4tor4 (x(1,j),work,nd)
     do i = j+1,k
        call r4tor4 (x(1,i),x(1,i-1),nd)
     enddo
     call r4tor4 (work,x(1,k),nd)
110  continue
  enddo
end function trione
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
