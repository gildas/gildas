!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine load_map(error)
  use gkernel_interfaces
  use xymap_def
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! MAP initialization routine:
  ! MAP language is loaded.
  !---------------------------------------------------------------------
  logical, intent(out) :: error     ! Error status
  ! Local
  integer(kind=4), parameter :: mmap=3 ! MAP Language
  character(len=12) :: vocab_map(mmap)
  character(len=76) :: version
  !
  external :: run_map
  !
  data version /'0.2 22-Oct-2008 J.P.'/
  data vocab_map / &
       ' XY_MAP', '/NOGRID','/TYPE' /
!      ' APODISE', &
!      ' DECONVOLVE'/
  !
  error = gterrtst()
  call sic_begin('MAP','GAG_HELP_MAP',mmap,vocab_map,version,run_map,gterrtst)
  !
  call init_xymap_struct(sic_xymap_user)
  call define_sic_xymap(error)
  !
end subroutine load_map
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine run_map(line,comm,error)
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! MAP Main library entry point
  !     Call appropriate subroutine according to COMM
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: line   ! Command line
  character(len=*), intent(in)  :: comm   ! Command name
  logical,          intent(out) :: error  ! Error status
  ! Local
  character(len=80) :: mess
  !
  call class_message(seve%c,'MAP',line)
  !
  if (comm.eq.'XY_MAP') then
     call xymap(line,error)
!!$  elseif (comm.eq.'APODISE') then
!!$     !
!!$  elseif (comm.eq.'DECONVOLVE') then
!!$     !
  else
     mess = 'MAP'//char(92)//comm//' Unknown command'
     call class_message(seve%e,'MAP',mess)
     error = .true.
  endif
  !
end subroutine run_map
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
