subroutine class_accumulate(set,line,r,t,error)
  use classcore_interfaces, except_this=>class_accumulate
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! CLASS Support routine for command ACCUMULATE
  !   Adds the R and T spectra together
  !   Reset the plotting constants
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  character(len=*),    intent(in)    :: line   ! Input command line
  type(observation),   intent(inout) :: r,t    ! The observations to be added
  logical,             intent(inout) :: error  ! Logical error flag
  !
  call accumulate_generic(set,line,r,t,error)
  !
end subroutine class_accumulate
!
subroutine accumulate_generic(set,line,r,t,error)
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>accumulate_generic
  use class_averaging
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS Internal routine
  !    AVERAGE all scans of current index together with
  !    various weights and options. Update the origin information and
  !    reset the plotting constants
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  character(len=*),    intent(in)    :: line   ! Input command line
  type(observation),   intent(inout) :: r,t    ! The observations to be added
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='ACCUMULATE'
  integer(kind=4), parameter :: optresample=1
  integer(kind=4), parameter :: optnocheck=2
  integer(kind=4), parameter :: optweight=3
  type(observation) :: sumio
  type(consistency_t) :: cons
  type(average_t) :: aver
  integer(kind=4) :: nc
  !
  if (r%head%xnum.eq.0) then
    call class_message(seve%e,rname,'No R spectrum in memory')
    error = .true.
    return
  endif
  if (t%head%xnum.eq.0) then
    call class_message(seve%e,rname,'No T spectrum in memory')
    error = .true.
    return
  endif
  !
  call init_obs(sumio)
  !
  ! Prepare the addition
  if (sic_present(optresample,0)) then
    if (sic_present(optresample,1)) then
      ! /RESAMPLE Nchan Xref Xval Xinc Unit
      aver%do%resampling = resample_user
      ! aver%do%alignment = unused
      call resample_parse_command(line,optresample,rname,r%head,aver%do%axis,error)
      if (error)  return
    else
      ! /RESAMPLE
      aver%do%resampling = resample_auto
    endif
  else
    ! No /RESAMPLE option
    aver%do%resampling = resample_no
  endif
  !
  aver%do%rname       = rname
  aver%do%kind        = r%head%gen%kind
  aver%do%alignment   = align_auto
  aver%do%weight      = set%weigh
  if (sic_present(optweight,0)) then
    call sic_ke(line,optweight,1,aver%do%weight,nc,.true.,error)
    if (error)  return
  endif
  aver%do%composition = composite_user
  aver%do%range       = .false.  ! No option /RANGE
  aver%do%modfreq     = .false.
  aver%do%restf       = 0.d0  ! Not used
  aver%do%modvelo     = .false.
  aver%do%voff        = 0.d0  ! Not used
  !
  ! Select what will be checked according to user input
  call consistency_defaults(set,cons)
  call consistency_check_selection(set,line,optnocheck,cons,error)
  if (error) return
  ! Consistency of the spectroscopic axes is enabled by default (but the user
  ! can deactivate it, to his own risk). Note that this check is useful if
  ! /RESAMPLE is present (resampling automatically disabled if useless) or
  ! not (request for enabling resampling in case of inconsistency).
  ! No need for inconsistency messages if /RESAMPLE is enabled: user
  ! already knows the axes can be inconsistent!
  cons%spe%mess = aver%do%resampling.eq.resample_no
  ! Set checking tolerances
  call consistency_tole(r%head,cons)
  ! Give user feedback on what will be done
  call consistency_print(set,r%head,cons)
  !
  ! -------------- Duplicate of SUMLIN_HEADER, for R & T --------------
  ! 1) Parse the input parameters
  call sumlin_init(set,cons,aver,error)
  if (error)  goto 10
  !
  ! 2) Prepare the sum header
  call sumlin_header_init(aver,r%head,sumio,error)
  if (error)  goto 10
  !
  ! 3) Prepare internal variables for the addition
  call sumlin_init_variables(aver,r%head,sumio%head,error)
  if (error)  goto 10
  !
  ! 4) Loop on the observations to store their axis parameters. Include
  !    also some checks.
  call sumlin_header_register(r,sumio,aver,error)
  if (error)  goto 10
  ! Check consistency of T against R
  call observation_consistency_check(set,r%head,t%head,cons)
  if (aver%done%kind_is_spec) then
    if ((cons%gen%check.and.cons%gen%prob) .or.  &
        (cons%sou%check.and.cons%sou%prob) .or.  &
        (cons%pos%check.and.cons%pos%prob) .or.  &
        (cons%off%check.and.cons%off%prob) .or.  &
        (cons%lin%check.and.cons%lin%prob) .or.  &
        (cons%cal%check.and.cons%cal%prob)) then
      error = .true.
    endif
    if (cons%spe%check.and.cons%spe%prob) then
      if (aver%do%resampling.eq.resample_no) then
        call class_message(seve%e,aver%do%rname,'Inconsistent spectroscopic '//  &
          'axes, use option /RESAMPLE to enforce resampling')
        error = .true.
      elseif (aver%do%resampling.eq.resample_auto) then
        aver%done%resample = .true.
      endif
    endif
  else
    if ((cons%gen%check.and.cons%gen%prob) .or.  &
        (cons%sou%check.and.cons%sou%prob) .or.  &
        (cons%pos%check.and.cons%pos%prob) .or.  &
        (cons%off%check.and.cons%off%prob)) then
      error = .true.
    endif
    if (cons%dri%check.and.cons%dri%prob) then
      if (aver%do%resampling.eq.resample_no) then
        call class_message(seve%e,aver%do%rname,'Inconsistent drift '//  &
          'axes, use option /RESAMPLE to enforce resampling')
        error = .true.
      elseif (aver%do%resampling.eq.resample_auto) then
        aver%done%resample = .true.
      endif
    endif
  endif
  if (error) then
    call class_message(seve%e,aver%do%rname,'R and T are inconsistent')
    goto 10
  endif
  call sumlin_header_register(t,sumio,aver,error)
  if (error)  goto 10
  !
  ! 5) Set the sum header and X axis description
  call sumlin_header_xaxis(set,aver,sumio,2_8,error)  ! Possibly modify nchan
  if (error)  goto 10
  ! --------------- End of duplicate of SUMLIN_HEADER ---------------
  !
  ! 6) Prepare the sum data
  call sumlin_data_prepro(aver,sumio,r%assoc,error)
  if (error)  goto 10
  !
  ! if (aver%do%modfreq) ...
  ! if (aver%do%modvelo) ...
  !
  ! 7) Loop on the observations to average them
  call sumlin_wadd_new(set,r,aver,sumio,error)
  if (error)  goto 10
  call sumlin_wadd_new(set,t,aver,sumio,error)
  if (error)  goto 10
  !
  ! Special for EQUAL weight: return sum instead of average
  if (set%weigh.eq.'E')  then
    call class_message(seve%w,rname,'Gives sum of spectra for EQUAL weight')
    sumio%data1 = sumio%data1*sumio%dataw
  endif
  !
  call sumlin_data_postpro_waverage(aver,sumio,error)
  if (error)  continue
  sumio%spectre => sumio%data1
  !
  call abscissa(set,sumio,error)
  if (error)  goto 10
  !
  ! Convert temporary R*4 Associated Arrays into correct format
  call copy_assoc_r4toaa(aver%comm%rname,r%assoc,sumio%assoc,error)
  if (error)  continue
  !
  ! Push the stack and copy SUM to R
  call copy_obs(r,t,error)
  if (error)  goto 10
  call copy_obs(sumio,r,error)
  if (error)  goto 10
  !
  call newdat(set,r,error)
  call newdat_assoc(set,r,error)
  call newdat_user(set,r,error)
  !
10 continue
  !
  ! Free memory
  call free_obs(sumio)
  !
end subroutine accumulate_generic
