subroutine class_divide(set,line,r,t,error)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_divide
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! CLASS ANALYSE Support routine for command
  !     DIVIDE Threshold
  !
  !  R --> R/T  T --> T
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  character(len=*),    intent(in)    :: line   ! Command line
  type(observation),   intent(inout) :: r      !
  type(observation),   intent(in)    :: t      !
  logical,             intent(inout) :: error  ! Flag
  ! Local
  character(len=*), parameter :: proc='DIVIDE'
  real(kind=4) :: thre,div
  integer(kind=4) :: i
  !
  call sic_r4 (line,0,1,thre,.true.,error)
  if (error) return
  thre = abs(thre)
  if (r%head%spe%nchan.ne.t%head%spe%nchan) then
     call class_message(seve%e,proc,'Spectra have different number of channels')
     error = .true.
     return
  endif
  if (r%head%spe%vres.eq.-t%head%spe%vres) then
     if ( abs( (1.d0-r%head%spe%rchan)*r%head%spe%vres+r%head%spe%voff     &
             -((1.d0-t%head%spe%rchan)*t%head%spe%vres+t%head%spe%voff) )  &
          .gt. abs(r%head%spe%vres)*1d-2) then
        call class_message(seve%e,proc,'Spectra do not have the same velocity scale')
        error = .true.
     else
        do i=1,r%head%spe%nchan
           div = t%spectre(t%head%spe%nchan-i+1)
           if (abs(div).gt.thre .and. div.ne.t%head%spe%bad .and.   &
                r%spectre(i).ne.r%head%spe%bad) then
              r%spectre(i) = r%spectre(i)/div
           else
              r%spectre(i) = r%head%spe%bad
           endif
        enddo
     endif
  elseif (r%head%spe%vres.eq.t%head%spe%vres) then
     if ( abs( (1.d0-r%head%spe%rchan)*r%head%spe%vres+r%head%spe%voff     &
             -((1.d0-t%head%spe%rchan)*t%head%spe%vres+t%head%spe%voff) )  &
          .gt. abs(r%head%spe%vres)*1d-2) then
        call class_message(seve%e,proc,'Spectra do not have the same velocity scale')
        error = .true.
     else
        do i=1,r%head%spe%nchan
           div = t%spectre(i)
           if (abs(div).gt.thre .and. div.ne.t%head%spe%bad .and.   &
                r%spectre(i).ne.r%head%spe%bad) then
              r%spectre(i) = r%spectre(i)/div
           else
              r%spectre(i) = r%head%spe%bad
           endif
        enddo
     endif
  else
     call class_message(seve%e,proc,'Spectra do not have the same velocity scale')
     error = .true.
  endif
  if (error) return
  call newdat(set,r,error)
end subroutine class_divide
