!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine my_get_beam(telescope,freq_mhz,found,beam_rad,error)
  use phys_const
  use gkernel_interfaces
  use classcore_interfaces, except_this=>my_get_beam
  !---------------------------------------------------------------------
  ! @ public
  ! Compute the beam width for given telescope at given frequency
  ! The telescope name can be either:
  !  - a Class telescope name e.g. 30ME0HUI-V01,
  !  - a telescope name given by the user (e.g. through variable MAP%TELES)
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: telescope  ! [      ] Class telescope name
  real(kind=8),     intent(in)    :: freq_mhz   ! [MHz   ] Sky frequency
  logical,          intent(out)   :: found      ! [      ] Found the telescope beam?
  real(kind=4),     intent(out)   :: beam_rad   ! [radian] Beam size
  logical,          intent(inout) :: error      ! [      ] Return status
  ! Local
  character(len=*), parameter :: rname='GET_BEAM'
  character(len=12) :: tele,name
  real(kind=4) :: diam
  real(kind=8) :: freq_ghz,beam_arcsec,lonlat(2),alti,slim
  real(kind=8), parameter :: hifi_constant=(2.d0/pi)*(1.6d0+2.1d-2*7.94d0)*clight_mhz/3.28d0/rad_per_sec
  !
  ! Initialization
  freq_ghz = freq_mhz*1d-3  ! convert to GHz
  tele = telescope
  call sic_upper(tele)
  !
  ! Try a specific name and apply custom formula
  found = .true.
  if (index(tele,'30M').ne.0) then
     beam_arcsec = 2460.d0/freq_ghz
  elseif (index(tele,'PDB').ne.0  .or. index(tele,'15M').ne.0 .or.  &
          index(tele,'SEST').ne.0 .or. index(tele,'JCMT').ne.0 ) then
     beam_arcsec = 56d0*(88d0/freq_ghz)
  elseif (index(tele,'AP-').ne.0 .or. index(tele,'ALMA').ne.0 ) then
     beam_arcsec = 6286.7d0/freq_ghz   ! 1.22 lambda / D (D=12m) [from D. Muders]
  elseif (index(tele,'HIF-').ne.0) then
     ! (2/pi)*(1.6+0.021*7.94)*lambda/3.28  [rad] with lambda in meters
     ! See http://herschel.esac.esa.int/Docs/TechnicalNotes/HIFI_Beam_Efficiencies_17Nov2010.pdf
     beam_arcsec = hifi_constant/freq_mhz
  elseif (index(tele,'FCRAO').ne.0) then
     beam_arcsec = 15d0/14.0d0*56d0*(88d0/freq_ghz)
  elseif (index(tele,'CSO').ne.0) then
     beam_arcsec = 15d0/10.4d0*56d0*(88d0/freq_ghz)
  elseif (index(tele,'KP').ne.0 .or. index(tele,'KITT').ne.0 .or.  &
          index(tele,'UASO').ne.0) then
     beam_arcsec = 15d0/12d0*56d0*(88d0/freq_ghz)
  elseif (index(tele,'HSO').ne.0) then
     beam_arcsec = 15d0/3d0*56d0*(88d0/freq_ghz)
  elseif (index(tele,'GBT').ne.0) then
     beam_arcsec = 15d0/100d0*56d0*(88d0/freq_ghz)
  else
     found = .false.
  endif
  if (found) then
    beam_rad = beam_arcsec*rad_per_sec
    return
  endif
  !
  ! Not found yet. Try a telescope recognized by Astro and apply generic
  ! formula:
  call my_get_teles(rname,tele,.false.,name,error)  ! Translate to Astro name
  if (error)  return
  if (name.ne.'') then
    call gwcs_observatory(name,lonlat,alti,slim,diam,error)
    if (error)  return
    beam_rad = 1.22d0*(clight_mhz/freq_mhz)/diam
    found = .true.
    return
  endif
  !
  ! Not found. Not an error at this level (can be an error upon the decision
  ! of the calling procedure)
  found = .false.
  beam_rad = 0.0
  return
  !
end subroutine my_get_beam
!
subroutine my_get_teles(caller,telescope,mustfind,name,error)
  use gbl_message
  use classcore_interfaces, except_this=>my_get_teles
  !---------------------------------------------------------------------
  ! @ private
  !  Translate a Class obs%head%gen%teles into a telescope name
  ! gwcs_observatory and astro_observatory can understand. Symmetric
  ! to my_set_teles
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: caller     !
  character(len=*), intent(in)    :: telescope  !
  logical,          intent(in)    :: mustfind   !
  character(len=*), intent(out)   :: name       !
  logical,          intent(inout) :: error      !
  !
  if (index(telescope,'30M').ne.0) then
    name = '30M'
  elseif (telescope(1:3).eq."AP-".or.index(telescope,'APEX').ne.0) then
    name = 'APEX'
  elseif (telescope(1:4).eq."12M-") then
    name = 'KITTPEAK'
  elseif (telescope(1:4).eq."MED-") then
    name = 'MEDICINA'
  elseif (telescope(1:4).eq."SMT-") then
    name = 'SMT'  ! ARO's (Arizona Radio Observatory) SubMillimeter Telescope
  elseif (telescope(1:4).eq."GBT-") then
    name = 'GBT'  ! Green Bank Telescope
  elseif (telescope(1:4).eq."OAN-") then
    name = 'YEBES'  ! Yebes 40m
  elseif (telescope(1:4).eq."HIF-") then
    name = 'HERSCHEL'  ! Herschel Space Observatory... not known by Astro!!!
  elseif (telescope(1:4).eq."JCMT") then
    name = 'JCMT'
  elseif (telescope(1:5).eq."TRAO-") then
    name = 'TRAO'
  elseif (telescope(1:4).eq."FAST") then
    name = 'FAST'
  elseif (index(telescope,'CSO').ne.0) then  ! Is there a better rule?
    name = 'CSO'
  else
    name = ''
    if (mustfind) then
      call class_message(seve%e,caller,'Telescope not understood from '//telescope)
      error = .true.
      return
    endif
  endif
  !
end subroutine my_get_teles
!
subroutine my_set_teles(caller,name,mustfind,telescope,error)
  use gbl_message
  use classcore_interfaces, except_this=>my_set_teles
  !---------------------------------------------------------------------
  ! @ private
  !  Translate a telescope name into a Class obs%head%gen%teles.
  ! Symmetric to my_get_teles.
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: caller     !
  character(len=*), intent(in)    :: name       !
  logical,          intent(in)    :: mustfind   !
  character(len=*), intent(out)   :: telescope  !
  logical,          intent(inout) :: error      !
  !
  select case (name)
  case ('30M')
    telescope = '30M-'
  case ('APEX')
    telescope = 'AP-'
  case ('KITTPEAK')
    telescope = '12M-'
  case ('MEDICINA')
    telescope = 'MED-'
  case ('SMT')
    telescope = 'SMT-'
  case ('GBT')
    telescope = 'GBT-'
  case ('YEBES')
    telescope = 'OAN-'
  case ('JCMT')
    telescope = 'JCMT'
  case ('TRAO')
    telescope = 'TRAO-'
  case ('FAST')
    telescope = 'FAST'
  case default
    telescope = 'UNKNOWN'
    if (mustfind) then
      call class_message(seve%e,caller,'Telescope name not understood: '//name)
      error = .true.
      return
    endif
  end select
  !
end subroutine my_set_teles
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
