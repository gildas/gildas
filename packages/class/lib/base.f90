subroutine baseline(set,line,r,error,user_function)
  use gbl_constant
  use gbl_format
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>baseline
  use class_data
  use class_index
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! CLASS Support routine for command
  !  BASELINE [N|SINUS|LAST]
  ! 1         [/PLOT [ipen]]
  ! 2         [/CONTINUUM [flux]]
  ! 3         [/INDEX]
  ! 4         [/OBS]
  !  Computes and optionnaly plots a polynomial baseline of degree N or
  ! the default value defined in SET BASE. Optionnaly uses the last
  ! computed baseline, i.e. the last polynomial coefficients.
  !---------------------------------------------------------------------
  type(class_setup_t), intent(inout) :: set            !
  character(len=*),    intent(in)    :: line           ! command line
  type(observation),   intent(inout) :: r              !
  logical,             intent(inout) :: error          ! flag
  logical,             external      :: user_function  ! External function
  ! Local
  character(len=*), parameter :: rname='BASE'
  character(len=12), parameter :: clast='LAST        '
  character(len=12), parameter :: csinus='SINUS       '
  character(len=12), parameter :: cnewsinus='NEWSINUS       '
  character(len=12) :: argum
  logical :: do_cont,do_oldsinus,do_newsinus(3),do_last,do_plot,do_index
  integer(kind=4) :: narg,do_plot_pen
  real(kind=4) :: flux,sinuspar(3)
  integer(kind=4) :: deg
  character(len=message_length) :: mess
  !
  ! Decode line command
  do_cont  = sic_present(2,0)
  do_index = set%action.eq.'I'
  if (sic_present(3,0).and.sic_present(4,0)) then
     call class_message(seve%e,rname,'/INDEX and /OBS are exclusive from each other')
     error = .true.
     return
  else if (sic_present(3,0)) then
     if (.not.associated(p%data2)) then
        call class_message(seve%e,rname,'No index loaded')
        error = .true.
        return
     endif
     do_index = .true.
  else if (sic_present(4,0)) then
     do_index = .false.
  endif
  do_plot = sic_present(1,0)
  if (sic_present(1,1)) then
     ! Get pen color from option arguments
     error = .false.
     call sic_i4(line,1,1,do_plot_pen,.true.,error)
     if (error) return
  else
     do_plot_pen = 1  ! Pen #1, red by default.
  endif
  !
  ! Number of windows
  if (set%nwind.gt.mwind) then
     call class_message(seve%e,rname,'Too many spectral windows')
     error = .true.
     return
  endif
  !
  ! If window defined through polygon, check if variable WINDOW exists
  if (set%nwind.eq.setnwind_polygon) then
     if (.not.sic_varexist('SET%WINDOW')) then
        call class_message(seve%e,rname,'No 2D window (polygon) defined')
        error = .true.
        return
     endif
  endif
  !
  ! Check if spectrum in R buffer
  if (r%head%xnum.eq.0.and.(.not.do_index)) then
     call class_message(seve%e,rname,'No spectrum in memory')
     error = .true.
     return
  endif
  do_last = .false.
  flux = 0.0
  !
  ! R spectrum is of type Continuum
  if (do_cont) then
     call sic_r4 (line,2,1,flux,.false.,error)
     if (error) return
  endif
  !
  ! Find algorithm: LAST SINUS POLY NEWSINUS
  argum = ' '
  call sic_ke (line,0,1,argum,narg,.false.,error)
  if (error) return
  narg = max(narg,1)
  if (argum.eq.clast(1:narg)) then
     ! BASE LAST
     do_last = .true.
     do_newsinus = .false.
     if (set%lbase.ge.0) then  ! Polynomial
        deg = set%lbase
        do_oldsinus = .false.
     elseif (set%lbase.eq.-1) then  ! Sinus
        deg = set%lbase
        do_oldsinus = .true.
     else
        call class_message(seve%e,rname,'No valid baseline for LAST mode')
        error = .true.
     endif
     if (error) return
  elseif (argum.eq.csinus(1:narg) .or.  &
         (argum.eq.' ' .and. set%base.eq.-1)) then
     ! BASE SINUS
     do_oldsinus = .true.
     do_newsinus = .false.
     deg = -1
  elseif (argum.eq.cnewsinus(1:narg)) then
     ! BASE NEWSINUS
     do_oldsinus = .false.
     do_newsinus = .true.
     deg = -1
  else
     ! BASE [number]
     do_oldsinus = .false.
     do_newsinus = .false.
     if (argum .eq. ' ') then
        deg = set%base
     else
        call sic_i4 (line,0,1,deg,.false.,error)
        if (error) return
        if (deg.gt.mdeg) then
          write(mess,'(A,I0,A,I0,A)')  &
            'Degree ',deg,' truncated to ',mdeg,' (maximum allowed)'
          call class_message(seve%w,rname,mess)
          deg = mdeg
        elseif (deg.lt.0) then
          write(mess,'(A,I0,A)')  'Degree ',deg,' forced to 0'
          call class_message(seve%w,rname,mess)
          deg = 0
        endif
     endif
  endif
  if (error) return
  !
  if (do_oldsinus .or. any(do_newsinus)) then
    if (do_index) then
      call class_message(seve%e,rname,  &
        'BASE SINUS is not implemented for the whole index')
      error = .true.
      return
    endif
    if (do_cont) then
      call class_message(seve%e,rname,  &
        'BASE SINUS is not implemented for continuum data')
      error = .true.
      return
    endif
    if (any(do_newsinus)) then
      call parse_newsinus(1,error)
      if (error)  return
      call parse_newsinus(2,error)
      if (error)  return
      call parse_newsinus(3,error)
      if (error)  return
    elseif (.not.do_last) then
      call sic_r4 (line,0,2,sinuspar(1),.true.,error)
      if (error) return
      call sic_r4 (line,0,3,sinuspar(2),.true.,error)
      if (error) return
      call sic_r4 (line,0,4,sinuspar(3),.true.,error)
      if (error) return
    endif
  endif
  !
  if (do_index) then
     ! 1. define a single mask from SET WINDOW /POLYGON
     ! 2. for each record, compute the spectral window
     ! 3. save spectra window parameters in each record header
     ! 4. fill in the new 2D array
     call baseline_index(set,deg,sinuspar,do_last,do_cont,flux,error,user_function)
     if (error)  return
  else
     ! Single spectrum
     !
     call baseline_obs_prepro(set,r,knext,deg,error)
     if (error)  return
     !
     call baseline_obs(set,                               &
                       r,                                 &  ! Observation
                       do_oldsinus,do_newsinus,sinuspar,  &  ! SINUS|NEWSINUS
                       do_plot,do_plot_pen,               &  ! Plot
                       do_last,                           &  ! LAST
                       do_cont,flux,                      &  ! Continuum
                       error)
     if (error) return
     !
     call baseline_obs_postpro(r,error)
     if (error)  return
  endif
  !
  ! Save degree for next time
  set%lbase = deg
  !
  ! Reset y-scale
  call newlimy(set,r,error)
  if (error) return
  !
  if (do_plot.and.do_index)  call plot_index(set,error)
  !
contains
  subroutine parse_newsinus(iarg,error)
    integer(kind=4), intent(in)    :: iarg
    logical,         intent(inout) :: error
    ! Local
    character(len=32) :: argum
    integer(kind=4) :: nc
    !
    call sic_ch(line,0,iarg+1,argum,nc,.true.,error)
    if (error)  return
    do_newsinus(iarg) = argum.eq.'*'
    if (do_newsinus(iarg)) then
      sinuspar(iarg) = 0.  ! Dummy value (unused)
    else
      call sic_math_real(argum,nc,sinuspar(iarg),error)
      if (error)  return
    endif
    !
  end subroutine parse_newsinus
  !
end subroutine baseline
!
subroutine baseline_index(set,deg,sinuspar,last,cont,flux,error,user_function)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>baseline_index
  use class_data
  use class_index
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command BASE.
  ! This version for the whole index.
  ! Make the loop on records.
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  integer(kind=4),     intent(in)    :: deg            ! Degree of polynom
  real(kind=4),        intent(in)    :: sinuspar(3)    ! Sine guess if sinus fitting
  logical,             intent(inout) :: last           ! Use last baseline determination
  logical,             intent(in)    :: cont           ! Continuum?
  real(kind=4),        intent(inout) :: flux           ! Flux value (if 0, average of base channels)
  logical,             intent(inout) :: error          ! Logical error flag
  logical,             external      :: user_function  ! External function
  ! Local
  character(len=*), parameter :: rname='BASE'
  integer(kind=entry_length) :: ir
  logical :: dosinus,newsinus(3)
  type(observation) :: obs
  !
  dosinus = deg.eq.-1
  newsinus = .false.
  call init_obs(obs)
  !
  do ir=1,cx%next-1
    !
    if (sic_ctrlc()) then
      call class_message(seve%e,rname,'Aborted by ^C')
      error = .true.
      return
    endif
    !
    ! Get header and data of the current record
    call get_it(set,obs,cx%ind(ir),user_function,error)
    call abscissa(set,obs,error)
    call newlim(set,obs,error)
    if (error) return
    !
    call baseline_obs_prepro(set,obs,ir,deg,error)
    if (error)  return
    !
    ! Compute the polynomial baseline
    call baseline_obs(set,                        &
                      obs,                        &  ! Observation
                      dosinus,newsinus,sinuspar,  &  ! Sinus
                      .false.,0,                  &  ! Plot
                      last,                       &  ! LAST
                      cont,flux,                  &  ! Continuum
                      error)
    if (error) return
    !
    call baseline_obs_postpro(obs,error)
    if (error)  return
    !
    ! Update the 2D array for further display of the result
    p%spectre => p%data2(:,ir)
    p%spectre = obs%spectre
  enddo
  !
  call free_obs(obs)
  !
end subroutine baseline_index
!
subroutine baseline_obs_prepro(set,obs,ir,deg,error)
  use gbl_message
  use classcore_interfaces, except_this=>baseline_obs_prepro
  use class_types
  !---------------------------------------------------------------------
  ! @ private (?)
  ! Support routine for command BASE.
  !  This version for a single observation. Prepare the observation
  ! header for ongoing baseline fit.
  !---------------------------------------------------------------------
  type(class_setup_t),        intent(in)    :: set    !
  type(observation),          intent(inout) :: obs    ! The input observation to work on
  integer(kind=entry_length), intent(in)    :: ir     ! Position of observation in index
  integer(kind=4),            intent(in)    :: deg    ! Degree of polynom
  logical,                    intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='BASE'
  integer(kind=4) :: iw
  !
  if (set%nwind.eq.setnwind_polygon) then
    ! From polygon(s) (PLOT /INDEX mode)
    call reset_windows(obs)
    obs%head%bas%nwind = set%window_polygon(ir,1)
    do iw=1,obs%head%bas%nwind
      obs%head%bas%w1(iw) = set%window_polygon(ir,2*iw)
      obs%head%bas%w2(iw) = set%window_polygon(ir,2*iw+1)
    enddo
    !
  elseif (set%nwind.eq.setnwind_assoc) then
    if (.not.class_assoc_exists(obs,'LINE')) then
      call class_message(seve%e,rname,  &
        'No LINE Associated Array while SET WINDOW /ASSOCIATED is set')
      error = .true.
      return
    endif
    call reset_windows(obs)
    obs%head%bas%nwind = basnwind_assoc
    !
  elseif (set%nwind.eq.setnwind_auto) then
    ! Automatic from data
    if (.not.obs%head%presec(class_sec_bas_id)) then
      call class_message(seve%e,rname,  &
        'No previous Baseline section while SET WINDOW is AUTO')
      error = .true.
      return
    endif
    if (obs%head%bas%nwind.ge.0) then
      ! Reuse the previous windows saved in section, unchanged
      continue
    elseif (obs%head%bas%nwind.eq.basnwind_assoc) then
      ! Reuse the LINE Associated Array. Check if it still exists
      if (.not.class_assoc_exists(obs,'LINE')) then
        call class_message(seve%e,rname,  &
          'No LINE Associated Array while SET WINDOW is AUTO')
        error = .true.
        return
      endif
      ! Let the section unchanged
    else
      call class_message(seve%e,rname,  &
        'Previous calibration section not understood while SET WINDOW is AUTO')
      error = .true.
      return
    endif
    !
  elseif (set%nwind.ge.0) then
    ! The usual pair of values for each window
    call reset_windows(obs)
    obs%head%bas%nwind = min(set%nwind,mwind)
    do iw=1,obs%head%bas%nwind
      obs%head%bas%w1(iw) = set%wind1(iw)
      obs%head%bas%w2(iw) = set%wind2(iw)
    enddo
    !
  else
    ! Mode setnwind_default, or code not understood
    call class_message(seve%e,rname,'No line window or polygon defined')
    error = .true.
    return
    !
  endif
  !
  obs%head%bas%deg = deg
  !
  ! write(*,'(i5,i5,20f8.1)')                     &
  !       ir,obs%head%bas%nwind,                  &
  !       obs%head%bas%w1(1:obs%head%bas%nwind),  &
  !       obs%head%bas%w2(1:obs%head%bas%nwind)
  !
contains
  subroutine reset_windows(obs)
    type(observation), intent(inout) :: obs
    obs%head%bas%nwind = 0
    obs%head%bas%w1(:) = 0.
    obs%head%bas%w2(:) = 0.
  end subroutine reset_windows
  !
end subroutine baseline_obs_prepro
!
subroutine baseline_obs(set,obs,dooldsinus,donewsinus,sinuspar,lplot,  &
  plot_pen,last,cont,flux,error)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>baseline_obs
  use class_types
  !---------------------------------------------------------------------
  ! @ private (?)
  ! Support routine for command BASE.
  ! This version for a single observation. Fit polynomial or sine.
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  type(observation),   intent(inout) :: obs            ! The input observation to work on
  logical,             intent(in)    :: dooldsinus     ! BASE SINUS
  logical,             intent(in)    :: donewsinus(3)  ! BASE NEWSINUS
  real(kind=4),        intent(in)    :: sinuspar(3)    ! Sine initial guess if sinus fitting
  logical,             intent(in)    :: lplot          ! Plot the baseline or not
  integer(kind=4),     intent(in)    :: plot_pen       ! Pen number, if plot is enabled
  logical,             intent(inout) :: last           ! Use last baseline determination
  logical,             intent(in)    :: cont           ! Divide rather than subtract baseline
  real(kind=4),        intent(inout) :: flux           ! Flux value (if 0, average of base channels)
  logical,             intent(inout) :: error          ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='BASE'
  real(kind=4), allocatable :: z(:)  ! Model intensity values
  integer(kind=4) :: ier
  !
  allocate(z(obs_nchan(obs%head)),stat=ier)
  if (failed_allocate(rname,'z',ier,error))  return
  !
  ! Fit
  if (dooldsinus .or. any(donewsinus)) then
    if (dooldsinus) then
      call sinus_obs(set,obs,sinuspar,last,z,error)
    else
      call sinus_obs_new(set,obs,donewsinus,sinuspar,z,error)
    endif
  else
    call polyno_obs(set,obs,last,cont,flux,z,error)
  endif
  if (error)  goto 100
  !
  ! Plot the baseline if needed
  if (lplot) then
    call baseline_plot(obs,z,plot_pen,error)
    if (error)  goto 100
  endif
  !
100 continue
  !
  ! Allocate/deallocate unefficient on the index, but each observation size
  ! can vary when reading the index. And what about parallel processing?
  deallocate(z)
  !
end subroutine baseline_obs
!
subroutine baseline_obs_postpro(obs,error)
  use class_types
  !---------------------------------------------------------------------
  ! @ private (?)
  ! Support routine for command BASE.
  ! This version for a single observation. Fit polynomial or sine.
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs    ! The input observation to work on
  logical,           intent(inout) :: error  ! Logical error flag
  !
  obs%head%presec(class_sec_bas_id) = .true.
  !
end subroutine baseline_obs_postpro
!
subroutine baseline_plot(obs,z,ipen,error)
  use gkernel_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Plot the baseline for a single observation
  !---------------------------------------------------------------------
  type(observation), intent(in)    :: obs    ! The observation to work on
  real(kind=4),      intent(in)    :: z(*)   ! The model Z values
  integer(kind=4),   intent(in)    :: ipen   ! Pen to be used for the plot
  logical,           intent(inout) :: error  ! Logical error flag
  ! Global
  external :: cplot
  ! Local
  integer(kind=4) :: curpen,oldpen,nnn
  !
  curpen = ipen
  oldpen = gr_spen(curpen)
  !
  call gr_segm('BASE',error)
  if (error)  return
  !
  nnn = obs%cimax-obs%cimin+1
  if (obs%head%bas%deg.eq.0 .or. obs%head%bas%deg.eq.1) then
    call cplot(real(obs%cimin),z(obs%cimin),3)
    call cplot(real(obs%cimax),z(obs%cimax),2)
  else
    call conne2(real(obs%cimin),1.,1.,z(obs%cimin:obs%cimax),nnn,cplot)
  endif
  !
  call gr_segm_close(error)
  if (error)  return
  !
  curpen = gr_spen(oldpen)
  !
end subroutine baseline_plot
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
