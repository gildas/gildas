subroutine wgen(set,obs,error)
  use classcore_interfaces, except_this=>wgen
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the GENeral Section
  !-------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec(set,obs,class_sec_gen_id,error)
end subroutine wgen
!
subroutine wres(set,obs,error)
  use classcore_interfaces, except_this=>wres
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the RESolution section
  !-------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec(set,obs,class_sec_res_id,error)
end subroutine wres
!
subroutine wbase(set,obs,error)
  use classcore_interfaces, except_this=>wbase
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the BASE Section
  !-------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec(set,obs,class_sec_bas_id,error)
end subroutine wbase
!
subroutine wcom(set,obs,error)
  use classcore_interfaces, except_this=>wcom
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the COMment Section
  !-------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec(set,obs,class_sec_com_id,error)
end subroutine wcom
!
subroutine wcal(set,obs,error)
  use classcore_interfaces, except_this=>wcal
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the CALibration Section
  !-------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec(set,obs,class_sec_cal_id,error)
end subroutine wcal
!
subroutine wcont(set,obs,error)
  use classcore_interfaces, except_this=>wcont
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the CONTinuum Section
  !-------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec(set,obs,class_sec_dri_id,error)
end subroutine wcont
!
subroutine wbeam(set,obs,error)
  use classcore_interfaces, except_this=>wbeam
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the BEAM Section
  !-------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec(set,obs,class_sec_bea_id,error)
end subroutine wbeam
!
subroutine wswi(set,obs,error)
  use classcore_interfaces, except_this=>wswi
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the SWitching Section
  !-------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec(set,obs,class_sec_swi_id,error)
end subroutine wswi
!
subroutine wgaus(set,obs,error)
  use classcore_interfaces, except_this=>wgaus
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the GAUSs Section
  !-------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec(set,obs,class_sec_gau_id,error)
end subroutine wgaus
!
subroutine wnh3(set,obs,error)
  use classcore_interfaces, except_this=>wnh3
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the NH3 Section
  !-------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec(set,obs,class_sec_hfs_id,error)
end subroutine wnh3
!
subroutine wabs(set,obs,error)
  use classcore_interfaces, except_this=>wabs
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the ABSorption Section
  !-------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec(set,obs,class_sec_abs_id,error)
end subroutine wabs
!
subroutine wshel(set,obs,error)
  use classcore_interfaces, except_this=>wshel
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the SHELl Section
  !-------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec(set,obs,class_sec_she_id,error)
end subroutine wshel
!
subroutine worig(set,obs,error)
  use classcore_interfaces, except_this=>worig
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the ORIGin Section
  !-------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec(set,obs,class_sec_his_id,error)
end subroutine worig
!
subroutine wplot(set,obs,error)
  use classcore_interfaces, except_this=>wplot
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the PLOT Section
  !-------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec(set,obs,class_sec_plo_id,error)
end subroutine wplot
!
subroutine wpos(set,obs,error)
  use classcore_interfaces, except_this=>wpos
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the POSition Section
  !-------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec(set,obs,class_sec_pos_id,error)
end subroutine wpos
!
subroutine wspec(set,obs,error)
  use classcore_interfaces, except_this=>wspec
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the SPECtroscopic Section
  !-------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec(set,obs,class_sec_spe_id,error)
end subroutine wspec
!
subroutine wpoint(set,obs,error)
  use classcore_interfaces, except_this=>wpoint
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the POINTing Section
  !-------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec(set,obs,class_sec_poi_id,error)
end subroutine wpoint
!
subroutine wsky(set,obs,error)
  use classcore_interfaces, except_this=>wsky
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the SKYdip Section
  !-------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec(set,obs,class_sec_sky_id,error)
end subroutine wsky
!
subroutine wxcoo(set,obs,error)
  use classcore_interfaces, except_this=>wxcoo
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the X COOrdinates Section
  !-------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec_xcoo(set,obs,error)
end subroutine wxcoo
!
subroutine wdescr(set,obs,error)
  use classcore_interfaces, except_this=>wdescr
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Write the DESCRiptor Section
  !-------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec(set,obs,class_sec_desc_id,error)
end subroutine wdescr
!
subroutine wuser(set,obs,error)
  use classcore_interfaces, except_this=>wuser
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  ! Write the USER defined section description
  !----------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec(set,obs,class_sec_user_id,error)
end subroutine wuser
!
subroutine wherschel(set,obs,error)
  use classcore_interfaces, except_this=>wherschel
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  ! Write the HERSCHEL defined section description
  !----------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec(set,obs,class_sec_her_id,error)
end subroutine wherschel
!
subroutine wassoc(set,obs,error)
  use classcore_interfaces, except_this=>wassoc
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  ! Write the ASSOCiated Arrays section
  !----------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  call cwsec(set,obs,class_sec_assoc_id,error)
end subroutine wassoc
!
subroutine wdata(obs,ndata,array,error)
  use gbl_message
  use classic_api
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>wdata
  use class_types
  use class_common
  !----------------------------------------------------------------------
  ! @ private
  ! Write or update the data section
  !----------------------------------------------------------------------
  type(observation), intent(inout) :: obs           ! Observation to be written
  integer(kind=4),   intent(in)    :: ndata         ! Length in 32_bit words of data
  real(kind=4),      intent(in)    :: array(ndata)  ! Data itself
  logical,           intent(inout) :: error         ! Error flag
  ! Local
  character(len=*), parameter :: rname='WDATA'
  real(kind=4), allocatable, save :: carray(:)  ! Converted array
  integer(kind=data_length) :: len8
  logical :: alloc
  integer(kind=4) :: ier
  !
  if (ndata.le.0) then
    call class_message(seve%e,rname,'Size of data can not be null or negative')
    error = .true.
    return
  endif
  if (obufobs%lun.ne.fileout%lun) then
    call class_message(seve%e,rname,  &
      'Observation not open for write nor modify')
    error = .true.
    return
  endif
  !
  len8 = ndata
  if (fileout%conv%code.eq.0) then
    ! Native format: avoid copy, send directly the input data array
    if (outobs_modify) then
      call classic_entry_data_update(array,len8,obs%desc,obufobs,error)
    else
      call classic_entry_data_add(array,len8,obs%desc,obufobs,error)
    endif
    !
  else
    ! Non-native, conversion needed. Can not do it in-place, this
    ! would modify the input 'array' argument, which, in memory, is
    ! always native (e.g. it can be reused for a replot)
    if (allocated(carray)) then
      if (size(carray).lt.ndata) then
        deallocate(carray)
        alloc = .true.
      else
        alloc = .false.
      endif
    else
      alloc = .true.
    endif
    if (alloc) then
      allocate(carray(ndata),stat=ier)
      if (failed_allocate(rname,'carray',ier,error))  return
    endif
    call fileout%conv%writ%r4(array,carray,ndata)
    if (outobs_modify) then
      call classic_entry_data_update(carray,len8,obs%desc,obufobs,error)
    else
      call classic_entry_data_add(carray,len8,obs%desc,obufobs,error)
    endif
    !
  endif
  if (error)  return
  !
end subroutine wdata
