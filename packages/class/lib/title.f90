subroutine class_title(set,line,r,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_title
  use class_data
  use class_types
  !----------------------------------------------------------------------
  ! @ public (for libclass only)
  ! CLASS support routine for command line
  !       TITLE
  ! 1           [/INDEX]
  ! 2           [/OBS]
  ! 3           [/BRIEF]
  ! 4           [/LONG]
  ! 5           [/FULL]
  !  Draw a header above the box
  !----------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  character(len=*),    intent(in)    :: line   ! Input command line
  type(observation),   intent(in)    :: r      !
  logical,             intent(inout) :: error  ! Error flag
  ! Local
  character(len=*), parameter :: rname='TITLE'
  real(4) :: x,y,expa,cdef,lx2,ly2
  character(len=1) :: key1,key2
  !
  ! Command line parsing for title format
  key1 = set%heade
  if (sic_present(3,0)) key1 = 'B'
  if (sic_present(4,0)) key1 = 'L'
  if (sic_present(5,0)) key1 = 'F'
  !
  ! Command line parsing for action level
  key2 = set%action
  if (sic_present(1,0).and.sic_present(2,0)) then
     call class_message(seve%e,rname,'/INDEX and /OBS are exclusive from each other')
     error = .true.
     return
  else if (sic_present(1,0)) then
     key2 = 'I'
  else if (sic_present(2,0)) then
     key2 = 'O'
  endif
  !
  if (key2.eq.'I') then
     if (.not.associated(p%data2)) then
        call class_message(seve%e,rname,'No index loaded')
        error = .true.
        return
     endif
  endif
  !
  call sic_get_real('PAGE_X',lx2,error)
  call sic_get_real('PAGE_Y',ly2,error)
  call sic_get_real('CHARACTER_SIZE',cdef,error)
  expa = 1.0
  x = lx2/2.0
  y = ly2-cdef*0.575*expa
  call out0('Graphic',x,y,error)
  if (key2.eq.'I') then
     call titout(set,p%head,key1,key2)
  else if (key2.eq.'O') then
     call titout(set,r%head,key1,key2)
  else
     call class_message(seve%e,rname,'Unknown action level: '//key2)
     error = .true.
     return
  endif
  !
end subroutine class_title
