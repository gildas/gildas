module class_index
  use classic_params
  use class_types 
  !
  type (indx_t) :: smin    ! Valeurs minimales pour FIND
  type (indx_t) :: smax    ! Valeurs maximales pour FIND
  !
  type (optimize), target, save :: ix  ! Index du fichier d'entree
  type (optimize), target, save :: ox  ! Index du fichier de sortie
  type (optimize), target, save :: cx  ! Index de la selection courante
  !
  type (flag_t), save :: flg     ! Flag for selecting data
  !
  ! Index numbers of observation in memory. If 0 : no obs in memory,
  ! if -1 : an obs in memory that  was not read in a file.
  ! knext : pointer to the next index entry to consider
  integer(kind=entry_length) :: knext          ! Pointer to next observation
  integer(kind=entry_length) :: nindex         ! Index size
  integer(kind=entry_length) :: last_xnum = 0  ! Last observation obtained by GET
end module class_index
!
module class_data
  use class_types
  !
  type(observation), target  :: P  ! Plotted observation (from LOAD)
  !
  real(kind=xdata_kind), allocatable, target :: pdatas(:), pdatai(:), pdatav(:), pdatax(:)
  real, allocatable, target :: pdataw(:)
  real, allocatable, target :: pdata2(:,:)
end module class_data
