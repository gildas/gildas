subroutine las_setvar(set,line,r,error)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>las_setvar
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !  SET VARIABLE Section_name [READ|WRITE|OFF]
  ! This commands has 2 purposes:
  !  - change the RO/RW status of the R%HEAD%Section,
  !  - create/remove aliases to this section
  !---------------------------------------------------------------------
  type(class_setup_t), intent(inout) :: set    !
  character(len=*),    intent(in)    :: line   !
  type(observation),   intent(inout) :: r      !
  logical,             intent(inout) :: error  !
  ! Local
  integer(kind=4) :: nkey,nc,mode
  character(len=12) :: argum,keywor
  integer, parameter :: mvocab1=24
  character(len=12) :: vocab1(mvocab1)
  ! Data
  data vocab1 /'GENERAL','POSITION','SPECTRO','RESOLUTION','BASE',        &
    'HISTORY','PLOT','FSWITCH','SWITCH','CALIBRATION','SKYDIP','GAUSS',   &
    'SHELL','NH3','HFS','ABSORPTION','DRIFT','BEAM', 'CONTINUUM',         &
    'ASSOCIATED','HERSCHEL','SECTIONS','COMMENT','USER' /
  !
  ! Access mode
  argum = setvar_modes(setvar_read)
  call sic_ke (line,0,3,argum,nc,.false.,error)
  if (error) return
  call sic_ambigs('SET',argum,keywor,mode,setvar_modes,setvar_mmodes,error)
  if (error) return
  !
  ! Section
  call sic_ke (line,0,2,argum,nc,.true.,error)
  if (error) return
  call sic_ambigs('SET',argum,keywor,nkey,vocab1,mvocab1,error)
  if (error) return
  !
  call las_setvar_R(set,r,keywor,mode,error)
  if (error)  return
  !
end subroutine las_setvar
!
subroutine class_variable(set,line,error,user_function)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_variable
  use class_parameter
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! Support routine for command
  !   VARIABLE Section1 [... SectionN]
  ! 1   /MODE  READ|WRITE|OFF
  ! 2   /INDEX
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  character(len=*),    intent(in)    :: line           ! Input command line
  logical,             intent(inout) :: error          ! Logical error flag
  logical,             external      :: user_function  !
  ! Local
  character(len=*), parameter :: rname='VARIABLE'
  integer(kind=4) :: nkey,nc,mode,iarg
  character(len=12) :: argum,keywor
  logical :: presec,readsec(-mx_sec:0)  ! Which section are to be added
  ! Data
  integer, parameter :: msections=22
  character(len=12) :: sections(msections)
  data sections /'SECTIONS','GENERAL','POSITION','SPECTRO','RESOLUTION',      &
    'BASE','HISTORY','PLOT','FSWITCH','SWITCH','CALIBRATION','SKYDIP',        &
    'GAUSS','SHELL','HFS','ABSORPTION','DRIFT','BEAM','CONTINUUM','COMMENT',  &
    'USER','HERSCHEL' /
  !
  ! Sections
  presec = .false.
  readsec(:) = .false.  ! None but:
  do iarg=1,sic_narg(0)
    call sic_ke(line,0,iarg,argum,nc,.true.,error)
    if (error) return
    call sic_ambigs(rname,argum,keywor,nkey,sections,msections,error)
    if (error) return
    select case (keywor)
    case ('SECTIONS')
      presec = .true.
    case ('GENERAL')
      readsec(class_sec_gen_id) = .true.
    case ('POSITION')
      readsec(class_sec_pos_id) = .true.
    case ('SPECTRO')
      readsec(class_sec_spe_id) = .true.
    case ('RESOLUTION')
      readsec(class_sec_res_id) = .true.
    case ('BASE')
      readsec(class_sec_bas_id) = .true.
    case ('HISTORY')
      readsec(class_sec_his_id) = .true.
    case ('PLOT')
      readsec(class_sec_plo_id) = .true.
    case ('FSWITCH','SWITCH')
      readsec(class_sec_swi_id) = .true.
    case ('CALIBRATION')
      readsec(class_sec_cal_id) = .true.
    case ('SKYDIP')
      readsec(class_sec_sky_id) = .true.
    case ('GAUSS')
      readsec(class_sec_gau_id) = .true.
    case ('SHELL')
      readsec(class_sec_she_id) = .true.
    case ('HFS')
      readsec(class_sec_hfs_id) = .true.
    case ('ABSORPTION')
      readsec(class_sec_abs_id) = .true.
    case ('DRIFT')
      readsec(class_sec_dri_id) = .true.
    case ('BEAM')
      readsec(class_sec_bea_id) = .true.
    case ('CONTINUUM')
      readsec(class_sec_poi_id) = .true.
    case ('COMMENT')
      readsec(class_sec_com_id) = .true.
    case ('USER')
      readsec(class_sec_user_id) = .true.
    case ('HERSCHEL')
      readsec(class_sec_her_id) = .true.
    case default
      call class_message(seve%w,rname,  &
        'Section '//trim(keywor)//' not implemented')
    end select
  enddo
  !
  ! Access mode
  argum = setvar_modes(setvar_read)
  call sic_ke (line,1,1,argum,nc,.false.,error)
  if (error) return
  call sic_ambigs(rname,argum,keywor,mode,setvar_modes,setvar_mmodes,error)
  if (error) return
  !
  if (sic_present(2,0)) then  ! /INDEX
    call class_variable_index(set,presec,readsec,mode,error,user_function)
    if (error)  return
  else
    call class_message(seve%e,rname,'Only /INDEX is implemented')
    error = .true.
    return
  endif
  !
end subroutine class_variable
!
subroutine las_setvar_R(set,r,keywor,mode,error)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>las_setvar_R
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Instantiate the requested section in Sic structure R%HEAD
  !---------------------------------------------------------------------
  type(class_setup_t), intent(inout) :: set     !
  type(observation),   intent(inout) :: r       !
  character(len=*),    intent(in)    :: keywor  !
  integer(kind=4),     intent(in)    :: mode    ! setvar_off/read/write
  logical,             intent(inout) :: error   !
  ! Local
  character(len=*), parameter :: rname='SETVAR'
  logical :: userreq
  integer(kind=index_length) :: dim(4)
  !
  userreq = .false.
  !
  select case (keywor)
  case('GENERAL')
    call las_variables_rgen('R%HEAD',r,mode.ne.setvar_write,error)  ! Must be re-created before the aliases
    if (error)  return
    if (mode.eq.setvar_write) r%head%presec(class_sec_gen_id) = .true.  ! else unchanged
    call las_setvar_R_alias(class_sec_gen_id,mode.eq.setvar_off,error)
    if (error)  return
    set%varpresec(class_sec_gen_id) = mode
    !
  case('POSITION')
    call las_variables_rpos('R%HEAD',r,mode.ne.setvar_write,error)
    if (error)  return
    if (mode.eq.setvar_write) r%head%presec(class_sec_pos_id) = .true.
    call las_setvar_R_alias(class_sec_pos_id,mode.eq.setvar_off,error)
    if (error)  return
    set%varpresec(class_sec_pos_id) = mode
    !
  case('SPECTRO')
    call las_variables_rspe('R%HEAD',r,mode.ne.setvar_write,error)
    if (error)  return
    if (mode.eq.setvar_write) r%head%presec(class_sec_spe_id) = .true.
    call las_setvar_R_alias(class_sec_spe_id,mode.eq.setvar_off,error)
    if (error)  return
    set%varpresec(class_sec_spe_id) = mode
    !
  case('RESOLUTION')
    call las_variables_rres('R%HEAD',r,mode.ne.setvar_write,error)
    if (error)  return
    if (mode.eq.setvar_write) r%head%presec(class_sec_res_id) = .true.
    call las_setvar_R_alias(class_sec_res_id,mode.eq.setvar_off,error)
    if (error)  return
    set%varpresec(class_sec_res_id) = mode
    !
  case('BASE')
    call las_variables_rbas('R%HEAD',r,mode.ne.setvar_write,error)
    if (error)  return
    if (mode.eq.setvar_write) r%head%presec(class_sec_bas_id) = .true.
    call las_setvar_R_alias(class_sec_bas_id,mode.eq.setvar_off,error)
    if (error)  return
    set%varpresec(class_sec_bas_id) = mode
    !
  case('HISTORY')
    call las_variables_rhis('R%HEAD',r,mode.ne.setvar_write,error)
    if (error)  return
    if (mode.eq.setvar_write) r%head%presec(class_sec_his_id) = .true.
    call las_setvar_R_alias(class_sec_his_id,mode.eq.setvar_off,error)
    if (error)  return
    set%varpresec(class_sec_his_id) = mode
    !
  case('PLOT')
    call las_variables_rplo('R%HEAD',r,mode.ne.setvar_write,error)
    if (error)  return
    if (mode.eq.setvar_write) r%head%presec(class_sec_plo_id) = .true.
    call las_setvar_R_alias(class_sec_plo_id,mode.eq.setvar_off,error)
    if (error)  return
    set%varpresec(class_sec_plo_id) = mode
    !
  case('FSWITCH','SWITCH')
    call las_variables_rswi('R%HEAD',r,mode.ne.setvar_write,error)
    if (error)  return
    if (mode.eq.setvar_write) r%head%presec(class_sec_swi_id) = .true.
    call las_setvar_R_alias(class_sec_swi_id,mode.eq.setvar_off,error)
    if (error)  return
    set%varpresec(class_sec_swi_id) = mode
    !
  case('CALIBRATION')
    call las_variables_rcal('R%HEAD',r,mode.ne.setvar_write,error)
    if (error)  return
    if (mode.eq.setvar_write) r%head%presec(class_sec_cal_id)=.true.
    call las_setvar_R_alias(class_sec_cal_id,mode.eq.setvar_off,error)
    if (error)  return
    set%varpresec(class_sec_cal_id) = mode
    !
  case('SKYDIP')
    call las_variables_rsky('R%HEAD',r,mode.ne.setvar_write,error)
    if (error)  return
    if (mode.eq.setvar_write) r%head%presec(class_sec_sky_id)=.true.
    call las_setvar_R_alias(class_sec_sky_id,mode.eq.setvar_off,error)
    if (error)  return
    set%varpresec(class_sec_sky_id) = mode
    !
  case('GAUSS')
    call las_variables_rgau('R%HEAD',r,mode.ne.setvar_write,error)
    if (error)  return
    if (mode.eq.setvar_write) r%head%presec(class_sec_gau_id) = .true.
    call las_setvar_R_alias(class_sec_gau_id,mode.eq.setvar_off,error)
    if (error)  return
    set%varpresec(class_sec_gau_id) = mode
    !
  case('SHELL')
    call las_variables_rshe('R%HEAD',r,mode.ne.setvar_write,error)
    if (error)  return
    if (mode.eq.setvar_write) r%head%presec(class_sec_she_id) = .true.
    call las_setvar_R_alias(class_sec_she_id,mode.eq.setvar_off,error)
    if (error)  return
    set%varpresec(class_sec_she_id) = mode
    !
  case('NH3','HFS')
    call las_variables_rhfs('R%HEAD',r,mode.ne.setvar_write,error)
    if (error)  return
    if (mode.eq.setvar_write) r%head%presec(class_sec_hfs_id) = .true.
    call las_setvar_R_alias(class_sec_hfs_id,mode.eq.setvar_off,error)
    if (error)  return
    set%varpresec(class_sec_hfs_id) = mode
    !
  case('ABS')
    call las_variables_rabs('R%HEAD',r,mode.ne.setvar_write,error)
    if (error)  return
    if (mode.eq.setvar_write) r%head%presec(class_sec_abs_id)=.true.
    call las_setvar_R_alias(class_sec_abs_id,mode.eq.setvar_off,error)
    if (error)  return
    set%varpresec(class_sec_abs_id) = mode
    !
  case('DRIFT')
    call las_variables_rdri('R%HEAD',r,mode.ne.setvar_write,error)
    if (error)  return
    if (mode.eq.setvar_write) r%head%presec(class_sec_dri_id) = .true.
    call las_setvar_R_alias(class_sec_dri_id,mode.eq.setvar_off,error)
    if (error)  return
    set%varpresec(class_sec_dri_id) = mode
    !
  case('BEAM')
    call las_variables_rbea('R%HEAD',r,mode.ne.setvar_write,error)
    if (error)  return
    if (mode.eq.setvar_write) r%head%presec(class_sec_bea_id) = .true.
    call las_setvar_R_alias(class_sec_bea_id,mode.eq.setvar_off,error)
    if (error)  return
    set%varpresec(class_sec_bea_id) = mode
    !
  case('CONTINUUM')
    call las_variables_rpoi('R%HEAD',r,mode.ne.setvar_write,error)
    if (error)  return
    if (mode.eq.setvar_write) r%head%presec(class_sec_poi_id)=.true.
    call las_setvar_R_alias(class_sec_poi_id,mode.eq.setvar_off,error)
    if (error)  return
    set%varpresec(class_sec_poi_id) = mode
    !
  case('HERSCHEL')
    call las_variables_rher('R%HEAD',r,mode.ne.setvar_write,error)
    if (error)  return
    if (mode.eq.setvar_write) r%head%presec(class_sec_her_id)=.true.
    call las_setvar_R_alias(class_sec_her_id,mode.eq.setvar_off,error)
    if (error)  return
    set%varpresec(class_sec_her_id) = mode
    !
  case('ASSOCIATED')
    set%varpresec(class_sec_assoc_id) = mode  ! New mode
    call newdat_assoc(set,r,error)  ! Must come after the new mode
    if (error)  return
    ! if (mode.eq.setvar_write) r%head%presec(class_sec_assoc_id)=.true.
    !     No: activating the section with SET VAR ASSOC WR is useless without
    !     adding an Associated Array. Use command ASSOCIATE instead.
    ! call las_setvar_R_alias(class_sec_her_id,mode.eq.setvar_off,error)
    !     No: no aliases onto the assoc arrays...
    !
  case('SECTIONS')
     call sic_delvariable('R%HEAD%PRESEC',userreq,error)
     error = .false.  ! Attempt to delete non-existent variables is not an error
     if (mode.eq.setvar_off) return
     !
     dim(1) = mx_sec+1
     call sic_def_login('R%HEAD%PRESEC',r%head%presec(-mx_sec),1,dim,mode.ne.setvar_write,error)
     !
  case('COMMENT')
     if (.not.r%head%presec(class_sec_com_id)) then
        call class_message(seve%w,rname,'Comment section does not exist')
        return
     elseif (mode.eq.setvar_write) then
        call class_message(seve%e,rname,  &
          'Keyword WRITE not possible, use command COMMENT')
        error = .true.
        return
     else
        call sic_delvariable('COMMENT',userreq,error)
        error = .false.  ! Attempt to delete non-existent variables is not an error
        if (mode.eq.setvar_off) return
        call sic_def_char('COMMENT',r%head%com%ctext,mode.ne.setvar_write,error)
     endif
     set%varpresec(class_sec_com_id) = mode
     !
  case('USER')
     set%varpresec(class_sec_user_id) = mode  ! Must be set before calling newdat_user
     call user_sec_setvar(set,r,mode.eq.setvar_off,error)
     if (error)  return
     !
  case default
     call class_message(seve%e,rname,'Unknown section '//keywor)
     error = .true.
  end select
  !
end subroutine las_setvar_R
!
subroutine las_setvar_R_alias(code,delete,error)
  use gkernel_interfaces
  use classcore_interfaces, except_this=>las_setvar_R_alias
  use class_parameter
  !---------------------------------------------------------------------
  ! @ private
  !  Delete and/or (re)create aliases to the section elements.
  !  NB: aliases inherit the RW/RO status of their targets
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)    :: code    ! Section code
  logical,         intent(in)    :: delete  ! Delete or create aliases?
  logical,         intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  !
  userreq = .false.
  !
  select case (code)
  case (class_sec_gen_id)
    call sic_delvariable('NUM',userreq,error)
    call sic_delvariable('VER',userreq,error)
    call sic_delvariable('TELES',userreq,error)
    call sic_delvariable('DOBS',userreq,error)
    call sic_delvariable('DRED',userreq,error)
    call sic_delvariable('CDOBS',userreq,error)
    call sic_delvariable('CDRED',userreq,error)
    call sic_delvariable('KIND',userreq,error)
    call sic_delvariable('QUAL',userreq,error)
    call sic_delvariable('UT',userreq,error)
    call sic_delvariable('ST',userreq,error)
    call sic_delvariable('AZ',userreq,error)
    call sic_delvariable('EL',userreq,error)
    call sic_delvariable('TAU',userreq,error)
    error = .false.  ! Attempt to delete non-existent variables is not an error
    if (delete) return
    call sic_def_alias('NUM',  'R%HEAD%GEN%NUM',  .true.,error)
    call sic_def_alias('VER',  'R%HEAD%GEN%VER',  .true.,error)
    call sic_def_alias('TELES','R%HEAD%GEN%TELES',.true.,error)
    call sic_def_alias('DOBS', 'R%HEAD%GEN%DOBS', .true.,error)
    call sic_def_alias('DRED', 'R%HEAD%GEN%DRED', .true.,error)
    call sic_def_alias('CDOBS','R%HEAD%GEN%CDOBS',.true.,error)
    call sic_def_alias('CDRED','R%HEAD%GEN%CDRED',.true.,error)
    call sic_def_alias('KIND', 'R%HEAD%GEN%KIND', .true.,error)
    call sic_def_alias('QUAL', 'R%HEAD%GEN%QUAL', .true.,error)
    call sic_def_alias('UT',   'R%HEAD%GEN%UT',   .true.,error)
    call sic_def_alias('ST',   'R%HEAD%GEN%ST',   .true.,error)
    call sic_def_alias('AZ',   'R%HEAD%GEN%AZ',   .true.,error)
    call sic_def_alias('EL',   'R%HEAD%GEN%EL',   .true.,error)
    call sic_def_alias('TAU',  'R%HEAD%GEN%TAU',  .true.,error)
    if (error)  return
    !
  case (class_sec_pos_id)
    call sic_delvariable('SOURC',userreq,error)
    call sic_delvariable('EQUINOX',userreq,error)
    call sic_delvariable('LAM',userreq,error)
    call sic_delvariable('BET',userreq,error)
    call sic_delvariable('LAMOF',userreq,error)
    call sic_delvariable('BETOF',userreq,error)
    call sic_delvariable('PROJ',userreq,error)
    error = .false.  ! Attempt to delete non-existent variables is not an error
    if (delete) return
    call sic_def_alias('SOURC',  'R%HEAD%POS%SOURC',  .true.,error)
    call sic_def_alias('EQUINOX','R%HEAD%POS%EQUINOX',.true.,error)
    call sic_def_alias('LAM',    'R%HEAD%POS%LAM',    .true.,error)
    call sic_def_alias('BET',    'R%HEAD%POS%BET',    .true.,error)
    call sic_def_alias('LAMOF',  'R%HEAD%POS%LAMOF',  .true.,error)
    call sic_def_alias('BETOF',  'R%HEAD%POS%BETOF',  .true.,error)
    call sic_def_alias('PROJ',   'R%HEAD%POS%PROJ',   .true.,error)
    if (error)  return
    !
  case (class_sec_spe_id)
    call sic_delvariable('LINE',userreq,error)
    call sic_delvariable('NCHAN',userreq,error)
    call sic_delvariable('RESTF',userreq,error)
    call sic_delvariable('RCHAN',userreq,error)
    call sic_delvariable('FRES',userreq,error)
    call sic_delvariable('VRES',userreq,error)
    call sic_delvariable('VOFF',userreq,error)
    call sic_delvariable('BAD',userreq,error)
    call sic_delvariable('IMAGE',userreq,error)
    call sic_delvariable('VTYPE',userreq,error)
    call sic_delvariable('SKYFR',userreq,error)
    call sic_delvariable('VTELES',userreq,error)
    call sic_delvariable('DOPPLER',userreq,error)
    error = .false.  ! Attempt to delete non-existent variables is not an error
    if (delete) return
    call sic_def_alias('LINE',   'R%HEAD%SPE%LINE',   .true.,error)
    call sic_def_alias('NCHAN',  'R%HEAD%SPE%NCHAN',  .true.,error)
    call sic_def_alias('RESTF',  'R%HEAD%SPE%RESTF',  .true.,error)
    call sic_def_alias('RCHAN',  'R%HEAD%SPE%RCHAN',  .true.,error)
    call sic_def_alias('FRES',   'R%HEAD%SPE%FRES',   .true.,error)
    call sic_def_alias('VRES',   'R%HEAD%SPE%VRES',   .true.,error)
    call sic_def_alias('VOFF',   'R%HEAD%SPE%VOFF',   .true.,error)
    call sic_def_alias('BAD',    'R%HEAD%SPE%BAD',    .true.,error)
    call sic_def_alias('IMAGE',  'R%HEAD%SPE%IMAGE',  .true.,error)
    call sic_def_alias('VTYPE',  'R%HEAD%SPE%VTYPE',  .true.,error)
  ! call sic_def_alias('SKYFR',  'R%HEAD%SPE%SKYFR',  .true.,error)
  ! call sic_def_alias('VTELES', 'R%HEAD%SPE%VTELES', .true.,error)
    call sic_def_alias('DOPPLER','R%HEAD%SPE%DOPPLER',.true.,error)
    if (error)  return
    !
  case (class_sec_bas_id)
    call sic_delvariable('DEG',userreq,error)
    call sic_delvariable('SIGFI',userreq,error)
    call sic_delvariable('AIRE',userreq,error)
    call sic_delvariable('NWIND',userreq,error)
    call sic_delvariable('W1',userreq,error)
    call sic_delvariable('W2',userreq,error)
    call sic_delvariable('SINUS',userreq,error)
    error = .false.  ! Attempt to delete non-existent variables is not an error
    if (delete) return
    call sic_def_alias('DEG',  'R%HEAD%BAS%DEG',  .true.,error)
    call sic_def_alias('SIGFI','R%HEAD%BAS%SIGFI',.true.,error)
    call sic_def_alias('AIRE', 'R%HEAD%BAS%AIRE', .true.,error)
    call sic_def_alias('NWIND','R%HEAD%BAS%NWIND',.true.,error)
    call sic_def_alias('W1',   'R%HEAD%BAS%W1',   .true.,error)
    call sic_def_alias('W2',   'R%HEAD%BAS%W2',   .true.,error)
    call sic_def_alias('SINUS','R%HEAD%BAS%SINUS',.true.,error)
    if (error)  return
    !
  case (class_sec_his_id)
    call sic_delvariable('NSEQ',userreq,error)
    call sic_delvariable('START',userreq,error)
    call sic_delvariable('END',userreq,error)
    error = .false.  ! Attempt to delete non-existent variables is not an error
    if (delete) return
    call sic_def_alias('NSEQ', 'R%HEAD%HIS%NSEQ', .true.,error)
    call sic_def_alias('START','R%HEAD%HIS%START',.true.,error)
    call sic_def_alias('END',  'R%HEAD%HIS%END',  .true.,error)
    if (error)  return
    !
  case (class_sec_plo_id)
    call sic_delvariable('AMIN',userreq,error)
    call sic_delvariable('AMAX',userreq,error)
    call sic_delvariable('VMIN',userreq,error)
    call sic_delvariable('VMAX',userreq,error)
    error = .false.  ! Attempt to delete non-existent variables is not an error
    if (delete) return
    call sic_def_alias('AMIN','R%HEAD%PLO%AMIN',.true.,error)
    call sic_def_alias('AMAX','R%HEAD%PLO%AMAX',.true.,error)
    call sic_def_alias('VMIN','R%HEAD%PLO%VMIN',.true.,error)
    call sic_def_alias('VMAX','R%HEAD%PLO%VMAX',.true.,error)
    if (error)  return
    !
  case (class_sec_swi_id)
    call sic_delvariable('NPHAS',userreq,error)
    call sic_delvariable('DECAL',userreq,error)
    call sic_delvariable('DUREE',userreq,error)
    call sic_delvariable('POIDS',userreq,error)
    call sic_delvariable('SWMOD',userreq,error)
    call sic_delvariable('LDECAL',userreq,error)
    call sic_delvariable('BDECAL',userreq,error)
    error = .false.  ! Attempt to delete non-existent variables is not an error
    if (delete) return
    call sic_def_alias('NPHAS', 'R%HEAD%FSW%NPHAS', .true.,error)
    call sic_def_alias('SWMOD', 'R%HEAD%FSW%SWMOD', .true.,error)
    call sic_def_alias('DECAL', 'R%HEAD%FSW%DECAL', .true.,error)
    call sic_def_alias('DUREE', 'R%HEAD%FSW%DUREE', .true.,error)
    call sic_def_alias('POIDS', 'R%HEAD%FSW%POIDS', .true.,error)
    call sic_def_alias('LDECAL','R%HEAD%FSW%LDECAL',.true.,error)
    call sic_def_alias('BDECAL','R%HEAD%FSW%BDECAL',.true.,error)
    if (error)  return
    !
  case (class_sec_cal_id)
    call sic_delvariable('BEEFF',userreq,error)
    call sic_delvariable('FOEFF',userreq,error)
    call sic_delvariable('GAINI',userreq,error)
    call sic_delvariable('H2OMM',userreq,error)
    call sic_delvariable('PAMB',userreq,error)
    call sic_delvariable('TAMB',userreq,error)
    call sic_delvariable('TATMS',userreq,error)
    call sic_delvariable('TCHOP',userreq,error)
    call sic_delvariable('TCOLD',userreq,error)
    call sic_delvariable('TAUS',userreq,error)
    call sic_delvariable('TAUI',userreq,error)
    call sic_delvariable('TATMI',userreq,error)
    call sic_delvariable('TREC',userreq,error)
    call sic_delvariable('CMODE',userreq,error)
    call sic_delvariable('ATFAC',userreq,error)
    call sic_delvariable('ALTI',userreq,error)
    call sic_delvariable('COUNT',userreq,error)
    call sic_delvariable('LCALOF',userreq,error)
    call sic_delvariable('BCALOF',userreq,error)
    call sic_delvariable('GEOLONG',userreq,error)
    call sic_delvariable('GEOLAT',userreq,error)
    error = .false.  ! Attempt to delete non-existent variables is not an error
    if (delete) return
    call sic_def_alias('BEEFF',  'R%HEAD%CAL%BEEFF',  .true.,error)
    call sic_def_alias('FOEFF',  'R%HEAD%CAL%FOEFF',  .true.,error)
    call sic_def_alias('GAINI',  'R%HEAD%CAL%GAINI',  .true.,error)
    call sic_def_alias('H2OMM',  'R%HEAD%CAL%H2OMM',  .true.,error)
    call sic_def_alias('PAMB',   'R%HEAD%CAL%PAMB',   .true.,error)
    call sic_def_alias('TAMB',   'R%HEAD%CAL%TAMB',   .true.,error)
    call sic_def_alias('TATMS',  'R%HEAD%CAL%TATMS',  .true.,error)
    call sic_def_alias('TCHOP',  'R%HEAD%CAL%TCHOP',  .true.,error)
    call sic_def_alias('TCOLD',  'R%HEAD%CAL%TCOLD',  .true.,error)
    call sic_def_alias('TAUS',   'R%HEAD%CAL%TAUS',   .true.,error)
    call sic_def_alias('TAUI',   'R%HEAD%CAL%TAUI',   .true.,error)
    call sic_def_alias('TATMI',  'R%HEAD%CAL%TATMI',  .true.,error)
    call sic_def_alias('TREC',   'R%HEAD%CAL%TREC',   .true.,error)
    call sic_def_alias('CMODE',  'R%HEAD%CAL%CMODE',  .true.,error)
    call sic_def_alias('ATFAC',  'R%HEAD%CAL%ATFAC',  .true.,error)
    call sic_def_alias('ALTI',   'R%HEAD%CAL%ALTI',   .true.,error)
    call sic_def_alias('COUNT',  'R%HEAD%CAL%COUNT',  .true.,error)
    call sic_def_alias('LCALOF', 'R%HEAD%CAL%LCALOF', .true.,error)
    call sic_def_alias('BCALOF', 'R%HEAD%CAL%BCALOF', .true.,error)
    call sic_def_alias('GEOLONG','R%HEAD%CAL%GEOLONG',.true.,error)
    call sic_def_alias('GEOLAT', 'R%HEAD%CAL%GEOLAT', .true.,error)
    if (error)  return
    !
  case (class_sec_sky_id)
    call sic_delvariable('SREST',userreq,error)
    call sic_delvariable('SIMAG',userreq,error)
    call sic_delvariable('NSKY',userreq,error)
    call sic_delvariable('NCHOP',userreq,error)
    call sic_delvariable('NCOLD',userreq,error)
    call sic_delvariable('ELEV',userreq,error)
    call sic_delvariable('EMISS',userreq,error)
    call sic_delvariable('CHOPP',userreq,error)
    call sic_delvariable('COLD',userreq,error)
    error = .false.  ! Attempt to delete non-existent variables is not an error
    if (delete) return
    call sic_def_alias('SREST','R%HEAD%SKY%RESTF',.true.,error)
    call sic_def_alias('SIMAG','R%HEAD%SKY%IMAGE',.true.,error)
    call sic_def_alias('NSKY', 'R%HEAD%SKY%NSKY', .true.,error)
    call sic_def_alias('NCHOP','R%HEAD%SKY%NCHOP',.true.,error)
    call sic_def_alias('NCOLD','R%HEAD%SKY%NCOLD',.true.,error)
    call sic_def_alias('ELEV', 'R%HEAD%SKY%ELEV', .true.,error)
    call sic_def_alias('EMISS','R%HEAD%SKY%EMISS',.true.,error)
    call sic_def_alias('CHOPP','R%HEAD%SKY%CHOPP',.true.,error)
    call sic_def_alias('COLD', 'R%HEAD%SKY%COLD', .true.,error)
    if (error)  return
    !
  case (class_sec_gau_id)
    call sic_delvariable('NLINE',userreq,error)
    call sic_delvariable('SIGBA',userreq,error)
    call sic_delvariable('SIGRA',userreq,error)
    call sic_delvariable('NFIT',userreq,error)
    call sic_delvariable('NERR',userreq,error)
    error = .false.  ! Attempt to delete non-existent variables is not an error
    if (delete) return
    call sic_def_alias('NLINE','R%HEAD%GAU%NLINE',.true.,error)
    call sic_def_alias('SIGBA','R%HEAD%GAU%RMS_BASE',.true.,error)
    call sic_def_alias('SIGRA','R%HEAD%GAU%RMS_LINE',.true.,error)
    call sic_def_alias('NFIT', 'R%HEAD%GAU%RESULT', .true.,error)
    call sic_def_alias('NERR', 'R%HEAD%GAU%ERROR', .true.,error)
    if (error)  return
    !
  case (class_sec_she_id)
    call sic_delvariable('LSHEL',userreq,error)
    call sic_delvariable('BSHEL',userreq,error)
    call sic_delvariable('RSHEL',userreq,error)
    call sic_delvariable('NSHEL',userreq,error)
    call sic_delvariable('ESHEL',userreq,error)
    error = .false.  ! Attempt to delete non-existent variables is not an error
    if (delete) return
    call sic_def_alias('LSHEL','R%HEAD%SHE%NLINE',.true.,error)
    call sic_def_alias('BSHEL','R%HEAD%SHE%RMS_BASE',.true.,error)
    call sic_def_alias('RSHEL','R%HEAD%SHE%RMS_LINE',.true.,error)
    call sic_def_alias('NSHEL','R%HEAD%SHE%RESULT', .true.,error)
    call sic_def_alias('ESHEL','R%HEAD%SHE%ERROR', .true.,error)
    if (error)  return
    !
  case (class_sec_hfs_id)
    call sic_delvariable('LNH3',userreq,error)
    call sic_delvariable('BNH3',userreq,error)
    call sic_delvariable('RNH3',userreq,error)
    call sic_delvariable('NNH3',userreq,error)
    call sic_delvariable('ENH3',userreq,error)
    error = .false.  ! Attempt to delete non-existent variables is not an error
    if (delete) return
    call sic_def_alias('LNH3','R%HEAD%HFS%NLINE',.true.,error)
    call sic_def_alias('BNH3','R%HEAD%HFS%RMS_BASE',.true.,error)
    call sic_def_alias('RNH3','R%HEAD%HFS%RMS_LINE',.true.,error)
    call sic_def_alias('NNH3','R%HEAD%HFS%RESULT', .true.,error)
    call sic_def_alias('ENH3','R%HEAD%HFS%ERROR', .true.,error)
    if (error)  return
    !
  case (class_sec_abs_id)
    call sic_delvariable('LABS',userreq,error)
    call sic_delvariable('BABS',userreq,error)
    call sic_delvariable('RABS',userreq,error)
    call sic_delvariable('NABS',userreq,error)
    call sic_delvariable('EABS',userreq,error)
    error = .false.  ! Attempt to delete non-existent variables is not an error
    if (delete) return
    call sic_def_alias('LABS','R%HEAD%ABS%NLINE',.true.,error)
    call sic_def_alias('BABS','R%HEAD%ABS%RMS_BASE',.true.,error)
    call sic_def_alias('RABS','R%HEAD%ABS%RMS_LINE',.true.,error)
    call sic_def_alias('NABS','R%HEAD%ABS%RESULT', .true.,error)
    call sic_def_alias('EABS','R%HEAD%ABS%ERROR', .true.,error)
    if (error)  return
    !
  case (class_sec_dri_id)
    call sic_delvariable('FREQ',userreq,error)
    call sic_delvariable('WIDTH',userreq,error)
    call sic_delvariable('NPOIN',userreq,error)
    call sic_delvariable('RPOIN',userreq,error)
    call sic_delvariable('TREF',userreq,error)
    call sic_delvariable('AREF',userreq,error)
    call sic_delvariable('TRES',userreq,error)
    call sic_delvariable('ARES',userreq,error)
    call sic_delvariable('APOS',userreq,error)
    call sic_delvariable('CBAD',userreq,error)
    call sic_delvariable('CTYPE',userreq,error)
    call sic_delvariable('CIMAG',userreq,error)
    call sic_delvariable('COLLA',userreq,error)
    call sic_delvariable('COLLE',userreq,error)
    error = .false.  ! Attempt to delete non-existent variables is not an error
    if (delete) return
    call sic_def_alias('FREQ', 'R%HEAD%DRI%FREQ', .true.,error)
    call sic_def_alias('WIDTH','R%HEAD%DRI%WIDTH',.true.,error)
    call sic_def_alias('NPOIN','R%HEAD%DRI%NPOIN',.true.,error)
    call sic_def_alias('RPOIN','R%HEAD%DRI%RPOIN',.true.,error)
    call sic_def_alias('TREF', 'R%HEAD%DRI%TREF', .true.,error)
    call sic_def_alias('AREF', 'R%HEAD%DRI%AREF', .true.,error)
    call sic_def_alias('TRES', 'R%HEAD%DRI%TRES', .true.,error)
    call sic_def_alias('ARES', 'R%HEAD%DRI%ARES', .true.,error)
    call sic_def_alias('APOS', 'R%HEAD%DRI%APOS', .true.,error)
    call sic_def_alias('CBAD', 'R%HEAD%DRI%BAD',  .true.,error)
    call sic_def_alias('CTYPE','R%HEAD%DRI%CTYPE',.true.,error)
    call sic_def_alias('CIMAG','R%HEAD%DRI%CIMAG',.true.,error)
    call sic_def_alias('COLLA','R%HEAD%DRI%COLLA',.true.,error)
    call sic_def_alias('COLLE','R%HEAD%DRI%COLLE',.true.,error)
    if (error)  return
    !
  case (class_sec_bea_id)
    call sic_delvariable('CAZIM',userreq,error)
    call sic_delvariable('CELEV',userreq,error)
    call sic_delvariable('SPACE',userreq,error)
    call sic_delvariable('BPOS',userreq,error)
    call sic_delvariable('BTYPE',userreq,error)
    error = .false.  ! Attempt to delete non-existent variables is not an error
    if (delete) return
    call sic_def_alias('CAZIM','R%HEAD%BEA%CAZIM',.true.,error)
    call sic_def_alias('CELEV','R%HEAD%BEA%CELEV',.true.,error)
    call sic_def_alias('SPACE','R%HEAD%BEA%SPACE',.true.,error)
    call sic_def_alias('BPOS', 'R%HEAD%BEA%BPOS', .true.,error)
    call sic_def_alias('BTYPE','R%HEAD%BEA%BTYPE',.true.,error)
    if (error)  return
    !
  case (class_sec_poi_id)
    call sic_delvariable('LCONT',userreq,error)
    call sic_delvariable('BCONT',userreq,error)
    call sic_delvariable('RCONT',userreq,error)
    call sic_delvariable('NCONT',userreq,error)
    call sic_delvariable('ECONT',userreq,error)
    error = .false.  ! Attempt to delete non-existent variables is not an error
    if (delete) return
    call sic_def_alias('LCONT','R%HEAD%POI%NLINE',.true.,error)
    call sic_def_alias('BCONT','R%HEAD%POI%RMS_BASE',.true.,error)
    call sic_def_alias('RCONT','R%HEAD%POI%RMS_LINE',.true.,error)
    call sic_def_alias('NCONT','R%HEAD%POI%RESULT', .true.,error)
    call sic_def_alias('ECONT','R%HEAD%POI%ERROR', .true.,error)
    if (error)  return
    !
  case default
    ! No aliases for this section, not an error
    return
  end select
  !
end subroutine las_setvar_R_alias
!
subroutine las_setvar_R_aliases(set,error)
  use classcore_interfaces, except_this=>las_setvar_R_aliases
  use class_parameter
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  !  Recreate the aliases for ALL sections which are currently
  ! activated (SET VARIABLE Section ON|READ|WRITE)
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  integer(kind=4) :: isec
  !
  do isec=-mx_sec,0
    if (set%varpresec(isec).ne.setvar_off) then
      call las_setvar_R_alias(isec,.false.,error)
      if (error)  return
    endif
  enddo
  !
end subroutine las_setvar_R_aliases
!
!-----------------------------------------------------------------------
!
subroutine class_variable_index_reset(error)
  use classcore_interfaces, except_this=>class_variable_index_reset
  !---------------------------------------------------------------------
  ! @ private
  !  Reset the IDX% structure in Sic, probably because something has
  ! changed in CX.
  !---------------------------------------------------------------------
  logical, intent(inout) :: error
  !
  ! Destroy IDX%HEAD%: the support arrays are now outdated. Do not
  ! recompute them implicitly, this is too costly
  call sic_delvariable('IDX%HEAD',.false.,error)
  if (error)  error = .false.  ! No error if it does not exists
  call sic_delvariable('IDX%USER',.false.,error)
  if (error)  error = .false.  ! No error if it does not exists
  !
  ! Remap the CX arrays
  call class_variable_index_cx(error)
  if (error)  return
  !
end subroutine class_variable_index_reset
!
subroutine class_variable_index_cx(error)
  use classcore_dependencies_interfaces
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  !  Create the IDX% Sic structure mapping the CX arrays (always
  ! available)
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  ! Error status
  ! Local
  integer(kind=index_length) :: dim(4)
  !
  ! Create structure if needed
  if (.not.sic_varexist('idx')) then
     call sic_defstructure('idx%',.true.,error)
     call sic_def_logi('idx%oldfmt',cx%has_old_fmt,.false.,error)
     call sic_def_long('idx%time',cx%time,0,dim,.false.,error)
     if (error) return
  endif
  !
  ! Removed previously defined structure variable
  call sic_delvariable('idx%ind',    .false.,error)
  call sic_delvariable('idx%num',    .false.,error)
  call sic_delvariable('idx%ver',    .false.,error)
  call sic_delvariable('idx%kind',   .false.,error)
  call sic_delvariable('idx%qual',   .false.,error)
  call sic_delvariable('idx%scan',   .false.,error)
  call sic_delvariable('idx%subscan',.false.,error)
  call sic_delvariable('idx%dobs',   .false.,error)
  call sic_delvariable('idx%loff',   .false.,error)
  call sic_delvariable('idx%boff',   .false.,error)
  call sic_delvariable('idx%sourc',  .false.,error)
  call sic_delvariable('idx%line',   .false.,error)
  call sic_delvariable('idx%teles',  .false.,error)
  ! UT is not in index V1 or V2. Comment out IDX%UT as long as index V3
  ! is not the default and widely distributed.
! call sic_delvariable('idx%ut',     .false.,error)
  error = .false.
  !
  nindex = cx%next-1
  if (nindex.gt.0) then
     dim(1) = nindex
     call sic_def_long ('idx%ind',    cx%ind,    1,dim,.false.,error)
     call sic_def_long ('idx%num',    cx%num,    1,dim,.false.,error)
     call sic_def_inte ('idx%ver',    cx%ver,    1,dim,.false.,error)
     call sic_def_inte ('idx%kind',   cx%kind,   1,dim,.false.,error)
     call sic_def_inte ('idx%qual',   cx%qual,   1,dim,.false.,error)
     call sic_def_long ('idx%scan',   cx%scan,   1,dim,.false.,error)
     call sic_def_inte ('idx%subscan',cx%subscan,1,dim,.false.,error)
     call sic_def_inte ('idx%dobs',   cx%dobs,   1,dim,.false.,error)
     call sic_def_real ('idx%loff',   cx%off1,   1,dim,.false.,error)
     call sic_def_real ('idx%boff',   cx%off2,   1,dim,.false.,error)
     call sic_def_charn('idx%sourc',  cx%csour,  1,dim,.false.,error)
     call sic_def_charn('idx%line',   cx%cline,  1,dim,.false.,error)
     call sic_def_charn('idx%teles',  cx%ctele,  1,dim,.false.,error)
   ! call sic_def_dble ('idx%ut',     cx%ut,     1,dim,.false.,error)
     error = .false.
  endif
  !
end subroutine class_variable_index_cx
!
subroutine class_variable_index(set,presec,readsec,mode,error,user_function)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_variable_index
  use class_parameter
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create a section in the IDX%HEAD% Sic structure
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set                 !
  logical,             intent(in)    :: presec              ! Enable the 'present sections' array?
  logical,             intent(in)    :: readsec(-mx_sec:0)  ! Which section are to be added
  integer(kind=4),     intent(in)    :: mode                ! setvar_off/read/write
  logical,             intent(inout) :: error               !
  logical,             external      :: user_function       !
  !
  if (mode.eq.setvar_write) then
    call class_message(seve%e,'VARIABLE',  &
      '/MODE WRITE is exclusive with option /INDEX')
    error = .true.
    return
  endif
  !
  ! Delete support variables
  call class_variable_index_delvar(presec,readsec,error)
  if (error)  return
  !
  if (mode.eq.setvar_off) then  ! DELETE mode
    ! Free associated memory
    call class_variable_index_reallocate(presec,readsec,0_8,error)
    return  ! Nothing more
  endif
  !
  call class_variable_index_fill(set,presec,readsec,error,user_function)
  if (error)  return
  !
  ! Create support variables
  call class_variable_index_defvar(presec,readsec,error)
  if (error)  return
  !
end subroutine class_variable_index
!
subroutine class_variable_index_delvar(presec,readsec,error)
  use classcore_dependencies_interfaces
  use class_parameter
  !---------------------------------------------------------------------
  ! @ private
  !  Delete variables (structures) in IDX%HEAD%
  !---------------------------------------------------------------------
  logical, intent(in)    :: presec              ! Enable the 'present sections' array?
  logical, intent(in)    :: readsec(-mx_sec:0)  ! Which section are to be added
  logical, intent(inout) :: error               !
  !
  if (presec)  call sic_delvariable('IDX%HEAD%PRESEC',.false.,error)
  if (readsec(class_sec_gen_id))   call sic_delvariable('IDX%HEAD%GEN',.false.,error)
  if (readsec(class_sec_pos_id))   call sic_delvariable('IDX%HEAD%POS',.false.,error)
  if (readsec(class_sec_spe_id))   call sic_delvariable('IDX%HEAD%SPE',.false.,error)
  if (readsec(class_sec_res_id))   call sic_delvariable('IDX%HEAD%RES',.false.,error)
  if (readsec(class_sec_bas_id))   call sic_delvariable('IDX%HEAD%BAS',.false.,error)
  if (readsec(class_sec_his_id))   call sic_delvariable('IDX%HEAD%HIS',.false.,error)
  if (readsec(class_sec_plo_id))   call sic_delvariable('IDX%HEAD%PLO',.false.,error)
  if (readsec(class_sec_swi_id))   call sic_delvariable('IDX%HEAD%FSW',.false.,error)
  if (readsec(class_sec_cal_id))   call sic_delvariable('IDX%HEAD%CAL',.false.,error)
  if (readsec(class_sec_sky_id))   call sic_delvariable('IDX%HEAD%SKY',.false.,error)
  if (readsec(class_sec_gau_id))   call sic_delvariable('IDX%HEAD%GAU',.false.,error)
  if (readsec(class_sec_she_id))   call sic_delvariable('IDX%HEAD%SHE',.false.,error)
  if (readsec(class_sec_hfs_id))   call sic_delvariable('IDX%HEAD%HFS',.false.,error)
  if (readsec(class_sec_abs_id))   call sic_delvariable('IDX%HEAD%ABS',.false.,error)
  if (readsec(class_sec_dri_id))   call sic_delvariable('IDX%HEAD%DRI',.false.,error)
  if (readsec(class_sec_bea_id))   call sic_delvariable('IDX%HEAD%BEA',.false.,error)
  if (readsec(class_sec_poi_id))   call sic_delvariable('IDX%HEAD%POI',.false.,error)
  if (readsec(class_sec_com_id))   call sic_delvariable('IDX%HEAD%COM',.false.,error)
  if (readsec(class_sec_her_id))   call sic_delvariable('IDX%HEAD%HER',.false.,error)
  if (readsec(class_sec_user_id))  call sic_delvariable('IDX%USER',    .false.,error)
  !
end subroutine class_variable_index_delvar
!
subroutine class_variable_index_fill(set,presec,readsec,error,user_function)
  use gbl_message
  use gkernel_types
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_variable_index_fill
  use class_index
  use class_parameter
  use class_sicidx
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Fill the section arrays for the IDX%HEAD% Sic structure
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set                 !
  logical,             intent(in)    :: presec              ! Enable the 'present sections' array?
  logical,             intent(in)    :: readsec(-mx_sec:0)  ! Which section are to be added
  logical,             intent(inout) :: error               !
  logical,             external      :: user_function       !
  ! Local
  character(len=*), parameter :: rname='VARIABLE'
  type(observation) :: obs
  integer(kind=entry_length) :: ient,kx
  type(time_t) :: time
  !
  if (cx%next.le.1) then
    call class_message(seve%w,rname,'Current index is empty')
    return
  endif
  !
  ! (Re)allocations
  call class_variable_index_reallocate(presec,readsec,cx%next-1,error)
  if (error)  return
  !
  call init_obs(obs)
  !
  call gtime_init(time,cx%next-1,error)
  if (error)  return
  !
  ! (Re)fill
  do ient=1,cx%next-1
    call gtime_current(time)
    !
    ! Exit when asked by user
    call class_controlc(rname,error)
    if (error)  return
    !
    kx = cx%ind(ient)
    call rheader(set,obs,kx,user_function,error,readsec)
    if (error)  return
    !
    if (presec) then
      idxhead%presec(:,ient) = obs%head%presec(:)
    endif
    if (readsec(class_sec_gen_id)) then
      idxhead%gen%num(ient)     = obs%head%gen%num
      idxhead%gen%ver(ient)     = obs%head%gen%ver
      idxhead%gen%teles(ient)   = obs%head%gen%teles
      idxhead%gen%dobs(ient)    = obs%head%gen%dobs
      idxhead%gen%dred(ient)    = obs%head%gen%dred
      idxhead%gen%kind(ient)    = obs%head%gen%kind
      idxhead%gen%qual(ient)    = obs%head%gen%qual
      idxhead%gen%subscan(ient) = obs%head%gen%subscan
      idxhead%gen%scan(ient)    = obs%head%gen%scan
      idxhead%gen%ut(ient)      = obs%head%gen%ut
      idxhead%gen%st(ient)      = obs%head%gen%st
      idxhead%gen%az(ient)      = obs%head%gen%az
      idxhead%gen%el(ient)      = obs%head%gen%el
      idxhead%gen%tau(ient)     = obs%head%gen%tau
      idxhead%gen%tsys(ient)    = obs%head%gen%tsys
      idxhead%gen%time(ient)    = obs%head%gen%time
      idxhead%gen%parang(ient)  = obs%head%gen%parang
      idxhead%gen%xunit(ient)   = obs%head%gen%xunit
    endif
    if (readsec(class_sec_pos_id)) then
      idxhead%pos%sourc(ient)   = obs%head%pos%sourc
      idxhead%pos%system(ient)  = obs%head%pos%system
      idxhead%pos%equinox(ient) = obs%head%pos%equinox
      idxhead%pos%proj(ient)    = obs%head%pos%proj
      idxhead%pos%lam(ient)     = obs%head%pos%lam
      idxhead%pos%bet(ient)     = obs%head%pos%bet
      idxhead%pos%projang(ient) = obs%head%pos%projang
      idxhead%pos%lamof(ient)   = obs%head%pos%lamof
      idxhead%pos%betof(ient)   = obs%head%pos%betof
    endif
    if (readsec(class_sec_spe_id)) then
      idxhead%spe%line(ient)    = obs%head%spe%line
      idxhead%spe%restf(ient)   = obs%head%spe%restf
      idxhead%spe%nchan(ient)   = obs%head%spe%nchan
      idxhead%spe%rchan(ient)   = obs%head%spe%rchan
      idxhead%spe%fres(ient)    = obs%head%spe%fres
      idxhead%spe%vres(ient)    = obs%head%spe%vres
      idxhead%spe%voff(ient)    = obs%head%spe%voff
      idxhead%spe%bad(ient)     = obs%head%spe%bad
      idxhead%spe%image(ient)   = obs%head%spe%image
      idxhead%spe%vtype(ient)   = obs%head%spe%vtype
      idxhead%spe%vconv(ient)   = obs%head%spe%vconv
      idxhead%spe%vdire(ient)   = obs%head%spe%vdire
      idxhead%spe%doppler(ient) = obs%head%spe%doppler
    endif
    if (readsec(class_sec_res_id)) then
      idxhead%res%major(ient)  = obs%head%res%major
      idxhead%res%minor(ient)  = obs%head%res%minor
      idxhead%res%posang(ient) = obs%head%res%posang
    endif
    if (readsec(class_sec_bas_id)) then
      idxhead%bas%deg(ient)     = obs%head%bas%deg
      idxhead%bas%sigfi(ient)   = obs%head%bas%sigfi
      idxhead%bas%aire(ient)    = obs%head%bas%aire
      idxhead%bas%nwind(ient)   = obs%head%bas%nwind
      idxhead%bas%w1(:,ient)    = obs%head%bas%w1(:)
      idxhead%bas%w2(:,ient)    = obs%head%bas%w2(:)
      idxhead%bas%sinus(:,ient) = obs%head%bas%sinus(:)
    endif
    if (readsec(class_sec_his_id)) then
      idxhead%his%nseq(ient)    = obs%head%his%nseq
      idxhead%his%start(:,ient) = obs%head%his%start(:)
      idxhead%his%end(:,ient)   = obs%head%his%end(:)
    endif
    if (readsec(class_sec_plo_id)) then
      idxhead%plo%amin(ient) = obs%head%plo%amin
      idxhead%plo%amax(ient) = obs%head%plo%amax
      idxhead%plo%vmin(ient) = obs%head%plo%vmin
      idxhead%plo%vmax(ient) = obs%head%plo%vmax
    endif
    if (readsec(class_sec_swi_id)) then
      idxhead%swi%nphas(ient)    = obs%head%swi%nphas
      idxhead%swi%decal(:,ient)  = obs%head%swi%decal(:)
      idxhead%swi%duree(:,ient)  = obs%head%swi%duree(:)
      idxhead%swi%poids(:,ient)  = obs%head%swi%poids(:)
      idxhead%swi%swmod(ient)    = obs%head%swi%swmod
      idxhead%swi%ldecal(:,ient) = obs%head%swi%ldecal(:)
      idxhead%swi%bdecal(:,ient) = obs%head%swi%bdecal(:)
    endif
    if (readsec(class_sec_cal_id)) then
      idxhead%cal%beeff(ient)   = obs%head%cal%beeff
      idxhead%cal%foeff(ient)   = obs%head%cal%foeff
      idxhead%cal%gaini(ient)   = obs%head%cal%gaini
      idxhead%cal%h2omm(ient)   = obs%head%cal%h2omm
      idxhead%cal%pamb(ient)    = obs%head%cal%pamb
      idxhead%cal%tamb(ient)    = obs%head%cal%tamb
      idxhead%cal%tatms(ient)   = obs%head%cal%tatms
      idxhead%cal%tchop(ient)   = obs%head%cal%tchop
      idxhead%cal%tcold(ient)   = obs%head%cal%tcold
      idxhead%cal%taus(ient)    = obs%head%cal%taus
      idxhead%cal%taui(ient)    = obs%head%cal%taui
      idxhead%cal%tatmi(ient)   = obs%head%cal%tatmi
      idxhead%cal%trec(ient)    = obs%head%cal%trec
      idxhead%cal%cmode(ient)   = obs%head%cal%cmode
      idxhead%cal%atfac(ient)   = obs%head%cal%atfac
      idxhead%cal%alti(ient)    = obs%head%cal%alti
      idxhead%cal%count(:,ient) = obs%head%cal%count(:)
      idxhead%cal%lcalof(ient)  = obs%head%cal%lcalof
      idxhead%cal%bcalof(ient)  = obs%head%cal%bcalof
      idxhead%cal%geolong(ient) = obs%head%cal%geolong
      idxhead%cal%geolat(ient)  = obs%head%cal%geolat
    endif
    if (readsec(class_sec_sky_id)) then
      idxhead%sky%restf(ient)   = obs%head%sky%restf
      idxhead%sky%image(ient)   = obs%head%sky%image
      idxhead%sky%nsky(ient)    = obs%head%sky%nsky
      idxhead%sky%nchop(ient)   = obs%head%sky%nchop
      idxhead%sky%ncold(ient)   = obs%head%sky%ncold
      idxhead%sky%elev(:,ient)  = obs%head%sky%elev(:)
      idxhead%sky%emiss(:,ient) = obs%head%sky%emiss(:)
      idxhead%sky%chopp(:,ient) = obs%head%sky%chopp(:)
      idxhead%sky%cold(:,ient)  = obs%head%sky%cold(:)
    endif
    if (readsec(class_sec_gau_id)) then
      idxhead%gau%nline(ient)  = obs%head%gau%nline
      idxhead%gau%sigba(ient)  = obs%head%gau%sigba
      idxhead%gau%sigra(ient)  = obs%head%gau%sigra
      idxhead%gau%nfit(:,ient) = obs%head%gau%nfit(:)
      idxhead%gau%nerr(:,ient) = obs%head%gau%nerr(:)
    endif
    if (readsec(class_sec_she_id)) then
      idxhead%she%nline(ient)  = obs%head%she%nline
      idxhead%she%sigba(ient)  = obs%head%she%sigba
      idxhead%she%sigra(ient)  = obs%head%she%sigra
      idxhead%she%nfit(:,ient) = obs%head%she%nfit(:)
      idxhead%she%nerr(:,ient) = obs%head%she%nerr(:)
    endif
    if (readsec(class_sec_hfs_id)) then
      idxhead%hfs%nline(ient)  = obs%head%hfs%nline
      idxhead%hfs%sigba(ient)  = obs%head%hfs%sigba
      idxhead%hfs%sigra(ient)  = obs%head%hfs%sigra
      idxhead%hfs%nfit(:,ient) = obs%head%hfs%nfit(:)
      idxhead%hfs%nerr(:,ient) = obs%head%hfs%nerr(:)
    endif
    if (readsec(class_sec_abs_id)) then
      idxhead%abs%nline(ient)  = obs%head%abs%nline
      idxhead%abs%sigba(ient)  = obs%head%abs%sigba
      idxhead%abs%sigra(ient)  = obs%head%abs%sigra
      idxhead%abs%nfit(:,ient) = obs%head%abs%nfit(:)
      idxhead%abs%nerr(:,ient) = obs%head%abs%nerr(:)
    endif
    if (readsec(class_sec_dri_id)) then
      idxhead%dri%freq(ient)  = obs%head%dri%freq
      idxhead%dri%width(ient) = obs%head%dri%width
      idxhead%dri%npoin(ient) = obs%head%dri%npoin
      idxhead%dri%rpoin(ient) = obs%head%dri%rpoin
      idxhead%dri%tref(ient)  = obs%head%dri%tref
      idxhead%dri%aref(ient)  = obs%head%dri%aref
      idxhead%dri%apos(ient)  = obs%head%dri%apos
      idxhead%dri%tres(ient)  = obs%head%dri%tres
      idxhead%dri%ares(ient)  = obs%head%dri%ares
      idxhead%dri%bad(ient)   = obs%head%dri%bad
      idxhead%dri%ctype(ient) = obs%head%dri%ctype
      idxhead%dri%cimag(ient) = obs%head%dri%cimag
      idxhead%dri%colla(ient) = obs%head%dri%colla
      idxhead%dri%colle(ient) = obs%head%dri%colle
    endif
    if (readsec(class_sec_bea_id)) then
      idxhead%bea%cazim(ient) = obs%head%bea%cazim
      idxhead%bea%celev(ient) = obs%head%bea%celev
      idxhead%bea%space(ient) = obs%head%bea%space
      idxhead%bea%bpos(ient)  = obs%head%bea%bpos
      idxhead%bea%btype(ient) = obs%head%bea%btype
    endif
    if (readsec(class_sec_poi_id)) then
      idxhead%poi%nline(ient)  = obs%head%poi%nline
      idxhead%poi%sigba(ient)  = obs%head%poi%sigba
      idxhead%poi%sigra(ient)  = obs%head%poi%sigra
      idxhead%poi%nfit(:,ient) = obs%head%poi%nfit(:)
      idxhead%poi%nerr(:,ient) = obs%head%poi%nerr(:)
    endif
    if (readsec(class_sec_com_id)) then
      idxhead%com%ltext(ient) = obs%head%com%ltext
      idxhead%com%ctext(ient) = obs%head%com%ctext
    endif
    if (readsec(class_sec_her_id)) then
      idxhead%her%obsid(ient)      = obs%head%her%obsid
      idxhead%her%instrument(ient) = obs%head%her%instrument
      idxhead%her%proposal(ient)   = obs%head%her%proposal
      idxhead%her%aor(ient)        = obs%head%her%aor
      idxhead%her%operday(ient)    = obs%head%her%operday
      idxhead%her%dateobs(ient)    = obs%head%her%dateobs
      idxhead%her%dateend(ient)    = obs%head%her%dateend
      idxhead%her%obsmode(ient)    = obs%head%her%obsmode
      idxhead%her%vinfo(ient)      = obs%head%her%vinfo
      idxhead%her%zinfo(ient)      = obs%head%her%zinfo
      idxhead%her%posangle(ient)   = obs%head%her%posangle
      idxhead%her%reflam(ient)     = obs%head%her%reflam
      idxhead%her%refbet(ient)     = obs%head%her%refbet
      idxhead%her%hifavelam(ient)  = obs%head%her%hifavelam
      idxhead%her%hifavebet(ient)  = obs%head%her%hifavebet
      idxhead%her%etamb(ient)      = obs%head%her%etamb
      idxhead%her%etal(ient)       = obs%head%her%etal
      idxhead%her%etaa(ient)       = obs%head%her%etaa
      idxhead%her%hpbw(ient)       = obs%head%her%hpbw
      idxhead%her%tempscal(ient)   = obs%head%her%tempscal
      idxhead%her%lodopave(ient)   = obs%head%her%lodopave
      idxhead%her%gim0(ient)       = obs%head%her%gim0
      idxhead%her%gim1(ient)       = obs%head%her%gim1
      idxhead%her%gim2(ient)       = obs%head%her%gim2
      idxhead%her%gim3(ient)       = obs%head%her%gim3
      idxhead%her%mixercurh(ient)  = obs%head%her%mixercurh
      idxhead%her%mixercurv(ient)  = obs%head%her%mixercurv
      idxhead%her%datehcss(ient)   = obs%head%her%datehcss
      idxhead%her%hcssver(ient)    = obs%head%her%hcssver
      idxhead%her%calver(ient)     = obs%head%her%calver
      idxhead%her%level(ient)      = obs%head%her%level
    endif
    if (readsec(class_sec_user_id)) then
      call user_sec_varidx_fill(obs,ient,error)
      if (error)  exit
    endif
  enddo
  !
  call free_obs(obs)
  !
end subroutine class_variable_index_fill
!
subroutine class_variable_index_defvar(presec,readsec,error)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_variable_index_defvar
  use class_index
  use class_parameter
  use class_sicidx
  !---------------------------------------------------------------------
  ! @ private
  !  Create the sections in the IDX%HEAD% Sic structure
  !---------------------------------------------------------------------
  logical, intent(in)    :: presec              ! Enable the 'present sections' array?
  logical, intent(in)    :: readsec(-mx_sec:0)  ! Which section are to be added
  logical, intent(inout) :: error               !
  ! Local
  character(len=10) :: struct
  integer(kind=index_length) :: dims(4)
  !
  if (.not.sic_varexist('IDX')) then
    call sic_defstructure('IDX',.true.,error)
    if (error)  return
  endif
  !
  struct = 'IDX%HEAD'
  if (.not.sic_varexist(struct)) then
    call sic_defstructure(struct,.true.,error)
    if (error)  return
  endif
  !
  ! Present sections
  if (presec) then
    dims(1) = mx_sec+1
    dims(2) = cx%next-1
    call sic_def_login(trim(struct)//'%PRESEC',idxhead%presec,2,dims,.true.,error)
    if (error)  return
  endif
  !
  ! General
  if (readsec(class_sec_gen_id)) then
    call sic_defstructure(trim(struct)//'%GEN',.true.,error)
    if (error)  return
    dims(1) = cx%next-1
    call sic_def_long (trim(struct)//'%GEN%NUM',    idxhead%gen%num,    1,dims,.true.,error)
    call sic_def_inte (trim(struct)//'%GEN%VER',    idxhead%gen%ver,    1,dims,.true.,error)
    call sic_def_charn(trim(struct)//'%GEN%TELES',  idxhead%gen%teles,  1,dims,.true.,error)
    call sic_def_inte (trim(struct)//'%GEN%DOBS',   idxhead%gen%dobs,   1,dims,.true.,error)
    call sic_def_inte (trim(struct)//'%GEN%DRED',   idxhead%gen%dred,   1,dims,.true.,error)
    call sic_def_inte (trim(struct)//'%GEN%KIND',   idxhead%gen%kind,   1,dims,.true.,error)
    call sic_def_inte (trim(struct)//'%GEN%QUAL',   idxhead%gen%qual,   1,dims,.true.,error)
    call sic_def_inte (trim(struct)//'%GEN%SUBSCAN',idxhead%gen%subscan,1,dims,.true.,error)
    call sic_def_long (trim(struct)//'%GEN%SCAN',   idxhead%gen%scan,   1,dims,.true.,error)
    call sic_def_dble (trim(struct)//'%GEN%UT',     idxhead%gen%ut,     1,dims,.true.,error)
    call sic_def_dble (trim(struct)//'%GEN%ST',     idxhead%gen%st,     1,dims,.true.,error)
    call sic_def_real (trim(struct)//'%GEN%AZ',     idxhead%gen%az,     1,dims,.true.,error)
    call sic_def_real (trim(struct)//'%GEN%EL',     idxhead%gen%el,     1,dims,.true.,error)
    call sic_def_real (trim(struct)//'%GEN%TAU',    idxhead%gen%tau,    1,dims,.true.,error)
    call sic_def_real (trim(struct)//'%GEN%TSYS',   idxhead%gen%tsys,   1,dims,.true.,error)
    call sic_def_real (trim(struct)//'%GEN%TIME',   idxhead%gen%time,   1,dims,.true.,error)
    call sic_def_dble (trim(struct)//'%GEN%PARANG', idxhead%gen%parang, 1,dims,.true.,error)
    call sic_def_inte (trim(struct)//'%GEN%XUNIT',  idxhead%gen%xunit,  1,dims,.true.,error)
    if (error)  return
  endif
  !
  ! Position
  if (readsec(class_sec_pos_id)) then
    call sic_defstructure(trim(struct)//'%POS',.true.,error)
    if (error)  return
    dims(1) = cx%next-1
    call sic_def_charn(trim(struct)//'%POS%SOURC',  idxhead%pos%sourc,  1,dims,.true.,error)
    call sic_def_inte (trim(struct)//'%POS%SYSTEM', idxhead%pos%system, 1,dims,.true.,error)
    call sic_def_real (trim(struct)//'%POS%EQUINOX',idxhead%pos%equinox,1,dims,.true.,error)
    call sic_def_inte (trim(struct)//'%POS%PROJ',   idxhead%pos%proj,   1,dims,.true.,error)
    call sic_def_dble (trim(struct)//'%POS%LAM',    idxhead%pos%lam,    1,dims,.true.,error)
    call sic_def_dble (trim(struct)//'%POS%BET',    idxhead%pos%bet,    1,dims,.true.,error)
    call sic_def_dble (trim(struct)//'%POS%PROJANG',idxhead%pos%projang,1,dims,.true.,error)
    call sic_def_real (trim(struct)//'%POS%LAMOF',  idxhead%pos%lamof,  1,dims,.true.,error)
    call sic_def_real (trim(struct)//'%POS%BETOF',  idxhead%pos%betof,  1,dims,.true.,error)
    if (error)  return
  endif
  !
  ! Spectro
  if (readsec(class_sec_spe_id)) then
    call sic_defstructure(trim(struct)//'%SPE',.true.,error)
    if (error)  return
    dims(1) = cx%next-1
    call sic_def_charn(trim(struct)//'%SPE%LINE',   idxhead%spe%line,   1,dims,.true.,error)
    call sic_def_dble (trim(struct)//'%SPE%RESTF',  idxhead%spe%restf,  1,dims,.true.,error)
    call sic_def_inte (trim(struct)//'%SPE%NCHAN',  idxhead%spe%nchan,  1,dims,.true.,error)
    call sic_def_dble (trim(struct)//'%SPE%RCHAN',  idxhead%spe%rchan,  1,dims,.true.,error)
    call sic_def_dble (trim(struct)//'%SPE%FRES',   idxhead%spe%fres,   1,dims,.true.,error)
    call sic_def_dble (trim(struct)//'%SPE%VRES',   idxhead%spe%vres,   1,dims,.true.,error)
    call sic_def_dble (trim(struct)//'%SPE%VOFF',   idxhead%spe%voff,   1,dims,.true.,error)
    call sic_def_real (trim(struct)//'%SPE%BAD',    idxhead%spe%bad,    1,dims,.true.,error)
    call sic_def_dble (trim(struct)//'%SPE%IMAGE',  idxhead%spe%image,  1,dims,.true.,error)
    call sic_def_inte (trim(struct)//'%SPE%VTYPE',  idxhead%spe%vtype,  1,dims,.true.,error)
    call sic_def_inte (trim(struct)//'%SPE%VCONV',  idxhead%spe%vconv,  1,dims,.true.,error)
    call sic_def_inte (trim(struct)//'%SPE%VDIRE',  idxhead%spe%vdire,  1,dims,.true.,error)
    call sic_def_dble (trim(struct)//'%SPE%DOPPLER',idxhead%spe%doppler,1,dims,.true.,error)
    if (error)  return
  endif
  !
  ! Beam
  if (readsec(class_sec_res_id)) then
    call sic_defstructure(trim(struct)//'%RES',.true.,error)
    if (error)  return
    dims(1) = cx%next-1
    call sic_def_real(trim(struct)//'%RES%MAJOR', idxhead%res%major, 1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%RES%MINOR', idxhead%res%minor, 1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%RES%POSANG',idxhead%res%posang,1,dims,.true.,error)
    if (error)  return
  endif
  !
  ! Base
  if (readsec(class_sec_bas_id)) then
    call sic_defstructure(trim(struct)//'%BAS',.true.,error)
    if (error)  return
    dims(1) = cx%next-1
    call sic_def_inte(trim(struct)//'%BAS%DEG',  idxhead%bas%deg,  1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%BAS%SIGFI',idxhead%bas%sigfi,1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%BAS%AIRE', idxhead%bas%aire, 1,dims,.true.,error)
    call sic_def_inte(trim(struct)//'%BAS%NWIND',idxhead%bas%nwind,1,dims,.true.,error)
    dims(1) = mwind
    dims(2) = cx%next-1
    call sic_def_real(trim(struct)//'%BAS%W1',   idxhead%bas%w1,   2,dims,.true.,error)
    call sic_def_real(trim(struct)//'%BAS%W2',   idxhead%bas%w2,   2,dims,.true.,error)
    dims(1) = 3
    dims(2) = cx%next-1
    call sic_def_real(trim(struct)//'%BAS%SINUS',idxhead%bas%sinus,2,dims,.true.,error)
    if (error)  return
  endif
  !
  ! History
  if (readsec(class_sec_his_id)) then
    call sic_defstructure(trim(struct)//'%HIS',.true.,error)
    if (error)  return
    dims(1) = cx%next-1
    call sic_def_inte(trim(struct)//'%HIS%NSEQ', idxhead%his%nseq, 1,dims,.true.,error)
    dims(1) = mseq
    dims(2) = cx%next-1
    call sic_def_inte(trim(struct)//'%HIS%START',idxhead%his%start,2,dims,.true.,error)
    call sic_def_inte(trim(struct)//'%HIS%END',  idxhead%his%end,  2,dims,.true.,error)
    if (error)  return
  endif
  !
  ! Plot
  if (readsec(class_sec_plo_id)) then
    call sic_defstructure(trim(struct)//'%PLO',.true.,error)
    if (error)  return
    dims(1) = cx%next-1
    call sic_def_real(trim(struct)//'%PLO%AMIN',idxhead%plo%amin,1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%PLO%AMAX',idxhead%plo%amax,1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%PLO%VMIN',idxhead%plo%vmin,1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%PLO%VMAX',idxhead%plo%vmax,1,dims,.true.,error)
    if (error)  return
  endif
  !
  ! Switching
  if (readsec(class_sec_swi_id)) then
    call sic_defstructure(trim(struct)//'%FSW',.true.,error)
    if (error)  return
    dims(1) = cx%next-1
    call sic_def_inte(trim(struct)//'%FSW%NPHAS',idxhead%swi%nphas,1,dims,.true.,error)
    call sic_def_inte(trim(struct)//'%FSW%SWMOD',idxhead%swi%swmod,1,dims,.true.,error)
    dims(1) = mxphas
    dims(2) = cx%next-1
    call sic_def_dble(trim(struct)//'%FSW%DECAL', idxhead%swi%decal, 2,dims,.true.,error)
    call sic_def_real(trim(struct)//'%FSW%DUREE', idxhead%swi%duree, 2,dims,.true.,error)
    call sic_def_real(trim(struct)//'%FSW%POIDS', idxhead%swi%poids, 2,dims,.true.,error)
    call sic_def_real(trim(struct)//'%FSW%LDECAL',idxhead%swi%ldecal,2,dims,.true.,error)
    call sic_def_real(trim(struct)//'%FSW%BDECAL',idxhead%swi%bdecal,2,dims,.true.,error)
    if (error)  return
  endif
  !
  ! Calibration
  if (readsec(class_sec_cal_id)) then
    call sic_defstructure(trim(struct)//'%CAL',.true.,error)
    if (error)  return
    dims(1) = cx%next-1
    call sic_def_real(trim(struct)//'%CAL%BEEFF',  idxhead%cal%beeff,  1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%CAL%FOEFF',  idxhead%cal%foeff,  1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%CAL%GAINI',  idxhead%cal%gaini,  1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%CAL%H2OMM',  idxhead%cal%h2omm,  1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%CAL%PAMB',   idxhead%cal%pamb,   1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%CAL%TAMB',   idxhead%cal%tamb,   1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%CAL%TATMS',  idxhead%cal%tatms,  1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%CAL%TCHOP',  idxhead%cal%tchop,  1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%CAL%TCOLD',  idxhead%cal%tcold,  1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%CAL%TAUS',   idxhead%cal%taus,   1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%CAL%TAUI',   idxhead%cal%taui,   1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%CAL%TATMI',  idxhead%cal%tatmi,  1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%CAL%TREC',   idxhead%cal%trec,   1,dims,.true.,error)
    call sic_def_inte(trim(struct)//'%CAL%CMODE',  idxhead%cal%cmode,  1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%CAL%ATFAC',  idxhead%cal%atfac,  1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%CAL%ALTI',   idxhead%cal%alti,   1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%CAL%LCALOF', idxhead%cal%lcalof, 1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%CAL%BCALOF', idxhead%cal%bcalof, 1,dims,.true.,error)
    call sic_def_dble(trim(struct)//'%CAL%GEOLONG',idxhead%cal%geolong,1,dims,.true.,error)
    call sic_def_dble(trim(struct)//'%CAL%GEOLAT', idxhead%cal%geolat, 1,dims,.true.,error)
    dims(1) = 3
    dims(2) = cx%next-1
    call sic_def_real(trim(struct)//'%CAL%COUNT',idxhead%cal%count,2,dims,.true.,error)
    if (error)  return
  endif
  !
  ! Skydip
  if (readsec(class_sec_sky_id)) then
    call sic_defstructure(trim(struct)//'%SKY',.true.,error)
    if (error)  return
    dims(1) = cx%next-1
    call sic_def_dble(trim(struct)//'%SKY%SREST',idxhead%sky%restf,1,dims,.true.,error)
    call sic_def_dble(trim(struct)//'%SKY%SIMAG',idxhead%sky%image,1,dims,.true.,error)
    call sic_def_inte(trim(struct)//'%SKY%NSKY', idxhead%sky%nsky, 1,dims,.true.,error)
    call sic_def_inte(trim(struct)//'%SKY%NCHOP',idxhead%sky%nchop,1,dims,.true.,error)
    call sic_def_inte(trim(struct)//'%SKY%NCOLD',idxhead%sky%ncold,1,dims,.true.,error)
    dims(1) = msky
    dims(2) = cx%next-1
    call sic_def_real(trim(struct)//'%SKY%ELEV', idxhead%sky%elev, 2,dims,.true.,error)
    call sic_def_real(trim(struct)//'%SKY%EMISS',idxhead%sky%emiss,2,dims,.true.,error)
    call sic_def_real(trim(struct)//'%SKY%CHOPP',idxhead%sky%chopp,2,dims,.true.,error)
    call sic_def_real(trim(struct)//'%SKY%COLD', idxhead%sky%cold, 2,dims,.true.,error)
    if (error)  return
  endif
  !
  ! Gauss
  if (readsec(class_sec_gau_id)) then
    call sic_defstructure(trim(struct)//'%GAU',.true.,error)
    if (error)  return
    dims(1) = cx%next-1
    call sic_def_inte(trim(struct)//'%GAU%NLINE',   idxhead%gau%nline,1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%GAU%RMS_BASE',idxhead%gau%sigba,1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%GAU%RMS_LINE',idxhead%gau%sigra,1,dims,.true.,error)
    dims(1) = mgausfit
    dims(2) = cx%next-1
    call sic_def_real(trim(struct)//'%GAU%RESULT',idxhead%gau%nfit,2,dims,.true.,error)
    call sic_def_real(trim(struct)//'%GAU%ERROR', idxhead%gau%nerr,2,dims,.true.,error)
    if (error)  return
  endif
  !
  ! Shell
  if (readsec(class_sec_she_id)) then
    call sic_defstructure(trim(struct)//'%SHE',.true.,error)
    if (error)  return
    dims(1) = cx%next-1
    call sic_def_inte(trim(struct)//'%SHE%NLINE',   idxhead%she%nline,1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%SHE%RMS_BASE',idxhead%she%sigba,1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%SHE%RMS_LINE',idxhead%she%sigra,1,dims,.true.,error)
    dims(1) = mshellfit
    dims(2) = cx%next-1
    call sic_def_real(trim(struct)//'%SHE%RESULT',idxhead%she%nfit,2,dims,.true.,error)
    call sic_def_real(trim(struct)//'%SHE%ERROR', idxhead%she%nerr,2,dims,.true.,error)
    if (error)  return
  endif
  !
  ! HFS
  if (readsec(class_sec_hfs_id)) then
    call sic_defstructure(trim(struct)//'%HFS',.true.,error)
    if (error)  return
    dims(1) = cx%next-1
    call sic_def_inte(trim(struct)//'%HFS%NLINE',   idxhead%hfs%nline,1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%HFS%RMS_BASE',idxhead%hfs%sigba,1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%HFS%RMS_LINE',idxhead%hfs%sigra,1,dims,.true.,error)
    dims(1) = mhfsfit
    dims(2) = cx%next-1
    call sic_def_real(trim(struct)//'%HFS%RESULT',idxhead%hfs%nfit,2,dims,.true.,error)
    call sic_def_real(trim(struct)//'%HFS%ERROR', idxhead%hfs%nerr,2,dims,.true.,error)
    if (error)  return
  endif
  !
  ! Absorption
  if (readsec(class_sec_abs_id)) then
    call sic_defstructure(trim(struct)//'%ABS',.true.,error)
    if (error)  return
    dims(1) = cx%next-1
    call sic_def_inte(trim(struct)//'%ABS%NLINE',   idxhead%abs%nline,1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%ABS%RMS_BASE',idxhead%abs%sigba,1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%ABS%RMS_LINE',idxhead%abs%sigra,1,dims,.true.,error)
    dims(1) = mabsfit
    dims(2) = cx%next-1
    call sic_def_real(trim(struct)//'%ABS%RESULT',idxhead%abs%nfit,2,dims,.true.,error)
    call sic_def_real(trim(struct)//'%ABS%ERROR', idxhead%abs%nerr,2,dims,.true.,error)
    if (error)  return
  endif
  !
  ! Drift
  if (readsec(class_sec_dri_id)) then
    call sic_defstructure(trim(struct)//'%DRI',.true.,error)
    if (error)  return
    dims(1) = cx%next-1
    call sic_def_dble(trim(struct)//'%DRI%FREQ', idxhead%dri%freq, 1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%DRI%WIDTH',idxhead%dri%width,1,dims,.true.,error)
    call sic_def_inte(trim(struct)//'%DRI%NPOIN',idxhead%dri%npoin,1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%DRI%RPOIN',idxhead%dri%rpoin,1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%DRI%TREF', idxhead%dri%tref, 1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%DRI%AREF', idxhead%dri%aref, 1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%DRI%APOS', idxhead%dri%apos, 1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%DRI%TRES', idxhead%dri%tres, 1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%DRI%ARES', idxhead%dri%ares, 1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%DRI%CBAD', idxhead%dri%bad,  1,dims,.true.,error)
    call sic_def_inte(trim(struct)//'%DRI%CTYPE',idxhead%dri%ctype,1,dims,.true.,error)
    call sic_def_dble(trim(struct)//'%DRI%CIMAG',idxhead%dri%cimag,1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%DRI%COLLA',idxhead%dri%colla,1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%DRI%COLLE',idxhead%dri%colle,1,dims,.true.,error)
    if (error)  return
  endif
  !
  ! Beam
  if (readsec(class_sec_bea_id)) then
    call sic_defstructure(trim(struct)//'%BEA',.true.,error)
    if (error)  return
    dims(1) = cx%next-1
    call sic_def_real(trim(struct)//'%BEA%CAZIM',idxhead%bea%cazim,1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%BEA%CELEV',idxhead%bea%celev,1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%BEA%SPACE',idxhead%bea%space,1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%BEA%BPOS', idxhead%bea%bpos, 1,dims,.true.,error)
    call sic_def_inte(trim(struct)//'%BEA%BTYPE',idxhead%bea%btype,1,dims,.true.,error)
    if (error)  return
  endif
  !
  ! Continuum
  if (readsec(class_sec_poi_id)) then
    call sic_defstructure(trim(struct)//'%POI',.true.,error)
    if (error)  return
    dims(1) = cx%next-1
    call sic_def_inte(trim(struct)//'%POI%LCONT',idxhead%poi%nline,1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%POI%BCONT',idxhead%poi%sigba,1,dims,.true.,error)
    call sic_def_real(trim(struct)//'%POI%RCONT',idxhead%poi%sigra,1,dims,.true.,error)
    dims(1) = mpoifit
    dims(2) = cx%next-1
    call sic_def_real(trim(struct)//'%POI%NCONT',idxhead%poi%nfit,2,dims,.true.,error)
    call sic_def_real(trim(struct)//'%POI%ECONT',idxhead%poi%nerr,2,dims,.true.,error)
    if (error)  return
  endif
  !
  ! Herschel
  if (readsec(class_sec_her_id)) then
    call sic_defstructure(trim(struct)//'%HER',.true.,error)
    if (error)  return
    dims(1) = cx%next-1
    call sic_def_long (trim(struct)//'%HER%OBSID',     idxhead%her%obsid,     1,dims,.true.,error)
    call sic_def_charn(trim(struct)//'%HER%INSTRUMENT',idxhead%her%instrument,1,dims,.true.,error)
    call sic_def_charn(trim(struct)//'%HER%PROPOSAL',  idxhead%her%proposal,  1,dims,.true.,error)
    call sic_def_charn(trim(struct)//'%HER%AOR',       idxhead%her%aor,       1,dims,.true.,error)
    call sic_def_inte (trim(struct)//'%HER%OPERDAY',   idxhead%her%operday,   1,dims,.true.,error)
    call sic_def_charn(trim(struct)//'%HER%DATEOBS',   idxhead%her%dateobs,   1,dims,.true.,error)
    call sic_def_charn(trim(struct)//'%HER%DATEEND',   idxhead%her%dateend,   1,dims,.true.,error)
    call sic_def_charn(trim(struct)//'%HER%OBSMODE',   idxhead%her%obsmode,   1,dims,.true.,error)
    call sic_def_real (trim(struct)//'%HER%VINFO',     idxhead%her%vinfo,     1,dims,.true.,error)
    call sic_def_real (trim(struct)//'%HER%ZINFO',     idxhead%her%zinfo,     1,dims,.true.,error)
    call sic_def_dble (trim(struct)//'%HER%POSANGLE',  idxhead%her%posangle,  1,dims,.true.,error)
    call sic_def_dble (trim(struct)//'%HER%REFLAM',    idxhead%her%reflam,    1,dims,.true.,error)
    call sic_def_dble (trim(struct)//'%HER%REFBET',    idxhead%her%refbet,    1,dims,.true.,error)
    call sic_def_dble (trim(struct)//'%HER%HIFAVELAM', idxhead%her%hifavelam, 1,dims,.true.,error)
    call sic_def_dble (trim(struct)//'%HER%HIFAVEBET', idxhead%her%hifavebet, 1,dims,.true.,error)
    call sic_def_real (trim(struct)//'%HER%ETAMB',     idxhead%her%etamb,     1,dims,.true.,error)
    call sic_def_real (trim(struct)//'%HER%ETAL',      idxhead%her%etal,      1,dims,.true.,error)
    call sic_def_real (trim(struct)//'%HER%ETAA',      idxhead%her%etaa,      1,dims,.true.,error)
    call sic_def_real (trim(struct)//'%HER%HPBW',      idxhead%her%hpbw,      1,dims,.true.,error)
    call sic_def_charn(trim(struct)//'%HER%TEMPSCAL',  idxhead%her%tempscal,  1,dims,.true.,error)
    call sic_def_dble (trim(struct)//'%HER%LODOPAVE',  idxhead%her%lodopave,  1,dims,.true.,error)
    call sic_def_real (trim(struct)//'%HER%GIM0',      idxhead%her%gim0,      1,dims,.true.,error)
    call sic_def_real (trim(struct)//'%HER%GIM1',      idxhead%her%gim1,      1,dims,.true.,error)
    call sic_def_real (trim(struct)//'%HER%GIM2',      idxhead%her%gim2,      1,dims,.true.,error)
    call sic_def_real (trim(struct)//'%HER%GIM3',      idxhead%her%gim3,      1,dims,.true.,error)
    call sic_def_real (trim(struct)//'%HER%MIXERCURH', idxhead%her%mixercurh, 1,dims,.true.,error)
    call sic_def_real (trim(struct)//'%HER%MIXERCURV', idxhead%her%mixercurv, 1,dims,.true.,error)
    call sic_def_charn(trim(struct)//'%HER%DATEHCSS',  idxhead%her%datehcss,  1,dims,.true.,error)
    call sic_def_charn(trim(struct)//'%HER%HCSSVER',   idxhead%her%hcssver,   1,dims,.true.,error)
    call sic_def_charn(trim(struct)//'%HER%CALVER',    idxhead%her%calver,    1,dims,.true.,error)
    call sic_def_real (trim(struct)//'%HER%LEVEL',     idxhead%her%level,     1,dims,.true.,error)
    if (error)  return
  endif
  !
  ! Comment
  if (readsec(class_sec_com_id)) then
    call sic_defstructure(trim(struct)//'%COM',.true.,error)
    if (error)  return
    dims(1) = cx%next-1
    call sic_def_inte (trim(struct)//'%COM%LTEXT',idxhead%com%ltext,1,dims,.true.,error)
    call sic_def_charn(trim(struct)//'%COM%CTEXT',idxhead%com%ctext,1,dims,.true.,error)
    if (error)  return
  endif
  !
  ! User section
  if (readsec(class_sec_user_id)) then
    call user_sec_varidx_defvar(error)
    if (error)  return
  endif
  !
end subroutine class_variable_index_defvar
!
subroutine class_variable_index_reallocate(presec,readsec,nent,error)
  use classcore_dependencies_interfaces
  use class_parameter
  use class_sicidx
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Reallocate (if needed) the IDX support arrays, section by section.
  !  nent <= 0 means deallocate only.
  !---------------------------------------------------------------------
  logical,         intent(in)    :: presec              ! Enable the 'present sections' array?
  logical,         intent(in)    :: readsec(-mx_sec:0)  ! Which section are to be reallocated
  integer(kind=8), intent(in)    :: nent                ! Size of reallocation
  logical,         intent(inout) :: error               ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='REALLOCATE'
  logical :: realloc
  integer(kind=4) :: ier
  !
  ! Present sections
  if (presec) then
    if (allocated(idxhead%presec)) then
      realloc = ubound(idxhead%presec,2).lt.nent
      if (nent.le.0 .or. realloc) then
        deallocate(idxhead%presec)
      endif
    else
      realloc = nent.gt.0
    endif
    !
    if (realloc) then
      allocate(idxhead%presec(-mx_sec:0,nent),stat=ier)
      if (failed_allocate(rname,'IDX%PRESEC arrays',ier,error))  return
    endif
  endif
  !
  ! General
  if (readsec(class_sec_gen_id)) then
    if (allocated(idxhead%gen%num)) then
      realloc = size(idxhead%gen%num).lt.nent
      if (nent.le.0 .or. realloc) then
        deallocate(idxhead%gen%num, idxhead%gen%ver,    idxhead%gen%teles)
        deallocate(idxhead%gen%dobs,idxhead%gen%dred,   idxhead%gen%kind )
        deallocate(idxhead%gen%qual,idxhead%gen%subscan,idxhead%gen%scan )
        deallocate(idxhead%gen%ut,  idxhead%gen%st,     idxhead%gen%az   )
        deallocate(idxhead%gen%el,  idxhead%gen%tau,    idxhead%gen%tsys )
        deallocate(idxhead%gen%time,idxhead%gen%parang, idxhead%gen%xunit)
      endif
    else
      realloc = nent.gt.0
    endif
    !
    if (realloc) then
      allocate(idxhead%gen%num(nent),   idxhead%gen%ver(nent),    stat=ier)
      allocate(idxhead%gen%teles(nent), idxhead%gen%dobs(nent),   stat=ier)
      allocate(idxhead%gen%dred(nent),  idxhead%gen%kind(nent),   stat=ier)
      allocate(idxhead%gen%qual(nent),  idxhead%gen%subscan(nent),stat=ier)
      allocate(idxhead%gen%scan(nent),  idxhead%gen%ut(nent),     stat=ier)
      allocate(idxhead%gen%st(nent),    idxhead%gen%az(nent),     stat=ier)
      allocate(idxhead%gen%el(nent),    idxhead%gen%tau(nent),    stat=ier)
      allocate(idxhead%gen%tsys(nent),  idxhead%gen%time(nent),   stat=ier)
      allocate(idxhead%gen%parang(nent),idxhead%gen%xunit(nent),  stat=ier)
      if (failed_allocate(rname,'IDX%HEAD%GEN arrays',ier,error))  return
    endif
  endif
  !
  ! Position
  if (readsec(class_sec_pos_id)) then
    if (allocated(idxhead%pos%sourc)) then
      realloc = size(idxhead%pos%sourc).lt.nent
      if (nent.le.0 .or. realloc) then
        deallocate(idxhead%pos%sourc,  idxhead%pos%system,idxhead%pos%equinox)
        deallocate(idxhead%pos%proj,   idxhead%pos%lam,   idxhead%pos%bet    )
        deallocate(idxhead%pos%projang,idxhead%pos%lamof, idxhead%pos%betof  )
      endif
    else
      realloc = nent.gt.0
    endif
    !
    if (realloc .and. nent.gt.0) then
      allocate(idxhead%pos%sourc(nent),  idxhead%pos%system(nent),stat=ier)
      allocate(idxhead%pos%equinox(nent),idxhead%pos%proj(nent),  stat=ier)
      allocate(idxhead%pos%lam(nent),    idxhead%pos%bet(nent),   stat=ier)
      allocate(idxhead%pos%projang(nent),idxhead%pos%lamof(nent), stat=ier)
      allocate(idxhead%pos%betof(nent),                           stat=ier)
      if (failed_allocate(rname,'IDX%HEAD%POS arrays',ier,error))  return
    endif
  endif
  !
  ! Spectro
  if (readsec(class_sec_spe_id)) then
    if (allocated(idxhead%spe%line)) then
      realloc = size(idxhead%spe%line).lt.nent
      if (nent.le.0 .or. realloc) then
        deallocate(idxhead%spe%line, idxhead%spe%restf,idxhead%spe%nchan)
        deallocate(idxhead%spe%rchan,idxhead%spe%fres, idxhead%spe%vres)
        deallocate(idxhead%spe%voff, idxhead%spe%bad,  idxhead%spe%image)
        deallocate(idxhead%spe%vtype,idxhead%spe%vconv,idxhead%spe%vdire)
        deallocate(idxhead%spe%doppler)
      endif
    else
      realloc = nent.gt.0
    endif
    !
    if (realloc .and. nent.gt.0) then
      allocate(idxhead%spe%line(nent), idxhead%spe%restf(nent),stat=ier)
      allocate(idxhead%spe%nchan(nent),idxhead%spe%rchan(nent),stat=ier)
      allocate(idxhead%spe%fres(nent), idxhead%spe%vres(nent), stat=ier)
      allocate(idxhead%spe%voff(nent), idxhead%spe%bad(nent),  stat=ier)
      allocate(idxhead%spe%image(nent),idxhead%spe%vtype(nent),stat=ier)
      allocate(idxhead%spe%vconv(nent),idxhead%spe%vdire(nent),stat=ier)
      allocate(idxhead%spe%doppler(nent),stat=ier)
      if (failed_allocate(rname,'IDX%HEAD%SPE arrays',ier,error))  return
    endif
  endif
  !
  ! Resolution
  if (readsec(class_sec_res_id)) then
    if (allocated(idxhead%res%major)) then
      realloc = size(idxhead%res%major).lt.nent
      if (nent.le.0 .or. realloc) then
        deallocate(idxhead%res%major,idxhead%res%minor,idxhead%res%posang)
      endif
    else
      realloc = nent.gt.0
    endif
    !
    if (realloc .and. nent.gt.0) then
      allocate(idxhead%res%major(nent),idxhead%res%minor(nent),stat=ier)
      allocate(idxhead%res%posang(nent),stat=ier)
      if (failed_allocate(rname,'IDX%HEAD%RES arrays',ier,error))  return
    endif
  endif
  !
  ! Base
  if (readsec(class_sec_bas_id)) then
    if (allocated(idxhead%bas%deg)) then
      realloc = size(idxhead%bas%deg).lt.nent
      if (nent.le.0 .or. realloc) then
        deallocate(idxhead%bas%deg,  idxhead%bas%sigfi,idxhead%bas%aire)
        deallocate(idxhead%bas%nwind,idxhead%bas%w1,   idxhead%bas%w2  )
        deallocate(idxhead%bas%sinus)
      endif
    else
      realloc = nent.gt.0
    endif
    !
    if (realloc .and. nent.gt.0) then
      allocate(idxhead%bas%deg(nent),     idxhead%bas%sigfi(nent),   stat=ier)
      allocate(idxhead%bas%aire(nent),    idxhead%bas%nwind(nent),   stat=ier)
      allocate(idxhead%bas%w1(mwind,nent),idxhead%bas%w2(mwind,nent),stat=ier)
      allocate(idxhead%bas%sinus(3,nent), stat=ier)
      if (failed_allocate(rname,'IDX%HEAD%BAS arrays',ier,error))  return
    endif
  endif
  !
  ! History
  if (readsec(class_sec_his_id)) then
    if (allocated(idxhead%his%nseq)) then
      realloc = size(idxhead%his%nseq).lt.nent
      if (nent.le.0 .or. realloc) then
        deallocate(idxhead%his%nseq,idxhead%his%start,idxhead%his%end)
      endif
    else
      realloc = nent.gt.0
    endif
    !
    if (realloc .and. nent.gt.0) then
      allocate(idxhead%his%nseq(nent),    idxhead%his%start(mseq,nent),stat=ier)
      allocate(idxhead%his%end(mseq,nent),stat=ier)
      if (failed_allocate(rname,'IDX%HEAD%HIS arrays',ier,error))  return
    endif
  endif
  !
  ! Plot
  if (readsec(class_sec_plo_id)) then
    if (allocated(idxhead%plo%amin)) then
      realloc = size(idxhead%plo%amin).lt.nent
      if (nent.le.0 .or. realloc) then
        deallocate(idxhead%plo%amin,idxhead%plo%amax,idxhead%plo%vmin)
        deallocate(idxhead%plo%vmax)
      endif
    else
      realloc = nent.gt.0
    endif
    !
    if (realloc .and. nent.gt.0) then
      allocate(idxhead%plo%amin(nent),idxhead%plo%amax(nent),stat=ier)
      allocate(idxhead%plo%vmin(nent),idxhead%plo%vmax(nent),stat=ier)
      if (failed_allocate(rname,'IDX%HEAD%PLO arrays',ier,error))  return
    endif
  endif
  !
  ! Switching
  if (readsec(class_sec_swi_id)) then
    if (allocated(idxhead%swi%nphas)) then
      realloc = size(idxhead%swi%nphas).lt.nent
      if (nent.le.0 .or. realloc) then
        deallocate(idxhead%swi%nphas,idxhead%swi%decal,idxhead%swi%duree)
        deallocate(idxhead%swi%poids,idxhead%swi%swmod,idxhead%swi%ldecal)
        deallocate(idxhead%swi%bdecal)
      endif
    else
      realloc = nent.gt.0
    endif
    !
    if (realloc .and. nent.gt.0) then
      allocate(idxhead%swi%nphas(nent),       idxhead%swi%decal(mxphas,nent), stat=ier)
      allocate(idxhead%swi%duree(mxphas,nent),idxhead%swi%poids(mxphas,nent), stat=ier)
      allocate(idxhead%swi%swmod(nent),       idxhead%swi%ldecal(mxphas,nent),stat=ier)
      allocate(idxhead%swi%bdecal(mxphas,nent),                               stat=ier)
      if (failed_allocate(rname,'IDX%HEAD%FSW arrays',ier,error))  return
    endif
  endif
  !
  ! Calibration
  if (readsec(class_sec_cal_id)) then
    if (allocated(idxhead%cal%beeff)) then
      realloc = size(idxhead%cal%beeff).lt.nent
      if (nent.le.0 .or. realloc) then
        deallocate(idxhead%cal%beeff, idxhead%cal%foeff,  idxhead%cal%gaini )
        deallocate(idxhead%cal%h2omm, idxhead%cal%pamb,   idxhead%cal%tamb  )
        deallocate(idxhead%cal%tatms, idxhead%cal%tchop,  idxhead%cal%tcold )
        deallocate(idxhead%cal%taus,  idxhead%cal%taui,   idxhead%cal%tatmi )
        deallocate(idxhead%cal%trec,  idxhead%cal%cmode,  idxhead%cal%atfac )
        deallocate(idxhead%cal%alti,  idxhead%cal%count,  idxhead%cal%lcalof)
        deallocate(idxhead%cal%bcalof,idxhead%cal%geolong,idxhead%cal%geolat)
      endif
    else
      realloc = nent.gt.0
    endif
    !
    if (realloc .and. nent.gt.0) then
      allocate(idxhead%cal%beeff(nent),  idxhead%cal%foeff(nent), stat=ier)
      allocate(idxhead%cal%gaini(nent),  idxhead%cal%h2omm(nent), stat=ier)
      allocate(idxhead%cal%pamb(nent),   idxhead%cal%tamb(nent),  stat=ier)
      allocate(idxhead%cal%tatms(nent),  idxhead%cal%tchop(nent), stat=ier)
      allocate(idxhead%cal%tcold(nent),  idxhead%cal%taus(nent),  stat=ier)
      allocate(idxhead%cal%taui(nent),   idxhead%cal%tatmi(nent), stat=ier)
      allocate(idxhead%cal%trec(nent),   idxhead%cal%cmode(nent), stat=ier)
      allocate(idxhead%cal%atfac(nent),  idxhead%cal%alti(nent),  stat=ier)
      allocate(idxhead%cal%count(3,nent),                         stat=ier)
      allocate(idxhead%cal%lcalof(nent), idxhead%cal%bcalof(nent),stat=ier)
      allocate(idxhead%cal%geolong(nent),idxhead%cal%geolat(nent),stat=ier)
      if (failed_allocate(rname,'IDX%HEAD%CAL arrays',ier,error))  return
    endif
  endif
  !
  ! Skydip
  if (readsec(class_sec_sky_id)) then
    if (allocated(idxhead%sky%restf)) then
      realloc = size(idxhead%sky%restf).lt.nent
      if (nent.le.0 .or. realloc) then
        deallocate(idxhead%sky%restf,idxhead%sky%image,idxhead%sky%nsky)
        deallocate(idxhead%sky%nchop,idxhead%sky%ncold,idxhead%sky%elev)
        deallocate(idxhead%sky%emiss,idxhead%sky%chopp,idxhead%sky%cold)
      endif
    else
      realloc = nent.gt.0
    endif
    !
    if (realloc .and. nent.gt.0) then
      allocate(idxhead%sky%restf(nent),     idxhead%sky%image(nent),     stat=ier)
      allocate(idxhead%sky%nsky(nent),      idxhead%sky%nchop(nent),     stat=ier)
      allocate(idxhead%sky%ncold(nent),     idxhead%sky%elev(msky,nent), stat=ier)
      allocate(idxhead%sky%emiss(msky,nent),idxhead%sky%chopp(msky,nent),stat=ier)
      allocate(idxhead%sky%cold(msky,nent),                              stat=ier)
      if (failed_allocate(rname,'IDX%HEAD%SKY arrays',ier,error))  return
    endif
  endif
  !
  ! Gauss
  if (readsec(class_sec_gau_id)) then
    if (allocated(idxhead%gau%nline)) then
      realloc = size(idxhead%gau%nline).lt.nent
      if (nent.le.0 .or. realloc) then
        deallocate(idxhead%gau%nline,idxhead%gau%sigba,idxhead%gau%sigra)
        deallocate(idxhead%gau%nfit, idxhead%gau%nerr)
      endif
    else
      realloc = nent.gt.0
    endif
    !
    if (realloc .and. nent.gt.0) then
      allocate(idxhead%gau%nline(nent),                                        stat=ier)
      allocate(idxhead%gau%sigba(nent),        idxhead%gau%sigra(nent),        stat=ier)
      allocate(idxhead%gau%nfit(mgausfit,nent),idxhead%gau%nerr(mgausfit,nent),stat=ier)
      if (failed_allocate(rname,'IDX%HEAD%GAU arrays',ier,error))  return
    endif
  endif
  !
  ! Shell
  if (readsec(class_sec_she_id)) then
    if (allocated(idxhead%she%nline)) then
      realloc = size(idxhead%she%nline).lt.nent
      if (nent.le.0 .or. realloc) then
        deallocate(idxhead%she%nline,idxhead%she%sigba,idxhead%she%sigra)
        deallocate(idxhead%she%nfit, idxhead%she%nerr)
      endif
    else
      realloc = nent.gt.0
    endif
    !
    if (realloc .and. nent.gt.0) then
      allocate(idxhead%she%nline(nent),                                          stat=ier)
      allocate(idxhead%she%sigba(nent),         idxhead%she%sigra(nent),         stat=ier)
      allocate(idxhead%she%nfit(mshellfit,nent),idxhead%she%nerr(mshellfit,nent),stat=ier)
      if (failed_allocate(rname,'IDX%HEAD%SHE arrays',ier,error))  return
    endif
  endif
  !
  ! HFS
  if (readsec(class_sec_hfs_id)) then
    if (allocated(idxhead%hfs%nline)) then
      realloc = size(idxhead%hfs%nline).lt.nent
      if (nent.le.0 .or. realloc) then
        deallocate(idxhead%hfs%nline,idxhead%hfs%sigba,idxhead%hfs%sigra)
        deallocate(idxhead%hfs%nfit, idxhead%hfs%nerr)
      endif
    else
      realloc = nent.gt.0
    endif
    !
    if (realloc .and. nent.gt.0) then
      allocate(idxhead%hfs%nline(nent),                                      stat=ier)
      allocate(idxhead%hfs%sigba(nent),       idxhead%hfs%sigra(nent),       stat=ier)
      allocate(idxhead%hfs%nfit(mhfsfit,nent),idxhead%hfs%nerr(mhfsfit,nent),stat=ier)
      if (failed_allocate(rname,'IDX%HEAD%HFS arrays',ier,error))  return
    endif
  endif
  !
  ! Absorption
  if (readsec(class_sec_abs_id)) then
    if (allocated(idxhead%abs%nline)) then
      realloc = size(idxhead%abs%nline).lt.nent
      if (nent.le.0 .or. realloc) then
        deallocate(idxhead%abs%nline,idxhead%abs%sigba,idxhead%abs%sigra)
        deallocate(idxhead%abs%nfit, idxhead%abs%nerr)
      endif
    else
      realloc = nent.gt.0
    endif
    !
    if (realloc .and. nent.gt.0) then
      allocate(idxhead%abs%nline(nent),                                      stat=ier)
      allocate(idxhead%abs%sigba(nent),       idxhead%abs%sigra(nent),       stat=ier)
      allocate(idxhead%abs%nfit(mabsfit,nent),idxhead%abs%nerr(mabsfit,nent),stat=ier)
      if (failed_allocate(rname,'IDX%HEAD%ABS arrays',ier,error))  return
    endif
  endif
  !
  ! Drift
  if (readsec(class_sec_dri_id)) then
    if (allocated(idxhead%dri%freq)) then
      realloc = size(idxhead%dri%freq).lt.nent
      if (nent.le.0 .or. realloc) then
        deallocate(idxhead%dri%freq, idxhead%dri%width,idxhead%dri%npoin)
        deallocate(idxhead%dri%rpoin,idxhead%dri%tref, idxhead%dri%aref )
        deallocate(idxhead%dri%apos, idxhead%dri%tres, idxhead%dri%ares )
        deallocate(idxhead%dri%bad,  idxhead%dri%ctype,idxhead%dri%cimag)
        deallocate(idxhead%dri%colla,idxhead%dri%colle)
      endif
    else
      realloc = nent.gt.0
    endif
    !
    if (realloc .and. nent.gt.0) then
      allocate(idxhead%dri%freq(nent), idxhead%dri%width(nent),stat=ier)
      allocate(idxhead%dri%npoin(nent),idxhead%dri%rpoin(nent),stat=ier)
      allocate(idxhead%dri%tref(nent), idxhead%dri%aref(nent), stat=ier)
      allocate(idxhead%dri%apos(nent), idxhead%dri%tres(nent), stat=ier)
      allocate(idxhead%dri%ares(nent), idxhead%dri%bad(nent),  stat=ier)
      allocate(idxhead%dri%ctype(nent),idxhead%dri%cimag(nent),stat=ier)
      allocate(idxhead%dri%colla(nent),idxhead%dri%colle(nent),stat=ier)
      if (failed_allocate(rname,'IDX%HEAD%DRI arrays',ier,error))  return
    endif
  endif
  !
  ! Resolution
  if (readsec(class_sec_bea_id)) then
    if (allocated(idxhead%bea%cazim)) then
      realloc = size(idxhead%bea%cazim).lt.nent
      if (nent.le.0 .or. realloc) then
        deallocate(idxhead%bea%cazim,idxhead%bea%celev,idxhead%bea%space)
        deallocate(idxhead%bea%bpos, idxhead%bea%btype)
      endif
    else
      realloc = nent.gt.0
    endif
    !
    if (realloc .and. nent.gt.0) then
      allocate(idxhead%bea%cazim(nent),idxhead%bea%celev(nent),stat=ier)
      allocate(idxhead%bea%space(nent),idxhead%bea%bpos(nent), stat=ier)
      allocate(idxhead%bea%btype(nent),stat=ier)
      if (failed_allocate(rname,'IDX%HEAD%BEA arrays',ier,error))  return
    endif
  endif
  !
  ! Continuum
  if (readsec(class_sec_poi_id)) then
    if (allocated(idxhead%poi%nline)) then
      realloc = size(idxhead%poi%nline).lt.nent
      if (nent.le.0 .or. realloc) then
        deallocate(idxhead%poi%nline,idxhead%poi%sigba,idxhead%poi%sigra)
        deallocate(idxhead%poi%nfit, idxhead%poi%nerr)
      endif
    else
      realloc = nent.gt.0
    endif
    !
    if (realloc .and. nent.gt.0) then
      allocate(idxhead%poi%nline(nent),                                      stat=ier)
      allocate(idxhead%poi%sigba(nent),       idxhead%poi%sigra(nent),       stat=ier)
      allocate(idxhead%poi%nfit(mpoifit,nent),idxhead%poi%nerr(mpoifit,nent),stat=ier)
      if (failed_allocate(rname,'IDX%HEAD%POI arrays',ier,error))  return
    endif
  endif
  !
  ! Herschel
  if (readsec(class_sec_her_id)) then
    if (allocated(idxhead%her%obsid)) then
      realloc = size(idxhead%her%obsid).lt.nent
      if (nent.le.0 .or. realloc) then
        deallocate(idxhead%her%obsid,   idxhead%her%instrument,idxhead%her%proposal)
        deallocate(idxhead%her%aor,     idxhead%her%operday,   idxhead%her%dateobs)
        deallocate(idxhead%her%dateend, idxhead%her%obsmode,   idxhead%her%vinfo)
        deallocate(idxhead%her%zinfo,   idxhead%her%posangle,  idxhead%her%reflam)
        deallocate(idxhead%her%refbet,  idxhead%her%hifavelam, idxhead%her%hifavebet)
        deallocate(idxhead%her%etamb,   idxhead%her%etal,      idxhead%her%etaa)
        deallocate(idxhead%her%hpbw,    idxhead%her%tempscal,  idxhead%her%lodopave)
        deallocate(idxhead%her%gim0,    idxhead%her%gim1,      idxhead%her%gim2)
        deallocate(idxhead%her%gim3,    idxhead%her%mixercurh, idxhead%her%mixercurv)
        deallocate(idxhead%her%datehcss,idxhead%her%hcssver,   idxhead%her%calver)
        deallocate(idxhead%her%level)
      endif
    else
      realloc = nent.gt.0
    endif
    !
    if (realloc .and. nent.gt.0) then
      allocate(idxhead%her%obsid(nent),    idxhead%her%instrument(nent),stat=ier)
      allocate(idxhead%her%proposal(nent), idxhead%her%aor(nent),       stat=ier)
      allocate(idxhead%her%operday(nent),  idxhead%her%dateobs(nent),   stat=ier)
      allocate(idxhead%her%dateend(nent),  idxhead%her%obsmode(nent),   stat=ier)
      allocate(idxhead%her%vinfo(nent),    idxhead%her%zinfo(nent),     stat=ier)
      allocate(idxhead%her%posangle(nent), idxhead%her%reflam(nent),    stat=ier)
      allocate(idxhead%her%refbet(nent),   idxhead%her%hifavelam(nent), stat=ier)
      allocate(idxhead%her%hifavebet(nent),idxhead%her%etamb(nent),     stat=ier)
      allocate(idxhead%her%etal(nent),     idxhead%her%etaa(nent),      stat=ier)
      allocate(idxhead%her%hpbw(nent),     idxhead%her%tempscal(nent),  stat=ier)
      allocate(idxhead%her%lodopave(nent), idxhead%her%gim0(nent),      stat=ier)
      allocate(idxhead%her%gim1(nent),     idxhead%her%gim2(nent),      stat=ier)
      allocate(idxhead%her%gim3(nent),     idxhead%her%mixercurh(nent), stat=ier)
      allocate(idxhead%her%mixercurv(nent),idxhead%her%datehcss(nent),  stat=ier)
      allocate(idxhead%her%hcssver(nent),  idxhead%her%calver(nent),    stat=ier)
      allocate(idxhead%her%level(nent),                                 stat=ier)
      if (failed_allocate(rname,'IDX%HEAD%HER arrays',ier,error))  return
    endif
  endif
  !
  ! Comment
  if (readsec(class_sec_com_id)) then
    if (allocated(idxhead%com%ltext)) then
      realloc = size(idxhead%com%ltext).lt.nent
      if (nent.le.0 .or. realloc) then
        deallocate(idxhead%com%ltext,idxhead%com%ctext)
      endif
    else
      realloc = nent.gt.0
    endif
    !
    if (realloc .and. nent.gt.0) then
      allocate(idxhead%com%ltext(nent),idxhead%com%ctext(nent),stat=ier)
      if (failed_allocate(rname,'IDX%HEAD%COM arrays',ier,error))  return
    endif
  endif
  !
  ! User section
  if (readsec(class_sec_user_id)) then
    call user_sec_varidx_realloc(nent,error)
    if (error)  return
  endif
  !
end subroutine class_variable_index_reallocate
