subroutine extract(set,line,r,error,user_function)
  use gbl_message
  use gkernel_types
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>extract
  use class_common
  use class_index
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! CLASS Support routine for command
  !   EXTRACT [X1 X2 [Unit]] [/INDEX]
  ! Extract the subset of a spectrum in the given range
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  character(len=*),    intent(in)    :: line           ! Input command line
  type(observation),   intent(inout) :: r              !
  logical,             intent(inout) :: error          ! Error flag
  logical,             external      :: user_function  !
  ! Local
  character(len=*), parameter :: rname='EXTRACT'
  type(observation) :: obs
  type(extract_t) :: extr
  real(kind=4) :: xc1,xc2,xv,xf,xi,ya,xo,yo
  character(len=1) :: ch
  integer(kind=4) :: lunit
  integer(kind=entry_length) :: iobs
  character(len=message_length) :: mess
  type(time_t) :: time
  !
  extr%rname = rname
  !
  if (sic_present(0,1)) then
    ! Get range from command line
    call sic_r8(line,0,1,extr%xa,.true.,error)
    if (error)  return
    call sic_r8(line,0,2,extr%xb,.true.,error)
    if (error)  return
    extr%unit = set%unitx(1)
    call sic_ke(line,0,3,extr%unit,lunit,.false.,error)
    if (error)  return
    !
  else
    ! Get range from cursor
    call class_message(seve%i,rname,'Select range with cursor')
    call getcur(xc1,xv,xf,xi,ya,xo,yo,ch)
    call getcur(xc2,xv,xf,xi,ya,xo,yo,ch)
    extr%xa = xc1
    extr%xb = xc2
    extr%unit = 'C'
  endif
  !
  if (sic_present(1,0) .or. set%action.eq.'I') then
    ! INDEX mode: write to output file
    !
    ! Basic checks
    if (.not.fileout_opened(rname,error))  return
    if (cx%next.le.1) then
      call class_message(seve%e,rname,'Index is empty')
      error = .true.
      return
    endif
    !
    call init_obs(obs)
    call gtime_init(time,cx%next-1,error)
    if (error)  return
    !
    do iobs=1,cx%next-1
      call gtime_current(time)
      !
      call class_controlc(rname,error)
      if (error)  exit
      !
      call get_it_subset(set,obs,cx%ind(iobs),extr,user_function,error)
      if (error)  exit
      !
      ! Same as COPY:
      ! Output file is multiple: keep the observation number. If same
      !        number already exists, a new version will be written.
      ! Output file is single: set obs number to 0, class_write() will
      !        assign a new one automatically.
      if (fileout%desc%single)  obs%head%gen%num = 0
      !
      call class_write(set,obs,error,user_function)
      if (error)  exit
      !
    enddo
    !
    if (error) then
      write (mess,'(A,I0,A,I0,A)')  &
        'Incomplete output (stopped at ',iobs,  &
        '-th observation over ',cx%next-1,' in index)'
      call class_message(seve%e,rname,mess)
    endif
    !
    call free_obs(obs)
    !
    call classcore_fileout_flush(error)
    if (error)  return
    !
  else
    ! OBSERVATION mode: extract and update R
    !
    ! Basic checks
    if (r%head%xnum.eq.0) then
      call class_message(seve%e,rname,'No spectrum in memory.')
      error = .true.
      return
    elseif (r%head%presec(class_sec_xcoo_id)) then
      call class_message(seve%e,rname,'Irregularly spaced data not yet supported')
      error = .true.
      return
    endif
    !
    ! Decode the units
    call do_extract_units(r,extr,error)
    if (error) return
    if (extr%nc.le.10) then
      ! Warn if 10 or less channels for a single extraction
      write(mess,'(A,I0,A)')  'Only ',extr%nc,' channels extracted'
      call class_message(seve%w,extr%rname,mess)
    endif
    !
    call do_extract(r,extr,error)
    if (error) return
    !
    call newdat(set,r,error)
    call newdat_assoc(set,r,error)
    call newdat_user(set,r,error)
    !
  endif
  !
end subroutine extract
!
subroutine do_extract_units(obs,extr,error)
  use gbl_constant
  use gbl_message
  use classcore_interfaces, except_this=>do_extract_units
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Compute extraction channel range from range values
  ! Must have been set before:
  ! - extr%rname
  ! - extr%xa
  ! - extr%xb
  ! - extr%unit
  !---------------------------------------------------------------------
  type(observation), intent(in)    :: obs    ! Observation before extraction
  type(extract_t),   intent(inout) :: extr   ! Extraction descriptor
  logical,           intent(inout) :: error  ! Logical error flag
  ! Local
  real(kind=8) :: ca,cb
  integer(kind=4) :: ndata
  !
  if (obs%head%gen%kind.eq.kind_spec) then
    ! Spectroscopic data
    ndata = obs%head%spe%nchan
    select case (extr%unit)
    case ('C')
      ca = extr%xa
      cb = extr%xb
    case ('V')
      call abscissa_velo2chan(obs%head,extr%xa,ca)
      call abscissa_velo2chan(obs%head,extr%xb,cb)
    case ('F')
      call abscissa_sigabs2chan(obs%head,extr%xa,ca)
      call abscissa_sigabs2chan(obs%head,extr%xb,cb)
    case ('I')
      call abscissa_imaabs2chan(obs%head,extr%xa,ca)
      call abscissa_imaabs2chan(obs%head,extr%xb,cb)
    case default
      call class_message(seve%e,extr%rname,  &
        'Unit '''//trim(extr%unit)//''' not supported for spectroscopic data')
      error = .true.
      return
    end select
    !
  else
    ! Continuum data
    ndata = obs%head%dri%npoin
    select case (extr%unit)
    case ('C')
      ca = extr%xa
      cb = extr%xb
    case ('A')
      call abscissa_angl2chan(obs%head,extr%xa,ca)
      call abscissa_angl2chan(obs%head,extr%xb,cb)
    case ('T')
      call abscissa_time2chan(obs%head,extr%xa,ca)
      call abscissa_time2chan(obs%head,extr%xb,cb)
    case default
      call class_message(seve%e,extr%rname,  &
        'Unit '''//trim(extr%unit)//''' not supported for continuum data')
      error = .true.
      return
    end select
    !
  endif
  !
  ! Sort the values
  if (ca.lt.cb) then
    extr%c1 = floor(ca)
    extr%c2 = ceiling(cb)
  else
    extr%c1 = floor(cb)
    extr%c2 = ceiling(ca)
  endif
  extr%nc = extr%c2-extr%c1+1
  !
  if (extr%c2.le.0 .or. extr%c1.gt.ndata) then
    call class_message(seve%e,extr%rname,'Range is off the input observation limits')
    error = .true.
    return
    !
  elseif (extr%c1.le.0 .or. extr%c2.gt.ndata) then
    call class_message(seve%w,extr%rname,  &
      'Range overlaps the input observation limits, '//  &
      'output observation completed with blanks')
  endif
  !
end subroutine do_extract_units
!
subroutine do_extract(obs,extr,error)
  use gbl_constant
  use gkernel_interfaces
  use classcore_interfaces, except_this=>do_extract
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Extract a subset of an observation
  ! Must have been set before:
  ! - extr%rname
  ! - extr%c1
  ! - extr%c2
  ! - extr%nc
  !  For efficiency purpose, assume that extr%c1 and extr%c2 are already
  ! sorted.
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs    ! Observation before and after extraction
  type(extract_t),   intent(in)    :: extr   ! Extraction descriptor
  logical,           intent(inout) :: error  ! Logical error flag
  ! Local
  integer(kind=4) :: old_nchan,ier
  real(kind=4) :: bad
  real(kind=4), allocatable :: yvalue(:)
  !
  if (obs%head%gen%kind.eq.kind_spec) then
    bad = obs%head%spe%bad
    old_nchan = obs%head%spe%nchan
  else
    bad = obs%head%dri%bad
    old_nchan = obs%head%dri%npoin
  endif
  !
  ! Do the job
  allocate (yvalue(old_nchan),stat=ier)
  if (failed_allocate(extr%rname,'yvalue',ier,error))  return
  !
  ! Save data
  yvalue(:) = obs%spectre(1:old_nchan)
  !
  ! Enlarge data space when needed
  call reallocate_obs(obs,extr%nc,error)
  if (error)  goto 10
  !
  call do_extract_data(yvalue,old_nchan,obs%spectre,extr%nc,bad,extr,error)
  if (error)  goto 10
  !
  ! Update the header. Easy: just change Nchan and shift Rchan
  if (obs%head%gen%kind.eq.kind_spec) then
    obs%head%spe%nchan = extr%nc
    obs%head%spe%rchan = obs%head%spe%rchan-extr%c1+1.d0
  else
    obs%head%dri%npoin = extr%nc
    obs%head%dri%rpoin = obs%head%dri%rpoin-extr%c1+1.d0
  endif
  !
  ! Same action on Associated Arrays section
  call extract_assoc(obs%assoc,extr,error)
  if (error)  goto 10
  !
  ! Disable PLOT section. We can also choose to update the Vmin-Vmax values
  ! but we don't want to recompute the Ymin-Ymax in order to save time
  obs%head%presec(class_sec_plo_id) = .false.
  !
10 continue
  !
  ! Free memory
  deallocate(yvalue)
  !
end subroutine do_extract
!
subroutine do_extract_data_r4(in,inchan,out,onchan,bad,extr,error)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic do_extract_data
  !  Extract a subset of a data array according the extraction
  ! descriptor.
  ! ---
  !  R*4 version
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)    :: inchan       ! Input nchan
  real(kind=4),    intent(in)    :: in(inchan)   !
  integer(kind=4), intent(in)    :: onchan       ! Output nchan
  real(kind=4),    intent(out)   :: out(onchan)  !
  real(kind=4),    intent(in)    :: bad          ! Blank value
  type(extract_t), intent(in)    :: extr         ! Extraction descriptor
  logical,         intent(inout) :: error        ! Logical error flag
  ! Local
  integer(kind=4) :: iichan,oichan
  !
  ! Writing algorithm made efficient:
  ! 1) avoid writing beyond extr%nc
  ! 2) avoid initializing with blanks everywhere before overwriting with real data
  ! 3) scan only the needed part of the old spectrum
  if (extr%c1.le.0)  &
    out(1:-extr%c1+1) = bad
  if (extr%c2.gt.inchan)  &
    out(inchan-extr%c1+1:extr%nc) = bad
  !
  do iichan = max(1,extr%c1),min(inchan,extr%c2)
    oichan = iichan-extr%c1+1
    out(oichan) = in(iichan)
  enddo
  !
end subroutine do_extract_data_r4
!
subroutine do_extract_data_i4(in,inchan,out,onchan,bad,extr,error)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic do_extract_data
  !  Extract a subset of a data array according the extraction
  ! descriptor.
  ! ---
  !  I*4 version
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)    :: inchan       ! Input nchan
  integer(kind=4), intent(in)    :: in(inchan)   !
  integer(kind=4), intent(in)    :: onchan       ! Output nchan
  integer(kind=4), intent(out)   :: out(onchan)  !
  integer(kind=4), intent(in)    :: bad          ! Blank value
  type(extract_t), intent(in)    :: extr         ! Extraction descriptor
  logical,         intent(inout) :: error        ! Logical error flag
  ! Local
  integer(kind=4) :: iichan,oichan
  !
  ! Writing algorithm made efficient:
  ! 1) avoid writing beyond extr%nc
  ! 2) avoid initializing with blanks everywhere before overwriting with real data
  ! 3) scan only the needed part of the old spectrum
  if (extr%c1.le.0)  &
    out(1:-extr%c1+1) = bad
  if (extr%c2.gt.inchan)  &
    out(inchan-extr%c1+1:extr%nc) = bad
  !
  do iichan = max(1,extr%c1),min(inchan,extr%c2)
    oichan = iichan-extr%c1+1
    out(oichan) = in(iichan)
  enddo
  !
end subroutine do_extract_data_i4
