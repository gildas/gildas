subroutine put_item(item,ni,buffer,fmt,error)
  use gildas_def
  use gkernel_interfaces
  use gbl_format
  use gbl_message
  use class_fits
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! FITS        Internal routine.
  !     Transfers (and translate) a data vector to 3D row.
  !---------------------------------------------------------------------
  integer(kind=1) :: item(*)      ! CLASS information             Input
  integer :: ni                   ! Number of elements in vector  Input
  integer(kind=1) :: buffer(*)    ! Row buffer                    Output
  integer :: fmt                  ! Data format                   Input
  logical :: error                ! Logical error flag            Output
  ! Global
  integer(kind=4) :: gdf_conv
  ! Local
  integer :: i, status
  character(len=80) :: chain
  logical :: ll
  !
  ! Convert from local format to corresponding IEEE format.
  if (fmt.gt.0) then           ! Strings
    call bytoby(item,buffer,ni)
    !
  elseif (fmt.eq.fmt_i2) then
#if defined(EEEI)
    call bytoby(item,buffer,ni*2)
#endif
#if defined(VAX) || defined(IEEE)
    call eii2ie (item,buffer,ni)
#endif
    !
  elseif (fmt.eq.fmt_i4) then
#if defined(EEEI)
    call bytoby(item,buffer,ni*4)
#endif
#if defined(VAX) || defined(IEEE)
    call eii4ie (item,buffer,ni)
#endif
    !
  elseif (fmt.eq.fmt_i8) then
#if defined(EEEI)
    call bytoby(item,buffer,ni*8)
#endif
#if defined(VAX) || defined(IEEE)
    call eii8ie (item,buffer,ni)
#endif
    !
  elseif (fmt.eq.fmt_l) then
    do i=1,ni
      call bytoby(item(i),ll,1)
      if (ll) then
        buffer(i) = ichar('T')
      else
        buffer(i) = ichar('F')
      endif
    enddo
    !
  elseif (fmt.eq.fmt_r4) then
    call gdf_setblnk4(fits%head%rbad)
    if (fmt_r4.eq.eei_r4) then
      call r4tor4(item,buffer,ni)
    else
      status = gdf_conv(item,buffer,ni,eei_r4,fmt_r4)
      if (status.ne.1) goto 99
    endif
    !
  elseif (fmt.eq.fmt_r8) then
    call gdf_setblnk8(dble(fits%head%rbad))
    if (fmt_r8.eq.eei_r8) then
      call r8tor8(item,buffer,ni)
    else
      status = gdf_conv(item,buffer,2*ni,eei_r8,fmt_r8)
      if (status.ne.1) goto 99
    endif
    !
  else
    chain = 'Unsupported format for 3D binary table.'
    call class_message(seve%w,'3DFITS',chain)
    error = .true.
  endif
  !
  return
  99   error = .true.
end subroutine put_item
!
subroutine get_item(item,ni,fmtout,buffer,fmtin,error)
  use gildas_def
  use gkernel_interfaces
  use class_data
  use class_fits
  use gbl_message
  use gbl_format
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  ! FITS        Internal routine.
  !     Transfers (and translate) a data vector from 3D row.
  !---------------------------------------------------------------------
  integer(kind=1), intent(out)   :: item(*)    ! Requested information
  integer,         intent(in)    :: ni         ! Number of requested information
  integer,         intent(in)    :: fmtout     ! Format for requested information
  integer(kind=1), intent(in)    :: buffer(*)  ! Row buffer
  integer,         intent(in)    :: fmtin      ! Format of data in buffer
  logical,         intent(out)   :: error      ! Error status
  ! Global
  integer(kind=4) :: gdf_conv
  ! Local
  character(len=*), parameter :: proc='GET_ITEM'
  integer   :: i, fmt, status, nm
  character(len=80) :: chain
  logical   :: log, doall
  ! Pray it is 8-Byte aligned...
  integer(kind=1), allocatable :: bbuf(:) !
  !
  nm = max(fmtin, 8*ni)  ! Maximum Number of bytes
  if (allocated(bbuf)) then
     doall = nm.gt.ubound(bbuf,1)
     if (doall) deallocate(bbuf)
  else
     doall = .true.
  endif
  if (doall) then
     allocate (bbuf(nm),stat=status)
     if (status.ne.0) then
        call class_message(seve%e,proc,'BBUF allocation error')
        error = .true.
        return
     endif
  endif
  !
  ! First convert from IEEE format to corresponding local format. Force R8
  ! alignment in the process to avoid later problems on HPs, SUNs and ALPHAs
  if (fmtin.gt.0) then  ! Characters
     fmt = fmtin
     call bytoby(buffer,bbuf,fmtin)
  elseif (fmtin.eq.eei_i2) then
     fmt = fmt_i2
     if (fmt.eq.eei_i2) then
        call bytoby(buffer,bbuf,2*ni)
     else
        if (fmt.eq.iee_i2) then
           call eii2ie(buffer,bbuf,ni)
        elseif (fmt.eq.vax_i2) then
           ! Missing in shareable library,
           ! replaced with equivalent defined routine...
           !               CALL EII2VA(BUFFER,R8BUF,ni)
           call eii2ie(buffer,bbuf,ni)
        else
           goto 99
        endif
     endif
  elseif (fmtin.eq.eei_i4) then
     fmt = fmt_i4
     if (fmt.eq.eei_i4) then
        call bytoby(buffer,bbuf,4*ni)
     else
        if (fmt.eq.iee_i4) then
           call eii4ie(buffer,bbuf,ni)
        elseif (fmt.eq.vax_i4) then
           call eii4va(buffer,bbuf,ni)
        else
           goto 99
        endif
     endif
  elseif (fmtin.eq.eei_i8) then
     fmt = fmt_i8
     if (fmt.eq.eei_i8) then
        call bytoby(buffer,bbuf,8*ni)
     else
        if (fmt.eq.iee_i8) then
           call eii8ie(buffer,bbuf,ni)
        else
           ! NB: vax_i8 not supported
           goto 99
        endif
     endif
     !
  elseif (fmtin.eq.eei_l) then
     ! Probably not fully portable: assumes logicals fit in one byte
     do i=1,ni
        if (buffer(i).eq.ichar('T')) then
           log = .true.
           call bytoby(log,bbuf(i),1)
        elseif (buffer(i).eq.ichar('F')) then
           log = .false.
           call bytoby(log,bbuf(i),1)
        else
           call class_message(seve%e,proc,'Invalid value for logical. Must be T or F.')
           error = .true.
        endif
        fmt = fmt_l
     enddo
  elseif (fmtin.eq.eei_r4) then
     call gdf_setblnk4(fits%head%rbad)
     fmt = fmt_r4
     if (fmt.eq.eei_r4) then
        call bytoby(buffer,bbuf,4*ni)
     else
        status = gdf_conv(buffer,bbuf,ni,fmt_r4,eei_r4)
        if (status.ne.1) goto 99
     endif
  elseif (fmtin.eq.eei_r8) then
     call gdf_setblnk8(dble(fits%head%rbad))
     fmt= fmt_r8
     if (fmt.eq.eei_r8) then
        call bytoby(buffer,bbuf,8*ni)
     else
        status = gdf_conv(buffer,bbuf,2*ni,fmt_r8,eei_r8)
        if (status.ne.1) goto 99
     endif
  elseif (fmtin.le.0) then
     write(chain,*) 'Unsupported format in 3D binary table: ',fmtin
     call class_message(seve%e,proc,chain)
     error = .true.
  endif
  if (error) return
  !
  ! Then, try to force it into the requested output type...
  call fits_convert(bbuf,fmt,item,fmtout,ni,error)
  return
99 error = .true.
end subroutine get_item
!
subroutine fits_convert(inbuf,fmtin,outbuf,fmtout,ndat,error)
  use gildas_def
  use gbl_format
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! FITS        Internal routine.
  !     Conversion between compatible local data types.
  !---------------------------------------------------------------------
  integer(kind=1), intent(in)  :: inbuf(*)   ! Input buffer
  integer,         intent(in)  :: fmtin      ! Format of data in buffer
  integer(kind=1), intent(out) :: outbuf(*)  ! Output buffer
  integer,         intent(in)  :: fmtout     ! Format for requested information
  integer,         intent(in)  :: ndat       ! Size of buffer
  logical,         intent(out) :: error      ! Error status
  ! Local
  character(len=*), parameter :: proc='FITS_CONVERT'
  integer :: i
  character(len=80) :: chain
  !
  ! Check for grossly illegal conversions first...
  if (fmtout*fmtin.le.0) then
     if (fmtin.le.0) then
        call class_message(seve%e,proc,'Requesting translation from numeric to character data type')
        error = .true.
     elseif (fmtout.le.0) then
        call class_message(seve%e,proc,'Requesting translation from character to numeric data type')
        error = .true.
     endif
     !
  elseif (fmtout.gt.0) then
     ! Character string: copy the byte stream
     do i=1,ndat
        call bytoby(inbuf((i-1)*ndat+1),outbuf,min(fmtin,fmtout))
     enddo
     !
  elseif (fmtout.eq.fmt_l) then
     if (fmtin.eq.fmt_l) then
        call bytoby(inbuf,outbuf,ndat)
     else
        call class_message(seve%e,proc,'Numeric to logical conversion is illegal')
        error = .true.
     endif
     !
  elseif (fmtout.eq.fmt_i2) then
     ! Integer*2 (should not occur in CLASS, shout...)
     call class_message(seve%e,proc,'Requesting translation to INTEGER*2 format')
     error = .true.
     !
  elseif (fmtout.eq.fmt_i4) then
     if (fmtin.eq.fmt_i8) then
        call i8toi4(inbuf,outbuf,ndat)
     elseif (fmtin.eq.fmt_i4) then
        call i4toi4(inbuf,outbuf,ndat)
     elseif (fmtin.eq.fmt_i2) then
        call i2toi4(inbuf,outbuf,ndat)
     elseif (fmtin.eq.fmt_r4) then
        call r4toi4(inbuf,outbuf,ndat)
     elseif (fmtin.eq.fmt_r8) then
        call r8toi4(inbuf,outbuf,ndat)
     elseif (fmtin.eq.fmt_l) then
        call class_message(seve%e,proc,'Logical to integer conversion is illegal')
        error = .true.
     else
        call class_message(seve%e,proc,'Input format not supported')
        error = .true.
     endif
     !
  elseif (fmtout.eq.fmt_l) then
     if (fmtin.eq.fmt_l) then
        call bytoby(inbuf,outbuf,ndat)
     else
        call class_message(seve%e,proc,'Numeric to logical conversion is illegal.')
        error = .true.
     endif
     !
  elseif (fmtout.eq.fmt_r4) then
     if (fmtin.eq.fmt_i8) then
        call i8tor4(inbuf,outbuf,ndat)
     elseif (fmtin.eq.fmt_i4) then
        call i4tor4(inbuf,outbuf,ndat)
     elseif (fmtin.eq.fmt_i2) then
        call i2tor4(inbuf,outbuf,ndat)
     elseif (fmtin.eq.fmt_r4) then
        call r4tor4(inbuf,outbuf,ndat)
     elseif (fmtin.eq.fmt_r8) then
        call r8tor4(inbuf,outbuf,ndat)
     elseif (fmtin.eq.fmt_l) then
        chain = 'Logical to floating conversion is illegal.'
        call class_message(seve%e,proc,'Logical to floating conversion is illegal.')
        error = .true.
     else
        call class_message(seve%e,proc,'Input format not supported')
        error = .true.
     endif
     !
  elseif (fmtout.eq.fmt_r8) then
     if (fmtin.eq.fmt_i8) then
        call i8tor8(inbuf,outbuf,ndat)
     elseif (fmtin.eq.fmt_i4) then
        call i4tor8(inbuf,outbuf,ndat)
     elseif (fmtin.eq.fmt_i2) then
        call i2tor8(inbuf,outbuf,ndat)
     elseif (fmtin.eq.fmt_r4) then
        call r4tor8(inbuf,outbuf,ndat)
     elseif (fmtin.eq.fmt_r8) then
        call bytoby(inbuf,outbuf,8*ndat)
     elseif (fmtin.eq.fmt_l) then
        call class_message(seve%e,proc,'Logical to floating conversion is illegal')
        error = .true.
     else
        call class_message(seve%e,proc,'Input format not supported')
        error = .true.
     endif
  else
    call class_message(seve%e,proc,'Output format not supported')
    error = .true.
  endif
  !
end subroutine fits_convert
!
subroutine vararrayread(ioffset,vbuffer,isize,ierror)
  use gildas_def
  use gkernel_interfaces
  use class_fits
  !---------------------------------------------------------------------
  ! @ private
  ! FITS        Internal routine.
  !---------------------------------------------------------------------
  integer,                   intent(in)  :: ioffset         !
  integer(kind=size_length), intent(in)  :: isize           !
  integer(kind=1),           intent(out) :: vbuffer(isize)  !
  logical,                   intent(out) :: ierror          !
  ! Local
  integer(kind=record_length) :: vararrayrec
  integer(kind=4) :: vararrayb
  integer(kind=record_length) :: oldirec,afterirec
  integer :: oldib,afterib
  integer(kind=record_length) :: icalled
  !
  vararrayrec = heaprec
  vararrayb   = heapb+ioffset
  !
  do while (vararrayb.ge.2880)
    vararrayrec = vararrayrec+1
    vararrayb   = vararrayb-2880
  enddo
  !
  call gfits_getrecnum(oldirec)
  call gfits_getrecoffset(oldib)
  !
  ! g95 bugs if called directly with expression...
  icalled = vararrayrec-1
  call gfits_setrecnum(icalled)
  call gfits_setrecoffset(vararrayb)
  call gfits_getstdrec(ierror)
  call gfits_getbuf(vbuffer,isize,ierror)
  !
  call gfits_getrecnum(afterirec)
  call gfits_getrecoffset(afterib)
  icalled = oldirec -1
  call gfits_setrecnum(icalled)
  call gfits_setrecoffset(oldib)
  !
  call gfits_getstdrec(ierror) ! will add 1 to irec.
end subroutine vararrayread
