subroutine las_setdef(set,error)
  use gildas_def
  use gbl_constant
  use phys_const
  use classcore_interfaces, except_this=>las_setdef
  use class_types
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  !  Setup default parameters
  !---------------------------------------------------------------------
  type(class_setup_t), intent(inout) :: set    !
  logical,             intent(inout) :: error  ! Error flag
  ! Local
  real(kind=4) :: any
  integer(kind=4), parameter :: mini2=-32768
  integer(kind=4), parameter :: maxi2=32767
  integer(kind=4), parameter :: maxi4=2147483647_4
  integer(kind=8), parameter :: maxi8=9223372036854775807_8
  !
  call chtoby('ANY ',any,4)
  !
  set%method  = 'GAUSS'
  set%action  = 'O'
  set%align   = 'C'
  set%alig2   = 'I'
  set%offs1   = any
  set%offs2   = any
  set%offl1   = any
  set%offl2   = any
  set%obse1   = mini2
  set%obse2   = maxi2
  set%redu1   = mini2
  set%redu2   = maxi2
  set%match   = .true.
  set%fft     = .false.
  set%coord   = 0                   ! Automatic
  set%veloc   = vel_aut
  set%equinox = 2000.0
  set%line    = '*'
  set%heade   = 'L'
  set%plot    = 'H'
  set%sourc   = '*'
  set%modex   = 'A'
  set%modey   = 'A'
  set%modez   = 'T'
  set%teles   = '*'
  set%tole    = 2.e0*rad_per_sec
  set%qual1   = 0
  set%qual2   = 8
  set%posa1   = -1e7
  set%posa2   = +1e7
  set%freqmin = -1.d0
  set%freqmax = -1.d0
  set%freqsig = .true.
  set%weigh   = 'T'
  set%unitx(1) = 'F'
  set%unitx(2) = 'V'
  set%entr1 = 0
  set%entr2 = maxi8
  set%nume1 = 0
  set%nume2 = maxi8
  set%scan1 = 0
  set%scan2 = maxi8
  set%sub1  = 0
  set%sub2  = maxi4
  set%base = 1
  set%lbase = -2  ! BASE LAST (-2: invalid, -1: sinus, 0+: poly)
  set%nwind = setnwind_default
  set%bad = 'O'
  set%defext = '.30m'
  set%sort_name = 'NONE'
  set%do_sort = .false.
  set%do_scan = .false.
  set%slev = 1
  set%flev = 0
  set%scale = yunit_unknown  ! Unknown = do nothing
  set%write_r8 = .false.
  set%fupda = .false.
  set%uniqueness = .false.
  set%title%position = .true.
  set%title%quality = .true.
  set%title%spectral = .true.
  set%title%calibration = .false.
  set%title%atmosphere = .false.
  set%title%continuum = .true.
  set%title%origin = .false.
  set%origin = .false.
  set%beamt = 0.02
  set%gaint = 0
  set%obs_name = '*'   ! compute_doppler guess observatory from TELESCOPE name
  set%obs_long = 0.d0  ! Unused if name is *
  set%obs_lati = 0.d0  ! Unused if name is *
  set%obs_alti = 0.d0  ! Unused if name is *
  set%check%sou = .true.
  set%check%pos = .true.
  set%check%off = .true.
  set%check%lin = .true.
  set%check%spe = .true.
  set%check%cal = .true.
  set%check%swi = .true.
  set%varpresec(:) = setvar_off
  !
  ! Initialisation du Terminal Graphique
  gvx1 = -1.0
  gvx2 =  1.0
  gcx1 = -1.0
  gcx2 =  1.0
  gfx1 = -1.0
  gfx2 =  1.0
  gfxo =  0.d0
  gix1 = -1.0
  gix2 =  1.0
  gixo =  0.d0
  gux1 = -1.0
  gux2 =  1.0
  guy1 = -1.0
  guy2 =  1.0
  guy  =  1.0
  gux  =  1.0
  gcx  =  1.0
  gvx  =  1.0
  gfx  =  1.0
  gix  =  1.0
  gx1 = 3.
  gx2 = 28.0
  gy1 = 2.5
  gy2 = 15.5
  call gr_exec1('SET VIEWPORT 0.10 0.90 0.10 0.70')
  !
  error = .false.
end subroutine las_setdef
!
subroutine las_setup(set,error)
  use gildas_def
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>las_setup
  use class_common
  use class_fits
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  !  Called only once at startup
  !---------------------------------------------------------------------
  type(class_setup_t), intent(inout) :: set    !
  logical,             intent(inout) :: error  !
  !
  filein%nspec = 1
  fileout%nspec = 1
  !
  ! Initialize the Classic library
  call classic_init(error)
  if (error)  call sysexi(fatale)
  ! Initialize the FILE IN|OUT conversion routines to something (actually
  ! we want to call these routines to fill a Class user section even if
  ! no file is opened...)
  filein%conv%code = 0    ! No conversion
  call classic_conv(filein%conv,error)
  fileout%conv%code = 0    ! No conversion
  call classic_conv(fileout%conv,error)
  !
  ! Initialize the CURS01 common.
  !  xcurs = 16.
  !  ycurs = 9.
  !
  fits_mode = 'NONE'
  snbit = 32
  !
  call las_setdef(set,error)
  !
end subroutine las_setup
