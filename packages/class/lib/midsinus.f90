subroutine midsinus(obs,fit,ifatal,liter)
  use gildas_def
  use sinus_parameter
  use class_data
  use gkernel_interfaces
  use fit_minuit
  !-------------------------------------------------------------------
  ! @ private
  ! Start a BASE SINUS fit by building the par array and internal
  ! variable used by minuit
  ! IFATAL basically not used
  !-------------------------------------------------------------------
  type(observation) , intent(in)    :: obs    !
  type(fit_minuit_t), intent(inout) :: fit    ! Fitting variables
  logical,            intent(in)    :: liter  ! Iterate a fit
  integer(kind=4),    intent(out)   :: ifatal ! Number of fatal errors
  ! Local
  integer(kind=4) :: i,k
  real(kind=8) :: sav,sav2,vplu,vminu
  !
  fit%isw    = 0
  fit%sigma  = 0.d0
  fit%npfix  = 0
  fit%nu     = 0
  fit%npar   = 0
  ifatal = 0
  do i= 1, fit%maxext
     fit%u(i)       = 0.0d0
     fit%lcode(i)   = 0
     fit%lcorsp (i) = 0
  enddo
  fit%isw(5) = 1
  fit%data = locwrd(obs)
  !
  ! Output initial guesses
  if (fit%verbose) then
     write(6,1000) par(1),par(2),par(3)
  endif
  !
  ! set up parameters for major variables
  !
  ! amplitude
  fit%u(1)    = par(1)
  fit%alim(1) = 0.0d0
  fit%blim(1) = 1.0d10
  fit%werr(1) = 0.2*sigbas
  fit%lcode(1) = 4
  !
  ! period
  fit%u(2)    = par(2)
  fit%alim(2) = deltav
  fit%blim(2) = max(float(4*obs%cnchan)*deltav,par(2))
  fit%werr(2) = sqrt(float(obs%cnchan))*deltav
  fit%lcode(2) = 4
  !
  ! phase
  fit%u(3)    = par(3)
  fit%alim(3) = 0.0d0
  fit%blim(3) = max(fit%blim(2),dble(par(3)))
  fit%werr(3) = fit%werr(2)
  fit%lcode(3) = 4
  !
  ! slope
  fit%u(4) = par(4)
  fit%werr(4) = sigbas/(obs%cnchan*deltav)
  fit%lcode(4) = 1
  !
  ! offset
  fit%u(5) = par(5)
  fit%werr(5) = sigbas
  fit%lcode(5) = 1
  !
  ! o.k. start
  ! calculate step sizes dirin
  fit%npar = 0
  fit%nu = 5
  do k= 1, fit%nu
     if (fit%lcode(k) .gt. 0)  then
        fit%npar = fit%npar + 1
        fit%lcorsp(k) = fit%npar
        sav = fit%u(k)
        fit%x(fit%npar) = pintf(fit,sav,k)
        fit%xt(fit%npar) = fit%x(fit%npar)
        sav2 = sav + fit%werr(k)
        vplu = pintf(fit,sav2,k) - fit%x(fit%npar)
        sav2 = sav - fit%werr(k)
        vminu = pintf(fit,sav2,k) - fit%x(fit%npar)
        fit%dirin(fit%npar) = 0.5d0 * (abs(vplu) +abs(vminu))
     endif
  enddo
  !
1000 format(' Input parameters :  ',f8.3,'    ',f8.3,'    ',f8.3)
end subroutine midsinus
