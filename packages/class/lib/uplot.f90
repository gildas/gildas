subroutine uplot(x,y,imode)
  use plot_formula
  !-------------------------------------------------------------------
  ! @ private
  ! CLASS Internal routine
  !	Draw a line from current pen position to specified (X,Y) position.
  !	Several entries are available, and correspond  to different units
  !	for the coordinates. IMODE=3 corresponds to a pen up move.
  !	User unit entry.
  !-------------------------------------------------------------------
  real(kind=4), intent(in) :: x ! X coordinate
  real(kind=4), intent(in) :: y ! Y coordinate
  integer, intent(in) :: imode  ! Pen down (#3) or up (=3) mode
  real :: xp, yp
  !
  xp = gx1 + gux * (x-gux1)
  yp = gy1 + guy * (y-guy1)
  ! Do plot
  if (imode.eq.3) then
     call grelocate(xp,yp)
  else
     call gdraw(xp,yp)
  endif
end subroutine uplot
!
subroutine cplot(x,y,imode)
  use plot_formula
  !-------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  ! CLASS Internal routine
  !	Draw a line from current pen position to specified (X,Y) position.
  !	Several entries are available, and correspond  to different units
  !	for the coordinates. IMODE=3 corresponds to a pen up move.
  !	Channel units entry
  !-------------------------------------------------------------------
  real(kind=4), intent(in) :: x ! X coordinate
  real(kind=4), intent(in) :: y ! Y coordinate
  integer, intent(in) :: imode  ! Pen down (#3) or up (=3) mode
  real :: xp, yp
  !
  xp = gx1 + gcx * (x-gcx1)
  yp = gy1 + guy * (y-guy1)
  ! Do plot
  if (imode.eq.3) then
    call grelocate(xp,yp)
  else
    call gdraw(xp,yp)
  endif
end subroutine cplot
!
subroutine vplot(x,y,imode)
  use plot_formula
  !-------------------------------------------------------------------
  ! @ private
  ! CLASS Internal routine
  !	Draw a line from current pen position to specified (X,Y) position.
  !	Several entries are available, and correspond  to different units
  !	for the coordinates. IMODE=3 corresponds to a pen up move.
  !	Velocity units entry
  !-------------------------------------------------------------------
  real(kind=4), intent(in) :: x ! X coordinate
  real(kind=4), intent(in) :: y ! Y coordinate
  integer, intent(in) :: imode  ! Pen down (#3) or up (=3) mode
  real :: xp, yp
  !
  xp = gx1 + gvx * (x-gvx1)
  yp = gy1 + guy * (y-guy1)
  ! Do plot
  if (imode.eq.3) then
    call grelocate(xp,yp)
  else
    call gdraw(xp,yp)
  endif
end subroutine vplot
!
subroutine fplot(x,y,imode)
  use plot_formula
  !-------------------------------------------------------------------
  ! @ private
  ! CLASS Internal routine
  !	Draw a line from current pen position to specified (X,Y) position.
  !	Several entries are available, and correspond  to different units
  !	for the coordinates. IMODE=3 corresponds to a pen up move.
  !	Frequency units entry
  !-------------------------------------------------------------------
  real(kind=4), intent(in) :: x ! X coordinate
  real(kind=4), intent(in) :: y ! Y coordinate
  integer, intent(in) :: imode  ! Pen down (#3) or up (=3) mode
  real :: xp, yp
  !
  xp = gx1 + gfx * (x-gfx1)
  yp = gy1 + guy * (y-guy1)
  ! Do plot
  if (imode.eq.3) then
    call grelocate(xp,yp)
  else
    call gdraw(xp,yp)
  endif
end subroutine fplot
!
subroutine iplot(x,y,imode)
  use plot_formula
  !-------------------------------------------------------------------
  ! @ private
  ! CLASS Internal routine
  !	Draw a line from current pen position to specified (X,Y) position.
  !	Several entries are available, and correspond  to different units
  !	for the coordinates. IMODE=3 corresponds to a pen up move.
  !	Image units entry
  !-------------------------------------------------------------------
  real(kind=4), intent(in) :: x ! X coordinate
  real(kind=4), intent(in) :: y ! Y coordinate
  integer, intent(in) :: imode  ! Pen down (#3) or up (=3) mode
  real :: xp, yp
  !
  xp = gx1 + gix * (x-gix1)
  yp = gy1 + guy * (y-guy1)
  ! Do plot
  if (imode.eq.3) then
    call grelocate(xp,yp)
  else
    call gdraw(xp,yp)
  endif
end subroutine iplot
!
subroutine gplot(x,y,imode)
  !-------------------------------------------------------------------
  ! @ private
  ! CLASS Internal routine
  !	Draw a line from current pen position to specified (X,Y) position.
  !	Several entries are available, and correspond  to different units
  !	for the coordinates. IMODE=3 corresponds to a pen up move.
  !	Paper units entry
  !-------------------------------------------------------------------
  real(kind=4), intent(in) :: x ! X coordinate
  real(kind=4), intent(in) :: y ! Y coordinate
  integer, intent(in) :: imode  ! Pen down (#3) or up (=3) mode
  real :: xp, yp
  !
  xp = x
  yp = y
  ! Do plot
  if (imode.eq.3) then
    call grelocate(xp,yp)
  else
    call gdraw(xp,yp)
  endif
end subroutine gplot
