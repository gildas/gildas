module plot_formula
  use class_parameter
  real(kind=plot_length) :: gx1,gx2,gy1,gy2  ! box location
  real(kind=plot_length) :: gvx, gvx1, gvx2  ! in velocity
  real(kind=plot_length) :: gcx, gcx1, gcx2  ! in channels
  real(kind=plot_length) :: gfx, gfx1, gfx2  ! in rest frequency offset
  real(kind=8)           :: gfxo             ! the rest frequency offset
  real(kind=plot_length) :: gix, gix1, gix2  ! in image offset
  real(kind=8)           :: gixo             ! the image frequency offset
  real(kind=plot_length) :: gux, gux1, gux2  ! in current units
  real(kind=plot_length) :: guy, guy1, guy2  ! on y axis
  real(kind=plot_length) :: guz, guz1, guz2  ! on y axis
end module plot_formula
!
module class_popup
  use classic_params
  use class_parameter
  !
  integer(kind=4), parameter :: cpop_none=0
  integer(kind=4), parameter :: cpop_plotindex=-2
  integer(kind=4), parameter :: cpop_stamp=-1
  integer(kind=4), parameter :: cpop_map=+1
  !
  integer(kind=obsnum_length), allocatable :: ipop(:)
  real(kind=4),                allocatable :: xpop(:)
  real(kind=4),                allocatable :: ypop(:)
  integer(kind=entry_length) :: npop
  integer(kind=4) :: cpop
  real(kind=4) :: pgx1,pgx2,pux1,pux2
  real(kind=4) :: pgy1,pgy2,puy1,puy2
  real(kind=4) :: pgz1,pgz2,puz1,puz2
end module class_popup
!
module class_common
  use image_def
  use classic_api
  use class_types
  !
  type(classic_file_t), save :: filein,fileout
  logical :: filein_isvlm,fileout_isvlm
  type(gildas), save :: filein_vlmhead
  !
  integer(kind=4) :: lun_1,lun_2  ! Support LUNs, reserved once for efficiency
  !
  type(classic_recordbuf_t) :: ibufbi   ! File In Index buffer
  type(classic_recordbuf_t) :: ibufobs  ! File In Observation buffer
  integer(kind=4) :: idatabi(lind_v1)   ! Index buffer. Use length = max(lind_v*)
  type(classic_recordbuf_t) :: obufbi   ! File Out Index buffer
  type(classic_recordbuf_t) :: obufobs  ! File Out Observation buffer
  integer(kind=4) :: odatabi(lind_v1)   ! Index buffer. Use length = max(lind_v*)
  !
  logical :: outobs_modify  ! The current observation is open for modification
  !
end module class_common
