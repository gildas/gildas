subroutine class_fft(set,line,r,error,user_function)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_fft
  use class_data
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! CLASS ANALYSE Support routine for command
  !
  !   FFT
  ! 1     [/REMOVE wmin wmax]
  ! 2     [[s1 e1] [s2 e2] ... [s10 e10] /KILL]
  ! 3     [/INDEX]
  ! 4     [/OBS]
  ! 5     [/NOCURS]
  ! 6     [/CURS]   Obsolete since 10-sep-2010
  !
  ! Plots the power spectrum of the current scan
  !---------------------------------------------------------------------
  type(class_setup_t), intent(inout)         :: set            !
  character(len=*),    intent(in)            :: line           ! Command line
  type(observation),   intent(inout), target :: r              !
  logical,             intent(inout)         :: error          ! Error status
  logical,             external              :: user_function  !
  ! Local
  character(len=*), parameter :: rname='FFT'
  logical :: doremove,dokill,doindex,docurs
  real(kind=4) :: wmin,wmax
  integer(kind=4), parameter :: mkill=10
  integer(kind=4) :: iarg,nkill
  real(kind=8) :: fkill(2,mkill)
  type(observation), pointer :: obs
  !
  if (sic_present(6,0)) then
    ! Obsolete since 10-sep-2010
    call class_message(seve%w,rname,'Option /CURSOR is obsolete.')
    call class_message(seve%w,rname,'The cursor is always available on the devices supporting it.')
  endif
  !
  ! Decode command line arguments
  if (sic_present(5,0).and.sic_present(6,0) .or.  &
      sic_present(3,0).and.sic_present(4,0)) then
     call class_message(seve%e,rname,'Options are not compatible')
     error = .true.
     return
  endif
  doremove = sic_present(1,0)
  dokill   = sic_present(2,0)
  doindex  = sic_present(3,0)
  docurs   = .not.sic_present(5,0)
  !
  ! /REMOVE wmin wmax (in current X units)
  if (doremove) then
    wmin = 0.
    call sic_r4(line,1,1,wmin,.false.,error)
    if (error) return
    wmax = wmin
    call sic_r4(line,1,2,wmax,.false.,error)
    if (error) return
  endif
  !
  if (doindex) then
    obs => p
  else
    obs => r
  endif
  !
  ! Get kill windows in the FFT (units are MHz-1)
  nkill = 0
  if (sic_narg(0).ge.1 .and. .not.dokill) then
     call class_message(seve%w,rname,'Input parameters ignored. Use /KILL')
  endif
  if (dokill) then
    if (sic_narg(0).gt.0) then
      ! Get kill windows from command line
      do iarg = 1,sic_narg(0),2
        nkill = nkill+1
        call sic_r8 (line,0,iarg,fkill(1,nkill),.true.,error)
        if (error) return
        call sic_r8 (line,0,iarg+1,fkill(2,nkill),.true.,error)
        if (error) return
      enddo
    else
      ! Get kill windows from cursor
      ! 1) Compute the FFT and plot it (disable any action on the R or P)
      ! NB: no more line parsing after this call (because of internal gr_exec)
      call class_fft_do(set,obs,doindex,.false.,.false.,0,fkill,0.,0.,  &
        error,user_function)
      if (error)  return
      ! 2) Get windows from cursor
      call class_fft_kill_getwindows(docurs,fkill,mkill,nkill,error)
      if (error)  return
    endif
  endif
  !
  call class_fft_do(set,obs,doindex,docurs,doremove,nkill,fkill,wmin,wmax,  &
    error,user_function)
  if (error)  return
  !
end subroutine class_fft
!
subroutine class_fft_do(set,obs,doindex,docurs,doremove,nkill,fkill,  &
  wmin,wmax,error,user_function)
  use gbl_message
  use classcore_interfaces, except_this=>class_fft_do
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(class_setup_t), intent(inout) :: set             !
  type(observation),   intent(inout) :: obs             !
  logical,             intent(in)    :: doindex         ! Index or obs mode?
  logical,             intent(in)    :: docurs          ! Get parameters from cursor
  logical,             intent(in)    :: doremove        ! Remove option
  integer(kind=4),     intent(in)    :: nkill           ! Number of FFT windows to kill
  real(kind=8),        intent(in)    :: fkill(2,nkill)  ! FFT window values
  real(kind=4),        intent(in)    :: wmin            ! Lower bound for window removal
  real(kind=4),        intent(in)    :: wmax            ! Upper bound for window removal
  logical,             intent(inout) :: error           ! Error status
  logical,             external      :: user_function   !
  ! Local
  character(len=*), parameter :: rname='FFT'
  integer(kind=4) :: nx,ny
  !
  ! Checks and reserve space
  if (doindex) then
     if (.not.associated(obs%data2)) then
        call class_message(seve%e,rname,'No index loaded')
        error = .true.
        return
     endif
     ny = obs%head%des%ndump
  else
     if (obs%head%xnum.eq.0) then
        call class_message(seve%e,rname,'No spectrum in memory')
        error = .true.
        return
     endif
     ny = 1
  endif
  !
  if (obs%head%gen%kind.eq.kind_spec) then
    nx = obs%head%spe%nchan
  elseif (obs%head%gen%kind.eq.kind_cont) then
    nx = obs%head%dri%npoin
  endif
  !
  call sub_fourier(set,obs,nx,ny,user_function,nkill,fkill,wmin,wmax,  &
    doindex,docurs,doremove,error)
  if (error)  return
  !
end subroutine class_fft_do
!
subroutine sub_fourier(set,obs,nx,ny,user_function,nkill,fkill,wmin,wmax,  &
  doindex,docurs,doremove,error)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>sub_fourier
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS Support routine for command
  !    FFT [Wave1 Wave2 [Wave3 ...]]
  !  Display and possibly edit the Fourier Transform of the spectrum.
  !  Interpolation is done in Amplitude and Phase, but only the Power
  !  Spectrum is displayed
  !---------------------------------------------------------------------
  type(class_setup_t), intent(inout) :: set             !
  type(observation),   intent(inout) :: obs             ! Input observation
  integer(kind=4),     intent(in)    :: nx              ! X-size
  integer(kind=4),     intent(in)    :: ny              ! Y-size
  logical,             external      :: user_function   !
  integer(kind=4),     intent(in)    :: nkill           ! Number of FFT windows to kill
  real(kind=8),        intent(in)    :: fkill(2,nkill)  ! FFT window values
  real(kind=4),        intent(in)    :: wmin            ! Lower bound for window removal
  real(kind=4),        intent(in)    :: wmax            ! Upper bound for window removal
  logical,             intent(in)    :: doindex         ! Observation or index mode?
  logical,             intent(in)    :: docurs          ! Get parameters from cursor
  logical,             intent(in)    :: doremove        ! Remove option
  logical,             intent(inout) :: error           ! Error status
  ! Local
  character(len=*), parameter :: rname='FFT'
  complex(kind=4), allocatable :: cdata(:,:)
  real(kind=4), allocatable :: removed(:,:)
  integer(kind=4) :: i,j,ier
  real(kind=4) :: cbad
  real(kind=4), parameter :: ebad=0.0
  !
  ! Initializations
  cbad = obs_bad(obs%head)
  !
  allocate(cdata(nx,ny),removed(nx,ny),stat=ier)
  if (failed_allocate(rname,'CDATA buffer',ier,error))  return
  removed(:,:) = 0.
  cdata(:,:) = 0.
  !
  if (doindex) then
     do j=1,ny
        obs%spectre => obs%data2(:,j)
        do i=1,nx
           cdata(i,j) = obs_good(obs,i)
           removed(i,j) = 0.
        enddo
     enddo
  else
     do i=1,nx
        cdata(i,1) = obs_good(obs,i)
        removed(i,1) = 0.
     enddo
  endif
  !
  call reallocate_fft(obs%fft,nx,ny,error)
  if (error)  return
  !
  ! Remove channels in [ixmin:ixmax] if required
  ! If ixmin:ixmax is supplied, the interval is filled with a straight line
  ! If not, the sum of all fitted components is subtracted.
  if (doremove) then
     call class_fft_remove(set,obs,cdata,wmin,wmax,docurs,user_function,error)
     if (error)  goto 99
  endif
  !
  ! Compute the Fourier transform and associadted actions
  call class_fft_compute(obs,cdata,error)
  if (error)  goto 99
  !
  ! Plot power spectrum
  call class_fft_plot(set,obs,cbad,ebad,error)
  if (error)  goto 99
  !
  ! Manipulate the Fourier spectrum
  if (nkill.ge.0) then
    call class_fft_kill_fill(set,obs,cdata,fkill,nkill,doindex,removed,error)
    if (error)  goto 99
  endif
  deallocate(cdata,removed)
  return
  !
  ! On error
99 continue
  deallocate(cdata,removed)
  call newdat(set,obs,error)
  error = .true.
  set%fft = .false.
  return
  !
end subroutine sub_fourier
!
subroutine class_fft_remove(set,obs,cdata,v1,v2,docurs,user_function,error)
  use gbl_message
  use gkernel_types
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_fft_remove
  use class_parameter
  use class_index
  use class_types
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  !   Support routine for command:
  !     FFT /REMOVE
  !   Remove fitted lines or interval
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  type(observation),   intent(in)    :: obs            ! Input observation
  complex(kind=4),     intent(inout) :: cdata(obs%fft%nx,obs%fft%ny)
  real(kind=4),        intent(in)    :: v1             ! Interval lower bound
  real(kind=4),        intent(in)    :: v2             ! Interval upper bound
  logical,             intent(in)    :: docurs         ! Get values from cursor ?
  logical,             external      :: user_function  !
  logical,             intent(out)   :: error          ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='FFT /REMOVE'
  type(observation) :: obs_local
  integer(kind=4) :: i1,i2,ichan,is,box(4),ixmin(obs%fft%ny),ixmax(obs%fft%ny)
  integer(kind=obsnum_length) :: iobs
  logical :: m2d(obs%fft%nx,obs%fft%ny),r1d(obs%fft%ny),from_fit,from_curs
  logical :: index,removed,ldummy
  real(kind=8) :: xconv(3),yconv(3)
  real(kind=4) :: a,b,dummy
  real(kind=4), dimension(obs%fft%ny) :: wmin,wmax
  real(kind=4), dimension(obs%fft%nx,obs%fft%ny) :: diff
  character(len=80) :: chain
  type(polygon_t) :: poly
  !
  ! Index or observation
  if (obs%fft%ny.gt.1) then
     index = .true.
  else
     index = .false.
  endif
  !
  ! From cursor if needed
  if (docurs.and.v1.eq.v2) then
     from_curs = .true.
  else
     from_curs = .false.
  endif
  if (from_curs.and..not.gtg_curs()) then
     call class_message(seve%e,rname,'No cursor available')
     error = .true.
     return
  endif
  !
  ! From fitted components
  if (.not.from_curs.and.v1.eq.v2) then
     from_fit = .true.
  else
     from_fit = .false.
  endif
  !
  ! Check that removal feasible
  if (.not.from_curs.and..not.from_fit.and.v1.eq.v2) then
     call class_message(seve%e,rname,'Not enough parameters for window removal')
     error = .true.
     return
  endif
  !
  if (from_fit) then
     if (index) then
       call class_message(seve%e,rname,'FFT /REMOVE /NOCURS /INDEX is not implemented')
       error = .true.
       return
     else
       call class_message(seve%e,rname,'FFT /REMOVE /NOCURS is not implemented')
       call class_message(seve%e,rname,'You can use RESIDUAL then FFT as a replacement')
       error = .true.
       return
     endif
  else if (from_curs) then
     if (index) then
        ! FFT /REMOVE /INDEX: use a polygon for the boundaries. As this is
        ! coded for now, it reuses the previous Greg polygon (cursor is not
        ! invoked)
        !
        ! Get conversion formula
        xconv(1) = gcx1
        xconv(2) = gux1
        xconv(3) = (gux2-gux1)/(gcx2-gcx1)
        yconv(1) = 0
        yconv(2) = 0
        yconv(3) = 1
        ! blc
        box(1) = gcx1
        box(2) = 1
        ! trc
        box(3) = gcx2
        box(4) = obs%fft%ny
        !
        call greg_poly_get(poly)  ! Get the Greg polygon
        if (poly%ngon.le.2) then
          call class_message(seve%e,rname,'No polygon available')
          error = .true.
          return
        endif
        m2d(:,:) = .false.
        call gr8_glmsk(poly,m2d,obs%fft%nx,obs%fft%ny,xconv,yconv,box)
        !
        ! Loop on observations
        do iobs=1,obs%fft%ny
           ! Determine the window for each observation
           ! Make linear interpolation for each observation
           is = max(2,box(1))
           if (all(.not.m2d(:,iobs))) then
              r1d(iobs) = .false.
           else
              r1d(iobs) = .true.
              do ichan=is,box(3)
                 if (m2d(ichan,iobs).and.(.not.m2d(ichan-1,iobs))) then
                    i1 = obs%datax(ichan)
                 else if (m2d(ichan-1,iobs).and.(.not.m2d(ichan,iobs))) then
                    i2 = obs%datax(ichan-1)
                    exit
                 endif
              enddo
              ixmin(iobs) = max(1,min(i1,i2))
              ixmax(iobs) = min(max(i1,i2),obs%fft%nx)
              if (ixmax(iobs)-ixmin(iobs).lt.2) r1d(iobs) = .false.
              call abscissa_chan2velo(obs%head,real(i1),wmin(iobs))
              call abscissa_chan2velo(obs%head,real(i2),wmax(iobs))
           endif
        enddo
        removed = .false.
     else
        !
        ! Observation level
        call class_message(seve%i,rname,'Enter boundaries (N or space); Type E to exit')
        call wincur(set,1,i1,wmin,wmax,'WINDOW')
        ! convert wmin wmax into pixel
        i1 = (wmin(1)-obs%datax(1))/(obs%datax(2)-obs%datax(1))+1
        i2 = (wmax(1)-obs%datax(1))/(obs%datax(2)-obs%datax(1))+1
        ixmin = max(1,min(i1,i2))
        ixmax = min(max(i1,i2),obs%fft%nx)
        wmin = wmin(1)
        wmax = wmax(1)
        r1d = .true.
        removed = .false.
     endif
  else ! From input values
     i1 = (v1-obs%datax(1))/(obs%datax(2)-obs%datax(1))+1
     i2 = (v2-obs%datax(1))/(obs%datax(2)-obs%datax(1))+1
     ixmin = max(1,min(i1,i2))
     ixmax = min(max(i1,i2),obs%fft%nx)
     wmin = v1
     wmax = v2
     r1d = .true.
     removed = .false.
  endif
  !
  ! Perform linear interpolation if needed
  if (.not.removed) then
     do iobs=1,obs%fft%ny
        if (r1d(iobs)) then
           write(chain,'(2(a,f10.2))') 'Interpolate spectrum from ',wmin(iobs),' to ',wmax(iobs)
           call class_message(seve%i,rname,chain)
           do ichan=ixmin(iobs),ixmax(iobs)
             cdata(ichan,iobs) = &
                cdata(ixmin(iobs),iobs)*(ixmax(iobs)-ichan)/(ixmax(iobs)-ixmin(iobs))  &
              + cdata(ixmax(iobs),iobs)*(ichan-ixmin(iobs))/(ixmax(iobs)-ixmin(iobs))
           enddo
        endif
     enddo
  endif
end subroutine class_fft_remove
!
subroutine class_fft_kill(obs,cdata,fkill,nkill,fill,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_fft_kill
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Support routine for command:
  !   FFT /KILL
  !---------------------------------------------------------------------
  type(observation), intent(in)    :: obs             ! Input observation
  complex(kind=4),   intent(inout) :: cdata(obs%fft%nx,obs%fft%ny)
  integer(kind=4),   intent(in)    :: nkill           ! Number of FFT windows to kill
  real(kind=8),      intent(in)    :: fkill(2,nkill)  ! FFT window values
  logical,           intent(out)   :: fill            !
  logical,           intent(inout) :: error           ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='FFT /KILL'
  integer(kind=4) :: i,j,lx,jx,k
  real(kind=4) :: cbad
  !
  fill = nkill.gt.0
  if (.not.fill)  return
  !
  cbad = obs_bad(obs%head)
  !
  ! Removing channels in [fkill(1,i):fkill(2,i)]
  do i=1,nkill
    lx = max(1,min(nint(fkill(1,i)/obs%fft%xinc),obs%fft%nchan))
    jx = max(1,min(nint(fkill(2,i)/obs%fft%xinc),obs%fft%nchan))
    do j=1,obs%fft%ny
      do k = min(lx,jx), max(lx,jx)
        cdata(k,j) = cmplx(cbad,0.0)
      enddo
    enddo
    fill = .true.
  enddo
  !
end subroutine class_fft_kill
!
subroutine class_fft_compute(obs,cdata,error)
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_fft_compute
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Compute obs%fft%datay(:,:) and obs%fft%datax(:), and associated X
  ! Fourier axis description.
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs    !
  complex(kind=4),   intent(inout) :: cdata(obs%fft%nx,obs%fft%ny)
  logical,           intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='FFT>COMPUTE'
  real(kind=4) :: ux1,ux2,ux
  integer(kind=4) :: j,ichan,dims(1)
  real(kind=4) :: work(obs%fft%nx,2)  ! Automatic array
  !
  ! --- Y data ---
  !
  ! Compute Direct Fourier transform
  dims(1) = obs%fft%nx
  work(:,:) = 0.
  do j=1,obs%fft%ny
    call fourt(cdata(1,j),dims,1,1,0,work)
  enddo
  !
  ! Compute modulus and normalize
  obs%fft%datay(:,:) = abs(cdata)/obs%fft%nx*2
  obs%fft%nchan = obs%fft%nx/2+1
  ! Special case of middle channel if odd number
  if (obs%fft%nx.eq.(obs%fft%nchan-1)*2)  &
    obs%fft%datay(obs%fft%nchan,:) = obs%fft%datay(obs%fft%nchan,:)*0.5
  !
  !
  ! --- X data ---
  !
  ux1 = 0.
  select case (obs%head%gen%kind)
  case (kind_spec)
    ux2 = 0.5/abs(obs%head%spe%fres)
  case (kind_cont)
    ux2 = 0.5/abs(obs%head%dri%ares)
  case default
    call class_message(seve%e,rname,'Kind of data not supported')
    error = .true.
    return
  end select
  ux = (ux2-ux1)/(obs%fft%nchan-1.)
  !
  obs%fft%xref = 1.0
  obs%fft%xval = ux1
  obs%fft%xinc = ux
  !
  ! Fill all datax. NB: actually because of folding this is useful only
  ! up to obs%fft%nchan e.g. for plotting
  do ichan=1,obs%fft%nx
    obs%fft%datax(ichan) = (ichan-obs%fft%xref)*obs%fft%xinc + obs%fft%xval
  enddo
  !
  if (.not.obs%is_R)  return
  !
  ! This is the R buffer: add Sic variables R%FFT%X and R%FFT%Y
  call sic_delvariable ('R%FFT',.false.,error)
  if (error)  error = .false.
  !
  call sic_defstructure('R%FFT',.true.,error)
  if (error)  return
  ! NB: buffer exposed up to Nchan
  call sic_def_inte('R%FFT%N',obs%fft%nchan,0,0,.true.,error)
  call sic_def_real('R%FFT%X',obs%fft%datax,1,obs%fft%nchan,.true.,error)
  call sic_def_real('R%FFT%Y',obs%fft%datay,1,obs%fft%nchan,.true.,error)
  if (error)  return
  !
end subroutine class_fft_compute
!
subroutine reallocate_fft(fft,new_nx,new_ny,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>reallocate_fft
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(class_observation_fft_t), intent(inout) :: fft
  integer(kind=4),               intent(in)    :: new_nx
  integer(kind=4),               intent(in)    :: new_ny
  logical,                       intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='REALLOCATE>FFT'
  logical :: reallocate
  integer(kind=4) :: old_nx,old_ny,ier
  !
  ! Sanity check
  if (new_nx.le.0 .or. new_ny.le.0) then
     call class_message(seve%e,rname,'Array size is null or negative!')
     error = .true.
     return
  endif
  !
  if (allocated(fft%datay)) then
    old_nx = ubound(fft%datay,1)
    old_ny = ubound(fft%datay,2)
    if (old_nx.ne.new_nx .or. old_ny.ne.new_ny) then
      ! Not same size
      deallocate(fft%datax,fft%datay)
      fft%nx = 0
      fft%ny = 0
      reallocate = .true.
    else
      ! Same size
      reallocate = .false.
    endif
  else
    reallocate = .true.
  endif
  !
  if (reallocate) then
    allocate(fft%datax(new_nx),         &
             fft%datay(new_nx,new_ny),  &
             stat=ier)
    if (failed_allocate(rname,'FFT arrays',ier,error)) return
    fft%nx = new_nx
    fft%ny = new_ny
  endif
  !
end subroutine reallocate_fft
!
subroutine class_fft_plot(set,obs,cbad,ebad,error)
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_fft_plot
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Plot power spectrum
  !---------------------------------------------------------------------
  type(class_setup_t), intent(inout) :: set    !
  type(observation),   intent(in)    :: obs    !
  real(kind=4),        intent(in)    :: cbad   !
  real(kind=4),        intent(in)    :: ebad   !
  logical,             intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='FFT'
  character(len=80) :: chain
  real(kind=4) :: a1,a2,ux1,ux2,lx2,ly2,dummy
  real(kind=8) :: conv(6)
  !
  ! Find limits
  call class_minmax(a1,a2,obs%fft%datay,obs%fft%nx,obs%fft%ny,0.)
  !
  call gtclear
  ux1 = (1.0          -obs%fft%xref)*obs%fft%xinc+obs%fft%xval
  ux2 = (obs%fft%nchan-obs%fft%xref)*obs%fft%xinc+obs%fft%xval
  if (obs%fft%ny.eq.1) then
     write(chain,1000) 'LIMITS ',ux1,ux2,a1,a2
     call gr_exec(chain(1:lenc(chain)))
     call gr_segm('FFT',error)
     if (set%plot.eq.'N') then
       call gr4_connect(obs%fft%nchan,obs%fft%datax,obs%fft%datay,cbad,ebad)
     else
       call gr4_histo  (obs%fft%nchan,obs%fft%datax,obs%fft%datay,cbad,ebad)
     endif
     call gr_segm_close(error)
     if (error)  return
  else
     write(chain,1000) 'LIMITS ',ux1,ux2,0.5,obs%fft%ny+0.5
     call gr_exec(chain(1:lenc(chain)))
     conv(1) = obs%fft%xref
     conv(2) = obs%fft%xval
     conv(3) = obs%fft%xinc
     conv(4) = 0.d0
     conv(5) = 0.d0
     conv(6) = 1.d0
     call gr4_tgive(obs%fft%nx,obs%fft%ny,conv,obs%fft%datay)
     write(chain,'(a,2g20.5)') 'PLOT /SCALING LINEAR ',a1,a2
     call gr_exec(chain(1:lenc(chain)))
  endif
  !
  ! Plot box (current position)
  call gr_exec('BOX')
  if (obs%fft%ny.gt.1)  call gr_exec2('WEDGE')
  !
  ! Write text: Label and title
  chain = 'LABEL "Inverse Frequency (MHz\\u-1\d)" /X'
  call gr_exec(chain)
  call sic_get_real('PAGE_X',lx2,error)
  call sic_get_real('PAGE_Y',ly2,error)
  call sic_get_real('CHARACTER_SIZE',dummy,error)
  lx2 = lx2*0.5
  ly2 = ly2-dummy*0.575
  call out0('Graphic',lx2,ly2,error)
  call titout(set,obs%head,set%heade,'O')
  !
  ! Set Fourier space
  set%fft = .true.
1000 format(a,4(1x,g14.7))
end subroutine class_fft_plot
!
subroutine class_fft_kill_fill(set,obs,cdata,fkill,nkill,doindex,removed,error)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_fft_kill_fill
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Support routine for command:
  !   FFT /KILL
  !   1: kill the desired window(s)
  !   2: replace (fill) these windows
  !---------------------------------------------------------------------
  type(class_setup_t), intent(inout) :: set             !
  type(observation),   intent(inout) :: obs             !
  complex(kind=4),     intent(inout) :: cdata(obs%fft%nx,obs%fft%ny)
  integer(kind=4),     intent(in)    :: nkill           ! Number of FFT windows to kill
  real(kind=8),        intent(in)    :: fkill(2,nkill)  ! FFT window values
  logical,             intent(in)    :: doindex         !
  real(kind=4),        intent(in)    :: removed(obs%fft%nx,obs%fft%ny)
  logical,             intent(inout) :: error           ! Logical error flag
  ! Local
  logical :: fill
  real(kind=4) :: cbad
  real(kind=4), parameter :: ebad=0.0
  integer(kind=4) :: i,j,dims(1)
  real(kind=4) :: work(obs%fft%nx,2)
  !
  call class_fft_kill(obs,cdata,fkill,nkill,fill,error)
  if (error)  return
  !
  if (.not.fill)  return
  !
  ! Recompute and replot the power spectrum
  cbad = obs_bad(obs%head)
  do j=1,obs%fft%ny
    cdata(obs%fft%nchan+1,j) = 0.0
    do i=1,obs%fft%nchan
      if (real(cdata(i,j)).eq.cbad) then
        cdata(i,j) = cfillin (cdata(1,j),i,1,obs%fft%nchan+1,cbad)
      endif
      obs%fft%datay(i,j) = abs(cdata(i,j))/obs%fft%nx*2
    enddo
    if (obs%fft%nx.eq.(obs%fft%nchan-1)*2)  &
      obs%fft%datay(obs%fft%nchan,j) = obs%fft%datay(obs%fft%nchan,j)*0.5
    do i=2,obs%fft%nchan
      cdata(obs%fft%nx-i+2,j) = conjg(cdata(i,j))
    enddo
  enddo
  call class_fft_plot(set,obs,cbad,ebad,error)
  !
  ! Save result in obs
  dims(1) = obs%fft%nx
  do j=1,obs%fft%ny
    call fourt(cdata(1,j),dims,1,-1,1,work)
  enddo
  if (doindex) then
    obs%data2 = real(cdata)/obs%fft%nx + removed
  else
    obs%spectre = real(cdata(:,1))/obs%fft%nx + removed(:,1)
  endif
  !
end subroutine class_fft_kill_fill
!
subroutine class_fft_kill_getwindows(docurs,fkill,mkill,nkill,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_fft_kill_getwindows
  !---------------------------------------------------------------------
  ! @ private
  !  Get the "inverse frequency" window(s) to be killed from cursor
  !---------------------------------------------------------------------
  logical,          intent(in)    :: docurs          !
  integer(kind=4),  intent(in)    :: mkill           ! Max number of windows
  real(kind=8),     intent(out)   :: fkill(2,mkill)  ! Kill windows
  integer(kind=4),  intent(out)   :: nkill           ! Actual number of windows
  logical,          intent(inout) :: error           ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='FFT /KILL'
  character(len=*), parameter :: segname='FFT_KILL'
  character(len=1) :: ch
  logical :: flast
  real(kind=8) :: xcurs,y8
  real(kind=4) :: x4,y4
  character(len=message_length) :: mess
  integer(kind=4) :: old_pen,new_pen
  !
  nkill = 0
  if (.not.docurs) then
    call class_message(seve%e,rname,'No input parameters and cursor disabled.')
    error = .true.
    return
  endif
  if (.not.gtg_curs()) then
    call class_message(seve%e,rname,'No cursor available')
    error = .true.
    return
  endif
  !
  call class_message(seve%i,rname,  &
    'Define windows with k or <space>; Type E to exit, H for help')
  flast = .false.
  ch = ' '
  old_pen = gr_spen(1)
  do while (ch.ne.'E'.and.ch.ne.'e')
    call gr_curs(xcurs,y8,x4,y4,ch)
    if (gterrtst()) then
      error = .true.
      return
    endif
    !
    if (ch.eq.'K' .or. ch.eq.'k' .or. ch.eq.' ') then
      if (flast) then
        nkill = nkill+1
        fkill(2,nkill) = xcurs
        write(mess,101) 'KILL',nkill,'up ',fkill(2,nkill),' MHz^-1'
        call class_message(seve%r,'KILL',mess)
        !
        call gr_segm(segname,error)
        call relocate(fkill(2,nkill),0.d0)
        call draw(fkill(1,nkill),0.d0)
        call gr_segm_close(error)
        if (error)  return
        if (nkill.eq.1)  &
          call gr_exec('DRAW TEXT 0 1 "Killed" 8 0 /BOX 8')
        flast = .false.
        !
      else
        if (nkill.eq.mkill) then
          write(mess,'(A,I0,A)')  &
            'Only ',mkill,' kill FFT windows can de defined. Ignored.'
          call class_message(seve%w,rname,mess)
          exit
        endif
        fkill(1,nkill+1) = xcurs
        write(mess,101) 'KILL',nkill+1,'low',fkill(1,nkill+1),' MHz^-1'
        call class_message(seve%r,'KILL',mess)
        !
        flast = .true.
      endif
      !
      !
    elseif (ch.eq.'H'.or.ch.eq.'h') then
      write(6,100)
    endif
  enddo
  new_pen = gr_spen(old_pen)
  !
100 format(' Use key K to delete Fourier components. First strike',/   &
          ,' marks beginning of area to be deleted, next strike the end.'/   &
          ,' Upon exit, Fourier components are interpolated in module and'/   &
          ,' phase from the boundaries of deleted areas.')
  !
101 format(2x,A,' #',I0,' (',A,') : ',F7.3,A)
  !
end subroutine class_fft_kill_getwindows
!
complex function cfillin(cr,ival,imin,imax,bad)
  use classcore_interfaces, except_this=>cfillin
  !---------------------------------------------------------------------
  ! @ private
  !  Interpolate bad channel (if possible)
  ! ---
  !  Complex*4 version
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)  ::  imax      ! Last pixel in array
  complex(kind=4), intent(in)  ::  cr(imax)  ! Array to be interpolated
  integer(kind=4), intent(in)  ::  ival      ! Pixel to be interpolated
  integer(kind=4), intent(in)  ::  imin      ! First pixel in array
  real(kind=4),    intent(in)  ::  bad       ! Blanking value
  ! Local
  complex(kind=4) :: cbad
  real(kind=4) :: al,af,phasel,phasef,phase,modul
  integer(kind=4) :: i,if,il
  !
  cbad = cmplx(bad,0.0)
  ! Any good channel below ?
  do i=ival-1,imin,-1
     if (cr(i).ne.cbad) goto 10
  enddo
  !
  ! No good channel before 'ival'. Try after.
  do i=ival+1,imax-1
     if (cr(i).ne.cbad) goto 10
  enddo
  !
  ! All are bad. Perhapas last one.
  cfillin = cr(imax)
  return
  !
  ! One good channel either before or after
  ! If one before, search one after
  ! If not before, search for a second one after
10 if = i
  do i=max(if+1,ival+1),imax
     if (cr(i).ne.cbad) goto 20
  enddo
  ! Only one good channel, before or after
  if (if.gt.ival .or. if.eq.imin) then
     cfillin = cr(if)
     return
  endif
  ! The good channel was before. Any other before ?
  do i=if-1,imin,-1
     if (cr(i).ne.cbad) goto 20
  enddo
  ! Definitly, only one good left
  cfillin = cr(if)
  return
  !
  ! Two good channels.
20 il = i
  !
  ! Interpolate Real and Imaginary
  cfillin = ( (ival-if)*cr(il) + (il-ival)*cr(if) ) / real(il-if)
  return
  !
  ! Old code: interpolate Phase and Module
  af = abs(cr(if))
  al = abs(cr(il))
  if (af.ne.0.and.al.ne.0) then
     phasef = atan2(aimag(cr(if)),real(cr(if)))
     phasel = atan2(aimag(cr(il)),real(cr(il)))
     phase = ( (ival-if)*phasel + (il-ival)*phasef) / real(il-if)
     modul = ( (ival-if)*al     + (il-ival)*af    ) / real(il-if)
  elseif (af.ne.0) then
     phase = atan2(aimag(cr(if)),real(cr(if)))
     modul = (il-ival)*af / real(il-if)
  elseif (al.ne.0) then
     phase = atan2(aimag(cr(il)),real(cr(il)))
     modul = (ival-if)*al / real(il-if)
  else
     phase = 0
     modul = 0
  endif
  cfillin = cmplx ( modul*cos(phase), modul*sin(phase) )
end function cfillin
