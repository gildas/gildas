subroutine ignore(line,error)
  use gkernel_interfaces
  use gkernel_types
  use class_parameter
  use class_index
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! CLASS support routine for command
  !
  ! IGNORE [List_of_Observations]
  ! 1      [/SCAN]
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Command line
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  integer(kind=4), parameter :: mlist=15
  type(sic_listi8_t) :: list
  integer(kind=4) :: i,ic,iv,if
  integer(kind=obsnum_length) :: iscan,rr
  !
  if (sic_present(1,0)) then
     call sic_i8(line,1,1,iscan,.true.,error)
     if (error) return
     do if=1,ix%next-1
        if (ix%scan(if).eq.iscan) then
           ix%qual(if) = qual_deleted
        endif
     enddo
  else
     ic = index(line,' ')+1
     iv = lenc(line)
     if (iv.lt.ic) return
     call sic_parse_listi8('IGNORE',line(ic:iv),list,mlist,error)
     if (error) return
     !
     do i=1,list%nlist
        do rr=list%i1(i),list%i2(i),list%i3(i)
           error = .false.
           do if=1,ix%next-1
              if (rr.eq.ix%num(if)) then
                 ix%qual(if) = qual_deleted
                 if (ix%ver(if).gt.0) exit
              endif
           enddo
        enddo
     enddo
  endif
  !
end subroutine ignore
!
subroutine tagout(line,r,error)
  use gbl_message
  use gkernel_interfaces
  use gkernel_types
  use classic_api
  use classcore_interfaces, except_this=>tagout
  use class_parameter
  use class_index
  use class_data
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! CLASS Support routine for command
  !   TAG Quality List_of_scans
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: line   ! Command line
  type(observation), intent(inout) :: r      !
  logical,           intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='TAG'
  type(indx_t) :: ind
  integer(kind=4), parameter :: mlist=15
  type(sic_listi8_t) :: list
  integer(kind=4) :: i,ic,iv,nqual
  integer(kind=entry_length) :: if
  integer(kind=obsnum_length) :: rr
  character(len=message_length) :: mess
  !
  ! Quality
  call sic_i4 (line,0,1,nqual,.true.,error)
  if (error) return
  if (nqual.lt.0 .or. nqual.gt.9) then
     call class_message(seve%e,rname,'Quality out of range 0-9')
     error = .true.
     return
  endif
  !
  if (.not.sic_present(0,2)) then
    ! Target is R buffer
    r%head%gen%qual = nqual
    return
  endif
  !
  ! Target is output file
  if (.not.fileout_opened(rname,error))  return
  !
  ic = index(line,' ')+1
  ic = index(line(ic:),' ')+ic
  iv = lenc(line)
  call sic_parse_listi8(rname,line(ic:iv),list,mlist,error)
  if (error) return
  !
  do if=1,ox%next-1
    do i=1,list%nlist
      do rr=list%i1(i),list%i2(i),list%i3(i)
        error = .false.
        if (rr.eq.ox%num(if)) then
          call rox(if,ind,error)
          if (error) goto 100
          ind%qual = nqual
          call mox(if,ind,error)
          if (error) goto 100
        endif
      enddo
    enddo
  enddo
  return
  !
100 continue
  write(mess,'(A,I0)') 'Error while updating observation #',if
  call class_message(seve%e,rname,mess)
end subroutine tagout
