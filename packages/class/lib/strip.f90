subroutine class_strip(set,line,error,user_function)
  use gildas_def
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_strip
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! CLASS support routine for command
  !   ANA\STRIP FileName
  ! Redirection routine
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  character(len=*),    intent(in)    :: line           !
  logical,             intent(inout) :: error          !
  logical,             external      :: user_function  !
  ! Local
  character(len=*), parameter :: rname='STRIP'
  character(len=filename_length) :: name
  integer(kind=4) :: nc
  !
  ! Find file name
  call sic_ch (line,0,1,name,nc,.true.,error)
  if (error) return
  !
  call strip_map(set,name,error,user_function)
  !
end subroutine class_strip
!
subroutine strip_map(set,name,error,user_function)
  use gildas_def
  use image_def
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>strip_map
  use class_index
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS Support for command
  !   ANA\STRIP FileName
  ! Generic version. Make a strip map, i.e. a Velocity-Position plot.
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  character(len=*),    intent(in)    :: name           ! File name
  logical,             intent(inout) :: error          !
  logical,             external      :: user_function  !
  ! Local
  character(len=*), parameter :: rname='STRIP'
  real(kind=4), allocatable :: cx_off(:)
  integer :: ier
  type (gildas) :: str
  logical :: beta
  character(len=message_length) :: mess
  integer(kind=entry_length) :: nent
  real(kind=4) :: offmin,offmax,dis
  !
  if (.not.filein_opened(rname,error))  return
  !
  ! Consistency tests could be done here instead of in the data loop,
  ! namely:
  !  kind consistency
  !  regular axis consistency
  !  position consistency
  !  spectroscopy/continuum consistency
  !
  nent = cx%next-1
  allocate(cx_off(nent),stat=ier)
  if (failed_allocate(rname,'cx_off',ier,error))  return
  !
  call strip_map_offsets(set,cx_off,offmin,offmax,dis,beta,error)
  if (error)  goto 99
  !
  ! Fill the GDF image header
  call strip_map_header(set,str,name,offmin,offmax,dis,beta,user_function,error)
  if (error)  goto 99
  !
  ! Initialize data
  allocate(str%r2d(str%gil%dim(1),str%gil%dim(2)),stat=ier)
  if (failed_allocate(rname,'r2d',ier,error))  goto 99
  !
  ! Fill the GDF image data
  if (set%kind.eq.kind_spec) then
    call strip_map_data_spec(set,str,cx_off,offmin,dis,error,user_function)
  elseif (set%kind.eq.kind_cont) then
    call class_message(seve%w,rname,'Not yet tested for continuum')
    call strip_map_data_cont(set,str,cx_off,offmin,dis,error,user_function)
  else
    call class_message(seve%e,rname,'Unsupported kind of data')
    error = .true.
    return
  endif
  if (error)  goto 99
  !
  call gdf_write_image(str,str%r2d,error)
  if (error) then
     call class_message(seve%e,rname,'Cannot create output image')
     goto 99
  endif
  write(mess,'(A,I0,A,I0,A)')  &
    'Created a cube of ',str%gil%dim(1),' by ',str%gil%dim(2),' pixels'
  call class_message(seve%i,rname,mess)
  !
99 continue
  if (associated(str%r2d))  deallocate(str%r2d)
  if (allocated(cx_off))    deallocate(cx_off)
  !
end subroutine strip_map
!
subroutine strip_map_offsets(set,cx_off,offmin,offmax,dis,beta,error)
  use gbl_message
  use classcore_interfaces, except_this=>strip_map_offsets
  use class_index
  use class_setup_new
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Store offsets, check if they define a slice
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set        !
  real(kind=4),        intent(out)   :: cx_off(:)  ! Offset array
  real(kind=4),        intent(out)   :: offmin     !
  real(kind=4),        intent(out)   :: offmax     !
  real(kind=4),        intent(out)   :: dis        !
  logical,             intent(out)   :: beta       ! Beta fixed or not?
  logical,             intent(inout) :: error      !
  ! Local
  character(len=*), parameter :: rname='STRIP'
  real(kind=4) :: offx,offy,off,aval
  integer(kind=entry_length) :: nent,ient,jent
  character(len=20) :: coffmin,coffmax,cdis,cdirection
  character(len=message_length) :: mess
  logical :: xvaries,yvaries
  !
  nent = cx%next-1
  if (nent.lt.2) then
    call class_message(seve%e,rname,'At least 2 positions are needed')
    error = .true.
    return
  endif
  !
  offx = cx%off1(1)
  offy = cx%off2(1)
  ! Note that FIND /OFFSET searches in OffVal +- tolerance, i.e. in a
  ! 2*tolerance range
  xvaries = any(abs(offx-cx%off1(2:nent)).gt.2.*set%tole)
  yvaries = any(abs(offy-cx%off2(2:nent)).gt.2.*set%tole)
  if (xvaries .and. yvaries) then
    call class_message(seve%e,rname,  &
      'Index varies in both directions (does not define a strip)')
    error = .true.
    return
  elseif ((.not.xvaries) .and. (.not.yvaries)) then
    call class_message(seve%e,rname,  &
      'Index does not vary in any direction (SET MATCH too large?)')
    error = .true.
    return
  elseif (xvaries) then
    ! Lambda varies, beta fixed
    beta = .true.
    cx_off(:) = cx%off1(1:nent)
    dis = abs(offx-cx%off1(2))
  else
    ! Lambda fixed, beta varies
    beta = .false.
    cx_off(:) = cx%off2(1:nent)
    dis = abs(offy-cx%off2(2))
  endif
  offmin = minval(cx_off(:))
  offmax = maxval(cx_off(:))
  !
  do ient=1,nent
    off = cx_off(ient)
    do jent=ient+1,nent
      aval = abs(off-cx_off(jent))
      if (aval.gt.set%tole) dis = min(dis,aval)
    enddo
  enddo
  !
  ! Display
  call offsec(set,offmin,coffmin)
  call offsec(set,offmax,coffmax)
  call offsec(set,dis,cdis)
  if (beta) then
    cdirection = 'Lambda'
  else
    cdirection = 'Beta'
  endif
  write(mess,'(10A)')  'Offsets from ',trim(coffmin),' to ',trim(coffmax),  &
                       ' by ',trim(cdis),' ',class_setup_get_angle(),' along ',cdirection
  call class_message(seve%i,rname,mess)
  !
end subroutine strip_map_offsets
!
subroutine strip_map_header(set,str,name,offmin,offmax,dis,beta,user_function,error)
  use image_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>strip_map_header
  use class_index
  use class_parameter
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Define the GDF image header
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  type(gildas),        intent(inout) :: str            ! GDF image
  character(len=*),    intent(in)    :: name           ! File name
  real(kind=4),        intent(in)    :: offmin         ! Minimal offset
  real(kind=4),        intent(in)    :: offmax         ! Maximal offset
  real(kind=4),        intent(in)    :: dis            !
  logical,             intent(in)    :: beta           !
  logical,             external      :: user_function  !
  logical,             intent(inout) :: error          ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='STRIP'
  logical :: kind_is_spec
  integer(kind=entry_length) :: kx
  integer(kind=4) :: nx,ny,ltypem
  real(kind=8) :: ref
  real(kind=4) :: rbad,xxx,yyy
  type(observation) :: obs
  !
  ! Use 1st observation in Current Index as reference
  call init_obs(obs)
  kx = cx%ind(1)
  call get_it(set,obs,kx,user_function,error)
  if (error)  goto 100
  kind_is_spec = obs%head%gen%kind.eq.kind_spec
  !
  ! Basic checks
  if (.not.obs%head%presec(class_sec_gen_id)) then
    call class_message(seve%e,rname,'Missing section General')
    error = .true.
  endif
  if (.not.obs%head%presec(class_sec_pos_id)) then
    call class_message(seve%e,rname,'Missing section Position')
    error = .true.
  endif
  if (kind_is_spec) then
    if (.not.obs%head%presec(class_sec_spe_id)) then
      call class_message(seve%e,rname,'Missing section Spectroscopy')
      error = .true.
    endif
  else
    if (.not.obs%head%presec(class_sec_poi_id)) then
      call class_message(seve%e,rname,'Missing section Continuum')
      error = .true.
    endif
  endif
  if (obs%head%presec(class_sec_xcoo_id)) then
    call class_message(seve%e,rname,'Irregularly sampled data not supported')
    error = .true.
  endif
  if (error)  goto 100
  !
  call abscissa(set,obs,error)  ! Compute obs%datax
  if (error)  goto 100
  !
  ! Fill the header
  nx = nint((offmax-offmin)/dis)+1
  if (kind_is_spec) then
    ny = obs%head%spe%nchan
    ref = obs%head%spe%rchan
    rbad = obs%head%spe%bad
  else
    ny = obs%head%dri%npoin
    ref = obs%head%dri%rpoin
    rbad = obs%head%dri%bad
  endif
  !
  ! Create Image
  call gildas_null(str)
  str%gil%ndim = 2
  str%gil%dim(:) = 1
  !
  str%gil%dim(1) = ny
  str%gil%ref(1) = ref
  call abscissa_chan2any(set,obs,ref,str%gil%val(1))
  str%gil%inc(1) = obs%datax(2)-obs%datax(1)
  !
  str%gil%dim(2) = nx
  str%gil%ref(2) = 1.0
  str%gil%val(2) = offmin
  str%gil%inc(2) = dis
  !
  str%gil%dim(3) = 1    ! Single plane
  str%gil%ref(3) = 1.0  !
  str%gil%inc(3) = 0.0  ! No variation
  if (beta) then  ! Beta fixed, Lambda varies
    str%gil%val(3) = obs%head%pos%betof
  else            ! Lambda fixed, Beta varies
    str%gil%val(3) = obs%head%pos%lamof
  endif
  !
  ! Axis types
  if (set%unitx(1).eq.'V') then
    str%char%code(1) = 'VELOCITY'
  elseif (set%unitx(1).eq.'I') then
    str%char%code(1) = 'FREQUENCY'
  elseif (set%unitx(1).eq.'F') then
    str%char%code(1) = 'FREQUENCY'
  elseif (set%unitx(1).eq.'C') then
    str%char%code(1) = 'CHANNELS'
  else
    call class_message(seve%e,rname,'Unsupported X axis unit')
    error = .true.
    goto 100
  endif
  !
  ! Projection information
  if (beta) then
    if (kind_is_spec) then
      str%gil%xaxi = 2
      str%gil%yaxi = 3
    else
      str%gil%xaxi = 2
      str%gil%yaxi = 1
    endif
  else
    if (kind_is_spec) then
      str%gil%xaxi = 3
      str%gil%yaxi = 2
    else
      str%gil%xaxi = 1
      str%gil%yaxi = 2
    endif
  endif
  ltypem = obs%head%pos%system
  if (ltypem.eq.type_eq) then
    str%char%code(str%gil%xaxi) = 'RA'
    str%char%code(str%gil%yaxi) = 'DEC'
    str%gil%epoc = obs%head%pos%equinox
    str%gil%ra = obs%head%pos%lam
    str%gil%dec = obs%head%pos%bet
    call equ_to_gal(str%gil%ra, str%gil%dec,0.0,0.0,str%gil%epoc,  &
                    str%gil%lii,str%gil%bii,xxx,yyy,error)
    if (error)  goto 100
    str%gil%a0 = str%gil%ra
    str%gil%d0 = str%gil%dec
    str%char%syst = 'EQUATORIAL'
    !
  elseif (ltypem.eq.type_ga) then
    str%char%code(str%gil%xaxi) = 'LII'
    str%char%code(str%gil%yaxi) = 'BII'
    str%gil%lii = obs%head%pos%lam
    str%gil%bii = obs%head%pos%bet
    str%gil%epoc = set%equinox
    call gal_to_equ(str%gil%lii,str%gil%bii,0.0,0.0,  &
                    str%gil%ra, str%gil%dec,xxx,yyy,str%gil%epoc,error)
    if (error)  goto 100
    str%gil%a0 = str%gil%lii
    str%gil%d0 = str%gil%bii
    str%char%syst = 'GALACTIC'
    !
  elseif (ltypem.eq.type_ic) then
    str%char%code(str%gil%xaxi) = 'RA'
    str%char%code(str%gil%yaxi) = 'DEC'
    str%gil%epoc = equinox_null
    str%gil%ra = obs%head%pos%lam
    str%gil%dec = obs%head%pos%bet
    str%gil%lii = 0.d0
    str%gil%bii = 0.d0
    str%gil%a0 = str%gil%ra
    str%gil%d0 = str%gil%dec
    str%char%syst = 'ICRS'
    !
  else
    if (kind_is_spec) then
      call class_message(seve%e,rname,'Unsupported type of coordinate')
      error = .true.
      goto 100
    endif
    str%char%code(str%gil%xaxi) = 'Lambda'
    str%char%code(str%gil%yaxi) = 'Beta'
    str%gil%lii = obs%head%pos%lam
    str%gil%bii = obs%head%pos%bet
    str%gil%epoc = set%equinox
    call gal_to_equ(str%gil%lii,str%gil%bii,0.0,0.0,   &
                    str%gil%ra,str%gil%dec,xxx,yyy,str%gil%epoc,error)
    if (error)  goto 100
    str%gil%a0 = str%gil%lii
    str%gil%d0 = str%gil%bii
    str%char%syst = 'UNKNOWN'
  endif
  !
  str%gil%bval = rbad
  str%gil%eval = 0.0
  str%char%name = obs%head%pos%sourc
  !
  ! Projection
  str%gil%ptyp = obs%head%pos%proj
  str%gil%pang = obs%head%pos%projang
  !
  if (obs%head%gen%kind.eq.kind_spec) then
    ! Spectroscopy
    str%gil%spec_words = 12
    str%char%line = obs%head%spe%line
    str%gil%freq = obs%head%spe%restf
    str%gil%voff = obs%head%spe%voff
    str%gil%vres = obs%head%spe%vres
    str%gil%fima = obs%head%spe%image
    str%gil%fres = obs%head%spe%fres
    str%gil%faxi = 1
    str%gil%dopp = obs%head%spe%doppler
    str%gil%vtyp = obs%head%spe%vtype
  else
    str%gil%spec_words = 0
  endif
  !
  ! Define section length and write
  str%gil%coor_words = 6*gdf_maxdims
  str%gil%blan_words = 2
  str%gil%extr_words = 0
  str%gil%desc_words = 18
  str%gil%posi_words = 12
  str%gil%proj_words = 9
  !
  call sic_parse_file(name,' ','.gdf',str%file)
  !
100 continue
  call free_obs(obs)
  !
end subroutine strip_map_header
!
subroutine strip_map_data_spec(set,str,cx_off,offmin,dis,error,user_function)
  use gbl_constant
  use phys_const
  use image_def
  use gbl_message
  use classcore_interfaces, except_this=>strip_map_data_spec
  use class_index
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Fill the GDF image data (for spectroscopic observations)
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  type(gildas),        intent(inout) :: str            ! The GDF image
  real(kind=4),        intent(in)    :: cx_off(:)      ! The offsets
  real(kind=4),        intent(in)    :: offmin         !
  real(kind=4),        intent(in)    :: dis            !
  logical,             intent(inout) :: error          ! Logical error flag
  logical,             external      :: user_function  !
  ! Local
  character(len=*), parameter :: rname='STRIP'
  logical :: readsec(-mx_sec:0)  ! Which section are to be read
  type(observation) :: obs
  integer(kind=entry_length) :: ient,nent
  integer(kind=4) :: ny,yrow
  real(kind=8) :: ref1
  real(kind=4) :: br,bt
  character(len=message_length) :: mess
  !
  str%r2d(:,:) = str%gil%bval  ! Default
  !
  call init_obs(obs)
  readsec(:) = .false.  ! Read no section except...
  readsec(class_sec_gen_id) = .true.  ! General
  readsec(class_sec_spe_id) = .true.  ! Spectro
  nent = cx%next-1
  do ient=1,nent
    !
    ! Read header and data sections
    call rheader(set,obs,cx%ind(ient),user_function,error,readsec)
    if (error)  goto 97
    !
    ! This should be done with the CONSISTENCY mechanism
    if (obs%head%gen%kind.ne.kind_spec) then
      call class_message(seve%e,rname,'Continuum data not supported')
      error =.true.
      goto 97
    elseif (obs%head%presec(class_sec_xcoo_id)) then
      call class_message(seve%e,rname,'Irregularly sampled data not supported')
      error = .true.
      goto 97
    endif
    !
    ! Check spectroscopic information is self consistent
    ! This should be done with the CONSISTENCY mechanism
    ref1 = str%gil%convert(1,1)
    bt = str%gil%voff - str%gil%vres*ref1 + &
         (obs%head%spe%restf-str%gil%freq)/obs%head%spe%restf*clight_kms
    br = obs%head%spe%voff - obs%head%spe%vres*obs%head%spe%rchan
    if (abs(obs%head%spe%rchan-ref1).gt.1d-2) then
      write (mess,1001) obs%head%spe%rchan,ref1
      mess = 'Different reference channels'//mess
      call class_message(seve%w,rname,mess)
    endif
    if (obs%head%spe%fres.ne.str%gil%fres) then
      write(mess,1001) obs%head%spe%fres,str%gil%fres
      mess = 'Different resolutions'//mess
      call class_message(seve%w,rname,mess)
    endif
    if (abs(bt-br).gt.abs(obs%head%spe%vres)*1e-2) then  ! SG 22-Apr-1986
      call class_message(seve%w,rname,'Spectra not aligned in sky frequency')
      write(6,*) br,bt
    endif
    !
    ! Read data
    call reallocate_obs(obs,obs%head%spe%nchan,error)
    if (error)  goto 97
    call rdata(set,obs,obs%head%spe%nchan,obs%data1,error)
    if (error)  goto 97
    !
    ! now write all channels
    yrow = nint((cx_off(ient)-offmin)/dis) + 1
    ny = min(str%gil%dim(1),obs%head%spe%nchan)
    where (obs%data1(1:ny).ne.obs%head%spe%bad)
      str%r2d(1:ny,yrow) = obs%data1(1:ny)
    end where
  enddo
  !
97 continue
  call free_obs(obs)
  !
1001 format(' R:',1pg12.5,' T:',1pg12.5)
end subroutine strip_map_data_spec
!
subroutine strip_map_data_cont(set,str,cx_off,offmin,dis,error,user_function)
  use gbl_constant
  use phys_const
  use image_def
  use gbl_message
  use classcore_interfaces, except_this=>strip_map_data_cont
  use class_index
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Fill the GDF image data (for continuum observations)
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  type(gildas),        intent(inout) :: str            ! The GDF image
  real(kind=4),        intent(in)    :: cx_off(:)      ! The offsets
  real(kind=4),        intent(in)    :: offmin         !
  real(kind=4),        intent(in)    :: dis            !
  logical,             intent(inout) :: error          ! Logical error flag
  logical,             external      :: user_function  !
  ! Local
  character(len=*), parameter :: rname='STRIP'
  logical :: readsec(-mx_sec:0)  ! Which section are to be read
  type(observation) :: obs
  integer(kind=entry_length) :: ient,nent
  integer(kind=4) :: ny,yrow
  !
  str%r2d(:,:) = str%gil%bval  ! Default
  !
  call init_obs(obs)
  readsec(:) = .false.  ! Read no section except...
  readsec(class_sec_gen_id) = .true.  ! General
  readsec(class_sec_poi_id) = .true.  ! Continuum
  nent = cx%next-1
  do ient=1,nent
    !
    ! Read header and data sections
    call rheader(set,obs,cx%ind(ient),user_function,error,readsec)
    if (error)  goto 97
    !
    ! This should be done with the CONSISTENCY mechanism
    if (obs%head%gen%kind.ne.kind_cont) then
      call class_message(seve%e,rname,'Only continuum data supported')
      error =.true.
      goto 97
    elseif (obs%head%presec(class_sec_xcoo_id)) then
      call class_message(seve%e,rname,'Irregularly sampled data not supported')
      error = .true.
      goto 97
    endif
    !
    ! Read data
    call reallocate_obs(obs,obs%head%dri%npoin,error)
    if (error)  goto 97
    call rdata(set,obs,obs%head%dri%npoin,obs%data1,error)
    if (error)  goto 97
    !
    ! now write all channels
    yrow = nint((cx_off(ient)-offmin)/dis) + 1
    ny = min(str%gil%dim(1),obs%head%dri%npoin)
    where (obs%data1(1:ny).ne.obs%head%dri%bad)
      str%r2d(1:ny,yrow) = obs%data1(1:ny)
    end where
  enddo
  !
97 continue
  call free_obs(obs)
  !
end subroutine strip_map_data_cont
