subroutine classcore_filein_open(spec,nspec,error)
  use gildas_def
  use gbl_format
  use gbl_convert
  use gbl_message
  use gkernel_types
  use classic_api
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>classcore_filein_open
  use class_common
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  !   Close any input file, set the file specification for input, open
  ! this file.
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: spec   ! File name including extension
  integer(kind=4),  intent(in)  :: nspec  ! Length of filename
  logical,          intent(out) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='INPUT'
  integer(kind=4), parameter :: max_record=2147483647
  integer(kind=4) :: olun,onspec,flun,filekind
  integer(kind=entry_length) :: i,nentry
  character(len=filename_length) :: ospec,name
  logical :: fexist,fopened,error2,doopen
  type(time_t) :: time
  !
  error = .false.
  !
  ! Save current unit to help error recovery
  olun = filein%lun
  ospec = filein%spec
  onspec = filein%nspec
  !
  inquire(file=spec,exist=fexist,opened=fopened,number=flun)
  !
  if (fexist .and. fopened .and. flun.eq.fileout%lun) then
     ! The same file is already opened on unit OLUN (NB: do not test filenames
     ! equality, the test above is more portable i.e. support non-
     ! case-sensitive file systems, hard/symbolic links, etc).
     call classcore_filein_close(error)  ! Close previous Input File
     if (error)  goto 19
     doopen = .false.
     filein%lun = fileout%lun
     !
     if (fileout%desc%single) then
        if (fileout%update) then
           call class_message(seve%w,rname,  &
           'File of type "single" opened for UPDATE only')
        else
           call class_message(seve%w,rname,  &
           'File of type "single" can not be both input and output')
           call class_message(seve%w,rname,  &
           'File opened for input but closed for output')
           fileout%lun = 0
           fileout%spec = ''
           fileout%nspec = 1
           ox%next = 1
        endif
     endif
  else
     doopen = .true.
     if (filein_is_fileout()) then
        ! A file is already opened for Input AND Output on unit OLUN/ILUN
        ! Allocate the other available unit and open it
        if (fileout%lun.eq.lun_1) then
           filein%lun = lun_2
        else
           filein%lun = lun_1
        endif
     else
        ! A file may have been opened for Input only on unit ILUN
        ! Close the Input unit or allocate it, and open it
        call classcore_filein_close(error)
        if (error)  goto 19
        if (fileout%lun.ne.lun_1) then
          filein%lun = lun_1
        else
          filein%lun = lun_2
        endif
     endif
  endif
  filein%spec = spec
  filein%nspec = nspec
  filein%update = .false.  ! Never used for the input file
  !
  if (doopen) then
    call classic_file_open(filein,.false.,error)
    if (error)  goto 19
  endif
  !
  call gag_file_guess(rname,filein%lun,filekind,error)
  if (error)  goto 19
  select case (filekind)
  case (1)
    ! GDF file
    call classcore_filein_close(error)
    if (error)  goto 19
    ! NB: File extension was resolved earlier, using SET EXTENSION
    call class_file_read_gdfhead(rname,spec,filein_vlmhead,.true.,error)
    if (error)  goto 19
    filein_isvlm = .true.
    nentry = filein_vlmhead%gil%dim(2)*filein_vlmhead%gil%dim(3)
  case (2)
    ! FITS file
    call class_message(seve%e,rname,'FITS files can not be opened directly in Class')
    call class_message(seve%e,rname,'See HELP V\FITS and HELP LAS\FITS for help')
    error = .true.
    goto 19
  case (0)
    ! Not a GDF file nor FITS: try Class file
    call classic_filedesc_open(filein,error)
    if (error)  goto 19
    filein_isvlm = .false.
    call class_file_check_classic(rname,filein,error)
    if (error)  goto 19
    ! (Re)allocate the filein working buffers
    call reallocate_recordbuf(ibufbi, filein%desc%reclen,error)
    call reallocate_recordbuf(ibufobs,filein%desc%reclen,error)
    if (error) then
      call class_message(seve%e,rname,'Error allocating file buffers')
      return
    endif
    call classic_recordbuf_nullify(ibufbi)
    nentry = filein%desc%xnext-1
  end select
  !
  ! Resize Input File Index
  call reallocate_optimize(ix,nentry,.true.,.false.,error)
  if (error)  return
  !
  ! read the index : Read all blocks even first one
  if (nentry.gt.0) then
    call gtime_init(time,nentry,error)
    if (error)  return
    do i = 1,nentry
      call gtime_current(time)
      call rix_to_ix(i,error)  ! Fill in IX% structure
      if (error)  return
    enddo
  else
    call class_message(seve%w,rname,'Empty index')
  endif
  ix%next = nentry+1  ! For symetry with CX%NEXT
  !
  ! Reset the current index
  cx%next = 1
  knext   = 0
  nindex  = 0
  !
  ! OK Acknowledge the operation
  if (filein_isvlm) then
    name = filein_vlmhead%file
  else
    inquire (unit=filein%lun,name=name)
  endif
  call class_message(seve%i,rname,trim(name)//' successfully opened')
  return
  !
  ! Try to recover from the error
19 continue
  ! error = .true.  ! Already done, keep the error flag on
  error2 = .false.  ! Use another error flag
  call classcore_filein_close(error2)
  if (error2)  goto 20
  filein%lun = olun
  filein%spec = ospec
  filein%nspec = onspec
  if (filein%spec.eq.' ' .or. olun.eq.0)  return
  !
  ! Reopen the file if necessary
  if (.not.filein_is_fileout()) then
    call classic_file_open(filein,.false.,error2)
    if (error2)  goto 20
  endif
  !
  call classic_filedesc_open(filein,error2)
  if (error2) goto 20  ! Error during error recovery...
  !
  ! No need to reallocate the buffers, they have not changed
  call classic_recordbuf_nullify(ibufbi)
  !
  call class_message(seve%i,rname,trim(filein%spec)//' is reopened')
  return
  !
20 call class_message(seve%w,rname,'No input file opened')
  filein%lun = 0
end subroutine classcore_filein_open
!
subroutine classcore_fileout_open(set,spec,lnew,lover,lsize,lsingle,error)
  use classic_api
  use classcore_interfaces, except_this=>classcore_fileout_open
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  !  Public entry point to open a new or reopen an old output file.
  !---------------------------------------------------------------------
  type(class_setup_t),        intent(in)    :: set      !
  character(len=*),           intent(in)    :: spec     ! File name including extension
  logical,                    intent(in)    :: lnew     ! Should it be a new file or an old?
  logical,                    intent(in)    :: lover    ! If new, overwrite existing file or raise an error?
  integer(kind=entry_length), intent(in)    :: lsize    ! If new, maximum number of observations allowed
  logical,                    intent(in)    :: lsingle  ! If new, should spectra be unique?
  logical,                    intent(inout) :: error    ! Logical error flag
  ! Local
  integer(kind=4) :: nspec
  !
  nspec = len_trim(spec)
  if (lnew) then
    call classcore_fileout_new(set,spec,nspec,lsize,lsingle,lover,error)
  else
    call classcore_fileout_old(set,spec,nspec,.false.,error)
  endif
  if (error)  return
  !
end subroutine classcore_fileout_open
!
subroutine classcore_fileout_old(set,spec,nspec,update,error)
  use gildas_def
  use gbl_message
  use gbl_format
  use gbl_convert
  use gkernel_types
  use classic_api
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>classcore_fileout_old
  use class_common
  use class_index
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  !  Close any output file, set the file specifications for output,
  !  open this file.
  !  (also selected for input if infile not defined)
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)  :: set     !
  character(len=*),    intent(in)  :: spec    ! File name including extension
  integer(kind=4),     intent(in)  :: nspec   ! Length of SPEC
  logical,             intent(in)  :: update  ! Open it in update mode?
  logical,             intent(out) :: error   ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='OUTPUT'
  integer(kind=4), parameter ::  max_record=2147483647
  integer(kind=4) :: olun,onspec,flun
  integer(kind=entry_length) :: i,nentry
  character(len=filename_length) :: ospec,name
  logical :: fexist,fopened,error2,oupdate
  type(time_t) :: time
  !
  ! Save current unit to help error recovery
  olun = fileout%lun
  ospec = fileout%spec
  onspec = fileout%nspec
  oupdate = fileout%update
  !
  inquire(file=spec,exist=fexist,opened=fopened,number=flun)
  !
  if (fexist .and. fopened .and. flun.eq.filein%lun) then
     ! The same file is already opened on unit ILUN (NB: do not test filenames
     ! equality, the test above is more portable i.e. support non-
     ! case-sensitive file systems, hard/symbolic links, etc).
     call classcore_fileout_close(error)  ! Close previous output file
     if (error)  goto 29
     fileout%lun = filein%lun
     !
     if (filein%desc%single) then
        ! Forbid opening same output as input if of 'single' type
        if (update) then
           call class_message(seve%w,rname,  &
           'File of type "single" opened for UPDATE only')
        else
           call class_message(seve%w,rname,  &
           'File of type "single" can not be both input and output')
           call class_message(seve%w,rname,  &
           'File opened for output but closed for input')
           filein%lun = 0
           filein%spec = ''
           filein%nspec = 1
           ix%next = 1
           ! Reset the current index
           cx%next = 1
           knext   = 0
           nindex  = 0
        endif
     endif
  elseif (filein_is_fileout()) then
     ! A file is already opened for Input and Output on unit ILUN
     ! Allocate the other available unit and open it
     if (fileout%lun.eq.lun_1) then
        fileout%lun = lun_2
     else
        fileout%lun = lun_1
     endif
  else
     ! A file may have been opened for Output only on unit OLUN
     ! Close the Output unit or allocate it, and open it.
     if (fileout%lun.eq.0) then
        if (filein%lun.ne.lun_2) then
           fileout%lun = lun_2
        else
           fileout%lun = lun_1
        endif
     endif
  endif
  fileout%spec = spec(1:nspec)
  fileout%nspec = nspec
  fileout%update = update
  !
  ! In any case, close the output file and reopen it with write access
  call classic_file_close(fileout,error)
  if (error)  goto 29
  call classic_file_open(fileout,.true.,error)
  if (error)  goto 29
  !
  call classic_filedesc_open(fileout,error)
  if (error)  goto 29
  !
  call class_file_check_classic(rname,fileout,error)
  if (error)  goto 29
  fileout_isvlm = .false.
  !
  ! (Re)allocate the fileout working buffers
  call reallocate_recordbuf(obufbi, fileout%desc%reclen,error)
  call reallocate_recordbuf(obufobs,fileout%desc%reclen,error)
  if (error) then
    call class_message(seve%e,rname,'Error allocating file buffers')
    return
  endif
  !
  ! Resize Output File Index
  nentry = fileout%desc%xnext-1
  call reallocate_optimize(ox,nentry,.false.,.false.,error)
  if (error)  return
  !
  ! Read the index
  call classic_recordbuf_nullify(obufbi)
  if (nentry.gt.0) then
    call gtime_init(time,nentry,error)
    if (error)  return
    do i = 1,nentry
      call gtime_current(time)
      call rox_to_ox(i,error)
      if (error) return
    enddo
  else
    call class_message(seve%w,rname,'Empty index')
  endif
  ox%next = fileout%desc%xnext  ! For symetry with CX%NEXT
  call ox_sort_reset(set,error)
  if (error)  return
  !
  inquire (unit=fileout%lun,name=name)
  call class_message(seve%i,rname,trim(name)//' successfully opened')
  !
  ! zzz see comments in COX
  call classic_recordbuf_nullify(obufbi)
  return
  !
  ! Try to recover from the error
29 continue
  ! error = .true.  ! Already done, keep the error flag on
  error2 = .false.  ! Use another error flag
  if (.not.filein_is_fileout()) then
    call classcore_fileout_close(error2)
    if (error2)  goto 30
  elseif (filein%lun.ne.0 .and. filein%spec.ne.' ') then
    ! IN == OUT: Close the File and reopen it in Read-Only mode
    call classcore_fileout_close(error2)  ! Was missing ???
    if (error2)  goto 30
    call classic_file_open(filein,.false.,error2)
    if (error2)  goto 30
  endif
  !
  fileout%lun = olun
  fileout%spec = ospec
  fileout%nspec = onspec
  fileout%update = oupdate
  if (fileout%spec.eq.' ' .or. olun.eq.0)  return
  !
  call classic_file_open(fileout,.true.,error2)
  if (error2)  goto 30
  call classic_filedesc_open(fileout,error2)
  if (error2)  goto 30  ! Error during error recovery...
  !
  ! No need to reallocate the buffers, they have not changed
  !
  call class_message(seve%i,rname,trim(fileout%spec)//' is reopened')
  return
  !
30 call class_message(seve%w,rname,'No output file opened')
  fileout%lun = 0
end subroutine classcore_fileout_old
!
subroutine classcore_fileout_new(set,spec,nspec,lsize,lsingle,lover,error)
  use gildas_def
  use gbl_format
  use gbl_convert
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>classcore_fileout_new
  use class_common
  use class_index
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  !  Initializes a new output file
  !  Close any output file, set the file specifications for output
  ! open this file.
  !---------------------------------------------------------------------
  type(class_setup_t),        intent(in)  :: set      !
  character(len=*),           intent(in)  :: spec     ! File name including extension
  integer(kind=4),            intent(in)  :: nspec    ! Length of SPEC
  integer(kind=entry_length), intent(in)  :: lsize    ! Maximum number of observations allowed
  logical,                    intent(in)  :: lsingle  ! Should spectra be unique ?
  logical,                    intent(in)  :: lover    ! Overwrite existing file ?
  logical,                    intent(out) :: error    ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='NEWPUT'
  integer(kind=4) :: olun,onspec,version,reclen,gex,vind,lind,ier,flun
  character(len=filename_length) :: ospec
  logical :: error2,oupdate,fexist,fopened
  character(len=message_length) :: mess
  !
  error = .false.
  !
  ! Save current unit to help error recovery
  olun = fileout%lun
  ospec = fileout%spec
  onspec = fileout%nspec
  oupdate = fileout%update
  !
  inquire(file=spec,exist=fexist,opened=fopened,number=flun)
  !
  if (fexist .and. fopened .and. flun.eq.filein%lun) then
     ! The same file is already opened on unit ILUN (NB: do not test filenames
     ! equality, the test above is more portable i.e. support non-
     ! case-sensitive file systems, hard/symbolic links, etc).
     call class_message(seve%w,rname,'Overwriting current input file')
     call classcore_filein_close(error)  ! Close previous Input File
     if (error)  goto 39
     ix%next = 1
     cx%next = 1
     knext = 0
     nindex = 0
     !
     ! Also close the current output file, if any
     call classcore_fileout_close(error)
     if (error)  goto 39
     !
     ! Use an available lun for the new output file:
     fileout%lun = flun  ! This one was used by filein%lun but is now free
     !
  elseif (filein_is_fileout()) then
     ! Another file is already opened for Input and Output on unit
     ! ILUN/OLUN
     ! Allocate the other available unit and open it
     if (fileout%lun.eq.lun_1) then
        fileout%lun = lun_2
     else
        fileout%lun = lun_1
     endif
  else
     ! A file may have been opened for Output only on unit OLUN.
     ! Close the Output unit or allocate it, and open it.
     if (fileout%lun.eq.0) then
        if (filein%lun.ne.lun_2) then
           fileout%lun = lun_2
        else
           fileout%lun = lun_1
        endif
     endif
  endif
  fileout%spec = spec
  fileout%nspec = nspec
  fileout%update = .false.
  !
  ! In any case, close the output file and reopen it with write access
  call classic_file_close(fileout,error)
  if (error)  goto 39
  ! Remove it if requested by user
  if (lover .and. gag_inquire(spec,nspec).eq.0)  &
    call gag_filrm(fileout%spec(1:fileout%nspec))
  !
  ! Initialize and open the file
  version = 2  ! Default
  ier = sic_getlog('CLASS_FILE_VERSION',version)
  if (version.eq.1) then
    reclen = classic_reclen_v1  ! Default for V1 files
  else
    reclen = 1024  ! [words] Default for V2 files
    ier = sic_getlog('CLASS_FILE_RECORD',reclen)
  endif
  call classic_file_init(fileout,version,reclen,error)
  if (error)  goto 39
  fileout_isvlm = .false.
  !
  ! Initialize and write its File Descriptor
  if (version.eq.1) then
    ! File version 1: use Index version 1 + linear growth
    call classic_filedesc_init(fileout,classic_kind_class,lsingle,lsize,  &
      vind_v1,lind_v1,1.0,error)
  else
    ! File version 2: custom Index version + custom growth
    vind = 2  ! Default index version for file version 2
    ier = sic_getlog('CLASS_INDEX_VERSION',vind)
    if (vind.eq.2) then
      lind = lind_v2
    elseif (vind.eq.3) then
      lind = lind_v3
    else
      write(mess,'(A,I0,A)')  &
        'Unsupported index version ',vind,' for file version 2'
      call class_message(seve%e,rname,mess)
      error = .true.
      goto 39
    endif
    gex = 20  ! Default extension growth is exponential
    ier = sic_getlog('CLASS_FILE_GROWTH',gex)
    call classic_filedesc_init(fileout,classic_kind_class,lsingle,lsize,  &
      vind,lind,gex/10.,error)
  endif
  if (error)  goto 39
  !
  call class_message(seve%i,rname,trim(fileout%spec)//' initialized')
  !
  ! (Re)allocate the fileout working buffers
  call reallocate_recordbuf(obufbi, fileout%desc%reclen,error)
  call reallocate_recordbuf(obufobs,fileout%desc%reclen,error)
  if (error) then
    call class_message(seve%e,rname,'Error allocating file buffers')
    return
  endif
  !
  ! Allocate the Output File Index
  call reallocate_optimize(ox,lsize,.false.,.false.,error)
  if (error)  return
  !
  obufbi%data(:) = 0
  call classic_recordbuf_nullify(obufbi)
  ox%next = fileout%desc%xnext  ! For symetry with CX%NEXT, should be 1 here
  call ox_sort_reset(set,error)
  if (error)  return
  return
  !
  ! Try to recover from the error
39 continue
  ! error = .true.  ! Already done, keep the error flag on
  error2 = .false.  ! Use another error flag
  ! File was new: it could not have shared its unit with the Input File
  call classcore_fileout_close(error2)
  if (error2)  goto 40
  fileout%lun = olun
  fileout%spec = ospec
  fileout%nspec = onspec
  fileout%update = oupdate
  if (fileout%spec.eq.' ' .or. olun.eq.0)  return
  !
  call classic_file_open(fileout,.true.,error2)
  if (error2)  goto 40
  call classic_filedesc_open(fileout,error2)
  if (error2)  goto 40  ! Error during error recovery...
  !
  ! No need to reallocate the buffers: they have not changed
  !
  call class_message(seve%i,rname,trim(fileout%spec)//' is reopened')
  return
  !
40 call class_message(seve%e,rname,'No output file opened')
  fileout%lun = 0
end subroutine classcore_fileout_new
!
subroutine class_file_check_classic(rname,file,error)
  use gbl_message
  use classic_api
  use classcore_interfaces, except_this=>class_file_check_classic
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Check if the input Classic file is supported
  !---------------------------------------------------------------------
  character(len=*),     intent(in)    :: rname  ! Calling routine name
  type(classic_file_t), intent(in)    :: file   !
  logical,              intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=message_length) :: mess
  !
  ! Check file version
  if (file%desc%version.gt.2) then
    write(mess,'(A,I0,A)')  'Version ',file%desc%version,' files not supported'
    call class_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  ! Check file kind. Accept only: V2+kind_class, or V1+kind_unknown
  if (file%desc%kind.ne.classic_kind_class) then
    if (file%desc%version.eq.1 .and. file%desc%kind.ne.classic_kind_unknown) then
      call class_message(seve%e,rname,trim(file%spec)//' is not a Class file')
      error = .true.
      continue
    endif
  endif
  !
  ! Check index versions
  if (file%desc%vind.gt.vind_v3) then
    write(mess,'(A,I0,A)')  'Index version ',file%desc%vind,' not supported'
    call class_message(seve%e,rname,mess)
    error = .true.
    continue
  endif
  !
end subroutine class_file_check_classic
!
subroutine classcore_fileout_flush(error)
  use gbl_format
  use gbl_message
  use classcore_interfaces, except_this=>classcore_fileout_flush
  use class_common
  !---------------------------------------------------------------------
  ! @ private
  ! Flush the output file to the disk
  !---------------------------------------------------------------------
  logical, intent(inout) :: error
  !
  if (fileout%lun.eq.0)  return
  !
  call classic_file_fflush(fileout,error)
  if (error)  return
  !
end subroutine classcore_fileout_flush
!
subroutine classcore_filein_close(error)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>classcore_filein_close
  use class_common
  !---------------------------------------------------------------------
  ! @ private
  !  Close the input file without closing the output file if they are
  ! the same
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  ! Error status
  !
  if (filein%lun.ne.0) then
    ! FILE IN was a Classic file
    if (filein%lun.ne.fileout%lun) then
      call classic_file_close(filein,error)
      if (error)  continue
    endif
    filein%lun = 0
    !
  elseif (filein_vlmhead%loca%islo.ne.0) then
    ! FILE IN was a GDF file
    call gdf_close_image(filein_vlmhead,error)
  endif
  !
end subroutine classcore_filein_close
!
subroutine classcore_fileout_close(error)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>classcore_fileout_close
  use class_common
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  !  Close the output file without closing the input file if they are
  ! the same
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  ! Error status
  !
  if (fileout%lun.eq.0)  return  ! Nothing to do
  if (fileout%lun.ne.filein%lun) then
    call classic_file_close(fileout,error)
    if (error)  continue
  endif
  fileout%lun = 0
  !
end subroutine classcore_fileout_close
!
subroutine class_files_close(error)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_files_close
  use class_common
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  !  Close both input and/or output files if they are opened, also
  ! taking care if they are identical
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  ! Error status
  !
  if (fileout%lun.ne.0) then
    call classic_file_close(fileout,error)
    if (error)  continue
    if (filein%lun.eq.fileout%lun)  filein%lun = 0
    fileout%lun = 0
  endif
  !
  if (filein%lun.ne.0) then
    ! FILE IN was a Classic file
    call classic_file_close(filein,error)
  elseif (filein_vlmhead%loca%islo.ne.0) then
    ! FILE IN was a GDF file
    call gdf_close_image(filein_vlmhead,error)
  endif
end subroutine class_files_close
!
subroutine class_files_info
  use gildas_def
  use gbl_format
  use gbl_convert
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_files_info
  use class_common
  !---------------------------------------------------------------------
  ! @ private
  !  Prints input and output file names
  !---------------------------------------------------------------------
  ! Local
  character(len=*), parameter :: rname='INFO'
  character(len=filename_length) :: name
  character(len=20) :: aconv
  !
  if (filein%lun.ne.0) then
    call gdf_conversion(filein%conv%code,aconv)
    inquire(unit=filein%lun,name=name)
    call class_message(seve%r,rname,'Input file: '//trim(name)//' '//aconv)
    if (filein%desc%single) then
      call class_message(seve%r,rname,'- Several versions of spectra not allowed (SINGLE)')
    else
      call class_message(seve%r,rname,'- Several versions of spectra allowed (MULTIPLE)')
    endif
  elseif (filein_vlmhead%loca%islo.ne.0) then
    call class_message(seve%r,rname,'Input file: '//filein_vlmhead%file)
  else
    call class_message(seve%r,rname,'Input file: none')
  endif
  !
  if (fileout%lun.le.0) then
     call class_message(seve%r,rname,'Output file: none')
  else
     if (.not.filein_is_fileout()) then
        inquire(unit=fileout%lun,name=name)
        call gdf_conversion(fileout%conv%code,aconv)
     endif
     call class_message(seve%r,rname,'Output file: '//trim(name)//' '//aconv)
     if (fileout%desc%single) then
        call class_message(seve%r,rname,'- Several versions of spectra not allowed (SINGLE)')
     else
        call class_message(seve%r,rname,'- Several versions of spectra allowed (MULTIPLE)')
     endif
     if (fileout%update) then
        call class_message(seve%r,rname,'- Opened in UPDATE mode')
     endif
  endif
end subroutine class_files_info
!
subroutine class_luns_get(error)
  use gbl_message
  use gkernel_interfaces
  use class_common
  use classcore_interfaces, except_this=>class_luns_get
  !---------------------------------------------------------------------
  ! @ private
  ! Allocate 2 luns for filein and fileout support
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='FILE'
  !
  if (sic_getlun(lun_1).ne.1)  goto 10
  if (sic_getlun(lun_2).ne.1)  goto 10
  return
  !
10 continue
  error = .true.
  call class_message(seve%e,rname,'No logical unit left')
  !
end subroutine class_luns_get
!
subroutine class_luns_free(error)
  use gkernel_interfaces
  use class_common
  use classcore_interfaces, except_this=>class_luns_free
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! Free the 2 luns for filein and fileout support
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  ! Logical error flag
  !
  call sic_frelun(lun_1)
  call sic_frelun(lun_2)
  !
end subroutine class_luns_free
!
function filein_opened(rname,error)
  use gbl_message
  use classcore_interfaces, except_this=>filein_opened
  use class_common
  !---------------------------------------------------------------------
  ! @ private
  !  Return .true. if an input file is opened, or error with message
  ! otherwise
  !---------------------------------------------------------------------
  logical :: filein_opened  ! Function value on return
  character(len=*), intent(in)    :: rname  ! Calling routine name
  logical,          intent(inout) :: error  ! Logical error flag
  !
  filein_opened = filein%lun.gt.0 .or. filein_vlmhead%loca%islo.gt.0
  if (.not.filein_opened) then
    call class_message(seve%e,rname,'No input file opened')
    error = .true.
    return
  endif
end function filein_opened
!
function fileout_opened(rname,error)
  use gbl_message
  use classcore_interfaces, except_this=>fileout_opened
  use class_common
  !---------------------------------------------------------------------
  ! @ private
  !  Return .true. if an output file is opened, or error with message
  ! otherwise
  !---------------------------------------------------------------------
  logical :: fileout_opened  ! Function value on return
  character(len=*), intent(in)    :: rname  ! Calling routine name
  logical,          intent(inout) :: error  ! Logical error flag
  !
  fileout_opened = fileout%lun.gt.0
  if (.not.fileout_opened) then
    call class_message(seve%e,rname,'No output file opened')
    error = .true.
    return
  endif
end function fileout_opened
!
function filein_is_fileout()
  use classcore_interfaces, except_this=>filein_is_fileout
  use class_common
  !---------------------------------------------------------------------
  ! @ public (for Class libraries only)
  !  Return .true. if output file is input file
  !---------------------------------------------------------------------
  logical :: filein_is_fileout  ! Function value on return
  filein_is_fileout = filein%lun.ne.0 .and. filein%lun.eq.fileout%lun
end function filein_is_fileout
