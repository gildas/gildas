subroutine class_drop(line,r,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_drop
  use class_parameter
  use class_index
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! CLASS Support routine for command
  !   DROP [Number] [Version]
  ! Deletes an observation from current index. Default is current
  ! observation in R buffer.
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: line   ! Command line
  type(observation), intent(in)    :: r      !
  logical,           intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='DROP'
  logical :: lst,found
  integer(kind=obsnum_length) :: number
  integer(kind=4) :: versio,tver
  character(len=message_length) :: mess
  integer(kind=entry_length) :: i
  !
  if (cx%next.le.1) then
     call class_message(seve%e,rname,'Current index is empty')
     error = .true.
     return
  endif
  !
  ! Decode line argument
  if (sic_present(0,1)) then
     call sic_i8 (line,0,1,number,.true.,error)
     if (error) return
     versio = 0
     call sic_i4 (line,0,2,versio,.false.,error)
     if (error) return
     lst = versio .eq. 0
  else
     number=r%head%gen%num
     versio=abs(r%head%gen%ver)
     lst = .false.
  endif
  found = .false.
  do i=1,cx%next-1
     if (.not.found) then
        if (lst) then
           found = cx%num(i).eq.number .and. cx%ver(i).ge.0
        else
           tver = abs(cx%ver(i))
           found = cx%num(i).eq.number .and. tver.eq.versio
        endif
        if (found) then
           if (knext.ge.i)  knext = knext-1
        endif
     else
       ! Shift
       call optimize_tooptimize(cx,i,cx,i-1,.true.,error)
       if (error)  return
     endif
  enddo
  if (found) then
     cx%next = cx%next - 1
  else
     write(mess,'(A,I0,A,I0,A)')  &
       'Observation ',number,';',versio,' not in current index'
     call class_message(seve%e,rname,mess)
     error = .true.
     return
  endif
  !
  ! Reset the IDX% Sic structure
  call class_variable_index_reset(error)
  if (error) return
  !
  ! Update the CX index ranges
  cx%ranges%done = .false.  ! Force update
  call index_ranges(cx,error)
  if (error)  return
  !
end subroutine class_drop
