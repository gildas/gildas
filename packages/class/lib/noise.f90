subroutine class_noise(set,line,r,error,user_function)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_noise
  use class_types
  !----------------------------------------------------------------------
  ! @ public
  ! CLASS ANALYSE Support routine for command
  !   NOISE [Sigma [New]]
  ! Compute a spectrum with a gaussian random noise of specified sigma.
  ! The spectrum is immediately plotted, or loaded into R memory.
  !----------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set             !
  character(len=*),    intent(in)    :: line            ! Input command line
  type(observation),   intent(inout) :: r               !
  logical,             external      :: user_function   !
  logical,             intent(inout) :: error           ! Error flag
  ! Local
  character(len=*), parameter :: rname='NOISE'
  real, allocatable :: ynoise(:)
  real :: sigma
  integer :: n,i,ier
  !
  if (sic_present(0,1)) then
    ! Get RMS from command line
    call sic_r4(line,0,1,sigma,.false.,error)
    if (error) return
    !
  elseif (r%head%xnum.ne.0) then
    ! Guess from header parameters
    call class_noise_guess(rname,r,sigma,error)
    !
  else
    ! Can not guess
    call class_message(seve%e,rname,'No spectrum in memory')
    error = .true.
  endif
  if (error) return
  !
  if (sic_present(0,2)) then  ! NEW
     ! New data
     do i=1,r%cnchan
        r%spectre(i) = rangau(sigma)
     enddo
     ! New baseline section
     r%head%presec(class_sec_bas_id) = .true.
     r%head%bas%deg      = 0
     r%head%bas%sigfi    = sigma
     r%head%bas%aire     = 0.
     r%head%bas%nwind    = 0
     r%head%bas%w1(:)    = 0.
     r%head%bas%w2(:)    = 0.
     r%head%bas%sinus(:) = 0.
     ! New R buffer
     call newdat(set,r,error)
     if (error)  return
     !
  else
     call gr_segm('NOISE',error)
     allocate (ynoise(r%cnchan),stat=ier)
     do i=1,r%cnchan
        ynoise(i) = rangau(sigma)
     enddo
     n = r%cnchan
     if (set%plot.eq.'N') then
        call conne2(1.,1.,1.,ynoise,n,cplot)
     else
        call histo2(1.,1.,1.,ynoise,n,cplot)
     endif
     deallocate(ynoise)
     call gr_segm_close(error)
  endif
  !
end subroutine class_noise
!
subroutine class_noise_guess(rname,obs,sigma,error)
  use gbl_message
  use gbl_constant
  use classcore_interfaces, except_this=>class_noise_guess
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Try to guess the noise (RMS) value from header values
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: rname  ! Calling routine name
  type(observation), intent(in)    :: obs    !
  real(kind=4),      intent(out)   :: sigma  ! Computed RMS
  logical,           intent(inout) :: error  ! Logical error flag
  ! Local
  real(kind=8) :: res
  character(len=message_length) :: mess
  !
  if (obs%head%gen%kind.eq.kind_spec) then
    res = obs%head%spe%fres*1.d6  ! MHz
  else
    res = obs%head%dri%width*1.e6  ! MHz
  endif
  !
  if (obs%head%presec(class_sec_bas_id) .and. obs%head%bas%sigfi.gt.0.) then
    sigma = obs%head%bas%sigfi
    write(mess,'(a,1pg10.3,a)') 'RMS value: ',sigma,' (from baseline informations)'
    call class_message(seve%i,rname,mess)
    !
  elseif (obs%head%gen%tsys.gt.0. .and. res.ne.0.d0 .and. obs%head%gen%time.gt.0.) then
    sigma = obs%head%gen%tsys/sqrt(abs(res*obs%head%gen%time))
    write(mess,'(a,1pg10.3,a)') 'RMS value: ',sigma,' (from general informations)'
    call class_message(seve%i,rname,mess)
    !
  else 
    ! This can happen for simulated data e.g. from command MODEL
    call class_message(seve%e,rname,  &
      'Can not guess a noise RMS from header: parameters missing or null')
    error = .true.
    return
  endif
  !
end subroutine class_noise_guess
