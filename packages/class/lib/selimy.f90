subroutine selimy(y1,y2)
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS	Internal routine
  !	Set Y limits to Y1 Y2
  !---------------------------------------------------------------------
  real(kind=plot_length), intent(in) :: y1,y2  !
  ! Global
  include 'gbl_pi.inc'
  ! Local
  real(kind=plot_length) :: deps
  !
  ! Minimal protection against floating overflows...
  deps = epsr4
  if ((abs(y1).gt.maxr4/2).or.(abs(y2).gt.maxr4/2)) then
     guy1 = -maxr4/2
     guy2 = +maxr4/2
  elseif (abs(y1-y2).lt.min(deps*abs(y1),deps*deps)) then
     guy1 = y1*(1+deps)
     guy2 = y1*(1-deps)
  else
     guy1 = y1
     guy2 = y2
  endif
  call get_box(gx1,gx2,gy1,gy2)
  guy  = (gy2-gy1)/(guy2-guy1)
end subroutine selimy
!
subroutine gelimy(y1,y2,dy)
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS	Internal routine
  !	Returns current limits in Y and conversion factor
  ! Arguments :
  !	Y1,Y2	R*4	Limits 			Output
  !	DY	R*4	Conversion factor	Output
  !---------------------------------------------------------------------
  real(kind=plot_length), intent(out) :: y1,y2  !
  real(kind=plot_length), intent(out) :: dy     !
  !
  y1  = guy1
  y2  = guy2
  guy = (gy2-gy1)/(guy2-guy1)
  dy  = guy
end subroutine gelimy
!
subroutine selimy2d(y1,y2)
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS	Internal routine
  !	Set Y limits to Y1 Y2
  !---------------------------------------------------------------------
  real(kind=4), intent(in) :: y1,y2  !
  ! Global
  include 'gbl_pi.inc'
  !
  ! Minimal protection against floating overflows...
  if ((abs(y1).gt.maxr4/2).or.(abs(y2).gt.maxr4/2)) then
     guy1 = -maxr4/2
     guy2 = +maxr4/2
  elseif (abs(y1-y2).lt.min(epsr4*abs(y1),epsr4*epsr4)) then
     guy1 = y1*(1+epsr4)
     guy2 = y1*(1-epsr4)
  else
     guy1 = y1
     guy2 = y2
  endif
end subroutine selimy2d
!
subroutine selimz(z1,z2)
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS	Internal routine
  !	Set Z limits to Z1 Z2
  !---------------------------------------------------------------------
  real(kind=plot_length), intent(in) :: z1,z2  !
  ! Global
  include 'gbl_pi.inc'
  ! Local
  real(kind=plot_length) :: deps
  !
  ! Minimal protection against floating overflows...
  deps = epsr4
  if ((abs(z1).gt.maxr4/2).or.(abs(z2).gt.maxr4/2)) then
     guz1 = -maxr4/2
     guz2 = +maxr4/2
  elseif (abs(z1-z2).lt.min(deps*abs(z1),deps*deps)) then
     guz1 = z1*(1+deps)
     guz2 = z1*(1-deps)
  else
     guz1 = z1
     guz2 = z2
  endif
  call get_box(gx1,gx2,gy1,gy2)
  guz  = (gy2-gy1)/(guz2-guz1)
end subroutine selimz
!
subroutine gelimz(z1,z2,dz)
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS	Internal routine
  !	Returns current limits in Z and conversion factor
  !---------------------------------------------------------------------
  real(kind=4), intent(out) :: z1,z2  !
  real(kind=4), intent(out) :: dz     !
  !
  z1  = guz1
  z2  = guz2
  guz = (gy2-gy1)/(guz2-guz1)
  dz  = guz
  !
end subroutine gelimz
