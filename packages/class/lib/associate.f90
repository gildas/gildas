!-----------------------------------------------------------------------
! Support file for command
!   ASSOCIATE
! Add an Associated Array to the appropriate section in the R buffer
!-----------------------------------------------------------------------
subroutine class_associate(set,line,r,error)
  use gbl_message
  use gkernel_types
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_associate
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! Support routine for command
  !   ASSOCIATE  AAName  Value
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  character(len=*),    intent(in)    :: line
  type(observation),   intent(inout) :: r
  logical,             intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='ASSOCIATE'
  integer(kind=4), parameter :: optdel=3
  !
  if (r%head%xnum.eq.0) then
    call class_message(seve%e,rname,'No R spectrum in memory')
    error = .true.
    return
  endif
  !
  if (sic_present(optdel,0)) then  ! /DELETE
    call class_associate_delete(line,r,error)
  else
    call class_associate_add(set,line,r,error)
  endif
  if (error)  return
  !
end subroutine class_associate
!
subroutine class_associate_add(set,line,r,error)
  use gbl_message
  use gkernel_types
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_associate_add
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !   Add an Associated Array in the dedicated section
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  character(len=*),    intent(in)    :: line
  type(observation),   intent(inout) :: r
  logical,             intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='ASSOCIATE'
  character(len=12) :: aaname,aaunit
  character(len=varname_length) :: var
  integer(kind=4) :: nc,iarray,nelem,dim2,memory(1),badi4
  type(sic_descriptor_t) :: desc
  logical :: found
  integer(kind=address_length) :: ip
  real(kind=4) :: badr4
  integer(kind=4), parameter :: optbad=1
  integer(kind=4), parameter :: optunit=2
  !
  ! Associated Array name
  call sic_ch(line,0,1,aaname,nc,.true.,error)
  if (error)  return
  call sic_upper(aaname)
  !
  ! Data array
  call sic_ch(line,0,2,var,nc,.true.,error)
  if (error)  return
  call sic_descriptor(var,desc,found)
  if (.not.found) then
    call class_message(seve%e,rname,'No such data array')
    error = .true.
    return
  endif
  !
  ! Main sanity checks
  if (desc%ndim.le.0 .or. desc%ndim.gt.2) then
    call class_message(seve%e,rname,'Associated Array must have 1 or 2 dimensions')
    error = .true.
    return
  endif
  if (desc%dims(1).ne.r%cnchan) then
    call class_message(seve%e,rname,'Associated Array must have as many channels as RY')
    error = .true.
    return
  endif
  !
  ip = gag_pointer(desc%addr,memory)
  if (desc%ndim.eq.1) then
    dim2 = 0
  else
    dim2 = desc%dims(2)
  endif
  nelem = desc%dims(1)*max(1,desc%dims(2))
  !
  if (class_assoc_isreserved(aaname)) then
    ! Reserved arrays
    if (sic_present(optunit,0)) then
      call class_message(seve%e,rname,'/UNIT option forbidden for reserved array '//aaname)
      error = .true.
      return
    endif
    if (sic_present(optbad,0)) then
      call class_message(seve%e,rname,'/BAD option forbidden for reserved array '//aaname)
      error = .true.
      return
    endif
    call class_assoc_add(r,aaname,iarray,error)
    if (error)  return
  else
    ! Free (non-reserved) arrays
    !
    ! /UNIT
    aaunit = ''
    call sic_ch(line,optunit,1,aaunit,nc,.false.,error)
    if (error)  return
    !
    ! /BAD
    if (.not.sic_present(optbad,0)) then
      call class_message(seve%e,rname,'You must provide the blank value with the option /BAD')
      error = .true.
      return
    endif
    !
    select case (desc%type)
    case (fmt_r4)
      call sic_r4(line,optbad,1,badr4,.true.,error)
      if (error)  return
      call class_assoc_add(r,aaname,aaunit,desc%type,dim2,badr4,iarray,error)
      if (error)  return
      !
    case (fmt_i4)
      call sic_i4(line,optbad,1,badi4,.true.,error)
      if (error)  return
      call class_assoc_add(r,aaname,aaunit,desc%type,dim2,badi4,iarray,error)
      if (error)  return
      !
    case default
      call class_message(seve%e,rname,'Kind of data not yet supported (1)')
      error = .true.
      return
    end select
  endif
  !
  ! Now copy the values into the Associated Array
  select case (r%assoc%array(iarray)%fmt)
  case (fmt_i4,fmt_by,fmt_b2)
    if (desc%type.eq.fmt_i4) then
      call i4toi4(memory(ip),r%assoc%array(iarray)%i4,nelem)
    elseif (desc%type.eq.fmt_r4) then
      call r4toi4(memory(ip),r%assoc%array(iarray)%i4,nelem)
    else
      call class_message(seve%e,rname,'Kind of data not yet supported (2)')
      error = .true.
      return
    endif
  case (fmt_r4)
    if (desc%type.eq.fmt_i4) then
      call i4tor4(memory(ip),r%assoc%array(iarray)%r4,nelem)
    elseif (desc%type.eq.fmt_r4) then
      call r4tor4(memory(ip),r%assoc%array(iarray)%r4,nelem)
    else
      call class_message(seve%e,rname,'Kind of data not yet supported (3)')
      error = .true.
      return
    endif
  case default
    call class_message(seve%e,rname,'Kind of data not yet supported (4)')
    error = .true.
    return
  end select
  !
  call newdat_assoc(set,r,error)
  !
end subroutine class_associate_add
!
subroutine class_associate_delete(line,r,error)
  use gbl_message
  use gkernel_types
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_associate_delete
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !   Add an Associated Array in the dedicated section
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: line
  type(observation), intent(inout) :: r
  logical,           intent(inout) :: error
  ! Local
  character(len=12) :: aaname
  integer(kind=4) :: nc
  !
  ! Associated Array name
  call sic_ke(line,0,1,aaname,nc,.true.,error)
  if (error)  return
  !
  call class_assoc_delete(r,aaname,error)
  if (error)  return
  !
end subroutine class_associate_delete
