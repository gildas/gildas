subroutine class_unblank(line,error)
  use gildas_def
  use image_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_unblank
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  !   Support routine for command
  !    EXPERIM\UNBLANK in.tab [out.tab]
  !      /MODE  REJECT|NOISE|INTERPOLATE|VALUE [Value]
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   !
  logical,          intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='UNBLANK'
  type(gildas), target :: tabin
  type(gildas), pointer :: tabou
  character(len=filename_length) :: namein,nameou
  integer(kind=4) :: nc,mode
  real(kind=4) :: value
  logical :: inplace,error2
  integer(kind=4), parameter :: nmode=4
  character(len=11) :: vocab(nmode),argum,found
  data vocab /'VALUE','NOISE','INTERPOLATE','REJECT'/
  !
  ! 1) Command line parsing
  call sic_ch(line,0,1,namein,nc,.true.,error)
  if (error) return
  !
  inplace = .not.sic_present(0,2)
  nameou = ''
  call sic_ch(line,0,2,nameou,nc,.false.,error)
  if (error) return
  !
  mode = 4
  value = 0.  ! Default if mode.eq.1  (VALUE)
  if (sic_present(1,0)) then
    call sic_ke(line,1,1,argum,nc,.true.,error)
    if (error)  return
    call sic_ambigs(rname,argum,found,mode,vocab,nmode,error)
    if (error)  return
    call sic_r4(line,1,2,value,.false.,error)
    if (error)  return
  endif
  !
  ! Sanity check
  if (mode.eq.4 .and. inplace) then
    ! /MODE REJECT produces a table of different size. Can not perform in
    ! place.
    call class_message(seve%e,rname,'/MODE REJECT can not be performed in place')
    error = .true.
    return
  endif
  !
  ! 2) Read input table (header+data)
  call gildas_null(tabin)
  tabin%gil%form = fmt_r4
  tabin%gil%ndim = 2
  call gdf_read_gildas(tabin,namein,'.tab',error,data=.true.)
  if (gildas_error(tabin,rname,error)) return
  ! Check that input table is a Class table? => Should perform same tests
  ! as in XY-MAP, if any.
  !
  ! 3) Prepare the output table
  if (inplace) then
    tabou => tabin
  else
    allocate(tabou)
    call gildas_null(tabou)
    call gdf_copy_header(tabin,tabou,error)
    if (error)  goto 100
    call gdf_allocate(tabou,error)
    if (error)  goto 100
    tabou%file = nameou
    call gdf_create_image(tabou,error)
    if (gildas_error(tabou,rname,error)) goto 100
  endif
  !
  ! 4) Do the job
  if (mode.eq.4) then  ! REJECT
    call unblank_reject(tabin,tabou,error)
  else  ! VALUE|NOISE|INTERPOLATE
    call unblank_patch(tabin,tabou,mode,value,error)
  endif
  if (error)  goto 100
  !
  ! 5) Fill the output file
  call gdf_write_data(tabou,tabou%r2d,error)
  if (gildas_error(tabou,rname,error))  continue
  !
  call gdf_close_image(tabou,error)
  if (gildas_error(tabou,rname,error))  continue
  !
  ! Cleaning
100 continue
  error2 = .false.
  if (.not.inplace) then
    if (associated(tabou%r2d))  deallocate(tabou%r2d)
    deallocate(tabou)
    call gdf_close_image(tabin,error2)
  endif
  if (associated(tabin%r2d))  deallocate(tabin%r2d)
  error = error .or. error2
  !
end subroutine class_unblank
!
subroutine unblank_reject(tabin,tabou,error)
  use image_def
  use gbl_message
  use classcore_interfaces, except_this=>unblank_reject
  !---------------------------------------------------------------------
  ! @ private
  !  Copy input table into output table, but reject all spectra which
  ! contain one or more blank values
  !---------------------------------------------------------------------
  type(gildas), intent(in)    :: tabin  !
  type(gildas), intent(inout) :: tabou  !
  logical,      intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='UNBLANK'
  integer(kind=index_length) :: inspec,onspec,iblc(2),itrc(2),oblc(2),otrc(2)
  character(len=message_length) :: mess
  integer(kind=4) :: faxi,saxi
  !
  faxi = tabin%gil%faxi  ! Freq/Velo axis
  if (faxi.eq.1) then
    saxi = 2  ! Spectrum axis
  else
    !   In case of .bat, this means we will shrink the 1st dimension (at the end
    ! of this subroutine), which gives incorrect results in gio if we write all
    ! the data block at once... This, because we want to discard non-contiguous
    ! elements in the data block while gio writes a big contiguous piece.
    !   The solution is to write the data by pieces of contiguous blocks, e.g.
    ! do i=1,tabou%gil%dim(2)
    !   tabou%blc(2) = i
    !   tabou%trc(2) = i
    !   call gdf_write_data(tabou,tabou%r2d(:,i),error)
    ! enddo
    !  But this means Nposi calls to gdf_write_data.
    !
    call class_message(seve%e,rname,'/MODE REJECT is not supported for .bat tables')
    error = .true.
    return
    saxi = 1
  endif
  !
  itrc(faxi) = tabin%gil%dim(faxi)
  !
  oblc(faxi) = 1
  otrc(faxi) = tabou%gil%dim(faxi)
  !
  onspec = 0
  ! NB: this loop is efficient (contiguous accesses) only if faxi = 1
  do inspec=1,tabin%gil%dim(saxi)
    iblc(faxi) = 4  ! Do not check for blanks in X,Y,W
    iblc(saxi) = inspec
    itrc(saxi) = inspec
    if (any(tabin%r2d(iblc(1):itrc(1),iblc(2):itrc(2)).eq.tabin%gil%bval))  cycle
    !
    onspec = onspec+1
    iblc(faxi) = 1  ! Do not forget to copy X,Y,W
    oblc(saxi) = onspec
    otrc(saxi) = onspec
    tabou%r2d(oblc(1):otrc(1),oblc(2):otrc(2)) =  &
      tabin%r2d(iblc(1):itrc(1),iblc(2):itrc(2))
  enddo
  !
  ! User feedback
  if (onspec.eq.0) then
    call class_message(seve%e,rname,'All spectra rejected')
    error = .true.
    return
  elseif (onspec.lt.tabin%gil%dim(saxi)) then
    write(mess,'(A,I0,A,I0)')  &
      'Kept ',onspec,' spectra among ',tabin%gil%dim(saxi)
    call class_message(seve%i,rname,mess)
  else
    call class_message(seve%i,rname,'All spectra kept')
  endif
  !
  ! Update the header
  tabou%gil%dim(saxi) = onspec
  call gdf_update_header(tabou,error)
  if (error)  return
  !
end subroutine unblank_reject
!
subroutine unblank_patch(tabin,tabou,mode,val,error)
  use image_def
  use gbl_message
  use gkernel_interfaces
  use classcore_interfaces, except_this=>unblank_patch
  !---------------------------------------------------------------------
  ! @ private
  !  Copy input table into output table, but replace blanking values
  ! by a value, noise, or interpolation
  !---------------------------------------------------------------------
  type(gildas),    intent(in)    :: tabin  !
  type(gildas),    intent(inout) :: tabou  ! Can be same as tabin
  integer(kind=4), intent(in)    :: mode   ! Replacement mode 1|2|3
  real(kind=4),    intent(in)    :: val    ! Custom value (mode 1)
  logical,         intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='UNBLANK'
  integer(kind=index_length) :: ispec,blc(2),trc(2),pix(2)
  integer(kind=size_length) :: nchan,ntot
  character(len=message_length) :: mess
  real(kind=4) :: value,sigma
  integer(kind=4) :: faxi,saxi,ichan
  !
  faxi = tabin%gil%faxi  ! Freq/Velo axis
  if (faxi.eq.1) then
    saxi = 2  ! Spectrum axis
  else
    saxi = 1
  endif
  !
  blc(faxi) = 4
  trc(faxi) = tabin%gil%dim(faxi)
  !
  nchan = 0
  ! NB: this loop is efficient (contiguous accesses) only if faxi = 1
  do ispec=1,tabin%gil%dim(saxi)
    !
    pix(saxi) = ispec
    !
    if (mode.eq.1) then  ! Custom value
      value = val
    elseif (mode.eq.2) then  ! Noise
      ! Reuse the spectrum weight. This assumes that the 3rd column is the
      ! weight, sigma-based, in units s.MHz/K^2. The 1.e-3 factor is here
      ! because of the MHz unit.
      pix(faxi) = 3  ! Weight is 3rd column
      sigma = 1.e-3/sqrt(tabin%r2d(pix(1),pix(2)))
      value = rangau(sigma)
    endif
    !
    do ichan=4,tabin%gil%dim(faxi)
      pix(faxi) = ichan
      if (tabin%r2d(pix(1),pix(2)).eq.tabin%gil%bval) then
        ! Found a blank value => patch it
        nchan = nchan+1
        if (mode.eq.3) then  ! Interpolation
          blc(saxi) = ispec
          trc(saxi) = ispec
          tabou%r2d(pix(1),pix(2)) = obs_fillin(     &
            tabin%r2d(blc(1):trc(1),blc(2):trc(2)),  &
            ichan-3,                                 &
            1,int(tabin%gil%dim(faxi)-3,kind=4),     &
            tabin%gil%bval)
        else
          tabou%r2d(pix(1),pix(2)) = value
        endif
      else
        tabou%r2d(pix(1),pix(2)) = tabin%r2d(pix(1),pix(2))
      endif
    enddo
  enddo
  !
  ! User feedback
  ntot = (tabou%gil%dim(faxi)-3)*tabou%gil%dim(saxi)
  if (nchan.eq.0) then
    call class_message(seve%i,rname,'No channel modified')
  elseif (nchan.lt.ntot) then
    write(mess,'(A,I0,A,I0)')  &
      'Patched ',nchan,' channels among ',ntot
    call class_message(seve%i,rname,mess)
  elseif (nchan.eq.ntot) then
    call class_message(seve%e,rname,'All channels patched')
    error = .true.
    return
  endif
  !
end subroutine unblank_patch
