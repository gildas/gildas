subroutine eix_newdata(first_new,last_new,error)
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>eix_newdata
  use class_common
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! Action to be performed when new data has actually been found in the
  ! input file
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(out)   :: first_new  ! First new index entry
  integer(kind=entry_length), intent(out)   :: last_new   ! Last new index entry
  logical,                    intent(inout) :: error      ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='FIND'
  integer(kind=entry_length) :: i
  !
  if (filein_isvlm) then
    call class_message(seve%e,rname,'NEW_DATA is not relevant for FILE IN VLM')
    error = .true.
    return
  endif
  !
  call classic_file_fflush(filein,error)  ! Flush for reading
  if (error)  return  ! e.g. file has disappearead
  call classic_filedesc_read(filein,error)
  if (error)  return
  !
  ! Resize The Input File Index to the new size
  call reallocate_optimize(ix,filein%desc%xnext-1,.true.,.true.,error)
  if (error)  return
  !
  ! Nullify the Entry Index buffer: it may contain a record which has
  ! been updated on disk. This forces re-reading.
  call classic_recordbuf_nullify(ibufbi)
  ! Nullify the Observation buffer: in V2 files, there can be several
  ! (pieces of) observations sharing the same record. This forces
  ! re-reading from disk when GET will be done later.
  call classic_recordbuf_nullify(ibufobs)
  !
  ! Skip the new observations with wrong kind
  do
    if (filein%desc%xnext.le.ix%next) then
      ! No more entries, no new data of correct kind
      call class_message(seve%w,rname,'No new data present')
      first_new = ix%next-1  ! Define a null range
      last_new = 1
      return
    endif
    !
    ! Update IX
    call rix_to_ix(ix%next,error)
    if (error) return
    ix%next = ix%next + 1
    !
    if (smin%kind.eq.ix%kind(ix%next-1))  exit
  enddo
  !
  ! At least one with correct kind
  call class_message(seve%i,rname,'New data present')
  first_new = ix%next-1
  last_new = first_new
  !
  if (filein%desc%xnext.le.ix%next) return
  !
  ! More than one new: care is taken that line and continuum mode are not mixed
  do i = first_new+1, filein%desc%xnext-1
    call rix_to_ix(i,error)
    if (error) return
    ix%next = ix%next+1
    !
    if (smin%kind.eq.ix%kind(i)) then
      last_new = i
    else
      ! Stop there; keep other data for next FIND NEW_DATA command
      return
    endif
  enddo
  !
end subroutine eix_newdata
