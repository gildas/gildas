!-----------------------------------------------------------------------
! Support file for PYCLASS extensions, Fortran part
!-----------------------------------------------------------------------
!  Always compile this file, even if Python binding is disabled. This
! avoids different interface files when Python is enabled or not, and
! subsequent troubles when compiling in this 2 modes with the same
! sources.
!-----------------------------------------------------------------------
subroutine pyclass_obsx_val(obs,ival,iunitin,oval,ounitin,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>pyclass_obsx_val
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! Convert the input value from input unit to the output unit, for
  ! the current R spectrum in memory. Frequency (signal or image) units
  ! (in and out) assumed to be absolute frequencies.
  !---------------------------------------------------------------------
  type(observation), intent(in)  :: obs      !
  real(kind=8),      intent(in)  :: ival     ! Input value
  character(len=*),  intent(in)  :: iunitin  ! Input unit
  real(kind=8),      intent(out) :: oval     ! Output value
  character(len=*),  intent(in)  :: ounitin  ! Output unit
  logical,           intent(out) :: error    ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='RX_VAL'
  character(len=1) :: iunit,ounit
  real(kind=8) :: cval
  !
  error = .false.
  !
  ! Convert input value to channel value
  iunit = iunitin
  call sic_upper(iunit)
  select case (iunit)
  case ('F')
    call abscissa_sigabs2chan(obs%head,ival,cval)
  case ('I')
    call abscissa_imaabs2chan(obs%head,ival,cval)
  case ('V')
    call abscissa_velo2chan(obs%head,ival,cval)
  case ('C')
    cval = ival
  case default
    call class_message(seve%e,rname,trim(iunit)//' is not a supported unit')
    error = .true.
    return
  end select
  !
  ! Convert channel value to output value
  ounit = ounitin
  call sic_upper(ounit)
  select case (ounit)
  case ('F')
    call abscissa_chan2sigabs(obs%head,cval,oval)
  case ('I')
    call abscissa_chan2imaabs(obs%head,cval,oval)
  case ('V')
    call abscissa_chan2velo(obs%head,cval,oval)
  case ('C')
    oval = cval
  case default
    call class_message(seve%e,rname,trim(iunit)//' is not a supported unit')
    error = .true.
    return
  end select
  !
end subroutine pyclass_obsx_val
!
subroutine pyclass_obsx_minmax(set,obs,unitin,mini,maxi,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>pyclass_obsx_minmax
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! Return the X range of the R spectrum
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)  :: set     !
  type(observation),   intent(in)  :: obs     !
  character(len=*),    intent(in)  :: unitin  ! Unit for returning values. Blank means current unit
  real(kind=8),        intent(out) :: mini    !
  real(kind=8),        intent(out) :: maxi    !
  logical,             intent(out) :: error   ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='RX_MINMAX'
  character(len=1) :: unit
  real(kind=8) :: vl,vr
  !
  error = .false.
  !
  ! Blank: default to current lower X unit
  if (unitin.eq.' ') then
    unit = set%unitx(1)
  else
    unit = unitin
  endif
  call sic_upper(unit)
  !
  select case (unit)
  case ('F')
    call abscissa_sigabs_left(obs%head,vl)
    call abscissa_sigabs_right(obs%head,vr)
  case ('I')
    call abscissa_imaabs_left(obs%head,vl)
    call abscissa_imaabs_right(obs%head,vr)
  case ('V')
    call abscissa_velo_left(obs%head,vl)
    call abscissa_velo_right(obs%head,vr)
  case ('C')
    vl = .5d0
    vr = dble(obs%head%spe%nchan)+.5d0
  case default
    call class_message(seve%e,rname,trim(unit)//' is not a supported unit')
    error = .true.
    return
  end select
  !
  if (vl.lt.vr) then
    mini = vl
    maxi = vr
  else
    mini = vr
    maxi = vl
  endif
  !
end subroutine pyclass_obsx_minmax
!
subroutine pyclass_plotx_minmax(set,unitin,mini,maxi,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>pyclass_plotx_minmax
  use class_types
  use plot_formula
  !---------------------------------------------------------------------
  ! @ public
  ! Return the X range of the plot
  ! NB: the subroutine returns actually the SET MODE X range, not
  ! the effective PLOT range (they are the same AFTER the command PLOT).
  ! But the command ANA\DRAW has the same bug-feature, and since the
  ! cursor is used by Weeds to identify lines, we are consistent...
  ! ---
  ! Entry point with a 'class_setup_t' as argument
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)  :: set     !
  character(len=*),    intent(in)  :: unitin  ! Unit for returning values. Blank means current unit
  real(kind=8),        intent(out) :: mini    !
  real(kind=8),        intent(out) :: maxi    !
  logical,             intent(out) :: error   ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='PX_MINMAX'
  character(len=1) :: unit
  real(kind=8) :: vl,vr
  !
  error = .false.
  !
  ! Blank: default to current lower X unit
  if (unitin.eq.' ') then
    unit = set%unitx(1)
  else
    unit = unitin
  endif
  call sic_upper(unit)
  !
  select case (unit)
  case ('F')
    vl = gfx1+gfxo
    vr = gfx2+gfxo
  case ('I')
    vl = gix1+gixo
    vr = gix2+gixo
  case ('V')
    vl = gvx1
    vr = gvx2
  case ('C')
    vl = gcx1
    vr = gcx2
  case default
    call class_message(seve%e,rname,trim(unit)//' is not a supported unit')
    error = .true.
    return
  end select
  !
  if (vl.lt.vr) then
    mini = vl
    maxi = vr
  else
    mini = vr
    maxi = vl
  endif
  !
end subroutine pyclass_plotx_minmax
