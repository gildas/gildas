!==============================================================================
!==========================                         ===========================
!==========================  F I T S   E X P O R T  ===========================
!==========================                         ===========================
!==============================================================================
!
!==============================================================================
!============================  Header card images  ============================
!==============================================================================
!
subroutine zeros_to_spaces(String)
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*), intent(inout) :: String  !
  ! Local
  integer :: i
  !
  do i = 1,len(String)
     if (ichar(String(i:i)).eq.0) String(i:i) = ' '
  enddo
end subroutine zeros_to_spaces
!
subroutine fits_put_keyword(pCardImage,pKeyWord,pValue,pError)
  use classcore_interfaces, except_this=>fits_put_keyword
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=80), intent(out) :: pCardImage  !
  character(len=*),  intent(in)  :: pKeyWord    !
  logical,           intent(in)  :: pValue      !
  logical,           intent(out) :: pError      !
  ! Local
  character(len=8) :: KeyWord
  !
  pError = .false.
  pCardImage = repeat(' ',80)
  !
  KeyWord = pKeyWord
  call zeros_to_spaces(KeyWord)
  !
  ! KeyWord (bytes 1-8).
  pCardImage(1 : 8) = KeyWord
  !
  if (pValue) then
     ! Value indicator (bytes 9-10).
     pCardImage(9 : 10) =  '= '
  endif
end subroutine fits_put_keyword
!
subroutine fits_put_comment(pCardImage,pComment,pError)
  use classcore_interfaces, except_this=>fits_put_comment
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=80), intent(inout) :: pCardImage  !
  character(len=*),  intent(in)    :: pComment    !
  logical,           intent(out)   :: pError      !
  ! Local
  character(len=48) :: Comment
  integer :: iLen
  !
  pError = .false.
  !
  ! Comment.
  if (len(pComment).gt.0) then
     Comment = pComment
     call zeros_to_spaces(Comment)
     !
     iLen = len_trim(pCardImage)
     if (iLen.le.30) then
        ! Comment indicator (bytes 31-32).
        pCardImage(31 : 32) = ' /'
        !
        ! Comment (bytes 33-80).
        pCardImage(33 : 80) = Comment
     elseif (iLen.le.77) then
        pCardImage(iLen + 1 : iLen + 2) = ' /'
        pCardImage(iLen + 3 :       80) = Comment
     endif
  endif
end subroutine fits_put_comment
!
subroutine fits_put_logi(pKeyWord,pValue,pComment,pCheck,pError)
  use gkernel_interfaces
  use classcore_interfaces, except_this=>fits_put_logi
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: pKeyWord  !
  logical,          intent(in)  :: pValue    !
  character(len=*), intent(in)  :: pComment  !
  logical,          intent(in)  :: pCheck    !
  logical,          intent(out) :: pError    !
  ! Local
  character(len=80) :: CardImage
  !
  pError = .false.
  !
  ! Keyword.
  call fits_put_keyword(CardImage,pKeyWord,.true.,pError)
  if (pError) return
  !
  ! Logical value.
  ! Spaces in columns 11-29.
  ! 'T' for .true. or 'F' for .false. in column 30.
  if (pValue) then
     CardImage(11:30) = '                   T'
  else
     CardImage(11:30) = '                   F'
  endif
  !
  ! Comment.
  call fits_put_comment(CardImage,pComment,pError)
  if (pError) return
  !
  ! Write the card image in the fits file.
  call gfits_put(CardImage,pCheck,pError)
  if (pError) return
end subroutine fits_put_logi
!
subroutine fits_put_inte(pKeyWord,pValue,pComment,pCheck,pError)
  use gkernel_interfaces
  use classcore_interfaces, except_this=>fits_put_inte
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: pKeyWord  !
  integer(kind=4),  intent(in)  :: pValue    !
  character(len=*), intent(in)  :: pComment  !
  logical,          intent(in)  :: pCheck    !
  logical,          intent(out) :: pError    !
  ! Local
  character(len=80) :: CardImage
  character(len=20) :: sValue
  !
  pError = .false.
  !
  ! Keyword.
  call fits_put_keyword(CardImage,pKeyWord,.true.,pError)
  if (pError) return
  !
  ! Integer value.
  ! Right-centered in column 11-30.
  write (sValue,'(i20)') pValue
  CardImage(11:30) = sValue
  !
  ! Comment.
  call fits_put_comment(CardImage,pComment,pError)
  if (pError) return
  !
  ! Write the card image in the fits file.
  call gfits_put(CardImage,pCheck,pError)
  if (pError) return
end subroutine fits_put_inte
!
subroutine fits_put_long(pKeyWord,pValue,pComment,pCheck,pError)
  use gkernel_interfaces
  use classcore_interfaces, except_this=>fits_put_long
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: pKeyWord  !
  integer(kind=8),  intent(in)  :: pValue    !
  character(len=*), intent(in)  :: pComment  !
  logical,          intent(in)  :: pCheck    !
  logical,          intent(out) :: pError    !
  ! Local
  character(len=80) :: CardImage
  character(len=20) :: sValue
  !
  pError = .false.
  !
  ! Keyword.
  call fits_put_keyword(CardImage,pKeyWord,.true.,pError)
  if (pError) return
  !
  ! Integer value.
  ! Right-centered in column 11-30.
  write (sValue,'(i20)') pValue
  CardImage(11:30) = sValue
  !
  ! Comment.
  call fits_put_comment(CardImage,pComment,pError)
  if (pError) return
  !
  ! Write the card image in the fits file.
  call gfits_put(CardImage,pCheck,pError)
  if (pError) return
end subroutine fits_put_long
!
subroutine fits_put_dble(pKeyWord,pValue,pComment,pCheck,pError)
  use gkernel_interfaces
  use classcore_interfaces, except_this=>fits_put_dble
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: pKeyWord  !
  real(kind=8),     intent(in)  :: pValue    !
  character(len=*), intent(in)  :: pComment  !
  logical,          intent(in)  :: pCheck    !
  logical,          intent(out) :: pError    !
  ! Local
  character(len=80) :: CardImage
  character(len=20) :: sValue
  !
  pError = .false.
  !
  ! Keyword.
  call fits_put_keyword(CardImage,pKeyWord,.true.,pError)
  if (pError) return
  !
  ! Real value.
  ! Right-centered in column 11-30.
  write (sValue,'(e20.13)') pValue
  CardImage(11:30) = sValue
  !
  ! Comment.
  call fits_put_comment(CardImage,pComment,pError)
  if (pError) return
  !
  ! Write the card image in the fits file.
  call gfits_put(CardImage,pCheck,pError)
  if (pError) return
end subroutine fits_put_dble
!
subroutine fits_put_simple(pKeyWord,pValue,pComment,pCheck,pError)
  use classcore_interfaces, except_this=>fits_put_simple
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: pKeyWord  !
  real(kind=4),     intent(in)  :: pValue    !
  character(len=*), intent(in)  :: pComment  !
  logical,          intent(in)  :: pCheck    !
  logical,          intent(out) :: pError    !
  !
  call fits_put_dble(pKeyWord,dble(pValue),pComment,pCheck,pError)
  if (pError) return
end subroutine fits_put_simple
!
subroutine fits_put_string(pKeyWord,pValue,pComment,pCheck,pError)
  use gkernel_interfaces
  use classcore_interfaces, except_this=>fits_put_string
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: pKeyWord  !
  character(len=*), intent(in)  :: pValue    !
  character(len=*), intent(in)  :: pComment  !
  logical,          intent(in)  :: pCheck    !
  logical,          intent(out) :: pError    !
  ! Local
  character(len=80) :: CardImage
  integer :: iEndQuotePos
  integer :: iLenValue
  !
  pError = .false.
  !
  ! Keyword.
  call fits_put_keyword(CardImage,pKeyWord,.true.,pError)
  if (pError) return
  !
  ! String value.
  !
  ! Opening quote.
  CardImage(11 : 11) = ''''
  !
  ! Closing quote.
  iLenValue = len(pValue)
  iEndQuotePos = 12 + iLenValue
  if (iEndQuotePos.lt.20) iEndQuotePos = 20
  if (iEndQuotePos.gt.80) iEndQuotePos = 80
  CardImage(iEndQuotePos : iEndQuotePos) = ''''
  !
  ! Value.
  if (iLenValue.ge.8) then
     CardImage(12 : iEndQuotePos - 1) = pValue(1 : iEndQuotePos - 12)
  else
     CardImage(12 : 12 + iLenValue) = pValue
  endif
  !
  ! Comment.
  call fits_put_comment(CardImage,pComment,pError)
  if (pError) return
  !
  ! Write the card image in the fits file.
  call gfits_put(CardImage,pCheck,pError)
  if (pError) return
end subroutine fits_put_string
!
subroutine fits_put_novalue(pKeyWord,pComment,pCheck,pError)
  use gkernel_interfaces
  use classcore_interfaces, except_this=>fits_put_novalue
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: pKeyWord  !
  character(len=*), intent(in)  :: pComment  !
  logical,          intent(in)  :: pCheck    !
  logical,          intent(out) :: pError    !
  ! Local
  character(len=80) :: CardImage
  !
  pError = .false.
  !
  ! Keyword.
  call fits_put_keyword(CardImage,pKeyWord,.false.,pError)
  if (pError) return
  !
  ! Comment.
  call fits_put_comment(CardImage,pComment,pError)
  if (pError) return
  !
  ! Write the card image in the fits file.
  call gfits_put(CardImage,pCheck,pError)
  if (pError) return
end subroutine fits_put_novalue
!
!==============================================================================
!=========================  Primary header and data  ==========================
!==============================================================================
!
subroutine fits_write_primary_header(pCheck,pError)
  use gbl_message
  use gkernel_interfaces
  use classcore_interfaces, except_this=>fits_write_primary_header
  !---------------------------------------------------------------------
  ! @ private
  ! LAS
  ! Primary header and data
  !---------------------------------------------------------------------
  logical, intent(in)  :: pCheck    !
  logical, intent(out) :: pError    !
  ! Local
  character(len=*), parameter :: proc='FITS'
  !
  !print *, 'fits_write_primary_header'
  pError = .false.
  call fits_put_logi('SIMPLE' ,.true.,'',pCheck,pError)
  if (pError) goto 98
  call fits_put_inte    ('BITPIX' ,     8,'',pCheck,pError)
  if (pError) goto 98
  call fits_put_inte    ('NAXIS'  ,     0,'',pCheck,pError)
  if (pError) goto 98
  call fits_put_logi('EXTEND' ,.true.,'',pCheck,pError)
  if (pError) goto 98
  call fits_put_string ('ORIGIN' ,'CLASS-Gildas','',pCheck,pError)
  if (pError) goto 98
  call fits_put_string ('CREATOR','CLASS-Gildas','',pCheck,pError)
  if (pError) goto 98
  call fits_put_novalue('END'    ,        '',pCheck,pError)
  if (pError) goto 98
  call gfits_flush_header(pError)
  if (pError) then
     call class_message(seve%e,proc,'Could not flush the primary header properly.')
     return
  endif
  return
  !
  ! Errors.
98 continue
  call class_message(seve%e,proc,'Could not write card image in primary header.')
end subroutine fits_write_primary_header
!
subroutine fits_write_primary_data(pError)
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  logical, intent(out) :: pError    !
  !
  !print *, 'fits_write_primary_data'
  pError = .false.
end subroutine fits_write_primary_data
!
subroutine fits_write_primary(pCheck,pError)
  use gbl_message
  use classcore_interfaces, except_this=>fits_write_primary
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  logical, intent(in)  :: pCheck    !
  logical, intent(out) :: pError    !
  ! Local
  character(len=*), parameter :: proc='FITS'
  !
  !print *, 'fits_write_primary'
  call fits_write_primary_header(pCheck,pError)
  if (pError) then
     call class_message(seve%e,proc,'Could not write primary header.')
     return
  endif
  !
  call fits_write_primary_data(pError)
  if (pError) then
     call class_message(seve%e,proc,'Could not write primary data.')
     return
  endif
end subroutine fits_write_primary
!
!==============================================================================
!==============================  Binary tables  ===============================
!==============================================================================
!
subroutine fits_put_column(iCol,sName,iNbElem,sFormat,sUnit,pCheck,pError)
  use gbl_message
  use classcore_interfaces, except_this=>fits_put_column
  !---------------------------------------------------------------------
  ! @ private
  ! Binary tables
  !---------------------------------------------------------------------
  integer,          intent(in)  :: iCol     !
  character(len=*), intent(in)  :: sName    !
  integer,          intent(in)  :: iNbElem  !
  character(len=*), intent(in)  :: sFormat  !
  character(len=*), intent(in)  :: sUnit    !
  logical,          intent(in)  :: pCheck   !
  logical,          intent(out) :: pError   !
  ! Local
  character(len=*), parameter :: proc='FITS'
  character(len=3) :: sCol
  character(len=32) :: tmpformat  ! Arbitrary long
  character(len=8) :: sFullFormat  ! Final length desired. Can we use more than 8 chars?
  !
  write(sCol,'(i0)') iCol
  !
  write(tmpformat,'(I0,A)') iNbElem,sFormat
  if (len_trim(tmpformat).gt.8) then
    call class_message(seve%e,proc,'Output format '//trim(tmpformat)//' is larger than 8 characters')
    pError = .true.
    goto 98
  endif
  sFullFormat = tmpformat
  !
  call fits_put_string('TTYPE'//sCol,sName      ,'',pCheck,pError)
  if (pError) goto 98
  call fits_put_string('TFORM'//sCol,sFullFormat,'',pCheck,pError)
  if (pError) goto 98
  if (sUnit.ne.'') then
    call fits_put_string('TUNIT'//sCol,sUnit,'',pCheck,pError)
    if (pError) goto 98
  endif
  return
  !
  ! Errors.
98 continue
  call class_message(seve%e,proc,'Could not write column description into the bintable header.')
end subroutine fits_put_column
!
subroutine fits_write_binheader(set,pCheck,user_function,pError)
  use gbl_constant     ! vel_lsr
  use gbl_message
  use phys_const
  use gkernel_interfaces
  use classcore_interfaces, except_this=>fits_write_binheader
  use class_types      ! type (observation)
  use class_index      ! cx
  use class_fits       ! i_* columns
  !---------------------------------------------------------------------
  ! @ private
  ! LAS
  ! Binary tables
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)  :: set            !
  logical,             intent(in)  :: pCheck         !
  logical,             external    :: user_function  !
  logical,             intent(out) :: pError         !
  ! Local
  character(len=*), parameter :: proc='FITS'
  type (observation)   :: obs
  integer              :: iDay,iMonth,iYear
  character(len=80) :: sDate
  integer(kind=4)      :: iCol,velref
  real(kind=4)         :: posang,el,az,equinox
  real(kind=8) :: ra,dec
  character(len=8) :: value,velkey,specsys
  integer(kind=4), parameter :: epsr4=1e-7
  character(len=4), parameter :: pcode(0:mproj) =  &
    (/ '    ','-TAN','-SIN','-ARC','-STG','lamb',  &
       '-AIT','-GLS','-SFL','-MOL','-NCP','-CAR' /)
  !
  pError = .false.
  !
  !print *,'fits_write_binheader'
  ! Mandatory keywords.
  call fits_put_string('XTENSION','BINTABLE','',pCheck,pError)
  if (pError) goto 98
  call fits_put_inte('BITPIX' ,    8,'',pCheck,pError)
  if (pError) goto 98
  call fits_put_inte('NAXIS'  ,    2,'',pCheck,pError)
  if (pError) goto 98
  call fits_put_inte('NAXIS1' ,fits%cols%desc%lrow,'',pCheck,pError)
  if (pError) goto 98
  call fits_put_inte('NAXIS2' ,fits%cols%desc%nrows,'',pCheck,pError)
  if (pError) goto 98
  call fits_put_inte('PCOUNT' ,    0,'',pCheck,pError)
  if (pError) goto 98
  call fits_put_inte('GCOUNT' ,    1,'',pCheck,pError)
  if (pError) goto 98
  call fits_put_inte('TFIELDS',fits%cols%desc%ncols,'',pCheck,pError)
  if (pError) goto 98
  !
  ! Less mandatory keywords.
  call fits_put_string('EXTNAME','MATRIX','',pCheck,pError)
  if (pError) goto 98
  call fits_put_inte   ('EXTVER' , 1,'',pCheck,pError)
  if (pError) goto 98
  !
  ! Initialize the observation.
  call init_obs(obs)
  !
  ! Get observation 1 to fill keywords.
  call rheader(set,obs,cx%ind(1),user_function,pError)
  if (pError) then
     call class_message(seve%e,proc,'Bintable header writing, could not read header.')
     return
  endif
  !
  ! Axis.
  call fits_put_inte('MAXIS' ,4,'',pCheck,pError)
  if (pError) goto 98
  if (obs%head%gen%kind.eq.kind_spec) then
     ! Kind of observation : spectrum.
     call fits_put_inte('MAXIS1',obs%head%spe%nchan,'',pCheck,pError)
     if (pError) goto 98
     call fits_put_inte('MAXIS2',1,'',pCheck,pError)
     if (pError) goto 98
     call fits_put_inte('MAXIS3',1,'',pCheck,pError)
     if (pError) goto 98
     posang = 0
  elseif (obs%head%gen%kind.eq.kind_cont) then
     ! Kind of observation : continuum.
     call fits_put_inte('MAXIS1',1,'',pCheck,pError)
     if (pError) goto 98
     posang = mod(dble(obs%head%dri%apos),2.d0 * pi)
     ! Should check here if PI/2 rotations can bring scan direction along one of
     ! the main axes
     if (posang.gt.(pi - epsr4)) then
        posang = posang - pi
        obs%head%dri%ares  = -obs%head%dri%ares
     endif
     if (abs(posang).le.epsr4) posang = 0
     ! IF (ABS(POSANG-PI/2).LE.EPSR4) THEN
     ! ELSE		! Not along an axis: will have to use CROTA
     ! ENDIF
     call fits_put_inte('MAXIS2',obs%head%dri%npoin,'',pCheck,pError)
     if (pError) goto 98
     call fits_put_inte('MAXIS3',1,'',pCheck,pError)
     if (pError) goto 98
  else
     if (obs%head%gen%kind.eq.kind_sky) then
        ! Kind of observation : Skydip.
        call class_message(seve%e,proc,'Skydips not yet supported.')
     else
        ! Kind of observation : Unknown.
        call class_message(seve%e,proc,'Unknown data type. Corrupted data file.')
     endif
     pError = .true.
     return
  endif
  call fits_put_inte('MAXIS4',1,'',pCheck,pError)
  if (pError) goto 98
  !
  ! Axis 1: frequency.
  ! ATTENTION ! Only freq supported.
  call fits_put_string('CTYPE1','FREQ','',pCheck,pError)
  if (pError) goto 98
  call fits_put_dble('CRVAL1',0.d0,'[Hz]',pCheck,pError)
  if (pError) goto 98
  if (fits%cols%posi%wave.eq.0) then
     call fits_put_dble('CDELT1',obs%head%spe%fres * 1.d6,'[Hz]',pCheck,pError)
  else
     call fits_put_dble('CDELT1',1.d0,'Overriden by WAVE column',pCheck,pError)
  endif
  if (pError) goto 98
  call fits_put_dble('CRPIX1',obs%head%spe%rchan      ,'',pCheck,pError)
  if (pError) goto 98
  !
  ! Axis 2: RA.
  if (obs%head%pos%system.eq.type_ga) then
    value = 'GLON'//pcode(obs%head%pos%proj)
  else  ! Equatorial, ICRS, or...?
    value = 'RA--'//pcode(obs%head%pos%proj)
  endif
  call fits_put_string('CTYPE2',value,'',pCheck,pError)
  if (pError) goto 98
  if (fits%cols%posi%crval(2).eq.0) then
     call fits_put_dble('CRVAL2',obs%head%pos%lam*180.d0/pi,'[deg]',pCheck,pError)
  else
     call fits_put_simple('CRVAL2',0.e0,'Overriden by CRVAL2 column',pCheck,pError)
  endif
  if (pError) goto 98
  if (fits%cols%posi%cdelt(2).eq.0) then
     call fits_put_dble('CDELT2',obs%head%pos%lamof*180.d0/pi,'[deg]',pCheck,pError)
  else
     call fits_put_simple('CDELT2',0.e0,'Overriden by CDELT2 column.',pCheck,pError)
  endif
  if (pError) goto 98
  call fits_put_dble('CRPIX2',0.d0                      ,'',pCheck,pError)
  if (pError) goto 98
  !
  ! Axis 3: Dec.
  if (obs%head%pos%system.eq.type_ga) then
    value = 'GLAT'//pcode(obs%head%pos%proj)
  else  ! Equatorial, ICRS, or...?
    value = 'DEC-'//pcode(obs%head%pos%proj)
  endif
  call fits_put_string('CTYPE3',value,'',pCheck,pError)
  if (pError) goto 98
  if (fits%cols%posi%crval(3).eq.0) then
     call fits_put_dble('CRVAL3',obs%head%pos%bet*180.d0/pi,'[deg]',pCheck,pError)
  else
     call fits_put_simple('CRVAL3',0.e0,'Overriden by CRVAL3 column',pCheck,pError)
  endif
  if (pError) goto 98
  if (fits%cols%posi%cdelt(3).eq.0) then
     call fits_put_dble('CDELT3',obs%head%pos%betof*180.d0/pi,'[deg]',pCheck,pError)
  else
     call fits_put_simple('CDELT3',0.e0,'Overriden by CDELT3 column',pCheck,pError)
  endif
  if (pError) goto 98
  call fits_put_simple('CRPIX3',0.e0                      ,'',pCheck,pError)
  if (pError) goto 98
  !
  ! Axis 4: Stokes
  call fits_put_string('CTYPE4','STOKES','',pCheck,pError)
  if (pError) goto 98
  call fits_put_simple('CRVAL4',0.e0,'',pCheck,pError)
  if (pError) goto 98
  call fits_put_simple('CDELT4',0.e0,'',pCheck,pError)
  if (pError) goto 98
  call fits_put_simple('CRPIX4',0.e0,'',pCheck,pError)
  if (pError) goto 98
  !
  ! Keywords instead of columns.
  if (fits%cols%posi%scan.eq.0) then
     call fits_put_long('SCAN'   ,obs%head%gen%scan   ,'',pCheck,pError)
     if (pError) goto 98
  endif
  !
  if (fits%cols%posi%subscan.eq.0) then
     call fits_put_inte('SUBSCAN',obs%head%gen%subscan,'',pCheck,pError)
     if (pError) goto 98
  endif
  !
  if (fits%cols%posi%line.eq.0) then
     call fits_put_string('LINE'   ,obs%head%spe%line,'',pCheck,pError)
     if (pError) goto 98
  endif
  !
  if (fits%cols%posi%object.eq.0) then
     call fits_put_string('OBJECT' ,obs%head%pos%sourc,'',pCheck,pError)
     if (pError) goto 98
  endif
  !
  if (fits%cols%posi%telesc.eq.0) then
     call fits_put_string('TELESCOP' ,obs%head%gen%teles,'',pCheck,pError)
     if (pError) goto 98
  endif
  !
  if (fits%cols%posi%tsys.eq.0) then
     call fits_put_simple('TSYS',obs%head%gen%tsys,'[K]',pCheck,pError)
     if (pError) goto 98
  endif
  !
  ! Always put a zero valued FOFFSET for backward compatibility
  call fits_put_dble('FOFFSET',0.d0,'',pCheck,pError)
  if (pError) goto 98
  !
  if (fits%cols%posi%restfreq.eq.0) then ! Converted from MHz to Hz => *1d6.
     call fits_put_dble('RESTFREQ',obs%head%spe%restf * 1d6,'[Hz]',pCheck,pError)
     if (pError) goto 98
  endif
  !
  if (fits%cols%posi%imagfreq.eq.0) then ! Converted from MHz to Hz => *1d6.
     call fits_put_dble('IMAGFREQ',obs%head%spe%image * 1d6,'[Hz]',pCheck,pError)
     if (pError) goto 98
  endif
  !
  call tofits_specsys(obs%head,velkey,velref,specsys,pError)
  if (pError) goto 98
  if (fits%cols%posi%velocity.eq.0 .and. velkey.ne.'') then
     if (fits%cols%posi%specsys.gt.0)  velkey = 'VELOCITY'  ! Several specsys => use generic velocity name
     call fits_put_dble(velkey,obs%head%spe%voff*1d3,'[m/s] Velocity of reference channel',pCheck,pError)
     if (pError) goto 98
  endif
  if (fits%cols%posi%specsys.eq.0 .and. specsys.ne.'') then
     call fits_put_string('SPECSYS',trim(specsys),'Reference frame',pCheck,pError)
     if (pError) goto 98
  endif
  if (fits%cols%posi%velref.eq.0 .and. velref.gt.0) then
     call fits_put_inte('VELREF',velref,'>256 RADIO, 1 LSR 2 HEL 3 OBS',pCheck,pError)
     if (pError) goto 98
  endif
  !
  if (fits%cols%posi%veldef.eq.0) then
     if (obs%head%spe%vtype.eq.vel_lsr) then
        call fits_put_string('VELDEF','RADI-LSR','',pCheck,pError)
     elseif (obs%head%spe%vtype.eq.vel_hel) then
        call fits_put_string('VELDEF','RADI-HEL','',pCheck,pError)
     elseif (obs%head%spe%vtype.eq.vel_obs) then
        call fits_put_string('VELDEF','RADI-OBS','',pCheck,pError)
     elseif (obs%head%spe%vtype.eq.vel_ear) then
        call fits_put_string('VELDEF','RADI-EAR','',pCheck,pError)
     else
        call class_message(seve%i,'fits_write_binheader','Unkn. vel referential.')
     endif
     if (pError) goto 98
  endif
  !
  if (fits%cols%posi%deltav.eq.0) then
     call fits_put_dble('DELTAV',obs%head%spe%vres*1d3,'[m/s]',pCheck,pError)
     if (pError) goto 98
  endif
  !
  call tofits_radesys(set,obs%head,value,equinox,ra,dec,pError)
  if (pError) goto 98
  if (fits%cols%posi%radesys.eq.0) then
    if (value.ne.'') then
      call fits_put_string('RADESYS',value,'',pCheck,pError)
      if (pError) goto 98
    endif
  endif
  if (fits%cols%posi%equinox.eq.0) then
    if (equinox.ne.0.) then
      call fits_put_simple('EQUINOX',equinox,'',pCheck,pError)
      if (pError) goto 98
    endif
  endif
  !
  if (fits%cols%posi%tau_atm.eq.0) then
     call fits_put_simple('TAU-ATM',obs%head%gen%tau,'',pCheck,pError)
     if (pError) goto 98
  endif
  !
  if (fits%cols%posi%h2omm.eq.0) then
     call fits_put_simple('MH2O',obs%head%cal%h2omm,'[mm]',pCheck,pError)
     if (pError) goto 98
  endif
  !
  if (fits%cols%posi%tamb.eq.0) then
     call fits_put_simple('TOUTSIDE',obs%head%cal%tamb,'[K]',pCheck,pError)
     if (pError) goto 98
  endif
  !
  if (fits%cols%posi%pamb.eq.0) then
     call fits_put_simple('PRESSURE',obs%head%cal%pamb,'[hPa]',pCheck,pError)
     if (pError) goto 98
  endif
  !
  if (fits%cols%posi%tchop.eq.0) then
     call fits_put_simple('TCHOP',obs%head%cal%tchop,'[K]',pCheck,pError)
     if (pError) goto 98
  endif
  !
  if (fits%cols%posi%tcold.eq.0) then
     call fits_put_simple('TCOLD',obs%head%cal%tcold,'[K]',pCheck,pError)
     if (pError) goto 98
  endif
  !
  if (fits%cols%posi%elevatio.eq.0) then
     el = obs%head%gen%el*180.d0/pi
     call fits_put_simple('ELEVATIO',el,'[deg]',pCheck,pError)
     if (pError) goto 98
  endif
  !
  if (fits%cols%posi%azimuth.eq.0) then
     az = obs%head%gen%az*180.d0/pi
     call fits_put_simple('AZIMUTH',az,'[deg]',pCheck,pError)
     if (pError) goto 98
  endif
  !
  if (fits%cols%posi%gainimag.eq.0) then
     call fits_put_simple('GAINIMAG',obs%head%cal%gaini,'',pCheck,pError)
     if (pError) goto 98
  endif
  !
  if (fits%cols%posi%beameff.eq.0) then
     call fits_put_simple('BEAMEFF',obs%head%cal%beeff,'',pCheck,pError)
     if (pError) goto 98
  endif
  !
  if (fits%cols%posi%forweff.eq.0) then
     call fits_put_simple('FORWEFF',obs%head%cal%foeff,'',pCheck,pError)
     if (pError) goto 98
  endif
  !
  if (fits%cols%posi%dobs.eq.0) then
     call gag_jdat(obs%head%gen%dobs,iDay,iMonth,iYear)
     if (iYear.lt.1999) then
        iYear = mod(iYear,100)
        write (sDate,10) iDay,iMonth,iYear
        call fits_put_string('DATE-OBS',sDate(1:8),'',pCheck,pError)
        if (pError) goto 98
     else
        write (sDate,11) iYear,iMonth,iDay,'00:00:00.000'
        call fits_put_string('DATE-OBS',sDate(1:23),'',pCheck,pError)
        if (pError) goto 98
     endif
  endif
  !
  if (fits%cols%posi%dred.eq.0) then
     call gag_jdat(obs%head%gen%dred,iDay,iMonth,iYear)
     !print *,'Keyword DATE-RED : ',iDay,iMonth,iYear,'::',obs%head%gen%dred
     if (iYear.lt.1999) then
        iYear = mod(iYear,100)
        write (sDate,10) iDay,iMonth,iYear
        call fits_put_string('DATE-RED',sDate(1:8),'',pCheck,pError)
        if (pError) goto 98
     else
        write (sDate,11) iYear,iMonth,iDay,'00:00:00.000'
        call fits_put_string('DATE-RED',sDate(1:23),'',pCheck,pError)
        if (pError) goto 98
     endif
  endif
  !
  if (fits%cols%posi%ut.eq.0) then
     call fits_put_dble('UT',obs%head%gen%ut*3600*12/pi,'[s]',pCheck,pError)
     if (pError) goto 98
  endif
  !
  if (fits%cols%posi%lst.eq.0) then
     call fits_put_dble('LST',obs%head%gen%st*3600*12/pi,'[s]',pCheck,pError)
     if (pError) goto 98
  endif
  !
  if (fits%cols%posi%time.eq.0) then
     call fits_put_simple('OBSTIME',obs%head%gen%time,'[s]',pCheck,pError)
     if (pError) goto 98
  endif
  !
  ! Resolution information (if available)
  if (obs%head%presec(class_sec_res_id)) then
    if (fits%cols%posi%bmaj.eq.0) then
      call fits_put_simple('BMAJ',real(obs%head%res%major*deg_per_rad,kind=4),  &
        '[deg] Beam major axis',pCheck,pError)
      if (pError) goto 98
    endif
    if (fits%cols%posi%bmin.eq.0) then
      call fits_put_simple('BMIN',real(obs%head%res%minor*deg_per_rad,kind=4),  &
        '[deg] Beam minor axis',pCheck,pError)
      if (pError) goto 98
    endif
    if (fits%cols%posi%bpa.eq.0) then
      call fits_put_simple('BPA',real(obs%head%res%posang*deg_per_rad,kind=4),  &
        '[deg] Beam position angle',pCheck,pError)
      if (pError) goto 98
    endif
  endif
  !
  ! Description of the columns.
  iCol = 1
  !
  if (fits%cols%posi%axis(1).gt.0) then
     call fits_put_column(iCol,'MAXIS1',1,'J','',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%scan.gt.0) then
     ! NB: declare as J format (I*4), while obs%head%gen%scan is I*8 (see
     ! after when we write the column)
     call fits_put_column(iCol,'SCAN',1,'J','',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%subscan.gt.0) then
     call fits_put_column(iCol,'SUBSCAN',1,'J','',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif

  if (fits%cols%posi%line.gt.0) then
     call fits_put_column(iCol,'LINE',12,'A','',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%object.gt.0) then
     call fits_put_column(iCol,'OBJECT',12,'A','',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%telesc.gt.0) then
     call fits_put_column(iCol,'TELESCOP',12,'A','',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%crval(2).gt.0) then
     call fits_put_column(iCol,'CRVAL2',1,'E','',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%crval(3).gt.0) then
     call fits_put_column(iCol,'CRVAL3',1,'E','',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%cdelt(2).gt.0) then
     call fits_put_column(iCol,'CDELT2',1,'E','',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%cdelt(3).gt.0) then
     call fits_put_column(iCol,'CDELT3',1,'E','',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%tsys.gt.0) then
     call fits_put_column(iCol,'TSYS',1,'E','K',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%restfreq.gt.0) then
     call fits_put_column(iCol,'RESTFREQ',1,'E','Hz',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%imagfreq.gt.0) then
     call fits_put_column(iCol,'IMAGFREQ',1,'E','Hz',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%velocity.gt.0) then
     call fits_put_column(iCol,'VELOCITY',1,'E','m/s',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%specsys.gt.0) then
     call fits_put_column(iCol,'SPECSYS',8,'A','',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%velref.gt.0) then
     call fits_put_column(iCol,'VELREF',1,'J','',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%veldef.gt.0) then
     call fits_put_column(iCol,'VELDEF',12,'A','',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%deltav.gt.0) then
     call fits_put_column(iCol,'DELTAV',1,'E','m/s',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%tau_atm.gt.0) then
     call fits_put_column(iCol,'TAU-ATM',1,'E','',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%h2omm.gt.0) then
     call fits_put_column(iCol,'MH2O',1,'E','mm',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%tamb.gt.0) then
     call fits_put_column(iCol,'TOUTSIDE',1,'E','K',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%pamb.gt.0) then
     call fits_put_column(iCol,'PRESSURE',1,'E','hPa',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%tchop.gt.0) then
     call fits_put_column(iCol,'TCHOP',1,'E','K',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%tcold.gt.0) then
     call fits_put_column(iCol,'TCOLD',1,'E','K',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%elevatio.gt.0) then
     call fits_put_column(iCol,'ELEVATIO',1,'E','deg',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%azimuth.gt.0) then
     call fits_put_column(iCol,'AZIMUTH',1,'E','deg',pCheck,pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%gainimag.gt.0) then
     call fits_put_column(iCol,'GAINIMAG', 1, 'E', '',pCheck, pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%beameff.gt.0) then
     call fits_put_column(iCol, 'BEAMEFF', 1, 'E', '',pCheck, pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%forweff.gt.0) then
     call fits_put_column(iCol, 'FORWEFF', 1, 'E', '',pCheck, pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%radesys.gt.0) then
     call fits_put_column(iCol, 'RADESYS', 8, 'A', '',pCheck, pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%equinox.gt.0) then
     call fits_put_column(iCol, 'EQUINOX', 1, 'E', '',pCheck, pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%dobs.gt.0) then
     call fits_put_column(iCol, 'DATE-OBS', 23, 'A', '',pCheck, pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%dred.gt.0) then
     call fits_put_column(iCol, 'DATE-RED', 23, 'A', '',pCheck, pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%ut.gt.0) then
     call fits_put_column(iCol, 'UT', 1, 'D', 's',pCheck, pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%lst.gt.0) then
     call fits_put_column(iCol, 'LST', 1, 'D', 's',pCheck, pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%time.gt.0) then
     call fits_put_column(iCol, 'OBSTIME', 1, 'E', 's',pCheck, pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%bmaj.gt.0) then
     call fits_put_column(iCol, 'BMAJ', 1, 'E', 'deg',pCheck, pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%bmin.gt.0) then
     call fits_put_column(iCol, 'BMIN', 1, 'E', 'deg',pCheck, pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%bpa.gt.0) then
     call fits_put_column(iCol, 'BPA', 1, 'E', 'deg',pCheck, pError)
     if (pError) goto 98
     iCol = iCol + 1
  endif
  !
  ! Data.
  !
  if (fits%cols%posi%matrix.gt.0) then
     call fits_put_column(iCol, 'SPECTRUM', fits%head%desc%axis(1), 'E', '',pCheck, pError)
     if (pError) goto 98
     !write (sCol, '(i3)') iCol
     !sCol = AdjustL(sCol)
     !write (sLine, '(i6)') fits%head%desc%axis(1)
     !call fits_put_string('TTYPE'//sCol, 'SPECTRUM', '', pCheck, pError)
     !call fits_put_string('TFORM'//sCol, Trim(AdjustL(sLine))//'E', '', pCheck, pError)
     iCol = iCol + 1
  endif
  !
  if (fits%cols%posi%wave.gt.0) then
     call fits_put_column(iCol, 'WAVE', fits%head%desc%axis(1), 'E', '',pCheck, pError)
     if (pError) goto 98
     !write (sCol, '(i3)') iCol
     !sCol = AdjustL(sCol)
     !write (sLine, '(i6)') fits%head%desc%axis(1)
     !call fits_put_string('TTYPE'//sCol, 'WAVE', '', pCheck, pError)
     !call fits_put_string('TFORM'//sCol, Trim(AdjustL(sLine))//'E', '', pCheck, pError)
     iCol = iCol + 1
  endif
  !
  ! End of the header.
  call fits_put_novalue('END', '', pCheck, pError)
  if (pError) goto 98
  !
  call gfits_flush_header(pError)
  if (pError) then
     call class_message(seve%e,proc, 'Could not flush the bintable header properly.')
     return
  endif
  !
  ! Free memory.
  call free_obs(obs)
  return
  !
  ! Errors.
98 continue
  call class_message(seve%e,proc, 'Could not write card image into the bintable header.')
  return

10 format(i2.2,'/',i2.2,'/',i2.2) ! Date 31/12/00
11 format(i4.4,'-',i2.2,'-',i2.2,'T',a12) ! Date 2000-12-31T23:59:59.999
end subroutine fits_write_binheader
!
subroutine fits_write_bindata(set,pCheck,user_function,pError)
  use gbl_constant ! vel_lsr
  use gbl_format   ! fmt_i4...
  use phys_const
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>fits_write_bindata
  use class_types
  use class_index  ! cx
  use class_fits   ! i_* columns, nrows...
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)  :: set            !
  logical,             intent(in)  :: pCheck         !
  logical,             external    :: user_function  !
  logical,             intent(out) :: pError         !
  ! Local
  character(len=*), parameter :: proc='FITS'
  type (observation)   :: obs
  integer (kind = 1), allocatable :: RowBuffer(:)
  integer (kind = 4), allocatable :: Wave(:)
  integer :: ier,iRow,velref
  integer(kind=size_length) :: icol,ndata
  character(len=12) :: sLine
  character(len=8) :: radesys,specsys,velkey
  real(kind=4) :: fTmpSngl,az,el,equinox
  real(kind=8) :: ra,dec
  character(len=80) :: sDate
  integer              :: iDay, iMonth,iYear
  integer(kind=1) :: tmpbytes(23)
  integer(kind=4) :: scan
  integer(kind=4), parameter :: epsr4=1e-7
  !
  !print *, 'fits_write_bindata'
  if (fits%cols%desc%lrow.gt.0) then
     allocate(RowBuffer(fits%cols%desc%lrow),stat = ier)
     if (ier.eq.0) then
        ! Initialize the observation.
        call init_obs(obs)
        do iRow = 1,fits%cols%desc%nrows
           ! Get the observation from the index.
           call rheader(set,obs,cx%ind(iRow),user_function,pError)
           !
           ! Fill the cells of the row.
           if (fits%cols%posi%axis(1).gt.0) then
              call put_item(obs%head%spe%nchan,1,RowBuffer(fits%cols%posi%axis(1)),fmt_i4,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%scan.gt.0) then
              ! Write scan number as J format (I*4) to avoid breaking our fillers
              ! (and those of external users)
              call i8toi4_fini(obs%head%gen%scan,scan,1,pError)
              if (pError) then
                call class_message(seve%e,proc,'Scan number is too large')
                goto 98
              endif
              call put_item(scan,1,RowBuffer(fits%cols%posi%scan),fmt_i4,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%subscan.gt.0) then
              call put_item(obs%head%gen%subscan,1,RowBuffer(fits%cols%posi%subscan),fmt_i4,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%line.gt.0) then
              call chtoby(obs%head%spe%line,tmpbytes,12)
              call put_item(tmpbytes,12,RowBuffer(fits%cols%posi%line),1,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%object.gt.0) then
              call chtoby(obs%head%pos%sourc,tmpbytes,12)
              call put_item(tmpbytes,12,RowBuffer(fits%cols%posi%object),1,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%telesc.gt.0) then
              call chtoby(obs%head%gen%teles,tmpbytes,12)
              call put_item(tmpbytes,12,RowBuffer(fits%cols%posi%telesc),1,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%crval(2).gt.0) then
              call put_item(sngl(obs%head%pos%lam*180.d0/pi), 1, RowBuffer(fits%cols%posi%crval(2)), fmt_r4, pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%crval(3).gt.0) then
              call put_item(sngl(obs%head%pos%bet*180.d0/pi), 1, RowBuffer(fits%cols%posi%crval(3)), fmt_r4, pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%cdelt(2).gt.0) then
              call put_item(sngl(obs%head%pos%lamof*180.d0/pi), 1, RowBuffer(fits%cols%posi%cdelt(2)), fmt_r4, pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%cdelt(3).gt.0) then
              call put_item(sngl(obs%head%pos%betof*180.d0/pi), 1, RowBuffer(fits%cols%posi%cdelt(3)), fmt_r4, pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%tsys.gt.0) then
              call put_item(obs%head%gen%tsys,1,RowBuffer(fits%cols%posi%tsys),fmt_r4,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%restfreq.gt.0) then ! Converted from MHz to Hz => *1d6.
              fTmpSngl = sngl(obs%head%spe%restf * 1d6)
              call put_item(fTmpSngl,1,RowBuffer(fits%cols%posi%restfreq),fmt_r4,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%imagfreq.gt.0) then ! Converted from MHz to Hz => *1d6.
              fTmpSngl = sngl(obs%head%spe%image * 1d6)
              call put_item(fTmpSngl,1,RowBuffer(fits%cols%posi%imagfreq),fmt_r4,pError)
              if (pError) goto 98
           endif
           !
           call tofits_specsys(obs%head,velkey,velref,specsys,pError)
           if (pError) goto 98
           if (fits%cols%posi%velocity.gt.0) then
              fTmpSngl = sngl(obs%head%spe%voff*1d3)
              call put_item(fTmpSngl,1,RowBuffer(fits%cols%posi%velocity),fmt_r4,pError)
              if (pError) goto 98
           endif
           if (fits%cols%posi%specsys.gt.0) then
              call chtoby(specsys,tmpbytes,8)
              call put_item(tmpbytes,8,RowBuffer(fits%cols%posi%specsys),1,pError)
              if (pError) goto 98
           endif
           if (fits%cols%posi%velref.gt.0) then
              call put_item(velref,1,RowBuffer(fits%cols%posi%velref),fmt_i4,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%veldef.gt.0) then
              if (obs%head%spe%vtype.eq.vel_lsr) then
                 sLine = 'RADI-LSR'
              elseif (obs%head%spe%vtype.eq.vel_hel) then
                 sLine = 'RADI-HEL'
              elseif (obs%head%spe%vtype.eq.vel_obs) then
                 sLine = 'RADI-OBS'
              elseif (obs%head%spe%vtype.eq.vel_ear) then
                 sLine = 'RADI-EAR'
              else
                 sLine = 'UNKNOWN'
              endif
              call chtoby(sLine,tmpbytes,12)
              call put_item(tmpbytes,12,RowBuffer(fits%cols%posi%veldef),1,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%deltav.gt.0) then
              ! From km/s to m/s (same as DELTAV in header, reverse done at read time)
              call put_item(sngl(obs%head%spe%vres*1.e3),1,RowBuffer(fits%cols%posi%deltav),fmt_r4,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%tau_atm.gt.0) then
              call put_item(obs%head%gen%tau,1,RowBuffer(fits%cols%posi%tau_atm),fmt_r4,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%h2omm.gt.0) then
              call put_item(obs%head%cal%h2omm,1,RowBuffer(fits%cols%posi%h2omm),fmt_r4,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%tamb.gt.0) then
              call put_item(obs%head%cal%tamb,1,RowBuffer(fits%cols%posi%tamb),fmt_r4,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%pamb.gt.0) then
              call put_item(obs%head%cal%pamb,1,RowBuffer(fits%cols%posi%pamb),fmt_r4,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%tchop.gt.0) then
              call put_item(obs%head%cal%tchop,1,RowBuffer(fits%cols%posi%tchop),fmt_r4,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%tcold.gt.0) then
              call put_item(obs%head%cal%tcold,1,RowBuffer(fits%cols%posi%tcold),fmt_r4,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%elevatio.gt.0) then
              el = obs%head%gen%el*180.d0/pi
              call put_item(el,1,RowBuffer(fits%cols%posi%elevatio),fmt_r4,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%azimuth.gt.0) then
              az = obs%head%gen%az*180.d0/pi
              call put_item(az,1,RowBuffer(fits%cols%posi%azimuth),fmt_r4,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%gainimag.gt.0) then
              call put_item(obs%head%cal%gaini,1,RowBuffer(fits%cols%posi%gainimag),fmt_r4,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%beameff.gt.0) then
              call put_item(obs%head%cal%beeff,1,RowBuffer(fits%cols%posi%beameff),fmt_r4,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%forweff.gt.0) then
              call put_item(obs%head%cal%foeff,1,RowBuffer(fits%cols%posi%forweff),fmt_r4,pError)
              if (pError) goto 98
           endif
           !
           call tofits_radesys(set,obs%head,radesys,equinox,ra,dec,pError)
           if (pError) goto 98
           if (fits%cols%posi%radesys.gt.0) then
              call chtoby(radesys,tmpbytes,8)
              call put_item(tmpbytes,8,RowBuffer(fits%cols%posi%radesys),1,pError)
              if (pError) goto 98
           endif
           if (fits%cols%posi%equinox.gt.0) then
              call put_item(equinox,1,RowBuffer(fits%cols%posi%equinox),fmt_r4,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%dobs.gt.0) then
              call gag_jdat(obs%head%gen%dobs,iDay,iMonth,iYear)
              if (pError) goto 98
              if (iYear.lt.1999) then
                 iYear = mod(iYear,100)
                 write (sDate,10) iDay,iMonth,iYear
                 !call fits_put_string('DATE-OBS',sDate(1:8),'',pCheck,pError)
                 call chtoby(sDate,tmpbytes,8)
                 call put_item(tmpbytes, 8,RowBuffer(fits%cols%posi%dobs)  ,1,pError)
                 if (pError) goto 98
                 call chtoby('               ',tmpbytes,15)
                 call put_item(tmpbytes,15,RowBuffer(fits%cols%posi%dobs)+8,1,pError)
                 if (pError) goto 98
              else
                 write (sDate,11) iYear,iMonth,iDay,'00:00:00.000'
                 !call fits_put_string('DATE-OBS',sDate(1:23),'',pCheck,pError)
                 call chtoby(sDate,tmpbytes,23)
                 call put_item(tmpbytes,23,RowBuffer(fits%cols%posi%dobs),1,pError)
                 if (pError) goto 98
              endif
           endif
           !
           if (fits%cols%posi%dred.gt.0) then
              call gag_jdat(obs%head%gen%dred,iDay,iMonth,iYear)
              if (iYear.lt.1999) then
                 iYear = mod(iYear,100)
                 write (sDate,10) iDay,iMonth,iYear
                 !call fits_put_string('DATE-RED',sDate(1:8),'',pCheck,pError)
                 call chtoby(sDate,tmpbytes,8)
                 call put_item(tmpbytes, 8,RowBuffer(fits%cols%posi%dred)  ,1,pError)
                 if (pError) goto 98
                 call chtoby('               ',tmpbytes,15)
                 call put_item(tmpbytes,15,RowBuffer(fits%cols%posi%dred)+8,1,pError)
                 if (pError) goto 98
              else
                 write (sDate,11) iYear,iMonth,iDay,'00:00:00.000'
                 !call fits_put_string('DATE-RED',sDate(1:23),'',pCheck,pError)
                 call chtoby(sDate,tmpbytes,23)
                 call put_item(tmpbytes,23,RowBuffer(fits%cols%posi%dred),1,pError)
                 if (pError) goto 98
              endif
           endif
           !
           if (fits%cols%posi%ut.gt.0) then
              call put_item(obs%head%gen%ut*3600*12/pi,1,RowBuffer(fits%cols%posi%ut),fmt_r8,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%lst.gt.0) then
              call put_item(obs%head%gen%st*3600*12/pi,1,RowBuffer(fits%cols%posi%lst),fmt_r8,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%matrix.gt.0) then
              ! Read the data and dump it
              call reallocate_obs(obs,obs%head%spe%nchan,pError)
              if (pError)  goto 98
              call rdata(set,obs,obs%head%spe%nchan,obs%spectre,pError)
              if (pError)  goto 98
              !
              ! Export the spectrum data (always in R*4 in the SPECTRUM column).
              ! Use 'real_to_real4' which deals correctly with bad values.
              ndata = obs%head%spe%nchan
              icol = 0
              call real_to_real4(                   &
                RowBuffer(fits%cols%posi%matrix),   &
                ndata,                              &  ! [words] (Used) size of buffer
                obs%spectre,                        &
                ndata,                              &
                icol,                               &  ! [words] Start position in buffer
                obs%cbad,                           &
                abs(obs%cbad)*epsr4)
              !
              if (fits%cols%posi%wave.gt.0) then
                 allocate(Wave(obs%head%spe%nchan),stat = ier)
                 if (ier.eq.0) then
                    call r8tor4(obs%datax,Wave,obs%head%spe%nchan)
                    call put_item(Wave,obs%head%spe%nchan,RowBuffer(fits%cols%posi%wave),fmt_r4,pError)
                    if (pError) goto 98
                    deallocate(Wave)
                 else
                    call class_message(seve%e,proc ,'Could not allocate memory to write the irregular frequency axis.')
                    goto 99
                 endif
              endif
           endif
           !
           if (fits%cols%posi%time.gt.0) then
              call put_item(obs%head%gen%time,1,RowBuffer(fits%cols%posi%time),fmt_r4,pError)
              if (pError) goto 98
           endif
           !
           if (fits%cols%posi%bmaj.gt.0) then
              call put_item(real(obs%head%res%major*deg_per_rad,kind=4),1,  &
                RowBuffer(fits%cols%posi%bmaj),fmt_r4,pError)
              if (pError) goto 98
           endif
           if (fits%cols%posi%bmin.gt.0) then
              call put_item(real(obs%head%res%minor*deg_per_rad,kind=4),1,  &
                RowBuffer(fits%cols%posi%bmin),fmt_r4,pError)
              if (pError) goto 98
           endif
           if (fits%cols%posi%bpa.gt.0) then
              call put_item(real(obs%head%res%posang*deg_per_rad,kind=4),1,  &
                RowBuffer(fits%cols%posi%bpa),fmt_r4,pError)
              if (pError) goto 98
           endif
           !
           ! Save the row into the FITS file.
           !----------------------------------
           call gfits_putbuf(RowBuffer,fits%cols%desc%lrow,pError)
           if (pError) then
              call class_message(seve%e,proc,'Could not save the bintable row in the FITS file.')
              goto 99
           endif
        enddo

        ! Free the memory.
        !------------------
        call free_obs(obs)
        deallocate(RowBuffer)
     else
        call class_message(seve%e,'fits_write_bindata','Allocation error.')
     endif
  endif
  !
  call gfits_flush_data(pError)
  if (pError) then
     call class_message(seve%e,proc,'Could not flush the end of the bintable.')
     return
  endif
  return
  !
  ! Errors.
  ! put_item error.
98 continue
  call class_message(seve%e,proc,'Could not properly fill the RowBuffer.')
  !
  ! Free the memory.
99 continue
  call free_obs(obs)
  if (allocated(RowBuffer)) then
     deallocate(RowBuffer)
  endif
  return
  !
  ! Formats.
10 format(i2.2,'/',i2.2,'/',i2.2) ! Date 31/12/00
11 format(i4.4,'-',i2.2,'-',i2.2,'T',a12) ! Date 2000-12-31T23:59:59.999
end subroutine fits_write_bindata
!
subroutine fits_write_bintables(set,pCheck,user_function,pError)
  use gbl_message
  use classcore_interfaces, except_this=>fits_write_bintables
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)  :: set            !
  logical,             intent(in)  :: pCheck         !
  logical,             external    :: user_function  !
  logical,             intent(out) :: pError         !
  ! Local
  character(len=*), parameter :: proc='FITS'
  !
  !print *,'fits_write_bintables'
  call fits_write_binheader(set,pCheck,user_function,pError)
  if (pError) then
     call class_message(seve%e,proc,'Could not write bintable header.')
     return
  endif
  !
  call fits_write_bindata(set,pCheck,user_function,pError)
  if (pError) then
     call class_message(seve%e,proc,'Could not write bintable data.')
     return
  endif
end subroutine fits_write_bintables
!
!==============================================================================
!================================  Whole FITS  ================================
!==============================================================================
!
subroutine fits_analyse_index(set,nomore,user_function,pError)
  use gbl_message
  use classcore_interfaces, except_this=>fits_analyse_index
  use class_types
  use class_index
  use class_fits
  !---------------------------------------------------------------------
  ! @ private
  ! Read entries in the INDEX
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)  :: set            !
  logical,             intent(out) :: nomore         !
  logical,             external    :: user_function  !
  logical,             intent(out) :: pError         !
  ! Local
  character(len=*), parameter :: proc='FITS'
  type (observation)   :: obs1,obs
  integer(kind=entry_length) :: iObs
  integer              :: iPos
  character(len=8) :: radesys,radesys1
  character(len=8) :: velkey,velkey1
  character(len=8) :: specsys,specsys1
  integer(kind=4) :: velref,velref1
  real(kind=4) :: equinox,equinox1
  real(kind=8) :: ra,dec
  !
  !print *, 'fits_analyse_index'
  ! Initialize the FITS variables.
  !--------------------------------
  !print *, 'Initialize the FITS variables.'
  call fits_reset(fits) ! From toclass.f90.
  !
  ! Trivia: index is empty,nothing to do.
  !----------------------------------------
  !print *, 'Trivia: index is empty, nothing to do.'
  if (cx%next.eq.1)  then
    call class_message(seve%w,proc,'Current index is empty. Nothing done.')
    nomore = .true.
    return
  else
    nomore = .false.
  endif
  !
  ! Initialize the observations.
  !------------------------------
  !print *, 'Initialize the observations.'
  call init_obs(obs1)
  call init_obs(obs)
  !
  ! Get the first observation.
  !----------------------------
  !print *, 'Get the first observation.'
  call rheader(set,obs1,cx%ind(1),user_function,pError)
  if (pError) then
     call class_message(seve%e,proc,'Index analysis failure, could not read header.')
     return
  endif
  !
  call tofits_radesys(set,obs1%head,radesys1,equinox1,ra,dec,pError)
  if (pError)  return
  call tofits_specsys(obs1%head,velkey1,velref1,specsys1,pError)
  if (pError)  return
  !
  fits%cols%desc%nrows = 1                     ! Nb of rows in Bintable.
  fits%head%desc%axis(1) = obs1%head%spe%nchan ! Max nb of channels.
  !
  ! Find the columns we will use.
  !-------------------------------
  !print *, 'Find the columns we will use.'
  do iObs = 2,cx%next-1
     ! Get the next observation.
     !---------------------------
     !print *, 'Get the next observation.'
     call rheader(set,obs,cx%ind(iObs),user_function,pError)
     if (pError) then
        call class_message(seve%e,proc,'Index analysis failure, could not read header.')
        return
     endif
     !
     fits%cols%desc%nrows = fits%cols%desc%nrows + 1 ! Nb of rows in Bintable.
     if (obs%head%spe%nchan.gt.fits%head%desc%axis(1)) then
        fits%head%desc%axis(1) = obs%head%spe%nchan
        !print *, "I've changed to ", fits%head%desc%axis(1) !$$$ DEBUG
     endif
     !
     ! Columns or keyword?
     !---------------------
     !print *, 'Columns or keyword?'
     if (obs%head%spe%nchan  .ne.obs1%head%spe%nchan  ) fits%cols%posi%axis(1)  = 1
     if (obs%head%gen%scan   .ne.obs1%head%gen%scan   ) fits%cols%posi%scan     = 1
     if (obs%head%gen%subscan.ne.obs1%head%gen%subscan) fits%cols%posi%subscan  = 1
     if (obs%head%spe%line   .ne.obs1%head%spe%line   ) fits%cols%posi%line     = 1
     if (obs%head%pos%sourc  .ne.obs1%head%pos%sourc  ) fits%cols%posi%object   = 1
     if (obs%head%gen%teles  .ne.obs1%head%gen%teles  ) fits%cols%posi%telesc   = 1
     if (obs%head%gen%tsys   .ne.obs1%head%gen%tsys   ) fits%cols%posi%tsys     = 1
     if (obs%head%spe%restf  .ne.obs1%head%spe%restf  ) fits%cols%posi%restfreq = 1
     if (obs%head%spe%image  .ne.obs1%head%spe%image  ) fits%cols%posi%imagfreq = 1
     call tofits_specsys(obs%head,velkey,velref,specsys,pError)
     if (pError)  return
     if (obs%head%spe%voff   .ne.obs1%head%spe%voff   ) fits%cols%posi%velocity = 1
     if (velref.ne.velref1)                             fits%cols%posi%velref   = 1
     if (specsys.ne.specsys1)                           fits%cols%posi%specsys  = 1
     if (obs%head%spe%vtype  .ne.obs1%head%spe%vtype  ) fits%cols%posi%veldef   = 1
     if (obs%head%spe%vres   .ne.obs1%head%spe%vres   ) fits%cols%posi%deltav   = 1
     if (obs%head%gen%tau    .ne.obs1%head%gen%tau    ) fits%cols%posi%tau_atm  = 1
   ! if (obs%head%           .ne.obs1%head%           ) fits%cols%posi%tauo2    = 1
   ! if (obs%head%           .ne.obs1%head%           ) fits%cols%posi%tauh2o   = 1
     if (obs%head%cal%h2omm  .ne.obs1%head%cal%h2omm  ) fits%cols%posi%h2omm    = 1
     if (obs%head%cal%tamb   .ne.obs1%head%cal%tamb   ) fits%cols%posi%tamb     = 1
     if (obs%head%cal%pamb   .ne.obs1%head%cal%pamb   ) fits%cols%posi%pamb     = 1
     if (obs%head%cal%tchop  .ne.obs1%head%cal%tchop  ) fits%cols%posi%tchop    = 1
     if (obs%head%cal%tcold  .ne.obs1%head%cal%tcold  ) fits%cols%posi%tcold    = 1
     if (obs%head%gen%el     .ne.obs1%head%gen%el     ) fits%cols%posi%elevatio = 1
     if (obs%head%gen%az     .ne.obs1%head%gen%az     ) fits%cols%posi%azimuth  = 1
     if (obs%head%cal%gaini  .ne.obs1%head%cal%gaini  ) fits%cols%posi%gainimag = 1
     if (obs%head%cal%beeff  .ne.obs1%head%cal%beeff  ) fits%cols%posi%beameff  = 1
     if (obs%head%cal%foeff  .ne.obs1%head%cal%foeff  ) fits%cols%posi%forweff  = 1
     call tofits_radesys(set,obs%head,radesys,equinox,ra,dec,pError)
     if (pError)  return
     if (radesys.ne.radesys1)                           fits%cols%posi%radesys  = 1
     if (equinox.ne.equinox1)                           fits%cols%posi%equinox  = 1
     if (obs%head%gen%dobs   .ne.obs1%head%gen%dobs   ) fits%cols%posi%dobs     = 1
     if (obs%head%gen%dred   .ne.obs1%head%gen%dred   ) fits%cols%posi%dred     = 1
     if (obs%head%gen%ut     .ne.obs1%head%gen%ut     ) fits%cols%posi%ut       = 1
     if (obs%head%gen%st     .ne.obs1%head%gen%st     ) fits%cols%posi%lst      = 1
   ! if (obs%head%           .ne.obs1%head%           ) fits%cols%posi%datamax  = 1
   ! if (obs%head%           .ne.obs1%head%           ) fits%cols%posi%datamin  = 1
     if (obs%head%gen%time   .ne.obs1%head%gen%time   ) fits%cols%posi%time     = 1
     if (obs%head%pos%lam    .ne.obs1%head%pos%lam    ) fits%cols%posi%crval(2) = 1
     if (obs%head%pos%bet    .ne.obs1%head%pos%bet    ) fits%cols%posi%crval(3) = 1
     if (obs%head%pos%lamof  .ne.obs1%head%pos%lamof  ) fits%cols%posi%cdelt(2) = 1
     if (obs%head%pos%betof  .ne.obs1%head%pos%betof  ) fits%cols%posi%cdelt(3) = 1
     if (obs%head%presec(class_sec_res_id)) then
       if (obs%head%res%major .ne.obs1%head%res%major ) fits%cols%posi%bmaj     = 1
       if (obs%head%res%minor .ne.obs1%head%res%minor ) fits%cols%posi%bmin     = 1
       if (obs%head%res%posang.ne.obs1%head%res%posang) fits%cols%posi%bpa      = 1
     endif
  enddo
  !
  ! Calculate offsets for these columns.
  !--------------------------------------
  !print *, 'Calculate offsets for these columns.'
  ipos = 1
  if (fits%cols%posi%axis(1).eq.1) then
     fits%cols%posi%axis(1) = ipos
     ipos = ipos+4 ! fmt_i4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%scan.eq.1) then
     fits%cols%posi%scan = ipos
     ipos = ipos+4 ! fmt_i8 in memory, but written as fmt_i4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%subscan.eq.1) then
     fits%cols%posi%subscan = ipos
     ipos = ipos+4 ! fmt_i4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%line.eq.1) then
     fits%cols%posi%line = ipos
     ipos = ipos+12 ! 12 (string)
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%object.eq.1) then
     fits%cols%posi%object = ipos
     ipos = ipos+12 ! 12 (string)
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%telesc.eq.1) then
     fits%cols%posi%telesc = ipos
     ipos = ipos+12 ! 12 (string)
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%crval(2).eq.1) then
     fits%cols%posi%crval(2) = iPos
     iPos = iPos + 4
     fits%cols%desc%ncols = fits%cols%desc%ncols + 1
  endif
  if (fits%cols%posi%crval(3).eq.1) then
     fits%cols%posi%crval(3) = iPos
     iPos = iPos + 4
     fits%cols%desc%ncols = fits%cols%desc%ncols + 1
  endif
  if (fits%cols%posi%cdelt(2).eq.1) then
     fits%cols%posi%cdelt(2) = iPos
     iPos = iPos + 4
     fits%cols%desc%ncols = fits%cols%desc%ncols + 1
  endif
  if (fits%cols%posi%cdelt(3).eq.1) then
     fits%cols%posi%cdelt(3) = iPos
     iPos = iPos + 4
     fits%cols%desc%ncols = fits%cols%desc%ncols + 1
  endif
  if (fits%cols%posi%tsys.eq.1) then
     fits%cols%posi%tsys = ipos
     ipos = ipos+4 ! fmt_r4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%restfreq.eq.1) then
     fits%cols%posi%restfreq = ipos
     ipos = ipos+4 ! fmt_r8
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%imagfreq.eq.1) then
     fits%cols%posi%imagfreq = ipos
     ipos = ipos+4 ! fmt_r8
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%velocity.eq.1) then
     fits%cols%posi%velocity = ipos
     ipos = ipos+4 ! fmt_r4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%velref.eq.1) then
     fits%cols%posi%velref = ipos
     ipos = ipos+4 ! fmt_i4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%specsys.eq.1) then
     fits%cols%posi%specsys = ipos
     ipos = ipos+8 ! 8 (string)
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%veldef.eq.1) then
     fits%cols%posi%veldef = ipos
     ipos = ipos+12 ! 12 (string)
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%deltav.eq.1) then
     fits%cols%posi%deltav = ipos
     ipos = ipos+4 ! fmt_r4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%tau_atm.eq.1) then
     fits%cols%posi%tau_atm = ipos
     ipos = ipos+4 ! fmt_r4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%h2omm.eq.1) then
     fits%cols%posi%h2omm = ipos
     ipos = ipos+4 ! fmt_r4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%tamb.eq.1) then
     fits%cols%posi%tamb = ipos
     ipos = ipos+4 ! fmt_r4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%pamb.eq.1) then
     fits%cols%posi%pamb = ipos
     ipos = ipos+4 ! fmt_r4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%tchop.eq.1) then
     fits%cols%posi%tchop = ipos
     ipos = ipos+4 ! fmt_r4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%tcold.eq.1) then
     fits%cols%posi%tcold = ipos
     ipos = ipos+4 ! fmt_r4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%elevatio.eq.1) then
     fits%cols%posi%elevatio = ipos
     ipos = ipos+4 ! fmt_r4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%azimuth.eq.1) then
     fits%cols%posi%azimuth = ipos
     ipos = ipos+4 ! fmt_r4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%gainimag.eq.1) then
     fits%cols%posi%gainimag = ipos
     ipos = ipos+4 ! fmt_r4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%beameff.eq.1) then
     fits%cols%posi%beameff = ipos
     ipos = ipos+4 ! fmt_r4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%forweff.eq.1) then
     fits%cols%posi%forweff = ipos
     ipos = ipos+4 ! fmt_r4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%radesys.eq.1) then
     fits%cols%posi%radesys = ipos
     ipos = ipos+8 ! 8 (string)
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%equinox.eq.1) then
     fits%cols%posi%equinox = ipos
     ipos = ipos+4 ! fmt_r4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%dobs.eq.1) then
     fits%cols%posi%dobs = ipos
     ipos = ipos+23 ! 23 (string)
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%dred.eq.1) then
     fits%cols%posi%dred = ipos
     ipos = ipos+23 ! 23 (string)
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%ut.eq.1) then
     fits%cols%posi%ut = ipos
     ipos = ipos+8 ! fmt_r8
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%lst.eq.1) then
     fits%cols%posi%lst = ipos
     ipos = ipos+8 ! fmt_r8
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%time.eq.1) then
     fits%cols%posi%time = ipos
     ipos = ipos+4 ! fmt_r8 but it does not work -> r4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%bmaj.eq.1) then
     fits%cols%posi%bmaj = ipos
     ipos = ipos+4  ! fmt_r4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%bmin.eq.1) then
     fits%cols%posi%bmin = ipos
     ipos = ipos+4  ! fmt_r4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  if (fits%cols%posi%bpa.eq.1) then
     fits%cols%posi%bpa = ipos
     ipos = ipos+4  ! fmt_r4
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  !
  ! Y Column.
  !-----------
  !print *, 'Y Column.'
  fits%cols%posi%matrix = iPos
  iPos = iPos+fits%head%desc%axis(1)*4 ! fmt_r4
  fits%cols%desc%ncols = fits%cols%desc%ncols+1
  !
  ! X column: for irregular frequency axis.
  !-----------------------------------------
  !print *, 'X column: for irregular frequency axis.'
  if (obs%head%presec(class_sec_xcoo_id)) then
     fits%cols%posi%wave = ipos
     ipos = ipos+fits%head%desc%axis(1)*4 ! fmt_r4 but r8 could be better.
     fits%cols%desc%ncols = fits%cols%desc%ncols+1
  endif
  !
  ! Length of the row in the BinTable.
  !------------------------------------
  !print *, 'Length of the row in the BinTable.'
  fits%cols%desc%lrow = ipos-1
  !
  ! Free memory.
  !--------------
  !print *, 'Free memory.'
  call free_obs(obs1)
  call free_obs(obs)
end subroutine fits_analyse_index
!
subroutine fits_write_index(set,pCheck,user_function,error)
  use gbl_message
  use classcore_interfaces, except_this=>fits_write_index
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Used to write the current index in INDEX mode
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)  :: set            !
  logical,             intent(in)  :: pCheck         !
  logical,             external    :: user_function  !
  logical,             intent(out) :: error          !
  ! Local
  character(len=*), parameter :: proc='FITS'
  !
  !print *, 'fits_write_index'
  call fits_write_primary(pCheck,error)
  if (error) then
     call class_message(seve%e,proc,'Could not write primary header.')
     return
  endif
  !
  call fits_write_bintables(set,pCheck,user_function,error)
  if (error) then
     call class_message(seve%e,proc,'Could not write bintables properly.')
     return
  endif
end subroutine fits_write_index
!
subroutine fits_save_index(set,pcheck,user_function,error)
  use gbl_message
  use classcore_interfaces, except_this=>fits_save_index
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! LAS
  ! Check the current INDEX
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)  :: set            !
  logical,             intent(in)  :: pcheck         !
  logical,             external    :: user_function  !
  logical,             intent(out) :: error          !
  ! Local
  character(len=*), parameter :: proc='FITS'
  logical :: nomore
  !
  call fits_analyse_index(set,nomore,user_function,error)
  if (error) then
     call class_message(seve%e,proc,'Index analysis error, cannot save the FITS file.')
     return
  endif
  if (nomore)  return
  !
  call fits_write_index(set,pCheck,user_function,error)
  if (error) then
     call class_message(seve%e,proc,'Error while saving the FITS file.')
     return
  endif
end subroutine fits_save_index
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
