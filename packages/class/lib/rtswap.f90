subroutine copy_obs(in,out,error)
  use gbl_message
  use classcore_interfaces, except_this=>copy_obs
  use class_types
  !-------------------------------------------------------------------
  ! @ public
  ! Copy IN into OUT observation
  !-------------------------------------------------------------------
  type(observation), intent(in)    :: in     ! In  observation
  type(observation), intent(inout) :: out    ! Out observation
  logical,           intent(out)   :: error  ! Error flag
  ! Local
  character(len=*), parameter :: rname='COPY_OBS'
  integer(kind=4) :: ndata
  !
  ! Initialization
  error = .false.
  !
  ! Sanity check
  if (.not.associated(in%data1)) then
     call class_message(seve%e,rname,'Observation to be copied is empty')
     error = .true.
     return
  endif
  !
  ! Reallocate OUT buffer if needed
  ndata = min(obs_nchan(in%head),size(in%datax))
  call reallocate_obs(out,ndata,error)
  if (error) return
  !
  ! Copy header
  out%head = in%head
  !
  ! Copy scalars
  out%cnchan = in%cnchan
  out%cbad   = in%cbad
  out%cimin  = in%cimin
  out%cimax  = in%cimax
  out%is_otf = in%is_otf
  ! out%is_R = DO NOT CHANGE!
  !
  ! Copy Arrays
  out%datax(1:ndata) = in%datax
  out%datas(1:ndata) = in%datas
  out%datai(1:ndata) = in%datai
  out%datav(1:ndata) = in%datav
  out%data1(1:ndata) = in%data1
  out%dataw(1:ndata) = in%dataw
  !
  ! User Section
  call copy_user(in%user,out%user,error)
  if (error)  return
  !
  ! Associated Arrays section
  call copy_assoc(in%assoc,out%assoc,error)
  if (error)  return
  !
  ! Set pointers
  out%spectre => out%data1
  !
end subroutine copy_obs
