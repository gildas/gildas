subroutine class_save(set,line,error)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_save
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! CLASS Support routine for command
  !
  ! SAVE  [Filename]
  !
  ! Save all current default on a CLASS procedure
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  character(len=*),    intent(in)    :: line   !
  logical,             intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: proc='SAVE'
  character(len=64) :: argum
  character(len=filename_length) :: file_save,mess
  integer :: nc,ier,lun
  !
  argum='SAVED'
  call sic_ch (line,0,1,argum,nc,.false.,error)
  if (error) return
  call sic_parse_file(argum,' ','.class',file_save)
  ier = sic_getlun(lun)
  if (ier.ne.1) then
     mess = 'Cannot open file '//file_save
     call class_message(seve%e,proc,mess)
     call class_message(seve%e,proc,'No logical unit left')
     error = .true.
     return
  endif
  !
  ier = sic_open(lun,file_save,'NEW',.false.)
  if (ier.ne.0) then
     mess = 'Cannot open file '//file_save
     call class_message(seve%e,proc,mess)
     call putios('         ',ier)
     error = .true.
  else
     call sas_save(set,lun,error)
     close(unit=lun)
     mess = 'Parameters saved on '//file_save
     call class_message(seve%e,proc,mess)
  endif
  call sic_frelun(lun)
end subroutine class_save
!
subroutine sas_save(set,lun,error)
  use gbl_constant
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>sas_save
  use class_setup_new
  use class_types
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command:
  !    SAVE
  ! Save the current CLASS parameters on a file opend on a Logical Unit
  ! Number
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  integer(kind=4),     intent(in)    :: lun    ! Fortran Logical Unit Number to use
  logical,             intent(inout) :: error  !
  ! Local
  integer(kind=4) :: i,nc
  character(len=12) :: arg,argum1,argum2,argum3,argum4
  character(len=256) :: chaine
  character(len=80) :: messa,mess
  character(len=1), parameter :: backslash=char(92)
  integer(kind=8), parameter :: maxi8=9223372036854775807_8
  !
  ! Setup parameters
  write(lun,1009) '!'
  write(lun,1009) '!  Saved CLASS parameters'
  write(lun,1009) '!'
  !
  ! angle
  mess = 'LAS'//backslash//'SET ANGLE'
  write(lun,1000) mess(1:lenc(mess)),class_setup_get_angle(),'! Angle unit'
  !
  ! align
  mess = 'LAS'//backslash//'SET ALIGN'
  messa = set%align//' '//set%alig2
  write(lun,1000) mess(1:lenc(mess)),messa(1:lenc(messa)),'! Align type for adding spectra'
  !
  ! bad
  mess = 'LAS'//backslash//'SET BAD'
  write(lun,1000) mess(1:lenc(mess)),set%bad,'! Bad channel checking mode'
  !
  ! base
  if (set%base.eq.-1) then
     mess = 'LAS'//backslash//'SET BASELINE'
     write(lun,1000) mess(1:lenc(mess)),'SINUS','! Sinusoidal baseline'
  else
     mess = 'LAS'//backslash//'SET BASELINE'
     write(lun,1001) mess(1:lenc(mess)),set%base,'! degree of polynomial'
  endif
  !
  ! coord
  if (set%coord.eq.0) then
     mess = 'LAS'//backslash//'SET SYSTEM'
     write(lun,1000) mess(1:lenc(mess)),'AUTO','! Coordinate system '
  elseif (set%coord.eq.type_eq) then
     mess = 'LAS'//backslash//'SET SYSTEM EQUATORIAL '
     write(lun,1005) mess(1:lenc(mess)),set%equinox,'! Coordinate system '
  else
     mess = 'LAS'//backslash//'SET SYSTEM'
     write(lun,1000) mess(1:lenc(mess)),'GALACTIC','! Coordinate system '
  endif
  !
  ! format
  mess = 'LAS'//backslash//'SET FORMAT'
  write(lun,1000) mess(1:lenc(mess)),set%heade,'! Default header format'
  !
  ! line
  mess = 'LAS'//backslash//'SET LINE'
  write(lun,1013) mess(1:lenc(mess)),set%line,'! Molecular transition'
  ! fit mask
  if (set%nmask.eq.0) then
     mess = 'LAS'//backslash//'SET MASK'
     write(lun,1000) mess(1:lenc(mess)),'NONE','! no fit mask'
  else
     if (set%kind.eq.kind_spec) then
        mess = 'LAS'//backslash//'SET MASK'
        write(chaine,1010) &
             mess(1:lenc(mess)),(set%mask1(i),set%mask2(i),i=1,set%nmask)
     elseif (set%kind.eq.kind_cont) then
        mess = 'LAS'//backslash//'SET MASK'
        write(chaine,1010)  mess(1:lenc(mess)),  &
          (set%mask1(i)*class_setup_get_fangle(),  &
           set%mask2(i)*class_setup_get_fangle(),i=1,set%nmask)
     endif
     call sic_noir(chaine,nc)
     write(lun,1009) chaine(1:nc)
  endif
  !
  ! match
  if (set%match) then
     call offsec(set,set%tole,arg)
     mess = 'LAS'//backslash//'SET MATCH'
     write(lun,1000) mess(1:lenc(mess)),arg(1:lenc(arg)),'! Position match tested.'
  else
     mess = 'LAS'//backslash//'SET NOMATCH'
     write(lun,1000) mess(1:lenc(mess)),' ','! Position match not tested.'
  endif
  !
  ! MODE X
  if (set%modex.eq.'T') then
     mess = 'LAS'//backslash//'SET MODE X'
     write(lun,1000) mess(1:lenc(mess)),   &
          &      'TOTAL','! Whole spectrum displayed'
  elseif  (set%modex.eq.'A') then
     mess = 'LAS'//backslash//'SET MODE X'
     write(lun,1000) mess(1:lenc(mess)),   &
          &      'AUTO','! Pre-determined scale for X'
  else
     mess = 'LAS'//backslash//'SET MODE X'
     write(lun,1002) mess(1:lenc(mess)),gux1,   &
          &      gux2,'! Fixed mode for X'
  endif
  !
  ! MODE Y
  if (set%modey.eq.'T') then
     mess = 'LAS'//backslash//'SET MODE Y'
     write(lun,1000) mess(1:lenc(mess)),   &
          &      'TOTAL','! Whole spectrum displayed'
  elseif  (set%modey.eq.'A') then
     mess = 'LAS'//backslash//'SET MODE Y'
     write(lun,1000) mess(1:lenc(mess)),   &
          &      'AUTO','! Pre-determined scale for Y'
  else
     mess = 'LAS'//backslash//'SET MODE Y'
     write(lun,1002) mess(1:lenc(mess)),   &
          &      guy1,guy2,'! Fixed mode for Y'
  endif
  !
  ! Number
  if (set%nume2.lt.maxi8) then
     mess = 'LAS'//backslash//'SET NUMBER'
     write(lun,1003) trim(mess),set%nume1,set%nume2,'! Scan number range.'
  else
     mess = 'LAS'//backslash//'SET NUMBER'
     write(lun,1012) trim(mess),set%nume1,' *','! Scan number range.'
  endif
  !
  ! Observed date
  call gag_todate(set%obse1,argum1,error)
  call gag_todate(set%obse2,argum2,error)
  mess = 'LAS'//backslash//'SET OBSERVED '   &
       &    //argum1(1:lenc(argum1))//' '//argum2(1:lenc(argum2))
  write(lun,1004) mess(1:lenc(mess)),'! Range of observ. dates.'
  !
  ! Offset-range
  call offsec(set,set%offs1,argum1)
  call offsec(set,set%offl1,argum2)
  call offsec(set,set%offs2,argum3)
  call offsec(set,set%offl2,argum4)
  mess = 'LAS'//backslash//'SET RANGE '//argum1(1:lenc(argum1))   &
       &    //' '//argum2(1:lenc(argum2))//' '//argum3(1:lenc(argum3))   &
       &    //' '//argum4(1:lenc(argum4))
  write(lun,1004) mess(1:lenc(mess)),'! Offset range.'
  !
  ! Plot
  mess = 'LAS'//backslash//'SET PLOT'
  write(lun,1000) mess(1:lenc(mess)),set%plot,'! plot type.'
  !
  ! Reduction date
  call gag_todate(set%redu1,argum1,error)
  call gag_todate(set%redu2,argum2,error)
  mess = 'LAS'//backslash//'SET REDUCED '//argum1(1:lenc(argum1))   &
       &    //' '//argum2(1:lenc(argum2))
  write(lun,1004) mess(1:lenc(mess)),'! Range of reduc. dates.'
  !
  ! Source
  mess = 'LAS'//backslash//'SET SOURCE'
  write(lun,1013) mess(1:lenc(mess)),set%sourc,'! Source name'
  !
  ! Telescope
  mess = 'LAS'//backslash//'SET TELESCOPE'
  write(lun,1013) mess(1:lenc(mess)),set%teles,'! Telescope name'
  !
  ! Type of data
  if (set%kind.eq.kind_spec) then
     mess = 'LAS'//backslash//'SET TYPE SPECTROSCOPY'
     write(lun,1004) mess(1:lenc(mess)),'! Data type'
  elseif (set%kind.eq.kind_cont) then
     mess = 'LAS'//backslash//'SET TYPE CONTINUUM'
     write(lun,1004) mess(1:lenc(mess)),'! Data type'
  elseif (set%kind.eq.kind_sky) then
     mess = 'LAS'//backslash//'SET TYPE SKYDIP'
     write(lun,1004) mess(1:lenc(mess)),'! Data type'
  elseif (set%kind.eq.kind_onoff) then
     mess = 'LAS'//backslash//'SET TYPE ONOFF'
     write(lun,1004) mess(1:lenc(mess)),'! Data type'
  else
     write(lun,1009) '! Unsupported kind of data'
  endif
  !
  ! unit of X axis
  mess = 'LAS'//backslash//'SET UNIT'
  write(lun,1000) mess(1:lenc(mess)),set%unitx(1),   &
       &    '! Unit of x axis (vel. freq. or chan.)'
  !
  ! weight
  mess = 'LAS'//backslash//'SET WEIGHT'
  write(lun,1000) mess(1:lenc(mess)),   &
       &    set%weigh,'! Weight type for summations.'
  !
  ! window
  if (set%nwind.eq.0) then
     mess = 'LAS'//backslash//'SET WINDOW'
     write(lun,1000) mess(1:lenc(mess)),'NONE',   &
          &      '! no line window.'
  else
     if (set%kind.eq.kind_spec) then
        mess = 'LAS'//backslash//'SET WINDOW'
        write(chaine,1010) mess(1:lenc(mess)),   &
             &        (set%wind1(i),set%wind2(i),   &
             &        i=1,set%nwind)
     elseif (set%kind.eq.kind_cont) then
        mess = 'LAS'//backslash//'SET WINDOW'
        write(chaine,1010) mess(1:lenc(mess)),   &
             &        (set%wind1(i)*class_setup_get_fangle(),   &
             &        set%wind2(i)*class_setup_get_fangle(),i=1,set%nwind)
     endif
     call sic_noir(chaine,nc)
     write(lun,1009) chaine(1:nc)
  endif
  !
1000 format(a,1x,a,t50,a)
1001 format(a,1x,i2,t50,a)
1002 format(a,2x,2(1pg11.4),t50,a)
1003 format(a,2(2x,i0),t50,a)
1004 format(a,t50,a)
1005 format(a,1x,1pg11.4,t50,a)
1009 format(a)
1010 format(a,10(1pg10.3))
1012 format(a,i0,a,t50,a)
1013 format(a,' "',a,'"',t50,a)
end subroutine sas_save
