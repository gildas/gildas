subroutine rix(entry_num,ind,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>rix
  use class_common
  use class_types
  !---------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  ! Read an Entry Index in the input file
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in)  :: entry_num  ! Entry number
  type(indx_t),               intent(out) :: ind        !
  logical,                    intent(out) :: error      ! Logical error flag
  !
  error = .false.
  !
  if (filein_isvlm) then
    call index_fromgdf(filein_vlmhead,entry_num,ind,error)
    if (error)  return
  else
    ! Read the index of entry_num in idatabi(:)
    call classic_entryindex_read(filein,entry_num,idatabi,ibufbi,error)
    if (error)  return
    !
    ! Fill ind% from idatabi(:), convert bytes if needed
    if (filein%desc%vind.eq.vind_v3) then
      call index_frombuf_v2orv3(idatabi,.true.,ind,filein%conv,error)
    elseif (filein%desc%vind.eq.vind_v2) then
      call index_frombuf_v2orv3(idatabi,.false.,ind,filein%conv,error)
    elseif (filein%desc%vind.eq.vind_v1) then
      call index_frombuf_v1(idatabi,ind,filein%conv,error)
    else
      call class_message(seve%e,'RIX','Index version not implemented')
      error = .true.
      return
    endif
    if (error)  return
  endif
  !
  ! No update of IX%, this was only a read access
  !
end subroutine rix
!
subroutine rox(entry_num,ind,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>rox
  use class_common
  use class_types
  !---------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  ! Read an Entry Index in the output file
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in)  :: entry_num  ! Entry number
  type(indx_t),               intent(out) :: ind        !
  logical,                    intent(out) :: error      ! Logical errror flag
  !
  error = .false.
  !
  ! Read the index of entry_num in odatabi(:)
  call classic_entryindex_read(fileout,entry_num,odatabi,obufbi,error)
  if (error)  return
  !
  ! Fill ind% from odatabi(:), convert bytes if needed
  if (fileout%desc%vind.eq.vind_v3) then
    call index_frombuf_v2orv3(odatabi,.true.,ind,fileout%conv,error)
  elseif (fileout%desc%vind.eq.vind_v2) then
    call index_frombuf_v2orv3(odatabi,.false.,ind,fileout%conv,error)
  elseif (fileout%desc%vind.eq.vind_v1) then
    call index_frombuf_v1(odatabi,ind,fileout%conv,error)
  else
    call class_message(seve%e,'ROX','Index version not implemented')
    error = .true.
    return
  endif
  if (error)  return
  !
  ! No update of OX%, this was only a read access
  !
end subroutine rox
!
subroutine wox(ind,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>wox
  use class_common
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! Write a new Entry Index in the output file
  !---------------------------------------------------------------------
  type(indx_t), intent(in)    :: ind    !
  logical,      intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='WOX'
  integer(kind=entry_length) :: entry_num
  !
  entry_num = fileout%desc%xnext
  if (entry_num.gt.ox%mobs) then
    ! Enlarge the Output File Index. Use a factor 2 (NB: ox%mobs can be
    ! 0 if an old (but empty) output file was reopened)
    call reallocate_optimize(ox,max(class_idx_size,ox%mobs*2_8),.false.,  &
      .true.,error)
    if (error)  return
  endif
  if (filein_is_fileout() .and. entry_num.gt.ix%mobs) then
    ! Same if FILE BOTH
    call reallocate_optimize(ix,max(class_idx_size,ix%mobs*2_8),.true.,  &
      .true.,error)
    if (error)  return
  endif
  !
  ! Fill odatabi(:) from ind%, convert bytes if needed
  if (fileout%desc%vind.eq.vind_v3) then
    call index_tobuf_v2orv3(ind,.true.,odatabi,fileout%conv,error)
  elseif (fileout%desc%vind.eq.vind_v2) then
    call index_tobuf_v2orv3(ind,.false.,odatabi,fileout%conv,error)
  elseif (fileout%desc%vind.eq.vind_v1) then
    call index_tobuf_v1(ind,odatabi,fileout%conv,error)
  else
    call class_message(seve%e,rname,'Index version not implemented')
    error = .true.
    return
  endif
  if (error)  return
  !
  ! Write the index of entry_num stored in odatabi(:)
  call classic_entryindex_write(fileout,entry_num,odatabi,obufbi,error)
  if (error)  return
  !
  ! Update optimize'd list of Indexes
  call index_tooptimize(ind,entry_num,.false.,entry_num,ox,error)
  if (error)  return
  ox%next = entry_num+1
  if (filein_is_fileout()) then
    call index_tooptimize(ind,entry_num,.true.,entry_num,ix,error)
    if (error)  return
    ix%next = entry_num+1
    ! Must force "forgetting" the record currently buffered in ibufbi,
    ! because the same record may have changed on disk
    call classic_recordbuf_nullify(ibufbi)
  endif
  !
  fileout%desc%xnext = entry_num+1
  !
end subroutine wox
!
subroutine mox(entry_num,ind,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>mox
  use class_common
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! Modify an old Entry Index in the output file
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in)    :: entry_num  !
  type(indx_t),               intent(in)    :: ind        !
  logical,                    intent(inout) :: error      !
  ! Local
  character(len=*), parameter :: rname='MOX'
  character(len=message_length) :: mess
  !
  if (entry_num.ge.fileout%desc%xnext) then
     error = .true.
     write(mess,'(A,I0)') 'Wrong entry number #',entry_num
     call class_message(seve%e,rname,mess)
     return
  endif
  !
  ! Fill odatabi(:) from ind%, convert bytes if needed
  if (fileout%desc%vind.eq.vind_v3) then
    call index_tobuf_v2orv3(ind,.true.,odatabi,fileout%conv,error)
  elseif (fileout%desc%vind.eq.vind_v2) then
    call index_tobuf_v2orv3(ind,.false.,odatabi,fileout%conv,error)
  elseif (fileout%desc%vind.eq.vind_v1) then
    call index_tobuf_v1(ind,odatabi,fileout%conv,error)
  else
    call class_message(seve%e,rname,'Index version not implemented')
    error = .true.
    return
  endif
  if (error)  return
  !
  ! Write the index of entry_num stored in odatabi(:)
  call classic_entryindex_write(fileout,entry_num,odatabi,obufbi,error)
  if (error)  return
  !
  call index_tooptimize(ind,entry_num,.false.,entry_num,ox,error)
  if (error)  return
  if (filein_is_fileout()) then
    call index_tooptimize(ind,entry_num,.true.,entry_num,ix,error)
    if (error)  return
  endif
  !
end subroutine mox
!
subroutine cox(error)
  use gbl_convert
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>cox
  use class_common
  !---------------------------------------------------------------------
  ! @ private
  ! Close an observation in the output file index
  !---------------------------------------------------------------------
  logical, intent(out) :: error
  ! Local
  character(len=*), parameter :: rname='COX'
  !
  if (.not.fileout_opened(rname,error))  return
  !
  ! Write the updated File Index to the file (in particular desc%xnext)
  call classic_filedesc_write(fileout,error)
  ! if (error) continue
  !
  ! Disable the following line, this would kill the 'classic_recordbuf_open'
  ! optimization which does not re-read a record which is already in memory.
  ! This optimization is usefull when writing indexes since they are small
  ! block of data contiguous in the same (often) record. Since 'obufbi'
  ! is only used for this purpose, this should not add any problem.
  ! call classic_recordbuf_nullify(obufbi)
  !
  if (filein_is_fileout()) then
    ! call classic_recordbuf_nullify(ibufbi)
    filein%desc = fileout%desc
  endif
  !
end subroutine cox
