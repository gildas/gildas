module classfits_types
  use gbl_message  ! for message_length
  use gkernel_types
  use class_types  ! for mxphas
  !
  integer(kind=4), parameter :: maxis=7       ! Maximum number of axes
  integer(kind=4), parameter :: mcols=256     ! Maximum number of columns in a 3d table
  integer(kind=4), parameter :: mhifibands=4  ! Maximum number of HIFI subbands
  integer(kind=4), parameter :: mwarning=100  ! Maximum number of warnings
  !
  type :: classfits_header_desc_t
    ! Description of the HDU
    integer(kind=4)   :: num            ! HDU number (0=Primary)
    integer(kind=4)   :: nbit           ! Number of bits per value in current header
    character(len=12) :: kind           ! Type of fits extension: "BASIC", "BINTABLE", ...
    ! Axes description
    integer(kind=4)   :: naxis          ! Number of axes present in data header
    integer(kind=4)   :: axis(maxis)    ! Axes dimensions
    integer(kind=4)   :: main_axis      ! The non dummy axis
    character(len=20) :: axtype(maxis)  ! CTYPE*
    real(kind=8)      :: rota(maxis)    ! CROTA*
    real(kind=8)      :: axval(maxis)   ! CRVAL*
    real(kind=8)      :: axref(maxis)   ! CRPIX*
    real(kind=8)      :: axinc(maxis)   ! CDELT*
    ! Data
    integer(kind=4)   :: ndata          ! Number of data points (in one row if apropriate)
    integer(kind=4)   :: lheap          ! Length of Heap area
    integer(kind=4)   :: theap          ! Beginning of variable length arrays
  end type classfits_header_desc_t
  !
  type :: classfits_header_t
    ! Header descriptor (reloaded for each HDU)
    type(classfits_header_desc_t) :: desc
    type(gfits_hdict_t) :: dict  ! Dictionary of cards in the header
    ! Misc ZZZ
    real(kind=4)      :: bscal     ! Tape scaling factor
    real(kind=4)      :: bzero     ! Map offset
    real(kind=8)      :: blank     ! Blanking value
    real(kind=4)      :: rbad      ! Real blanking value
    real(kind=4)      :: datamin   ! Minimum map value
    real(kind=4)      :: datamax   ! Maximum map value
    character(len=12) :: instrume  ! Instrument
    real(kind=4)      :: tauo2     ! oxygen atmospheric opacity
    real(kind=4)      :: tauh2o    ! water atmospheric opacity
  end type classfits_header_t
  !
  type :: classfits_column_positions_t
    integer(kind=4) :: naxis
    integer(kind=4) :: axis(maxis)
    integer(kind=4) :: crval(maxis)
    integer(kind=4) :: cdelt(maxis)
    integer(kind=4) :: crpix(maxis)
    integer(kind=4) :: crota(maxis)
    integer(kind=4) :: axtyp(maxis)
    integer(kind=4) :: wave
    integer(kind=4) :: channels
    integer(kind=4) :: scan
    integer(kind=4) :: subscan
    integer(kind=4) :: line
    integer(kind=4) :: object
    integer(kind=4) :: telesc
    integer(kind=4) :: backend
    integer(kind=4) :: matrix
    integer(kind=4) :: tsys
    integer(kind=4) :: restfreq
    integer(kind=4) :: imagfreq
    integer(kind=4) :: velocity
    integer(kind=4) :: velref
    integer(kind=4) :: specsys
    integer(kind=4) :: veldef
    integer(kind=4) :: deltav
    integer(kind=4) :: radesys
    integer(kind=4) :: equinox
    integer(kind=4) :: nphase         ! Frequency switch, number of phases
    integer(kind=4) :: deltaf(mxphas) ! Frequency throw for that phase
    integer(kind=4) :: ptime(mxphas)  ! Integration time for that phase
    integer(kind=4) :: weight(mxphas) ! Weight for that phase
    integer(kind=4) :: tau_atm
    integer(kind=4) :: tauo2
    integer(kind=4) :: tauh2o
    integer(kind=4) :: h2omm
    integer(kind=4) :: tamb
    integer(kind=4) :: pamb
    integer(kind=4) :: tchop
    integer(kind=4) :: tcold
    integer(kind=4) :: elevatio
    integer(kind=4) :: azimuth
    integer(kind=4) :: gainimag
    integer(kind=4) :: beameff
    integer(kind=4) :: forweff
    integer(kind=4) :: dobs
    integer(kind=4) :: dred
    integer(kind=4) :: ut
    integer(kind=4) :: lst
    integer(kind=4) :: datamax
    integer(kind=4) :: datamin
    integer(kind=4) :: time
    integer(kind=4) :: bmaj
    integer(kind=4) :: bmin
    integer(kind=4) :: bpa
  end type classfits_column_positions_t
  !
  type classfits_column_desc_t
    integer(kind=4)   :: ncols            ! Number of columns in table
    integer(kind=4)   :: nrows            ! Number of rows in table
    integer(kind=4)   :: lrow             ! [Bytes] Length of one row in table
    character(len=20) :: ttype(mcols)     ! Type of information in column i
    character(len=20) :: tform(mcols)     ! Format of data in column i (fits syntax)
    character(len=20) :: tunit(mcols)     ! Unit of data in column i
    logical           :: vararray(mcols)  ! Is Column of variable length ?
    integer(kind=4)   :: addr(mcols+1)    ! ZZZ suppress +1 Address of column i (in bytes within row)
    integer(kind=4)   :: leng(mcols)      ! Length (in bytes) of column i
    integer(kind=4)   :: fmt(mcols)       ! Format of column i (standard sic codes)
    integer(kind=4)   :: nitem(mcols)     ! Repeat count for column i
  end type classfits_column_desc_t
  !
  type :: classfits_columns_t
    ! Describe columns in FITS binary table
    type(classfits_column_desc_t)      :: desc  ! Column descriptions
    type(classfits_column_positions_t) :: posi  ! Column positions
  end type classfits_columns_t
  !
  type :: classfits_warnings_t
    ! Gather warnings in a structure, to be displayed (or not) when needed
    integer(kind=4) :: n
    character(len=message_length) :: mess(mwarning)
  end type classfits_warnings_t
  !
  type :: classfits_t
    logical               :: ishcss   ! Is this an HCSS FITS?
    character(len=12)     :: version  ! FITS version (some string format-dependent)
    type(classfits_header_t)   :: head     ! Header of current HDU
    type(classfits_columns_t)  :: cols     ! Columns of current HDU
    type(classfits_warnings_t) :: warn     ! Warnings
  end type classfits_t
  !
end module classfits_types
!
module class_fits
  use gildas_def
  use classfits_types
  !---------------------------------------------------------------------
  ! class_types is imported only for mxphas, the maximum number of
  ! phases in a frequency/position switch observation.
  !---------------------------------------------------------------------
  !
  ! Technical components for reading FITS
  integer(kind=record_length) :: HeapRec  ! First record of variable length arrays.
  integer(kind=4) :: HeapB                ! Offset of 1st var. len. arr. in record.
  !
  ! Command behavior
  character(len=12) :: fits_mode          ! Support for SET FITS MODE INDEX|SPECTRUM
  integer(kind=4) :: snbit                ! Support for SET FITS NBITS Value
  !
  type(classfits_t), save :: fits
  !
end module class_fits
