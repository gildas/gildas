module class_skydip
  ! FITSKY (minimization routine) variables
  integer, parameter :: mdata=20
  real :: elev(mdata), sky(mdata), par(2), sigrms
  integer :: ndata,ife
  logical :: trec_mode
  ! Calibration variables
  real :: tau_dry_s,tau_wet_s,t_atm_s
  real :: tau_dry_i,tau_wet_i,t_atm_i
  real :: tcal,tamb,gainim,tchop,feff
end module class_skydip
!
subroutine redsky(line,r,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>redsky
  use class_skydip
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  !   Reduce a skydip. Compute the starting atmospheric model.
  !----------------------------------------------------------------------
  character(len=*),  intent(in)    :: line  ! Input command line
  type(observation), intent(inout) :: r     !
  logical,           intent(inout) :: error ! Error flag
  ! Local
  character(len=*), parameter :: rname='SKYDIP'
  real :: freqsi,freqim,obshum
  character(len=25) :: mess(3)
  real :: temi,tau_tot_s,tau_tot_i
  integer :: ier, i, nc
  character(len=2) :: argum
  character(len=message_length) :: chain
  ! Data
  data obshum /0.0/
  data mess /'Zero atmospheric opacity',  &
             'No oxygen in atmosphere',   &
             'No water in atmosphere'/
  !
  call atm_atmosp (r%head%cal%tamb,r%head%cal%pamb,r%head%cal%alti*1e-3) ! press at ALTI
  freqsi = 1d-3*r%head%sky%restf
  tau_tot_s = r%head%cal%taus
  t_atm_s = r%head%cal%tatms
  call atm_transm (1.,1.,freqsi,temi,t_atm_s,tau_dry_s,tau_wet_s,tau_tot_s,ier)
  if (ier.ne.0) then
    chain = 'Stupid calibration '//mess(ier)
    call class_message(seve%e,rname,chain)
  endif
  freqim = 1d-3*r%head%sky%image
  tau_tot_i = r%head%cal%taui
  t_atm_i = r%head%cal%tatmi
  call atm_transm (1.,1.,freqim,temi,t_atm_i,tau_dry_i,tau_wet_i,tau_tot_i,ier)
  if (ier.ne.0) then
    chain = 'Stupid calibration '//mess(ier)
    call class_message(seve%e,rname,chain)
  endif
  argum = 'EF'
  call sic_ke (line,0,1,argum,nc,.false.,error)
  if (error) return
  if (argum.eq.'TR') then
    trec_mode = .true.
  elseif (argum.eq.'EF') then
    trec_mode = .false.
  else
    call class_message(seve%e,'REDUCE','Invalid argument '//argum)
    error = .true.
    return
  endif
  tamb   = r%head%cal%tamb
  tchop  = r%head%cal%tchop
  feff   = r%head%cal%foeff
  gainim = r%head%cal%gaini
  ndata = r%head%sky%nsky
  if (trec_mode) then
    do i=1,ndata
      sky(i) = r%head%sky%emiss(i)-r%head%sky%chopp(i)
      elev(i) = r%head%sky%elev(i)
    enddo
    par(1) = r%head%sky%chopp(1)/(0.9*r%head%cal%trec+r%head%cal%tchop)
  else
    do i=1,ndata
      sky(i) = (r%head%cal%tchop+r%head%cal%trec)*r%head%sky%emiss(i) &
               /r%head%sky%chopp(i) - r%head%cal%trec
      elev(i) = r%head%sky%elev(i)
    enddo
  endif
  !
  ! Minimize
  call fitsky(minsky,.false.,ier,r%head%sky%chopp(1))
  if (ier.ne.0) call class_message(seve%w,rname,'Solution not converged')
  !
  if (trec_mode) then
    r%head%cal%trec = par(1)
  else
    r%head%cal%foeff = par(1)
  endif
  r%head%cal%h2omm = par(2)
  r%head%cal%taus = tau_dry_s + r%head%cal%h2omm * tau_wet_s
  r%head%cal%taui = tau_dry_i + r%head%cal%h2omm * tau_wet_i
  ! Display results
  write(6,1000) r%head%gen%scan,r%head%cal%trec,r%head%cal%tchop,r%head%cal%tcold
  write(6,1001)  &
    r%head%gen%num, r%head%gen%az, r%head%cal%tamb, r%head%cal%pamb, obshum,    &
    r%head%cal%h2omm, r%head%cal%foeff, sigrms,                                 &
    freqsi, r%head%cal%taus, tau_dry_s, tau_wet_s*r%head%cal%h2omm, r%head%cal%tatms,  &
    freqim, r%head%cal%taui, tau_dry_i, tau_wet_i*r%head%cal%h2omm, r%head%cal%tatmi
  return
  !
1000 format(t5,'Scan',t15,'Trec',t25,'Tchopper',t35,'Tcold',  &
  /,i9,3f10.1)
1001 format(  &
  t3,'Observation',t15,'Az',t25,'Tamb',t35,'Pamb',t44,'Humidity',  &
  t55,'Water',t65,'Feff', t75,'Rms(K)',                            &
  /,i9,7f10.4,                                                     &
  /,t25,'Freq',t35,'Tau',t45,'O2',t55,'H2O',t65,'Tatm',            &
  /,' Signal band : ',t20,5f10.4,                                  &
  /,' Image  band : ',t20,5f10.4)
end subroutine redsky
!
subroutine fitsky(fcn,liter,ier,meancho)
  use gbl_message
  use fit_minuit
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>fitsky
  use class_skydip
  !----------------------------------------------------------------------
  ! @ private
  !  Setup and starts a SKYDIP fit minimisation using MINUIT
  !----------------------------------------------------------------------
  external             :: fcn      ! Function to be mininized
  logical, intent(in)  :: liter    ! Logical of iterate fit
  integer, intent(out) :: ier      ! Error flag
  real,    intent(in)  :: meancho  !
  ! Local
  character(len=*), parameter :: rname='FITSKY'
  integer :: i,l
  real :: tau,a1,ap
  real(8) :: dx,al,ba,du1,du2
  type(fit_minuit_t) :: fit
  character(len=message_length) :: mess
  logical :: error
  !
  error = .false.
  ier = 0
  !
  fit%isyswr=6
  fit%owner = gpack_get_id('class',.true.,error)
  if (error)  return
  fit%maxext=ntot
  fit%maxint=nvar
  !
  ! Initialise values
  ! Note : should put TCABIN instead of OBsTAM...
  a1 = 1./sin(elev(1))
  ap = 1./sin(elev(ndata))
  if (trec_mode) then
    tau = (sky(ndata)-sky(1))/par(1)/(ap-a1)/t_atm_s/feff
    par(2) = (tau-tau_dry_s)/tau_wet_s * (1.0+gainim)
    par(2) = min(20.,max(0.,par(2)))
  else
    par(1) = 1. - (a1*sky(ndata) - ap*sky(1))/tamb/(a1-ap)
    par(1) = min(1.,max(0.,par(1)))
    tau = (sky(ndata)-sky(1))/par(1)/(ap-a1)/t_atm_s
    par(2) = (tau-tau_dry_s)/tau_wet_s * (1.0+gainim)
    par(2) = min(20.,max(0.,par(2)))
  endif
  write(6,*) 'Starting value of wH2O ',par(2)
  !
  call midsky(fit,ier,liter)
  if (ier.ne.0) return
  call intoex(fit,fit%x)
  fit%nfcnmx = 1000
  fit%newmin = 0
  fit%itaur  = 0
  fit%isw(1) = 0
  fit%isw(3) = 1
  fit%nfcn = 1
  fit%vtest  = 0.04
  call fcn(fit%npar,fit%g,fit%amin,fit%u,1)
  sigrms = sqrt(fit%amin/ndata)
  fit%up = sigrms**2
  fit%epsi  = 0.1d0 * fit%up
  !
  ! Simplex Minimization
  if (.not.liter) then
    call simplx(fit,fcn,ier)
    if (ier.ne.0) return
    par(1) = fit%u(1)
    par(2) = fit%u(2)
  endif
  !
  ! Gradient Minimization
  call intoex(fit,fit%x)
  call fcn(fit%npar,fit%g,fit%amin,fit%u,3)
  sigrms  = sqrt(fit%amin/ndata)
  write(mess,1002) sigrms
  call class_message(seve%r,rname,mess)
  fit%up = sigrms**2
  fit%epsi  = 0.1d0 * fit%up
  fit%apsi  = fit%epsi
  call hesse(fit,fcn)
  call migrad(fit,fcn,ier)
  call intoex(fit,fit%x)
  call fcn(fit%npar,fit%g,fit%amin,fit%u,3)
  sigrms  = sqrt(fit%amin/ndata)
  fit%up = sigrms**2
  fit%epsi  = 0.1d0 * fit%up
  fit%apsi  = fit%epsi
  ier = 0
  call migrad(fit,fcn,ier)
  !
  ! Iterate once if not converged
  if (ier.eq.3) then
    ier = 0
    call migrad(fit,fcn,ier)
  endif
  !
  ! Compute covariance matrix if bad
  if (ier.eq.1) then
    call hesse(fit,fcn)
    ier = 0
  endif
  !
  ! Print Results
  call intoex(fit,fit%x)
  call fcn(fit%npar,fit%g,fit%amin,fit%u,3)
  sigrms = sqrt(fit%amin/ndata)
  write(mess,1002) sigrms
  call class_message(seve%r,rname,mess)
  fit%up  = sigrms**2
  !
  ! Calculate External Errors
  do i=1,fit%nu
    l  = fit%lcorsp(i)
    if (l .eq. 0)  then
      fit%werr(i)=0.
    else
      if (fit%isw(2) .ge. 1)  then
        dx = dsqrt(dabs(fit%v(l,l)*fit%up))
        if (fit%lcode(i) .gt. 1) then
          al = fit%alim(i)
          ba = fit%blim(i) - al
          du1 = al + 0.5d0 *(dsin(fit%x(l)+dx) +1.0d0) * ba - fit%u(i)
          du2 = al + 0.5d0 *(dsin(fit%x(l)-dx) +1.0d0) * ba - fit%u(i)
          if (dx .gt. 1.0d0)  du1 = ba
          dx = 0.5d0 * (dabs(du1) + dabs(du2))
        endif
        fit%werr(i) = dx
      endif
    endif
  enddo
  !
  if (trec_mode) then
    fit%werr(1) = fit%werr(1)/par(1)**2*meancho
    par(1) = meancho/par(1) - tchop
    write(mess,1004)
    call class_message(seve%r,rname,mess)
    write(mess,1003) par(1),fit%werr(1),par(2),fit%werr(2)
    call class_message(seve%r,rname,mess)
  else
    write(mess,1001)
    call class_message(seve%r,rname,mess)
    write(mess,1000) par(1),fit%werr(1),par(2),fit%werr(2)
    call class_message(seve%r,rname,mess)
  endif
  return
  !
1000 format(4x,f9.3,' (',f8.3,')    ',f10.3,' (',f8.3,')')
1001 format(/,'  SKYDIP Fit Results   ',//,  &
              ' Forward Efficiency        Water vapor (mm) ')
1002 format(' RMS of Residuals : ',1pg10.3)
1003 format(4x,f9.2,' (',f8.2,')    ',f10.3,' (',f8.3,')')
1004 format(/,'  SKYDIP Fit Results   ',//,  &
              ' Trec                      Water vapor (mm) ')
end subroutine fitsky
!
subroutine midsky(fit,ier,liter)
  use gbl_message
  use fit_minuit
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>midsky
  use class_skydip
  !----------------------------------------------------------------------
  ! @ private
  ! RED Internal routine
  !   Start a SKYDIP fit by building the PAR array and internal
  !    variable used by Minuit.
  !   Parameters are
  !    PAR(1) = RFOEFF Forward efficiency
  !     or      RTREC  Receiver temperature
  !    PAR(2) = RH2OMM Water vapor amount (mm)
  !----------------------------------------------------------------------
  type(fit_minuit_t), intent(inout) :: fit    ! Fitting variables
  integer,            intent(out)   :: ier    ! Error code
  logical,            intent(in)    :: liter  ! Iterate a fit
  ! Local
  character(len=*), parameter :: rname='MIDSKY'
  integer :: ifatal
  integer :: i,ninte,k
  real(8) :: sav,sav2,vplu,vminu,value
  character(len=message_length) :: mess
  !
  ier = 0
  do i= 1, 7
    fit%isw(i) = 0
  enddo
  fit%npfix = 0
  ninte = 0
  fit%nu = 2
  fit%npar = 0
  ifatal = 0
  do i= 1, fit%maxext
    fit%u(i) = 0.0d0
    fit%lcode(i) = 0
    fit%lcorsp (i) = 0
  enddo
  fit%isw(5) = 1
  !
  ! Set up Parameters
  fit%nu=2
  !
  ! Forward Efficiency
  fit%u(1) = par(1)
  if (trec_mode) then
    fit%werr(1) = abs(0.2*par(1))
    fit%lcode(1) = 1
  else
    fit%werr(1) = 0.01
    fit%alim(1) = 0.30
    fit%blim(1) = 1.00
    fit%lcode(1) = 0
  endif
  !
  ! Water Vapor
  fit%u(2) = par(2)
  fit%werr(2) = max(0.2,0.1*par(2))
  fit%alim(2) = 0.0                ! No water
  fit%blim(2) = 20.0
  !
  ! Various checks
  do k=1,fit%nu
    if (k.gt.fit%maxext)  then
      ifatal = ifatal + 1
      cycle
    endif
    ! Fixed parameter
    if (fit%werr(k) .le. 0.0d0)  then
      fit%lcode(k) = 0
      write(mess,'(A,I0,A)') 'Parameter ',k,' is fixed'
      call class_message(seve%i,rname,mess)
      cycle
    endif
    ! Variable parameter
    ninte = ninte + 1
    if (fit%lcode(k).eq.1)  cycle
    fit%lcode(k) = 4
    value = (fit%blim(k)-fit%u(k))*(fit%u(k)-fit%alim(k))
    if (value.lt.0.0d0) then
      write(mess,'(A,I0,A)') 'Parameter ',k,' is outside limits'
      call class_message(seve%e,rname,mess)
      ifatal = ifatal+1
    elseif (value.eq.0.0d0) then
      write(mess,'(A,I0,A)') 'Parameter ',k,' is at limit'
      call class_message(seve%w,rname,mess)
    endif
  enddo
  !
  ! End parameter cards
  ! Stop if fatal error
  if (ninte .gt. fit%maxint)  then
    write(mess,'(A,I0,A,I0)') 'Too many variable parameters: ',ninte,' > ',fit%maxint
    call class_message(seve%e,rname,mess)
    ifatal = ifatal + 1
  endif
  if (ninte .eq. 0) then
    call class_message(seve%e,rname,'All input parameters are fixed')
    ifatal = ifatal + 1
  endif
  if (ifatal.gt.0)  then
    write(mess,'(I0,A)') ifatal,' errors on input parameters, abort.'
    call class_message(seve%e,rname,mess)
    ier = 2
    return
  endif
  !
  ! O.K. Start
  ! Calculate step sizes DIRIN
  fit%npar = 0
  do k= 1, fit%nu
    if (fit%lcode(k) .gt. 0)  then
      fit%npar = fit%npar + 1
      fit%lcorsp(k) = fit%npar
      sav = fit%u(k)
      fit%x(fit%npar) = pintf(fit,sav,k)
      fit%xt(fit%npar) = fit%x(fit%npar)
      sav2 = sav + fit%werr(k)
      vplu = pintf(fit,sav2,k) - fit%x(fit%npar)
      sav2 = sav - fit%werr(k)
      vminu = pintf(fit,sav2,k) - fit%x(fit%npar)
      fit%dirin(fit%npar) = 0.5d0 * (dabs(vplu) +dabs(vminu))
    endif
  enddo
  !
end subroutine midsky
!
subroutine minsky(npar,grad,sqsum,x,iflag)
  use class_skydip
  !----------------------------------------------------------------------
  ! @ private-mandatory
  ! RED Internal routine
  !   Function to be minimized in the SKYDIP.
  !----------------------------------------------------------------------
  integer, intent(in)  :: npar        ! Number of parameters
  real(8), intent(out) :: grad(npar)  ! Array of derivatives
  real(8), intent(out) :: sqsum       ! Sum of the square distances to the data points
  real(8), intent(in)  :: x(npar)     ! Parameter values
  integer, intent(in)  :: iflag       ! Code operation
  ! Local
  real :: airmass
  real*8 :: yy,dyy(2),dist,dw_sqsum,de_sqsum
  logical :: dograd
  integer :: ip
  !
  sqsum = 0.0d0
  dw_sqsum = 0.0d0
  de_sqsum = 0.0d0
  dograd = iflag.eq.2
  !
  ! Compute function and derivative
  do ip=1,ndata
    !
    call fsky(elev(ip),npar,x,dograd,airmass,yy,dyy)
    !
    ! Compute CHI**2
    dist = yy - sky(ip)
    sqsum = sqsum + dist**2
    !
    if (.not.dograd) cycle  ! ip
    !
    ! Compute Gradients
    de_sqsum = de_sqsum + 2.0*dist*dyy(1)
    dw_sqsum = dw_sqsum + 2.0*dist*dyy(2)
    !
  enddo
  !
  ! Setup values and return
  grad(1) = de_sqsum
  grad(2) = dw_sqsum
end subroutine minsky
!
subroutine fsky(eleva,npar,params,dograd,airmass,yy,dyy)
  use phys_const
  use class_skydip
  !----------------------------------------------------------------------
  ! @ private
  ! RED Internal routine
  !   Compute sky emission
  !----------------------------------------------------------------------
  real,    intent(in)  :: eleva         ! Elevation in degrees
  integer, intent(in)  :: npar          ! Number of parameters (forward_eff, wh2o,...)
  real(8), intent(in)  :: params(npar)  ! Values of parameters
  logical, intent(in)  :: dograd        ! Compute the gradient
  real,    intent(out) :: airmass       ! Number of airmasses
  real(8), intent(out) :: yy            ! Emissivity (Kelvins)
  real(8), intent(out) :: dyy(npar)     ! Derivate value according to each parameter
  ! Local
  real(8) :: eps,gamma,hz
  real(8), parameter :: h0=5.5d0     ! Atmospheric scale height
  real(8), parameter :: r0=6370.0d0  ! Earth radius
  real :: tsky,           dw_tsky
  real :: tsky_s,e_tsky_s,dw_tsky_s,tau_tot_s
  real :: tsky_i,e_tsky_i,dw_tsky_i,tau_tot_i
  real :: zz,de_yy,dw_yy
  !
  ! Curved atmosphere, equivalent height 5.5 KM
  eps = asin(r0/(r0+h0)*cos(eleva))
  gamma = 0.5*pi-eleva-eps
  hz = r0*r0 + (r0+h0)**2 - 2.*r0*(r0+h0)*cos(gamma)
  hz = sqrt(max(hz,h0**2))     ! Avoid floating point errors
  airmass = hz/h0
  !
  ! Tsky (signal frequency component)
  tau_tot_s = params(2)*tau_wet_s + tau_dry_s
  e_tsky_s = exp(-airmass*tau_tot_s)
  tsky_s = t_atm_s * (1. - e_tsky_s)
  !
  ! Tsky (image frequency component)
  tau_tot_i = params(2)*tau_wet_i + tau_dry_i
  e_tsky_i = exp(-airmass*tau_tot_i)
  tsky_i = t_atm_i * (1. - e_tsky_i)
  !
  ! Tsky (total)
  tsky  = (tsky_s + tsky_i*gainim) / (1. + gainim)
  !
  if (trec_mode) then
    zz = tamb-tchop + feff*(tsky-tamb)
    yy = params(1) * zz
  else
    yy = params(1)*tsky + (1.-params(1)) * tamb
  endif
  !
  if (.not.dograd) return
  !
  dw_tsky_s = t_atm_s * tau_wet_s * e_tsky_s * airmass
  dw_tsky_i = t_atm_i * tau_wet_i * e_tsky_i * airmass
  dw_tsky = (dw_tsky_s + dw_tsky_i*gainim) / (1.+gainim)
  if (trec_mode) then
    de_yy = zz
    dw_yy = params(1)*feff*dw_tsky
  else
    de_yy = tsky - tamb
    dw_yy = params(1)*dw_tsky
  endif
  !
  dyy(1) = de_yy
  dyy(2) = dw_yy
  !
end subroutine fsky
