subroutine rsec(ed,ksec,lsec,sec,error)
  use gbl_message
  use classic_api
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>rsec
  use class_common
  !----------------------------------------------------------------------
  ! @ private
  !  Read a section in the input observation
  !----------------------------------------------------------------------
  type(classic_entrydesc_t), intent(in)    :: ed      ! Entry Descriptor
  integer(kind=4),           intent(in)    :: ksec    ! Section number
  integer(kind=8),           intent(inout) :: lsec    ! Maximum and actual length of section
  integer(kind=4),           intent(out)   :: sec(*)  ! Section buffer
  logical,                   intent(out)   :: error   ! Error flag
  ! Local
  character(len=*), parameter :: rname='RSEC'
  integer(kind=4), parameter :: mnsec=18
  character(len=12) :: section(0:mnsec)
  data section /'USER CODE','COMMENT','GENERAL','POSITION','SPECTROSCOPY',       &
                'BASELINE','ORIGIN','PLOT','Freq. SWITCH','Gauss FIT',           &
                'CONTINUUM','Beam. SWITCH','Shell FIT','NH3 FIT','CALIBRATION',  &
                'Cont. FIT','SKYDIP','X Coord.','Abs. fit'/
  !
  error = .false.
  call classic_entry_section_read(ksec,lsec,sec,ed,ibufobs,error)
  if (error) then
    ! Translate section id to human readable name:
    if (ksec.le.0 .and. -ksec.le.mnsec) then
      call class_message(seve%e,rname,'Error reading section '//section(-ksec))
    endif
    return
  endif
  !
end subroutine rsec
!
subroutine wsec(ed,ksec,lsec,sec,error)
  use gbl_message
  use classic_api
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>wsec
  use class_common
  !----------------------------------------------------------------------
  ! @ private
  !  Write a section to the output file
  !----------------------------------------------------------------------
  type(classic_entrydesc_t), intent(inout) :: ed      ! Entry Descriptor
  integer(kind=4),           intent(in)    :: ksec    ! Section number
  integer(kind=8),           intent(in)    :: lsec    ! Maximum and actual length of section
  integer(kind=4),           intent(in)    :: sec(*)  ! Section buffer
  logical,                   intent(out)   :: error   ! Error flag
  ! Local
  character(len=*), parameter :: rname='WSEC'
  integer(kind=4), parameter :: mnsec=18
  character(len=12) :: section(0:mnsec)
  data section /'USER CODE','COMMENT','GENERAL','POSITION','SPECTROSCOPY',     &
                'BASELINE','ORIGIN','PLOT','Freq. SWITCH','Gauss FIT',         &
                'CONTINUUM','PROJECTION','Shell FIT','NH3 FIT','CALIBRATION',  &
                'Cont. FIT','SKYDIP','X Coord.','Abs. fit'/
  !
  error = .false.
  !
  if (obufobs%lun.ne.fileout%lun) then
     error = .true.
     call class_message(seve%f,rname,'Observation not opened for write or modify')
     return
  endif
  !
  if (outobs_modify) then
    call classic_entry_section_update(ksec,lsec,sec,ed,obufobs,error)
  else
    call classic_entry_section_add(ksec,lsec,sec,ed,obufobs,error)
  endif
  if (error) then
    ! Translate section id to human readable name:
    if (ksec.le.0 .and. -ksec.le.mnsec) then
      call class_message(seve%e,rname,'Error writing section '//section(-ksec))
    endif
    return
  endif
  !
end subroutine wsec
