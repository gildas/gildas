!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine class_spectrum(set,line,r,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_spectrum
  use class_data
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! CLASS Support routine for command
  !       SPECTRUM [ArrayName] [Yoffset]
  ! 1              [/INDEX]
  ! 2              [/OBS]
  ! 3              [/PEN Ipen]
  ! Plot the spectrum. Alternatively, plot the INDEX as a 2D image.
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  character(len=*),    intent(in)    :: line   ! Input command line
  type(observation),   intent(inout) :: r      !
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='SPECTRUM'
  logical :: doindex,dopen
  real(kind=4) :: offset
  integer(kind=4) :: oldpen,newpen,nc
  character(len=12) :: aaname
  !
  ! Command line parsing
  if (sic_present(1,0).and.sic_present(2,0)) then
     call class_message(seve%e,rname,'/INDEX and /OBS are exclusive from each other')
     error = .true.
     return
  endif
  !
  if (sic_present(1,0)) then
     if (.not.associated(p%data2)) then
        call class_message(seve%e,rname,'No index loaded')
        error = .true.
        return
     endif
     doindex = .true.
  elseif (sic_present(2,0)) then
     doindex = .false.
  else
     doindex = set%action.eq.'I'
  endif
  !
  dopen = sic_present(3,0)
  if (dopen) then
    if (doindex)  &
      call class_message(seve%w,rname,'/PEN ignored with option /INDEX')
    call sic_i4(line,3,1,newpen,.true.,error)
    if (error) return
  endif
  !
  ! Compute plot limits
  if (doindex) then
     call newlim(set,p,error)
  else
     call newlim(set,r,error)
  endif
  if (error) return
  !
  ! Plot kind
  if (doindex) then
     call spectr2d(p,error)
     if (error) return
  else
     ! Plot a single spectrum. 4 possible syntaxes:
     !  1: SPECTRUM
     !  2: SPECTRUM Offset
     !  3: SPECTRUM Arrayname
     !  4: SPECTRUM Arrayname Offset
     offset = 0.
     aaname = 'Y'  ! Default is to plot RY
     call sic_ke(line,0,1,aaname,nc,.false.,error)
     if (error)  return
     if (sic_present(0,2)) then
       ! Syntax is unambiguous: SPECTRUM Arrayname Offset
       ! aaname = already read
       call sic_r4 (line,0,2,offset,.true.,error)
       if (error) return
     elseif (sic_present(0,1)) then
       ! Syntax is ambiguous: SPECTRUM Arrayname|Offset
       ! NB: 'Y' is accepted as not-yet-assoc-array RY
       if (aaname.ne.'Y' .and. .not.class_assoc_exists(r,aaname)) then
         ! Not an associated array, must be an offset
         aaname = 'Y'
         call sic_r4 (line,0,1,offset,.true.,error)
         if (error) return
        endif
     endif
     !
     if (dopen)  oldpen = gr_spen(newpen)
     call spectr1d(rname,set,r,error,offset,aaname)
     if (dopen)  newpen = gr_spen(oldpen)
     if (error) return
  endif
  !
end subroutine class_spectrum
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine spectr1d(caller,set,obs,error,offset,aaname)
  use gildas_def
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>spectr1d
  use class_types
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  ! Plot the spectrum (RY), with an optional Y offset.
  ! If an Associated Array name is given, plot this array instead.
  !---------------------------------------------------------------------
  character(len=*),           intent(in)    :: caller  ! Calling routine name
  type(class_setup_t),        intent(in)    :: set     !
  type(observation),          intent(in)    :: obs     ! Observation to be plotted
  logical,                    intent(inout) :: error   ! Logical error flag
  real(kind=4),     optional, intent(in)    :: offset  ! Y offset value
  character(len=*), optional, intent(in)    :: aaname  ! Associated Array name
  ! Local
  integer(kind=4) :: n
  character(len=256) :: ch
  real(kind=4) :: yoff,mini,maxi
  character(len=12) :: aanam
  logical :: yreset
  type(class_assoc_sub_t) :: array
  !
  ! Basic checks
  if (obs%head%xnum.eq.0) then
     call class_message(seve%e,caller,'No spectrum in memory')
     error = .true.
     return
  endif
  !
  if (present(offset)) then
    yoff = offset
  else
    yoff = 0.0
  endif
  if (present(aaname)) then
    aanam = aaname
  else
    aanam = 'Y'  ! Default is to plot RY (not-yet-assoc-array)
  endif
  !
  n = obs%cimax-obs%cimin+1
  yreset = yoff.ne.0  ! Will we need to reset Y scale at the end?
  !
  if (aanam.ne.'Y') then
    ! Plot an associated array
    if (set%modey.eq.'F') then  ! FIXED limits
      call yrescale(guy1-yoff,guy2-yoff,error)
      if (error)  return
    else  ! Use array total limits
      call class_assoc_minmax(obs,aanam,mini,maxi,error)
      if (error)  return
      call yrescale(mini-yoff,maxi-yoff,error)
      if (error)  return
      yreset = .true.
    endif
    !
    if (.not.class_assoc_exists(obs,aanam,array)) then
      call class_message(seve%e,caller,'No such associated array '//aanam)
      error = .true.
      return
    endif
    !
    call gr_segm('SPECTRUM',error)  ! New segment
    if (error)  return
    select case (array%fmt)
    case (fmt_r4)
      call spectr1d_draw(set,obs,array%r4(:,1),array%badr4,error)
    case (fmt_i4,fmt_by,fmt_b2)
      call spectr1d_draw(set,obs,array%i4(:,1),array%badi4,error)
    case default
      call class_message(seve%e,caller,'Kind of data not implemented')
      error = .true.
    end select
    if (error)  continue
    call gr_segm_close(error)  ! Close segment SPECTRUM
    if (error)  return
    !
  else
    ! Default is to plot RY
    call yrescale(guy1-yoff,guy2-yoff,error)
    if (error)  return
    call gr_segm('SPECTRUM',error)  ! New segment
    if (error)  return
    call spectr1d_draw(set,obs,obs%spectre,obs_bad(obs%head),error)
    if (error)  continue
    call gr_segm_close(error)  ! Close segment SPECTRUM
    if (error)  return
  endif
  if (error)  return
  !
  if (yreset) then
    ! Revert to normal RY limits
    call yrescale(guy1,guy2,error)
    if (error)  return
  endif
  !
contains
  subroutine yrescale(ymin,ymax,error)
    real(kind=4), intent(in)    :: ymin,ymax
    logical,      intent(inout) :: error
    write(ch,'(A,4(1X,G14.7))') 'LIMITS',gux1,gux2,ymin,ymax
    call gr_exec(ch)
    error = gr_error()
  end subroutine yrescale
end subroutine spectr1d
!
subroutine spectr1d_draw_r4(set,obs,y,bad,error)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>spectr1d_draw_r4
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic spectr1d_draw
  ! Draw the input Y array according to the current SET PLOT rule.
  ! X in found in obs%datax(:), plot is done according to the current
  ! SET MODE X range.
  ! ---
  !  R*4 version
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(observation),   intent(in)    :: obs    !
  real(kind=4),        intent(in)    :: y(:)   !
  real(kind=4),        intent(in)    :: bad    ! Blanking value
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  integer(kind=4) :: n
  real(kind=4), allocatable :: x4(:)
  !
  n = obs%cimax-obs%cimin+1
  !
  allocate(x4(n))
  x4(:) = obs%datax(obs%cimin:obs%cimax)  ! Duplicate R*8 into R*4. Precision loss?
  !
  if (set%plot.eq.'N') then
    call gr4_connect(n,x4,y(obs%cimin:obs%cimax),bad,0.0)
  else if (set%plot.eq.'H') then
    if (n.eq.1 .and. y(obs%cimin).ne.bad) then
      call spectr1d_draw_histo1chan(set,obs,real(y(obs%cimin),kind=8))
    else
      call gr4_histo(n,x4,y(obs%cimin:obs%cimax),bad,0.0)
    endif
  else if (set%plot.eq.'P') then
    call gr4_marker (n,x4,y(obs%cimin:obs%cimax),bad,0.0)
  endif
  !
  deallocate(x4)
  !
end subroutine spectr1d_draw_r4
!
subroutine spectr1d_draw_r8(set,obs,y,bad,error)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>spectr1d_draw_r8
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic spectr1d_draw
  ! Draw the input Y array according to the current SET PLOT rule.
  ! X in found in obs%datax(:), plot is done according to the current
  ! SET MODE X range.
  ! ---
  !  R*8 version
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(observation),   intent(in)    :: obs    !
  real(kind=8),        intent(in)    :: y(:)   !
  real(kind=8),        intent(in)    :: bad    ! Blanking value
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  integer(kind=4) :: n
  !
  n = obs%cimax-obs%cimin+1
  !
  if (set%plot.eq.'N') then
    call gr8_connect(n,obs%datax(obs%cimin:obs%cimax),y(obs%cimin:obs%cimax),bad,0.d0)
  else if (set%plot.eq.'H') then
    if (n.eq.1 .and. y(obs%cimin).ne.bad) then
      call spectr1d_draw_histo1chan(set,obs,y(obs%cimin))
    else
      call gr8_histo(n,obs%datax(obs%cimin:obs%cimax),y(obs%cimin:obs%cimax),bad,0.d0)
    endif
  else if (set%plot.eq.'P') then
    call gr8_marker (n,obs%datax(obs%cimin:obs%cimax),y(obs%cimin:obs%cimax),bad,0.d0)
  endif
  !
end subroutine spectr1d_draw_r8
!
subroutine spectr1d_draw_i4(set,obs,y,bad,error)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>spectr1d_draw_i4
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic spectr1d_draw
  ! Draw the input Y array according to the current SET PLOT rule.
  ! X in found in obs%datax(:), plot is done according to the current
  ! SET MODE X range.
  ! ---
  !  I*4 version
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(observation),   intent(in)    :: obs    !
  integer(kind=4),     intent(in)    :: y(:)   !
  integer(kind=4),     intent(in)    :: bad    ! Blanking value
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  integer(kind=4) :: n
  real(kind=8), allocatable :: y8(:)
  real(kind=8) :: bad8
  !
  n = obs%cimax-obs%cimin+1
  !
  allocate(y8(n))
  y8(:) = y(obs%cimin:obs%cimax)  ! Duplicate I*4 into R*8
  bad8 = real(bad,kind=8)
  !
  if (set%plot.eq.'N') then
    call gr8_connect(n,obs%datax(obs%cimin:obs%cimax),y8,bad8,0.d0)
  else if (set%plot.eq.'H') then
    if (n.eq.1 .and. y(obs%cimin).ne.bad) then
      call spectr1d_draw_histo1chan(set,obs,y8(obs%cimin))
    else
      call gr8_histo(n,obs%datax(obs%cimin:obs%cimax),y8,bad8,0.d0)
    endif
  else if (set%plot.eq.'P') then
    call gr8_marker (n,obs%datax(obs%cimin:obs%cimax),y8,bad8,0.d0)
  endif
  !
  deallocate(y8)
  !
end subroutine spectr1d_draw_i4
!
subroutine spectr1d_draw_histo1chan(set,obs,y)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>spectr1d_draw_histo1chan
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Ad hoc subroutine which draws a single-channel histogram. This can
  ! not be done with gr4_histo/gr8_histo as they guess the width from
  ! 2 contiguous channels.
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in) :: set
  type(observation),   intent(in) :: obs
  real(kind=8),        intent(in) :: y
  ! Local
  real(kind=8) :: c(2),x(2)
  !
  c(1) = obs%cimin-0.5d0
  c(2) = obs%cimin+0.5d0
  !
  if (obs%head%gen%kind.eq.kind_spec) then
    if (set%unitx(1).eq.'C') then
      x(:) = c(:)
    elseif (set%unitx(1).eq.'V') then
      call abscissa_chan2velo(obs%head,c(1),x(1))
      call abscissa_chan2velo(obs%head,c(2),x(2))
    elseif (set%unitx(1).eq.'F') then
      call abscissa_chan2sigoff(obs%head,c(1),x(1))
      call abscissa_chan2sigoff(obs%head,c(2),x(2))
    elseif (set%unitx(1).eq.'I') then
      call abscissa_chan2imaoff(obs%head,c(1),x(1))
      call abscissa_chan2imaoff(obs%head,c(2),x(2))
    endif
  else
    if (set%unitx(1).eq.'C') then
      x(:) = c(:)
    elseif (set%unitx(1).eq.'T') then
      call abscissa_chan2time(obs%head,c(1),x(1))
      call abscissa_chan2time(obs%head,c(2),x(2))
    elseif (set%unitx(1).eq.'A') then
      call abscissa_chan2angl(obs%head,c(1),x(1))
      call abscissa_chan2angl(obs%head,c(2),x(2))
    endif
  endif
  !
  call relocate(x(1),y)
  call draw(x(2),y)
  !
end subroutine spectr1d_draw_histo1chan
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine spectr2d(obs,error)
  use gildas_def
  use gbl_message
  use classcore_interfaces, except_this=>spectr2d
  use class_data
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS Internal routine
  !
  ! Plot the INDEX as a 2D image.
  !---------------------------------------------------------------------
  type(observation), intent(in)  :: obs    ! Observation to be plotted
  logical,           intent(out) :: error  ! Logical error flag
  ! Global
  logical :: gr_error
  ! Local
  integer :: nx,ny
  character(len=256) :: ch
  real(kind=8) :: conv(6)
  !
  ny = obs%head%des%ndump
  nx = obs%head%spe%nchan
  if (nx.le.0.or.ny.le.0) then
     write(ch,'(a,2i7)') 'Bad dimensions for input array:',nx,ny
     call class_message(seve%e,'SPECTR2D',ch)
     return
  endif
  !
  ! send the plot limits to GreG
  call get_box(gx1,gx2,gy1,gy2)
  write(ch,'(a,4(1x,g20.13))') 'LIMITS ',gux1,gux2,guz1,guz2
  call gr_exec(ch(1:91))
  error = gr_error()
  if (error) return
  !
  ! Plot the array
  conv(1) = gcx1
  conv(2) = gux1
  conv(3) = (gux2-gux1)/(gcx2-gcx1)
  conv(4) = 0
  conv(5) = 0
  conv(6) = 1
  call gr4_rgive(nx,ny,conv,p%data2)  ! Load the RG data
  !
  ! Create a RG image-segment in GreG
  write(ch,'(a,2g20.5,a,g20.6,a)')  &
    'PLOT /SCALING LINEAR ',guy1,guy2,' /BLANK ',obs_bad(obs%head),' 0.'
  call gr_exec(ch)
  error = gr_error()
  !
end subroutine spectr2d
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
