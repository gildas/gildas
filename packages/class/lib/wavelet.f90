module class_wavelets
  !---------------------------------------------------------------------
  ! Support module for command LAS\WAVELET
  !---------------------------------------------------------------------
  real(kind=4), allocatable :: wavelets(:,:)  ! Support for Sic variable 'WAVELET'
end module class_wavelets
!
subroutine class_wavelet(line,r,error,user_function)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_wavelet
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! Support routine for command
  !  WAVELET [/BASE]
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: line
  type(observation), intent(inout) :: r
  logical,           intent(inout) :: error
  logical,           external      :: user_function
  ! Local
  character(len=*), parameter :: rname='WAVELET'
  integer(kind=4), parameter :: optbase=1
  integer(kind=4), parameter :: optplot=2
  logical :: dobase,doplot
  integer(kind=4) :: base_order,plotpen
  !
  if (r%head%xnum.eq.0) then
    call class_message(seve%e,rname,'No R spectrum in memory')
    error = .true.
    return
  endif
  !
  ! /BASE
  dobase = sic_present(optbase,0)
  if (dobase) then
    base_order = 5  ! Default
    call sic_i4(line,optbase,1,base_order,.false.,error)
    if (error) return
  else
    base_order = 0 ! => R not modified on return
  endif
  !
  ! /PLOT
  doplot = dobase.and.sic_present(optplot,0)
  plotpen = 1
  call sic_i4(line,optplot,1,plotpen,.false.,error)
  if (error) return
  !
  call wavelet_obs(r,base_order,doplot,plotpen,error)
  if (error) return
  !
end subroutine class_wavelet
!
subroutine wavelet_obs(obs,iorder,doplot,plotpen,error)
  use gildas_def
  use gkernel_interfaces
  use classcore_interfaces, except_this=>wavelet_obs
  use class_wavelets
  use class_types
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Compute and modify inplace the wavelets for input observation
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  integer(kind=4),   intent(in)    :: iorder   ! /BASE order?
  logical,           intent(in)    :: doplot   !
  integer(kind=4),   intent(in)    :: plotpen  !
  logical,           intent(inout) :: error    !
  ! Local
  integer(kind=4) :: nchan,odeg
  integer(kind=index_length) :: dims(2)
  character(len=*), parameter :: rname='WAVELET'
  !
  ! Remove previous SIC variable when it exists
  call sic_delvariable('WAVELET',.false.,error)
  if (error) error = .false.
  ! Compute the wavelet array
  nchan = obs%cnchan
  call gwavelet_gaps(obs%spectre(1:nchan),wavelets,error)
  if (error) return
  ! Redefine the SIC variable when the computation succeeded
  dims(1) = nchan
  dims(2) = size(wavelets,2)
  call sic_def_real('WAVELET',wavelets,2,dims,.true.,error)
  if (error) return
  call gwavelet_subtract(iorder,wavelets,obs%spectre(1:nchan),error)
  if (error) return
  !
  if (doplot) then
    ! NB1: this needs a non-zero iorder, i.e. /BASE was invoked
    ! NB2: temporarily set obs%head%bas%deg to -2, pseudo-code for
    ! BASE WAVELET (similar to BASE SINUS, code -1). But since this
    ! is not yet in the standard, undo this right after the plot.
    odeg = obs%head%bas%deg
    obs%head%bas%deg = -2
    call baseline_plot(obs,wavelets(:,iorder),plotpen,error)
    if (error)  return
    obs%head%bas%deg = odeg
  endif
  !
end subroutine wavelet_obs
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
