subroutine las_tofits(set,obs,check,error)
  use gildas_def
  use phys_const
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>las_tofits
  use class_fits
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Class private routine
  ! Write a single spectrum (the current one) in a FITS file
  ! Current limitations:
  !  - Horizontal offsets incorrectly handled.
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(observation),   intent(inout) :: obs    ! The input observation
  logical,             intent(in)    :: check  ! Echo card images on terminal
  logical,             intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: proc='TOFITS'
  integer(kind=4), parameter :: buflen=2880
  real(kind=4), parameter :: epsr4=1e-7
  integer(kind=1) :: buffer(buflen)
  character(len=80) :: mess,line
  character(len=20) :: chain
  character(len=8) :: specsys,radesys,velkey
  real(kind=4) :: obsmin,obsmax,equinox
  integer(kind=4) :: idate,i,n,id,iy,im,ni2,ni4,nr4,velref
  integer(kind=size_length) :: idata, kdata
  real(kind=8) :: ra,dec,posang,off1,off2,frq,frqres,rchan
  character(len=4), parameter :: pcode(0:mproj) =  &
    (/ '    ','-TAN','-SIN','-ARC','-STG','lamb',  &
       '-AIT','-GLS','-SFL','-MOL','-NCP','-CAR' /)
  !
  if (obs%head%xnum.eq.0) then
     call class_message(seve%e,proc,'No spectrum in memory.')
     error = .true.
     return
  elseif (obs%head%presec(class_sec_xcoo_id)) then
     call class_message(seve%e,proc,  &
       'Irregularly sampled data not yet supported')
     error = .true.
     return
  endif
  !
  !           123456789012345678901234567890
  if (fitput('SIMPLE  =                    T         /',check)) goto 99
  write (line,10) 'BITPIX  =           ',fits%head%desc%nbit
  if (fitput(line,check)) goto 99
  !
  ! Defines 4 different axis
  !
  if (fitput('NAXIS   =                    4         /',check)) goto 99
  ! Data type dependent information
  if (obs%head%gen%kind.eq.kind_spec) then
     fits%head%desc%ndata  = obs%head%spe%nchan
     write (line,10) 'NAXIS1  =           ',fits%head%desc%ndata
     if (fitput(line,check)) goto 99
     if (fitput('NAXIS2  =                    1         /',check)) goto 99
     if (fitput('NAXIS3  =                    1         /',check)) goto 99
     posang = 0.d0  ! Axes are not rotated
  elseif (obs%head%gen%kind.eq.kind_cont) then
     fits%head%desc%ndata  = obs%head%dri%npoin
     if (fitput('NAXIS1  =                    1         /',check)) goto 99
     posang = mod(dble(obs%head%dri%apos),2.d0*pi)
     ! Should check here if PI/2 rotations can bring scan direction along one of
     ! the main axes
     if (posang.gt.(pi-epsr4)) then
        posang = posang-pi
        obs%head%dri%ares  = -obs%head%dri%ares
     endif
     if (abs(posang).le.epsr4) posang = 0
     !	    IF (ABS(POSANG-PI/2).LE.EPSR4) THEN
     !	    ELSE		! Not along an axis: will have to use CROTA
     !	    ENDIF
     !
     write (line,10) 'NAXIS2  =           ',fits%head%desc%ndata
     if (fitput(line,check)) goto 99
     if (fitput('NAXIS3  =                    1         /',check)) goto 99
  else
     if (obs%head%gen%kind.eq.kind_sky) then
        call class_message(seve%e,proc,'Skydips not yet supported')
     else
        call class_message(seve%e,proc,'Unknown data type. Corrupted data file.')
     endif
     error = .true.
     return
  endif
  if (fitput('NAXIS4  =                    1         /',check)) goto 99
  !
  ! Compute extrema
  i = obs%cnchan
  call class_minmax (obsmin,obsmax,obs%spectre,i,obs%cbad)
  if (fits%head%desc%nbit.eq.16) then
     fits%head%bscal = (obsmax - obsmin) / 65534.
     fits%head%bzero  = obsmin + fits%head%bscal * 32768.
     write (line,10) 'BLANK   =           ',32767,'Blanking value'
     if (fitput(line,check)) goto 99
     write (line,20) 'BSCALE  = ',fits%head%bscal
     if (fitput(line,check)) goto 99
     write (line,20) 'BZERO   = ',fits%head%bzero
     if (fitput(line,check)) goto 99
  elseif (fits%head%desc%nbit.eq.32) then
     fits%head%bscal = (obsmax - obsmin) / 4294967294.
     fits%head%bscal = fits%head%bscal * (1-epsr4)  ! Allow some room for roundoff
     fits%head%bzero  = obsmin + fits%head%bscal * 2147483648.
     write (line,10) 'BLANK   =           ',2147483647,'Blanking value'
     if (fitput(line,check)) goto 99
     write (line,20) 'BSCALE  = ',fits%head%bscal
     if (fitput(line,check)) goto 99
     write (line,20) 'BZERO   = ',fits%head%bzero
     if (fitput(line,check)) goto 99
  elseif (fits%head%desc%nbit.eq.-32) then
     continue
  else
     write(mess,'(A,I6)') 'Invalid format. Internal logic error',fits%head%desc%nbit
     call class_message(seve%e,proc,mess)
     goto 99
  endif
  write (line,20) 'DATAMIN = ',obsmin
  if (fitput(line,check)) goto 99
  write (line,20) 'DATAMAX = ',obsmax
  if (fitput(line,check)) goto 99
  if (obs%head%cal%foeff.eq.obs%head%cal%beeff) then
     write (line,30) 'BUNIT   = ','K (Ta*)'
  else
     write (line,30) 'BUNIT   = ','K (Tmb)'
  endif
  if (fitput(line,check)) goto 99
  !
  ! First axis: Frequency
  !
  if (fitput('CTYPE1  = ''FREQ    ''                   /',check)) goto 99
  write (line,20) 'CRVAL1  = ',0.d0,'Offset frequency'
  if (fitput(line,check)) goto 99
  if (obs%head%gen%kind.eq.kind_cont) then
     frqres = obs%head%dri%width
     rchan  = 0.5d0
  else
     frqres = obs%head%spe%fres
     rchan  = obs%head%spe%rchan
  endif
  write (line,20) 'CDELT1  = ',frqres*1.d6,'Frequency resolution'
  if (fitput(line,check)) goto 99
  write (line,20) 'CRPIX1  = ',rchan
  if (fitput(line,check)) goto 99
  !
  ! Bring the offsets onto the rotated axes
  off1 =  obs%head%pos%lamof*cos(posang)+obs%head%pos%betof*sin(posang)
  off2 = -obs%head%pos%lamof*sin(posang)+obs%head%pos%betof*cos(posang)
  if (obs%head%gen%kind.eq.kind_cont) off1 = off1+obs%head%dri%aref
  !
  ! Axis 2 : Right Ascension or Lii
  !
  if (obs%head%pos%system.eq.type_ga) then
     line = 'CTYPE2  = ''GLON'//pcode(obs%head%pos%proj)//'''                   /'
     if (fitput(line,check)) goto 99
  elseif (obs%head%pos%system.eq.type_eq .or. obs%head%pos%system.eq.type_ic) then
     line = 'CTYPE2  = ''RA--'//pcode(obs%head%pos%proj)//'''                   /'
     if (fitput(line,check)) goto 99
  elseif (obs%head%pos%system.ge.0) then
     if (fitput('CTYPE2  = ''ANGLE   ''                   /',check)) goto 99
     ! Not correct for continuum drifts
  elseif ((obs%head%pos%lamof**2+obs%head%pos%betof**2).le.set%tole**2) then
     if (abs(obs%head%pos%system).eq.type_ga) then
        line = 'CTYPE2  = ''GLON'//pcode(obs%head%pos%proj)//'''                   /'
        if (fitput(line,check)) goto 99
     elseif (abs(obs%head%pos%system).eq.type_eq) then
        line = 'CTYPE2  = ''RA--'//pcode(obs%head%pos%proj)//'''                   /'
        if (fitput(line,check)) goto 99
     else
        if (fitput('CTYPE2  = ''ANGLE   ''                   /',check)) goto 99
     endif
  else
     if (fitput('CTYPE2  = ''ANGLE   ''                   /',check)) goto 99
  endif
  !
  ! For projected data, the reference point has to be the projection centre.
  write (line,20) 'CRVAL2  = ',obs%head%pos%lam*180.d0/pi
  if (fitput(line,check)) goto 99
  if (obs%head%gen%kind.eq.kind_spec) then
     write (line,20) 'CDELT2  = ',dble(off1)*180.d0/pi
     if (fitput(line,check)) goto 99
     write (line,20) 'CRPIX2  = ',0.0
     if (fitput(line,check)) goto 99
  elseif (obs%head%gen%kind.eq.kind_cont) then
     write (line,20) 'CDELT2  = ',dble(obs%head%dri%ares)*180.d0/pi
     if (fitput(line,check)) goto 99
     write (line,20) 'CRPIX2  = ', obs%head%dri%rpoin-off1/obs%head%dri%ares
     if (fitput(line,check)) goto 99
  endif
  !
  ! Axis 3 : Declination, or galactic latitude
  !
  if (obs%head%pos%system.eq.type_ga) then
     line = 'CTYPE3  = ''GLAT'//pcode(obs%head%pos%proj)//'''                   /'
     if (fitput(line,check)) goto 99
  elseif (obs%head%pos%system.eq.type_eq .or. obs%head%pos%system.eq.type_ic) then
     line = 'CTYPE3  = ''DEC-'//pcode(obs%head%pos%proj)//'''                   /'
     if (fitput(line,check)) goto 99
  elseif (obs%head%pos%system.ge.0) then
     if (fitput('CTYPE3  = ''ANGLE   ''                   /',check)) goto 99
  elseif ((obs%head%pos%lamof**2+obs%head%pos%betof**2).le.set%tole**2) then
     if (abs(obs%head%pos%system).eq.type_ga) then
        line = 'CTYPE3  = ''GLAT'//pcode(obs%head%pos%proj)//'''                   /'
        if (fitput(line,check)) goto 99
     elseif (abs(obs%head%pos%system).eq.type_eq) then
        line = 'CTYPE3  = ''DEC-'//pcode(obs%head%pos%proj)//'''                   /'
        if (fitput(line,check)) goto 99
     else
        if (fitput('CTYPE3  = ''ANGLE   ''                   /',check)) goto 99
     endif
  else
     if (fitput('CTYPE3  = ''ANGLE   ''                   /',check)) goto 99
  endif
  write (line,20) 'CRVAL3  = ',obs%head%pos%bet*180.d0/pi
  if (fitput(line,check)) goto 99
  write (line,20) 'CDELT3  = ',dble(off2)*180.d0/pi
  if (fitput(line,check)) goto 99
  write (line,20) 'CRPIX3  = ',0.0
  if (fitput(line,check)) goto 99
  if (posang.ne.0) then
     write (line,20) 'CROTA3  = ',dble(posang)*180.d0/pi
     if (fitput(line,check)) goto 99
  endif
  !
  ! Axis 4 : Polarisation
  !
  if (fitput('CTYPE4  = ''STOKES  ''                   /',check)) goto 99
  if (fitput('CRVAL4  =      1.0000000000000         /',check)) goto 99
  if (fitput('CDELT4  =      1.0000000000000         /',check)) goto 99
  if (fitput('CRPIX4  =      0.0000000000000         /',check)) goto 99
  !
  ! Miscellaneous
  call my_get_teles(proc,obs%head%gen%teles,.false.,chain,error)
  if (chain.ne.'') then
    write (line,30) 'TELESCOP= ',trim(chain)
    if (fitput(line,check)) goto 99
  endif
  write (line,30) 'INSTRUME= ',obs%head%gen%teles(1:12)
  if (fitput(line,check)) goto 99
  write (line,30) 'OBJECT  = ',trim(obs%head%pos%sourc)
  if (fitput(line,check)) goto 99
  call tofits_radesys(set,obs%head,radesys,equinox,ra,dec,error)
  if (error)  return
  if (radesys.ne.'') then
    write (line,30) 'RADESYS = ',radesys
    if (fitput(line,check)) goto 99
  endif
  if (equinox.ne.0.) then
    write (line,20) 'EQUINOX = ',equinox
    if (fitput(line,check)) goto 99
  endif
  if (ra.ne.0.d0 .or. dec.ne.0.d0) then
    write (line,20) 'RA      = ',ra,'Right Ascension'
    if (fitput(line,check)) goto 99
    write (line,20) 'DEC     = ',dec,'Declination'
    if (fitput(line,check)) goto 99
  endif
  !
  ! Spectral line information
  !
  write (line,30) 'LINE    = ',trim(obs%head%spe%line),'Line name'
  if (fitput(line,check)) goto 99
  if (obs%head%gen%kind.eq.kind_cont) then
     frq = obs%head%dri%freq
  else
     frq = obs%head%spe%restf
  endif
  write (line,20) 'RESTFREQ= ',frq*1d6,'Rest frequency'
  if (fitput(line,check)) goto 99
  call tofits_specsys(obs%head,velkey,velref,specsys,error)
  if (error)  return
  if (velkey.ne.'') then
     write (line,20) velkey//'= ',obs%head%spe%voff*1d3,'[m/s] Velocity of reference channel'
     if (fitput(line,check)) goto 99
  endif
  if (specsys.ne.'') then
     write (line,30) 'SPECSYS = ',trim(specsys),'Reference frame'
     if (fitput(line,check)) goto 99
  endif
  if (velref.gt.0) then
     write (line,10) 'VELREF  =           ',velref,'>256 RADIO, 1 LSR 2 HEL 3 OBS'
     if (fitput(line,check)) goto 99
  endif
  write (line,20) 'DELTAV  = ',obs%head%spe%vres*1d3,'Velocity spacing of channels'
  if (fitput(line,check)) goto 99
  write (line,20) 'IMAGFREQ= ',obs%head%spe%image*1d6,'Image frequency'
  if (fitput(line,check)) goto 99
  !
  ! Integration observation
  write (line,20) 'TSYS    = ',obs%head%gen%tsys,'System temperature'
  if (fitput(line,check)) goto 99
  write (line,20) 'OBSTIME = ',obs%head%gen%time,'Integration time'
  if (fitput(line,check)) goto 99
  write (line,20) 'SCAN-NUM= ',float(obs%head%gen%num),'Scan number'
  if (fitput(line,check)) goto 99
  write (line,20) 'TAU-ATM = ',obs%head%gen%tau,'Atmospheric opacity'
  if (fitput(line,check)) goto 99
  !
  ! Observing mode
  if (obs%head%presec(class_sec_swi_id) .or. obs%head%swi%nphas.ge.1) then
     write (line,10) 'NPHASE  =           ',obs%head%swi%nphas,'Number of frequency phases'
     if (fitput(line,check)) goto 99
     do i=1,obs%head%swi%nphas
        write (line,50) 'DELTAF',i,' = ',obs%head%swi%decal(i)*1d6,'Frequency offset Phase ',i
        if (fitput(line,check)) goto 99
        write (line,50) 'PTIME',i,'  = ',obs%head%swi%duree(i),'Duration of Phase ',i
        if (fitput(line,check)) goto 99
        write (line,50) 'WEIGHT',i,' = ',obs%head%swi%poids(i),'Weight of Phase ',i
        if (fitput(line,check)) goto 99
     enddo
  endif
  !
  ! Calibration parameters
  if (obs%head%presec(class_sec_cal_id)) then
     write (line,20) 'GAINIMAG= ',obs%head%cal%gaini,'Image sideband gain ratio'
     if (fitput(line,check)) goto 99
     write (line,20) 'BEAMEFF = ',obs%head%cal%beeff,'Beam efficiency'
     if (fitput(line,check)) goto 99
     write (line,20) 'FORWEFF = ',obs%head%cal%foeff,'Image sideband gain ratio'
     if (fitput(line,check)) goto 99
  endif
  !
  ! Resolution section
  if (obs%head%presec(class_sec_res_id)) then
     write (line,20) 'BMAJ    = ',obs%head%res%major*deg_per_rad,'[deg] Beam major axis'
     if (fitput(line,check)) goto 99
     write (line,20) 'BMIN    = ',obs%head%res%minor*deg_per_rad,'[deg] Beam minor axis'
     if (fitput(line,check)) goto 99
     write (line,20) 'BPA     = ',obs%head%res%posang*deg_per_rad,'[deg] Beam position angle'
     if (fitput(line,check)) goto 99
  endif
  !
  ! Miscellaneous
  if (fitput('ORIGIN  = ''CLASS-Grenoble  ''           /',check)) goto 99
  call sic_gagdate(idate)
  call gag_jdat(idate,id,im,iy)
  if (iy.lt.1999) then
     iy = mod(iy,100)
     write (line,12) 'DATE    = ''',id,im,iy,'Date written'
  else
     write (line,13) 'DATE    = ''',iy,im,id,'00:00:00.000','Date written'
  endif
  if (fitput(line,check)) goto 99
  do while (obs%head%gen%ut.lt.0)
     obs%head%gen%ut   = obs%head%gen%ut+2*pi
     obs%head%gen%dobs = obs%head%gen%dobs-1
  enddo
  do while (obs%head%gen%st.lt.0)
     obs%head%gen%st = obs%head%gen%st+2*pi
  enddo
  call gag_jdat(obs%head%gen%dobs,id,im,iy)
  call sexag(chain,obs%head%gen%ut,24)
  call sic_black(chain,i)
  ! Fix UT hour format to 12 char
  if (fitput('TIMESYS = ''UTC             ''           /',check)) goto 99
  ! RL: FITS recommends to use the old date format until Jan 1st, 1999.
  ! this is because data may be propagated faster than software.
  if (iy.lt.1999) then
     iy = mod(iy,100)
     write (line,12) 'DATE-OBS= ''',id,im,iy,'Date observed'
  else
     write (line,13) 'DATE-OBS= ''',iy,im,id,chain(1:12),'Date observed'
  endif
  if (fitput(line,check)) goto 99
  call gag_jdat(obs%head%gen%dred,id,im,iy)
  if (iy.lt.1999) then
     iy = mod(iy,100)
     write (line,12) 'DATE-RED= ''',id,im,iy,'Date reduced'
  else
     write (line,13) 'DATE-RED= ''',iy,im,id,'00:00:00.000','Date reduced'
  endif
  if (fitput(line,check)) goto 99
  !
  write (line,20) 'ELEVATIO= ',180.d0*obs%head%gen%el/pi,' Telescope elevation'
  if (fitput(line,check)) goto 99
  write (line,20) 'AZIMUTH = ',180.d0*obs%head%gen%az/pi,' Telescope azimuth'
  if (fitput(line,check)) goto 99
  call sexag(chain,obs%head%gen%ut,24)
  write (line,31) 'UT      = ',chain, ' Universal time at start'
  if (fitput(line,check)) goto 99
  call sexag(chain,obs%head%gen%st,24)
  write (line,31) 'LST     = ',chain, ' Sideral time at start'
  if (fitput(line,check)) goto 99
  ! History
  line = 'HISTORY SCAN-LIST '
  n = 20
  do i =1,obs%head%his%nseq
     write(line(n:),29) obs%head%his%start(i),obs%head%his%end(i)
     n = n+20
     if (mod(i,3).eq.0) then
        if (fitput(line,check)) goto 99
        n = 20
     endif
  enddo
  if (mod(obs%head%his%nseq,3).ne.0) then
     if (fitput(line,check)) goto 99
  endif
  !
  ! Finish Header
  if (fitput('END                         ',check)) goto 99
  call gfits_flush_header(error)
  if (error) goto 99
  !
  ! Write data
  idata = 0
  kdata = fits%head%desc%ndata
  ni2 = buflen/2  ! Buffer size in 2-bytes words
  ni4 = buflen/4  ! Buffer size in 4-bytes words
  nr4 = buflen/4  ! Buffer size in 4-bytes words
  do while (idata.lt.kdata)
     buffer(:) = 0  ! Bytes which are left unused will be zero, as required by FITS
     if (fits%head%desc%nbit.eq.16) then
        call real_to_int2 (buffer,ni2,obs%spectre,kdata,idata,fits%head%bscal,  &
          fits%head%bzero,obs%cbad,abs(obs%cbad)*epsr4)
     elseif (fits%head%desc%nbit.eq.32) then
        call real_to_int4 (buffer,ni4,obs%spectre,kdata,idata,fits%head%bscal,  &
          fits%head%bzero,obs%cbad,abs(obs%cbad)*epsr4)
     elseif (fits%head%desc%nbit.eq.-32) then
        call real_to_real4 (buffer,nr4,obs%spectre,kdata,idata,  &
          obs%cbad,abs(obs%cbad)*epsr4)
     else
        write(mess,'(a,i6)') 'Invalid format. Internal logic error BITPIX ',fits%head%desc%nbit
        call class_message(seve%e,proc,mess)
        goto 99
     endif
     call gfits_putbuf(buffer,buflen,error)
  enddo
  return
  !
99 error = .true.
  return
  !
10 format(a,i10,'         / ',a)
12 format(a,i2.2,'/',i2.2,'/',i2.2,'''                   / ',a)
13 format(a,i4.4,'-',i2.2,'-',i2.2,'T',a12,'''    / ',a)
20 format(a,e20.12e3,'         / ',a)  ! Support for large exponents e.g. -0.123456d-123
29 format(i8,'-',i8,3x)
30 format(a,'''',a,'''',t40,'/ ',a)
31 format(a,'''',a13,'''','              / ',a)
50 format(a,i1,a,e20.12e3,'         / ',a,i1)
end subroutine las_tofits
!
function fitput(line,check)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! @ private
  !  Write a FITS line of the header
  !---------------------------------------------------------------------
  logical :: fitput                     ! Function return value
  character(len=*), intent(in) :: line  ! Line to be written in FITS
  logical, intent(in) :: check          ! Echo line on terminal
  ! Local
  logical :: error
  !
  error = .false.
  call gfits_put(line,check,error)
  fitput = error
end function fitput
!
subroutine tofits_radesys(set,head,radesys,equinox,ra,dec,error)
  use phys_const
  use gbl_constant
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>tofits_radesys
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Convert the current position section to:
  !  - RADESYS is relevant
  !  - EQUINOX if relevant
  !  - alternate RA, DEC if relevant (i.e. if starting from galactic)
  ! suited for FITS.
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(header),        intent(in)    :: head
  character(len=*),    intent(out)   :: radesys
  real(kind=4),        intent(out)   :: equinox  ! [yr]
  real(kind=8),        intent(out)   :: ra,dec   ! [deg]
  logical,             intent(inout) :: error
  ! Local
  real(kind=4) :: raoff,decoff
  !
  ! Defaults to be ignored if not changed
  radesys = ''
  equinox = 0.
  ra = 0.d0
  dec = 0.d0
  !
  select case (head%pos%system)
  case (type_ic)
     radesys = 'ICRS'
  case (type_eq)
     if (nearly_equal(head%pos%equinox,1950.,1e-6)) then
        radesys = 'FK4'
     elseif (nearly_equal(head%pos%equinox,2000.,1e-6)) then
        radesys = 'FK5'
     endif
     equinox = head%pos%equinox
  case (type_ga)
     ! Use preset equinox
     call gal_to_equ(head%pos%lam,head%pos%bet,0.0,0.0,  &
                     ra,dec,raoff,decoff,set%equinox,error)  ! ZZZ better go to ICRS
     if (error)  return
     ra = ra*deg_per_rad
     dec = dec*deg_per_rad
     equinox = set%equinox
  end select
  !
end subroutine tofits_radesys
!
subroutine tofits_specsys(head,velkey,velref,specsys,error)
  use classcore_interfaces, except_this=>tofits_specsys
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Compute the following FITS keywords for the given observation:
  !   VELO*
  !   VELREF
  !   SPECSYS
  !---------------------------------------------------------------------
  type(header),     intent(in)    :: head
  character(len=*), intent(out)   :: velkey
  integer(kind=4),  intent(out)   :: velref
  character(len=*), intent(out)   :: specsys
  logical,          intent(inout) :: error
  !
  velkey = ''
  velref = 0
  specsys = ''
  !
  select case (head%spe%vtype)
  case (vel_lsr)
    velkey = 'VELO-LSR'
    velref = 1
    specsys = 'LSRK'
  case(vel_hel)
    velkey = 'VELO-HEL'
    velref = 2
    specsys = 'BARYCENT'
  case (vel_obs)
    velkey = 'VELO-TOP'
    velref = 3
    specsys = 'TOPOCENT'
  case (vel_ear)
    velkey = 'VELO-EAR'
  case default
    velkey = 'VELO    '
  end select
  !
  if (velref.gt.0) then
    select case (head%spe%vconv)
    case (vconv_opt)
      velref = 0+velref
    case (vconv_rad)
      velref = 256+velref
    case default
      velref = 0
    end select
  endif
  !
end subroutine tofits_specsys
