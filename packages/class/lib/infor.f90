subroutine allinfo(obs,v1,v2,v,s0,data1)
  use gildas_def
  use classcore_interfaces, except_this=>allinfo
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  !---------------------------------------------------------------------
  type(observation), intent(in)  :: obs    !
  real(kind=8),      intent(in)  :: v1     ! Lower bound of velocity interval
  real(kind=8),      intent(in)  :: v2     ! Upper bound of velocity interval
  real(kind=4),      intent(out) :: v      !
  real(kind=4),      intent(out) :: s0     !
  real(kind=4),      intent(out) :: data1  !
  ! Local
  real(kind=8) :: ri1,ri2
  integer(kind=4) :: i1,i2,imin,imax,nmax(1)
  !
  ! Compute the integrated area,Tmax,V of MaX
  call abscissa_velo2chan(obs%head,v1,ri1)
  call abscissa_velo2chan(obs%head,v2,ri2)
  i1 = nint(ri1)
  i2 = nint(ri2)
  imin = max(min(i1,i2),1)
  imax = min(max(i1,i2),obs%head%spe%nchan)
!!$  s0 = 0
!!$  n = 0
!!$  v = 0
!!$  nc=imin
!!$  data1 = obs%spectre(nc)
!!$  do while(obs%spectre(nc).eq.obs%head%spe%bad.and.nc.le.imax)
!!$     nc=nc+1
!!$  enddo
!!$  if (nc.lt.imax) data1 = obs%spectre(nc)
!!$  datav = 0.0
!!$  do i=nc,imax
!!$     if (obs%spectre(i).ne.obs%head%spe%bad) then
!!$        datav = obs%spectre(i)
!!$        n = n+1
!!$        s0 = s0+datav
!!$        if (datav.gt.data1) then
!!$           data1=datav
!!$           call abscissa_chan2velo(obs%head,i,v)
!!$        endif
!!$     endif
!!$  enddo
!!$  ! Transforms K*channels to K*km/s
!!$  if (n.ne.0) then
!!$     s0 = s0*abs(obs%head%spe%vres)
!!$  else
!!$     s0 = obs%head%spe%bad
!!$  endif
  data1 = maxval(obs%spectre(imin:imax),1,obs%spectre(imin:imax).ne.obs%head%spe%bad)
  s0    = sum   (obs%spectre(imin:imax),1,obs%spectre(imin:imax).ne.obs%head%spe%bad)
  nmax  = maxloc(obs%spectre(imin:imax),  obs%spectre(imin:imax).ne.obs%head%spe%bad)
  nmax  = imin+nmax-1
  call abscissa_chan2velo(obs%head,real(nmax(1)),v)
  if (s0.eq.0.) then
     s0 = obs%head%spe%bad
  else
     s0 = s0*abs(obs%head%spe%vres)
  endif
end subroutine allinfo
