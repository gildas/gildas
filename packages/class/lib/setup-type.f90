module class_setup_new  ! To be renamed to 'class_setup' when the old
                        ! module is removed
  use phys_const
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Support module for the SET structure
  ! No direct access to the SET structure. Only use methods to get and
  ! set values.
  !---------------------------------------------------------------------
  !
  type class_setup_new_t
    real(kind=8) :: fangle=sec_per_rad  ! Angle conversion factor
    !
    character(len=3) :: angle='SEC'  ! Angle unit
    !
  end type class_setup_new_t
  type(class_setup_new_t) :: set
  !
  public :: class_setup_put_fangle,class_setup_get_fangle
  public :: class_setup_put_angle, class_setup_get_angle,class_setup_sicdef_angle
  private
  !
contains
  !
  subroutine class_setup_put_fangle(fangle,error)
    real(kind=8), intent(in)    :: fangle
    logical,      intent(inout) :: error
    set%fangle = fangle
  end subroutine class_setup_put_fangle
  function class_setup_get_fangle()
    real(kind=8) :: class_setup_get_fangle
    class_setup_get_fangle = set%fangle
  end function class_setup_get_fangle
  !
  subroutine class_setup_put_angle(angle,error)
    character(len=*), intent(in)    :: angle
    logical,          intent(inout) :: error
    set%angle = angle
  end subroutine class_setup_put_angle
  function class_setup_get_angle()
    character(len=3) :: class_setup_get_angle
    class_setup_get_angle = set%angle
  end function class_setup_get_angle
  subroutine class_setup_sicdef_angle(struct,error)
    character(len=*), intent(in)    :: struct
    logical,          intent(inout) :: error
    call sic_def_char(struct//'%ANGLE',set%angle,.true.,error)
  end subroutine class_setup_sicdef_angle
  !
end module class_setup_new
