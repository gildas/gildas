subroutine wincur(set,mw,nw,w1,w2,wtype)
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>wincur
  use class_setup_new
  use class_types
  use plot_formula
  !----------------------------------------------------------------------
  ! @ private
  !  Use the cursor to setup a list of windows, or masks
  !----------------------------------------------------------------------
  type(class_setup_t), intent(in)  :: set     !
  integer(kind=4),     intent(in)  :: mw      ! Maximum number of regions
  integer(kind=4),     intent(out) :: nw      ! Actual number of regions
  real(kind=4),        intent(out) :: w1(mw)  ! Lower bounds of regions
  real(kind=4),        intent(out) :: w2(mw)  ! Upper bounds of regions
  character(len=*),    intent(in)  :: wtype   ! Type of regions (Window or Mask)
  ! Local
  integer(kind=4) :: iw,i
  real(kind=4) :: xv,x,y
  logical :: in_interval,finish,error
  character(len=1) :: ch
  character(len=message_length) :: mess
  !
  nw = 0
  iw = nw+1
  in_interval = .false.        ! Outside an interval at start
  finish  = .false.
  error = .false.
  !
  do while (.not.finish)
     call gtcurs(x,y,ch,error)
     if (error)  return
     !
     if (ch.eq.'N' .or. ch.eq.'n' .or. ch.eq.' ' .or. ch.eq.'^') then
        in_interval = .not.in_interval
        xv = gux1 + (x-gx1)/gux
        if (in_interval) then
           if (nw.ge.mw) then
              finish = .true.
              in_interval = .false.
              call class_message(seve%w,wtype,  &
                'Maximum number of pairs reached. Exit.')
           else
              nw = nw+1
              w1(nw) = xv
              write(mess,100) wtype,nw,'low',xv
              call class_message(seve%r,wtype,mess)
           endif
        else
           w2(nw) = xv
           write(mess,100) wtype,nw,'up ',xv
           call class_message(seve%r,wtype,mess)
        endif
        !
     elseif (ch.eq.'H' .or. ch.eq.'h') then
        write(6,*) 'Type N or Space or Left button for setting next '//wtype//' boundary'
        write(6,*) '     E or Right button to finish'
        write(6,*) '     C or Middle button to correct last boundary'
        !
     elseif (ch.eq.'C' .or. ch.eq.'c'.or.ch.eq.'&') then
        if (nw.gt.0) then
           xv = gux1 + (x-gx1)/gux
           if (in_interval) then
              w1(nw) = xv
              write(mess,100) wtype,nw,'low',xv,' (corrected)'
              call class_message(seve%r,wtype,mess)
           else
              w2(nw) = xv
              write(mess,100) wtype,nw,'up ',xv,' (corrected)'
              call class_message(seve%r,wtype,mess)
           endif
        endif
        !
     elseif (ch.eq.'E' .or. ch.eq.'e'.or.ch.eq.'*') then
        if (in_interval) nw = nw-1
        finish = .true.
     endif
  enddo
  !
  if (in_interval)  &
    call class_message(seve%w,wtype,'Missing upper boundary, last lower ignored.')
  !
  ! Handle continuum drifts
  if (set%kind.eq.kind_cont) then
     do i=iw,nw
        w1(i) = w1(i)*class_setup_get_fangle()
        w2(i) = w2(i)*class_setup_get_fangle()
     enddo
  endif
  !
100 format(2x,A,' #',I0,' (',A,') : ',F0.1,A)
end subroutine wincur
