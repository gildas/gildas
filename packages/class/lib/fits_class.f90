subroutine fits_class(set,line,r,error,user_function)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>fits_class
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! CLASS Support for command FITS
  !
  ! GILDAS Syntax
  !     FITS File.fits FROM File.gdf /STYLE [/XTENSION Number]
  !     FITS File.gdf TO File.fits /STYLE /BITS
  ! Launches the FITS_GILDAS or GILDAS_FITS task to do the job.
  !
  ! CLASS Syntax
  !     FITS READ File.fits  [/CHECK]
  !     FITS WRITE File.fits [/BITS Nbits] [/MODE SPECTRUM|INDEX] [/CHECK]
  !
  ! Option sequence
  !     /BITS
  !     /STYLE
  !     /XTENSION
  !     /MODE
  !     /CHECK
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  character(len=*),    intent(in)    :: line           ! Command line
  type(observation),   intent(inout) :: r              !
  logical,             intent(inout) :: error          ! Error status
  logical,             external      :: user_function  ! intent(in)
  ! Local
  integer            :: n, nkey
  integer, parameter :: mstyle=5
  integer, parameter :: mact=2
  character(len=filename_length) :: argum
  character(len=12) :: key,action(mact)
  data action/'READ','WRITE'/
  !
  ! Check GILDAS syntax
  call sic_ch(line,0,2,argum,n,.true.,error)
  if (error) return
  call sic_upper(argum)
  if (argum.eq.'FROM' .or. argum.eq.'TO') then
     call fits_gildas(line,error)
     return
  endif
  !
  ! Else, use CLASS syntax
  call sic_ke(line,0,1,argum,n,.true.,error)
  call sic_ambigs('FITS',argum,key,nkey,action,mact,error)
  if (error) return
  !
  if (key.eq.'READ') then
     call fits_class_read(set,line,r,user_function,error)
  elseif (key.eq.'WRITE') then
     call fits_class_write(set,line,r,user_function,error)
  else
     call class_message(seve%e,'FITS_CLASS',key//' not yet supported')
     error = .true.
  endif
  !
end subroutine fits_class
!
subroutine fits_class_read(set,line,r,user_function,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>fits_class_read
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS FITS Support routine for command FITS READ
  ! Read current FITS file
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  character(len=*),    intent(in)    :: line           ! Command line
  type(observation),   intent(inout) :: r              !
  logical,             external      :: user_function  !
  logical,             intent(inout) :: error          ! Error status
  ! Local
  character(len=*), parameter :: proc='FITS_CLASS_READ'
  character(len=256) :: argum
  logical :: check
  integer :: n
  !
  error = .false.
  check = sic_present(5,0) ! Is /CHECK option present?
  argum = ' '
  call sic_ch (line,0,2,argum,n,.false.,error)
  if (error) return
  if (argum.ne.'*') then
     ! READ Fits_file
     call gfits_open(argum,'IN',error)
     if (error) return
     call toclass(set,r,check,user_function,error)
     call gfits_close(error)
  else
     ! READ *
     call class_message(seve%e,proc,'FITS READ * not yet implemented.')
  endif
end subroutine fits_class_read
!
subroutine fits_class_write(set,line,r,user_function,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>fits_class_write
  use class_fits
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS
  ! Support routine for command
  !   FITS WRITE File.fits [/BITS Nbits] [/MODE SPECTRUM|INDEX] [/CHECK]
  ! Option sequence
  !   1  /BITS
  !   2  /STYLE
  !   3  /XTENSION
  !   4  /MODE
  !   5  /CHECK
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  character(len=*),    intent(in)    :: line           ! Command line
  type(observation),   intent(inout) :: r              !
  logical,             external      :: user_function  !
  logical,             intent(inout) :: error          ! Error status
  ! Local
  character(len=*), parameter :: proc='FITS_CLASS_WRITE'
  logical :: check
  integer :: n,nkey
  character(len=256) :: argum
  character(len=message_length) :: mess
  integer, parameter :: mvocab=3
  character(len=12) :: vocab(mvocab),the_fits_mode
  data vocab/'SPECTRUM','INDEX','NONE'/
  !
  ! Decode input line arguments
  !
  ! /BITS option
  fits%head%desc%nbit = snbit
  if (sic_present(1,0)) then
    call sic_ch (line,1,1,argum,n,.true.,error)
    if (error) return
    if (argum.eq.'I*2') then
      fits%head%desc%nbit = 16
    elseif (argum.eq.'I*4') then
      fits%head%desc%nbit = 32
    elseif (argum.eq.'R*4') then
      fits%head%desc%nbit = -32
    else
      call sic_i4(line,1,1,fits%head%desc%nbit,.true.,error)
      if (error) return
      if (fits%head%desc%nbit.ne.16 .and.  &
          fits%head%desc%nbit.ne.32 .and.  &
          fits%head%desc%nbit.ne.-32) then
        call class_message(seve%e,proc,'Invalid number of bits')
        error = .true.
        return
      endif
    endif
  endif
  !
  ! /MODE option
  argum = fits_mode
  if (sic_present(4,0)) then
     call sic_ke (line,4,1,argum,nkey,.false.,error)
     call sic_ambigs('FITS',argum,the_fits_mode,nkey,vocab,mvocab,error)
  else
     call class_message(seve%e,proc,'Mode is undefined')
     error = .true.
     return
  endif
  !
  check = sic_present(5,0)
  !
  if (the_fits_mode.eq.'INDEX') then !
     ! Write all spectra as a Binary Table
     call sic_ch(line,0,2,argum,n,.true.,error)
     if (error) return
     !
     call gfits_open(argum,'OUT',error)
     if (error)  return
     call fits_save_index(set,check, user_function, error)
     call gfits_close(error)
     !
  elseif (the_fits_mode.eq.'SPECTRUM') then
     ! Write current spectrum
     if (r%head%xnum.eq.0) then
        call class_message(seve%e,proc,'No spectrum in memory.')
        error = .true.
        return
     endif
     ! Get the output file name
     call sic_ch (line,0,2,argum,n,.false.,error)
     if (error)  return
     !
     call gfits_open(argum,'OUT',error)
     if (error)  return
     call las_tofits(set,r,check,error)
     if (error) then
        write(mess,'(A,A,A)') 'File ',trim(argum),' incomplete'
        call class_message(seve%e,proc,mess)
        goto 99
     endif
     call gfits_close(error)
     !
  else
     ! Undefined
     call class_message(seve%e,proc,'Mode is undefined')
     error = .true.
  endif
  return
  !
99 error = .false.
  call gfits_close(error)       ! Just in case
  error = .true.
end subroutine fits_class_write
!
subroutine fits_select (line,error)
  use gkernel_interfaces
  use gbl_message
  use class_fits
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS Support routine for command SELECT.
  !     SET FITS BITS Nbits
  !     SET FITS MODE Spectrum|Index|None
  !		Define optional parameters for writing
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: line   ! Command line
  logical,          intent(out) :: error  ! Error status
  ! Local
  character(len=message_length) :: mess
  integer(kind=4) :: n,nkey,nbit
  integer(kind=4), parameter :: mact=2
  integer(kind=4), parameter :: mode=3
  character(len=12) :: argum,key,action(mact),modes(mode)
  ! Data
  data action/'BITS','MODE'/
  data modes/'SPECTRUM','INDEX','NONE'/
  !
  call sic_ke(line,0,2,argum,n,.true.,error)
  call sic_ambigs('FITS',argum,key,nkey,action,mact,error)
  if (error) return
  !
  if (key.eq.'BITS') then
     argum = ' '
     call sic_ch (line,0,3,argum,n,.false.,error)
     if (error) return
     if (argum.eq.'I*2') then
        nbit = 16
     elseif (argum.eq.'I*4') then
        nbit = 32
     elseif (argum.eq.'R*4') then
        nbit = -32
     else
        nbit = snbit
        call sic_i4(line,0,3,nbit,.false.,error)
        if (error) return
     endif
     if (nbit.eq.16 .or. nbit.eq.32 .or. nbit.eq.-32) then
        snbit = nbit
        write(mess,'(A,I4)') 'Number of bits ',snbit
        call class_message(seve%i,'FITS_SELECT',mess)
     else
        call class_message(seve%e,'FITS_SELECT','Invalid number of bits')
        error = .true.
     endif
     !
  elseif (key.eq.'MODE') then
     argum = fits_mode
     call sic_ke (line,0,3,argum,n,.false.,error)
     if (error) return
     call sic_ambigs('FITS',argum,key,nkey,modes,mode,error)
     if (error) return
     fits_mode = key
     call class_message(seve%i,'FITS_SELECT','Mode is '//fits_mode)
     !
  else
     call class_message(seve%e,'FITS_SELECT','Unknown keyword '//argum)
     error = .true.
  endif
end subroutine fits_select
