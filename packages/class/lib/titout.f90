subroutine titout(set,head,key1,key2)
  use classcore_interfaces, except_this=>titout
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  ! Write a title
  !----------------------------------------------------------------------
  type(class_setup_t), intent(in) :: set   !
  type(header),        intent(in) :: head  ! Observation header
  character(len=1),    intent(in) :: key1  ! Output Format type (Brief, Long, Full)
  character(len=1),    intent(in) :: key2  ! Output Format kind (INDEX or OBS)
  ! Local
  logical :: long,full,brief
  !
  ! Decode arguments
  brief = key1.eq.'B'
  long  = key1.eq.'L'
  full  = key1.eq.'F'
  !
  if (key2.eq.'I') then
    ! Index title
    call titout_index(set,head,brief)
  else
    call titout_observation(set,head,long,full)
  endif
  !
end subroutine titout
!
subroutine titout_index(set,head,brief)
  use gbl_constant
  use phys_const
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>titout_index
  use class_types
  use class_index
  !----------------------------------------------------------------------
  ! @ private
  ! Write a title
  !----------------------------------------------------------------------
  type(class_setup_t), intent(in) :: set    !
  type(header),        intent(in) :: head   ! Observation header
  logical,             intent(in) :: brief  ! Brief or long?
  ! Local
  character :: ch*88,ch1*80,ch2*40,ch3*40
  character :: off1*12,off2*12
  logical :: error
  !
  ! Brief title
  !
  !-- Source, Line
  !-- Absolute Coordinates
  if (abs(head%pos%system).eq.type_eq) then
    call sexag(ch2,head%pos%lam,24)
    call sexag(ch3,head%pos%bet,360)
    ch2 = adjustl(trim(ch2))
    ch3 = adjustl(trim(ch3))
    write(ch1,1999) head%pos%sourc,head%gen%teles,ch2,ch3,  &
                    obs_system(head%pos%system),head%pos%equinox
  elseif (abs(head%pos%system).eq.type_ic) then
    call sexag(ch2,head%pos%lam,24)
    call sexag(ch3,head%pos%bet,360)
    ch2 = adjustl(trim(ch2))
    ch3 = adjustl(trim(ch3))
    write(ch1,1999) head%pos%sourc,head%gen%teles,ch2,ch3,  &
                    obs_system(head%pos%system)
  else
    write(ch1,1104) head%pos%sourc,head%gen%teles,180.*head%pos%lam/pi,  &
                    180.*head%pos%bet/pi,obs_system(head%pos%system)
  endif
  call outlin(ch1,len_trim(ch1))
  !
  if (brief) then
    !-- Scan number or Scan range
    if (cx%ranges%scan1.eq.cx%ranges%scan2) then
        write(ch,1001) head%spe%line,cx%ranges%scan1
    else
        write(ch,1002) head%spe%line,cx%ranges%scan1,cx%ranges%scan2
    endif
    call outlin(ch,len_trim(ch))
    !
  else  ! Long or Full title
    !-- Scan number or Scan range
    if (cx%ranges%scan1.eq.cx%ranges%scan2) then
        write(ch3,1011) cx%ranges%scan1
    else
        write(ch3,1012) cx%ranges%scan1,cx%ranges%scan2
    endif
    !-- Obs date range
    call gag_todate(cx%ranges%dobs1,ch1,error)
    call gag_todate(cx%ranges%dobs2,ch2,error)
    write(ch,1004) trim(ch1),trim(ch2)
    ch = trim(adjustl(ch3))//"  "//trim(ch)
    call outlin(ch,len_trim(ch))
    !-- Number of spectra
    write(ch2,1003) cx%next-1
    ch = adjustl(trim(ch))//trim(ch1)
    !-- Offsets ranges
    call offsec(set,cx%ranges%lam1,off1)
    call offsec(set,cx%ranges%lam2,off2)
    ch = '('//trim(adjustl(off1))//':'//trim(adjustl(off2))//')'
    call offsec(set,cx%ranges%bet1,off1)
    call offsec(set,cx%ranges%bet2,off2)
    ch = trim(ch)//'  ('//trim(adjustl(off1))//':'//trim(adjustl(off2))//')'
    !
    ch = adjustl(trim(ch2))//'  Offset ranges: '//trim(ch)
    call outlin(ch,len_trim(ch))
    !-- Frequency information
    if (head%gen%kind.eq.kind_spec) then
      write(ch,1105) head%spe%nchan,head%spe%rchan,head%spe%voff,head%spe%vres, &
          obs_typev(head%spe%vtype)
      call outlin(ch,len_trim(ch))
      write(ch,1106) head%spe%line,head%spe%restf,head%spe%fres
      call outlin(ch,len_trim(ch))
    endif
    !-- Calibration information
    if (head%presec(class_sec_cal_id)) then
        write(ch,1110) head%cal%beeff,head%cal%foeff,head%spe%image,head%cal%gaini
        call outlin(ch,len_trim(ch))
    else
        write(ch,1110) 0.0,0.0,0.0
        call outlin(ch,len_trim(ch))
    endif
  endif
  !
1001 format(a12,2x,' Scan: ',i0)
1002 format(a12,2x,' Scan: ',i0,'-',i0)
1003 format(' Nspectra: ',i0)
1004 format('O: from ',a,' to ',a)
1011 format(' Scan: ',i0)
1012 format(' Scan: ',i0,'-',i0)
1104 format(a12,1x,a12,1x,' l: ',f8.3,2x,'b: ',f8.3,2x,a2)
1105 format('N:',i0,2x,'I0:',f8.1,2x,'V0:',1pg11.3,2x,'Dv:',1pg11.3,1x,a)
1106 format(a12,1x,'F0: ',f13.3,2x,'Df: ',1pg11.2)
1110 format('Bef:',f5.2,2x,'Fef:',f5.2,2x,'Fi: ',1pg16.9,2x,'Gim:',0pf6.3)
1999 format(a12,1x,a12,1x,'RA: ',a12,2x,'DEC: ',a12,2x,a4,f7.1)
end subroutine titout_index
!
subroutine titout_observation(set,head,long,full)
  use gbl_constant
  use phys_const
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>titout_observation
  use class_setup_new
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  ! Write a title
  !----------------------------------------------------------------------
  type(class_setup_t), intent(in) :: set   !
  type(header),        intent(in) :: head  ! Observation header
  logical,             intent(in) :: long  !
  logical,             intent(in) :: full  !
  ! Local
  character(len=128) :: ch
  character :: ch1*60,ch2*40,ch3*40
  character(len=10) :: quality(0:9)
  character(len=12) :: off1,off2
  integer(kind=4) :: i,tver,lch
  logical :: error
  ! Data
  data quality /'Unknown','Excellent','Good','Fair','Average',  &
       'Poor','Bad','Awful','Worst','Deleted'/
  !
  if (long .or. full) then
     ! Long title
     tver = abs(head%gen%ver)
     call gag_todate(head%gen%dobs,ch1,error)
     call gag_todate(head%gen%dred,ch2,error)
     write(ch,100) head%gen%num,tver,head%pos%sourc,head%spe%line,head%gen%teles,  &
       ch1(1:11),ch2(1:11)
     call outlin(ch,len_trim(ch))
     if (set%title%position.or.full) then
        !-- Absolute Coordinates
        if (abs(head%pos%system).eq.type_eq) then
           call sexag(ch2,head%pos%lam,24)
           call sexag(ch3,head%pos%bet,360)
           write(ch1,999) ch2,ch3,obs_system(head%pos%system),head%pos%equinox
        elseif (abs(head%pos%system).eq.type_ic) then
           call sexag(ch2,head%pos%lam,24)
           call sexag(ch3,head%pos%bet,360)
           write(ch1,999) ch2,ch3,obs_system(head%pos%system)
        else
           write(ch1,104) 180.*head%pos%lam/pi,180.*head%pos%bet/pi,  &
             obs_system(head%pos%system)
        endif
        !-- Projection
        ! '^' is symbol for degree in Greg plots
        write(ch2,'(1X,A,1X,F6.1,A1)')  &
          obs_projection(head%pos%proj),head%pos%projang*deg_per_rad,'^'
        !-- Offsets
        call offsec(set,head%pos%lamof,off1)
        call offsec(set,head%pos%betof,off2)
        write(ch3,'(" Offs: ",a12,a12)') off1,off2
        !--- All
        ch = trim(ch1)//trim(ch2)//trim(ch3)
        lch = len_trim(ch)
        call sic_blanc(ch,lch)
        call outlin(ch,lch)
     endif
     !
     ! Noise information
     if (set%title%quality.or.full) then
        if (head%gen%time.ge.6000.) then  ! 100 min
          write(ch1,'(f5.1,a)')  head%gen%time/3600.,'hr'   ! Format up to 999.9hr
        elseif (head%gen%time.ge.100.) then  ! 100 sec
          write(ch1,'(f4.1,a)')  head%gen%time/60.,'min'  ! Format up to 99.9min
        else
          write(ch1,'(f4.1,a)')  head%gen%time,'sec'      ! Format up to 99.9sec
        endif
        write(ch,102) quality(head%gen%qual),head%gen%tau, &
             head%gen%tsys,trim(ch1),head%gen%el*180./pi
        call outlin(ch,len_trim(ch))
     endif
     !
     ! Frequency information
     if ((set%title%spectral.or.full) .and. head%gen%kind.eq.kind_spec) then
        write(ch,105) head%spe%nchan,head%spe%rchan,head%spe%voff,head%spe%vres, &
             obs_typev(head%spe%vtype)
        call outlin(ch,len_trim(ch))
        if (head%spe%image.eq.image_null) then
          write(ch,1061) head%spe%restf,head%spe%fres
        else
          write(ch,106) head%spe%restf,head%spe%fres,head%spe%image
        endif
        call outlin(ch,len_trim(ch))
     endif
     !
     ! Continuum Drift information
     if ((set%title%continuum.or.full) .and. head%gen%kind.eq.kind_cont) then
        write(ch,113) head%dri%npoin,head%dri%rpoin,  &
          head%dri%aref*class_setup_get_fangle(),head%dri%ares*class_setup_get_fangle()
        call outlin(ch,len_trim(ch))
        write(ch,114) head%dri%freq,head%dri%width,head%dri%apos*180/pi,obs_system(head%dri%ctype)
        call outlin(ch,len_trim(ch))
     endif
     !
     ! Calibration information
     if (set%title%calibration .or. full) then
        if (head%presec(class_sec_cal_id)) then
           write(ch,110) head%cal%beeff,head%cal%foeff,head%cal%gaini
           call outlin(ch,len_trim(ch))
        else
           write(ch,110) 0.0,0.0,0.0
           call outlin(ch,len_trim(ch))
        endif
     endif
     if (set%title%atmosphere .or. full) then
        write(ch,111) head%cal%h2omm,head%cal%pamb,head%cal%tamb,head%cal%tchop,head%cal%tcold
        call outlin(ch,len_trim(ch))
        write(ch,112) head%cal%tatms,head%cal%taus,head%cal%tatmi,head%cal%taui
        call outlin(ch,len_trim(ch))
     endif
     !
     ! Origin information
     if (set%title%origin .or. full) then
        if (head%his%nseq.gt.0) then
           lch = 1
           do i=1, head%his%nseq
              if (head%his%start(i).ge.head%his%end(i)) then
                 if (lch.gt.73) then
                    call outlin(ch,lch-1)
                    lch = 1
                 endif
                 write (ch(lch:),107) head%his%start(i)
                 lch = lch+8
              else
                 if (lch.gt.65) then
                    call outlin(ch,lch-1)
                    lch = 1
                 endif
                 write (ch(lch:),108) head%his%start(i),head%his%end(i)
                 lch = lch+16
              endif
           enddo
           if (lch.gt.1) call outlin(ch,lch-1)
        else
           write(ch,*) 'Scan: ',head%gen%scan,' Subscan:',head%gen%subscan
           call outlin(ch,len(trim(ch)))
        endif
     endif
  else
     ! Brief title from R memory
     tver = abs(head%gen%ver)
     call offsec(set,head%pos%lamof,ch1)
     call offsec(set,head%pos%betof,ch2)
     write(ch,101) head%gen%num,tver,head%pos%sourc,head%spe%line,head%gen%teles,  &
                   ch1(1:8),ch2(1:8),obs_system(head%pos%system),head%gen%scan,  &
                   head%gen%subscan
     call outlin(ch,79)
  endif
  !
100 format(i0,';',i0,1x,A,1x,A,1x,A,1x,'O:',a,' R:',a)
101 format(i0,';',i0,1x,A,1x,A,1x,A,1x,a,1x,a,1x,a2,1x,i0,'.',i0)
102 format(a,'tau: ',f7.3,2x,'Tsys: ',f6.0,2x,'Time: ',a,2x,'El: ',0pf4.1)
104 format(' l: ',f8.3,2x,'b: ',f8.3,2x,a2)
105 format('N: ',i0,2x,'I0: ',1pg12.6,7x,'V0: ',1pg11.4,2x,'Dv: ',1pg11.4,2x,a)
106 format('F0: ',1pg16.9,2x,'Df: ',1pg11.4,2x,'Fi: ',1pg16.9)
1061 format('F0: ',1pg16.9,2x,'Df: ',1pg11.4,2x,'Fi: N/A')
107 format(i6,', ')
108 format(i6,'-',i6,', ')
110 format('Bef: ',1pg11.2,2x,'Fef: ',1pg11.2,2x,'Gim: ',1pg11.4)
111 format('H2O : ',1pg11.4,2x,'Pamb: ',0pf5.1,2x,&
         'Tamb: ',0pf5.1,' Thot: ',0pf5.1,' Tcold: ',0pf5.1)
112 format('Tatm: ',0pf5.1,2x,'Tau:',0pf7.3,2x,'Tatm_i: ',0pf5.1,2x,'Tau_i:',0pf7.3)
113 format('N: ',i6,1x,'I0: ',1pg11.4,7x,'A0: ',1pg11.4,2x,'Da: ',1pg11.4)
114 format('F0: ',1pg16.9,2x,'Df: ',1pg11.4,2x,'Pos. Ang. : ',0pf6.2,1x,a2)
999 format('RA: ',a12,2x,'DEC: ',a12,2x,a4,f7.1)
end subroutine titout_observation
