!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine class_catalog(line,error)
  use gildas_def
  use gkernel_interfaces
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! Support routine for command
  !     CATALOG [filename]
  !     1. /STATUS
  !---------------------------------------------------------------------
  character(len=*) :: line
  logical :: error
  ! Local
  character(len=filename_length) :: molfile
  character(len=256) :: chain
  !
  ! Maximum number of molecules on catalog
  integer(kind=4), parameter :: mmol = 10000
  integer(kind=4), parameter :: argu_l = 32
  !
  integer(kind=4), save :: nmol
  character(len=argu_l), save :: molname(mmol)
  real(kind=8), save :: molfreq(mmol)
  integer(kind=4), save :: molstatus(mmol)
  character(len=argu_l),allocatable :: chbuff(:)
  integer(kind=4), allocatable :: keys(:),i4buff(:)
  !
  integer(kind=4) :: lun,nv,ier,nmolfile
  integer(kind=4) :: astatus
  real(kind=8) :: afreq
  character(len=argu_l) :: aname
  character(len=16) :: varname
  character(len=*), parameter :: rname='catalog'
  integer(kind=4), parameter :: nsort=2
  character(len=argu_l)  :: sortopts(nsort)
  integer(kind=4), parameter :: isort = 2
  integer(kind=4) :: code_sort,nc,nlines
  integer(kind=4),parameter :: code_none = 1
  integer(kind=4),parameter :: code_freq = 2
  character(len=argu_l)  :: usersort,desambig
  character(len=message_length) :: mess
  !
  data sortopts/'NONE','FREQUENCY'/
  !
  ! Initialization
  error = .false.
  !
  molfile = 'gag_molecules'
  if (sic_present(0,1)) then
     call sic_ch(line,0,1,molfile,nmolfile,.false.,error)
     if (error) return
  endif
  if (.not.sic_query_file(molfile,'data#dir:','.dat',molfile)) then
     call class_message(seve%e,rname,trim(molfile)//' not found')
     error = .true.
     return
  endif
  !
  if (sic_present(isort,0)) then
     call sic_ke(line,isort,1,usersort,nc,.true.,error)
     if (error) return
     call sic_ambigs(rname,usersort,desambig,code_sort,sortopts,nsort,error)
     if(error) return
  else
     code_sort = code_freq
  endif
  !
  call gag_fillines(trim(molfile),.true.,nlines,error)
  if(error) return
  if (nlines.gt.mmol) then
     write(mess,'(a,i0,a,i0)') "Number of lines of the catalog, ",nlines,", goes beyond the maximum of ",mmol
     call class_message(seve%e,rname,mess)
     error = .true.
     return
  endif
  !
  ! Open file
  ier = sic_getlun(lun)
  if (ier.ne.1) then
     call class_message(seve%e,rname,'Cannot allocate LUN')
     call putios('E-LINE, ',ier)
     return
  endif
  open(unit=lun,file=trim(molfile), status='old',iostat=ier)
  if (ier.ne.0) then
     call class_message(seve%e,rname,'Cannot open file: '//trim(molfile))
     call putios('E-LINE, ',ier)
     goto 9
  endif
  !
  ! Read file
  nmol = 0
  if (sic_present(1,0)) then
4    read(lun,'(a)',iostat=ier,end=9) chain
     if (ier.ne.0 .or. lenc(chain).eq.0) goto 4
     if (chain(1:1).ne.'!') then
        read(chain,*,err=4) afreq,aname,astatus
        nmol = nmol + 1
        molfreq(nmol) = afreq
        molname(nmol) = aname
        molstatus(nmol) = astatus
        if (nmol.le.mmol) goto 4
     else
        goto 4
     endif
  else
5    read(lun,'(a)',iostat=ier,end=9) chain
     if (ier.ne.0 .or. lenc(chain).eq.0) goto 5
     if (chain(1:1).ne.'!') then
        read(chain,*,err=5) afreq,aname
        nmol = nmol + 1
        molfreq(nmol) = afreq
        molname(nmol) = aname
        molstatus(nmol) = 1
        if (nmol.le.mmol) goto 5
     else
        goto 5
     endif
 endif
9 close(unit=lun)
  call sic_frelun(lun)
  !
  if (code_sort.eq.code_freq) then
     !
     ! Sort Catalog according to frequency, This is needed to ensure
     ! proper functioning of dependant procedures such as go browse
     !
     call class_message(seve%i,rname,"Sorting catalog by frequency")
     !
     allocate(keys(nmol),chbuff(nmol),i4buff(nmol),stat=ier)
     if (failed_allocate(rname,'Sorting keys and buffers',ier,error)) return
     ! Sort
     call gr8_trie(molfreq,keys,nmol,error)
     if (error) return
     ! Apply sorting
     call gch_sort(molname,chbuff,keys,argu_l,nmol)
     call gi4_sort(molstatus,i4buff,keys,nmol)
     !
     deallocate(keys,chbuff,i4buff)
  else if (code_sort.eq.code_none) then
     call class_message(seve%i,rname,"Catalog not sorted")
  else
     write(mess,'(a,i3,a)') "Sorting code ",code_sort," unknown"
     call class_message(seve%e,rname,mess)
     error = .true.
     return
  endif
  !
  ! Create the SIC substructure (Delete it first if needed)
  varname = 'CLASS'
  if (.not.sic_varexist(varname)) then
     call sic_defstructure(varname,.true.,error)
     if (error) return
  endif
  varname = 'CLASS%LINE'
  if (sic_varexist(varname)) then
     call sic_delvariable(varname,.false.,error)
     if (error) return
  endif
  call sic_defstructure(varname,.true.,error)
  if (error) return
  nv = lenc(varname)
  call sic_def_inte(varname(1:nv)//'%N',nmol,0,0,.true.,error)
  call sic_def_dble(varname(1:nv)//'%FREQ',molfreq,1,nmol,.true.,error)
  call sic_def_charn(varname(1:nv)//'%NAME',molname,1,nmol,.true.,error)
  call sic_def_inte(varname(1:nv)//'%STATUS',molstatus,1,nmol,.true.,error)
  if (error) return
  !
end subroutine class_catalog
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
