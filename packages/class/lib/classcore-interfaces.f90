module classcore_interfaces
  !---------------------------------------------------------------------
  ! CLASS interfaces, all kinds (private or public). Do not use directly
  ! out of the library.
  !---------------------------------------------------------------------
  !
  use classcore_interfaces_public
  use classcore_interfaces_private
  !
end module classcore_interfaces
!
module classcore_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS dependencies public interfaces. Do not use out of the library.
  !---------------------------------------------------------------------
  !
  use gkernel_interfaces
  use atm_interfaces_public
  use astro_interfaces_public
  use classic_interfaces_public
end module classcore_dependencies_interfaces
