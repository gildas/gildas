!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module output_header
  use gildas_def
  !---------------------------------------------------------------------
  ! Describe the current status of the Header or List output
  !---------------------------------------------------------------------
  integer(kind=4) :: p_lun   ! Output LUN
  real(kind=4) :: x1, y1  ! Position of text
  character(len=filename_length) :: filnam  ! Output filename
  character(len=1)   :: type1   ! Output device type
end module output_header
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine class_list(set,line,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_list
  use class_index
  use class_types
  use output_header
  !---------------------------------------------------------------------
  ! @ public
  ! CLASS Support routine for command
  !    LAS\LIST [IN|OUT]
  ! 1           [/BRIEF]
  ! 2           [/LONG]
  ! 3           [/SCAN]
  ! 4           [/OUTPUT File]
  ! 5           [/TOC List]
  !
  ! Output format is BRIEF | NORMAL | LONG
  ! LIST     list the index built by FIND
  ! LIST IN  list the whole input file
  ! LIST OUT list the whole output file (LONG format unsupported here)
  !---------------------------------------------------------------------
  type(class_setup_t), intent(inout) :: set    !
  character(len=*),    intent(in)    :: line   ! Input argument line
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='LIST'
  logical :: in,out,brief,long,doprint,doscan,dotoc
  integer(kind=4) :: nc,nkey,listmode
  character(len=80) :: snam,mess
  character(len=12) :: argum
  character(len=3) :: vocab(2),keyword
  data vocab/'IN','OUT'/
  !
  ! decode arguments
  argum=' '
  call sic_ke (line,0,1,argum,nc,.false.,error)
  if (error) return
  nkey = 0
  if (nc.gt.0) then
     call sic_ambigs(rname,argum,keyword,nkey,vocab,2,error)
     if (error) return
  endif
  select case (nkey)
  case(1)
     out = .false.
     in  = .true.
  case(2)
     in  = .false.
     out = .true.
  case default
     in  = .false.
     out = .false.
  end select
  brief   = sic_present(1,0)
  long    = sic_present(2,0)
  doscan  = sic_present(3,0)
  doprint = sic_present(4,0)
  dotoc   = sic_present(5,0)
  !
  ! Check compatibilities
  if ((brief.and.long).or.(doscan.and.long)) then
     call class_message(seve%e,rname,'incompatible options')
     error = .true.
     return
  endif
  !
  if (long) then
    listmode = 2
  elseif (brief) then
    listmode = 1
  else
    listmode = 0  ! Normal
  endif
  !
  ! Check if any INDEX
  if (in) then
     if (ix%next.le.1 .and. .not.dotoc) then
        call class_message(seve%w,rname,'Input index is empty')
        return
     elseif (.not.doprint) then
        call class_message(seve%r,rname,'Input index contains:')
     endif
  elseif (out) then
     if (doscan.or.dotoc.or.long) then
        call class_message(seve%w,rname,'Option not available for Output file')
        return
     endif
     if (ox%next.le.1) then
        call class_message(seve%w,rname,'Output index is empty')
        return
     elseif (.not.doprint) then
        call class_message(seve%r,rname,'Output index contains:')
     endif
  else
     if (cx%next.le.1 .and. .not.dotoc) then
        call class_message(seve%w,rname,'Current index is empty')
        return
     elseif (.not.doprint) then
        call class_message(seve%r,rname,'Current index contains:')
     endif
  endif
  !
  ! Output file
  if (doprint) then
     call sic_ch (line,4,1,snam,nc,.true.,error)
     if (error) return
     call sic_parse_file(snam,' ','.lis',filnam)
     call out0('File',0.,0.,error)
     if (error) return
  else
     call out0('Terminal',0.,0.,error)
     if (error)  return
  endif
  !
  ! loop on index entries
  if (doscan) then
     ! Decode arguments and checks
     call list_scan(set,argum(1:1),brief,error)
     !
     ! Close output file in any
     call out1(error)
     if (doprint.and..not.error) then
        mess = 'List on file '//filnam
        call class_message(seve%i,rname,mess)
     endif
     !
     return
  endif
  !
  ! LIST /TOC
  if (dotoc) then
    if (out) then
      call class_message(seve%e,rname,'/TOC not available for Output file')
      error = .true.
      return
    endif
    !
    if (in) then  ! Input file TOC
      call class_list_toc_comm(set,line,ix,error)
    else          ! Default: Current index TOC
      call class_list_toc_comm(set,line,cx,error)
    endif
    if (error)  return
    !
    ! Close output file in any
    call out1(error)
    if (doprint.and..not.error) then
      mess = 'List on file '//filnam
      call class_message(seve%i,rname,mess)
    endif
    !
    return
  endif
  !
  ! LIST [IN|OUT] [/BRIEF|/LONG]
  if (in) then
    call class_list_defbrieflong(set,ix,rix,listmode,error)
  else if (out) then
    call class_list_defbrieflong(set,ox,rox,listmode,error)
  else
    call class_list_defbrieflong(set,cx,rix,listmode,error)
  endif
  if (error)  return
  call out1(error)  ! Close
  if (doprint.and..not.error) then
     mess = 'List on file '//filnam
     call class_message(seve%i,rname,mess)
  endif
  !
end subroutine class_list
!
subroutine class_list_defbrieflong(set,optx,roptx,mode,error)
  use gbl_constant
  use gbl_message
  use classic_api
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_list_defbrieflong
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Support routine for command
  !   LIST [IN|OUT]  (default mode)
  !   LIST [IN|OUT]  /BRIEF
  !   LIST [IN|OUT]  /LONG
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(optimize),      intent(in)    :: optx   ! Index to be listed
  interface
    subroutine roptx(entry_num,ind,error)  ! Index reading routine
    use classic_api
    use class_types
    integer(kind=entry_length), intent(in)  :: entry_num
    type(indx_t),               intent(out) :: ind
    logical,                    intent(out) :: error
    end subroutine roptx
  end interface
  integer(kind=4),     intent(in)    :: mode   ! List mode (0=def,1=brief,2=long)
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='LIST'
  integer(kind=entry_length) :: k,kk
  character(len=24) :: stitle
  character(len=128) :: chaine
  integer(kind=4) :: lchain,nc,snc
  character(len=12) :: bforma  ! Brief format
  character(len=12) :: sforma  ! Scan/subscan format
  character(len=80) :: dforma  ! Default format
  type(indx_t) :: ind
  type(observation) :: obs
  !
  if (mode.eq.1) then  ! /BRIEF
    call list_numver_format(optx,bforma,nc)
    lchain = 1
    do k=1,optx%next-1
      if (lchain.gt.69) then
        call outlin(chaine,lchain-1)
        lchain = 1
      endif
      write(chaine(lchain:),bforma)  optx%num(k),';',abs(optx%ver(k))
      lchain=lchain+nc+2  ! Separate elements with 2 spaces
      if (sic_ctrlc())  goto 100
    enddo
    if (lchain.gt.1)  call outlin(chaine,lchain)
    !
  else if (mode.eq.2) then  ! /LONG
    ! This code reads in FILEIN, i.e. no support for LIST OUT /LONG
    do k = 1,optx%next-1
      kk = optx%ind(k)
      call robs(obs,kk,error)
      if (error) cycle
      call rgen(set,obs,error)
      call rpos(set,obs,error)
      call convert_pos(set,obs%head,error)
      if (obs%head%gen%kind.eq.kind_spec) then
        call rspec(set,obs,error)
        call convert_vtype(set,obs%head,error)
      else
        call rcont(set,obs,error)
      endif
      call rcal(set,obs,error)
      if (obs%head%presec(class_sec_his_id)) then
        call rorig(set,obs,error)
      else
        obs%head%his%nseq = 0
      endif
      call titout(set,obs%head,'L','O')
      call outlin(' ',1)
      if (sic_ctrlc())  goto 100
    enddo
    !
  else  ! Normal LIST
    call list_numver_format(optx,bforma,nc)          ! Automatic format for N;V
    call list_scansub_format(optx,sforma,snc,stitle) ! Automatic format for Scan Subscan
    ! Title
    chaine = ''
    chaine(nc-2:nc) = 'N;V'
    !                     SSSSSSSSSSSS LLLLLLLLLLLL TTTTTTTTTTTT LLLLLLLL BBBBBBBB SS
    chaine(nc+2:nc+60) = 'Source       Line         Telescope      Lambda     Beta Sys'
    chaine(nc+62:)     = stitle
    call outlin(chaine,len_trim(chaine))
    ! Format for each line (NB: drop the last ')' in bforma)
    write(dforma,'(4A)') bforma(1:len_trim(bforma)-1),  &
                         ',1X,A,1X,A,1X,A,1X,A,1X,A,1X,A2,1X,', &
                         sforma(1:len_trim(sforma)),  &
                         ')'
    do k = 1,optx%next-1
      kk = optx%ind(k)
      call roptx(kk,ind,error)  ! Needed for LIST OUT
      if (error) cycle
      !
      call class_list_default(set,ind,dforma)
      !
      if (sic_ctrlc())  goto 100
    enddo
  endif
  !
  error = .false.
  return  ! Normal return
  !
  ! CTRL-C
100 continue
  error = .true.
  call class_message(seve%i,rname,'Command interrupted by pressing ^C')
  return
end subroutine class_list_defbrieflong
!
subroutine list_numver_format(optx,forma,leng)
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Return the "best" (minimum and sufficient) format to display the
  ! any number and version as "N;V"
  !---------------------------------------------------------------------
  type(optimize),   intent(in)  :: optx   !
  character(len=*), intent(out) :: forma  ! Computed format
  integer(kind=4),  intent(out) :: leng   ! Length when format will be applied
  ! Local
  integer(kind=4) :: ncn,ncv
  integer(kind=obsnum_length) :: m
  !
  if (optx%next.le.1) then
    ncn = 0
    ncv = 0
    leng = 12
  else
    ! Number: can not be negative
    m = maxval(optx%num(1:optx%next-1))+1  ! +1 because log10(10) is 1 while we need
                                           ! 2 digits. Same for 1, 10, 100, 1000 etc
    if (m.gt.0) then
      ncn = ceiling(log10(real(m,kind=8)))
    else
      ncn = 1
    endif
    ! Version: can be negative (but always displayed positive)
    m = max(abs(minval(optx%ver(1:optx%next-1))),  &
            abs(maxval(optx%ver(1:optx%next-1))))+1
    if (m.gt.0) then
      ncv = ceiling(log10(real(m,kind=8)))
    else
      ncv = 1
    endif
    leng = ncn+1+ncv  ! NNN;VV
  endif
  write(forma,'(A,I0,A,I0,A)')  '(I',ncn,',A,I',ncv,')'
  !
end subroutine list_numver_format
!
subroutine list_scansub_format(optx,forma,leng,title)
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Return the "best" (minimum and sufficient) format to display any
  ! scan and subscan as "Scan Subscan"
  !---------------------------------------------------------------------
  type(optimize),   intent(in)  :: optx   !
  character(len=*), intent(out) :: forma  ! Computed format
  integer(kind=4),  intent(out) :: leng   ! Length when format will be applied
  character(len=*), intent(out) :: title  !
  ! Local
  integer(kind=4) :: ncsc,ncsu
  integer(kind=obsnum_length) :: m
  !
  if (optx%next.le.1) then
    ncsc = 5
    ncsu = 3
    leng = 9
  else
    ! Scan: can not be negative
    m = maxval(optx%scan(1:optx%next-1))+1  ! +1 because log10(10) is 1 while we need
                                            ! 2 digits. Same for 1, 10, 100, 1000 etc
    if (m.gt.0) then
      ncsc = ceiling(log10(real(m,kind=8)))
    else
      ncsc = 1
    endif
    ncsc = max(ncsc,3)  ! At least 3 chars
    ! Subscan: can not be negative
    m = maxval(optx%subscan(1:optx%next-1))+1  ! Same
    if (m.gt.0) then
      ncsu = ceiling(log10(real(m,kind=8)))
    else
      ncsu = 1
    endif
    ncsu = max(ncsu,3)  ! At least 3 chars
    leng = ncsc+1+ncsu  ! Scan Subscan
  endif
  write(forma,'(A,I0,A,I0)')  'I',ncsc,',1X,I',ncsu
  !
  title = ''
  title(1:ncsc) = 'Scan'                  ! Might be truncated
  title(1:ncsc) = adjustr(title(1:ncsc))  ! Right-justified
  title(ncsc+2:ncsc+1+ncsu) = 'Subscan'   ! Might be truncated
  !
end subroutine list_scansub_format
!
subroutine class_list_default(set,ind,forma)
  use classcore_interfaces, except_this=>class_list_default
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Print the current indx with the default LIST format
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)           :: set
  type(indx_t),        intent(in)           :: ind
  character(len=*),    intent(in), optional :: forma
  ! Local
  character(len=80) :: dforma
  character(len=128) :: chaine
  character(len=60) :: ch1,ch2
  !
  if (present(forma)) then
    dforma = forma
  else
    dforma = '(I0,A,I0,1X,A,1X,A,1X,A,1X,A,1X,A,1X,A2,1X,I5,1X,I0)'
  endif
  !
  call offsec(set,ind%off1,ch1)
  call offsec(set,ind%off2,ch2)
  write(chaine,dforma) ind%num,';',abs(ind%ver),ind%csour,ind%cline,  &
    ind%ctele,ch1(1:8),ch2(1:8),obs_system(ind%type),ind%scan,ind%subscan
  call outlin(chaine,len_trim(chaine))
  !
end subroutine class_list_default
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine out0(gtype,x,y,error)
  use gkernel_interfaces
  use output_header
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS Internal routine
  ! Output on terminal or graphic device
  !	'G',x,y   : for graphic device in x,y coordinates
  !	'T',0.,0. : write on terminal
  !	'F',0.,0. : write in file
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: gtype  ! Type of output (G,T,F)
  real,             intent(in)  :: x,y    ! Coordinates
  logical,          intent(out) :: error  ! Logical error flag
  ! Local
  integer :: ier
  !
  type1 = gtype(1:1)
  x1=x
  y1=y
  !
  if (type1.eq.'G') then
     call gplot(x1,y1,3)
     !
  elseif (type1.eq.'F') then
     ier = sic_getlun(p_lun)
     if (mod(ier,2).eq.0) then
        call class_message(seve%e,'OUT','Cannot open file '//filnam)
        call class_message(seve%e,'OUT','No logical unit left')
        error = .true.
        return
     endif
     ier = sic_open(p_lun,filnam,'NEW',.false.)
     if (ier.ne.0) then
        call class_message(seve%e,'OUT','Cannot open file '//filnam)
        call putios ('        ',ier)
        error = .true.
        call sic_frelun(p_lun)
        return
     endif
     !
  else
     p_lun = 6
  endif
  !
end subroutine out0
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine outlin(cin,lin)
  use gkernel_interfaces
  use gbl_message
  use output_header
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS Internal routine
  ! Writes string cin(1:lin) on screen or file
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: cin  !
  integer,          intent(in) :: lin  !
  ! Local
  character(len=*), parameter :: rname='LIST'
  logical :: error
  real :: cdef
  character(len=256) :: chain
  !
  if (type1.eq.'G') then
     chain = 'LABEL "'//cin(1:lin)//'" /CENTER 5'
     call gr_exec (chain)
     error = .false.
     call sic_get_real('CHARACTER_SIZE',cdef,error)
     y1 = y1-cdef*1.15
     call gplot(x1,y1,3)
     !
  elseif (p_lun.ne.6) then
     write(p_lun,'(A)') cin(1:lin)
     !
  else
     call class_message(seve%r,rname,cin)
  endif
  !
end subroutine outlin
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine out1(error)
  use gkernel_interfaces
  use output_header
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS Internal routine
  ! Close output device
  !---------------------------------------------------------------------
  logical, intent(in) :: error      !
  !
  if (type1.eq.'F') then
     if (error) then
        close(unit=p_lun,status='DELETE')
     else
        close(unit=p_lun)
     endif
     call sic_frelun(p_lun)
  endif
end subroutine out1
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine list_scan(set,key,brief,error)
  use classcore_dependencies_interfaces
  use class_index
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS Support routine for command
  !   LIST /SCAN
  !---------------------------------------------------------------------
  type(class_setup_t), intent(inout) :: set    !
  character(len=1),    intent(in)    :: key    ! I(n) or O(ut). Default is Current index
  logical,             intent(in)    :: brief  ! BRIEF or NORMAL
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  integer :: ier
  integer(kind=entry_length) :: kmax0,cx_iobs,ix_iobs
  logical :: dosort,doin,doout
  character(len=12) :: user_sortname
  integer(kind=entry_length), allocatable :: local_ind(:)
  !
  doin  = key.eq.'I'
  doout = key.eq.'O'
  if (doin) then
    kmax0 = ix%next-1
  else if (doout) then
    kmax0 = ox%next-1
  else
    kmax0 = cx%next-1
  endif
  !
  ! Store the current index
  allocate(local_ind(kmax0),stat=ier)
  if (failed_allocate('LIST','local_ind',ier,error))  goto 99
  if (key.eq.'I') then
     local_ind(:) = ix%ind(1:kmax0)
  else if (key.eq.'O') then
     return
  else
     local_ind(:) = cx%ind(1:kmax0)
  endif
  !
  ! 1st step: SCAN
  ! Sort index according to SCAN number
  ! If user sorting keyword is SCAN, check if index has been sorted
  if (set%sort_name.eq.'SCAN') then
     set%do_sort = .not.set%do_sort
     dosort = set%do_sort
  else if (set%sort_name.ne.'NONE') then
     dosort = .true.
     user_sortname = set%sort_name
  else
     dosort = .false.
  endif
  set%sort_name = 'SCAN'
  call sort_index(set,key,error)
  if (error) goto 99
  !
  if (brief) then
     if (doin) then
        call list_scan_brief(ix%ind(1:kmax0),kmax0,error)
     else if (doout) then
        return
     else
        call list_scan_brief(cx%ind(1:kmax0),kmax0,error)
     endif
  else
     if (doin) then
        call list_scan_long(set,ix%ind(1:kmax0),kmax0,error)
     else if (doout) then
        return
     else
        call list_scan_long(set,cx%ind(1:kmax0),kmax0,error)
     endif
  endif
  if (error) goto 99
  !
  ! Sort index according to USER last sorting keyword
  if (dosort) then
     do cx_iobs=1,kmax0
       ix_iobs = local_ind(cx_iobs)
       call optimize_tooptimize(ix,ix_iobs,cx,cx_iobs,.true.,error)
       if (error)  goto 99
     enddo
  else
     set%do_sort = .false.
  endif
  !
  ! Restore index
99 if (doin) then
     ix%ind(1:kmax0) = local_ind(:)
  else if (doout) then
     return
  else
     do cx_iobs=1,kmax0
       ix_iobs = local_ind(cx_iobs)
       call optimize_tooptimize(ix,ix_iobs,cx,cx_iobs,.true.,error)
       if (error)  return
     enddo
  endif
  !
  deallocate(local_ind)
end subroutine list_scan
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine list_scan_brief(idx,kmax0,error)
  use gkernel_interfaces
  use classic_api
  use class_parameter
  use class_index
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in)  :: kmax0       !
  integer(kind=entry_length), intent(in)  :: idx(kmax0)  !
  logical,                    intent(out) :: error       !
  ! Local
  character(len=*), parameter :: rname='LIST'
  integer(kind=entry_length) :: k,kk,k1,k2,kmax
  integer(kind=obsnum_length) :: current,snum
  integer :: lchain
  character(len=80) :: chaine
  !
  lchain = 1
  k  = 1
  do while(.true.) ! Loop on SCAN blocks
     k1    = k
     kk    = idx(k1)
     snum  = ix%scan(kk)
     k2    = 0
     do k=k1,kmax0 ! Find SCAN blocks
        kk      = idx(k)
        current = ix%scan(kk)
        if (current.ne.snum) exit
        k2      = k2+1
     enddo
     ! SCAN block: from k1 to k1+k2-1
     kmax = k1+k2-1
     if (lchain.gt.69) then
        call outlin(chaine,lchain-1)
        lchain = 1
     endif
     write(chaine(lchain:),'(i5,": ",i0)') snum,k2
     lchain=lchain+12
     k = kmax+1
     if (k.gt.kmax0) exit
     error = .false.
     ! ^C
     if (sic_ctrlc()) then
        error = .true.
        lchain = 1
        call class_message(seve%i,rname,'Command interrupted by pressing ^C')
        return
     endif
  enddo
  call outlin(chaine,lchain-1)
end subroutine list_scan_brief
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine list_scan_long(set,idx,kmax0,error)
  use gbl_message
  use classic_api
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>list_scan_long
  use class_parameter
  use class_index
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(class_setup_t),        intent(inout) :: set         !
  integer(kind=entry_length), intent(in)    :: kmax0       ! Number of entries
  integer(kind=entry_length), intent(in)    :: idx(kmax0)  ! Scan-sorted Index array (IX, OX, CX)
  logical,                    intent(inout) :: error       ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='LIST'
  type(indx_t) :: ind
  real(kind=4) :: xoff1,xoff2,yoff1,yoff2
  integer(kind=entry_length) :: k,kk,k1,k2,kmax
  integer(kind=obsnum_length) :: current,snum
  integer(kind=4) :: nspec
  integer(kind=entry_length) :: local_idx(kmax0)  ! Automatic array
  character(len=36) :: prev,last
  !
  k  = 1
  k1 = 0
  local_idx(:) = idx(:)
  !
  ! Loop on SCAN blocks
  do while(k1.lt.kmax0)
     k1    = k
     kk    = idx(k1)
     snum  = ix%scan(kk)
     k2    = 0
     !
     ! Find SCAN blocks from k1 to k1+k2-1
     do k=k1,kmax0
        kk      = idx(k)
        current = ix%scan(kk)
        if (current.ne.snum) exit
        k2      = k2+1
     enddo
     !
     ! 2nd step: sort scan block according to SOURCE//LINE//TELESCOPE
     kmax  = k1+k2-1
     k     = k1
     set%sort_name = 'TOC'
     if (k2.ge.2) then
        local_idx(:) = idx(:)
        call quicksort(set,local_idx(k1:kmax),k2,ix_toc_default_gt,ix_toc_default_ge,error)
        if (error) return
     endif
     !
     ! 3rd step: in a scan block, sorted according to TOC, loop on toc blocks
     do while (.true.)
        kk    = local_idx(k1)
        prev  = ix%csour(kk)//ix%cline(kk)//ix%ctele(kk)
        k2    = 0
        do k=k1,kmax ! Find toc blocks
           kk   = local_idx(k)
           last = ix%csour(kk)//ix%cline(kk)//ix%ctele(kk)
           if (prev.ne.last) exit
           k2   = k2+1
        enddo
        ! Should be here in one coherent block (1 scan, 1 toc)
        ! Compute Nb of spectra and offset range
        nspec = 0
        xoff1=  1.e10
        xoff2= -1.e10
        yoff1=  1.e10
        yoff2= -1.e10
        do k=k1,k1+k2-1
           kk = local_idx(k)
           nspec = nspec + 1
           xoff1 = min(xoff1,ix%off1(kk))
           xoff2 = max(xoff2,ix%off1(kk))
           yoff1 = min(yoff1,ix%off2(kk))
           yoff2 = max(yoff2,ix%off2(kk))
        enddo
        call rix(kk,ind,error)
        if (error) cycle
        call out_scan(set,ind,xoff1,xoff2,yoff1,yoff2,snum,nspec)
        k1 = k1+k2
        if (k1.gt.kmax) exit
        ! ^C
        if (sic_ctrlc()) then
           error = .true.
           call class_message(seve%i,rname,'Command interrupted by pressing ^C')
           return
        endif
     enddo
  enddo
end subroutine list_scan_long
!
subroutine out_scan(set,ind,xoff1,xoff2,yoff1,yoff2,snum,nspec)
  use classcore_interfaces, except_this=>out_scan
  use class_parameter
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(class_setup_t),         intent(in) :: set    !
  type(indx_t),                intent(in) :: ind    !
  real(kind=4),                intent(in) :: xoff1  ! Min lam offset
  real(kind=4),                intent(in) :: xoff2  ! Max lam offset
  real(kind=4),                intent(in) :: yoff1  ! Min bet offset
  real(kind=4),                intent(in) :: yoff2  ! Max bet offset
  integer(kind=obsnum_length), intent(in) :: snum   ! Scan number
  integer(kind=4),             intent(in) :: nspec  ! Number of spectra
  ! Local
  character(len=9) :: ch1,ch2
  character(len=100) :: chaine
  character(len=20) :: m1,m2
  !
  if (abs(xoff1-xoff2).le.set%tole) then
     call offsec(set,xoff1,ch1)
     m1 = ch1
     m1 = '      '//trim(m1)
  else
     call offsec(set,xoff1,ch1)
     call offsec(set,xoff2,ch2)
     m1 = adjustr(ch1)//':'//adjustr(ch2)
     m1 = adjustr(m1)
  endif
  if (abs(yoff1-yoff2).le.set%tole) then
     call offsec(set,yoff1,ch1)
     m2 = ch1
     m2 = '      '//trim(m2)
  else
     call offsec(set,yoff1,ch1)
     call offsec(set,yoff2,ch2)
     m2 = adjustr(ch1)//':'//adjustr(ch2)
     m2 = adjustr(m2)
  endif
  write(chaine,100) ind%csour,ind%cline,ind%ctele,m1,m2,obs_system(ind%type),  &
                    snum,nspec
  call outlin(chaine,len_trim(chaine))
100 format(3(a13),1x,a20,2x,a20,a3,1x,i8,1x,i4)
end subroutine out_scan
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module class_sort_var
  use toc_types
  !---------------------------------------------------------------------
  ! Support module for command LAS\LIST /TOC
  !---------------------------------------------------------------------
  !
  ! Class types, i.e. for fields which needs special decoding (e.g. gag_date:
  ! we won't display the I*4 value, but its translation as a string)
  integer(kind=4), parameter :: class_ptype_null=0  ! No decoding, use value as is
  integer(kind=4), parameter :: class_ptype_angle=1
  integer(kind=4), parameter :: class_ptype_gagdate=2
  integer(kind=4), parameter :: class_ptype_kind=3  ! Spectro, continuum, etc
  !
  type(toc_t), target, save :: ltoc
  !
end module class_sort_var
!
subroutine class_list_toc_comm(set,line,idx,error)
  use gildas_def
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_list_toc_comm
  use class_types
  use class_sort_var
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS support routine for command
  !   LIST [IN|CURRENT] /TOC [Key1] ... [KeyN]  [/VARIABLE VarName]
  ! Main entry point
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  character(len=*),    intent(in)    :: line   ! Input command line
  type(optimize),      intent(in)    :: idx    !
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  integer(kind=4), parameter :: opttoc=5  ! /TOC
  integer(kind=4), parameter :: optvar=6  ! /VARIABLE
  character(len=16) :: toc_args(20)  ! Arbitrary number of args accepted in option /TOC
  character(len=varname_length) :: tocname
  integer(kind=4) :: nc
  !
  call class_toc_init(ltoc,error)
  if (error)  return
  !
  ! Default (unchanged in no arguments)
  toc_args(:) = ' '
  toc_args(1) = 'SOUR'
  toc_args(2) = 'LINE'
  toc_args(3) = 'TELE'
  call toc_getkeys(line,opttoc,ltoc,toc_args,error)
  if (error)  return
  !
  tocname = 'TOC'  ! Default structure name
  call sic_ch(line,optvar,1,tocname,nc,.false.,error)
  if (error)  return
  !
  call class_list_toc(set,idx,toc_args,tocname,error)
  if (error)  return
  !
end subroutine class_list_toc_comm
!
subroutine class_toc_init(toc,error)
  use gkernel_interfaces
  use classcore_dependencies_interfaces
  use toc_types
  use classcore_interfaces, except_this=>class_toc_init
  use class_sort_var
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS Support routine for command LIST /TOC
  ! Initialization routine
  !---------------------------------------------------------------------
  type(toc_t), intent(inout) :: toc    !
  logical,     intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='TOC/INIT'
  integer(kind=4) :: ier
  !
  if (toc%initialized)  return
  !
  toc%nkey = 14
  allocate(toc%keys(toc%nkey),stat=ier)
  if (failed_allocate(rname,'keys array',ier,error)) return
  !
  toc%keys(1)%keyword      = 'SOUR'
  toc%keys(1)%sic_var_name = 'sour'
  toc%keys(1)%human_name   = 'SOURCE'
  toc%keys(1)%message      = 'Number of sources......'
  toc%keys(1)%ftype        = toc_ftype_c12_1d
  toc%keys(1)%ptype        = class_ptype_null
  !
  toc%keys(2)%keyword      = 'LINE'
  toc%keys(2)%sic_var_name = 'line'
  toc%keys(2)%human_name   = 'LINE'
  toc%keys(2)%message      = 'Number of lines........'
  toc%keys(2)%ftype        = toc_ftype_c12_1d
  toc%keys(2)%ptype        = class_ptype_null
  !
  toc%keys(3)%keyword      = 'TELE'
  toc%keys(3)%sic_var_name = 'tele'
  toc%keys(3)%human_name   = 'TELESCOPE'
  toc%keys(3)%message      = 'Number of backends.....'
  toc%keys(3)%ftype        = toc_ftype_c12_1d
  toc%keys(3)%ptype        = class_ptype_null
  !
  toc%keys(4)%keyword      = 'OFF1'
  toc%keys(4)%sic_var_name = 'off1'
  toc%keys(4)%human_name   = 'OFF1'
  toc%keys(4)%message      = 'Number of off1.........'
  toc%keys(4)%ftype        = toc_ftype_r4_1d
  toc%keys(4)%ptype        = class_ptype_angle
  !
  toc%keys(5)%keyword      = 'OFF2'
  toc%keys(5)%sic_var_name = 'off2'
  toc%keys(5)%human_name   = 'OFF2'
  toc%keys(5)%message      = 'Number of off2.........'
  toc%keys(5)%ftype        = toc_ftype_r4_1d
  toc%keys(5)%ptype        = class_ptype_angle
  !
  toc%keys(6)%keyword      = 'ENT'
  toc%keys(6)%sic_var_name = 'ent'
  toc%keys(6)%human_name   = 'ENTRY'
  toc%keys(6)%message      = 'Number of entries......'
  toc%keys(6)%ftype        = toc_ftype_i8_1d
  toc%keys(6)%ptype        = class_ptype_null
  !
  toc%keys(7)%keyword      = 'NUM'
  toc%keys(7)%sic_var_name = 'num'
  toc%keys(7)%human_name   = 'NUMBER'
  toc%keys(7)%message      = 'Number of numbers......'
  toc%keys(7)%ftype        = toc_ftype_i8_1d
  toc%keys(7)%ptype        = class_ptype_null
  !
  toc%keys(8)%keyword      = 'BLOC'
  toc%keys(8)%sic_var_name = 'bloc'
  toc%keys(8)%human_name   = 'BLOCK'
  toc%keys(8)%message      = 'Number of blocks.......'
  toc%keys(8)%ftype        = toc_ftype_i8_1d
  toc%keys(8)%ptype        = class_ptype_null
  !
  toc%keys(9)%keyword      = 'VER'
  toc%keys(9)%sic_var_name = 'ver'
  toc%keys(9)%human_name   = 'VERSION'
  toc%keys(9)%message      = 'Number of versions.....'
  toc%keys(9)%ftype        = toc_ftype_i4_1d
  toc%keys(9)%ptype        = class_ptype_null
  !
  toc%keys(10)%keyword      = 'KIND'
  toc%keys(10)%sic_var_name = 'kind'
  toc%keys(10)%human_name   = 'KIND'
  toc%keys(10)%message      = 'Number of kinds.......'
  toc%keys(10)%ftype        = toc_ftype_i4_1d
  toc%keys(10)%ptype        = class_ptype_kind
  !
  toc%keys(11)%keyword      = 'QUAL'
  toc%keys(11)%sic_var_name = 'qual'
  toc%keys(11)%human_name   = 'QUALITY'
  toc%keys(11)%message      = 'Number of qualities...'
  toc%keys(11)%ftype        = toc_ftype_i4_1d
  toc%keys(11)%ptype        = class_ptype_null
  !
  toc%keys(12)%keyword      = 'OBS'
  toc%keys(12)%sic_var_name = 'obs'
  toc%keys(12)%human_name   = 'OBSERVED' ! Observed date
  toc%keys(12)%message      = 'Number of observation dates'
  toc%keys(12)%ftype        = toc_ftype_i4_1d
  toc%keys(12)%ptype        = class_ptype_gagdate
  !
  toc%keys(13)%keyword      = 'SCAN'
  toc%keys(13)%sic_var_name = 'scan'
  toc%keys(13)%human_name   = 'SCAN'
  toc%keys(13)%message      = 'Number of scans.......'
  toc%keys(13)%ftype        = toc_ftype_i8_1d
  toc%keys(13)%ptype        = class_ptype_null
  !
  toc%keys(14)%keyword      = 'SUBSCAN'
  toc%keys(14)%sic_var_name = 'subscan'
  toc%keys(14)%human_name   = 'SUBSCAN'
  toc%keys(14)%message      = 'Number of subscans....'
  toc%keys(14)%ftype        = toc_ftype_i4_1d
  toc%keys(14)%ptype        = class_ptype_null
  !
  call toc_init_pointers(toc,error)
  if (error)  return
  !
  toc%initialized = .true.
  !
end subroutine class_toc_init
!
subroutine class_toc_clean(error)
  use classcore_dependencies_interfaces
  use class_sort_var
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! CLASS Support routine for command LIST /TOC
  ! Global the global variable holding the TOC
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  !
  !
  call toc_clean(ltoc,error)
  ! if (error)  continue
  !
end subroutine class_toc_clean
!
subroutine class_list_toc(set,idx,keywords,tocname,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_list_toc
  use class_types
  use class_sort_var
  use output_header
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS Support routine for command LIST /TOC
  ! Processing routine
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set          !
  type(optimize),      intent(in)    :: idx          ! The idx whose TOC is desired
  character(len=*),    intent(in)    :: keywords(:)  ! Selection
  character(len=*),    intent(in)    :: tocname      ! Structure name
  logical,             intent(inout) :: error        ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='LIST/TOC'
  !
  call class_toc_datasetup(ltoc,idx)
  !
  call toc_main(rname,ltoc,idx%next-1,keywords,tocname,p_lun,class_toc_format,error)
  if (error)  return
  !
contains
  !
  subroutine class_toc_format(key,ival,output)
    use phys_const
    use classcore_dependencies_interfaces
    use class_setup_new
    use toc_types
    use class_sort_var
    !---------------------------------------------------------------------
    ! Custom display routine (because there are custom types)
    !---------------------------------------------------------------------
    type(toc_descriptor_t),     intent(in)  :: key     !
    integer(kind=entry_length), intent(in)  :: ival    !
    character(len=*),           intent(out) :: output  !
    ! Local
    logical :: error
    !
    select case(key%ftype)
    case(toc_ftype_i4_1d) ! integer*4
      if (key%ptype.eq.class_ptype_gagdate) then
        call gag_todate(key%ptr%i4%data1(ival),output,error)
      elseif (key%ptype.eq.class_ptype_kind) then
        call class_toc_format_kind(key%ptr%i4%data1(ival),output)
      else
        write(output,'(i12)') key%ptr%i4%data1(ival)
      endif
      !
    case(toc_ftype_i8_1d)  ! integer*8
      write(output,'(i12)') key%ptr%i8%data1(ival)
      !
    case(toc_ftype_r4_1d)  ! real*4
      if (key%ptype.eq.class_ptype_angle) then
        write(output,'(f8.3)') key%ptr%r4%data1(ival) * class_setup_get_fangle()
      else
        write(output,'(f8.3)') key%ptr%r4%data1(ival)
      endif
      !
    case(toc_ftype_c12_1d) ! string*12
      output = key%ptr%c12%data1(ival)
      !
    end select
  end subroutine class_toc_format
  !
  subroutine class_toc_format_kind(kind,string)
    use gbl_constant
    !-------------------------------------------------------------------
    ! Format the 'kind' value (i4 value to string)
    !-------------------------------------------------------------------
    integer(kind=4),  intent(in)  :: kind
    character(len=*), intent(out) :: string
    select case(kind)
    case (kind_spec)
      string = 'SPECTRUM'
    case (kind_cont)
      string = 'CONTINUUM'
    case default
      write(string,'(A,I0)') 'CODE ',kind
    end select
  end subroutine class_toc_format_kind
  !
end subroutine class_list_toc
!
subroutine class_toc_datasetup(toc,idx)
  use class_types
  use toc_types
  !---------------------------------------------------------------------
  ! @ private
  ! Associate the TOC data pointers to the index whose TOC is desired
  !---------------------------------------------------------------------
  type(toc_t),    intent(inout) :: toc
  type(optimize), intent(in)    :: idx
  !
  toc%keys( 1)%ptr%c12%data1 => idx%csour
  toc%keys( 2)%ptr%c12%data1 => idx%cline
  toc%keys( 3)%ptr%c12%data1 => idx%ctele
  toc%keys( 4)%ptr%r4%data1  => idx%off1
  toc%keys( 5)%ptr%r4%data1  => idx%off2
  toc%keys( 6)%ptr%i8%data1  => idx%ind
  toc%keys( 7)%ptr%i8%data1  => idx%num
  toc%keys( 8)%ptr%i8%data1  => idx%bloc
  toc%keys( 9)%ptr%i4%data1  => idx%ver
  toc%keys(10)%ptr%i4%data1  => idx%kind
  toc%keys(11)%ptr%i4%data1  => idx%qual
  toc%keys(12)%ptr%i4%data1  => idx%dobs
  toc%keys(13)%ptr%i8%data1  => idx%scan
  toc%keys(14)%ptr%i4%data1  => idx%subscan
  !
end subroutine class_toc_datasetup
