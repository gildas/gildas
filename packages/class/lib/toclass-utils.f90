subroutine fits_get_header_card_i4(fits,iname,i4,found,error,target)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>fits_get_header_card_i4
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private-generic fits_get_header_card
  !  Translate a card to its associated value
  ! ---
  !  I*4 version
  !---------------------------------------------------------------------
  type(classfits_t),          intent(inout) :: fits    !
  character(len=*),           intent(in)    :: iname   ! Card name
  integer(kind=4),            intent(inout) :: i4      ! inout: if not found, input is preserved
  logical,                    intent(out)   :: found   !
  logical,                    intent(inout) :: error   ! Logical error flag
  character(len=*), optional, intent(in)    :: target  ! Target name
  ! Local
  character(len=*), parameter :: rname='FITS'
  character(len=80) :: string
  integer(kind=4) :: ier
  !
  call fits_get_header_key2val(fits%head,iname,string,found)
  !
  if (present(target)) then
    call fits_warn_missing_i4(fits%warn,'Card',iname,target,i4,found,error)
    if (error)  return
  endif
  !
  if (.not.found)  return
  !
  read(string,'(I20)',iostat=ier)  i4
  if (ier.ne.0) then
    call class_message(seve%e,rname,  &
      'Error decoding meta keyword '//trim(iname)//' = "'//trim(string)//'"')
    call putios('E-FITS,  ',ier)
    error = .true.
  endif
  !
end subroutine fits_get_header_card_i4
!
subroutine fits_get_header_card_i8(fits,iname,i8,found,error,target)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>fits_get_header_card_i8
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private-generic fits_get_header_card
  !  Translate a card to its associated value
  ! ---
  !  I*8 version
  !---------------------------------------------------------------------
  type(classfits_t),          intent(inout) :: fits    !
  character(len=*),           intent(in)    :: iname   ! Card name
  integer(kind=8),            intent(inout) :: i8      ! inout: if not found, input is preserved
  logical,                    intent(out)   :: found   !
  logical,                    intent(inout) :: error   ! Logical error flag
  character(len=*), optional, intent(in)    :: target  ! Target name
  ! Local
  character(len=*), parameter :: rname='FITS'
  character(len=80) :: string
  integer(kind=4) :: ier
  !
  call fits_get_header_key2val(fits%head,iname,string,found)
  !
  if (present(target)) then
    call fits_warn_missing_i8(fits%warn,'Card',iname,target,i8,found,error)
    if (error)  return
  endif
  !
  if (.not.found)  return
  !
  read(string,'(I20)',iostat=ier)  i8
  if (ier.ne.0) then
    call class_message(seve%e,rname,  &
      'Error decoding meta keyword '//trim(iname)//' = "'//trim(string)//'"')
    call putios('E-FITS,  ',ier)
    error = .true.
  endif
  !
end subroutine fits_get_header_card_i8
!
subroutine fits_get_header_card_r4(fits,iname,r4,found,error,target)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>fits_get_header_card_r4
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private-generic fits_get_header_card
  !  Translate a card to its associated value
  ! ---
  !  R*4 version
  !---------------------------------------------------------------------
  type(classfits_t),          intent(inout) :: fits    !
  character(len=*),           intent(in)    :: iname   ! Card name
  real(kind=4),               intent(inout) :: r4      ! inout: if not found, input is preserved
  logical,                    intent(out)   :: found   !
  logical,                    intent(inout) :: error   ! Logical error flag
  character(len=*), optional, intent(in)    :: target  ! Target name
  ! Local
  character(len=*), parameter :: rname='FITS'
  character(len=80) :: string
  integer(kind=4) :: ier
  !
  call fits_get_header_key2val(fits%head,iname,string,found)
  !
  if (present(target)) then
    call fits_warn_missing_r4(fits%warn,'Card',iname,target,r4,found,error)
    if (error)  return
  endif
  !
  if (.not.found)  return
  !
  read(string,*,iostat=ier)  r4
  if (ier.ne.0) then
    call class_message(seve%e,rname,  &
      'Error decoding meta keyword '//trim(iname)//' = "'//trim(string)//'"')
    call putios('E-FITS,  ',ier)
    error = .true.
  endif
  !
end subroutine fits_get_header_card_r4
!
subroutine fits_get_header_card_r8(fits,iname,r8,found,error,target)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>fits_get_header_card_r8
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private-generic fits_get_header_card
  !  Translate a card to its associated value
  ! ---
  !  R*8 version
  !---------------------------------------------------------------------
  type(classfits_t),          intent(inout) :: fits    !
  character(len=*),           intent(in)    :: iname   ! Card name
  real(kind=8),               intent(inout) :: r8      ! inout: if not found, input is preserved
  logical,                    intent(out)   :: found   !
  logical,                    intent(inout) :: error   ! Logical error flag
  character(len=*), optional, intent(in)    :: target  ! Target name
  ! Local
  character(len=*), parameter :: rname='FITS'
  character(len=80) :: string
  integer(kind=4) :: ier
  !
  call fits_get_header_key2val(fits%head,iname,string,found)
  !
  if (present(target)) then
    call fits_warn_missing_r8(fits%warn,'Card',iname,target,r8,found,error)
    if (error)  return
  endif
  !
  if (.not.found)  return
  !
  read(string,*,iostat=ier)  r8
  if (ier.ne.0) then
    call class_message(seve%e,rname,  &
      'Error decoding meta keyword '//trim(iname)//' = "'//trim(string)//'"')
    call putios('E-FITS,  ',ier)
    error = .true.
  endif
  !
end subroutine fits_get_header_card_r8
!
subroutine fits_get_header_card_l(fits,iname,l,found,error,target)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>fits_get_header_card_l
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private-generic fits_get_header_card
  !  Translate a card to its associated value
  ! ---
  !  Logical version
  !---------------------------------------------------------------------
  type(classfits_t),          intent(inout) :: fits    !
  character(len=*),           intent(in)    :: iname   ! Card name
  logical,                    intent(inout) :: l       ! inout: if not found, input is preserved
  logical,                    intent(out)   :: found   !
  logical,                    intent(inout) :: error   ! Logical error flag
  character(len=*), optional, intent(in)    :: target  ! Target name
  ! Local
  character(len=*), parameter :: rname='FITS'
  character(len=80) :: string
  integer(kind=4) :: ier
  !
  call fits_get_header_key2val(fits%head,iname,string,found)
  if (.not.found)  return
  !
  read(string,*,iostat=ier)  l
  if (ier.ne.0) then
    call class_message(seve%e,rname,  &
      'Error decoding meta keyword '//trim(iname)//' = "'//trim(string)//'"')
    call putios('E-FITS,  ',ier)
    error = .true.
  endif
  !
end subroutine fits_get_header_card_l
!
subroutine fits_get_header_card_cc(fits,iname,value,found,error,target)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>fits_get_header_card_cc
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private-generic fits_get_header_card
  !  Translate a card to its associated value
  ! ---
  !  Character*(*) version
  !---------------------------------------------------------------------
  type(classfits_t),          intent(inout) :: fits    !
  character(len=*),           intent(in)    :: iname   ! Card name
  character(len=*),           intent(inout) :: value   ! inout: if not found, input is preserved
  logical,                    intent(out)   :: found   !
  logical,                    intent(inout) :: error   ! Logical error flag
  character(len=*), optional, intent(in)    :: target  ! Target name
  ! Local
  character(len=80) :: string
  !
  call fits_get_header_key2val(fits%head,iname,string,found)
  !
  if (present(target)) then
    call fits_warn_missing_cc(fits%warn,'Card',iname,target,value,found,error)
    if (error)  return
  endif
  !
  if (.not.found)  return
  !
  value = gfits_unquote(string)
  !
end subroutine fits_get_header_card_cc
!
!-----------------------------------------------------------------------
!
subroutine fits_get_header_metacard_i4(fits,iname,i4,found,error,target)
  use classcore_interfaces, except_this=>fits_get_header_metacard_i4
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private-generic fits_get_header_metacard
  !  Translate meta-cards to their associated value, i.e. follow the
  ! indirection due to hierarch cards. For example:
  !  HIERARCH  key.META_20='bbtype'
  !  META_20 =                 6031 / [] Building Block Type
  ! The parsing here is probably very HIFI-specific
  ! ---
  !  I*4 version
  !---------------------------------------------------------------------
  type(classfits_t),          intent(inout) :: fits    !
  character(len=*),           intent(in)    :: iname   ! Card name
  integer(kind=4),            intent(inout) :: i4      ! inout: if not found, input is preserved
  logical,                    intent(out)   :: found   !
  logical,                    intent(inout) :: error   ! Logical error flag
  character(len=*), optional, intent(in)    :: target  ! Target name
  ! Local
  character(len=8) :: key
  !
  call fits_get_header_val2key(fits%head,iname,key,found)
  !
  if (found) &
    call fits_get_header_card_i4(fits,key,i4,found,error)
  !
  if (present(target)) then
    call fits_warn_missing_i4(fits%warn,'Metacard',iname,target,i4,found,error)
    if (error)  return
  endif
  !
end subroutine fits_get_header_metacard_i4
!
subroutine fits_get_header_metacard_i8(fits,iname,i8,found,error,target)
  use classcore_interfaces, except_this=>fits_get_header_metacard_i8
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private-generic fits_get_header_metacard
  !  Translate meta-cards to their associated value, i.e. follow the
  ! indirection due to hierarch cards. For example:
  !  HIERARCH  key.META_20='bbtype'
  !  META_20 =                 6031 / [] Building Block Type
  ! The parsing here is probably very HIFI-specific
  ! ---
  !  I*8 version
  !---------------------------------------------------------------------
  type(classfits_t),          intent(inout) :: fits    !
  character(len=*),           intent(in)    :: iname   ! Card name
  integer(kind=8),            intent(inout) :: i8      ! inout: if not found, input is preserved
  logical,                    intent(out)   :: found   !
  logical,                    intent(inout) :: error   ! Logical error flag
  character(len=*), optional, intent(in)    :: target  ! Target name
  ! Local
  character(len=8) :: key
  !
  call fits_get_header_val2key(fits%head,iname,key,found)
  !
  if (found) &
    call fits_get_header_card_i8(fits,key,i8,found,error)
  !
  if (present(target)) then
    call fits_warn_missing_i8(fits%warn,'Metacard',iname,target,i8,found,error)
    if (error)  return
  endif
  !
end subroutine fits_get_header_metacard_i8
!
subroutine fits_get_header_metacard_r4(fits,iname,r4,found,error,target)
  use classcore_interfaces, except_this=>fits_get_header_metacard_r4
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private-generic fits_get_header_metacard
  !  Translate meta-cards to their associated value, i.e. follow the
  ! indirection due to hierarch cards. For example:
  !  HIERARCH  key.META_20='bbtype'
  !  META_20 =                 6031 / [] Building Block Type
  ! The parsing here is probably very HIFI-specific
  ! ---
  !  R*4 version
  !---------------------------------------------------------------------
  type(classfits_t),          intent(inout) :: fits    !
  character(len=*),           intent(in)    :: iname   ! Card name
  real(kind=4),               intent(inout) :: r4      ! inout: if not found, input is preserved
  logical,                    intent(out)   :: found   !
  logical,                    intent(inout) :: error   ! Logical error flag
  character(len=*), optional, intent(in)    :: target  ! Target name
  ! Local
  character(len=8) :: key
  !
  call fits_get_header_val2key(fits%head,iname,key,found)
  !
  if (found) &
    call fits_get_header_card_r4(fits,key,r4,found,error)
  !
  if (present(target)) then
    call fits_warn_missing_r4(fits%warn,'Metacard',iname,target,r4,found,error)
    if (error)  return
  endif
  !
end subroutine fits_get_header_metacard_r4
!
subroutine fits_get_header_metacard_r8(fits,iname,r8,found,error,target)
  use classcore_interfaces, except_this=>fits_get_header_metacard_r8
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private-generic fits_get_header_metacard
  !  Translate meta-cards to their associated value, i.e. follow the
  ! indirection due to hierarch cards. For example:
  !  HIERARCH  key.META_20='bbtype'
  !  META_20 =                 6031 / [] Building Block Type
  ! The parsing here is probably very HIFI-specific
  ! ---
  !  R*8 version
  !---------------------------------------------------------------------
  type(classfits_t),          intent(inout) :: fits    !
  character(len=*),           intent(in)    :: iname   ! Card name
  real(kind=8),               intent(inout) :: r8      ! inout: if not found, input is preserved
  logical,                    intent(out)   :: found   !
  logical,                    intent(inout) :: error   ! Logical error flag
  character(len=*), optional, intent(in)    :: target  ! Target name
  ! Local
  character(len=8) :: key
  !
  call fits_get_header_val2key(fits%head,iname,key,found)
  !
  if (found) &
    call fits_get_header_card_r8(fits,key,r8,found,error)
  !
  if (present(target)) then
    call fits_warn_missing_r8(fits%warn,'Metacard',iname,target,r8,found,error)
    if (error)  return
  endif
  !
end subroutine fits_get_header_metacard_r8
!
subroutine fits_get_header_metacard_l(fits,iname,l,found,error,target)
  use classcore_interfaces, except_this=>fits_get_header_metacard_l
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private-generic fits_get_header_metacard
  !  Translate meta-cards to their associated value, i.e. follow the
  ! indirection due to hierarch cards. For example:
  !  HIERARCH  key.META_20='bbtype'
  !  META_20 =                 6031 / [] Building Block Type
  ! The parsing here is probably very HIFI-specific
  ! ---
  !  Logical version
  !---------------------------------------------------------------------
  type(classfits_t),          intent(inout) :: fits    !
  character(len=*),           intent(in)    :: iname   ! Card name
  logical,                    intent(inout) :: l       ! inout: if not found, input is preserved
  logical,                    intent(out)   :: found   !
  logical,                    intent(inout) :: error   ! Logical error flag
  character(len=*), optional, intent(in)    :: target  ! Target name
  ! Local
  character(len=8) :: key
  !
  call fits_get_header_val2key(fits%head,iname,key,found)
  !
  if (found) &
    call fits_get_header_card_l(fits,key,l,found,error)
  !
end subroutine fits_get_header_metacard_l
!
subroutine fits_get_header_metacard_cc(fits,iname,value,found,error,target)
  use classcore_interfaces, except_this=>fits_get_header_metacard_cc
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private-generic fits_get_header_metacard
  !  Translate meta-cards to their associated value, i.e. follow the
  ! indirection due to hierarch cards. For example:
  !  HIERARCH  key.META_20='bbtype'
  !  META_20 =                 6031 / [] Building Block Type
  ! The parsing here is probably very HIFI-specific
  ! ---
  !  Character*(*) version
  !---------------------------------------------------------------------
  type(classfits_t),          intent(inout) :: fits    !
  character(len=*),           intent(in)    :: iname   ! Card name
  character(len=*),           intent(inout) :: value   ! inout: if not found, input is preserved
  logical,                    intent(out)   :: found   !
  logical,                    intent(inout) :: error   ! Logical error flag
  character(len=*), optional, intent(in)    :: target  ! Target name
  ! Local
  character(len=8) :: key
  !
  call fits_get_header_val2key(fits%head,iname,key,found)
  !
  if (found) &
    call fits_get_header_card_cc(fits,key,value,found,error)
  !
  if (present(target)) then
    call fits_warn_missing_cc(fits%warn,'Metacard',iname,target,value,found,error)
    if (error)  return
  endif
  !
end subroutine fits_get_header_metacard_cc
!
subroutine fits_get_header_val2key(head,val,key,found)
  use classcore_dependencies_interfaces
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Find the key corresponding to input value. Key must start with
  ! "key." to avoid false positives. This prefix is stripped off on
  ! return. Search is case sensitive. For example
  !    HIERARCH  key.META_20='bbtype'
  ! returns "META_20".
  !---------------------------------------------------------------------
  type(classfits_header_t), intent(in)  :: head   ! Unit header
  character(len=*),         intent(in)  :: val    ! Desired value name
  character(len=*),         intent(out) :: key    ! Corresponding key name
  logical,                  intent(out) :: found  !
  ! Local
  integer(kind=4) :: icard
  !
  found = .false.
  key = ''
  !
  do icard=1,head%dict%ncard
    if (val.eq.gfits_unquote(head%dict%card(icard)%val)) then
      if (head%dict%card(icard)%key(1:4).eq.'key.') then
        key = head%dict%card(icard)%key(5:)
        found = .true.
        return
      endif
    endif
  enddo
  !
end subroutine fits_get_header_val2key
!
subroutine fits_get_header_key2val(head,key,val,found)
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Find the (string) value corresponding to input key
  !---------------------------------------------------------------------
  type(classfits_header_t), intent(in)  :: head   ! Unit header
  character(len=*),         intent(in)  :: key    ! Key name
  character(len=*),         intent(out) :: val    ! Value found
  logical,                  intent(out) :: found  !
  ! Local
  integer(kind=4) :: icard
  !
  found = .false.
  val = ''
  !
  do icard=1,head%dict%ncard
    if (key.eq.head%dict%card(icard)%key) then
      val = head%dict%card(icard)%val
      found = .true.
      return
    endif
  enddo
  !
end subroutine fits_get_header_key2val
!
subroutine fits_get_bintable_key2column(cols,key,icol,found)
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private
  ! Find the column number whose TTYPE is 'key'
  ! Case-sensitive search
  !---------------------------------------------------------------------
  type(classfits_columns_t), intent(in)  :: cols   ! Columns description
  character(len=*),          intent(in)  :: key    ! Key name
  integer(kind=4),           intent(out) :: icol   ! Column number, if found
  logical,                   intent(out) :: found  !
  ! Local
  integer(kind=4) :: jcol
  !
  found = .false.
  icol = 0
  !
  do jcol=1,cols%desc%ncols
    if (cols%desc%ttype(jcol).eq.key) then
      icol = jcol
      found = .true.
      return
    endif
  enddo
  !
end subroutine fits_get_bintable_key2column
!
!-----------------------------------------------------------------------
!
subroutine fits_get_metacard_or_column_i4(fits,buffer,name,i4,found,error,target)
  use gbl_format
  use classcore_interfaces, except_this=>fits_get_metacard_or_column_i4
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private-generic fits_get_metacard_or_column
  !  If buffer(:) is zero-sized, read the metacard 'name' from the
  ! FITS header
  !  If not, read the single-valued column 'name' from the bintable
  !  NB: zero-sized arrays are legitimate objects in Fortran
  ! ---
  !  I*4 version
  !---------------------------------------------------------------------
  type(classfits_t),          intent(inout) :: fits       !
  integer(kind=1),            intent(in)    :: buffer(:)  !
  character(len=*),           intent(in)    :: name       ! Element name (case-sensitive)
  integer(kind=4),            intent(inout) :: i4         ! Previous value preserved if not found
  logical,                    intent(out)   :: found      !
  logical,                    intent(inout) :: error      !
  character(len=*), optional, intent(in)    :: target     ! Target name
  ! Local
  character(len=8) :: ftype
  integer(kind=4) :: icol
  !
  if (size(buffer).eq.0) then
    ! Get a metacard value
    ftype = 'Metacard'
    call fits_get_header_metacard(fits,name,i4,found,error)
    if (error)  return
  else
    ! Get a column single value
    ftype = 'Column'
    call fits_get_bintable_key2column(fits%cols,name,icol,found)
    if (found) then
      call get_item(i4,1,fmt_i4,buffer(fits%cols%desc%addr(icol)),  &
          fits%cols%desc%fmt(icol),error)
      if (error)  return
    endif
  endif
  !
  if (present(target)) then
    call fits_warn_missing_i4(fits%warn,ftype,name,target,i4,found,error)
    if (error)  return
  endif
  !
end subroutine fits_get_metacard_or_column_i4
!
subroutine fits_get_metacard_or_column_r4(fits,buffer,name,r4,found,error,target)
  use gbl_format
  use classcore_interfaces, except_this=>fits_get_metacard_or_column_r4
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private-generic fits_get_metacard_or_column
  !  If buffer(:) is zero-sized, read the metacard 'name' from the
  ! FITS header
  !  If not, read the single-valued column 'name' from the bintable
  !  NB: zero-sized arrays are legitimate objects in Fortran
  ! ---
  !  R*4 version
  !---------------------------------------------------------------------
  type(classfits_t),          intent(inout) :: fits       !
  integer(kind=1),            intent(in)    :: buffer(:)  !
  character(len=*),           intent(in)    :: name       ! Element name (case-sensitive)
  real(kind=4),               intent(inout) :: r4         ! Target value (preserved if not found)
  logical,                    intent(out)   :: found      !
  logical,                    intent(inout) :: error      !
  character(len=*), optional, intent(in)    :: target     ! Target name
  ! Local
  character(len=8) :: ftype
  integer(kind=4) :: icol
  !
  if (size(buffer).eq.0) then
    ! Get a metacard value
    ftype = 'Metacard'
    call fits_get_header_metacard(fits,name,r4,found,error)
    if (error)  return
  else
    ! Get a column single value
    ftype = 'Column'
    call fits_get_bintable_key2column(fits%cols,name,icol,found)
    if (found) then
      call get_item(r4,1,fmt_r4,buffer(fits%cols%desc%addr(icol)),  &
          fits%cols%desc%fmt(icol),error)
      if (error)  return
    endif
  endif
  !
  if (present(target)) then
    call fits_warn_missing_r4(fits%warn,ftype,name,target,r4,found,error)
    if (error)  return
  endif
  !
end subroutine fits_get_metacard_or_column_r4
!
subroutine fits_get_metacard_or_column_r8(fits,buffer,name,r8,found,error,target)
  use gbl_format
  use classcore_interfaces, except_this=>fits_get_metacard_or_column_r8
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private-generic fits_get_metacard_or_column
  !  If buffer(:) is zero-sized, read the metacard 'name' from the
  ! FITS header
  !  If not, read the single-valued column 'name' from the bintable
  !  NB: zero-sized arrays are legitimate objects in Fortran
  ! ---
  !  Scalar R*8 version
  !---------------------------------------------------------------------
  type(classfits_t),          intent(inout) :: fits       !
  integer(kind=1),            intent(in)    :: buffer(:)  !
  character(len=*),           intent(in)    :: name       ! Element name (case-sensitive)
  real(kind=8),               intent(inout) :: r8         ! Previous value preserved if not found
  logical,                    intent(out)   :: found      !
  logical,                    intent(inout) :: error      !
  character(len=*), optional, intent(in)    :: target     ! Target name
  ! Local
  character(len=8) :: ftype
  integer(kind=4) :: icol
  !
  if (size(buffer).eq.0) then
    ! Get a metacard value
    ftype = 'Metacard'
    call fits_get_header_metacard(fits,name,r8,found,error)
    if (error)  return
  else
    ! Get a column single value
    ftype = 'Column'
    call fits_get_bintable_key2column(fits%cols,name,icol,found)
    if (found) then
      call get_item(r8,1,fmt_r8,buffer(fits%cols%desc%addr(icol)),  &
          fits%cols%desc%fmt(icol),error)
      if (error)  return
    endif
  endif
  !
  if (present(target)) then
    call fits_warn_missing_r8(fits%warn,ftype,name,target,r8,found,error)
    if (error)  return
  endif
  !
end subroutine fits_get_metacard_or_column_r8
!
!-----------------------------------------------------------------------
!
subroutine fits_warn_missing_i4(warn,ftype,fname,tname,tvalue,found,error)
  use gbl_message
  use classcore_interfaces, except_this=>fits_warn_missing_i4
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private-generic fits_warn_missing
  !  If not found, add a warning to the warning list
  ! ---
  !  I*4 version
  !---------------------------------------------------------------------
  type(classfits_warnings_t), intent(inout) :: warn    !
  character(len=*),           intent(in)    :: ftype   ! FITS element type
  character(len=*),           intent(in)    :: fname   ! FITS element name
  character(len=*),           intent(in)    :: tname   ! Target name
  integer(kind=4),            intent(in)    :: tvalue  ! Target value
  logical,                    intent(in)    :: found   ! Was it found or not?
  logical,                    intent(inout) :: error   !
  ! Local
  character(len=message_length) :: mess
  !
  if (found)  return
  !
  write(mess,'(A,1X,4A,I0)')  trim(ftype),trim(fname),' not found, ',  &
    trim(tname),' defaults to ',tvalue
  !
  call fits_warning_add(warn,mess,error)
  if (error)  return
  !
end subroutine fits_warn_missing_i4
!
subroutine fits_warn_missing_i8(warn,ftype,fname,tname,tvalue,found,error)
  use gbl_message
  use classcore_interfaces, except_this=>fits_warn_missing_i8
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private-generic fits_warn_missing
  !  If not found, add a warning to the warning list
  ! ---
  !  I*8 version
  !---------------------------------------------------------------------
  type(classfits_warnings_t), intent(inout) :: warn    !
  character(len=*),           intent(in)    :: ftype   ! FITS element type
  character(len=*),           intent(in)    :: fname   ! FITS element name
  character(len=*),           intent(in)    :: tname   ! Target name
  integer(kind=8),            intent(in)    :: tvalue  ! Target value
  logical,                    intent(in)    :: found   ! Was it found or not?
  logical,                    intent(inout) :: error   !
  ! Local
  character(len=message_length) :: mess
  !
  if (found)  return
  !
  write(mess,'(A,1X,4A,I0)')  trim(ftype),trim(fname),' not found, ',  &
    trim(tname),' defaults to ',tvalue
  !
  call fits_warning_add(warn,mess,error)
  if (error)  return
  !
end subroutine fits_warn_missing_i8
!
subroutine fits_warn_missing_r4(warn,ftype,fname,tname,tvalue,found,error)
  use gbl_message
  use classcore_interfaces, except_this=>fits_warn_missing_r4
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private-generic fits_warn_missing
  !  If not found, add a warning to the warning list
  ! ---
  !  R*4 version
  !---------------------------------------------------------------------
  type(classfits_warnings_t), intent(inout) :: warn    !
  character(len=*),           intent(in)    :: ftype   ! FITS element type
  character(len=*),           intent(in)    :: fname   ! FITS element name
  character(len=*),           intent(in)    :: tname   ! Target name
  real(kind=4),               intent(in)    :: tvalue  ! Target value
  logical,                    intent(in)    :: found   ! Was it found or not?
  logical,                    intent(inout) :: error   !
  ! Local
  character(len=message_length) :: mess
  !
  if (found)  return
  !
  write(mess,'(A,1X,4A,F0.1)')  trim(ftype),trim(fname),' not found, ',  &
    trim(tname),' defaults to ',tvalue
  !
  call fits_warning_add(warn,mess,error)
  if (error)  return
  !
end subroutine fits_warn_missing_r4
!
subroutine fits_warn_missing_r8(warn,ftype,fname,tname,tvalue,found,error)
  use gbl_message
  use classcore_interfaces, except_this=>fits_warn_missing_r8
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private-generic fits_warn_missing
  !  If not found, add a warning to the warning list
  ! ---
  !  R*8 version
  !---------------------------------------------------------------------
  type(classfits_warnings_t), intent(inout) :: warn    !
  character(len=*),           intent(in)    :: ftype   ! FITS element type
  character(len=*),           intent(in)    :: fname   ! FITS element name
  character(len=*),           intent(in)    :: tname   ! Target name
  real(kind=8),               intent(in)    :: tvalue  ! Target value
  logical,                    intent(in)    :: found   ! Was it found or not?
  logical,                    intent(inout) :: error   !
  ! Local
  character(len=message_length) :: mess
  !
  if (found)  return
  !
  write(mess,'(A,1X,4A,F0.1)')  trim(ftype),trim(fname),' not found, ',  &
    trim(tname),' defaults to ',tvalue
  !
  call fits_warning_add(warn,mess,error)
  if (error)  return
  !
end subroutine fits_warn_missing_r8
!
subroutine fits_warn_missing_cc(warn,ftype,fname,tname,tvalue,found,error)
  use gbl_message
  use classcore_interfaces, except_this=>fits_warn_missing_cc
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private-generic fits_warn_missing
  !  If not found, add a warning to the warning list
  ! ---
  !  C*(*) version
  !---------------------------------------------------------------------
  type(classfits_warnings_t), intent(inout) :: warn    !
  character(len=*),           intent(in)    :: ftype   ! FITS element type
  character(len=*),           intent(in)    :: fname   ! FITS element name
  character(len=*),           intent(in)    :: tname   ! Target name
  character(len=*),           intent(in)    :: tvalue  ! Target value
  logical,                    intent(in)    :: found   ! Was it found or not?
  logical,                    intent(inout) :: error   !
  ! Local
  character(len=message_length) :: mess
  !
  if (found)  return
  !
  if (tvalue.eq.'') then
    write(mess,'(A,1X,4A)')  trim(ftype),trim(fname),' not found, ',  &
      trim(tname),' defaults to blank string'
  else
    write(mess,'(A,1X,5A)')  trim(ftype),trim(fname),' not found, ',  &
      trim(tname),' defaults to ',tvalue
  endif
  !
  call fits_warning_add(warn,mess,error)
  if (error)  return
  !
end subroutine fits_warn_missing_cc
!
!-----------------------------------------------------------------------
!
subroutine fits_warning_add(warn,mess,error)
  use gbl_message
  use classcore_interfaces, except_this=>fits_warning_add
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Add a new warning to the warning list. Duplicates are not added
  !---------------------------------------------------------------------
  type(classfits_warnings_t), intent(inout) :: warn
  character(len=*),           intent(in)    :: mess
  logical,                    intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='FITS>WARNING'
  integer(kind=4) :: imess
  !
  ! Check if already present
  do imess=1,warn%n
    if (mess.eq.warn%mess(imess))  return
  enddo
  !
  ! A new one: check if we have room
  if (warn%n.ge.mwarning) then
    call class_message(seve%w,rname,'Warning buffer exhausted, message lost:')
    call class_message(seve%w,rname,mess)
    return
  endif
  !
  ! Add it
  warn%n = warn%n+1
  warn%mess(warn%n) = mess
  !
end subroutine fits_warning_add
!
subroutine fits_warning_dump(warn,error)
  use gbl_message
  use classcore_interfaces, except_this=>fits_warning_dump
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Displays the warnings in the structure input structure.
  !---------------------------------------------------------------------
  type(classfits_warnings_t), intent(inout) :: warn
  logical,                    intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='FITS'
  integer(kind=4) :: imess
  !
  if (warn%n.le.0)  return
  !
  call class_message(seve%r,rname,'')  ! Blank line
  call class_message(seve%w,rname,'--- Warning summary (all extensions) ---')
  do imess=1,warn%n
    call class_message(seve%w,rname,warn%mess(imess))
  enddo
  !
end subroutine fits_warning_dump
