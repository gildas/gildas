subroutine class_comment(line,r,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_comment
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! CLASS ANALYSE Support routine for command
  !  COMMENT APPEND or WRITE "New comment text"
  !          EDIT
  !          READ
  !          DELETE
  ! Reads, writes and modifies the comment section.
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: line   ! Command line
  type(observation), intent(inout) :: r      !
  logical,           intent(inout) :: error  ! flag
  ! Local
  character(len=*), parameter :: rname='COMMENT'
  integer(kind=4) :: nkey,i,nc
  integer(kind=4), parameter :: mkey=5
  character(len=8) :: keys(mkey),argum,keywor
  character(len=9) :: prompt
  integer(kind=4) :: lprompt
  data keys/'APPEND','WRITE','EDIT','DELETE','READ'/
  !
  call sic_ke (line,0,1,argum,nc,.true.,error)
  if (error) return
  call sic_ambigs(rname,argum,keywor,nkey,keys,mkey,error)
  if (error) return
  !
  select case (keywor)
  case ('READ')
     if (r%head%presec(class_sec_com_id)) then
        do i = 1,r%head%com%ltext,76
           write(6,'(1X,A)') r%head%com%ctext(i:min(i+75,r%head%com%ltext))
        enddo
     else
        call class_message(seve%w,rname,'No comment section present')
     endif
  case ('WRITE','APPEND')
     if (keywor.eq.'APPEND')  call class_message(seve%w,rname,  &
       'COMMENT APPEND is obsolescent, use COMMENT WRITE instead')
     if (r%head%presec(class_sec_com_id)) then
        call sic_ch (line,0,2,r%head%com%ctext(r%head%com%ltext+1:),nc,.true.,error)
        if (error) return
     else
        r%head%com%ltext = 0
        call sic_ch (line,0,2,r%head%com%ctext,nc,.true.,error)
        if (error) return
        r%head%presec(class_sec_com_id) = .true.
     endif
     r%head%com%ltext = lenc(r%head%com%ctext)
  case ('EDIT')
     prompt = 'COMMENT: '
     lprompt = 9
     call sic_edit (r%head%com%ctext,r%head%com%ltext,prompt,lprompt)
     r%head%presec(class_sec_com_id) = .true.
  case ('DELETE')
     r%head%com%ltext = 0
     r%head%presec(class_sec_com_id) = .false.
  end select
end subroutine class_comment
