subroutine coffse(set,rname,value,unit,offset,error)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>coffse
  use class_setup_new
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Converts the string value in current units (LAS\SET ANGLE) or
  ! specified unit to internal value (radians).
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set     !
  character(len=*),    intent(in)    :: rname   ! Caller name
  character(len=*),    intent(in)    :: value   ! String containing the angle value
  character(len=*),    intent(in)    :: unit    ! String containing the angle unit
  real(kind=4),        intent(out)   :: offset  ! Output angle in radians
  logical,             intent(inout) :: error   ! Logical error flag
  ! Local
  real(kind=8) :: doubl,fangle
  character(len=3) :: angle
  integer(kind=4) :: n
  !
  n = lenc(value)
  if (value(1:n).eq.'*') return  ! Offset not modified
  !
  ! Decode the value
  call sic_math_dble(value,n,doubl,error)
  if (error) return
  !
  if (unit.eq.' ') then
    ! Value is provided in current unit (LAS\SET ANGLE)
    fangle = class_setup_get_fangle()
  else
    ! Decode unit, compute conversion factor
    call set_angle_factor(rname,unit,angle,fangle,error)
    if (error)  return
  endif
  !
  offset = doubl/fangle
  !
end subroutine coffse
!
subroutine offsec(set,offset,line)
  use class_setup_new
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Converts an offset value from radian to a formatted string in
  ! current angle units (LAS\SET ANGLE)
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)  :: set     !
  real(kind=4),        intent(in)  :: offset  ! angle in radian
  character(len=*),    intent(out) :: line    ! formatted angle
  ! Local
  real(kind=8) :: double,match
  character(len=4) :: cany
  !
  call bytoch(offset,cany,4)
  if (cany.eq.'ANY ') then
     line = '  *'
  else
     double = dble(offset)*class_setup_get_fangle()
     match  = 0.1*set%tole*class_setup_get_fangle()
     if (abs(double-int(double)).lt.match) then
        if (abs(double).le.9999.99d0) then
           write(line,'(SP,f8.1)') double
        else
           write(line,'(SP,es8.1e1)') double
        endif
     else
        if (abs(double).le.9999.99d0.and.abs(double).ge.0.1d0) then
           write(line,'(SP,f8.1)') double
        else
           write(line,'(SP,es8.1e1)') double
        endif
     endif
  endif
end subroutine offsec
