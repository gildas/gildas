subroutine class_table(set,line,r,error,user_function)
  use gildas_def
  use image_def
  use gbl_constant
  use gbl_message
  use gkernel_interfaces
  use classcore_interfaces, except_this=>class_table
  use class_types
  use class_data
  use class_index
  use class_averaging
  !----------------------------------------------------------------------
  ! @ public
  ! CLASS Support routine for command
  !   TABLE Filename [OLD|NEW]
  ! 1         [/MATH Expression1 ... ExpressionN]
  ! 2         [/RANGE rmin rmax unit]
  ! 3         [/RESAMPLE nc ref val inc unit [shape] [width]]
  ! 4         [/FFT]
  ! 5         [/FREQUENCY name rest-freq]
  ! 6         [/NOCHECK [SOURCE|POSITION|LINE|SPECTROSCOPY]]
  ! 7         [/SIGMA]  (obsolete)
  ! 8         [/WEIGHT Time|Sigma|Equal]
  ! 9         [/LIKE GDFFile]
  !----------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  character(len=*),    intent(in)    :: line           ! Command line
  type(observation),   intent(inout) :: r              ! For TABLE /MATH expression
  logical,             intent(inout) :: error          ! Logical error flag
  logical,             external      :: user_function  !
  ! Local
  character(len=*), parameter :: rname='TABLE'
  integer(kind=4), parameter :: mexp=20  ! Maximum number of expressions in MATH mode
  character(len=256) :: expression(mexp)
  character(len=filename_length) :: filename
  character(len=16) :: new_line
  integer(kind=4) :: nexp,lexp(mexp),nchanmax
  integer(kind=entry_length) :: ksave
  real(kind=8) :: new_restf
  !
  type(observation) :: obs
  type(consistency_t) :: cons
  type(header) :: ref_head
  type(resampling) :: faxis
  type(average_range_t) :: range
  type(gildas) :: htable
  !
  logical :: append,math,fft,frequency
  logical :: resample_index,donasmyth
  character(len=1) :: weighting
  !
  ! Ideally 'filename' should be the only saved parameter.
  ! All other parameters should be deduced from the old table.
  ! This way, we ensure that the user can achieve the same results
  ! between two consecutive CLASS sessions.
  save filename
  !
  if (set%kind.ne.kind_spec) then
    call class_message(seve%e,rname,'Only spectroscopic data is supported')
    error = .true.
    return
  elseif (.not.filein_opened(rname,error)) then
    return
  elseif (cx%next.le.1) then
    call class_message(seve%e,rname,'Index is empty')
    error = .true.
    return
  endif
  !
  call class_table_parse(set,line,filename,append,math,nexp,expression,lexp,  &
    fft,faxis,range,frequency,new_line,new_restf,weighting,donasmyth,cons,  &
    error,user_function)
  if (error)  return
  !
  ! Interact with table header:
  !   * Fill table header from ref header (set to obs) in new mode
  !   * Fill ref header from table header in append mode
  ! Check consistency of the index in both cases, and check its consistency
  ! with the old table in append mode.
  call class_table_header(set,filename,ref_head,htable,append,frequency,    &
    new_line,new_restf,weighting,faxis,range,math,nexp,expression,lexp,cons,  &
    resample_index,nchanmax,error,user_function)
  if (error)  return
  !
  ! Data treatment
  if (math) then
     ! In math mode, all computation are done through SIC using the SIC
     ! variables associated to the R buffer => We must use the R buffer, this
     ! is more complex.
     !  The idea is the following:
     ! 1) we load the first spectrum from the index, apply the corrections
     !   (new restf, resampling, etc), and run the math expression. Trap
     !   any error.
     ! 2) we use R as working buffer in the data loop. The first step above
     !   is not strictly needed, as we could have trap the errors in the data
     !   loop, but this avoids starting the loop, opening the buffers, etc.
     !
     call reallocate_obs(r,nchanmax,error)
     if (error)  return
     !
     ! Read first index spectrum
     ksave = knext
     call get_first(set,r,user_function,error)
     knext = ksave
     if (error)  return
     !
     ! Change rest frequency and resample first spectrum if needed
     ! This is to ensure that the whole processing of the index is working at least
     ! on the first spectrum in real conditions (in particular, the following
     ! mathematical test)
     call class_table_first_changes(set,r,new_line,new_restf,resample_index,faxis,fft,error)
     if (error)  return
     !
     ! Check validity of math expression on R (first spectrum)
     call class_table_first_math(math,nexp,expression,lexp,error)
     if (error)  return
     !
     ! Real work (resampling, math computation, filling) is done here.
     call class_table_fill(set,htable,append,r,ref_head,resample_index,fft,  &
       faxis,new_restf,math,nexp,expression,lexp,weighting,donasmyth,  &
       user_function,error)
     if (error)  return
     !
  else
     ! Normal mode (we do not need the R buffer)
     ! Real work (resampling, math computation, filling) is done here.
     !
     call init_obs(obs)
     call reallocate_obs(obs,nchanmax,error)
     if (error)  return
     !
     call class_table_fill(set,htable,append,obs,ref_head,resample_index,fft,  &
       faxis,new_restf,math,nexp,expression,lexp,weighting,donasmyth,  &
       user_function,error)
     call free_obs(obs)  ! Free memory
     !
  endif
  if (error)  return
  !
end subroutine class_table
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine class_table_parse(set,line,filename,append,math,nexp,expression,  &
  lexp,fft,faxis,range,frequency,new_line,new_restf,weighting,donasmyth,  &
  cons,error,user_function)
  use gildas_def
  use gbl_constant
  use gbl_message
  use gkernel_interfaces
  use classcore_interfaces, except_this=>class_table_parse
  use class_index
  use class_averaging
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Parse the input command line and make first checks
  !---------------------------------------------------------------------
  type(class_setup_t),   intent(in)    :: set
  character(len=*),      intent(in)    :: line
  character(len=*),      intent(inout) :: filename
  logical,               intent(out)   :: append
  logical,               intent(out)   :: math
  integer(kind=4),       intent(out)   :: nexp  ! Number of /MATH expressions
  character(len=*),      intent(out)   :: expression(:)
  integer(kind=4),       intent(out)   :: lexp(:)
  logical,               intent(out)   :: fft
  type(resampling),      intent(out)   :: faxis
  type(average_range_t), intent(out)   :: range
  logical,               intent(out)   :: frequency
  character(len=*),      intent(out)   :: new_line
  real(kind=8),          intent(out)   :: new_restf
  character(len=*),      intent(out)   :: weighting
  logical,               intent(out)   :: donasmyth
  type(consistency_t),   intent(out)   :: cons
  logical,               intent(inout) :: error
  logical,               external      :: user_function
  ! Local
  character(len=*), parameter :: rname='TABLE'
  logical :: dorange,resample,dolike
  character(len=256) :: chain,key
  integer(kind=4) :: nchain,nrtype,nkey,iarg,nopt
  character(len=filename_length) :: file
  type(observation) :: obs
  character(len=message_length) :: mess
  integer(kind=4), parameter :: mrtype=4
  integer(kind=4), parameter :: matype=2
  character(len=12) :: rtype(mrtype)  ! Range type
  character(len=12) :: atype(matype)  ! Access type
  integer(kind=4), parameter :: optmath=1
  integer(kind=4), parameter :: optrange=2
  integer(kind=4), parameter :: optresample=3
  integer(kind=4), parameter :: optfft=4
  integer(kind=4), parameter :: optfrequency=5
  integer(kind=4), parameter :: optnocheck=6
  integer(kind=4), parameter :: optsigma=7
  integer(kind=4), parameter :: optweight=8
  integer(kind=4), parameter :: optlike=9
  integer(kind=4), parameter :: optnasmyth=10
  ! Data
  data rtype/'CHANNEL','VELOCITY','FREQUENCY','IMAGE'/
  data atype/'NEW','OLD'/
  !
  error = .false.
  !
  ! Default behaviors
  append    = .true.   ! Access mode is OLD (ie append)
  math      = .false.  ! Output spectra, not a mathematical expression
  dorange   = .false.  ! Use all available velocity axis
  resample  = .false.  ! Do not resample
  frequency = .false.  ! Do not change line name and rest frequency
  dolike    = .false.  !
  !
  call class_message(seve%i,rname,'Table modes:')
  !
  ! --- Command arguments ----------------------------------------------
  if (sic_present(0,1)) then
     ! Get table name
     nchain = 0
     call sic_ch(line,0,1,file,nchain,.true.,error)
     if (error) return
     call sic_parse_file(file,' ','.tab',filename)
     ! Get access mode
     if (sic_present(0,2)) then
        call sic_ke(line,0,2,chain,nchain,.true.,error)
        if (error) return
        call sic_ambigs(rname,chain,key,nkey,atype,matype,error)
        if (error)  return
        append = nkey.eq.2
     endif
  else
     ! No Table name => Current table
     if (len_trim(filename).eq.0) then
        call class_message(seve%e,rname,'Empty filename')
        error = .true.
        return
     endif
  endif
  !
  if (append) then
    if (gag_inquire(filename,len_trim(filename)).ne.0) then
      call class_message(seve%e,rname,'No such OLD file '//trim(filename))
      if (.not.sic_present(0,2))  &
        call class_message(seve%e,rname,'Missing NEW keyword?')
      error = .true.
      return
    endif
  endif
  !
  write(chain,'(A,L)') '  Append data: ',append
  call class_message(seve%r,rname,chain)
  !
  ! --- /MATH ----------------------------------------------------------
  if (sic_present(optmath,0)) then
    if (append) then
      call class_message(seve%e,rname,'Old table: /MATH option forbidden')
      error = .true.
      return
    endif
    !
    ! User want to compute a mathematical expression
    math=.true.
    ! Get mathematical expression
    nexp = size(expression)
    if (sic_narg(optmath).gt.nexp) then
      write(mess,'(A,I0,A)')  &
      'TABLE /MATH limited to ',nexp,' expressions (others are ignored)'
      call class_message(seve%w,rname,mess)
    else
      nexp = sic_narg(optmath)
    endif
    do iarg=1,nexp
      lexp(iarg) = 0
      call sic_ke(line,optmath,iarg,expression(iarg),lexp(iarg),.true.,error)
      if (error) return
    enddo
  else
    nexp = 0  ! Do not leave unset since it is used as actual array size
              ! later on (even if those arrays are not used)
  endif
  write(chain,'(A,L)') '  Math mode: ',math
  call class_message(seve%r,rname,chain)
  !
  ! --- /FREQUENCY -----------------------------------------------------
  ! This parsing must come before other options
  if (sic_present(optfrequency,0)) then
    if (append) then
      call class_message(seve%e,rname,'Old table: /FREQUENCY option forbidden')
      error = .true.
    else
      frequency = .true.
      call sic_ch(line,optfrequency,1,new_line,nchain,.true.,error)
      call sic_r8(line,optfrequency,2,new_restf,.true.,error)
    endif
    if (error)  return
  endif
  !
  ! --- /RANGE ---------------------------------------------------------
  if (sic_present(optrange,0)) then
    if (append) then
      call class_message(seve%e,rname,'Old table: /RANGE option forbidden')
      error = .true.
      return
    endif
    !
    dorange = .true.
    call sic_ke(line,optrange,3,chain,nchain,.true.,error)
    if (error)  return
    call sic_ambigs('TABLE /RANGE',chain,range%unit,nrtype,rtype,mrtype,error)
    if (error)  return
    ! CHANNEL, VELOCITY, FREQUENCY or IMAGE: read with real*8 arguments
    call sic_r8(line,optrange,1,range%xmin,.true.,error)
    if (error)  return
    call sic_r8(line,optrange,2,range%xmax,.true.,error)
    if (error)  return
  else
    range%unit = ''  ! No unit <=> no range
    range%xmin = 0.d0
    range%xmax = 0.d0
  endif
  !
  ! --- /FFT -----------------------------------------------------------
  fft = sic_present(optfft,0)
  if (fft.and.append) then
     call class_message(seve%e,rname,'Old table: /FFT option forbidden')
     error = .true.
     return
  endif
  !
  ! --- /SIGMA ---------------------------------------------------------
  ! Obsolete since 20-nov-2015
  if (sic_present(optsigma,0)) then
    call class_message(seve%e,rname,'/SIGMA is obsolete')
    call class_message(seve%e,rname,'Use TABLE /WEIGHT SIGMA or SET WEIGHT SIGMA instead')
    error = .true.
    return
  endif
  !
  ! --- /WEIGHT --------------------------------------------------------
  weighting = set%weigh
  if (sic_present(optweight,0)) then
    call setweight('TABLE /WEIGHT',line,optweight,1,weighting,error)
    if (error)  return
  endif
  if (weighting.eq.'A') then
    call class_message(seve%e,rname,'SET WEIGHT ASSOCIATED is not supported for TABLE')
    error = .true.
    return
  endif
  !
  ! --- /NOCHECK -------------------------------------------------------
  !
  ! cons is used to test the consistency of the first spectrum against
  ! the reference spectrum while cx%cons is used to test the internal
  ! consistency of the index (which may have already been checked to be
  ! inconsistent => no need to continue then...)
  !
  ! The selection algorithm is to check everything unless it has already
  ! been checked or the user does not want to check a particular criterion
  call consistency_check_selection(set,line,optnocheck,cx%cons,error)
  if (error)  return
  if ((cx%cons%gen%check.and.cx%cons%gen%done.and.cx%cons%gen%prob) .or.  &
      (cx%cons%sou%check.and.cx%cons%sou%done.and.cx%cons%sou%prob) .or.  &
      (cx%cons%pos%check.and.cx%cons%pos%done.and.cx%cons%pos%prob) .or.  &
      (cx%cons%lin%check.and.cx%cons%lin%done.and.cx%cons%lin%prob.and..not.frequency) .or.  &
      (cx%cons%cal%check.and.cx%cons%cal%done.and.cx%cons%cal%prob)) then
    ! Do not bother about:
    !  - Spectroscopy, we will check it ourselves
    !  - Offset positions, since we are building a table
    !  - Line, if /FREQUENCY is present
    error = .true.
    return
  endif
  !
  ! Define a custom 'consistency' instance:
  call consistency_defaults(set,cons)
  call consistency_check_selection(set,line,optnocheck,cons,error)
  if (error)  return
  ! Overload some of the checks:
  cons%off%check = .false.  ! Never, we are building a table
  cons%spe%check = .true.   ! Always (the result helps us to enable resampling or not)
  cons%spe%mess  = .false.
  if (frequency) cons%lin%check = .false.
  !
  resample = sic_present(optresample,0)
  dolike = sic_present(optlike,0)
  ! --- /RESAMPLE ------------------------------------------------------
  if (resample) then
    ! Read first spectrum in index
    call init_obs(obs)
    ! Read 1st observation. Fill header structure from buffer. Data not needed
    ! at this stage.
    call rheader(set,obs,cx%ind(1),user_function,error)
    if (error)  goto 100
    ! Apply /FREQUENCY option on the reference spectrum
    if (frequency) then
      call modify_frequency(obs,new_restf,error)
      if (error)  goto 100
    endif
    !
    call resample_parse_command(line,optresample,rname,obs%head,faxis,error)
    if (error)  goto 100
    !
    ! Free memory
100 call free_obs(obs)
    !
  ! --- /LIKE ----------------------------------------------------------
  elseif (dolike) then
    call resample_parse_like(rname,line,optlike,faxis,error)
    if (error)  return
    !
  else
    faxis%nchan = 0  ! <=> no user-defined axis: automatic definition of the X axis
  endif
  !
  ! --- /NASMYTH -------------------------------------------------------
  donasmyth = sic_present(optnasmyth,0)
  !
  ! --- Exclusive options ----------------------------------------------
  nopt = 0
  if (math)      nopt = nopt+1
  if (dorange)   nopt = nopt+1
  if (resample)  nopt = nopt+1
  if (dolike)    nopt = nopt+1
  if (nopt.gt.1) then
     call class_message(seve%e,rname,  &
       '/MATH, /RANGE, /RESAMPLE and /LIKE options are exclusive')
     error = .true.
     return
  endif
  if (append .and. nopt.gt.0) then
    call class_message(seve%e,rname,  &
       '/MATH, /RANGE, /RESAMPLE and /LIKE options are forbidden in APPEND mode')
    error = .true.
    return
  endif
  !
end subroutine class_table_parse
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine class_table_header(set,filename,ref_head,htable,append,frequency,  &
   new_line,new_restf,weighting,faxis,range,math,nexp,expression,lexp,cons,     &
   resample,nchanmax,error,user_function)
  use image_def
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_table_header
  use class_types
  use class_averaging
  !----------------------------------------------------------------------
  ! @ private
  !   * In NEW mode, ref_head is set to first obs and then table header
  !     is filled from ref_head
  !   * In APPEND mode, ref_header is filled from table header
  !   * Finally ensure that the new quantities (rest frequency and x-axis
  !     definition) are defined in all cases
  !----------------------------------------------------------------------
  type(class_setup_t),   intent(in)    :: set            !
  character(len=*),      intent(in)    :: filename       ! Table filename
  type(header),          intent(out)   :: ref_head       ! Reference header for resampling and consistency checks
  type(gildas),          intent(out)   :: htable         ! Table header
  logical,               intent(in)    :: append         ! Append to the table?
  logical,               intent(in)    :: frequency      !
  character(len=*),      intent(inout) :: new_line       !
  real(kind=8),          intent(inout) :: new_restf      !
  character(len=*),      intent(in)    :: weighting      ! Weighting type
  type(resampling),      intent(inout) :: faxis          ! New x-axis definition
  type(average_range_t), intent(in)    :: range          ! Range requested
  logical,               intent(inout) :: math           ! Output a mathematical formula instead of the spectrum?
  integer(kind=4),       intent(inout) :: nexp           ! Number of /MATH expressions
  character(len=*),      intent(inout) :: expression(:)  ! String containing the mathematical expressions
  integer,               intent(inout) :: lexp(:)        ! Length of expression strings
  type(consistency_t),   intent(inout) :: cons           !
  logical,               intent(out)   :: resample       !
  integer(kind=4),       intent(out)   :: nchanmax       ! Largest spectrum size in index
  logical,               intent(out)   :: error          ! Error flag
  logical,               external      :: user_function  !
  !
  ! Initialization
  error = .false.
  call gildas_null(htable)
  call sic_parse_file(filename,' ','.tab',htable%file)
  !
  ! Either define the reference header from the table header or vice versa
  if (append) then
    call class_table_header_old(set,htable,ref_head,math,nexp,expression,lexp,  &
      faxis,cons,resample,nchanmax,error,user_function)
  else
     call class_table_header_new(set,ref_head,htable,frequency,new_line,       &
       new_restf,weighting,faxis,range,math,nexp,expression,lexp,cons,resample,  &
       nchanmax,error,user_function)
  endif
  if (error)  return
  !
  ! Ensure correct definition of new quantities (rest frequency and line name)
  new_line = ref_head%spe%line
  new_restf = ref_head%spe%restf
  !
end subroutine class_table_header
!
subroutine class_table_header_old(set,htable,ref_head,math,nexp,expression,  &
  lexp,faxis,cons,resample,nchanmax,error,user_function)
  use image_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_table_header_old
  use class_index
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Set the header when reusing an OLD table (append mode)
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  type(gildas),        intent(inout) :: htable         ! Table header
  type(header),        intent(out)   :: ref_head       ! Reference header for resampling and consistency checks
  logical,             intent(out)   :: math           ! Output a mathematical formula instead of the spectrum?
  integer(kind=4),     intent(out)   :: nexp           ! Number of /MATH expressions
  character(len=*),    intent(out)   :: expression(:)  ! String containing the mathematical expressions
  integer(kind=4),     intent(out)   :: lexp(:)        ! Length of expression strings
  type(resampling),    intent(out)   :: faxis          ! Resampling axis
  type(consistency_t), intent(inout) :: cons           !
  logical,             intent(out)   :: resample       !
  integer(kind=4),     intent(out)   :: nchanmax       ! Largest spectrum size in index
  logical,             intent(inout) :: error          ! Error flag
  logical,             external      :: user_function  !
  ! Local
  character(len=*), parameter :: rname='TABLE'
  type(observation) :: obs
  integer(kind=entry_length) :: iobs
  !
  ! Read table header
  call gdf_read_header(htable,error)
  if (gildas_error(htable,rname,error)) return
  !
  ! Free the image slot and associated lun (they will be reopened at write time)
  call gdf_close_image(htable,error)
  if (gildas_error(htable,rname,error)) return
  !
  call class_table_header_to_ref(htable,ref_head,math,nexp,expression,lexp,error)
  if (error)  return
  !
  ! Define the resampling axis
  faxis%unit = 'V'
  faxis%nchan = ref_head%spe%nchan
  faxis%ref = ref_head%spe%rchan
  faxis%val = ref_head%spe%voff
  faxis%inc = ref_head%spe%vres
  !
  ! Check consistency AFTER defining the X axis and reference header
  call consistency_tole(ref_head,cons)
  call consistency_print(set,ref_head,cons)
  !
  ! Consistency of the index against the reference header
  resample = .false.
  nchanmax = 0
  call init_obs(obs)
  do iobs = 1,cx%next-1
    ! Exit when asked by user
    call class_controlc(rname,error)
    if (error)  exit
    !
    ! Fill header structure from buffer. Data not needed at this stage.
    call rheader(set,obs,cx%ind(iobs),user_function,error)
    if (error)  exit
    !
    ! Check consistency
    call spectrum_consistency_check(set,ref_head,obs%head,cons)
    if ((cons%gen%check.and.cons%gen%prob) .or.  &
        (cons%sou%check.and.cons%sou%prob) .or.  &
        (cons%pos%check.and.cons%pos%prob) .or.  &
        (cons%lin%check.and.cons%lin%prob) .or.  &
        (cons%cal%check.and.cons%cal%prob)) then
      error = .true.
      exit
    endif
    if (cons%spe%prob) then
      ! One of the spectra is misaligned with the table. 2 possibilities:
      ! 1) in V range: no problem, do_resample will perform a resampling.
      !    If ranges do not intersect an error will be raised.
      ! 2) in F range (while we resample in Velocity...): no problem,
      !    there is a modify_frequency before do_resample, so that we
      !    fall back in case 1).
      resample = .true.
      ! No exit, keep running to find nchanmax
    endif
    !
    if (obs%head%spe%nchan.gt.nchanmax)  nchanmax = obs%head%spe%nchan
    !
  enddo
  call free_obs(obs)
  if (error)  return
  !
  if (resample) then
    call class_message(seve%i,rname,  &
      'Index X axes do not match with table X axis, resampling enabled')
  else
    call class_message(seve%i,rname,  &
      'Index X axes match with table X axis, no resampling will be performed')
  endif
  !
end subroutine class_table_header_old
!
subroutine class_table2class_spectro(htable,hclass,error)
  use gbl_constant
  use image_def
  use classcore_interfaces, except_this=>class_table2class_spectro
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Set the spectro section of a class header from a table header
  !---------------------------------------------------------------------
  type(gildas), intent(in)    :: htable
  type(header), intent(inout) :: hclass
  logical,      intent(inout) :: error
  !
  call gdf2class_spectro(htable,hclass,error)
  if (error)  return
  !
  ! Patch for specific Class-table format (XYW leading columns)
  hclass%spe%nchan = hclass%spe%nchan-3
  hclass%spe%rchan = hclass%spe%rchan-3.d0
  !
end subroutine class_table2class_spectro
!
subroutine class_class2table_spectro(htable,math,nexp,expression1,hclass,error)
  use gbl_constant
  use image_def
  use gbl_message
  use classcore_interfaces, except_this=>class_class2table_spectro
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Set the spectro section of a table header from a class header
  !---------------------------------------------------------------------
  type(header),     intent(in)    :: hclass       !
  logical,          intent(in)    :: math         ! /MATH mode?
  integer(kind=4),  intent(in)    :: nexp         ! Number of math expressions
  character(len=*), intent(in)    :: expression1  ! First math expression
  type(gildas),     intent(inout) :: htable       !
  logical,          intent(inout) :: error        !
  ! Local
  character(len=*), parameter :: rname='TABLE'
  integer(kind=4) :: faxi
  character(len=message_length) :: mess
  !
  faxi = 1
  call class2gdf_spectro(hclass,faxi,htable,error)
  if (error)  return
  !
  ! Patch for Class-table specific format
  if (math) then
    htable%gil%dim(faxi) = 3+nexp  ! X + Y + Weights + expression results
    htable%gil%ref(faxi) = 4.d0
    htable%gil%val(faxi) = 0.d0
    htable%gil%inc(faxi) = 1.d0
    htable%char%code(faxi) = expression1(1:12)  ! Can save only the 1st expression...
  else
    htable%gil%dim(faxi) = htable%gil%dim(faxi)+3  ! X + Y + Weights + Nchan
    htable%gil%ref(faxi) = htable%gil%ref(faxi)+3.d0
    write(mess,'(A,I0,3ES12.3)') 'Table velocity axis ',htable%gil%dim(faxi),  &
      htable%gil%ref(faxi),htable%gil%val(faxi),htable%gil%inc(faxi)
    call class_message(seve%i,rname,mess)
  endif
  !
end subroutine class_class2table_spectro
!
subroutine class_table_header_to_ref(htable,ref_head,math,nexp,expression,  &
  lexp,error)
  use gbl_constant
  use image_def
  use gbl_message
  use gkernel_interfaces
  use classcore_interfaces, except_this=>class_table_header_to_ref
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Define the Class observation header from the Gildas table header
  !---------------------------------------------------------------------
  type(gildas),     intent(in)    :: htable         ! Table header
  type(header),     intent(out)   :: ref_head       ! Reference header for resampling and consistency checks
  logical,          intent(out)   :: math           ! Output a mathematical formula instead of the spectrum?
  integer(kind=4),  intent(out)   :: nexp           ! Number of /MATH expressions
  character(len=*), intent(out)   :: expression(:)  ! String containing the mathematical expression
  integer(kind=4),  intent(out)   :: lexp(:)        ! Length of expression string
  logical,          intent(inout) :: error          ! Error flag
  ! Local
  character(len=*), parameter :: rname='TABLE'
  !
  ! Is it a mathematical expression?
  if (htable%char%code(1).ne.'VELOCITY'  .and.  &
      htable%char%code(1).ne.'FREQUENCY' .and.  &
      htable%char%code(1).ne.'CHANNELS') then
    math = .true.
    if (htable%gil%dim(1).ne.4) then
      call class_message(seve%e,rname,  &
        'Can not retrieve more than 1 expression from previous table header')
      error = .true.
      return
    endif
    nexp = 1
    expression(1) = htable%char%code(1)
    lexp(1) = len_trim(htable%char%code(1))
  else
    math = .false.
    nexp = 0
    expression(:) = ' '
    lexp(:) = 0
  endif
  !
  ! Disable all sections by default
  ref_head%presec(:) = .false.
  !
  ! General information
  ref_head%presec(class_sec_gen_id) = .true.
  ref_head%gen%kind = kind_spec
  !
  ! Position information
  ref_head%presec(class_sec_pos_id) = .true.
  ref_head%pos%sourc = htable%char%name
  if (htable%char%syst.eq.'EQUATORIAL') then
    ref_head%pos%system  = type_eq
    ref_head%pos%equinox = htable%gil%epoc
    ref_head%pos%lam     = htable%gil%ra
    ref_head%pos%bet     = htable%gil%dec
  elseif (htable%char%syst.eq.'GALACTIC') then
    ref_head%pos%system  = type_ga
    ref_head%pos%equinox = htable%gil%epoc  ! Equinox for alternate system
    ref_head%pos%lam     = htable%gil%lii
    ref_head%pos%bet     = htable%gil%bii
  elseif (htable%char%syst.eq.'ICRS') then
    ref_head%pos%system  = type_ic
    ref_head%pos%equinox = equinox_null
    ref_head%pos%lam     = htable%gil%ra
    ref_head%pos%bet     = htable%gil%dec
  else
    call class_message(seve%e,rname,'Unknown coordinate system')
    error = .true.
    return
  endif
  ref_head%pos%proj    = htable%gil%ptyp
  ref_head%pos%projang = htable%gil%pang
  !
  call class_table2class_spectro(htable,ref_head,error)
  if (error) return
  !
end subroutine class_table_header_to_ref
!
subroutine class_table_header_new(set,ref_head,htable,frequency,new_line,  &
  new_restf,weighting,faxis,range,math,nexp,expression,lexp,cons,resample,   &
  nchanmax,error,user_function)
  use gbl_constant
  use image_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_table_header_new
  use class_averaging
  use class_index
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Set the header of a new table
  !---------------------------------------------------------------------
  type(class_setup_t),   intent(in)    :: set               !
  type(header),          intent(out)   :: ref_head          ! Reference header for resampling and consistency checks
  type(gildas),          intent(inout) :: htable            ! Table header
  logical,               intent(in)    :: frequency         ! /FREQUENCY was used?
  character(len=*),      intent(in)    :: new_line          ! /FREQUENCY Line
  real(kind=8),          intent(in)    :: new_restf         ! /FREQUENCY Restf
  character(len=*),      intent(in)    :: weighting         ! Weighting type
  type(resampling),      intent(inout) :: faxis             ! New x-axis definition, if any
  type(average_range_t), intent(in)    :: range             ! Requested range
  logical,               intent(in)    :: math              ! Output a mathematical formula instead of the spectrum?
  integer(kind=4),       intent(in)    :: nexp              ! Number of /MATH expressions
  character(len=*),      intent(in)    :: expression(nexp)  ! String containing the mathematical expressions
  integer,               intent(in)    :: lexp(nexp)        ! Length of expression strings
  type(consistency_t),   intent(inout) :: cons              !
  logical,               intent(out)   :: resample          !
  integer(kind=4),       intent(out)   :: nchanmax          ! Largest spectrum size in index
  logical,               intent(inout) :: error             ! Error flag
  logical,               external      :: user_function     !
  ! Local
  character(len=*), parameter :: rname='TABLE'
  type(observation) :: tmp,auto
  type(average_t) :: aver
  !
  ! Check consistency BEFORE defining the X axis and reference header
  ! 3 possibilities:
  ! 1) User has given a custom X axis. Run through the index, check its
  !    consistency, and check if this X axis is ok (e.g. if it intersects
  !    any of the spectra)
  ! 2) User has given a range converted to an equivalent X axis. Same as
  !    above.
  ! 3) No user defined X axis. Run through the index, check its
  !    consistency, define the best X axis from the index, and
  !    check if resampling is really needed (if not, disable it).
  !
  aver%do%rname = rname
  aver%do%kind = kind_spec
  if (faxis%nchan.gt.0) then
    aver%do%resampling = resample_user
  else
    aver%do%resampling = resample_auto
  endif
  aver%do%alignment = align_auto
  aver%do%weight = weighting
  aver%do%composition = composite_inter
  aver%do%axis = faxis
  aver%do%range = range%unit.ne.''
  aver%do%rangeval = range
  aver%do%modfreq = frequency
  aver%do%restf = new_restf
  aver%do%modvelo = faxis%unit.eq.'V'
  aver%do%voff = faxis%val  ! Used if faxis%modvelo is .true.
  !
  call init_obs(tmp)
  ! Read 1st observation. Fill header structure from buffer. Data not needed
  ! at this stage.
  call rheader(set,tmp,cx%ind(1),user_function,error)
  if (error)  goto 10
  !
  if (aver%do%modfreq) then
    ! Line name and rest frequency may be redefined by the /FREQUENCY option
    tmp%head%spe%line = new_line
    call modify_frequency(tmp,aver%do%restf,error)
    if (error)  goto 10
  endif
  if (aver%do%modvelo) then
    ! Velocity is redefined by the /RESAMPLE option
    call modify_velocity(tmp,aver%do%voff,error)
    if (error)  goto 10
  endif
  !
  call consistency_tole(tmp%head,cons)   ! Set checking tolerances
  call consistency_print(set,tmp%head,cons)  ! Give user feedback on what will be done
  !
  call init_obs(auto)
  ! Compute auto%head and 'faxis' axis on return
  call sumlin_header(set,aver,tmp%head,cons,auto,error,user_function)
  tmp%head = auto%head
  call free_obs(auto)
  if (error)  goto 10
  !
  resample = aver%done%resample
  faxis%unit  = aver%done%axis%unit
  faxis%nchan = aver%done%axis%nchan
  ! Reuse the spectroscopic axis to fill the faxis% structure. Do not reuse
  ! the aver%done%axis% structure, since it uses units (e.g. offset frequencies
  ! instead of absolute) and references (e.g. FOFF != 0) which are not correct
  ! for do_resample.
  faxis%ref   = tmp%head%spe%rchan
  if (faxis%unit.eq.'V') then
    faxis%val = tmp%head%spe%voff
    faxis%inc = tmp%head%spe%vres
  elseif (faxis%unit.eq.'F') then
    faxis%val = tmp%head%spe%restf
    faxis%inc = tmp%head%spe%fres
  elseif (faxis%unit.eq.'I') then
    faxis%val = tmp%head%spe%image
    faxis%inc = -tmp%head%spe%fres
  endif
  nchanmax = aver%done%nchanmax
  !
  ! Build a reference header by modifying the first spectrum header
  ref_head = tmp%head
  if (frequency) ref_head%spe%line = new_line  ! *** JP I do not understand why this is needed
  call class_table_header_from_ref(ref_head,math,nexp,expression,lexp,htable,error)
  ! if (error)  continue
  !
10 continue
  call free_obs(tmp)
  !
end subroutine class_table_header_new
!
subroutine class_table_header_from_ref(ref_head,math,nexp,expression,lexp,  &
  htable,error)
  use phys_const
  use image_def
  use gbl_message
  use gkernel_interfaces
  use classcore_interfaces, except_this=>class_table_header_from_ref
  use class_index
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Define the Gildas table header from the Class observation header
  !---------------------------------------------------------------------
  type(header),     intent(in)    :: ref_head          ! Reference header
  logical,          intent(in)    :: math              ! Output a mathematical formula instead of the spectrum?
  integer(kind=4),  intent(in)    :: nexp              ! Number of /MATH expressions
  character(len=*), intent(in)    :: expression(nexp)  ! String containing the mathematical expression
  integer,          intent(in)    :: lexp(nexp)        ! Length of expression string
  type(gildas),     intent(inout) :: htable            ! Table header
  logical,          intent(inout) :: error             !
  ! Local
  character(len=*), parameter :: rname='TABLE'
  real(kind=4) :: xxx,yyy
  character(len=message_length) :: chain
  logical :: found
  real(kind=8) :: dummy(3)
  character(len=12) :: telname
  !
  ! Data unit: K for the time being. JP ***
  if (ref_head%cal%foeff.eq.ref_head%cal%beeff) then
    htable%char%unit = 'K (Ta*)'
  else
    htable%char%unit = 'K (Tmb)'
  endif
  !
  ! Definition of the table line axis (i.e. the frequency/velocity axis)
  htable%gil%ndim = 2
  !
  ! Definition of the table column axis (i.e. the observation axis)
  htable%gil%dim(2) = cx%next-1
  htable%gil%ref(2) = 1.d0
  htable%gil%val(2) = 1.d0
  htable%gil%inc(2) = 1.d0
  htable%gil%dim(3) = 1
  htable%gil%dim(4) = 1
  !
  ! Blanking values
  htable%gil%bval = class_bad
  htable%gil%eval = 0.0
  !
  ! Position information
  htable%char%name = ref_head%pos%sourc
  if (ref_head%pos%system.eq.type_eq) then
    htable%char%code(2) = 'RA-DEC'
    htable%gil%epoc = ref_head%pos%equinox
    htable%gil%ra   = ref_head%pos%lam
    htable%gil%dec  = ref_head%pos%bet
    xxx=0.0
    yyy=0.0
    call equ_to_gal(htable%gil%ra,htable%gil%dec,xxx,yyy,htable%gil%epoc,   &
                    htable%gil%lii,htable%gil%bii,xxx,yyy,error)
    if (error)  return
    htable%gil%a0 = htable%gil%ra
    htable%gil%d0 = htable%gil%dec
    htable%char%syst = 'EQUATORIAL'
  elseif (ref_head%pos%system.eq.type_ga) then
    htable%char%code(2) = 'LII-BII'
    htable%gil%epoc = ref_head%pos%equinox
    htable%gil%lii = ref_head%pos%lam
    htable%gil%bii = ref_head%pos%bet
    xxx=0.0
    yyy=0.0
    call gal_to_equ(htable%gil%lii,htable%gil%bii,xxx,yyy,   &
                    htable%gil%ra,htable%gil%dec,xxx,yyy,htable%gil%epoc,error)
    if (error)  return
    htable%gil%a0 = htable%gil%lii
    htable%gil%d0 = htable%gil%bii
    htable%char%syst = 'GALACTIC'
  elseif (ref_head%pos%system.eq.type_ic) then
    htable%char%code(2) = 'RA-DEC'
    htable%gil%epoc = equinox_null
    htable%gil%ra = ref_head%pos%lam
    htable%gil%dec = ref_head%pos%bet
    htable%gil%lii = 0.d0
    htable%gil%bii = 0.d0
    htable%gil%a0 = htable%gil%ra
    htable%gil%d0 = htable%gil%dec
    htable%char%syst = 'ICRS'
  else
    call class_message(seve%e,rname,'Unknown coordinate system')
    error = .true.
    return
  endif
  htable%gil%ptyp = ref_head%pos%proj
  htable%gil%pang = ref_head%pos%projang
  htable%gil%xaxi = 0
  htable%gil%yaxi = 0
  !
  ! Spectroscopic information
  call class_class2table_spectro(htable,math,nexp,expression(1),ref_head,error)
  if (error) return
  !
  ! Resolution section
  if (ref_head%presec(class_sec_res_id)) then
    ! The resolution section is available in the Class header
    htable%gil%majo = ref_head%res%major
    htable%gil%mino = ref_head%res%minor
    htable%gil%posa = ref_head%res%posang
    htable%gil%reso_words = 3
  else
    ! Guess from telescope name (antenna size)
    call my_get_beam(ref_head%gen%teles,htable%gil%freq,found,htable%gil%majo,error)
    if (error)  return
    if (.not.found) then
      ! No problem: deactivate the Resolution section, but warn user about this
      htable%gil%reso_words = 0
      call class_message(seve%w,rname,  &
        'Unknown telescope name '//trim(ref_head%gen%teles)//', resolution section undefined')
    else
      ! Everything is OK! Issue some feedback which used to be in my_get_beam:
      write(chain,'(A,F0.5,A,F0.3,A)')  &
        "freq: ",htable%gil%freq/1d3," GHz   sd_beam: ",htable%gil%majo/rad_per_sec," arcsec"
      call class_message(seve%i,rname,chain)
      !
      htable%gil%mino = htable%gil%majo
      htable%gil%posa = 0.0
      htable%gil%reso_words = 3
    endif
  endif
  !
  ! Add the telescope section, only if telescope is understood from
  ! obs%head%gen%teles (else not an error)
  call my_get_teles(rname,ref_head%gen%teles,.false.,telname,error)
  if (error)  return
  if (telname.ne.'' .and. telname.ne.'HERSCHEL') then
    ! Only telescopes known by gwcs
    call gdf_addteles(htable,'TELE',telname,dummy,error)
    if (error)  return
  endif
  !
  ! Define section length
  htable%gil%blan_words = 2
  htable%gil%extr_words = 0
  htable%gil%desc_words = 18
  htable%gil%posi_words = 12
  htable%gil%proj_words = 9
  htable%gil%spec_words = 12
  htable%gil%uvda_words = 0
  !
end subroutine class_table_header_from_ref
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine class_table_first_changes(set,fst,new_line,new_restf,  &
  resample_index,faxis,fft,error)
  use classcore_interfaces, except_this=>class_table_first_changes
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  !  Modify rest frequency and resample if needed
  !----------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set             !
  type(observation),   intent(inout) :: fst             ! First index spectrum
  character(len=*),    intent(inout) :: new_line        !
  real(kind=8),        intent(inout) :: new_restf       !
  logical,             intent(in)    :: resample_index  ! Enforce resampling of the whole index?
  logical,             intent(in)    :: fft             !
  type(resampling),    intent(in)    :: faxis           ! New x-axis definition
  logical,             intent(inout) :: error           ! Error flag
  ! Local
  character(len=12) :: old_line
  !
  old_line = fst%head%spe%line
  if (old_line.ne.new_line) then
     fst%head%spe%line = new_line
  endif
  call modify_frequency(fst,new_restf,error)
  if (error)  return
  if (resample_index) then
     call do_resample(set,fst,faxis,fft,error)
     if (error) return
  endif
  !
end subroutine class_table_first_changes
!
subroutine class_table_first_math(math,nexp,expression,lexp,error)
  use gkernel_interfaces
  use gbl_message
  !----------------------------------------------------------------------
  ! @ private
  !    Check validity of math expression on the R buffer
  !----------------------------------------------------------------------
  logical,          intent(in)    :: math              ! Output a mathematical formula instead of the spectrum?
  integer(kind=4),  intent(in)    :: nexp              ! Number of /MATH expressions
  character(len=*), intent(in)    :: expression(nexp)  ! String containing the mathematical expression
  integer(kind=4),  intent(in)    :: lexp(nexp)        ! Length of expression string
  logical,          intent(inout) :: error             ! Error flag
  ! Local
  character(len=*), parameter :: rname='TABLE'
  real(kind=4) :: arg
  integer(kind=4) :: iexp
  !
  ! Check validity of mathematical expressions
  do iexp=1,nexp
    call sic_math_real(expression(iexp),lexp(iexp),arg,error)
    if (error) then
      call class_message(seve%e,rname,  &
        'Invalid MATH expression '''//trim(expression(iexp))//'''')
      error = .true.
      return
    endif
  enddo
  !
end subroutine class_table_first_math
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine class_table_fill(set,htable,append,obs,ref_head,resample,fft,faxis,  &
  new_restf,math,nexp,expression,lexp,weighting,donasmyth,user_function,error)
  use gildas_def
  use image_def
  use gbl_message
  use gkernel_interfaces
  use gkernel_types
  use classcore_interfaces, except_this=>class_table_fill
  use class_types
  use class_index
  !----------------------------------------------------------------------
  ! @ private
  !  Fill the table from the index. To do this, it performs:
  !    * Consistency checks against reference header
  !    * Resampling on reference header
  !    * Mathematical computation
  !----------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set               !
  type(gildas),        intent(inout) :: htable            ! Table header
  logical,             intent(in)    :: append            ! Append to the table?
  type(observation),   intent(inout) :: obs               ! Working buffer (special trick for math mode)
  type(header),        intent(in)    :: ref_head          ! Reference header
  logical,             intent(in)    :: resample          ! Enforce resampling of the whole index?
  logical,             intent(in)    :: fft               ! Use FFT for resampling?
  type(resampling),    intent(in)    :: faxis             ! New x-axis definition
  real(kind=8),        intent(in)    :: new_restf         !
  logical,             intent(in)    :: math              ! Output a mathematical formula instead of the spectrum?
  integer(kind=4),     intent(in)    :: nexp              ! Number of /MATH expressions
  character(len=*),    intent(in)    :: expression(nexp)  ! String containing the mathematical expressions
  integer(kind=4),     intent(in)    :: lexp(nexp)        ! Length of expression strings
  character(len=*),    intent(in)    :: weighting         ! Weighting type
  logical,             intent(in)    :: donasmyth         ! Nasmyth offsets?
  logical,             intent(out)   :: error             ! Error flag
  logical,             external      :: user_function     !
  ! Local
  character(len=*), parameter :: rname='TABLE'
  character(len=message_length) :: mess
  integer(kind=4) :: oline,iline,ier,ncol,iexp
  integer(kind=entry_length) :: ient,nent
  integer(kind=index_length) :: nline
  real(kind=4) :: sarg(nexp),weight,lamof,betof
  real(kind=4), pointer :: dtable(:,:)
  type(time_t) :: time
  logical :: readsec(-mx_sec:0)  ! Which section are to be read
  !
  ncol = htable%gil%dim(1)
  nent = cx%next-1
  !
  ! Allocate needed memory and initialize it
  allocate(dtable(ncol,nent),stat=ier)
  dtable(:,:) = htable%gil%bval
  iline = 1  ! Use an 'iline' (and not 'ient') because we cycle to next
             ! obs in case of error.
  !
  if (math) then
    ! Read all header as the mathematical expressions may be sioux...
    readsec(:) = .true.
  else
    ! Read only interesting sections when making a spectra cube
    readsec(:) = .false.  ! Read no section except...
    readsec(class_sec_gen_id) = .true.            ! General
    readsec(class_sec_pos_id) = .true.            ! Position
    readsec(class_sec_spe_id) = .true.            ! Spectro
    readsec(class_sec_bas_id) = weighting.eq.'S'  ! Base (only if Sigma weighting)
  endif
  !
  ! Loop on current index entry
  call gtime_init(time,nent,error)
  if (error)  return
  do ient=1,nent
     call gtime_current(time)
     !
     ! Exit when asked by user
     error = .false.
     if (sic_ctrlc()) exit
     !
     ! Read into buffer header section from file
     call rheader(set,obs,cx%ind(ient),user_function,error,readsec)
     if (error) cycle
     ! Get data
     call rdata(set,obs,obs%head%spe%nchan,obs%data1,error)
     if (error) cycle
     ! Modify frequency when needed
     call modify_frequency(obs,new_restf,error)
     if (error)  exit
     ! Resample only when needed + do_resample is clever enough to choose between
     ! resampling and extracting only a subset
     if (resample) then
        call do_resample(set,obs,faxis,fft,error)
        if (error) exit
     endif
     ! Compute weight
     if (weighting.eq.'T') then
        call obs_weight_time(rname,obs,weight,error)
        if (error)  exit
        ! Weight unit is s.MHz/K^2 (to match UVT format)
     elseif (weighting.eq.'S') then
        call obs_weight_sigma(rname,obs,weight,error)
        if (error)  exit
        ! Weight is homogeneous to s.MHz/K^2 above
     elseif (weighting.eq.'E') then  ! Equal
        weight = 1.
     else
        call class_message(seve%e,rname,'Weight '//weighting//' is not implemented')
        error = .true.
        exit
     endif
     ! Compute offsets
     if (donasmyth) then
       call class_table_nasmyth_offsets(obs,lamof,betof,error)
       if (error)  return
     else
       lamof = obs%head%pos%lamof
       betof = obs%head%pos%betof
     endif
     ! Fill in the table
     if (math) then
        ! Compute mathematical expressions
        do iexp=1,nexp
          call sic_math_real(expression(iexp),lexp(iexp),sarg(iexp),error)
          if (error) then
            write (mess,'(A,A,A,I0)')  &
              'Invalid mathematical expression ''',trim(expression(iexp)),  &
              ''' at spectrum #',obs%head%gen%num
            call class_message(seve%e,rname,mess)
            cycle
          endif
        enddo
        ! Everything is OK. Now we can fill one line of the table
        call class_table_line(nexp,sarg,dtable(:,iline), &
          lamof,betof,weight,obs%head%spe%bad,htable%gil%bval)
     else
        ! Everything is OK. Now we can fill one line of the table
        call class_table_line(ref_head%spe%nchan,obs%spectre,dtable(:,iline),   &
          lamof,betof,weight,obs%head%spe%bad,htable%gil%bval)
     endif
     ! Success: we can try to fill next line
     iline = iline+1
  enddo ! ient
  if (error) then
     write(mess,'(a,i0,a,i0)')  &
          'Table failed at entry #',ient,', observation #',obs%head%gen%num
     call class_message(seve%e,rname,mess)
     goto 100
  endif
  iline = iline-1
  !
  ! Update header information
  if (append) then
    oline = htable%gil%dim(2)
    write(mess,'(a,i0,a,i0,a)')  &
      'Old table size ',oline,', adding ',iline,' spectra'
    call class_message(seve%i,rname,mess)
    !
    ! Extend the table header
    nline = oline+iline
    call gdf_extend_image(htable,nline,error)
    if (error)  goto 100
    !
    ! We will only write this portion of the extended table:
    htable%blc(1) = 1
    htable%trc(1) = ncol
    htable%blc(2) = oline+1
    htable%trc(2) = nline
    !
  else
    call gdf_create_image(htable,error)
    if (gildas_error(htable,rname,error)) goto 100
  endif
  !
  call class_table_size(htable,error)
  if (error)  goto 100
  !
  ! Write table now
  call gdf_write_data(htable,dtable,error)
  if (gildas_error(htable,rname,error))  continue
  !
  call gdf_close_image(htable,error)
  if (gildas_error(htable,rname,error))  continue
  !
  ! Cleaning
100 continue
  if (error)  &
     call class_message(seve%e,rname,'Cannot create output table')
  deallocate(dtable)
  !
end subroutine class_table_fill
!
subroutine class_table_nasmyth_offsets(obs,lamof,betof,error)
  use gbl_message
  use classcore_interfaces, except_this=>class_table_nasmyth_offsets
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Compute the observation offsets in Nasmyth plane coordinates
  !---------------------------------------------------------------------
  type(observation), intent(in)    :: obs
  real(kind=4),      intent(out)   :: lamof,betof
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='TABLE /NASMYTH'
  character(len=message_length) :: mess
  real(kind=8) :: chi0
  !
  if (obs%head%gen%parang.eq.parang_null) then
    write(mess,'(A,I0,A)')  'Observation #',obs%head%gen%num,  &
      ' has no parallactic angle defined'
    call class_message(seve%e,rname,mess)
    mess = 'You should consider applying MODIFY PARALLACTIC_ANGLE on '// &
           'all the spectra and save them in a new file'
    call class_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  ! Offsets rotated to Nasmyth
  chi0 = obs%head%gen%el - obs%head%gen%parang
  ! print *,"El=",obs%head%gen%el,", pa=",parangle,", chi0=",chi0
  lamof = -(obs%head%pos%lamof*cos(chi0)+obs%head%pos%betof*sin(chi0))
  betof =  (obs%head%pos%lamof*sin(chi0)-obs%head%pos%betof*cos(chi0))
end subroutine class_table_nasmyth_offsets
!
subroutine class_table_size(htable,error)
  use gbl_message
  use image_def
  !---------------------------------------------------------------------
  ! @ private
  !  Should be removed and replaced by a simple assignment when
  ! loca%size will have an automatic length (kind=axis_length or
  ! index_length)
  !---------------------------------------------------------------------
  type(gildas), intent(in)    :: htable  ! Table header
  logical,      intent(inout) :: error   ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='TABLE'
  character(len=message_length) :: mess
  integer(kind=8) :: size8,dim8(2)
  integer(kind=4), parameter :: size4max=2147483647  ! 2**31-1
  !
  dim8(1:2) = htable%gil%dim(1:2)
  size8 = dim8(1)*dim8(2)
  !
  if (size_length.eq.4 .and. size8.gt.size4max) then
    write(mess,'(2(A,I0))')  &
      'Table size is larger than 2^31-1 elements: ', &
      htable%gil%dim(1),' x ',htable%gil%dim(2)
    call class_message(seve%e,rname,mess)
    call class_message(seve%e,rname,  &
      'Such big tables are not supported on your system')
    error = .true.
    return
  else
    write(mess,'(3(A,I0))')  &
      'Table size is ',size8,' elements : ', &
      htable%gil%dim(1),' x ',htable%gil%dim(2)
    call class_message(seve%d,rname,mess)
  endif
  !
end subroutine class_table_size
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine class_table_line(nchan,intensities,line,offx,offy,weight,bad,blank)
  !----------------------------------------------------------------------
  ! @ private
  !  Fill a line of one table line taking blanking into account
  !----------------------------------------------------------------------
  integer(kind=4), intent(in)    :: nchan              ! Number of spectrum channels
  real(kind=4),    intent(in)    :: intensities(nchan) ! Spectrum intensities
  real(kind=4),    intent(inout) :: line(-2:nchan)     ! Table line to be filled
  real(kind=4),    intent(in)    :: offx,offy          ! Spectrum x and y offsets
  real(kind=4),    intent(in)    :: weight             ! Spectrum weight
  real(kind=4),    intent(in)    :: bad,blank          ! Spectrum and table blanking values
  ! Local
  integer(kind=4) :: ichan
  !
  line(-2) = offx
  line(-1) = offy
  line(0) = weight
  do ichan=1,nchan
     if (intensities(ichan).eq.bad) then
        line(ichan) = blank
     else
        line(ichan) = intensities(ichan)
     endif
  enddo
  !
end subroutine class_table_line
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
