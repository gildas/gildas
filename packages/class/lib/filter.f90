subroutine class_filter(line,r,error)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_filter
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: line   !
  type(observation), intent(inout) :: r      !
  logical,           intent(inout) :: error  !
  ! Local
  logical :: store
  !
  store = sic_present(1,0)
  call class_filter_do(r,store,error)
  if (error)  return
  !
end subroutine class_filter
!
subroutine class_filter_do(obs,store,error)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_filter_do
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! See http://en.wikipedia.org/wiki/Median_absolute_deviation
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs    !
  logical,           intent(in)    :: store  ! Store result in the observation header
  logical,           intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='FILTER'
  real(kind=4) :: median,sigma
  real(kind=4), allocatable :: diff(:)
  integer(kind=4) :: ier
  character(len=message_length) :: mess
  integer(kind=size_length) :: nchan
  ! For normally distributed data K is taken to be 1/phi^-1(3/4) =~ 1.4826,
  ! where phi^-1 is the inverse of the cumulative distribution function for
  ! the standard normal distribution, i.e., the quantile function.
  real(kind=4), parameter :: kfactor=1.4826
  !
  ! Compute the Median
  nchan = obs%cnchan
  call gr4_median(obs%data1,nchan,obs%cbad,0.,median,error)
  if (error)  return
  !
  allocate(diff(nchan),stat=ier)
  if (failed_allocate(rname,'DIFF array',ier,error))  return
  !
  ! Compute the Median Absolute Deviation from Median
  where (obs%data1(1:nchan).eq.obs%cbad)
    diff(:) = obs%cbad
  elsewhere
    diff(:) = abs(obs%data1(1:nchan)-median)
  end where
  !
  call gr4_median(diff,nchan,obs%cbad,0.,median,error)
  if (error)  goto 10
  !
  ! Compute the Standard Deviation from MAD
  sigma = kfactor*median
  !
  write (mess,'(A,1PG10.3,A)') 'Sigma is ',sigma,' K'
  call class_message(seve%r,rname,mess)
  !
  if (store) then
    obs%head%bas%sigfi = sigma
  endif
  !
10 continue
  if (allocated(diff)) deallocate(diff)
  !
end subroutine class_filter_do
