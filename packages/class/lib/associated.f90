!-----------------------------------------------------------------------
! Support for file Associated Arrays section
!-----------------------------------------------------------------------
module class_associated
  integer(kind=4), parameter :: nassociated=11
  character(len=12), parameter :: assoc_reserved(nassociated) =  (/ &
    'X           ',  &
    'Y           ',  &
    'W           ',  &
    'C           ',  &  ! Channel
    'V           ',  &  ! Velocity
    'F           ',  &  ! Frequency
    'I           ',  &  ! Image
    'T           ',  &  ! Time
    'A           ',  &  ! Angle
    'BLANKED     ',  &  ! Blanked channels
    'LINE        '  /)  ! Line windows
end module class_associated
!
function class_assoc_exists_noarg(obs,name)
  use classcore_interfaces, except_this=>class_assoc_exists_noarg
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic class_assoc_exists
  !  Search an Associated Array by its name (name is assumed already
  ! upcased).
  ! ---
  !  This version returns nothing
  !---------------------------------------------------------------------
  logical :: class_assoc_exists_noarg  ! Function value on return
  type(observation), intent(in)  :: obs
  character(len=*),  intent(in)  :: name
  ! Local
  integer(kind=4) :: iarray
  !
  class_assoc_exists_noarg = class_assoc_exists_bynum(obs,name,iarray)
  !
end function class_assoc_exists_noarg
!
function class_assoc_exists_bynum(obs,name,iarray)
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic class_assoc_exists
  !  Search an Associated Array by its name (name is assumed already
  ! upcased). Return index 0 if no such array exists.
  ! ---
  !  This version returns the array index
  !---------------------------------------------------------------------
  logical :: class_assoc_exists_bynum  ! Function value on return
  type(observation), intent(in)  :: obs
  character(len=*),  intent(in)  :: name
  integer(kind=4),   intent(out) :: iarray
  !
  class_assoc_exists_bynum = .true.
  if (obs%head%presec(class_sec_assoc_id)) then
    do iarray=1,obs%assoc%n
      if (obs%assoc%array(iarray)%name.eq.name)  return
    enddo
  endif
  !
  ! We have not returned from the loop: no such array
  class_assoc_exists_bynum = .false.
  iarray = 0
  !
end function class_assoc_exists_bynum
!
function class_assoc_exists_array(obs,name,array)
  use classcore_interfaces, except_this=>class_assoc_exists_array
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic class_assoc_exists
  !  Search an Associated Array by its name (name is assumed already
  ! upcased).
  ! ---
  !  This version returns a sub Associated Array
  !---------------------------------------------------------------------
  logical :: class_assoc_exists_array  ! Function value on return
  type(observation),       intent(in)  :: obs
  character(len=*),        intent(in)  :: name
  type(class_assoc_sub_t), intent(out) :: array
  ! Local
  integer(kind=4) :: iarray
  !
  class_assoc_exists_array = class_assoc_exists_bynum(obs,name,iarray)
  !
  if (class_assoc_exists_array) then
    ! Just a reassocation: the returned object does not own the allocation.
    ! Same as reassociate_assoc_sub except that we never steal the allocation.
    ! Do not use directly 'reassociate_assoc_sub' because 'obs' would have to
    ! be inout here and for all callers...
    !
    array%fmt  = obs%assoc%array(iarray)%fmt
    array%dim1 = obs%assoc%array(iarray)%dim1
    array%dim2 = obs%assoc%array(iarray)%dim2
    array%name = obs%assoc%array(iarray)%name
    array%unit = obs%assoc%array(iarray)%unit
    !
    array%badr4 = obs%assoc%array(iarray)%badr4
    array%badi4 = obs%assoc%array(iarray)%badi4
    array%r4 => obs%assoc%array(iarray)%r4
    array%i4 => obs%assoc%array(iarray)%i4
    !
    array%status = code_pointer_associated
    !
  else
    ! For safety
    call nullify_assoc_sub(array)
  endif
  !
end function class_assoc_exists_array
!
function class_assoc_exists_i41d(obs,name,ptr)
  use gbl_message
  use classcore_interfaces, except_this=>class_assoc_exists_i41d
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic class_assoc_exists
  !  Search an Associated Array by its name (name is assumed already
  ! upcased).
  ! ---
  !  This version returns a pointer to the 1D I*4 data
  !---------------------------------------------------------------------
  logical :: class_assoc_exists_i41d  ! Function value on return
  type(observation), intent(in) :: obs
  character(len=*),  intent(in) :: name
  integer(kind=4),   pointer    :: ptr(:)
  ! Local
  character(len=*), parameter :: rname='ASSOC>EXISTS'
  integer(kind=4) :: iarray
  logical :: error
  !
  ptr => null()  ! For safety
  class_assoc_exists_i41d = class_assoc_exists_bynum(obs,name,iarray)
  if (.not.class_assoc_exists_i41d)  return
  !
  ! Sanity checks
  if (obs%assoc%array(iarray)%fmt.ne.fmt_i4 .and.  &
      obs%assoc%array(iarray)%fmt.ne.fmt_by .and.  &
      obs%assoc%array(iarray)%fmt.ne.fmt_b2) then
    call class_message(seve%e,rname,  &
      'Programming error: can not map non I*4 array onto a I*4 pointer')
    error = .true.
    return
  endif
  if (obs%assoc%array(iarray)%dim2.ne.0) then
    call class_message(seve%e,rname,  &
      'Programming error: can not map a 2D array onto a 1D pointer')
    error = .true.
    return
  endif
  !
  ptr => obs%assoc%array(iarray)%i4(:,1)
  !
end function class_assoc_exists_i41d
!
function class_assoc_exists_r41d(obs,name,ptr)
  use gbl_message
  use classcore_interfaces, except_this=>class_assoc_exists_r41d
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic class_assoc_exists
  !  Search an Associated Array by its name (name is assumed already
  ! upcased).
  ! ---
  !  This version returns a pointer to the 1D R*4 data
  !---------------------------------------------------------------------
  logical :: class_assoc_exists_r41d  ! Function value on return
  type(observation), intent(in) :: obs
  character(len=*),  intent(in) :: name
  real(kind=4),      pointer    :: ptr(:)
  ! Local
  character(len=*), parameter :: rname='ASSOC>EXISTS'
  integer(kind=4) :: iarray
  logical :: error
  !
  ptr => null()  ! For safety
  class_assoc_exists_r41d = class_assoc_exists_bynum(obs,name,iarray)
  if (.not.class_assoc_exists_r41d)  return
  !
  ! Sanity checks
  if (obs%assoc%array(iarray)%fmt.ne.fmt_r4) then
    call class_message(seve%e,rname,  &
      'Programming error: can not map non R*4 array onto a R*4 pointer')
    error = .true.
    return
  endif
  if (obs%assoc%array(iarray)%dim2.ne.0) then
    call class_message(seve%e,rname,  &
      'Programming error: can not map a 2D array onto a 1D pointer')
    error = .true.
    return
  endif
  !
  ptr => obs%assoc%array(iarray)%r4(:,1)
  !
end function class_assoc_exists_r41d
!
function class_assoc_isreserved(name)
  use class_associated
  !---------------------------------------------------------------------
  ! @ private
  !  Return .true. if the name is a reserved Associated Array
  !---------------------------------------------------------------------
  logical :: class_assoc_isreserved
  character(len=*), intent(in) :: name
  ! Local
  integer(kind=4) :: ireserv
  !
  class_assoc_isreserved = .false.
  do ireserv=1,nassociated
    ! NB: upcase done by caller
    if (name.eq.assoc_reserved(ireserv)) then
      class_assoc_isreserved = .true.
      return
    endif
  enddo
  !
end function class_assoc_isreserved
!
subroutine class_assoc_add_reservedi41d(obs,name,ptr,error)
  use gbl_format
  use gbl_message
  use classcore_interfaces, except_this=>class_assoc_add_reservedi41d
  use class_types
  !---------------------------------------------------------------------
  ! @ public-generic class_assoc_add
  !   Add a reserved Associated Array to the Associated Section of the
  ! input observation. For reserved Arrays, the unit, format, and 2nd
  ! dimension are implicit and can not be customized by the caller.
  ! ---
  !  This version for:
  !   - reserved keywords
  !   - associate 1D I*4 pointer to the array in return
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs     ! Observation
  character(len=*),  intent(in)    :: name    ! The array name
  integer(kind=4),   pointer       :: ptr(:)  ! Pointer on array
  logical,           intent(inout) :: error   ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='ASSOC>ADD>RESERVED'
  integer(kind=4) :: numarray
  !
  ptr => null()  ! Initialization, for safety
  !
  call class_assoc_add_reservednum(obs,name,numarray,error)
  if (error)  return
  !
  ! Sanity checks
  if (obs%assoc%array(numarray)%fmt.ne.fmt_i4 .and.  &
      obs%assoc%array(numarray)%fmt.ne.fmt_by .and.  &
      obs%assoc%array(numarray)%fmt.ne.fmt_b2) then
    call class_message(seve%e,rname,  &
      'Programming error: can not map non I*4 array onto a I*4 pointer')
    error = .true.
    return
  endif
  if (obs%assoc%array(numarray)%dim2.ne.0) then
    call class_message(seve%e,rname,  &
      'Programming error: can not map a 2D array onto a 1D pointer')
    error = .true.
    return
  endif
  !
  ptr => obs%assoc%array(numarray)%i4(:,1)
  !
end subroutine class_assoc_add_reservedi41d
!
subroutine class_assoc_add_reservedr41d(obs,name,ptr,error)
  use gbl_format
  use gbl_message
  use classcore_interfaces, except_this=>class_assoc_add_reservedr41d
  use class_types
  !---------------------------------------------------------------------
  ! @ public-generic class_assoc_add
  !   Add a reserved Associated Array to the Associated Section of the
  ! input observation. For reserved Arrays, the unit, format, and 2nd
  ! dimension are implicit and can not be customized by the caller.
  ! ---
  !  This version for:
  !   - reserved keywords
  !   - associate 1D R*4 pointer to the array in return
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs     ! Observation
  character(len=*),  intent(in)    :: name    ! The array name
  real(kind=4),      pointer       :: ptr(:)  ! Pointer on array
  logical,           intent(inout) :: error   ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='ASSOC>ADD>RESERVED'
  integer(kind=4) :: numarray
  !
  ptr => null()  ! Initialization, for safety
  !
  call class_assoc_add_reservednum(obs,name,numarray,error)
  if (error)  return
  !
  ! Sanity checks
  if (obs%assoc%array(numarray)%fmt.ne.fmt_r4) then
    call class_message(seve%e,rname,  &
      'Programming error: can not map non R*4 array onto a R*4 pointer')
    error = .true.
    return
  endif
  if (obs%assoc%array(numarray)%dim2.ne.0) then
    call class_message(seve%e,rname,  &
      'Programming error: can not map a 2D array onto a 1D pointer')
    error = .true.
    return
  endif
  !
  ptr => obs%assoc%array(numarray)%r4(:,1)
  !
end subroutine class_assoc_add_reservedr41d
!
subroutine class_assoc_add_reservednum(obs,name,numarray,error)
  use gbl_format
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_assoc_add_reservednum
  use class_types
  !---------------------------------------------------------------------
  ! @ public-generic class_assoc_add
  !   Add a reserved Associated Array to the Associated Section of the
  ! input observation. For reserved Arrays, the unit, format, and 2nd
  ! dimension are implicit and can not be customized by the caller.
  ! ---
  !  This version for:
  !   - reserved keywords
  !   - get the array identifier in return
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs       ! Observation
  character(len=*),  intent(in)    :: name      ! The array name
  integer(kind=4),   intent(out)   :: numarray  ! Array number
  logical,           intent(inout) :: error     ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='ASSOC>ADD>RESERVED'
  character(len=12) :: uname,unit
  integer(kind=4) :: form,dim2,badi4
  real(kind=4) :: badr4
  !
  uname = name
  call sic_upper(uname)
  !
  if (.not.class_assoc_isreserved(uname)) then
    call class_message(seve%e,rname,  &
      'Programming error: '''//trim(name)//''' is not a reserved keyword')
    error = .true.
    return
  endif
  !
  select case (uname)
  case ('Y')
    unit = ''
    form = fmt_r4
    badr4 = obs_bad(obs%head)
    dim2 = 0  ! This might be more flexible in the future
    call class_assoc_add_sub1(obs,name,unit,form,dim2,badr4,numarray,error)
    !
  case ('W')
    unit = ''
    form = fmt_r4
    badr4 = -1000.0  ! ZZZ Or the same as RY?
    dim2 = 0
    call class_assoc_add_sub1(obs,name,unit,form,dim2,badr4,numarray,error)
    !
  case ('BLANKED')
    unit = ''
    form = fmt_b2
    badi4 = -1
    dim2 = 0
    call class_assoc_add_sub1(obs,name,unit,form,dim2,badi4,numarray,error)
    !
  case ('LINE')
    unit = ''
    form = fmt_b2
    badi4 = -1
    dim2 = 0
    call class_assoc_add_sub1(obs,name,unit,form,dim2,badi4,numarray,error)
    !
  case default
    call class_message(seve%e,rname,'Reserved array '''//trim(name)//''' is not implemented')
    error = .true.
    !
  end select
  if (error)  return
  !
end subroutine class_assoc_add_reservednum
!
subroutine class_assoc_add_free_i41d(obs,name,unit,form,dim2,badi4,ptr,error)
  use gbl_format
  use gbl_message
  use classcore_interfaces, except_this=>class_assoc_add_free_i41d
  use class_types
  !---------------------------------------------------------------------
  ! @ public-generic class_assoc_add
  !   Add an array to the Associated Section of the input observation
  ! The array name must be a 'free' (non-reserved) Associated Array
  ! ---
  ! This version for:
  !  - non-reserved arrays
  !  - I*4 bad value
  !  - associate 1D I*4 pointer to the array in return
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs       ! Observation
  character(len=*),  intent(in)    :: name      ! The array name
  character(len=*),  intent(in)    :: unit      ! Unit for values
  integer(kind=4),   intent(in)    :: form      ! Format on disk
  integer(kind=4),   intent(in)    :: dim2      ! 2nd dimension, 0 for 1D array
  integer(kind=4),   intent(in)    :: badi4     ! Bad value
  integer(kind=4),   pointer       :: ptr(:)    ! Pointer on array
  logical,           intent(inout) :: error     ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='ASSOC>ADD>FREE'
  integer(kind=4) :: numarray
  !
  ptr => null()  ! Initialization, for safety
  !
  call class_assoc_add_free_i4num(obs,name,unit,form,dim2,badi4,numarray,error)
  if (error)  return
  !
  ! Sanity checks
  if (obs%assoc%array(numarray)%fmt.ne.fmt_i4 .and.  &
      obs%assoc%array(numarray)%fmt.ne.fmt_by .and.  &
      obs%assoc%array(numarray)%fmt.ne.fmt_b2) then
    call class_message(seve%e,rname,  &
      'Programming error: can not map non I*4 array onto a I*4 pointer')
    error = .true.
    return
  endif
  if (obs%assoc%array(numarray)%dim2.ne.0) then
    call class_message(seve%e,rname,  &
      'Programming error: can not map a 2D array onto a 1D pointer')
    error = .true.
    return
  endif
  !
  ptr => obs%assoc%array(numarray)%i4(:,1)
  !
end subroutine class_assoc_add_free_i41d
!
subroutine class_assoc_add_free_i4num(obs,name,unit,form,dim2,badi4,numarray,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_assoc_add_free_i4num
  use class_types
  !---------------------------------------------------------------------
  ! @ public-generic class_assoc_add
  !   Add an array to the Associated Section of the input observation
  ! The array name must be a 'free' (non-reserved) Associated Array
  ! ---
  ! This version for:
  !  - non-reserved arrays
  !  - I*4 bad value
  !  - get the array identifier in return
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs       ! Observation
  character(len=*),  intent(in)    :: name      ! The array name
  character(len=*),  intent(in)    :: unit      ! Unit for values
  integer(kind=4),   intent(in)    :: form      ! Format on disk
  integer(kind=4),   intent(in)    :: dim2      ! 2nd dimension, 0 for 1D array
  integer(kind=4),   intent(in)    :: badi4     ! Bad value
  integer(kind=4),   intent(out)   :: numarray  ! Array number
  logical,           intent(inout) :: error     ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='ASSOC>ADD>FREE'
  character(len=12) :: uname
  !
  uname = name
  call sic_upper(uname)
  !
  if (class_assoc_isreserved(uname)) then
    call class_message(seve%e,rname,  &
      'Programming error: '''//trim(name)//''' is a reserved keyword')
    error = .true.
    return
  endif
  !
  call class_assoc_add_sub1(obs,uname,unit,form,dim2,badi4,numarray,error)
  if (error)  return
  !
end subroutine class_assoc_add_free_i4num
!
subroutine class_assoc_add_free_r41d(obs,name,unit,form,dim2,badr4,ptr,error)
  use gbl_format
  use gbl_message
  use classcore_interfaces, except_this=>class_assoc_add_free_r41d
  use class_types
  !---------------------------------------------------------------------
  ! @ public-generic class_assoc_add
  !   Add an array to the Associated Section of the input observation
  ! The array name must be a 'free' (non-reserved) Associated Array
  ! ---
  ! This version for:
  !  - non-reserved arrays
  !  - R*4 bad value
  !  - associate 1D R*4 pointer to the array in return
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs       ! Observation
  character(len=*),  intent(in)    :: name      ! The array name
  character(len=*),  intent(in)    :: unit      ! Unit for values
  integer(kind=4),   intent(in)    :: form      ! Format on disk
  integer(kind=4),   intent(in)    :: dim2      ! 2nd dimension, 0 for 1D array
  real(kind=4),      intent(in)    :: badr4     ! Bad value
  real(kind=4),      pointer       :: ptr(:)    ! Pointer on array
  logical,           intent(inout) :: error     ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='ASSOC>ADD>FREE'
  integer(kind=4) :: numarray
  !
  ptr => null()  ! Initialization, for safety
  !
  call class_assoc_add_free_r4num(obs,name,unit,form,dim2,badr4,numarray,error)
  if (error)  return
  !
  ! Sanity checks
  if (obs%assoc%array(numarray)%fmt.ne.fmt_r4) then
    call class_message(seve%e,rname,  &
      'Programming error: can not map non R*4 array onto a R*4 pointer')
    error = .true.
    return
  endif
  if (obs%assoc%array(numarray)%dim2.ne.0) then
    call class_message(seve%e,rname,  &
      'Programming error: can not map a 2D array onto a 1D pointer')
    error = .true.
    return
  endif
  !
  ptr => obs%assoc%array(numarray)%r4(:,1)
  !
end subroutine class_assoc_add_free_r41d
!
subroutine class_assoc_add_free_r4num(obs,name,unit,form,dim2,badr4,numarray,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_assoc_add_free_r4num
  use class_types
  !---------------------------------------------------------------------
  ! @ public-generic class_assoc_add
  !   Add an array to the Associated Section of the input observation
  ! The array name must be a 'free' (non-reserved) Associated Array
  ! ---
  ! This version for:
  !  - non-reserved arrays
  !  - R*4 bad value
  !  - get the array identifier in return
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs       ! Observation
  character(len=*),  intent(in)    :: name      ! The array name
  character(len=*),  intent(in)    :: unit      ! Unit for values
  integer(kind=4),   intent(in)    :: form      ! Format on disk
  integer(kind=4),   intent(in)    :: dim2      ! 2nd dimension, 0 for 1D array
  real(kind=4),      intent(in)    :: badr4     ! Bad value
  integer(kind=4),   intent(out)   :: numarray  ! Array number
  logical,           intent(inout) :: error     ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='ASSOC>ADD>FREE'
  character(len=12) :: uname
  !
  uname = name
  call sic_upper(uname)
  !
  if (class_assoc_isreserved(uname)) then
    call class_message(seve%e,rname,  &
      'Programming error: '''//trim(name)//''' is a reserved keyword')
    error = .true.
    return
  endif
  !
  call class_assoc_add_sub1(obs,uname,unit,form,dim2,badr4,numarray,error)
  if (error)  return
  !
end subroutine class_assoc_add_free_r4num
!
subroutine class_assoc_add_sub1_i4(obs,name,unit,form,dim2,badi4,numarray,error)
  use gbl_message
  use classcore_interfaces, except_this=>class_assoc_add_sub1_i4
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic class_assoc_add_sub1
  !  DO NOT CALL DIRECTLY: use generic entry point 'class_assoc_add'
  ! instead.
  ! ---
  !  Add an array to the Associated Section, with an integer bad value.
  ! Check that the array format supports such an integer bad value.
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs       ! Observation
  character(len=*),  intent(in)    :: name      ! The array name
  character(len=*),  intent(in)    :: unit      ! Unit for values
  integer(kind=4),   intent(in)    :: form      ! Format on disk
  integer(kind=4),   intent(in)    :: dim2      ! 2nd dimension, 0 for 1D array
  integer(kind=4),   intent(in)    :: badi4     ! Bad value
  integer(kind=4),   intent(out)   :: numarray  ! Array number
  logical,           intent(inout) :: error     ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='ASSOC>ADD'
  integer(kind=4), parameter :: mini4=-2147483648
  integer(kind=4), parameter :: maxi4=2147483647
  integer(kind=4), parameter :: minby=-128
  integer(kind=4), parameter :: maxby=127
  integer(kind=4), parameter :: minb2=-2
  integer(kind=4), parameter :: maxb2=1
  !
  ! 3 sanity checks:
  !  1 - supported formats
  !  2 - match format with type of bad value
  !  3 - check if bad value is in range of the format.
  select case (form)
  case (fmt_i4)
    ! Can not check I*4 overflow with I*4 values...
    continue
  case (fmt_by)
    if (badi4.lt.minby .or. badi4.gt.maxby) then
      call class_message(seve%e,rname,  &
        'Bad value is beyond the valid range of values for I*1')
      error = .true.
      return
    endif
  case (fmt_b2)
    if (badi4.lt.minb2 .or. badi4.gt.maxb2) then
      call class_message(seve%e,rname,  &
        'Bad value is beyond the valid range of values for 2-bits integer')
      error = .true.
      return
    endif
  case default
    call class_message(seve%e,rname,  &
      'Associated Array format does not support an integer bad value')
    error = .true.
    return
  end select
  !
  call class_assoc_add_sub2(obs,name,unit,form,dim2,numarray,error)
  if (error)  return
  !
  obs%assoc%array(numarray)%badi4 = badi4
  !
end subroutine class_assoc_add_sub1_i4
!
subroutine class_assoc_add_sub1_r4(obs,name,unit,form,dim2,badr4,numarray,error)
  use gbl_message
  use classcore_interfaces, except_this=>class_assoc_add_sub1_r4
  use class_types
  !---------------------------------------------------------------------
  ! @ private-generic class_assoc_add_sub1
  !  DO NOT CALL DIRECTLY: use generic entry point 'class_assoc_add'
  ! instead.
  ! ---
  !  Add an array to the Associated Section, with a real bad value.
  ! Check that the array format supports such an integer bad value.
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs       ! Observation
  character(len=*),  intent(in)    :: name      ! The array name
  character(len=*),  intent(in)    :: unit      ! Unit for values
  integer(kind=4),   intent(in)    :: form      ! Format on disk
  integer(kind=4),   intent(in)    :: dim2      ! 2nd dimension, 0 for 1D array
  real(kind=4),      intent(in)    :: badr4     ! Bad value
  integer(kind=4),   intent(out)   :: numarray  ! Array number
  logical,           intent(inout) :: error     ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='ASSOC>ADD'
  !
  ! 2 sanity checks:
  !  1 - supported formats
  !  2 - match format with type of bad value
  select case (form)
  case (fmt_r4)
    continue
  case default
    call class_message(seve%e,rname,  &
      'Associated Array format does not support a real bad value')
    error = .true.
    return
  end select
  !
  call class_assoc_add_sub2(obs,name,unit,form,dim2,numarray,error)
  if (error)  return
  !
  obs%assoc%array(numarray)%badr4 = badr4
  !
end subroutine class_assoc_add_sub1_r4
!
subroutine class_assoc_add_sub2(obs,name,unit,form,dim2,numarray,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_assoc_add_sub2
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  DO NOT CALL DIRECTLY: use generic entry point 'class_assoc_add'
  ! instead.
  ! ---
  !   Add an array to the Associated Section of the input observation
  ! The array is allocated but not initialized for efficiency purpose
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs       ! Observation
  character(len=*),  intent(in)    :: name      ! The array name
  character(len=*),  intent(in)    :: unit      ! Unit for values
  integer(kind=4),   intent(in)    :: form      ! Format on disk
  integer(kind=4),   intent(in)    :: dim2      ! 2nd dimension, 0 for 1D array
  integer(kind=4),   intent(out)   :: numarray  ! Array number
  logical,           intent(inout) :: error     ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='ASSOC>ADD'
  !
  ! NB: upcase done by caller
  if (name.eq.'') then
    call class_message(seve%e,rname,'Array name must not be empty')
    error = .true.
    return
  endif
  call sic_validname(name,error)
  if (error) then
    call class_message(seve%e,rname,'Associated Array name must be a Sic valid name')
    return
  endif
  if (obs_nchan(obs%head).le.0) then
    call class_message(seve%e,rname,'Observation has invalid Nchan')
    error = .true.
    return
  endif
  !
  ! First check if such a section already exist
  if (class_assoc_exists(obs,name)) then
    call class_message(seve%e,rname,  &
      'Observation already contains an Associated Array '//name)
    error = .true.
    return
  endif
  !
  numarray = obs%assoc%n+1
  call reallocate_assoc(obs%assoc,numarray,.true.,error)
  if (error)  return
  !
  obs%head%presec(class_sec_assoc_id) = .true.
  !
  obs%assoc%array(numarray)%name = name
  obs%assoc%array(numarray)%unit = unit
  obs%assoc%array(numarray)%fmt  = form
  obs%assoc%array(numarray)%dim1 = obs_nchan(obs%head)
  obs%assoc%array(numarray)%dim2 = dim2
! obs%assoc%array(numarray)%badXX = done by class_assoc_add_sub1
! obs%assoc%array(numarray)%XXX(:,:) = done by caller
  !
  ! Data size
  call reallocate_assoc_sub(obs%assoc%array(numarray),error)
  if (error)  return
  !
end subroutine class_assoc_add_sub2
!
subroutine class_assoc_delete(obs,name,error)
  use classcore_interfaces, except_this=>class_assoc_delete
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Delete the named array from the section.
  !  No error if no such array
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs    ! Observation
  character(len=*),  intent(in)    :: name   ! The array name
  logical,           intent(inout) :: error  ! Logical error flag
  ! Local
  integer(kind=4) :: iarray,jarray
  !
  if (.not.class_assoc_exists(obs,name,iarray))  return
  ! This also checks if the section is enabled
  !
  ! Free this array
  call deallocate_assoc_sub(obs%assoc%array(iarray),error)
  if (error)  return
  !
  ! Have to compress the remaining part of the list
  do jarray=iarray+1,obs%assoc%n
    ! Let's be efficient: we steal the allocation from the upper neighbour,
    ! and so on. No copies, and this avoids knowing the details of the
    ! structure
    call reassociate_assoc_sub(obs%assoc%array(jarray),  &
                               obs%assoc%array(jarray-1),.true.,error)
    if (error)  return
  enddo
  !
  ! Dissociate the latest one for safety
  call deallocate_assoc_sub(obs%assoc%array(obs%assoc%n),error)
  if (error)  return
  !
  ! Final
  obs%assoc%n = obs%assoc%n-1
  obs%head%presec(class_sec_assoc_id) = obs%assoc%n.gt.0
  !
end subroutine class_assoc_delete
!
subroutine nullify_assoc_sub(array)
  use gildas_def
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Initialize an Associated Array. This does not take care of previous
  ! allocation status. If or an array which already exists, use the
  ! reallocation, reassociation, or deallocation subroutines
  !---------------------------------------------------------------------
  type(class_assoc_sub_t), intent(out)   :: array
  !
  array%fmt  = fmt_un
  array%dim1 = 0
  array%dim2 = 0
  array%name = ''
  array%unit = ''
  array%badi4 = -1000
  array%badr4 = -1000.
  !
  array%status = code_pointer_null
  array%r4 => null()
  array%i4 => null()
  !
end subroutine nullify_assoc_sub
!
recursive subroutine reallocate_assoc(assoc,narray,keep,error)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>reallocate_assoc
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Allocates or reallocates the section Associated Arrays depending on
  ! the 'narray' value. In case of reallocation, previous data can be
  ! preserved.
  !  Subroutine is recursive because it calls reassociate_assoc which
  ! calls back here (no infinite loop though).
  !---------------------------------------------------------------------
  type(class_assoc_t), intent(inout) :: assoc   !
  integer(kind=4),     intent(in)    :: narray  ! Number of Associated Arrays needed
  logical,             intent(in)    :: keep    ! Keep the previous data when enlarging?
  logical,             intent(inout) :: error   ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='REALLOCATE>ASSOC'
  integer(kind=4) :: ier,iarray
  type(class_assoc_t) :: sav
  logical :: dokeep
  !
  if (.not.allocated(assoc%array)) then
    dokeep = .false.  ! Nothing to keep
    continue
    !
  elseif (narray.gt.size(assoc%array)) then
    dokeep = keep
    if (dokeep) then
      ! Make an efficient save (use pointers instead of copy)
      call reassociate_assoc(assoc,sav,.true.,error)
      if (error)  return
    endif
    ! Free everything
    call deallocate_assoc(assoc,error)
    if (error)  return
    !
  else
    ! Already allocated to a larger size. Nothing to do
    assoc%n = narray
    return
  endif
  !
  if (narray.gt.0) then
    !
    ! Enlarge
    allocate(assoc%array(narray),stat=ier)
    if (failed_allocate(rname,'ASSOC%ARRAY',ier,error))  return
    !
    ! Initialize
    do iarray=1,narray
      assoc%array(iarray)%status = code_pointer_null
      assoc%array(iarray)%r4 => null()
      assoc%array(iarray)%i4 => null()
    enddo
    !
    if (dokeep) then
      ! Copy back the values
      call reassociate_assoc(sav,assoc,.true.,error)
      if (error)  return
      call deallocate_assoc(sav,error)
      if (error)  return
    endif
    !
  endif
  !
  ! Set the current number of arrays. Comes after 'dokeep' because the
  ! back-copy resets assoc%n to its former value.
  assoc%n = narray
  !
end subroutine reallocate_assoc
!
subroutine reallocate_assoc_sub(array,error)
  use gbl_format
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>reallocate_assoc_sub
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Reallocate the appropriate array according to its format
  !---------------------------------------------------------------------
  type(class_assoc_sub_t), intent(inout) :: array
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='REALLOCATE>ASSOC>SUB'
  integer(kind=4) :: dim1,dim2,ier
  character(len=message_length) :: mess
  logical :: realloc
  !
  ! Sanity check
  if (array%dim1.le.0 .or. array%dim2.lt.0) then
    write(mess,'(A,I0,A,I0)')  'Invalid dimensions: ',array%dim1,' x ',array%dim2
    call class_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  if (array%status.eq.code_pointer_associated) then
    ! This means we want a real allocation while we are pointing to another
    ! array. Not a problem, this is used at least in 'extract_assoc'. Just
    ! disassociate.
    array%r4 => null()
    array%i4 => null()
    array%status = code_pointer_null
  endif
  !
  dim1 = array%dim1
  dim2 = max(1,array%dim2)  ! Can be 0, i.e. 1D stored into degenerate 2D
  !
  realloc = .true.
  select case (array%fmt)
  case (fmt_r4)
    if (associated(array%r4)) then
      realloc = dim1.ne.ubound(array%r4,1) .or. dim2.ne.ubound(array%r4,2)
      if (realloc)  deallocate(array%r4)
    endif
    !
  case (fmt_i4,fmt_by,fmt_b2)
    if (associated(array%i4)) then
      realloc = dim1.ne.ubound(array%i4,1) .or. dim2.ne.ubound(array%i4,2)
      if (realloc)  deallocate(array%i4)
    endif
    !
  case default
    call class_message(seve%e,rname,'Kind of data not implemented')
    error = .true.
    return
  end select
  !
  if (.not.realloc)  return
  !
  select case (array%fmt)
  case (fmt_r4)
    allocate(array%r4(dim1,dim2),stat=ier)
  case (fmt_i4,fmt_by,fmt_b2)
    allocate(array%i4(dim1,dim2),stat=ier)
  case default
    call class_message(seve%e,rname,'Kind of data not implemented')
    error = .true.
    return
  end select
  if (failed_allocate(rname,'ARRAY%XX',ier,error))  return
  array%status = code_pointer_allocated
  !
end subroutine reallocate_assoc_sub
!
subroutine reassociate_assoc(in,ou,steal,error)
  use classcore_interfaces, except_this=>reassociate_assoc
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Copy a class_assoc_t, but use pointers instead of actual copies for
  ! the data arrays.
  ! - If 'steal' is .false., the output instance will point to the input
  !   one, and its pointer status is 'code_pointer_associated' in
  !   return.
  ! - If 'steal' is .true., the ownership of the actual allocation
  !   (code_pointer_allocated) is stolen to the input instance. It is an
  !   error to try to steal the ownership to a non-owner
  !   (code_pointer_associated).
  !---------------------------------------------------------------------
  type(class_assoc_t), intent(inout) :: in
  type(class_assoc_t), intent(inout) :: ou
  logical,             intent(in)    :: steal
  logical,             intent(inout) :: error
  ! Local
  integer(kind=4) :: iarray
  !
  call reallocate_assoc(ou,in%n,.false.,error)
  if (error)  return
  !
  do iarray=1,in%n
    call reassociate_assoc_sub(in%array(iarray),ou%array(iarray),steal,error)
    if (error)  return
  enddo
  !
end subroutine reassociate_assoc
!
subroutine reassociate_assoc_sub(in,ou,steal,error)
  use gildas_def
  use gbl_message
  use classcore_interfaces, except_this=>reassociate_assoc_sub
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Copy a class_assoc_sub_t, but use pointers instead of actual
  ! copies for the data arrays
  ! - If 'steal' is .false., the output instance will point to the input
  !   one, and its pointer status is 'code_pointer_associated' in
  !   return.
  ! - If 'steal' is .true., the ownership of the actual allocation
  !   (code_pointer_allocated) is stolen to the input instance. It is an
  !   error to try to steal the ownership to a non-owner
  !   (code_pointer_associated).
  !---------------------------------------------------------------------
  type(class_assoc_sub_t), intent(inout), target :: in
  type(class_assoc_sub_t), intent(inout)         :: ou
  logical,                 intent(in)            :: steal
  logical,                 intent(inout)         :: error
  ! Local
  character(len=*), parameter :: rname='REASSOCIATE>ASSOC>SUB'
  !
  if (steal .and. in%status.ne.code_pointer_allocated) then
    call class_message(seve%e,rname,  &
      'Internal error: can not steal allocation to an associated pointer')
    error = .true.
    return
  endif
  !
  ou%fmt  = in%fmt
  ou%dim1 = in%dim1
  ou%dim2 = in%dim2
  ou%name = in%name
  ou%unit = in%unit
  !
  ou%badr4 = in%badr4
  ou%badi4 = in%badi4
  ou%r4 => in%r4
  ou%i4 => in%i4
  !
  if (steal) then
    ou%status = in%status  ! We checked it is allocated
    in%status = code_pointer_associated
  else
    ou%status = code_pointer_associated
  endif
  !
end subroutine reassociate_assoc_sub
!
subroutine copy_assoc(in,out,error,dim1)
  use classcore_interfaces, except_this=>copy_assoc
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Copy an Associated Arrays section.
  !  - If dim1 is present, the output arrays will have this size,
  !    and the data is left unitialized.
  !  - If dim1 is absent, the output arrays have same size as input,
  !    and the input data is duplicated to the output.
  !---------------------------------------------------------------------
  type(class_assoc_t),       intent(in)    :: in
  type(class_assoc_t),       intent(inout) :: out
  logical,                   intent(inout) :: error
  integer(kind=4), optional, intent(in)    :: dim1
  ! Local
  integer(kind=4) :: iarray
  !
  ! First allocate or enlarge the section
  call reallocate_assoc(out,in%n,.false.,error)
  if (error)  return
  !
  ! Then, copy (duplicate) the section
  do iarray=1,in%n
    call copy_assoc_sub(in%array(iarray),out%array(iarray),error,dim1)
    if (error)  return
  enddo
  !
end subroutine copy_assoc
!
subroutine copy_assoc_sub(in,out,error,dim1)
  use gbl_format
  use gbl_message
  use classcore_interfaces, except_this=>copy_assoc_sub
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Copy an Associated Arrays subsection.
  !  - If dim1 is present, the output arrays will have this size,
  !    and the data is left unitialized.
  !  - If dim1 is absent, the output arrays have same size as input,
  !    and the input data is duplicated to the output.
  !---------------------------------------------------------------------
  type(class_assoc_sub_t),   intent(in)    :: in
  type(class_assoc_sub_t),   intent(inout) :: out
  logical,                   intent(inout) :: error
  integer(kind=4), optional, intent(in)    :: dim1
  ! Local
  character(len=*), parameter :: rname='COPY>ASSOC>SUB'
  !
  out%fmt  = in%fmt
  if (present(dim1)) then
    out%dim1 = dim1
  else
    out%dim1 = in%dim1
  endif
  out%dim2 = in%dim2
  out%name = in%name
  out%unit = in%unit
  out%badr4 = in%badr4
  out%badi4 = in%badi4
  !
  call reallocate_assoc_sub(out,error)
  if (error)  return
  !
  if (present(dim1))  return  ! Custom size, do not duplicate data
  !
  select case (in%fmt)
  case (fmt_r4)
    out%r4(:,:) = in%r4(:,:)
  case (fmt_i4,fmt_by,fmt_b2)
    out%i4(:,:) = in%i4(:,:)
  case default
    call class_message(seve%e,rname,'Kind of data not implemented')
    error = .true.
    return
  end select
  !
end subroutine copy_assoc_sub
!
subroutine copy_assoc_r4toaa(caller,in,out,error)
  use gbl_constant
  use classcore_interfaces, except_this=>copy_assoc_r4toaa
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Copy the input section of Associated Arrays into the inout updated
  ! section. Input data is ignored and their dim1 may differ. The data
  ! of the inout section is reused but incarnated from R*4 to the
  ! appropriate format and bad value.
  !---------------------------------------------------------------------
  character(len=*),    intent(in)    :: caller  ! Calling routine name
  type(class_assoc_t), intent(in)    :: in      ! Reference section
  type(class_assoc_t), intent(inout) :: out     ! Updated section
  logical,             intent(inout) :: error   ! Logical error flag
  ! Local
  integer(kind=4) :: iarray
  type(class_assoc_t) :: sav
  !
  if (out%n.le.0)  return
  !
  ! Save out (steal allocation)
  call reassociate_assoc(out,sav,.true.,error)
  if (error)  return
  !
  ! Delete old Arrays
  call deallocate_assoc(out,error)
  if (error)  goto 10
  !
  ! Recreate the section with actual name and formats
  call copy_assoc(in,out,error,sav%array(1)%dim1)
  if (error)  goto 10
  !
  do iarray=1,out%n
    call copy_assoc_sub_r4toaa(caller,out%array(iarray),  &
                               sav%array(iarray)%r4(:,1),  &
                               sav%array(iarray)%badr4,error)
    if (error)  goto 10
  enddo
  !
10 continue
  call deallocate_assoc(sav,error)  ! Because we stealed the allocation
  if (error)  return
  !
end subroutine copy_assoc_r4toaa
!
subroutine copy_assoc_sub_aator4(caller,array,r4,badr4,error)
  use gbl_message
  use classcore_interfaces, except_this=>copy_assoc_sub_aator4
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Copy (including incarnation) the Associated Array data into
  ! the R*4 array. Target must be of appropriate size.
  !---------------------------------------------------------------------
  character(len=*),        intent(in)    :: caller  ! Calling routine name
  type(class_assoc_sub_t), intent(in)    :: array   !
  real(kind=4),            intent(out)   :: r4(:)   !
  real(kind=4),            intent(out)   :: badr4   !
  logical,                 intent(inout) :: error   !
  !
  if (array%dim2.gt.1) then
    call class_message(seve%e,caller,  &
      'Associated Arrays with a 2nd dimension are not yet supported')
    error = .true.
    return
  endif
  !
  ! Work arrays, incarnation into R*4
  select case (array%fmt)
  case (fmt_r4)
    r4(:) = array%r4(:,1)
    badr4 = array%badr4
  case (fmt_i4,fmt_by,fmt_b2)
    r4(:) = array%i4(:,1)  ! I*4 to R*4
    badr4 = array%badi4  ! I*4 to R*4
  case default
    call class_message(seve%e,caller,'Kind of Associated Array not supported')
    error = .true.
    return
  end select
  !
end subroutine copy_assoc_sub_aator4
!
subroutine copy_assoc_sub_r4toaa(caller,array,r4,badr4,error)
  use gbl_message
  use classcore_interfaces, except_this=>copy_assoc_sub_r4toaa
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Copy (including incarnation) the R*4 array into the Associated
  ! Array data. The Associated Array is reallocated to appropriate
  ! size if needed.
  !---------------------------------------------------------------------
  character(len=*),        intent(in)    :: caller  ! Calling routine name
  type(class_assoc_sub_t), intent(inout) :: array   ! Updated Associated Array
  real(kind=4),            intent(in)    :: r4(:)   !
  real(kind=4),            intent(in)    :: badr4   !
  logical,                 intent(inout) :: error   !
  ! Local
  integer(kind=4) :: nchan
  !
  if (array%dim2.gt.1) then
    call class_message(seve%e,caller,  &
      'Associated Arrays with a 2nd dimension are not yet supported')
    error = .true.
    return
  endif
  !
  nchan = size(r4)
  !
  ! Reallocate Associated Array if needed
  array%dim1 = nchan
  call reallocate_assoc_sub(array,error)
  if (error)  return
  !
  ! Copy + incarnate into R*4 if needed
  select case (array%fmt)
  case (fmt_r4)
    where (r4.eq.badr4)
      array%r4(:,1) = array%badr4
    elsewhere
      array%r4(:,1) = r4(1:nchan)
    endwhere
    !
  case (fmt_i4,fmt_by,fmt_b2)
    call r4toi4(r4,array%i4,nchan)  ! R*4 **nint-rounded** to I*4
    where (r4.eq.badr4)
      array%i4(:,1) = array%badi4
    endwhere
    !
  case default
    call class_message(seve%e,caller,'Kind of Associated Array not supported')
    error = .true.
    return
  end select
  !
end subroutine copy_assoc_sub_r4toaa
!
subroutine deallocate_assoc(assoc,error)
  use gbl_format
  use classcore_interfaces, except_this=>deallocate_assoc
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(class_assoc_t), intent(inout) :: assoc
  logical,             intent(inout) :: error
  ! Local
  integer(kind=4) :: iarray
  !
  if (allocated(assoc%array)) then
    do iarray=1,size(assoc%array)
      call deallocate_assoc_sub(assoc%array(iarray),error)
      if (error)  return
    enddo
    deallocate(assoc%array)
  endif
  !
  assoc%n = 0
  !
end subroutine deallocate_assoc
!
subroutine deallocate_assoc_sub(array,error)
  use gildas_def
  use classcore_interfaces, except_this=>deallocate_assoc_sub
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(class_assoc_sub_t), intent(inout) :: array
  logical,                 intent(inout) :: error
  !
  if (array%status.eq.code_pointer_allocated) then
    ! Actual deallocation
    if (associated(array%r4))  deallocate(array%r4)
    if (associated(array%i4))  deallocate(array%i4)
  endif
  !
  call nullify_assoc_sub(array)
  !
end subroutine deallocate_assoc_sub
