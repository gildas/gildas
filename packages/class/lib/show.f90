module class_show_names
  !
  integer(kind=4), parameter :: nshow=43
  integer(kind=4), parameter :: showlength=13
  character(len=showlength), parameter :: show(nshow) = (/ &
    'ACTION       ','ALIGN        ','ANGLE        ','BAD          ',  &
    'BASELINE     ','CALIBRATION  ','CHECK        ','ENTRY        ',  &
    'EXTENSION    ','FILE         ','FIND         ','FITS         ',  &
    'FORMAT       ','FREQUENCY    ','IDX_SIZE     ','LINE         ',  &
    'LEVEL        ','MASK         ','MATCH        ','MODE         ',  &
    'NOCHECK      ','NUMBER       ','OBSERVATORY  ','OBSERVED_DATE',  &
    'OFFSET       ','PLOT         ','QUALITY      ','RANGE        ',  &
    'REDUCED_DATE ','SCALE        ','SCAN         ','SORT         ',  &
    'SOURCE       ','SUBSCAN      ','SYSTEM       ','TELESCOPE    ',  &
    'TYPE         ','UNIQUENESS   ','UNIT         ','VARIABLE     ',  &
    'VELOCITY     ','WEIGHT       ','WINDOW       ' /)
  !
  integer(kind=4), parameter :: arg_action        = 1
  integer(kind=4), parameter :: arg_align         = 2
  integer(kind=4), parameter :: arg_angle         = 3
  integer(kind=4), parameter :: arg_bad           = 4
  integer(kind=4), parameter :: arg_baseline      = 5
  integer(kind=4), parameter :: arg_calibration   = 6
  integer(kind=4), parameter :: arg_check         = 7
  integer(kind=4), parameter :: arg_entry         = 8
  integer(kind=4), parameter :: arg_extension     = 9
  integer(kind=4), parameter :: arg_file          = 10
  integer(kind=4), parameter :: arg_find          = 11
  integer(kind=4), parameter :: arg_fits          = 12
  integer(kind=4), parameter :: arg_format        = 13
  integer(kind=4), parameter :: arg_frequency     = 14
  integer(kind=4), parameter :: arg_idx_size      = 15
  integer(kind=4), parameter :: arg_line          = 16
  integer(kind=4), parameter :: arg_level         = 17
  integer(kind=4), parameter :: arg_mask          = 18
  integer(kind=4), parameter :: arg_match         = 19
  integer(kind=4), parameter :: arg_mode          = 20
  integer(kind=4), parameter :: arg_nocheck       = 21
  integer(kind=4), parameter :: arg_number        = 22
  integer(kind=4), parameter :: arg_observatory   = 23
  integer(kind=4), parameter :: arg_observed_date = 24
  integer(kind=4), parameter :: arg_offset        = 25
  integer(kind=4), parameter :: arg_plot          = 26
  integer(kind=4), parameter :: arg_quality       = 27
  integer(kind=4), parameter :: arg_range         = 28
  integer(kind=4), parameter :: arg_reduced_date  = 29
  integer(kind=4), parameter :: arg_scale         = 30
  integer(kind=4), parameter :: arg_scan          = 31
  integer(kind=4), parameter :: arg_sort          = 32
  integer(kind=4), parameter :: arg_source        = 33
  integer(kind=4), parameter :: arg_subscan       = 34
  integer(kind=4), parameter :: arg_system        = 35
  integer(kind=4), parameter :: arg_telescope     = 36
  integer(kind=4), parameter :: arg_type          = 37
  integer(kind=4), parameter :: arg_uniqueness    = 38
  integer(kind=4), parameter :: arg_unit          = 39
  integer(kind=4), parameter :: arg_variable      = 40
  integer(kind=4), parameter :: arg_velocity      = 41
  integer(kind=4), parameter :: arg_weight        = 42
  integer(kind=4), parameter :: arg_window        = 43
  !
end module class_show_names
!
subroutine class_show_comm(set,line,r,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_show_comm
  use class_show_names
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! CLASS  Support routine for command
  !   SHOW ALL  | Arg1 ... ArgN
  ! Line decoding routine
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  character(len=*),    intent(in)    :: line   ! Input command line
  type(observation),   intent(in)    :: r      !
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='SHOW'
  integer(kind=4) :: iarg,nch,ikey
  character(len=showlength+1) :: argum  ! +1 in order to trap too long inputs
  character(len=showlength) :: key
  !
  call sic_ke(line,0,1,argum,nch,.true.,error)
  if (error) return
  !
  if (argum.eq.'ALL') then
    if (sic_present(0,2)) then
      call class_message(seve%e,rname,'Trailing argument after ALL keyword')
      error = .true.
      return
    endif
    do iarg=1,nshow
      call class_show(set,iarg,r,error)
      if (error)  return
    enddo
    !
  else
    ! Custom list of arguments
    do iarg=1,sic_narg(0)  ! Restart from 1 for simplicity
      call sic_ke(line,0,iarg,argum,nch,.true.,error)
      if (error) return
      call sic_ambigs(rname,argum,key,ikey,show,nshow,error)
      if (error) return
      call class_show(set,ikey,r,error)
      if (error)  return
    enddo
  endif
  !
end subroutine class_show_comm
!
subroutine class_show(set,ikey,r,error)
  use phys_const
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_show
  use class_setup_new
  use class_fits
  use class_show_names
  use class_types
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS  Support routine for command
  !   SHOW ALL  | Arg1 ... ArgN
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  integer(kind=4),     intent(in)    :: ikey
  type(observation),   intent(in)    :: r
  logical,             intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SHOW'
  integer(kind=4), parameter :: maxi4=2147483647_4
  integer(kind=8), parameter :: maxi8=9223372036854775807_8
  real(kind=8) :: x0
  real(kind=plot_length) :: x1,x2,dx
  character(len=20) :: argum1,argum2,argum3,argum4
  character(len=80) :: chain
  integer(kind=4) :: i
  !
  select case (ikey)
  !
  case (arg_action)
    if (set%action.eq."O") then
      write(6,'(1x,a,t16,a)') 'ACTION','Observation'
    else if (set%action.eq."I") then
      write(6,'(1x,a,t16,a)') 'ACTION','Index'
    endif
    !
  case (arg_align)
    write(6,'(1x,a,t16,a,1x,a)') 'ALIGN',set%align,set%alig2
    !
  case (arg_angle)
    select case(class_setup_get_angle())
    case("RAD")
      chain = "Radian"
    case("DEG")
      chain = "Degree"
    case("MIN")
      chain = "Minute"
    case("SEC")
      chain = "Second"
    case("MAS")
      chain = 'Milli Arc-Second'
    end select
    write(6,'(1x,a,t16,a,4x,a)') 'ANGLE',class_setup_get_angle(),trim(adjustl(chain))
    !
  case (arg_bad)
    write(6,'(1x,a,t16,a)') 'BAD',set%bad
    !
  case (arg_baseline)
    if (set%base.eq.-1) then
      write(6,'(1x,a,t16,a)') 'BASELINE','Sinusoidal'
    else
      write(6,'(1x,a,t16,a,i4)') 'BASELINE',"Degree:",set%base
    endif
    !
  case (arg_calibration)
    if (set%beamt.eq.0.0) then
      write(6,'(1x,a,t16,a)') 'CALIBRATION','no check'
    else
      write(6,'(1x,a,t16,2f8.3)') 'CALIBRATION',set%beamt,set%gaint
    endif
    !
  case (arg_check,arg_nocheck)
    write(6,400) 'CHECK SOURCE', set%check%sou
    write(6,400) 'CHECK POSITIO',set%check%pos
    write(6,400) 'CHECK OFFSET', set%check%pos.and.set%check%off.and.set%match
    write(6,400) 'CHECK LINE',   set%check%lin
    write(6,400) 'CHECK SPECTRO',set%check%spe
    write(6,400) 'CHECK CALIBRA',set%check%cal.and.(set%beamt.gt.0..or.set%gaint.gt.0.)
    write(6,400) 'CHECK SWITCHI',set%check%swi
    !
  case (arg_entry)
    argum1 = '*'
    if (set%entr1.ne.0)  write(argum1,'(I0)')  set%entr1
    argum2 = '*'
    if (set%entr2.ne.maxi8)  write(argum2,'(I0)')  set%entr2
    write(6,'(1x,a,t16,3a)') 'ENTRY',trim(argum1),' to ',trim(argum2)
    !
  case (arg_extension)
    write(6,'(1x,a,t16,a)') 'EXTENSION',set%defext
    !
  case (arg_file)
    call class_files_info
    !
  case (arg_find)
    if (set%fupda) then
      write(6,'(1x,a,t16,a)') 'FIND','UPDATE'
    else
      write(6,'(1x,a,t16,a)') 'FIND','NOUPDATE'
    endif
    !
  case (arg_fits)
    write(6,'(1x,a,t16,a,i0,a,a)')  &
      'FITS','BITS: ',snbit,', MODE: ',trim(fits_mode)
    !
  case (arg_format)
    if (set%heade.eq.'B') then
      write(6,'(1x,a,t16,a)') 'FORMAT','BRIEF'
    else
      write(6,'(1x,a,t16,a)') 'FORMAT','LONG, information written:'
      if (set%title%position) chain = 'Position'
      if (set%title%quality) chain = trim(chain)//'  '//'Quality'
      if (set%title%spectral) chain = trim(chain)//'  '//'Spectral'
      if (set%title%calibration) chain = trim(chain)//'  '//'Calibration'
      if (set%title%atmosphere) chain = trim(chain)//'  '//'Atmosphere'
      if (set%title%origin) chain = trim(chain)//'  '//'Origin'
      write(6,'(21x,a)') trim(adjustl(chain))
    endif
    !
  case (arg_frequency)
    if (set%freqsig) then
      chain = ' (signal)'
    else
      chain = ' (image)'
    endif
    if (set%freqmin.le.0.d0 .and. set%freqmax.le.0.d0) then
      write(6,'(1x,a,t16,a)')  &
        'FREQUENCY','all'
    elseif (set%freqmin.le.0.d0 .and. set%freqmax.gt.0.d0) then
      write(6,'(1x,a,t16,a,f11.3,a9)')  &
        'FREQUENCY','0 to ',set%freqmax,chain
    elseif (set%freqmin.gt.0.d0 .and. set%freqmax.le.0.d0) then
      write(6,'(1x,a,t16,f11.3,a,a9)')  &
        'FREQUENCY',set%freqmin,' to infinity',chain
    elseif (set%freqmin.eq.set%freqmax) then
      write(6,'(1x,a,t16,f11.3,a9)')  &
        'FREQUENCY',set%freqmin,chain
    else
      write(6,'(1x,a,t16,f11.3,a,f11.3,a9)')  &
        'FREQUENCY',set%freqmin,' to ',set%freqmax,chain
    endif
    !
  case (arg_idx_size)
    write(6,'(1x,a,t16,i0)') 'IDX_SIZE',class_idx_size
    !
  case (arg_line)
    write(6,'(1x,a,t16,a)') 'LINE',set%line
    !
  case (arg_level)
    write(6,'(1x,a,t16,i5,i3)') 'LEVEL',set%slev,set%flev
    !
  case (arg_mask)
    if (set%nmask.eq.0) then
      write(6,'(1x,a,t16,a)') 'MASK','NO'
    else
      do  i=1,set%nmask
        if (r%head%gen%kind.eq.kind_spec) then
          write(6,300) 'Mask',i,' from',set%mask1(i),' to',set%mask2(i)
        elseif (r%head%gen%kind.eq.kind_cont) then
          write(6,300) 'Mask',i,' from',set%mask1(i)*class_setup_get_fangle()   &
                ,' to',set%mask2(i)*class_setup_get_fangle()
        endif
      enddo
    endif
    !
  case (arg_match)
    if (set%match) then
      call offsec(set,set%tole,argum1)
      write(6,'(1x,a,t16,a,a)') 'MATCH','YES: tolerance is ',trim(argum1)
    else
      write(6,'(1x,a,t16,a)') 'MATCH','NO'
    endif
    !
  case (arg_mode)
    if (set%modex.eq.'T') then
      ! Total
      write(chain,'(1x,a,t16,a)') 'MODE','X is TOTAL'
    elseif (set%modex.eq.'A') then
      ! Auto
      write(chain,'(1x,a,t16,a)') 'MODE','X is AUTO'
    else
      ! Fixed
      call gelimx(x0,x1,x2,dx,set%unitx(1))
      if (set%unitx(1).eq.'A') then  ! Angle
        write(chain,100) 'MODE','X is FIXED',  &
          (x0+x1)*class_setup_get_fangle(),(x0+x2)*class_setup_get_fangle()
      else
        write(chain,100) 'MODE','X is FIXED',x0+x1,x0+x2
      endif
    endif
    write(6,*) trim(adjustl(chain))
    !
    if (set%modey.eq.'T') then
      write(chain,'(1x,a,t16,a)') 'MODE','Y is TOTAL'
    elseif (set%modey.eq.'A') then
      write(chain,'(1x,a,t16,a)') 'MODE','Y is AUTO'
    else
      write(chain,'(1x,a,t16,a,2(1pg11.3))') 'MODE','Y is FIXED',guy1,guy2
    endif
    write(6,*) trim(adjustl(chain))
    !
  case (arg_number)
    if (set%nume2.lt.maxi8) then
      write(6,'(1x,a,t16,i0,a,i0)') 'NUMBER',set%nume1,' to ',set%nume2
    else
      write(6,'(1x,a,t16,i0,a)') 'NUMBER',set%nume1,' to *'
    endif
    !
  case (arg_observatory)
    if (set%obs_name.eq.'*') then
      write(6,'(1x,a,t16,2(a))')  'OBSERVATORY',trim(set%obs_name),  &
        ' (guess observatory from TELESCOPE name)'
    else
      call sexag(argum1,set%obs_long*rad_per_deg,360)
      call sexag(argum2,set%obs_lati*rad_per_deg,360)
      write(6,'(1x,a,t16,3(a,x),f0.2)') 'OBSERVATORY',trim(set%obs_name),  &
        trim(argum1),trim(argum2),set%obs_alti
    endif
    !
  case (arg_observed_date)
    call gag_todate(set%obse1,argum1,error)
    call gag_todate(set%obse2,argum2,error)
    write(6,'(1x,a,t16,a,a,a)') 'OBSERVED_DATE',trim(argum1),' to ',trim(argum2)
    !
  case (arg_offset,arg_range)
    ! SET OFFSET and SET RANGE are stored in the same structure
    call offsec(set,set%offs1,argum1)
    call offsec(set,set%offl1,argum2)
    call offsec(set,set%offs2,argum3)
    call offsec(set,set%offl2,argum4)
    write(6,'(1x,a,t16,a,a,a,a,a,a,a)')  'OFFSET/RANGE',trim(argum1),' to ',  &
      trim(argum2),' and ',trim(argum3),' to ',trim(argum4)
    !
  case (arg_plot)
    write(6,'(1x,a,t16,a)') 'PLOT',set%plot
    !
  case (arg_quality)
    write(6,'(1x,a,t16,a,i2,4x,a,i2)')  &
      'QUALITY','Minimal',set%qual2,'Maximal',set%qual1
    !
  case (arg_reduced_date)
    call gag_todate(set%redu1,argum1,error)
    call gag_todate(set%redu2,argum2,error)
    write(6,'(1x,a,t16,a,a,a)') 'REDUCED_DATE',trim(argum1),' to ',trim(argum2)
    !
  case (arg_scale)
    if (set%scale.eq.yunit_unknown) then
      write(6,'(1x,a,t16,a)') 'SCALE','NO'
    else
      write(6,'(1x,a,t16,a)') 'SCALE',obs_yunit(set%scale)
    endif
    !
  case (arg_scan)
    argum1 = '*'
    if (set%scan1.ne.0)  write(argum1,'(I0)')  set%scan1
    argum2 = '*'
    if (set%scan2.ne.maxi8)  write(argum2,'(I0)')  set%scan2
    write(6,'(1x,a,t16,3a)') 'SCAN',trim(argum1),' to ',trim(argum2)
    !
  case (arg_sort)
    write(6,'(1x,a,t16,a)') 'SORT',set%sort_name
    !
  case (arg_source)
    write(6,'(1x,a,t16,a)') 'SOURCE',set%sourc
    !
  case (arg_subscan)
    argum1 = '*'
    if (set%sub1.ne.0)  write(argum1,'(I0)')  set%sub1
    argum2 = '*'
    if (set%sub2.ne.maxi4)  write(argum2,'(I0)')  set%sub2
    write(6,'(1x,a,t16,3a)') 'SUBSCAN',trim(argum1),' to ',trim(argum2)
    !
  case (arg_system)
    if (set%coord.eq.0) then
      write(6,'(1x,a,t16,a)') 'SYSTEM','AUTO'
    elseif (set%coord.eq.type_eq) then
      write(6,'(1x,a,t16,a,f7.1)') 'SYSTEM','EQUATORIAL',set%equinox
    else
      write(6,'(1x,a,t16,a)') 'SYSTEM','GALACTIC'
    endif
    !
  case (arg_telescope)
    write(6,'(1x,a,t16,a)') 'TELESCOPE',set%teles
    !
  case (arg_type)
    if (set%kind.eq.kind_spec) then
      write(6,'(1x,a,t16,a)') 'TYPE','SPECTROSCOPY'
    elseif (set%kind.eq.kind_cont) then
      write(6,'(1x,a,t16,a)') 'TYPE','CONTINUUM'
    elseif (set%kind.eq.kind_sky) then
      write(6,'(1x,a,t16,a)') 'TYPE','SKYDIP'
    elseif (set%kind.eq.kind_onoff) then
      write(6,'(1x,a,t16,a)') 'TYPE','ONOFF'
    else
      write(6,'(1x,a,t16,a)') 'TYPE','<UNKNOWN>'
    endif
    !
  case (arg_uniqueness)
    if (set%uniqueness) then
      write(6,'(1x,a,t16,a)') 'UNIQUENESS','YES'
    else
      write(6,'(1x,a,t16,a)') 'UNIQUENESS','NO'
    endif
    !
  case (arg_unit)
    write(6,'(1x,a,t16,a,a,4x,a,a)') 'UNIT',&
      'lower axis ',set%unitx(1),'upper axis ',set%unitx(2)
    !
  case (arg_variable)
    write(6,100) 'VAR GENERAL ',setvar_modes(set%varpresec(class_sec_gen_id))
    write(6,100) 'VAR POSITION',setvar_modes(set%varpresec(class_sec_pos_id))
    write(6,100) 'VAR SPECTRO ',setvar_modes(set%varpresec(class_sec_spe_id))
    write(6,100) 'VAR RESOLUTI',setvar_modes(set%varpresec(class_sec_res_id))
    write(6,100) 'VAR BASE    ',setvar_modes(set%varpresec(class_sec_bas_id))
    write(6,100) 'VAR HISTORY ',setvar_modes(set%varpresec(class_sec_his_id))
    write(6,100) 'VAR PLOT    ',setvar_modes(set%varpresec(class_sec_plo_id))
    write(6,100) 'VAR SWITCH  ',setvar_modes(set%varpresec(class_sec_swi_id))
    write(6,100) 'VAR CALIBRAT',setvar_modes(set%varpresec(class_sec_cal_id))
    write(6,100) 'VAR SKYDIP  ',setvar_modes(set%varpresec(class_sec_sky_id))
    write(6,100) 'VAR GAUSS   ',setvar_modes(set%varpresec(class_sec_gau_id))
    write(6,100) 'VAR SHELL   ',setvar_modes(set%varpresec(class_sec_she_id))
    write(6,100) 'VAR NH3     ',setvar_modes(set%varpresec(class_sec_hfs_id))
    write(6,100) 'VAR ABSORPTI',setvar_modes(set%varpresec(class_sec_abs_id))
    write(6,100) 'VAR DRIFT   ',setvar_modes(set%varpresec(class_sec_dri_id))
    write(6,100) 'VAR BEAM    ',setvar_modes(set%varpresec(class_sec_bea_id))
    write(6,100) 'VAR CONTINUU',setvar_modes(set%varpresec(class_sec_poi_id))
    write(6,100) 'VAR ASSOCIAT',setvar_modes(set%varpresec(class_sec_assoc_id))
    write(6,100) 'VAR HERSCHEL',setvar_modes(set%varpresec(class_sec_her_id))
    write(6,100) 'VAR COMMENT ',setvar_modes(set%varpresec(class_sec_com_id))
    write(6,100) 'VAR USER    ',setvar_modes(set%varpresec(class_sec_user_id))
    !
  case (arg_velocity)
    if (set%veloc.eq.vel_aut) then
      write(6,'(1x,a,t16,a)') 'VELOCITY','Automatic mode'
    elseif (set%veloc.eq.vel_lsr) then
      write(6,'(1x,a,t16,a)') 'VELOCITY','LSR mode'
    elseif (set%veloc.eq.vel_hel) then
      write(6,'(1x,a,t16,a)') 'VELOCITY','Heliocentric mode'
    endif
    !
  case (arg_weight)
    write(6,'(1x,a,t16,a)') 'WEIGHT',set%weigh
    !
  case (arg_window)
    if (set%nwind.eq.setnwind_default) then
      write(6,'(1x,a,t16,a)') 'WINDOW','Not yet defined'
    elseif (set%nwind.eq.setnwind_polygon) then
      write(6,'(1x,a,t16,a)') 'WINDOW','Defined from polygon(s) in SET%WINDOW'
    elseif (set%nwind.eq.setnwind_assoc) then
      write(6,'(1x,a,t16,a)') 'WINDOW','Defined from LINE Associated Array'
    elseif (set%nwind.eq.0) then
      write(6,'(1x,a,t16,a)') 'WINDOW','Defined with 0 window'
    elseif (set%nwind.gt.0) then
      write(6,'(1x,a,t16)',advance='no') 'WINDOW'
      do i=1,set%nwind
          if (r%head%gen%kind.eq.kind_spec) then
            write(6,200,advance='no') set%wind1(i),set%wind2(i)
          elseif (r%head%gen%kind.eq.kind_cont) then
            write(6,200,advance='no') set%wind1(i)*class_setup_get_fangle(),  &
                                      set%wind2(i)*class_setup_get_fangle()
          endif
      enddo
      write(6,*)
    elseif (set%nwind.eq.setnwind_auto) then
      write(6,'(1x,a,t16,a)',advance='no') 'WINDOW','Automatic from data'
      if (r%head%bas%nwind.eq.basnwind_assoc) then
        write(6,'(A)',advance='no')  ' (LINE Associated Array)'
      else
        do i=1,r%head%bas%nwind
          if (r%head%gen%kind.eq.kind_spec) then
              write(6,200,advance='no') r%head%bas%w1(i),r%head%bas%w2(i)
          elseif (r%head%gen%kind.eq.kind_cont) then
              write(6,200,advance='no') r%head%bas%w1(i)*class_setup_get_fangle(),  &
                                        r%head%bas%w2(i)*class_setup_get_fangle()
          endif
        enddo
      endif
      write(6,*)
    endif
    !
  end select
  !
100 format (1x,a,t16,a,2(f11.2))
200 format (1x,f9.1,f9.1)
300 format (1x,a,i0,a,f9.1,a,f9.1)
400 format (1x,a,t16,l)
end subroutine class_show
