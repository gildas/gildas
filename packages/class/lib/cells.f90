subroutine class_cells(set,line,error,user_function)
  use gildas_def
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_cells
  use class_types
  use plot_formula
  !---------------------------------------------------------------------
  ! @ public
  ! CLASS ANALYSE Support routine for command
  !
  ! MAP [WHERE | MATCH | KEEP]
  ! 1   [/CELL X Y]
  ! 2   [/GRID]
  ! 3   [/NOLABEL]
  ! 4   [/NUMBER]
  ! 5   [/BASE [Ipen]]
  !
  ! Make a map of positions in the index.
  ! WHERE: put a marker at spectra locations; do not display the spectra
  ! MATCH: force identical scale on X and Y axis
  ! KEEP : take the previous grid
  ! Keywords MATCH and WHERE may be combined
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  character(len=*),    intent(in)    :: line           ! Input command line
  logical,             intent(inout) :: error          ! Logical error flag
  logical,             external      :: user_function  !
  ! Local
  character(len=*), parameter :: rname='MAP'
  integer(kind=4), parameter :: optcell=1
  integer(kind=4), parameter :: optgrid=2
  integer(kind=4), parameter :: optnola=3
  integer(kind=4), parameter :: optnumb=4
  integer(kind=4), parameter :: optbase=5
  logical :: grid,donolabel,match,keep,donumber,wher,docell,dobase
  integer(kind=4) :: n1,nch,i,basepen
  character(len=80) :: ch
  character(len=20) :: argum
  character(len=8), parameter :: voc1(3) = (/ 'MATCH','KEEP ','WHERE' /)
  character(len=8) :: avoc
  real(kind=4) :: disx,disy
  real(kind=plot_length)  :: gx1sav,gx2sav,gy1sav,gy2sav,guysav
  !
  if (set%kind.ne.kind_spec) then
     call class_message(seve%e,rname,'Unsupported kind of data')
     error =.true.
     return
  endif
  !
  ! Check options
  docell  = sic_present(optcell,0)
  grid    = sic_present(optgrid,0)
  donolabel = sic_present(optnola,0)
  donumber  = sic_present(optnumb,0)
  !
  ! /BASE [Ipen]
  dobase  = sic_present(optbase,0)
  if (sic_present(optbase,1)) then
    call sic_i4(line,optbase,1,basepen,.true.,error)
    if (error)  return
    if (basepen.lt.0) then
      call class_message(seve%e,rname,'/BASE IPen must be positive')
      error = .true.
      return
    endif
  else
    basepen = -1  ! means unset
  endif
  !
  ! Get current GreG Box
  call get_box(gx1,gx2,gy1,gy2)
  !
  ! Save current observation
  gx1sav = gx1
  gx2sav = gx2
  gy1sav = gy1
  gy2sav = gy2
  guysav = guy
  !
  argum = '*'
  keep  = .false.
  match = .false.
  wher  = .false.
  do i=1,2
     if (sic_present(0,i)) then
        call sic_ke (line,0,i,argum,nch,.true.,error)
        if (error) return
        call sic_ambigs(rname,argum,avoc,n1,voc1,3,error)
        if (error) return
        select case(avoc)
        case('MATCH')
           match = .true.  ! Match X and Y axis
        case('KEEP')
           keep = .true.   ! Use previous grid (for on line display)
        case('WHERE')
           wher = .true.   ! Put only markers
        end select
     endif
  enddo
  if (.not.wher.and.(set%modex.ne.'F' .or. set%modey.ne.'F')) then
     call class_message(seve%e,rname,'Mode X and Y must be fixed')
     error = .true.
     return
  endif
  !
  if (docell) then
     call sic_ch(line,1,1,argum,nch,.true.,error)
     if (error) return
     call coffse(set,rname,argum,' ',disx,error)  ! NB: no syntax for custom angle unit
     if (error) return
     call sic_ch(line,1,2,argum,nch,.false.,error)
     if (error) return
     call coffse(set,rname,argum,' ',disy,error)
     if (error) return
  endif
  !
  call class_cells_do(set,match,keep,wher,docell,disx,disy,grid,donolabel,  &
    donumber,dobase,basepen,error,user_function)
  if (error)  return
  !
  ! Restore box location
  gx1 = gx1sav
  gx2 = gx2sav
  gy1 = gy1sav
  gy2 = gy2sav
  guy = guysav
  write(ch,1000) 'SET BOX',gx1,gx2,gy1,gy2
  call gr_exec(ch(1:67))
  error = gr_error()
  !
1000 format(a,4(1x,g14.7))
end subroutine class_cells
!
subroutine class_cells_do(set,match,keep,wher,docell,disx,disy,grid,  &
  donolabel,donumber,dobase,basepen,error,user_function)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_cells_do
  use class_setup_new
  use class_index
  use class_popup
  use class_types
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  !  Support routine for command  ANA\MAP
  !  Do the actual job
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  logical,             intent(in)    :: match          !
  logical,             intent(in)    :: keep           !
  logical,             intent(in)    :: wher           !
  logical,             intent(in)    :: docell         !
  real(kind=4),        intent(inout) :: disx,disy      !
  logical,             intent(in)    :: grid           !
  logical,             intent(in)    :: donolabel      !
  logical,             intent(in)    :: donumber       !
  logical,             intent(in)    :: dobase         !
  integer(kind=4),     intent(in)    :: basepen        !
  logical,             intent(inout) :: error          !
  logical,             external      :: user_function  !
  ! Local
  character(len=*), parameter :: rname='MAP'
  integer(kind=4) :: ier,i,nchar,ltypem,nch,ltypev,nx,ny,oldpen,newpen
  character(len=80) :: ch,cdir,chain
  type(observation) :: obs
  real(kind=8) :: a1,a2,b1,b2,x1,x2
  real(kind=4) :: aval,cdef,cdef0,nxr,nyr
  character(len=1) :: c2,c1
  real(kind=4) :: celx,cely,lo1,lo2,mo1,mo2,scale
  logical :: clip_state
  integer(kind=entry_length) :: ient,ispec,jspec
  real(kind=plot_length) :: gx,gy
  integer(kind=8) :: cx_time
  logical :: saved,tree
  real(kind=4) :: lo1min,lo2min,lo1max,lo2max
  real(kind=plot_length) :: gx1new,gx2new,gy1new,gy2new
  data saved /.false./
  ! SAVE'd for the KEEP mode:
  save saved,cx_time,nxr,nyr
  save lo1min,lo1max,lo2min,lo2max
  save gx1new,gx2new,gy1new,gy2new
  !
  if (cx%next.le.1) then
     call class_message(seve%e,rname,'Index is empty')
     error = .true.
     return
  endif
  !
  tree = set%mapcl .and. .not.wher
  !
  ! Save current character size
  call sic_get_real('CHARACTER_SIZE',cdef,error)
  if (error) return
  cdef0 = cdef
  !
  ! Prepare the POPUP command
  npop = cx%next-1
  if (allocated(ipop)) deallocate(ipop,xpop,ypop)
  allocate(ipop(npop),xpop(npop),ypop(npop),stat=ier)
  if (ier.ne.0) then
     write(ch,'(a,i0)') 'POPUP Allocation error. Nb of cells: ',npop
     call class_message(seve%e,rname,ch)
     error = .true.
     npop = 0
     return
  endif
  !
  ! Grid definition
  if (keep) then
     ! Reuse SAVE'd values
     if (saved) then
       if (cx_time.ne.cx%time) then
         ! Error or warning?
         call class_message(seve%w,rname,  &
           'Current indeX has changed since last grid was KEEP''d')
       endif
     else
       call class_message(seve%e,rname,'Nothing KEEP''d in memory yet!')
       error = .true.
       return
     endif
  else
     ! (Re)define the grid
     gx1new = gx1
     gy1new = gy1
     gx2new = gx2
     gy2new = gy2
     if (.not.docell) then
        disx=1.e14
        disy=1.e14
     endif
     !
     ! Initialization
     xpop(1:cx%next-1) = cx%off1(1:cx%next-1)
     ypop(1:cx%next-1) = cx%off2(1:cx%next-1)
     lo1min = cx%ranges%lam1
     lo1max = cx%ranges%lam2
     lo2min = cx%ranges%bet1
     lo2max = cx%ranges%bet2
     !
     ! Size of a cell (rad)
     ! Takes into account user specified tolerance
     if (.not.docell) then
        do ispec=1,cx%next-1
           lo1=xpop(ispec)
           lo2=ypop(ispec)
           do jspec=ispec+1,cx%next-1
              mo1 = xpop(jspec)
              mo2 = ypop(jspec)
              aval = abs(lo1-mo1)
              if (aval.gt.set%tole) disx = min(disx,aval)
              aval = abs(lo2-mo2)
              if (aval.gt.set%tole) disy = min(disy,aval)
           enddo
           ! Abort if <^C>
           if (sic_ctrlc()) then
              call class_message(seve%e,rname,'Aborted by ^C')
              error = .true.
              return
           endif
        enddo
     endif
     !
     ! Size of a cell (cm). Use of 'nxr' (not yet rounded) is mandatory
     ! to ensure a strict equivalence between 'disx' and 'celx'. Using
     ! 'nx' can introduce offset in the cell positions.
     nxr = (lo1max-lo1min)/disx+1
     celx = (gx2new-gx1new)/nxr
     nx = nint(nxr)
     nyr  = (lo2max-lo2min)/disy+1
     cely = (gy2new-gy1new)/nyr
     ny = nint(nyr)
     if (nx.gt.2560 .or. ny.gt.2560) then
        write(chain,'(A,I0,A,I0)') 'Map too large: ',nx,' x ',ny
        call class_message(seve%e,rname,chain)
        call class_message(seve%e,rname,'Check tolerance (SET MATCH) please')
        error = .true.
        return
     endif
     !
     ! Force same scale
     if (match) then
        if (nx.eq.1) then
           scale = cely/disy
           disx = disy
        else if (ny.eq.1) then
           scale = celx/disx
           disy = disx
        else
           scale = min(celx/disx,cely/disy)
        endif
        celx = scale * disx
        cely = scale * disy
     endif
     if (set%verbose) then
        write(*,*) ' Cell X ',celx, disx
        write(*,*) ' Cell Y ',cely, disy
     endif
     !
     cx_time = cx%time
     saved = .true.
  endif
  !
  ! Plot
  if (set%mapcl) then
     call gtclear
     error = gr_error()
     if (error) return
  endif
  !
  ! WHERE keyword
  call init_obs(obs)
  ! We need the General section
  call get_it(set,obs,cx%ind(1),user_function,error)
  if (error)  goto 999
  ltypem = obs%head%pos%system
  ltypev = obs%head%spe%vtype
  !
  if (wher) then
     call gr_segm('MAPWHERE',error)
     write(ch,'(a,f5.2,a)') 'SET MARKER 4 1 ',min(celx,cely)*0.95,' 0'
     call gr_exec(ch)
     do ispec=1,cx%next-1
        ! Abort if <^C>
        if (sic_ctrlc()) then
           call class_message(seve%e,rname,'Aborted by ^C')
           error = .true.
           goto 999
        endif
        !
        if (ltypem.ne.type_eq .and. ltypem.ne.type_ga .and. ltypem.ne.type_ic) then
           gx1=gx1new+((xpop(ispec)-lo1min)/disx*celx)
           gx2=gx1+celx
        else
           gx2=gx2new-((xpop(ispec)-lo1min)/disx*celx)
           gx1=gx2-celx
        endif
        gy1=gy1new+((ypop(ispec)-lo2min)/disy*cely)
        gy2=gy1+cely
        !
        write(ch,1000) 'SET BOX',gx1,gx2,gy1,gy2
        call gr_exec(ch)
        error = gr_error()
        if (error) goto 999
        call grelocate(gx1+0.5*celx,gy1+0.5*cely)
        call gr_point(4,1)
     enddo
     call gr_segm_close(error)
     if (error) return
  endif
  !
  ! /NUMBER option
  if (donumber) then
     ! Compute max length of 'number'
     nchar = nint(log10(real(maxval(cx%num(1:cx%next-1)))))
     cdef = celx/nchar
     write(ch,1000) 'SET CHAR',cdef
     call gr_exec(ch(1:23))
     error = gr_error()
     if (error) goto 999
  endif
  !
  ! Loop on spectra: ONLY if not WHERE keyword
  if (.not.wher) then
     do ient=1,cx%next-1
        ! We need General, Position, Spectro, and Data. Read everything...
        call get_it(set,obs,cx%ind(ient),user_function,error)
        if (error)  goto 999
        call newdat(set,obs,error)  ! Compute obs%datax in current unit
        if (error)  goto 999
        if (ient.eq.1) ltypev = obs%head%spe%vtype  ! First spectrum
        if (ient.eq.1) ltypem = obs%head%pos%system
        !
        if (tree) then
           write (cdir,'(A,I4,A)') 'CREATE DIRECTORY ',ient,'PLOT'
           call gr_execl(cdir)
           cdir(1:6) = 'CHANGE'
           call gr_execl(cdir)
        endif
        !
        ipop(ient) = obs%head%gen%num
        npop = ient
        !
        ! Define position of spectra
        if (obs%head%pos%system.ne.ltypem) then
           write(chain,'(a,i0)') 'Incoherent offset type for observation ',obs%head%gen%num
           call class_message(seve%f,rname,chain)
           goto 999
        elseif (obs%head%spe%vtype.ne.ltypev) then
           write(chain,'(a,i0)') 'Incoherent velocity type for observation ',obs%head%gen%num
           call class_message(seve%f,rname,chain)
           goto 999
        elseif (ltypem.ne.type_eq .and. ltypem.ne.type_ga .and. ltypem.ne.type_ic) then
           gx1=gx1new+((obs%head%pos%lamof-lo1min)/disx*celx)
           gx2=gx1+celx
        else
           gx2=gx2new-((obs%head%pos%lamof-lo1min)/disx*celx)
           gx1=gx2-celx
        endif
        gy1=gy1new+((obs%head%pos%betof-lo2min)/disy*cely)
        gy2=gy1+cely
        write(ch,1000) 'SET BOX',gx1,gx2,gy1,gy2
        call gr_exec(ch)
        error = gr_error()
        if (error) goto 999
        !
        ! Plot the spectrum
        call spectr1d(rname,set,obs,error)
        if (error)  return
        !
        ! Plot the grid if needed
        if (grid .and. docell)  call gr_exec('BOX N N N')
        !
        ! Plot the baseline if needed
        if (dobase) then
          ! Draw a line at y=0. User coordinates:
          call abscissa_chan2any(set,obs,0.5d0,x1)
          call abscissa_chan2any(set,obs,obs%head%spe%nchan+0.5d0,x2)
          !
          if (basepen.ge.0)  oldpen = gr_spen(basepen)
          call gr_segm('BASE',error)
          if (error)  return
          call relocate(x1,0.d0)
          call draw(x2,0.d0)
          call gr_segm_close(error)
          if (error)  return
          if (basepen.ge.0)  newpen = gr_spen(oldpen)
        endif
        !
        ! Write the observation number if needed
        if (donumber) then
           write (ch,'(I0)') obs%head%gen%num
           call sic_black(ch,nch)
           call grelocate(gx1,gy2)
           call gr_exec ('LABEL "'//ch(1:nch)//'" /CENTER 3')
        endif
        if (tree) then
           cdir = 'CHANGE DIRECTORY ..'
           call gr_execl(cdir)
        endif
        !
        ! Abort if <^C>
        if (sic_ctrlc()) then
           call class_message(seve%e,rname,'Aborted by ^C')
           error = .true.
           goto 999
        endif
     enddo
  endif
  !
  ! Make the box with ticks
  if (ltypem.ne.type_eq .and. ltypem.ne.type_ga .and. ltypem.ne.type_ic) then
     a1 = dble(lo1min-0.5*disx)
     a2 = dble(lo1max+0.5*disx)
     gx1= gx1new
     gx2= gx1new+nxr*celx
  else
     a1 = dble(lo1max+0.5*disx)
     a2 = dble(lo1min-0.5*disx)
     gx2= gx2new
     gx1= gx2new-nxr*celx
  endif
  b1 = dble(lo2min-0.5*disy)
  b2 = dble(lo2max+0.5*disy)
  gy1= gy1new
  gy2= gy1new+nyr*cely
  !
  ! Popup limits
  pgx1 = gx1
  pgx2 = gx2
  pgy1 = gy1
  pgy2 = gy2
  pux1 = a1
  pux2 = a2
  puy1 = b1
  puy2 = b2
  cpop = cpop_map
  !
  ! Plot the lines if option /GRID present but not /CELL
  if (grid .and. .not.docell) then
     ! Such a grid makes sense only if the spectra are regularly spaced
     ! on a grid.
     call gr_segm('GRID',error)
     if (error)  return
     clip_state = gr_clip(.false.)
     do i=0,nx
        gx = gx1+i*celx
        call grelocate(gx,gy1)
        call gdraw(gx,gy2)
     enddo
     do i=0,ny
        gy = gy1+i*cely
        call grelocate(gx1,gy)
        call gdraw(gx2,gy)
     enddo
     clip_state = gr_clip(clip_state)
     call gr_segm_close(error)
     if (error)  return
  endif
  !
  ! Plot the Axis
  cdef = cdef0
  write(ch,1000) 'SET CHAR',cdef0
  call gr_exec(ch(1:23))
  error = gr_error()
  if (error) goto 999
  a1 = a1*class_setup_get_fangle()
  a2 = a2*class_setup_get_fangle()
  b1 = b1*class_setup_get_fangle()
  b2 = b2*class_setup_get_fangle()
  !
  write(ch,1000) 'LIMITS',a1,a2,b1,b2
  call gr_exec(ch(1:66))
  error = gr_error()
  if (error) goto 999
  write(ch,1000) 'SET BOX',gx1,gx2,gy1,gy2
  call gr_exec(ch(1:67))
  error = gr_error()
  if (error) goto 999
  !
  ! Supress labels for strips
  if (donolabel .or. (lo1min.eq.lo1max)) then
     c1 = 'N'
  else
     c1 = 'P'
  endif
  if (donolabel .or. (lo2min.eq.lo2max))  then
     c2 = 'N'
  else
     c2 = 'O'
  endif
  ch = 'BOX '//c1//' '//c2//' O'
  call gr_exec(ch)
  error = gr_error()
  if (error) return
  !
999 continue
  ! Free the OBS
  call free_obs(obs)
  !
  ! Restore current values
  cdef = cdef0
  write(ch,1000) 'SET CHAR',cdef0
  call gr_exec(ch(1:23))
  error = gr_error()
  if (error) return
  !
1000 format(a,4(1x,g14.7))
end subroutine class_cells_do
