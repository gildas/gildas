!-----------------------------------------------------------------------
! HIFI HCSS-FITS to Class specific subroutines
!-----------------------------------------------------------------------
!
subroutine fits_convert_bintable_hifi(set,fits,row_buffer,obs,invalid,  &
    user_function,error)
  use gbl_message
  use classcore_interfaces, except_this=>fits_convert_bintable_hifi
  use classfits_types
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  type(classfits_t),   intent(inout) :: fits           !
  integer(kind=1),     intent(inout) :: row_buffer(:)  !
  type(observation),   intent(inout) :: obs            !
  logical,             intent(inout) :: invalid        !
  logical,             external      :: user_function  !
  logical,             intent(inout) :: error          ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='FITS'
  integer(kind=4) :: icol
  !
  call fits_check_version_hifi(fits,error)
  if (error)  return
  !
  ! Try first simple columns
  do icol=1,fits%cols%desc%ncols
    if (fits%cols%desc%ttype(icol).eq.'frequency' .or.  &
        fits%cols%desc%ttype(icol).eq.'wave') then
      ! HIFI: there is a FREQUENCY or a WAVE column
      call fits_convert_bintable_bycolumn_hifi(set,fits,row_buffer,obs,  &
        user_function,error)
      return
    endif
  enddo
  !
  ! Then try rows
  do icol=1,fits%cols%desc%ncols
    if (fits%cols%desc%ttype(icol)(1:13).eq.'lsbfrequency_' .or.  &
        fits%cols%desc%ttype(icol)(1:13).eq.'usbfrequency_') then
      ! HIFI: there is 1 or more frequency arrays per row
      call fits_convert_bintable_byrow(set,fits,row_buffer,obs,  &
        user_function,error)
      return
    endif
  enddo
  !
  ! Not an error, just invalid i.e. skip
  call class_message(seve%w,rname,'No relevant data in this HIFI extension, skipping')
  invalid = .true.
  !
end subroutine fits_convert_bintable_hifi
!
subroutine fits_parse_ishcss_hifi(fits,ishcss,error)
  use classcore_interfaces, except_this=>fits_parse_ishcss_hifi
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Parse the HCSS vs CLASS-FITS format
  !---------------------------------------------------------------------
  type(classfits_t), intent(inout) :: fits    ! inout because of fits_get_header_card
  logical,           intent(out)   :: ishcss  !
  logical,           intent(inout) :: error   !
  ! Local
  character(len=80) :: value
  logical :: found
  !
  ishcss = .false.
  !
  call fits_get_header_card(fits,'HCSS____',value,found,error)
  if (error)  return
  if (found) then
    call fits_get_header_card(fits,'TYPE',value,found,error)
    if (error)  return
    ! HCSS FITS files with those 2 TYPE values use the CLASS-FITS format:
    if (found)  ishcss = value.ne.'HICLASS' .and.  &
                         value.ne.'Class formatted fits file'
  endif
  !
end subroutine fits_parse_ishcss_hifi
!
subroutine fits_parse_version_hifi(fits,version,error)
  use gbl_message
  use classcore_interfaces, except_this=>fits_parse_version_hifi
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Parse the FITS version from CREATOR card
  !---------------------------------------------------------------------
  type(classfits_t), intent(inout) :: fits  ! inout because of fits_get_header_card
  character(len=*),  intent(out)   :: version
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='FITS'
  integer(kind=4) :: idigit,lcreator
  character(len=20) :: creator
  logical :: found
  !
  version = ''  ! If not found or not understood
  !
  creator = 'Unknown'  ! The default in some HDUs...
  call fits_get_header_card(fits,'CREATOR',creator,found,error)
  if (error) return
  !
  if (creator.eq.'Unknown') then  ! Found or not found...
    ! We can say nothing. Avoid 1 warning per HDU...
    return
  endif
  !
  ! CREATOR can be e.g.
  !  "SPG v12.1.0"
  !  "HifiPipeline_14.0.19"
  !
  ! Find position of 1st digit in string
  lcreator = len_trim(creator)
  idigit = 1
  do while (llt(creator(idigit:idigit),'1') .or.  &
            lgt(creator(idigit:idigit),'9'))
    idigit = idigit+1
    if (idigit.gt.lcreator) then
      call class_message(seve%w,rname,'FITS version not understood from CREATOR = '//creator)
      return
    endif
  enddo
  !
  version = creator(idigit:)  ! e.g. "12.1.0" or "14.0.19"
  !
end subroutine fits_parse_version_hifi
!
subroutine fits_check_version_hifi(fits,error)
  use gbl_message
  use classcore_interfaces, except_this=>fits_check_version_hifi
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Check if the FITS version is supported
  !---------------------------------------------------------------------
  type(classfits_t), intent(inout) :: fits
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='FITS'
  character(len=12) :: version
  character(len=*), parameter :: minversion='12'
  !
  call fits_parse_version_hifi(fits,version,error)
  if (error)  return
  if (version.eq.'')  return
  !
  ! Now we can check the value
  if (llt(version,minversion)) then
    call class_message(seve%e,rname,  &
      'Support starts from FITS version '//minversion//' (found '//trim(version)//')')
    call class_message(seve%e,rname,  &
      'You have to download a newer FITS version from the Herschel archive')
    error = .true.
    return
  endif
  !
end subroutine fits_check_version_hifi
!
subroutine fits_convert_bintable_bycolumn_hifi(set,fits,row_buffer,obs,  &
  user_function,error)
  use gbl_message
  use classcore_interfaces, except_this=>fits_convert_bintable_bycolumn_hifi
  use classfits_types
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Extract the 'flux' column as a single spectrum
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  type(classfits_t),   intent(inout) :: fits           !
  integer(kind=1),     intent(inout) :: row_buffer(:)  !
  type(observation),   intent(inout) :: obs            ! Observation buffer
  logical,             external      :: user_function  !
  logical,             intent(inout) :: error          !
  ! Local
  character(len=*), parameter :: rname='FITS'
  integer(kind=4) :: irow,ifreq,iflux,iflag
  integer(kind=4), pointer :: aablank(:),aaline(:)
  integer(kind=4) :: subband
  integer(kind=size_length) :: nbyt
  logical :: found
  integer(kind=1) :: zerobuf(0)  ! Zero-sized buffer
  real(kind=8) :: lam,bet,hotcold(2)
  character(len=20) :: freq_ttype
  !
  ! Which flux column?
  call fits_get_bintable_key2column(fits%cols,'flux',iflux,found)
  if (.not.found) then
    call class_message(seve%e,rname,'No flux column found')
    error = .true.
    return
  endif
  !
  ! Which frequency column?
  freq_ttype = 'frequency'
  call fits_get_bintable_key2column(fits%cols,freq_ttype,ifreq,found)
  if (.not.found) then
    freq_ttype = 'wave'
    call fits_get_bintable_key2column(fits%cols,freq_ttype,ifreq,found)
    if (.not.found) then
      call class_message(seve%e,rname,'No ''frequency'' or ''wave'' column found')
      error = .true.
      return
    endif
  endif
  !
  ! Which flag column?
  call fits_get_bintable_key2column(fits%cols,'flag',iflag,found)
  if (.not.found) then
    call fits_warn_missing(fits%warn,'Column','flag','channel flags',0,found,error)
    if (error)  return
    iflag = 0
  endif
  !
  if (iflag.eq.0) then
    call class_message(seve%i,rname,  &
      'Importing data from the '''//trim(freq_ttype)//''' and ''flux'' columns')
  else
    call class_message(seve%i,rname,  &
      'Importing data from the '''//trim(freq_ttype)//''', ''flux'' and ''flag'' columns')
  endif
  !
  ! --- Associated Arrays section (stage 1) ---
  ! Must come before the row-decoding below
  call fits_convert_header_assoc_hifi(fits,zerobuf,fits%cols%desc%nrows,obs,  &
    aablank,aaline,error)
  if (error)  return
  !
  ! --- Data (freq, flux) + Associated Arrays section (stage 2) ---
  call reallocate_obs(obs,fits%cols%desc%nrows,error)
  if (error)  return
  nbyt = fits%cols%desc%lrow
  do irow=1,fits%cols%desc%nrows
    ! Read the row into buffer.
    call gfits_getbuf(row_buffer,nbyt,error)
    if (error)  return
    !
    call fits_chopbuf_1chan_hifi(fits,row_buffer,fits%cols%desc%lrow,ifreq,iflux,  &
      iflag,obs,aablank,aaline,irow,error)
    if (error)  return
  enddo
  !
  ! --- General section ---
  subband = 0  ! Subband is set to "00" in telescope name
  call fits_convert_header_gen_hifi(fits,zerobuf,subband,obs,error)
  if (error)  return
  !
  ! --- Spectroscopic section ---
  call fits_convert_header_spe_hifi(fits,zerobuf,ifreq,fits%cols%desc%nrows,obs,error)
  if (error)  return
  !
  ! --- Position section ---
  ! In FITS level 2.5, the actual position is found in cards 'RA' and DEC'.
  ! The metacards 'longitude' and 'latitude', even if they seem identical in
  ! practice, are not guaranteed.
  call fits_get_header_card(fits,'RA',lam,found,error)
  if (error) return
  if (.not.found) then
    ! Will use RA_NOM as actual position, i.e. same as reference, i.e. 0 offset
    call fits_warn_missing(fits%warn,'Card','RA','R%HEAD%POS%LAMOF',0.,found,error)
    if (error)  return
    call fits_get_header_card(fits,'RA_NOM',lam,found,error)
    if (error) return
    if (.not.found) then
      call class_message(seve%e,rname,'Card RA_NOM not found')
      error = .true.
      return
    endif
  endif
  call fits_get_header_card(fits,'DEC',bet,found,error)
  if (error) return
  if (.not.found) then
    ! Will use RA_NOM as actual position, i.e. same as reference, i.e. 0 offset
    call fits_warn_missing(fits%warn,'Card','DEC','R%HEAD%POS%BETOF',0.,found,error)
    if (error)  return
    call fits_get_header_card(fits,'DEC_NOM',bet,found,error)
    if (error) return
    if (.not.found) then
      call class_message(seve%e,rname,'Card DEC_NOM not found')
      error = .true.
      return
    endif
  endif
  call fits_convert_header_pos_hifi(set,fits,lam,bet,obs,error)
  if (error)  return
  !
  ! --- Calibration section ---
  hotcold(1) = 0.d0
  call fits_get_header_metacard(fits,'hbbTemp',hotcold(1),found,error,'R%HEAD%CAL%TCHOP')
  if (error)  return
  hotcold(2) = 0.d0
  call fits_get_header_metacard(fits,'cbbTemp',hotcold(2),found,error,'R%HEAD%CAL%TCOLD')
  if (error)  return
  call fits_convert_header_cal_hifi(fits,hotcold,obs,error)
  if (error)  return
  !
  ! --- Frequency switch section ---
  call fits_convert_header_swi_hifi(fits,zerobuf,obs,error)
  if (error)  return
  !
  ! --- Herschel section ---
  call fits_convert_header_her_hifi(fits,zerobuf,obs,error)
  if (error)  return
  !
  ! Set default value if not present.
  call fits_check_head(obs,error)
  if (error)  return
  !
  call class_write(set,obs,error,user_function)
  if (error)  return
  !
end subroutine fits_convert_bintable_bycolumn_hifi
!
subroutine fits_convert_bintable_byrow_hifi(set,row_buffer,fits,isub,obs,  &
  user_function,error)
  use classcore_interfaces, except_this=>fits_convert_bintable_byrow_hifi
  use classfits_types
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Read data from a HIFI FLUX_i columns and write it as a Class
  ! spectrum
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  integer(kind=1),     intent(in)    :: row_buffer(:)  !
  type(classfits_t),   intent(inout) :: fits           !
  integer(kind=4),     intent(in)    :: isub           !
  type(observation),   intent(inout) :: obs            !
  logical,             external      :: user_function  !
  logical,             intent(inout) :: error          !
  ! Local
  character(len=*), parameter :: rname='FITS>READ>BINTABLE>HIFI'
  logical, parameter :: debug = .false.
  logical :: done
  !
  ! Decode FITS header into CLASS parameters.
  call fits_head2obs(set,fits,obs,error)
  if (error)  return
  !
  ! Decode FITS-HIFI table into CLASS parameters
  call fits_read_bintable_byrow_hifi(set,row_buffer,fits,isub,obs,done,error)
  if (error)  return
  if (.not.done)  return
  !
  ! Set default value if not present.
  call fits_check_head(obs,error)
  if (error)  return
  !
  call class_write(set,obs,error,user_function)
  if (error)  return
  !
end subroutine fits_convert_bintable_byrow_hifi
!
subroutine fits_read_bintable_byrow_hifi(set,row_buffer,fits,isub,obs,  &
  obsdone,error)
  use gbl_constant
  use gbl_format
  use gbl_message
  use phys_const
  use classcore_interfaces, except_this=>fits_read_bintable_byrow_hifi
  use classfits_types
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Read FITS binary table into Class parameters (HIFI-specific table)
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  integer(kind=1),     intent(in)    :: row_buffer(:)  !
  type(classfits_t),   intent(inout) :: fits           !
  integer(kind=4),     intent(in)    :: isub           !
  type(observation),   intent(inout) :: obs            !
  logical,             intent(out)   :: obsdone        !
  logical,             intent(inout) :: error          !
  ! Local
  character(len=*), parameter :: rname='FITS'
  integer(kind=4) :: iflux,ifreq,iflag,nydata,nxdata
  integer(kind=4), pointer :: aablank(:),aaline(:)
  integer(kind=4) :: ichan,icol
  logical :: found
  character(len=20) :: flux_ttype,freq_ttype,flag_ttype
  integer(kind=4), allocatable :: flags(:)
  real(kind=8) :: lam,bet,hotcold(2)
  !
  obsdone = .false.
  !
  write(flux_ttype,'(A,I0)')  'flux_',isub
  call fits_get_bintable_key2column(fits%cols,flux_ttype,iflux,found)
  if (.not.found)  return  ! No error
  !
  write(freq_ttype,'(A,I0)')  'lsbfrequency_',isub
  call fits_get_bintable_key2column(fits%cols,freq_ttype,ifreq,found)
  if (.not.found) then
    write(freq_ttype,'(A,I0)')  'usbfrequency_',isub
    call fits_get_bintable_key2column(fits%cols,freq_ttype,ifreq,found)
  endif
  if (.not.found) then
    call class_message(seve%e,rname,'No SBFREQUENCY_i column!')
    error = .true.
    return
  endif
  !
  write(flag_ttype,'(A,I0)')  'flag_',isub
  call fits_get_bintable_key2column(fits%cols,flag_ttype,iflag,found)
  if (.not.found)  iflag = 0  ! No error
  !
  nydata = fits%cols%desc%nitem(iflux)
  if (nydata.le.0) then
    call class_message(seve%w,rname,  &
      'No data in '//trim(flux_ttype)//' column, skip this subband')
    ! No error
    return
  endif
  !
  nxdata = fits%cols%desc%nitem(ifreq)
  if (nxdata.ne.nydata) then
    call class_message(seve%w,rname,  &
      trim(flux_ttype)//' and '//trim(freq_ttype)//' mismatch, skip this subband')
    ! No error
    return
  endif
  !
  if (iflag.eq.0) then
    call class_message(seve%i,rname,'Importing data from the '''//trim(freq_ttype)  &
      //''' and '''//trim(flux_ttype)//''' columns')
  else
    call class_message(seve%i,rname,'Importing data from the '''//trim(freq_ttype)  &
      //''', '''//trim(flux_ttype)//''' and '''//trim(flag_ttype)//''' columns')
  endif
  !
  ! --- Associated Arrays section (stage 1) ---
  ! Must come before the row-decoding below
  call fits_convert_header_assoc_hifi(fits,row_buffer,nydata,obs,aablank,aaline,error)
  if (error)  return
  !
  ! --- Data (freq, flux) ---
  call reallocate_obs(obs,nydata,error)
  if (error)  return
  call get_item(obs%data1,nydata,fmt_r4,row_buffer(fits%cols%desc%addr(iflux)),  &
    fits%cols%desc%fmt(iflux),error)
  if (error)  return
  ! Read irregular X axis (NB: DATAV will contain freq or velo according to XUNIT)
  if (xdata_kind.eq.4) then
    call get_item(obs%datav,nxdata,fmt_r4,row_buffer(fits%cols%desc%addr(ifreq)),  &
      fits%cols%desc%fmt(ifreq),error)
  else
    call get_item(obs%datav,nxdata,fmt_r8,row_buffer(fits%cols%desc%addr(ifreq)),  &
      fits%cols%desc%fmt(ifreq),error)
  endif
  if (error)  return
  !
  ! --- Associated Arrays section (stage 2) ---
  if (iflag.gt.0) then
    allocate(flags(nydata))
    call get_item(flags,nydata,fmt_i4,row_buffer(fits%cols%desc%addr(iflag)),  &
      fits%cols%desc%fmt(iflag),error)
    if (error)  return
    do ichan=1,nydata
      if (aablank(ichan).eq.0) then  ! Not yet blanked
        aablank(ichan) = fits_convert_flag_hifi(2,1,flags(ichan))
      endif
      if (aaline(ichan).eq.0) then  ! Not yet in a line window
        aaline(ichan) = fits_convert_flag_hifi(2,2,flags(ichan))
      endif
    enddo
    deallocate(flags)
  endif
  !
  ! --- General section ---
! if (fits%cols%desc%tunit(icol).ne.'s')  call class_message(seve%w,rname,  &
!   'INTEGRATION TIME unit is not supported :'//fits%cols%desc%tunit(icol))
  call fits_convert_header_gen_hifi(fits,row_buffer,isub,obs,error)
  if (error)  return
  !
  ! --- Spectroscopic section ---
  call fits_convert_header_spe_hifi(fits,row_buffer,ifreq,nydata,obs,error)
  if (error)  return
  !
  ! --- Frequency Switch section ---
  call fits_convert_header_swi_hifi(fits,row_buffer,obs,error)
  if (error)  return
  !
  ! --- Calibration section ---
  hotcold(:) = 0.d0
  call fits_get_bintable_key2column(fits%cols,'hot_cold',icol,found)
  if (found) then
    call get_item(hotcold,2,fmt_r8,row_buffer(fits%cols%desc%addr(icol)),  &
        fits%cols%desc%fmt(icol),error)
    if (error)  return
  endif
  call fits_convert_header_cal_hifi(fits,hotcold,obs,error)
  if (error)  return
  !
  ! --- Herschel-HIFI section ---
  call fits_convert_header_her_hifi(fits,row_buffer,obs,error)
  if (error)  return
  !
  ! --- Position section ---
  ! In FITS level 2.0, the actual position is found in columns 'longitude'
  ! and 'latitude'.
  call fits_get_bintable_key2column(fits%cols,'longitude',icol,found)
  if (.not.found) then
    call class_message(seve%e,rname,'"longitude" position not found')
    error = .true.
    return
  endif
  if (fits%cols%desc%tunit(icol).ne.'deg') then
    call class_message(seve%e,rname,  &
      '"longitude" unit is not supported: '//fits%cols%desc%tunit(icol))
    error = .true.
    return
  endif
  call get_item(lam,1,fmt_r8,row_buffer(fits%cols%desc%addr(icol)),  &
      fits%cols%desc%fmt(icol),error)
  if (error)  return
  !
  call fits_get_bintable_key2column(fits%cols,'latitude',icol,found)
  if (.not.found) then
    call class_message(seve%e,rname,'"latitude" position not found')
    error = .true.
    return
  endif
  if (fits%cols%desc%tunit(icol).ne.'deg') then
    call class_message(seve%e,rname,  &
      '"latitude" unit is not supported: '//fits%cols%desc%tunit(icol))
    error = .true.
    return
  endif
  call get_item(bet,1,fmt_r8,row_buffer(fits%cols%desc%addr(icol)),  &
      fits%cols%desc%fmt(icol),error)
  if (error)  return
  !
  call fits_convert_header_pos_hifi(set,fits,lam,bet,obs,error)
  if (error)  return
  !
  obsdone = .true.
  !
end subroutine fits_read_bintable_byrow_hifi
!
subroutine fits_convert_header_gen_hifi(fits,buffer,subband,obs,error)
  use gbl_message
  use classcore_interfaces, except_this=>fits_convert_header_gen_hifi
  use classfits_types
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Set up the General section
  !---------------------------------------------------------------------
  type(classfits_t), intent(inout) :: fits       !
  integer(kind=1),   intent(in)    :: buffer(:)  !
  integer(kind=4),   intent(in)    :: subband    ! Subband number
  type(observation), intent(inout) :: obs        !
  logical,           intent(inout) :: error      ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='FITS>CONVERT>HEADER>GEN>HIFI'
  character(len=80) :: backend,band,datebeg,dateend
  real(kind=8) :: mjdbeg,mjdend
  logical :: found
  integer(kind=4) :: bbtype,bbnumber
  !
  ! Single or multiple, set obs num to 0 in order to give a new (unused)
  ! number in the output file at write time.
  ! No reason there should be several versions of the same spectrum
  ! in this context (reading a FITS file)
  obs%head%gen%num = 0  ! Automatic in class_write()
  obs%head%gen%ver = 0
  !
  ! Telescope name
  obs%head%gen%teles = 'HIF-  -  -  '
  write(obs%head%gen%teles(5:6),'(I2.2)')  subband
  !
  backend = ''
  call fits_get_header_card(fits,'BACKEND',backend,found,error)
  if (error)  return
  if (backend.eq.'') then
    ! BACKEND is not available, use meta keyword 'polarization' instead
    call fits_get_header_metacard(fits,'polarization',backend,found,error)
    if (error)  return
  endif
  obs%head%gen%teles(8:8) = backend(1:1)  ! spectro
  obs%head%gen%teles(9:9) = backend(5:5)  ! polar
  !
  band = ''
  call fits_get_header_card(fits,'BAND',band,found,error)
  if (error)  return
  obs%head%gen%teles(11:12) = band  ! LO band
  !
  ! DOBS + UT, DRED
  call fits_get_header_card(fits,'DATE-OBS',datebeg,found,error)
  if (error)  return
  if (.not.found) then
    call fits_warn_missing(fits%warn,'Card','DATE-OBS','R%HEAD%GEN%DOBS and R%HEAD%GEN%UT','today',found,error)
    if (error)  return
    call sic_isodate(datebeg)  ! Get current date-time
  endif
  call gag_isodate2mjd(datebeg,mjdbeg,error)
  if (error)  return
  dateend = datebeg  ! Defaults to DATE-OBS
  call fits_get_header_card(fits,'DATE-END',dateend,found,error)
  if (error)  return
  if (.not.found) then
    call fits_warn_missing(fits%warn,'Card','DATE-END','R%HEAD%GEN%DOBS and R%HEAD%GEN%UT','today',found,error)
    if (error)  return
  endif
  call gag_isodate2mjd(dateend,mjdend,error)
  if (error)  return
  call gag_mjd2gagut((mjdbeg+mjdend)/2.d0,obs%head%gen%dobs,obs%head%gen%ut,error)
  if (error)  return
  !
  obs%head%gen%dred = 0  ! class_write will force it to today
  !
  bbtype = obs%head%gen%scan  ! If not found, leave previous value as is
  call fits_get_metacard_or_column(fits,buffer,'bbtype',bbtype,found,error,'R%HEAD%GEN%SCAN')
  if (error) return
  obs%head%gen%scan = bbtype
  !
  bbnumber = obs%head%gen%subscan  ! If not found, leave previous value as is
  call fits_get_metacard_or_column(fits,buffer,'bbnumber',bbnumber,found,error,'R%HEAD%GEN%SUBSCAN')
  if (error) return
  obs%head%gen%subscan = bbnumber
  !
  obs%head%gen%tsys = 0.  ! Default if not found
  call fits_get_metacard_or_column(fits,buffer,'tsys_median',obs%head%gen%tsys,found,error,'R%HEAD%GEN%TSYS')
  if (error) return
  !
  obs%head%gen%time = 0.  ! Default if not found
  call fits_get_metacard_or_column(fits,buffer,'integrationTime',obs%head%gen%time,found,error,'R%HEAD%GEN%TIME')
  if (error) return
  !
  ! Easy ones
  obs%head%gen%kind = kind_spec
  obs%head%gen%qual = qual_unknown
  obs%head%gen%tau  = 0.0
  !
end subroutine fits_convert_header_gen_hifi
!
subroutine fits_convert_header_pos_hifi(set,fits,lam,bet,obs,error)
  use gbl_constant
  use gbl_message
  use phys_const
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>fits_convert_header_pos_hifi
  use classfits_types
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Set up the position section
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(classfits_t),   intent(inout) :: fits   !
  real(kind=8),        intent(in)    :: lam    ! [deg] Actual position (RA)
  real(kind=8),        intent(in)    :: bet    ! [deg] Actual position (DEC)
  type(observation),   intent(inout) :: obs    ! Position section
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='FITS>CONVERT>HEADER>POS>HIFI'
  real(kind=8) :: reflam,refbet,newpang
  integer(kind=4) :: newproj
  logical :: found
  !
  obs%head%pos%sourc = 'UNKNOWN'  ! Default if not found
  call fits_get_header_card(fits,'OBJECT',obs%head%pos%sourc,found,error,'R%HEAD%POS%SOURC')
  if (error)  return
  !
  ! 1) Get the absolute reference we wish
  call fits_get_header_card(fits,'RA_NOM',reflam,found,error)
  if (error)  return
  if (.not.found) then
    call class_message(seve%e,rname,'RA_NOM not found in header')
    error = .true.
    return
  endif
  reflam = reflam*rad_per_deg
  !
  call fits_get_header_card(fits,'DEC_NOM',refbet,found,error)
  if (error)  return
  if (.not.found) then
    call class_message(seve%e,rname,'DEC_NOM not found in header')
    error = .true.
    return
  endif
  refbet = refbet*rad_per_deg
  !
  ! 2) System of coordinates + Equinox
  obs%head%pos%system = type_eq  ! Obviously
  !
  call fits_get_header_card(fits,'EQUINOX',obs%head%pos%equinox,found,error)
  if (error)  return
  if (.not.found) then
    call class_message(seve%e,rname,'EQUINOX not found in header')
    error = .true.
    return
  endif
  !
  ! 3) Set up the current position, and offset+projection
  obs%head%pos%proj = p_radio
  obs%head%pos%lam = lam*rad_per_deg
  obs%head%pos%bet = bet*rad_per_deg
  obs%head%pos%projang = 0.d0
  obs%head%pos%lamof = 0.0
  obs%head%pos%betof = 0.0
  !
  ! 4) Modify actual position to the desired reference
  newproj = obs%head%pos%proj     ! Kind of projection unchanged
  newpang = obs%head%pos%projang  ! Projection angle unchanged
  call modify_projection(set,obs%head,newproj,reflam,refbet,newpang,error)
  if (error)  return
  !
end subroutine fits_convert_header_pos_hifi
!
subroutine fits_convert_header_spe_hifi(fits,buffer,colnum,nchan,obs,error)
  use phys_const
  use gbl_constant
  use gbl_message
  use classcore_interfaces, except_this=>fits_convert_header_spe_hifi
  use classfits_types
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Set up the spectroscopic section from obs%datav(:)
  !---------------------------------------------------------------------
  type(classfits_t), intent(inout) :: fits       !
  integer(kind=1),   intent(in)    :: buffer(:)  !
  integer(kind=4),   intent(in)    :: colnum     !
  integer(kind=4),   intent(in)    :: nchan      ! Number of channels
  type(observation), intent(inout) :: obs        !
  logical,           intent(inout) :: error      ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='FITS>CONVERT>HEADER>SPE>HIFI'
  real(kind=8) :: factor,lofreq
  character(len=20) :: veltype
  logical :: found
  !
  ! Correct to MHz if needed
  select case (fits%cols%desc%tunit(colnum))
  case ('MHz','')  ! Blank == MHz
    factor = 1.d0
  case ('GHz')
    factor = 1.d3
  case default
    call class_message(seve%e,rname,'FREQ unit is not supported: '//fits%cols%desc%tunit(colnum))
    error = .true.
    return
  end select
  !
  obs%cnchan = nchan  ! Needed by model_obs_fillgaps
  if (model_X_isregular(obs%datav,nchan,1e-3)) then
    ! X axis is regularly sampled
    continue
    !
  elseif (model_obs_fillgaps(obs,1.d-3)) then
    ! We could convert it to a regular axis just by filling gaps
    continue
    !
  else
    ! If we do want to support irregular axes, let's do it cleanly in the header.
    ! Disable for now.
    call class_message(seve%e,rname,'Spectrum is irregularly sampled')
    error = .true.
    return
    !
    call class_message(seve%w,rname,'Spectrum is irregularly sampled, basic support only')
    obs%head%presec(class_sec_xcoo_id) = .true.
    obs%head%gen%xunit = a_freq  ! ZZZ is this a default?
    obs%datav(1:nchan) = obs%datav(1:nchan)*factor
  endif
  !
  ! If needed, revert the axes so that frequency is always increasing.
  ! NB: HCSS ensures that there is only one sideband per FITS file.
  if (obs%datav(1).gt.obs%datav(nchan)) then
    obs%datav(1:nchan) = obs%datav(nchan:1:-1)
    obs%data1(1:nchan) = obs%data1(nchan:1:-1)
  endif
  !
  ! Get LO frequency
  call fits_convert_header_lofreq_hifi(fits,buffer,lofreq,error)
  if (error) return
  !
  ! Spectrum is (now) regularly sampled
  obs%head%presec(class_sec_xcoo_id) = .false.
  obs%head%spe%nchan   = nchan
  ! Reference channel: use arbitrarily middle of the range. If Nchan is
  ! even, round to upper value (same as HiClass). NB: tests above ensure
  ! that the obs%datav is regularly sampled.
  obs%head%spe%rchan   = ceiling((obs%head%spe%nchan+1)/2.d0)  ! Integer rounding
  obs%head%spe%restf   = obs%datav(nint(obs%head%spe%rchan))*factor
  obs%head%spe%doppler = 0.d0
  obs%head%spe%fres    = (obs%datav(2)-obs%datav(1))*factor
  obs%head%spe%vres    = -clight_kms*obs%head%spe%fres/obs%head%spe%restf
  obs%head%spe%voff    = 0.d0
  if (lofreq.eq.0.d0) then
    obs%head%spe%image = image_null
  else
    obs%head%spe%image = 2.d3*lofreq-obs%head%spe%restf
  endif
  !
  ! Velocity type. NB: 'freqFrame' is a metacard pointing to 'SPECSYS' card.
  ! Should we use SPECSYS directly?
  call fits_get_header_metacard(fits,'freqFrame',veltype,found,error)
  if (error)  return
  if (.not.found)  then
    call class_message(seve%e,rname,'Missing keyword freqFrame')
    error = .true.
    return
  endif
  select case (veltype)
  case ('LSRk')
    obs%head%spe%vtype = vel_lsr
  case ('source')
    call fits_warning_add(fits%warn,  &
      'Velocity type ''source'' not supported, R%HEAD%SPE%VTYPE defaults to unknown',error)
    if (error)  return
    obs%head%spe%vtype = vel_unk
  case default
    call class_message(seve%w,rname,  &
      'Velocity type '''//trim(veltype)//''' not recognized')
    error = .true.
    return
  end select
  !
  ! Line name
  call fits_convert_header_line_hifi(fits,lofreq,obs%head%spe%line,error)
  if (error)  return
  !
  ! Set bad value to class_bad and patch NaN
  call modify_blanking(obs,class_bad)
  !
end subroutine fits_convert_header_spe_hifi
!
subroutine fits_convert_header_lofreq_hifi(fits,buffer,lofreq,error)
  use classcore_interfaces, except_this=>fits_convert_header_lofreq_hifi
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private
  ! Get the LO frequency from FITS header (zero-sized buffer) or from
  ! binary columns (non-zero buffer)
  !---------------------------------------------------------------------
  type(classfits_t), intent(inout) :: fits       !
  integer(kind=1),   intent(in)    :: buffer(:)  !
  real(kind=8),      intent(out)   :: lofreq     !
  logical,           intent(inout) :: error      !
  ! Local
  logical :: found
  character(len=8) :: ftype
  !
  lofreq = 0.d0  ! Default if not found
  call fits_get_metacard_or_column(fits,buffer,'LoFrequency',lofreq,found,error)
  if (error) return
  if (.not.found) then
    call fits_get_metacard_or_column(fits,buffer,'LoFrequency_measured',lofreq,found,error)
    if (error) return
  endif
  !
  if (size(buffer).le.0) then
    ftype = 'Metacards'
  else
    ftype = 'Columns'
  endif
  call fits_warn_missing(fits%warn,ftype,'LoFrequency or LoFrequency_measured',  &
    'LO frequency',lofreq,found,error)
  if (error)  return
  !
end subroutine fits_convert_header_lofreq_hifi
!
subroutine fits_convert_header_line_hifi(fits,lofrequency,line,error)
  use classfits_types
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>fits_convert_header_line_hifi
  !---------------------------------------------------------------------
  ! @ private
  !   Set up the LINE name. 2 possibilities:
  ! - according to the input LO frequency, of the form: "1234.567_XSB"
  ! - or "DECON_SSB" for a deconvolved spectrum
  !   HIFI specific.
  !---------------------------------------------------------------------
  type(classfits_t), intent(inout) :: fits         ! Unit header
  real(kind=8),      intent(in)    :: lofrequency  ! [GHz]
  character(len=*),  intent(inout) :: line         !
  logical,           intent(inout) :: error        ! Logical error flag
  ! Local
  character(len=80) :: obs_mode,class___
  integer(kind=4) :: ic
  logical :: found
  !
  ! Translate cards "OBS_MODE" and "CLASS___"
  obs_mode = ''
  call fits_get_header_card(fits,'OBS_MODE',obs_mode,found,error)
  if (error)  return
  class___ = ''
  call fits_get_header_card(fits,'CLASS___',class___,found,error)
  if (error)  return
  !
  if (index(obs_mode,'SScan')     .ne.0 .and.  &
      index(class___,'Spectrum1d').ne.0) then
    line = 'DECON_SSB'
    return
  endif
  !
  write(line,'(F8.3)') lofrequency
  ! Patch for leading zeroes
  do ic=1,8
    if (line(ic:ic).eq.' ') then
      line(ic:ic) = '0'
    else
      exit
    endif
  enddo
  !
  line(9:9) = '_'
  !
  call fits_get_header_metacard(fits,'sideband',line(10:12),found,error)
  if (error)  return
  if (.not.found)  line(10:12) = 'GHZ'
  !
  call sic_upper(line)
  !
end subroutine fits_convert_header_line_hifi
!
subroutine fits_convert_header_swi_hifi(fits,buffer,obs,error)
  use gbl_constant
  use classcore_interfaces, except_this=>fits_convert_header_swi_hifi
  use classfits_types
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Set up the Frequency SWitch section
  !---------------------------------------------------------------------
  type(classfits_t), intent(inout) :: fits       !
  integer(kind=1),   intent(in)    :: buffer(:)  !
  type(observation), intent(inout) :: obs        !
  logical,           intent(inout) :: error      ! Logical error flag
  ! Local
  logical :: found,isfolded
  integer(kind=4) :: iphase
  integer(kind=4), parameter :: nphase=2
  real(kind=8) :: lothrowGHz,lothrowHz,throw(nphase)
  real(kind=4), parameter :: weight(nphase) = (/ +0.5 , -0.5 /)
  character(len=80) :: obs_mode
  !
  lothrowGHz = 0.d0  ! Default if not found
  call fits_get_metacard_or_column(fits,buffer,'LoThrow',lothrowGHz,found,error)
  if (error) return
  if (.not.found) then
    call fits_get_metacard_or_column(fits,buffer,'loThrow',lothrowGHz,found,error)
    if (error) return
  endif
  !
  if (lothrowGHz.eq.0.d0)  return  ! No switching section
  !
  obs_mode = ''
  call fits_get_header_card(fits,'OBS_MODE',obs_mode,found,error)
  if (error)  return
  if (.not.found)  return
  if (index(obs_mode,'FSwitch').eq.0)  return
  !
  ! Ok, this is Frequency Switch data
  !
  ! Round lothrow to nearest Hz
  lothrowHz = real(nint(lothrowGHz*1d9),kind=8)  ! GHz to Hz, rounded
  throw(1) = 0.d0
  throw(2) = lothrowHz*1.d-6  ! Hz to MHz
  !
  obs%head%presec(class_sec_swi_id) = .true.
  obs%head%swi%nphas = nphase
  do iphase=1,nphase
    obs%head%swi%decal(iphase)  = throw(iphase)
    obs%head%swi%duree(iphase)  = 1.
    obs%head%swi%poids(iphase)  = weight(iphase)
    obs%head%swi%ldecal(iphase) = 0.
    obs%head%swi%bdecal(iphase) = 0.
  enddo
  !
  isfolded = .false.  ! Default if not present
  call fits_get_header_metacard(fits,'isFolded',isfolded,found,error)
  if (error)  return
  if (isfolded) then
    obs%head%swi%swmod = mod_fold
  else
    obs%head%swi%swmod = mod_freq
  endif
  !
end subroutine fits_convert_header_swi_hifi
!
subroutine fits_convert_header_cal_hifi(fits,hotcold,obs,error)
  use gbl_constant
  use gbl_message
  use classcore_interfaces, except_this=>fits_convert_header_cal_hifi
  use classfits_types
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Set up the Calibration section
  !---------------------------------------------------------------------
  type(classfits_t), intent(inout) :: fits        !
  real(kind=8),      intent(in)    :: hotcold(2)  ! [K]
  type(observation), intent(inout) :: obs         !
  logical,           intent(inout) :: error       ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='FITS>CONVERT>HEADER>CAL>HIFI'
  character(len=20) :: sideband,sbgain,tscale
  real(kind=4) :: foeff,beeff
  logical :: found
  !
  ! Feff, Beff
  call fits_get_header_metacard(fits,'temperatureScale',tscale,found,error)
  if (error)  return
  if (.not.found)  return  ! No Calibration section
  !
  foeff = 0.0
  call fits_get_header_metacard(fits,'forwardEff',foeff,found,error)
  if (error)  return
  beeff = 0.0
  call fits_get_header_metacard(fits,'beamEff',beeff,found,error)
  if (error)  return
  select case (tscale)
  case ('T_A*')
    obs%head%cal%beeff = foeff
    obs%head%cal%foeff = foeff
  case ('T_MB')
    obs%head%cal%beeff = beeff
    obs%head%cal%foeff = foeff
  case default
    call class_message(seve%w,rname,  &
      'Temperature Scale '''//trim(tscale)//''' not supported')
    return
  end select
  !
  ! Is this LSB or USB? This assumes only one sideband per binary table
  sideband = ''
  call fits_get_header_metacard(fits,'sideband',sideband,found,error)
  if (error)  return
  call sic_lower(sideband)
  !
  ! GAINI
  sbgain = trim(sideband)//'Gain'
  obs%head%cal%gaini = 0.0
  call fits_get_header_metacard(fits,sbgain,obs%head%cal%gaini,found,error,'R%HEAD%CAL%GAINI')
  if (error)  return
  !
  ! HOT + COLD
  obs%head%cal%tchop = hotcold(1)
  obs%head%cal%tcold = hotcold(2)
  !
  obs%head%presec(class_sec_cal_id) = .true.
  !
end subroutine fits_convert_header_cal_hifi
!
subroutine fits_convert_header_her_hifi(fits,buffer,obs,error)
  use gbl_message
  use phys_const
  use classcore_interfaces, except_this=>fits_convert_header_her_hifi
  use classfits_types
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Set up the Herschel section
  !---------------------------------------------------------------------
  type(classfits_t), intent(inout) :: fits       !
  integer(kind=1),   intent(in)    :: buffer(:)  !
  type(observation), intent(inout) :: obs        !
  logical,           intent(inout) :: error      ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='FITS>CONVERT>HEADER>HER>HIFI'
  logical :: found
  character(len=3) :: sideband
  character(len=8) :: level
  !
  obs%head%her%obsid = 0
  call fits_get_header_card(fits,'OBS_ID',obs%head%her%obsid,found,error,'R%HEAD%HER%OBSID')
  if (error)  return
  !
  obs%head%her%operday = 0
  call fits_get_header_card(fits,'ODNUMBER',obs%head%her%operday,found,error,'R%HEAD%HER%OPERDAY')
  if (error)  return
  !
  obs%head%her%hcssver = ''
  call fits_get_header_card(fits,'CREATOR',obs%head%her%hcssver,found,error,'R%HEAD%HER%HCSSVER')
  if (error)  return
  !
  obs%head%her%calver = ''
  call fits_get_header_card(fits,'CALVERS',obs%head%her%calver,found,error,'R%HEAD%HER%CALVER')
  if (error)  return
  !
  obs%head%her%etamb = 0.
  call fits_get_header_card(fits,'ETAMB',obs%head%her%etamb,found,error,'R%HEAD%HER%ETAMB')
  if (error)  return
  !
  obs%head%her%etal = 0.
  call fits_get_header_card(fits,'ETAL',obs%head%her%etal,found,error,'R%HEAD%HER%ETAL')
  if (error)  return
  !
  obs%head%her%etaa = 0.
  call fits_get_header_card(fits,'ETAA',obs%head%her%etaa,found,error,'R%HEAD%HER%ETAA')
  if (error)  return
  !
  obs%head%her%hpbw = 0.
  call fits_get_header_card(fits,'HPBW',obs%head%her%hpbw,found,error,'R%HEAD%HER%HPBW')
  if (error)  return
  obs%head%her%hpbw = obs%head%her%hpbw*rad_per_sec  ! arcsec to rad
  !
  obs%head%her%tempscal = ''
  call fits_get_header_card(fits,'TEMPSCAL',obs%head%her%tempscal,found,error,'R%HEAD%HER%TEMPSCAL')
  if (error)  return
  !
  call fits_convert_header_vzinfo_hifi(fits,obs%head%her%vinfo,obs%head%her%zinfo,error)
  if (error)  return
  !
  obs%head%her%posangle = 0.d0
  call fits_get_header_card(fits,'POSANGLE',obs%head%her%posangle,found,error,'R%HEAD%HER%POSANGLE')
  if (error)  return
  obs%head%her%posangle = obs%head%her%posangle*rad_per_deg  ! Deg to rad
  !
  obs%head%her%lodopave = 0.d0
  call fits_get_header_card(fits,'LODOPPAV',obs%head%her%lodopave,found,error)
  if (error)  return
  if (.not.found) then
    call fits_warn_missing(fits%warn,'Card','LODOPPAV','R%HEAD%HER%LODOPAVE','LO frequency',found,error)
    if (error)  return
    call fits_convert_header_lofreq_hifi(fits,buffer,obs%head%her%lodopave,error)
    if (error)  return
  endif
  obs%head%her%lodopave = obs%head%her%lodopave*1d3  ! GHz to MHz
  !
  ! Is this LSB or USB? This assumes only one sideband per binary table
  sideband = ''
  call fits_get_header_metacard(fits,'sideband',sideband,found,error)
  if (error)  return
  call sic_lower(sideband)
  !
  obs%head%her%gim0 = 0.
  call fits_get_header_metacard(fits,sideband//'Gain_0',obs%head%her%gim0,found,error,'R%HEAD%HER%GIM0')
  if (error)  return
  !
  obs%head%her%gim1 = 0.
  call fits_get_header_metacard(fits,sideband//'Gain_1',obs%head%her%gim1,found,error,'R%HEAD%HER%GIM1')
  if (error)  return
  !
  obs%head%her%gim2 = 0.
  call fits_get_header_metacard(fits,sideband//'Gain_2',obs%head%her%gim2,found,error,'R%HEAD%HER%GIM2')
  if (error)  return
  !
  obs%head%her%gim3 = 0.
  call fits_get_header_metacard(fits,sideband//'Gain_3',obs%head%her%gim3,found,error,'R%HEAD%HER%GIM3')
  if (error)  return
  !
  obs%head%her%instrument = ''
  call fits_get_header_card(fits,'INSTRUME',obs%head%her%instrument,found,error,'R%HEAD%HER%INSTRUMENT')
  if (error)  return
  !
  obs%head%her%obsmode = ''
  call fits_get_header_card(fits,'OBS_MODE',obs%head%her%obsmode,found,error,'R%HEAD%HER%OBSMODE')
  if (error)  return
  !
  obs%head%her%proposal = ''
  call fits_get_header_card(fits,'PROPOSAL',obs%head%her%proposal,found,error,'R%HEAD%HER%PROPOSAL')
  if (error)  return
  !
  obs%head%her%aor = ''
  call fits_get_header_card(fits,'AOR',obs%head%her%aor,found,error,'R%HEAD%HER%AOR')
  if (error)  return
  !
  obs%head%her%level = 0.
  call fits_get_header_card(fits,'LEVEL',level,found,error,'R%HEAD%HER%LEVEL')
  if (error)  return
  read(level,*)  obs%head%her%level  ! String to numeric
  obs%head%her%level = obs%head%her%level/10.
  !
  obs%head%her%reflam = 0.d0
  call fits_get_header_card(fits,'RAOFF',obs%head%her%reflam,found,error,'R%HEAD%HER%REFLAM')
  if (error)  return
  obs%head%her%reflam = obs%head%her%reflam*rad_per_deg  ! Deg to rad
  !
  obs%head%her%refbet = 0.d0
  call fits_get_header_card(fits,'DECOFF',obs%head%her%refbet,found,error,'R%HEAD%HER%REFBET')
  if (error)  return
  obs%head%her%refbet = obs%head%her%refbet*rad_per_deg  ! Deg to rad
  !
  obs%head%her%hifavelam = 0.d0
  call fits_get_metacard_or_column(fits,buffer,'longitude_cmd',obs%head%her%hifavelam,found,error,'R%HEAD%HER%HIFAVELAM')
  if (error) return
  obs%head%her%hifavelam = obs%head%her%hifavelam*rad_per_deg  ! Deg to rad
  !
  obs%head%her%hifavebet = 0.d0
  call fits_get_metacard_or_column(fits,buffer,'latitude_cmd',obs%head%her%hifavebet,found,error,'R%HEAD%HER%HIFAVEBET')
  if (error) return
  obs%head%her%hifavebet = obs%head%her%hifavebet*rad_per_deg  ! Deg to rad
  !
  obs%head%her%mixercurh = 0.
  call fits_get_metacard_or_column(fits,buffer,'MJC_Hor',obs%head%her%mixercurh,found,error,'R%HEAD%HER%MIXERCURH')
  if (error) return
  !
  obs%head%her%mixercurv = 0.
  call fits_get_metacard_or_column(fits,buffer,'MJC_Ver',obs%head%her%mixercurv,found,error,'R%HEAD%HER%MIXERCURV')
  if (error) return
  !
  obs%head%her%dateobs = ''
  call fits_get_header_card(fits,'DATE-OBS',obs%head%her%dateobs,found,error,'R%HEAD%HER%DATEOBS')
  if (error)  return
  !
  obs%head%her%dateend = ''
  call fits_get_header_card(fits,'DATE-END',obs%head%her%dateend,found,error,'R%HEAD%HER%DATEEND')
  if (error)  return
  !
  obs%head%her%datehcss = ''
  call fits_get_header_card(fits,'DATE',obs%head%her%datehcss,found,error,'R%HEAD%HER%DATEHCSS')
  if (error)  return
  !
  obs%head%presec(class_sec_her_id) = .true.
  !
end subroutine fits_convert_header_her_hifi
!
subroutine fits_convert_header_vzinfo_hifi(fits,vinfo,zinfo,error)
  use gbl_constant
  use gbl_message
  use classcore_interfaces, except_this=>fits_convert_header_vzinfo_hifi
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private
  ! Set up the elements R%HEAD%HER%VINFO (informative source velocity
  ! in LSR) and R%HEAD%HER%ZINFO (Informative target redshift)
  !---------------------------------------------------------------------
  type(classfits_t), intent(inout) :: fits
  real(kind=4),      intent(out)   :: vinfo
  real(kind=4),      intent(out)   :: zinfo
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='FITS>CONVERT>HEADER>VZINFO>HIFI'
  character(len=20) :: veltype
  character(len=8) :: redshft
  real(kind=4) :: vel_or_dop
  logical :: found,found_redshft,found_vlsr
  !
  call fits_get_header_metacard(fits,'freqFrame',veltype,found,error)
  if (error)  return
  if (.not.found) then
    call class_message(seve%e,rname,'Missing keyword freqFrame')
    error = .true.
    return
  endif
  !
  if (veltype.eq.'source') then
    vinfo = 0.
    zinfo = 0.
    return
  elseif (veltype.ne.'LSRk') then
    call class_message(seve%e,rname,'Velocity type '//trim(veltype)//' not supported')
    error = .true.
    return
  endif
  ! From this stage we know we are in LSRk
  !
  redshft = 'radio'
  call fits_get_header_card(fits,'REDSHFT',redshft,found_redshft,error)
  if (error)  return
  !
  vel_or_dop = 0.0
  call fits_get_header_metacard(fits,'vlsr',vel_or_dop,found_vlsr,error)
  if (error)  return
  !
  if (.not.found_redshft .or. .not.found_vlsr)  then
    call fits_warning_add(fits%warn,'Card REDSHFT and/or metacard ''vlsr'' not found, '//  &
      'R%HEAD%HER%VINFO and R%HEAD%HER%ZINFO default to 0',error)
    if (error)  return
  endif
  !
  ! According to Herschel-FITS header, metacard 'vlsr' is:
  ! "Proposal target redshift value (km/s if redshiftType is optical or radio)"
  if (redshft.eq.'optical' .or. redshft.eq.'radio') then
    ! A velocity
    vinfo = vel_or_dop
    zinfo = 0.
  else
    ! A Doppler
    vinfo = 0.
    zinfo = vel_or_dop
  endif
  !
end subroutine fits_convert_header_vzinfo_hifi
!
subroutine fits_convert_header_assoc_hifi(fits,buffer,nchan,obs,aablank,aaline,error)
  use classcore_interfaces, except_this=>fits_convert_header_assoc_hifi
  use classfits_types
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Set up the Associated Arrays section
  !---------------------------------------------------------------------
  type(classfits_t), intent(inout) :: fits        !
  integer(kind=1),   intent(in)    :: buffer(:)   !
  integer(kind=4),   intent(in)    :: nchan       !
  type(observation), intent(inout) :: obs         !
  integer(kind=4),   pointer       :: aablank(:)  ! Pointer to Associated Array set in return
  integer(kind=4),   pointer       :: aaline(:)   ! Pointer to Associated Array set in return
  logical,           intent(inout) :: error       ! Logical error flag
  ! Local
  integer(kind=4) :: rowflag
  logical :: found
  !
  aaline => null()
  aablank => null()
  call rzero_assoc(obs)  ! Forget previous Associated Arrays (efficient because
  !                      ! they are not deallocated)
  !
  ! Allocate Associated Arrays
  obs%head%spe%nchan = nchan  ! Because it is needed by class_assoc_add
  call class_assoc_add(obs,'BLANKED',aablank,error)
  if (error)  return
  call class_assoc_add(obs,'LINE',aaline,error)
  if (error)  return
  !
  ! Initialize Associated Arrays with the row flag
  rowflag = 0  ! Default if not found
  call fits_get_metacard_or_column(fits,buffer,'rowflag',rowflag,found,error,'row flags')
  if (error) return
  !
  aablank(:) = fits_convert_flag_hifi(1,1,rowflag)
  aaline(:) = fits_convert_flag_hifi(1,2,rowflag)
  !
end subroutine fits_convert_header_assoc_hifi
!
subroutine fits_warning_hifi(fits,error)
  use classcore_interfaces, except_this=>fits_warning_hifi
  use classfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Add the HIFI-specific warning
  !---------------------------------------------------------------------
  type(classfits_t), intent(inout) :: fits
  logical,           intent(inout) :: error
  ! Local
  ! Versions '14.*' are the highest versions distributed in the Herschel
  ! archive. User may also be able to produce versions '15'.
  character(len=*), parameter :: maxversion='14'
  !
  ! If there were no warning, there is no advice to give to the user
  if (fits%warn%n.le.0)  return
  !
  ! 1 warning or more are already stored.
  call fits_warning_add(fits%warn,  &
    '==> One or more meta-data missing, default values used in CLASS',error)
  if (error)  return
  !
  ! Add the Herschel-HIFI disclaimer
  if (llt(fits%version,maxversion)) then
    call fits_warning_add(fits%warn,  &
      '==> We recommend that you download the latest FITS version from the Herschel Science Archive',error)
    if (error)  return
  endif
  !
end subroutine fits_warning_hifi
!
!-----------------------------------------------------------------------
!
subroutine fits_chopbuf_1chan_hifi(fits,row,lr,ifreq,iflux,iflag,obs,aablank,  &
  aaline,ichan,error)
  use gbl_format
  use classcore_interfaces, except_this=>fits_chopbuf_1chan_hifi
  use classfits_types
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  ! Chop the row buffer into individual channel elements.
  ! Granularity is at the channel level.
  !  HIFI-specific
  !----------------------------------------------------------------------
  type(classfits_t), intent(in)    :: fits        !
  integer(kind=4),   intent(in)    :: lr          ! Row buffer length
  integer(kind=1),   intent(in)    :: row(lr)     ! Row buffer
  integer(kind=4),   intent(in)    :: ifreq       ! Frequency column
  integer(kind=4),   intent(in)    :: iflux       ! Flux column
  integer(kind=4),   intent(in)    :: iflag       ! Flag column
  type(observation), intent(inout) :: obs         !
  integer(kind=4),   intent(inout) :: aablank(:)  ! BLANKED array
  integer(kind=4),   intent(inout) :: aaline(:)   ! LINE array
  integer(kind=4),   intent(in)    :: ichan       ! Channel to be updated
  logical,           intent(inout) :: error       ! Logical error flag
  ! Local
  integer(kind=4) :: i4
  real(kind=8) :: r8
  !
  ! Frequency
  call get_item(r8,1,fmt_r8,row(fits%cols%desc%addr(ifreq)),  &
                fits%cols%desc%fmt(ifreq),error)
  if (error)  return
  obs%datav(ichan) = r8
  !
  ! Flux
  call get_item(r8,1,fmt_r8,row(fits%cols%desc%addr(iflux)),  &
                fits%cols%desc%fmt(iflux),error)
  if (error)  return
  obs%data1(ichan) = r8
  !
  if (iflag.gt.0) then  ! Can be absent from the table
    ! Flag
    call get_item(i4,1,fmt_i4,row(fits%cols%desc%addr(iflag)),  &
                  fits%cols%desc%fmt(iflag),error)
    if (error)  return
    if (aablank(ichan).eq.0) then  ! Not yet blanked
      aablank(ichan) = fits_convert_flag_hifi(2,1,i4)
    endif
    if (aaline(ichan).eq.0) then  ! Not yet in a line window
      aaline(ichan) = fits_convert_flag_hifi(2,2,i4)
    endif
  endif
  !
end subroutine fits_chopbuf_1chan_hifi
!
function fits_convert_flag_hifi(ikind,okind,flag)
  !---------------------------------------------------------------------
  ! @ private
  !  Convert a:
  !   ikind=1: row flag
  !   ikind=2: channel flag
  ! into a:
  !   okind=1: blank flag
  !   okind=2: window flag
  !  HIFI specific
  !---------------------------------------------------------------------
  integer(kind=4) :: fits_convert_flag_hifi  ! Function value on return
  integer(kind=4), intent(in) :: ikind  ! Input kind of flag
  integer(kind=4), intent(in) :: okind  ! Output kind of flag
  integer(kind=4), intent(in) :: flag   ! The flag value
  ! Local
  ! Bit flag positions
  integer(kind=4) :: rowflag_ignore_data     = 20
  integer(kind=4) :: chanflag_spur_candidate =  7
  integer(kind=4) :: chanflag_line           = 28
  integer(kind=4) :: chanflag_bright_line    = 29
  integer(kind=4) :: chanflag_ignore_data    = 30
  !
  fits_convert_flag_hifi = 0
  !
  if (ikind.eq.1) then ! A row flag
    if (okind.eq.1) then  ! A blank flag
      if (btest(flag,rowflag_ignore_data))  fits_convert_flag_hifi = 1
    elseif (okind.eq.2) then  ! A window flag
      continue  ! No known flag
    endif
  elseif (ikind.eq.2) then  ! A channel flag
    if (okind.eq.1) then  ! A blank flag
      if (btest(flag,chanflag_spur_candidate))  fits_convert_flag_hifi = 1
      if (btest(flag,chanflag_ignore_data))     fits_convert_flag_hifi = 1
    elseif (okind.eq.2) then  ! A window flag
      if (btest(flag,chanflag_line))         fits_convert_flag_hifi = 1
      if (btest(flag,chanflag_bright_line))  fits_convert_flag_hifi = 1
    endif
  endif
  !
end function fits_convert_flag_hifi
