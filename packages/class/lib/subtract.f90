subroutine class_subtract_comm(set,line,r,t,error)
  use gbl_message
  use classcore_interfaces, except_this=>class_subtract_comm
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! Support routine for command
  !   EXPERIMENTAL\SUBTRACT
  ! Compute T-R
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  character(len=*),    intent(in)    :: line   ! Input command line
  type(observation),   intent(inout) :: r,t    !
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='SUBTRACT'
  type(observation) :: diff
  !
  if (r%head%xnum.eq.0) then
    call class_message(seve%e,rname,'No R spectrum in memory')
    error = .true.
    return
  endif
  if (t%head%xnum.eq.0) then
    call class_message(seve%e,rname,'No T spectrum in memory')
    error = .true.
    return
  endif
  !
  call init_obs(diff)
  !
  call class_subtract(set,t,r,diff,error)
  if (.not.error) then
    ! Push the stack and copy DIFF to R
    call copy_obs(r,t,error)
    call copy_obs(diff,r,error)
    call newdat(set,r,error)
    call newdat_assoc(set,r,error)
    call newdat_user(set,r,error)
  endif
  !
  call free_obs(diff)
  !
end subroutine class_subtract_comm
!
subroutine class_subtract(set,obs1,obs2,diff,error)
  use classcore_interfaces, except_this=>class_subtract
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! Perform obs1-obs2 and return the result in the output observation
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(observation),   intent(in)    :: obs1   !
  type(observation),   intent(in)    :: obs2   !
  type(observation),   intent(inout) :: diff   !
  logical,             intent(inout) :: error  !
  !
  call class_subtract_cons(set,obs1%head,obs2%head,error)
  if (error)  return
  !
  call class_subtract_header(obs1%head,obs2%head,diff%head,error)
  if (error)  return
  !
  call class_subtract_data(obs1,obs2,diff,error)
  if (error)  return
  !
end subroutine class_subtract
!
subroutine class_subtract_cons(set,h1,h2,error)
  use gbl_message
  use classcore_interfaces, except_this=>class_subtract_cons
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! Check the consistency of the 2 input observations
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(header),        intent(in)    :: h1     ! Header of 1st obs
  type(header),        intent(in)    :: h2     ! Header of 2nd obs
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='SUBTRACT>CONSISTENCY'
  type(consistency_t) :: cons
  !
  ! Select what will be checked according to user input
  call consistency_defaults(set,cons)
  cons%gen%check = .false.
  cons%sou%check = .false.
  cons%pos%check = .false.
  cons%off%check = .false.
  cons%lin%check = .false.
  cons%spe%check = .true.
  cons%cal%check = .false.
  !
  ! Set checking tolerances
  call consistency_tole(h1,cons)
  !
  ! Check consistency of the 2 headers
  call observation_consistency_check(set,h1,h2,cons)
  if (cons%spe%check .and. cons%spe%prob) then
    call class_message(seve%e,rname,'Spectroscopic axes are inconsistent')
    error = .true.
    return
  endif
  !
end subroutine class_subtract_cons
!
subroutine class_subtract_header(h1,h2,ho,error)
  use classcore_interfaces, except_this=>class_subtract_header
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Compute the header of the difference from the input header
  ! NOT IMPLEMENTED! Use 1st header instead!
  !---------------------------------------------------------------------
  type(header), intent(in)    :: h1     ! Header of 1st obs
  type(header), intent(in)    :: h2     ! Header of 2nd obs
  type(header), intent(inout) :: ho     ! Output header
  logical,      intent(inout) :: error  ! Logical error flag
  !
  call copy_header(h1,ho)
  !
end subroutine class_subtract_header
!
subroutine class_subtract_data(obs1,obs2,diff,error)
  use gbl_constant
  use gbl_message
  use classcore_interfaces, except_this=>class_subtract_data
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for mrtcal)
  ! Data difference. Only aligned spectra are implemented!
  !---------------------------------------------------------------------
  type(observation), intent(in)    :: obs1   !
  type(observation), intent(in)    :: obs2   !
  type(observation), intent(inout) :: diff   !
  logical,           intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='SUBTRACT>DATA'
  integer(kind=4) :: ndata
  !
  ! Safe guard
  if (obs1%head%gen%kind.eq.kind_spec) then
    ndata = diff%head%spe%nchan
    error = ndata.ne.obs1%head%spe%nchan .or. ndata.ne.obs2%head%spe%nchan
  else
    ndata = diff%head%dri%npoin
    error = ndata.ne.obs1%head%dri%npoin .or. ndata.ne.obs2%head%dri%npoin
  endif
  if (error) then
    call class_message(seve%e,rname,'Inconsistent number of data points')
    error = .true.
    return
  endif
  !
  call reallocate_obs(diff,ndata,error)
  if (error)  return
  !
  diff%data1(1:ndata) = obs1%data1(1:ndata) - obs2%data1(1:ndata)
  !
end subroutine class_subtract_data
