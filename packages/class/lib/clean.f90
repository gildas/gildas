!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine class_fill(set,line,r,error,user_function)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_fill
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! CLASS ANALYSE Support routine for command
  !   FILL s1 e1 [[s2 e2] ...]  [/INTER] [/NOISE [sigma]] [/BLANK [bval]]
  !
  ! * "Clean" some specified windows, i.e. interpolate them
  !   from first and last channels.
  ! * Units are the current units
  ! * Possibility to specify several windows
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  character(len=*),    intent(in)    :: line           ! Command line
  type(observation),   intent(inout) :: r              !
  logical,             intent(inout) :: error          ! Logical error flag
  logical,             external      :: user_function  ! Header handling
  ! Local
  character(len=*), parameter :: rname='CLEAN'
  logical :: donoise,doblank,dointer
  integer(kind=4) :: i,narg,j,nw,ier
  real(kind=8), allocatable :: wmin(:),wmax(:)
  real(kind=4) :: sigma,bval
  !
  ! Check if spectrum
  if (r%head%xnum.eq.0) then
     call class_message(seve%e,rname,'No spectrum in memory')
     error = .true.
     return
  endif
  !
  ! Decode input line
  !
  ! Number of windows = narg/2
  narg = sic_narg(0)
  if (mod(narg,2).ne.0) then
     call class_message(seve%e,rname,'Odd number of arguments')
     error = .true.
     return
  endif
  nw = narg/2
  allocate (wmin(nw),wmax(nw),stat=ier)
  if (failed_allocate(rname,"wmin,wmax",ier,error))  return
  wmin(:) = 0.
  wmax(:) = 0.
  j = 1
  do i=1,nw
     call sic_r8 (line,0,j,wmin(i),.true.,error)
     if (error) goto 999
     j = j+1
     call sic_r8 (line,0,j,wmax(i),.true.,error)
     if (error) goto 999
     j = j+1
  enddo
  !
  ! /INTER is default unless /NOISE is present
  dointer = .true.
  donoise = sic_present(2,0)
  if (donoise) then
     dointer = sic_present(1,0)
  endif
  doblank = sic_present(3,0)
  if (doblank) dointer = .false.
  !
  sigma = 0.0
  bval  = -1.e7
  if (donoise) then
     call sic_r4(line,2,1,sigma,.false.,error)
     if (error) goto 999
  else if (doblank) then
     call sic_r4(line,3,1,bval,.false.,error)
     if (error) goto 999
  endif
  !
  if (set%unitx(1).eq.'F') then
     do i=1,nw
        wmin(i) = wmin(i)-r%head%spe%restf
        wmax(i) = wmax(i)-r%head%spe%restf
     enddo
  else if (set%unitx(1).eq.'I') then
     do i=1,nw
        wmin(i) = r%head%spe%image-wmin(i)
        wmax(i) = r%head%spe%image-wmax(i)
     enddo
  endif
  !
  if (donoise.and..not.dointer) then
     call fill_noise(set,r,wmin,wmax,nw,sigma,.false.,error)
  else if (donoise.and.dointer) then
     call fill_inter(set,r,wmin,wmax,nw)
     call fill_noise(set,r,wmin,wmax,nw,sigma,.true.,error)
  else if (doblank) then
     call fill_blank(set,r,wmin,wmax,nw,bval)
  else
     call fill_inter(set,r,wmin,wmax,nw)
  endif
  if (error)  goto 999
  !
  call newdat(set,r,error)
  !
999 continue
  deallocate (wmin,wmax)
  !
end subroutine class_fill
!
subroutine fill_noise(set,obs,wmin,wmax,nw,sigma,dointer,error)
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>fill_noise
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command:
  !   FILL /NOISE [sigma]
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set       !
  type(observation),   intent(inout) :: obs       ! Observation to be filled
  integer(kind=4),     intent(in)    :: nw        ! Number of windows
  real(kind=8),        intent(in)    :: wmin(nw)  ! Lower bounds of velo windows
  real(kind=8),        intent(in)    :: wmax(nw)  ! Upper bounds of velo windows
  real(kind=4),        intent(inout) :: sigma     ! RMS for Gaussian noise
  logical,             intent(in)    :: dointer   ! Add the noise to previous interpolation
  logical,             intent(inout) :: error     ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='FILL_NOISE'
  character(len=message_length) :: mess
  integer(kind=4) :: iw,i,imin,imax
  real(kind=8) :: i1,i2
  !
  ! Set rms if Gaussian noise
  if (sigma.le.0.0) then
    call class_noise_guess(rname,obs,sigma,error)
    if (error)  return
  endif
  !
  ! Loop on windows
  do iw=1,nw
     ! Convert wmin wmax into pixel
     call abscissa_any2chan(set,obs,wmin(iw),i1)
     call abscissa_any2chan(set,obs,wmax(iw),i2)
     if (i1.lt.i2) then
       ! We use nint() below because channel #i covers the range [i-.5,i+.5]
       imin = max(1,nint(i1))
       imax = min(nint(i2),obs%cnchan)
     else
       imin = max(1,nint(i2))
       imax = min(nint(i1),obs%cnchan)
     endif
     ! Mask the corresponding area
     if (imin.gt.imax) then
        ! Range is off the spectrum limits
        write(mess,'(a,i3,a)') 'Window num.',iw,' ignored'
        call class_message(seve%i,rname,mess)
     else
        if (dointer) then
           do i = imin,imax
              obs%spectre(i) = obs%spectre(i) + rangau(sigma)
           enddo
        else
           do i = imin,imax
              obs%spectre(i) = rangau(sigma)
           enddo
        endif
     endif
  enddo
  !
end subroutine fill_noise
!
subroutine fill_blank(set,obs,wmin,wmax,nw,blank)
  use gbl_constant
  use gbl_message
  use classcore_interfaces, except_this=>fill_blank
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine for command FILL /BLANK [bval]
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set       !
  type(observation),   intent(inout) :: obs       ! Observation to be filled
  integer(kind=4),     intent(in)    :: nw        ! Number of windows
  real(kind=8),        intent(in)    :: wmin(nw)  ! Lower bounds of velo windows
  real(kind=8),        intent(in)    :: wmax(nw)  ! Upper bounds of velo windows
  real(kind=4),        intent(inout) :: blank     ! Blanking value
  ! Local
  character(len=*), parameter :: rname='FILL_BLANK'
  integer(kind=4) :: iw,i,imin,imax
  real(kind=8) :: i1,i2
  character(len=message_length) :: mess
  !
  ! Blanking value
  if (blank.eq.-1.e7) then
     if (obs%head%gen%kind.eq.kind_spec) then
        blank = obs%head%spe%bad
     else
        blank = obs%cbad
     endif
  endif
  write(mess,'(a,f12.3)') "Blanking value:",blank
  call class_message(seve%i,rname,mess)
  !
  ! Loop on windows
  do iw=1,nw
     ! Convert wmin wmax into pixel
     call abscissa_any2chan(set,obs,wmin(iw),i1)
     call abscissa_any2chan(set,obs,wmax(iw),i2)
     if (i1.lt.i2) then
       ! We use nint() below because channel #i covers the range [i-.5,i+.5]
       imin = max(1,nint(i1))
       imax = min(nint(i2),obs%cnchan)
     else
       imin = max(1,nint(i2))
       imax = min(nint(i1),obs%cnchan)
     endif
     ! Mask the corresponding area
     if (imin.gt.imax) then
        ! Range is off the spectrum limits
        write(mess,'(a,i3,a)') 'Window num.',iw,' ignored'
        call class_message(seve%i,rname,mess)
     else
        do i = imin,imax
           obs%spectre(i) = blank
        enddo
     endif
  enddo
  !
end subroutine fill_blank
!
subroutine fill_inter(set,obs,wmin,wmax,nw)
  use gbl_message
  use classcore_interfaces, except_this=>fill_inter
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine for command FILL /NOISE [sigma]
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set       !
  type(observation),   intent(inout) :: obs       ! Observation to be filled
  integer(kind=4),     intent(in)    :: nw        ! Number of windows
  real(kind=8),        intent(in)    :: wmin(nw)  ! Lower bounds of velo windows
  real(kind=8),        intent(in)    :: wmax(nw)  ! Upper bounds of velo windows
  ! Local
  character(len=*), parameter :: rname='FILL_INTER'
  integer(kind=4) :: iw,i,imin,imax
  real(kind=8) :: i1,i2
  real(kind=4) :: slope
  character(len=message_length) :: mess
  !
  ! Loop on windows
  do iw=1,nw
     ! Convert wmin wmax into pixel
     call abscissa_any2chan(set,obs,wmin(iw),i1)
     call abscissa_any2chan(set,obs,wmax(iw),i2)
     if (i1.lt.i2) then
       ! We use nint() below because channel #i covers the range [i-.5,i+.5]
       imin = max(1,nint(i1))
       imax = min(nint(i2),obs%cnchan)
     else
       imin = max(1,nint(i2))
       imax = min(nint(i1),obs%cnchan)
     endif
     ! Mask the corresponding area, with straight line interpolation
     if (imin.gt.imax) then
        ! Range is off the spectrum limits
        write(mess,'(a,i3,a)') 'Window num.',iw,' ignored'
        call class_message(seve%i,rname,mess)
     else
        slope = (obs%spectre(imax)-obs%spectre(imin))/(imax-imin)
        do i = imin+1,imax-1
           obs%spectre(i) = obs%spectre(imin) + (i-imin)*slope
        enddo
     endif
  enddo
  !
end subroutine fill_inter
