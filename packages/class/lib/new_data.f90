subroutine new_data (line,error)
  use gbl_format
  use gbl_message
  use gkernel_interfaces
  use classcore_interfaces, except_this=>new_data
  use class_index
  use class_common
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! CLASS Support routine for command
  !   NEW_DATA [Interval]
  !  Wait until new data is present in the input file. No update the
  ! input and current indexes.
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: line   !
  logical,          intent(out) :: error  !
  ! Local
  character(len=*), parameter :: rname='NEW_DATA'
  real(kind=4) :: interval
  integer(kind=4) :: errcount
  character(len=message_length) :: mess
  !
  if (.not.filein_opened(rname,error))  return
  !
  if (filein_isvlm) then
    call class_message(seve%e,rname,'Not relevant for FILE IN VLM')
    error = .true.
    return
  endif
  !
  interval = 5.0
  call sic_r4(line,0,1,interval,.false.,error)
  if (error) return
  !
  ! Find if new data present
  errcount = 0
  !
  do
    do  ! Infinite loop, except in case of error. Normal exit with ^C
      !
      ! Re-read the File Descriptor and check if file has new observations.
      call classic_file_fflush(filein,error)  ! Flush for reading
      if (error)  exit  ! e.g. file has disappearead
      call classic_filedesc_read(filein,error)
      if (error)  exit
      !
      if (ix%next.lt.filein%desc%xnext) then
        ! There is new data present in file
        call class_message(seve%i,rname,'New data present')
        call class_message(seve%i,rname,'Call FIND NEW_DATA|UPDATE to rebuild the indexes')
        return
      else
        call sic_wait(interval)  ! wait for N seconds
        if (sic_ctrlc()) then
          call class_message(seve%e,rname,'Waiting loop aborted by ^C')
          error = .true.
          return
        endif
      endif
      !
      ! Reached here with no error, reset error count
      errcount = 0
    enddo
    !
    ! An error occured
    if (errcount.eq.1) then
      ! 2nd error in a row => abort
      call class_message(seve%e,rname,  &
        'Read error file '//trim(filein%spec)//'. Abort.')
      return
    endif
    !
    write(mess,'(A,A,A,F0.1,A)')  'Error reading file ',trim(filein%spec),  &
      ', try again in ',interval,' seconds'
    call class_message(seve%e,rname,mess)
    errcount = errcount+1
    error = .false.
    !
    call sic_wait(interval)
  enddo
  !
end subroutine new_data
