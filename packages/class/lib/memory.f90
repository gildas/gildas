module class_memorize
  use gildas_def
  use class_types
  !---------------------------------------------------------------------
  ! Support module for commands MEMORIZE/RETRIEVE
  !---------------------------------------------------------------------
  !
  type :: memory
    character(len=12) :: name = ' '
    ! Observation
    type(observation), pointer :: obs => null()
  end type memory
  !
  integer(kind=4), parameter :: max_memory=100
  type(memory), save :: memories(max_memory)
  integer(kind=4) :: nmem_max=0  ! Maximum slot number in use (0 at startup)
  !
end module class_memorize
!
subroutine memorize(line,r,error)
  use gildas_def
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>memorize
  use class_types
  use class_memorize
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! CLASS ANALYSE Support for command
  !   MEMORIZE Name [/DELETE]
  !  Put a copy of R spectrum in some user defined place
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: line  ! Command line
  type(observation), intent(inout) :: r     !
  logical,           intent(inout) :: error ! Error flag
  ! Local
  character(len=*), parameter :: rname='MEMORIZE'
  integer :: imem,inew,nc,ier,iline
  character(len=12) :: argum
  character(len=80) :: oneline
  !
  if (sic_present(1,0)) then
    ! MEMO Name|* /DELETE
    call sic_ke(line,0,1,argum,nc,.true.,error)
    if (error) return
    !
    if (argum.eq.'*') then
      call memorize_free_all
      nmem_max = 0
    else
      do imem=1,nmem_max
        if (argum.eq.memories(imem)%name) then
          call memorize_free(imem)
          return
        endif
      enddo
      call class_message(seve%e,rname,'No such memory '//argum)
      error = .true.
    endif
    return
    !
  elseif (.not.sic_present(0,1)) then
    ! List memories
    if (nmem_max.gt.0) then
      call class_message(seve%i,rname,'Current memories:')
      iline = 1
      do imem=1,nmem_max
        if (memories(imem)%name.eq.' ')  cycle
        !
        if (iline.ge.60) then
          call class_message(seve%r,rname,oneline)
          iline = 1
        endif
        !
        write(oneline(iline:),'(A)') memories(imem)%name
        iline = iline+14
      enddo
      if (iline.gt.1)  call class_message(seve%r,rname,oneline)
    else
      call class_message(seve%w,rname,'No memory defined')
    endif
    !
  else
    !
    ! MEMO Name: create new memory
    call sic_ke (line,0,1,argum,nc,.true.,error)
    if (error) return
    !
    ! Look for existing empty slot, or a slot with same name
    inew = 0
    do imem=1,nmem_max
      if (memories(imem)%name.eq.' ')  then
        inew = imem
        exit  ! Loop
      endif
      !
      if (argum.eq.memories(imem)%name) then
        call memorize_free(imem)
        inew = imem
        exit  ! Loop
      endif
    enddo
    if (inew.eq.0) then
      ! No empty slot found, try to add one
      if (nmem_max.eq.max_memory) then
        call class_message(seve%e,rname,'Too many memories allocated')
        error = .true.
        return
      endif
      nmem_max = nmem_max+1
      inew = nmem_max
    endif
    !
    ! Allocate the inew-th slot
    memories(inew)%name = argum
    !
    allocate(memories(inew)%obs,stat=ier)
    if (failed_allocate(rname,'observation',ier,error)) return
    call init_obs(memories(inew)%obs)
    call copy_obs(r,memories(inew)%obs,error)
    if (error)  return
    !
  endif
  !
end subroutine memorize
!
subroutine memorize_free(imem)
  use classcore_interfaces, except_this=>memorize_free
  use class_memorize
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  !  Cleanly free the imem-th MEMORY slot
  !---------------------------------------------------------------------
  integer(kind=4), intent(in) :: imem  ! Slot number
  !
  memories(imem)%name = ' '
  if (associated(memories(imem)%obs))  then
    call free_obs(memories(imem)%obs)
    deallocate(memories(imem)%obs)
  endif
  !
end subroutine memorize_free
!
subroutine memorize_free_all
  use classcore_interfaces, except_this=>memorize_free_all
  use class_memorize
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  !  Cleanly free the all the MEMORY slot
  !---------------------------------------------------------------------
  integer(kind=4) :: imem
  !
  do imem=1,max_memory
    call memorize_free(imem)
  enddo
  !
end subroutine memorize_free_all
!
subroutine retrieve(set,line,out,error,user_function)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>retrieve
  use class_types
  use class_memorize
  !---------------------------------------------------------------------
  ! @ public
  ! Support for command
  !   RETRIEVE Name
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set            !
  character(len=*),    intent(in)    :: line           ! Input command line
  type(observation),   intent(inout) :: out            !
  logical,             intent(inout) :: error          ! Logical error flag
  logical,             external      :: user_function  !
  ! Local
  character(len=*), parameter :: rname='RETRIEVE'
  integer :: nc,imem
  character(len=12) :: argum
  !
  call sic_ke (line,0,1,argum,nc,.true.,error)
  if (error) return
  !
  do imem=1,nmem_max
    if (argum.eq.memories(imem)%name) then
      call rzero(out,'NULL',user_function)
      call copy_obs(memories(imem)%obs,out,error)
      ! if (error)  continue
      call newdat(set,out,error)
      call newdat_assoc(set,out,error)
      call newdat_user(set,out,error)
      out%head%xnum = -1  ! Obs comes from memory
      return
    endif
  enddo
  !
  call class_message(seve%e,rname,'No such memory '//argum)
  error = .true.
  !
end subroutine retrieve
