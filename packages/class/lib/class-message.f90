!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage CLASS messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module class_message_private
  use gpack_def
  !
  ! Identifier used for message identification
  integer :: class_message_id = gpack_global_id  ! Default value for startup message
  !
end module class_message_private
!
subroutine class_message_set_id(id)
  use class_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer, intent(in) :: id         !
  ! Local
  character(len=message_length) :: mess
  !
  class_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',class_message_id
  call class_message(seve%d,'class_message_set_id',mess)
  !
end subroutine class_message_set_id
!
subroutine class_message(mkind,procname,message)
  use gkernel_interfaces
  use class_message_private
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! Messaging facility for the current library. Calls the low-level
  ! (internal) messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  !
  call gmessage_write(class_message_id,mkind,procname,message)
  !
end subroutine class_message
!
subroutine class_iostat(mkind,procname,ier)
  use gkernel_interfaces
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Output system message corresponding to IO error code IER, and
  ! using the MESSAGE routine
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message severity
  character(len=*), intent(in) :: procname  ! Calling routine name
  integer,          intent(in) :: ier       ! IO error number
  ! Local
  character(len=message_length) :: msg
  !
  if (ier.eq.0) return
  call gag_iostat(msg,ier)
  call class_message(mkind,procname,msg)
  !
end subroutine class_iostat
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
