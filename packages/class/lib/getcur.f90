module cursor_position
  real(kind=4), save :: xcurs,ycurs
end module cursor_position
!
subroutine getcur(xc,xv,xf,xi,ya,xo,yo,ch)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>getcur
  use cursor_position
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  !  Sends back cursor position in all user units
  !---------------------------------------------------------------------
  real(kind=4),     intent(out) :: xc  ! Channel number
  real(kind=4),     intent(out) :: xv  ! Velocity
  real(kind=4),     intent(out) :: xf  ! Frequency offset
  real(kind=4),     intent(out) :: xi  ! Image offset
  real(kind=4),     intent(out) :: ya  ! Intensity
  real(kind=4),     intent(out) :: xo  ! Cursor X-offset
  real(kind=4),     intent(out) :: yo  ! Cursor Y-offset
  character(len=*), intent(out) :: ch  ! Character struck
  ! Local
  logical :: error
  !
  call get_box(gx1,gx2,gy1,gy2)
  error = .false.
  call gtcurs(xcurs,ycurs,ch,error)
  if (error) return
  !
  xc = gcx1 + (xcurs-gx1)/gcx
  xv = gvx1 + (xcurs-gx1)/gvx  ! For RADIO definition
  xf = gfx1 + (xcurs-gx1)/gfx
  xi = gix1 + (xcurs-gx1)/gix
  ya = guy1 + (ycurs-gy1)/guy
  xo = xcurs
  yo = ycurs
  call sic_upper(ch)
end subroutine getcur
!
subroutine setcur(xa,ya,unit)
  use classcore_interfaces, except_this=>setcur
  use cursor_position
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  !  Set cursor at (XA,YA) in UNIT coordinate system
  !---------------------------------------------------------------------
  real(kind=4),     intent(in) ::  xa   ! X position
  real(kind=4),     intent(in) ::  ya   ! Intensity
  character(len=*), intent(in) :: unit  ! X unit
  !
  call get_box(gx1,gx2,gy1,gy2)
  if (unit.eq.'C') then
     xcurs = gx1 + (xa-gcx1)*gcx
  elseif (unit.eq.'V') then
     xcurs = gx1 + (xa-gvx1)*gvx    ! For RADIO definition
  elseif (unit.eq.'F') then
     xcurs = gx1 + (xa-gfx1)*gfx
  elseif (unit.eq.'I') then
     xcurs = gx1 + (xa-gix1)*gix
  endif
  ycurs = gy1 + (ya-guy1)*guy
end subroutine setcur
!
subroutine cursor(r,par,jcode,error)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>cursor
  use class_types
  use cursor_position
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  ! Compute initial values for gauss fit using cursor
  !---------------------------------------------------------------------
  type(observation), intent(in)    :: r       !
  real(kind=4),      intent(out)   :: par(3)  ! Parameters array
  integer(kind=4),   intent(in)    :: jcode   ! Code to tell what to compute
  logical,           intent(inout) :: error   ! Error flag
  ! Local
  real(kind=4) :: ymax,ymin,aire,vinf,vsup,y,s0,s1,s2
  character(len=1) :: ch
  integer(kind=4) :: i1,i2,i
  !
  call get_box(gx1,gx2,gy1,gy2)
  call gtcurs(xcurs,ycurs,ch,error)
  if (error) return
  if (ch.eq.'/') return
  i1 = gcx1 + (xcurs-gx1)/gcx
  call gtcurs(xcurs,ycurs,ch,error)
  if (error) return
  i2 = gcx1 + (xcurs-gx1)/gcx
  error = .true.
  !
  ! Compute intensity, maximum and width
  if (jcode.eq.0) then
     ymax=0.
     ymin=0.
     aire=0.
     do i=min(i1,i2)+1,max(i1,i2)-1
        y = r%spectre(i)
        if (y.ne.r%cbad) then
           if (y.ge.ymax) then
              ymax = y
              vsup = r%datax(i)
           elseif (y.le.ymin) then
              ymin = y
              vinf = r%datax(i)
           endif
           aire = aire+y*abs(r%datax(i+1)-r%datax(i-1))
        endif
     enddo
     aire = aire*0.5
     if (aire.lt.0.) then
        par(3) = abs(aire/ymin)
        par(2) = vinf
        par(1) = ymin
     elseif (aire.gt.0) then
        par(3) = abs(aire/ymax)
        par(2) = vsup
        par(1) = ymax
     endif
     error = .false.
     !
     ! Compute area, position and width
  elseif (jcode.eq.1) then
     !
     s0=0.
     s1=0.
     s2=0.
     do i=min(i1,i2)+1,max(i1,i2)-1
        y = r%spectre(i)
        if (y.ne.r%cbad) then
           s0 = s0 + y
           s1 = s1 + y * r%datax(i)
           s2 = s2 + y * r%datax(i)**2
        endif
     enddo
     if (s0.ne.0) then
        s1 = s1 / s0
        s2 = s2 / s0
        y  = abs ((r%datax(i1)-r%datax(i2)) / (i1-i2))
        par(3) = y * sqrt (abs(s2-s1**2)*8.*alog(2.))
        par(2) = s1
        par(1) = s0 * y
        error = .false.
     else
        call class_message(seve%e,'LINES','Null area found, use manual mode')
        error = .true.
     endif
  endif
end subroutine cursor
