subroutine newdat(set,obs,error)
  use gildas_def
  use gbl_constant
  use gbl_message
  use gkernel_types
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>newdat
  use class_setup_new
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  !  Recompute scaling factors when a new scan is read in. Also called
  ! after changes in plotting mode or unit. Calls only NEWLIMX and
  ! NEWLIMY. Support for continuum
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(observation),   intent(inout) :: obs    ! Current observation
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='NEWDAT'
  integer(kind=index_length) :: dims(4)
  logical :: error2,found,redefine
  type(sic_descriptor_t), save :: rx_desc  ! Saved for subsequent calls
  logical, save :: gotdescriptor
  ! Data
  data gotdescriptor /.false./
  !
  if (obs%head%xnum.eq.0) then
     call class_message(seve%e,rname,'Empty R buffer')
     return
  endif
  !
  if (.not.associated(obs%datax)) then
    write(*,'(a8,l3)') 'ALLOCATION ERROR diagnosed in NEWDAT'
    write(*,'(a8,l3)') 'X ',associated(obs%datax)
    write(*,'(a8,l3)') '1D ',associated(obs%data1)
    write(*,'(a8,l3)') 'S ',associated(obs%datas)
    write(*,'(a8,l3)') 'I ',associated(obs%datai)
    write(*,'(a8,l3)') 'V ',associated(obs%datav)
    write(*,'(a8,l3)') 'W ',associated(obs%dataw)
    write(*,'(a8,l3)') '2D ',associated(obs%data2)
    error = .true.
    return
  endif
  !
  error2 = .false.  ! Use a local error flag since it happens 'newdat' is called
                    ! with 'error' = .true. as input (error recovery mode)
  !
  call abscissa(set,obs,error2)   ! Compute X arrays according to type and unit
  if (error2) then
    error = .true.
    return
  endif
  call newlim(set,obs,error2)     ! Set limits
  if (error2) then
    error = .true.
    return
  endif
  !
  ! Synchronize duplicate values
  if (obs%head%presec(class_sec_gen_id)) then  ! Probably always true
    call gag_todate(obs%head%gen%dobs,obs%head%gen%cdobs,error2)
    call gag_todate(obs%head%gen%dred,obs%head%gen%cdred,error2)
  endif
  if (obs%head%presec(class_sec_pos_id)) then  ! Probably always true
    obs%head%pos%rx_off = obs%head%pos%lamof*class_setup_get_fangle()
    obs%head%pos%ry_off = obs%head%pos%betof*class_setup_get_fangle()
  endif
  !
  if (.not.obs%is_R)  return
  !
  ! This is the R observation, we need to check the R* Sic variables:
  !   - data arrays may need redefinition e.g. if number of channels
  !     has changed,
  !   - scalar variables never need redefinition, except on a R/T swap,
  !     but this is supported in 'swaprt'
  ! NB: the variables below are suited for the 'R' buffer only, not any
  ! 'obs' got as input
  ! The code below avoids calling sic_descriptor, sic_delvariable,
  ! and sic_def_* as much as possible, as searching in Sic dictionary has
  ! a non-negligible cost.
  !
  if (.not.gotdescriptor) then
    redefine = .true.
  else
    ! On which condition Sic arrays should be redefined (see
    ! sic_def_real calling sequence)?
    ! - Variable name:        no (never changes)
    ! - Data address:         yes (in case of reallocation or RT-swap)
    ! - Number of dimensions: no (always 1)
    ! - Dimensions:           yes (can change even if data address is kept)
    ! - Read-write status:    no (never changes)
    redefine = rx_desc%addr.ne.locwrd(obs%datax) .or. &
               rx_desc%dims(1).ne.obs%cnchan
  endif
  if (.not.redefine)  return
  !
  gotdescriptor = .false.  ! For safety, if an error occurs before the end
  !
  ! Cleaning
  call sic_delvariable ('RX',.false.,error2)
  call sic_delvariable ('RY',.false.,error2)
  call sic_delvariable ('R%DATAS',.false.,error2)
  call sic_delvariable ('R%DATAI',.false.,error2)
  call sic_delvariable ('R%DATAV',.false.,error2)
  call sic_delvariable ('R%FFT',.false.,error2)  ! See class_fft_compute
  error2 = .false.  ! No error if not found
  !
  ! Definition
  if (xdata_kind.ne.8) then
    ! If xdata_kind==4, use sic_def_real for the X arrays below
    call class_message(seve%f,rname,  &
      'Internal error: only xdata_kind==8 is supported')
    error = .true.
    return
  endif
  !
  ! Arrays
  dims(1) = obs%cnchan
  call sic_def_real('RY',obs%spectre,1,dims,.false.,error2)
  if (error2)  error = .true.
  call sic_def_dble('RX',obs%datax,1,dims,.true.,error2)
  if (error2)  error = .true.
  if (associated(obs%datas))  &
    call sic_def_dble('R%DATAS',obs%datas,1,dims,.true.,error2)
  if (associated(obs%datai))  &
    call sic_def_dble('R%DATAI',obs%datai,1,dims,.true.,error2)
  if (associated(obs%datav))  &
    call sic_def_dble('R%DATAV',obs%datav,1,dims,.true.,error2)
  if (error2)  error = .true.
  !
  ! Save RX descriptor for subsequent calls
  call sic_descriptor('RX',rx_desc,found)
  if (.not.found)  error = .true.
  if (error)  return
  !
  gotdescriptor = .true.
  !
end subroutine newdat
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine abscissa(set,obs,error)
  use gildas_def
  use gbl_message
  use gbl_constant
  use phys_const
  use classcore_interfaces, except_this=>abscissa
  use class_parameter
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Recompute the observation arrays DATAV, DATAS and DATAI, and
  ! set the observation array DATAX according to the current SET
  ! UNIT value.
  !  Use of DATA arrays:
  !  -------------------------
  !   Regul | SPEC  |  CONT
  !   DATAV |  V    |   A
  !   DATAS |  F    |   T
  !   DATAI |  I    |   -
  !   DATAC |  C    |   C    (NB: computed on the fly)
  !   DATAX | VFIC  |  ATC   (NB: copied from above arrays)
  !  -------------------------
  !   Irreg | SPEC  |  CONT
  !   DATAV | VFI   |   AT
  !   DATAS |  -    |   -
  !   DATAI |  -    |   -
  !   DATAC |  C    |   C    (NB: computed on the fly)
  !   DATAX | VFIC  |  ATC   (NB: copied from above arrays)
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)     :: set    !
  type(observation),   intent(inout)  :: obs    ! Current observation
  logical,             intent(out)    :: error  ! Error status
  ! Local
  character(len=*), parameter :: rname='ABSCISSA'
  character(len=80) :: chain
  integer :: i
  !
  error = .false.
  if (.not.associated(obs%datax)) then
     call class_message(seve%e,rname,'X data not allocated')
     error = .true.
     return
  endif
  !
  ! Compute X coordinates according to the units used
  !
  if (obs%head%gen%kind.eq.kind_spec) then
     if (obs%head%presec(class_sec_xcoo_id)) then
        if (set%verbose)  &
          call class_message(seve%i,rname,'Irregularly sampled X data')
        if (set%unitx(1).eq.'C') then
           do i=1,obs%head%spe%nchan
              obs%datax(i) = i
           enddo
        else
           if (.not.associated(obs%datav)) then
              call class_message(seve%e,rname,'Velocity data not allocated')
              error = .true.
              return
           endif
           obs%datax = obs%datav
        endif
     else
        if (obs%head%spe%vres.eq.0.d0 .and. obs%head%spe%fres.eq.0.d0) then
           obs%head%spe%vres = 1.d0
           obs%head%spe%fres = 1.d0
        else if (obs%head%spe%vres.eq.0.d0) then
           if (obs%head%spe%restf.ne.0.d0) then
              obs%head%spe%vres = -clight_kms*obs%head%spe%fres/obs%head%spe%restf
           else
              obs%head%spe%vres = 1.d0
           endif
        else if (obs%head%spe%fres.eq.0.d0) then
           if (obs%head%spe%restf.ne.0.d0) then
              obs%head%spe%fres = -obs%head%spe%restf*(obs%head%spe%vres/clight_kms)
           else
              obs%head%spe%fres = 1.d0
           endif
        endif
        !
        ! Compute arrays
        if (associated(obs%datas))  &
          call abscissa_sigoff(obs%head,obs%datas,1,obs%head%spe%nchan)
        if (associated(obs%datai))  &
          call abscissa_imaoff(obs%head,obs%datai,1,obs%head%spe%nchan)
        if (associated(obs%datav))  &
          call abscissa_velo(obs%head,obs%datav,1,obs%head%spe%nchan)
        !
        ! Set X array (DATAX) according to X unit
        if (set%unitx(1).eq.'C') then
           do i=1,obs%head%spe%nchan
              obs%datax(i) = i
           enddo
        elseif (set%unitx(1).eq.'V') then
           if (.not.associated(obs%datav)) then
              call class_message(seve%e,rname,'Velocity data not allocated')
              error = .true.
              return
           endif
           obs%datax = obs%datav
        elseif (set%unitx(1).eq.'F') then
           if (.not.associated(obs%datas)) then
              call class_message(seve%e,rname,'Sky freq. data not allocated')
              error = .true.
              return
           endif
           obs%datax = obs%datas
        elseif (set%unitx(1).eq.'I') then
           if (.not.associated(obs%datai)) then
              call class_message(seve%e,rname,'Image freq. data not allocated')
              error = .true.
              return
           endif
           obs%datax = obs%datai
        else
           chain = 'Invalid unit '//set%unitx(1)//' for spectral line data.'
           call class_message(seve%e,rname,chain)
           error = .true.
           return
        endif
     endif
     obs%cnchan = obs%head%spe%nchan
     obs%cbad   = obs%head%spe%bad
  else
     if (obs%head%presec(class_sec_xcoo_id)) then
        if (set%verbose)  &
          call class_message(seve%i,rname,'Irregularly sampled X data')
        if (set%unitx(1).eq.'C') then       ! Channels
           do i=1,obs%head%dri%npoin
              obs%datax(i) = i
           enddo
        else
           if (.not.associated(obs%datax)) then
              call class_message(seve%e,rname,'Offset data not allocated')
              error = .true.
              return
           endif
           obs%datax = obs%datav
        endif
     else
        if (obs%head%dri%ares.eq.0) obs%head%dri%ares = 1.0
        if (obs%head%dri%tres.eq.0) obs%head%dri%tres = 1.0
        !
        ! Compute arrays
        if (associated(obs%datas))  &
          call abscissa_time(obs%head,obs%datas,1,obs%head%dri%npoin)
        if (associated(obs%datav))  &
          call abscissa_angl(obs%head,obs%datav,1,obs%head%dri%npoin)
        !
        ! Set X array (DATAX) according to X unit
        if (set%unitx(1).eq.'C') then       ! Channels
           do i=1,obs%head%dri%npoin
              obs%datax(i) = i
           enddo
        elseif (set%unitx(1).eq.'T') then   ! Time
           if (.not.associated(obs%datax)) then
              call class_message(seve%e,rname,'Time data not allocated')
              error = .true.
              return
           endif
           obs%datax = obs%datas
        elseif (set%unitx(1).eq.'A') then   ! Angle
           if (.not.associated(obs%datax)) then
              call class_message(seve%e,rname,'Offset data not allocated')
              error = .true.
              return
           endif
           obs%datax = obs%datav
        else
           chain = 'Invalid unit '//set%unitx(1)//' for continuum data.'
           call class_message(seve%e,rname,chain)
           error = .true.
           return
        endif
     endif
     obs%cnchan = obs%head%dri%npoin
     obs%cbad = obs%head%dri%bad
  endif
end subroutine abscissa
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine newlim(set,obs,error)
  use gildas_def
  use gbl_constant
  use gbl_message
  use classcore_interfaces, except_this=>newlim
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(observation),   intent(inout) :: obs    ! Current observation
  logical,             intent(out)   :: error  ! Error flag
  !
  if (.not.associated(obs%datax)) then
     call class_message(seve%e,'NEWLIM','No observation in memory or loaded')
     error = .true.
     return
  endif
  !
  call newlimx(set,obs,error)
  call newlimy(set,obs,error)
  call newlimz(set,obs,error)
end subroutine newlim
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine newlimx(set,obs,error)
  use gildas_def
  use gbl_message
  use gbl_constant
  use classcore_interfaces, except_this=>newlimx
  use class_types
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS Internal routine
  ! Set X plotting limits according to selected mode.
  ! It is called only by NEWDAT, and calls SELIMX and GELIMX.
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(observation),   intent(inout) :: obs    ! Current observation
  logical,             intent(out)   :: error  ! Error flag
  ! Local
  character(len=*), parameter :: rname='NEWLIMX'
  real(kind=plot_length) :: xc1,xc2,xv1,xv2,xf1,xf2,xi1,xi2
  real(kind=8) :: xfo,xio,dummy
  logical :: spectrum
  !
  ! Initialization and sanity checks
  error = .false.
  if (obs%head%xnum.eq.0) then
     call class_message(seve%e,rname,'No spectrum in memory')
     error = .true.
     return
  endif
  !
  ! Irregular coordinate grid.
  ! The present code is crude.
  ! For optimisation reasons, make the (safe?) assumption that the data are
  ! already sorted.
  !
  spectrum = obs%head%gen%kind.eq.kind_spec
  if (obs%head%presec(class_sec_xcoo_id)) then
    if (spectrum) then
      call newlimx_spec_irreg(set,obs,xc1,xc2,xv1,xv2,xf1,xf2,xfo,xi1,xi2,xio)
    else
      call newlimx_cont_irreg(set,obs,xc1,xc2,xv1,xv2,xf1,xf2,xfo)
    endif
  else
    if (spectrum) then
      call newlimx_spec_regul(set,obs,xc1,xc2,xv1,xv2,xf1,xf2,xfo,xi1,xi2,xio)
    else
      call newlimx_cont_regul(set,obs,xc1,xc2,xv1,xv2,xf1,xf2,xfo)
    endif
  endif
  !
  ! Send new limits
  call selimx(obs,xc1,xc2,xv1,xv2,xf1,xf2,xfo,xi1,xi2,xio)
  ! Compute new current limits
  call gelimx(dummy,gux1,gux2,gux,set%unitx(1))
  !
end subroutine newlimx
!
subroutine newlimx_spec_irreg(set,obs,xc1,xc2,xv1,xv2,xf1,xf2,xfo,xi1,xi2,xio)
  use classcore_interfaces, except_this=>newlimx_spec_irreg
  use class_types
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  !   Compute the X limits for the 4 supported units for spectroscopic
  ! data irregularly sampled
  !---------------------------------------------------------------------
  type(class_setup_t),    intent(in)  :: set      !
  type(observation),      intent(in)  :: obs      !
  real(kind=plot_length), intent(out) :: xc1,xc2  !
  real(kind=plot_length), intent(out) :: xv1,xv2  !
  real(kind=plot_length), intent(out) :: xf1,xf2  !
  real(kind=8),           intent(out) :: xfo      !
  real(kind=plot_length), intent(out) :: xi1,xi2  !
  real(kind=8),           intent(out) :: xio      !
  ! Local
  real(kind=plot_length) :: s
  real(kind=xdata_kind), parameter :: one=1.0
  !
  if (set%modex.ne.'F') then  ! Not fixed
    ! Automatic
    if (set%modex.eq.'A' .and. obs%head%plo%vmin.ne.obs%head%plo%vmax) then
      xv1 = obs%head%plo%vmin
      xv2 = obs%head%plo%vmax
      call ichan_from_value(obs,xv1,xv2,xc1,xc2)
    else
      ! Total
      if (set%unitx(1).eq.'V' .or. set%unitx(1).eq.'F') then
        s = sign(one,obs%datax(obs%head%spe%nchan)-obs%datax(1))
      elseif (set%unitx(1).eq.'I') then
        s = -sign(one,obs%datax(obs%head%spe%nchan)-obs%datax(1))
      else                   ! Channel units
        s = 1.0
      endif
      ! Irregular: use center of boundary channels, since we can not know
      ! values at channel boundaries
      if (s.gt.0.0) then
        xc1 = 1.d0
        xc2 = obs%head%spe%nchan
      else
        xc1 = obs%head%spe%nchan
        xc2 = 1.d0
      endif
      xv1 = obs%datax(nint(xc1))
      xv2 = obs%datax(nint(xc2))
    endif
    xf1 = obs%datax(nint(xc1))  ! Unknown for irregularly spaced data...
    xf2 = obs%datax(nint(xc2))  ! Id.
    xfo = 0.d0                  ! Id.
    xi1 = obs%datax(nint(xc1))  ! Id.
    xi2 = obs%datax(nint(xc2))  ! Id.
    xio = 0.d0                  ! Id.
  elseif (set%unitx(1).eq.'C') then  ! Fixed in channels
    xc1 = gcx1
    xc2 = gcx2
    xv1 = obs%datax(nint(xc1))
    xv2 = obs%datax(nint(xc2))
    xf1 = xv1   ! Unknown for irregularly spaced data...
    xf2 = xv2   ! Id.
    xfo = 0.d0  ! Id.
    xi1 = xv1   ! Id.
    xi2 = xv2   ! Id.
    xio = 0.d0  ! Id.
  elseif (set%unitx(1).eq.'V') then  ! Fixed in Velocity
    call ichan_from_value(obs,gvx1,gvx2,xc1,xc2)
    xv1 = gvx1
    xv2 = gvx2
    xf1 = gvx1  ! Unknown for irregularly spaced data...
    xf2 = gvx2  ! Id.
    xfo = 0.d0  ! Id.
    xi1 = gvx1  ! Id.
    xi2 = gvx2  ! Id.
    xio = 0.d0  ! Id.
  elseif (set%unitx(1).eq.'F') then  ! Fixed in frequency
    xf1 = (gfxo+gfx1)-obs%head%spe%restf
    xf2 = (gfxo+gfx2)-obs%head%spe%restf
    xfo = obs%head%spe%restf
    call ichan_from_value(obs,xf1,xf2,xc1,xc2)
    xv1 = xf1   ! Unknown for irregularly spaced data...
    xv2 = xf2   ! Id.
    xi1 = xf1   ! Id.
    xi2 = xf2   ! Id.
    xio = 0.d0  ! Id.
  elseif (set%unitx(1).eq.'I') then  ! Fixed in image frequency
    xi1 = (gixo+gix1)-obs%head%spe%image
    xi2 = (gixo+gix2)-obs%head%spe%image
    xio = obs%head%spe%image
    call ichan_from_value(obs,xi1,xi2,xc1,xc2)
    xv1 = xi1   ! Unknown for irregularly spaced data...
    xv2 = xi2   ! Id.
    xf1 = xi1   ! Id.
    xf2 = xi2   ! Id.
    xfo = 0.d0  ! Id.
  endif
  !
end subroutine newlimx_spec_irreg
!
subroutine newlimx_cont_irreg(set,obs,xc1,xc2,xv1,xv2,xf1,xf2,xfo)
  use classcore_interfaces, except_this=>newlimx_cont_irreg
  use class_types
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  !   Compute the X limits for the 3 supported units for continuum
  ! data irregularly sampled
  !---------------------------------------------------------------------
  type(class_setup_t),    intent(in)  :: set      !
  type(observation),      intent(in)  :: obs      !
  real(kind=plot_length), intent(out) :: xc1,xc2  !
  real(kind=plot_length), intent(out) :: xv1,xv2  !
  real(kind=plot_length), intent(out) :: xf1,xf2  !
  real(kind=8),           intent(out) :: xfo      !
  ! Local
  real(kind=plot_length) :: s
  real(kind=xdata_kind), parameter :: one=1.0
  !
  if (set%modex.ne.'F') then  ! Not fixed
    ! Automatic
    if (set%modex.eq.'A' .and. obs%head%plo%vmin.ne.obs%head%plo%vmax) then
      xv1 = obs%head%plo%vmin
      xv2 = obs%head%plo%vmax
      call ichan_from_value(obs,xv1,xv2,xc1,xc2)
    else
      ! Total
      if (set%unitx(1).eq.'T' .or. set%unitx(1).eq.'A' ) then
        s = sign(one,obs%datax(obs%head%dri%npoin)-obs%datax(1))
      else  ! Channel units
        s = 1.0
      endif
      ! Irregular: use center of boundary channels, since we can not know
      ! values at channel boundaries
      if (s.gt.0.0) then
        xc1 = 1.d0
        xc2 = obs%head%dri%npoin
      else
        xc1 = obs%head%dri%npoin
        xc2 = 1.d0
      endif
      xv1 = obs%datax(nint(xc1))
      xv2 = obs%datax(nint(xc2))
    endif
    xf1 = obs%datax(nint(xc1))  ! Unknown for irregularly spaced data...
    xf2 = obs%datax(nint(xc2))  ! Id.
    xfo = 0.d0                  ! Id.
  elseif (set%unitx(1).eq.'C') then  !----------- Fixed in channels
    xc1 = gcx1
    xc2 = gcx2
    xv1 = obs%datax(nint(xc1))
    xv2 = obs%datax(nint(xc2))
    xf1 = xv1   ! Unknown for irregularly spaced data...
    xf2 = xv2   ! Id.
    xfo = 0.d0  ! Id.
  elseif (set%unitx(1).eq.'T') then  !----------- Fixed in time
    xf1 = gfx1
    xf2 = gfx2
    xfo = 0.d0
    call ichan_from_value(obs,xf1,xf2,xc1,xc2)
    xv1 = xf1   ! Unknown for irregularly spaced data...
    xv2 = xf2   ! Id.
  else                               !----------- Fixed in angular offset
    xv1 = gvx1
    xv2 = gvx2
    call ichan_from_value(obs,xv1,xv2,xc1,xc2)
    xf1 = xv1   ! Unknown for irregularly spaced data...
    xf2 = xv2   ! Id.
    xfo = 0.d0  ! Id.
  endif
  !
end subroutine newlimx_cont_irreg
!
subroutine newlimx_spec_regul(set,obs,xc1,xc2,xv1,xv2,xf1,xf2,xfo,xi1,xi2,xio)
  use classcore_interfaces, except_this=>newlimx_spec_regul
  use class_types
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  !   Compute the X limits for the 4 supported units for spectroscopic
  ! data regularly sampled
  !---------------------------------------------------------------------
  type(class_setup_t),    intent(in)  :: set      !
  type(observation),      intent(in)  :: obs      !
  real(kind=plot_length), intent(out) :: xc1,xc2  !
  real(kind=plot_length), intent(out) :: xv1,xv2  !
  real(kind=plot_length), intent(out) :: xf1,xf2  !
  real(kind=8),           intent(out) :: xfo      !
  real(kind=plot_length), intent(out) :: xi1,xi2  !
  real(kind=8),           intent(out) :: xio      !
  ! Local
  real(kind=plot_length) :: s
  !
  if (set%modex.ne.'F') then      ! Not fixed
    if (set%modex.eq.'A' .and. obs%head%plo%vmin.ne.obs%head%plo%vmax) then
      ! Automatic
      xv1 = obs%head%plo%vmin
      xv2 = obs%head%plo%vmax
      call abscissa_velo2chan(obs%head,xv1,xc1)
      call abscissa_velo2chan(obs%head,xv2,xc2)
    else
      ! Total
      if (set%unitx(1).eq.'V') then
        s = sign(1.0d0,obs%head%spe%vres)
      elseif (set%unitx(1).eq.'I') then
        s = -sign(1.d0,obs%head%spe%fres)
      elseif (set%unitx(1).eq.'F') then
        s = sign(1.d0,obs%head%spe%fres)
      else
        s = 1.0
      endif
      ! Each channel covers ichan+-.5, so the whole range is .5 to Nchan+.5
      if (s.gt.0.0) then
        xc1 = 0.5d0
        xc2 = obs%head%spe%nchan+0.5d0
      else
        xc1 = obs%head%spe%nchan+0.5d0
        xc2 = 0.5d0
      endif
      call abscissa_chan2velo(obs%head,xc1,xv1)
      call abscissa_chan2velo(obs%head,xc2,xv2)
    endif
    call abscissa_chan2sigoff(obs%head,xc1,xf1)
    call abscissa_chan2sigoff(obs%head,xc2,xf2)
    xfo = obs%head%spe%restf
    call abscissa_chan2imaoff(obs%head,xc1,xi1)
    call abscissa_chan2imaoff(obs%head,xc2,xi2)
    xio = obs%head%spe%image
  elseif (set%unitx(1).eq.'C') then    ! Fixed in channels
    xc1 = gcx1
    xc2 = gcx2
    call abscissa_chan2velo(obs%head,xc1,xv1)
    call abscissa_chan2velo(obs%head,xc2,xv2)
    call abscissa_chan2sigoff(obs%head,xc1,xf1)
    call abscissa_chan2sigoff(obs%head,xc2,xf2)
    xfo = obs%head%spe%restf
    call abscissa_chan2imaoff(obs%head,xc1,xi1)
    call abscissa_chan2imaoff(obs%head,xc2,xi2)
    xio = obs%head%spe%image
  elseif (set%unitx(1).eq.'V') then    ! Fixed in velocity
    xv1 = gvx1
    xv2 = gvx2
    call abscissa_velo2chan(obs%head,xv1,xc1)
    call abscissa_velo2chan(obs%head,xv2,xc2)
    call abscissa_chan2sigoff(obs%head,xc1,xf1)
    call abscissa_chan2sigoff(obs%head,xc2,xf2)
    xfo = obs%head%spe%restf
    call abscissa_chan2imaoff(obs%head,xc1,xi1)
    call abscissa_chan2imaoff(obs%head,xc2,xi2)
    xio = obs%head%spe%image
  elseif (set%unitx(1).eq.'F') then    ! Fixed in frequency
    xf1 = (gfxo+gfx1)-obs%head%spe%restf
    xf2 = (gfxo+gfx2)-obs%head%spe%restf
    xfo = obs%head%spe%restf
    call abscissa_sigoff2chan(obs%head,xf1,xc1)
    call abscissa_sigoff2chan(obs%head,xf2,xc2)
    call abscissa_chan2velo(obs%head,xc1,xv1)
    call abscissa_chan2velo(obs%head,xc2,xv2)
    call abscissa_chan2imaoff(obs%head,xc1,xi1)
    call abscissa_chan2imaoff(obs%head,xc2,xi2)
    xio = obs%head%spe%image
  elseif (set%unitx(1).eq.'I') then    ! Fixed in image frequency
    xi1 = (gixo+gix1)-obs%head%spe%image
    xi2 = (gixo+gix2)-obs%head%spe%image
    xio = obs%head%spe%image
    call abscissa_imaoff2chan(obs%head,xi1,xc1)
    call abscissa_imaoff2chan(obs%head,xi2,xc2)
    call abscissa_chan2velo(obs%head,xc1,xv1)
    call abscissa_chan2velo(obs%head,xc2,xv2)
    call abscissa_chan2sigoff(obs%head,xc1,xf1)
    call abscissa_chan2sigoff(obs%head,xc2,xf2)
    xfo = obs%head%spe%restf
  endif
  !
end subroutine newlimx_spec_regul
!
subroutine newlimx_cont_regul(set,obs,xc1,xc2,xv1,xv2,xf1,xf2,xfo)
  use classcore_interfaces, except_this=>newlimx_cont_regul
  use class_types
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  !   Compute the X limits for the 3 supported units for continuum
  ! data regularly sampled
  !---------------------------------------------------------------------
  type(class_setup_t),    intent(in)  :: set      !
  type(observation),      intent(in)  :: obs      !
  real(kind=plot_length), intent(out) :: xc1,xc2  !
  real(kind=plot_length), intent(out) :: xv1,xv2  !
  real(kind=plot_length), intent(out) :: xf1,xf2  !
  real(kind=8),           intent(out) :: xfo      !
  ! Local
  real(kind=plot_length) :: s
  !
  if (set%modex.ne.'F') then  ! not Fixed
    if (set%modex.eq.'A' .and. obs%head%plo%vmin.ne.obs%head%plo%vmax) then
      ! Automatic
      xv1 = obs%head%plo%vmin
      xv2 = obs%head%plo%vmax
      call abscissa_angl2chan(obs%head,xv1,xc1)
      call abscissa_angl2chan(obs%head,xv2,xc2)
    else
      ! Total
      if (set%unitx(1).eq.'C') then
        s = 1.0
      elseif (set%unitx(1).eq.'T') then
        s = sign(1.0,obs%head%dri%tres)
      else
        s = sign(1.0,obs%head%dri%ares)
      endif
      ! Each channel covers ichan+-.5, so the whole range is .5 to Nchan+.5:
      if (s.gt.0.0) then
        xc1 = 0.5d0
        xc2 = obs%head%dri%npoin+0.5d0
      else
        xc1 = obs%head%dri%npoin+0.5d0
        xc2 = 0.5d0
      endif
      call abscissa_chan2angl(obs%head,xc1,xv1)
      call abscissa_chan2angl(obs%head,xc2,xv2)
    endif
    call abscissa_chan2time(obs%head,xc1,xf1)
    call abscissa_chan2time(obs%head,xc2,xf2)
    xfo = 0.d0
  elseif (set%unitx(1).eq.'C') then  !----------- Fixed in channels
    xc1 = gcx1
    xc2 = gcx2
    call abscissa_chan2angl(obs%head,xc1,xv1)
    call abscissa_chan2angl(obs%head,xc2,xv2)
    call abscissa_chan2time(obs%head,xc1,xf1)
    call abscissa_chan2time(obs%head,xc2,xf2)
    xfo = 0.d0
  elseif (set%unitx(1).eq.'T') then  !----------- Fixed in time
    xf1 = gfx1
    xf2 = gfx2
    xfo = 0.d0
    call abscissa_time2chan(obs%head,xf1,xc1)
    call abscissa_time2chan(obs%head,xf2,xc2)
    call abscissa_chan2angl(obs%head,xc1,xv1)
    call abscissa_chan2angl(obs%head,xc2,xv2)
  else                               !----------- Fixed in angular offset
    xv1 = gvx1
    xv2 = gvx2
    call abscissa_angl2chan(obs%head,xv1,xc1)
    call abscissa_angl2chan(obs%head,xv2,xc2)
    call abscissa_chan2time(obs%head,xc1,xf1)
    call abscissa_chan2time(obs%head,xc2,xf2)
    xfo = 0.d0
  endif
  !
end subroutine newlimx_cont_regul
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine newlimy(set,obs,error)
  use gildas_def
  use gbl_constant
  use gbl_message
  use classcore_interfaces, except_this=>newlimy
  use class_types
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS Internal routine
  ! Set Y plotting limits according to selected mode.
  ! It is called only by NEWDAT, and calls SELIMY.
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(observation),   intent(inout) :: obs    ! Current observation
  logical,             intent(out)   :: error  ! Error flag
  ! Local
  character(len=*), parameter :: rname='NEWLIMY'
  real(kind=plot_length) :: y1,y2,yy,yy1,yy2
  real(kind=plot_length), parameter :: dy=0.05
  integer :: i4,ir
  real :: bad
  logical :: is2d
  !
  ! Sanity checks
  error = .false.
  if (obs%head%xnum.eq.0) then
     call class_message(seve%e,rname,'No spectrum in memory')
     error = .true.
     return
  endif
  !
  if (obs%head%gen%kind.eq.1) then
     ! Continuum drift
     bad = obs%cbad
  else
     ! Spectral type
     bad = obs%head%spe%bad
  endif
  !
  ! 1D or 2D plot
  if (associated(obs%data2)) then
     is2d = .true.
  else if (associated(obs%datax)) then
     is2d = .false.
  else
     call class_message(seve%e,rname,'No spectrum in memory / no index loaded')
     error = .true.
     return
  endif
  !
  if (.not.is2d) then
     if (set%modey.ne.'F') then
        if (set%modey.eq.'A' .and. obs%head%plo%amin.ne.obs%head%plo%amax) then
           y1 = obs%head%plo%amin
           y2 = obs%head%plo%amax
        else
           i4 = obs%cimax-obs%cimin+1
           call class_minmax(y1,y2,obs%spectre(obs%cimin:obs%cimax),i4,bad)
        endif
        if (y1.eq.y2) then
           yy = max(dy,dy*abs(y1))
           y1 = y1-yy
           y2 = y2+yy
        endif
        call selimy(y1,y2)
     endif
     if (obs%head%plo%amin.ne.obs%head%plo%amax .or. obs%head%plo%vmin.ne.obs%head%plo%vmax) then
        obs%head%plo%amin = y1
        obs%head%plo%amax = y2
        obs%head%presec(class_sec_plo_id) = .true.
     endif
  else
     if (set%modey.ne.'F') then
        if (set%modey.eq.'A' .and. obs%head%plo%amin.ne.obs%head%plo%amax) then
           y1 = obs%head%plo%amin
           y2 = obs%head%plo%amax
        else
           i4 = obs%cimax-obs%cimin+1
           y1 = 1e10
           y2 = -1e10
           ! Compute Y limits depending on Z limits if any
           do ir=max(1,int(guz1)),min(obs%head%des%ndump,int(guz2))
              call class_minmax(yy1,yy2,obs%data2(obs%cimin:obs%cimax,ir),i4,bad)
              y1 = min(y1,yy1)
              y2 = max(y2,yy2)
           enddo
        endif
        if (y1.eq.y2) then
           yy = max(dy,dy*y1)
           y1 = y1-yy
           y2 = y2+yy
        endif
        call selimy2d(y1,y2)
     else
        call selimy2d(guy1,guy2)
     endif
     if (obs%head%plo%amin.ne.obs%head%plo%amax .or. obs%head%plo%vmin.ne.obs%head%plo%vmax) then
        obs%head%plo%amin = y1
        obs%head%plo%amax = y2
        obs%head%presec(class_sec_plo_id) = .true.
     endif
  endif
end subroutine newlimy
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine newlimz(set,obs,error)
  use gildas_def
  use gbl_constant
  use gbl_message
  use classcore_interfaces, except_this=>newlimz
  use class_types
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS Internal routine
  ! Used only in the case of 2D plots
  ! Set Z plotting limits according to selected mode in Z
  ! It is called when getting a multirecord scan.
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)  :: set    !
  type(observation),   intent(in)  :: obs    ! Current observation
  logical,             intent(out) :: error  ! Error flag
  ! Local
  character(len=*), parameter :: rname='NEWLIMZ'
  real(kind=plot_length) :: z1,z2
  real(kind=plot_length), parameter :: dy=0.05
  !
  ! Check whether obs is 2D or 1D
  if (.not.associated(obs%data2)) then
     return
  endif
  !
  error = .false.
  if (obs%head%xnum.eq.0) then
     call class_message(seve%e,rname,'No spectrum in memory')
     error = .true.
     return
  endif
  !
  if (set%modez.ne.'F') then
     z1 = 0.5
     z2 = obs%head%des%ndump+0.5
     call selimz(z1,z2)
  else
     call selimz(guz1,guz2)
  endif
end subroutine newlimz
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine set_angle(set,head)
  use gildas_def
  use class_setup_new
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS Internal routine
  ! Compute true offsets taking into account position angle
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(header),        intent(inout) :: head
  !
  head%pos%rx_off = head%pos%lamof*class_setup_get_fangle()
  head%pos%ry_off = head%pos%betof*class_setup_get_fangle()
end subroutine set_angle
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine ichan_from_value(obs,v1,v2,i1,i2)
  use gildas_def
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Convert velocity interval into corresponding channel interval
  !   obs%datax(i1).ge.min(v1,v2)
  !   obs%datax(i2).le.max(v1,v2)
  !---------------------------------------------------------------------
  type(observation),      intent(in)  :: obs  !
  real(kind=plot_length), intent(in)  :: v1   ! Velocity interval
  real(kind=plot_length), intent(in)  :: v2   ! Velocity interval
  real(kind=plot_length), intent(out) :: i1   ! First channel
  real(kind=plot_length), intent(out) :: i2   ! Last channel
  ! Local
  integer :: i,nc
  !
  nc = obs%cnchan  ! For both spec or continuum data
  !
  if (v1.lt.v2) then
     i = 1
     do while (i.lt.nc-1.and.obs%datax(i+1).lt.v1)
        i = i+1
     enddo
     i1 = i
     i = nc
     do while (i.gt.2.and.obs%datax(i-1).gt.v2)
        i = i-1
     enddo
     i2 = i
  else
     i = nc
     do while (i.gt.2.and.obs%datax(i-1).lt.v1)
        i = i-1
     enddo
     i1 = i
     i = 1
     do while (i.lt.nc-1.and.obs%datax(i+1).gt.v2)
        i = i+1
     enddo
     i2 = i
  endif
end subroutine ichan_from_value
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
