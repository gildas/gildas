module classcore_interfaces_public
  interface
    subroutine abscissa_velo_left(head,velo)
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! Compute the leftmost (channelwise) velocity of input spectrum
      !---------------------------------------------------------------------
      type(header), intent(in)  :: head  !
      real(kind=8), intent(out) :: velo  !
    end subroutine abscissa_velo_left
  end interface
  !
  interface
    subroutine abscissa_velo_right(head,velo)
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! Compute the rightmost (channelwise) velocity of input spectrum
      !---------------------------------------------------------------------
      type(header), intent(in)  :: head  !
      real(kind=8), intent(out) :: velo  !
    end subroutine abscissa_velo_right
  end interface
  !
  interface
    subroutine abscissa_imaabs_left(head,imaabs)
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! Compute the leftmost (channelwise) absolute image frequency of
      ! input spectrum
      !---------------------------------------------------------------------
      type(header), intent(in)  :: head    !
      real(kind=8), intent(out) :: imaabs  !
    end subroutine abscissa_imaabs_left
  end interface
  !
  interface
    subroutine abscissa_imaabs_right(head,imaabs)
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! Compute the rightmost (channelwise) absolute image frequency of
      ! input spectrum
      !---------------------------------------------------------------------
      type(header), intent(in)  :: head    !
      real(kind=8), intent(out) :: imaabs  !
    end subroutine abscissa_imaabs_right
  end interface
  !
  interface abscissa_chan2imaabs
    subroutine abscissa_chan2imaabs_r4(head,chan,imaabs)
      use class_types
      !--------------------------------------------------------------------
      ! @ public-generic abscissa_chan2imaabs
      !--------------------------------------------------------------------
      type(header), intent(in)  :: head    !
      real(kind=4), intent(in)  :: chan    !
      real(kind=4), intent(out) :: imaabs  !
    end subroutine abscissa_chan2imaabs_r4
    subroutine abscissa_chan2imaabs_r8_head(head,chan,imaabs)
      use class_types
      !--------------------------------------------------------------------
      ! @ public-generic abscissa_chan2imaabs
      !  *** type(header) as argument ***
      !--------------------------------------------------------------------
      type(header), intent(in)  :: head    !
      real(kind=8), intent(in)  :: chan    !
      real(kind=8), intent(out) :: imaabs  !
    end subroutine abscissa_chan2imaabs_r8_head
    subroutine abscissa_chan2imaabs_r8_spe(spe,chan,imaabs)
      use class_types
      !--------------------------------------------------------------------
      ! @ public-generic abscissa_chan2imaabs
      !  *** type(class_spectro_t) as argument ***
      !--------------------------------------------------------------------
      type(class_spectro_t), intent(in)  :: spe     !
      real(kind=8),          intent(in)  :: chan    !
      real(kind=8),          intent(out) :: imaabs  !
    end subroutine abscissa_chan2imaabs_r8_spe
  end interface abscissa_chan2imaabs
  !
  interface abscissa_chan2sigabs
    subroutine abscissa_chan2sigabs_r4(head,chan,sigabs)
      use class_types
      !--------------------------------------------------------------------
      ! @ public-generic abscissa_chan2sigabs
      !--------------------------------------------------------------------
      type(header), intent(in)  :: head    !
      real(kind=4), intent(in)  :: chan    !
      real(kind=4), intent(out) :: sigabs  !
    end subroutine abscissa_chan2sigabs_r4
    subroutine abscissa_chan2sigabs_r8_head(head,chan,sigabs)
      use class_types
      !--------------------------------------------------------------------
      ! @ public-generic abscissa_chan2sigabs
      !  *** type(header) as argument ***
      !--------------------------------------------------------------------
      type(header), intent(in)  :: head    !
      real(kind=8), intent(in)  :: chan    !
      real(kind=8), intent(out) :: sigabs  !
    end subroutine abscissa_chan2sigabs_r8_head
    subroutine abscissa_chan2sigabs_r8_spe(spe,chan,sigabs)
      use class_types
      !--------------------------------------------------------------------
      ! @ public-generic abscissa_chan2sigabs
      !  *** type(class_spectro_t) as argument ***
      !--------------------------------------------------------------------
      type(class_spectro_t), intent(in)  :: spe     !
      real(kind=8),          intent(in)  :: chan    !
      real(kind=8),          intent(out) :: sigabs  !
    end subroutine abscissa_chan2sigabs_r8_spe
  end interface abscissa_chan2sigabs
  !
  interface abscissa_imaabs_middle
    subroutine abscissa_imaabs_middle_head(head,imaabs)
      use class_types
      !---------------------------------------------------------------------
      ! @ public-generic abscissa_imaabs_middle
      ! Compute the middle absolute image frequency of input spectrum
      !  *** type(header) as argument ***
      !---------------------------------------------------------------------
      type(header), intent(in)  :: head    !
      real(kind=8), intent(out) :: imaabs  !
    end subroutine abscissa_imaabs_middle_head
    subroutine abscissa_imaabs_middle_spe(spe,imaabs)
      use class_types
      !---------------------------------------------------------------------
      ! @ public-generic abscissa_imaabs_middle
      ! Compute the middle absolute image frequency of input spectrum
      !  *** type(class_spectro_t) as argument ***
      !---------------------------------------------------------------------
      type(class_spectro_t), intent(in)  :: spe     !
      real(kind=8),          intent(out) :: imaabs  !
    end subroutine abscissa_imaabs_middle_spe
  end interface abscissa_imaabs_middle
  !
  interface abscissa_sigabs2chan
    subroutine abscissa_sigabs2chan_r4(head,sigabs,chan)
      use class_types
      !--------------------------------------------------------------------
      ! @ public-generic abscissa_sigabs2chan
      !--------------------------------------------------------------------
      type(header), intent(in)  :: head    !
      real(kind=4), intent(in)  :: sigabs  !
      real(kind=4), intent(out) :: chan    !
    end subroutine abscissa_sigabs2chan_r4
    subroutine abscissa_sigabs2chan_r8_head(head,sigabs,chan)
      use class_types
      !--------------------------------------------------------------------
      ! @ public-generic abscissa_sigabs2chan
      !  *** type(header) as argument ***
      !--------------------------------------------------------------------
      type(header), intent(in)  :: head    !
      real(kind=8), intent(in)  :: sigabs  !
      real(kind=8), intent(out) :: chan    !
    end subroutine abscissa_sigabs2chan_r8_head
    subroutine abscissa_sigabs2chan_r8_spe(spe,sigabs,chan)
      use class_types
      !--------------------------------------------------------------------
      ! @ public-generic abscissa_sigabs2chan
      !  *** type(class_spectro_t) as argument ***
      !--------------------------------------------------------------------
      type(class_spectro_t), intent(in)  :: spe     !
      real(kind=8),          intent(in)  :: sigabs  !
      real(kind=8),          intent(out) :: chan    !
    end subroutine abscissa_sigabs2chan_r8_spe
  end interface abscissa_sigabs2chan
  !
  interface abscissa_sigabs_left
    subroutine abscissa_sigabs_left_head(head,sigabs)
      use class_types
      !---------------------------------------------------------------------
      ! @ public-generic abscissa_sigabs_left
      ! Compute the leftmost (channelwise) absolute signal frequency of
      ! input spectrum
      !  *** type(header) as argument ***
      !---------------------------------------------------------------------
      type(header), intent(in)  :: head    !
      real(kind=8), intent(out) :: sigabs  !
    end subroutine abscissa_sigabs_left_head
    subroutine abscissa_sigabs_left_spe(spe,sigabs)
      use class_types
      !---------------------------------------------------------------------
      ! @ public-generic abscissa_sigabs_left
      ! Compute the leftmost (channelwise) absolute signal frequency of
      ! input spectrum
      !  *** type(class_spectro_t) as argument ***
      !---------------------------------------------------------------------
      type(class_spectro_t), intent(in)  :: spe     !
      real(kind=8),          intent(out) :: sigabs  !
    end subroutine abscissa_sigabs_left_spe
  end interface abscissa_sigabs_left
  !
  interface abscissa_sigabs_middle
    subroutine abscissa_sigabs_middle_head(head,sigabs)
      use class_types
      !---------------------------------------------------------------------
      ! @ public-generic abscissa_sigabs_middle
      ! Compute the middle absolute signal frequency of input spectrum
      !  *** type(header) as argument ***
      !---------------------------------------------------------------------
      type(header), intent(in)  :: head    !
      real(kind=8), intent(out) :: sigabs  !
    end subroutine abscissa_sigabs_middle_head
    subroutine abscissa_sigabs_middle_spe(spe,sigabs)
      use class_types
      !---------------------------------------------------------------------
      ! @ public-generic abscissa_sigabs_middle
      ! Compute the middle absolute signal frequency of input spectrum
      !  *** type(class_spectro_t) as argument ***
      !---------------------------------------------------------------------
      type(class_spectro_t), intent(in)  :: spe     !
      real(kind=8),          intent(out) :: sigabs  !
    end subroutine abscissa_sigabs_middle_spe
  end interface abscissa_sigabs_middle
  !
  interface abscissa_sigabs_right
    subroutine abscissa_sigabs_right_head(head,sigabs)
      use class_types
      !---------------------------------------------------------------------
      ! @ public-generic abscissa_sigabs_right
      ! Compute the rightmost (channelwise) absolute signal frequency of
      ! input spectrum
      !  *** type(header) as argument ***
      !---------------------------------------------------------------------
      type(header), intent(in)  :: head    !
      real(kind=8), intent(out) :: sigabs  !
    end subroutine abscissa_sigabs_right_head
    subroutine abscissa_sigabs_right_spe(spe,sigabs)
      use class_types
      !---------------------------------------------------------------------
      ! @ public-generic abscissa_sigabs_right
      ! Compute the rightmost (channelwise) absolute signal frequency of
      ! input spectrum
      !  *** type(class_spectro_t) as argument ***
      !---------------------------------------------------------------------
      type(class_spectro_t), intent(in)  :: spe     !
      real(kind=8),          intent(out) :: sigabs  !
    end subroutine abscissa_sigabs_right_spe
  end interface abscissa_sigabs_right
  !
  interface
    subroutine class_accumulate(set,line,r,t,error)
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! CLASS Support routine for command ACCUMULATE
      !   Adds the R and T spectra together
      !   Reset the plotting constants
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      character(len=*),    intent(in)    :: line   ! Input command line
      type(observation),   intent(inout) :: r,t    ! The observations to be added
      logical,             intent(inout) :: error  ! Logical error flag
    end subroutine class_accumulate
  end interface
  !
  interface
    subroutine class_associate(set,line,r,error)
      use gbl_message
      use gkernel_types
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! Support routine for command
      !   ASSOCIATE  AAName  Value
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set
      character(len=*),    intent(in)    :: line
      type(observation),   intent(inout) :: r
      logical,             intent(inout) :: error
    end subroutine class_associate
  end interface
  !
  interface class_assoc_add
    subroutine class_assoc_add_reservedi41d(obs,name,ptr,error)
      use gbl_format
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public-generic class_assoc_add
      !   Add a reserved Associated Array to the Associated Section of the
      ! input observation. For reserved Arrays, the unit, format, and 2nd
      ! dimension are implicit and can not be customized by the caller.
      ! ---
      !  This version for:
      !   - reserved keywords
      !   - associate 1D I*4 pointer to the array in return
      !---------------------------------------------------------------------
      type(observation), intent(inout) :: obs     ! Observation
      character(len=*),  intent(in)    :: name    ! The array name
      integer(kind=4),   pointer       :: ptr(:)  ! Pointer on array
      logical,           intent(inout) :: error   ! Logical error flag
    end subroutine class_assoc_add_reservedi41d
    subroutine class_assoc_add_reservedr41d(obs,name,ptr,error)
      use gbl_format
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public-generic class_assoc_add
      !   Add a reserved Associated Array to the Associated Section of the
      ! input observation. For reserved Arrays, the unit, format, and 2nd
      ! dimension are implicit and can not be customized by the caller.
      ! ---
      !  This version for:
      !   - reserved keywords
      !   - associate 1D R*4 pointer to the array in return
      !---------------------------------------------------------------------
      type(observation), intent(inout) :: obs     ! Observation
      character(len=*),  intent(in)    :: name    ! The array name
      real(kind=4),      pointer       :: ptr(:)  ! Pointer on array
      logical,           intent(inout) :: error   ! Logical error flag
    end subroutine class_assoc_add_reservedr41d
    subroutine class_assoc_add_reservednum(obs,name,numarray,error)
      use gbl_format
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public-generic class_assoc_add
      !   Add a reserved Associated Array to the Associated Section of the
      ! input observation. For reserved Arrays, the unit, format, and 2nd
      ! dimension are implicit and can not be customized by the caller.
      ! ---
      !  This version for:
      !   - reserved keywords
      !   - get the array identifier in return
      !---------------------------------------------------------------------
      type(observation), intent(inout) :: obs       ! Observation
      character(len=*),  intent(in)    :: name      ! The array name
      integer(kind=4),   intent(out)   :: numarray  ! Array number
      logical,           intent(inout) :: error     ! Logical error flag
    end subroutine class_assoc_add_reservednum
    subroutine class_assoc_add_free_i41d(obs,name,unit,form,dim2,badi4,ptr,error)
      use gbl_format
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public-generic class_assoc_add
      !   Add an array to the Associated Section of the input observation
      ! The array name must be a 'free' (non-reserved) Associated Array
      ! ---
      ! This version for:
      !  - non-reserved arrays
      !  - I*4 bad value
      !  - associate 1D I*4 pointer to the array in return
      !---------------------------------------------------------------------
      type(observation), intent(inout) :: obs       ! Observation
      character(len=*),  intent(in)    :: name      ! The array name
      character(len=*),  intent(in)    :: unit      ! Unit for values
      integer(kind=4),   intent(in)    :: form      ! Format on disk
      integer(kind=4),   intent(in)    :: dim2      ! 2nd dimension, 0 for 1D array
      integer(kind=4),   intent(in)    :: badi4     ! Bad value
      integer(kind=4),   pointer       :: ptr(:)    ! Pointer on array
      logical,           intent(inout) :: error     ! Logical error flag
    end subroutine class_assoc_add_free_i41d
    subroutine class_assoc_add_free_i4num(obs,name,unit,form,dim2,badi4,numarray,error)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public-generic class_assoc_add
      !   Add an array to the Associated Section of the input observation
      ! The array name must be a 'free' (non-reserved) Associated Array
      ! ---
      ! This version for:
      !  - non-reserved arrays
      !  - I*4 bad value
      !  - get the array identifier in return
      !---------------------------------------------------------------------
      type(observation), intent(inout) :: obs       ! Observation
      character(len=*),  intent(in)    :: name      ! The array name
      character(len=*),  intent(in)    :: unit      ! Unit for values
      integer(kind=4),   intent(in)    :: form      ! Format on disk
      integer(kind=4),   intent(in)    :: dim2      ! 2nd dimension, 0 for 1D array
      integer(kind=4),   intent(in)    :: badi4     ! Bad value
      integer(kind=4),   intent(out)   :: numarray  ! Array number
      logical,           intent(inout) :: error     ! Logical error flag
    end subroutine class_assoc_add_free_i4num
    subroutine class_assoc_add_free_r41d(obs,name,unit,form,dim2,badr4,ptr,error)
      use gbl_format
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public-generic class_assoc_add
      !   Add an array to the Associated Section of the input observation
      ! The array name must be a 'free' (non-reserved) Associated Array
      ! ---
      ! This version for:
      !  - non-reserved arrays
      !  - R*4 bad value
      !  - associate 1D R*4 pointer to the array in return
      !---------------------------------------------------------------------
      type(observation), intent(inout) :: obs       ! Observation
      character(len=*),  intent(in)    :: name      ! The array name
      character(len=*),  intent(in)    :: unit      ! Unit for values
      integer(kind=4),   intent(in)    :: form      ! Format on disk
      integer(kind=4),   intent(in)    :: dim2      ! 2nd dimension, 0 for 1D array
      real(kind=4),      intent(in)    :: badr4     ! Bad value
      real(kind=4),      pointer       :: ptr(:)    ! Pointer on array
      logical,           intent(inout) :: error     ! Logical error flag
    end subroutine class_assoc_add_free_r41d
    subroutine class_assoc_add_free_r4num(obs,name,unit,form,dim2,badr4,numarray,error)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public-generic class_assoc_add
      !   Add an array to the Associated Section of the input observation
      ! The array name must be a 'free' (non-reserved) Associated Array
      ! ---
      ! This version for:
      !  - non-reserved arrays
      !  - R*4 bad value
      !  - get the array identifier in return
      !---------------------------------------------------------------------
      type(observation), intent(inout) :: obs       ! Observation
      character(len=*),  intent(in)    :: name      ! The array name
      character(len=*),  intent(in)    :: unit      ! Unit for values
      integer(kind=4),   intent(in)    :: form      ! Format on disk
      integer(kind=4),   intent(in)    :: dim2      ! 2nd dimension, 0 for 1D array
      real(kind=4),      intent(in)    :: badr4     ! Bad value
      integer(kind=4),   intent(out)   :: numarray  ! Array number
      logical,           intent(inout) :: error     ! Logical error flag
    end subroutine class_assoc_add_free_r4num
  end interface class_assoc_add
  !
  interface
    subroutine newdat_assoc(set,obs,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! Recompute the Associated Section related data, typically when a new
      ! observation is loaded. Code is optimized to avoid deleting or
      ! defining the Sic variables if there is no need.
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set
      type(observation),   intent(inout) :: obs
      logical,             intent(inout) :: error
    end subroutine newdat_assoc
  end interface
  !
  interface
    subroutine simple_waverage(rdata1,rdataw,rbad,  &
                               sdata1,sdataw,sbad,  &
                               schanmin,schanmax,contaminate,wup)
      !---------------------------------------------------------------------
      ! @ public
      ! Simple summation in the case SET ALIGN is CHANNEL. For efficiency
      ! purpose, addition is performed only in a given range were we know
      ! the input data array contributes.
      ! Weighting :
      ! * All channels are assumed to be aligned
      ! * Input weights should have been computed according to SET WEIGHT.
      !   Output weights (possibly non-uniform) are computed with special
      !   care.
      ! * Blanked channels propagates or not in output sum according to
      !   input parameter 'contaminate'
      !---------------------------------------------------------------------
      real(kind=4),    intent(in)    :: rdata1(:)    ! Data to be added
      real(kind=4),    intent(in)    :: rdataw(:)    ! Weigth of data to be added
      real(kind=4),    intent(in)    :: rbad         ! Bad value for the data to be added
      real(kind=4),    intent(inout) :: sdata1(:)    ! Current sum
      real(kind=4),    intent(inout) :: sdataw(:)    ! Weight of current sum
      real(kind=4),    intent(in)    :: sbad         ! Bad value for the current sum
      integer(kind=4), intent(in)    :: schanmin     ! First channel
      integer(kind=4), intent(in)    :: schanmax     ! Last channel
      logical,         intent(in)    :: contaminate  ! Bad channels contaminate output or not?
      logical,         intent(in)    :: wup          ! Update weight in return?
    end subroutine simple_waverage
  end interface
  !
  interface
    subroutine class_average(set,line,obs,error,user_function)
      use gbl_message
      use class_averaging
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! CLASS Support routine for command AVERAGE
      ! AVERAGE
      !   1  [/NOMATCH]
      !   2  [/RESAMPLE]
      !   3  [/NOCHECK]
      !   4  [/WEIGHT]
      ! * Average scans of current index together with various weights and
      !   options.
      ! * Update the origin information and reset the plotting constants
      !---------------------------------------------------------------------
      type(class_setup_t), intent(inout) :: set            !
      character(len=*),    intent(in)    :: line           ! Input command line
      type(observation),   intent(inout) :: obs            !
      logical,             intent(inout) :: error          ! Logical error flag
      logical,             external      :: user_function  ! Routine to handle the User sections
    end subroutine class_average
  end interface
  !
  interface
    subroutine class_stitch(set,line,obs,error,user_function)
      use gbl_message
      use gbl_constant
      use class_averaging
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! CLASS Support routine for command STITCH
      ! STITCH
      !   1  [/NOMATCH]
      !   2  [/RESAMPLE]
      !   3  [/IMAGE]
      !   4  [/LINE]
      !   5  [/TELESCOPE]
      !   6  [/NOCHECK]
      !   7  [/WEIGHT]
      !---------------------------------------------------------------------
      type(class_setup_t), intent(inout) :: set            !
      character(len=*),    intent(in)    :: line           ! Input command line
      type(observation),   intent(inout) :: obs            !
      logical,             intent(inout) :: error          ! Logical error flag
      logical,             external      :: user_function  ! Routine to handle the User sections
    end subroutine class_stitch
  end interface
  !
  interface
    subroutine class_rms(set,line,obs,error,user_function)
      use gbl_message
      use class_averaging
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! CLASS Support routine for command RMS
      ! RMS
      !   1  [/NOCHECK]
      !   2  [/RESAMPLE]
      !   3  [/WEIGHT]
      ! * Average and rms scans of current index together with various
      !   weights and options.
      ! * Update the origin information and reset the plotting constants
      !---------------------------------------------------------------------
      type(class_setup_t), intent(inout) :: set            !
      character(len=*),    intent(in)    :: line           ! Input command line
      type(observation),   intent(inout) :: obs            !
      logical,             intent(inout) :: error          ! Logical error flag
      logical,             external      :: user_function  ! Routine to handle the User sections
    end subroutine class_rms
  end interface
  !
  interface
    subroutine baseline(set,line,r,error,user_function)
      use gbl_constant
      use gbl_format
      use gbl_message
      use class_data
      use class_index
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! CLASS Support routine for command
      !  BASELINE [N|SINUS|LAST]
      ! 1         [/PLOT [ipen]]
      ! 2         [/CONTINUUM [flux]]
      ! 3         [/INDEX]
      ! 4         [/OBS]
      !  Computes and optionnaly plots a polynomial baseline of degree N or
      ! the default value defined in SET BASE. Optionnaly uses the last
      ! computed baseline, i.e. the last polynomial coefficients.
      !---------------------------------------------------------------------
      type(class_setup_t), intent(inout) :: set            !
      character(len=*),    intent(in)    :: line           ! command line
      type(observation),   intent(inout) :: r              !
      logical,             intent(inout) :: error          ! flag
      logical,             external      :: user_function  ! External function
    end subroutine baseline
  end interface
  !
  interface
    subroutine class_box(set,line,r,error)
      use gbl_message
      use class_data
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! CLASS Support routine for command
      !    BOX [C1 C2 C3 C4]
      ! 1      [/INDEX]
      ! 2      [/OBS]
      ! 3      [/UNIT Type [LOWER|UPPER]]
      ! Setup the units to plot a box (and a wedge).
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      character(len=*),    intent(in)    :: line   ! Command line
      type(observation),   intent(in)    :: r      !
      logical,             intent(inout) :: error  ! flag
    end subroutine class_box
  end interface
  !
  interface
    subroutine class_catalog(line,error)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! Support routine for command
      !     CATALOG [filename]
      !     1. /STATUS
      !---------------------------------------------------------------------
      character(len=*) :: line
      logical :: error
    end subroutine class_catalog
  end interface
  !
  interface
    subroutine class_cells(set,line,error,user_function)
      use gildas_def
      use gbl_constant
      use gbl_message
      use class_types
      use plot_formula
      !---------------------------------------------------------------------
      ! @ public
      ! CLASS ANALYSE Support routine for command
      !
      ! MAP [WHERE | MATCH | KEEP]
      ! 1   [/CELL X Y]
      ! 2   [/GRID]
      ! 3   [/NOLABEL]
      ! 4   [/NUMBER]
      ! 5   [/BASE [Ipen]]
      !
      ! Make a map of positions in the index.
      ! WHERE: put a marker at spectra locations; do not display the spectra
      ! MATCH: force identical scale on X and Y axis
      ! KEEP : take the previous grid
      ! Keywords MATCH and WHERE may be combined
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      character(len=*),    intent(in)    :: line           ! Input command line
      logical,             intent(inout) :: error          ! Logical error flag
      logical,             external      :: user_function  !
    end subroutine class_cells
  end interface
  !
  interface
    subroutine class_lmv(set,line,error,user_function)
      use gildas_def
      use gbl_message
      use class_parameter
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! CLASS  Support routine for command
      ! LMV CubeName
      !     [/SCAN LAMBDA|BETA]
      !     [/STEP N]
      !     [/LINE LineCube]
      !     [/FORCE What Value]
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      character(len=*),    intent(in)    :: line           ! Command line
      logical,             intent(inout) :: error          ! Logical error flag
      logical,             external      :: user_function  !
    end subroutine class_lmv
  end interface
  !
  interface
    subroutine class_message_set_id(id)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! Alter library id into input id. Should be called by the library
      ! which wants to share its id with the current one.
      !---------------------------------------------------------------------
      integer, intent(in) :: id         !
    end subroutine class_message_set_id
  end interface
  !
  interface
    subroutine class_message(mkind,procname,message)
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! Messaging facility for the current library. Calls the low-level
      ! (internal) messaging routine with its own identifier.
      !---------------------------------------------------------------------
      integer,          intent(in) :: mkind     ! Message kind
      character(len=*), intent(in) :: procname  ! Name of calling procedure
      character(len=*), intent(in) :: message   ! Message string
    end subroutine class_message
  end interface
  !
  interface
    subroutine class_fill(set,line,r,error,user_function)
      use gildas_def
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! CLASS ANALYSE Support routine for command
      !   FILL s1 e1 [[s2 e2] ...]  [/INTER] [/NOISE [sigma]] [/BLANK [bval]]
      !
      ! * "Clean" some specified windows, i.e. interpolate them
      !   from first and last channels.
      ! * Units are the current units
      ! * Possibility to specify several windows
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      character(len=*),    intent(in)    :: line           ! Command line
      type(observation),   intent(inout) :: r              !
      logical,             intent(inout) :: error          ! Logical error flag
      logical,             external      :: user_function  ! Header handling
    end subroutine class_fill
  end interface
  !
  interface
    subroutine class_comment(line,r,error)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! CLASS ANALYSE Support routine for command
      !  COMMENT APPEND or WRITE "New comment text"
      !          EDIT
      !          READ
      !          DELETE
      ! Reads, writes and modifies the comment section.
      !---------------------------------------------------------------------
      character(len=*),  intent(in)    :: line   ! Command line
      type(observation), intent(inout) :: r      !
      logical,           intent(inout) :: error  ! flag
    end subroutine class_comment
  end interface
  !
  interface
    subroutine index_consistency_check(set,line,error,user_function)
      use class_index
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! Generic for spectroscopic or continuum data
      ! Support routine for command
      !     CONSISTENCY [/NOCHECK [SOURCE|POSITION|OFFSET|LINE|SPECTROSCOPY|
      !                            CALIBRATION|SWITCHING]]
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      character(len=*),    intent(in)    :: line           ! Command line
      logical,             intent(inout) :: error          ! Logical error flag
      logical,             external      :: user_function  !
    end subroutine index_consistency_check
  end interface
  !
  interface
    subroutine class_copy(set,line,error,user_function)
      use gbl_message
      use gkernel_types
      use class_index
      use class_common
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! CLASS support routine for command
      !   COPY
      ! 1    [/SORTED]
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      character(len=*),    intent(in)    :: line           ! Command line
      logical,             intent(inout) :: error          ! Logical error flag
      logical,             external      :: user_function  !
    end subroutine class_copy
  end interface
  !
  interface
    subroutine deconv_init(set,line,r,error,user_function)
      use gildas_def
      use gbl_constant
      use gbl_message
      use class_index
      use class_types
      !-------------------------------------------------------------------
      ! @ public
      ! Support routine for command
      !
      ! INITIALIZE [cont_offset]
      ! 1          [/MODEL file]
      ! 2          [/SIMULATE file noise]
      ! 3          [/WEIGHT weight]
      ! 4          [/BMSW n [a p ph [...]]
      !
      ! Initializes the deconvolution process.
      !
      ! - read the DSB scans from the opened input CLASS file.
      ! - contatenate all these DSB scan into a single vector called
      !   dsb_spectrum
      ! - determine information about the output SSB from information
      !   about the DSB : number of channels,frequency resolution,min and
      !   max frequencies...
      ! - It sets up a flat null model.
      ! - Allocate global arrays
      !-------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      character(len=*),    intent(in)    :: line           ! Input command line
      type(observation),   intent(inout) :: r              !
      logical,             intent(out)   :: error          ! Error flag
      logical,             external      :: user_function  ! External function, intent(in)
    end subroutine deconv_init
  end interface
  !
  interface
    subroutine deconv(set,line,r,error,user_function)
      use gildas_def
      use gbl_message
      use gbl_constant
      use class_types
      !-------------------------------------------------------------------
      ! @ public
      ! Support routine for command
      !
      ! DECONVOLVE tolerance itmax [lambda1 [lambda2 [lambda3]]]
      ! 1          [/GAIN [first_guess]]      !!CC 19.6.2013
      ! 2          [/DERIV]
      ! 3          [/BMSW n [NOA] [NOP] [NOPH]]
      ! 4          [/NOCHANNELS]
      ! 5          [/KEEP]
      !
      !-------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set
      character(len=*),    intent(in)    :: line
      type(observation),   intent(inout) :: r
      logical,             intent(out)   :: error
      logical,             external      :: user_function
    end subroutine deconv
  end interface
  !
  interface
    subroutine class_diff(set,line,r,t,error,user_function)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      !  Support routine for command
      !    EXPERIM\DIFF [Num1 Num2]
      !  Print the differences between 2 observations
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set
      character(len=*),    intent(in)    :: line
      type(observation),   intent(in)    :: r,t
      logical,             intent(inout) :: error
      logical,             external      :: user_function
    end subroutine class_diff
  end interface
  !
  interface
    subroutine class_divide(set,line,r,t,error)
      use gildas_def
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! CLASS ANALYSE Support routine for command
      !     DIVIDE Threshold
      !
      !  R --> R/T  T --> T
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      character(len=*),    intent(in)    :: line   ! Command line
      type(observation),   intent(inout) :: r      !
      type(observation),   intent(in)    :: t      !
      logical,             intent(inout) :: error  ! Flag
    end subroutine class_divide
  end interface
  !
  interface
    subroutine class_draw(set,line,r,error)
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! Support routine for command
      !   DRAW [Args] [/PEN Ipen]
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      character(len=*),    intent(inout) :: line   ! Command line
      type(observation),   intent(inout) :: r      !
      logical,             intent(inout) :: error  ! Flag
    end subroutine class_draw
  end interface
  !
  interface
    subroutine class_drop(line,r,error)
      use gbl_message
      use class_parameter
      use class_index
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! CLASS Support routine for command
      !   DROP [Number] [Version]
      ! Deletes an observation from current index. Default is current
      ! observation in R buffer.
      !---------------------------------------------------------------------
      character(len=*),  intent(in)    :: line   ! Command line
      type(observation), intent(in)    :: r      !
      logical,           intent(inout) :: error  ! Logical error flag
    end subroutine class_drop
  end interface
  !
  interface
    subroutine extract(set,line,r,error,user_function)
      use gbl_message
      use gkernel_types
      use class_common
      use class_index
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! CLASS Support routine for command
      !   EXTRACT [X1 X2 [Unit]] [/INDEX]
      ! Extract the subset of a spectrum in the given range
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      character(len=*),    intent(in)    :: line           ! Input command line
      type(observation),   intent(inout) :: r              !
      logical,             intent(inout) :: error          ! Error flag
      logical,             external      :: user_function  !
    end subroutine extract
  end interface
  !
  interface
    subroutine class_file(set,line,error)
      use gildas_def
      use gbl_message
      use class_index
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! LAS Support routine for command
      ! FILE IN|OUT|BOTH|UPDATE Filename [SINGLE|MULTIPLE]
      !      [/OVERWRITE]
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)  :: set    !
      character(len=*),    intent(in)  :: line   ! Command line
      logical,             intent(out) :: error  ! Error status
    end subroutine class_file
  end interface
  !
  interface
    subroutine class_filter(line,r,error)
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !---------------------------------------------------------------------
      character(len=*),  intent(in)    :: line   !
      type(observation), intent(inout) :: r      !
      logical,           intent(inout) :: error  !
    end subroutine class_filter
  end interface
  !
  interface
    subroutine find(set,line,error)
      use gbl_message
      use class_index
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! CLASS Support routine for command
      !    FIND [APPEND|NEW_DATA|UPDATE]
      !  1       [/ALL]
      !  2       [/LINE]
      !  3       [/NUMBER]
      !  4       [/SCAN]
      !  5       [/OFFSET]
      !  6       [/SOURCE]
      !  7       [/RANGE]
      !  8       [/QUALITY]
      !  9       [/TELESCOPE]
      ! 10       [/SUBSCAN]
      ! 11       [/ENTRY]
      ! 12       [/OBSERVED]
      ! 13       [/REDUCED]
      ! 14       [/FREQUENCY]
      ! 15       [/SECTION]
      ! 16       [/USER]
      ! 17       [/MASK]
      ! 18       [/POSITION]
      ! 19       [/SWITCHMODE]
      !  Find a set of observations according to setup search rules and
      ! command options.
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      character(len=*),    intent(in)    :: line   ! Command line
      logical,             intent(inout) :: error  ! Error status
    end subroutine find
  end interface
  !
  interface
    subroutine fits_class(set,line,r,error,user_function)
      use gildas_def
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! CLASS Support for command FITS
      !
      ! GILDAS Syntax
      !     FITS File.fits FROM File.gdf /STYLE [/XTENSION Number]
      !     FITS File.gdf TO File.fits /STYLE /BITS
      ! Launches the FITS_GILDAS or GILDAS_FITS task to do the job.
      !
      ! CLASS Syntax
      !     FITS READ File.fits  [/CHECK]
      !     FITS WRITE File.fits [/BITS Nbits] [/MODE SPECTRUM|INDEX] [/CHECK]
      !
      ! Option sequence
      !     /BITS
      !     /STYLE
      !     /XTENSION
      !     /MODE
      !     /CHECK
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      character(len=*),    intent(in)    :: line           ! Command line
      type(observation),   intent(inout) :: r              !
      logical,             intent(inout) :: error          ! Error status
      logical,             external      :: user_function  ! intent(in)
    end subroutine fits_class
  end interface
  !
  interface
    subroutine class_fft(set,line,r,error,user_function)
      use gbl_message
      use class_data
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! CLASS ANALYSE Support routine for command
      !
      !   FFT
      ! 1     [/REMOVE wmin wmax]
      ! 2     [[s1 e1] [s2 e2] ... [s10 e10] /KILL]
      ! 3     [/INDEX]
      ! 4     [/OBS]
      ! 5     [/NOCURS]
      ! 6     [/CURS]   Obsolete since 10-sep-2010
      !
      ! Plots the power spectrum of the current scan
      !---------------------------------------------------------------------
      type(class_setup_t), intent(inout)         :: set            !
      character(len=*),    intent(in)            :: line           ! Command line
      type(observation),   intent(inout), target :: r              !
      logical,             intent(inout)         :: error          ! Error status
      logical,             external              :: user_function  !
    end subroutine class_fft
  end interface
  !
  interface
    subroutine get(set,line,r,error,user_function)
      use gildas_def
      use gbl_constant
      use gbl_message
      use class_parameter
      use class_index
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! CLASS Support routine for command
      !      GET Number|NEXT|FIRST|LAST|PREVIOUS [Version]
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      character(len=*),    intent(in)    :: line           ! Command line
      type(observation),   intent(inout) :: r              !
      logical,             intent(inout) :: error          ! Error flag
      logical,             external      :: user_function  !
    end subroutine get
  end interface
  !
  interface
    subroutine class_header(set,r,error)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !  Gives full header information
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set
      type(observation),   intent(in)    :: r
      logical,             intent(inout) :: error
    end subroutine class_header
  end interface
  !
  interface
    subroutine ignore(line,error)
      use gkernel_types
      use class_parameter
      use class_index
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! CLASS support routine for command
      !
      ! IGNORE [List_of_Observations]
      ! 1      [/SCAN]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine ignore
  end interface
  !
  interface
    subroutine tagout(line,r,error)
      use gbl_message
      use gkernel_types
      use classic_api
      use class_parameter
      use class_index
      use class_data
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! CLASS Support routine for command
      !   TAG Quality List_of_scans
      !---------------------------------------------------------------------
      character(len=*),  intent(in)    :: line   ! Command line
      type(observation), intent(inout) :: r      !
      logical,           intent(inout) :: error  ! Logical error flag
    end subroutine tagout
  end interface
  !
  interface
    subroutine allinfo(obs,v1,v2,v,s0,data1)
      use gildas_def
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !---------------------------------------------------------------------
      type(observation), intent(in)  :: obs    !
      real(kind=8),      intent(in)  :: v1     ! Lower bound of velocity interval
      real(kind=8),      intent(in)  :: v2     ! Upper bound of velocity interval
      real(kind=4),      intent(out) :: v      !
      real(kind=4),      intent(out) :: s0     !
      real(kind=4),      intent(out) :: data1  !
    end subroutine allinfo
  end interface
  !
  interface
    subroutine classcore_fileout_open(set,spec,lnew,lover,lsize,lsingle,error)
      use classic_api
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !  Public entry point to open a new or reopen an old output file.
      !---------------------------------------------------------------------
      type(class_setup_t),        intent(in)    :: set      !
      character(len=*),           intent(in)    :: spec     ! File name including extension
      logical,                    intent(in)    :: lnew     ! Should it be a new file or an old?
      logical,                    intent(in)    :: lover    ! If new, overwrite existing file or raise an error?
      integer(kind=entry_length), intent(in)    :: lsize    ! If new, maximum number of observations allowed
      logical,                    intent(in)    :: lsingle  ! If new, should spectra be unique?
      logical,                    intent(inout) :: error    ! Logical error flag
    end subroutine classcore_fileout_open
  end interface
  !
  interface
    subroutine classcore_fileout_old(set,spec,nspec,update,error)
      use gildas_def
      use gbl_message
      use gbl_format
      use gbl_convert
      use gkernel_types
      use classic_api
      use class_common
      use class_index
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !  Close any output file, set the file specifications for output,
      !  open this file.
      !  (also selected for input if infile not defined)
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)  :: set     !
      character(len=*),    intent(in)  :: spec    ! File name including extension
      integer(kind=4),     intent(in)  :: nspec   ! Length of SPEC
      logical,             intent(in)  :: update  ! Open it in update mode?
      logical,             intent(out) :: error   ! Logical error flag
    end subroutine classcore_fileout_old
  end interface
  !
  interface
    subroutine classcore_fileout_new(set,spec,nspec,lsize,lsingle,lover,error)
      use gildas_def
      use gbl_format
      use gbl_convert
      use gbl_message
      use class_common
      use class_index
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !  Initializes a new output file
      !  Close any output file, set the file specifications for output
      ! open this file.
      !---------------------------------------------------------------------
      type(class_setup_t),        intent(in)  :: set      !
      character(len=*),           intent(in)  :: spec     ! File name including extension
      integer(kind=4),            intent(in)  :: nspec    ! Length of SPEC
      integer(kind=entry_length), intent(in)  :: lsize    ! Maximum number of observations allowed
      logical,                    intent(in)  :: lsingle  ! Should spectra be unique ?
      logical,                    intent(in)  :: lover    ! Overwrite existing file ?
      logical,                    intent(out) :: error    ! Logical error flag
    end subroutine classcore_fileout_new
  end interface
  !
  interface
    subroutine classcore_fileout_close(error)
      use class_common
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !  Close the output file without closing the input file if they are
      ! the same
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Error status
    end subroutine classcore_fileout_close
  end interface
  !
  interface
    subroutine class_files_close(error)
      use class_common
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !  Close both input and/or output files if they are opened, also
      ! taking care if they are identical
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Error status
    end subroutine class_files_close
  end interface
  !
  interface
    subroutine class_luns_free(error)
      use class_common
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! Free the 2 luns for filein and fileout support
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine class_luns_free
  end interface
  !
  interface
    function filein_is_fileout()
      use class_common
      !---------------------------------------------------------------------
      ! @ public (for Class libraries only)
      !  Return .true. if output file is input file
      !---------------------------------------------------------------------
      logical :: filein_is_fileout  ! Function value on return
    end function filein_is_fileout
  end interface
  !
  interface
    subroutine class_list(set,line,error)
      use gbl_message
      use class_index
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! CLASS Support routine for command
      !    LAS\LIST [IN|OUT]
      ! 1           [/BRIEF]
      ! 2           [/LONG]
      ! 3           [/SCAN]
      ! 4           [/OUTPUT File]
      ! 5           [/TOC List]
      !
      ! Output format is BRIEF | NORMAL | LONG
      ! LIST     list the index built by FIND
      ! LIST IN  list the whole input file
      ! LIST OUT list the whole output file (LONG format unsupported here)
      !---------------------------------------------------------------------
      type(class_setup_t), intent(inout) :: set    !
      character(len=*),    intent(in)    :: line   ! Input argument line
      logical,             intent(inout) :: error  ! Logical error flag
    end subroutine class_list
  end interface
  !
  interface
    subroutine class_toc_clean(error)
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! CLASS Support routine for command LIST /TOC
      ! Global the global variable holding the TOC
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  !
    end subroutine class_toc_clean
  end interface
  !
  interface
    subroutine load_2d(set,line,error,user_function)
      use gildas_def
      use gbl_constant
      use gbl_message
      use class_types
      use class_index
      use class_data
      !---------------------------------------------------------------------
      ! @ public
      ! CLASS Support routine for command
      !   LOAD /NOCHECK
      ! Convert the entire index in a single 2-D data set for further plot
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)  :: set            !
      character(len=*),    intent(in)  :: line           ! Command Line
      logical,             intent(out) :: error          ! Return error code
      logical,             external    :: user_function  ! handling of user defined sections, intent(in)
    end subroutine load_2d
  end interface
  !
  interface
    subroutine class_median(set,line,r,error)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      !  Support routine for command
      !   EXP\MEDIAN [Width] [Sampling]
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      character(len=*),    intent(in)    :: line   !
      type(observation),   intent(inout) :: r      !
      logical,             intent(inout) :: error  !
    end subroutine class_median
  end interface
  !
  interface
    subroutine memorize(line,r,error)
      use gildas_def
      use gbl_constant
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! CLASS ANALYSE Support for command
      !   MEMORIZE Name [/DELETE]
      !  Put a copy of R spectrum in some user defined place
      !---------------------------------------------------------------------
      character(len=*),  intent(in)    :: line  ! Command line
      type(observation), intent(inout) :: r     !
      logical,           intent(inout) :: error ! Error flag
    end subroutine memorize
  end interface
  !
  interface
    subroutine memorize_free_all
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !  Cleanly free the all the MEMORY slot
      !---------------------------------------------------------------------
    end subroutine memorize_free_all
  end interface
  !
  interface
    subroutine retrieve(set,line,out,error,user_function)
      use gildas_def
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! Support for command
      !   RETRIEVE Name
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      character(len=*),    intent(in)    :: line           ! Input command line
      type(observation),   intent(inout) :: out            !
      logical,             intent(inout) :: error          ! Logical error flag
      logical,             external      :: user_function  !
    end subroutine retrieve
  end interface
  !
  interface
    subroutine merge(set,all,error,user_function)
      use gbl_constant
      use gbl_message
      use class_index
      use class_types
      !----------------------------------------------------------------------
      ! @ public
      ! Support routine for command MERGE.
      ! Gather observations in the index into a single observations
      !
      ! * No consistency check is done
      ! * The resulting R buffer is labeled as "irregularly sampled"
      ! * time is sum(time)
      ! * Tsys is <Tsys> weighted by time
      ! * tau  is <tau>  weighted by time
      !----------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set
      type(observation),   intent(inout) :: all
      logical,             intent(inout) :: error
      logical,             external      :: user_function
    end subroutine merge
  end interface
  !
  interface
    subroutine model(set,line,r,t,error,user_function)
      use gildas_def
      use gbl_format
      use gbl_constant
      use phys_const
      use gbl_message
      use gkernel_types
      use class_types
      !----------------------------------------------------------------------
      ! @ public
      ! CLASS ANALYSE Support routine for command
      ! MODEL
      ! 1     [/BLANK]
      ! 2     [/REGULAR [Xref Xval RestFrequency]]
      ! 3     [/FREQUENCY LineName RestFreq]
      ! 4     [/XAXIS Xref Xval Xinc Unit]
      !
      !  Define a spectrum from a SIC 1D array.
      !----------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      character(len=*),    intent(in)    :: line   ! Input command line
      type(observation),   intent(inout) :: r      !
      type(observation),   intent(in)    :: t      !
      logical,             intent(inout) :: error  ! Error status
      logical,             external      :: user_function
    end subroutine model
  end interface
  !
  interface
    subroutine modify_vconvention(set,head,error)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! Support routine for command:
      !  MODIFY VCONVENTION
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      type(header),        intent(inout) :: head   !
      logical,             intent(inout) :: error  !
    end subroutine modify_vconvention
  end interface
  !
  interface
    subroutine modify_vdirection(set,head,error)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! Support routine for command:
      !  MODIFY VDIRECTION
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      type(header),        intent(inout) :: head   !
      logical,             intent(inout) :: error  !
    end subroutine modify_vdirection
  end interface
  !
  interface
    subroutine multi(set,line,r,error)
      use gildas_def
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! Support routine for command
      !  MULTIPLY Factor
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set
      character(len=*),    intent(in)    :: line
      type(observation),   intent(inout) :: r
      logical,             intent(inout) :: error
    end subroutine multi
  end interface
  !
  interface
    subroutine new_data (line,error)
      use gbl_format
      use gbl_message
      use class_index
      use class_common
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! CLASS Support routine for command
      !   NEW_DATA [Interval]
      !  Wait until new data is present in the input file. No update the
      ! input and current indexes.
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line   !
      logical,          intent(out) :: error  !
    end subroutine new_data
  end interface
  !
  interface
    subroutine newdat(set,obs,error)
      use gildas_def
      use gbl_constant
      use gbl_message
      use gkernel_types
      use class_setup_new
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !  Recompute scaling factors when a new scan is read in. Also called
      ! after changes in plotting mode or unit. Calls only NEWLIMX and
      ! NEWLIMY. Support for continuum
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      type(observation),   intent(inout) :: obs    ! Current observation
      logical,             intent(inout) :: error  ! Logical error flag
    end subroutine newdat
  end interface
  !
  interface
    subroutine class_noise(set,line,r,error,user_function)
      use gildas_def
      use gbl_message
      use class_types
      !----------------------------------------------------------------------
      ! @ public
      ! CLASS ANALYSE Support routine for command
      !   NOISE [Sigma [New]]
      ! Compute a spectrum with a gaussian random noise of specified sigma.
      ! The spectrum is immediately plotted, or loaded into R memory.
      !----------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set             !
      character(len=*),    intent(in)    :: line            ! Input command line
      type(observation),   intent(inout) :: r               !
      logical,             external      :: user_function   !
      logical,             intent(inout) :: error           ! Error flag
    end subroutine class_noise
  end interface
  !
  interface
    subroutine class_plot(set,line,r,error)
      use gbl_constant
      use gbl_message
      use class_data
      use class_types
      !----------------------------------------------------------------------
      ! @ public
      ! CLASS Support routine for command
      !      PLOT [ArrayName]
      ! 1         [/INDEX]
      ! 2         [/OBS]
      ! Plot specified observation for command
      !----------------------------------------------------------------------
      type(class_setup_t), intent(inout) :: set    !
      character(len=*),    intent(in)    :: line   ! Command line
      type(observation),   intent(inout) :: r      !
      logical,             intent(inout) :: error  ! Error status
    end subroutine class_plot
  end interface
  !
  interface
    subroutine popup(set,line,error,user_function)
      use gbl_message
      use classic_api
      use class_parameter
      use class_popup
      use class_data
      use class_setup_new
      use class_types
      !----------------------------------------------------------------------
      ! @ public
      ! CLASS Support routine for command
      ! POPUP [Number | OffX OffY]
      !----------------------------------------------------------------------
      type(class_setup_t), intent(in)  :: set           !
      character(len=*),    intent(in)  :: line          ! Input command line
      logical,             intent(out) :: error         ! Logical error flag
      logical,             external    :: user_function !
    end subroutine popup
  end interface
  !
  interface
    subroutine class_print(set,line,error)
      use gildas_def
      use image_def
      use gbl_format
      use gbl_constant
      use gbl_message
      use class_setup_new
      use class_types
      use class_common
      use class_index
      !----------------------------------------------------------------------
      ! @ public
      ! ANALYSE Support routine for command
      !	ANALYSE\PRINT   FIT   [/OUTPUT filename]
      !			AREA    [V1 [V2 [V3 [...]]]]
      !			MOMENT  [V1 V2 [V3 V4 [...]]]
      !			CHANNEL Liste
      !----------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      character(len=*),    intent(in)    :: line   ! Input command line
      logical,             intent(inout) :: error  ! Output error flag
    end subroutine class_print
  end interface
  !
  interface
    subroutine pyclass_obsx_val(obs,ival,iunitin,oval,ounitin,error)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! Convert the input value from input unit to the output unit, for
      ! the current R spectrum in memory. Frequency (signal or image) units
      ! (in and out) assumed to be absolute frequencies.
      !---------------------------------------------------------------------
      type(observation), intent(in)  :: obs      !
      real(kind=8),      intent(in)  :: ival     ! Input value
      character(len=*),  intent(in)  :: iunitin  ! Input unit
      real(kind=8),      intent(out) :: oval     ! Output value
      character(len=*),  intent(in)  :: ounitin  ! Output unit
      logical,           intent(out) :: error    ! Logical error flag
    end subroutine pyclass_obsx_val
  end interface
  !
  interface
    subroutine pyclass_obsx_minmax(set,obs,unitin,mini,maxi,error)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! Return the X range of the R spectrum
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)  :: set     !
      type(observation),   intent(in)  :: obs     !
      character(len=*),    intent(in)  :: unitin  ! Unit for returning values. Blank means current unit
      real(kind=8),        intent(out) :: mini    !
      real(kind=8),        intent(out) :: maxi    !
      logical,             intent(out) :: error   ! Logical error flag
    end subroutine pyclass_obsx_minmax
  end interface
  !
  interface
    subroutine pyclass_plotx_minmax(set,unitin,mini,maxi,error)
      use gbl_message
      use class_types
      use plot_formula
      !---------------------------------------------------------------------
      ! @ public
      ! Return the X range of the plot
      ! NB: the subroutine returns actually the SET MODE X range, not
      ! the effective PLOT range (they are the same AFTER the command PLOT).
      ! But the command ANA\DRAW has the same bug-feature, and since the
      ! cursor is used by Weeds to identify lines, we are consistent...
      ! ---
      ! Entry point with a 'class_setup_t' as argument
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)  :: set     !
      character(len=*),    intent(in)  :: unitin  ! Unit for returning values. Blank means current unit
      real(kind=8),        intent(out) :: mini    !
      real(kind=8),        intent(out) :: maxi    !
      logical,             intent(out) :: error   ! Logical error flag
    end subroutine pyclass_plotx_minmax
  end interface
  !
  interface
    subroutine class_dump(line,r,t,error)
      use class_data
      !----------------------------------------------------------------------
      ! @ public (for libclass only)
      ! CLASS Support routine for command DUMP
      ! Types the contents of an obs.
      !----------------------------------------------------------------------
      character(len=*),  intent(in)    :: line   ! Input command line
      type(observation), intent(in)    :: r,t    !
      logical,           intent(inout) :: error  ! Logical error flag
    end subroutine class_dump
  end interface
  !
  interface
    subroutine class_fold(set,line,r,error,user_function)
      use class_types
      !----------------------------------------------------------------------
      ! @ public
      ! Support routine for command:
      !    FOLD
      ! 1  /BOUNDARIES KEEP|DROP
      ! Fold an unfolded frequency switched observation.
      !-----------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set
      character(len=*),    intent(in)    :: line
      type(observation),   intent(inout) :: r
      logical,             intent(inout) :: error
      logical,             external      :: user_function
    end subroutine class_fold
  end interface
  !
  interface
    subroutine classcore_fold_obs(set,obs,keepblank,error)
      use gbl_constant
      use gbl_message
      use class_types
      !----------------------------------------------------------------------
      ! @ public (for libclass only)
      !   Fold an unfolded frequency switched observation.
      !  Each phase is assumed to be given a weight and a frequency change.
      !  The frequency change need not be an integer number of channels.
      !  The number of phases is not limited.
      !-----------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set        !
      type(observation),   intent(inout) :: obs        !
      logical,             intent(in)    :: keepblank  ! Keep blank channels at boundaries?
      logical,             intent(inout) :: error      !
    end subroutine classcore_fold_obs
  end interface
  !
  interface
    subroutine class_resample(set,line,r,error)
      use gbl_constant
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! CLASS Support routine for command
      !   RESAMPLE NX Xref Xval Xinc UNIT [shape] [width] [/FFT] [/NOFFT]
      ! [/LIKE GDFFile]
      ! Resamples a spectrum on a different grid
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      character(len=*),    intent(in)    :: line   ! Command line
      type(observation),   intent(inout) :: r      !
      logical,             intent(inout) :: error  ! Error flag
    end subroutine class_resample
  end interface
  !
  interface
    subroutine copy_obs(in,out,error)
      use gbl_message
      use class_types
      !-------------------------------------------------------------------
      ! @ public
      ! Copy IN into OUT observation
      !-------------------------------------------------------------------
      type(observation), intent(in)    :: in     ! In  observation
      type(observation), intent(inout) :: out    ! Out observation
      logical,           intent(out)   :: error  ! Error flag
    end subroutine copy_obs
  end interface
  !
  interface
    function sas_function(action)
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
      logical :: sas_function                 ! intent(out)
      character(len=*), intent(in) :: action  !
    end function sas_function
  end interface
  !
  interface
    subroutine class_obs_init(obs,error)
      use class_types
      !---------------------------------------------------------------------
      ! @ public (available to external programs)
      !  Initialize the input observation for later use
      !  See demo program classdemo-telwrite.f90
      !---------------------------------------------------------------------
      type(observation), intent(inout) :: obs    !
      logical,           intent(inout) :: error  !
    end subroutine class_obs_init
  end interface
  !
  interface
    subroutine class_obs_reset(obs,ndata,error)
      use class_types
      !---------------------------------------------------------------------
      ! @ public (available to external programs)
      !  Zero-ify and resize the input observation for later use
      !  See demo program classdemo-telwrite.f90
      !---------------------------------------------------------------------
      type(observation), intent(inout) :: obs    !
      integer(kind=4),   intent(in)    :: ndata  ! Number of values the observation can accept in return
      logical,           intent(inout) :: error  ! Logical error flag
    end subroutine class_obs_reset
  end interface
  !
  interface
    subroutine class_obs_clean(obs,error)
      use class_types
      !---------------------------------------------------------------------
      ! @ public (available to external programs)
      !  Clean/free the input observation before deleting it
      !  See demo program classdemo-telwrite.f90
      !---------------------------------------------------------------------
      type(observation), intent(inout) :: obs    !
      logical,           intent(inout) :: error  !
    end subroutine class_obs_clean
  end interface
  !
  interface
    function obs_nchan(head)
      use gbl_constant
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      !  Return the 'nchan' value of the observation, taking care of the
      ! observation kind
      !---------------------------------------------------------------------
      integer(kind=4) :: obs_nchan  ! Function value on return
      type(header), intent(in) :: head   ! Current observation header
    end function obs_nchan
  end interface
  !
  interface
    function obs_bad(head)
      use gbl_constant
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      !  Return the 'bad' value of the observation, taking care of the
      ! observation kind
      !---------------------------------------------------------------------
      real(kind=4) :: obs_bad  !
      type(header), intent(in) :: head  ! Current observation header
    end function obs_bad
  end interface
  !
  interface obs_good
    function obs_good_obs(obs,ichan)
      use class_types
      !-------------------------------------------------------------------
      ! @ public-generic obs_good
      ! Return a "good" (i.e. non blanked) value for the specified channel
      ! ---
      !  Obs version
      !-------------------------------------------------------------------
      real(kind=4) :: obs_good_obs  ! Function value on return
      type(observation), intent(in) :: obs
      integer(kind=4),   intent(in) :: ichan
    end function obs_good_obs
    function obs_good_r4(datay,bad,imin,imax,ichan)
      !-------------------------------------------------------------------
      ! @ public-generic obs_good
      ! Return a "good" (i.e. non blanked) value for the specified channel
      ! ---
      ! R*4 version
      !-------------------------------------------------------------------
      real(kind=4) :: obs_good_r4  ! Function value on return
      real(kind=4),    intent(in) :: datay(:)
      real(kind=4),    intent(in) :: bad
      integer(kind=4), intent(in) :: imin
      integer(kind=4), intent(in) :: imax
      integer(kind=4), intent(in) :: ichan
    end function obs_good_r4
  end interface obs_good
  !
  interface
    subroutine class_save(set,line,error)
      use gildas_def
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! CLASS Support routine for command
      !
      ! SAVE  [Filename]
      !
      ! Save all current default on a CLASS procedure
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      character(len=*),    intent(in)    :: line   !
      logical,             intent(inout) :: error  !
    end subroutine class_save
  end interface
  !
  interface
    subroutine las_setup(set,error)
      use gildas_def
      use class_common
      use class_fits
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !  Called only once at startup
      !---------------------------------------------------------------------
      type(class_setup_t), intent(inout) :: set    !
      logical,             intent(inout) :: error  !
    end subroutine las_setup
  end interface
  !
  interface
    subroutine class_variable(set,line,error,user_function)
      use gbl_message
      use class_parameter
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! Support routine for command
      !   VARIABLE Section1 [... SectionN]
      ! 1   /MODE  READ|WRITE|OFF
      ! 2   /INDEX
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      character(len=*),    intent(in)    :: line           ! Input command line
      logical,             intent(inout) :: error          ! Logical error flag
      logical,             external      :: user_function  !
    end subroutine class_variable
  end interface
  !
  interface
    subroutine las_setvar_R_aliases(set,error)
      use class_parameter
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !  Recreate the aliases for ALL sections which are currently
      ! activated (SET VARIABLE Section ON|READ|WRITE)
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      logical,             intent(inout) :: error  ! Logical error flag
    end subroutine las_setvar_R_aliases
  end interface
  !
  interface
    subroutine class_show_comm(set,line,r,error)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! CLASS  Support routine for command
      !   SHOW ALL  | Arg1 ... ArgN
      ! Line decoding routine
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      character(len=*),    intent(in)    :: line   ! Input command line
      type(observation),   intent(in)    :: r      !
      logical,             intent(inout) :: error  ! Logical error flag
    end subroutine class_show_comm
  end interface
  !
  interface
    subroutine smooth(set,line,r,t,error,user_function)
      use gildas_def
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! CLASS Support routine for command
      ! SMOOTH [AUTO|BOX n|HANNING|GAUSS Width]
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      character(len=*),    intent(in)    :: line           !
      type(observation),   intent(inout) :: r,t            !
      logical,             intent(inout) :: error          !
      logical,             external      :: user_function  !
    end subroutine smooth
  end interface
  !
  interface
    subroutine class_spectrum(set,line,r,error)
      use gbl_message
      use class_data
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! CLASS Support routine for command
      !       SPECTRUM [ArrayName] [Yoffset]
      ! 1              [/INDEX]
      ! 2              [/OBS]
      ! 3              [/PEN Ipen]
      ! Plot the spectrum. Alternatively, plot the INDEX as a 2D image.
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      character(len=*),    intent(in)    :: line   ! Input command line
      type(observation),   intent(inout) :: r      !
      logical,             intent(inout) :: error  ! Logical error flag
    end subroutine class_spectrum
  end interface
  !
  interface
    subroutine stamp(set,line,error,user_function)
      use gildas_def
      use gbl_constant
      use gbl_message
      use class_index
      use class_types
      use class_popup
      use plot_formula
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! CLASS Support routine for command
      !   STAMP [NX NY] [/LABEL [SCAN|NUMBER|SOURCE|LINE]]
      ! Makes a plot of "stamp format" spectra for NX by NY data of the
      ! index.
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      character(len=*),    intent(in)    :: line           ! Input command line
      logical,             intent(inout) :: error          ! Logical error flag
      logical,             external      :: user_function  ! intent(in)
    end subroutine stamp
  end interface
  !
  interface
    subroutine class_strip(set,line,error,user_function)
      use gildas_def
      use gbl_constant
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! CLASS support routine for command
      !   ANA\STRIP FileName
      ! Redirection routine
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      character(len=*),    intent(in)    :: line           !
      logical,             intent(inout) :: error          !
      logical,             external      :: user_function  !
    end subroutine class_strip
  end interface
  !
  interface
    subroutine allocate_classcore(error)
      use gbl_message
      use class_buffer
      use class_data
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! CLASS internal routine
      !---------------------------------------------------------------------
      logical, intent(out) :: error     ! Error flag
    end subroutine allocate_classcore
  end interface
  !
  interface
    subroutine deallocate_classcore(error)
      use gbl_message
      use class_buffer
      use class_common
      use class_data
      use class_index
      use class_popup
      !-------------------------------------------------------------------
      ! @ public (for libclass only)
      ! Deallocate Class buffers
      !-------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine deallocate_classcore
  end interface
  !
  interface
    subroutine reallocate_obs(obs,new_ndata,error)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      !   (Re)allocate obs. buffers
      !   Keep contents when enlarging the buffers
      !   The headers are not modified at all
      !---------------------------------------------------------------------
      type(observation), intent(inout) :: obs        ! Current observation
      integer(kind=4),   intent(in)    :: new_ndata  ! Size of new obs arrays
      logical,           intent(out)   :: error      ! Error flag
    end subroutine reallocate_obs
  end interface
  !
  interface
    subroutine class_subtract_comm(set,line,r,t,error)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! Support routine for command
      !   EXPERIMENTAL\SUBTRACT
      ! Compute T-R
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      character(len=*),    intent(in)    :: line   ! Input command line
      type(observation),   intent(inout) :: r,t    !
      logical,             intent(inout) :: error  ! Logical error flag
    end subroutine class_subtract_comm
  end interface
  !
  interface
    subroutine class_subtract(set,obs1,obs2,diff,error)
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! Perform obs1-obs2 and return the result in the output observation
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      type(observation),   intent(in)    :: obs1   !
      type(observation),   intent(in)    :: obs2   !
      type(observation),   intent(inout) :: diff   !
      logical,             intent(inout) :: error  !
    end subroutine class_subtract
  end interface
  !
  interface
    subroutine class_subtract_cons(set,h1,h2,error)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! Check the consistency of the 2 input observations
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      type(header),        intent(in)    :: h1     ! Header of 1st obs
      type(header),        intent(in)    :: h2     ! Header of 2nd obs
      logical,             intent(inout) :: error  ! Logical error flag
    end subroutine class_subtract_cons
  end interface
  !
  interface
    subroutine class_subtract_data(obs1,obs2,diff,error)
      use gbl_constant
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for mrtcal)
      ! Data difference. Only aligned spectra are implemented!
      !---------------------------------------------------------------------
      type(observation), intent(in)    :: obs1   !
      type(observation), intent(in)    :: obs2   !
      type(observation), intent(inout) :: diff   !
      logical,           intent(inout) :: error  ! Logical error flag
    end subroutine class_subtract_data
  end interface
  !
  interface
    subroutine class_table(set,line,r,error,user_function)
      use gildas_def
      use image_def
      use gbl_constant
      use gbl_message
      use class_types
      use class_data
      use class_index
      use class_averaging
      !----------------------------------------------------------------------
      ! @ public
      ! CLASS Support routine for command
      !   TABLE Filename [OLD|NEW]
      ! 1         [/MATH Expression1 ... ExpressionN]
      ! 2         [/RANGE rmin rmax unit]
      ! 3         [/RESAMPLE nc ref val inc unit [shape] [width]]
      ! 4         [/FFT]
      ! 5         [/FREQUENCY name rest-freq]
      ! 6         [/NOCHECK [SOURCE|POSITION|LINE|SPECTROSCOPY]]
      ! 7         [/SIGMA]  (obsolete)
      ! 8         [/WEIGHT Time|Sigma|Equal]
      ! 9         [/LIKE GDFFile]
      !----------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      character(len=*),    intent(in)    :: line           ! Command line
      type(observation),   intent(inout) :: r              ! For TABLE /MATH expression
      logical,             intent(inout) :: error          ! Logical error flag
      logical,             external      :: user_function  !
    end subroutine class_table
  end interface
  !
  interface
    subroutine my_get_beam(telescope,freq_mhz,found,beam_rad,error)
      use phys_const
      !---------------------------------------------------------------------
      ! @ public
      ! Compute the beam width for given telescope at given frequency
      ! The telescope name can be either:
      !  - a Class telescope name e.g. 30ME0HUI-V01,
      !  - a telescope name given by the user (e.g. through variable MAP%TELES)
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: telescope  ! [      ] Class telescope name
      real(kind=8),     intent(in)    :: freq_mhz   ! [MHz   ] Sky frequency
      logical,          intent(out)   :: found      ! [      ] Found the telescope beam?
      real(kind=4),     intent(out)   :: beam_rad   ! [radian] Beam size
      logical,          intent(inout) :: error      ! [      ] Return status
    end subroutine my_get_beam
  end interface
  !
  interface
    subroutine class_title(set,line,r,error)
      use gbl_message
      use class_data
      use class_types
      !----------------------------------------------------------------------
      ! @ public (for libclass only)
      ! CLASS support routine for command line
      !       TITLE
      ! 1           [/INDEX]
      ! 2           [/OBS]
      ! 3           [/BRIEF]
      ! 4           [/LONG]
      ! 5           [/FULL]
      !  Draw a header above the box
      !----------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      character(len=*),    intent(in)    :: line   ! Input command line
      type(observation),   intent(in)    :: r      !
      logical,             intent(inout) :: error  ! Error flag
    end subroutine class_title
  end interface
  !
  interface
    subroutine class_unblank(line,error)
      use gildas_def
      use image_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !   Support routine for command
      !    EXPERIM\UNBLANK in.tab [out.tab]
      !      /MODE  REJECT|NOISE|INTERPOLATE|VALUE [Value]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      logical,          intent(inout) :: error  !
    end subroutine class_unblank
  end interface
  !
  interface
    subroutine class_user_exists(obs,iuser)
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      !  Search the User subsection matching the current hooks known by
      ! Class. Return index 0 if no such subsection exists.
      !---------------------------------------------------------------------
      type(observation), intent(in)  :: obs
      integer(kind=4),   intent(out) :: iuser
    end subroutine class_user_exists
  end interface
  !
  interface
    subroutine classcore_user_add(obs,sversion,sdata,error)
      use gildas_def
      use gbl_message
      use class_buffer
      use class_types
      use class_user
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !   Add a User Section to the input observation. The caller has no
      ! control on the number of this new user section
      !---------------------------------------------------------------------
      type(observation), intent(inout) :: obs       ! Observation
      integer(kind=4),   intent(in)    :: sversion  ! Version value
      integer(kind=4),   intent(in)    :: sdata     ! The user data (do not use "as is")
      logical,           intent(inout) :: error     ! Logical error flag
    end subroutine classcore_user_add
  end interface
  !
  interface
    subroutine newdat_user(set,obs,error)
      use gildas_def
      use class_buffer
      use class_types
      use class_user
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! Recompute the User Section related data, typically when a new
      ! observation is loaded.
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set
      type(observation),   intent(in)    :: obs
      logical,             intent(inout) :: error
    end subroutine newdat_user
  end interface
  !
  interface
    subroutine classcore_user_update(obs,sversion,sdata,error)
      use gildas_def
      use gbl_message
      use class_buffer
      use class_types
      use class_user
      !---------------------------------------------------------------------
      ! @ public  (for libclass only)
      !  Update the User Section in the input observation. The caller has no
      ! control on the number of this new user section
      !---------------------------------------------------------------------
      type(observation), intent(inout) :: obs       ! Observation
      integer(kind=4),   intent(in)    :: sversion  ! Version value
      integer(kind=4),   intent(in)    :: sdata     ! The user data (do not use "as is")
      logical,           intent(inout) :: error     ! Logical error flag
    end subroutine classcore_user_update
  end interface
  !
  interface
    subroutine classcore_user_owner(sowner,stitle)
      use class_user
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !  Set the userhooks(cuserhooks)%owner character variable
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: sowner  ! Section owner
      character(len=*), intent(in) :: stitle  ! Section title
    end subroutine classcore_user_owner
  end interface
  !
  interface
    subroutine classcore_user_toclass(usertoclass)
      use class_user
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !  Set the userhooks(cuserhooks)%toclass procedure to the input procedure
      !---------------------------------------------------------------------
      external usertoclass
    end subroutine classcore_user_toclass
  end interface
  !
  interface
    subroutine classcore_user_dump(userdump)
      use class_user
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !  Set the userhooks(cuserhooks)%dump procedure to the input procedure
      !---------------------------------------------------------------------
      external userdump
    end subroutine classcore_user_dump
  end interface
  !
  interface
    subroutine classcore_user_setvar(set,usersetvar)
      use class_types
      use class_user
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !  Set the userhooks(cuserhooks)%setvar procedure to the input procedure
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in) :: set
      external                        :: usersetvar
    end subroutine classcore_user_setvar
  end interface
  !
  interface
    subroutine classcore_user_fix(userfix)
      use class_user
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !  Set the userhooks(cuserhooks)%fix procedure to the input procedure
      !---------------------------------------------------------------------
      external userfix
    end subroutine classcore_user_fix
  end interface
  !
  interface
    subroutine classcore_user_find(userfind)
      use class_user
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !  Set the userhooks(cuserhooks)%find procedure to the input procedure
      !---------------------------------------------------------------------
      external userfind
    end subroutine classcore_user_find
  end interface
  !
  interface
    subroutine classcore_user_varidx_fill(uservaridx_fill)
      use class_user
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !  Set the userhooks(cuserhooks)%varidx_fill procedure to the input
      ! procedure
      !---------------------------------------------------------------------
      external uservaridx_fill
    end subroutine classcore_user_varidx_fill
  end interface
  !
  interface
    subroutine classcore_user_varidx_defvar(uservaridx_defvar)
      use class_user
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !  Set the userhooks(cuserhooks)%varidx_defvar procedure to the input
      ! procedure
      !---------------------------------------------------------------------
      external uservaridx_defvar
    end subroutine classcore_user_varidx_defvar
  end interface
  !
  interface
    subroutine classcore_user_varidx_realloc(uservaridx_realloc)
      use class_user
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      !  Set the userhooks(cuserhooks)%varidx_realloc procedure to the input
      ! procedure
      !---------------------------------------------------------------------
      external uservaridx_realloc
    end subroutine classcore_user_varidx_realloc
  end interface
  !
  interface
    subroutine classcore_user_def_inte(set,obs,suffix,ndim,dims,error)
      use gildas_def
      use class_buffer
      use class_types
      use class_user
      !---------------------------------------------------------------------
      ! @ public
      !  Define the Sic integer variable R%USER%OWNER%SUFFIX
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set      !
      type(observation),   intent(in)    :: obs      !
      character(len=*),    intent(in)    :: suffix   ! Component name
      integer(kind=4),     intent(in)    :: ndim     ! Number of dimensions (0=scalar)
      integer(kind=4),     intent(in)    :: dims(4)  ! Dimensions (if needed)
      logical,             intent(inout) :: error    ! Logical error flag
    end subroutine classcore_user_def_inte
  end interface
  !
  interface
    subroutine classcore_user_def_real(set,obs,suffix,ndim,dims,error)
      use gildas_def
      use class_buffer
      use class_types
      use class_user
      !---------------------------------------------------------------------
      ! @ public
      !  Define the Sic real variable R%USER%OWNER%SUFFIX
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set      !
      type(observation),   intent(in)    :: obs      !
      character(len=*),    intent(in)    :: suffix   ! Component name
      integer(kind=4),     intent(in)    :: ndim     ! Number of dimensions (0=scalar)
      integer(kind=4),     intent(in)    :: dims(4)  ! Dimensions (if needed)
      logical,             intent(inout) :: error    ! Logical error flag
    end subroutine classcore_user_def_real
  end interface
  !
  interface
    subroutine classcore_user_def_dble(set,obs,suffix,ndim,dims,error)
      use gildas_def
      use class_buffer
      use class_types
      use class_user
      !---------------------------------------------------------------------
      ! @ public
      !  Define the Sic double variable R%USER%OWNER%SUFFIX
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set      !
      type(observation),   intent(in)    :: obs      !
      character(len=*),    intent(in)    :: suffix   ! Component name
      integer(kind=4),     intent(in)    :: ndim     ! Number of dimensions (0=scalar)
      integer(kind=4),     intent(in)    :: dims(4)  ! Dimensions (if needed)
      logical,             intent(inout) :: error    ! Logical error flag
    end subroutine classcore_user_def_dble
  end interface
  !
  interface
    subroutine classcore_user_def_char(set,obs,suffix,lchain,error)
      use class_buffer
      use class_types
      use class_user
      !---------------------------------------------------------------------
      ! @ public
      !  Define the Sic character variable R%USER%OWNER%SUFFIX
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set      !
      type(observation),   intent(in)    :: obs      !
      character(len=*),    intent(in)    :: suffix   ! Component name
      integer(kind=4),     intent(in)    :: lchain   ! String length in the 'data' buffer
      logical,             intent(inout) :: error    ! Logical error flag
    end subroutine classcore_user_def_char
  end interface
  !
  interface class_user_classtodata
    subroutine classtoi4_0d(i4)
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_classtodata
      ! Write scalar integer*4 at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      integer(kind=4), intent(out) :: i4
    end subroutine classtoi4_0d
    subroutine classtor4_0d(r4)
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_classtodata
      ! Write scalar real*4 at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      real(kind=4), intent(out) :: r4
    end subroutine classtor4_0d
    subroutine classtor8_0d(r8)
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_classtodata
      ! Write scalar real*8 at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      real(kind=8), intent(out) :: r8
    end subroutine classtor8_0d
    subroutine classtocc_0d(cc)
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_classtodata
      ! Write scalar character(len=*) at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      character(len=*), intent(out) :: cc
    end subroutine classtocc_0d
    subroutine classtoi4_1d(i4)
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_classtodata
      ! Write 1D array integer*4 at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      integer(kind=4), intent(out) :: i4(:)
    end subroutine classtoi4_1d
    subroutine classtor4_1d(r4)
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_classtodata
      ! Write 1D array real*4 at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      real(kind=4), intent(out) :: r4(:)
    end subroutine classtor4_1d
    subroutine classtor8_1d(r8)
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_classtodata
      ! Write 1D array real*8 at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      real(kind=8), intent(out) :: r8(:)
    end subroutine classtor8_1d
    subroutine classtocc_1d(cc)
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_classtodata
      ! Write 1D array character(len=*) at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      character(len=*), intent(out) :: cc(:)
    end subroutine classtocc_1d
    subroutine classtoi4_2d(i4)
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_classtodata
      ! Write 2D array integer*4 at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      integer(kind=4), intent(out) :: i4(:,:)
    end subroutine classtoi4_2d
    subroutine classtor4_2d(r4)
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_classtodata
      ! Write 2D array real*4 at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      real(kind=4), intent(out) :: r4(:,:)
    end subroutine classtor4_2d
    subroutine classtor8_2d(r8)
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_classtodata
      ! Write 2D array real*8 at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      real(kind=8), intent(out) :: r8(:,:)
    end subroutine classtor8_2d
    subroutine classtocc_2d(cc)
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_classtodata
      ! Write 2D array character(len=*) at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      character(len=*), intent(out) :: cc(:,:)
    end subroutine classtocc_2d
  end interface class_user_classtodata
  !
  interface class_user_datatoclass
    subroutine i4toclass_0d(i4)
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_datatoclass
      ! Write scalar integer*4 at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: i4
    end subroutine i4toclass_0d
    subroutine r4toclass_0d(r4)
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_datatoclass
      ! Write scalar real*4 at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      real(kind=4), intent(in) :: r4
    end subroutine r4toclass_0d
    subroutine r8toclass_0d(r8)
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_datatoclass
      ! Write scalar real*8 at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      real(kind=8), intent(in) :: r8
    end subroutine r8toclass_0d
    subroutine cctoclass_0d(cc)
      use gbl_message
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_datatoclass
      ! Write scalar character(len=*) at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: cc
    end subroutine cctoclass_0d
    subroutine i4toclass_1d(i4)
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_datatoclass
      ! Write 1D array integer*4 at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: i4(:)
    end subroutine i4toclass_1d
    subroutine r4toclass_1d(r4)
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_datatoclass
      ! Write 1D array real*4 at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      real(kind=4), intent(in) :: r4(:)
    end subroutine r4toclass_1d
    subroutine r8toclass_1d(r8)
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_datatoclass
      ! Write 1D array real*8 at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      real(kind=8), intent(in) :: r8(:)
    end subroutine r8toclass_1d
    subroutine cctoclass_1d(cc)
      use gbl_message
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_datatoclass
      ! Write 1D array character(len=*) at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: cc(:)
    end subroutine cctoclass_1d
    subroutine i4toclass_2d(i4)
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_datatoclass
      ! Write 2D array integer*4 at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: i4(:,:)
    end subroutine i4toclass_2d
    subroutine r4toclass_2d(r4)
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_datatoclass
      ! Write 2D array real*4 at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      real(kind=4), intent(in) :: r4(:,:)
    end subroutine r4toclass_2d
    subroutine r8toclass_2d(r8)
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_datatoclass
      ! Write 2D array real*8 at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      real(kind=8), intent(in) :: r8(:,:)
    end subroutine r8toclass_2d
    subroutine cctoclass_2d(cc)
      use gbl_message
      use class_buffer
      use class_common
      !---------------------------------------------------------------------
      ! @ public-generic class_user_datatoclass
      ! Write 2D array character(len=*) at next position in buffer 'uwork'
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: cc(:,:)
    end subroutine cctoclass_2d
  end interface class_user_datatoclass
  !
  interface class_user_varidx_def_charn
    subroutine class_user_varidx_def_charn_1d(suffix,array,error)
      use gildas_def
      use class_index
      use class_user
      !---------------------------------------------------------------------
      ! @ public-generic class_user_varidx_def_charn
      !  Define the Sic character array IDX%USER%OWNER%TITLE%SUFFIX[:]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: suffix    ! Component name
      character(len=*), intent(in)    :: array(:)  ! Target 1D array
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine class_user_varidx_def_charn_1d
    subroutine class_user_varidx_def_charn_2d(suffix,array,error)
      use gildas_def
      use class_index
      use class_user
      !---------------------------------------------------------------------
      ! @ public-generic class_user_varidx_def_charn
      !  Define the Sic character array IDX%USER%OWNER%TITLE%SUFFIX[:,:]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: suffix      ! Component name
      character(len=*), intent(in)    :: array(:,:)  ! Target 2D array
      logical,          intent(inout) :: error       ! Logical error flag
    end subroutine class_user_varidx_def_charn_2d
  end interface class_user_varidx_def_charn
  !
  interface class_user_varidx_def_dble
    subroutine class_user_varidx_def_dble_1d(suffix,array,error)
      use gildas_def
      use class_index
      use class_user
      !---------------------------------------------------------------------
      ! @ public-generic class_user_varidx_def_dble
      !  Define the Sic double array IDX%USER%OWNER%TITLE%SUFFIX[:]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: suffix    ! Component name
      real(kind=8),     intent(in)    :: array(:)  ! Target 1D array
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine class_user_varidx_def_dble_1d
    subroutine class_user_varidx_def_dble_2d(suffix,array,error)
      use gildas_def
      use class_index
      use class_user
      !---------------------------------------------------------------------
      ! @ public-generic class_user_varidx_def_dble
      !  Define the Sic double array IDX%USER%OWNER%TITLE%SUFFIX[:,:]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: suffix      ! Component name
      real(kind=8),     intent(in)    :: array(:,:)  ! Target 2D array
      logical,          intent(inout) :: error       ! Logical error flag
    end subroutine class_user_varidx_def_dble_2d
  end interface class_user_varidx_def_dble
  !
  interface class_user_varidx_def_inte
    subroutine class_user_varidx_def_inte_1d(suffix,array,error)
      use gildas_def
      use class_index
      use class_user
      !---------------------------------------------------------------------
      ! @ public-generic class_user_varidx_def_inte
      !  Define the Sic integer array IDX%USER%OWNER%TITLE%SUFFIX[:]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: suffix    ! Component name
      integer(kind=4),  intent(in)    :: array(:)  ! Target 1D array
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine class_user_varidx_def_inte_1d
    subroutine class_user_varidx_def_inte_2d(suffix,array,error)
      use gildas_def
      use class_index
      use class_user
      !---------------------------------------------------------------------
      ! @ public-generic class_user_varidx_def_inte
      !  Define the Sic integer array IDX%USER%OWNER%TITLE%SUFFIX[:,:]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: suffix      ! Component name
      integer(kind=4),  intent(in)    :: array(:,:)  ! Target 2D array
      logical,          intent(inout) :: error       ! Logical error flag
    end subroutine class_user_varidx_def_inte_2d
  end interface class_user_varidx_def_inte
  !
  interface class_user_varidx_def_real
    subroutine class_user_varidx_def_real_1d(suffix,array,error)
      use gildas_def
      use class_index
      use class_user
      !---------------------------------------------------------------------
      ! @ public-generic class_user_varidx_def_real
      !  Define the Sic real array IDX%USER%OWNER%TITLE%SUFFIX[:]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: suffix    ! Component name
      real(kind=4),     intent(in)    :: array(:)  ! Target 1D array
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine class_user_varidx_def_real_1d
    subroutine class_user_varidx_def_real_2d(suffix,array,error)
      use gildas_def
      use class_index
      use class_user
      !---------------------------------------------------------------------
      ! @ public-generic class_user_varidx_def_real
      !  Define the Sic real array IDX%USER%OWNER%TITLE%SUFFIX[:,:]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: suffix      ! Component name
      real(kind=4),     intent(in)    :: array(:,:)  ! Target 2D array
      logical,          intent(inout) :: error       ! Logical error flag
    end subroutine class_user_varidx_def_real_2d
  end interface class_user_varidx_def_real
  !
  interface
    subroutine class_uvt(set,line,r,error,user_function)
      use gildas_def
      use image_def
      use gbl_constant
      use gbl_message
      use class_types
      !----------------------------------------------------------------------
      ! @ public (for libclass only)
      ! CLASS Support routine for command
      !  UV_ZERO Name [/VERBOSE]
      ! Create a Zero Spacing UV Table from the current R spectrum
      !----------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      character(len=*),    intent(in)    :: line           ! Input command line
      type(observation),   intent(in)    :: r              !
      logical,             intent(inout) :: error          ! Logical flag
      logical,             external      :: user_function  !
    end subroutine class_uvt
  end interface
  !
  interface
    subroutine las_variables(set,r,error)
      use gildas_def
      use gbl_message
      use class_common
      use class_index
      use class_types
      !----------------------------------------------------------------------
      ! @ public (for libclass only)
      !  Initialisation routine
      !  Define all SIC variables
      !----------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set
      type(observation),   intent(in)    :: r
      logical,             intent(inout) :: error
    end subroutine las_variables
  end interface
  !
  interface
    subroutine las_variables_r(set,r,error)
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! Define and populate the R%HEAD structure
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      type(observation),   intent(in)    :: r      !
      logical,             intent(inout) :: error  ! Logical error flag
    end subroutine las_variables_r
  end interface
  !
  interface
    subroutine class_wavelet(line,r,error,user_function)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! Support routine for command
      !  WAVELET [/BASE]
      !---------------------------------------------------------------------
      character(len=*),  intent(in)    :: line
      type(observation), intent(inout) :: r
      logical,           intent(inout) :: error
      logical,           external      :: user_function
    end subroutine class_wavelet
  end interface
  !
  interface
    subroutine class_write_comm(set,line,r,error,user_function)
      use gbl_message
      use class_common
      use class_types
      use class_index
      use class_parameter
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! CLASS Support routine for command
      !     WRITE [Obs_Number]
      ! Write the current R observation to the output file
      ! If this observation is a old OTF data, splits it into separate
      ! spectra, one per dump.
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      character(len=*),    intent(in)    :: line           ! Command Line
      type(observation),   intent(inout) :: r              !
      logical,             intent(inout) :: error          ! Return error code
      logical,             external      :: user_function  ! handling of user defined sections intent(in)
    end subroutine class_write_comm
  end interface
  !
  interface
    subroutine class_write(set,obs,error,user_function)
      use gbl_message
      use class_common
      use class_types
      use plot_formula
      !---------------------------------------------------------------------
      ! @ public
      !  Writes the input observation onto the output file. "write" means:
      ! open the observation in the output file, transfer, close in the
      ! output file. Output file is not flushed, it is up to the calling
      ! routine to flush it or not for efficiency purpose.
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      type(observation),   intent(inout) :: obs            !
      logical,             intent(inout) :: error          ! Return error code
      logical,             external      :: user_function  ! Handling of user defined sections
    end subroutine class_write
  end interface
  !
  interface
    subroutine class_update_comm(set,r,error,user_function)
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ public (for libclass only)
      ! CLASS Support routine for command
      !  UPDATE
      ! Update the observation in R in the output file
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set            !
      type(observation),   intent(inout) :: r              !
      logical,             intent(inout) :: error          ! Return error code
      logical,             external      :: user_function  ! Handling of user defined sections
    end subroutine class_update_comm
  end interface
  !
  interface
    subroutine class_write_flush(set,obs,error)
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! CLASS External routine (available to other programs)
      ! Close an observation and reopen the output file
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      type(observation),   intent(inout) :: obs    !
      logical,             intent(inout) :: error  ! Error status
    end subroutine class_write_flush
  end interface
  !
  interface
    subroutine class_write_close(set,obs,error)
      use class_types
      !---------------------------------------------------------------------
      ! @ public
      ! Close an observation, but not the file
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set    !
      type(observation),   intent(inout) :: obs    !
      logical,             intent(inout) :: error  ! Error status
    end subroutine class_write_close
  end interface
  !
end module classcore_interfaces_public
