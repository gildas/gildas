subroutine class_fold(set,line,r,error,user_function)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_fold
  use class_types
  !----------------------------------------------------------------------
  ! @ public
  ! Support routine for command:
  !    FOLD
  ! 1  /BOUNDARIES KEEP|DROP
  ! Fold an unfolded frequency switched observation.
  !-----------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  character(len=*),    intent(in)    :: line
  type(observation),   intent(inout) :: r
  logical,             intent(inout) :: error
  logical,             external      :: user_function
  ! Local
  character(len=*), parameter :: rname='FOLD'
  integer(kind=4), parameter :: optbound=1
  character(len=4), parameter :: boundaries(2) = (/ 'KEEP','DROP' /)
  character(len=12) :: arg,key
  integer(kind=4) :: nc,ikey
  !
  arg = 'KEEP'
  call sic_ke(line,optbound,1,arg,nc,.false.,error)
  if (error)  return
  call sic_ambigs(rname,arg,key,ikey,boundaries,2,error)
  if (error)  return
  !
  call classcore_fold_obs(set,r,key.eq.'KEEP',error)
  if (error)  return
  !
  call newdat(set,r,error)
  call newdat_assoc(set,r,error)
  !
end subroutine class_fold
!
subroutine classcore_fold_obs(set,obs,keepblank,error)
  use gbl_constant
  use gbl_message
  use classcore_interfaces, except_this=>classcore_fold_obs
  use class_types
  !----------------------------------------------------------------------
  ! @ public (for libclass only)
  !   Fold an unfolded frequency switched observation.
  !  Each phase is assumed to be given a weight and a frequency change.
  !  The frequency change need not be an integer number of channels.
  !  The number of phases is not limited.
  !-----------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set        !
  type(observation),   intent(inout) :: obs        !
  logical,             intent(in)    :: keepblank  ! Keep blank channels at boundaries?
  logical,             intent(inout) :: error      !
  ! Local
  character(len=*), parameter :: rname='FOLD'
  real(kind=4), pointer :: data1(:),data2(:)
  real(kind=4), allocatable :: ffold(:),weight(:)
  integer(kind=4) :: onchan,omin,omax,shift,ier
  real(kind=4) :: throw(obs%head%swi%nphas)
  logical :: withyassoc
  !
  if (obs%head%swi%nphas.le.1 .or. .not.obs%head%presec(class_sec_swi_id)) then
     call class_message(seve%e,rname,'Cannot fold a single phase spectrum')
     error = .true.
     return
  elseif (obs%head%swi%swmod.eq.mod_fold) then
     call class_message(seve%e,rname,'Spectrum has already been folded')
     error = .true.
     return
  elseif (obs%head%swi%swmod.ne.mod_freq) then
     call class_message(seve%e,rname,'Can only fold a frequency switched spectrum')
     error = .true.
     return
  endif
  !
  withyassoc = class_assoc_exists(obs,'Y',data2)
  if (withyassoc) then
    data1 => obs%spectre
  ! data2 => points to Y array data
  else
    data1 => obs%spectre
    data2 => obs%spectre
  endif
  ! Work arrays: assume folding is performed at least with 1 channel overlap,
  ! so we need at least 1 channel in intersect mode, and at most 2 times
  ! channels +1 in composite mode. If phases do not overlap (which is not supposed
  ! to happen), you should enlarge this value (probably by computing automatically
  ! the number of channels in the folded spectrum).
  onchan = 2*obs%head%spe%nchan+1
  allocate (ffold(onchan),weight(onchan),stat=ier)
  throw(1:obs%head%swi%nphas) = obs%head%swi%decal(1:obs%head%swi%nphas)/obs%head%spe%fres
  !
  call classcore_fold_obs_sub(set,data1,data2,obs%head%spe%nchan,obs%cbad,        &
                              obs%head%swi%nphas,throw(:),obs%head%swi%poids(:),  &
                              keepblank,                                          &
                              ffold,weight,onchan,                                &
                              omin,omax,shift,                                    &
                              error)
  if (error)  return
  !
  ! Update the data and header
  obs%head%spe%rchan = obs%head%spe%rchan-omin+1.d0-shift
  obs%head%swi%swmod = mod_fold
  obs%head%spe%nchan = omax-omin+1
  obs%cnchan = obs%head%spe%nchan
  call reallocate_obs(obs,obs%cnchan,error)
  if (error)  return
  obs%spectre(1:obs%cnchan) = ffold(omin:omax)
  !
  deallocate(ffold,weight)
  !
  ! Associated Arrays
  if (withyassoc) then
    call class_assoc_delete(obs,'Y',error)
    if (error)  return
  endif
  !
  if (obs%head%presec(class_sec_assoc_id)) then
    ! Also fold Associated Arrays
    call fold_assoc(set,obs%assoc,obs%head%swi%nphas,throw,  &
      obs%head%swi%poids,omin,omax,error)
    if (error) then
      call class_message(seve%w,rname,  &
        'Section Associated Array could not be folded. Removed.')
      obs%head%presec(class_sec_assoc_id) = .false.
      call rzero_assoc(obs)
      error = .false.  ! Not fatal
    endif
  endif
  !
end subroutine classcore_fold_obs
!
subroutine classcore_fold_obs_sub(set,data1,data2,inchan,bad,nphase,throw,  &
  weight,keepblank,odata1,odataw,onchan,omin,omax,shift,error)
  use gbl_message
  use classcore_interfaces, except_this=>classcore_fold_obs_sub
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Two main use cases:
  ! 1) 1 data spectrum to be folded for N phases,
  ! 2) N data spectra  to be folded for N phases
  ! Ideally we should receive a 'data(Nchan,Nspec)' array, and compare
  ! Nspec to Nphase. However this is not programmer friendly as long
  ! as we can not avoid duplicating the data in the case 1, i.e. we can
  ! not write before entering here:
  !   data(:,1) => obs%spectre
  ! This is pointer reshape and this is not supported by gfortran 4.4
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)         :: set             !
  integer(kind=4),     intent(in)         :: inchan          ! Input number of channels
  real(kind=4),        intent(in), target :: data1(inchan)   ! Input data (phase1)
  real(kind=4),        intent(in), target :: data2(inchan)   ! Input data (phase2)
  real(kind=4),        intent(in)         :: bad             ! Blanking value
  integer(kind=4),     intent(in)         :: nphase          ! Number of phases
  real(kind=4),        intent(in)         :: throw(nphase)   ! [chan] Can be non-int, can be negative
  real(kind=4),        intent(in)         :: weight(nphase)  ! Phase weight
  logical,             intent(in)         :: keepblank       ! Keep blank channels at boundaries?
  integer(kind=4),     intent(in)         :: onchan          ! Size of output arrays
  real(kind=4),        intent(out)        :: odata1(onchan)  ! Output data
  real(kind=4),        intent(out)        :: odataw(onchan)  ! Output weight
  integer(kind=4),     intent(out)        :: omin,omax       ! Useful part of odata1(:) in return
  integer(kind=4),     intent(out)        :: shift           ! By how many channels the idata and odata axes are shifted?
  logical,             intent(inout)      :: error           ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='FOLD'
  integer(kind=4) :: i,j,iphas,nd,ifirst,ilast
  real(kind=4) :: poids,decal,od,pd
  logical :: contaminate
  real(kind=4), pointer :: idata(:)
  integer(kind=1) :: onphas(onchan)  ! Automatic array. Kind=1 is enough
  character(len=message_length) :: mess
  !
  if (nphase.gt.2) then
    call class_message(seve%e,rname,'More than 2 phases not implemented')
    error = .true.
    return
  endif
  !
  contaminate = set%bad.eq.'O'
  odata1(:) = 0.0
  odataw(:) = 0.0
  onphas(:) = 0
  !
  ! The output spectrum will be filled to the left and to the right
  ! This means the data1(:) and odata(:) axes are shifted by some
  ! channel amount
  shift = floor(minval(throw))
  !
  if (inchan.eq.1) then
    ! Continuum data. A bit more relaxed regarding boundary conditions.
    ! We will explore pairs of input channel [0,1] and [1,2], and save
    ! the results in the corresponding output channels (see how i and j
    ! are computed below). Obviously there is no channel 0 or 2, but the
    ! code is protected against. This means that the output boundary
    ! channel may not have a full contribution from both phases. We want
    ! to avoid this in the standard case... but with continuum the unique
    ! channel is at boundary!
    ifirst = 1
    ilast = inchan+1
  else
    ! Spectrum data. Keep conservative regarding boundary conditions.
    ifirst = 2
    ilast = inchan
  endif
  !
  ! Loop over phases
  do iphas=1,nphase
     if (iphas.eq.1) then
       idata => data1
     else
       idata => data2
     endif
     decal = throw(iphas)
     !
     ! Non integer shift: interpolate
     if (decal.ge.0.0) then
        nd = int(decal)    ! 0 <= nd <= decal
     else
        nd = int(decal)-1  !      nd <= decal <= 0
     endif
     pd = decal - nd  !  0 <= pd < 1
     od = 1. - pd     !  0 < od <= 1
     poids = weight(iphas)
     !
     do i = ifirst,ilast
      j = i+nd-shift
      !         j = channel position in output array for this phase
      ! i and i-1 = channel positions in input array for output channel j
      ! Note that this means we may use input channels 0 to inchan+1. Those
      ! boundaries are obviously ignored, but this helps filling the
      ! corresponding output channel j (e.g. the pair (inchan,inchan+1)
      ! has a contribution to some output channel we do not want to ignore).
      ! This is particularly useful for continuum where inchan=1
      !
      ! The caller may have given odata1(:) and odataw(:) too small. Avoid
      ! overflow (this truncates the resulting spectrum).
      if (j.lt.1 .or. j.gt.onchan)  cycle
      !
      onphas(j) = onphas(j)+1  ! Channel j gets contribution from this phase,
                               ! from its channel 'i' or 'i-1' at least
      !
      ! Channel i
      if (i.ne.inchan+1) then  ! Boundary condition
        if (idata(i).ne.bad) then
          if (odata1(j).ne.bad) then
            odata1(j) = odata1(j) + poids*od*idata(i)
          elseif (.not.contaminate) then
            odata1(j) = poids*od*idata(i)
          else
            odata1(j) = bad
          endif
        elseif (contaminate) then
          odata1(j) = bad
        endif
        odataw(j) = odataw(j) + abs(poids)*od
      endif
      !
      ! Channel i-1
      if (i.ne.1) then  ! Boundary condition
        if (idata(i-1).ne.bad) then
          if (odata1(j).ne.bad) then
            odata1(j) = odata1(j) + poids*pd*idata(i-1)
          elseif (.not.contaminate) then
            odata1(j) = poids*pd*idata(i-1)
          else
            odata1(j) = bad
          endif
        elseif (contaminate) then
          odata1(j) = bad
        endif
        odataw(j) = odataw(j) + abs(poids)*pd
      endif
    enddo
    !
  enddo
  !
  ! ODATAW(I) = 0.      0 phase contributing => Reject
  ! ODATAW(I) # NPHASE  some phases contributing => Reject if INTERSECT
  ! ODATAW(I) = NPHASE  all phases contributing => Keep
  !
  if (set%alig2.ne.'I') then  ! COMPOSITE
     do i = 1,onchan
       if (onphas(i).eq.0)                        cycle
       if (odata1(i).eq.bad .and. .not.keepblank) cycle
       exit
     enddo
     omin = i
     do i = onchan,1,-1
       if (onphas(i).eq.0)                        cycle
       if (odata1(i).eq.bad .and. .not.keepblank) cycle
       exit
     enddo
     omax = i
  else                        ! INTERSECT
     do i = 1,onchan
       if (onphas(i).ne.nphase)                   cycle
       if (odata1(i).eq.bad .and. .not.keepblank) cycle
       exit
     enddo
     omin = i
     do i = onchan,1,-1
       if (onphas(i).ne.nphase)                   cycle
       if (odata1(i).eq.bad .and. .not.keepblank) cycle
       exit
     enddo
     omax = i
  endif
  if (omin.gt.omax) then
    write(mess,'(A,I0,A)')  &
      'FSW phases do not intersect (Nchan is ',inchan,' while throws are '
    do iphas=1,nphase
      write(mess,'(A,1X,F0.2)') trim(mess),throw(iphas)
    enddo
    write(mess,'(2A)')  trim(mess),' channels)'
    call class_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  ! Normalisation
  where (odataw.eq.0.)
    odata1 = bad
  elsewhere (odata1.ne.bad)
    odata1 = odata1/odataw
  end where
  !
end subroutine classcore_fold_obs_sub
