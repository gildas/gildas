!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine class_plot(set,line,r,error)
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_plot
  use class_data
  use class_types
  !----------------------------------------------------------------------
  ! @ public
  ! CLASS Support routine for command
  !      PLOT [ArrayName]
  ! 1         [/INDEX]
  ! 2         [/OBS]
  ! Plot specified observation for command
  !----------------------------------------------------------------------
  type(class_setup_t), intent(inout) :: set    !
  character(len=*),    intent(in)    :: line   ! Command line
  type(observation),   intent(inout) :: r      !
  logical,             intent(inout) :: error  ! Error status
  ! Local
  character(len=*), parameter :: rname='PLOT'
  logical :: doindex
  character(len=16) :: aline,comm,aaname
  integer(kind=4) :: nline,nc
  !
  ! Parse input line
  doindex = set%action.eq.'I'
  if (sic_present(1,0).and.sic_present(2,0)) then
     call class_message(seve%e,rname,'/INDEX and /OBS are not compatible')
     error = .true.
     return
  else if (sic_present(1,0)) then
     if (.not.associated(p%data2)) then
        call class_message(seve%e,rname,'No index loaded')
        error = .true.
        return
     endif
     doindex = .true.
  else if (sic_present(2,0)) then
     doindex = .false.
  endif
  !
  if (doindex) then
     call plot_index(set,error)
  else
     if (r%head%xnum.eq.0) then
        call class_message(seve%e,rname,'No such spectrum in memory.')
        error = .true.
        return
     endif
     if (r%head%gen%kind.eq.kind_sky) then
        call class_message(seve%e,rname,'Not yet implemented for SKYDIP.')
        error = .true.
        !!call plot_sky(line,error)
     elseif (r%head%gen%kind.eq.kind_onoff) then
        call class_message(seve%e,rname,'Not yet implemented for ON/OFF.')
        error = .true.
     else
        call gtv_clear_directory('^',error)  ! ^ = ancestor of current tree
        if (error)  return
        call newlim(set,r,error)
        ! Spectrum
        aaname = 'Y'  ! Plot RY
        call sic_ke(line,0,1,aaname,nc,.false.,error)
        if (error)  return
        call spectr1d(rname,set,r,error,aaname=aaname)
        if (error)  return
        ! Box
        call class_box_default(set,.false.,r,aaname,error)
        if (error)  return
        ! Title
        aline = 'LAS'//CHAR(92)//'TITLE /OBS'
        nline = len_trim(aline)
        call sic_analyse(comm,aline,nline,error)
        call class_title(set,aline,r,error)
     endif
  endif
  !
  ! Set space to direct
  set%fft = .false.
  !
end subroutine class_plot
!
subroutine plot_index(set,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>plot_index
  use class_data
  use class_index
  use class_popup
  use class_types
  use plot_formula
  !-----------------------------------------------------------------------
  ! @ private
  ! CLASS Support routine for command
  !     PLOT /INDEX
  ! Plot the currently loaded 2-D array as an image
  !-----------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='PLOT'
  character(len=16) :: aline,comm
  character(len=1) :: unit_up,unit_low
  integer(kind=entry_length) :: i
  integer(kind=4) :: ier,nline
  !
  if (.not.associated(p%data2)) then
     call class_message(seve%e,rname,'No 2-D data loaded')
     error = .true.
     return
  endif
  !
  ! Compute plot limits
  call newlim(set,p,error)
  if (error) return
  !
  ! Clear plot
  call geunit(set,p%head,unit_low, unit_up)
  call gtclear
  !
  ! Spectrum
  call spectr2d(p,error)
  !
  ! Box and wedge
  call class_box_default(set,.true.,p,'Y',error)
  if (error)  return
  call gr_exec2('WEDGE')
  !
  ! Title
  aline = 'LAS'//CHAR(92)//'TITLE /INDEX'
  nline = len_trim(aline)
  call sic_analyse(comm,aline,nline,error)
  call class_title(set,aline,p,error)
  !
  ! Save for POPUP commands.
  npop = p%head%des%ndump
  if (allocated(ipop)) deallocate(ipop,xpop,ypop)
  allocate(ipop(npop),xpop(npop),ypop(npop),stat=ier)
  if (ier.ne.0) then
     call class_message(seve%f,rname,'Allocation error')
     error = .true.
     npop = 0
  else
     do i=1, npop
        ipop(i) = cx%num(i) !! r%head%gen%num
     enddo
  endif
  cpop = cpop_plotindex
  pgx1 = gx1
  pgx2 = gx2
  pgz1 = gy1
  pgz2 = gy2
  pux1 = 0.5
  pux2 = p%head%spe%nchan+0.5
  if (set%modez.eq.'A'.or.set%modez.eq.'T') then
     puz1 = 0.5
     puz2 = p%head%des%ndump+0.5
  endif
  !
end subroutine plot_index
