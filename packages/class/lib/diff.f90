subroutine class_diff(set,line,r,t,error,user_function)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_diff
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  !  Support routine for command
  !    EXPERIM\DIFF [Num1 Num2]
  !  Print the differences between 2 observations
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  character(len=*),    intent(in)    :: line
  type(observation),   intent(in)    :: r,t
  logical,             intent(inout) :: error
  logical,             external      :: user_function
  ! Local
  character(len=*), parameter :: rname='DIFF'
  integer(kind=obsnum_length) :: num
  integer(kind=entry_length) :: kx
  type(observation) :: obs(2)
  integer(kind=4) :: iarg
  logical :: found
  character(len=message_length) :: mess
  !
  if (sic_narg(0).ne.0 .and. sic_narg(0).ne.2) then
    call class_message(seve%e,rname,'Command expects 0 or 2 arguments')
    error = .true.
    return
  endif
  !
  if (sic_narg(0).eq.0) then
    ! 0 argument: use R and T
    if (r%head%xnum.eq.0) then
      call class_message(seve%e,rname,'No R spectrum in memory')
      error = .true.
      return
    endif
    if (t%head%xnum.eq.0) then
      call class_message(seve%e,rname,'No T spectrum in memory')
      error = .true.
      return
    endif
    call class_diff_head(r%head,t%head,error)
    if (error)  return
    !
  else
    ! 2 arguments: observation numbers from current input file
    call init_obs(obs(1))
    call init_obs(obs(2))
    !
    do iarg=1,2
      call sic_i0(line,0,iarg,num,.true.,error)
      if (error)  return
      !
      call get_num_ix(set,num,0,found,kx,error)
      if (error)  return
      if (.not.found) then
        write(mess,'(A,I0,A)')  'Observation #',num,' not found'
        call class_message(seve%e,rname,mess)
        error = .true.
        return
      endif
      call rheader(set,obs(iarg),kx,user_function,error)
      if (error)  return
    enddo
    !
    call class_diff_head(obs(1)%head,obs(2)%head,error)
    if (error)  continue
    !
    call free_obs(obs(1))
    call free_obs(obs(2))
  endif
  !
end subroutine class_diff
!
subroutine class_diff_head(a,b,error)
  use class_types
  use classcore_interfaces, except_this=>class_diff_head
  !---------------------------------------------------------------------
  ! @ private
  !  Print the differences between 2 observations
  !---------------------------------------------------------------------
  type(header), intent(in)    :: a,b
  logical,      intent(inout) :: error
  !
  call class_diff_gen(a,b,error)
  if (error)  return
  call class_diff_pos(a,b,error)
  if (error)  return
  call class_diff_spe(a,b,error)
  if (error)  return
  call class_diff_cal(a,b,error)
  if (error)  return
  call class_diff_swi(a,b,error)
  if (error)  return
  !
end subroutine class_diff_head
!
subroutine class_diff_gen(a,b,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_diff_gen
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Print the differences between 2 observations (section GENERAL)
  !---------------------------------------------------------------------
  type(header), intent(in)    :: a,b
  logical,      intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DIFF'
  character(len=*), parameter :: secname='General'
  logical :: secwarned
  !
  if (class_diff_presec(class_sec_gen_id,secname,a,b)) then
     error =.true.
     return
  endif
  !
  secwarned = .false.
  ! No diff for NUM
  ! No diff for VERSION
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%GEN%TELES:',  a%gen%teles,b%gen%teles)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%GEN%CDOBS:',  a%gen%cdobs,b%gen%cdobs)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%GEN%CDRED:',  a%gen%cdred,b%gen%cdred)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%GEN%KIND:',   a%gen%kind,b%gen%kind)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%GEN%QUAL:',   a%gen%qual,b%gen%qual)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%GEN%SCAN:',   a%gen%scan,b%gen%scan)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%GEN%SUBSCAN:',a%gen%subscan,b%gen%subscan)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%GEN%UT:',     a%gen%ut,b%gen%ut)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%GEN%ST;',     a%gen%st,b%gen%st)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%GEN%AZ:',     a%gen%az,b%gen%az)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%GEN%EL:',     a%gen%el,b%gen%el)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%GEN%TAU:',    a%gen%tau,b%gen%tau)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%GEN%TSYS:',   a%gen%tsys,b%gen%tsys)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%GEN%TIME:',   a%gen%time,b%gen%time)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%GEN%PARANG:', a%gen%parang,b%gen%parang)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%GEN%YUNIT:',  a%gen%yunit,b%gen%yunit)
  if (secwarned) error=.true.
  !
end subroutine class_diff_gen
!
subroutine class_diff_pos(a,b,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_diff_pos
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Print the differences between 2 observations (section POSITION)
  !---------------------------------------------------------------------
  type(header), intent(in)    :: a,b
  logical,      intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DIFF'
  character(len=*), parameter :: secname='Position'
  logical :: secwarned
  !
  if (class_diff_presec(class_sec_pos_id,secname,a,b)) then
     error =.true.
     return
  endif
  !
  secwarned = .false.
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%POS%SOURC:',  a%pos%sourc,b%pos%sourc)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%POS%SYSTEM:', obs_system(a%pos%system),obs_system(b%pos%system))
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%POS%EQUINOX:',a%pos%equinox,b%pos%equinox)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%POS%PROJ:',   projnam(a%pos%proj),projnam(b%pos%proj))
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%POS%LAM:',    a%pos%lam,b%pos%lam)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%POS%BET:',    a%pos%bet,b%pos%bet)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%POS%PROJANG:',a%pos%projang,b%pos%projang)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%POS%LAMOF:',  a%pos%lamof,b%pos%lamof)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%POS%BETOF:',  a%pos%betof,b%pos%betof)
  if (secwarned) error=.true.
  !
end subroutine class_diff_pos
!
subroutine class_diff_spe(a,b,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_diff_spe
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Print the differences between 2 observations (section SPECTROSCOPY)
  !---------------------------------------------------------------------
  type(header), intent(in)    :: a,b
  logical,      intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DIFF'
  character(len=*), parameter :: secname='Spectroscopic'
  logical :: secwarned
  !
  if (class_diff_presec(class_sec_spe_id,secname,a,b)) then
     error =.true.
     return
  endif
  !
  secwarned = .false.
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%SPE%LINE:',   a%spe%line,b%spe%line)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%SPE%NCHAN:',  a%spe%nchan,b%spe%nchan)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%SPE%RESTF:',  a%spe%restf,b%spe%restf)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%SPE%IMAGE:',  a%spe%image,b%spe%image)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%SPE%DOPPLER:',a%spe%doppler,b%spe%doppler)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%SPE%RCHAN:',  a%spe%rchan,b%spe%rchan)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%SPE%FRES:',   a%spe%fres,b%spe%fres)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%SPE%VRES:',   a%spe%vres,b%spe%vres)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%SPEVOFF%:',   a%spe%voff,b%spe%voff)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%SPE%BAD:',    a%spe%bad,b%spe%bad)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%SPE%VTYPE:',  obs_typev(a%spe%vtype),obs_typev(b%spe%vtype))
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%SPE%VCONV:',  a%spe%vconv,b%spe%vconv)
  if (secwarned) error=.true.
  !
end subroutine class_diff_spe
!
subroutine class_diff_cal(a,b,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_diff_cal
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Print the differences between 2 observations (section CALIBRATION)
  !---------------------------------------------------------------------
  type(header), intent(in)    :: a,b
  logical,      intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DIFF'
  character(len=*), parameter :: secname='Calibration'
  logical :: secwarned
  !
  if (class_diff_presec(class_sec_cal_id,secname,a,b))  then
     error =.true.
     return
  endif
  !
  secwarned = .false.
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%BEEFF:',   a%cal%beeff,b%cal%beeff)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%FOEFF:',   a%cal%foeff,b%cal%foeff)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%GAINI:',   a%cal%gaini,b%cal%gaini)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%H2OMM:',   a%cal%h2omm,b%cal%h2omm)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%PAMB:',    a%cal%pamb,b%cal%pamb)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%TAMB:',    a%cal%tamb,b%cal%tamb)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%TATMS:',   a%cal%tatms,b%cal%tatms)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%TCHOP:',   a%cal%tchop,b%cal%tchop)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%TCOLD:',   a%cal%tcold,b%cal%tcold)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%TAUS:',    a%cal%taus,b%cal%taus)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%TAUI:',    a%cal%taui,b%cal%taui)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%TATMI:',   a%cal%tatmi,b%cal%tatmi)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%TREC:',    a%cal%trec,b%cal%trec)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%CMODE:',   a%cal%cmode,b%cal%cmode)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%ATFAC:',   a%cal%atfac,b%cal%atfac)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%ALTI:',    a%cal%alti,b%cal%alti)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%COUNT[1]:',a%cal%count(1),b%cal%count(1))
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%COUNT[2]:',a%cal%count(2),b%cal%count(2))
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%COUNT[3]:',a%cal%count(3),b%cal%count(3))
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%LCALOF:',  a%cal%lcalof,b%cal%lcalof)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%BCALOF:',  a%cal%bcalof,b%cal%bcalof)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%GEOLONG:', a%cal%geolong,b%cal%geolong)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%CAL%GEOLAT:',  a%cal%geolat,b%cal%geolat)
  if (secwarned) error=.true.
  !
end subroutine class_diff_cal
!
subroutine class_diff_swi(a,b,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_diff_swi
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Print the differences between 2 observations (section SWITCHING)
  !---------------------------------------------------------------------
  type(header), intent(in)    :: a,b
  logical,      intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DIFF'
  character(len=*), parameter :: secname='Switching'
  logical :: secwarned
  character(len=64) :: elname
  integer(kind=4) :: iphas
  !
  if (class_diff_presec(class_sec_swi_id,secname,a,b)) then
     error =.true.
     return
  endif
  !
  secwarned = .false.
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%SWI%SWMOD:',a%swi%swmod,b%swi%swmod)
  call gag_diff_elem(rname,secname,secwarned,'R%HEAD%SWI%NPHAS:',a%swi%nphas,b%swi%nphas)
  do iphas=1,min(a%swi%nphas,b%swi%nphas)
    write(elname,'(A,I0,A)') 'R%HEAD%SWI%DECAL[',iphas,']:'
    call gag_diff_elem(rname,secname,secwarned,elname,a%swi%decal(iphas),b%swi%decal(iphas))
    write(elname,'(A,I0,A)') 'R%HEAD%SWI%DUREE[',iphas,']:'
    call gag_diff_elem(rname,secname,secwarned,elname,a%swi%duree(iphas),b%swi%duree(iphas))
    write(elname,'(A,I0,A)') 'R%HEAD%SWI%POIDS[',iphas,']:'
    call gag_diff_elem(rname,secname,secwarned,elname,a%swi%poids(iphas),b%swi%poids(iphas))
    write(elname,'(A,I0,A)') 'R%HEAD%SWI%LDECAL[',iphas,']:'
    call gag_diff_elem(rname,secname,secwarned,elname,a%swi%ldecal(iphas),b%swi%ldecal(iphas))
    write(elname,'(A,I0,A)') 'R%HEAD%SWI%BDECAL[',iphas,']:'
    call gag_diff_elem(rname,secname,secwarned,elname,a%swi%bdecal(iphas),b%swi%bdecal(iphas))
  enddo
  if (secwarned) error=.true.
  !
end subroutine class_diff_swi
!
!--- Generic utilities follow ------------------------------------------
!
function class_diff_presec(secid,secname,a,b)
  use gbl_message
  use classcore_interfaces, except_this=>class_diff_presec
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Return .true. if the section is absent from 1 or from the 2 headers
  !---------------------------------------------------------------------
  logical :: class_diff_presec
  integer(kind=4),  intent(in) :: secid    ! Section code
  character(len=*), intent(in) :: secname  !
  type(header),     intent(in) :: a,b      !
  !
  if (.not.a%presec(secid) .and. .not.b%presec(secid)) then
    class_diff_presec = .true.
  elseif (a%presec(secid) .and. .not.b%presec(secid)) then
    call class_message(seve%r,'DIFF','Only in first observation: '//(secname)//' section')
    class_diff_presec = .true.
  elseif (.not.a%presec(secid) .and. b%presec(secid)) then
    call class_message(seve%r,'DIFF','Only in second observation: '//(secname)//' section')
    class_diff_presec = .true.
  else
    class_diff_presec = .false.
  endif
  !
end function class_diff_presec
