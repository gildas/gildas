subroutine class_header(set,r,error)
  use gbl_message
  use classcore_interfaces, except_this=>class_header
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  !  Gives full header information
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(in)    :: r
  logical,             intent(inout) :: error
  !
  if (r%head%xnum.eq.0) then
    call class_message(seve%e,'HEADER','No R spectrum in memory')
    error = .true.
    return
  endif
  !
  call out0('Terminal',0.,0.,error)
  call titout(set,r%head,'F',' ')
  call out1(error)
  !
end subroutine class_header
