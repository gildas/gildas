!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine load_2d(set,line,error,user_function)
  use gildas_def
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>load_2d
  use class_types
  use class_index
  use class_data
  !---------------------------------------------------------------------
  ! @ public
  ! CLASS Support routine for command
  !   LOAD /NOCHECK
  ! Convert the entire index in a single 2-D data set for further plot
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)  :: set            !
  character(len=*),    intent(in)  :: line           ! Command Line
  logical,             intent(out) :: error          ! Return error code
  logical,             external    :: user_function  ! handling of user defined sections, intent(in)
  ! Local
  character(len=*), parameter :: rname='LOAD'
  type(observation) :: obs
  type(header) :: ref_head
  integer(kind=index_length) :: dim(4)
  integer(kind=entry_length) :: iobs,nobs,ksave
  integer(kind=4) :: iline,nchan
  character(len=message_length) :: mess
  character(len=12) :: stele  ! Telescope name for the index
  logical :: stele_c(12)  ! Characters to be compared
  !
  if (set%kind.ne.kind_spec) then
    call class_message(seve%e,rname,'Unsupported kind of data')
    error = .true.
    return
  endif
  !
  ! Initialization
  error = .false.
  call init_obs(obs)
  call init_obs(p)
  !
  ! Select what will be checked according to user input
  call consistency_check_selection(set,line,1,cx%cons,error)
  if (error) return  ! Ignore cx%cons%prob since we ignore cx%cons%off%prob
  cx%cons%off%check = .false.
  !
  ! Sanity check
  if (cx%next.le.1) then
     call class_message(seve%f,rname,'Index is empty')
     error = .true.
     return
  endif
  !
  ! Read first index spectrum to set its header as reference
  ksave = knext
  call get_first(set,obs,user_function,error)
  knext = ksave
  if (error) return
  ref_head = obs%head
  !
  ! Set checking tolerances
  call consistency_tole(ref_head,cx%cons)
  !
  ! Give user feedback on what will be done
  call consistency_print(set,ref_head,cx%cons)
  !
  ! Clean associated SIC variable
  call sic_delvariable('P',.false.,error)
  error = .false.
  !
  ! Allocation of P arrays
  nobs = cx%next-1
  nchan = ref_head%spe%nchan
  call reallocate_pq(nchan,nobs,error)
  if (error) return
  write(mess,*) 'Found ',nobs,' spectra of ',nchan,' channels'
  call class_message(seve%i,rname,mess)
  !
  ! Initialize header and x arrays
  p%head = ref_head
  call abscissa(set,p,error)
  if (error) return
  p%head%des%ndump = nobs
  p%head%xnum = -1 ! Is that correct ??? JP
  !
  ! Initialize telescope variables
  stele = ref_head%gen%teles
  stele_c(:) = .true.  ! At start, compare the 12 characters
  !
  ! Initialization of p
  p%data2 = ref_head%spe%bad
  !
  ! Loop on observation
  iline = 0
  do iobs = 1,nobs
     if (sic_ctrlc()) exit
     ! Read into buffer header section from file
     call robs(obs,cx%ind(iobs),error)
     if (error) cycle
     ! Read only interesting sections
     call rgen(set,obs,error)  ! General section
     if (error) cycle
     call rpos(set,obs,error)  ! Position section
     if (error) cycle
     call rspec(set,obs,error) ! Spectroscopic section
     if (error) cycle
     if (obs%head%presec(class_sec_cal_id)) then
       call rcal(set,obs,error)
       if (error) cycle
     endif
     if (obs%head%presec(class_sec_swi_id)) then
       call rswi(set,obs,error)  ! Switching section
       if (error) cycle
     endif
     ! Consistency checks
     if (cx%cons%check) then
        call spectrum_consistency_check(set,ref_head,obs%head,cx%cons)
        if (cx%cons%prob)  exit
     endif
     ! Telescope
     call sumlin_header_telescope(obs%head%gen%teles,stele_c,stele)
     ! Get data
     if (obs%head%spe%nchan.ne.nchan) then
       ! This can happen if user has disabled SPECTROSCOPY check. This leads
       ! to segfaults in rdata() and p%data2 assignments.
       write (mess,'(A,I0,A,I0,A,I0,A)')  &
         'Obs #',obs%head%gen%num,' has an unexpected number of channels (',  &
         obs%head%spe%nchan,' instead of ',nchan,')'
       call class_message(seve%e,rname,mess)
       if (.not.cx%cons%spe%check) then
         call class_message(seve%e,rname,  &
           'Check of spectroscopy sections is disabled but they differ too much')
       endif
       error = .true.
       goto 100
     endif
     call rdata(set,obs,obs%head%spe%nchan,obs%data1,error)
     if (error) cycle
     iline = iline+1
     p%data2(:,iline) = obs%data1
  enddo
  !
  p%head%gen%teles = stele
  !
  ! Compute limits
  call newlim(set,p,error)
  if (error) return
  !
  ! Define associated SIC structure
  dim(1) = nchan
  dim(2) = nobs
  if (.not.sic_varexist('p')) then
     call sic_defstructure('p%',.true.,error)
     if (error) return
  endif
  call sic_delvariable('p%x',.false.,error)
  call sic_delvariable('p%y',.false.,error)
  error = .false.
  call sic_def_dble('p%x',p%datax,1,dim,.true.,error)
  call sic_def_real('p%y',p%data2,2,dim,.false.,error)
  error = .false.
  !
  ! Feedback
  call index_consistency_analysis(set,cx%cons,rname)
  if (cx%cons%prob) then
     error = .true.
  endif
  !
100 continue
  !
  ! Cleaning
  call free_obs(obs)
end subroutine load_2d
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
