subroutine convert_vtype(set,obs,error)
  use gbl_constant
  use gbl_message
  use phys_const
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>convert_vtype
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !   Convert the velocity information to the current default type,
  ! according to SET VELO
  !
  ! Conversion is done only between LSRK and HELIO
  !
  ! Parameters of the solar motion relative to the LSR: apex
  ! and velocity. Right ascension and declination of the apex
  ! are referred to equinox 1900.0 (Lang, Astrophysical Formulae)
  !-------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(header),        intent(inout) :: obs
  logical,             intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='VTYPE'
  real(kind=8) :: x,y,v
  real(kind=4) :: xof,yof
  real(kind=8), parameter :: lii_apex=56.d0*pi/180.d0
  real(kind=8), parameter :: bii_apex=23.d0*pi/180.d0
  real(kind=8), parameter :: ra_apex =18.d0*pi/12.d0
  real(kind=8), parameter :: dec_apex=30.d0*pi/180.d0
  real(kind=8), parameter :: solar_velocity=20.0
  real(kind=4), parameter :: equinox=1900.0
  !
  if (obs%gen%kind.ne.kind_spec)  return
  !
  v = 0.d0
  x = 0.d0
  y = 0.d0
  xof = 0.
  yof = 0.
  !
  ! check for obvious cases
  if (set%veloc.eq.vel_aut .or. obs%spe%vtype.eq.set%veloc) then
     return
  elseif (obs%spe%vtype.eq.vel_ear) then
     call class_message(seve%w,rname,'Earth velocity not converted')
     return
  elseif (obs%spe%vtype.eq.vel_obs) then
     call class_message(seve%w,rname,'Observatory velocity not converted')
     return
  elseif (obs%spe%vtype.eq.vel_unk) then
     if (set%veloc.eq.vel_lsr) then
        call class_message(seve%w,rname,'Velocity type was unknown. Set to LSR')
     else
        call class_message(seve%w,rname,'Velocity type was unknown. Set to HELiocentric')
     endif
     obs%spe%vtype = set%veloc
     return
  elseif (iabs(obs%pos%system).eq.type_ga) then
     ! Compute the component of hel velocity in lsr, along the line of sight
     ! Horizontal offsets are ignored when computing solar motion. basis offsets
     ! could be handled correctly one day, but should not matter too much unless
     ! they become much larger than one degree.
     ! v = hel/lsr
     ! 1. conversion from hel to lsr: source/lsr = source/hel + hel/lsr
     ! 2. conversion from lsr to hel: source/hel = source/lsr - hel/lsr
     x = obs%pos%lam
     y = obs%pos%bet
     v = solar_velocity *   &
          &      ( cos(lii_apex)*cos(bii_apex)*cos(x)*cos(y)   &
          &      + sin(lii_apex)*cos(bii_apex)*sin(x)*cos(y)   &
          &      + sin(bii_apex)*sin(y) )
  elseif (iabs(obs%pos%system).eq.type_eq) then
     error = .false.
     if (obs%pos%system.eq.type_eq) then
        call equ_to_equ(obs%pos%lam,obs%pos%bet,obs%pos%lamof,obs%pos%betof,  &
                        obs%pos%equinox,x,y,xof,yof,1900.0,error)
     else
        call equ_to_equ(obs%pos%lam,obs%pos%bet,0.0,0.0,obs%pos%equinox,  &
                        x,y,xof,yof,1900.0,error)
     endif
     v = solar_velocity *   &
          &      ( cos(ra_apex)*cos(dec_apex)*cos(x)*cos(y)   &
          &      + sin(ra_apex)*cos(dec_apex)*sin(x)*cos(y)   &
          &      + sin(dec_apex)*sin(y) )
  elseif (iabs(obs%pos%system).eq.type_ic) then
    call class_message(seve%w,rname,'ICRS not supported, velocity not converted')
    return
  endif
  if (set%veloc.eq.vel_lsr) then
     obs%spe%voff = obs%spe%voff + v ! conversion from hel to lsr
     call class_message(seve%i,rname,'Convert velocity from HEL to LSR')
  else
     obs%spe%voff = obs%spe%voff - v ! conversion from lsr to hel
     call class_message(seve%i,rname,'Convert velocity from LSR to HEL')
  endif
  obs%spe%vtype = set%veloc
  !
end subroutine convert_vtype
!
subroutine convert_pos(set,obs,error)
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>convert_pos
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  !   Convert positions to current coordinate system and equinox,
  ! according to SET SYSTEM
  !----------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(header),        intent(inout) :: obs
  logical,             intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CONVERT'
  real(kind=8) :: x,y
  real(kind=4) :: xof,yof,xxx,yyy
  !
  if (set%coord.eq.0)  return
  !
  if (set%coord.eq.type_eq) then
    if (obs%pos%system.eq.type_ga) then
      call gal_to_equ (obs%pos%lam,obs%pos%bet,obs%pos%lamof,obs%pos%betof,  &
                       x,y,xof,yof,set%equinox,error)
      obs%pos%lam = x
      obs%pos%bet = y
      obs%pos%lamof   = xof
      obs%pos%betof   = yof
      obs%pos%equinox = set%equinox
      obs%pos%system  = type_eq
    elseif (obs%pos%system.eq.-type_ga) then
      xxx=0.0
      yyy=0.0
      call gal_to_equ (obs%pos%lam,obs%pos%bet,xxx,yyy,  &
                       x,y,xof,yof,set%equinox,error)
      obs%pos%lam = x
      obs%pos%bet = y
      obs%pos%equinox = set%equinox
      obs%pos%system  = -type_eq
    elseif (obs%pos%system.eq.type_eq .and. obs%pos%equinox.ne.set%equinox) then
      call equ_to_equ (obs%pos%lam,obs%pos%bet,obs%pos%lamof,obs%pos%betof,  &
                       obs%pos%equinox,x,y,xof,yof,set%equinox,error)
      obs%pos%lam = x
      obs%pos%bet = y
      obs%pos%lamof   = xof
      obs%pos%betof   = yof
      obs%pos%equinox = set%equinox
    elseif (obs%pos%system.eq.-type_eq .and. obs%pos%equinox.ne.set%equinox) then
      call equ_to_equ (obs%pos%lam,obs%pos%bet,0.0,0.0,obs%pos%equinox,  &
                       x,y,xof,yof,set%equinox,error)
      obs%pos%lam = x
      obs%pos%bet = y
      obs%pos%equinox = set%equinox
    elseif (abs(obs%pos%system).eq.type_ic) then
      call class_message(seve%e,rname,'Conversion from ICRS not implemented')
      error = .true.
      return
    endif
    !
  elseif (set%coord.eq.type_ga) then
    if (obs%pos%system.eq.type_eq) then
      call equ_to_gal (obs%pos%lam,obs%pos%bet,obs%pos%lamof,obs%pos%betof,  &
                       obs%pos%equinox,x,y,xof,yof,error)
      obs%pos%lam = x
      obs%pos%bet = y
      obs%pos%lamof = xof
      obs%pos%betof = yof
      obs%pos%system = type_ga
    elseif (obs%pos%system.eq.-type_eq) then
      xxx=0.0
      yyy=0.0
      call equ_to_gal (obs%pos%lam,obs%pos%bet,xxx,yyy,obs%pos%equinox,  &
                       x,y,xof,yof,error)
      obs%pos%lam = x
      obs%pos%bet = y
      obs%pos%system = -type_ga
    elseif (abs(obs%pos%system).eq.type_ic) then
      call class_message(seve%e,rname,'Conversion from ICRS not implemented')
      error = .true.
      return
    endif
    !
  elseif (set%coord.eq.type_ic) then
    call class_message(seve%e,rname,'Conversion to ICRS not implemented')
    error = .true.
    return
  endif
  !
  call set_angle(set,obs)
end subroutine convert_pos
!
subroutine convert_drop(set,obs,error)
  use classcore_interfaces, except_this=>convert_drop
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  !  Drop off data edges, according to SET DROP
  !----------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(inout) :: error
  !
  if (set%drop(1).ne.0)  &
    obs%spectre(1:set%drop(1)) = obs%cbad
  !
  if (set%drop(2).ne.0)  &
    obs%spectre(obs%cnchan-set%drop(2)+1:obs%cnchan) = obs%cbad
  !
end subroutine convert_drop
!
subroutine convert_scale(set,obs,verbose,error)
  use classcore_interfaces, except_this=>convert_scale
  use class_parameter
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  !  Rescale RY to new Y scale unit if needed, according to SET SCALE
  !----------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set      !
  type(observation),   intent(inout) :: obs
  logical,             intent(in)    :: verbose  ! Verbose warnings and infos?
  logical,             intent(inout) :: error
  !
  if (set%scale.eq.yunit_unknown)  return  ! Means no rescale desired
  !
  call modify_scale(obs,set%scale,verbose,error)
  if (error)  return
  !
end subroutine convert_scale
