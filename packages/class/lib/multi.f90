subroutine multi(set,line,r,error)
  use gildas_def
  use classcore_dependencies_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! Support routine for command
  !  MULTIPLY Factor
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  character(len=*),    intent(in)    :: line
  type(observation),   intent(inout) :: r
  logical,             intent(inout) :: error
  ! Local
  real(kind=4) :: factor
  !
  call sic_r4 (line,0,1,factor,.true.,error)
  if (error) return
  if (factor.eq.1.0) return
  if (sic_present(0,2)) then
    call rescale_data (r,factor)
  else
    call rescale_header (r%head,factor)
    call rescale_data (r,factor)
  endif
  call newdat(set,r,error)
end subroutine multi
!
subroutine rescale_data(r,r5)
  use gildas_def
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: r
  real(kind=4),      intent(in)    :: r5  ! Scale factor
  !
  where (r%data1.ne.r%cbad)  r%data1 = r5*r%data1
  !
end subroutine rescale_data
!
subroutine rescale_header(r,r5)
  use gildas_def
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  !  Apply a scaling factor to current header parameters.
  !----------------------------------------------------------------------
  type(header), intent(inout) :: r
  real(kind=4), intent(in)    :: r5  ! Scale factor
  ! Local
  real(kind=4) :: ar5
  integer(kind=4) :: i
  !
  ar5 = abs(r5)
  r%gen%tsys = ar5*r%gen%tsys
  !
  if (r%presec(class_sec_gau_id)) then
    do i=1,max(r%gau%nline,1)
      r%gau%nfit(3*i-2) = r5 * r%gau%nfit(3*i-2)
      r%gau%nerr(3*i-2) = ar5 * r%gau%nerr(3*i-2)
    enddo
    r%gau%sigba = ar5 * r%gau%sigba
    r%gau%sigra = ar5 * r%gau%sigra
  endif
  if (r%presec(class_sec_hfs_id)) then
    do i=1,max(r%hfs%nline,1)
      r%hfs%nfit(4*i-3) = r5 * r%hfs%nfit(4*i-3)
      r%hfs%nerr(4*i-3) = ar5 * r%hfs%nerr(4*i-3)
    enddo
    r%hfs%sigba = ar5 * r%hfs%sigba
    r%hfs%sigra = ar5 * r%hfs%sigra
  endif
  if (r%presec(class_sec_abs_id)) then
    do i=1,max(r%abs%nline,1)
      r%abs%nfit(4*i-3) = r5 * r%abs%nfit(4*i-3)
      r%abs%nerr(4*i-3) = ar5 * r%abs%nerr(4*i-3)
    enddo
    r%abs%sigba = ar5 * r%abs%sigba
    r%abs%sigra = ar5 * r%abs%sigra
  endif
  if (r%presec(class_sec_she_id)) then
    do i=1,max(r%she%nline,1)
      r%she%nfit(4*i-3) = r5 * r%she%nfit(4*i-3)
      r%she%nerr(4*i-3) = ar5 * r%she%nerr(4*i-3)
    enddo
    r%she%sigba = ar5 * r%she%sigba
    r%she%sigra = ar5 * r%she%sigra
 endif
  if (r%presec(class_sec_bas_id)) then
    r%bas%sigfi = ar5 * r%bas%sigfi
    r%bas%aire  = r5  * r%bas%aire
  endif
end subroutine rescale_header
