subroutine class_dump(line,r,t,error)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_dump
  use class_data
  !----------------------------------------------------------------------
  ! @ public (for libclass only)
  ! CLASS Support routine for command DUMP
  ! Types the contents of an obs.
  !----------------------------------------------------------------------
  character(len=*),  intent(in)    :: line   ! Input command line
  type(observation), intent(in)    :: r,t    !
  logical,           intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='DUMP'
  character(len=12) :: key,argum
  integer(kind=4) :: nc,nkey
  ! Keywords
  integer(kind=4), parameter :: mkeys=7
  integer(kind=4), parameter :: msections=26
  character(len=12) :: keys(mkeys),sections(msections)
  data keys /'ADDRESSES','DATA','FILE','INDEX','MEMORY','OTF','PLOT'/
  data sections /'ALL','HEADER','GENERAL','POSITION','SPECTRO','RESOLUTION',    &
    'BASELINE','ORIGIN','HISTORY','PLOT','FSWITCH','SWITCH','CALIBRATION',      &
    'CONTINUUM','BEAM','GAUSS','SHELL','NH3','ABS','DRIFT','COMMENT','SKYDIP',  &
    'SECTIONS','USER','ASSOCIATED','HERSCHEL'/
  !
  argum = ' '
  call sic_ke (line,0,1,argum,nc,.false.,error)
  if (error) return
  !
  if (nc.gt.0) then
    call sic_ambigs(rname,argum,key,nkey,keys,mkeys,error)
    if (error) return
    select case (key)
    case('FILE')  ! FILE [IN|OUT]
      call filedump(line,error)
    case('INDEX')
      call idump(error)
    case('DATA')  ! DATA [buffer]
      argum = 'R'
      call sic_ke (line,0,2,argum,nc,.false.,error)
      if (error) return
      if (argum.eq.'R') then
        call ddump(r,error)
      else if (argum.eq.'P') then
        call ddump(p,error)
      else if (argum.eq.'T') then
        call ddump(t,error)
      endif
    case('ADDRESSES')
      call adump(r,t,error)
    case('OTF')
      call odump(r,error)
    case('PLOT')
      call pdump(error)
    case('MEMORY')
      call mdump(error)
    end select
    !
  else
    ! /SECTION: identify desired header section (default: all)
    if (sic_present(1,0)) then
      call sic_ke (line,1,1,argum,nc,.false.,error)
      if (error) return
      if (nc.gt.0) then
        call sic_ambigs(rname,argum,key,nkey,sections,msections,error)
        if (error) return
      else
        key = 'ALL'
      endif
    else
      key = 'ALL'
    endif
    !
    ! 2nd argument: which buffer?
    argum = 'R'
    call sic_ke (line,0,2,argum,nc,.false.,error)
    if (error) return
    if (argum.eq.'R') then
      call rdump(r,key,error)
    else if (argum.eq.'P') then
      call rdump(p,key,error)
    else if (argum.eq.'T') then
      call rdump(t,key,error)
    endif
  endif
  !
end subroutine class_dump
!
subroutine rdump(obs,key,error)
  use gildas_def
  use gbl_constant
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>rdump
  use class_common
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(observation), intent(in)    :: obs    !
  character(len=*),  intent(in)    :: key    !
  logical,           intent(inout) :: error  !
  ! Local
  integer(kind=4) :: i,j
  character(len=11) :: cdobs,cdred
  !
  if (key.eq."ALL" .or. key.eq."HEADER" .or. key.eq."SECTIONS") then
     write(6,*) 'HEADER -----------------------'
     write(6,*) 'Index # ',obs%head%xnum
     write(6,*) 'Record # ',ibufobs%rstart, ', word # ',ibufobs%wstart
     call classic_entrydesc_dump(obs%desc)
     write(6,*) 'Present sections ',obs%head%presec
  endif
  !
  if (key.eq."ALL".or.key.eq."GENERAL") then
     if(obs%head%presec(class_sec_gen_id))then
        write(6,*) ' '
        write(6,*) 'GENERAL ---------------------------------------------------'
        write(6,*) 'r%head%gen%num   = ',obs%head%gen%num
        write(6,*) 'r%head%gen%ver   = ',obs%head%gen%ver
        write(6,*) 'r%head%gen%teles = ',obs%head%gen%teles
        call gag_todate(obs%head%gen%dobs,cdobs,error)
        call gag_todate(obs%head%gen%dred,cdred,error)
        write(6,*) 'r%head%gen%dobs  = ',cdobs
        write(6,*) 'r%head%gen%dred  = ',cdred
        write(6,*) 'r%head%gen%ut    = ',obs%head%gen%ut
        write(6,*) 'r%head%gen%st    = ',obs%head%gen%st
        write(6,*) 'r%head%gen%az    = ',obs%head%gen%az
        write(6,*) 'r%head%gen%el    = ',obs%head%gen%el
        write(6,*) 'r%head%gen%tau   = ',obs%head%gen%tau
        write(6,*) 'r%head%gen%tsys  = ',obs%head%gen%tsys
        write(6,*) 'r%head%gen%time  = ',obs%head%gen%time
        write(6,*) 'r%head%gen%parang= ',obs%head%gen%parang
        write(6,*) 'r%head%gen%yunit = ',obs%head%gen%yunit
     endif
  endif
  !
  if (key.eq."ALL".or.key.eq."POSITION") then
     if(obs%head%presec(class_sec_pos_id))then
        write(6,*) ' '
        write(6,*) 'POSITION ---------------------------------------------------'
        write(6,*) 'r%head%pos%sourc   = ',obs%head%pos%sourc
        write(6,*) 'r%head%pos%system  = ',obs%head%Pos%system
        write(6,*) 'r%head%pos%equinox = ',obs%head%Pos%equinox
        write(6,*) 'r%head%pos%proj    = ',obs%head%Pos%proj
        write(6,*) 'r%head%pos%lam     = ',obs%head%Pos%lam
        write(6,*) 'r%head%pos%bet     = ',obs%head%Pos%bet
        write(6,*) 'r%head%pos%projang = ',obs%head%Pos%projang
        write(6,*) 'r%head%pos%lamof   = ',obs%head%Pos%lamof
        write(6,*) 'r%head%pos%betof   = ',obs%head%Pos%betof
     endif
  endif
  !
  if (key.eq."ALL".or.key.eq."SPECTRO") then
     if(obs%head%presec(class_sec_spe_id))then
        write(6,*) ' '
        write(6,*) 'SPECTROSCOPY (for spectra) --------------------------------'
        write(6,*) 'r%head%spe%line    = "',obs%head%Spe%line,'"'
        write(6,*) 'r%head%spe%restf   = ', obs%head%spe%restf
        write(6,*) 'r%head%spe%image   = ', obs%head%spe%image
        write(6,*) 'r%head%spe%nchan   = ', obs%head%spe%nchan
        write(6,*) 'r%head%spe%rchan   = ',obs%head%Spe%rchan
        write(6,*) 'r%head%spe%doppler = ', obs%head%spe%doppler
        if (obs%head%spe%doppler.ne.-1.d0) then
          write(6,*) 'r%head%spe%fres    = ', obs%head%spe%fres, ' (obs.), ',  &
            obs%head%spe%fres/(1.d0+obs%head%spe%doppler),' (rest)'
        else
          write(6,*) 'r%head%spe%fres    = ', obs%head%spe%fres
        endif
        write(6,*) 'r%head%spe%vres    = ', obs%head%spe%vres
        write(6,*) 'r%head%spe%voff    = ',obs%head%Spe%voff
        write(6,*) 'r%head%spe%bad     = ', obs%head%spe%bad
        write(6,*) 'r%head%spe%vtype   = ', obs%head%Spe%vtype
        write(6,*) 'r%head%spe%vconv   = ',obs%head%Spe%vconv
        write(6,*) 'r%head%spe%vdire   = ',obs%head%Spe%vdire
     endif
  endif
  !
  if (key.eq."ALL".or.key.eq."RESOLUTION") then
     if(obs%head%presec(class_sec_res_id))then
        write(6,*) ' '
        write(6,*) 'RESOLUTION -------------------------------------------------'
        write(6,*) 'r%head%res%major  = ',obs%head%res%major
        write(6,*) 'r%head%res%minor  = ',obs%head%res%minor
        write(6,*) 'r%head%res%posang = ',obs%head%res%posang
     endif
  endif
  !
  if (key.eq."ALL".or.key.eq."BASELINE") then
     if(obs%head%presec(class_sec_bas_id))then
        write(6,*) ' '
        write(6,*) 'BASELINE (for spectra of drifts) --------------------------'
        write(6,*) 'r%head%bas%deg   = ',obs%head%Bas%deg
        write(6,*) 'r%head%bas%sigfi = ',obs%head%Bas%sigfi
        write(6,*) 'r%head%bas%aire  = ',obs%head%Bas%aire
        write(6,*) 'r%head%bas%nwind = ',obs%head%Bas%nwind
        if (obs%head%bas%nwind.gt.0 .and. obs%head%bas%nwind.lt.10) then
           write(6,*) 'r%head%bas%w1    = ',(obs%head%Bas%w1(j),j=1,obs%head%Bas%nwind)
           write(6,*) 'r%head%bas%w2    = ',(obs%head%Bas%w2(j),j=1,obs%head%Bas%nwind)
        endif
     endif
  endif
  !
  if (key.eq."ALL".or.key.eq."HISTORY") then  ! Origin
     if(obs%head%presec(class_sec_his_id))then
        write(6,*) ' '
        write(6,*) 'HISTORY ---------------------------------------------------'
        write(6,*) 'r%head%his%nseq  = ',obs%head%His%nseq
        if (obs%head%his%nseq.gt.0 .and. obs%head%his%nseq.lt.20) then
           write(6,*) 'r%head%his%start = ',(obs%head%His%start(j),j=1,obs%head%His%nseq)
           write(6,*) 'r%head%his%end   = ',(obs%head%His%end(j),j=1,obs%head%His%nseq)
        endif
     endif
  endif
  !
  if (key.eq."ALL".or.key.eq."PLOT") then
     if(obs%head%presec(class_sec_plo_id))then
        write(6,*) ' '
        write(6,*) 'PLOT Default plotting limits ------------------------------'
        write(6,*) 'r%head%plo%amin = ',obs%head%Plo%amin
        write(6,*) 'r%head%plo%amax = ',obs%head%Plo%amax
        write(6,*) 'r%head%plo%vmin = ',obs%head%Plo%vmin
        write(6,*) 'r%head%plo%vmax = ',obs%head%Plo%vmax
     endif
  endif
  !
  if (key.eq."ALL".or.key.eq."FSWITCH".or.key.eq."SWITCH") then
     if(obs%head%presec(class_sec_swi_id))then
        write(6,*) ' '
        write(6,*)  'FSWITCH (for spectra) ------------------------------------'
        write(6,*)  'r%head%fsw%nphas  = ',obs%head%swi%nphas
        write(6,*)  'r%head%fsw%swmod  = ',obs%head%swi%swmod
        write(6,*)  'r%head%fsw%decal  = ',(obs%head%swi%decal(i),i=1,obs%head%swi%nphas)
        write(6,*)  'r%head%fsw%duree  = ',(obs%head%swi%duree(i),i=1,obs%head%swi%nphas)
        write(6,*)  'r%head%fsw%poids  = ',(obs%head%swi%poids(i),i=1,obs%head%swi%nphas)
        write(6,*)  'r%head%fsw%ldecal = ',(obs%head%swi%ldecal(i),i=1,obs%head%swi%nphas)
        write(6,*)  'r%head%fsw%bdecal = ',(obs%head%swi%bdecal(i),i=1,obs%head%swi%nphas)
     endif
  endif
  !
  if (key.eq."ALL".or.key.eq."CALIBRATION") then
     if(obs%head%presec(class_sec_cal_id))then
        write(6,*) ' '
        write(6,*)  'CALIBRATION ----------------------------------------------'
        write(6,*)  'r%head%cal%beeff    = ',obs%head%cal%beeff
        write(6,*)  'r%head%cal%foeff    = ',obs%head%cal%foeff
        write(6,*)  'r%head%cal%gaini    = ',obs%head%cal%gaini
        write(6,*)  'r%head%cal%h2omm    = ',obs%head%cal%h2omm
        write(6,*)  'r%head%cal%pamb     = ',obs%head%cal%pamb
        write(6,*)  'r%head%cal%tamb     = ',obs%head%cal%tamb
        write(6,*)  'r%head%cal%tatms    = ',obs%head%cal%tatms
        write(6,*)  'r%head%cal%tchop    = ',obs%head%cal%tchop
        write(6,*)  'r%head%cal%tcold    = ',obs%head%cal%tcold
        write(6,*)  'r%head%cal%taus     = ',obs%head%cal%taus
        write(6,*)  'r%head%cal%taui     = ',obs%head%cal%taui
        write(6,*)  'r%head%cal%tatmi    = ',obs%head%cal%tatmi
        write(6,*)  'r%head%cal%trec     = ',obs%head%cal%trec
        write(6,*)  'r%head%cal%cmode    = ',obs%head%cal%cmode
        write(6,*)  'r%head%cal%atfac    = ',obs%head%cal%atfac
        write(6,*)  'r%head%cal%alti     = ',obs%head%cal%alti
        write(6,*)  'r%head%cal%count(:) = ',obs%head%cal%count(:)
        write(6,*)  'r%head%cal%lcalof   = ',obs%head%cal%lcalof
        write(6,*)  'r%head%cal%bcalof   = ',obs%head%cal%bcalof
        write(6,*)  'r%head%cal%geolong  = ',obs%head%cal%geolong
        write(6,*)  'r%head%cal%geolat   = ',obs%head%cal%geolat
     endif
  endif
  !
  if (key.eq."ALL".or.key.eq."CONTINUUM") then
     if(obs%head%presec(class_sec_dri_id))then
        write(6,*) ' '
        write(6,*) 'CONTINUUM Double gaussian  (for drifts) -------------------'
        write(6,*) 'Observing Frequency ',obs%head%Dri%freq,obs%head%Dri%cimag
        write(6,*) 'Frequency Bandwidth ', obs%head%Dri%width
        write(6,*) 'Reference channel ',obs%head%Dri%rpoin
        write(6,*) 'Time at reference channel ', obs%head%Dri%tref
        write(6,*) 'Angle at reference channel ', obs%head%Dri%aref
        write(6,*) 'Position angle of the drift ', obs%head%Dri%apos
        write(6,*) 'Time Resolution (s) ', obs%head%Dri%tres
        write(6,*) 'Angular resolution (radian) ', obs%head%Dri%ares
        write(6,*) 'Blanking value ', obs%head%Dri%bad
        write(6,*) 'Number of data points ',obs%head%Dri%npoin
        write(6,*) 'Type of offsets ', obs%head%Dri%ctype
        write(6,*) 'Collimations ', obs%head%Dri%colla,obs%head%Dri%colle
     endif
  endif
  !
  if (key.eq."ALL".or.key.eq."BEAM") then  ! Beam-switch
     if(obs%head%presec(class_sec_bea_id))then
        write(6,*) ' '
        write(6,*)  'BEAM (for spectra or drifts) -----------------------------'
        write(6,*) 'Azimuth of observing (rad) ', obs%head%Bea%cazim
        write(6,*) 'Elevation of observing (rad) ', obs%head%Bea%celev
        write(6,*) 'Angular offset of reference beam ',obs%head%Bea%space
        write(6,*) 'Position angle of reference beam ', obs%head%Bea%bpos
        write(6,*) 'Coordinate system for RBPOS ', obs%head%Bea%btype
     endif
  endif
  !
  if (key.eq."ALL".or.key.eq."GAUSS") then
     if(obs%head%presec(class_sec_gau_id))then
        write(6,*) ' '
        write(6,*)  'GAUSS METHOD FIT RESULTS ---------------------------------'
        write(6,*) 'Number of components',obs%head%Gau%nline   ! Number of components
        write(6,*) 'Sigma on base', obs%head%Gau%sigba ! Sigma on base
        write(6,*) 'Sigma on line', obs%head%Gau%sigra
        write(6,*) 'Fit results', obs%head%Gau%nfit
        write(6,*) 'Fit errors', obs%head%Gau%nerr
     endif
  endif
  !
  if (key.eq."ALL".or.key.eq."SHELL") then
     if(obs%head%presec(class_sec_she_id))then
        write(6,*) ' '
        write(6,*)  'SHELL METHOD FIT RESULTS ---------------------------------'
        write(6,*) 'Number of components',obs%head%She%nline   ! Number of components
        write(6,*) 'Sigma on base', obs%head%She%sigba ! Sigma on base
        write(6,*) 'Sigma on line', obs%head%She%sigra
        write(6,*) 'Fit results', obs%head%She%nfit
        write(6,*) 'Fit errors', obs%head%She%nerr
     endif
  endif
  !
  if (key.eq."ALL".or.key.eq."NH3") then
     if(obs%head%presec(class_sec_hfs_id))then
        write(6,*) ' '
        write(6,*)  'NH3 METHOD FIT RESULTS -----------------------------------'
        write(6,*) 'Number of components',obs%head%Hfs%nline   ! Number of components
        write(6,*) 'Sigma on base', obs%head%Hfs%sigba ! Sigma on base
        write(6,*) 'Sigma on line', obs%head%Hfs%sigra
        write(6,*) 'Fit results', obs%head%Hfs%nfit
        write(6,*) 'Fit errors', obs%head%Hfs%nerr
     endif
  endif
  !
  if (key.eq."ALL".or.key.eq."ABS") then
     if(obs%head%presec(class_sec_abs_id))then
        write(6,*) ' '
        write(6,*)  'ABSORPTION METHOD FIT RESULTS ----------------------------'
        write(6,*) 'Number of components', obs%head%Abs%nline ! Number of components
        write(6,*) 'Sigma on base       ', obs%head%Abs%sigba ! Sigma on base
        write(6,*) 'Sigma on line       ', obs%head%Abs%sigra
        write(6,*) 'Fit results         ', obs%head%Abs%nfit
        write(6,*) 'Fit errors          ', obs%head%Abs%nerr
     endif
  endif
  !
  if (key.eq."ALL".or.key.eq."DRIFT") then
     if(obs%head%presec(class_sec_poi_id))then
        write(6,*) ' '
        write(6,*)  'CONTINUUM DRIFT FIT RESULTS ------------------------------'
        write(6,*) 'Number of components ', obs%head%poi%nline
        write (6,*) 'rms on Baseline and line ', obs%head%poi%sigba, obs%head%poi%sigra
        write (6,*) 'fit results ',obs%head%poi%nfit
        write (6,*) 'fit errors ',obs%head%poi%nerr
     endif
  endif
  !
  if (key.eq."ALL".or.key.eq."COMMENT") then
     if(obs%head%presec(class_sec_com_id))then
        write(6,*) ' '
        write(6,*) 'COMMENT  --------------------------------------------------'
        do i = 1,obs%head%com%ltext,76
           write(6,'(1X,A)') obs%head%com%ctext(i:min(i+75,obs%head%com%ltext))
        enddo
     endif
  endif
  !
  if (key.eq."ALL".or.key.eq."SKYDIP") then
     if(obs%head%presec(class_sec_sky_id))then
        write(6,*) ' '
        write(6,*) 'SKYDIP ----------------------------------------------------'
        write(6,*) 'Number of points on sky ',obs%head%sky%nsky
        write(6,*) 'Number of points on sky ',obs%head%sky%nchop
        write(6,*) 'Number of points on sky ',obs%head%sky%ncold
        write(6,*) 'Elevations ', obs%head%sky%elev(1:obs%head%sky%nsky)
        write(6,*) 'Power on sky ',obs%head%sky%emiss(1:obs%head%sky%nsky)
        write(6,*) 'Power on chopper ',obs%head%sky%chopp(1:obs%head%sky%nchop)
        write(6,*) 'Power on cold load ',obs%head%sky%cold(1:obs%head%sky%ncold)
     endif
  endif
  !
  if (key.eq."ALL".or.key.eq."XCOORDINATE") then
     if(obs%head%presec(class_sec_xcoo_id))then
        write(6,*) ' '
        write(6,*) 'X COORDINATES ---------------------------------------------'
        write(6,*) 'X unit ',obs%head%gen%xunit
        write(6,*) 'DATAX should be in RX'
     endif
  endif
  !
  if (key.eq."ALL" .or. key.eq."DESCRIPTOR") then
     if (obs%head%presec(class_sec_desc_id))then
        write(6,*) ' '
        write(6,*) 'DATA DESCRIPTOR -------------------------------------------'
        write(6,*) 'Number of dumps ',obs%head%des%ndump
        write(6,*) 'Length of one dump ',obs%head%des%ldump
        write(6,*) 'Length of dump header ',obs%head%des%ldpar
        write(6,*) 'Length of dump data ', obs%head%des%ldatl
     elseif (key.eq."DESCRIPTOR") then
        write(6,*) ' '
        write(6,*) 'NO DATA DESCRIPTOR ---------------------------'
        write(6,*) 'Number of dumps ',obs%head%des%ndump
        write(6,*) 'Length of one dump ',obs%head%des%ldump
     endif
  endif
  !
  if (key.eq."ALL" .or. key.eq."USER") then  ! User Section
     if (obs%head%presec(class_sec_user_id))then
        write(6,*) ' '
        write(6,*) 'USER ------------------------------------------------------'
        call user_sec_dump(obs,error)
        if (error)  return
     elseif (key.eq.'USER') then
        write(6,*) ' '
        write(6,*) 'NO USER SECTION -------------------------------------------'
     endif
  endif
  !
  if (key.eq."ALL" .or. key.eq."HERSCHEL") then  ! Herschel-HIFI section
     if (obs%head%presec(class_sec_her_id))then
        write(6,*) ' '
        write(6,*) 'HERSCHEL --------------------------------------------------'
        write(6,11) 'Observation id',obs%head%her%obsid
        write(6,10) 'Instrument, proposal',obs%head%her%instrument,obs%head%her%proposal
        write(6,10) 'Astronomical Obs. Request',obs%head%her%aor
        write(6,11) 'Operational day number',obs%head%her%operday
        write(6,10) 'Date of observation',obs%head%her%dateobs
        write(6,10) 'End of observation',obs%head%her%dateend
        write(6,10) 'Observing mode',obs%head%her%obsmode
        write(6,12) 'Informative source LSR vel.',obs%head%her%vinfo
        write(6,12) 'Informative target redshift',obs%head%her%zinfo
        write(6,13) 'Spacecraft pointing pos. angle', obs%head%her%posangle
        write(6,13) 'Sky reference (a.k.a. OFF)', obs%head%her%reflam, obs%head%her%refbet
        write(6,13) 'ON average H and V coordinate (HIFI)',obs%head%her%hifavelam, obs%head%her%hifavebet
        write(6,12) 'MB and forward effic. used',obs%head%her%etamb, obs%head%her%etal
        write(6,10) 'Temperature scale',obs%head%her%tempscal
        write(6,13) 'Average LO freq Doppler corrected',obs%head%her%lodopave
        write(6,12) 'Sideband gain polynom. coeffs',obs%head%her%gim0, obs%head%her%gim1,obs%head%her%gim2, obs%head%her%gim3
        write(6,12) 'Calibr. mixer junction current',obs%head%her%mixercurh, obs%head%her%mixercurv
        write(6,10) 'Processing date',obs%head%her%datehcss
        write(6,10) 'HCSS and calibration versions',obs%head%her%hcssver, obs%head%her%calver
        write(6,12) 'Pipeline level',obs%head%her%level
     endif
  endif
  !
  if (key.eq."ALL" .or. key.eq."ASSOCIATED") then  ! Associated arrays
     if (obs%head%presec(class_sec_assoc_id)) then
        write(6,*) ' '
        write(6,*) 'ASSOCIATED ARRAYS -----------------------------------------'
        call class_assoc_dump(obs,error)
        if (error)  return
     elseif (key.eq.'ASSOCIATED') then
        write(6,*) ' '
        write(6,*) 'NO ASSOCIATED ARRAY ---------------------------------------'
     endif
  endif
  !
10 format(1X,A,T32,' = ',2A)
11 format(1X,A,T32,' = ',I0)
12 format(1X,A,T32,' = ',4(1PG14.7,1X))
13 format(1X,A,T32,' = ',4(1PG25.18,1X))
  !
end subroutine rdump
!
subroutine adump(r,t,error)
  use gildas_def
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  ! Dump addresses
  !----------------------------------------------------------------------
  type(observation), intent(in)    :: r,t
  logical,           intent(inout) :: error
  !
  write(6,*) 'ADDRESSES --------------------------'
  call aadump('R',r)
  call aadump('T',t)
end subroutine adump
!
subroutine aadump(chain,r)
  use gildas_def
  use gkernel_interfaces
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  ! Dump addresses
  !----------------------------------------------------------------------
  character(len=*), intent(in) :: chain
  type (OBSERVATION), intent(in) :: r
  !
  write(6,*) 'Addresses X & W',chain,locwrd(r%datax(1)), locwrd(r%dataw(1))
  write(6,*) 'Addresses 1 & 2 ',chain,locwrd(r%data1(1)), locwrd(r%data2(1,1))
end subroutine aadump
!
subroutine odump(r,error)
  use gildas_def
  use class_types
  use gbl_constant
  !----------------------------------------------------------------------
  ! @ private
  ! Dump OTF
  !----------------------------------------------------------------------
  type (OBSERVATION), intent(in) :: r
  logical, intent(out) :: error
  !
  integer i
  !
  error = .false.
  if (associated(r%dap)) then
     do i=1,r%head%des%ndump
        print *,i,r%dap(i)%lamof,r%dap(i)%betof
     enddo
  else
     print *,'Not an OTF'
  endif
end subroutine odump
!
subroutine ddump(r,error)
  use gildas_def
  use class_types
  use gbl_constant
  !----------------------------------------------------------------------
  ! @ private
  ! Dump data
  !----------------------------------------------------------------------
  type (OBSERVATION), intent(in) :: r
  logical, intent(out) :: error
  !
  integer j,ndata
  !
  error = .false.
  write(6,*) 'DATA -------------------------------'
  if (r%head%gen%kind.eq.kind_spec) then
     ndata = r%head%spe%nchan
  else
     ndata = r%head%dri%npoin
  endif
  write(6,*) 'ndata = ',ndata,'   data = '
  write(6,1005) (r%spectre(j),j=1,ndata)
1005 format(8(1pg10.2))
end subroutine ddump
!
subroutine idump(error)
  use classic_api
  use class_index
  !----------------------------------------------------------------------
  ! @ private
  ! Dump INDEX
  !----------------------------------------------------------------------
  logical, intent(out) :: error
  ! Local
  integer(kind=entry_length) :: i
  !
  error = .false.
  write(6,*) 'INDEX --------------------------'
  write(6,'(A,I0,A)') 'Input file index (ixnext=',ix%next,'):'
  write(6,'(5(A12))') 'Entry num','Obs. num','Obs. ver','Record','Word'
  do i=1,ix%next-1
     write(6,'(5(I12))') i,ix%num(i),ix%ver(i),ix%bloc(i),ix%word(i)
  enddo
  write(6,*) ' '
  write(6,'(A,I0,A)') 'Output file index (oxnext=',ox%next,'):'
  write(6,'(5(A12))') 'Entry num','Obs. num','Obs. ver','Record','Word'
  do i=1,ox%next-1
     write(6,'(5(I12))') i,ox%num(i),ox%ver(i),ox%bloc(i),ox%word(i)
  enddo
  write(6,*) ' '
  write(6,'(A,I0,A)') 'Current index (cxnext=',cx%next,'):'
  write(6,'(5(A12))') 'Entry num','Obs. num','Obs. ver','Record','Word'
  do i=1,cx%next-1
     write(6,'(5(I12))') cx%ind(i),cx%num(i),cx%ver(i),cx%bloc(i),cx%word(i)
  enddo
end subroutine idump
!
subroutine filedump(line,error)
  use gkernel_interfaces
  use classcore_interfaces, except_this=>filedump
  use class_common
  !----------------------------------------------------------------------
  ! @ private
  ! Dump FILE [IN|OUT]
  !----------------------------------------------------------------------
  character(len=*), intent(in)    :: line   !
  logical,          intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='DUMP'
  character(len=12) :: argum,found
  integer(kind=4) :: nc,nf
  integer(kind=4), parameter :: nwhat=2
  character(len=12) :: what(nwhat)
  data what /'IN','OUT'/
  !
  write(6,*) 'FILE ---------------------------'
  !
  if (sic_present(0,2)) then
    call sic_ke (line,0,2,argum,nc,.true.,error)
    if (error) return
    call sic_ambigs(rname,argum,found,nf,what,nwhat,error)
    if (error)  return
    if (nf.eq.1) then
      call filedump_one(filein,'i',error)
    else
      call filedump_one(fileout,'o',error)
    endif
  else
    call filedump_one(filein,'i',error)
    call filedump_one(fileout,'o',error)
  endif
  !
end subroutine filedump
!
subroutine filedump_one(file,name,error)
  use classic_api
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>filedump_one
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(classic_file_t), intent(inout) :: file   !
  character(len=*),     intent(in)    :: name   ! The File name (some prefix)
  logical,              intent(inout) :: error  !
  ! Local
  integer(kind=4) :: lind_unused
  !
  write(6,*) trim(name)//'lun = ',file%lun
  if (file%lun.eq.0) return
  !
  write(6,*) file%spec(1:file%nspec)
  call classic_filedesc_dump(file%desc,name)
  !
  if (file%desc%vind.eq.vind_v1) then
    lind_unused = lind_unused_v1
  else
    lind_unused = lind_unused_v2
  endif
  call classic_file_loss(file,lind_unused,error)
  if (error)  return
  !
end subroutine filedump_one
!
subroutine pdump(error)
  use plot_formula
  !----------------------------------------------------------------------
  ! @ private
  ! Dump PLOT
  !----------------------------------------------------------------------
  logical, intent(out) :: error
  error = .false.
  !
  print *,'GC ',GCX, GCX1, GCX2
  print *,'GV ',GVX, GVX1, GVX2
  print *,'GF ',GFX, GFX1, GFX2, GFXO
  print *,'GI ',GIX, GIX1, GIX2, GIXO
  print *,'GU ',GUX, GUX1, GUX2
  print *,'GY ',GUY, GUY1, GUY2
  print *,'GZ ',GUZ, GUZ1, GUZ2
end subroutine pdump
!
subroutine mdump(error)
  use gbl_message
  use classcore_interfaces, except_this=>mdump
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! Display the memory usage of the various Class buffers. For
  ! debugging purpose
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='DUMP'
  character(len=message_length) :: mess
  integer(kind=8) :: sizetot
  !
  sizetot = 0
  !
  ! --- Indexes --------------------------------------------------------
  if (associated(ix%num)) then
    write(mess,101)  &
      'IX','allocated for ',size(ix%num,kind=8),' entries (',ix%mobs,  &
      ' used)',24*4*size(ix%num,kind=8)/1024./1024.
    sizetot = sizetot+24*4*size(ix%num,kind=8)
  else
    write(mess,101)  'IX','not allocated'
  endif
  call class_message(seve%r,rname,mess)
  !
  if (associated(cx%num)) then
    write(mess,101)  &
      'CX','allocated for ',size(cx%num,kind=8),' entries (',cx%next-1,  &
      ' used)',24*4*size(cx%num,kind=8)/1024./1024.
    sizetot = sizetot+24*4*size(cx%num,kind=8)
  else
    write(mess,101)  'CX','not allocated'
  endif
  call class_message(seve%r,rname,mess)
  !
  if (associated(ox%num)) then
    write(mess,101)  &
      'OX','allocated for ',size(ox%num,kind=8),' entries (',ox%mobs,  &
      ' used)',9*4*size(ox%num,kind=8)/1024./1024.
    sizetot = sizetot+9*4*size(ox%num,kind=8)
  else
    write(mess,101)  'OX','not allocated'
  endif
  call class_message(seve%r,rname,mess)
  !
  ! --- Total ----------------------------------------------------------
  write(mess,103)  'TOTAL',sizetot/1024./1024.
  call class_message(seve%r,rname,mess)
  !
101 format(A,T14,': ',A,I0,A,I0,A,T64,F9.1,' MB')
103 format(A,T14,': ',            T64,F9.1,' MB')
end subroutine mdump
