subroutine class_draw(set,line,r,error)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_draw
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! Support routine for command
  !   DRAW [Args] [/PEN Ipen]
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  character(len=*),    intent(inout) :: line   ! Command line
  type(observation),   intent(inout) :: r      !
  logical,             intent(inout) :: error  ! Flag
  ! Local
  integer(kind=4), parameter :: optpen=1
  logical :: dopen
  integer(kind=4) :: oldpen,newpen
  !
  dopen = sic_present(optpen,0)
  if (dopen) then
    call sic_i4(line,optpen,1,newpen,.true.,error)
    if (error) return
    oldpen = gr_spen(newpen)
  endif
  !
  call class_draw_sub(set,line,r,error)
  if (error)  continue  ! Reset pen before leaving
  !
  if (dopen)  newpen = gr_spen(oldpen)
  !
end subroutine class_draw
!
subroutine class_draw_sub(set,line,r,error)
  use gildas_def
  use gbl_constant
  use phys_const
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_draw_sub
  use class_setup_new
  use class_types
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !   DRAW [FILL     Ichan]
  !   DRAW [KILL     Ichan]
  !   DRAW [LOWER    Xpos String [Angle]]
  !   DRAW [UPPER    Xpos String [Angle]]
  !   DRAW [MASK     [Ylev]]
  !   DRAW [TEXT     Xpos Ypos Text [Centering]]
  !   DRAW [WINDOW   [Yup] [Ylow]]
  !   DRAW MOLECULE  Xpos "Texte" [Angle]
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  character(len=*),    intent(inout) :: line   ! Command line
  type(observation),   intent(inout) :: r      !
  logical,             intent(inout) :: error  ! Flag
  ! Local
  character(len=*), parameter :: rname='DRAW'
  logical :: interactive,kill
  integer, parameter :: narg=9
  character(len=8) :: chain,argum,action(narg)
  character(len=1) :: ch,backslash
  character(len=160) :: text
  character(len=255) :: mess
  integer(kind=4) :: ic,nc,ltext,n,ichan,nl,lire,ier,iarg
  real(kind=4) :: xxp,yyp,xc,xv,xf,xi,xl,ya,n2
  real(kind=8) :: xf0,xi0,ux1,ux2
  data action/'FILL','KILL','LOWER','MASK','MOLECULE','TEXT','UPPER',  &
              'WINDOW','EXIT'/
  !
  call get_box(gx1,gx2,gy1,gy2)
  text = ' '
  backslash=char(92)
  kill = .false.
  lire = sic_lire()
  !
  if (sic_present(0,1)) then
     ! Parse DRAW kind from command line, the associated parameters
     ! will be read after
     call sic_ke (line,0,1,chain,nc,.true.,error)
     if (error) return
     call sic_ambigs(rname,chain,argum,iarg,action,narg,error)
     if (error)  return
     ch = argum(1:1)
     interactive = .false.
     !
     if (argum.eq.'MOLECULE') then
        call class_draw_molecule(line,r,error)
        ch = "E" ! DRAW MOLECULE never uses the cursor loop
     else
        ! Not MOLECULE
        if (sic_present(0,2)) then   ! Read arguments
           call sic_r4 (line,0,2,xc,.true.,error)
           if (error) return
           if (r%head%gen%kind.eq.kind_spec) then
              call abscissa_chan2velo(r%head,xc,xv)
              call abscissa_chan2sigoff(r%head,xc,xf)
              call abscissa_chan2sigabs(r%head,dble(xc),xf0)
              call abscissa_chan2imaabs(r%head,dble(xc),xi0)
           else
              call abscissa_chan2angl(r%head,xc,xv)
              call abscissa_chan2time(r%head,xc,xf)
           endif
        endif
     endif
     !
  else
     ! Get arguments from cursor
     if (.not. gtg_curs()) then
        call class_message(seve%e,rname,'No cursor available')
        error = .true.
        return
     endif
     call getcur(xc,xv,xf,xi,ya,xxp,yyp,ch)
     nl = index(line,' ')+1
     if (set%fft) then                           ! Fourier space
        n2  = r%head%spe%nchan/2+1.
        ux1 = 0.
        ux2 = 0.5/abs(r%head%spe%fres)
        xc  = 1.+(n2-1.)*(xxp-gx1)/(gx2-gx1)
        xf  = ux1+(ux2-ux1)/(gx2-gx1)*(xxp-gx1)
        xv  = clight_kms/xf/r%head%spe%restf
        xl  = clight_kms*xf*1d-3                     ! wavelength   [m]
        ch = ' '
     else
        if (ch.eq."Q") ch="E"
        xf0 = r%head%spe%restf+xf
        if (r%head%spe%image.eq.image_null) then
          xi0 = image_null
        else
          xi0 = r%head%spe%image-xf
        endif
     endif
     interactive = .true.
  endif
  !
  ! Handle cursor loop
  do while (.true.)
     if (set%fft) then
        select case (ch)
        case ('E')
          exit
        case ('F','K','L','U','M','T','W')
          call class_message(seve%w,rname,'Action '//ch//' ignored on FFT plot')
        case default
          if (r%head%gen%kind.eq.kind_spec) then
            write(6,1002) xc,xv,xf,xl
          elseif (r%head%gen%kind.eq.kind_cont) then
            call class_message(seve%w,rname,'Not implemented for continuum drifts')
            exit
          endif
          if (interactive)  line(nl:) = ' '  ! Last action was "none" => no argument after DRAW
        end select
     else
        select case(ch)
        case("F") ! FILL
           if (.not.interactive) then
              call sic_i4 (line,0,2,ichan,.true.,error)
              if (error) return
           else
              ichan = nint(xc)
           endif
           if (ichan.lt.1 .or. ichan.gt.r%cnchan) then
              call class_message(seve%w,'DRAW FILL','Channel outside spectrum')
           elseif (r%spectre(ichan).ne.r%cbad) then
              call class_message(seve%w,'DRAW FILL','Channel is not blanked')
           else
              r%spectre(ichan) = obs_good(r,ichan)
              if (interactive)  write(line(nl:),103) 'FILL',ichan
           endif
           !
           !
        case("K") ! KILL
           if (.not.interactive) then
              call sic_i4 (line,0,2,ichan,.true.,error)
              if (error) return
           else
              ichan = nint(xc)
           endif
           if (ichan.lt.1 .or. ichan.gt.r%cnchan) then
              call class_message(seve%w,rname,'Channel outside spectrum')
           else
              r%spectre(ichan) = r%cbad
              kill = .true.
              if (interactive) then
                ! Echo on screen
                write(mess,'(A,I0)')  'Killed channel number ',ichan
                call class_message(seve%i,rname,mess)
                ! Save in command line
                write(line(nl:),103) 'KILL',ichan
              endif
           endif
           !
           !
        case("L") ! DRAW LOWER Xpos "String"
           call class_draw_uplow(set,line,r,interactive,1,xxp,xf0,xi0,xc,xv,error)
           if (error)  return
           !
           !
        case("U") ! DRAW Upper Xpos "String"
           call class_draw_uplow(set,line,r,interactive,2,xxp,xf0,xi0,xc,xv,error)
           if (error)  return
           !
           !
        case("M") ! MASK
           call class_draw_mask(set,line,error)
           if (error)  return
           !
           !
        case("T") ! DRAW TEXT Xpos Ypos "Texte" Centering
           if (.not.interactive) then
              call sic_r4 (line,0,2,xxp,.true.,error)
              if (error) return
              call sic_r4 (line,0,3,yyp,.true.,error)
              if (error) return
              call sic_ch (line,0,4,text,ltext,.true.,error)
              if (error) return
              ic = 6
              call sic_i4 (line,0,5,ic,.false.,error)
              if (error) return
           else
              call sic_wprn('Text : ',text,ltext)
              call sic_wprn('Centering option [1-9] :',argum,n)
              read (argum(1:n),'(I1)',iostat=ier) ic
              if (ier.ne.0) then
                 ic = 0
              elseif (ic.ge.1 .and. ic.le.9) then
                 write (line(nl:),100) 'TEXT',xxp,yyp,text(1:ltext),ic
              endif
           endif
           if (ic.lt.1 .or. ic.gt.9) then
              call class_message(seve%e,rname,'Invalid Centering')
              error = .true.
              return
           endif
           call grelocate(xxp,yyp)
           write(mess,'(A,1X,F8.2,A,i1)')'LABEL "'//text(1:ltext)//'"',0.0,' /CENTER ',ic
           call gr_exec(mess)
           !
           !
        case ("W") ! WINDOW
           call class_draw_window(set,line,r,error)
           if (error)  return
           !
           !
        case("E") ! Exit the loop from cursor
           exit
           !
        case default
           if (r%head%gen%kind.eq.kind_spec) then
             if (xi0.eq.image_null) then
               write(6,999) xc,xv,xf0,xf,ya
             else
               write(6,1000) xc,xv,xf0,xi0,xf,ya
             endif
           elseif (r%head%gen%kind.eq.kind_cont) then
              write(6,1001) xc,xv*class_setup_get_fangle(),class_setup_get_angle(),ya
           endif
           line(nl:) = ' '  ! Last action was "none" => no argument after DRAW
        end select
     endif
     nl = lenc(line)
     !
     ! Compatibility SIC versions patch
     if (line(8:8).ne.backslash) then
        mess = 'ANALYSE'//backslash//line(1:nl)
        call sic_log (mess,nl+8,lire)
        if (lire.eq.0) call sic_insert(mess)
     else
        call sic_log (line,nl+8,lire)
        if (lire.eq.0) call sic_insert (line(1:nl))
     endif
     !
     if (.not.interactive) exit
     !
     call getcur(xc,xv,xf,xi,ya,xxp,yyp,ch)
     if (set%fft) then
        ! Fourier space
        ux1 = 0.
        ux2 = 0.5/abs(r%head%spe%fres)
        xc = 1.+(n2-1.)*(xxp-gx1)/(gx2-gx1)
        xf = ux1+(ux2-ux1)/(gx2-gx1)*(xxp-gx1)
        if (xf.ne.0.d0) then
           xv = clight_kms/xf/r%head%spe%restf
        else
           xv = 0.d0
        endif
        xl = clight_kms*xf*1d-3 ! wavelength   [m]
     else
        if (ch.eq."Q") ch = "E"
        xf0 = r%head%spe%restf+xf
        if (r%head%spe%image.eq.image_null) then
          xi0 = image_null
        else
          xi0 = r%head%spe%image-xf
        endif
        nl = index(line,' ')+1
     endif
  enddo
  !
  ! Normal end
  if (kill) then
     call newlimy(set,r,error)
     if (error) return
  endif
  !
  !
100 format(a,1x,1pg11.4,1x,1pg11.4,' "',a,'" ',i1)
103 format(a,1x,i6)
999 format(&
       t5, 'Channel number', t25,': ',f0.2,/,   &
       t5, 'Velocity', t25,': ',1pg12.5,' km/s',/,   &
       t5, 'Rest Frequency', t25,': ',1pg19.12,' MHz',/,   &
       t5, 'Image Frequency', t25,': N/A',/,   &
       t5, 'Offset Frequency', t25,': ',1pg12.5,' MHz',/,   &
       t5, 'Antenna Temperature', t25,': ',1pg12.5,' K')
1000 format(&
       t5, 'Channel number', t25,': ',f0.2,/,   &
       t5, 'Velocity', t25,': ',1pg12.5,' km/s',/,   &
       t5, 'Rest Frequency', t25,': ',1pg19.12,' MHz',/,   &
       t5, 'Image Frequency', t25,': ',1pg19.12,' MHz',/,   &
       t5, 'Offset Frequency', t25,': ',1pg12.5,' MHz',/,   &
       t5, 'Antenna Temperature', t25,': ',1pg12.5,' K')
1001 format(&
       t5, 'Channel number', t25,': ',f0.2,/,   &
       t5, 'Angle ', t25,': ',1pg12.5,a,/,   &
       t5, 'Antenna Temperature', t25,': ',1pg12.5,' K')
1002 format(&
       t5, 'Channel number', t25,': ',f0.2,/,   &
       t5, 'Velocity', t25,': ',1pg15.5,'     km/s',/,   &
       t5, 'Inverse Frequency', t25,': ',1pg19.12,' MHz-1',/,   &
       t5, 'Wavelength', t25,': ',1pg19.12,' m')
end subroutine class_draw_sub
!
subroutine class_draw_molecule(line,r,error)
  use phys_const
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_draw_molecule
  use class_types
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command:
  !  DRAW MOLECULE Frequency Name [Angle]
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: line   ! Input command line
  type(observation), intent(in)    :: r      !
  logical,           intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=160) :: text
  real(kind=8) :: x1
  integer :: ltext,ic
  real(kind=4) :: ang,si,xxp,y1,cdef,slength,yp1,yp2
  character(len=commandline_length) :: cline
  !
  ! Parse the arguments
  call sic_r8 (line,0,2,x1,.true.,error)
  if (error) return
  !
  text = ' '
  call sic_ch (line,0,3,text,ltext,.false.,error)
  if (error) return
  !
  ang = 90.0
  call sic_r4 (line,0,4,ang,.false.,error)
  if (error) return
  ang = mod(ang,360.)
  si = sin(pi*ang/180.0)
  !
  call sic_get_real('CHARACTER_SIZE',cdef,error)
  if (error)  return
  !
  ! Is the frequency in the signal frequency range?
  xxp = gx1 + gfx * (x1 - (gfxo+gfx1))
  if (xxp.gt.gx1 .and. xxp.lt.gx2) then
    call gr_segm('MOLECULE',error)
    ic = nint(gcx1 + (xxp-gx1)/gcx)
    ic = max(r%cimin,min(r%cimax,ic))
    y1 = r%spectre(ic)
    call gtg_charlen(ltext,text,cdef,slength,0)
    slength = slength+2*cdef
    yp2 = gy1 + slength*si
    yp1 = gy1 + guy*(y1-guy1)
    if (yp2.gt.yp1) then
      call gplot(xxp,yp1,3)
      call gplot(xxp,yp2,2)
    endif
    call gr_segm_close(error)
    if (error)  return
    !
    ! Open a new segment to plot the text (with solid line style)
    call grelocate(xxp,gy1)
    write(cline,'(A,1X,F8.2,A)') 'LABEL "'//text(1:ltext)//'"',ang,' /CENTER 6'
    call gr_exec(cline)
  endif
  !
  ! Is the frequency in the image frequency range?
  xxp = gx1 + gix * (x1-(gixo+gix1))
  if (xxp.gt.gx1 .and. xxp.lt.gx2) then
    call gr_segm('MOLECULE',error)
    ic = nint(gcx1 + (xxp-gx1)/gcx)
    ic = max(r%cimin,min(r%cimax,ic))
    y1 = r%spectre(ic)
    call gtg_charlen(ltext,text,cdef,slength,0)
    slength = slength+2*cdef
    yp2 = gy2 - slength*si
    yp1 = gy1 + guy*(y1-guy1)
    if (yp2.gt.yp1) then
      call gplot(xxp,yp1,3)
      call gplot(xxp,yp2,2)
    endif
    call gr_segm_close(error)
    if (error)  return
    !
    ! Open a new segment to plot the text (with solid line style)
    call grelocate(xxp,gy2)
    write(cline,'(A,1X,F8.2,A)') 'LABEL "'//text(1:ltext)//'"',ang,' /CENTER 6'
    call gr_exec(cline)
  endif
  !
end subroutine class_draw_molecule
!
subroutine class_draw_uplow(set,line,r,interactive,axis,xxp,xf0,xi0,xc,xv,error)
  use phys_const
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_draw_uplow
  use class_types
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !   ANA\DRAW UPPER|LOWER Xpos String
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set          !
  character(len=*),    intent(inout) :: line         ! Command line
  type(observation),   intent(in)    :: r            !
  logical,             intent(in)    :: interactive  ! Command line or cursor?
  integer(kind=4),     intent(in)    :: axis         ! 1 = lower, 2 = upper
  real(kind=4),        intent(inout) :: xxp          ! X position from cursor (Paper)
  real(kind=8),        intent(in)    :: xf0          ! X position from cursor (Signal)
  real(kind=8),        intent(in)    :: xi0          ! X position from cursor (Image)
  real(kind=4),        intent(in)    :: xc           ! X position from cursor (Channel)
  real(kind=4),        intent(in)    :: xv           ! X position from cursor (Velocity)
  logical,             intent(inout) :: error        ! Logical error flag
  ! Local
  character(len=160) :: text
  real(kind=8) :: x1
  real(kind=4) :: ang,cdef,y1,yp1,yp2,si,slength
  integer(kind=4) :: ic,ltext,nl,align
  character(len=commandline_length) :: cline
  character(len=5) :: skind
  !
  if (.not.interactive) then
    call sic_r8 (line,0,2,x1,.true.,error)
    if (error) return
    if (set%unitx(axis).eq.'F') then
      xxp = gx1 + gfx * (x1 - (gfxo+gfx1))
    elseif (set%unitx(axis).eq.'I') then
      xxp = gx1 + gix * (x1 - (gixo+gix1))
    elseif (set%unitx(axis).eq.'C') then
      xxp = gx1 + gcx * (x1 - gcx1)
    elseif (set%unitx(axis).eq.'V') then
      xxp = gx1 + gvx * (x1 - gvx1)
    endif
    text = ' '
    call sic_ch (line,0,3,text,ltext,.false.,error)
    if (error) return
    ang = 90.0
    call sic_r4 (line,0,4,ang,.false.,error)
    ang = mod(ang,360.)
    if (error) return
  else
    nl = index(line,' ')+1
    if (axis.eq.1) then
      skind = 'LOWER'
    else
      skind = 'UPPER'
    endif
    call sic_wprn('Text : ',text,ltext)
    !
    if (set%unitx(axis).eq.'F') then
      write (line(nl:),101) skind,xf0,text(1:ltext)
    elseif (set%unitx(axis).eq.'I') then
      write (line(nl:),101) skind,xi0,text(1:ltext)
    elseif (set%unitx(axis).eq.'C') then
      write (line(nl:),102) skind,xc,text(1:ltext)
    elseif (set%unitx(axis).eq.'V') then
      write (line(nl:),102) skind,xv,text(1:ltext)
    endif
    ang = 90.0
  endif
  !
  si = sin(pi*ang/180.0)
  if (xxp.gt.gx1 .and. xxp.lt.gx2) then
    ic = nint(gcx1 + (xxp-gx1)/gcx)
    ic = max(r%cimin,min(r%cimax,ic))
    y1 = r%spectre(ic)
    !
    call sic_get_real('CHARACTER_SIZE',cdef,error)
    if (error)  return
    call gtg_charlen(ltext,text,cdef,slength,0)
    slength = slength+2*cdef
    !
    if (axis.eq.1) then
      yp2 = gy1 + slength*si
    else
      yp2 = gy2 - slength*si
    endif
    yp1 = gy1 + guy*(y1-guy1)
    if (axis.eq.1 .and. yp1.gt.yp2 .or.  &
        axis.eq.2 .and. yp2.gt.yp1) then
      call gr_segm ('DRAW',error)
      call gplot(xxp,yp1,3)
      call gplot(xxp,yp2,2)
      call gr_segm_close(error)
      if (error)  return
    endif
    !
    ! Open a new segment to plot the text (with solid line style)
    if (axis.eq.1) then
      call grelocate(xxp,gy1+cdef)  ! 1 character away from bottom axis
      align = 6
    else
      call grelocate(xxp,gy2-cdef)  ! 1 character away from top axis
      align = 4
    endif
    write(cline,'(A,1X,F8.2,A,I0)')  &
      'LABEL "'//text(1:ltext)//'"',ang,' /CENTER ',align
    call gr_exec(cline)
  endif
  !
101 format(a,1x,1pg15.8,' "',a,'"')
102 format(a,1x,1pg11.4,' "',a,'"')
end subroutine class_draw_uplow
!
subroutine class_draw_mask(set,line,error)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_draw_mask
  use class_types
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  !  Support routine for command
  !    DRAW MASK [Ylev]
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  character(len=*),    intent(in)    :: line   ! Input command line
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  integer(kind=4) :: i
  real(kind=4) :: ylev
  !
  ylev = guy1*0.5
  call sic_r4(line,0,2,ylev,.false.,error)
  if (error) return
  !
  call gr_segm('DRAW',error)
  if (error)  return
  !
  do i=1,set%nmask
    call uplot(set%mask1(i),0.0,3)
    call uplot(set%mask1(i),ylev,2)
    call uplot(set%mask2(i),ylev,2)
    call uplot(set%mask2(i),0.0,2)
  enddo
  !
  call gr_segm_close(error)
  if (error)  return
  !
end subroutine class_draw_mask
!
subroutine class_draw_window(set,line,r,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_draw_window
  use class_types
  use class_index
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  !  Support routine for command
  !    DRAW WINDOW [Ymin [Ymax]]
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  character(len=*),    intent(in)    :: line   ! Input command line
  type(observation),   intent(in)    :: r      !
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='DRAW'
  integer(kind=4) :: i,nwind
  real(kind=8) :: xw1,xw2,yw1,yw2
  real(kind=4) :: w1(mwind),w2(mwind)
  character(len=commandline_length) :: comm
  type(class_assoc_sub_t) :: array
  !
  if (set%nwind.eq.setnwind_auto) then
    ! Take from data
    if (r%head%bas%nwind.eq.basnwind_assoc) then
      nwind = setnwind_assoc
    else
      nwind = r%head%bas%nwind
      w1(:) = r%head%bas%w1(:)
      w2(:) = r%head%bas%w2(:)
    endif
  else
    ! Take from SET WINDOW
    nwind = set%nwind
    w1(:) = set%wind1(:)
    w2(:) = set%wind2(:)
  endif
  !
  ! Sanity checks
  select case (nwind)
  case (setnwind_default)
    call class_message(seve%w,rname,'SET WINDOW is not defined')
    return
  case (setnwind_assoc)
    if (.not.class_assoc_exists(r,'LINE',array)) then
      call class_message(seve%w,rname,  &
        'No LINE Associated Array while SET WINDOW /ASSOCIATED is set')
      return
    endif
  case (0)
    call class_message(seve%w,rname,'0 windows defined')
    return
  end select
  !
  yw1 = guy1
  call sic_r8 (line,0,2,yw1,.false.,error)
  if (error)  return
  yw2 = 0.9*guy1+0.1*guy2
  call sic_r8 (line,0,3,yw2,.false.,error)
  if (error) return
  !
  call gr_segm ('DRAW',error)
  if (error)  return
  !
  if (nwind.eq.setnwind_polygon) then
    ! Draw the window(s) from polygon(s) for the current spectrum
    do i=1,nint(set%window_polygon(knext,1))
      xw1 = set%window_polygon(knext,2*i)
      xw2 = set%window_polygon(knext,2*i+1)
      call relocate(xw1,yw1)
      call draw(xw1,yw2)
      call draw(xw2,yw2)
      call draw(xw2,yw1)
    enddo
    !
  elseif (nwind.eq.setnwind_assoc) then
    ! LINE is an array of 0 and 1. We want 0 at level yw1, and 1 at level yw2
    write(comm,'(A,4(1X,G14.7))') 'LIMITS',gux1,gux2,(guy1-yw1)/(yw2-yw1),(guy2-yw1)/(yw2-yw1)
    call gr_exec(comm)
    error = gr_error()
    call spectr1d_draw(set,r,array%i4(:,1),array%badi4,error)
    if (error)  continue
    ! Revert to normal limits
    write(comm,'(A,4(1X,G14.7))') 'LIMITS',gux1,gux2,guy1,guy2
    call gr_exec(comm)
    error = gr_error()
    !
  else
    do i=1,nwind
      xw1 = w1(i)
      xw2 = w2(i)
      call relocate(xw1,yw1)
      call draw(xw1,yw2)
      call draw(xw2,yw2)
      call draw(xw2,yw1)
    enddo
  endif
  !
  call gr_segm_close(error)
  if (error)  return
  !
end subroutine class_draw_window
