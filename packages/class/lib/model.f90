module model_variables
  !---------------------------------------------------------------------
  ! Support module for MODEL command
  !---------------------------------------------------------------------
  character(len=*), parameter :: rname='MODEL'
  !
  logical :: spec      ! Kind is spectrum?
  integer :: ynchan    ! Number of channels
  !
  ! /REGULAR support variables
  logical      :: optregular    ! /REGULAR present
  logical      :: optregular_v  ! /REGULAR values are present
  real(kind=8) :: rchan         ! Reference channel
  real(kind=8) :: voff          ! Velocity at reference channel
  real(kind=8) :: restf         ! Rest frequency
  !
end module model_variables
!
subroutine model(set,line,r,t,error,user_function)
  use gildas_def
  use gbl_format
  use gbl_constant
  use phys_const
  use gbl_message
  use gkernel_types
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>model
  use class_types
  use model_variables
  !----------------------------------------------------------------------
  ! @ public
  ! CLASS ANALYSE Support routine for command
  ! MODEL
  ! 1     [/BLANK]
  ! 2     [/REGULAR [Xref Xval RestFrequency]]
  ! 3     [/FREQUENCY LineName RestFreq]
  ! 4     [/XAXIS Xref Xval Xinc Unit]
  !
  !  Define a spectrum from a SIC 1D array.
  !----------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  character(len=*),    intent(in)    :: line   ! Input command line
  type(observation),   intent(inout) :: r      !
  type(observation),   intent(in)    :: t      !
  logical,             intent(inout) :: error  ! Error status
  logical,             external      :: user_function
  ! Local
  character(len=12) :: yname,xname
  character(len=message_length) :: mess
  type(sic_descriptor_t) :: incay
  integer(4) :: nc
  logical :: newx,blank,old_r
  ! /BLANK support variables
  real(4) :: blank_value
  ! /FREQUENCY support variables
  character(len=12) :: lname  ! Line name
  integer :: lc
  ! /XAXIS support variables
  logical :: xaxis
  integer, parameter :: mutype=2
  character(len=*), parameter :: utype(mutype)=(/'VELOCITY ','FREQUENCY'/)
  real(kind=8) :: xval,xinc,vres
  real(kind=8) :: fres
  character(len=16) :: chain,uname
  integer :: nutype
  !
  ! Set default values. Some will be set thanks to options, remaining will
  ! be derived by 'abscissa' routine.
  restf= 3.e5  ! Rest frequency
  fres = 0.d0  ! Frequency resolution
  voff = 0.d0  ! Velocity offset
  vres = 0.d0  ! Velocity resolution
  lname = ' '  ! Line name
  !
  ! Decode line arguments
  ! 0) MODEL Y_variable [X_variable]
  call sic_ke(line,0,1,yname,nc,.true.,error)
  if (error) return
  newx = sic_present(0,2)
  !
  ! 1) /BLANK Bval
  blank = sic_present(1,0)
  if (blank) then
     call sic_r4(line,1,1,blank_value,.true.,error)
     if (error) return
  else
     ! Else default blanking value if not yet defined in header
     blank_value = class_bad
  endif
  !
  ! 2) /REGULAR [Xref Xval RestFreq]
  optregular   = sic_present(2,0)
  optregular_v = sic_present(2,1)
  if (optregular_v) then
     call sic_r8(line,2,1,rchan,.true.,error)  ! reference channel
     call sic_r8(line,2,2,voff ,.true.,error)  ! in km/s
     call sic_r8(line,2,3,restf,.true.,error)  ! in MHz
     if (error) return
  endif
  !
  ! 3) /FREQUENCY Name RestFreq
  if (sic_present(3,0)) then
     call sic_ch(line,3,1,lname,lc,.true.,error)  ! Line name
     call sic_r8(line,3,2,restf,.true.,error)     ! in MHz
     if (error) return
  endif
  !
  ! 4) /XAXIS Xref Xval Xinc Unit
  xaxis = sic_present(4,0)
  if (xaxis) then
     if (optregular) then
        call class_message(seve%e,rname,  &
         'Incompatible options /REGULAR and /XAXIS')
        error=.true.
        return
     endif
     if (newx) then
        call class_message(seve%e,rname,  &
          'Option /XAXIS is incompatible with a X variable')
        error=.true.
        return
     endif
     call sic_r8(line,4,1,rchan,.true.,error)
     call sic_r8(line,4,2,xval,.true.,error)
     call sic_r8(line,4,3,xinc,.true.,error)
     call sic_ke(line,4,4,chain,lc,.true.,error)
     if (error) return
     call sic_ambigs(rname//' /XAXIS',chain,uname,nutype,utype,mutype,error)
     if (error) return
  endif
  !
  ! Incarnate the Y variable
  call model_Y_from_var(yname,incay,error)
  if (error) return
  !
  ! Spectrum type: default = spectrum
  spec = r%head%gen%kind.eq.kind_spec
  if (r%head%gen%kind.eq.kind_sky) then
     call class_message(seve%e,rname,'Can not handle skydips')
     error = .true.
     return
  endif
  !
  ! Where X axis will be found?
  old_r = sic_varexist('RX')
  if (.not.old_r .and. .not.newx) then
     ! No R buffer, no X variable => must define X axis thanks to options.
     if (.not.optregular_v .and. .not.xaxis) then
        call class_message(seve%e,rname,  &
          'No previous spectrum in memory. Use /XAXIS or provide X array.')
        error = .true.
        return
     endif
  endif
  !
  ! Set up a default header, for missing sections (ie. do not erase previous
  ! values if they exist)
  call model_header_default(set,r,error)
  if (error)  return
  !
  ! Compute X axis into R buffer
  if (old_r .and. .not.newx .and. .not.xaxis) then
     ! Use previous R buffer only if no new X array is provided
     !
     ! Check that Y dimension matches X array dimension
     if (ynchan.ne.r%cnchan) then
        call class_message(seve%e,rname,'X and Y Dimensions do not fit.')
        error = .true.
        return
     endif
     !
     ! Reallocate RT buffers
     r%cnchan = ynchan
     if (spec) then
       r%head%spe%nchan = ynchan
     else
       r%head%dri%npoin = ynchan
     endif
     call reallocate_obs(r,ynchan,error)
     if (error) return
     !
     ! Copy X data. In case of irregular sampling, X values are stored in
     ! datav. datax in then filled appropriately by newdat.
     r%datav = t%datav
     !
  else
     ! Read or compute new X axis and new header
     call sic_gagdate(r%head%gen%dobs)
     r%head%presec(class_sec_xcoo_id) = .false.  ! Regular axes in most cases, except where relevant
     !
     ! Allocate RT buffers
     r%cnchan = ynchan
     if (spec) then
       r%head%spe%nchan = ynchan
     else
       r%head%dri%npoin = ynchan
     endif
     call reallocate_obs(r,ynchan,error)
     if (error) return
     !
     if (newx) then
        ! Build X axis from X_variable
        !
        ! Check variable name
        call sic_ke (line,0,2,xname,nc,.true.,error)
        if (error) return
        !
        ! Set r%datav from X variable
        call model_X_from_var(xname,r,error)
        if (error) return
        !
     elseif (optregular_v) then
        ! Compute new X axis and header from /REGULAR values. Syntax:
        !     MODEL Y /REGULAR Xref Xval RestFreq
        ! This is non-sense: vres is taken from previous R buffer but the
        ! other parameters are taken from command line...
        !
        ! Initialize header
        if (spec) then
           r%head%spe%rchan = rchan                  ! reference channel
           r%head%spe%voff  = voff                   ! in km/s
           r%head%spe%restf = restf                  ! in MHz
           r%head%spe%vres  = r%datav(2)-r%datav(1)  !
           r%head%spe%fres  = fres                   ! Will be computed by abscissa()
        else
           r%head%dri%ares  = r%datav(2)-r%datav(1)
        endif
        !
        ! Compute X axis
        call abscissa(set,r,error)
        !
     elseif (xaxis) then
        ! Compute the new X axis and header from /XAXIS values
        !
        select case (uname)
        case('VELOCITY')
           voff = xval
           vres = xinc
           ! fres will be computed by abscissa()
        case('FREQUENCY')
           restf = xval
           fres = xinc
           ! vres will be computed by abscissa()
        case default
           call class_message(seve%e,rname//' /XAXIS','Unrecognized unit '//uname)
           error=.true.
           return
        end select
        !
        if (spec) then
           r%head%spe%rchan = rchan  ! [    ] Reference channel
           r%head%spe%voff  = voff   ! [km/s] Velocity at reference channel
           r%head%spe%vres  = vres   ! [km/s] Velocity resolution
           r%head%spe%restf = restf  ! [ MHz] Rest frequency
           r%head%spe%fres  = fres   ! [ MHz] Frequency resolution
        else
           call class_message(seve%e,rname//' /XAXIS','Drifts not yet implemented')
           error = .true.
           return
        endif
        !
        ! Compute X axis
        call abscissa(set,r,error)
        if (error) return
        !
     else
        ! Do not know how to compute X values
        call class_message(seve%e,rname,  &
          'No previous spectrum in memory. Use /XAXIS or provide X array.')
        error = .true.
        return
     endif
  endif
  !
  ! Read the Y data into R buffer
  call model_Y_to_R(incay%addr,r)
  call sic_volatile(incay)
  !
  ! X data sampling
  ! Check if old R buffer was regularly sampled
  if (.not.newx.and.t%head%presec(class_sec_xcoo_id).and.optregular)  &
     call class_message(seve%w,rname,'X array from previous R buffer was not regular.')
  !
  ! Update header
  if (lname.ne.' ')  r%head%spe%line = lname
  !
  ! Defaults
  if (spec) then
     if (r%head%spe%vres .eq.0.d0)  r%head%spe%vres  = 1.d0
     if (r%head%spe%restf.eq.0.d0)  r%head%spe%restf = 3.d5
     if (r%head%spe%fres .eq.0.d0)  r%head%spe%fres  = -r%head%spe%restf*r%head%spe%vres/clight_kms
     r%head%gen%xunit  = a_wave   ! Presumably optical data
  endif
  !
  ! Blanking value
  if (blank) then
     r%cbad = blank_value
  else
     if (r%cbad.eq.0.0)  r%cbad = blank_value
  endif
  write(mess,*) 'Blanking value: ',r%cbad
  call class_message(seve%i,rname,mess)
  if (spec) then
     r%head%spe%bad = r%cbad
  else
     r%head%dri%bad = r%cbad
  endif
  !
  call newdat(set,r,error)
  !
end subroutine model
!
subroutine model_header_default(set,obs,error)
  use gbl_constant
  use classcore_interfaces, except_this=>model_header_default
  use class_parameter
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Set up the needed part of the header, if not defined yet
  ! Should use RZERO to nullify section by section
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(observation),   intent(inout) :: obs    !
  logical,             intent(inout) :: error  !
  ! Local
  character(len=12), parameter :: cmodel='MODEL       '
  !
  ! Modeled observation will be in memory but was not read from a file
  obs%head%xnum = -1
  !
  ! General
  if (.not.obs%head%presec(class_sec_gen_id)) then
    obs%head%presec(class_sec_gen_id) = .true.
    obs%head%gen%num = 0
    obs%head%gen%ver = 0
    obs%head%gen%teles = cmodel
    call sic_date(obs%head%gen%cdobs)
    call sic_gagdate(obs%head%gen%dobs)
    obs%head%gen%cdred = obs%head%gen%cdobs
    obs%head%gen%dred = obs%head%gen%dobs
    obs%head%gen%kind = set%kind  ! Use current kind of observation
    obs%head%gen%qual = qual_unknown
    obs%head%gen%subscan = 0
    obs%head%gen%scan = 0
    obs%head%gen%ut = 0.d0
    obs%head%gen%st = 0.d0
    obs%head%gen%az = 0.
    obs%head%gen%el = 0.
    obs%head%gen%tau = 0.
    obs%head%gen%tsys = 0.
    obs%head%gen%time = 0.
    obs%head%gen%parang = parang_null
    obs%head%gen%yunit = yunit_unknown
    obs%head%gen%xunit = a_wave   ! Presumably optical data
  endif
  !
  ! Position
  if (.not.obs%head%presec(class_sec_pos_id)) then
    obs%head%presec(class_sec_pos_id) = .true.
    obs%head%pos%sourc   = cmodel
    obs%head%pos%system  = type_eq
    obs%head%pos%equinox = 0.
    obs%head%pos%proj    = p_none
    obs%head%pos%lam     = 0.d0
    obs%head%pos%bet     = 0.d0
    obs%head%pos%projang = 0.d0
    obs%head%pos%lamof   = 0.
    obs%head%pos%betof   = 0.
  endif
  !
  ! Spectro
  if (obs%head%gen%kind.eq.kind_spec .and.  &
      .not.obs%head%presec(class_sec_spe_id)) then
    obs%head%presec(class_sec_spe_id) = .true.
    obs%head%spe%line = cmodel
    obs%head%spe%restf = 0.d0
    obs%head%spe%nchan = 0
    obs%head%spe%rchan = 0.d0
    obs%head%spe%fres = 0.d0
    obs%head%spe%vres = 0.d0
    obs%head%spe%voff = 0.d0
    obs%head%spe%bad = -1000.
    obs%head%spe%image = image_null
    obs%head%spe%vtype = vel_unk
    obs%head%spe%vconv = vconv_unk
    obs%head%spe%doppler = 0.d0
  endif
  !
  if (obs%head%gen%kind.eq.kind_cont .and.  &
      .not.obs%head%presec(class_sec_dri_id)) then
    obs%head%presec(class_sec_dri_id) = .true.
    obs%head%dri%freq = 0.d0
    obs%head%dri%width = 0.
    obs%head%dri%npoin = 0
    obs%head%dri%rpoin = 0.
    obs%head%dri%tref = 0.
    obs%head%dri%aref = 0.
    obs%head%dri%apos = 0.
    obs%head%dri%tres = 0.
    obs%head%dri%ares = 0.
    obs%head%dri%bad = -1000.
    obs%head%dri%ctype = type_eq
    obs%head%dri%cimag = 0.d0
    obs%head%dri%colla = 0.
    obs%head%dri%colle = 0.
  endif
  !
  ! Be Compatible with OTF modifs:
  obs%head%presec(class_sec_desc_id) = .false.
  obs%head%des%ndump=1
  !
end subroutine model_header_default
!
subroutine model_Y_from_var(yname,incay,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use gkernel_types
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>model_Y_from_var
  use model_variables
  !---------------------------------------------------------------------
  ! @ private
  ! Incarnate Y variable given its name.
  !---------------------------------------------------------------------
  character(len=*),       intent(in)    :: yname  ! Name of Y variable
  type(sic_descriptor_t), intent(out)   :: incay  ! Y variable incarnation
  logical,                intent(inout) :: error  ! Error flag
  ! Local
  type(sic_descriptor_t) :: desc
  logical :: found
  !
  ! Check Y variable
  call sic_descriptor(yname,desc,found)
  if (.not.found) then
     call class_message(seve%e,rname,'No such Y variable')
     error = .true.
     return
  endif
  !
  ! Read input Y array
  call sic_incarnate(fmt_r4,desc,incay,error)
  if (error) return
  !
  ! Check the variable is a 1D array.
  if (incay%ndim.ne.1) then
     call class_message(seve%e,rname,'Invalid number of dimensions')
     error = .true.
     return
  endif
  !
  ynchan = incay%dims(1)  ! Number of channels for output model
  !
end subroutine model_Y_from_var
!
subroutine model_X_from_var(xname,r,error)
  use gildas_def
  use gbl_format
  use phys_const
  use gbl_message
  use gkernel_types
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>model_X_from_var
  use class_types
  use model_variables
  !---------------------------------------------------------------------
  ! @ private
  ! Read X axis from variable 'xname', and store it in r%datav. Set also
  ! r%head. In case of irregular sampling, X values are stored in datav.
  ! datax in then filled appropriately by newdat.
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: xname  ! Name of X variable
  type(observation), intent(inout) :: r      !
  logical,           intent(inout) :: error  ! Error flag
  ! Global
  integer(4) :: memory(1)  ! Local use, can be local
  ! Local
  type(sic_descriptor_t) :: desc,incax
  integer(kind=address_length) :: ipi
  logical :: found,isregular
  !
  ! Get X variable descriptor
  call sic_descriptor(xname,desc,found)
  if (.not.found) then
    call class_message(seve%e,rname,'No such variable '//xname)
    error = .true.
    return
  endif
  !
  ! Incarnate it
  call sic_incarnate(fmt_r8,desc,incax,error)
  if (error) return
  !
  ! Check if the variable is a 1D array
  if (incax%ndim.ne.1) then
    call class_message(seve%e,rname,'Invalid number of dimensions')
    error = .true.
    return
  endif
  !
  ! Check that Y dimension matches X array dimension
  if (incax%dims(1).ne.ynchan) then
    call class_message(seve%e,rname,'X and Y dimensions do not fit')
    error = .true.
    return
  endif
  !
  ! Read X data in R
  ipi = gag_pointer(incax%addr,memory)
  if (xdata_kind.eq.4) then
    call r8tor4(memory(ipi),r%datav,r%cnchan)
  else
    call r8tor8(memory(ipi),r%datav,r%cnchan)
  endif
  call sic_volatile(incax)
  !
  isregular = model_X_isregular(r%datav,r%cnchan,0.02)
  if (optregular .and. .not.isregular) then
    call class_message(seve%e,rname,'X axis is not regularly sampled (to 1/50th of channel or less)')
    call class_message(seve%e,rname,'Incompatible with /REGULAR option')
    error = .true.
    return
  elseif (.not.optregular .and. isregular) then
    call class_message(seve%i,rname,'X axis seems regularly sampled (to 1/50th of channel or less)')
    call class_message(seve%i,rname,'Assuming /REGULAR option')
    optregular = .true.
  endif
  !
  ! Set R header
  if (optregular_v) then
    ! Compute new header from input values
    if (spec) then
      r%head%spe%rchan = rchan                  ! reference channel
      r%head%spe%voff  = voff                   ! in km/s
      r%head%spe%restf = restf                  ! in MHz
      r%head%spe%vres  = r%datav(2)-r%datav(1)
      r%head%spe%fres  = -r%head%spe%restf*r%head%spe%vres/clight_kms  ! in MHz
    else
      r%head%dri%ares = r%datav(2)-r%datav(1)
    endif
    !
  elseif (optregular) then
    ! Default values in header
    if (spec) then
      ! Set reference at middle of bandpass
      r%head%spe%restf = 0.
      r%head%spe%rchan = (r%head%spe%nchan+1)*0.5d0
      r%head%spe%voff  = 0.5d0*(r%datav(1)+r%datav(r%head%spe%nchan))
      r%head%spe%vres  = r%datav(2)-r%datav(1)
      r%head%spe%fres  = 0.d0
    else
      call class_message(seve%w,rname,'Do not what to do for non-spectroscopy file.')
    endif
    !
  else
    ! Default values in header for irregularly sampled
    call class_message(seve%w,rname,'X axis is not regularly sampled. Update header by hand.')
    r%head%presec(class_sec_xcoo_id) = .true.
    !
  endif
  !
end subroutine model_X_from_var
!
function model_X_isregular(datax,nx,tol)
  use class_parameter
  !---------------------------------------------------------------------
  ! @ private
  !  Check if the input array is regularly spaced or not. Tolerance is
  ! given as a fraction of the first interval. Positive or negative
  ! increments are accepted.
  !  Single precision version.
  !---------------------------------------------------------------------
  logical :: model_X_isregular  ! Function value on return
  integer(kind=4),       intent(in) :: nx         ! Number of values
  real(kind=xdata_kind), intent(in) :: datax(nx)  ! Values
  real(kind=4),          intent(in) :: tol        ! Tolerance
  ! Local
  integer(kind=4) :: ix
  real(kind=xdata_kind) :: xinc
  !
  model_X_isregular = .false.
  if (nx.le.1)  return
  !
  xinc = datax(2)-datax(1)
  do ix=3,nx
    if ( abs( (datax(ix)-datax(ix-1))/xinc - 1.) .gt. tol )  return
  enddo
  !
  model_X_isregular = .true.
  !
end function model_X_isregular
!
function model_obs_fillgaps(obs,tol)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>model_obs_fillgaps
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Insert 'bad' channels in obs%datav and obs%data1 so that obs%datav
  ! is regularly sampled. This is suited for obs%datav(:) being a
  ! regular grid except some missing channels (gaps).
  !  Axes less regular than tolerance are assumed completely irregular
  ! and the function will fail. Use a resampling in this case.
  !---------------------------------------------------------------------
  logical :: model_obs_fillgaps  ! success = .true. on return
  type(observation), intent(inout) :: obs    !
  real(kind=8),      intent(in)    :: tol    ! Tolerance (channel fraction)
  ! Local
  character(len=*), parameter :: rname='OBS>FILLGAPS'
  real(kind=8) :: xinc
  integer(kind=4) :: ier,iold,inew,new_ndata
  real(kind=xdata_kind), allocatable :: datav(:),data1(:)
  logical :: error
  !
  error = .false.
  model_obs_fillgaps = .false.  ! Failure by default
  !
  ! Should have a better way to find the grid interval. FFT? Equivalence
  ! classes of all differences?
  xinc = obs%datav(2)-obs%datav(1)
  !
  new_ndata = nint((obs%datav(obs%cnchan)-obs%datav(1))/xinc) + 1
  !
  allocate(datav(new_ndata),data1(new_ndata),stat=ier)
  if (failed_allocate('MODEL','data rrays',ier,error))  return
  !
  iold = 1
  inew = 1
  datav(inew) = obs%datav(iold)
  data1(inew) = obs%data1(iold)
  !
  iold = 2
  do inew = 2,new_ndata
    if ( abs( (obs%datav(iold)-datav(inew-1))/xinc - 1.) .le. tol ) then
      ! Spacing is correct
      datav(inew) = obs%datav(iold)
      data1(inew) = obs%data1(iold)
      iold = iold+1
    else
      ! Spacing is incorrect, insert 1 bad channel
      datav(inew) = datav(inew-1)+xinc
      data1(inew) = obs_bad(obs%head)
    endif
  enddo
  model_obs_fillgaps = iold.eq.obs%cnchan+1  ! Success
  !
  if (model_obs_fillgaps .and. obs%cnchan.ne.new_ndata) then
    ! Transfer the new arrays to the observation
    if (obs%assoc%n.gt.0) then
      call class_message(seve%e,rname,  &
        'Not implemented: filling gaps in Associated Arrays')
      error = .true.
      return
    endif
    call reallocate_obs(obs,new_ndata,error)
    if (error) then
      model_obs_fillgaps = .false.
      return
    endif
    obs%cnchan = new_ndata
    obs%datav(1:obs%cnchan) = datav(:)
    obs%data1(1:obs%cnchan) = data1(:)
  endif
  !
  deallocate(datav,data1)
  !
end function model_obs_fillgaps
!
subroutine model_Y_to_R(addr_y,r)
  use classcore_dependencies_interfaces
  use class_types
  use model_variables
  !---------------------------------------------------------------------
  ! @ private
  ! Copy values in Y variable into R buffer
  !---------------------------------------------------------------------
  integer(kind=address_length), intent(in)    :: addr_y  ! Y variable address
  type(observation),            intent(inout) :: r      !
  ! Global
  integer(4) :: memory(1)  ! Local use, can be local
  ! Local
  integer(kind=address_length) :: ipi
  !
  ipi = gag_pointer(addr_y,memory)
  if (spec) then
    call r4tor4(memory(ipi),r%data1,r%head%spe%nchan)
  else
    call r4tor4(memory(ipi),r%data1,r%head%dri%npoin)
  endif
  !
end subroutine model_Y_to_R
