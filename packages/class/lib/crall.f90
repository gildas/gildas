subroutine crsec(set,obs,scode,error)
  use gbl_message
  use classcore_interfaces, except_this=>crsec
  use class_common
  use class_parameter
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !   Read the appropriate section into the input observation from the
  ! associated entry in the input file, taking care of conversion of
  ! the section data from System data type to File data type.
  !  OBS%DESC: input
  !  OBS%HEAD: updated
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(observation),   intent(inout) :: obs    ! Header
  integer(kind=4),     intent(in)    :: scode  ! Section code
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='CRSEC'
  !
  if (error) return
  !
  if (filein_isvlm) then
    select case (scode)
    case (class_sec_gen_id)  ! General section
      call rgen_gdf(filein_vlmhead,obs,error)
    case (class_sec_pos_id)  ! Position section
      call rpos_gdf(filein_vlmhead,obs,error)
    case (class_sec_spe_id)  ! Spectroscopy section
      call rspec_gdf(filein_vlmhead,obs,error)
    case (class_sec_res_id)  ! Resolution section
      call rres_gdf(filein_vlmhead,obs,error)
    case default
      call class_message(seve%e,rname,  &
        'Reading this section is not implemented for VLM input file')
      error = .true.
      return
    end select
  else
    select case (scode)
    case (class_sec_gen_id)  ! General section
      call rgen_classic(obs,error)
    case (class_sec_pos_id)  ! Position section
      call rpos_classic(obs,error)
    case (class_sec_spe_id)  ! Spectroscopy section
      call rspec_classic(set,obs,error)
    case (class_sec_bas_id)  ! Base section
      call rbas_classic(obs,error)
    case (class_sec_plo_id)  ! Plotting section
      call rplo_classic(obs,error)
    case (class_sec_swi_id)  ! Switching section
      call rswi_classic(obs,error)
    case (class_sec_gau_id)  ! Gauss fit
      call rgau_classic(obs,error)
    case (class_sec_she_id)  ! Shell fit
      call rshe_classic(obs,error)
    case (class_sec_hfs_id)  ! HFS fit
      call rhfs_classic(obs,error)
    case (class_sec_abs_id)  ! Absorption fit
      call rabs_classic(obs,error)
    case (class_sec_dri_id)  ! Drift Continuum section
      call rdri_classic(obs,error)
    case (class_sec_bea_id)  ! Beam-switching section (for spectra or drifts)
      call rbea_classic(obs,error)
    case (class_sec_res_id)  ! Resolution section
      call rres_classic(obs,error)
    case (class_sec_user_id)  ! User section
      call ruser_classic(obs,error)
    case (class_sec_assoc_id)  ! Associated Arrays section
      call rassoc_classic(obs,error)
    case (class_sec_her_id)  ! Herchel section
      call rherschel_classic(obs,error)
    case default
      ! Same to be done for the other sections
      call crsec_classic(obs,scode,error)
    end select
  endif
  !
end subroutine crsec
!
subroutine rgen_classic(obs,error)
  use gildas_def
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>rgen_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !  Read the GENeral section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  integer(kind=data_length) :: slen
  integer(kind=4) :: iwork(class_sec_gen_len_max)
  !
  slen = class_sec_gen_len_max
  call rsec(obs%desc,class_sec_gen_id,slen,iwork,error)
  if (error)  return
  !
  ! obs%desc%version 1 or 2
  call filein%conv%read%r8(iwork(1),obs%head%gen%ut,2)  ! 1-4
  call filein%conv%read%r4(iwork(5),obs%head%gen%az,5)  ! 5-9
  if (slen.ge.class_sec_gen_len_v2_2) then
    call filein%conv%read%r8(iwork(10),obs%head%gen%parang,1)  ! 10-11
  else
    obs%head%gen%parang = parang_null
  endif
  !
end subroutine rgen_classic
!
subroutine rpos_classic(obs,error)
  use gildas_def
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>rpos_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !  Read the POSition section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='RPOS'
  integer(kind=data_length) :: slen
  integer(kind=4) :: iwork(class_sec_pos_len_max)  ! Largest possible
  character(len=message_length) :: mess
  !
  slen = class_sec_pos_len_max
  call rsec(obs%desc,class_sec_pos_id,slen,iwork,error)
  if (error)  return
  !
  if (obs%desc%version.ge.2) then
    call filein%conv%read%cc(iwork(1), obs%head%pos%sourc, 3)
    call filein%conv%read%r4(iwork(4), obs%head%pos%system,3)
    call filein%conv%read%r8(iwork(7), obs%head%pos%lam,   3)
    call filein%conv%read%r4(iwork(13),obs%head%pos%lamof, 2)
  else
    call filein%conv%read%cc(iwork(1), obs%head%pos%sourc,  3)
    ! V1: obs%head%pos%system read from the index
    call filein%conv%read%r4(iwork(4), obs%head%pos%equinox,1)
    call filein%conv%read%r8(iwork(5), obs%head%pos%lam,    2)
    obs%head%pos%projang = 0.d0
    call filein%conv%read%r4(iwork(9), obs%head%pos%lamof,  2)
    call filein%conv%read%i4(iwork(11),obs%head%pos%proj,   1)
  ! call filein%conv%read%r8(iwork(12),sl0p+sb0p+sk0p,      3)  Ignored
  endif
  !
  if (obs%head%pos%system.lt.type_un .or.  &
      obs%head%pos%system.gt.type_ic) then
    write(mess,'(A,I0,A)')  'Unrecognized type of coordinates (code ',  &
      obs%head%pos%system,') set to Unknown'
    call class_message(seve%w,rname,mess)
    obs%head%pos%system = type_un
  endif
  !
end subroutine rpos_classic
!
subroutine rspec_classic(set,obs,error)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>rspec_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !  Read the SPECtroscopic section
  !-------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(inout) :: obs
  logical,             intent(out)   :: error
  ! Local
  character(len=*), parameter :: rname='RSPEC'
  integer(kind=data_length) :: slen
  integer(kind=4) :: iwork(class_sec_spe_len_max)  ! Largest possible
  real(kind=4) :: r4value(5),vteles,foff
  real(kind=8) :: skyfr
  character(len=12) :: telname
  character(len=message_length) :: mess
  !
  slen = class_sec_spe_len_max
  call rsec(obs%desc,class_sec_spe_id,slen,iwork,error)
  if (error)  return
  !
  ! Needed for CSO patch and 30M velocity convention
  call my_get_teles(rname,obs%head%gen%teles,.false.,telname,error)
  if (error)  return
  !
  if (obs%desc%version.ge.3) then  ! Version 3
    call filein%conv%read%cc(iwork( 1), obs%head%spe%line,   3)  !  1-3
    call filein%conv%read%i4(iwork( 4), obs%head%spe%nchan,  1)  !  4
    call filein%conv%read%r8(iwork( 5), obs%head%spe%restf,  7)  !  5-18
    call filein%conv%read%r4(iwork(19), obs%head%spe%bad,    1)  ! 19
    call filein%conv%read%i4(iwork(20), obs%head%spe%vtype,  3)  ! 20-22
  else                             ! Version 1.1, 1.2, 1.3
    call filein%conv%read%cc(iwork(1), obs%head%spe%line,   3)  ! 1-3
    call filein%conv%read%r8(iwork(4), obs%head%spe%restf,  1)  ! 4-5
    call filein%conv%read%i4(iwork(6), obs%head%spe%nchan,  1)  ! 6
    call filein%conv%read%r4(iwork(7), r4value,             5)  ! 7-11
    ! R*4 on disk, R*8 in memory
    obs%head%spe%rchan = real(r4value(1),kind=8)
    obs%head%spe%fres  = real(r4value(2),kind=8)
    foff               =      r4value(3)
    obs%head%spe%vres  = real(r4value(4),kind=8)
    obs%head%spe%voff  = real(r4value(5),kind=8)
    call filein%conv%read%r4(iwork(12),obs%head%spe%bad,    1)  ! 12
    call filein%conv%read%r8(iwork(13),obs%head%spe%image,  1)  ! 13-14
    call filein%conv%read%i4(iwork(15),obs%head%spe%vtype,  1)  ! 15
    if (slen.eq.class_sec_spe_len_v1_3) then
      call filein%conv%read%r8(iwork(16),obs%head%spe%doppler,1)  ! 16-17
    elseif (slen.eq.class_sec_spe_len_v1_2) then
      call filein%conv%read%r8(iwork(16),skyfr,               1)  ! 16-17
      call filein%conv%read%r4(iwork(18),vteles,              1)  ! 18
      obs%head%spe%doppler = -1.d0
      if (telname.eq.'CSO')  call rspec_classic_CSOpatch(obs,skyfr)
    elseif (slen.eq.class_sec_spe_len_v1_1) then
      ! No more elements to read
      obs%head%spe%doppler = -1.d0
    else
      call class_message(seve%w,rname,'Unexpected Spectroscopic section length')
    endif
    ! Velocity convention was not available in V1
    if (telname.eq.'30M') then
      obs%head%spe%vconv = vconv_30m
    else
      obs%head%spe%vconv = vconv_unk
    endif
    ! Velocity direction was not available in V1
    obs%head%spe%vdire = vdire_unk
    !
    ! Foff was available only in V1
    if (foff.ne.0.)  call rspec_classic_foffpatch(obs%head%spe,foff)
  endif
  !
  if (obs%head%spe%doppler.eq.-1.d0) then
    ! Try to compute a Doppler factor
    call compute_doppler(set,obs%head,.true.,error)
    if (error) then
      call class_message(seve%w,rname,'Doppler effect undefined')
      error = .false.
      obs%head%spe%doppler = -1.d0
    else
      write(mess,'(A,F0.15)') 'Doppler factor (radio convention): ',obs%head%spe%doppler
      call class_message(seve%i,rname,mess)
    endif
  endif
  !
contains
  subroutine rspec_classic_CSOpatch(obs,skyfr)
    !-------------------------------------------------------------------
    ! CSO patch to support scans with the old format SPECTRO section
    ! taken at the CSO before February 4, 2009 UTC
    !-------------------------------------------------------------------
    type(observation), intent(inout) :: obs
    real(kind=8),      intent(in)    :: skyfr
    !
    ! Compute DOPPLER from SKYFR only for CSO because we know how
    ! they used SKYFR and VTELES. Take care if you want to enable
    ! this patch for another telescope, in particular PICO!
    obs%head%spe%doppler = skyfr / (obs%head%spe%restf+foff) - 1.d0
  ! obs%head%spe%doppler = - (obs%head%spe%voff + vteles) / clight * 1.d3
    write(mess,'(A,F0.15)') 'Doppler factor (radio convention): ',obs%head%spe%doppler
    call class_message(seve%i,rname,mess)
  end subroutine rspec_classic_CSOpatch
  !
end subroutine rspec_classic
!
subroutine rspec_classic_foffpatch(spe,foff)
  use gbl_message
  use classcore_interfaces, except_this=>rspec_classic_foffpatch
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Apply patch for suppressing a frequency offset on the spectroscopic
  ! axis
  !---------------------------------------------------------------------
  type(class_spectro_t), intent(inout) :: spe   ! The spectroscopic section to update
  real(kind=4),          intent(in)    :: foff  ! The frequency offset to apply
  !
  call class_message(seve%w,'RSPEC',  &
    'Shifting rest and image frequencies for non-zero frequency offset')
  spe%restf = spe%restf+foff
  spe%image = spe%image-foff
  !
end subroutine rspec_classic_foffpatch
!
subroutine rbas_classic(obs,error)
  use gildas_def
  use gbl_constant
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>rbas_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !  Read the BASe section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='RBAS'
  integer(kind=data_length) :: slen
  integer(kind=4) :: iwork(class_sec_bas_len)  ! Largest possible
  !
  slen = class_sec_bas_len
  call rsec(obs%desc,class_sec_bas_id,slen,iwork,error)
  if (error)  return
  !
  call filein%conv%read%i4(iwork(1),obs%head%bas%deg,  1)
  call filein%conv%read%r4(iwork(2),obs%head%bas%sigfi,2)
  call filein%conv%read%i4(iwork(4),obs%head%bas%nwind,1)
  ! Valid Nwind values can be:
  !  -1 = basnwind_assoc for LINE associated array,
  !   0 for no windows
  !  >0 for 1 or more windows
  if (obs%head%bas%nwind.gt.0) then
    call filein%conv%read%r4(iwork(5),                   obs%head%bas%w1,obs%head%bas%nwind)
    call filein%conv%read%r4(iwork(5+obs%head%bas%nwind),obs%head%bas%w2,obs%head%bas%nwind)
  endif
  !
end subroutine rbas_classic
!
subroutine rplo_classic(obs,error)
  use gildas_def
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>rplo_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !  Read the Plotting section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  integer(kind=data_length) :: slen
  integer(kind=4) :: iwork(class_sec_plo_len)
  !
  slen = class_sec_plo_len
  call rsec(obs%desc,class_sec_plo_id,slen,iwork,error)
  if (error)  return
  !
  call filein%conv%read%r4(iwork,obs%head%plo%amin,class_sec_plo_len)
  !
end subroutine rplo_classic
!
subroutine rswi_classic(obs,error)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>rswi_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !  Read the Switching section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='RSWI'
  integer(kind=data_length) :: slen
  integer(kind=4) :: i,iwork(class_sec_swi_len)
  real(kind=8)    :: xx(2)
  integer(kind=4) :: jx(4)
  equivalence (xx,jx)
  !
  slen = class_sec_swi_len
  call rsec(obs%desc,class_sec_swi_id,slen,iwork,error)
  if (error)  return
  !
  call filein%conv%read%i4(iwork(1),obs%head%swi%nphas,1)
  do i=1,obs%head%swi%nphas
    jx(1) = iwork(2*i)
    jx(2) = iwork(2*i+1)
    call filein%conv%read%r8(xx,obs%head%swi%decal(i),1)
  enddo
  call filein%conv%read%r4(iwork(2+2*obs%head%swi%nphas),obs%head%swi%duree,obs%head%swi%nphas)
  call filein%conv%read%r4(iwork(2+3*obs%head%swi%nphas),obs%head%swi%poids,obs%head%swi%nphas)
  call filein%conv%read%i4(iwork(2+4*obs%head%swi%nphas),obs%head%swi%swmod,1)
  call filein%conv%read%r4(iwork(3+4*obs%head%swi%nphas),obs%head%swi%ldecal,obs%head%swi%nphas)
  call filein%conv%read%r4(iwork(3+5*obs%head%swi%nphas),obs%head%swi%bdecal,obs%head%swi%nphas)
  !
end subroutine rswi_classic
!
subroutine rgau_classic(obs,error)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>rgau_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !  Read the Gauss fit section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='RGAU'
  integer(kind=4) :: dline,dw,mline,mw
  integer(kind=data_length) :: dlen
  integer(kind=4), allocatable :: iwork(:)
  character(len=message_length) :: mess
  !
  dlen = classic_entrydesc_seclen(obs%desc,class_sec_gau_id)
  allocate(iwork(dlen))
  call rsec(obs%desc,class_sec_gau_id,dlen,iwork,error)
  if (error)  return
  !
  ! Guess the provision for the number of lines in section
  dline = (dlen-3)/2/ngauspar  ! Max number of lines on disk
  dw = ngauspar * dline        ! Max number of words on disk
  mline = min(dline,mgauslin)  ! Max number of lines in memory
  mw = ngauspar * mline        ! Max number of words in memory
  !
  call filein%conv%read%i4(iwork(1),   obs%head%gau%nline,1)
  call filein%conv%read%r4(iwork(2),   obs%head%gau%sigba,2)
  call filein%conv%read%r4(iwork(4),   obs%head%gau%nfit,mw)
  call filein%conv%read%r4(iwork(4+dw),obs%head%gau%nerr,mw)
  !
  if (obs%head%gau%nline.gt.mgauslin) then
    write(mess,'(A,I0,A,I0)')  'Number of lines truncated from ',  &
      obs%head%gau%nline,' to ',mgauslin
    call class_message(seve%w,rname,mess)
    obs%head%gau%nline = mgauslin
  elseif (obs%head%gau%nline.lt.mgauslin) then
    ! Fill unused parameters with blanks
    obs%head%gau%nfit(mw+1:) = 0.
    obs%head%gau%nerr(mw+1:) = 0.
  endif
  !
end subroutine rgau_classic
!
subroutine rshe_classic(obs,error)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>rshe_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !  Read the Shell fit section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='RSHE'
  integer(kind=4) :: dline,dw,mline,mw
  integer(kind=data_length) :: dlen
  integer(kind=4), allocatable :: iwork(:)
  character(len=message_length) :: mess
  !
  dlen = classic_entrydesc_seclen(obs%desc,class_sec_she_id)
  allocate(iwork(dlen))
  call rsec(obs%desc,class_sec_she_id,dlen,iwork,error)
  if (error)  return
  !
  ! Guess the provision for the number of lines in section
  dline = (dlen-3)/2/nshellpar  ! Max number of lines on disk
  dw = nshellpar * dline        ! Max number of words on disk
  mline = min(dline,mshelllin)  ! Max number of lines in memory
  mw = nshellpar * mline        ! Max number of words in memory
  !
  call filein%conv%read%i4(iwork(1),   obs%head%she%nline,1)
  call filein%conv%read%r4(iwork(2),   obs%head%she%sigba,2)
  call filein%conv%read%r4(iwork(4),   obs%head%she%nfit,mw)
  call filein%conv%read%r4(iwork(4+dw),obs%head%she%nerr,mw)
  !
  if (obs%head%she%nline.gt.mshelllin) then
    write(mess,'(A,I0,A,I0)')  'Number of lines truncated from ',  &
      obs%head%she%nline,' to ',mshelllin
    call class_message(seve%w,rname,mess)
    obs%head%she%nline = mshelllin
  elseif (obs%head%she%nline.lt.mshelllin) then
    ! Fill unused parameters with blanks
    obs%head%she%nfit(mw+1:) = 0.
    obs%head%she%nerr(mw+1:) = 0.
  endif
  !
end subroutine rshe_classic
!
subroutine rhfs_classic(obs,error)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>rhfs_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !  Read the HFS fit section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='RHFS'
  integer(kind=4) :: dline,dw,mline,mw
  integer(kind=data_length) :: dlen
  integer(kind=4), allocatable :: iwork(:)
  character(len=message_length) :: mess
  !
  dlen = classic_entrydesc_seclen(obs%desc,class_sec_hfs_id)
  allocate(iwork(dlen))
  call rsec(obs%desc,class_sec_hfs_id,dlen,iwork,error)
  if (error)  return
  !
  ! Guess the provision for the number of lines in section
  dline = (dlen-3)/2/nhfspar  ! Max number of lines on disk
  dw = nhfspar * dline        ! Max number of words on disk
  mline = min(dline,mhfslin)  ! Max number of lines in memory
  mw = nhfspar * mline        ! Max number of words in memory
  !
  call filein%conv%read%i4(iwork(1),   obs%head%hfs%nline,1)
  call filein%conv%read%r4(iwork(2),   obs%head%hfs%sigba,2)
  call filein%conv%read%r4(iwork(4),   obs%head%hfs%nfit,mw)
  call filein%conv%read%r4(iwork(4+dw),obs%head%hfs%nerr,mw)
  !
  if (obs%head%hfs%nline.gt.mhfslin) then
    write(mess,'(A,I0,A,I0)')  'Number of lines truncated from ',  &
      obs%head%hfs%nline,' to ',mhfslin
    call class_message(seve%w,rname,mess)
    obs%head%hfs%nline = mhfslin
  elseif (obs%head%hfs%nline.lt.mhfslin) then
    ! Fill unused parameters with blanks
    obs%head%hfs%nfit(mw+1:) = 0.
    obs%head%hfs%nerr(mw+1:) = 0.
  endif
  !
end subroutine rhfs_classic
!
subroutine rabs_classic(obs,error)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>rabs_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !  Read the Absorption fit section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='RABS'
  integer(kind=4) :: dline,dw,mline,mw
  integer(kind=data_length) :: dlen
  integer(kind=4), allocatable :: iwork(:)
  character(len=message_length) :: mess
  !
  dlen = classic_entrydesc_seclen(obs%desc,class_sec_abs_id)
  allocate(iwork(dlen))
  call rsec(obs%desc,class_sec_abs_id,dlen,iwork,error)
  if (error)  return
  !
  ! Guess the provision for the number of lines in section
  dline = ((dlen-3)/2-1)/nabspar  ! Max number of lines on disk
  dw = 1 + nabspar * dline        ! Max number of words on disk
  mline = min(dline,mabslin)      ! Max number of lines in memory
  mw = 1 + nabspar * mline        ! Max number of words in memory
  !
  call filein%conv%read%i4(iwork(1),   obs%head%abs%nline,1)
  call filein%conv%read%r4(iwork(2),   obs%head%abs%sigba,2)
  call filein%conv%read%r4(iwork(4),   obs%head%abs%nfit,mw)
  call filein%conv%read%r4(iwork(4+dw),obs%head%abs%nerr,mw)
  !
  if (obs%head%abs%nline.gt.mabslin) then
    write(mess,'(A,I0,A,I0)')  'Number of lines truncated from ',  &
      obs%head%abs%nline,' to ',mabslin
    call class_message(seve%w,rname,mess)
    obs%head%abs%nline = mabslin
  elseif (obs%head%abs%nline.lt.mabslin) then
    ! Fill unused parameters with blanks
    obs%head%abs%nfit(mw+1:) = 0.
    obs%head%abs%nerr(mw+1:) = 0.
  endif
  !
end subroutine rabs_classic
!
subroutine rdri_classic(obs,error)
  use gildas_def
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>rdri_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !  Read the Drift Continuum section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='RDRI'
  integer(kind=data_length) :: slen
  integer(kind=4) :: iwork(class_sec_dri_len)
  !
  slen = class_sec_dri_len
  call rsec(obs%desc,class_sec_dri_id,slen,iwork,error)
  if (error)  return
  !
  call filein%conv%read%r8(iwork(1),obs%head%dri%freq,1)
  call filein%conv%read%r4(iwork(3),obs%head%dri%width,1)
  call filein%conv%read%i4(iwork(4),obs%head%dri%npoin,1)
  call filein%conv%read%r4(iwork(5),obs%head%dri%rpoin,7)
  call filein%conv%read%i4(iwork(12),obs%head%dri%ctype,1)
  call filein%conv%read%r8(iwork(13),obs%head%dri%cimag,1)
  call filein%conv%read%r4(iwork(15),obs%head%dri%colla,2)
  !
end subroutine rdri_classic
!
subroutine rbea_classic(obs,error)
  use gildas_def
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>rbea_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !  Read the BEAm-switching section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  integer(kind=data_length) :: slen
  integer(kind=4) :: iwork(class_sec_bea_len)
  !
  slen = class_sec_bea_len
  call rsec(obs%desc,class_sec_bea_id,slen,iwork,error)
  if (error)  return
  !
  call filein%conv%read%r4(iwork(1),obs%head%bea%cazim,4)
  call filein%conv%read%i4(iwork(5),obs%head%bea%btype,1)
  !
end subroutine rbea_classic
!
subroutine rres_classic(obs,error)
  use gildas_def
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>rres_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !  Read the RESolution section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  integer(kind=data_length) :: slen
  integer(kind=4) :: iwork(class_sec_res_len)
  !
  slen = class_sec_res_len
  call rsec(obs%desc,class_sec_res_id,slen,iwork,error)
  if (error)  return
  !
  call filein%conv%read%r4(iwork(1),obs%head%res%major,3)  !  1- 3
  !
end subroutine rres_classic
!
subroutine ruser_classic(obs,error)
  use gildas_def
  use gbl_message
  use classic_api
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>ruser_classic
  use class_buffer
  use class_common
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !   Convert User Section from File data type to System data type.
  ! Read the User Section from buffer 'uwork'.
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs    ! Observation
  logical,           intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='RUSER'
  integer(kind=data_length) :: len  ! Length of section
  integer(kind=4) :: iuser,nuser,usec
  logical :: found
  !
  if (filein%conv%code.ne.0) then
    call class_message(seve%w,rname,  &
      'Input file is not in native format: skipping User Section')
    obs%user%n = 0
    return
  endif
  !
  ! Search for a user section
  call classic_entrydesc_secfind_one(obs%desc,class_sec_user_id,found,usec)
  if (.not.found) then
    call class_message(seve%e,rname,'Section not present')
    error = .true.
    return
  endif
  !
  len = obs%desc%secleng(usec)
  call reallocate_uwork(len,.false.,error)
  if (error)  return
  call rsec(obs%desc,class_sec_user_id,len,uwork,error)  ! Fill the buffer 'uwork'
  if (error)  return
  !
  call filein%conv%read%i4(uwork(1),nuser,1)
  unext = 2
  !
  ! First reallocate (if needed). 'keep' is set to true because we
  ! want the pointer targets to be preserved
  call reallocate_user(obs%user,nuser,.true.,error)
  if (error)  return
  !
  ! Then update user%n (we must be consistant in reallocate_user)
  obs%user%n = nuser
  !
  do iuser=1,obs%user%n
    call filein%conv%read%cc(uwork(unext),obs%user%sub(iuser)%owner,3)
    unext = unext+3
    call filein%conv%read%cc(uwork(unext),obs%user%sub(iuser)%title,3)
    unext = unext+3
    call filein%conv%read%i4(uwork(unext),obs%user%sub(iuser)%version,1)
    unext = unext+1
    call filein%conv%read%i4(uwork(unext),obs%user%sub(iuser)%ndata,1)
    unext = unext+1
    ! Then, the data
    call reallocate_user_sub(obs%user%sub(iuser),error)
    if (error)  return
    call bytoby(uwork(unext),  &
                obs%user%sub(iuser)%data,  &
                obs%user%sub(iuser)%ndata*4)
    unext = unext+obs%user%sub(iuser)%ndata
  enddo
  !
  if (len.ne.(unext-1))  &
    call class_message(seve%w,rname,'Unexpected User Section size')
  !
end subroutine ruser_classic
!
subroutine rassoc_classic(obs,error)
  use gbl_format
  use gbl_message
  use classic_api
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>rassoc_classic
  use class_common
  use class_types
  !----------------------------------------------------------------------
  ! @ private
  ! Read the ASSOCiated array section
  !----------------------------------------------------------------------
  type(observation), intent(inout) :: obs    !
  logical,           intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='RASSOC'
  logical :: found
  integer(kind=data_length) :: slen,inext
  integer(kind=4), allocatable :: iwork(:)
  integer(kind=4) :: ier,iarray,narray,usec,ndata,idata,nchan,nbytes,ibyte
  integer(kind=4) :: pos,nwords,dim2,idime
  integer(kind=1), allocatable :: by(:)
  !
  ! We can't do anything if Nchan is not available
  nchan = obs_nchan(obs%head)
  if (nchan.le.0) then
    call class_message(seve%e,rname,'Observation has invalid Nchan')
    error = .true.
    return
  endif
  !
  ! Search for the assoc section
  call classic_entrydesc_secfind_one(obs%desc,class_sec_assoc_id,found,usec)
  if (.not.found) then
    call class_message(seve%e,rname,'Section not present')
    error = .true.
    return
  endif
  !
  ! Buffer
  slen = obs%desc%secleng(usec)
  allocate(iwork(slen),stat=ier)
  if (failed_allocate(rname,'iwork',ier,error))  return
  !
  ! Read bytes
  call rsec(obs%desc,class_sec_assoc_id,slen,iwork,error)
  if (error)  return
  !
  call filein%conv%read%i4(iwork(1),narray,1)
  !
  ! First reallocate (if needed)
  call reallocate_assoc(obs%assoc,narray,.false.,error)
  if (error)  return
  !
  inext = 2
  do iarray=1,obs%assoc%n
    call filein%conv%read%cc(iwork(inext),obs%assoc%array(iarray)%name,3)
    inext = inext+3
    call filein%conv%read%cc(iwork(inext),obs%assoc%array(iarray)%unit,3)
    inext = inext+3
    call filein%conv%read%i4(iwork(inext),obs%assoc%array(iarray)%dim2,1)
    inext = inext+1
    call filein%conv%read%i4(iwork(inext),obs%assoc%array(iarray)%fmt, 1)
    inext = inext+1
    ! Not in section on disk
    obs%assoc%array(iarray)%dim1 = nchan  ! Not in data
    ! Then, the data
    call reallocate_assoc_sub(obs%assoc%array(iarray),error)
    if (error)  return
    dim2 = max(1,obs%assoc%array(iarray)%dim2)
    ndata = nchan*dim2
    select case (obs%assoc%array(iarray)%fmt)
    case (fmt_r4)
      call filein%conv%read%r4(iwork(inext),obs%assoc%array(iarray)%badr4,1)
      inext = inext+1
      call filein%conv%read%r4(iwork(inext),obs%assoc%array(iarray)%r4,ndata)
      inext = inext+ndata
      !
    case (fmt_i4)
      call filein%conv%read%i4(iwork(inext),obs%assoc%array(iarray)%badi4,1)
      inext = inext+1
      call filein%conv%read%i4(iwork(inext),obs%assoc%array(iarray)%i4,ndata)
      inext = inext+ndata
      !
    case (fmt_by)  ! 1 byte = 1 value per byte
      nbytes = 1 + ndata  ! Bad + data values
      allocate(by(1+ndata))
      call bytoby(iwork(inext),by,1+ndata)
      obs%assoc%array(iarray)%badi4 = by(1)
      do idime=1,dim2
        obs%assoc%array(iarray)%i4(:,idime) =  &
          by(2+nchan*(idime-1):1+nchan*idime)  ! Single byte on disk to I*4 in memory
      enddo
      deallocate(by)
      nwords = (nbytes+3)/4
      inext = inext+nwords
      !
    case (fmt_b2)  ! 2 bits per value = 4 values per byte
      nbytes = (1+ndata+3)/4  ! Bad + data values
      allocate(by(nbytes))  ! Allocate bytes buffer
      call bytoby(iwork(inext),by,nbytes)
      ! Decode the bytes buffer
      ibyte = 1
      pos = 1
      call twobit_to_i4(by(ibyte),pos,obs%assoc%array(iarray)%badi4)
      do idime=1,dim2
        do idata=1,ndata
          if (pos.eq.4) then
            ibyte = ibyte+1
            pos = 1
          else
            pos = pos+1
          endif
          call twobit_to_i4(by(ibyte),pos,obs%assoc%array(iarray)%i4(idata,idime))
        enddo
      enddo
      deallocate(by)
      nwords = (nbytes+3)/4
      inext = inext+nwords
      !
    case default
      call class_message(seve%e,rname,'Kind of data not yet implemented')
      error = .true.
      return
    end select
    !
  enddo
  !
  if (slen.ne.(inext-1))  &
    call class_message(seve%w,rname,'Unexpected Associated Array section size')
  !
  deallocate(iwork)
  !
contains
  !
  subroutine twobit_to_i4(by,pos,i4)
    integer(kind=1), intent(in)  :: by   ! 8-bits buffer
    integer(kind=4), intent(in)  :: pos  ! The 2-bits position in the 8-bits buffer (1 ... 4)
    integer(kind=4), intent(out) :: i4   ! Resulting I*4 value
    ! Local
    integer(kind=4), parameter :: bi4val=0  ! Position of value bit (we use only one)
    integer(kind=4) :: bb2sign,bb2val
    !
    ! See comments in symetric subroutine 'i4_to_twobit'
    !
    bb2val = 2*(pos-1)
    bb2sign = bb2val+1
    !
    if (btest(by,bb2sign)) then
      i4 = -1  ! Negative value, fill all bits with ones
    else
      i4 = 0   ! Positive value, fill all bits with zeroes
    endif
    !
    if (btest(by,bb2val)) then
      i4 = ibset(i4,bi4val)
    else
      i4 = ibclr(i4,bi4val)
    endif
  end subroutine twobit_to_i4
  !
end subroutine rassoc_classic
!
subroutine rherschel_classic(obs,error)
  use gildas_def
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>rherschel_classic
  use class_common
  use class_types
  !-------------------------------------------------------------------
  ! @ private
  !  Read the HERSCHEL section
  !-------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='RHERSCHEL'
  integer(kind=data_length) :: slen
  integer(kind=4) :: iwork(class_sec_her_len)
  !
  slen = class_sec_her_len
  call rsec(obs%desc,class_sec_her_id,slen,iwork,error)
  if (error)  return
  !
  call filein%conv%read%i8(iwork( 1),obs%head%her%obsid,      1)  !  1- 2
  call filein%conv%read%cc(iwork( 3),obs%head%her%instrument, 2)  !  3- 4
  call filein%conv%read%cc(iwork( 5),obs%head%her%proposal,   6)  !  5-10
  call filein%conv%read%cc(iwork(11),obs%head%her%aor,       17)  ! 11-27
  call filein%conv%read%i4(iwork(28),obs%head%her%operday,    1)  ! 28-28
  call filein%conv%read%cc(iwork(29),obs%head%her%dateobs,    7)  ! 29-35
  call filein%conv%read%cc(iwork(36),obs%head%her%dateend,    7)  ! 36-42
  call filein%conv%read%cc(iwork(43),obs%head%her%obsmode,   10)  ! 43-52
  call filein%conv%read%r4(iwork(53),obs%head%her%vinfo,      2)  ! 53-54
  call filein%conv%read%r8(iwork(55),obs%head%her%posangle,   5)  ! 55-64
  call filein%conv%read%r4(iwork(65),obs%head%her%etamb,      4)  ! 65-68
  call filein%conv%read%cc(iwork(69),obs%head%her%tempscal,   2)  ! 69-70
  call filein%conv%read%r8(iwork(71),obs%head%her%lodopave,   1)  ! 71-72
  call filein%conv%read%r4(iwork(73),obs%head%her%gim0,       6)  ! 73-78
  call filein%conv%read%cc(iwork(79),obs%head%her%datehcss,   7)  ! 79-85
  call filein%conv%read%cc(iwork(86),obs%head%her%hcssver,    6)  ! 86-91
  call filein%conv%read%cc(iwork(92),obs%head%her%calver,     4)  ! 92-95
  call filein%conv%read%r4(iwork(96),obs%head%her%level,      1)  ! 96-96
  !
end subroutine rherschel_classic
!
subroutine crsec_classic(obs,scode,error)
  use gildas_def
  use gbl_constant
  use gbl_message
  use classic_api
  use classcore_interfaces, except_this=>crsec_classic
  use class_common
  use class_types
  use class_buffer
  !---------------------------------------------------------------------
  ! @ private
  !   Read the appropriate section into the input observation from the
  ! associated entry in the input file, taking care of conversion of
  ! the section data from System data type to File data type.
  !  OBS%DESC: input
  !  OBS%HEAD: updated
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs    ! Header
  integer(kind=4),   intent(in)    :: scode  ! Section code
  logical,           intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='CRSEC'
  integer(kind=data_length) :: len  ! Length of section
  integer(kind=4) :: len4
  real(kind=8)    :: xx(2)
  integer(kind=4) :: jx(4)
  equivalence (xx,jx)
  !
  select case(scode)
!  case (class_sec_gen_id)
    ! Moved to rgen_classic. Same to be done for the other sections
    !
! case (class_sec_pos_id)
    ! Moved to rpos_classic. Same to be done for the other sections
    !
! case (class_sec_spe_id)
    ! Moved to rspec_classic. Same to be done for the other sections
    !
  case (class_sec_com_id)  ! Comment section
    len = class_sec_comm_len
    call rsec(obs%desc,scode,len,iwork,error)
    if (error)  return
    len4 = len
    call filein%conv%read%cc (iwork(1),obs%head%com%ctext,len4)
    obs%head%com%ltext=len*4
    !
!  case (class_sec_bas_id)  ! Baseline section
    ! Moved to rbas_classic. Same to be done for the other sections
    !
  case (class_sec_his_id)
    len = class_sec_his_len
    call rsec(obs%desc,scode,len,iwork,error)
    if (error)  return
    ! All integers
    call filein%conv%read%i4(iwork(1),obs%head%his%nseq,1)
    if (obs%head%his%nseq.eq.0) return
    call filein%conv%read%i4(iwork(2),obs%head%his%start,obs%head%his%nseq)
    call filein%conv%read%i4(iwork(2+obs%head%his%nseq),obs%head%his%end,obs%head%his%nseq)
    !
! case (class_sec_plo_id)
    ! Moved to rplo_classic. Same to be done for the other sections
    !
! case (class_sec_swi_id)
    ! Moved to rswi_classic. Same to be done for the other sections
    !
! case (class_sec_gau_id)
    ! Moved to rgau_classic. Same to be done for the other sections
    !
! case (class_sec_dri_id)
    ! Moved to rdri_classic. Same to be done for the other sections
    !
! case (class_sec_bea_id)
    ! Moved to rbea_classic. Same to be done for the other sections
    !
! case (class_sec_she_id)
    ! Moved to rshe_classic. Same to be done for the other sections
    !
! case (class_sec_hfs_id)
    ! Moved to rhfs_classic. Same to be done for the other sections
    !
! case (class_sec_abs_id)
    ! Moved to rabs_classic. Same to be done for the other sections
    !
  case (class_sec_cal_id)
    len = class_sec_cal_len
    call rsec(obs%desc,scode,len,iwork,error)
    if (error)  return
    call filein%conv%read%r4 (iwork,obs%head%cal%beeff,13)
    call filein%conv%read%i4 (iwork(14),obs%head%cal%cmode,1)
    call filein%conv%read%r4 (iwork(15),obs%head%cal%atfac,class_sec_cal_len-14)
    !
  case (class_sec_poi_id)
    len = class_sec_poi_len
    call rsec(obs%desc,scode,len,iwork,error)
    if (error)  return
    call filein%conv%read%i4 (iwork(1),obs%head%poi%nline,1)
    call filein%conv%read%r4 (iwork(2),obs%head%poi%sigba,class_sec_poi_len-1)
    !
  case (class_sec_sky_id)
    len = class_sec_sky_len
    call rsec(obs%desc,scode,len,iwork,error)
    if (error)  return
    call filein%conv%read%cc(iwork(1),obs%head%sky%line,3)
    jx(1) = iwork(4)
    jx(2) = iwork(5)
    jx(3) = iwork(6)
    jx(4) = iwork(7)
    call filein%conv%read%r8(xx,obs%head%sky%restf,2)
    call filein%conv%read%i4(iwork(8),obs%head%sky%nsky,3)
    if (obs%head%sky%nsky.gt.0) then
      call filein%conv%read%r4(iwork(11),obs%head%sky%elev,obs%head%sky%nsky)
      call filein%conv%read%r4(iwork(11+obs%head%sky%nsky),obs%head%sky%emiss,obs%head%sky%nsky)
    endif
    if (obs%head%sky%nchop.gt.0)  &
      call filein%conv%read%r4(iwork(11+2*obs%head%sky%nsky),  &
                               obs%head%sky%chopp,obs%head%sky%nchop)
    if (obs%head%sky%ncold.gt.0) &
      call filein%conv%read%r4(iwork(11+2*obs%head%sky%nsky+obs%head%sky%nchop),  &
                               obs%head%sky%cold,obs%head%sky%ncold)
    !
  case (class_sec_desc_id)
    len = class_sec_desc_len
    call rsec(obs%desc,scode,len,iwork,error)
    if (error)  return
    len4 = len
    call filein%conv%read%i4 (iwork,obs%head%des%ndump,len4)
    !
  case default
    call class_message(seve%e,rname,'Unknown section')
    error = .true.
    return
  end select
  !
end subroutine crsec_classic
!
subroutine crsec_xcoo(set,obs,error)
  use gildas_def
  use gbl_constant
  use gbl_message
  use classic_api
  use classcore_dependencies_interfaces
  use class_common
  use class_types
  use class_buffer
  !---------------------------------------------------------------------
  ! @ private
  ! Convert X_coord section from File data type to System data type
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(observation),   intent(inout) :: obs    ! Observation
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='CRSEC_XCOO'
  real(kind=4), allocatable :: aaa4(:)
  real(kind=8), allocatable :: aaa8(:)
  integer(kind=data_length) :: len  ! Length of section
  integer(kind=4) :: len1,jsec,ier
  logical :: found
  character(len=message_length) :: mess
  ! In case of alignment problems..
!  integer :: i,k
!  real(kind=8)    :: xx(1)
!  integer(kind=4) :: jx(2)
!  equivalence (xx,jx)
  !
  if (error) return
  !
  call classic_entrydesc_secfind_one(obs%desc,class_sec_xcoo_id,found,jsec)
  if (.not.found) then
    call class_message(seve%e,rname,'Section not present')
    error = .true.
    return
  endif
  !
  len = obs%desc%secleng(jsec)
  if (len.gt.jlen) then
    if (jlen.ne.0) deallocate(jwork,stat=ier)
    allocate (jwork(len),stat=ier)
    jlen = len
  endif
  call rsec(obs%desc,class_sec_xcoo_id,len,jwork,error)
  if (error)  return
  !
  ! First read X unit
  call filein%conv%read%i4 (jwork,obs%head%gen%xunit,1)
  !
  ! Then read X values
  if (obs%head%gen%kind.eq.kind_spec) then
     len1 = obs%head%spe%nchan
  elseif (obs%head%gen%kind.eq.kind_cont) then
     len1 = obs%head%dri%npoin
  endif
  !
  ! Store the X values in DATAV. DATAX will be filled later on (by
  ! the subroutine 'abscissa') with either the channel number (SET
  ! UNIT C) or DATAV (SET UNIT V)
  if (len-1.eq.len1) then
    if (set%verbose)  call class_message(seve%i,rname,'Reading REAL*4 data')
    if (xdata_kind.eq.4) then
      call filein%conv%read%r4 (jwork(2),obs%datav,len1)
    else
      allocate(aaa4(len1))
      call filein%conv%read%r4 (jwork(2),aaa4,len1)
      obs%datav(1:len1) = aaa4(:)  ! Upcast to R*8
      deallocate(aaa4)
    endif
    !
  elseif (2*(len-1).eq.len1) then
    if (set%verbose)  &
      call class_message(seve%i,rname,'Reading REAL*8 data')
! In case of alignment problems..
!    k = 2
!    do i=1,len-1
!       jx = jwork(k:k+1)
!       call filein%conv%read%r8 (jx,obs%datav(i),1)
!       k = k+2
!    enddo
! If no alignment problem..
    if (xdata_kind.eq.4) then
      allocate(aaa8(len1))
      call filein%conv%read%r8 (jwork(2),aaa8,len1)
      obs%datav(1:len1) = aaa8(:)  ! Downcast to R*4
      deallocate(aaa8)
    else
      call filein%conv%read%r8 (jwork(2),obs%datav,len1)
    endif
    !
  else
    write(mess,'(A,I0,A,I0,A)')  &
      'Unexpected X coordinate section length: ',  &
      len-1,' 4-byte words for ',len1,' data points'
    call class_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
end subroutine crsec_xcoo
!
#if defined(OBSOLETE)
subroutine compute_doppler_compare(obs,telescope,error)
  use gildas_def
  use class_types
  use gbl_message
  use gbl_constant
  use phys_const
  !---------------------------------------------------------------------
  ! @ private
  ! Debugging routine used to compare the SLOW and FAST
  ! version of COMPUTE_DOPPLER
  !---------------------------------------------------------------------
  type(header),     intent(inout) :: obs        ! Current header
  character(len=*), intent(in)    :: telescope  ! Observatory
  logical,          intent(out)   :: error      ! flag
  !
  call compute_doppler      (set,obs,.true.,error)
  print *,obs%spe%doppler
  call compute_doppler_slow (obs,telescope,error)
  print *,obs%spe%doppler
end subroutine compute_doppler_compare
!
subroutine compute_doppler_slow(obs,telescope,error)
  use gildas_def
  use phys_const
  use gbl_constant
  use gbl_message
  use gkernel_interfaces
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>compute_doppler_slow
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  ! Compute the Doppler correction, calling ASTRO routine, according to:
  ! * observatory location
  ! * reference frame (LSR, Helio...)
  ! Radio convention is used
  !---------------------------------------------------------------------
  type(header),     intent(inout) :: obs        ! Current header
  character(len=*), intent(in)    :: telescope  ! Observatory
  logical,          intent(out)   :: error      ! flag
  ! Local
  character(len=*), parameter :: rname='COMPUTE_DOPPLER'
  character(len=2) :: coord
  character(len=4) :: line
  character(len=80) :: comm
  character(len=16) :: ch1
  integer :: nc
  real(4) :: equinox
  real(8) :: lambda, beta, vshift
  real(8) :: s_2(2), s_3(3), dop, lsr, svec(3), x_0(3), parang
  !
  dop = 0d0
  error = .true.
  if (telescope.eq.'PICO') then
     call astro_observatory('PICO',error)
  else
     line = telescope
     call astro_observatory(line,error)
     if (error) then
        call class_message(seve%e,rname,'No such observatory '//line)
        return
     endif
  endif
  !
  ! Reset the time
  !
  call gag_todate(obs%gen%dobs,ch1,error)
  write(comm,'(A,1pg17.10,A)') 'ASTRO'//CHAR(92)//'TIME ',obs%gen%ut*12d0/pi,ch1
  nc = len_trim(comm)
  call sic_blanc(comm,nc)
  call sic_analyse(line,comm,nc,error)
  call astro_time(comm,error)
  !
  if (obs%pos%system.eq.type_eq) then
     coord = 'EQ'
  elseif (obs%pos%system.eq.type_ga) then
     coord = 'GA'
  elseif (obs%pos%system.eq.type_ic) then
     coord = 'IC'
  else
     return
  endif
  error = .false.
  !
  equinox = obs%pos%equinox
  lambda = obs%pos%lam
  beta   = obs%pos%bet
  line = ' '
  !
  ! Call ASTRO
  call do_object(coord,equinox,lambda,beta,s_2,s_3,dop,lsr,svec,x_0,parang,error)
  if (error) return
  !
  ! Now retrieve the Doppler value
  !
  if (obs%spe%vtype.eq.vel_lsr) then
     vshift = dop+lsr+obs%spe%voff
  elseif  (obs%spe%vtype.eq.vel_hel) then
     vshift = dop+obs%spe%voff
  elseif  (obs%spe%vtype.eq.vel_ear) then
     vshift = obs%spe%voff
  else
     vshift = 0.0d0
  endif
  obs%spe%doppler = - vshift / clight * 1d3
end subroutine compute_doppler_slow
#endif
!
subroutine compute_doppler(set,obs,use_ref,error)
  use gildas_def
  use phys_const
  use gbl_constant
  use gbl_message
  use gkernel_types
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>compute_doppler
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  ! Compute the Doppler correction, calling ASTRO routine, according to:
  ! * observatory location
  ! * reference frame (LSR, Helio...)
  ! CLASS convention is used (Doppler factor = -V / c)
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set      !
  type(header),        intent(inout) :: obs      ! Current header
  logical,             intent(in)    :: use_ref  ! Compute at reference position?
  logical,             intent(inout) :: error    ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='COMPUTE_DOPPLER'
  character(len=2) :: coord
  real(kind=4) :: equinox
  real(kind=8) :: lambda, beta, vshift, rxoff, ryoff
  real(kind=8) :: s_2(2), s_3(3), dop, lsr, svec(3), x_0(3), parang
  real(kind=8) :: jtdt, jut1, jutc
  character(len=12) :: telescope
  type(projection_t) :: proj
  !
  character(len=12), save :: last_telescope = ' '
  !
  ! Set defaults
  dop = 0d0
  !
  ! Check we know the system
  if (obs%pos%system.eq.type_eq) then
     coord = 'EQ'
  elseif (obs%pos%system.eq.type_ga) then
     coord = 'GA'
  elseif (obs%pos%system.eq.type_ic) then
     coord = 'IC'
  else
     call class_message(seve%e,rname,  &
       'Unsupported system of coordinates '//obs_system(obs%pos%system))
     error = .true.
     return
  endif
  !
  if (set%obs_name.eq.'*') then
    ! Guess observatory from TELESCOPE name
    call my_get_teles(rname,obs%gen%teles,.true.,telescope,error)
    if (error)  return
    if (telescope.ne.last_telescope) then
      if (telescope.eq.'HERSCHEL') then
        ! Not Earth based, Astro can not help!
        ! Herschel HIFI frequencies are normally expressed in the LSR frame
        obs%spe%doppler = 0.d0
        return
      else
        call astro_observatory(telescope,error)
        if (error) then
          call class_message(seve%e,rname,'No such observatory '//telescope)
          return
        endif
      endif
      last_telescope = telescope
    endif
  else
    ! User-defined observatory
    ! NB: we should think in a way to avoid re-setting the observatory
    ! at each call
    call astro_observatory(set%obs_long,set%obs_lati,set%obs_alti,0.d0,error)
    if (error)  return
  endif
  !
  ! Reset the time
  jutc = obs%gen%dobs + obs%gen%ut/(2d0*pi) +  2460549.5d0
  !
  ! For what we want to do, these are negligible
  jut1 = 0.
  Jtdt = 0.
  call do_astro_time(jutc,jut1,jtdt,error)
  if (error) return
  !
  equinox = obs%pos%equinox
  !
  if (use_ref) then
    ! Compute Doppler at reference position
    lambda = obs%pos%lam
    beta   = obs%pos%bet
  else
    ! Compute Doppler at offset position
    ! Setup the projection in use
    call gwcs_projec(obs%pos%lam,obs%pos%bet,obs%pos%projang,obs%pos%proj,  &
      proj,error)
    if (error)  return
    ! Convert to absolute coordinates
    rxoff = obs%pos%lamof  ! Convert to R*8
    ryoff = obs%pos%betof  ! Convert to R*8
    call rel_to_abs(proj,rxoff,ryoff,lambda,beta,1)
  endif
  !
  ! Call ASTRO
  call do_object(coord,equinox,lambda,beta,s_2,s_3,dop,lsr,svec,x_0,parang,error)
  if (error) return
  !
  ! Now retrieve the Doppler value
  !
  if (obs%spe%vtype.eq.vel_lsr) then
     vshift = dop+lsr+obs%spe%voff
  elseif  (obs%spe%vtype.eq.vel_hel) then
     vshift = dop+obs%spe%voff
  elseif  (obs%spe%vtype.eq.vel_ear) then
     vshift = obs%spe%voff
  else
     vshift = 0.0d0
  endif
  obs%spe%doppler = - vshift / clight * 1d3
end subroutine compute_doppler
