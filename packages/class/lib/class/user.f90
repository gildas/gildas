!-----------------------------------------------------------------------
! Support for file for User Section
!-----------------------------------------------------------------------
!
subroutine class_user_owner(sowner,stitle)
  use class_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ public (available to external programs)
  !  Set the userhooks(cuserhooks)%owner character variable
  !---------------------------------------------------------------------
  character(len=*), intent(in) :: sowner  ! Section owner
  character(len=*), intent(in) :: stitle  ! Section title
  !
  call classcore_user_owner(sowner,stitle)
  !
end subroutine class_user_owner
!
subroutine class_user_add(obs,sversion,sdata,error)
  use class_dependencies_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch),
  !   public (available to external programs)
  !   Add a User Section to the input observation. The caller has no
  ! control on the number of this new user section
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs       ! Observation
  integer(kind=4),   intent(in)    :: sversion  ! Version value
  integer(kind=4),   intent(in)    :: sdata     ! The user data (do not use "as is")
  logical,           intent(inout) :: error     ! Logical error flag
  !
  call classcore_user_add(obs,sversion,sdata,error)
  if (error)  return
  !
end subroutine class_user_add
!
subroutine class_user_update(obs,sversion,sdata,error)
  use class_dependencies_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ no-interface (because of argument type mismatch)
  !   public (available to external programs)
  !  Update the User Section in the input observation. The caller has no
  ! control on the number of this new user section
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs       ! Observation
  integer(kind=4),   intent(in)    :: sversion  ! Version value
  integer(kind=4),   intent(in)    :: sdata     ! The user data (do not use "as is")
  logical,           intent(inout) :: error     ! Logical error flag
  !
  call classcore_user_update(obs,sversion,sdata,error)
  if (error)  return
  !
end subroutine class_user_update
!
subroutine class_user_toclass(usertoclass)
  use class_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ public (available to external programs)
  !  Set the userhooks(cuserhooks)%toclass procedure to the input procedure
  !---------------------------------------------------------------------
  external usertoclass
  !
  call classcore_user_toclass(usertoclass)
  !
end subroutine class_user_toclass
!
subroutine class_user_dump(userdump)
  use class_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ public (available to external programs)
  !  Set the userhooks(cuserhooks)%dump procedure to the input procedure
  !---------------------------------------------------------------------
  external userdump
  !
  call classcore_user_dump(userdump)
  !
end subroutine class_user_dump
!
subroutine class_user_fix(userfix)
  use class_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ public (available to external programs)
  !  Set the userhooks(cuserhooks)%fix procedure to the input procedure
  !---------------------------------------------------------------------
  external userfix
  !
  call classcore_user_fix(userfix)
  !
end subroutine class_user_fix
!
subroutine class_user_find(userfind)
  use class_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ public (available to external programs)
  !  Set the userhooks(cuserhooks)%find procedure to the input procedure
  !---------------------------------------------------------------------
  external userfind
  !
  call classcore_user_find(userfind)
  !
end subroutine class_user_find
!
subroutine class_user_setvar(usersetvar)
  use class_dependencies_interfaces
  use class_setup
  !---------------------------------------------------------------------
  ! @ public (available to external programs)
  !  Set the userhooks(cuserhooks)%setvar procedure to the input procedure
  !---------------------------------------------------------------------
  external usersetvar
  !
  call classcore_user_setvar(set,usersetvar)
  !
end subroutine class_user_setvar
!
subroutine class_user_varidx_fill(uservaridx_fill)
  use class_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ public (available to external programs)
  !  Set the userhooks(cuserhooks)%varidx_fill procedure to the input
  ! procedure
  !---------------------------------------------------------------------
  external uservaridx_fill
  !
  call classcore_user_varidx_fill(uservaridx_fill)
  !
end subroutine class_user_varidx_fill
!
subroutine class_user_varidx_defvar(uservaridx_defvar)
  use class_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ public (available to external programs)
  !  Set the userhooks(cuserhooks)%varidx_defvar procedure to the input
  ! procedure
  !---------------------------------------------------------------------
  external uservaridx_defvar
  !
  call classcore_user_varidx_defvar(uservaridx_defvar)
  !
end subroutine class_user_varidx_defvar
!
subroutine class_user_varidx_realloc(uservaridx_realloc)
  use class_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ public (available to external programs)
  !  Set the userhooks(cuserhooks)%varidx_realloc procedure to the input
  ! procedure
  !---------------------------------------------------------------------
  external uservaridx_realloc
  !
  call classcore_user_varidx_realloc(uservaridx_realloc)
  !
end subroutine class_user_varidx_realloc
!
subroutine class_user_def_inte(suffix,ndim,dims,error)
  use class_dependencies_interfaces
  use class_rt
  use class_setup
  !---------------------------------------------------------------------
  ! @ public (available to external programs)
  !  Define the Sic integer variable R%USER%OWNER%SUFFIX
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: suffix   ! Component name
  integer(kind=4),  intent(in)    :: ndim     ! Number of dimensions (0=scalar)
  integer(kind=4),  intent(in)    :: dims(4)  ! Dimensions (if needed)
  logical,          intent(inout) :: error    ! Logical error flag
  !
  call classcore_user_def_inte(set,r,suffix,ndim,dims,error)
  if (error)  return
  !
end subroutine class_user_def_inte
!
subroutine class_user_def_real(suffix,ndim,dims,error)
  use class_dependencies_interfaces
  use class_rt
  use class_setup
  !---------------------------------------------------------------------
  ! @ public (available to external programs)
  !  Define the Sic real variable R%USER%OWNER%SUFFIX
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: suffix   ! Component name
  integer(kind=4),  intent(in)    :: ndim     ! Number of dimensions (0=scalar)
  integer(kind=4),  intent(in)    :: dims(4)  ! Dimensions (if needed)
  logical,          intent(inout) :: error    ! Logical error flag
  !
  call classcore_user_def_real(set,r,suffix,ndim,dims,error)
  if (error)  return
  !
end subroutine class_user_def_real
!
subroutine class_user_def_dble(suffix,ndim,dims,error)
  use class_dependencies_interfaces
  use class_rt
  use class_setup
  !---------------------------------------------------------------------
  ! @ public (available to external programs)
  !  Define the Sic double variable R%USER%OWNER%SUFFIX
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: suffix   ! Component name
  integer(kind=4),  intent(in)    :: ndim     ! Number of dimensions (0=scalar)
  integer(kind=4),  intent(in)    :: dims(4)  ! Dimensions (if needed)
  logical,          intent(inout) :: error    ! Logical error flag
  !
  call classcore_user_def_dble(set,r,suffix,ndim,dims,error)
  if (error)  return
  !
end subroutine class_user_def_dble
!
subroutine class_user_def_char(suffix,lchain,error)
  use class_dependencies_interfaces
  use class_rt
  use class_setup
  !---------------------------------------------------------------------
  ! @ public (available to external programs)
  !  Define the Sic character variable R%USER%OWNER%SUFFIX
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: suffix   ! Component name
  integer(kind=4),  intent(in)    :: lchain   ! String length in the 'data' buffer
  logical,          intent(inout) :: error    ! Logical error flag
  !
  call classcore_user_def_char(set,r,suffix,lchain,error)
  if (error)  return
  !
end subroutine class_user_def_char
