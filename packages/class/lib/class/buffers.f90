module class_rt
  use class_types
  !
  type(observation), target  :: RandT(2)  ! Target for R and T
  !
  type(observation), pointer :: R  ! R buffer (current observation)
  type(observation), pointer :: T  ! T buffer (previous observation)
  !
end module class_rt
!
module class_setup
  use class_types
  type(class_setup_t) :: set  ! Obsolescent
end module class_setup
