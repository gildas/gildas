subroutine output(spec,nspec,lshare,error)
  use class_dependencies_interfaces
  use class_setup
  !---------------------------------------------------------------------
  ! @ obsolete (since 02-Nov-2011)
  ! Compatibility routine with the "old" calling syntax for MIRA
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: spec    ! File name including extension
  integer,          intent(in)  :: nspec   ! Length of SPEC
  logical,          intent(in)  :: lshare  ! Is file to be shared (ignored)
  logical,          intent(out) :: error   ! Logical error flag
  !
  call classcore_fileout_old(set,spec,nspec,.false.,error)
end subroutine output
!
subroutine newput(spec,nspec,lsize,lshare,error)
  use classic_api
  use class_dependencies_interfaces
  use class_setup
  !---------------------------------------------------------------------
  ! @ obsolete (since 22-Oct-08)
  ! Compatibility routine with the "old" calling syntax for MIRA
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: spec    ! File name including extension
  integer,          intent(in)  :: nspec   ! Length of SPEC
  integer,          intent(in)  :: lsize   ! Maximum number of observations allowed
  logical,          intent(in)  :: lshare  ! Is file to be shared ? (ignored)
  logical,          intent(out) :: error   ! Logical error flag
  ! Local
  logical :: lsingle ! Should spectra be unique ?
  logical :: lover   ! Overwrite existing file ?
  integer(kind=entry_length) :: lsize8
  !
  lsize8 = lsize
  lsingle = .true.
  lover = .false.
  call classcore_fileout_new(set,spec,nspec,lsize8,lsingle,lover,error)
end subroutine newput
!
subroutine iobs (n,entry_num,error)
  use class_dependencies_interfaces
  use class_setup
  use class_rt
  !---------------------------------------------------------------------
  ! @ obsolete (since 22-Oct-08)
  ! Compatibility routine for MIRA
  ! Starts the writing of an observation.
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)  :: n          ! Max num of sections to be written (in addition of the data)
  integer(kind=4), intent(out) :: entry_num  ! Entry number of the observation in the output file
  logical,         intent(out) :: error      ! Error flag
  ! Local
  integer(kind=8) :: num8
  !
  call class_write_open(set,r,n,num8,error)
  entry_num = num8
end subroutine iobs
!
subroutine class_close(error)
  use class_dependencies_interfaces
  use class_rt
  use class_setup
  !---------------------------------------------------------------------
  ! @ obsolete (since 02-Nov-2011)
  ! Compatibility routine for MIRA
  ! Close an observation and reopen the output file
  !---------------------------------------------------------------------
  logical, intent(inout) :: error ! Error status
  !
  call class_write_flush(set,r,error)
end subroutine class_close
!
subroutine message(level,severity,progname,line)
  use class_dependencies_interfaces
  use class_setup
  !---------------------------------------------------------------------
  ! @ obsolete (used by Mira only)
  !  OBSOLETE messaging method. Please use the gmessage facility
  ! provided by the kernel.
  !  Output message according to current priority levels, and set the
  ! severity information.
  !---------------------------------------------------------------------
  integer,          intent(in) :: level    ! Priority level controlled by SET LEVEL
  integer,          intent(in) :: severity ! Severity 'I','W','E','F'
  character(len=*), intent(in) :: progname ! Name of calling procedure
  character(len=*), intent(in) :: line     ! Character string
  !
  ! Local variables
  character(len=256) :: linout, string
  character(len=1)   :: severities(4)
  integer :: nl
  integer :: csev
  data severities/'I','W','E','F'/
  !
  ! Do nothing if not serious enough...
  if (set%slev.gt.level .and. set%flev.gt.level) return
  !
  csev = min(max(severity,1),4)
  linout = line
  nl = len(linout)
  call sic_noir(linout,nl)
  string = severities(csev)//'-'//progname//', '//linout(1:nl)
  nl = lenc(string)
  !
  if (set%slev.le.level) then
     call gagout(string)
  endif
!   if (set%flev.le.level) then
!      write(mfile,'(i2,a,a)') level,'-',string(1:nl)
!   endif
  !
end subroutine message
