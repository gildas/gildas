subroutine allocate_class(error)
  use class_dependencies_interfaces
  use class_interfaces, except_this=>allocate_class
  !---------------------------------------------------------------------
  ! @ private
  ! Allocate all Class buffers
  !---------------------------------------------------------------------
  logical, intent(inout) :: error
  !
  call allocate_classcore(error)
  if (error)  return
  call allocate_RT(error)
  if (error)  return
  !
end subroutine allocate_class
!
subroutine allocate_RT(error)
  use class_dependencies_interfaces
  use class_interfaces, except_this=>allocate_RT
  use class_rt
  !---------------------------------------------------------------------
  ! @ private
  ! Allocate R and T buffers
  !---------------------------------------------------------------------
  logical, intent(inout) :: error
  ! Local
  integer(kind=4), parameter :: class_data_len=32  ! Default size for R/T arrays
  !
  r => RandT(1)
  t => RandT(2)
  !
  call class_obs_init(r,error)
  r%is_R = .true.
  call class_obs_init(t,error)
  t%is_R = .false.
  !
  call reallocate_obs(r,class_data_len,error)
  if (error) return
  call reallocate_obs(t,class_data_len,error)
  if (error) return
  !
end subroutine allocate_RT
!
!-----------------------------------------------------------------------
!
subroutine deallocate_class(error)
  use class_dependencies_interfaces
  use class_interfaces, except_this=>deallocate_class
  !---------------------------------------------------------------------
  ! @ private
  ! Deallocate all Class buffers
  !---------------------------------------------------------------------
  logical, intent(inout) :: error
  !
  call deallocate_RT(error)
  if (error)  return
  call deallocate_classcore(error)
  if (error)  return
  !
end subroutine deallocate_class
!
subroutine deallocate_RT(error)
  use class_dependencies_interfaces
  use class_interfaces, except_this=>deallocate_RT
  use class_rt
  !---------------------------------------------------------------------
  ! @ private
  ! Deallocate R and T buffers
  !---------------------------------------------------------------------
  logical, intent(inout) :: error
  !
  call class_obs_clean(r,error)
  call class_obs_clean(t,error)
  !
end subroutine deallocate_RT
!
subroutine class_exit(error)
  use class_dependencies_interfaces
  use class_interfaces, except_this=>class_exit
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  logical, intent(inout) :: error
  !
  ! Class buffers
  call deallocate_class(error)
  !
  ! Free MEMORY buffers
  call memorize_free_all
  !
  call class_files_close(error)
  ! if (error)  continue
  call class_luns_free(error)
  ! if (error)  continue
  !
  call class_toc_clean(error)
  ! if (error)  continue
  !
end subroutine class_exit
