module class_interfaces_public
  interface
    subroutine class_pack_set(pack)
      use gpack_def
      !---------------------------------------------------------------------
      ! @ public-mandatory (because symbol is used elsewhere)
      !---------------------------------------------------------------------
      type(gpack_info_t), intent(out) :: pack
    end subroutine class_pack_set
  end interface
  !
  interface
    subroutine class_write_init(error)
      !---------------------------------------------------------------------
      ! @ public (available to external programs)
      !   Public procedure suited for programs linked basically to the
      ! libclass, i.e. with no Gildas specific features like interpreter or
      ! initializations or so. See demo program classdemo-telwrite.f90
      !   Perform the basic initializations of the libclass needed for
      ! the writing process (e.g. stand-alone program at the telescope).
      !   Should be done once at program startup.
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine class_write_init
  end interface
  !
  interface
    subroutine class_write_clean(error)
      !---------------------------------------------------------------------
      ! @ public (available to external programs)
      !   Public procedure suited for programs linked basically to the
      ! libclass, i.e. with no Gildas specific features like interpreter or
      ! initializations or so. See demo program classdemo-telwrite.f90
      !   Perform the basic cleaning of the libclass needed after the
      ! writing process (e.g. stand-alone program at the telescope).
      !  Should be done once before leaving the program
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine class_write_clean
  end interface
  !
  interface
    subroutine class_fileout_open(spec,lnew,lover,lsize,lsingle,error)
      use classic_api
      use class_setup
      !---------------------------------------------------------------------
      ! @ public (available to external programs)
      !  Public entry point to open a new or reopen an old output file.
      !---------------------------------------------------------------------
      character(len=*),           intent(in)    :: spec     ! File name including extension
      logical,                    intent(in)    :: lnew     ! Should it be a new file or an old?
      logical,                    intent(in)    :: lover    ! If new, overwrite existing file or raise an error?
      integer(kind=entry_length), intent(in)    :: lsize    ! If new, maximum number of observations allowed
      logical,                    intent(in)    :: lsingle  ! If new, should spectra be unique?
      logical,                    intent(inout) :: error    ! Logical error flag
    end subroutine class_fileout_open
  end interface
  !
  interface
    subroutine class_fileout_close(error)
      !---------------------------------------------------------------------
      ! @ public (available to external programs)
      !  Close the output file without closing the input file if they are
      ! the same
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Error status
    end subroutine class_fileout_close
  end interface
  !
  interface
    subroutine class_obs_write(obs,error)
      use class_setup
      use class_types
      !---------------------------------------------------------------------
      ! @ public (available to external programs)
      !  Write the observation to the currently opened output file
      !  See demo program classdemo-telwrite.f90
      !---------------------------------------------------------------------
      type(observation), intent(inout) :: obs    !
      logical,           intent(inout) :: error  ! Logical error flag
    end subroutine class_obs_write
  end interface
  !
  interface
    subroutine class_fold_obs(obs,keepblank,error)
      use class_setup
      use class_types
      !----------------------------------------------------------------------
      ! @ public (available to external programs)
      !   Fold an unfolded frequency switched observation.
      !  Each phase is assumed to be given a weight and a frequency change.
      !  The frequency change need not be an integer number of channels.
      !  The number of phases is not limited.
      !-----------------------------------------------------------------------
      type(observation), intent(inout) :: obs
      logical,           intent(in)    :: keepblank  ! Keep blank channels at boundaries?
      logical,           intent(inout) :: error
    end subroutine class_fold_obs
  end interface
  !
  interface
    subroutine class_modify_vdirection(head,error)
      use class_setup
      use class_types
      !---------------------------------------------------------------------
      ! @ public (available to external programs)
      ! Support routine for command:
      !  MODIFY VDIRECTION
      !---------------------------------------------------------------------
      type(header), intent(inout) :: head   !
      logical,      intent(inout) :: error  !
    end subroutine class_modify_vdirection
  end interface
  !
  interface
    subroutine class_user_owner(sowner,stitle)
      !---------------------------------------------------------------------
      ! @ public (available to external programs)
      !  Set the userhooks(cuserhooks)%owner character variable
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: sowner  ! Section owner
      character(len=*), intent(in) :: stitle  ! Section title
    end subroutine class_user_owner
  end interface
  !
  interface
    subroutine class_user_toclass(usertoclass)
      !---------------------------------------------------------------------
      ! @ public (available to external programs)
      !  Set the userhooks(cuserhooks)%toclass procedure to the input procedure
      !---------------------------------------------------------------------
      external usertoclass
    end subroutine class_user_toclass
  end interface
  !
  interface
    subroutine class_user_dump(userdump)
      !---------------------------------------------------------------------
      ! @ public (available to external programs)
      !  Set the userhooks(cuserhooks)%dump procedure to the input procedure
      !---------------------------------------------------------------------
      external userdump
    end subroutine class_user_dump
  end interface
  !
  interface
    subroutine class_user_fix(userfix)
      !---------------------------------------------------------------------
      ! @ public (available to external programs)
      !  Set the userhooks(cuserhooks)%fix procedure to the input procedure
      !---------------------------------------------------------------------
      external userfix
    end subroutine class_user_fix
  end interface
  !
  interface
    subroutine class_user_find(userfind)
      !---------------------------------------------------------------------
      ! @ public (available to external programs)
      !  Set the userhooks(cuserhooks)%find procedure to the input procedure
      !---------------------------------------------------------------------
      external userfind
    end subroutine class_user_find
  end interface
  !
  interface
    subroutine class_user_setvar(usersetvar)
      use class_setup
      !---------------------------------------------------------------------
      ! @ public (available to external programs)
      !  Set the userhooks(cuserhooks)%setvar procedure to the input procedure
      !---------------------------------------------------------------------
      external usersetvar
    end subroutine class_user_setvar
  end interface
  !
  interface
    subroutine class_user_varidx_fill(uservaridx_fill)
      !---------------------------------------------------------------------
      ! @ public (available to external programs)
      !  Set the userhooks(cuserhooks)%varidx_fill procedure to the input
      ! procedure
      !---------------------------------------------------------------------
      external uservaridx_fill
    end subroutine class_user_varidx_fill
  end interface
  !
  interface
    subroutine class_user_varidx_defvar(uservaridx_defvar)
      !---------------------------------------------------------------------
      ! @ public (available to external programs)
      !  Set the userhooks(cuserhooks)%varidx_defvar procedure to the input
      ! procedure
      !---------------------------------------------------------------------
      external uservaridx_defvar
    end subroutine class_user_varidx_defvar
  end interface
  !
  interface
    subroutine class_user_varidx_realloc(uservaridx_realloc)
      !---------------------------------------------------------------------
      ! @ public (available to external programs)
      !  Set the userhooks(cuserhooks)%varidx_realloc procedure to the input
      ! procedure
      !---------------------------------------------------------------------
      external uservaridx_realloc
    end subroutine class_user_varidx_realloc
  end interface
  !
  interface
    subroutine class_user_def_inte(suffix,ndim,dims,error)
      use class_rt
      use class_setup
      !---------------------------------------------------------------------
      ! @ public (available to external programs)
      !  Define the Sic integer variable R%USER%OWNER%SUFFIX
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: suffix   ! Component name
      integer(kind=4),  intent(in)    :: ndim     ! Number of dimensions (0=scalar)
      integer(kind=4),  intent(in)    :: dims(4)  ! Dimensions (if needed)
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine class_user_def_inte
  end interface
  !
  interface
    subroutine class_user_def_real(suffix,ndim,dims,error)
      use class_rt
      use class_setup
      !---------------------------------------------------------------------
      ! @ public (available to external programs)
      !  Define the Sic real variable R%USER%OWNER%SUFFIX
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: suffix   ! Component name
      integer(kind=4),  intent(in)    :: ndim     ! Number of dimensions (0=scalar)
      integer(kind=4),  intent(in)    :: dims(4)  ! Dimensions (if needed)
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine class_user_def_real
  end interface
  !
  interface
    subroutine class_user_def_dble(suffix,ndim,dims,error)
      use class_rt
      use class_setup
      !---------------------------------------------------------------------
      ! @ public (available to external programs)
      !  Define the Sic double variable R%USER%OWNER%SUFFIX
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: suffix   ! Component name
      integer(kind=4),  intent(in)    :: ndim     ! Number of dimensions (0=scalar)
      integer(kind=4),  intent(in)    :: dims(4)  ! Dimensions (if needed)
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine class_user_def_dble
  end interface
  !
  interface
    subroutine class_user_def_char(suffix,lchain,error)
      use class_rt
      use class_setup
      !---------------------------------------------------------------------
      ! @ public (available to external programs)
      !  Define the Sic character variable R%USER%OWNER%SUFFIX
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: suffix   ! Component name
      integer(kind=4),  intent(in)    :: lchain   ! String length in the 'data' buffer
      logical,          intent(inout) :: error    ! Logical error flag
    end subroutine class_user_def_char
  end interface
  !
end module class_interfaces_public
