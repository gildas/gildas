subroutine cube_functions(error)
  use gbl_message
  use class_dependencies_interfaces
  use class_interfaces, except_this=>cube_functions
  !---------------------------------------------------------------------
  ! @ private
  ! Define cube-building functions
  !---------------------------------------------------------------------
  logical, intent(inout) :: error
  !
  call sic_def_func('TDV',   s_tdv,   d_tdv,   2,error,hlpfile='gag_help_func_class')
  call sic_def_func('STDV',  s_stdv,  d_stdv,  1,error,hlpfile='gag_help_func_class')
  call sic_def_func('TPEAK', s_tpeak, d_tpeak, 1,error,hlpfile='gag_help_func_class')
  call sic_def_func('VPEAK', s_vpeak, d_vpeak, 1,error,hlpfile='gag_help_func_class')
  call sic_def_func('TPEAKV',s_tpeakv,d_tpeakv,2,error,hlpfile='gag_help_func_class')
  call sic_def_func('VPEAKV',s_vpeakv,d_vpeakv,2,error,hlpfile='gag_help_func_class')
  if (error) &
     call class_message(seve%f,'CUBE_FUNCTIONS','Error in function definitions')
  !
end subroutine cube_functions
!
function s_tdv(v1,v2)
  use class_dependencies_interfaces
  use class_rt
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  !---------------------------------------------------------------------
  real(kind=4) :: s_tdv  !
  real(kind=8), intent(in) :: v1,v2  !
  ! Local
  real(kind=4) :: v,s0,data1
  !
  call allinfo(r,v1,v2,v,s0,data1)
  s_tdv=s0
end function s_tdv
!
function d_tdv(v1,v2)
  use class_dependencies_interfaces
  use class_rt
  !-------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  !-------------------------------------------------------------------
  real(kind=8) :: d_tdv  !
  real(kind=8), intent(in) :: v1,v2  !
  ! Local
  real(kind=4) :: v,s0,data1
  !
  call allinfo(r,v1,v2,v,s0,data1)
  d_tdv=dble(s0)
end function d_tdv
!
function s_tpeakv(v1,v2)
  use class_dependencies_interfaces
  use class_rt
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  !---------------------------------------------------------------------
  real(kind=4) :: s_tpeakv  !
  real(kind=8), intent(in) :: v1,v2  !
  ! Local
  real(kind=4) :: v,s0,data1
  !
  call allinfo(r,v1,v2,v,s0,data1)
  s_tpeakv=data1
end function s_tpeakv
!
function d_tpeakv(v1,v2)
  use class_dependencies_interfaces
  use class_rt
  !-------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  !-------------------------------------------------------------------
  real(kind=8) :: d_tpeakv  !
  real(kind=8), intent(in) :: v1,v2  !
  ! Local
  real(kind=4) v,s0,data1
  !
  call allinfo(r,v1,v2,v,s0,data1)
  d_tpeakv=dble(data1)
end function d_tpeakv
!
function s_vpeakv(v1,v2)
  use class_dependencies_interfaces
  use class_rt
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  !---------------------------------------------------------------------
  real(kind=4) :: s_vpeakv  !
  real(kind=8), intent(in) :: v1,v2  !
  ! Local
  real(kind=4) :: v,s0,data1
  !
  call allinfo(r,v1,v2,v,s0,data1)
  s_vpeakv=v
end function s_vpeakv
!
function d_vpeakv(v1,v2)
  use class_dependencies_interfaces
  use class_rt
  !-------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  !-------------------------------------------------------------------
  real(kind=8) :: d_vpeakv  !
  real(kind=8), intent(in) :: v1,v2  !
  ! Local
  real(kind=4) :: v,s0,data1
  !
  call allinfo(r,v1,v2,v,s0,data1)
  d_vpeakv=dble(v)
end function d_vpeakv
!
function s_stdv(dummy)
  use class_dependencies_interfaces
  use class_rt
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  !---------------------------------------------------------------------
  real(kind=4) :: s_stdv  !
  real(kind=8), intent(in) :: dummy  !
  ! Local
  real(kind=8) :: v1,v2
  real(kind=4) :: v,s0,data1
  !
  call abscissa_velo_left(r%head,v1)
  call abscissa_velo_right(r%head,v2)
  call allinfo(r,v1,v2,v,s0,data1)
  s_stdv=s0
end function s_stdv
!
function d_stdv(dummy)
  use class_dependencies_interfaces
  use class_rt
  !-------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  !-------------------------------------------------------------------
  real(kind=8) :: d_stdv  !
  real(kind=8), intent(in) :: dummy  !
  ! Local
  real(kind=8) :: v1,v2
  real(kind=4) :: v,s0,data1
  !
  call abscissa_velo_left(r%head,v1)
  call abscissa_velo_right(r%head,v2)
  call allinfo(r,v1,v2,v,s0,data1)
  d_stdv=dble(s0)
end function d_stdv
!
function s_tpeak(dummy)
  use class_dependencies_interfaces
  use class_rt
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  !---------------------------------------------------------------------
  real(kind=4) :: s_tpeak  !
  real(kind=8), intent(in) :: dummy  !
  ! Local
  real(kind=8) :: v1,v2
  real(kind=4) :: v,s0,data1
  !
  call abscissa_velo_left(r%head,v1)
  call abscissa_velo_right(r%head,v2)
  call allinfo(r,v1,v2,v,s0,data1)
  s_tpeak=data1
end function s_tpeak
!
function d_tpeak(dummy)
  use class_dependencies_interfaces
  use class_rt
  !-------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  !-------------------------------------------------------------------
  real(kind=8) :: d_tpeak  !
  real(kind=8), intent(in) :: dummy  !
  ! Local
  real(kind=8) :: v1,v2
  real(kind=4) :: v,s0,data1
  !
  call abscissa_velo_left(r%head,v1)
  call abscissa_velo_right(r%head,v2)
  call allinfo(r,v1,v2,v,s0,data1)
  d_tpeak=dble(data1)
end function d_tpeak
!
function s_vpeak(dummy)
  use class_dependencies_interfaces
  use class_rt
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  !---------------------------------------------------------------------
  real(kind=4) :: s_vpeak  !
  real(kind=8), intent(in) :: dummy  !
  ! Local
  real(kind=8) :: v1,v2
  real(kind=4) :: v,s0,data1
  !
  call abscissa_velo_left(r%head,v1)
  call abscissa_velo_right(r%head,v2)
  call allinfo(r,v1,v2,v,s0,data1)
  s_vpeak=v
  return
end function s_vpeak
!
function d_vpeak(dummy)
  use class_dependencies_interfaces
  use class_rt
  !-------------------------------------------------------------------
  ! @ private
  ! CLASS internal routine
  !-------------------------------------------------------------------
  real(kind=8) :: d_vpeak  !
  real(kind=8), intent(in) :: dummy  !
  ! Local
  real(kind=8) :: v1,v2
  real(kind=4) :: v,s0,data1
  !
  call abscissa_velo_left(r%head,v1)
  call abscissa_velo_right(r%head,v2)
  call allinfo(r,v1,v2,v,s0,data1)
  d_vpeak=dble(v)
end function d_vpeak
