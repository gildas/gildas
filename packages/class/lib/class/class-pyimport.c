/*****************************************************************************
 *                              Dependencies                                 *
 *****************************************************************************/

#ifdef GAG_USE_PYTHON

#define PY_SSIZE_T_CLEAN /* s# format in PyArg_Parse* API expect Py_ssize_t argument */
#include <Python.h>
#include "gsys/cfc.h"

#define pyclass_rx_val     CFC_EXPORT_NAME( pyclass_rx_val )
#define pyclass_rx_minmax  CFC_EXPORT_NAME( pyclass_rx_minmax )
#define pyclass_px_minmax  CFC_EXPORT_NAME( pyclass_px_minmax )

void gpy_addmethods(PyObject *m, PyMethodDef *methods);
void pyclass_rx_val    (double *, char *, double *, char *, int *, int, int);
void pyclass_rx_minmax (char *, double *, double *, int *, int);
void pyclass_px_minmax (char *, double *, double *, int *, int);

#endif /* GAG_USE_PYTHON */


/*****************************************************************************
 *                             Function bodies                               *
 *****************************************************************************/

/* Enable call of pyclass_extensions */
#define GPACK_PYTHON_EXTENSIONS yes

/* Define entry point "initpyclass" for command "import pyclass" in Python */
#include "sic/gpackage-pyimport.h"
GPACK_DEFINE_PYTHON_IMPORT(class);


#ifdef GAG_USE_PYTHON

/**
 * C-Python overlay to Fortran subroutine pyclass_rx_val.
 *
 * @param[in] self.
 * @param[in] args is a PyObject with input params values.
 * @param[in] kwds is a PyObject with input params optional keywords.
 * @return a new reference to a PyFloat.
 */
PyObject* pyclass_rx_val_C(PyObject *self, PyObject *args, PyObject *kwds) {
  char      *iunit,*ounit;
  Py_ssize_t li,lo;
  int       fli,flo;
  double    ival,oval;
  int       error;

  static char *kwlist[] = {"ival","iunit","ounit",NULL};

  if (! PyArg_ParseTupleAndKeywords(args, kwds, "ds#s#", kwlist,&ival,
                                    &iunit,&li,&ounit,&lo))
      return NULL;

  fli = (int) li;
  flo = (int) lo;
  pyclass_rx_val(&ival,iunit,&oval,ounit,&error,fli,flo);
  if (error) {
    PyErr_SetString(PyExc_Exception,"Error while executing rx_val");
    return NULL;
  }

  return PyFloat_FromDouble(oval);  /* New reference */
}

/**
 * C-Python overlay to Fortran subroutine pyclass_rx_minmax.
 *
 * @param[in] self.
 * @param[in] args is a PyObject with input params values.
 * @param[in] kwds is a PyObject with input params optional keywords.
 * @return the PyArray sharing data with the corresponding SIC variable.
 */
PyObject* pyclass_rx_minmax_C(PyObject *self, PyObject *args, PyObject *kwds) {
  char      *unit;
  double    mini,maxi;
  int       error;
  PyObject  *tuple;

  static char *kwlist[] = {"unit",NULL};

  /* "|s": Python expect a single string argument. "|" means optional.
     Optional arguments must be initialized before parsing, they are
     not modified on return if not provided by user */
  unit = " ";
  if (! PyArg_ParseTupleAndKeywords(args, kwds, "|s", kwlist, &unit))
      return NULL;

  pyclass_rx_minmax(unit,&mini,&maxi,&error,strlen(unit));
  if (error) {
    PyErr_SetString(PyExc_Exception,"Error while executing rx_minmax");
    return NULL;
  }

  tuple = PyTuple_New(2);  /* New reference */
  PyTuple_SetItem(tuple, 0, Py_BuildValue("d", mini));
  PyTuple_SetItem(tuple, 1, Py_BuildValue("d", maxi));

  return tuple;  /* New reference */
}

/**
 * C-Python overlay to Fortran subroutine pyclass_px_minmax.
 *
 * @param[in] self.
 * @param[in] args is a PyObject with input params values.
 * @param[in] kwds is a PyObject with input params optional keywords.
 * @return the PyArray sharing data with the corresponding SIC variable.
 */
PyObject* pyclass_px_minmax_C(PyObject *self, PyObject *args, PyObject *kwds) {
  char      *unit;
  double    mini,maxi;
  int       error;
  PyObject  *tuple;

  static char *kwlist[] = {"unit",NULL};

  /* "|s": Python expect a single string argument. "|" means optional.
     Optional arguments must be initialized before parsing, they are
     not modified on return if not provided by user */
  unit = " ";
  if (! PyArg_ParseTupleAndKeywords(args, kwds, "|s", kwlist, &unit))
      return NULL;

  pyclass_px_minmax(unit,&mini,&maxi,&error,strlen(unit));
  if (error) {
    PyErr_SetString(PyExc_Exception,"Error while executing px_minmax");
    return NULL;
  }

  tuple = PyTuple_New(2);  /* New reference */
  PyTuple_SetItem(tuple, 0, Py_BuildValue("d", mini));
  PyTuple_SetItem(tuple, 1, Py_BuildValue("d", maxi));

  return tuple;  /* New reference */
}

static PyMethodDef pyclass_methods[] = {
  {"rx_val",    (PyCFunction)pyclass_rx_val_C,
                METH_VARARGS | METH_KEYWORDS,
                "Convert the X input value of the R spectrum to output unit"},
  {"rx_minmax", (PyCFunction)pyclass_rx_minmax_C,
                METH_VARARGS | METH_KEYWORDS,
                "Return the X range of values of the R spectrum, in the given unit"},
  {"px_minmax", (PyCFunction)pyclass_px_minmax_C,
                METH_VARARGS | METH_KEYWORDS,
                "Return the X range of values of the plot, in the given unit"},
  {NULL}  /* Sentinel */
};


/**
 * Add specific attributes and methods to the module pyclass.
 *
 * @param[in] the pyclass module
 * @return    nothing
 */
void pyclass_extensions(PyObject *modu) {

  /* Add attributes */
  /* Py_INCREF(Py_False);
     PyModule_AddObject(modu,"hello",Py_False); */

  /* Add methods */
  gpy_addmethods(modu,pyclass_methods);

};

#endif /* GAG_USE_PYTHON */
