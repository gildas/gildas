!-----------------------------------------------------------------------
! Support file for PYCLASS extensions, Fortran part
!-----------------------------------------------------------------------
!  Always compile this file, even if Python binding is disabled. This
! avoids different interface files when Python is enabled or not, and
! subsequent troubles when compiling in this 2 modes with the same
! sources.
!-----------------------------------------------------------------------
subroutine pyclass_rx_val(ival,iunitin,oval,ounitin,error)
  use gbl_message
  use class_dependencies_interfaces
  use class_interfaces, except_this=>pyclass_rx_val
  use class_rt
  !---------------------------------------------------------------------
  ! @ private
  !
  ! ---
  ! Entry point which can be called from Python. In particular the
  ! 'observation' is shared through a module.
  !---------------------------------------------------------------------
  real(kind=8),     intent(in)  :: ival     ! Input value
  character(len=*), intent(in)  :: iunitin  ! Input unit
  real(kind=8),     intent(out) :: oval     ! Output value
  character(len=*), intent(in)  :: ounitin  ! Output unit
  logical,          intent(out) :: error    ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='RX_VAL'
  !
  if (r%head%xnum.eq.0) then
    call class_message(seve%e,rname,'No R spectrum in memory')
    error = .true.
    return
  endif
  !
  call pyclass_obsx_val(r,ival,iunitin,oval,ounitin,error)
  if (error)  return
  !
end subroutine pyclass_rx_val
!
subroutine pyclass_rx_minmax(unitin,mini,maxi,error)
  use gbl_message
  use class_dependencies_interfaces
  use class_interfaces, except_this=>pyclass_rx_minmax
  use class_rt
  use class_setup
  !---------------------------------------------------------------------
  ! @ private
  !
  ! ---
  ! Entry point which can be called from Python. In particular the
  ! 'observation' is shared through a module.
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: unitin  ! Unit for returning values. Blank means current unit
  real(kind=8),     intent(out) :: mini    !
  real(kind=8),     intent(out) :: maxi    !
  logical,          intent(out) :: error   ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='RX_MINMAX'
  !
  if (r%head%xnum.eq.0) then
    call class_message(seve%e,rname,'No R spectrum in memory')
    error = .true.
    return
  endif
  !
  call pyclass_obsx_minmax(set,r,unitin,mini,maxi,error)
  if (error)  return
  !
end subroutine pyclass_rx_minmax
!
subroutine pyclass_px_minmax(unitin,mini,maxi,error)
  use class_dependencies_interfaces
  use class_interfaces, except_this=>pyclass_px_minmax
  use class_setup
  !---------------------------------------------------------------------
  ! @ private
  ! Return the X range of the plot
  ! NB: the subroutine returns actually the SET MODE X range, not
  ! the effective PLOT range (they are the same AFTER the command PLOT).
  ! But the command ANA\DRAW has the same bug-feature, and since the
  ! cursor is used by Weeds to identify lines, we are consistent...
  ! ---
  ! Entry point which can be called from Python. In particular the
  ! 'class_setup_t' is shared through a module.
  !---------------------------------------------------------------------
  character(len=*), intent(in)  :: unitin  ! Unit for returning values. Blank means current unit
  real(kind=8),     intent(out) :: mini    !
  real(kind=8),     intent(out) :: maxi    !
  logical,          intent(out) :: error   ! Logical error flag
  !
  call pyclass_plotx_minmax(set,unitin,mini,maxi,error)
  if (error)  return
  !
end subroutine pyclass_px_minmax
