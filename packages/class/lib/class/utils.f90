subroutine copyrt(user_function,key)
  use class_interfaces, except_this=>copyrt
  use class_rt
  !----------------------------------------------------------------------
  ! @ private
  ! Copy R memory to T memory
  !----------------------------------------------------------------------
  logical,          external   :: user_function  !
  character(len=*), intent(in) :: key            !
  ! Local
  logical :: ll,error
  !
  if (r%head%xnum.eq.0)  return  ! Nothing to copy yet (copy_obs would raise an error)
  !
  call copy_obs(r,t,error)
  ll = user_function('COPY')
  !
end subroutine copyrt
!
subroutine copy2r(obs,error)
  use class_interfaces, except_this=>copy2r
  use class_rt
  use class_setup
  !-------------------------------------------------------------------
  ! @ private
  ! Copy the observation into R memory
  !-------------------------------------------------------------------
  type(observation), intent(in)  :: obs    ! Current observation
  logical,           intent(out) :: error  ! Error flag
  !
  error = .false.
  call copy_obs(obs,r,error)
  if (error)  return
  !
  call newdat(set,r,error)
  call newdat_assoc(set,r,error)
  call newdat_user(set,r,error)
end subroutine copy2r
