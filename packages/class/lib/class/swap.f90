subroutine class_swap(line,error,user_function)
  use class_interfaces, except_this=>class_swap
  !----------------------------------------------------------------------
  ! @ private
  !  Exchange R and T memories
  !----------------------------------------------------------------------
  character(len=*), intent(in)    :: line
  logical,          intent(inout) :: error
  logical,          external      :: user_function
  !
  call swaprt('SWAP',error,user_function)
  !
end subroutine class_swap
!
subroutine swaprt(rname,error,user_function)
  use gbl_message
  use class_dependencies_interfaces
  use class_interfaces, except_this=>swaprt
  use class_rt
  use class_setup
  !----------------------------------------------------------------------
  ! @ private
  !  Exchange R and T memories
  !----------------------------------------------------------------------
  character(len=*), intent(in)    :: rname
  logical,          intent(inout) :: error
  logical,          external      :: user_function
  ! Local
  type(observation), pointer :: tmp
  logical :: ll,myerror
  !
  ! Use a local 'myerror' because the subroutine can be called in error
  ! recovery mode.
  !
  if (t%head%xnum.eq.0) then
    if (rname.eq.'SWAP') then
      call class_message(seve%e,rname,'No spectrum in T buffer')
      error = .true.
    endif
    return  ! Nothing to swap...
  endif
  !
  tmp => r
  r => t
  t => tmp
  r%is_R = .true.
  t%is_R = .false.
  !
  ll = user_function('SWAP')
  !
  ! Need a complete redefinition of all Sic variables pointing to R
  myerror = .false.
  call sic_delvariable ('R',.false.,myerror)
  myerror = .false.  ! No error if not found
  call las_variables_r(set,r,myerror)
  if (myerror) then
    error = .true.
    return
  endif
  call las_setvar_R_aliases(set,myerror)
  if (myerror) then
    error = .true.
    return  ! Also redefine SET VARIABLE aliases
  endif
  !
  call newdat(set,r,myerror)
  call newdat_assoc(set,r,myerror)
  call newdat_user(set,r,myerror)
  if (myerror)  error = .true.
  !
end subroutine swaprt
