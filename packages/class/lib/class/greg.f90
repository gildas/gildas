subroutine class_greg(set,line,r,error,user_function)
  use class_dependencies_interfaces
  use class_interfaces, except_this=>class_greg
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS Support routine for command
  !    GREG Name
  ! 1   [/FORMATTED [Format]]
  ! 2   [/READ [X n] [Y m]]
  !---------------------------------------------------------------------
  type(class_setup_t), intent(inout) :: set            !
  character(len=*),    intent(in)    :: line           ! Input command line
  type(observation),   intent(inout) :: r              !
  logical,             intent(inout) :: error          ! Logical flag
  logical,             external      :: user_function  !
  !
  if (sic_present(2,0)) then
    call class_greg_read_formatted(set,line,r,error,user_function)
  elseif (set%method.eq.'GAUSS') then
    call class_greg_write(set,line,r,progauss,r%head%gau%nline,class_sec_gau_id,error)
  elseif (set%method.eq.'NH3' .or. set%method.eq.'HFS') then
    call class_greg_write(set,line,r,pronh3,r%head%hfs%nline,class_sec_hfs_id,error)
  elseif (set%method.eq.'ABSORPTION') then
    call class_greg_write(set,line,r,proabs,r%head%abs%nline,class_sec_abs_id,error)
  elseif (set%method.eq.'SHELL') then
    call class_greg_write(set,line,r,proshell,r%head%she%nline,class_sec_she_id,error)
  endif
end subroutine class_greg
!
subroutine class_greg_write(set,line,r,fnc,nfit,fit_sec,error)
  use gildas_def
  use gbl_message
  use class_dependencies_interfaces
  use class_interfaces, except_this=>class_greg_write
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Writes a table or a formatted file
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set      !
  character(len=*),    intent(in)    :: line     ! Input command line
  type(observation),   intent(in)    :: r        !
  real(kind=4),        external      :: fnc      ! Fitting function
  integer(kind=4),     intent(in)    :: nfit     ! Number of lines fitted
  integer(kind=4),     intent(in)    :: fit_sec  ! Fit section (eg class_sec_gau_id)
  logical,             intent(inout) :: error    ! Logical flag
  ! Local
  character(len=*), parameter :: rname='GREG'
  integer(kind=4) :: nc
  character(len=filename_length) :: chain
  character(len=64) :: forma
  !
  if (r%head%xnum.eq.0) then
    call class_message(seve%e,rname,'No R spectrum in memory')
    error = .true.
    return
  endif
  !
  call sic_ch (line,0,1,chain,nc,.true.,error)
  if (error) return
  !
  if (sic_present(1,0)) then  ! /FORMATTED [Format]
    forma = ''
    call sic_ch(line,1,1,forma,nc,.false.,error)
    if (error)  return
    call class_greg_write_formatted(set,r,chain,fnc,nfit,fit_sec,forma,error)
    if (error)  return
  else
    call class_greg_write_gdf(r,chain,fnc,nfit,error)
    if (error)  return
  endif
  !
end subroutine class_greg_write
!
subroutine class_greg_write_gdf(r,chain,fnc,nfit,error)
  use gbl_message
  use image_def
  use class_dependencies_interfaces
  use class_interfaces, except_this=>class_greg_write_gdf
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(observation), intent(in)    :: r      !
  character(len=*),  intent(in)    :: chain  ! File name
  real(kind=4),      external      :: fnc    ! Fitting function
  integer(kind=4),   intent(in)    :: nfit   ! Number of lines fitted
  logical,           intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='GREG'
  type(gildas) :: tab
  real(kind=4), allocatable :: dtab(:,:)
  integer(kind=4) :: ier,nlines,ncols
  !
  call gildas_null(tab)
  !
  call sic_parse_file(chain,' ','.gdf',tab%file)
  call class_message(seve%i,rname,'Creating table '//tab%file)
  !
  tab%gil%dim(1) = r%cnchan
  if (nfit.ne.0) then
    tab%gil%dim(2) = 7 + nfit
  else
    tab%gil%dim(2) = 6
  endif
  tab%gil%coor_words = 0
  tab%gil%ndim = 2
  allocate(dtab(tab%gil%dim(1),tab%gil%dim(2)),stat=ier)
  if (failed_allocate(rname,'dtab',ier,error)) return
  !
  tab%loca%size = tab%gil%dim(1)*tab%gil%dim(2)
  nlines = tab%gil%dim(1)
  ncols = tab%gil%dim(2)
  call sgildas(r,dtab,nlines,ncols,fnc)
  !
  call gdf_write_image(tab,dtab,error)
  if (error) then
    call class_message(seve%e,rname,'Could not create output table')
    continue
  endif
  !
  deallocate(dtab)
  !
end subroutine class_greg_write_gdf
!
subroutine class_greg_write_formatted(set,r,chain,fnc,nfit,fit_sec,informa,error)
  use gildas_def
  use gbl_message
  use class_dependencies_interfaces
  use class_interfaces, except_this=>class_greg_write_formatted
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set      !
  type(observation),   intent(in)    :: r        !
  character(len=*),    intent(in)    :: chain    ! File name
  real(kind=4),        external      :: fnc      ! Fitting function
  integer(kind=4),     intent(in)    :: nfit     ! Number of lines fitted
  integer(kind=4),     intent(in)    :: fit_sec  ! Fit section (eg class_sec_gau_id)
  character(len=*),    intent(in)    :: informa  ! Input format ('' = auto)
  logical,             intent(inout) :: error    ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='GREG'
  character(len=filename_length) :: name
  integer(kind=4) :: ier,i,j,jtmp
  real(kind=8) :: o_l
  logical :: dobase
  character(len=64) :: forma
  !
  call sic_parse_file(chain,' ','.dat',name)
  call class_message(seve%i,rname,'Creating formatted '//name)
  !
  if (informa.eq.'') then
    forma = '(1pg17.9,12(1pg12.4))'
  else
    forma = informa
  endif
  !
  ier = sic_getlun(jtmp)
  if (ier.ne.1) then
    call class_message(seve%e,rname,'No logical unit left')
    error = .true.
    return
  endif
  if (.not.sic_present(3,0)) then
    ier = sic_open(jtmp,name,'NEW',.false.)
  else
    ier = sic_open(jtmp,name,'UNKNOWN',.false.)
    do while (.true.)
      read(jtmp,'(A)',end=50)    ! Go to EOF...
    enddo
50  continue
  endif
  if (ier.ne.0) then
    call class_message(seve%e,rname,'Cannot open file '//name)
    call putios('         ',ier)
    error=.true.
    call sic_frelun(jtmp)
    return
  endif
  !
  if (set%unitx(1).eq.'F') then
    o_l = r%head%spe%restf
  elseif (set%unitx(1).eq.'I') then
    o_l = r%head%spe%image
  else
    o_l = 0.d0
  endif
  if (r%head%presec(fit_sec)) then
    do i=r%cimin,r%cimax
      write(jtmp,forma,iostat=ier)  &
            r%datax(i)+o_l,r%spectre(i),            &  ! RX, RY
            fnc(r,real(r%datax(i),4),0,dobase),     &  ! Sum of all lines
            (fnc(r,real(r%datax(i),4),j,dobase),j=1,nfit)  ! Each line
      if (ier.ne.0) then
        call class_iostat(seve%e,rname,ier)
        error = .true.
        exit
      endif
    enddo
  else
    do i=r%cimin,r%cimax
      write(jtmp,forma,iostat=ier) r%datax(i)+o_l,r%spectre(i)
      if (ier.ne.0) then
        call class_iostat(seve%e,rname,ier)
        error = .true.
        exit
      endif
    enddo
  endif
  close(jtmp)
  call sic_frelun(jtmp)
  !
end subroutine class_greg_write_formatted
!
subroutine sgildas(obs,x,nlines,ncols,fnc)
  use gildas_def
  use class_interfaces, except_this=>sgildas
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Fills a table at the GILDAS format from the current scan and its
  ! fit results.
  !---------------------------------------------------------------------
  type(observation), intent(in)  :: obs              ! Input spectrum
  integer(kind=4),   intent(in)  :: nlines           ! Number of lines in table
  integer(kind=4),   intent(in)  :: ncols            ! Number of columns in table
  real(kind=4),      intent(out) :: x(nlines,ncols)  ! Table to be filled
  real(kind=4), external         :: fnc              ! Fitting function
  ! Local
  integer(kind=4) :: i,j
  logical :: dobase
  !
  do i=1,nlines
     x(i,1) = obs%spectre(i)  ! Values
  enddo
  !
  do i=1,nlines
     x(i,2) = i  ! Channels
  enddo
  !
  call abscissa_velo_r4(obs%head,x(1,3),1,nlines)    ! Velocities
  call abscissa_sigoff_r4(obs%head,x(1,4),1,nlines)  ! Offset frequency
  call abscissa_sigabs_r4(obs%head,x(1,5),1,nlines)  ! Rest frequency
  call abscissa_imaabs_r4(obs%head,x(1,6),1,nlines)  ! Image frequency
  !
  ! dobase is used as a flag for SHELL method
  ! dobase is used for pointing fits
  ! initialize it to false
  dobase = .false.
  do j = 0,ncols-7
     do i=1,nlines
        x(i,7+j) = fnc(obs,real(obs%datax(i),4),j,dobase) ! Fit results
     enddo
  enddo
end subroutine sgildas
!
subroutine class_greg_read_formatted(set,line,r,error,user_function)
  use gildas_def
  use gbl_constant
  use phys_const
  use gbl_message
  use class_dependencies_interfaces
  use class_interfaces, except_this=>class_greg_read_formatted
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS Internal routine
  ! Read a formatted file
  !---------------------------------------------------------------------
  type(class_setup_t), intent(inout) :: set            !
  character(len=*),    intent(in)    :: line           ! Input command line
  type(observation),   intent(inout) :: r              !
  logical,             intent(inout) :: error          ! Logical flag
  logical,             external      :: user_function  !
  ! Local
  character(len=*), parameter :: rname='GREG'
  integer :: nc,nl
  integer :: i,jtmp,ier,xcol,ycol,ucol,ndata
  logical :: probably_freq_scale
  character(len=message_length) :: mess
  character(len=filename_length) :: arg1,file
  character(len=1024) :: chain
  character(len=1) :: letter
  real(8), allocatable :: input(:)
  !
  call sic_ch (line,0,1,arg1,nc,.true.,error)
  if (error) return
  call sic_parse_file (arg1,' ','.dat',file)
  xcol = 1                     !a priori
  ycol = 2
  if (sic_present (2,1)) then
     call sic_ke (line,2,1,letter,nc,.false.,error)
     if (error) return
     if (letter.eq.'X') then
        call sic_i4 (line,2,2,xcol,.true.,error)
     elseif (letter.eq.'Y') then
        call sic_i4 (line,2,2,ycol,.true.,error)
     else
        mess = 'Unknown axis '//letter//' [X or Y only]'
        call class_message(seve%e,rname, mess)
        error = .true.
     endif
     if (error) return
  endif
  if (sic_present (2,3)) then
     call sic_ke (line, 2,3,letter,nl,.false.,error)
     if (error) return
     if (letter .eq. 'X') then
        call sic_i4 (line,2,4,xcol,.true.,error)
     elseif (letter .eq. 'Y') then
        call sic_i4 (line,2,4,ycol,.true.,error)
     else
        mess = 'Unknown axis '//letter//' [X or Y only]'
        call class_message(seve%e,rname, mess)
        error = .true.
     endif
     if (error) return
  endif
  ucol = max (xcol, ycol)
  ier = sic_getlun (jtmp)
  if (ier.ne.1) then
     call class_message(seve%e,rname,'No logical unit left')
     error = .true.
     return
  endif
  ier = sic_open(jtmp,file,'OLD',.false.)
  if (ier.ne.0) then
     mess = 'Cannot open file '//file(1:lenc(file))
     call class_message(seve%e,rname,mess)
     call putios('         ',ier)
     error=.true.
     call sic_frelun(jtmp)
     return
  endif
  !
  allocate (input(ucol),stat=ier)
  if (failed_allocate(rname,'input',ier,error))  return
  !
  call rzero (r,'NULL',user_function)
  ! Xcol = 0 -> pas de Xcol, on numerote en canaux
  r%head%spe%nchan = 0
  probably_freq_scale = .false.
  !
  ! Count channel number
  ndata = 0
  do while (.true.)
     read (jtmp,'(A)',end=10) chain
     if (.not.(chain(1:1) .eq. '!' .or. chain(2:2) .eq. '!')) then
        ndata =ndata+1
     endif
  enddo
10 continue
  call reallocate_obs(r,ndata,error)
  rewind(jtmp)
  !
  r%head%spe%nchan = 0
  do while (.true.)
     read (jtmp,'(A)',end=20) chain
     if (.not.(chain(1:1) .eq. '!' .or. chain(2:2) .eq. '!')) then
        r%head%spe%nchan = r%head%spe%nchan + 1
        read (chain,*,iostat=ier) (input(i),i=1,ucol)
        if (ier.ne.0) then
           call class_message(seve%e,rname,'Internal error: reading '//  &
              'chain "'//trim(chain)//'" :')
           if (ier.eq.-1) then  ! EOF
              ! 'chain' variable is probably too short
              write(mess,'(A,I3,A,I3,A)') 'No data at column #',ucol,  &
                 ', or data beyond internal limit of ',len(chain),'th character'
              call class_message(seve%e,rname,mess)
           else
              call putios('E-GREG,  ',ier)
           endif
           error=.true.
           goto 30
        endif
        if (xcol .eq. 0) then
           r%datax(r%head%spe%nchan) = r%head%spe%nchan
        elseif (r%head%spe%nchan .eq. 1) then
           ! X < 1000 : probablement un numero de canal ou une vitesse
           ! X > 1000 (MHz) : probablement une frEquence (pas de raie < 1 GHz)
           ! auquel cas il faut travailler par offset car R%DATAX est R*4 only
           if (input(xcol) .gt. 1.0d3) then
              probably_freq_scale = .true.
              r%datax(1) = 0.0
              r%head%spe%restf = input(xcol)
           else
              r%datax(r%head%spe%nchan) = input(xcol)
           endif
        else
           if (probably_freq_scale) then
              r%datax(r%head%spe%nchan) = input(xcol) - r%head%spe%restf
           else
              r%datax(r%head%spe%nchan) = input(xcol)
           endif
        endif
        r%spectre(r%head%spe%nchan) = input(ycol)
     endif
  enddo
  !
20 continue
  r%head%spe%rchan = 1.d0
  if (probably_freq_scale) then
     r%head%spe%fres = r%datax(2) - r%datax(1)
     if (abs(r%head%spe%fres - (r%datax(r%head%spe%nchan) - r%datax(r%head%spe%nchan - 1)))   &
         .gt. abs (0.0001 * r%head%spe%fres)) then
        r%head%presec(class_sec_xcoo_id) = .true.
     else
        r%head%spe%vres = -r%head%spe%fres*clight_kms/r%head%spe%restf
        r%head%presec(class_sec_xcoo_id) = .false.
     endif
     set%unitx(1) = 'F'
     set%unitx(2) = ' '
  elseif ((r%datax(1) .eq. 1.0d0) .or.   &
       &    (r%datax(1) .eq. r%head%spe%nchan)) then
     set%unitx(1) = 'C'
     set%unitx(2) = ' '
     ! r%head%spe%foff or voff = ?  What about Foff or Voff? What can newdat()
     ! do without a minimum of information?
  else
     set%unitx(1) = 'V'
     set%unitx(2) = ' '
     r%head%spe%voff = r%datax(1)  ! Because Rchan is set to 1.
     r%head%spe%vres = r%datax(2) - r%datax(1)
     if (abs(r%head%spe%vres - (r%datax(r%head%spe%nchan) - r%datax(r%head%spe%nchan - 1)))   &
          &      .gt. abs (0.0001 * r%head%spe%vres)) then
        r%head%presec(class_sec_xcoo_id) = .true.
     endif
  endif
  r%head%pos%sourc = 'GreG Data'
  r%head%xnum = -1
  r%head%gen%scan = 1
  r%head%gen%num = 1
  r%head%presec(class_sec_cal_id) = .false.  !
  r%head%presec(class_sec_gen_id) = .true.
  r%head%presec(class_sec_spe_id) = .true.
  call newdat(set,r,error)
  !
  ! Cleaning
30 continue
  close(jtmp)
  call sic_frelun(jtmp)
  deallocate(input)
  !
end subroutine class_greg_read_formatted
