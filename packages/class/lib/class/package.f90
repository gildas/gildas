!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine class_pack_set(pack)
  use gpack_def
  use class_dependencies_interfaces
  use class_interfaces, except_this=>class_pack_set
  !---------------------------------------------------------------------
  ! @ public-mandatory (because symbol is used elsewhere)
  !---------------------------------------------------------------------
  type(gpack_info_t), intent(out) :: pack
  !
  pack%name='class'
  pack%ext='.class'
  pack%depend(1:2) = (/ locwrd(ephem_pack_set), locwrd(greg_pack_set) /)
  pack%init=locwrd(class_pack_init)
  pack%clean=locwrd(class_pack_clean)
  pack%authors="S.Bardeau, J.Pety, P.Hily-Blant, S.Guilloteau"
  !
end subroutine class_pack_set
!
subroutine class_pack_init(gpack_id,error)
  use gbl_message
  use sic_def
  use class_dependencies_interfaces
  use class_interfaces, except_this=>class_pack_init
  use class_rt
  use class_setup
  !---------------------------------------------------------------------
  ! @ private-mandatory
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)    :: gpack_id
  logical,         intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='INIT'
  !
  ! One time initialization
  call classic_message_set_id(gpack_id)  ! Set library id
  call class_message_set_id(gpack_id)  ! Set library id
  !
  ! Allocate buffer memory
  call allocate_class(error)
  if (error) return
  !
  call las_setup(set,error)
  call las_variables(set,r,error)
  !
  ! Local language
  call class_languages(sas_function)  ! Must come after R/T association (in allocate_class)
  call load_map(error)
  if (error) return
  !
  ! Define Functions
  call cube_functions(error)
  if (error) return
  !
  call exec_program('sic'//backslash//'sic priority 1 las 2 analyse fit')
  ! Set defaults
  call exec_program('las'//backslash//'set default')
  !
end subroutine class_pack_init
!
subroutine class_pack_clean(error)
  use class_interfaces, except_this=>class_pack_clean
  !---------------------------------------------------------------------
  ! @ private-mandatory
  !---------------------------------------------------------------------
  logical, intent(inout) :: error
  !
  call class_exit(error)
  !
end subroutine class_pack_clean
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
