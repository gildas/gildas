!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module class_interfaces
  !---------------------------------------------------------------------
  ! CLASS interfaces, all kinds (private or public). Do not use directly
  ! out of the library.
  !---------------------------------------------------------------------
  !
  use class_interfaces_public
  use class_interfaces_private
  !
end module class_interfaces
!
module class_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! CLASS dependencies public interfaces. Do not use out of the library.
  !---------------------------------------------------------------------
  use gkernel_interfaces
  use astro_interfaces_public
  use classcore_interfaces_public
  use classfit_interfaces_public
end module class_dependencies_interfaces
!
module class_api
  !---------------------------------------------------------------------
  ! @ public
  !   CLASS public interfaces, parameters, and types. Can be used by
  ! external programs
  !---------------------------------------------------------------------
  !
  use classic_api  ! This helps to hide the libclassic to programs using
                   ! the libclass
  !
  ! use class_parameters
  use class_types
  use classcore_interfaces_public
  use class_interfaces_public
  !
end module class_api
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
