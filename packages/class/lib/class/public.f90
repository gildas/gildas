!-----------------------------------------------------------------------
! Public API for interactive or non-interactive programs linked to
! the libclass, as described in the memo "Class Data Fillers".
!
!  Fortran entry points:
!   class_write_init: initialize the libclass for writing process,
!   class_write_clean: clean the libclass after writing process,
!   class_fileout_open: initialize/reopen a Class output file,
!   class_fileout_close: close the Class output file,
!   class_obs_init: initialize an observation variable,
!   class_obs_reset: zero-ify and resize an observation,
!   class_obs_write: write an observation to the output file,
!   class_obs_clean: clean/free an observation variable.
!-----------------------------------------------------------------------
!
subroutine class_write_init(error)
  use class_dependencies_interfaces
  use class_interfaces, except_this=>class_write_init
  !---------------------------------------------------------------------
  ! @ public (available to external programs)
  !   Public procedure suited for programs linked basically to the
  ! libclass, i.e. with no Gildas specific features like interpreter or
  ! initializations or so. See demo program classdemo-telwrite.f90
  !   Perform the basic initializations of the libclass needed for
  ! the writing process (e.g. stand-alone program at the telescope).
  !   Should be done once at program startup.
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  ! Logical error flag
   !
  ! libclassic/libclass initialization
  call classic_init(error)
  if (error)  return
  call allocate_class(error)
  if (error)  return
  !
end subroutine class_write_init
!
subroutine class_write_clean(error)
  use class_interfaces, except_this=>class_write_clean
  !---------------------------------------------------------------------
  ! @ public (available to external programs)
  !   Public procedure suited for programs linked basically to the
  ! libclass, i.e. with no Gildas specific features like interpreter or
  ! initializations or so. See demo program classdemo-telwrite.f90
  !   Perform the basic cleaning of the libclass needed after the
  ! writing process (e.g. stand-alone program at the telescope).
  !  Should be done once before leaving the program
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  ! Logical error flag
  !
  call class_exit(error)
  !
end subroutine class_write_clean
!
subroutine class_fileout_open(spec,lnew,lover,lsize,lsingle,error)
  use classic_api
  use class_dependencies_interfaces
  use class_setup
  !---------------------------------------------------------------------
  ! @ public (available to external programs)
  !  Public entry point to open a new or reopen an old output file.
  !---------------------------------------------------------------------
  character(len=*),           intent(in)    :: spec     ! File name including extension
  logical,                    intent(in)    :: lnew     ! Should it be a new file or an old?
  logical,                    intent(in)    :: lover    ! If new, overwrite existing file or raise an error?
  integer(kind=entry_length), intent(in)    :: lsize    ! If new, maximum number of observations allowed
  logical,                    intent(in)    :: lsingle  ! If new, should spectra be unique?
  logical,                    intent(inout) :: error    ! Logical error flag
  !
  call classcore_fileout_open(set,spec,lnew,lover,lsize,lsingle,error)
  if (error)  return
  !
end subroutine class_fileout_open
!
subroutine class_fileout_close(error)
  use class_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ public (available to external programs)
  !  Close the output file without closing the input file if they are
  ! the same
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  ! Error status
  !
  call classcore_fileout_close(error)
  if (error)  return
  !
end subroutine class_fileout_close
!
subroutine class_obs_write(obs,error)
  use class_dependencies_interfaces
  use class_setup
  use class_types
  !---------------------------------------------------------------------
  ! @ public (available to external programs)
  !  Write the observation to the currently opened output file
  !  See demo program classdemo-telwrite.f90
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs    !
  logical,           intent(inout) :: error  ! Logical error flag
  !
  call class_write(set,obs,error,sas_function)
  ! NB: this uses Class's global setup
  ! NB: no way to use a custom 'user_function', use class dummy one instead
  !
end subroutine class_obs_write
!
subroutine class_fold_obs(obs,keepblank,error)
  use class_dependencies_interfaces
  use class_setup
  use class_types
  !----------------------------------------------------------------------
  ! @ public (available to external programs)
  !   Fold an unfolded frequency switched observation.
  !  Each phase is assumed to be given a weight and a frequency change.
  !  The frequency change need not be an integer number of channels.
  !  The number of phases is not limited.
  !-----------------------------------------------------------------------
  type(observation), intent(inout) :: obs
  logical,           intent(in)    :: keepblank  ! Keep blank channels at boundaries?
  logical,           intent(inout) :: error
  !
  call classcore_fold_obs(set,obs,keepblank,error)
  if (error)  return
  !
end subroutine class_fold_obs
!
subroutine class_modify_vdirection(head,error)
  use class_dependencies_interfaces
  use class_setup
  use class_types
  !---------------------------------------------------------------------
  ! @ public (available to external programs)
  ! Support routine for command:
  !  MODIFY VDIRECTION
  !---------------------------------------------------------------------
  type(header), intent(inout) :: head   !
  logical,      intent(inout) :: error  !
  !
  call modify_vdirection(set,head,error)
  if (error)  return
  !
end subroutine class_modify_vdirection
