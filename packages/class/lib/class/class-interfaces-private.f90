module class_interfaces_private
  interface
    subroutine cube_functions(error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Define cube-building functions
      !---------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine cube_functions
  end interface
  !
  interface
    function s_tdv(v1,v2)
      use class_rt
      !---------------------------------------------------------------------
      ! @ private
      ! CLASS internal routine
      !---------------------------------------------------------------------
      real(kind=4) :: s_tdv  !
      real(kind=8), intent(in) :: v1,v2  !
    end function s_tdv
  end interface
  !
  interface
    function d_tdv(v1,v2)
      use class_rt
      !-------------------------------------------------------------------
      ! @ private
      ! CLASS internal routine
      !-------------------------------------------------------------------
      real(kind=8) :: d_tdv  !
      real(kind=8), intent(in) :: v1,v2  !
    end function d_tdv
  end interface
  !
  interface
    function s_tpeakv(v1,v2)
      use class_rt
      !---------------------------------------------------------------------
      ! @ private
      ! CLASS internal routine
      !---------------------------------------------------------------------
      real(kind=4) :: s_tpeakv  !
      real(kind=8), intent(in) :: v1,v2  !
    end function s_tpeakv
  end interface
  !
  interface
    function d_tpeakv(v1,v2)
      use class_rt
      !-------------------------------------------------------------------
      ! @ private
      ! CLASS internal routine
      !-------------------------------------------------------------------
      real(kind=8) :: d_tpeakv  !
      real(kind=8), intent(in) :: v1,v2  !
    end function d_tpeakv
  end interface
  !
  interface
    function s_vpeakv(v1,v2)
      use class_rt
      !---------------------------------------------------------------------
      ! @ private
      ! CLASS internal routine
      !---------------------------------------------------------------------
      real(kind=4) :: s_vpeakv  !
      real(kind=8), intent(in) :: v1,v2  !
    end function s_vpeakv
  end interface
  !
  interface
    function d_vpeakv(v1,v2)
      use class_rt
      !-------------------------------------------------------------------
      ! @ private
      ! CLASS internal routine
      !-------------------------------------------------------------------
      real(kind=8) :: d_vpeakv  !
      real(kind=8), intent(in) :: v1,v2  !
    end function d_vpeakv
  end interface
  !
  interface
    function s_stdv(dummy)
      use class_rt
      !---------------------------------------------------------------------
      ! @ private
      ! CLASS internal routine
      !---------------------------------------------------------------------
      real(kind=4) :: s_stdv  !
      real(kind=8), intent(in) :: dummy  !
    end function s_stdv
  end interface
  !
  interface
    function d_stdv(dummy)
      use class_rt
      !-------------------------------------------------------------------
      ! @ private
      ! CLASS internal routine
      !-------------------------------------------------------------------
      real(kind=8) :: d_stdv  !
      real(kind=8), intent(in) :: dummy  !
    end function d_stdv
  end interface
  !
  interface
    function s_tpeak(dummy)
      use class_rt
      !---------------------------------------------------------------------
      ! @ private
      ! CLASS internal routine
      !---------------------------------------------------------------------
      real(kind=4) :: s_tpeak  !
      real(kind=8), intent(in) :: dummy  !
    end function s_tpeak
  end interface
  !
  interface
    function d_tpeak(dummy)
      use class_rt
      !-------------------------------------------------------------------
      ! @ private
      ! CLASS internal routine
      !-------------------------------------------------------------------
      real(kind=8) :: d_tpeak  !
      real(kind=8), intent(in) :: dummy  !
    end function d_tpeak
  end interface
  !
  interface
    function s_vpeak(dummy)
      use class_rt
      !---------------------------------------------------------------------
      ! @ private
      ! CLASS internal routine
      !---------------------------------------------------------------------
      real(kind=4) :: s_vpeak  !
      real(kind=8), intent(in) :: dummy  !
    end function s_vpeak
  end interface
  !
  interface
    function d_vpeak(dummy)
      use class_rt
      !-------------------------------------------------------------------
      ! @ private
      ! CLASS internal routine
      !-------------------------------------------------------------------
      real(kind=8) :: d_vpeak  !
      real(kind=8), intent(in) :: dummy  !
    end function d_vpeak
  end interface
  !
  interface
    subroutine class_greg(set,line,r,error,user_function)
      use class_types
      !---------------------------------------------------------------------
      ! @ private
      ! CLASS Support routine for command
      !    GREG Name
      ! 1   [/FORMATTED [Format]]
      ! 2   [/READ [X n] [Y m]]
      !---------------------------------------------------------------------
      type(class_setup_t), intent(inout) :: set            !
      character(len=*),    intent(in)    :: line           ! Input command line
      type(observation),   intent(inout) :: r              !
      logical,             intent(inout) :: error          ! Logical flag
      logical,             external      :: user_function  !
    end subroutine class_greg
  end interface
  !
  interface
    subroutine class_greg_write(set,line,r,fnc,nfit,fit_sec,error)
      use gildas_def
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ private
      ! Writes a table or a formatted file
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set      !
      character(len=*),    intent(in)    :: line     ! Input command line
      type(observation),   intent(in)    :: r        !
      real(kind=4),        external      :: fnc      ! Fitting function
      integer(kind=4),     intent(in)    :: nfit     ! Number of lines fitted
      integer(kind=4),     intent(in)    :: fit_sec  ! Fit section (eg class_sec_gau_id)
      logical,             intent(inout) :: error    ! Logical flag
    end subroutine class_greg_write
  end interface
  !
  interface
    subroutine class_greg_write_gdf(r,chain,fnc,nfit,error)
      use gbl_message
      use image_def
      use class_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(observation), intent(in)    :: r      !
      character(len=*),  intent(in)    :: chain  ! File name
      real(kind=4),      external      :: fnc    ! Fitting function
      integer(kind=4),   intent(in)    :: nfit   ! Number of lines fitted
      logical,           intent(inout) :: error  ! Logical error flag
    end subroutine class_greg_write_gdf
  end interface
  !
  interface
    subroutine class_greg_write_formatted(set,r,chain,fnc,nfit,fit_sec,informa,error)
      use gildas_def
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(class_setup_t), intent(in)    :: set      !
      type(observation),   intent(in)    :: r        !
      character(len=*),    intent(in)    :: chain    ! File name
      real(kind=4),        external      :: fnc      ! Fitting function
      integer(kind=4),     intent(in)    :: nfit     ! Number of lines fitted
      integer(kind=4),     intent(in)    :: fit_sec  ! Fit section (eg class_sec_gau_id)
      character(len=*),    intent(in)    :: informa  ! Input format ('' = auto)
      logical,             intent(inout) :: error    ! Logical error flag
    end subroutine class_greg_write_formatted
  end interface
  !
  interface
    subroutine sgildas(obs,x,nlines,ncols,fnc)
      use gildas_def
      use class_types
      !---------------------------------------------------------------------
      ! @ private
      !  Fills a table at the GILDAS format from the current scan and its
      ! fit results.
      !---------------------------------------------------------------------
      type(observation), intent(in)  :: obs              ! Input spectrum
      integer(kind=4),   intent(in)  :: nlines           ! Number of lines in table
      integer(kind=4),   intent(in)  :: ncols            ! Number of columns in table
      real(kind=4),      intent(out) :: x(nlines,ncols)  ! Table to be filled
      real(kind=4), external         :: fnc              ! Fitting function
    end subroutine sgildas
  end interface
  !
  interface
    subroutine class_greg_read_formatted(set,line,r,error,user_function)
      use gildas_def
      use gbl_constant
      use phys_const
      use gbl_message
      use class_types
      !---------------------------------------------------------------------
      ! @ private
      ! CLASS Internal routine
      ! Read a formatted file
      !---------------------------------------------------------------------
      type(class_setup_t), intent(inout) :: set            !
      character(len=*),    intent(in)    :: line           ! Input command line
      type(observation),   intent(inout) :: r              !
      logical,             intent(inout) :: error          ! Logical flag
      logical,             external      :: user_function  !
    end subroutine class_greg_read_formatted
  end interface
  !
  interface
    subroutine allocate_class(error)
      !---------------------------------------------------------------------
      ! @ private
      ! Allocate all Class buffers
      !---------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine allocate_class
  end interface
  !
  interface
    subroutine allocate_RT(error)
      use class_rt
      !---------------------------------------------------------------------
      ! @ private
      ! Allocate R and T buffers
      !---------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine allocate_RT
  end interface
  !
  interface
    subroutine deallocate_class(error)
      !---------------------------------------------------------------------
      ! @ private
      ! Deallocate all Class buffers
      !---------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine deallocate_class
  end interface
  !
  interface
    subroutine deallocate_RT(error)
      use class_rt
      !---------------------------------------------------------------------
      ! @ private
      ! Deallocate R and T buffers
      !---------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine deallocate_RT
  end interface
  !
  interface
    subroutine class_exit(error)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine class_exit
  end interface
  !
  interface
    subroutine class_pack_init(gpack_id,error)
      use gbl_message
      use sic_def
      use class_rt
      use class_setup
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: gpack_id
      logical,         intent(inout) :: error
    end subroutine class_pack_init
  end interface
  !
  interface
    subroutine class_pack_clean(error)
      !---------------------------------------------------------------------
      ! @ private-mandatory
      !---------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine class_pack_clean
  end interface
  !
  interface
    subroutine pyclass_rx_val(ival,iunitin,oval,ounitin,error)
      use gbl_message
      use class_rt
      !---------------------------------------------------------------------
      ! @ private
      !
      ! ---
      ! Entry point which can be called from Python. In particular the
      ! 'observation' is shared through a module.
      !---------------------------------------------------------------------
      real(kind=8),     intent(in)  :: ival     ! Input value
      character(len=*), intent(in)  :: iunitin  ! Input unit
      real(kind=8),     intent(out) :: oval     ! Output value
      character(len=*), intent(in)  :: ounitin  ! Output unit
      logical,          intent(out) :: error    ! Logical error flag
    end subroutine pyclass_rx_val
  end interface
  !
  interface
    subroutine pyclass_rx_minmax(unitin,mini,maxi,error)
      use gbl_message
      use class_rt
      use class_setup
      !---------------------------------------------------------------------
      ! @ private
      !
      ! ---
      ! Entry point which can be called from Python. In particular the
      ! 'observation' is shared through a module.
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: unitin  ! Unit for returning values. Blank means current unit
      real(kind=8),     intent(out) :: mini    !
      real(kind=8),     intent(out) :: maxi    !
      logical,          intent(out) :: error   ! Logical error flag
    end subroutine pyclass_rx_minmax
  end interface
  !
  interface
    subroutine pyclass_px_minmax(unitin,mini,maxi,error)
      use class_setup
      !---------------------------------------------------------------------
      ! @ private
      ! Return the X range of the plot
      ! NB: the subroutine returns actually the SET MODE X range, not
      ! the effective PLOT range (they are the same AFTER the command PLOT).
      ! But the command ANA\DRAW has the same bug-feature, and since the
      ! cursor is used by Weeds to identify lines, we are consistent...
      ! ---
      ! Entry point which can be called from Python. In particular the
      ! 'class_setup_t' is shared through a module.
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: unitin  ! Unit for returning values. Blank means current unit
      real(kind=8),     intent(out) :: mini    !
      real(kind=8),     intent(out) :: maxi    !
      logical,          intent(out) :: error   ! Logical error flag
    end subroutine pyclass_px_minmax
  end interface
  !
  interface
    subroutine class_languages(user_function)
      !---------------------------------------------------------------------
      ! @ private
      ! LAS initialization routine
      ! SIC is loaded with LAS and ANALYSE languages, plot library is
      ! initialized, and SIC variables are defined.
      !---------------------------------------------------------------------
      logical, external :: user_function
    end subroutine class_languages
  end interface
  !
  interface
    function class_error()
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for the error status of the library. Does nothing
      ! here.
      !---------------------------------------------------------------------
      logical :: class_error  ! Function value on return
    end function class_error
  end interface
  !
  interface
    subroutine run_sas(line,comm,error)
      use class_rt
      !---------------------------------------------------------------------
      ! @ private-mandatory
      ! Support routine for LAS\ language
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line   ! Command line
      character(len=*), intent(in)  :: comm   ! Command name
      logical,          intent(out) :: error  ! Error status
    end subroutine run_sas
  end interface
  !
  interface
    subroutine run_ana(line,comm,error)
      use class_rt
      !---------------------------------------------------------------------
      ! @ private-mandatory
      ! Support routine for ANALYSE\ language
      !---------------------------------------------------------------------
      character(len=*), intent(inout) :: line   ! Command line
      character(len=*), intent(in)    :: comm   ! Command name
      logical,          intent(out)   :: error  ! Error status
    end subroutine run_ana
  end interface
  !
  interface
    subroutine run_fit(line,comm,error)
      use class_rt
      !---------------------------------------------------------------------
      ! @ private-mandatory
      ! Support routine for FIT\ language
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line   ! Command line
      character(len=*), intent(in)  :: comm   ! Command name
      logical,          intent(out) :: error  ! Error status
    end subroutine run_fit
  end interface
  !
  interface
    subroutine run_dec(line,comm,error)
      use class_rt
      !---------------------------------------------------------------------
      ! @ private-mandatory
      ! Support routine for DSB2SSB\ language
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line   ! Command line
      character(len=*), intent(in)  :: comm   ! Command name
      logical,          intent(out) :: error  ! Error status
    end subroutine run_dec
  end interface
  !
  interface
    subroutine run_exp(line,comm,error)
      use class_rt
      !---------------------------------------------------------------------
      ! @ private-mandatory
      ! Support routine for EXPERIMENTAL\ language
      !---------------------------------------------------------------------
      character(len=*), intent(in)  :: line   ! Command line
      character(len=*), intent(in)  :: comm   ! Command name
      logical,          intent(out) :: error  ! Error status
    end subroutine run_exp
  end interface
  !
  interface
    subroutine class_swap(line,error,user_function)
      !----------------------------------------------------------------------
      ! @ private
      !  Exchange R and T memories
      !----------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      logical,          intent(inout) :: error
      logical,          external      :: user_function
    end subroutine class_swap
  end interface
  !
  interface
    subroutine swaprt(rname,error,user_function)
      use gbl_message
      use class_rt
      use class_setup
      !----------------------------------------------------------------------
      ! @ private
      !  Exchange R and T memories
      !----------------------------------------------------------------------
      character(len=*), intent(in)    :: rname
      logical,          intent(inout) :: error
      logical,          external      :: user_function
    end subroutine swaprt
  end interface
  !
  interface
    subroutine copyrt(user_function,key)
      use class_rt
      !----------------------------------------------------------------------
      ! @ private
      ! Copy R memory to T memory
      !----------------------------------------------------------------------
      logical,          external   :: user_function  !
      character(len=*), intent(in) :: key            !
    end subroutine copyrt
  end interface
  !
  interface
    subroutine copy2r(obs,error)
      use class_rt
      use class_setup
      !-------------------------------------------------------------------
      ! @ private
      ! Copy the observation into R memory
      !-------------------------------------------------------------------
      type(observation), intent(in)  :: obs    ! Current observation
      logical,           intent(out) :: error  ! Error flag
    end subroutine copy2r
  end interface
  !
end module class_interfaces_private
