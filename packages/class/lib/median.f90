subroutine class_median(set,line,r,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_median
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  !  Support routine for command
  !   EXP\MEDIAN [Width] [Sampling]
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  character(len=*),    intent(in)    :: line   !
  type(observation),   intent(inout) :: r      !
  logical,             intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='MEDIAN'
  real(kind=8) :: width,sampling
  !
  if (r%head%xnum.eq.0) then
    call class_message(seve%e,rname,'No R spectrum in memory')
    error = .true.
    return
  endif
  !
  width = 20.d0  ! [MHz]
  call sic_r8(line,0,1,width,.false.,error)
  if (error)  return
  !
  sampling = width/2.d0
  call sic_r8(line,0,2,sampling,.false.,error)
  if (error)  return
  !
  call exp_medians(r,width,sampling,error)
  if (error)  return
  !
  call newdat_assoc(set,r,error)
  if (error)  return
  !
end subroutine class_median
!
subroutine exp_medians(obs,width,sampling,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>exp_medians
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs       !
  real(kind=8),      intent(in)    :: width     ! [MHz]
  real(kind=8),      intent(in)    :: sampling  ! [MHz]
  logical,           intent(inout) :: error     !
  ! Local
  character(len=*), parameter :: rname='MEDIAN'
  integer(kind=4) :: ier
  real(kind=4), allocatable :: tmp(:)
  real(kind=8) :: w
  type(class_assoc_sub_t) :: array
  !
  ! Sanity checks
  w = width
  if (w.le.0.d0) then
    call class_message(seve%e,rname,'Width must be positive')
    error = .true.
    return
  endif
  if (w.lt.2.d0*obs%head%spe%fres) then
    call class_message(seve%e,rname,'Width must be at least 2 times R%HEAD%SPE%FRES')
    error = .true.
    return
  endif
  if (w.gt.obs%head%spe%nchan*abs(obs%head%spe%fres)) then
    call class_message(seve%w,rname,'Width truncated to the spectrum bandwidth')
    w = obs%head%spe%nchan*abs(obs%head%spe%fres)
  endif
  if (sampling.lt.abs(obs%head%spe%fres)) then
    call class_message(seve%e,rname,'Sampling must be at least R%HEAD%SPE%FRES')
    error = .true.
    return
  endif
  !
  ! Compute the median
  call exp_median(obs,obs%spectre,w,sampling,'BASELINE',1.d0,error)
  if (error)  return
  !
  ! Compute the RMS from the Median Absolute Deviation (MAD)
  allocate(tmp(obs%head%spe%nchan),stat=ier)
  if (failed_allocate(rname,'tmp buffer',ier,error))  return
  !
  if (.not.class_assoc_exists(obs,'BASELINE',array)) then
    call class_message(seve%e,rname,'Internal error: BASELINE array not found')
    error = .true.
    return
  endif
  !
  where (obs%spectre(1:obs%head%spe%nchan).eq.obs%head%spe%bad .or.  &
         array%r4(:,1).eq.array%badr4)
    tmp(:) = obs%head%spe%bad
  elsewhere
    tmp(:) = abs(obs%spectre(1:obs%head%spe%nchan) - array%r4(:,1))
  end where
  ! For normally distributed data K is taken to be 1/phi^-1(3/4) =~ 1.4826,
  ! where phi^-1 is the inverse of the cumulative distribution function for
  ! the standard normal distribution, i.e., the quantile function.
  call exp_median(obs,tmp,w,sampling,'RMS',1.4826d0,error)
  if (error)  return
  !
  deallocate(tmp)
  !
end subroutine exp_medians
!
subroutine exp_median(obs,data,width,sampling,aaname,factor,error)
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>exp_median
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Compute the median of the input 'data', save the result in an
  ! associated array
  !---------------------------------------------------------------------
  type(observation), intent(inout) :: obs       !
  real(kind=4),      intent(in)    :: data(*)   !
  real(kind=8),      intent(in)    :: width     ! [MHz]
  real(kind=8),      intent(in)    :: sampling  ! [MHz]
  character(len=*),  intent(in)    :: aaname    !
  real(kind=8),      intent(in)    :: factor    ! Extra factor to be used
  logical,           intent(inout) :: error     !
  ! Local
  integer(kind=4) :: isub,ifirst,imid,iprev,ilast,ichan
  integer(kind=size_length) :: wc
  real(kind=8) :: shift,middle,a,b
  real(kind=4) :: y1,y2,cbad
  real(kind=4), pointer :: aarray(:)
  logical :: dobad
  !
  cbad = obs%head%spe%bad
  !
  wc = nint(width/abs(obs%head%spe%fres))
  shift = sampling/abs(obs%head%spe%fres)  ! [chan] One point every 'shift', interpolate between
  !
  if (.not.class_assoc_exists(obs,aaname,aarray)) then
    call class_assoc_add(obs,aaname,'',fmt_r4,0,cbad,aarray,error)
    if (error)  return
  endif
  aarray(:) = 0.d0 ! cbad
  !
  ! Dummy initializations for compiler warnings
  iprev = 0
  imid = 0
  y1 = cbad
  dobad = .true.
  !
  isub = 0
  do
    isub = isub+1
    if (isub.eq.1) then  ! Left boundary condition
      middle = real(wc,kind=8)/2.d0
      ifirst = 1
      iprev = 1
      imid = nint(middle)
      y2 = cbad  ! Not modified if no valid data
      call gr4_median(data(1:wc),wc,cbad,0.,y2,error)
      if (error)  return
      y1 = y2  ! Flat (no interpolation)
    else
      middle = middle+shift
      imid = nint(middle)
      ifirst = imid-wc/2
      ilast = ifirst+wc-1  ! Constant width
      if (ilast.gt.obs%head%spe%nchan)  exit
      y2 = cbad  ! Not modified if no valid data
      call gr4_median(data(ifirst:ilast),wc,cbad,0.,y2,error)
      if (error)  return
    endif
    !
    ! Fill left part of the chunk with interpolated values
    call interpolation_factors(iprev,imid,y1,y2,cbad,a,b,dobad)
    do ichan=iprev,imid
      if (dobad) then
        aarray(ichan) = cbad
      elseif (data(ichan).eq.cbad) then
        aarray(ichan) = cbad
      else
        aarray(ichan) = factor * (a*ichan+b)
      endif
    enddo
    !
    ! Prepare next loop
    iprev = imid
    y1 = y2
    !
  enddo
  !
  ! Right boundary condition: flat fill right part of last chunk
  do ichan=iprev,obs%head%spe%nchan
    if (dobad) then
      aarray(ichan) = cbad
    elseif (data(ichan).eq.cbad) then
      aarray(ichan) = cbad
    else
      aarray(ichan) = factor * y2
    endif
  enddo
  !
contains
  subroutine interpolation_factors(x1,x2,y1,y2,cbad,a,b,dobad)
    integer(kind=4), intent(in)  :: x1,x2
    real(kind=4),    intent(in)  :: y1,y2
    real(kind=4),    intent(in)  :: cbad
    real(kind=8),    intent(out) :: a,b
    logical,         intent(out) :: dobad
    !
    if (y1.ne.cbad .and. y2.ne.cbad) then
      dobad = .false.
      a = (y2-y1)/(x2-x1)
      b = y1-a*x1
    elseif (y1.eq.cbad .and. y2.eq.cbad) then
      dobad = .true.
      a = 0.d0
      b = 0.d0
    elseif (y1.eq.cbad) then
      dobad = .false.
      a = 0.d0
      b = y2
    else  ! if (y2.eq.cbad) then
      dobad = .false.
      a = 0.d0
      b = y1
    endif
    !
  end subroutine interpolation_factors
  !
end subroutine exp_median
