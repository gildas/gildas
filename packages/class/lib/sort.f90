!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine set_sort(set,line,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>set_sort
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! SET SORT NONE
  ! SET SORT Para1
  !---------------------------------------------------------------------
  type(class_setup_t), intent(inout) :: set    !
  character(len=*),    intent(in)    :: line   ! Input command line
  logical,             intent(inout) :: error  ! Error flag
  ! Local
  integer(kind=4), parameter :: msort=15
  character(len=12) :: csort(msort),asort
  character(len=12) :: argum
  integer(kind=4) :: nc,n
  data csort /'NONE', &
    'NUMBER','VERSION','SCAN','LAMBDA','BETA','SUBSCAN','OBSERVED', &
    'KIND','QUALITY','BLOC','SOURCE','LINE','TELESCOPE','TOC'/
  !
  call sic_ke (line,0,2,argum,nc,.true.,error)
  if (error) return
  call sic_ambigs('SET SORT',argum,asort,n,csort,msort,error)
  if (error) return
  if (asort.eq.'NONE') then
    set%do_sort = .false.
  else
    set%do_sort = .false.
    nc = sic_narg(0)-1
    if (nc.gt.1) then
      call class_message(seve%e,'SET SORT','Too many sort keys')
      error = .true.
      return
    endif
    set%do_sort = .true.
  endif
  set%sort_name = asort
  !
end subroutine set_sort
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine quicksort(set,x,n,gtt,gte,error)
  use gbl_message
  use classic_api
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Sorting program that uses a quicksort algorithm. Apply for an input
  ! array of integer values, which are SORTED
  !---------------------------------------------------------------------
  type(class_setup_t),        intent(in)    :: set    !
  integer(kind=entry_length), intent(in)    :: n      ! Length of array
  integer(kind=entry_length), intent(inout) :: x(n)   ! Index array to be sorted
  logical,                    external      :: gtt    ! Greater than
  logical,                    external      :: gte    ! Greater than or equal
  logical,                    intent(out)   :: error  ! Error status
  ! Local
  character(len=*), parameter :: rname='SORT'
  integer(kind=4), parameter :: maxstack=1000
  integer(kind=4), parameter :: nstop=15
  integer(kind=entry_length) :: i,j,k,l,r,l1,r1,m,sp
  integer(kind=entry_length) :: lstack(maxstack),rstack(maxstack)
  integer(kind=entry_length) :: temp,key
  logical :: mgtl,lgtr,rgtm
  character(len=message_length) :: mess
  !
  if (n.le.nstop) goto 50
  sp = 0
  sp = sp + 1
  lstack(sp) = 1
  rstack(sp) = n
  !
  ! Sort a subrecord off the stack
  ! Set KEY = median of X(L), X(M), X(R)
  ! No! This is not reasonable, as systematic very inequal partitioning will
  ! occur in some cases (especially for nearly already sorted files)
  ! To fix this problem, I found (but I cannot prove it) that it is best to
  ! select the estimation of the median value from intermediate records. P.V.
1 l = lstack(sp)
  r = rstack(sp)
  sp = sp - 1
  m = (l + r) / 2
  l1=(2*l+r)/3
  r1=(l+2*r)/3
  !
  !   MGTL = X(M) .GT. X(L)
  !   RGTM = X(R) .GT. X(M)
  !
  mgtl = gtt (x(m),x(l))
  rgtm = gtt (x(r),x(m))
  !
  ! Algorithm to select the median key. The original one from MONGO
  ! was completely wrong. P. Valiron, 24-Jan-84 .
  !
  !		   	MGTL	RGTM	LGTR	MGTL.EQV.LGTR	MEDIAN_KEY
  !
  !	KL < KM < KR	T	T	*	*		KM
  !	KL > KM > KR	F	F	*	*		KM
  !
  !	KL < KM > KR	T	F	F	F		KR
  !	KL < KM > KR	T	F	T	T		KL
  !
  !	KL > KM < KR	F	T	F	T		KL
  !	KL > KM < KR	F	T	T	F		KR
  !
  if (mgtl .eqv. rgtm) then
     key = x(m)
  else
     !         LGTR = X(L) .GT. X(R)
     lgtr = gtt (x(l),x(r))
     if (mgtl .eqv. lgtr) then
        key = x(l)
     else
        key = x(r)
     endif
  endif
  i = l
  j = r
  !
  ! Find a big record on the left
10 if (gte(x(i),key)) goto 11
  i = i + 1
  goto 10
11 continue
  ! Find a small record on the right
  !!20 if (gtt(key,x(j))) goto 21  ! No, that was WRONG...
20 if (gte(key,x(j))) goto 21
  if (j.eq.l) call gte_p(key,x(j))
  j = j - 1
  goto 20
21 continue
  if (i.ge.j) goto 2
  !
  ! Exchange records
  temp = x(i)
  x(i) = x(j)
  x(j) = temp
  i = i + 1
  j = j - 1
  goto 10
  !
  ! Subfile is partitioned into two halves, left .le. right
  ! Push the two halves on the stack
2 continue
  if (j-l+1 .gt. nstop) then
     sp = sp + 1
     if (sp.gt.maxstack) then
        write(mess,*) 'Stack overflow ',sp
        call class_message(seve%e,rname,mess)
        error = .true.
        return
     endif
     lstack(sp) = l
     rstack(sp) = j
  endif
  if (r-j .gt. nstop) then
     sp = sp + 1
     if (sp.gt.maxstack) then
        write(mess,*) 'Stack overflow ',sp
        call class_message(seve%e,rname,mess)
        error = .true.
        return
     endif
     lstack(sp) = j+1
     rstack(sp) = r
  endif
  !
  ! Anything left to process?
  if (sp.gt.0) goto 1
  !
50 continue
  !
  do j = n-1,1,-1
     k = j
     do i = j+1,n
        if (gtt(x(i),x(j))) exit
        k = i
     enddo
     if (k.eq.j) cycle
     temp = x(j)
     do i = j+1,k
        x(i-1) = x(i)
     enddo
     x(k) = temp
  enddo
  !
contains
  subroutine gte_p (m,l)
    use class_index
    use gbl_message
    !---------------------------------------------------------------------
    ! TRUE if observation m is greater than observation l
    ! Sorting arrays:
    !    %dobs
    !    %scan
    !    %num
    !    %ver
    !---------------------------------------------------------------------
    integer(kind=entry_length), intent(in) :: m  ! Observation number
    integer(kind=entry_length), intent(in) :: l  ! Observation number
    ! Local
    logical :: gte_i
    character(len=12) :: ksort
    !
    ksort = set%sort_name
    Print *,'Final sort ',ksort
    select case (ksort)
    case ('BLOC')
      gte_i = ix%bloc(m).ge.ix%bloc(l)
    case ('NUMBER')
      gte_i = ix%num(m).ge.ix%num(l)
    case ('VERSION')
      gte_i = abs(ix%ver(m)).ge.abs(ix%ver(l))
    case ('OBSERVED')
      gte_i = ix%dobs(m).ge.ix%dobs(l)
    case ('LAMBDA')
      gte_i = ix%off1(m).ge.ix%off1(l)
    case ('BETA')
      gte_i = ix%off2(m).ge.ix%off2(l)
    case ('SCAN')
      gte_i = ix%scan(m).ge.ix%scan(l)
    case ('SUBSCAN')
      gte_i = ix%subscan(m).ge.ix%subscan(l)
    case ('KIND')
      gte_i = ix%kind(m).ge.ix%kind(l)
    case ('QUALITY')
      gte_i = ix%qual(m).ge.ix%qual(l)
    ! case ('TYPE')
    !   gte_i = ix%type(m).ge.ix%type(l)
    ! case ('REDUCED')
    !   gte_i = ix%dred(m).ge.ix%dred(l)
    case ('SOURCE')
      gte_i = ix%csour(m).ge.ix%csour(l)
    case ('LINE')
      gte_i = ix%cline(m).ge.ix%cline(l)
    case ('TELESCOPE')
      gte_i = ix%ctele(m).ge.ix%ctele(l)
    ! case ('ANGLE')
    !   gte_i = ix%posa(m).ge.ix%posa(l)
    case default
      call class_message(seve%w,'GTE_P','There is sort of a problem')
    end select
    if (gte_i) then
      print *,m,ix%scan(m),' >= ',ix%scan(l),l
      print *,m,ix%subscan(m),' >= ',ix%subscan(l),l
    else
      print *,m,ix%scan(m),' < ',ix%scan(l),l
      print *,m,ix%subscan(m),' >= ',ix%subscan(l),l
    endif
  end subroutine gte_p
  !
end subroutine quicksort
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! ********************************************************************
!
! IX index Greater than: TRUE if observation m is greater than observation l
!
! ********************************************************************
!
logical function ix_csour_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_csour_gt = ix%csour(m).gt.ix%csour(l)
end function ix_csour_gt
!
logical function ix_cline_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_cline_gt = ix%cline(m).gt.ix%cline(l)
end function ix_cline_gt
!
logical function ix_ctele_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_ctele_gt = ix%ctele(m).gt.ix%ctele(l)
end function ix_ctele_gt
!
logical function ix_bloc_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_bloc_gt = ix%bloc(m).gt.ix%bloc(l)
end function ix_bloc_gt
!
logical function ix_num_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_num_gt = ix%num(m).gt.ix%num(l)
end function ix_num_gt
!
logical function ix_ver_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_ver_gt = ix%ver(m).gt.ix%ver(l)
end function ix_ver_gt
!
logical function ix_dobs_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  ! Sorting arrays (by precedence order):
  !    %dobs
  !    %scan
  !    %num
  !    %ver
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_dobs_gt = .false.
  if (ix%dobs(m).gt.ix%dobs(l)) then
     ix_dobs_gt=.true.
  else if (ix%dobs(m).eq.ix%dobs(l)) then
     if (ix%scan(m) .gt. ix%scan(l)) then
        ix_dobs_gt = .true.
     elseif (ix%scan(m) .eq. ix%scan(l)) then
        if (ix%num(m) .gt. ix%num(l)) then
           ix_dobs_gt = .true.
        elseif  (ix%num(m) .eq. ix%num(l)) then
           if (ix%ver(m) .gt. ix%ver(l)) ix_dobs_gt = .true.
        endif
     endif
  endif
end function ix_dobs_gt
!
logical function ix_off1_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_off1_gt = ix%off1(m).gt.ix%off1(l)
end function ix_off1_gt
!
logical function ix_off2_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_off2_gt = ix%off2(m).gt.ix%off2(l)
end function ix_off2_gt
!
logical function ix_scan_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_scan_gt = ix%scan(m).gt.ix%scan(l)
end function ix_scan_gt
!
logical function ix_subscan_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_subscan_gt = ix%subscan(m).gt.ix%subscan(l)
end function ix_subscan_gt
!
logical function ix_kind_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_kind_gt = ix%kind(m).gt.ix%kind(l)
end function ix_kind_gt
!
logical function ix_qual_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_qual_gt = ix%qual(m).gt.ix%qual(l)
end function ix_qual_gt
!
logical function ix_toc_default_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  ! Local
  character(len=36) :: ch1,ch2
  !
  ch1 = ix%csour(m)//ix%cline(m)//ix%ctele(m)
  ch2 = ix%csour(l)//ix%cline(l)//ix%ctele(l)
  ix_toc_default_gt = ch1.gt.ch2
end function ix_toc_default_gt
!
! ********************************************************************
!
! IX index Greater Equal: TRUE if observation m is greater or equal than observation l
!
! ********************************************************************
!
logical function ix_csour_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_csour_ge = ix%csour(m).ge.ix%csour(l)
end function ix_csour_ge
!
logical function ix_cline_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_cline_ge = ix%cline(m).ge.ix%cline(l)
end function ix_cline_ge
!
logical function ix_ctele_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_ctele_ge = ix%ctele(m).ge.ix%ctele(l)
end function ix_ctele_ge
!
logical function ix_bloc_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_bloc_ge = ix%bloc(m).ge.ix%bloc(l)
end function ix_bloc_ge
!
logical function ix_num_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_num_ge = ix%num(m).ge.ix%num(l)
end function ix_num_ge
!
logical function ix_ver_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_ver_ge = ix%ver(m).ge.ix%ver(l)
end function ix_ver_ge
!
logical function ix_dobs_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  ! Sorting arrays (by precedence order):
  !    %dobs
  !    %scan
  !    %num
  !    %ver
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_dobs_ge = .false.
  if (ix%dobs(m).gt.ix%dobs(l)) then
     ix_dobs_ge=.true.
  else if (ix%dobs(m).eq.ix%dobs(l)) then
     if (ix%scan(m) .gt. ix%scan(l)) then
        ix_dobs_ge = .true.
     elseif (ix%scan(m) .eq. ix%scan(l)) then
        if (ix%num(m) .gt. ix%num(l)) then
           ix_dobs_ge = .true.
        elseif  (ix%num(m) .eq. ix%num(l)) then
           if (ix%ver(m) .ge. ix%ver(l)) ix_dobs_ge = .true.
        endif
     endif
  endif
end function ix_dobs_ge
!
logical function ix_off1_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_off1_ge = ix%off1(m).ge.ix%off1(l)
end function ix_off1_ge
!
logical function ix_off2_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_off2_ge = ix%off2(m).ge.ix%off2(l)
end function ix_off2_ge
!
logical function ix_scan_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_scan_ge = ix%scan(m).ge.ix%scan(l)
end function ix_scan_ge
!
logical function ix_subscan_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_subscan_ge = ix%subscan(m).ge.ix%subscan(l)
end function ix_subscan_ge
!
logical function ix_kind_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_kind_ge = ix%kind(m).ge.ix%kind(l)
end function ix_kind_ge
!
logical function ix_qual_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_qual_ge = ix%qual(m).ge.ix%qual(l)
end function ix_qual_ge
!
logical function ix_toc_default_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  ! Local
  character(len=36) :: ch1,ch2
  !
  ch1 = ix%csour(m)//ix%cline(m)//ix%ctele(m)
  ch2 = ix%csour(l)//ix%cline(l)//ix%ctele(l)
  ix_toc_default_ge = ch1.ge.ch2
end function ix_toc_default_ge
!
! ********************************************************************
!
! IX index Equal: TRUE if observation m is equal to observation l
!
! ********************************************************************
!
logical function ix_csour_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_csour_eq = ix%csour(m).eq.ix%csour(l)
end function ix_csour_eq
!
logical function ix_cline_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_cline_eq = ix%cline(m).eq.ix%cline(l)
end function ix_cline_eq
!
logical function ix_ctele_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_ctele_eq = ix%ctele(m).eq.ix%ctele(l)
end function ix_ctele_eq
!
logical function ix_bloc_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_bloc_eq = ix%bloc(m).eq.ix%bloc(l)
end function ix_bloc_eq
!
logical function ix_num_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_num_eq = ix%num(m).eq.ix%num(l)
end function ix_num_eq
!
logical function ix_ver_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_ver_eq = ix%ver(m).eq.ix%ver(l)
end function ix_ver_eq
!
logical function ix_dobs_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_dobs_eq = ix%dobs(m).eq.ix%dobs(l)
end function ix_dobs_eq
!
logical function ix_off1_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_off1_eq = ix%off1(m).eq.ix%off1(l)
end function ix_off1_eq
!
logical function ix_off2_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_off2_eq = ix%off2(m).eq.ix%off2(l)
end function ix_off2_eq
!
logical function ix_scan_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_scan_eq = ix%scan(m).eq.ix%scan(l)
end function ix_scan_eq
!
logical function ix_subscan_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_subscan_eq = ix%subscan(m).eq.ix%subscan(l)
end function ix_subscan_eq
!
logical function ix_kind_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_kind_eq = ix%kind(m).eq.ix%kind(l)
end function ix_kind_eq
!
logical function ix_qual_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  ix_qual_eq = ix%qual(m).eq.ix%qual(l)
end function ix_qual_eq
!
logical function ix_toc_default_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  ! Local
  character(len=36) :: ch1,ch2
  !
  ch1 = ix%csour(m)//ix%cline(m)//ix%ctele(m)
  ch2 = ix%csour(l)//ix%cline(l)//ix%ctele(l)
  ix_toc_default_eq = ch1.eq.ch2
end function ix_toc_default_eq
!
logical function ix_toc_date_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  ! Local
  character(len=36) :: ch1,ch2
  !
  ch1 = ix%csour(m)//ix%cline(m)//ix%ctele(m)
  ch2 = ix%csour(l)//ix%cline(l)//ix%ctele(l)
  ix_toc_date_eq = (ch1.eq.ch2) .and. (ix%dobs(m).eq.ix%dobs(l))
end function ix_toc_date_eq
!
logical function ix_toc_scan_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  ! Local
  character(len=36) :: ch1,ch2
  !
  ch1 = ix%csour(m)//ix%cline(m)//ix%ctele(m)
  ch2 = ix%csour(l)//ix%cline(l)//ix%ctele(l)
  ix_toc_scan_eq = (ch1.eq.ch2) .and. (ix%dobs(m).eq.ix%dobs(l)) .and.(ix%scan(m).eq.ix%scan(l))
end function ix_toc_scan_eq
!
! ********************************************************************
!
! CX index Greater than
!
! ********************************************************************
!
logical function cx_csour_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  ! TRUE if observation m is greater than observation l
  ! Comparison is done according to one keyword
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_csour_gt = cx%csour(m).gt.cx%csour(l)
end function cx_csour_gt
!
logical function cx_cline_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  ! TRUE if observation m is greater than observation l
  ! Comparison is done according to one keyword
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_cline_gt = cx%cline(m).gt.cx%cline(l)
end function cx_cline_gt
!
logical function cx_ctele_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  ! TRUE if observation m is greater than observation l
  ! Comparison is done according to one keyword
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_ctele_gt = cx%ctele(m).gt.cx%ctele(l)
end function cx_ctele_gt
!
logical function cx_bloc_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_bloc_gt = cx%bloc(m).gt.cx%bloc(l)
end function cx_bloc_gt
!
logical function cx_num_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_num_gt = cx%num(m).gt.cx%num(l)
end function cx_num_gt
!
logical function cx_ver_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_ver_gt = cx%ver(m).gt.cx%ver(l)
end function cx_ver_gt
!
logical function cx_dobs_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_dobs_gt = cx%dobs(m).gt.cx%dobs(l)
end function cx_dobs_gt
!
logical function cx_off1_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_off1_gt = cx%off1(m).gt.cx%off1(l)
end function cx_off1_gt
!
logical function cx_off2_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_off2_gt = cx%off2(m).gt.cx%off2(l)
end function cx_off2_gt
!
logical function cx_scan_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_scan_gt = cx%scan(m).gt.cx%scan(l)
end function cx_scan_gt
!
logical function cx_subscan_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_subscan_gt = cx%subscan(m).gt.cx%subscan(l)
end function cx_subscan_gt
!
logical function cx_kind_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_kind_gt = cx%kind(m).gt.cx%kind(l)
end function cx_kind_gt
!
logical function cx_qual_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_qual_gt = cx%qual(m).gt.cx%qual(l)
end function cx_qual_gt
!
logical function cx_toc_default_gt(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  ! Local
  character(len=36) :: ch1,ch2
  !
  ch1 = cx%csour(m)//cx%cline(m)//cx%ctele(m)
  ch2 = cx%csour(l)//cx%cline(l)//cx%ctele(l)
  cx_toc_default_gt = ch1.gt.ch2
end function cx_toc_default_gt
!
! ********************************************************************
!
! CX index Greater Equal
!
! ********************************************************************
!
logical function cx_csour_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  ! TRUE if observation m is greater than observation l
  ! Comparison is done according to one keyword
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_csour_ge = cx%csour(m).ge.cx%csour(l)
end function cx_csour_ge
!
logical function cx_cline_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  ! TRUE if observation m is greater than observation l
  ! Comparison is done according to one keyword
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_cline_ge = cx%cline(m).ge.cx%cline(l)
end function cx_cline_ge
!
logical function cx_ctele_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  ! TRUE if observation m is greater than observation l
  ! Comparison is done according to one keyword
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_ctele_ge = cx%ctele(m).ge.cx%ctele(l)
end function cx_ctele_ge
!
logical function cx_bloc_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_bloc_ge = cx%bloc(m).ge.cx%bloc(l)
end function cx_bloc_ge
!
logical function cx_num_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_num_ge = cx%num(m).ge.cx%num(l)
end function cx_num_ge
!
logical function cx_ver_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_ver_ge = cx%ver(m).ge.cx%ver(l)
end function cx_ver_ge
!
logical function cx_dobs_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_dobs_ge = cx%dobs(m).ge.cx%dobs(l)
end function cx_dobs_ge
!
logical function cx_off1_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_off1_ge = cx%off1(m).ge.cx%off1(l)
end function cx_off1_ge
!
logical function cx_off2_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_off2_ge = cx%off2(m).ge.cx%off2(l)
end function cx_off2_ge
!
logical function cx_scan_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_scan_ge = cx%scan(m).ge.cx%scan(l)
end function cx_scan_ge
!
logical function cx_subscan_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_subscan_ge = cx%subscan(m).ge.cx%subscan(l)
end function cx_subscan_ge
!
logical function cx_kind_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_kind_ge = cx%kind(m).ge.cx%kind(l)
end function cx_kind_ge
!
logical function cx_qual_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_qual_ge = cx%qual(m).ge.cx%qual(l)
end function cx_qual_ge
!
logical function cx_toc_default_ge(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  ! Local
  character(len=36) :: ch1,ch2
  !
  ch1 = cx%csour(m)//cx%cline(m)//cx%ctele(m)
  ch2 = cx%csour(l)//cx%cline(l)//cx%ctele(l)
  cx_toc_default_ge = ch1.ge.ch2
end function cx_toc_default_ge
!
! ********************************************************************
!
! CX index Equal
!
! ********************************************************************
!
logical function cx_csour_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  ! TRUE if observation m is greater than observation l
  ! Comparison is done according to one keyword
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_csour_eq = cx%csour(m).eq.cx%csour(l)
end function cx_csour_eq
!
logical function cx_cline_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  ! TRUE if observation m is greater than observation l
  ! Comparison is done according to one keyword
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_cline_eq = cx%cline(m).eq.cx%cline(l)
end function cx_cline_eq
!
logical function cx_ctele_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  ! TRUE if observation m is greater than observation l
  ! Comparison is done according to one keyword
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_ctele_eq = cx%ctele(m).eq.cx%ctele(l)
end function cx_ctele_eq
!
logical function cx_bloc_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_bloc_eq = cx%bloc(m).eq.cx%bloc(l)
end function cx_bloc_eq
!
logical function cx_num_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_num_eq = cx%num(m).eq.cx%num(l)
end function cx_num_eq
!
logical function cx_ver_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_ver_eq = cx%ver(m).eq.cx%ver(l)
end function cx_ver_eq
!
logical function cx_dobs_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_dobs_eq = cx%dobs(m).eq.cx%dobs(l)
end function cx_dobs_eq
!
logical function cx_off1_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_off1_eq = cx%off1(m).eq.cx%off1(l)
end function cx_off1_eq
!
logical function cx_off2_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_off2_eq = cx%off2(m).eq.cx%off2(l)
end function cx_off2_eq
!
logical function cx_scan_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_scan_eq = cx%scan(m).eq.cx%scan(l)
end function cx_scan_eq
!
logical function cx_subscan_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_subscan_eq = cx%subscan(m).eq.cx%subscan(l)
end function cx_subscan_eq
!
logical function cx_kind_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_kind_eq = cx%kind(m).eq.cx%kind(l)
end function cx_kind_eq
!
logical function cx_qual_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  !
  cx_qual_eq = cx%qual(m).eq.cx%qual(l)
end function cx_qual_eq
!
logical function cx_toc_default_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  ! Local
  character(len=36) :: ch1,ch2
  !
  ch1 = cx%csour(m)//cx%cline(m)//cx%ctele(m)
  ch2 = cx%csour(l)//cx%cline(l)//cx%ctele(l)
  cx_toc_default_eq = ch1.eq.ch2
end function cx_toc_default_eq
!
logical function cx_toc_date_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  ! Local
  character(len=36) :: ch1,ch2
  !
  ch1 = cx%csour(m)//cx%cline(m)//cx%ctele(m)
  ch2 = cx%csour(l)//cx%cline(l)//cx%ctele(l)
  cx_toc_date_eq = (ch1.eq.ch2) .and. (cx%dobs(m).eq.cx%dobs(l))
end function cx_toc_date_eq
!
logical function cx_toc_scan_eq(m,l)
  use class_index
  !---------------------------------------------------------------------
  ! @ private
  ! LAS internal routine
  !---------------------------------------------------------------------
  integer(kind=entry_length), intent(in) :: m  ! Observation number
  integer(kind=entry_length), intent(in) :: l  ! Observation number
  ! Local
  character(len=36) :: ch1,ch2
  !
  ch1 = cx%csour(m)//cx%cline(m)//cx%ctele(m)
  ch2 = cx%csour(l)//cx%cline(l)//cx%ctele(l)
  cx_toc_scan_eq = (ch1.eq.ch2) .and. (cx%dobs(m).eq.cx%dobs(l)) .and. (cx%scan(m).eq.cx%scan(l))
end function cx_toc_scan_eq
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
