subroutine las_variables(set,r,error)
  use gildas_def
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>las_variables
  use class_common
  use class_index
  use class_types
  !----------------------------------------------------------------------
  ! @ public (for libclass only)
  !  Initialisation routine
  !  Define all SIC variables
  !----------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set
  type(observation),   intent(in)    :: r
  logical,             intent(inout) :: error
  ! Local
  integer(kind=index_length) :: dim(4)
  !
  dim = 0
  !
  call sic_def_long ('NEXT_OBS',fileout%desc%xnext,0,0,.true.,error)
  call sic_def_long ('FOUND',nindex,0,dim,.true.,error)
  if (error)  return
  !
  call class_set_structure(set,error)
  if (error) return
  !
  call las_variables_r(set,r,error)
  if (error)  return
  !
  call las_codes(error)
  if (error)  return
  !
end subroutine las_variables
!
subroutine class_set_structure(set,error)
  use gildas_def
  use classcore_dependencies_interfaces
  use class_setup_new
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !   Define the SIC variables which map the internal set% values.
  ! Currently only map a few values, mostly those which are scalar,
  ! i.e. unique in the SET calling sequence (e.g. SET TYPE SPECTRO).
  ! Will have to think twice for the variable name when multiple
  ! arguments are provided in the SET calling sequence (e.g. SET ALIGN
  ! VELO INTER)
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: struct='SET%LAS'
  integer(kind=index_length) :: dims(4)
  !
  dims = 0
  !
  call sic_defstructure(struct,.true.,error)
  if (error) return
  !
  ! Character strings (scalar)
  call class_setup_sicdef_angle(struct,error)
  call sic_def_char(struct//'%BAD',      set%bad,   .true.,error)
  call sic_def_char(struct//'%EXTENSION',set%defext,.true.,error)
  call sic_def_char(struct//'%FORMAT',   set%heade, .true.,error)
  call sic_def_char(struct//'%LINE',     set%line,  .true.,error)
  call sic_def_char(struct//'%SOURCE',   set%sourc, .true.,error)
  call sic_def_char(struct//'%TELESCOPE',set%teles, .true.,error)
  call sic_def_char(struct//'%WEIGHT',   set%weigh, .true.,error)
  if (error) return
  !
  ! Character strings (array)
  dims(1) = 2
  call sic_def_charn(struct//'%UNIT',set%unitx,1,dims,.true.,error)
  if (error) return
  !
  ! Integers (scalar)
  call sic_def_inte(struct//'%BASE', set%base, 0,1,.true.,error)
  call sic_def_inte(struct//'%TYPE', set%kind, 0,1,.true.,error)
  call sic_def_inte(struct//'%NWIND',set%nwind,0,1,.true.,error)
  call sic_def_inte(struct//'%NMASK',set%nmask,0,1,.true.,error)
  if (error) return
  !
  ! Reals (arrays)
  dims(1) = mwind1
  call sic_def_real(struct//'%WIND1',set%wind1,1,dims,.true.,error)
  call sic_def_real(struct//'%WIND2',set%wind2,1,dims,.true.,error)
  dims(1) = mmask1
  call sic_def_real(struct//'%MASK1',set%mask1,1,dims,.true.,error)
  call sic_def_real(struct//'%MASK2',set%mask2,1,dims,.true.,error)
  !
  ! Logical (scalar)
  call sic_def_logi(struct//'%MATCH', set%match,.true.,error)
  call sic_def_logi(struct//'%WRITE8',set%write_r8,.false.,error)
  if (error) return
  !
end subroutine class_set_structure
!
subroutine las_variables_r(set,r,error)
  use classcore_dependencies_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ public (for libclass only)
  ! Define and populate the R%HEAD structure
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  type(observation),   intent(in)    :: r      !
  logical,             intent(inout) :: error  ! Logical error flag
  ! Local
  integer(kind=4) :: dims
  !
  ! R structure
  call sic_defstructure('R',.true.,error)
  if (error)  return
  !
  ! R header
  call sic_defstructure('R%HEAD',.true.,error)
  dims = mx_sec+1
  call sic_def_login('R%HEAD%PRESEC',r%head%presec(-mx_sec),1,dims,.true.,error)
  if (error)  return
  !
  ! R header substructures
  call las_variables_rgen('R%HEAD',r,set%varpresec(class_sec_gen_id).ne.setvar_write,error)
  call las_variables_rpos('R%HEAD',r,set%varpresec(class_sec_pos_id).ne.setvar_write,error)
  call las_variables_rspe('R%HEAD',r,set%varpresec(class_sec_spe_id).ne.setvar_write,error)
  call las_variables_rres('R%HEAD',r,set%varpresec(class_sec_res_id).ne.setvar_write,error)
  call las_variables_rbas('R%HEAD',r,set%varpresec(class_sec_bas_id).ne.setvar_write,error)
  call las_variables_rhis('R%HEAD',r,set%varpresec(class_sec_his_id).ne.setvar_write,error)
  call las_variables_rplo('R%HEAD',r,set%varpresec(class_sec_plo_id).ne.setvar_write,error)
  call las_variables_rswi('R%HEAD',r,set%varpresec(class_sec_swi_id).ne.setvar_write,error)
  call las_variables_rcal('R%HEAD',r,set%varpresec(class_sec_cal_id).ne.setvar_write,error)
  call las_variables_rsky('R%HEAD',r,set%varpresec(class_sec_sky_id).ne.setvar_write,error)
  call las_variables_rgau('R%HEAD',r,set%varpresec(class_sec_gau_id).ne.setvar_write,error)
  call las_variables_rshe('R%HEAD',r,set%varpresec(class_sec_she_id).ne.setvar_write,error)
  call las_variables_rhfs('R%HEAD',r,set%varpresec(class_sec_hfs_id).ne.setvar_write,error)
  call las_variables_rabs('R%HEAD',r,set%varpresec(class_sec_abs_id).ne.setvar_write,error)
  call las_variables_rdri('R%HEAD',r,set%varpresec(class_sec_dri_id).ne.setvar_write,error)
  call las_variables_rbea('R%HEAD',r,set%varpresec(class_sec_bea_id).ne.setvar_write,error)
  call las_variables_rpoi('R%HEAD',r,set%varpresec(class_sec_poi_id).ne.setvar_write,error)
  call las_variables_rher('R%HEAD',r,set%varpresec(class_sec_her_id).ne.setvar_write,error)
  if (error)  return
  !
  ! Also the old shortcuts
  call las_variables_r_old(r,error)
  if (error)  return
  !
end subroutine las_variables_r
!
subroutine las_variables_rgen(struct,obs,ro,error)
  use gildas_def
  use gkernel_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: struct  ! Structure to be filled
  type(observation), intent(in)    :: obs     ! Observation to be mapped
  logical,           intent(in)    :: ro      ! Should the variables be RO?
  logical,           intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  integer(kind=index_length) :: dims(4)
  character(len=20) :: str
  !
  userreq = .false.
  dims = 0
  str = trim(struct)//'%GEN'
  !
  call sic_delvariable(str,userreq,error)
  !
  call sic_defstructure(str,.true.,error)
  call sic_def_long(trim(str)//'%NUM',obs%head%gen%num,0,dims,ro,error)
  call sic_def_inte(trim(str)//'%VER',obs%head%gen%ver,0,dims,ro,error)
  call sic_def_strn(trim(str)//'%TELES',obs%head%gen%teles,12,ro,error)
  call sic_def_inte(trim(str)//'%DOBS',obs%head%gen%dobs,0,dims,ro,error)
  call sic_def_inte(trim(str)//'%DRED',obs%head%gen%dred,0,dims,ro,error)
  call sic_def_char(trim(str)//'%CDOBS',obs%head%gen%cdobs,ro,error)
  call sic_def_char(trim(str)//'%CDRED',obs%head%gen%cdred,ro,error)
  call sic_def_inte(trim(str)//'%KIND',obs%head%gen%kind,0,dims,ro,error)
  call sic_def_inte(trim(str)//'%QUAL',obs%head%gen%qual,0,dims,ro,error)
  call sic_def_long(trim(str)//'%SCAN',obs%head%gen%scan,0,dims,ro,error)
  call sic_def_inte(trim(str)//'%SUBSCAN',obs%head%gen%subscan,0,dims,ro,error)
  call sic_def_dble(trim(str)//'%UT',obs%head%gen%ut,0,dims,ro,error)
  call sic_def_dble(trim(str)//'%ST',obs%head%gen%st,0,dims,ro,error)
  call sic_def_real(trim(str)//'%AZ',obs%head%gen%az,0,dims,ro,error)
  call sic_def_real(trim(str)//'%EL',obs%head%gen%el,0,dims,ro,error)
  call sic_def_real(trim(str)//'%TAU',obs%head%gen%tau,0,dims,ro,error)
  call sic_def_real(trim(str)//'%TSYS',obs%head%gen%tsys,0,dims,ro,error)
  call sic_def_real(trim(str)//'%TIME',obs%head%gen%time,0,dims,ro,error)
  call sic_def_dble(trim(str)//'%PARANG',obs%head%gen%parang,0,dims,ro,error)
  call sic_def_inte(trim(str)//'%YUNIT',obs%head%gen%yunit,0,dims,ro,error)
  call sic_def_inte(trim(str)//'%XUNIT',obs%head%gen%xunit,0,dims,ro,error)
  !
end subroutine las_variables_rgen
!
subroutine las_variables_rpos(struct,obs,ro,error)
  use gildas_def
  use gkernel_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: struct  ! Structure to be filled
  type(observation), intent(in)    :: obs     ! Observation to be mapped
  logical,           intent(in)    :: ro      ! Should the variables be RO?
  logical,           intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  integer(kind=index_length) :: dims(4)
  character(len=20) :: str
  !
  userreq = .false.
  dims = 0
  str = trim(struct)//'%POS'
  !
  call sic_delvariable(str,userreq,error)
  !
  call sic_defstructure(str,.true.,error)
  call sic_def_strn(trim(str)//'%SOURC',obs%head%pos%sourc,12,ro,error)
  call sic_def_inte(trim(str)//'%SYSTEM',obs%head%pos%system,0,dims,ro,error)
  call sic_def_real(trim(str)//'%EQUINOX',obs%head%pos%equinox,0,dims,ro,error)
  call sic_def_inte(trim(str)//'%PROJ',obs%head%pos%proj,0,dims,ro,error)
  call sic_def_dble(trim(str)//'%LAM',obs%head%pos%lam,0,dims,ro,error)
  call sic_def_dble(trim(str)//'%BET',obs%head%pos%bet,0,dims,ro,error)
  call sic_def_dble(trim(str)//'%PROJANG',obs%head%pos%projang,0,dims,ro,error)
  call sic_def_real(trim(str)//'%LAMOF',obs%head%pos%lamof,0,dims,ro,error)
  call sic_def_real(trim(str)//'%BETOF',obs%head%pos%betof,0,dims,ro,error)
  !
end subroutine las_variables_rpos
!
subroutine las_variables_rspe(struct,obs,ro,error)
  use gildas_def
  use gkernel_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: struct  ! Structure to be filled
  type(observation), intent(in)    :: obs     ! Observation to be mapped
  logical,           intent(in)    :: ro      ! Should the variables be RO?
  logical,           intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  integer(kind=index_length) :: dims(4)
  character(len=20) :: str
  !
  userreq = .false.
  dims = 0
  str = trim(struct)//'%SPE'
  !
  call sic_delvariable(str,userreq,error)
  !
  call sic_defstructure(str,.true.,error)
  call sic_def_strn(trim(str)//'%LINE',obs%head%spe%line,12,ro,error)
  call sic_def_inte(trim(str)//'%NCHAN',obs%head%spe%nchan,0,dims,ro,error)
  call sic_def_dble(trim(str)//'%RESTF',obs%head%spe%restf,0,dims,ro,error)
  call sic_def_dble(trim(str)//'%RCHAN',obs%head%spe%rchan,0,dims,ro,error)
  call sic_def_dble(trim(str)//'%FRES',obs%head%spe%fres,0,dims,ro,error)
  call sic_def_dble(trim(str)//'%VRES',obs%head%spe%vres,0,dims,ro,error)
  call sic_def_dble(trim(str)//'%VOFF',obs%head%spe%voff,0,dims,ro,error)
  call sic_def_real(trim(str)//'%BAD',obs%head%spe%bad,0,dims,ro,error)
  call sic_def_dble(trim(str)//'%IMAGE',obs%head%spe%image,0,dims,ro,error)
  call sic_def_inte(trim(str)//'%VTYPE',obs%head%spe%vtype,0,dims,ro,error)
  call sic_def_inte(trim(str)//'%VCONV',obs%head%spe%vconv,0,dims,ro,error)
  call sic_def_inte(trim(str)//'%VDIRE',obs%head%spe%vdire,0,dims,ro,error)
  call sic_def_dble(trim(str)//'%DOPPLER',obs%head%spe%doppler,0,dims,ro,error)
  !
end subroutine las_variables_rspe
!
subroutine las_variables_rres(struct,obs,ro,error)
  use gildas_def
  use gkernel_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: struct  ! Structure to be filled
  type(observation), intent(in)    :: obs     ! Observation to be mapped
  logical,           intent(in)    :: ro      ! Should the variables be RO?
  logical,           intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  integer(kind=index_length) :: dims(4)
  character(len=20) :: str
  !
  userreq = .false.
  dims(:) = 0
  str = trim(struct)//'%RES'
  !
  call sic_delvariable(str,userreq,error)
  !
  call sic_defstructure(str,.true.,error)
  call sic_def_real(trim(str)//'%MAJOR', obs%head%res%major, 0,dims,ro,error)
  call sic_def_real(trim(str)//'%MINOR', obs%head%res%minor, 0,dims,ro,error)
  call sic_def_real(trim(str)//'%POSANG',obs%head%res%posang,0,dims,ro,error)
  !
end subroutine las_variables_rres
!
subroutine las_variables_rbas(struct,obs,ro,error)
  use gildas_def
  use gkernel_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: struct  ! Structure to be filled
  type(observation), intent(in)    :: obs     ! Observation to be mapped
  logical,           intent(in)    :: ro      ! Should the variables be RO?
  logical,           intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  integer(kind=index_length) :: dims(4)
  character(len=20) :: str
  !
  userreq = .false.
  dims = 0
  str = trim(struct)//'%BAS'
  !
  call sic_delvariable(str,userreq,error)
  !
  call sic_defstructure(str,.true.,error)
  call sic_def_inte(trim(str)//'%DEG',obs%head%bas%deg,0,dims,ro,error)
  call sic_def_real(trim(str)//'%SIGFI',obs%head%bas%sigfi,0,dims,ro,error)
  call sic_def_real(trim(str)//'%AIRE',obs%head%bas%aire,0,dims,ro,error)
  call sic_def_inte(trim(str)//'%NWIND',obs%head%bas%nwind,0,dims,ro,error)
  dims(1) = mwind
  call sic_def_real(trim(str)//'%W1',obs%head%bas%w1,1,dims,ro,error)
  call sic_def_real(trim(str)//'%W2',obs%head%bas%w2,1,dims,ro,error)
  dims(1) = 3
  call sic_def_real(trim(str)//'%SINUS',obs%head%bas%sinus,1,dims,ro,error)
  !
end subroutine las_variables_rbas
!
subroutine las_variables_rhis(struct,obs,ro,error)
  use gildas_def
  use gkernel_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: struct  ! Structure to be filled
  type(observation), intent(in)    :: obs     ! Observation to be mapped
  logical,           intent(in)    :: ro      ! Should the variables be RO?
  logical,           intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  integer(kind=index_length) :: dims(4)
  character(len=20) :: str
  !
  userreq = .false.
  dims = 0
  str = trim(struct)//'%HIS'
  !
  call sic_delvariable(str,userreq,error)
  !
  call sic_defstructure(str,.true.,error)
  call sic_def_inte(trim(str)//'%NSEQ',obs%head%his%nseq,0,dims,ro,error)
  dims(1) = mseq
  call sic_def_inte(trim(str)//'%START',obs%head%his%start,1,dims,ro,error)
  call sic_def_inte(trim(str)//'%END',obs%head%his%end,1,dims,ro,error)
  !
end subroutine las_variables_rhis
!
subroutine las_variables_rplo(struct,obs,ro,error)
  use gildas_def
  use gkernel_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: struct  ! Structure to be filled
  type(observation), intent(in)    :: obs     ! Observation to be mapped
  logical,           intent(in)    :: ro      ! Should the variables be RO?
  logical,           intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  integer(kind=index_length) :: dims(4)
  character(len=20) :: str
  !
  userreq = .false.
  dims = 0
  str = trim(struct)//'%PLO'
  !
  call sic_delvariable(str,userreq,error)
  !
  call sic_defstructure(str,.true.,error)
  call sic_def_real(trim(str)//'%AMIN',obs%head%plo%amin,0,dims,ro,error)
  call sic_def_real(trim(str)//'%AMAX',obs%head%plo%amax,0,dims,ro,error)
  call sic_def_real(trim(str)//'%VMIN',obs%head%plo%vmin,0,dims,ro,error)
  call sic_def_real(trim(str)//'%VMAX',obs%head%plo%vmax,0,dims,ro,error)
  !
end subroutine las_variables_rplo
!
subroutine las_variables_rswi(struct,obs,ro,error)
  use gildas_def
  use gkernel_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: struct  ! Structure to be filled
  type(observation), intent(in)    :: obs     ! Observation to be mapped
  logical,           intent(in)    :: ro      ! Should the variables be RO?
  logical,           intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  integer(kind=index_length) :: dims(4)
  character(len=20) :: str
  !
  userreq = .false.
  dims = 0
  str = trim(struct)//'%FSW'
  !
  call sic_delvariable(str,userreq,error)
  !
  call sic_defstructure(str,.true.,error)
  call sic_def_inte(trim(str)//'%NPHAS',obs%head%swi%nphas,0,dims,ro,error)
  call sic_def_inte(trim(str)//'%SWMOD',obs%head%swi%swmod,0,dims,ro,error)
  dims(1) = mxphas
  call sic_def_dble(trim(str)//'%DECAL',obs%head%swi%decal,1,dims,ro,error)
  call sic_def_real(trim(str)//'%DUREE',obs%head%swi%duree,1,dims,ro,error)
  call sic_def_real(trim(str)//'%POIDS',obs%head%swi%poids,1,dims,ro,error)
  call sic_def_real(trim(str)//'%LDECAL',obs%head%swi%ldecal,1,dims,ro,error)
  call sic_def_real(trim(str)//'%BDECAL',obs%head%swi%bdecal,1,dims,ro,error)
  !
end subroutine las_variables_rswi
!
subroutine las_variables_rcal(struct,obs,ro,error)
  use gildas_def
  use gkernel_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: struct  ! Structure to be filled
  type(observation), intent(in)    :: obs     ! Observation to be mapped
  logical,           intent(in)    :: ro      ! Should the variables be RO?
  logical,           intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  integer(kind=index_length) :: dims(4)
  character(len=20) :: str
  !
  userreq = .false.
  dims = 0
  str = trim(struct)//'%CAL'
  !
  call sic_delvariable(str,userreq,error)
  !
  call sic_defstructure(str,.true.,error)
  call sic_def_real(trim(str)//'%BEEFF',obs%head%cal%beeff,0,dims,ro,error)
  call sic_def_real(trim(str)//'%FOEFF',obs%head%cal%foeff,0,dims,ro,error)
  call sic_def_real(trim(str)//'%GAINI',obs%head%cal%gaini,0,dims,ro,error)
  call sic_def_real(trim(str)//'%H2OMM',obs%head%cal%h2omm,0,dims,ro,error)
  call sic_def_real(trim(str)//'%PAMB',obs%head%cal%pamb,0,dims,ro,error)
  call sic_def_real(trim(str)//'%TAMB',obs%head%cal%tamb,0,dims,ro,error)
  call sic_def_real(trim(str)//'%TATMS',obs%head%cal%tatms,0,dims,ro,error)
  call sic_def_real(trim(str)//'%TCHOP',obs%head%cal%tchop,0,dims,ro,error)
  call sic_def_real(trim(str)//'%TCOLD',obs%head%cal%tcold,0,dims,ro,error)
  call sic_def_real(trim(str)//'%TAUS',obs%head%cal%taus,0,dims,ro,error)
  call sic_def_real(trim(str)//'%TAUI',obs%head%cal%taui,0,dims,ro,error)
  call sic_def_real(trim(str)//'%TATMI',obs%head%cal%tatmi,0,dims,ro,error)
  call sic_def_real(trim(str)//'%TREC',obs%head%cal%trec,0,dims,ro,error)
  call sic_def_inte(trim(str)//'%CMODE',obs%head%cal%cmode,0,dims,ro,error)
  call sic_def_real(trim(str)//'%ATFAC',obs%head%cal%atfac,0,dims,ro,error)
  call sic_def_real(trim(str)//'%ALTI',obs%head%cal%alti,0,dims,ro,error)
  dims(1) = 3
  call sic_def_real(trim(str)//'%COUNT',obs%head%cal%count,1,dims,ro,error)
  call sic_def_real(trim(str)//'%LCALOF',obs%head%cal%lcalof,0,dims,ro,error)
  call sic_def_real(trim(str)//'%BCALOF',obs%head%cal%bcalof,0,dims,ro,error)
  call sic_def_dble(trim(str)//'%GEOLONG',obs%head%cal%geolong,0,dims,ro,error)
  call sic_def_dble(trim(str)//'%GEOLAT',obs%head%cal%geolat,0,dims,ro,error)
  !
end subroutine las_variables_rcal
!
subroutine las_variables_rsky(struct,obs,ro,error)
  use gildas_def
  use gkernel_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: struct  ! Structure to be filled
  type(observation), intent(in)    :: obs     ! Observation to be mapped
  logical,           intent(in)    :: ro      ! Should the variables be RO?
  logical,           intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  integer(kind=index_length) :: dims(4)
  character(len=20) :: str
  !
  userreq = .false.
  dims = 0
  str = trim(struct)//'%SKY'
  !
  call sic_delvariable(str,userreq,error)
  !
  call sic_defstructure(str,.true.,error)
  call sic_def_dble(trim(str)//'%SREST',obs%head%sky%restf,0,dims,ro,error)
  call sic_def_dble(trim(str)//'%SIMAG',obs%head%sky%image,0,dims,ro,error)
  call sic_def_inte(trim(str)//'%NSKY',obs%head%sky%nsky,0,dims,ro,error)
  call sic_def_inte(trim(str)//'%NCHOP',obs%head%sky%nchop,0,dims,ro,error)
  call sic_def_inte(trim(str)//'%NCOLD',obs%head%sky%ncold,0,dims,ro,error)
  dims(1) = msky
  call sic_def_real(trim(str)//'%ELEV',obs%head%sky%elev,1,dims,ro,error)
  call sic_def_real(trim(str)//'%EMISS',obs%head%sky%emiss,1,dims,ro,error)
  call sic_def_real(trim(str)//'%CHOPP',obs%head%sky%chopp,1,dims,ro,error)
  call sic_def_real(trim(str)//'%COLD',obs%head%sky%cold,1,dims,ro,error)
  !
end subroutine las_variables_rsky
!
subroutine las_variables_rgau(struct,obs,ro,error)
  use gildas_def
  use gkernel_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: struct  ! Structure to be filled
  type(observation), intent(in)    :: obs     ! Observation to be mapped
  logical,           intent(in)    :: ro      ! Should the variables be RO?
  logical,           intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  integer(kind=index_length) :: dims(4)
  character(len=20) :: str
  !
  userreq = .false.
  dims = 0
  str = trim(struct)//'%GAU'
  !
  call sic_delvariable(str,userreq,error)
  !
  call sic_defstructure(str,.true.,error)
  call sic_def_inte(trim(str)//'%NLINE',obs%head%gau%nline,0,dims,ro,error)
  call sic_def_real(trim(str)//'%RMS_BASE',obs%head%gau%sigba,0,dims,ro,error)
  call sic_def_real(trim(str)//'%RMS_LINE',obs%head%gau%sigra,0,dims,ro,error)
  dims(1) = mgausfit
  call sic_def_real(trim(str)//'%RESULT',obs%head%gau%nfit,1,dims,ro,error)
  call sic_def_real(trim(str)//'%ERROR',obs%head%gau%nerr,1,dims,ro,error)
  if (error)  return
  !
  ! jan12 and older names:
  call sic_def_alias(trim(str)//'%SIGBA',trim(str)//'%RMS_BASE',.true.,error)
  call sic_def_alias(trim(str)//'%SIGRA',trim(str)//'%RMS_LINE',.true.,error)
  call sic_def_alias(trim(str)//'%NFIT',trim(str)//'%RESULT',.true.,error)
  call sic_def_alias(trim(str)//'%NERR',trim(str)//'%ERROR',.true.,error)
  if (error)  return
  !
end subroutine las_variables_rgau
!
subroutine las_variables_rshe(struct,obs,ro,error)
  use gildas_def
  use gkernel_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: struct  ! Structure to be filled
  type(observation), intent(in)    :: obs     ! Observation to be mapped
  logical,           intent(in)    :: ro      ! Should the variables be RO?
  logical,           intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  integer(kind=index_length) :: dims(4)
  character(len=20) :: str
  !
  userreq = .false.
  dims = 0
  str = trim(struct)//'%SHE'
  !
  call sic_delvariable(str,userreq,error)
  !
  call sic_defstructure(str,.true.,error)
  call sic_def_inte(trim(str)//'%NLINE',obs%head%she%nline,0,dims,ro,error)
  call sic_def_real(trim(str)//'%RMS_BASE',obs%head%she%sigba,0,dims,ro,error)
  call sic_def_real(trim(str)//'%RMS_LINE',obs%head%she%sigra,0,dims,ro,error)
  dims(1) = mshellfit
  call sic_def_real(trim(str)//'%RESULT',obs%head%she%nfit,1,dims,ro,error)
  call sic_def_real(trim(str)//'%ERROR',obs%head%she%nerr,1,dims,ro,error)
  if (error)  return
  !
  ! jan12 and older names:
  call sic_def_alias(trim(str)//'%LSHELL',trim(str)//'%NLINE',.true.,error)
  call sic_def_alias(trim(str)//'%BSHELL',trim(str)//'%RMS_BASE',.true.,error)
  call sic_def_alias(trim(str)//'%RSHELL',trim(str)//'%RMS_LINE',.true.,error)
  call sic_def_alias(trim(str)//'%NSHELL',trim(str)//'%RESULT',.true.,error)
  call sic_def_alias(trim(str)//'%ESHELL',trim(str)//'%ERROR',.true.,error)
  if (error)  return
  !
end subroutine las_variables_rshe
!
subroutine las_variables_rhfs(struct,obs,ro,error)
  use gildas_def
  use gkernel_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: struct  ! Structure to be filled
  type(observation), intent(in)    :: obs     ! Observation to be mapped
  logical,           intent(in)    :: ro      ! Should the variables be RO?
  logical,           intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  integer(kind=index_length) :: dims(4)
  character(len=20) :: str,str2
  !
  userreq = .false.
  dims = 0
  str = trim(struct)//'%HFS'
  !
  call sic_delvariable(str,userreq,error)
  !
  call sic_defstructure(str,.true.,error)
  call sic_def_inte(trim(str)//'%NLINE',obs%head%hfs%nline,0,dims,ro,error)
  call sic_def_real(trim(str)//'%RMS_BASE',obs%head%hfs%sigba,0,dims,ro,error)
  call sic_def_real(trim(str)//'%RMS_LINE',obs%head%hfs%sigra,0,dims,ro,error)
  dims(1) = mhfsfit
  call sic_def_real(trim(str)//'%RESULT',obs%head%hfs%nfit,1,dims,ro,error)
  call sic_def_real(trim(str)//'%ERROR',obs%head%hfs%nerr,1,dims,ro,error)
  if (error)  return
  !
  ! jan12 and older names:
  str2 = trim(struct)//'%NH3'
  call sic_delvariable(str2,userreq,error)
  call sic_defstructure(str2,.true.,error)
  call sic_def_alias(trim(str2)//'%LNH3',trim(str)//'%NLINE',.true.,error)
  call sic_def_alias(trim(str2)//'%BNH3',trim(str)//'%RMS_BASE',.true.,error)
  call sic_def_alias(trim(str2)//'%RNH3',trim(str)//'%RMS_LINE',.true.,error)
  call sic_def_alias(trim(str2)//'%NNH3',trim(str)//'%RESULT',.true.,error)
  call sic_def_alias(trim(str2)//'%ENH3',trim(str)//'%ERROR',.true.,error)
  if (error)  return
  !
end subroutine las_variables_rhfs
!
subroutine las_variables_rabs(struct,obs,ro,error)
  use gildas_def
  use gkernel_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: struct  ! Structure to be filled
  type(observation), intent(in)    :: obs     ! Observation to be mapped
  logical,           intent(in)    :: ro      ! Should the variables be RO?
  logical,           intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  integer(kind=index_length) :: dims(4)
  character(len=20) :: str
  !
  userreq = .false.
  dims = 0
  str = trim(struct)//'%ABS'
  !
  call sic_delvariable(str,userreq,error)
  !
  call sic_defstructure(str,.true.,error)
  call sic_def_inte(trim(str)//'%NLINE',obs%head%abs%nline,0,dims,ro,error)
  call sic_def_real(trim(str)//'%RMS_BASE',obs%head%abs%sigba,0,dims,ro,error)
  call sic_def_real(trim(str)//'%RMS_LINE',obs%head%abs%sigra,0,dims,ro,error)
  dims(1) = mabsfit
  call sic_def_real(trim(str)//'%RESULT',obs%head%abs%nfit,1,dims,ro,error)
  call sic_def_real(trim(str)//'%ERROR',obs%head%abs%nerr,1,dims,ro,error)
  if (error)  return
  !
  ! jan12 and older names:
  call sic_def_alias(trim(str)//'%LABS',trim(str)//'%NLINE',.true.,error)
  call sic_def_alias(trim(str)//'%BABS',trim(str)//'%RMS_BASE',.true.,error)
  call sic_def_alias(trim(str)//'%RABS',trim(str)//'%RMS_LINE',.true.,error)
  call sic_def_alias(trim(str)//'%NABS',trim(str)//'%RESULT',.true.,error)
  call sic_def_alias(trim(str)//'%EABS',trim(str)//'%ERROR',.true.,error)
  if (error)  return
  !
end subroutine las_variables_rabs
!
subroutine las_variables_rdri(struct,obs,ro,error)
  use gildas_def
  use gkernel_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: struct  ! Structure to be filled
  type(observation), intent(in)    :: obs     ! Observation to be mapped
  logical,           intent(in)    :: ro      ! Should the variables be RO?
  logical,           intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  integer(kind=index_length) :: dims(4)
  character(len=20) :: str
  !
  userreq = .false.
  dims = 0
  str = trim(struct)//'%DRI'
  !
  call sic_delvariable(str,userreq,error)
  !
  call sic_defstructure(str,.true.,error)
  call sic_def_dble(trim(str)//'%FREQ',obs%head%dri%freq,0,dims,ro,error)
  call sic_def_real(trim(str)//'%WIDTH',obs%head%dri%width,0,dims,ro,error)
  call sic_def_inte(trim(str)//'%NPOIN',obs%head%dri%npoin,0,dims,ro,error)
  call sic_def_real(trim(str)//'%RPOIN',obs%head%dri%rpoin,0,dims,ro,error)
  call sic_def_real(trim(str)//'%TREF',obs%head%dri%tref,0,dims,ro,error)
  call sic_def_real(trim(str)//'%AREF',obs%head%dri%aref,0,dims,ro,error)
  call sic_def_real(trim(str)//'%TRES',obs%head%dri%tres,0,dims,ro,error)
  call sic_def_real(trim(str)//'%ARES',obs%head%dri%ares,0,dims,ro,error)
  call sic_def_real(trim(str)//'%APOS',obs%head%dri%apos,0,dims,ro,error)
  call sic_def_real(trim(str)//'%CBAD',obs%head%dri%bad,0,dims,ro,error)
  call sic_def_inte(trim(str)//'%CTYPE',obs%head%dri%ctype,0,dims,ro,error)
  call sic_def_dble(trim(str)//'%CIMAG',obs%head%dri%cimag,0,dims,ro,error)
  call sic_def_real(trim(str)//'%COLLA',obs%head%dri%colla,0,dims,ro,error)
  call sic_def_real(trim(str)//'%COLLE',obs%head%dri%colle,0,dims,ro,error)
  !
end subroutine las_variables_rdri
!
subroutine las_variables_rbea(struct,obs,ro,error)
  use gildas_def
  use gkernel_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: struct  ! Structure to be filled
  type(observation), intent(in)    :: obs     ! Observation to be mapped
  logical,           intent(in)    :: ro      ! Should the variables be RO?
  logical,           intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  integer(kind=index_length) :: dims(4)
  character(len=20) :: str
  !
  userreq = .false.
  dims = 0
  str = trim(struct)//'%BEA'
  !
  call sic_delvariable(str,userreq,error)
  !
  call sic_defstructure(str,.true.,error)
  call sic_def_real(trim(str)//'%CAZIM',obs%head%bea%cazim,0,dims,ro,error)
  call sic_def_real(trim(str)//'%CELEV',obs%head%bea%celev,0,dims,ro,error)
  call sic_def_real(trim(str)//'%SPACE',obs%head%bea%space,0,dims,ro,error)
  call sic_def_real(trim(str)//'%BPOS',obs%head%bea%bpos,0,dims,ro,error)
  call sic_def_inte(trim(str)//'%BTYPE',obs%head%bea%btype,0,dims,ro,error)
  !
end subroutine las_variables_rbea
!
subroutine las_variables_rpoi(struct,obs,ro,error)
  use gildas_def
  use gkernel_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: struct  ! Structure to be filled
  type(observation), intent(in)    :: obs     ! Observation to be mapped
  logical,           intent(in)    :: ro      ! Should the variables be RO?
  logical,           intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  integer(kind=index_length) :: dims(4)
  character(len=20) :: str
  !
  userreq = .false.
  dims = 0
  str = trim(struct)//'%POI'
  !
  call sic_delvariable(str,userreq,error)
  !
  call sic_defstructure(str,.true.,error)
  call sic_def_inte(trim(str)//'%LCONT',obs%head%poi%nline,0,dims,ro,error)
  call sic_def_real(trim(str)//'%BCONT',obs%head%poi%sigba,0,dims,ro,error)
  call sic_def_real(trim(str)//'%RCONT',obs%head%poi%sigra,0,dims,ro,error)
  dims(1) = mpoifit
  call sic_def_real(trim(str)//'%NCONT',obs%head%poi%nfit,1,dims,ro,error)
  call sic_def_real(trim(str)//'%ECONT',obs%head%poi%nerr,1,dims,ro,error)
  !
end subroutine las_variables_rpoi
!
subroutine las_variables_rher(struct,obs,ro,error)
  use gildas_def
  use gkernel_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: struct  ! Structure to be filled
  type(observation), intent(in)    :: obs     ! Observation to be mapped
  logical,           intent(in)    :: ro      ! Should the variables be RO?
  logical,           intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  integer(kind=index_length) :: dims(4)
  character(len=20) :: str
  !
  userreq = .false.
  dims = 0
  str = trim(struct)//'%HER'
  !
  call sic_delvariable(str,userreq,error)
  !
  call sic_defstructure(str,.true.,error)
  call sic_def_long(trim(str)//'%OBSID',     obs%head%her%obsid,     0,dims,ro,error)
  call sic_def_char(trim(str)//'%INSTRUMENT',obs%head%her%instrument,       ro,error)
  call sic_def_char(trim(str)//'%PROPOSAL',  obs%head%her%proposal,         ro,error)
  call sic_def_char(trim(str)//'%AOR',       obs%head%her%aor,              ro,error)
  call sic_def_inte(trim(str)//'%OPERDAY',   obs%head%her%operday,   0,dims,ro,error)
  call sic_def_char(trim(str)//'%DATEOBS',   obs%head%her%dateobs,          ro,error)
  call sic_def_char(trim(str)//'%DATEEND',   obs%head%her%dateend,          ro,error)
  call sic_def_char(trim(str)//'%OBSMODE',   obs%head%her%obsmode,          ro,error)
  call sic_def_real(trim(str)//'%VINFO',     obs%head%her%vinfo,     0,dims,ro,error)
  call sic_def_real(trim(str)//'%ZINFO',     obs%head%her%zinfo,     0,dims,ro,error)
  call sic_def_dble(trim(str)//'%POSANGLE',  obs%head%her%posangle,  0,dims,ro,error)
  call sic_def_dble(trim(str)//'%REFLAM',    obs%head%her%reflam,    0,dims,ro,error)
  call sic_def_dble(trim(str)//'%REFBET',    obs%head%her%refbet,    0,dims,ro,error)
  call sic_def_dble(trim(str)//'%HIFAVELAM', obs%head%her%hifavelam, 0,dims,ro,error)
  call sic_def_dble(trim(str)//'%HIFAVEBET', obs%head%her%hifavebet, 0,dims,ro,error)
  call sic_def_real(trim(str)//'%ETAMB',     obs%head%her%etamb,     0,dims,ro,error)
  call sic_def_real(trim(str)//'%ETAL',      obs%head%her%etal,      0,dims,ro,error)
  call sic_def_real(trim(str)//'%ETAA',      obs%head%her%etaa,      0,dims,ro,error)
  call sic_def_real(trim(str)//'%HPBW',      obs%head%her%hpbw,      0,dims,ro,error)
  call sic_def_char(trim(str)//'%TEMPSCAL',  obs%head%her%tempscal,         ro,error)
  call sic_def_dble(trim(str)//'%LODOPAVE',  obs%head%her%lodopave,  0,dims,ro,error)
  call sic_def_real(trim(str)//'%GIM0',      obs%head%her%gim0,      0,dims,ro,error)
  call sic_def_real(trim(str)//'%GIM1',      obs%head%her%gim1,      0,dims,ro,error)
  call sic_def_real(trim(str)//'%GIM2',      obs%head%her%gim2,      0,dims,ro,error)
  call sic_def_real(trim(str)//'%GIM3',      obs%head%her%gim3,      0,dims,ro,error)
  call sic_def_real(trim(str)//'%MIXERCURH', obs%head%her%mixercurh, 0,dims,ro,error)
  call sic_def_real(trim(str)//'%MIXERCURV', obs%head%her%mixercurv, 0,dims,ro,error)
  call sic_def_char(trim(str)//'%DATEHCSS',  obs%head%her%datehcss,         ro,error)
  call sic_def_char(trim(str)//'%HCSSVER',   obs%head%her%hcssver,          ro,error)
  call sic_def_char(trim(str)//'%CALVER',    obs%head%her%calver,           ro,error)
  call sic_def_real(trim(str)//'%LEVEL',     obs%head%her%level,     0,dims,ro,error)
  !
end subroutine las_variables_rher
!
subroutine las_variables_r_old(r,error)
  use classcore_dependencies_interfaces
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  (Re)define the set of old R-related variables which are always
  ! present in Class.
  !  NB: There are yet other sets of old R-related variables, section
  ! per section, defined by the command SET VARIABLE <section>. The
  ! associated names may be different again e.g. NCHAN vs CHANNELS
  !---------------------------------------------------------------------
  type(observation), intent(in)    :: r      !
  logical,           intent(inout) :: error  ! Logical error flag
  ! Local
  logical :: userreq
  !
  userreq = .false.
  call sic_delvariable('TELESCOPE',userreq,error)
  call sic_delvariable('NUMBER',userreq,error)
  call sic_delvariable('VERSION',userreq,error)
  call sic_delvariable('SCAN',userreq,error)
  call sic_delvariable('DATATYPE',userreq,error)
  call sic_delvariable('QUALITY',userreq,error)
  call sic_delvariable('UTOBS',userreq,error)
  call sic_delvariable('LSTOBS',userreq,error)
  call sic_delvariable('AZIMUTH',userreq,error)
  call sic_delvariable('ELEVATION',userreq,error)
  call sic_delvariable('TSYS',userreq,error)
  call sic_delvariable('TIME',userreq,error)
  call sic_delvariable('SUBSCAN',userreq,error)
  call sic_delvariable('SOURCE',userreq,error)
  call sic_delvariable('LAMBDA',userreq,error)
  call sic_delvariable('BETA',userreq,error)
  call sic_delvariable('OFF_LAMBDA',userreq,error)
  call sic_delvariable('OFF_BETA',userreq,error)
  call sic_delvariable('EQUINOX',userreq,error)
  call sic_delvariable('LINE',userreq,error)
  call sic_delvariable('CHANNELS',userreq,error)
  call sic_delvariable('REFERENCE',userreq,error)
  call sic_delvariable('FREQ_STEP',userreq,error)
  call sic_delvariable('VELO_STEP',userreq,error)
  call sic_delvariable('VELOCITY',userreq,error)
  call sic_delvariable('FREQUENCY',userreq,error)
  call sic_delvariable('IMAGE',userreq,error)
  call sic_delvariable('SIGMA',userreq,error)
  call sic_delvariable('AREA',userreq,error)
  call sic_delvariable('BEAM_EFF',userreq,error)
  call sic_delvariable('FORWARD_EFF',userreq,error)
  call sic_delvariable('GAIN_IMAGE',userreq,error)
  call sic_delvariable('WATER',userreq,error)
  call sic_delvariable('PRESSURE',userreq,error)
  call sic_delvariable('AMBIANT_T',userreq,error)
  call sic_delvariable('CHOPPER_T',userreq,error)
  call sic_delvariable('COLD_T',userreq,error)
  call sic_delvariable('TAU_SIGNAL',userreq,error)
  call sic_delvariable('TAU_IMAGE',userreq,error)
  call sic_delvariable('ATM_SIGNAL',userreq,error)
  call sic_delvariable('ATM_IMAGE',userreq,error)
  call sic_delvariable('BEAM_THROW',userreq,error)
  call sic_delvariable('RAPOS',userreq,error)
  call sic_delvariable('NRECORDS',userreq,error)
  error = .false.  ! Attempt to delete non-existent variables is not an error
  !
  ! Note some are read-only, others read-write
  call sic_def_strn ('TELESCOPE',r%head%gen%teles,12,.false.,error)
  call sic_def_long ('NUMBER',r%head%gen%num,0,0,.false.,error)
  call sic_def_inte ('VERSION',r%head%gen%ver,0,0,.true.,error)
  call sic_def_long ('SCAN',r%head%gen%scan,0,0,.false.,error)
  call sic_def_inte ('DATATYPE',r%head%gen%kind,0,0,.true.,error)
  call sic_def_inte ('QUALITY',r%head%gen%qual,0,0,.true.,error)
  call sic_def_dble ('UTOBS',r%head%gen%ut,0,0,.true.,error)
  call sic_def_dble ('LSTOBS',r%head%gen%st,0,0,.true.,error)
  call sic_def_real ('AZIMUTH',r%head%gen%az,0,0,.false.,error)
  call sic_def_real ('ELEVATION',r%head%gen%el,0,0,.false.,error)
  call sic_def_real ('TSYS',r%head%gen%tsys,0,0,.false.,error)
  call sic_def_real ('TIME',r%head%gen%time,0,0,.false.,error)
  call sic_def_inte ('SUBSCAN',r%head%gen%subscan,0,0,.false.,error)
  call sic_def_strn ('SOURCE',r%head%pos%sourc,12,.false.,error)
  call sic_def_dble ('LAMBDA',r%head%pos%lam,0,0,.false.,error)
  call sic_def_dble ('BETA',r%head%pos%bet,0,0,.false.,error)
  call sic_def_real ('OFF_LAMBDA',r%head%pos%rx_off,0,0,.true.,error)
  call sic_def_real ('OFF_BETA',r%head%pos%ry_off,0,0,.true.,error)
  call sic_def_real ('EQUINOX',r%head%pos%equinox,0,0,.false.,error)
  call sic_def_strn ('LINE',r%head%spe%line,12,.false.,error)
  call sic_def_inte ('CHANNELS',r%head%spe%nchan,0,0,.true.,error)
  call sic_def_dble ('REFERENCE',r%head%spe%rchan,0,0,.false.,error)
  call sic_def_dble ('FREQ_STEP',r%head%spe%fres,0,0,.false.,error)
  call sic_def_dble ('VELO_STEP',r%head%spe%vres,0,0,.false.,error)
  call sic_def_dble ('VELOCITY',r%head%spe%voff,0,0,.false.,error)
  call sic_def_dble ('FREQUENCY',r%head%spe%restf,0,0,.false.,error)
  call sic_def_dble ('IMAGE',r%head%spe%image,0,0,.false.,error)
  call sic_def_real ('SIGMA',r%head%bas%sigfi,0,0,.true.,error)
  call sic_def_real ('AREA',r%head%bas%aire,0,0,.true.,error)
  call sic_def_real ('BEAM_EFF',r%head%cal%beeff,0,0,.false.,error)
  call sic_def_real ('FORWARD_EFF',r%head%cal%foeff,0,0,.false.,error)
  call sic_def_real ('GAIN_IMAGE',r%head%cal%gaini,0,0,.false.,error)
  call sic_def_real ('WATER',r%head%cal%h2omm,0,0,.true.,error)
  call sic_def_real ('PRESSURE',r%head%cal%pamb,0,0,.true.,error)
  call sic_def_real ('AMBIANT_T',r%head%cal%tamb,0,0,.true.,error)
  call sic_def_real ('CHOPPER_T',r%head%cal%tchop,0,0,.true.,error)
  call sic_def_real ('COLD_T',r%head%cal%tcold,0,0,.true.,error)
  call sic_def_real ('TAU_SIGNAL',r%head%cal%taus,0,0,.true.,error)
  call sic_def_real ('TAU_IMAGE',r%head%cal%taui,0,0,.true.,error)
  call sic_def_real ('ATM_SIGNAL',r%head%cal%tatms,0,0,.true.,error)
  call sic_def_real ('ATM_IMAGE',r%head%cal%tatmi,0,0,.true.,error)
  call sic_def_real ('BEAM_THROW',r%head%bea%space,0,0,.false.,error)
  call sic_def_real ('RAPOS',r%head%dri%apos,0,0,.false.,error)
  call sic_def_inte ('NRECORDS',r%head%des%ndump,0,0,.true.,error)
  !
end subroutine las_variables_r_old
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine las_codes(error)
  use gkernel_interfaces
  use classcore_interfaces, except_this=>las_codes
  !---------------------------------------------------------------------
  ! @ private
  !  Define SIC variables providing the various codes used by the Class
  ! Data Format
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  ! Logical error flag
  !
  call sic_defstructure('CLASSCODES',.true.,error)
  if (error) return
  !
  ! Section codes
  call las_codes_sections('CLASSCODES',error)
  if (error)  return
  call sic_defstructure('CLASSCODES%SEC',.true.,error)
  if (error) return
  call las_codes_sections('CLASSCODES%SEC',error)
  if (error)  return
  !
  ! Quality of data
  call sic_defstructure('CLASSCODES%QUAL',.true.,error)
  if (error) return
  call las_codes_qual('CLASSCODES%QUAL',error)
  if (error)  return
  !
  ! Calibration mode
  call sic_defstructure('CLASSCODES%CALIB',.true.,error)
  if (error) return
  call las_codes_calmode('CLASSCODES%CALIB',error)
  if (error)  return
  !
end subroutine las_codes
!
subroutine las_codes_sections(parent,error)
  use class_parameter
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! @ private
  !  Define SIC variables providing the Section codes
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: parent  ! Parent structure name
  logical,          intent(inout) :: error   ! Logical error flag
  ! Local
  integer(kind=4), save :: sic_gen_sec=class_sec_gen_id+mx_sec+1
  integer(kind=4), save :: sic_pos_sec=class_sec_pos_id+mx_sec+1
  integer(kind=4), save :: sic_spe_sec=class_sec_spe_id+mx_sec+1
  integer(kind=4), save :: sic_res_sec=class_sec_res_id+mx_sec+1
  integer(kind=4), save :: sic_bas_sec=class_sec_bas_id+mx_sec+1
  integer(kind=4), save :: sic_his_sec=class_sec_his_id+mx_sec+1
  integer(kind=4), save :: sic_plo_sec=class_sec_plo_id+mx_sec+1
  integer(kind=4), save :: sic_swi_sec=class_sec_swi_id+mx_sec+1
  integer(kind=4), save :: sic_cal_sec=class_sec_cal_id+mx_sec+1
  integer(kind=4), save :: sic_sky_sec=class_sec_sky_id+mx_sec+1
  integer(kind=4), save :: sic_gau_sec=class_sec_gau_id+mx_sec+1
  integer(kind=4), save :: sic_she_sec=class_sec_she_id+mx_sec+1
  integer(kind=4), save :: sic_hfs_sec=class_sec_hfs_id+mx_sec+1
  integer(kind=4), save :: sic_abs_sec=class_sec_abs_id+mx_sec+1
  integer(kind=4), save :: sic_dri_sec=class_sec_dri_id+mx_sec+1
  integer(kind=4), save :: sic_bea_sec=class_sec_bea_id+mx_sec+1
  integer(kind=4), save :: sic_poi_sec=class_sec_poi_id+mx_sec+1
  integer(kind=4), save :: sic_her_sec=class_sec_her_id+mx_sec+1
  integer(kind=4), save :: sic_user_sec=class_sec_user_id+mx_sec+1
  !
  call sic_def_inte(trim(parent)//'%GEN',sic_gen_sec,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%POS',sic_pos_sec,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%SPE',sic_spe_sec,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%RES',sic_res_sec,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%BAS',sic_bas_sec,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%HIS',sic_his_sec,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%PLO',sic_plo_sec,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%FSW',sic_swi_sec,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%CAL',sic_cal_sec,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%SKY',sic_sky_sec,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%GAU',sic_gau_sec,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%SHE',sic_she_sec,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%HFS',sic_hfs_sec,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%ABS',sic_abs_sec,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%DRI',sic_dri_sec,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%BEA',sic_bea_sec,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%POI',sic_poi_sec,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%HER',sic_her_sec,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%USER',sic_user_sec,0,0,.true.,error)
  if (error)  return
  !
end subroutine las_codes_sections
!
subroutine las_codes_qual(parent,error)
  use gkernel_interfaces
  use class_parameter
  !---------------------------------------------------------------------
  ! @ private
  !  Define SIC variables providing the Quality codes
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: parent  ! Parent structure name
  logical,          intent(inout) :: error   ! Logical error flag
  ! Local
  integer(kind=4), save :: sic_qual_unknown=qual_unknown
  integer(kind=4), save :: sic_qual_excellent=qual_excellent
  integer(kind=4), save :: sic_qual_good=qual_good
  integer(kind=4), save :: sic_qual_fair=qual_fair
  integer(kind=4), save :: sic_qual_average=qual_average
  integer(kind=4), save :: sic_qual_poor=qual_poor
  integer(kind=4), save :: sic_qual_bad=qual_bad
  integer(kind=4), save :: sic_qual_awful=qual_awful
  integer(kind=4), save :: sic_qual_worst=qual_worst
  integer(kind=4), save :: sic_qual_deleted=qual_deleted
  !
  call sic_def_inte(trim(parent)//'%UNKNOWN',sic_qual_unknown,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%EXCELLENT',sic_qual_excellent,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%GOOD',sic_qual_good,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%FAIR',sic_qual_fair,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%AVERAGE',sic_qual_average,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%POOR',sic_qual_poor,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%BAD',sic_qual_bad,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%AWFUL',sic_qual_awful,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%WORST',sic_qual_worst,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%DELETED',sic_qual_deleted,0,0,.true.,error)
  if (error)  return
end subroutine las_codes_qual
!
subroutine las_codes_calmode(parent,error)
  use gkernel_interfaces
  use class_calibr
  !---------------------------------------------------------------------
  ! @ private
  !  Define SIC variables providing the Calibration mode codes
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: parent  ! Parent structure name
  logical,          intent(inout) :: error   ! Logical error flag
  ! Local
  integer(kind=4), save :: sic_mode_auto=mode_auto
  integer(kind=4), save :: sic_mode_trec=mode_trec
  integer(kind=4), save :: sic_mode_humi=mode_humi
  integer(kind=4), save :: sic_mode_manu=mode_manu
  !
  call sic_def_inte(trim(parent)//'%AUTO',sic_mode_auto,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%TREC',sic_mode_trec,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%HUMI',sic_mode_humi,0,0,.true.,error)
  call sic_def_inte(trim(parent)//'%MANU',sic_mode_manu,0,0,.true.,error)
  if (error)  return
end subroutine las_codes_calmode
