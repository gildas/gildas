subroutine class_box(set,line,r,error)
  use gbl_message
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_box
  use class_data
  use class_types
  !---------------------------------------------------------------------
  ! @ public
  ! CLASS Support routine for command
  !    BOX [C1 C2 C3 C4]
  ! 1      [/INDEX]
  ! 2      [/OBS]
  ! 3      [/UNIT Type [LOWER|UPPER]]
  ! Setup the units to plot a box (and a wedge).
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set    !
  character(len=*),    intent(in)    :: line   ! Command line
  type(observation),   intent(in)    :: r      !
  logical,             intent(inout) :: error  ! flag
  ! Local
  character(len=*), parameter :: rname='BOX'
  character(len=1) :: unit_up,unit_low,c1,c2,c3,c4
  character(len=32) :: argum,keyword
  logical :: doindex
  integer(kind=4) :: nc,ikey
  integer(kind=4), parameter :: mlowup=2
  character(len=5), parameter :: lowup(mlowup) = (/ 'LOWER','UPPER' /)
  integer(kind=4), parameter :: optindex=1
  integer(kind=4), parameter :: optobs=2
  integer(kind=4), parameter :: optunit=3
  !
  c1 = ''
  c2 = ''
  c3 = ''
  c4 = ''
  call sic_ke(line,0,1,c1,nc,.false.,error)
  if (error)  return
  call sic_ke(line,0,2,c2,nc,.false.,error)
  if (error)  return
  call sic_ke(line,0,3,c3,nc,.false.,error)
  if (error)  return
  call sic_ke(line,0,4,c4,nc,.false.,error)
  if (error)  return
  !
  doindex = set%action.eq.'I'
  if (sic_present(optindex,0) .and. sic_present(optobs,0)) then
     call class_message(seve%e,rname,'/INDEX and /OBS are exclusive from each other')
     error = .true.
     return
  elseif (sic_present(optindex,0)) then
     if (.not.associated(p%data2)) then
        call class_message(seve%e,rname,'No index loaded')
        error = .true.
        return
     endif
     doindex = .true.
  elseif (sic_present(optobs,0)) then
     doindex = .false.
  endif
  !
  ! Units for X axes
  argum = 'LOWER'
  call sic_ke(line,optunit,2,argum,nc,.false.,error)
  if (error)  return
  call sic_ambigs(rname,argum,keyword,ikey,lowup,mlowup,error)
  if (error)  return
  !
  unit_low = ''
  unit_up = ''
  if (keyword.eq.'LOWER') then
    call sic_ke(line,optunit,1,unit_low,nc,.false.,error)
    if (error)  return
  else
    call sic_ke(line,optunit,1,unit_up,nc,.false.,error)
    if (error)  return
  endif
  !
  ! Now check arguments and do the job
  call class_box_do(set,doindex,r,'Y',unit_low,unit_up,c1,c2,c3,c4,error)
  if (error)  return
  !
end subroutine class_box
!
subroutine class_box_default(set,doindex,r,aaname,error)
  use classcore_interfaces, except_this=>class_box_default
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  !  Plot a default box, suited for RY or index or the given Associated
  ! Array, i.e. shortcut for
  !         PLOT [Array] /OBS
  !      or PLOT /INDEX
  !  without any other customization.
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set      !
  logical,             intent(in)    :: doindex  !
  type(observation),   intent(in)    :: r        !
  character(len=*),    intent(in)    :: aaname   ! Assoc Array name, Y means RY
  logical,             intent(inout) :: error    !
  !
  call class_box_do(set,doindex,r,aaname,'','','','','','',error)
  if (error)  return
  !
end subroutine class_box_default
!
subroutine class_box_do(set,doindex,r,aaname,chlow,chup,ch1,ch2,ch3,ch4,error)
  use gbl_message
  use classcore_interfaces, except_this=>class_box_do
  use class_data
  !---------------------------------------------------------------------
  ! @ private
  !  Check arguments and do the job
  !  Empty arguments mean to use internal default
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set              !
  logical,             intent(in)    :: doindex          !
  type(observation),   intent(in)    :: r                !
  character(len=*),    intent(in)    :: aaname           ! Assoc Array name, Y means RY
  character(len=*),    intent(in)    :: chlow,chup       !
  character(len=*),    intent(in)    :: ch1,ch2,ch3,ch4  !
  logical,             intent(inout) :: error            !
  ! Local
  character(len=*), parameter :: rname='BOX'
  character(len=1) :: def_low,def_up,unit_low,unit_up,c1,c2,c3,c4
  !
  if (doindex .and. aaname.ne.'Y') then
    call class_message(seve%e,rname,'/INDEX not implemented for Associated Arrays')
    error = .true.
    return
  endif
  !
  ! Defaults, if no value provided
  c1 = ch1
  c2 = ch2
  c3 = ch3
  c4 = ch4
  if (c1.eq.'')  c1 = 'P'  ! Parallel
  if (c2.eq.'')  c2 = 'O'  ! Orthogonal
  if (c3.eq.'')  c3 = 'I'  ! Ticks are in
  if (c4.eq.'')  c4 = ' '  ! Xup same as Xlow
  !
  if (index('PON', c1).eq.0 .or.  &
      index('PON', c2).eq.0 .or.  &
      index('POIN',c3).eq.0 .or.  &
      index('PON ',c4).eq.0) then
    call class_message(seve%e,rname,'Error in arguments')
    error = .true.
    return
  endif
  !
  unit_low = chlow
  unit_up = chup
  call geunit(set,r%head,def_low,def_up)  ! Default
  if (unit_low.eq.'')  unit_low = def_low
  if (unit_up.eq.'')   unit_up = def_up
  !
  if (doindex) then
     call class_box_sub(set,p,aaname,unit_low,unit_up,c1,c2,c3,c4,.true.,error)
     if (error)  return
     call gr_exec2('WEDGE')
  else
     call class_box_sub(set,r,aaname,unit_low,unit_up,c1,c2,c3,c4,.false.,error)
     if (error)  return
  endif
  !
end subroutine class_box_do
!
subroutine class_box_sub(set,obs,aaname,unit_low,unit_up,c1,c2,c3,c4,  &
  plot2d,error)
  use gbl_constant
  use classcore_dependencies_interfaces
  use classcore_interfaces, except_this=>class_box_sub
  use class_setup_new
  use class_data
  use class_types
  use plot_formula
  !---------------------------------------------------------------------
  ! @ private
  !   Plot a standard box, with the requested X axis units for lower
  ! and upper axis.
  !---------------------------------------------------------------------
  type(class_setup_t), intent(in)    :: set       !
  type(observation),   intent(in)    :: obs       !
  character(len=*),    intent(in)    :: aaname    ! Assoc Array name, Y means RY
  character(len=*),    intent(in)    :: unit_low  ! Lower axis unit
  character(len=*),    intent(in)    :: unit_up   ! Upper axis unit
  character(len=*),    intent(in)    :: c1        ! Parallel   Orthogonal None
  character(len=*),    intent(in)    :: c2        ! Orthogonal Parallel None
  character(len=*),    intent(in)    :: c3        ! In Out
  character(len=*),    intent(in)    :: c4        ! X Y (for labels)
  logical,             intent(in)    :: plot2d    ! 2D data
  logical,             intent(inout) :: error     ! flag
  ! Local
  character(len=1) :: c0
  character(len=30) :: chain
  character(len=132) :: ch
  real(kind=8) :: b1,b2
  real(kind=plot_length) :: he,uy1,uy2,ux1,ux2,ux,uy
  real(kind=4) :: cdef,expa,x1,y1
  integer(kind=4) :: nchain
  !
  call sic_get_real('CHARACTER_SIZE',cdef,error)
  if (error) return
  expa = 1.0 ! assumed ...
  he = cdef*expa
  ! 1- Compute limits of spectrum or 2D data
  ! 2- Send the plot limits to GreG
  ! 3- If 1D data, Y-limits are the Y spectrum ones
  !    If 2D data, Y-limits are the number of records
  call get_box(gx1,gx2,gy1,gy2)
  !
  if (aaname.eq.'Y' .or. set%modey.eq.'F') then
    ! Y scale for RY (precomputed) or Fixed scale
    call gelimy (uy1,uy2,uy)
    call gulimx (ux1,ux2,ux)
    if (.not.plot2d) then
      b1 = dble(uy1)
      b2 = dble(uy2)
    else
      b1 = guz1
      b2 = guz2
    endif
  else
    ! Automatic scale for Associated Array
    call class_assoc_minmax(obs,aaname,x1,y1,error)
    if (error)  return
    call selimy(x1,y1)
    b1 = x1
    b2 = y1
  endif
  write(ch,'(A,4(1X,G20.13))') 'LIMITS ',gux1,gux2,b1,b2
  call gr_exec(ch)
  error = gr_error()
  if (error) return
  !
  ! XLow
  call sub_box_xaxis('XL',unit_low,c1,error)
  if (error)  return
  !
  ! Text for lower X axis
!!$  if (c1.ne.'N' .and. (c4.eq.'X'.or.c4.eq.' ')) then
  if (c1.ne.'N') then
     call sic_get_char('TEXT_LOW',chain,nchain,error)
     if (error) then
        call textx(unit_low,chain,nchain,obs%head%gen%kind)
        error = .false.
     endif
     call gr_exec('LABEL "'//chain(1:nchain)//'" /X')
     error = gr_error()
     if (error) return
  endif
  !
  ! YRight
  write(ch,1000) 'AXIS YR',b1,b2,' /TICK ',c3
  call gr_exec(ch)
  error = gr_error()
  if (error) return
  !
  ! YLeft
  write(ch,1000) 'AXIS YL',b1,b2,' /TICK ',c3,' /LABEL ',c2
  call gr_exec(ch)
  error = gr_error()
  if (error) return
  ! Text for Yleft axis if 2D plot
  if (c2.ne.'N') then
    if (plot2d) then
      chain = 'Sort: '//adjustl(set%sort_name)
    else
      if (obs%head%gen%yunit.eq.yunit_unknown) then
        chain = ''
      else
        chain = obs_yunit(obs%head%gen%yunit)
      endif
    endif
    nchain = len_trim(chain)
    if (nchain.gt.0) then
      y1 = 0.5*(gy1+gy2)
      x1 = max(he,gx1 - 6.0 * he)
      call grelocate(x1,y1)
      call gr_exec('LABEL "'//chain(1:nchain)//'" 90 /CENTERING 5')
      error = gr_error()
      if (error) return
    endif
  endif
  !
  ! Text for upper X axis
!!$  if (unit_up.ne.unit_low.and.unit_up.ne.' ' .and.   &
!!$     &    (c1.ne.'N' .and. (c4.eq.'X'.or.c4.eq.' '))) then
  if (unit_up.ne.unit_low .and. unit_up.ne.' ') then
     if (c4.eq.'O'.or.c4.eq.'P') then
        y1 = gy2 + 2*he
        x1 = (gx1 + gx2) /2
        call sic_get_char('TEXT_UP',chain,nchain,error)
        if (error) then
           call textx(unit_up,chain,nchain,obs%head%gen%kind)
           error = .false.
        endif
        call grelocate(x1,y1)
        call gr_exec('LABEL "'//chain(1:nchain)//'" /CENTERING 5')
        error = gr_error()
        if (error) return
        c0 = c4
     else if (c4.eq.' ') then
        c0 = c1
     else
        c0 = 'N'
     endif
  else
     c0 = 'N'
  endif
  call sub_box_xaxis('XU',unit_up,c0,error)
  if (error)  return
  !
1000 format(a,2(1x,g20.13),a,a1,a,a1)
  !
contains
  subroutine sub_box_xaxis(xname,unit,labeling,error)
    character(len=*), intent(in)    :: xname
    character(len=*), intent(in)    :: unit
    character(len=*), intent(in)    :: labeling  ! P, O, or N
    logical,          intent(inout) :: error
    ! Local
    real(kind=8) :: ax0,a1,a2
    real(kind=plot_length) :: ax1,ax2,ax
    !
    call gelimx(ax0,ax1,ax2,ax,unit)
    !
    if (unit.eq.'A') then  ! Angle
      a1 = (ax0+ax1)*class_setup_get_fangle()
      a2 = (ax0+ax2)*class_setup_get_fangle()
    else
      a1 = ax0+ax1
      a2 = ax0+ax2
    endif
    write(ch,'(2(A,1X),2(1X,G20.13),A,A1,A,A1)')  &
      'AXIS',xname,a1,a2,' /TICK ',c3,' /LABEL ',labeling
    call gr_exec(ch)
    if (gr_error())  error = .true.
    !
  end subroutine sub_box_xaxis
end subroutine class_box_sub
!
subroutine textx(unit,text,ntext,kind)
  use gbl_constant
  !-------------------------------------------------------------------
  ! @ private
  ! Return a text for axis labels according to specified unit and
  ! observation type
  !-------------------------------------------------------------------
  character(len=1), intent(in)  :: unit   ! X axis unit
  character(len=*), intent(out) :: text   ! Output full text
  integer(kind=4),  intent(out) :: ntext  ! TEXT length
  integer(kind=4),  intent(in)  :: kind   ! Observation kind
  !
  text = ''
  if (kind.eq.kind_spec) then
     select case (unit)
     case('C')
        text = 'Channel Number'
     case('F')
        text = 'Rest Frequency (MHz)'
     case('I')
        text = 'Image Frequency (MHz)'
     case('V')
        text = 'Velocity (km/s)'
     case('W')
        text = 'Wavelength (micron)'
     end select
  else
     select case (unit)
     case('C')
        text = 'Dump Number'
     case('T')
        text = 'Time Offset (sec)'
     case('S')
        text = 'Angular Offset (arcsec)'
     case('M')
        text = 'Angular Offset (arcmin)'
     case('D')
        text = 'Angular Offset (degree)'
     case('R')
        text = 'Angular Offset (radian)'
     end select
  endif
  ntext = len_trim(text)
end subroutine textx
!
subroutine texty(unit,text,ntext,kind)
  use gbl_constant
  use gbl_message
  !-------------------------------------------------------------------
  ! @ private
  ! Return a text for axis labels according to specified unit and
  ! observation type
  !-------------------------------------------------------------------
  character(len=*), intent(in)  :: unit   ! X axis unit
  character(len=*), intent(out) :: text   ! Output full text
  integer(kind=4),  intent(out) :: ntext  ! TEXT length
  integer(kind=4),  intent(in)  :: kind   ! Observation kind
  !
  if (kind.eq.kind_spec) then
     text = 'Sort: '//trim(adjustl(unit))
  else
     call class_message(seve%w,'TEXTY','Not implemented for continuum drifts')
     text = ''
  endif
  ntext = len_trim(text)
end subroutine texty
