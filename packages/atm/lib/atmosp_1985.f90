subroutine atm_1985_atmosp(t0,p0,h0)
  !----------------------------------------------------------------------
  ! @ private
  ! 1985 ATM version
  !
  ! Compute an atmospheric model, interpolated between standard atmospheres
  ! of winter and summer (subroutines ase45 and asj 45), to fit with temperature
  ! t0 (k) and pressure p0 (mbar) at altitude h0 (km).
  ! 15 layers are used.
  ! The transmission of the model atmosphere can then be computed by calling
  ! entry point transm.
  !----------------------------------------------------------------------
  real(kind=4), intent(in) :: t0  ! [K]    Temperature
  real(kind=4), intent(in) :: p0  ! [mbar] Pressure
  real(kind=4), intent(in) :: h0  ! [km]   Altitude
  ! Local
  integer mp
  parameter (mp=80)
  real  t(mp), h(mp), p(mp), r(mp), rr(mp),   &
    pe, te, de, pj, tj, dj, ape, ate, apj, atj, p1,   &
    t1, d1, height, r1, water, airmass, tauw, tauox, taut, temi,   &
    freq, tag, tox, tagu, toxi, tatm
  integer np, j, ier
  real path
  real dpath, z, pr_ag
  real*8 n_index, c_snell
  !
  save t,h,p,r,np
  !
  ! average summer and winter model atmospheres according to
  ! given values of temperature and pression
  call ase45(pe,te,de,h0)
  call asj45(pj,tj,dj,h0)
  ape = (p0-pj)/(pe-pj)
  apj = (p0-pe)/(pj-pe)
  ate = (t0-tj)/(te-tj)
  atj = (t0-te)/(tj-te)
  !
  ! set layers
  do j=1,6
    h(j) = .5e5
  enddo
  do j=7,12
    h(j) = 2.e5
  enddo
  do j=13,15
    h(j) = 15.e5
  enddo
  np = 15
  !
  ! Set t,p, and r (H2O for 1mm precipitable content) profiles
  height = h0
  p1 = p0
  t1 = t0
  r1 = .5
  do j = 1, np
    height = height + h(j)/100000. ! in km.
    p(j) = p1
    t(j) = t1
    r(j) = r1
    call ase45(pe,te,de,height)
    call asj45(pj,tj,dj,height)
    p1 = ape*pe+apj*pj
    t1 = ate*te+atj*tj
    d1 = de*(1+(p1-pe)/pe-(t1-te)/te)
    r1 = .5*exp(-.5*(height-h0))
    if(height.gt.15.) r1 = r1 + d1*2e-6
    p(j) = (p(j) + p1)/2.
    t(j) = (t(j) + t1)/2.
    r(j) = (r(j) + r1)/2.
  enddo
  return
  !
entry atm_1985_transm(water,airmass,freq,temi,tatm,tauox,tauw,taut,ier)
  !----------------------------------------------------------------------
  ! Compute atmospheric emission and  absorption.
  !
  ! Input:
  ! 	water 	R	H2O precipitable content(mm)
  !	airmass R	Number of air masses
  !	freq 	R	Frequency		(GHz)
  !
  ! Output:
  !	temi	R	atmosph emission	(K)
  !	tatm   	R	mean temperature	(K)
  !	tauox	R	Oxygen optical depth  AT ZENITH	(nepers)
  !       tauw   	R	Water  optical depth  AT ZENITH	(nepers)
  !       taut   	R	Total  optical depth  AT ZENITH	(nepers)
  !	IER	I	Error code
  !----------------------------------------------------------------------
  do j = 1, np
    rr(j) = r(j) * water
  enddo
  ier = 0
  call kvatm(np,p,t,rr,h,tauw,tauox,freq,temi,tatm,tag,tagu,tox,   &
    toxi,0,0,taut,airmass,ier)
  tauox = tauox / airmass      ! RL 14 MAR 86
  tauw = tauw / airmass        !
  taut = taut / airmass        !
  return
  !
entry atm_1985_path(water,airmass,freq,path,ier)
  !----------------------------------------------------------------------
  !       integrated optical pathlength of atmosphere
  !
  !	np .... numero de capas
  !	h  .... espesor de las capas     (cm)
  !	p  .... presion (milibares)
  !	t  .... temperatura (k)
  !	rho ... cantidad de vapor de agua (gr/m**3)
  !----------------------------------------------------------------------
  !-----------------------------------------------------------------------
  ! MB: zenith distance angle from airmass (parallel layers):
  z = acos( 1. / airmass)
  c_snell = -1
  path    = 0.
  !
  do j=1,np
    !
    ! partial pressure of water vapor. Rspec = Rgas/M_H2O = 8314/18.02 = 461.4
    ! Conversion from pascal->mbar 1e-2, g->kg 1e-3:
    pr_ag = 4.614e-03  * t(j) * r(j) * water
    call excess_path (freq, p(j), pr_ag, t(j), h(j), z, dpath, c_snell, n_index)
    ! IF (J .EQ. 1) DI = Z - ASIN(SIN(Z) / N_INDEX)
    path = path + dpath
  enddo
end subroutine atm_1985_atmosp
