!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage the ATM package
! Mostly empty because ATM is just a library
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine atm_pack_set(pack)
  use gpack_def
  use gkernel_interfaces
  use atm_interfaces, except_this=>atm_pack_set
  !----------------------------------------------------------------------
  ! @ public
  !----------------------------------------------------------------------
  type(gpack_info_t), intent(out) :: pack
  !
  pack%name='atm'
  pack%init=locwrd(atm_pack_init)
  !
end subroutine atm_pack_set
!
subroutine atm_pack_init(gpack_id,error)
  use sic_def
  use atm_interfaces, except_this=>atm_pack_init
  !----------------------------------------------------------------------
  ! @ private-mandatory
  !----------------------------------------------------------------------
  integer, intent(in) :: gpack_id
  logical, intent(inout) :: error
  !
  ! Synchronize Fortran support variables of SIC variables with
  ! the internal default values of ATM library
  call atm_atmosp_variables_get(error)
  !
end subroutine atm_pack_init
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
