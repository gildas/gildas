module atm_interfaces_public
  interface
    subroutine atm_message_set_id(id)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Alter library id into input id. Should be called by the library
      ! which wants to share its id with the current one.
      !---------------------------------------------------------------------
      integer, intent(in) :: id
    end subroutine atm_message_set_id
  end interface
  !
  interface
    subroutine atm_i(error)
      !-----------------------------------------------------------------------
      ! @ public
      ! Make sure the atm data will be interpolated from file
      !-----------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine atm_i
  end interface
  !
  interface
    subroutine atm_atmosp_i(tem,pre,alt)
      use gildas_def
      !-----------------------------------------------------------------------
      ! @ public
      !-----------------------------------------------------------------------
      real(kind=4), intent(in) :: tem
      real(kind=4), intent(in) :: pre
      real(kind=4), intent(in) :: alt
    end subroutine atm_atmosp_i
  end interface
  !
  interface
    subroutine atmos_i_table(compute,file,nfile,n_f,fmin,fmax,h0,error)
      use gildas_def
      use gbl_format
      use gbl_message
      !-----------------------------------------------------------------------
      ! @ public
      ! Write a table of atmospheric emission data or further interpolation.
      !-----------------------------------------------------------------------
      logical,          intent(in)    :: compute
      character(len=*), intent(inout) :: file
      integer(kind=4),  intent(out)   :: nfile
      integer(kind=4),  intent(in)    :: n_f
      real(kind=4),     intent(in)    :: fmin
      real(kind=4),     intent(in)    :: fmax
      real(kind=4),     intent(in)    :: h0
      logical,          intent(inout) :: error
    end subroutine atmos_i_table
  end interface
  !
  interface
    subroutine atm_pack_set(pack)
      use gpack_def
      !----------------------------------------------------------------------
      ! @ public
      !----------------------------------------------------------------------
      type(gpack_info_t), intent(out) :: pack
    end subroutine atm_pack_set
  end interface
  !
  interface
    subroutine atm_sicvariables(error)
      !---------------------------------------------------------------------
      ! @ public
      ! Instantiate SIC variables
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine atm_sicvariables
  end interface
  !
  interface
    subroutine atm_print(error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Displays the values of ATM parameters
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine atm_print
  end interface
  !
  interface
    subroutine atm_setup(inversion,error)
      !---------------------------------------------------------------------
      ! @ public
      ! Set the ATM version to be used. Supported versions are 1985 and
      ! 2009. 'OLD' (resp. 'NEW') is an alias for 1985 (resp. 2009).
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: inversion  ! User input version
      logical,          intent(inout) :: error      ! Error flag
    end subroutine atm_setup
  end interface
  !
  interface
    function atm_get_version()
      !---------------------------------------------------------------------
      ! @ public
      ! get atm version
      !---------------------------------------------------------------------
      character(len=4) :: atm_get_version
    end function atm_get_version
  end interface
  !
  interface
    subroutine atm_atmosp(t0,p0,h0)
      !---------------------------------------------------------------------
      ! @ public
      ! Setup the atmospheric structure
      !---------------------------------------------------------------------
      real, intent(in) :: t0  ! Ground temperature [K]
      real, intent(in) :: p0  ! Ground pressure [hPa]
      real, intent(in) :: h0  ! Altitude of the site [km]
    end subroutine atm_atmosp
  end interface
  !
  interface
    subroutine atm_path(water,airmass,freq,path,ier)
      !---------------------------------------------------------------------
      ! @ public
      ! Integrated optical length of current atmosphere
      !---------------------------------------------------------------------
      real,    intent(in)  :: water    ! Precipitable water vapor [mm]
      real,    intent(in)  :: airmass  ! Number of air masses []
      real,    intent(in)  :: freq     ! Frequency [GHz]
      real,    intent(out) :: path     ! Optical length [cm]
      integer, intent(out) :: ier      ! Error status
    end subroutine atm_path
  end interface
  !
  interface
    subroutine atm_atmosp_get(ctype_out,humidity_out,wvsh_out,tlr_out,top_out,  &
      p_step_out,p_step_factor_out,error)
      !---------------------------------------------------------------------
      ! @ public
      ! Public entry point to:
      !  1) get the C++ global variables into the Fortran-SIC ones,
      !  2) return these latter ones.
      ! Warning: these variables make sense only with ATM 2009 or 2016.
      ! Return ATM 2009 or 2016 values even if it is not currently used (not
      ! an error).
      !---------------------------------------------------------------------
      character(len=*), intent(out)    :: ctype_out          ! Atmospheric type (as a string)
      real(4),          intent(out)    :: humidity_out       ! [%]    Ground Relative Humidity (indication)
      real(4),          intent(out)    :: wvsh_out           ! [km]   Water vapor scale height
      real(4),          intent(out)    :: tlr_out            ! [K/km] Tropospheric lapse rate
      real(4),          intent(out)    :: top_out            ! [km]   Upper atm. boundary for calculations
      real(4),          intent(out)    :: p_step_out         ! [mb]   Primary pressure step
      real(4),          intent(out)    :: p_step_factor_out  ! []     Pressure step ratio between two consecutive layers
      logical,          intent(inout)  :: error              ! Logical error flag
    end subroutine atm_atmosp_get
  end interface
  !
  interface
    subroutine atm_atmosp_set(ctype_in,humidity_in,wvsh_in,tlr_in,top_in,  &
      p_step_in,p_step_factor_in,error)
      !---------------------------------------------------------------------
      ! @ public
      ! Public entry point to:
      !  1) set the Fortran-SIC global variables values,
      !  2) set the C++ global variables from these values.
      ! Warning: these variables make sense only with ATM 2009 or 2016.
      ! Nothing will be done if ATM 2009 or 2016 is not the version in use
      ! (not an error). NB: 'ctype_in' is not case-sensitive, and can be
      ! shorten as long as it is not ambiguous.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: ctype_in          ! Atmospheric type (as a string)
      real(4),          intent(in)    :: humidity_in       ! [%]    Ground Relative Humidity (indication)
      real(4),          intent(in)    :: wvsh_in           ! [km]   Water vapor scale height
      real(4),          intent(in)    :: tlr_in            ! [K/km] Tropospheric lapse rate
      real(4),          intent(in)    :: top_in            ! [km]   Upper atm. boundary for calculations
      real(4),          intent(in)    :: p_step_in         ! [mb]   Primary pressure step
      real(4),          intent(in)    :: p_step_factor_in  ! []     Pressure step ratio between two consecutive layers
      logical,          intent(inout) :: error             ! Logical error flag
    end subroutine atm_atmosp_set
  end interface
  !
  interface atm_transm
    subroutine atm_transm_0d(water,airmass,freq,temi,tatm,tauox,tauw,taut,ier)
      !---------------------------------------------------------------------
      ! @ public-generic atm_transm
      ! Return the atmospheric temperatures and opacities given the amount
      ! of water and the frequency, for current atmosphere previously set
      ! with 'atm_atmosp()'
      !---
      ! This version for scalar frequency as input
      !---------------------------------------------------------------------
      real(kind=4),    intent(in)  :: water    ! [mm]    Precipitable water vapo
      real(kind=4),    intent(in)  :: airmass  ! []      Number of air masses
      real(kind=4),    intent(in)  :: freq     ! [GHz]   Frequency
      real(kind=4),    intent(out) :: temi     ! [K]     Atmospheric emission
      real(kind=4),    intent(out) :: tatm     ! [K]     Mean temperature
      real(kind=4),    intent(out) :: tauox    ! [neper] Opacity (dry component)
      real(kind=4),    intent(out) :: tauw     ! [neper] Opacity (wet component)
      real(kind=4),    intent(out) :: taut     ! [neper] Total opacity
      integer(kind=4), intent(out) :: ier      ! []      Error status
    end subroutine atm_transm_0d
    subroutine atm_transm_1d(water,airmass,freq,temi,tatm,tauox,tauw,taut,ier)
      !---------------------------------------------------------------------
      ! @ public-generic atm_transm
      ! Return the atmospheric temperatures and opacities given the amount
      ! of water and the frequency, for current atmosphere previously set
      ! with 'atm_atmosp()'
      !---
      ! This version for vector frequency as input, and vector outputs
      !---------------------------------------------------------------------
      real(kind=4),    intent(in)  :: water     ! [mm]    Precipitable water vapor
      real(kind=4),    intent(in)  :: airmass   ! []      Number of air masses
      real(kind=4),    intent(in)  :: freq(:)   ! [GHz]   Frequency
      real(kind=4),    intent(out) :: temi(:)   ! [K]     Atmospheric emission
      real(kind=4),    intent(out) :: tatm(:)   ! [K]     Mean temperature
      real(kind=4),    intent(out) :: tauox(:)  ! [neper] Opacity (dry component)
      real(kind=4),    intent(out) :: tauw(:)   ! [neper] Opacity (wet component)
      real(kind=4),    intent(out) :: taut(:)   ! [neper] Total opacity
      integer(kind=4), intent(out) :: ier(:)    ! []      Error status
    end subroutine atm_transm_1d
  end interface atm_transm
  !
end module atm_interfaces_public
