function flin(v,vl,dv)
  !----------------------------------------------------------------------
  ! J Cernicharo, Model atmosphere.
  !
  !	FORMA CINETICA DEL PERFIL
  !	V... FRECUENCIA
  !	VL.. FRECUENCIA DE LA LINEA
  !	DV.. ANCHURA DE LA LINEA
  !----------------------------------------------------------------------
  real flin,v,vl,dv,pi,v2
  data pi/3.141592654/
  !
  flin=4.*v*vl*dv/pi
  v2=v*v
  flin=flin/(4.*v2*dv*dv+(vl*vl-v2)**2)
end function flin
