function pwat(t,u)
  !----------------------------------------------------------------------
  !	calculo de la presion parcial del vapor de agua
  !	t es la temperatura en grados kelvin
  !	p es la presion atmosferica total en milibares
  !	u es la humedad relativa
  !	pwat es la presion parcial del vapor de agua
  !
  !
  !	calculo de la presion de saturacion
  !
  ! J Cernicharo
  !----------------------------------------------------------------------
  real pwat,t,u
  real p,es,e
  !
  p=1013.
  es=6.105*exp(25.22/t*(t-273.)-5.31*alog(t/273.))
  e=1.-(1.-u/100.)*es/p
  pwat=es*u/100.*e
  pwat=pwat*216.5/t
  return
end function pwat
