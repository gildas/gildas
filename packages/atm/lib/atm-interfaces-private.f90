module atm_interfaces_private
  interface
    subroutine atm_message(mkind,procname,message)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Messaging facility for the current library. Calls the low-level
      ! messaging routine with its own identifier.
      !---------------------------------------------------------------------
      integer,          intent(in) :: mkind     ! Message kind
      character(len=*), intent(in) :: procname  ! Name of calling procedure
      character(len=*), intent(in) :: message   ! Message string
    end subroutine atm_message
  end interface
  !
  interface
    subroutine atm_1985_atmosp(t0,p0,h0)
      !----------------------------------------------------------------------
      ! @ private
      ! 1985 ATM version
      !
      ! Compute an atmospheric model, interpolated between standard atmospheres
      ! of winter and summer (subroutines ase45 and asj 45), to fit with temperature
      ! t0 (k) and pressure p0 (mbar) at altitude h0 (km).
      ! 15 layers are used.
      ! The transmission of the model atmosphere can then be computed by calling
      ! entry point transm.
      !----------------------------------------------------------------------
      real(kind=4), intent(in) :: t0  ! [K]    Temperature
      real(kind=4), intent(in) :: p0  ! [mbar] Pressure
      real(kind=4), intent(in) :: h0  ! [km]   Altitude
    end subroutine atm_1985_atmosp
  end interface
  !
  interface
    subroutine atm_read_table(error)
      use gildas_def
      use gbl_message
      use gbl_format
      use gio_convert
      !-----------------------------------------------------------------------
      ! @ private
      ! Actually read the table file to be interpolated.
      !-----------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine atm_read_table
  end interface
  !
  interface
    subroutine sub_atm_decode(lun,buffer,r4,r8,i4,cc,error)
      use gildas_def
      use gbl_message
      !-----------------------------------------------------------------------
      ! @ private
      !-----------------------------------------------------------------------
      integer(kind=4), intent(in)    :: lun
      integer(kind=4), intent(inout) :: buffer(128)
      external                       :: r4
      external                       :: i4
      external                       :: r8
      external                       :: cc
      logical,         intent(inout) :: error
    end subroutine sub_atm_decode
  end interface
  !
  interface
    subroutine reallocate_atm_table(np,nt,nf,nw,na,error)
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: np
      integer(kind=4), intent(in)    :: nt
      integer(kind=4), intent(in)    :: nf
      integer(kind=4), intent(in)    :: nw
      integer(kind=4), intent(in)    :: na
      logical,         intent(inout) :: error
    end subroutine reallocate_atm_table
  end interface
  !
  interface
    subroutine indexp(n,xt,x,ix,xi)
      !-----------------------------------------------------------------------
      ! @ private
      ! computes the index and fractional increment of value x in table xt
      ! e.g.       x = xt(ix)+ xi*(xt(ix+1)-xt(ix))
      ! returns ix=1, xi=0. if x < xt(1)
      !         ix=n, xi=0. if x > xt(n)
      !-----------------------------------------------------------------------
      integer(kind=4), intent(in)  :: n      ! dimension of table xt
      real(kind=4),    intent(in)  :: xt(n)  ! input table
      real(kind=4),    intent(in)  :: x      ! input value of x
      real(kind=4),    intent(out) :: xi     ! fractional increment
      integer(kind=4), intent(out) :: ix     ! index
    end subroutine indexp
  end interface
  !
  interface
    subroutine interp(nv,n,t,ix,xi,v,w)
      !-----------------------------------------------------------------------
      ! @ private
      !-----------------------------------------------------------------------
      integer(kind=4), intent(in)    :: nv        ! number of variables
      integer(kind=4), intent(in)    :: n(nv)     ! dimensions of the table
      real(kind=4),    intent(in)    :: t(*)      ! table (nv dimensions)
      integer(kind=4), intent(in)    :: ix(nv)    ! index of x value xx where the function is to be taken
      real(kind=4),    intent(in)    :: xi(nv)    ! fractional increment of x value e.g. xx = x(ix)+xi*(x(ix+1)-x(ix))
      real(kind=4),    intent(out)   :: v         ! interpolated value (output)
      real(kind=4),    intent(inout) :: w(nv,*)   ! work array of dim (nv,2**nv)
    end subroutine interp
  end interface
  !
  interface
    subroutine atmos_i_table_fill(n_f,fmin,fmax,h0,error)
      use gildas_def
      use gbl_format
      use gbl_message
      use gkernel_types
      !-----------------------------------------------------------------------
      ! @ private
      ! Compute the table in the ATM memory buffer
      !-----------------------------------------------------------------------
      integer(kind=4), intent(in)    :: n_f
      real(kind=4),    intent(in)    :: fmin
      real(kind=4),    intent(in)    :: fmax
      real(kind=4),    intent(in)    :: h0
      logical,         intent(inout) :: error
    end subroutine atmos_i_table_fill
  end interface
  !
  interface
    subroutine atm_sicvariables_table(error)
      use gildas_def
      !---------------------------------------------------------------------
      ! @ private
      !  Define the structure ATM%TABLE mapped on the table loaded in memory
      !---------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine atm_sicvariables_table
  end interface
  !
  interface
    subroutine atm_pack_init(gpack_id,error)
      use sic_def
      !----------------------------------------------------------------------
      ! @ private-mandatory
      !----------------------------------------------------------------------
      integer, intent(in) :: gpack_id
      logical, intent(inout) :: error
    end subroutine atm_pack_init
  end interface
  !
  interface
    subroutine atm_atmosp_variables_get(error)
      !---------------------------------------------------------------------
      ! @ private
      ! Get the atmospheric parameters currently in use in ATM, i.e. copy
      ! the C++ global variables in the Fortran-SIC global ones.
      ! Always get from 2009 to avoid mixing several set of parameters for
      ! each ATM version.
      !---------------------------------------------------------------------
      logical, intent(inout) :: error         ! Logical error flag
    end subroutine atm_atmosp_variables_get
  end interface
  !
  interface
    subroutine atm_atmosp_variables_set(error)
      !---------------------------------------------------------------------
      ! @ private
      ! Set the atmospheric parameters to be used in ATM, i.e. copy the
      ! Fortran-SIC global variables in the C++ ones.
      !---------------------------------------------------------------------
      logical, intent(inout) :: error         ! Logical error flag
    end subroutine atm_atmosp_variables_set
  end interface
  !
  interface
    subroutine atm_20XX_atmosp_type(itype,ctype,int2str,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Translate the atmosphere type either from integer to string or from
      ! string to integer.
      !---------------------------------------------------------------------
      integer,          intent(inout) :: itype   ! Type as integer
      character(len=*), intent(inout) :: ctype   ! Type as string
      logical,          intent(in)    :: int2str ! Convert integer to string or vice-versa?
      logical,          intent(inout) :: error   ! Logical error flag
    end subroutine atm_20XX_atmosp_type
  end interface
  !
end module atm_interfaces_private
