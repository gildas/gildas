module atm_interfaces_none
  interface
    subroutine ase45(p,t,d,ha)
      ! J CERNICHARO
      !
      !	ATMOSFERA U.S. 1962 MES DE ENERO 45 GRADOS DE LATITUD NORTE
      !	HA ES LA ALTURA A LA QUE SE QUIEREN CALCULAR LA PRESION,
      !	(MILIBARES),LA TEMPERATURA (K) Y LA DENSIDAD (GR/M**3)
      !	HA DEBE ESTAR EN KM
      !
      !	P === PRESION
      !	T === TEMPERATURA
      !	D === DENSIDAD
      !
      real p,t,d,ha
    end subroutine ase45
  end interface
  !
  interface
    subroutine asj45(p,t,d,ha)
      ! J CERNICHARO
      !
      !	ATMOSFERA STANDARD U.S. 1962 MES DE JULIO 45 GRADOS LATITUD NORTE
      !	HA === ALTURA EN KM DONDE SE DESEA CALCULAR P,T,D
      !	P  === PRESION EN MILIBARES
      !	D  === DENSIDAD EN GR/M**3
      !	T  === TEMPERATURA EN K
      !
      real p,t,d,ha
    end subroutine asj45
  end interface
  !
  interface
    subroutine excess_path (f_ghz, p_atm, p_vap, t, dh,   &
         &    z, path, c_snell, n_index)
      !--------------------------------------------------------------------------
      !
      ! Calculation of the excess path length (path length difference between
      ! vacuum and atmospheric propagation) for a parallel medium of
      ! constant refraction index.
      ! Source for the path length formula: Thompson, Moran, Swenson (1986),
      !                     Interferometry and Synthesis in Radio Astronomy, p 407
      !
      ! Input:  real*4 f_ghz   frequency in GHz
      !                p_atm   total atmospherical pressure in millibar
      !                p_vap   partial pressure of water vapor in millibar
      !                t       temperature in kelvin
      !                z       Zenith distance angle (radians)
      ! Output: real*8 c_snell : Constant of Snell's law
      !                n_index : refraction index
      !         real*4 path    : path length difference in units of dh.
      !
      ! Author: 25-Jan-1994  Michael Bremer, IRAM
      !-------------------------------------------------------------------------
      real refract_total, f_ghz, p_atm, p_vap, t, dh, z, path, refr
      real*8 sin_z, cos_z,  c_snell, n_index
    end subroutine excess_path
  end interface
  !
  interface
    function refract_total (f_ghz, p_atm, p_vap, t )
      !-------------------------------------------------------------------
      !
      ! Calculation of the total atmospheric refractivity (dry component and
      ! water vapor), taking into account the dependences from
      ! frequency, pressure, temperature
      ! Source of formulae: Hill and Cliffort (1981), Radio Science 16, pp. 77-82
      !                and  Thompson, Moran, Swenson (1986),
      !                     Interferometry and Synthesis in Radio Astronomy, p 407
      !
      ! Input: real*4 f_ghz  frequency in GHz
      !               p_atm  total atmospherical pressure in millibar
      !               p_vap  partial pressure of water vapor in millibar
      !               t      temperature in kelvin
      !
      ! Author: 25-Jan-1994  Michael Bremer, IRAM
      !
      !-------------------------------------------------------------------
      real refract_total, f_ghz, p_atm, p_vap, t
      real ref_dry, ref_vap,  sc, refract_vapor
      !
      !--------------------------------------------
      !
      ! sc = scaling factor for the wavelenght dependent part of the wet refration
      ! (normal conditions 300K, 1013mbar, 80% humidity -> partial pressure of
      !  water vapor 28.2mbar ):
      !
    end function refract_total
  end interface
  !
  interface
    function refract_vapor (f_ghz)
      !-----------------------------------------------------------------------
      !
      ! Function to calculate the refractivity of water vapor 0-480 GHz, under
      ! conditions T=300K, P=1atm, 80% rel. humidity (i.e. partial pressure of
      ! water vapor 28.2 mbar).
      !
      ! Source: Hill and Clifford (1981), Radio Science 16, curve p. 80
      ! Method of digitalisation: zoomed copy to a transparency,
      !                           points read by cursor.
      !                           Approx. errors: F +-1.5 GHz, R +-0.1
      !
      ! Author: 24-Jan-1993 Michael Bremer, IRAM
      !------------------------------------------------------------------------
      real refract_vapor, f_ghz
    end function refract_vapor
  end interface
  !
  interface
    function flin(v,vl,dv)
      !----------------------------------------------------------------------
      ! J Cernicharo, Model atmosphere.
      !
      !	FORMA CINETICA DEL PERFIL
      !	V... FRECUENCIA
      !	VL.. FRECUENCIA DE LA LINEA
      !	DV.. ANCHURA DE LA LINEA
      !----------------------------------------------------------------------
      real flin,v,vl,dv,pi,v2
    end function flin
  end interface
  !
  interface
    function fvvw(v,vl,dv)
      !----------------------------------------------------------------------
      !	PERFIL DE VAN VLECK & WEISSKOPF
      !
      ! J CERNICHARO
      !----------------------------------------------------------------------
      real fvvw,v,dv,vl,pi,dv2,a1,a2
    end function fvvw
  end interface
  !
  interface
    function kh2o(rho,t,p,v,il)
      !----------------------------------------------------------------------
      !	COEFICIENTE DE ABSORCION DEL VAPOR DE AGUA ATMOSFERICO
      !	T ... ES LA TEMPERATURA (K)
      !	P ... ES LA PRESION EN MILIBARES
      !	RHO . ES LA CONCENTRACION DE VAPOR DE AGUA EN GR/M**3
      !	V ... ES LA FRECUENCIA EN GHZ
      !	KH2O . COEFICIENTE DE ABSORCION EN CM-1
      !	IL=0   PERFIL CINETICO
      !	IL=1   PERFIL DE VAN VLECK & WEISSKPOF
      !
      ! J.Cernicharo
      !----------------------------------------------------------------------
      real rho,t,p,v
      integer il
      real fre(19),gl(19),flm(19),el(19),dv0(19),dvlm(19),   &
      real kh2o,sum,ta,tkk,tk,som,dv,rd,rd0,tt,gg,pi,fv
    end function kh2o
  end interface
  !
  interface
    function ko2(t,p,v,il)
      !----------------------------------------------------------------------
      !	OPACIDAD DE LA ATMOSFERA DEBIDA AL OXIGENO (O2)
      !	T .... ES LA TEMPERATURA (K)
      !	P .... ES LA PRESION EN MILIBARES
      !	V .... ES LA FRECUENCIA A LA CUAL SE DESEA CALCULAR KO2
      !	IL ... =0  PERFIL CINETICO
      !	IL ... =1  PERFIL DE VAN VLECK & WEISSKOPF
      !	KO2 .. OPACIDAD EN CM-1
      !
      !	ANCHURA DE LAS RAYAS DE REBER... BUENA APROXIMACION EN LAS ALAS
      !
      ! J.Cernicharo
      !----------------------------------------------------------------------
      real ko2,t,p,v
      integer il,l,j
    end function ko2
  end interface
  !
  interface
    subroutine kvatm(np,p,t,rho,h,agu,oxi,v,temi,tatm,tag,tagu,tox,   &
         &    toxi,ilag,ilox,kvat,ama,ier)
      !----------------------------------------------------------------------
      !	opacidad de la atmosfera a la frecuencia 'v' debida al
      !	vapor de agua y al oxigeno.
      !
      !	np .... numero de capas
      !	h  .... espesor de las capas     (cm)
      !	p  .... presion (milibares)
      !	t  .... temperatura (k)
      !	rho ... cantidad de vapor de agua (gr/m**3)
      !	temi .. emisividad total de la atmosfera     (k)
      !	kvat .. opacidad total (nepers)
      !	tatm .. temperatura media de la atmosfera   (k)
      !	agu ... opacidad debida al vapor de agua (nepers)
      !	oxi ... opacidad debida al oxigeno       (   "  )
      !	tag ... emisividad debida al vapor de agua (atmosfera sin oxigeno)
      !	tox ... idem para el oxigeno (sin vapor de agua)
      !	tagu .. temperatura media de la capa de vapor de agua (k)
      !	toxi .. temperatura media de la capa de oxigeno (k)
      !
      ! J.Cernicharo
      !----------------------------------------------------------------------
      integer np,ilag,ilox,ier
      real agu,oxi,v,temi,tatm,tag,tagu,tox,toxi,kvat,ama
      real h(*),p(*),t(*),rho(*)
    end subroutine kvatm
  end interface
  !
  interface
    subroutine jnu(j,v,t)
      real j                       ! Rayleigh Jeans temperature
      real v                       ! Frequency in GHz
      real t                       ! Temperature
    end subroutine jnu
  end interface
  !
  interface
    subroutine poli2_4(x1,x2,x3,y1,y2,y3,a,b,c)
      !----------------------------------------------------------------------
      !	ESTA SUBRUTINA CALCULA LOS COEFICIENTES A,B,C DEL POLINOMIO DE
      !	SEGUNDO GRADO A+BX+CX**2, QUE PASA POR LOS PUNTOS (X1,Y1),
      !	(X2,Y2),(X3,Y3)
      ! J.Cernicharo
      !----------------------------------------------------------------------
      real x1,x2,x3,y1,y2,y3,a,b,c
    end subroutine poli2_4
  end interface
  !
  interface
    function pwat(t,u)
      !----------------------------------------------------------------------
      !	calculo de la presion parcial del vapor de agua
      !	t es la temperatura en grados kelvin
      !	p es la presion atmosferica total en milibares
      !	u es la humedad relativa
      !	pwat es la presion parcial del vapor de agua
      !
      !
      !	calculo de la presion de saturacion
      !
      ! J Cernicharo
      !----------------------------------------------------------------------
      real pwat,t,u
    end function pwat
  end interface
  !
end module atm_interfaces_none
