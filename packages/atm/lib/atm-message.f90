!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage ATM messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module atm_message_private
  use gpack_def
  !---------------------------------------------------------------------
  ! @ private
  ! Identifier used for message identification
  !---------------------------------------------------------------------
  integer :: atm_message_id = gpack_global_id  ! Default @ startup
  !
end module atm_message_private
!
subroutine atm_message_set_id(id)
  use atm_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer, intent(in) :: id
  ! Local
  character(len=message_length) :: mess
  !
  atm_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',atm_message_id
  call atm_message(seve%d,'atm_message_set_id',mess)
  !
end subroutine atm_message_set_id
!
subroutine atm_message(mkind,procname,message)
  use atm_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Messaging facility for the current library. Calls the low-level
  ! messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  !
  call gmessage_write(atm_message_id,mkind,procname,message)
  !
end subroutine atm_message
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
