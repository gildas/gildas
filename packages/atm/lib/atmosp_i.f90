module atm_data
  use gildas_def
  !
  integer(kind=4), parameter :: atm_mode_none=-1    ! < 0: table not read
  integer(kind=4), parameter :: atm_mode_compute=0  ! = 0: do not interpolate, compute instead (never used by Astro)
  integer(kind=4), parameter :: atm_mode_table=+1   ! > 0: table successfully read, interpolate
  integer(kind=4) :: atm_mode = atm_mode_none
  !
  ! Support variables for table contents
  character(len=filename_length), save :: tab_file
  character(len=4), save :: tab_code
  integer(kind=4), save :: tab_np,tab_nt,tab_nf,tab_nw,tab_na
  real(kind=4), allocatable, save :: tab_p(:),tab_t(:),tab_f(:),tab_w(:),tab_a(:)
  real(kind=4), allocatable, save :: tab_tauox(:,:,:),tab_tauw(:,:,:)
  real(kind=4), allocatable, save :: tab_temis(:,:,:,:,:),tab_path(:,:,:,:,:)
  !
end module atm_data
!
subroutine atm_i(error)
  use atm_data
  !-----------------------------------------------------------------------
  ! @ public
  ! Make sure the atm data will be interpolated from file
  !-----------------------------------------------------------------------
  logical, intent(inout) :: error
  !
  atm_mode = atm_mode_none
  !
end subroutine atm_i
!
subroutine atm_read_table(error)
  use gildas_def
  use gkernel_interfaces
  use gbl_message
  use gbl_format
  use gio_convert
  use atm_data
  !-----------------------------------------------------------------------
  ! @ private
  ! Actually read the table file to be interpolated.
  !-----------------------------------------------------------------------
  logical, intent(inout) :: error
  ! Global
  external r4tor4,r8tor8,i4toi4,bytoby
  external ier4va,eir4va,eir4ie,ier4ei,var4ie,var4ei
  external ier8va,eir8va,eir8ie,ier8ei,var8ie,var8ei
  external        eii4va,eii4ie,iei4ei       ,vai4ei
  ! Local
  character(len=*), parameter :: rname='ATM_INTERPOLATE'
  integer(kind=4) :: lun,ier
  integer(kind=4) :: buffer(128)
  character(len=4) :: csyst
  integer(kind=4) :: conve
  !
  if (.not.sic_query_file('gag_atmosphere','data#dir:','',tab_file)) then
    call gag_message(seve%e,'ATM_READ_TABLE','Logical GAG_ATMOSPHERE not found')
    error = .true.
    return
  endif
  !
  ier = sic_getlun(lun)
  open (unit=lun, file=tab_file, form='UNFORMATTED',status='OLD',  &
        recl=128*facunf, access='DIRECT',iostat=ier)
  if (ier.ne.0) then
    call putios('E-ATM_INTERPOLATE, Open error: ',ier)
    call atm_message(seve%e,rname,'Filename: '//tab_file)
    error = .true.
    return
  endif
  !
  ! first record
  read(lun,rec=1,err=998,iostat = ier) buffer
  call bytoch(buffer,tab_code,4)
  call gdf_getcod (csyst)
  call gdf_convcod(tab_code,csyst,conve)
  !
  if (conve.eq.vax_to_ieee) then
    call sub_atm_decode(lun,buffer,var4ie,var8ie,r4tor4,bytoby,error)
  elseif (conve.eq.ieee_to_vax) then
    call sub_atm_decode(lun,buffer,ier4va,ier8va,r4tor4,bytoby,error)
  elseif (conve.eq.vax_to_eeei) then
    call sub_atm_decode(lun,buffer,var4ei,var8ei,vai4ei,bytoby,error)
  elseif (conve.eq.ieee_to_eeei) then
    call sub_atm_decode(lun,buffer,ier4ei,ier8ei,iei4ei,bytoby,error)
  elseif (conve.eq.eeei_to_vax) then
    call sub_atm_decode(lun,buffer,eir4va,eir8va,eii4va,bytoby,error)
  elseif (conve.eq.eeei_to_ieee) then
    call sub_atm_decode(lun,buffer,eir4ie,eir8ie,eii4ie,bytoby,error)
  else
    ! Native
    call sub_atm_decode(lun,buffer,r4tor4,r8tor8,i4toi4,bytoby,error)
  endif
  !
  call atm_sicvariables_table(error)
  if (error)  continue
  !
998  continue
  if (ier.ne.0) then
    call putios('E-ATM_INTERPOLATE, Read Error: ',ier)
    call atm_message(seve%e,rname,'Filename: '//tab_file)
    error = .true.
  endif
  !
  close(unit=lun)
  call sic_frelun(lun)
end subroutine atm_read_table
!
subroutine sub_atm_decode(lun,buffer,r4,r8,i4,cc,error)
  use gildas_def
  use gbl_message
  use gkernel_interfaces
  use atm_data
  !-----------------------------------------------------------------------
  ! @ private
  !-----------------------------------------------------------------------
  integer(kind=4), intent(in)    :: lun
  integer(kind=4), intent(inout) :: buffer(128)
  external                       :: r4
  external                       :: i4
  external                       :: r8
  external                       :: cc
  logical,         intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='ATM'
  integer(kind=4) :: currec,recpos,ier
  !
  call i4(buffer(2),tab_np,1)
  call i4(buffer(3),tab_nt,1)
  call i4(buffer(4),tab_nf,1)
  call i4(buffer(5),tab_nw,1)
  call i4(buffer(6),tab_na,1)
  !
  call reallocate_atm_table(tab_np,tab_nt,tab_nf,tab_nw,tab_na,error)
  if (error)  return
  !
  currec = 1  ! Current record in buffer
  recpos = 7  ! CUrrent position to be read in buffer
  !
  call fill_my_array(tab_p,tab_np,error)
  if (error)  return
  call fill_my_array(tab_t,tab_nt,error)
  if (error)  return
  call fill_my_array(tab_f,tab_nf,error)
  if (error)  return
  call fill_my_array(tab_w,tab_nw,error)
  if (error)  return
  call fill_my_array(tab_a,tab_na,error)
  if (error)  return
  !
  call fill_my_array(tab_tauox,tab_nf*tab_nt*tab_np,error)
  if (error)  return
  call fill_my_array(tab_tauw,tab_nf*tab_nt*tab_np,error)
  if (error)  return
  !
  call fill_my_array(tab_temis,tab_na*tab_nw*tab_nf*tab_nt*tab_np,error)
  if (error)  return
  call fill_my_array(tab_path,tab_na*tab_nw*tab_nf*tab_nt*tab_np,error)
  if (error)  return
  !
  atm_mode = atm_mode_table
  !
contains
  subroutine fill_my_array(array,n,error)
    integer(kind=4), intent(in)    :: n
    real(kind=4),    intent(out)   :: array(n)
    logical,         intent(inout) :: error
    ! Local
    integer(kind=4), parameter :: reclen=128
    integer(kind=4) :: nreadtot,nreadcur
    character(len=message_length) :: mess
    !
    nreadtot = 0  ! Number of elements read into array up to now
    do while (nreadtot.lt.n)
      !
      if (recpos.gt.reclen) then
        currec = currec+1
        ! print *,"Reading record ",currec
        read(lun,rec=currec,iostat=ier)  buffer
        if (ier.ne.0) then
          write(mess,'(A,I0)')  'Error reading record #',currec
          call atm_message(seve%e,rname,mess)
          error = .true.
          return
        endif
        recpos = 1
      endif
      !
      nreadcur = min(n-nreadtot,reclen-recpos+1)
      call r4(buffer(recpos),array(nreadtot+1),nreadcur)
      nreadtot = nreadtot+nreadcur
      recpos = recpos+nreadcur
      !
    enddo
  end subroutine fill_my_array
  !
end subroutine sub_atm_decode
!
subroutine reallocate_atm_table(np,nt,nf,nw,na,error)
  use gkernel_interfaces
  use atm_data
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)    :: np
  integer(kind=4), intent(in)    :: nt
  integer(kind=4), intent(in)    :: nf
  integer(kind=4), intent(in)    :: nw
  integer(kind=4), intent(in)    :: na
  logical,         intent(inout) :: error
  ! Local
  integer(kind=4) :: ier
  !
  if (allocated(tab_p))  deallocate(tab_p)
  if (allocated(tab_t))  deallocate(tab_t)
  if (allocated(tab_f))  deallocate(tab_f)
  if (allocated(tab_w))  deallocate(tab_w)
  if (allocated(tab_a))  deallocate(tab_a)
  !
  if (allocated(tab_tauox))  deallocate(tab_tauox)
  if (allocated(tab_tauw))   deallocate(tab_tauw)
  !
  if (allocated(tab_temis))  deallocate(tab_temis)
  if (allocated(tab_path))   deallocate(tab_path)
  !
  ! Then allocate to appropriate size
  tab_np = np
  tab_nt = nt
  tab_nf = nf
  tab_nw = nw
  tab_na = na
  allocate(tab_p(tab_np),  &
           tab_t(tab_nt),  &
           tab_f(tab_nf),  &
           tab_w(tab_nw),  &
           tab_a(tab_na),stat=ier)
  if (failed_allocate('ATM','P,T,F,W,A buffers',ier,error))  return
  !
  allocate(tab_tauox(tab_nf,tab_nt,tab_np),  &
           tab_tauw(tab_nf,tab_nt,tab_np),stat=ier)
  if (failed_allocate('ATM','TAUOX,TAUW buffers',ier,error))  return
  !
  allocate(tab_temis(tab_na,tab_nw,tab_nf,tab_nt,tab_np),  &
           tab_path(tab_na,tab_nw,tab_nf,tab_nt,tab_np),stat=ier)
  if (failed_allocate('ATM','TEMIS,PATH buffers',ier,error))  return
  !
end subroutine reallocate_atm_table
!
subroutine atm_atmosp_i(tem,pre,alt)
  use gildas_def
  use gkernel_interfaces
  use atm_interfaces, except_this=>atm_atmosp_i
  use atm_data
  !-----------------------------------------------------------------------
  ! @ public
  !-----------------------------------------------------------------------
  real(kind=4), intent(in) :: tem
  real(kind=4), intent(in) :: pre
  real(kind=4), intent(in) :: alt
  ! Local
  real(kind=4) :: wat,air,fre,temi,tat,tauox,tauw,taut, pat
  integer(kind=4) :: ier
  integer(kind=4) :: ix(5), n(5)
  real(kind=4) :: w(5,32), xi(5), kv
  save w, xi, ix, n
  logical :: error
  !
  if (atm_mode.eq.atm_mode_none) then
    ! First call: read table
    error = .false.
    call atm_read_table(error)
    if (error) return
  endif
  !
  if (atm_mode.eq.atm_mode_table) then
    ! Interpolate
    call indexp(tab_np,tab_p,pre,ix(1),xi(1))
    call indexp(tab_nt,tab_t,tem,ix(2),xi(2))
  else
    ! Compute
    call atm_atmosp(tem,pre,alt)
  endif
  return
  !
entry atm_transm_i(wat,air,fre,temi,tat,tauox,tauw,taut,ier)
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  !
  if (atm_mode.eq.atm_mode_none) then
    ! First call: read table
    error = .false.
    call atm_read_table(error)
    if (error) return
  endif
  !
  if (atm_mode.eq.atm_mode_table) then
    ! Interpolate
    call indexp(tab_nf,tab_f,fre,ix(3),xi(3))
    call indexp(tab_nw,tab_w,wat,ix(4),xi(4))
    call indexp(tab_na,tab_a,air,ix(5),xi(5))
    ! Transposed dimensions of tab_temis
    n(1:5) = (/ tab_np,tab_nt,tab_nf,tab_nw,tab_na /)
    call interp(5,n,tab_temis,ix,xi,temi,w)
    call interp(3,n,tab_tauox,ix,xi,tauox,w)
    call interp(3,n,tab_tauw,ix,xi,tauw,w)
    tauw = tauw*wat
    taut = tauw+tauox
    kv = taut*air
    tat = temi /(1.-exp(-kv))
  else
    ! Compute
    call atm_transm(wat,air,fre,temi,tat,tauox,tauw,taut,ier)
  endif
  return
  !
entry atm_path_i(wat,air,fre,pat,ier)
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  !
  if (atm_mode.eq.atm_mode_none) then
    ! First call: read table
    error = .false.
    call atm_read_table(error)
    if (error) return
  endif
  !
  if (atm_mode.eq.atm_mode_table) then
    ! Interpolate
    call indexp(tab_nf,tab_f,fre,ix(3),xi(3))
    call indexp(tab_nw,tab_w,wat,ix(4),xi(4))
    call indexp(tab_na,tab_a,air,ix(5),xi(5))
    ! Transposed dimensions of tab_temis
    n(1:5) = (/ tab_np,tab_nt,tab_nf,tab_nw,tab_na /)
    call interp(5,n,tab_path,ix,xi,pat,w)
  else
    ! Compute
    call atm_path(wat,air,fre,pat,ier)
  endif
end subroutine atm_atmosp_i
!
subroutine indexp(n,xt,x,ix,xi)
  !-----------------------------------------------------------------------
  ! @ private
  ! computes the index and fractional increment of value x in table xt
  ! e.g.       x = xt(ix)+ xi*(xt(ix+1)-xt(ix))
  ! returns ix=1, xi=0. if x < xt(1)
  !         ix=n, xi=0. if x > xt(n)
  !-----------------------------------------------------------------------
  integer(kind=4), intent(in)  :: n      ! dimension of table xt
  real(kind=4),    intent(in)  :: xt(n)  ! input table
  real(kind=4),    intent(in)  :: x      ! input value of x
  real(kind=4),    intent(out) :: xi     ! fractional increment
  integer(kind=4), intent(out) :: ix     ! index
  ! Local
  integer(kind=4) :: i
  !
  ix = 1
  xi = 0.0
  if (x.lt.xt(1)) return
  if (n.lt.2) return
  do i=1, n-1
    if (x.lt.xt(i+1)) then
      ix = i
      xi = (x-xt(i))/(xt(i+1)-xt(i))
      return
    endif
  enddo
  ix = n
end subroutine indexp
!
subroutine interp(nv,n,t,ix,xi,v,w)
  !-----------------------------------------------------------------------
  ! @ private
  !-----------------------------------------------------------------------
  integer(kind=4), intent(in)    :: nv        ! number of variables
  integer(kind=4), intent(in)    :: n(nv)     ! dimensions of the table
  real(kind=4),    intent(in)    :: t(*)      ! table (nv dimensions)
  integer(kind=4), intent(in)    :: ix(nv)    ! index of x value xx where the function is to be taken
  real(kind=4),    intent(in)    :: xi(nv)    ! fractional increment of x value e.g. xx = x(ix)+xi*(x(ix+1)-x(ix))
  real(kind=4),    intent(out)   :: v         ! interpolated value (output)
  real(kind=4),    intent(inout) :: w(nv,*)   ! work array of dim (nv,2**nv)
  ! Local
  integer(kind=4) :: m, nn, ip, i, ipv, j, k
  !
  m = 0
  !
  ! w(1,m) contains the values at the corners of the nv-th order hypercube
  do while (m.lt.2**nv)
    nn = 1
    ip = 1
    k = 1
    do i= nv, 1, -1
      ipv = ix(i)
      if (mod(m/k,2).ne.0 .and. xi(i).gt.0) then
        ipv = ipv+1
      endif
      ip = ip + (ipv-1)*nn
      nn = nn*n(i)
      k = k*2
    enddo
    m = m+1
    w(nv,m) = t(ip)
  enddo
  !
  ! Now interpolate
  do i = nv, 2, -1
    m = 2**(i-1)
    do j=1, m
      w(i-1,j) = w(i,2*j-1)+(w(i,2*j)-w(i,2*j-1))*xi(i)
    enddo
  enddo
  v = w(1,1)+(w(1,2)-w(1,1))*xi(1)
  !
end subroutine interp
!
subroutine atmos_i_table(compute,file,nfile,n_f,fmin,fmax,h0,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use gkernel_interfaces
  use atm_data
  !-----------------------------------------------------------------------
  ! @ public
  ! Write a table of atmospheric emission data or further interpolation.
  !-----------------------------------------------------------------------
  logical,          intent(in)    :: compute
  character(len=*), intent(inout) :: file
  integer(kind=4),  intent(out)   :: nfile
  integer(kind=4),  intent(in)    :: n_f
  real(kind=4),     intent(in)    :: fmin
  real(kind=4),     intent(in)    :: fmax
  real(kind=4),     intent(in)    :: h0
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='ATM_TABLE'
  integer(kind=4) :: ier,currec,recpos
  integer(kind=4) :: lun,buffer(128)
  character(len=80) :: name
  character(len=message_length) :: mess
  !
  name = file
  call sic_parse_file(name,' ','.bin',file)
  ier = sic_getlun(lun)
  nfile = len_trim(file)
  open(unit=lun,file=file(1:nfile),status='NEW',access='DIRECT',   &
       form='UNFORMATTED',iostat=ier,recl=128*facunf)
  if (ier.ne.0) then
    call atm_message(seve%e,rname,'Filename: '//file)
    call putios('E-ATM, Open error: ',ier)
    error = .true.
    goto 99
  endif
  !
  if (compute) then
    ! Compute the table in memory
    call atmos_i_table_fill(n_f,fmin,fmax,h0,error)
    if (error)  goto 98
  else
    ! Reuse the table from memory. This is usefull e.g. for loading
    ! an old format and re-writing it with current system format
    if (atm_mode.eq.atm_mode_none) then
      call atm_message(seve%e,rname,'No table in memory. Use ATM MAKE')
      error = .true.
      goto 98
    endif
  endif
  !
  ! Write data
  tab_file = file
  call gdf_getcod(tab_code)
  call chtoby(tab_code,buffer(1),4)
  call i4toi4(tab_np,buffer(2),1)
  call i4toi4(tab_nt,buffer(3),1)
  call i4toi4(tab_nf,buffer(4),1)
  call i4toi4(tab_nw,buffer(5),1)
  call i4toi4(tab_na,buffer(6),1)
  !
  currec = 1
  recpos = 7
  !
  call write_my_array(tab_p,tab_np,error)
  if (error)  goto 98
  call write_my_array(tab_t,tab_nt,error)
  if (error)  goto 98
  call write_my_array(tab_f,tab_nf,error)
  if (error)  goto 98
  call write_my_array(tab_w,tab_nw,error)
  if (error)  goto 98
  call write_my_array(tab_a,tab_na,error)
  if (error)  goto 98
  !
  call write_my_array(tab_tauox,tab_nf*tab_nt*tab_np,error)
  if (error)  goto 98
  call write_my_array(tab_tauw,tab_nf*tab_nt*tab_np,error)
  if (error)  goto 98
  !
  call write_my_array(tab_temis,tab_na*tab_nw*tab_nf*tab_nt*tab_np,error)
  if (error)  goto 98
  call write_my_array(tab_path,tab_na*tab_nw*tab_nf*tab_nt*tab_np,error)
  if (error)  goto 98
  !
  if (recpos.ne.1) then
    ! Flush last record
    write(lun,rec=currec,iostat=ier)  buffer
    if (ier.ne.0) then
      write(mess,'(A,I0)') 'Error writing record ',currec
      call atm_message(seve%e,rname,mess)
      error = .true.
      goto 98
    endif
  endif
  !
  write(mess,'(I0,A)') currec,' records written'
  call atm_message(seve%i,rname,mess)
  !
  call atm_sicvariables_table(error)
  if (error)  goto 98
  !
98 close(lun)
99 call sic_frelun(lun)
  !
contains
  subroutine write_my_array(array,n,error)
    integer(kind=4), intent(in)    :: n
    real(kind=4),    intent(in)    :: array(n)
    logical,         intent(inout) :: error
    ! Local
    integer(kind=4), parameter :: reclen=128
    integer(kind=4) :: nwritetot,nwritecur
    !
    nwritetot = 0
    do while (nwritetot.lt.n)
      !
      nwritecur = min(n-nwritetot,reclen-recpos+1)
      call r4tor4(array(nwritetot+1),buffer(recpos),nwritecur)
      nwritetot = nwritetot+nwritecur
      recpos = recpos+nwritecur
      !
      if (recpos.gt.reclen) then
        ! print *,"Writing record ",currec
        write(lun,rec=currec,iostat=ier)  buffer
        if (ier.ne.0) then
          write(mess,'(A,I0)') 'Error writing record ',currec
          call atm_message(seve%e,rname,mess)
          error = .true.
          return
        endif
        currec = currec+1
        recpos = 1
      endif
      !
    enddo
  end subroutine write_my_array
  !
end subroutine atmos_i_table
!
subroutine atmos_i_table_fill(n_f,fmin,fmax,h0,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use gkernel_interfaces
  use gkernel_types
  use atm_interfaces, except_this=>atmos_i_table_fill
  use atm_data
  !-----------------------------------------------------------------------
  ! @ private
  ! Compute the table in the ATM memory buffer
  !-----------------------------------------------------------------------
  integer(kind=4), intent(in)    :: n_f
  real(kind=4),    intent(in)    :: fmin
  real(kind=4),    intent(in)    :: fmax
  real(kind=4),    intent(in)    :: h0
  logical,         intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='ATM_TABLE'
  real(kind=4) :: tatm, tauox, tauw, taut, temis, paths
  integer(kind=4) :: i, ier, ia, iw, if, it, ip
  character(len=message_length) :: mess
  type(time_t) :: time
  ! Parameter space for table
  integer(kind=4), parameter :: n_p=5
  integer(kind=4), parameter :: n_t=10
  integer(kind=4), parameter :: n_w=7
  integer(kind=4), parameter :: n_a=10
  real(kind=4), parameter :: pmin=985.0, pmax=1030.  ! Sea level
  real(kind=4), parameter :: tmin=250.0, tmax=305.0  !
  real(kind=4), parameter :: wmin=0.999, wmax=0.001  ! actually exp(-water/10.)
  real(kind=4), parameter :: amin=1.000, amax=10.00  !
  !
  if (n_f.le.0) then
    call atm_message(seve%e,rname,'Number of frequency points is null or negative')
    error = .true.
    return
  endif
  !
  call reallocate_atm_table(n_p,n_t,n_f,n_w,n_a,error)
  if (error)  return
  !
  do i=1, n_p
    tab_p(i) = ((n_p-i)*pmin+(i-1)*pmax)/(n_p-1.)*2.0**(-h0/5.75)
  enddo
  do i=1, n_t
    tab_t(i) = ((n_t-i)*tmin+(i-1)*tmax)/(n_t-1)
  enddo
  do i=1, n_w
    tab_w(i) = ((n_w-i)*wmin+(i-1)*wmax)/(n_w-1)
    tab_w(i) = -10.*log(tab_w(i))
  enddo
  do i=1, n_a
    tab_a(i) = ((n_a-i)*amin+(i-1)*amax)/(n_a-1)
  enddo
  do i=1, n_f
    tab_f(i) = ((n_f-i)*fmin+(i-1)*fmax)/(n_f-1)
  enddo
  !
  call atm_message(seve%i,rname,'Computing table with')
  write (mess,'(I5,A,F0.2,A,F0.2,A)')  &
    n_p,' pressures    from ',tab_p(1),' to ',tab_p(n_p),' x '
  call atm_message(seve%i,rname,mess)
  write (mess,'(I5,A,F0.2,A,F0.2,A)')  &
    n_t,' temperatures from ',tab_t(1),' to ',tab_t(n_t),' x '
  call atm_message(seve%i,rname,mess)
  write (mess,'(I5,A,F0.2,A,F0.2,A)')  &
    n_f,' frequencies  from ',tab_f(1),' to ',tab_f(n_f)
  call atm_message(seve%i,rname,mess)
  !
  call gtime_init(time,n_p*n_t*n_f,error)
  if (error)  return
  !
  do ip=1, n_p
    do it = 1, n_t
      call atm_atmosp(tab_t(it),tab_p(ip),h0)
      do if=1, n_f
        do iw = 1, n_w
          do ia = 1, n_a
            call atm_transm(tab_w(iw),tab_a(ia),tab_f(if),temis,tatm,tauox,tauw,taut,ier)
            tab_temis(ia,iw,if,it,ip) = temis
            call atm_path(tab_w(iw),tab_a(ia),tab_f(if),paths,ier)
            tab_path(ia,iw,if,it,ip) = paths
            tauw = tauw/tab_w(iw)
          enddo
        enddo
        tab_tauox(if,it,ip) = tauox
        tab_tauw(if,it,ip) = tauw
        !
        call gtime_current(time)
        if (sic_ctrlc()) then
          call atm_message(seve%e,rname,'Aborted')
          error = .true.
          return
        endif
      enddo
    enddo
  enddo
  !
  atm_mode = atm_mode_table
  !
end subroutine atmos_i_table_fill
!
subroutine atm_sicvariables_table(error)
  use gildas_def
  use gkernel_interfaces
  use atm_data
  !---------------------------------------------------------------------
  ! @ private
  !  Define the structure ATM%TABLE mapped on the table loaded in memory
  !---------------------------------------------------------------------
  logical, intent(inout) :: error
  ! Local
  integer(kind=index_length) :: dims(sic_maxdims)
  !
  if (atm_mode.ne.atm_mode_table)  return
  !
  ! Do not create the ATM%TABLE% if ATM% does not exist. There are
  ! callers which do not need ATM Sic variables and have not
  ! created the usual ATM variables
  if (.not.sic_varexist('ATM'))  return
  !
  call sic_defstructure ('ATM%TABLE',.true.,error)
  if (error) return
  !
  call sic_def_char('ATM%TABLE%FILE',tab_file,.true.,error)
  if (error)  return
  call sic_def_char('ATM%TABLE%SYSTEM',tab_code,.true.,error)
  if (error)  return
  !
  call sic_def_inte('ATM%TABLE%NPRESSURE',tab_np,0,0,.true.,error)
  if (error)  return
  call sic_def_inte('ATM%TABLE%NTEMPERATURE',tab_nt,0,0,.true.,error)
  if (error)  return
  call sic_def_inte('ATM%TABLE%NFREQUENCY',tab_nf,0,0,.true.,error)
  if (error)  return
  call sic_def_inte('ATM%TABLE%NWATER',tab_nw,0,0,.true.,error)
  if (error)  return
  call sic_def_inte('ATM%TABLE%NAIRMASS',tab_na,0,0,.true.,error)
  if (error)  return
  !
  call sic_def_real('ATM%TABLE%PRESSURE',   tab_p,1,tab_np,.true.,error)
  if (error)  return
  call sic_def_real('ATM%TABLE%TEMPERATURE',tab_t,1,tab_nt,.true.,error)
  if (error)  return
  call sic_def_real('ATM%TABLE%FREQUENCY',  tab_f,1,tab_nf,.true.,error)
  if (error)  return
  call sic_def_real('ATM%TABLE%WATER',      tab_w,1,tab_nw,.true.,error)
  if (error)  return
  call sic_def_real('ATM%TABLE%AIRMASS',    tab_a,1,tab_na,.true.,error)
  if (error)  return
  !
  dims(1) = tab_nf
  dims(2) = tab_nt
  dims(3) = tab_np
  call sic_def_real('ATM%TABLE%TAU_O2',tab_tauox,3,dims,.true.,error)
  if (error)  return
  call sic_def_real('ATM%TABLE%TAU_H2O',tab_tauw,3,dims,.true.,error)
  if (error)  return
  !
  dims(1) = tab_na
  dims(2) = tab_nw
  dims(3) = tab_nf
  dims(4) = tab_nt
  dims(5) = tab_np
  call sic_def_real('ATM%TABLE%EMIS',tab_temis,5,dims,.true.,error)
  if (error)  return
  call sic_def_real('ATM%TABLE%PATH',tab_path,5,dims,.true.,error)
  if (error)  return
  !
end subroutine atm_sicvariables_table
