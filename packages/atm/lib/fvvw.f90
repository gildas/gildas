function fvvw(v,vl,dv)
  !----------------------------------------------------------------------
  !	PERFIL DE VAN VLECK & WEISSKOPF
  !
  ! J CERNICHARO
  !----------------------------------------------------------------------
  real fvvw,v,dv,vl,pi,dv2,a1,a2
  data pi/3.141592654/
  !
  fvvw=dv*v/vl/pi
  dv2=dv*dv
  a1=dv2+(v-vl)**2
  a2=dv2+(v+vl)**2
  fvvw=fvvw*(1./a1+1./a2)
end function fvvw
