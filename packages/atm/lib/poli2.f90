subroutine poli2_4(x1,x2,x3,y1,y2,y3,a,b,c)
  !----------------------------------------------------------------------
  !	ESTA SUBRUTINA CALCULA LOS COEFICIENTES A,B,C DEL POLINOMIO DE
  !	SEGUNDO GRADO A+BX+CX**2, QUE PASA POR LOS PUNTOS (X1,Y1),
  !	(X2,Y2),(X3,Y3)
  ! J.Cernicharo
  !----------------------------------------------------------------------
  real x1,x2,x3,y1,y2,y3,a,b,c
  !
  c=(y3-y2)*(x2-x1)-(y2-y1)*(x3-x2)
  b=(x2-x1)*(x3*x3-x2*x2)-(x2*x2-x1*x1)*(x3-x2)
  c=c/b
  b=(y2-y1)-c*(x2*x2-x1*x1)
  b=b/(x2-x1)
  a=y1-c*x1*x1-b*x1
end subroutine poli2_4
