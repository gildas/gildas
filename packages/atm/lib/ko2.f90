function ko2(t,p,v,il)
  !----------------------------------------------------------------------
  !	OPACIDAD DE LA ATMOSFERA DEBIDA AL OXIGENO (O2)
  !	T .... ES LA TEMPERATURA (K)
  !	P .... ES LA PRESION EN MILIBARES
  !	V .... ES LA FRECUENCIA A LA CUAL SE DESEA CALCULAR KO2
  !	IL ... =0  PERFIL CINETICO
  !	IL ... =1  PERFIL DE VAN VLECK & WEISSKOPF
  !	KO2 .. OPACIDAD EN CM-1
  !
  !	ANCHURA DE LAS RAYAS DE REBER... BUENA APROXIMACION EN LAS ALAS
  !
  ! J.Cernicharo
  !----------------------------------------------------------------------
  real ko2,t,p,v
  integer il,l,j
  real fmen(20),fmas(20),rn(20),fdeb(6),b1(6),b2(6),b3(6)
  !
  real ta,v2,sum,e0,dv1,dv,dv2,a1,a2,a3,e,pi,rd,rd0,gg,b,rr
  real flin,fvvw
  !
  !	FMEN ... FRECUENCIAS EN GHZ  N-
  !
  data fmen/118.750343,62.486255,60.306044,59.164215,              &
             58.323885,57.612488,56.968180,56.363393,55.783819,    &
             55.221372,54.671145,54.1302,53.5959,53.0669,52.5424,  &
             52.0214,51.50302,50.9873,50.4736,49.9618/
  !
  !	FMAS ... FRECUENCIAS EN GHZ N+
  !
  data fmas/56.264766,58.446580,59.590978,60.434776,          &
            61.15057,61.800169,62.411223,62.997991,63.56852,  &
            64.127777,64.678914,65.22412,65.764744,66.30206,  &
            66.83677,67.36951,67.90073,68.4308,68.9601,69.4887/
  !
  !	N    ... NUMERO CUANTICO DE ROTACION
  !
  data rn/1.,3.,5.,7.,9.,11.,13.,15.,17.,19.,21.,23.,   &
     &    25.,27.,29.,31.,33.,35.,37.,39./
  !
  !	RAYAS CON DN=2
  !
  data fdeb/368.499,424.7638,487.25,715.3944,773.841,834.147/
  data b1/6.79e-6,6.43e-05,2.39e-5,9.79e-6,5.71e-5,1.83e-5/
  data b2/.202,.0112,.0112,.0891,.0798,.0798/
  data b3/15.6e-4,14.7e-4,14.7e-4,14.4e-4,14e-4,14e-4/
  data pi/3.141592654/
  ko2=1.44e-05*p*v/t/t/t
  ta=300./t
  v2=v**2
  sum=0.
  e0=2.07/t
  dv1=1.41e-03*p*300./t
  dv=dv1
  if(dv1.gt.0.0527)dv=dv/3.+0.03513
  dv2=dv*dv
  do 1 l=1,20
    a1=(rn(l)**2+rn(l)+1.)*(2.*rn(l)+1.)/rn(l)/(rn(l)+1.)
    e=e0*rn(l)*(rn(l)+1.)
    a1=a1*2.*v*dv/pi/(v2+dv2)
    a2=rn(l)*(2.*rn(l)+3.)/(rn(l)+1.)
    if(il.eq.0)a2=a2*flin(v,fmas(l),dv)*fmas(l)
    if(il.eq.1)a2=a2*fvvw(v,fmas(l),dv)*fmas(l)
    a3=(rn(l)+1.)*(2.*rn(l)-1.)/rn(l)
    b=dv
    if(l.eq.1)b=dv1
    if(il.eq.0.)a3=a3*fmen(l)*flin(v,fmen(l),b)
    if(il.eq.1.)a3=a3*fmen(l)*fvvw(v,fmen(l),b)
  1     sum=sum+(a1+a2+a3)*exp(-e)
  ko2=sum*ko2
  !
  !	RAYAS CON DN=2
  !
  rd=p*ta**3*4.193e-07*v
  rd0=0.
  do 10 j=1,6
    !	IF(V.LE.FDEB(J)+200..AND.V.GE.FDEB(J)-200.)GO TO 15
    !	GO TO 10
    15       rr=b1(j)*exp(b2(j)*(1.-ta))
    gg=b3(j)*p*ta**.9
    if(il.eq.0)rr=rr*flin(v,fdeb(j),gg)
    if(il.eq.1)rr=rr*fvvw(v,fdeb(j),gg)
    rd0=rd0+rr
  10    continue
  rd=rd*rd0
  ko2=ko2+rd
  return
end function ko2
