function kh2o(rho,t,p,v,il)
  !----------------------------------------------------------------------
  !	COEFICIENTE DE ABSORCION DEL VAPOR DE AGUA ATMOSFERICO
  !	T ... ES LA TEMPERATURA (K)
  !	P ... ES LA PRESION EN MILIBARES
  !	RHO . ES LA CONCENTRACION DE VAPOR DE AGUA EN GR/M**3
  !	V ... ES LA FRECUENCIA EN GHZ
  !	KH2O . COEFICIENTE DE ABSORCION EN CM-1
  !	IL=0   PERFIL CINETICO
  !	IL=1   PERFIL DE VAN VLECK & WEISSKPOF
  !
  ! J.Cernicharo
  !----------------------------------------------------------------------
  real rho,t,p,v
  integer il
  real fre(19),gl(19),flm(19),el(19),dv0(19),dvlm(19),   &
     &    x(19),b1(9),b2(9),b3(9),fdeb(9)
  real kh2o,sum,ta,tkk,tk,som,dv,rd,rd0,tt,gg,pi,fv
  integer l,j
  !
  real fvvw,flin
  !
  !	FRE ... FRECUENCIAS DE LAS TRANSICIONES DEL VAPOR DE AGUA (GHZ)
  !
  data fre/22.23507985,183.3100906,321.225644,325.152919,380.197372,  &
          390.14,437.346667,439.150812,443.018295,448.001075,         &
          470.888947,474.689127,488.491133,556.936002,620.700807,     &
          752.033227,916.62,970.31,987.94/
  !
  !	GL ... DEGENERACION DE LOS NIVELES
  !
  data gl/3.,1.,3.,1.,3.,1.,1.,3.,3.,3.,1.,1.,1.,3.,3.,   &
     &    1.,1.,1.,1./
  !
  !	FLM ... CUADRADO DEL ELEMENTO DE MATRIZ LM
  !
  data flm/.057,.102,.089,.091,.123,.068,.088,.0101,.088,   &
     &    .132,.102,.118,.036,1.5,.122,2.073,.161,.262,.7557/
  !
  !	EM ... ENERGIAS EN CM-1 DEL NIVEL SUPERIOR EN LA TRANS L=>M
  !	EL ...     "     "  "    "    "   INFERIOR  "  "   "     "
  !
  !	DATA EM/447.3,142.27,1293.8,326.62,224.84,1538.31,1059.63,
  !	1 756.76,1059.90,300.37/
  data el/446.56,136.16,1283.02,315.78,212.16,1525.31,   &
     &    1045.03,742.11,1045.11,285.42,742.074,488.135,586.48,   &
     &    23.794,488.108,70.091,285.217,383.837,37.137/
  data dv0/2.85,2.68,2.3,3.03,3.19,2.11,1.5,1.94,1.51,   &
     &    2.47,1.89,2.07,2.58,3.33,2.28,3.13,2.59,2.48,3.09/
  data dvlm/13.68,14.49,12.04,15.21,15.84,11.42,7.94,10.44,   &
     &    8.13,14.24,10.56,11.95,14.77,14.66,12.78,13.93,14.06,14.16,   &
     &    15.20/
  !
  !	X  ... EXPONENTE DE LA TEMPERATURA
  !
  data x/.626,.649,.42,.619,.63,.33,.29,.36,.332,.51,   &
     &    .380,.38,.57,.645,.6,.69,.676,.56,.66/
  data fdeb/68.052,503.56,504.46,658.34,841.01,859.81,899.38,   &
     &    903.28,906.21/
  data b1/1.8e-3,3.5e-3,1.2e-3,4.6e-2,1.2e-3,1.5e-3,9.1e-3,   &
     &    6.4e-3,1.79e-2/
  data b2/8.75,6.69,6.69,7.76,8.11,7.99,7.84,8.35,5.04/
  data b3/2.8e-3,1.27e-3,1.3e-3,3.28e-3,1.7e-3,2.7e-3,3e-3,   &
     &    2.8e-3,2.04e-3/
  data pi/3.141592654/,tk/.69503096/
  kh2o=1.44*rho*v/sqrt(t**3)
  sum=0.
  ta=300./t
  tkk=tk*t
  do 1 l=1,19
    !	IF(V.LE.FRE(L)+200..AND.V.GE.FRE(L)-200.)GO TO 5
    !	GO TO 1
    5        som=gl(l)*flm(l)*exp(-el(l)/tkk)*(1.-exp(-fre(l)/tkk/29.97925))
    dv=dv0(l)*p/1013./((t/300.)**x(l))
    dv=dv*(1.+(4.6e-03*rho*t/p)*(dvlm(l)/dv0(l)-1.))
    if(il.eq.0)fv=flin(v,fre(l),dv)
    if(il.eq.1)fv=fvvw(v,fre(l),dv)
    sum=sum+som*fv
  1     continue
  kh2o=kh2o*sum
  !
  !	TERMINO CORRECTOR EMPIRICO (POSIBLE CONTRIBUCION DE LOS DIMEROS
  !				    DE H2O)
  !
  kh2o=kh2o+1.08e-11*rho*v*v*p/1000.*(ta)**2.1
  !
  !	RAYAS DEBILES
  !
  rd=1.937e-9*v*rho*t
  rd0=0.
  do 10 j=1,9
    !	IF(V.LE.FDEB(J)+100..AND.V.GE.FDEB(J)-100.)GO TO 15
    !	GO TO 10
    15       tt=b2(j)*(1.-ta)
    tt=b1(j)*exp(tt)*(ta)**3.5
    gg=ta**0.6*p*b3(j)
    if(il.eq.0)fv=flin(v,fdeb(j),gg)
    if(il.eq.1)fv=fvvw(v,fdeb(j),gg)
    rd0=rd0+tt*fv
  10    continue
  rd=rd*rd0
  kh2o=kh2o+rd
  return
end function kh2o
