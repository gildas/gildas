subroutine kvatm(np,p,t,rho,h,agu,oxi,v,temi,tatm,tag,tagu,tox,   &
     &    toxi,ilag,ilox,kvat,ama,ier)
  !----------------------------------------------------------------------
  !	opacidad de la atmosfera a la frecuencia 'v' debida al
  !	vapor de agua y al oxigeno.
  !
  !	np .... numero de capas
  !	h  .... espesor de las capas     (cm)
  !	p  .... presion (milibares)
  !	t  .... temperatura (k)
  !	rho ... cantidad de vapor de agua (gr/m**3)
  !	temi .. emisividad total de la atmosfera     (k)
  !	kvat .. opacidad total (nepers)
  !	tatm .. temperatura media de la atmosfera   (k)
  !	agu ... opacidad debida al vapor de agua (nepers)
  !	oxi ... opacidad debida al oxigeno       (   "  )
  !	tag ... emisividad debida al vapor de agua (atmosfera sin oxigeno)
  !	tox ... idem para el oxigeno (sin vapor de agua)
  !	tagu .. temperatura media de la capa de vapor de agua (k)
  !	toxi .. temperatura media de la capa de oxigeno (k)
  !
  ! J.Cernicharo
  !----------------------------------------------------------------------
  integer np,ilag,ilox,ier
  real agu,oxi,v,temi,tatm,tag,tagu,tox,toxi,kvat,ama
  real h(*),p(*),t(*),rho(*)
  !
  integer j
  real r,pr,tem,dh,ox,ag,kv
  real kh2o, ko2
  !
  temi=0.
  kv=0.
  tag=0.
  tox=0.
  agu=0.
  oxi=0.
  do j=1,np
    r=rho(j)
    pr=p(j)
    tem=t(j)
    dh=h(j)
    ag=kh2o(r,tem,pr,v,ilag)*dh*ama
    ox=ko2(tem,pr,v,ilox)*dh*ama
    call jnu(tem,v,tem)        ! Can give TEM as output
    tag=tag+tem*exp(-agu)*(1.-exp(-ag))
    agu=agu+ag
    tox=tox+tem*exp(-oxi)*(1.-exp(-ox))
    oxi=oxi+ox
    temi=temi+tem*exp(-kv)*(1.-exp(-ag-ox))
    kv=agu+oxi
  enddo
  kvat=kv
  if ( kv.le.1.e-10 ) then
    ier = 1
  elseif ( oxi.le.1.e-20) then
    ier = 2
  elseif ( agu.le.1.e-20 ) then
    ier = 3
  else
    tatm=temi/(1.-exp(-kv))
    tagu=tag/(1.-exp(-agu))
    toxi=tox/(1.-exp(-oxi))
    ier = 0
  endif
end subroutine kvatm
!
subroutine jnu(j,v,t)
  real j                       ! Rayleigh Jeans temperature
  real v                       ! Frequency in GHz
  real t                       ! Temperature
  logical l
  !
  real hsurk, r
  parameter (hsurk=6.62e-34*1e9/1.38e-23)
  logical rayleigh_case
  save rayleigh_case
  !
  if (rayleigh_case) then
    r = hsurk*v/(exp(hsurk*v/t)-1)
    j = r
  else
    j = t
  endif
  return
  !
  entry atm_jnu (l)
  rayleigh_case = l
end subroutine jnu
