module atm_interfaces
  !---------------------------------------------------------------------
  ! ATM interfaces, all kinds (private or public). Do not use directly
  ! out of the library.
  !---------------------------------------------------------------------
  !
  use atm_interfaces_public
  use atm_interfaces_private
  !
end module atm_interfaces
