subroutine telcal_setup(line,error)
  use telcal_interfaces, except_this=>telcal_setup
  use gkernel_interfaces
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! TELCAL
  ! Support routine for command
  !   SET ATM
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Local
  character(len=*), parameter :: rname='TELCAL_SET'
  character(len=*), parameter :: echain='E-'//rname//',  '
  integer, parameter :: mkeys=1
  integer :: nkey,nc
  character(len=12) :: arg,key,keys(mkeys)
  ! Data
  data keys /'ATM'/
  !
  call sic_ke(line,0,1,arg,nc,.true.,error)
  if (error) return
  call sic_ambigs('SET',arg,key,nkey,keys,mkeys,error)
  if (error) return
  !
  select case(key)
  !
  ! ATM model
  case ('ATM')
    call sic_ch(line,0,2,arg,nc,.true.,error)
    if (error) return
    call sic_upper(arg)  ! 'OLD', 'NEW', '1985', ...
    call atm_setup(arg,error)
  !
  case default
    call gagout(echain//trim(key)//' not yet implemented')
    error = .true.
  end select
  !
end subroutine telcal_setup
