module telcal_interfaces_private
  interface
    subroutine telcal_reallocate_chopper(n,chopper,error)
      use gbl_message
      use chopper_definitions
      !-------------------------------------------------------------------
      ! @ private
      !-------------------------------------------------------------------
      integer(kind=4), intent(in)    :: n
      type(chopper_t), intent(inout) :: chopper
      logical,         intent(inout) :: error
    end subroutine telcal_reallocate_chopper
  end interface
  !
  interface
    subroutine telcal_free_chopper(chopper,error)
      use gbl_message
      use chopper_definitions
      !-------------------------------------------------------------------
      ! @ private
      !-------------------------------------------------------------------
      type(chopper_t), intent(inout) :: chopper
      logical,         intent(inout) :: error
    end subroutine telcal_free_chopper
  end interface
  !
  interface
    subroutine telcal_chopper_NwaterN(search,tel,nfreq,freqs,loads,recs,atms,flags)
      use chopper_definitions
      !---------------------------------------------------------------------
      ! @ private
      ! Compute N independent water vapor amounts for the N frequencies
      !---------------------------------------------------------------------
      type(chop_mode), intent(in)    :: search
      type(telescope), intent(in)    :: tel
      integer(kind=4), intent(in)    :: nfreq
      type(dsb_value), intent(in)    :: freqs(nfreq)
      type(chop_meas), intent(in)    :: loads(nfreq)
      type(mixer),     intent(inout) :: recs(nfreq)
      type(atm_prop),  intent(inout) :: atms(nfreq)
      integer(kind=4), intent(inout) :: flags(nfreq)
    end subroutine telcal_chopper_NwaterN
  end interface
  !
  interface
    subroutine telcal_chopper_1water1(search,tel,freq,load,rec,atm,flag)
      use gbl_message
      use chopper_definitions
      !---------------------------------------------------------------------
      ! @ private
      ! Compute 1 water vapor amount for 1 frequency
      !---------------------------------------------------------------------
      type(chop_mode), intent(in)    :: search
      type(telescope), intent(in)    :: tel
      type(dsb_value), intent(in)    :: freq
      type(chop_meas), intent(in)    :: load
      type(mixer),     intent(inout) :: rec
      type(atm_prop),  intent(inout) :: atm
      integer(kind=4), intent(inout) :: flag
    end subroutine telcal_chopper_1water1
  end interface
  !
  interface
    subroutine telcal_chopper_1waterN(search,tel,nfreq,freqs,loads,recs,atms,flags)
      use gbl_message
      use chopper_definitions
      !---------------------------------------------------------------------
      ! @ private
      ! Compute 1 water vapor amount for the N frequencies
      !---------------------------------------------------------------------
      type(chop_mode), intent(in)    :: search
      type(telescope), intent(in)    :: tel
      integer(kind=4), intent(in)    :: nfreq
      type(dsb_value), intent(in)    :: freqs(nfreq)
      type(chop_meas), intent(in)    :: loads(nfreq)
      type(mixer),     intent(inout) :: recs(nfreq)
      type(atm_prop),  intent(inout) :: atms(nfreq)
      integer(kind=4), intent(inout) :: flags(nfreq)
    end subroutine telcal_chopper_1waterN
  end interface
  !
  interface
    subroutine telcal_chopper_temperatures(tel,load,dotrec,rec,temi0)
      use chopper_definitions
      !---------------------------------------------------------------------
      ! @ private
      !  Compute:
      !   - Trec (if enabled)
      !   - Temi0
      !---------------------------------------------------------------------
      type(telescope), intent(in)    :: tel
      type(chop_meas), intent(in)    :: load
      logical,         intent(in)    :: dotrec
      type(mixer),     intent(inout) :: rec
      real(kind=8),    intent(out)   :: temi0
    end subroutine telcal_chopper_temperatures
  end interface
  !
  interface
    subroutine atm_dsb_transmission(freq,atm,ier)
      use chopper_definitions
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type (dsb_value), intent(in)    :: freq !
      type (atm_prop),  intent(inout) :: atm  ! Atm%h2omm and others must be defined
      integer,          intent(inout) :: ier  ! Error flag
    end subroutine atm_dsb_transmission
  end interface
  !
  interface
    subroutine min_dnls(iflag,mdat,npar,x,fvec,fjac,ldfjac)
      !----------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      !
      !----------------------------------------------------------------------
      integer, intent(in)  :: iflag          ! Input flag
      integer, intent(in)  :: mdat           ! Number of data points
      integer, intent(in)  :: npar           ! Number of variable (ie non-fixed) fitted parameters
      integer, intent(in)  :: ldfjac         ! First dimension of fjac
      real(8), intent(in)  :: x(npar)        ! Variable fitted parameters
      real(8), intent(out) :: fvec(mdat)     ! Difference between the function value and the data points
      real(8), intent(out) :: fjac(ldfjac,*) ! Jacobian (ie partial derivatives of f)
    end subroutine min_dnls
  end interface
  !
  interface
    subroutine get_difference(dat,fun,fvec)
      use fit_definitions  ! Transmission of hidden (eg fixed) parameters
      !----------------------------------------------------------------------
      ! @ private
      !
      !----------------------------------------------------------------------
      type(simple_1d),  intent(in) :: dat ! Data vectors
      type(fit_fun), intent(inout) :: fun ! Fitting function description
      real(8), intent(out) :: fvec(dat%n) ! Difference between the function value and the data points
    end subroutine get_difference
  end interface
  !
  interface
    subroutine get_jacobian(dat,fun,var,fvec,fjac)
      use fit_definitions ! Transmission of hidden (eg fixed) parameters
      !----------------------------------------------------------------------
      ! @ private
      ! fvec must *NOT* be altered here (See dnsl1e documentation).
      !
      ! The difficulty here is that some parameters can be fixed, other
      ! variable. We calculate all derivatives in this version, and place
      ! them according to the indexing of the variable parameters ...
      !----------------------------------------------------------------------
      type(simple_1d),  intent(in) :: dat   ! Data vectors
      type(fit_var),    intent(in) :: var   !
      type(fit_fun), intent(inout) :: fun   ! Fitting function description
      real(8), intent(in)  :: fvec(dat%n)   ! Difference between the function value and the data points
      real(8), intent(out) :: fjac(dat%n,*) ! Jacobian (ie partial derivatives of f)
    end subroutine get_jacobian
  end interface
  !
  interface
    subroutine get_profile(fun,profile)
      use fit_definitions ! Transmission of hidden (eg fixed) parameters
      !----------------------------------------------------------------------
      ! @ private
      !
      !----------------------------------------------------------------------
      type(fit_fun), intent(in)      :: fun     ! Fitting function description
      type(simple_1d), intent(inout) :: profile ! Solution profile
    end subroutine get_profile
  end interface
  !
  interface
    subroutine fit_polynomial(dat,fun)
      use fit_definitions
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(simple_1d), intent(in)  :: dat ! Data vectors
      type(fit_fun), intent(inout) :: fun ! Fitting function description
    end subroutine fit_polynomial
  end interface
  !
  interface
    subroutine fit_xy_polynomial(line,error)
      use fit_definitions
      use gildas_def
      use gbl_format
      use gkernel_types
      !----------------------------------------------------------------------
      ! @ private
      ! TELCAL support routine for command:
      !     POLYNOM Degree Xvar Yvar Wvar
      ! Purpose: Computes the weighted least-square polynomial fit of a given
      !          degree
      !----------------------------------------------------------------------
      character(len=*), intent(in)  :: line  ! Command line
      logical,          intent(out) :: error ! Error flag
    end subroutine fit_xy_polynomial
  end interface
  !
  interface
    subroutine free_focus
      use focus_definitions
      !----------------------------------------------------------------------
      ! @ private
      ! TELCAL support routine for focus
      ! Purpose:
      !----------------------------------------------------------------------
      !
      ! Local variables
    end subroutine free_focus
  end interface
  !
  interface
    subroutine define_sic_focus
      use focus_definitions
      use gildas_def
      !----------------------------------------------------------------------
      ! @ private
      ! TELCAL support routine for focus
      ! Purpose:
      !----------------------------------------------------------------------
      !
      ! Local variables
    end subroutine define_sic_focus
  end interface
  !
  interface
    subroutine telcal_pack_init(gpack_id,error)
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      integer(kind=4) :: gpack_id
      logical :: error
    end subroutine telcal_pack_init
  end interface
  !
  interface
    subroutine telcal_pack_clean(error)
      !----------------------------------------------------------------------
      ! @ private
      ! Called at end of session.
      !----------------------------------------------------------------------
      logical :: error
    end subroutine telcal_pack_clean
  end interface
  !
  interface
    subroutine fit_xy_gauss(line,error)
      use fit_definitions
      use gildas_def
      use gbl_format
      use gkernel_types
      !----------------------------------------------------------------------
      ! @ private
      ! TELCAL support routine for command:
      !     GAUSS Xvar Yvar [Wvar]
      ! 1        [/DUAL]
      ! 2        [/BASE]
      ! 3        [/GUESS   Area1 Position1 Width1 [Area2/Area1 Position2-Position1 Width2/Width1] [Offset Slope]]
      ! 4        [/FIXED   Area1 Position1 Width1 [Area2/Area1 Position2-Position1 Width2/Width1] [Offset Slope]]
      ! 5        [/RESULTS Area1 Position1 Width1 [Area2/Area1 Position2-Position1 Width2/Width1] [Offset Slope]]]
      ! 6        [/PROFILE Y]
      !
      ! Purpose: Fit one or two Gaussian + a 1st order polynomial into given variables
      !          the 1st order polynomial fit is implied by the 2 Gaussian fit
      !----------------------------------------------------------------------
      character(len=*), intent(in)  :: line  ! Command line
      logical,          intent(out) :: error ! Error flag
    end subroutine fit_xy_gauss
  end interface
  !
  interface
    subroutine default_pointing_guess(guess)
      use point_definitions
      !----------------------------------------------------------------------
      ! @ private
      ! TELCAL support routine for pointing
      ! Purpose: Try to make clever guesses to prepare to solve for pointing
      !          parameters. A few comments:
      !            * It is safe to leave offset and slope values to zero as
      !              the fit easily find good values from those guesses
      !            * The initial guesses of area and position will anyway
      !              be changed in the solving process. They are present here
      !              for generality
      !            * The area_ratio and width_ratio guesses are quite easy.
      !            * Nevertheless, to obtain a robust fitting, it is mandatory
      !              that the user gives (outside this routine) useful guesses
      !              of the width (probably the beam FWHM) and the beam throw
      !          We have at most 8 parameters to fit. At high SNR, without bias
      !          this will be easy. However, this is rarely the case. Fixing
      !          the value of the area_ratio, beam_throw and width_ratio is a
      !          good way to obtain a robust value of the position (the goal).
      !          Indeed, it is easy to have good guesses for those values. While
      !          it is true that those guesses are not perfect, simulations
      !          show that this gives robust determination of the pointing
      !          error even when the fit looks biased.
      !----------------------------------------------------------------------
      type(point_guess_t), intent(out) :: guess ! User guesses from command line
    end subroutine default_pointing_guess
  end interface
  !
  interface
    subroutine init_pointing(ndat,nsol,psys,pdir,ptype,point,error)
      use point_definitions
      !----------------------------------------------------------------------
      ! @ private
      ! TELCAL support routine for pointing
      ! Purpose: Free, reallocate and initialize the POINT structure
      !          according to user defined input. Data must be filled
      !          outside
      !----------------------------------------------------------------------
      integer, intent(in) :: ndat             ! Number of data points
      integer, intent(in) :: nsol             ! Number of solution points
      integer, intent(in) :: psys             ! Pointing coordinate system
      character(len=16), intent(in) :: pdir   ! Pointing direction in above system
      character(len=16), intent(in) :: ptype  ! Pointing type (SINGLE|DUAL)
      type(pointing_t), intent(out) :: point  ! Pointing structure
      logical, intent(out) :: error           ! Error flag
    end subroutine init_pointing
  end interface
  !
  interface
    subroutine define_sic_pointing(point,varname,error)
      use point_definitions
      use gildas_def
      !----------------------------------------------------------------------
      ! @ private
      ! TELCAL support routine for pointing
      ! Purpose: (Re)define the SIC structure VARNAME associated to the
      !          FORTRAN90 structure POINT
      !----------------------------------------------------------------------
      type(pointing_t), intent(in) :: point     ! FORTRAN 90 pointing structure
      character(len=*), intent(in) :: varname   ! Name of the SIC structure associated to POINT
      logical, intent(out) :: error             ! Logical flag
    end subroutine define_sic_pointing
  end interface
  !
  interface
    subroutine solve_pointing(point,guess,verbose,error)
      use point_definitions
      use phys_const
      !----------------------------------------------------------------------
      ! @ private
      ! TELCAL support routine for pointing
      ! Purpose: Find the first guesses, fit and compute profile of fit
      !          solution. The fit is iterated with position guess every half
      !          FWHM in the x coordinate range. The best fit is then selected.
      !          In real cases, this implies of the order of 20 fits if the width
      !          parameter. This scheme provides a very robust solution.
      !----------------------------------------------------------------------
      type(pointing_t), intent(inout) :: point ! FORTRAN 90 pointing structure
      type(point_guess_t), intent(in) :: guess ! User guesses from command line
      logical, intent(in)  :: verbose          ! Verbose or quiet solving?
      logical, intent(out) :: error            ! Error flag
    end subroutine solve_pointing
  end interface
  !
  interface
    subroutine free_pointing(point)
      use point_definitions
      !----------------------------------------------------------------------
      ! @ private
      ! TELCAL support routine for pointing
      ! Purpose: Free memory associated to the FORTRAN 90 pointing structure
      !----------------------------------------------------------------------
      type(pointing_t), intent(inout) :: point ! FORTRAN 90 pointing structure
    end subroutine free_pointing
  end interface
  !
  interface
    subroutine init_point_cross(my_ncross,ndat,nsol,psys,pdir,ptype,error)
      use pcross_definitions
      use gildas_def
      !----------------------------------------------------------------------
      ! @ private
      ! TELCAL support routine for pointing through a cross
      ! Purpose: Initialize the PCROSS FORTRAN90 and SIC structures
      !----------------------------------------------------------------------
      integer, intent(in) :: my_ncross        ! Number of subscans real used in the PCROSS structure
      integer, intent(in) :: ndat(my_ncross)  ! Number of data points in each subscan
      integer, intent(in) :: nsol(my_ncross)  ! Number of solution points in each subscan
      integer, intent(in) :: psys(my_ncross)  ! Coordinate system of each subscan
      character(len=16), intent(in) :: pdir(my_ncross)  ! Direction of each subscan
      character(len=16), intent(in) :: ptype(my_ncross) ! Type of each subscan (SINGLE or DUAL)
      logical, intent(out) :: error
    end subroutine init_point_cross
  end interface
  !
  interface
    subroutine solve_point_cross(guess,verbose,plot,error)
      use pcross_definitions
      !----------------------------------------------------------------------
      ! @ private
      ! TELCAL support routine for pointing through a cross
      ! Purpose: Solve the telescope pointing parameters through a cross
      !          and plot the results
      !----------------------------------------------------------------------
      type(point_guess_t), intent(in)  :: guess   ! User guesses from command line
      logical,             intent(in)  :: verbose ! Verbose or quiet solve?
      logical,             intent(in)  :: plot    ! Do we plot the results?
      logical,             intent(out) :: error   ! Error flag
    end subroutine solve_point_cross
  end interface
  !
  interface
    subroutine free_point_cross
      use pcross_definitions
      use gildas_def
      !----------------------------------------------------------------------
      ! @ private
      ! TELCAL support routine for pointing through a cross
      ! Purpose: Free the PCROSS FORTRAN90 and SIC structures
      !----------------------------------------------------------------------
      ! Local
    end subroutine free_point_cross
  end interface
  !
  interface
    subroutine telcal_point(line,error)
      use pcross_definitions
      use gbl_constant
      use phys_const
      !----------------------------------------------------------------------
      ! @ private
      ! TELCAL support routine for command
      !     POINT
      ! 1        [/INIT  SINGLE|DUAL Ndat Nsol]
      ! 2        [/SOLVE Width [BeamThrow [Area_Ratio Width_Ratio]]]
      !----------------------------------------------------------------------
      character(len=*), intent(in)  :: line  ! Command line
      logical,          intent(out) :: error ! Error flag
    end subroutine telcal_point
  end interface
  !
  interface
    subroutine telcal_message_set_id(id)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Alter library id into input id. Should be called by the library
      ! which wants to share its id with the current one.
      !---------------------------------------------------------------------
      integer, intent(in) :: id
    end subroutine telcal_message_set_id
  end interface
  !
  interface
    subroutine telcal_message(mkind,procname,message)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Messaging facility for the current library. Calls the low-level
      ! (internal) messaging routine with its own identifier.
      !---------------------------------------------------------------------
      integer,          intent(in) :: mkind     ! Message kind
      character(len=*), intent(in) :: procname  ! Name of calling procedure
      character(len=*), intent(in) :: message   ! Message string
    end subroutine telcal_message
  end interface
  !
  interface
    subroutine telcal_setup(line,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! TELCAL
      ! Support routine for command
      !   SET ATM
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      logical :: error                  !
    end subroutine telcal_setup
  end interface
  !
  interface
    subroutine solve_skydip(tamb_in,pamb_in,alti_in,     &
                            nmeas_in,rec_in,skydip_in,   &
                            fit_mode_in,  &
                            params_out,   &
                            error)
      use chopper_definitions
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      ! SKYDIP Internal routine
      !   Main entrance routine to reduce a skydip.
      !   1) Copy input data into the real variables used for the reduction.
      !      These are shared through subroutines thanks to a module.
      !   2) Call the real work routine
      !----------------------------------------------------------------------
      real*4,             intent(in)    :: tamb_in         ! [K]
      real*4,             intent(in)    :: pamb_in         ! [mbar/hPa]
      real*4,             intent(in)    :: alti_in         ! [m]
      integer,            intent(in)    :: nmeas_in        !
      type (mixer),       intent(in)    :: rec_in(nmeas_in)     !
      type (skydip_meas), intent(in)    :: skydip_in(nmeas_in)  !
      logical,            intent(in)    :: fit_mode_in(2)  ! Which values to fit
      real*8,             intent(out)   :: params_out(*)   ! Output fitted parameters
      logical,            intent(inout) :: error           ! Error flag
    end subroutine solve_skydip
  end interface
  !
  interface
    subroutine solve_skydip_sub(error)
      !----------------------------------------------------------------------
      ! @ private
      ! SKYDIP Internal routine
      !   Reduce a skydip. Compute the starting atmospheric model.
      !----------------------------------------------------------------------
      logical, intent(inout) :: error  ! Error flag
    end subroutine solve_skydip_sub
  end interface
  !
  interface
    subroutine solve_skydip_set(inmeas,error)
      !---------------------------------------------------------------------
      ! @ private
      ! Prepare the environment (atmosphere...) and calibration variables
      ! for the inmeas-th skydip.
      !---------------------------------------------------------------------
      integer, intent(in)    :: inmeas  ! Skydip number
      logical, intent(inout) :: error   ! Logical error flag
    end subroutine solve_skydip_set
  end interface
  !
  interface
    subroutine skydip_display()
      !---------------------------------------------------------------------
      ! @ private
      ! SKYDIP Internal routine
      !   Display the input and fitted values
      !---------------------------------------------------------------------
      ! Local
    end subroutine skydip_display
  end interface
  !
  interface
    subroutine fitsky(fcn,liter,ier)
      use fit_minuit
      !----------------------------------------------------------------------
      ! @ private
      ! SKYDIP Internal routine
      !   Setup and starts a SKYDIP fit minimisation using MINUIT
      !----------------------------------------------------------------------
      external             :: fcn      ! Function to be mininized
      logical, intent(in)  :: liter    ! Logical of iterate fit
      integer, intent(out) :: ier      ! Error flag
    end subroutine fitsky
  end interface
  !
  interface
    subroutine fitsky_print(fit)
      use fit_minuit
      !---------------------------------------------------------------------
      ! @ private
      ! SKYDIP Internal routine
      !  Display fitted values and RMS
      !---------------------------------------------------------------------
      type(fit_minuit_t), intent(inout) :: fit  ! Fitting variables
    end subroutine fitsky_print
  end interface
  !
  interface
    subroutine midsky(fit,ier,liter)
      use fit_minuit
      !----------------------------------------------------------------------
      ! @ private
      ! SKYDIP Internal routine
      !   Start a SKYDIP fit by building the PAR array and internal
      !    variable used by Minuit.
      !   Parameters are
      !    PAR(1:NMEAS) = FEFF Forward efficiencies
      !     or            TREC Receivers temperature (K)
      !    PAR(PH2O)    = H2OMM Water vapor amount (mm)
      !    PAR(PTLOSS)  = TLOSS (K)   (if fitted)
      !----------------------------------------------------------------------
      type(fit_minuit_t), intent(inout) :: fit    !
      integer,            intent(out)   :: ier    ! Error code
      logical,            intent(in)    :: liter  ! Iterate a fit
    end subroutine midsky
  end interface
  !
  interface
    subroutine minsky(npar,grad,chi2,params,iflag)
      !----------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! SKYDIP Internal routine
      !   Function to be minimized in the SKYDIP.
      !----------------------------------------------------------------------
      integer, intent(in)  :: npar          ! Number of parameters
      real(8), intent(out) :: grad(npar)    ! Array of derivatives
      real(8), intent(out) :: chi2          ! Chi**2
      real(8), intent(in)  :: params(npar)  ! Parameter values
      integer, intent(in)  :: iflag         ! Code operation
    end subroutine minsky
  end interface
  !
  interface
    subroutine telcal_skydip_sic_default()
      !---------------------------------------------------------------------
      ! @ private
      ! Default the user's variables in order to detect unset variables
      !---------------------------------------------------------------------
      !
    end subroutine telcal_skydip_sic_default
  end interface
  !
  interface
    subroutine telcal_skydip_sic_check(error)
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine telcal_skydip_sic_check
  end interface
  !
  interface
    subroutine telcal_skydip(line,error)
      use gildas_def
      !----------------------------------------------------------------------
      ! @ private
      ! TELCAL Internal routine
      ! Support routine for command:
      !   SKYDIP
      ! 1        [/VARIABLE [N]]
      ! 2        [/MODE TREC|EFFICIENCY]
      ! Reduce a skydip from user level.
      !----------------------------------------------------------------------
      character(len=*), intent(in)    :: line  ! Input command line
      logical,          intent(inout) :: error ! Error flag
    end subroutine telcal_skydip
  end interface
  !
  interface
    subroutine telcal_skydip_results(error)
      use gildas_def
      use phys_const
      !---------------------------------------------------------------------
      ! @ private
      !
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  ! Logical error flag
    end subroutine telcal_skydip_results
  end interface
  !
  interface
    subroutine telcal_skydip_plot()
      use phys_const
      !----------------------------------------------------------------------
      ! @ private
      ! SKYDIP Internal routine
      !  Plot the data points and the fitted model
      !----------------------------------------------------------------------
      ! Local
    end subroutine telcal_skydip_plot
  end interface
  !
end module telcal_interfaces_private
