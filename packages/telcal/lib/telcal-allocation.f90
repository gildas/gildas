!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine telcal_reallocate_chopper(n,chopper,error)
  use gbl_message
  use gkernel_interfaces
  use telcal_interfaces, except_this=>telcal_reallocate_chopper
  use chopper_definitions
  !-------------------------------------------------------------------
  ! @ private
  !-------------------------------------------------------------------
  integer(kind=4), intent(in)    :: n
  type(chopper_t), intent(inout) :: chopper
  logical,         intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='REALLOCATE>CHOPPER'
  character(len=message_length) :: mess
  logical :: alloc
  integer(kind=4) :: ier
  real(kind=8), parameter :: telcal_null=-1000.d0
  !
  call telcal_message(seve%t,rname,'Welcome')
  !
  if (n.le.0) then
     write(mess,'(a,i0)') 'Input array size is <= 0: ',n
     call telcal_message(seve%t,rname,mess)
     error = .true.
     return
  endif
  if (chopper%n.eq.n) then
     write(mess,'(a,i0)') 'Chopper arrays already associated at the right size: ',n
     call telcal_message(seve%d,rname,mess)
     alloc = .false.
  else
     call telcal_free_chopper(chopper,error)
     if (error)  return
     alloc = .true.
  endif
  if (alloc) then
     allocate( &
          chopper%freqs(n),chopper%loads(n),chopper%recs(n), &
          chopper%atms(n),chopper%tcals(n),chopper%atsyss(n), &
          chopper%tsyss(n),chopper%errors(n),&
          stat=ier)
     if (failed_allocate(rname,"chopper",ier,error)) then
        call telcal_free_chopper(chopper,error)
        return
     endif
     write(mess,'(a,i0)') 'Allocated chopper arrays of size: ',n
     call telcal_message(seve%d,rname,mess)
  endif
  !
  ! Allocation success => Initialize
  chopper%n = n
  chopper%bad = telcal_null
  !
end subroutine telcal_reallocate_chopper
!
subroutine telcal_free_chopper(chopper,error)
  use gbl_message
  use telcal_interfaces, except_this => telcal_free_chopper
  use chopper_definitions
  !-------------------------------------------------------------------
  ! @ private
  !-------------------------------------------------------------------
  type(chopper_t), intent(inout) :: chopper
  logical,         intent(inout) :: error
  !
  character(len=*), parameter :: rname='TELCAL>FREE>CHOPPER'
  !
  call telcal_message(seve%t,rname,'Welcome')
  !
  chopper%n = 0
  if (associated(chopper%freqs))  deallocate(chopper%freqs)
  if (associated(chopper%loads))  deallocate(chopper%loads)
  if (associated(chopper%recs))   deallocate(chopper%recs)
  if (associated(chopper%atms))   deallocate(chopper%atms)
  if (associated(chopper%tcals))  deallocate(chopper%tcals)
  if (associated(chopper%atsyss)) deallocate(chopper%atsyss)
  if (associated(chopper%tsyss))  deallocate(chopper%tsyss)
  if (associated(chopper%errors)) deallocate(chopper%errors)
  ! Nullification is implicit with deallocate
  !
end subroutine telcal_free_chopper
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
