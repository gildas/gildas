!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage TELCAL messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module telcal_message_private
  use gpack_def
  !
  ! Identifier used for message identification
  integer :: telcal_message_id = gpack_global_id  ! Default value for startup message
  !
end module telcal_message_private
!
subroutine telcal_message_set_id(id)
  use telcal_interfaces, except_this=>telcal_message_set_id
  use telcal_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer, intent(in) :: id
  ! Local
  character(len=message_length) :: mess
  !
  telcal_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',telcal_message_id
  call telcal_message(seve%d,'telcal_message_set_id',mess)
  !
end subroutine telcal_message_set_id
!
subroutine telcal_message(mkind,procname,message)
  use telcal_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Messaging facility for the current library. Calls the low-level
  ! (internal) messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  !
  call gmessage_write(telcal_message_id,mkind,procname,message)
  !
end subroutine telcal_message
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
