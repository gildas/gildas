!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage the TELCAL package
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine telcal_pack_set(pack)
  use gpack_def
  use gkernel_interfaces
  !----------------------------------------------------------------------
  ! @ public
  !----------------------------------------------------------------------
  type(gpack_info_t), intent(out) :: pack
  !
  external :: atm_pack_set
  external :: telcal_pack_init
  external :: telcal_pack_clean
  !
  pack%name='telcal'
  pack%ext='.telcal'
  pack%depend(1:2) = (/ locwrd(atm_pack_set), locwrd(greg_pack_set) /)
  pack%init=locwrd(telcal_pack_init)
  pack%clean=locwrd(telcal_pack_clean)
  pack%authors="J.Pety, S.Guilloteau, S.Bardeau"
  !
end subroutine telcal_pack_set
!
subroutine telcal_pack_init(gpack_id,error)
  use gkernel_interfaces
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  integer(kind=4) :: gpack_id
  logical :: error
  ! Local
  integer(kind=4) :: ier
  !
  call telcal_message_set_id(gpack_id)
  !
  ! Specific variables
  ier = sic_setlog('gag_help_telcal','gag_doc:hlp/telcal-help-telcal.hlp')
  if (ier.eq.0) then
    error=.true.
    return
  endif
  !
  ! Load local language
  call load_telcal
  !
end subroutine telcal_pack_init
!
subroutine telcal_pack_clean(error)
  !----------------------------------------------------------------------
  ! @ private
  ! Called at end of session.
  !----------------------------------------------------------------------
  logical :: error
  !
  ! Free memory
  call free_point_cross
  call free_focus
  !
end subroutine telcal_pack_clean
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
