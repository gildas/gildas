module telcal_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! TELCAL interfaces, all kinds (private or public). Do not use
  ! directly out of the library.
  !---------------------------------------------------------------------
  !
  use telcal_interfaces_public
  use telcal_interfaces_private
  !
end module telcal_interfaces
