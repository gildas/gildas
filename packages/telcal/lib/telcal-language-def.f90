!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! TELCAL initialization routine
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine load_telcal
  use telcal_interfaces, except_this=>load_telcal
  !----------------------------------------------------------------------
  ! @ public
  ! TELCAL Support routine
  ! Purpose: Language is loaded
  !----------------------------------------------------------------------
  ! Global
  logical, external :: gr_error
  ! Local variables
  logical :: error
  integer, parameter :: mtelcal=17 ! telcal language
  character(len=12) :: vocab(mtelcal)
  character(len=76) :: version
  !
  data version /'0.4  05-Mar-2006 J.P., S.G., H.W.'/
  data vocab /' FOCUS',                     &
              ' GAUSS', '/DUAL', '/BASE', '/GUESS', '/FIXED', '/RESULTS', '/PROFILE', &
              ' POLYNOMIAL',                &
              ' POINT', '/INIT', '/SOLVE',  &
              ' SET',                       &
              ' SKYDIP', '/VARIABLE', '/MODE', '/PLOT' /
  !
  error = gr_error()
  call sic_begin('TELCAL','GAG_HELP_TELCAL',mtelcal,vocab,version, &
  & run_telcal,gr_error)
  !
  call init_focus(5)
  call define_sic_focus
  !
  ! Initialize ATM
  call atm_sicvariables(error)
  if (error) then
    call gagout('E-LOAD_TELCAL,  Error creating ATM% variables')
    return
  endif
  !
end subroutine load_telcal
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine run_telcal(line,comm,error)
  use telcal_interfaces, except_this=>run_telcal
  !----------------------------------------------------------------------
  ! @ public-mandatory (because symbol is used elsewhere)
  ! TELCAL Main library entry point
  ! Purpose: Call appropriate subroutine according to COMM
  !----------------------------------------------------------------------
  character(len=*), intent(in)    :: line  ! Command line
  character(len=*), intent(in)    :: comm  ! Typed command
  logical,          intent(inout) :: error ! Error flag
  !
  select case(comm)
  case ('FOCUS')
    call solve_focus(line,error)
  case ('GAUSS')
    call fit_xy_gauss(line,error)
  case ('POINT')
    call telcal_point(line,error)
  case ('POLYNOMIAL')
    call fit_xy_polynomial(line,error)
  case ('SET')
    call telcal_setup(line,error)
  case ('SKYDIP')
    call telcal_skydip(line,error)
  case default
    call gagout('I-TELCAL, '//trim(comm)//' not yet implemented')
  end select
  !
end subroutine run_telcal
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
