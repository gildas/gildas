module telcal_interfaces_public
  interface
    subroutine telcal_chopper(chopper,error)
      use gbl_message
      use chopper_definitions
      !----------------------------------------------------------------------
      ! @ public
      !
      ! Compute Trec, Tcal, Tsys from the chop and atm values observed
      ! according to the usual chopper method (ulich and haas) Double side band
      ! operation.
      !
      ! Note that each for each frequency, an error status is set in
      ! chopper%errors(ifreq). It indicates if the operation could be performed
      ! or not. It is the responsibility of the caller to check them or not,
      ! e.g.
      !
      !   call telcal_chopper(chopper,error)
      !   if (error .or. any(chopper%errors(:))  return
      !
      ! If the error status of a given frequency is set to .true., the output
      ! parameters are also nullified to the value chopper%bad, so that the
      ! caller can identify them as bad values.
      ! ----------------------------------------------------------------------
      type(chopper_t), intent(inout) :: chopper
      logical,         intent(inout) :: error
    end subroutine telcal_chopper
  end interface
  !
  interface
    subroutine telcal_chopper_nullify(chopper,ifreq)
      use chopper_definitions
      !---------------------------------------------------------------------
      ! @ public
      !  Nullify the chopper results (only the outputs, not the inputs) e.g.
      ! in case of error
      !---------------------------------------------------------------------
      type(chopper_t), intent(inout) :: chopper
      integer(kind=4) :: ifreq
    end subroutine telcal_chopper_nullify
  end interface
  !
  interface
    function airmass(el,error)
      use gbl_message
      !----------------------------------------------------------------------
      ! @ public
      ! Compute airmass number corresponding to elevation EL
      ! Use a curved atmosphere, equivalent height 5.5 km
      !----------------------------------------------------------------------
      real(8), intent(in)  :: el      ! Elevation [radian]
      logical, intent(out) :: error   ! Error flag
      real(8)              :: airmass ! Airmass
    end function airmass
  end interface
  !
  interface
    subroutine fit_1d(dat,fun,verbose)
      use fit_definitions
      !----------------------------------------------------------------------
      ! @ public
      ! General unidimensional fitting routine
      !----------------------------------------------------------------------
      type(simple_1d), intent(in)  :: dat ! Data vectors
      type(fit_fun), intent(inout) :: fun ! Fitting function description
      logical, intent(in)      :: verbose ! Verbose or quiet?
    end subroutine fit_1d
  end interface
  !
  interface
    subroutine null_simple_1d(dat)
      use fit_definitions
      !----------------------------------------------------------------------
      ! @ public
      ! TELCAL support routine of its derived types
      ! Purpose: Initialize a simple precision data vector
      !----------------------------------------------------------------------
      type(simple_1d), intent(inout) :: dat ! Data vector
    end subroutine null_simple_1d
  end interface
  !
  interface
    subroutine null_parameter(par)
      use fit_definitions
      !----------------------------------------------------------------------
      ! @ public
      ! TELCAL support routine of its derived types
      ! Purpose: Initialize a fitted parameter
      !----------------------------------------------------------------------
      type(fit_par), intent(inout) :: par ! Fitted parameter
    end subroutine null_parameter
  end interface
  !
  interface
    subroutine null_function(fun)
      use fit_definitions
      !----------------------------------------------------------------------
      ! @ public
      ! TELCAL support routine of its derived types
      ! Purpose: Initialize a fitted function description
      !----------------------------------------------------------------------
      type(fit_fun), intent(inout) :: fun ! Description of the fitted function
    end subroutine null_function
  end interface
  !
  interface
    subroutine copy_simple_1d(in,out)
      use fit_definitions
      !----------------------------------------------------------------------
      ! @ public
      ! TELCAL support routine of its derived types
      ! Purpose: Copy a simple precision data vector
      !----------------------------------------------------------------------
      type(simple_1d), intent(in)    :: in  ! Original vector
      type(simple_1d), intent(inout) :: out ! Copied vector
    end subroutine copy_simple_1d
  end interface
  !
  interface
    subroutine copy_function(in,out)
      use fit_definitions
      !----------------------------------------------------------------------
      ! @ public
      ! TELCAL support routine of its derived types
      ! Purpose: Copy the description of a fitted function
      !----------------------------------------------------------------------
      type(fit_fun), intent(in)    :: in  ! Original function
      type(fit_fun), intent(inout) :: out ! Copied function
    end subroutine copy_function
  end interface
  !
  interface
    subroutine solve_focus(line,error)
      use fit_definitions
      use focus_definitions
      !----------------------------------------------------------------------
      ! @ public
      ! TELCAL support routine for command
      !    FOCUS [Polynomial|Lorentzian|Gaussian]
      !----------------------------------------------------------------------
      character(*), intent(in)  :: line  ! Command line
      logical,      intent(out) :: error ! Error flag
    end subroutine solve_focus
  end interface
  !
  interface
    subroutine init_focus(ndat)
      use focus_definitions
      !----------------------------------------------------------------------
      ! @ public
      ! TELCAL support routine for focus
      ! Purpose:
      !----------------------------------------------------------------------
      integer, intent(in) :: ndat ! Number of data points
    end subroutine init_focus
  end interface
  !
  interface
    subroutine load_telcal
      !----------------------------------------------------------------------
      ! @ public
      ! TELCAL Support routine
      ! Purpose: Language is loaded
      !----------------------------------------------------------------------
      ! Global
    end subroutine load_telcal
  end interface
  !
  interface
    subroutine run_telcal(line,comm,error)
      !----------------------------------------------------------------------
      ! @ public-mandatory (because symbol is used elsewhere)
      ! TELCAL Main library entry point
      ! Purpose: Call appropriate subroutine according to COMM
      !----------------------------------------------------------------------
      character(len=*), intent(in)    :: line  ! Command line
      character(len=*), intent(in)    :: comm  ! Typed command
      logical,          intent(inout) :: error ! Error flag
    end subroutine run_telcal
  end interface
  !
  interface
    subroutine telcal_pack_set(pack)
      use gpack_def
      !----------------------------------------------------------------------
      ! @ public
      !----------------------------------------------------------------------
      type(gpack_info_t), intent(out) :: pack
    end subroutine telcal_pack_set
  end interface
  !
end module telcal_interfaces_public
