!-----------------------------------------------------------------------
! Support file for SKYDIP command in TELCAL (testing purpose only!)
!-----------------------------------------------------------------------
module telcal_skydip_sic
  use chopper_definitions
  !---------------------------------------------------------------------
  ! SIC support variables for a user instance of a skydip (SKYDIP%)
  ! To be able to handle 'nmeas' measurement sets, SIC variables can not
  ! map the (array of) derived type. We must define temporary arrays
  ! with elements contiguous in memory. Copy is made when required.
  !---------------------------------------------------------------------
  logical :: first = .true.
  !
  ! Atmosphere definition
  real*4, save :: atm_tamb,atm_pamb,atm_alti
  !
  integer, parameter :: mmeas=10 ! Maximum number of measurements to reduce simultaneously
  integer :: nmeas=0             ! Number of effective measurement set requested by user
  !
  ! Input parameters
  logical :: trec_mode,tloss_mode
  !
  ! Receiver(s)
  type (mixer), save :: sic_rec(mmeas)
  real*8, save :: sic_rec_temp(mmeas)
  real*8, save :: sic_rec_feff(mmeas)
  real*8, save :: sic_rec_sbgr(mmeas)
  !
  ! Skydip measurements
  type (skydip_meas), save :: sic_skydip(mmeas)
  real*8,  save :: sic_skydip_freq_s(mmeas)
  real*8,  save :: sic_skydip_freq_i(mmeas)
  integer, save :: sic_skydip_nsky(mmeas)
  real*8,  save :: sic_skydip_elev(msky,mmeas)
  real*8,  save :: sic_skydip_emiss(msky,mmeas)
  real*8,  save :: sic_skydip_hot(msky,mmeas)
  real*8,  save :: sic_skydip_thot(msky,mmeas)
  real*8,  save :: sic_skydip_cold(msky,mmeas)
  real*8,  save :: sic_skydip_tcold(msky,mmeas)
  !
  ! Define non-sense defaults (easily detectable) for each of these values
  real*8  :: sic_rec_temp_def = -1.d0
  real*8  :: sic_rec_feff_def = -1.d0
  real*8  :: sic_rec_sbgr_def = -1.d0
  real*8  :: sic_skydip_freq_s_def = -1.d0
  real*8  :: sic_skydip_freq_i_def = -1.d0
  integer :: sic_skydip_nsky_def   = -1
  real*8  :: sic_skydip_elev_def   = -1.d0
  real*8  :: sic_skydip_emiss_def  = -1.d0
  real*8  :: sic_skydip_hot_def    = -1.d0
  real*8  :: sic_skydip_thot_def   = -1.d0
  real*8  :: sic_skydip_cold_def   = -1.d0
  real*8  :: sic_skydip_tcold_def  = -1.d0
  !
  ! Output parameters
  real*8 :: params(mmeas+2)
  !
  ! Fitted (interpolated) model
  integer, parameter :: npmodel=50  ! Number of points in the model
  real*4, save :: fit_elev(npmodel),fit_tsky(npmodel,mmeas)
  !
end module telcal_skydip_sic
!
subroutine telcal_skydip_sic_default()
  use telcal_interfaces, except_this=>telcal_skydip_sic_default
  use telcal_skydip_sic
  !---------------------------------------------------------------------
  ! @ private
  ! Default the user's variables in order to detect unset variables
  !---------------------------------------------------------------------
  !
  sic_rec_temp = sic_rec_temp_def
  sic_rec_feff = sic_rec_feff_def
  sic_rec_sbgr = sic_rec_sbgr_def
  sic_skydip_freq_s = sic_skydip_freq_s_def
  sic_skydip_freq_i = sic_skydip_freq_i_def
  sic_skydip_nsky   = sic_skydip_nsky_def
  sic_skydip_elev   = sic_skydip_elev_def
  sic_skydip_emiss  = sic_skydip_emiss_def
  sic_skydip_hot    = sic_skydip_hot_def
  sic_skydip_thot   = sic_skydip_thot_def
  sic_skydip_cold   = sic_skydip_cold_def
  sic_skydip_tcold  = sic_skydip_tcold_def
  !
end subroutine telcal_skydip_sic_default
!
subroutine telcal_skydip_sic_check(error)
  use telcal_interfaces, except_this=>telcal_skydip_sic_check
  use telcal_skydip_sic
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='SKYDIP'
  character(len=*), parameter :: echain='E-'//rname//',  '
  !
  if (all(sic_rec_temp(1:nmeas).eq.sic_rec_temp_def)) then
    call gagout(echain//'REC%TEMP is not set')
    error = .true.
    return
  endif
  !
  if (all(sic_rec_feff(1:nmeas).eq.sic_rec_feff_def)) then
    call gagout(echain//'REC%FEFF is not set')
    error = .true.
    return
  endif
  !
  if (all(sic_rec_sbgr(1:nmeas).eq.sic_rec_sbgr_def)) then
    call gagout(echain//'REC%GAINIMA is not set')
    error = .true.
    return
  endif
  !
  if (all(sic_skydip_freq_s(1:nmeas).eq.sic_skydip_freq_s_def)) then
    call gagout(echain//'DATA%FREQSIG is not set')
    error = .true.
    return
  endif
  !
  if (all(sic_skydip_freq_i(1:nmeas).eq.sic_skydip_freq_i_def)) then
    call gagout(echain//'DATA%FREQIMA is not set')
    error = .true.
    return
  endif
  !
  if (all(sic_skydip_nsky(1:nmeas).eq.sic_skydip_nsky_def)) then
    call gagout(echain//'DATA%NDATA is not set')
    error = .true.
    return
  endif
  !
  if (all(sic_skydip_elev(:,1:nmeas).eq.sic_skydip_elev_def)) then
    call gagout(echain//'DATA%ELEV is not set')
    error = .true.
    return
  endif
  !
  if (all(sic_skydip_emiss(:,1:nmeas).eq.sic_skydip_emiss_def)) then
    call gagout(echain//'DATA%EMISS is not set')
    error = .true.
    return
  endif
  !
  if (all(sic_skydip_hot(:,1:nmeas).eq.sic_skydip_hot_def)) then
    call gagout(echain//'DATA%HOT is not set')
    error = .true.
    return
  endif
  !
  if (all(sic_skydip_thot(:,1:nmeas).eq.sic_skydip_thot_def)) then
    call gagout(echain//'DATA%THOT is not set')
    error = .true.
    return
  endif
  !
  if (all(sic_skydip_cold(:,1:nmeas).eq.sic_skydip_cold_def)) then
    call gagout(echain//'DATA%COLD is not set')
    error = .true.
    return
  endif
  !
  if (all(sic_skydip_tcold(:,1:nmeas).eq.sic_skydip_tcold_def)) then
    call gagout(echain//'DATA%TCOLD is not set')
    error = .true.
    return
  endif
  !
end subroutine telcal_skydip_sic_check
!
subroutine telcal_skydip(line,error)
  use gildas_def
  use gkernel_interfaces
  use telcal_interfaces, except_this=>telcal_skydip
  use telcal_skydip_sic
  !----------------------------------------------------------------------
  ! @ private
  ! TELCAL Internal routine
  ! Support routine for command:
  !   SKYDIP
  ! 1        [/VARIABLE [N]]
  ! 2        [/MODE TREC|EFFICIENCY]
  ! Reduce a skydip from user level.
  !----------------------------------------------------------------------
  character(len=*), intent(in)    :: line  ! Input command line
  logical,          intent(inout) :: error ! Error flag
  ! Local
  character(len=*), parameter :: rname='SKYDIP'
  character(len=*), parameter :: echain='E-'//rname//',  '
  character(len=*), parameter :: ichain='I-'//rname//',  '
  character(len=100) :: mess
  integer :: nc,imeas
  integer(kind=index_length) :: dims(4)
  character(len=2) :: argum
  logical :: fit_mode(2)
  !
  ! /VARIABLE
  if (sic_present(1,0)) then
    !
    nmeas = 1  ! Default only 1 skydip measurement set
    call sic_i4(line,1,1,nmeas,.false.,error)
    if (error) return
    if (nmeas.le.0 .or. nmeas.gt.mmeas) then
      write (mess,'(A,I2,A)')  &
        'Invalid number of skydip measurements (min 1, max ',mmeas,')'
      call gagout(echain//mess)
      error = .true.
      return
    endif
    !
    if (.not.first) then
      call sic_delvariable('SKYDIP',.false.,error)
      if (error) then
        call gagout(echain//'Deleting SKYDIP% structure')
        return
      endif
    endif
    !
    call sic_defstructure('SKYDIP',.true.,error)
    if (error) return
    !
    call sic_defstructure('SKYDIP%ATM',.true.,error)
    call sic_def_real('SKYDIP%ATM%TAMB',atm_tamb,0,1,.false.,error)
    call sic_def_real('SKYDIP%ATM%PAMB',atm_pamb,0,1,.false.,error)
    call sic_def_real('SKYDIP%ATM%ALTI',atm_alti,0,1,.false.,error)
    if (error) return
    !
    call sic_defstructure('SKYDIP%REC',.true.,error)
    call sic_def_dble('SKYDIP%REC%TEMP',   sic_rec_temp,1,nmeas,.false.,error)
    call sic_def_dble('SKYDIP%REC%FEFF',   sic_rec_feff,1,nmeas,.false.,error)
    call sic_def_dble('SKYDIP%REC%GAINIMA',sic_rec_sbgr,1,nmeas,.false.,error)
    if (error) return
    !
    dims(1) = msky
    dims(2) = nmeas
    call sic_defstructure('SKYDIP%DATA',.true.,error)
    call sic_def_dble('SKYDIP%DATA%FREQSIG',sic_skydip_freq_s,1,nmeas,.false.,error)
    call sic_def_dble('SKYDIP%DATA%FREQIMA',sic_skydip_freq_i,1,nmeas,.false.,error)
    call sic_def_inte('SKYDIP%DATA%NDATA',  sic_skydip_nsky,  1,nmeas,.false.,error)
    call sic_def_dble('SKYDIP%DATA%ELEV',   sic_skydip_elev,  2,dims, .false.,error)
    call sic_def_dble('SKYDIP%DATA%EMISS',  sic_skydip_emiss, 2,dims, .false.,error)
    call sic_def_dble('SKYDIP%DATA%HOT',    sic_skydip_hot,   2,dims, .false.,error)
    call sic_def_dble('SKYDIP%DATA%THOT',   sic_skydip_thot,  2,dims, .false.,error)
    call sic_def_dble('SKYDIP%DATA%COLD',   sic_skydip_cold,  2,dims, .false.,error)
    call sic_def_dble('SKYDIP%DATA%TCOLD',  sic_skydip_tcold, 2,dims, .false.,error)
    if (error) return
    !
    first = .false.
    write (mess,'(A,I2,A)')  &
      'Structure SKYDIP% (re)created for ',nmeas,' set of skydips.'
    call gagout(ichain//mess)
    !
    call telcal_skydip_sic_default()
    !
    return
  endif
  !
  ! /MODE [TREC|EFF [TLOSS] ]
  trec_mode = .false.
  tloss_mode = .false.
  if (sic_present(2,0)) then
    ! 1st argument: TREC|EFF
    argum = 'EF'
    call sic_ke(line,2,1,argum,nc,.false.,error)
    if (error) return
    if (argum.eq.'TR') then
      trec_mode = .true.
      ! Disable Trec mode until the formulas have been checked. There
      ! are inconsistencies and inhomogeneities...
      call gagout(echain//'Unsupported TREC mode')
      error = .true.
      return
    elseif (argum.eq.'EF') then
      trec_mode = .false.
    else
      call gagout(echain//'Invalid argument '//argum)
      error = .true.
      return
    endif
    !
    ! 2nd argument: TLOSS
    argum = ''
    call sic_ke(line,2,2,argum,nc,.false.,error)
    if (error) return
    tloss_mode = argum.eq.'TL'
    !
  endif
  !
  if (nmeas.eq.0) then
    call gagout(echain//'No data. Call SKYDIP /VAR [N] and fill the structure.')
    error = .true.
    return
  endif
  !
  ! Check if user has filled all the values
  call telcal_skydip_sic_check(error)
  if (error) return
  !
  ! Copy arrays in structure
  do imeas=1,nmeas
    sic_rec(imeas)%temp = sic_rec_temp(imeas)
    sic_rec(imeas)%feff = sic_rec_feff(imeas)
    sic_rec(imeas)%sbgr = sic_rec_sbgr(imeas)
    !
    sic_skydip(imeas)%freq%s         = sic_skydip_freq_s(imeas)
    sic_skydip(imeas)%freq%i         = sic_skydip_freq_i(imeas)
    sic_skydip(imeas)%nsky           = sic_skydip_nsky(imeas)
    sic_skydip(imeas)%elev(:)        = sic_skydip_elev(:,imeas)
    sic_skydip(imeas)%emiss(:)%count = sic_skydip_emiss(:,imeas)
    sic_skydip(imeas)%hot(:)%count   = sic_skydip_hot(:,imeas)
    sic_skydip(imeas)%hot(:)%temp    = sic_skydip_thot(:,imeas)
    sic_skydip(imeas)%cold(:)%count  = sic_skydip_cold(:,imeas)
    sic_skydip(imeas)%cold(:)%temp   = sic_skydip_tcold(:,imeas)
  enddo
  !
  ! Compute
  fit_mode = (/ trec_mode,tloss_mode /)
  call solve_skydip(atm_tamb,atm_pamb,atm_alti,  &
                    nmeas,sic_rec,sic_skydip,    &
                    fit_mode,                    &
                    params,                      &
                    error)
  if (error) return
  !
  ! Compute the fitted model and store in SIC structure
  call telcal_skydip_results(error)
  if (error) return
  !
  if (sic_present(3,0))  &
     call telcal_skydip_plot()
  !
end subroutine telcal_skydip
!
subroutine telcal_skydip_results(error)
  use gildas_def
  use phys_const
  use gkernel_interfaces
  use telcal_interfaces, except_this=>telcal_skydip_results
  use telcal_skydip_sic
  !---------------------------------------------------------------------
  ! @ private
  !
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  ! Logical error flag
  ! Local
  real*8 :: airmas,elstep,ielev,tsky,dtsky
  integer :: imeas,iloop
  integer(kind=index_length) :: dims(4)
  !
  ! Define the structure
  call sic_defstructure('SKYDIP%FIT',.true.,error)
  if (error) return
  !
  ! Parameters
  call sic_def_dble('SKYDIP%FIT%FEFF',params(1),      1,nmeas,.true.,error)
  call sic_def_dble('SKYDIP%FIT%PWV', params(nmeas+1),0,1,    .true.,error)
  if (tloss_mode) then
    call sic_def_dble('SKYDIP%FIT%TLOSS',params(nmeas+2),0,1,.true.,error)
  endif
  if (error) return
  !
  ! Compute the fitted model
  elstep = pi/2./npmodel
  do imeas=1,nmeas
    do iloop=1,npmodel
      ielev = elstep*iloop
      call fsky(imeas,ielev,params,.false.,airmas,tsky,dtsky)
      fit_elev(iloop) = ielev
      fit_tsky(iloop,imeas) = tsky
    enddo
  enddo
  dims(1) = npmodel
  dims(2) = nmeas
  call sic_def_real('SKYDIP%FIT%ELEV',fit_elev,1,npmodel,.true.,error)
  call sic_def_real('SKYDIP%FIT%TSKY',fit_tsky,2,dims,   .true.,error)
  if (error) return
  !
  ! RMS
  ! call sic_def_dble('SKYDIP%FIT%CHI2',chi2tot,  0,1,    .true.,error)
  ! call sic_def_dble('SKYDIP%FIT%RMS', rmstot,   0,1,    .true.,error)
  ! if (error) return
  !
end subroutine telcal_skydip_results
!
subroutine telcal_skydip_plot()
  use telcal_interfaces, except_this=>telcal_skydip_plot
  use telcal_skydip_sic
  use phys_const
  !----------------------------------------------------------------------
  ! @ private
  ! SKYDIP Internal routine
  !  Plot the data points and the fitted model
  !----------------------------------------------------------------------
  ! Local
  real*4 :: vmin,vmax,vrange
  character(len=100) :: chain
  integer :: imeas,ipoint
  real*8 :: sky(msky,mmeas)   ! Unknown (???) (Trec mode)
  real*8 :: temi(msky,mmeas)  ! Temi (Feff mode)
  !
  ! Clear the current plot
  call gr_clea('')
  !
  ! Compute temperatures from counts (plotting purpose only)
  do imeas=1,nmeas
    do ipoint=1,sic_skydip(imeas)%nsky
      if (trec_mode) then
        sky(ipoint,imeas) =  &
          sic_skydip(imeas)%emiss(ipoint)%count - &
          sic_skydip(imeas)%hot(ipoint)%count
      else
        ! Eq.7 Memo I:
        ! Temi = ((Tload+Trec)*Cemi)/Cload - Trec
        temi(ipoint,imeas) =  &
          (sic_skydip(imeas)%hot(ipoint)%temp + sic_rec(imeas)%temp) *  &
          sic_skydip(imeas)%emiss(ipoint)%count /            &
          sic_skydip(imeas)%hot(ipoint)%count                &
          - sic_rec(imeas)%temp
      endif
    enddo  ! ipoint
  enddo  ! imeas
  !
  ! Limits
  vmin = 999.
  vmax = 0.
  do imeas=1,nmeas
    if (trec_mode) then
      vmin = min(vmin,minval(sky(1:sic_skydip(imeas)%nsky,imeas)))
      vmax = max(vmax,maxval(sky(1:sic_skydip(imeas)%nsky,imeas)))
    else
      vmin = min(vmin,minval(temi(1:sic_skydip(imeas)%nsky,imeas)))
      vmax = max(vmax,maxval(temi(1:sic_skydip(imeas)%nsky,imeas)))
    endif
  enddo
  vrange = vmax-vmin
  vmin = vmin-vrange/10.
  vmax = vmax+vrange/10.
  chain = 'LIMITS'
  write(chain(8:),*) 0.,90.,vmin,vmax
  ! write(*,'(A)') chain
  call gr_exec(chain)
  !
  ! Box (with elevation in degrees)
  chain = 'BOX'
  ! write(*,'(A)') chain
  call gr_exec(chain)
  !
  ! Labels
  chain = 'LABEL "Elevation (deg)" /X'
  ! write(*,'(A)') chain
  call gr_exec(chain)
  chain = 'LABEL "T\\Dsky\\U (K)" /Y'
  ! write(*,'(A)') chain
  call gr_exec(chain)
  !
  ! Limits again (with elevation in radians, its internal unit)
  chain = 'LIMITS'
  write(chain(8:),*) 0.,pi/2.,vmin,vmax
  ! write(*,'(A)') chain
  call gr_exec(chain)
  !
  ! Change marker
  chain = 'SET MARKER 4 1 * 45'
  ! write(*,'(A)') chain
  call gr_exec(chain)
  !
  do imeas=1,nmeas
    !
    ! call gr_pen_colo(imeas)
    write(chain,'(A,I3)') 'PEN /COLO ',imeas+1
    ! write(*,'(A)') chain
    call gr_exec(chain)
    !
    ! Plot data points
    if (trec_mode) then
      call gr8_marker(sic_skydip(imeas)%nsky,sic_skydip(imeas)%elev,sky(1,imeas),0.0,-1.0)
    else
      call gr8_marker(sic_skydip(imeas)%nsky,sic_skydip(imeas)%elev,temi(1,imeas),0.0,-1.0)
    endif
    !
    ! Plot the fitted model, computed in telcal_skydip_results()
    call gr4_connect(npmodel,fit_elev,fit_tsky(1,imeas),0.,-1.)
    !
  enddo
  !
  ! call gr_pen_defa()
  chain = 'PEN /DEF'
  ! write(*,'(A)') chain
  call gr_exec(chain)
  !
end subroutine telcal_skydip_plot
