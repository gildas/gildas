!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
program chopper_test
  use chopper_definitions
  !
  type(chopper_t) :: chopper
  logical :: error
  !
  error = .false.
  !
  call telcal_reallocate_chopper(2,chopper,error)
  if (error) stop
  !
  chopper%search%strict = .true.
  chopper%search%water  = chopper_water_elem
  chopper%search%atm    = .true.
  chopper%search%trec   = .true.
  chopper%search%tcal   = .true.
  chopper%search%tsys   = .true.
  !
  chopper%tel%alti = 2.552
  chopper%tel%elev = 0.5569708
  chopper%tel%tout = 279.1569 
  chopper%tel%tcab = 293.4844
  chopper%tel%pres = 754.2711
  !
  chopper%n = 2
  chopper%freqs(1)%s = 89.192245777485
  chopper%freqs(1)%i = 92.108162268213 
  chopper%freqs(2)%s = 230.0
  chopper%freqs(2)%i = 233.0
  !
  chopper%recs(1)%sbgr = 1.0e-02
  chopper%recs(1)%beff = 0.8970001
  chopper%recs(1)%feff = 0.8970001
  chopper%recs(1)%fout = 0.0
  chopper%recs(1)%fcab = 0.00
  !
  chopper%recs(2)%sbgr = 1.
  chopper%recs(2)%beff = 0.90
  chopper%recs(2)%feff = 0.90
  chopper%recs(2)%fout = 0.10
  chopper%recs(2)%fcab = 0.00
  !
  chopper%loads(1)%dark_count = 0.0
  chopper%loads(1)%sky_count  = 132.1918
  chopper%loads(1)%cold%count =  62.68372
  chopper%loads(1)%cold%temp  =  15.57297
  chopper%loads(1)%cold%ceff  =   1.0
  chopper%loads(1)%hot%count  = 369.1028
  chopper%loads(1)%hot%temp   = 293.6250
  chopper%loads(1)%hot%ceff   =   1.0
  !
  chopper%loads(2)%dark_count = 0.0
  chopper%loads(2)%sky_count  = 150
  chopper%loads(2)%cold%count =  77
  chopper%loads(2)%cold%temp  = 77.0
  chopper%loads(2)%cold%ceff  =  1.0
  chopper%loads(2)%hot%count  = 200
  chopper%loads(2)%hot%temp   = 293.0
  chopper%loads(2)%hot%ceff   =   1.0
  !
  call telcal_chopper(chopper,error)
  if (error) stop
  !
  print *,"Receiver 1: Real PdBI data"
  print *,"  Error:  ", chopper%errors(1)
  print *,"  Water:  ", chopper%atms(1)%h2omm, "(expected: 9.651008)"
  print *,"  Trec:   ", chopper%recs(1)%temp,  "(expected: 41.30774)"
  print *,"  Tcal%s: ", chopper%tcals(1)%s, "(expected: ?)"
  print *,"  Tcal%i: ", chopper%tcals(1)%i, "(expected: ?)"
  print *,"  Tsys%s: ", chopper%tsyss(1)%s, "(expected: 171.0220)"
  print *,"  Tsys%i: ", chopper%tsyss(1)%i, "(expected: 17175.93)"
  print *,"  Attenuated Tsys%s: ", chopper%atsyss(1)%s
  print *,"  Attenuated Tsys%i: ", chopper%atsyss(1)%i
  print *," "
  print *,"Receiver 2:"
  print *,"  Error: ", chopper%errors(2)
  print *,"  Water: ", chopper%atms(2)%h2omm
  print *,"  Trec:  ", chopper%recs(2)%temp
  print *,"  Tcal:  ", chopper%tcals(2)
  print *,"  Tsys:  ", chopper%tsyss(2)
  print *,"  Attenuated Tsys: ", chopper%atsyss(2)
  print *," "
  !
  call telcal_free_chopper(chopper,error)
  if (error) stop
  !
end program chopper_test
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
