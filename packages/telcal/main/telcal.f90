!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! TELCAL main program
!       This is the main program of telcal library. It should not be used 
!       by itself. It is just proposed as an help for the library debugging.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
program telcal
  !----------------------------------------------------------------------
  ! Main gtemplate program
  !----------------------------------------------------------------------
  external :: telcal_pack_set
  !
  call gmaster_run(telcal_pack_set)
  !
end program telcal
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
