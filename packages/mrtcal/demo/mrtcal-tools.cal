!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Administration
!
begin procedure mrtcal-def
  if .not.exist(mrtcal) then
     define structure mrtcal /global
     define character mrtcal%proj*12 /global
     define character mrtcal%sou*12 /global
  endif
  let mrtcal%proj 09054
  let mrtcal%sou controld
  msetup bookkeeping space 32 ! MB (to ensure that we will test data buffering)
  msetup calibration feedback element
  msetup calibration bandwidth 0
  msetup calibration tsys nearest
  msetup calibration off  nearest
  msetup calibration scan nearest
  msetup output spectra yes
  msetup output usersec yes
  msetup output integration scan
  msetup output chunk set
  msetup pipeline onerror stop
  msetup ! User feedback
  !
!  mset output usersection no
!  mset output calibration associated
  !
end procedure mrtcal-def
!
begin procedure mrtcal-init
  !
  sic mkdir ha
  sic mkdir red-mrtcal
  !
  symbol verbose "sic message class s+i"
  symbol quiet   "sic message class s-i"
  set format full
  set plot h
  set unit f f
  !
  @ mrtcal-def
end procedure mrtcal-init
!
begin procedure mrtcal-separator
  message r mrtcal-separator ""
end procedure mrtcal-separator
!
begin procedure mrtcal-project
  @ mrtcal-separator
  let mrtcal%proj &1 
  let mrtcal%sou  &2
  message r mrtcal-project "Project set to "'mrtcal%proj'
  message r mrtcal-project "Source  set to "'mrtcal%sou'
end procedure mrtcal-project
!
begin procedure mrtcal-pause
  !
  g\draw relo
  if (cursor_code.eq."E") then
     message w mrtcal-pause "Exit current demo"
     return base
  endif
  !
end procedure mrtcal-pause
!
begin procedure mrtcal-actions
  @ mrtcal-separator
  message r mrtcal-actions "MRTCAL tool actions:"
  message r mrtcal-actions "  @ mrtcal-project ID SOU    Set project ID (e.g., 09054) and source name (e.g., CONTROLD)"
  message r mrtcal-actions "  @ mrtcal-calibrate         Calibrate the project"
  message r mrtcal-actions "  @ mrtcal-toc               Table of Content of the calibrated class file"
  message r mrtcal-actions "  @ mrtcal-noise             Theoretical vs experimental noise comparison"
  message r mrtcal-actions "  @ mrtcal-loads             Plot load bandpass"
  message r mrtcal-actions "  @ mrtcal-temps             Plot Tsys, Tcal, and Trec bandpass"  
  message r mrtcal-actions "  @ mrtcal-spectra           Plot science spectra"  
  message r mrtcal-actions "  @ mrtcal-compare-with-mira Compare science spectra with MIRA result"  
  @ mrtcal-separator
end procedure mrtcal-actions
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Computations
!
begin procedure mrtcal-calibrate
  file out red-mrtcal/'mrtcal%proj' single /over
  index build data/'mrtcal%proj'
  mfind
  pipeline /show
end procedure mrtcal-calibrate
!
begin procedure mrtcal-toc
  @ mrtcal-separator
  file in red-mrtcal/'mrtcal%proj'
  find
  list /toc
end procedure mrtcal-toc
!
begin procedure mrtcal-noise
  set var user
  file in red-mrtcal/'mrtcal%proj'
  find /sou 'mrtcal%sou'
  @ mrtcal-separator
  quiet
  say "source" "line" "telescope" "Meas." "Theo." "Ratio (M/T)" /format a12 a12 a13 a9 a9 a12
  for ient 1 to found
     get next
     set mode x frequency-100 frequency+100
     set wind 0 0
     plot
     base 10 /plot
     say 'source' 'line' 'telescope' 'sigma' 'r%user%30m%noise' 'sigma/r%user%30m%noise' /format a12 a12 a13 1pg9.2 1pg9.2 f12.2
     @ mrtcal-pause
  next ient
  verbose
end procedure mrtcal-noise
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Plots
!
begin procedure mrtcal-loads-old
  file in red-mrtcal/'mrtcal%proj'
  find
  list /toc line tele
  for isetup 1 to toc%nsetup
     set mode all tot
     find  /sou calambient /line 'toc%setup[isetup,1]' /tele 'toc%setup[isetup,2]'
     stitch
     plot
     set mode x current
     set mode y -0.1*user_ymax user_ymax
     plot
     find /sou calsky /line 'toc%setup[isetup,1]' /tele 'toc%setup[isetup,2]'
     stitch
     pen 1
     spectrum
     find /sou calcold /line 'toc%setup[isetup,1]' /tele 'toc%setup[isetup,2]'
     stitch
     pen 2
     spectrum
     pen 0
     ha ha/'toc%setup[isetup,1]'"-"'toc%setup[isetup,2]'
     @ mrtcal-pause
  next isetup
end procedure mrtcal-loads-old
!
begin procedure mrtcal-temps-old
  file in red-mrtcal/'mrtcal%proj'
  find
  list /toc line tele
  for isetup 1 to toc%nsetup
     set mode all tot
     find  /sou tsys /line 'toc%setup[isetup,1]' /tele 'toc%setup[isetup,2]'
     stitch
     plot
     set mode x current
     set mode y -0.1*user_xmax user_xmax
     find /sou tcal /line 'toc%setup[isetup,1]' /tele 'toc%setup[isetup,2]'
     stitch
     pen 1
     spectrum
     find /sou trec /line 'toc%setup[isetup,1]' /tele 'toc%setup[isetup,2]'
     stitch
     pen 2
     spectrum
     pen 0
     ha ha/'toc%setup[isetup,1]'"-"'toc%setup[isetup,2]'
     @ mrtcal-pause
  next isetup
  set mode all tot
end procedure mrtcal-temps-old
!
begin procedure mrtcal-loads
  file in red-mrtcal/'mrtcal%proj'
  find /sou calsky
  list /toc line tele
  for isetup 1 to toc%nsetup
     set mode all tot
     find /sou calsky /line 'toc%setup[isetup,1]' /tele 'toc%setup[isetup,2]'
     get first ! *** JP here I would like to stitch...
     plot calambient
     set mode y -0.1*user_ymax user_ymax
     spectrum /pen 1
     spectrum calcold /pen 2
     ha ha/'toc%setup[isetup,1]'"-"'toc%setup[isetup,2]'
     @ mrtcal-pause
  next isetup
end procedure mrtcal-loads
!
begin procedure mrtcal-temps
  file in red-mrtcal/'mrtcal%proj'
  find
  list /toc line tele
  for isetup 1 to toc%nsetup
     set mode all tot
     find  /sou calsky /line 'toc%setup[isetup,1]' /tele 'toc%setup[isetup,2]'
     get first
     plot tsys
     set mode x current
     set mode y -0.1*user_xmax user_xmax
     set mode y -40 400
     plot tsys
     spectrum tcal /pen 1
     spectrum trec /pen 2
     ha ha/'toc%setup[isetup,1]'"-"'toc%setup[isetup,2]'
     @ mrtcal-pause
  next isetup
  set mode all tot
end procedure mrtcal-temps
!
begin procedure mrtcal-spectra
  file in red-mrtcal/'mrtcal%proj'
  find /sou 'mrtcal%sou'
  list /toc
  set mode all tot
  for isetup 1 to toc%nsetup
     find /sou 'toc%setup[isetup,1]' /line 'toc%setup[isetup,2]' /tele 'toc%setup[isetup,3]'
     stitch
     plot
     ha ha/'toc%setup[isetup,1]'"-"'toc%setup[isetup,2]'"-"'toc%setup[isetup,3]'
     @ mrtcal-pause
  next isetup
end procedure mrtcal-spectra
!
begin procedure mrtcal-compare
  file in data/'mrtcal%proj'/&1
!  if (pro%narg.eq.1) then
!     find
!  else
     find /source 'mrtcal%sou'
!  endif
  list /toc /var mytoc
  define double ymin ymax
!!$list /toc
!!$pause
  for isetup 1 to mytoc%nsetup
     set mode all tot
     file in red-mrtcal/'mrtcal%proj'
     find /sou 'mytoc%setup[isetup,1]' /line 'mytoc%setup[isetup,2]' /tele 'mytoc%setup[isetup,3]'
     if (found.ne.0) then
        stitch
        plot
        let ymin user_ymin
        let ymax user_ymax
        file in data/'mrtcal%proj'/&1
        find /sou 'mytoc%setup[isetup,1]' /line 'mytoc%setup[isetup,2]' /tele 'mytoc%setup[isetup,3]'
list /toc
!pause
        stitch
        plot
        set mode x tot
        set mode y min(ymin,user_ymin) max(ymax,user_ymax)
        clear
        box
        spectrum
        ! *** JP *** I retrieve the first one because many parameters of the header has been zeroed by the STITCH command!
        get f
        title
        file in red-mrtcal/'mrtcal%proj'
        find /sou 'mytoc%setup[isetup,1]' /line 'mytoc%setup[isetup,2]' /tele 'mytoc%setup[isetup,3]'
list /toc
!pause
        stitch
        pen 1
        spectrum
        ! *** JP *** I retrieve the first one because many parameters of the header has been zeroed by the STITCH command!
        get f
        title
        pen 0
        ha ha/'mytoc%setup[isetup,1]'"-"'mytoc%setup[isetup,2]'"-"'mytoc%setup[isetup,3]'
     else
        clear
        say ""
        say "Nothing found for "'mytoc%setup[isetup,1]'" "'mytoc%setup[isetup,2]'" "'mytoc%setup[isetup,3]'
        say ""
     endif
     @ mrtcal-pause
  next isetup
end procedure mrtcal-compare
!
begin procedure mrtcal-compare-with-mira
  @ mrtcal-compare mira
end procedure mrtcal-compare-with-mira
!
begin procedure mrtcal-compare-with-mrtcal
  @ mrtcal-compare mrtcal
end procedure mrtcal-compare-with-mrtcal
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! OTF processing
!
begin procedure mrtcal-otf-11128
  @ class-prefix 'mrtcal%proj'
  @ class-import 'mrtcal%proj' horsehead C3HP-CH3CN *f05 * * 89188.5247 5
  @ class-cover 1
  @ mrtcal-pause
  @ class-modify 89188.5247 10.70
  @ class-average 2
  @ mrtcal-pause
  @ class-base 9 13 1 ! Left velocity, Right velocity, Base order
  @ class-average 3
  @ class-grid 3
  @ class-clip 1e-5
  @ class-export 'mrtcal%proj' 3
end procedure mrtcal-otf-11128
!
begin procedure mrtcal-otf-12177
  @ class-prefix 'mrtcal%proj'
!  @ class-import 'mrtcal%proj' CIG96 CO21-CIG96 * * * 229240.500420 200
!  @ class-cover 1
!  @ mrtcal-pause
!  @ class-modify 230538 1680
  @ class-average 2
  @ mrtcal-pause
return
  @ class-base 1659.5 1776.0 1 ! Left velocity, Right velocity, Base order
  @ class-average 3
  @ class-grid 3
  @ class-clip 1e-5
  @ class-export 'mrtcal%proj' 3
end procedure mrtcal-otf-12177
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
begin procedure mrtcal-widget
  gui\panel "MRTCAL demonstration" gag_scratch:mrtcal-demo.hlp
  gui\menu "Project setup" /choices
     gui\button "@ mrtcal-project 09054 controld" "EMIR Tracked PSW"
     gui\button "@ mrtcal-project 09228 w3oh"     "EMIR Tracked WSW"
     gui\button "@ mrtcal-project 12142 chain-1"  "EMIR Tracked FSW"
     gui\button "@ mrtcal-project 11128 *"        "EMIR OTF PSW"
     gui\button "@ mrtcal-project 12177 *"        "HERA OTF PSW"
  gui\menu /close
  gui\menu "Calibration" /choices
     gui\button "@ mrtcal-calibrate" "Calibrate"
  gui\menu /close
  gui\menu "Visualization" /choices
     gui\button "@ mrtcal-spectra" "Spectra"
     gui\button "@ mrtcal-temps" "Temperatures"
     gui\button "@ mrtcal-loads" "Loads"
     gui\button "@ mrtcal-toc" "TOC"
  gui\menu /close
  gui\menu "Comparison" /choices
     gui\button "@ mrtcal-compare-with-mira"   "MIRA"
     gui\button "@ mrtcal-compare-with-mrtcal" "MRTCAL"
  gui\menu /close
  gui\menu "Noise" /choices
     gui\button "@ mrtcal-noise" "Noise"
  gui\menu /close
  gui\menu "Help" /choices
     gui\button "@ mrtcal-actions" "Actions Help"   
  gui\menu /close
  !
  say "Project setup"
  let mrtcal%proj 'mrtcal%proj' /prompt "Project name"
  let mrtcal%sou 'mrtcal%sou' /prompt "Source  name"
  gui\go
end procedure mrtcal-widget
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
@ mrtcal-init
!@ mrtcal-widget
@ mrtcal-actions
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
