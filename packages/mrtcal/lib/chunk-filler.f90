!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_chunksets_from_data(imbf,subs,databuf,error)
  use gbl_message
  use mrtcal_buffer_types
  use mrtcal_interfaces, except_this=>mrtcal_chunksets_from_data
  !---------------------------------------------------------------------
  ! @ private
  ! Extract all chunks from DATA.
  !---------------------------------------------------------------------
  type(imbfits_t), target, intent(in)    :: imbf
  type(imbfits_subscan_t), intent(in)    :: subs
  type(data_buffer_t),     intent(inout) :: databuf
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CHUNKSETS>FROM>DATA'
  integer(kind=4) :: itime,ipix,ntime,dtime,jtime,ktime,first,dfirst
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  ! Allocate the appropriate number of chunksets
  ntime = databuf%time%cur%n
  call reallocate_chunkset_3d(      &
       imbf%back%chunks%eclass%nequ,&  ! Nset
       databuf%imbf%npix,           &  ! Npix
       ntime,                       &  ! Ntime
       databuf%mrtc,                &
       error)
  if (error)  return
  !
  ! Absolute position of the first dump in the "compressed" columns:
  first = databuf%time%cur%first
  ! Absolute position of the first dump in the whole DATA column:
  dfirst = subs%backdata%table%backpoin%val(first)
  do itime=1,ntime                                    ! Position in the chunkset 3D array
     jtime = first + itime - 1                        ! Absolute position in the "compressed" columns
     ktime = subs%backdata%table%backpoin%val(jtime)  ! Absolute position in the whole DATA column
     dtime = ktime - dfirst + 1                       ! Position in the current DATA block
     do ipix=1,databuf%imbf%npix
        call mrtcal_chunksets_from_data_1time1pix(&
             imbf,                                &
             subs,                                &  ! The subscan
             databuf%imbf%val(:,ipix,dtime),      &  ! The data row to be split
             databuf%imbf%wei(:),                 &  ! The weight row to be split (always use the same)
             ipix,                                &
             jtime,                               &
             imbf%back%chunks,                    &  ! Chunks description
             databuf%mrtc%chunkset(:,ipix,itime), &  ! The chunksets row to be filled
             error)
        if (error)  return
     enddo
  enddo
  !
end subroutine mrtcal_chunksets_from_data
!
subroutine mrtcal_chunksets_from_data_1time1pix(imbf,subs,data,weight,  &
     ipix,itime,chunks,set,error)
  use gbl_message
  use imbfits_types
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_chunksets_from_data_1time1pix
  !---------------------------------------------------------------------
  ! @ private
  ! Gather the chunks by sets, for 1 time dump and 1 receiver pixel.
  !---------------------------------------------------------------------
  type(imbfits_t),             intent(in)    :: imbf
  type(imbfits_subscan_t),     intent(in)    :: subs
  real(kind=4),                intent(inout) :: data(:)    ! Input data values
  real(kind=4),                intent(in)    :: weight(:)  ! Input data weights
  integer(kind=4),             intent(in)    :: ipix
  integer(kind=4),             intent(in)    :: itime
  type(imbfits_back_chunks_t), intent(in)    :: chunks     ! Chunks description
  type(chunkset_t),            intent(inout) :: set(:)     ! Output chunksets
  logical,                     intent(inout) :: error
  ! Local
  integer(kind=4) :: filled(chunks%eclass%nequ)  ! Automatic array
  integer(kind=4) :: iselect,ichunk,iequiv
  character(len=message_length) :: mess
  character(len=*), parameter :: rname='CHUNKSETS>FROM>DATA>1TIME1PIX'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  do iequiv=1,chunks%eclass%nequ
     call reallocate_chunkset(chunks%eclass%cnt(iequiv),set(iequiv),error)
     if (error)  return
  enddo
  !
  filled(1:chunks%eclass%nequ) = 0
  do iselect=1,chunks%eclass%nval
     ichunk = chunks%used(iselect)
     ! NB: ichunk must be the absolute chunk position in the BACKEND table.
     iequiv = chunks%eclass%bak(iselect)
     filled(iequiv) = filled(iequiv)+1
     if (filled(iequiv).gt.chunks%eclass%cnt(iequiv)) then
        ! Seems that the equivalence classes are broken...
        write (mess,'(A,I0)')  &
             'Lost my mind: too many chunks have PART value ',imbf%back%table%part%val(ichunk)
        call mrtcal_message(seve%e,rname,mess)
        error = .true.
        exit
     endif
     !
     call mrtcal_chunksets_from_data_1time1pix_1chunk(subs,data,weight,  &
       ipix,itime,ichunk,imbf%primary,imbf%scan,imbf%front,imbf%back,    &
       imbf%derot,set(iequiv)%chunks(filled(iequiv)),error)
     if (error)  exit
     !
  enddo
  !
end subroutine mrtcal_chunksets_from_data_1time1pix
!
subroutine mrtcal_chunksets_from_data_1time1pix_1chunk(subs,data,weight,  &
  ipix,itime,ichunk,primary,scan,front,back,derot,chunk,error)
  use gbl_message
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_chunksets_from_data_1time1pix_1chunk
  use mrtcal_calib_types
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Fill the 'chunk' instance with correct values and data, from given
  ! IMBFITS tables.
  !---------------------------------------------------------------------
  type(imbfits_subscan_t), intent(in)    :: subs
  real(kind=4),            intent(inout) :: data(:)    ! Input data values
  real(kind=4),            intent(in)    :: weight(:)  ! Input data weights
  integer(kind=4),         intent(in)    :: ipix
  integer(kind=4),         intent(in)    :: itime
  integer(kind=4),         intent(in)    :: ichunk     ! Chunk position in the backend table
  type(imbfits_primary_t), intent(in)    :: primary
  type(imbfits_scan_t),    intent(in)    :: scan
  type(imbfits_front_t),   intent(in)    :: front
  type(imbfits_back_t),    intent(in)    :: back
  type(imbfits_derot_t),   intent(in)    :: derot
  type(chunk_t),           intent(inout) :: chunk      ! The chunk to be filled
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CHUNKSETS>FROM>DATA>1TIME1PIX>1CHUNK'
  integer(kind=4) :: pixel,obstype
  real(kind=4) :: longoff,latoff,azimuth,elevation
  real(kind=8) :: lst
  !
  chunk%frontend = back%table%frontend%val(ichunk)
  chunk%ifront = back%table%ifront%val(ichunk)
  pixel = back%table%pixel%val(ichunk)
  obstype = mrtindex_obstype_decode(primary%obstype%val,error)
  if (error)  return
  !
  ! Get elements which are needed by several sections
  chunk%id = back%table%part%val(ichunk)
  call mrtcal_chunk_mjd_from_data(subs%backdata,itime,chunk%mjd,error)
  if (error)  return
  call mrtcal_interp_coord_from_antslow(subs%antslow,subs%onsky,chunk%mjd,  &
    longoff,latoff,azimuth,elevation,lst,error)
  if (error) return
  !
  ! Now fill the chunk data and sections
  !
  call mrtcal_chunk_data_from_data(data,weight,back,ichunk,chunk,error)
  if (error)  return
  !
  ! Spectroscopic section (also filled for continuum e.g. line name)
  call mrtcal_chunk_spe_from_data(subs%antslow,front,chunk%ifront,back,ichunk,  &
    chunk%spe,error)
  if (error)  return
  !
  call mrtcal_chunk_cal_from_data(scan,front,chunk%ifront,chunk%cal,error)
  if (error)  return
  !
  call mrtcal_chunk_gen_from_data(primary,obstype,scan,back,ichunk,subs%backdata,  &
      itime,chunk%mjd,azimuth,elevation,lst,chunk%frontend,chunk%gen,error)
  if (error)  return
  !
  call mrtcal_chunk_pos_from_data(primary,scan,front,derot,chunk%ifront,  &
    pixel,subs,chunk%mjd,longoff,latoff,chunk%pos,error)
  if (error)  return
  !
  ! Continuum section (after the position section)
  if (obstype.eq.obstype_pointing) then
    call mrtcal_chunk_con_from_data(scan,subs%antslow,chunk%pos%lamof,  &
      front,chunk%ifront,  &
      back,ichunk,subs%backdata,itime,chunk%mjd,chunk%con,error)
    if (error)  return
  endif
  !
  call mrtcal_chunk_swi_from_data(chunk%gen%time,chunk%swi,error)
  if (error)  return
  !
  call mrtcal_chunk_res_from_data(chunk%gen%teles,chunk%spe%restf,chunk%res,error)
  if (error)  return
  !
  call mrtcal_chunk_user_from_data(primary,chunk%user,error)
  if (error)  return
  !
end subroutine mrtcal_chunksets_from_data_1time1pix_1chunk
!
subroutine mrtcal_chunk_data_from_data(dataval,datawei,backe,ichunk,  &
     chunk,error)
  use gbl_message
  use imbfits_types
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_chunk_data_from_data
  !---------------------------------------------------------------------
  ! @ private
  ! Extract one chunk from DATA
  !---------------------------------------------------------------------
  real(kind=4), target, intent(inout) :: dataval(:)  ! inout because of sign flip
  real(kind=4), target, intent(in)    :: datawei(:)
  type(imbfits_back_t), intent(in)    :: backe
  integer(kind=4),      intent(in)    :: ichunk
  type(chunk_t),        intent(inout) :: chunk
  logical,              intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CHUNK>FROM>DATA>DATA'
  integer(kind=4) :: fchan,nchan
  character(len=message_length) :: mess
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  ! Data. NB: this assumes that the 'refchan' is always the first in the
  ! 'raw' chunk (a raw chunk is a chunk before dropping edge channels)
  nchan = backe%table%used%val(ichunk)
  fchan = backe%table%refchan%val(ichunk) + backe%table%dropped%val(ichunk)
  if ((backe%table%refchan%val(ichunk)+backe%table%chans%val(ichunk)-1).gt.size(dataval)) then
     write(mess,'(4(A,I0))') 'Unexpected data length: chunk #',ichunk,  &
       ' has REFCHAN=',backe%table%refchan%val(ichunk),' and CHANS=',  &
       backe%table%chans%val(ichunk),' while size of DATAVAL is ',size(dataval)
     call mrtcal_message(seve%e,rname,mess)
     error = .true.
     return
  endif
  !
  call reassociate_chunk(dataval,datawei,fchan,nchan,chunk,error)
  if (error)  return
  !
  ! Sign of data must be flipped under specific cases
  if (backe%table%dataflip%val(ichunk)) then
    chunk%data1 = -chunk%data1  ! Should be done efficiently (i.e. swap only the sign bit)
  endif
  !
end subroutine mrtcal_chunk_data_from_data
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_chunk_cal_from_data(scan,front,ifront,cal,error)
  use gbl_message
  use phys_const
  use class_types
  use imbfits_types
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_chunk_cal_from_data
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(imbfits_scan_t),    intent(in)    :: scan
  type(imbfits_front_t),   intent(in)    :: front
  integer(kind=4),         intent(in)    :: ifront
  type(class_calib_t),     intent(out)   :: cal
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CHUNK>FROM>DATA>CAL'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  ! Parameters which are just copied from the imbfits
  cal%alti = scan%head%siteelev%val  ! [m]
  cal%lcalof = 0.0
  cal%bcalof = 0.0
  cal%geolong = scan%head%sitelong%val*rad_per_deg
  cal%geolat  = scan%head%sitelat%val*rad_per_deg
  !
  cal%beeff = front%table%beameff%val(ifront)
  cal%foeff = front%table%etafss%val(ifront)
  cal%gaini = front%table%gainimag%val(ifront)
  cal%pamb  = scan%head%pressure%val
  cal%tamb  = scan%head%tambient%val+273.15
  cal%tchop = front%table%thot%val(ifront)
  cal%tcold = front%table%tcold%val(ifront)
  !
  ! Parameters which are just initialized
  cal%h2omm = 0.0
  cal%tatms = 0.0
  cal%tatmi = 0.0
  cal%taus  = 0.0
  cal%taui  = 0.0
  cal%trec  = 0.0
  cal%atfac = 0.0
  !
end subroutine mrtcal_chunk_cal_from_data
!
subroutine mrtcal_chunk_spe_from_data(antslow,front,ifront,back,ichunk,spe,error)
  use gbl_message
  use gbl_constant
  use phys_const
  use class_types
  use imbfits_types
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_chunk_spe_from_data
  !---------------------------------------------------------------------
  ! @ private
  ! It is unclear whether at this stage we want the freq/vel axis
  ! to be defined in the source or the observatory frame *** JP
  ! Right now it is defined in the source frame
  !
  ! Order matters here!...
  !
  !---------------------------------------------------------------------
  type(imbfits_antslow_t), intent(in)    :: antslow
  type(imbfits_front_t),   intent(in)    :: front
  integer(kind=4),         intent(in)    :: ifront
  type(imbfits_back_t),    intent(in)    :: back
  integer(kind=4),         intent(in)    :: ichunk
  type(class_spectro_t),   intent(out)   :: spe
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CHUNK>FROM>DATA>SPE'
  logical :: lsb_tuned,lsb_obser
  character(len=char1d_length) :: recname
  character(len=1)  :: tuned_sideb
  character(len=1)  :: obser_sideb
  real(kind=8) :: tuned_restf,tuned_image,sbsep,ioff,ifcenter
  real(kind=8) :: tuned_sbsign,obser_sbsign
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  ! Parameters which are just copied from the imbfits
  spe%vconv = vconv_30m  ! 30M uses its own velocity convention
  spe%bad = bad  ! *** JP This should be initialized in the class type definition
  spe%fres = back%table%spacing%val(ichunk)
  spe%voff = front%head%velosys%val
  spe%nchan = back%table%used%val(ichunk)  ! ZZZ should be chunk%ndata
  tuned_restf = front%table%restfreq%val(ifront)*1000d0 ! in MHz
  tuned_sideb = front%table%sideband%val(ifront)(1:1)
  call sic_upper(tuned_sideb)
  recname = front%table%recname%val(ifront)
  call sic_upper(recname)
  if (recname(1:1).eq."E") then
     ! EMIR => 2SB
     obser_sideb = back%table%band%val(ichunk)(4:4)
  else if (recname(1:4).eq."HERA") then
     ! HERA => SSB
     obser_sideb = tuned_sideb
  else if (recname.eq."HOLOGRAPHY") then
     ! Holography
     obser_sideb = tuned_sideb
  else
     call mrtcal_message(seve%e,rname,'Unsupported RECNAME '//recname)
     error = .true.
     return
  endif
  call sic_upper(obser_sideb)
  sbsep = front%table%sbsep%val(ifront)/1d6 ! in MHz
  ifcenter = front%table%ifcenter%val(ifront)*1d3 ! in MHz
  !
  ! Parameters which are translated from imbfits
  ! Unclear whether we need both the logical and the sign. *** JP
  spe%line = front%table%linename%val(ifront)
  if (tuned_sideb.eq.'L') then
     lsb_tuned = .true.
     tuned_sbsign = -1.0
  else if (tuned_sideb.eq.'U') then
     lsb_tuned = .false.
     tuned_sbsign = +1.0
  else
     call mrtcal_message(seve%e,rname,'Unknown tuned sideband '//tuned_sideb)
     error = .true.
     return
  endif
  if (obser_sideb.eq.'L') then
     lsb_obser = .true.
     obser_sbsign = -1.0
  else if (obser_sideb.eq.'U') then
     lsb_obser = .false.
     obser_sbsign = +1.0
  else
     call mrtcal_message(seve%e,rname,'Unknown obser sideband '//obser_sideb)
     error = .true.
     return
  endif
  select case (front%head%specsys%val)
  case ('LSR')
     spe%vtype = vel_lsr
  case ('hel') ! *** JP
     spe%vtype = vel_hel
  case ('obs') ! *** JP
     spe%vtype = vel_obs
  case ('ear') ! *** JP
     spe%vtype = vel_ear
  case default
     spe%vtype = vel_unk
  end select
  !
  ! Parameters which are derived
  if (front%head%veloconv%val.eq.'optical') then
     spe%doppler = -(spe%voff+antslow%head%obsvelrf%val)/clight_kms
  else if (front%head%veloconv%val.eq.'radio') then
     spe%doppler = -antslow%head%dopplerc%val
  else
     call mrtcal_message(seve%e,rname,'Unknown velocity convention '//front%head%veloconv%val)
     error = .true.
     return
  endif
  ! Signal and image frequencies
  spe%rchan = 1.d0-back%table%dropped%val(ichunk)
  tuned_image = tuned_restf-tuned_sbsign*sbsep/(1d0+spe%doppler)
  if (spe%fres.ne.0.d0) then
     ! Old code was going through the frequency axis in the source frame,
     ! implying a division and a multiplication and by (1d0+spe%doppler).
     ! Now it does the computation directly in the observatory frame.
     ! This avoids rounding errors.
     ioff = (back%table%reffreq%val(ichunk)-obser_sbsign*ifcenter)/spe%fres
  else
     call mrtcal_message(seve%e,rname,'Zero valued channel spacing')
     error = .true.
     return
  endif
  ! *** JP some test needed here.
  if (lsb_obser.eqv.lsb_tuned) then
     ! Tuned = Observed sideband
     spe%restf = tuned_restf
     spe%image = tuned_image
     spe%rchan = spe%rchan-ioff
  else
     ! Tuned and observed sideband are opposed
     spe%restf = tuned_image
     spe%image = tuned_restf
     spe%rchan = spe%rchan-ioff
  endif
  if (spe%restf.ne.0.d0) then
     spe%vres = -clight_kms*spe%fres/spe%restf
  else
     call mrtcal_message(seve%e,rname,'Zero valued rest frequency')
     error = .true.
     return
  endif
  !
end subroutine mrtcal_chunk_spe_from_data
!
subroutine mrtcal_chunk_con_from_data(scan,antslow,lamof,front,ifront,  &
  back,ichunk,backdata,itime,mjd,con,error)
  use phys_const
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_chunk_con_from_data
  !---------------------------------------------------------------------
  ! @ private
  ! Write the continuum drift section
  !---------------------------------------------------------------------
  type(imbfits_scan_t),     intent(in)    :: scan
  type(imbfits_antslow_t),  intent(in)    :: antslow
  real(kind=4),             intent(in)    :: lamof
  type(imbfits_front_t),    intent(in)    :: front
  integer(kind=4),          intent(in)    :: ifront
  type(imbfits_back_t),     intent(in)    :: back
  integer(kind=4),          intent(in)    :: ichunk
  type(imbfits_backdata_t), intent(in)    :: backdata
  integer(kind=4),          intent(in)    :: itime
  real(kind=8),             intent(in)    :: mjd
  type(class_drift_t),      intent(out)   :: con
  logical,                  intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CHUNK>FROM>DATA>CON'
  type(class_spectro_t) :: spe
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  con%width = back%table%spacing%val(ichunk)  ! [MHz] Bandwidth
  con%npoin = 1
  con%rpoin = 1.0  ! [ ] Single point drift: reference is middle of the point
  !
  ! Times ZZZ which unit is expected???
  con%tref  = mjd  ! [day] Time at rpoin=1.0 <=> middle of the integration
  con%tres  = backdata%table%integtim%val(itime)  ! [s] When dealing with a single point drift,
                                                  ! use integration time as pseudo-resolution
  !
  ! Single-point drift: arbitrarily set position angle along X
  con%apos  = 0.0              ! [rad]
  con%aref  = lamof            ! [rad]
  con%ares  = 1.0*rad_per_sec  ! Angular resolution of the drift is defined by looking at 2 consecutive points => leave unknown (1)
  con%bad   = bad              ! *** JP This should be initialized in the class type definition
  con%ctype = mrtcal_chunk_system_from_data(scan,error)  ! Coordinate system
  if (error)  return
  !
  ! Frequencies. Due to the complexity, compute a whole spectro section
  call mrtcal_chunk_spe_from_data(antslow,front,ifront,back,ichunk,spe,error)
  if (error)  return
  ! ZZZ These are tuned frequencies. Should we use middle of the bandwidth instead?
  con%freq  = spe%restf
  con%cimag = spe%image
  !
  ! Collimation errors
  con%colla = scan%head%p2cor%val  ! [rad]
  con%colle = scan%head%p7cor%val  ! [rad]
  !
end subroutine mrtcal_chunk_con_from_data
!
subroutine mrtcal_chunk_gen_from_data(primary,obstype,scan,back,ichunk,backdata,  &
  itime,mjd,azimuth,elevation,lst,frontend,gen,error)
  use gbl_message
  use gbl_constant
  use phys_const
  use class_types
  use imbfits_types
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_chunk_gen_from_data
  use mrtcal_calib_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(imbfits_primary_t),  intent(in)    :: primary
  integer(kind=4),          intent(in)    :: obstype
  type(imbfits_scan_t),     intent(in)    :: scan
  type(imbfits_back_t),     intent(in)    :: back
  integer(kind=4),          intent(in)    :: ichunk
  type(imbfits_backdata_t), intent(in)    :: backdata
  integer(kind=4),          intent(in)    :: itime
  real(kind=8),             intent(in)    :: mjd
  real(kind=4),             intent(in)    :: azimuth
  real(kind=4),             intent(in)    :: elevation
  real(kind=8),             intent(in)    :: lst
  character(len=*),         intent(in)    :: frontend
  type(class_general_t),    intent(out)   :: gen
  logical,                  intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CHUNK>GEN>FROM>DATA'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_chunk_teles_from_data(gen%teles,error)
  if (error)  return
  gen%num = 0
  gen%ver = 0
  select case (obstype)
  case (obstype_pointing,obstype_focus,obstype_tip)
    gen%kind = kind_cont
  case default
    gen%kind = kind_spec
  end select
  gen%qual = qual_unknown
  gen%scan = scan%head%scannum%val
  gen%subscan = backdata%head%obsnum%val
  ! gen%dred is automatically set by class
  call gag_mjd2gagut(mjd,gen%dobs,gen%ut,error)
  gen%time = backdata%table%integtim%val(itime)
  gen%st = lst
  gen%az = azimuth
  gen%el = elevation
  ! Parallactic angle from AntSlow table is not reliable. Compute it from
  ! azimuth-elevation:
  call mrtcal_chunk_parang_from_gen(gen,error)
  !
  ! The following values are computed later on, just initialize them here
  ! *** JP Not sure this is useful as some of these quantities may be intent(out)
  ! *** JP later on...
  gen%tau = bad ! Set after calibration
  gen%tsys = bad ! Set after calibration
  !
contains
  !
  subroutine mrtcal_chunk_teles_from_data(teles,error)
    character(len=*), intent(out)   :: teles
    logical,          intent(inout) :: error
    ! Local
    logical :: ishera
    character(len=12) :: mybackend
    !
    mybackend = primary%instrume%val
    call sic_upper(mybackend)
    select case (mybackend)
    case ('4MHZ')
      write(mybackend,'(a3,i1.1)') '-4M',back%table%part%val(ichunk)
    case ('FTS')
      write(mybackend,'(a2,i2.2)') '-F',back%table%part%val(ichunk)
    case ('VESPA')
      write(mybackend,'(a2,i2.2)') '-V',back%table%part%val(ichunk)
    case ('WILMA')
      write(mybackend,'(a2,i2.2)') '-W',back%table%part%val(ichunk)
    case ('BBC')
      write(mybackend,'(a2,i2.2)') '-B',back%table%part%val(ichunk)
    case ('NBC')
      write(mybackend,'(a2,i2.2)') '-N',back%table%part%val(ichunk)
    case ('CONTINUUM')
      write(mybackend,'(a2,i2.2)') '-C',back%table%part%val(ichunk)
    case ('HOLO')
      write(mybackend,'(a2,i2.2)') '-H',back%table%part%val(ichunk)
    case default
      call mrtcal_message(seve%e,rname,'Unknown backend: '//mybackend)
      error = .true.
      return
    end select
    !
    ishera = back%table%receiver%val(ichunk)(1:4).eq.'HERA'
    if (ishera) then
      teles = '30M'//trim(mybackend)//'-'//trim(frontend)
    else
      teles = '30M'//trim(frontend)//trim(mybackend)
    endif
    !
  end subroutine mrtcal_chunk_teles_from_data
  !
end subroutine mrtcal_chunk_gen_from_data
!
subroutine mrtcal_chunk_mjd_from_data(backdata,itime,mjd,error)
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ public for mrtholo
  ! Set the MJD at the middle of the integration time. Caution: this
  ! value is used later for chunk position!
  !
  !  Tstamped:
  !    0.0 = MJD given at the beginning of the integration time
  !    0.5 = MJD given at the middle    of the integration time ???
  !    1.0 = MJD given at the end       of the integration time
  !---------------------------------------------------------------------
  type(imbfits_backdata_t), intent(in)    :: backdata  ! Backend Data table
  integer(kind=4),          intent(in)    :: itime     ! Chunk position in table
  real(kind=8),             intent(out)   :: mjd       ! Resulting MJD
  logical,                  intent(inout) :: error     ! Logical error flag
  !
  mjd = backdata%table%mjd%val(itime) +  &
        (0.5d0-backdata%head%tstamped%val) *  &
        backdata%table%integtim%val(itime)/86400.d0
  !
end subroutine mrtcal_chunk_mjd_from_data
!
subroutine mrtcal_chunk_parang_from_gen(gen,error)
  use class_types
  !---------------------------------------------------------------------
  ! @ private
  ! Compute the parallactic angle from/to general section, assuming
  ! 30m latitude and azimuth convention.
  !---------------------------------------------------------------------
  type(class_general_t), intent(inout) :: gen
  logical,               intent(inout) :: error
  ! Local
  real(kind=8), parameter :: latitude=0.6469658708531066d0  ! [rad] 30m latitude 37:04:06.29
  !
  call gwcs_azel2pa(latitude,real(gen%az,kind=8),real(gen%el,kind=8),gen%parang)
end subroutine mrtcal_chunk_parang_from_gen
!
subroutine mrtcal_chunk_pos_from_data(primary,scan,front,derot,ifront,  &
  pixel,subs,mjd,longoff,latoff,pos,error)
  use gbl_message
  use gbl_constant
  use phys_const
  use class_types
  use imbfits_types
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_chunk_pos_from_data
  use mrtcal_calib_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(imbfits_primary_t), intent(in)    :: primary
  type(imbfits_scan_t),    intent(in)    :: scan
  type(imbfits_front_t),   intent(in)    :: front
  type(imbfits_derot_t),   intent(in)    :: derot
  integer(kind=4),         intent(in)    :: ifront
  integer(kind=4),         intent(in)    :: pixel
  type(imbfits_subscan_t), intent(in)    :: subs
  real(kind=8),            intent(in)    :: mjd
  real(kind=4),            intent(in)    :: longoff  ! [rad] Antenna X offset (interpolated)
  real(kind=4),            intent(in)    :: latoff   ! [rad] Antenna Y offset (interpolated)
  type(class_position_t),  intent(out)   :: pos
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CHUNK>POS>FROM>DATA'
  real(kind=4) :: xoffset,yoffset
  real(kind=8) :: dewang
  integer(kind=4) :: iprojscan,iprojant
  character(len=32) :: cprojscan,cprojant
  character(len=switchmode_length) :: swmod
  character(len=message_length) :: mess
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  pos%sourc = scan%head%object%val
  call sic_upper(pos%sourc)
  pos%system = mrtcal_chunk_system_from_data(scan,error)
  if (error)  return
  pos%equinox = scan%head%equinox%val
  pos%lam = scan%head%longobj%val*rad_per_deg
  pos%bet = scan%head%latobj%val*rad_per_deg
  !
  ! Offsets. There are 3 type of offsets in IMB-FITS:
  ! - one in the SCAN table,
  ! - one in the subscan ANTENNA traces,
  ! - one in the DEROT table (pixel offset e.g. for HERA)
  !
  if (.not.subs%onsky) then
    pos%proj = p_unknown
    pos%projang = 0.d0
    pos%lamof = 0.
    pos%betof = 0.
    return
  endif
  !
  ! 1) Scan offsets
  call mrtcal_get_offset_from_scan(scan,iprojscan,cprojscan,xoffset,yoffset,error)
  if (error)  return
  !
  ! 2) Antenna offsets
  ! longoff = already done (interpolated earlier)
  ! latoff  = already done (interpolated earlier)
  if (subs%antslow%head%systemof%val.eq.'projection') then
    ! 'projection' stands for radio projection in the current system
    iprojant = p_radio
    cprojant = "'projection' (radio)"
  else  ! e.g. horizontalTrue
    iprojant = p_unknown
    cprojant = "'"//trim(subs%antslow%head%systemof%val)//"' (unknown)"
  endif
  !
  ! 1+2) Add scan offsets and antenna offsets
  swmod = scan%head%swtchmod%val
  call sic_upper(swmod)
  if (swmod.eq.mrtindex_swmode_imbfits(switchmode_wob)) then
    ! If Wobbler Switching, longoff and latoff are the primary dish
    ! position. This is not what we want. Depending on the phase, we point
    ! either on OFF or ON. Always assume (0,0) antenna offsets for ON and
    ! OFF; the switching position offsets will be saved in the switching
    ! section.
    pos%proj = iprojscan
    pos%projang = 0.d0
    pos%lamof = xoffset ! xoffset + 0
    pos%betof = yoffset ! yoffset + 0
    !
  elseif (iprojscan.eq.iprojant) then
    ! Same projection: direct addition of offsets
    pos%proj = iprojscan
    pos%projang = 0.d0
    pos%lamof = xoffset + longoff
    pos%betof = yoffset + latoff
    !
  elseif (xoffset.eq.0. .and. yoffset.eq.0.) then
    ! Tolerate (0,0) scan offset in an unknown projection
    pos%proj = iprojant
    pos%projang = 0.d0
    pos%lamof = longoff
    pos%betof = latoff
    !
  else
    call mrtcal_message(seve%e,rname,  &
      'Scan and Antslow have non-zero offsets in different projection systems:')
    write(mess,'(A,2(F10.5,2X),A)') '  Scan   : ',xoffset,yoffset,cprojscan
    call mrtcal_message(seve%e,rname,mess)
    write(mess,'(A,2(F10.5,2X),A)') '  Antslow: ',longoff,latoff,cprojant
    call mrtcal_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  ! 3) Pixel offsets (multi-beam), if relevant
  if (derot%head%desc%status.eq.code_imbfits_done) then
    ! Subscan has a derotator HDU defined
    call mrtcal_get_dewang_from_derot(front,derot,mjd,dewang,error)
    if (error)  return
    call mrtcal_pixel_offset(pos,front%table%recname%val(ifront),pixel,dewang,error)
    if (error)  return
  endif
  !
end subroutine mrtcal_chunk_pos_from_data
!
function mrtcal_chunk_system_from_data(scan,error)
  use gbl_format
  use gbl_message
  use mrtcal_interfaces, except_this=>mrtcal_chunk_system_from_data
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  ! Return the coordinate system code from scan HDU
  !---------------------------------------------------------------------
  integer(kind=4) :: mrtcal_chunk_system_from_data
  type(imbfits_scan_t), intent(in)    :: scan
  logical,              intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CHUNK>SYSTEM>FROM>DATA'
  !
  ! Note that we do not check for the suffix (e.g. -SFL) as it is not
  ! used (it does not apply to the offsets in the antenna table). See
  ! mrtcal documentation for details.
  if (scan%head%ctype1%val(1:2).eq.'RA' .and.  &
      scan%head%ctype2%val(1:3).eq.'DEC') then
     mrtcal_chunk_system_from_data = type_eq
     !
  elseif (scan%head%ctype1%val(1:4).eq.'GLON' .and.  &
          scan%head%ctype2%val(1:4).eq.'GLAT') then
     mrtcal_chunk_system_from_data = type_ga
     !
  else
    call mrtcal_message(seve%e,rname,'Kind of coordinates not understood: '//  &
      trim(scan%head%ctype1%val)//' '//trim(scan%head%ctype2%val))
    mrtcal_chunk_system_from_data = type_un
    error = .true.
    return
  endif
  !
end function mrtcal_chunk_system_from_data
!
subroutine mrtcal_chunk_swi_from_data(time,swi,error)
  use gkernel_interfaces
  use gbl_message
  use gbl_constant
  use phys_const
  use class_types
  use imbfits_types
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_chunk_swi_from_data
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  real(kind=4),         intent(in)    :: time
  type(class_switch_t), intent(out)   :: swi
  logical,              intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CHUNK>SWI>FROM>DATA'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  swi%swmod = mod_unk
  swi%nphas = 1       ! Here we only have read one phase, not a complete cycle
  swi%poids(1) = 0    ! This is not yet known, but 0 is nicer than -1000
  swi%duree(1) = time
  swi%decal(:)  = 0   ! This is not yet known, but 0 is nicer than -1000
  swi%ldecal(:) = 0   ! This is not yet known, but 0 is nicer than -1000
  swi%bdecal(:) = 0   ! This is not yet known, but 0 is nicer than -1000
  !
end subroutine mrtcal_chunk_swi_from_data
!
subroutine mrtcal_chunk_res_from_data(teles,restf,res,error)
  use gbl_message
  use class_types
  use mrtcal_interfaces, except_this=>mrtcal_chunk_res_from_data
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: teles  !
  real(kind=8),      intent(in)    :: restf  ! [MHz]
  type(class_res_t), intent(out)   :: res    !
  logical,           intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='CHUNK>RES>FROM>DATA'
  logical :: found
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call my_get_beam(teles,restf,found,res%major,error)
  if (.not.found)  error = .true.  ! Could also be a warning and deactivate the section
  if (error) then
    call mrtcal_message(seve%e,rname,'Could not compute beam size for telescope '//teles)
    return
  endif
  !
  res%minor = res%major
  res%posang = 0.
  !
end subroutine mrtcal_chunk_res_from_data
!
subroutine mrtcal_get_offset_from_scan(scan,projcode,projname,xoffset,yoffset,error)
  use gbl_constant
  use gbl_message
  use imbfits_types
  use mrtcal_interfaces, except_this=>mrtcal_get_offset_from_scan
  !---------------------------------------------------------------------
  ! @ private
  !  Set the observed scan offsets (there can be additional per-subscan
  ! offsets that will be added later on).
  !---------------------------------------------------------------------
  type(imbfits_scan_t), intent(in)    :: scan      !
  integer(kind=4),      intent(out)   :: projcode  ! [code] Projection kind for these offsets
  character(len=*),     intent(out)   :: projname  ! [char] Projection kind for these offsets
  real(kind=4),         intent(out)   :: xoffset   ! [rad]
  real(kind=4),         intent(out)   :: yoffset   ! [rad]
  logical,              intent(inout) :: error     !
  ! Local
  character(len=*), parameter :: rname='GET>OFFSET>FROM>SCAN'
  integer(kind=4) :: iproj,nproj,jprojection,jnasmyth,jother
  character(len=message_length) :: mess
  !
  jprojection = 0
  jnasmyth = 0
  nproj = scan%head%desc%naxis2%val
  do iproj=1,nproj
     if (scan%table%sysoff%val(iproj).eq.'projection') then
        jprojection = iproj
        exit
     elseif (scan%table%sysoff%val(iproj).eq.'Nasmyth') then
        jnasmyth = iproj
     endif
  enddo ! iproj
  !
  if (jprojection.ne.0) then
    projcode = p_radio  ! 'projection' stands for radio projection in the current system
    projname = "'projection' (radio)"
    xoffset = scan%table%xoffset%val(jprojection)  ! [rad]
    yoffset = scan%table%yoffset%val(jprojection)  ! [rad]
    return
  endif
  !
  ! Error recovery: 'projection' not found. Tolerate other systems as long as
  ! offsets are 0.0
  if (nproj.gt.2) then
    ! Code below supports only 2 rows
    call mrtcal_message(seve%e,rname,'Offset system not supported')
    write(mess,'(10(A,1X))')  'SYSOFF is:',scan%table%sysoff%val(1:nproj)
    call mrtcal_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  if (jnasmyth.eq.1) then
    jother = 2
  elseif (jnasmyth.eq.2) then
    jother = 1
  else
    call mrtcal_message(seve%e,rname,'Offset system not understood')
    write(mess,'(10(A,1X))')  &
      'Looking for "Nasmyth" but SYSOFF is:',scan%table%sysoff%val(1:nproj)
    call mrtcal_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  projcode = p_unknown
  projname = "'"//trim(scan%table%sysoff%val(jother))//"' (unknown)"
  xoffset = scan%table%xoffset%val(jother)  ! [rad]
  yoffset = scan%table%yoffset%val(jother)  ! [rad]
  !
  if (xoffset.ne.0.0 .or. yoffset.ne.0.0) then
    call mrtcal_message(seve%e,rname,'Offset system not supported')
    write(mess,'(A,A,1X,2F0.5)')  &
      'Offsets are: ',trim(scan%table%sysoff%val(jother)),xoffset,yoffset
    call mrtcal_message(seve%e,rname,mess)
    error = .true.
    return
  else
    ! Fine, but should raise a warning
    ! ZZZ Too verbose (once per chunk!)
    ! write(mess,'(A,A,A)')  &
    !   'Offset system ',trim(scan%table%sysoff%val(jother)),' with (0,0) offsets'
    ! call mrtcal_message(seve%w,rname,mess)
  endif
  !
end subroutine mrtcal_get_offset_from_scan
!
subroutine mrtcal_pixel_offset(pos,recname,pixel,rotang,error)
  use phys_const
  use gbl_message
  use gkernel_types
  use class_types
  use mrtcal_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! Apply the receiver array offsets (e.g. for HERA). pos%lamof and
  ! pos%betof are modified in return
  !---------------------------------------------------------------------
  type(class_position_t), intent(inout) :: pos      !
  character(len=*),       intent(in)    :: recname  ! Receiver name
  integer(kind=4),        intent(in)    :: pixel    ! Pixel number
  real(kind=8),           intent(in)    :: rotang   ! [deg] Derotator angle
  logical,                intent(inout) :: error    ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='PIXEL>OFFSET'
  type(projection_t) :: proj
  real(kind=8) :: xoff,yoff,aoff,rxoff,ryoff,axoff1,ayoff1,poff,angl,axoff2,ayoff2
  character(len=message_length) :: mess
  !
  ! If unrotated HERA pixels are numbered like this (see Fig.4 in HERA manual):
  !         3  6  9
  !         2  5  8
  !         1  4  7
  !
  ! Then the corresponding offsets are
  real(kind=8), parameter :: xhera(9) =  &
    (/  24.d0, 24.d0, 24.d0,   0.d0, 0.d0,  0.d0, -24.d0, -24.d0, -24.d0 /)
  real(kind=8), parameter :: yhera(9) =  &
    (/ -24.d0,  0.d0, 24.d0, -24.d0, 0.d0, 24.d0, -24.d0,  0.d0,   24.d0 /)
  !
  ! The corresponsing orientation w/t central pixel (degrees). 0 is North, 90 is
  ! East (increasing longitude):
  real(kind=8), parameter :: ahera(9) =  &
    (/ 135d0, 90.d0, 45.d0, 180d0, 0.d0,  0.d0, -135.d0, -90.d0, -45.d0 /)
  !
  ! For now, only support for HERA
  if (recname(1:4).eq.'HERA') then
    if (pixel.lt.1 .or. pixel.gt.9) then
      write (mess,'(A,I0,2A)')  'Invalid pixel number ',pixel,' for receiver ',recname
      call mrtcal_message(seve%e,rname,mess)
      error = .true.
      return
    endif
    xoff = xhera(pixel)
    yoff = yhera(pixel)
    aoff = ahera(pixel)
    !
  else
    call mrtcal_message(seve%e,rname,'Receiver '//trim(recname)//' not understood')
    error = .true.
    return
  endif
  !
  ! Early exit if no offset (save time)
  if (xoff.eq.0.d0 .and. yoff.eq.0.d0)  return
  !
  ! Setup the current projection
  call gwcs_projec(pos%lam,pos%bet,pos%projang,pos%proj,proj,error)
  if (error)  return
  !
  ! Convert to absolute spherical coordinates
  rxoff = pos%lamof
  ryoff = pos%betof
  call rel_to_abs(proj,rxoff,ryoff,axoff1,ayoff1,1)
  !
  ! Get pixel offset and orientation from center
  poff = sqrt(xoff*xoff+yoff*yoff)*rad_per_sec
  angl = (aoff+rotang)*rad_per_deg
  !
  ! Add the pixel offset
  call abs_add_distance(axoff1,ayoff1,poff,angl,axoff2,ayoff2)
  !
  ! Convert back to current projection
  call abs_to_rel(proj,axoff2,ayoff2,rxoff,ryoff,1)
  pos%lamof = rxoff
  pos%betof = ryoff
  !
end subroutine mrtcal_pixel_offset
!
subroutine mrtcal_chunk_user_from_data(primary,user,error)
  use gbl_message
  use class_types
  use imbfits_types
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_chunk_user_from_data
  use mrtcal_calib_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(imbfits_primary_t),  intent(in)    :: primary
  type(mrtcal_classuser_t), intent(out)   :: user
  logical,                  intent(inout) :: error
  ! Local
  character(len=backname_length) :: mybackend
  character(len=obstype_length) :: myobstype
  integer(kind=4) :: itype
  character(len=*), parameter :: rname='CHUNK>USER>FROM>DATA'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  mybackend = primary%instrume%val
  call sic_upper(mybackend)
  select case (mybackend)
  case ('VESPA')
     user%backeff = 0.87
  case ('WILMA')
     user%backeff = 0.87 ! *** JP
!!$  case ('FTS')
!!$  case ('4MHZ')
!!$  case ('BBC')
!!$  case ('NBC')
!!$  case ('CONT')
  case default
     ! All other backends are supposed to have an efficiency of 1
     user%backeff = 1.0
  end select
  !
  ! ZZZ Isn't this already parsed somewhere else?
  myobstype = primary%obstype%val
  call sic_upper(myobstype)
  user%obstype = obstype_unknown
  do itype=1,nobstypes_imbfits
    if (myobstype.eq.mrtindex_obstype_imbfits(itype)) then
      if (itype.eq.obstype_onoff)then
        user%obstype = obstype_tracked
      else
        user%obstype = itype
      endif
    endif
  enddo
  !
end subroutine mrtcal_chunk_user_from_data
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_chunk_show(chunk)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_chunk_show
  !---------------------------------------------------------------------
  ! @ private
  ! Display a 'chunk' element. For debugging purpose.
  !---------------------------------------------------------------------
  type(chunk_t), intent(in) :: chunk
  ! Local
  character(len=*), parameter :: rname='CHUNK>SHOW'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  write(*,'(A,T12,I0)')   ' - Id:',chunk%id
  if (associated(chunk%data1)) then
     write(*,'(A,T12,2(1X,1PG12.6),A,2(1X,1PG12.6))')  &
          ' - Values:',chunk%data1(1:2),' ... ',chunk%data1(chunk%ndata-1:chunk%ndata)
  else
     write(*,'(A)')  &
          ' - Values: not associated'
  endif
  write(*,'(A,T12,I0)')   ' - nchan:',chunk%spe%nchan
  write(*,'(A,T12,F0.5)') ' - rchan:',chunk%spe%rchan
  write(*,'(A,T12,F0.5)') ' - restf:',chunk%spe%restf
  write(*,'(A,T12,F0.5)') ' - image:',chunk%spe%image
  write(*,'(A,T12,F0.5)') ' - fres:',chunk%spe%fres
  write(*,'(A,T12,F0.5)') ' - vres:',chunk%spe%vres
  write(*,'(A,T12,F0.5)') ' - voff:',chunk%spe%voff
  !
end subroutine mrtcal_chunk_show
