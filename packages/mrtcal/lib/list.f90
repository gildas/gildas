subroutine mrtcal_list_comm(line,error)
  use gbl_message
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_list_comm
  use mrtcal_index_vars
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !   LIST [IN|CURRENT]
  !   1    [/TOC]
  !   2    [/PAGE]
  !   3    [/COLUMNS List]
  !   4    [/FILE OutputFile]
  !   5    [/VARIABLE VarName]
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Input command line
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='MLIST'
  integer(kind=4), parameter :: nwhat=2
  character(len=7), parameter :: what(nwhat) = (/ 'IN     ','CURRENT' /)
  character(len=7) :: argum,key,optname
  integer(kind=4) :: nchar,ikey,ier,olun
  type(mrtindex_optimize_t), pointer :: optx
  integer(kind=4) :: custom(20)  ! Up to 20 custom columns
  character(len=filename_length) :: file
  integer(kind=4), parameter :: optpage=2
  !
  argum = 'CURRENT'
  call sic_ke(line,0,1,argum,nchar,.false.,error)
  if (error)  return
  call sic_ambigs(rname,argum,key,ikey,what,nwhat,error)
  if (error)  return
  !
  if (ikey.eq.1) then
    optx => ix
    optname = 'In'
  else
    optx => cx
    optname = 'Current'
  endif
  !
  ! Output to STDOUT or file?
  if (sic_present(4,0)) then  ! /FILE
    call sic_ch(line,4,1,file,nchar,.true.,error)
    if (error)  return
    !
    ier = sic_getlun(olun)
    if (ier.ne.1) then
      call mrtcal_message(seve%e,rname,'No logical unit left')
      error = .true.
      return
    endif
    ier = sic_open(olun,file,'NEW',.false.)
    if (ier.ne.0) then
      call mrtcal_message(seve%e,rname,'Cannot open file '//file)
      error = .true.
      call sic_frelun(olun)
      return
    endif
  else
    olun = 6  ! STDOUT
  endif
  !
  if (sic_present(1,0)) then  ! /TOC
    call mrtindex_list_toc_comm(optx,line,olun,error)
    if (error)  goto 10
  else
    call mrtindex_list_columns(line,custom,error)
    if (error)  goto 10
    call mrtindex_list(optx,optname,custom,olun,sic_present(optpage,0),error)
    if (error)  goto 10
  endif
  !
10 continue
  if (olun.ne.6) then
    ier = sic_close(olun)
    call sic_frelun(olun)
  endif
  !
end subroutine mrtcal_list_comm
