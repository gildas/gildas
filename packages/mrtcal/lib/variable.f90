subroutine mrtcal_variable_comm(line,error)
  use gbl_message
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_variable_comm
  use mrtcal_buffers
  use mrtcal_index_vars
  !---------------------------------------------------------------------
  ! @ private
  !  Support routine for command
  !   VARIABLE [*|WHAT [READ|WRITE]]
  !  Create the Sic structures mapping the mrtcal internal buffers
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   !
  logical,          intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='VARIABLE'
  integer(kind=4), parameter :: nwhat=4
  character(len=7), parameter :: what(nwhat) =  &
    (/ 'MHEAD  ','IMBF   ','SUBS   ','IMBDATA' /)
  logical :: dowhat(nwhat)
  character(len=5), parameter :: rw(2) = (/ 'READ ','WRITE' /)
  character(len=12) :: argum,keyword
  integer(kind=4) :: nc,ikey
  logical :: ro
  !
  ! Decode which structure is to be created
  argum = '*'
  call sic_ke(line,0,1,argum,nc,.false.,error)
  if (error)  return
  if (argum.eq.'*') then
    dowhat(:) = .true.
  else
    call sic_ambigs(rname,argum,keyword,ikey,what,nwhat,error)
    if (error)  return
    dowhat(:) = .false.
    dowhat(ikey) = .true.
  endif
  !
  ! Decode READ or WRITE flag
  argum = 'READ'
  call sic_ke(line,0,2,argum,nc,.false.,error)
  if (error)  return
  call sic_ambigs(rname,argum,keyword,ikey,rw,2,error)
  if (error)  return
  ro = keyword.eq.'READ'
  !
  ! Safety check
  if (.not.ro .and. any(dowhat(2:4))) then
    call mrtcal_message(seve%e,rname,'MAIN, SUBSCAN, and DATA variables can not be WRITEable as')
    call mrtcal_message(seve%e,rname,'the corresponding MUPDATE and MWRITE are not implemented')
    error = .true.
    return
  endif
  !
  ! Do the real job
  if (dowhat(1)) then
    call mrtindex_variable_entry('MHEAD',kentry%head,ro,error)
    if (error)  return
  endif
  if (dowhat(2)) then
    call imbfits_variable_imbfits('IMBF',rfile%imbf,ro,error)
    if (error)  return
  endif
  if (dowhat(3)) then
    call imbfits_variable_subscan('SUBS',rfile%subscanbuf%subscan,ro,error)
    if (error)  return
  endif
  if (dowhat(4)) then
    call mrtcal_variable_databuf('IMBDATA',rfile%subscanbuf%databuf,ro,error)
    if (error)  return
  endif
  !
end subroutine mrtcal_variable_comm
!
subroutine mrtcal_variable_databuf(struct,databuf,ro,error)
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_variable_databuf
  use mrtcal_buffer_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a data_buffer_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),    intent(in)    :: struct   ! Parent structure name
  type(data_buffer_t), intent(in)    :: databuf  !
  logical,             intent(in)    :: ro       ! Read-Only
  logical,             intent(inout) :: error    !
  ! Local
  logical :: userreq
  !
  userreq = .false.
  !
  call sic_delvariable(struct,userreq,error)
  call sic_defstructure(struct,.true.,error)
  if (error)  return
  !
  call mrtcal_variable_book(trim(struct)//'%TIME',databuf%time,ro,error)
  if (error)  return
  call imbfits_variable_imbfdata(trim(struct)//'%IMBF',databuf%imbf,ro,error)
  if (error)  return
  !
end subroutine mrtcal_variable_databuf
!
subroutine mrtcal_variable_book(struct,book,ro,error)
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_variable_book
  use mrtcal_bookkeeping_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a book_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: struct  ! Parent structure name
  type(book_t),     intent(in)    :: book    !
  logical,          intent(in)    :: ro      ! Read-Only
  logical,          intent(inout) :: error   !
  ! Local
  logical :: userreq
  !
  userreq = .false.
  !
  call sic_delvariable(struct,userreq,error)
  call sic_defstructure(struct,.true.,error)
  if (error)  return
  !
!   call mrtcal_variable_range(trim(struct)//'%TOT',book%tot,ro,error)
!   if (error)  return
!   call mrtcal_variable_range(trim(struct)//'%CUR',book%cur,ro,error)
!   if (error)  return
!   call mrtcal_variable_block(trim(struct)//'%BLOCK',book%block,ro,error)
!   if (error)  return
  !
end subroutine mrtcal_variable_book
!
subroutine mrtcal_variable_range(struct,range,ro,error)
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_variable_range
  use mrtcal_bookkeeping_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a range_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: struct  ! Parent structure name
  type(range_t),    intent(in)    :: range   !
  logical,          intent(in)    :: ro      ! Read-Only
  logical,          intent(inout) :: error   !
  ! Local
  logical :: userreq
  !
  userreq = .false.
  !
  call sic_delvariable(struct,userreq,error)
  call sic_defstructure(struct,.true.,error)
  if (error)  return
  !
  call sic_def_inte(trim(struct)//'%FIRST', range%first, 0,0,ro,error)
  call sic_def_inte(trim(struct)//'%LAST',  range%last,  0,0,ro,error)
  call sic_def_inte(trim(struct)//'%N',     range%n,     0,0,ro,error)
  call sic_def_inte(trim(struct)//'%I',     range%i,     0,0,ro,error)
  call sic_def_dble(trim(struct)//'%VFIRST',range%vfirst,0,0,ro,error)
  call sic_def_dble(trim(struct)//'%VLAST', range%vlast, 0,0,ro,error)
  !
end subroutine mrtcal_variable_range
!
subroutine mrtcal_variable_block(struct,block,ro,error)
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_variable_block
  use mrtcal_bookkeeping_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a block_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: struct  ! Parent structure name
  type(block_t),    intent(in)    :: block   !
  logical,          intent(in)    :: ro      ! Read-Only
  logical,          intent(inout) :: error   !
  ! Local
  logical :: userreq
  !
  userreq = .false.
  !
  call sic_delvariable(struct,userreq,error)
  call sic_defstructure(struct,.true.,error)
  if (error)  return
  !
#if defined(BITS64)
  call sic_def_long(trim(struct)//'%NELT',   block%nelt,   0,0,ro,error)
  call sic_def_long(trim(struct)//'%ELTSIZE',block%eltsize,0,0,ro,error)
  call sic_def_long(trim(struct)//'%BUFSIZE',block%bufsize,0,0,ro,error)
#else
  call sic_def_inte(trim(struct)//'%NELT',   block%nelt,   0,0,ro,error)
  call sic_def_inte(trim(struct)//'%ELTSIZE',block%eltsize,0,0,ro,error)
  call sic_def_inte(trim(struct)//'%BUFSIZE',block%bufsize,0,0,ro,error)
#endif
  !
end subroutine mrtcal_variable_block
