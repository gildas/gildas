!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_pack_set(pack)
  use gpack_def
  use gkernel_interfaces
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this => mrtcal_pack_set
  !----------------------------------------------------------------------
  ! @ public-mandatory (because symbol is used elsewhere)
  !----------------------------------------------------------------------
  type(gpack_info_t), intent(out) :: pack
  !
  pack%name='mrtcal'
  pack%ext='.cal'
  pack%authors="A.Sievers, S.Bardeau, J.Pety" 
  pack%depend(1:3) = (/ locwrd(sic_pack_set), locwrd(greg_pack_set), locwrd(class_pack_set)/) ! List here dependencies
  pack%init=locwrd(mrtcal_pack_init)
  pack%clean=locwrd(mrtcal_pack_clean)
  !
end subroutine mrtcal_pack_set
!
subroutine mrtcal_pack_init(gpack_id,error)
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this => mrtcal_pack_init
  !----------------------------------------------------------------------
  ! @ private-mandatory
  !----------------------------------------------------------------------
  integer :: gpack_id
  logical :: error
  !
  ! Init messaging
  call mrtcal_message_set_id(gpack_id)
  call mrtindex_message_set_id(gpack_id)
  call imbfits_message_set_id(gpack_id)
  call telcal_message_set_id(gpack_id)
  ! Init error flag
  error = .false.
  ! MRTCAL package specific initialization
  call mrtcal_init(error)
  if (error) return
  ! Load local language
  call mrtcal_load
  !
end subroutine mrtcal_pack_init
!
subroutine mrtcal_pack_clean(error)
  use mrtcal_interfaces, except_this => mrtcal_pack_clean
  !----------------------------------------------------------------------
  ! @ private-mandatory
  ! Called at end of session. Might clean here for example global buffers
  ! allocated during the session
  !----------------------------------------------------------------------
  logical :: error
  !
  call mrtcal_exit(error)
  if (error) return
  !
end subroutine mrtcal_pack_clean
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
