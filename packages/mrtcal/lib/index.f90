subroutine mrtcal_index_comm(line,error)
  use gbl_message
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_index_comm
  use mrtcal_index_vars
  !---------------------------------------------------------------------
  ! @ private
  !  Support routine for command
  !   INDEX BUILD|UPDATE|OPEN|APPEND|WATCH|OUTPUT [DirName]
  ! 1       [/FILE IndexFile]
  ! 2       [/RECURSIVE]
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line  ! Input command line
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='INDEX'
  !
  if (sic_present(1,0) .and. sic_present(2,0)) then
    call mrtcal_message(seve%e,rname,'Exclusive options /FILE and /RECURSIVE')
    error = .true.
    return
  endif
  !
  ! All these commands modify the input index: reset the current index
  mfound = 0
  !
  call mrtindex_index_comm(line,ix,cx,error)
  if (error)  return
  !
end subroutine mrtcal_index_comm
