module mrtcal_interfaces
  use mrtcal_interfaces_private
  use mrtcal_interfaces_public
end module mrtcal_interfaces
!
module mrtcal_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! MRTCAL dependencies public interfaces. Do not use out of the library.
  !---------------------------------------------------------------------
  use gkernel_interfaces
  use telcal_interfaces_public
  use classic_interfaces_public
  use classcore_interfaces_public
  use class_interfaces_public
  use imbfits_interfaces_public
  use mrtindex_interfaces_public
end module mrtcal_dependencies_interfaces
