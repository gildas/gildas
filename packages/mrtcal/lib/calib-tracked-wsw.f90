!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_calib_tracked_wsw(mrtset,backcal,backsci,filebuf,error)
  use gbl_message
  use gkernel_interfaces
  use mrtcal_level_codes
  use mrtcal_buffer_types
  use mrtcal_setup_types
  use mrtcal_interfaces, except_this => mrtcal_calib_tracked_wsw
  !---------------------------------------------------------------------
  ! @ private
  ! Gather all needed steps to read, calibrate, and write a 
  ! tracked wobbler switched scan for a given backend
  !---------------------------------------------------------------------
  type(mrtcal_setup_t),    intent(in)    :: mrtset
  type(calib_backend_t),   intent(in)    :: backcal
  type(science_backend_t), intent(inout) :: backsci
  type(imbfits_buffer_t),  intent(inout) :: filebuf
  logical,                 intent(inout) :: error
  ! Local
  integer(kind=4) :: ion,non
  integer(kind=4) :: iref,nref
  character(len=*), parameter :: rname='CALIB>TRACKED>WSW'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_init_refloop(backsci,nref,error)
  if (error) return
  do iref=1,nref
     call mrtcal_get_nextref(backsci,error)
     if (error) return
     call mrtcal_init_onloop(backsci,non,error)
     if (error) return
     do ion=1,non
        call mrtcal_get_nexton(backsci,error)
        if (error) return
        call mrtcal_init_dumpcycle_loop('track',mrtset,filebuf,backsci,error)
        if (error) return
        do while (backsci%switch%book%idump.le.backsci%switch%book%ndump)
           call mrtcal_get_next_dumpcycle(mrtset,filebuf,backsci,error)
           if (error) return
           if (backsci%switch%book%found) then
              call mrtcal_on_minus_off_new(nototfmap,notfsw,backcal,backsci,error)
              if (error) return
              call mrtcal_tscale_computation(backcal,backsci,error)
              if (error) return
              call mrtcal_tscale_application(backsci%tscale,backsci%diff,error)
              if (error) return
              call mrtcal_accumulate_or_write(level_cycl_code,mrtset,backsci,error)
              if (error) return
           endif
           ! User can interrupt the pipeline
           if (sic_ctrlc_status()) then
              error = .true.
              exit
           endif
        enddo ! switch%book%idump
        call mrtcal_switch_book_list(backsci%switch%book,error)
        if (error) return
        call mrtcal_accumulate_or_write(level_subs_code,mrtset,backsci,error)
        if (error) return
     enddo ! ion
  enddo ! iref
  call mrtcal_accumulate_or_write(level_scan_code,mrtset,backsci,error)
  if (error) return
  !
end subroutine mrtcal_calib_tracked_wsw
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

