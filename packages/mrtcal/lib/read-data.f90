!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_read_subscan_data(imbf,subs,tochunk,databuf,error)
  use gbl_message
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this => mrtcal_read_subscan_data
  use mrtcal_buffer_types
  use mrtcal_setup_types
  !---------------------------------------------------------------------
  ! @ public
  ! Read part of the DATA column from the current subscan (i.e. the
  ! current one CFITSIO is positioned in), and implicitely convert it
  ! in chunksets.
  !---------------------------------------------------------------------
  type(imbfits_t),         intent(in)    :: imbf
  type(imbfits_subscan_t), intent(in)    :: subs
  logical,                 intent(in)    :: tochunk
  type(data_buffer_t),     intent(inout) :: databuf
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='READ>SUBSCAN>DATA'
  integer(kind=4) :: nchan,npix
  type(range_t) :: drange
  !
  if (databuf%time%cur%n.eq.0) then
    ! We actually want a warning (no error), but we have to check twice
    ! the subsequent protection for 0-sized arrays...
    call mrtcal_message(seve%e,rname,'Nothing to read')
    error = .true.
    return
  endif
  !
  ! 1) Data only
  nchan = subs%backdata%head%channels%val
  npix  = 1  ! ZZZ TBD: set correctly
  ! Compute DATA range to read from compressed range:
  call mrtcal_bookkeeping_compr2uncompr(databuf%time%cur,  &
    subs%backdata%table%backpoin%val,drange,error)
  if (error)  return
  call imbfits_read_data(rname,         &
                         imbf%file,     &
                         nchan,         &
                         npix,          &
                         drange%first,  &
                         drange%n,      &  ! Number of non-compressed DATA rows to read
                         databuf%imbf,  &
                         error)
  if (error)  return
  !
  if (tochunk) then
    ! 2) Convert in chunk sets
    call mrtcal_chunksets_from_data(imbf,subs,databuf,error)
    if (error)  return
  endif
  !
end subroutine mrtcal_read_subscan_data
!
subroutine mrtcal_free_subscan_data(databuf,error)
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_free_subscan_data
  use mrtcal_buffer_types
  !---------------------------------------------------------------------
  ! @ private
  ! Free of the DATA column from the current subscan, and its conversion
  ! in chunksets.
  !---------------------------------------------------------------------
  type(data_buffer_t), intent(inout) :: databuf
  logical,             intent(inout) :: error
  !
  ! 1) Data only
  call imbfits_free_data(databuf%imbf,error)
  if (error)  return
  !
  ! 2) Conversion in chunk sets
  call free_chunkset_3d(databuf%mrtc,error)
  if (error)  return
  !
end subroutine mrtcal_free_subscan_data
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
