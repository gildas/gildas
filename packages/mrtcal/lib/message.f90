!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage MRTCAL messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module mrtcal_message_private
  use gpack_def
  !
  ! Identifier used for message identification
  integer :: mrtcal_message_id = gpack_global_id  ! Default value for startup message
  !
end module mrtcal_message_private
!
subroutine mrtcal_message_set_id(id)
  use mrtcal_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer, intent(in) :: id
  ! Local
  character(len=message_length) :: mess
  !
  mrtcal_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',mrtcal_message_id
  call mrtcal_message(seve%d,'mrtcal_message_set_id',mess)
  !
end subroutine mrtcal_message_set_id
!
subroutine mrtcal_message(mkind,procname,message)
  use mrtcal_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! Messaging facility for the current library. Calls the low-level
  ! (internal) messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  !
  call gmessage_write(mrtcal_message_id,mkind,procname,message)
  !
end subroutine mrtcal_message
!
function failed_calibrate_name(rname,sname,error)
  use gbl_message
  use mrtcal_interfaces, except_this=>failed_calibrate_name
  !---------------------------------------------------------------------
  ! @ private-generic failed_calibrate
  !  Display the subscan name if input error is .true.
  !  Nothing done if error is .false.
  !---------------------------------------------------------------------
  logical :: failed_calibrate_name  ! Function value on return
  character(len=*), intent(in) :: rname  ! Calling routine name
  character(len=*), intent(in) :: sname  ! Subscan name
  logical,          intent(in) :: error  ! Error status
  !
  failed_calibrate_name = error
  if (.not.error)  return
  !
  call mrtcal_message(seve%e,rname,'Failed for subscan '''//trim(sname)//'''')
  !
end function failed_calibrate_name
!
function failed_calibrate_num(rname,snum,error)
  use gbl_message
  use mrtcal_interfaces, except_this=>failed_calibrate_num
  !---------------------------------------------------------------------
  ! @ private-generic failed_calibrate
  !  Display the subscan number if input error is .true.
  !  Nothing done if error is .false.
  !---------------------------------------------------------------------
  logical :: failed_calibrate_num  ! Function value on return
  character(len=*), intent(in) :: rname  ! Calling routine name
  integer(kind=4),  intent(in) :: snum   ! Subscan number
  logical,          intent(in) :: error  ! Error status
  ! Local
  character(len=message_length) :: mess
  !
  failed_calibrate_num = error
  if (.not.error)  return
  !
  write(mess,'(A,I0)')  'Failed for subscan #',snum
  call mrtcal_message(seve%e,rname,mess)
  !
end function failed_calibrate_num
