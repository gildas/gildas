!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_calib_tracked_psw(mrtset,backcal,backsci,filebuf,error)
  use gbl_message
  use gkernel_interfaces
  use mrtcal_buffer_types
  use mrtcal_setup_types
  use mrtcal_interfaces, except_this => mrtcal_calib_tracked_psw
  !---------------------------------------------------------------------
  ! @ private
  ! Gather all needed steps to read, calibrate, and write a 
  ! tracked position switched scan for a given backend
  !---------------------------------------------------------------------
  type(mrtcal_setup_t),    intent(in)    :: mrtset
  type(calib_backend_t),   intent(in)    :: backcal
  type(science_backend_t), intent(inout) :: backsci
  type(imbfits_buffer_t),  intent(inout) :: filebuf
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CALIB>TRACKED>PSW'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_iterate_subscancycle(mrtset,filebuf%imbf, &
    filebuf%subscanbuf,backcal,backsci,error)
  if (error) return
  !
end subroutine mrtcal_calib_tracked_psw
!
subroutine mrtcal_calib_tracked_psw_cycle(mrtset,imbf,ifirst,nswitch,  &
  subscanbuf,backcal,backsci,error)
  use gbl_message
  use mrtcal_interfaces, except_this=>mrtcal_calib_tracked_psw_cycle
  use mrtcal_buffer_types
  use mrtcal_setup_types
  !---------------------------------------------------------------------
  ! @ private
  ! Calibrate a TRACKED PSW cycle of subscans
  !---------------------------------------------------------------------
  type(mrtcal_setup_t),    intent(in)            :: mrtset      !
  type(imbfits_t),         intent(in)            :: imbf        !
  integer(kind=4),         intent(in)            :: ifirst      ! First subscan in cycle
  integer(kind=4),         intent(in)            :: nswitch     ! Number of subscans in cycle
  type(subscan_buffer_t),  intent(inout)         :: subscanbuf  !
  type(calib_backend_t),   intent(in)            :: backcal     !
  type(science_backend_t), intent(inout), target :: backsci     !
  logical,                 intent(inout)         :: error       ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='CALIB>TRACKED>PSW>CYCLE'
  integer(kind=4) :: isub
  logical, parameter :: iscal=.false.
  type(chunkset_2d_t) :: curr  ! Buffer for reading a ON or OFF subscan
  type(chunkset_2d_t), pointer :: cumul  ! Pointer to current ON or OFF cumulation
  logical :: initon,initoff
  character(len=23) :: iso
  character(len=message_length) :: mess
  !
  initon = .true.
  initoff = .true.
  do isub=ifirst,ifirst+nswitch-1
    call mrtcal_average_times(mrtset,iscal,isub,'track',imbf,subscanbuf,curr,error)
    if (error)  goto 10
    !
    if (backsci%list%onoff%bak(isub).eq.backsci%list%on) then
      if (initon) then
        ! Use first ON subscan to initialize the ON structure (in particular
        ! regarding offsets)
        call mrtcal_chunkset_2d_accumulate_init(curr,backsci%on%curr,error)
        if (error)  goto 10
        initon = .false.
      endif
      cumul => backsci%on%curr
    else
      if (initoff) then
        ! Use first OFF subscan to initialize the OFF structure (in particular
        ! regarding offsets)
        if (.not.associated(backsci%off%curr)) then
          ! Because this one is a pointer => allocate a scalar object
          allocate(backsci%off%curr)
        endif
        call mrtcal_chunkset_2d_accumulate_init(curr,backsci%off%curr,error)
        if (error)  goto 10
        initoff = .false.
      endif
      cumul => backsci%off%curr
    endif
    !
    call mrtcal_chunkset_2d_accumulate_setweight(curr,backsci%tscale,  &
      mrtset%out%weight,error)
    if (error)  goto 10
    call mrtcal_chunkset_2d_accumulate_do(curr,cumul,error)
    if (error)  goto 10
  enddo
  !
  backsci%switch%cycle%ion = 0   ! Deactivated in this context
  backsci%switch%cycle%ioff = 0  ! Deactivated in this context
  call mrtcal_on_minus_off(nototfmap,notfsw,backsci,error)
  if (error)  goto 10
  call mrtcal_tscale_computation(backcal,backsci,error)
  if (error)  goto 10
  call mrtcal_tscale_application(backsci%tscale,backsci%diff,error)
  if (error)  goto 10
  !
  call gag_mjd2isodate(backsci%off%curr%chunkset(1,1)%chunks(1)%mjd,iso,error)
  write(mess,'(6a)') 'Average OFF    date: ',iso
  call mrtcal_message(seve%i,rname,mess)
  call gag_mjd2isodate(backsci%on%curr%chunkset(1,1)%chunks(1)%mjd,iso,error)
  write(mess,'(6a)') 'Average ON     date: ',iso
  call mrtcal_message(seve%i,rname,mess)
  call gag_mjd2isodate(backsci%diff%chunkset(1,1,1)%chunks(1)%mjd,iso,error)
  write(mess,'(6a)') 'Average ON-OFF date: ',iso
  call mrtcal_message(seve%i,rname,mess)
  !
  if (mrtset%out%accmode.eq.accmode_cycl) then
    call mrtcal_write_toclass(backsci%diff,mrtset%out,backsci%nspec,error)
    if (error)  goto 10
  endif
  !
10 continue
  call free_chunkset_2d(curr,error)
  if (error)  return
  !
end subroutine mrtcal_calib_tracked_psw_cycle
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
