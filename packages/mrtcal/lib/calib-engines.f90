!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_check_substype(ith,name,subheader,error)
  use gbl_message
  use gkernel_interfaces
  use imbfits_types
  use mrtcal_interfaces, except_this => mrtcal_check_substype
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4),         intent(in)    :: ith
  character(len=*),        intent(in)    :: name
  type(imbfits_subscan_t), intent(in)    :: subheader
  logical,                 intent(inout) :: error
  ! Local
  character(len=char0d_length) :: name1,name2
  character(len=message_length) :: mess
  character(len=*), parameter :: rname='MRTCAL>CHECK>SUBSTYPE'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  name1 = subheader%antslow%head%substype%val
  name2 = name
  call sic_upper(name1)
  call sic_upper(name2)
  if (name1.ne.name2) then
     write(mess,'(a,i0,a,a,a,a)') 'Subscan #',ith,&
          ' is a ',trim(name1),&
          ', not a ',trim(name2)
     call mrtcal_message(seve%e,rname,mess)
     error = .true.
     return
  endif
  !
end subroutine mrtcal_check_substype
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_average_times(mrtset,iscal,ith,name,imbf,subscanbuf,averaged,error)
  use gbl_message
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this => mrtcal_average_times
  use mrtcal_buffer_types
  use mrtcal_setup_types
  !---------------------------------------------------------------------
  ! @ private
  ! We read the imbfits data in a limited size buffer. To do this, we
  ! read it by part. Thus, there is no reason to keep it in the main
  ! structure. We keep only the product, i.e. the time average...
  !---------------------------------------------------------------------
  type(mrtcal_setup_t),   intent(in)    :: mrtset
  logical,                intent(in)    :: iscal
  integer(kind=4),        intent(in)    :: ith
  character(len=*),       intent(in)    :: name
  type(imbfits_t),        intent(in)    :: imbf
  type(subscan_buffer_t), intent(inout) :: subscanbuf
  type(chunkset_2d_t),    intent(inout) :: averaged
  logical,                intent(inout) :: error
  ! Local
  real(kind=8) :: elevation ! Double in contrast with CLASS header
  character(len=message_length) :: mess
  integer(kind=4) :: idump
  logical :: needupdate
  logical, parameter :: tochunk=.true.  ! DATA always mapped on chunks in this context
  character(len=*), parameter :: rname='AVERAGE>TIMES'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  ! Initialize to zero at start of the process
  averaged%isub = 0
  if (iscal) then
     call imbfits_read_subscan_header(imbf,name,ith,.not.mrtset%cal%bad,  &
       mrtset%cal%mjdinter,mrtset%cal%antslowmjdshift,subscanbuf%subscan,error)
     if (error)  return
  else
     call imbfits_read_subscan_header_bynum(imbf,ith,.not.mrtset%cal%bad,  &
       mrtset%cal%mjdinter,mrtset%cal%antslowmjdshift,subscanbuf%subscan,error)
     if (error)  return
     call mrtcal_check_substype(ith,name,subscanbuf%subscan,error)
     if (error)return
  endif
  call mrtcal_get_median_elevation(subscanbuf%subscan,elevation,error)
  if (error) return
  call mrtcal_get_time_range_for_backend(subscanbuf%subscan,subscanbuf%databuf%time%tot,error)
  if (error)  return
  call mrtcal_bookkeeping_init_time(mrtset%bufsize,subscanbuf%subscan,subscanbuf%databuf%time,error)
  if (error)  return
  if (subscanbuf%databuf%time%tot%n.le.0) then
    ! Special case for 0-sized backendDATA: deallocate previous allocation
    ! and set (nset,npix) to (0,0), so that later code does not attempt to
    ! access unallocated chunkset_2d.
    ! This is not done always in the initialization part a few lines above
    ! because in the general case we want to keep the previous allocation.
    if (iscal) then
      write (mess,'(A,A,A)') 'DATA table is zero-sized for subscan ''',name,''''
    else
      write (mess,'(A,I0)') 'DATA table is zero-sized for subscan #',ith
    endif
    call mrtcal_message(seve%w,rname,mess)
    call free_chunkset_2d(averaged,error)
    if (error)  return
  else
    ! Average the DATA dumps
    ! Start by loading in the buffer a piece of DATA starting at the first on-track
    ! dump. Do not try to guess the last one to be loaded in this piece of DATA:
    ! this is a complex issue because of bad dumps. Anyway mrtcal_bookkeeping_iterate
    ! will not read a single dump: it will do its best to read at once as much DATA
    ! as allowed.
    idump = subscanbuf%databuf%time%tot%first
    do while (idump.le.subscanbuf%databuf%time%tot%last)
      ! Load a new piece of DATA, starting from 'idump'
      call mrtcal_bookkeeping_iterate(subscanbuf%databuf%time,subscanbuf%subscan,  &
        idump,idump,needupdate,error)
      if (error)  return
      if (.not.needupdate) then
        ! Must always be true!
        call mrtcal_message(seve%e,rname,'Internal error')
        error = .true.
        return
      endif
      call mrtcal_read_subscan_data(imbf,subscanbuf%subscan,tochunk,subscanbuf%databuf,error)
      if (error)  return
      if (idump.eq.subscanbuf%databuf%time%tot%first) then
        call clone_chunkset_2d(subscanbuf%databuf%mrtc,averaged,.true.,error)
        if (error)  return
        call mrtcal_average_time_init(elevation,averaged,error)
        if (error)  return
      endif
      call mrtcal_average_time_range(subscanbuf%databuf%mrtc,averaged,error)
      if (error)  return
      !
      ! Next piece of DATA? Start at next not-yet-loaded dump
      idump = subscanbuf%databuf%time%cur%last+1
    enddo
  endif
  call mrtcal_chunkset_2d_noise_init(averaged,error)
  if (error) return
  ! Set to current value at end of process
  averaged%isub = ith
  averaged%corr%el = elevation ! *** JP Is elevation computed in the authorized time range?
  averaged%corr%airmass = airmass(elevation,error)
  if (error) return
  if (subscanbuf%databuf%time%tot%first.eq.0) then  ! If 0 data available
    averaged%mjd%beg = 0.d0
  else
    averaged%mjd%beg = subscanbuf%subscan%backdata%table%mjd%val(subscanbuf%databuf%time%tot%first)
  endif
  if (subscanbuf%databuf%time%tot%last.eq.0) then  ! If 0 data available
    averaged%mjd%end = 0.d0
  else
    averaged%mjd%end = subscanbuf%subscan%backdata%table%mjd%val(subscanbuf%databuf%time%tot%last)
  endif
  averaged%mjd%med = 0.5d0*(averaged%mjd%beg+averaged%mjd%end) ! *** JP a true median would be better...
  !
end subroutine mrtcal_average_times
!
subroutine mrtcal_average_time_init(elevation,chunkset_2d,error)
  use gbl_message
  use mrtcal_dependencies_interfaces
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this => mrtcal_average_time_init
  !---------------------------------------------------------------------
  ! @ private
  !  Initialize the input array of chunksets:
  !  - the data and weights arrays (Npix x Nset) are filled with zeros,
  !---------------------------------------------------------------------
  real(kind=8),        intent(in)    :: elevation    ! Double in contrast with CLASS header
  type(chunkset_2d_t), intent(inout) :: chunkset_2d  !
  logical,             intent(inout) :: error        !
  ! Local
  type(chunkset_t), pointer :: outset
  integer(kind=size_length) :: ipix,iset,ichunk
  character(len=*), parameter :: rname='AVERAGE>TIME>INIT'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  do ipix=1,chunkset_2d%npix
     do iset=1,chunkset_2d%nset
        outset => chunkset_2d%chunkset(iset,ipix)
        do ichunk=1,outset%n
           ! Init header (over elements already cloned)
           outset%chunks(ichunk)%gen%el = real(elevation)
           outset%chunks(ichunk)%gen%time = 0.
           ! Init data with zeroes (suited for ongoing average)
           call mrtcal_chunk_init_data(outset%chunks(ichunk),0.,0.,0.,error)
           if (error) return
        enddo ! ichunk
     enddo ! iset
  enddo ! ipix
  !
end subroutine mrtcal_average_time_init
!
subroutine mrtcal_average_time_range(chunkset_3d,chunkset_2d,error)
  use gbl_message
  use mrtcal_dependencies_interfaces
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this => mrtcal_average_time_range
  !---------------------------------------------------------------------
  ! @ private
  !  Average all the chunksets from the input chunkset_3d into the
  ! output chunkset_2d.
  !---------------------------------------------------------------------
  type(chunkset_3d_t), intent(in)    :: chunkset_3d
  type(chunkset_2d_t), intent(inout) :: chunkset_2d
  logical,             intent(inout) :: error
  ! Local
  type(chunkset_t), pointer :: inchunkset,ouchunkset
  integer(kind=size_length) :: itime,ipix,iset,ichunk,iphas,nphas
  character(len=*), parameter :: rname='AVERAGE>TIME>RANGE'
  logical :: contaminate
  real(kind=8) :: iinteg,ointeg,sinteg
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  contaminate = .true.  ! Input bad channels contaminate the output sum
  !
  do itime=1,chunkset_3d%ntime
     do ipix=1,chunkset_3d%npix
        do iset=1,chunkset_3d%nset
           inchunkset => chunkset_3d%chunkset(iset,ipix,itime)
           ouchunkset => chunkset_2d%chunkset(iset,ipix)
           do ichunk=1,inchunkset%n
              ! Data
              call simple_waverage(                      &
                   inchunkset%chunks(ichunk)%data1,      &
                   inchunkset%chunks(ichunk)%dataw,bad,  &
                   ouchunkset%chunks(ichunk)%data1,      &
                   ouchunkset%chunks(ichunk)%dataw,bad,  &
                   1,inchunkset%chunks(ichunk)%ndata,    &
                   contaminate,.true.)
              ! Header:
              !  - integration time
              !  - mean MJD, with corresponding DOBS+UT
              !  - other elements left unmodified from initial cloning
              iinteg = inchunkset%chunks(ichunk)%gen%time
              ointeg = ouchunkset%chunks(ichunk)%gen%time
              sinteg = iinteg+ointeg  ! Total integration time
              ouchunkset%chunks(ichunk)%mjd =  &
                (ouchunkset%chunks(ichunk)%mjd*ointeg +  &
                 inchunkset%chunks(ichunk)%mjd*iinteg)/sinteg  ! Mean MJD
              call gag_mjd2gagut(ouchunkset%chunks(ichunk)%mjd,       &
                                 ouchunkset%chunks(ichunk)%gen%dobs,  &
                                 ouchunkset%chunks(ichunk)%gen%ut,    &
                                 error)
              ! *** JP: Why isn't time integration done as part of simple_waverage?
              ! The following lines assume that the whole spectrum is not bad
              ouchunkset%chunks(ichunk)%gen%time = sinteg
              nphas = ouchunkset%chunks(ichunk)%swi%nphas
              if (nphas.gt.0) then
                 do iphas=1,nphas
                    ouchunkset%chunks(ichunk)%swi%duree(iphas) = sinteg
                 enddo ! iphas
              endif
              ! *** JP: End of question.
           enddo ! ichunk
        enddo ! iset
     enddo ! ipix
  enddo ! itime
end subroutine mrtcal_average_time_range
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_average_channels(chunkset_2d,error)
  use gildas_def
  use gbl_message
  use gkernel_interfaces
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this => mrtcal_average_channels
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(chunkset_2d_t), intent(inout) :: chunkset_2d
  logical,             intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='AVERAGE>CHANNELS'
  integer(kind=4) :: ipix,iset,ichunk
  integer(kind=size_length) :: nchan
  type(chunkset_t), pointer :: chunkset
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  do ipix=1,chunkset_2d%npix
     do iset=1,chunkset_2d%nset
        chunkset => chunkset_2d%chunkset(iset,ipix)
        do ichunk=1,chunkset%n
           ! Fill 1 continuum value per chunk
           nchan = chunkset%chunks(ichunk)%ndata
           call gr4_mean(chunkset%chunks(ichunk)%data1,  &
                         nchan,                          &
                         bad,0.,                         &
                         chunkset%chunks(ichunk)%cont1)
           if (error)  return
        enddo ! ichunk
     enddo ! iset
  enddo ! ipix
  !
end subroutine mrtcal_average_channels
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_compute_opacity_corr(tauzen,current,expatau,error)
  use gbl_message
  use phys_const
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_compute_opacity_corr
  !---------------------------------------------------------------------
  ! @ private
  ! Compute the opacity correction for all the chunks of a chunkset_2d
  !---------------------------------------------------------------------
  type(chunkset_2d_t), intent(in)    :: tauzen
  type(chunkset_2d_t), intent(in)    :: current
  type(chunkset_2d_t), intent(inout) :: expatau
  logical,             intent(inout) :: error
  ! Local
  integer(kind=4) :: ipix,iset,ichunk
  character(len=*), parameter :: rname='COMPUTE>OPACITY>CORR'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
! *** JP debugging message
!print *,"current",current%corr%el*deg_per_rad,current%corr%airmass
!
  call chunkset_2d_consistency(rname,tauzen,current,error)
  if (error) return
  ! Clone expatau from tauzen + duplicate its header (in particular
  ! the expatau%...%chunk%gen%tau must be set before computing expatau)
  call clone_chunkset_2d(tauzen,expatau,.true.,error)
  if (error) return
  do ipix=1,current%npix
     do iset=1,current%nset
        do ichunk=1,current%chunkset(iset,ipix)%n
           call mrtcal_compute_opacity_corr_data(bad,&
                tauzen%chunkset(iset,ipix)%chunks(ichunk)%ndata,&
                current%corr%airmass,&
                tauzen%chunkset(iset,ipix)%chunks(ichunk)%data1,&
                expatau%chunkset(iset,ipix)%chunks(ichunk)%data1,&
                error)
           if (error) return
           call mrtcal_compute_opacity_corr_head(&
                current%corr%airmass,&
                expatau%chunkset(iset,ipix)%chunks(ichunk),&
                error)
           if (error) return
        enddo ! ichunk
     enddo ! iset
  enddo ! ipix
  !
end subroutine mrtcal_compute_opacity_corr
!
subroutine mrtcal_compute_opacity_corr_data(bad,nchan,airmass,tauzen,expatau,error)
  use gbl_message
  use mrtcal_interfaces, except_this=>mrtcal_compute_opacity_corr_data
  !---------------------------------------------------------------------
  ! @ private
  ! Compute the opacity correction, i.e., exp( airmass * tauzen ), as
  ! a function of the frequency
  !---------------------------------------------------------------------
  real(kind=4),    intent(in)    :: bad
  integer(kind=4), intent(in)    :: nchan
  real(kind=4),    intent(in)    :: airmass
  real(kind=4),    intent(in)    :: tauzen(nchan)
  real(kind=4),    intent(out)   :: expatau(nchan)
  logical,         intent(inout) :: error
  ! Local
  integer(kind=4) :: ichan
  character(len=*), parameter :: rname='COMPUTE>OPACITY>CORR>DATA'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  do ichan=1,nchan
     if (tauzen(ichan).ne.bad) then
        expatau(ichan) = exp(airmass*tauzen(ichan))
     else
        expatau(ichan) = bad
     endif
  enddo ! ichan
  !
end subroutine mrtcal_compute_opacity_corr_data
!
subroutine mrtcal_compute_opacity_corr_head(airmass,chunk,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_compute_opacity_corr_head
  !---------------------------------------------------------------------
  ! @ private
  ! Compute the opacity correction, i.e., exp( airmass * tauzen )
  ! *** JP it could be some kind of average or median value
  !---------------------------------------------------------------------
  real(kind=4),  intent(in)    :: airmass
  type(chunk_t), intent(inout) :: chunk
  logical,       intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='COMPUTE>OPACITY>CORR>HEAD'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  chunk%user%airmass = airmass
  if (chunk%gen%tau.ne.bad) then
     chunk%user%expatau = exp(airmass*chunk%gen%tau)
  else
     chunk%user%expatau = bad
  endif
  !
end subroutine mrtcal_compute_opacity_corr_head
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_on_minus_off(isotfmap,isfsw,backsci,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_on_minus_off
  !---------------------------------------------------------------------
  ! @ private
  ! Compute ON-OFF and update the associated header
  !---------------------------------------------------------------------
  logical,                         intent(in)    :: isotfmap
  logical,                         intent(in)    :: isfsw
  type(science_backend_t), target, intent(inout) :: backsci
  logical,                         intent(inout) :: error
  ! Local
  integer(kind=4) :: ipix,iset,ichunk,ion,ioff,ndiff,idiff
  type(chunkset_t), pointer :: oncurr,offcurr,diffcurr
  character(len=*), parameter :: rname='ON>MINUS>OFF'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call chunkset_2d_consistency(rname,backsci%on%curr,backsci%off%curr,error)
  if (error) return
  if (isfsw) then
    ! Each phase is the OFF of the other one. We store (PHASE1-PHASE2)/PHASE2
    ! and (PHASE2-PHASE1)/PHASE1. They will be stitched and "folded" at write
    ! time.
    ndiff = 2
  else
    ! We store (ON-OFF)/OFF
    ndiff = 1
  endif
  backsci%diff%kind = ckind_onoff
  call clone_chunkset_3d(backsci%on%curr,ndiff,backsci%diff,.true.,error)
  if (error) return
  call mrtcal_chunkset_3d_init_data(backsci%diff,0.,0.,0.,error)
  if (error) return
  !
  if (backsci%switch%cycle%mode.eq.switchmode_pos) then
     ! Just a convention in PSW case.
     ion = 1
     ioff = 2
  else
     ! Depends on the data in WSW or FSW cases.
     ion = backsci%switch%cycle%ion
     ioff = backsci%switch%cycle%ioff
  endif
  !
  do idiff=1,ndiff
    do ipix=1,backsci%diff%npix
      do iset=1,backsci%diff%nset
        if (idiff.eq.1) then
          oncurr => backsci%on%curr%chunkset(iset,ipix)
          offcurr => backsci%off%curr%chunkset(iset,ipix)
        else
          offcurr => backsci%on%curr%chunkset(iset,ipix)
          oncurr => backsci%off%curr%chunkset(iset,ipix)
        endif
        diffcurr => backsci%diff%chunkset(iset,ipix,idiff)
        do ichunk=1,diffcurr%n
          call mrtcal_on_minus_off_head(&
                isotfmap,&
                ion,ioff,&
                oncurr%chunks(ichunk),&
                offcurr%chunks(ichunk),&
                diffcurr%chunks(ichunk),&
                error)
          if (error)  return
          call mrtcal_on_minus_off_data_auto(bad,&
                diffcurr%chunks(ichunk)%ndata,&
                oncurr%chunks(ichunk)%data1,&
                offcurr%chunks(ichunk)%data1,&
                diffcurr%chunks(ichunk)%data1)
        enddo ! ichunk
      enddo ! iset
    enddo ! ipix
  enddo ! idiff
  !
end subroutine mrtcal_on_minus_off

subroutine mrtcal_on_minus_off_new(isotfmap,isfsw,backcal,backsci,error)
  use gbl_message
  use imbfits_types
  use mrtcal_calib_types
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_on_minus_off_new
  !---------------------------------------------------------------------
  ! @ private
  ! Compute ON-OFF and update the associated header
  !---------------------------------------------------------------------
  logical,                         intent(in)    :: isotfmap
  logical,                         intent(in)    :: isfsw
  type(calib_backend_t),   target, intent(in)    :: backcal
  type(science_backend_t), target, intent(inout) :: backsci
  logical,                         intent(inout) :: error
  ! Local
  integer(kind=4) :: idiff,ipix,istokes,ichunk,ion,ioff
  integer(kind=4) :: ndiff
  type(stokesset_book_t) :: book
  type(chunkset_t), pointer :: on,off,diff
  type(chunkset_t), pointer :: auto1,auto2,real,imag,cos,sin
  character(len=message_length) :: mess
  character(len=*), parameter :: rname='ON>MINUS>OFF>NEW'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call chunkset_2d_consistency(rname,backsci%on%curr,backsci%off%curr,error)
  if (error) return
  if (isfsw) then
    ! Each phase is the OFF of the other one. We store (PHASE1-PHASE2)/PHASE2
    ! and (PHASE2-PHASE1)/PHASE1. They will be stitched and "folded" at write
    ! time.
    ndiff = 2
  else
    ! We store (ON-OFF)/OFF
    ndiff = 1
  endif
  backsci%diff%kind = ckind_onoff
  call clone_chunkset_3d(backsci%on%curr,ndiff,backsci%diff,.true.,error)
  if (error) return
  call mrtcal_chunkset_3d_init_data(backsci%diff,0.,0.,0.,error)
  if (error) return
  !
  ion = backsci%switch%cycle%ion
  ioff = backsci%switch%cycle%ioff
  !
  do idiff=1,ndiff
     do ipix=1,backsci%diff%npix
        call imbfits_init_stokesloop(book,error)
        if (error) return
        do while (.true.)
           call imbfits_get_next_stokesset(backsci%chunksetlist,book,error)
           if (error) return
           if (.not.book%found) exit
           if ((book%nstokes.ne.1).and.(book%nstokes.ne.4)) then
              write(mess,'(a,i0,a)') &
                   'Unknown number of Stokes measures: ',&
                   book%nstokes,' (expected: 1 or 4)'
              call mrtcal_message(seve%e,rname,mess)
              error = .true.
              return
           endif
           if (book%nstokes.eq.4) then
             if (.not.backcal%polar) then
               call mrtcal_message(seve%e,rname,'Attempt to calibrate a '//  &
                 'polarimetric science scan with a non-polarimetric calibration scan')
               error = .true.
               return
             endif
             cos => backcal%sincos%chunkset(book%iset(code_r),ipix)
             sin => backcal%sincos%chunkset(book%iset(code_i),ipix)
           endif
           do istokes=1,book%nstokes
              if (idiff.eq.1) then
                 on  => backsci%on%curr%chunkset(book%iset(istokes),ipix)
                 off => backsci%off%curr%chunkset(book%iset(istokes),ipix)
              else
                 off => backsci%on%curr%chunkset(book%iset(istokes),ipix)
                 on  => backsci%off%curr%chunkset(book%iset(istokes),ipix)
              endif
              diff => backsci%diff%chunkset(book%iset(istokes),ipix,idiff)
              do ichunk=1,diff%n
                 call mrtcal_on_minus_off_head(&
                      isotfmap,&
                      ion,ioff,&
                      on%chunks(ichunk),&
                      off%chunks(ichunk),&
                      diff%chunks(ichunk),&
                      error)
                 if (error)  return
                 if (istokes.le.2) then
                    call mrtcal_on_minus_off_data_auto(bad,&
                         diff%chunks(ichunk)%ndata,&
                         on%chunks(ichunk)%data1,&
                         off%chunks(ichunk)%data1,&
                         diff%chunks(ichunk)%data1)
                 else
                    ! Amplitude calibration
                    auto1 => backsci%off%curr%chunkset(book%iset(code_h),ipix)
                    auto2 => backsci%off%curr%chunkset(book%iset(code_v),ipix)
                    call mrtcal_on_minus_off_data_cross(bad,&
                         diff%chunks(ichunk)%ndata,&
                         on%chunks(ichunk)%data1,&
                         off%chunks(ichunk)%data1,&
                         auto1%chunks(ichunk)%data1,&
                         auto2%chunks(ichunk)%data1,&
                         diff%chunks(ichunk)%data1)
                    ! Phase calibration on difference
                    real => backsci%diff%chunkset(book%iset(code_r),ipix,idiff)
                    imag => backsci%diff%chunkset(book%iset(code_i),ipix,idiff)
                    call mrtcal_chunk_unrotate(&
                         cos%chunks(ichunk),&
                         sin%chunks(ichunk),&
                         real%chunks(ichunk),&
                         imag%chunks(ichunk),&
                         error)
                    if (error) return
                 endif
              enddo ! ichunk
           enddo ! istokes
        enddo ! istokesset
     enddo ! ipix
  enddo ! idiff
  !
end subroutine mrtcal_on_minus_off_new
!
subroutine mrtcal_on_minus_off_head(isotfmap,ion,ioff,on,off,diff,error)
  use gbl_message
  use gildas_def
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_on_minus_off_head
  !---------------------------------------------------------------------
  ! @ private
  ! Update the ON-OFF time and noise
  !---------------------------------------------------------------------
  logical,         intent(in)    :: isotfmap
  integer(kind=4), intent(in)    :: ion
  integer(kind=4), intent(in)    :: ioff
  type(chunk_t),   intent(in)    :: on
  type(chunk_t),   intent(in)    :: off
  type(chunk_t),   intent(inout) :: diff
  logical,         intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='ON>MINUS>OFF>HEAD'
  real(kind=8) :: oninteg,offinteg,diffinteg
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  if (isotfmap) then
     diff%gen%time = on%gen%time
     diff%mjd      = on%mjd
     diff%gen%dobs = on%gen%dobs
     diff%gen%ut   = on%gen%ut
  else
     oninteg = on%gen%time
     offinteg = off%gen%time
     diffinteg = oninteg+offinteg
     diff%gen%time = diffinteg
     diff%mjd = (on%mjd*oninteg+off%mjd*offinteg)/diffinteg
     call gag_mjd2gagut(diff%mjd,diff%gen%dobs,diff%gen%ut,error)
     if (error)  return
  endif
  diff%user%noise = sqrt(on%user%noise**2+off%user%noise**2)
  diff%swi%nphas = on%swi%nphas+off%swi%nphas
  !
  if ((ion.le.0).or.(ioff.le.0)) then
!!$     *** JP Currently the following is only true for WSW and FSW. Not yet for PSW.
!!$     call mrtcal_message(seve%e,rname,'Negative or zero valued ION or IOFF')
!!$     error = .true.
     return
  endif
  diff%swi%poids(ion) = on%swi%poids(1)
  diff%swi%poids(ioff) = off%swi%poids(1)
  diff%swi%duree(ion) = on%swi%duree(1)
  diff%swi%duree(ioff) = off%swi%duree(1)
  diff%swi%decal(ion) = on%swi%decal(1)
  diff%swi%decal(ioff) = off%swi%decal(1)
  diff%swi%ldecal(ion) = on%swi%ldecal(1)
  diff%swi%ldecal(ioff) = off%swi%ldecal(1)
  diff%swi%bdecal(ion) = on%swi%bdecal(1)
  diff%swi%bdecal(ioff) = off%swi%bdecal(1)
  !
end subroutine mrtcal_on_minus_off_head
!
subroutine mrtcal_on_minus_off_data_auto(bad,nchan,on,off,diff)
  use gbl_message
  use gildas_def
  use mrtcal_interfaces, except_this=>mrtcal_on_minus_off_data_auto
  !---------------------------------------------------------------------
  ! @ private
  ! Compute:  ON/OFF - 1
  !---------------------------------------------------------------------
  real(kind=4),    intent(in)  :: bad
  integer(kind=4), intent(in)  :: nchan
  real(kind=4),    intent(in)  :: on(nchan)
  real(kind=4),    intent(in)  :: off(nchan)
  real(kind=4),    intent(out) :: diff(nchan)
  ! Local
  integer(kind=4) :: ichan
  character(len=*), parameter :: rname='ON>MINUS>OFF>DATA>AUTO'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  do ichan=1,nchan
     if ((on(ichan).ne.bad).and.(off(ichan).ne.bad).and.(off(ichan).ne.0.0)) then
        diff(ichan) = (on(ichan)/off(ichan))-1.0
     else
        diff(ichan) = bad
     endif
  enddo ! ichan
  !
end subroutine mrtcal_on_minus_off_data_auto
!
subroutine mrtcal_on_minus_off_data_cross(bad,nchan,on,off,auto1,auto2,diff)
  use gbl_message
  use gildas_def
  use mrtcal_interfaces, except_this=>mrtcal_on_minus_off_data_cross
  !---------------------------------------------------------------------
  ! @ private
  ! Compute:  (ON-OFF)/sqrt(AUTO1*AUTO2)
  !---------------------------------------------------------------------
  real(kind=4),    intent(in)  :: bad
  integer(kind=4), intent(in)  :: nchan
  real(kind=4),    intent(in)  :: on(nchan)
  real(kind=4),    intent(in)  :: off(nchan)
  real(kind=4),    intent(in)  :: auto1(nchan)
  real(kind=4),    intent(in)  :: auto2(nchan)
  real(kind=4),    intent(out) :: diff(nchan)
  ! Local
  integer(kind=4) :: ichan
  character(len=*), parameter :: rname='ON>MINUS>OFF>DATA>CROSS'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  do ichan=1,nchan
     ! It is important here not to check that on and off are bad because
     ! values of -1000 are perfectly OK for cross products
     if ((auto1(ichan).ne.bad).and.(auto2(ichan).ne.bad).and.&
          (auto1(ichan).ne.0.0).and.(auto2(ichan).ne.0.0)) then
        diff(ichan) = (on(ichan)-off(ichan))/sqrt(auto1(ichan)*auto2(ichan))
     else
        diff(ichan) = bad
     endif
  enddo ! ichan
  !
end subroutine mrtcal_on_minus_off_data_cross
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_tscale_computation(backcal,backsci,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_tscale_computation
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(calib_backend_t),   intent(in)    :: backcal
  type(science_backend_t), intent(inout) :: backsci
  logical,                 intent(inout) :: error
  ! Local
  integer(kind=4) :: ipix,iset,ichunk
  character(len=*), parameter :: rname='TSCALE>COMPUTATION'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  ! Sanity check
  call chunkset_2d_consistency(rname,backsci%on%curr,backcal%atsys,error)
  if (error) return
  !
  call mrtcal_compute_opacity_corr(&
       backcal%ztau,backsci%on%curr,backsci%expatau,error)
  if (error) return
  !
  call clone_chunkset_2d(backcal%atsys,backsci%tscale,.false.,error)
  if (error)  return
  do ipix=1,backsci%tscale%npix
     do iset=1,backsci%tscale%nset
        do ichunk=1,backsci%tscale%chunkset(iset,ipix)%n
           call mrtcal_chunk_multiply_head(&
                backcal%atsys%chunkset(iset,ipix)%chunks(ichunk),&
                backsci%expatau%chunkset(iset,ipix)%chunks(ichunk),&
                backsci%tscale%chunkset(iset,ipix)%chunks(ichunk),&
                error)
           if (error) return
           call mrtcal_chunk_multiply_data(&
                backcal%atsys%chunkset(iset,ipix)%chunks(ichunk),&
                backsci%expatau%chunkset(iset,ipix)%chunks(ichunk),&
                backsci%tscale%chunkset(iset,ipix)%chunks(ichunk),&
                error)
           if (error) return
        enddo ! ichunk
     enddo ! iset
  enddo ! ipix
  !
end subroutine mrtcal_tscale_computation
!
subroutine mrtcal_tscale_application(tscale,inout,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_tscale_application
  !---------------------------------------------------------------------
  ! @ private
  ! Multiply the data spectra by the tscale spectra
  !---------------------------------------------------------------------
  type(chunkset_2d_t), intent(in)    :: tscale
  type(chunkset_3d_t), intent(inout) :: inout
  logical,             intent(inout) :: error
  ! Local
  integer(kind=4) :: itime,ipix,iset,ichunk
  character(len=*), parameter :: rname='TSCALE>APPLICATION'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  ! Sanity check
  call chunkset_2d_consistency(rname,tscale,inout,error)
  if (error) return
  !
  do itime=1,inout%ntime
    do ipix=1,inout%npix
      do iset=1,inout%nset
        do ichunk=1,inout%chunkset(iset,ipix,itime)%n
          call mrtcal_chunk_self_multiply(&
            tscale%chunkset(iset,ipix)%chunks(ichunk),&
            inout%chunkset(iset,ipix,itime)%chunks(ichunk),&
            error)
          if (error) return
        enddo ! ichunk
      enddo ! iset
    enddo ! ipix
  enddo ! itime
  !
end subroutine mrtcal_tscale_application
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_fill_switch_section(cycle,error)
  use gbl_message
  use gbl_constant
  use mrtcal_interfaces, except_this=>mrtcal_fill_switch_section
  use mrtcal_calib_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(switch_cycle_t), intent(inout) :: cycle
  logical,              intent(inout) :: error
  ! Local
  integer(kind=4) :: ipha
  character(len=*), parameter :: rname='FILL>SWITCH>SECTION'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  if ((cycle%ndata.ne.cycle%npha).or.(cycle%ndesc.ne.cycle%npha)) then
     call mrtcal_message(seve%e,rname,  &
          'Programming error: Cycle structure have inconsistent number of filled elements')
     print *,"Goals: cycle%npha = ",cycle%npha
     print *,"Filled data: cycle%ndata = ",cycle%ndata
     print *,"Filled desc: cycle%ndesc = ",cycle%ndesc
     error = .true.
     return
  endif
  do ipha=1,cycle%npha
     call mrtcal_chunkset_2d_swi_fill(cycle%mode,cycle%desc(ipha),cycle%data(ipha),error)
     if (error) return
  enddo ! ipha
  !
end subroutine mrtcal_fill_switch_section
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
