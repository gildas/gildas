subroutine mrtcal_entry_chopperset2calsec(backcal,calsec,error)
  use gbl_message
  use mrtcal_interfaces, except_this=>mrtcal_entry_chopperset2calsec
  use mrtcal_calib_types
  use mrtindex_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(calib_backend_t), intent(in)    :: backcal  !
  type(sec_calib_t),     intent(inout) :: calsec   !
  logical,               intent(inout) :: error    !
  ! Local
  character(len=*), parameter :: rname='ENTRY>CHOPPERSET2CALSEC'
  integer(kind=4) :: nfreq,mfreq,is,ip
  real(kind=8), parameter :: MHz_per_GHz=1d3
  !
  ! No absolute warranty that nfreq is the same for all (iset,ipix)
  ! combinations. Use largest one:
  if (size(backcal%chopperset).gt.0) then
    mfreq = maxval(backcal%chopperset(:,:)%n)
  else
    mfreq = 0  ! for zero-sized backend tables
  endif
  !
  ! No more warning: this is a standard feature (e.g. with VESPA) which
  ! we support with blanks for unused values.
  ! if (any(backcal%chopperset(:,:)%n.ne.mfreq)) then
  !   ! This means that 'nfreq' is not the same everywhere, and that we
  !   ! need an array 'nfreq(nset,npix)'. Damned this makes the section
  !   ! very complicated!
  !   call mrtcal_message(seve%w,rname,'Nfreq is different from one set to another')
  ! endif
  !
  call reallocate_calib_section(mfreq,backcal%nset,backcal%npix,calsec,error)
  if (error)  return
  ! Zero-ify the whole array, because of the possible nfreq/mfreq difference
  ! i.e. there could be unused values
  ! ZZZ subroutine zero_calsec (and NOT nullify_calsec)
  calsec%frontend(:) = ''
  calsec%freq(:,:,:) = 0.d0
  calsec%atsys(:,:,:) = 0.0
  calsec%ztau(:,:,:) = 0.0
  !
  do is=1,backcal%nset
    calsec%frontend(is) = backcal%sky%chunkset(is,1)%chunks(1)%frontend  ! e.g. E0HLI
  enddo
  !
  do ip=1,backcal%npix
    do is=1,backcal%nset
      nfreq = backcal%chopperset(is,ip)%n
      calsec%freq(1:nfreq,is,ip) = backcal%chopperset(is,ip)%freqs(1:nfreq)%s*MHz_per_GHz
      calsec%atsys(1:nfreq,is,ip) = backcal%chopperset(is,ip)%atsyss(1:nfreq)%s
      calsec%ztau(1:nfreq,is,ip) = backcal%chopperset(is,ip)%atms(1:nfreq)%taus%tot
    enddo
  enddo
  !
end subroutine mrtcal_entry_chopperset2calsec
!
subroutine mrtcal_entry_calsec2chopperset(calsec,backcal,error)
  use gbl_message
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_entry_calsec2chopperset
  use mrtcal_calib_types
  use mrtindex_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(sec_calib_t),     intent(in)    :: calsec   !
  type(calib_backend_t), intent(inout) :: backcal  !
  logical,               intent(inout) :: error    !
  ! Local
  character(len=*), parameter :: rname='ENTRY>CALSEC2CHOPPERSET'
  type(chopper_t), pointer :: chopper
  integer(kind=4) :: if,is,ip
  real(kind=8), parameter :: GHz_per_MHz=1d-3
  !
  ! This subroutine assumes backcal%sky is already allocated to
  ! appropriate size. The calibration section remembers the number of
  ! pixels, of sets, and of chunks per set (nfreq), but not more. For
  ! example it does not remember the number of channels for each
  ! individual chunks, nor its frequency axis description.
  !
  call mrtcal_calsec_chunkset_consistency(calsec,backcal%sky,error)
  if (error)  return
  !
  call reallocate_chopperset(backcal%sky,backcal%chopperset,error)
  if (error) return
  ! Save size of chopperset (re)allocation:
  backcal%nset = backcal%sky%nset
  backcal%npix = backcal%sky%npix
  !
  do ip=1,calsec%npix
    do is=1,calsec%nset
      chopper => backcal%chopperset(is,ip)
      do if=1,backcal%sky%chunkset(is,ip)%n  ! Not calsec%nfreq
        ! First nullify everyone (NB: this nullifies only the outputs)
        ! ZZZ Need a telcal subroutine which nullifies everything
        call telcal_chopper_nullify(chopper,if)
        chopper%freqs(if)%s       = calsec%freq(if,is,ip)*GHz_per_MHz
        chopper%atsyss(if)%s      = calsec%atsys(if,is,ip)
        chopper%atms(if)%taus%tot = calsec%ztau(if,is,ip)
      enddo
    enddo
  enddo
  !
end subroutine mrtcal_entry_calsec2chopperset
!
subroutine mrtcal_calsec_chunkset_consistency(calsec,c2d,error)
  use gbl_message
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_calsec_chunkset_consistency
  use mrtcal_calib_types
  use mrtindex_types
  !---------------------------------------------------------------------
  ! @ private
  !  Check the consistency of a chunkset_2d against calibration section
  !---------------------------------------------------------------------
  type(sec_calib_t),   intent(in)    :: calsec
  type(chunkset_2d_t), intent(in)    :: c2d
  logical,             intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CALSEC>CHUNKSET>CONSISTENCY'
  character(len=message_length) :: mess
  real(kind=8) :: freq1,freq2
  integer(kind=4) :: ip,is,if
  type(chunkset_t), pointer :: chunkset
  real(kind=8), parameter :: ftole=1.d-2  ! [Mhz] 10 kHz tolerance
  !
  if (.not.associated(c2d%chunkset)) then
    call mrtcal_message(seve%e,rname,'Programming error: chunkset is not defined')
    error = .true.
    return
  endif
  if (calsec%npix.le.0 .or. calsec%nset.le.0 .or. calsec%nfreq.le.0) then
    call mrtcal_message(seve%e,rname,'Programming error: CAL section is empty')
    error = .true.
    return
  endif
  if (c2d%npix.ne.calsec%npix .or. c2d%nset.ne.calsec%nset) then
    write(mess,'(4(A,I0))')  'Inconsistent CHUNKSET dimensions vs CAL section: ',  &
      c2d%npix,'x',c2d%nset,' vs ',calsec%npix,'x',calsec%nset
    call mrtcal_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  ! Consistency on frontend names
  do is=1,calsec%nset
    ! Compare arbitrarily with the 1st pixel and 1st chunk of this set
    chunkset => c2d%chunkset(is,1)
    if (calsec%frontend(is).ne.chunkset%chunks(1)%frontend) then
      write(mess,'(3(A,1X))')  'Inconsistent frontends:',  &
        calsec%frontend(is),chunkset%chunks(1)%frontend
      call mrtcal_message(seve%e,rname,mess)
      error = .true.
      return
    endif
  enddo
  !
  ! Consistency at the chunk level
  do ip=1,calsec%npix
    do is=1,calsec%nset
      chunkset => c2d%chunkset(is,ip)
      if (calsec%nfreq.lt.chunkset%n) then
        write(mess,'(4(A,I0))')  'Inconsistent number of chunks vs CAL section: ',  &
          chunkset%n,' vs ',calsec%nfreq
        call mrtcal_message(seve%e,rname,mess)
        error = .true.
        return
      endif
      do if=1,calsec%nfreq
        freq1 = calsec%freq(if,is,ip)
        ! For some backends (e.g. VESPA) the number or chunks can vary from one
        ! set to another. Unused frequencies are saved as 0.d0 in the section.
        if ((freq1.eq.0.d0 .and. if.le.chunkset%n) .or.  &
            (freq1.ne.0.d0 .and. if.gt.chunkset%n)) then
          call mrtcal_message(seve%e,rname,'Inconsistent SKY chunks vs CAL section')
          error = .true.
          return
        endif
        if (if.gt.chunkset%n)  cycle  ! Unused frequency, skip
        !
        ! Consistency on frequencies:
        ! *** JP It should be the frequencies in the observatory, not in the rest frame...
        call abscissa_sigabs_middle(chunkset%chunks(if)%spe,freq2)
        ! Because the section has been duplicated from the science scan,
        ! and because we use rest frequencies, the Doppler factor is
        ! slightly different from the original one in the calibration
        ! scan. Need a tolerance.
        if (abs(freq1-freq2).gt.ftole) then
          write(mess,'(A,2F20.10)') 'Inconsistent frequencies: ',freq1,freq2
          call mrtcal_message(seve%e,rname,mess)
          error = .true.
          return
        endif
      enddo
    enddo
  enddo
  !
end subroutine mrtcal_calsec_chunkset_consistency
