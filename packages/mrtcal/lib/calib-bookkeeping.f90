!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_init_scan_cal(backsci,imbf,error)
  use gbl_message
  use mrtcal_interfaces, except_this => mrtcal_init_scan_cal
  use mrtcal_buffer_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(science_backend_t), intent(inout) :: backsci
  type(imbfits_t),         intent(inout) :: imbf
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='INIT>SCAN>CAL'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  backsci%cumulinit = .true.
  backsci%nspec = 0
  call mrtcal_subscan_list_build(backsci%head%key%switchmode,backsci%list,imbf,error)
  if (error) return
  call mrtcal_subscan_list_print(backsci%head%key%switchmode,backsci%list,error)
  if (error) return
  !
end subroutine mrtcal_init_scan_cal
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_accumulate_cycle(backsci,dowei,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_messaging
  use mrtcal_interfaces, except_this=>mrtcal_accumulate_cycle
  !---------------------------------------------------------------------
  ! @ private
  ! Accumulate last dump after (re)initialization of the accumulation when
  ! requested
  !---------------------------------------------------------------------
  type(science_backend_t), intent(inout) :: backsci
  logical,                 intent(in)    :: dowei
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='ACCUMULATE>CYCLE'
  !
  if (backsci%cumulinit) then
     call mrtcal_message(mseve%output%acc,rname,'Initialize accumulation')
     call mrtcal_chunkset_3d_accumulate_init(backsci%diff,backsci%cumul,error)
     if (error) return
     backsci%cumulinit = .false.
  endif
  call mrtcal_message(mseve%output%acc,rname,'Accumulate')
  call mrtcal_chunkset_3d_accumulate_setweight(backsci%diff,backsci%tscale,  &
    dowei,error)
  if (error) return
  call mrtcal_chunkset_3d_accumulate_do(backsci%diff,backsci%cumul,error)
  if (error) return
  !
end subroutine mrtcal_accumulate_cycle
!
subroutine mrtcal_init_accumulate_or_write(obstype,swmode,iset,oset,error)
  use gbl_message
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_init_accumulate_or_write
  use mrtindex_parameters
  use mrtcal_setup_types
  !---------------------------------------------------------------------
  ! @ private
  ! Set up the accumulation mode according to user's MSET OUTPUT
  ! INTEGRATION choice, and to observing mode in case of automatic
  ! choice.
  !---------------------------------------------------------------------
  integer(kind=4),      intent(in)    :: obstype  ! [code] Observing type
  integer(kind=4),      intent(in)    :: swmode   ! [code] Switching mode
  type(mrtcal_setup_t), intent(in)    :: iset     ! Setup (user choices)
  type(mrtcal_setup_t), intent(inout) :: oset     ! Setup (resolved)
  logical,              intent(inout) :: error    ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='INIT>ACCUMULATE>OR>WRITE'
  !
  oset%out%accmode = accmode_unkn
  !
  select case (obstype)
  case (obstype_tracked)
    select case (swmode)
    case (switchmode_pos)
      ! TRACKED PSW
      select case (iset%out%accmode)
      case (accmode_defa)
        oset%out%accmode = accmode_scan
      case (accmode_cycl,accmode_scan)  ! Reject SUBSCAN mode
        oset%out%accmode = iset%out%accmode
      end select
    case (switchmode_wob,switchmode_fre)
      ! TRACKED WSW or FSW
      select case (iset%out%accmode)
      case (accmode_defa)
        oset%out%accmode = accmode_scan
      case (accmode_cycl,accmode_subs,accmode_scan)
        oset%out%accmode = iset%out%accmode
      end select
    end select
  case (obstype_otf)
    ! OTF PSW or FSW
    ! NB: accumulate will NOT be used in this case
    select case (iset%out%accmode)
    case (accmode_defa)  ! Reject anything other than *
      oset%out%accmode = accmode_cycl
    end select
  case (obstype_pointing)
    ! Pointing BSW or PSW or WSW
    select case (iset%out%accmode)
    case (accmode_defa)
      oset%out%accmode = accmode_subs
    case (accmode_cycl,accmode_subs)  ! Cycle writes the drift points one by one
      oset%out%accmode = iset%out%accmode
    end select
  end select
  !
  if (oset%out%accmode.eq.accmode_unkn) then
    call mrtcal_message(seve%e,rname,  &
      'Unsupported integration mode '//trim(accmodes(iset%out%accmode))//  &
      ' for '//trim(mrtindex_obstype(obstype))//  &
      ' '//mrtindex_swmode(swmode))
    error = .true.
    return
  endif
  !
end subroutine mrtcal_init_accumulate_or_write
!
subroutine mrtcal_write_cycle(setout,backsci,error)
  use gbl_message
  use mrtcal_setup_types
  use mrtcal_calib_types
  use mrtcal_messaging
  use mrtcal_interfaces, except_this=>mrtcal_write_cycle
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(mrtcal_setup_output_t), intent(in)    :: setout
  type(science_backend_t),     intent(inout) :: backsci
  logical,                     intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='WRITE>CYCLE'
  !
  if (setout%accmode.eq.accmode_cycl) then
     call mrtcal_message(mseve%output%acc,rname,'Write, and reset initialization status')
     call mrtcal_write_toclass(backsci%cumul,setout,backsci%nspec,error)
     if (error) return
     backsci%cumulinit = .true.
  endif
  !
end subroutine mrtcal_write_cycle
!
subroutine mrtcal_write_subscan(setout,backsci,error)
  use gbl_message
  use mrtcal_setup_types
  use mrtcal_calib_types
  use mrtcal_messaging
  use mrtcal_interfaces, except_this=>mrtcal_write_subscan
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(mrtcal_setup_output_t), intent(in)    :: setout
  type(science_backend_t),     intent(inout) :: backsci
  logical,                     intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='WRITE>SUBSCAN'
  !
  if (setout%accmode.eq.accmode_subs) then
     call mrtcal_message(mseve%output%acc,rname,'Write, and reset initialization status')
     call mrtcal_write_toclass(backsci%cumul,setout,backsci%nspec,error)
     if (error) return
     backsci%cumulinit = .true.
  endif
  !
end subroutine mrtcal_write_subscan
!
subroutine mrtcal_write_scan(setout,backsci,error)
  use gbl_message
  use mrtcal_setup_types
  use mrtcal_calib_types
  use mrtcal_messaging
  use mrtcal_interfaces, except_this=>mrtcal_write_scan
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(mrtcal_setup_output_t), intent(in)    :: setout
  type(science_backend_t),     intent(inout) :: backsci
  logical,                     intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='WRITE>SCAN'
  !
  if (setout%accmode.eq.accmode_scan) then
     call mrtcal_message(mseve%output%acc,rname,'Write, and reset initialization status')
     call mrtcal_write_toclass(backsci%cumul,setout,backsci%nspec,error)
     if (error) return
     backsci%cumulinit = .true.
  endif
  !
end subroutine mrtcal_write_scan
!
subroutine mrtcal_accumulate_or_write(level_code,mrtset,backsci,error)
  use gbl_message
  use mrtcal_level_codes
  use mrtcal_setup_types
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_accumulate_or_write
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4),         intent(in)    :: level_code
  type(mrtcal_setup_t),    intent(in)    :: mrtset
  type(science_backend_t), intent(inout) :: backsci
  logical,                 intent(inout) :: error
  ! Local
  logical :: dowrite,doaccum
  character(len=message_length) :: mess
  character(len=*), parameter :: rname='ACCUMULATE>OR>WRITE'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  ! ZZZ What about the spectra positions in dump, subscan, or scan mode?
  ! 2 examples:
  !  - in scan mode, should we check that all the ON positions (which
  !    are used as the output spectrum position) are consistant? If not,
  !    should we make an average or so?
  !  - in scan mode, for multi-pixel receiver, the pixel positions vary
  !    on sky during the scan if dewar tracking is not 'sky' (see e.g.
  !    iram30m-wilma-20130320s159-imb.fits). What is the resulting
  !    position?
  !
  select case(mrtset%out%accmode)
  case(accmode_cycl)
     dowrite = .false.
     if (level_code.eq.level_cycl_code) then
        call mrtcal_write_toclass(backsci%diff,mrtset%out,backsci%nspec,error)
        if (error) return
        doaccum = .false.
     else
        doaccum = .true.
     endif
  case(accmode_subs)
     if (level_code.eq.level_cycl_code) then
        dowrite = .false.
        doaccum = .true.
     else
        doaccum = .false.
        if (level_code.eq.level_subs_code) then
           dowrite = .true.
        else
           dowrite = .false.
        endif
     endif
  case(accmode_scan)
     if (level_code.eq.level_cycl_code) then
        dowrite = .false.
        doaccum = .true.
     else
        doaccum = .false.
        if (level_code.eq.level_scan_code) then
           dowrite = .true.
        else
           dowrite = .false.
        endif
     endif
  case default
    write(mess,'(A,I0)') 'Unsupported accumulate mode: ',mrtset%out%accmode
    call mrtcal_message(seve%e,rname,mess)
    error = .true.
    return
  end select
  if (dowrite) then
     backsci%cumulinit = .true.
     call mrtcal_write_toclass(backsci%cumul,mrtset%out,backsci%nspec,error)
     if (error) return
  endif
  if (doaccum) then
     if (backsci%cumulinit) then
        call mrtcal_chunkset_3d_accumulate_init(backsci%diff,backsci%cumul,error)
        if (error) return
        backsci%cumulinit = .false.
     endif
     call mrtcal_chunkset_3d_accumulate_setweight(backsci%diff,backsci%tscale,  &
       mrtset%out%weight,error)
     if (error) return
     call mrtcal_chunkset_3d_accumulate_do(backsci%diff,backsci%cumul,error)
     if (error) return
  endif
  !
end subroutine mrtcal_accumulate_or_write
!
subroutine mrtcal_append_or_write(level_code,mrtset,backsci,error)
  use gbl_message
  use mrtcal_level_codes
  use mrtcal_setup_types
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_append_or_write
  !---------------------------------------------------------------------
  ! @ private
  ! Append chunk_t to the 'cumul' set, or write the 'cumul' set
  !---------------------------------------------------------------------
  integer(kind=4),         intent(in)    :: level_code
  type(mrtcal_setup_t),    intent(in)    :: mrtset
  type(science_backend_t), intent(inout) :: backsci
  logical,                 intent(inout) :: error
  ! Local
  logical :: dowrite,doaccum
  character(len=message_length) :: mess
  character(len=*), parameter :: rname='APPEND>OR>WRITE'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  ! ZZZ What about the spectra positions in dump, subscan, or scan mode?
  ! 2 examples:
  !  - in scan mode, should we check that all the ON positions (which
  !    are used as the output spectrum position) are consistant? If not,
  !    should we make an average or so?
  !  - in scan mode, for multi-pixel receiver, the pixel positions vary
  !    on sky during the scan if dewar tracking is not 'sky' (see e.g.
  !    iram30m-wilma-20130320s159-imb.fits). What is the resulting
  !    position?
  !
  select case(mrtset%out%accmode)
  case(accmode_cycl)
     dowrite = .false.
     if (level_code.eq.level_cycl_code) then
        call mrtcal_write_toclass(backsci%diff,mrtset%out,backsci%nspec,error)
        if (error) return
        doaccum = .false.
     else
        doaccum = .true.
     endif
  case(accmode_subs)
     if (level_code.eq.level_cycl_code) then
        dowrite = .false.
        doaccum = .true.
     else
        doaccum = .false.
        if (level_code.eq.level_subs_code) then
           dowrite = .true.
        else
           dowrite = .false.
        endif
     endif
  case(accmode_scan)
     if (level_code.eq.level_cycl_code) then
        dowrite = .false.
        doaccum = .true.
     else
        doaccum = .false.
        if (level_code.eq.level_scan_code) then
           dowrite = .true.
        else
           dowrite = .false.
        endif
     endif
  case default
    write(mess,'(A,I0)') 'Unsupported accumulate mode: ',mrtset%out%accmode
    call mrtcal_message(seve%e,rname,mess)
    error = .true.
    return
  end select
  if (dowrite) then
     backsci%cumulinit = .true.
     call mrtcal_write_toclass(backsci%cumul,mrtset%out,backsci%nspec,error)
     if (error) return
  endif
  if (doaccum) then
     if (backsci%cumulinit) then
        call mrtcal_chunkset_3d_append_init(backsci%diff,backsci%cumul,error)
        if (error) return
        backsci%cumulinit = .false.
     endif
     ! call mrtcal_chunkset_3d_accumulate_setweight(backsci%diff,backsci%tscale,  &
     !   mrtset%out%weight,error)
     ! if (error) return
     call mrtcal_chunkset_3d_append_do(backsci%diff,backsci%cumul,error)
     if (error) return
  endif
  !
end subroutine mrtcal_append_or_write
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_init_refloop(backsci,nref,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_init_refloop
  !---------------------------------------------------------------------
  ! @ private
  ! Initialize the loop on REF subscans, i.e., number of REF.
  ! Enforce initialization of the cumulated chunkset_2d structure if and when needed
  !---------------------------------------------------------------------
  type(science_backend_t), intent(inout) :: backsci
  integer(kind=4),         intent(out)   :: nref
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='INIT>REFLOOP'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  ! *** JP not at its right place to be generic...
  backsci%list%on = 0
  nref = backsci%list%onoff%nequ
  !
end subroutine mrtcal_init_refloop
!
subroutine mrtcal_get_nextref(backsci,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_get_nextref
  !---------------------------------------------------------------------
  ! @ private
  ! Update the REF number
  !---------------------------------------------------------------------
  type(science_backend_t), intent(inout) :: backsci
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='GET>NEXTREF'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  backsci%list%on = backsci%list%on+1
  !
end subroutine mrtcal_get_nextref
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_init_onloop(backsci,non,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_init_onloop
  !---------------------------------------------------------------------
  ! @ private
  ! Initialize the loop on ON subscans, i.e., number of ON, current on and
  ! OFF subscan numbers
  !---------------------------------------------------------------------
  type(science_backend_t), intent(inout) :: backsci
  integer(kind=4),         intent(out)   :: non
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='INIT>ONLOOP'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  if ((backsci%list%on.le.0).or.(backsci%list%off.gt.backsci%list%onoff%nequ)) then
     call mrtcal_message(seve%e,rname,'ON kind is outside the number of subscan types')
     error = .true.
     return
  endif
  non = backsci%list%onoff%cnt(backsci%list%on)
  backsci%on%curr%isub = 0
  backsci%off%stack(:)%isub = 0
  !
  ! *** JP Unclear whether this should be done here because the
  ! *** JP switchmode could or shouldn't change between subscans
  backsci%switch%cycle%mode = backsci%head%key%switchmode
  !
end subroutine mrtcal_init_onloop
!
subroutine mrtcal_get_nexton(backsci,error)
  use gkernel_types
  use imbfits_types
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_get_nexton
  use mrtcal_calib_types
  use mrtcal_messaging
  !---------------------------------------------------------------------
  ! @ private
  ! Get in the current list of subscans the number of next ON subscan
  !---------------------------------------------------------------------
  type(science_backend_t), intent(inout) :: backsci
  logical,                 intent(inout) :: error
  ! Local
  logical :: found
  integer(kind=4) :: inext
  character(len=message_length) :: mess
  character(len=*), parameter :: rname='GET>NEXTON'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  inext = backsci%on%curr%isub
  call eclass_getnext(backsci%list%onoff,backsci%list%on,inext,found,error)
  if (error.or..not.found) return
  ! Use a back index to take care of the fact that the list of subscan was sorted
  ! by increasing time
  backsci%on%curr%isub = backsci%list%isub(inext)
  write(mess,'(A,I0,A,I0)') 'Processing subscan #',backsci%on%curr%isub,&
       '/',backsci%list%onoff%cnt(backsci%list%on)
  call mrtcal_message(mseve%calib%book,rname,mess)
  !
end subroutine mrtcal_get_nexton
!
subroutine mrtcal_sanity_check(backsci,error)
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_sanity_check
  use mrtcal_calib_types
  use mrtcal_messaging
  !---------------------------------------------------------------------
  ! @ private
  ! Check whether ON and OFF subscans are red and give user feedback
  ! about the next processing step
  !---------------------------------------------------------------------
  type(science_backend_t), intent(inout) :: backsci
  logical,                 intent(inout) :: error
  !
  character(len=message_length) :: mess
  character(len=*), parameter :: rname='SANITY>CHECK'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  select case (backsci%head%key%switchmode)
  case (switchmode_pos)
     if (associated(backsci%off%curr)) then
        if ((backsci%on%curr%isub.ne.0).and.(backsci%off%curr%isub.ne.0)) then
           ! *** JP Should we give here feedback about the interpolation method 
           ! *** JP (linear or nearest neighbour)?
           write(mess,'(A,I0,A,I0,A,I0)')  &
                'Processing ON #',backsci%on%curr%isub,' surrounded by OFF #',&
                backsci%off%prev%isub,' and ',backsci%off%next%isub
           call mrtcal_message(mseve%calib%book,rname,mess)
        else
           if (backsci%on%curr%isub.eq.0) then
              call mrtcal_message(seve%e,rname,'No current ON')
           elseif (backsci%off%curr%isub.eq.0) then
              call mrtcal_message(seve%e,rname,'No current OFF')
           endif
           error = .true.
           return
        endif
     else
        call mrtcal_message(seve%e,rname,'Current OFF position pointer is not associated')
        error = .true.
        return     
     endif
  case (switchmode_wob,switchmode_fre,switchmode_bea)
     if (backsci%on%curr%isub.ne.0) then
        write(mess,'(A,I0,A,I0)') 'Processing REF #',backsci%list%on,', SUBSCAN #',backsci%on%curr%isub
        call mrtcal_message(mseve%calib%book,rname,mess)
     else
        call mrtcal_message(seve%e,rname,'No subscan')
        error = .true.
        return
     endif
  case default
     call mrtcal_message(seve%e,rname,  &
          'Unsupported switched mode '//mrtindex_swmode(backsci%head%key%switchmode))
     error = .true.
     return
  end select
  !
end subroutine mrtcal_sanity_check
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_init_dumpcycle_loop(name,mrtset,filebuf,backsci,error)
  use gbl_message
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_init_dumpcycle_loop
  use mrtcal_buffer_types
  use mrtcal_setup_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*),        intent(in)    :: name
  type(mrtcal_setup_t),    intent(in)    :: mrtset
  type(imbfits_buffer_t),  intent(inout) :: filebuf
  type(science_backend_t), intent(inout) :: backsci
  logical,                 intent(inout) :: error
  ! 
  character(len=*), parameter :: rname='INIT>DUMPCYCLE>LOOP'
  integer(kind=4) :: isub
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_sanity_check(backsci,error)
  if (error) return
  isub = backsci%on%curr%isub
  call imbfits_read_subscan_header_bynum(&
       filebuf%imbf,isub,&
       .not.mrtset%cal%bad,&
       mrtset%cal%mjdinter,&
       mrtset%cal%antslowmjdshift,&
       filebuf%subscanbuf%subscan,&
       error)
  if (error) goto 10
  call mrtcal_check_substype(isub,&
       name,&
       filebuf%subscanbuf%subscan,&
       error)
  if (error) goto 10
  call mrtcal_get_time_range_for_backend(&
       filebuf%subscanbuf%subscan,&
       filebuf%subscanbuf%databuf%time%tot,&
       error)
  if (error) goto 10
  call mrtcal_bookkeeping_init_time(&
       mrtset%bufsize,&
       filebuf%subscanbuf%subscan,&
       filebuf%subscanbuf%databuf%time,&
       error)
  if (error) goto 10
  call mrtcal_switch_cycle_init(&
       filebuf%imbf%front,&
       filebuf%subscanbuf%subscan%backdata,&
       filebuf%subscanbuf%subscan%antslow,&
       backsci%list,&
       backsci%switch%cycle,error)
  if (error) goto 10
  call mrtcal_switch_cycle_list(backsci%switch%cycle,error)
  if (error) goto 10
  call mrtcal_init_dumpcycle_book(&
       filebuf%subscanbuf%databuf%time,&
       backsci%switch,error)
  if (error) goto 10
  !
10 continue
  if (failed_calibrate(rname,backsci%on%curr%isub,error)) return
  !
end subroutine mrtcal_init_dumpcycle_loop
!
subroutine mrtcal_init_dumpcycle_book(time,switch,error)
  use gbl_message
  use mrtcal_interfaces, except_this=>mrtcal_init_dumpcycle_book
  use mrtcal_buffer_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(book_t),   intent(in)    :: time
  type(switch_t), intent(inout) :: switch
  logical,        intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='INIT>DUMPCYCLE>BOOK'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  if (switch%cycle%npha.le.0) then
     call mrtcal_message(seve%e,rname,'Negative or zero valued NPHASES')    
     error = .true.
     return
  else if (switch%cycle%npha.gt.2) then
     call mrtcal_message(seve%e,rname,'More than two phases is currently unsupported')    
     error = .true.
     return
  else
     switch%book%nphase = switch%cycle%ndump
     switch%book%ncycle = 0
     switch%book%ndump = time%tot%n
     switch%book%izero = time%tot%first-1
     switch%book%norphan = 0
     switch%book%idump = 0
     switch%book%ifirst = 1-switch%book%nphase
  endif
  !
end subroutine mrtcal_init_dumpcycle_book
!
subroutine mrtcal_get_next_dumpcycle(mrtset,filebuf,backsci,error)
  use gbl_message
  use mrtcal_buffer_types
  use mrtcal_setup_types
  use mrtcal_interfaces, except_this=>mrtcal_get_next_dumpcycle
  !---------------------------------------------------------------------
  ! @ private
  ! Search for and then read next cycle
  !---------------------------------------------------------------------
  type(mrtcal_setup_t),    intent(in)    :: mrtset
  type(imbfits_buffer_t),  intent(inout) :: filebuf
  type(science_backend_t), intent(inout) :: backsci
  logical,                 intent(inout) :: error
  !
  character(len=*), parameter :: rname='GET>NEXT>DUMPCYCLE'
  !  
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_find_next_dumpcycle(&
       filebuf%subscanbuf%subscan%backdata,&
       backsci%switch%book,error)
  if (error) return
  call mrtcal_read_next_dumpcycle(mrtset,&
       filebuf%imbf,&
       filebuf%subscanbuf%subscan,&
       filebuf%subscanbuf%databuf,&
       backsci%switch,&
       error)
  if (error) return
  if (backsci%switch%book%found) then
     call mrtcal_fill_switch_section(backsci%switch%cycle,error)
     if (error) return
     call reassociate_chunkset_2d(backsci%switch%cycle%data(backsci%switch%cycle%ion),backsci%on%curr,error)
     if (error)  return
     backsci%off%curr => backsci%switch%cycle%data(backsci%switch%cycle%ioff)
  endif
end subroutine mrtcal_get_next_dumpcycle
!
subroutine mrtcal_find_next_dumpcycle(backdata,book,error)
  use gbl_message
  use imbfits_types
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_find_next_dumpcycle
  !---------------------------------------------------------------------
  ! @ private
  ! Search for cycle phases in increasing order and return when found.
  ! If the cycle is incomplete, try to find the next cycle.
  ! Return with found = .false. when it tries to read past the last dump
  ! of the subscan.
  ! Caution: Modifying this code can lead to infinite loops...
  !---------------------------------------------------------------------
  type(imbfits_backdata_t), intent(in)    :: backdata
  type(switch_book_t),      intent(inout) :: book
  logical,                  intent(inout) :: error
  ! Local
  logical :: notyetwarned,match
  integer(kind=4) :: iphase,istream
  character(len=message_length) :: mess
  character(len=*), parameter :: rname='FIND>NEXT>DUMPCYCLE'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  ! Iterate internal counters
  book%idump = book%idump+1
  book%ifirst = book%ifirst+book%nphase
  ! Initialize next cycle search
  iphase = 1
  notyetwarned = .true.
  do
     if (book%idump.gt.book%ndump) then
        if (iphase.gt.1) then
           ! That's just a warning, *not* an error
           call mrtcal_message(seve%w,rname,'Incomplete last switch cycle')
        endif
        book%found = .false.
        book%ncycle = book%ncycle+1
        return
     endif
     istream = book%idump+book%izero
     if (istream.gt.backdata%table%iswitch%n) then
        ! Something is wrong. This should not happen.
        write(mess,'(a,i0,a,i0,a)') 'Trying to read ',istream,&
          ', i.e., outside the last dump time (1,',backdata%table%iswitch%n,')'
        error = .true.
        return
     endif
     ! Check phase number
     match = backdata%table%iswitch%val(istream).eq.iphase
     if (match .and. iphase.gt.1) then
       ! Check time contiguity in addition. At this stage we may have found e.g.
       ! a sequence 1 2 3 4, but how to be sure that the streams are not missing
       ! some dumps, e.g. 1 2 [3 4 1 2] 3 4? Note that dumps may be missing if:
       ! 1) they have been compressed out at read time (MSET CALIB BAD NO)
       ! 2) they are just absent from the IMBFITS
       ! Assuming that:
       ! 1) phase integration times are identical (to a given precision).
       ! 2) NCS does not have dead times when integrating, i.e. it is not
       !    normal to have (large) gaps betwen dumps and we should not pair
       !    phases separated by such gaps.
       ! In practice, tolerate a gap up to 50% of the dump integration time,
       ! i.e. the next dump should not be further than 1.5 integration time.
       ! NB: iphase>1 => istream-1 can not be 0
       match = (backdata%table%mjd%val(istream)-backdata%table%mjd%val(istream-1))*8.64d4  &
               .lt.  &
               1.5d0*backdata%table%integtim%val(istream)
       match = .true.
       if (.not.match) then
         write(mess,'(A,I0,A,I0,A,F0.3,A,F0.3,A)')  &
         'Dumps ',istream-1,' and ',istream,' are separated by ',  &
         (backdata%table%mjd%val(istream)-backdata%table%mjd%val(istream-1))*8.64d4,  &
         ' > 1.5 x ',backdata%table%integtim%val(istream),' => skip spurious cycle'
         call mrtcal_message(seve%w,rname,mess)
       endif
     endif
     if (match) then
        notyetwarned = .true.
        ! Expected phase number
        if (iphase.eq.book%nphase) then
          ! Completed a full cycle
          book%found = .true.
          book%ncycle = book%ncycle+1
          ! write(mess,'(A,I0,A,I0)') 'Found next cycle starting at ',book%ifirst,', i.e., ',book%ifirst+book%izero
          ! call mrtcal_message(seve%i,rname,mess)
          return
        else
          ! Not yet a full cycle => Increment phase and stream counters
          iphase = iphase+1
          book%idump = book%idump+1
        endif
     else
        ! Unexpected phase number => Warn only once while searching for a new start of a complete cycle
        if (notyetwarned) then
          write(mess,'(A,I0,A,I0,A)') 'Incomplete or inconsistent switch cycle starting at ',&
                book%ifirst,', i.e., ',book%ifirst+book%izero,' => Searching for start of next cycle'
          call mrtcal_message(seve%w,rname,mess)
          notyetwarned = .false.
        endif
        write(mess,'(A,I0,A,I0,A,I0,A)') 'Dump #',book%idump,&
            ' has wrong switch type (',&
            backdata%table%iswitch%val(book%idump+book%izero),&
            ' vs ',iphase,')'
        call mrtcal_message(seve%w,rname,mess)
        if (iphase.gt.1) then
          ! We were in the middle of a cycle
          !   => Reset phase number to find start of next cycle
          ! The current dump could be the start of this cycle
          !   => Do not increase the position counter in the dump stream
          iphase = 1
        else
          ! Already searching for start of cycle
          !   => Just increase the position counter in the dump stream
          book%idump = book%idump+1
          book%norphan = book%norphan+1
        endif
        book%ifirst = book%idump
     endif ! Is the data phase number the expected one?
  enddo ! Infinite loop
end subroutine mrtcal_find_next_dumpcycle
!
subroutine mrtcal_read_next_dumpcycle(setup,imbf,subshead,databuf,switch,error)
  use gbl_message
  use imbfits_types
  use mrtcal_setup_types
  use mrtcal_buffer_types
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_read_next_dumpcycle
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(mrtcal_setup_t),    intent(in)    :: setup
  type(imbfits_t),         intent(in)    :: imbf
  type(imbfits_subscan_t), intent(in)    :: subshead
  type(data_buffer_t),     intent(inout) :: databuf
  type(switch_t),          intent(inout) :: switch
  logical,                 intent(inout) :: error
  ! Local
  integer(kind=4) :: iphase
  character(len=*), parameter :: rname='READ>NEXT>DUMPCYCLE'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  if (.not.switch%book%found) return
  call mrtcal_update_databuf(setup,imbf,subshead,switch%book,databuf,error)
  if (error) return
  do iphase=1,switch%book%nphase
     call mrtcal_fill_dumpphase(iphase,subshead,databuf,switch%book,&
          switch%cycle%data(iphase),error)
     if (error) return
     ! Success => Update the ndata counter.
     switch%cycle%ndata = iphase
  enddo ! iphase
end subroutine mrtcal_read_next_dumpcycle
!
subroutine mrtcal_update_databuf(setup,imbf,subshead,book,databuf,error)
  use gbl_message
  use mrtcal_buffer_types
  use mrtcal_setup_types
  use mrtcal_interfaces, except_this=>mrtcal_update_databuf
  !---------------------------------------------------------------------
  ! @ private
  ! Update (only IF NEEDED) the DATA buffer currently in memory
  ! according to the switching cycle described in 'switch'. As output
  ! the subroutine guarantees that (at least) this cycle is entirely in
  ! the DATA buffer.
  !---------------------------------------------------------------------
  type(mrtcal_setup_t),    intent(in)    :: setup
  type(imbfits_t),         intent(in)    :: imbf
  type(imbfits_subscan_t), intent(in)    :: subshead
  type(switch_book_t),     intent(in)    :: book
  type(data_buffer_t),     intent(inout) :: databuf
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='UPDATE>DATABUF'
  integer(kind=4) :: first,last
  logical :: needupdate
  logical, parameter :: tochunk=.true.  ! DATA always mapped on chunks in this context
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  ! First and last dumps to read (absolute position in compressed columns)
  first = book%izero+book%ifirst
  last = book%izero+book%ifirst+book%nphase-1
  call mrtcal_bookkeeping_iterate(databuf%time,subshead,first,last,needupdate,error)
  if (error) return
  if (needupdate) then
    call mrtcal_read_subscan_data(imbf,subshead,tochunk,databuf,error)
    if (error) return
  endif
  !
end subroutine mrtcal_update_databuf
!
subroutine mrtcal_fill_dumpphase(iphase,subshead,databuf,book,dump,error)
  use gbl_message
  use telcal_interfaces
  use mrtcal_buffer_types
  use mrtcal_interfaces, except_this=>mrtcal_fill_dumpphase
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4),         intent(in)    :: iphase
  type(imbfits_subscan_t), intent(in)    :: subshead
  type(data_buffer_t),     intent(in)    :: databuf
  type(switch_book_t),     intent(in)    :: book
  type(chunkset_2d_t),     intent(inout) :: dump
  logical,                 intent(inout) :: error
  ! Local
  integer(kind=4) :: itable,idata
  real(kind=8) :: elevation
  character(len=*), parameter :: rname='FILL>DUMPPHASE'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  ! Move the on pointer to its new position
  itable = book%izero+book%ifirst+iphase-1
  idata = itable-databuf%time%cur%first+1
  call reassociate_chunkset_2d(idata,databuf%mrtc,dump,error)
  if (error)  return
  ! Compute row number in subscan header table
  dump%mjd%med = subshead%backdata%table%mjd%val(itable)
  dump%mjd%beg = dump%mjd%med
  dump%mjd%end = dump%mjd%med
  !
  elevation = dble(dump%chunkset(1,1)%chunks(1)%gen%el) ! *** JP Poor man's solution
  dump%corr%el = real(elevation)
  dump%corr%airmass = airmass(elevation,error)
  if (error) return
  !
end subroutine mrtcal_fill_dumpphase
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
