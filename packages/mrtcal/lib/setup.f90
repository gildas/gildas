!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_setup_comm(line,error)
  use gbl_message
  use gkernel_interfaces
  use mrtcal_buffers
  use mrtcal_interfaces, except_this=>mrtcal_setup_comm
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !   MSETUP INPUT|BOOKKEEPING|CALIBRATION|OUTPUT|PIPELINE|DEBUG
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SETUP'
  character(len=16) :: keyword
  logical :: found
  ! MSET Keywords
  integer(kind=4), parameter :: mvocab=7
  character(len=16) :: vocab(mvocab)
  data vocab /'INPUT','BOOKKEEPING','CALIBRATION','OUTPUT','PIPELINE',  &
    'PSW','DEBUG'/
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_setup_parse(line,1,mvocab,vocab,keyword,found,error)
  if (error) return
  if (found) then
     select case (keyword)
     case('INPUT')
        call mrtcal_setup_input_parse(line,rsetup%inp,error)
        if (error) return
     case('BOOKKEEPING')
        call mrtcal_setup_bookkeeping_parse(line,rsetup,error)
        if (error) return
     case('CALIBRATION')
        call mrtcal_setup_calibration_parse(line,rsetup%cal,error)
        if (error) return
     case('OUTPUT')
        call mrtcal_setup_output_parse(line,rsetup%out,error)
        if (error) return
     case('PIPELINE')
        call mrtcal_setup_pipeline_parse(line,rsetup,error)
        if (error) return
     case('DEBUG')
        call mrtcal_setup_debug_parse(line,error)
        if (error) return
     case default
        call mrtcal_message(seve%e,rname,'Unimplemeted setup category '//keyword)
        error = .true.
        return
     end select
  else
     call mrtcal_setup_print(rsetup,error)
     if (error) return
  endif
end subroutine mrtcal_setup_comm
!
subroutine mrtcal_setup_parse_keyword(line,iarg,mvocab,vocab,keyword,found,error)
  use gbl_message
  use gkernel_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_setup_parse_keyword
  !---------------------------------------------------------------------
  ! @ private-generic mrtcal_setup_parse
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line
  integer(kind=4),  intent(in)    :: iarg
  integer(kind=4),  intent(in)    :: mvocab
  character(len=*), intent(in)    :: vocab(mvocab)
  character(len=*), intent(out)   :: keyword
  logical,          intent(out)   :: found
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SETUP>PARSE>KEYWORD'
  character(len=16) :: argum
  integer(kind=4) :: nc,ikey
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  if (sic_present(0,iarg)) then
     found = .true.
     call sic_ke(line,0,iarg,argum,nc,.true.,error)
     if (error) return
     call sic_ambigs(rname,argum,keyword,ikey,vocab,mvocab,error)
     if (error) return
  else
     found = .false.
  endif
end subroutine mrtcal_setup_parse_keyword
!
subroutine mrtcal_setup_parse_ikey(line,iarg,mvocab,vocab,ikey,error)
  use gbl_message
  use gkernel_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_setup_parse_ikey
  !---------------------------------------------------------------------
  ! @ private-generic mrtcal_setup_parse
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line
  integer(kind=4),  intent(in)    :: iarg
  integer(kind=4),  intent(in)    :: mvocab
  character(len=*), intent(in)    :: vocab(mvocab)
  integer(kind=4),  intent(out)   :: ikey
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SETUP>PARSE>IKEY'
  character(len=16) :: argum,keyword
  integer(kind=4) :: nc
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call sic_ke(line,0,iarg,argum,nc,.true.,error)
  if (error) return
  call sic_ambigs(rname,argum,keyword,ikey,vocab,mvocab,error)
  if (error) return
end subroutine mrtcal_setup_parse_ikey
!
subroutine mrtcal_setup_print(mrtset,error)
  use gbl_message
  use mrtcal_interfaces, except_this=>mrtcal_setup_print
  use mrtcal_setup_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(mrtcal_setup_t), intent(in)    :: mrtset
  logical,              intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SETUP>BOOKKEEPING>PRINT'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_message(seve%r,rname,'Setups')
  call mrtcal_setup_input_print(mrtset%inp,error)
  if (error) return
  call mrtcal_setup_bookkeeping_print(mrtset,error)
  if (error) return
  call mrtcal_setup_calibration_print(mrtset%cal,error)
  if (error) return
  call mrtcal_setup_output_print(mrtset%out,error)
  if (error) return
  call mrtcal_setup_pipeline_print(mrtset,error)
  if (error) return
  call mrtcal_setup_debug_print(error)
  if (error) return
  !
end subroutine mrtcal_setup_print
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_setup_input_parse(line,mrtsetinp,error)
  use gbl_message
  use mrtcal_interfaces, except_this=>mrtcal_setup_input_parse
  use mrtcal_setup_types
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !    MSETUP INPUT BANDWIDTH   Value  ! [MHz]
  ! or MSETUP INPUT DATA        NONE|ONTRACK|ALL
  ! or MSETUP INPUT TOCHUNK     YES|NO
  ! or MSETUP INPUT BAD         YES|NO
  ! or MSETUP INPUT MJDINTER    YES|NO
  !---------------------------------------------------------------------
  character(len=*),           intent(in)    :: line
  type(mrtcal_setup_input_t), intent(inout) :: mrtsetinp
  logical,                    intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SETUP>INPUT>PARSE'
  integer(kind=4), parameter :: mvocab=5
  integer(kind=4), parameter :: myesno=2
  integer(kind=4), parameter :: iyes = 1
  integer(kind=4) :: ikey
  character(len=16) :: keyword
  character(len=16) :: vocab(mvocab)
  character(len=3) :: yesno(myesno)
  real(kind=4) :: r4
  logical :: found
  ! Data
  data vocab /'BAD','BANDWIDTH','DATA','MJDINTER','TOCHUNK'/
  data yesno / 'YES','NO' /
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_setup_parse(line,2,mvocab,vocab,keyword,found,error)
  if (error) return
  if (found) then
     select case (keyword)
     case('BAD')
        call mrtcal_setup_parse(line,3,myesno,yesno,ikey,error)
        if (error) return
        mrtsetinp%bad = ikey.eq.iyes
        !
     case('BANDWIDTH')
        call sic_r4(line,0,3,r4,.true.,error)
        if (error) return
        if (r4.lt.0.0) then
           call mrtcal_message(seve%e,rname,'Negative bandwidth')
           error = .true.
           return
        endif
        mrtsetinp%bandwidth = r4
        !
     case('DATA')
        call mrtcal_setup_parse(line,3,mdatamodes,datamodes,mrtsetinp%data,error)
        if (error) return
        !
     case('MJDINTER')
        call mrtcal_setup_parse(line,3,myesno,yesno,ikey,error)
        if (error) return
        mrtsetinp%mjdinter = ikey.eq.iyes
        !
     case('TOCHUNK')
        call mrtcal_setup_parse(line,3,myesno,yesno,ikey,error)
        if (error) return
        mrtsetinp%tochunk = ikey.eq.iyes
        !
     case default
        call mrtcal_message(seve%e,rname,'Unknown input category '//keyword)
        error = .true.
        return 
     end select
  else
     call mrtcal_setup_input_print(mrtsetinp,error)
     if (error) return
  endif
end subroutine mrtcal_setup_input_parse
!
subroutine mrtcal_setup_input_print(mrtsetinp,error)
  use gbl_message
  use mrtcal_interfaces, except_this=>mrtcal_setup_input_print
  use mrtcal_setup_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(mrtcal_setup_input_t), intent(in)    :: mrtsetinp
  logical,                    intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SETUP>INPUT>PRINT'
  character(len=message_length) :: mess
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_message(seve%r,rname,'  Input (command READ)')
  !
  if (mrtsetinp%bad) then
     call mrtcal_message(seve%r,rname,'    BAD: read good and bad dumps from the backendXXX tables')
  else
     call mrtcal_message(seve%r,rname,'    BAD: read only good dumps from the backendXXX tables')
  endif
  !
  if (mrtsetinp%bandwidth.le.0.0) then
     call mrtcal_message(seve%r,rname,'    BANDWIDTH: use hardware chunk width')
  else
     write(mess,'(A,F0.1,A)') '    BANDWIDTH: reading bandwidth set to ',  &
       mrtsetinp%bandwidth,' MHz (or hardware width when narrower)'
     call mrtcal_message(seve%r,rname,mess)
  endif
  !
  select case (mrtsetinp%data)
  case (datamode_none)
    call mrtcal_message(seve%r,rname,'    DATA: do not read the DATA column')
  case (datamode_ontrack)
    call mrtcal_message(seve%r,rname,'    DATA: read the on-track part of the DATA column')
  case (datamode_all)
    call mrtcal_message(seve%r,rname,'    DATA: read all the DATA column')
  end select
  !
  if (mrtsetinp%mjdinter) then
     call mrtcal_message(seve%r,rname,'    MJDINTER: intersect the subscan DATE-OBS and DATE-END with the tables MJD ranges')
  else
     call mrtcal_message(seve%r,rname,'    MJDINTER: do not intersect the subscan DATE-OBS and DATE-END with the tables MJD ranges')
  endif
  !
  if (mrtsetinp%tochunk) then
     call mrtcal_message(seve%r,rname,'    TOCHUNK: DATA column is mapped into chunks at read time')
  else
     call mrtcal_message(seve%r,rname,'    TOCHUNK: DATA column is NOT mapped into chunks at read time')
  endif
  !
end subroutine mrtcal_setup_input_print
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_setup_bookkeeping_parse(line,mrtset,error)
  use gbl_message
  use gkernel_interfaces
  use ram_sizes
  use mrtcal_interfaces, except_this=>mrtcal_setup_bookkeeping_parse
  use mrtcal_setup_types
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command 
  ! MSETUP BOOKKEPPING SPACE value
  ! *** JP It would probably be better if it was a fraction of the
  ! *** JP available ramsize. Afterthought: Not fully clear because
  ! *** JP even with a large amount of RAM, there may be an optimal
  ! *** JP amount of buffer space. Maybe the two possibility should
  ! *** JP be authorized MSETUP BOOKKEEPING SPACE value % or value MB.
  !---------------------------------------------------------------------
  character(len=*),     intent(in)    :: line
  type(mrtcal_setup_t), intent(inout) :: mrtset
  logical,              intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SETUP>BOOKKEEPING>PARSE'
  character(len=message_length) :: mess
  real(kind=8) :: ramsize
  integer(kind=4), parameter :: mvocab=1
  character(len=16) :: keyword
  character(len=16) :: vocab(mvocab)
  logical :: found
  ! Data
  data vocab /'SPACE'/
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_setup_parse(line,2,mvocab,vocab,keyword,found,error)
  if (error) return
  if (found) then
     select case (keyword)
     case('SPACE')
        call sic_r8(line,0,3,ramsize,.true.,error)
        if (error) return
        mrtset%bufsize = ramsize*MB
        write(mess,'(a,f0.1,a)') 'Buffer space set to ',ramsize,' MB'
        call mrtcal_message(seve%i,rname,mess)
     case default
        call mrtcal_message(seve%e,rname,'Unknown bookkeeping category '//keyword)
        error = .true.
        return
     end select
  else
     call mrtcal_setup_bookkeeping_print(mrtset,error)
     if (error) return
  endif
end subroutine mrtcal_setup_bookkeeping_parse
!
subroutine mrtcal_setup_bookkeeping_print(mrtset,error)
  use gbl_message
  use ram_sizes
  use mrtcal_interfaces, except_this=>mrtcal_setup_bookkeeping_print
  use mrtcal_setup_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(mrtcal_setup_t), intent(in)    :: mrtset
  logical,              intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SETUP>BOOKKEEPING>PRINT'
  character(len=message_length) :: mess
  integer(kind=8) :: ramsize
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_message(seve%r,rname,'  Bookkeeping')
  ramsize = mrtset%bufsize/MB
  write(mess,'(a,i0,a)') '    SPACE: Buffer space set to ',ramsize,' MB'
  call mrtcal_message(seve%r,rname,mess)
  !
end subroutine mrtcal_setup_bookkeeping_print
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_setup_calibration_parse(line,mrtsetcal,error)
  use gbl_message
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_setup_calibration_parse
  use mrtcal_setup_types
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !    MSETUP CALIBRATION BAD         YES|NO
  ! or MSETUP CALIBRATION BANDWIDTH   Value
  ! or MSETUP CALIBRATION CHOPPER     STRICT|TOLERANT
  ! or MSETUP CALIBRATION FEEDBACK    PIXEL|SET|ELEMENT
  ! or MSETUP CALIBRATION INTERVAL    Value
  ! or MSETUP CALIBRATION MATCH       Value
  ! or MSETUP CALIBRATION MJDINTER    YES|NO
  ! or MSETUP CALIBRATION OFF         NEAREST|LINEAR
  ! or MSETUP CALIBRATION PRODUCTS    NEAREST|LINEAR|SPLINE
  ! or MSETUP CALIBRATION SCAN        NEAREST|LINEAR
  ! or MSETUP CALIBRATION WATER       SET|ELEMENT|FIXED [Value]
  !---------------------------------------------------------------------
  character(len=*),           intent(in)    :: line
  type(mrtcal_setup_calib_t), intent(inout) :: mrtsetcal
  logical,                    intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SETUP>CALIBRATION>PARSE'
  integer(kind=4), parameter :: mvocab=12
  integer(kind=4), parameter :: myesno=2
  integer(kind=4), parameter :: iyes = 1
  integer(kind=4), parameter :: mchopper=2
  integer(kind=4), parameter :: istrict=1
  integer(kind=4), parameter :: mwater=3
  integer(kind=4), parameter :: iwater_set=1
  integer(kind=4), parameter :: iwater_element=2
  integer(kind=4), parameter :: iwater_fixed=3
  integer(kind=4) :: ikey
  character(len=20) :: keyword
  character(len=20) :: vocab(mvocab)
  character(len=3) :: yesno(myesno)
  character(len=8) :: chopper(mchopper)
  character(len=7) :: water(mwater)
  real(kind=4) :: r4,interval
  real(kind=8) :: r8
  logical :: found
  ! Data
  data vocab /'ANTSLOW_MJD_SHIFT','BAD','BANDWIDTH','CHOPPER','FEEDBACK',  &
              'INTERVAL','MATCH','MJDINTER','OFF','PRODUCTS','SCAN','WATER'/
  data chopper /'STRICT','TOLERANT'/
  data yesno / 'YES','NO' /
  data water / 'SET','ELEMENT','FIXED' /
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_setup_parse(line,2,mvocab,vocab,keyword,found,error)
  if (error) return
  if (found) then
     select case (keyword)
     case('ANTSLOW_MJD_SHIFT')
        call sic_r8(line,0,3,r8,.true.,error)
        if (error) return
        if (r8.eq.0.d0) then
           call mrtcal_message(seve%w,rname,'No shift on antenna slow traces')
        endif
        mrtsetcal%antslowmjdshift = r8
        !
     case('BAD')
        call mrtcal_setup_parse(line,3,myesno,yesno,ikey,error)
        if (error) return
        mrtsetcal%bad = ikey.eq.iyes
        !
     case('BANDWIDTH')
        call sic_r4(line,0,3,r4,.true.,error)
        if (error) return
        if (r4.lt.0.0) then
           call mrtcal_message(seve%e,rname,'Negative bandwidth')
           error = .true.
           return
        endif
        mrtsetcal%bandwidth = r4
        !
     case('CHOPPER')
        call mrtcal_setup_parse(line,3,mchopper,chopper,ikey,error)
        if (error) return
        mrtsetcal%chopperstrict = ikey.eq.istrict
        !
     case('FEEDBACK')
        call mrtcal_setup_parse(line,3,mfeedbacks,feedbacks,ikey,error)
        if (error) return
        mrtsetcal%feedback = ikey
        !
     case('INTERVAL')
        call sic_r4(line,0,3,interval,.true.,error)
        if (error) return
        if (interval.lt.0.0) then
           call mrtcal_message(seve%e,rname,'Negative interval')
           error = .true.
           return
        endif
        mrtsetcal%einterval = interval  ! Limit for an error
        interval = 2.*interval/3.       ! Limit for a warning = 2/3 of error limit by default
        call sic_r4(line,0,4,interval,.false.,error)
        if (error) return
        if (interval.lt.0.0) then
           call mrtcal_message(seve%e,rname,'Negative interval')
           error = .true.
           return
        endif
        mrtsetcal%winterval = interval  ! Limit for a warning
        !
     case('MATCH')
        call sic_r4(line,0,3,r4,.true.,error)
        if (error) return
        if (r4.lt.0.0) then
           call mrtcal_message(seve%e,rname,'Negative position matching tolerance')
           error = .true.
           return
        endif
        mrtsetcal%match = r4*rad_per_sec
        !
     case('MJDINTER')
        call mrtcal_setup_parse(line,3,myesno,yesno,ikey,error)
        if (error) return
        mrtsetcal%mjdinter = ikey.eq.iyes
        !
     case('OFF')
        call mrtcal_setup_parse(line,3,minterpmodes,interpmodes,ikey,error)
        if (error) return
        mrtsetcal%interpoff = ikey
        !
     case('PRODUCTS')
        call mrtcal_setup_parse(line,3,minterpmodes,interpmodes,ikey,error)
        if (error) return
        mrtsetcal%interpprod = ikey
        !
     case('SCAN')
        call mrtcal_setup_parse(line,3,minterpmodes,interpmodes,ikey,error)
        if (error) return
        mrtsetcal%interpscan = ikey
        !
     case('WATER')
        call mrtcal_setup_parse(line,3,mwater,water,ikey,error)
        if (error) return
        if (ikey.ne.iwater_fixed .and. sic_present(0,4)) then
          call mrtcal_message(seve%e,rname,'Trailing argument after WATER mode')
          error = .true.
          return
        endif
        select case (ikey)
        case (iwater_set)
          mrtsetcal%watermode = chopper_water_set
          mrtsetcal%watervalue = 0.  ! Initial guess for minimization
        case (iwater_element)
          mrtsetcal%watermode = chopper_water_elem
          mrtsetcal%watervalue = 0.  ! Initial guess for minimization
        case (iwater_fixed)
          mrtsetcal%watermode = chopper_water_fixed
          call sic_r4(line,0,4,mrtsetcal%watervalue,.true.,error)
          if (error) return
        end select
        !
     case default
        call mrtcal_message(seve%e,rname,'Unknown calibration category '//keyword)
        error = .true.
        return
    end select
  else
     call mrtcal_setup_calibration_print(mrtsetcal,error)
     if (error) return
  endif
end subroutine mrtcal_setup_calibration_parse
!
subroutine mrtcal_setup_calibration_print(mrtsetcal,error)
  use gbl_message
  use mrtcal_interfaces, except_this=>mrtcal_setup_calibration_print
  use mrtcal_setup_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(mrtcal_setup_calib_t), intent(in)    :: mrtsetcal
  logical,                    intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SETUP>CALIBRATION>PRINT'
  character(len=message_length) :: mess
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_message(seve%r,rname,'  Calibration (command CALIBRATE or PIPELINE)')
  !
  if (mrtsetcal%antslowmjdshift.eq.0.d0) then
     call mrtcal_message(seve%r,rname,'    ANTSLOW_MJD_SHIFT: no shift will be applied')
  else
    write(mess,'(A,F0.3,A)')  '    ANTSLOW_MJD_SHIFT: shift the antenna slow traces by ',mrtsetcal%antslowmjdshift,' seconds'
    call mrtcal_message(seve%r,rname,mess)
  endif
  !
  if (mrtsetcal%bad) then
     call mrtcal_message(seve%r,rname,'    BAD: read good and bad dumps from the backendXXX tables')
  else
     call mrtcal_message(seve%r,rname,'    BAD: read only good dumps from the backendXXX tables')
  endif
  !
  if (mrtsetcal%bandwidth.le.0.0) then
     call mrtcal_message(seve%r,rname,'    BANDWIDTH: use hardware chunk width')
  else
     write(mess,'(A,F0.1,A)')  '    BANDWIDTH: calibration bandwidth set to ',  &
       mrtsetcal%bandwidth,' MHz (or hardware width when narrower)'
     call mrtcal_message(seve%r,rname,mess)
  endif
  !
  if (mrtsetcal%chopperstrict) then
     call mrtcal_message(seve%r,rname,'    CHOPPER: chopper calibration engine runs in strict mode (any problem blanks the data)')
  else
     call mrtcal_message(seve%r,rname,  &
       '    CHOPPER: chopper calibration engine runs in tolerant mode (go on even in case of problem)')
  endif
  !
  if (mrtsetcal%feedback.eq.feedback_pix) then
     call mrtcal_message(seve%r,rname,'    FEEDBACK: give calibration feedback for every pixel')
  else if (mrtsetcal%feedback.eq.feedback_set) then
     call mrtcal_message(seve%r,rname,'    FEEDBACK: give calibration feedback for every chunkset')
  else if (mrtsetcal%feedback.eq.feedback_elt) then
     call mrtcal_message(seve%r,rname,'    FEEDBACK: give calibration feedback for every chunk element')
  endif
  !
  write(mess,'(A,2(F0.1,A))') '    INTERVAL: calibration interval set to ',  &
    mrtsetcal%einterval,' minutes (warning at ',mrtsetcal%winterval,')'
  call mrtcal_message(seve%r,rname,mess)
  !
  write(mess,'(A,F5.3,A)') '    MATCH: position matching tolerance set to ',  &
    mrtsetcal%match*sec_per_rad,' arcsec'
  call mrtcal_message(seve%r,rname,mess)
  !
  if (mrtsetcal%mjdinter) then
     call mrtcal_message(seve%r,rname,'    MJDINTER: intersect the subscan DATE-OBS and DATE-END with the tables MJD ranges')
  else
     call mrtcal_message(seve%r,rname,'    MJDINTER: do not intersect the subscan DATE-OBS and DATE-END with the tables MJD ranges')
  endif
  !
  if (mrtsetcal%interpoff.eq.interp_linear) then
     call mrtcal_message(seve%r,rname,'    OFF: interpolate time-surrounding off subscans (On-The-Fly PSW)')
  elseif (mrtsetcal%interpoff.eq.interp_nearest) then
     call mrtcal_message(seve%r,rname,'    OFF: use time-nearest off subscan (On-The-Fly PSW)')
  endif
  !
  if (mrtsetcal%interpprod.eq.interp_nearest) then
     call mrtcal_message(seve%r,rname,  &
       '    PRODUCTS: use frequency-nearest values for  Trec, Tcal, Tsys, water, Ztau')
  elseif (mrtsetcal%interpprod.eq.interp_linear) then
     call mrtcal_message(seve%r,rname,  &
       '    PRODUCTS: linear interpolation of the Trec, Tcal, Tsys, water, Ztau values as a function of frequency')
  elseif (mrtsetcal%interpprod.eq.interp_spline) then
     call mrtcal_message(seve%r,rname,  &
       '    PRODUCTS: cubic spline interpolation of the Trec, Tcal, Tsys, water, Ztau values as a function of frequency')
  endif
  !
  if (mrtsetcal%interpscan.eq.interp_linear) then
     call mrtcal_message(seve%r,rname,'    SCAN: interpolate time-surrounding calibration scans')
     call mrtcal_message(seve%w,rname,'Interpolation of surrounding calibration scans is not yet implemented!')
  elseif (mrtsetcal%interpscan.eq.interp_nearest) then
     call mrtcal_message(seve%r,rname,'    SCAN: use previous calibration scan')
  endif
  !
  select case (mrtsetcal%watermode)
  case (chopper_water_set)
    call mrtcal_message(seve%r,rname,'    WATER: pwv amount is minimized (one single value for the whole bandwidth)')
  case (chopper_water_elem)
    call mrtcal_message(seve%r,rname,'    WATER: pwv amount is minimized (one independent value per calibration bandwidth)')
  case (chopper_water_fixed)
    write(mess,'(A,F0.2,A)')  '    WATER: pwv forced to ',mrtsetcal%watervalue,' mm for the whole bandwith'
    call mrtcal_message(seve%r,rname,mess)
  end select
  !
end subroutine mrtcal_setup_calibration_print
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_setup_output_parse(line,mrtsetout,error)
  use gbl_message
  use mrtcal_interfaces, except_this=>mrtcal_setup_output_parse
  use mrtcal_setup_types
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !    MSETUP OUTPUT CALIBRATION SPECTRA|ASSOCIATED|NONE
  ! or MSETUP OUTPUT CALTABLE    YES|NO
  ! or MSETUP OUTPUT CHUNK       ELEMENT|SET
  ! or MSETUP OUTPUT FOLD        YES|NO
  ! or MSETUP OUTPUT INTEGRATION CYCLE|SUBSCAN|SCAN|*
  ! or MSETUP OUTPUT SPECTRA     YES|NO
  ! or MSETUP OUTPUT USERSECTION YES|NO
  ! or MSETUP OUTPUT VDIRECTION  YES|NO
  ! or MSETUP OUTPUT VOXML       YES|NO
  ! or MSETUP OUTPUT VODIR       DirName
  ! or MSETUP OUTPUT WEIGHT      YES|NO
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: line
  type(mrtcal_setup_output_t), intent(inout) :: mrtsetout
  logical,                     intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SETUP>OUTPUT>PARSE'
  integer(kind=4), parameter :: mvocab=11
  integer(kind=4), parameter :: mchunk=2
  integer(kind=4), parameter :: myesno=2
  integer(kind=4), parameter :: iyes = 1
  integer(kind=4) :: ikey,nc
  character(len=16) :: keyword
  character(len=16) :: vocab(mvocab)
  character(len=16) :: chunk(mchunk)
  character(len=3) :: yesno(myesno)
  logical :: found
  ! Data
  data vocab /'CALIBRATION','CALTABLE','CHUNK','FOLD','INTEGRATION',  &
              'SPECTRA','USERSECTION','VDIRECTION','VOXML','VODIR',   &
              'WEIGHT'/
  data chunk /'ELEMENT','SET'/
  data yesno /'YES','NO'/
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_setup_parse(line,2,mvocab,vocab,keyword,found,error)
  if (error) return
  if (found) then
     select case (keyword)
     case('CALIBRATION')
        call mrtcal_setup_parse(line,3,moutputcalib,outputcalibs,mrtsetout%calib,error)
        if (error) return
     case('CALTABLE')
        call mrtcal_setup_parse(line,3,myesno,yesno,ikey,error)
        if (error) return
        mrtsetout%caltable = ikey.eq.iyes
     case('CHUNK')
        call mrtcal_setup_parse(line,3,mchunk,chunk,ikey,error)
        if (error) return
        mrtsetout%bychunk = ikey.eq.1
     case('FOLD')
        call mrtcal_setup_parse(line,3,myesno,yesno,ikey,error)
        if (error) return
        mrtsetout%fold = ikey.eq.iyes
     case('INTEGRATION')
        call mrtcal_setup_parse(line,3,maccmodes,accmodes,mrtsetout%accmode,error)
        if (error) return
     case('SPECTRA')
        call mrtcal_setup_parse(line,3,myesno,yesno,ikey,error)
        if (error) return
        mrtsetout%toclass = ikey.eq.iyes
     case('USERSECTION')
        call mrtcal_setup_parse(line,3,myesno,yesno,ikey,error)
        if (error) return
        mrtsetout%user = ikey.eq.iyes
     case('VDIRECTION')
        call mrtcal_setup_parse(line,3,myesno,yesno,ikey,error)
        if (error) return
        mrtsetout%vdirection = ikey.eq.iyes
     case('VOXML')
        call mrtcal_setup_parse(line,3,myesno,yesno,ikey,error)
        if (error) return
        mrtsetout%voxml = ikey.eq.iyes
     case('VODIR')
        call sic_ch(line,0,3,mrtsetout%vodir,nc,.true.,error)
        if (error)  return
     case('WEIGHT')
        call mrtcal_setup_parse(line,3,myesno,yesno,ikey,error)
        if (error) return
        mrtsetout%weight = ikey.eq.iyes
     case default
        call mrtcal_message(seve%e,rname,'Unknown output category '//keyword)
        error = .true.
        return
     end select
  else
     call mrtcal_setup_output_print(mrtsetout,error)
     if (error) return
  endif
end subroutine mrtcal_setup_output_parse
!
subroutine mrtcal_setup_output_print(mrtsetout,error)
  use gbl_message
  use mrtcal_interfaces, except_this=>mrtcal_setup_output_print
  use mrtcal_setup_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(mrtcal_setup_output_t), intent(in)    :: mrtsetout
  logical,                     intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SETUP>OUTPUT>PRINT'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_message(seve%r,rname,'  Output')
  !
  select case (mrtsetout%calib)
  case (outputcalib_none)
    call mrtcal_message(seve%r,rname,'    CALIBRATION: calibration products are not written to the Class file')
  case (outputcalib_spec)
    call mrtcal_message(seve%r,rname,'    CALIBRATION: write calibration products in separated observations')
  case (outputcalib_asso)
    call mrtcal_message(seve%r,rname,'    CALIBRATION: write calibration products in a single observation with associated arrays')
  end select
  !
  if (mrtsetout%bychunk) then
     call mrtcal_message(seve%r,rname,'    CHUNK: write one spectrum per chunk')
  else
     call mrtcal_message(seve%r,rname,'    CHUNK: write one spectrum per chunkset')
  endif
  !
  if (mrtsetout%accmode.eq.accmode_defa) then
     call mrtcal_message(seve%r,rname,'    INTEGRATION: accumulate dumps along scan (TRACKED) or phase cycle (OTF)')
  elseif (mrtsetout%accmode.ge.2 .and. mrtsetout%accmode.le.maccmodes) then
     call mrtcal_message(seve%r,rname,'    INTEGRATION: accumulate dumps along every '//accmodes(mrtsetout%accmode))
  else
     call mrtcal_message(seve%w,rname,'    INTEGRATION: unknown accumulation mode')
  endif
  !
  if (mrtsetout%toclass) then
     call mrtcal_message(seve%r,rname,'    SPECTRA: write spectra in the CLASS output file')
  else
     call mrtcal_message(seve%r,rname,'    SPECTRA: do not write spectra in the CLASS output file')
  endif
  !
  if (mrtsetout%user) then
     call mrtcal_message(seve%r,rname,'    USERSECTION: write the very experimental MRTCAL user section')
  else
     call mrtcal_message(seve%r,rname,'    USERSECTION: do not write the very experimental MRTCAL user section')
  endif
  !
  if (mrtsetout%vdirection) then
     call mrtcal_message(seve%r,rname,&
          '    VDIRECTION: correct the doppler factor according to the observed line-of-sight direction')
  else
     call mrtcal_message(seve%r,rname,&
          '    VDIRECTION: do not correct the doppler factor according to the observed line-of-sight direction')
  endif
  !
  call mrtcal_message(seve%r,rname,'    VODIR: VO XML output directory is '//mrtsetout%vodir)
  !
  if (mrtsetout%voxml) then
     call mrtcal_message(seve%r,rname,'    VOXML: write calibration results in VO XML')
  else
     call mrtcal_message(seve%r,rname,'    VOXML: do not write calibration results in VO XML')
  endif
  !
  if (mrtsetout%weight) then
     call mrtcal_message(seve%r,rname,'    WEIGHT: write the weight array as associated array W')
  else
     call mrtcal_message(seve%r,rname,'    WEIGHT: do not write the weight array as associated array W')
  endif
  !
end subroutine mrtcal_setup_output_print
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_setup_pipeline_parse(line,mrtset,error)
  use gbl_message
  use mrtcal_interfaces, except_this=>mrtcal_setup_pipeline_parse
  use mrtcal_setup_types
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command 
  ! MSETUP PIPELINE ONERROR CONTINUE|STOP
  !---------------------------------------------------------------------
  character(len=*),     intent(in)    :: line
  type(mrtcal_setup_t), intent(inout) :: mrtset
  logical,              intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SETUP>PIPELINE>PARSE'
  integer(kind=4), parameter :: mvocab=1
  integer(kind=4), parameter :: monerror=2
  integer(kind=4) :: ikey
  character(len=16) :: keyword
  character(len=16) :: vocab(mvocab)
  character(len=16) :: onerror(monerror)
  logical :: found
  ! Data
  data vocab /'ONERROR'/
  data onerror /'CONTINUE','STOP'/
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_setup_parse(line,2,mvocab,vocab,keyword,found,error)
  if (error) return
  if (found) then
     select case (keyword)
     case('ONERROR')
        call mrtcal_setup_parse(line,3,monerror,onerror,ikey,error)
        if (error) return
        mrtset%pipeline = ikey.eq.1
     case default
        call mrtcal_message(seve%e,rname,'Unknown pipeline category '//keyword)
        error = .true.
        return
     end select
  else
     call mrtcal_setup_pipeline_print(mrtset,error)
     if (error) return
  endif
end subroutine mrtcal_setup_pipeline_parse
!
subroutine mrtcal_setup_pipeline_print(mrtset,error)
  use gbl_message
  use mrtcal_interfaces, except_this=>mrtcal_setup_pipeline_print
  use mrtcal_setup_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(mrtcal_setup_t), intent(in)    :: mrtset
  logical,              intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SETUP>PIPELINE>PRINT'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_message(seve%r,rname,'  Pipeline')
  if (mrtset%pipeline) then
     call mrtcal_message(seve%r,rname,  &
       '    ONERROR: continue to iterate with the next scan when an error is raised on the current scan')
  else
     call mrtcal_message(seve%r,rname,  &
       '    ONERROR: stop as soon as an error is raised')
  endif
  !
end subroutine mrtcal_setup_pipeline_print
!
subroutine mrtcal_setup_debug_all(newseve,error)
  use mrtcal_dependencies_interfaces
  use mrtcal_messaging
  !---------------------------------------------------------------------
  ! @ private
  ! Set the default state for command
  !    MSET DEBUG ON|OFF
  ! for all topics
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)    :: newseve
  logical,         intent(inout) :: error
  !
  ! IMBFITS
  call imbfits_message_debug(.true.,newseve,.true.,newseve,error)
  if (error)  return
  ! INDEX
  call mrtindex_message_debug(.true.,newseve,.true.,newseve,error)
  if (error)  return
  ! CALIBRATION
  mseve%calib%alloc = newseve
  mseve%calib%book = newseve
  mseve%calib%others = newseve
  ! OUTPUT
  mseve%output%acc = newseve
  ! SYNCHRONIZATION
  mseve%sync = newseve
  !
end subroutine mrtcal_setup_debug_all
!
subroutine mrtcal_setup_debug_parse(line,error)
  use gbl_message
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_setup_debug_parse
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  ! MSETUP DEBUG [Topic [Subtopic]] ON|OFF
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SETUP>DEBUG>PARSE'
  integer(kind=4) :: iswitch,newseve
  character(len=16) :: keyword
  logical :: found
  ! Topics
  integer(kind=4), parameter :: mvocab=5
  character(len=16) :: vocab(mvocab)
  data vocab /'IMBFITS','INDEX','SYNCHRONIZATION','CALIBRATION','OUTPUT'/
  ! On-off
  integer(kind=4), parameter :: monoff=2
  character(len=4) :: onoff(monoff)
  data onoff /'ON','OFF'/
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  ! Parse the ON|OFF switch (last argument)
  iswitch = sic_narg(0)
  if (iswitch.eq.1) then
    call mrtcal_setup_debug_print(error)
    return
  elseif (iswitch.gt.4) then
    call mrtcal_message(seve%e,rname,  &
      'Syntax should be: MSET DEBUG [Topic [Subtopic]] ON|OFF')
    error = .true.
    return
  endif
  call mrtcal_setup_parse(line,iswitch,monoff,onoff,keyword,found,error)
  if (error) return
  if (keyword.eq.'ON') then
    newseve = seve%i
  else
    newseve = seve%d
  endif
  !
  if (iswitch.eq.2) then
    ! MSET DEBUG ON|OFF
    call mrtcal_setup_debug_all(newseve,error)
    return
  endif
  !
  ! Parse the topic
  call mrtcal_setup_parse(line,2,mvocab,vocab,keyword,found,error)
  if (error) return
  if (found) then
     select case (keyword)
     case('IMBFITS')
        call mrtcal_setup_debug_imbfits_parse(error)
        if (error)  return
     case('INDEX')
        call mrtcal_setup_debug_index_parse(error)
        if (error)  return
     case('SYNCHRONIZATION')
        call mrtcal_setup_debug_sync_parse(error)
        if (error)  return
     case('CALIBRATION')
        call mrtcal_setup_debug_calib_parse(error)
        if (error)  return
     case('OUTPUT')
        call mrtcal_setup_debug_output_parse(error)
        if (error)  return
     case default
        call mrtcal_message(seve%e,rname,'Unknown topic '//keyword)
        error = .true.
        return
     end select
  else
     call mrtcal_setup_debug_print(error)
     if (error) return
  endif
  !
contains
  subroutine mrtcal_setup_debug_imbfits_parse(error)
    use mrtcal_dependencies_interfaces
    use mrtcal_interfaces
    !-------------------------------------------------------------------
    ! Support routine for command
    !   MSET DEBUG IMBFITS [*|ALLOCATION|OTHERS] ON|OFF
    !-------------------------------------------------------------------
    logical, intent(inout) :: error
    ! Local
    integer(kind=4), parameter :: mivocab=3
    character(len=16) :: ivocab(mivocab)
    data ivocab /'*','ALLOCATION','OTHERS'/
    !
    if (iswitch.eq.3) then  ! MSET DEBUG IMBFITS ON|OFF
      keyword = '*'
    else                    ! MSET DEBUG IMBFITS *|ALLOCATION|OTHERS ON|OFF
      call mrtcal_setup_parse(line,3,mivocab,ivocab,keyword,found,error)
      if (error) return
    endif
    !
    select case (keyword)
    case ('*')
      call imbfits_message_debug(.true.,newseve,.true.,newseve,error)
    case ('ALLOCATION')
      call imbfits_message_debug(.true.,newseve,.false.,newseve,error)
    case ('OTHERS')
      call imbfits_message_debug(.false.,newseve,.true.,newseve,error)
    end select
    if (error)  return
  end subroutine mrtcal_setup_debug_imbfits_parse
  !
  subroutine mrtcal_setup_debug_index_parse(error)
    use mrtcal_dependencies_interfaces
    use mrtcal_interfaces
    use mrtcal_messaging
    !-------------------------------------------------------------------
    ! Support routine for command
    !   MSET DEBUG INDEX [*|ALLOCATION|OTHERS] ON|OFF
    !-------------------------------------------------------------------
    logical, intent(inout) :: error
    ! Local
    integer(kind=4), parameter :: mivocab=3
    character(len=16) :: ivocab(mivocab)
    data ivocab /'*','ALLOCATION','OTHERS'/
    !
    if (iswitch.eq.3) then  ! MSET DEBUG INDEX ON|OFF
      keyword = '*'
    else                    ! MSET DEBUG INDEX *|ALLOCATION|OTHERS ON|OFF
      call mrtcal_setup_parse(line,3,mivocab,ivocab,keyword,found,error)
      if (error) return
    endif
    !
    select case (keyword)
    case ('*')
      call mrtindex_message_debug(.true.,newseve,.true.,newseve,error)
    case ('ALLOCATION')
      call mrtindex_message_debug(.true.,newseve,.false.,newseve,error)
    case ('OTHERS')
      call mrtindex_message_debug(.false.,newseve,.true.,newseve,error)
    end select
    if (error)  return
  end subroutine mrtcal_setup_debug_index_parse
  !
  subroutine mrtcal_setup_debug_sync_parse(error)
    use mrtcal_dependencies_interfaces
    use mrtcal_interfaces
    use mrtcal_messaging
    !-------------------------------------------------------------------
    ! Support routine for command
    !   MSET DEBUG SYNCHRONIZATION ON|OFF
    !-------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    if (iswitch.ne.3) then  ! MSET DEBUG SYNC ON|OFF
      call mrtcal_message(seve%e,rname,'No subtopic for MSET DEBUG SYNC')
      error = .true.
      return
    endif
    mseve%sync = newseve
  end subroutine mrtcal_setup_debug_sync_parse
  !
  subroutine mrtcal_setup_debug_calib_parse(error)
    use mrtcal_dependencies_interfaces
    use mrtcal_interfaces
    use mrtcal_messaging
    !-------------------------------------------------------------------
    ! Support routine for command
    !   MSET DEBUG CALIB [*|ALLOCATION|OTHERS] ON|OFF
    !-------------------------------------------------------------------
    logical, intent(inout) :: error
    ! Local
    integer(kind=4), parameter :: mivocab=4
    character(len=16) :: ivocab(mivocab)
    data ivocab /'*','ALLOCATION','BOOKKEEPING','OTHERS'/
    !
    if (iswitch.eq.3) then  ! MSET DEBUG CALIB ON|OFF
      keyword = '*'
    else                    ! MSET DEBUG CALIB *|ALLOCATION|BOOK|OTHERS ON|OFF
      call mrtcal_setup_parse(line,3,mivocab,ivocab,keyword,found,error)
      if (error) return
    endif
    !
    select case (keyword)
    case ('*')
      mseve%calib%alloc = newseve
      mseve%calib%book = newseve
      mseve%calib%others = newseve
    case ('ALLOCATION')
      mseve%calib%alloc = newseve
    case ('BOOKKEEPING')
      mseve%calib%book = newseve
    case ('OTHERS')
      mseve%calib%others = newseve
    end select
    if (error)  return
  end subroutine mrtcal_setup_debug_calib_parse
  !
  subroutine mrtcal_setup_debug_output_parse(error)
    use mrtcal_dependencies_interfaces
    use mrtcal_interfaces
    use mrtcal_messaging
    !-------------------------------------------------------------------
    ! Support routine for command
    !   MSET DEBUG OUTPUT [*|ACCUMULATE] ON|OFF
    !-------------------------------------------------------------------
    logical, intent(inout) :: error
    ! Local
    integer(kind=4), parameter :: mivocab=2
    character(len=16) :: ivocab(mivocab)
    data ivocab /'*','ACCUMULATE'/
    !
    if (iswitch.eq.3) then
       ! MSET DEBUG OUTPUT ON|OFF
       keyword = '*'
    else
       ! MSET DEBUG OUTPUT *|ACCUMULATE ON|OFF
       call mrtcal_setup_parse(line,3,mivocab,ivocab,keyword,found,error)
       if (error) return
    endif
    !
    select case (keyword)
    case ('*')
      mseve%output%acc = newseve
    case ('ACCUMULATE')
      mseve%output%acc = newseve
    end select
    if (error)  return
  end subroutine mrtcal_setup_debug_output_parse
  !
end subroutine mrtcal_setup_debug_parse
!
subroutine mrtcal_setup_debug_print(error)
  use mrtcal_interfaces, except_this=>mrtcal_setup_debug_print
  use mrtcal_messaging
  use mrtcal_setup_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  logical, intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='SETUP>DEBUG>PRINT'
  character(len=message_length) :: mess
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_message(seve%r,rname,'  Debug')
  call mrtcal_message(seve%r,rname,'    IMBFITS: (not implemented)')
  !
  call mrtcal_message(seve%r,rname,'    INDEX: (not implemented)')
  !
  write(mess,'(3(A,L))')  '    CALIBRATION: Allocation ',mseve%calib%alloc.eq.seve%i,  &
                          ', Bookkeeping ',mseve%calib%book.eq.seve%i, &
                          ', Others ',mseve%calib%others.eq.seve%i
  call mrtcal_message(seve%r,rname,mess)
  !
  write(mess,'(1(A,L))')  '    OUTPUT: Accumulation ',mseve%output%acc.eq.seve%i
  call mrtcal_message(seve%r,rname,mess)
  !
  write(mess,'(1(A,L))')  '    SYNCHRONIZATION: ',mseve%sync.eq.seve%i
  call mrtcal_message(seve%r,rname,mess)
  !
end subroutine mrtcal_setup_debug_print
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_setup_variable(error)
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_setup_variable
  use mrtcal_buffers
  !---------------------------------------------------------------------
  ! @ private
  ! Fill the structure SET%MRTCAL%
  !---------------------------------------------------------------------
  logical, intent(inout) :: error
  ! Local
  character(len=*), parameter :: struct='SET%MRTCAL%'
  !
  call sic_defstructure(struct,.true.,error)
  if (error)  return
  !
  call mrtcal_setup_calibration_variable(struct,rsetup%cal,error)
  if (error)  return
  call mrtcal_setup_output_variable(struct,rsetup%out,error)
  if (error)  return
  !
end subroutine mrtcal_setup_variable
!
subroutine mrtcal_setup_calibration_variable(parent,cal,error)
  use mrtcal_dependencies_interfaces
  use mrtcal_setup_types
  !---------------------------------------------------------------------
  ! @ private
  ! Fill the structure SET%MRTCAL%CALIB%
  !---------------------------------------------------------------------
  character(len=*),           intent(in)    :: parent
  type(mrtcal_setup_calib_t), intent(in)    :: cal
  logical,                    intent(inout) :: error
  ! Local
  character(len=20) :: str
  integer(kind=4) :: ns
  logical, parameter :: ro=.true.
  !
  str = trim(parent)//'CALIB%'
  ns = len_trim(str)
  call sic_defstructure(str(1:ns),.true.,error)
  if (error)  return
  !
  call sic_def_logi(str(1:ns)//'BAD',      cal%bad,              ro,error)
  if (error)  return
  call sic_def_real(str(1:ns)//'BANDWIDTH',cal%bandwidth,    0,0,ro,error)
  if (error)  return
! call sic_def_logi(str(1:ns)//'',         cal%chopperstrict,    ro,error)
! if (error)  return
! call sic_def_inte(str(1:ns)//'',         cal%feedback,     0,0,ro,error)
! if (error)  return
  call sic_def_real(str(1:ns)//'WINTERVAL',cal%winterval,    0,0,ro,error)
  if (error)  return
  call sic_def_real(str(1:ns)//'EINTERVAL',cal%einterval,    0,0,ro,error)
  if (error)  return
! call sic_def_inte(str(1:ns)//'',         cal%interpscan,   0,0,ro,error)
! if (error)  return
! call sic_def_inte(str(1:ns)//'',         cal%interpoff,    0,0,ro,error)
! if (error)  return
! call sic_def_logi(str(1:ns)//'',         cal%mjdinter,         ro,error)
! if (error)  return
! call sic_def_inte(str(1:ns)//'',         cal%interpprod,   0,0,ro,error)
! if (error)  return
  !
end subroutine mrtcal_setup_calibration_variable
!
subroutine mrtcal_setup_output_variable(parent,out,error)
  use mrtcal_dependencies_interfaces
  use mrtcal_setup_types
  !---------------------------------------------------------------------
  ! @ private
  ! Fill the structure SET%MRTCAL%OUTPUT%
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: parent
  type(mrtcal_setup_output_t), intent(in)    :: out
  logical,                     intent(inout) :: error
  ! Local
  character(len=20) :: str
  integer(kind=4) :: ns
  logical, parameter :: ro=.true.
  !
  str = trim(parent)//'OUTPUT%'
  ns = len_trim(str)
  call sic_defstructure(str(1:ns),.true.,error)
  if (error)  return
  !
! call sic_def_inte(str(1:ns)//'',       out%accmode,0,0,ro,error)
! if (error)  return
! call sic_def_inte(str(1:ns)//'',       out%calib,  0,0,ro,error)
! if (error)  return
  call sic_def_logi(str(1:ns)//'CHUNK',  out%bychunk,    ro,error)
  if (error)  return
  call sic_def_logi(str(1:ns)//'SPECTRA',out%toclass,    ro,error)
  if (error)  return
  call sic_def_logi(str(1:ns)//'VOXML',  out%voxml,      ro,error)
  if (error)  return
  call sic_def_logi(str(1:ns)//'WEIGHT', out%weight,     ro,error)
  if (error)  return
  call sic_def_logi(str(1:ns)//'USER',   out%user,       ro,error)
  if (error)  return
  !
end subroutine mrtcal_setup_output_variable
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_setup_do2done(obstype,swmode,doset,doneset,error)
  use mrtcal_interfaces, except_this=>mrtcal_setup_do2done
  use mrtcal_setup_types
  !---------------------------------------------------------------------
  ! @ private
  ! Translate a user setup type (which may include * choices, i.e. let
  ! Mrtcal choose) to actual setup. This depends on the current scan.
  !---------------------------------------------------------------------
  integer(kind=4),      intent(in)    :: obstype  ! [code] Observing type
  integer(kind=4),      intent(in)    :: swmode   ! [code] Switching mode
  type(mrtcal_setup_t), intent(in)    :: doset    ! Setup (user choices)
  type(mrtcal_setup_t), intent(out)   :: doneset  ! Setup (resolved)
  logical,              intent(inout) :: error    ! Logical error flag
  !
  ! Copy everything
  doneset = doset
  !
  ! Translate MSET OUTPUT INTEGRATION
  call mrtcal_init_accumulate_or_write(obstype,swmode,doset,doneset,error)
  if (error)  return
  !
end subroutine mrtcal_setup_do2done
