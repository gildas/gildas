subroutine calibration_to_VO(fits,backcal,error)
  use phys_const
  use gbl_format
  use gbl_message
  use class_types
  use imbfits_types
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>calibration_to_VO
  use mrtcal_buffers
  use mrtcal_calib_types
  use pako_xml
  use results_to_vo
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(imbfits_t),        intent(in)    :: fits
  type(calib_backend_t),  intent(in)    :: backcal
  logical,                intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='calibration_to_VO'
  character(len=64) :: filename
  character(len=filename_length) :: dirname,fullname
  character(len=128) :: site
  character(len=backname_length) :: backendname
  character(len=15) :: scanid
  character(len=4) :: year
  character(len=2) :: day, month
  logical :: resetBe, resetCal, resetRx
  type(observation) :: cold,hot,sky  ! Stitched spectra
  type(chunk_t), pointer :: refchunk
  integer(kind=4) :: ier,lun,isys,jsys,iset,ifront,iunsort,ipix,ipart
  real(kind=4) :: foffx,foffy,pcold,phot,psky
  integer(kind=4) :: chunkid(backcal%nset),sort(backcal%nset)
  integer(kind=size_length) :: ndata
  character(len=24) :: subdir,recname
  logical :: partdone(backcal%nset)
  !
  backendName = mrtindex_backend(backcal%head%key%backend)
  !
  year  = fits%primary%date_obs%val(1:4)
  month = fits%primary%date_obs%val(6:7)
  day   = fits%primary%date_obs%val(9:10)
  !
  write(filename,'(a20,a,a1,a4,a2,a2,a1,i0,a4)')  &
       'iram30m-calibration-',trim(backendName),'-',year,month,day,  &
       's',backcal%head%key%scan,'.xml'
  !
  ! Build the full file name with path. Check for a subdirectory of the
  ! form "20161115/scans/77"
  write(subdir,'(a4,a2,a2,a,i0)') year,month,day,"/scans/",backcal%head%key%scan
  dirname = trim(rsetup%out%vodir)//'/'//subdir
  if (gag_inquire(dirname,len_trim(dirname)).ne.0) then
    dirname = rsetup%out%vodir
    if (gag_inquire(dirname,len_trim(dirname)).ne.0) then
      dirname = "."
    endif
  endif
  call sic_parse_file(filename,trim(dirname)//'/','.xml',fullname)
  !
  ier = sic_getlun(lun)
  open(unit=lun,file=fullname,recl=facunf*512,status='unknown',iostat=ier)
  if (ier.ne.0) then
    call mrtcal_message(seve%e,rname,'Failed to create file '//fullname)
    error = .true.
    goto 100
  endif
  !
  call pakoXMLsetOutputUnit(iUnit = lun, error = error)
  if (error)  goto 100
  call pakoXMLsetIndent(iIndent = 2, error = error)
  if (error)  goto 100
  call results_to_VO_writeProlog(error)
  if (error)  goto 100

  if (backcal%head%key%telescope.eq.telescope_iram30m) then
     site = "Pico Veleta"
  else
     call mrtcal_message(seve%e,rname,'Telescope not supported')
     error = .true.
     return
  endif
  !
  call results_to_VO_setOdpHeader(                                 &
            telescope   = mrtindex_telescope(backcal%head%key%telescope),  &
            observatory = site,                                    &
            odpSoftware = fits%primary%creator%val,                &
            reset       = .True.,                                  &
            error       = error)
  if (error)  goto 100
  !
  call results_to_VO_writeOdpHeader(error)
  if (error)  goto 100
  !
  call results_to_VO_setMeasurementHeader(                          &
    measurement        = "Calibration",                             &
    sourceName         = trim(backcal%head%key%source),             &
    timeStamp          = fits%primary%date_obs%val,                 &
    azimuth            = real(backcal%head%key%az*deg_per_rad),     &  ! ZZZ Typical value
    elevation          = real(backcal%head%key%el*deg_per_rad),     &  ! ZZZ Typical value
    refraction         = real(fits%scan%head%refracti%val),         &  ! ZZZ To be checked
    pressureAmbient    = real(fits%scan%head%pressure%val),         &
    temperatureAmbient = real(273.16+fits%scan%head%tambient%val),  &  ! Outside temperature
    humidityAmbient    = real(fits%scan%head%humidity%val),         &
    switchingMode      = mrtindex_swmode_voxml(backcal%head%key%switchmode),  &
    observingMode      = "calibrate",                               &
    reset              = .true.,                                    &
    error              = error)
  if (error)  goto 100
  !
  resetRx  = .true.
  resetBe  = .true.
  resetCal = .true.
  !
  write(scanId,'(a11,i4.4)') fits%primary%date_obs%val(1:10)//'.',backcal%head%key%scan
  !
  call results_to_VO_setScanId(      &
    scanId = scanId,                 &
    scanN  = backcal%head%key%scan,  &
    reset  = .true.,                 &
    error  = error)
  if (error)  goto 100
  !
  call results_to_VO_writeMeasurementHeader(error)
  if (error)  goto 100
  !
  jsys = 0
  do isys=1,fits%scan%head%desc%naxis2%val
     if (fits%scan%table%sysoff%val(isys).eq.'Nasmyth') then
        jsys = isys
        exit
     endif
  enddo
  if (jsys.eq.0) then
    call mrtcal_message(seve%i,rname,'No Nasmyth offset')
    foffx = 0.
    foffy = 0.
  else
    foffx = fits%scan%table%xoffset%val(jsys)*sec_per_rad
    foffy = fits%scan%table%yoffset%val(jsys)*sec_per_rad
  endif
  !
  ! Sorting array based on the PART id of the chunk, then by PIXEL number
  if (fits%front%table%recname%val(1)(1:4).eq."HERA") then
    do iset=1,backcal%nset
      ! ZZZ We should have a better way to know to which pixel a chunk or chunkset
      ! belongs
      read(backcal%sky%chunkset(iset,1)%chunks(1)%gen%teles(12:12),'(I1)')  ipix
      chunkid(iset) = 100*backcal%sky%chunkset(iset,1)%chunks(1)%id + ipix
    enddo
  else
    do iset=1,backcal%nset
      chunkid(iset) = backcal%sky%chunkset(iset,1)%chunks(1)%id
    enddo
  endif
  call gi4_trie(chunkid,sort,backcal%nset,error)
  if (error)  return
  !
  call init_obs(cold)
  call init_obs(hot)
  call init_obs(sky)
  ipix = 1  ! This "Npix" dimension is not used for HERA
  !
  ! Frontend and backend
  partdone(:) = .false.
  do iunsort=1,backcal%nset
    iset = sort(iunsort)  ! Use sorted index
    ipart = backcal%sky%chunkset(iset,ipix)%chunks(1)%id  ! Backend part for this set
    !
    if (partdone(ipart))  cycle
    !
    ! Stitch sky load chunks as a single spectrum:
    call mrtcal_chunkset_to_obs_ry(backcal%sky%chunkset(iset,ipix),sky,error)
    if (error)  return
    !
    ifront = backcal%sky%chunkset(iset,ipix)%chunks(1)%ifront  ! Frontend for this set
    if (fits%front%table%recname%val(ifront)(1:4).eq."HERA") then
      recname = fits%front%table%recname%val(ifront)(1:5)//" Pixel 0"
    else
      recname = sky%head%gen%teles(4:8)
    endif
    !
    call results_to_VO_setReceiver(              &
      name           = recname,                  &
      frequency      = sky%head%spe%restf*1d-3,  &  ! [GHz]
      frequencyImage = sky%head%spe%image*1d-3,  &  ! [GHz]
      lineName       = sky%head%spe%line,        &
      sideBand       = fits%front%table%sideband%val(ifront),  &
      doppler        = fits%front%table%doppler%val(ifront),   &
      width          = fits%front%table%widenar%val(ifront),   &
      effForward     = sky%head%cal%foeff,       &
      effBeam        = sky%head%cal%beeff,       &  ! Probably not the correct one....
      scale          = fits%front%table%tscale%val(ifront),    &
      gainImage      = sky%head%cal%gaini,       &
      tempAmbient    = sky%head%cal%tchop,       &  ! Thot, not Tambient
      tempCold       = sky%head%cal%tcold,       &
      offset         = 0.d0,                     &  ! ZZZ TBD What is this???
      centerIF       = dble(fits%front%table%ifcenter%val(ifront)),  &  ! [GHz]
      xOffsetNasmyth = foffx,                    &
      yOffsetNasmyth = foffy,                    &
      reset          = resetRx,                  &
      error          = error)
    if (error)  goto 100
    resetRx = .false.
    !
    refchunk => backcal%sky%chunkset(iset,ipix)%chunks(1)
    call results_to_VO_setBackend(           &
      name       = backendName,              &
      nPart      = refchunk%id,              &
      resolution = real(abs(sky%head%spe%fres)),                     &  ! [MHz]
      bandwidth  = real(abs(sky%head%spe%fres*sky%head%spe%nchan)),  &  ! [MHz]
      fShift     = 0.,                                               &  ! [MHz] ZZZ TBD
      receiver   = recname,                  &
      nChannels  = sky%head%spe%nchan,       &
      reset      = resetBe,                  &
      error      = error)
    if (error)  goto 100
    resetBe = .false.
    !
    partdone(ifront) = .true.
  enddo
  !
  ! Calibration results
  do iunsort=1,backcal%nset
    iset = sort(iunsort)  ! Use sorted index
    !
    ifront = backcal%sky%chunkset(iset,ipix)%chunks(1)%ifront  ! Frontend for this set
    !
    ! Stitch cold load chunks as a single spectrum:
    call mrtcal_chunkset_to_obs_ry(backcal%cold%chunkset(iset,ipix),cold,error)
    if (error)  return
    ! Stitch hot load chunks as a single spectrum:
    call mrtcal_chunkset_to_obs_ry(backcal%hot%chunkset(iset,ipix),hot,error)
    if (error)  return
    ! Stitch sky load chunks as a single spectrum:
    call mrtcal_chunkset_to_obs_ry(backcal%sky%chunkset(iset,ipix),sky,error)
    if (error)  return
    !
    if (fits%front%table%recname%val(ifront)(1:4).eq."HERA") then
      write(recname,'(A5,1X,A5,1X,A1)')   &
        fits%front%table%recname%val(ifront)(1:5),"Pixel",sky%head%gen%teles(12:12)
    else
      recname = sky%head%gen%teles(4:8)
    endif
    !
    ndata = cold%head%spe%nchan
    call gr4_mean(cold%data1,ndata,cold%head%spe%bad,0.,pcold)
    if (error)  goto 100
    ndata = hot%head%spe%nchan
    call gr4_mean(hot%data1,ndata,hot%head%spe%bad,0.,phot)
    if (error)  goto 100
    ndata = sky%head%spe%nchan
    call gr4_mean(sky%data1,ndata,sky%head%spe%bad,0.,psky)
    if (error)  goto 100
    !
    call results_to_VO_setCalibrationResults(                 &
      name           = recname,                               &
      frequency      = sky%head%spe%restf*1d-3,               &
      frequencyImage = sky%head%spe%image*1d-3,               &
      lineName       = sky%head%spe%line,                     &
      sideBand       = fits%front%table%sideband%val(ifront), &
      effForward     = sky%head%cal%foeff,                    &
      effBeam        = sky%head%cal%beeff,                    &  ! Probably not the correct one....
      gainImage      = sky%head%cal%gaini,                    &
      tempAmbient    = sky%head%cal%tchop,                    &  ! Thot, not Tambient
      tempCold       = sky%head%cal%tcold,                    &
      xOffsetNasmyth = foffx,                                 &
      yOffsetNasmyth = foffy,                                 &
      lcalof         = real(sky%head%pos%lamof*sec_per_rad),  &
      bcalof         = real(sky%head%pos%betof*sec_per_rad),  &
      h2omm          = sky%head%cal%h2omm,                    &
      trx            = sky%head%cal%trec,                     &
      tsys           = sky%head%gen%tsys,                     &
      tcal           = sky%head%cal%atfac,                    &  ! atfac is Tcal
      tatms          = sky%head%cal%tatms,                    &
      tatmi          = sky%head%cal%tatmi,                    &
      tauzen         = sky%head%cal%taus,                     &
      tauzenImage    = sky%head%cal%taui,                     &
      pcold          = pcold,                                 &
      phot           = phot,                                  &
      psky           = psky,                                  &
      reset          = resetCal,                              &
      error          = error)
    if (error)  goto 100
    resetCal = .false.
    !
  enddo ! iset
  call free_obs(cold)
  call free_obs(hot)
  call free_obs(sky)
  !
  call results_to_VO_writeReceivers(error)
  if (error)  goto 100
  call results_to_VO_writeBackends(error)
  if (error)  goto 100
  call results_to_VO_writeCalibrationResults(error)
  if (error)  goto 100
  call results_to_VO_writeMeasurementEnd(error)
  if (error)  goto 100
  call results_to_VO_writeEnd(error)
  if (error)  goto 100
  !
100 continue
  Close(lun)
  ier = gag_frelun(lun)
  !
end subroutine calibration_to_VO
