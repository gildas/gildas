subroutine mrtcal_find_comm(line,error)
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_find_comm
  use mrtcal_index_vars
  !---------------------------------------------------------------------
  ! @ private
  !  Support routine for command:
  !   MFIND
  ! Read the index file
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   ! Input command line
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='FIND'
  !
  mfound = 0
  !
  call mrtindex_find_comm(line,ix,cx,error)
  if (error)  return
  !
  mfound = cx%next-1
  kcurr_cx = 0  ! For READ ZERO|FIRST|LAST|NEXT|PREV
  kcurr_ix = 0
  !
  call sic_def_long('mdx%curr',kcurr_cx,0,1,.false.,error)
  !
end subroutine mrtcal_find_comm
