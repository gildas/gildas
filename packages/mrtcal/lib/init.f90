!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_init(error)
  use gbl_message
  use classic_api
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this => mrtcal_init
  use mrtcal_index_vars
  use mrtcal_buffers
  !----------------------------------------------------------------------
  ! @ private
  ! MRTCAL Initialization
  !----------------------------------------------------------------------
  logical, intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='INIT'
  integer :: ier
  integer(kind=4) :: dim
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_setup_debug_all(seve%d,error)
  if (error)  return
  !
  ier = sic_setlog('gag_help_mrtcal','gag_doc:hlp/mrtcal-help-mrtcal.hlp')
  if (ier.eq.0) then
    error=.true.
    return
  endif
  !
  call mrtindex_init(error)
  if (error)  return
  !
  call mrtcal_toclass_init(error)
  if (error)  return
  !
  ! Buffer allocation
  call reallocate_calib_scan(nbackends_mrtcal,rcalib%scan,error)
  if (error)  return
  call reallocate_science_scan(nbackends_mrtcal,rscience%scan,error)
  if (error)  return
  !
  ! Variables
  call sic_def_long('MFOUND',mfound,0,dim,.true.,error)
  call mrtindex_code2sic(error)
  if (error) return
  call mrtcal_setup_variable(error)
  if (error)  return
  !
end subroutine mrtcal_init
!
subroutine mrtcal_exit(error)
  use gbl_message
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this => mrtcal_exit
  use mrtcal_buffers
  use mrtcal_index_vars
  !----------------------------------------------------------------------
  ! @ private
  ! MRTCAL Cleaning on exit
  !----------------------------------------------------------------------
  logical, intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='EXIT'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_toclass_exit(error)
  ! if (error)  continue
  !
  ! Deallocate RFILE buffers
  call imbfits_free_leadhdus(rfile%imbf,error)
  call free_eclass_char(rfile%imbf%seclass,error)
  call imbfits_free_subscan_header(rfile%subscanbuf%subscan,error)
  call mrtcal_free_subscan_data(rfile%subscanbuf%databuf,error)
  !
  ! Deallocate RCALIB buffers
  call free_calib_scan(rcalib%scan,error)
  call deallocate_mrtoptimize(rcalib%idx,error)
  !
  ! Deallocate RSCIENCE buffers
  call free_science_scan(rscience%scan,error)
  call deallocate_mrtoptimize(rscience%idx,error)
  !
  ! Deallocate read-write buffers used for Index file
  call deallocate_mrtoptimize(ix,error)
  call deallocate_mrtoptimize(cx,error)
  !
  call mrtindex_exit(error)
  !
  ! Deallocate support variables for command VARIABLE
  call mrtindex_entry_fheader(kentry%head,error)
  !
end subroutine mrtcal_exit
!
subroutine mrtcal_load
  use mrtcal_interfaces, except_this => mrtcal_load
  !---------------------------------------------------------------------
  ! @ private
  ! Define and load the MRTCAL language
  !---------------------------------------------------------------------
  !
  integer, parameter :: mmrtcal=38
  character(len=12) :: vocab(mmrtcal)
  data vocab /                                                            &
    ' CALIBRATE', '/WITH',                                                &
    ' MDUMP', '/OUTPUT',                                                  &
    ' MFIND', '/DATE','/SCAN','/BACKEND','/OBSTYPE','/SOURCE','/PROJID',  &
              '/COMPLETENES','/CALIBRATED','/SWITCHMODE','/FRONTEND',     &
              '/POLARIMETRY',                                             &
    ' INDEX', '/FILE','/RECURSIVE','/PATTERN','/DATE','/TIMEOUT',         &
    ' MCOPY',                                                             &
    ' MLIST', '/TOC','/PAGE','/COLUMNS','/FILE','/VARIABLE',              &
    ' PIPELINE', '/SHOW',                                                 &
    ' READ', '/SUBSCAN','/FILENAME',                                      &
    ' MSETUP',                                                            &
    ' MUPDATE',                                                           &
    ' VARIABLE',                                                          &
    ' MWRITE' /
  !
  ! Load the new language
  call sic_begin(         &
    'MRTCAL',             &  ! Language name
    'GAG_HELP_MRTCAL',    &  !
    mmrtcal,              &  ! Number of commands + options in language
    vocab,                &  ! Array of commands + options
    '0.1',                &  ! Some version string
    mrtcal_run,           &  ! The routine which handles incoming commands
    mrtcal_error)            ! The error status routine of the library
  !
end subroutine mrtcal_load
!
subroutine mrtcal_run(line,command,error)
  use gbl_message
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this => mrtcal_run
  use mrtcal_index_vars
  !---------------------------------------------------------------------
  ! @ private-mandatory
  ! Support routine for the language MRTCAL.
  ! This routine is able to call the routines associated to each
  ! command of the language MRTCAL
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: line
  character(len=12), intent(in)    :: command
  logical,           intent(inout) :: error
  !
  call mrtcal_message(seve%c,'MRTCAL',line)
  !
  ! Call appropriate subroutine according to COMMAND
  select case(command)
  case('CALIBRATE')
    call mrtcal_calib_command(line,error)
  case('MCOPY')
    call mrtindex_copy_command(line,cx,error)
  case('MDUMP')
    call mrtcal_dump_command(line,error)
  case('MFIND')
    call mrtcal_find_comm(line,error)
  case('INDEX')
    call mrtcal_index_comm(line,error)
  case('MLIST')
    call mrtcal_list_comm(line,error)
  case('PIPELINE')
    call mrtcal_pipe_command(line,error)
  case('READ')
    call mrtcal_read_command(line,error)
  case('MSETUP')
    call mrtcal_setup_comm(line,error)
  case('MUPDATE')
    call mrtcal_update_command(line,error)
  case('VARIABLE')
    call mrtcal_variable_comm(line,error)
  case('MWRITE')
    call mrtcal_write_command(line,error)
  case default
    call mrtcal_message(seve%e,'MRTCAL_RUN','Unimplemented command '//command)
    error = .true.
  end select
  ! Reset ctrlc to .FALSE. in case SIC_CTRLC_STATUS was used (e.g., in mrtcal_calib_command) 
  if (sic_ctrlc()) error = .true.
  !
end subroutine mrtcal_run
!
function mrtcal_error()
  use mrtcal_interfaces, except_this => mrtcal_error
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for the error status of the library. 
  ! It is a scory. Please do not use it
  !---------------------------------------------------------------------
  logical :: mrtcal_error
  !
  mrtcal_error = .false.
  !
end function mrtcal_error
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
