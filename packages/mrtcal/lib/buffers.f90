!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module mrtcal_buffers
  use gildas_def
  use mrtcal_buffer_types
  use mrtcal_setup_types
  !
  ! Current global program behavior
  type(mrtcal_setup_t) :: rsetup
  !
  ! Current image of the imbfits file. SAVE it because Sic variables can
  ! be mapped on it.
  type(imbfits_buffer_t), save :: rfile
  !
  ! Current calibration parameters
  type(calib_t) :: rcalib
  !
  ! Current science data
  type(science_t) :: rscience
  !
end module mrtcal_buffers
!
module mrtcal_messaging
  use gbl_message
  !
  type :: mrtcal_debug_calib_message_t
    integer(kind=4) :: alloc   ! Support for MSET DEBUG CALIB ALLOCATION
    integer(kind=4) :: book    ! Support for MSET DEBUG CALIB BOOKKEEPING
    integer(kind=4) :: others  ! Support for MSET DEBUG CALIB OTHERS
  end type mrtcal_debug_calib_message_t
  !
  type :: mrtcal_debug_output_message_t
    integer(kind=4) :: acc   ! Support for MSET DEBUG OUTPUT ACCUMULATION
  end type mrtcal_debug_output_message_t
  !
  type :: mrtcal_debug_message_t
    type(mrtcal_debug_calib_message_t)  :: calib  ! Support for MSET DEBUG CALIB
    type(mrtcal_debug_output_message_t) :: output ! Support for MSET DEBUG OUTPUT
    integer(kind=4)                     :: sync   ! Support for MSET DEBUG SYNC
  end type mrtcal_debug_message_t
  !
  type(mrtcal_debug_message_t) :: mseve
  !
end module mrtcal_messaging
!
module mrtcal_index_vars
  use mrtindex_types
  use mrtindex_parse_types
  !
  type(mrtindex_optimize_t), target :: ix,cx
  !
  ! Current idx number, header, and filename for
  !  READ Filename|FileNum|ZERO|FIRST|LAST|NEXT|PREV
  ! Positions: kcurr_ix can be derived from kcurr_cx, but not easily in the other way
  integer(kind=entry_length) :: kcurr_cx=0  ! Position in CX for current entry
  integer(kind=entry_length) :: kcurr_ix=0  ! Position in IX for current entry
  type(mrtindex_entry_t), save :: kentry
  integer(kind=entry_length), save :: mfound=0   ! CX size, support for MFOUND
  character(len=filename_length) :: filecurr=""  ! Duplicate of khead%key%filename (full path)
  !
  ! Mrtcal User Section: LAS\FIND /USER criteria:
  type(user_find_t) :: userfind
  !
end module mrtcal_index_vars
