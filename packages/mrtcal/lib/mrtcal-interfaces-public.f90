module mrtcal_interfaces_public
  interface
    subroutine mrtcal_bookkeeping_all(subs,time,error)
      use imbfits_types
      use mrtcal_bookkeeping_types
      !---------------------------------------------------------------------
      ! @ public (for mrtholo)
      !  Fill the time%cur% structure suited for reading ALL the block
      ! of data at once.
      !  This is suited for applications which do not want to iterate the
      ! DATA block, i.e. reading it all at once. Use with caution, memory
      ! consumption may be large.
      !  This must come after a 'get_time_range_*' call (which fills the
      ! time%tot).
      !---------------------------------------------------------------------
      type(imbfits_subscan_t), intent(in)    :: subs
      type(book_t),            intent(inout) :: time
      logical,                 intent(inout) :: error
    end subroutine mrtcal_bookkeeping_all
  end interface
  !
  interface
    subroutine mrtcal_chunk_mjd_from_data(backdata,itime,mjd,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ public for mrtholo
      ! Set the MJD at the middle of the integration time. Caution: this
      ! value is used later for chunk position!
      !
      !  Tstamped:
      !    0.0 = MJD given at the beginning of the integration time
      !    0.5 = MJD given at the middle    of the integration time ???
      !    1.0 = MJD given at the end       of the integration time
      !---------------------------------------------------------------------
      type(imbfits_backdata_t), intent(in)    :: backdata  ! Backend Data table
      integer(kind=4),          intent(in)    :: itime     ! Chunk position in table
      real(kind=8),             intent(out)   :: mjd       ! Resulting MJD
      logical,                  intent(inout) :: error     ! Logical error flag
    end subroutine mrtcal_chunk_mjd_from_data
  end interface
  !
  interface
    subroutine mrtcal_pack_set(pack)
      use gpack_def
      !----------------------------------------------------------------------
      ! @ public-mandatory (because symbol is used elsewhere)
      !----------------------------------------------------------------------
      type(gpack_info_t), intent(out) :: pack
    end subroutine mrtcal_pack_set
  end interface
  !
  interface
    subroutine mrtcal_read_subscan_data(imbf,subs,tochunk,databuf,error)
      use gbl_message
      use mrtcal_buffer_types
      use mrtcal_setup_types
      !---------------------------------------------------------------------
      ! @ public
      ! Read part of the DATA column from the current subscan (i.e. the
      ! current one CFITSIO is positioned in), and implicitely convert it
      ! in chunksets.
      !---------------------------------------------------------------------
      type(imbfits_t),         intent(in)    :: imbf
      type(imbfits_subscan_t), intent(in)    :: subs
      logical,                 intent(in)    :: tochunk
      type(data_buffer_t),     intent(inout) :: databuf
      logical,                 intent(inout) :: error
    end subroutine mrtcal_read_subscan_data
  end interface
  !
  interface
    subroutine mrtcal_get_time_range_for_backend(subs,time,error)
      use gildas_def
      use imbfits_types
      use mrtcal_bookkeeping_types
      use mrtcal_messaging
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
      type(imbfits_subscan_t), intent(in)    :: subs
      type(range_t),           intent(out)   :: time
      logical,                 intent(inout) :: error
    end subroutine mrtcal_get_time_range_for_backend
  end interface
  !
  interface
    subroutine mrtcal_interp_coord_from_antslow(antslow,onsky,mjd,longoff,latoff,  &
      azimuth,elevation,lst,error)
      use gildas_def
      use phys_const
      use gbl_constant
      use gbl_message
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ public
      !  Interpolate
      !   - longoff, latoff
      !   - azimuth, elevation
      !   - lst
      !  from the antslow table for the given MJD time.
      !---------------------------------------------------------------------
      type(imbfits_antslow_t), intent(in)    :: antslow    !
      logical,                 intent(in)    :: onsky      !
      real(kind=8),            intent(in)    :: mjd        !
      real(kind=4),            intent(out)   :: longoff    ! [rad] ZZZ should check table unit
      real(kind=4),            intent(out)   :: latoff     ! [rad] ZZZ same
      real(kind=4),            intent(out)   :: azimuth    ! [rad] ZZZ same
      real(kind=4),            intent(out)   :: elevation  ! [rad] ZZZ same
      real(kind=8),            intent(out)   :: lst        ! [rad] ZZZ same
      logical,                 intent(inout) :: error      ! Logical error flag
    end subroutine mrtcal_interp_coord_from_antslow
  end interface
  !
end module mrtcal_interfaces_public
