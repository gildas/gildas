!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine antslow_mjd(mjd,antslow,ceil,itrace,error)
  use gbl_message
  use gildas_def
  use gkernel_interfaces
  use mrtcal_interfaces, except_this=>antslow_mjd
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  ! Search in the input 'AntSlow' the slow trace which has the nearest
  ! MJD. Return the index of this slow trace
  !---------------------------------------------------------------------
  real(kind=8),              intent(in)    :: mjd
  type(imbfits_antslow_t),   intent(in)    :: antslow
  logical,                   intent(in)    :: ceil
  integer(kind=size_length), intent(out)   :: itrace
  logical,                   intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='ANTSLOW>MJD'
  real(kind=8), parameter :: toler=1.d0/86400.d0 ! 1 second in fraction of the day
  integer(kind=size_length) :: nmjd,jtrace
  character(len=message_length) :: mess
  character(len=23) :: iso1,iso2,iso3
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  if (mjd.lt.(antslow%table%mjd%val(1)-toler) .or.  &
      mjd.gt.(antslow%table%mjd%val(antslow%table%mjd%n)+toler)) then
    call mrtcal_message(seve%e,rname,  &
      'Requesting a MJD out of range of the AntSlow table:')
    ! MJD:
    write(mess,'(F0.8,A,F0.8,1X,F0.8)')  &
      mjd,' not in range ',              &
      antslow%table%mjd%val(1),antslow%table%mjd%val(antslow%table%mjd%n)
    call mrtcal_message(seve%e,rname,mess)
    ! ISO DATE:
    call gag_mjd2isodate(mjd,iso1,error)
    call gag_mjd2isodate(antslow%table%mjd%val(1),iso2,error)
    call gag_mjd2isodate(antslow%table%mjd%val(antslow%table%mjd%n),iso3,error)
    write(mess,'(3A,1X,A)')  iso1,' not in range ',iso2,iso3
    call mrtcal_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  ! Find itrace
  nmjd = antslow%table%mjd%n ! *** JP It should be an implicit cast
  call mrtcal_dicho(rname,nmjd,antslow%table%mjd%val,mjd,ceil,toler,jtrace,error)
  itrace = jtrace ! *** JP There is a loss here
  if (error) then
    write(mess,'(A,F0.8,A)')  'Failed to find value ',mjd,' in MJD table'
    call mrtcal_message(seve%e,rname,mess)
    return
  endif
  !
end subroutine antslow_mjd
!
subroutine mrtcal_get_median_elevation(subs,medelev,error)
  use gbl_message
  use phys_const
  use mrtcal_buffer_types
  use mrtcal_interfaces, except_this => mrtcal_get_median_elevation
  !---------------------------------------------------------------------
  ! @ private
  !  Get the median elevation from CELEVATIO in AntSlow table, on track
  ! part.
  !---------------------------------------------------------------------
  type(imbfits_subscan_t), intent(in)    :: subs    !
  real(kind=8),            intent(out)   :: medelev ! [rad] double in contrast with CLASS
  logical,                 intent(inout) :: error   !
  ! Local
  type(range_t) :: ant_time
  character(len=*), parameter :: rname='GET>MEDIAN>ELEVATION'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  if (subs%onsky) then
    call mrtcal_get_time_range_for_antslow(subs,ant_time,error)
    if (error) return
    ! *** JP Right now use first element. But we should return 
    !        the median value over the time interval
    medelev = subs%antslow%table%celevatio%val(ant_time%first)
    !
  else
    ! This subscan is not observed on-sky (e.g. a hot or cold calibration
    ! subscan). In order to avoid troubles with antenna position tables
    ! (which we don't need), set a dummy value. Use zenith which gives an
    ! airmass 1 (0 would return an error for airmass).
    medelev = pi/2.d0
    !
  endif
  !
end subroutine mrtcal_get_median_elevation
!
subroutine mrtcal_get_time_range_for_antslow(subs,time,error)
  use gildas_def
  use gkernel_interfaces
  use imbfits_types
  use mrtcal_interfaces, except_this => mrtcal_get_time_range_for_antslow
  use mrtcal_bookkeeping_types
  use mrtcal_messaging
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(imbfits_subscan_t), intent(in)    :: subs
  type(range_t),           intent(out)   :: time
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='GET>TIME>RANGE>ANTSLOW'
  integer(kind=size_length) :: ibeg,iend
  character(len=message_length) :: mess
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call antslow_mjd(subs%antslow%head%mjd_beg%val,subs%antslow,.true.,ibeg,error)
  if (error)  return
  call antslow_mjd(subs%antslow%head%mjd_end%val,subs%antslow,.false.,iend,error)
  if (error)  return
  time%first  = ibeg
  time%last   = iend
  time%n      = (iend-ibeg)+1
  time%vfirst = subs%antslow%table%mjd%val(ibeg)
  time%vlast  = subs%antslow%table%mjd%val(iend)
  !
  ! User feedback
  write(mess,100)  'Subscan #',subs%isub,' starts at ',  &
    trim(subs%antslow%head%date_obs%val),'=',subs%antslow%head%mjd_beg%val,  &
    ', first dump at ',subs%antslow%table%mjd%val(ibeg),'=',ibeg
  call mrtcal_message(mseve%sync,rname,mess)
  write(mess,100)  'Subscan #',subs%isub,' stops  at ',  &
    trim(subs%antslow%head%date_end%val),'=',subs%antslow%head%mjd_end%val,  &
    ', last  dump at ',subs%antslow%table%mjd%val(iend),'=',iend
  call mrtcal_message(mseve%sync,rname,mess)
  !
100 format(A,I0,A,A,A,F0.8,A,F0.8,A,I0)
end subroutine mrtcal_get_time_range_for_antslow
!
subroutine mrtcal_get_time_range_for_antfast(subs,time,error)
  use gildas_def
  use gkernel_interfaces
  use imbfits_types
  use mrtcal_interfaces, except_this => mrtcal_get_time_range_for_antfast
  use mrtcal_bookkeeping_types
  use mrtcal_messaging
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(imbfits_subscan_t), intent(in)    :: subs
  type(range_t),           intent(out)   :: time
  logical,                 intent(inout) :: error
  ! Local
  real(kind=8) :: toler
  integer(kind=size_length) :: nmjd,ibeg,iend
  character(len=message_length) :: mess
  character(len=*), parameter :: rname='GET>TIME>RANGE>ANTFAST'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  ! There may be much less antfast data than foreseen and we may still do
  ! something with depending on the situation. So set the tolerance to the
  ! duration of the subscan in fraction of the day.
  toler=subs%antslow%head%substime%val/86400.0
  nmjd = subs%antfast%table%mjd%n ! We have n mjd values from the table
  call mrtcal_dicho(rname,nmjd,subs%antfast%table%mjd%val,  &
    subs%antfast%head%mjd_beg%val,.true.,toler,ibeg,error)
  if (error) then
    write(mess,'(A,F0.8,A)')  &
      'Failed to find value ',subs%antfast%head%mjd_beg%val,' in MJD table'
    call mrtcal_message(seve%e,rname,mess)
    return
  endif
  call mrtcal_dicho(rname,nmjd,subs%antfast%table%mjd%val,  &
    subs%antfast%head%mjd_end%val,.false.,toler,iend,error)
  if (error) then
    write(mess,'(A,F0.8,A)')  &
      'Failed to find value ',subs%antfast%head%mjd_end%val,' in MJD table'
    call mrtcal_message(seve%e,rname,mess)
    return
  endif
  time%first  = ibeg
  time%last   = iend
  time%n      = (iend-ibeg)+1
  time%vfirst = subs%antfast%table%mjd%val(ibeg)
  time%vlast  = subs%antfast%table%mjd%val(iend)
  !
  ! User feedback
  write(mess,100)  'Subscan #',subs%isub,' starts at ',  &
    trim(subs%antfast%head%date_obs%val),'=',subs%antfast%head%mjd_beg%val,  &
    ', first dump at ',subs%antfast%table%mjd%val(ibeg),'=',ibeg
  call mrtcal_message(mseve%sync,rname,mess)
  write(mess,100)  'Subscan #',subs%isub,' stops  at ',  &
    trim(subs%antfast%head%date_end%val),'=',subs%antfast%head%mjd_end%val,  &
    ', last dump  at ',subs%antfast%table%mjd%val(iend),'=',iend
  call mrtcal_message(mseve%sync,rname,mess)
  !
100 format(A,I0,A,A,A,F0.8,A,F0.8,A,I0)
end subroutine mrtcal_get_time_range_for_antfast
!
subroutine mrtcal_get_time_range_for_backend(subs,time,error)
  use gildas_def
  use gkernel_interfaces
  use imbfits_types
  use mrtcal_interfaces, except_this => mrtcal_get_time_range_for_backend
  use mrtcal_bookkeeping_types
  use mrtcal_messaging
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  type(imbfits_subscan_t), intent(in)    :: subs
  type(range_t),           intent(out)   :: time
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='GET>TIME>RANGE>BACKEND'
  real(kind=8) :: toler
  integer(kind=size_length) :: nmjd,ibeg,iend
  character(len=message_length) :: mess
  real(kind=8), allocatable :: mjd(:)
  integer(kind=4) :: ier
  real(kind=4) :: integtim
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  if (subs%backdata%table%mjd%n.eq.0) then
    ! Special case of zero-sized backend tables
    write (mess,'(A,I2,A)')  &
      'Backend table is zero-sized for subscan ',subs%isub,' (no dumps)'
    call mrtcal_message(seve%w,rname,mess)
    call nullify_range_t(time)
    return
  endif
  !
  ! There may be much less backend data than foreseen and we may still do
  ! something with depending on the situation (e.g. OTF ON source scans).
  ! So set the tolerance to the duration of the subscan in fraction of the day.
  toler=subs%antslow%head%substime%val/86400.0
  nmjd = subs%backdata%table%mjd%n ! We have n mjd values from the table
  !
  ! Compute the start and end of each time dump. We have their MJD(N), their
  ! INTEGTIM(N), and how INTEGTIM applies thanks to TSTAMPED:
  !    0.0 = MJD(i) given at the beginning of the integration time
  !    0.5 = MJD(i) given at the middle    of the integration time ???
  !    1.0 = MJD(i) given at the end       of the integration time
  allocate(mjd(nmjd),stat=ier)
  if (failed_allocate(rname,'mjd buffer',ier,error)) return
  !
  ! Search first time dump fully on-track. MJD_BEG(N) =
  mjd(1:nmjd) = subs%backdata%table%mjd%val(1:nmjd) +  &
                (0.0d0-subs%backdata%head%tstamped%val) *  &
                subs%backdata%table%integtim%val(1:nmjd)/86400.d0
  call mrtcal_dicho(rname,nmjd,mjd,subs%backdata%head%mjd_beg%val,.true.,  &
    toler,ibeg,error)
  if (error) then
    write(mess,'(A,F0.8,A)')  &
      'Failed to find value ',subs%antfast%head%mjd_beg%val,' in MJD table'
    call mrtcal_message(seve%e,rname,mess)
    return
  endif
  time%first = ibeg
  time%vfirst = mjd(ibeg)  ! Beginning of integration of first dump
  !
  ! Search last time dump fully on-track. MJD_END(N) =
  mjd(1:nmjd) = subs%backdata%table%mjd%val(1:nmjd) +  &
                (1.0d0-subs%backdata%head%tstamped%val) *  &
                subs%backdata%table%integtim%val(1:nmjd)/86400.d0
  call mrtcal_dicho(rname,nmjd,mjd,subs%backdata%head%mjd_end%val,.false.,  &
    toler,iend,error)
  if (error) then
    write(mess,'(A,F0.8,A)')  &
      'Failed to find value ',subs%antfast%head%mjd_end%val,' in MJD table'
    call mrtcal_message(seve%e,rname,mess)
    return
  endif
  time%last = iend
  time%vlast = mjd(iend)  ! End of integration of last dump
  !
  if (iend.lt.ibeg) then
    ! This can happen is mjd_beg and mjd_end are so close that there is
    ! no full dump in between.
    write (mess,'(A,I2,A)')  &
      'Backend table for subscan ',subs%isub,' has no fully on-track dumps'
    call mrtcal_message(seve%w,rname,mess)
    call nullify_range_t(time)
    return
  endif
  time%n = (iend-ibeg)+1
  !
  deallocate(mjd)
  !
  ! User feedback
  write(mess,100)  'Subscan #',subs%isub,' starts at ',  &
    trim(subs%backdata%head%date_obs%val),'=',subs%backdata%head%mjd_beg%val,  &
    ', first dump at ',subs%backdata%table%mjd%val(ibeg),'=',ibeg
  call mrtcal_message(mseve%sync,rname,mess)
  write(mess,100)  'Subscan #',subs%isub,' stops  at ',  &
    trim(subs%backdata%head%date_end%val),'=',subs%backdata%head%mjd_end%val,  &
    ', last  dump at ',subs%backdata%table%mjd%val(iend),'=',iend
  call mrtcal_message(mseve%sync,rname,mess)
  !
  integtim = sum(subs%backdata%table%integtim%val(time%first:time%last))
  if (integtim.lt.subs%antslow%head%substime%val) then
    write(mess,'(A,I0,A,F0.2,A,I0,A,F0.2,A)')  'Subscan #',subs%isub,  &
      ' on-track integration time is ',integtim,' sec (',time%n,  &
      ' dumps), while commanded subscan duration is ',  &
      subs%antslow%head%substime%val,' sec'
    call mrtcal_message(seve%w,rname,mess)
  endif
  !
100 format(A,I0,A,A,A,F0.8,A,F0.8,A,I0)
  !
contains
  subroutine nullify_range_t(t)
    type(range_t), intent(out) :: t
    t%first = 0
    t%last = 0
    t%n = 0
    t%i = 0
    t%vfirst = 0.d0
    t%vlast = 0.d0
  end subroutine nullify_range_t
  !
end subroutine mrtcal_get_time_range_for_backend
!
subroutine mrtcal_get_time_range_for_all(subs,time,error)
  use imbfits_types
  use mrtcal_interfaces, except_this => mrtcal_get_time_range_for_all
  use mrtcal_bookkeeping_types
  !---------------------------------------------------------------------
  ! @ private
  !  Select the whole time range (on-track and off-track)
  !---------------------------------------------------------------------
  type(imbfits_subscan_t), intent(in)    :: subs
  type(range_t),           intent(out)   :: time
  logical,                 intent(inout) :: error
  ! Local
  integer(kind=size_length) :: nmjd
  !
  nmjd = subs%backdata%table%mjd%n ! We have n mjd values from the table
  time%first  = 1
  time%last   = nmjd
  time%n      = nmjd
  time%vfirst = subs%backdata%table%mjd%val(1)
  time%vlast  = subs%backdata%table%mjd%val(nmjd)
  !
end subroutine mrtcal_get_time_range_for_all
!
subroutine mrtcal_dicho(rname,np,x,xval,ceil,toler,ival,error)
  use gildas_def
  use gbl_message
  use mrtcal_interfaces, except_this=>mrtcal_dicho
  use mrtcal_messaging
  !---------------------------------------------------------------------
  ! @ private
  ! Find ival such as
  !    X(ival-1) < xval <= X(ival)     (ceiling mode)
  !  or
  !    X(ival) <= xval < X(ival+1)     (floor mode)
  ! Suited for MJD values only.
  ! The algorithm (dichotomic search) assumes the input data is
  ! increasingly ordered.
  !---------------------------------------------------------------------
  character(len=*),          intent(in)    :: rname  ! Calling routine name
  integer(kind=size_length), intent(in)    :: np     ! Number of input points
  real(kind=8),              intent(in)    :: x(np)  ! Input ordered Values
  real(kind=8),              intent(in)    :: xval   ! The value we search for
  logical,                   intent(in)    :: ceil   ! Ceiling or floor mode?
  real(kind=8),              intent(in)    :: toler  ! Acceptable error
  integer(kind=size_length), intent(out)   :: ival   ! Position in the array
  logical,                   intent(inout) :: error  ! Logical error flag
  ! Local
  real(kind=8) :: dx
  character(len=message_length) :: mess
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  ! Sanity check. Keep test basic to remain efficient
  if (x(1).gt.x(np)) then
    write(mess,'(A,F0.8,A,F0.8,A)')  &
      'Input array is not ordered (first: ',x(1),', last: ',x(np),')'
    call mrtcal_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  if (x(1).gt.xval .or. x(np).lt.xval) then
     if (x(1).gt.xval) then
        dx = (x(1)-xval)*86400.0 ! *** JP 86400 should be a parameter in phys_const
        ival=1
     else if (x(np).lt.xval) then 
        dx = (xval-x(np))*86400.0 ! *** JP 86400 should be a parameter in phys_const
        ival=np
     endif
     write(mess,'(A,F0.8,A,F0.8,A,F0.8,A,F0.2,A,F0.2,A)')  &
          'Input value (',xval,') out of array range (', &
          x(1),' to ',x(np),') by ',dx,'s (tolerance = ', &
          toler*86400,'s)'
     ! Take care here to compute the correct test. From a numerical point
     ! of view, we can have xval.eq.x(1)-toler but x(1)-xval.ne.toler !!!
     if ( (xval.ge.x(1)-toler) .and. (xval.le.x(np)+toler) ) then
        ! Inside tolerance => Just a debug-synchro
        call mrtcal_message(mseve%sync,rname,mess)
     else
        ! Outside tolerance => An error
        call mrtcal_message(seve%e,rname,mess)
        error = .true.
        return
     endif
  else
     call gr8_dicho(np,x,xval,ceil,ival,error)
     return
  endif
  !
end subroutine mrtcal_dicho
!
subroutine mrtcal_interp_coord_from_antslow(antslow,onsky,mjd,longoff,latoff,  &
  azimuth,elevation,lst,error)
  use gildas_def
  use phys_const
  use gbl_constant
  use gbl_message
  use gkernel_interfaces
  use imbfits_types
  use mrtcal_interfaces, except_this=>mrtcal_interp_coord_from_antslow
  !---------------------------------------------------------------------
  ! @ public
  !  Interpolate
  !   - longoff, latoff
  !   - azimuth, elevation
  !   - lst
  !  from the antslow table for the given MJD time.
  !---------------------------------------------------------------------
  type(imbfits_antslow_t), intent(in)    :: antslow    !
  logical,                 intent(in)    :: onsky      !
  real(kind=8),            intent(in)    :: mjd        !
  real(kind=4),            intent(out)   :: longoff    ! [rad] ZZZ should check table unit
  real(kind=4),            intent(out)   :: latoff     ! [rad] ZZZ same
  real(kind=4),            intent(out)   :: azimuth    ! [rad] ZZZ same
  real(kind=4),            intent(out)   :: elevation  ! [rad] ZZZ same
  real(kind=8),            intent(out)   :: lst        ! [rad] ZZZ same
  logical,                 intent(inout) :: error      ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='GET>OFFSET>FROM>ANTSLOW'
  real(kind=8), parameter :: rad_per_second=2d0*pi/86400d0
  real(kind=8) :: frac
  integer(kind=size_length) :: nmjd,jtrace
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  if (.not.onsky) then
    ! Subscan is not on sky: antenna position tables have recurrent problems
    ! in this case. Set offsets and others to null value.
    longoff   = 0.
    latoff    = 0.
    azimuth   = 0.
    elevation = pi/2.
    lst       = 0.
    return
  endif
  !
  call antslow_mjd(mjd,antslow,.false.,jtrace,error)
  if (error)  return
  !
  nmjd = antslow%table%mjd%n
  if (mjd.lt.antslow%table%mjd%val(1)) then
    ! Out of bounds => use lower bound value. Should never happen, such
    ! dumps are normally out of tracking.
    longoff   = antslow%table%longoff%val(1)
    latoff    = antslow%table%latoff%val(1)
    azimuth   = antslow%table%cazimuth%val(1)
    elevation = antslow%table%celevatio%val(1)
    lst       = antslow%table%lst%val(1)
    !
  elseif (mjd.gt.antslow%table%mjd%val(nmjd) .or.  &
          jtrace.eq.nmjd) then
    ! Out of bounds / on upper bound => use upper bound value. Should never
    ! happen, such dumps are normally out of tracking
    longoff   = antslow%table%longoff%val(nmjd)
    latoff    = antslow%table%latoff%val(nmjd)
    azimuth   = antslow%table%cazimuth%val(nmjd)
    elevation = antslow%table%celevatio%val(nmjd)
    lst       = antslow%table%lst%val(nmjd)
    !
  else
    ! Interpolate. No problem if 'mjd' is exactly on 'jtrace' (frac = 0)
    frac = (mjd                             - antslow%table%mjd%val(jtrace))/  &
           (antslow%table%mjd%val(jtrace+1) - antslow%table%mjd%val(jtrace))
    longoff   = antslow%table%longoff%val(jtrace) + frac *  &
               (antslow%table%longoff%val(jtrace+1)-antslow%table%longoff%val(jtrace))
    latoff    = antslow%table%latoff%val(jtrace) + frac *  &
               (antslow%table%latoff%val(jtrace+1)-antslow%table%latoff%val(jtrace))
    azimuth   = antslow%table%cazimuth%val(jtrace) + frac *  &
               (antslow%table%cazimuth%val(jtrace+1)-antslow%table%cazimuth%val(jtrace))
    elevation = antslow%table%celevatio%val(jtrace) + frac *  &
               (antslow%table%celevatio%val(jtrace+1)-antslow%table%celevatio%val(jtrace))
    lst       = antslow%table%lst%val(jtrace) + frac *  &
               (antslow%table%lst%val(jtrace+1)-antslow%table%lst%val(jtrace))
  endif
  lst = lst*rad_per_second ! Conversion to CLASS unit
  !
end subroutine mrtcal_interp_coord_from_antslow
!
subroutine mrtcal_get_dewang_from_derot(front,derot,mjd,dewang,error)
  use gbl_message
  use imbfits_types
  use mrtcal_interfaces, except_this=>mrtcal_get_dewang_from_derot
  !---------------------------------------------------------------------
  ! @ private
  !  Interpolate the derotator angle from the derotator table, according
  ! to the input MJD
  !---------------------------------------------------------------------
  type(imbfits_front_t), intent(in)    :: front   !
  type(imbfits_derot_t), intent(in)    :: derot   !
  real(kind=8),          intent(in)    :: mjd     !
  real(kind=8),          intent(out)   :: dewang  ! [Derot table unit]
  logical,               intent(inout) :: error   ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='GET>DEWANG>FROM>DEROT'
  real(kind=8) :: mjddist,frac
  real(kind=8), parameter :: mjdtol=5.d0/8.64d4  ! [day] 5 seconds = normal sampling rate
  real(kind=8), parameter :: angtol=0.5d0  ! [deg]
  integer(kind=size_length) :: nmjd,jtrace
  character(len=message_length) :: mess
  logical :: dointerp
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  nmjd = derot%head%desc%naxis2%val
  if (nmjd.eq.0) then
    call mrtcal_message(seve%w,rname,  &
      'Empty derotator table, actual dewar angle defaults to commanded one')
    ! Check system from header
    if (bad_system(front%head%dewrtmod%val,'sky',error))  return
    dewang = front%head%dewang%val
    return
  else
    if (derot%head%mjd_beg%val.gt.derot%table%mjd%val(nmjd) .or. &
        derot%head%mjd_end%val.lt.derot%table%mjd%val(1)) then
      call mrtcal_message(seve%w,rname,  &
        'Derotator table has no value within the subscan range')
      ! There will be another warning if the value is too far
    endif
    if (mjd.lt.derot%table%mjd%val(1)) then
      ! Nearest value, no interpolation. There will be a warning
      ! if the value is too far
      jtrace = 1
      dointerp = .false.
    elseif (mjd.gt.derot%table%mjd%val(nmjd)) then
      ! Nearest value, no interpolation. There will be a warning
      ! if the value is too far
      jtrace = nmjd
      dointerp = .false.
    else
      ! We have values before and after (no need for a tolerance
      ! => 0). There will be a warning if they are too spaced
      call mrtcal_dicho(rname,nmjd,derot%table%mjd%val,mjd,.false.,0.d0,jtrace,error)
      if (error)  return
      dointerp = .true.
    endif
  endif
  !
  ! No system check from table: we assume the 'sAct' column is valid
  ! i.e. consistant with the 2 other columns from modes f and h.
  ! if (bad_system(derot%table%system%val(jtrace),'s',error))  return
  ! ... but raise a warning for now, as long as we are not fully sure:
  if (derot%table%system%val(jtrace).ne.'s') then
    call mrtcal_message(seve%w,rname,'Dewar tracking system '''//  &
      trim(derot%table%system%val(jtrace))//''' has experimental support')
  endif
  !
  if (.not.dointerp .or. jtrace.eq.nmjd) then
    ! On upper bound (=> use upper bound value), or only 1 value,
    ! or beyond values
    dewang = derot%table%sact%val(jtrace)
    mjddist = abs(mjd-derot%table%mjd%val(jtrace))
  else
    frac = (mjd                           - derot%table%mjd%val(jtrace))/  &
           (derot%table%mjd%val(jtrace+1) - derot%table%mjd%val(jtrace))
    dewang = derot%table%sact%val(jtrace) + frac *  &
            (derot%table%sact%val(jtrace+1)-derot%table%sact%val(jtrace))
    if (frac.lt.0.5d0) then
      mjddist = mjd - derot%table%mjd%val(jtrace)
    else
      mjddist = derot%table%mjd%val(jtrace+1) - mjd
    endif
  endif
  !
  ! Warn about points too far by more than tolerance
  if (mjddist.gt.mjdtol) then
    write(mess,'(A,F0.1,A)')  &
      'Nearest derotator value is at ',mjddist*86400,' sec'
    call mrtcal_message(seve%w,rname,mess)
  endif
  !
  if (front%head%dewrtmod%val.eq.'sky') then
    ! If commanded angle is in sky mode, we can compare the commanded value
    ! with the actual one.
    if (abs(dewang-front%head%dewang%val).gt.angtol) then
      write(mess,'(A,F0.2,A)')  'Actual dewar angle away by ',  &
        dewang-front%head%dewang%val,' degrees from commanded value'
      call mrtcal_message(seve%w,rname,mess)
    endif
  endif
  !
contains
  !
  function bad_system(in,ref,error)
    logical :: bad_system
    character(len=*), intent(in)    :: in,ref
    logical,          intent(inout) :: error
    if (in.ne.ref) then
      call mrtcal_message(seve%e,rname,'Dewar tracking system '''//  &
        trim(in)//''' is not supported')
      error = .true.
    endif
    bad_system = error
  end function bad_system
  !
end subroutine mrtcal_get_dewang_from_derot
