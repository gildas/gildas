!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_calib_otf_fsw(mrtset,backcal,backsci,filebuf,error)
  use gbl_message
  use gkernel_interfaces
  use mrtcal_buffer_types
  use mrtcal_setup_types
  use mrtcal_interfaces, except_this => mrtcal_calib_otf_fsw
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(mrtcal_setup_t),    intent(in)    :: mrtset
  type(calib_backend_t),   intent(in)    :: backcal
  type(science_backend_t), intent(inout) :: backsci
  type(imbfits_buffer_t),  intent(inout) :: filebuf
  logical,                 intent(inout) :: error
  ! Local
  integer(kind=4) :: ion,non
  character(len=*), parameter :: rname='CALIB>OTF>FSW'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_init_onloop(backsci,non,error)
  if (error) return
  do ion=1,non
     call mrtcal_get_nexton(backsci,error)
     if (error) return
     call mrtcal_init_dumpcycle_loop('onTheFly',mrtset,filebuf,backsci,error)
     if (error) return
     do while (backsci%switch%book%idump.lt.backsci%switch%book%ndump)
        call mrtcal_get_next_dumpcycle(mrtset,filebuf,backsci,error)
        if (error) return
        if (backsci%switch%book%found) then
           call mrtcal_on_minus_off(isotfmap,isfsw,backsci,error)
           if (error) return
           call mrtcal_tscale_computation(backcal,backsci,error)
           if (error) return
           call mrtcal_tscale_application(backsci%tscale,backsci%diff,error)
           if (error) return
           call mrtcal_write_toclass(backsci%diff,mrtset%out,backsci%nspec,error)
           if (error) return
        endif
     enddo ! switch%book%idump
     call mrtcal_switch_book_list(backsci%switch%book,error)
     if (error) return
     ! User can interrupt the pipeline
     if (sic_ctrlc_status()) then
        error = .true.
        exit
     endif
  enddo !ion
  !
end subroutine mrtcal_calib_otf_fsw
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
