!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_dump_command(line,error)
  use gbl_message
  use gildas_def
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this => mrtcal_dump_command
  use mrtcal_buffers
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command DUMP
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DUMP>COMMAND'
  logical :: all,short
  integer(kind=4), parameter :: nhdu=12
  character(len=10) :: argum,hduname,hdulist(nhdu)
  character(len=key_length) :: column
  integer(kind=4) :: ihdu,nc,ier,olun
  character(len=filename_length) :: fich,ofile
  data hdulist  &
    / 'ALL','SUMMARY','SUBSCANS','PRIMARY','SCAN','FRONTEND','BACKEND',  &
      'BACKDATA','ANTSLOW','ANTFAST','DEROT','DATA' /
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  if (rfile%imbf%file%unit.eq.0) then
    call mrtcal_message(seve%w,rname,'No file opened in buffer')
    return
  endif
  !
  ! Get table name (optional)
  if (sic_present(0,1)) then
     argum = ' '
     call sic_ke(line,0,1,argum,nc,.true.,error)
     if (error) return
     call sic_ambigs(rname,argum,hduname,ihdu,hdulist,nhdu,error)
     if (error) return
     short = .false.
  else
     hduname = 'ALL'
     ihdu = 1
     short = .true.
  endif
  all = hduname.eq.'ALL'
  ! Get column name (optional)
  column = ''
  call sic_ke(line,0,2,column,nc,.false.,error)
  if (error) return
  !
  ! Get output file (optional)
  if (sic_present(1,0)) then
    call sic_ch(line,1,1,fich,nc,.true.,error)
    if (error) return
    ier = sic_getlun(olun)
    if (mod(ier,2).eq.0) then
      call putmsg('E-DUMP, ',ier)
      error=.true.
      return
    endif
    call sic_parsef(fich,ofile,' ','.dat')
    ier = sic_open(olun,ofile,'NEW',.false.)
    if (ier.ne.0) then
      call putios('E-DUMP, ',ier)
      error = .true.
      return
    endif
  else
    olun = 6  ! STDOUT
  endif
  !
  ! DUMP the R buffer
  ! Summary: always
  call imbfits_dump_summary(rfile%imbf,olun,error)
  if (error) return
  if (all .or. hduname.eq.'SUBSCANS') then
    call imbfits_dump_subscans(rfile%imbf,olun,error)
    if (error) return
  endif
  if (all .or. hduname.eq.'PRIMARY') then
    call imbfits_dump_primary(rfile%imbf%primary,short,olun,error)
    if (error) return
  endif
  if (all .or. hduname.eq.'SCAN') then
    call imbfits_dump_scan(rfile%imbf%scan,column,short,olun,error)
    if (error) return
  endif
  if (all .or. hduname.eq.'FRONTEND') then
    call imbfits_dump_frontend(rfile%imbf%front,column,short,olun,error)
    if (error) return
  endif
  if (all .or. hduname.eq.'BACKEND') then
    call imbfits_dump_backend(rfile%imbf%back,column,short,olun,error)
    if (error) return
  endif
  if (all .or. hduname.eq.'DEROT') then
    call imbfits_dump_derot(rfile%imbf%derot,column,short,olun,error)
    if (error) return
  endif
  if (all .or. hduname.eq.'BACKDATA') then
    call imbfits_dump_backdata(rfile%subscanbuf%subscan%isub,  &
      rfile%subscanbuf%subscan%backdata,column,short,olun,error)
    if (error) return
  endif
  if (all .or. hduname.eq.'ANTSLOW') then
    call imbfits_dump_antslow(rfile%subscanbuf%subscan%isub,  &
      rfile%subscanbuf%subscan%antslow,column,short,olun,error)
    if (error) return
  endif
  if (all .or. hduname.eq.'ANTFAST') then
    call imbfits_dump_antfast(rfile%subscanbuf%subscan%isub,  &
      rfile%subscanbuf%subscan%antfast,column,short,olun,error)
    if (error) return
  endif
  if (all .or. hduname.eq.'DATA') then
    call mrtcal_imbfits_dump_data(rfile%subscanbuf%subscan%isub,  &
      rfile%subscanbuf%databuf,rfile%imbf%back%chunks%eclass,  &
      short,olun,error)  ! ZZZ merge arguments for 'databuf'
    if (error) return
  endif
  !
  if (olun.ne.6) then
    ier = sic_close(olun)
    if (ier.ne.0) then
      call putios('E-DUMP, ',ier)
      error = .true.
      ! continue
    endif
    !
    ! Free the unit
    call sic_frelun(olun)
  endif
  !
end subroutine mrtcal_dump_command
!
subroutine mrtcal_imbfits_dump_data(isub,databuf,ceclass,short,olun,error)
  use gbl_message
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this => mrtcal_imbfits_dump_data
  use mrtcal_buffer_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4),           intent(in)    :: isub     ! Subscan number
  type(data_buffer_t),       intent(in)    :: databuf  !
  type(eclass_2inte2char_t), intent(in)    :: ceclass  ! Chunkset equiv classes
  logical,                   intent(in)    :: short    ! Short or verbose output?
  integer(kind=4),           intent(in)    :: olun     ! Output LUN
  logical,                   intent(inout) :: error    ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='IMBFITS>DUMP>DATA'
  !
  write(olun,'(A,I0,A)') '--- Data (subscan #',isub,') ---'
  !
  if (.not.associated(databuf%imbf%val)) then
    write(olun,'(A)')  'No data available'
    write(olun,'(1X)')
    return
  endif
  !
  if (short) then
    write(olun,'(A)')  'Data available'  ! ZZZ improve this
    write(olun,'(1X)')
    return
  endif
  !
  call imbfits_dump_imbfdata(databuf%imbf,olun,error)
  if (error)  return
  !
  write(olun,'(A)')
  write(olun,'(A)') '  mapped into'
  write(olun,'(A)')
  !
  call mrtcal_imbfits_dump_mrtcdata(databuf%mrtc,ceclass,olun,error)
  if (error)  return
  !
  write(olun,'(1X)')
  !
end subroutine mrtcal_imbfits_dump_data
!
subroutine mrtcal_imbfits_dump_mrtcdata(mrtc,ceclass,olun,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this => mrtcal_imbfits_dump_mrtcdata
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(chunkset_3d_t),       intent(in)    :: mrtc     !
  type(eclass_2inte2char_t), intent(in)    :: ceclass  ! Chunkset equiv classes
  integer(kind=4),           intent(in)    :: olun     ! Output LUN
  logical,                   intent(inout) :: error    !
  ! Local
  character(len=*), parameter :: rname='IMBFITS>DUMP>MRTCDATA'
  integer(kind=4) :: iset,ichunk,nchar,nchunks
  character(len=message_length) :: string
  !
  write(olun,'(A,I8,A)')  &
    '   ',mrtc%nset,' chunk sets (gathered by PART+PIXEL+RECEIVER+POLAR):'
  !
  nchunks = 0
  do iset=1,mrtc%nset
    nchunks = nchunks+mrtc%chunkset(iset,1,1)%n
    !
    write(string,'(T13,I0,A,I0,A,A,A,A,A,I0,A,A,A)')  &
      ceclass%val1(iset),'+',ceclass%val2(iset),'+',trim(ceclass%val3(iset)),  &
      '+',trim(ceclass%val4(iset)),' = ',ceclass%cnt(iset),' chunks (',        &
      mrtc%chunkset(iset,1,1)%chunks(1)%gen%teles,', width'
    nchar = len_trim(string)
    do ichunk=1,mrtc%chunkset(iset,1,1)%n
      write(string(nchar+1:),'(1X,I0,A,F0.1)')  &
        mrtc%chunkset(iset,1,1)%chunks(ichunk)%spe%nchan,'/',  &
        abs(mrtc%chunkset(iset,1,1)%chunks(ichunk)%spe%nchan *  &
            mrtc%chunkset(iset,1,1)%chunks(ichunk)%spe%fres)
      nchar = len_trim(string)
    enddo
    string(nchar+1:) = ' chans/MHz)'
    write(olun,'(A)')  trim(string)
  enddo
  !
  write(olun,'(A,I8,A)')  &
    ' x ',mrtc%npix,' pixel(s)'
  write(olun,'(A,I8,A,I0)')  &
    ' x ',mrtc%ntime,' time dumps'
  write(olun,'(A,I8,A,I0,A)')  &
    ' = ',size(mrtc%chunkset),' spectra (containing ',nchunks*mrtc%npix*mrtc%ntime,' chunks)'
  !
end subroutine mrtcal_imbfits_dump_mrtcdata
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! subroutine mrtcal_imbfits_dump_tracking(imbf,error)
!   use gildas_def
!   use mrtcal_dependencies_interfaces
!   use mrtcal_interfaces, except_this=>mrtcal_imbfits_dump_tracking
!   use imbfits_types
!   use mrtcal_bookkeeping_types
!   !---------------------------------------------------------------------
!   ! @ private
!   !  Display tracking errors
!   !---------------------------------------------------------------------
!   type(imbfits_t), intent(inout) :: imbf
!   logical,         intent(inout) :: error
!   ! Local
!   integer(kind=4) :: isub
!   type(imbfits_subscan_t) :: subs
!   integer(kind=4) :: npts,ntrace,nline,iline,first,last
!   real(kind=8), pointer :: myx(:),myaz(:)
!   character(len=128) :: chain
!   real(kind=8) :: ymin,ymax,yrange
!   !
!   npts = 0
!   do isub=1,imbf%primary%n_obsp%val
!     call imbfits_read_subscan_header_bynum(imbf,isub,subs,error)
!     if (error)  return
!     !
!     ntrace = 128  ! subs%antfast%head%tracerat%val not yet defined
!     nline = subs%antfast%head%desc%naxis2%val
!     !
!     call reallocate_dble_1d(myaz,npts+nline,.true.,error)
!     if (error)  return
!     call reallocate_dble_1d(myx,npts+nline,.true.,error)
!     if (error)  return
!     !
!     do iline=1,nline
!       first = ntrace*(iline-1)+1
!       last  = ntrace*iline
!       myx(npts+iline) = subs%antslow%table%mjd%val(iline)
!       myaz(npts+iline) = sum(subs%antfast%table%tracking_az%val(first:last))/ntrace
!     enddo
!     !
!     npts = npts+nline
!   enddo
!   !
!   ymin = minval(myaz)
!   ymax = maxval(myaz)
!   yrange = ymax-ymin
!   ymin = ymin-yrange/20.
!   ymax = ymax+yrange/20.
!   !
!   write(chain,'(A,2(1X,F0.5),2(1X,1PG13.6))') 'LIMITS ',myx(1),myx(npts),ymin,ymax
!   call gr_exec1(chain)
!   call gr_exec1('SET BOX 4. 28. 2.5 19.5')
!   call gr_exec1('BOX')
!   call gr_set_marker (4,0,0.3)
!   call gr_segm('TRACKING',error)
!   if (error)  return
!   call gr8_marker(npts,myx,myaz,0.d0,-1.d0)
!   call gr_segm_close(error)
!   if (error)  return
!   !
!   deallocate(myx,myaz)
!   !
! end subroutine mrtcal_imbfits_dump_tracking
