subroutine mrtcal_update_command(line,error)
  use gbl_message
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_update_command
  use mrtcal_index_vars
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command MUPDATE
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='MUPDATE'
  !
  ! Sanity checks
  if (kcurr_ix.eq.0) then
    call mrtcal_message(seve%e,rname,'No current entry in memory')
    error = .true.
    return
  endif
  !
  ! We have to update the kcurr_ix-th entry from the input index (IX)
  ! Its contents is already in kentry, and user may have modified it.
  call mrtindex_update_command(kentry,ix%idir(kcurr_ix),error)
  if (error)  return
  !
end subroutine mrtcal_update_command
