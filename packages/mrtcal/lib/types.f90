!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module mrtcal_level_codes
  integer(kind=4), parameter :: level_cycl_code = 1
  integer(kind=4), parameter :: level_subs_code = 2
  integer(kind=4), parameter :: level_scan_code = 3
end module mrtcal_level_codes
!
module mrtcal_bookkeeping_types
  use gildas_def
  !
  type range_t
     integer(kind=4) :: first    = 0
     integer(kind=4) :: last     = 0
     integer(kind=4) :: n        = 0     ! n = last-first+1
     integer(kind=4) :: i        = 0     ! 
     real(kind=8)    :: vfirst   = 0.d0  ! Actual value associated to first
     real(kind=8)    :: vlast    = 0.d0  ! Actual value associated to last
  end type range_t
  !
  type block_t
    ! Description of processing block
    integer(kind=size_length) :: eltsize = 0 ! [Bytes] 1 element size
    integer(kind=size_length) :: nelt = 0    ! Max number of elements per block
    integer(kind=size_length) :: bufsize = 0 ! [Bytes] Max buffer size
  end type block_t
  !
  type book_t
     type(block_t) :: block !
     type(range_t) :: tot   ! Total range (e.g. range of time rows)
     type(range_t) :: cur   ! Current range (  "  )
  end type book_t
  !
end module mrtcal_bookkeeping_types
!
module mrtcal_buffer_types
  use mrtcal_bookkeeping_types
  use imbfits_types
  use mrtcal_calib_types
  !
  logical, parameter :: isotfmap=.true.
  logical, parameter :: nototfmap=.false.
  !
  logical, parameter :: isfsw=.true.
  logical, parameter :: notfsw=.false.
  !
  type data_buffer_t
     type(book_t)         :: time  ! Bookkeeping structure
     type(imbfits_data_t) :: imbf
     type(chunkset_3d_t)  :: mrtc
  end type data_buffer_t
  !
  type subscan_buffer_t
     type(imbfits_subscan_t) :: subscan 
     type(data_buffer_t)     :: databuf ! Current data
  end type subscan_buffer_t
  !
  type imbfits_buffer_t
     type(imbfits_t)        :: imbf       ! Current file
     type(subscan_buffer_t) :: subscanbuf ! Current subscan
  end type imbfits_buffer_t
  !
end module mrtcal_buffer_types
!
module mrtcal_setup_types
  use phys_const
  use ram_sizes
  use chopper_definitions
  !
  integer(kind=4), parameter :: mdatamodes=3
  integer(kind=4), parameter :: datamode_none=1
  integer(kind=4), parameter :: datamode_ontrack=2
  integer(kind=4), parameter :: datamode_all=3
  character(len=8) :: datamodes(mdatamodes) =  &
    (/ 'NONE    ','ONTRACK ','ALL     ' /)
  !
  integer(kind=4), parameter :: minterpmodes=3
  integer(kind=4), parameter :: interp_nearest=1
  integer(kind=4), parameter :: interp_linear=2
  integer(kind=4), parameter :: interp_spline=3
  character(len=11), parameter :: interpmodes(minterpmodes) =  &
       (/ 'NEAREST    ','LINEAR     ','SPLINE     ' /)
  !
  integer(kind=4), parameter :: maccmodes=4
  integer(kind=4), parameter :: accmode_unkn=0
  integer(kind=4), parameter :: accmode_defa=1  ! Default (best depending on context)
  integer(kind=4), parameter :: accmode_cycl=2  ! Phase cycle or subscan cycle (depending on context)
  integer(kind=4), parameter :: accmode_subs=3
  integer(kind=4), parameter :: accmode_scan=4
  character(len=8), parameter :: accmodes(maccmodes) = &
       (/ '*       ','CYCLE   ','SUBSCAN ','SCAN    ' /)
  !
  integer(kind=4), parameter :: mfeedbacks=3
  integer(kind=4), parameter :: feedback_pix=1
  integer(kind=4), parameter :: feedback_set=2
  integer(kind=4), parameter :: feedback_elt=3
  character(len=12), parameter :: feedbacks(mfeedbacks) = &
       (/ 'PIXEL       ','SET         ','ELEMENT     ' /)
  !
  integer(kind=4), parameter :: moutputcalib=3
  integer(kind=4), parameter :: outputcalib_none=1
  integer(kind=4), parameter :: outputcalib_spec=2
  integer(kind=4), parameter :: outputcalib_asso=3
  character(len=12), parameter :: outputcalibs(moutputcalib) = &
    (/ 'NONE        ','SPECTRA     ','ASSOCIATED  ' /)
  !
  type mrtcal_setup_input_t
    logical         :: bad = .true.             ! [----] Read or not the bad dumps from the backendXXX tables?
    real(kind=4)    :: bandwidth = 0.0          ! [ MHz] Desired calibration bandwidth
    integer(kind=4) :: data = datamode_ontrack  ! [code] Read on-track part of the DATA column?
    logical         :: mjdinter = .false.       ! [----] Intersect or not the DATE-OBS and DATE-END with the MJD colums
    logical         :: tochunk = .true.         ! [----] Map the DATA to chunks at read time
  end type mrtcal_setup_input_t
  type mrtcal_setup_calib_t
    logical         :: bad = .false.                ! [----] Read or not the bad dumps from the backendXXX tables?
    real(kind=4)    :: bandwidth = 20.0             ! [ MHz] Desired calibration bandwidth
    logical         :: chopperstrict = .true.       ! [----] Shall telcal_chopper work in strict or tolerant mode?
    integer(kind=4) :: feedback = feedback_set      ! [code] Feedback level for calibration results
    real(kind=4)    :: winterval = 20.0             ! [ min] Time interval for searching nearest calibration scan (warning)
    real(kind=4)    :: einterval = 30.0             ! [ min] Time interval for searching nearest calibration scan (error)
    integer(kind=4) :: interpscan = interp_nearest  ! [code] Shall MRTCAL interpolate the surrounding calibration scans?
    integer(kind=4) :: interpoff = interp_linear    ! [code] Shall MRTCAL interpolate the calibration Tsys with time between two calibrations? *** JP This default should depend on the observing mode
    real(kind=8)    :: antslowmjdshift = 0.d0       ! [sec ] Shift the antenna slow traces?
    logical         :: mjdinter = .false.           ! [----] Intersect or not the DATE-OBS and DATE-END with the MJD colums
    integer(kind=4) :: interpprod = interp_linear   ! [code] Trec, Tcal, Tsys, Ztau frequency interpolation mode
    real(kind=4)    :: match=4e-2*rad_per_sec       ! [ rad] Matching tolerance for subscan position
    integer(kind=4) :: watermode=chopper_water_elem ! [code] Chopper mode for searching water
    real(kind=4)    :: watervalue=0.                ! [  mm] PWV if water mode is fixed
  end type mrtcal_setup_calib_t
  type mrtcal_setup_output_t
    integer(kind=4) :: accmode = accmode_defa      ! [code] On which periodicity MRTCAL shall accumulate data?
    integer(kind=4) :: calib   = outputcalib_asso  ! [code] How calibration products are written
    logical         :: caltable   = .false.        ! [----] Create a calibration ASCII table in current dir
    logical         :: bychunk    = .false.        ! [----] Shall MRTCAL stitched chunks before writing them as CLASS observations?
    logical         :: fold       = .true.         ! [----] Shall MRTCAL fold the FSW spectra?
    logical         :: toclass    = .true.         ! [----] Shall MRTCAL write CLASS observations?
    logical         :: vdirection = .false.        ! [----] Apply MODIFY VDIRECTION before writing to Class?
    logical         :: voxml      = .false.        ! [----] Write VO XML calibration results?
    character(len=256) :: vodir   = '.'            ! [----] VO XML output directory
    logical         :: weight     = .false.        ! [----] Write the W Associated Array together with RY?
    logical         :: user       = .false.        ! [----] Shall MRTCAL write the User section in CLASS observations?
  end type mrtcal_setup_output_t
  type :: mrtcal_setup_t
    type(mrtcal_setup_input_t)  :: inp           ! READ tunings
    type(mrtcal_setup_calib_t)  :: cal           ! CALIBRATE or PIPELINE tunings
    type(mrtcal_setup_output_t) :: out           ! OUTPUT tunings
    integer(kind=8) :: bufsize   = 512*MB        ! [Byte]
    logical         :: pipeline  = .true.        ! [----] Shall the CALIBRATE command stop or not if an error occur?
  end type mrtcal_setup_t
end module mrtcal_setup_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
