!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_read_command(line,error)
  use gbl_message
  use gildas_def
  use gkernel_interfaces
  use mrtcal_buffers
  use mrtcal_index_vars
  use mrtcal_interfaces, except_this => mrtcal_read_command
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !   READ [Filename|FileNum|ZERO|FIRST|LAST|NEXT|PREV] [/FILENAME] [/SUBSCAN Isub]
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='READ>COMMAND'
  logical :: found
  integer(kind=4) :: isub
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  ! Parse 1st argument
  call mrtcal_read_argument(line,found,error)
  if (error)  return
  if (.not.found)  return  ! Not an error
  !
  ! /SUBSCAN
  isub = 1
  call sic_i4(line,1,1,isub,.false.,error)
  if (error)  return
  !
  ! Do the real job
  call mrtcal_read_main(filecurr,isub,rfile,error)
  if (error) return
  !
end subroutine mrtcal_read_command
!
subroutine mrtcal_read_argument(line,found,error)
  use gildas_def
  use gbl_message
  use classic_api
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_read_argument
  use mrtcal_index_vars
  use mrtindex_types
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command READ. Decode command 1st argument:
  !   READ [Filename|FileNum|ZERO|FIRST|LAST|NEXT|PREV] [/FILENAME]
  ! Of course, PREV or NEXT are unrelated to Filename or Filenum.
  ! This may be confusing for newcomers but there is no obvious solution
  ! as Filename or FileNum do not need to be related to the current index.
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line
  logical,          intent(out)   :: found ! Found a name to be opened?
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='READ>ARGUMENT'
  character(len=filename_length) :: name
  integer(kind=4) :: nc,ikey
  integer, parameter :: nkey=5
  character(len=message_length) :: mess
  character(len=12) :: argum,key,keys(nkey)
  ! Data
  data keys / 'ZERO','FIRST','LAST','NEXT','PREVIOUS' /
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  found = .false.
  !
  if (sic_present(2,0)) then
     ! User sends a file name
     call sic_ch(line,0,1,name,nc,.true.,error)
     if (error)  return
     call sic_parsef(name,filecurr,' ','.fits')
     found = .true.
     ! Given by file name, we do not know if it refers to an indexed
     ! entry. Zero the pointers (see MUPDATE) and the structure...
     kcurr_cx = 0
     kcurr_ix = 0
     call mrtindex_entry_zheader(kentry%head,error)
     if (error)  return
  else
     ! Is there an argument?
     if (.not.sic_present(0,1)) then
        ! No argument => Just reuse current filename
        ! *** JP Should there be some test about the filename?
        found = .true.
     else
        ! If non-empty index file is opened, try a Number or a Keyword
        if (cx%next.gt.1) then
           ! Get argument as a keyword
           call sic_ke(line,0,1,argum,nc,.true.,error)
           if (error)  return
           !
           call sic_ambigs_sub(rname,argum,key,ikey,keys,nkey,error)
           if (error)  return
           !
           select case (ikey)
           case(1)  ! ZERO
              kcurr_cx = 0
              kcurr_ix = 0
              return  ! Nothing more to do
           case(2)  ! FIRST
              kcurr_cx = 1                  ! Entry position in Current indeX
              kcurr_ix = cx%mnum(kcurr_cx)  ! Entry position in Input indeX
              found = .true.
           case(3)  ! LAST
              kcurr_cx = cx%next-1
              kcurr_ix = cx%mnum(kcurr_cx)
              found = .true.
           case(4)  ! NEXT
              if (kcurr_cx.ge.cx%next-1) then
                 call mrtcal_message(seve%e,rname,'End of current index encountered')
                 error = .true.
                 return
              endif
              kcurr_cx = kcurr_cx+1
              kcurr_ix = cx%mnum(kcurr_cx)
              found = .true.
           case(5)  ! PREVIOUS
              if (kcurr_cx.le.1) then
                 call mrtcal_message(seve%e,rname,'Beginning of index encountered')
                 error = .true.
                 return
              endif
              kcurr_cx = kcurr_cx-1
              kcurr_ix = cx%mnum(kcurr_cx)
              found = .true.
           case(0)  ! Not a known keyword, try to get N.V
              call mrtcal_parse_numver(rname,line,0,1,ix,kcurr_ix,error)
              if (error)  return
              found = .true.
              kcurr_cx = 0  ! Can not guess easily, or entry not in cx
           end select
        else
           call mrtcal_message(seve%e,rname,'Empty current index')
           error = .true.
           return
        endif ! Empty current index?
        !
        if (found) then
           ! NB: filecurr and kentry are 2 global variables i.e. describe
           ! the current entry which was READ.
           call mrtindex_list_one_default(ix,kcurr_ix,mess,error)
           if (error) return
           call mrtcal_message(seve%r,rname,'Reading '//mess)
           !
           call mrtindex_optimize_to_entry(ix,kcurr_ix,kentry,error)
           if (error)  return
           !
           call mrtindex_optimize_to_filename(ix,kcurr_ix,filecurr,error)
           if (error)  return
        endif
     endif ! First argument present?
  endif ! /FILENAME present?
  !
end subroutine mrtcal_read_argument
!
subroutine mrtcal_parse_numver(rname,line,iopt,iarg,ix,ient,error)
  use gbl_message
  use classic_api
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this=>mrtcal_parse_numver
  use mrtindex_types
  !---------------------------------------------------------------------
  ! @ private
  !  Parse the command line to retrieve an argument of the form "N" or
  ! "N.V" and convert it into the entry position in IX
  !---------------------------------------------------------------------
  character(len=*),           intent(in)    :: rname  ! Calling routine name
  character(len=*),           intent(in)    :: line   ! Command line
  integer(kind=4),            intent(in)    :: iopt   ! Option number
  integer(kind=4),            intent(in)    :: iarg   ! Argument number
  type(mrtindex_optimize_t),  intent(in)    :: ix     !
  integer(kind=entry_length), intent(out)   :: ient   ! Resolved entry number
  logical,                    intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=24) :: string
  integer(kind=4) :: nc,posd,ver
  integer(kind=entry_length) :: num
  logical :: dupl
  !
  call sic_ch(line,iopt,iarg,string,nc,.true.,error)
  if (error)  return
  !
  posd = index(string,'.')  ! Dot position
  !
  ! Decode version
  if (posd.eq.0) then
    ver = 0
    posd = nc+1
  else
    call sic_math_inte(string(posd+1:nc),nc-posd,ver,error)
    if (error)  return
  endif
  !
  ! Decode number
  call sic_math_long(string(1:posd-1),posd-1,num,error)
  if (error)  return
  !
  call mrtindex_numver2ent(rname,ix,num,ver,ient,dupl,error)
  if (error)  return
  !
end subroutine mrtcal_parse_numver
!
subroutine mrtcal_read_main(filename,isub,filebuf,error)
  use gildas_def
  use gbl_message
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this => mrtcal_read_main
  use mrtcal_buffers
  use mrtcal_buffer_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*),       intent(in)    :: filename
  integer(kind=4),        intent(in)    :: isub
  type(imbfits_buffer_t), intent(inout) :: filebuf
  logical,                intent(inout) :: error
  !
  call imbfits_read_header(filename,filebuf%imbf,rsetup%inp%bandwidth,error)
  if (error) return
  !
  ! Read subscan #isub: headers for backendXXX, antenna, and subreflector
  call imbfits_read_subscan_header_bynum(filebuf%imbf,isub,.not.rsetup%inp%bad,  &
    rsetup%inp%mjdinter,0.d0,filebuf%subscanbuf%subscan,error)
  if (error)  return
  !
  ! Read subscan #isub: data
  if (rsetup%inp%data.eq.datamode_none) then
    ! Just free the buffer (do not keep previous=inconsistant data)
    call mrtcal_free_subscan_data(filebuf%subscanbuf%databuf,error)
    return
  endif
  !
  ! time%tot: the desired range of the DATA column
  if (rsetup%inp%data.eq.datamode_ontrack) then
    ! Find the on-track range
    call mrtcal_get_time_range_for_backend(filebuf%subscanbuf%subscan,  &
      filebuf%subscanbuf%databuf%time%tot,error)
    if (error)  return
  elseif (rsetup%inp%data.eq.datamode_all) then
    ! Select all (on-track + off-track) the DATA column
    call mrtcal_get_time_range_for_all(filebuf%subscanbuf%subscan,  &
      filebuf%subscanbuf%databuf%time%tot,error)
    if (error)  return
  endif
  !
  ! time%cur: the current sub-selection. We do not iterate by piece,
  ! sub-selection is eveything:
  call mrtcal_bookkeeping_all(filebuf%subscanbuf%subscan,  &
    filebuf%subscanbuf%databuf%time,error)
  !
  ! Read it
  call mrtcal_read_subscan_data(    &
       filebuf%imbf,                &
       filebuf%subscanbuf%subscan,  &
       rsetup%inp%tochunk,          &
       filebuf%subscanbuf%databuf,  &
       error)
  if (error)  return
  !
end subroutine mrtcal_read_main
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
