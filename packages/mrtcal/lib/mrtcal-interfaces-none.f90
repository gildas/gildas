module mrtcal_interfaces_none
  interface
    subroutine mrtcal_message_set_id(id)
      use gbl_message
      !---------------------------------------------------------------------
      ! Alter library id into input id. Should be called by the library
      ! which wants to share its id with the current one.
      !---------------------------------------------------------------------
      integer, intent(in) :: id
    end subroutine mrtcal_message_set_id
  end interface
  !
  interface
    subroutine mrtcal_message(mkind,procname,message)
      use gbl_message
      !---------------------------------------------------------------------
      ! Messaging facility for the current library. Calls the low-level
      ! (internal) messaging routine with its own identifier.
      !---------------------------------------------------------------------
      integer,          intent(in) :: mkind     ! Message kind
      character(len=*), intent(in) :: procname  ! Name of calling procedure
      character(len=*), intent(in) :: message   ! Message string
    end subroutine mrtcal_message
  end interface
  !
end module mrtcal_interfaces_none
