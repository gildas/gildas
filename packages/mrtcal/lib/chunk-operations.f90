!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_chunk_swi_fill(swmode,swdesc,chunk,error)
  use gbl_message
  use gbl_constant
  use mrtcal_calib_types
  use mrtcal_dependencies_interfaces
  use mrtcal_interfaces, except_this => mrtcal_chunk_swi_fill
  !---------------------------------------------------------------------
  ! @ private
  ! At this point, the phase of the cycle are not yet combined. Hence,
  ! only the part corresponding to each phase is filled in the CLASS 
  ! switching section. Combination will be done during ON-OFF so that
  ! the user can write separately the different phases with consistent
  ! headers...
  !
  ! Moreover the different decal arrays have been set to zero at data read
  ! time. So only the first value is set here according to the switch
  ! mode.
  !---------------------------------------------------------------------
  integer(kind=4),     intent(in)    :: swmode
  type(switch_desc_t), intent(in)    :: swdesc
  type(chunk_t),       intent(inout) :: chunk
  logical,             intent(inout) :: error
  ! Local
  integer(kind=4), parameter :: ilam = 1
  integer(kind=4), parameter :: ibet = 2
  integer(kind=4), parameter :: ifre = 1
  character(len=*), parameter :: rname='CHUNK>SWI>FILL'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  if (chunk%ifront.le.0) then
     call mrtcal_message(seve%e,rname,'Negative or zero valued chunk%ifront')
     error = .true.
     return
  endif
  ! chunk%swi%nphas    done in mrtcal_chunk_swi_from_data
  ! chunk%swi%duree(1) done in mrtcal_chunk_swi_from_data
  chunk%swi%poids(1) = swdesc%wei(chunk%ifront)
  select case (swmode)
  case(switchmode_pos)
     chunk%swi%swmod = mod_pos
     chunk%swi%ldecal(1) = swdesc%off(ilam,chunk%ifront)
     chunk%swi%bdecal(1) = swdesc%off(ibet,chunk%ifront)
  case(switchmode_wob)
     chunk%swi%swmod = mod_wob
     chunk%swi%ldecal(1) = swdesc%off(ilam,chunk%ifront)
     chunk%swi%bdecal(1) = swdesc%off(ibet,chunk%ifront)
  case(switchmode_bea)
     chunk%swi%swmod = mod_bea
     chunk%swi%ldecal(1) = swdesc%off(ilam,chunk%ifront)
     chunk%swi%bdecal(1) = swdesc%off(ibet,chunk%ifront)
  case(switchmode_fre)
     chunk%swi%swmod = mod_freq
     chunk%swi%decal(1) = swdesc%off(ifre,chunk%ifront)
  case default
     call mrtcal_message(seve%e,rname,  &
       'Not yet implemented for switch mode '//mrtindex_swmode(swmode))
     error = .true.
     return
  end select
  !
end subroutine mrtcal_chunk_swi_fill
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_chunk_init_data(chunk,v,w,c,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this => mrtcal_chunk_init_data
  !---------------------------------------------------------------------
  ! @ private
  ! Initialize the chunk data from the input values
  !---------------------------------------------------------------------
  type(chunk_t), intent(inout) :: chunk
  real(kind=4),  intent(in)    :: v      ! Data value (duplicated to all channels)
  real(kind=4),  intent(in)    :: w      ! Weight (duplicated to all channels)
  real(kind=4),  intent(in)    :: c      ! Continuum value
  logical,       intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CHUNK>INIT>DATA'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  ! Data
  chunk%cont1 = c
  chunk%data1(:) = v
  chunk%dataw(:) = w
  !
end subroutine mrtcal_chunk_init_data
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_chunk_copy_header(in,out,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this => mrtcal_chunk_copy_header
  !---------------------------------------------------------------------
  ! @ private
  ! Section are copied
  !---------------------------------------------------------------------
  type(chunk_t), intent(in)    :: in
  type(chunk_t), intent(inout) :: out
  logical,       intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CHUNK>COPY>HEADER'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  ! Misc
  out%id       = in%id
  out%mjd      = in%mjd
  out%ifront   = in%ifront
  out%frontend = in%frontend
  ! Sections
  out%gen  = in%gen   ! general
  out%pos  = in%pos   ! position
  out%spe  = in%spe   ! spectroscopy
  out%con  = in%con   ! continuum drift
  out%swi  = in%swi   ! switching
  out%res  = in%res   ! resolution
  out%cal  = in%cal   ! calibration
  out%user = in%user  ! user section
  !
end subroutine mrtcal_chunk_copy_header
!
subroutine mrtcal_chunk_copy_data(in,out,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this => mrtcal_chunk_copy_data
  !---------------------------------------------------------------------
  ! @ private
  ! Section are copied
  !---------------------------------------------------------------------
  type(chunk_t), intent(in)    :: in
  type(chunk_t), intent(inout) :: out
  logical,       intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CHUNK>COPY>DATA'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  out%cont1 = in%cont1
  out%data1(:) = in%data1(:)
  out%dataw(:) = in%dataw(:)
  !
end subroutine mrtcal_chunk_copy_data
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_chunk_self_multiply(gain,inout,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_chunk_self_multiply
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(chunk_t), intent(in)    :: gain
  type(chunk_t), intent(inout) :: inout
  logical,       intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CHUNK>SELF>MULTIPLY'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_chunk_self_multiply_head(gain,inout,error)
  if (error) return
  call mrtcal_chunk_self_multiply_data(bad,&
       inout%ndata,gain%data1,inout%data1,error)
  if (error) return
  !
end subroutine mrtcal_chunk_self_multiply
!
subroutine mrtcal_chunk_self_multiply_head(gain,inout,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_chunk_self_multiply_head
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(chunk_t), intent(in)    :: gain
  type(chunk_t), intent(inout) :: inout
  logical,       intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CHUNK>SELF>MULTIPLY>HEAD'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  inout%cal = gain%cal
  !
  inout%gen%tsys = gain%gen%tsys
  inout%gen%tau = gain%gen%tau
  !
  ! inout%user%obstype = unchanged
  inout%user%airmass = gain%user%airmass
  inout%user%expatau = gain%user%expatau
  inout%user%backeff = gain%user%backeff
  inout%user%noise   = inout%user%noise*gain%gen%tsys
  !
end subroutine mrtcal_chunk_self_multiply_head
!
subroutine mrtcal_chunk_self_multiply_data(bad,nchan,gain,inout,error)
  use gbl_message
  use gildas_def
  use mrtcal_interfaces, except_this=>mrtcal_chunk_self_multiply_data
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  real(kind=4),    intent(in)    :: bad
  integer(kind=4), intent(in)    :: nchan
  real(kind=4),    intent(in)    :: gain(nchan)
  real(kind=4),    intent(inout) :: inout(nchan)
  logical,         intent(inout) :: error
  ! Local
  integer(kind=4) :: ichan
  character(len=*), parameter :: rname='CHUNK>SELF>MULTIPLY>DATA'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  do ichan=1,nchan
     if ((gain(ichan).ne.bad).and.(inout(ichan).ne.bad)) then
        inout(ichan) = inout(ichan)*gain(ichan)
     else
        inout(ichan) = bad
     endif
  enddo ! ichan
  !
end subroutine mrtcal_chunk_self_multiply_data
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_chunk_multiply_head(atsys,expatau,tscale,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_chunk_multiply_head
  !---------------------------------------------------------------------
  ! @ private
  ! This subroutine is NOT a generic multiplication of 2 headers.
  !---------------------------------------------------------------------
  type(chunk_t), intent(in)    :: atsys
  type(chunk_t), intent(in)    :: expatau
  type(chunk_t), intent(inout) :: tscale
  logical,       intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CHUNK>MULTIPLY>HEAD'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  call mrtcal_chunk_copy_header(atsys,tscale,error)
  if (error) return
  if (expatau%user%expatau.ne.bad) then
     tscale%gen%tsys = atsys%gen%tsys*expatau%user%expatau
  endif
  !
end subroutine mrtcal_chunk_multiply_head
!
subroutine mrtcal_chunk_multiply_data(in1,in2,out,error)
  use gbl_message
  use mrtcal_interfaces, except_this=>mrtcal_chunk_multiply_data
  use mrtcal_calib_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(chunk_t), intent(in)    :: in1
  type(chunk_t), intent(in)    :: in2
  type(chunk_t), intent(inout) :: out
  logical,       intent(inout) :: error
  ! Local
  integer(kind=4) :: ichan
  character(len=*), parameter :: rname='CHUNK>MULTIPLY>DATA'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  do ichan=1,out%ndata
    if (in1%data1(ichan).ne.bad .and. in2%data1(ichan).ne.bad) then
      out%data1(ichan) = in1%data1(ichan)*in2%data1(ichan)
    else
      out%data1(ichan) = bad
    endif
  enddo ! ichan
  !
end subroutine mrtcal_chunk_multiply_data
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_chunk_noise_init(chunk,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_chunk_noise_init
  !---------------------------------------------------------------------
  ! @ private
  ! Initialize the computation of the theoretical noise of the chunk with
  ! 1.0 / [backeff * sqrt(dnu * tint)]
  !---------------------------------------------------------------------
  type(chunk_t), intent(inout) :: chunk
  logical,       intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CHUNK>NOISE>INIT'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  if (chunk%spe%fres.ne.0.d0 .and.  &
      chunk%gen%time.gt.0.   .and.  &
      chunk%user%backeff.gt.0.) then
     chunk%user%noise = 1d0 / (chunk%user%backeff * sqrt(1d6*abs(chunk%spe%fres)*chunk%gen%time))
  else
     call mrtcal_message(seve%e,rname,'Backend efficiency and/or integration time and/or frequency resolution are zero valued')
     error = .true.
     return
  endif
  !
end subroutine mrtcal_chunk_noise_init
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_chunk_interpolate_init(prev,next,slope,offset,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this => mrtcal_chunk_interpolate_init
  !---------------------------------------------------------------------
  ! @ private
  ! Initialize linear interpolation using the following formula
  !     xoffset = xprev
  !     y(x) = (x-xoffset)*slope + offset
  ! We shift the x axis by xoffset to avoid rounding errors as the
  ! computation are done in single precision while the x axis is MJD...
  !---------------------------------------------------------------------
  type(chunk_t), target, intent(in)    :: prev
  type(chunk_t), target, intent(in)    :: next
  type(chunk_t),         intent(inout) :: slope
  type(chunk_t),         intent(inout) :: offset
  logical,               intent(inout) :: error
  ! Local
  integer(kind=4) :: ichan
  real(kind=8) :: xprev,xnext,xdiff
  real(kind=4), pointer :: yprev(:),ynext(:)
  character(len=*), parameter :: rname='CHUNK>INTERPOLATE>INIT'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  xprev = prev%mjd
  xnext = next%mjd
  yprev => prev%data1
  ynext => next%data1
  !
  offset%mjd = prev%mjd
  xdiff = xnext-xprev
  if (xdiff.ne.0d0) then
     do ichan=1,ubound(yprev,1)
        if ((yprev(ichan).ne.bad).and.(ynext(ichan).ne.bad)) then
           slope%data1(ichan) = (ynext(ichan)-yprev(ichan)) / xdiff
           offset%data1(ichan) = yprev(ichan)
        else
           slope%data1(ichan) = bad
           offset%data1(ichan) = bad
        endif
     enddo ! ichan
  else
     slope%data1(:) = bad
     offset%data1(:) = bad
  endif
!!$print *," "
!!$write(*,'(2(F0.20,1X))') yprev(1),ynext(1)
!!$print *,xprev,dble(yprev(1)),xnext,dble(ynext(1)),slope%data1(1),offset%data1(1)
!!$print *,(ynext(1)-yprev(1)),xdiff
  !
end subroutine mrtcal_chunk_interpolate_init
!
subroutine mrtcal_chunk_interpolate_do(mjd,slope,offset,interp,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this => mrtcal_chunk_interpolate_do
  !---------------------------------------------------------------------
  ! @ private
  ! Compute linear interpolation from previously computed slope and offset
  ! using the following formula
  !     xoffset = xprev
  !     y(x) = (x-xoffset)*slope + offset
  ! We shift the x axis by xoffset to avoid rounding errors as the
  ! computation are done in single precision while the x axis is MJD...
  !---------------------------------------------------------------------
  real(kind=8),    intent(in)    :: mjd
  type(chunk_t),   intent(in)    :: slope
  type(chunk_t),   intent(in)    :: offset
  type(chunk_t),   intent(inout) :: interp
  logical,         intent(inout) :: error
  ! Local
  integer(kind=4) :: ichan,nchan
  character(len=*), parameter :: rname='CHUNK>INTERPOLATE>INIT'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !  
  nchan = ubound(interp%data1,1)
  do ichan=1,nchan
     if ((slope%data1(ichan).ne.bad).and.(offset%data1(ichan).ne.bad)) then
        interp%data1(ichan) = slope%data1(ichan)*(mjd-offset%mjd) + offset%data1(ichan)
     else
        interp%data1(ichan) = bad
     endif
  enddo ! ichan
  interp%mjd = mjd
  !
end subroutine mrtcal_chunk_interpolate_do
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtcal_chunk_calgrid(hh,vv,real,imag,amp,pha,cos,sin,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_chunk_calgrid
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(chunk_t), intent(inout) :: hh,vv
  type(chunk_t), intent(inout) :: real,imag
  type(chunk_t), intent(inout) :: amp,pha
  type(chunk_t), intent(inout) :: cos,sin
  logical,       intent(inout) :: error
  ! Local
  integer(kind=4) :: ichan,nchanr,nchani
  character(len=*), parameter :: rname='CHUNK>CALGRID'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  nchanr = ubound(real%data1,1)
  nchani = ubound(imag%data1,1)
  if (nchanr.ne.nchani) then
     call mrtcal_message(seve%e,rname,'Different number of channels between R and I')
     error = .true.
     return
  endif
  do ichan=1,nchanr
     amp%data1(ichan) = sqrt(real%data1(ichan)**2+imag%data1(ichan)**2)
     if (amp%data1(ichan).ne.0) then
        cos%data1(ichan) = real%data1(ichan)/amp%data1(ichan)
        ! *** SB: we flip the whole IMAG sign because this gives results
        !         consistant with MIRA (i.e. mostly flat).
        ! *** JP to GP: but why do we have this sign HERE?
        ! *** JP: is it linked to the DATAFLIP applied on some of the IMAG data?
        sin%data1(ichan) = -imag%data1(ichan)/amp%data1(ichan)
        pha%data1(ichan) = atan2(sin%data1(ichan),cos%data1(ichan))
     else
        cos%data1(ichan) = bad
        sin%data1(ichan) = bad
        pha%data1(ichan) = bad
     endif
  enddo ! ichan
  hh%pos%sourc = 'HH' ! *** JP unclear whether this should be done here
  vv%pos%sourc = 'VV' ! *** JP unclear whether this should be done here
  real%pos%sourc = 'REAL' ! *** JP unclear whether this should be done here
  imag%pos%sourc = 'IMAG' ! *** JP unclear whether this should be done here
  amp%pos%sourc = 'AMP'
  pha%pos%sourc = 'PHA'
  sin%pos%sourc = 'SIN'
  cos%pos%sourc = 'COS'
end subroutine mrtcal_chunk_calgrid
!
subroutine mrtcal_chunk_cross(h,v,hv,vh,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_chunk_cross
  !---------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  !---------------------------------------------------------------------
  type(chunk_t), intent(in)    :: h
  type(chunk_t), intent(in)    :: v
  type(chunk_t), intent(inout) :: hv
  type(chunk_t), intent(inout) :: vh
  logical,       intent(inout) :: error
  ! Local
  integer(kind=4) :: ichan,nchanh,nchanv
  character(len=*), parameter :: rname='CHUNK>CROSS'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  nchanh = ubound(h%data1,1)
  nchanv = ubound(v%data1,1)
  if (nchanh.ne.nchanv) then
     call mrtcal_message(seve%e,rname,'Different number of channels between H and V')
     error = .true.
     return
  endif
  do ichan=1,nchanh
     hv%data1(ichan) = sqrt(h%data1(ichan)*v%data1(ichan))
     vh%data1(ichan) = hv%data1(ichan)
  enddo ! ichan
  hv%gen%teles = 'HV' ! *** JP Is it correct?
  vh%gen%teles = 'VH' ! *** JP Is it correct?
  !
end subroutine mrtcal_chunk_cross
!
subroutine mrtcal_chunk_mean(h,v,hv,vh,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_chunk_mean
  !---------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  !---------------------------------------------------------------------
  type(chunk_t), intent(in)    :: h
  type(chunk_t), intent(in)    :: v
  type(chunk_t), intent(inout) :: hv
  type(chunk_t), intent(inout) :: vh
  logical,       intent(inout) :: error
  ! Local
  integer(kind=4) :: ichan,nchanh,nchanv
  character(len=*), parameter :: rname='CHUNK>MEAN'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  nchanh = ubound(h%data1,1)
  nchanv = ubound(v%data1,1)
  if (nchanh.ne.nchanv) then
     call mrtcal_message(seve%e,rname,'Different number of channels between H and V')
     error = .true.
     return
  endif
  do ichan=1,nchanh
     hv%data1(ichan) = 0.5*(h%data1(ichan)+v%data1(ichan))
     vh%data1(ichan) = hv%data1(ichan)
  enddo ! ichan
  hv%gen%teles = 'HV' ! *** JP Is it correct?
  vh%gen%teles = 'VH' ! *** JP Is it correct?
  !
end subroutine mrtcal_chunk_mean
!
subroutine mrtcal_chunk_flag(h,v,hv,vh,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_chunk_flag
  !---------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  !---------------------------------------------------------------------
  type(chunk_t), intent(in)    :: h
  type(chunk_t), intent(in)    :: v
  type(chunk_t), intent(inout) :: hv
  type(chunk_t), intent(inout) :: vh
  logical,       intent(inout) :: error
  ! Local
  integer(kind=4) :: ichan,nchanh,nchanv
  character(len=*), parameter :: rname='CHUNK>FLAG'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  nchanh = ubound(h%data1,1)
  nchanv = ubound(v%data1,1)
  if (nchanh.ne.nchanv) then
     call mrtcal_message(seve%e,rname,'Different number of channels between H and V')
     error = .true.
     return
  endif
  do ichan=1,nchanh
     if ((h%data1(ichan).eq.0).and.(v%data1(ichan).eq.0)) then
        hv%data1(ichan) = 0
        vh%data1(ichan) = 0
     else
        hv%data1(ichan) = 1
        vh%data1(ichan) = 1
     endif
  enddo ! ichan
  hv%gen%teles = 'HV' ! *** JP Is it correct?
  vh%gen%teles = 'VH' ! *** JP Is it correct?
  !
end subroutine mrtcal_chunk_flag
!
subroutine mrtcal_chunk_unrotate(cos,sin,real,imag,error)
  use gbl_message
  use mrtcal_calib_types
  use mrtcal_interfaces, except_this=>mrtcal_chunk_unrotate
  !---------------------------------------------------------------------
  ! @ private
  ! The phase is rotated by a given phase with cos = cos(phase) and sin =
  ! sin(phase). This subroutine unrotate the phase. Hence the sign
  ! convention used here in applying a rotation of -phase.
  ! ---------------------------------------------------------------------
  type(chunk_t), intent(in)    :: cos
  type(chunk_t), intent(in)    :: sin
  type(chunk_t), intent(inout) :: real
  type(chunk_t), intent(inout) :: imag
  logical,       intent(inout) :: error
  ! Local
  integer(kind=4) :: ichan,nchanr,nchani
  real(kind=4) :: rotreal,rotimag
  character(len=*), parameter :: rname='CHUNK>UNROTATE'
  !
  call mrtcal_message(seve%t,rname,'Welcome')
  !
  nchanr = ubound(real%data1,1)
  nchani = ubound(imag%data1,1)
  if (nchanr.ne.nchani) then
     call mrtcal_message(seve%e,rname,'Different number of channels between R and I')
     error = .true.
     return
  endif
  do ichan=1,nchanr
     rotreal = + real%data1(ichan)*cos%data1(ichan) + imag%data1(ichan)*sin%data1(ichan)
     rotimag = - real%data1(ichan)*sin%data1(ichan) + imag%data1(ichan)*cos%data1(ichan)
     real%data1(ichan) = rotreal
     imag%data1(ichan) = rotimag
  enddo ! ichan
  !
end subroutine mrtcal_chunk_unrotate
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
