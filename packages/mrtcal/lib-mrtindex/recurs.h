
#ifndef _SYSC_H_
#define _SYSC_H_


#include "gsys/cfc.h"
#include <dirent.h>
#if defined(darwin)
#define _DIRENT_HAVE_D_TYPE 1
#endif

#define GAG_MAX_PATH_LENGTH 1024

#define gag_directory_exedir CFC_EXPORT_NAME( gag_directory_exedir)


#endif /* _SYSC_H_ */
