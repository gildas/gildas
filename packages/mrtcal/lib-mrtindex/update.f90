subroutine mrtindex_update_command(entry,fileid,error)
  use gbl_message
  use mrtindex_interfaces, except_this=>mrtindex_update_command
  use mrtindex_vars
  !---------------------------------------------------------------------
  ! @ public
  ! Support routine for command MUPDATE
  !---------------------------------------------------------------------
  type(mrtindex_entry_t), intent(inout) :: entry
  integer(kind=4),        intent(in)    :: fileid
  logical,                intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='MUPDATE'
  !
  ! Copy back key to index (reverse operation comparing to read time)
  call mrtindex_key_to_index(entry%head%key,entry%indx,error)
  if (error)  return
  !
  ! Open file in read-write mode (if not already):
  call mrtindex_file_old(fileid,.true.,error)
  if (error)  return
  !
  ! Update current index entry on disk
  call mrtindex_entry_update(ix_files(fileid),entry,error)
  if (error)  return
  !
  call classic_file_fflush(ix_files(fileid),error)
  if (error)  return
  !
end subroutine mrtindex_update_command
