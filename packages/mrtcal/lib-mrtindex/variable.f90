subroutine mrtindex_variable_entry(struct,head,ro,error)
  use mrtindex_dependencies_interfaces
  use mrtindex_interfaces, except_this=>mrtindex_variable_entry
  use mrtindex_types
  !---------------------------------------------------------------------
  ! @ public
  !  Map a Sic structure on an entry
  !---------------------------------------------------------------------
  character(len=*),      intent(in)    :: struct  ! Structure name
  type(mrtindex_header_t), intent(in)    :: head    !
  logical,               intent(in)    :: ro      ! Read-Only?
  logical,               intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  !
  userreq = .false.
  !
  call sic_delvariable(struct,userreq,error)
  call sic_defstructure(struct,.true.,error)
  if (error)  return
  !
  call mrtindex_variable_entry_key(struct,head%key,ro,error)
  if (error)  return
  call mrtindex_variable_entry_prim(struct,head%pri,ro,error)
  if (error)  return
  call mrtindex_variable_entry_calib(struct,head%cal,ro,error)
  if (error)  return
  call mrtindex_variable_entry_science(struct,head%sci,ro,error)
  if (error)  return
  !
end subroutine mrtindex_variable_entry
!
subroutine mrtindex_variable_entry_key(struct,key,ro,error)
  use gildas_def
  use mrtindex_dependencies_interfaces
  use mrtindex_interfaces, except_this=>mrtindex_variable_entry_key
  use mrtindex_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*),   intent(in)    :: struct  ! Structure name
  type(mrtindex_key_t), intent(in)    :: key     !
  logical,            intent(in)    :: ro      ! Read-Only?
  logical,            intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  character(len=32) :: str
  integer(kind=index_length) :: dims(sic_maxdims)
  !
  userreq = .false.
  str = trim(struct)//'%KEY'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call sic_def_inte (trim(str)//'%VERSION',     key%version,     0,0,   ro,error)
  call sic_def_inte (trim(str)//'%TELESCOPE',   key%telescope,   0,0,   ro,error)
  call sic_def_char (trim(str)//'%PROJID',      key%projid,             ro,error)
  call sic_def_char (trim(str)//'%SOURCE',      key%source,             ro,error)
  call sic_def_inte (trim(str)//'%DOBS',        key%dobs,        0,0,   ro,error)
  call sic_def_dble (trim(str)//'%UT',          key%ut,          0,0,   ro,error)
  call sic_def_dble (trim(str)//'%LST',         key%lst,         0,0,   ro,error)
  call sic_def_real (trim(str)//'%AZ',          key%az,          0,0,   ro,error)
  call sic_def_real (trim(str)//'%EL',          key%el,          0,0,   ro,error)
  call sic_def_dble (trim(str)//'%LON',         key%lon,         0,0,   ro,error)
  call sic_def_dble (trim(str)//'%LAT',         key%lat,         0,0,   ro,error)
  call sic_def_inte (trim(str)//'%SYSTEM',      key%system,      0,0,   ro,error)
  call sic_def_real (trim(str)//'%EQUINOX',     key%equinox,     0,0,   ro,error)
  dims(1) = mrtcal_indx_mfrontend
  call sic_def_charn(trim(str)//'%FRONTEND',    key%frontend,    1,dims,ro,error)
  call sic_def_inte (trim(str)//'%SCAN',        key%scan,        0,0,   ro,error)
  call sic_def_inte (trim(str)//'%BACKEND',     key%backend,     0,0,   ro,error)
  call sic_def_inte (trim(str)//'%OBSTYPE',     key%obstype,     0,0,   ro,error)
  call sic_def_inte (trim(str)//'%SWITCHMODE',  key%switchmode,  0,0,   ro,error)
  call sic_def_inte (trim(str)//'%POLARIMETRY', key%polstatus,   0,0,   ro,error)
  call sic_def_inte (trim(str)//'%FILSTATUS',   key%filstatus,   0,0,   ro,error)
  call sic_def_inte (trim(str)//'%CALSTATUS',   key%calstatus,   0,0,   ro,error)
  call sic_def_char (trim(str)//'%FILENAME',    key%filename,           ro,error)
  !
end subroutine mrtindex_variable_entry_key
!
subroutine mrtindex_variable_entry_prim(struct,prim,ro,error)
  use mrtindex_dependencies_interfaces
  use mrtindex_interfaces, except_this=>mrtindex_variable_entry_prim
  use mrtindex_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*),    intent(in)    :: struct  ! Structure name
  type(sec_primary_t), intent(in)    :: prim    !
  logical,             intent(in)    :: ro      ! Read-Only?
  logical,             intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%PRI'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call sic_def_real(trim(str)//'%IMBFVERS',prim%imbfvers,0,0,ro,error)
  !
end subroutine mrtindex_variable_entry_prim
!
subroutine mrtindex_variable_entry_calib(struct,calib,ro,error)
  use gildas_def
  use mrtindex_dependencies_interfaces
  use mrtindex_interfaces, except_this=>mrtindex_variable_entry_calib
  use mrtindex_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: struct  ! Structure name
  type(sec_calib_t), intent(in)    :: calib   !
  logical,           intent(in)    :: ro      ! Read-Only?
  logical,           intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  character(len=32) :: str
  integer(kind=index_length) :: dims(sic_maxdims)
  !
  userreq = .false.
  str = trim(struct)//'%CAL'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call sic_def_inte(trim(str)//'%NFREQ',calib%nfreq,0,0,ro,error)
  if (calib%nfreq.le.0)  return
  !
  dims(1) = calib%nset
  call sic_def_charn(trim(str)//'%FRONTEND',calib%frontend,1,dims,ro,error)
  !
  dims(1) = calib%nfreq
  dims(2) = calib%nset
  dims(3) = calib%npix
  call sic_def_dble(trim(str)//'%FREQ',calib%freq,3,dims,ro,error)
  call sic_def_real(trim(str)//'%TSYS',calib%atsys,3,dims,ro,error)
  call sic_def_real(trim(str)//'%ZTAU',calib%ztau,3,dims,ro,error)
  !
end subroutine mrtindex_variable_entry_calib
!
subroutine mrtindex_variable_entry_science(struct,sci,ro,error)
  use mrtindex_dependencies_interfaces
  use mrtindex_interfaces, except_this=>mrtindex_variable_entry_science
  use mrtindex_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*),    intent(in)    :: struct  ! Structure name
  type(sec_science_t), intent(in)    :: sci     !
  logical,             intent(in)    :: ro      ! Read-Only?
  logical,             intent(inout) :: error   ! Logical error flag
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%SCI'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call sic_def_inte(trim(str)//'%CALDOBS',sci%caldobs,0,0,ro,error)
  call sic_def_inte(trim(str)//'%CALSCAN',sci%calscan,0,0,ro,error)
  call sic_def_inte(trim(str)//'%CALBACK',sci%calback,0,0,ro,error)
  call sic_def_inte(trim(str)//'%CALVERS',sci%calvers,0,0,ro,error)
  !
end subroutine mrtindex_variable_entry_science
