module mrtindex_interfaces_private
  interface
    subroutine reallocate_calib_section(nfreq,nset,npix,calib,error)
      use gbl_message
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4),   intent(in)    :: nfreq  !
      integer(kind=4),   intent(in)    :: nset   !
      integer(kind=4),   intent(in)    :: npix   !
      type(sec_calib_t), intent(inout) :: calib  !
      logical,           intent(inout) :: error  !
    end subroutine reallocate_calib_section
  end interface
  !
  interface
    subroutine free_calib_section(calib,error)
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(sec_calib_t), intent(inout) :: calib  !
      logical,           intent(inout) :: error  !
    end subroutine free_calib_section
  end interface
  !
  interface
    subroutine reallocate_ix_dirs(ndir,error)
      use gbl_message
      use classic_api
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      !  Reallocate the support arrays for indexing several directories
      ! in memory. If reallocation is needed, double the size of current
      ! allocation.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)    :: ndir
      logical,         intent(inout) :: error
    end subroutine reallocate_ix_dirs
  end interface
  !
  interface
    subroutine free_ix_dirs(error)
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      !   Deallocate the support arrays for indexing several directories in
      ! memory.
      !---------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine free_ix_dirs
  end interface
  !
  interface reallocate_mrtoptimize
    subroutine reallocate_mrtoptimize_expo(optx,mobs,error)
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private-generic reallocate_mrtoptimize
      !  Reallocate the 'optimize' type arrays. If current allocation is not
      ! enough, double the size of allocation. Since this reallocation
      ! routine is used in a context of adding more and more data, data
      ! is always preserved after reallocation.
      !---------------------------------------------------------------------
      type(mrtindex_optimize_t),    intent(inout) :: optx   !
      integer(kind=entry_length), intent(in)    :: mobs   ! Requested size
      logical,                    intent(inout) :: error  ! Logical error flag
    end subroutine reallocate_mrtoptimize_expo
    subroutine reallocate_mrtoptimize_more(optx,mobs,keep,error)
      use gbl_message
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private-generic reallocate_mrtoptimize
      !  Allocate the 'optimize' type arrays. Enlarge the arrays to the
      ! requested size, if needed. No shrink possible. Keep data if
      ! requested.
      !---------------------------------------------------------------------
      type(mrtindex_optimize_t),    intent(inout) :: optx   !
      integer(kind=entry_length), intent(in)    :: mobs   ! Requested size
      logical,                    intent(in)    :: keep   ! Keep previous data?
      logical,                    intent(inout) :: error  ! Logical error flag
    end subroutine reallocate_mrtoptimize_more
  end interface reallocate_mrtoptimize
  !
  interface
    subroutine mrtindex_copy_index(filename,optx,error)
      use gbl_message
      use classic_api
      use mrtindex_types
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      !  Copy the memory index into the disk index
      !---------------------------------------------------------------------
      character(len=*),        intent(in)    :: filename
      type(mrtindex_optimize_t), intent(in)    :: optx
      logical,                 intent(inout) :: error
    end subroutine mrtindex_copy_index
  end interface
  !
  interface
    subroutine mrtindex_file_update(indexfile,directory,pattern,overwrite,  &
      present,error)
      use gildas_def
      use gbl_message
      use classic_api
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      ! Build/Update (according to overwrite flag) the index file
      !---------------------------------------------------------------------
      character(len=*),     intent(in)    :: indexfile  !
      character(len=*),     intent(in)    :: directory  !
      type(user_pattern_t), intent(in)    :: pattern    ! Pattern(s) for file(s) to be indexed
      logical,              intent(in)    :: overwrite  !
      logical,              intent(in)    :: present    ! Is it expected to find FITS files?
      logical,              intent(inout) :: error      !
    end subroutine mrtindex_file_update
  end interface
  !
  interface
    subroutine mrtindex_file_new(indexfile,overwrite,directory,fileid,error)
      !---------------------------------------------------------------------
      ! @ private
      !  Open the index file. Create new one if requested or if it does
      ! not exists. Also return its memory identifier.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: indexfile  !
      logical,          intent(in)    :: overwrite  !
      character(len=*), intent(in)    :: directory  ! The directory indexed by the index file
      integer(kind=4),  intent(out)   :: fileid     !
      logical,          intent(inout) :: error      !
    end subroutine mrtindex_file_new
  end interface
  !
  interface
    subroutine mrtindex_file_classic_new(filename,error)
      use gbl_message
      use classic_api
      use mrtindex_types
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      !  Create (from a Classic point of view) a new index file
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: filename  !
      logical,          intent(inout) :: error     ! Logical error flag
    end subroutine mrtindex_file_classic_new
  end interface
  !
  interface
    subroutine mrtindex_file_classic_old(fileid,readwrite,error)
      use mrtindex_messaging
      use mrtindex_types
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in)    :: fileid     !
      logical,          intent(in)    :: readwrite  !
      logical,          intent(inout) :: error      !
    end subroutine mrtindex_file_classic_old
  end interface
  !
  interface
    subroutine mrtindex_file_close(file,error)
      use classic_api
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(classic_file_t), intent(inout) :: file
      logical,              intent(inout) :: error
    end subroutine mrtindex_file_close
  end interface
  !
  interface mrtindex_file_old
    subroutine mrtindex_file_old_byname(indexfile,directory,rw,fileid,error)
      use gbl_message
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private-generic mrtindex_file_old
      !  Open an 'old' (already existing) index file in the Input indeX
      ! Open or reuse a pre-opened file, and return its identifier in the
      ! list of opened files
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: indexfile  !
      character(len=*), intent(in)    :: directory  !
      logical,          intent(in)    :: rw         ! Open in RW mode?
      integer(kind=4),  intent(out)   :: fileid     !
      logical,          intent(inout) :: error      !
    end subroutine mrtindex_file_old_byname
    subroutine mrtindex_file_old_byid(fileid,rw,error)
      use gbl_message
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private-generic mrtindex_file_old
      !  Open an 'old' (already existing) index file in the Input indeX
      ! Reuse a pre-opened file given its identifier
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in)    :: fileid   !
      logical,          intent(in)    :: rw       ! Open in RW mode?
      logical,          intent(inout) :: error    !
    end subroutine mrtindex_file_old_byid
  end interface mrtindex_file_old
  !
  interface
    subroutine mrtindex_find_parse(line,fuser,error)
      use imbfits_parameters
      use mrtindex_parse_types
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !  Parse the fuser input for selection criteria
      !---------------------------------------------------------------------
      character(len=*),   intent(in)    :: line   !
      type(user_calib_t), intent(out)   :: fuser   !
      logical,            intent(inout) :: error  !
    end subroutine mrtindex_find_parse
  end interface
  !
  interface
    subroutine mrtindex_find_criter(user,criter,error)
      use gbl_message
      use imbfits_parameters
      use mrtindex_parse_types
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !  Convert the user inputs into selection criteria
      !---------------------------------------------------------------------
      type(user_calib_t),  intent(in)    :: user    !
      type(mrtindex_find_t), intent(out)   :: criter  !
      logical,             intent(inout) :: error   !
    end subroutine mrtindex_find_criter
  end interface
  !
  interface
    subroutine mrtindex_index_update(line,overwrite,ix,error)
      use gildas_def
      use mrtindex_types
      use mrtindex_parse_types
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command
      !   INDEX BUILD|UPDATE [DirName] [/FILE IndexFile] [/RECURSIVE]
      ! Build or update the scans index.
      !---------------------------------------------------------------------
      character(len=*),          intent(in)    :: line       ! Input command line
      logical,                   intent(in)    :: overwrite  ! Overwrite or update only?
      type(mrtindex_optimize_t), intent(inout) :: ix
      logical,                   intent(inout) :: error      ! Logical error flag
    end subroutine mrtindex_index_update
  end interface
  !
  interface
    recursive subroutine mrtindex_index_build_recurs(directory,pattern,error)
      use gildas_def
      use gbl_message
      use mrtindex_parse_types
      !---------------------------------------------------------------------
      ! @ private
      !  Build index files recursively from given directory
      !---------------------------------------------------------------------
      character(len=*),     intent(in)    :: directory  !
      type(user_pattern_t), intent(in)    :: pattern    ! Pattern for file to be indexed
      logical,              intent(inout) :: error      !
    end subroutine mrtindex_index_build_recurs
  end interface
  !
  interface
    recursive subroutine mrtindex_index_update_recurs(directory,pattern,error)
      use gildas_def
      use gbl_message
      use mrtindex_parse_types
      !---------------------------------------------------------------------
      ! @ private
      !  Update index files recursively from given directory
      !---------------------------------------------------------------------
      character(len=*),     intent(in)    :: directory  !
      type(user_pattern_t), intent(in)    :: pattern    ! Pattern for file to be indexed
      logical,              intent(inout) :: error      !
    end subroutine mrtindex_index_update_recurs
  end interface
  !
  interface
    subroutine mrtindex_index_open(line,append,ix,error)
      use gildas_def
      use gbl_message
      use classic_api
      use mrtindex_types
      use mrtindex_parse_types
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command
      !   INDEX OPEN|APPEND [DirName] [/FILE IndexFile] [/RECURSIVE]
      ! Load in memory an index file
      !---------------------------------------------------------------------
      character(len=*),          intent(in)    :: line    ! Input command line
      logical,                   intent(in)    :: append  ! Overwrite or append in memory?
      type(mrtindex_optimize_t), intent(inout) :: ix
      logical,                   intent(inout) :: error   ! Logical error flag
    end subroutine mrtindex_index_open
  end interface
  !
  interface
    subroutine mrtindex_parse(line,dirname,filename,pattern,error)
      use gbl_message
      use mrtindex_parse_types
      !---------------------------------------------------------------------
      ! @ private
      !  Parse command line of the form
      !   COMMAND YYY Dirname /OPT1 FileName /OPT3 Pattern
      !---------------------------------------------------------------------
      character(len=*),     intent(in)    :: line      ! Input command line
      character(len=*),     intent(out)   :: dirname   !
      character(len=*),     intent(out)   :: filename  !
      type(user_pattern_t), intent(out)   :: pattern   ! For files to be indexed
      logical,              intent(inout) :: error     ! Logical error flag
    end subroutine mrtindex_parse
  end interface
  !
  interface
    recursive subroutine mrtindex_open_recurs(directory,ix,error)
      use gildas_def
      use gbl_message
      use mrtindex_types
      use mrtindex_parse_types
      !---------------------------------------------------------------------
      ! @ private
      !  Open and load in IX index files recursively found in given
      ! directory
      !---------------------------------------------------------------------
      character(len=*),          intent(in)    :: directory  !
      type(mrtindex_optimize_t), intent(inout) :: ix
      logical,                   intent(inout) :: error      !
    end subroutine mrtindex_open_recurs
  end interface
  !
  interface
    subroutine mrtindex_index_output(line,error)
      use gildas_def
      use gbl_message
      use mrtindex_vars
      use mrtindex_parse_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create the new INDEX OUTPUT file, used for saving the calibrated
      ! entries (default is to save them in the index they come from).
      !  Note that the associated directory is left undeclared, which may
      ! mean more or less troubles...
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   ! Input command line
      logical,          intent(inout) :: error  ! Logical error flag
    end subroutine mrtindex_index_output
  end interface
  !
  interface
    subroutine mrtindex_ix_sort(ix,error)
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !  Sort the whole IX. Can be useful if e.g. 1) the index.mrt was not
      ! sorted, or 2) if we merged (APPEND or /RECURSIVE) several index
      ! files
      !---------------------------------------------------------------------
      type(mrtindex_optimize_t), intent(inout) :: ix
      logical,                   intent(inout) :: error
    end subroutine mrtindex_ix_sort
  end interface
  !
  interface
    subroutine mrtindex_ix_setnum(ix,error)
      use classic_api
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(mrtindex_optimize_t), intent(inout) :: ix
      logical,                   intent(inout) :: error
    end subroutine mrtindex_ix_setnum
  end interface
  !
  interface
    subroutine mrtindex_ix_close(error)
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      ! Close all index files used in IX
      !---------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine mrtindex_ix_close
  end interface
  !
  interface
    subroutine reallocate_index_ibuf(file,error)
      use classic_api
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      !  Reallocate the Input working buffers according to given file
      !---------------------------------------------------------------------
      type(classic_file_t), intent(in)    :: file   !
      logical,              intent(inout) :: error  !
    end subroutine reallocate_index_ibuf
  end interface
  !
  interface
    subroutine reallocate_index_obuf(file,error)
      use classic_api
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      !  Reallocate the Ouput working buffers according to given file
      !---------------------------------------------------------------------
      type(classic_file_t), intent(in)    :: file   !
      logical,              intent(inout) :: error  !
    end subroutine reallocate_index_obuf
  end interface
  !
  interface
    subroutine mrtindex_list_one_raw(optx,ient,mess,error)
      use classic_api
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   MLIST [IN|OUT] [/FILE OutputFile]
      ! Print 1 line for 1 entry (debugging output => raw values)
      !---------------------------------------------------------------------
      type(mrtindex_optimize_t),  intent(in)    :: optx   !
      integer(kind=entry_length), intent(in)    :: ient   !
      character(len=*),           intent(out)   :: mess   !
      logical,                    intent(inout) :: error  !
    end subroutine mrtindex_list_one_raw
  end interface
  !
  interface
    subroutine mrtindex_list_one_custom(optx,ient,custom,mess,error)
      use gbl_constant
      use phys_const
      use classic_api
      use imbfits_parameters
      use mrtindex_types
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   MLIST [IN|OUT]
      ! Print 1 line for 1 entry (custom output)
      !---------------------------------------------------------------------
      type(mrtindex_optimize_t),  intent(in)    :: optx       !
      integer(kind=entry_length), intent(in)    :: ient       !
      integer(kind=4),            intent(in)    :: custom(:)  !
      character(len=*),           intent(out)   :: mess       !
      logical,                    intent(inout) :: error      !
    end subroutine mrtindex_list_one_custom
  end interface
  !
  interface
    subroutine mrtindex_list_one_print(message,olun)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Send the message to the given Logical Unit. Quite generic, could
      ! be used in other contexts.
      !---------------------------------------------------------------------
      character(len=*), intent(in) :: message
      integer(kind=4),  intent(in) :: olun
    end subroutine mrtindex_list_one_print
  end interface
  !
  interface
    subroutine mrtindex_toc_init(toc,error)
      use toc_types
      use classic_api
      !---------------------------------------------------------------------
      ! @ private
      ! MRTCAL Support routine for command MLIST /TOC
      ! Initialization routine
      !---------------------------------------------------------------------
      type(toc_t), intent(inout) :: toc    !
      logical,     intent(inout) :: error  !
    end subroutine mrtindex_toc_init
  end interface
  !
  interface
    subroutine mrtindex_toc_clean(error)
      use classic_api
      !---------------------------------------------------------------------
      ! @ private
      ! MRTCAL Support routine for command MLIST /TOC
      ! Clean the global variable holding the TOC
      !---------------------------------------------------------------------
      logical, intent(inout) :: error  !
    end subroutine mrtindex_toc_clean
  end interface
  !
  interface
    subroutine mrtindex_list_toc(optx,keywords,tocname,olun,error)
      use classic_api
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      ! MRTCAL Support routine for command
      !   MLIST /TOC
      ! Processing routine
      !---------------------------------------------------------------------
      type(mrtindex_optimize_t), intent(in)    :: optx         ! The optx whose TOC is desired
      character(len=*),          intent(in)    :: keywords(:)  !
      character(len=*),          intent(in)    :: tocname      ! Structure name
      integer(kind=4),           intent(in)    :: olun         !
      logical,                   intent(inout) :: error        ! Logical error flag
    end subroutine mrtindex_list_toc
  end interface
  !
  interface
    subroutine mrtindex_toc_datasetup(toc,idx)
      use toc_types
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      ! MRTCAL support routine for command
      !   MLIST /TOC
      ! Associate the TOC data pointers to the index whose TOC is desired
      !---------------------------------------------------------------------
      type(toc_t),               intent(inout) :: toc
      type(mrtindex_optimize_t), intent(in)    :: idx  ! The idx whose TOC is desired
    end subroutine mrtindex_toc_datasetup
  end interface
  !
  interface
    subroutine mrtindex_toc_format(key,ival,output)
      use toc_types
      use imbfits_parameters
      use mrtindex_parameters
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! MRTCAL support routine for command
      !   MLIST /TOC
      ! Custom display routine (because there are custom types)
      !---------------------------------------------------------------------
      type(toc_descriptor_t),     intent(in)  :: key     !
      integer(kind=entry_length), intent(in)  :: ival    !
      character(len=*),           intent(out) :: output  !
    end subroutine mrtindex_toc_format
  end interface
  !
  interface
    subroutine mrtindex_message(mkind,procname,message)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Messaging facility for the current library. Calls the low-level
      ! (internal) messaging routine with its own identifier.
      !---------------------------------------------------------------------
      integer,          intent(in) :: mkind     ! Message kind
      character(len=*), intent(in) :: procname  ! Name of calling procedure
      character(len=*), intent(in) :: message   ! Message string
    end subroutine mrtindex_message
  end interface
  !
  interface
    subroutine mrtindex_open_one(indexfile,directory,ix,error)
      use classic_api
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      !  Open the index file and append IX accordingly
      !---------------------------------------------------------------------
      character(len=*),          intent(in)    :: indexfile  !
      character(len=*),          intent(in)    :: directory  !
      type(mrtindex_optimize_t), intent(inout) :: ix
      logical,                   intent(inout) :: error      !
    end subroutine mrtindex_open_one
  end interface
  !
  interface
    subroutine mrtindex_index_read(file,idir,optx,error)
      use gbl_message
      use classic_api
      use imbfits_parameters
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !   Read all entries in one index file, and append them to the given
      ! index.
      !---------------------------------------------------------------------
      type(classic_file_t),    intent(in)    :: file    !
      integer(kind=4),         intent(in)    :: idir    ! Directory identifier
      type(mrtindex_optimize_t), intent(inout) :: optx    !
      logical,                 intent(inout) :: error   ! Logical error flag
    end subroutine mrtindex_index_read
  end interface
  !
  interface
    subroutine mrtindex_entry_read(filein,ient,entry,error)
      use gbl_message
      use classic_api
      use mrtindex_types
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      ! Read the ient-th entry in the output file
      !---------------------------------------------------------------------
      type(classic_file_t),       intent(in)    :: filein   !
      integer(kind=entry_length), intent(in)    :: ient     !
      type(mrtindex_entry_t),       intent(inout) :: entry    !
      logical,                    intent(inout) :: error    ! Logical error flag
    end subroutine mrtindex_entry_read
  end interface
  !
  interface
    subroutine mrtindex_entry_rheader_sub(filein,ient,bloc,word,edesc,head,error)
      use gildas_def
      use classic_api
      use mrtindex_vars
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !  Read the header of the ient-th entry in file.
      !  Also return the entry descriptor as a side product, which is
      ! useful in some cases
      ! ---
      !   DO NOT CALL DIRECTLY!!! Use generic entry point
      ! 'mrtindex_entry_rheader' instead
      !---------------------------------------------------------------------
      type(classic_file_t),       intent(in)    :: filein  !
      integer(kind=entry_length), intent(in)    :: ient    ! Position in file
      integer(kind=8),            intent(in)    :: bloc    ! In which record?
      integer(kind=4),            intent(in)    :: word    ! Which word in this record?
      type(classic_entrydesc_t),  intent(out)   :: edesc   !
      type(mrtindex_header_t),      intent(inout) :: head    !
      logical,                    intent(inout) :: error   !
    end subroutine mrtindex_entry_rheader_sub
  end interface
  !
  interface
    subroutine mrtindex_entry_rprim(filein,edesc,prim,error)
      use classic_api
      use mrtindex_types
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(classic_file_t),      intent(in)    :: filein  !
      type(classic_entrydesc_t), intent(in)    :: edesc   ! Entry Descriptor
      type(sec_primary_t),       intent(out)   :: prim    !
      logical,                   intent(inout) :: error   !
    end subroutine mrtindex_entry_rprim
  end interface
  !
  interface
    subroutine mrtindex_entry_rcalib(filein,edesc,calib,error)
      use gbl_message
      use classic_api
      use mrtindex_types
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(classic_file_t),      intent(in)    :: filein  !
      type(classic_entrydesc_t), intent(in)    :: edesc   ! Entry Descriptor
      type(sec_calib_t),         intent(inout) :: calib   !
      logical,                   intent(inout) :: error   !
    end subroutine mrtindex_entry_rcalib
  end interface
  !
  interface
    subroutine mrtindex_entry_rscience(filein,edesc,sci,error)
      use classic_api
      use mrtindex_types
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(classic_file_t),      intent(in)    :: filein  !
      type(classic_entrydesc_t), intent(in)    :: edesc   ! Entry Descriptor
      type(sec_science_t),       intent(out)   :: sci     !
      logical,                   intent(inout) :: error   !
    end subroutine mrtindex_entry_rscience
  end interface
  !
  interface
    subroutine mrtindex_entry_rindx(filein,ient,indx,error)
      use gbl_message
      use classic_api
      use mrtindex_types
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(classic_file_t),       intent(in)    :: filein  !
      integer(kind=entry_length), intent(in)    :: ient    ! Entry position in file
      type(mrtindex_indx_t),        intent(out)   :: indx    !
      logical,                    intent(inout) :: error   ! Logical error flag
    end subroutine mrtindex_entry_rindx
  end interface
  !
  interface
    subroutine mrtindex_index_frombuf(data,indx,conv,error)
      use gbl_message
      use classic_api
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !   Copy the Block Index into the given buffer
      !---------------------------------------------------------------------
      integer(kind=4),          intent(in)    :: data(*)  !
      type(mrtindex_indx_t),      intent(out)   :: indx     !
      type(classic_fileconv_t), intent(in)    :: conv     !
      logical,                  intent(inout) :: error    !
    end subroutine mrtindex_index_frombuf
  end interface
  !
  interface
    subroutine mrtindex_cx2sic(cx,error)
      use gbl_message
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      ! Create the MDX% Sic structure mapping the CX arrays (always available)
      !---------------------------------------------------------------------
      type(mrtindex_optimize_t), intent(in)    :: cx
      logical,                   intent(inout) :: error
    end subroutine mrtindex_cx2sic
  end interface
  !
  interface
    subroutine mrtindex_index_to_optimize_inplace(indx,fileid,fnum,mnum,num,  &
      sort,islast,optx,ioptx,error)
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !  Write the indx at the given position, overwriting the previous
      ! element, if any
      !---------------------------------------------------------------------
      type(mrtindex_indx_t),        intent(in)    :: indx    ! The entry index on disk
      integer(kind=4),            intent(in)    :: fileid  ! Index file associated to the entry
      integer(kind=entry_length), intent(in)    :: fnum    ! Position in associated file
      integer(kind=entry_length), intent(in)    :: mnum    ! Position in memory (IX)
      integer(kind=entry_length), intent(in)    :: num     ! "Observation" number
      integer(kind=entry_length), intent(in)    :: sort    ! Position in the sorted index
      logical,                    intent(in)    :: islast  ! "is-last-version" status
      type(mrtindex_optimize_t),  intent(inout) :: optx    !
      integer(kind=entry_length), intent(in)    :: ioptx   ! Position in output optx
      logical,                    intent(inout) :: error   !
    end subroutine mrtindex_index_to_optimize_inplace
  end interface
  !
  interface
    subroutine mrtindex_index_to_key(indx,key,error)
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(mrtindex_indx_t), intent(in)    :: indx   !
      type(mrtindex_key_t),  intent(inout) :: key    !
      logical,             intent(inout) :: error  !
    end subroutine mrtindex_index_to_key
  end interface
  !
  interface
    subroutine mrtindex_key_to_index(key,indx,error)
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(mrtindex_key_t),  intent(in)    :: key    !
      type(mrtindex_indx_t), intent(inout) :: indx   !
      logical,             intent(inout) :: error  !
    end subroutine mrtindex_key_to_index
  end interface
  !
  interface
    subroutine mrtindex_optimize_to_key(optx,ient,key,error)
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(mrtindex_optimize_t),    intent(in)    :: optx   !
      integer(kind=entry_length), intent(in)    :: ient   !
      type(mrtindex_key_t),         intent(out)   :: key    !
      logical,                    intent(inout) :: error  !
    end subroutine mrtindex_optimize_to_key
  end interface
  !
  interface
    subroutine mrtindex_optimize_setsort(in,error)
      use classic_api
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !  Compute the sorting list of the input index (in%sort)
      !---------------------------------------------------------------------
      type(mrtindex_optimize_t),    intent(inout) :: in           !
      logical,                    intent(inout) :: error        !
    end subroutine mrtindex_optimize_setsort
  end interface
  !
  interface
    subroutine mrtindex_entry_zindx(indx,error)
      use gbl_constant
      use imbfits_parameters
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !  Zero-ify an 'mrtindex_indx_t' structure. Default can not be in
      ! in the structure definition because of the 'sequence' statement
      !---------------------------------------------------------------------
      type(mrtindex_indx_t), intent(out)   :: indx
      logical,             intent(inout) :: error
    end subroutine mrtindex_entry_zindx
  end interface
  !
  interface
    subroutine mrtindex_entry_zkey(key,error)
      use gbl_constant
      use imbfits_parameters
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !  Zero-ify an 'mrtindex_key_t' structure
      !---------------------------------------------------------------------
      type(mrtindex_key_t), intent(out)   :: key
      logical,            intent(inout) :: error
    end subroutine mrtindex_entry_zkey
  end interface
  !
  interface
    subroutine mrtindex_entry_zprim(prim,error)
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !  Zero-ify an 'sec_primary_t' structure
      !---------------------------------------------------------------------
      type(sec_primary_t), intent(out)   :: prim
      logical,             intent(inout) :: error
    end subroutine mrtindex_entry_zprim
  end interface
  !
  interface
    subroutine mrtindex_entry_zcalib(calib,error)
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !  Zero-ify an 'sec_calib_t' structure
      !---------------------------------------------------------------------
      type(sec_calib_t), intent(inout) :: calib
      logical,           intent(inout) :: error
    end subroutine mrtindex_entry_zcalib
  end interface
  !
  interface
    subroutine mrtindex_modify_entry(mrtindex_entry_modify,entry,ient,ix,error)
      use gbl_message
      use classic_api
      use mrtindex_types
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      !  UPDATE or EXTEND an old entry in the file (on disk) and update in
      ! IX (in memory). CX is NOT updated, this is a feature.
      !  To be called by mrtindex_update_entry or mrtindex_extend_entry only!!!
      !---------------------------------------------------------------------
      interface
        subroutine mrtindex_entry_modify(fileout,entry,error)
        use classic_api
        use mrtindex_types
        type(classic_file_t),   intent(inout) :: fileout
        type(mrtindex_entry_t), intent(inout) :: entry
        logical,                intent(inout) :: error
        end subroutine mrtindex_entry_modify
      end interface
      type(mrtindex_entry_t),     intent(inout) :: entry  !
      integer(kind=entry_length), intent(in)    :: ient   ! Entry position in IX
      type(mrtindex_optimize_t),  intent(inout) :: ix
      logical,                    intent(inout) :: error  ! Logical error flag
    end subroutine mrtindex_modify_entry
  end interface
  !
  interface
    subroutine datelist_decode(line,optdate,string,error)
      !---------------------------------------------------------------------
      ! @ private
      !  Re-encode a Sic-list of dates for the CURRENT line as known and
      ! parsed by Sic, into a string of GAG dates (much easier to parse
      ! afterwards).
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line     !
      integer(kind=4),  intent(in)    :: optdate  ! Option /DATE identifier
      character(len=*), intent(out)   :: string   ! Decoded as GAG dates
      logical,          intent(inout) :: error    !
    end subroutine datelist_decode
  end interface
  !
  interface
    subroutine date_decode(string,dobs,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Decode a string date in any supported format into a gag date.
      ! Supported formats are:
      !  - DD-MMM-YYYY
      !  - YYYYMMDD
      !  - Keywords "TODAY" and "YESTERDAY"
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: string  !
      integer(kind=4),  intent(out)   :: dobs    !
      logical,          intent(inout) :: error   !
    end subroutine date_decode
  end interface
  !
  interface
    function imbfits_name(date)
      use mrtindex_parse_types
      !---------------------------------------------------------------------
      ! @ private
      !   Build an IMB-FITS file name according to the IRAM 30m standard.
      ! This function factorizes this standard at a unique place
      !---------------------------------------------------------------------
      character(len=imbfits_pattern_length) :: imbfits_name  ! Function value on return
      character(len=*), intent(in) :: date   ! The date string, can be '*'
    end function imbfits_name
  end interface
  !
  interface
    subroutine mrtindex_variable_entry_key(struct,key,ro,error)
      use gildas_def
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*),   intent(in)    :: struct  ! Structure name
      type(mrtindex_key_t), intent(in)    :: key     !
      logical,            intent(in)    :: ro      ! Read-Only?
      logical,            intent(inout) :: error   ! Logical error flag
    end subroutine mrtindex_variable_entry_key
  end interface
  !
  interface
    subroutine mrtindex_variable_entry_prim(struct,prim,ro,error)
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*),    intent(in)    :: struct  ! Structure name
      type(sec_primary_t), intent(in)    :: prim    !
      logical,             intent(in)    :: ro      ! Read-Only?
      logical,             intent(inout) :: error   ! Logical error flag
    end subroutine mrtindex_variable_entry_prim
  end interface
  !
  interface
    subroutine mrtindex_variable_entry_calib(struct,calib,ro,error)
      use gildas_def
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*),  intent(in)    :: struct  ! Structure name
      type(sec_calib_t), intent(in)    :: calib   !
      logical,           intent(in)    :: ro      ! Read-Only?
      logical,           intent(inout) :: error   ! Logical error flag
    end subroutine mrtindex_variable_entry_calib
  end interface
  !
  interface
    subroutine mrtindex_variable_entry_science(struct,sci,ro,error)
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*),    intent(in)    :: struct  ! Structure name
      type(sec_science_t), intent(in)    :: sci     !
      logical,             intent(in)    :: ro      ! Read-Only?
      logical,             intent(inout) :: error   ! Logical error flag
    end subroutine mrtindex_variable_entry_science
  end interface
  !
  interface
    subroutine mrtindex_watch_comm(line,error)
      use gbl_message
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   INDEX WATCH  [/TIMEOUT Value]
      ! Watch directories currently opened in IX
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line   !
      logical,          intent(inout) :: error  !
    end subroutine mrtindex_watch_comm
  end interface
  !
  interface
    subroutine mrtindex_watch(dirname,checktimeout,timeout,new,error)
      use gbl_message
      use gkernel_types
      !---------------------------------------------------------------------
      ! @ private
      !  Watch indefinitely as long as something happens in the directory
      ! or CTRL-C is pressed
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: dirname       ! Directory to be watched
      logical,          intent(in)    :: checktimeout  !
      real(kind=4),     intent(in)    :: timeout       ! Timeout
      logical,          intent(out)   :: new           ! Found something new?
      logical,          intent(inout) :: error         ! Logical error flag
    end subroutine mrtindex_watch
  end interface
  !
  interface
    subroutine mrtindex_index_write(fileout,dire,list,nlist,error)
      use gbl_message
      use gkernel_types
      use classic_api
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      ! Write all the new entries in the index
      !---------------------------------------------------------------------
      type(classic_file_t), intent(inout) :: fileout      !
      character(len=*),     intent(in)    :: dire         ! Working directory
      integer(kind=4),      intent(in)    :: nlist        !
      character(len=*),     intent(in)    :: list(nlist)  ! File list
      logical,              intent(inout) :: error        ! Logical error flag
    end subroutine mrtindex_index_write
  end interface
  !
  interface
    subroutine mrtindex_index_exists(optx,indx,istart,ifound)
      use classic_api
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !   Check if input index is already present in optimize list of
      ! entries. Uniqueness is ensured with the date+scan+backend.
      !   Return its existing entry number, if any.
      !---------------------------------------------------------------------
      type(mrtindex_optimize_t),    intent(in)    :: optx    !
      type(mrtindex_indx_t),        intent(in)    :: indx    !
      integer(kind=entry_length), intent(inout) :: istart  !
      integer(kind=entry_length), intent(out)   :: ifound  !
    end subroutine mrtindex_index_exists
  end interface
  !
  interface
    subroutine mrtindex_index_sort(nfile,file,sort,error)
      !---------------------------------------------------------------------
      ! @ private
      !  Sort the input list of file names
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in)    :: nfile        ! Number of files
      character(len=*), intent(in)    :: file(nfile)  ! File names
      integer(kind=4),  intent(out)   :: sort(nfile)  ! Sorted indexes
      logical,          intent(inout) :: error        ! Logical error flag
    end subroutine mrtindex_index_sort
  end interface
  !
  interface
    function mrtindex_index_modified(filename,time,error)
      !---------------------------------------------------------------------
      ! @ private
      !  Returns .true. if file was modified since the input Unix time
      ! (01-jan-1970) given in nanoseconds (integer*8)
      !---------------------------------------------------------------------
      logical :: mrtindex_index_modified  ! Function value on return
      character(len=*), intent(in)    :: filename  ! Path included
      integer(kind=8),  intent(in)    :: time      ! In nanoseconds
      logical,          intent(inout) :: error     ! Logical error flag
    end function mrtindex_index_modified
  end interface
  !
  interface
    subroutine mrtindex_entry_write(fileout,entry,error)
      use classic_api
      use mrtindex_types
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      ! Write one new entry in the output file
      !---------------------------------------------------------------------
      type(classic_file_t),  intent(inout) :: fileout  !
      type(mrtindex_entry_t),  intent(inout) :: entry    !
      logical,               intent(inout) :: error    ! Logical error flag
    end subroutine mrtindex_entry_write
  end interface
  !
  interface
    subroutine mrtindex_entry_update(fileout,entry,error)
      use classic_api
      use mrtindex_types
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Update one old entry in the output file
      !---------------------------------------------------------------------
      type(classic_file_t), intent(inout) :: fileout  !
      type(mrtindex_entry_t), intent(inout) :: entry    !
      logical,              intent(inout) :: error    ! Logical error flag
    end subroutine mrtindex_entry_update
  end interface
  !
  interface
    subroutine mrtindex_entry_extend(fileout,entry,error)
      use gbl_message
      use classic_api
      use mrtindex_types
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private-mandatory (because symbol is used elsewhere)
      ! Extend the LAST entry of the output file (in particular, update the
      ! old sections and add the new ones)
      !---------------------------------------------------------------------
      type(classic_file_t), intent(inout) :: fileout  !
      type(mrtindex_entry_t), intent(inout) :: entry    !
      logical,              intent(inout) :: error    ! Logical error flag
    end subroutine mrtindex_entry_extend
  end interface
  !
  interface
    subroutine mrtindex_entry_fill(dire,filename,indx,prim,error)
      use gbl_constant
      use gbl_message
      use phys_const
      use classic_api
      use imbfits_parameters
      use mrtindex_types
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Fill the types by reading the file Primary header
      !---------------------------------------------------------------------
      character(len=*),    intent(in)    :: dire
      character(len=*),    intent(in)    :: filename
      type(mrtindex_indx_t), intent(inout) :: indx
      type(sec_primary_t), intent(inout) :: prim
      logical,             intent(inout) :: error
    end subroutine mrtindex_entry_fill
  end interface
  !
  interface
    subroutine mrtindex_index_decode(filename,telescope,backcode,dobs,scan,error)
      use gbl_message
      use mrtindex_parameters
      !---------------------------------------------------------------------
      ! @ private
      !  Decode:
      !  - telescope [code],
      !  - backend [code],
      !  - date [gag_date],
      !  - scan number
      ! from file name.
      !  Note that the date decoded from the file name is the SCAN date. It
      ! can differ from the OBSERVATION date (as found in the MJD in Primary
      ! header) by 1 if scan starts just before midnight and observation
      ! starts just after.
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: filename   !
      integer(kind=4),  intent(out)   :: telescope  !
      integer(kind=4),  intent(out)   :: backcode   !
      integer(kind=4),  intent(out)   :: dobs       !
      integer(kind=4),  intent(out)   :: scan       !
      logical,          intent(inout) :: error      ! Logical error flag
    end subroutine mrtindex_index_decode
  end interface
  !
  interface
    subroutine mrtindex_entry_wprim(fileout,edesc,prim,new,error)
      use gbl_message
      use classic_api
      use mrtindex_types
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(classic_file_t),      intent(inout) :: fileout  !
      type(classic_entrydesc_t), intent(inout) :: edesc    ! Entry Descriptor
      type(sec_primary_t),       intent(in)    :: prim     ! The section
      logical,                   intent(in)    :: new      ! New or old (update) section?
      logical,                   intent(inout) :: error    !
    end subroutine mrtindex_entry_wprim
  end interface
  !
  interface
    subroutine mrtindex_entry_wcalib(fileout,edesc,calib,new,error)
      use gbl_message
      use classic_api
      use mrtindex_types
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(classic_file_t),      intent(inout) :: fileout  !
      type(classic_entrydesc_t), intent(inout) :: edesc    ! Entry Descriptor
      type(sec_calib_t),         intent(in)    :: calib    ! The section
      logical,                   intent(in)    :: new      ! New or old (update) section?
      logical,                   intent(inout) :: error    !
    end subroutine mrtindex_entry_wcalib
  end interface
  !
  interface
    subroutine mrtindex_entry_wscience(fileout,edesc,sci,new,error)
      use gbl_message
      use classic_api
      use mrtindex_types
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(classic_file_t),      intent(inout) :: fileout  !
      type(classic_entrydesc_t), intent(inout) :: edesc    ! Entry Descriptor
      type(sec_science_t),       intent(in)    :: sci      ! The section
      logical,                   intent(in)    :: new      ! New or old (update) section?
      logical,                   intent(inout) :: error    !
    end subroutine mrtindex_entry_wscience
  end interface
  !
  interface
    subroutine mrtindex_entry_windx(file,ient,indx,error)
      use gbl_message
      use classic_api
      use mrtindex_types
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(classic_file_t),       intent(in)    :: file   !
      integer(kind=entry_length), intent(in)    :: ient   !
      type(mrtindex_indx_t),        intent(in)    :: indx   !
      logical,                    intent(inout) :: error  ! Logical error flag
    end subroutine mrtindex_entry_windx
  end interface
  !
  interface
    subroutine mrtindex_index_tobuf(indx,data,conv,error)
      use gildas_def
      use classic_api
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ private
      !   Copy the Block Index into the given buffer
      !---------------------------------------------------------------------
      type(mrtindex_indx_t),      intent(in)    :: indx     !
      integer(kind=4),          intent(out)   :: data(*)  !
      type(classic_fileconv_t), intent(in)    :: conv     !
      logical,                  intent(inout) :: error    !
    end subroutine mrtindex_index_tobuf
  end interface
  !
end module mrtindex_interfaces_private
