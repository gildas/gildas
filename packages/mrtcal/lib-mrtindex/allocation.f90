subroutine reallocate_calib_section(nfreq,nset,npix,calib,error)
  use gbl_message
  use gkernel_interfaces
  use mrtindex_interfaces, except_this=>reallocate_calib_section
  use mrtindex_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4),   intent(in)    :: nfreq  !
  integer(kind=4),   intent(in)    :: nset   !
  integer(kind=4),   intent(in)    :: npix   !
  type(sec_calib_t), intent(inout) :: calib  !
  logical,           intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='REALLOCATE>CALIB>SECTION'
  character(len=message_length) :: mess
  logical :: alloc
  integer(kind=4) :: ier
  !
  if (nfreq.lt.0 .or. nset.lt.0 .or. npix.lt.0) then
    ! NB: zero-sized arrays are legal Fortran
    write(mess,'(A,3(I0,A))')  &
      'Array size can not be negative (got ',nfreq,'x',nset,'x',npix,')'
    call mrtindex_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  alloc = .true.
  if (allocated(calib%freq)) then
    if (ubound(calib%freq,1).eq.nfreq .and.  &  ! Strict equality
        ubound(calib%freq,2).eq.nset  .and.  &  ! Strict equality
        ubound(calib%freq,3).eq.npix) then      ! Strict equality
      alloc = .false.
    else
      call free_calib_section(calib,error)
      if (error)  return
    endif
  endif
  !
  if (alloc) then
    allocate(calib%frontend(nset),          &
             calib%freq(nfreq,nset,npix),   &
             calib%atsys(nfreq,nset,npix),  &
             calib%ztau(nfreq,nset,npix),stat=ier)
    if (failed_allocate(rname,'CALIB ARRAYS',ier,error)) then
      call free_calib_section(calib,error)
      return
    endif
  endif
  !
  calib%nfreq = nfreq
  calib%nset  = nset
  calib%npix  = npix
  !
end subroutine reallocate_calib_section
!
subroutine free_calib_section(calib,error)
  use mrtindex_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(sec_calib_t), intent(inout) :: calib  !
  logical,           intent(inout) :: error  !
  !
  calib%nfreq = 0
  calib%nset  = 0
  calib%npix  = 0
  if (allocated(calib%frontend))  deallocate(calib%frontend)
  if (allocated(calib%freq))      deallocate(calib%freq)
  if (allocated(calib%atsys))     deallocate(calib%atsys)
  if (allocated(calib%ztau))      deallocate(calib%ztau)
  !
end subroutine free_calib_section
!
subroutine reallocate_mrtoptimize_expo(optx,mobs,error)
  use mrtindex_interfaces, except_this=>reallocate_mrtoptimize_expo
  use mrtindex_types
  !---------------------------------------------------------------------
  ! @ private-generic reallocate_mrtoptimize
  !  Reallocate the 'optimize' type arrays. If current allocation is not
  ! enough, double the size of allocation. Since this reallocation
  ! routine is used in a context of adding more and more data, data
  ! is always preserved after reallocation.
  !---------------------------------------------------------------------
  type(mrtindex_optimize_t),    intent(inout) :: optx   !
  integer(kind=entry_length), intent(in)    :: mobs   ! Requested size
  logical,                    intent(inout) :: error  ! Logical error flag
  ! Local
  integer(kind=entry_length) :: nobs
  !
  if (associated(optx%num)) then
    nobs = size(optx%num,kind=8)
    if (nobs.ge.mobs)  return       ! Enough size yet
    nobs = 2_8*nobs                 ! Request twice more place than before
    if (nobs.lt.mobs)  nobs = mobs  ! Twice is not enough, use mobs
  else
    nobs = mobs  ! No allocation yet, use mobs
  endif
  !
  call reallocate_mrtoptimize_more(optx,nobs,.true.,error)
  if (error)  return
  !
end subroutine reallocate_mrtoptimize_expo
!
subroutine reallocate_mrtoptimize_more(optx,mobs,keep,error)
  use gbl_message
  use gkernel_interfaces
  use mrtindex_interfaces, except_this=>reallocate_mrtoptimize_more
  use mrtindex_types
  !---------------------------------------------------------------------
  ! @ private-generic reallocate_mrtoptimize
  !  Allocate the 'optimize' type arrays. Enlarge the arrays to the
  ! requested size, if needed. No shrink possible. Keep data if
  ! requested.
  !---------------------------------------------------------------------
  type(mrtindex_optimize_t),    intent(inout) :: optx   !
  integer(kind=entry_length), intent(in)    :: mobs   ! Requested size
  logical,                    intent(in)    :: keep   ! Keep previous data?
  logical,                    intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='REALLOCATE>OPTIMIZE'
  integer(kind=4) :: ier
  integer(kind=entry_length) :: nobs
  integer(kind=4), allocatable :: bufi4(:)
  integer(kind=8), allocatable :: bufi8(:)
  real(kind=4), allocatable :: bufr4(:)
  real(kind=8), allocatable :: bufr8(:)
  logical, allocatable :: bufl4(:)
  character(len=8), allocatable :: bufc8(:)    ! ZZZ Merge those 3 and use only
  character(len=12), allocatable :: bufc12(:)  ! ZZZ the largest?
  character(len=fitsname_length), allocatable :: bufc40(:)  ! ZZZ
  character(len=8), allocatable :: bufc8_2d(:,:)
  !
  if (associated(optx%num)) then
    nobs = size(optx%num,kind=8)  ! Size of allocation
    if (nobs.ge.mobs) then
      ! Index is already allocated with a larger size. Keep it like this.
      ! Shouldn't we deallocate huge allocations if user requests a small one?
      return
    endif
  elseif (mobs.eq.0) then
    ! Reading or creating an empty index file, which is valid.
    ! Allocate zero-sized so that the working flow is the same. Allocation
    ! with zero-size is prefered rather than leaving the arrays unallocated,
    ! this will prevent some error at run time when passing arrays to
    ! subroutines.
    nobs = 0
  elseif (mobs.lt.0) then
    call mrtindex_message(seve%e,rname,'Can not allocate empty indexes')
    error = .true.
    return
  endif
  !
  nobs = min(nobs,optx%next-1)  ! Used part of the arrays
  if (keep) then
    allocate(bufi4(nobs),stat=ier)
    allocate(bufi8(nobs),stat=ier)
    allocate(bufr4(nobs),stat=ier)
    allocate(bufr8(nobs),stat=ier)
    allocate(bufc8(nobs),stat=ier)
    allocate(bufl4(nobs),stat=ier)
    allocate(bufc12(nobs),stat=ier)
    allocate(bufc40(nobs),stat=ier)
    allocate(bufc8_2d(mrtcal_indx_mfrontend,nobs),stat=ier)  ! ZZZ Not generic...
                                              ! ... ZZZ except if using Fortran 2003
                                              ! allocatable character(len=:)
    if (failed_allocate(rname,'buf arrays',ier,error)) then
      error = .true.
      return
    endif
  endif
  !
  call reallocate_mrtoptimize_i8('bloc array',optx%bloc,error)
  if (error)  return
  call reallocate_mrtoptimize_i4('word array',optx%word,error)
  if (error)  return
  call reallocate_mrtoptimize_i4('version array',optx%version,error)
  if (error)  return
  call reallocate_mrtoptimize_i4('telescope array',optx%telescope,error)
  if (error)  return
  call reallocate_mrtoptimize_c8('projid array',optx%projid,error)
  if (error)  return
  call reallocate_mrtoptimize_c12('source array',optx%source,error)
  if (error)  return
  call reallocate_mrtoptimize_i4('dobs array',optx%dobs,error)
  if (error)  return
  call reallocate_mrtoptimize_r8('ut array',optx%ut,error)
  if (error)  return
  call reallocate_mrtoptimize_r8('lst array',optx%lst,error)
  if (error)  return
  call reallocate_mrtoptimize_r4('az array',optx%az,error)
  if (error)  return
  call reallocate_mrtoptimize_r4('el array',optx%el,error)
  if (error)  return
  call reallocate_mrtoptimize_r8('lon array',optx%lon,error)
  if (error)  return
  call reallocate_mrtoptimize_r8('lat array',optx%lat,error)
  if (error)  return
  call reallocate_mrtoptimize_i4('system array',optx%system,error)
  if (error)  return
  call reallocate_mrtoptimize_r4('equinox array',optx%equinox,error)
  if (error)  return
  call reallocate_mrtoptimize_c8_2d('frontend array',optx%frontend,error)
  if (error)  return
  call reallocate_mrtoptimize_i4('scan array',optx%scan,error)
  if (error)  return
  call reallocate_mrtoptimize_i4('backend array',optx%backend,error)
  if (error)  return
  call reallocate_mrtoptimize_i4('obstype array',optx%obstype,error)
  if (error)  return
  call reallocate_mrtoptimize_i4('switchmode array',optx%switchmode,error)
  if (error)  return
  call reallocate_mrtoptimize_i4('polstatus array',optx%polstatus,error)
  if (error)  return
  call reallocate_mrtoptimize_i4('filstatus array',optx%filstatus,error)
  if (error)  return
  call reallocate_mrtoptimize_i4('calstatus array',optx%calstatus,error)
  if (error)  return
  call reallocate_mrtoptimize_c40('filename array',optx%filename,error)
  if (error)  return
  call reallocate_mrtoptimize_i8('itime array',optx%itime,error)
  if (error)  return
  call reallocate_mrtoptimize_i8('num array',optx%num,error)
  if (error)  return
  call reallocate_mrtoptimize_i8('mnum array',optx%mnum,error)
  if (error)  return
  call reallocate_mrtoptimize_i8('fnum array',optx%fnum,error)
  if (error)  return
  call reallocate_mrtoptimize_i8('sort array',optx%sort,error)
  if (error)  return
  call reallocate_mrtoptimize_i4('dir array',optx%idir,error)
  if (error)  return
  call reallocate_mrtoptimize_l4('islast array',optx%islast,error)
  if (error)  return
  !
  if (keep) then
    if (allocated(bufi4))     deallocate(bufi4)
    if (allocated(bufi8))     deallocate(bufi8)
    if (allocated(bufr4))     deallocate(bufr4)
    if (allocated(bufr8))     deallocate(bufr8)
    if (allocated(bufl4))     deallocate(bufl4)
    if (allocated(bufc8))     deallocate(bufc8)
    if (allocated(bufc12))    deallocate(bufc12)
    if (allocated(bufc40))    deallocate(bufc40)
    if (allocated(bufc8_2d))  deallocate(bufc8_2d)
  endif
  !
contains
  subroutine reallocate_mrtoptimize_i4(name,val,error)
    character(len=*), intent(in)    :: name
    integer(kind=4),  pointer       :: val(:)
    logical,          intent(inout) :: error
    ! Local
    integer(kind=4) :: ier
    !
    if (keep)  bufi4(:) = val(1:nobs)
    if (associated(val)) deallocate(val)
    allocate(val(mobs),stat=ier)
    if (failed_allocate(rname,name,ier,error))  return
    if (keep) val(1:nobs) = bufi4(:)
  end subroutine reallocate_mrtoptimize_i4
  !
  subroutine reallocate_mrtoptimize_i8(name,val,error)
    character(len=*), intent(in)    :: name
    integer(kind=8),  pointer       :: val(:)
    logical,          intent(inout) :: error
    ! Local
    integer(kind=4) :: ier
    !
    if (keep)  bufi8(:) = val(1:nobs)
    if (associated(val)) deallocate(val)
    allocate(val(mobs),stat=ier)
    if (failed_allocate(rname,name,ier,error))  return
    if (keep) val(1:nobs) = bufi8(:)
  end subroutine reallocate_mrtoptimize_i8
  !
  subroutine reallocate_mrtoptimize_r4(name,val,error)
    character(len=*), intent(in)    :: name
    real(kind=4),     pointer       :: val(:)
    logical,          intent(inout) :: error
    ! Local
    integer(kind=4) :: ier
    !
    if (keep)  bufr4(:) = val(1:nobs)
    if (associated(val)) deallocate(val)
    allocate(val(mobs),stat=ier)
    if (failed_allocate(rname,name,ier,error))  return
    if (keep) val(1:nobs) = bufr4(:)
  end subroutine reallocate_mrtoptimize_r4
  !
  subroutine reallocate_mrtoptimize_r8(name,val,error)
    character(len=*), intent(in)    :: name
    real(kind=8),     pointer       :: val(:)
    logical,          intent(inout) :: error
    ! Local
    integer(kind=4) :: ier
    !
    if (keep)  bufr8(:) = val(1:nobs)
    if (associated(val)) deallocate(val)
    allocate(val(mobs),stat=ier)
    if (failed_allocate(rname,name,ier,error))  return
    if (keep) val(1:nobs) = bufr8(:)
  end subroutine reallocate_mrtoptimize_r8
  !
  subroutine reallocate_mrtoptimize_l4(name,val,error)
    character(len=*), intent(in)    :: name
    logical,          pointer       :: val(:)
    logical,          intent(inout) :: error
    ! Local
    integer(kind=4) :: ier
    !
    if (keep)  bufl4(:) = val(1:nobs)
    if (associated(val)) deallocate(val)
    allocate(val(mobs),stat=ier)
    if (failed_allocate(rname,name,ier,error))  return
    if (keep) val(1:nobs) = bufl4(:)
  end subroutine reallocate_mrtoptimize_l4
  !
  subroutine reallocate_mrtoptimize_c8(name,val,error)
    character(len=*), intent(in)    :: name
    character(len=8), pointer       :: val(:)
    logical,          intent(inout) :: error
    ! Local
    integer(kind=4) :: ier
    !
    if (keep)  bufc8(:) = val(1:nobs)
    if (associated(val)) deallocate(val)
    allocate(val(mobs),stat=ier)
    if (failed_allocate(rname,name,ier,error))  return
    if (keep) val(1:nobs) = bufc8(:)
  end subroutine reallocate_mrtoptimize_c8
  !
  subroutine reallocate_mrtoptimize_c12(name,val,error)
    character(len=*),  intent(in)    :: name
    character(len=12), pointer       :: val(:)
    logical,           intent(inout) :: error
    ! Local
    integer(kind=4) :: ier
    !
    if (keep)  bufc12(:) = val(1:nobs)
    if (associated(val)) deallocate(val)
    allocate(val(mobs),stat=ier)
    if (failed_allocate(rname,name,ier,error))  return
    if (keep) val(1:nobs) = bufc12(:)
  end subroutine reallocate_mrtoptimize_c12
  !
  subroutine reallocate_mrtoptimize_c40(name,val,error)
    character(len=*),               intent(in)    :: name
    character(len=fitsname_length), pointer       :: val(:)
    logical,                        intent(inout) :: error
    ! Local
    integer(kind=4) :: ier
    !
    if (keep)  bufc40(:) = val(1:nobs)
    if (associated(val)) deallocate(val)
    allocate(val(mobs),stat=ier)
    if (failed_allocate(rname,name,ier,error))  return
    if (keep) val(1:nobs) = bufc40(:)
  end subroutine reallocate_mrtoptimize_c40
  !
  subroutine reallocate_mrtoptimize_c8_2d(name,val,error)
    character(len=*), intent(in)    :: name
    character(len=8), pointer       :: val(:,:)
    logical,          intent(inout) :: error
    ! Local
    integer(kind=4) :: ier
    !
    if (keep)  bufc8_2d(:,:) = val(:,1:nobs)
    if (associated(val)) deallocate(val)
    allocate(val(mrtcal_indx_mfrontend,mobs),stat=ier)  ! ZZZ Not generic
    if (failed_allocate(rname,name,ier,error))  return
    if (keep) val(:,1:nobs) = bufc8_2d(:,:)
  end subroutine reallocate_mrtoptimize_c8_2d
  !
end subroutine reallocate_mrtoptimize_more
!
subroutine deallocate_mrtoptimize(optx,error)
  use mrtindex_types
  !---------------------------------------------------------------------
  ! @ public
  !  Deallocate the 'optimize' type arrays.
  !  Accomodates well with error = .true. as input.
  !---------------------------------------------------------------------
  type(mrtindex_optimize_t), intent(inout) :: optx   !
  logical,                 intent(inout) :: error  ! Logical error flag
  ! Local
  integer(kind=4) :: ier
  !
  ier = 0
  if (associated(optx%bloc))          deallocate(optx%bloc,stat=ier)
  if (associated(optx%word))          deallocate(optx%word,stat=ier)
  if (associated(optx%version))       deallocate(optx%version,stat=ier)
  if (associated(optx%telescope))     deallocate(optx%telescope,stat=ier)
  if (associated(optx%projid))        deallocate(optx%projid,stat=ier)
  if (associated(optx%source))        deallocate(optx%source,stat=ier)
  if (associated(optx%dobs))          deallocate(optx%dobs,stat=ier)
  if (associated(optx%ut))            deallocate(optx%ut,stat=ier)
  if (associated(optx%lst))           deallocate(optx%lst,stat=ier)
  if (associated(optx%az))            deallocate(optx%az,stat=ier)
  if (associated(optx%el))            deallocate(optx%el,stat=ier)
  if (associated(optx%lon))           deallocate(optx%lon,stat=ier)
  if (associated(optx%lat))           deallocate(optx%lat,stat=ier)
  if (associated(optx%system))        deallocate(optx%system,stat=ier)
  if (associated(optx%equinox))       deallocate(optx%equinox,stat=ier)
  if (associated(optx%frontend))      deallocate(optx%frontend,stat=ier)
  if (associated(optx%scan))          deallocate(optx%scan,stat=ier)
  if (associated(optx%backend))       deallocate(optx%backend,stat=ier)
  if (associated(optx%obstype))       deallocate(optx%obstype,stat=ier)
  if (associated(optx%switchmode))    deallocate(optx%switchmode,stat=ier)
  if (associated(optx%polstatus))     deallocate(optx%polstatus,stat=ier)
  if (associated(optx%filstatus))     deallocate(optx%filstatus,stat=ier)
  if (associated(optx%calstatus))     deallocate(optx%calstatus,stat=ier)
  if (associated(optx%filename))      deallocate(optx%filename,stat=ier)
  if (associated(optx%itime))         deallocate(optx%itime,stat=ier)
  if (associated(optx%num))           deallocate(optx%num,stat=ier)
  if (associated(optx%mnum))          deallocate(optx%mnum,stat=ier)
  if (associated(optx%fnum))          deallocate(optx%fnum,stat=ier)
  if (associated(optx%sort))          deallocate(optx%sort,stat=ier)
  if (associated(optx%idir))          deallocate(optx%idir,stat=ier)
  if (associated(optx%islast))        deallocate(optx%islast,stat=ier)
  if (ier.ne.0) error = .true.
  !
end subroutine deallocate_mrtoptimize
!
subroutine reallocate_ix_dirs(ndir,error)
  use gbl_message
  use gkernel_interfaces
  use classic_api
  use mrtindex_interfaces, except_this=>reallocate_ix_dirs
  use mrtindex_vars
  !---------------------------------------------------------------------
  ! @ private
  !  Reallocate the support arrays for indexing several directories
  ! in memory. If reallocation is needed, double the size of current
  ! allocation.
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)    :: ndir
  logical,         intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='REALLOCATE>IX>DIRS'
  character(len=message_length) :: mess
  integer(kind=4) :: cdir,mdir,ier,ifile
  logical :: keep
  character(len=filename_length), allocatable :: tmp_dirs(:)
  type(classic_file_t), allocatable :: tmp_files(:)
  !
  if (ndir.le.0) then
    write(mess,'(A,I0,A)')  'Array size can not be zero nor negative (got ',ndir,')'
    call mrtindex_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  keep = .false.
  if (allocated(ix_dirs)) then
    cdir = size(ix_dirs)  ! Current allocation
    if (cdir.ge.ndir) then
      return
    else
      allocate(tmp_dirs(ix_ndir),tmp_files(ix_ndir),stat=ier)
      if (failed_allocate(rname,'TMP_DIRS',ier,error))  return
      do ifile=1,ix_ndir
        tmp_dirs(ifile) = ix_dirs(ifile)
        call classic_file_copy(ix_files(ifile),tmp_files(ifile),error)
        if (error)  return
      enddo
      keep = .true.
      !
      call free_ix_dirs(error)
      if (error)  return
      mdir = max(ndir,2*cdir)
    endif
  else
    mdir = ndir
  endif
  !
  allocate(ix_dirs(mdir),ix_files(mdir),stat=ier)
  if (failed_allocate(rname,'IX_DIRS',ier,error)) then
    call free_ix_dirs(error)
    return
  endif
  !
  if (keep) then
    do ifile=1,ix_ndir
      ix_dirs(ifile) = tmp_dirs(ifile)
      call classic_file_copy(tmp_files(ifile),ix_files(ifile),error)
      if (error)  return
    enddo
    deallocate(tmp_dirs,tmp_files)
  endif
  !
end subroutine reallocate_ix_dirs
!
subroutine free_ix_dirs(error)
  use mrtindex_vars
  !---------------------------------------------------------------------
  ! @ private
  !   Deallocate the support arrays for indexing several directories in
  ! memory.
  !---------------------------------------------------------------------
  logical, intent(inout) :: error
  !
  if (allocated(ix_dirs))   deallocate(ix_dirs)
  if (allocated(ix_files))  deallocate(ix_files)
end subroutine free_ix_dirs
