subroutine mrtindex_watch_comm(line,error)
  use gbl_message
  use gkernel_interfaces
  use mrtindex_interfaces, except_this=>mrtindex_watch_comm
  use mrtindex_vars
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !   INDEX WATCH  [/TIMEOUT Value]
  ! Watch directories currently opened in IX
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line   !
  logical,          intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='INDEX>WATCH'
  character(len=filename_length) :: directory,indexfile
  type(user_pattern_t) :: pattern
  real(kind=4) :: timeout
  logical :: checktimeout,newdata
  !
  if (sic_present(1,0)) then  ! INDEX WATCH /FILE
    call mrtindex_message(seve%e,rname,'Option /FILE is illegal is this context')
    call mrtindex_message(seve%e,rname,'INDEX WATCH watches for directories in the Input indeX')
    error = .true.
    return
  endif
  !
  if (sic_present(0,2)) then  ! INDEX WATCH DirName
    call mrtindex_message(seve%w,rname,'Providing a directory name is not implemented, ignored')
    call mrtindex_message(seve%w,rname,'INDEX WATCH watches only in the first known directory')
  endif
  !
  if (ix_ndir.le.0) then
    call mrtindex_message(seve%e,rname,'Nothing to watch. Use INDEX OPEN first')
    error = .true.
    return
  endif
  !
  checktimeout = sic_present(5,0)
  timeout = 0.0
  if (checktimeout) then
    call sic_r4(line,5,1,timeout,.true.,error)
    if (error)  return
  endif
  !
  ! ZZZ Should watch all directories
  call mrtindex_watch(ix_dirs(1),checktimeout,timeout,newdata,error)
  if (error)  return  ! Including CTRL-C
  if (.not.newdata)  return  ! Nothing to be done
  !
  call mrtindex_parse(line,directory,indexfile,pattern,error)
  if (error)  return
  ! 'indexfile' in return ignored
  !
  ! Something happened in the directory. Update its index.
  ! ZZZ for now update all files, but we should be able to update a single
  ! file!
  call mrtindex_file_update(ix_files(1)%spec,ix_dirs(1),pattern,.false.,  &
    .true.,error)
  if (error)  return
  ! ZZZ Should reload/update in memory
  !
end subroutine mrtindex_watch_comm
!
subroutine mrtindex_watch(dirname,checktimeout,timeout,new,error)
  use gbl_message
  use gkernel_types
  use gkernel_interfaces
  use mrtindex_interfaces, except_this=>mrtindex_watch
  !---------------------------------------------------------------------
  ! @ private
  !  Watch indefinitely as long as something happens in the directory
  ! or CTRL-C is pressed
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: dirname       ! Directory to be watched
  logical,          intent(in)    :: checktimeout  !
  real(kind=4),     intent(in)    :: timeout       ! Timeout
  logical,          intent(out)   :: new           ! Found something new?
  logical,          intent(inout) :: error         ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='INDEX>WATCH'
  type(mfile_t) :: modif
  type(cputime_t) :: time
  !
  new = .false.
  modif%modif = .false.  ! At first call
  call gag_cputime_init(time)
  !
  do  ! Infinite loop
    !
    call gag_filmodif(dirname,modif,error)
    if (error)  return
    if (modif%modif) then
      ! Something new!
      new = .true.
      return
    endif
    !
    ! Check for timeout AFTER searching for new data, so that INDEX WATCH
    ! behaves as INDEX UPDATE if there is actually new data
    if (checktimeout) then
      call gag_cputime_get(time)
      if (time%curr%elapsed.ge.timeout) then
        call mrtindex_message(seve%i,rname,'Time-out reached, leaving.')
        return  ! Stop with new=.false.
      endif
    endif
    !
    call sic_wait(1.0)
    !
    if (sic_ctrlc()) then
      error = .true.
      return
    endif
  enddo
  !
end subroutine mrtindex_watch
