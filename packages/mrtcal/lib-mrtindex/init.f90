subroutine mrtindex_init(error)
  use mrtindex_dependencies_interfaces
  use mrtindex_interfaces, except_this=>mrtindex_init
  use mrtindex_vars
  !----------------------------------------------------------------------
  ! @ public
  ! MRTINDEX Initialization
  !----------------------------------------------------------------------
  logical, intent(inout) :: error
  !
  ! Pre-reserved logical units for index files
  if (sic_getlun(ix_lun).ne.1) then
    error = .true.
    return
  endif
  if (sic_getlun(ox_lun).ne.1) then
    error = .true.
    return
  endif
  !
  call classic_init(error)
  if (error)  return
  !
end subroutine mrtindex_init
!
subroutine mrtindex_exit(error)
  use mrtindex_dependencies_interfaces
  use mrtindex_interfaces, except_this=>mrtindex_exit
  use mrtindex_vars
  !----------------------------------------------------------------------
  ! @ public
  ! MRTINDEX Cleaning on exit
  !----------------------------------------------------------------------
  logical, intent(inout) :: error
  !
  ! LIST /TOC support structure:
  call mrtindex_toc_clean(error)
  !
  ! Deallocate read-write buffers used for Index file
  call mrtindex_ix_close(error)  ! Before free_ix_dirs
  call sic_frelun(ix_lun)
  call sic_frelun(ox_lun)
  call free_ix_dirs(error)
  !
  call deallocate_recordbuf(obufobs,error)
  call deallocate_recordbuf(obufbi,error)
  call deallocate_recordbuf(ibufobs,error)
  call deallocate_recordbuf(ibufbi,error)
  !
end subroutine mrtindex_exit
