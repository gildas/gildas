module mrtindex_interfaces_public
  interface
    subroutine deallocate_mrtoptimize(optx,error)
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ public
      !  Deallocate the 'optimize' type arrays.
      !  Accomodates well with error = .true. as input.
      !---------------------------------------------------------------------
      type(mrtindex_optimize_t), intent(inout) :: optx   !
      logical,                 intent(inout) :: error  ! Logical error flag
    end subroutine deallocate_mrtoptimize
  end interface
  !
  interface
    subroutine mrtindex_copy_command(line,cx,error)
      use gildas_def
      use gbl_message
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ public
      !  Support routine for command
      !   MCOPY FileName
      !  Copy the Current indeX from memory to disk
      !---------------------------------------------------------------------
      character(len=*),          intent(in)    :: line
      type(mrtindex_optimize_t), intent(in)    :: cx
      logical,                   intent(inout) :: error
    end subroutine mrtindex_copy_command
  end interface
  !
  interface
    subroutine mrtindex_find_comm(line,ix,cx,error)
      use gbl_message
      use mrtindex_parse_types
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ public
      !  Support routine for command:
      !   MFIND
      ! Read the index file
      !---------------------------------------------------------------------
      character(len=*),          intent(in)    :: line   ! Input command line
      type(mrtindex_optimize_t), intent(in)    :: ix
      type(mrtindex_optimize_t), intent(inout) :: cx
      logical,                   intent(inout) :: error  ! Logical error flag
    end subroutine mrtindex_find_comm
  end interface
  !
  interface
    subroutine mrtindex_find(fuser,in,out,error)
      use gbl_message
      use mrtindex_types
      use mrtindex_parse_types
      !---------------------------------------------------------------------
      ! @ public
      !  Fill OUT index from IN index according to the user inputs
      !---------------------------------------------------------------------
      type(user_calib_t),      intent(in)    :: fuser   !
      type(mrtindex_optimize_t), intent(in)    :: in     !
      type(mrtindex_optimize_t), intent(inout) :: out    !
      logical,                 intent(inout) :: error  !
    end subroutine mrtindex_find
  end interface
  !
  interface
    subroutine mrtindex_index_comm(line,ix,cx,error)
      use gbl_message
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ public
      !  Support routine for command
      !   INDEX BUILD|UPDATE|OPEN|APPEND|WATCH|OUTPUT [DirName]
      ! 1       [/FILE IndexFile]
      ! 2       [/RECURSIVE]
      !---------------------------------------------------------------------
      character(len=*),          intent(in)    :: line   ! Input command line
      type(mrtindex_optimize_t), intent(inout) :: ix
      type(mrtindex_optimize_t), intent(inout) :: cx
      logical,                   intent(inout) :: error  ! Logical error flag
    end subroutine mrtindex_index_comm
  end interface
  !
  interface
    subroutine mrtindex_init(error)
      use mrtindex_vars
      !----------------------------------------------------------------------
      ! @ public
      ! MRTINDEX Initialization
      !----------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine mrtindex_init
  end interface
  !
  interface
    subroutine mrtindex_exit(error)
      use mrtindex_vars
      !----------------------------------------------------------------------
      ! @ public
      ! MRTINDEX Cleaning on exit
      !----------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine mrtindex_exit
  end interface
  !
  interface
    subroutine mrtindex_list_columns(line,custom,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Support routine for command:
      !   MLIST /COLUMNS Key1 ... KeyN
      ! Get the custom list of columns identifiers
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line       !
      integer(kind=4),  intent(out)   :: custom(:)  !
      logical,          intent(inout) :: error      !
    end subroutine mrtindex_list_columns
  end interface
  !
  interface
    subroutine mrtindex_list(optx,name,custom,olun,page,error)
      use gbl_message
      use imbfits_parameters
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
      type(mrtindex_optimize_t), intent(in)    :: optx       !
      character(len=*),          intent(in)    :: name       !
      integer(kind=4),           intent(in)    :: custom(:)  ! Columns to be listed
      integer(kind=4),           intent(in)    :: olun       ! Output logical unit
      logical,                   intent(in)    :: page       ! Page or Scroll mode?
      logical,                   intent(inout) :: error      !
    end subroutine mrtindex_list
  end interface
  !
  interface
    subroutine mrtindex_list_one_default(optx,ient,mess,error)
      use phys_const
      use classic_api
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ public
      ! Support routine for command
      !   MLIST [IN|OUT]
      ! Print 1 line for 1 entry (default output)
      !---------------------------------------------------------------------
      type(mrtindex_optimize_t),  intent(in)    :: optx   !
      integer(kind=entry_length), intent(in)    :: ient   !
      character(len=*),           intent(out)   :: mess   !
      logical,                    intent(inout) :: error  !
    end subroutine mrtindex_list_one_default
  end interface
  !
  interface
    subroutine mrtindex_list_toc_comm(optx,line,olun,error)
      use gildas_def
      use classic_api
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ public
      ! MRTCAL support routine for command
      !   MLIST [IN|CURRENT] /TOC [Key1] ... [KeyN]  [/VARIABLE VarName]
      ! Main entry point
      !---------------------------------------------------------------------
      type(mrtindex_optimize_t), intent(in)    :: optx   !
      character(len=*),          intent(in)    :: line   ! Input command line
      integer(kind=4),           intent(in)    :: olun   !
      logical,                   intent(inout) :: error  ! Logical error flag
    end subroutine mrtindex_list_toc_comm
  end interface
  !
  interface
    subroutine mrtindex_message_set_id(id)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ public
      ! Alter library id into input id. Should be called by the library
      ! which wants to share its id with the current one.
      !---------------------------------------------------------------------
      integer, intent(in) :: id
    end subroutine mrtindex_message_set_id
  end interface
  !
  interface
    subroutine mrtindex_message_debug(doalloc,alloc,doother,other,error)
      use mrtindex_messaging
      !---------------------------------------------------------------------
      ! @ public
      !  Alter the mrtindex specific severities. You can switch them to
      ! seve%d and seve%i only!
      !---------------------------------------------------------------------
      logical,         intent(in)    :: doalloc  !
      integer(kind=4), intent(in)    :: alloc    ! For iseve%alloc
      logical,         intent(in)    :: doother  !
      integer(kind=4), intent(in)    :: other    ! For iseve%other
      logical,         intent(inout) :: error    ! Logical error flag
    end subroutine mrtindex_message_debug
  end interface
  !
  interface mrtindex_entry_rheader
    subroutine mrtindex_entry_rheader_byoptx(optx,ient,edesc,head,error)
      use gildas_def
      use gbl_message
      use mrtindex_vars
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ public-generic mrtindex_entry_rheader
      !  Read the header of the ient-th entry in the given index
      !  Also return the entry descriptor as a side product, which is
      ! useful in some cases
      !---------------------------------------------------------------------
      type(mrtindex_optimize_t),    intent(in)    :: optx    !
      integer(kind=entry_length), intent(in)    :: ient    ! Position in index
      type(classic_entrydesc_t),  intent(out)   :: edesc   !
      type(mrtindex_header_t),      intent(inout) :: head    !
      logical,                    intent(inout) :: error   !
    end subroutine mrtindex_entry_rheader_byoptx
    subroutine mrtindex_entry_rheader_byindx(filein,ient,indx,edesc,head,error)
      use gildas_def
      use gbl_message
      use mrtindex_vars
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ public-generic mrtindex_entry_rheader
      !  Read the header of the ient-th entry in the file + indx
      !  Also return the entry descriptor as a side product, which is
      ! useful in some cases
      !---------------------------------------------------------------------
      type(classic_file_t),       intent(in)    :: filein  !
      integer(kind=entry_length), intent(in)    :: ient    ! Position in file
      type(mrtindex_indx_t),        intent(in)    :: indx    ! Associated indx
      type(classic_entrydesc_t),  intent(out)   :: edesc   !
      type(mrtindex_header_t),      intent(inout) :: head    !
      logical,                    intent(inout) :: error   !
    end subroutine mrtindex_entry_rheader_byindx
  end interface mrtindex_entry_rheader
  !
  interface
    subroutine mrtindex_update_command(entry,fileid,error)
      use gbl_message
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ public
      ! Support routine for command MUPDATE
      !---------------------------------------------------------------------
      type(mrtindex_entry_t), intent(inout) :: entry
      integer(kind=4),        intent(in)    :: fileid
      logical,                intent(inout) :: error
    end subroutine mrtindex_update_command
  end interface
  !
  interface
    subroutine mrtindex_code2sic(error)
      use gbl_message
      use mrtindex_parameters
      !---------------------------------------------------------------------
      ! @ public
      ! Create the MRT% Sic structure mapping the index codes.
      !---------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine mrtindex_code2sic
  end interface
  !
  interface
    subroutine mrtindex_optimize_to_entry(optx,ient,entry,error)
      use classic_api
      use mrtindex_types
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ public
      !  "GET" the full entry (header+sections), read from disk
      !---------------------------------------------------------------------
      type(mrtindex_optimize_t),    intent(in)    :: optx   !
      integer(kind=entry_length), intent(in)    :: ient   !
      type(mrtindex_entry_t),       intent(inout) :: entry  !
      logical,                    intent(inout) :: error  !
    end subroutine mrtindex_optimize_to_entry
  end interface
  !
  interface
    subroutine mrtindex_optimize_to_filename(optx,ient,filename,error)
      use mrtindex_types
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ public
      !---------------------------------------------------------------------
      type(mrtindex_optimize_t),    intent(in)    :: optx
      integer(kind=entry_length), intent(in)    :: ient
      character(len=*),           intent(out)   :: filename
      logical,                    intent(inout) :: error
    end subroutine mrtindex_optimize_to_filename
  end interface
  !
  interface
    subroutine mrtindex_entry_zscience(sci,error)
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ public
      !  Zero-ify an 'sec_science_t' structure
      !---------------------------------------------------------------------
      type(sec_science_t), intent(out)   :: sci
      logical,             intent(inout) :: error
    end subroutine mrtindex_entry_zscience
  end interface
  !
  interface
    subroutine mrtindex_entry_zheader(head,error)
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ public
      !  Zero-ify an 'mrtindex_header_t' structure
      !---------------------------------------------------------------------
      type(mrtindex_header_t), intent(inout) :: head
      logical,               intent(inout) :: error
    end subroutine mrtindex_entry_zheader
  end interface
  !
  interface
    subroutine mrtindex_entry_fheader(head,error)
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ public
      !  Free an 'mrtindex_header_t' structure
      !---------------------------------------------------------------------
      type(mrtindex_header_t), intent(inout) :: head
      logical,               intent(inout) :: error
    end subroutine mrtindex_entry_fheader
  end interface
  !
  interface
    subroutine mrtindex_entry_free(entry,error)
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ public
      !  Free an 'mrtindex_entry_t' structure
      !---------------------------------------------------------------------
      type(mrtindex_entry_t), intent(inout) :: entry
      logical,              intent(inout) :: error
    end subroutine mrtindex_entry_free
  end interface
  !
  interface
    subroutine mrtindex_entry_lcalib(head,string,error)
      use gildas_def
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ public
      !  List the calibration section, i.e. produce a string suited for
      ! MLIST
      !---------------------------------------------------------------------
      type(mrtindex_header_t), intent(in)    :: head
      character(len=*),      intent(out)   :: string
      logical,               intent(inout) :: error
    end subroutine mrtindex_entry_lcalib
  end interface
  !
  interface
    subroutine mrtindex_entry_lscience(head,string,nc,error)
      use gildas_def
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ public
      !  List the science section, i.e. produce a string suited for
      ! MLIST
      !---------------------------------------------------------------------
      type(mrtindex_header_t), intent(in)    :: head
      character(len=*),      intent(out)   :: string
      integer(kind=4),       intent(out)   :: nc
      logical,               intent(inout) :: error
    end subroutine mrtindex_entry_lscience
  end interface
  !
  interface
    subroutine mrtindex_append_entry(entry,ient,oent,ix,error)
      use gbl_message
      use classic_api
      use mrtindex_types
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ public
      !  Append a NEW entry, with a new version number (automatically set
      ! to a new available value) in the file (on disk) and in IX (in
      ! memory). CX is NOT updated (this is a feature)
      !---------------------------------------------------------------------
      type(mrtindex_entry_t),     intent(inout) :: entry  !
      integer(kind=entry_length), intent(in)    :: ient   ! Old entry position in IX
      integer(kind=entry_length), intent(out)   :: oent   ! New entry position in IX
      type(mrtindex_optimize_t),  intent(inout) :: ix
      logical,                    intent(inout) :: error  ! Logical error flag
    end subroutine mrtindex_append_entry
  end interface
  !
  interface
    subroutine mrtindex_update_entry(entry,ient,ix,error)
      use gbl_message
      use mrtindex_types
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ public
      !  Update an old entry in the file (on disk) and in IX (in memory).
      ! CX is NOT updated, this is a feature.
      !---------------------------------------------------------------------
      type(mrtindex_entry_t),     intent(inout) :: entry  !
      integer(kind=entry_length), intent(in)    :: ient   ! Entry position in IX
      type(mrtindex_optimize_t),  intent(inout) :: ix
      logical,                    intent(inout) :: error  ! Logical error flag
    end subroutine mrtindex_update_entry
  end interface
  !
  interface
    subroutine mrtindex_extend_entry(entry,ient,ix,error)
      use gbl_message
      use mrtindex_types
      use mrtindex_vars
      !---------------------------------------------------------------------
      ! @ public
      !  Extend the LAST entry in the file (on disk, in particular, add the
      ! new sections) and update in IX (in memory).
      !  CX is NOT updated, this is a feature.
      !---------------------------------------------------------------------
      type(mrtindex_entry_t),     intent(inout) :: entry  !
      integer(kind=entry_length), intent(in)    :: ient   ! Entry position in IX
      type(mrtindex_optimize_t),  intent(inout) :: ix
      logical,                    intent(inout) :: error  ! Logical error flag
    end subroutine mrtindex_extend_entry
  end interface
  !
  interface
    subroutine mrtindex_numver2ent(rname,ix,num,ver,ient,dupl,error)
      use gbl_message
      use classic_api
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ public
      !  Convert an observation number and version to the entry position
      ! in IX. ver<0 means to choose the newest version.
      !---------------------------------------------------------------------
      character(len=*),           intent(in)    :: rname  ! Calling routine name
      type(mrtindex_optimize_t),  intent(in)    :: ix
      integer(kind=entry_length), intent(in)    :: num    ! Observation number
      integer(kind=4),            intent(in)    :: ver    ! Version number
      integer(kind=entry_length), intent(out)   :: ient   ! Entry found
      logical,                    intent(inout) :: dupl   ! Error because of multiple match?
      logical,                    intent(inout) :: error  ! Logical error flag
    end subroutine mrtindex_numver2ent
  end interface
  !
  interface
    function mrtindex_telescope(itel)
      use mrtindex_parameters
      !---------------------------------------------------------------------
      ! @ public
      ! Return the telescope name given its identifier.
      ! ---
      ! Note that sharing character arrays between libraries is not
      ! portable, this is why a function is used.
      !---------------------------------------------------------------------
      character(len=telescope_length) :: mrtindex_telescope
      integer(kind=4), intent(in) :: itel
    end function mrtindex_telescope
  end interface
  !
  interface
    function mrtindex_backend(iback)
      use mrtindex_parameters
      !---------------------------------------------------------------------
      ! @ public
      ! Return the backend name given its identifier.
      ! ---
      ! Note that sharing character arrays between libraries is not
      ! portable, this is why a function is used.
      !---------------------------------------------------------------------
      character(len=backname_length) :: mrtindex_backend
      integer(kind=4), intent(in) :: iback
    end function mrtindex_backend
  end interface
  !
  interface
    function mrtindex_obstype_decode(name,error)
      use gbl_message
      use mrtindex_parameters
      !---------------------------------------------------------------------
      ! @ public
      ! Decode the IMBFIST obstype name and return the corresponding
      ! MRTCAL code
      !---------------------------------------------------------------------
      integer(kind=4) :: mrtindex_obstype_decode
      character(len=*), intent(in)    :: name
      logical,          intent(inout) :: error
    end function mrtindex_obstype_decode
  end interface
  !
  interface
    function mrtindex_obstype_imbfits(itype)
      use mrtindex_parameters
      !---------------------------------------------------------------------
      ! @ public
      ! Return the observation type name (IMBFITS) given its identifier.
      ! ---
      ! Note that sharing character arrays between libraries is not
      ! portable, this is why a function is used.
      !---------------------------------------------------------------------
      character(len=obstype_length) :: mrtindex_obstype_imbfits
      integer(kind=4), intent(in) :: itype
    end function mrtindex_obstype_imbfits
  end interface
  !
  interface
    function mrtindex_obstype(itype)
      use mrtindex_parameters
      !---------------------------------------------------------------------
      ! @ public
      ! Return the observation type name (MRTCAL) given its identifier.
      ! ---
      ! Note that sharing character arrays between libraries is not
      ! portable, this is why a function is used.
      !---------------------------------------------------------------------
      character(len=obstype_length) :: mrtindex_obstype
      integer(kind=4), intent(in) :: itype
    end function mrtindex_obstype
  end interface
  !
  interface
    function mrtindex_swmode_imbfits(imode)
      use mrtindex_parameters
      !---------------------------------------------------------------------
      ! @ public
      ! Return the switching mode name given its identifier.
      ! ---
      ! Note that sharing character arrays between libraries is not
      ! portable, this is why a function is used.
      !---------------------------------------------------------------------
      character(len=switchmode_length) :: mrtindex_swmode_imbfits
      integer(kind=4), intent(in) :: imode
    end function mrtindex_swmode_imbfits
  end interface
  !
  interface
    function mrtindex_swmode(imode)
      use mrtindex_parameters
      !---------------------------------------------------------------------
      ! @ public
      ! Return the switching mode name given its identifier.
      ! ---
      ! Note that sharing character arrays between libraries is not
      ! portable, this is why a function is used.
      !---------------------------------------------------------------------
      character(len=switchmode_length) :: mrtindex_swmode
      integer(kind=4), intent(in) :: imode
    end function mrtindex_swmode
  end interface
  !
  interface
    function mrtindex_swmode_voxml(imode)
      use mrtindex_parameters
      !---------------------------------------------------------------------
      ! @ public
      ! Return the switching mode name given its identifier.
      ! ---
      ! Note that sharing character arrays between libraries is not
      ! portable, this is why a function is used.
      !---------------------------------------------------------------------
      character(len=switchmode_length) :: mrtindex_swmode_voxml
      integer(kind=4), intent(in) :: imode
    end function mrtindex_swmode_voxml
  end interface
  !
  interface mrtindex_optimize_to_optimize
    subroutine mrtindex_optimize_to_optimize_1d(in,out,error)
      use gbl_message
      use classic_api
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ public-generic mrtindex_optimize_to_optimize
      !---------------------------------------------------------------------
      type(mrtindex_optimize_t), intent(in)    :: in
      type(mrtindex_optimize_t), intent(inout) :: out
      logical,                 intent(inout) :: error
    end subroutine mrtindex_optimize_to_optimize_1d
    subroutine mrtindex_optimize_to_optimize_next(in,i,out,error)
      use classic_api
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ public-generic mrtindex_optimize_to_optimize
      ! Copy the i-th element of the input optimize to the next available
      ! position in output optimize. It does not check if the optimize
      ! index is correctly allocated.
      !---------------------------------------------------------------------
      type(mrtindex_optimize_t),    intent(in)    :: in
      integer(kind=entry_length), intent(in)    :: i
      type(mrtindex_optimize_t),    intent(inout) :: out
      logical,                    intent(inout) :: error
    end subroutine mrtindex_optimize_to_optimize_next
    subroutine mrtindex_optimize_to_optimize_inplace(in,i,out,j,error)
      use classic_api
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ public-generic mrtindex_optimize_to_optimize
      ! Copy the i-th element of the input optimize to the j-th position
      ! in output optimize. It does not check if the optimize index is
      ! correctly allocated.
      !---------------------------------------------------------------------
      type(mrtindex_optimize_t),    intent(in)    :: in
      integer(kind=entry_length), intent(in)    :: i
      type(mrtindex_optimize_t),    intent(inout) :: out
      integer(kind=entry_length), intent(in)    :: j
      logical,                    intent(inout) :: error
    end subroutine mrtindex_optimize_to_optimize_inplace
  end interface mrtindex_optimize_to_optimize
  !
  interface
    subroutine mrtindex_variable_entry(struct,head,ro,error)
      use mrtindex_types
      !---------------------------------------------------------------------
      ! @ public
      !  Map a Sic structure on an entry
      !---------------------------------------------------------------------
      character(len=*),      intent(in)    :: struct  ! Structure name
      type(mrtindex_header_t), intent(in)    :: head    !
      logical,               intent(in)    :: ro      ! Read-Only?
      logical,               intent(inout) :: error   ! Logical error flag
    end subroutine mrtindex_variable_entry
  end interface
  !
end module mrtindex_interfaces_public
