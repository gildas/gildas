!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtindex_index_write(fileout,dire,list,nlist,error)
  use gbl_message
  use gkernel_types
  use gkernel_interfaces
  use classic_api
  use mrtindex_interfaces, except_this=>mrtindex_index_write
  use mrtindex_types
  !---------------------------------------------------------------------
  ! @ private
  ! Write all the new entries in the index
  !---------------------------------------------------------------------
  type(classic_file_t), intent(inout) :: fileout      !
  character(len=*),     intent(in)    :: dire         ! Working directory
  integer(kind=4),      intent(in)    :: nlist        !
  character(len=*),     intent(in)    :: list(nlist)  ! File list
  logical,              intent(inout) :: error        ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='INDEX>WRITE'
  type(mrtindex_entry_t) :: entry
  integer(kind=entry_length) :: ient,nold,nnew,nbad,nemp,ninc,nupd,ifound,istart
  type(time_t) :: time
  character(len=message_length) :: mess,extra
  integer(kind=4) :: nc,idir
  integer(kind=4) :: sort(nlist)  ! Automatic array
  type(mrtindex_optimize_t) :: ox
  character(len=10) :: action
  !
  ! First, load the output index (entries already in file). No problem
  ! if file is empty and/or new.
  idir = 0  ! Not used for writing
  ox%next = 1  ! No previous entry
  call mrtindex_index_read(fileout,idir,ox,error)
  if (error)  return
  nold = ox%next-1
  !
  ! Sort the list of files
  call mrtindex_index_sort(nlist,list,sort,error)
  if (error)  goto 100
  !
  call gtime_init(time,nlist,error)
  if (error)  goto 100
  !
  nnew = 0
  nupd = 0
  nbad = 0
  nemp = 0
  ninc = 0
  istart = 1
  entry%head%presec(:) = .false.           ! All sections disabled...
  entry%head%presec(sec_prim_id) = .true.  ! ... except Primary
  do ient=1,nlist
    call gtime_current(time)
    !
    ! Nullify everything to avoid uninitialized or remnant values from
    ! one call to another.
    call mrtindex_entry_zindx(entry%indx,error)
    if (error)  return
    call mrtindex_entry_zprim(entry%head%pri,error)
    if (error)  return
    !
    ! Partial filling of the 'indx'
    entry%indx%filename = list(sort(ient))
    call mrtindex_index_decode(entry%indx%filename,entry%indx%telescope,  &
      entry%indx%backend,entry%indx%dobs,entry%indx%scan,error)
    if (error)  return
    !
    call mrtindex_index_exists(ox,entry%indx,istart,ifound)
    if (ifound.eq.0) then
      ! Index a new IMBFITS file
      entry%indx%version = 1
    else
      ! If IMBFITS file was not modified since last update, nothing to do
      if (mrtindex_index_modified(trim(dire)//'/'//entry%indx%filename,ox%itime(ifound),error)) then
        cycle
      endif
      entry%indx%version = ox%version(ifound)  ! File has changed but keep its version
    endif
    !
    ! Fill the (remaining part of) indx and primary section:
    call mrtindex_entry_fill(dire,entry%indx%filename,entry%indx,entry%head%pri,error)
    if (entry%indx%filstatus.eq.complete_unreadable) then
      nbad = nbad+1
      if (error) then
        call mrtindex_message(seve%w,rname,'Error in file '//list(sort(ient)))
        write(*,*)
        error = .false.  ! Error recovered in this case (only)
      endif
    elseif (entry%indx%filstatus.eq.complete_empty) then
      nemp = nemp+1
    elseif (entry%indx%filstatus.eq.complete_partial) then
      ninc = ninc+1
    endif
    if (error)  goto 100
    !
    if (ifound.ne.0) then
      if (ox%filstatus(ifound).gt.entry%indx%filstatus) then
        call mrtindex_message(seve%e,rname,'File readability has deteriorated, '//  &
          'you must recompute the whole index (INDEX BUILD)')
        error = .true.
        goto 100
      elseif (ox%filstatus(ifound).eq.entry%indx%filstatus) then
        ! File readability unchanged: nothing to do
        cycle
      else
        ! File readability has improved: update in index file
        continue
      endif
    endif
    !
    if (ifound.ne.0) then
      ! Update an old entry
      nupd = nupd+1
      ! Technical informations (Classic Data Container)
      entry%indx%bloc = ox%bloc(ifound)
      entry%indx%word = ox%word(ifound)
      !
      call mrtindex_entry_update(fileout,entry,error)
      if (error) then
        write(mess,'(A,I0,A,A)')  &
          'Could not update entry #',entry%desc%xnum,' in file ',fileout%spec
        call mrtindex_message(seve%e,rname,mess)
        goto 100
      endif
    else
      ! Add a new entry
      nnew = nnew+1
      ! Technical informations (Classic Data Container)
      entry%indx%bloc = fileout%desc%nextrec
      entry%indx%word = fileout%desc%nextword
      !
      call mrtindex_entry_write(fileout,entry,error)
      if (error) then
        write(mess,'(A,I0,A,A)')  &
          'Could not add entry #',fileout%desc%xnext,' in file ',fileout%spec
        call mrtindex_message(seve%e,rname,mess)
        goto 100
      endif
    endif
    !
    if (sic_ctrlc()) then
      call mrtindex_message(seve%e,rname,'CTRL-C, incomplete indexing')
      error = .true.
      exit
    endif
  enddo
  !
  ! Update the File Descriptor (in particular desc%xnext). Avoid modifying
  ! the file (and its date on disk) if nothing new
  if (nnew.gt.0 .or. nupd.gt.0) then
    call classic_filedesc_write(fileout,error)
    if (error)  goto 100
    call classic_file_fflush(fileout,error)
    if (error)  return
  endif
  !
  if (nnew.eq.0 .and. nupd.eq.0) then
    action = 'Unmodified'
  elseif (nold.gt.0) then
    action = 'Updated'
  else
    action = 'Created'
  endif
  write(mess,'(A,A,A,A,I0,A)')  &
    trim(action),' file ',trim(fileout%spec),' indexing ',fileout%desc%xnext-1,' files'
  !
  extra = ' '
  if (nold.gt.0 .and. nnew.ne.0) then
    nc = len_trim(extra)
    write(extra(nc+1:),'(1X,I0,A)') nnew,' new,'
  endif
  if (nupd.ne.0) then
    nc = len_trim(extra)
    write(extra(nc+1:),'(1X,I0,A)') nupd,' updated,'
  endif
  if (nbad.ne.0) then
    nc = len_trim(extra)
    write(extra(nc+1:),'(1X,I0,A)') nbad,' unreadable,'
  endif
  if (nemp.ne.0) then
    nc = len_trim(extra)
    write(extra(nc+1:),'(1X,I0,A)') nemp,' empty,'
  endif
  if (ninc.ne.0) then
    nc = len_trim(extra)
    write(extra(nc+1:),'(1X,I0,A)') ninc,' incomplete,'
  endif
  !
  if (extra.ne.'') then
    nc = len_trim(extra)-1
    call mrtindex_message(seve%i,rname,trim(mess)//' (including'//extra(1:nc)//')')
  else
    call mrtindex_message(seve%i,rname,mess)
  endif
  !
100 continue
  call deallocate_mrtoptimize(ox,error)
  if (error)  continue
  !
  call mrtindex_entry_free(entry,error)
  if (error)  continue
  !
end subroutine mrtindex_index_write
!
subroutine mrtindex_index_exists(optx,indx,istart,ifound)
  use classic_api
  use mrtindex_types
  !---------------------------------------------------------------------
  ! @ private
  !   Check if input index is already present in optimize list of
  ! entries. Uniqueness is ensured with the date+scan+backend.
  !   Return its existing entry number, if any.
  !---------------------------------------------------------------------
  type(mrtindex_optimize_t),    intent(in)    :: optx    !
  type(mrtindex_indx_t),        intent(in)    :: indx    !
  integer(kind=entry_length), intent(inout) :: istart  !
  integer(kind=entry_length), intent(out)   :: ifound  !
  ! Local
  integer(kind=entry_length) :: ient
  !
  ifound = 0  ! Some dummy value
  !
  ! Split the loop in two, start from where we stopped last time
  do ient=istart,optx%next-1
    if (indx%dobs.ne.optx%dobs(ient))  cycle
    if (indx%scan.ne.optx%scan(ient))  cycle
    if (indx%backend.ne.optx%backend(ient))  cycle
    ifound = ient
    istart = ient+1  ! Next time we will restart from here
    return
  enddo
  !
  do ient=1,istart-1
    if (indx%dobs.ne.optx%dobs(ient))  cycle
    if (indx%scan.ne.optx%scan(ient))  cycle
    if (indx%backend.ne.optx%backend(ient))  cycle
    ifound = ient
    istart = ient+1  ! Next time we will restart from here
    return
  enddo
  !
end subroutine mrtindex_index_exists
!
subroutine mrtindex_index_sort(nfile,file,sort,error)
  use gkernel_interfaces
  use mrtindex_interfaces, except_this=>mrtindex_index_sort
  !---------------------------------------------------------------------
  ! @ private
  !  Sort the input list of file names
  !---------------------------------------------------------------------
  integer(kind=4),  intent(in)    :: nfile        ! Number of files
  character(len=*), intent(in)    :: file(nfile)  ! File names
  integer(kind=4),  intent(out)   :: sort(nfile)  ! Sorted indexes
  logical,          intent(inout) :: error        ! Logical error flag
  ! Local
  integer(kind=4) :: ifile,telescope
  ! Allocate automatic arrays:
  integer(kind=4) :: backend(nfile)
  integer(kind=4) :: dobs(nfile)
  integer(kind=4) :: scan(nfile)
  !
  ! Fill the arrays used for sorting:
  do ifile=1,nfile
    sort(ifile) = ifile
    call mrtindex_index_decode(file(ifile),telescope,backend(ifile),  &
      dobs(ifile),scan(ifile),error)
    if (error)  return
  enddo
  !
  call gi0_quicksort_index_with_user_gtge(sort,nfile,  &
    index_sort_gt,index_sort_ge,error)
  if (error)  return
  !
contains
  !
  ! Sorting functions: first by DOBS, then by SCAN (should be similar to
  ! UT, but we avoid reading the Primary HDU here), and then by BACKEND
  ! for cosmetics.
  ! Use local functions which know the arrays (this avoids making them
  ! global variables).
  function index_sort_gt(m,l)
    logical :: index_sort_gt
    integer(kind=4), intent(in) :: m,l
    if (dobs(m).ne.dobs(l)) then
      index_sort_gt = dobs(m).gt.dobs(l)
      return
    endif
    if (scan(m).ne.scan(l)) then
      index_sort_gt = scan(m).gt.scan(l)
      return
    endif
    index_sort_gt = backend(m).gt.backend(l)
  end function index_sort_gt
  !
  function index_sort_ge(m,l)
    logical :: index_sort_ge
    integer(kind=4), intent(in) :: m,l
    if (dobs(m).ne.dobs(l)) then
      index_sort_ge = dobs(m).ge.dobs(l)
      return
    endif
    if (scan(m).ne.scan(l)) then
      index_sort_ge = scan(m).ge.scan(l)
      return
    endif
    index_sort_ge = backend(m).ge.backend(l)
  end function index_sort_ge
  !
end subroutine mrtindex_index_sort
!
function mrtindex_index_modified(filename,time,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! @ private
  !  Returns .true. if file was modified since the input Unix time
  ! (01-jan-1970) given in nanoseconds (integer*8)
  !---------------------------------------------------------------------
  logical :: mrtindex_index_modified  ! Function value on return
  character(len=*), intent(in)    :: filename  ! Path included
  integer(kind=8),  intent(in)    :: time      ! In nanoseconds
  logical,          intent(inout) :: error     ! Logical error flag
  ! Local
  integer(kind=4) :: ier
  integer(kind=8) :: nsec
  !
  ier = gag_mtime(filename,nsec)
  if (ier.ne.0)  error = .true.
  mrtindex_index_modified = time.gt.nsec
  !
end function mrtindex_index_modified
!
subroutine mrtindex_entry_write(fileout,entry,error)
  use classic_api
  use mrtindex_interfaces, except_this=>mrtindex_entry_write
  use mrtindex_types
  use mrtindex_vars
  !---------------------------------------------------------------------
  ! @ private
  ! Write one new entry in the output file
  !---------------------------------------------------------------------
  type(classic_file_t),  intent(inout) :: fileout  !
  type(mrtindex_entry_t),  intent(inout) :: entry    !
  logical,               intent(inout) :: error    ! Logical error flag
  ! Local
  integer(kind=entry_length) :: entry_num
  logical :: full
  !
  ! Initialize the 'entry%desc' structure (entry descriptor)
  entry_num = fileout%desc%xnext
  call classic_entry_init(fileout,entry_num,mrtcal_maxsec,1,full,entry%desc,error)
  if (error)  return
  ! This allocates a new extension if needed and thus updates
  ! file%desc%nextrec and file%desc%nextword. Thus the position of this
  ! new entry position may have to be updated:
  entry%indx%bloc = fileout%desc%nextrec
  entry%indx%word = fileout%desc%nextword
  !
  call classic_recordbuf_open(fileout,entry%indx%bloc,entry%indx%word,obufobs,error)
  if (error)  return
  !
  if (entry%head%presec(sec_prim_id)) then
    call mrtindex_entry_wprim(fileout,entry%desc,entry%head%pri,.true.,error)
    if (error)  return
  endif
  !
  if (entry%head%presec(sec_calib_id)) then
    call mrtindex_entry_wcalib(fileout,entry%desc,entry%head%cal,.true.,error)
    if (error)  return
  endif
  !
  if (entry%head%presec(sec_science_id)) then
    call mrtindex_entry_wscience(fileout,entry%desc,entry%head%sci,.true.,error)
    if (error)  return
  endif
  !
  call mrtindex_entry_windx(fileout,entry_num,entry%indx,error)
  if (error)  return
  fileout%desc%xnext = fileout%desc%xnext+1
  !
  ! Close the "observation" = write the entry descriptor. Must be
  ! last since it contains the observation description e.g. number
  ! of blocks.
  call classic_entrydesc_write(fileout,obufobs,entry%desc,error)
  if (error)  return
  !
  call classic_entry_close(fileout,obufobs,error)
  if (error) return
  !
end subroutine mrtindex_entry_write
!
subroutine mrtindex_entry_update(fileout,entry,error)
  use classic_api
  use mrtindex_interfaces, except_this=>mrtindex_entry_update
  use mrtindex_types
  use mrtindex_vars
  !---------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  ! Update one old entry in the output file
  !---------------------------------------------------------------------
  type(classic_file_t), intent(inout) :: fileout  !
  type(mrtindex_entry_t), intent(inout) :: entry    !
  logical,              intent(inout) :: error    ! Logical error flag
  !
  call classic_recordbuf_open(fileout,entry%indx%bloc,entry%indx%word,obufobs,error)
  if (error)  return
  !
  ! Read the old entry descriptor
  call classic_entrydesc_read(fileout,obufobs,entry%desc,error)
  if (error)  return
  !
  ! ZZZ Factorize this?
  if (entry%indx%filstatus.ne.complete_unreadable .and.  &
      entry%head%presec(sec_prim_id)) then
    call mrtindex_entry_wprim(fileout,entry%desc,entry%head%pri,.false.,error)
    if (error)  return
  endif
  !
  if (entry%head%presec(sec_calib_id)) then
    call mrtindex_entry_wcalib(fileout,entry%desc,entry%head%cal,.false.,error)
    if (error)  return
  endif
  !
  if (entry%head%presec(sec_science_id)) then
    call mrtindex_entry_wscience(fileout,entry%desc,entry%head%sci,.false.,error)
    if (error)  return
  endif
  !
  call mrtindex_entry_windx(fileout,entry%desc%xnum,entry%indx,error)
  if (error)  return
  !
  ! Close the "observation" = write the entry descriptor. Must be
  ! last since it contains the observation description e.g. number
  ! of blocks.
  call classic_entrydesc_write(fileout,obufobs,entry%desc,error)
  if (error)  return
  !
  call classic_entry_close(fileout,obufobs,error)
  if (error) return
  !
end subroutine mrtindex_entry_update
!
subroutine mrtindex_entry_extend(fileout,entry,error)
  use gbl_message
  use classic_api
  use mrtindex_interfaces, except_this=>mrtindex_entry_extend
  use mrtindex_types
  use mrtindex_vars
  !---------------------------------------------------------------------
  ! @ private-mandatory (because symbol is used elsewhere)
  ! Extend the LAST entry of the output file (in particular, update the
  ! old sections and add the new ones)
  !---------------------------------------------------------------------
  type(classic_file_t), intent(inout) :: fileout  !
  type(mrtindex_entry_t), intent(inout) :: entry    !
  logical,              intent(inout) :: error    ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='ENTRY>EXTEND'
  logical :: found
  integer(kind=4) :: isec
  character(len=message_length) :: mess
  !
  call classic_recordbuf_open(fileout,entry%indx%bloc,entry%indx%word,obufobs,error)
  if (error)  return
  !
  ! Read the old entry descriptor
  call classic_entrydesc_read(fileout,obufobs,entry%desc,error)
  if (error)  return
  ! Patch: msec (size of sections descriptor arrays) is NOT in data (above
  ! subroutine returned 0), but we know the entries were created by
  ! mrtindex_entry_write with the following value. We need a correct value
  ! because we are adding a new section here.
  entry%desc%msec = mrtcal_maxsec
  !
  ! Sanity check
  if (entry%desc%xnum.ne.fileout%desc%xnext-1) then
    write(mess,'(A,I0,A,I0,A)')  'Only the last entry can be extended (got ',  &
      entry%desc%xnum,', last is ',fileout%desc%xnext-1,')'
    call mrtindex_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  ! ZZZ Factorize this?
  if (entry%head%presec(sec_prim_id)) then
    ! If this section already exists on disk, update it. Else, add it.
    call classic_entrydesc_secfind_one(entry%desc,sec_prim_id,found,isec)
    call mrtindex_entry_wprim(fileout,entry%desc,entry%head%pri,.not.found,error)
    if (error)  return
  endif
  !
  if (entry%head%presec(sec_calib_id)) then
    ! If this section already exists on disk, update it. Else, add it.
    call classic_entrydesc_secfind_one(entry%desc,sec_calib_id,found,isec)
    call mrtindex_entry_wcalib(fileout,entry%desc,entry%head%cal,.not.found,error)
    if (error)  return
  endif
  !
  if (entry%head%presec(sec_science_id)) then
    ! If this section already exists on disk, update it. Else, add it.
    call classic_entrydesc_secfind_one(entry%desc,sec_science_id,found,isec)
    call mrtindex_entry_wscience(fileout,entry%desc,entry%head%sci,.not.found,error)
    if (error)  return
  endif
  !
  call mrtindex_entry_windx(fileout,entry%desc%xnum,entry%indx,error)
  if (error)  return
  !
  ! Close the "observation" = write the entry descriptor. Must be
  ! last since it contains the observation description e.g. number
  ! of blocks.
  call classic_entrydesc_write(fileout,obufobs,entry%desc,error)
  if (error)  return
  !
  call classic_entry_close(fileout,obufobs,error)
  if (error) return
  !
end subroutine mrtindex_entry_extend
!
subroutine mrtindex_entry_fill(dire,filename,indx,prim,error)
  use gbl_constant
  use gbl_message
  use phys_const
  use classic_api
  use imbfits_parameters
  use mrtindex_dependencies_interfaces
  use mrtindex_interfaces, except_this=>mrtindex_entry_fill
  use mrtindex_types
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  ! Fill the types by reading the file Primary header
  !---------------------------------------------------------------------
  character(len=*),    intent(in)    :: dire
  character(len=*),    intent(in)    :: filename
  type(mrtindex_indx_t), intent(inout) :: indx
  type(sec_primary_t), intent(inout) :: prim
  logical,             intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='INDEX>FILL'
  type(imbfits_t) :: imbf
  integer(kind=4) :: ier,dobs,ifront,it(mrtcal_indx_mfrontend)
  integer(kind=8) :: fsize
  character(len=telescope_length) :: teles
  character(len=switchmode_length) :: swin,swfound
  !
  ! --- 1 Safe filling -------------------------------------------------
  ! Fill as soon as possible the elements we already know, and/or with
  ! more or less dummy values. They will be correctly set hereafter if
  ! reading the IMBFITS is successful.
  !
  indx%filename   = filename
  indx%filstatus  = complete_unreadable  ! As long as the end is not reached
  !
  ! Already set out of this subroutine:
  ! indx%filename
  ! indx%telescope
  ! indx%backend
  ! indx%dobs
  ! indx%scan
  !
  ! Set last indexing time *before* reading the IMB-FITS. This is
  ! conservative so that if modifications occur between end of reading and
  ! setting the values, we will catch them at next update.
  ier = gag_time(indx%itime)
  if (ier.ne.0) then
    call mrtindex_message(seve%e,rname,'Error getting current time')
    error = .true.
    goto 100
  endif
  !
  ! --- 2 Read IMBFITS -------------------------------------------------
  !
  ! If null-size (scan aborted early), skip...
  ier = gag_filsize(trim(dire)//'/'//filename,fsize)
  if (fsize.eq.0) then
    indx%filstatus = complete_unreadable
    ! Not an error
    goto 100
  endif
  !
  ! ZZZ probably need a reopen if the same file is already
  ! opened elsewhere in mrtcal
  ! ZZZ This is very similar to 'imbfits_read_header'
  call imbfits_open_file(trim(dire)//'/'//filename,imbf%file,imbf%hdus,error)
  if (error)  goto 100
  ! Read leading HDUs (we need Primary, Scan, Frontend, and Backend)
  call imbfits_read_leadhdus(imbf,0.0,error)
  if (error)  goto 100  ! Close file in case of error?
  !
  ! --- 2.a From PRIMARY HDU ---
  ! These ones need no particular check, set them asap
  prim%imbfvers = imbf%primary%imbftsve%val
!   prim%timegoal = imbf%primary%exptime%val
!   prim%nsubgoal = imbf%primary%n_obs%val
!   prim%nsub     = imbf%primary%n_obsp%val
  !
  call sic_ambigs(rname,imbf%primary%telescop%val,teles,indx%telescope,  &
    telescopes(1:),ntelescopes,error)
  if (error)  goto 100
  indx%projid    = imbf%primary%projid%val
  indx%source    = imbf%primary%object%val
  call sic_upper(indx%source)
  !
  ! DOBS and UT, take care of the midnigth problem
  call gag_mjd2gagut(imbf%primary%mjd_obs%val,dobs,indx%ut,error)
  if (error)  goto 100
  if (dobs.ne.indx%dobs) then  ! Compare with value already set from filename
    ! Tolerate 1h forward on next day (1h = max scan length)
    if (dobs.eq.indx%dobs+1 .and. indx%ut.lt.pi/12.d0) then
      ! Keep DOBS ad found in file name, set UT beyond 24h
      indx%ut = indx%ut+2.d0*pi
    else
      call mrtindex_message(seve%e,rname,  &
        'Inconsistent observation dates in filename and in MJD-OBS in Primary header')
      error = .true.
      goto 100
    endif
  endif
  indx%lst       = imbf%primary%lst%val
  !
  ! Decode obstype
  indx%obstype = mrtindex_obstype_decode(imbf%primary%obstype%val,error)
  if (error)  return
  !
  ! --- 2.b From SCAN HDU ---
  !
  ! Kind of coordinates. Note that we do not check for the suffix (e.g. -SFL)
  ! as it is not used (it does not apply to the offsets in the antenna
  ! table). See mrtcal documentation for details.
  if (imbf%scan%head%ctype1%val(1:2).eq.'RA' .and.  &
      imbf%scan%head%ctype2%val(1:3).eq.'DEC') then
    indx%system = type_eq
    !
  elseif (imbf%scan%head%ctype1%val(1:4).eq.'GLON' .and.  &
          imbf%scan%head%ctype2%val(1:4).eq.'GLAT') then
    indx%system = type_ga
    !
  else
    call mrtindex_message(seve%w,rname,'Kind of coordinates not understood: '//  &
      trim(imbf%scan%head%ctype1%val)//' '//trim(imbf%scan%head%ctype2%val))
    indx%system = type_un
  endif
  !
  indx%equinox = imbf%scan%head%equinox%val
  indx%lon = imbf%scan%head%longobj%val*rad_per_deg
  indx%lat = imbf%scan%head%latobj%val*rad_per_deg
  !
  ! Decode switchmode
  swin = imbf%scan%head%swtchmod%val
  call sic_upper(swin)
  call sic_ambigs_sub(rname,swin,swfound,indx%switchmode,switchmodes_imbfits,  &
    nswitchmodes,error)
  if (indx%switchmode.eq.0) then
    call mrtindex_message(seve%w,rname,'Switching mode '''//trim(swin)//  &
      ''' not supported (file '//trim(filename)//')')
    ! Not an error, continue
  endif
  !
  ! --- 2.c From FRONTEND HDU ---
  if (imbf%front%head%desc%naxis2%val.gt.mrtcal_indx_mfrontend) then
    call mrtindex_message(seve%e,rname,'Frontend table has too many rows, can not index')
    error = .true.
    return
  endif
  do ifront=1,imbf%front%head%desc%naxis2%val
    indx%frontend(ifront) = imbf%front%table%recname%val(ifront)
    call sic_upper(indx%frontend(ifront))
  enddo
  ! Sort by alphabetical order. This will ensure unique equivalence
  ! classes in MLIST /TOC even if some receiver names are permutted from
  ! one file to another.
  call gch_trie(indx%frontend,it,imbf%front%head%desc%naxis2%val,8,error)
  if (error)  return
  !
  ! --- 2.d From BACKEND HDU ---
  if (imbf%back%polchunks) then
    indx%polstatus = polstatus_yes
  else
    indx%polstatus = polstatus_no
  endif
  !
  ! --- 2.e From columns CAZIMUTH and CELEVATIO in 1st subscan ---
  call entry_fill_azel(imbf,indx%az,indx%el,error)
  if (error)  goto 100
  !
  ! --- 3 Finalize -----------------------------------------------------
  if (imbf%primary%n_obs%val*imbf%primary%n_obsp%val.eq.0) then
    indx%filstatus = complete_empty
  elseif (imbf%primary%n_obs%val.eq.imbf%primary%n_obsp%val) then
    indx%filstatus = complete_full
  else
    indx%filstatus = complete_partial
  endif
  !
100 continue
  if (error)  indx%filstatus = complete_unreadable
  !
  call imbfits_close_file(imbf%file,error)
  ! if (error)  continue
  call imbfits_free_leadhdus(imbf,error)
  ! if (error)  continue
  !
contains
  subroutine entry_fill_azel(imbf,az,el,error)
    use mrtindex_dependencies_interfaces
    !-------------------------------------------------------------------
    !  Return the Commanded Azimuth and Elevation of 1st trace in 1st
    ! subscan
    !-------------------------------------------------------------------
    type(imbfits_t), intent(inout) :: imbf
    real(kind=4),    intent(out)   :: az
    real(kind=4),    intent(out)   :: el
    logical,         intent(inout) :: error
    ! Local
    type(imbfits_antslow_table_t) :: anttable
    !
    if (imbf%primary%n_obsp%val.le.0) then
      ! No subscan, nowhere to find these values...
      call gag_notanum(az)
      call gag_notanum(el)
    else
      ! Go to 1st subscan
      call imbfits_mvhdu_subscan(1,hdu_antslow,imbf,error)
      if (error)  return
      ! Save time: do not read the header (number of lines), and just read 1 line
      call imbfits_read_column(rname,imbf%file,'CAZIMUTH',1,anttable%cazimuth,error)
      if (error)  return
      call imbfits_read_column(rname,imbf%file,'CELEVATIO',1,anttable%celevatio,error)
      if (error)  return
      az = anttable%cazimuth%val(1)
      el = anttable%celevatio%val(1)
      call imbfits_free_column(anttable%cazimuth,error)
      if (error)  return
      call imbfits_free_column(anttable%celevatio,error)
      if (error)  return
    endif
  end subroutine entry_fill_azel
end subroutine mrtindex_entry_fill
!
subroutine mrtindex_index_decode(filename,telescope,backcode,dobs,scan,error)
  use gbl_message
  use gkernel_interfaces
  use mrtindex_interfaces, except_this=>mrtindex_index_decode
  use mrtindex_parameters
  !---------------------------------------------------------------------
  ! @ private
  !  Decode:
  !  - telescope [code],
  !  - backend [code],
  !  - date [gag_date],
  !  - scan number
  ! from file name.
  !  Note that the date decoded from the file name is the SCAN date. It
  ! can differ from the OBSERVATION date (as found in the MJD in Primary
  ! header) by 1 if scan starts just before midnight and observation
  ! starts just after.
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: filename   !
  integer(kind=4),  intent(out)   :: telescope  !
  integer(kind=4),  intent(out)   :: backcode   !
  integer(kind=4),  intent(out)   :: dobs       !
  integer(kind=4),  intent(out)   :: scan       !
  logical,          intent(inout) :: error      ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='INDEX>DECODE'
  integer(kind=4) :: firstchar,lastchar,ier
  character(len=backname_length) :: name1,backname
  !
  lastchar = index(filename,'-')-1
  if (filename(1:lastchar).eq.'iram30m') then
    telescope = telescope_iram30m
  else
    telescope = telescope_unknown
  endif
  !
  ! Decode backend name from filename
  firstchar = lastchar+2
  lastchar = index(filename(firstchar:),'-')+firstchar-2
  name1 = filename(firstchar:lastchar)
  call sic_upper(name1)
  ! ZZZ sic_ambigs: not perfect since we would like to disambiguise
  ! full names (e.g. "ves" should be an error, not "vespa")
  call sic_ambigs_sub(rname,name1,backname,backcode,backends_imbfits,  &
    nbackends_imbfits,error)
  if (backcode.eq.0) then
    call mrtindex_message(seve%w,rname,'Backend '''//trim(name1)//  &
      ''' not supported (file '//trim(filename)//')')
    ! Not an error, continue
  elseif (backcode.eq.backend_continuum) then
    ! CONTINUUM => NBC
    backcode = backend_nbc
  endif
  !
  ! Decode date from filename
  firstchar = lastchar+2
  call gag_fromyyyymmdd(filename(firstchar:firstchar+7),dobs,error)
  if (error)  goto 100
  !
  ! Decode scan number
  firstchar = lastchar+11
  lastchar = index(filename,'-imb.fits')-1
  read(filename(firstchar:lastchar),'(I3)',iostat=ier) scan
  if (ier.ne.0) then
    call mrtindex_message(seve%e,rname,'Error decoding scan number')
    error = .true.
    goto 100
  endif
  !
100 continue
  if (error) &
    call mrtindex_message(seve%e,rname,  &
      'Error decoding file name '''//trim(filename)//'''')
  !
end subroutine mrtindex_index_decode
!
subroutine mrtindex_entry_wprim(fileout,edesc,prim,new,error)
  use gbl_message
  use gkernel_interfaces
  use classic_api
  use mrtindex_interfaces, except_this=>mrtindex_entry_wprim
  use mrtindex_types
  use mrtindex_vars
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(classic_file_t),      intent(inout) :: fileout  !
  type(classic_entrydesc_t), intent(inout) :: edesc    ! Entry Descriptor
  type(sec_primary_t),       intent(in)    :: prim     ! The section
  logical,                   intent(in)    :: new      ! New or old (update) section?
  logical,                   intent(inout) :: error    !
  ! Local
  character(len=*), parameter :: rname='INDEX>WPRIM'
  integer(kind=4) :: iwork(sec_prim_len)
  !
  call fileout%conv%writ%r4 (prim%imbfvers,iwork(1),1)
! call fileout%conv%writ%r8 (prim%lam,     iwork(),1)
! call fileout%conv%writ%r8 (prim%bet,     iwork(),1)
! call fileout%conv%writ%r8 (prim%timegoal,iwork(),1)
! call fileout%conv%writ%i4 (prim%nsubgoal,iwork(),1)
! call fileout%conv%writ%i4 (prim%nsub,    iwork(),1)
  !
  if (new) then
    call classic_entry_section_add(sec_prim_id,sec_prim_len,iwork,edesc,  &
      obufobs,error)
    if (error)  &
      call mrtindex_message(seve%e,rname,'Could not add section Primary')
  else
    call classic_entry_section_update(sec_prim_id,sec_prim_len,iwork,edesc,  &
      obufobs,error)
    if (error)  &
      call mrtindex_message(seve%e,rname,'Could not update section Primary')
  endif
  if (error)  return
  !
end subroutine mrtindex_entry_wprim
!
subroutine mrtindex_entry_wcalib(fileout,edesc,calib,new,error)
  use gbl_message
  use gkernel_interfaces
  use classic_api
  use mrtindex_interfaces, except_this=>mrtindex_entry_wcalib
  use mrtindex_types
  use mrtindex_vars
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(classic_file_t),      intent(inout) :: fileout  !
  type(classic_entrydesc_t), intent(inout) :: edesc    ! Entry Descriptor
  type(sec_calib_t),         intent(in)    :: calib    ! The section
  logical,                   intent(in)    :: new      ! New or old (update) section?
  logical,                   intent(inout) :: error    !
  ! Local
  character(len=*), parameter :: rname='INDEX>WCALIB'
  integer(kind=4) :: ndata,iset
  integer(kind=data_length) :: lsec,addr
  integer(kind=4), allocatable :: iwork(:)
  !
  ndata = calib%nfreq * calib%nset * calib%npix
  if (ndata.le.0) then
    call mrtindex_message(seve%e,rname,'Number of points must be > 0')
    error = .true.
    return
  endif
  !
  lsec = 3+2*calib%nset+4*ndata
  allocate(iwork(lsec))  ! zzz failed_allocate
  !
  call fileout%conv%writ%i4(calib%nfreq,iwork(1),1)
  call fileout%conv%writ%i4(calib%nset, iwork(2),1)
  call fileout%conv%writ%i4(calib%npix, iwork(3),1)
  addr = 4
  do iset=1,calib%nset
    call fileout%conv%writ%cc(calib%frontend(iset),iwork(addr),2)
    addr = addr+2
  enddo
  call fileout%conv%writ%r8(calib%freq,iwork(addr),ndata)
  addr = addr+2*ndata
  call fileout%conv%writ%r4(calib%atsys,iwork(addr),ndata)
  addr = addr+ndata
  call fileout%conv%writ%r4(calib%ztau,iwork(addr),ndata)
  !
  if (new) then
    call classic_entry_section_add(sec_calib_id,lsec,iwork,edesc,  &
      obufobs,error)
    if (error)  &
      call mrtindex_message(seve%e,rname,'Could not add section Calibration')
  else
    call classic_entry_section_update(sec_calib_id,lsec,iwork,edesc,  &
      obufobs,error)
    if (error)  &
      call mrtindex_message(seve%e,rname,'Could not update section Calibration')
  endif
  deallocate(iwork)
  if (error)  return
  !
end subroutine mrtindex_entry_wcalib
!
subroutine mrtindex_entry_wscience(fileout,edesc,sci,new,error)
  use gbl_message
  use gkernel_interfaces
  use classic_api
  use mrtindex_interfaces, except_this=>mrtindex_entry_wscience
  use mrtindex_types
  use mrtindex_vars
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(classic_file_t),      intent(inout) :: fileout  !
  type(classic_entrydesc_t), intent(inout) :: edesc    ! Entry Descriptor
  type(sec_science_t),       intent(in)    :: sci      ! The section
  logical,                   intent(in)    :: new      ! New or old (update) section?
  logical,                   intent(inout) :: error    !
  ! Local
  character(len=*), parameter :: rname='INDEX>WSCIENCE'
  integer(kind=4) :: iwork(sec_science_len)
  !
  call fileout%conv%writ%i4(sci%caldobs,iwork(1),1)
  call fileout%conv%writ%i4(sci%calscan,iwork(2),1)
  call fileout%conv%writ%i4(sci%calback,iwork(3),1)
  call fileout%conv%writ%i4(sci%calvers,iwork(4),1)
  !
  if (new) then
    call classic_entry_section_add(sec_science_id,sec_science_len,iwork,  &
      edesc,obufobs,error)
    if (error)  &
      call mrtindex_message(seve%e,rname,'Could not add section Science')
  else
    call classic_entry_section_update(sec_science_id,sec_science_len,iwork,  &
      edesc,obufobs,error)
    if (error)  &
      call mrtindex_message(seve%e,rname,'Could not update section Science')
  endif
  if (error)  return
  !
end subroutine mrtindex_entry_wscience
!
subroutine mrtindex_entry_windx(file,ient,indx,error)
  use gbl_message
  use classic_api
  use mrtindex_interfaces, except_this=>mrtindex_entry_windx
  use mrtindex_types
  use mrtindex_vars
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(classic_file_t),       intent(in)    :: file   !
  integer(kind=entry_length), intent(in)    :: ient   !
  type(mrtindex_indx_t),        intent(in)    :: indx   !
  logical,                    intent(inout) :: error  ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='ENTRY>WINDX'
  integer(kind=4) :: odatabi(mrtindex_indx_length)
  !
  call mrtindex_message(seve%t,rname,'Welcome')
  !
  ! Fill odatabi(:) from indx, convert bytes if needed
  call mrtindex_index_tobuf(indx,odatabi,file%conv,error)
  if (error)  return
  !
  ! Write the block index of ient in odatabi(:)
  call classic_entryindex_write(file,ient,odatabi,obufbi,error)
  if (error)  return
  !
end subroutine mrtindex_entry_windx
!
subroutine mrtindex_index_tobuf(indx,data,conv,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api
  use mrtindex_interfaces, except_this=>mrtindex_index_tobuf
  use mrtindex_types
  !---------------------------------------------------------------------
  ! @ private
  !   Copy the Block Index into the given buffer
  !---------------------------------------------------------------------
  type(mrtindex_indx_t),      intent(in)    :: indx     !
  integer(kind=4),          intent(out)   :: data(*)  !
  type(classic_fileconv_t), intent(in)    :: conv     !
  logical,                  intent(inout) :: error    !
  ! Local
  !
  ! Convert logical to integer
  call conv%writ%i8(indx%bloc,       data(1),   1)
  call conv%writ%i4(indx%word,       data(3),   3)
  call conv%writ%cc(indx%source,     data(6),   3)
  call conv%writ%cc(indx%projid,     data(9),   2)
  call conv%writ%r8(indx%ut,         data(11),  2)
  call conv%writ%r4(indx%az,         data(15),  2)
  call conv%writ%r8(indx%lon,        data(17),  2)
  call conv%writ%i4(indx%system,     data(21),  1)
  call conv%writ%r4(indx%equinox,    data(22),  1)
  call conv%writ%cc(indx%frontend(1),data(23),  2)
  call conv%writ%cc(indx%frontend(2),data(25),  2)
  call conv%writ%cc(indx%frontend(3),data(27),  2)
  call conv%writ%cc(indx%frontend(4),data(29),  2)
  call conv%writ%i4(indx%dobs,       data(31),  8)
  call conv%writ%cc(indx%filename,   data(39), 10)
  call conv%writ%i8(indx%itime,      data(49),  1)
  !
end subroutine mrtindex_index_tobuf
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
