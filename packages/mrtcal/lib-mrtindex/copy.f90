subroutine mrtindex_copy_command(line,cx,error)
  use gildas_def
  use gbl_message
  use mrtindex_dependencies_interfaces
  use mrtindex_interfaces, except_this=>mrtindex_copy_command
  use mrtindex_types
  !---------------------------------------------------------------------
  ! @ public
  !  Support routine for command
  !   MCOPY FileName
  !  Copy the Current indeX from memory to disk
  !---------------------------------------------------------------------
  character(len=*),          intent(in)    :: line
  type(mrtindex_optimize_t), intent(in)    :: cx
  logical,                   intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='MCOPY'
  character(len=filename_length) :: file
  integer(kind=4) :: nc
  !
  call sic_ch(line,0,1,file,nc,.true.,error)
  if (error)  return
  !
  call mrtindex_copy_index(file,cx,error)
  if (error) then
    call mrtindex_message(seve%e,rname,'Error copying index to file '//file)
    return
  endif
  !
end subroutine mrtindex_copy_command
!
subroutine mrtindex_copy_index(filename,optx,error)
  use gbl_message
  use classic_api
  use mrtindex_dependencies_interfaces
  use mrtindex_interfaces, except_this=>mrtindex_copy_index
  use mrtindex_types
  use mrtindex_vars
  !---------------------------------------------------------------------
  ! @ private
  !  Copy the memory index into the disk index
  !---------------------------------------------------------------------
  character(len=*),        intent(in)    :: filename
  type(mrtindex_optimize_t), intent(in)    :: optx
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='MCOPY'
  character(len=filename_length) :: newfile
  integer(kind=entry_length) :: ient
  type(mrtindex_entry_t) :: entry
  type(classic_file_t) :: fileout
  integer(kind=4) :: oldnum,fileid,iid,nids,ids(ix_ndir)  ! Automatic
  logical :: oldopened
  !
  call sic_parse_file(filename,'','.mrt',newfile)
  !
  ! --- Sanity check: must not overwrite one of the input files! ---
  ! Build a list of input file ids to check (it may be a restricted
  ! list compared to the full IX).
  nids = 0
ENTRIES: do ient=1,optx%next-1
    fileid = optx%idir(ient)
    do iid=1,nids
      if (fileid.eq.ids(iid))  cycle ENTRIES
    enddo
    nids = nids+1
    ids(nids) = fileid
  enddo ENTRIES
  !
  ! Check that the MCOPY output file is not one of the inputs!
  do iid=1,nids
    if (ids(iid).eq.ix_opened) then
      ! This input file is the one in memory, just compare it with
      ! the new file we want to write
      inquire(file=newfile,opened=oldopened,number=oldnum)
      if (oldopened .and. oldnum.eq.ix_files(ix_opened)%lun) then
        call mrtindex_message(seve%e,rname,'Output file is one of the IX files')
        error = .true.
        return
      endif
    else
      ! This file is not opened: we should open it temporarily and
      ! compare it. Remember file names can not be compared correctly
      continue
    endif
  enddo
  !
  call mrtindex_file_classic_new(newfile,error)
  if (error)  return
  !
  ! Work on our custom 'fileout' instance, instead of the global
  ! 'ix_files', because
  !   1) we build a new file, it is certainly not already in the
  !      ix_files list,
  !   2) we want to leave it opened while we read the input file(s)
  !      (and not closing and reopening the input and output files
  !      for each entry)
  fileout%spec = newfile
  fileout%nspec = len_trim(newfile)
  if (sic_getlun(fileout%lun).ne.1) then
    error = .true.
    return
  endif
  call classic_file_open(fileout,.true.,error)
  if (error)  goto 100
  !
  call classic_filedesc_open(fileout,error)
  if (error)  goto 100
  !
  ! Reallocate the output buffers
  call reallocate_index_obuf(fileout,error)
  if (error)  return
  !
  do ient=1,optx%next-1
    call mrtindex_optimize_to_entry(optx,ient,entry,error)
    if (error)  exit
    !
    call mrtindex_entry_write(fileout,entry,error)
    if (error)  exit
    !
  enddo
  !
  ! Update the File Descriptor on disk (in particular desc%xnext)
  call classic_filedesc_write(fileout,error)
  if (error)  continue
  !
  call classic_file_close(fileout,error)
  if (error)  continue
  !
100 continue
  call sic_frelun(fileout%lun)
  !
  call mrtindex_entry_free(entry,error)
  if (error)  continue
  !
end subroutine mrtindex_copy_index
