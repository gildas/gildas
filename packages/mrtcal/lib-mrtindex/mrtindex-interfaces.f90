module mrtindex_interfaces_private_c
  !---------------------------------------------------------------------
  ! @ private
  ! Private interfaces to C routines (hand made)
  !---------------------------------------------------------------------
  !
  integer(kind=4), external :: gag_directory_exedir  ! no-interface
  !
end module mrtindex_interfaces_private_c
!
module mrtindex_interfaces
  use mrtindex_interfaces_private
  use mrtindex_interfaces_public
  use mrtindex_interfaces_private_c
end module mrtindex_interfaces
!
module mrtindex_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! mrtindex dependencies public interfaces. Do not use out of the library.
  !---------------------------------------------------------------------
  use gkernel_interfaces
  use classic_interfaces_public
  use imbfits_interfaces_public
end module mrtindex_dependencies_interfaces
