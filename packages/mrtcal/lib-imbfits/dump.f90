subroutine imbfits_dump_summary(imbf,olun,error)
  use gildas_def
  use gbl_message
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_dump_summary
  !---------------------------------------------------------------------
  ! @ public
  ! Support routine for command:
  !   DUMP SUMMARY
  !---------------------------------------------------------------------
  type(imbfits_t), intent(in)    :: imbf
  integer(kind=4), intent(in)    :: olun   ! Output LUN
  logical,         intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='IMBFITS>DUMP>SUMMARY'
  !
  write(olun,'(A)') '--- Summary ---'
  !
  write(olun,'(A,T25,A)') 'File name:',trim(imbf%file%name)
  !
  write(olun,'(A,T25,A)') 'Source name:',trim(imbf%primary%object%val)
  !
  write(olun,'(A,T25,I0)')  'Scan number:',imbf%scan%head%scannum%val
  !
  call imbfits_dump_summary_seclass(imbf%seclass,olun,error)
  if (error)  return
  !
  write(olun,'(1X)')
  !
end subroutine imbfits_dump_summary
!
subroutine imbfits_dump_summary_seclass(seclass,olun,error)
  use gildas_def
  use gbl_message
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_dump_summary_seclass
  !---------------------------------------------------------------------
  ! @ private
  ! Loop over all the equivalence classes and give some details about
  ! them
  !---------------------------------------------------------------------
  type(eclass_char_t), intent(in)    :: seclass
  integer(kind=4),     intent(in)    :: olun     ! Output LUN
  logical,             intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='IMBFITS>DUMP>SUMMARY>SECLASS'
  integer(kind=4) :: iclass
  !
  write(olun,'(A,T25,I0)')  'Number of subscans:',seclass%nval
  write(olun,'(A,T25,I0)')  'Number of classes:', seclass%nequ
  !
  do iclass=1,seclass%nequ
    write(olun,'(2X,A,I0,A,T25,A,A,I0,A)')  &
      '#',iclass,':',                       &
      trim(seclass%val(iclass)),            &
      ' (',seclass%cnt(iclass),' subscan(s))'
  enddo
  !
end subroutine imbfits_dump_summary_seclass
!
subroutine imbfits_dump_file(file,olun,error)
  use gbl_message
  use gildas_def
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_dump_file
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(imbfits_file_t), intent(in)    :: file
  integer(kind=4),      intent(in)    :: olun   ! Output LUN
  logical,              intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='IMBFITS>DUMP>FILE'
  !
  write(olun,'(A)') '--- File ---'
  write(olun,'(A)') 'STATUS'
  write(olun,'(A)') 'UNIT'
  write(olun,'(A)') 'BLOCK'
  write(olun,'(A)') 'NHDU'
  write(olun,'(1X)')
  !
end subroutine imbfits_dump_file
!
subroutine imbfits_dump_subscans(imbf,olun,error)
  use gildas_def
  use gbl_message
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_dump_subscans
  !---------------------------------------------------------------------
  ! @ public
  ! Support routine for command:
  !   DUMP SUBSCANS
  ! The subroutine reopens the input file in its own structure to avoid
  ! messing up the global instances
  !---------------------------------------------------------------------
  type(imbfits_t), intent(inout) :: imbf
  integer(kind=4), intent(in)    :: olun   ! Output LUN
  logical,         intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='IMBFITS>DUMP>SUBSCANS'
  !
  call imbfits_read_leadhdus(imbf,0.0,error)
  if (error)  return
  !
  call imbfits_check_leadhdus(imbf,error)
  if (error)  return
  !
  write(olun,'(A)') '--- Subscans ---'
  !
  write(olun,'(A,T25,I0,A,I0,A)')  'Number of subscans:',  &
    imbf%primary%n_obs%val,' (',imbf%primary%n_obsp%val,' found)'
  !
  call imbfits_dump_subscans_list(imbf,olun,error)
  if (error)  return
  !
  write(olun,'(1X)')
  !
end subroutine imbfits_dump_subscans
!
subroutine imbfits_dump_subscans_list(imbf,olun,error)
  use gildas_def
  use gbl_message
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_dump_subscans_list
  !---------------------------------------------------------------------
  ! @ private
  ! Loop over all the subscans and give some details about them
  !---------------------------------------------------------------------
  type(imbfits_t), intent(inout) :: imbf
  integer(kind=4), intent(in)    :: olun   ! Output LUN
  logical,         intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='IMBFITS>DUMP>SUBSCANS>LIST'
  integer(kind=4) :: isub
  type(imbfits_subscan_t) :: subs
  !
  do isub=1,imbf%primary%n_obsp%val
    call imbfits_read_subscan_header_bynum(imbf,isub,.false.,.false.,0.d0,subs,error)
    if (error)  exit
    !
    write(olun,'(2X,A,I0,A,T25,A,A,A)')          &
      '#',subs%backdata%head%obsnum%val,':',     &
      trim(subs%antslow%head%obstype%val),', ',  &
      trim(subs%antslow%head%substype%val)
  enddo
  !
  call imbfits_free_subscan_header(subs,error)
  if (error)  return
  !
end subroutine imbfits_dump_subscans_list
!
subroutine imbfits_dump_primary(primary,short,olun,error)
  use gbl_message
  use gildas_def
  use imbfits_types
  use imbfits_interfaces, except_this=>imbfits_dump_primary
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  type(imbfits_primary_t), intent(in)    :: primary
  logical,                 intent(in)    :: short    ! Short or verbose output?
  integer(kind=4),         intent(in)    :: olun     ! Output LUN
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='IMBFITS>DUMP>PRIMARY'
  !
  write(olun,'(A)') '--- Primary HDU ---'
  !
  if (short) then
    write(olun,'(A)')  'Header available'  ! ZZZ improve this
    write(olun,'(1X)')
    return
  endif
  !
  call imbfits_dump_key(primary%simple,olun,error)
  if (error) return
  call imbfits_dump_key(primary%bitpix,olun,error)
  if (error) return
  call imbfits_dump_key(primary%naxis,olun,error)
  if (error) return
  call imbfits_dump_key(primary%extend,olun,error)
  if (error) return
  call imbfits_dump_key(primary%telescop,olun,error)
  if (error) return
  call imbfits_dump_key(primary%origin,olun,error)
  if (error) return
  call imbfits_dump_key(primary%creator,olun,error)
  if (error) return
  call imbfits_dump_key(primary%imbftsve,olun,error)
  if (error) return
  call imbfits_dump_key(primary%instrume,olun,error)
  if (error) return
  call imbfits_dump_key(primary%object,olun,error)
  if (error) return
  call imbfits_dump_key(primary%longobj,olun,error)
  if (error) return
  call imbfits_dump_key(primary%latobj,olun,error)
  if (error) return
  call imbfits_dump_key(primary%timesys,olun,error)
  if (error) return
  call imbfits_dump_key(primary%mjd_obs,olun,error)
  if (error) return
  call imbfits_dump_key(primary%date_obs,olun,error)
  if (error) return
  call imbfits_dump_key(primary%lst,olun,error)
  if (error) return
  call imbfits_dump_key(primary%projid,olun,error)
  if (error) return
  call imbfits_dump_key(primary%queue,olun,error)
  if (error) return
  call imbfits_dump_key(primary%exptime,olun,error)
  if (error) return
  call imbfits_dump_key(primary%n_obs,olun,error)
  if (error) return
  call imbfits_dump_key(primary%n_obsp,olun,error)
  if (error) return
  call imbfits_dump_key(primary%obstype,olun,error)
  if (error) return
  call imbfits_dump_key(primary%nusefeed,olun,error)
  if (error) return
  call imbfits_dump_key(primary%totant,olun,error)
  if (error) return
  call imbfits_dump_key(primary%totsubr,olun,error)
  if (error) return
  call imbfits_dump_key(primary%totback,olun,error)
  if (error) return
  !
  write(olun,'(1X)')
  !
end subroutine imbfits_dump_primary
!
subroutine imbfits_dump_header(desc,olun,error)
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_dump_header
  !---------------------------------------------------------------------
  ! @ private
  ! dumps the content on the common header elements found in the
  ! input 'imbfits_header_t'
  !---------------------------------------------------------------------
  type(imbfits_header_t), intent(in)    :: desc
  integer(kind=4),        intent(in)    :: olun   ! Output LUN
  logical,                intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DUMP>HEADER'
  !
  call imbfits_dump_key(desc%xtension,olun,error)
  if (error) return
  call imbfits_dump_key(desc%bitpix,olun,error)
  if (error) return
  call imbfits_dump_key(desc%naxis,olun,error)
  if (error) return
  call imbfits_dump_key(desc%naxis1,olun,error)
  if (error) return
  call imbfits_dump_key(desc%naxis2,olun,error)
  if (error) return
  call imbfits_dump_key(desc%pcount,olun,error)
  if (error) return
  call imbfits_dump_key(desc%gcount,olun,error)
  if (error) return
  call imbfits_dump_key(desc%tfields,olun,error)
  if (error) return
  call imbfits_dump_key(desc%extname,olun,error)
  if (error) return
  !
end subroutine imbfits_dump_header
!
subroutine imbfits_dump_scan(scan,column,short,olun,error)
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_dump_scan
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  type(imbfits_scan_t), intent(in)    :: scan
  character(len=*),     intent(in)    :: column  ! Optional column name
  logical,              intent(in)    :: short   ! Short or verbose output?
  integer(kind=4),      intent(in)    :: olun    ! Output LUN
  logical,              intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DUMP>SCAN'
  !
  write(olun,'(A,A,A)') '--- ',trim(scan%head%desc%extname%val),' HDU ---'
  !
  if (scan%head%desc%status.eq.code_imbfits_init) then
    write(olun,'(A)')  'WARNING! Header not available'
    write(olun,'(1X)')
    return
  elseif (scan%head%desc%status.eq.code_imbfits_started) then
    write(olun,'(A)')  'WARNING! Header reading was incomplete'
  elseif (short) then
    write(olun,'(A)')  'Header available'
    write(olun,'(1X)')
    return
  elseif (column.ne.'') then
    call imbfits_dump_scan_column(scan,column,olun,error)
    return
  endif
  !
  ! dump header
  call imbfits_dump_header(scan%head%desc,olun,error)
  if (error) return
  ! Telescope and site
  call imbfits_dump_key(scan%head%telescop,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%telsize,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%sitelong,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%sitelat,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%siteelev,olun,error)
  if (error) return
  ! Project
  call imbfits_dump_key(scan%head%projid,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%obsid,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%operator,olun,error)
  if (error) return
  ! Scan
  call imbfits_dump_key(scan%head%scannum,olun,error)
  if (error) return
  ! Date and time
  call imbfits_dump_key(scan%head%date_obs,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%date,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%mjd,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%lst,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%n_obs,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%exptime,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%timesys,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%ut1utc,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%taiutc,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%etutc,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%gpstai,olun,error)
  if (error) return
  ! Fixed source parameters
  call imbfits_dump_key(scan%head%polex,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%poley,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%object,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%ctype1,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%ctype2,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%radesys,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%equinox,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%crval1,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%crval2,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%lonpole,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%latpole,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%longobj,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%latobj,olun,error)
  if (error) return
  ! Moving source parameters
  ! TBD *** JP
  ! Switch mode
  call imbfits_dump_key(scan%head%swtchmod,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%noswitch,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%phasetim,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%wobthrow,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%wobdir,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%wobcycle,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%wobmode,olun,error)
  if (error) return
  ! Pointing and focus
  ! TBD *** JP
  !
  call imbfits_dump_key(scan%head%nfebe,olun,error)
  if (error) return
  ! Weather and calibration
  call imbfits_dump_key(scan%head%pressure,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%tambient,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%humidity,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%winddir,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%windvel,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%windvelm,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%date_wea,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%refracti,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%thot,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%date_hot,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%tiptauz,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%tiptauc,olun,error)
  if (error) return
  call imbfits_dump_key(scan%head%date_tip,olun,error)
  if (error) return
  ! dump table
  call imbfits_dump_column(scan%table%sysoff,olun,error)
  if (error)  return
  call imbfits_dump_column(scan%table%xoffset,olun,error)
  if (error)  return
  call imbfits_dump_column(scan%table%yoffset,olun,error)
  if (error)  return
  !
  write(olun,'(1X)')
  !
end subroutine imbfits_dump_scan
!
subroutine imbfits_dump_scan_column(scan,column,olun,error)
  use imbfits_interfaces, except_this=>imbfits_dump_scan_column
  use imbfits_parameters
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(imbfits_scan_t), intent(in)    :: scan
  character(len=*),     intent(in)    :: column  ! Column name
  integer(kind=4),      intent(in)    :: olun    ! Output LUN
  logical,              intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DUMP>SCAN>COLUMN'
  integer(kind=4) :: ifound
  integer(kind=4), parameter :: ncol=3
  character(len=key_length) :: columns(ncol),found
  data columns / 'SYSOFF','XOFFSET','YOFFSET' /
  !
  call sic_ambigs(rname,column,found,ifound,columns,ncol,error)
  if (error) return
  !
  select case (found)
  case('SYSOFF')
    call imbfits_dump_column(scan%table%sysoff,olun,error,full=.true.)
  case('XOFFSET')
    call imbfits_dump_column(scan%table%xoffset,olun,error,full=.true.)
  case('YOFFSET')
    call imbfits_dump_column(scan%table%yoffset,olun,error,full=.true.)
  end select
  !
end subroutine imbfits_dump_scan_column
!
subroutine imbfits_dump_frontend(front,column,short,olun,error)
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_dump_frontend
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  type(imbfits_front_t), intent(in)    :: front
  character(len=*),      intent(in)    :: column  ! Optional column name
  logical,               intent(in)    :: short   ! Short or verbose output?
  integer(kind=4),       intent(in)    :: olun    ! Output LUN
  logical,               intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DUMP>FRONTEND'
  !
  write(olun,'(A,A,A)') '--- ',trim(front%head%desc%extname%val),' HDU ---'
  !
  if (front%head%desc%status.eq.code_imbfits_init) then
    write(olun,'(A)')  'WARNING! Header not available'
    write(olun,'(1X)')
    return
  elseif (front%head%desc%status.eq.code_imbfits_started) then
    write(olun,'(A)')  'WARNING! Header reading was incomplete'
  elseif (short) then
    write(olun,'(A)')  'Header available'
    write(olun,'(1X)')
    return
  elseif (column.ne.'') then
    call imbfits_dump_frontend_column(front,column,olun,error)
    return
  endif
  !
  ! Read header
  call imbfits_dump_header(front%head%desc,olun,error)
  if (error) return
  call imbfits_dump_key(front%head%scannum,olun,error)
  if (error) return
  call imbfits_dump_key(front%head%date_obs,olun,error)
  if (error) return
  call imbfits_dump_key(front%head%dewrtmod,olun,error)
  if (error) return
  call imbfits_dump_key(front%head%dewang,olun,error)
  if (error) return
  call imbfits_dump_key(front%head%febeband,olun,error)
  if (error) return
  call imbfits_dump_key(front%head%febefeed,olun,error)
  if (error) return
  call imbfits_dump_key(front%head%nusefeed,olun,error)
  if (error) return
  call imbfits_dump_key(front%head%velosys,olun,error)
  if (error) return
  call imbfits_dump_key(front%head%specsys,olun,error)
  if (error) return
  call imbfits_dump_key(front%head%veloconv,olun,error)
  if (error) return
  call imbfits_dump_key(front%head%emirbeam,olun,error)
  if (error) return
  ! Read table
  ! Frequency axis
  call imbfits_dump_column(front%table%recname,olun,error)
  if (error) return
  call imbfits_dump_column(front%table%linename,olun,error)
  if (error) return
  call imbfits_dump_column(front%table%restfreq,olun,error)
  if (error) return
  ! Calibration
  call imbfits_dump_column(front%table%beameff,olun,error)
  if (error) return
  call imbfits_dump_column(front%table%etafss,olun,error)
  if (error) return
  call imbfits_dump_column(front%table%gainimag,olun,error)
  if (error) return
  call imbfits_dump_column(front%table%sideband,olun,error)
  if (error) return
  call imbfits_dump_column(front%table%sbsep,olun,error)
  if (error) return
  call imbfits_dump_column(front%table%widenar,olun,error)
  if (error) return
  call imbfits_dump_column(front%table%tcold,olun,error)
  if (error) return
  call imbfits_dump_column(front%table%thot,olun,error)
  if (error) return
  ! Pixel description
  call imbfits_dump_column(front%table%ifeed,olun,error)
  if (error) return
  call imbfits_dump_column(front%table%nofeeds,olun,error)
  if (error) return
  call imbfits_dump_column(front%table%pola,olun,error)
  if (error) return
  ! Frequency axis
  call imbfits_dump_column(front%table%doppler,olun,error)
  if (error) return
  call imbfits_dump_column(front%table%ifcenter,olun,error)
  if (error) return
  call imbfits_dump_column(front%table%bandwid,olun,error)
  if (error) return
  call imbfits_dump_column(front%table%ifflipps,olun,error)
  if (error) return
  call imbfits_dump_column(front%table%speclo,olun,error)
  if (error) return
  ! Brightness axis
  call imbfits_dump_column(front%table%tscale,olun,error)
  if (error) return
  ! Frequency switching
  call imbfits_dump_column(front%table%frqthrow,olun,error)
  if (error) return
  call imbfits_dump_column(front%table%frqoff1,olun,error)
  if (error) return
  call imbfits_dump_column(front%table%frqoff2,olun,error)
  if (error) return
  !
  write(olun,'(1X)')
  !
end subroutine imbfits_dump_frontend
!
subroutine imbfits_dump_frontend_column(front,column,olun,error)
  use imbfits_interfaces, except_this=>imbfits_dump_frontend_column
  use imbfits_parameters
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(imbfits_front_t), intent(in)    :: front
  character(len=*),      intent(in)    :: column  ! Column name
  integer(kind=4),       intent(in)    :: olun    ! Output LUN
  logical,               intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DUMP>FRONT>COLUMN'
  integer(kind=4) :: ifound
  integer(kind=4), parameter :: ncol=23
  character(len=key_length) :: columns(ncol),found
  data columns / 'RECNAME','LINENAME','RESTFREQ','BEAMEFF','ETAFSS',           &
    'GAINIMAG','SIDEBAND','SBSEP','WIDENAR','TCOLD','THOT','IFEED','NOFEEDS',  &
    'POLA','DOPPLER','IFCENTER','BANDWID','IFFLIPPS','SPECLO','TSCALE',        &
    'FRQTHROW','FRQOFF1','FRQOFF2' /
  !
  call sic_ambigs(rname,column,found,ifound,columns,ncol,error)
  if (error) return
  !
  select case (found)
  case('RECNAME')
    call imbfits_dump_column(front%table%recname,olun,error,full=.true.)
  case('LINENAME')
    call imbfits_dump_column(front%table%linename,olun,error,full=.true.)
  case('RESTFREQ')
    call imbfits_dump_column(front%table%restfreq,olun,error,full=.true.)
  case('BEAMEFF')
    call imbfits_dump_column(front%table%beameff,olun,error,full=.true.)
  case('ETAFSS')
    call imbfits_dump_column(front%table%etafss,olun,error,full=.true.)
  case('GAINIMAG')
    call imbfits_dump_column(front%table%gainimag,olun,error,full=.true.)
  case('SIDEBAND')
    call imbfits_dump_column(front%table%sideband,olun,error,full=.true.)
  case('SBSEP')
    call imbfits_dump_column(front%table%sbsep,olun,error,full=.true.)
  case('WIDENAR')
    call imbfits_dump_column(front%table%widenar,olun,error,full=.true.)
  case('TCOLD')
    call imbfits_dump_column(front%table%tcold,olun,error,full=.true.)
  case('THOT')
    call imbfits_dump_column(front%table%thot,olun,error,full=.true.)
  case('IFEED')
    call imbfits_dump_column(front%table%ifeed,olun,error,full=.true.)
  case('NOFEEDS')
    call imbfits_dump_column(front%table%nofeeds,olun,error,full=.true.)
  case('POLA')
    call imbfits_dump_column(front%table%pola,olun,error,full=.true.)
  case('DOPPLER')
    call imbfits_dump_column(front%table%doppler,olun,error,full=.true.)
  case('IFCENTER')
    call imbfits_dump_column(front%table%ifcenter,olun,error,full=.true.)
  case('BANDWID')
    call imbfits_dump_column(front%table%bandwid,olun,error,full=.true.)
  case('IFFLIPPS')
    call imbfits_dump_column(front%table%ifflipps,olun,error,full=.true.)
  case('SPECLO')
    call imbfits_dump_column(front%table%speclo,olun,error,full=.true.)
  case('TSCALE')
    call imbfits_dump_column(front%table%tscale,olun,error,full=.true.)
  case('FRQTHROW')
    call imbfits_dump_column(front%table%frqthrow,olun,error,full=.true.)
  case('FRQOFF1')
    call imbfits_dump_column(front%table%frqoff1,olun,error,full=.true.)
  case('FRQOFF2')
    call imbfits_dump_column(front%table%frqoff2,olun,error,full=.true.)
  end select
  !
end subroutine imbfits_dump_frontend_column
!
subroutine imbfits_dump_backend(back,column,short,olun,error)
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_dump_backend
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  type(imbfits_back_t), intent(in)    :: back
  character(len=*),     intent(in)    :: column  ! Optional column name
  logical,              intent(in)    :: short   ! Short or verbose output?
  integer(kind=4),      intent(in)    :: olun    ! Output LUN
  logical,              intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DUMP>BACKEND'
  !
  write(olun,'(A,A,A)') '--- ',trim(back%head%desc%extname%val),' HDU ---'
  !
  if (back%head%desc%status.eq.code_imbfits_init) then
    write(olun,'(A)')  'WARNING! Header not available'
    write(olun,'(1X)')
    return
  elseif (back%head%desc%status.eq.code_imbfits_started) then
    write(olun,'(A)')  'WARNING! Header reading was incomplete'
  elseif (short) then
    write(olun,'(A)')  'Header available'
    write(olun,'(1X)')
    return
  elseif (column.ne.'') then
    call imbfits_dump_backend_column(back,column,olun,error)
    return
  endif
  !
  call imbfits_dump_header(back%head%desc,olun,error)
  if (error) return
  !
  call imbfits_dump_key(back%head%scannum,olun,error)
  if (error) return
  call imbfits_dump_key(back%head%date_obs,olun,error)
  if (error) return
  call imbfits_dump_key(back%head%febeband,olun,error)
  if (error) return
  call imbfits_dump_key(back%head%febefeed,olun,error)
  if (error) return
  call imbfits_dump_key(back%head%nusefeed,olun,error)
  if (error) return
  !
  call imbfits_dump_column(back%table%part,olun,error)
  if (error) return
  call imbfits_dump_column(back%table%refchan,olun,error)
  if (error) return
  call imbfits_dump_column(back%table%chans,olun,error)
  if (error) return
  call imbfits_dump_column(back%table%dropped,olun,error)
  if (error)  return
  call imbfits_dump_column(back%table%used,olun,error)
  if (error)  return
  call imbfits_dump_column(back%table%receiver,olun,error)
  if (error)  return
  call imbfits_dump_column(back%table%ifront,olun,error)
  if (error)  return
  call imbfits_dump_column(back%table%band,olun,error)
  if (error)  return
  call imbfits_dump_column(back%table%frontend,olun,error)
  if (error)  return
  call imbfits_dump_column(back%table%polar,olun,error)
  if (error)  return
  call imbfits_dump_column(back%table%pixel,olun,error)
  if (error)  return
  call imbfits_dump_column(back%table%reffreq,olun,error)
  if (error)  return
  call imbfits_dump_column(back%table%spacing,olun,error)
  if (error)  return
  call imbfits_dump_column(back%table%dataflip,olun,error)
  if (error)  return
  call imbfits_dump_column(back%table%linename,olun,error)
  if (error)  return
  !
  write(olun,'(1X)')
  !
end subroutine imbfits_dump_backend
!
subroutine imbfits_dump_backend_column(back,column,olun,error)
  use imbfits_interfaces, except_this=>imbfits_dump_backend_column
  use imbfits_parameters
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(imbfits_back_t), intent(in)    :: back
  character(len=*),     intent(in)    :: column  ! Column name
  integer(kind=4),      intent(in)    :: olun    ! Output LUN
  logical,              intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DUMP>BACK>COLUMN'
  integer(kind=4) :: ifound
  integer(kind=4), parameter :: ncol=15
  character(len=key_length) :: columns(ncol),found
  data columns / 'PART','REFCHAN','CHANS','DROPPED','USED','PIXEL',     &
    'RECEIVER','IFRONT','BAND','FRONTEND','POLAR','REFFREQ','SPACING',  &
    'DATAFLIP','LINENAME' /
  !
  call sic_ambigs(rname,column,found,ifound,columns,ncol,error)
  if (error) return
  !
  select case (found)
  case('PART')
    call imbfits_dump_column(back%table%part,olun,error,full=.true.)
  case('REFCHAN')
    call imbfits_dump_column(back%table%refchan,olun,error,full=.true.)
  case('CHANS')
    call imbfits_dump_column(back%table%chans,olun,error,full=.true.)
  case('DROPPED')
    call imbfits_dump_column(back%table%dropped,olun,error,full=.true.)
  case('USED')
    call imbfits_dump_column(back%table%used,olun,error,full=.true.)
  case('PIXEL')
    call imbfits_dump_column(back%table%pixel,olun,error,full=.true.)
  case('RECEIVER')
    call imbfits_dump_column(back%table%receiver,olun,error,full=.true.)
  case('IFRONT')
    call imbfits_dump_column(back%table%ifront,olun,error,full=.true.)
  case('BAND')
    call imbfits_dump_column(back%table%band,olun,error,full=.true.)
  case('FRONTEND')
    call imbfits_dump_column(back%table%frontend,olun,error,full=.true.)
  case('POLAR')
    call imbfits_dump_column(back%table%polar,olun,error,full=.true.)
  case('REFFREQ')
    call imbfits_dump_column(back%table%reffreq,olun,error,full=.true.)
  case('SPACING')
    call imbfits_dump_column(back%table%spacing,olun,error,full=.true.)
  case('DATAFLIP')
    call imbfits_dump_column(back%table%dataflip,olun,error,full=.true.)
  case('LINENAME')
    call imbfits_dump_column(back%table%linename,olun,error,full=.true.)
  end select
  !
end subroutine imbfits_dump_backend_column
!
subroutine imbfits_dump_backdata(isub,backdata,column,short,olun,error)
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_dump_backdata
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  integer(kind=4),          intent(in)    :: isub      ! Subscan number
  type(imbfits_backdata_t), intent(in)    :: backdata
  character(len=*),         intent(in)    :: column    ! Optional column name
  logical,                  intent(in)    :: short     ! Short or verbose output?
  integer(kind=4),          intent(in)    :: olun      ! Output LUN
  logical,                  intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DUMP>BACKDATA'
  !
  write(olun,'(A,A,A,I0,A)') '--- ',trim(backdata%head%desc%extname%val),  &
    ' HDU, subscan #',isub,' ---'
  !
  if (backdata%head%desc%status.eq.code_imbfits_init) then
    write(olun,'(A)')  'WARNING! Header not available'
    write(olun,'(1X)')
    return
  elseif (backdata%head%desc%status.eq.code_imbfits_started) then
    write(olun,'(A)')  'WARNING! Header reading was incomplete'
  elseif (short) then
    write(olun,'(A)')  'Header available'
    write(olun,'(1X)')
    return
  elseif (column.ne.'') then
    call imbfits_dump_backdata_column(backdata,column,olun,error)
    return
  endif
  !
  ! Header
  call imbfits_dump_header(backdata%head%desc,olun,error)
  if (error) return
  !
  call imbfits_dump_key(backdata%head%scannum,olun,error)
  if (error) return
  call imbfits_dump_key(backdata%head%obsnum,olun,error)
  if (error) return
  call imbfits_dump_key(backdata%head%baseband,olun,error)
  if (error) return
  call imbfits_dump_key(backdata%head%date_obs,olun,error)
  if (error) return
  call imbfits_dump_key(backdata%head%mjd_beg,olun,error)
  if (error) return
  call imbfits_dump_key(backdata%head%date_end,olun,error)
  if (error) return
  call imbfits_dump_key(backdata%head%mjd_end,olun,error)
  if (error) return
  call imbfits_dump_key(backdata%head%channels,olun,error)
  if (error) return
  call imbfits_dump_key(backdata%head%nphases,olun,error)
  if (error) return
  call imbfits_dump_key(backdata%head%phaseone,olun,error)
  if (error) return
  call imbfits_dump_key(backdata%head%tstamped,olun,error)
  if (error) return
  !
  ! Tables (except DATA)
  call imbfits_dump_column(backdata%table%mjd,olun,error)
  if (error) return
  call imbfits_dump_column(backdata%table%integtim,olun,error)
  if (error) return
  call imbfits_dump_column(backdata%table%iswitch,olun,error)
  if (error) return
  call imbfits_dump_column(backdata%table%forepoin,olun,error)
  if (error) return
  call imbfits_dump_column(backdata%table%backpoin,olun,error)
  if (error) return
  !
  write(olun,'(1X)')
  !
end subroutine imbfits_dump_backdata
!
subroutine imbfits_dump_backdata_column(backdata,column,olun,error)
  use imbfits_interfaces, except_this=>imbfits_dump_backdata_column
  use imbfits_parameters
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(imbfits_backdata_t), intent(in)    :: backdata
  character(len=*),         intent(in)    :: column    ! Column name
  integer(kind=4),          intent(in)    :: olun      ! Output LUN
  logical,                  intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DUMP>BACKDATA>COLUMN'
  integer(kind=4) :: ifound
  integer(kind=4), parameter :: ncol=5
  character(len=key_length) :: columns(ncol),found
  data columns / 'MJD','INTEGTIM','ISWITCH','FOREPOIN','BACKPOIN' /
  !
  call sic_ambigs(rname,column,found,ifound,columns,ncol,error)
  if (error) return
  !
  select case (found)
  case('MJD')
    call imbfits_dump_column(backdata%table%mjd,olun,error,full=.true.)
  case('INTEGTIM')
    call imbfits_dump_column(backdata%table%integtim,olun,error,full=.true.)
  case('ISWITCH')
    call imbfits_dump_column(backdata%table%iswitch,olun,error,full=.true.)
  case('FOREPOIN')
    call imbfits_dump_column(backdata%table%forepoin,olun,error,full=.true.)
  case('BACKPOIN')
    call imbfits_dump_column(backdata%table%backpoin,olun,error,full=.true.)
  end select
  !
end subroutine imbfits_dump_backdata_column
!
subroutine imbfits_dump_antslow(isub,antslow,column,short,olun,error)
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_dump_antslow
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  integer(kind=4),         intent(in)    :: isub     ! Subscan number
  type(imbfits_antslow_t), intent(in)    :: antslow
  character(len=*),        intent(in)    :: column   ! Optional column name
  logical,                 intent(in)    :: short    ! Short or verbose output?
  integer(kind=4),         intent(in)    :: olun     ! Output LUN
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DUMP>ANTSLOW'
  !
  write(olun,'(A,A,A,I0,A)') '--- ',trim(antslow%head%desc%extname%val),  &
    ' (slow) HDU, subscan #',isub,' ---'
  !
  if (antslow%head%desc%status.eq.code_imbfits_init) then
    write(olun,'(A)')  'WARNING! Header not available'
    write(olun,'(1X)')
    return
  elseif (antslow%head%desc%status.eq.code_imbfits_started) then
    write(olun,'(A)')  'WARNING! Header reading was incomplete'
  elseif (short) then
    write(olun,'(A)')  'Header available'
    write(olun,'(1X)')
    return
  elseif (column.ne.'') then
    call imbfits_dump_antslow_column(antslow,column,olun,error)
    return
  endif
  !
  ! Header
  call imbfits_dump_header(antslow%head%desc,olun,error)
  if (error) return
  !
  call imbfits_dump_key(antslow%head%scannum,olun,error)
  if (error) return
  call imbfits_dump_key(antslow%head%obsnum,olun,error)
  if (error) return
  call imbfits_dump_key(antslow%head%date_obs,olun,error)
  if (error) return
  call imbfits_dump_key(antslow%head%mjd_beg,olun,error)
  if (error) return
  call imbfits_dump_key(antslow%head%date_end,olun,error)
  if (error) return
  call imbfits_dump_key(antslow%head%mjd_end,olun,error)
  if (error) return
  call imbfits_dump_key(antslow%head%obstype,olun,error)
  if (error) return
  call imbfits_dump_key(antslow%head%substype,olun,error)
  if (error) return
  call imbfits_dump_key(antslow%head%substime,olun,error)
  if (error) return
  call imbfits_dump_key(antslow%head%systemof,olun,error)
  if (error) return
  call imbfits_dump_key(antslow%head%subsxoff,olun,error)
  if (error) return
  call imbfits_dump_key(antslow%head%subsyoff,olun,error)
  if (error) return
  !
  call imbfits_dump_key(antslow%head%setype01,olun,error)
  if (error) return
  call imbfits_dump_key(antslow%head%setime01,olun,error)
  if (error) return
  call imbfits_dump_key(antslow%head%sexoff01,olun,error)
  if (error) return
  call imbfits_dump_key(antslow%head%seyoff01,olun,error)
  if (error) return
  call imbfits_dump_key(antslow%head%sexsta01,olun,error)
  if (error) return
  call imbfits_dump_key(antslow%head%seysta01,olun,error)
  if (error) return
  call imbfits_dump_key(antslow%head%sexend01,olun,error)
  if (error) return
  call imbfits_dump_key(antslow%head%seyend01,olun,error)
  if (error) return
  call imbfits_dump_key(antslow%head%sespes01,olun,error)
  if (error) return
  call imbfits_dump_key(antslow%head%sespee01,olun,error)
  if (error) return
  !
  call imbfits_dump_key(antslow%head%dopplerc,olun,error)
  if (error) return
  call imbfits_dump_key(antslow%head%obsvelrf,olun,error)
  if (error) return
  !
  ! Table
  call imbfits_dump_column(antslow%table%mjd,olun,error)
  if (error) return
  call imbfits_dump_column(antslow%table%lst,olun,error)
  if (error) return
  call imbfits_dump_column(antslow%table%longoff,olun,error)
  if (error) return
  call imbfits_dump_column(antslow%table%latoff,olun,error)
  if (error) return
  call imbfits_dump_column(antslow%table%cazimuth,olun,error)
  if (error) return
  call imbfits_dump_column(antslow%table%celevatio,olun,error)
  if (error) return
  call imbfits_dump_column(antslow%table%traceflag,olun,error)
  if (error) return
  !
  write(olun,'(1X)')
  !
end subroutine imbfits_dump_antslow
!
subroutine imbfits_dump_antslow_column(antslow,column,olun,error)
  use imbfits_interfaces, except_this=>imbfits_dump_antslow_column
  use imbfits_parameters
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(imbfits_antslow_t), intent(in)    :: antslow
  character(len=*),        intent(in)    :: column   ! Column name
  integer(kind=4),         intent(in)    :: olun     ! Output LUN
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DUMP>ANTSLOW>COLUMN'
  integer(kind=4) :: ifound
  integer(kind=4), parameter :: ncol=7
  character(len=key_length) :: columns(ncol),found
  data columns / 'MJD','LST','LONGOFF','LATOFF','CAZIMUTH','CELEVATIO',  &
    'TRACEFLAG' /
  !
  call sic_ambigs(rname,column,found,ifound,columns,ncol,error)
  if (error) return
  !
  select case (found)
  case('MJD')
    call imbfits_dump_column(antslow%table%mjd,olun,error,full=.true.)
  case('LST')
    call imbfits_dump_column(antslow%table%lst,olun,error,full=.true.)
  case('LONGOFF')
    call imbfits_dump_column(antslow%table%longoff,olun,error,full=.true.)
  case('LATOFF')
    call imbfits_dump_column(antslow%table%latoff,olun,error,full=.true.)
  case('CAZIMUTH')
    call imbfits_dump_column(antslow%table%cazimuth,olun,error,full=.true.)
  case('CELEVATIO')
    call imbfits_dump_column(antslow%table%celevatio,olun,error,full=.true.)
  case('TRACEFLAG')
    call imbfits_dump_column(antslow%table%traceflag,olun,error,full=.true.)
  end select
  !
end subroutine imbfits_dump_antslow_column
!
subroutine imbfits_dump_antfast(isub,antfast,column,short,olun,error)
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_dump_antfast
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  integer(kind=4),         intent(in)    :: isub     ! Subscan number
  type(imbfits_antfast_t), intent(in)    :: antfast
  character(len=*),        intent(in)    :: column   ! Optional column name
  logical,                 intent(in)    :: short    ! Short or verbose output?
  integer(kind=4),         intent(in)    :: olun     ! Output LUN
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DUMP>ANTFAST'
  !
  write(olun,'(A,A,A,I0,A)') '--- ',trim(antfast%head%desc%extname%val),  &
    ' (fast) HDU, subscan #',isub,' ---'
  !
  if (antfast%head%desc%status.eq.code_imbfits_init) then
    write(olun,'(A)')  'WARNING! Header not available'
    write(olun,'(1X)')
    return
  elseif (antfast%head%desc%status.eq.code_imbfits_started) then
    write(olun,'(A)')  'WARNING! Header reading was incomplete'
  elseif (short) then
    write(olun,'(A)')  'Header available'
    write(olun,'(1X)')
    return
  elseif (column.ne.'') then
    call imbfits_dump_antfast_column(antfast,column,olun,error)
    return
  endif
  !
  ! Header
  call imbfits_dump_header(antfast%head%desc,olun,error)
  if (error) return
  call imbfits_dump_key(antfast%head%date_obs,olun,error)
  if (error) return
  call imbfits_dump_key(antfast%head%mjd_beg,olun,error)
  if (error) return
  call imbfits_dump_key(antfast%head%date_end,olun,error)
  if (error) return
  call imbfits_dump_key(antfast%head%mjd_end,olun,error)
  if (error) return
  call imbfits_dump_key(antfast%head%tracerat,olun,error)
  if (error) return
  !
  ! Table
  call imbfits_dump_column(antfast%table%mjd,olun,error)
  if (error) return
  call imbfits_dump_column(antfast%table%azimuth,olun,error)
  if (error) return
  call imbfits_dump_column(antfast%table%elevation,olun,error)
  !
  write(olun,'(1X)')
  !
end subroutine imbfits_dump_antfast
!
subroutine imbfits_dump_antfast_column(antfast,column,olun,error)
  use imbfits_interfaces, except_this=>imbfits_dump_antfast_column
  use imbfits_parameters
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(imbfits_antfast_t), intent(in)    :: antfast
  character(len=*),        intent(in)    :: column   ! Column name
  integer(kind=4),         intent(in)    :: olun     ! Output LUN
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DUMP>ANTFAST>COLUMN'
  integer(kind=4) :: ifound
  integer(kind=4), parameter :: ncol=3
  character(len=key_length) :: columns(ncol),found
  data columns / 'MJDFAST','AZIMUTH','ELEVATION' /
  !
  call sic_ambigs(rname,column,found,ifound,columns,ncol,error)
  if (error) return
  !
  select case (found)
  case('MJDFAST')
    call imbfits_dump_column(antfast%table%mjd,olun,error,full=.true.)
  case('AZIMUTH')
    call imbfits_dump_column(antfast%table%azimuth,olun,error,full=.true.)
  case('ELEVATION')
    call imbfits_dump_column(antfast%table%elevation,olun,error,full=.true.)
  end select
  !
end subroutine imbfits_dump_antfast_column
!
subroutine imbfits_dump_derot(derot,column,short,olun,error)
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_dump_derot
  !---------------------------------------------------------------------
  ! @ public
  !---------------------------------------------------------------------
  type(imbfits_derot_t), intent(in)    :: derot
  character(len=*),      intent(in)    :: column   ! Optional column name
  logical,               intent(in)    :: short    ! Short or verbose output?
  integer(kind=4),       intent(in)    :: olun     ! Output LUN
  logical,               intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DUMP>DEROT'
  !
  write(olun,'(A,A,A)') '--- ',trim(derot%head%desc%extname%val),' HDU ---'
  !
  if (derot%head%desc%status.eq.code_imbfits_init) then
    write(olun,'(A)')  'WARNING! Header not available'
    write(olun,'(1X)')
    return
  elseif (derot%head%desc%status.eq.code_imbfits_started) then
    write(olun,'(A)')  'WARNING! Header reading was incomplete'
  elseif (short) then
    write(olun,'(A)')  'Header available'
    write(olun,'(1X)')
    return
  elseif (column.ne.'') then
    call imbfits_dump_derot_column(derot,column,olun,error)
    return
  endif
  !
  ! Header
  call imbfits_dump_header(derot%head%desc,olun,error)
  if (error) return
  call imbfits_dump_key(derot%head%scannum,olun,error)
  if (error) return
  call imbfits_dump_key(derot%head%obsnum,olun,error)
  if (error) return
  call imbfits_dump_key(derot%head%date_obs,olun,error)
  if (error) return
  call imbfits_dump_key(derot%head%mjd_beg,olun,error)
  if (error) return
  call imbfits_dump_key(derot%head%date_end,olun,error)
  if (error) return
  call imbfits_dump_key(derot%head%mjd_end,olun,error)
  if (error) return
  call imbfits_dump_key(derot%head%substime,olun,error)
  if (error) return
  !
  ! Table
  call imbfits_dump_column(derot%table%mjd,olun,error)
  if (error) return
  call imbfits_dump_column(derot%table%system,olun,error)
  if (error) return
  call imbfits_dump_column(derot%table%fwant,olun,error)
  if (error) return
  call imbfits_dump_column(derot%table%hwant,olun,error)
  if (error) return
  call imbfits_dump_column(derot%table%swant,olun,error)
  if (error) return
  call imbfits_dump_column(derot%table%fact,olun,error)
  if (error) return
  call imbfits_dump_column(derot%table%hact,olun,error)
  if (error) return
  call imbfits_dump_column(derot%table%sact,olun,error)
  if (error) return
  !
  write(olun,'(1X)')
  !
end subroutine imbfits_dump_derot
!
subroutine imbfits_dump_derot_column(derot,column,olun,error)
  use imbfits_interfaces, except_this=>imbfits_dump_derot_column
  use imbfits_parameters
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(imbfits_derot_t), intent(in)    :: derot
  character(len=*),      intent(in)    :: column   ! Column name
  integer(kind=4),       intent(in)    :: olun     ! Output LUN
  logical,               intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='DUMP>DEROT>COLUMN'
  integer(kind=4) :: ifound
  integer(kind=4), parameter :: ncol=8
  character(len=key_length) :: columns(ncol),found
  data columns / 'MJD','SYSTEM','FWANT','HWANT','SWANT','FACT','HACT','SACT' /
  !
  call sic_ambigs(rname,column,found,ifound,columns,ncol,error)
  if (error) return
  !
  select case (found)
  case('MJD')
    call imbfits_dump_column(derot%table%mjd,olun,error,full=.true.)
  case('SYSTEM')
    call imbfits_dump_column(derot%table%system,olun,error,full=.true.)
  case('FWANT')
    call imbfits_dump_column(derot%table%fwant,olun,error,full=.true.)
  case('HWANT')
    call imbfits_dump_column(derot%table%hwant,olun,error,full=.true.)
  case('SWANT')
    call imbfits_dump_column(derot%table%swant,olun,error,full=.true.)
  case('FACT')
    call imbfits_dump_column(derot%table%fact,olun,error,full=.true.)
  case('HACT')
    call imbfits_dump_column(derot%table%hact,olun,error,full=.true.)
  case('SACT')
    call imbfits_dump_column(derot%table%sact,olun,error,full=.true.)
  end select
  !
end subroutine imbfits_dump_derot_column
