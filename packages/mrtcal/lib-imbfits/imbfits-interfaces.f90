module imbfits_dependencies_interfaces
  use gkernel_interfaces
  use class_interfaces_public
end module imbfits_dependencies_interfaces
!
module imbfits_interfaces
  use imbfits_interfaces_private
  use imbfits_interfaces_public
end module imbfits_interfaces
