!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage IMBFITS messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module imbfits_message_private
  use gpack_def
  !
  ! Identifier used for message identification
  integer :: imbfits_message_id = gpack_global_id  ! Default value for startup message
  !
end module imbfits_message_private
!
subroutine imbfits_message_set_id(id)
  use imbfits_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ public
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer, intent(in) :: id
  ! Local
  character(len=message_length) :: mess
  !
  imbfits_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',imbfits_message_id
  call imbfits_message(seve%d,'imbfits_message_set_id',mess)
  !
end subroutine imbfits_message_set_id
!
subroutine imbfits_message(mkind,procname,message)
  use imbfits_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  ! Messaging facility for the current library. Calls the low-level
  ! (internal) messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  !
  call gmessage_write(imbfits_message_id,mkind,procname,message)
  !
end subroutine imbfits_message
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine imbfits_message_debug(doalloc,alloc,doother,other,error)
  use imbfits_messaging
  !---------------------------------------------------------------------
  ! @ public
  !  Alter the imbfits specific severities. You can switch them to
  ! seve%d and seve%i only!
  !---------------------------------------------------------------------
  logical,         intent(in)    :: doalloc  !
  integer(kind=4), intent(in)    :: alloc    ! For iseve%alloc
  logical,         intent(in)    :: doother  !
  integer(kind=4), intent(in)    :: other    ! For iseve%other
  logical,         intent(inout) :: error    ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='IMBFITS>MESSAGE>DEBUG'
  !
  if (doalloc) then
    if (alloc.ne.seve%i .and. alloc.ne.seve%d) then
      call imbfits_message(seve%e,rname,  &
        'You can switch the ALLOCATION messages to Info or Debug only')
      error = .true.
      return
    endif
    iseve%alloc = alloc
  endif
  !
  if (doother) then
    if (other.ne.seve%i .and. other.ne.seve%d) then
      call imbfits_message(seve%e,rname,  &
        'You can switch the OTHER messages to Info or Debug only')
      error = .true.
      return
    endif
    iseve%other = other
  endif
  !
end subroutine imbfits_message_debug
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
