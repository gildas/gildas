!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine imbfits_read_header_primary(file,primary,error)
  use gbl_message
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_read_header_primary
  !---------------------------------------------------------------------
  ! @ private
  ! Read the Primary HDU
  !---------------------------------------------------------------------
  type(imbfits_file_t),    intent(in)    :: file
  type(imbfits_primary_t), intent(inout) :: primary
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='READ>HEADER>PRIMARY'
  !
  call imbfits_mvhdu_pos(1,file,error)  ! Primary is always #1
  if (error)  return
  !
  ! Check asap if we support this IMBFITS version
  call imbfits_read_key(rname,file,'imbftsve',primary%imbftsve,error)
  if (error) then
    call imbfits_message(seve%e,rname,'Not an IMB-FITS file?')
    return
  endif
  call imbfits_check_version(primary%imbftsve%val,error)
  if (error)  return
  !
  call imbfits_read_key(rname,file,'simple',primary%simple,error)
  if (error) return
  call imbfits_read_key(rname,file,'bitpix',primary%bitpix,error)
  if (error) return
  call imbfits_read_key(rname,file,'naxis',primary%naxis,error)
  if (error) return
  call imbfits_read_key(rname,file,'extend',primary%extend,error)
  if (error) return
  call imbfits_read_key(rname,file,'telescop',primary%telescop,error)
  if (error) return
  call imbfits_read_key(rname,file,'origin',primary%origin,error)
  if (error) return
  call imbfits_read_key(rname,file,'creator',primary%creator,error)
  if (error) return
  call imbfits_read_key(rname,file,'instrume',primary%instrume,error)
  if (error) return
  call imbfits_read_key(rname,file,'object',primary%object,error)
  if (error) return
  call imbfits_read_key(rname,file,'timesys',primary%timesys,error)
  if (error) return
  call imbfits_read_key(rname,file,'mjd-obs',primary%mjd_obs,error)
  if (error) return
  call imbfits_read_key(rname,file,'date-obs',primary%date_obs,error)
  if (error) return
  call imbfits_read_key(rname,file,'projid',primary%projid,error)
  if (error) return
  ! QUEUE: new since v1.36 (HERA) and v2.13 (EMIR)
  call imbfits_read_key(rname,file,'queue',primary%queue,error,default=primary%projid%val)
  if (error) return
  !
  ! The following keywords are defined only when at least 1 subscan is present
  call imbfits_read_key(rname,file,'n_obs',primary%n_obs,error,default=0)
  if (error) return
  call imbfits_read_key(rname,file,'n_obsp',primary%n_obsp,error,default=0)
  if (error) return
  call imbfits_read_key(rname,file,'obstype',primary%obstype,error,default='UNKNOWN')
  if (error) return
  if (primary%n_obs%val.le.0 .or. primary%n_obsp%val.le.0) then
    ! No subscan at all, PRIMARY header is incomplete
    ! Abort reading, leave other values unset (no drama, there is no data
    ! to calibrate anyway)
    return
  endif
  call imbfits_read_key(rname,file,'longobj',primary%longobj,error)
  if (error) return
  call imbfits_read_key(rname,file,'latobj',primary%latobj,error)
  if (error) return
  call imbfits_read_key(rname,file,'lst',primary%lst,error)
  if (error) return
  call imbfits_read_key(rname,file,'exptime',primary%exptime,error)
  if (error) return
  if (primary%imbftsve%val.lt.2.0) then
    primary%nusefeed%val = 0
  else
    call imbfits_read_key(rname,file,'nusefeed',primary%nusefeed,error)
    if (error) return
  endif
  !
  ! The following keywords are defined only when all subscans are present
  if (primary%n_obsp%val.lt.primary%n_obs%val) then
    ! Can be absent, allow a default value
    call imbfits_read_key(rname,file,'totant',primary%totant,error,default=0)
    if (error) return
    call imbfits_read_key(rname,file,'totsubr',primary%totsubr,error,default=0)
    if (error) return
    call imbfits_read_key(rname,file,'totback',primary%totback,error,default=0)
    if (error) return
  else
    ! Must be present, error if not found
    call imbfits_read_key(rname,file,'totant',primary%totant,error)
    if (error) return
    call imbfits_read_key(rname,file,'totsubr',primary%totsubr,error)
    if (error) return
    call imbfits_read_key(rname,file,'totback',primary%totback,error)
    if (error) return
  endif
  !
end subroutine imbfits_read_header_primary
!
subroutine imbfits_read_header_scan(imbf,scan,error)
  use gbl_message
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_read_header_scan
  !---------------------------------------------------------------------
  ! @ private
  ! Read the IMBF-scan HDU
  !---------------------------------------------------------------------
  type(imbfits_t),      intent(in)    :: imbf
  type(imbfits_scan_t), intent(inout) :: scan
  logical,              intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='READ>HEADER>SCAN'
  integer(kind=4) :: nline
  character(len=15) :: sysoff1
  real(kind=4) :: xoffset1,yoffset1
  !
  call imbfits_mvhdu_name('IMBF-scan',imbf%file,error)
  if (error) return
  ! Read header
  call imbfits_read_header_init(imbf%file,scan%head%desc,error)
  if (error) return
  ! Telescope and site
  call imbfits_read_key(rname,imbf%file,'telescop',scan%head%telescop,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'telsize',scan%head%telsize,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'sitelong',scan%head%sitelong,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'sitelat',scan%head%sitelat,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'siteelev',scan%head%siteelev,error)
  if (error) return
  ! Project
  call imbfits_read_key(rname,imbf%file,'projid',scan%head%projid,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'obsid',scan%head%obsid,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'operator',scan%head%operator,error)
  if (error) return
  ! Scan
  call imbfits_read_key(rname,imbf%file,'scannum',scan%head%scannum,error)
  if (error) return
  ! Date and time
  call imbfits_read_key(rname,imbf%file,'date-obs',scan%head%date_obs,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'date',scan%head%date,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'mjd',scan%head%mjd,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'lst',scan%head%lst,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'n_obs',scan%head%n_obs,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'exptime',scan%head%exptime,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'timesys',scan%head%timesys,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'ut1utc',scan%head%ut1utc,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'taiutc',scan%head%taiutc,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'etutc',scan%head%etutc,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'gpstai',scan%head%gpstai,error)
  if (error) return
  ! Fixed source parameters
  if (imbf%primary%imbftsve%val.lt.1.35d0) then
    ! No POLEX/POLEY under V 1.35. Use some default.
    call imbfits_read_key(rname,imbf%file,'polex',scan%head%polex,error,default=0.d0)
    if (error) return
    call imbfits_read_key(rname,imbf%file,'poley',scan%head%poley,error,default=0.d0)
    if (error) return
  else
    call imbfits_read_key(rname,imbf%file,'polex',scan%head%polex,error)
    if (error) return
    call imbfits_read_key(rname,imbf%file,'poley',scan%head%poley,error)
    if (error) return
  endif
  call imbfits_read_key(rname,imbf%file,'object',scan%head%object,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'ctype1',scan%head%ctype1,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'ctype2',scan%head%ctype2,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'radesys',scan%head%radesys,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'equinox',scan%head%equinox,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'crval1',scan%head%crval1,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'crval2',scan%head%crval2,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'lonpole',scan%head%lonpole,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'latpole',scan%head%latpole,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'longobj',scan%head%longobj,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'latobj',scan%head%latobj,error)
  if (error) return
  ! Moving source parameters
  ! TBD *** JP
  ! Switch mode
  call imbfits_read_key(rname,imbf%file,'swtchmod',scan%head%swtchmod,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'noswitch',scan%head%noswitch,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'phasetim',scan%head%phasetim,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'wobthrow',scan%head%wobthrow,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'wobdir',scan%head%wobdir,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'wobcycle',scan%head%wobcycle,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'wobmode',scan%head%wobmode,error)
  if (error) return
  !
  call imbfits_read_key(rname,imbf%file,'nfebe',scan%head%nfebe,error)
  if (error) return
  ! Pointing and focus
  call imbfits_read_key(rname,imbf%file,'p1',scan%head%p1,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'p2',scan%head%p2,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'p3',scan%head%p3,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'p4',scan%head%p4,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'p5',scan%head%p5,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'p7',scan%head%p7,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'p8',scan%head%p8,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'p9',scan%head%p9,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'rxhori',scan%head%rxhori,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'rxvert',scan%head%rxvert,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'third_re',scan%head%third_re,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'zero_pol',scan%head%zero_pol,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'phi_pol',scan%head%phi_pol,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'eps_pol',scan%head%eps_pol,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'col_sin',scan%head%col_sin,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'col_cos',scan%head%col_cos,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'date-poi',scan%head%date_poi,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'p1cor',scan%head%p1cor,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'p2cor',scan%head%p2cor,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'p3cor',scan%head%p3cor,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'p4cor',scan%head%p4cor,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'p5cor',scan%head%p5cor,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'p7cor',scan%head%p7cor,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'p8cor',scan%head%p8cor,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'p9cor',scan%head%p9cor,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'rxhorico',scan%head%rxhorico,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'rxvertco',scan%head%rxvertco,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'focusx',scan%head%focusx,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'focusy',scan%head%focusy,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'focusz',scan%head%focusz,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'p4corinc',scan%head%p4corinc,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'p5corinc',scan%head%p5corinc,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'date-inc',scan%head%date_inc,error)
  if (error) return
  ! Weather and calibration
  call imbfits_read_key(rname,imbf%file,'pressure',scan%head%pressure,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'tambient',scan%head%tambient,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'humidity',scan%head%humidity,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'winddir',scan%head%winddir,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'windvel',scan%head%windvel,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'windvelm',scan%head%windvelm,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'date-wea',scan%head%date_wea,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'refracti',scan%head%refracti,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'thot',scan%head%thot,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'date-hot',scan%head%date_hot,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'tiptauz',scan%head%tiptauz,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'tiptauc',scan%head%tiptauc,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'date-tip',scan%head%date_tip,error)
  if (error) return
  call imbfits_read_header_done(imbf%file,scan%head%desc,error)
  if (error) return
  !
  ! Read table
  nline = scan%head%desc%naxis2%val
  if (nline.gt.0) then
    ! There is something to read:
    call imbfits_read_column(rname,imbf%file,'sysoff',nline,scan%table%sysoff,error)
    if (error)  return
    call imbfits_read_column(rname,imbf%file,'xoffset',nline,scan%table%xoffset,error)
    if (error)  return
    call imbfits_read_column(rname,imbf%file,'yoffset',nline,scan%table%yoffset,error)
    if (error)  return
    if (nline.eq.1) then
      ! 1 value: should be Nasmyth
      if (scan%table%sysoff%val(1).ne.'Nasmyth') then
        call imbfits_message(seve%e,rname,'Unexpected SYSOFF value "'//  &
          trim(scan%table%sysoff%val(1))//'" in Scan HDU')
        error = .true.
        return
      endif
    elseif (nline.ge.3) then
      call imbfits_message(seve%e,rname,'More than 2 SYSOFF values in Scan HDU')
      error = .true.
      return
    endif
  endif
  if (nline.ne.2) then
    if (nline.eq.1) then
      ! We already checked it is Nasmyth
      sysoff1  = scan%table%sysoff%val(1)
      xoffset1 = scan%table%xoffset%val(1)
      yoffset1 = scan%table%yoffset%val(1)
      scan%table%sysoff%comment = 'WARNING! Added a dummy ''projection'' value'
      scan%table%xoffset%comment = 'WARNING! Added a dummy ''projection'' value'
      scan%table%yoffset%comment = 'WARNING! Added a dummy ''projection'' value'
    else
      sysoff1  = 'Nasmyth'
      xoffset1 = 0.0
      yoffset1 = 0.0
      scan%table%sysoff%comment = 'WARNING! Added dummy ''Nasmyth'' and ''projection'' values'
      scan%table%xoffset%comment = 'WARNING! Added dummy ''Nasmyth'' and ''projection'' values'
      scan%table%yoffset%comment = 'WARNING! Added dummy ''Nasmyth'' and ''projection'' values'
    endif
    !
    scan%head%desc%naxis2%val = 2
    nline = scan%head%desc%naxis2%val
    call reallocate_fits_char_1d('sysoff',nline,scan%table%sysoff,error)
    if (error)  return
    call reallocate_fits_real_1d('xoffset',nline,scan%table%xoffset,error)
    if (error)  return
    call reallocate_fits_real_1d('yoffset',nline,scan%table%yoffset,error)
    if (error)  return
    !
    scan%table%sysoff%val(1)  = sysoff1
    scan%table%xoffset%val(1) = xoffset1
    scan%table%yoffset%val(1) = yoffset1
    scan%table%sysoff%val(2)  = 'projection'
    scan%table%xoffset%val(2) = 0.0
    scan%table%yoffset%val(2) = 0.0
  endif
  !
end subroutine imbfits_read_header_scan
!
subroutine imbfits_read_header_frontend(file,front,error)
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_read_header_frontend
  !---------------------------------------------------------------------
  ! @ private
  ! Read the IMBF-frontend HDU
  !---------------------------------------------------------------------
  type(imbfits_file_t),  intent(in)    :: file
  type(imbfits_front_t), intent(inout) :: front
  logical,               intent(inout) :: error
  ! Local
  integer(kind=4) :: nline,ifront
  character(len=*), parameter :: rname='READ>HEADER>FRONTEND'
  !
  call imbfits_mvhdu_name('IMBF-frontend',file,error)
  if (error) return
  ! Read header
  call imbfits_read_header_init(file,front%head%desc,error)
  if (error) return
  call imbfits_read_key(rname,file,'scannum',front%head%scannum,error)
  if (error) return
  call imbfits_read_key(rname,file,'date-obs',front%head%date_obs,error)
  if (error) return
  call imbfits_read_key(rname,file,'dewrtmod',front%head%dewrtmod,error)
  if (error) return
  call imbfits_read_key(rname,file,'dewang',front%head%dewang,error)
  if (error) return
  call imbfits_read_key(rname,file,'febeband',front%head%febeband,error)
  if (error) return
  call imbfits_read_key(rname,file,'febefeed',front%head%febefeed,error)
  if (error) return
  call imbfits_read_key(rname,file,'nusefeed',front%head%nusefeed,error)
  if (error) return
  call imbfits_read_key(rname,file,'velosys',front%head%velosys,error)
  if (error) return
  call imbfits_read_key(rname,file,'specsys',front%head%specsys,error)
  if (error) return
  call imbfits_read_key(rname,file,'veloconv',front%head%veloconv,error)
  if (error) return
  call imbfits_read_key(rname,file,'emirbeam',front%head%emirbeam,error,default='none')
  if (error) return
  call imbfits_read_header_done(file,front%head%desc,error)
  if (error) return
  ! Read table
  nline = front%head%desc%naxis2%val
  ! Frequency axis
  call imbfits_read_column(rname,file,'recname',nline,front%table%recname,error)
  if (error)  return
  call imbfits_read_column(rname,file,'linename',nline,front%table%linename,error)
  if (error)  return
  call imbfits_read_column(rname,file,'restfreq',nline,front%table%restfreq,error)
  if (error)  return
  ! Calibration
  call imbfits_read_column(rname,file,'beameff',nline,front%table%beameff,error)
  if (error)  return
  call imbfits_read_column(rname,file,'etafss',nline,front%table%etafss,error)
  if (error)  return
  call imbfits_read_column(rname,file,'gainimag',nline,front%table%gainimag,error)
  if (error)  return
  call imbfits_read_column(rname,file,'sideband',nline,front%table%sideband,error)
  if (error)  return
  call imbfits_read_column(rname,file,'sbsep',nline,front%table%sbsep,error)
  if (error)  return
  call imbfits_read_column(rname,file,'widenar',nline,front%table%widenar,error,default='NULL')
  if (error)  return
  call imbfits_read_column(rname,file,'tcold',nline,front%table%tcold,error)
  if (error)  return
  call imbfits_read_column(rname,file,'thot',nline,front%table%thot,error)
  if (error)  return
  ! Pixel description
  call imbfits_read_column(rname,file,'ifeed',nline,front%table%ifeed,error)
  if (error)  return
  call imbfits_read_column(rname,file,'nofeeds',nline,front%table%nofeeds,error)
  if (error)  return
  if (.not.file%isholography) then
    call imbfits_read_column(rname,file,'pola',nline,front%table%pola,error)
    if (error)  return
  endif
  ! Frequency axis
  if (.not.file%isholography) then
    call imbfits_read_column(rname,file,'doppler',nline,front%table%doppler,error)
    if (error)  return
  endif
  call imbfits_read_column(rname,file,'ifcenter',nline,front%table%ifcenter,error)
  if (error)  return
  call imbfits_read_column(rname,file,'bandwid',nline,front%table%bandwid,error)
  if (error)  return
  call imbfits_read_column(rname,file,'ifflipps',nline,front%table%ifflipps,error)
  if (error)  return
  call imbfits_read_column(rname,file,'speclo',nline,front%table%speclo,error)
  if (error)  return
  ! Brightness axis
  if (.not.file%isholography) then
    call imbfits_read_column(rname,file,'tscale',nline,front%table%tscale,error)
    if (error)  return
  endif
  ! Frequency switching
  call imbfits_read_column(rname,file,'frqthrow',nline,front%table%frqthrow,error)
  if (error)  return
  call imbfits_read_column(rname,file,'frqoff1',nline,front%table%frqoff1,error)
  if (error)  return
  call imbfits_read_column(rname,file,'frqoff2',nline,front%table%frqoff2,error)
  if (error)  return
  !
  ! HERA frequency axis is delivered as IF2 instead IF1, implying a change
  ! of sign of the IF center and of the frequency offsets (frequency throw in fsw)
  do ifront=1,front%head%desc%naxis2%val
    if (front%table%ifflipps%val(ifront)) then
      front%table%ifcenter%val(ifront) = -front%table%ifcenter%val(ifront)
      front%table%frqoff1%val(ifront) = -front%table%frqoff1%val(ifront)
      front%table%frqoff2%val(ifront) = -front%table%frqoff2%val(ifront)
      front%table%ifcenter%comment = 'WARNING! One or more values reversed because of IFFLIPPS'
      front%table%frqoff1%comment = 'WARNING! One or more values reversed because of IFFLIPPS'
      front%table%frqoff2%comment = 'WARNING! One or more values reversed because of IFFLIPPS'
    endif
  enddo
  !
end subroutine imbfits_read_header_frontend
!
subroutine imbfits_read_header_backend(imbf,bandwidth,error)
  use gbl_message
  use gkernel_interfaces
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_read_header_backend
  !---------------------------------------------------------------------
  ! @ private
  ! Read the IMBF-backend HDU
  !---------------------------------------------------------------------
  type(imbfits_t), intent(inout), target :: imbf
  real(kind=4),    intent(in)            :: bandwidth  ! IMBF-backend slicing width (0 = no slicing)
  logical,         intent(inout)         :: error
  ! Local
  character(len=*), parameter :: rname='READ>HEADER>BACKEND'
  type(imbfits_back_t), pointer :: back_p
  logical :: do_resample
  integer(kind=4) :: nline,ier
  !
  ! Frontend-HDU must have been read before
  if (imbf%front%head%desc%status.ne.code_imbfits_done) then
    call imbfits_message(seve%e,rname,  &
      'Frontend-HDU must have been fully read before Backend-HDU')
    error = .true.
    return
  endif
  !
  call imbfits_mvhdu_name('IMBF-backend',imbf%file,error)
  if (error) return
  !
  ! Select if we should read the original (the disk version) HDU directly
  ! in imbf%back or in a local copy. This avoids local copy and then recopy
  ! into imbf%back when not needed.
  do_resample = bandwidth.gt.0.0
  if (do_resample) then
    allocate(back_p,stat=ier)
    if (failed_allocate(rname,'Backend pointer',ier,error))  return
  else
    back_p => imbf%back
  endif
  !
  ! Read header
  call imbfits_read_header_init(imbf%file,back_p%head%desc,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'scannum',back_p%head%scannum,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'date-obs',back_p%head%date_obs,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'febeband',back_p%head%febeband,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'febefeed',back_p%head%febefeed,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'nusefeed',back_p%head%nusefeed,error)
  if (error) return
  call imbfits_read_header_done(imbf%file,back_p%head%desc,error)
  if (error) return
  ! Read table
  nline = back_p%head%desc%naxis2%val
  if (imbf%primary%imbftsve%val.ge.2.0) then
    call imbfits_read_column(rname,imbf%file,'part',nline,back_p%table%part,error)
    if (error)  return
  else
    call imbfits_read_column(rname,imbf%file,'band',nline,back_p%table%part,error)
    if (error)  return
  endif
  call imbfits_read_column(rname,imbf%file,'refchan',nline,back_p%table%refchan,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'chans',nline,back_p%table%chans,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'dropped',nline,back_p%table%dropped,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'used',nline,back_p%table%used,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'pixel',nline,back_p%table%pixel,error)
  if (error)  return
  if (imbf%primary%imbftsve%val.ge.2.0) then
    call imbfits_read_column(rname,imbf%file,'receiver',nline,back_p%table%receiver,error)
    if (error)  return
    call imbfits_read_column(rname,imbf%file,'band',nline,back_p%table%band,error)
    if (error)  return
    call imbfits_read_column(rname,imbf%file,'polar',nline,back_p%table%polar,error)
    if (error)  return
  else
    call imbfits_read_column(rname,imbf%file,'rec1',nline,back_p%table%receiver,error)
    if (error)  return
    ! 'rec2' has null values for HERA+NBC. Which default here? NULL or blank?
    call imbfits_read_column(rname,imbf%file,'rec2',nline,back_p%table%band,error,default='NULL')
    if (error)  return
    ! NB: blank polar is assumed NONE for IMB-FITS version < 2 (HERA)
    call imbfits_read_column(rname,imbf%file,'polar',nline,back_p%table%polar,error,default='NONE')
    if (error)  return
  endif
  ! Cosmetics:
  call imbfits_upcase_column(back_p%table%band,error)
  if (error)  return
  ! RECEIVER and BAND are read: add a dummy column for actual frontend name
  call imbfits_read_header_backend_frontend(back_p,error)
  if (error)  return
  ! RECEIVER is read: add a backpointer from backend to frontend table
  call imbfits_read_header_backend_ifront(imbf%front,back_p,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'reffreq',nline,back_p%table%reffreq,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'spacing',nline,back_p%table%spacing,error)
  if (error)  return
  ! SPACING, RECEIVER and POLAR are read: add a dummy column for possible flip sign
  call imbfits_read_header_backend_dataflip(back_p,error)
  if (error)  return
  if (imbf%file%isholography) then
    ! 1 linename per backend. Provide a default in case of undefined values (not an error)
    call imbfits_read_column(rname,imbf%file,'linename',nline,back_p%table%linename,error,default='NULL')
    if (error)  return
  else
    if (imbf%primary%imbftsve%val.ge.2.0 .and. imbf%primary%instrume%val.ne.'4mhz') then
      ! 1 linename per backend
      call imbfits_read_column(rname,imbf%file,'linename',nline,back_p%table%linename,error)
      if (error)  return
    else
      ! 1 linename per receiver, converted to 1 linename per backend. Must come after
      ! imbfits_read_header_backend_ifront
      call imbfits_read_header_backend_patchlinename(imbf%front,back_p,error)
      if (error)  return
    endif
  endif
  !
  ! Patch REFCHAN
  if (all(back_p%table%chans%val(1:nline).eq.1)) then
    ! Number of channels are all 1: this is a continuum backend
    if (all(back_p%table%refchan%val(1:nline).eq.1)) then
      ! "Bug" in the IMBFits: REFCHAN indicate to take the 1st first channel
      ! from the DATA column, for any PART, which is wrong!
      ! Must come after PART and PIXEL have been read
      call imbfits_read_header_backend_patchrefchan(imbf%front,back_p,error)
      if (error)  return
    endif
  endif
  !
  if (do_resample) then
    call imbfits_resample_header_backend(back_p,bandwidth,imbf%back,error)
    if (error)  return
    call imbfits_free_header_backend(back_p,error)
    if (error)  return
    deallocate(back_p)
  endif
  !
  ! Compute equivalence classes to gather the chunks by family. Must come
  ! after the above resampling
  call imbfits_read_header_backend_ceclass(imbf%back,error)
  if (error)  return
  !
end subroutine imbfits_read_header_backend
!
subroutine imbfits_read_header_backend_patchlinename(front,back,error)
  use gbl_message
  use imbfits_interfaces, except_this=>imbfits_read_header_backend_patchlinename
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Fill the column imbf%back%table%linename
  !  from the column imbf%front%table%linename
  ! This is generic for any version of IMBFITS, although latest versions
  ! provides themselves the 2 columns
  !---------------------------------------------------------------------
  type(imbfits_front_t), intent(in)    :: front  !
  type(imbfits_back_t),  intent(inout) :: back   !
  logical,               intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='READ>HEADER>BACKEND>PATCHLINENAME'
  integer(kind=4) :: ichunk,nchunk,ifront
  !
  ! Allocation
  nchunk = back%head%desc%naxis2%val
  call reallocate_fits_char_1d('linename',nchunk,back%table%linename,error)
  if (error)  return
  !
  do ichunk=1,nchunk
    ifront = back%table%ifront%val(ichunk)
    back%table%linename%val(ichunk) = front%table%linename%val(ifront)
  enddo
  back%table%linename%comment = 'WARNING! Values copied from FrontEnd table LINENAME column'
  !
end subroutine imbfits_read_header_backend_patchlinename
!
subroutine imbfits_read_header_backend_patchrefchan(front,back,error)
  use gbl_message
  use imbfits_interfaces, except_this=>imbfits_read_header_backend_patchrefchan
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Recompute correct REFCHAN.
  !  This code is generic but is needed only for a few cases.
  !---------------------------------------------------------------------
  type(imbfits_front_t), intent(in)    :: front  !
  type(imbfits_back_t),  intent(inout) :: back   !
  logical,               intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='READ>HEADER>BACKEND>PATCHREFCHAN'
  integer(kind=4) :: ichunk,nofeeds
  !
  nofeeds = front%table%nofeeds%val(1)
  if (any(front%table%nofeeds%val(:).ne.nofeeds)) then
    call imbfits_message(seve%e,rname,  &
      'Different number of pixels per receiver is not implemented')
    error = .true.
    return
  endif
  !
  do ichunk=1,back%head%desc%naxis2%val
    ! This formula should not depend on 'ichunk', i.e. the table could
    ! be consistently re-ordered this would not change the chunks definition!
    back%table%refchan%val(ichunk) =  &
      (back%table%part%val(ichunk)-1)*nofeeds + back%table%pixel%val(ichunk)
  enddo
  back%table%refchan%comment = 'WARNING! Incorrect values were patched!'
  !
end subroutine imbfits_read_header_backend_patchrefchan
!
subroutine imbfits_read_header_backend_frontend(back,error)
  use imbfits_interfaces, except_this=>imbfits_read_header_backend_frontend
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create a custom column FRONTEND. It provides the frontend name as
  ! used in the Class telescope name
  !---------------------------------------------------------------------
  type(imbfits_back_t),  intent(inout) :: back   !
  logical,               intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='READ>HEADER>BACKEND>FRONTEND'
  integer(kind=4) :: ichunk,nchunk
  !
  nchunk = back%head%desc%naxis2%val
  !
  ! Allocation
  call reallocate_fits_char_1d('frontend',nchunk,back%table%frontend,error)
  if (error)  return
  !
  do ichunk=1,nchunk
    if (back%table%receiver%val(ichunk)(1:4).eq.'HERA') then
      ! HERA
      write(back%table%frontend%val(ichunk),'(2(a1),i2.2)')  &
        back%table%receiver%val(ichunk)(5:5),  &
        'H',  &
        back%table%pixel%val(ichunk)
    else
      ! Non-HERA
      back%table%frontend%val(ichunk) = back%table%band%val(ichunk)
      if (back%table%polar%val(ichunk).eq.'REAL') then
        back%table%frontend%val(ichunk)(3:3) = 'R'
      elseif (back%table%polar%val(ichunk).eq.'IMAG') then
        back%table%frontend%val(ichunk)(3:3) = 'I'
      endif
    endif
  enddo
  !
  back%table%frontend%comment = 'Frontend name to be used in telescope name (memory only)'
  !
end subroutine imbfits_read_header_backend_frontend
!
subroutine imbfits_read_header_backend_ifront(front,back,error)
  use gbl_message
  use imbfits_interfaces, except_this=>imbfits_read_header_backend_ifront
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create a custom column IFRONT. It provides a backpointer from
  ! backend RECEIVER column to frontend RECNAME column.
  !---------------------------------------------------------------------
  type(imbfits_front_t), intent(in)    :: front  !
  type(imbfits_back_t),  intent(inout) :: back   !
  logical,               intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='READ>HEADER>BACKEND>IFRONT'
  integer(kind=4) :: ichunk,nchunk,ifront,nfront
  character(len=2) :: recname2(front%head%desc%naxis2%val)  ! Automatic array
  character(len=20) :: cfront
  character(len=message_length) :: mess
  !
  nchunk = back%head%desc%naxis2%val
  nfront = front%head%desc%naxis2%val
  recname2(1:nfront) = front%table%recname%val(1:nfront)  ! Truncated copy
  !
  ! Allocation
  call reallocate_fits_inte_1d('ifront',nchunk,back%table%ifront,error)
  if (error)  return
  !
  do ichunk=1,nchunk
    ! First try full name match (e.g. for HERA)
    call sic_ambigs_sub(rname,back%table%receiver%val(ichunk),cfront,ifront,  &
      front%table%recname%val,nfront,error)
    if (error)  goto 100
    if (ifront.gt.0) then
      back%table%ifront%val(ichunk) = ifront
      cycle  ! Unique match found, OK
    endif
    !
    ! Then try 2 first letters match (e.g. for EMIR)
    call sic_ambigs_sub(rname,back%table%receiver%val(ichunk)(1:2),cfront,ifront,  &
      recname2,nfront,error)
    if (error)  goto 100
    if (ifront.gt.0) then
      back%table%ifront%val(ichunk) = ifront
      cycle  ! Unique match found, OK
    endif
    !
    ! No match => error
    goto 100
    !
  enddo
  !
  back%table%ifront%comment = 'Backpointer to frontend table (memory only)'
  return
  !
100 continue
  write(mess,'(A,A,A)')  'Could not match backend RECEIVER name ''',  &
    trim(back%table%receiver%val(ichunk)),''' to frontend RECNAME:'
  call imbfits_message(seve%e,rname,mess)
  call imbfits_dump_column(front%table%recname,6,error,full=.true.)
  error = .true.
  !
end subroutine imbfits_read_header_backend_ifront
!
subroutine imbfits_read_header_backend_dataflip(back,error)
  use gbl_message
  use imbfits_interfaces, except_this=>imbfits_read_header_backend_dataflip
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create a custom column DATAFLIP. It provides a logical indicating
  ! if the data sign should be flipped or not at read time. As of today,
  ! this is used only in the context of polarimetry, but this is generic
  ! enough for other use cases if needed.
  !---------------------------------------------------------------------
  type(imbfits_back_t),  intent(inout) :: back   !
  logical,               intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='READ>HEADER>BACKEND>DATAFLIP'
  integer(kind=4) :: ichunk,nchunk
  !
  nchunk = back%head%desc%naxis2%val
  !
  ! Allocation
  call reallocate_fits_logi_1d('dataflip',nchunk,back%table%dataflip,error)
  if (error)  return
  !
  do ichunk=1,nchunk
    ! Sign must be flipped if:
    !  - this is an IMAG chunk, and
    !  - SPACING<0 and polar is H, or SPACING>0 and polar is V
    back%table%dataflip%val(ichunk) =  &
      back%table%polar%val(ichunk).eq.'IMAG'  .and.  &
      ( (back%table%spacing%val(ichunk).gt.0.) .neqv.  &
        (back%table%receiver%val(ichunk)(3:3).eq.'H') )
    !
  enddo
  !
  back%table%dataflip%comment = 'IMAG polar data needs sign flip (memory only)'
  !
end subroutine imbfits_read_header_backend_dataflip
!
subroutine imbfits_read_header_backend_ceclass(back,error)
  use gbl_message
  use gkernel_interfaces
  use gkernel_types
  use imbfits_interfaces, except_this=>imbfits_read_header_backend_ceclass
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Compute the Chunks Equivalence CLASSes, i.e. find all the families
  ! (each one defines a chunkset) with unique (PART,PIXEL,RECEIVER,
  ! POLAR). This is computed only once per backend table i.e. once per
  ! scan (=> low impact for the whole scan)
  !---------------------------------------------------------------------
  type(imbfits_back_t), intent(inout) :: back   !
  logical,              intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='READ>HEADER>BACKEND>CECLASS'
  integer(kind=4) :: nval,ichunk,nchunk
  !
  nval = back%head%desc%naxis2%val
  if (allocated(back%chunks%used))    deallocate(back%chunks%used)
  allocate(back%chunks%used(nval))
  !
  ! How many chunks? Store in 3 structures:
  !
  nchunk = back%head%desc%naxis2%val
  do ichunk=1,nchunk
    back%chunks%used(ichunk) = ichunk
  enddo
  !
  ! NB1: useful size (NOT allocation size) of back%chunks%used(:) will be
  !      back%chunks%eclass%nval
  ! NB2: nchunk can be 0 because of faulty imb-fits. We still want to index
  !      them, i.e. do not raise an error in this case.
  !
  call compute_eclass(nchunk,back%chunks,error)
  if (error)  return
  !
  back%polchunks = any(back%chunks%kind.eq.code_h) .or.  &
                   any(back%chunks%kind.eq.code_v) .or.  &
                   any(back%chunks%kind.eq.code_r) .or.  &
                   any(back%chunks%kind.eq.code_i)
  !
contains
  function chunk_kind(code,pol)
    integer(kind=4) :: chunk_kind
    character(len=*), intent(in) :: code
    character(len=*), intent(in) :: pol   ! Letter H or V
    select case (code)
    case ('','NONE')
      chunk_kind = code_n
    case ('AXIS')
      if (pol.eq.'H') then
        chunk_kind = code_h
      elseif (pol.eq.'V') then
        chunk_kind = code_v
      else
        call imbfits_message(seve%w,rname,'Unexpected polarization letter '//pol)
        chunk_kind = code_n
      endif
    case ('REAL')
      chunk_kind = code_r
    case ('IMAG')
      chunk_kind = code_i
    case default
      call imbfits_message(seve%w,rname,'Unexpected chunk kind '//code)
      chunk_kind = code_n
    end select
  end function chunk_kind
  !
  subroutine compute_eclass(nchunk,chunks,error)
    integer(kind=4),             intent(in)    :: nchunk
    type(imbfits_back_chunks_t), intent(inout) :: chunks
    logical,                     intent(inout) :: error
    ! Local
    integer(kind=4) :: iequ
    !
    ! Allocation
    call reallocate_eclass_2inte2char(chunks%eclass,nchunk,error)
    if (error)  return
    !
    ! Sanity check
    if (nchunk.ge.1) then
      if (len(chunks%eclass%val3(1)).lt.len(back%table%receiver%val(1)) .or.  &
          len(chunks%eclass%val4(1)).lt.len(back%table%polar%val(1))) then
        ! You have to enlarge the character string length in the type
        ! 'eclass_2inte2char_t' in the Gildas kernel, else you may loose
        ! characters in the val3(:) or val4(:) copy below
        call imbfits_message(seve%e,rname,  &
          'Internal error: string too small in type ''eclass_2inte2char_t''')
        call free_eclass_2inte2char(chunks%eclass,error)
        error = .true.
        return
      endif
    endif
    !
    ! Init
    do ichunk=1,nchunk
      chunks%eclass%val1(ichunk) = back%table%part%val(chunks%used(ichunk))
      chunks%eclass%val2(ichunk) = back%table%pixel%val(chunks%used(ichunk))
      chunks%eclass%val3(ichunk) = back%table%receiver%val(chunks%used(ichunk))
      chunks%eclass%val4(ichunk) = back%table%polar%val(chunks%used(ichunk))
      chunks%eclass%cnt(ichunk)  = 1
      !
      if (chunks%eclass%val4(ichunk).eq."REAL" .or.  &
          chunks%eclass%val4(ichunk).eq."IMAG") then
        ! When computing the equivalence classes for REAL and IMAG, the
        ! polarization should be ignored. This means that the 3rd letter
        ! should be dropped from the RECEIVER column
        chunks%eclass%val3(ichunk)(3:3) = '*'
      endif
    enddo
    !
    ! Compute
    call eclass_2inte2char(eclass_2inte2char_eq,chunks%eclass)
    !
    ! Derive some products for easier use:
    if (allocated(chunks%kind))  deallocate(chunks%kind)
    allocate(chunks%kind(chunks%eclass%nequ))
    do iequ=1,chunks%eclass%nequ
      chunks%kind(iequ) = chunk_kind(chunks%eclass%val4(iequ),  &
                                     chunks%eclass%val3(iequ)(3:3))
    enddo
  end subroutine compute_eclass
  !
end subroutine imbfits_read_header_backend_ceclass
!
subroutine imbfits_read_header_antslow(imbf,isub,mjd_shift,antslow,error)
  use gbl_message
  use cfitsio_api
  use gkernel_interfaces
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_read_header_antslow
  use imbfits_parameters
  !---------------------------------------------------------------------
  ! @ public
  ! Read the IMBF-antenna HDU
  !---------------------------------------------------------------------
  type(imbfits_t),         intent(in)    :: imbf       !
  integer(kind=4),         intent(in)    :: isub       ! Subscan number
  real(kind=8),            intent(in)    :: mjd_shift  ! [sec]
  type(imbfits_antslow_t), intent(inout) :: antslow    !
  logical,                 intent(inout) :: error      !
  ! Local
  character(len=*), parameter :: rname='READ>HEADER>ANTSLOW'
  integer(kind=4) :: nline
  !
  ! First move to the correct HDU
  call imbfits_mvhdu_subscan(isub,hdu_antslow,imbf,error)
  if (error)  return
  !
  ! Read header
  call imbfits_read_header_init(imbf%file,antslow%head%desc,error)
  if (error) return
  !
  call imbfits_read_key(rname,imbf%file,'SCANNUM',antslow%head%scannum,error)
  if (error)  return
  call imbfits_read_key(rname,imbf%file,'OBSNUM',antslow%head%obsnum,error)
  if (error)  return
  call imbfits_read_key(rname,imbf%file,'DATE-OBS',antslow%head%date_obs,error)
  if (error)  return
  call imbfits_read_header_isodate2mjd(antslow%head%date_obs,'MJD_BEG',antslow%head%mjd_beg,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'DATE-END',antslow%head%date_end,error)
  if (error)  return
  call imbfits_read_header_isodate2mjd(antslow%head%date_end,'MJD_END',antslow%head%mjd_end,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'OBSTYPE',antslow%head%obstype,error)
  if (error)  return
  call imbfits_read_key(rname,imbf%file,'SUBSTYPE',antslow%head%substype,error)
  if (error)  return
  call imbfits_read_key(rname,imbf%file,'SUBSTIME',antslow%head%substime,error)
  if (error)  return
  call imbfits_read_key(rname,imbf%file,'SYSTEMOF',antslow%head%systemof,error)
  if (error)  return
  call imbfits_read_key(rname,imbf%file,'SUBSXOFF',antslow%head%subsxoff,error)
  if (error)  return
  call imbfits_read_key(rname,imbf%file,'SUBSYOFF',antslow%head%subsyoff,error)
  if (error)  return
  !
  call imbfits_read_key(rname,imbf%file,'SETYPE01',antslow%head%setype01,error)
  if (error)  return
  call imbfits_read_key(rname,imbf%file,'SETIME01',antslow%head%setime01,error)
  if (error)  return
  call imbfits_read_key(rname,imbf%file,'SEXOFF01',antslow%head%sexoff01,error)
  if (error)  return
  call imbfits_read_key(rname,imbf%file,'SEYOFF01',antslow%head%seyoff01,error)
  if (error)  return
  call imbfits_read_key(rname,imbf%file,'SEXSTA01',antslow%head%sexsta01,error)
  if (error)  return
  call imbfits_read_key(rname,imbf%file,'SEYSTA01',antslow%head%seysta01,error)
  if (error)  return
  call imbfits_read_key(rname,imbf%file,'SEXEND01',antslow%head%sexend01,error)
  if (error)  return
  call imbfits_read_key(rname,imbf%file,'SEYEND01',antslow%head%seyend01,error)
  if (error)  return
  call imbfits_read_key(rname,imbf%file,'SESPES01',antslow%head%sespes01,error)
  if (error)  return
  call imbfits_read_key(rname,imbf%file,'SESPEE01',antslow%head%sespee01,error)
  if (error)  return
  !
  call imbfits_read_key(rname,imbf%file,'DOPPLERC',antslow%head%dopplerc,error)
  if (error)  return
  call imbfits_read_key(rname,imbf%file,'OBSVELRF',antslow%head%obsvelrf,error)
  if (error)  return
  !
  call imbfits_read_header_done(imbf%file,antslow%head%desc,error)
  if (error) return
  !
  ! Table
  nline = antslow%head%desc%naxis2%val
  call imbfits_read_column(rname,imbf%file,'MJD',nline,antslow%table%mjd,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'LST',nline,antslow%table%lst,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'LONGOFF',nline,antslow%table%longoff,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'LATOFF',nline,antslow%table%latoff,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'CAZIMUTH',nline,antslow%table%cazimuth,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'CELEVATIO',nline,antslow%table%celevatio,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'TRACEFLAG',nline,antslow%table%traceflag,error)
  if (error)  return
  !
  ! Patch MJD
  call imbfits_read_header_antslow_mjd_shift(antslow,mjd_shift,error)
  if (error)  return
  !
end subroutine imbfits_read_header_antslow
!
subroutine imbfits_read_header_antslow_mjd_shift(antslow,mjd_shift,error)
  use gbl_message
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_read_header_antslow_mjd_shift
  !---------------------------------------------------------------------
  ! @ private
  ! Patch the MJD column for the given shift (positive or negative, but
  ! usually zero).
  !---------------------------------------------------------------------
  type(imbfits_antslow_t), intent(inout) :: antslow
  real(kind=8),            intent(in)    :: mjd_shift  ! [sec]
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='READ>HEADER>ANTSLOW>MJD>SHIFT'
  character(len=message_length) :: mess
  !
  if (mjd_shift.eq.0.d0)  return
  !
  write(mess,'(A,F0.3,A)')  'Antenna slow traces shifted by ',mjd_shift,' seconds'
  !
  antslow%table%mjd%val(:) = antslow%table%mjd%val(:)+mjd_shift/86400.d0
  antslow%table%mjd%comment = mess
  !
  if (antslow%head%obsnum%val.eq.1) then
    ! Warn only on 1st subscan to avoid too many warnings
    call imbfits_message(seve%w,rname,mess)
  endif
  !
end subroutine imbfits_read_header_antslow_mjd_shift
!
subroutine imbfits_read_header_antfast(imbf,isub,antfast,error)
  use gbl_message
  use cfitsio_api
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_read_header_antfast
  use imbfits_parameters
  !---------------------------------------------------------------------
  ! @ private
  ! Read the IMBF-antenna HDU
  !---------------------------------------------------------------------
  type(imbfits_t),         intent(in)    :: imbf     !
  integer(kind=4),         intent(in)    :: isub     ! Subscan number
  type(imbfits_antfast_t), intent(inout) :: antfast  !
  logical,                 intent(inout) :: error    !
  ! Local
  character(len=*), parameter :: rname='READ>HEADER>ANTFAST'
  integer(kind=4) :: nline,nelt
  !
  ! First move to the correct HDU
  call imbfits_mvhdu_subscan(isub,hdu_antfast,imbf,error)
  if (error)  return
  !
  ! Read header
  call imbfits_read_header_init(imbf%file,antfast%head%desc,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'DATE-OBS',antfast%head%date_obs,error)
  if (error)  return
  call imbfits_read_header_isodate2mjd(antfast%head%date_obs,'MJD_BEG',antfast%head%mjd_beg,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'DATE-END',antfast%head%date_end,error)
  if (error)  return
  call imbfits_read_header_isodate2mjd(antfast%head%date_end,'MJD_END',antfast%head%mjd_end,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'TRACERAT',antfast%head%tracerat,error)
  if (error)  return
  call imbfits_read_header_done(imbf%file,antfast%head%desc,error)
  if (error) return
  !
  nline = antfast%head%desc%naxis2%val
  nelt = antfast%head%tracerat%val
  call imbfits_read_column(rname,imbf%file,'MJDFAST',nline,nelt,antfast%table%mjd,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'AZIMUTH',nline,nelt,antfast%table%azimuth,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'ELEVATION',nline,nelt,antfast%table%elevation,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'TRACKING_AZ',nline,nelt,antfast%table%tracking_az,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'TRACKING_EL',nline,nelt,antfast%table%tracking_el,error)
  if (error)  return
  !
end subroutine imbfits_read_header_antfast
!
subroutine imbfits_read_header_derot(imbf,derot,error)
  use gbl_message
  use imbfits_interfaces, except_this=>imbfits_read_header_derot
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ public
  ! Read the IMBF-derot HDU
  !---------------------------------------------------------------------
  type(imbfits_t),       intent(in)    :: imbf   !
  type(imbfits_derot_t), intent(inout) :: derot  !
  logical,               intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='READ>HEADER>DEROT'
  !
  if (imbf%primary%imbftsve%val.ge.2.0) then
    ! Move to the correct HDU. It is assumed to be last in the file
    call imbfits_mvhdu_pos(imbf%hdus%pos(hdu_derot),imbf%file,error)
    if (error)  return
    ! Then read it:
    call imbfits_read_header_derot_onehdu(imbf,derot,error)
    if (error)  return
  else
    ! There is one derotator table per subscan: read and merge them
    ! all at once:
    call imbfits_read_header_derot_allinone(imbf,derot,error)
    if (error)  return
  endif
  !
end subroutine imbfits_read_header_derot
!
subroutine imbfits_read_header_derot_onehdu(imbf,derot,error)
  use gbl_message
  use cfitsio_api
  use imbfits_types
  use imbfits_interfaces, except_this=>imbfits_read_header_derot_onehdu
  use imbfits_parameters
  !---------------------------------------------------------------------
  ! @ public
  ! Read the IMBF-derot HDU
  !---------------------------------------------------------------------
  type(imbfits_t),       intent(in)    :: imbf   !
  type(imbfits_derot_t), intent(inout) :: derot  !
  logical,               intent(inout) :: error  !
  ! Local
  character(len=*), parameter :: rname='READ>HEADER>DEROT'
  integer(kind=4) :: nline
  !
  ! Read header
  call imbfits_read_header_init(imbf%file,derot%head%desc,error)
  if (error) return
  !
  call imbfits_read_key(rname,imbf%file,'SCANNUM',derot%head%scannum,error)
  if (error)  return
  call imbfits_read_key(rname,imbf%file,'OBSNUM',derot%head%obsnum,error)
  if (error)  return
  call imbfits_read_key(rname,imbf%file,'DATE-OBS',derot%head%date_obs,error)
  if (error)  return
  call imbfits_read_header_isodate2mjd(derot%head%date_obs,'MJD_BEG',derot%head%mjd_beg,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'DATE-END',derot%head%date_end,error)
  if (error)  return
  call imbfits_read_header_isodate2mjd(derot%head%date_end,'MJD_END',derot%head%mjd_end,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'SUBSTIME',derot%head%substime,error)
  if (error)  return
  !
  call imbfits_read_header_done(imbf%file,derot%head%desc,error)
  if (error) return
  !
  ! Table
  nline = derot%head%desc%naxis2%val
  call imbfits_read_column(rname,imbf%file,'MJD',nline,derot%table%mjd,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'SYSTEM',nline,derot%table%system,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'FWANT',nline,derot%table%fwant,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'HWANT',nline,derot%table%hwant,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'SWANT',nline,derot%table%swant,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'FACT',nline,derot%table%fact,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'HACT',nline,derot%table%hact,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'SACT',nline,derot%table%sact,error)
  if (error)  return
  !
end subroutine imbfits_read_header_derot_onehdu
!
subroutine imbfits_read_header_derot_allinone(imbf,allderot,error)
  use gbl_message
  use cfitsio_api
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_read_header_derot_allinone
  use imbfits_parameters
  !---------------------------------------------------------------------
  ! @ public
  !   Read all the IMBF-derot HDUs (for all subscans) concatenated as a
  ! single pseudo HDU. This subroutine makes sense only for pre-v2
  ! IMB-FITS.
  !   Note that this code assumes:
  !  1) that the subscans are ordered by time, and
  !  2) that the derotator tables are ordered by time.
  ! Possible overlap of the individual tables is correctly accounted
  ! for (duplicates are discarded).
  !---------------------------------------------------------------------
  type(imbfits_t),       intent(in)    :: imbf      !
  type(imbfits_derot_t), intent(inout) :: allderot  !
  logical,               intent(inout) :: error     !
  ! Local
  character(len=*), parameter :: rname='READ>HEADER>DEROT>ALLINONE'
  integer(kind=4) :: isub,nrows
  type(imbfits_derot_t) :: onederot
  integer(kind=4) :: ifirst,ilast,ofirst,olast
  character(len=comment_length) :: warning
  real(kind=8), allocatable :: mjd(:)
  !
  ! Finding the final number of rows is done in 2 passes (+ 1 pass
  ! for actually filling the output structure). This is unefficient,
  ! but given the size of the derot tables (typically 10 rows per
  ! subscan), the overall impact is probably marginal.
  !
  ! Compute the total number of rows (including duplicates)
  nrows = 0
  do isub=1,imbf%primary%n_obsp%val
    ! Moving to each subscan might be unefficient...
    call imbfits_mvhdu_subscan(isub,hdu_derot,imbf,error)
    if (error)  return
    ! Reading the descriptor is enough here
    call imbfits_read_header_init(imbf%file,onederot%head%desc,error)
    if (error) return
    nrows = nrows+onederot%head%desc%naxis2%val
  enddo
  !
  ! Read all MJDs and find number of non-duplicates
  allocate(mjd(nrows))
  ofirst = 0
  olast = 0
  do isub=1,imbf%primary%n_obsp%val
    call imbfits_mvhdu_subscan(isub,hdu_derot,imbf,error)
    if (error)  return
    call imbfits_read_header_derot_onehdu(imbf,onederot,error)
    if (error)  return
    ifirst = 1
    ilast = onederot%head%desc%naxis2%val
    call compute_nonduplicates(onederot%table%mjd%val,mjd,ifirst,ilast,ofirst,olast)
    if (ifirst.gt.ilast) cycle  ! Nothing useful in this table: next subscan
    mjd(ofirst:olast) = onederot%table%mjd%val(ifirst:ilast)
  enddo
  deallocate(mjd)
  !
  ! Now allocate the output table to the appropriate size
  call reallocate_fits_dble_1d('MJD',olast,allderot%table%mjd,error)
  if (error)  return
  call reallocate_fits_char_1d('SYSTEM',olast,allderot%table%system,error)
  if (error)  return
  call reallocate_fits_real_1d('FWANT',olast,allderot%table%fwant,error)
  if (error)  return
  call reallocate_fits_real_1d('HWANT',olast,allderot%table%hwant,error)
  if (error)  return
  call reallocate_fits_real_1d('SWANT',olast,allderot%table%swant,error)
  if (error)  return
  call reallocate_fits_real_1d('FACT',olast,allderot%table%fact,error)
  if (error)  return
  call reallocate_fits_real_1d('HACT',olast,allderot%table%hact,error)
  if (error)  return
  call reallocate_fits_real_1d('SACT',olast,allderot%table%sact,error)
  if (error)  return
  !
  ! Fill all the HDU in memory
  write(warning,'(A,I0,A)') 'WARNING! ',imbf%primary%n_obsp%val,' subscans merged'
  !
  ofirst = 0
  olast = 0
  ! Loop again to store all the MJDs
  do isub=1,imbf%primary%n_obsp%val
    call imbfits_mvhdu_subscan(isub,hdu_derot,imbf,error)
    if (error)  return
    call imbfits_read_header_derot_onehdu(imbf,onederot,error)
    if (error)  return
    !
    ! Fill part of the header
    if (isub.eq.1) then
      allderot%head%desc = onederot%head%desc
      allderot%head%scannum = onederot%head%scannum
      allderot%head%date_obs = onederot%head%date_obs
      allderot%head%date_obs%comment = warning
      allderot%head%desc%naxis2%comment = warning
      allderot%table%mjd%comment = onederot%table%mjd%comment
      allderot%table%system%comment = onederot%table%system%comment
      allderot%table%fwant%comment = onederot%table%fwant%comment
      allderot%table%hwant%comment = onederot%table%hwant%comment
      allderot%table%swant%comment = onederot%table%swant%comment
      allderot%table%fact%comment = onederot%table%fact%comment
      allderot%table%hact%comment = onederot%table%hact%comment
      allderot%table%sact%comment = onederot%table%sact%comment
    elseif (isub.eq.imbf%primary%n_obsp%val) then
      allderot%head%date_end = onederot%head%date_end
      allderot%head%date_end%comment = warning
    endif
    !
    ifirst = 1
    ilast = onederot%head%desc%naxis2%val
    call compute_nonduplicates(onederot%table%mjd%val,allderot%table%mjd%val,ifirst,ilast,ofirst,olast)
    if (ifirst.gt.ilast) cycle  ! Nothing useful in this table: next subscan
    !
    ! Append the tables
    allderot%head%desc%naxis2%val = olast
    allderot%table%mjd%val(ofirst:olast)    = onederot%table%mjd%val(ifirst:ilast)
    allderot%table%system%val(ofirst:olast) = onederot%table%system%val(ifirst:ilast)
    allderot%table%fwant%val(ofirst:olast)  = onederot%table%fwant%val(ifirst:ilast)
    allderot%table%hwant%val(ofirst:olast)  = onederot%table%hwant%val(ifirst:ilast)
    allderot%table%swant%val(ofirst:olast)  = onederot%table%swant%val(ifirst:ilast)
    allderot%table%fact%val(ofirst:olast)   = onederot%table%fact%val(ifirst:ilast)
    allderot%table%hact%val(ofirst:olast)   = onederot%table%hact%val(ifirst:ilast)
    allderot%table%sact%val(ofirst:olast)   = onederot%table%sact%val(ifirst:ilast)
    !
  enddo
  !
  ! Fill remaining part of the header
  allderot%head%obsnum%val = 0  ! Subscan number. Non-sense here.
  allderot%head%obsnum%comment = warning
  allderot%head%substime%val = 0.  ! Subscan duration. Non-sense here.
  allderot%head%substime%comment = warning
  call imbfits_read_header_isodate2mjd(allderot%head%date_obs,'MJD_BEG',allderot%head%mjd_beg,error)
  if (error) return
  call imbfits_read_header_isodate2mjd(allderot%head%date_end,'MJD_END',allderot%head%mjd_end,error)
  if (error) return
  !
  call imbfits_free_header_derot(onederot,error)
  !
contains
  subroutine compute_nonduplicates(imjd,omjd,ifirst,ilast,ofirst,olast)
    real(kind=8), intent(in) :: imjd(:),omjd(:)
    integer(kind=4), intent(inout) :: ifirst,ilast
    integer(kind=4), intent(inout) :: ofirst,olast
    !
    if (olast.eq.0) then
      ! Nothing yet in the output buffer: copy everything
      ofirst = ifirst
      olast = ilast
    else
      do
        if (ifirst.gt.ilast)  exit  ! Nothing to be kept in this table
        if (imjd(ifirst).gt.omjd(olast))  exit  ! Found a non-duplicate
        ifirst = ifirst+1
      enddo
      ofirst = olast+1
      olast = ofirst+(ilast-ifirst)
    endif
  end subroutine compute_nonduplicates
  !
end subroutine imbfits_read_header_derot_allinone
!
subroutine imbfits_read_header_backdata(imbf,isub,goodonly,bd,error)
  use gbl_message
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_read_header_backdata
  use imbfits_parameters
  !---------------------------------------------------------------------
  ! @ private
  !  Read the current IMBF-backendFOO HDU (header + table, except DATA
  ! column)
  !---------------------------------------------------------------------
  type(imbfits_t),          intent(in)    :: imbf      !
  integer(kind=4),          intent(in)    :: isub      ! Subscan number
  logical,                  intent(in)    :: goodonly  ! Ignore bad data?
  type(imbfits_backdata_t), intent(inout) :: bd        !
  logical,                  intent(inout) :: error     !
  ! Local
  character(len=*), parameter :: rname='READ>HEADER>BACKDATA'
  integer(kind=4) :: nline
  !
  ! First move to the correct HDU
  call imbfits_mvhdu_subscan(isub,hdu_backdata,imbf,error)
  if (error)  return
  !
  ! Header
  call imbfits_read_header_init(imbf%file,bd%head%desc,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'SCANNUM',bd%head%scannum,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'OBSNUM',bd%head%obsnum,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'BASEBAND',bd%head%baseband,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'DATE-OBS',bd%head%date_obs,error)
  if (error) return
  call imbfits_read_header_isodate2mjd(bd%head%date_obs,'MJD_BEG',bd%head%mjd_beg,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'DATE-END',bd%head%date_end,error)
  if (error) return
  call imbfits_read_header_isodate2mjd(bd%head%date_end,'MJD_END',bd%head%mjd_end,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'CHANNELS',bd%head%channels,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'NPHASES',bd%head%nphases,error)
  if (error) return
  call imbfits_read_key(rname,imbf%file,'PHASEONE',bd%head%phaseone,error,default='UNKNOWN')
  if (error) return
  if (imbf%file%isarray .and. imbf%primary%imbftsve%val.lt.2.00d0) then
    ! This format is used by HERA. TSTAMPED is absent, use default 1.0. Warning,
    ! this value is actually backend-dependent!
    call imbfits_read_key(rname,imbf%file,'TSTAMPED',bd%head%tstamped,error,default=1.d0)
  else
    ! No default, error if no such keyword
    call imbfits_read_key(rname,imbf%file,'TSTAMPED',bd%head%tstamped,error)
  endif
  if (error) return
  call imbfits_read_header_done(imbf%file,bd%head%desc,error)
  if (error) return
  !
  ! Table (except data)
  nline = bd%head%desc%naxis2%val
  call imbfits_read_column(rname,imbf%file,'MJD',nline,bd%table%mjd,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'INTEGTIM',nline,bd%table%integtim,error)
  if (error)  return
  call imbfits_read_column(rname,imbf%file,'ISWITCH',nline,bd%table%iswitch,error)
  if (error)  return
  !
  call imbfits_compress_header_backdata(bd,goodonly,error)
  if (error)  return
  !
end subroutine imbfits_read_header_backdata
!
subroutine imbfits_compress_header_backdata(bd,goodonly,error)
  use gbl_message
  use imbfits_dependencies_interfaces
  use imbfits_interfaces, except_this=>imbfits_compress_header_backdata
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(imbfits_backdata_t), intent(inout) :: bd        !
  logical,                  intent(in)    :: goodonly  ! Ignore bad data?
  logical,                  intent(inout) :: error     !
  ! Local
  character(len=*), parameter :: rname='COMPRESS>HEADER>BACKDATA'
  integer(kind=4) :: nline,iline,ier,ngood
  logical, allocatable :: flag(:)
  real(kind=8), allocatable :: bufr8(:)
  integer(kind=4), allocatable :: bufi4(:)
  character(len=message_length) :: mess
  !
  ! Create contiguously numbered fore- and back- pointers
  nline = bd%head%desc%naxis2%val
  call imbfits_make_column('FOREPOIN',nline,'Forward pointer to compressed columns',  &
    bd%table%forepoin,error)
  if (error)  return
  call imbfits_make_column('BACKPOIN',nline,'Backward pointer to uncompressed columns',  &
    bd%table%backpoin,error)
  if (error)  return
  if (.not.goodonly)  return  ! No compression requested
  !
  allocate(flag(nline),stat=ier)
  if (failed_allocate(rname,'FLAG array',ier,error))  return
  !
  ! Compute the flag array, fore-pointer, and new size of arrays
  ngood = 0
  do iline=1,nline
    flag(iline) = bd%table%iswitch%val(iline).gt.0
    if (flag(iline)) then
      ngood = ngood+1
      bd%table%forepoin%val(iline) = ngood
    else
      bd%table%forepoin%val(iline) = 0
    endif
  enddo
  !
  if (ngood.lt.nline) then  ! Compress only if really needed
    write(mess,'(A,I0,A,I0,A)')  &
      'Subscan has ',nline-ngood,'/',nline,' bad time dump(s)'
    call imbfits_message(seve%w,rname,mess)
    ! For efficiency purpose, the buffers are not hidden in
    ! imbfits_compress_column i.e. they are allocated once
    allocate(bufr8(nline),bufi4(nline),stat=ier)
    if (failed_allocate(rname,'buffers',ier,error))  return
    !
    call imbfits_compress_column(bd%table%backpoin,flag,ngood,bufi4,error)
    if (error)  return
    call imbfits_compress_column(bd%table%mjd,flag,ngood,bufr8,error)
    if (error)  return
    call imbfits_compress_column(bd%table%integtim,flag,ngood,bufr8,error)
    if (error)  return
    call imbfits_compress_column(bd%table%iswitch,flag,ngood,bufi4,error)
    if (error)  return
    bd%head%desc%naxis2%val = ngood
    !
    deallocate(bufr8,bufi4)
  endif
  !
  deallocate(flag)
  !
end subroutine imbfits_compress_header_backdata
!
function imbfits_subscan_exists(imbf,sname,iclass)
  use imbfits_interfaces, except_this => imbfits_subscan_exists
  use imbfits_messaging
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ public
  ! Return .true. if the subscan class 'sname' exists. Optionally
  ! return the equivalence class number.
  !---------------------------------------------------------------------
  logical :: imbfits_subscan_exists
  type(imbfits_t),            intent(in)  :: imbf    !
  character(len=*),           intent(in)  :: sname   ! Subscan eclass name
  integer(kind=4),  optional, intent(out) :: iclass  ! Subscan eclass number
  ! Local
  character(len=char0d_length) :: name1,name2
  integer(kind=4) :: jclass
  !
  name1 = sname
  call sic_upper(name1)
  !
  imbfits_subscan_exists = .false.
  do jclass=1,imbf%seclass%nequ
    name2 = imbf%seclass%val(jclass)
    call sic_upper(name2)
    if (name1.eq.name2) then
      imbfits_subscan_exists = .true.
      if (present(iclass))  iclass = jclass
      return
    endif
  enddo
  !
end function imbfits_subscan_exists
!
subroutine imbfits_read_subscan_header(imbf,sname,snum,goodonly,mjdinter,  &
  mjd_shift,subs,error)
  use imbfits_interfaces, except_this => imbfits_read_subscan_header
  use imbfits_messaging
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ public
  ! Given a subscan name and relative number, move to the HDU and read
  ! the subscan headers from file.
  !---------------------------------------------------------------------
  type(imbfits_t),         intent(in)    :: imbf
  character(len=*),        intent(in)    :: sname      ! Subscan name
  integer(kind=4),         intent(in)    :: snum       ! Subscan number
  logical,                 intent(in)    :: goodonly   ! Ignore bad data?
  logical,                 intent(in)    :: mjdinter   ! Compute MJD intersection?
  real(kind=8),            intent(in)    :: mjd_shift  ! [sec]
  type(imbfits_subscan_t), intent(inout) :: subs
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='READ>SUBSCAN>HEADER'
  integer(kind=4) :: iclass,ifound,isub
  logical :: found
  character(len=message_length) :: mess
  !
  ! 1) Find the appropriate class
  if (.not.imbfits_subscan_exists(imbf,sname,iclass)) then
    call imbfits_message(seve%e,rname,'No such subscan '''//trim(sname)//'''')
    error = .true.
    return
  endif
  !
  ! 2) Find the snum-th subscan named 'sname'
  found = .false.
  ifound = 0
  do isub=1,imbf%seclass%nval
    if (imbf%seclass%bak(isub).eq.iclass) then
      ifound = ifound+1
      if (ifound.eq.snum) then
        found = .true.
        exit
      endif
    endif
  enddo
  if (.not.found) then
    write(mess,'(A,A,A,I0)')  'No such subscan ''',trim(sname),''' #',snum
    call imbfits_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
  write(mess,'(A,A,A,I0,A,I0,A)')  &
    'Reading headers of subscan ''',trim(sname),''' #',snum,' (#',isub,' in file)'
  call imbfits_message(iseve%other,rname,mess)
  !
  call imbfits_read_subscan_header_bynum(imbf,isub,goodonly,mjdinter,  &
    mjd_shift,subs,error)
  if (error)  return
  !
end subroutine imbfits_read_subscan_header
!
subroutine imbfits_read_subscan_header_bynum(imbf,isub,goodonly,mjdinter,  &
  mjd_shift,subs,error)
  use gbl_message
  use cfitsio_api
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_read_subscan_header_bynum
  !---------------------------------------------------------------------
  ! @ public
  ! Given a subscan absolute number, move the HDU and read the subscan
  ! headers from file. On return let the CFITSIO pointer to the
  ! IMBF-backdataXXX HDU, such as its DATA column can be read right
  ! after.
  !---------------------------------------------------------------------
  type(imbfits_t),         intent(in)    :: imbf
  integer(kind=4),         intent(in)    :: isub
  logical,                 intent(in)    :: goodonly   ! Ignore bad data?
  logical,                 intent(in)    :: mjdinter   ! Compute MJD intersection?
  real(kind=8),            intent(in)    :: mjd_shift  ! [sec] Antslow MJD shift
  type(imbfits_subscan_t), intent(inout) :: subs
  logical,                 intent(inout) :: error
  !
  ! Save the subscan number asap in the structure, so that we can
  ! understand what happens in case of early exit (e.g. error)
  subs%isub = isub
  !
  ! IMBF-antenna (antslow and antfast currently belong to the same hdu)
  call imbfits_read_header_antslow(imbf,isub,mjd_shift,subs%antslow,error)
  if (error)  return
  call imbfits_read_header_antfast(imbf,isub,subs%antfast,error)
  if (error)  return
  !
  ! IMBF-subreflector
  ! TDB
  !
  ! IMBF-backendFOO. This one MUST be last: on return the CFITSIO
  ! pointer is let to the IMBF-backdataXXX HDU, such as its DATA column
  ! can be read right after.
  call imbfits_read_header_backdata(imbf,isub,goodonly,subs%backdata,error)
  if (error)  return
  !
  ! Post reading
  !
  ! On sky or not on sky? Non on-sky subscans have regular problems with
  ! their antenna position tables.
  if (subs%antslow%head%obstype%val.eq.'calibrate') then
    subs%onsky = subs%antslow%head%substype%val.eq.'calSky'
    if (subs%antslow%head%substype%val.eq.'calGrid') then
      subs%calkind = 2
    else
      subs%calkind = 1
    endif
  else
    subs%calkind = 0
    subs%onsky = .true.
  endif
  !
  if (mjdinter) then
    ! Patch so that the subscan begin-end range is reduced to the
    ! intersection of the tables begin-end
    call imbfits_read_header_fixsubscanrange(subs,error)
    if (error)  return
  endif
  !
end subroutine imbfits_read_subscan_header_bynum
!
subroutine imbfits_read_header_isodate2mjd(timesys,mjd_key,mjd,error)
  use gkernel_types
  use imbfits_dependencies_interfaces
  use imbfits_interfaces, except_this=>imbfits_read_header_isodate2mjd
  !---------------------------------------------------------------------
  ! @ private
  !  Fill a duplicate, memory only, of a ISO date string into a MJD
  ! value.
  !---------------------------------------------------------------------
  type(fits_char_0d_t), intent(in)    :: timesys  !
  character(len=*),     intent(in)    :: mjd_key  ! Key name
  type(fits_dble_0d_t), intent(inout) :: mjd      !
  logical,              intent(inout) :: error    ! Logical error flag
  !
  mjd%comment = 'Duplicate of '//trim(timesys%key)//' (memory only)'
  mjd%key     = mjd_key
  call gag_isodate2mjd(timesys%val,mjd%val,error)
  if (error) return
  !
end subroutine imbfits_read_header_isodate2mjd
!
subroutine imbfits_read_header_fixsubscanrange(subs,error)
  use gbl_message
  use imbfits_interfaces, except_this=>imbfits_read_header_fixsubscanrange
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Fix the subscan on-track range (DATE-OBS to DATE-END) so that it
  ! is set to the intersection with the actual MJD ranges in the tables
  ! of the subscan.
  !  Intersection with Antenna Fast is disconnected for now, as it
  ! appears that we are often missing several fast traces at the end.
  ! But we don't use those traces for now.
  !---------------------------------------------------------------------
  type(imbfits_subscan_t), intent(inout) :: subs
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='MJD>INTERSECTION'
  integer(kind=4), parameter :: mmjd=4
  integer(kind=4), parameter :: ash=1  ! AntSlow Header
! integer(kind=4), parameter :: afh=   ! AntFast Header
  integer(kind=4), parameter :: bdh=2  ! BackData Header
  integer(kind=4), parameter :: ast=3  ! AntSlow Table
! integer(kind=4), parameter :: aft=   ! AntFast Table
  integer(kind=4), parameter :: bdt=4  ! BackData Table
  character(len=32) :: owner(mmjd),reason(mmjd)
  real(kind=8) :: minmjd(mmjd),maxmjd(mmjd)
  integer(kind=4) :: imin,imax,i
  character(len=message_length) :: mess
  !
  owner(ash) = trim(subs%antslow%head%desc%extname%val)//' (slow)'
! owner(afh) = trim(subs%antfast%head%desc%extname%val)//' (fast)'
  owner(bdh) = subs%backdata%head%desc%extname%val
  owner(ast) = trim(subs%antslow%head%desc%extname%val)//' (slow)'
! owner(aft) = trim(subs%antfast%head%desc%extname%val)//' (fast)'
  owner(bdt) = subs%backdata%head%desc%extname%val
  !
  reason(ash) = trim(owner(ash))//' header'
! reason(afh) = trim(owner(afh))//' header'
  reason(bdh) = trim(owner(bdh))//' header'
  reason(ast) = trim(owner(ast))//' MJD column'
! reason(aft) = trim(owner(aft))//' MJD column'
  reason(bdt) = trim(owner(bdt))//' MJD column'
  !
  ! Min MJD from headers
  minmjd(ash) = subs%antslow%head%mjd_beg%val
! minmjd(afh) = subs%antfast%head%mjd_beg%val
  minmjd(bdh) = subs%backdata%head%mjd_beg%val
  ! Min MJD from tables
  if (subs%antslow%table%mjd%n.ge.1) then
    minmjd(ast) = subs%antslow%table%mjd%val(1)
  else
    minmjd(ast) = subs%antslow%head%mjd_beg%val
  endif
! if (subs%antfast%table%mjd%n.ge.1) then
!   minmjd(aft) = subs%antfast%table%mjd%val(1)
! else
!   minmjd(aft) = subs%antfast%head%mjd_beg%val
! endif
  if (subs%backdata%table%mjd%n.ge.1) then
    minmjd(bdt) = subs%backdata%table%mjd%val(1) +  &
                  (0.0d0-subs%backdata%head%tstamped%val) *  &
                  subs%backdata%table%integtim%val(1)/86400.d0
  else
    minmjd(bdt) = subs%backdata%head%mjd_beg%val
  endif
  !
  ! Max MJD from headers
  maxmjd(ash) = subs%antslow%head%mjd_end%val
! maxmjd(afh) = subs%antfast%head%mjd_end%val
  maxmjd(bdh) = subs%backdata%head%mjd_end%val
  ! Max MJD from tables
  if (subs%antslow%table%mjd%n.ge.1) then
    maxmjd(ast) = subs%antslow%table%mjd%val(subs%antslow%table%mjd%n)
  else
    maxmjd(ast) = subs%antslow%head%mjd_end%val
  endif
! if (subs%antfast%table%mjd%n.ge.1) then
!   maxmjd(aft) = subs%antfast%table%mjd%val(subs%antfast%table%mjd%n)
! else
!   maxmjd(aft) = subs%antfast%head%mjd_end%val
! endif
  if (subs%backdata%table%mjd%n.ge.1) then
   maxmjd(bdt) = subs%backdata%table%mjd%val(subs%backdata%table%mjd%n) + &
                  (1.0d0-subs%backdata%head%tstamped%val) *  &
                  subs%backdata%table%integtim%val(subs%backdata%table%mjd%n)/86400.d0
  else
    maxmjd(bdt) = subs%backdata%head%mjd_end%val
  endif
  !
  ! Sanity check. Min should be lower than max...
  do i=1,mmjd
    if (minmjd(i).gt.maxmjd(i)) then
      write(mess,'(A,A,A,F0.6,A,F0.6,A)')  'MJD-min greater than MJD-max in ',  &
        trim(reason(i)),' (',minmjd(i),' > ',maxmjd(i),')'
      call imbfits_message(seve%e,rname,mess)
      error = .true.
      continue
    endif
  enddo
  if (error)  return
  !
  ! Now actual job
  imin = maxloc(minmjd,1)  ! Largest min mjd
  imax = minloc(maxmjd,1)  ! Smallest max mjd
  !
  if (minmjd(imin).gt.maxmjd(imax)) then
    write(mess,'(A,F0.1,A)')  'MJD ranges do not intersect (distance ',  &
      (minmjd(imin)-maxmjd(imax))*86400d0,' seconds)'
    call imbfits_message(seve%e,rname,mess)
    call imbfits_message(seve%e,rname,  &
      'min MJD in '//trim(reason(imin))//' > max MJD in '//reason(imax))
    error = .true.
    return
  endif
  !
  ! Patch headers if needed
  if (minmjd(ash).lt.minmjd(imin)) then
    call patch_and_warn(owner(ash),reason(imin),minmjd(imin),  &
      subs%antslow%head%mjd_beg,subs%antslow%head%date_obs,error)
  endif
! if (minmjd(afh).lt.minmjd(imin)) then
!   call patch_and_warn(owner(afh),reason(imin),minmjd(imin),  &
!     subs%antfast%head%mjd_beg,subs%antfast%head%date_obs,error)
! endif
  if (minmjd(bdh).lt.minmjd(imin)) then
    call patch_and_warn(owner(bdh),reason(imin),minmjd(imin),  &
      subs%backdata%head%mjd_beg,subs%backdata%head%date_obs,error)
  endif
  !
  if (maxmjd(ash).gt.maxmjd(imax)) then
    call patch_and_warn(owner(ash),reason(imax),maxmjd(imax),  &
      subs%antslow%head%mjd_end,subs%antslow%head%date_end,error)
  endif
! if (maxmjd(afh).gt.maxmjd(imax)) then
!   call patch_and_warn(owner(afh),reason(imax),maxmjd(imax),  &
!     subs%antfast%head%mjd_end,subs%antfast%head%date_end,error)
! endif
  if (maxmjd(bdh).gt.maxmjd(imax)) then
    call patch_and_warn(owner(bdh),reason(imax),maxmjd(imax),  &
      subs%backdata%head%mjd_end,subs%backdata%head%date_end,error)
  endif
  !
contains
  subroutine patch_and_warn(owner,reason,newmjd,mjd,iso,error)
    use gbl_message
    use gkernel_types
    character(len=*),     intent(in)    :: owner   !
    character(len=*),     intent(in)    :: reason  !
    real(kind=8),         intent(in)    :: newmjd  ! [MJD]
    type(fits_dble_0d_t), intent(inout) :: mjd     ! [MJD]
    type(fits_char_0d_t), intent(inout) :: iso     ! [ISODATE]
    logical,              intent(inout) :: error   !
    ! Local
    character(len=message_length) :: mess
    !
    ! Shift displayed positive or negative, on purpose for clarity
    write(mess,'(A,SP,F0.2,A,A)') 'patched by ',  &
      (newmjd-mjd%val)*86400,' seconds because of ',reason
    !
    mjd%val     = newmjd
  ! mjd%key     = unchanged
  ! mjd%comment = unchanged
    !
    call gag_mjd2isodate(mjd%val,iso%val,error)
    if (error) return
  ! iso%key     = unchanged
    iso%comment = mess
    !
    call imbfits_message(seve%w,'MJD',  &
      trim(owner)//' '//trim(iso%key)//' '//mess)
  end subroutine patch_and_warn
  !
end subroutine imbfits_read_header_fixsubscanrange
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine imbfits_free_header_primary(primary,error)
  use imbfits_interfaces, except_this => imbfits_free_header_primary
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  ! Free the Primary HDU in memory
  !---------------------------------------------------------------------
  type(imbfits_primary_t), intent(inout) :: primary
  logical,                 intent(inout) :: error
  !
  ! Scalar elements:
  !  => nothing to be freed.
  ! Array elements:
  !  => none
  !
end subroutine imbfits_free_header_primary
!
subroutine imbfits_free_header_scan(scan,error)
  use imbfits_interfaces, except_this=>imbfits_free_header_scan
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  ! Free the IMBF-scan HDU
  !---------------------------------------------------------------------
  type(imbfits_scan_t), intent(inout) :: scan
  logical,              intent(inout) :: error
  ! Local
  logical :: error2
  !
  error2 = .false.
  !
  ! Free header
  call imbfits_free_header_init(scan%head%desc,error2)
  if (error2)  error = .true.
  !
  ! Scalar elements:
  !  => nothing to be freed
  !
  ! Array elements:
  call imbfits_free_column(scan%table%sysoff,error2)
  if (error2)  error = .true.
  call imbfits_free_column(scan%table%xoffset,error2)
  if (error2)  error = .true.
  call imbfits_free_column(scan%table%yoffset,error2)
  if (error2)  error = .true.
  !
end subroutine imbfits_free_header_scan
!
subroutine imbfits_free_header_frontend(front,error)
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_free_header_frontend
  !---------------------------------------------------------------------
  ! @ private
  ! Free the IMBF-frontend HDU
  !---------------------------------------------------------------------
  type(imbfits_front_t), intent(inout) :: front
  logical,               intent(inout) :: error
  ! Local
  logical :: error2
  !
  error2 = .false.
  !
  ! Free header
  call imbfits_free_header_init(front%head%desc,error2)
  if (error2)  error = .true.
  !
  ! Scalar elements:
  !  => nothing to be freed
  !
  ! Array elements:
  !
  ! Frequency axis
  call imbfits_free_column(front%table%recname,error2)
  if (error2)  error = .true.
  call imbfits_free_column(front%table%linename,error2)
  if (error2)  error = .true.
  call imbfits_free_column(front%table%restfreq,error2)
  if (error2)  error = .true.
  ! Calibration
  call imbfits_free_column(front%table%beameff,error2)
  if (error2)  error = .true.
  call imbfits_free_column(front%table%etafss,error2)
  if (error2)  error = .true.
  call imbfits_free_column(front%table%gainimag,error2)
  if (error2)  error = .true.
  call imbfits_free_column(front%table%sideband,error2)
  if (error2)  error = .true.
  call imbfits_free_column(front%table%sbsep,error2)
  if (error2)  error = .true.
  call imbfits_free_column(front%table%widenar,error2)
  if (error2)  error = .true.
  call imbfits_free_column(front%table%tcold,error2)
  if (error2)  error = .true.
  call imbfits_free_column(front%table%thot,error2)
  if (error2)  error = .true.
  ! Pixel description
  call imbfits_free_column(front%table%ifeed,error2)
  if (error2)  error = .true.
  call imbfits_free_column(front%table%nofeeds,error2)
  if (error2)  error = .true.
  call imbfits_free_column(front%table%pola,error2)
  if (error2)  error = .true.
  ! Frequency axis
  call imbfits_free_column(front%table%doppler,error2)
  if (error2)  error = .true.
  call imbfits_free_column(front%table%ifcenter,error2)
  if (error2)  error = .true.
  call imbfits_free_column(front%table%bandwid,error2)
  if (error2)  error = .true.
  call imbfits_free_column(front%table%ifflipps,error2)
  if (error2)  error = .true.
  call imbfits_free_column(front%table%speclo,error2)
  if (error2)  error = .true.
  ! Brightness axis
  call imbfits_free_column(front%table%tscale,error2)
  if (error2)  error = .true.
  ! Frequency switching
  call imbfits_free_column(front%table%frqthrow,error2)
  if (error2)  error = .true.
  call imbfits_free_column(front%table%frqoff1,error2)
  if (error2)  error = .true.
  call imbfits_free_column(front%table%frqoff2,error2)
  if (error2)  error = .true.
  !
end subroutine imbfits_free_header_frontend
!
subroutine imbfits_free_header_backend(back,error)
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_free_header_backend
  !---------------------------------------------------------------------
  ! @ private
  ! Free the IMBF-backend HDU
  !---------------------------------------------------------------------
  type(imbfits_back_t), intent(inout) :: back
  logical,              intent(inout) :: error
  ! Local
  logical :: error2
  !
  error2 = .false.
  !
  ! Free header
  call imbfits_free_header_init(back%head%desc,error2)
  if (error2)  error = .true.
  !
  ! Scalar elements:
  !  => nothing to be freed
  !
  ! Array elements:
  call imbfits_free_column(back%table%part,error2)
  if (error2)  error = .true.
  call imbfits_free_column(back%table%refchan,error2)
  if (error2)  error = .true.
  call imbfits_free_column(back%table%chans,error2)
  if (error2)  error = .true.
  call imbfits_free_column(back%table%dropped,error2)
  if (error2)  error = .true.
  call imbfits_free_column(back%table%used,error2)
  if (error2)  error = .true.
  call imbfits_free_column(back%table%receiver,error2)
  if (error2)  error = .true.
  call imbfits_free_column(back%table%ifront,error2)
  if (error2)  error = .true.
  call imbfits_free_column(back%table%band,error2)
  if (error2)  error = .true.
  call imbfits_free_column(back%table%polar,error2)
  if (error2)  error = .true.
  call imbfits_free_column(back%table%pixel,error2)
  if (error2)  error = .true.
  call imbfits_free_column(back%table%reffreq,error2)
  if (error2)  error = .true.
  call imbfits_free_column(back%table%spacing,error2)
  if (error2)  error = .true.
  call imbfits_free_column(back%table%linename,error2)
  if (error2)  error = .true.
  call imbfits_free_column(back%table%dataflip,error2)
  if (error2)  error = .true.
  call imbfits_free_column(back%table%frontend,error2)
  if (error2)  error = .true.
  !
  call imbfits_free_back_chunks(back%chunks,error2)
  !
end subroutine imbfits_free_header_backend
!
subroutine imbfits_free_header_antslow(antslow,error)
  use imbfits_types
  use imbfits_interfaces, except_this=>imbfits_free_header_antslow
  !---------------------------------------------------------------------
  ! @ public
  ! Free the IMBF-antenna HDU
  !---------------------------------------------------------------------
  type(imbfits_antslow_t), intent(inout) :: antslow  !
  logical,                 intent(inout) :: error    !
  ! Local
  logical :: error2
  !
  error2 = .false.
  !
  ! Free header
  call imbfits_free_header_init(antslow%head%desc,error2)
  if (error2)  error = .true.
  !
  ! Scalar elements:
  !  => nothing to be freed
  !
  ! Array elements:
  call imbfits_free_column(antslow%table%mjd,error2)
  if (error2)  error = .true.
  call imbfits_free_column(antslow%table%lst,error2)
  if (error2)  error = .true.
  call imbfits_free_column(antslow%table%longoff,error2)
  if (error2)  error = .true.
  call imbfits_free_column(antslow%table%latoff,error2)
  if (error2)  error = .true.
  call imbfits_free_column(antslow%table%cazimuth,error2)
  if (error2)  error = .true.
  call imbfits_free_column(antslow%table%celevatio,error2)
  if (error2)  error = .true.
  call imbfits_free_column(antslow%table%traceflag,error2)
  if (error2)  error = .true.
  !
end subroutine imbfits_free_header_antslow
!
subroutine imbfits_free_header_antfast(antfast,error)
  use imbfits_interfaces, except_this => imbfits_free_header_antfast
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  ! Free the IMBF-antenna HDU
  !---------------------------------------------------------------------
  type(imbfits_antfast_t), intent(inout) :: antfast  !
  logical,                 intent(inout) :: error    !
  ! Local
  logical :: error2
  !
  error2 = .false.
  !
  ! Free header
  call imbfits_free_header_init(antfast%head%desc,error2)
  if (error2)  error = .true.
  !
  ! Scalar elements:
  !  => nothing to be freed
  !
  ! Array elements:
  call imbfits_free_column(antfast%table%mjd,error2)
  if (error2)  error = .true.
  call imbfits_free_column(antfast%table%azimuth,error2)
  if (error2)  error = .true.
  call imbfits_free_column(antfast%table%elevation,error2)
  if (error2)  error = .true.
  call imbfits_free_column(antfast%table%tracking_az,error2)
  if (error2)  error = .true.
  call imbfits_free_column(antfast%table%tracking_el,error2)
  if (error2)  error = .true.
  !
end subroutine imbfits_free_header_antfast
!
subroutine imbfits_free_header_derot(derot,error)
  use imbfits_types
  use imbfits_interfaces, except_this=>imbfits_free_header_derot
  !---------------------------------------------------------------------
  ! @ private
  ! Free the IMBF-xxx-derot HDU
  !---------------------------------------------------------------------
  type(imbfits_derot_t), intent(inout) :: derot  !
  logical,               intent(inout) :: error  !
  ! Local
  logical :: error2
  !
  error2 = .false.
  !
  ! Free header
  call imbfits_free_header_init(derot%head%desc,error2)
  if (error2)  error = .true.
  !
  ! Scalar elements:
  !  => nothing to be freed
  ! Derotator table is optional in IMB-FITS. Nullify header values so that
  ! there are no ambiguities from one file to another.
  derot%head%scannum%val = 0
  derot%head%obsnum%val = 0
  derot%head%date_obs%val = ''
  derot%head%mjd_beg%val = 0.d0
  derot%head%date_end%val = ''
  derot%head%mjd_end%val = 0.d0
  derot%head%substime%val = 0.d0
  !
  ! Array elements:
  call imbfits_free_column(derot%table%mjd,error2)
  if (error2)  error = .true.
  call imbfits_free_column(derot%table%system,error2)
  if (error2)  error = .true.
  call imbfits_free_column(derot%table%fwant,error2)
  if (error2)  error = .true.
  call imbfits_free_column(derot%table%hwant,error2)
  if (error2)  error = .true.
  call imbfits_free_column(derot%table%swant,error2)
  if (error2)  error = .true.
  call imbfits_free_column(derot%table%fact,error2)
  if (error2)  error = .true.
  call imbfits_free_column(derot%table%hact,error2)
  if (error2)  error = .true.
  call imbfits_free_column(derot%table%sact,error2)
  if (error2)  error = .true.
  !
end subroutine imbfits_free_header_derot
!
subroutine imbfits_free_header_backdata(bd,error)
  use imbfits_interfaces, except_this=>imbfits_free_header_backdata
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Free the IMBF-backendFOO HDU (header + table, except DATA
  ! column)
  !---------------------------------------------------------------------
  type(imbfits_backdata_t), intent(inout) :: bd     !
  logical,                  intent(inout) :: error  !
  ! Local
  logical :: error2
  !
  error2 = .false.
  !
  ! Free header
  call imbfits_free_header_init(bd%head%desc,error2)
  if (error2)  error = .true.
  !
  ! Scalar elements:
  !  => nothing to be freed
  !
  ! Array elements: table (except DATA)
  call imbfits_free_column(bd%table%mjd,error2)
  if (error2)  error = .true.
  call imbfits_free_column(bd%table%integtim,error2)
  if (error2)  error = .true.
  call imbfits_free_column(bd%table%iswitch,error2)
  if (error2)  error = .true.
  call imbfits_free_column(bd%table%forepoin,error2)
  if (error2)  error = .true.
  call imbfits_free_column(bd%table%backpoin,error2)
  if (error2)  error = .true.
  !
end subroutine imbfits_free_header_backdata
!
subroutine imbfits_free_subscan_header(subs,error)
  use imbfits_interfaces, except_this=>imbfits_free_subscan_header
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ public
  ! Free subscan data (except DATA column)
  !---------------------------------------------------------------------
  type(imbfits_subscan_t), intent(inout) :: subs
  logical,                 intent(inout) :: error
  !
  ! IMBF-antenna (antslow and antfast currently belong to the same hdu)
  call imbfits_free_header_antslow(subs%antslow,error)
  if (error)  return
  call imbfits_free_header_antfast(subs%antfast,error)
  if (error)  return
  !
  ! IMBF-subreflector
  ! TDB
  !
  ! IMBF-backendFOO. This one MUST be last: on return the CFITSIO
  ! pointer is let to the IMBF-backdataXXX HDU, such as its DATA column
  ! can be read right after.
  call imbfits_free_header_backdata(subs%backdata,error)
  if (error)  return
  !
end subroutine imbfits_free_subscan_header
