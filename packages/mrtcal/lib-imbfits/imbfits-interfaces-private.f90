module imbfits_interfaces_private
  interface
    subroutine reallocate_fits_logi_1d(kind,n,fitsarray,error)
      use imbfits_messaging
      use imbfits_types
      !-------------------------------------------------------------------
      ! @ private
      !-------------------------------------------------------------------
      character(len=*),     intent(in)    :: kind
      integer(kind=4),      intent(in)    :: n
      type(fits_logi_1d_t), intent(inout) :: fitsarray
      logical,              intent(inout) :: error
    end subroutine reallocate_fits_logi_1d
  end interface
  !
  interface
    subroutine reallocate_fits_inte_1d(kind,n,fitsarray,error)
      use imbfits_messaging
      use imbfits_types
      !-------------------------------------------------------------------
      ! @ private
      !-------------------------------------------------------------------
      character(len=*),     intent(in)    :: kind
      integer(kind=4),      intent(in)    :: n
      type(fits_inte_1d_t), intent(inout) :: fitsarray
      logical,              intent(inout) :: error
    end subroutine reallocate_fits_inte_1d
  end interface
  !
  interface
    subroutine reallocate_fits_real_1d(kind,n,fitsarray,error)
      use imbfits_messaging
      use imbfits_types
      !-------------------------------------------------------------------
      ! @ private
      !-------------------------------------------------------------------
      character(len=*),     intent(in)    :: kind
      integer(kind=4),      intent(in)    :: n
      type(fits_real_1d_t), intent(inout) :: fitsarray
      logical,              intent(inout) :: error
    end subroutine reallocate_fits_real_1d
  end interface
  !
  interface
    subroutine reallocate_fits_dble_1d(kind,n,fitsarray,error)
      use imbfits_messaging
      use imbfits_types
      !-------------------------------------------------------------------
      ! @ private
      !-------------------------------------------------------------------
      character(len=*),     intent(in)    :: kind
      integer(kind=4),      intent(in)    :: n
      type(fits_dble_1d_t), intent(inout) :: fitsarray
      logical,              intent(inout) :: error
    end subroutine reallocate_fits_dble_1d
  end interface
  !
  interface
    subroutine reallocate_fits_char_1d(kind,n,fitsarray,error)
      use imbfits_messaging
      use imbfits_types
      !-------------------------------------------------------------------
      ! @ private
      !-------------------------------------------------------------------
      character(len=*),     intent(in)    :: kind
      integer(kind=4),      intent(in)    :: n
      type(fits_char_1d_t), intent(inout) :: fitsarray
      logical,              intent(inout) :: error
    end subroutine reallocate_fits_char_1d
  end interface
  !
  interface
    subroutine reallocate_imbfits_data_val(nchan,npix,ntime,imbdata,error)
      use imbfits_messaging
      use imbfits_types
      !-------------------------------------------------------------------
      ! @ private
      ! (re)allocation routine specific for the DATA column
      !-------------------------------------------------------------------
      integer(kind=4),      intent(in)    :: nchan
      integer(kind=4),      intent(in)    :: npix
      integer(kind=4),      intent(in)    :: ntime
      type(imbfits_data_t), intent(inout) :: imbdata
      logical,              intent(inout) :: error
    end subroutine reallocate_imbfits_data_val
  end interface
  !
  interface
    subroutine free_fits_logi_1d(fitsarray,error)
      use gbl_message
      use imbfits_types
      !-------------------------------------------------------------------
      ! @ private
      !-------------------------------------------------------------------
      type(fits_logi_1d_t), intent(inout) :: fitsarray
      logical,              intent(inout) :: error
    end subroutine free_fits_logi_1d
  end interface
  !
  interface
    subroutine free_fits_inte_1d(fitsarray,error)
      use gbl_message
      use imbfits_types
      !-------------------------------------------------------------------
      ! @ private
      !-------------------------------------------------------------------
      type(fits_inte_1d_t), intent(inout) :: fitsarray
      logical,              intent(inout) :: error
    end subroutine free_fits_inte_1d
  end interface
  !
  interface
    subroutine free_fits_real_1d(fitsarray,error)
      use gbl_message
      use imbfits_types
      !-------------------------------------------------------------------
      ! @ private
      !-------------------------------------------------------------------
      type(fits_real_1d_t), intent(inout) :: fitsarray
      logical,              intent(inout) :: error
    end subroutine free_fits_real_1d
  end interface
  !
  interface
    subroutine free_fits_dble_1d(fitsarray,error)
      use gbl_message
      use imbfits_types
      !-------------------------------------------------------------------
      ! @ private
      !-------------------------------------------------------------------
      type(fits_dble_1d_t), intent(inout) :: fitsarray
      logical,              intent(inout) :: error
    end subroutine free_fits_dble_1d
  end interface
  !
  interface
    subroutine free_fits_char_1d(fitsarray,error)
      use gbl_message
      use imbfits_types
      !-------------------------------------------------------------------
      ! @ private
      !-------------------------------------------------------------------
      type(fits_char_1d_t), intent(inout) :: fitsarray
      logical,              intent(inout) :: error
    end subroutine free_fits_char_1d
  end interface
  !
  interface
    subroutine free_imbfits_data_val(imbdata,error)
      use gbl_message
      use imbfits_types
      !-------------------------------------------------------------------
      ! @ private
      ! Free the 'val' array of the input imbfits_data_t instance.
      !-------------------------------------------------------------------
      type(imbfits_data_t), intent(inout) :: imbdata
      logical,              intent(inout) :: error
    end subroutine free_imbfits_data_val
  end interface
  !
  interface
    subroutine imbfits_obstype_equal(filename,imbf,obstype,equal,error)
      use gbl_message
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Read the bare minimum as reading all the main hdus may take forever
      ! on large imbfitsfile.
      ! This could be a generic subroutine imbfits_equal. *** JP
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: filename
      type(imbfits_t),  intent(inout) :: imbf
      character(len=*), intent(in)    :: obstype
      logical,          intent(out)   :: equal
      logical,          intent(inout) :: error
    end subroutine imbfits_obstype_equal
  end interface
  !
  interface
    subroutine imbfits_check_version(version,error)
      use gbl_message
      use imbfits_parameters
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      real(kind=8), intent(in)    :: version
      logical,      intent(inout) :: error
    end subroutine imbfits_check_version
  end interface
  !
  interface
    subroutine imbfits_check_header_primary(file,primary,error)
      use gbl_message
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(imbfits_file_t),    intent(in)    :: file
      type(imbfits_primary_t), intent(in)    :: primary
      logical,                 intent(inout) :: error
    end subroutine imbfits_check_header_primary
  end interface
  !
  interface
    subroutine imbfits_check_header_scan(primary,scan,error)
      use gbl_message
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Check the consistency of the IMBF-scan HDU
      !---------------------------------------------------------------------
      type(imbfits_primary_t), intent(in)    :: primary
      type(imbfits_scan_t),    intent(in)    :: scan
      logical,                 intent(inout) :: error
    end subroutine imbfits_check_header_scan
  end interface
  !
  interface
    subroutine imbfits_check_header_backend(back,error)
      use gbl_message
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Check the consistency of the IMBF-backend HDU
      ! *** JP Nothing done right now because previous test was too much
      !        restrictive. It is unclear whether this function is needed.
      !        Leave it for the time being.
      !---------------------------------------------------------------------
      type(imbfits_back_t), intent(in)    :: back
      logical,              intent(inout) :: error
    end subroutine imbfits_check_header_backend
  end interface
  !
  interface
    subroutine imbfits_dump_summary_seclass(seclass,olun,error)
      use gildas_def
      use gbl_message
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Loop over all the equivalence classes and give some details about
      ! them
      !---------------------------------------------------------------------
      type(eclass_char_t), intent(in)    :: seclass
      integer(kind=4),     intent(in)    :: olun     ! Output LUN
      logical,             intent(inout) :: error
    end subroutine imbfits_dump_summary_seclass
  end interface
  !
  interface
    subroutine imbfits_dump_file(file,olun,error)
      use gbl_message
      use gildas_def
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(imbfits_file_t), intent(in)    :: file
      integer(kind=4),      intent(in)    :: olun   ! Output LUN
      logical,              intent(inout) :: error
    end subroutine imbfits_dump_file
  end interface
  !
  interface
    subroutine imbfits_dump_subscans_list(imbf,olun,error)
      use gildas_def
      use gbl_message
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Loop over all the subscans and give some details about them
      !---------------------------------------------------------------------
      type(imbfits_t), intent(inout) :: imbf
      integer(kind=4), intent(in)    :: olun   ! Output LUN
      logical,         intent(inout) :: error
    end subroutine imbfits_dump_subscans_list
  end interface
  !
  interface
    subroutine imbfits_dump_header(desc,olun,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! dumps the content on the common header elements found in the
      ! input 'imbfits_header_t'
      !---------------------------------------------------------------------
      type(imbfits_header_t), intent(in)    :: desc
      integer(kind=4),        intent(in)    :: olun   ! Output LUN
      logical,                intent(inout) :: error
    end subroutine imbfits_dump_header
  end interface
  !
  interface
    subroutine imbfits_dump_scan_column(scan,column,olun,error)
      use imbfits_parameters
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(imbfits_scan_t), intent(in)    :: scan
      character(len=*),     intent(in)    :: column  ! Column name
      integer(kind=4),      intent(in)    :: olun    ! Output LUN
      logical,              intent(inout) :: error
    end subroutine imbfits_dump_scan_column
  end interface
  !
  interface
    subroutine imbfits_dump_frontend_column(front,column,olun,error)
      use imbfits_parameters
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(imbfits_front_t), intent(in)    :: front
      character(len=*),      intent(in)    :: column  ! Column name
      integer(kind=4),       intent(in)    :: olun    ! Output LUN
      logical,               intent(inout) :: error
    end subroutine imbfits_dump_frontend_column
  end interface
  !
  interface
    subroutine imbfits_dump_backend_column(back,column,olun,error)
      use imbfits_parameters
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(imbfits_back_t), intent(in)    :: back
      character(len=*),     intent(in)    :: column  ! Column name
      integer(kind=4),      intent(in)    :: olun    ! Output LUN
      logical,              intent(inout) :: error
    end subroutine imbfits_dump_backend_column
  end interface
  !
  interface
    subroutine imbfits_dump_backdata_column(backdata,column,olun,error)
      use imbfits_parameters
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(imbfits_backdata_t), intent(in)    :: backdata
      character(len=*),         intent(in)    :: column    ! Column name
      integer(kind=4),          intent(in)    :: olun      ! Output LUN
      logical,                  intent(inout) :: error
    end subroutine imbfits_dump_backdata_column
  end interface
  !
  interface
    subroutine imbfits_dump_antslow_column(antslow,column,olun,error)
      use imbfits_parameters
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(imbfits_antslow_t), intent(in)    :: antslow
      character(len=*),        intent(in)    :: column   ! Column name
      integer(kind=4),         intent(in)    :: olun     ! Output LUN
      logical,                 intent(inout) :: error
    end subroutine imbfits_dump_antslow_column
  end interface
  !
  interface
    subroutine imbfits_dump_antfast_column(antfast,column,olun,error)
      use imbfits_parameters
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(imbfits_antfast_t), intent(in)    :: antfast
      character(len=*),        intent(in)    :: column   ! Column name
      integer(kind=4),         intent(in)    :: olun     ! Output LUN
      logical,                 intent(inout) :: error
    end subroutine imbfits_dump_antfast_column
  end interface
  !
  interface
    subroutine imbfits_dump_derot_column(derot,column,olun,error)
      use imbfits_parameters
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(imbfits_derot_t), intent(in)    :: derot
      character(len=*),      intent(in)    :: column   ! Column name
      integer(kind=4),       intent(in)    :: olun     ! Output LUN
      logical,               intent(inout) :: error
    end subroutine imbfits_dump_derot_column
  end interface
  !
  interface
    subroutine imbfits_message(mkind,procname,message)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Messaging facility for the current library. Calls the low-level
      ! (internal) messaging routine with its own identifier.
      !---------------------------------------------------------------------
      integer,          intent(in) :: mkind     ! Message kind
      character(len=*), intent(in) :: procname  ! Name of calling procedure
      character(len=*), intent(in) :: message   ! Message string
    end subroutine imbfits_message
  end interface
  !
  interface
    subroutine imbfits_reopen_file(filename,file,hdus,error)
      use gildas_def
      use gbl_message
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Open, if needed, the given file. Close of needed any previous file
      ! (if different).
      !---------------------------------------------------------------------
      character(len=*),     intent(in)    :: filename
      type(imbfits_file_t), intent(inout) :: file
      type(imbfits_hdus_t), intent(inout) :: hdus
      logical,              intent(inout) :: error
    end subroutine imbfits_reopen_file
  end interface
  !
  interface
    subroutine imbfits_check_leadhdus(imbf,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Check the 4 first HDUs
      !---------------------------------------------------------------------
      type(imbfits_t),  intent(inout) :: imbf      !
      logical,          intent(inout) :: error     !
    end subroutine imbfits_check_leadhdus
  end interface
  !
  interface
    subroutine imbfits_read_subscan_eclass(imbf,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Run through all the subscans headers and build equivalences classes
      ! for all of them
      !---------------------------------------------------------------------
      type(imbfits_t), intent(inout) :: imbf
      logical,         intent(inout) :: error
    end subroutine imbfits_read_subscan_eclass
  end interface
  !
  interface
    subroutine imbfits_read_header_primary(file,primary,error)
      use gbl_message
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Read the Primary HDU
      !---------------------------------------------------------------------
      type(imbfits_file_t),    intent(in)    :: file
      type(imbfits_primary_t), intent(inout) :: primary
      logical,                 intent(inout) :: error
    end subroutine imbfits_read_header_primary
  end interface
  !
  interface
    subroutine imbfits_read_header_scan(imbf,scan,error)
      use gbl_message
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Read the IMBF-scan HDU
      !---------------------------------------------------------------------
      type(imbfits_t),      intent(in)    :: imbf
      type(imbfits_scan_t), intent(inout) :: scan
      logical,              intent(inout) :: error
    end subroutine imbfits_read_header_scan
  end interface
  !
  interface
    subroutine imbfits_read_header_frontend(file,front,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Read the IMBF-frontend HDU
      !---------------------------------------------------------------------
      type(imbfits_file_t),  intent(in)    :: file
      type(imbfits_front_t), intent(inout) :: front
      logical,               intent(inout) :: error
    end subroutine imbfits_read_header_frontend
  end interface
  !
  interface
    subroutine imbfits_read_header_backend(imbf,bandwidth,error)
      use gbl_message
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Read the IMBF-backend HDU
      !---------------------------------------------------------------------
      type(imbfits_t), intent(inout), target :: imbf
      real(kind=4),    intent(in)            :: bandwidth  ! IMBF-backend slicing width (0 = no slicing)
      logical,         intent(inout)         :: error
    end subroutine imbfits_read_header_backend
  end interface
  !
  interface
    subroutine imbfits_read_header_backend_patchlinename(front,back,error)
      use gbl_message
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Fill the column imbf%back%table%linename
      !  from the column imbf%front%table%linename
      ! This is generic for any version of IMBFITS, although latest versions
      ! provides themselves the 2 columns
      !---------------------------------------------------------------------
      type(imbfits_front_t), intent(in)    :: front  !
      type(imbfits_back_t),  intent(inout) :: back   !
      logical,               intent(inout) :: error  !
    end subroutine imbfits_read_header_backend_patchlinename
  end interface
  !
  interface
    subroutine imbfits_read_header_backend_patchrefchan(front,back,error)
      use gbl_message
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Recompute correct REFCHAN.
      !  This code is generic but is needed only for a few cases.
      !---------------------------------------------------------------------
      type(imbfits_front_t), intent(in)    :: front  !
      type(imbfits_back_t),  intent(inout) :: back   !
      logical,               intent(inout) :: error  !
    end subroutine imbfits_read_header_backend_patchrefchan
  end interface
  !
  interface
    subroutine imbfits_read_header_backend_frontend(back,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create a custom column FRONTEND. It provides the frontend name as
      ! used in the Class telescope name
      !---------------------------------------------------------------------
      type(imbfits_back_t),  intent(inout) :: back   !
      logical,               intent(inout) :: error  !
    end subroutine imbfits_read_header_backend_frontend
  end interface
  !
  interface
    subroutine imbfits_read_header_backend_ifront(front,back,error)
      use gbl_message
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create a custom column IFRONT. It provides a backpointer from
      ! backend RECEIVER column to frontend RECNAME column.
      !---------------------------------------------------------------------
      type(imbfits_front_t), intent(in)    :: front  !
      type(imbfits_back_t),  intent(inout) :: back   !
      logical,               intent(inout) :: error  !
    end subroutine imbfits_read_header_backend_ifront
  end interface
  !
  interface
    subroutine imbfits_read_header_backend_dataflip(back,error)
      use gbl_message
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create a custom column DATAFLIP. It provides a logical indicating
      ! if the data sign should be flipped or not at read time. As of today,
      ! this is used only in the context of polarimetry, but this is generic
      ! enough for other use cases if needed.
      !---------------------------------------------------------------------
      type(imbfits_back_t),  intent(inout) :: back   !
      logical,               intent(inout) :: error  !
    end subroutine imbfits_read_header_backend_dataflip
  end interface
  !
  interface
    subroutine imbfits_read_header_backend_ceclass(back,error)
      use gbl_message
      use gkernel_types
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Compute the Chunks Equivalence CLASSes, i.e. find all the families
      ! (each one defines a chunkset) with unique (PART,PIXEL,RECEIVER,
      ! POLAR). This is computed only once per backend table i.e. once per
      ! scan (=> low impact for the whole scan)
      !---------------------------------------------------------------------
      type(imbfits_back_t), intent(inout) :: back   !
      logical,              intent(inout) :: error  !
    end subroutine imbfits_read_header_backend_ceclass
  end interface
  !
  interface
    subroutine imbfits_read_header_antslow_mjd_shift(antslow,mjd_shift,error)
      use gbl_message
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Patch the MJD column for the given shift (positive or negative, but
      ! usually zero).
      !---------------------------------------------------------------------
      type(imbfits_antslow_t), intent(inout) :: antslow
      real(kind=8),            intent(in)    :: mjd_shift  ! [sec]
      logical,                 intent(inout) :: error
    end subroutine imbfits_read_header_antslow_mjd_shift
  end interface
  !
  interface
    subroutine imbfits_read_header_antfast(imbf,isub,antfast,error)
      use gbl_message
      use cfitsio_api
      use imbfits_types
      use imbfits_parameters
      !---------------------------------------------------------------------
      ! @ private
      ! Read the IMBF-antenna HDU
      !---------------------------------------------------------------------
      type(imbfits_t),         intent(in)    :: imbf     !
      integer(kind=4),         intent(in)    :: isub     ! Subscan number
      type(imbfits_antfast_t), intent(inout) :: antfast  !
      logical,                 intent(inout) :: error    !
    end subroutine imbfits_read_header_antfast
  end interface
  !
  interface
    subroutine imbfits_read_header_backdata(imbf,isub,goodonly,bd,error)
      use gbl_message
      use imbfits_types
      use imbfits_parameters
      !---------------------------------------------------------------------
      ! @ private
      !  Read the current IMBF-backendFOO HDU (header + table, except DATA
      ! column)
      !---------------------------------------------------------------------
      type(imbfits_t),          intent(in)    :: imbf      !
      integer(kind=4),          intent(in)    :: isub      ! Subscan number
      logical,                  intent(in)    :: goodonly  ! Ignore bad data?
      type(imbfits_backdata_t), intent(inout) :: bd        !
      logical,                  intent(inout) :: error     !
    end subroutine imbfits_read_header_backdata
  end interface
  !
  interface
    subroutine imbfits_compress_header_backdata(bd,goodonly,error)
      use gbl_message
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(imbfits_backdata_t), intent(inout) :: bd        !
      logical,                  intent(in)    :: goodonly  ! Ignore bad data?
      logical,                  intent(inout) :: error     !
    end subroutine imbfits_compress_header_backdata
  end interface
  !
  interface
    subroutine imbfits_read_header_isodate2mjd(timesys,mjd_key,mjd,error)
      use gkernel_types
      !---------------------------------------------------------------------
      ! @ private
      !  Fill a duplicate, memory only, of a ISO date string into a MJD
      ! value.
      !---------------------------------------------------------------------
      type(fits_char_0d_t), intent(in)    :: timesys  !
      character(len=*),     intent(in)    :: mjd_key  ! Key name
      type(fits_dble_0d_t), intent(inout) :: mjd      !
      logical,              intent(inout) :: error    ! Logical error flag
    end subroutine imbfits_read_header_isodate2mjd
  end interface
  !
  interface
    subroutine imbfits_read_header_fixsubscanrange(subs,error)
      use gbl_message
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Fix the subscan on-track range (DATE-OBS to DATE-END) so that it
      ! is set to the intersection with the actual MJD ranges in the tables
      ! of the subscan.
      !  Intersection with Antenna Fast is disconnected for now, as it
      ! appears that we are often missing several fast traces at the end.
      ! But we don't use those traces for now.
      !---------------------------------------------------------------------
      type(imbfits_subscan_t), intent(inout) :: subs
      logical,                 intent(inout) :: error
    end subroutine imbfits_read_header_fixsubscanrange
  end interface
  !
  interface
    subroutine imbfits_free_header_primary(primary,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Free the Primary HDU in memory
      !---------------------------------------------------------------------
      type(imbfits_primary_t), intent(inout) :: primary
      logical,                 intent(inout) :: error
    end subroutine imbfits_free_header_primary
  end interface
  !
  interface
    subroutine imbfits_free_header_scan(scan,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Free the IMBF-scan HDU
      !---------------------------------------------------------------------
      type(imbfits_scan_t), intent(inout) :: scan
      logical,              intent(inout) :: error
    end subroutine imbfits_free_header_scan
  end interface
  !
  interface
    subroutine imbfits_free_header_frontend(front,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Free the IMBF-frontend HDU
      !---------------------------------------------------------------------
      type(imbfits_front_t), intent(inout) :: front
      logical,               intent(inout) :: error
    end subroutine imbfits_free_header_frontend
  end interface
  !
  interface
    subroutine imbfits_free_header_backend(back,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Free the IMBF-backend HDU
      !---------------------------------------------------------------------
      type(imbfits_back_t), intent(inout) :: back
      logical,              intent(inout) :: error
    end subroutine imbfits_free_header_backend
  end interface
  !
  interface
    subroutine imbfits_free_header_antfast(antfast,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Free the IMBF-antenna HDU
      !---------------------------------------------------------------------
      type(imbfits_antfast_t), intent(inout) :: antfast  !
      logical,                 intent(inout) :: error    !
    end subroutine imbfits_free_header_antfast
  end interface
  !
  interface
    subroutine imbfits_free_header_derot(derot,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Free the IMBF-xxx-derot HDU
      !---------------------------------------------------------------------
      type(imbfits_derot_t), intent(inout) :: derot  !
      logical,               intent(inout) :: error  !
    end subroutine imbfits_free_header_derot
  end interface
  !
  interface
    subroutine imbfits_free_header_backdata(bd,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Free the IMBF-backendFOO HDU (header + table, except DATA
      ! column)
      !---------------------------------------------------------------------
      type(imbfits_backdata_t), intent(inout) :: bd     !
      logical,                  intent(inout) :: error  !
    end subroutine imbfits_free_header_backdata
  end interface
  !
  interface
    subroutine imbfits_read_header_init(file,hdu,error)
      use gbl_message
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Read the elements in the 'imbfits_header_t' type
      !---------------------------------------------------------------------
      type(imbfits_file_t),   intent(in)    :: file
      type(imbfits_header_t), intent(inout) :: hdu
      logical,                intent(inout) :: error
    end subroutine imbfits_read_header_init
  end interface
  !
  interface
    subroutine imbfits_free_header_init(hdu,error)
      use gbl_message
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Free the elements in the 'imbfits_header_t' type
      !---------------------------------------------------------------------
      type(imbfits_header_t), intent(inout) :: hdu
      logical,                intent(inout) :: error
    end subroutine imbfits_free_header_init
  end interface
  !
  interface
    subroutine imbfits_read_header_done(file,hdu,error)
      use gbl_message
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(imbfits_file_t), intent(in)      :: file
      type(imbfits_header_t), intent(inout) :: hdu
      logical,              intent(inout)   :: error
    end subroutine imbfits_read_header_done
  end interface
  !
  interface
    subroutine imbfits_hdus(file,hdus,error)
      use gbl_message
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Set up the HDUs description which is used later on when we
      ! access the subscan HDUs.
      !---------------------------------------------------------------------
      type(imbfits_file_t), intent(in)    :: file
      type(imbfits_hdus_t), intent(out)   :: hdus
      logical,              intent(inout) :: error
    end subroutine imbfits_hdus
  end interface
  !
  interface
    subroutine imbfits_hdus_preset_2_0(file,hdus,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Set up the HDUs description which is used later on when we access
      ! the subscan HDUs. The description is forced to the IMB-FITS V2.0
      ! (included) up to V2.2 (excluded)
      !---------------------------------------------------------------------
      type(imbfits_file_t), intent(in)    :: file
      type(imbfits_hdus_t), intent(out)   :: hdus
      logical,              intent(inout) :: error
    end subroutine imbfits_hdus_preset_2_0
  end interface
  !
  interface
    subroutine imbfits_hdus_preset_2_21(file,nsub,hdus,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Set up the HDUs description which is used later on when we access
      ! the subscan HDUs. The description is forced to the IMB-FITS V2.21
      ! (included)
      !---------------------------------------------------------------------
      type(imbfits_file_t), intent(in)    :: file   !
      integer(kind=4),      intent(in)    :: nsub   ! Number of subscans in file
      type(imbfits_hdus_t), intent(out)   :: hdus   !
      logical,              intent(inout) :: error  !
    end subroutine imbfits_hdus_preset_2_21
  end interface
  !
  interface
    subroutine imbfits_hdus_autodetect(file,hdus,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Analyse the (first) HDUs in the FITS file in order to guess:
      !   - the number of "leading" HDUs (i.e. the ones which are present
      !     only once at the beginning)
      !   - the number of HDUs per subscan
      !   - the order of the HDUs
      !  There is a slight cost because we open several HDUs.
      !---------------------------------------------------------------------
      type(imbfits_file_t), intent(in)    :: file
      type(imbfits_hdus_t), intent(out)   :: hdus
      logical,              intent(inout) :: error
    end subroutine imbfits_hdus_autodetect
  end interface
  !
  interface
    subroutine imbfits_mvhdu_name(key,file,error)
      use gbl_message
      use cfitsio_api
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Move to the ***first*** HDU with the given name
      !---------------------------------------------------------------------
      character(len=*),     intent(in)    :: key
      type(imbfits_file_t), intent(in)    :: file
      logical,              intent(inout) :: error
    end subroutine imbfits_mvhdu_name
  end interface
  !
  interface
    subroutine imbfits_mvhdu_pos(ihdu,file,error)
      use gbl_message
      use cfitsio_api
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      ! Move to the HDU with the given absolute position
      !---------------------------------------------------------------------
      integer(kind=4),      intent(in)    :: ihdu
      type(imbfits_file_t), intent(in)    :: file
      logical,              intent(inout) :: error
    end subroutine imbfits_mvhdu_pos
  end interface
  !
  interface
    subroutine cfitsio_message(status)
      use cfitsio_api
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Print the CFITSIO error message given the error status. Should
      ! probably be public.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in) :: status
    end subroutine cfitsio_message
  end interface
  !
  interface
    subroutine imbfits_resample_header_backend(in,owidth,out,error)
      use imbfits_messaging
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Resample a backend table, by splitting each chunk into several
      ! "sub-chunks". Cut at best in the USED channels at almost equal
      ! width.
      !---------------------------------------------------------------------
      type(imbfits_back_t),  intent(in)    :: in
      real(kind=4),          intent(in)    :: owidth   ! Desired cal. bandwidth [MHz]
      type(imbfits_back_t),  intent(inout) :: out
      logical,               intent(inout) :: error
    end subroutine imbfits_resample_header_backend
  end interface
  !
  interface imbfits_compress_column
    subroutine imbfits_compress_inte_1d(inte,flag,nnew,buf,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private-generic imbfits_compress_column
      ! Compress the column according to the flag array. Assume the column,
      ! the flag array, and the buffer have a consistent size.
      !---------------------------------------------------------------------
      type(fits_inte_1d_t), intent(inout) :: inte     !
      logical,              intent(in)    :: flag(:)  !
      integer(kind=4),      intent(in)    :: nnew     ! Number of .true. flag values
      integer(kind=4)                     :: buf(:)   ! Working buffer
      logical,              intent(inout) :: error    ! Logical error flag
    end subroutine imbfits_compress_inte_1d
    subroutine imbfits_compress_dble_1d(dble,flag,nnew,buf,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private-generic imbfits_compress_column
      ! Compress the column according to the flag array. Assume the column,
      ! the flag array, and the buffer have a consistent size.
      !---------------------------------------------------------------------
      type(fits_dble_1d_t), intent(inout) :: dble     !
      logical,              intent(in)    :: flag(:)  !
      integer(kind=4),      intent(in)    :: nnew     ! Number of .true. flag values
      real(kind=8)                        :: buf(:)   ! Working buffer
      logical,              intent(inout) :: error    ! Logical error flag
    end subroutine imbfits_compress_dble_1d
  end interface imbfits_compress_column
  !
  interface imbfits_dump_key
    subroutine imbfits_dump_dble_0d(dble,olun,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private-generic imbfits_dump_key
      !---------------------------------------------------------------------
      type(fits_dble_0d_t), intent(in)    :: dble
      integer(kind=4),      intent(in)    :: olun   ! Output LUN
      logical,              intent(inout) :: error
    end subroutine imbfits_dump_dble_0d
    subroutine imbfits_dump_inte_0d(inte,olun,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private-generic imbfits_dump_key
      !---------------------------------------------------------------------
      type(fits_inte_0d_t), intent(in)    :: inte
      integer(kind=4),      intent(in)    :: olun   ! Output LUN
      logical,              intent(inout) :: error
    end subroutine imbfits_dump_inte_0d
    subroutine imbfits_dump_logi_0d(logi,olun,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private-generic imbfits_dump_key
      !---------------------------------------------------------------------
      type(fits_logi_0d_t), intent(in)    :: logi
      integer(kind=4),      intent(in)    :: olun   ! Output LUN
      logical,              intent(inout) :: error
    end subroutine imbfits_dump_logi_0d
    subroutine imbfits_dump_char_0d(char,olun,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private-generic imbfits_dump_key
      !---------------------------------------------------------------------
      type(fits_char_0d_t), intent(in)    :: char
      integer(kind=4),      intent(in)    :: olun   ! Output LUN
      logical,              intent(inout) :: error
    end subroutine imbfits_dump_char_0d
  end interface imbfits_dump_key
  !
  interface imbfits_make_column
    subroutine imbfits_make_inte_1d(key,n,comment,inte,error)
      use gbl_message
      use imbfits_types
      !-------------------------------------------------------------------
      ! @ private-generic imbfits_make_column
      ! Allocate and fill a column with continuous numbering.
      !-------------------------------------------------------------------
      character(len=*),     intent(in)    :: key
      integer(kind=4),      intent(in)    :: n
      character(len=*),     intent(in)    :: comment
      type(fits_inte_1d_t), intent(inout) :: inte    ! INOUT: keep allocation status
      logical,              intent(inout) :: error
    end subroutine imbfits_make_inte_1d
  end interface imbfits_make_column
  !
  interface imbfits_read_key
    subroutine imbfits_read_dble_0d(caller,file,key,dble,error,default)
      use gbl_message
      use cfitsio_api
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private-generic imbfits_read_key
      !---------------------------------------------------------------------
      character(len=*),     intent(in)           :: caller
      type(imbfits_file_t), intent(in)           :: file
      character(len=*),     intent(in)           :: key
      type(fits_dble_0d_t), intent(out)          :: dble
      logical,              intent(inout)        :: error
      real(kind=8),         intent(in), optional :: default
    end subroutine imbfits_read_dble_0d
    subroutine imbfits_read_inte_0d(caller,file,key,inte,error,default)
      use gbl_message
      use cfitsio_api
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private-generic imbfits_read_key
      !---------------------------------------------------------------------
      character(len=*),     intent(in)           :: caller
      type(imbfits_file_t), intent(in)           :: file
      character(len=*),     intent(in)           :: key
      type(fits_inte_0d_t), intent(out)          :: inte
      logical,              intent(inout)        :: error
      integer(kind=4),      intent(in), optional :: default
    end subroutine imbfits_read_inte_0d
    subroutine imbfits_read_logi_0d(caller,file,key,logi,error,default)
      use gbl_message
      use cfitsio_api
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private-generic imbfits_read_key
      !---------------------------------------------------------------------
      character(len=*),     intent(in)           :: caller
      type(imbfits_file_t), intent(in)           :: file
      character(len=*),     intent(in)           :: key
      type(fits_logi_0d_t), intent(out)          :: logi
      logical,              intent(inout)        :: error
      logical,              intent(in), optional :: default
    end subroutine imbfits_read_logi_0d
    subroutine imbfits_read_char_0d(caller,file,key,char,error,default)
      use gbl_message
      use cfitsio_api
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private-generic imbfits_read_key
      !---------------------------------------------------------------------
      character(len=*),     intent(in)           :: caller
      type(imbfits_file_t), intent(in)           :: file
      character(len=*),     intent(in)           :: key
      type(fits_char_0d_t), intent(out)          :: char
      logical,              intent(inout)        :: error
      character(len=*),     intent(in), optional :: default
    end subroutine imbfits_read_char_0d
  end interface imbfits_read_key
  !
  interface
    subroutine imbfits_variable_header(struct,head,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_header_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),       intent(in)    :: struct  ! Parent structure name
      type(imbfits_header_t), intent(in)    :: head    !
      logical,                intent(in)    :: ro      ! Read-Only
      logical,                intent(inout) :: error   !
    end subroutine imbfits_variable_header
  end interface
  !
  interface
    subroutine imbfits_variable_file(struct,file,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_file_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),     intent(in)    :: struct  ! Parent structure name
      type(imbfits_file_t), intent(in)    :: file    !
      logical,              intent(in)    :: ro      ! Read-Only
      logical,              intent(inout) :: error   !
    end subroutine imbfits_variable_file
  end interface
  !
  interface
    subroutine imbfits_variable_primary(struct,prim,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_primary_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),        intent(in)    :: struct  ! Parent structure name
      type(imbfits_primary_t), intent(in)    :: prim    !
      logical,                 intent(in)    :: ro      ! Read-Only
      logical,                 intent(inout) :: error   !
    end subroutine imbfits_variable_primary
  end interface
  !
  interface
    subroutine imbfits_variable_scan(struct,scan,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_scan_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),     intent(in)    :: struct  ! Parent structure name
      type(imbfits_scan_t), intent(in)    :: scan    !
      logical,              intent(in)    :: ro      ! Read-Only
      logical,              intent(inout) :: error   !
    end subroutine imbfits_variable_scan
  end interface
  !
  interface
    subroutine imbfits_variable_scan_header(struct,head,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_scan_header_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: struct  ! Parent structure name
      type(imbfits_scan_header_t), intent(in)    :: head    !
      logical,                     intent(in)    :: ro      ! Read-Only
      logical,                     intent(inout) :: error   !
    end subroutine imbfits_variable_scan_header
  end interface
  !
  interface
    subroutine imbfits_variable_scan_table(struct,table,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_scan_table_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),           intent(in)    :: struct  ! Parent structure name
      type(imbfits_scan_table_t), intent(in)    :: table   !
      logical,                    intent(in)    :: ro      ! Read-Only
      logical,                    intent(inout) :: error   !
    end subroutine imbfits_variable_scan_table
  end interface
  !
  interface
    subroutine imbfits_variable_front(struct,front,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_front_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),      intent(in)    :: struct  ! Parent structure name
      type(imbfits_front_t), intent(in)    :: front   !
      logical,               intent(in)    :: ro      ! Read-Only
      logical,               intent(inout) :: error   !
    end subroutine imbfits_variable_front
  end interface
  !
  interface
    subroutine imbfits_variable_front_header(struct,head,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_front_header_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),             intent(in)    :: struct  ! Parent structure name
      type(imbfits_front_header_t), intent(in)    :: head    !
      logical,                      intent(in)    :: ro      ! Read-Only
      logical,                      intent(inout) :: error   !
    end subroutine imbfits_variable_front_header
  end interface
  !
  interface
    subroutine imbfits_variable_front_table(struct,table,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_front_table_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: struct  ! Parent structure name
      type(imbfits_front_table_t), intent(in)    :: table   !
      logical,                     intent(in)    :: ro      ! Read-Only
      logical,                     intent(inout) :: error   !
    end subroutine imbfits_variable_front_table
  end interface
  !
  interface
    subroutine imbfits_variable_back(struct,back,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_back_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),     intent(in)    :: struct  ! Parent structure name
      type(imbfits_back_t), intent(in)    :: back    !
      logical,              intent(in)    :: ro      ! Read-Only
      logical,              intent(inout) :: error   !
    end subroutine imbfits_variable_back
  end interface
  !
  interface
    subroutine imbfits_variable_back_header(struct,head,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_back_header_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: struct  ! Parent structure name
      type(imbfits_back_header_t), intent(in)    :: head    !
      logical,                     intent(in)    :: ro      ! Read-Only
      logical,                     intent(inout) :: error   !
    end subroutine imbfits_variable_back_header
  end interface
  !
  interface
    subroutine imbfits_variable_back_table(struct,table,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_back_table_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),           intent(in)    :: struct  ! Parent structure name
      type(imbfits_back_table_t), intent(in)    :: table   !
      logical,                    intent(in)    :: ro      ! Read-Only
      logical,                    intent(inout) :: error   !
    end subroutine imbfits_variable_back_table
  end interface
  !
  interface
    subroutine imbfits_variable_backdata(struct,backdata,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_backdata_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),         intent(in)    :: struct    ! Parent structure name
      type(imbfits_backdata_t), intent(in)    :: backdata  !
      logical,                  intent(in)    :: ro        ! Read-Only
      logical,                  intent(inout) :: error     !
    end subroutine imbfits_variable_backdata
  end interface
  !
  interface
    subroutine imbfits_variable_backdata_header(struct,head,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_backdata_header_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),                intent(in)    :: struct  ! Parent structure name
      type(imbfits_backdata_header_t), intent(in)    :: head    !
      logical,                         intent(in)    :: ro      ! Read-Only
      logical,                         intent(inout) :: error   !
    end subroutine imbfits_variable_backdata_header
  end interface
  !
  interface
    subroutine imbfits_variable_backdata_table(struct,table,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_backdata_table_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),               intent(in)    :: struct  ! Parent structure name
      type(imbfits_backdata_table_t), intent(in)    :: table   !
      logical,                        intent(in)    :: ro      ! Read-Only
      logical,                        intent(inout) :: error   !
    end subroutine imbfits_variable_backdata_table
  end interface
  !
  interface
    subroutine imbfits_variable_antslow(struct,antslow,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_antslow_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),        intent(in)    :: struct   ! Parent structure name
      type(imbfits_antslow_t), intent(in)    :: antslow  !
      logical,                 intent(in)    :: ro       ! Read-Only
      logical,                 intent(inout) :: error    !
    end subroutine imbfits_variable_antslow
  end interface
  !
  interface
    subroutine imbfits_variable_antslow_header(struct,head,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_antslow_header_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),               intent(in)    :: struct  ! Parent structure name
      type(imbfits_antslow_header_t), intent(in)    :: head    !
      logical,                        intent(in)    :: ro      ! Read-Only
      logical,                        intent(inout) :: error   !
    end subroutine imbfits_variable_antslow_header
  end interface
  !
  interface
    subroutine imbfits_variable_antslow_table(struct,table,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_antslow_table_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),              intent(in)    :: struct  ! Parent structure name
      type(imbfits_antslow_table_t), intent(in)    :: table   !
      logical,                       intent(in)    :: ro      ! Read-Only
      logical,                       intent(inout) :: error   !
    end subroutine imbfits_variable_antslow_table
  end interface
  !
  interface
    subroutine imbfits_variable_antfast(struct,antfast,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_antfast_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),        intent(in)    :: struct   ! Parent structure name
      type(imbfits_antfast_t), intent(in)    :: antfast  !
      logical,                 intent(in)    :: ro       ! Read-Only
      logical,                 intent(inout) :: error    !
    end subroutine imbfits_variable_antfast
  end interface
  !
  interface
    subroutine imbfits_variable_antfast_table(struct,table,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_antfast_table_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),              intent(in)    :: struct  ! Parent structure name
      type(imbfits_antfast_table_t), intent(in)    :: table   !
      logical,                       intent(in)    :: ro      ! Read-Only
      logical,                       intent(inout) :: error   !
    end subroutine imbfits_variable_antfast_table
  end interface
  !
  interface
    subroutine imbfits_variable_derot(struct,derot,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_derot_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),      intent(in)    :: struct  ! Parent structure name
      type(imbfits_derot_t), intent(in)    :: derot   !
      logical,               intent(in)    :: ro      ! Read-Only
      logical,               intent(inout) :: error   !
    end subroutine imbfits_variable_derot
  end interface
  !
  interface
    subroutine imbfits_variable_derot_header(struct,head,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_derot_header_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),             intent(in)    :: struct  ! Parent structure name
      type(imbfits_derot_header_t), intent(in)    :: head    !
      logical,                      intent(in)    :: ro      ! Read-Only
      logical,                      intent(inout) :: error   !
    end subroutine imbfits_variable_derot_header
  end interface
  !
  interface
    subroutine imbfits_variable_derot_table(struct,table,ro,error)
      use imbfits_types
      !---------------------------------------------------------------------
      ! @ private
      !  Create or recreate Sic variables mapping a imbfits_derot_table_t
      ! structure.
      !---------------------------------------------------------------------
      character(len=*),            intent(in)    :: struct  ! Parent structure name
      type(imbfits_derot_table_t), intent(in)    :: table   !
      logical,                     intent(in)    :: ro      ! Read-Only
      logical,                     intent(inout) :: error   !
    end subroutine imbfits_variable_derot_table
  end interface
  !
end module imbfits_interfaces_private
