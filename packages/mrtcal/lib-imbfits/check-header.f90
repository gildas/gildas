!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine imbfits_obstype_equal(filename,imbf,obstype,equal,error)
  use gbl_message
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_obstype_equal
  !---------------------------------------------------------------------
  ! @ private
  ! Read the bare minimum as reading all the main hdus may take forever
  ! on large imbfitsfile.
  ! This could be a generic subroutine imbfits_equal. *** JP
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: filename
  type(imbfits_t),  intent(inout) :: imbf
  character(len=*), intent(in)    :: obstype
  logical,          intent(out)   :: equal
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='OBSTYPE>EQUAL'
  !
  call imbfits_message(seve%t,rname,'Welcome')
  !
  call imbfits_reopen_file(filename,imbf%file,imbf%hdus,error)
  if (error) return
  call imbfits_read_header_primary(imbf%file,imbf%primary,error)
  if (error)  return
  if (imbf%primary%obstype%val.ne.'calibrate') then
     call imbfits_message(seve%w,rname,trim(imbf%primary%obstype%val)//' is not a calibration scan')
     equal = .false.
  else
     equal = .true.
  endif
  !
end subroutine imbfits_obstype_equal
!
subroutine imbfits_check_version(version,error)
  use gbl_message
  use imbfits_parameters
  use imbfits_interfaces, except_this => imbfits_check_version
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  real(kind=8), intent(in)    :: version
  logical,      intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='IMBFITS>CHECK>VERSION'
  character(len=message_length) :: mess
  !
  if (version.lt.imbfits_version_min .or.  &
      version.ge.imbfits_version_max) then
    write(mess,'(A,F4.2,A)')  &
      'Version ',version,' of IMBFITS is not supported'
    call imbfits_message(seve%e,rname,mess)
    write(mess,'(A,F4.2,A,F4.2,A)')  &
      'Support starts at ',imbfits_version_min,  &
      ', up to ',imbfits_version_max,' (excluded)'
    call imbfits_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  !
end subroutine imbfits_check_version
!
subroutine imbfits_check_header_primary(file,primary,error)
  use gbl_message
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_check_header_primary
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(imbfits_file_t),    intent(in)    :: file
  type(imbfits_primary_t), intent(in)    :: primary
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CHECK>HEADER>PRIMARY'
  character(len=message_length) :: mess
  !
! Computing NHDU with FTTHDU is highly unefficient => not available.
!   if (primary%n_obsp%val*3.ne.(file%nhdu-hdustart)) then
!     ! 3 extensions per subscan: backend, antenna, subreflector
!     ! Warning only?
!     write(mess,'(A,I0,A,I0)')  &
!       'File is inconsistent: found ',file%nhdu,  &
!       ' HDUs, expected ',hdustart+primary%n_obsp%val*3
!     call imbfits_message(seve%e,rname,mess)
!     error = .true.
!     return
!   endif
  !
  if (primary%n_obs%val.ne.primary%n_obsp%val) then
    ! Warning only
    write (mess,'(A,I0,A,I0,A)')  &
      'File is incomplete (',primary%n_obsp%val,'/',  &
      primary%n_obs%val,' subscans)'
    call imbfits_message(seve%w,rname,mess)
  endif
  !
end subroutine imbfits_check_header_primary
!
subroutine imbfits_check_header_scan(primary,scan,error)
  use gbl_message
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_check_header_scan
  !---------------------------------------------------------------------
  ! @ private
  ! Check the consistency of the IMBF-scan HDU
  !---------------------------------------------------------------------
  type(imbfits_primary_t), intent(in)    :: primary
  type(imbfits_scan_t),    intent(in)    :: scan
  logical,                 intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CHECK>HEADER>SCAN'
  !
  ! Nothing relevant to be tested.
  !
end subroutine imbfits_check_header_scan
!
subroutine imbfits_check_header_backend(back,error)
  use gbl_message
  use imbfits_types
  use imbfits_interfaces, except_this => imbfits_check_header_backend
  !---------------------------------------------------------------------
  ! @ private
  ! Check the consistency of the IMBF-backend HDU
  ! *** JP Nothing done right now because previous test was too much
  !        restrictive. It is unclear whether this function is needed.
  !        Leave it for the time being.
  !---------------------------------------------------------------------
  type(imbfits_back_t), intent(in)    :: back
  logical,              intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CHECK>HEADER>BACKEND'
  !
end subroutine imbfits_check_header_backend
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
