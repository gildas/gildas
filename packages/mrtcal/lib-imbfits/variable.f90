subroutine imbfits_variable_header(struct,head,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_header
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_header_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),       intent(in)    :: struct  ! Parent structure name
  type(imbfits_header_t), intent(in)    :: head    !
  logical,                intent(in)    :: ro      ! Read-Only
  logical,                intent(inout) :: error   !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%DESC'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call sic_def_char(trim(str)//'%XTENSION',head%xtension%val,    ro,error)
  call sic_def_inte(trim(str)//'%BITPIX',  head%bitpix%val,  0,0,ro,error)
  call sic_def_inte(trim(str)//'%NAXIS1',  head%naxis1%val,  0,0,ro,error)
  call sic_def_inte(trim(str)//'%NAXIS2',  head%naxis2%val,  0,0,ro,error)
  call sic_def_inte(trim(str)//'%PCOUNT',  head%pcount%val,  0,0,ro,error)
  call sic_def_inte(trim(str)//'%GCOUNT',  head%gcount%val,  0,0,ro,error)
  call sic_def_inte(trim(str)//'%TFIELDS', head%tfields%val, 0,0,ro,error)
  call sic_def_char(trim(str)//'%EXTNAME', head%extname%val,     ro,error)
  !
end subroutine imbfits_variable_header
!
subroutine imbfits_variable_imbfits(struct,imbf,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_imbfits
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ public
  !  Create or recreate Sic variables mapping a imbfits_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: struct  ! Parent structure name
  type(imbfits_t),  intent(in)    :: imbf    !
  logical,          intent(in)    :: ro      ! Read-Only
  logical,          intent(inout) :: error   !
  ! Local
  logical :: userreq
  !
  userreq = .false.
  !
  call sic_delvariable(struct,userreq,error)
  call sic_defstructure(struct,.true.,error)
  if (error)  return
  !
  call imbfits_variable_file(struct,imbf%file,ro,error)
  if (error)  return
  call imbfits_variable_primary(struct,imbf%primary,ro,error)
  if (error)  return
  call imbfits_variable_scan(struct,imbf%scan,ro,error)
  if (error)  return
  call imbfits_variable_front(struct,imbf%front,ro,error)
  if (error)  return
  call imbfits_variable_back(struct,imbf%back,ro,error)
  if (error)  return
  call imbfits_variable_derot(struct,imbf%derot,ro,error)
  if (error)  return
  !
end subroutine imbfits_variable_imbfits
!
subroutine imbfits_variable_file(struct,file,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_file
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_file_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),     intent(in)    :: struct  ! Parent structure name
  type(imbfits_file_t), intent(in)    :: file    !
  logical,              intent(in)    :: ro      ! Read-Only
  logical,              intent(inout) :: error   !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%FILE'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call sic_def_inte(trim(str)//'%STATUS',  file%status,  0,0,ro,error)
  call sic_def_inte(trim(str)//'%UNIT',    file%unit,    0,0,ro,error)
  call sic_def_inte(trim(str)//'%BLOCK',   file%block,   0,0,ro,error)
  call sic_def_inte(trim(str)//'%RWSTATUS',file%rwstatus,0,0,ro,error)
  call sic_def_char(trim(str)//'%NAME',    file%name,        ro,error)
  !
end subroutine imbfits_variable_file
!
subroutine imbfits_variable_primary(struct,prim,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_primary
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_primary_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),        intent(in)    :: struct  ! Parent structure name
  type(imbfits_primary_t), intent(in)    :: prim    !
  logical,                 intent(in)    :: ro      ! Read-Only
  logical,                 intent(inout) :: error   !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%PRIM'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call sic_def_inte(trim(str)//'%STATUS',  prim%status,      0,0,ro,error)
  call sic_def_logi(trim(str)//'%SIMPLE',  prim%simple%val,      ro,error)
  call sic_def_inte(trim(str)//'%BITPIX',  prim%bitpix%val,  0,0,ro,error)
  call sic_def_inte(trim(str)//'%NAXIS',   prim%naxis%val,   0,0,ro,error)
  call sic_def_logi(trim(str)//'%EXTEND',  prim%extend%val,      ro,error)
  call sic_def_char(trim(str)//'%TELESCOP',prim%telescop%val,    ro,error)
  call sic_def_char(trim(str)//'%ORIGIN',  prim%origin%val,      ro,error)
  call sic_def_char(trim(str)//'%CREATOR', prim%creator%val,     ro,error)
  call sic_def_dble(trim(str)//'%IMBFTSVE',prim%imbftsve%val,0,0,ro,error)
  call sic_def_char(trim(str)//'%INSTRUME',prim%instrume%val,    ro,error)
  call sic_def_char(trim(str)//'%OBJECT',  prim%object%val,      ro,error)
  call sic_def_dble(trim(str)//'%LONGOBJ', prim%longobj%val, 0,0,ro,error)
  call sic_def_dble(trim(str)//'%LATOBJ',  prim%latobj%val,  0,0,ro,error)
  call sic_def_char(trim(str)//'%TIMESYS', prim%timesys%val,     ro,error)
  call sic_def_dble(trim(str)//'%MJD_OBS', prim%mjd_obs%val, 0,0,ro,error)
  call sic_def_char(trim(str)//'%DATE_OBS',prim%date_obs%val,    ro,error)
  call sic_def_dble(trim(str)//'%LST',     prim%lst%val,     0,0,ro,error)
  call sic_def_char(trim(str)//'%PROJID',  prim%projid%val,      ro,error)
  call sic_def_char(trim(str)//'%QUEUE',   prim%queue%val,       ro,error)
  call sic_def_dble(trim(str)//'%EXPTIME', prim%exptime%val, 0,0,ro,error)
  call sic_def_inte(trim(str)//'%N_OBS',   prim%n_obs%val,   0,0,ro,error)
  call sic_def_inte(trim(str)//'%N_OBSP',  prim%n_obsp%val,  0,0,ro,error)
  call sic_def_char(trim(str)//'%OBSTYPE', prim%obstype%val,     ro,error)
  call sic_def_inte(trim(str)//'%NUSEFEED',prim%nusefeed%val,0,0,ro,error)
  call sic_def_inte(trim(str)//'%TOTANT',  prim%totant%val,  0,0,ro,error)
  call sic_def_inte(trim(str)//'%TOTSUBR', prim%totsubr%val, 0,0,ro,error)
  call sic_def_inte(trim(str)//'%TOTBACK', prim%totback%val, 0,0,ro,error)
  !
end subroutine imbfits_variable_primary
!
subroutine imbfits_variable_scan(struct,scan,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_scan
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_scan_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),     intent(in)    :: struct  ! Parent structure name
  type(imbfits_scan_t), intent(in)    :: scan    !
  logical,              intent(in)    :: ro      ! Read-Only
  logical,              intent(inout) :: error   !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%SCAN'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call sic_def_inte(trim(str)//'%STATUS',scan%status,0,0,ro,error)
  !
  call imbfits_variable_scan_header(str,scan%head,ro,error)
  if (error)  return
  call imbfits_variable_scan_table(str,scan%table,ro,error)
  if (error)  return
  !
end subroutine imbfits_variable_scan
!
subroutine imbfits_variable_scan_header(struct,head,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_scan_header
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_scan_header_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: struct  ! Parent structure name
  type(imbfits_scan_header_t), intent(in)    :: head    !
  logical,                     intent(in)    :: ro      ! Read-Only
  logical,                     intent(inout) :: error   !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%HEAD'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call imbfits_variable_header(str,head%desc,ro,error)
  if (error)  return
  !
  call sic_def_char(trim(str)//'%TELESCOP',head%telescop%val,    ro,error)
  call sic_def_dble(trim(str)//'%TELSIZE', head%telsize%val, 0,0,ro,error)
  call sic_def_dble(trim(str)//'%SITELONG',head%sitelong%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%SITELAT', head%sitelat%val, 0,0,ro,error)
  call sic_def_dble(trim(str)//'%SITEELEV',head%siteelev%val,0,0,ro,error)
  call sic_def_char(trim(str)//'%PROJID',  head%projid%val,      ro,error)
  call sic_def_char(trim(str)//'%OBSID',   head%obsid%val,       ro,error)
  call sic_def_char(trim(str)//'%OPERATOR',head%operator%val,    ro,error)
  call sic_def_inte(trim(str)//'%SCANNUM', head%scannum%val, 0,0,ro,error)
  call sic_def_char(trim(str)//'%DATE_OBS',head%date_obs%val,    ro,error)
  call sic_def_char(trim(str)//'%DATE',    head%date%val,        ro,error)
  call sic_def_dble(trim(str)//'%MJD',     head%mjd%val,     0,0,ro,error)
  call sic_def_dble(trim(str)//'%LST',     head%lst%val,     0,0,ro,error)
  call sic_def_inte(trim(str)//'%N_OBS',   head%n_obs%val,   0,0,ro,error)
  call sic_def_inte(trim(str)//'%EXPTIME', head%exptime%val, 0,0,ro,error)
  call sic_def_char(trim(str)//'%TIMESYS', head%timesys%val,     ro,error)
  call sic_def_dble(trim(str)//'%UT1UTC',  head%ut1utc%val,  0,0,ro,error)
  call sic_def_dble(trim(str)//'%TAIUTC',  head%taiutc%val,  0,0,ro,error)
  call sic_def_dble(trim(str)//'%ETUTC',   head%etutc%val,   0,0,ro,error)
  call sic_def_dble(trim(str)//'%GPSTAI',  head%gpstai%val,  0,0,ro,error)
  call sic_def_dble(trim(str)//'%POLEX',   head%polex%val,   0,0,ro,error)
  call sic_def_dble(trim(str)//'%POLEY',   head%poley%val,   0,0,ro,error)
  call sic_def_char(trim(str)//'%OBJECT',  head%object%val,      ro,error)
  call sic_def_char(trim(str)//'%CTYPE1',  head%ctype1%val,      ro,error)
  call sic_def_char(trim(str)//'%CTYPE2',  head%ctype2%val,      ro,error)
  call sic_def_char(trim(str)//'%RADESYS', head%radesys%val,     ro,error)
  call sic_def_dble(trim(str)//'%EQUINOX', head%equinox%val, 0,0,ro,error)
  call sic_def_dble(trim(str)//'%CRVAL1',  head%crval1%val,  0,0,ro,error)
  call sic_def_dble(trim(str)//'%CRVAL2',  head%crval2%val,  0,0,ro,error)
  call sic_def_dble(trim(str)//'%LONPOLE', head%lonpole%val, 0,0,ro,error)
  call sic_def_dble(trim(str)//'%LATPOLE', head%latpole%val, 0,0,ro,error)
  call sic_def_dble(trim(str)//'%LONGOBJ', head%longobj%val, 0,0,ro,error)
  call sic_def_dble(trim(str)//'%LATOBJ',  head%latobj%val,  0,0,ro,error)
  call sic_def_logi(trim(str)//'%MOVEFRAM',head%movefram%val,    ro,error)
  call sic_def_dble(trim(str)//'%PERIDATE',head%peridate%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%PERIDIST',head%peridist%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%LONGASC', head%longasc%val, 0,0,ro,error)
  call sic_def_dble(trim(str)//'%OMEGA',   head%omega%val,   0,0,ro,error)
  call sic_def_dble(trim(str)//'%INCLINAT',head%inclinat%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%ECCENTR', head%eccentr%val, 0,0,ro,error)
  call sic_def_dble(trim(str)//'%ORBEPOCH',head%orbepoch%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%ORBEQNOX',head%orbeqnox%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%DISTANCE',head%distance%val,0,0,ro,error)
  call sic_def_char(trim(str)//'%SWTCHMOD',head%swtchmod%val,    ro,error)
  call sic_def_inte(trim(str)//'%NOSWITCH',head%noswitch%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%PHASETIM',head%phasetim%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%WOBTHROW',head%wobthrow%val,0,0,ro,error)
  call sic_def_inte(trim(str)//'%WOBDIR',  head%wobdir%val,  0,0,ro,error)
  call sic_def_dble(trim(str)//'%WOBCYCLE',head%wobcycle%val,0,0,ro,error)
  call sic_def_char(trim(str)//'%WOBMODE', head%wobmode%val,     ro,error)
  call sic_def_inte(trim(str)//'%NFEBE',   head%nfebe%val,   0,0,ro,error)
  call sic_def_dble(trim(str)//'%P1',      head%p1%val,      0,0,ro,error)
  call sic_def_dble(trim(str)//'%P2',      head%p2%val,      0,0,ro,error)
  call sic_def_dble(trim(str)//'%P3',      head%p3%val,      0,0,ro,error)
  call sic_def_dble(trim(str)//'%P4',      head%p4%val,      0,0,ro,error)
  call sic_def_dble(trim(str)//'%P5',      head%p5%val,      0,0,ro,error)
  call sic_def_dble(trim(str)//'%P7',      head%p7%val,      0,0,ro,error)
  call sic_def_dble(trim(str)//'%P8',      head%p8%val,      0,0,ro,error)
  call sic_def_dble(trim(str)//'%P9',      head%p9%val,      0,0,ro,error)
  call sic_def_dble(trim(str)//'%RXHORI',  head%rxhori%val,  0,0,ro,error)
  call sic_def_dble(trim(str)//'%RXVERT',  head%rxvert%val,  0,0,ro,error)
  call sic_def_dble(trim(str)//'%THIRD_RE',head%third_re%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%ZERO_POL',head%zero_pol%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%PHI_POL', head%phi_pol%val, 0,0,ro,error)
  call sic_def_dble(trim(str)//'%EPS_POL', head%eps_pol%val, 0,0,ro,error)
  call sic_def_dble(trim(str)//'%COL_SIN', head%col_sin%val, 0,0,ro,error)
  call sic_def_dble(trim(str)//'%COL_COS', head%col_cos%val, 0,0,ro,error)
  call sic_def_char(trim(str)//'%DATE_POI',head%date_poi%val,    ro,error)
  call sic_def_dble(trim(str)//'%P1COR',   head%p1cor%val,   0,0,ro,error)
  call sic_def_dble(trim(str)//'%P2COR',   head%p2cor%val,   0,0,ro,error)
  call sic_def_dble(trim(str)//'%P3COR',   head%p3cor%val,   0,0,ro,error)
  call sic_def_dble(trim(str)//'%P4COR',   head%p4cor%val,   0,0,ro,error)
  call sic_def_dble(trim(str)//'%P5COR',   head%p5cor%val,   0,0,ro,error)
  call sic_def_dble(trim(str)//'%P7COR',   head%p7cor%val,   0,0,ro,error)
  call sic_def_dble(trim(str)//'%P8COR',   head%p8cor%val,   0,0,ro,error)
  call sic_def_dble(trim(str)//'%P9COR',   head%p9cor%val,   0,0,ro,error)
  call sic_def_dble(trim(str)//'%RXHORICO',head%rxhorico%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%RXVERTCO',head%rxvertco%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%FOCUSX',  head%focusx%val,  0,0,ro,error)
  call sic_def_dble(trim(str)//'%FOCUSY',  head%focusy%val,  0,0,ro,error)
  call sic_def_dble(trim(str)//'%FOCUSZ',  head%focusz%val,  0,0,ro,error)
  call sic_def_dble(trim(str)//'%P4CORINC',head%p4corinc%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%P5CORINC',head%p5corinc%val,0,0,ro,error)
  call sic_def_char(trim(str)//'%DATE_INC',head%date_inc%val,    ro,error)
  call sic_def_dble(trim(str)//'%PRESSURE',head%pressure%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%TAMBIENT',head%tambient%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%HUMIDITY',head%humidity%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%WINDDIR', head%winddir%val, 0,0,ro,error)
  call sic_def_dble(trim(str)//'%WINDVEL', head%windvel%val, 0,0,ro,error)
  call sic_def_dble(trim(str)//'%WINDVELM',head%windvelm%val,0,0,ro,error)
  call sic_def_char(trim(str)//'%DATE_WEA',head%date_wea%val,    ro,error)
  call sic_def_dble(trim(str)//'%REFRACTI',head%refracti%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%THOT',    head%thot%val,    0,0,ro,error)
  call sic_def_char(trim(str)//'%DATE_HOT',head%date_hot%val,    ro,error)
  call sic_def_dble(trim(str)//'%TIPTAUZ', head%tiptauz%val, 0,0,ro,error)
  call sic_def_dble(trim(str)//'%TIPTAUE', head%tiptaue%val, 0,0,ro,error)
  call sic_def_dble(trim(str)//'%TIPTAUC', head%tiptauc%val, 0,0,ro,error)
  call sic_def_char(trim(str)//'%DATE_TIP',head%date_tip%val,    ro,error)
  call sic_def_inte(trim(str)//'%RECON',   head%recon%val,   0,0,ro,error)
  call sic_def_inte(trim(str)//'%RECCNTRL',head%reccntrl%val,0,0,ro,error)
  !
end subroutine imbfits_variable_scan_header
!
subroutine imbfits_variable_scan_table(struct,table,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_scan_table
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_scan_table_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),           intent(in)    :: struct  ! Parent structure name
  type(imbfits_scan_table_t), intent(in)    :: table   !
  logical,                    intent(in)    :: ro      ! Read-Only
  logical,                    intent(inout) :: error   !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%TABLE'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call sic_def_inte (trim(str)//'%STATUS', table%status,     0,0,              ro,error)
  call sic_def_charn(trim(str)//'%SYSOFF', table%sysoff%val, 1,table%sysoff%n, ro,error)
  call sic_def_real (trim(str)//'%XOFFSET',table%xoffset%val,1,table%xoffset%n,ro,error)
  call sic_def_real (trim(str)//'%YOFFSET',table%yoffset%val,1,table%yoffset%n,ro,error)
  !
end subroutine imbfits_variable_scan_table
!
subroutine imbfits_variable_front(struct,front,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_front
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_front_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),      intent(in)    :: struct  ! Parent structure name
  type(imbfits_front_t), intent(in)    :: front   !
  logical,               intent(in)    :: ro      ! Read-Only
  logical,               intent(inout) :: error   !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%FRONT'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call sic_def_inte(trim(str)//'%STATUS',front%status,0,0,ro,error)
  !
  call imbfits_variable_front_header(str,front%head,ro,error)
  if (error)  return
  call imbfits_variable_front_table(str,front%table,ro,error)
  if (error)  return
  !
end subroutine imbfits_variable_front
!
subroutine imbfits_variable_front_header(struct,head,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_front_header
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_front_header_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),             intent(in)    :: struct  ! Parent structure name
  type(imbfits_front_header_t), intent(in)    :: head    !
  logical,                      intent(in)    :: ro      ! Read-Only
  logical,                      intent(inout) :: error   !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%HEAD'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call imbfits_variable_header(str,head%desc,ro,error)
  if (error)  return
  !
  call sic_def_inte(trim(str)//'%SCANNUM', head%scannum%val, 0,0,ro,error)
  call sic_def_char(trim(str)//'%DATE_OBS',head%date_obs%val,    ro,error)
  call sic_def_char(trim(str)//'%DEWRTMOD',head%dewrtmod%val,    ro,error)
  call sic_def_dble(trim(str)//'%DEWANG',  head%dewang%val,  0,0,ro,error)
  call sic_def_inte(trim(str)//'%FEBEBAND',head%febeband%val,0,0,ro,error)
  call sic_def_inte(trim(str)//'%FEBEFEED',head%febefeed%val,0,0,ro,error)
  call sic_def_inte(trim(str)//'%NUSEFEED',head%nusefeed%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%VELOSYS', head%velosys%val, 0,0,ro,error)
  call sic_def_char(trim(str)//'%SPECSYS', head%specsys%val,     ro,error)
  call sic_def_char(trim(str)//'%VELOCONV',head%veloconv%val,    ro,error)
  call sic_def_char(trim(str)//'%EMIRBEAM',head%emirbeam%val,    ro,error)
  !
end subroutine imbfits_variable_front_header
!
subroutine imbfits_variable_front_table(struct,table,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_front_table
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_front_table_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: struct  ! Parent structure name
  type(imbfits_front_table_t), intent(in)    :: table   !
  logical,                     intent(in)    :: ro      ! Read-Only
  logical,                     intent(inout) :: error   !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%TABLE'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call sic_def_inte (trim(str)//'%STATUS',  table%status,      0,0,               ro,error)
  call sic_def_charn(trim(str)//'%RECNAME', table%recname%val, 1,table%recname%n, ro,error)
  call sic_def_charn(trim(str)//'%LINENAME',table%linename%val,1,table%linename%n,ro,error)
  call sic_def_dble (trim(str)//'%RESTFREQ',table%restfreq%val,1,table%restfreq%n,ro,error)
  call sic_def_real (trim(str)//'%BEAMEFF', table%beameff%val, 1,table%beameff%n, ro,error)
  call sic_def_real (trim(str)//'%ETAFSS',  table%etafss%val,  1,table%etafss%n,  ro,error)
  call sic_def_real (trim(str)//'%GAINIMAG',table%gainimag%val,1,table%gainimag%n,ro,error)
  call sic_def_charn(trim(str)//'%SIDEBAND',table%sideband%val,1,table%sideband%n,ro,error)
  call sic_def_dble (trim(str)//'%SBSEP',   table%sbsep%val,   1,table%sbsep%n,   ro,error)
  call sic_def_charn(trim(str)//'%WIDENAR', table%widenar%val, 1,table%widenar%n, ro,error)
  call sic_def_real (trim(str)//'%TCOLD',   table%tcold%val,   1,table%tcold%n,   ro,error)
  call sic_def_real (trim(str)//'%THOT',    table%thot%val,    1,table%thot%n,    ro,error)
  call sic_def_inte (trim(str)//'%IFEED',   table%ifeed%val,   1,table%ifeed%n,   ro,error)
  call sic_def_inte (trim(str)//'%NOFEEDS', table%nofeeds%val, 1,table%nofeeds%n, ro,error)
  call sic_def_charn(trim(str)//'%POLA',    table%pola%val,    1,table%pola%n,    ro,error)
  call sic_def_charn(trim(str)//'%DOPPLER', table%doppler%val, 1,table%doppler%n, ro,error)
  call sic_def_real (trim(str)//'%IFCENTER',table%ifcenter%val,1,table%ifcenter%n,ro,error)
  call sic_def_real (trim(str)//'%BANDWID', table%bandwid%val, 1,table%bandwid%n, ro,error)
  call sic_def_login(trim(str)//'%IFFLIPPS',table%ifflipps%val,1,table%ifflipps%n,ro,error)
  call sic_def_login(trim(str)//'%SPECLO',  table%speclo%val,  1,table%speclo%n,  ro,error)
  call sic_def_charn(trim(str)//'%TSCALE',  table%tscale%val,  1,table%tscale%n,  ro,error)
  call sic_def_real (trim(str)//'%FRQTHROW',table%frqthrow%val,1,table%frqthrow%n,ro,error)
  call sic_def_real (trim(str)//'%FRQOFF1', table%frqoff1%val, 1,table%frqoff1%n, ro,error)
  call sic_def_real (trim(str)//'%FRQOFF2', table%frqoff2%val, 1,table%frqoff2%n, ro,error)
  !
end subroutine imbfits_variable_front_table
!
subroutine imbfits_variable_back(struct,back,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_back
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_back_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),     intent(in)    :: struct  ! Parent structure name
  type(imbfits_back_t), intent(in)    :: back    !
  logical,              intent(in)    :: ro      ! Read-Only
  logical,              intent(inout) :: error   !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%BACK'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call sic_def_inte(trim(str)//'%STATUS',back%status,0,0,ro,error)
  !
  call imbfits_variable_back_header(str,back%head,ro,error)
  if (error)  return
  call imbfits_variable_back_table(str,back%table,ro,error)
  if (error)  return
  !
end subroutine imbfits_variable_back
!
subroutine imbfits_variable_back_header(struct,head,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_back_header
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_back_header_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: struct  ! Parent structure name
  type(imbfits_back_header_t), intent(in)    :: head    !
  logical,                     intent(in)    :: ro      ! Read-Only
  logical,                     intent(inout) :: error   !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%HEAD'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call imbfits_variable_header(str,head%desc,ro,error)
  if (error)  return
  !
  call sic_def_inte(trim(str)//'%SCANNUM', head%scannum%val, 0,0,ro,error)
  call sic_def_char(trim(str)//'%DATE_OBS',head%date_obs%val,    ro,error)
  call sic_def_inte(trim(str)//'%FEBEBAND',head%febeband%val,0,0,ro,error)
  call sic_def_inte(trim(str)//'%FEBEFEED',head%febefeed%val,0,0,ro,error)
  call sic_def_inte(trim(str)//'%NUSEFEED',head%nusefeed%val,0,0,ro,error)
  !
end subroutine imbfits_variable_back_header
!
subroutine imbfits_variable_back_table(struct,table,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_back_table
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_back_table_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),           intent(in)    :: struct  ! Parent structure name
  type(imbfits_back_table_t), intent(in)    :: table   !
  logical,                    intent(in)    :: ro      ! Read-Only
  logical,                    intent(inout) :: error   !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%TABLE'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call sic_def_inte (trim(str)//'%STATUS',  table%status,      0,0,               ro,error)
  call sic_def_inte (trim(str)//'%PART',    table%part%val,    1,table%part%n,    ro,error)
  call sic_def_inte (trim(str)//'%REFCHAN', table%refchan%val, 1,table%refchan%n, ro,error)
  call sic_def_inte (trim(str)//'%CHANS',   table%chans%val,   1,table%chans%n,   ro,error)
  call sic_def_inte (trim(str)//'%DROPPED', table%dropped%val, 1,table%dropped%n, ro,error)
  call sic_def_inte (trim(str)//'%USED',    table%used%val,    1,table%used%n,    ro,error)
  call sic_def_inte (trim(str)//'%PIXEL',   table%pixel%val,   1,table%pixel%n,   ro,error)
  call sic_def_charn(trim(str)//'%RECEIVER',table%receiver%val,1,table%receiver%n,ro,error)
  call sic_def_inte (trim(str)//'%IFRONT',  table%ifront%val,  1,table%ifront%n,  ro,error)
  call sic_def_charn(trim(str)//'%BAND',    table%band%val,    1,table%band%n,    ro,error)
  call sic_def_charn(trim(str)//'%FRONTEND',table%frontend%val,1,table%frontend%n,ro,error)
  call sic_def_charn(trim(str)//'%POLAR',   table%polar%val,   1,table%polar%n,   ro,error)
  call sic_def_real (trim(str)//'%REFFREQ', table%reffreq%val, 1,table%reffreq%n, ro,error)
  call sic_def_real (trim(str)//'%SPACING', table%spacing%val, 1,table%spacing%n, ro,error)
  call sic_def_login(trim(str)//'%DATAFLIP',table%dataflip%val,1,table%dataflip%n,ro,error)
  call sic_def_charn(trim(str)//'%LINENAME',table%linename%val,1,table%linename%n,ro,error)
  !
end subroutine imbfits_variable_back_table
!
!-----------------------------------------------------------------------
!
subroutine imbfits_variable_subscan(struct,subs,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_subscan
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ public
  !  Create or recreate Sic variables mapping a imbfits_subscan_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),        intent(in)    :: struct  ! Parent structure name
  type(imbfits_subscan_t), intent(in)    :: subs    !
  logical,                 intent(in)    :: ro      ! Read-Only
  logical,                 intent(inout) :: error   !
  ! Local
  logical :: userreq
  !
  userreq = .false.
  !
  call sic_delvariable(struct,userreq,error)
  call sic_defstructure(struct,.true.,error)
  if (error)  return
  !
  call sic_def_inte(trim(struct)//'%ISUB',subs%isub,0,0,ro,error)
  !
  call imbfits_variable_backdata(struct,subs%backdata,ro,error)
  if (error)  return
  call imbfits_variable_antslow(struct,subs%antslow,ro,error)
  if (error)  return
  call imbfits_variable_antfast(struct,subs%antfast,ro,error)
  if (error)  return
  !
end subroutine imbfits_variable_subscan
!
subroutine imbfits_variable_backdata(struct,backdata,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_backdata
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_backdata_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),         intent(in)    :: struct    ! Parent structure name
  type(imbfits_backdata_t), intent(in)    :: backdata  !
  logical,                  intent(in)    :: ro        ! Read-Only
  logical,                  intent(inout) :: error     !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%BACKDATA'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call imbfits_variable_backdata_header(str,backdata%head,ro,error)
  if (error)  return
  call imbfits_variable_backdata_table(str,backdata%table,ro,error)
  if (error)  return
  !
end subroutine imbfits_variable_backdata
!
subroutine imbfits_variable_backdata_header(struct,head,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_backdata_header
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_backdata_header_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),                intent(in)    :: struct  ! Parent structure name
  type(imbfits_backdata_header_t), intent(in)    :: head    !
  logical,                         intent(in)    :: ro      ! Read-Only
  logical,                         intent(inout) :: error   !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%HEAD'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call imbfits_variable_header(str,head%desc,ro,error)
  if (error)  return
  !
  call sic_def_inte(trim(str)//'%SCANNUM', head%scannum%val, 0,0,ro,error)
  call sic_def_inte(trim(str)//'%OBSNUM',  head%obsnum%val,  0,0,ro,error)
  call sic_def_char(trim(str)//'%BASEBAND',head%baseband%val,    ro,error)
  call sic_def_char(trim(str)//'%DATE_OBS',head%date_obs%val,    ro,error)
  call sic_def_dble(trim(str)//'%MJD_BEG', head%mjd_beg%val, 0,0,ro,error)
  call sic_def_char(trim(str)//'%DATE_END',head%date_end%val,    ro,error)
  call sic_def_dble(trim(str)//'%MJD_END', head%mjd_end%val, 0,0,ro,error)
  call sic_def_inte(trim(str)//'%CHANNELS',head%channels%val,0,0,ro,error)
  call sic_def_inte(trim(str)//'%NPHASES', head%nphases%val, 0,0,ro,error)
  call sic_def_char(trim(str)//'%PHASEONE',head%phaseone%val,    ro,error)
  call sic_def_dble(trim(str)//'%TSTAMPED',head%tstamped%val,0,0,ro,error)
  !
end subroutine imbfits_variable_backdata_header
!
subroutine imbfits_variable_backdata_table(struct,table,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_backdata_table
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_backdata_table_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),               intent(in)    :: struct  ! Parent structure name
  type(imbfits_backdata_table_t), intent(in)    :: table   !
  logical,                        intent(in)    :: ro      ! Read-Only
  logical,                        intent(inout) :: error   !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%TABLE'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call sic_def_inte(trim(str)//'%STATUS',  table%status,      0,0,               ro,error)
  call sic_def_dble(trim(str)//'%MJD',     table%mjd%val,     1,table%mjd%n,     ro,error)
  call sic_def_dble(trim(str)//'%INTEGTIM',table%integtim%val,1,table%integtim%n,ro,error)
  call sic_def_inte(trim(str)//'%ISWITCH', table%iswitch%val, 1,table%iswitch%n, ro,error)
  call sic_def_inte(trim(str)//'%FOREPOIN',table%forepoin%val,1,table%forepoin%n,ro,error)
  call sic_def_inte(trim(str)//'%BACKPOIN',table%backpoin%val,1,table%backpoin%n,ro,error)
  !
end subroutine imbfits_variable_backdata_table
!
subroutine imbfits_variable_antslow(struct,antslow,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_antslow
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_antslow_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),        intent(in)    :: struct   ! Parent structure name
  type(imbfits_antslow_t), intent(in)    :: antslow  !
  logical,                 intent(in)    :: ro       ! Read-Only
  logical,                 intent(inout) :: error    !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%ANTSLOW'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call sic_def_inte(trim(str)//'%STATUS',antslow%status,0,0,ro,error)
  if (error)  return
  !
  call imbfits_variable_antslow_header(str,antslow%head,ro,error)
  if (error)  return
  call imbfits_variable_antslow_table(str,antslow%table,ro,error)
  if (error)  return
  !
end subroutine imbfits_variable_antslow
!
subroutine imbfits_variable_antslow_header(struct,head,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_antslow_header
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_antslow_header_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),               intent(in)    :: struct  ! Parent structure name
  type(imbfits_antslow_header_t), intent(in)    :: head    !
  logical,                        intent(in)    :: ro      ! Read-Only
  logical,                        intent(inout) :: error   !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%HEAD'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call imbfits_variable_header(str,head%desc,ro,error)
  if (error)  return
  !
  call sic_def_inte(trim(str)//'%SCANNUM', head%scannum%val, 0,0,ro,error)
  call sic_def_inte(trim(str)//'%OBSNUM',  head%obsnum%val,  0,0,ro,error)
  call sic_def_char(trim(str)//'%DATE_OBS',head%date_obs%val,    ro,error)
  call sic_def_dble(trim(str)//'%MJD_BEG', head%mjd_beg%val, 0,0,ro,error)
  call sic_def_char(trim(str)//'%DATE_END',head%date_end%val,    ro,error)
  call sic_def_dble(trim(str)//'%MJD_END', head%mjd_end%val, 0,0,ro,error)
  call sic_def_char(trim(str)//'%OBSTYPE', head%obstype%val,     ro,error)
  call sic_def_char(trim(str)//'%SUBSTYPE',head%substype%val,    ro,error)
  call sic_def_dble(trim(str)//'%SUBSTIME',head%substime%val,0,0,ro,error)
  call sic_def_char(trim(str)//'%SYSTEMOF',head%systemof%val,    ro,error)
  call sic_def_dble(trim(str)//'%SUBSXOFF',head%subsxoff%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%SUBSYOFF',head%subsyoff%val,0,0,ro,error)
  call sic_def_char(trim(str)//'%SETYPE01',head%setype01%val,    ro,error)
  call sic_def_dble(trim(str)//'%SETIME01',head%setime01%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%SEXOFF01',head%sexoff01%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%SEYOFF01',head%seyoff01%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%SEXSTA01',head%sexsta01%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%SEYSTA01',head%seysta01%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%SEXEND01',head%sexend01%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%SEYEND01',head%seyend01%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%SESPES01',head%sespes01%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%SESPEE01',head%sespee01%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%DOPPLERC',head%dopplerc%val,0,0,ro,error)
  call sic_def_dble(trim(str)//'%OBSVELRF',head%obsvelrf%val,0,0,ro,error)
  call sic_def_inte(trim(str)//'%TRACERAT',head%tracerat%val,0,0,ro,error)
  !
end subroutine imbfits_variable_antslow_header
!
subroutine imbfits_variable_antslow_table(struct,table,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_antslow_table
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_antslow_table_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),              intent(in)    :: struct  ! Parent structure name
  type(imbfits_antslow_table_t), intent(in)    :: table   !
  logical,                       intent(in)    :: ro      ! Read-Only
  logical,                       intent(inout) :: error   !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%TABLE'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call sic_def_inte(trim(str)//'%STATUS',   table%status,       0,0,                ro,error)
  call sic_def_dble(trim(str)//'%MJD',      table%mjd%val,      1,table%mjd%n,      ro,error)
  call sic_def_dble(trim(str)//'%LST',      table%lst%val,      1,table%lst%n,      ro,error)
  call sic_def_dble(trim(str)//'%LONGOFF',  table%longoff%val,  1,table%longoff%n,  ro,error)
  call sic_def_dble(trim(str)//'%LATOFF',   table%latoff%val,   1,table%latoff%n,   ro,error)
  call sic_def_dble(trim(str)//'%CAZIMUTH', table%cazimuth%val, 1,table%cazimuth%n, ro,error)
  call sic_def_dble(trim(str)//'%CELEVATIO',table%celevatio%val,1,table%celevatio%n,ro,error)
  call sic_def_inte(trim(str)//'%TRACEFLAG',table%traceflag%val,1,table%traceflag%n,ro,error)
  !
end subroutine imbfits_variable_antslow_table
!
subroutine imbfits_variable_antfast(struct,antfast,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_antfast
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_antfast_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),        intent(in)    :: struct   ! Parent structure name
  type(imbfits_antfast_t), intent(in)    :: antfast  !
  logical,                 intent(in)    :: ro       ! Read-Only
  logical,                 intent(inout) :: error    !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%ANTFAST'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call sic_def_inte(trim(str)//'%STATUS',antfast%status,0,0,ro,error)
  if (error)  return
  !
  ! There is not yet an antfast header
  call imbfits_variable_antslow_header(str,antfast%head,ro,error)
  if (error)  return
  call imbfits_variable_antfast_table(str,antfast%table,ro,error)
  if (error)  return
  !
end subroutine imbfits_variable_antfast
!
subroutine imbfits_variable_antfast_table(struct,table,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_antfast_table
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_antfast_table_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),              intent(in)    :: struct  ! Parent structure name
  type(imbfits_antfast_table_t), intent(in)    :: table   !
  logical,                       intent(in)    :: ro      ! Read-Only
  logical,                       intent(inout) :: error   !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%TABLE'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call sic_def_inte(trim(str)//'%STATUS',     table%status,         0,0,                  ro,error)
  call sic_def_dble(trim(str)//'%MJD',        table%mjd%val,        1,table%mjd%n,        ro,error)
  call sic_def_dble(trim(str)//'%AZIMUTH',    table%azimuth%val,    1,table%azimuth%n,    ro,error)
  call sic_def_dble(trim(str)//'%ELEVATION',  table%elevation%val,  1,table%elevation%n,  ro,error)
  call sic_def_dble(trim(str)//'%TRACKING_AZ',table%tracking_az%val,1,table%tracking_az%n,ro,error)
  call sic_def_dble(trim(str)//'%TRACKING_EL',table%tracking_el%val,1,table%tracking_el%n,ro,error)
  !
end subroutine imbfits_variable_antfast_table
!
subroutine imbfits_variable_derot(struct,derot,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_derot
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_derot_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),      intent(in)    :: struct  ! Parent structure name
  type(imbfits_derot_t), intent(in)    :: derot   !
  logical,               intent(in)    :: ro      ! Read-Only
  logical,               intent(inout) :: error   !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%DEROT'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call sic_def_inte(trim(str)//'%STATUS',derot%status,0,0,ro,error)
  if (error)  return
  !
  call imbfits_variable_derot_header(str,derot%head,ro,error)
  if (error)  return
  call imbfits_variable_derot_table(str,derot%table,ro,error)
  if (error)  return
  !
end subroutine imbfits_variable_derot
!
subroutine imbfits_variable_derot_header(struct,head,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_derot_header
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_derot_header_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),             intent(in)    :: struct  ! Parent structure name
  type(imbfits_derot_header_t), intent(in)    :: head    !
  logical,                      intent(in)    :: ro      ! Read-Only
  logical,                      intent(inout) :: error   !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%HEAD'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call imbfits_variable_header(str,head%desc,ro,error)
  if (error)  return
  !
  call sic_def_inte(trim(str)//'%SCANNUM', head%scannum%val, 0,0,ro,error)
  call sic_def_inte(trim(str)//'%OBSNUM',  head%obsnum%val,  0,0,ro,error)
  call sic_def_char(trim(str)//'%DATE_OBS',head%date_obs%val,    ro,error)
  call sic_def_dble(trim(str)//'%MJD_BEG', head%mjd_beg%val, 0,0,ro,error)
  call sic_def_char(trim(str)//'%DATE_END',head%date_end%val,    ro,error)
  call sic_def_dble(trim(str)//'%MJD_END', head%mjd_end%val, 0,0,ro,error)
  call sic_def_dble(trim(str)//'%SUBSTIME',head%substime%val,0,0,ro,error)
  !
end subroutine imbfits_variable_derot_header
!
subroutine imbfits_variable_derot_table(struct,table,ro,error)
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_derot_table
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ private
  !  Create or recreate Sic variables mapping a imbfits_derot_table_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),            intent(in)    :: struct  ! Parent structure name
  type(imbfits_derot_table_t), intent(in)    :: table   !
  logical,                     intent(in)    :: ro      ! Read-Only
  logical,                     intent(inout) :: error   !
  ! Local
  logical :: userreq
  character(len=32) :: str
  !
  userreq = .false.
  str = trim(struct)//'%TABLE'
  !
  call sic_delvariable(str,userreq,error)
  call sic_defstructure(str,.true.,error)
  if (error)  return
  !
  call sic_def_inte (trim(str)//'%STATUS',table%status,    0,0,             ro,error)
  if (error)  return
  !
  ! Do not define the arrays if 0-sized (not supported by SIC); most likely
  ! the whole Derotator HDU is missing.
  if (table%mjd%n.le.0)  return
  call sic_def_dble (trim(str)//'%MJD',   table%mjd%val,   1,table%mjd%n,   ro,error)
  call sic_def_charn(trim(str)//'%SYSTEM',table%system%val,1,table%system%n,ro,error)
  call sic_def_real (trim(str)//'%FWANT', table%fwant%val, 1,table%fwant%n, ro,error)
  call sic_def_real (trim(str)//'%HWANT', table%hwant%val, 1,table%hwant%n, ro,error)
  call sic_def_real (trim(str)//'%SWANT', table%swant%val, 1,table%swant%n, ro,error)
  call sic_def_real (trim(str)//'%FACT',  table%fact%val,  1,table%fact%n,  ro,error)
  call sic_def_real (trim(str)//'%HACT',  table%hact%val,  1,table%hact%n,  ro,error)
  call sic_def_real (trim(str)//'%SACT',  table%sact%val,  1,table%sact%n,  ro,error)
  !
end subroutine imbfits_variable_derot_table
!
subroutine imbfits_variable_imbfdata(struct,imbf,ro,error)
  use gildas_def
  use gkernel_interfaces
  use imbfits_interfaces, except_this=>imbfits_variable_imbfdata
  use imbfits_types
  !---------------------------------------------------------------------
  ! @ public
  !  Create or recreate Sic variables mapping a imbfits_data_t
  ! structure.
  !---------------------------------------------------------------------
  character(len=*),     intent(in)    :: struct  ! Parent structure name
  type(imbfits_data_t), intent(in)    :: imbf    !
  logical,              intent(in)    :: ro      ! Read-Only
  logical,              intent(inout) :: error   !
  ! Local
  integer(kind=index_length) :: dims(sic_maxdims)
  logical :: userreq
  !
  userreq = .false.
  !
  call sic_delvariable(struct,userreq,error)
  call sic_defstructure(struct,.true.,error)
  if (error)  return
  !
  call sic_def_inte(trim(struct)//'%NTIME',imbf%ntime,0,0,ro,error)
  call sic_def_inte(trim(struct)//'%NPIX', imbf%npix, 0,0,ro,error)
  call sic_def_inte(trim(struct)//'%NCHAN',imbf%nchan,0,0,ro,error)
  dims(1) = imbf%nchan
  dims(2) = imbf%npix
  dims(3) = imbf%ntime
  call sic_def_real(trim(struct)//'%VAL',  imbf%val,  3,dims,ro,error)
  call sic_def_real(trim(struct)//'%WEI',  imbf%wei,  1,dims,ro,error)
  !
end subroutine imbfits_variable_imbfdata
