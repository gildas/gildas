!-----------------------------------------------------------------------
! For each subscan show:
!  - the beginning and end of the subscan,
!  - the position of the AntSlow and BackendDATA dumps over the time
!    line.
! This is intended to see easily what happens for some cases where the
! streams are interrupted during the subscan. See also command MSET
! CALIBRATION MJDINTER
!-----------------------------------------------------------------------
!
begin procedure plot-mjd-variables

  if exist(plotmjd)  del /var plotmjd

  define structure plotmjd /global
  define character*512 plotmjd%file /global
  define logical plotmjd%antfast /global
  define integer plotmjd%first plotmjd%last plotmjd%n /global
  define double  plotmjd%min plotmjd%max /global

end procedure plot-mjd-variables
!
begin procedure plot-mjd-min
  !
  read 'plotmjd%file' /file /sub plotmjd%first
  variable
  !
  if plotmjd%first.eq.1 then
    let plotmjd%min min(imbf%prim%mjd_obs,subs%antslow%head%mjd_beg)
  else
    let plotmjd%min subs%antslow%head%mjd_beg
  endif
  !
  if subs%antslow%head%desc%naxis2.gt.0 then
    if (subs%antslow%table%mjd[1].lt.plotmjd%min) then
      plotmjd%min = subs%antslow%table%mjd[1]
    endif
  endif
  if subs%backdata%head%desc%naxis2.gt.0 then
    if (subs%backdata%table%mjd[1].lt.plotmjd%min) then
      plotmjd%min = subs%backdata%table%mjd[1]
    endif
  endif
  if imbf%derot%head%desc%naxis2.gt.0 then
    if imbf%derot%table%mjd[1].lt.plotmjd%min then
      plotmjd%min = imbf%derot%table%mjd[1]
    endif
  endif
  if plotmjd%antfast then
    if subs%antfast%head%desc%naxis2.gt.0 then
      if subs%antfast%table%mjd[1].lt.plotmjd%min then
        plotmjd%min = subs%antfast%table%mjd[1]
      endif
    endif
  endif
  !
end procedure plot-mjd-min
!
begin procedure plot-mjd-max
  !
  read 'plotmjd%file' /file /sub plotmjd%last
  variable
  !
  let plotmjd%max subs%antslow%head%mjd_end
  !
  if subs%antslow%head%desc%naxis2.gt.0 then
    if (subs%antslow%table%mjd[subs%antslow%head%desc%naxis2].gt.plotmjd%max) then
      plotmjd%max = subs%antslow%table%mjd[subs%antslow%head%desc%naxis2]
    endif
  endif
  if subs%backdata%head%desc%naxis2.gt.0 then
    if (subs%backdata%table%mjd[subs%backdata%head%desc%naxis2].gt.plotmjd%max) then
      plotmjd%max = subs%backdata%table%mjd[subs%backdata%head%desc%naxis2]
    endif
  endif
  if imbf%derot%head%desc%naxis2.gt.0 then
    if imbf%derot%table%mjd[imbf%derot%head%desc%naxis2].gt.plotmjd%max then
      plotmjd%max = imbf%derot%table%mjd[imbf%derot%head%desc%naxis2]
    endif
  endif
  if plotmjd%antfast then
    if subs%antfast%head%desc%naxis2.gt.0 then
      if subs%antfast%table%mjd[subs%antfast%head%desc%naxis2].gt.plotmjd%max then
        plotmjd%max = subs%antfast%table%mjd[subs%antfast%head%desc%naxis2]
      endif
    endif
  endif
  !
end procedure plot-mjd-max
!
begin procedure plot-mjd-box
  !
  define double xu_min xu_max
  !
  @ plot-mjd-min  ! Fills plotmjd%min
  @ plot-mjd-max  ! Fills plotmjd%max

  clear
  g\set box_loc 4 28 2.5 18.0
  !
  ! Limits
  let xu_min (plotmjd%min-imbf%prim%mjd_obs)*24*60  ! [min]
  let xu_max (plotmjd%max-imbf%prim%mjd_obs)*24*60  ! [min]
  if (xu_max-xu_min).le.2 then  ! Less than 2 min => use seconds
    limits xu_min*60 xu_max*60 plotmjd%first-1 plotmjd%last+1 /margin 5 0
    axis XU /label p
    g\draw text 0 1 "Time from scan start [sec]" /box 8
  else if (xu_max-xu_min).le.100 then  ! Less than 100 min => use minutes
    limits xu_min xu_max plotmjd%first-1 plotmjd%last+1 /margin 5 0
    axis XU /label p
    g\draw text 0 1 "Time from scan start [min]" /box 8
  else  ! > 100 min => use hours
    limits xu_min/60 xu_max/60 plotmjd%first-1 plotmjd%last+1 /margin 5 0
    axis XU /label p
    g\draw text 0 1 "Time from scan start [hr]" /box 8
  endif
  !
  g\tickspace 0 0 1 0  ! Auto ticks in X, auto major and minor at 1 in Y
  limits plotmjd%min plotmjd%max plotmjd%first-1 plotmjd%last+1 /margin 5 0
  axis XL /label p
  g\label "MJD [day]" /x
  axis YL /label o
  g\label "Subscan #" /y
  axis YR
  !
  ! Title
  define char*32 shortname
  sic parse 'plotmjd%file' shortname
  g\draw text 0 2.0 'shortname'".fits" /box 8
  !
  ! Start of scan marker
  g\draw text imbf%prim%mjd_obs user_ymin " Scan start" 9 90  /user
  g\pen /dash 2
  g\draw relo imbf%prim%mjd_obs user_ymin /user
  g\draw line imbf%prim%mjd_obs user_ymax /user /clip
  g\pen /dash 1
  !
  ! Midnight marker
  g\draw text 'nint(plotmjd%min)' user_ymin " Midnight" 9 90  /user
  g\pen /dash 2
  g\draw relo 'nint(plotmjd%min)' user_ymin /user
  g\draw line 'nint(plotmjd%min)' user_ymax /user /clip
  g\pen /dash 1
  !
end procedure plot-mjd-box
!
begin procedure plot-mjd-legend
  !
  g\set mark * 1 0.2 0.0
  !
  define double xmark ymark xshift yshift
  let yshift (user_ymax-user_ymin)/20
  let xshift (user_xmax-user_xmin)/30
  !
  g\pen /co 3
  let ymark user_ymax-1*yshift
  g\set mark 4 1 * 45  ! Cross
  for imark 1 to 4
    let xmark user_xmin+imark*xshift
    g\point xmark ymark
  next imark
  let xmark user_xmin+5*xshift
  g\draw text xmark ymark "AntSlow MJD" 6 /user
  !
  g\pen /co red
  let ymark user_ymax-2*yshift
  g\set mark 8 0 * 45  ! Cross
  for imark 1 to 4
    let xmark user_xmin+imark*xshift
    g\point xmark ymark
  next imark
  let xmark user_xmin+5*xshift
  g\draw text xmark ymark "AntSlow MJD (trace flagged)" 6 /user
  !
  let ymark user_ymax-3*yshift
  g\set mark 2 1 * 90 ! Vertical bar
  for imark 1 to 4
    if mod(imark,2).eq.0 then
      pen /co goldenrod
    else
      pen /co web_green
    endif
    let xmark user_xmin+imark*xshift
    g\point xmark ymark
    g\draw relo xmark-xshift/3 ymark /user
    g\draw line xmark+xshift/3 ymark /user
  next imark
  let xmark user_xmin+5*xshift
  g\draw text xmark ymark "BackendDATA MJD" 6 /user
  !
  g\pen /co 6
  let ymark user_ymax-4*yshift
  g\set mark 4 1 * 45  ! Cross
  for imark 1 to 4
    let xmark user_xmin+imark*xshift
    g\point xmark ymark
  next imark
  let xmark user_xmin+5*xshift
  g\draw text xmark ymark "Derot MJD" 6 /user
  !
  if plotmjd%antfast then
    g\pen /co dark_cyan
    let ymark user_ymax-5*yshift
    g\set mark 2 1 * 90  ! Vertical bars
    for imark 1 to 4
      let xmark user_xmin+imark*xshift
      g\point xmark ymark
    next imark
    let xmark user_xmin+5*xshift
    g\draw text xmark ymark "AntFast MJD" 6 /user
  endif
  !
  g\pen /co 0
  !
end procedure plot-mjd-legend

@ plot-mjd-variables

! Command line parsing
sic\parse 1 /antfast 0 /first 1 /last 1

let plotmjd%file 'pro%parse%command%arg[1]'
let plotmjd%antfast pro%parse%antfast%narg.ge.0

read 'plotmjd%file' /file /sub 1  ! Need n_obsp below
variable
if pro%parse%first%narg.ge.0 then
  let plotmjd%first 'pro%parse%first%arg[1]'
else
  let plotmjd%first 1
endif
if pro%parse%last%narg.ge.0 then
  let plotmjd%last 'pro%parse%last%arg[1]'
else
  let plotmjd%last imbf%prim%n_obsp
endif
let plotmjd%n plotmjd%last-plotmjd%first+1

! Avoid reading the DATA column will save us from MJD errors which
! prevent to execute this procedure to its end, while it is really
! useful to understand what happens in this context!
mset input data none

@ plot-mjd-box

@ plot-mjd-legend

define double mypol[4,2] ymark
define real ypos[3]
define integer dims[8]
!
! Set marker size as a function of scan duration (i.e.
! the longer the scan, the larger the number of MJD points,
! the smaller the markers)
g\set mark * 1 '5e-5/(user_xmax-user_xmin)' 0.0
! Set character size (for subscan labels) as a function of
! the number of subscans (more subscan means smaller labels)
g\set char '0.5*(3/plotmjd%n)**0.25'
!
!--- Derot -----------------------------------------------------------
if imbf%derot%head%desc%naxis2.gt.0
  g\pen /co 6
  define real myy /like imbf%derot%table%mjd
  let myy 0.5
  g\set mark 4 * * 45 ! Cross
  g\point imbf%derot%table%mjd myy
  del /var myy
  g\pen /co 0
endif
!---------------------------------------------------------------------
!
for isub plotmjd%first to plotmjd%last
  read 'pro%arg[1]' /file /sub isub
  variable
  let mypol[1] SUBS%ANTSLOW%HEAD%MJD_BEG SUBS%ANTSLOW%HEAD%MJD_END -
               SUBS%ANTSLOW%HEAD%MJD_END SUBS%ANTSLOW%HEAD%MJD_BEG
  let mypol[2] isub-0.3 isub-0.3 isub+0.3 isub+0.3
  g\poly mypol /var /plot
  g\draw text '(SUBS%ANTSLOW%HEAD%MJD_BEG+SUBS%ANTSLOW%HEAD%MJD_END)/2' -
              isub+0.3 -
              'SUBS%ANTSLOW%HEAD%SUBSTYPE' 8 /user
  !
  ! Positions
  if plotmjd%antfast then
    let ypos[1] +0.15  ! AntSlow
    let ypos[2] -0.15  ! BackendDATA
    let ypos[3]  0.00  ! AntFast
  else
    let ypos[1] +0.1  ! AntSlow
    let ypos[2] -0.1  ! BackendDATA
  endif
  !
  !--- AntSlow ---------------------------------------------------------
  define real myy
  let myy isub+ypos[1]
  compute dims dimof subs%antslow%table%traceflag
  for itrace 1 to dims[1]
    if subs%antslow%table%traceflag[itrace].eq.0 then
      g\pen /col blue
      g\set mark 4 1 * 45  ! Blue crosses
    else
      g\pen /col red
      g\set mark 8 0 * 45  ! Red circles
    endif
    g\point SUBS%antslow%table%mjd[itrace] myy
  next itrace
  del /var myy
  !---------------------------------------------------------------------
  !
  !--- BackendDATA -----------------------------------------------------------
  g\set mark 2 1 * 90 ! Vertical bar
  g\pen /co 1
  ! g\draw relo SUBS%BACKDATA%table%mjd[1] isub-.1 /user
  ! g\draw line SUBS%BACKDATA%table%mjd[SUBS%BACKDATA%HEAD%DESC%NAXIS2] isub-.1 /user
  let ymark isub+ypos[2]
  ! If we are using the mode MSET INPUT BAD NO, subs%backdata%head%desc%naxis2
  ! may not match the size of subs%backdata%table%mjd. Use its exact dimension
  ! instead
  compute dims dimof subs%backdata%table%mjd
  for idump 1 to dims[1]
    if mod(idump,2).eq.0 then
      pen /co goldenrod
    else
      pen /co web_green
    endif
    g\point SUBS%backdata%table%mjd[idump] ymark
    g\draw relo SUBS%BACKDATA%table%mjd[idump]+(0-SUBS%BACKDATA%head%tstamped)*subs%backdata%table%integtim[idump]/86400 ymark /user
    g\draw line SUBS%BACKDATA%table%mjd[idump]+(1-SUBS%BACKDATA%head%tstamped)*subs%backdata%table%integtim[idump]/86400 ymark /user
  next idump
  !---------------------------------------------------------------------
  !
  !--- AntFast ---------------------------------------------------------
  if plotmjd%antfast then
    compute dims dimof subs%antfast%table%mjd
    define real myy['dims[1]']
    let myy isub+ypos[3]
    g\pen /col dark_cyan
    g\set mark 2 1 * 90  ! Vertical bar
    g\point subs%antfast%table%mjd myy
    del /var myy
  endif
  !---------------------------------------------------------------------
  !
  g\pen /co 0
next isub
g\set char 0.6
