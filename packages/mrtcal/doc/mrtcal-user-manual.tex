\documentclass{article}

\usepackage{graphicx}
\usepackage{fancyhdr}

\makeindex{}

\makeatletter
\textheight 625pt
\textwidth 460pt
\oddsidemargin 0pt
\evensidemargin 0pt
\marginparwidth 50pt
\topmargin 0pt
\brokenpenalty=10000

\newcommand{\emm}[1]{\ensuremath{#1}}
\newcommand{\emr}[1]{\emm{\mathrm{#1}}}
\newcommand{\unit}[1]{\emm{\,{#1}}}

\newcommand{\Hz}[1]{\unit{Hz}}

\newcommand{\paren}[1]  {\emm{\left(  #1 \right) }} % Parenthesis. 
\newcommand{\cbrace}[1] {\emm{\left\{ #1 \right\}}} % Curly Braces.
\newcommand{\hbrace}[1] {\emm{\left\{ #1 \right.}}  % Left Curly Brace.
\newcommand{\bracket}[1]{\emm{\left[  #1 \right] }} % Brackets.

\newcommand{\ie} {{\it i.e.}}
\newcommand{\eg} {{\it e.g.}}
\newcommand{\cf} {cf.}

\newcommand{\mjd}[1]{\emm{\emr{mjd}_{#1}}}
\newcommand{\lamoff}[1]{\emm{l_{#1}}}

\newcommand{\fortran}[1]{\texttt{#1}}
\newcommand{\imbfitskey}[1]{\texttt{#1}}
\newcommand{\command}[1]{\texttt{#1}}
\newcommand{\brand}[1]{\textbf{#1}}

\newcommand{\gildas}  {\brand{GILDAS}}
\newcommand{\class}   {\brand{CLASS}}
\newcommand{\clic}    {\brand{CLIC}}
\newcommand{\mrtcal}  {\brand{MRTCAL}}
\newcommand{\mira}    {\brand{MIRA}}
\newcommand{\classic} {\brand{CLASSIC}}
\newcommand{\cdc}     {\brand{CLASSIC} Data Container}
\newcommand{\clib}    {\brand{CLASSIC} Library}
\newcommand{\classdf} {\brand{CLASS} Data Format}
\newcommand{\pako}    {\brand{PAKO}}
\newcommand{\cfitsio} {\brand{CFITSIO}}
\newcommand{\imbfits} {\brand{IMBFITS}}

\newcommand{\nint}{\emr{nint}}
\newcommand{\nused}{\emr{USED}}
\newcommand{\spacing}{\emr{SPACING}}
\newcommand{\bandwidth}{\emm{W}}
\newcommand{\nslices}{\emm{N}}
\newcommand{\quotient}{\emm{q}}
\newcommand{\remainder}{\emm{r}}

% Makes fancy headings and footers.
\pagestyle{fancy}

\fancyhf{} % Clears all fields.

\rhead{\scshape \nouppercase{\rightmark}}   %
\fancyfoot[C]{\bfseries \thepage{}}

\renewcommand{\headrulewidth}{0pt}    %
\renewcommand{\footrulewidth}{0pt}    %

\renewcommand{\sectionmark}[1]{%
  \markright{\thesection. \MakeLowercase{#1}}}
\renewcommand{\subsectionmark}[1]{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\begin{center}
    \includegraphics[width=5cm]{gildas-logo}\\[2\bigskipamount]
  \huge{}
  IRAM Memo 2016-?\\
  \mrtcal\ User Manual\\[3\bigskipamount]
  \large{}
  S. Bardeau$^1$, J. Pety$^{1,2}$, A. Sievers$^3$\\[0.5cm]
  1. IRAM (Grenoble)\\
  2. Observatoire de Paris\\
  3. IRAM (Granada)\\[\bigskipamount]
  \normalsize{}
  July, 06$^{th}$ 2017\\
  Version 0.5
\end{center}

\begin{abstract}
  Some abstract here.

Related documents: \mrtcal\ Programmer Manual, \class\ Associated Arrays.
\end{abstract}

\newpage{}
\tableofcontents{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage{}

\section{Quick start guide}

\subsection{Overview}

\mrtcal\ offers 2 main tasks:
\begin{itemize}
\item indexing the IMB-FITS files,
\item calibrating the IMB-FITS files.
\end{itemize}

Indexing is done by creating a database of the IMB-FITS. This offers
several advantages (\eg\ opening the IMB-FITS - which is usually slow
- is factorized once at indexing time) and flexibility (\eg\ the user
can select the desired subset of IMB-FITS files). The command to be
used for this task is {\tt INDEX} (see {\tt HELP} for details), and
the database is usually named \emph{index.mrt} and saved together with
the IMB-FITS files.\\

Calibration of the IMB-FITS files has 2 major modes: per file (command
{\tt CALIBRATE}), and for Current indeX (command {\tt PIPELINE}). The
pipeline mode is an automated way to calibrate each the files in the
Current indeX, selecting the calibration scans, etc.\\

In addition, \mrtcal\ offers a collection of tuning options with the
command {\tt MSETUP}. For a first approach, you may be interested in
the groups {\tt MSETUP CALIBRATION} (\ie\ tune the way the calibration
is performed) and {\tt MSETUP OUTPUT} (\ie\ what results and how they
are saved in the \class\ file). See dedicated HELP for exhaustive
description.

\subsection{A simple session (at home)}

Assuming the IMB-FITS files are in a local folder with no particular
restriction, the simplest \mrtcal\ session which you may think of is
the following one:
\begin{verbatim}
MRTCAL> index build      ! Build the index of IMB-FITS files in the current directory
MRTCAL> mfind            ! Select all the files for calibration
MRTCAL> mlist            ! Optionally list the files to be calibrated
MRTCAL> file out myclassfile.30m single  ! Open a new Class output file
MRTCAL> pipeline         ! Calibrate all the files
\end{verbatim}
After the processing, you may want to list again the IMB-FITS files,
with their updated calibration status:
\begin{verbatim}
MRTCAL> mfind            ! Update the list of files
MRTCAL> mlist            ! List the files and their calibration status
\end{verbatim}

After the {\tt PIPELINE} step, the \class\ file {\tt myclassfile.30m}
contains all the \emph{science} spectra (with the scientific target as
source name), but also \emph{calibration} spectra (with the source
name {\tt CALSKY}). See subsection~\ref{txt:products} for details. The
{\tt PIPELINE} command has run using the current defaults, but there
are a number of tunings which can be modified. See {\tt HELP MSET} for
details, and type {\tt MSET} to see their current values.

\subsection{A simple session (at Pico Veleta)}

If you want to recalibrate the IMB-FITS files in your project account
at Pico Veleta (i.e. located in {\tt
  $\sim$/observationData/imbfits/}), you must consider that you have
no write access to this directory. In this case, you can not put the
{\tt index.mrt} file together with the IMB-FITS files. The solution is
to create it \emph{elsewhere} (it will be your responsibility to
remember the association between the index file and the directory
where the IMB-FITS files can be found):

\begin{verbatim}
! Go to working local directory (with all permissions):
MRTCAL> sic directory some/working/dir/

! Build the index (named myindex.mrt in current working directory)
! for IMB-FITS files found in ~/observationData/imbfits/
MRTCAL> index build ~/observationData/imbfits/ /file myindex.mrt
\end{verbatim}

Then the subsequent commands are identical to the previous section. If
you need to update or reopen the index file without rebuilding it from
scratch, the corresponding commands are respectively:
\begin{verbatim}
MRTCAL> index update  ~/observationData/imbfits/ /file myindex.mrt

MRTCAL> index open  ~/observationData/imbfits/ /file myindex.mrt
\end{verbatim}

\section{Indexing}

\subsection{Indexing and listing a directory}

The easiest indexing method is the following one:
\begin{verbatim}
MRTCAL> index build  ! Build the index of IMB-FITS files in the current directory
\end{verbatim}
The {\tt INDEX} command offers many possibilities, such as recursive
indexing, restricting to some dates or patterns, indexing read-only
directories, waiting for new files to appear... See {\tt HELP INDEX}
for details.\\

Once the index is built, you can access its contents with \eg\
\begin{verbatim}
MRTCAL> index open                  ! (Re)open the index file
MRTCAL> mfind                       ! Select all the entries in the index
MRTCAL> mlist                       ! Sequential list
MRTCAL> mlist /toc                  ! Summarized list
MRTCAL> mlist /column number projid ! Custom list
\end{verbatim}
{\tt MFIND} and {\tt MLIST} offer a variety of options so that you can
check IMB-FITS contents and setup a list of scans to be calibrated.

\subsection{Entry numbering and versioning}

\subsubsection{Numbering}

When \mrtcal\ opens an index file (usually index.mrt), it builds what
is called the \emph{input index} in memory. However, for many reasons
the contents of the index file may be disordered. In order to give
friendly lists to the user, the input index is implicitly sorted by
observing date, then scan number, then backend
identifier\footnote{Backend identifiers are ordered
  chronologically.}. For example, the following files are ordered like
this:
\begin{verbatim}
MRTCAL> $ls *.fits
iram30m-4mhz-20100930s200-imb.fits  iram30m-fts-20100930s201-imb.fits
iram30m-4mhz-20100930s201-imb.fits  iram30m-wilma-20100930s200-imb.fits
iram30m-fts-20100930s200-imb.fits   iram30m-wilma-20100930s201-imb.fits
MRTCAL> mlist in  ! "in" lists the input index
  N.V ProjId    Source         Date          UT     Scn Backe  ObsType    Swi Calibr
  1.1 054-09   CONTROLD     30-SEP-2010  20:52:47.0 200 4MHZ  CALIBRATE   PSW NONE
  2.1 054-09   CONTROLD     30-SEP-2010  20:52:47.0 200 WILMA CALIBRATE   PSW NONE
  3.1 054-09   CONTROLD     30-SEP-2010  20:52:47.0 200 FTS   CALIBRATE   PSW NONE
  4.1 054-09   CONTROLD     30-SEP-2010  20:53:37.0 201 4MHZ  TRACKED     PSW NONE
  5.1 054-09   CONTROLD     30-SEP-2010  20:53:37.0 201 WILMA TRACKED     PSW NONE
  6.1 054-09   CONTROLD     30-SEP-2010  20:53:37.0 201 FTS   TRACKED     PSW NONE
\end{verbatim}

From the input index, the user has to build the \emph{current
  index}\footnote{The current index is the list of entries to be
  processed by the command {\tt PIPELINE}.} with {\tt MFIND}. They can
be identical ({\tt MFIND} called without argument), or the current
index can be a subset of the input index. For example:
\begin{verbatim}
MRTCAL> mfind /backend 4mhz
I-FIND,  2 entries in Current indeX
MRTCAL> mlist  ! No argument: list the current index
  N.V ProjId    Source         Date          UT     Scn Backe  ObsType    Swi Calibr
  1.1 054-09   CONTROLD     30-SEP-2010  20:52:47.0 200 4MHZ  CALIBRATE   PSW NONE
  4.1 054-09   CONTROLD     30-SEP-2010  20:53:37.0 201 4MHZ  TRACKED     PSW NONE
\end{verbatim}

The number $N$ which can be seen in the first column is used by
several commands to identify the file to be processed (for example,
{\tt CALIBRATE 1}). In most of the cases, especially when dealing with
a data set which does not evolve, using this number in procedures is
acceptable. However, there are some use cases where one needs to pay
attention:
\begin{itemize}
\item {\tt INDEX UPDATE}: if new files are to be added to an old index
  file, they will be inserted according to the sorting rule described
  earlier. If those files were observed after the old ones (\eg\ if
  adding a new observation session), they will be appended to the end
  of the index, so this makes no problem. But if a file has to be
  inserted before, this will modify the entry numbers.
\item {\tt INDEX OPEN /RECURSIVE}: when opening several directories
  (one index file each), one has to be careful that, again, all the
  files will be sorted with the same rule. This basically means that
  the entry numbers which are seen when opening a single directory are
  not the same when this directory is opened together with other
  directories.
\end{itemize}

\subsubsection{Versioning}

The first time an IMB-FITS file is indexed, it is given a version
number, starting at 1. Then, when it is calibrated, a new version of
the entry is added in the index. For example, using the same example
as before:

\begin{verbatim}
MRTCAL> calibrate 1

Calibrating   1.1 054-09   CONTROLD     30-SEP-2010  20:52:47.0 200 4MHZ  CALIBRATE   PSW NONE
[calibration messages]
\end{verbatim}
yields
\begin{verbatim}
MRTCAL> mlist in
  N.V ProjId    Source         Date          UT     Scn Backe  ObsType    Swi Calibr
  1.1 054-09   CONTROLD     30-SEP-2010  20:52:47.0 200 4MHZ  CALIBRATE   PSW NONE
  1.2 054-09   CONTROLD     30-SEP-2010  20:52:47.0 200 4MHZ  CALIBRATE   PSW DONE E0HLI:254 E0VLI:198
  2.1 054-09   CONTROLD     30-SEP-2010  20:52:47.0 200 WILMA CALIBRATE   PSW NONE
  3.1 054-09   CONTROLD     30-SEP-2010  20:52:47.0 200 FTS   CALIBRATE   PSW NONE
  4.1 054-09   CONTROLD     30-SEP-2010  20:53:37.0 201 4MHZ  TRACKED     PSW NONE
  5.1 054-09   CONTROLD     30-SEP-2010  20:53:37.0 201 WILMA TRACKED     PSW NONE
  6.1 054-09   CONTROLD     30-SEP-2010  20:53:37.0 201 FTS   TRACKED     PSW NONE
\end{verbatim}
One can see that a new entry, numbered 1.2, has been added to the
input index. It is identical to the version 1.1, except that its
calibration status is now {\tt DONE} (and the calibration results are
saved in the index file). The version 1.2 was inserted just
after the version 1.1 so that the correct sorting is kept.\\

Beware that, when a new entry is added to the input index, the current
index is NOT udpated. One has to call {\tt MFIND} again. Note that
{\tt MFIND} finds only the latest versions of each entry:
\begin{verbatim}
MRTCAL> mfind /backend 4mhz
I-FIND,  2 entries in Current indeX
MRTCAL> mlist
  N.V ProjId    Source         Date          UT     Scn Backe  ObsType    Swi Calibr
  1.2 054-09   CONTROLD     30-SEP-2010  20:52:47.0 200 4MHZ  CALIBRATE   PSW DONE E0HLI:254 E0VLI:198
  4.1 054-09   CONTROLD     30-SEP-2010  20:53:37.0 201 4MHZ  TRACKED     PSW NONE
\end{verbatim}

Consider also that an entry can be calibrated several times, \eg\ to
test the effect of a modified calibration tuning. Each of these
calibrations will increase the version number, producing a new entry
in the index, memorizing its calibration results.


\section{Calibrating}

\subsection{Processing all a directory}

The easiest \mrtcal\ session is:
\begin{verbatim}
MRTCAL> index build      ! Build the index of IMB-FITS files in the current directory
MRTCAL> mfind            ! Select all the files for calibration
MRTCAL> mlist            ! Optionally list the files to be calibrated
MRTCAL> file out myclassfile.30m single  ! Open a new Class output file
MRTCAL> pipeline         ! Calibrate all the files
\end{verbatim}
This calibrates all the IMB-FITS files present in the current
directory. The command {\tt MFIND} is used to build the \emph{current
  index} from all the files in the \emph{input index}. The command
{\tt PIPELINE} processes all, and only, the entries in the current
index. It chooses automatically how the calibration and science scans
are paired. As a result, the calibrated spectra are saved in the named
\class\ file and the index is updated to reflect the calibration
status of the IMB-FITS files.

\subsection{Processing a single IMB-FITS file}

In order to calibrate a single science scan, one has to use the
command {\tt CALIBRATE}:
\begin{verbatim}
MRTCAL> index build  ! Build index of files in current directory
MRTCAL> mfind        ! Selects all the entries in index
MRTCAL> mlist        ! List the entries in the Current indeX
MRTCAL> file out myclassfile single  ! The usual CLASS command
MRTCAL> calibrate 1  ! Where "1" is the entry number of the calibration scan
MRTCAL> calibrate 2  ! Where "2" is the entry number of the science scan
\end{verbatim}
The 2 last lines can also be merged in a single call:
\begin{verbatim}
MRTCAL> calibrate 2 /with 1
\end{verbatim}
As a result, only the 2 named scans are calibrated. The entries in the
index file are also updated to reflect that their calibration has been
done.

\subsection{Calibration status}

When an IMB-FITS file is first indexed, its calibration status is set
as {\tt NONE}. For example:
\begin{verbatim}
  N.V ProjId    Source         Date          UT     Scn Backe  ObsType    Swi Calibr
  1.1 054-09   CONTROLD     30-SEP-2010  20:52:47.0 200 4MHZ  CALIBRATE   PSW NONE
\end{verbatim}
When it is successfully calibrated, its status is set as {\tt DONE}:
\begin{verbatim}
  N.V ProjId    Source         Date          UT     Scn Backe  ObsType    Swi Calibr
  1.2 054-09   CONTROLD     30-SEP-2010  20:52:47.0 200 4MHZ  CALIBRATE   PSW DONE E0HLI:254 E0VLI:198
\end{verbatim}

However, in case of failure during the calibration, the status is set
as {\tt FAILED}. It can be useful to find all the failed calibrations
for a closer look. This can be achieved with the command:
\begin{verbatim}
MRTCAL> MFIND /CALIBRATION FAILED
\end{verbatim}

It can also happen that an IMB-FITS file has no useful data. The file
structure is correct but either it contains no subscan, or no {\tt ON}
subscan, or no data dumps within the susbcans. In these cases, the
calibration does not fail (from the computing point of view) but there
is nothing relevant which can be done from these files: calibration
scans can not be used, and science scans produce no spectra. Their
calibration status is set as {\tt EMPTY}.\\

Note that the {\tt PIPELINE} command chooses automatically which
calibration scan it pairs with a science scan. In particular, it
ignores all the {\tt FAILED} and {\tt EMPTY} calibration scans, which
means either it uses a {\tt DONE} calibration scan, or it processes
the desired one if not yet done.

\subsection{Calibration products}
\label{txt:products}

By default, after processing a calibration scan, \mrtcal\ will save
the calibration products in the \class\ output file\footnote{This can
  be disabled by setting {\tt MSET CALIBRATION PRODUCT NONE} before
  processing the calibration scans.}. They will thus be available to
the user when he opens the \class\ file for reading. For example,
after calibrating iram30m-4mhz-20100930s200-imb.fits and
iram30m-4mhz-20100930s201-imb.fits one can see:

\begin{verbatim}
MRTCAL> file in 09054.30m
I-CONVERT,  File is  [Native]
I-INPUT,  09054.30m successfully opened
MRTCAL> find
I-FIND,  14 observations found
MRTCAL> list
Current index contains:
 N;V Source       Line         Telescope      Lambda     Beta Sys  Sca Sub
 1;1 CALSKY       NULL         30ME0HLI-4M1   -600.0     +0.0 Eq   200 1
 2;1 CALSKY       NULL         30ME0VLI-4M2   -600.0     +0.0 Eq   200 1
 3;1 CONTROLD     NULL         30ME0HLI-4M1     +0.0     +0.0 Eq   201 2
 4;1 CONTROLD     NULL         30ME0VLI-4M2     +0.0     +0.0 Eq   201 2
 5;1 CONTROLD     NULL         30ME0HLI-4M1     +0.0     +0.0 Eq   201 4
 6;1 CONTROLD     NULL         30ME0VLI-4M2     +0.0     +0.0 Eq   201 4
 7;1 CONTROLD     NULL         30ME0HLI-4M1     +0.0     +0.0 Eq   201 6
 8;1 CONTROLD     NULL         30ME0VLI-4M2     +0.0     +0.0 Eq   201 6
 9;1 CONTROLD     NULL         30ME0HLI-4M1     +0.0     +0.0 Eq   201 8
10;1 CONTROLD     NULL         30ME0VLI-4M2     +0.0     +0.0 Eq   201 8
11;1 CONTROLD     NULL         30ME0HLI-4M1     +0.0     +0.0 Eq   201 10
12;1 CONTROLD     NULL         30ME0VLI-4M2     +0.0     +0.0 Eq   201 10
13;1 CONTROLD     NULL         30ME0HLI-4M1     +0.0     +0.0 Eq   201 12
14;1 CONTROLD     NULL         30ME0VLI-4M2     +0.0     +0.0 Eq   201 12
\end{verbatim}

The calibration products are saved as \class\ spectra with source name
{\tt CALSKY}. There are as many of them as frontend-backend
combination (\emph{telescope} name in \class). In order to find only
the science data, user has to find explicitly his source name, \eg:
\begin{verbatim}
MRTCAL> find /source controld
I-FIND,  12 observations found
\end{verbatim}

A {\tt CALSKY} entry is a spectrum providing the CalSky load as the
main data ({\tt RY}), plus a collection of Associated Arrays:
\begin{verbatim}
MRTCAL> find /source calsky
I-FIND,  2 observations found
MRTCAL> get first
I-GET,  Observation 1; Vers 1 Scan 200
MRTCAL> dump
[...]
 ASSOCIATED ARRAYS -----------------------------------------
  Number of associated arrays: 7
  Associated array #1: CALAMBIENT
    Unit:               Format: -11  Bad:   -1000.000  Dimensions: 1006 x 0
    Data:    10158.63       9447.842       ...      17594.74       15591.32
  Associated array #2: CALCOLD
    Unit:               Format: -11  Bad:   -1000.000  Dimensions: 1006 x 0
    Data:    5958.474       5238.263       ...      7394.842       7605.105
  Associated array #3: TREC
    Unit:               Format: -11  Bad:   -1000.000  Dimensions: 1006 x 0
    Data:    304.3947       304.3947       ...      177.7052       177.7052
  Associated array #4: TCAL
    Unit:               Format: -11  Bad:   -1000.000  Dimensions: 1006 x 0
    Data:    243.2139       243.2139       ...      249.4267       249.4267
  Associated array #5: TSYS
    Unit:               Format: -11  Bad:   -1000.000  Dimensions: 1006 x 0
    Data:    424.9489       423.0339       ...      279.2844       283.7039
  Associated array #6: TAUZEN
    Unit:               Format: -11  Bad:   -1000.000  Dimensions: 1006 x 0
    Data:   6.2096495E-02  6.2096495E-02   ...     5.5034418E-02  5.5034418E-02
  Associated array #7: FLAG
    Unit:               Format: -11  Bad:   -1000.000  Dimensions: 1006 x 0
    Data:    0.000000       0.000000       ...      0.000000       0.000000
\end{verbatim}
Those 8 arrays provide useful information as a function of frequency:
\begin{itemize}
\item CalSky, CalAmbient, CalCold: the calibration loads,
\item Trec, Tcal, Tsys: the receiver, calibration, and system temperatures,
\item TauZen: the zenithal opacity,
\item Flag: a flag array indicating if the data is reliable (0) or not
  (1). As of today, \mrtcal\ flags all the channels of a chunk if any
  error occurs when performing the chopper calibration (e.g. sky
  greater than hot load).
\end{itemize}
The Figure~\ref{fig:products} shows an example of these arrays.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.5\textwidth]{calib-products-loads}
  \includegraphics[width=0.5\textwidth]{calib-products-temp}
  \includegraphics[width=0.5\textwidth]{calib-products-tauzen}
  \includegraphics[width=0.5\textwidth]{calib-products-flag}
  \caption{Calibration products as found after calibrating the file
    iram30m-4mhz-20100930s200-imb.fits. The default calibration
    bandwidth was used (20 MHz).}
  \label{fig:products}
\end{figure}
\clearpage

\subsection{Weight array}

\mrtcal\ offers the possibility to associate a weight array
(Associated Array W) together with the spectrum intensities ({\tt
  RY}). This weight varies in frequency. The weight at channel $i$ is:
\begin{equation}
  W(i) = \frac{t \times | \delta f |}{(T_{sys}(i) \times e^{A \tau (i)})^2}
\end{equation}
where $t$, $\delta f$ and $A$ are respectively the integration time
(seconds), the frequency resolution (MHz) and the airmass (same scalar
values shared by all channels), and $\tau (i)$ is the opacity at
channel $i$. $T_{sys}(i)$ is the \emph{zenithal} system temperature,
which can be found in the calibration products from a calibration
scan (see subsection~\ref{txt:products}).\\

By default, \mrtcal\ does not save the weight array. This is activated
by the command:
\begin{verbatim}
MRTCAL> MSET OUTPUT WEIGHT YES
\end{verbatim}
Beware that if enabled, this writes twice more data per spectrum,
which typically doubles the \class\ file size! An example of a weight
array is plotted in the Figure~\ref{fig:weight}, produced with the
following commands:
\begin{verbatim}
LAS> file in 09054                                                        
I-CONVERT,  File is  [Native]                                             
I-INPUT,  09054.30m successfully opened                                   
LAS> find /source controld                                                
I-FIND,  12 observations found                                            
LAS> get first                                                               
I-GET,  Observation 3; Vers 1 Scan 201
LAS> plot w
\end{verbatim}

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.7\textwidth]{weight-array}
  \caption{Weight array associated to a spectrum produced from the
    file iram30m-4mhz-20100930s200-imb.fits.}
  \label{fig:weight}
\end{figure}
\clearpage

\section{Exploring IMB-FITS files (no calibration)}

\subsection{FITS reader}

Using \mrtcal\ as a FITS reader can be done with a few commands, \eg:
\begin{verbatim}
MRTCAL> read iram30m-fts-20120128s233-imb.fits /file /subscan 2 ! Load the HDUs for subscan 2
MRTCAL> mdump                  ! Summary of the file HDUs
MRTCAL> mdump antslow          ! Details of the AntSlow HDU for the current subscan
MRTCAL> mdump antslow longoff  ! The LONGOFF table for the current subscan
MRTCAL> variable               ! Create the related Sic variables, e.g.
MRTCAL> examine subs%antslow%table%longoff  ! the same as Sic array
\end{verbatim}
See {\tt HELP} for details and options about these commands. The
procedures in the next subsections are extensively based on this
IMB-FITS reader.

\subsection{Display calibration loads}

The loads of a calibration scan can be displayed in a plot with \eg\
the command:
\begin{verbatim}
MRTCAL> @ plot-loads iram30m-4mhz-20100930s200-imb.fits
\end{verbatim}
The result is shown in the Figure~\ref{fig:loads}. It shows the load
values as found in the {\tt DATA} column of each subscan. They can be
averaged (\ie\ display a single average load) or not (\ie\ display all
the single dumps) thanks to the option {\tt /AVERAGE YES|NO} (default
is {\tt YES}).
\begin{figure}[htb]
  \centering
  \includegraphics[width=\textwidth]{plot-loads}
  \caption{Resulting plot of the procedure {\tt @ plot-loads}. The X
    axis is not increasing frequency, but chunks as found in the
    IMB-FITS. They have to be reordered and reoriented to get the
    final frequency axis. The mean load value per chunk is also
    displayed in black, overlaid to each chunk load curve.}
  \label{fig:loads}
\end{figure}

\clearpage
\subsection{Display MJD values and tables}

The headers and tables found in the IMB-FITS file rely heavily on
time-stamps described with Modified Julian Date (MJD). This includes
subscan start and stop, instantaneous antenna positions during the
subscan, dump sampling during integration. These quantities can be
displayed in a graphical way with \eg\ the following command:
\begin{verbatim}
MRTCAL> @ plot-mjd iram30m-4mhz-20100930s200-imb.fits
\end{verbatim}
The result is shown in the Figure~\ref{fig:mjd}.

\begin{figure}[htb]
  \centering
  \includegraphics[width=\textwidth]{plot-mjd}
  \caption{Resulting plot of the procedure {\tt @ plot-mjd}, showing
    the subscans start and stop (black boxes), the time stamps of the
    AntSlow dumps in each subscan (blue crosses), and the time stamps
    of the derotator dumps if relevant (magenta crosses). Each DATA
    dump is displayed as an horizontal segment showing its actual
    integration time. The associated time stamp is displayed as a
    vertical segment (the time stamp can be found anywhere from the
    beginning to the end of the integration, according to the header
    value {\tt TSTAMPED}). Colors are alternated to avoid confusion.}
  \label{fig:mjd}
\end{figure}


\clearpage
\newpage
\section{MRTCAL Language Internal Help}
\input{mrtcal-help-mrtcal}

\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

