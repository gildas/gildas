!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module image_sampling_head
  !
  interface
     subroutine sampling_definition(sky_head, b_width, point_per_beam, &
          gposition, nxy, first_point, sampling)
       use image_def
       type (gildas), intent(in) :: sky_head
       real, intent(in) :: b_width
       integer, intent(in) :: point_per_beam, gposition
       integer, dimension(2), intent(out) :: nxy
       real, dimension(2), intent(out) :: first_point, sampling
     end subroutine sampling_definition
  end interface
  !
  interface
     subroutine sample_definition(nxy, first_point, sampling, &
          nant, sample_data)
       integer, intent(in) :: nant
       integer, dimension(2), intent(in) :: nxy
       real, dimension(2), intent(in) :: first_point, sampling
       real, dimension(:,:,:), intent(out) :: sample_data
     end subroutine sample_definition
  end interface
  !
end module image_sampling_head
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
program image_sampling
  use image_sampling_head
  use phys_const
  use gkernel_interfaces
  use image_def
  !
  type (gildas) :: sky_head, sample_head
  character (len = 15) :: rname = 'IMAGE_SAMPLING'
  integer, dimension(2) :: nxy
  integer :: stat, nant, point_per_beam, gposition
  real, dimension(:,:),   allocatable, save :: sky_data
  real, dimension(:,:,:), allocatable, save :: sample_data
  real, dimension(2) :: first_point, sampling
  real :: nu, diameter, b_width
  logical :: error = .false.
  !
  call gildas_null(sky_head)
  call gildas_null(sample_head)
  !
  call gildas_open
  call gildas_char('SKY$',sky_head%file)
  call gildas_char('SAMPLE$',sample_head%file)
  call gildas_inte('NANT$',nant,1)
  call gildas_real('DIAM$',diameter,1)
  call gildas_real('FREQ$',nu,1)
  call gildas_inte('POINT_PER_BEAM$',point_per_beam,1)
  call gildas_inte('GPOSITION$',gposition,1)
  call gildas_close
  !
  if ((nu <= 0.0).or.(diameter <= 0.0)) then
     call gagout('F-'//rname//',  Bad value(s) for frequency and/or diameter')
     call sysexi(fatale)
  endif
  !
  if ((gposition /= 1) .and. (gposition /= 2)) then
     call gagout('F-'//rname//',  Unsupported grid position')
     call sysexi(fatale)
  endif
  !
  ! Reference: 63" for 100 GHz and 12m.
  !
  nu = nu*1e9
  b_width = rad_per_sec*7.56e13/(nu*diameter)
  print *, '  Beam width: ', b_width
  !
  ! Reads sky header, allocates memory space and reads sky data.
  !
  call gdf_read_header(sky_head,error)
  if (gildas_error(sky_head,rname,error)) call sysexi(fatale)
  !
  allocate(sky_data(sky_head%gil%dim(1),sky_head%gil%dim(2)), &
       stat = sky_head%status)
  if (gildas_error(sky_head,rname,error)) call sysexi(fatale)
  !
  call gdf_read_data(sky_head,sky_data,error)
  if (gildas_error(sky_head,rname,error)) call sysexi(fatale)
  !
  ! Defines the sampling rate, the number of points and the first point.
  !
  call sampling_definition(sky_head, b_width, point_per_beam, gposition, &
       nxy, first_point, sampling)
  !
  ! Allocates memory space.
  !
  allocate(sample_data(product(nxy),nant,2), stat = sample_head%status)
  if (gildas_error(sample_head,rname,error)) call sysexi(fatale)
  !
  ! Makes sample.
  !
  call sample_definition(nxy, first_point, sampling, nant, sample_data)
  !
  ! Updates header.
  ! Keeps memory of the sampling grid in the 1st and 2nd axis.
  !
  sample_head%char%name = sky_head%char%name
  sample_head%gil%ndim  = 3
  sample_head%gil%dim(1:3) = shape(sample_data)
  !
!  sample_head%gil%dim(4) = nxy(1)
  sample_head%gil%ref(1)   = 1
  sample_head%gil%val(1)   = first_point(1)
  sample_head%gil%inc(1)   = sampling(1)
  sample_head%gil%ref(2)   = 1
  sample_head%gil%val(2)   = first_point(2)
  sample_head%gil%inc(2)   = sampling(2)
  !
  ! Writes it on disk.
  !
  call gdf_write_image(sample_head,sample_data,error)
  if (gildas_error(sample_head,rname,error)) call sysexi(fatale)
  !
  ! Frees memory space and returns.
  !
  deallocate(sky_data, sample_data, stat = sky_head%status)
  if (gildas_error(sky_head,rname,error)) call sysexi(fatale)
  !
  call gagout('I-'//rname//',  Successful completion')
  !
end program image_sampling
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine sampling_definition(sky_head, b_width, point_per_beam, &
     gposition, nxy, first_point, sampling)
  use image_def
  type (gildas), intent(in) :: sky_head
  real, intent(in) :: b_width
  integer, intent(in) :: point_per_beam, gposition
  integer, dimension(2), intent(out) :: nxy
  real, dimension(2), intent(out) :: first_point, sampling
  !
  real, dimension(2) :: sky_size,  sky_pix, sky_val, sky_ref, sky_blc
  real, dimension(2) :: data_size, data_blc, leftover
  real :: beam_size
  !
  ! Computes needed quantities.
  !
  sky_size(1) = abs(sky_head%gil%inc(1)*sky_head%gil%dim(1))
  sky_size(2) = abs(sky_head%gil%inc(2)*sky_head%gil%dim(2))
  !
  sky_pix(1) = sky_head%gil%inc(1)
  sky_pix(2) = sky_head%gil%inc(2)
  !
  sky_val(1) = sky_head%gil%val(1)
  sky_val(2) = sky_head%gil%val(2)
  !
  sky_ref(1) = sky_head%gil%ref(1)
  sky_ref(2) = sky_head%gil%ref(2)
  !
  sky_blc = (1-sky_ref)*sky_pix+sky_val
  !
  beam_size = 8.0*b_width/(2.0*sqrt(log(2.0)))
  data_size = sky_size+beam_size
  data_blc  = sky_blc-sign(0.5*beam_size,sky_pix)
  !
  ! Defines the sampling rate, the number of points and the first point.
  ! Makes the sampling either centered or passing by reference point.
  !
  sampling = b_width/point_per_beam
  sampling = sign(sampling,sky_pix)
  !
  if (gposition == 1) then
     nxy = 1+int(abs(data_size/sampling))
     leftover = mod(data_size,sampling)
     first_point = data_blc+0.5*sign(leftover,sky_pix)
  else
     nxy = int(abs(data_size/sampling))
     leftover = mod((sky_val-data_blc),sampling)
     first_point = data_blc+sign(leftover,sky_pix)
  endif
  !
end subroutine sampling_definition
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine sample_definition(nxy, first_point, sampling, nant, sample_data)
  integer, intent(in) :: nant
  integer, dimension(2), intent(in) :: nxy
  real, dimension(2), intent(in) :: first_point, sampling
  real, dimension(:,:,:), intent(out) :: sample_data
  !
  integer :: i, j, stat
  real, dimension(2,0:(nxy(1)-1),0:(nxy(2)-1)) :: xy_coord
  !
  ! Makes the sample.
  !
  do j = 0, nxy(2)-1
     do i = 0, nxy(1)-1
        xy_coord(:,i,j) = first_point+sampling*real((/ i, j /))
     end do
  end do
  !
  ! Packs the sample as a 1D array of positions.
  !
  do i = 1, nant
     sample_data(:,i,1) = pack(xy_coord(1,:,:),.true.)
     sample_data(:,i,2) = pack(xy_coord(2,:,:),.true.)
  end do
  !
end subroutine sample_definition
!
!!!!!!!!!!!!!!!!!! image_sampling.f90 ends here. !!!!!!!!!!!!!!!!!!!!!!!!!!
