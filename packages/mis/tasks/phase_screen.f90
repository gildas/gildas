!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module phase_screen_head
  !
  interface
     subroutine phase_screen_building(short_screen,long_screen, &
          short_averag,long_averag,sf_value_300m,exponents,ranges,&
          fourier_space,diameter,sf_verification,error)
       use image_def
       type (gildas), intent(inout) :: short_screen, long_screen
       type (gildas), intent(inout) :: short_averag, long_averag
       logical,               intent(out)   :: error
       logical,               intent(in)    :: fourier_space
       logical,               intent(in)    :: sf_verification
       real,                  intent(in)    :: sf_value_300m, diameter
       real,    dimension(3), intent(inout) :: exponents
       real,    dimension(2), intent(inout) :: ranges
     end subroutine phase_screen_building
  end interface
  !
  interface
     subroutine short_phase_screen_definition(screen,averag,nxy, &
          sf_value_300m,exponents,ranges,fourier_space,diameter,error)
       use image_def
       type (gildas), intent(inout) :: screen, averag
       logical,               intent(out)   :: error
       logical,               intent(in)    :: fourier_space
       integer, dimension(2), intent(in)    :: nxy
       real,                  intent(in)    :: sf_value_300m, diameter
       real,    dimension(3), intent(inout) :: exponents
       real,    dimension(2), intent(inout) :: ranges
     end subroutine short_phase_screen_definition
  end interface
  !
  interface
     subroutine screen_binning(short_screen,long_screen,nxy,error)
       use image_def
       type (gildas), intent(in)  :: short_screen
       type (gildas), intent(inout) :: long_screen
       integer, dimension(2), intent(in) :: nxy
       logical, intent(out) :: error
     end subroutine screen_binning
  end interface
  !
  interface
     subroutine structure_function_computation(data,sf_value_300m,sf_name, &
          verification,error)
       use image_def
       type (gildas),    intent(inout) :: data
       logical,          intent(out)   :: error
       logical,          intent(in)    :: verification
       character(len=*), intent(in)    :: sf_name
       real,             intent(in)    :: sf_value_300m
     end subroutine structure_function_computation
  end interface
  !
  interface
     subroutine gradient_computation(image,error)
       use image_def
       type (gildas), intent(inout) :: image
       logical,         intent(out) :: error
     end subroutine gradient_computation
  end interface
  !
end module phase_screen_head
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
program phase_screen
  use image_def
  use phys_const
  use phase_screen_head, only : phase_screen_building, gradient_computation
  use gkernel_interfaces
  !
  type (gildas) :: s_screen_head, l_screen_head
  type (gildas) :: s_averag_head, l_averag_head
  character (len = 16) :: rname = 'PHASE_SCREEN'
  logical :: error = .false.
  logical :: fourier_space, sf_verification
  integer, dimension(2) :: s_nxy, l_nxy
  integer :: nscreens
  real, dimension(:,:,:), allocatable, save, target :: l_screen_data, l_averag_data
  real, dimension(:,:,:), allocatable, save, target :: s_screen_data, s_averag_data, l_gradient_data
  real, dimension(3) :: ranges, exponents
  real, dimension(2) :: s_screen_size, l_screen_size, pix_size
  real :: sf_value_300m,  diameter
  !
  ! Initializes gildas headers.
  !
  call gildas_null(s_screen_head)
  call gildas_null(s_averag_head)
  call gildas_null(l_screen_head)
  call gildas_null(l_averag_head)
  !
  ! Gets input parameters and verifies them.
  !
  call gildas_open
  call gildas_char('LS_FILENAME$',l_screen_head%file)
  call gildas_char('LA_FILENAME$',l_averag_head%file)
  call gildas_char('SS_FILENAME$',s_screen_head%file)
  call gildas_char('SA_FILENAME$',s_averag_head%file)
  call gildas_real('PIX_SIZE$',pix_size,2)
  call gildas_real('L_SCREEN_SIZE$',l_screen_size,2)
  call gildas_real('S_SCREEN_SIZE$',s_screen_size,2)
  call gildas_real('SF_VALUE_300M$',sf_value_300m,1)
  call gildas_real('EXPONENTS$',exponents,3)
  call gildas_real('BASE_RANGES$',ranges,2)
  call gildas_logi('FOURIER_SPACE$',fourier_space,1)
  call gildas_real('DIAM$',diameter,1)         ! in m.
  call gildas_logi('SF_VERIF$',sf_verification,1)
  call gildas_close
  !
  if (pix_size(1) /= pix_size(2)) then
     call gagout('F-'//rname//',  '// &
          'I need square pixels, not rectangular ones')
     call sysexi(fatale)
  endif
  !
  if (any(pix_size == 0.0).or.&
      any(s_screen_size == 0.0).or.&
      any(l_screen_size == 0.0)) then
     call gagout('F-'//rname//',  '// &
          'Pixel and/or screen size are 0.0')
     call sysexi(fatale)
  endif
  !
  if (any(s_screen_size.le.300.0)) then
     call gagout('F-'//rname//',  '// &
          'Short screen size must be greater than 300m')
     call sysexi(fatale)
  endif
  !
  ! Input parameters define the square root of the 2nd order structure
  ! function. Here we will work with the structure function.
  !
  sf_value_300m = sf_value_300m**2
  exponents = 2.0*exponents
  !
  ! Defines screen sizes.
  !
  !   1. Short screen.
  !
  s_nxy = 2**int(ceiling(log(abs(s_screen_size/pix_size))/log(2.0)))
  s_screen_size = pix_size*s_nxy
  !
  !   2. Long screen.
  !
  l_nxy = int(ceiling(abs(l_screen_size/pix_size)))
  !
  ! Defines the number of short screens needed to make a long screen.
  !
  nscreens = int(ceiling(2.0*real(l_nxy(1))/real(s_nxy(1))))
  if (nscreens.le.2) then
     l_nxy(1) = s_nxy(1)
     nscreens = 1
  else
     l_nxy(1) = nscreens*(s_nxy(1)/2)
  endif
  !
!  if (l_nxy(2).gt.s_nxy(2)) l_nxy(2) = s_nxy(2)
  l_nxy(2) = s_nxy(2)
  l_screen_size = pix_size*l_nxy
  !
  ! Defines headers.
  !
  !   1. Short screen.
  !
  s_screen_head%char%name = 'Phase Screen'
  s_screen_head%gil%ndim = 3
  s_screen_head%gil%dim(1:2) = s_nxy
  s_screen_head%gil%dim(3) = nscreens
  s_screen_head%gil%ref(1) = s_nxy(1)/2+1
  s_screen_head%gil%val(1) = 0.0
  s_screen_head%gil%inc(1) = pix_size(1)
  s_screen_head%gil%ref(2) = s_nxy(2)/2+1
  s_screen_head%gil%val(2) = 0.0
  s_screen_head%gil%inc(2) = pix_size(2)
  !
  s_averag_head%char%name = 'Avg Screen'
  s_averag_head%gil%ndim = 3
  s_averag_head%gil%dim(1:2) = s_nxy
  s_averag_head%gil%dim(3) = nscreens
  s_averag_head%gil%ref(1) = s_nxy(1)/2+1
  s_averag_head%gil%val(1) = 0.0
  s_averag_head%gil%inc(1) = pix_size(1)
  s_averag_head%gil%ref(2) = s_nxy(2)/2+1
  s_averag_head%gil%val(2) = 0.0
  s_averag_head%gil%inc(2) = pix_size(2)
  !
  !   2. Long screen.
  !
  l_screen_head%char%name = 'Phase Screen'
  l_screen_head%gil%ndim = 3
  l_screen_head%gil%dim(1:2) = l_nxy
  l_screen_head%gil%dim(3) = 3
  l_screen_head%gil%ref(1) = l_nxy(1)/2+1
  l_screen_head%gil%val(1) = 0.0
  l_screen_head%gil%inc(1) = pix_size(1)
  l_screen_head%gil%ref(2) = l_nxy(2)/2+1
  l_screen_head%gil%val(2) = 0.0
  l_screen_head%gil%inc(2) = pix_size(2)
  !
  l_averag_head%char%name = 'Avg Screen'
  l_averag_head%gil%ndim = 3
  l_averag_head%gil%dim(1:2) = l_nxy
  l_averag_head%gil%dim(3) = 3
  l_averag_head%gil%ref(1) = l_nxy(1)/2+1
  l_averag_head%gil%val(1) = 0.0
  l_averag_head%gil%inc(1) = pix_size(1)
  l_averag_head%gil%ref(2) = l_nxy(2)/2+1
  l_averag_head%gil%val(2) = 0.0
  l_averag_head%gil%inc(2) = pix_size(2)
  !
  ! Allocates memory space.
  !
  !   1. Short screen.
  !
  allocate(s_screen_data(s_screen_head%gil%dim(1),s_screen_head%gil%dim(2),&
       s_screen_head%gil%dim(3)), stat = s_screen_head%status)
  if (gildas_error(s_screen_head,rname,error)) call sysexi(fatale)
  s_screen_head%r3d => s_screen_data
  !
  allocate(s_averag_data(s_averag_head%gil%dim(1),s_averag_head%gil%dim(2),&
       s_averag_head%gil%dim(3)), stat = s_averag_head%status)
  if (gildas_error(s_averag_head,rname,error)) call sysexi(fatale)
  s_averag_head%r3d => s_averag_data
  !
  !   2. Long screen.
  !
  allocate(l_screen_data(l_screen_head%gil%dim(1),l_screen_head%gil%dim(2),3),&
       stat = l_screen_head%status)
  if (gildas_error(l_screen_head,rname,error)) call sysexi(fatale)
  l_screen_head%r3d => l_screen_data
  !
  allocate(l_averag_data(l_averag_head%gil%dim(1),l_averag_head%gil%dim(2),3),&
       stat = l_averag_head%status)
  if (gildas_error(l_averag_head,rname,error)) call sysexi(fatale)
  l_averag_head%r3d => l_averag_data
  !
  ! Builds short and long phase screen.
  !
  call phase_screen_building(s_screen_head,l_screen_head, &
     s_averag_head,l_averag_head,sf_value_300m,exponents,&
     ranges,fourier_space,diameter,sf_verification,error)
  if (error) call sysexi(fatale)
  !
  ! Computes gradient of long phase screen.
  !
  call gradient_computation(l_screen_head,error)
  if (error) call sysexi(fatale)
  !
  call gradient_computation(l_averag_head,error)
  if (error) call sysexi(fatale)
  !
  ! Writes screens on disk.
  !
  !   1. Short screen.
  !
  call gdf_write_image(s_screen_head,s_screen_data,error)
  if (gildas_error(s_screen_head,rname,error)) call sysexi(fatale)
  !
  call gdf_write_image(s_averag_head,s_averag_data,error)
  if (gildas_error(s_averag_head,rname,error)) call sysexi(fatale)
  !
  !   2. Long screen.
  !
  call gdf_write_image(l_screen_head,l_screen_data,error)
  if (gildas_error(l_screen_head,rname,error)) call sysexi(fatale)
  !
  call gdf_write_image(l_averag_head,l_averag_data,error)
  if (gildas_error(l_averag_head,rname,error)) call sysexi(fatale)
  !
  ! Frees memory space and returns.
  !
  deallocate(s_screen_data, stat = s_screen_head%status)
  if (gildas_error(s_screen_head,rname,error)) call sysexi(fatale)
  !
  deallocate(s_averag_data, stat = s_averag_head%status)
  if (gildas_error(s_averag_head,rname,error)) call sysexi(fatale)
  !
  deallocate(l_screen_data, stat = l_screen_head%status)
  if (gildas_error(l_screen_head,rname,error)) call sysexi(fatale)
  !
  deallocate(l_averag_data, stat = l_averag_head%status)
  if (gildas_error(l_averag_head,rname,error)) call sysexi(fatale)
  !
  call gagout('I-'//rname//',  Successful completion')
  !
end program phase_screen
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine phase_screen_building(short_screen,long_screen, &
     short_averag,long_averag,sf_value_300m,exponents,ranges,&
     fourier_space,diameter,sf_verification,error)
  use image_def
  use gkernel_interfaces
  use phase_screen_head, only : short_phase_screen_definition, &
                                structure_function_computation, &
                                screen_binning
  type (gildas), intent(inout) :: short_screen, long_screen
  type (gildas), intent(inout) :: short_averag, long_averag
  logical,               intent(out)   :: error
  logical,               intent(in)    :: fourier_space
  logical,               intent(in)    :: sf_verification
  real,                  intent(in)    :: sf_value_300m, diameter
  real,    dimension(3), intent(inout) :: exponents
  real,    dimension(2), intent(inout) :: ranges
  !
  character (len = 32) :: rname = 'PHASE_SCREEN_BUILDING'
  !
  integer :: nxy(2)
  !
  ! Defines the short phase screen.
  ! When asked, also averages phase screens over the antenna dishes.
  !
  nxy = long_screen%gil%dim(1:2)
  call short_phase_screen_definition(short_screen,short_averag, &
       nxy,sf_value_300m,exponents,ranges, &
       fourier_space,diameter,error)
  if (error) return
  !
  ! Computes the structure function for normalization.
  !
  call structure_function_computation(short_screen,sf_value_300m,'short_raw_sf.gdf',.false.,error)
  if (error) return
  !
  call structure_function_computation(short_averag,sf_value_300m,'short_avg_sf.gdf',.false.,error)
  if (error) return
  !
  ! Computes the long screen phase from the short ones.
  !
  nxy = long_screen%gil%dim(1:2)
  call screen_binning(short_screen,long_screen, nxy, error)
  if (error) return
  !
  nxy = long_averag%gil%dim(1:2)
  call screen_binning(short_averag,long_averag, nxy, error)
  if (error) return
  !
  ! Computes the structure function as a test when asked.
  !
  if (sf_verification) then
     !
     call structure_function_computation(long_screen,sf_value_300m,'long_raw_sf.gdf',.true.,error)
     if (error) return
     !
     call structure_function_computation(long_averag,sf_value_300m,'long_avg_sf.gdf',.true.,error)
     if (error) return
     !
  endif
  !
end subroutine phase_screen_building
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine short_phase_screen_definition(screen,averag,nxy, &
     sf_value_300m,exponents,ranges,fourier_space,diameter,error)
  use image_def
  use phys_const
  use gkernel_interfaces, no_interface=>fourt
  type (gildas), intent(inout) :: screen, averag
  logical,               intent(out)   :: error
  logical,               intent(in)    :: fourier_space
  integer, dimension(2), intent(in)    :: nxy
  real,                  intent(in)    :: sf_value_300m, diameter
  real,    dimension(3), intent(inout) :: exponents
  real,    dimension(2), intent(inout) :: ranges
  !
  type (gildas) :: sf_head, sps_head, filter_head
  character (len = 32) :: rname = 'SHORT_PHASE_SCREEN_DEFINITION'
  integer               :: i, j, k, nr
  integer, dimension(2) :: nxy_2
  real                  :: sigma, max_filter
  real                  :: r_pix, kr_pix, kx_pix, ky_pix
  real,    dimension(3) :: amplitudes
  real,    dimension(nxy(1)) :: kx
  real,    dimension(nxy(2)) :: ky
  real,    dimension(nxy(1),nxy(2)) :: kxy, filter1, filter2, argument, beam
  complex, dimension(nxy(1),nxy(2)) :: cmplx_screen, cmplx_averag
  real,    dimension(2*max(nxy(1),nxy(2)),2) :: sf, sqrt_power_spectrum
  real,    dimension(2*max(nxy(1),nxy(2)))   :: kr, sps
  complex, dimension(2*max(nxy(1),nxy(2)))   :: cmplx_sf
  complex, dimension(4*max(nxy(1),nxy(2)))   :: work
  !
  ! Title.
  !
  call gagout('I-'//rname//',  Computation of short phase screens')
  !
  ! 0. Initializes useful variables.
  !
  call gildas_null(sf_head)
  call gildas_null(sps_head)
  call gildas_null(filter_head)
  !
  error = .false.
  !
  nxy_2 = nxy/2
  !
  ! 1. Builds the input 1D structure function.
  !
  ! 1.a Takes care of the fact that the filter dimension can be different.
  !
  if (nxy(1).ge.nxy(2)) then
     r_pix = 0.5*abs(screen%gil%inc(1)) ! Sign problem ?
     nr = 2*nxy(1)
  else
     r_pix = 0.5*abs(screen%gil%inc(2))
     nr = 2*nxy(2)
  endif
  !
  ! 1.b Computes the structure function lags (abscissa of the structure function).
  !
  sf(:,1) = (/ (real(i-1), i = 1, nr) /)
  sf(:,1) = sf(:,1)*r_pix
  !
  ! 1.c Ensures absolute value and continuity of the structure function.
  !
  if (300.0.le.ranges(1)) then
     amplitudes(1) = sf_value_300m/(300.0**exponents(1))
     amplitudes(2) = amplitudes(1)*ranges(1)**(exponents(1)-exponents(2))
     amplitudes(3) = amplitudes(2)*ranges(2)**(exponents(2)-exponents(3))
  elseif (ranges(2).le.300.0) then
     amplitudes(3) = sf_value_300m/(300.0**exponents(3))
     amplitudes(2) = amplitudes(3)*ranges(2)**(exponents(3)-exponents(2))
     amplitudes(1) = amplitudes(2)*ranges(1)**(exponents(2)-exponents(1))
  else
     amplitudes(2) = sf_value_300m/(300.0**exponents(2))
     amplitudes(3) = amplitudes(2)*ranges(2)**(exponents(2)-exponents(3))
     amplitudes(1) = amplitudes(2)*ranges(1)**(exponents(2)-exponents(1))
  endif
  !
  ! 1.d Builds the structure function for the 3 different ranges of lags.
  !
  where( abs(sf(:,1)).le.ranges(1))  sf(:,2) = amplitudes(1)*sf(:,1)**exponents(1)
  where((abs(sf(:,1)).gt.ranges(1)).and.&
        (abs(sf(:,1)).le.ranges(2))) sf(:,2) = amplitudes(2)*sf(:,1)**exponents(2)
  where( abs(sf(:,1)).gt.ranges(2))  sf(:,2) = amplitudes(3)*sf(:,1)**exponents(3)
  !
  ! 1.f Writes the structure function on disk.
  !
  sf_head%file = 'in_sf.gdf'
  sf_head%gil%ndim = 2
  sf_head%gil%dim(1:2) = shape(sf)
  call gdf_write_image(sf_head,sf,error)
  if (gildas_error(sf_head,rname,error)) return
  !
  ! 2. Computes the square root of the power spectrum.
  !
  ! 2.a Computes the spatial frequencies of the power spectrum.
  !
  kr_pix = 1.0/(nr*r_pix)
  kx_pix = 1.0/(nxy(1)*abs(screen%gil%inc(1))) ! Sign problem?
  ky_pix = 1.0/(nxy(2)*abs(screen%gil%inc(2))) ! Sign problem?
  !
  kr = (/ (real(i), i = 1, nr) /)
  kr = kr-(nr/2+1.0)
  kr = kr*kr_pix
  kr = cshift(kr,shift=-(nr/2))
  !
  ! 2.b Computes the sqrt of the power spectrum either directly in the
  !     Fourier space or by FFT of the 2nd order structure function.
  !
  if (fourier_space) then
     !
     ! Computes the exponents, the frequency ranges and the amplitudes
     ! for the direct computation in the Fourier space.
     !
 !! Bugged on gfortran 4.1.0
 !!    exponents((/ 3, 2, 1 /)) = -0.5*(exponents+2.0)
     exponents = (/ exponents(3), exponents(2) , exponents(1) /)
     exponents = -0.5*(exponents+2.0)
     !
 !! Bugged on gfortran 4.1.0
 !!    ranges((/ 2, 1 /)) = 1.0/ranges
     ranges = (/ 1.0/ranges(2), 1.0/ranges(1) /)
     !
     amplitudes(1) = 1.0
     amplitudes(2) = amplitudes(1)*ranges(1)**(exponents(1)-exponents(2))
     amplitudes(3) = amplitudes(2)*ranges(2)**(exponents(2)-exponents(3))
     !
     ! Computes the sqrt of the power spectrum in the 3 different ranges
     ! of spatial frequencies.
     !
     where((abs(kr).le.ranges(1)).and.&
           (abs(kr) /= 0.0))       sps = amplitudes(1)*abs(kr)**exponents(1)
     where((abs(kr).gt.ranges(1)).and.&
           (abs(kr).le.ranges(2))) sps = amplitudes(2)*abs(kr)**exponents(2)
     where((abs(kr).gt.ranges(2))) sps = amplitudes(3)*abs(kr)**exponents(3)
     !
  else
     !
     ! FFTs the 2nd order structure function.
     !
     cmplx_sf = cmplx(sf(:,2),0.0)
     call fourt(cmplx_sf,nr,1,1,0,work)
     !
     ! Changes the power to take into account the transformation
     ! from 1D to 2D.
     !
     where(kr /= 0.0) sps = abs(cmplx_sf)*sqrt(abs(kr(2)/kr))
     !
  endif
  !
  ! 2.c Takes care of the zero spatial frequency.
  !
  sps(1) = sps(2)
  !
  ! 2.d Writes on disk the 1D sqrt of the power spectrum.
  !
  sqrt_power_spectrum(:,1) = kr
  sqrt_power_spectrum(:,2) = sps
  !
  sps_head%file = 'sps.gdf'
  sps_head%gil%ndim = 2
  sps_head%gil%dim(1:2) = shape(sqrt_power_spectrum)
  call gdf_write_image(sps_head,sqrt_power_spectrum,error)
  !
  ! 3. Computes the 2D filter from the 1D sqrt of the power spectrum.
  !
  ! 3.a Computes a matrix of the 2D spatial frequencies.
  !
  kx = (/ (real(i), i = 1, nxy(1)) /)
  ky = (/ (real(i), i = 1, nxy(2)) /)
  !
  kx = kx-(nxy_2(1)+1.0)
  ky = ky-(nxy_2(2)+1.0)
  !
  kx = kx*kx_pix
  ky = ky*ky_pix
  !
  kx = cshift(kx,shift=-nxy_2(1))
  ky = cshift(ky,shift=-nxy_2(2))
  !
  ! 3.b Computes the 2D filter by a rotation of the 1D sqrt of the power
  !     spectrum.
  !
  do j = 1, nxy(2)
     !
! gfortran bug (array feature not implemented...)
!!     kxy(:,j) = sqrt(kx**2+ky(j)**2)
!!     filter1(:,j) = sps((int(kxy(:,j)/kr_pix)+1))
     do i = 1,nxy(1)
       kxy(i,j) = sqrt(kx(i)**2+ky(j)**2)
       filter1(i,j) = sps((int(kxy(i,j)/kr_pix)+1))
     enddo
     !
  end do
  !
  ! 3.c Convolution if the phase screen with the phase antenna beam.
  !
  call gagout('I-'//rname//',  Convolution of the phase screen with the phase antenna beam')
  !
  ! The dish illumination (which is a tension, not a power) must be
  ! a gaussian (G(r) = exp(-(r/sigma)**2)) truncated at 0.5*diameter with
  ! a edge value equal to 11.5dB of peak value.
  !
  sigma = (0.5*diameter)/sqrt(-log(10.0**(-1.15)))
  !
  ! Computes the unidimensional Gaussian where possible.
  !
  argument = (kxy*(pi*sigma))**2
  where (argument <= (0.5*abs(range(argument(1,1)))))
     beam = (pi*sigma**2)*exp(-argument)
  elsewhere
     beam = 0.0
  end where
  !
  ! Corrects the filter to take into account the convolution by the
  ! telescope beam.
  !
  filter2 = filter1*beam
  !
  ! 3.d Normalizes the filters to avoid overflow problems.
  !
  max_filter = maxval(filter1)
  if (max_filter.eq.0.0) then
     call gagout('F-'//rname//',  The filter is zero valued')
     error = .true.
     return
  endif
  filter1 = filter1/max_filter
  !
  max_filter = maxval(filter2)
  filter2 = filter2/max_filter
  !
  ! 3.e Writes the filters on disk
  !
  filter_head%file = 'filter1.gdf'
  filter_head%gil%ndim = 2
  filter_head%gil%dim(1:2) = shape(filter1)
  filter_head%gil%ref(1) = 1
  filter_head%gil%val(1) = 0.0
  filter_head%gil%inc(1) = kx_pix
  filter_head%gil%ref(2) = 1
  filter_head%gil%val(2) = 0.0
  filter_head%gil%inc(2) = ky_pix
  call gdf_write_image(filter_head,filter1,error)
  if (gildas_error(filter_head,rname,error)) return
  !
  filter_head%file = 'filter2.gdf'
  call gdf_write_image(filter_head,filter2,error)
  if (gildas_error(filter_head,rname,error)) return
  !
  ! 4. Computes the phase screen.
  !
  !    Loop over the number of needed screen.
  !
  do k = 1, screen%gil%dim(3)
     !
     ! 4.a Makes a gaussian random sample of the right dimension.
     !
     !     Loop over 2nd screen dimension.
     !
     do j = 1, nxy(2)
        !
        !     Loop over 1st screen dimension.
        !
        do i = 1, nxy(1)
           screen%r3d(i,j,k) = rangau(1.0)
        end do
        !
     end do
     !
     ! 4.b Fills the complex array with input gaussian random sample and
     !     FFTs it.
     !
     cmplx_screen = cmplx(screen%r3d(:,:,k),0.0)
     call fourt(cmplx_screen,nxy,2,1,0,work)
     !
     ! 4.c Filters.
     !
     cmplx_screen = cmplx_screen*filter1/abs(cmplx_screen)
     cmplx_averag = cmplx_screen*filter2/abs(cmplx_screen)
     !
     ! 4.d FFTs back and fills the output real array with the result.
     !
     call fourt(cmplx_screen,nxy,2,-1,0,work)
     screen%r3d(:,:,k) = real(cmplx_screen)
     !
     call fourt(cmplx_averag,nxy,2,-1,0,work)
     averag%r3d(:,:,k) = real(cmplx_averag)
     !
  end do
  !
  ! 4.e Normalizes the screens.
  !
  screen%r3d = screen%r3d/product(nxy)
  averag%r3d = averag%r3d/product(nxy)
  !
end subroutine short_phase_screen_definition
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine screen_binning(short_screen,long_screen,nxy,error)
  use image_def
  use gkernel_interfaces
  type (gildas), intent(in)  :: short_screen
  type (gildas), intent(inout) :: long_screen
  integer, dimension(2), intent(in) :: nxy
  logical, intent(out) :: error
  !
  character(len=16) :: rname = 'SCREEN_BINNING'
  integer :: i, k, l_nx, nx, nx_2, nx_2_1, istart, iend
  real :: factor
  real, dimension((nxy(1)/2),nxy(2)) :: filter1, filter2
  !
  ! Title.
  !
  call gagout('I-'//rname//',  Computation of the long phase screen from short ones')
  !
  ! Initialization of useful variables.
  !
  l_nx = long_screen%gil%dim(1)
  nx     = nxy(1)
  nx_2   = nx/2
  nx_2_1 = nx_2+1
  !
  error = .true.
  !
  ! nxy(1) must be a power of 2.
  !
  if (iand(nxy(1),nxy(1)-1) /= 0) then
     call gagout('F-'//rname//',  1st short screen dimension is not 2^n')
     return
  endif
  !
  ! The long screen size must be coherent with the small screen size.
  !
  if (l_nx.ne.(nx_2*(l_nx/nx_2))) then
     call gagout('F-'//rname//',  1st long screen dimension is not an integer times the 1st small screen dimension')
     return
  endif
  !
  error = .false.
  !
  !
  !
  if (l_nx.eq.nx) then
     long_screen%r3d(:,:,1) = short_screen%r3d(:,1:nxy(2),1)
     return
  endif
  !
  ! Computes the linear filters that will be applied to the short screens.
  !
  factor = 1.0/real(nx_2-1)
!! Bugged on gfortran 4.1.0
!!  filter1 = spread((/ (real(i-1)*factor, i = 1, nx_2) /), 2, nxy(2))
  filter1(:,1) = (/ (real(i-1)*factor, i = 1, nx_2) /)
  do i = 2, nxy(2)
     filter1(:,i) = filter1(:,1)
  enddo
  do i = 1, nx_2
     filter2((nx_2_1-i),:) = filter1(i,:)
  end do
  !
  ! Adds the linearly filtered short screens. This is equivalent to make
  ! a moving weighted average of the short screens. The way it is done,
  ! the long screen will be periodic.
  !
  istart = 1
  iend   = nx_2
  long_screen%r3d(istart:iend,:,1) = &
       short_screen%r3d(nx_2_1:nx,1:nxy(2),1)*filter2 + &
       short_screen%r3d(1:nx_2,1:nxy(2),2)*filter1
  !
  do k = 2, short_screen%gil%dim(3)-1
     !
     istart = iend+1
     iend   = iend+nx_2
     long_screen%r3d(istart:iend,:,1) = &
          short_screen%r3d(nx_2_1:nx,1:nxy(2),k)*filter2 + &
          short_screen%r3d(1:nx_2,1:nxy(2),(k+1))*filter1
     !
  end do
  !
  istart = iend+1
  iend   = iend+nx_2
  long_screen%r3d(istart:iend,:,1) = &
       short_screen%r3d(nx_2_1:nx,1:nxy(2),short_screen%gil%dim(3))*filter2 + &
       short_screen%r3d(1:nx_2,1:nxy(2),1)*filter1
  !
end subroutine screen_binning
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine structure_function_computation(data,sf_value_300m,sf_name, &
     verification,error)
  use image_def
  use gkernel_interfaces
  type (gildas),    intent(inout) :: data
  logical,          intent(out)   :: error
  logical,          intent(in)    :: verification
  character(len=*), intent(in)    :: sf_name
  real,             intent(in)    :: sf_value_300m
  !
  type(gildas) :: sf_head
  character(len=32) :: rname = 'STRUCTURE_FUNCTION_COMPUTATION'
  integer :: i, j, nlags, nscreens, ipos, i300m
  real    :: factor
  integer, dimension(2) :: nxy
  integer, dimension(:),     allocatable, save :: lags
  real,    dimension(:,:),   allocatable, save :: shifted, increments
  real,    dimension(:,:,:), allocatable, save :: sf
  !
  ! Title.
  !
  if (verification) then
     call gagout('I-'//rname//',  Computation of 2nd order structure function as a test')
  else
     call gagout('I-'//rname//',  Computation of 2nd order structure function to normalize the screens')
  endif
  !
  ! Initialization of useful variables.
  !
  call gildas_null(sf_head)
  !
  nxy = data%gil%dim(1:2)
  if (verification) then
     nscreens = 1
  else
     nscreens = data%gil%dim(3)
  endif
  !
  ! The highest lag will be at most half the input image size.
  !
  nlags = int(ceiling(log(real(nxy(1)))/log(2.0)))+1
  !
  ! Allocates memory space.
  !
  allocate(lags(nlags))
  allocate(shifted(nxy(1),nxy(2)))
  allocate(increments(nxy(1),nxy(2)))
  allocate(sf(nscreens,nlags,2))
  !
  ! The used lags will be a sequence of power of 2 + 300/pix_size.
  !
  lags(1:(nlags-1)) = (/ (2**i, i = 0, nlags-2) /)
  !
  i300m = int(300.0/abs(data%gil%inc(2)))
  if (i300m.gt.lags((nlags-1))) then
     ipos = nlags
  else
     ipos = int(ceiling(log(real(i300m))/log(2.0)))+1
     lags((ipos+1):nlags) = lags(ipos:(nlags-1))
  endif
  lags(ipos) = i300m
  !
  print *, "lags (in pixel unit)", lags
  !
  ! Computes the 2nd order structure functions: one for each screen.
  !
  sf = 0.0
  !
  do j = 1, nlags
     !
     do i = 1, nscreens
        !
!        if (verification) then
!           shifted = cshift(data%r2d(:,:),dim=1,shift=lags(j))
!           increments = (shifted-data%r2d(:,:))**2
!        else
           shifted = cshift(data%r3d(:,:,i),dim=1,shift=lags(j))
           increments = (shifted-data%r3d(:,:,i))**2
!        endif
        !
        sf(i,j,2) = sum(sum(increments, dim=1), dim=1)
        sf(i,j,1) = lags(j)*data%gil%inc(1)
        !
     end do
     !
  end do
  !
  sf(:,:,2) = sf(:,:,2)/product(real(nxy))
  !
  ! Verifies the normalization or normalize.
  !
  if (verification) then
     !
     ! Gives user the found normalization.
     !
     print *, "Structure function value at 300m: ", sqrt(sf(1,ipos,2)), " degree"
     !
  else
     !
     ! Normalizes structure functions and data to have the right first value
     ! for the structure function.
     !
     do i = 1, nscreens
        !
        factor = sf_value_300m/sf(i,ipos,2)
        sf(i,:,2) = factor*sf(i,:,2)
        data%r3d(:,:,i) = sqrt(factor)*data%r3d(:,:,i)
        !
     end do
     !
  endif
  !
  data%gil%inc(3) = sqrt(sum(sf(:,ipos,2))/real(nscreens))
  !
  ! Writes structure functions on disk.
  !
  sf_head%file = sf_name
  sf_head%gil%ndim = 3
  sf_head%gil%dim(1:3) = shape(sf)
  call gdf_write_image(sf_head,sf,error)
  if (gildas_error(sf_head,rname,error)) return
  !
  ! Frees memory space.
  !
  deallocate(lags,shifted,increments,sf)
  !
end subroutine structure_function_computation
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine gradient_computation(image,error)
  use image_def
  use gkernel_interfaces
  type (gildas), intent(inout) :: image
  logical,         intent(out) :: error
  !
  character(len=32) :: rname = 'GRADIENT_COMPUTATION'
  real, pointer :: screen(:,:)
  !
  ! Computes image differences.
  !
  error = .false.
  screen => image%r3d(:,:,1)
  image%r3d(:,:,2) = &
       cshift(screen,shift=-1,dim=1) - &
       cshift(screen,shift=1,dim=1)
  !
  image%r3d(:,:,3) = &
       cshift(screen,shift=-1,dim=2) - &
       cshift(screen,shift=1,dim=2)
  !
  ! Normalizes.
  !
  image%r3d(:,:,2) = image%r3d(:,:,2)/(2.0*image%gil%inc(1))
  image%r3d(:,:,3) = image%r3d(:,:,3)/(2.0*image%gil%inc(2))
  !
end subroutine gradient_computation
!
!!!!!!!!!!!!!!!!!! phase_screen.f90 ends here. !!!!!!!!!!!!!!!!!!!!!!!!!!
