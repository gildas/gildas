program uv_fast_model
  use gildas_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! GILDAS
  !	Compute a UV data set from an image model
  !	Input : a UV table, created for example by ASTRO
  !	Input : an image, LMV-like
  !	Output: the UV table, where the visibilities have been computed.
  !
  !     Uses an intermediate FFT with further interpolation for
  !     better speed than UV_MODEL
  !---------------------------------------------------------------------
  character(len=256) :: uvdata,image,uvmodel
  real :: frequency
  logical :: large
  logical :: error
  !
  ! Code:
  call gildas_open
  call gildas_char('UVSAMPLE$',uvdata)
  call gildas_char('IMAGE$',image)
  call gildas_char('UVMODEL$',uvmodel)
  call gildas_real('FREQUENCY$',frequency,1)
  call gildas_logi('LARGE$',large,1)
  call gildas_close
  !
  call uv_auto_model(uvdata,image,uvmodel,frequency,large,error)
  if (error) call sysexi(fatale)
end program uv_fast_model
!<FF>
subroutine uv_auto_model (uvdata,image,uvmodel,frequency,large,error)
  use image_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! GILDAS
  !	Compute a UV data set from an image model
  !	Input : a UV table, created for example by ASTRO
  !	Input : an image, LMV-like
  !	Output: the UV table, where the visibilities have been computed.
  !
  !     Uses an intermediate FFT with further interpolation for
  !     better speed than UV_MODEL
  !
  !	Intermediate FFT is a "large" version, i.e. 4 times the nearest power
  !	of 2 of the initial image, or a maximum of 4096 x 4096.
  !---------------------------------------------------------------------
  !
  character(len=*), intent(inout) :: uvdata        ! UV coverage
  character(len=*), intent(inout) :: image         ! 3-D data cube
  character(len=*), intent(inout) :: uvmodel       ! Result UV table
  real, intent(in) :: frequency                    ! Observing frequency
  logical, intent(in) :: large
  logical, intent(out) :: error
  ! Global
  type (gildas) :: huvd
  type (gildas) :: hima
  type (gildas) :: huvm
  !
  complex, allocatable :: fft(:,:,:)
  real, allocatable :: uvd(:,:)
  real, allocatable :: ima(:,:,:)
  real, allocatable :: uvm(:,:)
  !
  ! Local
  integer :: nf,nx,ny,mx,my,kx,ky, ier
  real :: cpu0, cpu1
  real(8) :: freq, fres, vres
  real :: rx, ry, lambda, factor, pixel_area
  !
  ! Code
  error = .false.
  call gag_cpu(cpu0)
  !
  ! Input file
  call gildas_null(huvd, type = 'UVT')
  call sic_parsef(uvdata,huvd%file,' ','.uvt')
  call gdf_read_header (huvd,error)
  if (error) then
    call gagout ('F-UV_MODEL,  Cannot read input table')
    goto 999
  endif
  if (huvd%char%type(1:9).ne.'GILDAS_UV') then
    call gagout ('W-UV_MODEL,  Input data is not a UV table')
    goto 999
  endif
  !
  allocate (uvd(huvd%gil%dim(1),huvd%gil%dim(2)),stat=ier)
  call gdf_read_data(huvd,uvd,error)
  !
  ! Image
  call gildas_null(hima)
  call sic_parsef(image,hima%file,' ','.gdf')
  call gdf_read_header (hima,error)
  if (error) then
    call gagout ('F-UV_MODEL,  Cannot read input image')
    goto 999
  endif
  if (hima%char%type.ne.'GILDAS_IMAGE') then
    call gagout ('W-UV_MODEL,  Input model is not an image' )
    goto 999
  endif
  !
  mx = hima%gil%dim(1)
  my = hima%gil%dim(2)
  nf = hima%gil%dim(3)
  allocate (ima(mx,my,nf),stat=ier)
  call gdf_read_data(hima,ima,error)
  !
  pixel_area = abs(hima%gil%inc(1)*hima%gil%inc(2))  ! in radian
  ! Define observing frequency
  freq = hima%gil%freq+hima%gil%fres*(nf*0.5-hima%gil%ref(3))
  vres = hima%gil%vres
  !
  ! Define the image size
  if (large) then
    rx = log(float(mx))/log(2.0)
    kx = nint(rx)
    if (kx.lt.rx) kx = kx+1
    nx = 2**kx
    ry = log(float(my))/log(2.0)
    ky = nint(ry)
    if (ky.lt.ry) ky = ky+1
    ny = 2**ky
    call gag_cpu(cpu1)
    print *,'Read data ',cpu1-cpu0
    kx = max(nx,ny)
    kx = min(4*kx,4096)
    nx = kx
    ny = kx
  else
    nx = mx 
    ny = my 
  endif
  !
  ! Get Virtual Memory & compute the FFT
  allocate (fft(nx,ny,nf),stat=ier)
  if (ier.ne.0) then
    call gagout('E-UV_FMODEL, FFT space allocation error')
    goto 999
  endif
  !
  if (large) then
    call plunge_real (ima,mx,my,fft,nx,ny,nf)
  else
    fft = cmplx(ima,0.0)
  endif
  ! Free map
  deallocate (ima)
  !
  ! Compute the FFT
  call do_fft(nx,ny,nf,fft)
  call gag_cpu(cpu1)
  print *,'Done fft ',cpu1-cpu0
  !
  call gildas_null(huvm, type = 'UVT')
  call gdf_copy_header(huvd,huvm, error)
  if (freq.eq.0.0) freq = 1e5   ! 100 GHz
  if (frequency.ne.0) freq = frequency*1e3
  if (fres.eq.0) fres = 1e3     ! 1 GHz
  !
  ! Define scaling factor from Brightness to Flux (2 k B^2 / l^2)
  lambda = 299792.458e3/(freq*1e6) ! in meter
  ! 1.38E3 is the Boltzmann constant times 10^26 to give result in Jy
  factor = 2*1.38e3/lambda**2*pixel_area
  print *,'Factor ',factor,pixel_area
  if (factor.eq.0.0) factor = 1.0
  !
  call sic_parsef(uvmodel,huvm%file,' ','.uvt')
  huvm%gil%dim(1) = 7+3*nf
  huvm%gil%ref(1)  = hima%gil%ref(3)
  huvm%gil%val(1)  = freq
  !
  ! Set the velocity resolution
  huvm%gil%vres = vres
  huvm%gil%fres = -vres*freq/299792.458d0  ! Velocity in km/s
  huvm%gil%inc(1)  = huvm%gil%fres 
  !
  huvm%gil%freq = freq
  huvm%char%line = hima%char%line 
  huvm%gil%voff = hima%gil%voff
  !
  ! Set the number of channels
  huvm%gil%nchan = nf
  !
  allocate (uvm(huvm%gil%dim(1),huvm%gil%dim(2)),stat=ier)
  call copyuv (huvm%gil%dim(1),huvm%gil%dim(2),uvm,  &
       &    huvd%gil%dim(1),uvd)
  deallocate (uvd)
  !
  ! OK, compute the model
  call do_model(uvm, huvm%gil%dim(1),huvm%gil%dim(2),   &
       &    fft,nx,ny,nf,freq,hima%gil%inc(1),hima%gil%inc(2),factor)
  !
  call gdf_write_image(huvm,uvm,error)
  call gagout('S-UV_MODEL,  Successful completion')
  call gag_cpu(cpu1)
  print *,'Done ',cpu1-cpu0
  error = .false.
  return
  !
999 error = .true.
  return
end subroutine uv_auto_model
!<FF>
subroutine do_model (visi,nc,nv,a,nx,ny,nf,   &
     &    freq,xinc,yinc,factor)
  integer, intent(in) :: nc                     ! Visibility size
  integer, intent(in) :: nv                     ! Number of visibilities
  real, intent(inout) :: visi(nc,nv)            ! Visibility array
  integer, intent(in) :: nx                     ! Map size
  integer, intent(in) :: ny                     ! Map size
  integer , intent(in):: nf                     ! Number of channels
  complex, intent(in) :: a(nx,ny,nf)            ! 3-D data cube
  real(kind=8), intent(in) :: freq              ! Observing frequency
  real(kind=8), intent(in) :: xinc              ! Pixel size
  real(kind=8), intent(in) :: yinc              ! Pixel size
  real, intent(in) :: factor                    ! Scale factor
  ! Local
  real(kind=8), parameter :: clight=299792458d0
  real(kind=8) :: kwx,kwy,stepx,stepy,lambda,bfin(2),xr,yr
  complex(kind=8) :: aplus,amoin,azero,afin
  integer :: i,if,ia,ja
  logical :: inside
  equivalence (afin,bfin)
  !
  lambda = clight/(freq*1d6)
  stepx = 1.d0/(nx*xinc)*lambda
  stepy = 1.d0/(ny*yinc)*lambda
  !
  ! Loop on visibility
  do i = 1, nv
    kwx =  visi(1,i) / stepx + dble(nx/2 + 1)
    kwy =  visi(2,i) / stepy + dble(ny/2 + 1)
    ia = int(kwx)
    ja = int(kwy)
    inside = (ia.gt.1 .and. ia.lt.nx) .and.   &
        &      (ja.gt.1 .and. ja.lt.ny)
    if (inside) then
      xr = kwx - ia
      yr = kwy - ja
      do if=1,nf
        !
        ! Interpolate (X or Y first, does not matter in this case)
        aplus = ( (a(ia+1,ja+1,if)+a(ia-1,ja+1,if)   &
            &          - 2.d0*a(ia,ja+1,if) )*xr   &
            &          + a(ia+1,ja+1,if)-a(ia-1,ja+1,if) )*xr*0.5d0   &
            &          + a(ia,ja+1,if)
        azero = ( (a(ia+1,ja,if)+a(ia-1,ja,if)   &
            &          - 2.d0*a(ia,ja,if) )*xr   &
            &          + a(ia+1,ja,if)-a(ia-1,ja,if) )*xr*0.5d0   &
            &          + a(ia,ja,if)
        amoin = ( (a(ia+1,ja-1,if)+a(ia-1,ja-1,if)   &
            &          - 2.d0*a(ia,ja-1,if) )*xr   &
            &          + a(ia+1,ja-1,if)-a(ia-1,ja-1,if) )*xr*0.5d0   &
            &          + a(ia,ja-1,if)
        ! Then Y (or X)
        afin = ( (aplus+amoin-2.d0*azero)   &
            &          *yr + aplus-amoin )*yr*0.5d0 + azero
        !
        visi(5+3*if,i) =  bfin(1)*factor
        ! There was a - sign in the precedent version
        visi(6+3*if,i) =  bfin(2)*factor
      enddo
    else
      print *,'Visi ',i,ia,nx,ja,ny
    endif
  enddo
end subroutine do_model
!<FF>
subroutine do_fft (nx,ny,nf,fft)
  integer, intent(in) :: nx                     !
  integer, intent(in) :: ny                     !
  integer, intent(in) :: nf                     !
  real, intent(inout) :: fft(2,nx,ny,nf)           !
  ! Local
  integer :: if,dim(2),nwork
  integer(kind=4), allocatable :: work(:)
  !
  ! Ndim is 2: work(:) array should be twice the largest dimension
  nwork = 2*max(nx,ny)
  allocate(work(nwork))
  !
  ! Loop on channels
  dim(1) = nx
  dim(2) = ny
  do if = 1, nf
    call fourt(fft(1,1,1,if),dim,2,1,1,work)
    call recent(nx,ny,fft(1,1,1,if))
  enddo
  !
  deallocate(work)
end subroutine do_fft
!<FF>
subroutine copyuv (nco,nv,out,nci,in)
  integer, intent(in) :: nco                    ! Size of output visibility
  integer, intent(in) :: nv                     ! Number of visibilities
  real, intent(out) :: out(nco,nv)              ! Output visibility array
  integer, intent(in) :: nci                    ! Size of input visibility
  real, intent(in) :: in(nci,nv)                ! Input visibility array
  ! Local
  integer :: i,j
  !
  do i=1,nv
    do j=1,7
      out(j,i) = in(j,i)
    enddo
    do j=8,nco,3
      out(j,i) = 0
      out(j+1,i) = 0
      out(j+2,i) = in(10,i)
    enddo
  enddo
end subroutine copyuv
!<FF>
subroutine recent(nx,ny,z)
  !---------------------------------------------------------------------
  ! Recenters the Fourier Transform, for easier display. The present version
  ! will only work for even dimensions.
  !---------------------------------------------------------------------
  integer :: nx                     !
  integer :: ny                     !
  complex :: z(nx,ny)               !
  ! Local
  integer :: i, j
  complex :: tmp
  !
  do j=1,ny/2
    do i=1,nx/2
      tmp = z(i+nx/2,j+ny/2)
      z(i+nx/2,j+ny/2) = z(i,j)
      z(i,j) = tmp
    enddo
  enddo
  !
  do j=1,ny/2
    do i=1,nx/2
      tmp = z(i,j+ny/2)
      z(i,j+ny/2) = z(i+nx/2,j)
      z(i+nx/2,j) = tmp
    enddo
  enddo
  !
  do j=1,ny
    do i=1,nx
      if (mod(i+j,2).ne.0) then
        z(i,j) = -z(i,j)
      endif
    enddo
  enddo
end subroutine recent
!<FF>
subroutine plunge_real (r,nx,ny,c,mx,my,nf)
  !---------------------------------------------------------------------
  !     Plunge a Real array into a Complex array
  !---------------------------------------------------------------------
  integer :: nx                     ! Size of input array
  integer :: ny                     ! Size of input array
  integer :: nf                     ! Number of channels
  real :: r(nx,ny,nf)               ! Input real array
  integer :: mx                     ! Size of output array
  integer :: my                     ! Size of output array
  complex :: c(mx,my,nf)            ! Output complex array
  ! Local
  integer :: kx,ky,lx,ly
  integer :: i,j,k
  !
  kx = nx/2+1
  lx = mx/2+1
  ky = ny/2+1
  ly = my/2+1
  !
  do k=1,nf
    do j=1,my
      do i=1,mx
        c(i,j,k) = 0.0
      enddo
    enddo
    do j=1,ny
      do i=1,nx
        c(i-kx+lx,j-ky+ly,k) = cmplx(r(i,j,k),0.0)
      enddo
    enddo
  enddo
end subroutine plunge_real


