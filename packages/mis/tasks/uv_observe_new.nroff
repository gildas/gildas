.ec _
.ll 76
.ad b
.in 4
.ti -4
.nf
1 UV__OBSERVE__NEW Compute uv coverage + phase/amplitude errors + anomalous refraction
.fi

UV__OBSERVE__NEW simulates the uv coverage of an observation specified
by the source, observatory, and hour angle range. The output is a uv
table that can be used as input for tasks such as UV__FMODEL. Caution:
this uv table is not standard, as it includes two addition columns
(#11 with elevation, #12 with integration time). 

The weight column (#10) is filled with as realistic as possible
estimates of the actual weights (derived from Trec, Tau, elevation).

The visibilities columns (#8 and #9) are filled with a point source
(amplitude = 1, phase = 0). Amplitude calibration errors (offset
+ drifts) can be included. Random phase noise can be added, to
simulate atmospheric phase noise.

If DO__SCREEN = YES, a phase screen is used to simulate phase noise:
this file contains a statistically correct spatial distribution of the
phase perturbation induced by the atmopshere. This screen moves above
the array at the wind velocity. The correct phase value is computed
each antenna at each time dump.

If DO__CALIB = YES, the observations of a calibrator are simulated,
assuming a loop calibrator-source-calibrator-source. The corresponding
uv table is created. 

If DO__RADIOM = YES, a phase correction based on WVR measurements is
simulated.

UV__TRACK family of tasks (in order of increasing complexity):

.ti +4 
- UV__TRACK: uv coverage + random phase noise
.ti +4 
- UV__TRACK__PHASE: phase noise from atmospheric screen + observations 
.ti +6 
of calibrator +  WVR correction 
.ti +4 
- UV__OBSERVE: also includes amplitude noise
.ti +4 
- UV__OBSERVE__NEW: also includes anomalous refraction


.ti -4
2 DECLINATION$ 
.ti +4
TASK\REAL "Source declination" DECLINATION$ 

Declination of the source (degrees). 

.ti -4
2 FREQUENCY$  
.ti +4
TASK\REAL "Line frequency" FREQUENCY$  

Observing frequency (GHz). 

.ti -4
2 NEW$  
.ti +4
TASK\LOGICAL "New or Old table" NEW$ 

Create new uv table or append to existing table.

.ti -4
2 UV__TABLE$ 
.ti +4
TASK\CHARACTER "Table name" UV__TABLE$ 

Output uv table name.

.ti -4
2 STATION__FILE$ 
.ti +4
TASK\CHARACTER "Station file name" STATION__FILE$ 

Input file with antenna position (stations).

.ti -4
2 UV__SPACING$ 
.ti +4
TASK\REAL "UV spacing" UV__SPACING$ 

Sampling in uv plane (should be antenna diameter/2), used to compute
integration time if INTEGRATION.ne.0.

.ti -4
2 DIAMETER$ 
.ti +4
TASK\REAL "Antenna diameter" DIAMETER$ 

Antenna diameter (m). Used to detect shadowing.

.ti -4
2 HORIZON$ 
.ti +4
TASK\REAL "Minimum elevation" HORIZON$ 

Minimum elevation of the source.

.ti -4
2 LATITUDE$ 
.ti +4
TASK\REAL "Observatory latitude" LATITUDE$ 

Latitude of the observatory.

.ti -4
2 HOUR$
.ti +4
TASK\REAL "Hour angle range" HOUR$[2] 

Hour angle range of the observations; only the time range
where the elevation is larger than the specified minimum
elevation will be considered. 

.ti -4
2 INTEGRATION$
.ti +4
TASK\REAL "Integration time" INTEGRATION$

Integration time of each dump (sec). 

.ti -4
2 BANDWIDTH$
.ti +4
TASK\REAL "Bandwidth" BANDWIDTH$

Bandwidth (MHz). Used to compute weigths.

.ti -4
2 TREC$ 
.ti +4
TASK\REAL "Receiver temperature" TREC$ 

Receiver temperature (K). Used to compute Tsys and weights. 

.ti -4
2 TAU$ 
.ti +4
TASK\REAL "Zenith opacity" TAU$ 

Zenith opacity. Used to compute Tsys and weights. 

.ti -4
2 PHASE$ 
.ti +4
TASK\REAL "Phase noise" PHASE$ 

Phase noise rms (degrees). The phase noise will be random with a
gaussian distribution. Used only if DO__SCREEN = NO. 

.ti -4
2 DO__SCREEN$
.ti +4
TASK\LOGICAL "Use phase screen?" DO__SCREEN$

Use or not the phase screen to estimate the atmospheric phase
noise.

.ti -4
2 PHASE__SCREEN$
.ti +4
TASK\CHARACTER "Phase screen file" PHASE__SCREEN$

Input phase screen file. 

.ti -4
2 PHASE__NOISE$ 
.ti +4
TASK\REAL "Phase noise at 300 m" PHASE__NOISE$ 

This is used to scale the phase screen values.

.ti -4
2 WINDANGLE$
.ti +4
TASK\REAL "Wind position angle (deg)" WINDANGLE$

Position angle of the wind direction (deg).

.ti -4
2 WINDVELO$
.ti +4
TASK\REAL "Wind velocity (m/s)" WINDVELO$

Wind velocity (m/s).

.ti -4
2 DO__CALIB$
.ti +4
TASK\LOGICAL "Create calibrator table?" DO__CALIB$

Simulate or not the observation of a phase calibrator.

.ti -4
2 CAL__UV__TABLE$
.ti +4
TASK\CHARACTER "Calibrator uv table" CAL__UV__TABLE$

Output uv table for the calibrator observations.

.ti -4
2 DEADTIME$
.ti +4
TASK\REAL "Dead time (sec)" DEADTIME$

Dead time between source and calibrator observations (sec).

.ti -4
2 CAL__INTEG$
.ti +4
TASK\REAL "Calib. integ. time (dumps)" CAL__INTEG$

Integration time on calibrator (in unit of dump, i.e. 
INTEGRATION).

.ti -4
2 SOURCE__INTEG$
.ti +4
TASK\REAL "Source integ. time (dumps)" SOURCE__INTEG$

Integration time on source (in unit of dump, i.e. 
INTEGRATION).

.ti -4
2 CAL__DIST$
.ti +4
TASK\REAL "Angular distance of calib. (deg)" CAL__DIST$

Angular distance between the source and the calibrator (deg).

.ti -4
2 CAL__ANGLE$
.ti +4
TASK\REAL "Position angle calib. direction" CAL__ANGLE$

Position angle of the source-calibrator direction (deg).

.ti -4
2 ALTITUDE$
.ti +4
TASK\REAL "Phase screen altitude" ALTITUDE$

Phase screen altitude (m). Used to compute the linear distance
between the source and calibrator directions at the phase
screen level.

.ti -4
2 DO__RADIOM$
.ti +4
TASK\LOGICAL "Apply WVR ?" DO__RADIOM$

Apply or not atmospheric phase correction from WVR measurements.

.ti -4
2 WVR__PRECIS$
.ti +4
TASK\REAL "WVR Precision" WVR__PRECIS$

WVR precision. The corrected phase is modelled by:
Pcorr(t) = P(0) + Precision * (P(t)-P(0)) + Noise 

Hence, precision = 0 means a perfect radiometer; precision = 1
means no correction. 

.ti -4
2 WVR__NOISE$ 
.ti +4
TASK\REAL "WVR Noise" WVR__NOISE$ 

WVR noise rms (random, gaussian distribution). 

.ti -4
2 DO__CORRECT$
.ti +4
TASK\LOGICAL "Apply Calibrator Phase " DO__CORRECT$

Phase referencing (or not) to the last measurement on calibrator.
Used only if DO__RADIOM = YES.

.ti -4
2 AMPLITUDE$[2]
.ti +4
TASK\REAL "Amplitude error and drift" AMPLITUDE$[2]

Amplitude errors: offset (in %) and drift (in %/hour). These
are rms, actual values are computed for each antenna.

.ti -4
2 AMP__TIME$
.ti +4
TASK\REAL "Time between amplitude calibration" AMP__TIME$

Time between "calibration amplitude", eg pointing measurements.
The offset of the amplitude error is recomputed.

.ti -4
2 DO__ANREFR$
.ti +4
TASK\LOGICAL "Compute anomalous refraction?" DO_ANREFR$

Compute or not the anomalous refraction (pointing errors). The
RA and DEC terms are written in the column #13 and #14 of the
output uv table. Note: this may introduce some trouble if 
this table is processed as a normal uv table (e.g. using
uv__map). 


.ti -4
1 ENDOFHELP
