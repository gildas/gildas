program sort_cct
  use gkernel_interfaces
!
! Sort a Clean component table
!
  character*256 nami,namo
  logical error
!
  call gildas_open
  call gildas_char('INPUT$',nami)
  call gildas_char('OUTPUT$',namo)
  call gildas_close
  call sub_sort_cct(nami,namo,error)
end program sort_cct
!<ff>
subroutine sub_sort_cct(nami,namo,error)
  use image_def
  use gkernel_interfaces
  character*(*) nami,namo
  logical error
!
  type (gildas) :: hin,hout
  real, allocatable :: din(:,:),dout(:,:)
  integer nc,mc,ic
  integer imax,jmax,ix,jy,ier
  real xmin,ymax,flux
  real, allocatable :: amap(:,:)
  real, allocatable :: wt(:)
  integer, allocatable :: it(:)
  character*60 chain
!
! Read Header
  call sic_parsef (nami,hin%file,' ','.cct')
  call gdf_read_header (hin,error)
  if (error) return
  allocate (din(hin%gil%dim(1),hin%gil%dim(2)),stat=ier)
  call gdf_read_data (hin,din,error)
  if (error) return
!
! Compress the Clean Components
  nc = hin%gil%dim(1)
  xmin = minval(din(:,1))
  ymax = maxval(din(:,2))
  imax = (xmin-hin%gil%val(1)) / hin%gil%inc(1) + hin%gil%ref(1)
  jmax = (ymax-hin%gil%val(2)) / hin%gil%inc(2) + hin%gil%ref(2)
  allocate (amap(imax,jmax),stat=ier)
  amap = 0.0
  do ic=1,nc
     ix = (din(ic,1)-hin%gil%val(1)) / hin%gil%inc(1) + hin%gil%ref(1)
     jy = (din(ic,2)-hin%gil%val(2)) / hin%gil%inc(2) + hin%gil%ref(2)
     amap(ix,jy) = amap(ix,jy) + din(ic,3)
  enddo
!
! Count the effective number
  mc = 0
  do jy=1,jmax
     do ix=1,jmax
        if (amap(ix,jy).ne.0) then
           mc = mc+1
        endif
     enddo
  enddo
  write(chain,'(A,I6,A)') 'I-CCT,  Identified ',mc,' Clean Components'
  call gagout(chain)
!
! Define the image header
  call gdf_copy_header (hin,hout,error)
  call sic_parsef (namo,hout%file,' ','.tcc')
!
  hout%gil%ndim = 2
  hout%gil%dim(1) = mc
  hout%gil%dim(2) = 5
!
! Load the Compressed Component Values
  allocate(dout(mc,5),stat=ier)
  mc = 0
  do jy=1,jmax
     do ix=1,imax
        if (amap(ix,jy).ne.0) then
           mc = mc+1
           dout(mc,1) = (dble(ix)-hout%gil%ref(1))*hout%gil%inc(1) + hout%gil%val(1)
           dout(mc,2) = (dble(jy)-hout%gil%ref(2))*hout%gil%inc(2) + hout%gil%val(2)
           dout(mc,3) = amap(ix,jy)
        endif
     enddo
  enddo
  deallocate (amap)
!
! Sort them by decreasing intensity
  allocate(it(mc),wt(mc),stat=ier)
  do ic=1,mc
     wt(ic) = -abs(dout(ic,3))
     it(ic) = ic
  enddo
! 
! Sort them. Please keep the order of the current sequence
  call gr4_trie(wt,it,mc,error)
  call gr4_sort(dout(:,1),wt,it,mc)
  call gr4_sort(dout(:,2),wt,it,mc)
  call gr4_sort(dout(:,3),wt,it,mc)
  flux = 0.0
  do ic=1,mc
     flux = flux + dout(ic,3)
     dout(ic,4) = ic
     dout(ic,5) = flux
  enddo
  deallocate(it,wt)
!
  call gdf_write_image(hout,dout,error)
  deallocate (dout)
end subroutine sub_sort_cct
