program p_uv_hybrid
  use image_def
  use gkernel_interfaces
  !
  ! Merge by Hybridization in the UV plane two different
  ! images - Keep the short spacings from the ACA one,
  ! and the long spacings from the ALMA one.
  !
  character*256 name1,name2,name3
  real uvradius
  logical error
  !
  call gildas_open
  call gildas_char ('ALMA$',name1)
  call gildas_char ('ACA$',name2)
  call gildas_char ('ALL$',name3)
  call gildas_real ('UVRADIUS$',uvradius,1)
  call gildas_close
  !
  error = .false.
  call s_uv_hybrid (name1,name2,name3,uvradius,error)
  if (error) call sysexi (fatale)
end program p_uv_hybrid
!
subroutine s_uv_hybrid(name1,name2,name3,uvradius,error)
  use image_def
  use gbl_message
  use gkernel_interfaces, no_interface=>fourt
  !---------------------------------------------------------------------
  ! Task UV_HYBRID
  !
  !   Take ALMA map (high resolution map) 
  !   Take ACA  map (map with shortest spacings)
  !
  !   Make oversampled Fourier Transform of boths
  !   Compute the truncation function f(r)
  !   Make the Truncated compact Fourier Transform, f(r) x T(ACA)
  !   Make the complement long baseline Fourier Transform, (1-f(r)) x T(ALMA)
  !   Sum them T(ALL) = f(r) x T(ACA) + (1-f(r)) x T(ALMA)
  !   Make the inverse Fourier Transform
  !   Truncate the resulting image to original size
  !---------------------------------------------------------------------
  character(len=*), intent(inout) :: name1 ! Highres "alma"
  character(len=*), intent(inout) :: name2 ! Lowres  "aca"
  character(len=*), intent(inout) :: name3 ! Combined image
  real, intent(in) ::  uvradius            ! Transition radius
  logical, intent(out) :: error            ! Error flag
  !
  character(len=*), parameter :: rname='UV_HYBRID'
  real :: balma, baca
  type (gildas) :: alma,aca,all
  real, allocatable :: dalma(:,:,:),daca(:,:,:),dout(:,:,:)
  real, allocatable :: f(:,:), x(:), y(:)
  complex, allocatable :: calma(:,:),caca(:,:), wfft(:), cout(:,:), cbig(:,:)
  integer :: nx,ny,nc,ic,nn,dim(2),ier,mx,my,i,j, expand
  real(8) :: lambda
  real :: uinc, vinc, scale
  real :: tole=1e-4
  real :: threshold, expo
  logical :: equal
  !
  error = .false.
  call gildas_null(alma)
  call gildas_null(aca)
  call gildas_null(all)
  !
  ! Read Headers Model & Image 
  call sic_parsef (name1,alma%file,' ','.lmv-clean')
  call gdf_read_header (alma,error)
  if (error) return
  call sic_parsef (name2,aca%file,' ','.lmv-clean')
  call gdf_read_header (aca,error)
  if (error) return
  !
  ! Check spectroscopic consistency
  call spectrum_consistency(rname,alma,aca,tole,error)
  if (error) return
  !
  ! Test that the ALMA image is the largest one and has the smallest beam
  call gdf_compare_shape (alma,aca,equal)
  ier = 0
  if (.not.equal) then
    call gag_message(seve%w,rname,'Images do not match')
    do i=1,2
      balma = abs(alma%gil%inc(i)*alma%gil%dim(i))
      baca  = abs(aca%gil%inc(i) * aca%gil%dim(i))
      if (balma.ne.baca) then
        print *,'ALMA ',balma,alma%gil%inc(i),alma%gil%dim(i)
        print *,' ACA ', baca, aca%gil%inc(i), aca%gil%dim(i)
        ier = 1
      endif
    enddo
  endif
  balma = sqrt(alma%gil%majo*alma%gil%mino)
  baca  = sqrt(aca%gil%majo*aca%gil%mino)
  if (balma.ge.baca) ier = ier+2
  if (ier.ne.0) then
    if (mod(ier,2).ne.0) then
      call gag_message(seve%e,rname,'Field of view of "ALMA" and "ACA" images differ')
      ier = ier/2
    endif
    if (ier.ne.0) then
      call gag_message(seve%e,rname,'Resolution of "ACA" image is better than that of "ALMA" image')
      call gag_message(seve%i,rname,'Consider swapping images !...')
    endif
    error = .true.
    return
  endif
  error = .false.
  !
  nx = alma%gil%dim(1)
  ny = alma%gil%dim(2)
  nc = alma%gil%dim(3)
  !
  ! Read Data Model & Image
  allocate (dalma(nx,ny,nc),stat=ier)
  !!Print *,'ALMA sizes ',nx,ny,nc,ier
  call gdf_read_data (alma,dalma,error)
  if (error) return
  !
  mx = aca%gil%dim(1)
  my = aca%gil%dim(2)
  allocate (daca(mx,my,nc),stat=ier)
  !!Print *,'ACA sizes ',mx,my,nc,ier
  call gdf_read_data (aca,daca,error)
  if (error) return
  !
  ! Skip blanking - Set it to zero, although this  will unavoidably
  ! cause some ringing issues if there is such a blanking.
  where (abs(dalma-alma%gil%bval).le.alma%gil%eval) dalma = 0.0
  where (abs(daca-aca%gil%bval).le.aca%gil%eval) daca = 0.0
  ! 
  ! Allocate output image
  allocate (dout(nx,ny,nc),stat=ier)
  !
  ! Define output image
  ! It must take the header of the high angular resolution image
  ! (since angular resolution has little influence on the inner part
  ! or the UV plane)
  call gdf_copy_header (ALMA,ALL, error)
  all%gil%ndim = 3
  all%gil%dim(1) = nx
  all%gil%dim(2) = ny
  all%gil%dim(3) = nc
  call sic_parsef (name3,all%file,' ','.lmv-clean')
  !
  ! Get the user units in "m"...
  ! Oops, quite a difficult problem, isn't it ?
  lambda = 299792458.d-6/all%gil%freq
  !! Print *,'Lambda ',lambda
  !
  ! Allocate Fourier space
  allocate(calma(nx,ny),stat=ier)
  allocate(caca(mx,my),stat=ier)
  allocate(wfft(max(nx,ny)),stat=ier)
  allocate(cout(nx,ny),f(nx,ny),stat=ier)
  allocate(x(nx),y(ny),stat=ier)
  allocate(cbig(nx,ny),stat=ier)
  nn = 2
  dim = (/nx,ny/)
  !
  ! Compute the combination factor F
  uinc = lambda / (all%gil%inc(1) * nx) 
  vinc = lambda / (all%gil%inc(2) * ny) 
  write(*,'(A,F8.2,F8.2,F8.2)') 'I-'//rname//',  UV Cell size ',uinc,vinc,uvradius
  uinc = uinc / uvradius
  do i=1,nx/2
    x(i) = (i-1)*uinc
  enddo
  do i=nx/2+1,nx
    x(i) = (i-nx-1)*uinc
  enddo
  x = x**2
  !
  vinc = vinc / uvradius
  do i=1,ny/2
    y(i) = (i-1)*vinc
  enddo
  do i=ny/2+1,ny
    y(i) = (i-ny-1)*vinc
  enddo
  y = y**2
  !	
  ! We apply here a sharp truncation, by a exp(-r^(2*expo)) function
  expo = 8.0
  threshold = log(huge(1.0))**(1.0/expo)
  Print *,'Threshold ',threshold,' Expo ',expo
  do j=1,ny
    do i=1,nx
      f(i,j) = x(i) + y(j)            ! This is r^2
      if (f(i,j).lt.threshold) then   ! It falls to Zero at Threshold   
        f(i,j) = exp(-(f(i,j)**expo)) 
      else
        f(i,j) = 0.0
      endif
    enddo
  enddo
  expand = (nx*ny)/(mx*my)
  !
  ! Get the beams and rescale the data accordingly
  !! Print *,'Computed F, Expand ',expand
  !
  scale = (alma%gil%majo*alma%gil%mino)/(aca%gil%majo*aca%gil%mino)
  !
  do ic = 1,nc
    !! Print *,'IC ',ic
    calma = cmplx(dalma(:,:,ic),0.0)
    caca = cmplx(daca(:,:,ic),0.0)
    dim = (/nx,ny/)
    call fourt (calma,dim,2,1,1,wfft)
    !
    ! ACA
    dim = (/mx,my/)
    call fourt (caca,dim,2,1,1,wfft)
    !
    ! Multiply by the other beam
    call mulgau(caca,mx,my, &
    alma%gil%majo,alma%gil%mino,alma%gil%posa, &
    aca%gil%inc(1),aca%gil%inc(2),-1) 
    !
    ! Divide by its own beam
    call mulgau(caca,mx,my, &
    aca%gil%majo,aca%gil%mino,aca%gil%posa, &
    aca%gil%inc(1),aca%gil%inc(2),1) 
    caca = caca * scale 
    !
    ! Expansion part
    if (expand.ne.1) then
      cbig = 0
      ! Load inner quarter
      do j=1,my/2
        cbig(1:mx,j) = caca(1:mx,j)
        cbig(1+nx-mx/2:nx,j) = caca(mx/2+1:mx,j)
      enddo
      do j=my/2+1,my
        cbig(1:mx,j+ny-my) = caca(1:mx,j)
        cbig(1+nx-mx/2:nx,j+ny-my) = caca(mx/2+1:mx,j)
      enddo
      cbig = cbig*expand ! rescale to appropriate units
      cout = f * cbig + (1.0-f) * calma
    else
      cout = f * caca + (1.0-f) * calma
    endif
    !
    ! Back transform
    dim = (/nx,ny/) 
    call fourt (cout,dim,2,-1,1,wfft)
    dout(:,:,ic) = real(cout)
  enddo
  !
  dout = dout / (nx*ny)
  call gdf_write_image(all,dout,error)
  deallocate (calma,caca,cout,wfft)
  deallocate (dalma,daca,dout,f)
end subroutine s_uv_hybrid
!
subroutine plunge_real (r,nx,ny,c,mx,my)
  !-----------------------------------------------------------------
  !     Plunge a Real array into a larger Complex array
  !-----------------------------------------------------------------
  integer nx,ny                      ! size of input array
  real r(nx,ny)                      ! input real array
  integer mx,my                      ! size of output array
  complex c(mx,my)                   ! output complex array
  !
  integer kx,ky,lx,ly
  integer i,j
  !
  kx = nx/2+1
  lx = mx/2+1
  ky = ny/2+1
  ly = my/2+1
  !
  c = 0.0
  do j=1,ny
    do i=1,nx
      c(i-kx+lx,j-ky+ly) = cmplx(r(i,j),0.0)
    enddo
  enddo
end subroutine plunge_real
!
subroutine extract_real (c,mx,my,r,nx,ny)  ! check with cmtore ...
  !-----------------------------------------------------------------
  !     Extract a Real array from a larger Complex array
  !-----------------------------------------------------------------
  integer nx,ny                      ! size of input array
  real r(nx,ny)                      ! input real array
  integer mx,my                      ! size of output array
  complex c(mx,my)                   ! output complex array
  !
  integer kx,ky,lx,ly
  integer i,j
  !
  kx = nx/2+1
  lx = mx/2+1
  ky = ny/2+1
  ly = my/2+1
  !
  do j=1,ny
    do i=1,nx
      r(i,j) = real(c(i-kx+lx,j-ky+ly)) 
    enddo
  enddo
end subroutine extract_real
!
subroutine mulgau(data,nx,ny,bmaj,bmin,pa,x_inc1,x_inc2,isign)
  !----------------------------------------------------------------------
  ! GDF	Multiply the TF of an image by the TF of
  !	a convolving gaussian function. BMAJ and BMIN are the
  !	widths of the original gaussian. PA is the position angle of major
  !	axis (from north towards east)
  !----------------------------------------------------------------------
  integer nx,ny
  real(4) bmaj,bmin,pa
  real(8) x_inc1,x_inc2
  complex data(nx,ny)
  integer i,j,nx1,nx2
  integer isign
  real amaj,amin,fact,cx,cy,sx,sy
  real lmax
  logical norot,rot90
  real(8) rpa
  real(8), parameter :: pi=3.141592653589793d0
  real(4), parameter :: eps=1.e-7
  logical :: debug=.false.
  !
  rpa = pa*180.0/pi ! in degrees
  norot = ( abs(mod(rpa,180.d0)).le.eps)
  rot90 = ( abs(mod(rpa,180.d0)-90.d0).le.eps)
  amaj = bmaj*pi/(2.*sqrt(log(2.)))
  amin = bmin*pi/(2.*sqrt(log(2.)))
  rpa = pa ! in radians here...  pa*pi/180.d0
  !!Print *,'major & minor ',bmaj*3600*180/pi,bmin*3600*180/pi
  !!Print *,'position angle ',pa*180/pi
  !!Print *,'isign ',isign
  !
  if (isign.gt.0) then
    lmax = 10.0
  else
    lmax = 80.0
  endif
  !
  cx = cos(rpa)/nx*amin
  cy = cos(rpa)/ny*amaj
  sx = sin(rpa)/nx*amaj
  sy = sin(rpa)/ny*amin
  !!Print *,'CX CY SX SY user ',cx,cy,sx,sy
  !
  ! convert map units to pixels
  cx = cx / x_inc1
  cy = cy / x_inc2
  sx = sx / x_inc1
  sy = sy / x_inc2
  !!Print *,'CX CY SX SY pixels ',cx,cy,sx,sy
  nx2 = nx/2
  nx1 = nx2+1
  !
  ! optimised code for position angle 0 degrees
  if (norot) then
    do j=1,ny/2
      do i=1,nx2
        fact = (float(j-1)*cy)**2 + (float(i-1)*cx)**2
        if (fact.lt.lmax) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact
        else
          data(i,j) = 0.
        endif
      enddo
      do i=nx1,nx
        fact = (float(j-1)*cy)**2 + (float(i-nx-1)*cx)**2
        if (fact.lt.lmax) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact
        else
          data(i,j) = 0.
        endif
      enddo
    enddo
    do j=ny/2+1,ny
      do i=1,nx2
        fact = (float(j-ny-1)*cy)**2 + (float(i-1)*cx)**2
        if (fact.lt.lmax) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact
        else
          data(i,j) = 0.
        endif
      enddo
      do i=nx1,nx
        fact = (float(j-ny-1)*cy)**2 + (float(i-nx-1)*cx)**2
        if (fact.lt.lmax) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact
        else
          data(i,j) = 0.
        endif
      enddo
    enddo
    !
    ! optimised code for position angle 90 degrees
  elseif (rot90) then
    do j=1,ny/2
      do i=1,nx2
        fact = (float(i-1)*sx)**2 +(float(j-1)*sy)**2
        if (fact.lt.lmax) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact
        else
          data(i,j) = 0.
        endif
      enddo
      do i=nx1,nx
        fact = (float(i-nx-1)*sx)**2 + (float(j-1)*sy)**2
        if (fact.lt.lmax) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact
        else
          data(i,j) = 0.
        endif
      enddo
    enddo
    do j=ny/2+1,ny
      do i=1,nx2
        fact = (float(i-1)*sx)**2 + (float(j-ny-1)*sy)**2
        if (fact.lt.lmax) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact
        else
          data(i,j) = 0.
        endif
      enddo
      do i=nx1,nx
        fact = (float(i-nx-1)*sx)**2 + (float(j-ny-1)*sy)**2
        if (fact.lt.lmax) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact
        else
          data(i,j) = 0.
        endif
      enddo
    enddo
    !
    ! general case of a rotated elliptical gaussian
  else
    do j=1,ny/2
      do i=1,nx2
        fact = (float(i-1)*sx + float(j-1)*cy)**2 + &
            (-float(i-1)*cx + float(j-1)*sy)**2
        if (fact.lt.lmax) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact
        else
          data(i,j) = 0.
        endif
      enddo
      do i=nx1,nx
        fact = (float(i-nx-1)*sx + float(j-1)*cy)**2 + &
            ( -float(i-nx-1)*cx + float(j-1)*sy)**2
        if (fact.lt.lmax) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact
        else
          data(i,j) = 0.
        endif
      enddo
    enddo
    do j=ny/2+1,ny
      do i=1,nx2
        fact = (float(i-1)*sx + float(j-ny-1)*cy)**2 + &
            ( -float(i-1)*cx + float(j-ny-1)*sy)**2
        if (fact.lt.lmax) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact
        else
          data(i,j) = 0.
        endif
      enddo
      do i=nx1,nx
        fact = (float(i-nx-1)*sx & 
          + float(j-ny-1)*cy)**2 + &
          ( -float(i-nx-1)*cx + float(j-ny-1)*sy)**2
        if (fact.lt.lmax) then
          fact = exp (isign*fact)
          data(i,j) = data(i,j)*fact
        else
          data(i,j) = 0.
        endif
      enddo
    enddo
  endif
end subroutine mulgau
!
subroutine spectrum_consistency(rname,ima,imb,tole,error)
  use image_def
  use gbl_message
  !---------------------------------------------------------------------
  ! @ private
  !
  ! IMAGER
  !   Support routine for command UV_SHORT
  !
  !   Verify spectral axis consistency
  !---------------------------------------------------------------------
  !
  character(len=*), intent(in) :: rname
  type(gildas), intent(in) :: ima
  type(gildas), intent(in) :: imb
  real, intent(in) :: tole
  logical, intent(out) :: error
  !
  integer :: na, nb, fa, fb
  real :: va, vb
  character(len=message_length) :: mess
  !
  error = .false.
  fa = ima%gil%faxi
  fb = imb%gil%faxi
  !
  ! Number of channels
  na = ima%gil%dim(fa)
  nb = ima%gil%dim(fb)
  !
  if (na.ne.nb) then
    write(mess,*) 'Mismatch in number of channels ',na,nb
    call gag_message(seve%w,rname,mess)
    error = .true.
  endif
  !
  ! Do not check spectral axis if only 1 channel
  if (na.eq.1 .and. nb.eq.1) return
  !
  ! Check here the spectral axis mismatch
  if (abs(ima%gil%vres-imb%gil%vres).gt.abs(imb%gil%vres*tole)) then
    write(mess,*) 'Mismatch in spectral resolution ',ima%gil%vres,imb%gil%vres
    call gag_message(seve%w,rname,mess)
    error = .true.
  endif
  if (abs(ima%gil%freq-imb%gil%freq).gt.abs(imb%gil%fres*tole)) then
    write(mess,*) 'Mismatch in frequency axis ',ima%gil%freq,imb%gil%freq
    call gag_message(seve%w,rname,mess)
    error = .true.
  endif
  !
  ! Velocity should be checked too
  va = (1.d0-ima%gil%ref(fa))*ima%gil%vres + ima%gil%voff
  vb = (1.d0-imb%gil%ref(fb))*imb%gil%vres + imb%gil%voff
  if (abs(va-vb).gt.abs(imb%gil%vres*tole)) then
    write(mess,*) 'Mismatch in velocity axis ',va, vb 
    call gag_message(seve%w,rname,mess)
    error = .true.
  endif
end subroutine spectrum_consistency
!




