program array_layout
  use gkernel_interfaces
  ! Global
  real :: calcul_rayon_max
  ! Local
  integer :: modtyp
  integer :: nbant
  real :: diam
  !
  integer :: listconfarray, listaleaflag, maxant, k
  integer :: ier,lun,i
  real :: altitude
  !
  parameter (maxant=128)
  real :: radius
  character(len=4) :: liste_noms(maxant)
  character(len=12) :: instrume, config
  character(len=32) :: file
  real :: liste_diam(maxant), liste_x(maxant), liste_y(maxant)
  real :: liste_z(maxant), ro_min, angle, totsurf
  real :: lx(maxant), ly(maxant), lz(maxant)
  real :: latitude, geopos(2), stretch
  real :: pi, cosl, sinl
  !
  call gagout('I-ARRAY_LAYOUT,  Version 1.1 07-Feb-2001')
  pi = acos(-1.0)
  !
  call gildas_open
  call gildas_char('ARRAY$',instrume)
  call gildas_real('LONGITUDE$',geopos,1)
  call gildas_real('LATITUDE$',geopos(2),1)
  latitude = geopos(2)*pi/180.0
  call gildas_char('CONFIGURATION$',config)
  call gildas_inte('NANT$',nbant,1)
  call gildas_real('DIAMETER$',diam,1)
  call gildas_inte('MODEL$',modtyp,1)
  call gildas_real('RADIUS$',radius,1)
  call gildas_real('ANGLE$',angle,1)
  call gildas_real('STRETCH$',stretch,1)
  call gildas_close
  !
  totsurf=nbant*pi*diam**2/4.
  !
  ! initialisations
  listconfarray = 0
  listaleaflag = 0
  !
  ! Normalisation des choix pour les types de reseau
  ! et activation du flag pour eventuellement modifier
  ! "aleatoirement" les reseaux
  !
  if (modtyp.eq.1) then
    listconfarray = 1
  elseif (modtyp.eq.2) then
    listconfarray = 2
  elseif (modtyp.eq.3) then
    listconfarray=3
  elseif (modtyp.eq.4) then
    listaleaflag=1
    listconfarray=3
  elseif (modtyp.eq.5) then
    listconfarray=4
  elseif (modtyp.eq.6) then
    listaleaflag=1
    listconfarray=4
  elseif (modtyp.eq.7) then
    listconfarray = 5
  endif
  if (listconfarray.eq.4) then
    radius = 2.*radius
  endif
  !
  ! En fait, on ne va en faire qu'un
  if (listconfarray.eq.1) then
    call array_model_d( maxant, nbant,   &
     &      diam, radius,   &
     &      liste_noms, liste_diam,   &
     &      liste_x ,liste_y, liste_z)
  elseif  (listconfarray.eq.2) then
    call array_model_a( maxant, nbant,   &
     &      diam, radius,   &
     &      liste_noms, liste_diam,   &
     &      liste_x ,liste_y, liste_z)
  elseif (listconfarray.eq.3) then
    call array_model_c( maxant, nbant,   &
     &      diam, radius, 1,   &
     &      liste_noms, liste_diam,   &
     &      liste_x ,liste_y, liste_z,   &
     &      ro_min, listaleaflag)
  elseif (listconfarray.eq.4) then
    call array_model_r( maxant, nbant,   &
     &      diam, radius, angle, 1,   &
     &      liste_noms, liste_diam,   &
     &      liste_x , liste_y, liste_z,   &
     &      ro_min, listaleaflag)
  elseif (listconfarray.eq.5) then
    call array_model_s( maxant, nbant,   &
     &      diam, radius, angle, 1,   &
     &      liste_noms, liste_diam,   &
     &      liste_x , liste_y, liste_z,   &
     &      ro_min, listaleaflag)
  endif
  !
  ! Initial (x,y,z) (in local plane + altitude)
  !     liste_x > 0 towards North
  !     liste_y > 0 towards East
  !     liste_z == Altitude
  !
  ! Final (X,Y,Z)
  !     LX > 0 outwards Earth Center // Equator
  !     LY > 0 towards East
  !     LZ > 0 towards North // Earth axis
  cosl = cos(latitude)
  sinl = sin(latitude)
  do k=1,nbant
    liste_x(k) = stretch*liste_x(k)    ! Change the N/S value
    lx(k)= - sinl * liste_x(k) + cosl * liste_z(k)
    ly(k)= liste_y(k)
    lz(k)= cosl * liste_x(k) + sinl * liste_z(k)
  enddo
  !
  ! Rayon Min ou Max ?
  ro_min=calcul_rayon_max(nbant, liste_x ,liste_y)
  !
  ier = sic_getlun(lun)
  file = instrume(1:lenc(instrume))//'-'//   &
     &    config(1:lenc(config))//'.cfg'
  call sic_lower(file)
  open (unit=lun,file=file,status='UNKNOWN',recl=128,   &
     &    form='FORMATTED')
  altitude = 4500.
  write (lun,'(A)') '! Commentaires:'
  write (lun,'(A,g14.7)') '! ALTITUDE  = ',altitude
  write (lun,'(A,g14.7)') '! LONGITUDE = ',geopos(1)
  write (lun,'(A,g14.7)') '! LATITUDE  = ',geopos(2)
  write (lun,'(A,I5)')    '! NBANT     = ',nbant
  write (lun,'("!")')
  write (lun,'("! Nom Ants, Lx, Ly, Lz, Diam Ants")')
  write (lun,'(A,F7.1)') '! RADIUS     = ',radius
  write (lun,'(A,F7.1)') '! ANGLE      = ',angle
  do i=1,nbant
    write (lun,*) liste_noms(i), lx(i), ly(i), lz(i),   &
     &      liste_diam(i), liste_y(i), liste_x(i)
    print *,liste_noms(i), lx(i), ly(i), lz(i),   &
     &      liste_diam(i), liste_y(i), liste_x(i)
  enddo
  close(lun)
  !
end program array_layout
!
function calcul_rayon_max(maxant, liste_x ,liste_y)
  real :: calcul_rayon_max          !
  integer :: maxant                 !
  real :: liste_x(maxant)           !
  real :: liste_y(maxant)           !
  ! Local
  integer :: i
  real :: ro_max, ro_cour
  !
  ro_max=0.
  do i=1,maxant
    ro_cour=liste_x(i)**2+liste_y(i)**2
    if (ro_cour.gt.ro_max) ro_max=ro_cour
  enddo
  calcul_rayon_max = sqrt(ro_max)
end
!<FF>
subroutine  array_model_a( maxant, nbant, diam, rayon,   &
     &    liste_noms, liste_diam, liste_x ,liste_y, liste_z)
  integer :: maxant                       ! Maximum number of antennas
  integer :: nbant                        ! Nb of antennas for each diameter
  real :: diam                            ! the antennas diameters
  real :: rayon                           ! OUT
  character(len=*) :: liste_noms(maxant)  !
  real :: liste_diam(maxant)              !
  real :: liste_x(maxant)                 !
  real :: liste_y(maxant)                 !
  real :: liste_z(maxant)                 !
  ! Local
  real :: fact_secur, pas_grille
  real :: rayon_min
  integer :: i
  !
  ! Lecture du rayon du cercle contenant tous les points
  ! pour calculer ce rayon, on suppose le disque circonscrit au carre limite
  !
  fact_secur=1.3                ! Close packing limit
  pas_grille=fact_secur*diam/2. ! Over sample
  !
  rayon_min=sqrt(float(nbant))
  rayon_min=1.+float(int(rayon_min))
  rayon_min=fact_secur*diam*rayon_min / 2.0
  !
  ! DEFINIR Rayon
  if (rayon.lt.rayon_min) then
    write(6,*) 'Minimum radius is ',rayon_min
    stop
  endif
  !
  ! calcul des positions des antennes dans le pseudo-disque interieur
  !
  ! on appelle la procedure suivante pour remplir un pseudo disque aleatoirement
  ! (elle peut servir aussi si un seul type d'antennes ...)
  !
  call fill_array_alea( rayon, nbant, pas_grille,   &
     &    liste_x, liste_y, maxant)
  !
  do i=1,nbant
    liste_z(i)=0.
    liste_diam(i)=diam
    write(liste_noms(i),'(''0'',I2.2)')i
  enddo
end
!<FF>
subroutine  fill_array_alea (rayon, nbpoints, pas_grille,   &
     &    liste_x, liste_y, maxant)
  real :: rayon                     !
  integer :: nbpoints               !
  real :: pas_grille                !
  integer :: maxant                 ! Maximum number of antennas
  real :: liste_x(maxant)           !
  real :: liste_y(maxant)           !
  ! Global
  real*8 :: randev
  ! Local
  logical :: again
  integer :: i,j, indix, indiy, seed
  integer :: posx(maxant), posy(maxant) ! Ca s'appelle du F90
  real :: ro, phi         ! coordonnees circulaires
  real :: pi
  !
  pi = 3.1415927
  !
  ! la methode consiste a tirer au hasard (dans [-1,1])
  ! puis a verifier que le point n'est pas deja occupe
  !
  seed=-1
  do i=1,nbpoints
    again = .true.
    do while (again)
      ro=0.5*(1.0+randev(5,seed))
      ro=ro*rayon/pas_grille
      phi=pi*randev(5,seed)
      indix=int(ro*cos(phi))
      indiy=int(ro*sin(phi))
      again = .false.
      do j=1,i-1
        if (abs(posx(j)-indix).le.1 .and.   &
     &          abs(posy(j)-indiy).le.1) then
          again = .true.
        endif
      enddo
    enddo
    posx(i) = indix
    posy(i) = indiy
    liste_x(i)=indix*pas_grille
    liste_y(i)=indiy*pas_grille
  enddo
end
!<FF>
function randev(icode, iseed)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  !
  !---------------------------------------------------------------------
  real*8 :: randev                  !
  integer :: icode                  !
  integer :: iseed                  !
  !
  if (icode.eq.5) then
    randev = 2.0d0*gag_random()-1.0d0
  else
    print *,'RANDEV not implemented for ',icode
    stop
  endif
end function randev
!<FF>
subroutine  array_model_c( maxant, nbant, diam,   &
     &    rayon, itypant,   &
     &    liste_noms, liste_diam, liste_x ,liste_y, liste_z,   &
     &    ro_min, optalea)
  integer :: maxant                       ! Maximum number of antennas
  integer :: nbant                        ! Nb of antennas for each diameter
  real :: diam                            ! the antennas diameters
  real :: rayon                           ! OUT
  integer :: itypant                      ! indice (1,2 or 3) of the Ant. Type
  character(len=*) :: liste_noms(maxant)  !
  real :: liste_diam(maxant)              !
  real :: liste_x(maxant)                 !
  real :: liste_y(maxant)                 !
  real :: liste_z(maxant)                 !
  real :: ro_min                          ! min. radius for the antenna region
  integer :: optalea                      ! if (optAlea==0), no modif
  ! position des antennes dans l'espace (X,Y,Z)
  !
  ! if (optAlea==1), random modif
  integer :: imin, imax
  real :: rayon_min
  real :: dro_min, dro_max
  real :: angle, delta_angle
  integer :: i, i0
  real :: pi
  real :: dtor           ! conversion Degree to Radian
  !
  pi = 3.141592
  dtor = 0.0174533
  !
  ! debut du programme: on lit le rayon du cercle des antennes
  rayon_min=1.3*diam*nbant/(2.*pi)
  rayon_min=max(rayon_min,ro_min)
  imin = 1
  imax = nbant
  !
  ! calcul des positions des antennes sur le cercle
  angle=(360./nbant)*dtor
  do i=imin,imax
    i0=i-imin+1
    liste_x(i)=rayon*cos(angle*real(i0))
    liste_y(i)=rayon*sin(angle*real(i0))
    liste_z(i)=0.
    liste_diam(i)=diam
    write(liste_noms(i),'(''0'',I2.2)') i0
  enddo
  !
  ! Appel a la procedure de modification "aleatoire" du cercle
  if (optalea.eq.1) then
    dro_min=(-1./10.)*rayon
    dro_max=( 1./10.)*rayon
    delta_angle=angle/4.
    call random_circle(liste_x, liste_y,   &
     &      maxant, imin, imax, diam,   &
     &      ro_min, dro_min, dro_max, delta_angle)
  endif
end
!<FF>
subroutine random_circle(liste_x_ref, liste_y_ref,   &
     &    maxant, indice_min, indice_max, diametre,   &
     &    rayon_min, dro_min, dro_max, delta_angle)
  integer ::      maxant               !
  real ::         liste_x_ref(maxant)  !
  real ::         liste_y_ref(maxant)  !
  integer ::      indice_min           !
  integer ::      indice_max           !
  real ::         diametre             ! against the antenna's superposition
  real ::         rayon_min            ! the min radius must be greater       !
  real ::         dro_min              ! min and max (Deltaro) at each iter
  real ::         dro_max              ! min and max (Deltaro) at each iter
  real ::         delta_angle          ! idem for angles
  ! Global
  real*8 :: randev
  ! Local
  real ::          liste_x(maxant+2)
  real ::          liste_y(maxant+2)
  real ::          liste_ro(maxant+2)
  real ::          liste_phi(maxant+2)
  !
  integer ::       i, seed, j, i_min, i_max, nb_tot_ant
  real ::          dro, dphi
  real ::          rayon_courant, angle
  real ::          eccart1, eccart2, x, y, limite
  real ::          coef1, coef2
  logical ::       modifvaleur
  !
  nb_tot_ant=indice_max-indice_min+1
  !
  ! on decale les indices de 1 en preparation de l'etape suivante
  do i=indice_min,indice_max
    j=i+2-indice_min
    liste_x(j)=liste_x_ref(i)
    liste_y(j)=liste_y_ref(i)
    liste_ro(j)=sqrt(liste_x(j)**2+liste_y(j)**2)
    liste_phi(j)=atan2(liste_y(j),liste_x(j))
  enddo
  !
  ! recopie des bornes "exception"
  liste_x(1)=liste_x(nb_tot_ant+1)
  liste_y(1)=liste_y(nb_tot_ant+1)
  liste_ro(1)=liste_ro(nb_tot_ant+1)
  liste_phi(1)=liste_phi(nb_tot_ant+1)
  !
  liste_x(nb_tot_ant+2)=liste_x(1)
  liste_y(nb_tot_ant+2)=liste_y(1)
  liste_ro(nb_tot_ant+2)=liste_ro(1)
  liste_phi(nb_tot_ant+2)=liste_phi(1)
  !
  ! C'est a partir d'ici qu'on va mofifier la liste des valeurs sur le cercle
  coef1=(dro_max-dro_min)/2.
  coef2=(dro_max+dro_min)/2.
  !
  ! On rappelle que la liste a ete decalee de 1 afin de pouvoir tester
  ! rapidement avec les voisins
  i_min=2
  i_max=nb_tot_ant+1
  limite=diametre*1.3
  seed=-1
  do j=1,3
    do i=i_min,i_max
      dro =coef2+coef1*randev(5,seed)
      dphi=delta_angle*randev(5,seed)
      angle=dphi+liste_phi(i)
      x=(dro+liste_ro(i))*cos(angle)
      y=(dro+liste_ro(i))*sin(angle)
      eccart1=sqrt((liste_x(i-1)-x)**2+(liste_y(i-1)-y)**2)
      eccart2=sqrt((liste_x(i+1)-x)**2+(liste_y(i+1)-y)**2)
      rayon_courant=sqrt(x**2+y**2)
      !
      ! pour garder la valeur modifiee, il ne faut pas que:
      !    - les antennes se touchent
      !    - pour simplifier, on demande qu'elles restent dans le meme ordre
      !    - que le rayon courant soit inferieur au rayon minimal
      !
      modifvaleur=.true.
      if (angle.le.liste_phi(i-1)) then
        modifvaleur=.false.
      elseif  (angle.ge.liste_phi(i+1)) then
        modifvaleur=.false.
      elseif  (eccart1.le.limite) then
        modifvaleur=.false.
      elseif  (eccart2.le.limite) then
        modifvaleur=.false.
      elseif  (rayon_courant.lt.rayon_min) then
        modifvaleur=.false.
      endif
      !
      if (modifvaleur) then
        liste_x(i)=x
        liste_y(i)=y
        liste_ro(i)=dro+liste_ro(i)
        liste_phi(i)=dphi+liste_phi(i)
        if (i.eq.i_min) then
          liste_x(i_max+1)=liste_x(i)
          liste_y(i_max+1)=liste_y(i)
          liste_ro(i_max+1)=liste_ro(i)
          liste_phi(i_max+1)=liste_phi(i)
        elseif  (i.eq.i_max) then
          liste_x(1)=liste_x(i)
          liste_y(1)=liste_y(i)
          liste_ro(1)=liste_ro(i)
          liste_phi(1)=liste_phi(i)
        endif
      endif
    enddo
  enddo
  !
  ! il faut remettre la liste en "phase" avec les bons indices
  do i=indice_min,indice_max
    j=i+2-indice_min
    liste_x_ref(i)=liste_x(j)
    liste_y_ref(i)=liste_y(j)
  enddo
end
!<FF>
subroutine  array_model_d( maxant, nbant, diam, rayon,   &
     &    liste_noms, liste_diam, liste_x ,liste_y, liste_z)
  integer ::      maxant                  ! Maximum number of antennas
  integer ::      nbant                   ! Nb of antennas for each diameter
  real ::         diam                    ! the antennas diameters
  real ::         rayon                   ! OUT
  character(len=*) :: liste_noms(maxant)  !
  real ::          liste_diam(maxant)     !
  real ::          liste_x(maxant)        !
  real ::          liste_y(maxant)        !
  real ::          liste_z(maxant)        !
  ! Local
  real ::          pas_grille, secur
  integer ::       i
  !
  secur=1.3
  pas_grille = secur*diam
  !
  ! calcul des positions des antennes sur la maille hexagonale
  ! NB: les "NbAnt" (et donc le nombre total d'antennes) peuvent
  ! avoir ete modifiees dans FILL_ARRAY_DENSE
  !
  call  fill_array_dense(maxant, diam, nbant, secur,   &
     &    liste_x, liste_y, rayon)
  do i=1,nbant
    liste_z(i)=0.
    liste_diam(i)=diam
    write(liste_noms(i),'(''0'',I2.2)') i
  enddo
end
!<FF>
subroutine  fill_array_dense(maxant, diam, nbant, secur,   &
     &    liste_x, liste_y, rayon_max)
  use gkernel_interfaces
  integer ::      maxant            ! Maximum number of antennas
  real ::         diam              ! Antenna diameter
  integer ::      nbant             ! Number of antennas
  real ::         secur             ! security factors
  real ::         liste_x(maxant)   !
  real ::         liste_y(maxant)   !
  real ::         rayon_max         ! here, "rayon" is an output variable
  ! Local
  integer ::      nbtotant
  real ::         temp_x(maxant)
  real ::         temp_y(maxant)
  integer ::      indices(maxant)
  integer ::      tab_i(maxant), tab_j(maxant)
  real ::         tab_ro(maxant)
  integer ::      j, n, nbmax, i, offset, a
  integer ::      nbc, indice_ro
  real ::         pas_ix, pas_jx, pas_iy, pas_jy
  real ::         ro, phi, angle, pas_grille
  real ::         x, y, phi_zero, homothetie
  integer ::      nbcircles
  real ::         ro_max
  real ::         dtor            ! conversion Degree to Radian
  logical :: error
  !
  dtor = 0.0174533
  !
  ! tout d'abord, on regarde combien il y aura de cercles
  ! en arrondissant au plus proche entier
  !
  nbcircles = anint(float(nbant)/6.0)
  nbtotant = 6*nbcircles
  nbtotant = nbtotant+1
  if (nbtotant.gt.maxant) then
    print *,'GROS Probleme ',nbtotant,nbant,maxant
    stop
  endif
  !
  ! calcul des pas de grille ...
  pas_grille = diam*secur
  !
  ! boucle pour remplir la liste des indices (i,j)
  ! NB: on calcule cette boucle sur trop de points
  !     afin de pouvoir recuperer a coup sur ceux de plus petit diametre.
  !
  nbmax=maxant
  i=0
  j=0
  do n=1,nbmax
    tab_i(n)=i
    tab_j(n)=j
    if (j.eq.0) then
      j=i
      i=1
    else
      j=j-1
      i=i+1
    endif
  enddo
  !
  ! calcul des rayons
  pas_ix=1.0
  pas_iy=0.0
  pas_jx=0.5
  pas_jy=sqrt(3.00)/2.
  !
  do n=1,nbmax
    ro=(real(tab_i(n))*pas_ix+real(tab_j(n))*pas_jx)**2
    ro=ro+(real(tab_j(n))*pas_jy)**2
    tab_ro(n)=sqrt(ro)
    indices(n)=n
  enddo
  !
  ! tri des rayons, la liste "indices" contient les indices associes tries
  call gr4_trie (tab_ro,indices,nbmax,error)
  !
  ! boucle sur les "nb_circles",
  ! chacun constituant un hexagone a rayon constant
  angle=60.*dtor
  temp_x(1)=0.
  temp_y(1)=0.
  offset=2
  !
  indice_ro=1
  do nbc=1,nbcircles
    indice_ro=indice_ro+1
    x=tab_i(indices(indice_ro))*pas_ix
    x=x+tab_j(indices(indice_ro))*pas_jx
    y=tab_j(indices(indice_ro))*pas_jy
    phi_zero=atan(y/x)
    ro=tab_ro(indice_ro)
    homothetie=ro*pas_grille
    ro=homothetie
    ro=tab_ro(indice_ro)*pas_grille
    do a=0,5
      phi=angle*a+phi_zero
      temp_x(offset+a)=ro*cos(phi)
      temp_y(offset+a)=ro*sin(phi)
    enddo
    offset=offset+6
    if (nbc.eq.nbcircles) ro_max = ro
  enddo
  !
  ! on recopie dans les listes les valeurs calculees,
  ! cette "redondance" permet d'eviter d'eventuels pbs
  ! d'ecrasement dans les listes
  !
  do n=1,nbant
    liste_x(n)=temp_x(n)
    liste_y(n)=temp_y(n)
  enddo
  !
  ! calcul du rayon du disque contenant la distribution
  rayon_max=liste_x(nbant)*liste_x(nbant)
  rayon_max=rayon_max+liste_y(nbant)*liste_y(nbant)
  rayon_max=sqrt(rayon_max)
end
!<FF>
subroutine  array_model_r( maxant, nbant, diam,   &
     &    rayon, angler, itypant,   &
     &    liste_noms, liste_diam, liste_x ,liste_y, liste_z,   &
     &    ro_min, optalea)
  integer ::      maxant             ! Maximum number of antennas
  integer ::      nbant              ! Nb of antennas
  real ::         diam               ! antenna diameters
  real ::         rayon              ! OUT
  real ::         angler             ! OUT
  integer ::      itypant            ! indice (1,2 or 3) of the Ant. Type
  character(len=*) :: liste_noms(*)  ! Antenna names
  real ::         liste_diam(*)      ! Their corresponding diameters (m)
  real ::         liste_x(*)         !
  real ::         liste_y(*)         ! Their positions
  real ::         liste_z(*)         !
  real ::         ro_min             ! min. radius for the antenna region
  integer ::      optalea            ! if (optAlea==0), no modif
  ! if (optAlea==1), random modif
  integer ::       imin, imax     ! indices for first and last antennas
  real ::          rayon_min
  real ::          dro_min, dro_max
  real ::          angle, angled, delta_angle, diametre
  real ::          centresx(3), centresy(3)
  real ::          anglerotation, x, y
  integer ::       i, i0, j
  real ::          pi
  real ::          dtor           ! conversion Degree to Radian
  real ::          rtod
  !
  ! initilisation of useful variables
  !
  pi = 3.141592
  dtor = 0.0174533
  rtod = 1.0/dtor
  !
  ! calcul des indices du premier et du dernier point de la liste a creer
  ! (il peut y avoir des points avant, il ne faut pas les ecraser !)
  imin=1
  imax=nbant
  !
  ! debut du programme: on lit le rayon du cercle des antennes
  ! Attention: pour le calcul par rapport a Ro_min,
  ! on a: ro_min=1./(1.-SQRT(3.)/3.) =2.366
  !
  rayon_min=1.3*diam*nbant/pi
  !      if (rayon.gt.rayon_min)
  print *,'Rayon ',rayon,rayon_min
  !      RAYON_MIN=AMAX1(RAYON_MIN,2.37*RO_MIN)
  !
  ! DEFINE RAYON & ANGLE (In call sequence)
  anglerotation = angler
  anglerotation =-anglerotation*dtor
  !
  ! calcul des positions des antennes sur le demi-cercle
  !
  angle=(180./nbant)*dtor
  centresx(1)=-1.0*(sqrt(3.0)/6.0)*rayon
  centresx(2)=(sqrt(3.0)/3.0)*rayon
  centresx(3)=centresx(1)
  centresy(1)=-1.0*rayon/2.0
  centresy(2)=0.0
  centresy(3)=-centresy(1)
  !
  ! position des points sur les arcs de cercle en fonction de l'angle (3 regions)
  do i=imin, imax
    i0 = i-imin+1
    angled=rtod*angle*i0
    if (angled.lt.60.0) then
      j = 1
      angled=(angled+ 30.)*dtor
    elseif  (angled.lt.120.0) then
      j = 2
      angled=(angled+ 90.)*dtor
    else
      j = 3
      angled=(angled+150.)*dtor
    endif
    liste_x(i)=rayon*cos(angled)+centresx(j)
    liste_y(i)=rayon*sin(angled)+centresy(j)
    liste_z(i)=0.
    liste_diam(i)=diam
    ! etablissement du nom des antennes
    write(liste_noms(i),'(''0'',I2.2)') i0
  enddo
  !
  ! Appel a la procedure de modification "aleatoire" du cercle
  if (optalea.eq.1) then
    dro_min=(-1./20.)*rayon
    dro_max=( 1./20.)*rayon
    delta_angle=angle/2.
    diametre=diam
    call random_circle(liste_x, liste_y,   &
     &      maxant, imin, imax, diametre,   &
     &      ro_min, dro_min, dro_max, delta_angle)
  endif
  !
  ! on applique l'eventuelle rotation
  do i=imin, imax
    x=cos(anglerotation)*liste_x(i)
    x=x+sin(anglerotation)*liste_y(i)
    y=-sin(anglerotation)*liste_x(i)
    y=y+cos(anglerotation)*liste_y(i)
    liste_x(i)=x
    liste_y(i)=y
  enddo
end
!<FF>
subroutine  array_model_s( maxant, nbant, diam,   &
     &    rayon, angler, itypant,   &
     &    liste_noms, liste_diam, liste_x ,liste_y, liste_z,   &
     &    ro_min, optalea)
  !---------------------------------------------------------------------
  ! Spiral pattern
  !---------------------------------------------------------------------
  integer ::      maxant            ! Maximum number of antennas
  integer ::      nbant             ! Nb of antennas for each diameter
  real ::         diam              ! the antennas diameters
  real ::         rayon             ! OUT
  real :: angler                    !
  integer ::      itypant           ! indice (1,2 or 3) of the Ant. Type
  real ::         ro_min            ! min. radius for the antenna region
  integer ::      optalea           ! if (optAlea==0), no modif
  ! if (optAlea==1), random modif
  real ::          liste_x(maxant)
  real ::          liste_y(maxant)
  real ::          liste_z(maxant)
  real ::          liste_diam(maxant)
  character(len=*) liste_noms(maxant)
  !
  real*8 :: randev
  integer ::       ntel, i, j, k, seed
  real ::          rayon_min
  real ::          theta, dtheta, r, rrad, ro
  real ::          pi
  !
  pi = 3.141592
  !
  ! debut du programme: on lit le rayon du cercle des antennes
  rayon_min=1.3*diam*nbant/(2.*pi)
  rayon_min=max(rayon_min,ro_min)
  !
  ! Three armed spiral
  ntel = nbant/3
  dtheta = 1.5                 ! Radian
  rrad = 8.0**(1./float(ntel-1))
  r = rayon/rrad/8.0
  !
  i = 1
  do k=1,3
    theta = 2*(k-1)*pi/3.0
    r = rayon/rrad/8.0
    do j=1,ntel
      liste_x(i)=r*cos(theta)
      liste_y(i)=r*sin(theta)
      liste_z(i)=0.
      ro=0.025*(1.0+randev(5,seed))*r
      liste_x(i) = liste_x(i)+ro
      ro=0.025*(1.0+randev(5,seed))*r
      liste_y(i) = liste_y(i)+ro
      liste_diam(i)=diam
      write(liste_noms(i),'(''0'',I2.2)') i
      r = r*rrad
      theta = theta+dtheta
      i = i+1
    enddo
  enddo
  do k=i,nbant
    write(liste_noms(k),'(''0'',I2.2)') k
  enddo
end
