program uv_timeaverage
!
! Smooth a UV table according to time.
!
  use image_def
  use gkernel_interfaces
  character(len=filename_length) nami,namo
  logical error
!
  type (gildas) :: hin,hout
  real, allocatable :: din(:,:),dout(:,:)
  real, allocatable :: tout(:)
  real, allocatable :: decorr(:,:)
  integer nt,nv,mv,ier,lv
  real stime
  character(len=60) chain
  character(len=16) :: rname = 'UV_TIMEAVERAGE'
  logical do_decorr
!
  call gildas_open
  call gildas_char('INPUT$',nami)
  call gildas_char('OUTPUT$',namo)
  call gildas_real('NTIME$',stime,1) ! In seconds
  call gildas_logi('DO_DECORR$',do_decorr,1)
  call gildas_close
!
! Read Header of a sorted, UV table
  call gildas_null(hin, type='UVT')
  call sic_parsef (nami,hin%file,' ','.uvt')
  call gdf_read_header (hin,error)
  if (gildas_error(hin,rname,error)) call sysexi(fatale)
  allocate (din(hin%gil%dim(1),hin%gil%dim(2)),stat=hin%status)
  if (gildas_error(hin,rname,error)) call sysexi(fatale)
  call gdf_read_data (hin,din,error)
  if (gildas_error(hin,rname,error)) call sysexi(fatale)
  nt = hin%gil%dim(1)
  nv = hin%gil%dim(2)
!
! Define the problem size
  call smooth_count (din,nt,nv,mv,stime,error)
  if (error) call sysexi(fatale)
  Print *,'Found ',MV,' output data points '
!
! Define the image header
  call gildas_null(hout, type='UVT')
  call gdf_copy_header (hin,hout,error)
  call sic_parsef (namo,hout%file,' ','.uvt')
  hout%gil%dim(1) = nt
  hout%gil%dim(2) = mv
  hout%gil%inc(2) = stime ! Change integration time
  allocate(dout(nt,mv),stat=hout%status)
  if (gildas_error(hout,rname,error)) call sysexi(fatale)
!
! Working space
  allocate(tout(mv),stat=hout%status)
  if (gildas_error(hout,rname,error)) call sysexi(fatale)
  if (do_decorr) then
     call gagout('I-'//rname//',  Correcting data for decorrelation')
     allocate(decorr(2,mv),stat=hout%status)
     if (gildas_error(hout,rname,error)) call sysexi(fatale)
  endif
!
! Load the time compressed data
  call smooth_uvtable (din,nt,nv,dout,mv,lv,tout,stime,do_decorr,decorr,error)
  Print *,'Done Smooth_uvtable'
  if (error) call sysexi(fatale)
!
! Write the data
  hout%gil%dim(2) = lv
  call gdf_write_image(hout,dout,error)
  if (gildas_error(hout,rname,error)) call sysexi(fatale)
  deallocate (din,dout)
  call gagout('I-'//rname//',  Successful completion')
!
end program uv_timeaverage
!
!<FF>
!
subroutine smooth_uvtable (din,nt,nv,dout,mv,lv,tout,stime,do_decorr, &
     decorr,error)
  integer nv,nt,mv,lv
  real din(nt,nv), dout(nt,mv), tout(mv), decorr(2,mv)
  real stime
  logical do_decorr, error
!
  integer iv,jv,kv, it,ib
  real(8) otime, ltime, ctime 
  real w, amp
!
! Assume things are already baseline-time sorted
! So, just scan and put at the appropriate times
!
  error = .false.
  dout = 0.0 ! Reset all to Zero, specially the weights
  tout = 0.0 !
  if (do_decorr) decorr = 0.0 ! decorrelation factor
  jv = 0
  kv = 1
  lv = 0
  it = 1
  ib = 1
  do iv=1,nv
!
! Increment output visibility (assumes baseline-time ordering)
     jv = jv+1
!
! If no time is set, initialize a new visibility
     ctime =  din(4,iv)*86400.d0+din(5,iv)
     if (lv.eq.0) then
        dout(4,jv) = din(4,iv)                  ! Date
        dout(5,jv) = 0.0                        ! Time
        otime = ctime 
        ltime = ctime+stime
        lv = iv
!
! If time exceeds defined time range, go to next integration
     else if (ctime.gt.ltime) then
        it = it+1
        kv = jv
        if (jv.gt.mv) then
          Print *,'Too many dumps',iv,nv,jv,mv
        endif
        dout(4,jv) = din(4,iv)                  ! Date
        dout(5,jv) = 0.0                        ! Time
        otime = ctime 
        ltime = ctime + stime
        ib = 1
        lv = iv
!
! Check time is increasing
     else if (ctime.lt.otime) then
        !!Print *,'IV ',iv,ctime,' otime ',otime,lv
        call gagout('E-SMOOTH,  Data not sorted')
        error = .true.
        return
!
! If time increments, restart from first baseline
     else if (ctime.gt.otime) THEN
        jv = kv
        ib = 1
        otime = ctime 
     endif
!
! Add to output UV table
     w = din(10,iv)
     if (w.gt.0) then
        dout(1,jv) = dout(1,jv) + din(1,iv)*w   ! u
        dout(2,jv) = dout(2,jv) + din(2,iv)*w   ! v
        dout(3,jv) = dout(3,jv) + din(3,iv)*w   ! w
        dout(4,jv) = din(4,iv)                  ! date
        dout(5,jv) = dout(5,jv) + din(5,iv)     ! time
        tout(jv) = tout(jv)+1
        dout(6,jv) = din(6,iv)                  ! start antenna
        dout(7,jv) = din(7,iv)                  ! start antenna
        dout(8,jv) = dout(8,jv) + din(8,iv)*w   ! real
        dout(9,jv) = dout(9,jv) + din(9,iv)*w   ! imag
        dout(10,jv) = dout(10,jv) + w           ! weight
!
! Average additional information, if present
        if (nt.eq.16) then
           dout(11,jv) = dout(11,jv) + din(11,iv)*w   ! elevation
           dout(12,jv) = dout(12,jv) + din(12,iv)     ! integ. time
           dout(13,jv) = dout(13,jv) + din(13,iv)*w   ! Ra error, ante 1
           dout(14,jv) = dout(14,jv) + din(14,iv)*w   ! Dec error, ante 1
           dout(15,jv) = dout(15,jv) + din(15,iv)*w   ! Ra error, ante 2
           dout(16,jv) = dout(16,jv) + din(16,iv)*w   ! Dec error, ante 2
        endif
!
! Also compute the average of normalized gains
        if (do_decorr) then
           amp = sqrt(din(8,iv)**2+din(9,iv))
           if (amp.ne.0.) then
              decorr(1,jv) = decorr(1,jv) + din(8,iv)/amp*w
              decorr(2,jv) = decorr(2,jv) + din(9,iv)/amp*w
           endif
        endif
!
     endif
  enddo
!
! Output counter
  lv = jv
!
! Normalize
  do jv=1,lv
     w = dout(10,jv)
     if (w.gt.0) then
        dout(1,jv) = dout(1,jv) / w
        dout(2,jv) = dout(2,jv) / w
        dout(3,jv) = dout(3,jv) / w
        dout(5,jv) = dout(5,jv) / tout(jv)
        dout(8,jv) = dout(8,jv) / w
        dout(9,jv) = dout(9,jv) / w
!
        if (nt.eq.16) then
           dout(11,jv) = dout(11,jv) / w
           dout(13,jv) = dout(13,jv) / w
           dout(14,jv) = dout(14,jv) / w
           dout(15,jv) = dout(15,jv) / w
           dout(16,jv) = dout(16,jv) / w
        endif
!
        if (do_decorr) then
           amp = sqrt(decorr(1,jv)**2+decorr(2,jv)**2)/w
           if (amp.ne.0.) then
              dout(8,jv) = dout(8,jv)/amp
              dout(9,jv) = dout(9,jv)/amp
           endif
        endif
!
     endif
  enddo
!
  if (lv.ne.mv) then
     call gagout('W-SMOOTH,  Inconsistent number of output data')
     Print *,lv,mv,mv-lv
     error = .true.
     return
  endif
end subroutine smooth_uvtable
!
!<FF>
!
subroutine smooth_count (din,nt,nv,mv,stime,error)
  integer nv,nt,mv
  real din(nt,nv)
  real stime
  logical error
!
  integer iv,jv,kv,lv
  real(8) otime, ltime, ctime
!
! Assume things are already baseline-time sorted
! So, just scan and count new output visibilities
!
  jv = 0
  kv = 1
  lv = 0
  do iv=1,nv
!
! Increment output visibility (assumes baseline-time ordering)
     jv = jv+1
!
! If no time is set, initialize a new visibility
     ctime =  din(4,iv)*86400.d0+din(5,iv)
     if (lv.eq.0) then
        ltime = ctime+stime                  ! time
        otime = ctime 
        lv = iv
!
! If time exceeds defined time range, go to next integration
     else if (ctime.gt.ltime) then
        kv = jv
        ltime = ctime+stime
        otime = ctime
        lv = iv
!
! Check time is increasing
     else if (ctime.lt.otime) then
        Print *,'IV ',iv,ctime,' otime ',otime,lv
        call gagout('E-SMOOTH,  Data not sorted')
        error = .true.
        return
!
! If time increments, restart from first baseline
     else if (ctime.gt.otime) then
        jv = kv
        otime = ctime
     endif
  enddo
!
! Define number of output visibilities
  mv = jv
  !!Print *,jv,kv,iv,nv
end subroutine smooth_count
