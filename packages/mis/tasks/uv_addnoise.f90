program uv_addnoise
  use gildas_def
  use gkernel_interfaces
  use image_def
  !---------------------------------------------------------------------
  ! TASK   Add a Gaussian noise to the Real and Imaginary
  !        parts of visibilities of a UV table
  !
  ! Input :
  !	      UV_TABLE$	The UV table to be modified
  !       WCOL$     The weight channel
  !       FACTOR$   Scale factor for noise
  !---------------------------------------------------------------------
  ! Local
  type(gildas) :: x
  character(len=filename_length) :: uv_table
  !
  integer :: sblock=1000
  integer :: i,nc,nv,n,j,k,l, wcol, ier
  real :: factor, noise, weight
  logical :: error
  !
  call gildas_open
  call gildas_char('UV_TABLE$',uv_table)
  call gildas_inte('WCOL$',wcol,1)
  call gildas_real('FACTOR$',factor,1)
  call gildas_close
  if (factor.le.0) then
    call gagout('W-UV_ADDNOISE,   Factor < 0, no noise added')
    call sysexi(1)
  endif
  !
  ! Open line table
  n = lenc(uv_table)
  if (n.le.0) goto 999
  call gildas_null (x, type = 'UVT')
  call gdf_read_gildas (x, uv_table, '.uvt', error)
  if (error) then
    call gagout('F-UV_ADDNOISE,  Cannot read input/output UV table')
    goto 999
  endif
  !
  nc = x%gil%nchan 
  if (wcol.le.0 .or. wcol.gt.nc) wcol = (nc+2)/3   ! Default channel
  wcol = x%gil%fcol-1+3*wcol
  !
  allocate (x%r2d(x%gil%dim(1), sblock), stat=ier)
  !
  ! Loop over line table
  do i=1,x%gil%dim(2),sblock
    x%blc(2) = i
    x%trc(2) = min(x%gil%dim(2),i-1+sblock)
    call gdf_read_data(x,x%r2d,error)
    !
    ! Add noise to this visbility range
    nv = x%trc(2)-x%blc(2)+1
    do j=1,nv
      weight = x%r2d(wcol,j)/factor**2
      if (weight.gt.0) then
        noise = 1e-3/sqrt(weight) 
        do k=1,nc
          l = x%gil%fcol+3*(k-1)
          x%r2d(l,j) = x%r2d(l,j) + rangau(noise)
          l = l+1
          x%r2d(l,j) = x%r2d(l,j) + rangau(noise)
          l = l+1
          x%r2d(l,j) = weight 
        enddo
      endif
    enddo  
    call gdf_write_data (x, x%r2d, error)
  enddo
  ! end loop
  call gdf_close_image (x, error)
  call gagout('S-UV_ADDNOISE,  Successful completion')
  call sysexi(1)
  999   call sysexi(fatale)
end program uv_addnoise
!
