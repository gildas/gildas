program uv_model
  use gildas_def
  use image_def
  use gbl_message
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! GILDAS
  !   Compute a UV data set from an image model
  !    Input : a UV table, created for example by ASTRO
  !    Input : an image.
  !    Output: the UV table, where the visibilities have been computed.
  !---------------------------------------------------------------------
  ! Local
  character(len=*), parameter :: rname='UV_MODEL'
  type(gildas) :: img,cover,uvt
  integer(kind=index_length) :: nf,nx,ny
  integer(kind=4) :: n,  ier
  character(len=filename_length) :: uvdata,image,name,uvmodel
  real(kind=8), allocatable :: mapx(:), mapy(:)
  real(kind=8) freq
  logical :: error
  !
  !
  ! Code:
  call gildas_open
  call gildas_char('UVSAMPLE$',uvdata)
  call gildas_char('IMAGE$',image)
  call gildas_char('UVMODEL$',uvmodel)
  call gildas_close
  !
  ! Input file
  n = lenc(uvdata)
  if (n.le.0) goto 999
  call gildas_null (cover, type = 'UVT')
  name = uvdata(1:n)
  call gdf_read_gildas (cover, name, '.uvt', error)
  if (error) then
    call gag_message(seve%f,rname,'Cannot read input table '//trim(cover%file))
    goto 999
  endif
  !
  ! Image
  n = lenc(image)
  if (n.le.0) goto 999
  call gildas_null (img, type = 'IMAGE')
  name = image(1:n)
  call gdf_read_gildas (img, name, '.vlm', error, rank=3)
  if (error) then
    call gag_message(seve%f,rname,'Cannot read input image '//trim(img%file))
    goto 999
  endif
  call sic_upper(img%char%code(1))
  if (img%char%code(1).eq.'FREQUENCY' .or.  &
      img%char%code(1).eq.'VELOCITY'  .or.  &
      img%char%code(1).eq.'UNKNOWN') then
    nf = img%gil%dim(1)
  else
    call gag_message(seve%e,rname,'First axis unit not FREQUENCY')
    goto 999
  endif
  nx = img%gil%dim(2)
  ny = img%gil%dim(3)
  call gag_message(seve%i,rname,trim(img%file)//' succesfully opened')
  !
  ! Get Virtual Memory to compute the positions
  allocate (mapx(nx), mapy(ny), stat=ier)
  if (ier.ne.0) goto 999
  call docoor (nx,img%gil%inc(2),mapx)
  call docoor (ny,img%gil%inc(3),mapy)
  !
  call gildas_null (uvt, type = 'UVT')
  call gdf_copy_header (cover,uvt,error)            ! uvt%file is defined LATER
  n = lenc(uvmodel)
  if (n.le.0) then
    call gag_message(seve%f,rname,'Output file name is empty.')
    goto 999
  endif
  name = uvmodel(1:n)
  call sic_parsef(name,uvt%file,' ','.uvt')
  uvt%gil%dim(1) = 7+3*nf
  uvt%gil%dim(2) = cover%gil%dim(2)
  !
  ! Conv part
  uvt%gil%ref(1)  = img%gil%ref(1)
  uvt%gil%val(1)  = img%gil%freq
  uvt%gil%inc(1)  = img%gil%fres
  !
  ! Rest of the spectroscopic info
  uvt%gil%freq    = img%gil%freq
  uvt%gil%voff    = img%gil%voff
  uvt%gil%fres    = img%gil%fres
  uvt%gil%vres    = img%gil%vres
  uvt%char%line   = img%char%line
  uvt%gil%nchan = nf
  !
  freq = img%gil%freq+img%gil%fres*(nf*0.5-img%gil%ref(1))
  if (freq.lt.0.1) then        ! 100 kHz threshold for warning
     call gag_message(seve%w,rname,'VERY low frequency. Please check!')
     write(6,*) 'Value is ',freq,' MHz'
     goto 999
  endif
  !
  call gdf_create_image (uvt, error)
  if (error) then
    call gag_message(seve%f,rname,'Cannot Cannot create output UV model '//trim(uvt%file))
    goto 999
  endif
  call gdf_allocate (uvt,error)
  if (error) then
    call gag_message(seve%f,rname,'Cannot obtain memory slot for UV model')
    goto 999
  endif
  !
  call gag_message(seve%i,rname,'Output table'//trim(uvt%file)//' succesfully opened')
  !
  call copyuv (uvt%gil%dim(1),uvt%gil%dim(2),uvt%r2d,cover%gil%dim(1),cover%r2d)
  !
  ! OK, compute the model
  call do_model(uvt%r2d,uvt%gil%dim(1),uvt%gil%dim(2),img%r3d,nf,nx,ny,mapx,mapy,freq)
  call gdf_write_data (uvt, uvt%r2d, error)
  call gagout('S-'//rname//',  Successful completion')
  call sysexi(1)
  !
999  call sysexi(fatale)
!
contains
!
subroutine do_model (visi,nc,nv,map,nf,nx,ny,mapx,mapy,freq)
  use gildas_def
  !---------------------------------------------------------------------
  !
  !---------------------------------------------------------------------
  integer(kind=index_length), intent(in)    :: nc             ! Size of visibility
  integer(kind=index_length), intent(in)    :: nv             ! Number of visibilities
  real(kind=4),               intent(inout) :: visi(nc,nv)    ! Visibilities
  integer(kind=index_length), intent(in)    :: nf             ! Number of channels
  integer(kind=index_length), intent(in)    :: nx             ! X size of map
  integer(kind=index_length), intent(in)    :: ny             ! Y size of map
  real(kind=4),               intent(in)    :: map(nf,nx,ny)  ! Map
  real(kind=8),               intent(in)    :: mapx(nx)       ! X coordinates
  real(kind=8),               intent(in)    :: mapy(ny)       ! Y coordinates
  real(kind=8),               intent(in)    :: freq           ! Frequency (MHz)
  ! Local
  real(kind=8), parameter :: pi=3.14159265358979323846d0
  real(kind=8), parameter :: f_to_k = 2.d0*pi/299792458.d-6  ! (2*pi/clight)*1d6
  real(kind=8) :: kwx,kwy,y,z
  real(kind=4) :: cosi,sinu
  integer(kind=index_length) :: i,ix,iy,if
  !
  do i = 1,nv                 ! loop on visibility points
    kwx = freq * f_to_k * visi(1,i)  ! Prepare exponential coefficients
    kwy = freq * f_to_k * visi(2,i)
    do iy=1, ny                ! Loop on pixels
      y = kwy*mapy(iy)
      do ix=1, nx
        z = kwx*mapx(ix) + y
        cosi = cos(z)
        sinu = sin(z)
        !               write(6,*) cosi,sinu
        do if = 1,nf
          visi(5+3*if,i) = visi(5+3*if,i)+map(if,ix,iy)*cosi
          visi(6+3*if,i) = visi(6+3*if,i)+map(if,ix,iy)*sinu  
        enddo
      enddo
    enddo
  enddo
end subroutine do_model
!
subroutine docoor (n,xinc,x)
  use gildas_def
  !---------------------------------------------------------------------
  ! GILDAS UVMAP
  !  Compute FFT grid coordinates in U or V
  !---------------------------------------------------------------------
  integer(kind=index_length), intent(in)  :: n     ! Size of array
  real(kind=8),               intent(in)  :: xinc  ! Increment
  real(kind=8),               intent(out) :: x(n)  ! Array
  ! Local
  integer(kind=index_length) :: i
  do i=1,n
    x(i) = dble(i-n/2-1)*xinc
  enddo
end subroutine docoor
!
subroutine copyuv (nco,nv,out,nci,in)
  use gildas_def
  !---------------------------------------------------------------------
  !
  !---------------------------------------------------------------------
  integer(kind=index_length), intent(in)  :: nco          ! Size of output visibility 
  integer(kind=index_length), intent(in)  :: nv           ! Number of visibilities
  real(kind=4),               intent(out) :: out(nco,nv)  ! Output visibilities
  integer(kind=index_length), intent(in)  :: nci          ! Size of input visibility
  real(kind=4),               intent(in)  :: in(nci,nv)   ! Input visibilities
  ! Local
  integer(kind=index_length) :: i,j
  do i=1,nv
    do j=1,7
      out(j,i) = in(j,i)
    enddo
    do j=8,nco,3
      out(j,i) = 0.
      out(j+1,i) = 0.
      out(j+2,i) = in(10,i)
    enddo
  enddo
end subroutine copyuv
!
end program uv_model
