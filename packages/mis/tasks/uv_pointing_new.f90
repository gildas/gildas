program uv_pointing
  !-------------------------------------------------------------------------
  ! Pointing Error Simulation
  !
  !     Fast pointing error simulation based on  gridded interpolation
  !     from the Fourier Transform of an image
  !
  !-------------------------------------------------------------------------
  !
  ! Expression of A
  !     A(x,y) = exp[-2ln(2)/Theta(i)^2 ((x-Dx(i))^2+(y-Dy(i))^2)]
  !            * exp[-2ln(2)/Theta(j)^2 ((x-Dx(j))^2+(y-Dy(j))^2)]
  !
  ! Expression of ThetB
  !     1/ThetB^2 = 1/2 (1/Theta(i)^2+1/Theta(j)^2)
  !
  ! Expression of FT(A)
  !
  !     FT(u,v) = pi ThetB^2 /(4 ln(2)) exp {-pi^2 ThetB^2 / (4 ln(2)) (u^2+v^2) }
  !       * exp{ -2 ln(2) [(Dx1^2_Dy1^2)/Theta1^2 + (Dx2^2_Dy2^2)/Theta2^2] }
  !       * exp{ ThetB^2 ln(2) [(Dx1/Theta1^2 + Dx2/Theta2^2)^2
  !                           + (Dy1/Theta1^2 + Dy2/Theta2^2)^2 ] }
  !       * exp{ -i pi ThetB^2 [(u Dx1 + v Dy1)/Theta1^2 + (u Dx2 + v Dy2)/Theta2^2 ] }
  !
  !
  ! Simplification in the case of identical antennas
  !
  !     FT(u,v) = pi Thet^2 /(4 ln(2)) exp {-pi^2 Thet^2 / (4 ln(2)) (u^2+v^2) }
  !       * exp{ -ln(2) / Thet^2 [(Dx1-Dx2)^2^ + (Dy1-Dy2)^2] }
  !       * exp{ -i pi [(u Dx1 + v Dy1) + (u Dx2 + v Dy2)] }
  !
  !
  ! Evaluation of disturbed visibility
  !      V(u_0,v_0) = FT(u-u_0,v-v_0,Dx(Iant),Dy(Iant) ** FTSKY(u,v)
  !
  ! Replaced by a simple sum of the nearest neighbours
  !     V(u_0,v_0) = SiSj
  !                FT(ui-u_0,vj-v_0,Dx(Iant),Dy(Iant) * FTSKY(ui,vj)
  !                Weight(ui-u_0,vj-v_0) * Du Dv
  !
  ! Note that this CONVOLUTION in Fourier Plane by a Weight(u,v) function
  ! is equivalent to a MULTIPLICATION in Image Plane by its Fourier Transform.
  !
  ! One could (in theory) correct for this gridding effect later.
  !
  use gildas_def
  use image_def
  use gkernel_interfaces
  use phys_const
  !
  real su,sv,suv,uvr,eps,frequency
  real(8) freq
  integer nx,ny,mx,my,nw,nv,ntt,na
  integer i,n,nn(2),ndim
  integer ier
  character(len=256) name, image, uvdata, uvmodel, pfile
  logical error, mosaic, add_ar
  real sigma(2)
  real theta, pixel_area, lambda, factor, diameter
  integer icode, mtt, ma
  !
  ! gildas tables
  type (gildas)     :: image_h                  ! header of input image 
  real, allocatable :: image_d(:,:)             ! data
  type (gildas)     :: inuv_h     ,outuv_h      ! header of input and output uv tables
  real, allocatable :: inuv_d(:,:),outuv_d(:,:) ! data  
  type (gildas)     :: ptab_h                   ! header external pointing (errors) table
  real, allocatable :: ptab_d(:,:,:)            ! data  
  !
  ! Other arrays
  complex, allocatable :: ft_image(:,:)  ! complex FFT of image 
  real, allocatable :: perr(:,:,:)       ! Pointing errors
  integer, allocatable :: itimes(:)      ! Storage for the time stamps
  real(kind=8), allocatable :: rtimes(:) ! Storage for time values
  real(kind=4), allocatable :: rtmp(:)   ! Work area 
  integer, allocatable :: iorder(:)      ! Ordering of times
  !
  !
  ! Code:
  call gagout('I-UV_POINTING,  Version 1.5 17-Mar-2012')
  call gildas_open
  call gildas_char('UVSAMPLE$',uvdata)
  call gildas_char('IMAGE$',image)
  call gildas_char('UVMODEL$',uvmodel)
  call gildas_real('FREQUENCY$',frequency,1)
  call gildas_logi('MOSAIC$',mosaic,1)
  call gildas_inte('CODE$',icode,1)
  call gildas_char('POINTING$',pfile)
  call gildas_real('ERRORS$',sigma,2)
  call gildas_real('PRIMARY$',theta,1)
  call gildas_logi('ADD_AR$',add_ar,1) ! add anomalous refraction?
  call gildas_real('DIAMETER$',diameter,1)
  call gildas_close
  !
  ! Get the model image
  n = lenc(image)
  if (n.le.0) goto 999
  call gildas_null(image_h)
  name = image(1:n)
  call sic_parsef(name,image_h%file,' ','.gdf')
  call gdf_read_header(image_h, error)
  if (error) then
    call gagout  ('E-UV_POINTING,  Cannot read input image') 
    goto 999
  endif
  if (image_h%char%type.ne.'GILDAS_IMAGE') then
    call gagout ('W-UV_POINTING,  Input image is not an image')
    goto 999
  endif
  allocate (image_d(image_h%gil%dim(1),image_h%gil%dim(2)), STAT=ier)
  if (ier .ne. 0) then
     call gagout ('E-UV_POINTING,  Error allocating IMAGE_D memory')
     goto 999
  endif
  call gdf_read_data (image_h,image_d,error)
  if (error) then
    call gagout ('E-UV_POINTING,  Cannot read input image')
    goto 999
  endif

  pixel_area = abs(image_h%gil%inc(1)*image_h%gil%inc(2))
  !
  ! Need to oversample in the UV plane, i.e. extend initial image,
  ! to avoid large gridding errors
  !
  nx = image_h%gil%dim(1)
  ny = image_h%gil%dim(2)
  mx = 8*nx
  my = 8*ny
  if (mx.gt.5000.or.my.gt.5000) then
    mx = 4*nx
    my = 4*ny
  endif
  allocate (ft_image(mx,my), STAT=ier)
  if (ier.ne.0) then
     call gagout ('E-UV_POINTING,  Error allocating FT_IMAGE memory')
     goto 999
  endif  
  call plunge_real (image_d,nx,ny,ft_image,mx,my)
  !
  ! Original image no longer needed...
  deallocate (image_d,STAT=ier)
  if (ier .ne. 0) then
     call gagout ('E-UV_POINTING,  Error deallocating IMAGE_D memory')
     goto 999
  endif
  !
  ! Template UV data set
  n = lenc(uvdata)
  if (n.le.0) goto 999
  call gildas_null(inuv_h, type = 'UVT' )
  name = uvdata(1:n)
  call sic_parsef(name,inuv_h%file,' ','.uvt')
  call gdf_read_header(inuv_h, error)
  if (error) then
    call gagout  ('E-UV_POINTING,  Cannot read input uv table') 
    goto 999
  endif
  if (inuv_h%char%type(1:9).ne.'GILDAS_UV') then
    call gagout ('E-UV_POINTING,  Input file is not a UV table')
    goto 999
  endif
  allocate (inuv_d(inuv_h%gil%dim(1),inuv_h%gil%dim(2)), STAT=ier)
  if (ier .ne. 0) then
     call gagout ('E-UV_POINTING,  Error allocating INUV_D memory')
     goto 999
  endif
  call gdf_read_data (inuv_h,inuv_d,error)
  if (error) then
    call gagout ('E-UV_POINTING,  Cannot read input file')
    goto 999
  endif  
  !
  ! Check if anomalous refraction parameters are written in the template uv table
  if ((inuv_h%gil%dim(1).ne.16).and.(add_ar)) then
    call gagout ('W-UV_POINTING,  Cannot add anomalous refraction')
    add_ar = .false.
  endif
  !
  ! Create the ouput model UV table 
  call gildas_null (outuv_h, type = 'UVT') 
  call gdf_copy_header(inuv_h,outuv_h,error) 
  n = lenc(uvmodel)
  if (n.le.0) goto 999
  name = uvmodel(1:n)
  call sic_parsef(name,outuv_h%file,' ','.uvt')
  allocate (outuv_d(outuv_h%gil%dim(1),outuv_h%gil%dim(2)),STAT=ier)
  if (ier .ne. 0) then
     call gagout ('E-UV_POINTING,  Error allocating OUTUV_D memory')
     goto 999
  endif
  !
  outuv_h%gil%dim(1) = 12  ! normal uv table. Otherwise if(add_ar) output table 
  !                        ! will also have 16 columns as the input uv table
  freq = image_h%gil%freq  ! NR: why image_h and not inuv_h ????
  if (frequency.ne.0) then
    freq = frequency*1e3
  endif
  outuv_h%gil%freq = freq
  print *,'Freq ',outuv_h%gil%freq,freq
  !
  ! Define scaling factor from Brightness to Flux (2 k B^2 / l^2)
  lambda = 299792.458e3/(freq*1e6)   ! in meter
  ! 1.38E3 is the Boltzmann constant times 10^26 to give result in Jy
  factor = 2*1.38e3/lambda**2*pixel_area
  print *,'Factor ',factor,pixel_area
  if (factor.eq.0.0) factor = 1.0
  !
  nw = inuv_h%gil%dim(1)
  nv = outuv_h%gil%dim(2)
  !
  ! Get the number of time samples
  allocate(itimes(nv),rtimes(nv),rtmp(nv),stat=ier)
  if (ier .ne. 0) then
    call gagout ('E-UV_POINTING,  Error allocating TIMES memory')
    goto 999
  endif
  call do_timelist ( &
     &    inuv_d,    &
     &    nw,nv,     &
     &    itimes,    &   ! Visibility --> Time
     &    rtimes,    &   ! Date + Time
     &    rtmp,      &   ! Time --> Visi
     &    ntt,na)  
  !
  ! Get the index order...
  allocate(iorder(ntt),stat=ier)
  if (ier .ne. 0) then
    call gagout ('E-UV_POINTING,  Error allocating IORDER memory')
    goto 999
  endif
  call gr8_trie(rtimes,iorder,ntt,error)
  deallocate(rtimes,rtmp)
  !
  ! Pointing errors
  if ((icode.gt.6).or.(icode.lt.1)) then
    call gagout('F-UV_POINTING,  Pointing mode is 1..6')
    goto 999
  endif
  !
  ! Pointing error file: gdf table
  if (mosaic.or.(icode.eq.5).or.(icode.eq.6)) then
    call gagout('I-UV_POINTING,  Reading an external table')
    n = lenc(pfile)
    if (n.le.0) goto 999
    call gildas_null(ptab_h)
    name = pfile(1:n)
    call sic_parsef(name,ptab_h%file,' ','.tab')
    call gdf_read_header(ptab_h,error)
    if (error) then
      call gagout ('E-UV_POINTING,  Cannot read error table')
      goto 999
    endif
    !
    ! Check dimension are ok
    ! We need dim(1) = number of visibilitiy dumps (NTT)
    !         dim(2) = number of antennas (NA)
    !         dim(3) = 2
    ! Table can be larger (the end won't be read)
    if ((ptab_h%gil%dim(1).lt.ntt).or.(ptab_h%gil%dim(2).lt.na).or.   &
     &      (ptab_h%gil%dim(3).ne.2)) then
      call gagout('W-UV_POINTING,  Pointing error file too small')
      goto 999
    endif
    mtt = ptab_h%gil%dim(1)
    ma =  ptab_h%gil%dim(2)
    !
    allocate (ptab_d(mtt,ma,2),STAT=ier)
    if (ier .ne. 0) then
      call gagout ('E-UV_POINTING,  Error allocating PTAB_D memory')
      goto 999
    endif 
    call gdf_read_data (ptab_h,ptab_d,error)
    if (error) then
      call gagout ('E-UV_POINTING,  Cannot read error table')
      goto 999
    endif
  endif
  !
  ! Now compute or read the errors
  sigma(1) = sigma(1)*pi/3600/180
  sigma(2) = sigma(2)*pi/3600/180
  print *,'Sigma ',sigma
  allocate(perr(2,na,ntt),STAT=ier)
  if (ier .ne. 0) then
    call gagout ('E-UV_POINTING,  Error allocating PERR memory')
    goto 999
  endif
  call do_pointerr (  &
     &    iorder,     &   ! Ordering of times
     &    ntt,na,     &
     &    perr,       &   ! Pointing errors
     &    sigma,      &   ! For each direction
     &    mosaic,     &   ! Mosaic mode
     &    ptab_d,     &   ! Pointing error table (icode=5 or 6)
     &    mtt,ma,icode,error)   
  if (error) goto 999
  !
  ! Get the model image Fourier Transform
  allocate(rtmp(2*max(mx,my)),stat=ier) 
  if (ier .ne. 0) then
    call gagout ('E-UV_POINTING,  Error allocating RTMP memory')
    goto 999
  endif
  nn(1) = mx
  nn(2) = my
  ndim = 2
  call fourt(ft_image,nn,ndim,1,0,rtmp)
  ! Recenter it
  call recent(mx,my,ft_image)
  !
  su = 1.0/(mx*image_h%gil%inc(1))
  sv = 1.0/(my*image_h%gil%inc(2))
  theta = theta*pi/180.0/3600.0    ! In radians
  suv = max(-su,sv)
  eps = 1e-8
  if (diameter.eq.0) then
    ! This is for a perfect Gaussian beam, up to precision EPS
    uvr = suv * nint(2.*sqrt(-log(2.)*log(eps))/(pi*theta)/suv)
  else
    ! Could be replaced by the Antenna Diameter, in Wavelength
    uvr = diameter / lambda
  endif
  print *,'UV cells ',uvr/suv,uvr,suv
  !
  ! Now to the job
  call do_model (inuv_d,nw,nv,itimes, &
     &    na,ntt,   &
     &    perr,   &
     &    freq,   &
     &    ft_image,   &    ! Fourier transform of sky brightness
     &    mx,my,   &
     &    rtmp,rtmp(mx+1:),   &  ! Use for U,V coordinates of grid
     &    su,sv,theta,uvr,factor,   &
     &    add_ar)
  !
  ! If icode=6 (OTF) write offsets in columns 11 and 12
  if (mosaic .or. (icode == 6)) then
     do i=8,code_uvt_last
	   outuv_h%gil%column_pointer(i) = 0
	   outuv_h%gil%column_size(i) = 0
     enddo
     outuv_h%gil%column_pointer(code_uvt_xoff) = 11
     outuv_h%gil%column_size(code_uvt_xoff) = 1
     outuv_h%gil%column_pointer(code_uvt_yoff) = 12
     outuv_h%gil%column_size(code_uvt_yoff) = 1 
     call do_offsets (inuv_d,nw,nv,itimes,ptab_d,mtt, ma)
  endif
  !
  ! Set the proper columns in case of Pointin Error,
  ! such as Anomalous Refraction information
  if (add_ar) then
     outuv_h%gil%column_pointer(code_uvt_xofi) = 13
     outuv_h%gil%column_pointer(code_uvt_yofi) = 14
     outuv_h%gil%column_pointer(code_uvt_xofj) = 15
     outuv_h%gil%column_pointer(code_uvt_yofj) = 16
     outuv_h%gil%column_size(code_uvt_xofi) = 1
     outuv_h%gil%column_size(code_uvt_xofj) = 1
     outuv_h%gil%column_size(code_uvt_yofi) = 1
     outuv_h%gil%column_size(code_uvt_yofj) = 1
  endif
  !
  ! Free memory: external pointing table for mosaics and OTF 
  if (mosaic.or.(icode.eq.5).or.(icode.eq.6)) then
    deallocate (ptab_d,STAT=ier)
    if (ier .ne. 0) then
      call gagout ('E-UV_POINTING,  Error deallocating PTAB_D memory')
      goto 999
    endif
  endif
  !
  !Free memory: global pointing table  
  deallocate (perr,STAT=ier)
  if (ier .ne. 0) then
      call gagout ('E-UV_POINTING,  Error deallocating PERR memory')
      goto 999
  endif
  !
  ! Move that damned stuff to a real UV table
  call copyuv (nv,inuv_d,nw,outuv_d,outuv_h%gil%dim(1))
  call gdf_write_image(outuv_h,outuv_d,error)
  if (error) then
    call gagout  ('E-UV_POINTING,  Cannot write output uv table') 
    goto 999
  endif
  deallocate(outuv_d,STAT=ier)
  if (ier .ne. 0) then
     call gagout ('E-UV_POINTING,  Error deallocating OUTUV_D memory')
     goto 999
  endif
  !
  stop 
  999 continue
  if(allocated(image_d)) deallocate(image_d)
  if(allocated(inuv_d))  deallocate(inuv_d)
  if(allocated(outuv_d)) deallocate(outuv_d)
  if(allocated(ptab_d))  deallocate(ptab_d)
  if(allocated(perr))    deallocate(perr)
  if(allocated(ft_image)) deallocate(ft_image)
  call sysexi(fatale)
end program uv_pointing
!
!<FF>
subroutine copyuv (nv,in,ni,out,no)
  integer ni, nv, no
  real in(ni,nv), out(no,nv)
  integer i,j
  do i = 1,nv
    do j=1,no
      out(j,i) = in(j,i)
    enddo
  enddo
end
!<FF>
subroutine plunge_real (r,nx,ny,c,mx,my)
  !-----------------------------------------------------------------
  !     Plunge a Real array into a larger Complex array
  !-----------------------------------------------------------------
  integer nx,ny                ! Size of input array
  real r(nx,ny)                ! Input real array
  integer mx,my                ! Size of output array
  real c(2,mx,my)
  !
  integer kx,ky,lx,ly
  integer i,j
  !
  kx = nx/2+1
  lx = mx/2+1
  ky = ny/2+1
  ly = my/2+1
  !
  do j=1,my
    do i=1,mx
      c(1,i,j) = 0.0
      c(2,i,j) = 0.0
    enddo
  enddo
  !
  do j=1,ny
    do i=1,nx
      c(1,i-kx+lx,j-ky+ly) = r(i,j)
    enddo
  enddo
end
!<FF>
subroutine do_timelist (visi,nw,nv,itt,dtt,vtt,ntt,na)
  !------------------------------------------------------------
  !     Build a list of times from the input visibilities
  !------------------------------------------------------------
  integer nw,nv
  real visi(nw,nv)             ! Visibilities
  integer itt(nv)              ! Pointer from visibility to time
  real*8 dtt(nv)               ! Date & Time of each time sample
  integer vtt(nv)              ! Pointer from time to visibility
  integer ntt
  integer na
  !
  integer iv,jv,it,i,la
  real date,time
  logical goon
  !
  itt(1) = 1
  vtt(1) = 1
  ntt = 1
  date = visi(4,1)
  time = visi(5,1)
  dtt(ntt) = dble(date)*dble(24*3600)+dble(time)
  !      PRINT *,'Found new time at visi ',1,DTT(NTT)
  na = max(visi(6,1),visi(7,1))
  !
  do iv=2,nv
    date = visi(4,iv)
    time = visi(5,iv)
    la = max(visi(6,iv),visi(7,iv))
    na = max(na,la)
    it = ntt
    goon = .true.
    do while (goon)
      jv = vtt(it)
      if (date.eq.visi(4,jv).and.time.eq.visi(5,jv)) then
        itt(iv) = it
        goon = .false.
      elseif  (it.eq.1) then
        ntt = ntt+1
        itt(iv) = ntt
        vtt(ntt) = iv
        dtt(ntt) = dble(date)*dble(24*3600)+dble(time)
        !               PRINT *,'Found new time at visi ',IV,DTT(NTT)
        goon = .false.
      else
        it = it-1
      endif
    enddo
  enddo
  print *,'Found ',na,' different antennas'
  print *,'Found ',ntt,' different times'
  print *,'Times ',(dtt(i),i=1,ntt)
end
!<FF>
subroutine do_pointerr (order,ntt,na,err,sigma,mosaic,   &
     &    poinerr,mtt,ma,icode,error)
  !----------------------------------------------------------------
  !     Generate the pointing errors
  !     5 Cases
  !     ICODE = 1       No error
  !     ICODE = 2       Fixed error (identical for all antennas)
  !     ICODE = 3       Purely random error
  !     ICODE = 4       Random per antenna, fixed in time
  !     ICODE = 5       Table driven error, gdf table
  !         Table dimension: NTT,NA,2
  !     ICODE = 6       Same that ICODE=5. Table driven error.
  !                     The difference is that do_model writes pointing
  !                     offsets in the uv table for OTF observations
  !
  !     If MOSAIC is true, then read table (icode=5 or 6) and add errors
  !     (icode = 1 to 4)
  !----------------------------------------------------------------
  integer ntt,na,mtt,ma
  integer order(ntt)
  real err(2,na,ntt),sigma(2)
  real poinerr(mtt,ma,2)
  integer icode
  logical error, mosaic
  !
  real rangau
  real*8 sec
  integer ia,it,jt,kt
  !
  include 'gbl_pi.inc'
  !
  error = .false.
  !
  ! No error
  do it=1,ntt
    do ia=1,na
      err(1,ia,it) = 0.0
      err(2,ia,it) = 0.0
    enddo
  enddo
  ! Purely fixed
  if (icode.eq.2) then
    do it=1,ntt
      do ia=1,na
        err(1,ia,it) = sigma(1)
        err(2,ia,it) = sigma(2)
      enddo
    enddo
    ! Purely random
  elseif (icode.eq.3) then
    do it=1,ntt
      do ia=1,na
        err(1,ia,it) = rangau(sigma(1))
        err(2,ia,it) = rangau(sigma(2))
      enddo
    enddo
    ! Random for each antenna, but fixed in time
  elseif (icode.eq.4) then
    do ia=1,na
      err(1,ia,1) = rangau(sigma(1))
      err(2,ia,1) = rangau(sigma(2))
    enddo
    do it=2,ntt
      do ia=1,na
        err(1,ia,it) = err(1,ia,1)
        err(2,ia,it) = err(2,ia,1)
      enddo
    enddo
  endif
  !
  ! For a Mosaic add the Mosaic centers, from the external file
  if (mosaic.or.(icode.eq.5).or.(icode.eq.6)) then
    ! External description: gdf file
    sec = pi/3600./180.
    do it=1,ntt
      jt = order(it)
      do ia=1,na
        err(1,ia,jt) = err(1,ia,jt)+poinerr(it,ia,1)*sec
        err(2,ia,jt) = err(2,ia,jt)+poinerr(it,ia,2)*sec
      enddo
    enddo
  endif
end
!<FF>
subroutine do_model (visi,nw,nv,itt,na,nt,err,freq,   &
     &    ftsky,mx,my,uval,vval,su,sv,thet,uvr,factor,add_ar)
  !-----------------------------------------------------------------------------
  !
  !-----------------------------------------------------------------------------
  integer nw,nv
  real visi(nw,nv)             ! Visibilities
  integer nt                   ! Number of different times
  integer itt(nv)              ! Pointer to times
  integer na                   ! Number of antennas
  real err(2,na,nt)            ! Pointing error for each antenna / time
  integer mx,my
  real*8 freq                  ! Observing frequency
  complex ftsky(mx,my)         ! Fourier Transform
  real uval(mx), vval(my)      ! u,v Values
  real su,sv                   ! Step of Fourier Transform
  real thet                    ! Beam size (in radian)
  real uvr                     ! UV radius for convolution
  real factor                  ! Input map scaling factor
  logical add_ar
  !
  real*8 u,v,lambda,uu,vv
  real*8 us,vs
  real*8 dxi,dyi,dxj,dyj
  real*8 pithet,uvthet,lnthet
  complex*16 avis,tvis,cmp
  complex*8 svis
  real*8 arg,carg,sarg
  integer ia,ja,it
  integer lx,ly,ilo,iup,jlo,jup
  integer i,iv,ii,jj
  !
  include 'gbl_pi.inc'
  !
  lambda = 299792.458d3/(freq*1d6)
  pithet = pi**2*thet**2/(4.0*log(2.))
  uvthet = -pithet/pi*su*sv * factor
  lnthet = log(2d0)/thet**2
  lx = mx/2+1
  ly = my/2+1
  us = 1.0/su
  vs = 1.0/sv
  do i=1,mx
    uval(i) = (i-lx)*su
  enddo
  do i=1,my
    vval(i) = (i-ly)*sv
  enddo
  !
  do iv = 1,nv
    u = visi(1,iv)/lambda
    v = visi(2,iv)/lambda
    ia = visi(6,iv)
    ja = visi(7,iv)
    it = itt(iv)
    dxi = err(1,ia,it)
    dyi = err(2,ia,it)
    dxj = err(1,ja,it)
    dyj = err(2,ja,it)
    if (add_ar) then
      dxi = dxi + visi(13,iv)
      dyi = dyi + visi(14,iv)
      dxj = dxj + visi(15,iv)
      dyj = dyj + visi(16,iv)
    endif
    !
    !     FT(u,v) = pi Thet^2 /(4 ln(2)) exp {-pi^2 Thet^2 / (4 ln(2)) (u^2+v^2) }
    !       * exp{ -ln(2) / Thet^2 [(Dx1-Dx2)^2^ + (Dy1-Dy2)^2] }
    !       * exp{ -i pi [(u Dx1 + v Dy1) + (u Dx2 + v Dy2)] }
    !
    !  FT(u,v) = pi Thet^2 /(4 ln(2))                       ! Scale
    !  * exp{ -ln(2) / Thet^2 [(Dx1-Dx2)^2 + (Dy1-Dy2)^2] } ! Cross-pointing error
    !  * exp {-pi^2 Thet^2 / (4 ln(2)) u^2}                 ! U attenuation term
    !  * exp { -i pi [u Dx1) + u Dx2] }                     ! U phase term
    !  * exp {-pi^2 Thet^2 / (4 ln(2)) v^2 }                ! V attenuation term
    !  * exp{ -i pi [v Dy1 + v Dy2] }                       ! V phase term
    !
    ilo = nint((u+uvr)*us) + lx
    iup = nint((u-uvr)*us) + lx
    jlo = nint((v-uvr)*vs) + ly
    jup = nint((v+uvr)*vs) + ly
    ilo = max(1,ilo)
    iup = min(mx,iup)
    jlo = max(1,jlo)
    jup = min(my,jup)
    !
    avis = (0.,0.)
    do jj = jlo,jup
      tvis = (0.,0.)
      do ii = ilo,iup
        uu = uval(ii) - u
        arg = -pi*uu*(dxi+dxj)
        carg = cos(arg)
        sarg = sin(arg)
        cmp = cmplx(carg,sarg)
        arg = exp(-pithet*uu**2)
        tvis = tvis+cmp*ftsky(ii,jj)*arg
      enddo
      vv = vval(jj) - v
      arg = -pi*vv*(dyi+dyj)
      carg = cos(arg)
      sarg = sin(arg)
      cmp = cmplx(carg,sarg)
      arg = exp(-pithet*vv**2)
      avis = avis+cmp*tvis*arg
    enddo
    !
    ! FT(u,v) = pi Thet^2 /(4 ln(2))
    !   * exp{ -ln(2) / Thet^2 [(Dx1-Dx2)^2^ + (Dy1-Dy2)^2] }
    !   * su * sv
    !
    arg = uvthet*exp(-lnthet*((dxi-dxj)**2+(dyi-dyj)**2))
    avis = avis*arg
    !
    ! Done
    svis = avis
    visi(8,iv) =  real(svis)
    visi(9,iv) = aimag(svis)
  enddo
end
!<FF> 
subroutine do_offsets (visi,nw,nv,itt,poinerr,mtt,ma)
  !-----------------------------------------------------------------------------
  !
  !-----------------------------------------------------------------------------
  integer nw,nv
  real visi(nw,nv)             ! Visibilities
  integer itt(nv)              ! Pointer to times
  integer :: mtt, ma
  real poinerr(mtt,ma,2)       ! Pointings for mosaic fields
  integer ia,ja,it
  integer iv
  real*8 sec
  !
  include 'gbl_pi.inc'
  do iv = 1,nv
    ia = visi(6,iv)
    !ja = visi(7,iv)
    it = itt(iv)
    sec = pi/3600./180.
    visi(11,iv)= poinerr(it,ia,1)*sec  
    visi(12,iv)= poinerr(it,ia,2)*sec  
    ! Note that there could be problems if the time order of the visibilities 
    ! is not the same that the time order of the pointing table, but at least 
    ! for OTF, this does not seem possible
  enddo
end
!<FF>
subroutine recent(nx,ny,z)
  !------------------------------------------------------------------------------
  ! Recenters the Fourier Transform, for easier display. The present version
  ! will only work for even dimensions.
  !------------------------------------------------------------------------------
  integer nx,ny, i, j
  complex z(nx,ny), tmp
  !
  do j=1,ny/2
    do i=1,nx/2
      tmp = z(i+nx/2,j+ny/2)
      z(i+nx/2,j+ny/2) = z(i,j)
      z(i,j) = tmp
    enddo
  enddo
  !
  do j=1,ny/2
    do i=1,nx/2
      tmp = z(i,j+ny/2)
      z(i,j+ny/2) = z(i+nx/2,j)
      z(i+nx/2,j) = tmp
    enddo
  enddo
  !
  do i=1,nx
    do j =1,ny
      if (mod(i+j,2).ne.0) then
        z(i,j) = -z(i,j)
      endif
    enddo
  enddo
end
