program uv_cct
  use gildas_def
  use gkernel_interfaces
  use image_def
  !---------------------------------------------------------------------
  ! GILDAS
  !	Compute a UV data set from a clean component table
  !	Input : a UV table (for sampling)
  !	Input : a lmv-cct file (1-channel)
  !	Output: the UV table, where the visibilities have been computed.
  !
  !     Uses an intermediate FFT with further interpolation for
  !     better speed than UV_MODEL
  !---------------------------------------------------------------------
  ! Local
  type(gildas) :: x,y,z,c
  complex, allocatable :: ifft(:,:), imap(:,:)
  integer :: nx,ny,n, i, nfft, ncomp, ier
  integer :: ichan
  character(len=filename_length) :: uvdata,image,name,uvmodel
  real :: freq, flux
  real*8 :: dx, dy
  logical :: error
  logical :: point=.false.
  !
  ! Code:
  call gildas_open
  call gildas_char('UVSAMPLE$',uvdata)
  call gildas_char('IMAGE$',image)
  call gildas_char('UVMODEL$',uvmodel)
  call gildas_inte('CHANNEL$',ichan,1)
  call gildas_real('FLUX$',flux,1)
  call gildas_logi('POINT$',point,1)
  call gildas_close
  !
  ! Input UV data file is read in slot Z
  n = lenc(uvdata)
  if (n.le.0) goto 999
  call gildas_null (z, type = 'UVT')
  name = uvdata(1:n)
  call gdf_read_gildas (z, name, '.uvt', error)
  if (error) then
    call gagout ('F-UV_MODEL,  Cannot read input UV table')
    goto 999
  endif
  call gagout('I-UV_CCT, read UV table '//z%file(1:lenc(z%file)))
  !
  ! Image: first open the .lmv-clean, in slot Y, to get the sampling.
  if (.not.point) then
    n = lenc(image)
    if (n.le.0) goto 999
    name = image(1:n)
    call gildas_null (y, type = 'IMAGE')
    call gdf_read_gildas (y, name, '.lmv-clean', error, data=.false.)
    if (error) then
      call gagout ('F-UV_MODEL,  Cannot read input image')
      goto 999
    endif
    ! Define observing frequency and other header parameters.
    nx = y%gil%dim(1)
    ny = y%gil%dim(2)
    dx = y%gil%inc(1)
    dy = y%gil%inc(2)
    freq = y%gil%freq
    call gagout('I-UV_CCT, read clean map '   &
     &      //y%file(1:lenc(y%file)))
    !
    ! Now open the CCT table, in slot C
    n = index(image,'.')
    name = image(1:n-1)
    call gildas_null (c, type = 'IMAGE')
    call gdf_read_gildas (c, name, '.lmv-cct', error, data=.false.)
    if (error) then
      call gagout('F-UV_MODEL,  Cannot read clean components')
      goto 999
    endif
    call gagout('I-UV_CCT, read component file '   &
     &      //c%file(1:lenc(c%file)))
    !
    ! we go directly to channel ichan
    allocate (c%r2d(c%gil%dim(1), c%gil%dim(2)), stat=ier)
    c%blc(2) = ichan
    c%trc(2) = ichan
    call gdf_read_data (c,c%r2d,error)
    !
    ! Get Virtual Memory & compute the FFT
    allocate (ifft(nx,ny), imap(nx,ny), stat=ier)
    ncomp = c%gil%dim(2)
    !
    ! compute the model map (at address imap)
    call do_map(nx,ny,ncomp,flux,c%r2d,imap)
    !
    ! compute its FFT (at address IFFT)
    call do_fft(nx,ny,imap,ifft)
    !
    ! Free maps
    call gdf_close_image(y,error)
    call gdf_close_image(c,error)
    if (error) goto 999
  else
    call gildas_null (y, type = 'IMAGE')
    call gdf_copy_header (z,y,error)
  endif
  !
  ! X slot will contain the model UV table.
  call gildas_null (x, type = 'IMAGE')
  call gdf_copy_header (y,x,error)             ! x%file is defined LATER
  n = lenc(uvmodel)
  if (n.le.0) goto 999
  name = uvmodel(1:n)
  call sic_parsef(name,x%file,' ','.uvt')
  x%gil%dim(1) = 10
  x%gil%dim(2) = z%gil%dim(2)
  x%gil%dim(3) = 1
  x%gil%dim(4) = 1
  x%gil%ndim = 2
  !
  ! call r8tor8 (z_conv,x_conv,12)
  x%gil%convert = z%gil%convert
  !
  x%gil%ref(1)  = y%gil%ref(1)
  x%gil%val(1)  = y%gil%freq
  x%gil%inc(1)  = y%gil%fres
  x%char%unit = z%char%unit
  x%gil%faxi = 1
  !
  call gdf_create_image (x,error)
  if (error) then
    call gagout ('F-UV_MODEL,  Cannot create output UV model')
    goto 999
  endif
  call gdf_allocate(x,error)
  if (error) goto 999
  !
  ! Copy UV data channel in Z into all channels of X
  call copyuv (x%gil%dim(1),x%gil%dim(2),x%r2d,   &
     &    z%gil%dim(1),z%r2d)
  if (error) goto 999
  !
  ! Compute the model from data in IFFT
  if (point) then
    call do_point(x%r2d,x%gil%dim(1),x%gil%dim(2),flux)
  else
    call do_model(x%r2d,x%gil%dim(1),x%gil%dim(2),   &
     &      ifft,nx,ny,freq,dx,dy)
  endif
  !
  call gdf_write_data (x, x%r2d, error)
  if (error) goto 999
  call gagout('S-UV_MODEL,  Successful completion')
  call sysexi(1)
  !
  999   call sysexi(fatale)
end program uv_cct
!<FF>
subroutine do_map(nx,ny,nc,flux,cct,map)
  integer :: nx                     !
  integer :: ny                     !
  integer :: nc                     !
  real :: flux                      !
  real :: cct(3,nc)                 !
  real :: map(nx,ny)                !
  ! Local
  ! Local:
  integer :: i, ix, iy
  real :: cmin, csum
  !-----------------------------------------------------------------------
  do iy=1, ny
    do ix=1, nx
      map(ix,iy) = 0
    enddo
  enddo
  !
  cmin = 1e10
  do i=1,nc
    cmin = min(cmin,cct(1,i))
  enddo
  write(6,*) 'Minimum feature is  ', cmin, ' Jy'
  write(6,*) 'Total flux fixed to ',flux,' Jy'
  csum = 0
  do i=1,nc
    if (abs(cct(1,i)).gt.abs(cmin)) then
      csum = csum + cct(1,i)
    endif
  enddo
  do i=1, nc
    if (abs(cct(1,i)).gt.abs(cmin)) then
      ix = nint(cct(2,i))
      iy = nint(cct(3,i))
      map(ix,iy) = map(ix,iy)+cct(1,i)/csum*flux
    endif
  enddo
end
!<FF>
subroutine do_model (visi,nc,nv,a,nx,ny,freq,xinc,yinc)
  !---------------------------------------------------------------------
  ! interpolate complex data data from regular  array A to given UV
  ! coordinates in VISI.
  !---------------------------------------------------------------------
  integer :: nc                     !
  integer :: nv                     !
  real :: visi(nc,nv)               !
  integer :: nx                     !
  integer :: ny                     !
  complex :: a(nx,ny)               !
  real :: freq                      !
  real*8 :: xinc                    !
  real*8 :: yinc                    !
  ! Local
  real*4 :: clight
  parameter (clight=299792458d0)
  real :: kwx,kwy,stepx,stepy,lambda,bfin(2),xr,yr
  complex :: aplus,amoin,azero,afin
  integer :: i, ia, ja
  logical :: inside
  equivalence (afin,bfin)
  !
  lambda = clight/(freq*1e6)
  stepx = 1./(nx*xinc)*lambda
  stepy = 1./(ny*yinc)*lambda
  !
  ! Loop on visibility
  do i = 1, nv
    kwx =  visi(1,i) / stepx + float(nx/2 + 1)
    kwy =  visi(2,i) / stepy + float(ny/2 + 1)
    ia = int(kwx)
    ja = int(kwy)
    inside = (ia.gt.1 .and. ia.lt.nx) .and.   &
     &      (ja.gt.1 .and. ja.lt.ny)
    if (inside) then
      xr = kwx - ia
      yr = kwy - ja
      !
      ! Interpolate (X or Y first, does not matter in this case)
      aplus = ( (a(ia+1,ja+1)+a(ia-1,ja+1)   &
     &        - 2.*a(ia,ja+1) )*xr   &
     &        + a(ia+1,ja+1)-a(ia-1,ja+1) )*xr*0.5   &
     &        + a(ia,ja+1)
      azero = ( (a(ia+1,ja)+a(ia-1,ja)   &
     &        - 2.*a(ia,ja) )*xr   &
     &        + a(ia+1,ja)-a(ia-1,ja) )*xr*0.5   &
     &        + a(ia,ja)
      amoin = ( (a(ia+1,ja-1)+a(ia-1,ja-1)   &
     &        - 2.*a(ia,ja-1) )*xr   &
     &        + a(ia+1,ja-1)-a(ia-1,ja-1) )*xr*0.5   &
     &        + a(ia,ja-1)
      ! Then Y (or X)
      afin = ( (aplus+amoin-2.*azero)   &
     &        *yr + aplus-amoin )*yr*0.5 + azero
      !
      visi(8,i) =  bfin(1)
      visi(9,i) = -bfin(2)
    endif
  enddo
end
!<FF>
subroutine do_point (visi,nc,nv,flux)
  !---------------------------------------------------------------------
  ! Put a point source of flux FLUX in visibilities VISI
  !---------------------------------------------------------------------
  integer :: nc                     !
  integer :: nv                     !
  real :: visi(nc,nv)               !
  real :: flux                      !
  ! Local
  integer :: i
  !
  ! Loop on visibility
  do i = 1, nv
    visi(8,i) =  flux
    visi(9,i) =  0
  enddo
end
!<FF>
subroutine do_fft (nx,ny,map,fft)
  integer :: nx                     !
  integer :: ny                     !
  real :: map(nx,ny)                !
  real :: fft(2,nx,ny)              !
  ! Local
  integer :: ix,iy,dim(2),work(4096)
  !
  ! Init
  ! Loop on channels
  dim(1) = nx
  dim(2) = ny
  do ix=1,nx
    do iy=1,ny
      fft(1,ix,iy) = map(ix,iy)
      fft(2,ix,iy) = 0
    enddo
  enddo
  call fourt(fft,dim,2,-1,1,work)
  call recent(nx,ny,fft)
end
!<FF>
subroutine copyuv (nco,nv,out,nci,in)
  integer :: nco                    !
  integer :: nv                     !
  real :: out(nco,nv)               !
  integer :: nci                    !
  real :: in(nci,nv)                !
  ! Local
  integer :: i,j
  !
  do i=1,nv
    do j=1,7
      out(j,i) = in(j,i)
    enddo
    do j=8,nco,3
      out(j,i) = 0
      out(j+1,i) = 0
      out(j+2,i) = in(10,i)
    enddo
  enddo
end
!<FF>
subroutine init0(rr,nxy,cc)
  !---------------------------------------------------------------------
  ! GDF	Fill complex array for FFT
  !---------------------------------------------------------------------
  integer :: nxy                    !
  real :: rr(nxy)                   !
  complex :: cc(nxy)                !
  ! Local
  integer :: i
  do i=1,nxy
    cc(i)=rr(i)
  enddo
end
!<FF>
subroutine recent(nx,ny,z)
  !---------------------------------------------------------------------
  ! Recenters the Fourier Transform, for easier display. The present version
  ! will only work for even dimensions.
  !---------------------------------------------------------------------
  integer :: nx                     !
  integer :: ny                     !
  complex :: z(nx,ny)               !
  ! Local
  integer :: i, j
  complex :: tmp
  !
  do j=1,ny/2
    do i=1,nx/2
      tmp = z(i+nx/2,j+ny/2)
      z(i+nx/2,j+ny/2) = z(i,j)
      z(i,j) = tmp
    enddo
  enddo
  !
  do j=1,ny/2
    do i=1,nx/2
      tmp = z(i,j+ny/2)
      z(i,j+ny/2) = z(i+nx/2,j)
      z(i+nx/2,j) = tmp
    enddo
  enddo
  !
  do i=1,nx
    do j =1,ny
      if (mod(i+j,2).ne.0) then
        z(i,j) = -z(i,j)
      endif
    enddo
  enddo
end
