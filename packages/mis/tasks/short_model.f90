!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module short_model_head
  !
  interface
     subroutine short_model_creation(sky_head, sky_data, point_data, &
          error_data, nu, b_width, noise, nfields, model_data, error)
       use image_def
       type (gildas), intent(in) :: sky_head
       real, dimension(:,:),   intent(in)  :: sky_data
       real, dimension(:,:,:), intent(in)  :: point_data, error_data
       real, dimension(:,:,:), intent(out) :: model_data
       real, intent(in) :: nu, b_width
       real, intent(inout) :: noise
       integer, intent(in)  :: nfields
       logical, intent(out) :: error
     end subroutine short_model_creation
  end interface
  !
  interface
     subroutine pointing_error_computation(pe_code,pe_sigma,error_data,error)
       real, dimension(2), intent(in) :: pe_sigma
       real, dimension(:,:,:), intent(out) :: error_data
       integer, intent(in) :: pe_code
       logical, intent(out) :: error
     end subroutine pointing_error_computation
  end interface
  !
  interface
     subroutine undersampling(original,compressed,error)
       real, dimension(:,:), intent(in) :: original
       real, dimension(:,:), intent(out) :: compressed
       logical, intent(out) :: error
     end subroutine undersampling
  end interface
  !
  interface data_padding
     subroutine data_padding_rc(input,output,error)
       real, dimension(:,:), intent(in) :: input
       complex, dimension(:,:), intent(out) :: output
       logical, intent(out) :: error
     end subroutine data_padding_rc
     subroutine data_padding_rr(input,output,error)
       real, dimension(:,:), intent(in) :: input
       real, dimension(:,:), intent(out) :: output
       logical, intent(out) :: error
     end subroutine data_padding_rr
  end interface
  !
  interface
     subroutine gauss_fft(sigma,xy_pix_size,nxy,beam,error)
       real, intent(in) :: sigma
       real, dimension(2), intent(in) :: xy_pix_size
       integer, dimension(2), intent(in) :: nxy
       real, dimension(:,:), intent(out) :: beam
       logical, intent(out) :: error
     end subroutine gauss_fft
  end interface
  !
  interface
     subroutine noise_adding(noise,model_data,nfields)
       integer, intent(in) :: nfields
       real,    intent(in) :: noise
       real, dimension(:,:,:), intent(inout) :: model_data
     end subroutine noise_adding
  end interface
  !
  interface
     subroutine calerr_adding(cal_off,cal_drift,caltime,inttime,model_data,nfields)
       integer,                intent(in)    :: nfields
       real,                   intent(in)    :: cal_drift, caltime, inttime
       real, dimension(2),     intent(in)    :: cal_off
       real, dimension(:,:,:), intent(inout) :: model_data
     end subroutine calerr_adding
  end interface
  !
end module short_model_head
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
program short_model
  use short_model_head, only : short_model_creation, &
       pointing_error_computation, noise_adding, calerr_adding
  use gkernel_interfaces
  use image_def
  use phys_const
  type (gildas) :: sky_head, model_head, point_head, error_head
  integer :: pe_code, npoint, nant, nfields, stat
  real, dimension(2) :: pe_sigma, cal_off
  real, dimension(:,:),   allocatable, save :: sky_data
  real, dimension(:,:,:), allocatable, save :: point_data, error_data
  real, dimension(:,:,:), allocatable, save :: model_data
  real :: nu, diameter, noise, inttime, caltime, cal_drift, b_width
  logical :: addnoise, addcalerr, error
  character(len=11) :: rname = 'SHORT_MODEL'
  !
  ! Initializes gildas header.
  !
  call gildas_null(sky_head)
  call gildas_null(model_head)
  call gildas_null(point_head)
  call gildas_null(error_head)
  !
  ! Gets input parameters and verifies them.
  !
  call gildas_open
  call gildas_char('SKY$',sky_head%file)
  call gildas_char('POINT$',point_head%file)
  call gildas_char('ERROR$',error_head%file)
  call gildas_char('MODEL$',model_head%file)
  call gildas_inte('NFIELDS$',nfields,1)
  call gildas_real('DIAM$',diameter,1)         ! in m.
  call gildas_real('FREQ$',nu,1)               ! in GHz.
  call gildas_real('NOISE$',noise,1)           ! in K.
  call gildas_real('CALOFF$',cal_off,2)        ! Mean and RMS amplitude offset at each calibration (in %).
  call gildas_real('CALDRIFT$',cal_drift,1)    ! RMS amplitude drift (in %/hour).
  call gildas_real('CALTIME$',caltime,1)       ! Time between 2 amplitude calibrations
  call gildas_real('INTTIME$',inttime,1)       ! Integration time on each pointing position
  call gildas_real('SERR$',pe_sigma,2)
  call gildas_inte('PEC$',pe_code,1)
  call gildas_logi('ADDNOISE$',addnoise,1)
  call gildas_logi('ADDCALERR$',addcalerr,1)
  call gildas_close
  !
  if ((nu <= 0).or.(diameter <= 0)) then
     call gagout('F-'//rname//',  Bad value(s) for frequency and/or diameter')
     call sysexi(fatale)
  endif
  !
  ! Computes beam width.
  ! Reference: 63" for 100 GHz and 12m.
  !
  b_width = rad_per_sec*(63.0*100.0*12.0)/(nu*diameter)
  print *, '  Beam Width (rad): ', b_width
  !
  ! Reads SKY header, allocates memory space and reads data.
  !
  call gdf_read_header(sky_head,error)
  if (gildas_error(sky_head,rname,error)) call sysexi(fatale)
  !
  allocate(sky_data(sky_head%gil%dim(1),sky_head%gil%dim(2)),&
       stat = sky_head%status)
  if (gildas_error(sky_head,rname,error)) call sysexi(fatale)
  !
  call gdf_read_data(sky_head,sky_data,error)
  if (gildas_error(sky_head,rname,error)) call sysexi(fatale)
  !
  ! Reads POINT header, verifies it, allocates memory space and
  ! reads data.
  !
  call gdf_read_header(point_head,error)
  if (gildas_error(point_head,rname,error)) call sysexi(fatale)
  !
  if ((point_head%gil%ndim /= 3).and.(point_head%gil%dim(3) /= 2)) then
     call gagout('F-'//rname//',  '// &
          'Dimensions of pointing center table are incorrect')
     call sysexi(fatale)
  endif
  !
  allocate(point_data(point_head%gil%dim(1),  &
                      point_head%gil%dim(2),  &
                      point_head%gil%dim(3)), &
                      stat = point_head%status)
  if (gildas_error(point_head,rname,error)) call sysexi(fatale)
  !
  call gdf_read_data(point_head,point_data,error)
  if (gildas_error(point_head,rname,error)) call sysexi(fatale)
  !
  npoint = point_head%gil%dim(1)
  nant   = point_head%gil%dim(2)
  !
  ! If there is a table of pointing errors, then reads ERROR header,
  ! verifies it, allocates memory space and reads data.
  ! Else allocates memory spaces and sets it to zero.
  !
  if (pe_code.eq.5) then
     !
     call gdf_read_header(error_head,error)
     if (gildas_error(error_head,rname,error)) call sysexi(fatale)
     !
     if ((error_head%gil%ndim /= 3).and.(error_head%gil%dim(3) /= 2)) then
     call gagout('F-'//rname//',  '// &
             'Dimensions of pointing error table are incorrect.')
        call sysexi(fatale)
     endif
     !
     allocate(error_data(error_head%gil%dim(1),  &
                         error_head%gil%dim(2),  &
                         error_head%gil%dim(3)), &
                         stat = error_head%status)
     if (gildas_error(error_head,rname,error)) call sysexi(fatale)
     !
     call gdf_read_data(error_head,error_data,error)
     if (gildas_error(error_head,rname,error)) call sysexi(fatale)
     !
     error_data = error_data*rad_per_sec
     !
  else
     !
     allocate(error_data(npoint,nant,2), stat = error_head%status)
     if (gildas_error(error_head,rname,error)) call sysexi(fatale)
     !
     pe_sigma = pe_sigma*rad_per_sec
     !
     call pointing_error_computation(pe_code,pe_sigma,error_data,error)
     if (error) call sysexi(fatale)
     !
  endif
  !
  ! Allocates memory space for the output model table.
  !
  allocate(model_data(4,nant,npoint),stat = model_head%status)
  if (gildas_error(model_head,rname,error)) call sysexi(fatale)
  !
  ! The noise level is for the *averaged* map. So needs to multiply 
  ! it by the number of antennas.
  !
  noise = noise*sqrt(real(nant))
  !
  ! Makes model of total power observation and adds noise if asked.
  !
  call short_model_creation(sky_head, sky_data, point_data, error_data, &
       nu, b_width, noise, nfields, model_data, error)
  if (error) call sysexi(fatale)
  !
  ! Adds additional and multiplicative noise to the model when asked.
  ! To have the following result:
  !       results = cal_err*(model+noise),
  ! additional noise must be done first.
  !
  if (addnoise) then
     call noise_adding(noise, model_data, nfields)
     model_head%gil%noise = noise
  endif
  if (addcalerr) then
     call calerr_adding(cal_off,cal_drift,caltime,inttime,model_data,nfields)
  endif
  !
  ! Updates header and writes output table.
  !
  model_head%char%name = sky_head%char%name
  model_head%gil%ndim = 3
  model_head%gil%dim(1:3) = shape(model_data)
  model_head%gil%freq = nu*1.0e3 ! in MHz
  !
  call gdf_write_image(model_head,model_data,error)
  if (gildas_error(model_head,rname,error)) call sysexi(fatale)
  !
  ! Frees memory space.
  !
  deallocate(sky_data,point_data,error_data,model_data, stat=stat)
  if (stat /= 0) then
     call gagout('F-'//rname//',  Deallocation error')
     call sysexi(fatale)
  endif
  !
  ! Indicates successful comletion and returns.
  !
  call gagout('I-'//rname//',  Successful completion')
  !
end program short_model
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Generate the pointing errors
!
! 5 Cases
!   pe_code = 1       No error
!   pe_code = 2       Fixed error (identical for all antennas)
!   pe_code = 3       Purely random error
!   pe_code = 4       Random per antenna, fixed in time
!   others            Should not be there
!
! Table format:
!   npointings, nantenna, 2 errors (X and Y).
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine pointing_error_computation(pe_code,pe_sigma,error_data,error)
  use gkernel_interfaces
  real, dimension(2), intent(in) :: pe_sigma
  real, dimension(:,:,:), intent(out) :: error_data
  integer, intent(in) :: pe_code
  logical, intent(out) :: error
  !
  integer :: ia, ip, nant, npoint
  character(len=256) :: rname = 'POINTING_ERROR_COMPUTATION'
  !
  error = .false.
  npoint = size(error_data,1)
  nant   = size(error_data,2)
  if (size(error_data,3).ne.2) then
     call gagout('F-'//rname//',  Wrong dimensions for the pointing error array')
     error = .true.
     return
  endif
  !
  select case (pe_code)
     !
     case (1) ! No pointing error.
        !
        error_data = 0.0
        !
     case (2) ! Purely fixed pointing error.
        !
        error_data(:,:,1) = pe_sigma(1)
        error_data(:,:,2) = pe_sigma(2)
        !
     case (3) ! Purely random pointing error.
        !
        do ip = 1, npoint
           do ia = 1, nant
              error_data(ip,ia,1) = rangau(pe_sigma(1))
              error_data(ip,ia,2) = rangau(pe_sigma(2))
           end do
        end do
        !
     case (4) ! Random for each antenna, but fixed in time.
        !
        do ia = 1, nant
           error_data(:,ia,1) = rangau(pe_sigma(1))
           error_data(:,ia,2) = rangau(pe_sigma(2))
        end do
        !
     case default ! There is a problem.
        !
        call gagout('F-'//rname//',  Pointing error code is wrong')
        error = .true.
        !
  end select
  !
end subroutine pointing_error_computation
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Convolves input image with the antenna beam by product of the Fourier
! transforms. The input image is padded with zero to avoid finite size
! effects during convolution and to be sure that the convolved map goes to
! zero at its edges.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine short_model_creation(sky_head, sky_data, point_data, &
     error_data, nu, b_width, noise, nfields, model_data, error)
  use phys_const
  use image_def
  use gkernel_interfaces, no_interface=>fourt
  use short_model_head, only : data_padding, undersampling, gauss_fft
  type (gildas), intent(in) :: sky_head
  real, dimension(:,:),   intent(in)  :: sky_data
  real, dimension(:,:,:), intent(in)  :: point_data, error_data
  real, dimension(:,:,:), intent(out) :: model_data
  real, intent(in) :: nu, b_width
  real, intent(inout) :: noise
  integer, intent(in)  :: nfields
  logical, intent(out) :: error
  !
  type (gildas) :: obs_head
  character(len=256) :: rname, message
  integer :: i, j, fft_limit, npoint
  real    :: df_f, beam_size, half_width
  real    :: lambda, Jy_per_K
  !
  integer, dimension(2) :: od_nxy, rd_nxy, os_nxy, rs_nxy, off_nxy
  integer, dimension(2) :: snxy, dnxy, onxy, ij
  real,    dimension(2) :: or_pix_size, re_pix_size, xy_pix_size, xy_blc
  real,    dimension(2) :: sky_size, data_size, tu, rtu, rij
  !
  real,    dimension(size(point_data,1),size(point_data,2),size(point_data,3)) :: xy_point
  real,    dimension(:,:), allocatable :: pad_data, res_data, obs
  complex, dimension(:,:), allocatable :: con_data
  complex, dimension(:),   allocatable :: work
  !
  logical :: save = .false.
  !
  ! Initializes variables and verifies input parameters.
  !
  fft_limit = 12 ! FFT size limited to reasonable value (4096^2).
  df_f = 1e-6    ! Searched relative flux precision.
  rname = 'SHORT_MODEL_CREATION'
  error = .true.
  call gildas_null(obs_head)
  !
  if (any(shape(point_data) /= shape(error_data))) then
     call gagout('E-'//rname//',  '// &
          'Pointing center and error tables have incoherent dimension')
     return
  endif
  !
  if (b_width == 0.0) then
     call gagout('E-'//rname//',  Beam width is zero valued')
     return
  endif
  !
  if (nu == 0.0) then
     call gagout('E-'//rname//',  Model frequency is zero valued')
     return
  endif
  !
  if (noise == 0.0) then
     call gagout('E-'//rname//',  Noise is zero valued')
     return
  endif
  !
  error = .false.
  !
  ! Computes needed quantities.
  !
  xy_point = point_data+error_data
  !
  or_pix_size(1) = sky_head%gil%inc(1)
  or_pix_size(2) = sky_head%gil%inc(2)
  !
  sky_size(1) = abs(sky_head%gil%inc(1))*sky_head%gil%dim(1)
  sky_size(2) = abs(sky_head%gil%inc(2))*sky_head%gil%dim(2)
  !
  ! The 2.O factor comes from the fract that b_width is the full width and
  ! we only need the half width. The sqrt(log(2.0)) factor comes from the
  ! fact that we have width at half maximum and we need width at max/e.
  !
  half_width = abs(b_width)/(2.0*sqrt(log(2.0)))
  !
  ! Finds needed data size to achieve zero aliasing.
  !
  beam_size = 8*half_width
  data_size = sky_size+beam_size
  !
  ! Size of the sampling pixel needed to achieve the flux precision.
  ! * If larger than original pixel size, then undersampling can perhaps
  !   be applied to speed up (Not obvious because undersampling is easy
  !   only when the compression factor is a power of 2).
  ! * If smaller than original pixel size, nothing can be done to speed up
  !   without precision loss.
  !
  re_pix_size = half_width*sqrt(df_f*exp(-1.0))
  !
  ! Finds power of two corresponding to original and resampled data size
  ! and to original and resampled sky size. Those quantities are needed to
  ! decide whether resampling is useful.
  !
  od_nxy = int(ceiling(log(abs(data_size/or_pix_size))/log(2.0)))
  rd_nxy = int(ceiling(log(abs(data_size/re_pix_size))/log(2.0)))
  !
  os_nxy = int(ceiling(log(abs(sky_size/or_pix_size))/log(2.0)))
  rs_nxy = int(ceiling(log(abs(sky_size/re_pix_size))/log(2.0)))
  !
  ! * Resamples when there is speed gain without precision loss.
  ! * Else just pads data to obtain power of 2 image size.
  ! * Always limits the size of the FFT needed for convolution to a
  !   reasonable value (Total data size < fft_limit^2).
  ! * `data_size' is recomputed to take into account all the possible
  !   paddings.
  !
  if (any(rd_nxy < od_nxy)) then
     !
     if (sum(rd_nxy) > 2*fft_limit) then
        call gagout('I-'//rname//',  Limitation of the FFT size by resampling')
        off_nxy = max(0,(rd_nxy-fft_limit))
        rs_nxy = max(0,(rs_nxy-off_nxy))
        rd_nxy = rd_nxy-off_nxy
     else
        call gagout('I-'//rname//',  Resampling of original image')
     endif
     !
     onxy = 2**os_nxy
     snxy = 2**rs_nxy
     dnxy = 2**rd_nxy
     xy_pix_size = or_pix_size*2.0**(os_nxy-rs_nxy)
     !
     allocate(pad_data(onxy(1),onxy(2)))
     call data_padding(sky_data,pad_data,error)
     if (error) return
     !
     allocate(res_data(snxy(1),snxy(2)))
     call undersampling(pad_data,res_data,error)
     if (error) return
     !
     deallocate(pad_data)
     !
  else if (sum(od_nxy) > 2*fft_limit) then
     !
     call gagout('I-'//rname//',  Limitation of the FFT size by resampling')
     !
     off_nxy = max(0,(od_nxy-fft_limit))
     rs_nxy = max(0,(os_nxy-off_nxy))
     od_nxy = od_nxy-off_nxy
     !
     onxy = 2**os_nxy
     snxy = 2**rs_nxy
     dnxy = 2**od_nxy
     xy_pix_size = or_pix_size*2.0**(os_nxy-rs_nxy)
     !
     allocate(pad_data(onxy(1),onxy(2)))
     call data_padding(sky_data,pad_data,error)
     if (error) return
     !
     allocate(res_data(snxy(1),snxy(2)))
     call undersampling(pad_data,res_data,error)
     if (error) return
     !
     deallocate(pad_data)
     !
  else
     !
     snxy = 2**os_nxy
     dnxy = 2**od_nxy
     xy_pix_size = or_pix_size
     !
     allocate(res_data(snxy(1),snxy(2)))
     call data_padding(sky_data,res_data,error)
     if (error) return
     !
  end if
  !
  data_size = real(dnxy)*abs(xy_pix_size)
  !
  print *, '  Size of the FFT: ', dnxy
  !
  ! Makes the convolution.
  ! Adds zero padding to avoid finite size effects.
  ! Analyticallly computes the Fourier transform of the beam because the
  ! beam is assumed gaussian.
  !
  allocate(con_data(dnxy(1),dnxy(2)))
  allocate(work(2*maxval(dnxy)))
  call data_padding(res_data,con_data,error)
  if (error) return
  deallocate(res_data)
  !
  call fourt(con_data,dnxy,2,1,0,work)
  !
  allocate(obs(dnxy(1),dnxy(2)))
  call gauss_fft(half_width,xy_pix_size,dnxy,obs,error)
  if (error) return
  !
  con_data = con_data*obs
  !
  call fourt(con_data,dnxy,2,-1,0,work)
  obs = real(con_data)/real(product(dnxy))
  !
  ! Transforms from brightness (K) to flux (Jy) unit.
  !
  lambda = clight/(nu*1e9) ! m
  Jy_per_K = 2.0e26*kbolt/(lambda**2)
  obs = obs*Jy_per_K
  !
  ! When asked, saves the convolved map for testing purpose.
  !
  if (save) then
     obs_head%file = 'convolved.gdf'
     obs_head%gil%ndim = 2
     obs_head%gil%dim(1:2) = shape(obs)
     call gdf_write_image(obs_head,obs,error)
     if (gildas_error(obs_head,rname,error)) return
  endif
  !
  ! Computes bottom,left corner of the convolved image for the sampling.
  !
  xy_blc(1) = (1-sky_head%gil%ref(1))*sky_head%gil%inc(1)+sky_head%gil%val(1)
  xy_blc(2) = (1-sky_head%gil%ref(2))*sky_head%gil%inc(2)+sky_head%gil%val(2)
  ! Does not work: only the 2nd element is updated!
  !  xy_blc = xy_blc-sign(0.5*(data_size-sky_size),xy_pix_size)
  xy_blc(1) = xy_blc(1)-sign(0.5*(data_size(1)-sky_size(1)),xy_pix_size(1))
  xy_blc(2) = xy_blc(2)-sign(0.5*(data_size(2)-sky_size(2)),xy_pix_size(2))
  !
  ! Finds the pixel closest to each pointing position and then makes
  ! a bilinear interpolation.
  !
  ! Loop over the real pointing position.
  !
  do j = 1, size(xy_point,1)
     !
     ! Loop over the antenna number.
     !
     do i = 1, size(xy_point,2)
        !
        rij = (xy_point(j,i,:)-xy_blc)/xy_pix_size
        ij = 1+int(rij)
        if (any(ij < 1) .or. any(ij > (shape(obs)-1))) then
           call gagout('I-'//rname//',  Pointing # i outside the computed area')
           call gagout('I-'//rname//',  Corresponding value set to BLANK')
           model_data(4,i,j) = obs_head%gil%bval ! Default blank value.
        else
           !
           tu = rij-real(ij)+1.0
           rtu = 1.0-tu
           !
           model_data(4,i,j) = &
                obs(ij(1),  ij(2))  *product(rtu) + &
                obs(ij(1)+1,ij(2)+1)*product(tu)  + &
                obs(ij(1)+1,ij(2))  *tu(1)*rtu(2) + &
                obs(ij(1)  ,ij(2)+1)*tu(2)*rtu(1)
           !
        endif
     end do
  end do
  !
  ! Fills out the weight column of the model array.
  !
  ! Converts `noise' from K to Jy. There is an additional factor relative
  ! to above because the noise must here be explicitely converted from
  ! a brigthness to a flux. This was done automatically in the convolution
  ! above.
  !
  ! The 1.0e6 factor takes into account the fact that frequency must be
  ! expressed in MHz for the weight computation.
  !
  ! Finally the input noise is a value given for the whole map. So must
  ! multiplicate it by the number of position. This is complicated by the
  ! fact that the first `nfields' lines of the model are the position of
  ! the mosaic for use by `uv_zero' and all the other ones are the
  ! position of a real map for use by `uv_single'.
  !
  npoint = size(point_data,1)
  noise = noise*(pi*half_width**2)*Jy_per_K
  model_data(3,:,1:nfields) = 1.0/(1.0e6*nfields*noise**2)
  model_data(3,:,(nfields+1):npoint) = &
       1.0/(1.0e6*(npoint-nfields)*noise**2)
  !
  ! Fills out the position columns of the model array.
  !
  do j = 1, size(point_data,2)     ! Loop over antenna number.
     do i = 1, size(point_data,1)  ! Loop over pointing position.
        model_data(1:2,j,i) = point_data(i,j,:)
     end do
  end do
  !
  ! Frees memory space and returns.
  !
  deallocate(con_data, obs)
  !
end subroutine short_model_creation
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine undersampling(original,compressed,error)
  real, dimension(:,:), intent(in) :: original
  real, dimension(:,:), intent(out) :: compressed
  logical, intent(out) :: error
  !
  complex, allocatable, dimension(:,:)  :: or_data, co_data
  complex, allocatable, dimension(:) :: work
  integer, dimension(2) :: onxy, cnxy, cnxy_2, onxy_2
  character(len=256) :: rname = 'UNDERSAMPLING'
  !
  ! Initialization and verification of input parameters.
  !
  error = .true.
  !
  onxy = shape(original)
  cnxy = shape(compressed)
  allocate(or_data(onxy(1),onxy(2)))
  allocate(co_data(cnxy(1),cnxy(2)))
  allocate(work(max(onxy(1),onxy(2))))
  !
  if (any(iand(onxy,onxy-1) /= 0)) then
     call gagout('F-'//rname//',  Original dimensions are not 2^n')
     return
  endif
  if (any(iand(cnxy,cnxy-1) /= 0)) then
     call gagout('F-'//rname//',  Compressed dimensions are not 2^n')
     return
  endif
  !
  if (any(onxy < cnxy)) then
     call gagout('I-'//rname//',  Expansion asked, not compression')
     return
  endif
  !
  error = .false.
  !
  if (all(onxy == cnxy)) then
     call gagout('I-'//rname//',  Compression factors = 1. Does nothing')
     compressed = original
     return
  endif
  !
  ! Computes the FFT of the original data.
  ! Takes the 4 corners (low frequencies) and puts them in a small array.
  ! Obtains the compressed result by an inverse FFT of the small array.
  !
  or_data = cmplx(original)
  call fourt(or_data,shape(or_data),2,1,0,work)
  !
  cnxy_2 = cnxy/2
  onxy_2 = onxy-cnxy_2+1
  !
  co_data(1:cnxy_2(1),1:cnxy_2(2)) = &
       or_data(1:cnxy_2(1),1:cnxy_2(2))
  co_data(1:cnxy_2(1),(cnxy_2(2)+1):cnxy(2)) = &
       or_data(1:cnxy_2(1),onxy_2(2):onxy(2))
  co_data((cnxy_2(1)+1):cnxy(1),1:cnxy_2(2)) = &
       or_data(onxy_2(1):onxy(1),1:cnxy_2(2))
  co_data((cnxy_2(1)+1):cnxy(1),(cnxy_2(2)+1):cnxy(2)) = &
       or_data(onxy_2(1):onxy(1),onxy_2(2):onxy(2))
  !
  call fourt(co_data,shape(co_data),2,-1,0,work)
  compressed = real(co_data)/(real(product(onxy)))
  !
end subroutine undersampling
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine data_padding_rc(input,output,error)
  real, dimension(:,:), intent(in) :: input
  complex, dimension(:,:), intent(out) :: output
  logical, intent(out) :: error
  !
  integer, dimension(2) :: inxy, onxy, offset, offnxy
  !
  ! Initialization and input parameter verification.
  !
  inxy = shape(input)
  onxy = shape(output)
  offset = nint((onxy-inxy)/2.0)
  !
  if (any(offset < 0)) then
     call gagout('F-DATA_PADDING,  Incompatible dimensions')
     error = .true.
  else
     error = .false.
  endif
  !
  offnxy = offset+inxy
  offset = offset+1
  !
  ! Pads.
  !
  output = cmplx(0.0)
  output(offset(1):offnxy(1),offset(2):offnxy(2)) = cmplx(input)
  !
end subroutine data_padding_rc
!
subroutine data_padding_rr(input,output,error)
  real, dimension(:,:), intent(in) :: input
  real, dimension(:,:), intent(out) :: output
  logical, intent(out) :: error
  !
  integer, dimension(2) :: inxy, onxy, offset, offnxy
  !
  ! Initialization and input parameter verification.
  !
  inxy = shape(input)
  onxy = shape(output)
  offset = nint((onxy-inxy)/2.0)
  !
  if (any(offset < 0)) then
     call gagout('F-DATA_PADDING,  Incompatible dimensions')
     error = .true.
  else
     error = .false.
  endif
  !
  offnxy = offset+inxy
  offset = offset+1
  !
  ! Pads.
  !
  output = 0.0
  output(offset(1):offnxy(1),offset(2):offnxy(2)) = input
  !
end subroutine data_padding_rr
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine gauss_fft(sigma,xy_pix_size,nxy,beam,error)
  use phys_const
  real, intent(in) :: sigma
  real, dimension(2), intent(in) :: xy_pix_size
  integer, dimension(2), intent(in) :: nxy
  real, dimension(:,:), intent(out) :: beam
  logical, intent(out) :: error
  !
  integer :: i,ier,nxy_max
  integer, dimension(2) :: nxy_2
  real,    dimension(:,:), allocatable :: xy_point
  real,    dimension(:), allocatable :: c_vector
  real,    dimension(:,:), allocatable :: xy_freq, argument, xy_beam
  !
  ! Initialization and input parameter verification.
  !
  if (any(shape(beam) /= nxy)) then
     call gagout('F-GAUSS_FFT,  "beam" dimensions incompatible with "nxy"')
     error = .true.
     return
  else
     error = .false.
  endif
  !
  nxy_max = max(nxy(1),nxy(2))
  allocate(xy_point(nxy(1),nxy(2)),c_vector(nxy_max),stat=ier)
  if (ier.ne.0) then
     call gagout('F-GAUSS_FFT, Could not allocate memory for xy_point or c_vector')
     write(*,*) "IER = ",ier
     error = .true.
     return
  endif
  allocate(xy_freq(nxy_max,2),argument(nxy_max,2),xy_beam(nxy_max,2),stat=ier)
  if (ier.ne.0) then
     call gagout('F-GAUSS_FFT, Could not allocate memory for xy_freq, argument or xy_beam')
     write(*,*) "IER = ",ier
     error = .true.
     return
  endif
  !
  nxy_2 = nxy/2
  !
  ! Computes the grid where the Gaussian will have to be computed.
  ! Uses the fact that exp(a+b) = exp(a)*exp(b) to delay 2D computation
  ! at the end.
  !
  c_vector = (/ (real(i), i = 1, maxval(nxy)) /)
  xy_freq(:,1) = c_vector-(nxy_2(1)+1.0)
  xy_freq(:,2) = c_vector-(nxy_2(2)+1.0)
  xy_freq(:,1) = xy_freq(:,1)/(nxy(1)*abs(xy_pix_size(1))) ! Sign problem?
  xy_freq(:,2) = xy_freq(:,2)/(nxy(2)*abs(xy_pix_size(2))) ! Sign problem?
  !
  ! Computes the unidimensional Gaussian where possible.
  !
  argument = (xy_freq*(pi*sigma))**2
  where (argument <= (0.5*abs(range(argument(1,1)))))
     xy_beam = (sqrt(pi)*sigma)*exp(-argument)
  elsewhere
     xy_beam = 0.0
  end where
  !
  ! Shifting to obtain the warped around order needed by the FFT algorithm.
  !
  xy_beam(1:nxy(1),1) = cshift(xy_beam(1:nxy(1),1),dim=1,shift=-nxy_2(1))
  xy_beam(1:nxy(2),2) = cshift(xy_beam(1:nxy(2),2),dim=1,shift=-nxy_2(2))
  !
  ! Multiplication.
  !
  do i = 1, nxy(2)
     beam(:,i) = xy_beam(1:nxy(1),1)*xy_beam(i,2)
  end do
  !
end subroutine gauss_fft
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine noise_adding(noise,model_data,nfields)
  integer, intent(in) :: nfields
  real,    intent(in) :: noise
  real, dimension(:,:,:), intent(inout) :: model_data
  !
  real :: factor1, factor2
  real, dimension(size(model_data,2),size(model_data,3)) :: noises, noises0
  real :: rangau
  integer :: i,j, npoint
  !
  ! Adds additional noise to the model.
  !
  npoint = size(model_data,3)
  !
  ! The input noise is for the whole observation run.
  !
  ! The first `nfields' lines of the model are the position of
  ! the mosaic for use by `uv_zero' and all the other ones are the
  ! position of a real map for use by `uv_single'.
  !
  ! We thus need 2 different noise sigma per position.
  !
  factor1 = sqrt(real(nfields))
  factor2 = sqrt(real(max(1,(npoint-nfields))))
  !
  ! Loop over the antenna number.
  !
  do j = 1, size(model_data,2)
     !
     ! Loop over the pointing positions.
     !
     do i = 1, npoint
        noises0(j,i) = rangau(abs(noise))
     end do
     !
  end do
  !
  ! Now select the same samples for the SD map used by uv_single and
  ! for the mosaic centers used by uv_zero.
  !
  noises(:,1:nfields)          = noises0(:,1:nfields)*factor1
  noises(:,(nfields+1):npoint) = noises0(:,1:(npoint-nfields))*factor2
  !
  model_data(4,:,:) = model_data(4,:,:)+noises
  !
end subroutine noise_adding
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine calerr_adding(cal_off,cal_drift,caltime,inttime,model_data,nfields)
  integer,                intent(in)    :: nfields
  real,                   intent(in)    :: cal_drift, caltime, inttime
  real, dimension(2),     intent(in)    :: cal_off
  real, dimension(:,:,:), intent(inout) :: model_data
  !
  integer :: i,j, istart, iend, npoint, ndump, ncal
  real, dimension(:), allocatable :: time
  real, dimension(size(model_data,2),size(model_data,3)) :: cal_errors, cal_errors0
  real, dimension(size(model_data,2)) :: calerr_off, calerr_drift
  real :: cal_off_mean, cal_off_sigma, cal_drift_sigma, rangau
  !
  ! Adds multiplicative noise to the model.
  ! The multiplicative PDF model is not very good.
  !
  ! Converts input parameters.
  !
  npoint = size(model_data,3)
  !
  cal_off_mean     = 1.0+cal_off(1)/100.0     ! Original unit: % and going from error to absolute value
  cal_off_sigma    = cal_off(2)/100.0         ! Original unit: %
  cal_drift_sigma  = cal_drift/(3600.0*100.0) ! Original unit: %/hour
  !
  ! Computes the drift for each antenna. This will not change with calibration.
  ! Loop over the antenna number.
  !
  do j = 1, size(model_data,2)
     !
     calerr_drift(j) = abs(rangau(abs(cal_drift_sigma)))
     !
  end do
  !
  print *, "Slope of amplitude calibration drift by antennas: ", calerr_drift
  print *, " "
  !
  ! Computes how many calibration will there be.
  !
  ndump = caltime/inttime
  ncal  = npoint/ndump
  !
  ! Makes an array of times between calibration.
  !
  allocate(time(ndump))
  time = (/ (real(i), i = 1, ndump)  /)
  time = time*inttime
  !
  ! Loop over the number of calibration.
  !
  istart = 1
  iend   = ndump
  !
  do i = 1, ncal
     !
     ! Loop over the antenna number.
     !
     do j = 1, size(model_data,2)
        !
        calerr_off(j) = abs(cal_off_mean+rangau(abs(cal_off_sigma)))
        cal_errors0(j,istart:iend) = calerr_off(j)+calerr_drift(j)*time
        !
     end do
     !
     print *, "Amplitude calibration #: ", i
     print *, "Amplitude calibration offset by antennas: ", calerr_off
     print *, " "
     !
     istart = iend+1
     iend = iend+ndump
     !
  end do
  !
  ! Takes care of the fact we can not finish with a calibration.
  !
  iend = iend-ndump
  !
  if (iend.lt.npoint) then
     !
     iend = npoint
     !
     ! Loop over the antenna number.
     !
     do j = 1, size(model_data,2)
        !
        calerr_off(j) = abs(cal_off_mean+rangau(abs(cal_off_sigma)))
        cal_errors0(j,istart:iend) = calerr_off(j)+calerr_drift(j)*time(1:(iend-istart+1))
        !
     end do
     !
     print *, "Last amplitude calibration"
     print *, "Amplitude calibration offset by antennas: ", calerr_off
     print *, " "
     !
  endif
  !
  ! Now select the same samples for the SD map used by uv_single and
  ! for the mosaic centers used by uv_zero.
  !
  cal_errors(:,1:nfields)          = cal_errors0(:,1:nfields)
  cal_errors(:,(nfields+1):npoint) = cal_errors0(:,1:(npoint-nfields))
  !
  model_data(4,:,:) = cal_errors*model_data(4,:,:)
  !
end subroutine calerr_adding
!
!!!!!!!!!!!!!!!!!!!!!!! short_model.f90 ends here !!!!!!!!!!!!!!!!!!!!!!!!!
