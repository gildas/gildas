program array_project
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Project a 2-D array onto the (X,Y,Z) coordinate system at the
  ! given longitude and latitude. An additional N/S stretching factor
  ! can be applied
  !---------------------------------------------------------------------
  ! Global
  real :: calcul_rayon_max
  ! Local
  integer :: modtyp
  integer :: nbant
  real :: diam
  !
  integer :: listconfarray, listaleaflag, maxant, k
  integer :: ier,lun,i
  real :: altitude
  !
  parameter (maxant=256)
  real :: radius
  character(len=4) :: liste_noms(maxant)
  character(len=12) :: instrume, config
  character(len=128) :: file,chain
  real :: liste_diam(maxant), liste_x(maxant), liste_y(maxant)
  real :: liste_z(maxant), ro_min, angle, totsurf
  real :: lx(maxant), ly(maxant), lz(maxant)
  real :: latitude, geopos(2), stretch
  real :: pi, array(32),  cosl, sinl
  integer :: column(3), lcol, fline
  !
  angle = 0.0
  pi = acos(-1.0)
  call gagout('I-ARRAY_PROJECT,  Version 1.2 21-Mar-2016')
  !
  call gildas_open
  call gildas_char('ARRAY$',instrume)
  call gildas_real('LONGITUDE$',geopos,1)
  call gildas_real('LATITUDE$',geopos(2),1)
  latitude = geopos(2)*pi/180.0
  call gildas_char('CONFIGURATION$',config)
  call gildas_real('DIAMETER$',diam,1)
  call gildas_char('FILE$',file)
  call gildas_inte('FIRST$',fline,1)
  call gildas_inte('COLUMNS$',column,3)
  call gildas_real('STRETCH$',stretch,1)
  call gildas_close
  !
  ier = sic_getlun(lun)
  open (unit=lun,file=file,status='old')
  do i=1,fline-1
    read (lun,*)
  enddo
  !
  lcol = max(column(1),column(2))
  lcol = max(lcol,column(3))
  nbant = 0
  do
    read(lun,'(a)',iostat=ier) chain
    if (ier.lt.0) exit ! EOF
    if (ier.gt.0) then
      call gagout('F-ARRAY_PROJECT, Error reading line')
      call sysexi(fatale)
    endif
    if (chain.eq.'') cycle ! empty line
    if (chain(1:1).eq.'#') cycle ! comment line
    read(chain,*,iostat=ier) (array(i),i=1,lcol)
    if (ier.ne.0) then 
      call gagout('F-ARRAY_PROJECT, Error decoding line:')
      call gagout(trim(chain))
      call sysexi(fatale)
    endif
    nbant = nbant+1
    liste_x(nbant) = array(column(1)) 
    liste_y(nbant) = array(column(2)) 
    if (column(3).ne.0) liste_z(nbant) = array(column(3))
  enddo
  close (unit=lun)
  call sic_frelun(lun)
  !
  ! Initial (x,y,z) (in local plane + altitude)
  !     liste_x > 0 towards North
  !     liste_y > 0 towards East
  !     liste_z == Altitude
  !
  ! Caution: CASA convention is offset east first, then offset north
  !
  ! Final (X,Y,Z)
  !     LX > 0 outwards Earth Center // Equator
  !     LY > 0 towards East
  !     LZ > 0 towards North // Earth axis
  !
  cosl = cos(latitude)
  sinl = sin(latitude)
  do k=1,nbant
    liste_x(k) = stretch*liste_x(k)    ! Change the N/S value
    lx(k)= - sinl * liste_x(k) + cosl * liste_z(k)
    ly(k)= liste_y(k)
    lz(k)= cosl * liste_x(k) + sinl * liste_z(k)
  enddo
  !
  do k=1,nbant
    write(liste_noms(k),'(I3)') k
  enddo
  totsurf=nbant*pi*diam**2/4.
  !
  ! Rayon Min ou Max ?
  ro_min=calcul_rayon_max(nbant, liste_x ,liste_y)
  !
  ier = sic_getlun(lun)
  file = instrume(1:lenc(instrume))//'-'//   &
     &    config(1:lenc(config))//'.cfg'
  call sic_lower(file)
  open (unit=lun,file=file,status='UNKNOWN',recl=128,   &
     &    form='FORMATTED')
  altitude = 5000.
  write (lun,'(A)') '! Commentaires:'
  write (lun,'(A,g14.7)') '! ALTITUDE  = ',altitude
  write (lun,'(A,g14.7)') '! LONGITUDE = ',geopos(1)
  write (lun,'(A,g14.7)') '! LATITUDE  = ',geopos(2)
  write (lun,'(A,I5)')    '! NBANT     = ',nbant
  write (lun,'("!")')
  write (lun,'("! Nom Ants, Lx, Ly, Lz, Diam Ants")')
  write (lun,'(A,F7.1)') '! RADIUS     = ',ro_min
  write (lun,'(A,F7.1)') '! ANGLE      = ',angle
  do i=1,nbant
    write (lun,*) liste_noms(i), lx(i), ly(i), lz(i),   &
     &      diam, liste_y(i), liste_x(i)
    print *,liste_noms(i), lx(i), ly(i), lz(i),   &
     &      diam, liste_y(i), liste_x(i)
  enddo
  close(lun)
  !
end program array_project
!<FF>
function calcul_rayon_max(maxant, liste_x ,liste_y)
  real :: calcul_rayon_max          !
  integer ::     maxant             !
  real ::        liste_x(maxant)    !
  real ::        liste_y(maxant)    !
  ! Local
  integer ::     i,j
  real ::        ro_max, ro_cour
  !
  ro_max=0.
  do i=1,maxant
     do j=1,maxant
        ro_cour=(liste_x(i)-liste_x(j))**2+(liste_y(i)-liste_y(j))**2
    	if (ro_cour.gt.ro_max) ro_max=ro_cour
     enddo
  enddo	
  calcul_rayon_max = sqrt(ro_max)
end
