program uv_splitfield
  use gkernel_interfaces
  !
  ! Split a UV table according to a Time-Baseline
  ! Sequence, in order to create an ensemble of
  ! tables used for mosaicing...
  !
  ! If FIELDS$ = 0
  !		Find the number of fields 
  ! Else
  !		Split in specified number of fields
  ! Original version makes some crazy assumptions about time ordering 
  ! 	and so.
  ! This can now be removed thanks to the new UV data file format.
  !
  ! Note that UV_MAP could conceivably work on the whole UV table
  ! and create a data hypercube with one "cube" par field. Identification
  ! of pointing center not straightforward however in this case (no
  ! place holder for this information). Perhaps just the field number,
  ! with reference to the primary beam cube ?
  !	This would simplify Mapping, which would have all information
  ! to treat the mosaic.
  ! See CLEAN in this case...
  !
  !
  character*256 nami,namo
  integer nfield
  logical error
  !
  call gildas_open
  call gildas_char('INPUT$',nami)
  call gildas_char('OUTPUT$',namo)
  call gildas_inte('FIELDS$',nfield,1)
  call gildas_close
  call sub_splitfield(nami,namo,nfield,error)
  call gagout('I-UV_SPLIFIELD,  Successful completion')
end program uv_splitfield
!<FF>
subroutine sub_splitfield(nami,namo,nfield,error)
  use image_def
  use gkernel_interfaces
  character(len=*), intent(inout) :: nami  ! Input file name
  character(len=*), intent(inout) :: namo  ! Output file name
  integer, intent(in) :: nfield  ! Number of fields
  logical, intent(out) ::  error ! Error flag
  !
  type (gildas) :: hin
  real(kind=4), allocatable :: din(:,:)
  integer nt,nv,nbas,ntime
  integer ier,i,istart,iend,istep
  real(kind=8), allocatable :: altimes(:), intimes(:)
  integer, allocatable :: nvisi(:)
  character(len=256) file
  !
  error = .false.
  !
  ! Read Header of a sorted, UV table
  call gildas_null(hin, type= 'UVT')
  call sic_parsef (nami,hin%file,' ','.uvt')
  call gdf_read_header (hin,error)
  if (error) return
  allocate (din(hin%gil%dim(1),hin%gil%dim(2)),stat=ier)
  call gdf_read_data (hin,din,error)
  !!print *,'DIN ',(din(1:10,i),i=1,10)
  if (error) return
  nt = hin%gil%dim(1)
  nv = hin%gil%dim(2)
  !
  ! Define the problem size
  allocate (intimes(nv),altimes(nv),nvisi(nv),stat=ier)
  call analyze_uvtable(din,nt,nv,intimes,altimes,nvisi,nbas,ntime)
  !
  if (nfield.eq.0) then
    !
    ! Write the procedure returning the number of time samples
    call sic_parsef(namo,file,' ','.map')
    open (unit=1,file=file,status='NEW',iostat=ier)
    write(1,'(A,I6)') 'LET &1 ',ntime
    write(file,'(i6)') ntime
    i = 6
    call sic_black(file,i)
    write(1,'(A)') 'DEFINE INTEGER &2['//file(1:i)//'] /global'
    write(1,'(A)') 'begin data uv_splitfield.dat'
    do i=1,ntime
      write(1,*) nvisi(i)
    enddo
    write(1,'(A)') 'end data uv_splitfield.dat'
    write(1,'(A)') 'accept &2 /column uv_splitfield.dat'
    write(1,'(A)') 'sic delete uv_splitfield.dat'
    close(unit=1)
  else
    !
    ! Split the input tables into the Nfield fields
    call load_fields(hin,din,nt,nv,intimes,altimes,nvisi,ntime, &
     &    namo,nfield,error)
  endif
  !
  deallocate (intimes,altimes,nvisi)
  if (error) return
  call gagout('I-SPLITFIELD,  Successful completion')
  return
end subroutine sub_splitfield
!
subroutine analyze_uvtable(din,nt,nv,intimes,altimes,nvisi,nbas,ntime)
  use gkernel_interfaces
  integer, intent(in) :: nv
  integer, intent(in) :: nt
  real(kind=4), intent(in) :: din(nt,nv)
  real(kind=8), intent(out) :: intimes(nv)
  real(kind=8), intent(out) :: altimes(nv)
  !
  integer, intent(out) :: nbas
  integer, intent(out) :: ntime
  integer, intent(out) ::  nvisi(nv)
  !
  logical error
  !
  integer, allocatable :: it(:)
  integer nd,iv,ii
  logical ok
  character*60 chain
  integer i,j
  !
  error = .false.
  !
  !!Print *,'DIN ',((din(i,j),i=1,10),j=1,10)
  !
  ! Scan how many dates
  nd = 1
  altimes(nd) = din(4,1)
  do iv=1,nv
    intimes(iv) = 0
    do ii=1,nd
      if (din(4,iv).eq.altimes(ii)) then
        intimes(iv) = ii
      endif
    enddo
    if (intimes(iv).eq.0) then
      nd = nd+1
      altimes(nd) = din(4,iv)
      intimes(iv) = nd
    endif
  enddo
  write(chain,*) 'Found ',nd,' dates '
  call gagout(chain(2:))
  !
  ! Time compacted value
  do iv=1,nv
    intimes(iv) = intimes(iv)-1.d0+din(5,iv)/86400d0
  enddo
  !
  ! Find out how many times...
  altimes = 0.0
  altimes(1) = intimes(1)
  nvisi(1) = 1
  ntime = 1
  do iv=2,nv
    ok = .false.
    ii = 1
    do while (.not.ok.and.ii.le.ntime)
      if (intimes(iv).eq.altimes(ii)) then
        ok = .true.
        nvisi(ii) = nvisi(ii)+1
      else
        ii = ii+1
      endif
    enddo
    if (.not.ok) then
      ntime = ntime+1
      altimes(ntime) = intimes(iv)
      nvisi(ntime) = 1
    endif
  enddo
  !
  write(chain,*) 'I-UV,  Found ',ntime,' times '
  call gagout(chain(2:))
  !
  ! Pending nightmare: are the times sorted ?
  ! Answer: of course not, let us do it now...
  allocate(it(nv))
  do ii=1,ntime
    it(ii) = ii
  enddo
  call gr8_trie(altimes,it,ntime,error)
  !
  ! Find out how many antennas
  nd = 1
  it(nd) = din(6,1)
  do iv=1,nv
    ok = .false.
    do ii=1,nd
      if (din(6,iv).eq.it(ii)) then
        ok = .true.
      endif
    enddo
    if (.not.ok) then
      nd = nd+1
      it(nd) = din(6,iv)
    endif
  enddo
  nd = nd+1                    ! Add the first antenna
  !!Print *,'Antennas ',nd,it(1:10),(din(6,iv),iv=1,10)
  deallocate(it)
  write(chain,*) 'I-UV,  Found ',nd,' antennas '
  call gagout(chain(2:))
  nbas = nd*(nd-1)/2
  write(chain,*) 'I-UV,  Found ',nbas,' baselines'
  call gagout(chain(2:))
end subroutine analyze_uvtable
!
!<FF>
subroutine load_fields(hin,din,nt,nv,intimes,altimes,nvisi,ntime,namo,nf,error)
  use gildas_def
  use image_def
  use gkernel_interfaces
  !
  type (gildas) :: hin
  integer nv,nt,ntime,nf
  real din(nt,nv)
  real*8 intimes(nv)
  real*8 altimes(ntime)
  integer nvisi(ntime)
  character(len=*) :: namo
  logical error
  !
  type (gildas) :: hout
  real, allocatable :: dout(:,:,:)
  integer, allocatable :: ivisi(:)
  integer ier,nn
  integer iv,ifield
  character(len=256) :: name
  !
  ! Define the image header
  call gildas_null(hout, type= 'UVT')
  call gdf_copy_header(hin,hout,error)
  !
  ! Assume things are already time-baseline sorted
  ! So, just scan and put at the appropriate times
  !
  allocate(ivisi(nf),dout(nt,nv,nf),stat=hout%status)
  if (gildas_error(hout,'SPLITFIELD',error)) return
  !
  ! Copy them to the output
  ivisi = 0
  ifield = 1
  ivisi(ifield) = 1
  dout(1:nt,ivisi(ifield),ifield) = din(1:nt,1)
  do iv=2,nv
     if (intimes(iv).eq.intimes(iv-1)) then
        ivisi(ifield) = ivisi(ifield)+1
        dout(1:nt,ivisi(ifield),ifield) = din(1:nt,iv)
     else
        ifield = mod(ifield,nf)+1
        ivisi(ifield) = ivisi(ifield)+1
        dout(1:nt,ivisi(ifield),ifield) = din(1:nt,iv)
     endif
  enddo
  !
  ! Write out the NF first fields
  do ifield=1,nf
    hout%gil%dim(2) = ivisi(ifield)
    hout%gil%nvisi  = ivisi(ifield)
    nn = lenc(namo)
    write(name,'(A,A,I12)') namo(1:nn),'-',ifield
    nn = lenc(name)
    call sic_black(name,nn)
    call sic_parsef(name,hout%file,' ','.uvt')
    call gdf_write_image(hout,dout(:,:,ifield),error)
    if (gildas_error(hout,'SPLITFIELD',error)) return
  enddo
  deallocate (dout)
end subroutine load_fields
