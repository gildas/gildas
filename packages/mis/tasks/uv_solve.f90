module spline_def
  integer, parameter :: mnant=64
  integer, parameter :: mnbas=(mnant-1)*mnant/2
  !
  integer, save :: r_iant(mnbas)
  integer, save :: r_jant(mnbas)
  !
  ! Fitted Splines
  real*8, save :: t_step       ! time step for splines
  integer, parameter :: m_spl=20
  integer, save :: f_spline (2,mnant)
  ! (amp/pha, antenna/baseline)
  integer, save :: n_knots(2,mnant)    ! number of interior knots
  real*8, save :: c_spline(m_spl+4,2,mnant)    ! coefficients
  real*8, save :: k_spline(m_spl+8,2,mnant)    ! knots
  real*4, save :: rms_spline_ant(2,mnbas)
  real*4, save :: rms_spline_bas(2,mnbas)
  !
  ! Fitted polynomials
  integer, parameter :: m_pol=m_spl+7
  integer, save :: f_pol(2,mnant)  ! 0, 1 (int), 2 (ext)
  integer, save :: n_pol(2,mnant)  ! polyn degree +1
  real*8, save :: c_pol(m_pol,2,mnant) ! coefficients
  real*8, save :: t_pol(2,2,mnant) ! time range limits
  real*4, save :: rms_pol_ant(2,mnbas)
  integer, save :: t_ipol
  !
  integer, parameter :: xy_ampli=1
  integer, parameter :: xy_phase=2
  !
end module spline_def
!<FF>
program uv_solve
  use image_def
  use gkernel_interfaces
  use spline_def
  character(len=16) :: rname='UV_SOLVE'
  !
  ! Solve the calibration using two UV tables, a "calibrator" one and a
  ! "source" one. Produce a calibrated source UV table.
  !
  character*256 nami,namo,namr,namc
  logical error
  logical solve_amp, solve_phase
  logical apply_amp, apply_phase
  integer ipol
  real rpol
  logical poly, plot
  !
  call gildas_open
  !
  ! Get the "Calibrator" UV Table, i.e. the noisy Baseline-based Gain Table
  call gildas_char('CALIBRATOR$',nami)
  !
  ! Open a temporary file for the solved baseline-based gain table
  call gildas_char('RESULT$',namo)
  !
  ! Get the raw "source" UV Table
  call gildas_char('RAW$',namr)
  !
  ! Open the calibrated source UV Table
  call gildas_char('CAL$',namc)
  ! Check solve option
  call gildas_logi('SOLVE_AMP$',solve_amp,1)
  call gildas_logi('SOLVE_PHASE$',solve_phase,1)
  ! Check apply option
  call gildas_logi('APPLY_AMP$',apply_amp,1)
  call gildas_logi('APPLY_PHASE$',apply_phase,1)
  ! Choice of interpolation function
  call gildas_inte('FUNCTION$',ipol,1)
  call gildas_real('ORDER$',rpol,1)
  !
  if (ipol.eq.1) then          ! Spline
    t_step = rpol/24.0
  elseif  (ipol.eq.2) then     ! Polynom
    t_ipol = nint(rpol)
    t_ipol = max(t_ipol,1)
  elseif (ipol.eq.3) then
    call gagout('W-'//rname//',  Boxcar smoothing')
    call gagout('        Not yet implemented')
    call sysexi(fatale)
  else
    call gagout('E-'//rname//',  Invalid FUNCTION$ ')
    call sysexi(fatale)
  endif
  call gildas_close
  call sub_solve (nami,namo,namr,namc,   &
     &    solve_amp,solve_phase,apply_amp,apply_phase,   &
     &    ipol,plot,error)
  if (error) call sysexi(fatale)
  call gagout('I-'//rname//',  Successful completion')
end program uv_solve
!<FF>
subroutine sub_solve(nami,namo,namr,namc,   &
     &    solve_amp,solve_phase,apply_amp,apply_phase,   &
     &    ipol,plot,error)
  use spline_def
  use image_def
  use gkernel_interfaces
  !
  character*(*) nami,namo,namr,namc
  logical error
  logical solve_amp, solve_phase
  logical apply_amp, apply_phase
  integer ipol
  logical plot
  !
  type (gildas) :: hin,hout,hraw
  real, allocatable, target :: dout(:,:,:)
  real, allocatable :: din(:,:), dsol(:,:)
  integer nt,nv,nbas,ntime,ier,nant, k,ia,ja
  real*8, allocatable :: altimes(:), intimes(:)
  real, pointer :: data_x(:,:), data_w(:,:), data_r(:,:),   &
     &    data_i(:,:)
  real, allocatable :: data_y(:,:)
  character*60 chain
  character*12 rname
  real cpu0,cpu1
  !
  if (ipol.eq.2) then
    print *,'Using polynom ',t_ipol
  elseif  (ipol.eq.1) then
    print *,'Using Spline ',t_step
  endif
  rname = 'UV_SOLVE'
  call gag_cpu(cpu0)
  !
  ! Read Header of a sorted, UV table
  call sic_parsef (nami,hin%file,' ','.uvt')
  call gdf_read_header (hin,error)
  if (gildas_error(hin,rname,error)) return
  allocate (din(hin%gil%dim(1),hin%gil%dim(2)),stat=ier)
  call gdf_read_data (hin,din,error)
  if (gildas_error(hin,rname,error)) return
  if (error) return
  nt = hin%gil%dim(1)
  nv = hin%gil%dim(2)
  !
  ! Define the problem size
  allocate (intimes(nv),altimes(nv),stat=ier)
  call analyze_uvtable(din,nt,nv,intimes,altimes,nbas,ntime)
  !
  ! Load the sorted data
  hout%gil%ndim = 3
  hout%gil%dim(1) = ntime
  hout%gil%dim(2) = nbas
  hout%gil%dim(3) = 4          ! Real, Imag, Weight, Time
  allocate(dout(ntime,nbas,4),stat=ier)
  !
  call load_uvtable (din,nt,nv,intimes,altimes,nbas,ntime,dout)
  deallocate (din)
  deallocate (intimes,altimes)
  call gag_cpu(cpu1)
  print *,'Data loaded at CPU ',cpu1-cpu0
  !
  !      CALL GDF_WRITE_IMAGE(HOUT,DOUT,ERROR)
  !      if (gildas_error(hout,'TIMEBASE',error)) call sysexi(fatale)
  !
  nant = nint(sqrt(8.0*nbas+1.0)+1)/2
  !
  ! Define the baseline ordering
  k = 0
  do ia=1,nant-1
    do ja=ia+1,nant
      k = k+1
      r_iant(k) = ia
      r_jant(k) = ja
    enddo
  enddo
  !
  allocate(data_y(ntime,nbas))
  !
  data_r => dout(:,:,1)
  data_i => dout(:,:,2)
  data_x => dout(:,:,4)
  data_w => dout(:,:,3)
  if (solve_amp) then
    data_y = sqrt(data_r**2 + data_i**2)
    call solve_cal (ntime,nbas,nant,data_x,   &
     &      data_y, data_w, .true., .false., ipol, error)
    if (error) return
    call gag_cpu(cpu1)
    print *,'Amplitude solution found at ',cpu1-cpu0
  endif
  !
  if (solve_phase) then
    data_y = atan2(data_i,data_r)
    call solve_cal (ntime,nbas,nant,data_x,   &
     &      data_y, data_w, .false., .true., ipol, error)
    if (error) return
    call gag_cpu(cpu1)
    print *,'Phase solution found at ',cpu1-cpu0
  endif
  deallocate (dout)
  !
  ! Compute the phase correction and/or apply it
  !
  ! Read Header of the sorted UV table to be calibrated
  call sic_parsef (namr,hin%file,' ','.uvt')
  call gdf_read_header (hin,error)
  if (gildas_error(hin,rname,error)) return
  allocate (din(hin%gil%dim(1),hin%gil%dim(2)),stat=ier)
  call gdf_read_data (hin,din,error)
  if (gildas_error(hin,rname,error)) return
  !
  ! Define the problem size
  nt = hin%gil%dim(1)
  nv = hin%gil%dim(2)
  !
  ! Prepare the time values
  allocate (intimes(nv),altimes(nv),stat=ier)
  call analyze_uvtable(din,nt,nv,intimes,altimes,nbas,ntime)
  allocate (dsol(nt,nv),stat=ier)
  dsol = din
  !
  ! Check NBAS vs NANT ?
  call apply_cal(nant,nv,nt,dsol,   &
     &    apply_amp, apply_phase, intimes, .true., ipol, error)
  if (error) return
  !
  ! Define the solution image header
  call gdf_copy_header (hin,hout,error)
  call sic_parsef (namo,hout%file,' ','.uvt')
  call gdf_write_image(hout,dsol,error)
  if (gildas_error(hout,rname,error)) return
  call gag_cpu(cpu1)
  print *,'Solution written at ',cpu1-cpu0
  !
  ! Write the new output image
  call apply_cal(nant,nv,nt,din,   &
     &    apply_amp, apply_phase, intimes, .false., ipol, error)
  if (error) return
  call gdf_copy_header (hin,hout,error)
  call sic_parsef (namc,hout%file,' ','.uvt')
  call gdf_write_image(hout,din,error)
  if (gildas_error(hout,rname,error)) return
  call gag_cpu(cpu1)
  print *,'Data calibrated at ',cpu1-cpu0
  !
  call gagout('I-'//rname//',  Successful completion')
  return
end subroutine sub_solve
!<FF>
subroutine solve_cal(nd,n_base,n_ant,data_x,data_y,data_w,   &
     &    solve_amp, solve_phase, ipol, error)
  !----------------------------------------------------------------------
  ! Solve Calibration
  !     INTEGER ND         Number of data
  !     INTEGER N_BASE     Number of baselines
  !     INTEGER N_ANT      Number of antennas
  !     REAL DATA_X(ND,N_BASE)   X coordinates (time)
  !     REAL DATA_Y(ND,N_BASE)   Y coordinates (amp or phase)
  !     REAL DATA_W(ND,N_BASE)   Weight
  !     LOGICAL SOLVE_AMP        Solve Amplitude
  !     LOGICAL SOLVE_PHASE      Solve Phase
  !     INTEGER IPOL             Polynom / Spline indicator
  !     LOGICAL ERROR
  !----------------------------------------------------------------------
  !
  use spline_def
  !
  integer nd                   ! Number of data
  integer n_base               ! Number of baselines
  integer n_ant                ! Number of antennas
  real data_x(nd,n_base)       ! X coordinates (time)
  real data_y(nd,n_base)       ! Y coordinates (amp or phase)
  real data_w(nd,n_base)       ! Weight
  logical solve_amp            ! Solve Amplitude
  logical solve_phase          ! Solve Phase
  integer ipol                 ! Polynom / Spline indicator
  logical error
  !----------------------------------------------------------------------
  !
  real*8, allocatable :: x_cal(:), y_cal(:,:), w_cal(:,:)
  real*8, allocatable :: work1(:,:,:)
  integer, allocatable :: work0(:)
  !
  integer nkn
  real tkn(10)
  logical weight
  !
  weight = .false.
  !
  ! add user fixed knots when needed
  nkn = 0
  !      IF (N_BR.GT.0) THEN
  !         DO I=1, N_BR
  !            DO J=1,4-K_BR(I)
  !               NKN = NKN + 1
  !               TKN(NKN) = T_BR(I)
  !            ENDDO
  !         ENDDO
  !      ENDIF
  !
  ! Get storage:
  allocate(x_cal(nd))
  allocate(y_cal(nd,n_base))
  allocate(w_cal(nd,n_base))
  allocate(work1(nd,n_base,4))
  allocate(work0(nd))
  !
  ! Do amplitudes first, then phase
  if (solve_amp) then
    call sub_solve_cal (xy_ampli,nd,n_base,n_ant,   &
     &      x_cal,y_cal,w_cal,   &
     &      work0, work1,   &
     &      nd,n_base,   &
     &      data_x,data_y,data_w,   &
     &      nkn,tkn,weight,ipol,error)
    if (error) return
  endif
  if (solve_phase) then
    call sub_solve_cal (xy_phase,nd,n_base,n_ant,   &
     &      x_cal,y_cal,w_cal,   &
     &      work0, work1,   &
     &      nd,n_base,   &
     &      data_x,data_y,data_w,   &
     &      nkn,tkn,weight,ipol,error)
    if (error) return
  endif
  deallocate (x_cal,y_cal,w_cal,work1,work0)
  error = .false.
end
!<FF>
subroutine apply_cal(nant,nv,mv,visi,   &
     &    apply_amp, apply_phase, times, plot, ipol, error)
  !
  ! Apply calibration
  !
  !      INTEGER NANT                       ! Number of antennas
  !      INTEGER MV                         ! Size of visibility
  !      INTEGER NV                         ! Number of visibilities
  !      REAL VISI(MV,NV)                   ! Visibilities
  !      LOGICAL APPLY_AMP                  ! Apply amplitude
  !      LOGICAL APPLY_PHASE                ! Apply phase
  !      REAL*8 TIMES(NV)                   ! Observing time stamps
  !      LOGICAL PLOT                       ! Apply or compute correction
  !      INTEGER IPOL                       ! Polynom (2) / Spline (1)
  !      LOGICAL ERROR
  !
  use spline_def
  !
  integer nant                 ! Number of antennas
  integer mv                   ! Size of visibility
  integer nv                   ! Number of visibilities
  real visi(mv,nv)             ! Visibilities
  logical apply_amp            ! Apply amplitude
  logical apply_phase          ! Apply phase
  real*8 times(nv)             ! Observing time stamps
  logical plot                 ! Apply or compute correction
  integer ipol                 ! Polynom (2) / Spline (1)
  logical error
  !
  ! Apply or Get Amplitude & Phase correction
  if (apply_amp) then
    if (ipol.eq.2) then
      call sub_apply_pol (nant,xy_ampli,nv,mv,visi,times,   &
     &        plot,error)
    elseif  (ipol.eq.1) then
      call sub_apply_spl (nant,xy_ampli,nv,mv,visi,times,   &
     &        plot,error)
    endif
  endif
  if (apply_phase) then
    if (ipol.eq.2) then
      call sub_apply_pol (nant,xy_phase,nv,mv,visi,times,   &
     &        plot,error)
    elseif  (ipol.eq.1) then
      call sub_apply_spl (nant,xy_phase,nv,mv,visi,times,   &
     &        plot,error)
    endif
  endif
  !
end
!<FF>
subroutine sub_solve_cal (ly,nd,nbas,nant,xxx,yyy,www,   &
     &    ind,wk1,md,mb,x_data,y_data,w_data,   &
     &    nkn,tkn,weight,ipol,error)
  !
  use gkernel_interfaces
  use spline_def
  ! Dummy variables:
  integer ly                   ! Type of solution (Amp/Phase)
  integer nd                   ! Number of data points
  integer nbas                 ! Number of baselines
  integer nant                 ! Number of antennas
  integer md                   ! Size of data array (data points)
  integer mb                   ! Size of data array (baselines)
  integer nkn                  ! Number of pre-set knots
  integer ind(nd)              ! Sorting index if needed
  integer ipol                 ! Spline / Polynom / Boxcar
  real x_data(md,mb)
  real y_data(md,mb)
  real w_data(md,mb)
  real tkn(nkn)                ! Preset X knots
  real*8 xxx(nd)               ! X coordinate (time)
  real*8 yyy(nd,nbas)          ! Values
  real*8 www(nd,nbas)          ! Weights
  real*8 wk1(nd,nbas,4)        ! Work area
  logical weight, error
  ! Global variables:
  include 'gbl_pi.inc'
  logical gr8_random
  ! Local variables:
  integer ib, iy, iba, i, ikn, nt, ifail, nk, isb, ia, nn
  integer ja, l, l1, l2, l3, ir
  real*8 cs((m_spl+4)*mnant)
  real*8 ks(m_spl+8)
  real*8 sss, x1, xx1
  real*8 wk2((m_spl+8)**2*mnant*mnant)
  real*8 ss(mnbas), wss(mnbas)
  real*8 wk3((m_spl+8)*mnant)
  real*8 w0, xold, xnew, x2, x3, x4, step, y2, y3
  real rmss
  character chain*256, cunit*20
  integer ref_ant
  !------------------------------------------------------------------------
  ! Code:
  ref_ant = 1
  !
  ! Define the default weight
  if (weight) then
    w0 = 0.0
    ib = 1
    i = 1
    do while (w0.eq.0.0)
      if (w_data(i,ib).gt.0) then
        w0 = w_data(i,ib)
      else
        i = i+1
        if (i.eq.nd) then
          ib = ib+1
          i = 1
          if (ib.gt.nbas) then
            w0 = -1.0
          endif
        endif
      endif
    enddo
    if (w0.lt.0) then
      call gagout('E-SOLVE_CAL,  No valid data')
      error = .true.
      return
    endif
  else
    w0 = 1.0
  endif
  !
  ! Now load the weights per baseline and data point
  do ib = 1, nbas
    iba = ib
    !
    ! Load arrays for calibration
    do i=1, nd
      xxx(i) = x_data(i,1)
      ! fit logarithm of amplitudes for antenna-based amplitudes
      if (ly.eq.1) then
        yyy(i,iba) = log(dble(y_data(i,ib)))
      elseif (ly.eq.2) then
        yyy(i,iba) = y_data(i,ib)
      endif
      if (weight .and. w_data(i,ib).gt.0) then
        www(i,iba) = w_data(i,ib)
      elseif  (ly.eq.1) then
        www(i,iba) = w0*y_data(i,ib)**2
      else
        www(i,iba) = w0
      endif
    enddo
  enddo
  !
  ! Load the knot coordinates
  if (ipol.eq.1 .and. nd.eq.3) then
    x2 = xxx(2)-xxx(1)
    x3 = xxx(3)-xxx(1)
    xxx(4) = (xxx(3)+xxx(2))/2
    x4 = xxx(4)-xxx(1)
    do iba=1,nbas
      y2 = yyy(2,iba)-yyy(1,iba)
      y3 = yyy(3,iba)-yyy(1,iba)
      yyy(4,iba) = yyy(1,iba) + x4**2*(y2/x2-y3/x3)/(x2-x3)   &
     &        + x4*(y3*x2/x3-y2*x3/x2)/(x2-x3)
      www(4,iba) = (www(3,iba)+www(2,iba))/2
    enddo
    nd = 4
  elseif (ipol.eq.1 .and. nd.eq.2) then
    x2 = xxx(2)-xxx(1)
    xxx(3) = xxx(2)-x2/3
    xxx(4) = xxx(1)+x2/3
    do iba=1,nbas
      y2 = yyy(2,iba)-yyy(1,iba)
      yyy(3,iba) = yyy(2,iba)-y2/3
      yyy(4,iba) = yyy(1,iba)+y2/3
      www(3,iba) = www(2,iba)
      www(4,iba) = www(1,iba)
    enddo
    nd = 4
  elseif (nd.eq.1) then
    xxx(2) = xxx(1)+0.01
    xxx(3) = xxx(1)+0.02
    xxx(4) = xxx(1)-0.01
    do iba=1,nbas
      yyy(2,iba) = yyy(1,iba)
      yyy(3,iba) = yyy(1,iba)
      yyy(4,iba) = yyy(1,iba)
      www(2,iba) = www(1,iba)
      www(3,iba) = www(1,iba)
      www(4,iba) = www(1,iba)
    enddo
    nd = 4
  endif
  sss = 0
  do i=1, nd
    sss = sss + www(i,iba)**2
  enddo
  !
  ! Sort if required
  if (gr8_random(xxx,nd)) then
    call gagout('W-CALIBRATE,  Data not sorted')
    call gr8_trie(xxx,ind,nd,error)
    if (error) goto 999
    do iba=1,nbas
      call gr8_sort(yyy(1,iba),wk1,ind,nd)
      call gr8_sort(www(1,iba),wk1,ind,nd)
    enddo
  endif
  !
  ! Set knots, and sort them.
  if (ipol.eq.1) then
    !
    xold = xxx(1)
    ikn = 0
    nk = 0
    do while (xold.lt.xxx(nd))
      if (ikn.eq.nkn) then
        xnew = xxx(nd)
      elseif (nk.lt.m_spl) then
        ikn = ikn + 1
        xnew = tkn(ikn)
        nk = nk+1
        ks(nk+4) = tkn(ikn)
      else
        call gagout('E-SOLVE_CAL,  Too many knots for this spline')
        error = .true.
        return
      endif
      nt = (xnew-xold)/t_step
      nt = min(nt, m_spl-nk, nd-4)
      if (nt.gt.0) then
        step = (xnew-xold)/(nt+1)
        if (nk+nt.gt.m_spl) then
          call gagout('E-SOLVE_CAL,  Too many knots for this spline')
          error = .true.
          return
        endif
        do i=1, nt
          ks(nk+i+4) = xold + i*step
        enddo
        nk = nk + nt
      endif
      xold = xnew
    enddo
    if (nk.gt.0) then
      call  gr8_trie(ks(5:),ind,nk,error)
      if (error) goto 999
    endif
    !
    ! Solve for Splines
    ifail = 1
    call splinant(ly, nd, nbas, r_iant, r_jant, ref_ant,   &
     &      nk+8, nant, xxx, yyy, www, ks, wk1, wk2, wk3, ss, cs, error)
    if (error) goto 999
    !
    ! Move Knots into saved array
    do ia = 1, nant
      n_knots(ly,ia) = nk
      do i=1, nk+8
        k_spline(i,ly,ia) = ks(i)  ! in hours
      enddo
      do i=1, nk+4
        c_spline(i,ly,ia) = cs(ia+nant*(i-1))
      enddo
      f_spline(ly,ia) = 1
      f_pol(ly,ia) = 0
    enddo
    do iba=1, nbas
      rms_spline_ant(ly,iba) = ss(iba)
    enddo
    !
    ! Polynom
  elseif  (ipol.eq.2) then
    t_ipol = min(t_ipol, nd)
    call  polyant(ly, nd, nbas, r_iant, r_jant, ref_ant,   &
     &      t_ipol, nant, xxx, yyy, www,   &
     &      wk1, wk2, wk3, ss, wss, cs, error)
    if (error) goto 999
    !
    ! Save Exponents
    do ia = 1, nant
      n_pol(ly,ia) = t_ipol
      t_pol(1,ly,ia) = xxx(1)
      t_pol(2,ly,ia) = xxx(nd)
      do i=1, t_ipol
        c_pol(i,ly,ia) = cs(ia+nant*(i-1))
      enddo
      f_pol(ly,ia) = 1
      f_spline(ly,ia) = 0
    enddo
    do iba=1, nbas
      rms_pol_ant(ly,iba) = ss(iba)
    enddo
  endif
  !
  ! Give rms
  do ib = 1,min(nbas,10)
    if (ly.eq.2) then
      rmss = ss(ib)*180./pi
      cunit = 'Deg.'
    elseif (ly.eq.1) then
      rmss = ss(ib)*100.
      cunit = '%'
    endif
    write(chain,1000) ib, rmss, cunit
    call gagout(chain)
  enddo
  !
  rmss = 0.0
  do iba=1, nbas
    rmss = rmss + ss(iba)
  enddo
  rmss = rmss/nbas
  if (ly.eq.2) then
    rmss = rmss*180./pi
    cunit = 'Deg.'
  elseif (ly.eq.1) then
    rmss = rmss*100.
    cunit = '%'
  endif
  write(chain,1001) rmss, cunit
  call gagout(chain)
  !
  1000  format('Baseline ',i4,' rms: ',f9.2,1x,a)
  1001  format('Mean  rms: ',f9.2,1x,a)
  return
  999   return
end
!<FF>
subroutine sub_apply_pol(nant,ly,nv,mv,visi,time,plot,error)
  !
  use spline_def
  !
  integer nant                 ! Number of antennas
  integer ly                   ! LY = 1 means amplitude, else phase
  integer nv,mv                ! Size of visibilities
  real visi(mv,nv)             ! Input/Output visibilities
  real*8 time(nv)              ! Time stamp values
  logical plot                 ! .TRUE. means compute solution, else apply
  logical error
  !
  integer i,iv
  integer nk
  real*8 cs((m_spl+4),mnant)
  real*8 old_time
  integer ipol
  real*8 xx1, yy1, x2, x3
  complex cvis, cphi
  real phi
  real*8 yant(mnant)
  !
  integer ifail, ia, ja
  real amp
  !
  old_time = -1.0
  !
  ! Compute the solution OR Remove Solution from the data
  do ia=1, nant
    ipol = n_pol(ly,ia)
    do i=1, ipol
      cs(i,ia) = c_pol(i,ly,ia)
    enddo
  enddo
  !
  do iv=1,nv
    if (time(iv).ne.old_time) then
      do ia=1,nant
        ifail = 1
        x2 = t_pol(1,ly,ia)
        x3 = t_pol(2,ly,ia)
        xx1 = min(max(time(iv),x2),x3)
        xx1 = ((xx1-x2)-(x3-xx1))/(x3-x2)
        call mth_getpol('SOLVE_CAL',   &
     &          ipol, cs(1,ia), xx1, yy1, error)
        if (error) goto 999
        yant(ia) = yy1
      enddo
      old_time = time(iv)
    endif
    ia = visi(6,iv)
    ja = visi(7,iv)
    if (ly.eq.1) then
      amp = exp(yant(ja)+yant(ia))
      if (plot) then
        visi(8,iv) = 1.0/amp
        visi(9,iv) = 0.0
      else
        visi(8,iv) = visi(8,iv)/amp
        visi(9,iv) = visi(9,iv)/amp
      endif
    else
      phi = yant(ja)-yant(ia)
      if (plot) then
        visi(8,iv) = cos(phi)
        visi(9,iv) = sin(phi)
      else
        cvis = cmplx(visi(8,iv),visi(9,iv))
        cphi = cmplx(cos(phi),sin(phi))
        cvis = cvis / cphi
        visi(8,iv) =  real(cvis)
        visi(9,iv) = aimag(cvis)
      endif
    endif
  enddo
  999   return
end
!<FF>
subroutine sub_apply_spl(nant,ly,nv,mv,visi,time,plot,error)
  !
  use spline_def
  !
  integer nant                 ! Number of antennas
  integer ly                   ! LY = 1 means amplitude, else phase
  integer nv,mv                ! Size of visibilities
  real visi(mv,nv)             ! Input/Output visibilities
  real*8 time(nv)              ! Time stamp values
  logical plot                 ! .TRUE. means compute solution, else apply
  logical error
  !
  integer nk,i,iv
  real*8 cs(m_spl+4,mnant)
  real*8 ks(m_spl+8,mnant)
  real*8 xx1, yy1
  complex cvis, cphi
  real phi
  real*8 yant(mnant)
  real*8 old_time
  integer ifail, ia, ja
  real amp
  !
  old_time = -1.0
  !
  ! Compute the solution AND/OR
  !
  ! Remove Solution from the data
  do ia=1, nant
    nk = n_knots(ly,ia)
    do i=1, nk+8
      ks(i,ia) = k_spline(i,ly,ia)
    enddo
    do i=1, nk+4
      cs(i,ia) = c_spline(i,ly,ia)
    enddo
  enddo
  !
  ! Loop on visibilities
  do iv=1,nv
    if (time(iv).ne.old_time) then
      do ia=1,nant
        ifail = 1
        nk = n_knots(ly,ia)
        xx1 = min(time(iv),ks(nk+5,ia))
        xx1 = max(xx1,ks(4,ia))
        call mth_getspl('SOLVE_CAL',   &
     &          nk+8, ks(1,ia), cs(1,ia), xx1, yy1, error)
        if (error) goto 999
        yant(ia) = yy1
      enddo
      old_time = time(iv)
    endif
    ia = visi(6,iv)
    ja = visi(7,iv)
    if (ly.eq.1) then
      amp = exp(yant(ja)+yant(ia))
      if (plot) then
        visi(8,iv) = 1.0/amp
        visi(9,iv) = 0.0
      else
        visi(8,iv) = visi(8,iv)/amp
        visi(9,iv) = visi(9,iv)/amp
      endif
    else
      phi = yant(ja)-yant(ia)
      if (plot) then
        visi(8,iv) = cos(phi)
        visi(9,iv) = sin(phi)
      else
        cvis = cmplx(visi(8,iv),visi(9,iv))
        cphi = cmplx(cos(phi),sin(phi))
        cvis = cvis / cphi
        visi(8,iv) =  real(cvis)
        visi(9,iv) = aimag(cvis)
      endif
    endif
  enddo
  999   return
end
!<FF>
subroutine analyze_uvtable(din,nt,nv,intimes,altimes,nbas,ntime)
  use gkernel_interfaces
  integer nv                   ! Number of visibilities
  integer nt                   ! Visibility size
  integer nbas                 ! Number of baselines
  integer ntime                ! Actual number of time stamps
  real din(nt,nv)              ! Visibilities
  real*8 intimes(nv)           ! Time stamps
  real*8 altimes(nv)           ! Different time stamps
  logical error
  !
  integer, allocatable :: it(:)
  integer nd,iv,ii
  logical ok
  character*60 chain
  !
  error = .false.
  !
  ! Scan how many dates
  nd = 1
  altimes(nd) = din(4,1)
  do iv=1,nv
    intimes(iv) = 0
    do ii=1,nd
      if (din(4,iv).eq.altimes(ii)) then
        intimes(iv) = ii
      endif
    enddo
    if (intimes(iv).eq.0) then
      nd = nd+1
      altimes(nd) = din(4,iv)
      intimes(iv) = nd
    endif
  enddo
  write(chain,*) 'I-UV,  Found ',nd,' dates '
  call gagout(chain(2:))
  !
  ! Time compacted value. Times are in seconds.
  do iv=1,nv
    if (din(5,iv).gt.86400.0) then
      call gagout('W-UV, Ooops time > 1 day')
    endif
    intimes(iv) = intimes(iv)-1.d0+din(5,iv)/86400d0
  enddo
  !
  ! Find out how many times...
  altimes = 0.0
  altimes(1) = intimes(1)
  ntime = 1
  do iv=2,nv
    ok = .false.
    ii = 1
    do while (.not.ok.and.ii.le.ntime)
      if (intimes(iv).eq.altimes(ii)) then
        ok = .true.
      else
        ii = ii+1
      endif
    enddo
    if (.not.ok) then
      ntime = ntime+1
      altimes(ntime) = intimes(iv)
    endif
  enddo
  !
  write(chain,*) 'I-UV,  Found ',ntime,' times '
  call gagout(chain(2:))
  !
  ! Pending nightmare: are the times sorted ?
  ! Answer: of course not, let us do it now...
  allocate(it(nv))
  do ii=1,ntime
    it(ii) = ii
  enddo
  call gr8_trie(altimes,it,ntime,error)
  !
  ! Find out how many antennas
  nd = 1
  it(nd) = din(6,1)
  do iv=1,nv
    ok = .false.
    do ii=1,nd
      if (din(6,iv).eq.it(ii)) then
        ok = .true.
      endif
    enddo
    if (.not.ok) then
      nd = nd+1
      it(nd) = din(6,iv)
    endif
  enddo
  nd = nd+1                    ! Add the first antenna
  deallocate(it)
  write(chain,*) 'I-UV,  Found ',nd,' antennas '
  call gagout(chain(2:))
  nbas = nd*(nd-1)/2
  write(chain,*) 'I-UV,  Found ',nbas,' baselines'
  call gagout(chain(2:))
end
!
!<FF>
subroutine load_uvtable (din,nt,nv,intimes,altimes,nbas,   &
     &    ntime,dout)
  integer nv,nt,nbas,ntime
  real din(nt,nv)
  real*8 intimes(nv)
  real*8 altimes(ntime)
  real dout(ntime,nbas,4)
  !
  integer iv,itime,ibas
  !
  ! Assume things are already time-baseline sorted
  ! So, just scan and put at the appropriate times
  !
  dout = 0.0
  !
  itime = 1
  ibas = 1
  do iv=1,nv
    !
    ! Check next time match
    do while (altimes(itime).ne.intimes(iv))
      if (itime.eq.ntime) then
        ibas = ibas+1
        itime = 0
      endif
      itime = itime+1
    enddo
    !
    ! Put in place
    dout(itime,ibas,1) = din(8,iv) ! Real
    dout(itime,ibas,2) = din(9,iv) ! Imag
    dout(itime,ibas,3) = din(10,iv)    ! Weight
    dout(itime,ibas,4) = altimes(itime)    ! Time
  enddo
end
!
!<FF>
function gr8_random (x,n)
  logical gr8_random
  integer i,n
  real*8 x(n)
  !
  do i=1,n-1
    if (x(i).gt.x(i+1)) then
      gr8_random = .true.
      return
    endif
  enddo
  gr8_random = .false.
end
!
subroutine nag_fail (fac,prog,ifail,error)
  character*(*) fac,prog
  logical error
  integer ifail
  character*60 chain
  !
  if (ifail.eq.0) return
  write(chain,'(A,A,A,I4)') 'ERROR in ',prog,', ifail = ',ifail
  call gagout('F-'//fac//',  '//chain)
  error = .true.
  return
end
