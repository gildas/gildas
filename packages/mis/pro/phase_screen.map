if (.not.exist(phase_screen_init)) then
   !
   define logical phase_screen_init /global
   !
   define character  long_screen*128  long_averag*128 /global
   define character short_screen*128 short_averag*128 /global
   define double pix_size l_screen_size[2] s_screen_size[2] /global
   define double sf_value_300m exponents[3] base_ranges[2] /global
   define double diameter /global
   define logical sf_verification /global
   !
   let long_screen   "long_screen"      !
   let long_averag   "long_screen_averaged" !
   !
   let short_screen  "short_screen"     !
   let short_averag  "short_averag"     !
   !
   let pix_size          4.0            !
   let l_screen_size  5000.0  500.0     !
   let s_screen_size  4000.0 4000.0     !
   !
   let sf_value_300m    60.0            !
   let exponents        0.83  0.333 0.0 !
   let base_ranges     200.0 1000.0     !
   !
   let diameter           12
   !
   let sf_verification    yes
   !
   define double sf_baseline ps_kr /global
   define double sf_exponent ps_exponent /global
   define logical sf_own_power_law ps_own_power_law /global
   define integer display_screen display_sf /global
   !
   let display_screen 1
   let display_sf     1
   !
   let sf_exponent 'exponents[1]'
   let ps_exponent '-(exponents[3]+1.0)'
   let sf_baseline 0.0
   let ps_kr       0.0
   !
   let sf_own_power_law no
   let ps_own_power_law no
   !
   input
   dev i w
   !
endif
!
gui\panel "Building of an atmospheric-like phase screen" gag_log:phase_screen.hlp
!
sic\let long_screen   'long_screen'   /prompt "File name of raw phase screen" /file *
sic\let long_averag   'long_averag'   /prompt "File name of averaged  screen" /file *
!
gui\button "run phase_screen gag_scratch:phase_screen.init /nowin" "COMPUTE" "Phase Screen" gag_log:screen_computation.hlp "Parameters"
sic\say "Length scales of the phase screen"
sic\let pix_size 'pix_size' /prompt "Pixel size (m)"
sic\let l_screen_size 'l_screen_size[1]' 'l_screen_size[2]' /prompt "Physical size (m)"
sic\let s_screen_size 's_screen_size[1]' 's_screen_size[2]' /prompt "Square subpart size (m)"
sic\say
sic\say "Definition of the SQRT(2nd order structure function of the phase)"
sic\let sf_value_300m 'sf_value_300m' /prompt "Value at 300m (degree)"
sic\let base_ranges 'base_ranges[1]' 'base_ranges[2]' /prompt "Baseline ranges (m)"
sic\let exponents 'exponents[1]' 'exponents[2]' 'exponents[3]' /prompt "Exponents"
sic\say
sic\say "Phase screen average over the antenna dish"
sic\let diameter 'diameter' /prompt "Antenna Diameter (m)"
sic\say
sic\let sf_verification 'sf_verification' /prompt "Compute the structure function of the output screen?"
!
gui\button "@ phase_screen_plotting" "VIEW" "Phase Screen" gag_log:screen_plotting.hlp "Parameters"
sic\let display_screen 'display_screen' /prompt "What to plot ?" -
/index "Raw result" "Averaged result" "XY gradient" "Intensity/Direction" "Raw subsquares" "Averaged subsquares"
!
gui\button "@ power_spectrum_plotting" "VIEW" "Power Spectrum" gag_log:power_spectrum_plotting.hlp "Parameters"
sic\let ps_own_power_law 'ps_own_power_law' /prompt "Define your own power law?"
sic\let ps_kr       'ps_kr'       /prompt "Spatial frequency (/m)"
sic\let ps_exponent 'ps_exponent' /prompt "Power law exponent"
 !
 gui\button "@ structure_function_plotting" "VIEW" "Structure Function" gag_log:structure_function_plotting.hlp "Parameters"
    sic\let display_sf 'display_sf' /prompt "Which structure function to plot ?" -
            /index "Output result" "Subsquares"
    sic\let sf_own_power_law 'sf_own_power_law' /prompt "Define your own power law?"
    sic\let sf_baseline 'sf_baseline' /prompt "Baseline (m)"
    sic\let sf_exponent 'sf_exponent' /prompt "Power law exponent"
 !
 on error "return"
 gui\go
 !
 if (phase_screen_init) then
    return
 endif
 !
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !
 begin procedure phase_screen_plotting
   !
   destroy all
   clear alpha
   set /default
   !
   if (display_screen.eq.1) then
      let name 'long_screen'
      let first 1
      let last 1
   else if (display_screen.eq.2) then
      let name 'long_averag'
      let first 1
      let last 1
   else if (display_screen.eq.3) then
      let name 'long_averag'
      @ gradient_plotting 1
      return
   else if (display_screen.eq.4) then
      let name 'long_averag'
      @ gradient_plotting 2
      return
 !!     let name 'long_gradient'
   else if (display_screen.eq.5) then
      let name 'short_screen'
      let first 0
      let last 0
   else if (display_screen.eq.6) then
      let name 'short_averag'
      let first 0
      let last 0
   else
      say "E-PHASE_SCREEN,  should not be here"
      return base
   endif
   !
   let type gdf
   let do_contour no
   go bit
   go wedge
   !
 end procedure phase_screen_plotting
 !
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !
 begin procedure power_spectrum_plotting
   !
   define image sqrt_power_spectrum sps.gdf read
   !
   define integer nx
   let nx 'sqrt_power_spectrum%dim[1]|2-1'
   !
   define real ps[nx,2] power[nx,2]
   define double max factor
   define integer ipos
   !
   let ps[1:nx,1] sqrt_power_spectrum[2:'nx+1',1]
   let ps[1:nx,2] sqrt_power_spectrum[2:'nx+1',2]
   compute max max ps[,2]
   let ps[,2] ps[,2]/max
   !
   let power[,1] ps[,1]
   let power[,2] power[,1]^ps_exponent
   !
   if (ps_own_power_law) then
      let ipos 1
      for /while (ps[ipos,1].le.ps_kr)
         let ipos ipos+1
      next
      let ipos ipos-1
   else
      let ipos 1
      let ps_kr 'ps[ipos,1]'
   endif
   let factor (ps_kr-ps[ipos,1])/(ps['ipos+1',1]-ps[ipos,1])
   let factor ps[ipos,2]+ps['ipos+1',2]*factor
   let factor factor/ps_kr^ps_exponent
   let power[,2] power[,2]*factor
   !
   destroy all
   clear alpha
   set /default
   !
   limits /var ps[,1] ps[,2] /xlog /ylog
   pen 1
   connect ps[,1] ps[,2]
   pen 2 
   connect power[,1] power[,2]
   pen 0 
   box
   !
   delete /var ps sqrt_power_spectrum nx
   !
   define image filter filter1.gdf read
   !
   define integer nx
   let nx 'filter%dim[1]|2-1'
   !
   define real ps[nx,2]
   !
   for j 1 to nx
      let ps[j,1] (j+1-filter%convert[1,1])*filter%convert[3,1]+filter%convert[2,1]
   next
   let ps[1:nx,2] filter[2:'nx+1',1]
   !
   pen 3
   connect ps[,1] ps[,2]
   pen 0
   !
   delete /var nx ps filter
   !
   define image filter filter2.gdf read
   !
   define integer nx
   let nx 'filter%dim[1]|2-1'
   !
   define real ps[nx,2]
   !
   for j 1 to nx
      let ps[j,1] (j+1-filter%convert[1,1])*filter%convert[3,1]+filter%convert[2,1]
   next
   let ps[1:nx,2] filter[2:'nx+1',1]
   !
   pen 4
   connect ps[,1] ps[,2]
   pen 0
   !
   delete /var nx ps filter
   !
 end procedure power_spectrum_plotting
 !
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !
 begin procedure structure_function_plotting
   !
   ! Plots input structure function.
   ! 
   define image sf_in in_sf.gdf read
   !
   define integer nx
   let nx 'sf_in%dim[1]-1'
   !
   define real sf1[nx,2]
   !
   let sf1[1:nx,1]  abs(sf_in[2:'nx+1',1])
   let sf1[1:nx,2] sqrt(sf_in[2:'nx+1',2])
   !
   destroy all
   clear alpha
   set /default
   !
   limits /var sf1[,1] sf1[,2] /xlog /ylog
   pen 1
   connect sf1[,1] sf1[,2]
   pen 0
   box
   !
   !
   !
   if (display_sf.eq.1) then
      !
      ! Plots structure function of the long raw phase screen.
      !
      define image sf_raw long_raw_sf.gdf read
      !
      let nx 'sf_raw%dim[2]'
      !
      define real sf2[nx,2] 
      !
      pen 3
      for i 1 to sf_raw%dim[1]
         !
         let sf2[,1]  abs(sf_raw[i,,1])
         let sf2[,2] sqrt(sf_raw[i,,2])
         !
         connect sf2[,1] sf2[,2]
         !
      next
      !
      ! Plots structure function of the long averaged phase screen.
      !
      define image sf_avg long_avg_sf.gdf read
      !
      let nx 'sf_avg%dim[2]'
      !
      define real sf3[nx,2]
      !
      pen 4
      for i 1 to sf_avg%dim[1]
         !
         let sf3[,1]  abs(sf_avg[i,,1])
         let sf3[,2] sqrt(sf_avg[i,,2])
         !
         connect sf3[,1] sf3[,2]
         !
      next
      !
   else
      !
      ! Plots structure function of the short raw phase screens.
      !
      define image sf_raw short_raw_sf.gdf read
      !
      let nx 'sf_raw%dim[2]'
      !
      define real sf2[nx,2] 
      !
      pen 3
      for i 1 to sf_raw%dim[1]
         !
         let sf2[,1]  abs(sf_raw[i,,1])
         let sf2[,2] sqrt(sf_raw[i,,2])
         !
         connect sf2[,1] sf2[,2]
         !
      next
      !
      ! Plots structure function of the short averaged phase screens.
      !
      define image sf_avg short_avg_sf.gdf read
      !
      let nx 'sf_avg%dim[2]'
      !
      define real sf3[nx,2]
      !
      pen 4
      for i 1 to sf_avg%dim[1]
         !
         let sf3[,1]  abs(sf_avg[i,,1])
         let sf3[,2] sqrt(sf_avg[i,,2])
         !
         connect sf3[,1] sf3[,2]
         !
      next
      !
   endif
   !
   ! Plots user defined power law.
   !
   define real power[nx,2]
   define integer ipos
   define real factor
   !
   let power[,1] sf2[,1]
   let power[,2] power[,1]^sf_exponent
   !
   if (sf_own_power_law) then 
      let ipos 1
      for /while (sf2[ipos,1].le.sf_baseline)
          let ipos ipos+1
      next
      let ipos ipos-1
   else
      let ipos 1
      let sf_baseline 'sf2[ipos,1]'
   endif
   let factor (sf_baseline-sf2[ipos,1])/(sf2['ipos+1',1]-sf2[ipos,1])
   let factor sf2[ipos,2]+sf2['ipos+1',2]*factor
   let factor factor/sf_baseline^sf_exponent
   let power[,2] power[,2]*factor
   !
   pen 2
   connect power[,1] power[,2]
   pen 0
   !
   delete /var sf1 sf2 sf3 sf_in sf_raw sf_avg power
   !
 end procedure structure_function_plotting
 !
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !
 begin procedure gradient_plotting
   define imag a 'name'.gdf read
   limits /rg a
   defin integer nx ny 
   defin real zmi zma sx sy ratio
   let nx a%dim[1] 
   let ny a%dim[2]
   !
   !
   ! Define box size
   let ratio abs((user_xmax-user_xmin)|(user_ymax-user_ymin)) 
   if (ratio.lt.26/10) then ! Y defines the size
      let sy 10
      let sx ratio*sy
   else ! X defines the size
      let sx 26
      let sy sx/ratio
   endif
   !
   if (&1.eq.2) then 
      define real b[nx,ny,2] 
      let b[1] sqrt(a[2]^2+a[3]^2)
      let b[2] 180*atan2(a[3],a[2])/pi
      !
      set box 2 2+sx 10.5-sy 10.5
      compute zma max b[1]
      plot b[1] /scal lin -zma/2 zma
      box n n n
      wedge 
      !
      set box 2 2+sx 10.5 10.5+sy  
      plot b[2] /scal lin -180 180
      box n n n
      wedge 
   else 
      compute zmi min a[2:3]
      compute zma max a[2:3]
      let zma max(-zmi,zma)
      !
      set box 2 2+sx 10.5-sy 10.5
      plot a[2] /scal lin -zma zma
      box n n n
      wedge
      !
      set box 2 2+sx 10.5 10.5+sy  
      plot a[3] /scal lin -zma zma
      box n n n
      wedge
   endif
   !
 end procedure gradient_plotting
 !
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !
begin data gag_scratch:phase_screen.init
   !
   ! Parameter file for PHASE_SCREEN
   !
   TASK\FILE "Name of the long output    phase screen"  LS_FILENAME$ 'long_screen'
   TASK\FILE "Name of the long averaged  phase screen"  LA_FILENAME$ 'long_averag'
   TASK\FILE "Name of the short output   phase screens" SS_FILENAME$ 'short_screen'
   TASK\FILE "Name of the short averaged phase screens" SA_FILENAME$ 'short_averag'
   TASK\REAL "Pixel  size (in m)"       PIX_SIZE$[2]      'pix_size' 'pix_size'
   TASK\REAL "Long  screen size (in m)" L_SCREEN_SIZE$[2] 'l_screen_size[1]' 'l_screen_size[2]'
   TASK\REAL "Short screen size (in m)" S_SCREEN_SIZE$[2] 's_screen_size[1]' 's_screen_size[2]'
   TASK\REAL "Structure function value at 300m (in degree)"          SF_VALUE_300M$ 'sf_value_300m'
   TASK\REAL "Structure function exponents"                          EXPONENTS$[3]  'exponents[1]' 'exponents[2]' 'exponents[3]'
   TASK\REAL "Ranges of baselines for the previous exponents (in m)" BASE_RANGES$[2] 'base_ranges[1]' 'base_ranges[2]'
   TASK\LOGI "Compute the power spectrum directly in Fourier space?" FOURIER_SPACE$  yes
   TASK\REAL "Antenna Diameter (in m)"  DIAM$ 'diameter'
   TASK\LOGI "Verify the structure function of the output screen?"   SF_VERIF$       'sf_verification'
   TASK\GO
   !
end data gag_scratch:phase_screen.init 
!
let phase_screen_init yes
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!



