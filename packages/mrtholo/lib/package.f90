!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtholo_pack_set(pack)
  use gpack_def
  use mrtholo_dependencies_interfaces
  use mrtholo_interfaces, except_this => mrtholo_pack_set
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(gpack_info_t), intent(out) :: pack
  !
  external :: clic_pack_set
  !
  pack%name='mrtholo'
  pack%ext='.holo'
  pack%authors="R.Lucas"
  pack%depend(1:4) = (/ locwrd(sic_pack_set),    locwrd(greg_pack_set),  &
                        locwrd(mrtcal_pack_set), locwrd(clic_pack_set)/)
  pack%init=locwrd(mrtholo_pack_init)
  pack%clean=locwrd(mrtholo_pack_clean)
  !
end subroutine mrtholo_pack_set
!
subroutine mrtholo_pack_init(gpack_id,error)
  use mrtholo_interfaces, except_this => mrtholo_pack_init
  !----------------------------------------------------------------------
  ! @ private-mandatory
  !----------------------------------------------------------------------
  integer :: gpack_id
  logical :: error
  !
  ! Init messaging
  call mrtholo_message_set_id(gpack_id)
  ! Init error flag
  error = .false.
  ! MRTHOLO package specific initialization
  call mrtholo_init(error)
  if (error) return
  ! Load local language
  call mrtholo_load
  !
end subroutine mrtholo_pack_init
!
subroutine mrtholo_pack_clean(error)
  use mrtholo_interfaces, except_this => mrtholo_pack_clean
  !----------------------------------------------------------------------
  ! @ private-mandatory
  ! Called at end of session. Might clean here for example global buffers
  ! allocated during the session
  !----------------------------------------------------------------------
  logical :: error
  !
  call mrtholo_exit(error)
  if (error) return
  !
end subroutine mrtholo_pack_clean
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
