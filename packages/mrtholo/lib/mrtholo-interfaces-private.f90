module mrtholo_interfaces_private
  interface
    subroutine mrtholo_init(error)
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      ! MRTHOLO Initialization
      !----------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine mrtholo_init
  end interface
  !
  interface
    subroutine mrtholo_exit(error)
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      ! MRTHOLO Cleaning on exit
      !----------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine mrtholo_exit
  end interface
  !
  interface
    subroutine mrtholo_load
      !---------------------------------------------------------------------
      ! @ private
      ! Define and load the MRTHOLO language
      !---------------------------------------------------------------------
      ! Local
    end subroutine mrtholo_load
  end interface
  !
  interface
    subroutine mrtholo_run(line,command,error)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private-mandatory
      ! Support routine for the language MRTHOLO.
      ! This routine is able to call the routines associated to each
      ! command of the language MRTHOLO
      !---------------------------------------------------------------------
      character(len=*),  intent(in)    :: line
      character(len=12), intent(in)    :: command
      logical,           intent(inout) :: error
    end subroutine mrtholo_run
  end interface
  !
  interface
    function mrtholo_error()
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for the error status of the library.
      ! It is a scory. Please do not use it
      !---------------------------------------------------------------------
      logical :: mrtholo_error
    end function mrtholo_error
  end interface
  !
  interface
    subroutine mrtholo_pack_set(pack)
      use gpack_def
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(gpack_info_t), intent(out) :: pack
    end subroutine mrtholo_pack_set
  end interface
  !
  interface
    subroutine mrtholo_pack_init(gpack_id,error)
      !----------------------------------------------------------------------
      ! @ private-mandatory
      !----------------------------------------------------------------------
      integer :: gpack_id
      logical :: error
    end subroutine mrtholo_pack_init
  end interface
  !
  interface
    subroutine mrtholo_pack_clean(error)
      !----------------------------------------------------------------------
      ! @ private-mandatory
      ! Called at end of session. Might clean here for example global buffers
      ! allocated during the session
      !----------------------------------------------------------------------
      logical :: error
    end subroutine mrtholo_pack_clean
  end interface
  !
  interface
    subroutine mrtholo_toclic_command(line,error)
      use gildas_def
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      !  Support routine for command
      !    TOCLIC  FileName [/NORMALIZE Power]
      !  Reads one imbfits file and write it to the currently opened
      ! Clic file
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      logical,          intent(inout) :: error
    end subroutine mrtholo_toclic_command
  end interface
  !
  interface
    subroutine mrtholo_toclic_file(filename,normpower,error)
      use gbl_message
      use mrtcal_buffer_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: filename
      real(kind=4),     intent(in)    :: normpower(2)
      logical,          intent(inout) :: error
    end subroutine mrtholo_toclic_file
  end interface
  !
  interface
    subroutine mrtholo_write(filebuf,normpower,error)
      use gbl_message
      use classic_api
      use mrtcal_buffer_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(imbfits_buffer_t), intent(in)    :: filebuf       !
      real(kind=4),           intent(in)    :: normpower(2)  ! Normalization power
      logical,                intent(inout) :: error         !
    end subroutine mrtholo_write
  end interface
  !
  interface
    subroutine mrtholo_toclic_header(filebuf,error)
      use gbl_constant
      use phys_const
      use gbl_message
      use mrtcal_buffer_types
      use clic_file
      !---------------------------------------------------------------------
      ! @ private
      !  Set up the General section
      !---------------------------------------------------------------------
      type(imbfits_buffer_t), intent(in)    :: filebuf  !
      logical,                intent(inout) :: error    !
    end subroutine mrtholo_toclic_header
  end interface
  !
  interface
    subroutine mrtholo_toclic_data(filebuf,ir,itime,jtime,normpower,dataArray,error)
      use phys_const
      use gbl_message
      use mrtcal_buffer_types
      !---------------------------------------------------------------------
      ! @ private
      !  Set up the data header
      !---------------------------------------------------------------------
      type(imbfits_buffer_t),    intent(in)    :: filebuf       !
      integer(kind=4),           intent(in)    :: ir            !
      integer(kind=4),           intent(in)    :: itime         !
      integer(kind=4),           intent(in)    :: jtime         !
      real(kind=4),              intent(in)    :: normpower(2)  !
      integer(kind=4),           intent(inout) :: dataArray(:)  !
      logical,                   intent(inout) :: error         !
    end subroutine mrtholo_toclic_data
  end interface
  !
end module mrtholo_interfaces_private
