subroutine mrtholo_write(filebuf,normpower,error)
  use gbl_message
  use classic_api
  use mrtcal_buffer_types
  use mrtholo_dependencies_interfaces
  use mrtholo_interfaces, except_this=>mrtholo_write
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(imbfits_buffer_t), intent(in)    :: filebuf       !
  real(kind=4),           intent(in)    :: normpower(2)  ! Normalization power
  logical,                intent(inout) :: error         !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  ! Local
  character(len=*), parameter :: rname='MRTHOLO>WRITE'
  logical :: check
  integer(kind=4), allocatable :: dataArray(:)
  integer(kind=data_length) :: ndata
  integer(kind=4) :: ir,itime,itime_offset,jtime,ier
  character(len=message_length) :: chain
  !
  ! this is writing one subscan
  !
  ! convert header sections
  !
!!!   print *,">>> 1 :",filebuf%imbf%primary%telescop%val
!!!   print *,">>> 2 :",filebuf%imbf%scan%head%projid%val
!!!   print *,">>> 3 :",filebuf%imbf%front%head%date_obs%val
!!!   print *,">>> 4 :",filebuf%imbf%back%head%febefeed%val
!!!   print *,">>> 4 :",filebuf%imbf%back%table%chans%val(:)
  !
  !print *,">>> 5 :",filebuf%subscanbuf%subscan%isub
  !print *,">>> 6 :",filebuf%subscanbuf%subscan%antslow%head%obsnum%val
  !print *,">>> 7 :",filebuf%subscanbuf%subscan%antfast%head%dopplerc%val
  !print *,">>> 8 :",filebuf%subscanbuf%subscan%backdata%head%channels%val
  !print *,">>> 9 :",filebuf%subscanbuf%subscan%backdata%table%mjd%val(:)
  !
  !print *,">>> 10 :",filebuf%subscanbuf%databuf%imbf%val(:,1,:)
  !print *,">>> 11 :",filebuf%subscanbuf%databuf%imbf%ntime
  !print *,">>> 12 :",filebuf%subscanbuf%databuf%imbf%npix
  !print *,">>> 13 :",filebuf%subscanbuf%databuf%imbf%nchan
  !print *,">>> 14 :",filebuf%subscanbuf%databuf%time%cur%first
  !print *,">>> 15 :",filebuf%subscanbuf%databuf%time%cur%last
  !
  call check_output_file(error)
  if (error) return
  ! write_mode = 'NEW'
  check = .true.
  r_presec(:) = .false.
  r_presec(rfset_sec)= .true.
  r_presec(contset_sec)= .true.
  r_presec(lineset_sec)= .true.
  r_presec(atparm_sec)= .true.
  r_presec(atmon_sec)= .true.
  r_presec(scanning_sec)= .true.
  r_presec(atparm_sec)= .true.
  r_presec(alma_sec)= .true.
  r_presec(abpcal_sec)= .true.
  r_presec(aical_sec)= .true.
  r_presec(bpcal_sec)= .true.
  r_presec(ical_sec)= .true.
  r_icdeg = 3
  r_aicdeg = 3

  call mrtholo_toclic_header(filebuf, error)
  if (error)  return

  ! compute ndata (length of data section)
  ndata = r_ndump*(r_ldpar+r_ldatc) + r_ldpar+r_ldatc+r_ldatl
  ! allocate data section (dataArray, length ndata)
  if (allocated(dataArray)) then
    if (ndata.ne.size(dataArray)) then
      if (size(dataArray).gt.0) deallocate(dataArray, stat=ier)
      if (ier.ne.0) return
      allocate(dataArray(ndata), stat=ier)
      if (ier.ne.0) return
    endif
  else
    allocate(dataArray(ndata), stat=ier)
    if (ier.ne.0) return
  endif
  !
  ! Loop on subintegrations
  ! itime: position in the DATA column (filebuf%subscanbuf%databuf%imbf%val), because
  !        we read only the rows between subscan-start and subscan-end
  ! jtime = itime+itime_offset: position in the others columns (e.g.
  !        filebuf%subscanbuf%subscan%backdata%table%mjd%val), because we read the
  !        whole columns
  itime_offset = filebuf%subscanbuf%databuf%time%cur%first-1
  do ir = 1,r_ndump
    itime = ir
    jtime = itime+itime_offset
    call mrtholo_toclic_data(filebuf,ir,itime,jtime,normpower,dataArray,error)
  enddo
  ! average record (write a dummy row since it is not used for holographies)
  ir = r_ndump+1
  itime = r_ndump  ! Must use a valid time row else we would overflow our DATA column
  jtime = itime+itime_offset
  call mrtholo_toclic_data(filebuf,ir,itime,jtime,normpower,dataArray,error)
  !
  check = .false.
  call ipb_write('NEW',.false.,error)
  if (error)  return
  call wdata(ndata,dataArray,.true.,error)
  if (error) return
  !
  call ipb_close(error)
  if (error)  return
  !
  write(chain,'(A,I0,A,I0,A)') 'Observation ',r_num,' written  (',r_ndump,' records)'
  call mrtholo_message(seve%i,rname,chain)
  !
end subroutine mrtholo_write
!
subroutine mrtholo_toclic_header(filebuf,error)
  use gbl_constant
  use phys_const
  use gbl_message
  use mrtcal_buffer_types
  use mrtholo_dependencies_interfaces
  use mrtholo_interfaces, except_this=>mrtholo_toclic_header
  use clic_file
  !---------------------------------------------------------------------
  ! @ private
  !  Set up the General section
  !---------------------------------------------------------------------
  type(imbfits_buffer_t), intent(in)    :: filebuf  !
  logical,                intent(inout) :: error    !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_proc_par.inc'
  include 'clic_dheader.inc'
  include 'clic_clic.inc'
  external :: dummyConvert
  ! Local
  character(len=*), parameter :: rname='MRTHOLO>TOCLIC>HEADER'
  ! Size of buffer to be passed to convert_dhsub. Actual size can be
  ! known a posteriori in CLIC; FILE IN file.ipb; FIND; GET F;
  ! DUMP H DD (r_ldpar value).
  integer(kind=4), parameter :: bufsize=1024
  integer(kind=4) :: testbuffer(bufsize),npol
  integer(kind=4), parameter :: holo_version=6
  character(len=message_length) :: mess
  !
  ! --- General ---
  r_teles = filebuf%imbf%primary%telescop%val
  r_ver = 0
  select case (filebuf%imbf%scan%head%ctype1%val)
  case ('RA-SFL')
    r_typec = type_eq
  case ('galactic') ! *** JP
    r_typec = type_ga
  case ('horizontal') ! *** JP
    r_typec = type_ho
  case default
    r_typec = type_un
  end select
  r_kind = 5
  r_scan = filebuf%imbf%scan%head%scannum%val
  call gag_mjd2gagut(filebuf%subscanbuf%subscan%antslow%head%mjd_beg%val,  &
    r_dobs,r_ut,error)
  if (error)  return
  r_dred = r_dobs
  if (filebuf%subscanbuf%subscan%antslow%head%substype%val.eq.'onTheFly') then
    r_proc = p_holog
  elseif  (filebuf%subscanbuf%subscan%antslow%head%substype%val.eq.'track') then
    r_proc = p_source
  endif
  r_project = filebuf%imbf%primary%projid%val
  !
  ! --- Interferometer General Information ---
  r_nant = 1
  r_nbas = 1
  r_kant(1) = 0  ! By convention
  r_iant(1) = 1
  r_jant(1) = 1
  r_istat(1) = 0
  !
  ! --- Continuum setup ---
  r_nsb = 2
  r_nband = 1
  r_cfwid(1) = 10. ! [MHz] find the right value (width in MHz) zzz
  r_sband(1,1) = 1
  r_sband(2,1) = 2
  !
  ! --- RF Frequency setup ---
  r_npol_rec = 1
  r_restf = filebuf%imbf%front%table%restfreq%val(1)*1000d0 ! [MHz]
  ! set to 40GHz so that the spacing in test data is reasonable.
  !  r_flo1 = 40.e3 ! ### find the right value (MHz) zzz
  r_flo1 = filebuf%imbf%front%table%restfreq%val(1)*1000d0 ! [MHz]
  r_nif = 1
  r_ifname(1) = "tbd"
  r_nbb = 1
  r_bbname(1) = "tbd"
  !
  ! --- Line setup ---
  r_lband = 1
  r_lntch = 0
  r_lnch(1) = 0  ! works?
  r_lich(1) = 0
  r_lmode = 1
  r_lnsb = 2
  r_band4(1) = 0
  r_flo2(1) = 0.
  r_flo3(1) = 0.
  r_flo4(1) = 0.
  r_lpolmode = 1
  r_lpolentry(1,1) = 1
  r_bb(1) = 1
  r_if(1) = 1
  r_lsband(1,1) = 1
  r_lsband(2,1) = 2
  !
  ! --- Atmospheric parameters ---
  r_pamb =  filebuf%imbf%scan%head%pressure%val            ! [hPa]
  r_tamb = filebuf%imbf%scan%head%tambient%val + 273.15d0  ! [K]
  r_alti = filebuf%imbf%scan%head%siteelev%val/1d3         ! [km]
  r_humid = filebuf%imbf%scan%head%Humidity%val            ! [%]
  r_avwind = filebuf%imbf%scan%head%windvel%val            ! [m/s]
  r_avwindir = filebuf%imbf%scan%head%winddir%val          ! [deg]
  r_topwind = filebuf%imbf%scan%head%windvelm%val          ! [m/s]
  !
  ! --- Antenna specific parameters ---
  r_trandist = 36000000.                                  ! [m] find the actual distance zzz
  r_tranfocu = 0.                                         ! no focus mechanical offset.
  r_antennaName(1) = 'IRAM30m'                            !
  r_antennaType(1) = 'IRAM 30m'                           !
  r_dishDiameter(1) = filebuf%imbf%scan%head%telsize%val  ! [m]
  !
  ! --- Positions ---
  r_sourc = filebuf%imbf%scan%head%object%val             ! []    Source name
  r_lam = filebuf%imbf%scan%head%longobj%val*rad_per_deg  ! [rad] Source longitude
  r_bet = filebuf%imbf%scan%head%latobj%val*rad_per_deg   ! [rad] Source latitude
  r_flux = 1.0
  r_spidx = 0.0
  ! force to horizon ?
  !
  ! --- Scanning function ---
  r_mobil(1) = .true.
  !
  ! --- Data Section Description ---
  new_receivers = .true.
  r_ndump = filebuf%subscanbuf%databuf%imbf%ntime
  r_ldpar = 1000000  ! length of data header: dummy value > 999999 so that
                     ! convert_dhsub recomputes the actual length in return
  npol = 1
  testbuffer(:) = 0  ! For dh_ntime in convert_dhsub
  ! data and observation data descriptor need to be set to version_current
  ! for coherence with iobs (called by ipb_write) in the computation
  ! of r_ldpar.
  if (holo_version.ne.version_current) then
    write(mess,'(A,I0)')  &
      'MRTHOLO only supports writing CLIC format version ',holo_version
    call mrtholo_message(seve%e,rname,mess)
    write(mess,'(A,I0)')  &
      'Use CLIC\SET VERSION ',holo_version
    call mrtholo_message(seve%e,rname,mess)
    error = .true.
    return
  endif
  e%version = holo_version
  de%version = holo_version
  ! Called just for computing r_ldpar
  call convert_dhsub(testbuffer,r_ldpar,r_nant,r_nbas,r_lband,npol,  &
    dummyconvert,dummyconvert,dummyconvert,dummyconvert)
  r_ldatc = 2*r_nband*r_nsb*r_nbas*npol   ! length of continuum data
  r_ldatl = 2*r_lntch*r_lnsb*r_nbas*npol  ! length of line data
  r_ldump = r_ldpar + r_ldatc             !
  r_ndatl = 1                             ! number of line datas (1,2)
  !
end subroutine mrtholo_toclic_header
!
subroutine dummyConvert(i,j,n)
  integer :: i,n,j
end subroutine dummyConvert
!
subroutine mrtholo_toclic_data(filebuf,ir,itime,jtime,normpower,dataArray,error)
  use phys_const
  use gbl_message
  use mrtcal_buffer_types
  use mrtholo_dependencies_interfaces
  use mrtholo_interfaces, except_this=>mrtholo_toclic_data
  !---------------------------------------------------------------------
  ! @ private
  !  Set up the data header
  !---------------------------------------------------------------------
  type(imbfits_buffer_t),    intent(in)    :: filebuf       !
  integer(kind=4),           intent(in)    :: ir            !
  integer(kind=4),           intent(in)    :: itime         !
  integer(kind=4),           intent(in)    :: jtime         !
  real(kind=4),              intent(in)    :: normpower(2)  !
  integer(kind=4),           intent(inout) :: dataArray(:)  !
  logical,                   intent(inout) :: error         !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! Local
  character(len=*), parameter :: rname='MRTHOLO>TOCLIC>DATA'
  real(kind=8) :: mjdtime,lst
  real(kind=4) :: longoff,latoff,azimuth,elevation
  integer(kind=4) :: h_offset,c_offset,k,kc,nchan,ichan
  real(kind=4) :: floatData(4),dist2,gauss
  real(kind=4) :: refpower,xreal,ximag,sumpower,sumreal,sumimag
  !
  dh_dump = min(ir, r_ndump)
  call mrtcal_chunk_mjd_from_data(filebuf%subscanbuf%subscan%backdata,jtime,mjdtime,error)
  if (error)  return
  call gag_mjd2gagut(mjdtime,dh_obs,dh_utc,error)
  if (error)  return
  dh_utc = dh_utc*43200d0/pi  ! From range [0:2*pi] to range [0:86400]
  dh_integ = filebuf%subscanbuf%subscan%backdata%table%integtim%val(jtime)
  !
  call mrtcal_interp_coord_from_antslow(filebuf%subscanbuf%subscan%antslow,  &
    filebuf%subscanbuf%subscan%onsky,mjdtime,  &
    longoff,latoff,azimuth,elevation,lst,error)
  if (error)  return
  r_az = azimuth    ! [rad]
  r_el = elevation  ! [rad]
  dh_offlam(1) = longoff*sec_per_rad
  dh_offbet(1) = latoff*sec_per_rad
  !
  k = 1 + h_offset(ir)
  call encode_header(dataArray(k))
  !
  ! the data
  mjdtime = filebuf%subscanbuf%subscan%backdata%table%mjd%val(jtime)
  ! Try to guess the number of data per channels. Should be more robust
  ! or support only one format in the end.
  if (mod(filebuf%subscanbuf%subscan%backdata%head%channels%val,7).eq.0) then
    ! (ReferencePower, Xreal, Ximag, XMagnitudedBV, XPhiDegree, ReferenceMagnitudedBV, SignalMagnitudedBV)
    nchan = filebuf%subscanbuf%subscan%backdata%head%channels%val/7
  elseif (mod(filebuf%subscanbuf%subscan%backdata%head%channels%val,3).eq.0) then
    ! (ReferencePower, Xreal, Ximag)
    nchan = filebuf%subscanbuf%subscan%backdata%head%channels%val/3
  else
    call mrtholo_message(seve%e,rname,'Unexpected number of CHANNELS (not a multiple of 3 or 7)')
    error = .true.
    return
  endif
  !
  ! find line peak

!!!   do i=1, channels
!!!     print 100, ir, i, (filebuf%subscanbuf%databuf%imbf%val(i+(k-1)*channels,1, itime), k=1, 3)
!!! 100 format (i4, i4, 3(1pg10.3))
!!!   enddo
!!!
  kc = 1 + c_offset(ir)
  dist2 = dh_offlam(1)**2 + dh_offbet(1)**2
  gauss = 1.8 * exp(-dist2/45**2)
  ! here copy the data into dataArray(kc), number of words TBD ...
  sumreal = 0.0
  sumimag = 0.0
  sumpower = 0.0
  !
  ! Normalization with refpower (NB: disabled if normpower(:) = 0)
  do ichan=1, nchan
    refpower = filebuf%subscanbuf%databuf%imbf%val(ichan,1, itime)
    xreal = filebuf%subscanbuf%databuf%imbf%val(ichan+nchan,1, itime)
    ximag = filebuf%subscanbuf%databuf%imbf%val(ichan+2*nchan,1, itime)
    sumpower = sumpower + refpower
    sumreal = sumreal + xreal * refpower**normpower(1)
    sumimag = sumimag + ximag * refpower**normpower(1)
  enddo
  sumreal = sumreal / sumpower**normpower(2)
  sumimag = sumimag / sumpower**normpower(2)
  !
  floatData(1) = sumreal
  floatData(2) = sumimag
  floatData(3) = sumreal
  floatData(4) = sumimag
  call r4tor4(floatData, dataArray(kc), 4)  ! should be the complex data
  !
end subroutine mrtholo_toclic_data
