!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage MRTHOLO messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module mrtholo_message_private
  use gpack_def
  !
  ! Identifier used for message identification
  integer :: mrtholo_message_id = gpack_global_id  ! Default value for startup message
  !
end module mrtholo_message_private
!
subroutine mrtholo_message_set_id(id)
  use mrtholo_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer, intent(in) :: id
  ! Local
  character(len=message_length) :: mess
  !
  mrtholo_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',mrtholo_message_id
  call mrtholo_message(seve%d,'mrtholo_message_set_id',mess)
  !
end subroutine mrtholo_message_set_id
!
subroutine mrtholo_message(mkind,procname,message)
  use mrtholo_message_private
  use gbl_message
  !---------------------------------------------------------------------
  ! Messaging facility for the current library. Calls the low-level
  ! (internal) messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind     ! Message kind
  character(len=*), intent(in) :: procname  ! Name of calling procedure
  character(len=*), intent(in) :: message   ! Message string
  !
  call gmessage_write(mrtholo_message_id,mkind,procname,message)
  !
end subroutine mrtholo_message
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
