module clic_interfaces_public
  !-------------------------------------------------------------------------
  !  CLIC public interfaces (used by MRTHOLO), generated once since CLIC
  ! does not yet generates automatically its interfaces
  !-------------------------------------------------------------------------
  !
  interface
    subroutine ipb_write(chain,check,error)
      use classic_api
      !---------------------------------------------------------------------
      ! @ public
      !  Write a scan
      ! Argument:
      ! CHAIN   C*(*)   Write status ('NEW', 'EXTEND', 'UPDATE'
      !       CHECK     L       Check for duplicate write
      ! ERROR   L       Error return
      !---------------------------------------------------------------------
      character(len=*) :: chain         !
      logical :: check                  !
      logical :: error                  !
    end subroutine ipb_write
  end interface
  !
  interface
    subroutine ipb_close(error)
      use gbl_format
      !---------------------------------------------------------------------
      ! @ public
      !         Close an observation. Reopens the output file.
      !---------------------------------------------------------------------
      logical :: error                  !
    end subroutine ipb_close
  end interface
  !
  interface
    subroutine convert_dhsub(iwin,nw,nant,nbas,nband,npol,r4,r8,i4,cc)
      use classic_api
      !---------------------------------------------------------------------
      ! @ public
      ! Convert data header IWIN using conversion code CONVE,
      ! assuming NW is the length of the data header.
      ! Arguments
      ! IWIN    I(*)    Array containing data header to be converted
      ! NW      I       Size of IWIN
      ! CONVE   I       Conversion code
      ! NANT    I       Number of antennas
      ! NBAS    I       Number of baselines
      !---------------------------------------------------------------------
      integer :: iwin(*)                !
      integer(kind=data_length) :: nw                     !
      integer :: nant                   !
      integer :: nbas                   !
      integer :: nband                  !
      integer :: npol                   !
      external :: r4                    !
      external :: r8                    !
      external :: i4                    !
      external :: cc                    !
    end subroutine convert_dhsub
  end interface
  !
  interface
    subroutine wdata (ndata,data,copy_data,error)
      use classic_api
      !---------------------------------------------------------------------
      ! @ public
      ! write the data section
      !---------------------------------------------------------------------
      integer(kind=data_length):: ndata      ! Data length (number of "words")
      integer                  :: data(*)    ! Data array
      logical                  :: copy_data  ! Use do_write_data to write data ?
      logical                  :: error      ! Logical error flag
    end subroutine wdata
  end interface
  !
end module clic_interfaces_public
!
module mrtholo_dependencies_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! MRTHOLO dependencies public interfaces. Do not use out of the
  ! library.
  !---------------------------------------------------------------------
  use gkernel_interfaces
  use class_interfaces_public
  use imbfits_interfaces_public
  use mrtcal_interfaces_public
  use clic_interfaces_public
end module mrtholo_dependencies_interfaces
!
module mrtholo_interfaces
  use mrtholo_interfaces_private
  use mrtholo_interfaces_public
end module mrtholo_interfaces
