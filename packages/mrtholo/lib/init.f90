!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mrtholo_init(error)
  use gbl_message
  use gkernel_interfaces
  use mrtholo_interfaces, except_this=>mrtholo_init
  !----------------------------------------------------------------------
  ! @ private
  ! MRTHOLO Initialization
  !----------------------------------------------------------------------
  logical, intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='INIT'
  integer(kind=4) :: ier
  !
  call mrtholo_message(seve%d,rname,'Welcome')
  !
  ier = sic_setlog('gag_help_mrtholo','gag_doc:hlp/mrtholo-help-mrtholo.hlp')
  if (ier.eq.0) then
    error=.true.
    return
  endif
  !
end subroutine mrtholo_init
!
subroutine mrtholo_exit(error)
  use gbl_message
  use mrtholo_interfaces, except_this=>mrtholo_exit
  !----------------------------------------------------------------------
  ! @ private
  ! MRTHOLO Cleaning on exit
  !----------------------------------------------------------------------
  logical, intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='EXIT'
  !
  call mrtholo_message(seve%d,rname,'Welcome')
  !
end subroutine mrtholo_exit
!
subroutine mrtholo_load
  use mrtholo_interfaces, except_this=>mrtholo_load
  !---------------------------------------------------------------------
  ! @ private
  ! Define and load the MRTHOLO language
  !---------------------------------------------------------------------
  ! Local
  integer, parameter :: mmrtholo=2
  character(len=12) :: vocab(mmrtholo)
  data vocab / ' TOCLIC', '/NORMALIZE' /
  !
  ! Load the new language
  call sic_begin(        &
    'MRTHOLO',           &  ! Language name
    'GAG_HELP_MRTHOLO',  &  !
    mmrtholo,            &  ! Number of commands + options in language
    vocab,               &  ! Array of commands
    '0.1',               &  ! Some version string
    mrtholo_run,         &  ! The routine which handles incoming commands
    mrtholo_error)          ! The error status routine of the library
  !
end subroutine mrtholo_load
!
subroutine mrtholo_run(line,command,error)
  use gbl_message
  use mrtholo_interfaces, except_this=>mrtholo_run
  !---------------------------------------------------------------------
  ! @ private-mandatory
  ! Support routine for the language MRTHOLO.
  ! This routine is able to call the routines associated to each
  ! command of the language MRTHOLO
  !---------------------------------------------------------------------
  character(len=*),  intent(in)    :: line
  character(len=12), intent(in)    :: command
  logical,           intent(inout) :: error
  !
  call mrtholo_message(seve%c,'MRTHOLO',line)
  !
  ! Call appropriate subroutine according to COMMAND
  select case(command)
  case ('TOCLIC')
    call mrtholo_toclic_command(line,error)
  case default
    call mrtholo_message(seve%e,'MRTHOLO_RUN','Unimplemented command '//command)
    error = .true.
  end select
  !
end subroutine mrtholo_run
!
function mrtholo_error()
  use mrtholo_interfaces, except_this=>mrtholo_error
  !---------------------------------------------------------------------
  ! @ private
  ! Support routine for the error status of the library.
  ! It is a scory. Please do not use it
  !---------------------------------------------------------------------
  logical :: mrtholo_error
  !
  mrtholo_error = .false.
  !
end function mrtholo_error
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
