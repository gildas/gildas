subroutine mrtholo_toclic_command(line,error)
  use gildas_def
  use gbl_message
  use gkernel_interfaces
  use mrtholo_interfaces, except_this=>mrtholo_toclic_command
  !---------------------------------------------------------------------
  ! @ private
  !  Support routine for command
  !    TOCLIC  FileName [/NORMALIZE Power]
  !  Reads one imbfits file and write it to the currently opened
  ! Clic file
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='TOCLIC>COMMAND'
  character(len=filename_length) :: filename
  real(kind=4) :: normpower(2)
  integer(kind=4) :: nc
  !
  call mrtholo_message(seve%d,rname,'Welcome')
  !
  call sic_ch(line,0,1,filename,nc,.true.,error)
  if (error)  return
  !
  ! /NORMALIZATION NONE | P1 [P2]
  if (sic_present(1,1)) then
    call parse_normpower(line,1,normpower(1),error)
    if (error)  return
  else
    normpower(1) = 0.0  ! Default normalization is NONE
  endif
  if (sic_present(1,2)) then
    call parse_normpower(line,2,normpower(2),error)
    if (error)  return
  else
    normpower(2) = normpower(1)
  endif
  !
  call mrtholo_toclic_file(filename,normpower,error)
  if (error)  return
  !
contains
  subroutine parse_normpower(line,iarg,val,error)
    character(len=*), intent(in)    :: line
    integer(kind=4),  intent(in)    :: iarg
    real(kind=4),     intent(out)   :: val
    logical,          intent(inout) :: error
    ! Local
    character(len=16) :: arg
    integer(kind=4) :: nc
    character(len=5), parameter :: keynone='NONE '
    !
    call sic_ke(line,1,iarg,arg,nc,.true.,error)
    if (error)  return
    !
    if (arg(1:nc).eq.keynone(1:nc)) then
      val = 0.0
    else
      call sic_math_real(arg,nc,val,error)
      if (error)  return
      if (val.lt.0.0) then
        ! User is not allowed to provide <0 power, this is our trick for no
        ! normalization.
        call mrtholo_message(seve%e,rname,'Normalization power must be positive')
        error = .true.
        return
      endif
    endif
    !
  end subroutine parse_normpower
  !
end subroutine mrtholo_toclic_command
!
subroutine mrtholo_toclic_file(filename,normpower,error)
  use gbl_message
  use mrtcal_buffer_types
  use mrtholo_dependencies_interfaces
  use mrtholo_interfaces, except_this=>mrtholo_toclic_file
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: filename
  real(kind=4),     intent(in)    :: normpower(2)
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='TOCLIC'
  type(imbfits_buffer_t) :: filebuf
  integer(kind=4) :: isub,ndone
  character(len=message_length) :: mess
  !
  call imbfits_read_header(filename,filebuf%imbf,0.0,error)
  if (error) return
  !
  ndone = 0
  do isub=1,filebuf%imbf%primary%n_obsp%val
    ! Read subscan headers and columns, except DATA
    call imbfits_read_subscan_header_bynum(filebuf%imbf,isub,  &
      .false.,.false.,0.d0,filebuf%subscanbuf%subscan,error)
    if (error)  exit
    !
    ! Find the on-track range for the DATA column
    call mrtcal_get_time_range_for_backend(filebuf%subscanbuf%subscan,  &
      filebuf%subscanbuf%databuf%time%tot,error)
    if (error) then
      write(mess,'(A,I0,A)') 'Error processing subscan #',isub,'. Skip.'
      call mrtholo_message(seve%w,rname,mess)
      error = .false.
      cycle
    endif
    !
    ! Select all the DATA column at once:
    call mrtcal_bookkeeping_all(filebuf%subscanbuf%subscan,  &
      filebuf%subscanbuf%databuf%time,error)
    if (error)  exit  ! Or skip this subscan?
    !
    ! Read the selected range:
    call mrtcal_read_subscan_data(filebuf%imbf,filebuf%subscanbuf%subscan,  &
      .true.,filebuf%subscanbuf%databuf,error)
    if (error)  exit
    !
    call mrtholo_write(filebuf,normpower,error)
    if (error)  exit
    !
    ndone = ndone+1
  enddo
  if (error) then
    ! Unrecoverable error
    write(mess,'(A,I0)') 'Error processing subscan #',isub
    call mrtholo_message(seve%e,rname,mess)
    goto 100
  endif
  !
  if (normpower(1).eq.0.0 .and. normpower(2).eq.0.0) then
    write(mess,'(A)')  'Data not normalized'
  else
    write(mess,'(A,F0.1,A,F0.1)')  'Data weighted with refpower**',normpower(1),  &
      ' and normalized with sum(refpower)**',normpower(2)
  endif
  call mrtholo_message(seve%i,rname,mess)
  !
  write(mess,'(I0,A,I0,A)')  &
    ndone,'/',filebuf%imbf%primary%n_obsp%val,' subscans successfully processed'
  call mrtholo_message(seve%i,rname,mess)
  !
100 continue
  !
  ! Free file unit
  call imbfits_close_file(filebuf%imbf%file,error)
  if (error) continue
  !
  ! Deallocate buffers
  call imbfits_free_leadhdus(filebuf%imbf,error)
  call free_eclass_char(filebuf%imbf%seclass,error)
  call imbfits_free_subscan_header(filebuf%subscanbuf%subscan,error)
  call mrtcal_free_subscan_data(filebuf%subscanbuf%databuf,error)
  !
end subroutine mrtholo_toclic_file
