!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! The developer needs to define the three following procedures:
!    1. l-ctrl-slave-init      => Initialize the widget.
!    2. l-ctrl-slave-action    => Process the action.
!    3. l-ctrl-slave-loop      => Decode the user request.
!    4. l-ctrl-slave-exit      => Clean-out the widget.
!    5. l-ctrl-slave-help-position-independent => help of position-independent actions
!    6. l-ctrl-slave-help-position-dependent   => help of position-dependent   actions
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
begin procedure l-ctrl-master-init
  ! Initialize the widget CTRL by calling its initialization procedure
  if exist(ctrlmast) delete /var ctrlmast
  define structure ctrlmast /global
  define structure ctrlmast%hardcopy /global
  define integer ctrlmast%hardcopy%i /global
  @ l-ctrl-slave-init
end procedure l-ctrl-master-init
!
begin procedure l-ctrl-master-panel-list-define
  ! (Re)initialize the list of panels
  if exist(ctrlmast%pan) delete /var ctrlmast%pan
  define structure ctrlmast%pan /global
  define integer ctrlmast%pan%n ctrlmast%pan%icurr ctrlmast%pan%iout /global
  let ctrlmast%pan%iout 0 ! Constant parameter meaning outside of any defined panel
  let ctrlmast%pan%n &1
  let ctrlmast%pan%icurr &1
  define character*16 ctrlmast%pan%name[ctrlmast%pan%n] /global
  define structure ctrlmast%pan%bound /global
  define double ctrlmast%pan%bound%max[4,ctrlmast%pan%n] /global
  define double ctrlmast%pan%bound%cur[4,ctrlmast%pan%n] /global
end procedure l-ctrl-master-panel-list-define
!
begin procedure l-ctrl-master-panel-list-enlarge
  ! Initialize or enlarge the list of panels
  if .not.exist(ctrlmast%pan) then
     @ l-ctrl-master-panel-list-define 1
  else
     define structure oldpan /like ctrlmast%pan
     let oldpan% ctrlmast%pan%
     @ l-ctrl-master-panel-list-define 'oldpan%n+1'
     !     let ctrlmast%pan%name[1:oldpan%n] oldpan%name
     for ipan 1 to oldpan%n
        let ctrlmast%pan%name[ipan] 'oldpan%name[ipan]'
     next ! ipan
     let ctrlmast%pan%bound%max[1:oldpan%n] oldpan%bound%max
     let ctrlmast%pan%bound%cur[1:oldpan%n] oldpan%bound%cur
  endif
end procedure l-ctrl-master-panel-list-enlarge
!
begin procedure l-ctrl-master-register-panel
  ! Register a panel
  @ l-ctrl-master-panel-list-enlarge
  let ctrlmast%pan%name[ctrlmast%pan%icurr] 'ctrlmast%pan%icurr'"-&1"
  let ctrlmast%pan%bound%max[ctrlmast%pan%icurr] &2 &3 &4 &5
  let &6 ctrlmast%pan%icurr
end procedure l-ctrl-master-register-panel
!
begin procedure l-ctrl-master-panel-update-bound
  ! Update panel boundaries when the image is smaller than the initial attributed
  ! space. This happens when the aspect ratio varies.
  gtvl\gtv search "<greg<"'ctrlmast%pan%name[&1]'
  if (.not.gtv%exist) then
     gtvl\change directory <greg
     gtvl\create directory "<greg<"'ctrlmast%pan%name[&1]'
  endif
  gtvl\change directory "<greg<"'ctrlmast%pan%name[&1]'
  !display tree
  !pause
  set\panel /default /position 'ctrlmast%pan%bound%max[1,&1]' 'ctrlmast%pan%bound%max[2,&1]' 'ctrlmast%pan%bound%max[3,&1]' 'ctrlmast%pan%bound%max[4,&1]'
  set\panel 1 /nxy 1 /inter 0 0 /aspect &2
  ! Panel actual boundaries must be updated as an aspect ratio different from
  ! the available maximum boundaries may have been requested
  let ctrlmast%pan%bound%cur[&1] 'box_xmin' 'box_xmax' 'box_ymin' 'box_ymax'
end procedure l-ctrl-master-panel-update-bound
!
begin procedure l-ctrl-master-get-user-coord
  ! Get user coordinates at the position of the cursor when the user triggered
  ! an action
  gtvl\change directory "<greg<"'ctrlmast%pan%name[ctrlmast%pan%icurr]'
  !display tree
  !pause
  greg1\draw relo * *
  let &1 use_curs
end procedure l-ctrl-master-get-user-coord
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
begin procedure l-ctrl-master-action
  @ l-ctrl-slave-action
end procedure l-ctrl-master-action
!
begin procedure l-ctrl-master-wait-event
  ! Generic actions to go into wait mode and to interpret the new user action
  !
  ! Get back to full window
  gtvl\change directory <greg
  ! Wait for user event
  greg1\draw relo
  ! Analyse user event position and go to the associated directory
  let ctrlmast%pan%icurr ctrlmast%pan%iout
  for ipan 1 to ctrlmast%pan%n
    if ((x_pen.ge.ctrlmast%pan%bound%cur[1,ipan]).and.(x_pen.le.ctrlmast%pan%bound%cur[2,ipan])) then
       if ((y_pen.ge.ctrlmast%pan%bound%cur[3,ipan]).and.(y_pen.le.ctrlmast%pan%bound%cur[4,ipan])) then
          let ctrlmast%pan%icurr ipan
          gtvl\change directory "<greg<"'ctrlmast%pan%name[ctrlmast%pan%icurr]'
       endif
    endif
  next ipan
end procedure l-ctrl-master-wait-event
!
begin procedure l-ctrl-master-help
  say " "
  say "Position-independent actions:"
  say "   Press ? key: Display help"
  say "   Press E key: Exit loop"
  say "   Press H key: Hardcopy in ha subdirectory"
  say "   Middle click: toggle lens"
  @ l-ctrl-slave-help-position-independent
  say "Position-dependent actions:"
  @ l-ctrl-slave-help-position-dependent
  say " "
end procedure l-ctrl-master-help
!
begin procedure l-ctrl-master-hardcopy
  if .not.file(ha) then
     sic mkdir ha
  endif
  let ctrlmast%hardcopy%i ctrlmast%hardcopy%i+1
  define character myname*256
!  let myname "ha/"'myname'"-"'type'"-"'ctrlmast%hardcopy%i'
  let myname "ha/"'ctrlmast%hardcopy%i'
  gtvl\hardcopy 'myname' /dev epdf
  say " "
  say "PDF file available as "'myname'".pdf"
  say " "
end procedure l-ctrl-master-hardcopy
!
begin procedure l-ctrl-master-exit
  @ l-ctrl-slave-exit
  gtvl\change directory <greg
end procedure l-ctrl-master-exit
!
begin procedure l-ctrl-master-loop
  ! Interactive loop
  define logical loop
  let loop yes
  for /while loop
     @ l-ctrl-master-wait-event
     !
     ! First global behavior, then position-dependant behavior
     ! exa cursor_code
     if (cursor_code.eq."?") then
        @ l-ctrl-master-help
     else if (cursor_code.eq."&") then
        gtvl\lens
     else if (cursor_code.eq."E") then
        let loop no ! Exit the loop
     else if (cursor_code.eq."H") then
        @ l-ctrl-master-hardcopy
     else
        @ l-ctrl-slave-loop
        @ l-ctrl-master-action
     endif
  next ! loop
  @ l-ctrl-master-exit
end procedure l-ctrl-master-loop
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
@ l-ctrl-master-init
@ l-ctrl-master-action
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
