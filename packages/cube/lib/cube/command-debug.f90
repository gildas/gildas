!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cube_debug
  use cubetools_structure
  use cubetools_keywordlist_types
  use cubetools_parameters
  !
  use cubetools_cmessaging
  use cubeadm_messaging
  use cubecompute_messaging
  use cubedag_messaging
  use cubeedit_messaging
  use cubefit_messaging
  use cubefitsio_messaging
  use cubego_messaging
  use cubeio_messaging
  use cubemain_messaging
  use cubeset_messaging
  use cubesimulate_messaging
  use cubetools_messaging
  use cubetuple_messaging
  !
  public :: cube_debug_register
  private
  !
  type :: debug_library_t
    type(option_t),                  pointer :: opt
    type(keywordlist_comm_t),             pointer :: subtopic
    type(keywordlist_comm_t),             pointer :: onoff
    procedure(cube_debug_adm_parse), pointer :: parse => null()
  contains
    procedure :: register  => cube_debug_library_register
    procedure :: get_topic => cube_debug_library_get_topic
    procedure :: get_onoff => cube_debug_library_get_onoff
  end type debug_library_t
  !
  integer(kind=4), parameter :: nlib=13
  type :: debug_comm_t
     type(debug_library_t) :: lib(0:nlib)
  end type debug_comm_t
  type(debug_comm_t) :: comm
  !
  character(len=*), parameter :: onoff(2)  = ['ON ','OFF']
  !
contains
  !
  subroutine cube_debug_register(error) 
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    type(keywordlist_comm_t) :: keyarg
    character(len=*), parameter :: comm_abstract = &
         'Set which debugging messages are displayed'
    character(len=*), parameter :: comm_help = &
         'DEBUG without any argument or option will display which&
         & debug or trace messages are active. All debug messages&
         & for a topic can be turned ON or OFF at once if no option&
         & is provided. Also all debug messages for all topics can be&
         & turned ON or OFF if no option is given'
    character(len=1) :: empty(0)
    !
    character(len=*), parameter :: rname='DEBUG>REGISTER'
    !
    call cubemain_message(setseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'DEBUG','[ON|OFF]',&
         comm_abstract,&
         comm_help,&
         cube_debug_command,&
         comm%lib(0)%opt,error)
    if (error) return
    call keyarg%register( &
         'onoff',  &
         'Debug all messages ON or OFF', &
         strg_id,&
         code_arg_optional, &
         onoff, &
         .not.flexible, &
         comm%lib(0)%onoff,&
         error)
    if (error) return
    comm%lib(0)%parse => cube_debug_all
    !
    ! Register the /LIB* options. NB: topics TRACE, OTHERS, and * are implicit
    call comm%lib(1)%register ('ADM',    ['ALLOCATION'],cube_debug_adm_parse,error)
    if (error)  return
    call comm%lib(2)%register ('CUBE',   ['ALLOCATION'],cube_debug_main_parse,error)
    if (error)  return
    call comm%lib(3)%register ('COMPUTE',['ALLOCATION'],cube_debug_compute_parse,error)
    if (error)  return
    call comm%lib(4)%register ('EDIT',   empty,cube_debug_edit_parse,error)
    if (error)  return
    call comm%lib(5)%register ('FIT',    ['ALLOCATION'],cube_debug_fit_parse,error)
    if (error)  return
    call comm%lib(6)%register ('FITSIO', empty,cube_debug_fitsio_parse,error)
    if (error)  return
    call comm%lib(7)%register ('GO',     empty,cube_debug_go_parse,error)
    if (error)  return
    call comm%lib(8)%register ('IO',     ['ALLOCATION   ','TRANSPOSITION'],  &
                               cube_debug_io_parse,error)
    if (error)  return
    call comm%lib(9)%register ('SET',    empty,cube_debug_set_parse,error)
    if (error)  return
    call comm%lib(10)%register ('SIMULATE',['ALLOCATION'],cube_debug_simulate_parse,error)
    if (error)  return
    call comm%lib(11)%register('TOOLS',  empty,cube_debug_tools_parse,error)
    if (error)  return
    !
    ! 12th option: /MESSAGES
    call cubetools_register_option(&
         'MESSAGES','ON|OFF',&
         'Enable or disable messages in debug mode',&
         'If enabled, messages in debug mode will also show the calling &
         &subroutine name',&
         comm%lib(12)%opt,error)
    if (error) return
    call keyarg%register( &
         'ONOFF',  &
         'Debug messages ON or OFF', &
         strg_id,&
         code_arg_mandatory, &
         onoff, &
         .not.flexible, &
         comm%lib(12)%onoff,&
         error)
    if (error) return
    comm%lib(12)%parse => cube_debug_messages_parse
    !
    ! 13th option: /TRANSPOSITION
    call cubetools_register_option(&
         'TRANSPOSITION','ON|OFF',&
         'Enable or disable transposition messages',&
         strg_id,&
         comm%lib(13)%opt,error)
    if (error) return
    call keyarg%register( &
         'ONOFF',  &
         'Transposition messages ON or OFF', &
         strg_id,&
         code_arg_mandatory, &
         onoff, &
         .not.flexible, &
         comm%lib(13)%onoff,&
         error)
    if (error) return
    comm%lib(13)%parse => cube_debug_transposition_parse
    !
  end subroutine cube_debug_register
  !
  subroutine cube_debug_library_register(lib,name,topics,debug_parse,error)
    !-------------------------------------------------------------------
    ! Register a DEBUG /LIB* option
    !-------------------------------------------------------------------
    class(debug_library_t), intent(inout) :: lib
    character(len=*),       intent(in)    :: name
    character(len=*),       intent(in)    :: topics(:)
    procedure(cube_debug_adm_parse)       :: debug_parse
    logical,                intent(inout) :: error
    !
    type(keywordlist_comm_t) :: keyarg
    integer(kind=4) :: ltopic,ntopic
    character(len=:), allocatable :: alltopics(:)
    !
    ntopic = size(topics)
    ltopic = 6  ! For OTHERS
    if (ntopic.gt.0)  ltopic = max(ltopic,len(topics(1)))
    allocate(character(len=ltopic)::alltopics(ntopic+3))
    alltopics(1:ntopic) = topics
    alltopics(ntopic+1) = 'TRACE'
    alltopics(ntopic+2) = 'OTHERS'
    alltopics(ntopic+3) = '*'
    !
    call cubetools_register_option(&
         'LIB'//name,'Subtopic ON|OFF',&
         'Enable or disable messages for the '//name//' library',&
         strg_id,&
         lib%opt,error)
    if (error) return
    call keyarg%register( &
         'Subtopic',  &
         'Select subtopic for debug messages', &
         strg_id,&
         code_arg_mandatory, &
         alltopics, &
         .not.flexible, &
         lib%subtopic, &
         error)
    if (error) return
    call keyarg%register( &
         'Onoff',  &
         'Debug messages ON or OFF', &
         strg_id,&
         code_arg_mandatory, &
         onoff, &
         .not.flexible, &
         lib%onoff,&
         error)
    if (error) return
    !
    lib%parse => debug_parse
  end subroutine cube_debug_library_register
  !
  subroutine cube_debug_command(line,error)
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='DEBUG>COMMAND'
    !
    call cube_debug_parse(line,error)
    if (error) return
  end subroutine cube_debug_command
  !
  subroutine cube_debug_parse(line,error)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    ! Topics
    logical :: dolib(0:nlib)
    integer(kind=4) :: ilib
    character(len=*), parameter :: rname='DEBUG>PARSE'
    !
    ! /LIB* ?
    do ilib=0,nlib
      call comm%lib(ilib)%opt%present(line,dolib(ilib),error)
      if (error) return
    enddo
    !
    if (any(dolib(1:nlib))) then
      ! At least one option present
      do ilib=1,nlib
        if (dolib(ilib)) then
          call comm%lib(ilib)%parse(line,error)
          if (error)  return
        endif
      enddo
    else
      ! No option present
      if (comm%lib(0)%opt%getnarg().eq.0) then
        ! No argument to the command
        call cube_debug_print(error)
        if (error)  return
      else
        call comm%lib(0)%parse(line,error)
        if (error)  return
      endif
    endif
  end subroutine cube_debug_parse
  !
  subroutine cube_debug_library_get_topic(lib,line,keyword,error)
    !-------------------------------------------------------------------
    ! Retrieve the topic passed to the option (1st argument by
    ! defintion)
    !-------------------------------------------------------------------
    class(debug_library_t), intent(in)    :: lib
    character(len=*),       intent(in)    :: line
    character(len=*),       intent(out)   :: keyword
    logical,                intent(inout) :: error
    !
    character(len=argu_l) :: argum
    integer(kind=4) :: ikey
    !
    call cubetools_getarg(line,lib%opt,1,argum,mandatory,error)
    if (error) return
    call cubetools_keywordlist_user2prog(lib%subtopic,argum,ikey,keyword,error)
    if (error) return
  end subroutine cube_debug_library_get_topic
  !
  subroutine cube_debug_library_get_onoff(lib,line,turnon,error)
    !-------------------------------------------------------------------
    ! Retrieve the on|off status passed to the option (last argument by
    ! defintion)
    !-------------------------------------------------------------------
    class(debug_library_t), intent(in)    :: lib
    character(len=*),       intent(in)    :: line
    logical,                intent(out)   :: turnon
    logical,                intent(inout) :: error
    !
    character(len=argu_l) :: argum,keyword
    integer(kind=4) :: ikey
    !
    call cubetools_getarg(line,lib%opt,lib%opt%getnarg(),argum,mandatory,error)
    if (error) return
    call cubetools_keywordlist_user2prog(lib%onoff,argum,ikey,keyword,error)
    if (error) return
    turnon = keyword.eq.'ON'
  end subroutine cube_debug_library_get_onoff
  !
  subroutine cube_debug_all(lib,line,error)
    !---------------------------------------------------------------------
    ! Support routine for command
    ! DEBUG  ON|OFF
    !---------------------------------------------------------------------
    class(debug_library_t), intent(in)    :: lib
    character(len=*),       intent(in)    :: line
    logical,                intent(inout) :: error
    !
    logical :: turnon
    !
    call lib%get_onoff(line,turnon,error)
    if (error)  return
    !
!!$    call cubedag_message_set_trace(turnon)
!!$    call cubedag_message_set_others(turnon)
    !
    call cubego_message_set_trace(turnon)
    call cubego_message_set_others(turnon)
    !
    call cubefitsio_message_set_trace(turnon)
    call cubefitsio_message_set_others(turnon)
    !
    call cubeio_message_set_alloc(turnon)
    call cubeio_message_set_trans(turnon)
    call cubeio_message_set_trace(turnon)
    call cubeio_message_set_others(turnon)
    !
    call cubeadm_message_set_alloc(turnon)
    call cubeadm_message_set_trace(turnon)
    call cubeadm_message_set_others(turnon)
    !
    call cubemain_message_set_alloc(turnon)
    call cubemain_message_set_trace(turnon)
    call cubemain_message_set_others(turnon)
    !
    call cubeset_message_set_trace(turnon)
    call cubeset_message_set_others(turnon)
    !
    call cubetools_message_set_alloc(turnon)
    call cubetools_message_set_trace(turnon)
    call cubetools_message_set_others(turnon)
    !
    call cubefit_message_set_alloc(turnon)
    call cubefit_message_set_trace(turnon)
    call cubefit_message_set_others(turnon)
    !
    call cubeedit_message_set_trace(turnon)
    call cubeedit_message_set_others(turnon)
    !
    call cubetools_cmessaging_debug(turnon)
  end subroutine cube_debug_all
  !
  subroutine cube_debug_adm_parse(lib,line,error)
    !---------------------------------------------------------------------
    ! Support routine for command
    !   DEBUG /LIBADM Subtopic ON|OFF
    !---------------------------------------------------------------------
    class(debug_library_t), intent(in)    :: lib
    character(len=*),       intent(in)    :: line
    logical,                intent(inout) :: error
    !
    character(len=argu_l) :: keyword
    logical :: turnon
    character(len=*), parameter :: rname='DEBUG>ADM>PARSE'
    !
    call lib%get_topic(line,keyword,error)
    if (error)  return
    call lib%get_onoff(line,turnon,error)
    if (error)  return
    !
    select case (keyword)
    case (strg_star)
      call cubeadm_message_set_trace(turnon)
      call cubeadm_message_set_alloc(turnon)
      call cubeadm_message_set_others(turnon)
    case('ALLOCATION')
      call cubeadm_message_set_alloc(turnon)
    case ('TRACE')
      call cubeadm_message_set_trace(turnon)
    case ('OTHERS')
      call cubeadm_message_set_others(turnon)
    case default
      call cubemain_message(seve%w,rname,"Subtopic "//trim(keyword)//" not available for this topic")
    end select
    if (error) return
  end subroutine cube_debug_adm_parse
  !
  subroutine cube_debug_compute_parse(lib,line,error)
    !---------------------------------------------------------------------
    ! Support routine for command
    !   DEBUG /LIBCOMPUTE Subtopic ON|OFF
    !---------------------------------------------------------------------
    class(debug_library_t), intent(in)    :: lib
    character(len=*),       intent(in)    :: line
    logical,                intent(inout) :: error
    !
    character(len=argu_l) :: keyword
    logical :: turnon
    character(len=*), parameter :: rname='DEBUG>COMPUTE>PARSE'
    !
    call lib%get_topic(line,keyword,error)
    if (error)  return
    call lib%get_onoff(line,turnon,error)
    if (error)  return
    !
    select case (keyword)
    case (strg_star)
      call cubecompute_message_set_trace(turnon)
      call cubecompute_message_set_alloc(turnon)
      call cubecompute_message_set_others(turnon)
    case('ALLOCATION')
      call cubecompute_message_set_alloc(turnon)
    case ('TRACE')
      call cubecompute_message_set_trace(turnon)
    case ('OTHERS')
      call cubecompute_message_set_others(turnon)
    case default
      call cubemain_message(seve%w,rname,"Subtopic "//trim(keyword)//" not available for this topic")
    end select
    if (error) return
  end subroutine cube_debug_compute_parse
  !
  subroutine cube_debug_dag_parse(lib,line,error)
    !---------------------------------------------------------------------
    ! Support routine for command
    !   DEBUG /LIBDAG Subtopic ON|OFF
    !---------------------------------------------------------------------
    class(debug_library_t), intent(in)    :: lib
    character(len=*),       intent(in)    :: line
    logical,                intent(inout) :: error
    !
    character(len=argu_l) :: keyword
    logical :: turnon
    character(len=*), parameter :: rname='DEBUG>DAG>PARSE'
    !
    call lib%get_topic(line,keyword,error)
    if (error)  return
    call lib%get_onoff(line,turnon,error)
    if (error)  return
    !
!!$    select case (keyword)
!!$    case (strg_star)
!!$       call cubedag_message_set_trace(turnon)
!!$       call cubedag_message_set_others(turnon)
!!$    case ('TRACE')
!!$       call cubedag_message_set_trace(turnon)
!!$    case ('OTHERS')
!!$       call cubedag_message_set_others(turnon)
!!$    case default
!!$       call cubemain_message(seve%w,rname,"Subtopic "//trim(keyword)//" not available for this topic")
!!$    end select
!!$    if (error) return
  end subroutine cube_debug_dag_parse
  !
  subroutine cube_debug_fitsio_parse(lib,line,error)
    !---------------------------------------------------------------------
    ! Support routine for command
    !   DEBUG /LIBFITSIO Subtopic ON|OFF
    !---------------------------------------------------------------------
    class(debug_library_t), intent(in)    :: lib
    character(len=*),       intent(in)    :: line
    logical,                intent(inout) :: error
    !
    character(len=argu_l) :: keyword
    logical :: turnon
    character(len=*), parameter :: rname='DEBUG>FITSIO>PARSE'
    !
    call lib%get_topic(line,keyword,error)
    if (error)  return
    call lib%get_onoff(line,turnon,error)
    if (error)  return
    !
    select case (keyword)
    case (strg_star)
      call cubefitsio_message_set_trace(turnon)
      call cubefitsio_message_set_others(turnon)
    case ('TRACE')
      call cubefitsio_message_set_trace(turnon)
    case ('OTHERS')
      call cubefitsio_message_set_others(turnon)
    case default
      call cubemain_message(seve%w,rname,"Subtopic "//trim(keyword)//" not available for this topic")
    end select
    if (error) return
  end subroutine cube_debug_fitsio_parse
  !
  subroutine cube_debug_go_parse(lib,line,error)
    !---------------------------------------------------------------------
    ! Support routine for command
    !   DEBUG /LIBGO Subtopic ON|OFF
    !---------------------------------------------------------------------
    class(debug_library_t), intent(in)    :: lib
    character(len=*),       intent(in)    :: line
    logical,                intent(inout) :: error
    !
    character(len=argu_l) :: keyword
    logical :: turnon
    character(len=*), parameter :: rname='DEBUG>GO>PARSE'
    !
    call lib%get_topic(line,keyword,error)
    if (error)  return
    call lib%get_onoff(line,turnon,error)
    if (error)  return
    !
    select case (keyword)
    case (strg_star)
      call cubego_message_set_trace(turnon)
      call cubego_message_set_others(turnon)
    case ('TRACE')
      call cubego_message_set_trace(turnon)
    case ('OTHERS')
      call cubego_message_set_others(turnon)
    case default
      call cubemain_message(seve%w,rname,"Subtopic "//trim(keyword)//" not available for this topic")
    end select
    if (error) return
  end subroutine cube_debug_go_parse
  !
  subroutine cube_debug_io_parse(lib,line,error)
    !---------------------------------------------------------------------
    ! Support routine for command
    !   DEBUG /LIBIO Subtopic ON|OFF
    !---------------------------------------------------------------------
    class(debug_library_t), intent(in)    :: lib
    character(len=*),       intent(in)    :: line
    logical,                intent(inout) :: error
    !
    character(len=argu_l) :: keyword
    logical :: turnon
    character(len=*), parameter :: rname='DEBUG>IO>PARSE'
    !
    call lib%get_topic(line,keyword,error)
    if (error)  return
    call lib%get_onoff(line,turnon,error)
    if (error)  return
    !
    select case (keyword)
    case (strg_star)
      call cubeio_message_set_alloc(turnon)
      call cubeio_message_set_trans(turnon)
      call cubeio_message_set_trace(turnon)
      call cubeio_message_set_others(turnon)
    case ('ALLOCATION')
      call cubeio_message_set_alloc(turnon)
    case ('TRANSPOSITION')
      call cubeio_message_set_trans(turnon)
    case ('TRACE')
      call cubeio_message_set_trace(turnon)
    case ('OTHERS')
      call cubeio_message_set_others(turnon)
    case default
      call cubemain_message(seve%w,rname,"Subtopic "//trim(keyword)//" not available for this topic")
    end select
    if (error) return
  end subroutine cube_debug_io_parse
  !
  subroutine cube_debug_main_parse(lib,line,error)
    !---------------------------------------------------------------------
    ! Support routine for command
    !   DEBUG /LIBCUBE Subtopic ON|OFF
    !---------------------------------------------------------------------
    class(debug_library_t), intent(in)    :: lib
    character(len=*),       intent(in)    :: line
    logical,                intent(inout) :: error
    !
    character(len=argu_l) :: keyword
    logical :: turnon
    character(len=*), parameter :: rname='DEBUG>CUBE>PARSE'
    !
    call lib%get_topic(line,keyword,error)
    if (error)  return
    call lib%get_onoff(line,turnon,error)
    if (error)  return
    !
    select case (keyword)
    case (strg_star)
      call cubemain_message_set_alloc(turnon)
      call cubemain_message_set_trace(turnon)
      call cubemain_message_set_others(turnon)
    case ('ALLOCATION')
      call cubemain_message_set_alloc(turnon)
    case ('TRACE')
      call cubemain_message_set_trace(turnon)
    case ('OTHERS')
      call cubemain_message_set_others(turnon)
    case default
      call cubemain_message(seve%w,rname,"Subtopic "//trim(keyword)//" not available for this topic")
    end select
    if (error) return
  end subroutine cube_debug_main_parse
  !
  subroutine cube_debug_fit_parse(lib,line,error)
    !---------------------------------------------------------------------
    ! Support routine for command
    !   DEBUG /LIBFIT Subtopic ON|OFF
    !---------------------------------------------------------------------
    class(debug_library_t), intent(in)    :: lib
    character(len=*),       intent(in)    :: line
    logical,                intent(inout) :: error
    !
    character(len=argu_l) :: keyword
    logical :: turnon
    character(len=*), parameter :: rname='DEBUG>FIT>PARSE'
    !
    call lib%get_topic(line,keyword,error)
    if (error)  return
    call lib%get_onoff(line,turnon,error)
    if (error)  return
    !
    select case (keyword)
    case (strg_star)
      call cubefit_message_set_alloc(turnon)
      call cubefit_message_set_trace(turnon)
      call cubefit_message_set_others(turnon)
    case ('ALLOCATION')
      call cubefit_message_set_alloc(turnon)
    case ('TRACE')
      call cubefit_message_set_trace(turnon)
    case ('OTHERS')
      call cubefit_message_set_others(turnon)
    case default
      call cubemain_message(seve%w,rname,"Subtopic "//trim(keyword)//" not available for this topic")
    end select
    if (error) return
  end subroutine cube_debug_fit_parse
  !
  subroutine cube_debug_edit_parse(lib,line,error)
    !---------------------------------------------------------------------
    ! Support routine for command
    !   DEBUG /LIBEDIT Subtopic ON|OFF
    !---------------------------------------------------------------------
    class(debug_library_t), intent(in)    :: lib
    character(len=*),       intent(in)    :: line
    logical,                intent(inout) :: error
    !
    character(len=argu_l) :: keyword
    logical :: turnon
    character(len=*), parameter :: rname='DEBUG>EDIT>PARSE'
    !
    call lib%get_topic(line,keyword,error)
    if (error)  return
    call lib%get_onoff(line,turnon,error)
    if (error)  return
    !
    select case (keyword)
    case (strg_star)
      call cubeedit_message_set_trace(turnon)
      call cubeedit_message_set_others(turnon)
    case ('TRACE')
      call cubeedit_message_set_trace(turnon)
    case ('OTHERS')
      call cubeedit_message_set_others(turnon)
    case default
      call cubemain_message(seve%w,rname,"Subtopic "//trim(keyword)//" not available for this topic")
    end select
    if (error) return
  end subroutine cube_debug_edit_parse
  !
  subroutine cube_debug_set_parse(lib,line,error)
    !---------------------------------------------------------------------
    ! Support routine for command
    !   DEBUG /LIBSET [subtopic] ON|OFF
    !---------------------------------------------------------------------
    class(debug_library_t), intent(in)    :: lib
    character(len=*),       intent(in)    :: line
    logical,                intent(inout) :: error
    !
    character(len=argu_l) :: keyword
    logical :: turnon
    character(len=*), parameter :: rname='DEBUG>SET>PARSE'
    !
    call lib%get_topic(line,keyword,error)
    if (error)  return
    call lib%get_onoff(line,turnon,error)
    if (error)  return
    !
    select case (keyword)
    case (strg_star)
      call cubeset_message_set_trace(turnon)
      call cubeset_message_set_others(turnon)
    case ('TRACE')
      call cubeset_message_set_trace(turnon)
    case ('OTHERS')
      call cubeset_message_set_others(turnon)
    case default
      call cubemain_message(seve%w,rname,"Subtopic "//trim(keyword)//" not available for this topic")
    end select
    if (error) return
  end subroutine cube_debug_set_parse
  !
  subroutine cube_debug_simulate_parse(lib,line,error)
    !---------------------------------------------------------------------
    ! Support routine for command
    !   DEBUG /LIBSIMULATE Subtopic ON|OFF
    !---------------------------------------------------------------------
    class(debug_library_t), intent(in)    :: lib
    character(len=*),       intent(in)    :: line
    logical,                intent(inout) :: error
    !
    character(len=argu_l) :: keyword
    logical :: turnon
    character(len=*), parameter :: rname='DEBUG>SIMULATE>PARSE'
    !
    call lib%get_topic(line,keyword,error)
    if (error)  return
    call lib%get_onoff(line,turnon,error)
    if (error)  return
    !
    select case (keyword)
    case (strg_star)
      call cubesimulate_message_set_trace(turnon)
      call cubesimulate_message_set_alloc(turnon)
      call cubesimulate_message_set_others(turnon)
    case('ALLOCATION')
      call cubesimulate_message_set_alloc(turnon)
    case ('TRACE')
      call cubesimulate_message_set_trace(turnon)
    case ('OTHERS')
      call cubesimulate_message_set_others(turnon)
    case default
      call cubemain_message(seve%w,rname,"Subtopic "//trim(keyword)//" not available for this topic")
    end select
    if (error) return
  end subroutine cube_debug_simulate_parse
  !
  subroutine cube_debug_tools_parse(lib,line,error)
    !---------------------------------------------------------------------
    ! Support routine for command
    !   DEBUG /LIBTOOLS Subtopic ON|OFF
    !---------------------------------------------------------------------
    class(debug_library_t), intent(in)    :: lib
    character(len=*),       intent(in)    :: line
    logical,                intent(inout) :: error
    !
    character(len=argu_l) :: keyword
    logical :: turnon
    character(len=*), parameter :: rname='DEBUG>TOOLS>PARSE'
    !
    call lib%get_topic(line,keyword,error)
    if (error)  return
    call lib%get_onoff(line,turnon,error)
    if (error)  return
    !
    select case (keyword)
    case (strg_star)
      call cubetools_message_set_alloc(turnon)
      call cubetools_message_set_trace(turnon)
      call cubetools_message_set_others(turnon)
    case ('ALLOCATION')
      call cubetools_message_set_alloc(turnon)
    case ('TRACE')
      call cubetools_message_set_trace(turnon)
    case ('OTHERS')
      call cubetools_message_set_others(turnon)
    case default
      call cubemain_message(seve%w,rname,"Subtopic "//trim(keyword)//" not available for this topic")
    end select
    if (error) return
  end subroutine cube_debug_tools_parse
  !
  subroutine cube_debug_messages_parse(lib,line,error)
    !---------------------------------------------------------------------
    ! Support routine for command
    ! DEBUG /MESSAGES ON|OFF
    !---------------------------------------------------------------------
    class(debug_library_t), intent(in)    :: lib
    character(len=*),       intent(in)    :: line
    logical,                intent(inout) :: error
    !
    logical :: turnon
    !
    call lib%get_onoff(line,turnon,error)
    if (error)  return
    !
    call cubetools_cmessaging_debug(turnon)
  end subroutine cube_debug_messages_parse
  !
  subroutine cube_debug_transposition_parse(lib,line,error)
    !---------------------------------------------------------------------
    ! Support routine for command
    ! DEBUG /TRANSPOSITION ON|OFF
    !---------------------------------------------------------------------
    class(debug_library_t), intent(in)    :: lib
    character(len=*),       intent(in)    :: line
    logical,                intent(inout) :: error
    !
    logical :: turnon
    !
    call lib%get_onoff(line,turnon,error)
    if (error)  return
    !
    call cubeio_message_set_trans(turnon)
    call cubetuple_message_set_trans(turnon)
  end subroutine cube_debug_transposition_parse
  !
  subroutine cube_debug_print(error)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    character(len=*), parameter :: rname='DEBUG>PRINT'
    character(len=mess_l) :: mess
    !
    call cubemain_message(seve%r,rname,'  Debug')
    write(mess,'(A,4(A,L))')  '    IO    : ',  &
          'Allocation ',cubeio_message_get_alloc(),  &
          ', Transposition ',cubeio_message_get_trans(), &
          ', Trace ',cubeio_message_get_trace(), &
          ', Others ',cubeio_message_get_others()
    call cubemain_message(seve%r,rname,mess)
    write(mess,'(3(A,L))')  '    ADM   : Trace ',cubeadm_message_get_trace(),  &
          ', Allocation ',cubeadm_message_get_alloc(),', Others ',cubeadm_message_get_others()
    call cubemain_message(seve%r,rname,mess)
    write(mess,'(3(A,L))')  '    TOOLS : Allocation ',cubetools_message_get_alloc(),  &
          ', Trace ',cubetools_message_get_trace(), &
          ', Others ',cubetools_message_get_others()
    call cubemain_message(seve%r,rname,mess)
    !
    write(mess,'(3(A,L))')  '    CUBE  : Allocation ',cubemain_message_get_alloc(),  &
          ', Trace ',cubemain_message_get_trace(), &
          ', Others ',cubemain_message_get_others()
    call cubemain_message(seve%r,rname,mess)
    !
    write(mess,'(3(A,L))')  '    FIT   : Allocation ',cubefit_message_get_alloc(),  &
          ', Trace ',cubefit_message_get_trace(), &
          ', Others ',cubefit_message_get_others()
    call cubemain_message(seve%r,rname,mess)
    !
    write(mess,'(2(A,L))')  '    EDIT  : Trace ',cubeedit_message_get_trace(),  &
          ', Others ',cubeedit_message_get_others()
    call cubemain_message(seve%r,rname,mess)
    !
    write(mess,'(2(A,L))')  '    SET   : Trace ',cubeset_message_get_trace(),  &
          ', Others ',cubeset_message_get_others()
    call cubemain_message(seve%r,rname,mess)
    !
    write(mess,'(2(A,L))')  '    GO    : Trace ',cubego_message_get_trace(),  &
          ', Others ',cubego_message_get_others()
    call cubemain_message(seve%r,rname,mess)
    !
    write(mess,'(2(A,L))')  '    FITSIO: Trace ',cubefitsio_message_get_trace(),  &
          ', Others ',cubefitsio_message_get_others()
    call cubemain_message(seve%r,rname,mess)
    !
    ! write(mess,'(2(A,L))')  '    DAG: Trace ',cubedag_message_get_trace(),  &
    !      ', Others ',cubedag_message_get_others()
    ! call cubemain_message(seve%r,rname,mess)
  end subroutine cube_debug_print
  !
end module cube_debug
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
