!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!--------------------------------------------------------------------------
! Routines to manage the CUBE package
!--------------------------------------------------------------------------
!
module cube_package
  use gkernel_interfaces ! Absolutely needs to be here
  !
  public :: cube_pack_init,cube_pack_on_exit
  private
  !
contains
  !
  subroutine cube_pack_init(gpack_id,error)
    use sic_def ! Definition of backslash
    !
    use cubetools_parameters
    use cubetools_nan
    use cubetools_unit_setup
    use cubetools_structure
    use cubeadm_library
    use cubeset_init
    use cubego_init
    !
    use cubeadm_language
    use cubecompute_language
    use cubeedit_language
    use cubefield_language
    use cubefit_language
    use cubego_language
    use cubehelp_language
    use cubehistogram_language
    use cubemain_language
    use cubemask_language
    use cubeset_language
    use cubesimulate_language
    use cubestatistics_language
    use cubetemplate_language
    !
    use cube_messaging
    use cubeadm_messaging
    use cubecompute_messaging
    use cubedag_messaging
    use cubeedit_messaging
    use cubefield_messaging
    use cubefit_messaging
    use cubefitsio_messaging
    use cubego_messaging
    use cubehistogram_messaging
    use cubeio_messaging
    use cubemain_messaging
    use cubemask_messaging
    use cubeobsolete_messaging
    use cubeset_messaging
    use cubesimulate_messaging
    use cubesyntax_messaging
    use cubestatistics_messaging
    use cubetemplate_messaging
    use cubetopology_messaging
    use cubetools_messaging
    use cubetuple_messaging
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    integer(kind=4), intent(in)    :: gpack_id
    logical,         intent(inout) :: error
    !
    logical :: devcube
    !
    devcube = cube_dev_mode()
    !
    ! CUBE structure
    if (.not.sic_varexist('cube'))  call sic_defstructure('cube',.true.,error)
    !
    ! Library initializations (must come after CUBE structure)
    call cubetools_nan_init()   ! IEEE NaNs
    call unitbuffer%init(error) ! Prog and user units
    if (error) return
    call cubeadm_library_init(error)
    if (error) return
    call cubeset_library_init(error)
    if (error) return
    call cubego_library_init(error)
    if (error) return
    !
    ! Language initializations (must come after libraries)
    call cubetools_register_package('CUBE','CUBE package',error)
    if (error) return
    ! Help language first
    call cubehelp_register_language(error)
    if (error) return
    ! Then administrative languages
    call cubeadm_register_language(error)
    if (error) return
    call cubeedit_register_language(error)
    if (error) return
    call cubego_register_language(error)
    if (error) return
    call cubeset_register_language(error)
    if (error) return
    ! Application languages by alphabetical order
    call cubecompute_register_language(error)
    if (error) return
    call cubemain_register_language(error) ! The cubemain library delivers the CUBE language ;-).
    if (error) return
    call cubemask_register_language(error)
    if (error) return
    call cubefield_register_language(error)
    if (error) return
    call cubefit_register_language(error)
    if (error) return
    call cubehistogram_register_language(error)
    if (error) return
    call cubestatistics_register_language(error)
    if (error) return
    if (devcube) then
       call cubesimulate_register_language(error)
       if (error) return    
       call cubetemplate_register_language(error)
       if (error) return
    endif
    !
    ! One time initialization by alphabetical order
    call classic_message_set_id(gpack_id)
    call cubeadm_message_set_id(gpack_id)
    call cubecompute_message_set_id(gpack_id)
    call cubedag_message_set_id(gpack_id)
    call cubeedit_message_set_id(gpack_id)
    call cubefield_message_set_id(gpack_id)
    call cubefit_message_set_id(gpack_id)
    call cubefitsio_message_set_id(gpack_id)
    call cubego_message_set_id(gpack_id)
    call cubehistogram_message_set_id(gpack_id)
    call cubeio_message_set_id(gpack_id)
    call cubemain_message_set_id(gpack_id)
    call cubemask_message_set_id(gpack_id)
    call cubeobsolete_message_set_id(gpack_id)
    if (devcube) call cubesimulate_message_set_id(gpack_id)
    call cubestatistics_message_set_id(gpack_id)
    call cubeset_message_set_id(gpack_id)
    call cubesyntax_message_set_id(gpack_id)
    if (devcube) call cubetemplate_message_set_id(gpack_id)
    call cubetopology_message_set_id(gpack_id)
    call cubetools_message_set_id(gpack_id)
    call cubetuple_message_set_id(gpack_id)
    call cube_message_set_id(gpack_id)
    !
    ! Priorities:
    !   ADM\IMPORT         > SIC\IMPORT
    !   ADM\HISTORY        > HISTOGRAM\HISTO1D
    !   CUBE\MODIFY        > SIC\MODIFY
    !   COMPUTE\DIFFERENCE > SIC\DIFF
    !   SIC\QUIT           > STATISTICS\QUANTILE
    call exec_program('sic'//backslash//'sic priority '//    &
                      '1 help edit go adm set cube compute '//       &
                      '2 sic fit field histogram '// &
                      '3 statistics')
  end subroutine cube_pack_init
  !
  subroutine cube_pack_on_exit(error)
    use cubeadm_library
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    call cubeadm_library_exit(error)
    if (error) continue
  end subroutine cube_pack_on_exit
end module cube_package
!
subroutine cube_pack_set(pack)
  use gpack_def
  use gkernel_interfaces
  use cube_package
  !----------------------------------------------------------------------
  ! Can not be in a module as it is called by cube-pyimport.c and
  ! cube-sicimport.c
  !----------------------------------------------------------------------
  type(gpack_info_t), intent(out) :: pack
  !
  pack%name='cube'
  pack%ext = '.cube'
  pack%depend(1:1) = (/ locwrd(greg_pack_set) /)
  pack%init=locwrd(cube_pack_init)
  pack%on_exit=locwrd(cube_pack_on_exit)
  pack%authors='J.Pety, S.Bardeau, V.deSouzaMagalhaes'
end subroutine cube_pack_set
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
