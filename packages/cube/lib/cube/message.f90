!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage CUBE messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cube_messaging
  use gpack_def
  use gbl_message
  use cubetools_parameters
  !
  public :: seve,mess_l
  public :: cube_message_set_id,cube_message
  private
  !
  ! Identifier used for message identification
  integer(kind=4) :: cube_message_id = gpack_global_id  ! Default value for startup message
  !
contains
  !
  subroutine cube_message_set_id(id)
    !---------------------------------------------------------------------
    ! Alter library id into input id. Should be called by the library
    ! which wants to share its id with the current one.
    !---------------------------------------------------------------------
    integer(kind=4), intent(in) :: id
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='MESSAGE>SET>ID'
    !
    cube_message_id = id
    write (mess,'(A,I3)') 'Now use id #',cube_message_id
    call cube_message(seve%d,rname,mess)
  end subroutine cube_message_set_id
  !
  subroutine cube_message(mkind,procname,message)
    use cubetools_cmessaging
    !---------------------------------------------------------------------
    ! Messaging facility for the current library. Calls the low-level
    ! (internal) messaging routine with its own identifier.
    !---------------------------------------------------------------------
    integer(kind=4),  intent(in) :: mkind    ! Message kind
    character(len=*), intent(in) :: procname ! Name of calling procedure
    character(len=*), intent(in) :: message  ! Message string
    !
    call cubetools_cmessage(cube_message_id,mkind,'CUBE>'//procname,message)
  end subroutine cube_message
end module cube_messaging
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
