!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubehelp_question
  use cubetools_parameters
  use cubetools_structure
  use cube_messaging
  !
  public :: cubehelp_question_register
  private
  !
  type :: question_comm_t
     type(option_t), pointer :: question ! Option #0
  end type question_comm_t
  type(question_comm_t) :: comm
  !
contains
  !
  subroutine cubehelp_question_register(error) 
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    character(len=*), parameter :: rname='QUESTION>REGISTER'
    !
    call cube_message(seve%t,rname,'Welcome')
    !
    call cubetools_register_command(&
         '?','',&
         'List known languages',&
         strg_id,&
         cubehelp_question_command,&
         comm%question,error)
    if (error) return
  end subroutine cubehelp_question_register
  !
  subroutine cubehelp_question_command(line,error)
    use cubetools_structure
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='QUESTION>COMMAND'
    !
    call cubetools_list_package_languages(error)
    if (error) return
  end subroutine cubehelp_question_command
end module cubehelp_question
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
