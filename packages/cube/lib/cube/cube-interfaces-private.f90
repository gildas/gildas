module cube_interfaces_private
  interface
    subroutine cube_setup_command(line,error)
      use cubeadm_messaging
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !   CUBE\SETUP BUFFERING|DEBUG|OUTPUT|TIMING [Args]
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      logical,          intent(inout) :: error
    end subroutine cube_setup_command
  end interface
  !
  interface
    subroutine cube_setup_print(error)
      use cubeadm_messaging
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine cube_setup_print
  end interface
  !
  interface
    subroutine cube_setup_debug_parse(line,error)
      use cubeadm_messaging
      !---------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      ! SETUP DEBUG [Topic] ON|OFF
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      logical,          intent(inout) :: error
    end subroutine cube_setup_debug_parse
  end interface
  !
  interface
    subroutine cube_setup_debug_print(error)
      use cubetools_messaging
      use cubeadm_messaging
      use cubedag_messaging
      use cubego_messaging
      use cubeio_messaging
      use cubemain_messaging
      use cubeset_messaging
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine cube_setup_debug_print
  end interface
  !
  interface
    subroutine cube_load_adm(error)
      !----------------------------------------------------------------------
      ! @ private
      ! Initialize the ADM\ language
      !----------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine cube_load_adm
  end interface
  !
  interface
    subroutine cube_run_adm(line,comm,error)
      use gbl_message
      use cubeadm_directory
      !----------------------------------------------------------------------
      ! @ private
      ! Support routine for the ADM\ language
      !----------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      character(len=*), intent(in)    :: comm
      logical,          intent(inout) :: error
    end subroutine cube_run_adm
  end interface
  !
  interface
    subroutine cube_load_go(error)
      !----------------------------------------------------------------------
      ! @ private
      ! Initialize the GO\ language
      !----------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine cube_load_go
  end interface
  !
  interface
    subroutine cube_run_go(line,comm,error)
      use gbl_message
      use cubego_consistency
      !----------------------------------------------------------------------
      ! @ private
      ! Support routine for the GO\ language
      !----------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      character(len=*), intent(in)    :: comm
      logical,          intent(inout) :: error
    end subroutine cube_run_go
  end interface
  !
  interface
    subroutine cube_load_cube(error)
      !----------------------------------------------------------------------
      ! @ private
      ! Initialize the CUBE\ language
      !----------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine cube_load_cube
  end interface
  !
  interface
    subroutine cube_run_cube(line,comm,error)
      use gbl_message
      use cubeadm_timing
      !----------------------------------------------------------------------
      ! @ private
      ! Support routine for the CUBE\ language
      !----------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      character(len=*), intent(in)    :: comm
      logical,          intent(inout) :: error
    end subroutine cube_run_cube
  end interface
  !
  interface
    subroutine cube_load_set(error)
      !----------------------------------------------------------------------
      ! @ private
      ! Initialize the SET\ language
      !----------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine cube_load_set
  end interface
  !
  interface
    subroutine cube_run_set(line,comm,error)
      use gbl_message
      use cubeset_filename
      use cubeset_get
      use cubeset_header
      use cubeset_load
      use cubeset_maxbox
      use cubeset_panel
      use cubeset_range
      use cubeset_spatial
      use cubeset_spectral
      use cubeset_topology
      !----------------------------------------------------------------------
      ! @ private
      ! Support routine for the SET\ language
      !----------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      character(len=*), intent(in)    :: comm
      logical,          intent(inout) :: error
    end subroutine cube_run_set
  end interface
  !
  interface
    subroutine cube_message(mkind,procname,message)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! Messaging facility for the current library. Calls the low-level
      ! (internal) messaging routine with its own identifier.
      !---------------------------------------------------------------------
      integer(kind=4),  intent(in) :: mkind     ! Message kind
      character(len=*), intent(in) :: procname  ! Name of calling procedure
      character(len=*), intent(in) :: message   ! Message string
    end subroutine cube_message
  end interface
  !
  interface
    subroutine cube_pack_set(pack)
      use gpack_def
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(gpack_info_t), intent(out) :: pack
    end subroutine cube_pack_set
  end interface
  !
  interface
    subroutine cube_pack_init(gpack_id,error)
      use sic_def ! Definition of backslash
      use cubetools_nan
      use cubetools_messaging
      use cubeio_messaging
      use cubetuple_messaging
      use cubedag_messaging
      use cubeadm_messaging
      use cubemain_messaging
      use cubeset_messaging
      use cubego_messaging
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      integer(kind=4), intent(in)    :: gpack_id
      logical,         intent(inout) :: error
    end subroutine cube_pack_init
  end interface
  !
  interface
    subroutine cube_pack_on_exit(error)
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine cube_pack_on_exit
  end interface
  !
end module cube_interfaces_private
