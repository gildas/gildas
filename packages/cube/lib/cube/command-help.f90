!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubehelp_help
  use sic_types
  use cubetools_parameters
  use cubetools_structure
  use cube_messaging
  !
  public :: cubehelp_help_register
  private
  !
  type :: help_comm_t
    type(option_t), pointer :: help     ! Option #0
  end type help_comm_t
  type(help_comm_t) :: comm
  !
  type :: help_user_t
    character(len=command_length) :: topic
  end type help_user_t
  type(help_user_t) :: user
  !
  type :: help_prog_t
    character(len=command_length) :: uptopic
  end type help_prog_t
  type(help_prog_t) :: prog
  !
contains
  !
  subroutine cubehelp_help_register(error)
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    type(standard_arg_t) :: stdarg
    character(len=*), parameter :: rname='HELP>REGISTER'
    !
    call cube_message(seve%t,rname,'Welcome')
    !
    call cubetools_register_command(&
         'HELP','[COMMAND [/OPTION]]',&
         'Get help for commands and options',&
         strg_id,&
         cubehelp_help_command,&
         comm%help,error,&
         allowopt=.false.)
    if (error) return
    call stdarg%register( &
         'COMMAND',  &
         'Command name', &
         strg_id,&
         code_arg_optional, &
         error)
    if (error) return
    call stdarg%register( &
         'OPTION',  &
         'Option name', &
         strg_id,&
         code_arg_optional, &
         error)
    if (error) return
    !
  end subroutine cubehelp_help_register
  !
  subroutine cubehelp_help_command(line,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    call cubehelp_help_parse(line,user,error)
    if (error) return
    call cubehelp_help_main(line,user,prog,error)
    if (error) return
  end subroutine cubehelp_help_command
  !
  subroutine cubehelp_help_parse(line,user,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    character(len=*),  intent(in)    :: line
    type(help_user_t), intent(out)   :: user
    logical,           intent(inout) :: error
    !
    user%topic = strg_emp
    call cubetools_getarg(line,comm%help,1,user%topic,.not.mandatory,error)
    if (error) return
  end subroutine cubehelp_help_parse
  !
  subroutine cubehelp_help_main(line,user,prog,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    character(len=*),  intent(in)    :: line
    type(help_user_t), intent(in)    :: user
    type(help_prog_t), intent(inout) :: prog
    logical,           intent(inout) :: error
    !
    call cubehelp_help_traphelp(line,user,prog,error)
    if (error) return
  end subroutine cubehelp_help_main
  !
  subroutine cubehelp_help_traphelp(line,user,prog,error)
    use cubetools_disambiguate
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    character(len=*),  intent(in)    :: line
    type(help_user_t), intent(in)    :: user
    type(help_prog_t), intent(inout) :: prog
    logical,           intent(inout) :: error
    !
    integer(kind=4) :: iquiet,ilang,icomm,ocode
    character(len=12) :: lang
    character(len=*), parameter :: rname='HELP>COMMAND'
    !
    if (user%topic.eq.strg_emp)  goto 10
    !
    call cubetools_disambiguate_toupper(user%topic,prog%uptopic,error)
    !
    iquiet = 1
    call sic_parse_command(prog%uptopic,len_trim(prog%uptopic),iquiet,.true.,  &
      ilang,icomm,ocode,error)
    if (error) goto 10
    !
    call sic_ilang(ilang,lang)
    if (cubetools_pack_has_language(lang)) then
       call cubetools_show_command(lang,prog%uptopic,error)
       if (error) return
       return
    endif
    !
  10 continue
    !
    ! Standard SIC help
    call sic_help(line,error)
    if (error) return
    !
  end subroutine cubehelp_help_traphelp
end module cubehelp_help
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
