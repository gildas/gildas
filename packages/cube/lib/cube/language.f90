!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubehelp_language
  use cubetools_structure
  use cube_debug
  use cubehelp_collect
  use cubehelp_help
  use cubehelp_question
  !
  public :: cubehelp_register_language
  private
  !
  integer(kind=lang_k) :: langid
  !
contains
  !
  subroutine cubehelp_register_language(error)
    !----------------------------------------------------------------------
    ! Register the HELP\ language
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    call cubetools_register_language('HELP',&
         'J.Pety, S.Bardeau, V.deSouzaMagalhaes',&
         'Commands to get help or debug the program',&
         'gag_doc:hlp/cube-help-help.hlp',&
         cubehelp_execute_command,langid,error)
    if (error) return
    !
    call cubehelp_question_register(error) ! This one needs to be first here
    if (error) return
    call collect%register(error)
    if (error) return
    call cubehelp_help_register(error)
    if (error) return
    call cube_debug_register(error)
    if (error) return
    !
    call cubetools_register_dict(error)
    if (error) return
  end subroutine cubehelp_register_language
  !
  subroutine cubehelp_execute_command(line,comm,error)
    !----------------------------------------------------------------------
    ! Execute a command of the HELP\ language
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    character(len=*), intent(in)    :: comm
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='EXECUTE>COMMAND'
    !
    error = .false.
    if (comm.eq.'HELP?') then
       call cubetools_list_language_commands(langid,error)
       if (error) return
    else
       call cubetools_execute_command(line,langid,comm,error)
       if (error) return
    endif
  end subroutine cubehelp_execute_command
end module cubehelp_language
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
