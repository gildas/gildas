!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubego_hyperbit
  use cubetools_structure
  use cubetools_switch_types
  use cubeadm_cubeid_types
  use cubego_messaging
  !
  public :: hyperbit
  private
  !
  type :: bit_comm_t
     type(option_t),     pointer :: comm
     type(cubeid_arg_t), pointer :: cube
     type(switch_comm_t)         :: header
   contains
     procedure, public  :: register => cubego_bit_register
     procedure, private :: parse    => cubego_bit_parse
     procedure, private :: main     => cubego_bit_main
  end type bit_comm_t
  type(bit_comm_t) :: hyperbit
  !
  integer(kind=4), parameter :: icube = 1
  type :: bit_user_t
     type(cubeid_user_t) :: cubeids
     type(switch_user_t) :: header
   contains
     procedure, private :: toprog => cubego_bit_user_toprog
  end type bit_user_t
  !
  type bit_prog_t
     type(switch_prog_t) :: header
   contains
     procedure, private :: act => cubego_bit_prog_act
  end type bit_prog_t
  !
contains
  !
  subroutine cubego_hyperbit_command(line,error)
    use gkernel_interfaces
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(bit_user_t) :: user
    character(len=*),parameter :: rname='HYPERBIT>COMMAND'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call hyperbit%parse(line,user,error)
    if (error) return
    call hyperbit%main(user,error)
    if (error) return
    ! Force insertion in SIC stack ! *** JP Why?
    if (sic_lire().eq.0) call sic_insert(line)
  end subroutine cubego_hyperbit_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubego_bit_register(bit,error)
    use cubedag_allflags
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    class(bit_comm_t), intent(inout) :: bit
    logical,           intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    character(len=*), parameter :: comm_abstract='Interactive bit of a cube'
    character(len=*), parameter :: comm_help=strg_id
    character(len=*), parameter :: rname='BIT>REGISTER'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'HYPERBIT','[cube]',&
         comm_abstract,&
         comm_help,&
         cubego_hyperbit_command,&
         bit%comm,&
         error)
    if (error) return
    call cubearg%register(&
         'CUBE',&
         'Cube to be bited', &
         strg_id,&
         code_arg_optional, &
         [flag_any],&
         code_read,&  ! Indicative: data access done by procedure
         code_access_imaset_or_speset,&  ! Indicative: data access done by procedure
         bit%cube,&
         error)
    if (error) return
    !
    call bit%header%register(&
         'HEADER','the header display',&
         'ON',error)
    if (error) return
  end subroutine cubego_bit_register
  !
  subroutine cubego_bit_parse(comm,line,user,error)
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    class(bit_comm_t), intent(inout) :: comm
    character(len=*),  intent(in)    :: line
    type(bit_user_t),  intent(out)   :: user
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='BIT>PARSE'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%header%parse(line,user%header,error)
    if (error) return
  end subroutine cubego_bit_parse
  !
  subroutine cubego_bit_main(comm,user,error)
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    class(bit_comm_t), intent(in)    :: comm
    type(bit_user_t),  intent(in)    :: user
    logical,           intent(inout) :: error
    !
    type(bit_prog_t) :: prog
    character(len=*), parameter :: rname='BIT>MAIN'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%act(error)
    if (error) return
  end subroutine cubego_bit_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubego_bit_user_toprog(user,comm,prog,error)
    use cubetools_user2prog
    use cubego_buffer
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(bit_user_t), intent(in)    :: user
    type(bit_comm_t),  intent(in)    :: comm  
    type(bit_prog_t),  intent(out)   :: prog
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='BIT>USER>TOPROG'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_user2prog(comm%comm,user%cubeids,cubes,error)
    if (error) return
    call cubes%defstruct(cubego,error)
    if (error) return
    !
    call prog%header%init(comm%header,error)
    if (error) return
    call user%header%toprog(comm%header,prog%header,error)
    if (error) return
  end subroutine cubego_bit_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubego_bit_prog_act(prog,error)
    use gkernel_interfaces
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(bit_prog_t), intent(in)    :: prog
    logical,           intent(inout) :: error
    !
    character(len=*),parameter :: rname='BIT>PROG>ACT'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    if (prog%header%act) call exec_program('@ p-header.cube')
  end subroutine cubego_bit_prog_act
end module cubego_hyperbit
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
