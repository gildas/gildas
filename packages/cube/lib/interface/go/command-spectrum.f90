!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubego_spectrum
  use cubetools_structure
  use cubetools_switch_types
  use cubeadm_cubeid_types
  use cubego_messaging
  !
  public :: spectrum
  private
  !
  type :: spectrum_comm_t
     type(option_t),     pointer :: comm
     type(cubeid_arg_t), pointer :: cube
     type(switch_comm_t)         :: interactive
   contains
     procedure, public  :: register => cubego_spectrum_register
     procedure, private :: parse    => cubego_spectrum_parse
     procedure, private :: main     => cubego_spectrum_main
  end type spectrum_comm_t
  type(spectrum_comm_t) :: spectrum
  !
  type :: spectrum_user_t
     type(cubeid_user_t) :: cubeids
     type(switch_user_t) :: interactive
   contains
     procedure, private :: toprog => cubego_spectrum_user_toprog
  end type spectrum_user_t
  !
  type spectrum_prog_t
     type(switch_prog_t) :: interactive
   contains
     procedure, private :: act => cubego_spectrum_prog_act
  end type spectrum_prog_t
contains
  !
  subroutine cubego_spectrum_command(line,error)
    use gkernel_interfaces
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(spectrum_user_t) :: user
    character(len=*), parameter :: rname='SPECTRUM>COMMAND'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call spectrum%parse(line,user,error)
    if (error) return
    call spectrum%main(user,error)
    if (error) return
    ! Force insertion in SIC stack ! *** JP Why?
    if (sic_lire().eq.0) call sic_insert(line)
  end subroutine cubego_spectrum_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubego_spectrum_register(spectrum,error)
    use cubedag_allflags
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    class(spectrum_comm_t), intent(inout) :: spectrum
    logical,                intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    character(len=*), parameter :: comm_abstract='Show a map of spectra from a cube'
    character(len=*), parameter :: comm_help=strg_id
    character(len=*), parameter :: rname='SPECTRUM>REGISTER'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'SPECTRUM','[cube]',&
         comm_abstract,&
         comm_help,&
         cubego_spectrum_command,&
         spectrum%comm,&
         error)
    if (error) return
    call cubearg%register(&
         'CUBE',&
         'Cube to be spectrumed',&
         strg_id,&
         code_arg_optional, &
         [flag_any],&
         code_read,&  ! Indicative: data access done by procedure
         code_access_imaset_or_speset,&  ! Indicative: data access done by procedure
         spectrum%cube,&
         error)
    if (error) return
    !
    call spectrum%interactive%register(&
         'INTERACTIVE','the interactive mode',&
         'ON',error)
    if (error) return
  end subroutine cubego_spectrum_register
  !
  subroutine cubego_spectrum_parse(comm,line,user,error)
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    class(spectrum_comm_t), intent(inout) :: comm
    character(len=*),       intent(in)    :: line
    type(spectrum_user_t),  intent(out)   :: user
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRUM>PARSE'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call spectrum%interactive%parse(line,user%interactive,error)
    if (error) return
  end subroutine cubego_spectrum_parse
  !
  subroutine cubego_spectrum_main(comm,user,error)
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    class(spectrum_comm_t), intent(in)    :: comm
    type(spectrum_user_t),  intent(in)    :: user
    logical,                intent(inout) :: error
    !
    type(spectrum_prog_t) :: prog
    character(len=*), parameter :: rname='SPECTRUM>MAIN'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%act(error)
    if (error) return
  end subroutine cubego_spectrum_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubego_spectrum_user_toprog(user,comm,prog,error)
    use cubetools_user2prog
    use cubego_buffer
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spectrum_user_t), intent(in)    :: user
    type(spectrum_comm_t),  intent(in)    :: comm
    type(spectrum_prog_t),  intent(out)   :: prog
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRUM>USER>TOPROG'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_user2prog(comm%comm,user%cubeids,cubes,error)
    if (error) return
    call cubes%defstruct(cubego,error)
    if (error) return
    !
    call prog%interactive%init(comm%interactive,error)
    if (error) return
    call user%interactive%toprog(comm%interactive,prog%interactive,error)
    if (error) return
  end subroutine cubego_spectrum_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubego_spectrum_prog_act(prog,error)
    use gkernel_interfaces
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(spectrum_prog_t), intent(in)    :: prog
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRUM>PROG>ACT'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call exec_program('@ p-spectrum.cube')
    call exec_program('@ p-header.cube')
    ! *** JP: Nothing yet for the moment.
!!$    if (prog%interactive%enabled) call exec_program('@ l-ctrl-master-loop')
  end subroutine cubego_spectrum_prog_act
end module cubego_spectrum
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
