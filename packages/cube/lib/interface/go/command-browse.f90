!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubego_browse
  use cubetools_parameters
  use cubetools_structure
  use cubetools_switch_types
  use cubeadm_cubeid_types
  use cubetopology_spapos_types
  use cubego_messaging
  !
  public :: browse
  private
  !
  type :: browse_comm_t
     type(option_t),     pointer :: comm
     type(cubeid_arg_t), pointer :: cube
     type(spapos_comm_t)         :: pixel
     type(option_t),     pointer :: catalog
     type(switch_comm_t)         :: interactive
   contains
     procedure, public  :: register => cubego_browse_register
     procedure, private :: parse    => cubego_browse_parse
     procedure, private :: main     => cubego_browse_main
  end type browse_comm_t
  type(browse_comm_t) :: browse
  !
  integer(kind=4), parameter :: icube = 1
  type :: browse_user_t
     type(cubeid_user_t) :: cubeids
     type(spapos_user_t) :: pixel
     character(len=file_l) :: catalog
     type(switch_user_t) :: interactive
   contains
     procedure, private :: toprog => cubego_browse_user_toprog
  end type browse_user_t
  !
  type browse_prog_t
     type(switch_prog_t) :: interactive
   contains
     procedure, private :: act => cubego_browse_prog_act
  end type browse_prog_t
  !
contains
  !
  subroutine cubego_browse_command(line,error)
    use gkernel_interfaces
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(browse_user_t) :: user
    character(len=*),parameter :: rname='BROWSE>COMMAND'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call browse%parse(line,user,error)
    if (error) return
    call browse%main(user,error)
    if (error) return
    ! Force insertion in SIC stack ! *** JP Why?
    if (sic_lire().eq.0) call sic_insert(line)
  end subroutine cubego_browse_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubego_browse_register(browse,error)
    use cubedag_allflags
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    class(browse_comm_t), intent(inout) :: browse
    logical,            intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    type(standard_arg_t) :: stdarg
    character(len=*), parameter :: comm_abstract = &
         'Interactive browse of a cube spectrum'
    character(len=*), parameter :: comm_help = &
         strg_id
    character(len=*), parameter :: rname='BROWSE>REGISTER'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'BROWSE','[cube]',&
         comm_abstract,&
         comm_help,&
         cubego_browse_command,&
         browse%comm,error)
    if (error) return
    call cubearg%register( &
         'CUBE', &
         'Cube to be viewed',  &
         strg_id,&
         code_arg_optional,  &
         [flag_any], &
         code_read,&  ! Indicative: data access done by procedure
         code_access_imaset_or_speset,&  ! Indicative: data access done by procedure
         browse%cube,&
         error)
    if (error) return
    !
    call browse%pixel%register(&
         'PIXEL',&
         'Define the position of the pixel to be gotten',&
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'CATALOG','File',&
         'Supersede the line catalog to be used',&
         strg_id,&
         browse%catalog,error)
    if (error) return
    call stdarg%register( &
         'FILE',  &
         'The line catalog file name', &
         'The file format is an ASCII table with frequencies [GHz] in first&
         & column and line names in second column. Protect the names with single&
         & quotes if needed.',&
         code_arg_mandatory, &
         error)
    if (error) return
    !
    call browse%interactive%register(&
         'INTERACTIVE','the interactive mode',&
         'ON',error)
    if (error) return
  end subroutine cubego_browse_register
  !
  subroutine cubego_browse_parse(comm,line,user,error)
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    class(browse_comm_t), intent(inout) :: comm
    character(len=*),     intent(in)    :: line
    type(browse_user_t),  intent(out)   :: user
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='BROWSE>PARSE'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%pixel%parse(line,user%pixel,error)
    if (error) return
    user%catalog = ''
    call cubetools_getarg(line,comm%catalog,1,user%catalog,.not.mandatory,error)
    if (error) return
    call comm%interactive%parse(line,user%interactive,error)
    if (error) return
  end subroutine cubego_browse_parse
  !
  subroutine cubego_browse_main(comm,user,error)
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    class(browse_comm_t), intent(in)    :: comm
    type(browse_user_t),  intent(in)    :: user
    logical,            intent(inout) :: error
    !
    type(browse_prog_t) :: prog
    character(len=*), parameter :: rname='BROWSE>MAIN'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%act(error)
    if (error) return
  end subroutine cubego_browse_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubego_browse_user_toprog(user,comm,prog,error)
    use cubetools_user2prog
    use cubego_buffer
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(browse_user_t), intent(in)    :: user
    type(browse_comm_t),  intent(in)    :: comm    
    type(browse_prog_t),  intent(out)   :: prog
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='BROWSE>USER>TOPROG'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_user2prog(comm%comm,user%cubeids,cubes,error)
    if (error) return
    call cubes%defstruct(cubego,error)
    if (error) return
    !
    call prog%interactive%init(comm%interactive,error)
    if (error) return
    call user%interactive%toprog(comm%interactive,prog%interactive,error)
    if (error) return
    !
    ! *** JP The two next statements seem to belong to prog more than to user
    call cubego_browse_struct_pixel(user%pixel,error)
    if (error) return
    call cubego_browse_struct_catalog(user%catalog,error)
    if (error) return
    ! *** JP
  end subroutine cubego_browse_user_toprog
  !
  subroutine cubego_browse_struct_pixel(pixel,error)
    use gkernel_interfaces
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    type(spapos_user_t), intent(in)    :: pixel
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='BROWSE>STRUCT>PIXEL'
    character(len=128) :: line
    !
    ! ZZZ This is to be implemented properly!!!
    !
    if (sic_varexist('cubego%pixel')) then
      line = 'sic\delete /var cubego%pixel'
      call exec_command(line,error)
      if (error) return
    endif
    line = 'sic\define structure cubego%pixel /global'
    call exec_command(line,error)
    if (error) return
    line = 'sic\define character*64 cubego%pixel%l cubego%pixel%m /global'
    call exec_command(line,error)
    if (error) return
    line = 'sic\define character*64 cubego%pixel%type cubego%pixel%unit /global'
    call exec_command(line,error)
    if (error) return
    !
    line = 'let cubego%pixel%l '//pixel%x
    call exec_command(line,error)
    if (error) return
    line = 'let cubego%pixel%m '//pixel%y
    call exec_command(line,error)
    if (error) return
    line = 'let cubego%pixel%type '//pixel%type
    call exec_command(line,error)
    if (error) return
    line = 'let cubego%pixel%unit '//pixel%unit
    call exec_command(line,error)
    if (error) return
  end subroutine cubego_browse_struct_pixel
  !
  subroutine cubego_browse_struct_catalog(catalog,error)
    use gkernel_interfaces
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: catalog
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='BROWSE>STRUCT>CATALOG'
    character(len=128) :: line
    !
    ! ZZZ This is to be implemented properly!!!
    !
    if (sic_varexist('cubego%catalog')) then
      line = 'sic\delete /var cubego%catalog'
      call exec_command(line,error)
      if (error) return
    endif
    line = 'sic\define structure cubego%catalog /global'
    call exec_command(line,error)
    if (error) return
    line = 'sic\define character*256 cubego%catalog%file /global'
    call exec_command(line,error)
    if (error) return
    line = 'let cubego%catalog%file "'//trim(catalog)//'"'
    call exec_command(line,error)
    if (error) return
  end subroutine cubego_browse_struct_catalog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubego_browse_prog_act(prog,error)
    use gkernel_interfaces
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(browse_prog_t), intent(in)    :: prog
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='BROWSE>PROG>ACT'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call exec_program('@ q-browse.cube')
    if (prog%interactive%act) call exec_program('@ l-ctrl-master-loop')
  end subroutine cubego_browse_prog_act
end module cubego_browse
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
