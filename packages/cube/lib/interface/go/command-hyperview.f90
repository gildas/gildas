!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubego_hyperview
  use cubetools_structure
  use cubetools_switch_types
  use cubeadm_cubeid_types
  use cubego_messaging
  !
  public :: hyperview
  private
  !
  type :: view_comm_t
     type(option_t),     pointer :: comm
     type(cubeid_arg_t), pointer :: cube
     type(switch_comm_t)         :: interactive
   contains
     procedure, public  :: register => cubego_view_register
     procedure, private :: parse    => cubego_view_parse
     procedure, private :: main     => cubego_view_main
  end type view_comm_t
  type(view_comm_t) :: hyperview
  !
  integer(kind=4), parameter :: icube = 1
  type :: view_user_t
     type(cubeid_user_t) :: cubeids
     type(switch_user_t) :: interactive
   contains
     procedure, private :: toprog => cubego_view_user_toprog
  end type view_user_t
  !
  type view_prog_t
     type(switch_prog_t) :: interactive
   contains
     procedure, private :: act => cubego_view_prog_act
  end type view_prog_t
  !
contains
  !
  subroutine cubego_hyperview_command(line,error)
    use gkernel_interfaces
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(view_user_t) :: user
    character(len=*), parameter :: rname='HYPERVIEW>COMMAND'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call hyperview%parse(line,user,error)
    if (error) return
    call hyperview%main(user,error)
    if (error) return
    ! Force insertion in SIC stack ! *** JP Why?
    if (sic_lire().eq.0) call sic_insert(line)
  end subroutine cubego_hyperview_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubego_view_register(view,error)
    use cubedag_allflags
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    class(view_comm_t), intent(inout) :: view
    logical,            intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    character(len=*), parameter :: comm_abstract='Interactive view of a cube'
    character(len=*), parameter :: comm_help=strg_id
    character(len=*), parameter :: rname='VIEW>REGISTER'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'HYPERVIEW','[cube]',&
         comm_abstract,&
         comm_help,&
         cubego_hyperview_command,&
         view%comm,&
         error)
    if (error) return
    call cubearg%register(&
         'CUBE',&
         'Cube to be viewed',&
         strg_id,&
         code_arg_optional,&
         [flag_any],&
         code_read,&  ! Indicative: data access done by procedure
         code_access_imaset_or_speset,&  ! Indicative: data access done by procedure
         view%cube,&
         error)
    if (error) return
    !
    call view%interactive%register(&
         'INTERACTIVE','the interactive mode',&
         'ON',error)
    if (error) return
  end subroutine cubego_view_register
  !
  subroutine cubego_view_parse(comm,line,user,error)
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    class(view_comm_t), intent(inout) :: comm
    character(len=*),   intent(in)    :: line
    type(view_user_t),  intent(out)   :: user
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: rname='VIEW>PARSE'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%interactive%parse(line,user%interactive,error)
    if (error) return
  end subroutine cubego_view_parse
  !
  subroutine cubego_view_main(comm,user,error)
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    class(view_comm_t), intent(in)    :: comm
    type(view_user_t),  intent(in)    :: user
    logical,            intent(inout) :: error
    !
    type(view_prog_t) :: prog
    character(len=*), parameter :: rname='VIEW>MAIN'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%act(error)
    if (error) return
  end subroutine cubego_view_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubego_view_user_toprog(user,comm,prog,error)
    use cubetools_user2prog
    use cubego_buffer
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(view_user_t), intent(in)    :: user
    type(view_comm_t),  intent(in)    :: comm
    type(view_prog_t),  intent(out)   :: prog
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: rname='VIEW>USER>TOPROG'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_user2prog(comm%comm,user%cubeids,cubes,error)
    if (error) return
    call cubes%defstruct(cubego,error)
    if (error) return
    call prog%interactive%init(comm%interactive,error)
    if (error) return
    call user%interactive%toprog(comm%interactive,prog%interactive,error)
    if (error) return
  end subroutine cubego_view_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubego_view_prog_act(prog,error)
    use gkernel_interfaces
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(view_prog_t), intent(in)    :: prog
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: rname='VIEW>PROG>ACT'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call exec_program('@ q-view.cube')
    if (prog%interactive%enabled) then
       call exec_program('@ l-ctrl-master-loop')
    else
       call exec_program('@ l-ctrl-master-exit')
    endif
  end subroutine cubego_view_prog_act
end module cubego_hyperview
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
