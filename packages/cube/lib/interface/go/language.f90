!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubego_language
  use cubetools_structure
  use cubego_messaging
  use cubego_bit
  use cubego_hyperbit
  use cubego_browse
  use cubego_load
  use cubego_oldload
  use cubego_show
  use cubego_spectrum
  use cubego_topology
  use cubego_view
  use cubego_hyperview
  !
  public :: cubego_register_language
  private
  !
  integer(kind=lang_k) :: langid
  !
contains
  !
  subroutine cubego_register_language(error)
    !----------------------------------------------------------------------
    ! Register the GO\ language
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    call cubetools_register_language('GO',&
         'J.Pety, S.Bardeau, V.deSouzaMagalhaes',&
         'Commands to visualize or interact with a cube',&
         'gag_doc:hlp/cube-help-go.hlp',&
         cubego_execute_command,langid,error)
    if (error) return
    call bit%register(error)
    if (error) return
    call hyperbit%register(error)
    if (error) return
    call browse%register(error)
    if (error) return
    call load%register(error)
    if (error) return
    call cubego_oldload_register(error)
    if (error) return
    call show%register(error)
    if (error) return
    call spectrum%register(error)
    if (error) return
    call topology%register(error)
    if (error) return
    call view%register(error)
    if (error) return
    call hyperview%register(error)
    if (error) return
    !
    call cubetools_register_dict(error)
    if (error) return
  end subroutine cubego_register_language
  !
  subroutine cubego_execute_command(line,comm,error)
    use cubetools_parameters
    use cubeadm_opened
    use cubeadm_timing
    !----------------------------------------------------------------------
    ! Execute a command of the GO\ language
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    character(len=*), intent(in)    :: comm
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='EXECUTE>COMMAND'
    !
    error = .false.
    if (comm.eq.'GO?') then
       call cubetools_list_language_commands(langid,error)
       if (error) return
    else
       call cubeadm_timing_init()
       call cubetools_execute_command(line,langid,comm,error)
       if (error) continue ! To ensure error recovery in the call to cubeadm_finish_all
       call cubeadm_finish_all(comm,line,error)
       if (error) continue ! To ensure error recovery in timing
       call cubeadm_timing_final(comm)
       if (error) return
    endif
  end subroutine cubego_execute_command
end module cubego_language
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
