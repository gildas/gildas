!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubego_load
  use cubetools_parameters
  use cubetools_structure
  use cubetools_userspace
  use cubeadm_cubeid_types
  use cubego_messaging
  use cubeedit_getdata
  use cubeedit_getheader
  !
  public :: load
  private
  !
  type :: load_comm_t
     type(option_t),     pointer :: comm
     type(cubeid_arg_t), pointer :: cube
     type(userspace_opt_t)       :: into
     type(getdata_comm_t)        :: data
     type(getheader_comm_t)      :: head
   contains
     procedure, public  :: register => cubego_load_register
     procedure, private :: parse    => cubego_load_parse
     procedure, private :: main     => cubego_load_main 
  end type load_comm_t
  type(load_comm_t) :: load
  !
  type :: load_user_t
     type(userspace_user_t) :: userspace
     type(cubeid_user_t)    :: id
     type(getdata_user_t)   :: data
     type(getheader_user_t) :: head
  end type load_user_t
  !
  character(len=*), parameter :: defname = 'GOLOAD'
  !
contains
  !
  subroutine cubego_load_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(load_user_t) :: user
    character(len=*), parameter :: rname='LOAD>COMMAND'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call load%parse(line,user,error)
    if (error) return
    call load%main(user,error)
    if (error) return
  end subroutine cubego_load_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubego_load_register(load,error)
    use cubedag_allflags
    !-------------------------------------------------------------------
    ! Register GO\LOAD command and its options
    !-------------------------------------------------------------------
    class(load_comm_t), intent(inout) :: load
    logical,            intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    logical, parameter :: noiterator = .false.
    character(len=*), parameter :: comm_abstract = &
         'load information from a cube'
    character(len=*), parameter :: comm_help = &
         'Information loaded onto variables by GO\LOAD is intended as&
         & information. Modifying the values in these variables does&
         & not change the values for these parameters in the original&
         & cube. If option /INTO is not used the information&
         & gotten will be stored in a master structure called '//defname
    character(len=*), parameter :: rname='LOAD>REGISTER'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'LOAD','[cube]',&
         comm_abstract,&
         strg_id,&
         cubego_load_command,&
         load%comm,error)
    if (error) return
    call cubearg%register( &
         'CUBE', &
         'Input cube',  &
         strg_id,&
         code_arg_optional,  &
         [flag_any], &
         code_read, &
         code_access_imaset_or_speset, &
         load%cube, &
         error)
    if (error) return
    !
    call load%into%register(error)
    if (error) return
    call load%head%register(defname,load%into,error)
    if (error) return
    call load%data%register(load%into,access_not_fixed,noiterator,error)
    if (error) return
  end subroutine cubego_load_register
  !
  subroutine cubego_load_parse(load,line,user,error)
    !-------------------------------------------------------------------
    ! Support routine for command LOAD
    !-------------------------------------------------------------------
    class(load_comm_t), intent(in)    :: load
    character(len=*),   intent(in)    :: line
    type(load_user_t),  intent(inout) :: user
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: rname='LOAD>PARSE'
    !
    call load%into%parse(line,user%userspace,error)
    if (error) return
    !
    call cubeadm_cubeid_parse(line,load%comm,user%id,error)
    if (error) return
    call load%head%parse(line,user%userspace,user%head,error)
    if (error) return
    call load%data%parse(line,user%userspace,user%data,error)
    if (error) return
  end subroutine cubego_load_parse
  !
  subroutine cubego_load_main(load,user,error)
    use cube_types
    use cubetuple_format
    use cubeadm_get
    use cubeadm_opened
    !-------------------------------------------------------------------
    ! Support routine for command LOAD
    !-------------------------------------------------------------------
    class(load_comm_t), intent(in)    :: load
    type(load_user_t),  intent(in)    :: user
    logical,            intent(inout) :: error
    !
    type(cube_t), pointer :: cube
    class(format_t), pointer :: format
    integer(kind=entr_k) :: ie
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='LOAD>MAIN'
    !
    if (.not.user%head%do .and. .not.user%data%do) then
       call load%head%list(error)
       if (error) return
       call load%data%list(error)
       if (error) return
       return
    endif
    !
    if (user%data%do) then
      ! As of today, data access can only be done on a cube_t
      call cubeadm_get_header(load%cube,user%id,cube,error)
      if (error) return
      format => cube  ! Set if header access is also needed
      ! Do not forget to prepare the internal iterator so that
      ! 'get' image/channel/etc... works properly.
      call cubeadm_datainit_all(iter,error)
      if (error) return
    elseif (user%head%do) then
      ! Header access can be done on any format_t
      call cubeadm_get_fheader(load%cube,user%id,format,error)
      if (error) return
    endif
    !
    if (user%head%do) then
       call load%head%get(user%head,format,defname,user%data%do,error)
       if (error) return
    endif
    if (user%data%do) then
       ie = entr_unk
       call load%data%get(user%data,cube,defname,user%head%do,ie,error)
       if (error) return
    endif
  end subroutine cubego_load_main
end module cubego_load
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
