!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubego_view
  use cubetools_structure
  use cubedag_index_iterator_tool
  use cubego_index_iterator_types
  use cubeadm_cubeid_types
  use cubego_messaging
  !
  public :: view
  private
  !
  type :: view_comm_t
     type(option_t),     pointer :: comm
     type(cubeid_arg_t), pointer :: cube
     type(index_iterator_comm_t) :: iter
   contains
     procedure, public  :: register => view_comm_register
     procedure, private :: parse    => view_comm_parse
     procedure, private :: main     => view_comm_main
  end type view_comm_t
  type(view_comm_t) :: view
  !
  integer(kind=4), parameter :: icube = 1
  type :: view_user_t
     type(cubeid_user_t) :: cubeids
     type(index_iterator_user_t) :: iter
   contains
     procedure, private :: toprog => view_user_toprog
  end type view_user_t
  !
  type view_prog_t
   contains
     procedure, private :: act => view_prog_act
  end type view_prog_t
  !
  type(index_iterator_t) :: iter  ! Permanent life
  !
contains
  !
  subroutine view_command(line,error)
    use gkernel_interfaces
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(view_user_t) :: user
    character(len=*),parameter :: rname='VIEW>COMMAND'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call view%parse(line,user,error)
    if (error) return
    call view%main(user,error)
    if (error) return
    ! Force insertion in SIC stack ! *** JP Why?
    if (sic_lire().eq.0) call sic_insert(line)
  end subroutine view_command
  !
  !----------------------------------------------------------------------
  !
  subroutine view_comm_register(comm,error)
    use cubedag_allflags
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    class(view_comm_t), intent(inout) :: comm
    logical,            intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    character(len=*), parameter :: rname='VIEW>COMM>REGISTER'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'VIEW','[cube]',&
         'Interactive view of a cube',&
         strg_id,&
         view_command,&
         comm%comm,error)
    if (error) return
    call cubearg%register( &
         'CUBE', &
         'Cube to be viewed',  &
         strg_id,&
         code_arg_optional,  &
         [flag_any], &
         code_read,&  ! Indicative: data access done by procedure
         code_access_imaset_or_speset,&  ! Indicative: data access done by procedure
         comm%cube,&
         error)
    if (error) return
    call comm%iter%register('View','current',error)
    if (error) return
  end subroutine view_comm_register
  !
  subroutine view_comm_parse(comm,line,user,error)
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    class(view_comm_t), intent(inout) :: comm
    character(len=*),   intent(in)    :: line
    type(view_user_t),  intent(out)   :: user
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: rname='VIEW>COMM>PARSE'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%iter%parse(line,user%iter,error)
    if (error) return    
  end subroutine view_comm_parse
  !
  subroutine view_comm_main(comm,user,error)
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    class(view_comm_t), intent(in)    :: comm
    type(view_user_t),  intent(in)    :: user
    logical,            intent(inout) :: error
    !
    type(view_prog_t) :: prog
    character(len=*), parameter :: rname='VIEW>COMM>MAIN'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%act(error)
    if (error) return
  end subroutine view_comm_main
  !
  !----------------------------------------------------------------------
  !
  subroutine view_user_toprog(user,comm,prog,error)
    use cubetools_user2prog
    use cubego_buffer
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(view_user_t), intent(in)    :: user
    type(view_comm_t),  intent(in)    :: comm
    type(view_prog_t),  intent(out)   :: prog
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: rname='VIEW>USER>TOPROG'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_user2prog(comm%comm,user%cubeids,cubes,error,  &
      user%iter%code,iter)
    if (error) return
    call cubes%defstruct(cubego,error)
    if (error) return
  end subroutine view_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine view_prog_act(prog,error)
    use gkernel_interfaces
    use cubego_buffer
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(view_prog_t), intent(in)    :: prog
    logical,            intent(inout) :: error
    !
    character(len=mess_l) :: exec
    character(len=*), parameter :: rname='VIEW>PROG>ACT'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    write (exec,'(a,i0)') 'let name ',cubes%id(icube)
    call exec_program(trim(exec))
    call exec_program('@ q-old-view.cube')
    call exec_program('let name ""')
  end subroutine view_prog_act
end module cubego_view
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
