!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubego_sicvar
  use cubetools_parameters
  use cubetools_structure
  use cubetools_keywordlist_types
  use cubego_messaging
  !
  public :: sicvar_user_t, sicvar_t,sicvar_opt_t
  private
  !
  type sicvar_opt_t
     type(option_t),      pointer :: opt
     type(keywordlist_comm_t), pointer :: status
   contains
     procedure :: register  => cubego_sicvar_register
     procedure :: parse     => cubego_sicvar_parse
     procedure :: user2prog => cubego_sicvar_user2prog
  end type sicvar_opt_t
  !
  type sicvar_user_t
     character(len=varn_l) :: name   = strg_unk
     character(len=argu_l) :: status = strg_unk
  end type sicvar_user_t
  !
  type sicvar_t
     character(len=varn_l) :: name
     logical               :: readonly
   contains
     procedure :: exists => cubego_sicvar_exists
  end type sicvar_t
  !
contains
  !
  subroutine cubego_sicvar_register(opt,error)
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    class(sicvar_opt_t), intent(out)   :: opt
    logical,             intent(inout) :: error
    !
    type(standard_arg_t) :: stdarg
    type(keywordlist_comm_t)  :: keyarg
    character(len=*), parameter :: status(2) = ['READ ','WRITE']
    character(len=*), parameter :: rname='SICVAR>REGISTER'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         'INTO','varname [status]',&
         'Define the SIC variable name and its status',&
         strg_id,&
         opt%opt,error)
    if (error) return
    call stdarg%register( &
         'varname',  &
         'Variable name', &
         strg_id,&
         code_arg_mandatory, &
         error)
    if (error) return
    call keyarg%register( &
         'status',  &
         'READ or WRITE status for variable', &
         'Default is READ',&
         code_arg_optional, &
         status, &
         .not.flexible, &
         opt%status, &
         error)
    if (error) return
  end subroutine cubego_sicvar_register
  !
  subroutine cubego_sicvar_parse(opt,line,user,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(sicvar_opt_t), intent(in)    :: opt
    character(len=*),    intent(in)    :: line
    type(sicvar_user_t), intent(out)   :: user
    logical,             intent(inout) :: error
    !
    logical dovar
    character(len=*), parameter :: rname='SICVAR>PARSE'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call opt%opt%present(line,dovar,error)
    if (error) return
    if (.not.dovar) then
       call cubego_message(seve%e,rname,'Option /INTO is mandatory')
       error = .true.
       return
    else
       call cubetools_getarg(line,opt%opt,1,user%name,mandatory,error)
       if (error) return
       user%status = 'READ'
       call cubetools_getarg(line,opt%opt,2,user%status,.not.mandatory,error)
       if (error) return
    endif
  end subroutine cubego_sicvar_parse
  !
  subroutine cubego_sicvar_user2prog(opt,user,prog,error)
    use cubetools_disambiguate
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(sicvar_opt_t), intent(in)    :: opt
    type(sicvar_user_t), intent(in)    :: user
    type(sicvar_t),      intent(out)   :: prog
    logical,             intent(inout) :: error
    !
    character(len=argu_l) :: status
    integer(kind=4) :: ikey
    character(len=*), parameter :: rname='SICVAR>USER2PROG'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call cubetools_disambiguate_toupper(user%name,prog%name,error)
    if (error) return
    call cubetools_keywordlist_user2prog(opt%status,user%status,ikey,status,error)
    if (error) return
    prog%readonly = status.eq.'READ'
  end subroutine cubego_sicvar_user2prog
  !
  subroutine cubego_sicvar_exists(sicvar,error)
    use gkernel_interfaces
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    class(sicvar_t), intent(in)    :: sicvar
    logical,         intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICVAR>EXISTS'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    if (sic_varexist(trim(sicvar%name))) then
       call cubego_message(goseve%others,rname,trim(sicvar%name)//' user structure already exists')
       call sic_delvariable(trim(sicvar%name),.false.,error)
       if (error) return
    endif
  end subroutine cubego_sicvar_exists
end module cubego_sicvar
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
