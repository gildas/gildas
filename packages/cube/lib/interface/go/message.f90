!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage CUBE GO messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubego_messaging
  use gpack_def
  use gbl_message
  use cubetools_parameters
  !
  public :: goseve,seve,mess_l
  public :: cubego_message_set_id,cubego_message
  public :: cubego_message_set_trace,cubego_message_get_trace
  public :: cubego_message_set_others,cubego_message_get_others
  private
  !
  ! Identifier used for message identificatgon
  integer(kind=4) :: cubego_message_id = gpack_global_id  ! Default value for startup message
  !
  type :: cubego_messaging_debug_t
     integer(kind=code_k) :: trace = seve%t
     integer(kind=code_k) :: others = seve%d
  end type cubego_messaging_debug_t
  !
  type(cubego_messaging_debug_t) :: goseve
  !
contains
  !
  subroutine cubego_message_set_id(id)
    !---------------------------------------------------------------------
    ! Alter library id into input id. Should be called by the library
    ! which wants to share its id with the current one.
    !---------------------------------------------------------------------
    integer(kind=4), intent(in) :: id
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='MESSAGE>SET>ID'
    !
    cubego_message_id = id
    write (mess,'(A,I0)') 'Now use id #',cubego_message_id
    call cubego_message(seve%d,rname,mess)
  end subroutine cubego_message_set_id
  !
  subroutine cubego_message(mkind,procname,message)
    use cubetools_cmessaging
    !---------------------------------------------------------------------
    ! Messaging facility for the current library. Calls the low-level
    ! (internal) messaging routine with its own identifier.
    !---------------------------------------------------------------------
    integer(kind=4),  intent(in) :: mkind     ! Message kind
    character(len=*), intent(in) :: procname  ! Name of calling procedure
    character(len=*), intent(in) :: message   ! Message string
    !
    call cubetools_cmessage(cubego_message_id,mkind,'GO>'//procname,message)
  end subroutine cubego_message
  !
  subroutine cubego_message_set_trace(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       goseve%trace = seve%i
    else
       goseve%trace = seve%t
    endif
  end subroutine cubego_message_set_trace
  !
  subroutine cubego_message_set_others(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       goseve%others = seve%i
    else
       goseve%others = seve%d
    endif
  end subroutine cubego_message_set_others
  !
  function cubego_message_get_trace()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubego_message_get_trace
    !
    cubego_message_get_trace = goseve%trace.eq.seve%i
    !
  end function cubego_message_get_trace
  !
  function cubego_message_get_others()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubego_message_get_others
    !
    cubego_message_get_others = goseve%others.eq.seve%i
    !
  end function cubego_message_get_others
  !
end module cubego_messaging
!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
