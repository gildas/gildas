!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubego_buffer
  use cubetools_progstruct_types
  use cubeadm_cubeid_types
  !
  public :: cubego
  public :: cubes
  private
  !
  type(progstruct_t) :: cubego
  !
  type(cubeid_prog_t) :: cubes
end module cubego_buffer
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
