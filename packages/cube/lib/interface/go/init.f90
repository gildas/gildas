!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubego_init
  use cubego_buffer
  use cubego_messaging
  !
  public :: cubego_library_init
  private
  !
contains
  !
  subroutine cubego_library_init(error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    character(len=*), parameter :: rname='LIBRARY>INIT'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call cubego%init('cubego',error)
    if (error) return
  end subroutine cubego_library_init
end module cubego_init
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
