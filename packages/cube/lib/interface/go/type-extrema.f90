!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! *** JP: This code should be factorized with the primitive types used for the
! *** JP: header in header-array.f90.
!
module cubego_extrema_types
  use cubetools_parameters
  use cubego_messaging
  use cube_types
  use cubetopology_cuberegion_types
  !
  public :: extrema_prog_t
  private
  !
  integer(kind=ndim_k), parameter :: ndim = 3
  integer(kind=ndim_k), parameter :: ix = 1
  integer(kind=ndim_k), parameter :: iy = 2
  integer(kind=ndim_k), parameter :: ic = 3 
  !
  type extrema_prog_t
     real(kind=sign_k) :: min ! Minimum inside region of interest
     real(kind=sign_k) :: max ! Maximum inside region of interest
     !
     integer(kind=data_k),  private :: locmin(ndim)  ! Location of the minimum
     integer(kind=data_k),  private :: locmax(ndim)  ! Location of the maximum
     integer(kind=data_k),  private :: nnan          ! Number of NaNs
     integer(kind=data_k),  private :: ndata         ! Number of data
     integer(kind=ndim_k),  private :: iaxes(ndim) = [ix,iy,ic] ! Dimension pointers
     type(cube_t), pointer, private :: cube          ! Pointer to cube
   contains
     procedure, public  :: get           => cubego_extrema_get
     procedure, private :: test_range    => cubego_extrema_test_range
     procedure, private :: getorder      => cubego_extrema_getorder
     !
     procedure, private :: header        => cubego_extrema_header
     procedure, private :: data          => cubego_extrema_data
     procedure, private :: loop          => cubego_extrema_loop
     procedure, private :: act           => cubego_extrema_act
     procedure, private :: init          => cubego_extrema_init
     procedure, private :: merge_local   => cubego_extrema_merge_local
     !
     procedure, public  :: list          => cubego_extrema_list
     procedure, public  :: def_substruct => cubego_extrema_def_substruct
  end type extrema_prog_t
  !
contains
  !
  subroutine cubego_extrema_get(ext,cube,region,error)
    use cubetopology_sperange_types
    use cubetopology_sparange_types
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    class(extrema_prog_t),   intent(out)   :: ext
    type(cube_t), target,    intent(inout) :: cube
    type(cuberegion_prog_t), intent(in)    :: region
    logical,                 intent(inout) :: error
    !
    logical :: dodata
    character(len=*), parameter :: rname='EXTREMA>GET'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    ext%cube => cube
    call ext%test_range(region,dodata,error)
    if (error) return
    !
    if (dodata) then
       call ext%data(region,error)
       if (error) return
    else
       call ext%header(error)
       if (error) return
    endif
  end subroutine cubego_extrema_get
  !
  subroutine cubego_extrema_test_range(ext,region,dodata,error)
    use cubetools_nan
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    class(extrema_prog_t),   intent(inout) :: ext
    type(cuberegion_prog_t), intent(in)    :: region
    logical,                 intent(out)   :: dodata
    logical,                 intent(inout) :: error
    !
    logical :: totalx,totaly,totalc,wholecube,bothnan,allnan
    character(len=*), parameter :: rname='EXTREMA>TEST>RANGE'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call ext%getorder(error)
    if (error) return
    !
    ! If we cover the whole x,y,c axes we only need to compute extrema
    ! if they are both NaN
    totalx = region%ix%first.eq.1 .and. region%ix%last.eq.ext%cube%head%arr%n%l
    totaly = region%iy%first.eq.1 .and. region%iy%last.eq.ext%cube%head%arr%n%m
    totalc = region%iz%first.eq.1 .and. region%iz%last.eq.ext%cube%head%arr%n%c
    wholecube = totalc.and.totaly.and.totalx
    bothnan = ieee_is_nan(ext%cube%head%arr%min%val).and.ieee_is_nan(ext%cube%head%arr%max%val)
    allnan  = ext%cube%head%arr%n%nan.eq.ext%cube%head%arr%n%dat 
    !
    if (allnan) then
       dodata = .false.
    else
       if (wholecube.and..not.bothnan) then
          dodata = .false.
       else
          dodata = .true.
       endif
    endif
  end subroutine cubego_extrema_test_range
  !
  subroutine cubego_extrema_getorder(ext,error)
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    class(extrema_prog_t), intent(inout) :: ext
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='EXTREMA>GETORDER'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    select case(ext%cube%order())
    case(code_cube_imaset)
       ext%iaxes = [1,2,3]
    case (code_cube_speset)
       ext%iaxes = [2,3,1]
    case default
       call cubego_message(goseve%trace,rname,'Internal error: Cube is neither in imaset or speset')
       error = .true.
       return
    end select
  end subroutine cubego_extrema_getorder
  !
  !------------------------------------------------------------------------
  !
  subroutine cubego_extrema_header(ext,error)
    use cubetools_header_methods
    use cubetools_arrelt_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(extrema_prog_t), intent(inout) :: ext
    logical,               intent(inout) :: error
    !
    type(arrelt_t) :: min,max
    character(len=*), parameter :: rname='EXTREMA>FETCH'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call cubetools_header_get_array_minmax(ext%cube%head,min,max,error)
    if (error)  return
    !
    ext%min                   = min%val
    ext%locmin(ext%iaxes(ix)) = min%ix
    ext%locmin(ext%iaxes(iy)) = min%iy
    ext%locmin(ext%iaxes(ic)) = min%ic
    !
    ext%max                   = max%val
    ext%locmax(ext%iaxes(ix)) = max%ix
    ext%locmax(ext%iaxes(iy)) = max%iy
    ext%locmax(ext%iaxes(ic)) = max%ic
    !
    ext%nnan  = ext%cube%head%arr%n%nan
    ext%ndata = ext%cube%head%arr%n%dat
  end subroutine cubego_extrema_header
  !
  subroutine cubego_extrema_data(global,region,error)
    use cubetools_nan
    use cubeadm_opened
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(extrema_prog_t),   intent(inout) :: global
    type(cuberegion_prog_t), intent(in)    :: region
    logical,                 intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: itertask
    character(len=*), parameter :: rname='EXTREMA>DATA'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call global%init(error)
    if (error) return
    global%ndata = region%ndata()
    !
    call cubeadm_datainit_all(itertask,region,error)
    if (error) return
    !
    !$OMP PARALLEL DEFAULT(none) SHARED(global,error) FIRSTPRIVATE(itertask)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(itertask,error))
       if (error) exit
       !$OMP TASK SHARED(global,error) FIRSTPRIVATE(itertask)
       if (.not.error) then
          call global%loop(itertask,error)
       endif
       !$OMP END TASK
    enddo ! ie
    !$OMP END SINGLE
    !$OMP END PARALLEL
    !
    if (global%ndata.eq.global%nnan) then
       call global%init(error)
       if (error) return
       global%min = gr4nan
       global%max = gr4nan
    endif
  end subroutine cubego_extrema_data
  !
  subroutine cubego_extrema_loop(global,iter,error)
    use cubeadm_taskloop
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    class(extrema_prog_t),    intent(inout) :: global
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(extrema_prog_t) :: local
    character(len=*), parameter :: rname='EXTREMA>LOOP'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    do while (iter%iterate_entry(error))
      call global%act(iter,local,error)
      if (error) return
      !
      ! *** JP: A better parallel construct would be to computes
      ! *** JP: all the local min and max and merge them at the end.
      ! *** JP: In fact I am unsure that the OMP directive is actually used!
      !
      ! This part has to be critical to avoid conflict betwen
      ! threads. Third dimension correction has to be applied to
      ! account for i3 being the index inside the subcube.
      !
      ! !$OMP CRITICAL
      call global%merge_local(iter%ie,local,error)
      if (error) return
      ! !$OMP END CRITICAL
    enddo  ! ientry
  end subroutine cubego_extrema_loop
  !
  subroutine cubego_extrema_act(global,itertask,local,error)
    use cubeadm_taskloop
    use cubeadm_subcube_types
    use cubetools_nan
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    class(extrema_prog_t),    intent(inout) :: global
    type(cubeadm_iterator_t), intent(in)    :: itertask
    type(extrema_prog_t),     intent(out)   :: local
    logical,                  intent(inout) :: error
    !
    type(subcube_t) :: cube
    integer(kind=data_k) :: ix,iy,iz
    character(len=*), parameter :: rname='EXTREMA>ACT'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call local%init(error)
    if (error) return
    !
    ! Subcubes are initialized here as their size (3rd dim) may change from
    ! from one subcube to another.
    call cube%associate('cube',global%cube,itertask,error)
    if (error) return
    !
    call cube%get(error)
    if (error) return
    do iz=1,cube%nz
       do iy=1,cube%ny
          do ix=1,cube%nx
             if (ieee_is_nan(cube%val(ix,iy,iz))) then
                local%nnan = local%nnan+1
             else
                if (cube%val(ix,iy,iz).gt.local%max) then
                   local%max       = cube%val(ix,iy,iz)
                   local%locmax(1) = ix + cube%range1(1) - 1  ! Absolute position in full cube
                   local%locmax(2) = iy + cube%range2(1) - 1
                   local%locmax(3) = iz + cube%range3(1) - 1
                else if (cube%val(ix,iy,iz).lt.local%min) then
                   local%min       = cube%val(ix,iy,iz)
                   local%locmin(1) = ix + cube%range1(1) - 1
                   local%locmin(2) = iy + cube%range2(1) - 1
                   local%locmin(3) = iz + cube%range3(1) - 1
                else
                   ! Does nothing
                endif
             endif
          enddo
       enddo
    enddo
  end subroutine cubego_extrema_act
  !
  !--------------------------------------------------------------------------
  !
  subroutine cubego_extrema_init(ext,error)
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    class(extrema_prog_t), intent(inout) :: ext
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='EXTREMA>INIT'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    ext%min =  huge(ext%min)
    ext%max = -huge(ext%max)
    ext%locmin(:) = 0
    ext%locmax(:) = 0
    ext%nnan = 0
  end subroutine cubego_extrema_init
  !
  subroutine cubego_extrema_merge_local(global,isubcube,local,error)
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    class(extrema_prog_t), intent(inout) :: global
    integer(kind=entr_k),  intent(in)    :: isubcube
    type(extrema_prog_t),  intent(in)    :: local
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='EXTREMA>MERGE>LOCAL'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    if (local%max.gt.global%max) then
       global%max       = local%max
       global%locmax(:) = local%locmax(:)
    endif
    if (local%min.lt.global%min) then
       global%min       = local%min
       global%locmin(:) = local%locmin(:)
    endif
    global%nnan = global%nnan+local%nnan
  end subroutine cubego_extrema_merge_local
  !
  !------------------------------------------------------------------------
  !
  subroutine cubego_extrema_list(ext,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(extrema_prog_t), intent(in)    :: ext
    logical,               intent(inout) :: error
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='EXTREMA>LIST'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    write(mess,'(15x,a,4x,3(4x,a))') 'Value','Channel','X pixel','Y pixel'
    call cubego_message(seve%r,rname,mess)
    write(mess,1000) 'Maximum:',ext%max,ext%locmax(ext%iaxes(ic)),&
         ext%locmax(ext%iaxes(ix)),ext%locmax(ext%iaxes(iy))
    call cubego_message(seve%r,rname,mess)
    write(mess,1000) 'Minimum:',ext%min,ext%locmin(ext%iaxes(ic)),&
         ext%locmin(ext%iaxes(ix)),ext%locmin(ext%iaxes(iy))
    call cubego_message(seve%r,rname,mess)
    call cubego_message(seve%r,rname,'')
    write(mess,'(2(i0,a),x,1pg8.3,a)') ext%nnan,' NaNs out of ',ext%ndata,' elements, ',&
         100.0*ext%nnan/ext%ndata,'%'
    call cubego_message(seve%r,rname,mess)
    call cubego_message(seve%r,rname,'')
    !
1000 format(a,2x,1pg14.7,3(x,i10))
  end subroutine cubego_extrema_list
  !
  subroutine cubego_extrema_def_substruct(ext,name,struct,error)
    use cubetools_userstruct
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(extrema_prog_t), intent(in)    :: ext
    character(len=*),      intent(in)    :: name
    type(userstruct_t),    intent(inout) :: struct
    logical,               intent(inout) :: error
    !
    type(userstruct_t) :: substruct,min,max
    character(len=*), parameter :: rname='EXTREMA>DEF>SUBSTRUCT'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    call struct%def_substruct(name,substruct,error)
    if (error) return
    call substruct%def_substruct('min',min,error)
    if (error) return
    call min%set_member('value',ext%min,error)
    if (error) return
    call min%set_member('location',ext%locmin,error)
    if (error) return
    call substruct%def_substruct('max',max,error)
    if (error) return
    call max%set_member('value',ext%max,error)
    if (error) return
    call max%set_member('location',ext%locmax,error)
    if (error) return
  end subroutine cubego_extrema_def_substruct
end module cubego_extrema_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
