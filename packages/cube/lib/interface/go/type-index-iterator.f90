!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubego_index_iterator_types
  use cubetools_parameters
  use cubedag_index_iterator_tool
  use cubesyntax_key_types
  use cubego_messaging
  !
  public :: index_iterator_comm_t,index_iterator_user_t,index_iterator_prog_t
  private
  !
  type index_iterator_comm_t
     type(key_comm_t) :: keys(ncode_iter)
   contains
     procedure, public :: register => index_iterator_comm_register
     procedure, public :: parse    => index_iterator_comm_parse
  end type index_iterator_comm_t
  !
  type index_iterator_user_t
     integer(kind=code_k) :: code = code_iter_unknown
     type(key_user_t) :: keys(ncode_iter)
   contains
!!$     ! Generic
!!$     procedure, public :: init     => index_iterator_user_init
!!$     procedure, public :: allocate => index_iterator_user_allocate
!!$     procedure, public :: free     => index_iterator_user_free
!!$     procedure, public :: list     => index_iterator_user_list
!!$     final :: index_iterator_user_final
  end type index_iterator_user_t
  !
  type index_iterator_prog_t
     !link toward the index that the iterator iterates!
   contains
!!$     ! Generic
!!$     procedure, public :: init     => index_iterator_prog_init
!!$     procedure, public :: allocate => index_iterator_prog_allocate
!!$     procedure, public :: free     => index_iterator_prog_free
!!$     procedure, public :: list     => index_iterator_prog_list
!!$     final :: index_iterator_prog_final
!!$     ! Object specific
!!$     procedure, public :: current  => index_iterator_prog_current
!!$     procedure, public :: next     => index_iterator_prog_next
!!$     procedure, public :: previous => index_iterator_prog_previous
!!$     procedure, public :: first    => index_iterator_prog_first
!!$     procedure, public :: last     => index_iterator_prog_last
  end type index_iterator_prog_t
  !
contains
  !
  subroutine index_iterator_comm_register(comm,operation,idxkind,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    class(index_iterator_comm_t), intent(inout) :: comm
    character(len=*),             intent(in)    :: operation
    character(len=*),             intent(in)    :: idxkind
    logical,                      intent(inout) :: error
    !
    integer(kind=4) :: ikey
    character(len=*), parameter :: rname='INDEX>ITERATOR>REGISTER'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    do ikey=1,ncode_iter
       call comm%keys(ikey)%register(&
            iter_names(ikey),&
            trim(operation)//' the '//trim(iter_abstracts(ikey))//  &
            ' entry from the '//trim(idxkind)//' index',&
            error)
       if (error) return
    enddo ! ikey
  end subroutine index_iterator_comm_register
  !
  subroutine index_iterator_comm_parse(comm,line,user,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(index_iterator_comm_t), intent(in)    :: comm
    character(len=*),             intent(in)    :: line
    type(index_iterator_user_t),  intent(inout) :: user
    logical,                      intent(inout) :: error
    !
    integer(kind=code_k) :: ikey,nkey
    character(len=*), parameter :: rname='INDEX>ITERATOR>PARSE'
    !
    call cubego_message(goseve%trace,rname,'Welcome')
    !
    nkey = 0
    user%code = code_iter_unknown
    do ikey=1,ncode_iter
       call comm%keys(ikey)%parse(line,user%keys(ikey),error)
       if (error) return
       if (user%keys(ikey)%present) then
          nkey = nkey+1 
          user%code = ikey
       endif
    enddo ! ikey
    if (nkey.ge.2) then
       call cubego_message(seve%e,rname,'Iteration options are mutually exclusive')
       error = .true.
       return
    endif
  end subroutine index_iterator_comm_parse
end module cubego_index_iterator_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
