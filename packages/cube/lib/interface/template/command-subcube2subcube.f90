!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetemplate_subcube2subcube
  use cubetools_parameters
  use cube_types
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubetemplate_factor_types
  use cubetopology_cuberegion_types
  use cubetemplate_messaging
  !
  public :: subcube2subcube
  private
  !
  type :: subcube2subcube_comm_t
     type(option_t), pointer :: comm
     type(factor_comm_t)     :: factor
     type(cuberegion_comm_t) :: region
     type(cubeid_arg_t), pointer :: incube
     type(cube_prod_t),  pointer :: oucube     
   contains
     procedure, public  :: register => cubetemplate_subcube2subcube_comm_register
     procedure, private :: parse    => cubetemplate_subcube2subcube_comm_parse
     procedure, private :: main     => cubetemplate_subcube2subcube_comm_main
  end type subcube2subcube_comm_t
  type(subcube2subcube_comm_t) :: subcube2subcube  
  !
  type subcube2subcube_user_t
     type(cubeid_user_t)     :: cubeids
     type(factor_user_t)     :: factor
     type(cuberegion_user_t) :: region
   contains
     procedure, private :: toprog => cubetemplate_subcube2subcube_user_toprog
  end type subcube2subcube_user_t
  !
  type subcube2subcube_prog_t
     type(cuberegion_prog_t) :: region
     type(factor_prog_t)     :: factor
     type(cube_t), pointer   :: incube
     type(cube_t), pointer   :: oucube
   contains
     procedure, private :: header => cubetemplate_subcube2subcube_prog_header
     procedure, private :: data   => cubetemplate_subcube2subcube_prog_data
     procedure, private :: loop   => cubetemplate_subcube2subcube_prog_loop
     procedure, private :: act    => cubetemplate_subcube2subcube_prog_act
  end type subcube2subcube_prog_t
  !
contains
  !
  subroutine cubetemplate_subcube2subcube_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(subcube2subcube_user_t) :: user
    character(len=*), parameter :: rname='SUBCUBE2SUBCUBE>COMMAND'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call subcube2subcube%parse(line,user,error)
    if (error) return
    call subcube2subcube%main(user,error)
    if (error) continue
  end subroutine cubetemplate_subcube2subcube_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetemplate_subcube2subcube_comm_register(comm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(subcube2subcube_comm_t), intent(inout) :: comm
    logical,                       intent(inout) :: error
    !
    type(cubeid_arg_t) :: incube
    type(cube_prod_t) :: oucube
    character(len=*), parameter :: comm_abstract='Template command to input per subcube and output per subcube'
    character(len=*), parameter :: comm_help='Input and output cubes are real'
    character(len=*), parameter :: rname='SUBCUBE2SUBCUBE>COMM>REGISTER'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    ! Syntax
    call cubetools_register_command(&
         'SUBCUBE2SUBCUBE','[cubeid]',&
         comm_abstract,&
         comm_help,&
         cubetemplate_subcube2subcube_command,&
         comm%comm,&
         error)
    if (error) return
    call incube%register(&
         'INPUT',&
         'Signal cube',&
         strg_id,&
         code_arg_optional,&
         [flag_any],&
         code_read,&
         code_access_subset,&
         comm%incube,&
         error)
    if (error) return
    call comm%factor%register(error)
    if (error) return
    call comm%region%register(error)
    if (error) return
    !
    ! Products
    call oucube%register(&
         'OUTPUT',&
         'Output cube',&
         strg_id,&
         [flag_template],&
         comm%oucube,&
         error)
    if (error)  return
  end subroutine cubetemplate_subcube2subcube_comm_register
  !
  subroutine cubetemplate_subcube2subcube_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! SUBCUBE2SUBCUBE cubname
    !----------------------------------------------------------------------
    class(subcube2subcube_comm_t), intent(in)    :: comm
    character(len=*),              intent(in)    :: line
    type(subcube2subcube_user_t),  intent(out)   :: user
    logical,                       intent(inout) :: error
    !
    character(len=*), parameter :: rname='SUBCUBE2SUBCUBE>COMM>PARSE'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%region%parse(line,user%region,error)
    if (error) return
    call comm%factor%parse(line,user%factor,error)
    if (error) return
  end subroutine cubetemplate_subcube2subcube_comm_parse
  !
  subroutine cubetemplate_subcube2subcube_comm_main(comm,user,error)
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(subcube2subcube_comm_t), intent(in)    :: comm
    type(subcube2subcube_user_t),  intent(inout) :: user
    logical,                       intent(inout) :: error
    !
    type(subcube2subcube_prog_t) :: prog
    character(len=*), parameter :: rname='SUBCUBE2SUBCUBE>MAIN'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubetemplate_subcube2subcube_comm_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetemplate_subcube2subcube_user_toprog(user,comm,prog,error)
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(subcube2subcube_user_t), intent(in)    :: user
    type(subcube2subcube_comm_t),  intent(in)    :: comm
    type(subcube2subcube_prog_t),  intent(out)   :: prog
    logical,                       intent(inout) :: error
    !
    character(len=*), parameter :: rname='SUBCUBE2SUBCUBE>USER>TOPROG'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%incube,user%cubeids,prog%incube,error)
    if (error) return
    call user%factor%toprog(prog%factor,error)
    if (error) return
    call user%region%toprog(prog%incube,prog%region,error)
    if (error) return
    ! User feedback about the interpretation of his command line
    call prog%factor%list(error)
    if (error) return
    call prog%region%list(error)
    if (error) return
  end subroutine cubetemplate_subcube2subcube_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetemplate_subcube2subcube_prog_header(prog,comm,error)
    use cubeadm_clone
    use cubetools_header_methods
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(subcube2subcube_prog_t), intent(inout) :: prog
    type(subcube2subcube_comm_t),  intent(in)    :: comm
    logical,                       intent(inout) :: error
    !
    character(len=*), parameter :: rname='SUBCUBE2SUBCUBE>PROG>HEADER'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call cubeadm_clone_header_with_region(comm%oucube,  &
      prog%incube,prog%region,prog%oucube,error)
    if (error) return
    call prog%factor%header(prog%oucube,error)
    if (error) return
  end subroutine cubetemplate_subcube2subcube_prog_header
  !
  subroutine cubetemplate_subcube2subcube_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(subcube2subcube_prog_t), intent(inout) :: prog
    logical,                       intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='SUBCUBE2SUBCUBE>PROG>DATA'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,prog%region,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubetemplate_subcube2subcube_prog_data
  !   
  subroutine cubetemplate_subcube2subcube_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_subcube_types
    use cubeadm_subcube_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(subcube2subcube_prog_t), intent(inout) :: prog
    type(cubeadm_iterator_t),      intent(inout) :: iter
    logical,                       intent(inout) :: error
    !
    character(len=*), parameter :: rname='SUBCUBE2SUBCUBE>PROG>LOOP'
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter,error)
      if (error) return
    enddo
  end subroutine cubetemplate_subcube2subcube_prog_loop
  !   
  subroutine cubetemplate_subcube2subcube_prog_act(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_subcube_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(subcube2subcube_prog_t), intent(inout) :: prog
    type(cubeadm_iterator_t),      intent(in)    :: iter
    logical,                       intent(inout) :: error
    !
    integer(kind=indx_k) :: ix,iy,iz
    type(subcube_t) :: insub,ousub
    character(len=*), parameter :: rname='SUBCUBE2SUBCUBE>PROG>ACT'
    !
    ! Subcubes are initialized here as their size (3rd dim) may change from
    ! from one subcube to another.
    call insub%associate('insub',prog%incube,iter,error)
    if (error) return
    call ousub%allocate('ousub',prog%oucube,iter,error)
    if (error) return
    !
    call insub%get(error)
    if (error) return
    do iz=1,insub%nz
       do iy=1,insub%ny
          do ix=1,insub%nx
             ousub%val(ix,iy,iz) = prog%factor%val*insub%val(ix,iy,iz)
          enddo ! ix
       enddo ! iy
    enddo ! iz
    call ousub%put(error)
    if (error) return
  end subroutine cubetemplate_subcube2subcube_prog_act
end module cubetemplate_subcube2subcube
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
