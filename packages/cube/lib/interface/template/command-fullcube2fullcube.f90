!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetemplate_fullcube2fullcube
  use cubetools_parameters
  use cube_types
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubetemplate_factor_types
  use cubetopology_cuberegion_types
  use cubetemplate_messaging
  !
  public :: fullcube2fullcube
  private
  !
  type :: fullcube2fullcube_comm_t
     type(option_t), pointer :: comm  
     type(factor_comm_t)     :: factor
     type(cuberegion_comm_t) :: region
     type(cubeid_arg_t), pointer :: incube
     type(cube_prod_t),  pointer :: oucube
  contains
     procedure, public  :: register => cubetemplate_fullcube2fullcube_comm_register
     procedure, private :: parse    => cubetemplate_fullcube2fullcube_comm_parse
     procedure, private :: main     => cubetemplate_fullcube2fullcube_comm_main
  end type fullcube2fullcube_comm_t
  type(fullcube2fullcube_comm_t) :: fullcube2fullcube
  !
  type fullcube2fullcube_user_t
     type(cubeid_user_t)     :: cubeids
     type(factor_user_t)     :: factor
     type(cuberegion_user_t) :: region
   contains
     procedure, private :: toprog => cubetemplate_fullcube2fullcube_user_toprog
  end type fullcube2fullcube_user_t
  !
  type fullcube2fullcube_prog_t
     type(cuberegion_prog_t) :: region
     type(factor_prog_t)     :: factor
     type(cube_t), pointer   :: incube
     type(cube_t), pointer   :: oucube
   contains
     procedure, private :: header => cubetemplate_fullcube2fullcube_prog_header
     procedure, private :: data   => cubetemplate_fullcube2fullcube_prog_data
     procedure, private :: loop   => cubetemplate_fullcube2fullcube_prog_loop
     procedure, private :: act    => cubetemplate_fullcube2fullcube_prog_act
  end type fullcube2fullcube_prog_t
  !
contains
  !
  subroutine cubetemplate_fullcube2fullcube_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(fullcube2fullcube_user_t) :: user
    character(len=*), parameter :: rname='FULLCUBE2FULLCUBE>COMMAND'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call fullcube2fullcube%parse(line,user,error)
    if (error) return
    call fullcube2fullcube%main(user,error)
    if (error) continue
  end subroutine cubetemplate_fullcube2fullcube_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetemplate_fullcube2fullcube_comm_register(comm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(fullcube2fullcube_comm_t), intent(inout) :: comm
    logical,                         intent(inout) :: error
    !
    type(cubeid_arg_t) :: incube
    type(cube_prod_t) :: oucube
    character(len=*), parameter :: comm_abstract = 'Template command to access input/output data per fullcube'
    character(len=*), parameter :: comm_help = 'Input and output cube are real'
    character(len=*), parameter :: rname='FULLCUBE2FULLCUBE>COMM>REGISTER'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
!         'FULLCUBE2FULLCUBE','[cubeid]',& ! *** JP: SIC limitation?
         'FULCUBE2FULCUBE','[cubeid]',&
         comm_abstract,&
         comm_help,&
         cubetemplate_fullcube2fullcube_command,&
         comm%comm,&
         error)
    if (error) return
    call incube%register(&
         'INPUT',&
         'Signal cube',&
         strg_id,&
         code_arg_optional,&
         [flag_any],&
         code_read,&
         code_access_fullset,&
         comm%incube,&
         error)
    if (error) return
    call comm%factor%register(error)
    if (error) return
    call comm%region%register(error)
    if (error) return
    !
    ! Products
    call oucube%register(&
         'OUTPUT',&
         'Output cube',&
         strg_id,&
         [flag_template],&
         comm%oucube,&
         error)
    if (error)  return
  end subroutine cubetemplate_fullcube2fullcube_comm_register
  !
  subroutine cubetemplate_fullcube2fullcube_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! fulcube2fulcube cubeid
    !----------------------------------------------------------------------
    class(fullcube2fullcube_comm_t), intent(in)    :: comm
    character(len=*),                intent(in)    :: line
    type(fullcube2fullcube_user_t),  intent(out)   :: user
    logical,                         intent(inout) :: error
    !
    character(len=*), parameter :: rname='FULLCUBE2FULLCUBE>COMM>PARSE'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%region%parse(line,user%region,error)
    if (error) return
    call comm%factor%parse(line,user%factor,error)
    if (error) return
  end subroutine cubetemplate_fullcube2fullcube_comm_parse
  !
  subroutine cubetemplate_fullcube2fullcube_comm_main(comm,user,error) 
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(fullcube2fullcube_comm_t), intent(in)    :: comm
    type(fullcube2fullcube_user_t),  intent(inout) :: user
    logical,                         intent(inout) :: error
    !
    type(fullcube2fullcube_prog_t) :: prog
    character(len=*), parameter :: rname='FULLCUBE2FULLCUBE>COMM>MAIN'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubetemplate_fullcube2fullcube_comm_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetemplate_fullcube2fullcube_user_toprog(user,comm,prog,error)
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(fullcube2fullcube_user_t), intent(in)    :: user
    type(fullcube2fullcube_comm_t),  intent(in)    :: comm
    type(fullcube2fullcube_prog_t),  intent(out)   :: prog
    logical,                         intent(inout) :: error
    !
    character(len=*), parameter :: rname='FULLCUBE2FULLCUBE>USER>TOPROG'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%incube,user%cubeids,prog%incube,error)
    if (error) return
    call user%factor%toprog(prog%factor,error)
    if (error) return
    call user%region%toprog(prog%incube,prog%region,error)
    if (error) return
    ! User feedback about the interpretation of his command line
    call prog%factor%list(error)
    if (error) return
    call prog%region%list(error)
    if (error) return
  end subroutine cubetemplate_fullcube2fullcube_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetemplate_fullcube2fullcube_prog_header(prog,comm,error)
    use cubeadm_clone
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(fullcube2fullcube_prog_t), intent(inout) :: prog
    type(fullcube2fullcube_comm_t),  intent(in)    :: comm
    logical,                         intent(inout) :: error
    !
    character(len=*), parameter :: rname='FULLCUBE2FULLCUBE>PROG>HEADER'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call cubeadm_clone_header(comm%oucube,prog%incube,prog%oucube,error)
    if (error) return
    call prog%factor%header(prog%oucube,error)
    if (error) return
    call prog%region%header(prog%oucube,error)
    if (error) return
  end subroutine cubetemplate_fullcube2fullcube_prog_header
  !
  subroutine cubetemplate_fullcube2fullcube_prog_data(prog,error)
    use cubeadm_opened
    use cubeadm_taskloop
    use cubeadm_fullcube_types
    !----------------------------------------------------------------------
    ! Structure is simplified compared to other cases because only one task
    ! can work on a full cube at a time by construction!
    !----------------------------------------------------------------------
    class(fullcube2fullcube_prog_t), intent(inout) :: prog
    logical,                         intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    type(fullcube_t) :: infull,oufull
    character(len=*), parameter :: rname='FULLCUBE2FULLCUBE>PROG>DATA'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    ! 1. Open in and out cubes
    ! 2. Prepare parallel tasking (here only one task i.e. no parallelization)
    ! 2. Define iterator of entries (here only one fullcube)
    ! 3. Command feedback
    call cubeadm_datainit_all(iter,error)
    if (error) return
    !
    ! Note on full cubes association/allocation: in the current command
    ! we know there is only 1 iteration of the tasks (cubeadm_dataiterate_all)
    ! and 1 iteration of the entries (iterate_entry), so this can be done
    ! at any level without troubles. For symmetry with the others entry types,
    ! we should do it in cubetemplate_fullcube2fullcube_prog_loop. However,
    ! when using mixed accesses like in SLICE command (iterate the input cube
    ! as images, into a full cube as output), there are several iterations, so
    ! we DO want to factorize the association/allocation of the full cubes out
    ! of the loops, just here:
    ! Prepare the input cube for getting data
    call infull%associate('infull',prog%incube,error)
    if (error) return
    ! Prepare the output cube for putting data
    call oufull%allocate('oufull',prog%oucube,error)
    if (error) return
    !
    ! Loop on task => Only one task, (i.e. one iteration) => no parallelization statements
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       if (.not.error) &
         call prog%loop(iter,infull,oufull,error)
    enddo ! itask
  end subroutine cubetemplate_fullcube2fullcube_prog_data
  !
  subroutine cubetemplate_fullcube2fullcube_prog_loop(prog,iter,infull,oufull,error)
    use cubeadm_taskloop
    use cubeadm_fullcube_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(fullcube2fullcube_prog_t), intent(inout) :: prog
    type(cubeadm_iterator_t),        intent(inout) :: iter
    type(fullcube_t),                intent(inout) :: infull
    type(fullcube_t),                intent(inout) :: oufull
    logical,                         intent(inout) :: error
    !
    character(len=*), parameter :: rname='FULLCUBE2FULLCUBE>PROG>LOOP'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    do while (iter%iterate_entry(error))  ! Only 1 iteration in this case
      ! Get the input full cube
      call infull%get(error)
      if (error) return
      ! Do the computation
      call prog%act(infull,oufull,error)
      if (error) return
      ! Put the output full cube
      call oufull%put(error)
      if (error) return
    enddo  ! ientry
  end subroutine cubetemplate_fullcube2fullcube_prog_loop
  !
  subroutine cubetemplate_fullcube2fullcube_prog_act(prog,infull,oufull,error)
    use cubeadm_fullcube_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(fullcube2fullcube_prog_t), intent(inout) :: prog
    type(fullcube_t),                intent(inout) :: infull
    type(fullcube_t),                intent(inout) :: oufull
    logical,                         intent(inout) :: error
    !
    integer(kind=indx_k) :: ix,iy,iz
    character(len=*), parameter :: rname='FULLCUBE2FULLCUBE>PROG>ACT'
    !
    do iz=1,infull%nz
       do iy=1,infull%ny
          do ix=1,infull%nx
             oufull%val(ix,iy,iz) = prog%factor%val*infull%val(ix,iy,iz)
          enddo ! ix
       enddo ! iy
    enddo ! iz
  end subroutine cubetemplate_fullcube2fullcube_prog_act
end module cubetemplate_fullcube2fullcube
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
