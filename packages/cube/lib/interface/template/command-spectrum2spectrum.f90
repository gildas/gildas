!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetemplate_spectrum2spectrum
  use cubetools_parameters
  use cube_types
  use cubetools_structure
  use cubetopology_cuberegion_types
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubetemplate_factor_types
  use cubetemplate_messaging
  !
  public :: spectrum2spectrum
  private
  !
  type :: spectrum2spectrum_comm_t
     type(option_t), pointer :: comm
     type(factor_comm_t)     :: factor
     type(cuberegion_comm_t) :: region
     type(cubeid_arg_t), pointer :: incube
     type(cube_prod_t),  pointer :: oucube
   contains
     procedure, public  :: register => cubetemplate_spectrum2spectrum_comm_register
     procedure, private :: parse    => cubetemplate_spectrum2spectrum_comm_parse
     procedure, private :: main     => cubetemplate_spectrum2spectrum_comm_main
  end type spectrum2spectrum_comm_t
  type(spectrum2spectrum_comm_t) :: spectrum2spectrum
  !
  type spectrum2spectrum_user_t
     type(cubeid_user_t)     :: cubeids
     type(factor_user_t)     :: factor
     type(cuberegion_user_t) :: region
   contains
     procedure, private :: toprog => cubetemplate_spectrum2spectrum_user_toprog
  end type spectrum2spectrum_user_t
  !
  type spectrum2spectrum_prog_t
     type(cuberegion_prog_t) :: region
     type(factor_prog_t)     :: factor
     type(cube_t), pointer   :: incube
     type(cube_t), pointer   :: oucube
   contains
     procedure, private :: header => cubetemplate_spectrum2spectrum_prog_header
     procedure, private :: data   => cubetemplate_spectrum2spectrum_prog_data
     procedure, private :: loop   => cubetemplate_spectrum2spectrum_prog_loop
     procedure, private :: act    => cubetemplate_spectrum2spectrum_prog_act
  end type spectrum2spectrum_prog_t
  !
contains
  !
  subroutine cubetemplate_spectrum2spectrum_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(spectrum2spectrum_user_t) :: user
    character(len=*), parameter :: rname='SPECTRUM2SPECTRUM>COMMAND'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call spectrum2spectrum%parse(line,user,error)
    if (error) return
    call spectrum2spectrum%main(user,error)
    if (error) continue
  end subroutine cubetemplate_spectrum2spectrum_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetemplate_spectrum2spectrum_comm_register(comm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spectrum2spectrum_comm_t), intent(inout) :: comm
    logical,                         intent(inout) :: error
    !
    type(cubeid_arg_t) :: incube
    type(cube_prod_t) :: oucube
    character(len=*), parameter :: comm_abstract='Template to access input/output data per spectrum'
    character(len=*), parameter :: comm_help='Input and output cubes are real'
    character(len=*), parameter :: rname='SPECTRUM2SPECTRUM>COMM>REGISTER'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    ! Syntax
    call cubetools_register_command(&
!        'SPECTRUM2SPECTRUM','[cubeid]',& ! *** JP: SIC limitation?
         'SPEC2SPEC','[cubeid]',&
         comm_abstract,&
         comm_help,&
         cubetemplate_spectrum2spectrum_command,&
         comm%comm,&
         error)
    if (error) return
    call incube%register(&
         'INPUT',&
         'Signal cube',&
         strg_id,&
         code_arg_optional,&
         [flag_any],&
         code_read,&
         code_access_speset,&
         comm%incube,&
         error)
    if (error) return
    call comm%factor%register(error)
    if (error) return
    call comm%region%register(error)
    if (error) return
    !
    ! Products
    call oucube%register(&
         'OUTPUT',&
         'Output cube',&
         strg_id,&
         [flag_template],&
         comm%oucube,&
         error)
    if (error)  return
  end subroutine cubetemplate_spectrum2spectrum_comm_register
  !
  subroutine cubetemplate_spectrum2spectrum_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! SPEC2SPEC cubeid
    !----------------------------------------------------------------------
    class(spectrum2spectrum_comm_t), intent(in)    :: comm
    character(len=*),                intent(in)    :: line
    type(spectrum2spectrum_user_t),  intent(out)   :: user
    logical,                         intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRUM2SPECTRUM>COMM>PARSE'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%region%parse(line,user%region,error)
    if (error) return
    call comm%factor%parse(line,user%factor,error)
    if (error) return
  end subroutine cubetemplate_spectrum2spectrum_comm_parse
  !
  subroutine cubetemplate_spectrum2spectrum_comm_main(comm,user,error) 
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spectrum2spectrum_comm_t), intent(in)    :: comm
    type(spectrum2spectrum_user_t),  intent(inout) :: user
    logical,                         intent(inout) :: error
    !
    type(spectrum2spectrum_prog_t) :: prog
    character(len=*), parameter :: rname='SPECTRUM2SPECTRUM>COMM>MAIN'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubetemplate_spectrum2spectrum_comm_main
  !
  !------------------------------------------------------------------------
  !
  subroutine cubetemplate_spectrum2spectrum_user_toprog(user,comm,prog,error)
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spectrum2spectrum_user_t), intent(in)    :: user
    type(spectrum2spectrum_comm_t),  intent(in)    :: comm    
    type(spectrum2spectrum_prog_t),  intent(out)   :: prog
    logical,                         intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRUM2SPECTRUM>USER>TOPROG'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%incube,user%cubeids,prog%incube,error)
    if (error) return
    call user%factor%toprog(prog%factor,error)
    if (error) return
    call user%region%toprog(prog%incube,prog%region,error)
    if (error) return
    ! User feedback about the interpretation of his command line
    call prog%factor%list(error)
    if (error) return
    call prog%region%list(error)
    if (error) return
  end subroutine cubetemplate_spectrum2spectrum_user_toprog
  !
  !------------------------------------------------------------------------
  !
  subroutine cubetemplate_spectrum2spectrum_prog_header(prog,comm,error)
    use cubeadm_clone
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spectrum2spectrum_prog_t), intent(inout) :: prog
    type(spectrum2spectrum_comm_t),  intent(in)    :: comm
    logical,                         intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRUM2SPECTRUM>PROG>HEADER'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call cubeadm_clone_header_with_region(comm%oucube,  &
      prog%incube,prog%region,prog%oucube,error)
    if (error) return
    call prog%factor%header(prog%oucube,error)
    if (error) return
  end subroutine cubetemplate_spectrum2spectrum_prog_header
  !
  subroutine cubetemplate_spectrum2spectrum_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(spectrum2spectrum_prog_t), intent(inout) :: prog
    logical,                         intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='SPECTRUM2SPECTRUM>PROG>DATA'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,prog%region,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubetemplate_spectrum2spectrum_prog_data
  !   
  subroutine cubetemplate_spectrum2spectrum_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spectrum2spectrum_prog_t), intent(inout) :: prog
    type(cubeadm_iterator_t),        intent(inout) :: iter
    logical,                         intent(inout) :: error
    !
    type(spectrum_t) :: input,output
    character(len=*), parameter :: rname='SPECTRUM2SPECTRUM>PROG>LOOP'
    !
    call input%associate('input',prog%incube,iter,error)
    if (error) return
    call output%allocate('output',prog%oucube,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter%ie,input,output,error)
      if (error) return
    enddo ! ie
  end subroutine cubetemplate_spectrum2spectrum_prog_loop
  !   
  subroutine cubetemplate_spectrum2spectrum_prog_act(prog,ie,input,output,error)
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spectrum2spectrum_prog_t), intent(inout) :: prog
    integer(kind=entr_k),            intent(in)    :: ie
    type(spectrum_t),                intent(inout) :: input
    type(spectrum_t),                intent(inout) :: output
    logical,                         intent(inout) :: error
    !
    integer(kind=chan_k) :: ic
    character(len=*), parameter :: rname='SPECTRUM2SPECTRUM>PROG>ACT'
    !
    call input%get(ie,error)
    if (error) return
    do ic=1,input%n
      output%y%val(ic) = prog%factor%val*input%y%val(ic)
    enddo ! ic
    call output%put(ie,error)
    if (error) return
  end subroutine cubetemplate_spectrum2spectrum_prog_act
end module cubetemplate_spectrum2spectrum
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
