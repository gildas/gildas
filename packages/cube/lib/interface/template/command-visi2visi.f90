!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetemplate_visi2visi
  use cubetools_parameters
  use cube_types
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubetemplate_factor_types
  use cubetopology_cuberegion_types
  use cubetemplate_messaging
  !
  public :: visi2visi
  private
  !
  type :: visi2visi_comm_t
     type(option_t), pointer :: comm
     type(factor_comm_t)     :: factor
     type(cuberegion_comm_t) :: region
     type(cubeid_arg_t), pointer :: incube
     type(cube_prod_t),  pointer :: oucube
   contains
     procedure, public  :: register => cubetemplate_visi2visi_comm_register
     procedure, private :: parse    => cubetemplate_visi2visi_comm_parse
     procedure, private :: main     => cubetemplate_visi2visi_comm_main
  end type visi2visi_comm_t
  type(visi2visi_comm_t) :: visi2visi
  !
  type visi2visi_user_t
     type(cubeid_user_t)     :: cubeids
     type(factor_user_t)     :: factor
     type(cuberegion_user_t) :: region
   contains
     procedure, private :: toprog => cubetemplate_visi2visi_user_toprog
  end type visi2visi_user_t
  !
  type visi2visi_prog_t
     type(cuberegion_prog_t) :: region
     type(factor_prog_t)     :: factor
     type(cube_t), pointer   :: incube
     type(cube_t), pointer   :: oucube
   contains
     procedure, private :: header => cubetemplate_visi2visi_prog_header
     procedure, private :: data   => cubetemplate_visi2visi_prog_data
     procedure, private :: loop   => cubetemplate_visi2visi_prog_loop
     procedure, private :: act    => cubetemplate_visi2visi_prog_act
  end type visi2visi_prog_t
  !
contains
  !
  subroutine cubetemplate_visi2visi_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(visi2visi_user_t) :: user
    character(len=*), parameter :: rname='VISI2VISI>COMMAND'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call visi2visi%parse(line,user,error)
    if (error) return
    call visi2visi%main(user,error)
    if (error) continue
  end subroutine cubetemplate_visi2visi_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetemplate_visi2visi_comm_register(comm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(visi2visi_comm_t), intent(inout) :: comm
    logical,                 intent(inout) :: error
    !
    type(cubeid_arg_t) :: incube
    type(cube_prod_t) :: oucube
    character(len=*), parameter :: comm_abstract = 'Template command to access input/output data per visi'
    character(len=*), parameter :: comm_help = 'Input and output cubes are complex'
    character(len=*), parameter :: rname='VISI2VISI>COMM>REGISTER'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    ! Syntax
    call cubetools_register_command(&
         'VISI2VISI','[cubeid]',&
         comm_abstract,&
         comm_help,&
         cubetemplate_visi2visi_command,&
         comm%comm,&
         error)
    if (error) return
    call incube%register(&
         'INPUT',&
         'Signal cube',&
         strg_id,&
         code_arg_optional,&
         [flag_any],&
         code_read,&
         code_access_imaset,&
         comm%incube,&
         error)
    if (error) return
    call comm%factor%register(error)
    if (error) return
    call comm%region%register(error)
    if (error) return
    !
    ! Products
    call oucube%register(&
         'OUTPUT',&
         'Output cube',&
         strg_id,&
         [flag_template],&
         comm%oucube,&
         error)
    if (error)  return
  end subroutine cubetemplate_visi2visi_comm_register
  !
  subroutine cubetemplate_visi2visi_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! VISI2VISI cubeid
    !----------------------------------------------------------------------
    class(visi2visi_comm_t), intent(in)    :: comm
    character(len=*),        intent(in)    :: line
    type(visi2visi_user_t),  intent(out)   :: user
    logical,                 intent(inout) :: error
    !
    character(len=*), parameter :: rname='VISI2VISI>COMM>PARSE'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%region%parse(line,user%region,error)
    if (error) return
    call comm%factor%parse(line,user%factor,error)
    if (error) return
  end subroutine cubetemplate_visi2visi_comm_parse
  !
  subroutine cubetemplate_visi2visi_comm_main(comm,user,error) 
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(visi2visi_comm_t), intent(in)    :: comm
    type(visi2visi_user_t),  intent(inout) :: user
    logical,                 intent(inout) :: error
    !
    type(visi2visi_prog_t) :: prog
    character(len=*), parameter :: rname='VISI2VISI>COMM>MAIN'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubetemplate_visi2visi_comm_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetemplate_visi2visi_user_toprog(user,comm,prog,error)
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(visi2visi_user_t), intent(in)    :: user
    type(visi2visi_comm_t),  intent(in)    :: comm
    type(visi2visi_prog_t),  intent(out)   :: prog
    logical,                 intent(inout) :: error
    !
    character(len=*), parameter :: rname='VISI2VISI>USER>TOPROG'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%incube,user%cubeids,prog%incube,error)
    if (error) return
    call user%factor%toprog(prog%factor,error)
    if (error) return
    call user%region%toprog(prog%incube,prog%region,error)
    if (error) return
    ! User feedback about the interpretation of his command line
    call prog%factor%list(error)
    if (error) return
    call prog%region%list(error)
    if (error) return
  end subroutine cubetemplate_visi2visi_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetemplate_visi2visi_prog_header(prog,comm,error)
    use cubeadm_clone
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(visi2visi_prog_t), intent(inout) :: prog
    type(visi2visi_comm_t),  intent(in)    :: comm
    logical,                 intent(inout) :: error
    !
    character(len=*), parameter :: rname='VISI2VISI>PROG>HEADER'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call cubeadm_clone_header(comm%oucube,prog%incube,prog%oucube,error)
    if (error) return
  end subroutine cubetemplate_visi2visi_prog_header
  !
  subroutine cubetemplate_visi2visi_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(visi2visi_prog_t), intent(inout) :: prog
    logical,                 intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='VISI2VISI>PROG>DATA'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo ! ie
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubetemplate_visi2visi_prog_data
  !   
  subroutine cubetemplate_visi2visi_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(visi2visi_prog_t),  intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(visi_t) :: inima,ouima
    character(len=*), parameter :: rname='VISI2VISI>PROG>LOOP'
    !
    call inima%associate('inima',prog%incube,iter,error)
    if (error) return
    call ouima%allocate('ouima',prog%oucube,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter%ie,inima,ouima,error)
      if (error) return
    enddo ! ie
  end subroutine cubetemplate_visi2visi_prog_loop
  !   
  subroutine cubetemplate_visi2visi_prog_act(prog,ie,inima,ouima,error)
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(visi2visi_prog_t), intent(inout) :: prog
    integer(kind=entr_k),    intent(in)    :: ie
    type(visi_t),            intent(inout) :: inima
    type(visi_t),            intent(inout) :: ouima
    logical,                 intent(inout) :: error
    !
    integer(kind=pixe_k) :: ix,iy
    character(len=*), parameter :: rname='VISI2VISI>PROG>ACT'
    !
    call inima%get(ie,error)
    if (error) return
    do iy=1,inima%ny
       do ix=1,inima%nx
          ouima%val(ix,iy) = prog%factor%val*cmplx(real(inima%val(ix,iy)),ix+iy+ie)
       enddo ! iy
    enddo ! ix
    call ouima%put(ie,error)
    if (error) return
  end subroutine cubetemplate_visi2visi_prog_act
end module cubetemplate_visi2visi
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
