!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetemplate_image2image_template
  use cubetemplate_messaging
  use cubetemplate_one2one_template
  !
  public :: image2image_comm_t,image2image_user_t,image2image_prog_t
  private
  !
  type, extends(one2one_comm_t) :: image2image_comm_t
  end type image2image_comm_t
  !
  type, extends(one2one_user_t) :: image2image_user_t
  end type image2image_user_t
  !
  type, extends(one2one_prog_t) :: image2image_prog_t
   contains
     procedure, private :: loop   => cubetemplate_image2image_prog_loop
     procedure, private :: act    => cubetemplate_image2image_prog_act
  end type image2image_prog_t
  !
contains
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetemplate_image2image_prog_loop(prog,iter,error)
    use cubetools_parameters
    use cubeadm_taskloop
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(image2image_prog_t), intent(inout) :: prog
    type(cubeadm_iterator_t),  intent(inout) :: iter
    logical,                   intent(inout) :: error
    !
    type(image_t) :: input,output
    character(len=*), parameter :: rname='IMAGE2IMAGE>PROG>HEADER'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call input%associate('input',prog%incube,iter,error)
    if (error) return
    call output%allocate('output',prog%oucube,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter%ie,input,output,error)
      if (error) return
    enddo
  end subroutine cubetemplate_image2image_prog_loop
  !
  subroutine cubetemplate_image2image_prog_act(prog,ie,input,output,error)
    use cubetools_parameters
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(image2image_prog_t), intent(inout) :: prog
    integer(kind=entr_k),      intent(in)    :: ie
    type(image_t),             intent(inout) :: input
    type(image_t),             intent(inout) :: output
    logical,                   intent(inout) :: error
    !
    character(len=*), parameter :: rname='IMAGE2IMAGE>PROG>ACT'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
  end subroutine cubetemplate_image2image_prog_act
end module cubetemplate_image2image_template
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
