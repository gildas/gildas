!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetemplate_twosubc2onesubc_template
  use cubetemplate_messaging
  use cubetemplate_two2one_template
  !
  public :: twosubc2onesubc_comm_t,twosubc2onesubc_user_t,twosubc2onesubc_prog_t
  private
  !
  type, extends(two2one_comm_t) :: twosubc2onesubc_comm_t
  end type twosubc2onesubc_comm_t
  !
  type, extends(two2one_user_t) :: twosubc2onesubc_user_t
  end type twosubc2onesubc_user_t
  !
  type, extends(two2one_prog_t) :: twosubc2onesubc_prog_t
   contains
     procedure, private :: loop => cubetemplate_twosubc2onesubc_prog_loop
     procedure, private :: act  => cubetemplate_twosubc2onesubc_prog_act
  end type twosubc2onesubc_prog_t
  !
contains
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetemplate_twosubc2onesubc_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(twosubc2onesubc_prog_t), intent(inout) :: prog
    type(cubeadm_iterator_t),      intent(inout) :: iter
    logical,                       intent(inout) :: error
    !
    character(len=*), parameter :: rname='TWOSUBC2ONESUBC>PROG>LOOP'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter,error)
      if (error) return
    enddo
  end subroutine cubetemplate_twosubc2onesubc_prog_loop
  !
  subroutine cubetemplate_twosubc2onesubc_prog_act(prog,iter,error)
    use cubeadm_taskloop
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(twosubc2onesubc_prog_t), intent(inout) :: prog
    type(cubeadm_iterator_t),      intent(in)    :: iter
    logical,                       intent(inout) :: error
    !
    character(len=*), parameter :: rname='TWOSUBC2ONESUBC>PROG>ACT'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
  end subroutine cubetemplate_twosubc2onesubc_prog_act
end module cubetemplate_twosubc2onesubc_template
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
