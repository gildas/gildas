!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetemplate_subcube2subcube
  use cubetools_structure
  use cubetemplate_one2one_template
  use cubetemplate_subcube2subcube_template
  use cubetemplate_factor_types
  use cubetemplate_cuberegion_types
  use cubetemplate_messaging
  !
  public :: subcube2subcube
  private
  !
  type, extends(subcube2subcube_comm_t) :: ext_subcube2subcube_comm_t
     type(factor_comm_t)     :: factor
     type(cuberegion_comm_t) :: region
   contains
     procedure, public :: register => cubetemplate_ext_subcube2subcube_comm_register
     procedure, public :: parse    => cubetemplate_ext_subcube2subcube_comm_parse
  end type ext_subcube2subcube_comm_t
  type(ext_subcube2subcube_comm_t) :: subcube2subcube
  !
  type, extends(subcube2subcube_user_t) :: ext_subcube2subcube_user_t
     type(factor_user_t)     :: factor
     type(cuberegion_user_t) :: region
   contains
     procedure, public :: toprog => cubetemplate_ext_subcube2subcube_user_toprog
  end type ext_subcube2subcube_user_t
  !
  type, extends(subcube2subcube_prog_t) :: ext_subcube2subcube_prog_t
     type(cuberegion_prog_t) :: region
     type(factor_prog_t)     :: factor
   contains
     procedure, private :: act => cubetemplate_ext_subcube2subcube_prog_act
  end type ext_subcube2subcube_prog_t
  !
contains
  !
  subroutine cubetemplate_subcube2subcube_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(ext_subcube2subcube_user_t) :: user
    type(ext_subcube2subcube_prog_t) :: prog
    character(len=*), parameter :: rname='SUBCUBE2SUBCUBE>COMMAND'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call subcube2subcube%command(line,user,prog,error)
    if (error) return
  end subroutine cubetemplate_subcube2subcube_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetemplate_ext_subcube2subcube_comm_register(comm,error)
    use cubedag_allflags
    use cubeadm_cubeid_types
    use cubeadm_cubeprod_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(ext_subcube2subcube_comm_t), intent(inout) :: comm
    logical,                           intent(inout) :: error
    !
    type(cubeid_arg_t) :: incube
    type(cube_prod_t) :: oucube
    character(len=*), parameter :: comm_abstract = 'Template command to access input/output data per subcubes'
    character(len=*), parameter :: comm_help = 'Input and output cubes are real'
    character(len=*), parameter :: rname='SUBCUBE2SUBCUBE>COMM>REGISTER'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    ! Syntax
    call cubetools_register_command(&
         'SUBCUBE2SUBCUBE','[cube]',&
         comm_abstract,&
         comm_help,&
         cubetemplate_subcube2subcube_command,&
         comm%comm,error)
    if (error) return
    call incube%register(&
         'CUBE',&
         'Signal cube',&
         strg_id,&
         code_arg_optional,&
         [flag_any],&
         code_read,&
         code_access_subset,&
         comm%incube,&
         error)
    if (error) return
    call comm%factor%register(error)
    if (error) return
    call comm%region%register(error)
    if (error) return
    !
    ! Products
    call oucube%register(  &
         'CUBE',  &
         'The result of the command',  &
         strg_id,  &
         [flag_template],  &
         comm%oucube,  &
         error)
    if (error)  return
  end subroutine cubetemplate_ext_subcube2subcube_comm_register
  !
  subroutine cubetemplate_ext_subcube2subcube_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! SUBCUBE2SUBCUBE cubeid
    !----------------------------------------------------------------------
    class(ext_subcube2subcube_comm_t), intent(in)    :: comm
    character(len=*),                  intent(in)    :: line
    class(one2one_user_t),             intent(out)   :: user
    logical,                           intent(inout) :: error
    !
    type(ext_subcube2subcube_user_t), pointer :: iuser
    character(len=*), parameter :: rname='EXT>SUBCUBE2SUBCUBE>COMM>PARSE'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    iuser => ext_subcube2subcube_user_ptr(user,error)
    if (error)  return
    call comm%one2one_comm_t%parse(line,iuser,error)
    if (error) return
    call comm%region%parse(line,iuser%region,error)
    if (error) return
    call comm%factor%parse(line,iuser%factor,error)
    if (error) return
  end subroutine cubetemplate_ext_subcube2subcube_comm_parse
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetemplate_ext_subcube2subcube_user_toprog(user,comm,prog,error)
    use cubetools_unit
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(ext_subcube2subcube_user_t), intent(in)    :: user
    class(one2one_comm_t),             intent(in)    :: comm
    class(one2one_prog_t),             intent(inout) :: prog
    logical,                           intent(inout) :: error
    !
    type(ext_subcube2subcube_prog_t), pointer :: iprog
    character(len=*), parameter :: rname='SUBCUBE2SUBCUBE>USER>TOPROG'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call user%one2one_user_t%toprog(comm,prog,error)
    if (error) return
    !
    iprog => ext_subcube2subcube_prog_ptr(prog,error)
    if (error)  return
    call iprog%factor%header(iprog%oucube,error)
    if (error) return
    call iprog%region%header(iprog%oucube,error)
    if (error) return
  end subroutine cubetemplate_ext_subcube2subcube_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetemplate_ext_subcube2subcube_prog_act(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_subcube_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(ext_subcube2subcube_prog_t), intent(inout) :: prog
    type(cubeadm_iterator_t),          intent(in)    :: iter
    logical,                           intent(inout) :: error
    !
    integer(kind=indx_k) :: ix,iy,iz
    type(subcube_t) :: insub,ousub
    character(len=*), parameter :: rname='SUBCUBE2SUBCUBE>PROG>ACT'
    !
    ! Subcubes are initialized here as their size (3rd dim) may change from
    ! from one subcube to another.
    call insub%associate('insub',prog%incube,iter,error)
    if (error) return
    call ousub%allocate('ousub',prog%oucube,iter,error)
    if (error) return
    !
    call insub%get(error)
    if (error) return
    do iz=1,insub%nz
       do iy=1,insub%ny
          do ix=1,insub%nx
             ousub%val(ix,iy,iz) = prog%factor%val*insub%val(ix,iy,iz)
          enddo ! ix
       enddo ! iy
    enddo ! iz
    call ousub%put(error)
    if (error) return
  end subroutine cubetemplate_ext_subcube2subcube_prog_act
  !
  !----------------------------------------------------------------------
  !
  function ext_subcube2subcube_user_ptr(user,error)
    !-------------------------------------------------------------------
    ! Check if the input class is of type(ext_subcube2subcube_user_t), and
    ! return a pointer to it if yes.
    !-------------------------------------------------------------------
    type(ext_subcube2subcube_user_t), pointer       :: ext_subcube2subcube_user_ptr
    class(one2one_user_t),            target        :: user
    logical,                          intent(inout) :: error
    !
    character(len=*), parameter :: rname='EXT>SUBCUBE2SUBCUBE>USER>PTR'
    !
    select type(user)
    type is (ext_subcube2subcube_user_t)
      ext_subcube2subcube_user_ptr => user
    class default
      ext_subcube2subcube_user_ptr => null()
      call cubetemplate_message(seve%e,rname,  &
        'Internal error: object is not a ext_subcube2subcube_user_t type')
      error = .true.
      return
    end select
  end function ext_subcube2subcube_user_ptr
  !
  function ext_subcube2subcube_prog_ptr(prog,error)
    !-------------------------------------------------------------------
    ! Check if the input class is of type(ext_subcube2subcube_prog_t), and
    ! return a pointer to it if yes.
    !-------------------------------------------------------------------
    type(ext_subcube2subcube_prog_t), pointer       :: ext_subcube2subcube_prog_ptr
    class(one2one_prog_t),            target        :: prog
    logical,                          intent(inout) :: error
    !
    character(len=*), parameter :: rname='EXT>SUBCUBE2SUBCUBE>PROG>PTR'
    !
    select type(prog)
    type is (ext_subcube2subcube_prog_t)
      ext_subcube2subcube_prog_ptr => prog
    class default
      ext_subcube2subcube_prog_ptr => null()
      call cubetemplate_message(seve%e,rname,  &
        'Internal error: object is not a ext_subcube2subcube_prog_t type')
      error = .true.
      return
    end select
  end function ext_subcube2subcube_prog_ptr
end module cubetemplate_subcube2subcube
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
