!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetemplate_subcube2subcube_template
  use cubetemplate_messaging
  use cubetemplate_one2one_template
  !
  public :: subcube2subcube_comm_t,subcube2subcube_user_t,subcube2subcube_prog_t
  private
  !
  type, extends(one2one_comm_t) :: subcube2subcube_comm_t
  end type subcube2subcube_comm_t
  !
  type, extends(one2one_user_t) :: subcube2subcube_user_t
  end type subcube2subcube_user_t
  !
  type, extends(one2one_prog_t) :: subcube2subcube_prog_t
   contains
     procedure, private :: loop   => cubetemplate_subcube2subcube_prog_loop
     procedure, private :: act    => cubetemplate_subcube2subcube_prog_act
  end type subcube2subcube_prog_t
  !
contains
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetemplate_subcube2subcube_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(subcube2subcube_prog_t), intent(inout) :: prog
    type(cubeadm_iterator_t),      intent(inout) :: iter
    logical,                       intent(inout) :: error
    !
    character(len=*), parameter :: rname='SUBCUBE2SUBCUBE>PROG>LOOP'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter,error)
      if (error) return
    enddo
  end subroutine cubetemplate_subcube2subcube_prog_loop
  !
  subroutine cubetemplate_subcube2subcube_prog_act(prog,iter,error)
    use cubeadm_taskloop
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(subcube2subcube_prog_t), intent(inout) :: prog
    type(cubeadm_iterator_t),      intent(in)    :: iter
    logical,                       intent(inout) :: error
    !
    character(len=*), parameter :: rname='SUBCUBE2SUBCUBE>PROG>ACT'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
  end subroutine cubetemplate_subcube2subcube_prog_act
end module cubetemplate_subcube2subcube_template
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
