!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetemplate_language
  use cubetools_structure
  use cubetemplate_messaging
  !
  use cubetemplate_fullcube2fullcube
  use cubetemplate_image2image
  use cubetemplate_image2visi
  use cubetemplate_list
  use cubetemplate_spectrum2spectrum
  use cubetemplate_spectrum2spectrum_onechan
  use cubetemplate_subcube2subcube
  use cubetemplate_visi2visi
  !
  public :: cubetemplate_register_language
  private
  !
  integer(kind=lang_k) :: langid
  !
contains
  !
  subroutine cubetemplate_register_language(error)
    !----------------------------------------------------------------------
    ! Register the CUBE\ language
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    call cubetools_register_language('TEMPLATE',&
         'J.Pety, S.Bardeau, V.deSouzaMagalhaes',&
         'COMMAND templates',&
         'gag_doc:hlp/cube-help-template.hlp',&
         cubetemplate_execute_command,langid,error)
    if (error) return
    !----------------------------------------------------------------------
    !
    call fullcube2fullcube%register(error)
    if (error) return
    call image2image%register(error)
    if (error) return
    call image2visi%register(error)
    if (error) return
    call list%register(error)
    if (error) return
    call spectrum2spectrum%register(error)
    if (error) return
    call spectrum2spectrum_onechan%register(error)
    if (error) return
    call subcube2subcube%register(error)
    if (error) return
    call visi2visi%register(error)
    if (error) return
    !
    call cubetools_register_dict(error)
    if (error) return
  end subroutine cubetemplate_register_language
  !
  subroutine cubetemplate_execute_command(line,comm,error)
    use cubeadm_opened
    use cubeadm_timing
    !----------------------------------------------------------------------
    ! Execute a command of the CUBE\ language
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    character(len=*), intent(in)    :: comm
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='EXECUTE>COMMAND'
    !
    error = .false.
    if (comm.eq.'TEMPLATE?') then
       call cubetools_list_language_commands(langid,error)
       if (error) return
    else
       call cubeadm_timing_init()
       call cubetools_execute_command(line,langid,comm,error)
       if (error) continue ! To ensure error recovery in the call to cubeadm_finish_all
       call cubeadm_finish_all(comm,line,error)
       if (error) continue ! To ensure error recovery in timing
       call cubeadm_timing_final(comm)
    endif
  end subroutine cubetemplate_execute_command
end module cubetemplate_language
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
