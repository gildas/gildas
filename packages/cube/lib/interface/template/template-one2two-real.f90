!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetemplate_one2two_real_template
  use cube_types
  use cubetools_parameters
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubetopology_cuberegion_types
  use cubetemplate_messaging
  !
  public :: one2two_real_comm_t,one2two_real_user_t,one2two_real_prog_t
  private
  !
  type one2two_real_comm_t
     type(option_t), pointer :: comm
     type(cuberegion_comm_t) :: region
     type(cubeid_arg_t), pointer :: incube
     type(cube_prod_t),  pointer :: oucube1
     type(cube_prod_t),  pointer :: oucube2
     procedure(cubetemplate_one2two_real_prog_act), pointer, nopass, public :: act => null()
   contains
     procedure, public :: register_syntax => cubetemplate_one2two_real_register_syntax
     procedure, public :: register_act    => cubetemplate_one2two_real_register_act
     procedure, public :: parse           => cubetemplate_one2two_real_parse
     procedure, public :: main            => cubetemplate_one2two_real_main
  end type one2two_real_comm_t
  !
  type one2two_real_user_t
     type(cubeid_user_t)     :: cubeids
     type(cuberegion_user_t) :: region
   contains
     procedure, private :: toprog => cubetemplate_one2two_real_user_toprog
  end type one2two_real_user_t
  !
  type one2two_real_prog_t
     type(cuberegion_prog_t) :: region
     type(cube_t), pointer :: incube
     type(cube_t), pointer :: oucube1
     type(cube_t), pointer :: oucube2
     procedure(cubetemplate_one2two_real_prog_act), pointer, public :: act => null()
   contains
     procedure, private :: header => cubetemplate_one2two_real_prog_header
     procedure, private :: data   => cubetemplate_one2two_real_prog_data
     procedure, private :: loop   => cubetemplate_one2two_real_prog_loop
  end type one2two_real_prog_t
  !
contains
  !
  subroutine cubetemplate_one2two_real_register_syntax(comm,&
       opername,inid,inhelp,inflags,opercomm,&
       ou1name,ou1flags,ou2name,ou2flags,error)
    use gkernel_interfaces, only: sic_upper
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(one2two_real_comm_t), intent(inout)    :: comm
    character(len=*),           intent(in)       :: opername
    character(len=*),           intent(in)       :: inid
    character(len=*),           intent(in)       :: inhelp
    type(flag_t),               intent(in)       :: inflags(:)
    external                                     :: opercomm
    character(len=*),           intent(in)       :: ou1name
    type(flag_t),               intent(in)       :: ou1flags(:)
    character(len=*),           intent(in)       :: ou2name
    type(flag_t),               intent(in)       :: ou2flags(:)
    logical,                    intent(inout)    :: error
    !
    type(cubeid_arg_t) :: incube
    type(cube_prod_t) :: oucube
    character(len=argu_l) :: myinid
    character(len=*), parameter :: rname='ONE2TWO>REAL>REGISTER>SYNTAX'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    ! Syntax
    call cubetools_register_command(&
         opername,inid,&
         'Compute the '//trim(opername)//' image or spectrum of a '//trim(inhelp),&
         'Input/output cubes must/will be real',&
         opercomm,&
         comm%comm,error)
    if (error) return
    myinid = inid
    call sic_upper(myinid)
    call incube%register(&
         myinid,&
         inhelp,&
         strg_id,&
         code_arg_optional,&
         inflags,&
         code_read,&
         code_access_speset,&
         comm%incube,&
         error)
    if (error) return
    !
    call comm%region%register(error)
    if (error) return
    !
    ! Products
    call oucube%register(&
         ou1name,&
         trim(ou1name)//' output',&
         strg_id,&
         ou1flags,&
         comm%oucube1,&
         error)
    if (error)  return
    call oucube%register(&
         ou2name,&
         trim(ou2name)//' output',&
         strg_id,&
         ou2flags,&
         comm%oucube2,&
         error)
    if (error)  return
  end subroutine cubetemplate_one2two_real_register_syntax
  !
  subroutine cubetemplate_one2two_real_register_act(comm,act,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(one2two_real_comm_t),     intent(inout) :: comm
    procedure(cubetemplate_one2two_real_prog_act) :: act
    logical,                        intent(inout) :: error
    !
    character(len=*), parameter :: rname='ONE2TWO>REAL>REGISTER>ACT'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    comm%act => act
  end subroutine cubetemplate_one2two_real_register_act
  !
  subroutine cubetemplate_one2two_real_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! ONE2TWO cubeid
    ! /SIZE sx [sy]
    ! /CENTER xcen ycen
    ! /RANGE zfirst zlast
    ! /SPECTRUM
    ! /IMAGE
    !----------------------------------------------------------------------
    class(one2two_real_comm_t), intent(in)    :: comm
    character(len=*),           intent(in)    :: line
    type(one2two_real_user_t),  intent(out)   :: user
    logical,                    intent(inout) :: error
    !
    character(len=*), parameter :: rname='ONE2TWO>REAL>PARSE'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%region%parse(line,user%region,error)
    if (error) return
  end subroutine cubetemplate_one2two_real_parse
  !
  subroutine cubetemplate_one2two_real_main(comm,user,error)
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(one2two_real_comm_t), intent(in)    :: comm
    type(one2two_real_user_t),  intent(inout) :: user
    logical,                    intent(inout) :: error
    !
    type(one2two_real_prog_t) :: prog
    character(len=*), parameter :: rname='ONE2TWO>REAL>MAIN'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubetemplate_one2two_real_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetemplate_one2two_real_user_toprog(user,comm,prog,error)
    use cubeadm_get
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(one2two_real_user_t), intent(in)    :: user
    type(one2two_real_comm_t),  intent(in)    :: comm
    type(one2two_real_prog_t),  intent(out)   :: prog
    logical,                    intent(inout) :: error
    !
    character(len=*), parameter :: rname='ONE2TWO>REAL>USER>TOPROG'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%incube,user%cubeids,prog%incube,error)
    if (error) return
    call user%region%toprog(prog%incube,prog%region,error)
    if (error) return
    call prog%region%list(error)
    if (error) return
    !
    prog%act => comm%act
  end subroutine cubetemplate_one2two_real_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetemplate_one2two_real_prog_header(prog,comm,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(one2two_real_prog_t), intent(inout) :: prog
    type(one2two_real_comm_t),  intent(in)    :: comm
    logical,                    intent(inout) :: error
    !
    character(len=*), parameter :: rname='ONE2TWO>REAL>PROG>HEADER'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call one_header(comm%oucube1,prog%incube,prog%region,prog%oucube1,error)
    if (error) return
    call one_header(comm%oucube2,prog%incube,prog%region,prog%oucube2,error)
    if (error) return
    !
  contains
    !
    subroutine one_header(oucomm,incube,region,oucube,error)
      use cubeadm_clone
      use cubetools_header_methods
      !----------------------------------------------------------------------
      !
      !----------------------------------------------------------------------
      type(cube_prod_t),       intent(in)    :: oucomm
      type(cube_t), pointer,   intent(in)    :: incube
      type(cuberegion_prog_t), intent(inout) :: region
      type(cube_t), pointer,   intent(inout) :: oucube
      logical,                 intent(inout) :: error
      !
      character(len=unit_l) :: unit
      integer(kind=chan_k), parameter :: one=1
      !
      call cubeadm_clone_header(oucomm,incube,oucube,error)
      if (error) return
      call region%header(oucube,error)
      if (error) return
      call cubetools_header_put_nchan(one,oucube%head,error)
      if (error) return
      !
      call cubetools_header_get_array_unit(incube%head,unit,error)
      if (error) return
      call cubetools_header_put_array_unit(trim(unit),oucube%head,error)
      if (error) return
    end subroutine one_header
  end subroutine cubetemplate_one2two_real_prog_header
  !
  subroutine cubetemplate_one2two_real_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(one2two_real_prog_t), intent(inout) :: prog
    logical,                    intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='ONE2TWO>REAL>PROG>DATA'
    !
    call cubetemplate_message(templateseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo ! iter
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubetemplate_one2two_real_prog_data
  !   
  subroutine cubetemplate_one2two_real_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(one2two_real_prog_t), intent(inout) :: prog
    type(cubeadm_iterator_t),   intent(inout) :: iter
    logical,                    intent(inout) :: error
    !
    type(spectrum_t) :: spec,good,out1,out2
    character(len=*), parameter :: rname='ONE2TWO>REAL>PROG>LOOP'
    !
    call spec%associate('input',prog%incube,iter,error)
    if (error) return
    !***JP: Associating x for the input and not for the other ones may
    !***JP: lead to segfaults!
    call spec%associate_x(error)
    if (error) return
    call good%allocate('good',prog%incube,error)
    if (error) return
    call out1%allocate('output #1',prog%oucube1,iter,error)
    if (error) return
    call out2%allocate('output #2',prog%oucube2,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
       call prog%act(iter%ie,spec,good,out1,out2,error)
       if (error) return
    enddo ! ie
  end subroutine cubetemplate_one2two_real_prog_loop
  !
  subroutine cubetemplate_one2two_real_prog_act(prog,ie,spec,good,out1,out2,error)
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(one2two_real_prog_t), intent(inout) :: prog
    integer(kind=entr_k),       intent(in)    :: ie
    type(spectrum_t),           intent(inout) :: spec
    type(spectrum_t),           intent(inout) :: good
    type(spectrum_t),           intent(inout) :: out1
    type(spectrum_t),           intent(inout) :: out2
    logical,                    intent(inout) :: error
  end subroutine cubetemplate_one2two_real_prog_act
end module cubetemplate_one2two_real_template
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
