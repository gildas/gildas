!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage CUBE messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetemplate_messaging
  use gpack_def
  use gbl_message
  use cubetools_parameters
  !
  public :: templateseve,seve,mess_l
  public :: cubetemplate_message_set_id,cubetemplate_message
  public :: cubetemplate_message_set_alloc,cubetemplate_message_get_alloc
  public :: cubetemplate_message_set_trace,cubetemplate_message_get_trace
  public :: cubetemplate_message_set_others,cubetemplate_message_get_others
  private
  !
  ! Identifier used for message identification
  integer(kind=4) :: cubetemplate_message_id = gpack_global_id  ! Default value for startup message
  !
  type :: cubetemplate_messaging_debug_t
     integer(kind=code_k) :: alloc = seve%d
     integer(kind=code_k) :: trace = seve%t
     integer(kind=code_k) :: others = seve%d
  end type cubetemplate_messaging_debug_t
  !
  type(cubetemplate_messaging_debug_t) :: templateseve
  !
contains
  !
  subroutine cubetemplate_message_set_id(id)
    !---------------------------------------------------------------------
    ! Alter library id into input id. Should be called by the library
    ! which wants to share its id with the current one.
    !---------------------------------------------------------------------
    integer(kind=4), intent(in) :: id
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='MESSAGE>SET>ID'
    !
    cubetemplate_message_id = id
    write (mess,'(A,I0)') 'Now use id #',cubetemplate_message_id
    call cubetemplate_message(seve%d,rname,mess)
  end subroutine cubetemplate_message_set_id
  !
  subroutine cubetemplate_message(mkind,procname,message)
    use cubetools_cmessaging
    !---------------------------------------------------------------------
    ! Messaging facility for the current library. Calls the low-level
    ! (internal) messaging routine with its own identifier.
    !---------------------------------------------------------------------
    integer(kind=4),  intent(in) :: mkind     ! Message kind
    character(len=*), intent(in) :: procname  ! Name of calling procedure
    character(len=*), intent(in) :: message   ! Message string
    !
    call cubetools_cmessage(cubetemplate_message_id,mkind,'TEMPLATE>'//procname,message)
  end subroutine cubetemplate_message
  !
  subroutine cubetemplate_message_set_alloc(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       templateseve%alloc = seve%i
    else
       templateseve%alloc = seve%d
    endif
  end subroutine cubetemplate_message_set_alloc
  !
  subroutine cubetemplate_message_set_trace(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       templateseve%trace = seve%i
    else
       templateseve%trace = seve%t
    endif
  end subroutine cubetemplate_message_set_trace
  !
  subroutine cubetemplate_message_set_others(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       templateseve%others = seve%i
    else
       templateseve%others = seve%d
    endif
  end subroutine cubetemplate_message_set_others
  !
  function cubetemplate_message_get_alloc()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubetemplate_message_get_alloc
    !
    cubetemplate_message_get_alloc = templateseve%alloc.eq.seve%i
    !
  end function cubetemplate_message_get_alloc
  !
  function cubetemplate_message_get_trace()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubetemplate_message_get_trace
    !
    cubetemplate_message_get_trace = templateseve%trace.eq.seve%i
    !
  end function cubetemplate_message_get_trace
  !
  function cubetemplate_message_get_others()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubetemplate_message_get_others
    !
    cubetemplate_message_get_others = templateseve%others.eq.seve%i
    !
  end function cubetemplate_message_get_others
end module cubetemplate_messaging
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
