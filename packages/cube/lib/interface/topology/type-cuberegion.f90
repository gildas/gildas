!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetopology_cuberegion_types
  use cubetools_parameters
  use cubetools_structure
  use cubetopology_firstlaststride_types
  use cubetopology_sperange_types
  use cubetopology_sparange_types
  use cubetopology_spapos_types
  use cubetopology_spasize_types
  use cubetopology_messaging
  !
  public :: cuberegion_comm_t,cuberegion_user_t,cuberegion_prog_t
  private
  !
  type cuberegion_comm_t
     type(sperange_opt_t) :: range
     type(spapos_comm_t)  :: center
     type(spasize_opt_t)  :: size
   contains
     procedure, public :: register => cubetopology_cuberegion_register
     procedure, public :: parse    => cubetopology_cuberegion_parse
  end type cuberegion_comm_t
  !
  type cuberegion_user_t
     type(sperange_user_t) :: range
     type(spapos_user_t)   :: center
     type(spasize_user_t)  :: size
   contains
     procedure, public :: toprog => cubetopology_cuberegion_user_toprog
!     procedure, public :: list   => cubetopology_cuberegion_user_list ! *** JP could be implemented
  end type cuberegion_user_t
  !
  type cuberegion_prog_t
     type(sparange_prog_t) :: xrange
     type(sparange_prog_t) :: yrange
     type(sperange_prog_t) :: zrange
     type(firstlaststride_t) :: ix,iy,iz
   contains
     procedure, public :: list             => cubetopology_cuberegion_prog_list
     procedure, public :: list_size_center => cubetopology_cuberegion_prog_list_size_center
     procedure, public :: header           => cubetopology_cuberegion_prog_header
     procedure, public :: ndata            => cubetopology_cuberegion_prog_ndata
     procedure, public :: apply_to_cube    => cubetopology_cuberegion_prog_apply_to_cube
  end type cuberegion_prog_t
  !
contains
  !
  subroutine cubetopology_cuberegion_register(comm,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(cuberegion_comm_t), intent(inout) :: comm
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='CUBEREGION>REGISTER'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    call comm%size%register('Define the spatial size of the cube region of interest',error)
    if (error) return
    call comm%center%register('CENTER','Define the spatial center of the cube region of interest',error)
    if (error) return
    call comm%range%register('RANGE','Define the spectral range of the cube region of interest',error)
    if (error) return
  end subroutine cubetopology_cuberegion_register
  !
  subroutine cubetopology_cuberegion_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! /SIZE sx [sy]
    ! /CENTER xcen ycen
    ! /RANGE zfirst zlast
    !----------------------------------------------------------------------
    class(cuberegion_comm_t), intent(in)    :: comm
    character(len=*),         intent(in)    :: line
    type(cuberegion_user_t),  intent(out)   :: user
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='CUBEREGION>PARSE'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    call comm%size%parse(line,user%size,error)
    if (error) return
    call comm%center%parse(line,user%center,error)
    if (error) return
    call comm%range%parse(line,user%range,error)
    if (error) return
    ! The following test is a problem for the APERTURE command that needs to define
    ! an aperture center, the rectangle size being optional.
    !***JP: I thus unfactorize the following test. However this could lead
    !***JP: to confusion.  If this happens in the future, we should have
    !***JP: too flavor of cuberegion_user_type. A loose one and a strict
    !***JP: one.
!!$    if (user%center%present.and..not.user%size%do) then
!!$       call cubetopology_message(seve%e,rname,'A size must be specified when giving a new center')
!!$       error = .true.
!!$       return
!!$    endif
  end subroutine cubetopology_cuberegion_parse
  !
  subroutine cubetopology_cuberegion_user_toprog(user,cube,prog,error)
    use cube_types
    !----------------------------------------------------------------------
    ! This code
    ! 1. Converts users inputs into spatial and spectral ranges.
    ! 2. Ensure that the spatial and spectral ranges are within the limits
    !    of the cube.
    ! 3. Provide the associated firstlaststride objects.
    !----------------------------------------------------------------------
    class(cuberegion_user_t), intent(in)    :: user
    type(cube_t),             intent(in)    :: cube
    type(cuberegion_prog_t),  intent(inout) :: prog
    logical,                  intent(inout) :: error
    !
    type(spapos_prog_t) :: center
    integer(kind=ndim_k), parameter :: ix=1,iy=2
    character(len=*), parameter :: rname='CUBEREGION>USER>TOPROG'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    call user%center%toprog(cube,center,error)
    if (error) return
    call prog%xrange%fromuser(code_sparange_truncated,&
         cube%head%set%il,cube,center%rela(ix),&
         user%size%x,user%size%unit,cube%head%spa%l%inc,&
         cube%head%spa%l%kind,error)
    if (error) return
    call prog%yrange%fromuser(code_sparange_truncated,&
         cube%head%set%im,cube,center%rela(iy),&
         user%size%y,user%size%unit,cube%head%spa%m%inc,&
         cube%head%spa%m%kind,error)
    if (error) return
    call user%range%toprog(cube,code_sperange_truncated,prog%zrange,error)
    if (error) return
    !
    ! Ensure that the range will be inside the axis range
    call prog%xrange%intersect_axis(error)
    if (error) return
    call prog%yrange%intersect_axis(error)
    if (error) return
    call prog%zrange%intersect_axis(error)
    if (error) return
    !
    ! Convert to firstlaststride after ranges were intersected because it's not
    ! the same to compute the stride before or after intersection.
    call prog%xrange%to_pixe_k(prog%ix%first,prog%ix%last,prog%ix%stride,error)
    if (error) return
    call prog%yrange%to_pixe_k(prog%iy%first,prog%iy%last,prog%iy%stride,error)
    if (error) return
    call prog%zrange%to_chan_k(prog%iz%first,prog%iz%last,prog%iz%stride,error)
    if (error) return
  end subroutine cubetopology_cuberegion_user_toprog
  !
  subroutine cubetopology_cuberegion_prog_header(prog,cube,error)
    use cube_types
    use cubetools_axis_types
    use cubetools_header_methods
    !-------------------------------------------------------------------
    ! Update cube header according to the defined region
    !-------------------------------------------------------------------
    class(cuberegion_prog_t), intent(inout) :: prog
    class(cube_t),            intent(inout) :: cube
    logical,                  intent(inout) :: error
    !
    type(axis_t) :: axis
    character(len=*), parameter :: rname='CUBEREGION>PROG>HEADER'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    call cubetools_header_get_axis_head_l(cube%head,axis,error)
    if (error) return
    call prog%ix%update_axis_header(axis,error)
    if (error) return
    call cubetools_header_update_axset_l(axis,cube%head,error)
    if (error) return
    !
    call cubetools_header_get_axis_head_m(cube%head,axis,error)
    if (error) return
    call prog%iy%update_axis_header(axis,error)
    if (error) return
    call cubetools_header_update_axset_m(axis,cube%head,error)
    if (error) return
    !
    call cubetools_header_get_axis_head_c(cube%head,axis,error)
    if (error) return
    call prog%iz%update_axis_header(axis,error)
    if (error) return
    call cubetools_header_update_axset_c(axis,cube%head,error)
    if (error) return
  end subroutine cubetopology_cuberegion_prog_header
  !
  subroutine cubetopology_cuberegion_prog_list(prog,error)
    !-------------------------------------------------------------------
    ! List cube region in a user friendly way
    !-------------------------------------------------------------------
    class(cuberegion_prog_t), intent(in)    :: prog
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='CUBEREGION>PROG>LIST'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    call cubetopology_message(seve%r,rname,blankstr)
    call prog%xrange%list(error)
    if (error) return
    call prog%yrange%list(error)
    if (error) return
    call prog%zrange%list(error)
    if (error) return
  end subroutine cubetopology_cuberegion_prog_list
  !
  subroutine cubetopology_cuberegion_prog_list_size_center(prog,error)
    use cubetools_unit
    use cubetools_format
    !-------------------------------------------------------------------
    ! List cube spatial size and center in a user friendly way
    !-------------------------------------------------------------------
    class(cuberegion_prog_t), intent(in)    :: prog
    logical,                  intent(inout) :: error
    !
    real(kind=coor_k) :: xsize,xcenter
    real(kind=coor_k) :: ysize,ycenter
    character(len=*), parameter :: rname='CUBEREGION>PROG>LIST>SIZE>CENTER'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    call get_size_center(prog%xrange,xsize,xcenter,error)
    if (error) return
    call get_size_center(prog%yrange,ysize,ycenter,error)
    if (error) return
    !
    call cubetopology_message(seve%r,rname,blankstr)
    call cubetopology_message(seve%r,rname,&
         cubetools_format_size_center(xsize,ysize,xcenter,ycenter))
    !
  contains
    !
    subroutine get_size_center(range,size,center,error)
      !-----------------------------------------------------------------
      ! Converion unit is done here because it can be different for the
      ! x and y axes.
      !-----------------------------------------------------------------
      type(sparange_prog_t), intent(in)    :: range
      real(kind=coor_k),     intent(out)   :: size
      real(kind=coor_k),     intent(out)   :: center
      logical,               intent(inout) :: error
      !
      type(unit_user_t) :: unit
      !
      call range%get_size_center(size,center,error)
      if (error) return
      call range%get_unit(unit,error)
      if (error) return
      size = size*unit%user_per_prog
      center = center*unit%user_per_prog
    end subroutine get_size_center
  end subroutine cubetopology_cuberegion_prog_list_size_center
  !
  function cubetopology_cuberegion_prog_ndata(prog) result(ndata)
    !-------------------------------------------------------------------
    ! Return the region size (number of data values)
    !-------------------------------------------------------------------
    class(cuberegion_prog_t), intent(in) :: prog
    integer(kind=data_k) :: ndata
    !
    ndata =          prog%ix%last-prog%ix%first+1
    ndata = ndata * (prog%iy%last-prog%iy%first+1)
    ndata = ndata * (prog%iz%last-prog%iz%first+1)
  end function cubetopology_cuberegion_prog_ndata
  !
  subroutine cubetopology_cuberegion_prog_apply_to_cube(region,format,error,align)
    use cubetuple_format
    use cubetuple_iterator
    !-------------------------------------------------------------------
    ! 1) Declare the original axes of this cube to its iterator, i.e.
    !    the leading ones and the iterated one.
    ! 2) Declare that the region to be applied on the cube, i.e. the
    !    subset of the leading and iterated axes.
    ! See also format_t%default_region() if no region is to be applied.
    !-------------------------------------------------------------------
    class(cuberegion_prog_t), intent(in)           :: region
    class(format_t),          intent(inout)        :: format
    logical,                  intent(inout)        :: error
    integer(kind=code_k),     intent(in), optional :: align
    !
    integer(kind=code_k) :: lalign,order
    character(len=*), parameter :: rname='CUBEREGION>PROG>APPLY>TO>CUBE'
    !
    if (present(align)) then
      lalign = align
    else
      lalign = code_align_auto
    endif
    !
    ! Rely on internal data order (cub is already the cube with desired
    ! order), not the way we access it (e.g. by subcube):
    order = format%order()
    !
    ! Fetch this cube axes
    call format%iter%get_axes(format%head,order,align,error)
    if (error)  return
    !
    ! Ugly API which should understand directly the firstlaststride_t type
    call format%iter%set_region_l(region%ix%first,region%ix%last,region%ix%stride,error)
    if (error)  return
    call format%iter%set_region_m(region%iy%first,region%iy%last,region%iy%stride,error)
    if (error)  return
    call format%iter%set_region_c(region%iz%first,region%iz%last,region%iz%stride,error)
    if (error)  return
  end subroutine cubetopology_cuberegion_prog_apply_to_cube
end module cubetopology_cuberegion_types
! 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
