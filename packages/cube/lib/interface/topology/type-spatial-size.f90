!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetopology_spasize_types
  use cubetools_parameters
  use cubetopology_messaging
  use cubetools_structure
  use cubetools_unit_arg
  !
  public :: spasize_opt_t,spasize_user_t,spasize_prog_t
  private
  !
  type spasize_opt_t
     type(option_t),   pointer :: opt
     type(unit_arg_t), pointer :: unit_arg
   contains
     procedure :: register => cubetopology_spasize_register
     procedure :: parse    => cubetopology_spasize_parse
  end type spasize_opt_t
  !
  type spasize_user_t
     logical               :: do = .false.
     character(len=argu_l) :: x = strg_star
     character(len=argu_l) :: y = strg_star
     character(len=argu_l) :: unit  = strg_star
   contains
     procedure :: init   => cubetopology_spasize_user_init
     procedure :: toprog => cubetopology_spasize_user_toprog
     procedure :: list   => cubetopology_spasize_user_list
  end type spasize_user_t
  !
  type spasize_prog_t
     real(kind=coor_k) :: x ! [rad]
     real(kind=coor_k) :: y ! [rad]
  end type spasize_prog_t
  !
contains
  !
  subroutine cubetopology_spasize_register(size,abstract,error)
    use cubetools_unit
    !----------------------------------------------------------------------
    ! Register a /SIZE option according to a name and abstract
    ! provided by the command
    !----------------------------------------------------------------------
    class(spasize_opt_t), intent(out)   :: size      
    character(len=*),     intent(in)    :: abstract
    logical,              intent(inout) :: error
    !
    type(standard_arg_t) :: stdarg
    type(unit_arg_t) :: unitarg
    !
    character(len=*), parameter :: rname='SPASIZE>REGISTER'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         'SIZE','sl [sm [unit]]',&
         abstract,&
         strg_id,&
         size%opt,error)
    if (error) return
    call stdarg%register( &
         'sl','Size in L coordinate', &
         '"*" or "=" mean previous value is kept',&
         code_arg_mandatory, &
         error)
    if (error) return
    call stdarg%register( &
         'sm','Size in M coordinate', &
         '= means unchanged, * means equal to sl',&
         code_arg_optional, &
         error)
    if (error) return
    call unitarg%register( &
         'unit',  &
         'Size unit', &
         '"*" or "=" means current unit is used',&
         code_arg_optional, &
         code_unit_fov, &
         size%unit_arg,&
         error)
    if (error) return
  end subroutine cubetopology_spasize_register
  !
  subroutine cubetopology_spasize_parse(size,line,user,error)
    !----------------------------------------------------------------------
    ! /SIZE x y [unit]
    !
    ! x and y size can be either an absolute sexagesimal
    ! string or a relative offset in arcseconds.
    ! ----------------------------------------------------------------------
    class(spasize_opt_t), intent(in)    :: size  
    character(len=*),     intent(in)    :: line
    type(spasize_user_t), intent(out)   :: user
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPASIZE>PARSE'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    call size%opt%present(line,user%do,error)
    if (error) return
    if (user%do) then
       call cubetools_getarg(line,size%opt,1,user%x,mandatory,error)
       if (error) return
       user%y = user%x
       call cubetools_getarg(line,size%opt,2,user%y,.not.mandatory,error)
       if (error) return
       call cubetools_getarg(line,size%opt,3,user%unit,.not.mandatory,error)
       if (error) return
    endif
  end subroutine cubetopology_spasize_parse
  !
  !------------------------------------------------------------------------
  !
  subroutine cubetopology_spasize_user_init(size,error)
    !----------------------------------------------------------------------
    ! Initialize by setting the intent of size to out
    !----------------------------------------------------------------------
    class(spasize_user_t), intent(out)   :: size 
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPASIZE>USER>INIT'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
  end subroutine cubetopology_spasize_user_init
  !
  subroutine cubetopology_spasize_user_toprog(user,cube,prog,error)
    use cubetools_unit
    use cubetools_user2prog
    use cube_types
    use cubetopology_spatial_coordinates
    !----------------------------------------------------------------------
    ! Warning: The sign encodes the direction of plotting. Please do not
    ! use absolute values here.
    !----------------------------------------------------------------------
    class(spasize_user_t), intent(in)    :: user
    type(cube_t),          intent(in)    :: cube
    type(spasize_prog_t),  intent(out)   :: prog
    logical,               intent(inout) :: error
    !
    type(unit_user_t) :: unit
    real(kind=coor_k) :: rmin(2), rmax(2)
    character(len=*), parameter :: rname='SPASIZE>USER>TOPROG'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    call unit%get_from_name_for_code(user%unit,code_unit_fov,error)
    if (error) return
    !
    call cubetopology_spatial_relminrelmax(cube,rmin,rmax,error)
    if (error) return
    prog%x = rmax(1)-rmin(1)
    prog%y = rmax(2)-rmin(2)
    !
    call cubetools_user2prog_resolve_star(user%x,unit,prog%x,prog%x,error)
    if (error) return
    call cubetools_user2prog_resolve_star(user%y,unit,prog%x,prog%y,error)
    if (error) return
  end subroutine cubetopology_spasize_user_toprog
  !
  subroutine cubetopology_spasize_user_list(size,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spasize_user_t), intent(in)    :: size 
    logical,               intent(inout) :: error
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='SPASIZE>USER>LIST'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    write(mess,'(6a)') &
         'Size  : (',trim(size%x),',',trim(size%y),') ',size%unit
    call cubetopology_message(seve%r,rname,mess)
  end subroutine cubetopology_spasize_user_list
end module cubetopology_spasize_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
