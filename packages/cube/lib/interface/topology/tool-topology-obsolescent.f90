!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! *** JP Too much access to header. This probably means that these
! *** JP subroutine are at best ill-placed and more probably obsolescent.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetopology_tool
  use cubetools_parameters
  use cubetopology_messaging
  use cube_types
  !
  public :: cubetopology_tool_vrange2crange
  public :: cubetopology_tool_velocity2channel
  public :: cubetopology_tool_velocity2rchannel
  public :: cubetopology_tool_vminvmax
  public :: cubetopology_tool_fminfmax
  private
  !
contains
  !
  subroutine cubetopology_tool_vrange2crange(cube,vrange,crange,error)
    use cubetools_shape_types
    use cubetools_header_methods
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    type(cube_t),         intent(in)    :: cube
    real(kind=coor_k),    intent(in)    :: vrange(2)
    integer(kind=chan_k), intent(out)   :: crange(2)
    logical,              intent(inout) :: error
    !
    type(shape_t) :: n
    integer(kind=chan_k) :: irange(2)
    character(len=*), parameter :: rname='TOOL>VRANGE2CRANGE'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    if (cube%head%spe%inc%v.ne.0) then
       call cubetools_header_get_array_shape(cube%head,n,error)
       if (error) return
       irange = nint(cube%head%spe%ref%c+(vrange-cube%head%spe%ref%v)/cube%head%spe%inc%v)
       crange(1) = max(min(irange(1),irange(2)),1)
       crange(2) = min(max(irange(1),irange(2)),n%c)
    else
       error = .true.
    endif
  end subroutine cubetopology_tool_vrange2crange
  !
  subroutine cubetopology_tool_channel2velocity(cube,rchannel,velocity,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    type(cube_t),      intent(in)    :: cube
    real(kind=coor_k), intent(in)    :: rchannel
    real(kind=coor_k), intent(out)   :: velocity
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='TOOL>CHANNEL2VELOCITY'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    velocity = cube%head%spe%ref%v + cube%head%spe%inc%v * (rchannel-cube%head%spe%ref%c)
  end subroutine cubetopology_tool_channel2velocity
  !
  subroutine cubetopology_tool_velocity2channel(cube,velocity,ichannel,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    type(cube_t),         intent(in)    :: cube
    real(kind=coor_k),    intent(in)    :: velocity
    integer(kind=chan_k), intent(out)   :: ichannel
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='TOOL>VELOCITY2CHANNEL'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    if (cube%head%spe%inc%v.ne.0) then
       ichannel = nint(cube%head%spe%ref%c+(velocity-cube%head%spe%ref%v)/cube%head%spe%inc%v)
    else
       error = .true.
    endif
  end subroutine cubetopology_tool_velocity2channel
  !
  subroutine cubetopology_tool_velocity2rchannel(cube,velocity,rchannel,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    type(cube_t),      intent(in)    :: cube
    real(kind=coor_k), intent(in)    :: velocity
    real(kind=coor_k), intent(out)   :: rchannel
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='TOOL>VELOCITY2CHANNEL'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    if (cube%head%spe%inc%v.ne.0) then
       rchannel = cube%head%spe%ref%c+(velocity-cube%head%spe%ref%v)/cube%head%spe%inc%v
    else
       error = .true.
    endif
  end subroutine cubetopology_tool_velocity2rchannel
  !
  subroutine cubetopology_tool_vminvmax(cube,vmin,vmax,error)
    use cubetools_shape_types
    use cubetools_header_methods
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    type(cube_t),      intent(in)    :: cube
    real(kind=coor_k), intent(out)   :: vmin
    real(kind=coor_k), intent(out)   :: vmax
    logical,           intent(inout) :: error
    !
    type(shape_t) :: n
    character(len=*), parameter :: rname='TOOL>VMINVMAX'
    !
    call cubetools_header_get_array_shape(cube%head,n,error)
    if (error) return
    if (cube%head%spe%inc%v.gt.0) then
       call cubetopology_tool_channel2velocity(cube,0.5d0,vmin,error)
       if (error) return
       call cubetopology_tool_channel2velocity(cube,n%c+0.5d0,vmax,error)
       if (error) return
    else
       call cubetopology_tool_channel2velocity(cube,0.5d0,vmax,error)
       if (error) return
       call cubetopology_tool_channel2velocity(cube,n%c+0.5d0,vmin,error)
       if (error) return
    endif
  end subroutine cubetopology_tool_vminvmax
  !
  subroutine cubetopology_tool_channel2frequency(cube,rchannel,frequence,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    type(cube_t),      intent(in)    :: cube
    real(kind=coor_k), intent(in)    :: rchannel
    real(kind=coor_k), intent(out)   :: frequence
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='TOOL>CHANNEL2FREQUENCY'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    frequence = cube%head%spe%ref%f + cube%head%spe%inc%f * (rchannel-cube%head%spe%ref%c)
  end subroutine cubetopology_tool_channel2frequency
!!$  !
!!$  subroutine cubetopology_tool_frequency2channel(cube,frequency,ichannel,error)
!!$    !----------------------------------------------------------------------
!!$    ! 
!!$    !----------------------------------------------------------------------
!!$    type(cube_t),         intent(in)    :: cube
!!$    real(kind=coor_k),    intent(in)    :: frequency
!!$    integer(kind=chan_k), intent(out)   :: ichannel
!!$    logical,              intent(inout) :: error
!!$    !
!!$    character(len=*), parameter :: rname='TOOL>FREQUENCY2CHANNEL'
!!$    !
!!$    call cubetopology_message(toposeve%trace,rname,'Welcome')
!!$    !
!!$    if (cube%head%spe%inc%v.ne.0) then
!!$       ichannel = nint(cube%head%spe%ref%c+(frequency-cube%head%spe%ref%f)/cube%head%spe%inc%f)
!!$    else
!!$       error = .true.
!!$    endif
!!$  end subroutine cubetopology_tool_frequency2channel
  !
  subroutine cubetopology_tool_fminfmax(cube,fmin,fmax,error)
    use cubetools_shape_types
    use cubetools_header_methods
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    type(cube_t),      intent(in)    :: cube
    real(kind=coor_k), intent(out)   :: fmin
    real(kind=coor_k), intent(out)   :: fmax
    logical,           intent(inout) :: error
    !
    type(shape_t) :: n
    character(len=*), parameter :: rname='TOOL>FMINFMAX'
    !
    call cubetools_header_get_array_shape(cube%head,n,error)
    if (error) return
    if (cube%head%spe%inc%f.gt.0) then
       call cubetopology_tool_channel2frequency(cube,0.5d0,fmin,error)
       if (error) return
       call cubetopology_tool_channel2frequency(cube,n%c+0.5d0,fmax,error)
       if (error) return
    else
       call cubetopology_tool_channel2frequency(cube,0.5d0,fmax,error)
       if (error) return
       call cubetopology_tool_channel2frequency(cube,n%c+0.5d0,fmin,error)
       if (error) return
    endif
  end subroutine cubetopology_tool_fminfmax
end module cubetopology_tool
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
