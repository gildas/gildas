!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetopology_firstlaststride_types
  use cubetools_parameters
  use cubetopology_messaging
  !
  public :: firstlaststride_t
  private
  !
  type firstlaststride_t
     integer(kind=indx_k) :: first  = 0
     integer(kind=indx_k) :: last   = 0
     integer(kind=indx_k) :: stride = 0
   contains
     ! No intersection method because it's not the same to compute a stride
     ! before or after intersection.
     procedure, public :: list               => cubetopology_firstlaststride_list
     procedure, public :: update_axis_header => cubetopology_firstlaststride_update_axis_header
  end type firstlaststride_t
  !
contains
  !
  subroutine cubetopology_firstlaststride_list(iter,error)
    !-------------------------------------------------------------------
    ! List 
    !-------------------------------------------------------------------
    class(firstlaststride_t), intent(in)    :: iter
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='FIRSTLASTSTRIDE>LIST'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    print *,'First:  ',iter%first
    print *,'Last:   ',iter%last
    print *,'Stride: ',iter%stride
  end subroutine cubetopology_firstlaststride_list
  !
  subroutine cubetopology_firstlaststride_update_axis_header(iter,axis,error)
    use cubetools_axis_types
    !-------------------------------------------------------------------
    ! Update the header of axis according to the iterator 
    !-------------------------------------------------------------------
    class(firstlaststride_t), intent(in)    :: iter
    type(axis_t),             intent(inout) :: axis
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='FIRSTLASTSTRIDE>UPDATE>AXIS>HEADER'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    axis%n = iter%last-iter%first+1
    axis%ref = axis%ref-real(iter%stride*iter%first,kind=coor_k)+1d0
  end subroutine cubetopology_firstlaststride_update_axis_header
end module cubetopology_firstlaststride_types
! 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
