!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage CUBE messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetopology_messaging
  use gpack_def
  use gbl_message
  use cubetools_parameters
  !
  public :: toposeve,seve,mess_l
  public :: cubetopology_message_set_id,cubetopology_message
  public :: cubetopology_message_set_alloc,cubetopology_message_get_alloc
  public :: cubetopology_message_set_trace,cubetopology_message_get_trace
  public :: cubetopology_message_set_others,cubetopology_message_get_others
  private
  !
  ! Identifier used for message identification
  integer(kind=4) :: cubetopology_message_id = gpack_global_id  ! Default value for startup message
  !
  type :: cubetopology_messaging_debug_t
     integer(kind=code_k) :: alloc = seve%d
     integer(kind=code_k) :: trace = seve%t
     integer(kind=code_k) :: others = seve%d
  end type cubetopology_messaging_debug_t
  !
  type(cubetopology_messaging_debug_t) :: toposeve
  !
contains
  !
  subroutine cubetopology_message_set_id(id)
    !---------------------------------------------------------------------
    ! Alter library id into input id. Should be called by the library
    ! which wants to share its id with the current one.
    !---------------------------------------------------------------------
    integer(kind=4), intent(in) :: id
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='MESSAGE>SET>ID'
    !
    cubetopology_message_id = id
    write (mess,'(A,I0)') 'Now use id #',cubetopology_message_id
    call cubetopology_message(seve%d,rname,mess)
  end subroutine cubetopology_message_set_id
  !
  subroutine cubetopology_message(mkind,procname,message)
    use cubetools_cmessaging
    !---------------------------------------------------------------------
    ! @ private
    ! Messaging facility for the current library. Calls the low-level
    ! (internal) messaging routine with its own identifier.
    !---------------------------------------------------------------------
    integer(kind=4),  intent(in) :: mkind     ! Message kind
    character(len=*), intent(in) :: procname  ! Name of calling procedure
    character(len=*), intent(in) :: message   ! Message string
    !
    call cubetools_cmessage(cubetopology_message_id,mkind,'TOPOLOGY>'//procname,message)
  end subroutine cubetopology_message
  !
  subroutine cubetopology_message_set_alloc(on)
    !---------------------------------------------------------------------
    ! @ public
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       toposeve%alloc = seve%i
    else
       toposeve%alloc = seve%d
    endif
  end subroutine cubetopology_message_set_alloc
  !
  subroutine cubetopology_message_set_trace(on)
    !---------------------------------------------------------------------
    ! @ public
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       toposeve%trace = seve%i
    else
       toposeve%trace = seve%t
    endif
  end subroutine cubetopology_message_set_trace
  !
  subroutine cubetopology_message_set_others(on)
    !---------------------------------------------------------------------
    ! @ public
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       toposeve%others = seve%i
    else
       toposeve%others = seve%d
    endif
  end subroutine cubetopology_message_set_others
  !
  function cubetopology_message_get_alloc()
    !---------------------------------------------------------------------
    ! @ public
    !---------------------------------------------------------------------
    logical :: cubetopology_message_get_alloc
    !
    cubetopology_message_get_alloc = toposeve%alloc.eq.seve%i
    !
  end function cubetopology_message_get_alloc
  !
  function cubetopology_message_get_trace()
    !---------------------------------------------------------------------
    ! @ public
    !---------------------------------------------------------------------
    logical :: cubetopology_message_get_trace
    !
    cubetopology_message_get_trace = toposeve%trace.eq.seve%i
    !
  end function cubetopology_message_get_trace
  !
  function cubetopology_message_get_others()
    !---------------------------------------------------------------------
    ! @ public
    !---------------------------------------------------------------------
    logical :: cubetopology_message_get_others
    !
    cubetopology_message_get_others = toposeve%others.eq.seve%i
    !
  end function cubetopology_message_get_others
end module cubetopology_messaging
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
