!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetopology_spevsys_types
  use cubetools_parameters
  use cubetools_structure
  use cubetools_unit_arg
  use cubetopology_messaging
  !
  public :: spevsys_opt_t,spevsys_user_t,spevsys_prog_t
  private
  !
  type spevsys_opt_t
     type(option_t),   pointer :: opt
     type(unit_arg_t), pointer :: unit_arg
   contains
     procedure :: register => cubetopology_spevsys_register
     procedure :: parse    => cubetopology_spevsys_parse
  end type spevsys_opt_t
  !
  type spevsys_user_t
     logical               :: do = .false.
     character(len=argu_l) :: velo = strg_star
     character(len=argu_l) :: unit = strg_star
   contains
     procedure :: init          => cubetopology_spevsys_user_init
     ! procedure :: def_substruct => cubetopology_spevsys_user_def_substruct
     procedure :: toprog        => cubetopology_spevsys_user_toprog
     procedure :: list          => cubetopology_spevsys_user_list
  end type spevsys_user_t
  !
  type spevsys_prog_t
     real(kind=coor_k) :: velo = 0d0
  end type spevsys_prog_t
  !
contains
  !
  subroutine cubetopology_spevsys_register(vsys,abstract,error)
    use cubetools_unit
    !----------------------------------------------------------------------
    ! Register a /VSYS option according to a name and abstract
    ! provided by the command
    !----------------------------------------------------------------------
    class(spevsys_opt_t), intent(out)   :: vsys
    character(len=*),     intent(in)    :: abstract
    logical,              intent(inout) :: error
    !
    type(standard_arg_t) :: stdarg
    type(unit_arg_t) :: unitarg
    !
    character(len=*), parameter :: rname='VSYS>REGISTER'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         'VSYS','velo [unit]',&
         abstract,&
         strg_id,&
         vsys%opt,error)
    if (error) return
    call stdarg%register(&
         'vsys',&
         'New systemic velocity',&
         '"*" or "=" mean previous value is kept',&
         code_arg_mandatory,&
         error)
    if (error) return
    call unitarg%register(&
         'unit',&
         'Velocity unit',&
         '"*" or "=" mean previous value is kept',&
         code_arg_optional,&
         code_unit_velo,&
         vsys%unit_arg,&
         error)
    if (error) return
  end subroutine cubetopology_spevsys_register
  !
  subroutine cubetopology_spevsys_parse(vsys,line,user,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spevsys_opt_t), intent(in)    :: vsys
    character(len=*),     intent(in)    :: line
    type(spevsys_user_t), intent(out)   :: user
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='VSYS>PARSE'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    call vsys%opt%present(line,user%do,error)
    if (error) return
    if (user%do) then
       call cubetools_getarg(line,vsys%opt,1,user%velo,.not.mandatory,error)
       if (error) return
       call cubetools_getarg(line,vsys%opt,2,user%unit,.not.mandatory,error)
       if (error) return
    endif
  end subroutine cubetopology_spevsys_parse
  !
  !------------------------------------------------------------------------
  !
  subroutine cubetopology_spevsys_user_init(vsys,error)
    !----------------------------------------------------------------------
    ! Initialize by setting the intent of vsys to out
    !----------------------------------------------------------------------
    class(spevsys_user_t), intent(out)   :: vsys 
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='VSYS>USER>INIT'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
  end subroutine cubetopology_spevsys_user_init
  !
  ! subroutine cubetopology_spevsys_user_def_substruct(vsys,struct,error)
  !   use cubetools_userstruct
  !   !----------------------------------------------------------------------
  !   !
  !   !----------------------------------------------------------------------
  !   class(spevsys_user_t), intent(in)    :: vsys
  !   type(userstruct_t),    intent(inout) :: struct
  !   logical,               intent(inout) :: error
  !   !
  !   type(userstruct_t) :: substruct
  !   character(len=*), parameter :: rname='VSYS>USER>DEF>SUBSTRUCT'
  !   !
  !   call cubetopology_message(toposeve%trace,rname,'Welcome')
  !   !
  !   ! *** JP What happens if the sub-structure already exists?
  !   call struct%def_substruct('vsys',substruct,error)
  !   if (error) return
  !   call substruct%set_member('value',vsys%velo,error)
  !   if (error) return
  !   call substruct%set_member('unit',vsys%unit,error)
  !   if (error) return
  ! end subroutine cubetopology_spevsys_user_def_substruct
  !
  subroutine cubetopology_spevsys_user_toprog(user,cube,prog,error)
    use cubetools_unit
    use cubetools_user2prog
    use cubetools_header_methods
    use cube_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spevsys_user_t), intent(in)    :: user
    type(cube_t),          intent(in)    :: cube
    type(spevsys_prog_t),  intent(out)   :: prog
    logical,               intent(inout) :: error
    !
    type(unit_user_t) :: unit
    real(kind=coor_k) :: default
    character(len=*), parameter :: rname='VSYS>USER>TO>PROG'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    call unit%get_from_name_for_code(user%unit,code_unit_velo,error)
    if (error) return
    call cubetools_header_get_frame_velocity(cube%head,default,error)
    if (error) return
    call cubetools_user2prog_resolve_star(user%velo,unit,default,prog%velo,error)
    if (error) return
  end subroutine cubetopology_spevsys_user_toprog
  !
  subroutine cubetopology_spevsys_user_list(vsys,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spevsys_user_t), intent(in)    :: vsys 
    logical,               intent(inout) :: error
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='VSYS>USER>LIST'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    write(mess,'(2a,x,a)') 'Vsys      : ',trim(vsys%velo),trim(vsys%unit)
    call cubetopology_message(seve%r,rname,mess)
  end subroutine cubetopology_spevsys_user_list
end module cubetopology_spevsys_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
