!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetopology_spasamp_types
  use cubetools_parameters
  use cubetools_structure
  use cubetools_unit_arg
  use cubetools_unit
  use cubetopology_messaging
  !
  public :: spasamp_opt_t,spasamp_user_t,spasamp_prog_t
  private
  !
  type spasamp_opt_t
     type(option_t),   pointer :: opt
     type(unit_arg_t), pointer :: unit
   contains
     procedure :: register  => cubetopology_spasamp_register
     procedure :: parse     => cubetopology_spasamp_parse
  end type spasamp_opt_t
  !
  type spasamp_user_t
     logical               :: do = .false.
     character(len=argu_l) :: x = strg_star
     character(len=argu_l) :: y = strg_star
     character(len=argu_l) :: unit = strg_star
   contains
     procedure :: init          => cubetopology_spasamp_user_init
     ! procedure :: def_substruct => cubetopology_spasamp_user_def_substruct
     procedure :: toprog        => cubetopology_spasamp_user_toprog 
     procedure :: list          => cubetopology_spasamp_user_list
  end type spasamp_user_t
  !
  type spasamp_prog_t
     real(kind=coor_k) :: x = 0d0
     real(kind=coor_k) :: y = 0d0
     integer(kind=code_k) :: unit = code_unit_unk
  end type spasamp_prog_t
  !
contains
  !
  subroutine cubetopology_spasamp_register(samp,name,abstract,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spasamp_opt_t), intent(out)   :: samp
    character(len=*),     intent(in)    :: name
    character(len=*),     intent(in)    :: abstract
    logical,              intent(inout) :: error
    !
    type(standard_arg_t) :: stdarg
    type(unit_arg_t) :: unitarg
    character(len=*), parameter :: rname='SAMPLING>REGISTER'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         name,'sampl [sampm [unit]]',&
         abstract,&
         strg_id,&
         samp%opt,error)
    if (error) return
    call stdarg%register( &
         'sampl','Sampling in L coordinate', &
         '"*" or "=" mean previous value is kept',&
         code_arg_mandatory, &
         error)
    if (error) return
    call stdarg%register( &
         'sampm','Sampling in M coordinate', &
         '= means unchanged, * means equal to sampl',&
         code_arg_optional, &
         error)
    if (error) return
    call unitarg%register( &
         'unit',  &
         'Sampl and sampm unit', &
         '"*" or "=" means current unit is used',&
         code_arg_optional, &
         code_unit_fov, &
         samp%unit,&
         error)
    if (error) return
  end subroutine cubetopology_spasamp_register
  !
  subroutine cubetopology_spasamp_parse(samp,line,user,error)
    !----------------------------------------------------------------------
    ! /SAMPLING x y [unit]
    !----------------------------------------------------------------------
    class(spasamp_opt_t), intent(in)    :: samp  
    character(len=*),     intent(in)    :: line
    type(spasamp_user_t), intent(out)   :: user
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPASAMP>PARSE'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    user%x = strg_star
    user%y = strg_star
    user%unit = strg_star
    !
    call samp%opt%present(line,user%do,error)
    if (error) return
    if (user%do) then
       call cubetools_getarg(line,samp%opt,1,user%x,mandatory,error)
       if (error) return
       call cubetools_getarg(line,samp%opt,2,user%y,.not.mandatory,error)
       if (error) return
       call cubetools_getarg(line,samp%opt,3,user%unit,.not.mandatory,error)
       if (error) return
    endif
  end subroutine cubetopology_spasamp_parse
  !
  !------------------------------------------------------------------------
  !
  subroutine cubetopology_spasamp_user_init(sampling,error)
    !----------------------------------------------------------------------
    ! Initialize by setting the intent of sampling to out
    !----------------------------------------------------------------------
    class(spasamp_user_t), intent(out)   :: sampling 
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPASAMP>USER>INIT'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
  end subroutine cubetopology_spasamp_user_init
  !
  ! subroutine cubetopology_spasamp_user_def_substruct(samp,struct,error)
  !   use cubetools_userstruct
  !   !----------------------------------------------------------------------
  !   !
  !   !----------------------------------------------------------------------
  !   class(spasamp_user_t), intent(in)    :: samp
  !   type(userstruct_t),    intent(inout) :: struct
  !   logical,               intent(inout) :: error
  !   !
  !   type(userstruct_t) :: substruct
  !   character(len=*), parameter :: rname='SPASAMP>USER>DEF>SUBSTRUCT'
  !   !
  !   call cubetopology_message(toposeve%trace,rname,'Welcome')
  !   !
  !   ! *** JP What happens if the sub-structure alreay exists?
  !   call struct%def_substruct('sampling',substruct,error)
  !   if (error) return
  !   call substruct%set_member('x',samp%x,error)
  !   if (error) return
  !   call substruct%set_member('y',samp%y,error)
  !   if (error) return
  !   call substruct%set_member('unit',samp%unit,error)
  !   if (error) return
  ! end subroutine cubetopology_spasamp_user_def_substruct
  !
  subroutine cubetopology_spasamp_user_toprog(user,cube,prog,error)
    use cubetools_user2prog
    use cubetools_header_methods
    use cube_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spasamp_user_t), intent(in)    :: user
    type(cube_t),          intent(in)    :: cube
    type(spasamp_prog_t),  intent(out)   :: prog
    logical,               intent(inout) :: error
    !
    type(unit_user_t) :: unit
    real(kind=beam_k) :: radius
    real(kind=coor_k) :: default
    character(len=*), parameter :: rname='SPASAMP>USER>TOPROG'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    ! *** JP Some disambiguization would be needed here
    call sic_upper(user%unit)
    if (user%unit.eq.'BOX') then
       prog%unit = code_unit_pixe
       call unit%get_from_code(code_unit_pixe,error)
       if (error) return
       default = 10 ! boxes
       prog%x = 10
       prog%y = 10
    else
       prog%unit = code_unit_fov
       call unit%get_from_name_for_code(user%unit,code_unit_fov,error)
       if (error) return
       call cubetools_header_beamradius(cube%head,radius,error)
       if (error) return
       default = radius
       prog%x = radius
       prog%y = radius
    endif
    call cubetools_user2prog_resolve_star(user%x,unit,default,prog%x,error)
    if (error) return
    call cubetools_user2prog_resolve_star(user%y,unit,default,prog%y,error)
    if (error) return
  end subroutine cubetopology_spasamp_user_toprog
  !
  subroutine cubetopology_spasamp_user_list(samp,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spasamp_user_t), intent(in)    :: samp 
    logical,               intent(inout) :: error
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='SPASAMP>USER>LIST'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    write(mess,'(6a)') &
         'Sampling  : (',trim(samp%x),',',trim(samp%y),') ',samp%unit
    call cubetopology_message(seve%r,rname,mess)
  end subroutine cubetopology_spasamp_user_list
end module cubetopology_spasamp_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
