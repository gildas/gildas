!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetopology_speline_types
  use cubetools_parameters
  use cubetools_structure
  use cubetools_unit_arg
  use cubetopology_messaging
  !
  public :: speline_opt_t,speline_user_t,speline_prog_t
  private
  !
  type speline_opt_t
     type(option_t),   pointer :: opt
     type(unit_arg_t), pointer :: unit_arg
   contains
     procedure :: register => cubetopology_speline_register
     procedure :: parse    => cubetopology_speline_parse
  end type speline_opt_t
  !
  type speline_user_t
     logical               :: do = .false.
     character(len=argu_l) :: name = strg_star
     character(len=argu_l) :: freq = strg_star
     character(len=argu_l) :: unit = strg_star
   contains
     procedure :: init          => cubetopology_speline_user_init
     ! procedure :: def_substruct => cubetopology_speline_user_def_substruct
     procedure :: toprog        => cubetopology_speline_user_toprog
     procedure :: list          => cubetopology_speline_user_list
  end type speline_user_t
  !
  type speline_prog_t
     character(len=line_l) :: name = strg_unk
     real(kind=coor_k)     :: freq = 0d0
  end type speline_prog_t
  !
contains
  !
  subroutine cubetopology_speline_register(line,abstract,error)
    use cubetools_unit
    !----------------------------------------------------------------------
    ! Register a /LINE option according to a name and abstract
    ! provided by the command
    !----------------------------------------------------------------------
    class(speline_opt_t), intent(out)   :: line
    character(len=*),     intent(in)    :: abstract
    logical,              intent(inout) :: error
    !
    type(standard_arg_t) :: stdarg
    type(unit_arg_t) :: unitarg
    !
    character(len=*), parameter :: rname='SPELINE>REGISTER'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         'FREQUENCY','line frequency [unit]',&
         abstract,&
         strg_id,&
         line%opt,error)
    if (error) return
    call stdarg%register( &
         'line',  &
         'Line name', &
         '"*" or "=" mean previous value is kept',&
         code_arg_mandatory, &
         error)
    if (error) return
    call stdarg%register( &
         'frequency',  &
         'Line frequency', &
         '"*" or "=" mean previous value is kept',&
         code_arg_mandatory, &
         error)
    if (error) return
    call unitarg%register( &
         'unit',  &
         'Frequency unit', &
         '"*" or "=" mean previous value is kept',&
         code_arg_optional, &
         code_unit_freq, &
         line%unit_arg, &
         error)
    if (error) return
  end subroutine cubetopology_speline_register
  !
  subroutine cubetopology_speline_parse(line,cline,user,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(speline_opt_t), intent(in)    :: line
    character(len=*),     intent(in)    :: cline
    type(speline_user_t), intent(out)   :: user
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPELINE>PARSE'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    user%name = strg_star
    user%freq = strg_star
    user%unit = strg_star
    !
    call line%opt%present(cline,user%do,error)
    if (error) return
    if (user%do) then
       call cubetools_getarg(cline,line%opt,1,user%name,mandatory,error)
       if (error) return
       call cubetools_getarg(cline,line%opt,2,user%freq,.not.mandatory,error)
       if (error) return
       call cubetools_getarg(cline,line%opt,3,user%unit,.not.mandatory,error)
       if (error) return
    endif
  end subroutine cubetopology_speline_parse
  !
  !------------------------------------------------------------------------
  !
  subroutine cubetopology_speline_user_init(line,error)
    !----------------------------------------------------------------------
    ! Initialize by setting the intent of line to out
    !----------------------------------------------------------------------
    class(speline_user_t), intent(out)   :: line 
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPELINE>USER>INIT'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
  end subroutine cubetopology_speline_user_init
  !
  ! subroutine cubetopology_speline_user_def_substruct(line,struct,error)
  !   use cubetools_userstruct
  !   !----------------------------------------------------------------------
  !   !
  !   !----------------------------------------------------------------------
  !   class(speline_user_t), intent(in)    :: line
  !   type(userstruct_t),    intent(inout) :: struct
  !   logical,               intent(inout) :: error
  !   !
  !   type(userstruct_t) :: substruct
  !   character(len=*), parameter :: rname='SPELINE>USER>DEF>SUBSTRUCT'
  !   !
  !   call cubetopology_message(toposeve%trace,rname,'Welcome')
  !   !
  !   ! *** JP What happens if the sub-structure already exists?
  !   call struct%def_substruct('freq',substruct,error)
  !   if (error) return
  !   call substruct%set_member('name',line%name,error)
  !   if (error) return
  !   call substruct%set_member('val',line%freq,error)
  !   if (error) return
  !   call substruct%set_member('unit',line%unit,error)
  !   if (error) return
  ! end subroutine cubetopology_speline_user_def_substruct
  !
  subroutine cubetopology_speline_user_toprog(user,cube,prog,error)
    use cubetools_unit
    use cubetools_user2prog
    use cubetools_header_methods
    use cube_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(speline_user_t), intent(in)    :: user
    type(cube_t),          intent(in)    :: cube
    type(speline_prog_t),  intent(out)   :: prog
    logical,               intent(inout) :: error
    !
    real(kind=coor_k) :: default
    character(len=line_l) :: defline
    type(unit_user_t) :: unit
    character(len=*), parameter :: rname='SPELINE>USER>TOPROG'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    if (user%do) then
       call unit%get_from_name_for_code(user%unit,code_unit_freq,error)
       if (error) return
       call cubetools_header_get_line(cube%head,defline,error)
       if (error) return
       call cubetools_user2prog_resolve_star(user%name,defline,prog%name,error)
       if (error) return
       call cubetools_header_get_rest_frequency(cube%head,default,error)
       if (error) return
       prog%freq = default
       call cubetools_user2prog_resolve_star(user%freq,unit,default,prog%freq,error)
       if (error) return
    else
       call cubetools_header_get_rest_frequency(cube%head,prog%freq,error)
       if (error) return
       call cubetools_header_get_line(cube%head,prog%name,error)
       if (error) return
    endif
  end subroutine cubetopology_speline_user_toprog
  !
  subroutine cubetopology_speline_user_list(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(speline_user_t), intent(in)    :: line 
    logical,               intent(inout) :: error
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='SPELINE>USER>LIST'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    write(mess,'(2a)')     'Line      : ',trim(line%name)
    call cubetopology_message(seve%r,rname,mess)
    write(mess,'(2a,x,a)') 'Frequency : ',trim(line%freq),line%unit
    call cubetopology_message(seve%r,rname,mess)
  end subroutine cubetopology_speline_user_list
end module cubetopology_speline_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
