!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetopology_spaelli_types
  use cubetools_parameters
  use cubetools_structure
  use cubetools_unit_arg
  use cubetopology_messaging
  !
  public :: spaelli_comm_t,spaelli_user_t,spaelli_prog_t
  private
  !
  type spaelli_comm_t
     type(option_t),   pointer, private :: key
     type(unit_arg_t), pointer, private :: unit
     type(unit_arg_t), pointer, private :: paunit
   contains
     procedure :: register => cubetopology_spaelli_comm_register
     procedure :: parse    => cubetopology_spaelli_comm_parse
  end type spaelli_comm_t
  !
  type spaelli_user_t
     character(len=argu_l) :: major  ! [user unit] Major axis diameter
     character(len=argu_l) :: minor  ! [user unit] Minor axis diameter
     character(len=argu_l) :: unit   ! User unit for major and minor
     character(len=argu_l) :: pang   ! [user unit] Position Angle
     character(len=argu_l) :: paunit ! User unit for pang
     logical               :: do
   contains
     procedure :: toprog => cubetopology_spaelli_user_toprog
     procedure :: list   => cubetopology_spaelli_user_list
  end type spaelli_user_t
  !
  type spaelli_prog_t
     real(kind=coor_k), public :: major ! [rad]
     real(kind=coor_k), public :: minor ! [rad] 
     real(kind=coor_k), public :: pang  ! [rad]
   contains
     procedure :: list   => cubetopology_spaelli_prog_list
  end type spaelli_prog_t
  !
contains
  !
  subroutine cubetopology_spaelli_comm_register(comm,name,abstract,error)
    use cubetools_unit
    !----------------------------------------------------------------------
    ! Register an ellipse key
    !----------------------------------------------------------------------
    class(spaelli_comm_t), intent(out)   :: comm
    character(len=*),      intent(in)    :: name
    character(len=*),      intent(in)    :: abstract
    logical,               intent(inout) :: error
    !
    type(standard_arg_t) :: stdarg
    type(unit_arg_t) :: unitarg
    character(len=*), parameter :: rname='SPAELLI>COMM>REGISTER'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         name,'major [minor [unit [pang [paunit]]]]',&
         abstract,&
         'If major or minor are set to *, the current beamsize is&
         & assumed. If minor axis is omited it is assumed to be equal&
         & to the major axis, i.e. a circle. If pang is&
         & omited an angle of 0 degrees from north is&
         & assumed.', comm%key,error)
    if (error) return
    call stdarg%register( &
         'major',  &
         'Major axis diameter', &
         strg_id,&
         code_arg_mandatory, &
         error)
    if (error) return
    call stdarg%register( &
         'minor',  &
         'Minor axis diameter', &
         strg_id,&
         code_arg_optional, &
         error)
    if (error) return
    call unitarg%register( &
         'unit', &
         'diameter unit',&
         strg_id,&
         code_arg_optional,&
         code_unit_fov,&
         comm%unit,error)
    if (error) return
    call stdarg%register( &
         'pang',  &
         'Position angle', &
         strg_id,&
         code_arg_optional, &
         error)
    if (error) return
    call unitarg%register( &
         'paunit', &
         'Position angle unit',&
         strg_id,&
         code_arg_optional,&
         code_unit_pang,&
         comm%paunit,error)
    if (error) return
  end subroutine cubetopology_spaelli_comm_register
  !
  subroutine cubetopology_spaelli_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spaelli_comm_t), intent(in)    :: comm
    character(len=*),      intent(in)    :: line
    type(spaelli_user_t),  intent(out)   :: user
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPAELLI>COMM>PARSE'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    user%major  = strg_star
    user%minor  = strg_star
    user%unit   = strg_star
    user%pang   = strg_star
    user%paunit = strg_star
    !
    call comm%key%present(line,user%do,error)
    if (error) return
    !
    if (user%do) then
       call cubetools_getarg(line,comm%key,1,user%major,mandatory,error)
       if (error) return
       user%minor = user%major
       call cubetools_getarg(line,comm%key,2,user%minor,optional,error)
       if (error) return
       call cubetools_getarg(line,comm%key,3,user%unit,optional,error)
       if (error) return
       call cubetools_getarg(line,comm%key,4,user%pang,optional,error)
       if (error) return
       call cubetools_getarg(line,comm%key,5,user%paunit,optional,error)
       if (error) return
    endif
  end subroutine cubetopology_spaelli_comm_parse
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetopology_spaelli_user_toprog(user,cube,prog,error)
    use cube_types
    use cubetools_unit
    use cubetools_user2prog
    use cubetools_header_methods
    use cubetools_beam_types
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    class(spaelli_user_t), intent(in)    :: user
    type(cube_t), target,  intent(in)    :: cube
    type(spaelli_prog_t),  intent(out)   :: prog
    logical,               intent(inout) :: error
    !
    type(unit_user_t) :: fovunit,paunit
    real(kind=coor_k) :: defmajor,defminor,defangle
    type(beam_t) :: beam
    character(len=*), parameter :: rname='SPAELLI>USER>TOPROG'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    call cubetools_header_get_spabeam(cube%head,beam,error)
    if (error) return
    defangle = 0d0
    defmajor = beam%major
    defminor = beam%minor
    call fovunit%get_from_name_for_code(user%unit,code_unit_fov,error)
    if (error) return
    call paunit%get_from_name_for_code(user%paunit,code_unit_pang,error)
    if (error) return
    call cubetools_user2prog_resolve_star(user%major,fovunit,defmajor,prog%major,error)
    if (error) return
    call cubetools_user2prog_resolve_star(user%minor,fovunit,defminor,prog%minor,error)
    if (error) return
    call cubetools_user2prog_resolve_star(user%pang,paunit,defangle,prog%pang,error)
    if (error) return
  end subroutine cubetopology_spaelli_user_toprog
  !
  subroutine cubetopology_spaelli_user_list(user,error)
    !------------------------------------------------------------------------
    ! Mostly for debugging
    !------------------------------------------------------------------------
    class(spaelli_user_t), intent(in)    :: user
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPAELLI>USER>LIST'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    print *,'Has user been parsed?     ',user%do
    print *,'User major                ',trim(user%major)
    print *,'User minor                ',trim(user%minor)
    print *,'User major and minor unit ',trim(user%unit)
    print *,'User position angle       ',trim(user%pang)
    print *,'User position angle  unit ',trim(user%paunit)
  end subroutine cubetopology_spaelli_user_list
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetopology_spaelli_prog_list(prog,error)
    use cubetools_unit
    use cubetools_format
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spaelli_prog_t), intent(in)    :: prog
    logical,               intent(inout) :: error
    !
    character(len=mess_l) :: mess
    type(unit_user_t) :: axesunit,pangunit
    character(len=*), parameter :: rname='SPAELLI>PROG>LIST'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    call axesunit%get_from_code(code_unit_fov,error)
    if (error) return
    call pangunit%get_from_code(code_unit_pang,error)
    if (error) return
    mess = cubetools_format_stdkey_boldval('Major',prog%major*axesunit%user_per_prog,'f8.3',22)
    mess = trim(mess)//'  '//cubetools_format_stdkey_boldval('Minor',prog%minor*axesunit%user_per_prog,'f8.3',22)
    mess = trim(mess)//'  '//cubetools_format_stdkey_boldval('PA',prog%pang*pangunit%user_per_prog,'f8.3',10)
    call cubetopology_message(seve%r,rname,mess)
  end subroutine cubetopology_spaelli_prog_list
end module cubetopology_spaelli_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
