!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetopology_spechannel_types
  use cubetools_parameters
  use cubetools_structure
  use cubetools_keywordlist_types
  use cubetools_unit_arg
  use cubetopology_messaging
  !
  public :: spechannel_opt_t,spechannel_user_t,spechannel_prog_t
  private
  !
  type spechannel_opt_t
     type(option_t),           pointer :: opt
     type(keywordlist_comm_t), pointer :: coortype
     type(unit_arg_t),         pointer :: unit
   contains
     procedure :: register  => cubetopology_spechannel_register
     procedure :: parse     => cubetopology_spechannel_parse
  end type spechannel_opt_t
  !
  type spechannel_user_t
     character(len=argu_l) :: chan  = strg_unk ! Channel velocity
     character(len=argu_l) :: type  = strg_unk ! RELATIVE|ABSOLUTE channel velocity is absolute or relative
     character(len=argu_l) :: unit  = strg_unk ! Channel unit 
     logical               :: do    = .false.  ! Option was present
   contains
     procedure :: init   => cubetopology_spechannel_user_init
     procedure :: toprog => cubetopology_spechannel_user_toprog
  end type spechannel_user_t
  !
  type spechannel_prog_t
     integer(kind=chan_k) :: ic ! Nearest integer channel
  end type spechannel_prog_t
  !
contains
  !
  subroutine cubetopology_spechannel_register(option,name,abstract,error)
    use cubetools_unit
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    class(spechannel_opt_t), intent(out)   :: option
    character(len=*),        intent(in)    :: name
    character(len=*),        intent(in)    :: abstract
    logical,                 intent(inout) :: error
    !
    type(standard_arg_t) :: stdarg
    type(keywordlist_comm_t)  :: keyarg
    type(unit_arg_t)     :: unitarg
    character(len=*), parameter :: ctypes(2) = ['Absolute','Relative']
    character(len=*), parameter :: rname='CHANNEL>REGISTER'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         name,'velocity type [unit]',&
         abstract,&
         strg_id,&
         option%opt,error)
    if (error) return
    call stdarg%register(&
         'velocity',&
         'Velocity of the channel',&
         strg_id,&
         code_arg_mandatory,error)
    if (error) return
    call keyarg%register( &
         'type',  &
         'Velocity is absolute or relative to systemic velocity', &
         strg_id,&
         code_arg_mandatory,&
         ctypes, &
         .not.flexible, &
         option%coortype,&
         error)
    if (error) return
    call unitarg%register( &
         'unit',  &
         'Velocity unit', &
         strg_id,&
         code_arg_optional,&
         code_unit_velo, &
         option%unit,&
         error)
    if (error) return
  end subroutine cubetopology_spechannel_register
  !
  subroutine cubetopology_spechannel_parse(option,line,user,error)
    !----------------------------------------------------------------------
    ! /CHANNEL Vel type [unit]
    !----------------------------------------------------------------------
    class(spechannel_opt_t), intent(in)    :: option
    character(len=*),        intent(in)    :: line
    type(spechannel_user_t), intent(out)   :: user
    logical,                 intent(inout) :: error
    !
    character(len=*), parameter :: rname='CHANNEL>PARSE'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    user%chan = strg_star
    user%type = strg_star
    user%unit = strg_star    
    !
    call option%opt%present(line,user%do,error)
    if (error) return
    if (user%do) then
       call cubetools_getarg(line,option%opt,1,user%chan,mandatory,error)
       if (error) return
       call cubetools_getarg(line,option%opt,2,user%type,mandatory,error)
       if (error) return
       call cubetools_getarg(line,option%opt,3,user%unit,.not.mandatory,error)
       if (error) return
    else
       ! Do nothing
    endif
  end subroutine cubetopology_spechannel_parse
  !
  subroutine cubetopology_spechannel_user_init(user,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spechannel_user_t), intent(in)    :: user
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter     :: rname='CHANNEL>USER>INIT'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
  end subroutine cubetopology_spechannel_user_init
  !
  subroutine cubetopology_spechannel_user_list(user,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spechannel_user_t), intent(in)    :: user 
    logical,                  intent(inout) :: error
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='CHANNEL>USER>LIST'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    write(mess,'(2a,x,a,x,a)') &
         'Channel   : ',trim(user%chan),trim(user%type),trim(user%unit)
    call cubetopology_message(seve%r,rname,mess)
  end subroutine cubetopology_spechannel_user_list
  !
  subroutine cubetopology_spechannel_user_toprog(user,option,cube,prog,error)
    use cube_types
    use cubetools_user2prog
    use cubetools_unit
    use cubetopology_tool
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spechannel_user_t), intent(in)    :: user
    type(spechannel_opt_t),   intent(in)    :: option
    type(cube_t),             intent(in)    :: cube
    type(spechannel_prog_t),  intent(out)   :: prog
    logical,                  intent(inout) :: error
    !
    integer(kind=4) :: ikey
    character(len=12) :: type
    type(unit_user_t) :: velunit
    real(kind=coor_k) :: velo,default
    character(len=*), parameter     :: rname='CHANNEL>USER>TOPROG'
    !
    call cubetopology_message(toposeve%trace,rname,'Welcome')
    !
    call cubetools_keywordlist_user2prog(option%coortype,user%type,ikey,type,error)
    if (error) return
    call velunit%get_from_name_for_code(user%unit,code_unit_velo,error)
    if (error) return
    select case(type)
    case('ABSOLUTE')
       default = cube%head%spe%ref%v
       call cubetools_user2prog_resolve_star(user%chan,velunit,default,velo,error)
       if (error) return
    case('RELATIVE')
       default = 0d0
       call cubetools_user2prog_resolve_star(user%chan,velunit,default,velo,error)
       if (error) return
       velo = velo+cube%head%spe%ref%v
    end select
    call cubetopology_tool_velocity2channel(cube,velo,prog%ic,error)
    if (error) return
  end subroutine cubetopology_spechannel_user_toprog
end module cubetopology_spechannel_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

