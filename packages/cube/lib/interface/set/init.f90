!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeset_init
  use cubeset_buffer
  use cubeset_spatial
  use cubeset_spectral
  use cubeset_messaging
  !
  public :: cubeset_library_init
  private
  !
contains
  !
  subroutine cubeset_library_init(error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    character(len=*), parameter :: rname='LIBRARY>INIT'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    call cubeset%init('cubeset',error)
    if (error) return
    call userspe%init(error)
    if (error) return
    call userspa%init(error)
    if (error) return
  end subroutine cubeset_library_init
end module cubeset_init
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
