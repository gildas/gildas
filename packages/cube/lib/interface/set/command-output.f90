!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeset_output
  use cubetools_structure
  use cubetools_switch_types
  use cubesyntax_key_types
  use cubeset_messaging
  !
  public :: output
  private
  !
  type :: output_comm_t
     type(option_t), pointer :: comm
     type(switch_comm_t) :: extrema
     type(switch_comm_t) :: todisk
     type(key_comm_t)    :: default
   contains
     procedure, public  :: register => cubeset_output_register
     procedure, private :: parse    => cubeset_output_parse
     procedure, private :: main     => cubeset_output_main
  end type output_comm_t
  type(output_comm_t) :: output
  !
  type :: output_user_t
     logical :: list
     type(switch_user_t) :: extrema
     type(switch_user_t) :: todisk
     type(key_user_t)    :: default
   contains
     procedure, private :: toprog => cubeset_output_user_toprog
  end type output_user_t
  !
  type :: output_prog_t
     logical :: list
     type(switch_prog_t) :: extrema
     type(switch_prog_t) :: todisk
     type(key_prog_t)    :: default
   contains
     procedure, private :: act => cubeset_output_prog_act
  end type output_prog_t
  !
contains
  !
  subroutine cubeset_output_command(line,error)
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(output_user_t) :: user
    character(len=*), parameter :: rname='OUTPUT>COMMAND'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    call output%parse(line,user,error)
    if (error) return
    call output%main(user,error)
    if (error) return
  end subroutine cubeset_output_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubeset_output_register(output,error)
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    class(output_comm_t), intent(inout) :: output
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: comm_abstract = &
         'Set how output cubes are handled'
    character(len=*), parameter :: comm_help = &
         '/EXTREMA Enables or disables the automatic computation&
         &/update of the extrema section in the header of the output&
         & cubes. Default is ON. /TODISK Enables or disables&
         & writing the cubes from memory to disk at the end of the&
         & processing. This makes sense for memory mode only.&
         & Default is ON.'
    character(len=*), parameter :: rname='OUTPUT>REGISTER'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'OUTPUT','',&
         comm_abstract,&
         comm_help,&
         cubeset_output_command,&
         output%comm,error)
    if (error) return
    !
    call output%extrema%register(&
         'EXTREMA',&
         'automatic extrema calculation',&
         'ON',error)
    if (error) return
    call output%todisk%register(&
         'TODISK',&
         'flushing to disk in disk mode',&
         'ON',error)
    if (error) return
    call output%default%register(&
         'DEFAULT','Reset to default values',error)
    if (error) return
  end subroutine cubeset_output_register
  !
  subroutine cubeset_output_parse(comm,line,user,error)
    use cubeadm_setup
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(output_comm_t), intent(inout) :: comm
    character(len=*),     intent(in)    :: line
    type(output_user_t),  intent(out)   :: user
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='OUTPUT>PARSE'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    user%list = cubetools_nopt().eq.0
    !
    if (user%list) then
       ! nothing to do
    else
       call comm%extrema%parse(line,user%extrema,error)
       if (error) return
       call comm%todisk%parse(line,user%todisk,error)
       if (error) return
       call comm%default%parse(line,user%default,error)
       if (error) return
    endif
  end subroutine cubeset_output_parse
  !
  subroutine cubeset_output_main(comm,user,error)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(output_comm_t), intent(inout) :: comm
    type(output_user_t),  intent(in)    :: user
    logical,              intent(inout) :: error
    !
    type(output_prog_t) :: prog
    character(len=*), parameter :: rname='OUTPUT>MAIN'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%act(error)
    if (error) return
  end subroutine cubeset_output_main
  !
  !-----------------------------------------------------------------------
  !
  subroutine cubeset_output_user_toprog(user,comm,prog,error)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(output_user_t), intent(in)    :: user
    type(output_comm_t),  intent(in)    :: comm
    type(output_prog_t),  intent(inout) :: prog
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='OUTPUT>USER>TOPROG'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    prog%list = user%list
    call user%extrema%toprog(comm%extrema,prog%extrema,error)
    if (error) return
    call user%todisk%toprog(comm%todisk,prog%todisk,error)
    if (error) return
    call user%default%toprog(comm%default,prog%default,error)
    if (error) return
  end subroutine cubeset_output_user_toprog
  !
  !-----------------------------------------------------------------------
  !
  subroutine cubeset_output_prog_act(prog,error)
    use cubetools_setup_types
    use cubeadm_setup
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(output_prog_t), intent(inout) :: prog
    logical,              intent(inout) :: error
    !
    type(cube_setup_output_t) :: defaults
    character(len=*), parameter :: rname='OUTPUT>PROG>ACT'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    if (prog%default%act) cubset%output         = defaults
    if (prog%extrema%act) cubset%output%extrema = prog%extrema%enabled
    if (prog%todisk%act)  cubset%output%write   = prog%todisk%enabled
    if (prog%list) then
       call cubeset_message(seve%r,rname,'  Output')
       prog%extrema%action = 'EXTREMA'
       prog%extrema%enabled = cubset%output%extrema
       call prog%extrema%list(error)
       if (error) return
       prog%todisk%action = 'TODISK'
       prog%todisk%enabled = cubset%output%write
       call prog%todisk%list(error)
       if (error) return
    endif
  end subroutine cubeset_output_prog_act
end module cubeset_output
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
