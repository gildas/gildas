!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeset_blanking
  use cubetools_structure
  use cubetools_keywordlist_types
  use cubetools_setup_types
  use cubeset_messaging 
  !
  public :: blanking
  private
  !
  type :: blanking_comm_t
     type(option_t),      pointer :: comm
     type(keywordlist_comm_t), pointer :: behaviour
   contains
     procedure, public  :: register => cubeset_blanking_register
     procedure, private :: parse    => cubeset_blanking_parse
     procedure, private :: print    => cubeset_blanking_print
  end type blanking_comm_t
  type(blanking_comm_t) :: blanking
  !
contains
  !
  subroutine cubeset_blanking_command(line,error)
    !---------------------------------------------------------------------
    ! Support routine for command
    ! BLANKING Mode
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='BLANKING>COMMAND'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    if (blanking%comm%getnarg().eq.0) then
       call blanking%print(error)
       return
    endif
    call blanking%parse(line,error)
    if (error) return
  end subroutine cubeset_blanking_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubeset_blanking_register(blanking,error)
    use cubeadm_setup
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    class(blanking_comm_t), intent(inout) :: blanking
    logical,                intent(inout) :: error
    !
    type(keywordlist_comm_t)  :: keyarg
    character(len=*), parameter :: comm_abstract = &
         'Set how blanked values are treated when reading'
    character(len=*), parameter :: comm_help = &
         'If mode is set to ERROR, an error will be raised if data&
         & contain Bval and Eval, else if mode is set to ONTHEFLY&
         & blanked values in the data will be automatically patched&
         & to NaN'
    character(len=*), parameter :: rname='BLANKING>REGISTER'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'BLANKING','[ERROR|ONTHEFLY]',&
         comm_abstract,&
         comm_help,&
         cubeset_blanking_command,&
         blanking%comm,error)
    if (error) return
    call keyarg%register( &
         'behaviour',  &
         'behaviour when encountering blanking values', &
         strg_id,&
         code_arg_optional, &
         patchblank, &
         .not.flexible, &
         blanking%behaviour, &
         error)
    if (error) return
  end subroutine cubeset_blanking_register
  !
  subroutine cubeset_blanking_parse(blanking,line,error)
    use cubeadm_setup
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(blanking_comm_t), intent(inout) :: blanking
    character(len=*),       intent(in)    :: line
    logical,                intent(inout) :: error
    !
    integer(kind=4) :: ikey
    character(len=16) :: keyword,argum
    character(len=*), parameter :: rname='BLANKING>PARSE'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    call cubetools_getarg(line,blanking%comm,1,argum,mandatory,error)
    if (error) return
    call cubetools_keywordlist_user2prog(blanking%behaviour,argum,ikey,keyword,error)
    if (error) return
    cubset%blanking%rmode = ikey
  end subroutine cubeset_blanking_parse
  !
  subroutine cubeset_blanking_print(blanking,error)
    use cubeadm_setup
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(blanking_comm_t), intent(inout) :: blanking
    logical,                intent(inout) :: error
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='BLANKING>PRINT'
    !
    call cubeset_message(seve%r,rname,'  Blanking')
    write(mess,'(a,a)') '    Read mode: ',patchblank(cubset%blanking%rmode)
    call cubeset_message(seve%r,rname,mess)
  end subroutine cubeset_blanking_print
end module cubeset_blanking
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
