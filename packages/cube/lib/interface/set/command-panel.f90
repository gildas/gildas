!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeset_panel
  use cubetools_parameters
  use cubetools_structure
  use cubetools_keywordlist_types
  use cubeset_messaging
  !
  public :: cubeset_panel_register
  private
  !
  type :: panel_comm_t
     type(option_t),      pointer :: panel    
     type(option_t),      pointer :: nxy      
     type(option_t),      pointer :: inter    
     type(option_t),      pointer :: size
     type(option_t),      pointer :: aspect   
     type(option_t),      pointer :: position
     type(keywordlist_comm_t), pointer :: preset_arg
     type(option_t),      pointer :: fixed
     type(keywordlist_comm_t), pointer :: corner_arg
     type(option_t),      pointer :: defaults  
     type(option_t),      pointer :: box      
  end type panel_comm_t
  type(panel_comm_t) :: comm
  !
  integer(kind=4), parameter :: pane_k = 4
  !
  integer(kind=code_k), parameter :: code_x = 1
  integer(kind=code_k), parameter :: code_y = 2
  !
  !-------------------------------------------------------------------
  integer(kind=code_k), parameter :: code_page_landscape = 1
  integer(kind=code_k), parameter :: code_page_portrait  = 2
  integer(kind=code_k), parameter :: code_page_square    = 3
  !-------------------------------------------------------------------
  integer(argu_k), parameter :: nframe = 5
  character(len=argu_l) :: frame_kinds(nframe)
  data frame_kinds /'USER','DEFAULT','PLOT','HEADER','WEDGE'/
  integer(kind=code_k), parameter :: code_frame_user    = 1
  integer(kind=code_k), parameter :: code_frame_default = 2
  integer(kind=code_k), parameter :: code_frame_plot    = 3
  integer(kind=code_k), parameter :: code_frame_header  = 4
  integer(kind=code_k), parameter :: code_frame_wedge   = 5
  !
  real(kind=pane_k), parameter :: landscape_default(4) = [0.214,1.322,0.125,0.925]
  real(kind=pane_k), parameter :: landscape_plot(4)    = [0.171,1.081,0.085,0.995]
  real(kind=pane_k), parameter :: landscape_header(4)  = [1.081,1.428,0.085,0.995]
!  real(kind=pane_k), parameter :: landscape_wedge(4)   = [1.190,1.420,0.250,0.480]
  real(kind=pane_k), parameter :: landscape_wedge(4)   = [1.140,1.370,0.300,0.530]
  !
  real(kind=pane_k), parameter :: portrait_default(4) = [0.125,0.875,0.100,1.250]
  real(kind=pane_k), parameter :: portrait_plot(4)    = [0.085,0.995,0.171,1.081]
  real(kind=pane_k), parameter :: portrait_header(4)  = [0.085,0.995,1.081,1.428]
  real(kind=pane_k), parameter :: portrait_wedge(4)   = [0.550,0.780,1.140,1.370]
  !
  real(kind=pane_k), parameter :: frame_positions(4,5,2) = reshape([ &
       landscape_default,landscape_default,landscape_plot,landscape_header,landscape_wedge, &
       portrait_default,portrait_default,portrait_plot,portrait_header,portrait_wedge], &
       [4,5,2])
  !-------------------------------------------------------------------
  integer(kind=argu_k), parameter :: nfixed = 5
  character(len=argu_l) :: fixed_corners(nfixed)
  data fixed_corners /'NONE','TRC','TLC','BRC','BLC'/
  integer(kind=code_k), parameter :: code_fixed_none = 1
  integer(kind=code_k), parameter :: code_fixed_trc  = 2
  integer(kind=code_k), parameter :: code_fixed_tlc  = 3
  integer(kind=code_k), parameter :: code_fixed_brc  = 4
  integer(kind=code_k), parameter :: code_fixed_blc  = 5
  !-------------------------------------------------------------------
  !
  type panel_user_t
     logical :: dolist = .true.
     integer(kind=code_k) :: page_geo = code_page_landscape ! By default page is in landscape
     real(kind=pane_k)    :: page_size(2)  
     real(kind=pane_k)    :: page_aspect
     integer(kind=code_k) :: frame_fixed_corner = code_fixed_none ! By default, center inside the frame position
     integer(kind=code_k) :: frame_kind = code_frame_default
     real(kind=pane_k)    :: frame_pos(4) = landscape_default ! Frame x and y coordinates
     real(kind=pane_k) :: ixy(2) = [1.0,1.0]   ! Panel number on the x and y axes
     real(kind=pane_k) :: nxy(2) = [1.0,1.0]   ! Number of panels on the x and y axes
     real(kind=pane_k) :: inter(2) = [0.0,0.0] ! 
     real(kind=pane_k) :: size(2)  = [1.0,1.0] ! 
     real(kind=pane_k) :: aspect = 0.0         ! Aspect ratio. 0.0 => Not taken into account
     logical :: dobox = .false.
  end type panel_user_t
  type panel_frame_t
     real(kind=pane_k) :: pos(2,2) ! Position
     real(kind=pane_k) :: mar(2)   ! Symmetrical margin
     real(kind=pane_k) :: blc(2)   ! pos(1,:)+mar
     real(kind=pane_k) :: size(2)
     ! VVV temporary workaround for cube procedures
     logical           :: landscape =.true.
  end type panel_frame_t
  type panel_panel_t
     real(kind=pane_k) :: min(2)  !
     real(kind=pane_k) :: max(2)  !
     real(kind=pane_k) :: size(2) !
     real(kind=pane_k) :: aspect  !
  end type panel_panel_t
  type panel_prog_t
     type(panel_user_t)  :: request
     type(panel_frame_t) :: frame
     type(panel_panel_t) :: current
  end type panel_prog_t
  !
  type(panel_prog_t) :: prog ! Global buffer storing the current state
  !
contains
  !
  subroutine cubeset_panel_register(error)
    !-------------------------------------------------------------------
    ! Support routine for register PANEL
    !-------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    type(standard_arg_t) :: stdarg
    type(keywordlist_comm_t)  :: keyarg
    character(len=*), parameter :: comm_abstract = &
         'Define and configure plot regions in GREG'
    character(len=*), parameter :: comm_help = &
         'if only I is given panel counting starts at the top left&
         & and ends at the bottom right, if both I and J are given I&
         & stands for the column (from left to right) and J the line&
         & (from bottom to top). If no option is given PANEL uses the&
         & previous plot configurations, in the case of the first&
         & call of PANEL in the CUBE session, CUBE plot defaults are&
         & used'
    character(len=*), parameter :: rname='PANEL>REGISTER'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'PANEL','[I [J]]',&
         comm_abstract,&
         comm_help,&
         cubeset_panel_command,&
         comm%panel,error)
    if (error) return
    call stdarg%register( &
         'I',  &
         'I-eth panel or Column', &
         strg_id,&
         code_arg_optional, &
         error)
    if (error) return
     call stdarg%register( &
         'J',  &
         'J-eth line', &
         strg_id,&
         code_arg_optional, &
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'NXY','nx [ny]',&
         'Define the grid of panels',&
         'If only nx is given PANEL will use a square configuration&
         & to display nx plots. If both nx and ny are given nx stand&
         & for the number of columns of plots and ny for the number&
         & of lines of plots, with nx*ny being the total number of&
         & plots', &
         comm%nxy,error)
    if (error) return
    call stdarg%register( &
         'nx',  &
         'Number of panels or number of columns', &
         strg_id,&
         code_arg_mandatory, &
         error)
    if (error) return
    call stdarg%register( &
         'ny',  &
         'Number of lines', &
         strg_id,&
         code_arg_optional, &
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'INTER','rx [ry]',& ! *** JP: In english, it should be SPACE...
         'Define the space between panels in the grid',&
         'If rx and ry are given rx is the space in the x direction&
         & and ry is the space on the y direction.  If only rx is&
         & given rx is the space in both directions.', &
         comm%inter,&
         error)
    if (error) return
    call stdarg%register( &
         'rx',  &
         'Space between panels along the x axis', &
         strg_id,&
         code_arg_mandatory, &
         error)
    if (error) return
    call stdarg%register( &
         'ry',  &
         'Space between panels along the y axis',&
         strg_id,&
         code_arg_optional, &
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'SIZE','sx [sy]',&
         'Define the panel size relatively to one grid rectangle',&
         'If sx and sy are given sx is the size in the x direction&
         & and sy is the size on the y direction.  If only sx is&
         & given sx is the size in both directions.', &
         comm%size,&
         error)
    if (error) return
    call stdarg%register( &
         'sx',  &
         'Panel size along the x axis. The default value is 1', &
         strg_id,&
         code_arg_mandatory, &
         error)
    if (error) return
    call stdarg%register( &
         'sy',  &
         'Panel size along the y axis. The default value is 1',&
         strg_id,&
         code_arg_optional, &
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'ASPECT','ratio',&
         'Set the aspect ratio of the panel',&
         'A ratio larger than 1 means that the X axis will be larger&
         & than the Y axis. If ratio is 0 or * the aspect ratio is&
         & chosen automatically to maximise plot area. Default is 0',&
         & comm%aspect,error)
    if (error) return
    call stdarg%register( &
         'ratio',  &
         'panel aspect ratio', &
         strg_id,&
         code_arg_mandatory, &
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'POSITION','preset | px1 px2 py1 py2',&
         'Set the area of the plot page to be used',&
         'Unit is in fraction of the total GREG plot page smallest&
         & side, e.g. 0.1 0.9 0.1 1.4. Using the same unit in both&
         & direction ensure that the same value correspond to the&
         & same length on the plot', &
         comm%position&
         &,error)
    if (error) return
    call keyarg%register( &
         'px1',  &
         'Panel position preset or left side x coordinate', &
         strg_id,&
         code_arg_mandatory, &
         frame_kinds, &
         flexible, &
         comm%preset_arg, &
         error)
    if (error) return
    call stdarg%register( &
         'px2',  &
         'right side x coordinate', &
         strg_id,&
         code_arg_optional, &
         error)
    if (error) return
    call stdarg%register( &
         'py1',  &
         'bottom side y coordinate', &
         strg_id,&
         code_arg_optional, &
         error)
    if (error) return
    call stdarg%register( &
         'py2',  &
         'top side y coordinate', &
         strg_id,&
         code_arg_optional, &
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'FIXED','[corner]',&
         'Fix panels to a corner of the total plot page',&
         'TRC for Top Right corner, TLC for the Top Left Corner, BRC&
         & for the Bottom Right Corner and BLC for the Bottom Left&
         & Corner', &
         comm%fixed,error)
    if (error) return
    call keyarg%register( &
         'corner',  &
         'Corner to fix panel to', &
         strg_id,&
         code_arg_optional, &
         fixed_corners, &
         .not.flexible, &
         comm%corner_arg, &
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'DEFAULT','',&
         'Reset PANEL defaults',&
         strg_id,&
         comm%defaults,error)
    if (error) return
    !
    call cubetools_register_option(&
         'BOX','',&
         'Draw a box around the current panel',&
         strg_id,&
         comm%box,error)
    if (error) return
  end subroutine cubeset_panel_register
  !
  subroutine cubeset_panel_command(line,error)
    !-------------------------------------------------------------------
    ! Support routine for command PANEL
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(panel_user_t) :: user
    character(len=*), parameter :: rname='PANEL>COMMAND'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    ! Start from global current request state
    user = prog%request
    !
    call cubeset_panel_parse(line,user,error)
    if (error) return
    call cubeset_panel_fetch_pagegeometry(user%page_size,user%page_aspect,user%page_geo,error)
    if (error) return
    call cubeset_panel_main(user,prog,error)
    if (error) return
    call cubeset_panel_sicdef(error)
    if (error) return
  end subroutine cubeset_panel_command
  !
  subroutine cubeset_panel_parse(line,user,error)
    !-------------------------------------------------------------------
    ! Parsing routine for command
    ! PANEL [I [J]]
    ! /NXY nx [ny]
    ! /INTERVAL rx [ry]
    ! /SIZE sx [sy]
    ! /ASPECT *|ratio
    ! /POSITION px1 px2 py1 py2 | header|plot|wedge|default
    ! /FIXED_CORNER [NONE|TRC|TLC|BRC|BLC]
    ! /DEFAULTS
    ! /BOX
    !-------------------------------------------------------------------
    character(len=*),   intent(in)    :: line
    type(panel_user_t), intent(inout) :: user
    logical,            intent(inout) :: error
    !
    type(panel_user_t) :: default
    logical :: donxy,dointer,dosize,doaspect,doposition,dodefaults
    character(len=*), parameter :: rname='PANEL>PARSE'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    call comm%defaults%present(line,dodefaults,error)
    if (error) return
    if (dodefaults)then
       user = default
    endif
    !
    ! *** JP => There must be a cleaner way to check the absence of arguments.
    call comm%nxy%present(line,donxy,error)
    if (error) return
    call comm%inter%present(line,dointer,error)
    if (error) return
    call comm%size%present(line,dosize,error)
    if (error) return
    call comm%aspect%present(line,doaspect,error)
    if (error) return
    call comm%position%present(line,doposition,error)
    if (error) return
    user%dolist = cubetools_nopt().eq.0
    user%dolist = user%dolist.and.(comm%panel%getnarg().eq.0)
    user%dolist = user%dolist.and.(.not.donxy)
    user%dolist = user%dolist.and.(.not.dointer)
    user%dolist = user%dolist.and.(.not.dosize)
    user%dolist = user%dolist.and.(.not.doaspect)
    user%dolist = user%dolist.and.(.not.doposition)
    call cubeset_panel_parse_xy(line,comm%panel,user%ixy,error)
    if (error) return
    call cubeset_panel_parse_xy(line,comm%nxy,user%nxy,error)
    if (error) return
    call cubeset_panel_parse_xy(line,comm%inter,user%inter,error)
    if (error) return
    call cubeset_panel_parse_xy(line,comm%size,user%size,error)
    if (error) return
    call parse_string2value(line,comm%aspect,1,strg_star,0.0,user%aspect,error)
    if (error) return
    call cubeset_panel_parse_corner(line,comm%fixed,comm%corner_arg,user%frame_fixed_corner,error)
    if (error) return
    call cubeset_panel_parse_position(line,comm%position,user%frame_kind,user%frame_pos,error)
    if (error) return
    !
    call comm%box%present(line,user%dobox,error)
    if (error) return
    !
  contains
    !
    subroutine parse_string2value(line,opt,iarg,defstring,defval,argval,error)
      use gkernel_interfaces, only: sic_math_real
      !-------------------------------------------------------------------
      !***JP: Remaining call from CUBE early days
      !-------------------------------------------------------------------
      character(len=*),  intent(in)    :: line
      type(option_t),    intent(in)    :: opt
      integer(kind=4),   intent(in)    :: iarg
      character(len=*),  intent(in)    :: defstring
      real(kind=real_k), intent(in)    :: defval
      real(kind=real_k), intent(out)   :: argval
      logical,           intent(inout) :: error
      !
      logical :: do
      character(len=argu_l) :: arg
      character(len=mess_l) :: mess
      !
      call opt%present(line,do,error)
      if (error) return
      if (do) then
         call cubetools_getarg(line,opt,iarg,arg,mandatory,error)
         if (error) return
         if (trim(arg).eq.defstring) then
            argval = defval
         else
            call sic_math_real(arg,argu_l,argval,error)
            if (error) then
               write(mess,'(3a)') 'Cannot convert ',arg,' to REAL' 
               call cubeset_message(seve%e,rname,mess)
               return
            endif
         endif
      endif
    end subroutine parse_string2value
  end subroutine cubeset_panel_parse
  !
  subroutine cubeset_panel_parse_xy(line,opt,xy,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    character(len=*),  intent(in)    :: line
    type(option_t),    intent(in)    :: opt
    real(kind=pane_k), intent(inout) :: xy(2)
    logical,           intent(inout) :: error
    !
    integer(kind=4), parameter :: iarg_x = 1
    integer(kind=4), parameter :: iarg_y = 2
    !
    if (opt%getnarg().gt.0) then
       call cubetools_getarg(line,opt,iarg_x,xy(code_x),mandatory,error)
       if (error) return
       xy(code_y) = 0.0
       call cubetools_getarg(line,opt,iarg_y,xy(code_y),.not.mandatory,error)
       if (error) return
    endif
  end subroutine cubeset_panel_parse_xy
  !
  subroutine cubeset_panel_parse_corner(line,opt,arg,code,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    character(len=*),     intent(in)    :: line
    type(option_t),       intent(in)    :: opt
    type(keywordlist_comm_t),  intent(in)    :: arg
    integer(kind=code_k), intent(inout) :: code
    logical,              intent(inout) :: error
    !
    character(len=argu_l) :: argu,solved
    logical :: present
    character(len=*), parameter :: rname='PANEL>PARSE>POSITION'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    call opt%present(line,present,error)
    if (error) return
    if (present) then
       argu = 'TRC'
       call cubetools_getarg(line,opt,1,argu,.not.mandatory,error)
       if (error) return
       call cubetools_keywordlist_user2prog(arg,argu,code,solved,error)
       if (error) return
    endif
  end subroutine cubeset_panel_parse_corner
  !
  subroutine cubeset_panel_parse_position(line,opt,kind,pos,error)
    use cubetools_disambiguate
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    character(len=*),     intent(in)    :: line
    type(option_t),       intent(in)    :: opt
    integer(kind=code_k), intent(inout) :: kind
    real(kind=pane_k),    intent(inout) :: pos(4)
    logical,              intent(inout) :: error
    !
    integer(kind=argu_k) :: iarg,narg
    character(len=argu_l) :: arg,key
    logical :: present
    character(len=*), parameter :: rname='PANEL>PARSE>POSITION'
    !
    call opt%present(line,present,error)
    if (error) return
    if (present) then
       narg = opt%getnarg()
       if (narg.eq.1) then
          call cubetools_getarg(line,opt,1,arg,mandatory,error)
          if (error) return
          call cubetools_disambiguate_strict(arg,frame_kinds,kind,key,error)
          if (error) return
       else if (narg.eq.4) then
          kind = code_frame_user
          do iarg=1,narg
             call cubetools_getarg(line,opt,iarg,pos(iarg),mandatory,error)
             if (error) return
          enddo ! iarg
       else
          call cubeset_message(seve%e,rname,'/POSITION takes 1 or 4 arguments')
          error = .true.
          return
       endif
    endif
  end subroutine cubeset_panel_parse_position
  !
  subroutine cubeset_panel_main(user,prog,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    type(panel_user_t), intent(in)    :: user
    type(panel_prog_t), intent(inout) :: prog
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: rname='PANEL>MAIN'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    call cubeset_panel_user2request(user,prog%request,error)
    if (error) return
    if (prog%request%dolist) then
       call cubeset_panel_list_request(prog%request,error)
       if (error) return
    else
       call cubeset_panel_characteristics(prog%request,prog%frame,prog%current,error)
       if (error) return
       call cubeset_panel_position(prog%request,prog%frame,prog%current,error)
       if (error) return
       call cubeset_panel_resize(prog%request,prog%current,error)
       if (error) return
       call cubeset_panel_to_viewport(prog%request%page_aspect,prog%current,error)
       if (error) return
       if (prog%request%dobox) then
          call cubeset_panel_drawbox(error)
          if (error) return
       endif
    endif
  end subroutine cubeset_panel_main
  !
  subroutine cubeset_panel_drawbox(error)
    use gkernel_interfaces
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    character(len=commandline_length) :: command
    character(len=*), parameter :: rname='PANEL>DRAWBOX'
    !
    command = 'box n n n'
    call gr_exec1(command)
    error = gr_error()
    if (error) return
  end subroutine cubeset_panel_drawbox
  !
  subroutine cubeset_panel_user2request(user,request,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    type(panel_user_t), intent(in)    :: user
    type(panel_user_t), intent(inout) :: request
    logical,            intent(inout) :: error
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='PANEL>USER2REQUEST'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    request%dolist = user%dolist
    request%dobox = user%dobox
    ! Space between panels
    if (user%inter(code_y).eq.0.0) then
       request%inter(code_x) = user%inter(code_x)
       request%inter(code_y) = user%inter(code_x)
    else
       request%inter = user%inter
    endif
    ! Panel size 
    if (user%size(code_y).eq.0.0) then
       request%size(code_x) = user%size(code_x)
       request%size(code_y) = user%size(code_x)
    else
       request%size = user%size
    endif
    ! Page info
    request%page_geo = user%page_geo
    request%page_size = user%page_size
    request%page_aspect = user%page_aspect
    ! Aspect ratio
    request%aspect = user%aspect
    ! Frame properties
    request%frame_fixed_corner = user%frame_fixed_corner
    request%frame_kind = user%frame_kind
    if (request%frame_kind.eq.code_frame_user) then
       request%frame_pos = user%frame_pos
    else
       request%frame_kind = user%frame_kind
       request%frame_pos = frame_positions(:,request%frame_kind,request%page_geo)
    endif
    ! NXY
    if (user%nxy(code_y).eq.0.0) then
       call cubeset_panel_nxybox(nint(user%nxy(code_x)),request,error)
       if (error) return
    else
       request%nxy = user%nxy
    endif
    if (any(request%nxy.le.0.0)) then
       write(mess,'(a,f5.1,a,f5.1,a)') 'Can not create grid of panels of shape: (',&
            request%nxy(1),',',request%nxy(2),')'
       call cubeset_message(seve%e,rname,mess)
       error = .true.
       return
    endif
    ! IXY
    if (user%ixy(code_y).eq.0.0) then
       ! user%ixy(1) is the linear window number.
       ! In this case, origin is on the top, left corner.
       request%ixy(code_y) = int((user%ixy(code_x)-1)/request%nxy(code_x))+1
       request%ixy(code_x) = user%ixy(code_x)-(request%ixy(code_y)-1)*request%nxy(code_x)
       request%ixy(code_y) = (request%nxy(code_y)+1)-request%ixy(code_y)
    else
       ! user%ixy is the window number on the x and y axes.
       ! In this case, axis origin is on the bottom, left corner.
       request%ixy = user%ixy
    endif
  end subroutine cubeset_panel_user2request
  !
  subroutine cubeset_panel_nxybox(nboxes,request,error)
    !-------------------------------------------------------------------
    ! Re-creation of l-nxybox.cube
    !-------------------------------------------------------------------
    integer(kind=4),    intent(in)    :: nboxes
    type(panel_user_t), intent(inout) :: request
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: rname='PANEL>NXYBOX'
    real(kind=pane_k) :: locaspect
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    ! VVV is this the correct solution?
    if (request%aspect.eq.0.0) then
       locaspect = 1.0
    else
       locaspect = request%aspect
    endif
    !
    if (locaspect.gt.1.0) then
       request%nxy(code_y) = int(sqrt(nboxes*locaspect+1))
       request%nxy(code_x) = max(1,nint(request%nxy(code_y)/locaspect))
       if (request%nxy(code_x)*request%nxy(code_y).lt.nboxes) then
          request%nxy(code_y) = request%nxy(code_y)+1
          if (request%nxy(code_x)*request%nxy(code_y).lt.nboxes) then
             request%nxy(code_x) = request%nxy(code_x)+1
          endif
       else if ((request%nxy(code_y)-1)*request%nxy(code_x).eq.nboxes) then
          request%nxy(code_y) = request%nxy(code_y)-1
       endif
    else
       request%nxy(code_x) = int(sqrt(nboxes/locaspect+1))
       request%nxy(code_y) = max(1,nint(request%nxy(code_x)*locaspect))
       if (request%nxy(code_y)*request%nxy(code_x).lt.nboxes) then
          request%nxy(code_x) = request%nxy(code_x)+1
          if (request%nxy(code_y)*request%nxy(code_x).lt.nboxes) then
             request%nxy(code_y) = request%nxy(code_y)+1
          endif
       else if ((request%nxy(code_x)-1)*request%nxy(code_y).eq.nboxes) then
          request%nxy(code_x) = request%nxy(code_x)-1
       endif
    endif
  end subroutine cubeset_panel_nxybox
  !
  subroutine cubeset_panel_characteristics(request,frame,current,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    type(panel_user_t),  intent(inout) :: request
    type(panel_frame_t), intent(inout) :: frame
    type(panel_panel_t), intent(inout) :: current
    logical,             intent(inout) :: error
    !
    real(kind=pane_k) :: fullsize(2),fullaspect,corrfactor
    real(kind=pane_k) :: mynxy(2)
    character(len=*), parameter :: rname='PANEL>SIZE>BLC'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    frame%pos = reshape(request%frame_pos,[2,2])
    frame%size = frame%pos(2,:)-frame%pos(1,:)
    ! Take care of request%inter
    if (any(request%inter.ne.0.0)) then
       ! White inter-spaces are asked
       ! => Just need to redefine the number of panels
       mynxy = request%nxy*(1.0+request%inter)-request%inter
    else
       ! Just use user request
       mynxy = request%nxy
    endif
    ! Take care of aspect ratio
    if (request%aspect.ne.0) then
       ! The margin defining the plotting region are defined through the
       ! frame%pos variable. But when we want to respect the panel aspect
       ! ratio, we have to enlarge either the x or y one depending on this
       ! ratio.
       if (request%aspect.le.0.0) then
          call cubeset_message(seve%e,rname,'Negative request%aspect ratio!')
          error = .true.
          return
       endif
       ! Compute the panel size when using all the available plotting area
       fullsize = frame%size/mynxy
       ! Compute the corresponding aspect ratio
       fullaspect = fullsize(code_x)/fullsize(code_y)
       ! Compute the needed correction factor
       corrfactor = request%aspect*mynxy(code_x)/mynxy(code_y)
       if (fullaspect.gt.request%aspect) then
          ! Increase the x margin
          frame%mar(code_x) = 0.5*(frame%size(code_x)-frame%size(code_y)*corrfactor)
          frame%mar(code_y) = 0.0
       elseif (fullaspect.lt.request%aspect) then
          ! Increase the y margin
          frame%mar(code_x) = 0.0
          frame%mar(code_y) = 0.5*(frame%size(code_y)-frame%size(code_x)/corrfactor)
       endif
    else
       frame%mar = 0.0
    endif
    ! Computes the panel size in fraction of smallest page size
    current%size = ((frame%size-2.0*frame%mar)/mynxy)
    current%aspect = current%size(1)/current%size(2)
    ! Fix one of the four corners when asked
    select case (request%frame_fixed_corner)
    case (code_fixed_none)
       ! Does nothing
    case (code_fixed_trc)
       frame%mar = 2.0*frame%mar
    case (code_fixed_tlc)
       frame%mar(code_x) = 0
       frame%mar(code_y) = 2.0*frame%mar(code_y)     
    case (code_fixed_brc)
       frame%mar(code_x) = 2.0*frame%mar(code_x)
       frame%mar(code_y) = 0
    case (code_fixed_blc)
       frame%mar = 0
    case default
       call cubeset_message(seve%e,rname,'Unknown frame_fixed_corner code')
       error = .true.
       return
    end select
    frame%blc = frame%pos(1,:)+frame%mar
  end subroutine cubeset_panel_characteristics
  !
  subroutine cubeset_panel_position(request,frame,current,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    type(panel_user_t),  intent(in)    :: request
    type(panel_frame_t), intent(in)    :: frame
    type(panel_panel_t), intent(inout) :: current
    logical,             intent(inout) :: error
    !
    real(kind=pane_k) :: ixy(2)
    character(len=*), parameter :: rname='PANEL>POSITION'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    ! Take care of request%inter
    if (any(request%inter.ne.0.0)) then
       ! White inter-spaces are asked
       ! => Just need to redefine the panel number
       ixy = (request%ixy*(1.0+request%inter)-request%inter)
    else
       ixy = request%ixy
    endif
    ! Compute the panel position in fraction of smallest page size
    current%min = (frame%blc+(ixy-1)*current%size)
  end subroutine cubeset_panel_position
  !
  subroutine cubeset_panel_resize(request,current,error)
    !-------------------------------------------------------------------
    ! Resize the current panel size set to the size of one rectangle grid
    ! according to the request%size factor
    !-------------------------------------------------------------------
    type(panel_user_t),  intent(in)    :: request
    type(panel_panel_t), intent(inout) :: current
    logical,             intent(inout) :: error
    !
    real(kind=pane_k) :: ixy(2)
    character(len=*), parameter :: rname='PANEL>RESIZE'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    current%size = current%size*request%size
    current%max = current%min+current%size
  end subroutine cubeset_panel_resize
  !
  subroutine cubeset_panel_to_viewport(page_aspect,current,error)
    use gkernel_interfaces
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    real(kind=pane_k),   intent(in)    :: page_aspect
    type(panel_panel_t), intent(in)    :: current
    logical,             intent(inout) :: error
    !
    real(kind=pane_k) :: min(2),max(2)
    character(len=commandline_length) :: command
    character(len=*), parameter :: rname='PANEL>TO>VIEWPORT'
    !
    character(len=mess_l) :: mess
    !
    call cubeset_panel_pagefraction2normal(page_aspect,current%min,min)
    call cubeset_panel_pagefraction2normal(page_aspect,current%max,max)
    if (any(min.lt.0.0).or.any(max.gt.1.0)) then
       write(mess,'(a,4f5.1)') 'Desired viewport is set beyond bounds: ',&
            min(code_x),max(code_x),min(code_y),max(code_y)
       call cubeset_message(seve%e,rname,mess)
       call cubeset_message(seve%e,rname,'Limits are: 0.0 1.0 0.0 1.0')
       error = .true.
       return
    endif
    write(command,'(A,4(1X,1PG14.7))') &
         'SET VIEWPORT',&
         min(code_x),max(code_x),&
         min(code_y),max(code_y)
    call gr_exec1(command)
    error = gr_error()
    if (error) return
    !say 'Prog%Aspect ratio: ''(box_xmax-box_xmin)/(box_ymax-box_ymin)'
  end subroutine cubeset_panel_to_viewport
  !
  subroutine cubeset_panel_fetch_pagegeometry(page_size,page_aspect,code_geo,error)
    !-------------------------------------------------------------------
    ! Fetch page geometry from GREG
    !-------------------------------------------------------------------
    real(kind=pane_k),intent(out)   :: page_size(2)
    real(kind=pane_k),intent(out)   :: page_aspect
    integer(kind=4)  ,intent(out)   :: code_geo
    logical          ,intent(inout) :: error
    ! GILDAS A4 proportions
    real(kind=pane_k), parameter :: ga4landscape = 1.42857146
    real(kind=pane_k), parameter :: ga4portrait  = 0.699999988
    character(len=*), parameter :: rname='PANEL>FETCH>GEOMETRY'
    character(len=mess_l) :: mess
    ! 
    call gr_get_physical(page_size(code_x),page_size(code_y))
    !
    page_aspect = page_size(code_x)/page_size(code_y)
    !
    if (page_aspect.le.0) then
       call cubeset_message(seve%e,rname,' Page Aspect is zero or negative')
       error = .true.
       return
    else if (page_aspect.gt.1) then
       code_geo = code_page_landscape
    else if (page_aspect.lt.1) then
       code_geo = code_page_portrait
    else if (page_aspect.eq.1) then
       code_geo = code_page_square
    else ! page_aspect .eq. NAN +-inf
       write(mess,'(a,f8.3,a,f8.3)') 'Nonsense page size: ',page_size(code_x),'x',page_size(code_y)
       call cubeset_message(seve%e,rname,mess)
       error = .true.
       return
    endif
    !
    if (page_aspect.ne.ga4landscape.and.page_aspect.ne.ga4portrait) then
       write(mess,'(a,f8.3,a,f8.3,a)') 'Page size: ',page_size(code_x),'x',page_size(code_y),&
            ' is not 21x30 or 30x21, plots may set out of bounds'
       call cubeset_message(seve%w,rname,mess)
    endif
  end subroutine cubeset_panel_fetch_pagegeometry
  !
  subroutine cubeset_panel_pagefraction2normal(page_aspect,in,ou)
    !-------------------------------------------------------------------
    ! From fraction of smallest page size to normal (ie, between 0 and 1)
    ! coordinates
    !-------------------------------------------------------------------
    real(kind=pane_k), intent(in)  :: page_aspect
    real(kind=pane_k), intent(in)  :: in(2)
    real(kind=pane_k), intent(out) :: ou(2)
    !
    if (page_aspect.gt.1.0) then
       ou(code_x) = in(code_x)/page_aspect
       ou(code_y) = in(code_y)
    else
       ou(code_x) = in(code_x)
       ou(code_y) = in(code_y)*page_aspect
    endif
  end subroutine cubeset_panel_pagefraction2normal
  !
  subroutine cubeset_panel_list_request(request,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    type(panel_user_t), intent(inout) :: request
    logical,            intent(inout) :: error
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='PANEL>LIST>REQUEST'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    call cubeset_message(seve%r,rname,blankstr)
    write(mess,'(a26,1x,a,f4.1,a,f4.1,a,f4.1,a,f4.1,a)') &
         'Panel:','(',request%ixy(code_x),' x ',request%ixy(code_y), &
         ') in (',request%nxy(code_x),' x ',request%nxy(code_y),')'
    call cubeset_message(seve%r,rname,mess)
    write(mess,'(a26,1x,a,f4.1,a,f4.1,a)') &
         'Panel size:','(',request%size(code_x),' x ',request%size(code_y),&
         ') relative to size of one grid rectangle'
    call cubeset_message(seve%r,rname,mess)
    write(mess,'(a26,1x,a,f4.1,a,f4.1,a)') &
         'Space between panels:','(',request%inter(code_x),' x ',request%inter(code_y),&
         ') times the size of one grid rectangle'
    call cubeset_message(seve%r,rname,mess)
    write(mess,'(a26,2x,f4.1,9x,a)') &
         'Aspect ratio:',request%aspect,'! 0.0 => Not taken into account'
    call cubeset_message(seve%r,rname,mess)
    write(mess,'(a26,1x,a,f5.3,a,f5.3,a,f5.3,a,f5.3,a)') &
         'Global frame:','(',&
         request%frame_pos(1),' - ',request%frame_pos(2),') x (', &
         request%frame_pos(3),' - ',request%frame_pos(4),')'
    call cubeset_message(seve%r,rname,mess)
  end subroutine cubeset_panel_list_request
  !
  subroutine cubeset_panel_sicdef(error)
    use cubetools_progstruct_types
    use cubeset_buffer
    !-------------------------------------------------------------------
    ! Create the CUBESET%PANEL readonly SIC structure
    !-------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    type(progstruct_t) :: panel
    character(len=*), parameter :: rname='PANEL>SICDEF'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    call cubeset%recreate('panel',panel,error)
    if (error) return
    call cubeset_panel_do_sicdef(panel,prog%request,error)
    if (error) return
    call cubeset_panel_true_sicdef(panel,prog%current,error)
    if (error) return
  end subroutine cubeset_panel_sicdef
  !
  subroutine cubeset_panel_do_sicdef(panel,request,error)
    use gkernel_interfaces
    use cubetools_progstruct_types
    !-------------------------------------------------------------------
    ! Create the PANEL%DO readonly SIC structure
    !-------------------------------------------------------------------
    type(progstruct_t), intent(in)    :: panel
    type(panel_user_t), intent(in)    :: request
    logical,            intent(inout) :: error
    !
    type(progstruct_t) :: do
    character(len=*), parameter :: rname='PANEL>DO>SICDEF'
    !
    call panel%recreate('do',do,error)
    if (error) return
    call sic_def_logi(trim(do%name)//'list',request%dolist,readonly,error)
    if (error) return
    call sic_def_logi(trim(do%name)//'box',request%dobox,readonly,error)
    if (error) return
    call sic_def_inte(trim(do%name)//'page_geo',request%page_geo,0,0,readonly,error)
    if (error) return
    call sic_def_inte(trim(do%name)//'frame_fixed_corner',request%frame_fixed_corner,0,0,readonly,error)
    if (error) return
    call sic_def_inte(trim(do%name)//'frame_kind',request%frame_kind,0,0,readonly,error)
    if (error) return
    call sic_def_real(trim(do%name)//'frame_pos',request%frame_pos,1,4,readonly,error)
    if (error) return
    call sic_def_real(trim(do%name)//'ixy',request%ixy,1,2,readonly,error)
    if (error) return
    call sic_def_real(trim(do%name)//'nxy',request%nxy,1,2,readonly,error)
    if (error) return
    call sic_def_real(trim(do%name)//'inter',request%inter,1,2,readonly,error)
    if (error) return
    call sic_def_real(trim(do%name)//'aspect',request%aspect,0,0,readonly,error)
    if (error) return
  end subroutine cubeset_panel_do_sicdef
  !
  subroutine cubeset_panel_true_sicdef(panel,current,error)
    use gkernel_interfaces
    use cubetools_progstruct_types
    !-------------------------------------------------------------------
    ! Create the PANEL%TRUE readonly SIC structure
    !-------------------------------------------------------------------
    type(progstruct_t),  intent(in)    :: panel
    type(panel_panel_t), intent(in)    :: current
    logical,             intent(inout) :: error
    !
    type(progstruct_t) :: true
    character(len=*), parameter :: rname='PANEL>TRUE>SICDEF'
    !
    call panel%recreate('true',true,error)
    if (error) return
    call sic_def_real(trim(true%name)//'min',current%min,1,2,readonly,error)
    if (error) return
    call sic_def_real(trim(true%name)//'max',current%max,1,2,readonly,error)
    if (error) return
    call sic_def_real(trim(true%name)//'size',current%size,1,2,readonly,error)
    if (error) return
    call sic_def_real(trim(true%name)//'aspect',current%aspect,0,0,readonly,error)
    if (error) return
    call cubeset_panel_frame_sicdef(true,prog%frame,error)
    if (error) return
  end subroutine cubeset_panel_true_sicdef
  !
  subroutine cubeset_panel_frame_sicdef(true,progframe,error)
    use gkernel_interfaces
    use cubetools_progstruct_types
    !-------------------------------------------------------------------
    ! Create the TRUE%FRAME readonly SIC structure
    !-------------------------------------------------------------------
    type(progstruct_t),  intent(in)    :: true    
    type(panel_frame_t), intent(in)    :: progframe
    logical,             intent(inout) :: error
    !
    integer(kind=8) :: dims(2)
    type(progstruct_t) :: frame
    character(len=*), parameter :: rname='PANEL>FRAME>SICDEF'
    !
    call true%recreate('frame',frame,error)
    if (error) return
    !
    dims = 2
    call sic_def_real(trim(frame%name)//'pos',progframe%pos,2,dims,readonly,error)
    if (error) return
    call sic_def_real(trim(frame%name)//'mar',progframe%mar,1,2,readonly,error)
    if (error) return
    call sic_def_real(trim(frame%name)//'blc',progframe%blc,1,2,readonly,error)
    if (error) return
    call sic_def_real(trim(frame%name)//'size',progframe%size,1,2,readonly,error)
    if (error) return
    call sic_def_logi(trim(frame%name)//'landscape',progframe%landscape,readonly,error)
    if (error) return
  end subroutine cubeset_panel_frame_sicdef
end module cubeset_panel
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
