!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage CUBE SET messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeset_messaging
  use gpack_def
  use gbl_message
  use cubetools_parameters
  !
  public :: setseve,seve,mess_l
  public :: cubeset_message_set_id,cubeset_message
  public :: cubeset_message_get_trace,cubeset_message_set_trace
  public :: cubeset_message_get_others,cubeset_message_set_others
  private
  !
  ! Identifier used for message identification
  integer(kind=4) :: cubeset_message_id = gpack_global_id  ! Default value for startup message
  !
  type :: cubeset_messaging_debug_t
     integer(kind=code_k) :: trace = seve%t
     integer(kind=code_k) :: others = seve%d
  end type cubeset_messaging_debug_t
  !
  type(cubeset_messaging_debug_t) :: setseve
  !
contains
  !
  subroutine cubeset_message_set_id(id)
    !---------------------------------------------------------------------
    ! Alter library id into input id. Should be called by the library
    ! which wants to share its id with the current one.
    !---------------------------------------------------------------------
    integer(kind=4), intent(in) :: id
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='MESSAGE>SET>ID'
    !
    cubeset_message_id = id
    write (mess,'(A,I0)') 'Now use id #',cubeset_message_id
    call cubeset_message(seve%d,rname,mess)
  end subroutine cubeset_message_set_id
  !
  subroutine cubeset_message(mkind,procname,message)
    use cubetools_cmessaging
    !---------------------------------------------------------------------
    ! Messaging facility for the current library. Calls the low-level
    ! (internal) messaging routine with its own identifier.
    !---------------------------------------------------------------------
    integer(kind=4),  intent(in) :: mkind     ! Message kind
    character(len=*), intent(in) :: procname  ! Name of calling procedure
    character(len=*), intent(in) :: message   ! Message string
    !
    call cubetools_cmessage(cubeset_message_id,mkind,'SET>'//procname,message)
  end subroutine cubeset_message
  !
  subroutine cubeset_message_set_trace(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       setseve%trace = seve%i
    else
       setseve%trace = seve%t
    endif
  end subroutine cubeset_message_set_trace
  !
  subroutine cubeset_message_set_others(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       setseve%others = seve%i
    else
       setseve%others = seve%d
    endif
  end subroutine cubeset_message_set_others
  !
  function cubeset_message_get_trace()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubeset_message_get_trace
    !
    cubeset_message_get_trace = setseve%trace.eq.seve%i
    !
  end function cubeset_message_get_trace
  !
  function cubeset_message_get_others()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubeset_message_get_others
    !
    cubeset_message_get_others = setseve%others.eq.seve%i
    !
  end function cubeset_message_get_others
end module cubeset_messaging
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
