!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeset_axset
  use cubetools_structure
  use cubetools_switch_types
  use cubeset_messaging
  !
  public :: axset
  private
  !
  type :: axset_comm_t
     type(option_t), pointer :: comm
     type(switch_comm_t) :: ignoredegenerate
   contains
     procedure, public  :: register => cubeset_axset_register
     procedure, private :: parse    => cubeset_axset_parse
     procedure, private :: main     => cubeset_axset_main
  end type axset_comm_t
  type(axset_comm_t) :: axset
  !
  type :: axset_user_t
     type(switch_user_t) :: ignoredegenerate
   contains
     procedure, private :: toprog => cubeset_axset_user_toprog
  end type axset_user_t
  !
  type :: axset_prog_t
     type(switch_prog_t) :: ignoredegenerate
   contains
     procedure, private :: act => cubeset_axset_prog_act
  end type axset_prog_t
  !
contains
  !
  subroutine cubeset_axset_command(line,error)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(axset_user_t) :: user
    character(len=*), parameter :: rname='AXSET>COMMAND'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    call axset%parse(line,user,error)
    if (error) return
    call axset%main(user,error)
    if (error) return
  end subroutine cubeset_axset_command
  !
  !-----------------------------------------------------------------------
  !
  subroutine cubeset_axset_register(axset,error)
    use cubeadm_setup
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    class(axset_comm_t), intent(inout) :: axset
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: comm_abstract = &
         'Set how axes set are treated when reading'
    character(len=*), parameter :: comm_help = &
         'The behaviour set by this command is the global behaviour&
         & for CUBE. If the user desires to keep degenerate axes by&
         & default this should be set to off. Degenerate axes can be&
         & deactivated on a per cube basis with the CUBE\MODIFY command'
    character(len=*), parameter :: rname='AXSET>REGISTER'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'AXSET','',&
         comm_abstract,&
         comm_help,&
         cubeset_axset_command,&
         axset%comm,error)
    if (error) return
    !
    call axset%ignoredegenerate%register(&
         'IGNOREDEGENERAT','ignoring degenerate axes','ON',error)
    if (error) return
  end subroutine cubeset_axset_register 
  !
  subroutine cubeset_axset_parse(comm,line,user,error)
    !---------------------------------------------------------------------
    ! AXSET /IGNOREDEGENERATE ON|OFF
    !---------------------------------------------------------------------
    class(axset_comm_t), intent(inout) :: comm
    character(len=*),    intent(in)    :: line
    type(axset_user_t),  intent(inout) :: user
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='AXSET>PARSE'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    call comm%ignoredegenerate%parse(line,user%ignoredegenerate,error)
    if (error) return
  end subroutine cubeset_axset_parse
  !
  subroutine cubeset_axset_main(comm,user,error)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(axset_comm_t), intent(in)    :: comm
    type(axset_user_t),  intent(inout) :: user
    logical,             intent(inout) :: error
    !
    type(axset_prog_t) :: prog
    character(len=*), parameter :: rname='AXSET>COMMAND'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%act(error)
    if (error) return
  end subroutine cubeset_axset_main
  !
  !-----------------------------------------------------------------------
  !
  subroutine cubeset_axset_user_toprog(user,comm,prog,error)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(axset_user_t), intent(in)    :: user
    type(axset_comm_t),  intent(in)    :: comm
    type(axset_prog_t),  intent(inout) :: prog
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='AXSET>COMMAND'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    call user%ignoredegenerate%toprog(comm%ignoredegenerate,prog%ignoredegenerate,error)
    if (error) return
  end subroutine cubeset_axset_user_toprog
  !
  !-----------------------------------------------------------------------
  !
  subroutine cubeset_axset_prog_act(prog,error)
    use cubetools_axset_types
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(axset_prog_t), intent(inout) :: prog
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='AXSET>PROG>ACT'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    if (prog%ignoredegenerate%act) then
       call cubetools_axset_set_ignore_degenerate(prog%ignoredegenerate%enabled,error)
       if (error) return
    else
       call cubeset_message(seve%r,rname,'  Axset')
       prog%ignoredegenerate%action = 'IGNOREDEGENERAT'
       prog%ignoredegenerate%enabled = cubetools_axset_get_ignore_degenerate()
       call prog%ignoredegenerate%list(error)
       if (error) return
    endif
  end subroutine cubeset_axset_prog_act
end module cubeset_axset
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
