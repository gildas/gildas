!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeset_colors
  use cubetools_structure
  use cubetools_consistency_colors
  use cubeset_messaging
  !
  public :: colorcomm
  private
  !
  type :: colors_comm_t
     type(option_t), pointer :: comm
     type(color_opt_t)       :: inconsistent
     type(color_opt_t)       :: consistent
     type(color_opt_t)       :: notchecked
     type(option_t), pointer :: defaults
   contains
     procedure, public  :: register => cubeset_colors_register
     procedure, public  :: parse    => cubeset_colors_parse
     procedure, public  :: main     => cubeset_colors_main
  end type colors_comm_t
  type(colors_comm_t) :: colorcomm
  !
  type :: colors_user_t
     logical :: list
     logical :: dodefaults
  end type colors_user_t
  !
contains
  !
  subroutine cubeset_colors_command(line,error)
    !---------------------------------------------------------------------
    ! Support routine for command
    ! COLORS Mode
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(colors_user_t) :: user
    character(len=*), parameter :: rname='COLORS>COMMAND'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    call colorcomm%parse(line,user,error)
    if (error) return
    call colorcomm%main(user,error)
    if (error) return
  end subroutine cubeset_colors_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubeset_colors_register(colorcomm,error)
    use cubetools_parameters
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    class(colors_comm_t), intent(inout) :: colorcomm
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: comm_abstract = &
         'Set colors to be used in consistency prints'
    character(len=*), parameter :: comm_help = &
         'List the current consistency colors when called without arguments'
    character(len=*), parameter :: rname='COLORS>REGISTER'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'COLORS','[topic color]',&
         comm_abstract,&
         comm_help,&
         cubeset_colors_command,&
         colorcomm%comm,error)
    if (error) return
    !
    call colorcomm%inconsistent%register('INCONSISTENT',&
         'Set color for inconsistent fields',error)
    if (error) return
    call colorcomm%consistent%register('CONSISTENT',&
         'Set color for consistent fields',error)
    if (error) return
    call colorcomm%notchecked%register('NOTCHECKED',&
         'Set color for fields that are not checked',error)
    if (error) return
    !
    call cubetools_register_option(&
         'DEFAULTS','',&
         'Reset color defaults',&
         strg_id,&
         colorcomm%defaults,error)
    if (error) return
  end subroutine cubeset_colors_register
  !
  subroutine cubeset_colors_parse(colorcomm,line,user,error)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(colors_comm_t), intent(inout) :: colorcomm
    character(len=*),     intent(in)    :: line
    type(colors_user_t),  intent(out)   :: user
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='COLORS>PARSE'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    user%list = cubetools_nopt().eq.0
    !
    if (user%list) then
       ! nothing to do
    else
       call colorcomm%inconsistent%parse(line,error)
       if (error) return
       call colorcomm%consistent%parse(line,error)
       if (error) return
       call colorcomm%notchecked%parse(line,error)
       if (error) return
       call colorcomm%defaults%present(line,user%dodefaults,error)
       if (error) return
    endif
    !
  end subroutine cubeset_colors_parse
  !
  subroutine cubeset_colors_main(colorcomm,user,error)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(colors_comm_t), intent(inout) :: colorcomm
    type(colors_user_t),  intent(in)    :: user
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='COLORS>MAIN'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    if (user%list) then
       call colors%list(error)
       if (error) return
    else
       if (user%dodefaults) then
          call colors%reset_defaults(error)
          if (error) return
       endif
       !
       if (colorcomm%inconsistent%do) then
          call colors%set_inconsistent(colorcomm%inconsistent%icolor,error)
          if (error) return
       endif
       if (colorcomm%consistent%do) then
          call colors%set_consistent(colorcomm%consistent%icolor,error)
          if (error) return
       endif
       if (colorcomm%notchecked%do) then
          call colors%set_notchecked(colorcomm%notchecked%icolor,error)
          if (error) return
       endif
    endif
  end subroutine cubeset_colors_main
end module cubeset_colors
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
