module cubeset_index
  use cubetools_structure
  use cubetools_keywordlist_types
  use cubetools_setup_types
  use cubeset_messaging
  !
  public :: index
  private
  !
  type :: index_comm_t
     type(option_t),      pointer :: comm
     type(keywordlist_comm_t), pointer :: index_arg
   contains
     procedure, public  :: register => cubeset_index_register
     procedure, private :: parse    => cubeset_index_parse
     procedure, private :: print    => cubeset_index_print
  end type index_comm_t
  type(index_comm_t) :: index
  !
contains
  !
  subroutine cubeset_index_command(line,error)
    !---------------------------------------------------------------------
    ! Support routine for command
    ! INDEX INPUT|CURRENT
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='INDEX>COMMAND'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    if (index%comm%getnarg().eq.0) then
       call index%print(error)
       return
    endif
    call index%parse(line,error)
    if (error) return
  end subroutine cubeset_index_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubeset_index_register(index,error)
    use cubeadm_setup
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    class(index_comm_t), intent(inout) :: index
    logical,             intent(inout) :: error
    !
    type(keywordlist_comm_t) :: keyarg
    character(len=*), parameter :: comm_abstract = &
         'Set which index should be used when searching for cubes'
    character(len=*), parameter :: comm_help = &
         'No argument shows current value. Default is DAG'
    character(len=*), parameter :: rname='INDEX>REGISTER'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'INDEX','[INPUT|CURRENT]',&
         comm_abstract,&
         comm_help,&
         cubeset_index_command,&
         index%comm,error)
    if (error) return
    call keyarg%register( &
         'name',  &
         'Name of the index to be used', &
         strg_id,&
         code_arg_optional, &
         indexname, &
         .not.flexible, &
         index%index_arg, &
         error)
    if (error) return
  end subroutine cubeset_index_register
  !
  subroutine cubeset_index_parse(index,line,error)
    use cubeadm_setup
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(index_comm_t), intent(in)    :: index
    character(len=*),    intent(in)    :: line
    logical,             intent(inout) :: error
    !
    integer(kind=4) :: ikey
    character(len=16) :: keyword,argum
    character(len=*), parameter :: rname='INDEX>PARSE'
    !
    call cubeset_message(setseve%trace,rname,'Welcome')
    call cubetools_getarg(line,index%comm,1,argum,mandatory,error)
    if (error) return
    call cubetools_keywordlist_user2prog(index%index_arg,argum,ikey,keyword,error)
    if (error) return
    cubset%index%default = ikey
  end subroutine cubeset_index_parse
  !
  subroutine cubeset_index_print(index,error)
    use cubeadm_setup
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(index_comm_t), intent(in)    :: index
    logical,             intent(inout) :: error
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='INDEX>PRINT'
    !
    call cubeset_message(seve%r,rname,'  Index')
    write(mess,'(a,a)') '    Default: ',indexname(cubset%index%default)
    call cubeset_message(seve%r,rname,mess)
  end subroutine cubeset_index_print
end module cubeset_index
