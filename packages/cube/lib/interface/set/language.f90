!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeset_language
  use cubetools_structure
  use cubeset_messaging
  use cubeset_axset
  use cubeset_blanking
  use cubeset_buffering
  use cubeset_colors
  use cubeset_index
  use cubeset_output
  use cubeset_panel
  use cubeset_scaling
  use cubeset_spatial
  use cubeset_spectral
  use cubeset_timing
  use cubeset_units
  !
  public :: cubeset_register_language
  private
  !
  integer(kind=lang_k) :: langid
  !
contains
  !
  subroutine cubeset_register_language(error)
    !----------------------------------------------------------------------
    ! Register the SET\ language
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    call cubetools_register_language('SET',&
         'J.Pety, S.Bardeau, V.deSouzaMagalhaes',&
         'Commands that set global behaviors of the program',&
         'gag_doc:hlp/cube-help-set.hlp',&
         cubeset_execute_command,langid,error)
    if (error) return
    !
    call axset%register(error)
    if (error) return
    call blanking%register(error)
    if (error) return
    !
    ! These two commands are registered with name+comm because of
    ! buffers used by these commands that would raise name conflicts
    call bufferingcomm%register(error)
    if (error) return
    call colorcomm%register(error)
    if (error) return
    call index%register(error)
    if (error) return
    call output%register(error)
    if (error) return
    call cubeset_panel_register(error)
    if (error) return
    call scaling%register(error)
    if (error) return
    call spatial%register(error)
    if (error) return
    call spectral%register(error)
    if (error) return
    call timing%register(error)
    if (error) return
    call units%register(error)
    if (error) return
    !
    call cubetools_register_dict(error)
    if (error) return
  end subroutine cubeset_register_language
  !
  subroutine cubeset_execute_command(line,comm,error)
    !----------------------------------------------------------------------
    ! Execute a command of the SET\ language
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    character(len=*), intent(in)    :: comm
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='EXECUTE>COMMAND'
    !
    error = .false.
    if (comm.eq.'SET?') then
       call cubetools_list_language_commands(langid,error)
       if (error) return
    else
       call cubetools_execute_command(line,langid,comm,error)
       if (error) return
       ! There should be no call to cubeadm_finish_all here
    endif
  end subroutine cubeset_execute_command
end module cubeset_language
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
