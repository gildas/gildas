module cubeedit_entry
  use cubetools_parameters
  use cubetools_structure
  use cubetools_keywordlist_types
  use cubeedit_messaging
  !
  public :: entry_opt_t,entry_user_t
  private
  !
  type entry_opt_t
     type(option_t),      pointer :: opt
     type(keywordlist_comm_t), pointer :: access
     logical                      :: fixed
   contains
     procedure :: register  => cubeedit_entry_register
     procedure :: parse     => cubeedit_entry_parse
     procedure :: user2prog => cubeedit_entry_user2prog
  end type entry_opt_t
  !
  type entry_user_t
     integer(kind=entr_k)  :: ientry
     character(len=argu_l) :: access
     logical               :: do
  end type entry_user_t
  !
contains
  subroutine cubeedit_entry_register(entry,abstract,fixed,error)
    !----------------------------------------------------------------------
    ! Register a /ENTRY options
    !----------------------------------------------------------------------
    class(entry_opt_t), intent(out)   :: entry
    character(len=*),   intent(in)    :: abstract
    logical,            intent(in)    :: fixed
    logical,            intent(inout) :: error
    !
    type(standard_arg_t) :: stdarg
    type(keywordlist_comm_t) :: keyarg
    character(len=64) :: help,syntax
    character(len=*), parameter :: accesses(2) = ['IMAGE   ','SPECTRUM']
    character(len=*), parameter :: rname='ENTRY>REGISTER'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    entry%fixed = fixed
    if (fixed) then
       syntax = 'ientry'
       help   = 'Access is fixed when opening the cube for editing'
    else
       help   = 'Default access is IMAGE'
       syntax = 'ientry [access]'
    endif
    call cubetools_register_option(&
         'ENTRY',syntax,&
         abstract,&
         help,&
         entry%opt,error)
    if (error) return
    call stdarg%register(&
         'ientry',&
         'Entry number to be gotten',&
         strg_id,&
         code_arg_mandatory,error)
    if (error) return
    if (.not.fixed) then
       call keyarg%register( &
            'Access',  &
            'In which order the cube should be accessed', &
            strg_id, &
            code_arg_optional, &
            accesses, &
            .not.flexible, &
            entry%access, &
            error)
       if (error) return
    endif
  end subroutine cubeedit_entry_register
  !
  subroutine cubeedit_entry_parse(entry,line,user,error)
    !----------------------------------------------------------------------
    ! Parse /ENTRY ient [access]
    !----------------------------------------------------------------------
    class(entry_opt_t), intent(in)    :: entry
    character(len=*),   intent(in)    :: line
    type(entry_user_t), intent(out)   :: user
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: rname='ENTRY>PARSE'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call entry%opt%present(line,user%do,error)
    if (error) return
    if (user%do) then
       call cubetools_getarg(line,entry%opt,1,user%ientry,mandatory,error)
       if (error) return
       if (.not.entry%fixed) then
          user%access = 'IMAGE'
          call cubetools_getarg(line,entry%opt,2,user%access,.not.mandatory,error)
          if (error) return
       else
          user%access = strg_unk
       endif
    endif
  end subroutine cubeedit_entry_parse
  !
  subroutine cubeedit_entry_user2prog(entry,user,doaccess,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(entry_opt_t),    intent(in)    :: entry
    type(entry_user_t),    intent(in)    :: user
    integer(kind=code_k),  intent(out)   :: doaccess
    logical,               intent(inout) :: error
    !
    character(len=8) :: access
    integer(kind=4) :: ikey
    character(len=*), parameter :: rname='ENTRY>USER2PROG'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubetools_keywordlist_user2prog(entry%access,user%access,ikey,access,error)
    if (error) return
    !
    select case (access)
    case('IMAGE')
      doaccess = code_access_imaset
    case('SPECTRUM')
      doaccess = code_access_speset
    case default
      call cubeedit_message(seve%e,rname,'Unsupported access mode: '//trim(user%access))
      error = .true.
      return
    end select
  end subroutine cubeedit_entry_user2prog
end module cubeedit_entry
