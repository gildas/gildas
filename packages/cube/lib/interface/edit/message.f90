!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage CUBE EDIT messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeedit_messaging
  use gpack_def
  use gbl_message
  use cubetools_parameters
  !
  public :: edseve,seve,mess_l
  public :: cubeedit_message_set_id,cubeedit_message
  public :: cubeedit_message_set_trace,cubeedit_message_get_trace
  public :: cubeedit_message_set_others,cubeedit_message_get_others
  private
  !
  ! Identifier used for message identification
  integer(kind=4) :: cubeedit_message_id = gpack_global_id  ! Default value for startup message
  !
  type :: cubeedit_messaging_debug_t
     integer(kind=code_k) :: trace = seve%t
     integer(kind=code_k) :: others = seve%d
  end type cubeedit_messaging_debug_t
  !
  type(cubeedit_messaging_debug_t) :: edseve
  !
contains
  !
  subroutine cubeedit_message_set_id(id)
    !---------------------------------------------------------------------
    ! Alter library id into input id. Should be called by the library
    ! which wants to share its id with the current one.
    !---------------------------------------------------------------------
    integer(kind=4), intent(in) :: id
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='MESSAGE>SET>ID'
    !
    cubeedit_message_id = id
    write (mess,'(A,I0)') 'Now use id #',cubeedit_message_id
    call cubeedit_message(seve%d,rname,mess)
  end subroutine cubeedit_message_set_id
  !
  subroutine cubeedit_message(mkind,procname,message)
    use cubetools_cmessaging
    !---------------------------------------------------------------------
    ! Messaging facility for the current library. Calls the low-level
    ! (internal) messaging routine with its own identifier.
    !---------------------------------------------------------------------
    integer(kind=4),  intent(in) :: mkind     ! Message kind
    character(len=*), intent(in) :: procname  ! Name of calling procedure
    character(len=*), intent(in) :: message   ! Message string
    !
    call cubetools_cmessage(cubeedit_message_id,mkind,'EDIT>'//procname,message)
  end subroutine cubeedit_message
  !
  subroutine cubeedit_message_set_trace(on)
    !---------------------------------------------------------------------
    ! @ public
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       edseve%trace = seve%i
    else
       edseve%trace = seve%t
    endif
  end subroutine cubeedit_message_set_trace
  !
  subroutine cubeedit_message_set_others(on)
    !---------------------------------------------------------------------
    ! @ public
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       edseve%others = seve%i
    else
       edseve%others = seve%d
    endif
  end subroutine cubeedit_message_set_others
  !
  function cubeedit_message_get_trace()
    !---------------------------------------------------------------------
    ! @ public
    !---------------------------------------------------------------------
    logical :: cubeedit_message_get_trace
    !
    cubeedit_message_get_trace = edseve%trace.eq.seve%i
    !
  end function cubeedit_message_get_trace
  !
  function cubeedit_message_get_others()
    !---------------------------------------------------------------------
    ! @ public
    !---------------------------------------------------------------------
    logical :: cubeedit_message_get_others
    !
    cubeedit_message_get_others = edseve%others.eq.seve%i
    !
  end function cubeedit_message_get_others
  !
end module cubeedit_messaging
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
