!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeedit_put
  use cubetools_parameters
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeedit_messaging
  use cubeedit_putheader
  use cubeedit_putdata
  !
  public :: put
  private
  !
  type :: put_comm_t
     type(option_t), pointer :: comm
     type(putheader_comm_t)  :: head
     type(putdata_comm_t)    :: data
   contains
     procedure, public  :: register => cubeedit_put_register
     procedure, private :: parse    => cubeedit_put_parse
     procedure, private :: main     => cubeedit_put_main 
  end type put_comm_t
  type(put_comm_t) :: put
  !
  type put_user_t
     character(len=varn_l)  :: edid
     type(putheader_user_t) :: head
     type(putdata_user_t)   :: data
  end type put_user_t
  !
contains
  !
  subroutine cubeedit_put_command(line,error)
    !-------------------------------------------------------------------
    ! Support routine for command PUT
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(put_user_t) :: user
    character(len=*), parameter :: rname='PUT>COMMAND'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call put%parse(line,user,error)
    if (error) return
    call put%main(user,error)
    if (error) return
  end subroutine cubeedit_put_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubeedit_put_register(put,error)
    use cubedag_parameters
    !-------------------------------------------------------------------
    ! Register EDIT\PUT and its options
    !-------------------------------------------------------------------
    class(put_comm_t), intent(inout) :: put
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: comm_abstract = &
         'Put information into a cube being edited'
    character(len=*), parameter :: comm_help = &
         'WARNING: there are no checks on the data or header&
         & modifications being done, proceed with care. If the header&
         & needs to be modified ina coherent way, please refer to&
         & command CUBE\MODIFY'
    !
    type(standard_arg_t) :: stdarg
    character(len=*), parameter :: rname='PUT>REGISTER'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'PUT','[newid]',&
         comm_abstract,&
         comm_help,&
         cubeedit_put_command,&
         put%comm,error)
    if (error) return
    call stdarg%register(&
         'NewId',&
         'Editing Identifier',&
         strg_id,&
         code_arg_optional, error)
    if (error) return
    !
    call put%head%register(error)
    if (error) return
    !
    call put%data%register(error)
    if (error) return
  end subroutine cubeedit_put_register
  !
  subroutine cubeedit_put_parse(put,line,user,error)
    !-------------------------------------------------------------------
    ! Parse routine for command PUT
    !-------------------------------------------------------------------
    class(put_comm_t), intent(in)    :: put
    character(len=*),  intent(in)    :: line
    type(put_user_t),  intent(out)   :: user
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='PUT>PARSE'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    user%edid = strg_star
    call cubetools_getarg(line,put%comm,1,user%edid,.not.mandatory,error)
    if (error) return
    !
    call put%head%parse(line,user%head,error)
    if (error) return
    call put%data%parse(line,user%data,error)
    if (error) return
  end subroutine cubeedit_put_parse
  !
  subroutine cubeedit_put_main(put,user,error)
    use cube_types
    use cubeedit_cube_buffer
    !-------------------------------------------------------------------
    ! Main routine for command PUT
    !-------------------------------------------------------------------
    class(put_comm_t), intent(in)    :: put
    type(put_user_t),  intent(in)    :: user
    logical,           intent(inout) :: error
    !
    type(cube_t), pointer :: cube
    integer(kind=entr_k) :: ie
    character(len=*), parameter :: rname='PUT>MAIN'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubeedit_bufferlist_get_cube_pointer(user%edid,cube,error)
    if (error) return
    !
    call put%head%main(user%head,cube,error)
    if (error) return
    call cubeedit_bufferlist_get_cube_entry(user%edid,ie,error)
    if (error) return   
    call put%data%main(user%data,cube,ie,error)
    if (error) return
    call cubeedit_bufferlist_set_cube_entry(user%edid,ie,error)
    if (error) return   
    !
  end subroutine cubeedit_put_main
  ! 
end module cubeedit_put
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
