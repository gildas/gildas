!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeedit_cube_buffer
  use cubetools_parameters
  use cubetools_header_types
  use cubetools_list
  use cube_types
  use cubeedit_messaging
  !
  public :: undetermined_dim
  !
  public :: cubeedit_bufferlist_open,cubeedit_bufferlist_close
  public :: cubeedit_bufferlist_show,cubeedit_bufferlist_abort
  public :: cubeedit_bufferlist_get_cube_pointer
  public :: cubeedit_bufferlist_get_cube_entry,cubeedit_bufferlist_set_cube_entry
  public :: cubeedit_bufferlist_get_buffer_name
  public :: cubeedit_bufferlist_create,cubeedit_bufferlist_list
  private
  !
  integer(kind=list_k), parameter :: not_found = -1
  integer(kind=entr_k), parameter :: undetermined_dim = -2
  !
  integer(kind=4), parameter :: root_id = 0
  !
  type, extends(tools_object_t) :: cube_buffer_t
     type(cube_t), pointer :: cube=>null()  ! The new cube
     character(len=varn_l) :: name          ! NewId during editing
     integer(kind=code_k)  :: access        ! Access order
     integer(kind=4)       :: id            ! Id of original cube
     integer(kind=entr_k)  :: ie            ! Current entry
     logical               :: new           ! Buffer is created from scratch
   contains
     procedure :: open     => cubeedit_cube_buffer_open
     procedure :: create   => cubeedit_cube_buffer_create
     procedure :: show     => cubeedit_cube_buffer_show
     procedure :: close    => cubeedit_cube_buffer_close
     procedure :: abort    => cubeedit_cube_buffer_abort
     procedure :: resize   => cubeedit_cube_buffer_resize
     procedure :: initdata => cubeedit_cube_buffer_data_init
     procedure :: print    => cubeedit_cube_buffer_print
     !
     final     :: cubeedit_cube_buffer_final
  end type cube_buffer_t
  !
  type(tools_list_t) :: opened
  !
  interface cubeedit_cube_buffer_ptr
    module procedure cubeedit_cube_buffer_ptr_byname
    module procedure cubeedit_cube_buffer_ptr_bynum
  end interface cubeedit_cube_buffer_ptr
  !
contains
  !
  subroutine cubeedit_cube_buffer_final(buffer)
    !----------------------------------------------------------------------
    ! Finalizes a buffer
    !----------------------------------------------------------------------
    type(cube_buffer_t), intent(inout) :: buffer
    !
    character(len=*), parameter :: rname='CUBE>BUFFER>FINAL'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    buffer%cube => null()
    buffer%name = ''
  end subroutine cubeedit_cube_buffer_final
  !
  subroutine cubeedit_cube_buffer_open(buffer,buffprod,likearg,likeid,access,  &
    dims,doinit,initval,name,error)
    use cubetools_structure
    use cubedag_node_type
    use cubedag_dag
    use cubetuple_transpose
    use cubeadm_cubeid_types
    use cubeadm_cubeprod_types
    use cubeadm_clone
    use cubeadm_get
    use cubeadm_opened
    use cubeadm_copy_tool
    !----------------------------------------------------------------------
    ! Opens a cube in a buffer, resizes it and initializes it if need be
    !----------------------------------------------------------------------
    class(cube_buffer_t), intent(out)   :: buffer
    type(cube_prod_t),    intent(in)    :: buffprod
    type(cubeid_arg_t),   intent(in)    :: likearg
    type(cubeid_user_t),  intent(in)    :: likeid
    integer(kind=code_k), intent(in)    :: access
    integer(kind=entr_k), intent(in)    :: dims(3)
    logical,              intent(in)    :: doinit
    real(kind=sign_k),    intent(in)    :: initval
    character(len=*),     intent(in)    :: name
    logical,              intent(inout) :: error
    !
    type(cube_t), pointer :: ref
    logical :: modified_dims
    class(cubedag_node_object_t), pointer :: dno
    character(len=*), parameter :: rname='CUBE>BUFFER>OPEN'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    buffer%access = access
    buffer%name   = name
    modified_dims = any(dims.ne.undetermined_dim)
    buffer%ie     = 0
    buffer%new    = .false.
    !
    if (modified_dims) then
       call cubeadm_get_header(likearg,likeid,ref,error,  &
         access=buffer%access,action=code_read_head)
       if (error) return
       call cubeadm_clone_header(buffprod,ref,buffer%cube,error)
       if (error) return
       call cubedag_dag_nullid(buffer%cube%node%id)
       call buffer%resize(dims,error)
       if (error) return
       call buffer%initdata(initval,error)
       if (error) return
    else
       if (doinit) then
          call cubeadm_get_header(likearg,likeid,ref,error,  &
            access=buffer%access,action=code_read_head)
          if (error) return
          call cubeadm_clone_header(buffprod,ref,buffer%cube,error)
          if (error) return
          call cubedag_dag_nullid(buffer%cube%node%id)
          call buffer%initdata(initval,error)
          if (error) return
       else
          ! Header part:
          ! Here cubeadm_get_header prepares the transposition if relevant.
          ! The cube to be used in the tuple is set to the imaset or speset
          ! at this stage, but in case of transposition its data is not yet
          ! ready.
          call cubeadm_get_header(likearg,likeid,ref,error,  &
            access=buffer%access,action=code_read)
          if (error) return
          call cubeadm_clone_header(buffprod,ref,buffer%cube,error)
          if (error) return
          call cubedag_dag_nullid(buffer%cube%node%id)
          ! Data part
          ! First apply transposition on the input cube if needed
          call cubetuple_autotranspose_cube(ref,error)
          if (error)  return
          ! Now switch the input cube to subset access
          call cubeadm_access_header(ref,code_access_subset,code_read,error)
          if (error)  return
          ! And copy its data in subset access
          call cubeadm_copy_data(ref,buffer%cube,error)
          if (error) return
       endif
    endif
    ! All or part copied from likearg => inherit as child
    buffer%id = ref%node%id
    !
    ! Prepare extrema processing. Assume no parallel put involved (1 pseudo-task)
    ! ZZZ Should this be merged at clone_header time, knowing that most of all
    !     the other commands will reallocate with the proper number of tasks?
    call buffer%cube%proc%allocate_extrema(buffer%cube%head,1,error)
    if (error)  return
    !
    ! Unreference the output cube from the 'opened' ones, so that it is left
    ! "opened" in memory and not yet inserted in DAG. See symetric action
    ! in cubeedit_cube_buffer_close. The cube will be properly finalized and
    ! inserted at that moment.
    dno => buffer%cube
    call cubeadm_parents_children_pop(dno,error)
    if (error) return
  end subroutine cubeedit_cube_buffer_open
  !
  subroutine cubeedit_cube_buffer_create(buffer,access,dims,initval,name,error)
    use cubedag_allflags
    use cubedag_node_type
    use cubedag_dag
    use cubeadm_clone
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! creates a cube buffer from scratch and initializes it
    !----------------------------------------------------------------------
    class(cube_buffer_t), intent(out)   :: buffer
    integer(kind=code_k), intent(in)    :: access
    integer(kind=entr_k), intent(in)    :: dims(3) ! *** JP it should be of type integer(kind=data_k)
    real(kind=sign_k),    intent(in)    :: initval
    character(len=*),     intent(in)    :: name
    logical,              intent(inout) :: error
    !
    integer(kind=ndim_k) :: ndim
    integer(kind=data_k) :: mydims(maxdim)
    class(cubedag_node_object_t), pointer :: dno
    character(len=*), parameter :: rname='CUBE>BUFFER>OPEN'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    buffer%access = access
    buffer%name   = name
    buffer%ie     = 0
    buffer%id     = root_id
    buffer%new    = .true.
    !
    ndim = 3
    mydims(1:3)      = dims
    mydims(4:maxdim) = 0
    call cubeadm_create_header([flag_edit,flag_cube],access,ndim,mydims,buffer%cube,error)
    if (error) return
    ! *** JP: Does the order matters after this point?
    call cubedag_dag_nullid(buffer%cube%node%id)
    call buffer%initdata(initval,error)
    if (error) return
    !
    ! Prepare extrema processing. Assume no parallel put involved (1 pseudo-task)
    ! ZZZ Should this be merged at create_header time, knowing that most of all
    !     the other commands will reallocate with the proper number of tasks?
    call buffer%cube%proc%allocate_extrema(buffer%cube%head,1,error)
    if (error)  return
    !
    ! Unreference the output cube from the 'opened' ones, so that it is left
    ! "opened" in memory and not yet inserted in DAG. See symetric action
    ! in cubeedit_cube_buffer_close. The cube will be properly finalized and
    ! inserted at that moment.
    dno => buffer%cube
    call cubeadm_parents_children_pop(dno,error)
    if (error) return
  end subroutine cubeedit_cube_buffer_create
  !
  subroutine cubeedit_cube_buffer_show(buffer,error)
    use cubetools_header_types
    !----------------------------------------------------------------------
    ! Shows cube buffer header
    !----------------------------------------------------------------------
    class(cube_buffer_t), intent(in)    :: buffer
    logical,              intent(inout) :: error
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='CUBE>BUFFER>SHOW'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubeedit_message(seve%r,rname,'')
    write(mess,'(a,24x,a,4x,a,x,a)') 'Editing Id','Access','Orig Id','Entry'
    call cubeedit_message(seve%r,rname,mess)
    call buffer%print(mess,error)
    if (error) return
    call cubeedit_message(seve%r,rname,mess)
    call cubeedit_message(seve%r,rname,'')
    call buffer%cube%head%list(error)
    if (error) return
  end subroutine cubeedit_cube_buffer_show
  !
  subroutine cubeedit_cube_buffer_abort(buffer,error)
    use cubedag_node_type
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! Aborts buffer edition
    !----------------------------------------------------------------------
    class(cube_buffer_t), intent(inout) :: buffer
    logical,              intent(inout) :: error
    !
    class(cubedag_node_object_t), pointer :: dno
    logical :: lerror
    character(len=*), parameter :: rname='CUBE>BUFFER>ABORT'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    ! call cubeadm_parents_add(root,code_read)
    dno => buffer%cube
    call cubeadm_children_add(dno,code_write)
    !
    ! Explicit call of cubeadm_finish_all with error=.true. will ensure
    ! the cube is destroyed instead of attached in DAG
    lerror = .true.
    call cubeadm_finish_all('ABORT','ABORT',lerror)
  end subroutine cubeedit_cube_buffer_abort
  !
  subroutine cubeedit_cube_buffer_close(buffer,newname,error)
    use gkernel_interfaces
    use cubedag_node_type
    use cubedag_dag
    use cubedag_node
    use cubedag_flag
    use cubeadm_opened
    use cubeadm_cubeid_types
    !----------------------------------------------------------------------
    ! Closes buffer edition
    !----------------------------------------------------------------------
    class(cube_buffer_t), intent(inout) :: buffer
    character(len=*),     intent(in)    :: newname
    logical,              intent(inout) :: error
    !
    class(cubedag_node_object_t), pointer :: dno
    character(len=file_l) :: family
    type(flag_t), allocatable :: flags(:)
    character(len=*), parameter :: rname='CUBE>BUFFER>CLOSE'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubedag_dag_newid(buffer%cube%node%id)
    !
    ! Family + flags
    call cubeadm_cubeid_string2familyflags(newname,family,flags,error)
    if (error) return
    call cubedag_node_set_family(buffer%cube,family,error)
    if (error) return
    if (allocated(flags)) then
      ! Apply the user-defined flags
      call cubedag_node_set_flags(buffer%cube,flags,error)
      if (error) return
    else
      ! Let flags as they were created at clone/create time
    endif
    !
    call cubedag_node_set_header(buffer%cube,buffer%cube%head,error)
    if (error) return
    !
    ! Add parent
    call cubedag_dag_get_object(buffer%id,dno,error)
    if (error)  return
    call cubeadm_parents_add(dno,code_read)
    ! Add child
    dno => buffer%cube
    call cubeadm_children_add(dno,code_write)
  end subroutine cubeedit_cube_buffer_close
  !
  subroutine cubeedit_cube_buffer_print(buffer,mess,error)
    !----------------------------------------------------------------------
    ! Returns a message containing a summary of the buffer
    !----------------------------------------------------------------------
    class(cube_buffer_t),  intent(in)    :: buffer
    character(len=mess_l), intent(out)   :: mess
    logical,               intent(inout) :: error
    !
    character(len=8) :: access,cubeid
    character(len=*), parameter :: rname='CUBE>BUFFER>PRINT'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    if (buffer%access.eq.code_access_speset) then
       access = 'Spectrum'
    else if (buffer%access.eq.code_access_imaset) then
       access = 'Image'
    else
       access = strg_unk
    endif
    if (buffer%id.eq.root_id) then
       cubeid = 'New'
    else
       write(cubeid,'(i0)') buffer%id
    endif
    write(mess,'(a32,2(a10),i0)') buffer%name,access,cubeid,buffer%ie
  end subroutine cubeedit_cube_buffer_print
  !
  !----------------------------------------------------------------------
  !
  subroutine cubeedit_cube_buffer_resize(buffer,dims,error)
    use cubetools_axis_types
    use cubetools_header_methods
    use cubetools_unit
    !----------------------------------------------------------------------
    ! Resizes a cube buffer to dims. Ensure that the reference value
    ! keeps the same place on axis when the dimensions are identical
    ! to handle the /LIKE case. Else puts it at the axis center
    !----------------------------------------------------------------------
    class(cube_buffer_t), intent(inout) :: buffer
    integer(kind=entr_k), intent(in)    :: dims(3)
    logical,              intent(inout) :: error
    !
    type(axis_t) :: laxis,maxis,caxis
    character(len=*), parameter :: rname='CUBE>BUFFER>RESIZE'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubetools_header_get_axis_head_l(buffer%cube%head,laxis,error)
    if (error) return
    call cubetools_header_get_axis_head_m(buffer%cube%head,maxis,error)
    if (error) return
    call cubetools_header_get_axis_head_c(buffer%cube%head,caxis,error)
    if (error) return
    if (laxis%n.ne.dims(1)) laxis%ref = laxis%n/2.+1
    if (maxis%n.ne.dims(2)) maxis%ref = maxis%n/2.+1
    if (caxis%n.ne.dims(3)) caxis%ref = caxis%n/2.+1
    laxis%n = dims(1)
    maxis%n = dims(2)
    caxis%n = dims(3)
    call cubetools_header_update_axset_l(laxis,buffer%cube%head,error)
    if (error) return
    call cubetools_header_update_axset_m(maxis,buffer%cube%head,error)
    if (error) return
    if (caxis%kind.eq.code_unit_freq) then
       call cubetools_header_update_frequency_from_axis(caxis,buffer%cube%head,error)
       if (error) return
    else if (caxis%kind.eq.code_unit_velo) then
       call cubetools_header_update_velocity_from_axis(caxis,buffer%cube%head,error)
       if (error) return
    else
       call cubetools_header_update_axset_c(caxis,buffer%cube%head,error)
       if (error) return
       call cubeedit_message(seve%w,rname,'Unknown kind of Spectral axis')
    endif
  end subroutine cubeedit_cube_buffer_resize
  !
  subroutine cubeedit_cube_buffer_data_init(buffer,initval,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! Initializes a cube buffer to a value
    !
    ! *** JP: This kind of operation should always be done per subcube.
    !
    !----------------------------------------------------------------------
    class(cube_buffer_t), intent(inout) :: buffer
    real(kind=sign_k),    intent(in)    :: initval
    logical,              intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    integer(kind=code_k) :: order
    character(len=*), parameter :: rname='CUBE>BUFFER>DATA>INIT'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,error)
    if (error) return
    order = buffer%cube%order()
    !$OMP PARALLEL DEFAULT(none) SHARED(order,buffer,initval,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(order,buffer,initval,error) FIRSTPRIVATE(iter)
       if (.not.error) then
          select case (order)
          case (code_cube_imaset)
             call cubeedit_cube_buffer_data_init_imag(buffer%cube,initval,iter,error)
          case (code_cube_speset)
             call cubeedit_cube_buffer_data_init_spec(buffer%cube,initval,iter,error)
          end select
       endif
       !$OMP END TASK
    enddo ! ie
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubeedit_cube_buffer_data_init
  !
  subroutine cubeedit_cube_buffer_data_init_spec(cube,initval,iter,error)
    use cubeadm_taskloop
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    ! Initializes a speset cube buffer to a value
    !----------------------------------------------------------------------
    type(cube_t),             pointer       :: cube
    real(kind=sign_k),        intent(in)    :: initval
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(spectrum_t) :: spe
    character(len=*), parameter :: rname='CUBE>BUFFER>DATA>INIT>SPEC'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call spe%allocate('spectral buffer',cube,iter,error)
    if (error) return
    call spe%y%set(initval,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      call spe%put(iter%ie,error)
      if (error) return
    enddo ! ie
  end subroutine cubeedit_cube_buffer_data_init_spec
  !
  subroutine cubeedit_cube_buffer_data_init_imag(cube,initval,iter,error)
    use cubeadm_taskloop
    use cubeadm_image_types
    !----------------------------------------------------------------------
    ! Initializes a speset cube buffer to a value
    !----------------------------------------------------------------------
    type(cube_t),             pointer       :: cube
    real(kind=sign_k),        intent(in)    :: initval
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(image_t) :: ima
    character(len=*), parameter :: rname='CUBE>BUFFER>DATA>INIT>IMAG'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call ima%allocate('image buffer',cube,iter,error)
    if (error) return
    call ima%set(initval,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      call ima%put(iter%ie,error)
      if (error) return
    enddo ! ie
  end subroutine cubeedit_cube_buffer_data_init_imag
  !
  !----------------------------------------------------------------------
  !
  subroutine cubeedit_bufferlist_find(name,found,error)
    !----------------------------------------------------------------------
    ! Loops through the list to look for a opened cube with name 
    !----------------------------------------------------------------------
    character(len=*),     intent(in)    :: name 
    integer(kind=list_k), intent(out)   :: found
    logical,              intent(inout) :: error
    !
    integer(kind=list_k) :: ibuff
    type(cube_buffer_t), pointer :: buffer
    character(len=*), parameter :: rname='BUFFERLIST>FIND'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    if (name.eq.strg_star) then
       if (opened%n.gt.0) then
         found = opened%n
       else
         found = not_found
       endif
    else
       found = not_found
       do ibuff=1, opened%n
          buffer => cubeedit_cube_buffer_ptr(ibuff,error)
          if (error) return
          if (buffer%name.eq.name) then
             found = ibuff
             return
          endif
       enddo
    endif
  end subroutine cubeedit_bufferlist_find
  !
  subroutine cubeedit_bufferlist_open(name,buffprod,likearg,likeid,access,  &
    dims,doinit,init,error)
    use cubetools_structure
    use cubeadm_cubeid_types
    use cubeadm_cubeprod_types
    !----------------------------------------------------------------------
    ! Opens a new buffer in buffer list
    !----------------------------------------------------------------------
    character(len=*),     intent(in)    :: name
    type(cube_prod_t),    intent(in)    :: buffprod
    type(cubeid_arg_t),   intent(in)    :: likearg
    type(cubeid_user_t),  intent(in)    :: likeid
    integer(kind=code_k), intent(in)    :: access
    integer(kind=entr_k), intent(in)    :: dims(3)
    logical,              intent(in)    :: doinit
    real(kind=sign_k),    intent(in)    :: init
    logical,              intent(inout) :: error
    !
    integer(kind=list_k) :: found
    type(cube_buffer_t), pointer :: buffer
    type(cube_buffer_t) :: template
    character(len=*), parameter :: rname='BUFFERLIST>OPEN'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubeedit_bufferlist_find(name,found,error)
    if (error) return
    !
    if (found.ne.not_found) then
       call cubeedit_message(seve%e,rname,'There is already an opened cube&
            & with identifier '//name)
       error = .true.
       return
    endif
    !
    ! No previous buffer found, insert new one
    found = opened%n+1
    call opened%realloc(found,error)
    if (error) return
    ! Allocate one instance in memory
    call opened%list(found)%allocate(template,error)
    if (error)  return
    buffer => cubeedit_cube_buffer_ptr(found,error)
    if (error) return
    call buffer%open(buffprod,likearg,likeid,access,dims,doinit,init,name,error)
    if (error) return
    ! Save in list. In case of error we leave 1 allocated but unused buffer,
    ! it will be reused at next call.
    opened%n = found
  end subroutine cubeedit_bufferlist_open
  !
  subroutine cubeedit_bufferlist_create(name,access,dims,init,error)
    !----------------------------------------------------------------------
    ! Creates a new buffer in buffer list
    !----------------------------------------------------------------------
    character(len=*),     intent(in)    :: name
    integer(kind=code_k), intent(in)    :: access
    integer(kind=entr_k), intent(in)    :: dims(3)
    real(kind=sign_k),    intent(in)    :: init
    logical,              intent(inout) :: error
    !
    integer(kind=list_k) :: found
    type(cube_buffer_t), pointer :: buffer
    type(cube_buffer_t) :: template
    character(len=*), parameter :: rname='BUFFERLIST>CREATE'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubeedit_bufferlist_find(name,found,error)
    if (error) return
    !
    if (found.ne.not_found) then
       call cubeedit_message(seve%e,rname,'There is already an opened cube&
            & with identifier '//name)
       error = .true.
       return
    endif
    !
    ! No previous buffer found, insert new one
    found = opened%n+1
    call opened%realloc(found,error)
    if (error) return
    ! Allocate one instance in memory
    call opened%list(found)%allocate(template,error)
    if (error)  return
    buffer => cubeedit_cube_buffer_ptr(found,error)
    if (error) return
    call buffer%create(access,dims,init,name,error)
    if (error) return
    ! Save in list. In case of error we leave 1 allocated but unused buffer,
    ! it will be reused at next call.
    opened%n = found
  end subroutine cubeedit_bufferlist_create
  !
  subroutine cubeedit_bufferlist_show(name,error)
    !----------------------------------------------------------------------
    ! Shows the header of a cube being edited if found
    !----------------------------------------------------------------------
    character(len=*),     intent(in)    :: name 
    logical,              intent(inout) :: error
    !
    type(cube_buffer_t), pointer :: buffer
    character(len=*), parameter :: rname='BUFFERLIST>SHOW'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    buffer => cubeedit_cube_buffer_ptr(name,error)
    if (error) return
    call buffer%show(error)
    if (error) return
  end subroutine cubeedit_bufferlist_show
  !
  subroutine cubeedit_bufferlist_abort(name,error)
    !----------------------------------------------------------------------
    ! aborts the edition of a cube if found
    !----------------------------------------------------------------------
    character(len=*),     intent(in)    :: name 
    logical,              intent(inout) :: error
    !
    integer(kind=list_k) :: found
    type(cube_buffer_t), pointer :: buffer
    character(len=*), parameter :: rname='BUFFERLIST>ABORT'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubeedit_bufferlist_find(name,found,error)
    if (error) return
    !
    if (found.eq.not_found) then
       call cubeedit_message(seve%e,rname,trim(name)//' is not currently open')
       error = .true.
       return
    endif
    !
    buffer => cubeedit_cube_buffer_ptr(found,error)
    if (error)  return
    call buffer%abort(error)
    if (error) return
    call opened%pop(found,error)
    if (error) return
  end subroutine cubeedit_bufferlist_abort
  !
  subroutine cubeedit_bufferlist_close(name,newname,error)
    !----------------------------------------------------------------------
    ! closes the edition of a cube if found
    !----------------------------------------------------------------------
    character(len=*),     intent(in)    :: name
    character(len=*),     intent(in)    :: newname
    logical,              intent(inout) :: error
    !
    integer(kind=list_k) :: found
    type(cube_buffer_t), pointer :: buffer
    character(len=*), parameter :: rname='BUFFERLIST>CLOSE'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubeedit_bufferlist_find(name,found,error)
    if (error) return
    !
    if (found.eq.not_found) then
       call cubeedit_message(seve%e,rname,trim(name)//' is not currently open')
       error = .true.
       return
    endif
    !
    buffer => cubeedit_cube_buffer_ptr(found,error)
    if (error)  return
    call buffer%close(newname,error)
    if (error) return
    call opened%pop(found,error)
    if (error) return
  end subroutine cubeedit_bufferlist_close
  !
  subroutine cubeedit_bufferlist_list(error)
    !----------------------------------------------------------------------
    ! List the currently open buffers
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    character(len=mess_l) :: mess,bmess
    integer(kind=list_k) :: ibuff
    type(cube_buffer_t), pointer :: buffer
    character(len=*), parameter :: rname='BUFFERLIST>LIST'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    write(mess,'(a,i0)') 'Currently Opened cubes for edition: ',opened%n
    call cubeedit_message(seve%r,rname,mess)
    if (opened%n.gt.0) then
       write(mess,'(3x,a,x,a,24x,a,4x,a,x,a)') '#','Editing Id','Access','Orig Id','Entry'
       call cubeedit_message(seve%r,rname,mess)
       !
       do ibuff=1, opened%n
          buffer => cubeedit_cube_buffer_ptr(ibuff,error)
          if (error) return
          call buffer%print(bmess,error)
          if (error) return
          write(mess,'(i4)') ibuff
          mess = trim(mess)//' '//bmess
          call cubeedit_message(seve%r,rname,mess)
       enddo
    endif
  end subroutine cubeedit_bufferlist_list
  !
  !----------------------------------------------------------------------
  !
  subroutine cubeedit_bufferlist_get_cube_pointer(name,cube,error)
    !----------------------------------------------------------------------
    ! Returns a pointer to buffer cube if found
    !----------------------------------------------------------------------
    character(len=*),      intent(in)    :: name
    type(cube_t), pointer, intent(out)   :: cube
    logical,               intent(inout) :: error
    !
    type(cube_buffer_t), pointer :: buffer
    character(len=*), parameter :: rname='BUFFERLIST>GET>CUBE>POINTER'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    buffer => cubeedit_cube_buffer_ptr(name,error)
    if (error) return
    cube => buffer%cube
  end subroutine cubeedit_bufferlist_get_cube_pointer
  !
  subroutine cubeedit_bufferlist_get_buffer_name(name,oname,error)
    !----------------------------------------------------------------------
    ! Returns a pointer to buffer cube if found
    !----------------------------------------------------------------------
    character(len=*),      intent(in)    :: name
    character(len=*),      intent(out)   :: oname
    logical,               intent(inout) :: error
    !
    type(cube_buffer_t), pointer :: buffer
    character(len=*), parameter :: rname='BUFFERLIST>GET>BUFFER>NAME'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    buffer => cubeedit_cube_buffer_ptr(name,error)
    if (error) return
    oname = buffer%name
  end subroutine cubeedit_bufferlist_get_buffer_name
  !
  subroutine cubeedit_bufferlist_get_cube_entry(name,ie,error)
    !----------------------------------------------------------------------
    ! Returns the last touched entry for name
    !----------------------------------------------------------------------
    character(len=*),      intent(in)    :: name
    integer(kind=entr_k),  intent(out)   :: ie
    logical,               intent(inout) :: error
    !
    type(cube_buffer_t), pointer :: buffer
    character(len=*), parameter :: rname='BUFFERLIST>GET>CUBE>ENTRY'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    buffer => cubeedit_cube_buffer_ptr(name,error)
    if (error) return
    ie = buffer%ie
  end subroutine cubeedit_bufferlist_get_cube_entry
  !
  subroutine cubeedit_bufferlist_set_cube_entry(name,ie,error)
    !----------------------------------------------------------------------
    ! Sets the last touched entry for name
    !----------------------------------------------------------------------
    character(len=*),      intent(in)    :: name
    integer(kind=entr_k),  intent(in)    :: ie
    logical,               intent(inout) :: error
    !
    type(cube_buffer_t), pointer :: buffer
    character(len=*), parameter :: rname='BUFFERLIST>SET>CUBE>ENTRY'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    buffer => cubeedit_cube_buffer_ptr(name,error)
    if (error) return
    buffer%ie = ie
  end subroutine cubeedit_bufferlist_set_cube_entry
  !
  function cubeedit_cube_buffer_ptr_byname(name,error)
    !-------------------------------------------------------------------
    ! Given its name, return a pointer to the named buffer currently
    ! opened.
    !-------------------------------------------------------------------
    type(cube_buffer_t), pointer :: cubeedit_cube_buffer_ptr_byname
    character(len=*),    intent(in)    :: name
    logical,             intent(inout) :: error
    !
    integer(kind=list_k) :: found
    character(len=*), parameter :: rname='CUBE>BUFFER>PTR'
    !
    cubeedit_cube_buffer_ptr_byname => null()
    !
    call cubeedit_bufferlist_find(name,found,error)
    if (error) return
    if (found.eq.not_found) then
       call cubeedit_message(seve%e,rname,trim(name)//' is not currently opened')
       error = .true.
       return
    endif
    !
    cubeedit_cube_buffer_ptr_byname => cubeedit_cube_buffer_ptr_bynum(found,error)
    if (error) return
  end function cubeedit_cube_buffer_ptr_byname
  !
  function cubeedit_cube_buffer_ptr_bynum(ibuffer,error)
    !-------------------------------------------------------------------
    ! Given its number, return a pointer to the named buffer currently
    ! opened.
    !-------------------------------------------------------------------
    type(cube_buffer_t),  pointer :: cubeedit_cube_buffer_ptr_bynum
    integer(kind=list_k), intent(in)    :: ibuffer
    logical,              intent(inout) :: error
    !
    class(tools_object_t), pointer :: object
    character(len=*), parameter :: rname='CUBE>BUFFER>PTR'
    !
    cubeedit_cube_buffer_ptr_bynum => null()
    !
    if (ibuffer.le.0 .or. ibuffer.gt.ubound(opened%list,1)) then
      call cubeedit_message(seve%e,rname,'Internal error: invalid buffer number')
      error = .true.
      return
    endif
    !
    object => opened%list(ibuffer)%p
    select type (object)
    type is (cube_buffer_t)
       cubeedit_cube_buffer_ptr_bynum => object
    class default
       call cubeedit_message(seve%e,rname,'Internal error: object has wrong type')
       error = .true.
       return
    end select
  end function cubeedit_cube_buffer_ptr_bynum
end module cubeedit_cube_buffer
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
