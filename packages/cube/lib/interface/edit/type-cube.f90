!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! *** JP: module name is a bit ambiguous with cube_types defined in
! *** JP: tuple/type-cube.f90
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeedit_cube_types
  use cubetools_structure
  use cubetools_keywordlist_types
  use cubeedit_messaging
  !
  public :: cube_opt_t,cube_user_t,cube_prog_t
  private
  !
  type cube_opt_t
     type(option_t), pointer :: opt
   contains
     procedure :: register => cubeedit_cube_register
     procedure :: parse    => cubeedit_cube_parse
  end type cube_opt_t
  !
  type cube_user_t
     logical :: do = .false.  ! Option was present
   contains
     procedure :: init   => cubeedit_cube_user_init
     procedure :: toprog => cubeedit_cube_user_toprog
  end type cube_user_t
  !
  type cube_prog_t
  end type cube_prog_t
  !
contains
  !
  subroutine cubeedit_cube_register(option,name,abstract,error)
    use cubetools_unit
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    class(cube_opt_t), intent(out)   :: option
    character(len=*),  intent(in)    :: name
    character(len=*),  intent(in)    :: abstract
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='CUBE>REGISTER'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         name,'',&
         abstract,&
         'Load the whole cube in the named variable. If the cube is &
         &an image set, its first plane and its spatial coordinates &
         &are also loaded in the RG buffer.',&
         option%opt,error)
    if (error) return
  end subroutine cubeedit_cube_register
  !
  subroutine cubeedit_cube_parse(option,line,user,error)
    !----------------------------------------------------------------------
    ! /CUBE (no argument)
    !----------------------------------------------------------------------
    class(cube_opt_t), intent(in)    :: option
    character(len=*),  intent(in)    :: line
    type(cube_user_t), intent(out)   :: user
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='CUBE>PARSE'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call option%opt%present(line,user%do,error)
    if (error) return
  end subroutine cubeedit_cube_parse
  !
  subroutine cubeedit_cube_user_init(user,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(cube_user_t), intent(in)    :: user
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: rname='CUBE>USER>INIT'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
  end subroutine cubeedit_cube_user_init
  !
  subroutine cubeedit_cube_user_toprog(user,option,cube,prog,error)
    use cube_types
    use cubetools_user2prog
    use cubetools_unit
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(cube_user_t), intent(in)    :: user
    type(cube_opt_t),   intent(in)    :: option
    type(cube_t),       intent(in)    :: cube
    type(cube_prog_t),  intent(out)   :: prog
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: rname='CUBE>USER>TOPROG'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
  end subroutine cubeedit_cube_user_toprog
end module cubeedit_cube_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
