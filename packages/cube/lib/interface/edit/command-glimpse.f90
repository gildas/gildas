!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeedit_glimpse
  use cubetools_parameters
  use cubetools_structure
  use cubeedit_messaging
  !
  public :: glimpse
  private
  !
  type :: glimpse_comm_t
     type(option_t), pointer :: comm
   contains
     procedure, public  :: register => cubeedit_glimpse_register
     procedure, private :: parse    => cubeedit_glimpse_parse 
     procedure, private :: main     => cubeedit_glimpse_main  
  end type glimpse_comm_t
  type(glimpse_comm_t) :: glimpse
  !
  type glimpse_user_t
     character(len=varn_l) :: edid
  end type glimpse_user_t
  !
contains
  !
  subroutine cubeedit_glimpse_command(line,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(glimpse_user_t) :: user
    character(len=*), parameter :: rname='GLIMPSE>COMMAND'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call glimpse%parse(line,user,error)
    if (error) return
    call glimpse%main(user,error)
    if (error) return
  end subroutine cubeedit_glimpse_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubeedit_glimpse_register(glimpse,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    class(glimpse_comm_t), intent(inout) :: glimpse
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: comm_abstract = &
         'Glimpse the header of a cube in editing mode'
    character(len=*), parameter :: comm_help = strg_id
    !
    type(standard_arg_t) :: stdarg
    character(len=*), parameter :: rname='GLIMPSE>REGISTER'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'GLIMPSE','[newid]',&
         comm_abstract,&
         comm_help,&
         cubeedit_glimpse_command,&
         glimpse%comm,error)
    if (error) return
    call stdarg%register(&
         'NewId', &
         'Editing Identifier', &
         strg_id,&
         code_arg_optional, error)
    if (error) return
  end subroutine cubeedit_glimpse_register
  !
  subroutine cubeedit_glimpse_parse(glimpse,line,user,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    class(glimpse_comm_t), intent(in)    :: glimpse
    character(len=*),      intent(in)    :: line
    type(glimpse_user_t),  intent(out)   :: user
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='GLIMPSE>PARSE'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    user%edid = strg_star
    call cubetools_getarg(line,glimpse%comm,1,user%edid,.not.mandatory,error)
    if (error) return
  end subroutine cubeedit_glimpse_parse
  !
  subroutine cubeedit_glimpse_main(glimpse,user,error)
    use cubeedit_cube_buffer
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    class(glimpse_comm_t), intent(in)    :: glimpse
    type(glimpse_user_t),  intent(in)    :: user
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='GLIMPSE>MAIN'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubeedit_bufferlist_show(user%edid,error)
    if (error) return
  end subroutine cubeedit_glimpse_main
end module cubeedit_glimpse
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
