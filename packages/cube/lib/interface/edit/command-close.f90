module cubeedit_close
  use cubetools_parameters
  use cubetools_structure
  use cubeadm_identifier
  use cubeedit_messaging
  !
  public :: close
  private
  !
  type :: close_comm_t
     type(option_t), pointer :: comm
     type(identifier_opt_t)  :: as
   contains
     procedure, public  :: register => cubeedit_close_register
     procedure, private :: parse    => cubeedit_close_parse
     procedure, private :: main     => cubeedit_close_main
  end type close_comm_t
  type(close_comm_t) :: close
  !
  type close_user_t
     character(len=varn_l)   :: edid
     type(identifier_user_t) :: identifier
  end type close_user_t
  !
contains
  !
  subroutine cubeedit_close_command(line,error)
    !-------------------------------------------------------------------
    ! Support routine for command CLOSE
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(close_user_t) :: user
    character(len=*), parameter :: rname='CLOSE>COMMAND'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call close%parse(line,user,error)
    if (error) return
    call close%main(user,error)
    if (error) return
  end subroutine cubeedit_close_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubeedit_close_register(close,error)
    !-------------------------------------------------------------------
    ! Register EDIT\CLOSE and its options
    !-------------------------------------------------------------------
    class(close_comm_t), intent(inout) :: close
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: comm_abstract = &
         'Close a cube in editing mode'
    character(len=*), parameter :: comm_help = &
         'By default the family name of the output cube is the same as&
         & the Editing Id (newid) and its flags are "edited,cube" for&
         & cubes created from scratch (no /LIKE), or "edited" followed&
         & by the original flags for cubes created as a copy (/LIKE).&
         & This can be customized by using option /AS.'
    !
    type(standard_arg_t) :: stdarg
    character(len=*), parameter :: rname='CLOSE>REGISTER'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'CLOSE','[newid]',&
         comm_abstract,&
         comm_help,&
         cubeedit_close_command,&
         close%comm,error)
    if (error) return
    call stdarg%register(&
         'NewId', &
         'Editing Identifier', &
         strg_id,&
         code_arg_optional, error)
    if (error) return
    !
    call close%as%register(&
         'Define the identifier of the cube being closed',&
         changeflags,error)
    if (error) return
    !
  end subroutine cubeedit_close_register
  !
  subroutine cubeedit_close_parse(close,line,user,error)
    !-------------------------------------------------------------------
    ! Parse routine for command CLOSE
    !-------------------------------------------------------------------
    class(close_comm_t), intent(in)    :: close
    character(len=*),    intent(in)    :: line
    type(close_user_t),  intent(out)   :: user
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='CLOSE>PARSE'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    user%edid = strg_star
    call cubetools_getarg(line,close%comm,1,user%edid,.not.mandatory,error)
    if (error) return
    !
    call close%as%parse(line,user%identifier,error)
    if (error) return
    !
  end subroutine cubeedit_close_parse
  !
  subroutine cubeedit_close_main(close,user,error)
    use cubeedit_cube_buffer
    !-------------------------------------------------------------------
    ! Main routine for command CLOSE
    !-------------------------------------------------------------------
    class(close_comm_t), intent(in)    :: close
    type(close_user_t),  intent(in)    :: user
    logical,             intent(inout) :: error
    !
    character(len=base_l) :: newname
    character(len=*), parameter :: rname='CLOSE>MAIN'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    if (user%identifier%do) then
       newname = user%identifier%identifier
    else
       call cubeedit_bufferlist_get_buffer_name(user%edid,newname,error)
       if (error) return
    endif
    call cubeedit_bufferlist_close(user%edid,newname,error)
    if (error) return
  end subroutine cubeedit_close_main
end module cubeedit_close
