!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeedit_get
  use cubetools_parameters
  use cubetools_structure
  use cubetools_userspace
  use cube_types
  use cubeedit_messaging
  use cubeedit_getdata
  use cubeedit_getheader
  !
  public :: get
  private
  !
  type :: get_comm_t
     type(option_t), pointer :: comm
     type(userspace_opt_t)   :: into
     type(getdata_comm_t)    :: data
     type(getheader_comm_t)  :: head
   contains
     procedure, public  :: register => cubeedit_get_register
     procedure, private :: parse    => cubeedit_get_parse
     procedure, private :: main     => cubeedit_get_main 
  end type get_comm_t
  type(get_comm_t) :: get
  !
  type :: get_user_t
     character(len=varn_l)  :: edid 
     type(userspace_user_t) :: userspace
     type(getdata_user_t)   :: data
     type(getheader_user_t) :: head
  end type get_user_t
  !
  character(len=*), parameter :: defname = 'EDITGET'
  !
contains
  !
  subroutine cubeedit_get_command(line,error)
    use cubeedit_cube_buffer
    !-------------------------------------------------------------------
    ! Support routine for command GET
    ! -------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(get_user_t) :: user
    character(len=*), parameter :: rname='GET>COMMAND'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call get%parse(line,user,error)
    if (error) return
    call get%main(user,error)
    if (error) return
  end subroutine cubeedit_get_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubeedit_get_register(get,error)
    !-------------------------------------------------------------------
    ! Register GO\GET command and its options
    !-------------------------------------------------------------------
    class(get_comm_t), intent(inout) :: get
    logical,           intent(inout) :: error
    !
    type(standard_arg_t) :: stdarg
    logical, parameter :: iterator = .true.
    character(len=*), parameter :: comm_abstract = &
         'Get data or header information from a cube in editing mode'
    character(len=*), parameter :: comm_help = &
         'Values gotten onto variables are intended for information.&
         & To change the values in a opened cube command EDIT\PUT &
         & must be used. If option /INTO is not used the information&
         & gotten will be stored in a master structure called '//defname
    character(len=*), parameter :: rname='GET>REGISTER'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'GET','[newid]',&
         comm_abstract,&
         comm_help,&
         cubeedit_get_command,&
         get%comm,error)
    if (error) return
    call stdarg%register(&
         'NewId', &
         'Editing Identifier', &
         strg_id,&
         code_arg_optional, error)
    if (error) return
    !
    call get%into%register(error)
    if (error) return
    !
    call get%head%register(defname,get%into,error)
    if (error) return
    !
    call get%data%register(get%into,access_is_fixed,iterator,error)
    if (error) return
    !
  end subroutine cubeedit_get_register
  !
  subroutine cubeedit_get_parse(get,line,user,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    class(get_comm_t), intent(in)    :: get
    character(len=*),  intent(in)    :: line
    type(get_user_t),  intent(out)   :: user
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='GET>PARSE'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call get%into%parse(line,user%userspace,error)
    if (error) return
    user%edid = strg_star
    call cubetools_getarg(line,get%comm,1,user%edid,.not.mandatory,error)
    if (error) return
    !
    call get%head%parse(line,user%userspace,user%head,error)
    if (error) return
    call get%data%parse(line,user%userspace,user%data,error)
    if (error) return
  end subroutine cubeedit_get_parse
  !
  subroutine cubeedit_get_main(get,user,error)
    use cubeedit_cube_buffer
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(get_comm_t), intent(in)    :: get
    type(get_user_t),  intent(in)    :: user
    logical,           intent(inout) :: error
    !
    type(cube_t), pointer :: cube
    integer(kind=entr_k) :: ie
    character(len=*), parameter :: rname='GET>MAIN'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    if (.not.user%head%do .and. .not.user%data%do) then
       call get%head%list(error)
       if (error) return
       call get%data%list(error)
       if (error) return
       return
    endif
    !
    call cubeedit_bufferlist_get_cube_pointer(user%edid,cube,error)
    if (error) return   
    if (user%head%do) then
       call get%head%get(user%head,cube,defname,user%data%do,error)
       if (error) return
    endif
    if (user%data%do) then
       call cubeedit_bufferlist_get_cube_entry(user%edid,ie,error)
       if (error) return   
       call get%data%get(user%data,cube,defname,user%head%do,ie,error)
       if (error) return
       call cubeedit_bufferlist_set_cube_entry(user%edid,ie,error)
       if (error) return   
    endif
  end subroutine cubeedit_get_main
  !
end module cubeedit_get
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
