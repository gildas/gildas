module cubeedit_abort
  use cubetools_parameters
  use cubetools_structure
  use cubeedit_messaging
  !
  public :: abort
  private
  !
  type :: abort_comm_t
     type(option_t), pointer :: comm
   contains
     procedure, public  :: register => cubeedit_abort_register
     procedure, private :: parse    => cubeedit_abort_parse
     procedure, private :: main     => cubeedit_abort_main
  end type abort_comm_t
  type(abort_comm_t) :: abort
  !
  type abort_user_t
     character(len=varn_l) :: edid
  end type abort_user_t
  !
contains
  !
  subroutine cubeedit_abort_command(line,error)
    !-------------------------------------------------------------------
    ! Support routine for command ABORT
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(abort_user_t) :: user
    character(len=*), parameter :: rname='ABORT>COMMAND'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call abort%parse(line,user,error)
    if (error) return
    call abort%main(user,error)
    if (error) return
  end subroutine cubeedit_abort_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubeedit_abort_register(abort,error)
    !-------------------------------------------------------------------
    ! Register EDIT\ABORT and its options
    !-------------------------------------------------------------------
    class(abort_comm_t), intent(inout) :: abort
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: comm_abstract = &
         'Abort the editing of a cube'
    character(len=*), parameter :: comm_help = strg_id
    !
    type(standard_arg_t) :: stdarg
    character(len=*), parameter :: rname='ABORT>REGISTER'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'ABORT','[newid]',&
         comm_abstract,&
         comm_help,&
         cubeedit_abort_command,&
         abort%comm,error)
    if (error) return
    call stdarg%register(&
         'NewId', &
         'Editing Identifier', &
         strg_id,&
         code_arg_optional, error)
    if (error) return
    !
  end subroutine cubeedit_abort_register
  !
  subroutine cubeedit_abort_parse(abort,line,user,error)
    !-------------------------------------------------------------------
    ! Parse routine for command ABORT
    !-------------------------------------------------------------------
    class(abort_comm_t), intent(in)    :: abort
    character(len=*),    intent(in)    :: line
    type(abort_user_t),  intent(out)   :: user
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='ABORT>PARSE'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    user%edid = strg_star
    call cubetools_getarg(line,abort%comm,1,user%edid,.not.mandatory,error)
    if (error) return
    !
  end subroutine cubeedit_abort_parse
  !
  subroutine cubeedit_abort_main(abort,user,error)
    use cubeedit_cube_buffer
    !-------------------------------------------------------------------
    ! Main routine for command ABORT
    !-------------------------------------------------------------------
    class(abort_comm_t), intent(in)    :: abort
    type(abort_user_t),  intent(in)    :: user
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='ABORT>MAIN'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubeedit_bufferlist_abort(user%edid,error)
    if (error) return
  end subroutine cubeedit_abort_main
end module cubeedit_abort
