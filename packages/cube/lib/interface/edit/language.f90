!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeedit_language
  use cubetools_structure
  !
  use cubeedit_messaging
  use cubeedit_abort
  use cubeedit_copy
  use cubeedit_close
  use cubeedit_dump
  use cubeedit_get
  use cubeedit_glimpse
  use cubeedit_open
  use cubeedit_put
  use cubeedit_split
  !
  public :: cubeedit_register_language
  private
  !
  integer(kind=lang_k) :: langid
  !
contains
  !
  subroutine cubeedit_register_language(error)
    !----------------------------------------------------------------------
    ! Register the EDIT\ language
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    call cubetools_register_language('EDIT',&
         'J.Pety, S.Bardeau, V.deSouzaMagalhaes',&
         'Commands to edit a cube',&
         'gag_doc:hlp/cube-help-edit.hlp',&
         cubeedit_execute_command,langid,error)
    if (error) return
    !
    call abort%register(error)
    if (error) return
    call copy%register(error)
    if (error) return
    call close%register(error)
    if (error) return
    call dump%register(error)
    if (error) return
    call get%register(error)
    if (error) return
    call glimpse%register(error)
    if (error) return
    call open%register(error)
    if (error) return
    call put%register(error)
    if (error) return
    call split%register(error)
    if (error) return
    !
    call cubetools_register_dict(error)
    if (error) return
  end subroutine cubeedit_register_language
  !
  subroutine cubeedit_execute_command(line,comm,error)
    use cubetools_parameters
    use cubeadm_opened
    use cubeadm_timing
    !----------------------------------------------------------------------
    ! Execute a command of the EDIT\ language
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    character(len=*), intent(in)    :: comm
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='EXECUTE>COMMAND'
    !
    error = .false.
    if (comm.eq.'EDIT?') then
       call cubetools_list_language_commands(langid,error)
       if (error) return
    else
       call cubeadm_timing_init()
       call cubetools_execute_command(line,langid,comm,error)
       if (error) continue ! To ensure error recovery in the call to cubeadm_finish_all
       call cubeadm_finish_all(comm,line,error)
       if (error) continue ! To ensure error recovery in timing
       call cubeadm_timing_final(comm)
       if (error) return
    endif
  end subroutine cubeedit_execute_command
end module cubeedit_language
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
