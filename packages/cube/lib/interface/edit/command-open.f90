module cubeedit_open
  use cubetools_parameters
  use cubetools_structure
  use cubetools_keywordlist_types
  use cubetools_access_types
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubeedit_messaging
  use cubeedit_cube_buffer
  !
  public :: open
  private
  !
  type :: open_comm_t
     type(option_t),      pointer :: comm
     type(option_t),      pointer :: like
     type(cubeid_arg_t),  pointer :: like_arg
     type(order_comm_t)           :: access
     type(option_t),      pointer :: dims
     type(option_t),      pointer :: init
     type(keywordlist_comm_t), pointer :: init_arg
     type(cube_prod_t),   pointer :: buffer
   contains
     procedure, public  :: register => cubeedit_open_register
     procedure, private :: parse    => cubeedit_open_parse 
     procedure, private :: main     => cubeedit_open_main  
  end type open_comm_t
  type(open_comm_t) :: open
  !
  type open_user_t
     character(len=varn_l) :: edid
     type(order_user_t)    :: access
     character(len=argu_l) :: init
     type(cubeid_user_t)   :: likeid
     logical               :: dodims
     logical               :: dolike
     logical               :: doinit
     logical               :: doshow
     integer(kind=entr_k)  :: dims(3) = undetermined_dim
  end type open_user_t
  !
  type open_prog_t
    type(order_prog_t) :: access
  end type open_prog_t
  !
contains
  !
  subroutine cubeedit_open_command(line,error)
    use cubeedit_cube_buffer
    !-------------------------------------------------------------------
    ! Support routine for command OPEN
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(open_user_t) :: user
    character(len=*), parameter :: rname='OPEN>COMMAND'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call open%parse(line,user,error)
    if (error) return
    if (user%doshow) then
        call cubeedit_bufferlist_list(error)
        if (error) return
    else
       call open%main(user,error)
       if (error) return
    endif
  end subroutine cubeedit_open_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubeedit_open_register(open,error)
    use cubedag_allflags
    !-------------------------------------------------------------------
    ! Register EDIT\OPEN and its options
    !-------------------------------------------------------------------
    class(open_comm_t), intent(inout) :: open
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: comm_abstract = &
         'Open a cube for editing'
    character(len=*), parameter :: comm_help = &
         'Open a cube for editing under an editing identifier. If no&
         & identifier is given, list the currently open cubes.&
         & WARNING: there are no checks on the data or header&
         & modifications being done, proceed with care. If the header&
         & needs to be modified in a coherent way, please refer to&
         & command CUBE\MODIFY.'
    character(len=*), parameter :: keyvalues(2) = ['NaN ','COPY'] ! We could also add +- inf
    !
    type(standard_arg_t) :: stdarg
    type(cubeid_arg_t)   :: cubearg
    type(keywordlist_comm_t)  :: keyarg
    type(cube_prod_t) :: oucube
    character(len=*), parameter :: rname='OPEN>REGISTER'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'OPEN','newid',&
         comm_abstract,&
         comm_help,&
         cubeedit_open_command,&
         open%comm,error)
    if (error) return
    call stdarg%register(&
         'NewId', &
         'Editing Identifier', &
         strg_id,&
         code_arg_optional,error)
    if (error) return
    !
    call cubetools_register_option(&
         'LIKE','CUBE',&
         'Open new cube like a previous cube',&
         strg_id,&
         open%like,error)
    if (error) return
    call cubearg%register( &
         'CUBE', &
         'Cube to be opened for edition',  &
         strg_id,&
         code_arg_optional,  &
         [flag_any], &
         code_read_head, &
         code_access_imaset_or_speset, &
         open%like_arg, &
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'DIMS','nl nm nc',&
         'Define the dimensions of the new cube',&
         'If /DIMS and /LIKE are used the new dimensions are assumed&
         & to be centered on the references of the reference cube',&
         & open%dims, error)
    if (error) return
    call stdarg%register(&
         'nl', &
         'Number of pixels in L axis', &
         strg_id,&
         code_arg_mandatory, error)
    if (error) return
    call stdarg%register(&
         'nm', &
         'Number of pixels in M axis', &
         strg_id,&
         code_arg_optional, error)
    if (error) return
    call stdarg%register(&
         'nc', &
         'Number of channels', &
         strg_id,&
         code_arg_optional, error)
    if (error) return   
    !
    call cubetools_register_option(&
         'INIT','value',&
         'Define an initial value for the opened CUBE',&
         'If option is ommited the default value is NaN. Copy is only&
         & allowed if option /LIKE is present and option /DIMS is not&
         & present.',&
         open%init,error)
    if (error) return
    call keyarg%register( &
         'value',  &
         'Initial value', &
         strg_id,&
         code_arg_mandatory, &
         keyvalues,  &
         flexible,  &
         open%init_arg,  &
         error)
    if (error) return
    !
    call open%access%register(&
         'ACCESS',&
         'Define the access for the cube to be opened',&
         'Default access is image',&
         error)
    if (error) return
    !
    ! Product
    call oucube%register(&
         'BUFFER',&
         'The new cube buffer',&
         strg_id,&
         [flag_edit],&
         open%buffer,&
         error,&
         flagmode=keep_all)
    if (error) return
  end subroutine cubeedit_open_register
  !
  subroutine cubeedit_open_parse(open,line,user,error)
    !-------------------------------------------------------------------
    ! Parse routine for command OPEN
    !-------------------------------------------------------------------
    class(open_comm_t), intent(in)    :: open
    character(len=*),   intent(in)    :: line
    type(open_user_t),  intent(out)   :: user
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: rname='OPEN>PARSE'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call open%dims%present(line,user%dodims,error)
    if (error) return
    call open%like%present(line,user%dolike,error)
    if (error) return
    call open%init%present(line,user%doinit,error)
    if (error) return
    !
    user%doshow = open%comm%getnarg().eq.0
    if (user%doshow) return
    !
    call cubetools_getarg(line,open%comm,1,user%edid,mandatory,error)
    if (error) return
    !
    if (user%dodims) then
       call cubetools_getarg(line,open%dims,1,user%dims(1),mandatory,error)
       if (error) return
       call cubetools_getarg(line,open%dims,2,user%dims(2),mandatory,error)
       if (error) return
       call cubetools_getarg(line,open%dims,3,user%dims(3),mandatory,error)
       if (error) return
    endif
    !
    if (user%dolike) then
       call cubeadm_cubeid_parse(line,open%like,user%likeid,error)
       if (error) return
    else
       if (.not.user%dodims) then
          call cubeedit_message(seve%e,rname,'Dimensions must be specified when creating a cube from scratch')
          error =  .true.
          return
       endif
    endif
    !
    call open%access%parse(line,user%access,error)
    if (error)  return
    !
    if (user%doinit) then
       call cubetools_getarg(line,open%init,1,user%init,mandatory,error)
       if (error) return
    else
       user%init = strg_star
    endif
  end subroutine cubeedit_open_parse
  !
  subroutine cubeedit_open_main(open,user,error)
    use cubetools_user2prog
    use cubetools_nan
    use cubetools_unit
    use cubeedit_cube_buffer
    !-------------------------------------------------------------------
    ! Main routine for command OPEN
    !-------------------------------------------------------------------
    class(open_comm_t), intent(in)    :: open
    type(open_user_t),  intent(in)    :: user
    logical,            intent(inout) :: error
    !
    integer(kind=4) :: ikey
    type(open_prog_t) :: prog
    character(len=argu_l) :: argu
    type(unit_user_t) :: nounit
    real(kind=sign_k) :: val
    logical :: doinit
    character(len=*), parameter :: rname='OPEN>MAIN'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call user%access%toprog(open%access,code_access_imaset,prog%access,error)
    if (error)  return
    !
    if (user%doinit) then
       call nounit%get_from_code(code_unit_unk,error)
       if (error) return
       call cubetools_keywordlist_user2prog(open%init_arg,user%init,ikey,argu,error)
       if (error) return
       if (argu.eq.strg_unresolved) then
          call cubetools_user2prog_resolve_star(user%init,nounit,gr4nan,val,error)
          if (error) return
          doinit = .true.
       else
          select case(argu)
          case('NAN')
             val = gr4nan
             doinit = .true.
          case('COPY')
             if (.not.user%dolike) then
                call cubeedit_message(seve%e,rname,'Cannot copy values if cube is created from scratch')
                error =  .true.
                return
             endif
             if (user%dodims) then
                call cubeedit_message(seve%e,rname,'Cannot copy values if dimensions have changed')
                error =  .true.
                return
             endif
             doinit = .false.
          case default
             call cubeedit_message(seve%e,rname,"Unknown key value: "//argu)
             error =  .true.
             return
          end select
       endif
    else
       doinit = .true.
       val    = gr4nan
    endif
    !
    if (user%dolike) then
       call cubeedit_bufferlist_open(user%edid,open%buffer,open%like_arg,user%likeid,  &
         prog%access%code,user%dims,doinit,val,error)
       if (error) return
    else
       call cubeedit_bufferlist_create(user%edid,prog%access%code,user%dims,val,error)
       if (error) return
    endif
  end subroutine cubeedit_open_main
end module cubeedit_open
