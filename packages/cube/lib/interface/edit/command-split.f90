!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeedit_split
  use cubetools_parameters
  use cube_types
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubeedit_messaging
  !
  public :: split
  private
  !
  integer(kind=chan_k), parameter :: one = 1
  !
  type :: split_comm_t
     type(option_t),     pointer :: comm
     type(cubeid_arg_t), pointer :: incube
     type(cube_prod_t),  pointer :: oucube  ! Same for all
   contains
     procedure, public  :: register => cubeedit_split_register
     procedure, private :: parse    => cubeedit_split_parse
     procedure, private :: main     => cubeedit_split_main
  end type split_comm_t
  type(split_comm_t) :: split
  !
  type split_user_t
     type(cubeid_user_t)  :: cubeids
   contains
     procedure, private :: toprog => cubeedit_split_user_toprog
  end type split_user_t
  !
  type cube_p_t
     type(cube_t), pointer :: p
  end type cube_p_t
  type split_prog_t
     type(cube_t),   pointer     :: incube     ! Input cube
     type(cube_p_t), allocatable :: oucube(:)  ! Output cubes
     integer(kind=chan_k)        :: nc
   contains
     procedure, private :: header => cubeedit_split_prog_header
     procedure, private :: data   => cubeedit_split_prog_data
     procedure, private :: loop   => cubeedit_split_prog_loop
     procedure, private :: act    => cubeedit_split_prog_act
  end type split_prog_t
  !
contains
  !
  subroutine cubeedit_split_command(line,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(split_user_t) :: user
    character(len=*), parameter :: rname='SPLIT>COMMAND'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    call split%parse(line,user,error)
    if (error) return
    call split%main(user,error)
    if (error) return
  end subroutine cubeedit_split_command
  !
  !---------------------------------------------------------------------
  !
  subroutine cubeedit_split_register(split,error)
    use cubedag_allflags
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(split_comm_t), intent(inout) :: split
    logical,             intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    type(cube_prod_t) :: oucube
    !
    character(len=*), parameter :: comm_abstract = &
         'Split a cube into individual images'
    character(len=*), parameter :: comm_help = &
         'Split a cube in its frequency-velocity axis into individual&
         & images.'//strg_cr//strg_cr//&
         'WARNING: This command is a work around for complicated&
         & cases when complex set of images cannot be easily used&
         & in CUBE'
    character(len=*), parameter :: rname='SPLIT>REGISTER'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'SPLIT','[cube]',&
         comm_abstract,&
         comm_help,&
         cubeedit_split_command,&
         split%comm,error)
    if (error) return
    call cubearg%register( &
         'CUBE', &
         'Signal cube',  &
         strg_id,&
         code_arg_optional,  &
         [flag_cube], &
         code_read, &
         code_access_imaset, &
         split%incube, &
         error)
    if (error) return
    !
    ! Product (register only one)
    call oucube%register(&
         'IMAGE',&
         'The extracted images (as many as needed)',&
         strg_id,&
         [flag_image],&
         split%oucube,&
         error)
    if (error) return
  end subroutine cubeedit_split_register
  !
  subroutine cubeedit_split_parse(split,line,user,error)
    !-------------------------------------------------------------------
    ! SPLIT cubname
    ! /RANGE vfirst vlast
    ! /FREQUENCY newname newrestfreq [unit]
    ! /CENTER xcen ycen ! can be relative[arcsec] or absolute[RA,DEC or LII,BII]
    ! /SIZE sx [sy]
    !-------------------------------------------------------------------
    class(split_comm_t), intent(in)    :: split
    character(len=*),      intent(in)    :: line
    type(split_user_t),  intent(out)   :: user
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPLIT>PARSE'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,split%comm,user%cubeids,error)
    if (error) return
  end subroutine cubeedit_split_parse
  !
  subroutine cubeedit_split_main(split,user,error)
    use cubeadm_timing
    use cubeadm_get
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(split_comm_t), intent(in)    :: split
    type(split_user_t),  intent(in)    :: user
    logical,             intent(inout) :: error
    !
    type(split_prog_t) :: prog
    character(len=*), parameter :: rname='SPLIT>MAIN'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call user%toprog(prog,error)
    if (error) return
    call prog%header(split,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubeedit_split_main
  !
  !---------------------------------------------------------------------
  !
  subroutine cubeedit_split_user_toprog(user,prog,error)
    use cubetools_header_methods
    use cubeadm_get
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(split_user_t), intent(in)    :: user
    type(split_prog_t),  intent(out)   :: prog
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPLIT>USER>TOPROG'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(split%incube,user%cubeids,  &
         prog%incube,error)
    if (error) return
    !
    call cubetools_header_get_nchan(prog%incube%head,prog%nc,error)
    if (error) return
    !
  end subroutine cubeedit_split_user_toprog
  !
  !---------------------------------------------------------------------
  !
  subroutine cubeedit_split_prog_header(prog,comm,error)
    use gkernel_interfaces
    use cubetools_header_methods
    use cubeadm_clone
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(split_prog_t), intent(inout) :: prog
    type(split_comm_t),  intent(in)    :: comm
    logical,             intent(inout) :: error
    !
    integer(kind=4) :: ier
    integer(kind=chan_k) :: ic
    character(len=*), parameter :: rname='SPLIT>PROG>HEADER'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    allocate(prog%oucube(prog%nc),stat=ier)
    if (failed_allocate(rname,'cube array',ier,error)) return
    do ic=1, prog%nc
       call cubeadm_clone_header(comm%oucube,prog%incube,prog%oucube(ic)%p,error)
       if (error) return
       call cubetools_header_put_nchan(one,prog%oucube(ic)%p%head,error)
       if (error) return
    enddo
  end subroutine cubeedit_split_prog_header
  !
  subroutine cubeedit_split_prog_data(prog,error)
    use cubeadm_opened
    use cubetools_header_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(split_prog_t), intent(inout) :: prog
    logical,               intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='SPLIT>PROG>DATA'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,error)
    if (error) return
    !
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) then
          call prog%loop(iter,error)
       endif
       !$OMP END TASK
    enddo ! ie
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubeedit_split_prog_data
  !
  subroutine cubeedit_split_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_image_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(split_prog_t),      intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(image_t) :: image
    character(len=*), parameter :: rname='SPLIT>PROG>LOOP'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    call image%associate('image',prog%incube,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter%ie,image,error)
      if (error) return
    enddo ! ie
  end subroutine cubeedit_split_prog_loop
  !
  subroutine cubeedit_split_prog_act(prog,ie,image,error)
    use cubetools_nan
    use cubeadm_taskloop
    use cubeadm_image_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(split_prog_t),   intent(inout) :: prog
    integer(kind=entr_k),  intent(in)    :: ie
    type(image_t),         intent(inout) :: image
    logical,               intent(inout) :: error
    !
    integer(kind=entr_k), parameter :: one=1
    character(len=*), parameter :: rname='SPLIT>PROG>ACT'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    ! Get the ie-th image
    call image%get(ie,error)
    if (error) return
    ! Put it as first image in the ie-th cube
    call image%put_in(prog%oucube(ie)%p,one,error)
    if (error) return
  end subroutine cubeedit_split_prog_act
end module cubeedit_split
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
