!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeedit_which_data
  use cubetools_parameters
  use cubetools_structure
  use cubetools_keywordlist_types
  use cubeedit_messaging
  !
  public :: which_comm_t,which_user_t
  private
  !
  type which_comm_t
     type(option_t),      pointer :: opt      
     type(keywordlist_comm_t), pointer :: access
     logical                      :: fixed
     logical                      :: active
   contains
     procedure :: register  => cubeedit_which_data_register
     procedure :: parse     => cubeedit_which_data_parse
     procedure :: user2prog => cubeedit_which_data_user2prog
  end type which_comm_t
  !
  type which_user_t
     character(len=argu_l) :: access
     logical               :: do
  end type which_user_t
  !
contains
  !
  subroutine cubeedit_which_data_register(opt,name,abstract,fixed,active,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(which_comm_t), intent(out)   :: opt
    character(len=*),    intent(in)    :: name
    character(len=*),    intent(in)    :: abstract
    logical,             intent(in)    :: fixed
    logical,             intent(in)    :: active
    logical,             intent(inout) :: error
    !
    character(len=32) :: syntax
    type(keywordlist_comm_t) :: keyarg
    character(len=*), parameter :: accesses(2) = (/ 'IMAGE   ','SPECTRUM' /)
    character(len=*), parameter :: rname='WHICH>DATA>REGISTER'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    opt%active = active
    if (.not.active) return
    opt%fixed = fixed 
    if (fixed) then
       syntax = ''
    else
       syntax = '[Access]'
    endif
    call cubetools_register_option(&
         name,syntax,&
         abstract,&
         strg_id,&
         opt%opt,&
         error)
    if (error) return
    if (.not.fixed) then
       call keyarg%register(&
            'Access',&
            'In which order the cube should be accessed',&
            strg_id,&
            code_arg_optional,&
            accesses,&
            .not.flexible,&
            opt%access,&
            error)
       if (error) return
    endif
  end subroutine cubeedit_which_data_register
  !
  subroutine cubeedit_which_data_parse(opt,line,user,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(which_comm_t), intent(in)    :: opt
    character(len=*),    intent(in)    :: line
    type(which_user_t),  intent(out)   :: user
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='WHICH>DATA>PARSE'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    if (opt%active) then
       call opt%opt%present(line,user%do,error)
       if (error) return
       if (user%do.and..not.opt%fixed) then
          user%access = 'IMAGE'
          call cubetools_getarg(line,opt%opt,1,user%access,.not.mandatory,error)
          if (error) return
       else
          user%access = 'FIXED'
       endif
    else
       user%do = .false.
    endif
    !
  end subroutine cubeedit_which_data_parse
  !
  subroutine cubeedit_which_data_user2prog(opt,user,doaccess,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(which_comm_t),  intent(in)    :: opt
    type(which_user_t),   intent(in)    :: user
    integer(kind=code_k), intent(out)   :: doaccess
    logical,              intent(inout) :: error
    !
    character(len=8) :: access
    integer(kind=4) :: ikey
    character(len=*), parameter :: rname='WHICH>DATA>USER2PROG'
    !
    call cubeedit_message(edseve%trace,rname,'Welcome')
    !
    !
    call cubetools_keywordlist_user2prog(opt%access,user%access,ikey,access,error)
    if (error) return
    !
    select case (access)
    case('IMAGE')
      doaccess = code_access_imaset
    case('SPECTRUM')
      doaccess = code_access_speset
    case default
      call cubeedit_message(seve%e,rname,'Unsupported access mode: '//trim(user%access))
      error = .true.
      return
    end select
  end subroutine cubeedit_which_data_user2prog
end module cubeedit_which_data
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
