module cubeadm_taskloop_iteration
  use cubetools_parameters
  !
  ! Description of one task iteration
  type taskloop_iteration_t
    integer(kind=entr_k) :: num         ! This task number
    integer(kind=entr_k) :: ie          ! The entry number iterated in this task
    integer(kind=data_k) :: firstdata   ! First data  to be processed
    integer(kind=data_k) :: lastdata    ! Last  data  to be processed
    integer(kind=entr_k) :: firstplane  ! First plane to be processed
    integer(kind=entr_k) :: lastplane   ! Last  plane to be processed
    integer(kind=entr_k) :: first       ! First entry to be processed (kept for backward compatibility)
    integer(kind=entr_k) :: last        ! Last  entry to be processed (kept for backward compatibility)
  end type taskloop_iteration_t
  !
  public :: taskloop_iteration_t
  private
  !
end module cubeadm_taskloop_iteration
