!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeadm_touch
  use cube_types
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_messaging
  !
  public :: touch
  private
  !
  type :: touch_comm_t
     type(option_t),     pointer :: comm
     type(cubeid_arg_t), pointer :: cube
   contains
     procedure, public  :: register => cubeadm_touch_register
     procedure, private :: parse    => cubeadm_touch_parse
     procedure, private :: main     => cubeadm_touch_main
  end type touch_comm_t
  type(touch_comm_t) :: touch
  !
  type touch_user_t
     type(cubeid_user_t) :: cubeids
   contains
     procedure, private :: toprog => cubeadm_touch_user_toprog
  end type touch_user_t
  !
  type touch_prog_t
     type(cube_t), pointer :: cube
   contains
     procedure, private :: data => cubeadm_touch_prog_data
  end type touch_prog_t
  !
contains
  !
  subroutine cubeadm_touch_command(line,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(touch_user_t) :: user
    character(len=*), parameter :: rname='TOUCH>COMMAND'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call touch%parse(line,user,error)
    if (error) return
    call touch%main(user,error)
    if (error) continue
  end subroutine cubeadm_touch_command
  !
  subroutine cubeadm_touch_register(comm,error)
    use cubedag_allflags
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(touch_comm_t), intent(inout) :: comm
    logical,             intent(inout) :: error
    !
    type(cubeid_arg_t) :: cube
    character(len=*), parameter :: comm_abstract = &
      'Touch the file so that its data is pre-loaded in memory'
    character(len=*), parameter :: comm_help = &
      'This makes sense only if input files are accessed in memory mode'
    character(len=*), parameter :: rname='TOUCH>REGISTER'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    ! Syntax
    call cubetools_register_command(&
         'TOUCH','[CubeId]',&
         comm_abstract,&
         comm_help,&
         cubeadm_touch_command,&
         comm%comm,&
         error)
    if (error) return
    call cube%register(&
         'CUBE',&
         'Any cube',&
         strg_id,&
         code_arg_optional,&
         [flag_any],&
         code_read,&
         code_access_subset,&  ! An access with no enforced order
         comm%cube,&
         error)
    if (error) return
  end subroutine cubeadm_touch_register
  !
  subroutine cubeadm_touch_parse(comm,line,user,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(touch_comm_t), intent(in)    :: comm
    character(len=*),    intent(in)    :: line
    type(touch_user_t),  intent(out)   :: user
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='TOUCH>PARSE'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
  end subroutine cubeadm_touch_parse
  !
  subroutine cubeadm_touch_main(comm,user,error)
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(touch_comm_t), intent(in)    :: comm
    type(touch_user_t),  intent(inout) :: user
    logical,             intent(inout) :: error
    !
    type(touch_prog_t) :: prog
    character(len=*), parameter :: rname='TOUCH>MAIN'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubeadm_touch_main
  !
  subroutine cubeadm_touch_user_toprog(user,comm,prog,error)
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(touch_user_t), intent(in)    :: user
    type(touch_comm_t),  intent(in)    :: comm
    type(touch_prog_t),  intent(out)   :: prog
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='TOUCH>USER>TOPROG'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%cube,user%cubeids,prog%cube,error)
    if (error) return
  end subroutine cubeadm_touch_user_toprog
  !
  subroutine cubeadm_touch_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(touch_prog_t), intent(inout) :: prog
    logical,             intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    logical :: status
    character(len=*), parameter :: rname='TOUCH>PROG>DATA'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,error)
    if (error) return
    ! Touch is done by preparing a first iteration (i.e. prepare
    ! the appropriate buffers, namely load the cube in memory)
    status = cubeadm_dataiterate_all(iter,error)
    if (error)  return
  end subroutine cubeadm_touch_prog_data
  !
end module cubeadm_touch
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
