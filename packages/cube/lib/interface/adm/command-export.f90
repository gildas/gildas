!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeadm_export
  use cube_types
  use cubetools_parameters
  use cubetools_structure
  use cubetools_access_types
  use cubetools_dataformat_types
  use cubetools_keywordlist_types
  use cubetools_switch_types
  use cubeadm_messaging
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubeadm_identifier
  !
  public :: export
  private
  !
  type :: export_comm_t
     type(option_t),           pointer :: comm
     type(cubeid_arg_t),       pointer :: cube
     type(option_t),           pointer :: format
     type(keywordlist_comm_t), pointer :: format_arg
     type(option_t),           pointer :: blanking
     type(option_t),           pointer :: name
     type(identifier_opt_t)            :: as
     type(option_t),           pointer :: dir
     type(order_comm_t)                :: access
     type(cube_prod_t),        pointer :: exported
     type(switch_comm_t)               :: checksum
   contains
     procedure, public  :: register => cubeadm_export_register
     procedure, private :: parse    => cubeadm_export_parse
     procedure, private :: adm     => cubeadm_export_adm
  end type export_comm_t
  type(export_comm_t) :: export
  !
  type export_user_t
     type(cubeid_user_t)     :: cubeids
     type(identifier_user_t) :: identifier
     integer(kind=code_k)    :: format
     logical                 :: doblank
     logical                 :: dodir
     logical                 :: dofilename
     real(kind=sign_k)       :: bval=-1000.
     real(kind=sign_k)       :: eval=0.
     character(len=file_l)   :: filename
     character(len=file_l)   :: dir
     type(order_user_t)      :: access
     type(switch_user_t)     :: checksum
   contains
     procedure, private :: toprog => cubeadm_export_user_toprog
  end type export_user_t
  !
  type export_prog_t
     type(cubeid_prog_t)     :: incube
     character(len=file_l)   :: name
     integer(kind=code_k)    :: format
     logical                 :: doblank
     type(identifier_prog_t) :: identifier
     real(kind=sign_k)       :: bval,eval
     type(order_prog_t)      :: access
     type(switch_prog_t)     :: checksum
     type(cube_t), pointer   :: cube
     type(cube_t), pointer   :: exported
   contains
     procedure, private :: header => cubeadm_export_prog_header
     procedure, private :: data   => cubeadm_export_prog_data
  end type export_prog_t
  !
contains
  !
  subroutine cubeadm_export_command(line,error)
    !---------------------------------------------------------------------
    ! Support routine for command:
    !   EXPORT [CubeID] [/FORMAT FITS|GDF]
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(export_user_t) :: user
    character(len=*), parameter :: rname='EXPORT>COMMAND'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call export%parse(line,user,error)
    if (error) return
    call export%adm(user,error)
    if (error) return
  end subroutine cubeadm_export_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubeadm_export_register(export,error)
    use cubedag_allflags
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    class(export_comm_t), intent(inout) :: export
    logical,              intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    type(keywordlist_comm_t) :: keyarg
    type(standard_arg_t) :: stdarg
    type(cube_prod_t) :: oucube
    type(flag_t) :: noflag(0)
    integer(kind=4), parameter :: nformats=3
    character(len=*), parameter :: formats(nformats) =  &
         (/ 'FITS','GDF ','CDF ' /)
    character(len=*), parameter :: comm_abstract = &
         'Export reduced data'
    character(len=*), parameter :: comm_help = &
         'Export a cube to the RED (reduction) directory, by default&
         & the file name is based on its family name and flag. The&
         & option /NAME allows to customize the output file base name,&
         & and optionally the directory and the file extension.'
    character(len=*), parameter :: rname='EXPORT>REGISTER'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'EXPORT','[cube]',&
         comm_abstract,&
         comm_help,&
         cubeadm_export_command,&
         export%comm,error)
    if (error) return
    call cubearg%register( &
         'CUBE', &
         'Cube to be exported',  &
         strg_id,&
         code_arg_optional,  &
         [flag_any], &
         code_read, &
         code_access_imaset_or_speset, &
         export%cube, &
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'FORMAT','type',&
         'Set the data format to be used',&
         'Default is FITS if the option is omitted',&
         export%format,error)
    if (error) return
    call keyarg%register( &
         'type',  &
         'Format type', &
         strg_id,&
         code_arg_mandatory, &
         formats,  &
         .not.flexible,  &
         export%format_arg,  &
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'BLANKING','[Bval [Eval]]',&
         'Export with blank values instead of NaN (GDF only)',&
         strg_id,&
         export%blanking,error)
    if (error) return
    call stdarg%register( &
         'Bval',  &
         'Blanking value (default -1000)', &
         strg_id,&
         code_arg_optional, &
         error)
    if (error) return
    call stdarg%register( &
         'Eval',  &
         'Tolerance on blanking value (default 0)', &
         strg_id,&
         code_arg_optional, &
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'FILENAME','filename',&
         'Customize the name of exported file on disk',&
         'This option customizes the name of the file to be created on&
         & disk (use option /DIRECTORY to customize the target directory).&
         & The proper extension is implicitly added if missing, depending&
         & on the format to be used.'&
         & //strg_cr//strg_cr//&
         'The family name is not changed by this option. To change the&
         & family name use option /AS. If this option is not used the&
         & default file name on disk is: family-flag1-...-flagn',&
         export%name,error)
    if (error) return
    call stdarg%register( &
         'filename',  &
         'Customized file name', &
         strg_id,&
         code_arg_optional, &
         error)
    if (error) return
    !
    call export%as%register(&
         'Customize the identifier of the exported cube',&
         changeflags, error)
    if (error) return
    !
    call cubetools_register_option(&
         'DIRECTORY','dirname',&
         'Customize the export directory',&
         'If option is not given defaults to the default RED&
         & directory (set with command ADM\DIRECTORY)',&
         export%dir,error)
    if (error) return
    call stdarg%register(&
         'directory', &
         'Export directory', &
         strg_id,&
         code_arg_optional, error)
    if (error) return
    !
    call export%access%register(&
         'ACCESS',&
         'Set the access order of the output file',&
         'Default is IMAGE',&
         error)
    if (error)  return
    !
    call export%checksum%register(&
         'CHECKSUM','computation of the checksum during export','OFF',error)
    if (error) return
    !
    ! Product
    call oucube%register(&
         'EXPORTED',&
         'The exported cube',&
         strg_id,&
         noflag,&
         export%exported,&
         error,&
         access=code_access_subset,&
         flagmode=keep_all)
    if (error) return
  end subroutine cubeadm_export_register
  !
  subroutine cubeadm_export_parse(export,line,user,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(export_comm_t), intent(in)    :: export
    character(len=*),     intent(in)    :: line
    type(export_user_t),  intent(out)   :: user
    logical,              intent(inout) :: error
    !
    character(len=argu_l) :: argum
    character(len=12) :: key
    integer(kind=4) :: iformat
    character(len=*), parameter :: rname='EXPORT>PARSE'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,export%comm,user%cubeids,error)
    if (error) return
    !
    ! /FORMAT
    argum = 'FITS'  ! Default
    call cubetools_getarg(line,export%format,1,argum,.not.mandatory,error)
    if (error) return
    call cubetools_keywordlist_user2prog(export%format_arg,argum,iformat,key,error)
    if (error) return
    select case (key)
    case ('FITS')
      user%format = code_dataformat_fits
    case ('GDF')
      user%format = code_dataformat_gdf
    case ('CDF')
      user%format = code_dataformat_cdf
    end select
    !
    ! /BLANKING
    call export%blanking%present(line,user%doblank,error)
    if (error) return
    if (user%format.ne.code_dataformat_gdf .and. user%doblank) then
      call cubeadm_message(seve%e,rname,  &
        '/'//trim(export%blanking%name)//' is only relevant for GDF format')
      error = .true.
      return
    endif
    call cubetools_getarg(line,export%blanking,1,user%bval,.not.mandatory,error)
    if (error) return
    call cubetools_getarg(line,export%blanking,2,user%eval,.not.mandatory,error)
    if (error) return
    !
    ! /AS family[:flags]
    call export%as%parse(line,user%identifier,error)
    if (error) return
    !
    ! /DIRECTORY
    call export%dir%present(line,user%dodir,error)
    if (error) return
    if (user%dodir) then
       call cubetools_getarg(line,export%dir,1,user%dir,mandatory,error)
       if (error) return
    endif
    !
    ! /FILENAME
    call export%name%present(line,user%dofilename,error)
    if (error) return
    if (user%dofilename) then
       call cubetools_getarg(line,export%name,1,user%filename,mandatory,error)
       if (error) return
    endif    
    !
    ! /ACCESS
    call export%access%parse(line,user%access,error)
    if (error)  return
    !
    ! /CHECKSUM
    call export%checksum%parse(line,user%checksum,error)
    if (error)  return
  end subroutine cubeadm_export_parse
  !
  subroutine cubeadm_export_adm(comm,user,error)
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(export_comm_t), intent(in)    :: comm
    type(export_user_t),  intent(inout) :: user
    logical,              intent(inout) :: error
    !
    type(export_prog_t) :: prog
    character(len=*), parameter :: rname='EXPORT>ADM'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubeadm_export_adm
  !
  !----------------------------------------------------------------------
  !
  subroutine cubeadm_export_user_toprog(user,comm,prog,error)
    use cubeadm_directory_type
    use cubedag_flag
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(export_user_t), intent(in)    :: user
    class(export_comm_t), intent(in)    :: comm
    type(export_prog_t),  intent(inout) :: prog
    logical,              intent(inout) :: error
    !
    character(file_l):: dirname
    integer(kind=4) :: ldir,iflag
    type(flag_t), pointer :: flag
    character(len=*), parameter :: rname='EXPORT>USER>TOPROG'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    ! Default access for input cube is imaset (will be changed to subset after
    ! transposition, if relevant)
    call user%access%toprog(comm%access,code_access_imaset,prog%access,error)
    if (error)  return
    !
    ! Here cubeadm_get_header prepares the transposition if relevant.
    ! The cube to be used in the tuple is set to the imaset or speset
    ! at this stage, but in case of transposition its data is not yet
    ! ready.
    call cubeadm_get_header(comm%cube,user%cubeids,prog%cube,  &
      error,access=prog%access%code)
    if (error) return
    !
    prog%format  = user%format
    prog%doblank = user%doblank
    prog%bval    = user%bval
    prog%eval    = user%eval
    !
    call user%identifier%toprog(prog%cube,prog%identifier,error)
    if (error) return
    !
    if (user%dodir) then
       dirname = user%dir
       ldir = len_trim(dirname)
       if (dirname(ldir:ldir).ne.'/')  dirname(ldir+1:ldir+1) = '/'
    else
       dirname = dir%red
    endif
    call cubeadm_directory_create(dirname,error)
    if (error) return
    !
    if (user%dofilename) then
       prog%name = trim(dirname)//user%filename
    else
       prog%name = trim(dirname)//prog%identifier%family
       if (allocated(prog%identifier%flags)) then
          do iflag=1,size(prog%identifier%flags)
             prog%name = trim(prog%name)//prog%identifier%flags(iflag)%get_suffix()
          enddo
       else
          do iflag=1,prog%cube%node%flag%n
             flag => cubedag_flag_ptr(prog%cube%node%flag%list(iflag)%p,error)
             if (error) return
             prog%name = trim(prog%name)//flag%get_suffix()
          enddo
       endif
    endif
    !
    call user%checksum%toprog(comm%checksum,prog%checksum,error)
    if (error) return
  end subroutine cubeadm_export_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubeadm_export_prog_header(prog,comm,error)
    use cubeadm_clone
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(export_prog_t), intent(inout) :: prog
    type(export_comm_t),  intent(in)    :: comm
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='EXPORT>PROG>HEADER'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    ! NB: access is defined by the cubeprod access at register time
    !     (i.e. code_access_subset)
    call cubeadm_clone_header(comm%exported,prog%cube,prog%exported,error)
    if (error) return
    !
    call prog%identifier%apply(prog%exported,error)
    if (error) return
  end subroutine cubeadm_export_prog_header
  !
  subroutine cubeadm_export_prog_data(prog,error)
    use cubeio_cube_define
    use cubeio_file
    use cubedag_parameters
    use cubetuple_transpose
    use cubeadm_copy_tool
    use cubeadm_get
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(export_prog_t), intent(inout) :: prog
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='EXPORT>PROG>CUBE'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    ! First apply transposition on the input if needed
    call cubetuple_autotranspose_cube(prog%cube,error)
    if (error)  return
    !
    ! Now switch the input cube to subset access
    call cubeadm_access_header(prog%cube,code_access_subset,code_read,error)
    if (error)  return
    !
    ! Sanity
    if (prog%cube%node%origin.eq.code_origin_imported) then
      call cubeadm_message(seve%w,rname,'Exporting an imported cube')
    elseif (prog%cube%node%origin.eq.code_origin_exported) then
      call cubeadm_message(seve%w,rname,'Re-exporting an exported cube')
    endif
    !
    call prog%exported%prog%set_buffering(code_buffer_disk,error)
    if (error) return
    call prog%exported%prog%set_filename(prog%name,error)
    if (error) return
    call prog%exported%prog%set_filekind(prog%format,error)
    if (error) return
    if (prog%doblank) then
      call prog%exported%prog%set_reblank(prog%bval,prog%eval,error)
      if (error) return
    endif
    call prog%exported%prog%set_checksum(prog%checksum%enabled,error)
    if (error)  return
    !
    call cubeadm_copy_data(prog%cube,prog%exported,error)
    if (error) return
    !
    ! Update cube in memory
    prog%exported%node%origin = code_origin_exported
  end subroutine cubeadm_export_prog_data
end module cubeadm_export
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
