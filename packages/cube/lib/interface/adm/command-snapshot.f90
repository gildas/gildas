!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeadm_snapshot
  use cubetools_structure
  use cubedag_iterator
  use cubedag_repositories
  use cubetuple_export
  use cube_types
  use cubeadm_setup
  use cubeadm_directory_type
  use cubeadm_opened
  use cubeadm_messaging
  !
  public :: cubeadm_snapshot_register,cubeadm_snapshot_reimport
  public :: cubeadm_snapshot_dagname,cubeadm_snapshot_histname
  private
  !
  type :: snapshot_comm_t
     type(option_t), pointer :: snapshot
     type(option_t), pointer :: keep
  end type snapshot_comm_t
  type(snapshot_comm_t) :: comm
  !
  character(len=*), parameter :: dagnamedef = 'cube.dag'
  character(len=*), parameter :: histnamedef = 'cube.hist'
  !
  type snapshot_user_t
    logical :: keep
  end type snapshot_user_t
  !
contains
  !
  subroutine cubeadm_snapshot_register(error)
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    logical,          intent(inout) :: error
    ! Local
    character(len=*), parameter :: comm_abstract = &
         'Save the current DAG structure to the disk'
    character(len=*), parameter :: comm_help = &
         'Perform a snapshot (from memory to disk) of the relevant&
         & buffers for later reuse in a new CUBE session. In details:'&
         & //strg_cr//strg_cr//&
         &'1) memory-only cubes (i.e. created during the session in&
         & memory mode) are dumped in the TMP directory under an&
         & arbitrary name (based on their identifier and access&
         & mode). Other cubes (RAW and RED cubes) are not dumped as&
         & they are disk files;'&
         & //strg_cr//strg_cr//&
         &'2) the DAG is saved on disk (name&
         & cube.dag) in the TMP directory.'
    character(len=*), parameter :: rname='SNAPSHOT>REGISTER'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'SNAPSHOT','',&
         comm_abstract,&
         comm_help,&
         cubeadm_snapshot_command,&
         comm%snapshot,error)
    if (error) return
    !
    call cubetools_register_option(&
         'KEEP','',&
         'Keep the data buffers',&
         'Keep the data buffers. Default is to free them, they will &
         &be reloaded from disk if relevant.',&
         comm%keep,error)
    if (error) return
  end subroutine cubeadm_snapshot_register
  !
  subroutine cubeadm_snapshot_command(line,error)
    !---------------------------------------------------------------------
    ! Support for command
    !  ADM\SNAPSHOT
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='SNAPSHOT>COMMAND'
    type(snapshot_user_t) :: user
    !
    call cubeadm_message(seve%t,rname,'Welcome')
    call cubeadm_snapshot_parse(line,user,error)
    if (error) return
    call cubeadm_snapshot_main(user,error)
    if (error) continue
  end subroutine cubeadm_snapshot_command
  !
  subroutine cubeadm_snapshot_parse(line,user,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*),      intent(in)    :: line
    type(snapshot_user_t), intent(out)   :: user
    logical,               intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='SNAPSHOT>PARSE'
    !
    call cubeadm_message(seve%t,rname,'Welcome')
    !
    call comm%keep%present(line,user%keep,error)
    if (error) return
  end subroutine cubeadm_snapshot_parse
  !
  subroutine cubeadm_snapshot_main(user,error)
    type(snapshot_user_t), intent(in)    :: user
    logical,               intent(inout) :: error
    !
    call cubeadm_snapshot_export(user,error)
    if (error) return
  end subroutine cubeadm_snapshot_main
  !
  subroutine cubeadm_snapshot_export(user,error)
    type(snapshot_user_t), intent(in)    :: user
    logical,               intent(inout) :: error
    !
    call cubeadm_directory_create(dir%tmp,error)
    if (error) return
    !
    ! 1) Export the cubes
    call cubeadm_snapshot_cubes(user%keep,error)
    if (error) return
    !
    ! 2) Export the DAG (after the cubes are exported on disk)
    call cubedag_repositories_write(cubeadm_snapshot_dagname(),  &
                                    cubeadm_snapshot_histname(),  &
                                    error)
    if (error) return
    !
  end subroutine cubeadm_snapshot_export
  !
  subroutine cubeadm_snapshot_cubes(keep,error)
    use cubetools_access_types
    logical, intent(in)    :: keep  ! Keep the data buffers?
    logical, intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='SNAPSHOT>CUBES'
    character(len=file_l) :: filename
    class(cubedag_node_object_t), pointer :: cube
    integer(kind=entr_k) :: counter
    character(len=mess_l) :: mess
    !
    call cubedag_iterator_init(error)
    if (error) return
    !
    counter = 0
    do while (cubedag_iterator_iterate(cube))
      select type (cube)
      type is (cube_t)
        if (cube%node%origin.eq.code_origin_created) then
          ! Might need dumping from memory to disk
          write(filename,'(A,I0,A)')  &
            trim(dir%tmp),cube%node%id,cubetools_order2ext(cube%order())
          call cubetuple_export_cube(cube,filename,.false.,error)
          if (error) return
          ! Bookkeeping
          cube%node%origin = code_origin_snapshot
          counter = counter+1
        else
          ! All other origins have at least an access on disk
          ! => nothing related to disk
        endif
        if (.not.keep) then
          ! Free the memory data buffers
          call cube%free(error)
          if (error) return
        endif
      class default
        ! e.g. the root node. Skip.
        continue
      end select
      !
      call cubeadm_finish_one(cube,error)
      if (error) return
    enddo
    !
    write(mess,'(I0,A,A)')  counter,' cube(s) snapshot to ',trim(dir%tmp)
    call cubeadm_message(seve%i,rname,mess)
    !
  end subroutine cubeadm_snapshot_cubes
  !
  function cubeadm_snapshot_dagname()
    !-------------------------------------------------------------------
    ! Return default DAG location
    !-------------------------------------------------------------------
    character(len=file_l) :: cubeadm_snapshot_dagname
    cubeadm_snapshot_dagname = trim(dir%tmp)//dagnamedef
  end function cubeadm_snapshot_dagname
  !
  function cubeadm_snapshot_histname()
    !-------------------------------------------------------------------
    ! Return default DAG location
    !-------------------------------------------------------------------
    character(len=file_l) :: cubeadm_snapshot_histname
    cubeadm_snapshot_histname = trim(dir%tmp)//histnamedef
  end function cubeadm_snapshot_histname
  !
  !---------------------------------------------------------------------
  !
  subroutine cubeadm_snapshot_reimport(dagname,histname,merge,skip,error)
    use gkernel_interfaces
    use cubedag_find
    use cubetuple_format
    character(len=*), intent(in)    :: dagname
    character(len=*), intent(in)    :: histname
    logical,          intent(in)    :: merge  ! Allow merge in non-empty DAG
    logical,          intent(in)    :: skip   ! Skip reimport if no DAG file?
    logical,          intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='SNAPSHOT>REIMPORT'
    type(find_prog_t) :: criter
    class(cubedag_node_object_t), pointer :: object
    type(format_t), pointer :: format
    !
    ! 1) Reimport the cubes into the DAG
    if (gag_inquire(dagname,len_trim(dagname)).ne.0) then
      ! No DAG file at this location
      if (skip)  return
      call cubeadm_message(seve%w,rname,'No DAG file '//dagname)
      return
    endif
    call cubedag_repositories_read(dagname,histname,merge,error)
    if (error)  return
    !
    ! 2) Fill theirs headers (from node to head)
    !    Note that the header will actually be re-read from disk by
    !    the first command requesting access to the file (access to
    !    the header - e.g. HEADER - and/or data.
    call cubedag_iterator_init(error)
    if (error) return
    do while (cubedag_iterator_iterate(object))
      select type (object)
      class is (format_t)
        format => object
        call cubeadm_node_to_head(object,format%head,error)
        if (error) return
      class default
        ! e.g. the root node. Skip.
        continue
      end select
    enddo
    !
    if (cubset%index%default.eq.code_index_current) then
      ! 3) Implicitly FIND everything in current index
      call criter%ix2cx(error)
      if (error) return
    endif
  end subroutine cubeadm_snapshot_reimport
  !
  subroutine cubeadm_node_to_head(object,head,error)
    use cubetools_header_types
    use cubedag_node
    !---------------------------------------------------------------------
    ! Transfer the header to the DAG node
    !---------------------------------------------------------------------
    class(cubedag_node_object_t), intent(in)    :: object
    type(cube_header_t),          intent(inout) :: head
    logical,                      intent(inout) :: error
    !
    call cubedag_node_get_header(object,head,error)
    if (error) return
  end subroutine cubeadm_node_to_head
  !
end module cubeadm_snapshot
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
