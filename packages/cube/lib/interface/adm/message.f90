!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage CUBE ADM messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeadm_messaging
  use gpack_def
  use gbl_message
  use cubetools_parameters
  !
  public :: admseve,seve,mess_l
  public :: cubeadm_message_set_id,cubeadm_message
  public :: cubeadm_message_set_trace,cubeadm_message_get_trace
  public :: cubeadm_message_set_alloc,cubeadm_message_get_alloc
  public :: cubeadm_message_set_others,cubeadm_message_get_others
  private
  !
  ! Identifier used for message identification
  integer(kind=4) :: cubeadm_message_id = gpack_global_id  ! Default value for startup message
  !
  type :: cubeadm_messaging_debug_t
     integer(kind=code_k) :: trace  = seve%t
     integer(kind=code_k) :: alloc  = seve%d
     integer(kind=code_k) :: others = seve%d
  end type cubeadm_messaging_debug_t
  !
  type(cubeadm_messaging_debug_t) :: admseve
  !
contains
  !
  subroutine cubeadm_message_set_id(id)
    !---------------------------------------------------------------------
    ! Alter library id into input id. Should be called by the library
    ! which wants to share its id with the current one.
    !---------------------------------------------------------------------
    integer(kind=4), intent(in) :: id
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='MESSAGE>SET>ID'
    !
    cubeadm_message_id = id
    write (mess,'(A,I0)') 'Now use id #',cubeadm_message_id
    call cubeadm_message(seve%d,rname,mess)
  end subroutine cubeadm_message_set_id
  !
  subroutine cubeadm_message(mkind,procname,message)
    use cubetools_cmessaging
    !---------------------------------------------------------------------
    ! Messaging facility for the current library. Calls the low-level
    ! (internal) messaging routine with its own identifier.
    !---------------------------------------------------------------------
    integer(kind=4),  intent(in) :: mkind     ! Message kind
    character(len=*), intent(in) :: procname  ! Name of calling procedure
    character(len=*), intent(in) :: message   ! Message string
    !
    call cubetools_cmessage(cubeadm_message_id,mkind,'ADM>'//procname,message)
  end subroutine cubeadm_message
  !
  subroutine cubeadm_message_set_trace(on)
    !---------------------------------------------------------------------
    ! @ public
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       admseve%trace = seve%i
    else
       admseve%trace = seve%t
    endif
  end subroutine cubeadm_message_set_trace
  !
  subroutine cubeadm_message_set_alloc(on)
    !---------------------------------------------------------------------
    ! @ public
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       admseve%alloc = seve%i
    else
       admseve%alloc = seve%d
    endif
  end subroutine cubeadm_message_set_alloc
  !
  subroutine cubeadm_message_set_others(on)
    !---------------------------------------------------------------------
    ! @ public
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       admseve%others = seve%i
    else
       admseve%others = seve%d
    endif
  end subroutine cubeadm_message_set_others
  !
  function cubeadm_message_get_trace()
    !---------------------------------------------------------------------
    ! @ public
    !---------------------------------------------------------------------
    logical :: cubeadm_message_get_trace
    !
    cubeadm_message_get_trace = admseve%trace.eq.seve%i
    !
  end function cubeadm_message_get_trace
  !
  function cubeadm_message_get_alloc()
    !---------------------------------------------------------------------
    ! @ public
    !---------------------------------------------------------------------
    logical :: cubeadm_message_get_alloc
    !
    cubeadm_message_get_alloc = admseve%alloc.eq.seve%i
    !
  end function cubeadm_message_get_alloc
  !
  function cubeadm_message_get_others()
    !---------------------------------------------------------------------
    ! @ public
    !---------------------------------------------------------------------
    logical :: cubeadm_message_get_others
    !
    cubeadm_message_get_others = admseve%others.eq.seve%i
    !
  end function cubeadm_message_get_others
  !
end module cubeadm_messaging
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
