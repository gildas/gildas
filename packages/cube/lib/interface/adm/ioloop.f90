module cubeadm_ioloop
  use cubetools_parameters
  use cubeadm_messaging
  use cube_types
  use cubetuple_iterate

  public :: cubeadm_io_iterate,cubeadm_io_iterate_planes
  private

contains

  subroutine cubeadm_io_iterate(first,last,cube,error)
    !--------------------------------------------------------------------
    ! Declare the range of ENTRIES to be processed next. Request
    ! libcubeio to ensure they are made available in its internal buffers
    !--------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: first
    integer(kind=entr_k), intent(in)    :: last
    type(cube_t),         intent(inout) :: cube
    logical,              intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='IO>ITERATE>ENTRIES'
    integer(kind=chan_k) :: ichanfirst,ichanlast
    integer(kind=pixe_k) :: ypixfirst,ypixlast
    !
    select case (cube%access())
    case (code_access_imaset)
      ! File is native LMV => always assume image (efficient) access
      ichanfirst = first
      ichanlast = last
      call cubetuple_iterate_chan(ichanfirst,ichanlast,cube,error)
      if (error) return
    case (code_access_speset)
      ! File is native VLM => always assume spectrum (efficient) access
      ypixfirst = (first-1)/cube%tuple%current%desc%nx+1
      ypixlast = (last-1)/cube%tuple%current%desc%nx+1
      call cubetuple_iterate_pix(ypixfirst,ypixlast,cube,error)
      if (error) return
    case default
      call cubeadm_message(seve%e,rname,'Unsupported access mode')
      error = .true.
      return
    end select
  end subroutine cubeadm_io_iterate

  subroutine cubeadm_io_iterate_planes(first,last,cube,error)
    !--------------------------------------------------------------------
    ! Declare the range of PLANES to be processed next. Request
    ! libcubeio to ensure they are made available in its internal buffers
    !--------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: first
    integer(kind=entr_k), intent(in)    :: last
    type(cube_t),         intent(inout) :: cube
    logical,              intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='IO>ITERATE>PLANES'
    !
    select case (cube%access())
    case (code_access_imaset)
      call cubetuple_iterate_chan(first,last,cube,error)
      if (error) return
    case (code_access_speset)
      call cubetuple_iterate_pix(first,last,cube,error)
      if (error) return
    case (code_access_subset,code_access_fullset)
      call cubetuple_iterate_subcube(first,last,cube,error)
      if (error) return
    case default
      call cubeadm_message(seve%e,rname,'Unsupported access mode')
      error = .true.
      return
    end select
  end subroutine cubeadm_io_iterate_planes

end module cubeadm_ioloop
