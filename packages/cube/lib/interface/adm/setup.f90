!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeadm_setup
  use cubetools_setup_types
  use cubeadm_messaging
  !
  public :: cubset
  private
  !
  type(cube_setup_t), target :: cubset
  !  
end module cubeadm_setup
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
