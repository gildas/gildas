module cubeadm_transpose
  use cubetools_structure
  use cubetools_keywordlist_types
  use cubetools_access_types
  use cubedag_allflags
  use cube_types
  use cubeadm_cubeid_types
  use cubeadm_messaging
  !
  public :: transpose
  private
  !
  type :: transpose_comm_t
    type(option_t),      pointer :: comm
    type(cubeid_arg_t),  pointer :: incube
    type(order_comm_t)           :: access
  contains
    procedure, public  :: register => cubeadm_transpose_register
    procedure, private :: parse    => cubeadm_transpose_parse
    procedure, private :: main     => cubeadm_transpose_main
  end type transpose_comm_t
  type(transpose_comm_t) :: transpose
  !
  type transpose_user_t
    type(cubeid_user_t) :: cubeid
    type(order_user_t)  :: access
  contains
    procedure, private :: toprog => cubeadm_transpose_user_toprog
  end type transpose_user_t
  !
  type transpose_prog_t
    type(cube_t), pointer :: incube  ! Input cube
    type(order_prog_t)    :: access
  contains
    procedure, public :: do => cubeadm_transpose_do
  end type transpose_prog_t
  !
contains
  !
  subroutine cubeadm_transpose_command(line,error)
    !-------------------------------------------------------------------
    ! Support routine for command TRANSPOSE
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(transpose_user_t) :: user
    character(len=*), parameter :: rname='TRANSPOSE>COMMAND'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call transpose%parse(line,user,error)
    if (error) return
    call transpose%main(user,error)
    if (error) return
  end subroutine cubeadm_transpose_command
  !
  subroutine cubeadm_transpose_register(transpose,error)
    !-------------------------------------------------------------------
    ! Register ADM\TRANSPOSE command and its options
    !-------------------------------------------------------------------
    class(transpose_comm_t), intent(inout) :: transpose
    logical,                 intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    character(len=*), parameter :: rname='TRANSPOSE>REGISTER'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    ! Command
    call cubetools_register_command(&
         'TRANSPOSE','[cube]',&
         'Populate the tuple with the desired access',&
         'Populate the tuple to the desired access, transposing the data &
         &if relevant. This command is mostly for testing or debugging, as &
         &the transposition is an automatic feature triggered by the commands &
         &when appropriate.',&
         cubeadm_transpose_command,&
         transpose%comm,error)
    if (error) return
    call cubearg%register( &
         'CUBE', &
         '[CubeID]',  &
         strg_id,&
         code_arg_optional,  &
         [flag_any], &
         code_read, &
         code_access_imaset_or_speset, &  ! Forced to one or the other at run time
         transpose%incube, &
         error)
    if (error) return
    !
    call transpose%access%register(&
         'ACCESS',&
         'Specify the access order of the transposed cube',&
         'Default is IMAGE',&
         error)
    if (error) return
  end subroutine cubeadm_transpose_register
  !
  subroutine cubeadm_transpose_parse(transpose,line,user,error)
    use cubetools_disambiguate
    !-------------------------------------------------------------------
    ! Parsing facility for command
    !   TRANSPOSE [CubeID]
    !-------------------------------------------------------------------
    class(transpose_comm_t), intent(in)    :: transpose
    character(len=*),        intent(in)    :: line
    type(transpose_user_t),  intent(out)   :: user
    logical,                 intent(inout) :: error
    !
    character(len=*), parameter :: rname='TRANSPOSE>PARSE'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,transpose%comm,user%cubeid,error)
    if (error) return
    !
    ! /ACCESS IMAGE|SPECTRUM
    call transpose%access%parse(line,user%access,error)
    if (error) return
  end subroutine cubeadm_transpose_parse
  !
  subroutine cubeadm_transpose_main(transpose,user,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    class(transpose_comm_t), intent(in)    :: transpose
    type(transpose_user_t),  intent(in)    :: user
    logical,                 intent(inout) :: error
    !
    type(transpose_prog_t) :: prog
    character(len=*), parameter :: rname='TRANSPOSE>MAIN'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call user%toprog(prog,error)
    if (error) return
    call prog%do(error)
    if (error) return
  end subroutine cubeadm_transpose_main
  !
  subroutine cubeadm_transpose_user_toprog(user,prog,error)
    use cubeadm_get
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    class(transpose_user_t), intent(in)    :: user
    type(transpose_prog_t),  intent(out)   :: prog
    logical,                 intent(inout) :: error
    !
    character(len=*), parameter :: rname='TRANSPOSE>USER2PROG'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call user%access%toprog(transpose%access,code_access_imaset,  &
      prog%access,error)
    if (error)  return
    !
    call cubeadm_get_header(transpose%incube,user%cubeid, &
      prog%incube,error,access=prog%access%code)
    if (error) return
  end subroutine cubeadm_transpose_user_toprog
  !
  subroutine cubeadm_transpose_do(prog,error)
    use cubetuple_transpose
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(transpose_prog_t), intent(in)    :: prog
    logical,                 intent(inout) :: error
    !
    call cubetuple_autotranspose_cube(prog%incube,error)
    if (error)  return
  end subroutine cubeadm_transpose_do
end module cubeadm_transpose
