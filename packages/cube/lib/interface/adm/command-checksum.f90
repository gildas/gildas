!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeadm_checksum
  use cube_types
  use cubetools_structure
  use cubetools_checksum
  use cubeadm_cubeid_types
  use cubeadm_messaging
  !
  public :: checksum
  private
  !
  type :: checksum_comm_t
     type(option_t),     pointer :: comm
     type(cubeid_arg_t), pointer :: cube
   contains
     procedure, public  :: register => cubeadm_checksum_register
     procedure, private :: parse    => cubeadm_checksum_parse
     procedure, private :: main     => cubeadm_checksum_main
  end type checksum_comm_t
  type(checksum_comm_t) :: checksum
  !
  type checksum_user_t
     type(cubeid_user_t) :: cubeids
   contains
     procedure, private :: toprog => cubeadm_checksum_user_toprog
  end type checksum_user_t
  !
  type checksum_prog_t
     type(cube_t), pointer :: cube
   contains
     procedure, private :: data => cubeadm_checksum_prog_data
     procedure, private :: loop => cubeadm_checksum_prog_loop
     procedure, private :: act  => cubeadm_checksum_prog_act
  end type checksum_prog_t
  !
contains
  !
  subroutine cubeadm_checksum_command(line,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(checksum_user_t) :: user
    character(len=*), parameter :: rname='CHECKSUM>COMMAND'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call checksum%parse(line,user,error)
    if (error) return
    call checksum%main(user,error)
    if (error) continue
  end subroutine cubeadm_checksum_command
  !
  subroutine cubeadm_checksum_register(comm,error)
    use cubedag_allflags
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(checksum_comm_t), intent(inout) :: comm
    logical,                intent(inout) :: error
    !
    type(cubeid_arg_t) :: cube
    character(len=*), parameter :: comm_abstract = &
      'Compute the CRC of the cube data'
    character(len=*), parameter :: comm_help = &
      'Compute the cyclic redundancy check (CRC) of the cube data'
    character(len=*), parameter :: rname='CHECKSUM>REGISTER'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    ! Syntax
    call cubetools_register_command(&
         'CHECKSUM','[CubeId]',&
         comm_abstract,&
         comm_help,&
         cubeadm_checksum_command,&
         comm%comm,&
         error)
    if (error) return
    call cube%register(&
         'CUBE',&
         'Any cube',&
         strg_id,&
         code_arg_optional,&
         [flag_any],&
         code_read,&
         code_access_subset,&
         comm%cube,&
         error)
    if (error) return
  end subroutine cubeadm_checksum_register
  !
  subroutine cubeadm_checksum_parse(comm,line,user,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(checksum_comm_t), intent(in)    :: comm
    character(len=*),       intent(in)    :: line
    type(checksum_user_t),  intent(out)   :: user
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='CHECKSUM>PARSE'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
  end subroutine cubeadm_checksum_parse
  !
  subroutine cubeadm_checksum_main(comm,user,error)
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(checksum_comm_t), intent(in)    :: comm
    type(checksum_user_t),  intent(inout) :: user
    logical,                intent(inout) :: error
    !
    type(checksum_prog_t) :: prog
    character(len=*), parameter :: rname='CHECKSUM>MAIN'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubeadm_checksum_main
  !
  subroutine cubeadm_checksum_user_toprog(user,comm,prog,error)
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(checksum_user_t), intent(in)    :: user
    type(checksum_comm_t),  intent(in)    :: comm
    type(checksum_prog_t),  intent(out)   :: prog
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='CHECKSUM>USER>TOPROG'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%cube,user%cubeids,prog%cube,error)
    if (error) return
  end subroutine cubeadm_checksum_user_toprog
  !
  subroutine cubeadm_checksum_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(checksum_prog_t), intent(inout) :: prog
    logical,                intent(inout) :: error
    !
    integer(kind=4) :: ier
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='CHECKSUM>PROG>DATA'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    ier = cubetools_sha1sum_init()
    if (ier.ne.0) then
      error = .true.
      return
    endif
    !
    ! Note: parallelization can not be enabled here
    call cubeadm_datainit_all(iter,error)
    if (error) return
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       if (.not.error) &
         call prog%loop(iter,error)
    enddo
    if (error)  return
    !
    call cubeadm_message(seve%i,rname,'Data SHA1 sum is '//cubetools_sha1sum_get())
  end subroutine cubeadm_checksum_prog_data
  !
  subroutine cubeadm_checksum_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_subcube_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(checksum_prog_t),   intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='CHECKSUM>PROG>LOOP'
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter,error)
      if (error) return
    enddo
  end subroutine cubeadm_checksum_prog_loop
  !
  subroutine cubeadm_checksum_prog_act(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_subcube_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(checksum_prog_t),   intent(inout) :: prog
    type(cubeadm_iterator_t), intent(in)    :: iter
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='SUBCUBE2SUBCUBE>PROG>ACT'
    !
    if (prog%cube%iscplx()) then
      call cubeadm_checksum_prog_act_c4(prog,iter,error)
      if (error)  return
    else
      call cubeadm_checksum_prog_act_r4(prog,iter,error)
      if (error)  return
    endif
  end subroutine cubeadm_checksum_prog_act
  !
  subroutine cubeadm_checksum_prog_act_r4(prog,iter,error)
    use iso_c_binding, only: c_loc
    use cubeadm_taskloop
    use cubeadm_subcube_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(checksum_prog_t),   intent(inout) :: prog
    type(cubeadm_iterator_t), intent(in)    :: iter
    logical,                  intent(inout) :: error
    !
    integer(kind=4) :: ier
    type(subcube_t) :: insub
    integer(kind=8) :: nbytes
    character(len=*), parameter :: rname='SUBCUBE2SUBCUBE>PROG>ACT>R4'
    !
    ! Subcubes are initialized here as their size (3rd dim) may change from
    ! from one subcube to another.
    call insub%associate('insub',prog%cube,iter,error)
    if (error) return
    call insub%get(error)
    if (error) return
    nbytes = insub%nx*insub%ny*insub%nz*4
    ier = cubetools_sha1sum_update(c_loc(insub%val),nbytes)
    if (ier.ne.0) then
      call cubeadm_message(seve%e,rname,'Could not process subcube')
      error = .true.
      return
    endif
  end subroutine cubeadm_checksum_prog_act_r4
  !
  subroutine cubeadm_checksum_prog_act_c4(prog,iter,error)
    use iso_c_binding, only: c_loc
    use cubeadm_taskloop
    use cubeadm_subcube_cplx_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(checksum_prog_t),   intent(inout) :: prog
    type(cubeadm_iterator_t), intent(in)    :: iter
    logical,                  intent(inout) :: error
    !
    integer(kind=4) :: ier
    type(subcube_cplx_t) :: insub
    integer(kind=8) :: nbytes
    character(len=*), parameter :: rname='SUBCUBE2SUBCUBE>PROG>ACT>C4'
    !
    ! Subcubes are initialized here as their size (3rd dim) may change from
    ! from one subcube to another.
    call insub%associate('insub',prog%cube,iter,error)
    if (error) return
    call insub%get(error)
    if (error) return
    nbytes = insub%nx*insub%ny*insub%nz*8
    ier = cubetools_sha1sum_update(c_loc(insub%val),nbytes)
    if (ier.ne.0) then
      call cubeadm_message(seve%e,rname,'Could not process subcube')
      error = .true.
      return
    endif
  end subroutine cubeadm_checksum_prog_act_c4
  !
end module cubeadm_checksum
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
