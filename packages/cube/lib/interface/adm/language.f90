!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeadm_language
  use cubetools_parameters
  use cubetools_structure
  use cubedag_dagcomm
  use cubeadm_messaging
  use cubeadm_checksum
  use cubeadm_directory
  use cubeadm_export
  use cubeadm_find
  use cubeadm_flag
  use cubeadm_free
  use cubeadm_history
  use cubeadm_import
  use cubeadm_list
  use cubeadm_memory
  use cubeadm_remove
  use cubeadm_snapshot
  use cubeadm_touch
  use cubeadm_transpose
  use cubeadm_undo
  use cubeadm_update
  use cubeadm_verify
  !
  public :: cubeadm_register_language
  private
  !
  integer(kind=lang_k) :: langid
  !
contains
  !
  subroutine cubeadm_register_language(error)
    !----------------------------------------------------------------------
    ! Register the ADM\ language
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    call cubetools_register_language('ADM',&
         'J.Pety, S.Bardeau, V.deSouzaMagalhaes',&
         'Commands to manage list of cubes',&
         'gag_doc:hlp/cube-help-adm.hlp',&
         cubeadm_execute_command,langid,error)
    if (error) return
    !
    call checksum%register(error)
    if (error) return
    call directory%register(error)
    if (error) return
    call export%register(error)
    if (error) return
    call find%register(error)
    if (error) return
    call flag%register(error)
    if (error) return
    call free%register(error)
    if (error) return
    call history%register(error)
    if (error) return
    call import%register(error)
    if (error) return
    call cubeadm_list_register(error)
    if (error) return
    call cubeadm_memory_register(error)
    if (error) return
    call remove%register(error)
    if (error) return
    call cubeadm_snapshot_register(error)
    if (error) return
    call undo%register(error)
    if (error) return
    call update%register(error)
    if (error) return
    call verify%register(error)
    if (error) return
    if (cube_dev_mode()) then
      call cubedag_dag_register(error)
      if (error) return
      call touch%register(error)
      if (error) return
      call transpose%register(error)
      if (error) return
    endif
    !
    call cubetools_register_dict(error)
    if (error) return
  end subroutine cubeadm_register_language
  !
  subroutine cubeadm_execute_command(line,comm,error)
    use cubeadm_opened
    use cubeadm_timing
    !----------------------------------------------------------------------
    ! Execute a command of the ADM\ language
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    character(len=*), intent(in)    :: comm
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='EXECUTE>COMMAND'
    !
    error = .false.
    if (comm.eq.'ADM?') then
       call cubetools_list_language_commands(langid,error)
       if (error) return
    else
       call cubeadm_timing_init()
       call cubetools_execute_command(line,langid,comm,error)
       if (error) continue ! To ensure error recovery in the call to cubeadm_finish_all
       call cubeadm_finish_all(comm,line,error)
       if (error) continue ! To ensure error recovery in timing
       call cubeadm_timing_final(comm)
    endif
  end subroutine cubeadm_execute_command
end module cubeadm_language
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
