!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeadm_image_types
  use cubetools_parameters
  use cubetools_array_types
  use cubetools_axis_types
  use cube_types
  use cubeadm_messaging
  use cubeadm_taskloop
  use cubeadm_taskloop_iteration
  !
  public :: image_t
  private
  !
  type, extends(real_2d_t) :: image_t
     type(cube_t),             private, pointer :: cube => null() ! Associated cube
     type(cubeadm_iterator_t), private, pointer :: task => null() ! Associated task iteration
     type(axis_t),             public           :: x              ! Image X axis
     type(axis_t),             public           :: y              ! Image Y axis
     integer(kind=indx_k),     private          :: lrange(2)      ! Min-max range of L pixels
     integer(kind=indx_k),     private          :: mrange(2)      ! Min-max range of M pixels
   contains
     generic,   public  :: allocate       => allocate_iter,allocate_noiter
     generic,   public  :: associate      => associate_iter,associate_noiter
     procedure, public  :: associate_xy   => image_associate_xy
     procedure, public  :: get_pixel_area => image_get_pixel_area
     procedure, public  :: get            => image_get
     procedure, public  :: put            => image_put
     procedure, public  :: put_in         => image_put_in
     procedure, public  :: blank_like     => image_blank_like
     procedure, public  :: ngood          => image_ngood
     procedure, private :: tasknum        => image_task_num
     !
     procedure, private :: allocate_iter    => image_allocate_iter
     procedure, private :: allocate_noiter  => image_allocate_noiter
     procedure, private :: associate_iter   => image_associate_iter
     procedure, private :: associate_noiter => image_associate_noiter
     procedure, private :: prepare          => image_prepare
  end type image_t
  !
contains
  !
  subroutine image_allocate_iter(image,name,cube,iterator,error)
    !-------------------------------------------------------------------
    !-------------------------------------------------------------------
    class(image_t),                   intent(inout) :: image
    character(len=*),                 intent(in)    :: name
    type(cube_t),                     intent(in)    :: cube
    type(cubeadm_iterator_t), target, intent(in)    :: iterator
    logical,                          intent(inout) :: error
    !
    image%task => iterator
    call image_allocate_noiter(image,name,cube,error)
    if (error)  return
  end subroutine image_allocate_iter
  !
  subroutine image_allocate_noiter(image,name,cube,error)
    !-------------------------------------------------------------------
    !-------------------------------------------------------------------
    class(image_t),   intent(inout) :: image
    character(len=*), intent(in)    :: name
    type(cube_t),     intent(in)    :: cube
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='IMAGE>ALLOCATE'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call image%prepare(name,cube,error)
    if (error)  return
    call image%reallocate(name,image%nx,image%ny,error)
    if (error) return
  end subroutine image_allocate_noiter
  !
  subroutine image_associate_iter(image,name,cube,iterator,error)
    !-------------------------------------------------------------------
    !-------------------------------------------------------------------
    class(image_t),                   intent(inout) :: image
    character(len=*),                 intent(in)    :: name
    type(cube_t),                     intent(in)    :: cube
    type(cubeadm_iterator_t), target, intent(in)    :: iterator
    logical,                          intent(inout) :: error
    !
    image%task => iterator
    call image_associate_noiter(image,name,cube,error)
    if (error)  return
  end subroutine image_associate_iter
  !
  subroutine image_associate_noiter(image,name,cube,error)
    !-------------------------------------------------------------------
    !-------------------------------------------------------------------
    class(image_t),   intent(inout) :: image
    character(len=*), intent(in)    :: name
    type(cube_t),     intent(in)    :: cube
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='IMAGE>ASSOCIATE'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call image%prepare(name,cube,error)
    if (error)  return
    call image%prepare_association(name,image%nx,image%ny,error)
    if (error) return
  end subroutine image_associate_noiter
  !
  subroutine image_prepare(image,name,cube,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(image_t),       intent(inout) :: image
    character(len=*),     intent(in)    :: name
    type(cube_t), target, intent(in)    :: cube
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='IMAGE>PREPARE'
    !
    ! Sanity check
    if (cube%iscplx()) then
       call cubeadm_message(seve%e,rname,  &
            'Invalid attempt to get a R*4 image from a C*4 cube')
       error = .true.
       return
    endif
    if (cube%access().ne.code_access_imaset) then
       call cubeadm_message(seve%e,rname,  &
            'Can not get an image from a cube with other access declared')
       error = .true.
       return
    endif
    if (.not.cube%iter%ready(rname,error)) then
       call cubeadm_message(seve%e,rname,  &
         'Failed to associate or allocate the '''//name//''' image on its cube')
       error = .true.
       return
    endif
    !
    image%cube => cube
    call cube%iter%image_region(image%lrange,image%mrange,error)
    if (error)  return
    call cube%iter%image_size(image%nx,image%ny,error)
    if (error)  return
  end subroutine image_prepare
  !
  !------------------------------------------------------------------------
  !
  subroutine image_associate_xy(image,error)
    use cubetools_header_methods
    !----------------------------------------------------------------------
    ! Associate the X and Y axis descriptions to the cube ones.  This
    ! association is only required by some algorithms.
    !----------------------------------------------------------------------
    class(image_t), intent(inout) :: image
    logical,        intent(inout) :: error
    !
    character(len=*), parameter :: rname='IMAGE>ASSOCIATE>XY'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    ! Sanity check
    if (.not.associated(image%cube)) then
      call cubeadm_message(seve%e,rname,'Internal error: cube pointer is null')
      error = .true.
      return
    endif
    !
    call cubetools_header_get_axis_head_l(image%cube%head,image%x,error)
    if (error) return
    call cubetools_header_get_axis_head_m(image%cube%head,image%y,error)
    if (error) return
  end subroutine image_associate_xy
  !
  subroutine image_get_pixel_area(image,area,error)
    use cubetools_axis_types
    use cubetools_header_methods
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    class(image_t),    intent(in)    :: image
    real(kind=coor_k), intent(out)   :: area
    logical,           intent(inout) :: error
    !
    type(axis_t) :: axis
    real(kind=coor_k) :: xinc,yinc
    character(len=*), parameter :: rname='GET>PIXEL>AREA'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call cubetools_header_get_axis_head_l(image%cube%head,axis,error)
    if (error) return
    xinc = axis%inc
    call cubetools_header_get_axis_head_m(image%cube%head,axis,error)
    if (error) return
    yinc = axis%inc
    area = xinc*yinc
  end subroutine image_get_pixel_area
  !
  !------------------------------------------------------------------------
  !
  subroutine image_get(image,oent,error)
    use cubeio_chan
    use cubetuple_entry
    !---------------------------------------------------------------------
    ! Get the ient image from the cube
    ! When image%val is an allocated pointer, we make a copy.
    ! In all other cases (associated or null), we make it point to the data.
    !---------------------------------------------------------------------
    class(image_t),       intent(inout) :: image  !
    integer(kind=entr_k), intent(in)    :: oent   ! Output entry number
    logical,              intent(inout) :: error  !
    ! 
    type(cube_chan_t) :: entry
    integer(kind=entr_k) :: kimage
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='GET>IMAGE'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call image%cube%iter%image_number(oent,kimage,error)
    if (error)  return
    !
    call cubetuple_get_chan(image%cube%user,image%cube%prog,image%cube,  &
                            kimage,entry,error)
    if (error) return
    !
    ! Sanity check
    if (image%lrange(1).lt.1 .or. image%lrange(2).gt.entry%nx  .or.  &
        image%mrange(1).lt.1 .or. image%mrange(2).gt.entry%ny) then
      write(mess,'(9(A,I0))')  &
        'Region overlaps image range. Region: [',  &
        image%lrange(1),':',image%lrange(2),',',   &
        image%mrange(1),':',image%mrange(2),'], image: [',  &
        1,':',entry%nx,',',  &
        1,':',entry%ny,']'
      call cubeadm_message(seve%e,rname,mess)
      error = .true.
      return
    endif
    !
    if (image%pointeris.eq.code_pointer_allocated) then
       image%val(:,:) = entry%r4(image%lrange(1):image%lrange(2),  &
                                 image%mrange(1):image%mrange(2))
    else
       image%val => entry%r4(image%lrange(1):image%lrange(2),  &
                             image%mrange(1):image%mrange(2))
       image%pointeris = code_pointer_associated
    endif
    ! image%nx = entry%nx
    ! image%ny = entry%ny
    !
    call cubeio_free_chan(entry,error)
    if (error) return
  end subroutine image_get
  !
  subroutine image_put(image,oent,error)
    !---------------------------------------------------------------------
    ! Put the ient image to the cube
    ! Only use pointers => Nothing to free
    !---------------------------------------------------------------------
    class(image_t),       intent(inout) :: image  !
    integer(kind=entr_k), intent(in)    :: oent   ! Output entry number
    logical,              intent(inout) :: error  !
    !
    call image_put_in(image,image%cube,oent,error)
    if (error)  return
  end subroutine image_put
  !
  subroutine image_put_in(image,cube,oent,error)
    use cubeio_chan
    use cubetuple_entry
    !---------------------------------------------------------------------
    ! Put the ient image to the cube.
    ! Only use pointers => Nothing to free.
    !
    ! This flavor, which explicitely states the output cube, should be used
    ! when the input image needs to be written in another cube without
    ! copy. See, eg, the SPLIT command. This should an exotic use compare to
    ! image_put.
    ! ---------------------------------------------------------------------
    class(image_t),       intent(inout) :: image  !
    type(cube_t),         intent(inout) :: cube   !
    integer(kind=entr_k), intent(in)    :: oent   ! Output entry number
    logical,              intent(inout) :: error  !
    !
    type(cube_chan_t) :: entry
    integer(kind=entr_k) :: kimage
    character(len=*), parameter :: rname='IMAGE>PUT>IN'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call cube%iter%image_size(entry%nx,entry%ny,error)
    if (error)  return
    entry%allocated = code_pointer_associated
    entry%r4 => image%val
    entry%iscplx = .false.
    !
    call image%cube%iter%image_number(oent,kimage,error)
    if (error)  return
    !
    call cubetuple_put_chan(cube%user,       &
                            cube%prog,       &
                            cube,            &
                            image%tasknum(), &
                            kimage,          &
                            entry,           &
                            error)
    if (error) return
  end subroutine image_put_in
  !
  !-----------------------------------------------------------------------
  !
  subroutine image_blank_like(image,reference,error)
    use cubetools_nan
    !---------------------------------------------------------------------
    ! *** JP: Is it a method of the image_t type or of the real_2d_t one?
    !---------------------------------------------------------------------
    class(image_t), intent(inout) :: image
    type(image_t),  intent(in)    :: reference
    logical,        intent(inout) :: error
    !
    integer(kind=pixe_k) :: ix,iy
    character(len=*), parameter :: rname='IMAGE>BLANK>LIKE'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    do iy=1,image%ny
       do ix=1,image%nx
          if (ieee_is_nan(reference%val(ix,iy))) image%val(ix,iy) = gr4nan
       enddo ! ix
    enddo ! iy
  end subroutine image_blank_like
  !
  subroutine image_ngood(image,ngood,error)
    use cubetools_nan
    !---------------------------------------------------------------------
    ! Get the number of good pixels
    !---------------------------------------------------------------------
    class(image_t),       intent(in)    :: image
    integer(kind=indx_k), intent(inout) :: ngood
    logical,              intent(inout) :: error
    !
    integer(kind=pixe_k) :: ix,iy
    character(len=*), parameter :: rname='IMAGE>NGOOD'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    ngood = 0
    do iy=1,image%ny
       do ix=1,image%nx
          if (isnan(image%val(ix,iy))) cycle
          ngood = ngood+1
       enddo ! ix
    enddo ! iy
  end subroutine image_ngood
  !
  function image_task_num(image)
    !-------------------------------------------------------------------
    ! Return the task number this image_t is running with
    !-------------------------------------------------------------------
    integer(kind=entr_k) :: image_task_num
    class(image_t), intent(in) :: image
    !
    if (associated(image%task)) then
      image_task_num = image%task%num
    else
      ! Assume single thread
      image_task_num = 1
    endif
  end function image_task_num
end module cubeadm_image_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
