!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeadm_entryloop
  use gkernel_interfaces
  use gkernel_types
  use cubetools_parameters
  use cubeadm_messaging
  !
  type(time_t) :: time
  !
  public :: cubeadm_entryloop_init,cubeadm_entryloop_iterate
  private
  !
contains
  !
  subroutine cubeadm_entryloop_init(nentry,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: nentry
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='ENTRYLOOP>INIT'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call gtime_init(time,nentry,error)
    if (error) return
  end subroutine cubeadm_entryloop_init
  !
  subroutine cubeadm_entryloop_iterate(ient,error)
    !----------------------------------------------------------------------
    ! No welcome on purpose here!
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ient
    logical,              intent(inout) :: error
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='ENTRYLOOP>ITERATE'
    !
    !$OMP CRITICAL (CUBEADM_ENTRYLOOP)
    call gtime_current(time)
    if (sic_ctrlc()) then
       write(mess,'(A,I0,A,I0)') 'Aborted by user CTRL-C at entry ',ient,' over ',time%nloop
       call cubeadm_message(seve%w,rname,mess)
       error = .true.
    endif
    !$OMP END CRITICAL (CUBEADM_ENTRYLOOP)
  end subroutine cubeadm_entryloop_iterate
  !
end module cubeadm_entryloop
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
