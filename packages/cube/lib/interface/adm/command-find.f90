!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeadm_find
  use gkernel_interfaces
  use cubetools_structure
  use cubetools_keywordlist_types
  use cubesyntax_datatype_types
  use cubedag_parameters
  use cubedag_allflags
  use cubedag_find
  use cube_types
  use cubeadm_messaging
  use cubeadm_cubeid_types
  !
  public :: find
  private
  !
  type :: find_comm_t
    type(option_t),           pointer :: comm
    type(option_t),           pointer :: entry
    type(option_t),           pointer :: observatory
    type(option_t),           pointer :: source
    type(option_t),           pointer :: line
    type(option_t),           pointer :: family
    type(option_t),           pointer :: flags
    type(option_t),           pointer :: identifier
    type(option_t),           pointer :: freq
    type(option_t),           pointer :: proj
    type(keywordlist_comm_t), pointer :: proj_arg
    type(option_t),           pointer :: children
    type(cubeid_arg_t),       pointer :: children_parent
    type(datatype_comm_t)             :: datatype
  contains
    procedure, public  :: register => cubeadm_find_register
    procedure, private :: parse    => cubeadm_find_parse
    procedure, private :: main     => cubeadm_find_main
  end type find_comm_t
  type(find_comm_t) :: find

  type find_user_t
    ! Type used for parsing FIND options. Get arguments as character strings
    ! (because strg_star is often accepted), interpret them elsewhere
    character(len=12)     :: centr(2) = strg_star  ! Entry range
    character(len=12)     :: ciden    = strg_star  ! Identifier
    character(len=12)     :: cobse    = strg_star  ! Observatory name
    character(len=12)     :: csour    = strg_star  ! Source name
    character(len=12)     :: cline    = strg_star  ! Line name
    character(len=base_l) :: cfami    = strg_star  ! Family name
    character(len=32)     :: cfreq    = strg_star  ! Frequency value
    character(len=16)     :: cproj    = strg_star  ! Projection name
  ! To be reimplemented to support flag_t, if relevant
  ! character(len=12)     :: ciflag   = strg_star  ! Flag (string for flag name)
    character(len=flag_l) :: ccflag   = strg_star  ! Flag (character string/pattern)
    type(cubeid_user_t)   :: cchil                 ! Parent of the children
    type(datatype_user_t) :: cdatat                ! Data type
  contains
    procedure, private :: toprog => cubeadm_find_user_toprog
  end type find_user_t
  !
  ! *** JP: Can't we use a generic interface mecanism to alias the definition?
  !
  integer(kind=entr_k), parameter :: root_id=0
  !
contains
  !
  subroutine cubeadm_find_register(find,error)
    use gbl_constant
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(find_comm_t), intent(inout) :: find
    logical,            intent(inout) :: error
    !
    type(standard_arg_t) :: stdarg
    type(cubeid_arg_t) :: cubearg
    type(keywordlist_comm_t) :: keyarg
    character(len=13) :: projections(0:mproj)
    character(len=*), parameter :: comm_abstract = &
         'Search for cubes on the DAG'
    character(len=*), parameter :: comm_help = &
         'Search the DAG to build a current Index, according to&
         & selection criteria defined by the options. If no options&
         & are given the current index will contains all cubes on the&
         & DAG.'//&
         strg_cr//&
         strg_cr//&
         &'The current index description is available in the structure&
         & IDX%.'
    character(len=*), parameter :: rname='FIND>REGISTER'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'FIND','',&
         comm_abstract,&
         comm_help,&
         cubeadm_find_command,&
         find%comm,error)
    if (error) return
    !
    call cubetools_register_option(&
         'ENTRY','n1 [n2]',&
         'search for the specified range of entry numbers',&
         strg_id,&
         find%entry,error)
    if (error) return
    call stdarg%register( &
         'n1',  &
         'First entry in range', &
         strg_id,&
         code_arg_mandatory, &
         error)
    if (error) return
    call stdarg%register( &
         'n2',  &
         'Second entry in range', &
         strg_id,&
         code_arg_optional, &
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'OBSERVATORY','name',&
         'search for the specified observatory',&
         strg_id,&
         find%observatory,error)
    if (error) return
    call stdarg%register( &
         'name',  &
         'Observatory name', &
         strg_id,&
         code_arg_mandatory, &
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'SOURCE','name',&
         'search by source name',&
         strg_id,&
         find%source,error)
    if (error) return
    call stdarg%register( &
         'name',  &
         'Source name', &
         strg_id,&
         code_arg_mandatory, &
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'LINE','name',&
         'search by line name',&
         strg_id,&
         find%line,error)
    if (error) return
    call stdarg%register( &
         'name',  &
         'Line name', &
         strg_id,&
         code_arg_mandatory, &
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'FAMILY','name',&
         'search for the specified family name',&
         strg_id,&
         find%family,error)
    if (error) return
    call stdarg%register( &
         'name',  &
         'Family name', &
         strg_id,&
         code_arg_mandatory, &
         error)
    if (error) return
    !
    call cubetools_register_option('FLAGS',&
         'flags',&
         'search for the specified flag pattern',&
         strg_id,&
         find%flags,error)
    if (error) return
    call stdarg%register( &
         'flags',  &
         'Flag pattern', &
         strg_id,&
         code_arg_mandatory, &
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'IDENTIFIER','id1 [id2]',&
         'search for the specified range of identifiers',&
         strg_id,&
         find%identifier,error)
    if (error) return
    call stdarg%register( &
         'id1',  &
         'First identifier in range', &
         strg_id,&
         code_arg_mandatory, &
         error)
    if (error) return
    call stdarg%register( &
         'id2',  &
         'Second identifier in range', &
         strg_id,&
         code_arg_optional, &
         error)
    if (error) return
    !
    call cubetools_register_option('FREQUENCY',&
         'frequency',&
         'Search for the specified frequency [MHz]',&
         strg_id,&
         find%freq,error)
    if (error) return
    call stdarg%register( &
         'frequency',  &
         'Frequency value', &
         strg_id,&
         code_arg_mandatory, &
         error)
    if (error) return
    !
    call cubetools_register_option('PROJECTION',&
         'Name',&
         'Search for the named projection kind',&
         strg_id,&
         find%proj,error)
    if (error) return
    call projnam_list(projections)
    call keyarg%register( &
         'projection',  &
         'Projection name', &
         strg_id,&
         code_arg_mandatory, &
         projections, &
         .not.flexible, &
         find%proj_arg, &
         error)
    if (error) return
    !
    call cubetools_register_option('CHILDREN',&
         'ParentID',&
         'Search all children of the named cube',&
         strg_id,&
         find%children,error)
    if (error) return
    call cubearg%register( &
         'PARENT',  &
         'Parent identifier', &
         strg_id,&
         code_arg_optional, &
         [flag_any], &
         code_read_none,&  ! Header not needed
         code_access_any,&
         find%children_parent,&
         error)
    if (error) return
    !
    call find%datatype%register(error)
    if (error)  return
  end subroutine cubeadm_find_register
  !
  subroutine cubeadm_find_command(line,error)
    !---------------------------------------------------------------------
    ! Support for command
    !  ADM\FIND
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(find_user_t) :: fuser
    character(len=*), parameter :: rname='FIND>COMMAND'
    !
    call cubeadm_message(admseve%trace,rname,'welcome')
    !
    call find%parse(line,fuser,error)
    if (error) return
    call find%main(fuser,error)
    if (error) return
  end subroutine cubeadm_find_command
  !
  subroutine cubeadm_find_parse(comm,line,fuser,error)
    !---------------------------------------------------------------------
    !  Parse the fuser input for selection criteria
    !---------------------------------------------------------------------
    class(find_comm_t), intent(in)    :: comm
    character(len=*),   intent(in)    :: line
    type(find_user_t),  intent(out)   :: fuser
    logical,            intent(inout) :: error
    !
    character(len=argu_l) :: parentid
    character(len=*), parameter :: rname='FIND>PARSE'
    !
    ! /ENTRY [min|*] [max|*]
    fuser%centr(1) = strg_star
    call cubetools_getarg(line,comm%entry,1,fuser%centr(1),.not.mandatory,error)
    if (error) return
    fuser%centr(2) = fuser%centr(1)
    call cubetools_getarg(line,comm%entry,2,fuser%centr(2),.not.mandatory,error)
    if (error) return
    !
    ! /IDENTIFIER [number|*]
    fuser%ciden = strg_star
    call cubetools_getarg(line,comm%identifier,1,fuser%ciden,.not.mandatory,error)
    if (error) return
    !
    ! /OBSERVATORY [obsname|*]
    fuser%cobse = strg_star
    call cubetools_getarg(line,comm%observatory,1,fuser%cobse,.not.mandatory,error)
    if (error) return
    !
    ! /SOURCE [sourcename|*]
    fuser%csour = strg_star
    call cubetools_getarg(line,comm%source,1,fuser%csour,.not.mandatory,error)
    if (error) return
    !
    ! /LINE [linename|*]
    fuser%cline = strg_star
    call cubetools_getarg(line,comm%line,1,fuser%cline,.not.mandatory,error)
    if (error) return
    !
    ! /FAMILY [familyname|*]  (case-sensitive search)
    fuser%cfami = strg_star
    call cubetools_getarg(line,comm%family,1,fuser%cfami,.not.mandatory,error)
    if (error) return
    !
    ! /FLAGS [flagpattern|*]
    fuser%ccflag = strg_star
    call cubetools_getarg(line,comm%flags,1,fuser%ccflag,.not.mandatory,error)
    if (error) return
    !
    ! /FREQUENCY [freqvalue|*]
    fuser%cfreq = strg_star
    call cubetools_getarg(line,comm%freq,1,fuser%cfreq,.not.mandatory,error)
    if (error) return
    !
    ! /PROJECTION [projname|*]
    fuser%cproj = strg_star
    call cubetools_getarg(line,comm%proj,1,fuser%cproj,.not.mandatory,error)
    if (error) return
    !
    ! /CHILDREN [ParentID]
    parentid = strg_star
    call cubetools_getarg(line,comm%children,1,parentid,.not.mandatory,error)
    if (error) return
    if (parentid.eq.strg_star) then
      fuser%cchil%ncube = 0
    else
      ! Re-parse using cubeid dedicated API
      call cubeadm_cubeid_parse(line,comm%children,fuser%cchil,error)
      if (error) return
    endif
    !
    ! /DATATYPE [Type|*]
    call comm%datatype%parse(line,fuser%cdatat,error)
    if (error)  return
  end subroutine cubeadm_find_parse
  !
  subroutine cubeadm_find_user_toprog(user,comm,criter,error)
    use gbl_constant
    use cubetools_disambiguate
    use cubetools_user2prog
    use cubetools_unit
    use cubeadm_get
    use cubetuple_format
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(find_user_t), intent(in)    :: user
    type(find_comm_t),  intent(in)    :: comm
    type(find_prog_t),  intent(out)   :: criter
    logical,            intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='FIND>CRITER'
    ! character(len=32) :: keyword
    character(len=13) :: resolved
    type(flag_t) :: iflag
    integer(kind=entr_k) :: tmp
    integer(kind=4) :: ier
    type(unit_user_t) :: frequnit
    class(format_t), pointer :: parent
    !
    ! /ENTRY [min|*] [max|*]
    call cubetools_user2prog_resolve_star(user%centr(1),minentr,criter%ientr(1),error)
    if (error)  return
    call cubetools_user2prog_resolve_star(user%centr(2),maxentr,criter%ientr(2),error)
    if (error)  return
    if (criter%ientr(1).gt.criter%ientr(2)) then
      tmp = criter%ientr(1)
      criter%ientr(1) = criter%ientr(2)
      criter%ientr(2) = tmp
    endif
    !
    ! /IDENTIFIER [number|*]
    call cubetools_user2prog_resolve_star(user%ciden,-1,criter%iiden,error)
    if (error)  return
    !
    ! /OBSERVATORY [obsname|*] ! case insensitive
    call cubetools_disambiguate_toupper(user%cobse,criter%cobse,error)
    if (error) return
    !
    ! /SOURCE [sourcename|*] ! case insensitive
    call cubetools_disambiguate_toupper(user%csour,criter%csour,error)
    if (error) return
    !
    ! /LINE [linename|*] ! case insensitive
    call cubetools_disambiguate_toupper(user%cline,criter%cline,error)
    if (error) return
    !
    ! /FAMILY [familyname|*]
    criter%cfami = user%cfami
    !
    ! FLAG [flagkeyword|*]  ! ZZZ No syntax to provide several flag codes
!   if (user%ciflag.eq.strg_star) then
      iflag = flag_any
!   else
!     call cubetools_disambiguate_strict(user%ciflag,dag_flag_keys,iflag,keyword,error)
!     if (error)  return
!   endif
    allocate(criter%iflags(1),stat=ier)
    if (failed_allocate(rname,'iflags',ier,error)) return
    criter%iflags(1) = iflag
    !
    ! /FLAG [flagpattern|*]
    criter%ccflag = user%ccflag
    !
    ! /FREQUENCY [freqvalue|*]
    call frequnit%get_from_code(code_unit_freq,error)
    if (error)  return
    call cubetools_user2prog_resolve_star(user%cfreq,frequnit,0.d0,criter%rfreq,error)
    if (error)  return
    !
    ! /PROJECTION [projname|*]
    if (user%cproj.eq.strg_star) then
      criter%iproj = 0
    else
      call cubetools_keywordlist_user2prog(find%proj_arg,user%cproj,criter%iproj,resolved,error)
      if (error) return
      ! Beware the keywords are registered from 1 to mproj+1 but the corresponding
      ! codes run from 0 to mproj:
      criter%iproj = criter%iproj-1
    endif
    !
    ! /CHILDREN [ParentID|*]
    if (user%cchil%ncube.eq.0) then
      criter%ichil = root_id
    else
      call cubeadm_get_fheader(comm%children_parent,user%cchil,parent,error)
      if (error) return
      criter%ichil = parent%node%id
    endif
    !
    ! /DATATYPE [Type|*]
    call user%cdatat%toprog(comm%datatype,criter%datat,error)
    if (error)  return
  end subroutine cubeadm_find_user_toprog
  !
  subroutine cubeadm_find_main(find,fuser,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(find_comm_t), intent(in)    :: find
    type(find_user_t),  intent(in)    :: fuser
    logical,            intent(inout) :: error
    !
    type(find_prog_t) :: prog
    character(len=*), parameter :: rname='FIND>MAIN'
    !
    call cubeadm_message(admseve%trace,rname,'welcome')
    !
    call fuser%toprog(find,prog,error)
    if (error)  return
    call prog%main(error)
    if (error)  return
  end subroutine cubeadm_find_main
end module cubeadm_find
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
