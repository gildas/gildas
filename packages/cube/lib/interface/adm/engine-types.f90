!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeadm_engine_types
  use cubedag_node_type
  use cubeadm_messaging
  use cubeadm_setup
  !---------------------------------------------------------------------
  ! Support module for registering the supported node types
  ! ZZZ Shouldn't this go in the libcubetuple? Beware of cubeadm_setup
  !---------------------------------------------------------------------
  !
  integer(kind=4) :: code_ftype_cube=0  ! cube_t identifier in the DAG (initialized at init)
  integer(kind=4) :: code_ftype_uv=0    ! uv_t identifier in the DAG (initialized at init)
  !
  public :: code_ftype_cube,code_ftype_uv
  public :: cubeadm_types_register
  private
  !
contains
  !
  subroutine cubeadm_types_register(error)
    use cubedag_type
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    call cubedag_type_register('CUBE','cube',cubeadm_cube_allocate,&
                               cubeadm_cube_deallocate,code_ftype_cube,error)
    if (error) return
    call cubedag_type_register('UV','uv',cubeadm_uv_allocate,&
                               cubeadm_uv_deallocate,code_ftype_uv,error)
    if (error) return
  end subroutine cubeadm_types_register
  !
  !---------------------------------------------------------------------
  !
  subroutine cubeadm_cube_allocate(object,error)
    use cube_types
    !-------------------------------------------------------------------
    ! Allocate and initialize a 'cube_t' in memory
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), pointer       :: object
    logical,                      intent(inout) :: error
    !
    object => cube_allocate_new(cubset,error)
    if (error) return
  end subroutine cubeadm_cube_allocate
  !
  subroutine cubeadm_cube_deallocate(object,error)
    !-------------------------------------------------------------------
    ! Finalize and deallocate a 'cube_t' in memory
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), pointer       :: object
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='ADM>DEALLOCATE'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    if (.not.associated(object)) then
      call cubeadm_message(seve%e,rname,'Internal error: object is not allocated')
      error = .true.
      return
    endif
    deallocate(object)  ! NB: deallocation is polymorphic: we are deallocating
                        ! a cube_t, which invokes implicitly its FINAL procedure
  end subroutine cubeadm_cube_deallocate
  !
  !---------------------------------------------------------------------
  !
  subroutine cubeadm_uv_allocate(object,error)
    use cubetuple_uv
    !-------------------------------------------------------------------
    ! Allocate and initialize a 'uv_t' in memory
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), pointer       :: object
    logical,                      intent(inout) :: error
    !
    object => uv_allocate_new(cubset,error)
    if (error) return
  end subroutine cubeadm_uv_allocate
  !
  subroutine cubeadm_uv_deallocate(object,error)
    !-------------------------------------------------------------------
    ! Finalize and deallocate a 'uv_t' in memory
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), pointer       :: object
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='ADM>DEALLOCATE'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    if (.not.associated(object)) then
      call cubeadm_message(seve%e,rname,'Internal error: object is not allocated')
      error = .true.
      return
    endif
    deallocate(object)  ! NB: deallocation is polymorphic: we are deallocating
                        ! a uv_t, which invokes implicitly its FINAL procedure
  end subroutine cubeadm_uv_deallocate
end module cubeadm_engine_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
