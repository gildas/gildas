!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeadm_memory
  use cubetools_parameters
  use cubetools_structure
  use cubeadm_messaging
  !
  public :: cubeadm_memory_register
  private
  !
  type :: memory_comm_t
     type(option_t), pointer :: memory
  end type memory_comm_t
  type(memory_comm_t) :: comm
  !
contains
  !
  subroutine cubeadm_memory_register(error)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    character(len=*), parameter :: comm_abstract = &
         'Display the current memory and disk footprint'
    character(len=*), parameter :: comm_help = strg_id
    character(len=*), parameter :: rname='MEMORY>REGISTER'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'MEMORY','',&
         comm_abstract,&
         comm_help,&
         cubeadm_memory_command,&
         comm%memory,error)
    if (error) return
  end subroutine cubeadm_memory_register
  !
  subroutine cubeadm_memory_command(line,error)
    use cubetools_format
    use cubedag_dag
    !---------------------------------------------------------------------
    ! Support for command
    !  ADM\MEMORY
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='MEMORY>COMMAND'
    !
    call cubeadm_message(seve%r,rname,  &
      'DAG size in memory is '//cubetools_format_memsize(cubedag_dag_memsize()))
    call cubeadm_message(seve%r,rname,  &
      'DAG size on disk   is '//cubetools_format_memsize(cubedag_dag_disksize()))
  end subroutine cubeadm_memory_command
end module cubeadm_memory
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
