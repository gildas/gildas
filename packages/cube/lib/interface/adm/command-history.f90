!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeadm_history
  use cubetuple_format
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_messaging
  !
  public :: history
  public :: cubeadm_history_docube
  private
  !
  type :: history_comm_t
     type(option_t),     pointer :: comm     ! HISTORY
     type(option_t),     pointer :: cube     ! /CUBE
     type(option_t),     pointer :: command  ! /COMMAND
     type(cubeid_arg_t), pointer :: node     ! /CUBE or /COMMAND argument
  contains
    procedure, public  :: register => cubeadm_history_register
    procedure, private :: parse    => cubeadm_history_parse
    procedure, private :: main     => cubeadm_history_main
  end type history_comm_t
  type(history_comm_t) :: history
  !
  type history_user_t
    logical             :: docube  ! Display parent cubes of a given cube
    logical             :: docomm  ! Display parent commands of a given cube
    type(cubeid_user_t) :: cubeid  ! The cube ID
  contains
    procedure, private :: toprog => cubeadm_history_user_toprog
  end type history_user_t
  !
  type history_prog_t
    logical                  :: docube  ! Display parent cubes of a given cube
    logical                  :: docomm  ! Display parent commands of a given cube
    class(format_t), pointer :: node    ! The starting point
  contains
    procedure, public :: do => cubeadm_history_do
  end type history_prog_t
  !
contains
  !
  subroutine cubeadm_history_command(line,error)
    !-------------------------------------------------------------------
    ! Support routine for command HISTORY
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(history_user_t) :: user
    character(len=*), parameter :: rname='HISTORY>COMMAND'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call history%parse(line,user,error)
    if (error) return
    call history%main(user,error)
    if (error) return
  end subroutine cubeadm_history_command
  !
  !---------------------------------------------------------------------
  !
  subroutine cubeadm_history_register(history,error)
    use cubedag_allflags
    !---------------------------------------------------------------------
    ! Register ADM\HISTORY command and its options
    !---------------------------------------------------------------------
    class(history_comm_t), intent(inout) :: history
    logical,               intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    character(len=*), parameter :: comm_abstract = &
         'Display and interact with the processing history'
    character(len=*), parameter :: comm_help = strg_id
    character(len=*), parameter :: rname='HISTORY>REGISTER'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'HISTORY','',&
         comm_abstract,&
         comm_help,&
         cubeadm_history_command,&
         history%comm,error)
    if (error) return
    !
    call cubetools_register_option(&
         'CUBE','CubeId',&
         'Display parent cubes of this cube',&
         strg_id,&
         history%cube,error)
    if (error) return
    call cubearg%register(&
         'CUBE',&
         'The starting point cube',&
         strg_id,&
         code_arg_optional,&
         [flag_any],&
         code_read_none,&
         code_access_any,&
         history%node,&
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'COMMAND','CubeId',&
         'Display parent commands of this cube',&
         strg_id,&
         history%command,error)
    if (error) return
    call cubearg%register(&
         'CUBE',&
         'The starting point cube',&
         strg_id,&
         code_arg_optional,&
         [flag_any],&
         code_read_none,&
         code_access_any,&
         history%node,&
         error)
    if (error) return
  end subroutine cubeadm_history_register
  !
  subroutine cubeadm_history_parse(history,line,user,error)
    use cubetools_disambiguate
    !-------------------------------------------------------------------
    ! Parsing facility for command HISTORY
    !-------------------------------------------------------------------
    class(history_comm_t), intent(inout) :: history
    character(len=*),      intent(in)    :: line
    type(history_user_t),  intent(out)   :: user
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='HISTORY>PARSE'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call history%cube%present(line,user%docube,error)
    if (error) return
    call history%command%present(line,user%docomm,error)
    if (error) return
    if (user%docube .and. user%docomm) then
      call cubeadm_message(seve%e,rname,'/CUBE and /COMMAND are exclusive')
      error = .true.
      return
    endif
    !
    if (user%docube) then
      call cubeadm_cubeid_parse(line,history%cube,user%cubeid,error)
      if (error) return
    endif
    if (user%docomm) then
      call cubeadm_cubeid_parse(line,history%command,user%cubeid,error)
      if (error) return
    endif
  end subroutine cubeadm_history_parse
  !
  subroutine cubeadm_history_main(history,user,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    class(history_comm_t), intent(in)    :: history
    type(history_user_t),  intent(in)    :: user
    logical,               intent(inout) :: error
    !
    type(history_prog_t) :: prog
    character(len=*), parameter :: rname='HISTORY>MAIN'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call user%toprog(prog,error)
    if (error) return
    call prog%do(error)
    if (error) return
  end subroutine cubeadm_history_main
  !
  subroutine cubeadm_history_user_toprog(user,prog,error)
    use cubeadm_get
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    class(history_user_t), intent(in)    :: user
    type(history_prog_t),  intent(out)   :: prog
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='HISTORY>USER2PROG'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    prog%docube = user%docube
    if (prog%docube) then
      ! Note that the header is not read thanks to code_read_none
      call cubeadm_get_fheader(history%node,user%cubeid,prog%node,error)
      if (error) return
    endif
    !
    prog%docomm = user%docomm
    if (prog%docomm) then
      ! Note that the header is not read thanks to code_read_none
      call cubeadm_get_fheader(history%node,user%cubeid,prog%node,error)
      if (error) return
    endif
  end subroutine cubeadm_history_user_toprog
  !
  subroutine cubeadm_history_do(prog,error)
    use cubedag_history
    !---------------------------------------------------------------------
    ! Support for command ADM\HISTORY
    !---------------------------------------------------------------------
    class(history_prog_t), intent(in)    :: prog
    logical,               intent(inout) :: error
    !
    if (prog%docube) then
      call cubeadm_history_docube(prog%node%get_id(),error)
      if (error) return
    elseif (prog%docomm) then
      call cubeadm_history_docomm(prog%node%get_id(),error)
      if (error) return
    else
      call cubedag_history_list_hx(error)
      if (error) return
    endif
  end subroutine cubeadm_history_do
  !
  subroutine cubeadm_history_docube(id,error)
    use cubedag_parameters
    use cubedag_dag
    use cubedag_link_type
    use cubedag_node_type
    use cubedag_list
    use cubedag_walker
    !---------------------------------------------------------------------
    ! Support for command ADM\HISTORY /CUBE
    !---------------------------------------------------------------------
    integer(kind=iden_l), intent(in)    :: id
    logical,              intent(inout) :: error
    !
    class(cubedag_node_object_t), pointer :: object
    type(cubedag_link_t) :: link
    !
    call cubedag_dag_get_object(id,object,error)
    if (error) return
    call cubedag_parentwalker_reset(object,cubedag_walker_null,error)
    if (error) return
    !
    link%n = 0
    do while (cubedag_parentwalker_next(object))
      ! print *,'Found parent: ',object%node%id
      if (object%node%id.eq.0)  cycle  ! Skip root node
      call link%reallocate(link%n+1,error)
      if (error) return
      link%n = link%n+1
      link%list(link%n)%p => object
    enddo
    !
    call cubedag_list_link(link,.true.,['DEFAULT','PARENTS'],error)
    if (error) return
    call link%final(error)
    if (error) return
  end subroutine cubeadm_history_docube
  !
  subroutine cubeadm_history_docomm(id,error)
    use cubedag_parameters
    use cubedag_dag
    use cubedag_link_type
    use cubedag_node_type
    use cubedag_history_types
    use cubedag_history
    use cubedag_walker
    !---------------------------------------------------------------------
    ! Support for command ADM\HISTORY /COMMAND
    !---------------------------------------------------------------------
    integer(kind=iden_l), intent(in)    :: id
    logical,              intent(inout) :: error
    !
    class(cubedag_node_object_t), pointer :: object
    integer(kind=entr_k) :: ihist
    type(history_optimize_t) :: hoptx
    !
    call cubedag_dag_get_object(id,object,error)
    if (error) return
    call cubedag_parentwalker_reset(object,cubedag_walker_null,error)
    if (error) return
    !
mainloop: do while (cubedag_parentwalker_next(object))
      ! ZZZ can we imagine an history walker? Hard to say as the
      !     cubes are the links between commands
      ! print *,'Found parent: ',object%node%id
      if (object%node%id.eq.0)  cycle  ! Skip root node
      !
      ! Discard duplicate history entries
      do ihist=1,hoptx%next-1  ! ZZZ This is N^2 loop...
        if (object%node%history .eq. hoptx%id(ihist))  cycle mainloop
      enddo
      !
      call hoptx%reallocate(hoptx%next,error)
      if (error) return
      call cubedag_history_add_fromhx(hoptx,object%node%history,error)
      if (error) return
    enddo mainloop
    !
    call hoptx%list(error)
    if (error) return
  end subroutine cubeadm_history_docomm
  !
end module cubeadm_history
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
