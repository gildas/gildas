!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeadm_list
  use cubetools_structure
  use cubetools_keywordlist_types
  use cubeadm_messaging
  use cubeadm_cubeid_types
  !
  public :: cubeadm_list_register
  private
  !
  type :: list_comm_t
     type(option_t),      pointer :: list        
     type(cubeid_arg_t),  pointer :: listarg
     type(option_t),      pointer :: index
     type(keywordlist_comm_t), pointer :: index_arg
     type(option_t),      pointer :: toc
     type(keywordlist_comm_t), pointer :: toctopic_arg
     type(option_t),      pointer :: page        
     type(option_t),      pointer :: columns     
     type(keywordlist_comm_t), pointer :: column_arg
     type(option_t),      pointer :: file
     type(option_t),      pointer :: variable    
  end type list_comm_t
  type(list_comm_t) :: comm
  !
  ! *** JP: Can't we use a generic interface mecanism to alias the definition?
  !
contains
  subroutine cubeadm_list_register(error)
    use cubetools_setup_types
    use cubedag_allflags
    use cubedag_list
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    logical,          intent(inout) :: error
    !
    type(cubeid_arg_t)   :: cubearg
    type(standard_arg_t) :: stdarg
    type(keywordlist_comm_t)  :: keyarg
    character(len=*), parameter :: comm_abstract = &
         'List the cubes in the default index or the given index'
    character(len=*), parameter :: comm_help = &
         'LIST is used for a quick look to cube headers, in a more or&
         & less detailed format. A medium-sized format is used by&
         & default, this can be changed with option /COLUMNS. If the&
         & list is too large it can be displayed page by page with&
         & option /PAGE. The list can be output to an ASCII file with&
         & option /FILE.'
    character(len=*), parameter :: rname='LIST>REGISTER'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'LIST','[cube]',&
         comm_abstract,&
         comm_help,&
         cubeadm_list_command,&
         comm%list,error)
    if (error) return
    call cubearg%register( &
         'CUBE', &
         'Cube identifier pattern',  &
         strg_id,&
         code_arg_optional,  &
         [flag_any], &
         code_read_none, &
         code_access_any,&
         comm%listarg,&
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'INDEX','IndexName',&
         'Override the default index to be listed',&
         strg_id,&
         comm%index,error)
    if (error) return
    call keyarg%register( &
         'IndexName',  &
         'Name of the index to be listed', &
         strg_id,&
         code_arg_mandatory, &
         indexname, &
         .not.flexible, &
         comm%index_arg,&
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'TOC','topic',&
         'Table Of Contents of the index',&
         'A summary of the different values of each topic is made&
         &, plus a final summary of all the combinations (setups).&
         By default, all topics are used. The table of contents is also&
         & saved in a SIC structure. The default name for the&
         & structure is TOC%, this can be changed by using the option&
         & /VARIABLE. The content of the structure is updated after&
         & each call to the command LIST /TOC. One must take care&
         & that subsequent calls to FIND can lead to values in TOC%&
         & that are inconsistent with the actual index.',&
         comm%toc,error)
    if (error) return
    call keyarg%register(&
         'topic', &
         'Topic(s) over which build TOC',&
         strg_id, &
         code_arg_unlimited,&
         cubedag_list_toc_keys, &
         .not.flexible, &
         comm%toctopic_arg,&
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'PAGE','',&
         'Page through the list',&
         strg_id,&
         comm%page,error)
    if (error) return
    !
    call cubetools_register_option(&
         'COLUMNS','col1 col2 ... coln',&
         'Define columns to be listed',&
         strg_id,&
         comm%columns,error)
    if (error) return
    call keyarg%register( &
         'column',  &
         'Column(s) to be listed', &
         strg_id,&
         code_arg_unlimited, &
         columns, &
         .not.flexible, &
         comm%column_arg, &
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'FILE','OutputFile',&
         'Define an output file for the list',&
         strg_id,&
         comm%file,error)
    if (error) return
    call stdarg%register( &
         'OutputFile',  &
         'Name of the file to store the list', &
         strg_id,&
         code_arg_mandatory, &
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'VARIABLE','varname',&
         'Define the name of the SIC variable containing the TOC',&
         strg_id,&
         comm%variable,error)
    if (error) return
    call stdarg%register( &
         'varname',  &
         'Name of the SIC variable to store the TOC', &
         strg_id,&
         code_arg_mandatory, &
         error)
    if (error) return
  end subroutine cubeadm_list_register
  !
  subroutine cubeadm_list_command(line,error)
    use cubeadm_setup
    use cubedag_list
    !---------------------------------------------------------------------
    ! Support for command
    !  ADM\LIST
    !---------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(cubedag_list_user_t) :: user
    !
    call cubeadm_list_parse(cubset,line,user,error)
    if (error) return
    call cubedag_list_command(user,error)
    if (error) return
  end subroutine cubeadm_list_command
  !
  subroutine cubeadm_list_parse(cubset,line,user,error)
    use cubetools_setup_types
    use cubedag_list
    use cubetools_disambiguate
    use cubeadm_cubeid_types
    !---------------------------------------------------------------------
    ! Parsing for ADM\LIST
    !   LIST [CubeID]
    !     [/INDEX DAG|CURRENT]
    !     [/TOC]
    !     [/PAGE]
    !     [/COLUMNS List]
    !     [/FILE OutputFile]
    !     [/VARIABLE VarName]
    !---------------------------------------------------------------------
    type(cube_setup_t),        intent(in)    :: cubset
    character(len=*), target,  intent(in)    :: line
    type(cubedag_list_user_t), intent(out)   :: user
    logical,                   intent(inout) :: error
    !
    integer(kind=4) :: narg,ncol,iarg
    character(len=argu_l) :: argu,key
    type(cubeid_user_t) :: cubeid
    type(cubeid_arg_t) :: dummy
    character(len=*), parameter :: rname='LIST>PARSE'
    !
    ! Command argument. Build the user%find structure to restrict
    ! the search
    narg = comm%list%getnarg()
    if (narg.ge.1) then
      call cubeadm_cubeid_parse(line,comm%list,cubeid,error)
      if (error) return
      call cubeadm_cubeid_user2find(dummy,cubeid%cube(1),user%find,error)
      if (error) return
    else
      ! user%find => Keep defaults
    endif
    !
    ! /INDEX
    narg = comm%index%getnarg()
    if (narg.ge.1) then
       call cubetools_getarg(line,comm%index,1,argu,mandatory,error)
       if (error) return
       call cubetools_keywordlist_user2prog(comm%index_arg,argu,user%index,key,error)
       if (error) return
    else
       user%index = cubset%index%default
    endif
    !
    ! /FILE
    call comm%file%present(line,user%dofile,error)
    if (error) return
    if (user%dofile) then
       call cubetools_getarg(line,comm%file,1,user%file,mandatory,error)
       if (error) return
    endif
    !
    ! /TOC
    call comm%toc%present(line,user%dotoc,error)
    if (error) return
    if (user%dotoc) then
       user%line => line
       user%iopttoc = comm%toc%inum
       call comm%variable%present(line,user%dovar,error)
       if (error) return
       if (user%dovar) then
          call cubetools_getarg(line,comm%variable,1,user%tocvar,mandatory,error)
          if (error) return
       else
          user%tocvar = 'TOC'
       endif
    else ! /COLUMN, /PAGE
       narg = comm%columns%getnarg()
       ncol = size(user%ucols)
       if (narg.gt.ncol) then
          call cubeadm_message(seve%w,rname,'/COLUMNS list too long, truncated')
       endif
       user%ncol = max(1,min(narg,ncol))
       do iarg = 1,user%ncol
          user%ucols(iarg) = 'DEFAULT'
          call cubetools_getarg(line,comm%columns,iarg,user%ucols(iarg),.not.mandatory,error)
          if (error) return
       enddo
       call comm%page%present(line,user%dopage,error)
       if (error) return
    endif
  end subroutine cubeadm_list_parse
end module cubeadm_list
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
