!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeadm_library
  use cubeadm_directory_type
  use cubeadm_engine_types
  use cubeadm_snapshot
  use cubeadm_timing
  !
  public :: cubeadm_library_init,cubeadm_library_exit
  private
  !
contains
  !
  subroutine cubeadm_library_init(error)
    use gkernel_interfaces
    use cubedag_library
    use cubedag_type
    !---------------------------------------------------------------------
    ! Initialize the ADM library
    !---------------------------------------------------------------------
    logical, intent(inout) :: error
    ! Local
    integer(kind=4) :: ier
    logical :: autoreimport
    logical, parameter :: merge=.false.
    !
    ! Initialization of Directories list buffer
    call dir%init(error)
    if (error) return
    call dir%sicdef(error)
    if (error) return
    !
    call cubedag_library_init(error)
    if (error) return
    call cubeadm_types_register(error)
    if (error) return
    !
    ! Implicit import of the previous snapshot, if any
    autoreimport = .true.
    ier = sic_getlog('CUBE_REIMPORT_AUTO',autoreimport)
    if (autoreimport) then
      call cubeadm_snapshot_reimport(cubeadm_snapshot_dagname(),  &
                                    cubeadm_snapshot_histname(),  &
                                    merge,                        &
                                    .true.,error)
      if (error) then
        ! We should not stop the program initialization...
        error = .false.
      endif
    endif
    !
    call cubeadm_timing_variables(error)
    if (error) return
    !
  end subroutine cubeadm_library_init
  !
  subroutine cubeadm_library_exit(error)
    use cubedag_library
    !---------------------------------------------------------------------
    ! Exit the ADM library
    !---------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    call cubedag_library_exit(error)
    if (error) continue
  end subroutine cubeadm_library_exit

end module cubeadm_library
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
