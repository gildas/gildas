!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeadm_free
  use cubeadm_messaging
  use cubetools_structure
  !
  public :: free
  public :: free_prog_t
  private
  !
  type :: free_comm_t
    type(option_t), pointer :: comm
  contains
    procedure, public  :: register => cubeadm_free_register
    procedure, private :: parse    => cubeadm_free_parse
    procedure, private :: main     => cubeadm_free_main
  end type free_comm_t
  type(free_comm_t) :: free
  !
  type :: free_user_t
    ! No customization yet
  contains
    procedure, private :: toprog => cubeadm_free_user_toprog
  end type free_user_t
  !
  type :: free_prog_t
    ! No customization yet
  contains
    procedure, public :: do => cubeadm_free_do
  end type free_prog_t
  !
contains
  !
  subroutine cubeadm_free_command(line,error)
    !-------------------------------------------------------------------
    ! Support for command
    !  ADM\FREE
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(free_user_t) :: user
    character(len=*), parameter :: rname='FREE>COMMAND'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call free%parse(line,user,error)
    if (error) return
    call free%main(user,error)
    if (error) return
  end subroutine cubeadm_free_command
  !
  !---------------------------------------------------------------------
  !
  subroutine cubeadm_free_register(free,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(free_comm_t), intent(inout) :: free
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: comm_abstract = &
         'Free the current DAG'
    character(len=*), parameter :: comm_help = &
         'Free the DAG, i.e. delete all the cube and history&
         & structures. Beware memory-only objects are lost during&
         & this operation (use command SNAPSHOT to save them for&
         & possible later reuse). In return the DAG is ready for use&
         & as if starting from scratch.'
    character(len=*), parameter :: rname='FREE>REGISTER'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'FREE','',&
         comm_abstract,&
         comm_help,&
         cubeadm_free_command,&
         free%comm,error)
    if (error) return
  end subroutine cubeadm_free_register
  !
  subroutine cubeadm_free_parse(free,line,user,error)
    !-------------------------------------------------------------------
    !-------------------------------------------------------------------
    class(free_comm_t), intent(inout) :: free
    character(len=*),   intent(in)    :: line
    type(free_user_t),  intent(out)   :: user
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: rname='FREE>PARSE'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
  end subroutine cubeadm_free_parse
  !
  subroutine cubeadm_free_main(free,user,error)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(free_comm_t), intent(in)    :: free
    type(free_user_t),  intent(in)    :: user
    logical,            intent(inout) :: error
    !
    type(free_prog_t) :: prog
    character(len=*), parameter :: rname='FREE>MAIN'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call user%toprog(prog,error)
    if (error) return
    call prog%do(error)
    if (error) return
  end subroutine cubeadm_free_main
  !
  subroutine cubeadm_free_user_toprog(user,prog,error)
    !-------------------------------------------------------------------
    !-------------------------------------------------------------------
    class(free_user_t), intent(in)    :: user
    type(free_prog_t),  intent(out)   :: prog
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: rname='FREE>USER>TOPROG'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
  end subroutine cubeadm_free_user_toprog
  !
  subroutine cubeadm_free_do(prog,error)
    use cubedag_repositories
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(free_prog_t), intent(in)    :: prog
    logical,            intent(inout) :: error
    !
    call cubedag_repositories_free(error)
    if (error) continue
    call cubedag_repositories_init(error)
    if (error) return
  end subroutine cubeadm_free_do
  !
end module cubeadm_free
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
