module cubeadm_update
  use cubetools_structure
  use cubetools_keywordlist_types
  use cubetools_access_types
  use cubedag_allflags
  use cubetuple_format
  use cubeadm_messaging
  use cubeadm_cubeid_types
  use cubeadm_setup
  !
  public :: update
  private
  !
  integer(kind=code_k), parameter :: flags_action_set=1     ! =
  integer(kind=code_k), parameter :: flags_action_remove=2  ! -
  integer(kind=code_k), parameter :: flags_action_append=3  ! >
  integer(kind=code_k), parameter :: flags_action_prepend=4 ! <
  integer(kind=4),      parameter :: nflags_codes=4
  character(len=1),     parameter :: flags_codes(nflags_codes) =  &
    (/ '=','-','>','<' /)
  integer(kind=code_k), parameter :: flags_actions(nflags_codes) =  &
    (/ flags_action_set,     &
       flags_action_remove,  &
       flags_action_append,  &
       flags_action_prepend /)
  !
  type :: update_comm_t
     type(option_t),      pointer :: comm
     type(cubeid_arg_t),  pointer :: cube
     type(option_t),      pointer :: flags
     type(option_t),      pointer :: family
     type(option_t),      pointer :: index
     type(keywordlist_comm_t), pointer :: index_arg
     type(order_comm_t)           :: set
     type(option_t),      pointer :: axset
  contains
     procedure, public  :: register => cubeadm_update_register
     procedure, private :: parse    => cubeadm_update_parse
     procedure, private :: main     => cubeadm_update_main
  end type update_comm_t
  type(update_comm_t) :: update
  !
  type update_user_t
     type(cubeid_user_t)   :: id
     character(len=argu_l) :: family
     logical               :: dofamily
     character(len=argu_l) :: flags
     logical               :: doflag
     logical               :: doindex
     character(len=argu_l) :: index
     type(order_user_t)    :: set
     logical               :: doaxset
     integer(kind=4)       :: xaxis,yaxis,caxis
  contains
     procedure, private :: toprog => cubeadm_update_user_toprog
  end type update_user_t
  !
  type update_prog_t
     class(format_t), pointer  :: cube
     character(len=argu_l)     :: family
     integer(kind=code_k)      :: flags_action=code_null
     type(flag_t), allocatable :: flags(:)
     integer(kind=code_k)      :: index
     type(order_prog_t)        :: set
     integer(kind=4)           :: xaxis,yaxis,caxis
  contains
     procedure, private :: do => cubeadm_update_do
  end type update_prog_t
  !
  ! UPDATE currid /FLAG +-= progflag,progflag,progflag  ! Here order matter
  !               /FAMILY family
contains
  !
  subroutine cubeadm_update_register(update,error)
    use cubedag_parameters
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(update_comm_t), intent(inout) :: update
    logical,              intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    type(standard_arg_t) :: stdarg
    type(keywordlist_comm_t) :: keyarg
    character(len=*), parameter :: comm_abstract = &
         'Update family name and/or flags'
    character(len=*), parameter :: comm_help = strg_id
    character(len=*), parameter :: rname='UPDATE>REGISTER'
    !
    call cubeadm_message(admseve%trace,rname,'welcome')
    !
    call cubetools_register_command(&
         'UPDATE','[cube]',&
         comm_abstract,&
         comm_help,&
         cubeadm_update_command,&
         update%comm,error)
    if (error) return
    call cubearg%register( &
         'CUBE', &
         'Cube to be updated',  &
         strg_id,&
         code_arg_optional,  &
         [flag_any], &
         code_read_head, &
         code_access_any, &
         update%cube, &
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'FAMILY','NewFamily',&
         'Specify new family name',&
         strg_id,&
         update%family,error)
    if (error) return
    call stdarg%register( &
         'NewFamily', &
         'New family name',  &
         strg_id,&
         code_arg_optional,  &
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'FLAGS','[operator]flag1,...,flagN',&
         'Add, set, or remove flags',&
         'See argument help for complete syntax.',&
         update%flags,error)
    if (error) return
    call stdarg%register( &
         'flag', &
         'List of flags to be added, set, or removed',  &
         'Coma-separated list of flags. A character operator can be added&
         & in front of the list to specify the action: = uses the exact list,&
         & - to removed the named flags, > to append them, < to prepend.&
         & Default is =. For example if the cube currently have &
         & moment,area,signal flags:'//strg_cr//&
         '  CUBE> UPDATE /FLAGS =cube     will replace all the flags with cube only,'//strg_cr//&
         '  CUBE> UPDATE /FLAGS -area     will result in moment,signal only,'//strg_cr//&
         '  CUBE> UPDATE /FLAGS <modified will end with modified,moment,area,signal'//strg_cr//&
         '  CUBE> UPDATE /FLAGS >1        will end with moment,area,signal,1'//strg_cr//&
         strg_cr//&
         &'WARNING: The order of the flags is important. To see a&
         & list of all available flags use ADM\FLAG.',&
         code_arg_mandatory,  &
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'INDEX','[IndexName]',&
         'Update all cubes from default or named index',&
         strg_id,&
         update%index,error)
    if (error) return
    call keyarg%register( &
         'IndexName',  &
         'Name of the index to be udpated', &
         'If absent, default index is used.',&
         code_arg_optional, &
         indexname, &
         .not.flexible, &
         update%index_arg,&
         error)
    if (error) return
    !
    call update%set%register(&
         'SET',&
         'Force the cube to be considered as the given set',&
         strg_id,&
         error)
    if (error)  return
    !
    call cubetools_register_option(&
         'AXSET','Xnum Ynum Cnum',&
         'Specify the X Y C axes by their number',&
         'Force the IL IM IC axes to specified values. Only 1 2 3&
         & (i.e. cube is a native image set) or 3 1 2 (i.e. native&
         & spectrum set) are supported.',&
         update%axset,error)
    if (error) return
    call stdarg%register( &
         'XNUM', &
         'Axis to become new X axis',  &
         strg_id,&
         code_arg_mandatory,  &
         error)
    if (error) return
    call stdarg%register( &
         'YNUM', &
         'Axis to become new Y axis',  &
         strg_id,&
         code_arg_mandatory,  &
         error)
    if (error) return
    call stdarg%register( &
         'CNUM', &
         'Axis to become new C axis',  &
         strg_id,&
         code_arg_mandatory,  &
         error)
    if (error) return
  end subroutine cubeadm_update_register
  !
  subroutine cubeadm_update_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(update_user_t) :: user
    character(len=*), parameter :: rname='UPDATE>COMMAND'
    !
    call cubeadm_message(admseve%trace,rname,'welcome')
    !
    call update%parse(line,user,error)
    if (error) return
    call update%main(user,error)
    if (error) return
  end subroutine cubeadm_update_command
  !
  subroutine cubeadm_update_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(update_comm_t), intent(in)    :: comm
    character(len=*),     intent(in)    :: line
    type(update_user_t),  intent(out)   :: user
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='UPDATE>PARSE'
    !
    call cubeadm_message(admseve%trace,rname,'welcome')
    !
    call cubeadm_cubeid_parse(line,update%comm,user%id,error)
    if (error) return
    !
    call update%family%present(line,user%dofamily,error)
    if (error) return
    if (user%dofamily) then
       call cubetools_getarg(line,update%family,1,user%family,mandatory,error)
       if (error) return
    endif
    !
    call update%flags%present(line,user%doflag,error)
    if (error) return
    if (user%doflag) then
       call cubetools_getarg(line,update%flags,1,user%flags,mandatory,error)
       if (error) return
    endif
    !
    call update%index%present(line,user%doindex,error)
    if (error) return
    if (user%doindex .and. update%comm%getnarg().ge.1) then
      call cubeadm_message(seve%e,rname,  &
          'Option /INDEX is incompatible with a named cube passed to command')
      error = .true.
      return
    endif
    if (update%index%getnarg().ge.1) then
      call cubetools_getarg(line,update%index,1,user%index,mandatory,error)
      if (error) return
    else
      user%index = ''
    endif
    !
    call update%set%parse(line,user%set,error)
    if (error)  return
    !
    call update%axset%present(line,user%doaxset,error)
    if (error) return
    if (user%doaxset) then
       call cubetools_getarg(line,update%axset,1,user%xaxis,mandatory,error)
       if (error) return
       call cubetools_getarg(line,update%axset,2,user%yaxis,mandatory,error)
       if (error) return
       call cubetools_getarg(line,update%axset,3,user%caxis,mandatory,error)
       if (error) return
    endif
  end subroutine cubeadm_update_parse
  !
  subroutine cubeadm_update_main(update,user,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(update_comm_t), intent(in)    :: update
    type(update_user_t),  intent(in)    :: user
    logical,              intent(inout) :: error
    !
    type(update_prog_t) :: prog
    character(len=*), parameter :: rname='UPDATE>MAIN'
    !
    call cubeadm_message(admseve%trace,rname,'welcome')
    !
    call user%toprog(prog,error)
    if (error)  return
    call prog%do(error)
    if (error)  return
  end subroutine cubeadm_update_main
  !
  subroutine cubeadm_update_user_toprog(user,prog,error)
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(update_user_t), intent(in)    :: user
    type(update_prog_t),  intent(out)   :: prog
    logical,              intent(inout) :: error
    !
    integer(kind=4) :: icode,fc
    character(len=argu_l) :: key
    character(len=*), parameter :: rname='UPDATE>USER>TOPROG'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call cubeadm_get_fheader(update%cube,user%id,prog%cube,error)
    if (error) return
    !
    if (user%dofamily) then
      prog%family = user%family
    else
      prog%family = ''
    endif
    !
    if (user%doflag) then
       fc = 1
       prog%flags_action = flags_action_set  ! Default
       do icode=1,nflags_codes
         if (user%flags(1:1).eq.flags_codes(icode)) then
           fc = 2
           prog%flags_action = flags_actions(icode)
           exit
         endif
       enddo
       ! ZZZ the "allow creation of new user-defined flag" could also
       ! be switched on for implicit flag creation
       call cubedag_string_toflaglist(user%flags(fc:),.false.,prog%flags,error)
       if (error) return
    endif
    !
    if (user%doindex) then
      if (user%index.eq.'') then
        prog%index = cubset%index%default
      else
        call cubetools_keywordlist_user2prog(update%index_arg,user%index,prog%index,key,error)
        if (error) return
      endif
    else
      prog%index = code_null
    endif
    !
    call user%set%toprog(update%set,code_null,prog%set,error)
    if (error)  return
    !
    if (user%doaxset) then
      prog%xaxis = user%xaxis
      prog%yaxis = user%yaxis
      prog%caxis = user%caxis
    else
      prog%xaxis = code_null
      prog%yaxis = code_null
      prog%caxis = code_null
    endif
  end subroutine cubeadm_update_user_toprog
  !
  subroutine cubeadm_update_do(prog,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(update_prog_t), intent(inout) :: prog
    logical,              intent(inout) :: error
    !
    logical :: done
    character(len=*), parameter :: rname='UPDATE>DO'
    !
    done = .false.
    if (prog%index.ne.code_null) then
      call cubeadm_update_do_index(prog,done,error)
      if (error)  return
    else
      call cubeadm_update_do_onefile(prog,done,error)
      if (error)  return
    endif
    if (.not.done)  call cubeadm_message(seve%w,rname,'Nothing done')
  end subroutine cubeadm_update_do
  !
  subroutine cubeadm_update_do_index(prog,done,error)
    use cubedag_find
    use cubedag_link_type
    use cubedag_node_type
    !-------------------------------------------------------------------
    ! Apply the changes on the whole index
    !-------------------------------------------------------------------
    type(update_prog_t), intent(inout) :: prog
    logical,             intent(inout) :: done
    logical,             intent(inout) :: error
    !
    type(find_prog_t) :: find
    type(cubedag_link_t) :: optx
    integer(kind=4) :: iformat
    class(cubedag_node_object_t), pointer :: dno
    character(len=*), parameter :: rname='UPDATE>MAIN>INDEX'
    !
    select case (prog%index)
    case (code_index_dag)
      call find%ix2optx(optx,error)
      if (error)  return
    case (code_index_current)
      call find%cx2optx(optx,error)
      if (error)  return
    case default
      call cubedag_message(seve%e,rname,'Index is not supported')
      error = .true.
      return
    end select
    !
    do iformat=1,optx%n
      dno => cubedag_node_ptr(optx%list(iformat)%p,error)
      if (error) return
      prog%cube => cubetuple_format_ptr(dno,error)
      if (error) return
      call cubeadm_update_do_onefile(prog,done,error)
      if (error)  return
    enddo
  end subroutine cubeadm_update_do_index
  !
  subroutine cubeadm_update_do_onefile(prog,done,error)
    use cubeio_interface
    use cubedag_node
    !-------------------------------------------------------------------
    ! Apply the changes on the given format_t file
    !-------------------------------------------------------------------
    type(update_prog_t), intent(inout) :: prog
    logical,             intent(inout) :: done
    logical,             intent(inout) :: error
    !
    integer(kind=4) :: il,im,ic,code_set
    character(len=*), parameter :: rname='UPDATE>MAIN>ONEFILE'
    !
    select case (prog%flags_action)
    case (code_null)
       ! Nothing requested
       continue
    case (flags_action_set)
       call cubedag_node_set_flags(prog%cube,prog%flags,error)
       if (error) return
       done = .true.
    case (flags_action_remove)
       call prog%cube%node%flag%remove(prog%flags,error)
       if (error) return
       done = .true.
    case (flags_action_prepend)
       call prog%cube%node%flag%prepend(prog%flags,error)
       if (error) return
       done = .true.
    case (flags_action_append)
       call prog%cube%node%flag%append(prog%flags,error)
       if (error) return
       done = .true.
    case default
       call cubeadm_message(seve%e,rname,'/FLAGS action is not implemented')
       error = .true.
       return
    end select
    !
    if (prog%family.ne.'') then
       call cubedag_node_set_family(prog%cube,prog%family,error)
       if (error) return
       done = .true.
    endif
    !
    if (prog%set%code.ne.code_null) then
      code_set = prog%set%code
      if (code_set.eq.code_cube_imaset) then
        il = 1  ! ZZZ Should do it with some cubeio API ???
        im = 2
        ic = 3
      else  ! SPECTRUM
        ic = 1
        il = 2
        im = 3
      endif
    endif
    !
    if (prog%xaxis.ne.code_null) then
      il = prog%xaxis
      im = prog%yaxis
      ic = prog%caxis
      if (il.eq.1 .and. im.eq.2) then  ! ZZZ Should do it with some cubeio API ???
        code_set = code_cube_imaset
      elseif (ic.eq.1) then
        code_set = code_cube_speset
      else
        code_set = code_cube_unkset
      endif
    endif
    !
    if (prog%set%code.ne.code_null .or. prog%xaxis.ne.code_null) then
      prog%cube%head%set%il = il
      prog%cube%head%set%im = im
      prog%cube%head%set%ic = ic
      ! Update the IO desc
      call cubeio_header_put(prog%cube,code_set,prog%cube%tuple%current%desc,error)
      if (error)  return
      done = .true.
    endif
  end subroutine cubeadm_update_do_onefile
end module cubeadm_update
