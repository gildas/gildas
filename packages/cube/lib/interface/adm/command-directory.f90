!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeadm_directory
  use cubetools_parameters
  use cubetools_structure
  use cubedag_dag
  use cubeadm_messaging
  use cubeadm_directory_type
  use cubetools_keywordlist_types
  !
  public :: directory
  private
  !
  type :: directory_comm_t
     type(option_t), pointer :: comm
     type(dir_opt_t)         :: raw
     type(dir_opt_t)         :: tmp
     type(dir_opt_t)         :: red
     type(option_t), pointer :: defaults
     type(option_t), pointer :: free
     type(option_t), pointer :: merge
   contains
     procedure, public  :: register => cubeadm_directory_register
     procedure, private :: parse    => cubeadm_directory_parse
  end type directory_comm_t
  type(directory_comm_t) :: directory
  !
contains
  !
  subroutine cubeadm_directory_command(line,error)
    !----------------------------------------------------------------------
    ! DIRECTORY [RAW|TMP|RED dirname]
    !----------------------------------------------------------------------
    character(len=*), intent(in)  :: line
    logical,          intent(out) :: error
    !
    call directory%parse(line,error)
    if (error) return
  end subroutine cubeadm_directory_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubeadm_directory_register(directory,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(directory_comm_t), intent(inout) :: directory
    logical,                 intent(out)   :: error
    !
    character(len=*), parameter :: comm_abstract = &
         'Define directories where CUBE looks for files'
    character(len=*), parameter :: comm_help = &
         'Define the directories where CUBE looks for and/or stores&
         & raw (/RAW), temporary (/TMP) or reduced (/RED) cubes.&
         & Defaults are ./raw/, ./tmp/, and ./red/ respectively.'&
         & //strg_cr//strg_cr//&
         &'If the TMP directory is redefined, the DAG file available&
         & in this directory (if any) is implicitly reimported. Use&
         & option /FREE to free any previous DAG.'&
         & //strg_cr//strg_cr//&
         &'Without argument, display the current values.&
         & The current values are also visible in the SIC structure&
         & CUBE%DIR%.'
    character(len=*), parameter :: rname='DIRECTORY>REGISTER'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'DIRECTORY','',&
         comm_abstract,&
         comm_help,&
         cubeadm_directory_command,&
         directory%comm,error)
    if (error) return
    !
    call directory%raw%register('RAW',&
         'Define directory(ies) providing raw data',&
         'This can be a single directory or a semicolumn-separated&
        & list of paths, e.g. "./;./raw/"',error)
    if (error) return
    !
    call directory%tmp%register('TMP',&
         'Define directory buffering temporary data',&
         strg_id,error)
    if (error) return
    !
    call directory%red%register('RED',&
         'Define directory storing reduced data',&
         strg_id,error)
    if (error) return
    !
    call cubetools_register_option(&
         'DEFAULTS','',&
         'Reset directories to defaults',&
         strg_id,&
         directory%defaults,error)
    if (error) return
    !
    call cubetools_register_option(&
         'FREE','',&
         'Free the DAG before reimporting the one from TMP',&
         strg_id,&
         directory%free,error)
    if (error) return
    !
    call cubetools_register_option(&
         'MERGE','',&
         'Merge the snapshot in the current DAG',&
         strg_id,&
         directory%merge,error)
    if (error) return
  end subroutine cubeadm_directory_register
  !
  subroutine cubeadm_directory_parse(directory,line,error)
    use gkernel_interfaces
    use cubeadm_free
    use cubeadm_snapshot
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(directory_comm_t), intent(inout) :: directory
    character(len=*),        intent(in)    :: line
    logical,                 intent(out)   :: error
    !
    logical :: dodefaults,dofree,domerge
    type(free_prog_t) :: free_prog
    character(len=file_l) :: dagname
    character(len=*), parameter :: rname='DIRECTORY>PARSE'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    if (cubetools_nopt().eq.0) then
       call dir%list(error)
       if (error) return
    else
       call directory%defaults%present(line,dodefaults,error)
       if (error) return
       if (dodefaults) then
          call dir%init(error)
          if (error) return
       endif
       !
       call directory%raw%parse(line,error)
       if (error) return
       call directory%tmp%parse(line,error)
       if (error) return
       call directory%red%parse(line,error)
       if (error) return
       !
       call directory%free%present(line,dofree,error)
       if (error) return
       !
       call directory%merge%present(line,domerge,error)
       if (error) return
       !
       if (directory%tmp%do) then
         if (dofree) then
           call free_prog%do(error)
           if (error) return
         endif
         !
         ! Catch attempt to import while DAG is not empty, to give better
         ! feedback to user
         dagname = cubeadm_snapshot_dagname()
         if (.not.domerge .and.  &
             .not.cubedag_dag_empty() .and.  &
             gag_inquire(dagname,len_trim(dagname)).eq.0) then
           ! Current DAG is not empty, and new directory contains a DAG file
           call cubeadm_message(seve%e,rname,  &
             'Can not import '//trim(dagname)//' in a non-empty DAG')
           call cubeadm_message(seve%e,rname,  &
             'Use option /FREE to free the previous DAG or /MERGE to allow merge')
           error = .true.
           return
         endif
         call cubeadm_snapshot_reimport(dagname,  &
                                        cubeadm_snapshot_histname(),  &
                                        domerge,  &
                                        .true.,error)
         if (error) return
       else
         ! Reject /FREE and /MERGE if /TMP is absent
         if (dofree) then
           call cubeadm_message(seve%e,rname,  &
             '/FREE can only be used in conjunction with /TMP')
           error = .true.
         endif
         if (domerge) then
           call cubeadm_message(seve%e,rname,  &
             '/MERGE can only be used in conjunction with /TMP')
           error = .true.
         endif
         if (error)  return
       endif
       !
    endif
  end subroutine cubeadm_directory_parse
end module cubeadm_directory
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
