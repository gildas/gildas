!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! *** JP: This one could (should?) be an extension of cubeid_user_t => unclear
! 
module cubeadm_ancillary_cube_types
  use cubetools_parameters
  use cubetools_structure
  use cube_types
  use cubeadm_messaging
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  !
  public :: ancillary_cube_comm_t,ancillary_cube_user_t,ancillary_cube_prog_t
  private
  !
  type ancillary_cube_comm_t
     type(option_t), pointer :: key
     type(cubeid_arg_t), pointer :: cube
   contains
     procedure, public :: fully_register => cubeadm_ancillary_cube_comm_fully_register
     procedure, public :: parse          => cubeadm_ancillary_cube_comm_parse
  end type ancillary_cube_comm_t
  !
  type ancillary_cube_user_t
     logical             :: present = .false.
     type(cubeid_user_t) :: id
   contains
     procedure, public :: toprog => cubeadm_ancillary_cube_user_toprog
     procedure, public :: list   => cubeadm_ancillary_cube_user_list
  end type ancillary_cube_user_t
  !
  type ancillary_cube_prog_t
     character(len=80), private :: abstract = strg_unk ! ***JP: abst_l=80 is defined in structure-argument.f90!
     logical               :: do = .false.
     type(cube_t), pointer :: cube
   contains
    procedure, public :: list => cubeadm_ancillary_cube_prog_list
  end type ancillary_cube_prog_t
  !
contains
  !
  subroutine cubeadm_ancillary_cube_comm_fully_register(comm,&
       keyname,keysyntax,keyabstract,keyhelp,&
       cubename,cubeabstract,cubeflags,&
       cubemandatory,cubestatus,cubeaccess,error)
    use cubedag_flag
    !----------------------------------------------------------------------
    ! Register a "/KEYNAME [cubeid]" key
    !----------------------------------------------------------------------
    class(ancillary_cube_comm_t), intent(inout) :: comm
    character(len=*),             intent(in)    :: keyname
    character(len=*),             intent(in)    :: keysyntax
    character(len=*),             intent(in)    :: keyabstract
    character(len=*),             intent(in)    :: keyhelp
    character(len=*),             intent(in)    :: cubename
    character(len=*),             intent(in)    :: cubeabstract
    type(flag_t),                 intent(in)    :: cubeflags(:)
    integer(kind=code_k),         intent(in)    :: cubemandatory
    integer(kind=code_k),         intent(in)    :: cubestatus
    integer(kind=code_k),         intent(in)    :: cubeaccess
    logical,                      intent(inout) :: error
    !
    type(cubeid_arg_t) :: incube
    character(len=*), parameter :: rname='ANCILLARY>CUBE>COMM>FULLY>REGISTER'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         keyname,&
         keysyntax,&
         keyabstract,&
         keyhelp,&
         comm%key,&
         error)
    if (error) return
    call incube%register(&
         cubename,&
         cubeabstract,&
         strg_id,&
         cubemandatory,&
         cubeflags,&
         cubestatus,&
         cubeaccess,&
         comm%cube,&
         error)
    if (error) return
  end subroutine cubeadm_ancillary_cube_comm_fully_register
  !
  subroutine cubeadm_ancillary_cube_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! /KEYNAME [cubeid]
    !----------------------------------------------------------------------
    class(ancillary_cube_comm_t),  intent(in)    :: comm
    character(len=*),              intent(in)    :: line
    class(ancillary_cube_user_t),  intent(inout) :: user
    logical,                       intent(inout) :: error
    !
    character(len=*), parameter :: rname='ANCILLARY>CUBE>COMM>PARSE'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call comm%key%present(line,user%present,error)
    if (error) return
    if (user%present) then
       call cubeadm_cubeid_parse(line,comm%key,user%id,error)
       if (error) return
    endif
  end subroutine cubeadm_ancillary_cube_comm_parse
  !
  !------------------------------------------------------------------------
  !
  subroutine cubeadm_ancillary_cube_user_init(user,error)
    !----------------------------------------------------------------------
    ! Initialize by setting the intent of ancillary_cube_user_t to out
    !----------------------------------------------------------------------
    class(ancillary_cube_user_t), intent(out)   :: user
    logical,                      intent(inout) :: error
    !
    character(len=*), parameter :: rname='ANCILLARY>CUBE>USER>INIT'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
  end subroutine cubeadm_ancillary_cube_user_init
  !
  subroutine cubeadm_ancillary_cube_user_toprog(user,comm,prog,error,access)
    use cubeadm_get
    !----------------------------------------------------------------------
    ! Resolve cubeid into a cube
    !----------------------------------------------------------------------
    class(ancillary_cube_user_t),   intent(in)    :: user
    class(ancillary_cube_comm_t),   intent(in)    :: comm
    class(ancillary_cube_prog_t),   intent(inout) :: prog
    logical,                        intent(inout) :: error
    integer(kind=code_k), optional, intent(in)    :: access
    !
    character(len=*), parameter :: rname='ANCILLARY>CUBE>USER>TOPROG'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    prog%do = user%present
    prog%abstract = comm%key%abstract
    if (prog%do) then
       call cubeadm_get_header(comm%cube,user%id,prog%cube,error,access)
       if (error) return
    endif
  end subroutine cubeadm_ancillary_cube_user_toprog
  !
  subroutine cubeadm_ancillary_cube_user_list(user,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(ancillary_cube_user_t), intent(in)    :: user
    logical,                      intent(inout) :: error
    !
    character(len=*), parameter :: rname='ANCILLARY>CUBE>USER>LIST'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    ! ***JP: to be written
  end subroutine cubeadm_ancillary_cube_user_list
  !
  !------------------------------------------------------------------------
  !
  subroutine cubeadm_ancillary_cube_prog_list(prog,key,error)
    use cubetools_format
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(ancillary_cube_prog_t), intent(in)    :: prog
    character(len=*),             intent(in)    :: key
    logical,                      intent(inout) :: error
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='ANCILLARY>CUBE>PROG>LIST'
    !
    call cubeadm_message(admseve%trace,rname,'Welcome')
    !
    call cubeadm_message(seve%r,rname,'')
    mess = cubetools_format_stdkey_boldval(key,'Cube Mask',40)
    call cubeadm_message(seve%r,rname,mess)
    ! ***JP: And then I should have something like:
    !I MASK mask                * =>  2 3D ch3oh21-s1-fts mask      30M HORSEHEAD ch3oh21 3.75 kiB    
  end subroutine cubeadm_ancillary_cube_prog_list
end module cubeadm_ancillary_cube_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
