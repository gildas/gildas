 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_header_methods
  use cubetools_parameters
  use cubetools_messaging
  use cubetools_header_types
  !
  public :: cubetools_header_beamradius
  !
  public :: cubetools_header_get_size_x,cubetools_header_get_size_y
  !
  public :: cubetools_header_get_nchan,cubetools_header_get_line
  public :: cubetools_header_put_nchan,cubetools_header_put_line
  public :: cubetools_header_put_nxny
  !
  public :: cubetools_header_get_array_shape
  public :: cubetools_header_get_array_unit,cubetools_header_get_array_minmax,cubetools_header_get_array_ngood
  public :: cubetools_header_put_array_unit,cubetools_header_put_array_minmax,cubetools_header_put_array_ngood
  public :: cubetools_header_make_array_real,cubetools_header_make_array_cplx
  !
  public :: cubetools_header_point2axis
  !
  public :: cubetools_header_get_axis_head,cubetools_header_update_axset_axis
  public :: cubetools_header_get_axis_head_f
  public :: cubetools_header_get_axis_head_v
  public :: cubetools_header_get_axis_head_c,cubetools_header_update_axset_c,cubetools_header_nullify_axset_c
  public :: cubetools_header_get_axis_head_l,cubetools_header_update_axset_l,cubetools_header_nullify_axset_l
  public :: cubetools_header_get_axis_head_m,cubetools_header_update_axset_m,cubetools_header_nullify_axset_m
  !
  public :: cubetools_header_get_spapro,cubetools_header_spatial_pixel2offset
  public :: cubetools_header_put_spapro,cubetools_header_spatial_offset2pixel
  public :: cubetools_header_nullify_spapro
  !
  public :: cubetools_header_spectral_freq2chan,cubetools_header_spectral_chan2freq
  !
  public :: cubetools_header_get_spabeam,cubetools_header_update_spabeam
  !
  public :: cubetools_header_multiply_spectral_spacing
  public :: cubetools_header_rederive_spectral_axes
  public :: cubetools_header_update_frequency_from_user,cubetools_header_update_velocity_from_user
  public :: cubetools_header_update_frequency_from_axis,cubetools_header_update_velocity_from_axis
  !
  public :: cubetools_header_spatial_like
  public :: cubetools_header_spectral_like
  !
  public :: cubetools_header_add_observatories
  !
  public :: cubetools_header_brightness2brightness,cubetools_header_brightness2flux
  public :: cubetools_header_flux2flux
  !
  public :: cubetools_header_get_rest_wavelength
  public :: cubetools_header_get_rest_frequency,cubetools_header_modify_rest_frequency
  public :: cubetools_header_get_frame_velocity,cubetools_header_modify_frame_velocity
  !
  public :: cubetools_header_beam_to_table
  !
  public :: cubetools_header_extrema_init
  public :: cubetools_header_extrema_update_image
  public :: cubetools_header_extrema_update_spectrum
  public :: cubetools_header_extrema_update_subcube
  !
  private
  !
contains
  !
  subroutine cubetools_header_beamradius(head,radius,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t), intent(in)    :: head
    real(kind=beam_k),   intent(out)   :: radius
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>BEAMRADIUS'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    radius = 0.5*sqrt(head%spa%bea%major*head%spa%bea%minor)
  end subroutine cubetools_header_beamradius
  !
  subroutine cubetools_header_get_size_x(head,size,error)
    use cubetools_axis_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t), target, intent(in)    :: head
    real(kind=coor_k),           intent(out)   :: size
    logical,                     intent(inout) :: error
    !
    type(axis_t), pointer :: axis
    character(len=*), parameter :: rname='HEADER>GET>SIZE>X'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (head%set%il.ne.code_unk) then
       axis => head%set%axis(head%set%il)
    else
       call cubetools_message(seve%e,rname,'Unknown X axis')
       error = .true.
       return
    endif
    size = abs(axis%n*axis%inc)
  end subroutine cubetools_header_get_size_x
  !
  subroutine cubetools_header_get_size_y(head,size,error)
    use cubetools_axis_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t), target, intent(in)    :: head
    real(kind=coor_k),           intent(out)   :: size
    logical,                     intent(inout) :: error
    !
    type(axis_t), pointer :: axis
    character(len=*), parameter :: rname='HEADER>GET>SIZE>Y'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (head%set%im.ne.code_unk) then
       axis => head%set%axis(head%set%im)
    else
       call cubetools_message(seve%e,rname,'Unknown Y axis')
       error = .true.
       return
    endif
    size = abs(axis%n*axis%inc)
  end subroutine cubetools_header_get_size_y
  !
  !---------------------------------------------------------------------
  !
  subroutine cubetools_header_get_line(head,line,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t), intent(in)    :: head
    character(len=*),    intent(out)   :: line
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>GET>LINE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    line = head%spe%line
  end subroutine cubetools_header_get_line
  !
  subroutine cubetools_header_put_line(line,head,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    character(len=*),    intent(in)    :: line
    type(cube_header_t), intent(inout) :: head
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>PUT>LINE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    head%spe%line = line
  end subroutine cubetools_header_put_line
  !
  subroutine cubetools_header_get_nchan(head,nchan,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t),  intent(in)    :: head
    integer(kind=chan_k), intent(out)   :: nchan
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>PUT>NCHAN'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (head%set%ic.eq.code_unk) then
       call cubetools_message(seve%e,rname,'Spectral axis seems to be inexistent in axset array')
       error = .true.
       return
    else
       nchan = head%arr%n%axis(head%set%ic)
    endif
  end subroutine cubetools_header_get_nchan
  !
  subroutine cubetools_header_put_nchan(nchan,head,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    integer(kind=chan_k), intent(in)    :: nchan
    type(cube_header_t),  intent(inout) :: head
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>PUT>NCHAN'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (head%set%ic.eq.code_unk) then
       call cubetools_message(seve%e,rname,'Spectral axis seems to be inexistent in axset array')
       error = .true.
       return
    else
       head%arr%n%axis(head%set%ic) = nchan
       head%arr%n%c = nchan
       head%set%axis(head%set%ic)%n = nchan
       head%spe%nc = nchan
    endif
  end subroutine cubetools_header_put_nchan
  !
  subroutine cubetools_header_put_nxny(nx,ny,head,error)
    use cubetools_axis_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    integer(kind=pixe_k), intent(in)    :: nx
    integer(kind=pixe_k), intent(in)    :: ny
    type(cube_header_t),  intent(inout) :: head
    logical,              intent(inout) :: error
    !
    type(axis_t) :: axis
    character(len=*), parameter :: rname='HEADER>PUT>NXNY'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if ((nx.le.0).or.(ny.le.0)) then
       call cubetools_message(seve%e,rname,'Negative or zero valued nx or ny')
       error = .true.
       return
    endif
    call cubetools_header_get_axis_head_l(head,axis,error)
    if (error) return
    axis%n = nx
    call cubetools_header_update_axset_l(axis,head,error)
    if (error) return
    !
    call cubetools_header_get_axis_head_m(head,axis,error)
    if (error) return
    axis%n = ny
    call cubetools_header_update_axset_m(axis,head,error)
    if (error) return
  end subroutine cubetools_header_put_nxny
  !
  !---------------------------------------------------------------------
  !
  subroutine cubetools_header_get_array_shape(head,n,error)
    use cubetools_shape_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t), intent(in)    :: head
    type(shape_t),       intent(inout) :: n
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>GET>ARRAY>SHAPE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    n = head%arr%n
  end subroutine cubetools_header_get_array_shape
  !
  subroutine cubetools_header_get_array_unit(head,unit,error)
    use cubetools_noise_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t), intent(in)    :: head
    character(len=*),    intent(out)   :: unit
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>GET>ARRAY>UNIT'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    unit = head%arr%unit
  end subroutine cubetools_header_get_array_unit
  !
  subroutine cubetools_header_put_array_unit(unit,head,error)
    use cubetools_noise_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    character(len=*),    intent(in)    :: unit
    type(cube_header_t), intent(inout) :: head
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>PUT>ARRAY>UNIT'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    head%arr%unit = unit
    ! *** JP Unclear to me whether the noise section should automatically
    ! *** JP be reinitialized as the minimum and maximum?
    call cubetools_noise_init(head%arr%noi,error)
    if (error) return
  end subroutine cubetools_header_put_array_unit
  !
  subroutine cubetools_header_get_array_ngood(head,ngood,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t),  intent(in)    :: head
    integer(kind=data_k), intent(out)   :: ngood
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>GET>ARRAY>NGOOD'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    ! *** JP: This should be the following
    ! ngood = head%arr%n%good
    ! *** JP: or the following
    ! ngood = head%arr%n%l*head%arr%n%m*head%arr%n%c-head%arr%n%nan
    ! *** JP: but for the moment it is the following
    ngood = head%arr%n%l*head%arr%n%m*head%arr%n%c
    ! *** JP: because of a bug in the computation of head%arr%n%nan
  end subroutine cubetools_header_get_array_ngood
  !
  subroutine cubetools_header_put_array_ngood(ngood,head,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    integer(kind=data_k), intent(in)    :: ngood
    type(cube_header_t),  intent(inout) :: head
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>PUT>ARRAY>NGOOD'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    head%arr%n%good = ngood
  end subroutine cubetools_header_put_array_ngood
  !
  subroutine cubetools_header_get_array_minmax(head,min,max,error)
    use cubetools_arrelt_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t),  intent(in)    :: head
    type(arrelt_t),       intent(out)   :: min,max
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>GET>ARRAY>MINMAX'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    min = head%arr%min
    max = head%arr%max
    !
    ! Ensure min%ix min%iy min%ic pointers are properly set
    call min%associate(head%set%il,head%set%im,head%set%ic,error)
    if (error)  return
    call max%associate(head%set%il,head%set%im,head%set%ic,error)
    if (error)  return
  end subroutine cubetools_header_get_array_minmax
  !
  subroutine cubetools_header_put_array_minmax(min,max,head,error)
    use cubetools_arrelt_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(arrelt_t),      intent(in)    :: min,max
    type(cube_header_t), intent(inout) :: head
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>PUT>ARRAY>MINMAX'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    head%arr%min = min
    head%arr%max = max
    !
    ! Ensure min%ix min%iy min%ic pointers are properly set
    call head%arr%min%associate(head%set%il,head%set%im,head%set%ic,error)
    if (error)  return
    call head%arr%max%associate(head%set%il,head%set%im,head%set%ic,error)
    if (error)  return
  end subroutine cubetools_header_put_array_minmax
  !
  subroutine cubetools_header_make_array_cplx(head,error)
    use gkernel_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t),  intent(inout) :: head
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>MAKE>ARRAY>CPLX'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    head%arr%type = fmt_c4
  end subroutine cubetools_header_make_array_cplx
  !
  subroutine cubetools_header_make_array_real(head,error)
    use gkernel_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t),  intent(inout) :: head
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>MAKE>ARRAY>REAL'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    head%arr%type = fmt_r4
  end subroutine cubetools_header_make_array_real
  !
  !---------------------------------------------------------------------
  !
  subroutine cubetools_header_point2axis(iaxis,head,axis,error)
    use cubetools_axis_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    integer(kind=ndim_k),          intent(in)    :: iaxis
    type(cube_header_t),  target,  intent(in)    :: head
    type(axis_t),         pointer, intent(inout) :: axis
    logical,                       intent(inout) :: error
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='HEADER>POINT2AXIS'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if ((iaxis.eq.code_unk).or.(iaxis.lt.1).or.(iaxis.gt.head%set%n)) then
       write(mess,'(a,i0)') 'Trying to point to an inexistent axis in axset: ',iaxis
       call cubetools_message(seve%e,rname,mess)
       error = .true.
       return
    else
       axis => head%set%axis(iaxis)
    endif
  end subroutine cubetools_header_point2axis
  !
  subroutine cubetools_header_get_axis_head_f(head,axis,error)
    use cubetools_axis_types
    !-------------------------------------------------------------------
    ! *** JP: This code is a duplication of cubetools_header_get_axis_head
    ! *** JP: At some point, it would be useful to merge. But it's not trivial.
    !-------------------------------------------------------------------
    type(cube_header_t), intent(in)    :: head
    type(axis_t),        intent(inout) :: axis
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>GET>AXIS>HEAD>F'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_axis_associate(axis,error)
    if (error) return
    call cubetools_axis_get(head%spe%f,&
         axis%genuine,axis%name,axis%unit,axis%kind,axis%n,axis%conv,error)
    if (error) return
    axis%coord => head%spe%f%coord
    axis%pointeris = code_pointer_associated
  end subroutine cubetools_header_get_axis_head_f
  !
  subroutine cubetools_header_get_axis_head_v(head,axis,error)
    use cubetools_axis_types
    !-------------------------------------------------------------------
    ! *** JP: This code is a duplication of cubetools_header_get_axis_head
    ! *** JP: At some point, it would be useful to merge. But it's not trivial.
    !-------------------------------------------------------------------
    type(cube_header_t), intent(in)    :: head
    type(axis_t),        intent(inout) :: axis
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>GET>AXIS>HEAD>V'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_axis_associate(axis,error)
    if (error) return
    call cubetools_axis_get(head%spe%v,&
         axis%genuine,axis%name,axis%unit,axis%kind,axis%n,axis%conv,error)
    if (error) return
    axis%coord => head%spe%v%coord
    axis%pointeris = code_pointer_associated
  end subroutine cubetools_header_get_axis_head_v
  !
  subroutine cubetools_header_get_axis_head(iaxis,head,axis,error)
    use cubetools_axis_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    integer(kind=ndim_k), intent(in)    :: iaxis
    type(cube_header_t),  intent(in)    :: head
    type(axis_t),         intent(inout) :: axis
    logical,              intent(inout) :: error
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='HEADER>GET>AXIS>HEAD'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if ((iaxis.eq.code_unk).or.(iaxis.lt.1).or.(iaxis.gt.head%set%n)) then
       write(mess,'(a,i0)') 'Trying to get an inexistent axis in axset: ',iaxis
       call cubetools_message(seve%e,rname,mess)
       error = .true.
       return
    else
       call cubetools_axis_associate(axis,error)
       if (error) return
       call cubetools_axis_get(head%set%axis(iaxis),&
            axis%genuine,axis%name,axis%unit,axis%kind,axis%n,axis%conv,error)
       if (error) return
       axis%coord => head%set%axis(iaxis)%coord
       axis%pointeris = code_pointer_associated
    endif
  end subroutine cubetools_header_get_axis_head
  !
  subroutine cubetools_header_get_axis_head_c(head,axis,error)
    use cubetools_axis_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t), intent(in)    :: head
    type(axis_t),        intent(inout) :: axis
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>GET>AXIS>HEAD>C'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_header_get_axis_head(head%set%ic,head,axis,error)
    if (error) return
  end subroutine cubetools_header_get_axis_head_c
  !
  subroutine cubetools_header_get_axis_head_l(head,axis,error)
    use cubetools_axis_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t), intent(in)    :: head
    type(axis_t),        intent(inout) :: axis
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>GET>AXIS>HEAD>L'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_header_get_axis_head(head%set%il,head,axis,error)
    if (error) return
  end subroutine cubetools_header_get_axis_head_l
  !
  subroutine cubetools_header_get_axis_head_m(head,axis,error)
    use cubetools_axis_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t), intent(in)    :: head
    type(axis_t),        intent(inout) :: axis
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>GET>AXIS>HEAD>M'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_header_get_axis_head(head%set%im,head,axis,error)
    if (error) return
  end subroutine cubetools_header_get_axis_head_m
  !
  !---------------------------------------------------------------------
  !
  subroutine cubetools_header_update_axset_axis(axis,iaxis,head,error)
    use cubetools_axis_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(axis_t),         intent(in)    :: axis
    integer(kind=ndim_k), intent(in)    :: iaxis
    type(cube_header_t),  intent(inout) :: head
    logical,              intent(inout) :: error
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='HEADER>UPDATE>AXSET>AXIS'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if ((iaxis.eq.code_unk).or.(iaxis.lt.1).or.(iaxis.gt.head%set%n)) then
       write(mess,'(a,i0)') 'Trying to update an inexistent axis in axset: ',iaxis
       call cubetools_message(seve%e,rname,mess)
       error = .true.
       return
    else
       head%arr%n%axis(iaxis) = axis%n
       call cubetools_axis_copy(axis,head%set%axis(iaxis),error)
       if (error) return
    endif    
  end subroutine cubetools_header_update_axset_axis
  !
  subroutine cubetools_header_update_axset_c(axis,head,error)
    use cubetools_axis_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(axis_t),        intent(in)    :: axis
    type(cube_header_t), intent(inout) :: head
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>UPDATE>AXSET>C'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    head%arr%n%c = axis%n
    call cubetools_header_update_axset_axis(axis,head%set%ic,head,error)
    if (error) return
  end subroutine cubetools_header_update_axset_c
  !
  subroutine cubetools_header_update_axset_l(axis,head,error)
    use cubetools_axis_types
    use cubetools_axset_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(axis_t),        intent(in)    :: axis
    type(cube_header_t), intent(inout) :: head
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>UPDATE>AXSET>L'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    head%arr%n%l = axis%n
    call cubetools_header_update_axset_axis(axis,head%set%il,head,error)
    if (error) return
    call cubetools_axset_val2zero(head%set,error)
    if (error) return
  end subroutine cubetools_header_update_axset_l
  !
  subroutine cubetools_header_update_axset_m(axis,head,error)
    use cubetools_axis_types
    use cubetools_axset_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(axis_t),        intent(in)    :: axis
    type(cube_header_t), intent(inout) :: head
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>UPDATE>AXSET>M'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    head%arr%n%m = axis%n
    call cubetools_header_update_axset_axis(axis,head%set%im,head,error)
    if (error) return
    call cubetools_axset_val2zero(head%set,error)
    if (error) return
  end subroutine cubetools_header_update_axset_m
  !
  !---------------------------------------------------------------------
  !
  subroutine cubetools_header_nullify_axset_axis(iaxis,head,error)
    use cubetools_axis_types
    use cubetools_unit
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    integer(kind=ndim_k), intent(in)    :: iaxis
    type(cube_header_t),  intent(inout) :: head
    logical,              intent(inout) :: error
    !
    character(len=mess_l) :: mess
    integer(kind=data_k), parameter :: one = 1
    logical, parameter :: genuine = .true.
    character(len=*), parameter :: rname='HEADER>NULLIFY>AXSET>AXIS'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if ((iaxis.eq.code_unk).or.(iaxis.lt.1).or.(iaxis.gt.head%set%n)) then
       write(mess,'(a,i0)') 'Trying to nullify an inexistent axis in axset: ',iaxis
       call cubetools_message(seve%e,rname,mess)
       error = .true.
       return
    else
       head%set%axis(iaxis)%genuine = .not.genuine
       head%set%axis(iaxis)%name = strg_unk
       head%set%axis(iaxis)%unit = strg_unk
       head%set%axis(iaxis)%kind = code_unit_unk
       head%set%axis(iaxis)%conv = [0d0,0d0,1d0]
       call cubetools_axis_derive(one,head%set%axis(iaxis),error)
       if (error) return
       head%arr%n%axis(iaxis) = one
    endif    
  end subroutine cubetools_header_nullify_axset_axis
  !
  subroutine cubetools_header_nullify_axset_c(head,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t), intent(inout) :: head
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>NULLIFY>AXSET>C'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    head%arr%n%c = 1
    call cubetools_header_nullify_axset_axis(head%set%ic,head,error)
    if (error) return
  end subroutine cubetools_header_nullify_axset_c
  !
  subroutine cubetools_header_nullify_axset_l(head,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t), intent(inout) :: head
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>NULLIFY>AXSET>L'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    head%arr%n%l = 1
    call cubetools_header_nullify_axset_axis(head%set%il,head,error)
    if (error) return
  end subroutine cubetools_header_nullify_axset_l
  !
  subroutine cubetools_header_nullify_axset_m(head,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t), intent(inout) :: head
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>NULLIFY>AXSET>M'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    head%arr%n%m = 1
    call cubetools_header_nullify_axset_axis(head%set%im,head,error)
    if (error) return
  end subroutine cubetools_header_nullify_axset_m
  !
  !---------------------------------------------------------------------
  !
  subroutine cubetools_header_get_spapro(head,spapro,error)
    use gkernel_types
    use gkernel_interfaces
    use cubetools_spapro_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t), intent(in)    :: head
    type(spapro_t),      intent(inout) :: spapro
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>GET>SPAPRO'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_spapro_copy(head%spa%pro,spapro,error)
    if (error) return
  end subroutine cubetools_header_get_spapro
  !
  subroutine cubetools_header_put_spapro(spapro,head,error)
    use cubetools_spapro_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(spapro_t),      intent(in)    :: spapro
    type(cube_header_t), intent(inout) :: head
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>GET>SPAPRO'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_spapro_copy(spapro,head%spa%pro,error)
    if (error) return
  end subroutine cubetools_header_put_spapro
  !
  subroutine cubetools_header_nullify_spapro(head,error)
    use cubetools_spapro_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t), intent(inout) :: head
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>NULLIFY>SPAPRO'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_spapro_init(head%spa%pro,error)
    if (error) return
  end subroutine cubetools_header_nullify_spapro
  !
  !---------------------------------------------------------------------
  !
  subroutine cubetools_header_get_spabeam(head,beam,error)
    use cubetools_beam_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t), intent(in)    :: head
    type(beam_t),        intent(out)   :: beam
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>GET>SPABEAM'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_beam_copy(head%spa%bea,beam,error)
    if (error) return
  end subroutine cubetools_header_get_spabeam
  !
  subroutine cubetools_header_update_spabeam(beam,head,error)
    use cubetools_beam_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(beam_t),        intent(in)    :: beam
    type(cube_header_t), intent(inout) :: head
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>UPDATE>SPABEAM'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_beam_copy(beam,head%spa%bea,error)
    if (error) return
  end subroutine cubetools_header_update_spabeam
  !
  !---------------------------------------------------------------------
  !
  subroutine cubetools_header_offset2pixel(iaxis,head,offset,pixel,error)
    use cubetools_axis_types
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    integer(kind=ndim_k), intent(in)    :: iaxis
    type(cube_header_t),  intent(in)    :: head
    real(kind=coor_k),    intent(in)    :: offset
    real(kind=coor_k),    intent(out)   :: pixel
    logical,              intent(inout) :: error
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='HEADER>OFFSET2PIXEL'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if ((iaxis.eq.code_unk).or.(iaxis.lt.1).or.(iaxis.gt.head%set%n)) then
       write(mess,'(a,i0)') 'Trying to access an inexistent axis in axset: ',iaxis
       call cubetools_message(seve%e,rname,mess)
       error = .true.
       return
    else    
       call cubetools_axis_offset2pixel(head%set%axis(iaxis),offset,pixel,error)
       if (error) return
    endif
  end subroutine cubetools_header_offset2pixel
  !
  subroutine cubetools_header_pixel2offset(iaxis,head,pixel,offset,error)
    use cubetools_axis_types
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    integer(kind=ndim_k), intent(in)    :: iaxis
    type(cube_header_t),  intent(in)    :: head
    real(kind=coor_k),    intent(in)    :: pixel
    real(kind=coor_k),    intent(out)   :: offset
    logical,              intent(inout) :: error
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='HEADER>PIXEL2OFFSET'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if ((iaxis.eq.code_unk).or.(iaxis.lt.1).or.(iaxis.gt.head%set%n)) then
       write(mess,'(a,i0)') 'Trying to access an inexistent axis in axset: ',iaxis
       call cubetools_message(seve%e,rname,mess)
       error = .true.
       return
    else    
       call cubetools_axis_pixel2offset(head%set%axis(iaxis),pixel,offset,error)
       if (error) return
    endif
  end subroutine cubetools_header_pixel2offset
  !
  subroutine cubetools_header_spatial_offset2pixel(head,offset,pixel,error)
    use cubetools_axis_types
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    type(cube_header_t), intent(in)    :: head
    real(kind=coor_k),   intent(in)    :: offset(2)
    real(kind=coor_k),   intent(out)   :: pixel(2)
    logical,             intent(inout) :: error
    !
    integer(kind=ndim_k), parameter :: il = 1
    integer(kind=ndim_k), parameter :: im = 2
    character(len=*), parameter :: rname='HEADER>SPATIAL>OFFSET2PIXEL'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_header_offset2pixel(head%set%il,head,offset(il),pixel(il),error)
    if (error) return
    call cubetools_header_offset2pixel(head%set%im,head,offset(im),pixel(im),error)
    if (error) return
  end subroutine cubetools_header_spatial_offset2pixel
  !
  subroutine cubetools_header_spatial_pixel2offset(head,pixel,offset,error)
    use cubetools_axis_types
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    type(cube_header_t), intent(in)    :: head
    real(kind=coor_k),   intent(in)    :: pixel(2)
    real(kind=coor_k),   intent(out)   :: offset(2)
    logical,             intent(inout) :: error
    !
    integer(kind=ndim_k), parameter :: il = 1
    integer(kind=ndim_k), parameter :: im = 2
    character(len=*), parameter :: rname='HEADER>SPATIAL>PIXEL2OFFSET'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_header_pixel2offset(head%set%il,head,pixel(il),offset(il),error)
    if (error) return
    call cubetools_header_pixel2offset(head%set%im,head,pixel(im),offset(im),error)
    if (error) return
  end subroutine cubetools_header_spatial_pixel2offset
  !
  !---------------------------------------------------------------------
  !
  subroutine cubetools_header_spectral_freq2chan(head,freq,chan,error)
    use cubetools_axis_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(cube_header_t), intent(in)    :: head
    real(kind=coor_k),   intent(in)    :: freq
    real(kind=coor_k),   intent(out)   :: chan
    logical,             intent(inout) :: error
    !
    type(axis_t) :: faxis
    character(len=*), parameter :: rname='HEADER>SPECTRAL>FREQ2CHAN'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_header_get_axis_head_f(head,faxis,error)
    if (error) return
    call cubetools_axis_offset2pixel(faxis,freq,chan,error)
    if (error) return
  end subroutine cubetools_header_spectral_freq2chan
  !
  subroutine cubetools_header_spectral_chan2freq(head,chan,freq,error)
    use cubetools_axis_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(cube_header_t), intent(in)    :: head
    real(kind=coor_k),   intent(in)    :: chan
    real(kind=coor_k),   intent(out)   :: freq
    logical,             intent(inout) :: error
    !
    type(axis_t) :: faxis
    character(len=*), parameter :: rname='HEADER>SPECTRAL>CHAN2FREQ'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_header_get_axis_head_f(head,faxis,error)
    if (error) return
    call cubetools_axis_pixel2offset(faxis,chan,freq,error)
    if (error) return
  end subroutine cubetools_header_spectral_chan2freq
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_header_multiply_spectral_spacing(nfactor,head,error)
    use cubetools_axis_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    integer(kind=chan_k), intent(in)    :: nfactor
    type(cube_header_t),  intent(inout) :: head
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>MULTIPLY>SPECTRAL>SPACING'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (head%set%ic.eq.code_unk) then
       call cubetools_message(seve%e,rname,'Spectral axis seems to be inexistent in axset array')
       error = .true.
       return
    else
       ! Increment
       ! head%spe%inc%c => Unchanged on purpose
       head%spe%inc%f = head%spe%inc%f*nfactor
       head%spe%inc%i = head%spe%inc%i*nfactor
       head%spe%inc%l = head%spe%inc%l/nfactor
       head%spe%inc%v = head%spe%inc%v*nfactor
       head%spe%inc%z = head%spe%inc%z*nfactor
       head%set%axis(head%set%ic)%inc = head%set%axis(head%set%ic)%inc*nfactor
       !
       ! Use left edge of first channel to compute new ref channel
       head%spe%ref%c = 0.5d0-(0.5d0-head%spe%ref%c)/nfactor
       ! 
       head%set%axis(head%set%ic)%ref = head%spe%ref%c
    endif
  end subroutine cubetools_header_multiply_spectral_spacing
  !
  subroutine cubetools_header_rederive_spectral_axes(head,error)
    use cubetools_axis_types
    use cubetools_spectral_prog_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t), intent(inout) :: head
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>REDERIVE>SPECTRAL>AXES'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call head%spe%rederive(error)
    if (error) return
    if (head%set%ic.eq.code_unk) then
       call cubetools_message(seve%e,rname,'Spectral axis seems to be inexistent in axset array')
       error = .true.
       return
    else
       call cubetools_axis_rederive(head%set%axis(head%set%ic),error)
       if (error) return       
    endif
  end subroutine cubetools_header_rederive_spectral_axes
  !
  subroutine cubetools_header_update_frequency_from_user(user,head,error)
    use cubetools_axis_types
    !----------------------------------------------------------------------
    ! Consistently rederive spectroscopy related quantities in the header
    ! when the user request any change of the spectral frequency axis.
    ! ----------------------------------------------------------------------
    type(axis_user_t),   intent(in)    :: user
    type(cube_header_t), intent(inout) :: head
    logical,             intent(inout) :: error
    !
    type(axis_t) :: axis
    character(len=*), parameter :: rname='HEADER>UPDATE>FREQUENCY>FROM>USER'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_axis_init(axis,error)
    if (error) return
    call cubetools_axis_user2prog_sub(user,head%spe%f,axis,error)
    if (error) return
    call cubetools_header_update_frequency_from_axis(axis,head,error)
    if (error) return
  end subroutine cubetools_header_update_frequency_from_user
  !
  subroutine cubetools_header_update_frequency_from_axis(axis,head,error)
    use cubetools_axis_types
    use cubetools_spectral_prog_types
    !----------------------------------------------------------------------
    ! Consistently rederive spectroscopy related quantities in the header
    ! based on a given frequency axis
    ! ----------------------------------------------------------------------    
    type(axis_t),        intent(in)    :: axis
    type(cube_header_t), intent(inout) :: head
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>UPDATE>FREQUENCY>FROM>AXIS'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call head%spe%update_from_freqaxis(axis,error)
    if (error) return
    call head%spe%rederive(error)
    if (error) return
    ! It's much easier for the visualization and export to ensure that the
    ! spectral axis is always the velocity axis
    call cubetools_header_update_axset_c(head%spe%v,head,error)
    if (error) return
    call cubetools_header_update_array_spectral_dim(head%set,head%spe%nc,head%arr%n,error)
    if (error) return
  end subroutine cubetools_header_update_frequency_from_axis
  !
  subroutine cubetools_header_update_velocity_from_user(user,head,error)
    use cubetools_axis_types
    !----------------------------------------------------------------------
    ! Consistently rederive spectroscopy related quantities in header when
    ! the user request any change of the spectral velocity axis
    !----------------------------------------------------------------------
    type(axis_user_t),   intent(in)    :: user
    type(cube_header_t), intent(inout) :: head
    logical,             intent(inout) :: error
    !
    type(axis_t) :: axis
    character(len=*), parameter :: rname='HEADER>UPDATE>VELOCITY>FROM>USER'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_axis_init(axis,error)
    if (error) return
    call cubetools_axis_user2prog_sub(user,head%spe%v,axis,error)
    if (error) return
    call cubetools_header_update_velocity_from_axis(axis,head,error)
    if (error) return
  end subroutine cubetools_header_update_velocity_from_user
  !
  subroutine cubetools_header_update_velocity_from_axis(axis,head,error)
    use cubetools_axis_types
    use cubetools_spectral_prog_types
    !----------------------------------------------------------------------
    ! Consistently rederive spectroscopy related quantities in the header
    ! based on a given velocity axis
    ! ----------------------------------------------------------------------    
    type(axis_t),        intent(in)    :: axis
    type(cube_header_t), intent(inout) :: head
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>UPDATE>FREQUENCY>FROM>AXIS'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call head%spe%update_from_veloaxis(axis,error)
    if (error) return
    call head%spe%rederive(error)
    if (error) return
    call cubetools_header_update_axset_c(head%spe%v,head,error)
    if (error) return
    call cubetools_header_update_array_spectral_dim(head%set,head%spe%nc,head%arr%n,error)
    if (error) return
  end subroutine cubetools_header_update_velocity_from_axis
  !
  subroutine cubetools_header_update_array_spectral_dim(set,nc,n,error)
    use cubetools_axis_types
    use cubetools_axset_types
    use cubetools_shape_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(axset_t),   intent(in)    :: set
    integer(chan_k), intent(in)    :: nc
    type(shape_t),   intent(inout) :: n
    logical,         intent(inout) :: error
    !
    integer(kind=ndim_k) :: ic 
    character(len=*), parameter :: rname='HEADER>UPDATE>ARRAY>SPECTRAL>DIM'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    ic = set%ic
    if (ic.eq.code_unk) then
       call cubetools_message(toolseve%others,rname,'Spectral axis seems to be inexistent in axset array')
    else if ((1.le.ic).and.(ic.le.set%n)) then
       n%axis(ic) = nc
       n%c = nc
    else
       call cubetools_message(seve%e,rname,'Trying to modify inexistent spectral dimension in data array')
       error = .true.
       return
    endif
  end subroutine cubetools_header_update_array_spectral_dim
  !
  subroutine cubetools_header_spatial_like(ref,out,error)
    use cubetools_axis_types
    use cubetools_spatial_types
    use cubetools_beam_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t), intent(in)    :: ref
    type(cube_header_t), intent(inout) :: out
    logical,             intent(inout) :: error
    !
    type(axis_t) :: axis
    type(beam_t) :: orig
    character(len=*), parameter :: rname='HEADER>SPATIAL>LIKE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_header_get_axis_head_l(ref,axis,error)
    if (error) return
    call cubetools_header_update_axset_l(axis,out,error)
    if (error) return
    !
    call cubetools_header_get_axis_head_m(ref,axis,error)
    if (error) return
    call cubetools_header_update_axset_m(axis,out,error)
    if (error) return
    !
    ! Beam is stored on a local variable to be restored to the output
    ! header after the copy of the rest of the spatial information
    call cubetools_beam_copy(out%spa%bea,orig,error)
    if (error) return
    call cubetools_spatial_copy(out%set%axis(out%set%il),out%set%axis(out%set%im),&
         ref%spa,out%spa,error)
    if (error) return
    call cubetools_beam_copy(orig,out%spa%bea,error)
    if (error) return
  end subroutine cubetools_header_spatial_like
  !
  subroutine cubetools_header_spectral_like(ref,out,error)
    use cubetools_axis_types
    use cubetools_spectral_prog_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t), intent(in)    :: ref
    type(cube_header_t), intent(inout) :: out
    logical,             intent(inout) :: error
    !
    type(axis_t) :: axis
    character(len=*), parameter :: rname='HEADER>SPECTRAL>LIKE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_header_get_axis_head_c(ref,axis,error)
    if (error) return
    call cubetools_header_update_axset_c(axis,out,error)
    if (error) return
    !
    call ref%spe%copyto(out%spe,error)
    if (error) return
  end subroutine cubetools_header_spectral_like
  !
  subroutine cubetools_header_add_observatories(source,target,error)
    use cubetools_observatory_types
    !-------------------------------------------------------------------
    ! Adds observatories from source to target
    !-------------------------------------------------------------------
    type(cube_header_t), intent(in)    :: source
    type(cube_header_t), intent(inout) :: target
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>ADD>OBSERVATORIES'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_observatory_add_telescopes(source%obs,target%obs,error)
    if (error) return
  end subroutine cubetools_header_add_observatories
  !
  subroutine cubetools_header_brightness2brightness(head,&
       doapplyeff,feff,beff,unitou,factor,error)
    use cubetools_brightness
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    type(cube_header_t),  intent(in)    :: head
    logical,              intent(in)    :: doapplyeff
    real(kind=sign_k),    intent(in)    :: feff
    real(kind=sign_k),    intent(in)    :: beff
    integer(kind=code_k), intent(in)    :: unitou
    real(kind=sign_k),    intent(out)   :: factor
    logical,              intent(inout) :: error
    !
    integer(kind=code_k) :: unitin
    logical :: valid
    character(len=*), parameter :: rname='HEADER>BRIGHTNESS2BRIGHTNESS'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_brightness_valid_brightness_unit(head%arr%unit,unitin,valid,error)
    if (error) return
    if (.not.valid) then
       call cubetools_message(seve%e,rname,'Cannot convert from '//head%arr%unit)
       error = .true.
       return
    endif
    call cubetools_brightness_brightness2brightness(&
         doapplyeff,feff,beff,&
         head%spe%ref%f,&
         head%spa%l%inc,head%spa%m%inc,&
         head%spa%bea%major,head%spa%bea%minor,&
         unitin,unitou,factor,error)
    if (error) return
  end subroutine cubetools_header_brightness2brightness
  !
  subroutine cubetools_header_brightness2flux(head,unitou,factor,error)
    use cubetools_brightness
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    type(cube_header_t),  intent(in)    :: head
    integer(kind=code_K), intent(in)    :: unitou
    real(kind=sign_k),    intent(out)   :: factor
    logical,              intent(inout) :: error
    !
    integer(kind=code_k) :: unitin
    logical :: valid
    character(len=*), parameter :: rname='HEADER>BRIGHTNESS2FLUX'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_brightness_valid_brightness_unit(head%arr%unit,unitin,valid,error)
    if (error) return
    if (.not.valid) then
       call cubetools_message(seve%e,rname,'Cannot convert from '//head%arr%unit)
       error = .true.
       return
    endif
    call cubetools_brightness_brightness2flux(&
         head%spe%ref%f,&
         head%spa%l%inc,head%spa%m%inc,&
         head%spa%bea%major,head%spa%bea%minor,&
         unitin,unitou,factor,error)
    if (error) return
  end subroutine cubetools_header_brightness2flux
  !
  subroutine cubetools_header_flux2flux(head,unitou,factor,error)
    use cubetools_brightness
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    type(cube_header_t),  intent(in)    :: head
    integer(kind=code_K), intent(in)    :: unitou
    real(kind=sign_k),    intent(out)   :: factor
    logical,              intent(inout) :: error
    !
    integer(kind=code_k) :: unitin
    logical :: valid
    character(len=*), parameter :: rname='HEADER>BRIGHTNESS2FLUX'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_brightness_valid_flux_unit(head%arr%unit,unitin,valid,error)
    if (error) return
    if (.not.valid) then
       call cubetools_message(seve%e,rname,'Cannot convert flux from '//head%arr%unit)
       error = .true.
       return
    endif
    call cubetools_brightness_flux2flux(unitin,unitou,factor,error)
    if (error) return
  end subroutine cubetools_header_flux2flux
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_header_get_rest_wavelength(head,wave,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t), intent(in)    :: head
    real(kind=coor_k),   intent(out)   :: wave
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>GET>REST>WAVELENGTH'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    wave = head%spe%ref%l
  end subroutine cubetools_header_get_rest_wavelength
  !
  subroutine cubetools_header_get_rest_frequency(head,freq,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t), intent(in)    :: head
    real(kind=coor_k),   intent(out)   :: freq
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>GET>REST>FREQUENCY'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    freq = head%spe%ref%f
  end subroutine cubetools_header_get_rest_frequency
  !
  subroutine cubetools_header_modify_rest_frequency(newfreq,head,error)
    use cubetools_spectral_prog_types
    use cubetools_beam_types
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    real(kind=coor_k),   intent(in)    :: newfreq
    type(cube_header_t), intent(inout) :: head
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>MODIFY>REST>FREQUENCY'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_beam_scale_with_frequency(head%spe%ref%f,newfreq,head%spa%bea,error)
    if (error) return
    call head%spe%modify_rest_frequency(newfreq,error)
    if (error) return
  end subroutine cubetools_header_modify_rest_frequency
  !
  subroutine cubetools_header_get_frame_velocity(head,velo,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(cube_header_t), intent(in)    :: head
    real(kind=coor_k),   intent(out)   :: velo
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>GET>FRAME>VELOCITY'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    velo = head%spe%ref%v
  end subroutine cubetools_header_get_frame_velocity
  !
  subroutine cubetools_header_modify_frame_velocity(newvelo,head,error)
    use cubetools_spectral_prog_types
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    real(kind=coor_k),   intent(in)    :: newvelo
    type(cube_header_t), intent(inout) :: head
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>MODIFY>FRAME>VELOCITY'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call head%spe%modify_frame_velocity(newvelo,error)
    if (error) return
  end subroutine cubetools_header_modify_frame_velocity
  !
  subroutine cubetools_header_beam_to_table(head,table,error)
    use cubetools_beam_types
    !------------------------------------------------------------------------
    ! Produce a beam table from the frequency axis
    !------------------------------------------------------------------------
    type(cube_header_t), intent(in)    :: head
    type(beam_table_t),  intent(out)   :: table
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>BEAM>TO>TABLE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_beam_to_table(head%spa%bea,head%spe%ref%f,head%spe%f%coord,table,error)
    if (error) return
  end subroutine cubetools_header_beam_to_table
  !
  !---------------------------------------------------------------------
  !
  subroutine cubetools_header_extrema_init(head,error)
    use cubetools_arrelt_types
    !-------------------------------------------------------------------
    ! Also update the number of array elements and initialize the number
    ! of blanked (NaN) values.
    !-------------------------------------------------------------------
    type(cube_header_t), intent(inout) :: head
    logical,             intent(inout) :: error
    !
    head%arr%n%dat = product(head%arr%n%axis(1:head%arr%n%dim))
    head%arr%n%nan = 0
    call cubetools_arrelt_init(head%arr%min%name,head%arr%min,error)
    if (error) return
    call cubetools_arrelt_init(head%arr%max%name,head%arr%max,error)
    if (error) return
  end subroutine cubetools_header_extrema_init
  !
  subroutine cubetools_header_extrema_update_image(data,nx,ny,iimage,ext,error)
    use cubetools_nan
    use cubetools_extrema_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    integer(kind=pixe_k),      intent(in)    :: nx,ny
    real(kind=sign_k),         intent(in)    :: data(nx,ny)
    integer(kind=imag_k),      intent(in)    :: iimage
    type(cubetools_extrema_t), intent(inout) :: ext
    logical,                   intent(inout) :: error
    !
    integer(kind=size_length) :: nmin,nmax,nnan
    real(kind=sign_k) :: zmin,zmax
    integer(kind=pixe_k) :: xpixmin,ypixmin,xpixmax,ypixmax
    character(len=*), parameter :: rname='HEADER>EXTREMA>UPDATE>IMAGE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_array_extrema(nx*ny,data,zmin,zmax,nmin,nmax,nnan)
    !
    if (nmin.gt.0) then  ! One or more valid values found
      if (ieee_is_nan(ext%min%val) .or. zmin.lt.ext%min%val) then
        ext%min%val = zmin
        ypixmin = (nmin-1)/nx+1
        xpixmin = nmin-nx*(ypixmin-1)
        ext%min%ix = xpixmin
        ext%min%iy = ypixmin
        ext%min%ic = iimage
      endif
      if (ieee_is_nan(ext%max%val) .or. zmax.gt.ext%max%val) then
        ext%max%val = zmax
        ypixmax = (nmax-1)/nx+1
        xpixmax = nmax-nx*(ypixmax-1)
        ext%max%ix = xpixmax
        ext%max%iy = ypixmax
        ext%max%ic = iimage
      endif
    endif
    !
    if (nnan.gt.0) then
      ext%nnan = ext%nnan+nnan
    endif
  end subroutine cubetools_header_extrema_update_image
  !
  subroutine cubetools_header_extrema_update_spectrum(data,ni,ix,iy,ext,error)
    use cubetools_nan
    use cubetools_extrema_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    integer(kind=imag_k),      intent(in)    :: ni
    real(kind=sign_k),         intent(in)    :: data(ni)
    integer(kind=pixe_k),      intent(in)    :: ix,iy
    type(cubetools_extrema_t), intent(inout) :: ext
    logical,                   intent(inout) :: error
    !
    integer(kind=size_length) :: nmin,nmax,nnan
    real(kind=sign_k) :: zmin,zmax
    character(len=*), parameter :: rname='HEADER>EXTREMA>UPDATE>SPECTRUM'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_array_extrema(ni,data,zmin,zmax,nmin,nmax,nnan)
    !
    if (nmin.gt.0) then  ! One or more valid values found
      if (ieee_is_nan(ext%min%val) .or. zmin.lt.ext%min%val) then
        ext%min%val = zmin
        ext%min%ix = ix
        ext%min%iy = iy
        ext%min%ic = nmin
      endif
      if (ieee_is_nan(ext%max%val) .or. zmax.gt.ext%max%val) then
        ext%max%val = zmax
        ext%max%ix = ix
        ext%max%iy = iy
        ext%max%ic = nmax
      endif
    endif
    !
    if (nnan.gt.0) then
      ext%nnan = ext%nnan+nnan
    endif
  end subroutine cubetools_header_extrema_update_spectrum
  !
  subroutine cubetools_header_extrema_update_subcube(f3,data,n1,n2,n3,ext,error)
    use cubetools_nan
    use cubetools_extrema_types
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    integer(kind=data_k),      intent(in)    :: f3  ! Index of first plane in the full cube
    integer(kind=data_k),      intent(in)    :: n1,n2,n3
    real(kind=sign_k),         intent(in)    :: data(n1,n2,n3)
    type(cubetools_extrema_t), intent(inout) :: ext
    logical,                   intent(inout) :: error
    !
    integer(kind=size_length) :: nmin,nmax,nnan,remainder
    real(kind=sign_k) :: zmin,zmax
    character(len=*), parameter :: rname='HEADER>EXTREMA>UPDATE>SUBCUBE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_array_extrema(n1*n2*n3,data,zmin,zmax,nmin,nmax,nnan)
    !
    if (nmin.gt.0) then  ! One or more valid values found
      if (ieee_is_nan(ext%min%val) .or. zmin.lt.ext%min%val) then
        ext%min%val = zmin
        remainder = nmin
        ext%min%loc(3) = (remainder-1)/(n1*n2)+1
        remainder = remainder - (n1*n2)*(ext%min%loc(3)-1)
        ext%min%loc(2) = (remainder-1)/n1+1
        remainder = remainder - n1*(ext%min%loc(2)-1)
        ext%min%loc(1) = remainder
        ! Apply offset from full cube
        ext%min%loc(3) = ext%min%loc(3) + f3-1
      endif
      if (ieee_is_nan(ext%max%val) .or. zmax.gt.ext%max%val) then
        ext%max%val = zmax
        remainder = nmax
        ext%max%loc(3) = (remainder-1)/(n1*n2)+1
        remainder = remainder - (n1*n2)*(ext%max%loc(3)-1)
        ext%max%loc(2) = (remainder-1)/n1+1
        remainder = remainder - n1*(ext%max%loc(2)-1)
        ext%max%loc(1) = remainder
        ! Apply offset from full cube
        ext%max%loc(3) = ext%max%loc(3) + f3-1
      endif
    endif
    !
    if (nnan.gt.0) then
      ext%nnan = ext%nnan+nnan
    endif
  end subroutine cubetools_header_extrema_update_subcube
  !
  subroutine cubetools_array_extrema(nxy,z,zmin,zmax,nmin,nmax,nnan)
    use cubetools_nan
    !---------------------------------------------------------------------
    ! Find extrema of a (sign_k) array.
    ! +/- Infinity are considered as valid extrema in this routine.
    !---------------------------------------------------------------------
    integer(kind=size_length), intent(in)    :: nxy    ! Number of values
    real(kind=sign_k),         intent(in)    :: z(nxy) ! Values
    real(kind=sign_k),         intent(inout) :: zmin   ! Unchanged if no valid value
    real(kind=sign_k),         intent(inout) :: zmax   ! Unchanged if no valid value
    integer(kind=size_length), intent(out)   :: nmin   ! Set to 0 if no valid value
    integer(kind=size_length), intent(out)   :: nmax   ! Set to 0 if no valid value
    integer(kind=size_length), intent(out)   :: nnan   ! Number of NaN
    !
    integer(kind=size_length) :: i,j
    character(len=*), parameter :: rname='ARRAY>EXTREMA'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    j = nxy+1
    nmin = 0
    nmax = 0
    nnan = 0
    !
    do i=1,nxy
      if (ieee_is_nan(z(i))) then
        nnan = nnan+1
        cycle
      endif
      zmin = z(i)
      zmax = z(i)
      nmin = i
      nmax = i
      j = i
      exit
    enddo
    !
    do i=j,nxy
      if (ieee_is_nan(z(i))) then
        nnan = nnan+1
        cycle
      endif
      if (z(i).lt.zmin) then
        zmin = z(i)
        nmin = i
      elseif (z(i).gt.zmax) then
        zmax = z(i)
        nmax = i
      endif
    enddo
  end subroutine cubetools_array_extrema
end module cubetools_header_methods
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
