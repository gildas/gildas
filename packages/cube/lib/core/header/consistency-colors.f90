!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_consistency_colors
  use gbl_ansicodes
  use cubetools_parameters
  use cubetools_messaging
  use cubetools_structure
  use cubetools_keywordlist_types
  !
  public  :: colors,color_opt_t
  private
  !
  integer(kind=4),parameter :: colo_l = 12
  integer(kind=4),parameter :: ncolors = 9
  !
  character(len=*), parameter :: cnames(ncolors) = &
       ['ORANGE ','RED    ','GREEN  ','YELLOW ','BLUE   ','MAGENTA','CYAN   ','WHITE  ','BLACK  ']
  character(len=colo_l), parameter :: cvalues(ncolors) = &
       [c_orange,c_red,c_green,c_yellow,c_blue,c_magenta,c_cyan,c_white,c_black]
  !
  type cons_color_t
     character(len=colo_l)  :: cons   =  c_green
     character(len=colo_l)  :: incons =  c_orange
     character(len=colo_l)  :: notche =  c_white
     character(len=colo_l)  :: cname  =  'GREEN'
     character(len=colo_l)  :: iname  =  'ORANGE'
     character(len=colo_l)  :: nname  =  'WHITE'
   contains
     procedure, public :: set_inconsistent => cubetools_colors_set_inconsistent
     procedure, public :: set_consistent   => cubetools_colors_set_consistent
     procedure, public :: set_notchecked   => cubetools_colors_set_notchecked
     procedure, public :: reset_defaults   => cubetools_colors_reset_defaults
     procedure, public :: list             => cubetools_colors_list
  end type cons_color_t
  !
   type color_opt_t
     type(option_t),      pointer :: opt
     type(keywordlist_comm_t), pointer :: color
     integer(kind=4)              :: icolor
     logical                      :: do
   contains
     procedure, public :: register => cubetools_color_topic_register
     procedure, public :: parse    => cubetools_color_topic_parse   
  end type color_opt_t
  !
  type(cons_color_t) :: colors
  !
contains
  subroutine cubetools_colors_set_inconsistent(colors,icolor,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(cons_color_t), intent(inout) :: colors
    integer(kind=4),     intent(in)    :: icolor
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='COLORS>SET>INCONSISTENT'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    colors%incons = cvalues(icolor)
    colors%iname  = cnames(icolor)
  end subroutine cubetools_colors_set_inconsistent
  !
  subroutine cubetools_colors_set_consistent(colors,icolor,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(cons_color_t), intent(inout) :: colors
    integer(kind=4),     intent(in)    :: icolor
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='COLORS>SET>CONSISTENT'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    colors%cons  = cvalues(icolor)
    colors%cname = cnames(icolor)
  end subroutine cubetools_colors_set_consistent
  !
  subroutine cubetools_colors_set_notchecked(colors,icolor,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(cons_color_t), intent(inout) :: colors
    integer(kind=4),     intent(in)    :: icolor
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='COLORS>SET>NOTCHECKED'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    colors%notche = cvalues(icolor)
    colors%nname  = cnames(icolor)
  end subroutine cubetools_colors_set_notchecked
  !
  subroutine cubetools_colors_reset_defaults(colors,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(cons_color_t), intent(inout) :: colors
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='COLORS>RESET>DEFAULTS'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    colors%cons   =  c_green   
    colors%incons =  c_orange  
    colors%notche =  c_white   
    colors%cname  =  'GREEN'   
    colors%iname  =  'ORANGE'  
    colors%nname  =  'WHITE'   
  end subroutine cubetools_colors_reset_defaults
  !
  subroutine cubetools_colors_list(colors,error)
    use cubetools_format
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(cons_color_t), intent(in)    :: colors
    logical,             intent(inout) :: error
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='COLORS>LIST'
    !
    call cubetools_message(seve%r,rname,'  Consistency Colors:')
    write(mess,'(a,a)') '    CONSISTENT   : ',colors%cname
    mess = cubetools_format_color(mess,colors%cons)
    call cubetools_message(seve%r,rname,mess)
    write(mess,'(a,a)') '    INCONSISTENT : ',colors%iname
    mess = cubetools_format_color(mess,colors%incons)
    call cubetools_message(seve%r,rname,mess)
    write(mess,'(a,a)') '    NOT CHECKED  : ',colors%nname
    mess = cubetools_format_color(mess,colors%notche)
    call cubetools_message(seve%r,rname,mess)
    call cubetools_message(seve%r,rname,blankstr)
  end subroutine cubetools_colors_list
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_color_topic_register(topic,name,abstract,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(color_opt_t), intent(inout) :: topic
    character(len=*),   intent(in)    :: name
    character(len=*),   intent(in)    :: abstract
    logical,            intent(inout) :: error
    !
    type(keywordlist_comm_t) :: keyarg
    character(len=*), parameter :: rname='COLORS>TOPIC>REGISTER'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         name,'color',&
         abstract,&
         strg_id,&
         topic%opt,error)
    if (error) return
    call keyarg%register( &
         'color',  &
         'Chosen color', &
         strg_id,&
         code_arg_mandatory, &
         cnames, &
         .not.flexible, &
         topic%color,&
         error)
    if (error) return
  end subroutine cubetools_color_topic_register
  !
  subroutine cubetools_color_topic_parse(topic,line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(color_opt_t), intent(inout) :: topic
    character(len=*),   intent(in)    :: line
    logical,            intent(inout) :: error
    !
    character(len=argu_l) :: argum,desambig
    character(len=*), parameter :: rname='COLORS>TOPIC>PARSE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call topic%opt%present(line,topic%do,error)
    if (error) return
    if (topic%do) then
       call cubetools_getarg(line,topic%opt,1,argum,mandatory,error)
       if (error) return
       call cubetools_keywordlist_user2prog(topic%color,argum,topic%icolor,desambig,error)
       if (error) return
    else
       ! Do nothing
    endif
  end subroutine cubetools_color_topic_parse
end module cubetools_consistency_colors
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
