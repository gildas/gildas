!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! In this file, the following modules are present in this order:
!    cubetools_spectral_prog_types
!    cubetools_spectral_consistency_types
!    cubetools_spectral_frame_types
!    cubetools_spectral_v_or_z_types
!    cubetools_spectral_velocity_types
!    cubetools_spectral_redshift_types
!    cubetools_spectral_line_types
!    cubetools_spectral_f_or_w_types
!    cubetools_spectral_freq_types
!    cubetools_spectral_wave_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_spectral_prog_types
  !***JP: The notion of beam chould be here generalized to the response of
  !***JP: the correlator
  use cubetools_parameters
  use cubetools_messaging
  use cubetools_axis_types
  use cubetools_speelt_types
  use cubetools_header_interface
  !
  public :: spectral_prog_t
  private
  !
  type spectral_prog_t
     logical               :: genuine = .false. !
     integer(kind=chan_k)  :: nc = 0            ! Number of channels 
     integer(kind=code_k)  :: frame = code_speframe_unknown  ! Frame of reference
     integer(kind=code_k)  :: conv  = code_systemic_unknown  ! Convention for velocity or redshift
     character(len=line_l) :: line  = strg_unk  ! Name of the line
     type(speelt_t)        :: ref               ! Value     at reference pixel
     type(speelt_t)        :: inc               ! Increment at reference pixel
     type(axis_t)          :: c                 ! [----] Channel
     type(axis_t)          :: f                 ! [ MHz] Signal frequency
     type(axis_t)          :: i                 ! [ MHz] Image  frequency
     type(axis_t)          :: l                 ! [ mum] Wavelength
     type(axis_t)          :: v                 ! [km/s] Velocity
     type(axis_t)          :: z                 ! [----] Redshift
   contains
     ! Administration
     procedure, public :: init           => cubetools_spectral_init
     procedure, public :: get            => cubetools_spectral_get
     procedure, public :: put_and_derive => cubetools_spectral_put_and_derive
     procedure, public :: rederive       => cubetools_spectral_rederive
     procedure, public :: copyto         => cubetools_spectral_copyto
     procedure, public :: list           => cubetools_spectral_list
     procedure, public :: final          => cubetools_spectral_final
     ! Computation
     procedure, public :: derive_axes           => cubetools_spectral_derive_axes
     procedure, public :: modify_rest_frequency => cubetools_spectral_modify_rest_frequency
     procedure, public :: modify_frame_velocity => cubetools_spectral_modify_frame_velocity
     procedure, public :: update_from_freqaxis  => cubetools_spectral_update_from_freqaxis
     procedure, public :: update_from_veloaxis  => cubetools_spectral_update_from_veloaxis
     ! Feedback to user
     procedure, public :: load_frame_into      => cubetools_spectral_load_frame_into
     procedure, public :: load_velocity_into   => cubetools_spectral_load_velocity_into
     procedure, public :: load_redshift_into   => cubetools_spectral_load_redshift_into
     procedure, public :: load_line_into       => cubetools_spectral_load_line_into
     procedure, public :: load_frequency_into  => cubetools_spectral_load_frequency_into
     procedure, public :: load_wavelength_into => cubetools_spectral_load_wavelength_into
     procedure, public :: obsolete_sicdef      => cubetools_spectral_sicdef
  end type spectral_prog_t
  !
contains
  !
  subroutine cubetools_spectral_init(spe,error)
    !-------------------------------------------------------------------
    ! Just initialize the type, ie, set it to intent(out)
    !-------------------------------------------------------------------
    class(spectral_prog_t), intent(out)   :: spe
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>INIT'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_speelt_init(spe%ref,error)
    if (error) return
    call cubetools_speelt_init(spe%inc,error)
    if (error) return
    call cubetools_axis_init(spe%c,error)
    if (error) return
    call cubetools_axis_init(spe%f,error)
    if (error) return
    call cubetools_axis_init(spe%i,error)
    if (error) return
    call cubetools_axis_init(spe%l,error)
    if (error) return
    call cubetools_axis_init(spe%v,error)
    if (error) return
    call cubetools_axis_init(spe%z,error)
    if (error) return
  end subroutine cubetools_spectral_init
  !
  subroutine cubetools_spectral_get(spe,&
       frame,convention,line,spectral_code,increment,signal,image,&
       systemic_code,systemic_value,&
       error)
    use cubetools_header_interface
    !-------------------------------------------------------------------
    ! Parameters that are redundant elsewhere in the header type are not
    ! accessible through this method
    !-------------------------------------------------------------------
    class(spectral_prog_t), intent(in)    :: spe
    integer(kind=code_k),   intent(out)   :: frame
    integer(kind=code_k),   intent(out)   :: convention
    character(len=*),       intent(out)   :: line
    integer(kind=code_k),   intent(out)   :: spectral_code
    real(kind=coor_k),      intent(out)   :: increment
    real(kind=coor_k),      intent(out)   :: signal
    real(kind=coor_k),      intent(out)   :: image
    integer(kind=code_k),   intent(out)   :: systemic_code
    real(kind=coor_k),      intent(out)   :: systemic_value
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>GET'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    frame = spe%frame
    convention = spe%conv
    line = spe%line
    spectral_code = code_spectral_frequency
    increment = spe%inc%f
    signal = spe%ref%f
    image = spe%ref%i
    systemic_code = code_systemic_velocity
    systemic_value = spe%ref%v
  end subroutine cubetools_spectral_get
  !
  subroutine cubetools_spectral_put_and_derive(spe,genuine,&
       frame_code,convention_code,line,nc,channel,&
       spectral_code,increment,signal,image,&
       systemic_code,systemic_value,&
       error)
    use phys_const
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(spectral_prog_t), intent(inout) :: spe
    logical,                intent(in)    :: genuine
    integer(kind=code_k),   intent(in)    :: frame_code
    integer(kind=code_k),   intent(in)    :: convention_code
    character(len=*),       intent(in)    :: line
    integer(kind=chan_k),   intent(in)    :: nc
    real(kind=coor_k),      intent(in)    :: channel
    integer(kind=code_k),   intent(in)    :: spectral_code
    real(kind=coor_k),      intent(in)    :: increment
    real(kind=coor_k),      intent(in)    :: signal
    real(kind=coor_k),      intent(in)    :: image
    integer(kind=code_k),   intent(in)    :: systemic_code
    real(kind=coor_k),      intent(in)    :: systemic_value
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>PUT>AND>DERIVE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    !
    spe%line = line
    spe%frame = frame_code
    !
    spe%ref%c = channel
    spe%inc%c = 1d0
    !
    if (spectral_code.eq.code_spectral_frequency) then
       spe%ref%f = signal
       spe%ref%i = image
       spe%ref%l = clight/spe%ref%f
       !
       spe%inc%f = increment
       spe%inc%i = -spe%inc%f
       spe%inc%l = -spe%ref%l*(spe%inc%f/spe%ref%f)
    else if (spectral_code.eq.code_spectral_wavelength) then
       call cubetools_message(seve%w,rname,'Experimental support wavelengths => Things might still be incorrect')
       spe%ref%l = signal
       spe%ref%i = image
       spe%ref%f = clight/spe%ref%l
       !
       spe%inc%l = increment
       spe%inc%i = -spe%inc%l
       spe%inc%f = -spe%ref%f*(spe%inc%l/spe%ref%l)
    else if (spectral_code.eq.code_spectral_unknown) then
       spe%ref%f = 0
       spe%ref%i = 0
       spe%ref%l = 0
       !
       spe%inc%f = 0
       spe%inc%i = 0
       spe%inc%l = 0
    else
       call cubetools_message(seve%e,rname,'CUBE only handles frequencies or wavelengths')
       error = .true.
       return
    endif
    !
    spe%conv = convention_code
    if (systemic_code.eq.code_systemic_velocity) then
       spe%ref%v = systemic_value
       if (convention_code.eq.code_speconv_radio) then
          spe%ref%z = spe%ref%v/clight_kms
          !
          spe%inc%v = -clight_kms*spe%inc%f/spe%ref%f
          spe%inc%z = spe%inc%v/clight_kms
       else
          call cubetools_message(seve%w,rname,'Using radio convention conversion formulas for non-radio spectral axis')
          ! *** JP+SB *** Radio approximation so that we can work!
          spe%ref%z = spe%ref%v/clight_kms
          spe%inc%v = -clight_kms*spe%inc%f/spe%ref%f
          spe%inc%z = spe%inc%v/clight_kms
       endif
    else if (systemic_code.eq.code_systemic_redshift) then
       call cubetools_message(seve%e,rname,'CUBE format can only handle the source frame velocity, not yet its redshift')
       error = .true.
       return
    else if (systemic_code.eq.code_systemic_unknown) then
       spe%conv = code_speconv_unknown
       spe%ref%z = 0
       !
       spe%inc%v = 0
       spe%inc%z = 0
    else
       call cubetools_message(seve%e,rname,'CUBE only handles velocity or redshift')
       error = .true.
       return
    endif
    !
    call spe%derive_axes(genuine,nc,error)
    if (error) return
  end subroutine cubetools_spectral_put_and_derive
  !
  subroutine cubetools_spectral_rederive(spe,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    class(spectral_prog_t), intent(inout) :: spe
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>REDERIVE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call spe%derive_axes(spe%genuine,spe%nc,error)
    if (error) return
  end subroutine cubetools_spectral_rederive
  !
  subroutine cubetools_spectral_copyto(in,ou,error)
    !-------------------------------------------------------------------
    ! Copy spectral section from one cube to another
    !-------------------------------------------------------------------
    class(spectral_prog_t), intent(in)    :: in
    type(spectral_prog_t),  intent(inout) :: ou
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>COPYTO'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    ou%genuine = in%genuine
    ou%nc = in%nc
    ou%frame = in%frame
    ou%conv = in%conv
    ou%line = in%line 
    call cubetools_speelt_copy(in%ref,ou%ref,error)
    if (error) return
    call cubetools_speelt_copy(in%inc,ou%inc,error)
    if (error) return
    call cubetools_axis_copy(in%c,ou%c,error)
    if (error) return
    call cubetools_axis_copy(in%f,ou%f,error)
    if (error) return
    call cubetools_axis_copy(in%i,ou%i,error)
    if (error) return
    call cubetools_axis_copy(in%l,ou%l,error)
    if (error) return
    call cubetools_axis_copy(in%v,ou%v,error)
    if (error) return
    call cubetools_axis_copy(in%z,ou%z,error)
    if (error) return
  end subroutine cubetools_spectral_copyto
  !
  subroutine cubetools_spectral_list(spe,error)
    use cubetools_format
    use cubetools_header_interface
    use cubetools_unit
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    class(spectral_prog_t), intent(in)    :: spe
    logical,                intent(inout) :: error
    !
    integer(kind=code_k) :: iframe,iconv
    type(unit_user_t) :: unitvelo, unitfreq
    character(len=mess_l) :: mess
    character(len=5) :: forma
    character(len=*), parameter :: rname='SPECTRAL>LIST'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    iframe = spe%frame
    if ((iframe.lt.1).or.(iframe.gt.nspeframes)) iframe = code_speframe_unknown
    iconv  = spe%conv
    if ((iconv.lt.1).or.(iconv.gt.nspeconv)) iframe = code_speconv_unknown
    mess = cubetools_format_stdkey_boldval('','',22)
    mess = trim(mess)//'  '//cubetools_format_stdkey_boldval('Frame',speframes(iframe),22)
    mess = trim(mess)//'  '//cubetools_format_stdkey_boldval('Convention',speconvnames(iconv),22)
    call cubetools_message(seve%r,rname,mess)
    !
    call unitfreq%get_from_code(code_unit_freq,error)
    if (error) return
    call unitvelo%get_from_code(code_unit_velo,error)
    if (error) return
    mess = cubetools_format_stdkey_boldval('Line',spe%line,22)
    if (spe%ref%f*unitfreq%user_per_prog.ge.1d8 .or. spe%ref%i*unitfreq%user_per_prog.ge.1d8) then
      forma = 'f15.3'  ! More leading digits and less precision, still 15 digits in total
    else
      forma = 'f15.6'  ! Default
    endif
    mess = trim(mess)//'  '//cubetools_format_stdkey_boldval('Rest',spe%ref%f*unitfreq%user_per_prog,forma,22)
    mess = trim(mess)//'  '//cubetools_format_stdkey_boldval('Vsys',spe%ref%v*unitvelo%user_per_prog,'f10.2',22)
    call cubetools_message(seve%r,rname,mess)
    !
    mess = cubetools_format_stdkey_boldval('','',22)
    mess = trim(mess)//'  '//cubetools_format_stdkey_boldval('Image',spe%ref%i*unitfreq%user_per_prog,forma,22)
    mess = trim(mess)//'  '//cubetools_format_stdkey_boldval('Redshift',spe%ref%z,'f10.7',22)
    call cubetools_message(seve%r,rname,mess)
    call cubetools_message(seve%r,rname,' ')
  end subroutine cubetools_spectral_list
  !
  subroutine cubetools_spectral_final(spe,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(spectral_prog_t), intent(inout) :: spe
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>FINAL'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    spe%frame = code_speframe_unknown
    spe%conv  = code_speconv_unknown
    spe%line  = strg_unk
    call cubetools_speelt_final(spe%ref,error)
    if (error) return
    call cubetools_speelt_final(spe%inc,error)
    if (error) return
    call cubetools_axis_reinitialize(spe%c,error)
    call cubetools_axis_reinitialize(spe%f,error)
    call cubetools_axis_reinitialize(spe%i,error)
    call cubetools_axis_reinitialize(spe%l,error)
    call cubetools_axis_reinitialize(spe%v,error)
    call cubetools_axis_reinitialize(spe%z,error)
  end subroutine cubetools_spectral_final
  !
  !---------------------------------------------------------------------
  !
  subroutine cubetools_spectral_derive_axes(spe,genuine,nc,error)
    use cubetools_unit
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    class(spectral_prog_t), intent(inout) :: spe
    logical,                intent(in)    :: genuine
    integer(kind=chan_k),   intent(in)    :: nc
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>DERIVE>AXES'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_axis_put_and_derive(&
         genuine,&
         'Channel','---',code_unit_unk,&
         nc,&
         spe%ref%c,&
         spe%ref%c,&
         spe%inc%c,&
         spe%c,error)
    call cubetools_axis_put_and_derive(&
         genuine,&
         'Frequency','MHz',code_unit_freq,&
         nc,&
         spe%ref%c,&
         spe%ref%f,&
         spe%inc%f,&
         spe%f,error)
    if (error) return
    call cubetools_axis_put_and_derive(&
         genuine,&
         'Image Freq.','MHz',code_unit_freq,&
         nc,&
         spe%ref%c,&
         spe%ref%i,&
         spe%inc%i,&
         spe%i,error)
    if (error) return
    call cubetools_axis_put_and_derive(&
         genuine,&
         'Wavelength','um',code_unit_wave,&
         nc,&
         spe%ref%c,&
         spe%ref%l,&
         spe%inc%l,&
         spe%l,error)
    if (error) return
    call cubetools_axis_put_and_derive(&
         genuine,&
         'Velocity','km/s',code_unit_velo,&
         nc,&
         spe%ref%c,&
         spe%ref%v,&
         spe%inc%v,&
         spe%v,error)
    if (error) return
    call cubetools_axis_put_and_derive(&
         genuine,&
         'Redshift','---',code_unit_unk,&
         nc,&
         spe%ref%c,&
         spe%ref%z,&
         spe%inc%z,&
         spe%z,error)
    if (error) return
    ! Success => Fill genuinity and size
    spe%genuine = genuine
    spe%nc = nc
  end subroutine cubetools_spectral_derive_axes
  !
  subroutine cubetools_spectral_modify_rest_frequency(spe,freq,error)
    use phys_const
    use gkernel_interfaces
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spectral_prog_t), intent(inout) :: spe
    real(kind=coor_k),      intent(in)    :: freq
    logical,                intent(inout) :: error
    !
    real(kind=4) :: newvelinc
    character(len=*), parameter :: rname='SPECTRAL>MODIFY>REST>FREQUENCY'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (freq.eq.spe%ref%f) return
    !
    call modify_rest_frequency(&
         freq,&      ! desired freqref
         spe%ref%c,& ! chanref
         spe%ref%f,& ! freqref
         spe%ref%i,& ! imaref
         spe%inc%f,& ! freqinc 
         newvelinc,& ! veloinc => not used because only real*4
         error)
    if (error) return    
    !
    spe%ref%l = clight/spe%ref%f
    ! spe%ref%v => unmodified
    spe%ref%z = spe%ref%v/clight_kms
    !
    spe%inc%i = -spe%inc%f
    spe%inc%l = -spe%ref%l*(spe%inc%f/spe%ref%f)
    spe%inc%v = -clight_kms*spe%inc%f/spe%ref%f
    spe%inc%z = spe%inc%v/clight_kms
    !
    call cubetools_spectral_rederive(spe,error)
    if (error) return
  end subroutine cubetools_spectral_modify_rest_frequency
  !
  subroutine cubetools_spectral_modify_frame_velocity(spe,velo,error)
    use phys_const
    use gkernel_interfaces
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spectral_prog_t), intent(inout) :: spe
    real(kind=coor_k),      intent(in)    :: velo
    logical,                intent(inout) :: error
    !
    real(kind=4) :: newvelref,newvelinc
    character(len=*), parameter :: rname='SPECTRAL>MODIFY>FRAME>VELOCITY'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (velo.eq.spe%ref%v) return
    !
    newvelref = real(spe%ref%v,kind=4)
    call modify_frame_velocity(&
         real(velo,kind=4),& ! desired veloref
         spe%ref%c,&         ! chanref
         spe%ref%f,&         ! freqref
         spe%inc%f,&         ! freqinc
         newvelref,&         ! veloref => not used because only real*4
         newvelinc,&         ! veloinc => not used because only real*4
         error)
    if (error) return
    spe%ref%v = real(velo,kind=8)
    !
    ! spe%ref%i => unchanged
    spe%ref%l = clight/spe%ref%f
    spe%ref%z = spe%ref%v/clight_kms
    !
    spe%inc%i = -spe%inc%f
    spe%inc%l = -spe%ref%l*(spe%inc%f/spe%ref%f)
    spe%inc%v = -clight_kms*spe%inc%f/spe%ref%f
    spe%inc%z = spe%inc%v/clight_kms
    !
    call cubetools_spectral_rederive(spe,error)
    if (error) return
  end subroutine cubetools_spectral_modify_frame_velocity
  !
  subroutine cubetools_spectral_update_from_freqaxis(spe,axis,error)
    use phys_const
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spectral_prog_t), intent(inout) :: spe
    type(axis_t),           intent(in)    :: axis
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>UPDATE>FROM>FREQAXIS'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    spe%genuine = .true.
    spe%nc = axis%n
    spe%ref%c = axis%ref
    spe%ref%f = axis%val
    spe%inc%f = axis%inc
    !
    ! spe%ref%i => *** JP unclear
    spe%ref%l = clight/spe%ref%f
    ! spe%ref%v => unchanged
    ! spe%ref%z => unchanged
    !
    spe%inc%i = -spe%inc%f
    spe%inc%l = -spe%ref%l*(spe%inc%f/spe%ref%f)
    spe%inc%v = -clight_kms*spe%inc%f/spe%ref%f
    spe%inc%z = spe%inc%v/clight_kms    
  end subroutine cubetools_spectral_update_from_freqaxis
  !
  subroutine cubetools_spectral_update_from_veloaxis(spe,axis,error)
    use phys_const
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spectral_prog_t), intent(inout) :: spe
    type(axis_t),           intent(in)    :: axis
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>UPDATE>FROM>VELOAXIS'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    spe%genuine = .true.
    spe%nc = axis%n
    spe%ref%c = axis%ref
    spe%ref%v = axis%val
    spe%inc%v = axis%inc
    !
    ! spe%ref%f => unchanged
    ! spe%ref%i => unchanged
    ! spe%ref%l => unchanged
    spe%ref%z = spe%ref%v/clight_kms
    !
    spe%inc%f = -spe%ref%f*spe%inc%v/clight_kms
    spe%inc%i = -spe%inc%f
    spe%inc%l = -spe%ref%l*(spe%inc%f/spe%ref%f)
    spe%inc%z = spe%inc%v/clight_kms    
  end subroutine cubetools_spectral_update_from_veloaxis
  !
  !---------------------------------------------------------------------
  !
  subroutine cubetools_spectral_load_frame_into(prog,variable,error)
    use cubetools_uservar
    !------------------------------------------------------------------------
    ! Load the spectral frame information into a user variable
    !------------------------------------------------------------------------
    class(spectral_prog_t), intent(in)    :: prog
    type(uservar_t),        intent(inout) :: variable
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>LOAD>FRAME>INTO'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call variable%set(speframes(prog%frame),error)
    if (error) return
  end subroutine cubetools_spectral_load_frame_into
  !
  subroutine cubetools_spectral_load_velocity_into(prog,structure,error)
    use cubetools_unit
    use cubetools_userstruct
    !------------------------------------------------------------------------
    ! Load the systemic velocity information into a user structure
    !------------------------------------------------------------------------
    class(spectral_prog_t), intent(in)    :: prog
    type(userstruct_t),     intent(inout) :: structure
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>LOAD>VELOCITY>INTO'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_spectral_load_velocity_or_redshift_into(&
         prog%ref%v,code_unit_velo,prog%conv,structure,error)
  end subroutine cubetools_spectral_load_velocity_into
  !
  subroutine cubetools_spectral_load_redshift_into(prog,structure,error)
    use cubetools_unit
    use cubetools_userstruct
    !------------------------------------------------------------------------
    ! Load the systemic redshift information into a user structure
    !------------------------------------------------------------------------
    class(spectral_prog_t), intent(in)    :: prog
    type(userstruct_t),     intent(inout) :: structure
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>LOAD>REDSHIFT>INTO'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_spectral_load_velocity_or_redshift_into(&
         prog%ref%z,code_unit_unk,prog%conv,structure,error)
  end subroutine cubetools_spectral_load_redshift_into
  !
  subroutine cubetools_spectral_load_velocity_or_redshift_into(&
       value,code_unit,code_convention,structure,error)
    use cubetools_unit
    use cubetools_userstruct
    !------------------------------------------------------------------------
    ! Load the velocity or reshift information into a user structure
    !------------------------------------------------------------------------
    real(kind=coor_k),    intent(in)    :: value
    integer(kind=code_k), intent(in)    :: code_unit
    integer(kind=code_k), intent(in)    :: code_convention
    type(userstruct_t),   intent(inout) :: structure
    logical,              intent(inout) :: error
    !
    type(unit_user_t) :: unit
    character(len=*), parameter :: rname='SPECTRAL>LOAD>VELOCITY>OR>REDSHIFT>INTO'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call structure%def(error)
    if (error) return
    call unit%get_from_code(code_unit,error)
    if (error) return
    call structure%set_member('value',value*unit%user_per_prog,error)
    if (error) return
    call structure%set_member('unit',unit%name,error)
    if (error) return
    call structure%set_member('convention',speconvnames(code_convention),error)
    if (error) return
  end subroutine cubetools_spectral_load_velocity_or_redshift_into
  !
  subroutine cubetools_spectral_load_line_into(prog,variable,error)
    use cubetools_uservar
    !------------------------------------------------------------------------
    ! Load the spectral line name into a user variable
    !------------------------------------------------------------------------
    class(spectral_prog_t), intent(in)    :: prog
    type(uservar_t),        intent(inout) :: variable
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>LOAD>LINE>INTO'
    !
    call cubetools_message(toolseve%trace,rname,'welcome')
    !
    call variable%set(prog%line,error)
    if (error) return
  end subroutine cubetools_spectral_load_line_into
  !
  subroutine cubetools_spectral_load_frequency_into(prog,structure,error)
    use cubetools_unit
    use cubetools_userstruct
    !------------------------------------------------------------------------
    ! Load the rest frequency information into a user structure
    !------------------------------------------------------------------------
    class(spectral_prog_t), intent(in)    :: prog
    type(userstruct_t),     intent(inout) :: structure
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>LOAD>FREQUENCY>INTO'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_spectral_load_frequency_or_wavelength_into(&
         prog%ref%f,prog%ref%i,code_unit_freq,structure,error)
  end subroutine cubetools_spectral_load_frequency_into
  !
  subroutine cubetools_spectral_load_wavelength_into(prog,structure,error)
    use phys_const
    use cubetools_unit
    use cubetools_userstruct
    !------------------------------------------------------------------------
    ! Load the rest wavelength information into a user structure
    !------------------------------------------------------------------------
    class(spectral_prog_t), intent(in)    :: prog
    type(userstruct_t),     intent(inout) :: structure
    logical,                intent(inout) :: error
    !
    real(kind=coor_k) :: image
    character(len=*), parameter :: rname='SPECTRAL>WAVELENGTH>INTO'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    image = clight/prog%ref%i
    call cubetools_spectral_load_frequency_or_wavelength_into(&
         prog%ref%l,image,code_unit_wave,structure,error)
  end subroutine cubetools_spectral_load_wavelength_into
  !
  subroutine cubetools_spectral_load_frequency_or_wavelength_into(&
       signal,image,code_unit,structure,error)
    use cubetools_unit    
    use cubetools_userstruct
    !------------------------------------------------------------------------
    ! Load the frequency or wavelength information into a user structure
    !------------------------------------------------------------------------
    real(kind=coor_k),    intent(in)    :: signal
    real(kind=coor_k),    intent(in)    :: image
    integer(kind=code_k), intent(in)    :: code_unit
    type(userstruct_t),   intent(inout) :: structure
    logical,              intent(inout) :: error
    !
    type(unit_user_t) :: unit
    character(len=*), parameter :: rname='SPECTRAL>LOAD>FREQUENCY>OR>WAVELENGTH>INTO'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call structure%def(error)
    if (error) return
    call unit%get_from_code(code_unit,error)
    if (error) return
    call structure%set_member('signal',signal*unit%user_per_prog,error)
    if (error) return
    call structure%set_member('image',image*unit%user_per_prog,error)
    if (error) return
    call structure%set_member('unit',unit%name,error)
    if (error) return
  end subroutine cubetools_spectral_load_frequency_or_wavelength_into
  !
  subroutine cubetools_spectral_sicdef(spe,name,readonly,error)
    use gkernel_interfaces
    !-------------------------------------------------------------------
    !***JP: Obsolete => Only used by the OLDLOAD command!
    !-------------------------------------------------------------------
    class(spectral_prog_t), intent(in)    :: spe
    character(len=*),       intent(in)    :: name
    logical,                intent(in)    :: readonly
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>SICDEF'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call sic_defstructure(name,global,error)
    if (error) return
    call sic_def_inte(trim(name)//'%frame',spe%frame,0,0,readonly,error)
    if (error) return
    call sic_def_inte(trim(name)//'%conv',spe%conv,0,0,readonly,error)
    if (error) return
    call sic_def_charn(trim(name)//'%line',spe%line,0,0,readonly,error)
    if (error) return
    call cubetools_speelt_sicdef(trim(name)//'%ref',spe%ref,readonly,error)
    if (error) return
    call cubetools_speelt_sicdef(trim(name)//'%inc',spe%inc,readonly,error)
    if (error) return
    call cubetools_axis_sicdef(trim(name)//'%C',spe%c,readonly,error)
    if (error) return
    call cubetools_axis_sicdef(trim(name)//'%F',spe%f,readonly,error)
    if (error) return
    call cubetools_axis_sicdef(trim(name)//'%I',spe%i,readonly,error)
    if (error) return
    call cubetools_axis_sicdef(trim(name)//'%L',spe%l,readonly,error)
    if (error) return
    call cubetools_axis_sicdef(trim(name)//'%V',spe%v,readonly,error)
    if (error) return
    call cubetools_axis_sicdef(trim(name)//'%Z',spe%z,readonly,error)
    if (error) return
  end subroutine cubetools_spectral_sicdef  
end module cubetools_spectral_prog_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_spectral_consistency_types
  use cubetools_parameters
  use cubetools_messaging
  use cubetools_axis_types
  use cubetools_speelt_types
  use cubetools_header_interface
  use cubetools_consistency_types
  use cubetools_spectral_prog_types
  !
  public :: spectral_cons_t
  private
  !
  type spectral_cons_t
     ! *** JP genuine et nc should be added here
     logical                    :: do    = .true.  ! Check the section
     logical                    :: prob  = .false. ! Is there a problem
     logical                    :: mess  = .true.  ! Output message for this section?
     type(consistency_desc_t)   :: frame           ! frame Consistency
     type(consistency_desc_t)   :: conv            ! Convention Consistency
     type(consistency_desc_t)   :: line            ! line Consistency
     type(speelt_cons_t)        :: ref             ! Reference Consistency
     type(axis_cons_t)          :: c               ! channel axis consistency
     type(axis_cons_t)          :: f               ! frequency axis consistency
     type(axis_cons_t)          :: i               ! image axis consistency
     type(axis_cons_t)          :: l               ! wavelength axis consistency
     type(axis_cons_t)          :: v               ! velocity axis consistency
     type(axis_cons_t)          :: z               ! redshift axis consistency
   contains
     procedure, public :: init    => cubetools_spectral_consistency_init
     procedure, public :: set_tol => cubetools_spectral_consistency_set_tolerance
     procedure, public :: check   => cubetools_spectral_consistency_check
     procedure, public :: list    => cubetools_spectral_consistency_list
     procedure, public :: final   => cubetools_spectral_consistency_final
  end type spectral_cons_t
  !  
contains
  !
  subroutine cubetools_spectral_consistency_init(cons,error)
    !-------------------------------------------------------------------
    ! Init the consistency between two spectral sections
    !-------------------------------------------------------------------
    class(spectral_cons_t), intent(out)   :: cons
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>CONSISTENCY>INIT'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_consistency_init(notol,check,mess,cons%frame,error)
    if (error) return
    call cubetools_consistency_init(notol,check,mess,cons%conv,error)
    if (error) return
    call cubetools_consistency_init(notol,check,mess,cons%line,error)
    if (error) return
    call cubetools_speelt_consistency_init(cons%ref,error)
    if (error) return
    call cubetools_axis_consistency_init(cons%c,error)
    if (error) return
    call cubetools_axis_consistency_init(cons%f,error)
    if (error) return
    call cubetools_axis_consistency_init(cons%v,error)
    if (error) return
    call cubetools_axis_consistency_init(cons%i,error)
    if (error) return
    call cubetools_axis_consistency_init(cons%l,error)
    if (error) return
    call cubetools_axis_consistency_init(cons%z,error)
    if (error) return
  end subroutine cubetools_spectral_consistency_init
  !
  subroutine cubetools_spectral_consistency_set_tolerance(cons,spetol,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(spectral_cons_t), intent(inout) :: cons
    real(kind=tole_k),      intent(in)    :: spetol
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>CONSISTENCY>SET>TOLERANCE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_speelt_consistency_set_tol(spetol,cons%ref,error)
    if (error) return
    call cubetools_axis_consistency_set_tol(spetol,cons%c,error)
    if (error) return
    call cubetools_axis_consistency_set_tol(spetol,cons%f,error)
    if (error) return
    call cubetools_axis_consistency_set_tol(spetol,cons%v,error)
    if (error) return
    call cubetools_axis_consistency_set_tol(spetol,cons%i,error)
    if (error) return
    call cubetools_axis_consistency_set_tol(spetol,cons%l,error)
    if (error) return
    call cubetools_axis_consistency_set_tol(spetol,cons%z,error)
    if (error) return  
  end subroutine cubetools_spectral_consistency_set_tolerance
  !
  subroutine cubetools_spectral_consistency_check(cons,spe1,spe2,error)
    !-------------------------------------------------------------------
    ! Check the consistency between two spectral sections
    !-------------------------------------------------------------------
    class(spectral_cons_t), intent(inout) :: cons
    type(spectral_prog_t),  intent(in)    :: spe1
    type(spectral_prog_t),  intent(in)    :: spe2
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>CONSISTENCY>CHECK'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (.not.cons%do) return
    !
    call cubetools_consistency_integer_check(cons%frame,spe1%frame,spe2%frame,error)
    if (error) return
    call cubetools_consistency_integer_check(cons%conv,spe1%conv,spe2%conv,error)
    if (error) return
    call cubetools_consistency_string_check(cons%line,spe1%line,spe2%line,error)
    if (error) return
    call cubetools_speelt_consistency_check(cons%ref,spe1%inc,spe1%ref,spe2%inc,spe2%ref,error)
    if (error) return
    call cubetools_axis_consistency_check(cons%c,spe1%c,spe2%c,error)
    if (error) return
    call cubetools_axis_consistency_check(cons%f,spe1%f,spe2%f,error)
    if (error) return
    call cubetools_axis_consistency_check(cons%v,spe1%v,spe2%v,error)
    if (error) return
    call cubetools_axis_consistency_check(cons%i,spe1%i,spe2%i,error)
    if (error) return
    call cubetools_axis_consistency_check(cons%l,spe1%l,spe2%l,error)
    if (error) return
    call cubetools_axis_consistency_check(cons%z,spe1%z,spe2%z,error)
    if (error) return
    !
    cons%prob = cons%frame%prob.or.cons%conv%prob.or.cons%line%prob&
         .or.cons%ref%prob.or.cons%c%prob.or.cons%f%prob&
         .or.cons%v%prob.or.cons%i%prob.or.cons%l%prob.or.cons%z%prob
  end subroutine cubetools_spectral_consistency_check
  !
  subroutine cubetools_spectral_consistency_list(cons,spe1,spe2,error)
    !-------------------------------------------------------------------
    ! List the consistency between two spectral sections
    !-------------------------------------------------------------------
    class(spectral_cons_t), intent(in)    :: cons
    type(spectral_prog_t),  intent(in)    :: spe1
    type(spectral_prog_t),  intent(in)    :: spe2
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>CONSISTENCY>CHECK>LIST'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (.not.cons%mess) return
    !
    call cubetools_consistency_title('spectral sections',2,cons%do,cons%prob,error)
    if (error) return
    if (cons%do.and.cons%prob) then
       call cubetools_spectral_frame_consistency_list(cons%frame,spe1%frame,spe2%frame,error)
       if (error) return
       call cubetools_spectral_convention_consistency_list(cons%conv,spe1%conv,spe2%conv,error)
       if (error) return
       call cubetools_spectral_line_consistency_list(cons%line,spe1%line,spe2%line,error)
       if (error) return
       call cubetools_speelt_consistency_list(cons%ref,spe1%ref,spe2%ref,error)
       if (error) return
       call cubetools_axis_consistency_list('Channel',cons%c,spe1%c,spe2%c,error)
       if (error) return
       call cubetools_axis_consistency_list('Frequency',cons%f,spe1%f,spe2%f,error)
       if (error) return
       call cubetools_axis_consistency_list('Image frequency',cons%i,spe1%i,spe2%i,error)
       if (error) return
       call cubetools_axis_consistency_list('Wavelength',cons%l,spe1%l,spe2%l,error)
       if (error) return
       call cubetools_axis_consistency_list('Velocity',cons%v,spe1%v,spe2%v,error)
       if (error) return
       call cubetools_axis_consistency_list('Redshift',cons%z,spe1%z,spe2%z,error)
       if (error) return
    endif
    call cubetools_message(seve%r,rname,'')
  end subroutine cubetools_spectral_consistency_list
  !
  subroutine cubetools_spectral_frame_consistency_list(cons,frame1,frame2,error)
    use cubetools_header_interface
    !-------------------------------------------------------------------
    ! List the consistency between two frames
    !-------------------------------------------------------------------
    class(consistency_desc_t), intent(in)    :: cons
    integer(kind=code_k),      intent(in)    :: frame1
    integer(kind=code_k),      intent(in)    :: frame2
    logical,                   intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>FRAME>CONSISTENCY>LIST'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (.not.cons%mess) return
    !
    call cubetools_consistency_title('spectral frames',3,cons%check,cons%prob,error)
    if (error) return
    if (cons%check.and.cons%prob) then
       call cubetools_consistency_string_print('Frames',cons,speframes(frame1),speframes(frame2),error)
       if (error) return
    endif
    call cubetools_message(seve%r,rname,'')
  end subroutine cubetools_spectral_frame_consistency_list
  !
  subroutine cubetools_spectral_convention_consistency_list(cons,conv1,conv2,error)
    use cubetools_header_interface
    !-------------------------------------------------------------------
    ! List the consistency between two conventions
    !-------------------------------------------------------------------
    class(consistency_desc_t), intent(in)    :: cons
    integer(kind=code_k),      intent(in)    :: conv1
    integer(kind=code_k),      intent(in)    :: conv2
    logical,                   intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>CONVENTION>CONSISTENCY>LIST'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (.not.cons%mess) return
    !
    call cubetools_consistency_title('velocity conventions',3,cons%check,cons%prob,error)
    if (error) return
    if (cons%check.and.cons%prob) then
       call cubetools_consistency_string_print('Conventions',cons,speconvnames(conv1),speconvnames(conv2),error)
       if (error) return
    endif
    call cubetools_message(seve%r,rname,'')
  end subroutine cubetools_spectral_convention_consistency_list
  !
  subroutine cubetools_spectral_line_consistency_list(cons,line1,line2,error)
    !-------------------------------------------------------------------
    ! List the consistency between two lines
    !-------------------------------------------------------------------
    class(consistency_desc_t), intent(in)    :: cons
    character(len=*),          intent(in)    :: line1
    character(len=*),          intent(in)    :: line2
    logical,                   intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>LINE>CONSISTENCY>LIST'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (.not.cons%mess) return
    !
    call cubetools_consistency_title('spectral lines',3,cons%check,cons%prob,error)
    if (error) return
    if (cons%check.and.cons%prob) then
       call cubetools_consistency_string_print('Lines',cons,line1,line2,error)
       if (error) return
    endif
    call cubetools_message(seve%r,rname,'')
  end subroutine cubetools_spectral_line_consistency_list
  !
  subroutine cubetools_spectral_consistency_final(cons,error)
    !-------------------------------------------------------------------
    ! Final the consistency between two spectral sections
    !-------------------------------------------------------------------
    class(spectral_cons_t), intent(inout) :: cons
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>CONSISTENCY>FINAL'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_consistency_final(cons%frame,error)
    if (error) return
    call cubetools_consistency_final(cons%conv,error)
    if (error) return
    call cubetools_consistency_final(cons%line,error)
    if (error) return
    call cubetools_speelt_consistency_final(cons%ref,error)
    if (error) return
    call cubetools_axis_consistency_final(cons%c,error)
    if (error) return
    call cubetools_axis_consistency_final(cons%f,error)
    if (error) return
    call cubetools_axis_consistency_final(cons%v,error)
    if (error) return
    call cubetools_axis_consistency_final(cons%i,error)
    if (error) return
    call cubetools_axis_consistency_final(cons%l,error)
    if (error) return
    call cubetools_axis_consistency_final(cons%z,error)
    if (error) return
  end subroutine cubetools_spectral_consistency_final  
end module cubetools_spectral_consistency_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_spectral_frame_types
  use cubetools_parameters
  use cubetools_messaging
  use cubetools_structure
  use cubetools_keywordlist_types
  use cubetools_spectral_prog_types
  !
  public :: spectral_frame_comm_t,spectral_frame_user_t
  private
  !
  type spectral_frame_comm_t
     type(option_t),      pointer :: comm
     type(keywordlist_comm_t), pointer :: type
   contains
     procedure, public :: register  => cubetools_spectral_frame_comm_register
     procedure, public :: parse     => cubetools_spectral_frame_comm_parse
  end type spectral_frame_comm_t
  !
  type spectral_frame_user_t
     logical               :: do   = .false.  ! Key is present
     character(len=argu_l) :: type = strg_unk ! Spectral frame type
   contains
     procedure, public :: toprog => cubetools_spectral_frame_user_toprog
  end type spectral_frame_user_t
  !
contains
  subroutine cubetools_spectral_frame_comm_register(comm,abstract,error)
    use cubetools_header_interface
    !----------------------------------------------------------------------
    ! Register a /SPECTRALFRAME key
    !----------------------------------------------------------------------
    class(spectral_frame_comm_t), intent(inout) :: comm
    character(len=*),             intent(in)    :: abstract
    logical,                      intent(inout) :: error
    !
    type(keywordlist_comm_t) :: keyarg
    !
    character(len=*), parameter :: rname='SPECTRAL>FRAME>COMM>REGISTER'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         "SPECTRALFRAME",'type',&
         abstract,&
         'WARNING: Systemic velocity and redshift are not updated&
       & accordingly when the spectral frame is changed',&
         comm%comm,error)
    if (error) return
    call keyarg%register(&
         'type',&
         'Spectral frame type',&
         strg_id,&
         code_arg_mandatory,&
         speframes,&
         .not.flexible,&
         comm%type,&
         error)
    if (error) return
  end subroutine cubetools_spectral_frame_comm_register
  !
  subroutine cubetools_spectral_frame_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! Parse spectral frame information as /SPECTRALFRAME type
    ! ----------------------------------------------------------------------
    class(spectral_frame_comm_t), intent(in)    :: comm
    character(len=*),             intent(in)    :: line
    type(spectral_frame_user_t),  intent(inout) :: user
    logical,                      intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>FRAME>COMM>PARSE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    user%type = strg_equal
    !
    call comm%comm%present(line,user%do,error)
    if (error) return
    if (user%do) then
       call cubetools_getarg(line,comm%comm,1,user%type,mandatory,error)
       if (error) return
    endif
  end subroutine cubetools_spectral_frame_comm_parse
  !
  subroutine cubetools_spectral_frame_user_toprog(user,comm,prog,error)
    use cubetools_header_interface
    use cubetools_user2prog
    !----------------------------------------------------------------------
    ! Transfer frame value from user to prog
    !----------------------------------------------------------------------
    class(spectral_frame_user_t), intent(in)    :: user
    type(spectral_frame_comm_t),  intent(in)    :: comm
    type(spectral_prog_t),        intent(inout) :: prog
    logical,                      intent(inout) :: error
    !
    integer(kind=code_k) :: previous,default
    character(len=*), parameter :: rname='SPECTRAL>FRAME>USER>TOPROG'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (user%do) then
       default = code_speframe_lsrk
       previous = prog%frame
       call cubetools_user2prog_resolve_code(comm%type,user%type,&
            default,previous,prog%frame,error)
       if (error) return
    endif
  end subroutine cubetools_spectral_frame_user_toprog
end module cubetools_spectral_frame_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_spectral_v_or_z_types
  !------------------------------------------------------------------------
  ! Velocity or redshift information
  !------------------------------------------------------------------------
  use cubetools_parameters
  use cubetools_messaging
  use cubetools_structure
  use cubetools_keywordlist_types
  use cubetools_unit_arg
  use cubetools_spectral_prog_types
  !
  public :: spectral_v_or_z_comm_t,spectral_v_or_z_user_t
  private
  !
  type spectral_v_or_z_comm_t
     type(option_t),      pointer :: comm
     type(unit_arg_t),    pointer :: unit
     type(keywordlist_comm_t), pointer :: conv
   contains
     procedure, public :: register  => cubetools_spectral_v_or_z_comm_register
     procedure, public :: parse     => cubetools_spectral_v_or_z_comm_parse
  end type spectral_v_or_z_comm_t
  !
  type spectral_v_or_z_user_t
     logical               :: do          = .false.  ! Key is present
     character(len=argu_l) :: val         = strg_unk ! Value at reference pixel
     character(len=argu_l) :: unit        = strg_unk ! unit
     character(len=argu_l) :: convention  = strg_unk ! Convention (Optical|Radio)
  end type spectral_v_or_z_user_t
  !
contains
  !
  subroutine cubetools_spectral_v_or_z_comm_register(comm,name,abstract,error)
    use cubetools_unit
    use cubetools_header_interface
    !----------------------------------------------------------------------
    ! Register a /VELOCITY|REDSHIFT key
    !----------------------------------------------------------------------
    class(spectral_v_or_z_comm_t), intent(inout) :: comm
    character(len=*),              intent(in)    :: name
    character(len=*),              intent(in)    :: abstract
    logical,                       intent(inout) :: error
    !
    character(len=128) :: help
    type(standard_arg_t) :: stdarg
    type(keywordlist_comm_t)  :: keyarg
    type(unit_arg_t) :: unitarg
    integer(kind=code_k) :: unit_kind
    character(len=*), parameter :: rname='SPECTRAL>V>OR>Z>COMM>REGISTER'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    select case(name)
    case('VELOCITY')
       help = 'Systemic redshift is updated accordingly'
       unit_kind = code_unit_velo
    case('REDSHIFT')
       help = 'Systemic velocity is updated accordingly'
       unit_kind = code_unit_unk
    case default
       call cubetools_message(seve%e,rname,"Can only register a VELOCITY or REDSHIFT key")
       error = .true.
       return
    end select
    !
    call cubetools_register_option(&
         name,'value [unit [convention]]',&
         abstract,&
         help,&
         comm%comm,error)
    if (error) return
    call stdarg%register(&
         'value',&
         'Reference value',&
         '"*" or "=" mean previous value is kept',&
         code_arg_mandatory,&
         error)
    if (error) return   
    call unitarg%register(&
         'unit',&
         'Unit',&
         '"=" or "*" mean current unit',&
         code_arg_optional,&
         unit_kind,&
         comm%unit,&
         error)
    if (error) return
    call keyarg%register(&
         'convention',&
         'Convention type',&
         '"=" mean previous value is kept, * means RADIO. Currently&
         & only RADIO is properly supported',&
         code_arg_optional,&
         speconvnames,&
         .not.flexible,&
         comm%conv,&
         error)
    if (error) return
  end subroutine cubetools_spectral_v_or_z_comm_register
  !
  subroutine cubetools_spectral_v_or_z_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! Parse /VELOCITY|REDSHIFT val [convention]
    ! ----------------------------------------------------------------------
    class(spectral_v_or_z_comm_t), intent(in)    :: comm
    character(len=*),              intent(in)    :: line
    class(spectral_v_or_z_user_t), intent(inout) :: user
    logical,                       intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>V>OR>Z>COMM>PARSE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    ! Convention should not be changed if user has not given a value
    user%val        = strg_star
    user%unit       = strg_equal
    user%convention = strg_equal
    call comm%comm%present(line,user%do,error)
    if (error) return
    if (user%do) then
       call cubetools_getarg(line,comm%comm,1,user%val,mandatory,error)
       if (error) return
       call cubetools_getarg(line,comm%comm,2,user%unit,optional,error)
       if (error) return
       call cubetools_getarg(line,comm%comm,3,user%convention,optional,error)
       if (error) return
    endif
  end subroutine cubetools_spectral_v_or_z_comm_parse
end module cubetools_spectral_v_or_z_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_spectral_velocity_types
  use cubetools_parameters
  use cubetools_messaging
  use cubetools_spectral_v_or_z_types
  !
  public :: spectral_velocity_comm_t,spectral_velocity_user_t
  private
  !
  type, extends(spectral_v_or_z_comm_t) :: spectral_velocity_comm_t
  end type spectral_velocity_comm_t
  !
  type, extends(spectral_v_or_z_user_t) :: spectral_velocity_user_t
   contains
     procedure, public :: toprog => cubetools_spectral_velocity_user_toprog
  end type spectral_velocity_user_t
  !
contains
  !
  subroutine cubetools_spectral_velocity_user_toprog(user,comm,prog,error) 
    use phys_const
    use cubetools_unit
    use cubetools_user2prog
    use cubetools_header_interface
    use cubetools_spectral_prog_types
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(spectral_velocity_user_t), intent(in)    :: user
    type(spectral_velocity_comm_t),  intent(in)    :: comm
    type(spectral_prog_t),           intent(inout) :: prog
    logical,                         intent(inout) :: error
    !
    type(unit_user_t) :: unit
    integer(kind=code_k) :: defaultconv,previousconv
    real(kind=coor_k) :: defaultval,previousval
    character(len=*), parameter :: rname='SPECTRAL>VELOCITY>USER>TOPROG'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (user%do) then
       call unit%get_from_name_for_code(user%unit,code_unit_velo,error)
       if (error) return
       defaultval = prog%ref%v
       previousval = prog%ref%v
       call cubetools_user2prog_resolve_all(user%val,unit,&
            defaultval,previousval,prog%ref%v,error)
       if (error) return
       prog%ref%z = prog%ref%v/clight_kms
       !
       defaultconv = code_speconv_radio
       previousconv = prog%conv
       call cubetools_user2prog_resolve_code(comm%conv,user%convention,&
            defaultconv,previousconv,prog%conv,error)
       if (error) return
    endif
  end subroutine cubetools_spectral_velocity_user_toprog
end module cubetools_spectral_velocity_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_spectral_redshift_types
  use cubetools_parameters
  use cubetools_messaging
  use cubetools_spectral_v_or_z_types
  !
  public :: spectral_redshift_comm_t,spectral_redshift_user_t
  private
  !
  type, extends(spectral_v_or_z_comm_t) :: spectral_redshift_comm_t
  end type spectral_redshift_comm_t
  !
  type, extends(spectral_v_or_z_user_t) :: spectral_redshift_user_t
   contains
     procedure, public :: toprog => cubetools_spectral_redshift_user_toprog
  end type spectral_redshift_user_t
  !
contains
  !
  subroutine cubetools_spectral_redshift_user_toprog(user,comm,prog,error)
    use phys_const
    use cubetools_unit
    use cubetools_user2prog
    use cubetools_header_interface
    use cubetools_spectral_prog_types
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(spectral_redshift_user_t), intent(in)    :: user
    type(spectral_redshift_comm_t),  intent(in)    :: comm
    type(spectral_prog_t),           intent(inout) :: prog
    logical,                         intent(inout) :: error
    !
    type(unit_user_t) :: unit
    integer(kind=code_k) :: defaultconv,previousconv
    real(kind=coor_k) :: defaultval,previousval
    character(len=*), parameter :: rname='SPECTRAL>REDSHIFT>USER>TOPROG'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (user%do) then
       call unit%get_from_name_for_code(user%unit,code_unit_unk,error)
       if (error) return
       defaultval = prog%ref%z
       previousval = prog%ref%z
       call cubetools_user2prog_resolve_all(user%val,unit,&
            defaultval,previousval,prog%ref%z,error)
       if (error) return
       prog%ref%v = prog%ref%z*clight_kms
       !
       defaultconv = code_speconv_radio
       previousconv = prog%conv
       call cubetools_user2prog_resolve_code(comm%conv,user%convention,&
            defaultconv,previousconv,prog%conv,error)
       if (error) return
    endif
  end subroutine cubetools_spectral_redshift_user_toprog
end module cubetools_spectral_redshift_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_spectral_line_types
  use cubetools_parameters
  use cubetools_messaging
  use cubetools_structure
  use cubetools_spectral_prog_types
  !
  public :: spectral_line_comm_t,spectral_line_user_t
  private
  !
  type spectral_line_comm_t
     type(option_t), pointer :: comm
   contains
     procedure, public :: register  => cubetools_spectral_line_comm_register
     procedure, public :: parse     => cubetools_spectral_line_comm_parse
  end type spectral_line_comm_t
  !
  type spectral_line_user_t
     logical               :: do   = .false.  ! Key is present
     character(len=argu_l) :: line = strg_unk ! Line name
   contains
     procedure, public :: toprog => cubetools_spectral_line_user_toprog
  end type spectral_line_user_t
  !
contains
  !
  subroutine cubetools_spectral_line_comm_register(comm,abstract,error)
    use cubetools_unit
    !----------------------------------------------------------------------
    ! Register the /LINE option
    !----------------------------------------------------------------------
    class(spectral_line_comm_t), intent(inout) :: comm
    character(len=*),            intent(in)    :: abstract
    logical,                     intent(inout) :: error
    !
    type(standard_arg_t) :: stdarg
    character(len=*), parameter :: rname='SPECTRAL>LINE>COMM>REGISTER'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         'LINE','name',&
         abstract,&
         strg_id,&
         comm%comm,error)
    if (error) return
    call stdarg%register(&
         'line',&
         'Line name',&
         '"*" or "=" mean previous value is kept',&
         code_arg_mandatory,&
         error)
    if (error) return
  end subroutine cubetools_spectral_line_comm_register
  !
  subroutine cubetools_spectral_line_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! Parse /LINE Name
    ! ----------------------------------------------------------------------
    class(spectral_line_comm_t), intent(in)    :: comm
    character(len=*),            intent(in)    :: line
    type(spectral_line_user_t),  intent(inout) :: user
    logical,                     intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>LINE>COMM>PARSE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    user%line = strg_star
    !
    call comm%comm%present(line,user%do,error)
    if (error) return
    if (user%do) then
       call cubetools_getarg(line,comm%comm,1,user%line,mandatory,error)
       if (error) return
    endif
  end subroutine cubetools_spectral_line_comm_parse
  !
  subroutine cubetools_spectral_line_user_toprog(user,prog,error)
    use cubetools_user2prog
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(spectral_line_user_t), intent(in)    :: user
    type(spectral_prog_t),       intent(inout) :: prog
    logical,                     intent(inout) :: error
    !
    character(len=line_l) :: default,previous
    character(len=*), parameter :: rname='SPECTRAL>LINE>USER>TOPROG'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (user%do) then
       if (len_trim(user%line).gt.line_l) call cubetools_message(seve%w,rname,&
            'Line name will be truncated at 12 characters')
       default = prog%line
       previous = prog%line
       call cubetools_user2prog_resolve_all(user%line,default,previous,prog%line,error)
       if (error) return
    endif
  end subroutine cubetools_spectral_line_user_toprog
end module cubetools_spectral_line_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_spectral_f_or_w_types
  !------------------------------------------------------------------------
  ! Frequency or wavelength information
  !------------------------------------------------------------------------
  use cubetools_parameters
  use cubetools_messaging
  use cubetools_structure
  use cubetools_unit_arg
  use cubetools_spectral_prog_types
  !
  public :: spectral_f_or_w_comm_t,spectral_f_or_w_user_t
  private
  !
  type spectral_f_or_w_comm_t
     type(option_t),   pointer :: comm
     type(unit_arg_t), pointer :: unit
   contains
     procedure, public :: register => cubetools_spectral_f_or_w_comm_register
     procedure, public :: parse    => cubetools_spectral_f_or_w_comm_parse
  end type spectral_f_or_w_comm_t
  !
  type spectral_f_or_w_user_t
     logical               :: do     = .false.  ! Key is present
     character(len=argu_l) :: signal = strg_unk ! Rest signal frequency|wavelength
     character(len=argu_l) :: image  = strg_unk ! Rest image  frequency|wavelength
     character(len=argu_l) :: unit   = strg_unk ! Unit
  end type spectral_f_or_w_user_t
  !
contains
  !
  subroutine cubetools_spectral_f_or_w_comm_register(comm,name,abstract,error)
    use cubetools_unit
    use gkernel_interfaces
    !----------------------------------------------------------------------
    ! Register a /FREQUENCY|WAVELENGTH key
    !----------------------------------------------------------------------
    class(spectral_f_or_w_comm_t), intent(inout) :: comm
    character(len=*),              intent(in)    :: name
    character(len=*),              intent(in)    :: abstract
    logical,                       intent(inout) :: error
    !
    type(standard_arg_t) :: stdarg
    type(unit_arg_t) :: unitarg
    character(len=128) :: help
    integer(kind=code_k) :: unit_kind
    character(len=*), parameter :: rname='SPECTRAL>F>OR>W>COMM>REGISTER'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    select case(name)
    case('FREQUENCY')
       help = 'Spectral increments and wavelengths updated&
            & accordingly, values in current frequency unit'
       unit_kind = code_unit_freq
    case('WAVELENGTH')
       help = 'Spectral increments and frequencies updated&
            & accordingly, values in mm'
       unit_kind = code_unit_wave
    case default
       call cubetools_message(seve%e,rname,"Can only register&
            & WAVELENGTH or FREQUENCY options")
       error = .true.
       return
    end select
    !
    call cubetools_register_option(&
         name,'rest [image [unit]]',&
         abstract,&
         help,&
         comm%comm,error)
    if (error) return
    call stdarg%register(&
         'SIGNAL',&
         'Rest frame signal value',&
         '"*" or "=" mean previous value is kept',&
         code_arg_mandatory,&
         error)
    if (error) return
    call stdarg%register(&
         'IMAGE',&
         'Rest frame image value',&
         '"*" or "=" mean previous value is kept',&
         code_arg_optional,&
         error)
    if (error) return
    call unitarg%register(&
         'UNIT',&
         'Unit',&
         '"=" or "*" mean current unit',&
         code_arg_optional,&
         unit_kind,&
         comm%unit,&
         error)
    if (error) return
  end subroutine cubetools_spectral_f_or_w_comm_register
  !
  subroutine cubetools_spectral_f_or_w_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! Parse /FREQ|WAVE linename signal [image]
    ! ----------------------------------------------------------------------
    class(spectral_f_or_w_comm_t), intent(in)    :: comm
    character(len=*),              intent(in)    :: line
    class(spectral_f_or_w_user_t), intent(inout) :: user
    logical,                       intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>F>OR>W>COMM>PARSE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    user%signal = strg_star
    user%image  = strg_star
    user%unit   = strg_equal
    call comm%comm%present(line,user%do,error)
    if (error) return
    if (user%do) then
       call cubetools_getarg(line,comm%comm,1,user%signal,mandatory,error)
       if (error) return
       call cubetools_getarg(line,comm%comm,2,user%image,optional,error)
       if (error) return
       call cubetools_getarg(line,comm%comm,3,user%unit,optional,error)
       if (error) return
    endif
  end subroutine cubetools_spectral_f_or_w_comm_parse
end module cubetools_spectral_f_or_w_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_spectral_freq_types
  use cubetools_parameters
  use cubetools_messaging
  use cubetools_spectral_f_or_w_types
  !
  public :: spectral_freq_comm_t,spectral_freq_user_t
  private
  !
  type, extends(spectral_f_or_w_comm_t) :: spectral_freq_comm_t
  end type spectral_freq_comm_t
  !
  type, extends(spectral_f_or_w_user_t) :: spectral_freq_user_t
   contains
     procedure, public :: toprog => cubetools_spectral_freq_user_toprog
  end type spectral_freq_user_t
  !
contains
  !
  subroutine cubetools_spectral_freq_user_toprog(user,prog,error)
    use phys_const
    use cubetools_unit
    use cubetools_user2prog
    use cubetools_spectral_prog_types
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(spectral_freq_user_t), intent(in)    :: user
    type(spectral_prog_t),       intent(inout) :: prog
    logical,                     intent(inout) :: error
    !
    type(unit_user_t) :: unit
    real(kind=coor_k) :: default,previous
    character(len=*), parameter :: rname='SPECTRAL>FREQ>USER>TOPROG'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (user%do) then
       call unit%get_from_name_for_code(user%unit,code_unit_freq,error)
       if (error) return
       default = prog%ref%f
       previous = prog%ref%f
       call cubetools_user2prog_resolve_all(user%signal,unit,default,previous,prog%ref%f,error)
       if (error) return
       prog%ref%l = clight/prog%ref%f
       prog%inc%l = -prog%ref%l*(prog%inc%f/prog%ref%f)
       prog%inc%v = -clight_kms*prog%inc%f/prog%ref%f
       !
       default = prog%ref%i
       previous = prog%ref%i
       call cubetools_user2prog_resolve_all(user%image,unit,default,previous,prog%ref%i,error)
       if (error) return
    endif
  end subroutine cubetools_spectral_freq_user_toprog
end module cubetools_spectral_freq_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_spectral_wave_types
  use cubetools_parameters
  use cubetools_messaging
  use cubetools_spectral_f_or_w_types
  !
  public :: spectral_wave_comm_t,spectral_wave_user_t
  private
  !
  type, extends(spectral_f_or_w_comm_t) :: spectral_wave_comm_t
  end type spectral_wave_comm_t
  !
  type, extends(spectral_f_or_w_user_t) :: spectral_wave_user_t
   contains
     procedure, public :: toprog => cubetools_spectral_wave_user_toprog
  end type spectral_wave_user_t
  !
contains
  !
  subroutine cubetools_spectral_wave_user_toprog(user,prog,error)
    use phys_const
    use cubetools_unit
    use cubetools_user2prog
    use cubetools_spectral_prog_types
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(spectral_wave_user_t), intent(in)    :: user
    type(spectral_prog_t),       intent(inout) :: prog
    logical,                     intent(inout) :: error
    !
    type(unit_user_t) :: unit
    real(kind=coor_k) :: image,default,previous
    character(len=*), parameter :: rname='SPECTRAL>WAVE>USER>TOPROG'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (user%do) then
       call unit%get_from_name_for_code(user%unit,code_unit_wave,error)
       if (error) return
       default = prog%ref%l
       previous = prog%ref%l
       call cubetools_user2prog_resolve_all(user%signal,unit,default,previous,prog%ref%l,error)
       if (error) return
       prog%ref%f = clight/prog%ref%l
       prog%inc%f = -prog%ref%f*(prog%inc%l/prog%ref%l)
       prog%inc%v = -clight_kms*prog%inc%f/prog%ref%f
       !
       image = clight/prog%ref%i
       default = image
       previous = image
       call cubetools_user2prog_resolve_all(user%image,unit,default,previous,image,error)
       if (error) return
       prog%ref%i = clight/image
    endif
  end subroutine cubetools_spectral_wave_user_toprog
end module cubetools_spectral_wave_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
