!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_beam_types
  use cubetools_parameters
  use cubetools_messaging
  use cubetools_consistency_types
  use cubetools_structure
  use cubetools_unit_arg
  !
  public :: beam_t,beam_user_t,beam_cons_t,beam_opt_t
  public :: beam_table_t
  public :: cubetools_beam_beam2userstruct
  !
  public :: cubetools_beam_init,cubetools_beam_put_and_derive
  public :: cubetools_beam_final,cubetools_beam_get
  public :: cubetools_beam_list
  public :: cubetools_beam_sicdef,cubetools_beam_copy
  public :: cubetools_beam_consistency_init,cubetools_beam_consistency_final
  public :: cubetools_beam_consistency_check,cubetools_beam_consistency_list
  public :: cubetools_beam_consistency_set_tol
  public :: cubetools_beam_scale_with_frequency
  public :: cubetools_beam_to_table
  private
  !
  type beam_t
     real(kind=beam_k) :: major = 0.0 ! [rad]
     real(kind=beam_k) :: minor = 0.0 ! [rad]
     real(kind=beam_k) :: pang  = 0.0 ! [rad]
  end type beam_t
  !
  type beam_opt_t
     type(option_t),   pointer :: opt
     type(unit_arg_t), pointer :: unit_arg
     type(unit_arg_t), pointer :: paunit_arg
   contains
     procedure :: register  => cubetools_beam_register
     procedure :: parse     => cubetools_beam_parse
     procedure :: user2prog => cubetools_beam_user2prog
  end type beam_opt_t
  !
  type beam_user_t
     character(len=argu_l) :: major   = strg_unk ! [sec] Beam major axis
     character(len=argu_l) :: minor   = strg_unk ! [sec] Beam minor axis
     character(len=argu_l) :: pang    = strg_unk ! [deg|rad] Beam position angle
     character(len=argu_l) :: unit    = strg_unk ! Unit for beam axes
     character(len=argu_l) :: paunit  = strg_unk ! Unit for position angle (deg|rad)
     logical               :: do      = .false.  ! Option was present
  end type beam_user_t
  !
  type beam_cons_t
     logical                  :: check=.true.  ! Check the section
     logical                  :: prob =.false. ! Is there a problem
     logical                  :: mess =.true.  ! Output message for this section?
     type(consistency_desc_t) :: major
     type(consistency_desc_t) :: minor
     type(consistency_desc_t) :: pang
  end type beam_cons_t
  !
  type beam_table_t
     integer(kind=chan_k)      :: nb = 0  ! Number of beams
     type(beam_t), allocatable :: beam(:) ! Beams
  end type beam_table_t
  !
contains
  !
  subroutine cubetools_beam_init(beam,error)
    !-------------------------------------------------------------------
    ! Just initialize, ie, beam is intent(out)
    !-------------------------------------------------------------------
    type(beam_t), intent(out)   :: beam
    logical,      intent(inout) :: error
    !
    character(len=*), parameter :: rname='BEAM>INIT'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
  end subroutine cubetools_beam_init
  !
  subroutine cubetools_beam_put_and_derive(major,minor,pang,beam,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    real(kind=beam_k), intent(in)    :: major
    real(kind=beam_k), intent(in)    :: minor
    real(kind=beam_k), intent(in)    :: pang
    type(beam_t),      intent(inout) :: beam
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='BEAM>PUT>AND>DERIVE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    beam%major = major
    beam%minor = minor
    beam%pang  = pang
  end subroutine cubetools_beam_put_and_derive
  !
  subroutine cubetools_beam_get(beam,major,minor,pang,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(beam_t),      intent(in)    :: beam
    real(kind=beam_k), intent(out)   :: major
    real(kind=beam_k), intent(out)   :: minor
    real(kind=beam_k), intent(out)   :: pang
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='BEAM>GET'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    major = beam%major
    minor = beam%minor
    pang  = beam%pang
  end subroutine cubetools_beam_get
  !
  subroutine cubetools_beam_final(beam,error)
    !-------------------------------------------------------------------
    ! Just initialize, ie, beam is intent(out)
    !-------------------------------------------------------------------
    type(beam_t), intent(out)   :: beam
    logical,      intent(inout) :: error
    !
    character(len=*), parameter :: rname='BEAM>FINAL'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
  end subroutine cubetools_beam_final
  !
  !---------------------------------------------------------------------
  !
  subroutine cubetools_beam_list(beam,error)
    use cubetools_format
    use cubetools_unit
    !-------------------------------------------------------------------
    ! *** JP We should address the case where either major or minor or PA
    ! *** JP are set to NaN
    !-------------------------------------------------------------------
    type(beam_t), intent(in)    :: beam
    logical,      intent(inout) :: error
    !
    character(len=mess_l) :: mess
    type(unit_user_t) :: unitbeam,unitpang
    character(len=*), parameter :: rname='BEAM>LIST'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call unitbeam%get_from_code(code_unit_beam,error)
    if (error) return
    call unitpang%get_from_code(code_unit_pang,error)
    if (error) return
    mess = cubetools_format_stdkey_boldval('Major',beam%major*unitbeam%user_per_prog,'f8.3',22)
    mess = trim(mess)//'  '//cubetools_format_stdkey_boldval('Minor',beam%minor*unitbeam%user_per_prog,'f8.3',22)
    mess = trim(mess)//'  '//cubetools_format_stdkey_boldval('PA',beam%pang*unitpang%user_per_prog,'f7.2',10)
    call cubetools_message(seve%r,rname,mess)
  end subroutine cubetools_beam_list
  !
  subroutine cubetools_beam_register(beam,abstract,error)
    use cubetools_unit
    !----------------------------------------------------------------------
    ! Register a /BEAM option under a given name and abstract
    !----------------------------------------------------------------------
    class(beam_opt_t),       intent(out)   :: beam
    character(len=*),        intent(in)    :: abstract
    logical,                 intent(inout) :: error
    !
    type(unit_arg_t) :: unitarg
    type(standard_arg_t) :: stdarg
    !
    character(len=*), parameter :: rname='BEAM>REGISTER'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         'BEAM','major [minor [unit [pang [paunit]]]]',&
         abstract,&
         'If only the major axis is given the beam is assumed&
         & circular and hence minor = major', &
         beam%opt,error)
    if (error) return
    call stdarg%register( &
         'major',  &
         'Beam major axis', &
         '"*" or "=" mean previous value is kept',&
         code_arg_mandatory, &
         error)
    if (error) return
    call stdarg%register( &
         'minor',  &
         'Beam minor axis', &
         '"=" means unchanged, "*" means equal to major axis',&
         code_arg_optional, &
         error)
    if (error) return
    call unitarg%register( &
         'unit',  &
         'Beam axes unit', &
         '"*" or "=" current beam unit is used',&
         code_arg_optional, &
         code_unit_beam, &
         beam%unit_arg,&
         error)
    if (error) return
    call stdarg%register( &
         'pang',  &
         'Beam position angle', &
         '"*" or "=" mean previous value is kept',&
         code_arg_optional, &
         error)
    if (error) return
    call unitarg%register( &
         'paunit',  &
         'Position angle unit', &
         '"*" or "=" current position angle unit is used',&
         code_arg_optional, &
         code_unit_pang, &
         beam%paunit_arg,&
         error)
    if (error) return
  end subroutine cubetools_beam_register
  !
  subroutine cubetools_beam_parse(beam,line,user,error)
    !----------------------------------------------------------------------
    ! Parse beam 
    ! /BEAM major [minor [pang [pangunit]]]
    ! ----------------------------------------------------------------------
    class(beam_opt_t), intent(in)    :: beam
    character(len=*),  intent(in)    :: line
    type(beam_user_t), intent(inout) :: user
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='BEAM>PARSE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    user%major  = strg_star
    user%minor  = strg_star
    user%unit   = strg_star
    user%pang   = strg_star
    user%paunit = strg_star
    !
    call beam%opt%present(line,user%do,error)
    if (error) return
    if (user%do) then
       call cubetools_getarg(line,beam%opt,1,user%major,mandatory,error)
       if (error) return
       call cubetools_getarg(line,beam%opt,2,user%minor,.not.mandatory,error)
       if (error) return
       call cubetools_getarg(line,beam%opt,3,user%unit,.not.mandatory,error)
       if (error) return
       call cubetools_getarg(line,beam%opt,4,user%pang,.not.mandatory,error)
       if (error) return
       call cubetools_getarg(line,beam%opt,5,user%paunit,.not.mandatory,error)
       if (error) return
    endif
  end subroutine cubetools_beam_parse
  !
  subroutine cubetools_beam_user2prog(beam,user,prog,error)
    use cubetools_unit
    use cubetools_user2prog
    use cubetools_nan
    !----------------------------------------------------------------------
    ! Resolves a beam description based on a previous one according to
    ! user inputs. Assumes that beam is the current beam unit
    !----------------------------------------------------------------------
    class(beam_opt_t), intent(in)    :: beam
    type(beam_user_t), intent(in)    :: user
    type(beam_t),      intent(inout) :: prog
    logical,           intent(inout) :: error
    !
    type(unit_user_t) :: unitbeam, unitpang
    real(kind=beam_k) :: default,previous
    character(len=*), parameter :: rname='BEAM>USER2PROG'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (user%do) then
       call unitbeam%get_from_name_for_code(user%unit,code_unit_beam,error)
       if (error) return
       call unitpang%get_from_name_for_code(user%paunit,code_unit_pang,error)
       if (error) return
       !
       default = prog%major
       previous = prog%major
       call cubetools_user2prog_resolve_all(user%major,unitbeam,default,previous,prog%major,error)
       if (error) return
       default = prog%major
       previous = prog%minor
       call cubetools_user2prog_resolve_all(user%minor,unitbeam,default,previous,prog%minor,error)
       if (error) return
       default = 0.0
       previous = prog%pang
       call cubetools_user2prog_resolve_all(user%pang,unitpang,default,previous,prog%pang,error)
       if (error) return
       !
       if (prog%minor.gt.prog%major) then
          call cubetools_message(seve%e,rname,'Minor axis must be smaller than major axis')
          error = .true.
          return
       endif
    else ! .not.user%do
       continue
    endif
  end subroutine cubetools_beam_user2prog
  !
  subroutine cubetools_beam_prog2user(prog,user,error)
    use cubetools_unit
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    type(beam_t),      intent(in)    :: prog
    type(beam_user_t), intent(out)   :: user
    logical,           intent(inout) :: error
    !
    type(unit_user_t) :: unitbeam, unitpang
    character(len=*), parameter :: rname='BEAM>PROG2USER'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call unitbeam%get_from_code(code_unit_beam,error)
    if (error) return
    call unitpang%get_from_code(code_unit_pang,error)
    if (error) return
    !
    write(user%major,'(1pg14.7)') prog%major*unitbeam%user_per_prog
    write(user%minor,'(1pg14.7)') prog%minor*unitbeam%user_per_prog
    write(user%pang,'(1pg14.7)') prog%pang*unitpang%user_per_prog
    !
    user%unit = unitbeam%name
    user%paunit = unitpang%name
  end subroutine cubetools_beam_prog2user
  !
  subroutine cubetools_beam_sicdef_user(name,user,readonly,error)
    use gkernel_interfaces
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    character(len=*),  intent(in)    :: name
    type(beam_user_t), intent(in)    :: user
    logical,           intent(in)    :: readonly
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='BEAM>SICDEF>USER'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call sic_defstructure(name,global,error)
    if (error) return
    call sic_def_charn(trim(name)//'%major',user%major,0,0,readonly,error)
    if (error) return
    call sic_def_charn(trim(name)//'%minor',user%minor,0,0,readonly,error)
    if (error) return
    call sic_def_charn(trim(name)//'%pang',user%pang,0,0,readonly,error)
    if (error) return
    call sic_def_charn(trim(name)//'%unit',user%unit,0,0,readonly,error)
    if (error) return
    call sic_def_charn(trim(name)//'%paunit',user%paunit,0,0,readonly,error)
    if (error) return
  end subroutine cubetools_beam_sicdef_user
  !
  subroutine cubetools_beam_beam2userstruct(prog,userstruct,error)
    use cubetools_userspace
    use cubetools_userstruct
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    type(beam_t),       intent(in)    :: prog
    type(userstruct_t), intent(inout) :: userstruct
    logical,            intent(inout) :: error
    !
    type(beam_user_t) :: user
    character(len=*), parameter :: rname='BEAM>BEAM2USERSTRUCT'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_beam_prog2user(prog,user,error)
    if (error) return
    call userstruct%def(error)
    if (error) return
    call userstruct%set_member('major',user%major,error)
    if (error) return
    call userstruct%set_member('minor',user%minor,error)
    if (error) return
    call userstruct%set_member('pang',user%pang,error)
    if (error) return
    call userstruct%set_member('unit',user%unit,error)
    if (error) return
    call userstruct%set_member('paunit',user%paunit,error)
    if (error) return
  end subroutine cubetools_beam_beam2userstruct
  !
  !------------------------------------------------------------------------
  !
  subroutine cubetools_beam_sicdef(name,beam,readonly,error)
    use gkernel_interfaces
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: name
    type(beam_t),     intent(in)    :: beam
    logical,          intent(in)    :: readonly
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='BEAM>SICDEF'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call sic_defstructure(name,global,error)
    if (error) return
    call sic_def_real(trim(name)//'%major',beam%major,0,0,readonly,error)
    if (error) return
    call sic_def_real(trim(name)//'%minor',beam%minor,0,0,readonly,error)
    if (error) return
    call sic_def_real(trim(name)//'%pang',beam%pang,0,0,readonly,error)
    if (error) return
  end subroutine cubetools_beam_sicdef
  !
  subroutine cubetools_beam_copy(in,ou,error)
    !----------------------------------------------------------------------
    !
    ! ----------------------------------------------------------------------
    type(beam_t), intent(in)    :: in
    type(beam_t), intent(out)   :: ou
    logical,      intent(inout) :: error
    !
    character(len=*), parameter :: rname='BEAM>COPY'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    ou = in
  end subroutine cubetools_beam_copy
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_beam_consistency_init(cons,error)
    !-------------------------------------------------------------------
    ! Init the consistency between two beams
    !-------------------------------------------------------------------
    type(beam_cons_t)       , intent(out)   :: cons
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='BEAM>CONSISTENCY>INIT'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_consistency_init(tenper,check,mess,cons%major,error)
    if (error) return
    call cubetools_consistency_init(tenper,check,mess,cons%minor,error)
    if (error) return
    call cubetools_consistency_init(tenper,check,mess,cons%pang,error)
    if (error) return
  end subroutine cubetools_beam_consistency_init
  !
  subroutine cubetools_beam_consistency_set_tol(beatol,cons,error)
    !-------------------------------------------------------------------
    ! Set the tolerance for beam consistency check
    !-------------------------------------------------------------------
    real(kind=tole_k), intent(in)    :: beatol
    type(beam_cons_t), intent(inout) :: cons
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='BEAM>CONSISTENCY>SET>TOL'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    cons%major%tol = beatol
    cons%minor%tol = beatol
    cons%pang%tol  = beatol
  end subroutine cubetools_beam_consistency_set_tol
  !
  subroutine cubetools_beam_consistency_check(cons,beam1,beam2,error)
    !-------------------------------------------------------------------
    ! Check the consistency between two beams
    !-------------------------------------------------------------------
    type(beam_cons_t), intent(inout) :: cons
    type(beam_t),      intent(in)    :: beam1
    type(beam_t),      intent(in)    :: beam2
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='BEAM>CONSISTENCY>CHECK'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (.not.cons%check) return
    !
    cons%prob = .false.
    call cubetools_consistency_real_check(cons%major,beam1%major,beam2%major,error)
    if (error) return
    call cubetools_consistency_real_check(cons%minor,beam1%minor,beam2%minor,error)
    if (error) return
    call cubetools_consistency_angle_check(cons%pang,beam1%pang,beam2%pang,error)
    if (error) return
    cons%prob = cons%major%prob.or.cons%minor%prob.or.cons%pang%prob
  end subroutine cubetools_beam_consistency_check
  !
  subroutine cubetools_beam_consistency_list(cons,beam1,beam2,error)
    use cubetools_unit
    !-------------------------------------------------------------------
    ! Check the consistency between two beams
    !-------------------------------------------------------------------
    type(beam_cons_t), intent(in)    :: cons
    type(beam_t),      intent(in)    :: beam1
    type(beam_t),      intent(in)    :: beam2
    logical,           intent(inout) :: error
    !
    type(unit_user_t) :: unitbeam,unitpang
    character(len=*), parameter :: rname='BEAM>CONSISTENCY>LIST'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (.not.cons%mess) return
    !
    call cubetools_consistency_title('beams',3,cons%check,cons%prob,error)
    if (error) return
    if (cons%check.and.cons%prob) then
       call unitbeam%get_from_code(code_unit_beam,error)
       if (error) return
       call unitpang%get_from_code(code_unit_pang,error)
       if (error) return
       call cubetools_consistency_real_print('Major FWHMs',unitbeam%name,cons%major,&
            beam1%major*unitbeam%user_per_prog,beam2%major*unitbeam%user_per_prog,error)
       if (error) return
       call cubetools_consistency_real_print('Minor FWHMs',unitbeam%name,cons%minor,&
            beam1%minor*unitbeam%user_per_prog,beam2%minor*unitbeam%user_per_prog,error)
       if (error) return 
       call cubetools_consistency_real_print('Pos. angles',unitpang%name,cons%pang,&
            beam1%pang*unitpang%user_per_prog,beam2%pang*unitpang%user_per_prog,error)
       if (error) return
    endif
    call cubetools_message(seve%r,rname,'')
  end subroutine cubetools_beam_consistency_list
  !
  subroutine cubetools_beam_consistency_final(cons,error)
    !-------------------------------------------------------------------
    ! Final the consistency between two beams
    !-------------------------------------------------------------------
    type(beam_cons_t)       , intent(out)   :: cons
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='BEAM>CONSISTENCY>FINAL'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_consistency_final(cons%major,error)
    if (error) return
    call cubetools_consistency_final(cons%minor,error)
    if (error) return
    call cubetools_consistency_final(cons%pang,error)
    if (error) return
  end subroutine cubetools_beam_consistency_final
  !
  !---------------------------------------------------------------------
  !
  subroutine cubetools_beam_scale_with_frequency(oldfreq,newfreq,beam,error)
    !-------------------------------------------------------------------
    ! Scale the resolution according to the reference frequency
    ! *** JP => This makes me think that the reference frequency could be
    ! *** JP    part of the definition of the beam_t type but it would
    ! *** JP    additional introduce redundancy
    !-------------------------------------------------------------------
    real(kind=coor_k), intent(in)    :: oldfreq
    real(kind=coor_k), intent(in)    :: newfreq
    type(beam_t),      intent(inout) :: beam
    logical,           intent(inout) :: error
    !
    real(kind=coor_k) :: factor
    character(len=*), parameter :: rname='BEAM>SCALE>WITH>FREQUENCY'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (oldfreq.eq.newfreq)  return
    !
    factor = oldfreq/newfreq
    beam%major = beam%major*factor
    beam%minor = beam%minor*factor
  end subroutine cubetools_beam_scale_with_frequency
  !
  subroutine cubetools_beam_to_table(beam,restfreq,freq,table,error)
    !-------------------------------------------------------------------
    ! Produce a beam table by scalling beam from the rest frequency
    ! beam to the given frequencies
    !-------------------------------------------------------------------
    type(beam_t),          intent(in)    :: beam      ! input beam object
    real(kind=coor_k),     intent(in)    :: restfreq  ! Original rest frequency
    real(kind=coor_k),     intent(in)    :: freq(:)   ! Output frequencies
    type(beam_table_t),    intent(out)   :: table     ! Output beam table
    logical,               intent(inout) :: error
    !
    integer(kind=chan_k) :: ichan,nchan
    character(len=*), parameter :: rname='BEAM>TO>TABLE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    nchan = size(freq)
    call cubeetools_beam_table_reallocate(table,nchan,error)
    if (error) return
    !
    do ichan=1,nchan
       table%beam(ichan) = beam
       call cubetools_beam_scale_with_frequency(restfreq,freq(ichan),table%beam(ichan),error)
       if (error) return
    enddo
  end subroutine cubetools_beam_to_table
  !
  subroutine cubeetools_beam_table_reallocate(table,nb,error)
    use gkernel_interfaces
    !------------------------------------------------------------------------
    ! (Re)-allocates a beam table
    !------------------------------------------------------------------------
    class(beam_table_t),  intent(out)   :: table
    integer(kind=chan_k), intent(in)    :: nb
    logical,              intent(inout) :: error
    !
    logical :: alloc
    integer(kind=4) :: ier
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='BEAM>TABLE>REALLOCATE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (nb.le.0) then
       call cubetools_message(seve%e,rname,'Negative or zero number of beams')
       error = .true.
       return
    endif
    !
    if (allocated(table%beam)) then
       if (nb.eq.size(table%beam)) then
          write(mess,'(a,i0)') 'table already allocated at the right size: ',nb
          call cubetools_message(toolseve%alloc,rname,mess)
          alloc = .false.
       else
          call cubetools_message(toolseve%alloc,rname,&
               'table already allocated but with a different size, deallocating first')
          alloc = .true.
          deallocate(table%beam)
       endif
    else
       alloc = .true.
    endif
    !
    if (alloc) then
       allocate(table%beam(nb),stat=ier)
       if (failed_allocate(rname,'BEAM table',ier,error)) return
    endif
    table%nb = nb
  end subroutine cubeetools_beam_table_reallocate
end module cubetools_beam_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
