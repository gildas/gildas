###########################################################################
#
# Makefile system for GILDAS softwares (2003-2023).
#
# Please be careful: element order often matters in makefiles.
#
###########################################################################

include $(gagadmdir)/Makefile.def

###########################################################################

CONSISTENCY_OBJECTS = consistency-colors.o consistency-methods.o	\
type-consistency.o

CONSISTENCY_EXPORTS = cubetools_consistency_colors.mod			\
cubetools_consistency_methods.mod cubetools_consistency_types.mod

###########################################################################

HEADER_OBJECTS = header.o header-array.o header-array-element.o		\
header-array-noise.o header-array-shape.o header-axis.o header-axset.o	\
header-interface.o header-methods.o header-observatory.o		\
header-observatory-telescope.o header-spatial.o header-spatial-beam.o	\
header-spatial-element.o header-spatial-frame.o				\
header-spatial-projection.o header-spectral.o header-spectral-element.o	\
header-vo.o

HEADER_EXPORTS = cubetools_arrelt_types.mod cubetools_axis_types.mod		\
cubetools_axset_types.mod cubetools_beam_types.mod				\
cubetools_brightness.mod cubetools_header_array_types.mod			\
cubetools_header_interface.mod cubetools_header_types.mod			\
cubetools_header_methods.mod cubetools_header_vo.mod				\
cubetools_noise_types.mod cubetools_observatory_types.mod			\
cubetools_obstel_consistency_types.mod cubetools_obstel_prog_types.mod		\
cubetools_shape_types.mod cubetools_spaelt_types.mod				\
cubetools_spafra_types.mod cubetools_spapro_types.mod				\
cubetools_spatial_types.mod cubetools_spectral_prog_types.mod			\
cubetools_spectral_consistency_types.mod					\
cubetools_spectral_frame_types.mod cubetools_spectral_velocity_types.mod	\
cubetools_spectral_redshift_types.mod cubetools_spectral_line_types.mod		\
cubetools_spectral_freq_types.mod cubetools_spectral_wave_types.mod		\
cubetools_speelt_types.mod

###########################################################################

OTHER_OBJECTS = type-extrema.o type-processing.o 

OTHER_EXPORTS = cubetools_extrema_types.mod cubetools_processing_types.mod 

###########################################################################

LIB_IDENTITY = cubeheader

LIB_F_OBJECTS = $(CONSISTENCY_OBJECTS) $(HEADER_OBJECTS) $(OTHER_OBJECTS) 

LIB_EXPORTS = $(CONSISTENCY_EXPORTS) $(HEADER_EXPORTS) $(OTHER_EXPORTS) 

INTERFACES_DISABLE = yes

LIB_DEPENDS = -lcubesyntax -lcubetools $(SIC_LIBS)

###########################################################################

include $(gagadmdir)/Makefile.lib

###########################################################################

include $(builddir)/Makefile.deps

###########################################################################
