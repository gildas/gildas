!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_axset_types
  use cubetools_parameters
  use cubetools_messaging
  use cubetools_axis_types
  use cubetools_consistency_types
  !
  public :: axset_t,axset_cons_t
  public :: cubetools_axset_init,cubetools_axset_put_and_derive,cubetools_axset_rederive
  public :: cubetools_axset_final,cubetools_axset_get
  public :: cubetools_axset_ignore_degenerate
  public :: cubetools_axset_list
  public :: cubetools_axset_sicdef,cubetools_axset_copy
  public :: cubetools_axset_consistency_init,cubetools_axset_consistency_final
  public :: cubetools_axset_consistency_check,cubetools_axset_consistency_list
  public :: cubetools_axset_count_genuine,cubetools_axset_val2zero
  !
  ! /AXIS type, register, parse and user2prog
  public :: axset_axis_user_t
  public :: cubetools_axset_axis_register,cubetools_axset_axis_parse,cubetools_axset_axis_user2prog
  ! /AXNAMES type, register, parse and user2prog
  public :: axset_axnames_user_t
  public :: cubetools_axset_axnames_register,cubetools_axset_axnames_parse,cubetools_axset_axnames_user2prog
  public :: cubetools_axset_set_ignore_degenerate,cubetools_axset_get_ignore_degenerate
  !
  private
  !
  type axset_t
     integer(kind=ndim_k) :: m = maxdim        ! Maximum number of axes
     integer(kind=ndim_k) :: n = 0             ! Actual number of axes
     type(axis_t)         :: axis(maxdim)      ! Description of up to maxdim axes
     integer(kind=ndim_k) :: il = code_unk     ! 1st coordinate axis
     integer(kind=ndim_k) :: im = code_unk     ! 2nd coordinate axis
     integer(kind=ndim_k) :: ic = code_unk     ! Spectral axis
  end type axset_t
  !
  type axset_axnames_user_t
     integer(kind=code_k)               :: iopt  = code_abs ! Option identifier
     character(len=argu_l), allocatable :: names(:)         ! Axis name
     integer(kind=ndim_k)               :: n                ! User axis description
     logical                            :: do    = .false.  ! Option was present
  end type axset_axnames_user_t
  !
  type axset_axis_user_t
     character(len=argu_l) :: name = strg_unk ! Axis name
     character(len=unit_l) :: kind            ! Axis kind
     type(axis_user_t)     :: axis            ! User axis description
     logical               :: do = .false.    ! Option was present
  end type axset_axis_user_t
  !
  type axset_cons_t
     logical                  :: check=.true.  ! Check the section
     logical                  :: prob =.false. ! Is there a problem
     logical                  :: mess =.true.  ! Output message for this section?
     type(consistency_desc_t) :: m             ! Maximum number of axes
     type(consistency_desc_t) :: n             ! Actual number of axes
     type(axis_cons_t)        :: axis(maxdim)  ! Description of up to maxdim axes
     type(consistency_desc_t) :: il            ! 1st coordinate axis
     type(consistency_desc_t) :: im            ! 2nd coordinate axis
     type(consistency_desc_t) :: ic            ! Spectral axis
  end type axset_cons_t
  !
  logical :: ignore_degenerate=.true.  ! Support for SET\AXSET /IGNOREDEGENERATE YES|NO
  !
contains
  !
  subroutine cubetools_axset_init(set,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    type(axset_t), intent(inout) :: set
    logical,       intent(inout) :: error
    !
    integer(kind=ndim_k) :: iaxis
    character(len=*), parameter :: rname='AXSET>INIT'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    do iaxis = 1,set%m
       call cubetools_axis_init(set%axis(iaxis),error)
       if (error) return
    enddo ! iaxis
  end subroutine cubetools_axset_init
  !
  subroutine cubetools_axset_put_and_derive(genuine,name,unit,kind,&
       ndim,dim,conv,il,im,ic,set,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    logical,              intent(in)    :: genuine(:)
    character(len=*),     intent(in)    :: name(:)
    character(len=*),     intent(in)    :: unit(:)
    integer(kind=code_k), intent(in)    :: kind(:)
    integer(kind=ndim_k), intent(in)    :: ndim
    integer(kind=data_k), intent(in)    :: dim(:)
    real(kind=coor_k),    intent(in)    :: conv(:,:)
    integer(kind=ndim_k), intent(in)    :: il,im,ic
    type(axset_t),        intent(inout) :: set
    logical,              intent(inout) :: error
    !
    integer(kind=ndim_k) :: iaxis
    character(len=*), parameter :: rname='AXSET>PUT>AND>DERIVE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (ndim.gt.set%m) then
       error = .true.
       return
    endif
    !
    set%n = ndim
    do iaxis = 1,set%m
       call cubetools_axis_put_and_derive(&
            genuine(iaxis),&
            name(iaxis),&
            unit(iaxis),&
            kind(iaxis),&
            dim(iaxis), &
            conv(:,iaxis),&
            set%axis(iaxis),&
            error)
       if (error) return
    enddo ! iaxis
    set%il = il
    set%im = im
    set%ic = ic
    !
    if (ignore_degenerate) then
      call cubetools_axset_ignore_degenerate(set,error)
      if (error) return
    endif
  end subroutine cubetools_axset_put_and_derive
  !
  subroutine cubetools_axset_get(set,genuine,name,unit,kind,&
       ndim,dim,conv,il,im,ic,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    type(axset_t),        intent(in)    :: set
    logical,              intent(out)   :: genuine(:)
    character(len=*),     intent(out)   :: name(:)
    character(len=*),     intent(out)   :: unit(:)
    integer(kind=code_k), intent(out)   :: kind(:)
    integer(kind=ndim_k), intent(out)   :: ndim
    integer(kind=data_k), intent(out)   :: dim(:)
    real(kind=coor_k),    intent(out)   :: conv(:,:)
    integer(kind=ndim_k), intent(out)   :: il,im,ic
    logical,              intent(inout) :: error
    !
    integer(kind=ndim_k) :: iaxis
    character(len=*), parameter :: rname='AXSET>GET'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (size(dim).lt.set%n) then
       error = .true.
       return
    endif
    !
    ndim = set%n
    do iaxis = 1,set%m
       call cubetools_axis_get(&
            set%axis(iaxis),&
            genuine(iaxis),&
            name(iaxis),&
            unit(iaxis),&
            kind(iaxis),&
            dim(iaxis), &
            conv(:,iaxis),&
            error)
       if (error) return
    enddo ! iaxis
    il = set%il
    im = set%im
    ic = set%ic
  end subroutine cubetools_axset_get
  !
  subroutine cubetools_axset_rederive(set,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    type(axset_t), intent(inout) :: set
    logical,       intent(inout) :: error
    !
    integer(kind=ndim_k) :: iaxis
    character(len=*), parameter :: rname='AXSET>REDERIVE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    do iaxis = 1,set%m
       call cubetools_axis_rederive(set%axis(iaxis),error)
       if (error) return
    enddo ! iaxis
  end subroutine cubetools_axset_rederive
  !
  subroutine cubetools_axset_ignore_degenerate(set,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    type(axset_t), intent(inout) :: set
    logical,       intent(inout) :: error
    !
    integer(kind=ndim_k) :: iaxis
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='AXSET>IGNORE>DEGENERATE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    do iaxis = 1,set%m
       ! L, M and C axes can be valid axes and have size 1, they are
       ! hence not ignored
       if (iaxis.eq.set%ic .or. iaxis.eq.set%il .or. iaxis.eq.set%im) cycle
       if (set%axis(iaxis)%n.eq.1) then
          set%axis(iaxis)%genuine = .false.
          write(mess,'(a,i0,a)') 'Degenerate axis #',iaxis,' is neutralized'
          call cubetools_message(seve%w,rname,mess)
       endif
    enddo ! iaxis
  end subroutine cubetools_axset_ignore_degenerate
  !
  subroutine cubetools_axset_get_l(set,laxis,error)
    !-------------------------------------------------------------------
    ! Get L axis from the axset 
    !-------------------------------------------------------------------
    type(axset_t), intent(in)    :: set
    type(axis_t),  intent(out)   :: laxis
    logical,       intent(inout) :: error
    !
    character(len=*), parameter :: rname='AXSET>GET>L'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_axis_copy(set%axis(set%il),laxis,error)
    if (error) return
  end subroutine cubetools_axset_get_l
  !
  subroutine cubetools_axset_put_l(laxis,set,error)
    !-------------------------------------------------------------------
    ! Put L axis from the axset 
    !-------------------------------------------------------------------
    type(axis_t),  intent(in)    :: laxis
    type(axset_t), intent(inout) :: set
    logical,       intent(inout) :: error
    !
    character(len=*), parameter :: rname='AXSET>PUT>L'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_axis_copy(laxis,set%axis(set%il),error)
    if (error) return
  end subroutine cubetools_axset_put_l
  !
  subroutine cubetools_axset_get_m(set,maxis,error)
    !-------------------------------------------------------------------
    ! Get M axis from the axset 
    !-------------------------------------------------------------------
    type(axset_t), intent(in)    :: set
    type(axis_t),  intent(out)   :: maxis
    logical,       intent(inout) :: error
    !
    character(len=*), parameter :: rname='AXSET>GET>M'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_axis_copy(set%axis(set%im),maxis,error)
    if (error) return
  end subroutine cubetools_axset_get_m
  !
  subroutine cubetools_axset_put_m(maxis,set,error)
    !-------------------------------------------------------------------
    ! Put M axis from the axset 
    !-------------------------------------------------------------------
    type(axis_t),  intent(in)    :: maxis
    type(axset_t), intent(inout) :: set
    logical,       intent(inout) :: error
    !
    character(len=*), parameter :: rname='AXSET>PUT>M'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_axis_copy(maxis,set%axis(set%im),error)
    if (error) return
  end subroutine cubetools_axset_put_m
  !
  subroutine cubetools_axset_get_c(set,caxis,error)
    !-------------------------------------------------------------------
    ! Get C axis from the axset 
    !-------------------------------------------------------------------
    type(axset_t), intent(in)    :: set
    type(axis_t),  intent(out)   :: caxis
    logical,       intent(inout) :: error
    !
    character(len=*), parameter :: rname='AXSET>GET>C'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_axis_copy(set%axis(set%ic),caxis,error)
    if (error) return
  end subroutine cubetools_axset_get_c
  !
  subroutine cubetools_axset_put_c(caxis,set,error)
    !-------------------------------------------------------------------
    ! Put C axis from the axset 
    !-------------------------------------------------------------------
    type(axis_t),  intent(in)    :: caxis
    type(axset_t), intent(inout) :: set
    logical,       intent(inout) :: error
    !
    character(len=*), parameter :: rname='AXSET>PUT>C'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_axis_copy(caxis,set%axis(set%ic),error)
    if (error) return
  end subroutine cubetools_axset_put_c
  !
  subroutine cubetools_axset_final(set,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    type(axset_t), intent(inout) :: set
    logical,       intent(inout) :: error
    !
    integer(kind=ndim_k) :: iaxis
    character(len=*), parameter :: rname='AXSET>FINAL'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    set%n = 0
    do iaxis = 1,set%m
       call cubetools_axis_reinitialize(set%axis(iaxis),error)
       if (error) return
    enddo ! iaxis
    set%il = code_unk
    set%im = code_unk
    set%ic = code_unk
  end subroutine cubetools_axset_final
  !
  !---------------------------------------------------------------------
  !
  subroutine cubetools_axset_axis_register(name,abstract,option,kindarg,error)
    use cubetools_structure
    use cubetools_unit_setup
    use cubetools_unit_arg
    use cubetools_keywordlist_types
    !----------------------------------------------------------------------
    ! Register a /AXIS option that requires an axis name
    !----------------------------------------------------------------------
    character(len=*),             intent(in)    :: name
    character(len=*),             intent(in)    :: abstract
    type(option_t), pointer,      intent(out)   :: option
    type(keywordlist_comm_t), pointer, intent(out)   :: kindarg
    logical,                      intent(inout) :: error
    !
    type(standard_arg_t) :: stdarg
    type(keywordlist_comm_t)  :: keyarg
    type(unit_arg_t), pointer :: unitarg
    !
    character(len=*), parameter :: rname='AXIS>REGISTER>WITHNAME'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         name,'name n ref val inc [unit [kind]]',&
         abstract,&
         strg_id,&
         option,error)
    if (error) return
    call stdarg%register( &
         'name',  &
         'Axis name', &
         strg_id,&
         code_arg_mandatory, &
         error)
    if (error) return
    call cubetools_axis_register_sub(code_unit_unk,unitarg,error)
    if (error) return
    call keyarg%register( &
         'kind',  &
         'Axis unit kind', &
         strg_id,&
         code_arg_optional, &
         unitkinds(1:nunitkinds),  &
         .not.flexible,  &
         kindarg,  &
         error)
    if (error) return
  end subroutine cubetools_axset_axis_register
  !
  subroutine cubetools_axset_axis_parse(line,opt,user,error)
    use cubetools_structure
    !----------------------------------------------------------------------
    ! Parse an axis description, and its name. Parsing active only if
    ! iopt code is not absent
    !
    ! /AXIS name n ref val inc [unit]
    ! ----------------------------------------------------------------------
    character(len=*),        intent(in)    :: line
    type(option_t),          intent(in)    :: opt
    type(axset_axis_user_t), intent(out)   :: user
    logical,                 intent(out)   :: error
    !
    integer(kind=4), parameter :: istart = 2
    character(len=*), parameter :: rname='AXSET>AXIS>PARSE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call opt%present(line,user%do,error)
    if (error) return
    if (user%do) then
       call cubetools_getarg(line,opt,1,user%name,mandatory,error)
       if (error) return
       call cubetools_axis_parse_sub(line,opt,istart,user%axis,error)
       if (error) return
       user%axis%reallocate = .false.
       user%kind = strg_star
       call cubetools_getarg(line,opt,7,user%kind,.not.mandatory,error)
       if (error) return
    endif
  end subroutine cubetools_axset_axis_parse
  !
  subroutine cubetools_axset_axis_user2prog(user,prog,error)
    use cubetools_disambiguate
    use cubetools_unit_setup
    !----------------------------------------------------------------------
    ! Resolve an axis description and matches it to the corresponding
    ! header axis based on its name
    !----------------------------------------------------------------------
    type(axset_axis_user_t), intent(in)    :: user
    type(axset_t),           intent(inout) :: prog
    logical,                 intent(out)   :: error
    !
    integer(kind=4) :: iaxis,code_kind
    character(len=unit_l) :: names(maxdim),name,kind
    character(len=*), parameter :: rname='AXSET>AXIS>USER2PROG'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (user%do) then
       do iaxis=1,maxdim
          names(iaxis) = prog%axis(iaxis)%name
       enddo ! iaxis
       !
       ! Desambiguization of the axis name
       call cubetools_disambiguate_flexible(user%name,names,iaxis,name,error)
       if (error) return
       if (name.eq.strg_unresolved) then
          call cubetools_message(seve%e,rname,'Unknown axis '//user%name)
          error = .true.
          return
       endif
       !
       if (user%kind.eq.strg_star) then ! Let CUBE choose axis kind
          select case (user%axis%unit)
          case(strg_star) ! Complicated case Unit is also undetermined
             if (iaxis.eq.prog%il.or.iaxis.eq.prog%im) then ! Axis is il or im, unit kind is FOV
                code_kind = code_unit_fov
             else if (iaxis.eq.prog%ic) then ! Axis is spectral axis
                call cubetools_message(seve%w,rname,'Assuming spectral axis to be a velocity axis')
                code_kind = code_unit_velo
             else ! Axis is neither spectral nor spatial
                call cubetools_message(seve%w,rname,trim(name)//&
                     ' axis is not spatial or spectral, unit kind unchanged')
                code_kind = prog%axis(iaxis)%kind
             endif
          case(strg_equal) ! Unit is unchanged hence Axis kind is unchanged, easy
             code_kind = prog%axis(iaxis)%kind
          case default ! Unit has been changed, try to get axis kind from unit
             call cubetools_unit_get_code(user%axis%unit,code_kind,error)
             if (error) return
             ! Here we make an assumption that fov makes sense while beam does not
             if (code_kind.eq.code_unit_beam) code_kind = code_unit_fov
          end select
       else ! Simple case user has chosen axis kind
          call cubetools_disambiguate_strict(user%kind,unitkinds(1:nunitkinds),code_kind,kind,error)
          if (error) return
       endif
       prog%axis(iaxis)%kind = code_kind
       !
       call cubetools_axis_user2prog_sub(user%axis,prog%axis(iaxis),prog%axis(iaxis),error)
       if (error) return
       call cubetools_axset_val2zero(prog,error)
       if (error) return
    endif
  end subroutine cubetools_axset_axis_user2prog
  !
  !---------------------------------------------------------------------
  !
  subroutine cubetools_axset_axnames_register(name,abstract,option,error)
    use cubetools_structure
    !----------------------------------------------------------------------
    ! Register a /AXNAMES option under a given name, abstract and help
    ! into the option
    !----------------------------------------------------------------------
    character(len=*),        intent(in)    :: name
    character(len=*),        intent(in)    :: abstract
    type(option_t), pointer, intent(out)   :: option
    logical,                 intent(inout) :: error
    !
    type(standard_arg_t) :: stdarg
    !
    character(len=*), parameter :: rname='AXSET>AXNAMES>REGISTER>AXNAMES'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         name,'ax1 ... axn',&
         abstract,&
         'The total number of arguments should match the number of&
         & axes in the cube, as each argument corresponds to one of&
         & the axes in the cube', option,error)
    if (error) return
    call stdarg%register(&
         'ax1',&
         'Name for first axis',&
         '"*" or "=" mean previous value is kept',&
         code_arg_mandatory,&
         error)
    if (error) return
    call stdarg%register(&
         'axn',&
         '"*" or "=" mean previous value is kept',&
         strg_id,&
         code_arg_unlimited,&
         error)
    if (error) return
  end subroutine cubetools_axset_axnames_register
  !
  subroutine cubetools_axset_axnames_parse(line,opt,user,error)
    use gkernel_interfaces
    use cubetools_structure
    !----------------------------------------------------------------------
    ! Parse axes names
    ! /AXNAMES ax1 ax2 ... axn
    ! ----------------------------------------------------------------------
    character(len=*),           intent(in)    :: line
    type(option_t),             intent(in)    :: opt
    type(axset_axnames_user_t), intent(out)   :: user
    logical,                    intent(out)   :: error
    !
    integer(kind=4) :: narg,ier
    integer(kind=ndim_k) :: iax
    character(len=*), parameter :: rname='AXSET>AXNAMES>PARSE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call opt%present(line,user%do,error)
    if (error) return
    if (user%do) then
       narg = opt%getnarg()
       allocate(user%names(narg),stat=ier)
       if (failed_allocate(rname,'Axes names',ier,error)) return
       do iax=1,narg
          call cubetools_getarg(line,opt,iax,user%names(iax),mandatory,error)
          if (error) return
       enddo ! iax
       user%n = narg
    endif
  end subroutine cubetools_axset_axnames_parse
  !
  subroutine cubetools_axset_axnames_user2prog(user,prog,error)
    use cubetools_user2prog
    !----------------------------------------------------------------------
    ! Resolve axes names, and see if the given description matches
    ! with the current number of axes
    ! ----------------------------------------------------------------------
    type(axset_axnames_user_t), intent(in)    :: user
    type(axset_t),              intent(inout) :: prog
    logical,                    intent(out)   :: error
    !
    integer(kind=ndim_k) :: iax
    character(len=mess_l) :: mess
    character(len=unit_l) :: default, previous
    character(len=*), parameter :: rname='AXSET>AXNAMES>USER2PROG'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (user%do) then
       if (user%n.ne.prog%n) then
          write (mess,'(2(a,i0))') 'Number of axes given ',user%n,&
               ' is different from number of axes in cube ',prog%n 
          call cubetools_message(seve%e,rname,mess)
          error = .true.
          return
       endif
       do iax=1,user%n
          default = prog%axis(iax)%name
          previous = prog%axis(iax)%name
          call cubetools_user2prog_resolve_all(user%names(iax),default,previous,prog%axis(iax)%name,error)
          if (error) return
       enddo ! iax
    endif
  end subroutine cubetools_axset_axnames_user2prog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_axset_list(set,error)
    use gbl_format
    use cubetools_format
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(axset_t), intent(in)    :: set
    logical,       intent(inout) :: error
    !
    integer(kind=4) :: nchar
    character(len=8) :: format
    integer(kind=ndim_k) :: iaxis
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='AXSET>LIST'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    format = fsimple ! *** JP: It should be a user customization parameter
    nchar = nsimple ! *** JP: It should be a user customization parameter
    mess = cubetools_format_stdkey_boldval('Name','',12)
    mess = trim(mess)//cubetools_format_stdkey_boldval('Shape','',6)
    mess = trim(mess)//' '//cubetools_format_stdkey_boldval('Reference','',nchar)
    mess = trim(mess)//' '//cubetools_format_stdkey_boldval('Value','',nchar)
    mess = trim(mess)//' '//cubetools_format_stdkey_boldval('Increment','',nchar)
    mess = trim(mess)//' '//cubetools_format_stdkey_boldval('Unit','',12)
    call cubetools_message(seve%r,rname,mess)
    ! Only list existing axis, whether they are genuine or not => from 1 to set%n
    do iaxis = 1,set%n
       call cubetools_axis_list(set%axis(iaxis),format,nchar,error)
       if (error) return
    enddo ! iaxis
    call cubetools_message(seve%r,rname,' ')
  end subroutine cubetools_axset_list
  !
  subroutine cubetools_axset_sicdef(name,set,readonly,error)
    use gkernel_interfaces
    !-------------------------------------------------------------------
    ! Define the associated SIC structure
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: name
    type(axset_t),    intent(in)    :: set
    logical,          intent(in)    :: readonly
    logical,          intent(inout) :: error
    !
    character(len=varn_l) :: varname
    integer(kind=ndim_k) :: iaxis
    character(len=*), parameter :: rname='AXSET>SICDEF'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call sic_defstructure(trim(name),global,error)
    if (error) return
    call sic_def_inte(trim(name)//'%N',set%n,0,0,readonly,error)
    if (error) return
    do iaxis = 1,set%n
       write(varname,'(a,i0)') trim(name)//'%AXIS',iaxis
       call cubetools_axis_sicdef(varname,set%axis(iaxis),readonly,error)
       if (error) return
    enddo ! iaxis
    call sic_def_inte(trim(name)//'%IL',set%il,0,0,readonly,error)
    if (error) return
    call sic_def_inte(trim(name)//'%IM',set%im,0,0,readonly,error)
    if (error) return
    call sic_def_inte(trim(name)//'%IC',set%ic,0,0,readonly,error)
    if (error) return
  end subroutine cubetools_axset_sicdef
  !
  subroutine cubetools_axset_copy(in,ou,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(axset_t), intent(in)    :: in
    type(axset_t), intent(inout) :: ou
    logical,       intent(inout) :: error
    !
    integer(kind=ndim_k) :: iaxis
    character(len=*), parameter :: rname='AXSET>COPY'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    ! ou%m = in%m ! This one is not needed as long as ou is declared intent(out)
    ou%n = in%n
    do iaxis = 1,in%m
       call cubetools_axis_copy(in%axis(iaxis),ou%axis(iaxis),error)
       if (error) return
    enddo ! iaxis
    ou%il = in%il
    ou%im = in%im
    ou%ic = in%ic
  end subroutine cubetools_axset_copy
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_axset_consistency_init(cons,error)
    !-------------------------------------------------------------------
    ! Init the consistency between axsets
    !-------------------------------------------------------------------
    type(axset_cons_t), intent(out)   :: cons
    logical,            intent(inout) :: error
    !
    integer(kind=ndim_k) :: iaxis
    character(len=*), parameter :: rname='AXSET>CONSISTENCY>INIT'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    cons%check = .not.check
    cons%mess  = .not.mess
    !
    call cubetools_consistency_init(notol,.not.check,mess,cons%m,error)
    if (error) return
    call cubetools_consistency_init(notol,.not.check,mess,cons%n,error)
    if (error) return
    do iaxis=1,maxdim
       call cubetools_axis_consistency_init(cons%axis(iaxis),error)
       if (error) return
       cons%axis(iaxis)%check = .false.
    enddo ! iaxis
    call cubetools_consistency_init(notol,.not.check,mess,cons%il,error)
    if (error) return
    call cubetools_consistency_init(notol,.not.check,mess,cons%im,error)
    if (error) return
    call cubetools_consistency_init(notol,.not.check,mess,cons%ic,error)
    if (error) return
  end subroutine cubetools_axset_consistency_init
  !
  subroutine cubetools_axset_consistency_check(cons,set1,set2,error)
    !-------------------------------------------------------------------
    ! Check the consistency between axsets
    !-------------------------------------------------------------------
    type(axset_cons_t), intent(inout) :: cons
    type(axset_t),      intent(in)    :: set1
    type(axset_t),      intent(in)    :: set2
    logical,            intent(inout) :: error
    !
    integer(kind=ndim_k) :: idim
    character(len=*), parameter :: rname='AXSET>CONSISTENCY>CHECK'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (.not.cons%check) return
    !
    cons%prob = .false.
    call cubetools_consistency_integer_check(cons%m,set1%m,set2%m,error)
    if (error) return
    call cubetools_consistency_integer_check(cons%n,set1%n,set2%n,error)
    if (error) return
    do idim=1,maxdim
       call cubetools_axis_consistency_check(cons%axis(idim),set1%axis(idim),set2%axis(idim),error)
       if (error) return
    enddo ! idim
    call cubetools_consistency_integer_check(cons%il,set1%il,set2%il,error)
    if (error) return
    call cubetools_consistency_integer_check(cons%im,set1%im,set2%im,error)
    if (error) return
    call cubetools_consistency_integer_check(cons%ic,set1%ic,set2%ic,error)
    if (error) return
    !
    cons%prob = cons%m%prob.or.cons%n%prob.or.any(cons%axis(:)%prob).or.&
         cons%il%prob.or.cons%im%prob.or.cons%ic%prob
  end subroutine cubetools_axset_consistency_check
  !
  subroutine cubetools_axset_consistency_list(cons,set1,set2,error)
    !-------------------------------------------------------------------
    ! List the consistency between axsets
    !-------------------------------------------------------------------
    type(axset_cons_t), intent(in)    :: cons
    type(axset_t),      intent(in)    :: set1
    type(axset_t),      intent(in)    :: set2
    logical,            intent(inout) :: error
    !
    integer(kind=ndim_k) :: idim
    character(len=argu_l) :: axname
    character(len=*), parameter :: rname='AXSET>CONSISTENCY>LIST'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (.not.cons%mess) return
    call cubetools_consistency_title('axsets',2,cons%check,cons%prob,error)
    if (error) return
    if (cons%check.and.cons%prob) then
       call cubetools_consistency_integer_print('Max. axes',cons%m,set1%m,set2%m,error)
       if (error) return
       call cubetools_consistency_integer_print('# of axes',cons%n,set1%n,set2%n,error)
       if (error) return
       call cubetools_consistency_integer_print('L axis number',cons%il,set1%il,set2%il,error)
       if (error) return
       call cubetools_consistency_integer_print('M axis number',cons%im,set1%im,set2%im,error)
       if (error) return
       call cubetools_consistency_integer_print('Spectral axis',cons%ic,set1%ic,set2%ic,error)
       if (error) return
       do idim=1,maxdim
          write(axname,'(a,i0)') 'Axis #',idim
          call cubetools_axis_consistency_list(axname,cons%axis(idim),set1%axis(idim),set2%axis(idim),error)
          if (error) return
       enddo ! idim
    endif
    call cubetools_message(seve%r,rname,'')
  end subroutine cubetools_axset_consistency_list
  !
  subroutine cubetools_axset_consistency_final(cons,error)
    !-------------------------------------------------------------------
    ! Final the consistency between axsets
    !-------------------------------------------------------------------
    type(axset_cons_t), intent(out)   :: cons
    logical,            intent(inout) :: error
    !
    integer(kind=ndim_k) :: idim
    character(len=*), parameter :: rname='AXSET>CONSISTENCY>FINAL'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_consistency_final(cons%m,error)
    if (error) return
    call cubetools_consistency_final(cons%n,error)
    if (error) return
    do idim=1,maxdim
       call cubetools_axis_consistency_final(cons%axis(idim),error)
       if (error) return
    enddo ! idim
    call cubetools_consistency_final(cons%il,error)
    if (error) return
    call cubetools_consistency_final(cons%im,error)
    if (error) return
    call cubetools_consistency_final(cons%ic,error)
    if (error) return
  end subroutine cubetools_axset_consistency_final
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_axset_val2zero(axset,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(axset_t), intent(inout) :: axset  
    logical,       intent(inout) :: error
    !
    integer(kind=4) :: spaaxes(2),iax
    character(len=*), parameter :: rname='AXSET>VAL2ZERO'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    !***JP: This code is ambiguous. It assuems that il and im are always spatial axes.
    !***JP: This is not always the case anymore. The heuristics should be improved
    !***JP: to take into account the kind of axis.
    spaaxes = [axset%il,axset%im]
    do iax=1,2
       if (axset%axis(spaaxes(iax))%val.ne.0d0) then
          call cubetools_message(seve%w,rname,'Spatial axis with non 0 reference value not supported')
          call cubetools_message(seve%w,rname,'Changing axis description to a zero valued reference')
          axset%axis(spaaxes(iax))%ref= axset%axis(spaaxes(iax))%ref-&
               (axset%axis(spaaxes(iax))%val/axset%axis(spaaxes(iax))%inc)
          axset%axis(spaaxes(iax))%val = 0d0
       endif
    enddo ! iax
  end subroutine cubetools_axset_val2zero
  !
  !----------------------------------------------------------------------
  !
  function cubetools_axset_count_genuine(axset)
    !-------------------------------------------------------------------
    ! Return the number of genuine axes
    !-------------------------------------------------------------------
    integer(kind=ndim_k)  :: cubetools_axset_count_genuine
    type(axset_t), intent(in) :: axset
    !
    integer(kind=ndim_k) :: iaxis
    !
    cubetools_axset_count_genuine = 0
    do iaxis=1,axset%n
      if (axset%axis(iaxis)%genuine)  &
        cubetools_axset_count_genuine = cubetools_axset_count_genuine+1
    enddo
  end function cubetools_axset_count_genuine
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_axset_set_ignore_degenerate(bool,error)
    !-------------------------------------------------------------------
    ! Set the ignore_degenerate flag
    !-------------------------------------------------------------------
    logical, intent(in)    :: bool
    logical, intent(inout) :: error
    ignore_degenerate = bool
  end subroutine cubetools_axset_set_ignore_degenerate
  !
  function cubetools_axset_get_ignore_degenerate()
    !-------------------------------------------------------------------
    ! Get the ignore_degenerate flag
    !-------------------------------------------------------------------
    logical :: cubetools_axset_get_ignore_degenerate
    cubetools_axset_get_ignore_degenerate = ignore_degenerate
  end function cubetools_axset_get_ignore_degenerate
  !
end module cubetools_axset_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
