!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_consistency_methods
  use cubetools_header_types
  use cubetools_messaging
  !
  public :: cubetools_consistency_failed,cubetools_consistency_shape
  public :: cubetools_consistency_grid,cubetools_consistency_spatial,cubetools_consistency_spectral
  public :: cubetools_consistency_observatory,cubetools_consistency_signal_noise
  private
  !
contains
  !
  subroutine cubetools_consistency_check(cons,name1,head1,name2,head2,problem,error)
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    type(cube_header_cons_t), intent(inout) :: cons
    character(len=*),         intent(in)    :: name1
    type(cube_header_t),      intent(in)    :: head1
    character(len=*),         intent(in)    :: name2
    type(cube_header_t),      intent(in)    :: head2
    logical,                  intent(inout) :: problem
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='CONSISTENCY>CHECK'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_header_consistency_check(cons,head1,head2,error)
    if (error) return
    problem = cons%prob .or. problem
    if (cons%prob) then
       call cubetools_header_consistency_list(cons,head1,head2,error)
       if (error) return
       call cubetools_message(seve%w,rname,trim(name1)//' is inconsistent with '//trim(name2))
    endif
    call cubetools_header_consistency_final(cons,error)
    if (error) return
  end subroutine cubetools_consistency_check
  !
  subroutine cubetools_consistency_shape(name1,head1,name2,head2,problem,error)
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    character(len=*),    intent(in)    :: name1
    type(cube_header_t), intent(in)    :: head1
    character(len=*),    intent(in)    :: name2
    type(cube_header_t), intent(in)    :: head2
    logical,             intent(inout) :: problem
    logical,             intent(inout) :: error
    !
    type(cube_header_cons_t) :: cons
    character(len=*), parameter :: rname='CONSISTENCY>GRID'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_header_consistency_init(cons,error)
    if (error) return
    cons%spa%check = .false.
    cons%spe%do = .false.
    cons%obs%check = .false.
    !
    cons%arr%noi%check   = .false.
    cons%arr%unit%check  = .false.
    cons%arr%min%check   = .false.
    cons%arr%max%check   = .false.
    cons%arr%n%nan%check = .false.
    ! *** JP The next one is a work around 2D vs degenerate 3D cubes
    cons%arr%n%dim%check = .false.
    ! *** JP The previous one is a work around 2D vs degenerate 3D cubes
    !
    call cubetools_consistency_check(cons,name1,head1,name2,head2,problem,error)
    if (error) return
  end subroutine cubetools_consistency_shape
  !
  subroutine cubetools_consistency_grid(name1,head1,name2,head2,problem,error)
    !------------------------------------------------------------------------
    ! Only check the shape of the array, including the number of data, and
    ! does not check the consistency of blanked (NaN) values.
    !------------------------------------------------------------------------
    character(len=*),    intent(in)    :: name1
    type(cube_header_t), intent(in)    :: head1
    character(len=*),    intent(in)    :: name2
    type(cube_header_t), intent(in)    :: head2
    logical,             intent(inout) :: problem
    logical,             intent(inout) :: error
    !
    type(cube_header_cons_t) :: cons
    character(len=*), parameter :: rname='CONSISTENCY>GRID'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_header_consistency_init(cons,error)
    if (error) return
    cons%obs%check       = .false.
    cons%arr%min%check   = .false.
    cons%arr%max%check   = .false.
    cons%arr%n%nan%check = .false.
    cons%spa%bea%check   = .false.
    !
    call cubetools_consistency_check(cons,name1,head1,name2,head2,problem,error)
    if (error) return
  end subroutine cubetools_consistency_grid
  !
  subroutine cubetools_consistency_spectral(name1,head1,name2,head2,problem,error)
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    character(len=*),    intent(in)    :: name1
    type(cube_header_t), intent(in)    :: head1
    character(len=*),    intent(in)    :: name2
    type(cube_header_t), intent(in)    :: head2
    logical,             intent(inout) :: problem
    logical,             intent(inout) :: error
    !
    type(cube_header_cons_t) :: cons
    character(len=*), parameter :: rname='CONSISTENCY>SPECTRAL'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_header_consistency_init(cons,error)
    if (error) return
    cons%obs%check = .false.
    cons%arr%check = .false.
    cons%spa%check = .false.
    !
    call cubetools_consistency_check(cons,name1,head1,name2,head2,problem,error)
    if (error) return
  end subroutine cubetools_consistency_spectral
  !
  subroutine cubetools_consistency_spatial(name1,head1,name2,head2,problem,error)
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    character(len=*),    intent(in)    :: name1
    type(cube_header_t), intent(in)    :: head1
    character(len=*),    intent(in)    :: name2
    type(cube_header_t), intent(in)    :: head2
    logical,             intent(inout) :: problem
    logical,             intent(inout) :: error
    !
    type(cube_header_cons_t) :: cons
    character(len=*), parameter :: rname='CONSISTENCY>SPATIAL'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_header_consistency_init(cons,error)
    if (error) return
    cons%obs%check = .false.
    cons%arr%check = .false.
    cons%spe%do = .false.
    ! *** JP Work-around the fact that we have not yet a way to nocheck the beam
    ! *** JP consistency as this is not stricto-censu needed to avoid segmentation
    ! *** JP faults.
    cons%spa%bea%check = .false.
    ! *** JP
    !
    call cubetools_consistency_check(cons,name1,head1,name2,head2,problem,error)
    if (error) return
  end subroutine cubetools_consistency_spatial
  !
  subroutine cubetools_consistency_observatory(name1,head1,name2,head2,problem,error)
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    character(len=*),    intent(in)    :: name1
    type(cube_header_t), intent(in)    :: head1
    character(len=*),    intent(in)    :: name2
    type(cube_header_t), intent(in)    :: head2
    logical,             intent(inout) :: problem
    logical,             intent(inout) :: error
    !
    type(cube_header_cons_t) :: cons
    character(len=*), parameter :: rname='CONSISTENCY>OBSERVATORY'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_header_consistency_init(cons,error)
    if (error) return
    cons%arr%check = .false.
    cons%spe%do = .false.
    cons%spa%check = .false.
    !
    call cubetools_consistency_check(cons,name1,head1,name2,head2,problem,error)
    if (error) return
  end subroutine cubetools_consistency_observatory
  !
  subroutine cubetools_consistency_signal_noise(name1,head1,name2,head2,problem,error)
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    character(len=*),    intent(in)    :: name1
    type(cube_header_t), intent(in)    :: head1
    character(len=*),    intent(in)    :: name2
    type(cube_header_t), intent(in)    :: head2
    logical,             intent(inout) :: problem
    logical,             intent(inout) :: error
    !
    type(cube_header_cons_t) :: cons
    character(len=*), parameter :: rname='CONSISTENCY>SIGNAL>NOISE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_header_consistency_init(cons,error)
    if (error) return
    cons%arr%check  = .false.
    cons%obs%check  = .false.
    cons%spa%bea%check = .false.
    ! Channel axis is not to be checked
    cons%spe%c%check     = .false.
    cons%spe%ref%c%check = .false.
    ! All other axes must be checked except for their size, genuinity and increments
    cons%spe%f%n%check       = .false.
    cons%spe%f%inc%check     = .false.
    cons%spe%f%genuine%check = .false.
    cons%spe%v%n%check       = .false.
    cons%spe%v%inc%check     = .false.
    cons%spe%v%genuine%check = .false.
    cons%spe%i%n%check       = .false.
    cons%spe%i%inc%check     = .false.
    cons%spe%i%genuine%check = .false.
    cons%spe%l%n%check       = .false.
    cons%spe%l%inc%check     = .false.
    cons%spe%l%genuine%check = .false.
    cons%spe%z%n%check       = .false.
    cons%spe%z%inc%check     = .false.
    cons%spe%z%genuine%check = .false.
    !
    call cubetools_consistency_check(cons,name1,head1,name2,head2,problem,error)
    if (error) return
  end subroutine cubetools_consistency_signal_noise
  !
  function cubetools_consistency_failed(rname,prob,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: rname
    logical,          intent(in)    :: prob
    logical,          intent(inout) :: error
    !
    logical :: cubetools_consistency_failed
    !
    cubetools_consistency_failed = .false.
    if (prob) then
       call cubetools_message(seve%e,rname,'There are inconsistencies between input cubes')
       cubetools_consistency_failed = .true.
       error = .true.
    endif
  end function cubetools_consistency_failed
end module cubetools_consistency_methods
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
