!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_spaelt_types
  use cubetools_parameters
  use cubetools_messaging
  !
  public :: spaelt_t
  public :: cubetools_spaelt_init,cubetools_spaelt_put_and_derive
  public :: cubetools_spaelt_final,cubetools_spaelt_get
  public :: cubetools_spaelt_list,cubetools_spaelt_sicdef,cubetools_spaelt_copy
  private
  !
  type spaelt_t
     real(kind=coor_k) :: i = 0d0 ! [---] Pixel along l axis
     real(kind=coor_k) :: j = 0d0 ! [---] Pixel along m axis
     real(kind=coor_k) :: l = 0d0 ! [rad] RA  or Lii
     real(kind=coor_k) :: m = 0d0 ! [rad] Dec or Bii
  end type spaelt_t
  !
contains
  !
  subroutine cubetools_spaelt_init(spaelt,error)
    !-------------------------------------------------------------------
    ! Just initialize the type, ie, set it intent(out)
    !-------------------------------------------------------------------
    type(spaelt_t), intent(out)   :: spaelt
    logical,        intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPAELT>INIT'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
  end subroutine cubetools_spaelt_init
  !
  subroutine cubetools_spaelt_put_and_derive(ival,jval,lval,mval,spaelt,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    real(kind=coor_k), intent(in)    :: ival
    real(kind=coor_k), intent(in)    :: jval
    real(kind=coor_k), intent(in)    :: lval
    real(kind=coor_k), intent(in)    :: mval
    type(spaelt_t),    intent(inout) :: spaelt
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPAELT>PUT>AND>DERIVE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    spaelt%i = ival
    spaelt%j = jval
    spaelt%l = lval
    spaelt%m = mval
  end subroutine cubetools_spaelt_put_and_derive
  !
  subroutine cubetools_spaelt_get(spaelt,ival,jval,lval,mval,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    type(spaelt_t),    intent(in)    :: spaelt
    real(kind=coor_k), intent(out)   :: ival
    real(kind=coor_k), intent(out)   :: jval
    real(kind=coor_k), intent(out)   :: lval
    real(kind=coor_k), intent(out)   :: mval
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPAELT>GET'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    ival = spaelt%i
    jval = spaelt%j
    lval = spaelt%l
    mval = spaelt%m
  end subroutine cubetools_spaelt_get
  !
  subroutine cubetools_spaelt_final(spaelt,error)
    !-------------------------------------------------------------------
    ! Just initialize, ie, reset it intent(out)
    !-------------------------------------------------------------------
    type(spaelt_t), intent(out)   :: spaelt
    logical,        intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPAELT>FINAL'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
  end subroutine cubetools_spaelt_final
  !
  !---------------------------------------------------------------------
  !
  subroutine cubetools_spaelt_list(spaelt,error)
    use cubetools_format
    !-------------------------------------------------------------------
    ! Empty method until there is a clear usecase
    !-------------------------------------------------------------------
    type(spaelt_t), intent(in)    :: spaelt
    logical,        intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPAELT>LIST'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
  end subroutine cubetools_spaelt_list
  !
  subroutine cubetools_spaelt_sicdef(name,elt,readonly,error)
    use gkernel_interfaces
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: name
    type(spaelt_t),   intent(in)    :: elt
    logical,          intent(in)    :: readonly
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPAELT>SICDEF'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call sic_defstructure(name,global,error)
    if (error) return 
    call sic_def_dble(trim(name)//'%I',elt%i,0,0,readonly,error)
    if (error) return
    call sic_def_dble(trim(name)//'%J',elt%j,0,0,readonly,error)
    if (error) return
    call sic_def_dble(trim(name)//'%L',elt%l,0,0,readonly,error)
    if (error) return
    call sic_def_dble(trim(name)//'%M',elt%m,0,0,readonly,error)
    if (error) return
  end subroutine cubetools_spaelt_sicdef
  !
!!$  subroutine cubetools_spaelt_sicdef(name,lname,mname,error)
!!$    use gkernel_interfaces
!!$    !-------------------------------------------------------------------
!!$    !
!!$    !------------------------------------------------------------------- 
!!$    character(len=*), intent(in)    :: name
!!$    character(len=*), intent(in)    :: lname
!!$    character(len=*), intent(in)    :: mname
!!$    logical,          intent(inout) :: error
!!$    !
!!$    character(len=*), parameter :: rname='SPAELT>SICDEF'
!!$    !
!!$    call cubetools_message(toolseve%trace,rname,'Welcome')
!!$    !
!!$    ! *** JP What happen when the variable has not been defined?
!!$    !
!!$    call sic_defstructure(name,global,error)
!!$    if (error) return
!!$    call sic_def_alias(trim(name)//'%l',lname,global,error)
!!$    if (error) return
!!$    call sic_def_alias(trim(name)//'%m',mname,global,error)
!!$    if (error) return
!!$  end subroutine cubetools_spaelt_sicdef
  !
  subroutine cubetools_spaelt_copy(in,ou,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    type(spaelt_t), intent(in)    :: in
    type(spaelt_t), intent(out)   :: ou
    logical,        intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPAELT>COPY'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    ou = in
  end subroutine cubetools_spaelt_copy
end module cubetools_spaelt_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
