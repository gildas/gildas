!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! In this file, the following modules are present in this order:
!    cubetools_obstel_prog_types
!    cubetools_obstel_consistency_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_obstel_prog_types
  use cubetools_parameters
  use cubetools_messaging
  !
  public :: obstel_prog_t
  private
  !
  type obstel_prog_t
     real(kind=coor_k)     :: lon = 0d0       ! [rad] Longitude
     real(kind=coor_k)     :: lat = 0d0       ! [rad] Latitude
     real(kind=real_k)     :: alt = 0.0       ! [  m] Altitude
     real(kind=real_k)     :: diam = 0.0      ! [  m] Diameter
     character(len=tele_l) :: name = strg_unk ! [---] Name
   contains
     ! Administration
     procedure :: init             => cubetools_obstel_init
     procedure :: create_from_name => cubetools_obstel_create_from_name
     procedure :: put              => cubetools_obstel_put
     procedure :: get_and_derive   => cubetools_obstel_get_and_derive
     procedure :: read             => cubetools_obstel_read
     procedure :: write            => cubetools_obstel_write
     procedure :: copyto           => cubetools_obstel_copyto
     procedure :: isequalto        => cubetools_obstel_isequalto
     procedure :: list             => cubetools_obstel_list
     procedure :: final            => cubetools_obstel_final
     ! Feedback to user
     procedure :: load_into        => cubetools_obstel_load_into
     procedure :: obsolete_sicdef  => cubetools_obstel_sicdef
  end type obstel_prog_t
  !
  integer(kind=4), parameter :: key_l=24  ! Note the T26 tab below
  character(len=*), parameter :: form_tel='(A,T26,A12,2(1PG25.16),2(1PG15.7))'
  !
contains
  !
  subroutine cubetools_obstel_init(obstel,error)
    !-------------------------------------------------------------------
    ! Just initialize the type, ie, set it intent(out)
    !-------------------------------------------------------------------
    class(obstel_prog_t), intent(out)   :: obstel
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='OBSTEL>INIT'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
  end subroutine cubetools_obstel_init
  !
  subroutine cubetools_obstel_create_from_name(obstel,name,error)
    use phys_const
    use image_def, only: telesco
    use gkernel_interfaces, only: sic_upper,gwcs_observatory_exists,gwcs_observatory
    use cubetools_nan
    !-------------------------------------------------------------------
    ! Create a obstel object from a name and pull the telescope info
    ! from the kernel. The input name is upcased first because GILDAS
    ! only knows upcased telescope names!
    !-------------------------------------------------------------------
    class(obstel_prog_t),  intent(out)   :: obstel
    character(len=tele_l), intent(in)    :: name
    logical,               intent(inout) :: error
    !
    character(len=tele_l) :: upcasedname
    character(len=*), parameter :: rname='OBSTEL>CREATE>FROM>NAME'
    !
    type(telesco) :: gtel
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    upcasedname = name
    call sic_upper(upcasedname)
    if (gwcs_observatory_exists(upcasedname)) then
      ! Known by GILDAS
      call gwcs_observatory(upcasedname,gtel,error)
      if (error)  return
      call obstel%get_and_derive(&
        gtel%lon*rad_per_deg,  &
        gtel%lat*rad_per_deg,  &
        gtel%alt,              &
        gtel%diam,             &
        gtel%ctele,            &
        error)
      if (error)  return
    else
      ! Unknown telescope. Add it but for now use NaN parameters.
      ! We should have a way to provide the parameters by some external
      ! means (e.g. user input). Beware also of space telescopes!
      call obstel%get_and_derive(&
        gr8nan,&
        gr8nan,&
        gr4nan,&
        gr4nan,&
        upcasedname,&
        error)
      if (error)  return
    endif
  end subroutine cubetools_obstel_create_from_name
  !
  subroutine cubetools_obstel_get_and_derive(obstel,lon,lat,alt,diam,name,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(obstel_prog_t),  intent(inout) :: obstel
    real(kind=coor_k),     intent(in)    :: lon
    real(kind=coor_k),     intent(in)    :: lat
    real(kind=real_k),     intent(in)    :: alt
    real(kind=real_k),     intent(in)    :: diam
    character(len=tele_l), intent(in)    :: name
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='OBSTEL>GET>AND>DERIVE'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    obstel%lon = lon
    obstel%lat = lat
    obstel%alt = alt
    obstel%diam = diam
    obstel%name = name
  end subroutine cubetools_obstel_get_and_derive
  !
  subroutine cubetools_obstel_put(obstel,lon,lat,alt,diam,name,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    class(obstel_prog_t),  intent(in)    :: obstel
    real(kind=coor_k),     intent(out)   :: lon
    real(kind=coor_k),     intent(out)   :: lat
    real(kind=real_k),     intent(out)   :: alt
    real(kind=real_k),     intent(out)   :: diam
    character(len=tele_l), intent(out)   :: name
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='OBSTEL>PUT'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    lon = obstel%lon
    lat = obstel%lat
    alt = obstel%alt
    diam = obstel%diam
    name = obstel%name
  end subroutine cubetools_obstel_put
  !
  subroutine cubetools_obstel_read(obstel,lun,error)
    !-------------------------------------------------------------------
    ! Read the observatory from the given logical unit
    !-------------------------------------------------------------------
    class(obstel_prog_t), intent(inout) :: obstel
    integer(kind=4),      intent(in)    :: lun
    logical,              intent(inout) :: error
    !
    character(len=key_l) :: key
    !
    read(lun,form_tel)  key,          &
                        obstel%name,  &
                        obstel%lon,   &
                        obstel%lat,   &
                        obstel%alt,   &
                        obstel%diam
  end subroutine cubetools_obstel_read
  !
  subroutine cubetools_obstel_write(obstel,lun,error)
    !-------------------------------------------------------------------
    ! Write the observatory to the given logical unit
    !-------------------------------------------------------------------
    class(obstel_prog_t), intent(in)    :: obstel
    integer(kind=4),      intent(in)    :: lun
    logical,              intent(inout) :: error
    !
    write(lun,form_tel)  'OBSTEL',     &
                         obstel%name,  &
                         obstel%lon,   &
                         obstel%lat,   &
                         obstel%alt,   &
                         obstel%diam
  end subroutine cubetools_obstel_write
  !
  subroutine cubetools_obstel_copyto(in,ou,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(obstel_prog_t), intent(in)    :: in
    type(obstel_prog_t),  intent(out)   :: ou
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='OBSTEL>COPYTO'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    ou = in
  end subroutine cubetools_obstel_copyto
  !
  function cubetools_obstel_isequalto(tel1,tel2) result(equal)
    !-------------------------------------------------------------------
    ! Return true if two obstel_prog_t instances are equal
    !-------------------------------------------------------------------
    class(obstel_prog_t), intent(in) :: tel1
    type(obstel_prog_t),  intent(in) :: tel2
    logical                          :: equal
    !
    equal = tel1%lon.eq.tel2%lon
    equal = tel1%lat.eq.tel2%lat.and.equal
    equal = tel1%alt.eq.tel2%alt.and.equal
    equal = tel1%diam.eq.tel2%diam.and.equal
    equal = tel1%name.eq.tel2%name.and.equal
  end function cubetools_obstel_isequalto
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_obstel_list(obstel,error)
    use ieee_arithmetic
    use gkernel_interfaces
    use cubetools_format
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    class(obstel_prog_t), intent(in)    :: obstel
    logical,              intent(inout) :: error
    !
    character(len=15) :: lat,lon
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='OBSTEL>LIST'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    mess = cubetools_format_stdkey_boldval('Tel',obstel%name,10)
    if (ieee_is_nan(obstel%lon)) then
      mess = trim(mess)//'  '//cubetools_format_stdkey_boldval('Lon',obstel%lon,'f7.2',18)  ! NaN
    else
      call rad2sexa(obstel%lon,360,lon)
      mess = trim(mess)//'  '//cubetools_format_stdkey_boldval('Lon',lon,18)
    endif
    if (ieee_is_nan(obstel%lat)) then
      mess = trim(mess)//'  '//cubetools_format_stdkey_boldval('Lat',obstel%lat,'f7.2',18)  ! NaN
    else
      call rad2sexa(obstel%lat,360,lat)
      mess = trim(mess)//'  '//cubetools_format_stdkey_boldval('Lat',lat,18)
    endif
    mess = trim(mess)//'  '//cubetools_format_stdkey_boldval('Alt',obstel%alt,'f7.2',12)
    mess = trim(mess)//'  '//cubetools_format_stdkey_boldval('Dia',obstel%diam,'f5.1',10)
    call cubetools_message(seve%r,rname,mess)
  end subroutine cubetools_obstel_list
  !
  subroutine cubetools_obstel_final(obstel,error)
    !-------------------------------------------------------------------
    ! Just reinitialize the type, ie, set it intent(out)
    !-------------------------------------------------------------------
    class(obstel_prog_t), intent(out)   :: obstel
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='OBSTEL>FINAL'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
  end subroutine cubetools_obstel_final
  !
  !---------------------------------------------------------------------
  !
  subroutine cubetools_obstel_load_into(obstel,structure,error)
    use cubetools_userstruct
    use gkernel_interfaces, only: rad2sexa
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(obstel_prog_t), intent(in)    :: obstel
    type(userstruct_t),   intent(in)    :: structure
    logical,              intent(inout) :: error
    !
    character(len=argu_l) :: lon,lat
    character(len=*), parameter :: rname='OBSTEL>LOAD>INTO'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    ! User struct is created as substructure on the calling routine
    call rad2sexa(obstel%lon,360,lon)
    call rad2sexa(obstel%lat,360,lat)
    call structure%set_member('lon',adjustl(lon),error)
    if (error) return
    call structure%set_member('lat',adjustl(lat),error)
    if (error) return
    call structure%set_member('alt',obstel%alt,error)
    if (error) return
    call structure%set_member('diam',obstel%diam,error)
    if (error) return
    call structure%set_member('name',obstel%name,error)
    if (error) return
  end subroutine cubetools_obstel_load_into
  !
  subroutine cubetools_obstel_sicdef(obstel,name,readonly,error)
    use gkernel_interfaces
    !-------------------------------------------------------------------
    !***JP: Obsolete => Only used by the OLDLOAD command!
    !-------------------------------------------------------------------
    class(obstel_prog_t), intent(in)    :: obstel
    character(len=*),     intent(in)    :: name
    logical,              intent(in)    :: readonly
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='OBSTEL>SICDEF'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call sic_defstructure(name,global,error)
    if (error) return
    call sic_def_dble(trim(name)//'%lon',obstel%lon,0,0,readonly,error)
    if (error) return
    call sic_def_dble(trim(name)//'%lat',obstel%lat,0,0,readonly,error)
    if (error) return
    call sic_def_real(trim(name)//'%alt',obstel%alt,0,0,readonly,error)
    if (error) return
    call sic_def_real(trim(name)//'%diam',obstel%diam,0,0,readonly,error)
    if (error) return
    call sic_def_charn(trim(name)//'%name',obstel%name,0,0,readonly,error)
    if (error) return
  end subroutine cubetools_obstel_sicdef
end module cubetools_obstel_prog_types
!
module cubetools_obstel_consistency_types
  use cubetools_messaging
  use cubetools_consistency_types
  use cubetools_obstel_prog_types
  !
  public :: obstel_cons_t
  public :: cubetools_obstel_consistency_init,cubetools_obstel_consistency_final
  public :: cubetools_obstel_consistency_check,cubetools_obstel_consistency_list
  private
  !
  type obstel_cons_t
     logical                  :: check=.true.  ! Check the section
     logical                  :: prob =.false. ! Is there a problem
     logical                  :: mess =.true.  ! Output message for this section?
     type(consistency_desc_t) :: name
     type(consistency_desc_t) :: lon
     type(consistency_desc_t) :: lat
     type(consistency_desc_t) :: alt
     type(consistency_desc_t) :: diam
  end type obstel_cons_t
  !
contains
  !
  subroutine cubetools_obstel_consistency_init(cons,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(obstel_cons_t), intent(inout) :: cons
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='OBSTEL>CONSISTENCY>INIT'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_consistency_init(notol,check,mess,cons%name,error)
    if (error) return
    call cubetools_consistency_init(strict,check,mess,cons%lon,error)
    if (error) return
    call cubetools_consistency_init(strict,check,mess,cons%lat,error)
    if (error) return
    call cubetools_consistency_init(strict,check,mess,cons%alt,error)
    if (error) return
    call cubetools_consistency_init(strict,check,mess,cons%diam,error)
    if (error) return
  end subroutine cubetools_obstel_consistency_init
  !
  subroutine cubetools_obstel_consistency_check(cons,tel1,tel2,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(obstel_cons_t), intent(inout) :: cons
    type(obstel_prog_t), intent(in)    :: tel1
    type(obstel_prog_t), intent(in)    :: tel2
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='OBSTEL>CONSISTENCY>CHECK'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (.not.cons%check) return
    !
    cons%prob = .false.
    call cubetools_consistency_string_check(cons%name,tel1%name,tel2%name,error)
    if (error) return
    !if (cons%name%prob) return
    call cubetools_consistency_real_check(cons%lon,tel1%lon,tel2%lon,error)
    if (error) return
    call cubetools_consistency_real_check(cons%lat,tel1%lat,tel2%lat,error)
    if (error) return
    call cubetools_consistency_real_check(cons%alt,tel1%alt,tel2%alt,error)
    if (error) return
    call cubetools_consistency_real_check(cons%diam,tel1%diam,tel2%diam,error)
    if (error) return
    !
    cons%prob = cons%name%prob.or.cons%lon%prob.or.cons%lat%prob.or.cons%alt%prob.or.cons%diam%prob
  end subroutine cubetools_obstel_consistency_check
  !
   subroutine cubetools_obstel_consistency_list(cons,tel1,tel2,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(obstel_cons_t), intent(in)    :: cons
    type(obstel_prog_t), intent(in)    :: tel1
    type(obstel_prog_t), intent(in)    :: tel2
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='OBSTEL>CONSISTENCY>LIST'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (.not.cons%mess) return
    !
    call cubetools_consistency_title('telescopes '//trim(tel1%name)//' & '//trim(tel2%name),&
         3,cons%check,cons%prob,error,short=.true.)
    if (error) return
    if (cons%name%prob) return
    if (cons%check.and.cons%prob) then
       call cubetools_consistency_string_print('Names',cons%name,tel1%name,tel2%name,error)
       if (error) return
       call cubetools_consistency_real_print('Longitudes',cons%lon,tel1%lon,tel2%lon,error)
       if (error) return
       call cubetools_consistency_real_print('Latitudes',cons%lat,tel1%lat,tel2%lat,error)
       if (error) return
       call cubetools_consistency_real_print('Altitudes',cons%alt,tel1%alt,tel2%alt,error)
       if (error) return
       call cubetools_consistency_real_print('Diameters',cons%diam,tel1%diam,tel2%diam,error)
       if (error) return
       call cubetools_message(seve%r,rname,'') 
    endif
    !
  end subroutine cubetools_obstel_consistency_list
  !
  subroutine cubetools_obstel_consistency_final(cons,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(obstel_cons_t), intent(inout) :: cons
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='OBSTEL>CONSISTENCY>FINAL'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    call cubetools_consistency_final(cons%name,error)
    if (error) return
    call cubetools_consistency_final(cons%lon,error)
    if (error) return
    call cubetools_consistency_final(cons%lat,error)
    if (error) return
    call cubetools_consistency_final(cons%alt,error)
    if (error) return
    call cubetools_consistency_final(cons%diam,error)
    if (error) return
  end subroutine cubetools_obstel_consistency_final
end module cubetools_obstel_consistency_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
