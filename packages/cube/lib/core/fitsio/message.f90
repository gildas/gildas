!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage CUBE FITSIO messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubefitsio_messaging
  use cfitsio_api
  use gpack_def
  use gbl_message
  use cubetools_parameters
  !
  public :: seve,fitsioseve,mess_l
  public :: cubefitsio_message_set_id,cubefitsio_message,cubefitsio_error
  public :: cubefitsio_message_set_trace,cubefitsio_message_get_trace
  public :: cubefitsio_message_set_others,cubefitsio_message_get_others
  private
  !
  ! Identifier used for message identification
  integer(kind=4) :: cubefitsio_message_id = gpack_global_id  ! Default value for startup message
  !
  type :: cubefitsio_messaging_debug_t
     integer(kind=code_k) :: trace = seve%t
     integer(kind=code_k) :: others = seve%d
  end type cubefitsio_messaging_debug_t
  type(cubefitsio_messaging_debug_t) :: fitsioseve
  !
contains
  !
  subroutine cubefitsio_message_set_id(id)
    !---------------------------------------------------------------------
    ! Alter library id into input id. Should be called by the library
    ! which wants to share its id with the current one.
    !---------------------------------------------------------------------
    integer(kind=4), intent(in) :: id
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='MESSAGE>SET>ID'
    !
    cubefitsio_message_id = id
    write (mess,'(A,I0)') 'Now use id #',cubefitsio_message_id
    call cubefitsio_message(seve%d,rname,mess)
  end subroutine cubefitsio_message_set_id
  !
  subroutine cubefitsio_message(mkind,procname,message)
    use cubetools_cmessaging
    !---------------------------------------------------------------------
    ! Messaging facility for the current library. Calls the low-level
    ! (internal) messaging routine with its own identifier.
    !---------------------------------------------------------------------
    integer(kind=4),  intent(in) :: mkind     ! Message kind
    character(len=*), intent(in) :: procname  ! Name of calling procedure
    character(len=*), intent(in) :: message   ! Message string
    !
    call cubetools_cmessage(cubefitsio_message_id,mkind,'FITSIO>'//procname,message)
  end subroutine cubefitsio_message
  !
  function cubefitsio_error(procname,status,error)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubefitsio_error  ! Function value on return
    character(len=*), intent(in)    :: procname
    integer(kind=4),  intent(in)    :: status
    logical,          intent(inout) :: error
    ! Local
    character(len=message_length) :: mess
    !
    cubefitsio_error = status.ne.0
    if (cubefitsio_error) then
      error = .true.
      call ftgerr(status,mess)
      call cubefitsio_message(seve%e,procname,mess)
    endif
    !
  end function cubefitsio_error
  !
  subroutine cubefitsio_message_set_trace(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       fitsioseve%trace = seve%i
    else
       fitsioseve%trace = seve%t
    endif
  end subroutine cubefitsio_message_set_trace
  !
  subroutine cubefitsio_message_set_others(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       fitsioseve%others = seve%i
    else
       fitsioseve%others = seve%d
    endif
  end subroutine cubefitsio_message_set_others
  !
  function cubefitsio_message_get_trace()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubefitsio_message_get_trace
    !
    cubefitsio_message_get_trace = fitsioseve%trace.eq.seve%i
    !
  end function cubefitsio_message_get_trace
  !
  function cubefitsio_message_get_others()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubefitsio_message_get_others
    !
    cubefitsio_message_get_others = fitsioseve%others.eq.seve%i
    !
  end function cubefitsio_message_get_others
  !
end module cubefitsio_messaging
!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
