!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubefitsio_header
  use cfitsio_api
  use gkernel_interfaces
  use cubetools_parameters
  use cubetools_checksum
  use gfits_types
  use cubefitsio_messaging
  !
  integer(kind=4), parameter :: fits_primary_hdu=1
  !
  type :: fitsio_header_t
    integer(kind=4)      :: unit=0                   ! Logical unit for associated FITS file
    integer(kind=4)      :: hdunum=fits_primary_hdu  ! HDU number (1=Primary)
    integer(kind=code_k) :: hdutype=code_null        ! HDU type (CFITSIO code)
    character(len=512)   :: id=strg_unk              ! Some identifier string (for feedback)
    ! Components which have an effect on the FITS reading or writing
    integer(kind=code_k) :: action=code_null
    integer(kind=code_k) :: dtype=code_null       ! Type of data on disk (e.g. R*4, C*4, etc)
    integer(kind=code_k) :: mtype=code_null       ! Type of data in memory (e.g. R*4, C*4, etc)
    integer(kind=ndim_k) :: ndim
    integer(kind=data_k) :: dim(maxdim)
    logical              :: checksum
    ! Collection of components with no meaning for FITS reading or writing
    type(gfits_hdict_t) :: dict
  contains
    procedure :: init    => cubefitsio_header_init
    procedure :: open    => cubefitsio_header_open
    procedure :: close   => cubefitsio_header_close
    procedure :: message => cubefitsio_header_message
  end type fitsio_header_t
  !
  public :: fitsio_header_t
  public :: cubefitsio_file_nhdu
  private
  !
contains
  !
  subroutine cubefitsio_header_init(hfits,filename,dochecksum,error)
    !-------------------------------------------------------------------
    ! Create and open a new FITS file
    !-------------------------------------------------------------------
    class(fitsio_header_t), intent(inout) :: hfits
    character(len=*),       intent(in)    :: filename
    logical,                intent(in)    :: dochecksum
    logical,                intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='INIT'
    integer(kind=4) :: ier,status,blocksize
    !
    status = 0
    !
    ier = sic_getlun(hfits%unit)
    if (mod(ier,2).eq.0) then
      error = .true.
      return
    endif
    !
    hfits%action = code_write
    hfits%id = strg_unk
    hfits%checksum = dochecksum
    blocksize = 1  ! Ignored by FTINIT
    call ftinit(hfits%unit,filename,blocksize,status)
    if (cubefitsio_error(rname,status,error))  return
  end subroutine cubefitsio_header_init
  !
  subroutine cubefitsio_header_open(hfits,filename,id,error)
    !-------------------------------------------------------------------
    ! Open an old FITS file
    !-------------------------------------------------------------------
    class(fitsio_header_t), intent(inout) :: hfits
    character(len=*),       intent(in)    :: filename
    character(len=*),       intent(in)    :: id  ! Some identifier string
    logical,                intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='INIT'
    integer(kind=4) :: ier,status,blocksize
    integer(kind=4), parameter :: readonly=0
    !
    if (gag_inquire(filename,len_trim(filename)).ne.0) then
      call cubefitsio_message(seve%e,rname,'No such file '//filename)
      error = .true.
      return
    endif
    !
    ier = sic_getlun(hfits%unit)
    if (mod(ier,2).eq.0) then
      error = .true.
      return
    endif
    !
    hfits%action = code_read
    hfits%id = id
    hfits%checksum = .false.
    status = 0
    call ftopen(hfits%unit,filename,readonly,blocksize,status)
    if (cubefitsio_error(rname,status,error))  return
  end subroutine cubefitsio_header_open
  !
  subroutine cubefitsio_header_close(hfits,error)
    !-------------------------------------------------------------------
    ! Close a (new or old) FITS file
    !-------------------------------------------------------------------
    class(fitsio_header_t), intent(inout) :: hfits
    logical,                intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='CLOSE'
    integer(kind=4) :: status
    character(len=sha1_l) :: digest
    !
    if (hfits%unit.eq.0)  return  ! Already closed
    !
    status = 0
    !
    if (hfits%checksum .and. hfits%action.eq.code_write) then
      digest = cubetools_sha1sum_get()
      call cubefitsio_message(seve%i,rname,'Checksum is '//digest)
      call ftukys(hfits%unit,'SHA1SUM',digest,'Checksum of the data array',status)
      if (cubefitsio_error(rname,status,error))  return
    endif
    !
    call ftclos(hfits%unit,status)
    if (cubefitsio_error(rname,status,error))  return
    !
    call sic_frelun(hfits%unit)
    hfits%unit = 0
    hfits%action = code_null
    hfits%id = strg_unk
  end subroutine cubefitsio_header_close
  !
  subroutine cubefitsio_header_message(hfits,seve,rname,mess)
    !-------------------------------------------------------------------
    ! Subroutine dedicated to messages customized for the current hfits
    ! object
    !-------------------------------------------------------------------
    class(fitsio_header_t), intent(in)    :: hfits
    integer(kind=4),        intent(in)    :: seve
    character(len=*),       intent(in)    :: rname
    character(len=*),       intent(in)    :: mess
    !
    call cubefitsio_message(seve,rname,  &
      'Object '//trim(hfits%id)//' > '//mess)
    !
  end subroutine cubefitsio_header_message
  !
  subroutine cubefitsio_file_nhdu(filename,nhdu,error)
    !-------------------------------------------------------------------
    ! Return the number of HDU (all kind) in the named file
    ! ZZZ This subroutine is misplaced
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: filename
    integer(kind=4),  intent(out)   :: nhdu
    logical,          intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='FILE>NHDU'
    type(fitsio_header_t) :: hfits
    integer(kind=4) :: status
    !
    call hfits%open(filename,'???',error)
    if (error) return
    !
    status = 0
    call ftthdu(hfits%unit,nhdu,status)
    if (cubefitsio_error(rname,status,error))  return
    !
    call hfits%close(error)
    if (error)  return
  end subroutine cubefitsio_file_nhdu
end module cubefitsio_header
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
