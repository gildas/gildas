!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubefitsio_header_read
  use cfitsio_api
  use cubefitsio_messaging
  use cubefitsio_header
  !
  public :: fitsio_header_t
  public :: cubefitsio_header_fill
  private
  !
contains
  !
  subroutine cubefitsio_header_fill(hfits,error)
    use gbl_format
    use gkernel_interfaces
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(fitsio_header_t), intent(inout) :: hfits
    logical,               intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='HEADER>FILL'
    integer(kind=4) :: nbit
    character(len=mess_l) :: mess
    !
    ! Sanity
    if (hfits%unit.le.0)then
      call cubefitsio_message(seve%e,rname,'Internal error: logical unit is not set')
      error = .true.
      return
    endif
    !
    call cubefitsio_header_load_dict(hfits%unit,hfits%dict,error)
    if (error)  return
    !
    if (hfits%hdunum.eq.1) then
      ! SIMPLE: we are reading the Primary HDU
      call gfits_check_simple(hfits%dict%card(1),error)
      if (error)  return
    else
      ! XTENSION: can be an IMAGE or a BINTABLE
      call gfits_check_xtension(hfits%dict%card(1),error)
      if (error)  return
    endif
    ! BITPIX
    call gfits_check_format(hfits%dict%card(2),nbit,hfits%dtype,error)
    if (error)  return
    hfits%mtype = fmt_r4  ! CFITSIO API does implicit conversion to our R*4 buffers
    ! NAXIS: always the 3rd card
    call gfits_check_naxis(hfits%dict%card(3),hfits%ndim,error)
    if (error)  return
    if (hfits%ndim.le.0) then
      write(mess,'(a,i0,a)')  'No data in HDU #',hfits%hdunum,' (NAXIS=0)'
      call cubefitsio_message(seve%e,rname,mess)
      error = .true.
      return
    endif
    ! NAXISi
    call gfits_check_naxisi(hfits%dict,hfits%dim,error)
    if (error)  return
    if (any(hfits%dim(1:hfits%ndim).eq.0)) then
      write(mess,'(a,i0,a)')  'No data in HDU #',hfits%hdunum,' (NAXISi=0)'
      call cubefitsio_message(seve%e,rname,mess)
      error = .true.
      return
    endif
  end subroutine cubefitsio_header_fill
  !
  subroutine cubefitsio_header_load_dict(unit,fdict,error)
    use gfits_types
    use gkernel_interfaces
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    integer(kind=4),     intent(in)    :: unit
    type(gfits_hdict_t), intent(inout) :: fdict
    logical,             intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='HEADER>LOAD'
    integer(kind=4) :: icard,ikey,status,nkey,fkey
    !
    ! Fetch number of "keys" in the header (END is not counted)
    status = 0
    call ftghps(unit,nkey,fkey,status)
    if (cubefitsio_error(rname,status,error))  return
    !
    ! Load all the keys
    fdict%ncard = 0
    do ikey=1,nkey
      call gfits_reallocate_dict(fdict,error)
      if (error)  return
      !
      icard = fdict%ncard+1
      call ftgkyn(unit,ikey,  &
                  fdict%card(icard)%key,      &
                  fdict%card(icard)%val,      &
                  fdict%card(icard)%comment,  &
                  status)
      if (cubefitsio_error(rname,status,error))  return
      ! Note: ftghps return the number of "pseudo-keys", including blank
      ! lines, HISTORY, COMMENT. They are also read by ftgkyn. Need to
      ! filter these out.
      if (fdict%card(icard)%key.eq.'COMMENT')  cycle
      if (fdict%card(icard)%key.eq.'HISTORY')  cycle
      if (fdict%card(icard)%key.eq.'')         cycle  ! Must be a comment
      ! That's a valid card: keep it
      fdict%ncard = icard
      fdict%sort(icard) = icard
    enddo
    !
    ! Compute the sorting array
    call gfits_setsort(fdict,error)
    if (error)  return
  end subroutine cubefitsio_header_load_dict
end module cubefitsio_header_read
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
