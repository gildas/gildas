module cubefitsio_header_write
  use ieee_arithmetic
  use cfitsio_api
  use gfits_types
  use gkernel_interfaces
  use cubefitsio_header
  use cubefitsio_messaging

  public :: fitsio_header_t
  public :: cubefitsio_header_addstr,cubefitsio_header_addcomment
  public :: cubefitsio_header_addi4
  public :: cubefitsio_header_addr4,cubefitsio_header_addr8
  public :: cubefitsio_header_write_card
  private

contains

  subroutine cubefitsio_header_addstr(hfits,key,val,comment,error)
    type(fitsio_header_t), intent(inout) :: hfits
    character(len=*),      intent(in)    :: key
    character(len=*),      intent(in)    :: val
    character(len=*),      intent(in)    :: comment
    logical,               intent(inout) :: error
    ! Local
    integer(kind=4) :: icard
    !
    call gfits_reallocate_dict(hfits%dict,error)
    if (error)  return
    !
    icard = hfits%dict%ncard+1
    hfits%dict%card(icard)%key = key
    hfits%dict%card(icard)%val = ''''//trim(val)//''''
    hfits%dict%card(icard)%comment = comment
    hfits%dict%sort(icard) = icard
    hfits%dict%ncard = icard
  end subroutine cubefitsio_header_addstr

  subroutine cubefitsio_header_addi4(hfits,key,val,comment,error)
    type(fitsio_header_t), intent(inout) :: hfits
    character(len=*),      intent(in)    :: key
    integer(kind=4),       intent(in)    :: val
    character(len=*),      intent(in)    :: comment
    logical,               intent(inout) :: error
    ! Local
    integer(kind=4) :: icard
    !
    call gfits_reallocate_dict(hfits%dict,error)
    if (error)  return
    !
    icard = hfits%dict%ncard+1
    hfits%dict%card(icard)%key = key
    write(hfits%dict%card(icard)%val,'(I20)')  val
    hfits%dict%card(icard)%comment = comment
    hfits%dict%sort(icard) = icard
    hfits%dict%ncard = icard
  end subroutine cubefitsio_header_addi4

  subroutine cubefitsio_header_addr4(hfits,key,val,comment,error)
    type(fitsio_header_t), intent(inout) :: hfits
    character(len=*),      intent(in)    :: key
    real(kind=4),          intent(in)    :: val
    character(len=*),      intent(in)    :: comment
    logical,               intent(inout) :: error
    ! Local
    integer(kind=4) :: icard
    !
    if (ieee_is_nan(val)) then
      ! As per the FITS standard ("Definition of the Flexible Image
      ! Transport System", Version 4.0, section 4.1.2.3, undefined
      ! values should be represented as null field, i.e. unquoted
      ! spaces. In particular, unquoted NaN is invalid. Namely:
      ! hfits%dict%card(icard)%val = ''
      !
      ! An alternate possibility is that the keyword is not written
      ! at all, which is more in the FITS habits. At read time it
      ! will be the responsibility of the caller to provide a default
      ! for the missing keyword.
      return
    endif
    !
    call gfits_reallocate_dict(hfits%dict,error)
    if (error)  return
    !
    icard = hfits%dict%ncard+1
    hfits%dict%card(icard)%key = key
    write(hfits%dict%card(icard)%val,'(E20.8)')  val
    hfits%dict%card(icard)%comment = comment
    hfits%dict%sort(icard) = icard
    hfits%dict%ncard = icard
  end subroutine cubefitsio_header_addr4

  subroutine cubefitsio_header_addr8(hfits,key,val,comment,error)
    type(fitsio_header_t), intent(inout) :: hfits
    character(len=*),      intent(in)    :: key
    real(kind=8),          intent(in)    :: val
    character(len=*),      intent(in)    :: comment
    logical,               intent(inout) :: error
    ! Local
    integer(kind=4) :: icard
    !
    if (ieee_is_nan(val)) then
      ! As per the FITS standard ("Definition of the Flexible Image
      ! Transport System", Version 4.0, section 4.1.2.3, undefined
      ! values should be represented as null field, i.e. unquoted
      ! spaces. In particular, unquoted NaN is invalid. Namely:
      ! hfits%dict%card(icard)%val = ''
      !
      ! An alternate possibility is that the keyword is not written
      ! at all, which is more in the FITS habits.
      return
    endif
    !
    call gfits_reallocate_dict(hfits%dict,error)
    if (error)  return
    !
    icard = hfits%dict%ncard+1
    hfits%dict%card(icard)%key = key
    write(hfits%dict%card(icard)%val,'(E20.13)')  val
    hfits%dict%card(icard)%comment = comment
    hfits%dict%sort(icard) = icard
    hfits%dict%ncard = icard
  end subroutine cubefitsio_header_addr8

  subroutine cubefitsio_header_addcomment(hfits,comment,error)
    type(fitsio_header_t), intent(inout) :: hfits
    character(len=*),      intent(in)    :: comment
    logical,               intent(inout) :: error
    ! Local
    integer(kind=4) :: icard
    !
    call gfits_reallocate_dict(hfits%dict,error)
    if (error)  return
    !
    icard = hfits%dict%ncard+1
    hfits%dict%card(icard)%key = 'COMMENT'
    hfits%dict%card(icard)%val = ''
    hfits%dict%card(icard)%comment = comment
    hfits%dict%sort(icard) = icard
    hfits%dict%ncard = icard
  end subroutine cubefitsio_header_addcomment

  subroutine cubefitsio_header_write_card(unit,card,error)
    integer(kind=4),      intent(in)    :: unit
    type(fits_unkn_0d_t), intent(in)    :: card
    logical,              intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='HEADER>UPSERT>CARD'
    integer(kind=4) :: status,nv,nc
    character(len=160) :: str  ! String longer than 1 line (truncated by ftucrd)
    !
    status = 0
    !
    if (card%key.eq.'COMMENT') then
      call ftpcom(unit,card%comment,status)
      if (cubefitsio_error(rname,status,error))  return
    else
      nv = len_trim(card%val)
      nc = len_trim(card%comment)
      if (8+2+nv.lt.39) then  ! Does it fit before the T39?
        write(str,'(A,A,A,T39,A,A)')  &
          card%key(1:8),'= ',card%val(1:nv),' / ',card%comment(1:nc)
      else
        write(str,'(A,A,A,A,A)')  &
          card%key(1:8),'= ',card%val(1:nv),' / ',card%comment(1:nc)
      endif
      ! Note: if the string value + comment is longer than one line
      ! (80 chars) we could use ftukls which will split the value in
      ! several continued lines, but this function is too clever and
      ! adds extra quotes.
      call ftucrd(unit,card%key,str,status)
      if (cubefitsio_error(rname,status,error))  return
    endif
    !
  end subroutine cubefitsio_header_write_card

end module cubefitsio_header_write
