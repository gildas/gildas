module cubeio_write
  use cubetools_setup_types
  use cubeio_interface
  use cubeio_cube_define
  use cubeio_block
  use cubeio_range
  use cubeio_messaging
  use cubeio_cube

  public :: cubeio_create_cube_data
  public :: cubeio_write_any_block
  private

contains

  subroutine cubeio_create_cube_data(cubset,cubdef,dno,cub,error)
    use cubetools_nan
    use cubetools_dataformat_types
    use cubedag_node_type
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    type(cube_setup_t),           intent(in)    :: cubset
    type(cube_define_t),          intent(in)    :: cubdef
    class(cubedag_node_object_t), intent(inout) :: dno
    type(cubeio_cube_t),          intent(inout) :: cub
    logical,                      intent(inout) :: error
    !
    integer(kind=chan_k) :: nc
    integer(kind=pixe_k) :: nx,ny
    type(cubeio_range_t) :: range
    integer(kind=data_k) :: dim(maxdim)
    character(len=*), parameter :: rname='CREATE>CUBE>DATA'
    !
    if (cub%ready())  return
    !
    ! Sanity
    if (cub%desc%order.eq.code_null) then
      call cubeio_message(seve%e,rname,  &
        'Attempt to write cube data while header is not prepared')
      error = .true.
      return
    endif
    !
    select case (cub%desc%buffered)
    case (code_buffer_none)
      ! No data at all
      cub%file%kind = code_dataformat_none
      call cub%memo%free(error)
      if (error)  return
      !
    case (code_buffer_memory)
      ! Allocate a memory buffer
      call cubeio_message(ioseve%others,rname,'File is buffered in memory')
      cub%file%kind = code_dataformat_none
      call cub%memo%reallocate(cub%desc%iscplx, &
        cub%desc%nx,cub%desc%ny,cub%desc%nc,cub%desc%order,error)
      if (error) return
      !
    case (code_buffer_disk)
      ! Create a file on disk
      call cubeio_cube_create_cube(cub,cubdef,dno,dim,error)
      if (error)  return
      call cubeio_message(ioseve%others,rname,  &
        'File is not buffered in memory, using file '//cub%file%name)
      !
      ! Create dummy data
      if (cub%desc%order.eq.code_cube_imaset) then
        nx = dim(1)  ! Actual size of disk
        ny = dim(2)  ! Actual size of disk
        nc = dim(3)  !
      else
        nc = dim(1)  ! Actual size of disk
        nx = dim(2)  ! Actual size of disk
        ny = dim(3)  !
      endif
      call cub%file%block%reallocatexyc(cub%desc%iscplx,nx,ny,nc,  &
        cub%desc%order,error)
      if (error)  return
      if (cub%desc%iscplx) then
        cub%file%block%c4(:,:,:) = gr4nan  ! ZZZ what is a blank value for C*4?
      else
        cub%file%block%r4(:,:,:) = gr4nan
      endif
      ! Write dummy data, so that the GDF header and the records available on
      ! disk are consistent. Future calls to gdf_extend_image will work even
      ! when extending to 1 i.e. no actual extension.
      range%blc(:) = 0
      range%trc(:) = 0
      if (cub%desc%iscplx) then
        call cubeio_cube_write_data(cub,range,cub%file%block%c4,error)
      else
        call cubeio_cube_write_data(cub,range,cub%file%block%r4,error)
      endif
      if (error)  return
      ! NB: we leave the image opened, so that we can write e.g. one visibility
      ! after the other.
      !
    case default
      call cubeio_message(seve%e,rname,'Unexpected buffering kind')
      error = .true.
      return
    end select
    !
    cub%memo%ready = cub%desc%buffered
  end subroutine cubeio_create_cube_data

  subroutine cubeio_write_any_block(dno,cub,block,error)
    use cubedag_node_type
    !---------------------------------------------------------------------
    ! Write a block to file on disk
    !---------------------------------------------------------------------
    class(cubedag_node_object_t), intent(inout) :: dno
    type(cubeio_cube_t),          intent(inout) :: cub
    type(cubeio_block_t),         intent(inout) :: block
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='WRITE>ANY>BLOCK'
    type(cubeio_range_t) :: range
    logical :: ok
    !
    ! Sanity
    ok = .false.
    if (cub%order().eq.code_cube_imaset .and. block%order.eq.code_cube_imaset)  ok = .true.
    if (cub%order().eq.code_cube_speset .and. block%order.eq.code_cube_speset)  ok = .true.
    if (                                      block%order.eq.code_cube_unkset)  ok = .true.
    if (.not.ok) then
      call cubeio_message(seve%e,rname,'Block and output cube order mismatch')
      error = .true.
      return
    endif
    if (cub%desc%iscplx.neqv.cub%file%block%iscplx) then
      call cubeio_message(seve%e,rname,'Block and output cube type mismatch (R*4/C*4)')
      error = .true.
      return
    endif
    !
    call cubeio_cube_update_header(cub,dno,block%last,error)
    if (error)  return
    !
    ! Write data in the old or extended part
    range%blc(:) = 0
    range%trc(:) = 0
    range%blc(cub%desc%i3) = block%first
    range%trc(cub%desc%i3) = block%last
    if (cub%desc%iscplx) then
      call cubeio_cube_write_data(cub,range,block%c4,error)
    else
      call cubeio_cube_write_data(cub,range,block%r4,error)
    endif
    if (error)  return
    !
    ! Never close the image (GIO slot), the basic assumption is that it
    ! is always opened for reuse at any time.
  end subroutine cubeio_write_any_block

end module cubeio_write
