module cubeio_desc_setup
  use cubetools_parameters
  use cubetools_dataformat_types
  use cubetools_setup_types
  use cubeio_messaging
  use cubeio_header_iodesc
  use cubeio_interface
  use cubeio_cube_define
  use cubeio_cube
  !---------------------------------------------------------------------
  ! Support module to set up a cubeio_desc_t from a cubeio_cube_t
  !---------------------------------------------------------------------

  public :: cubeio_set_descriptor_intrinsic,cubeio_set_descriptor_external
  private

contains

  subroutine cubeio_set_descriptor_intrinsic(dno,cub,error)
    use cubedag_node_type
    !---------------------------------------------------------------------
    ! Fill the cube descriptor from the proper header (intrinsic part)
    ! This also incidently updates the interface_t and header_t
    !---------------------------------------------------------------------
    class(cubedag_node_object_t), intent(inout) :: dno
    type(cubeio_cube_t),          intent(inout) :: cub
    logical,                      intent(inout) :: error
    !
    ! Pre-set the descriptor as unrecognized: it will have some basic
    ! valid values even in case of error during export. Proper values
    ! are re-set during import.
    call cubeio_iodesc_unrecognized(cub%desc,error)
    if (error)  return
    call cubeio_header_get_and_derive(cub,dno,error)
    if (error)  return
    call cubeio_iodesc_import(dno%node%head,cub%desc,error)
    if (error)  return
  end subroutine cubeio_set_descriptor_intrinsic
  !
  subroutine cubeio_set_descriptor_external(cubset,cubdef,old,cub,error)
    !---------------------------------------------------------------------
    ! Fill the cube descriptor from the proper header (external part)
    !---------------------------------------------------------------------
    type(cube_setup_t),  intent(in)    :: cubset
    type(cube_define_t), intent(in)    :: cubdef
    logical,             intent(in)    :: old
    type(cubeio_cube_t), intent(inout) :: cub
    logical,             intent(inout) :: error
    !
    call cubeio_set_access(cubdef,cub,error)
    if (error)  return
    !
    call cubeio_set_filekind(cubdef,cub,error)
    if (error)  return
    !
    call cubeio_set_action(cubdef,old,cub,error)
    if (error)  return
    !
    ! Must come after cubeio_set_filekind
    call cubeio_set_unblank(cubset,cubdef,cub,error)
    if (error)  return
    call cubeio_set_reblank(cubset,cubdef,cub,error)
    if (error)  return
    !
    ! Data buffering kind: auto, disk or memory?
    if (cubdef%dobuffering) then
      call cubeio_set_buffering(cubdef%buffering,cubset%buff%limit,cub,error)
    else
      if (old) then
        call cubeio_set_buffering(cubset%buff%input,cubset%buff%limit,cub,error)
      else
        call cubeio_set_buffering(cubset%buff%output,cubset%buff%limit,cub,error)
      endif
    endif
    if (error)  return
    !
  end subroutine cubeio_set_descriptor_external
  !
  subroutine cubeio_set_access(cubdef,cub,error)
    use cubetools_access_types
    !---------------------------------------------------------------------
    ! Set the cub%desc%access flag according to input code
    !---------------------------------------------------------------------
    type(cube_define_t), intent(in)    :: cubdef  ! Access mode description
    type(cubeio_cube_t), intent(inout) :: cub     !
    logical,             intent(inout) :: error   !
    ! Local
    character(len=*), parameter :: rname='SET>ACCESS'
    !
    if (cubdef%doaccess) then
      select case (cubdef%access)
      case (code_access_imaset_or_speset,code_access_any)
        ! Use access for native order
        cub%desc%access = cubetools_order2access(cub%desc%order)
      case (code_access_imaset,code_access_speset,code_access_subset,code_access_fullset)
        cub%desc%access = cubdef%access
      case default
        call cubeio_message(seve%e,rname,'Access mode not supported')
        error = .true.
        return
      end select
    else
      ! Use access for native order
      cub%desc%access = cubetools_order2access(cub%desc%order)
    endif
    !
  end subroutine cubeio_set_access
  !
  subroutine cubeio_set_filekind(cubdef,cub,error)
    !---------------------------------------------------------------------
    ! Set the cub%file%kind flag according to input code
    !---------------------------------------------------------------------
    type(cube_define_t), intent(in)    :: cubdef  ! Action mode description
    type(cubeio_cube_t), intent(inout) :: cub     !
    logical,             intent(inout) :: error   !
    ! Local
    character(len=*), parameter :: rname='SET>FILEKIND'
    !
    if (cubdef%dofilekind) then
      cub%file%kind = cubdef%filekind
    else
      ! Leave unchanged
      continue
    endif
    !
  end subroutine cubeio_set_filekind
  !
  subroutine cubeio_set_buffering(buffered,limit,cub,error)
    !---------------------------------------------------------------------
    ! Set the cube%desc%buffered flag according to input code
    !---------------------------------------------------------------------
    integer(kind=4),     intent(in)    :: buffered  ! Buffering code
    real(kind=4),        intent(in)    :: limit     ! Limit in case of mode AUTO
    type(cubeio_cube_t), intent(inout) :: cub       !
    logical,             intent(inout) :: error     !
    !
    if (cub%ready() .and. cub%desc%buffered.eq.code_buffer_memory) then
      ! The cube data is already available AND is in memory
      !   => this is an "old" file, leave buffering as is
      ! If disk mode is requested, alternate possibilities are:
      !  - If data is also available on disk => free memory and switch to disk mode
      !  - If data is not available on disk (pure-memory object) => dump to disk,
      !    free memory, and switch to disk mode
      ! Note that this can happen if:
      !  1) Input mode is disk + output mode is memory,
      !  2) Reuse an output as input => should go from memory mode to disk mode...
      continue
    elseif (buffered.eq.code_buffer_auto) then
      if (cub%size().gt.limit) then
        cub%desc%buffered = code_buffer_disk
      else
        cub%desc%buffered = code_buffer_memory
      endif
    else
      cub%desc%buffered = buffered
    endif
    !
  end subroutine cubeio_set_buffering
  !
  subroutine cubeio_set_action(cubdef,old,cub,error)
    !---------------------------------------------------------------------
    ! Set the cub%desc%action flag according to input code
    !---------------------------------------------------------------------
    type(cube_define_t), intent(in)    :: cubdef  ! Action mode description
    logical,             intent(in)    :: old     ! Action on old or new file?
    type(cubeio_cube_t), intent(inout) :: cub     !
    logical,             intent(inout) :: error   !
    !
    character(len=mess_l) :: mess
    logical :: oldok,newok
    character(len=*), parameter :: rname='SET>ACTION'
    !
    if (cubdef%doaction) then
      oldok =      old .and. (cubdef%action.eq.code_read_none .or.  &
                              cubdef%action.eq.code_read_head .or.  &
                              cubdef%action.eq.code_read .or.  &
                              cubdef%action.eq.code_update)
      newok = .not.old .and.  cubdef%action.eq.code_write
      if (oldok.or.newok) then
        cub%desc%action = cubdef%action
      else
        write(mess,'(a,i0)') 'Invalid cube action code ',cubdef%action
        call cubeio_message(seve%e,rname,mess)
        error = .true.
        return
      endif
    else
      if (old) then
        cub%desc%action = code_read  ! Default action is readonly
      else
        cub%desc%action = code_write  ! Action is write
      endif
    endif
    !
  end subroutine cubeio_set_action
  !
  subroutine cubeio_set_unblank(cubset,cubdef,cub,error)
    !---------------------------------------------------------------------
    ! Set the cub%desc%unblank flag according to input code
    !---------------------------------------------------------------------
    type(cube_setup_t),  intent(in)    :: cubset  ! General setup
    type(cube_define_t), intent(in)    :: cubdef  ! Override?
    type(cubeio_cube_t), intent(inout) :: cub     !
    logical,             intent(inout) :: error   !
    ! Local
    character(len=*), parameter :: rname='SET>UNBLANK'
    !
    select case (cub%file%kind)
    case (code_dataformat_fits)
      ! Cube is FITS: nothing to patch if already NaN
      ! No blanking needed, as data is already patched by cubefitsio_image_dataread
      cub%desc%unblank = code_null
      return
    case (code_dataformat_gdf)
      ! Cube is GDF: nothing to patch if no blanking defined
      if (cub%file%hgdf%gil%blan_words.le.0 .or. cub%file%hgdf%gil%eval.lt.0.) then
        cub%desc%unblank = code_null
        return
      endif
    case default
      ! Not relevant if no associated file on disk
      continue
    end select
    !
    if (cubdef%dounblank) then
      ! General setup overriden
      cub%desc%unblank = cubdef%unblank
    else
      ! Use general setup
      cub%desc%unblank = cubset%blanking%rmode
    endif
    !
    if (cub%desc%unblank.eq.code_patchblank_err) then
      ! NB: the other modes are implemented when data are read
      call cubeio_message(seve%e,rname,'Cube has Bval/Eval values')
      call cubeio_message(seve%e,rname,  &
        'Use NAN command to patch them, or change SET\BLANKING mode to ONTHEFLY')
      error = .true.
      return
    endif
    !
  end subroutine cubeio_set_unblank
  !
  subroutine cubeio_set_reblank(cubset,cubdef,cub,error)
    !---------------------------------------------------------------------
    ! Set the cub%desc%reblank flag
    !---------------------------------------------------------------------
    type(cube_setup_t),  intent(in)    :: cubset  ! General setup
    type(cube_define_t), intent(in)    :: cubdef  ! Override?
    type(cubeio_cube_t), intent(inout) :: cub     !
    logical,             intent(inout) :: error   !
    ! Local
    character(len=*), parameter :: rname='SET>REBLANK'
    !
    select case (cub%file%kind)
    case (code_dataformat_fits)
      ! Cube is FITS: re-blanking is not supported/implemented
      cub%desc%reblank = .false.
      return
    case (code_dataformat_gdf)
      ! Cube is GDF
      cub%desc%reblank = cubdef%doreblank
      cub%desc%bval = cubdef%bval
      cub%desc%eval = cubdef%eval
    case default
      ! Not relevant if no associated file on disk
      cub%desc%reblank = .false.
      return
    end select
  end subroutine cubeio_set_reblank
  !
end module cubeio_desc_setup
