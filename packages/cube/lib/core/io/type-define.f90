module cubeio_cube_define
  use cubetools_parameters
  use cubeio_messaging
  !-----------------------------------------------------------------------
  ! Support routines to define a 'cube_define_t'
  !-----------------------------------------------------------------------

  type cube_define_t
    logical               :: dobuffering=.false. ! Supersede default buffering mode?
    integer(kind=4)       :: buffering           ! Buffering mode, if superseded
    logical               :: dofilename=.false.  ! Supersede default file naming?
    character(len=file_l) :: filename            ! Input file name, if superseded
    logical               :: dohdu=.false.       ! Read something else than first HDU?
    integer(kind=4)       :: hdu                 ! Desired HDU
    logical               :: doid=.false.        ! An identifier if provided?
    character(len=file_l) :: id                  ! Identifier to be used in feedback messages
    logical               :: dotransname=.false. !
    character(len=file_l) :: transname           ! Transposed file name
    logical               :: doorder=.false.     ! Order enabled?
    integer(kind=4)       :: order               ! Image set or spectrum set
    logical               :: doaccess=.false.    ! Access enabled?
    integer(kind=4)       :: access              ! Image set, spectrum set, or any
    logical               :: doaction=.false.    ! Action mode enabled?
    integer(kind=4)       :: action              ! Action mode (read, write, update)
    logical               :: dounblank=.false.   ! Unblank mode enabled?
    integer(kind=4)       :: unblank             ! Unblank mode (none, error, patch)
    logical               :: doreblank=.false.   ! Reblank mode enabled?
    real(kind=4)          :: bval,eval           ! Reblank values
    logical               :: dofilekind=.false.  ! Custom format enabled?
    integer(kind=code_k)  :: filekind            ! GDF or FITS format (write mode)
    logical               :: dochecksum=.false.  ! Compute the data checksum?
  contains
    procedure, public :: set_buffering => cubeio_cube_define_buffering
    procedure, public :: set_filename  => cubeio_cube_define_filename
    procedure, public :: set_hdu       => cubeio_cube_define_hdu
    procedure, public :: set_transname => cubeio_cube_define_transname
    procedure, public :: set_order     => cubeio_cube_define_order
    procedure, public :: set_access    => cubeio_cube_define_access
    procedure, public :: set_action    => cubeio_cube_define_action
    procedure, public :: set_unblank   => cubeio_cube_define_unblank
    procedure, public :: set_reblank   => cubeio_cube_define_reblank
    procedure, public :: set_filekind  => cubeio_cube_define_filekind
    procedure, public :: set_id        => cubeio_cube_define_id
    procedure, public :: set_checksum  => cubeio_cube_define_checksum
    ! Special
    procedure, public :: reset => cubeio_cube_define_reset
    procedure, public :: copy  => cubeio_cube_define_copy
  end type cube_define_t

  public :: cube_define_t
  private

contains

  subroutine cubeio_cube_define_reset(cubdef,error)
    !---------------------------------------------------------------------
    !---------------------------------------------------------------------
    class(cube_define_t), intent(out)   :: cubdef
    logical,              intent(inout) :: error
    !
    ! cubdef is reset thanks to intent(out), nothing more to do.
    continue
  end subroutine cubeio_cube_define_reset
  !
  subroutine cubeio_cube_define_buffering(cubdef,mode,error)
    !---------------------------------------------------------------------
    !---------------------------------------------------------------------
    class(cube_define_t), intent(inout) :: cubdef
    integer(kind=4),      intent(in)    :: mode
    logical,              intent(inout) :: error
    !
    ! ZZZ add sanity check
    cubdef%dobuffering = .true.
    cubdef%buffering = mode
  end subroutine cubeio_cube_define_buffering
  !
  subroutine cubeio_cube_define_filekind(cubdef,filekind,error)
    !---------------------------------------------------------------------
    ! Define the file kind (GDF, FITS,...) to be used if the cube is
    ! associated to a file on DISK.
    !---------------------------------------------------------------------
    class(cube_define_t),  intent(inout) :: cubdef
    integer(kind=code_k),  intent(in)    :: filekind
    logical,               intent(inout) :: error
    !
    ! ZZZ add sanity check
    cubdef%dofilekind = .true.
    cubdef%filekind = filekind
  end subroutine cubeio_cube_define_filekind
  !
  subroutine cubeio_cube_define_filename(cubdef,filename,error)
    !---------------------------------------------------------------------
    !---------------------------------------------------------------------
    class(cube_define_t), intent(inout) :: cubdef
    character(len=*),     intent(in)    :: filename
    logical,              intent(inout) :: error
    !
    cubdef%dofilename = .true.
    cubdef%filename = filename
  end subroutine cubeio_cube_define_filename
  !
  subroutine cubeio_cube_define_hdu(cubdef,hdu,error)
    !---------------------------------------------------------------------
    !---------------------------------------------------------------------
    class(cube_define_t), intent(inout) :: cubdef
    integer(kind=4),      intent(in)    :: hdu
    logical,              intent(inout) :: error
    !
    cubdef%dohdu = .true.
    cubdef%hdu = hdu
  end subroutine cubeio_cube_define_hdu
  !
  subroutine cubeio_cube_define_id(cubdef,id,error)
    !---------------------------------------------------------------------
    !---------------------------------------------------------------------
    class(cube_define_t), intent(inout) :: cubdef
    character(len=*),     intent(in)    :: id
    logical,              intent(inout) :: error
    !
    cubdef%doid = .true.
    cubdef%id = id
  end subroutine cubeio_cube_define_id
  !
  subroutine cubeio_cube_define_transname(cubdef,transname,error)
    !---------------------------------------------------------------------
    !---------------------------------------------------------------------
    class(cube_define_t), intent(inout) :: cubdef
    character(len=*),     intent(in)    :: transname
    logical,              intent(inout) :: error
    !
    cubdef%dotransname = .true.
    cubdef%transname = transname
  end subroutine cubeio_cube_define_transname
  !
  subroutine cubeio_cube_define_order(cubdef,order,error)
    !---------------------------------------------------------------------
    !---------------------------------------------------------------------
    class(cube_define_t), intent(inout) :: cubdef
    integer(kind=4),      intent(in)    :: order   ! code_cube_*
    logical,              intent(inout) :: error
    !
    ! ZZZ add sanity check
    cubdef%doorder = .true.
    cubdef%order = order
  end subroutine cubeio_cube_define_order
  !
  subroutine cubeio_cube_define_access(cubdef,access,error)
    !---------------------------------------------------------------------
    !---------------------------------------------------------------------
    class(cube_define_t), intent(inout) :: cubdef
    integer(kind=4),      intent(in)    :: access  ! code_access_*
    logical,              intent(inout) :: error
    !
    ! ZZZ add sanity check
    if (cubdef%doaccess) then
      ! A previous access was requested. Most likely a cube appears
      ! in 2 arguments on the command line. Tolerate this, as long
      ! as the requests are compatible.
      if (access.eq.code_access_any) then
        ! New requested access is any => leave unchanged
      elseif (cubdef%access.eq.code_access_any) then
        ! Previous requested access was any => use new request
        cubdef%access = access
      else
        ! Overwrite, with probably later troubles
        cubdef%access = access
      endif
    else
      cubdef%doaccess = .true.
      cubdef%access = access
    endif
  end subroutine cubeio_cube_define_access
  !
  subroutine cubeio_cube_define_action(cubdef,action,error)
    !---------------------------------------------------------------------
    !---------------------------------------------------------------------
    class(cube_define_t), intent(inout) :: cubdef
    integer(kind=4),      intent(in)    :: action
    logical,              intent(inout) :: error
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='DEFINE>ACTION'
    !
    ! ZZZ add sanity check
    if (cubdef%doaction) then
      ! A previous action was requested. Most likely a cube appears
      ! in 2 arguments on the command line. Tolerate this, as long
      ! as the requests are compatible.
      !
      ! Sanity: reject mixing read action (codes 1*) and write action (codes 2*)
      if (cubdef%action/10.ne.action/10) then
        write(mess,'(3(a,i0))')  &
          'Invalid attempt to mix read and write actions (old code ', &
          cubdef%action,', new code ',action,')'
        call cubeio_message(seve%e,rname,mess)
        error = .true.
        return
      endif
      if (action.gt.cubdef%action)  cubdef%action = action
    else
      cubdef%doaction = .true.
      cubdef%action = action
    endif
  end subroutine cubeio_cube_define_action
  !
  subroutine cubeio_cube_define_unblank(cubdef,unblank,error)
    !---------------------------------------------------------------------
    !---------------------------------------------------------------------
    class(cube_define_t), intent(inout) :: cubdef
    integer(kind=4),      intent(in)    :: unblank
    logical,              intent(inout) :: error
    !
    ! ZZZ add sanity check
    cubdef%dounblank = .true.
    cubdef%unblank = unblank
  end subroutine cubeio_cube_define_unblank
  !
  subroutine cubeio_cube_define_reblank(cubdef,bval,eval,error)
    !---------------------------------------------------------------------
    !---------------------------------------------------------------------
    class(cube_define_t), intent(inout) :: cubdef
    real(kind=4),         intent(in)    :: bval,eval
    logical,              intent(inout) :: error
    !
    ! ZZZ add sanity check
    cubdef%doreblank = .true.
    cubdef%bval = bval
    cubdef%eval = eval
  end subroutine cubeio_cube_define_reblank
  !
  subroutine cubeio_cube_define_checksum(cubdef,dochecksum,error)
    !---------------------------------------------------------------------
    !---------------------------------------------------------------------
    class(cube_define_t), intent(inout) :: cubdef
    logical,              intent(in)    :: dochecksum  ! Could be improved as a code none, sha1, etc
    logical,              intent(inout) :: error
    !
    cubdef%dochecksum = dochecksum
  end subroutine cubeio_cube_define_checksum
  !
  subroutine cubeio_cube_define_copy(in,out)
    !---------------------------------------------------------------------
    ! Duplicate a cube_define_t
    !---------------------------------------------------------------------
    class(cube_define_t), intent(in)  :: in
    type(cube_define_t),  intent(out) :: out
    !
    out = in  ! As of today, simple copy is enough
  end subroutine cubeio_cube_define_copy
end module cubeio_cube_define
