module cubeio_flush
  use cubetools_setup_types
  use cubedag_node_type
  use cubeio_messaging
  use cubeio_block
  use cubeio_cube
  use cubeio_read
  use cubeio_write

  public :: cubeio_check_output_chan_block,cubeio_check_input_chan_block
  public :: cubeio_check_output_pix_block,cubeio_check_input_pix_block
  public :: cubeio_check_output_any_block,cubeio_check_input_any_block
  public :: cubeio_flush_block,cubeio_flush_any_block
  private

contains

  subroutine cubeio_check_input_chan_block(cubset,dno,cub,first,last,error)
    !---------------------------------------------------------------------
    ! Check if the current block buffer provides the fichan:lichan channel
    ! range. If not, flush the previous contents and load proper one.
    !---------------------------------------------------------------------
    type(cube_setup_t),           intent(in)    :: cubset
    class(cubedag_node_object_t), intent(inout) :: dno
    type(cubeio_cube_t),          intent(inout) :: cub
    integer(kind=chan_k),         intent(in)    :: first
    integer(kind=chan_k),         intent(in)    :: last
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='CHECK>INPUT>CHAN>BLOCK'
    character(len=message_length) :: mess
    integer(kind=chan_k) :: nchanperblock,fichan,lichan
    !
    fichan = cub%chan_num(first)
    lichan = cub%chan_num(last)
    !
    ! ZZZ Check sanity of cub%file%block => should be LMV from previous calls
    ! ZZZ Assume fichan.le.lichan
    if (fichan.ge.cub%file%block%first .and. lichan.le.cub%file%block%last)  return
    !
    ! Before flushing the current buffer, ensure all the tasks are finished:
    !$OMP TASKWAIT
    !
    if (fichan.ne.cub%file%block%last+1) then
      ! This means the channel buffers are not read contiguously, in order.
      ! One of the risk is a possible overlap of the new buffer with
      ! data already read from disk.
      if (cub%file%block%last.ne.0)  &  ! No warning if block was not yet used
        call cubeio_message(ioseve%others,rname,'Non-contiguous input buffer might be inefficient')
    endif
    !
    ! Need to buffer another region. Flush current contents first if it has
    ! been modified
    call cubeio_flush_any_block(dno,cub,cub%file%block,error)
    if (error)  return
    !
    call cub%chan_per_block(cubset%buff%block,'SET\BUFFER /BLOCK',nchanperblock,error)
    if (error)  return
    if (nchanperblock.lt.lichan-fichan+1) then
      call cubeio_message(seve%e,rname,  &
        'SET\BUFFERING /TASK must be smaller than SET\BUFFERING /BLOCK')
      ! It should even be MUCH larger than the buffer block, so that several ranges
      ! can be processed in parallel by several parallelisation tasks.
      error = .true.
      return
    endif
    call cub%file%block%reallocatexyc(cub%desc%iscplx,  &
      cub%desc%nx,cub%desc%ny,nchanperblock,code_cube_imaset,error)
    if (error)  return
    cub%file%block%first = fichan
    cub%file%block%last  = min(fichan+nchanperblock-1,cub%desc%nc)
    write(mess,'(2(A,I0))')  &
      'Buffering input channel block from ',cub%file%block%first,' to ',cub%file%block%last
    call cubeio_message(ioseve%others,rname,mess)
    call cubeio_read_chan_block(cubset,cub,cub%file%block,error)
    if (error)  return
    ! At start the buffer is read-only. Note that this may change in a sequence
    ! read_chan(1), write_chan(1), read_chan(2), write_chan(2), ... Do not reset
    ! to .false. afterwards
    cub%file%block%readwrite = .false.
    !
  end subroutine cubeio_check_input_chan_block
  !
  subroutine cubeio_check_input_pix_block(cubset,dno,cub,first,last,error)
    !---------------------------------------------------------------------
    ! Check if the current block buffer provides the fypix:lypix Y pixel
    ! row range. If not, flush the previous contents and load proper one.
    !---------------------------------------------------------------------
    type(cube_setup_t),           intent(in)    :: cubset
    class(cubedag_node_object_t), intent(inout) :: dno
    type(cubeio_cube_t),          intent(inout) :: cub
    integer(kind=pixe_k),         intent(in)    :: first
    integer(kind=pixe_k),         intent(in)    :: last
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='CHECK>INPUT>PIX>BLOCK'
    integer(kind=pixe_k) :: nyperblock,fypix,lypix
    character(len=message_length) :: mess
    !
    fypix = cub%y_num(first)
    lypix = cub%y_num(last)
    !
    ! ZZZ Check sanity of cub%file%block => should be VLM from previous calls
    ! ZZZ Assume fypix.le.lypix
    if (fypix.ge.cub%file%block%first .and. lypix.le.cub%file%block%last)  return
    !
    ! Before flushing the current buffer, ensure all the tasks are finished:
    !$OMP TASKWAIT
    !
    if (fypix.ne.cub%file%block%last+1) then
      ! This means the Y row buffers are not read contiguously, in order.
      ! One of the risk is a possible overlap of the new buffer with
      ! data already read from disk.
      if (cub%file%block%last.ne.0)  &  ! No warning if block was not yet used
        call cubeio_message(ioseve%others,rname,'Non-contiguous input buffer might be inefficient')
    endif
    !
    ! Need to buffer another region. Flush current contents first if it has
    ! been modified
    call cubeio_flush_any_block(dno,cub,cub%file%block,error)
    if (error)  return
    !
    call cub%y_per_block(cubset%buff%block,'SET\BUFFER /BLOCK',nyperblock,error)
    if (error)  return
    if (nyperblock.lt.lypix-fypix+1) then
      call cubeio_message(seve%e,rname,  &
        'SET\BUFFERING /TASK must be smaller than SET\BUFFERING /BLOCK')
      ! It should even be MUCH larger than the buffer block, so that several ranges
      ! can be processed in parallel by several parallelisation tasks.
      error = .true.
      return
    endif
    call cub%file%block%reallocatexyc(cub%desc%iscplx,  &
      cub%desc%nx,nyperblock,cub%desc%nc,code_cube_speset,error)
    if (error)  return
    cub%file%block%first = fypix
    cub%file%block%last  = min(fypix+nyperblock-1,cub%desc%ny)
    write(mess,'(2(A,I0))')  &
      'Buffering input Y row block from ',cub%file%block%first,' to ',cub%file%block%last
    call cubeio_message(ioseve%others,rname,mess)
    call cubeio_read_y_block(cubset,cub,cub%file%block,error)
    if (error)  return
    ! At start the buffer is read-only. Note that this may change in a sequence
    ! read_pix(1), write_pix(1), read_pix(2), write_pix(2), ... Do not reset
    ! to .false. afterwards
    cub%file%block%readwrite = .false.
    !
  end subroutine cubeio_check_input_pix_block
  !
  subroutine cubeio_check_input_any_block(cubset,dno,cub,first,last,error)
    !---------------------------------------------------------------------
    ! Check if the current block buffer provides the fi3:li3 data
    ! range. If not, flush the previous contents and load proper one.
    !---------------------------------------------------------------------
    type(cube_setup_t),           intent(in)    :: cubset
    class(cubedag_node_object_t), intent(inout) :: dno
    type(cubeio_cube_t),          intent(inout) :: cub
    integer(kind=data_k),         intent(in)    :: first
    integer(kind=data_k),         intent(in)    :: last
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='CHECK>INPUT>ANY>BLOCK'
    character(len=message_length) :: mess
    integer(kind=data_k) :: n3perblock,fi3,li3,n3
    !
    fi3 = cub%plane_num(first)
    li3 = cub%plane_num(last)
    ! ZZZ Assume fi3.le.li3
    !
    if (fi3.ge.cub%file%block%first .and. li3.le.cub%file%block%last)  return
    !
    ! Before flushing the current buffer, ensure all the tasks are finished:
    !$OMP TASKWAIT
    !
    ! During the iteration, it is valid to request planes beyond
    ! the cube (e.g. surset extraction with EXTRACT). Deal with this:
    n3 = cub%desc%n3
    if (fi3.gt.n3 .or. li3.lt.1) then
      ! The range is fully off the cube. Free the block buffer and assume
      ! the caller will not use it.
      call cubeio_flush_any_block(dno,cub,cub%file%block,error)
      if (error)  return
      call cub%file%block%free(error)
      if (error)  return
      return
    endif
    if ((fi3.lt.1  .and. li3.ge.1) .or. &
        (fi3.le.n3 .and. li3.gt.n3)) then
      ! The range overlaps the cube boundaries. Solution?
      ! 1) Build a buffer with expected number of planes, this
      !   requires allocating a dedicated data array (instead of
      !   usual pointer, hence inefficient), and put NaN or valid
      !   values where relevant => too complicated.
      ! 2) Return a buffer with less planes (only the valid ones
      !    from the input cube => this breaks the rule to provide
      !    the requested range
      ! => Rejected!
      call cubeio_message(seve%e,rname,  &
        'Internal error: the input range overlaps the cube boundaries')
      error = .true.
      return
    endif
    !
    if (fi3.ne.cub%file%block%last+1) then
      ! This means the channel buffers are not read contiguously, in order.
      ! One of the risk is a possible overlap of the new buffer with
      ! data already read from disk.
      ! Under normal conditions, the task loop iterator is expected to
      ! properly split the accesses to the file.
      if (cub%file%block%last.ne.0)  &  ! No warning if block was not yet used
        call cubeio_message(ioseve%others,rname,'Non-contiguous input buffer might be inefficient')
    endif
    !
    ! Need to buffer another region. Flush current contents first if it has
    ! been modified
    call cubeio_flush_any_block(dno,cub,cub%file%block,error)
    if (error)  return
    !
    call cub%plane_per_block(cubset%buff%block,'SET\BUFFER /BLOCK',n3perblock,error)
    if (error)  return
    if (n3perblock.lt.li3-fi3+1) then
      call cubeio_message(seve%e,rname,  &
        'SET\BUFFERING /TASK must be smaller than SET\BUFFERING /BLOCK')
      ! It should even be MUCH larger than the buffer block, so that several ranges
      ! can be processed in parallel by several parallelisation tasks.
      error = .true.
      return
    endif
    call cub%file%block%reallocate123(cub%desc%iscplx,  &
      cub%desc%n1,cub%desc%n2,n3perblock,code_cube_unkset,error)
    if (error)  return
    cub%file%block%first = fi3
    cub%file%block%last  = min(fi3+n3perblock-1,cub%desc%n3)
    write(mess,'(2(A,I0))')  &
      'Buffering input channel block from ',cub%file%block%first,' to ',cub%file%block%last
    call cubeio_message(ioseve%others,rname,mess)
    call cubeio_read_any_block(cubset,cub,cub%file%block,error)
    if (error)  return
    ! At start the buffer is read-only. Note that this may change in a sequence
    ! read_any(1), write_any(1), read_any(2), write_any(2), ... Do not reset
    ! to .false. afterwards
    cub%file%block%readwrite = .false.
    !
  end subroutine cubeio_check_input_any_block

  subroutine cubeio_check_output_chan_block(cubset,dno,cub,fochan,lochan,error)
    !---------------------------------------------------------------------
    ! Check if the current block buffer provides the fochan:lochan channel
    ! range. If not, flush the previous contents and load proper one.
    !---------------------------------------------------------------------
    type(cube_setup_t),           intent(in)    :: cubset
    class(cubedag_node_object_t), intent(inout) :: dno
    type(cubeio_cube_t),          intent(inout) :: cub
    integer(kind=chan_k),         intent(in)    :: fochan
    integer(kind=chan_k),         intent(in)    :: lochan
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='CHECK>OUTPUT>CHAN>BLOCK'
    character(len=message_length) :: mess
    integer(kind=chan_k) :: nchanperblock
    !
    ! ZZZ Check sanity of cub%file%block => should be LMV from previous calls
    ! ZZZ Assume fochan.le.lochan
    if (fochan.ge.cub%file%block%first .and. lochan.le.cub%file%block%last)  return
    !
    ! Before flushing the current buffer, ensure all the tasks are finished:
    !$OMP TASKWAIT
    !
    if (fochan.ne.cub%file%block%last+1) then
      ! This means the channel buffers are not written contiguously, in order.
      ! One of the major risk is a possible overlap of the new buffer with
      ! data already flushed on disk. This would require re-loading what is
      ! already written. BUT, on the other hand, this is not a problem if this
      ! is a region which was not used in first buffer, but will only be filled
      ! by the new one.
      call cubeio_message(seve%w,rname,'Non-contiguous output buffer might be inefficient')
      write(mess,'(4(A,I0))')  &
        'Previous range: ',cub%file%block%first,'-',cub%file%block%last,  &
        ', new range: ',fochan,'-',lochan
      call cubeio_message(ioseve%others,rname,mess)
    endif
    !
    ! Need to buffer another region. Flush current contents first
    call cubeio_flush_any_block(dno,cub,cub%file%block,error)
    if (error)  return
    !
    call cub%chan_per_block(cubset%buff%block,'SET\BUFFER /BLOCK',nchanperblock,error)
    if (error)  return
    if (nchanperblock.lt.lochan-fochan+1) then
      call cubeio_message(seve%e,rname,  &
        'SET\BUFFERING /TASK must be smaller than SET\BUFFERING /BLOCK')
      ! It should even be MUCH larger than the buffer block, so that several ranges
      ! can be processed in parallel by several parallelisation tasks.
      error = .true.
      return
    endif
    call cub%file%block%reallocatexyc(cub%desc%iscplx,  &
      cub%desc%nx,cub%desc%ny,nchanperblock,code_cube_imaset,error)
    if (error)  return
    cub%file%block%first = fochan
    cub%file%block%last  = min(fochan+nchanperblock-1,cub%desc%nc)
    write(mess,'(2(A,I0))')  &
      'Buffering output channel block from ',cub%file%block%first,' to ',cub%file%block%last
    call cubeio_message(ioseve%others,rname,mess)
    !
  end subroutine cubeio_check_output_chan_block
  !
  subroutine cubeio_check_output_pix_block(cubset,dno,cub,fypix,lypix,error)
    !---------------------------------------------------------------------
    ! Check if the current block buffer provides the fypix:lypix Y pixel
    ! range. If not, flush the previous contents and load proper one.
    !---------------------------------------------------------------------
    type(cube_setup_t),           intent(in)    :: cubset
    class(cubedag_node_object_t), intent(inout) :: dno
    type(cubeio_cube_t),          intent(inout) :: cub
    integer(kind=pixe_k),         intent(in)    :: fypix
    integer(kind=pixe_k),         intent(in)    :: lypix
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='CHECK>OUTPUT>PIX>BLOCK'
    character(len=message_length) :: mess
    integer(kind=pixe_k) :: nyperblock
    !
    ! ZZZ Check sanity of cub%file%block => should be VLM from previous calls
    ! ZZZ Assume fypix.le.lypix
    if (fypix.ge.cub%file%block%first .and. lypix.le.cub%file%block%last)  return
    !
    ! Before flushing the current buffer, ensure all the tasks are finished:
    !$OMP TASKWAIT
    !
    if (fypix.ne.cub%file%block%last+1) then
      ! This means the Y row buffers are not written contiguously, in order.
      ! One of the major risk is a possible overlap of the new buffer with
      ! data already flushed on disk. This would require re-loading what is
      ! already written. BUT, on the other hand, this is not a problem if this
      ! is a region which was not used in first buffer, but will only be filled
      ! by the new one.
      call cubeio_message(seve%w,rname,'Non-contiguous output buffer might be inefficient')
      write(mess,'(4(A,I0))')  &
        'Previous range: ',cub%file%block%first,'-',cub%file%block%last,  &
        ', new range: ',fypix,'-',lypix
      call cubeio_message(ioseve%others,rname,mess)
    endif
    !
    ! Need to buffer another region. Flush current contents first
    call cubeio_flush_any_block(dno,cub,cub%file%block,error)
    if (error)  return
    !
    call cub%y_per_block(cubset%buff%block,'SET\BUFFER /BLOCK',nyperblock,error)
    if (error)  return
    if (nyperblock.lt.lypix-fypix+1) then
      call cubeio_message(seve%e,rname,  &
        'SET\BUFFERING /TASK must be smaller than SET\BUFFERING /BLOCK')
      ! It should even be MUCH larger than the buffer block, so that several ranges
      ! can be processed in parallel by several parallelisation tasks.
      error = .true.
      return
    endif
    call cub%file%block%reallocatexyc(cub%desc%iscplx,  &
      cub%desc%nx,nyperblock,cub%desc%nc,code_cube_speset,error)
    if (error)  return
    cub%file%block%first = fypix
    cub%file%block%last  = min(fypix+nyperblock-1,cub%desc%ny)
    write(mess,'(2(A,I0))')  &
      'Buffering output Y row block from ',cub%file%block%first,' to ',cub%file%block%last
    call cubeio_message(ioseve%others,rname,mess)
    !
  end subroutine cubeio_check_output_pix_block
  !
  subroutine cubeio_check_output_any_block(cubset,dno,cub,fo3,lo3,error)
    !---------------------------------------------------------------------
    ! Check if the current block buffer provides the fo3:lo3 data
    ! range. If not, flush the previous contents and load proper one.
    !---------------------------------------------------------------------
    type(cube_setup_t),           intent(in)    :: cubset
    class(cubedag_node_object_t), intent(inout) :: dno
    type(cubeio_cube_t),          intent(inout) :: cub
    integer(kind=data_k),         intent(in)    :: fo3
    integer(kind=data_k),         intent(in)    :: lo3
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='CHECK>OUTPUT>ANY>BLOCK'
    character(len=message_length) :: mess
    integer(kind=data_k) :: n3perblock
    !
    ! ZZZ Assume fo3.le.lo3
    if (fo3.ge.cub%file%block%first .and. lo3.le.cub%file%block%last)  return
    !
    ! Before flushing the current buffer, ensure all the tasks are finished:
    !$OMP TASKWAIT
    !
    if (fo3.ne.cub%file%block%last+1) then
      ! This means the plane buffers are not written contiguously, in order.
      ! One of the major risk is a possible overlap of the new buffer with
      ! data already flushed on disk. This would require re-loading what is
      ! already written. BUT, on the other hand, this is not a problem if this
      ! is a region which was not used in first buffer, but will only be filled
      ! by the new one.
      ! Under normal conditions, the task loop iterator is expected to
      ! properly split the accesses to the file.
      call cubeio_message(seve%w,rname,'Non-contiguous output buffer might be inefficient')
      write(mess,'(4(A,I0))')  &
        'Previous range: ',cub%file%block%first,'-',cub%file%block%last,  &
        ', new range: ',fo3,'-',lo3
      call cubeio_message(ioseve%others,rname,mess)
    endif
    !
    ! Need to buffer another region. Flush current contents first
    call cubeio_flush_any_block(dno,cub,cub%file%block,error)
    if (error)  return
    !
    call cub%plane_per_block(cubset%buff%block,'SET\BUFFER /BLOCK',n3perblock,error)
    if (error)  return
    if (n3perblock.lt.lo3-fo3+1) then
      call cubeio_message(seve%e,rname,  &
        'SET\BUFFERING /TASK must be smaller than SET\BUFFERING /BLOCK')
      ! It should even be MUCH larger than the buffer block, so that several ranges
      ! can be processed in parallel by several parallelisation tasks.
      error = .true.
      return
    endif
    call cub%file%block%reallocate123(cub%desc%iscplx,  &
      cub%desc%n1,cub%desc%n2,n3perblock,code_cube_unkset,error)
    if (error)  return
    cub%file%block%first = fo3
    cub%file%block%last  = min(fo3+n3perblock-1,cub%desc%n3)
    write(mess,'(2(A,I0))')  &
      'Buffering output subcube block from ',cub%file%block%first,' to ',cub%file%block%last
    call cubeio_message(ioseve%others,rname,mess)
    !
  end subroutine cubeio_check_output_any_block
  !
  subroutine cubeio_flush_block(dno,cub,block,error)
    !---------------------------------------------------------------------
    ! Flush IF RELEVANT a block buffer to file on disk
    !---------------------------------------------------------------------
    class(cubedag_node_object_t), intent(inout) :: dno
    type(cubeio_cube_t),          intent(inout) :: cub
    type(cubeio_block_t),         intent(inout) :: block
    logical,                      intent(inout) :: error
    !
    if (cub%desc%buffered.eq.code_buffer_disk) then
      call cubeio_flush_any_block(dno,cub,cub%file%block,error)
      if (error)  return
    endif
  end subroutine cubeio_flush_block
  !
  subroutine cubeio_flush_any_block(dno,cub,block,error)
    !---------------------------------------------------------------------
    ! Flush IF RELEVANT a block of planes to file on disk
    !---------------------------------------------------------------------
    class(cubedag_node_object_t), intent(inout) :: dno
    type(cubeio_cube_t),          intent(inout) :: cub
    type(cubeio_block_t),         intent(inout) :: block
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='FLUSH>ANY>BLOCK'
    character(len=message_length) :: mess
    character(len=8) :: what
    !
    if (block%first.le.0)      return  ! This buffer was never used
    if (.not.block%readwrite)  return  ! Read-only => nothing to write or update
    !
    select case (block%order)
    case (code_cube_imaset)
      what = 'channel'
    case (code_cube_speset)
      what = 'Y row'
    case (code_cube_unkset)
      what = 'subcube'
    case default
      what = '???'
    end select
    write(mess,'(A,A,A,I0,A,I0)')  &
      'Flushing ',trim(what),' block from ',block%first,' to ',block%last
    call cubeio_message(ioseve%others,rname,mess)
    !
    call cubeio_write_any_block(dno,cub,block,error)
    if (error)  return
    !
    ! Was flushed => set as read-only so that it is not flushed again
    block%readwrite = .false.
  end subroutine cubeio_flush_any_block

end module cubeio_flush
