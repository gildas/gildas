!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeio_cube
  use cubetools_parameters
  use cubetools_format
  use cubetools_setup_types
  use cubeio_messaging
  use cubeio_desc
  use cubeio_memory
  use cubeio_file
  use cubeio_timing
  !
  type :: cubeio_cube_t
    type(cubeio_desc_t)          :: desc  ! [public]  User friendly cube description
    type(cubeio_memory_t)        :: memo  ! [private] Data placeholder (memory mode)
    type(cubeio_file_t), pointer :: file  ! [private] Support for file on disk (disk mode)
    type(cubeio_time_t)          :: time  ! [private] time measurements
  contains
    procedure         :: order           => cubeio_get_order
    procedure         :: access          => cubeio_get_access
    procedure         :: iscplx          => cubeio_get_iscplx
    procedure         :: haskind         => cubeio_has_filekind
    procedure         :: nbytes          => cubeio_get_nbytes
    procedure         :: nentry          => cubeio_get_nentry
    procedure         :: ndata           => cubeio_get_ndata
    procedure         :: size            => cubeio_get_size
    procedure         :: planesize       => cubeio_get_planesize
    procedure         :: chansize        => cubeio_get_chansize
    procedure         :: ysize           => cubeio_get_ysize
    procedure, public :: plane_per_block => cubeio_plane_per_block
    procedure, public :: chan_per_block  => cubeio_chan_per_block
    procedure, public :: y_per_block     => cubeio_y_per_block
    procedure, public :: plane_num       => cubeio_plane_number
    procedure, public :: chan_num        => cubeio_chan_number
    procedure, public :: x_num           => cubeio_xpix_number
    procedure, public :: y_num           => cubeio_ypix_number
    procedure         :: memsize         => cubeio_get_memsize
    procedure         :: ready           => cubeio_data_ready
    procedure         :: open            => cubeio_cube_open
    procedure         :: close           => cubeio_cube_close
    procedure         :: init            => cubeio_cube_init
    procedure         :: free            => cubeio_cube_free
    procedure         :: feedback        => cubeio_cube_feedback
  ! final             :: cubeio_cube_final => crash if implicit (order matters?)
  end type cubeio_cube_t
  !
  public :: cubeio_cube_t
  public :: cubeio_cube_final
  private
  !
contains
  !
  function cubeio_get_order(cub)
    integer(kind=code_k) :: cubeio_get_order
    class(cubeio_cube_t), intent(in) :: cub
    cubeio_get_order = cub%desc%order
  end function cubeio_get_order
  !
  function cubeio_get_access(cub)
    integer(kind=code_k) :: cubeio_get_access
    class(cubeio_cube_t), intent(in) :: cub
    cubeio_get_access = cub%desc%access
  end function cubeio_get_access
  !
  function cubeio_get_iscplx(cub)
    logical :: cubeio_get_iscplx
    class(cubeio_cube_t), intent(in) :: cub
    cubeio_get_iscplx = cub%desc%iscplx
  end function cubeio_get_iscplx
  !
  function cubeio_has_filekind(cub,code_filekind)
    !-------------------------------------------------------------------
    ! Return .true. if the cubeio_cube_t provides the requested kind of
    ! file
    !-------------------------------------------------------------------
    logical :: cubeio_has_filekind
    class(cubeio_cube_t), intent(in) :: cub
    integer(kind=code_k), intent(in) :: code_filekind
    !
    ! The cube can be in memory and/or disk for this cubeio_cube_t.
    ! Request is to know if the given file kind is available on disk.
    cubeio_has_filekind = cub%file%iskind(code_filekind)
  end function cubeio_has_filekind
  !
  function cubeio_get_nbytes(cub)
    integer(kind=4) :: cubeio_get_nbytes
    class(cubeio_cube_t), intent(in) :: cub
    if (cub%iscplx()) then
      cubeio_get_nbytes = 8
    else
      cubeio_get_nbytes = 4
    endif
  end function cubeio_get_nbytes
  !
  function cubeio_get_nentry(cub)
    integer(kind=entr_k) :: cubeio_get_nentry
    class(cubeio_cube_t), intent(in) :: cub
    select case (cub%desc%access)
    case (code_access_imaset)
      cubeio_get_nentry = cub%desc%nc
    case (code_access_speset)
      cubeio_get_nentry = cub%desc%nx*cub%desc%ny
    case (code_access_subset)
      cubeio_get_nentry = cub%desc%n3
    case (code_access_fullset)
      cubeio_get_nentry = cub%desc%n3
    case default
      cubeio_get_nentry = 0
    end select
  end function cubeio_get_nentry
  !
  function cubeio_get_ndata(cub)
    !-------------------------------------------------------------------
    ! Return the number of data values
    !-------------------------------------------------------------------
    integer(kind=data_k) :: cubeio_get_ndata
    class(cubeio_cube_t), intent(in) :: cub
    cubeio_get_ndata = cub%desc%n1 * cub%desc%n2 * cub%desc%n3
  end function cubeio_get_ndata
  !
  function cubeio_get_size(cub)
    !-------------------------------------------------------------------
    ! Return the cube data size in bytes (allocated or not)
    !-------------------------------------------------------------------
    integer(kind=data_k) :: cubeio_get_size
    class(cubeio_cube_t), intent(in) :: cub
    cubeio_get_size = cub%ndata()
    if (cub%desc%iscplx) then
      cubeio_get_size = cubeio_get_size*8
    else
      cubeio_get_size = cubeio_get_size*4
    endif
  end function cubeio_get_size
  !
  function cubeio_get_planesize(cub)
    !-------------------------------------------------------------------
    ! Return the one plane (1st x 2nd dimension) size in bytes
    !-------------------------------------------------------------------
    integer(kind=size_length) :: cubeio_get_planesize
    class(cubeio_cube_t), intent(in) :: cub
    cubeio_get_planesize = cub%desc%n1 * cub%desc%n2
    if (cub%desc%iscplx) then
      cubeio_get_planesize = cubeio_get_planesize*8
    else
      cubeio_get_planesize = cubeio_get_planesize*4
    endif
  end function cubeio_get_planesize
  !
  function cubeio_get_chansize(cub)
    !-------------------------------------------------------------------
    ! Return the one channel (2D plane) size in bytes
    !-------------------------------------------------------------------
    integer(kind=size_length) :: cubeio_get_chansize
    class(cubeio_cube_t), intent(in) :: cub
    cubeio_get_chansize = cub%desc%nx * cub%desc%ny
    if (cub%desc%iscplx) then
      cubeio_get_chansize = cubeio_get_chansize*8
    else
      cubeio_get_chansize = cubeio_get_chansize*4
    endif
  end function cubeio_get_chansize
  !
  function cubeio_get_ysize(cub)
    !-------------------------------------------------------------------
    ! Return the one Y row (2D plane) size in bytes
    !-------------------------------------------------------------------
    integer(kind=size_length) :: cubeio_get_ysize
    class(cubeio_cube_t), intent(in) :: cub
    cubeio_get_ysize = cub%desc%nx * cub%desc%nc
    if (cub%desc%iscplx) then
      cubeio_get_ysize = cubeio_get_ysize*8
    else
      cubeio_get_ysize = cubeio_get_ysize*4
    endif
  end function cubeio_get_ysize
  !
  function cubeio_get_memsize(cub)
    !-------------------------------------------------------------------
    ! Return the memory footprint in bytes
    !-------------------------------------------------------------------
    integer(kind=size_length) :: cubeio_get_memsize
    class(cubeio_cube_t), intent(in) :: cub
    !
    cubeio_get_memsize =  &
      cub%file%memsize() +  &
      cub%memo%memsize()
  end function cubeio_get_memsize
  !
  subroutine cubeio_chan_per_block(cub,blocksize,blockname,nchan,error)
    !---------------------------------------------------------------------
    ! Return the maximum number of channels which can be used in a
    ! cube_block_t for cube given as argument
    !---------------------------------------------------------------------
    class(cubeio_cube_t), intent(in)    :: cub
    real(kind=4),         intent(in)    :: blocksize  ! [bytes]
    character(len=*),     intent(in)    :: blockname
    integer(kind=chan_k), intent(out)   :: nchan
    logical,              intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='MAX>CHAN>BLOCK'
    real(kind=4) :: onechan
    integer(kind=chan_k) :: ndiv
    character(len=message_length) :: mess
    !
    ! The size of the block should fit in the following constraint:
    !  - must fit at least 1 entry (otherwise we can't do anything)
    !  - if memory is below the dedicated maximum size, increase the buffer
    !    to the maximum possible
    onechan = cub%chansize() ! [Bytes] Weight of one channel
    nchan = min(floor(blocksize/onechan),cub%desc%nc)
    if (nchan.le.0) then
      nchan = 1
      write(mess,'(5A)')  'Buffer (',cubetools_format_memsize(blocksize),  &
        ') is not large enough to store one channel (',  &
        cubetools_format_memsize(onechan),')'
      call cubeio_message(seve%w,rname,mess)
      call cubeio_message(seve%w,rname,blockname//' should be increased for better efficiency')
    endif
    !
    ! Try to avoid very odd split, e.g. dividing 5=4+1 or 9=4+4+1. We prefer
    ! 5=3+2 or 9=3+3+3, i.e. same number of divisions but evenly distributed.
    ndiv = (cub%desc%nc-1)/nchan+1
    nchan = (cub%desc%nc-1)/ndiv+1
    !
    write(mess,'(A,I0,A)')  'Buffer will store up to ',nchan,' channels'
    call cubeio_message(ioseve%others,rname,mess)
  end subroutine cubeio_chan_per_block
  !
  subroutine cubeio_y_per_block(cub,blocksize,blockname,ny,error)
    !---------------------------------------------------------------------
    ! Return the maximum number of Y rows which can be used in a
    ! cube_block_t for cube given as argument
    !---------------------------------------------------------------------
    class(cubeio_cube_t), intent(in)    :: cub
    real(kind=4),         intent(in)    :: blocksize  ! [bytes]
    character(len=*),     intent(in)    :: blockname
    integer(kind=pixe_k), intent(out)   :: ny
    logical,              intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='MAX>Y>BLOCK'
    real(kind=4) :: oney
    integer(kind=pixe_k) :: ndiv
    character(len=message_length) :: mess
    !
    ! The size of the block should fit in the following constraint:
    !  - must fit at least 1 entry (otherwise we can't do anything)
    !  - if memory is below the dedicated maximum size, increase the buffer
    !    to the maximum possible
    oney = cub%ysize()  ! [Bytes] Weight of one Y row
    ny = min(floor(blocksize/oney),cub%desc%ny)
    if (ny.le.0) then
      ny = 1
      write(mess,'(5A)')  'Buffer (',cubetools_format_memsize(blocksize),  &
        ') is not large enough to store one Y row (',  &
        cubetools_format_memsize(oney),')'
      call cubeio_message(seve%w,rname,mess)
      call cubeio_message(seve%w,rname,blockname//' should be increased for better efficiency')
    endif
    !
    ! Try to avoid very odd split, e.g. dividing 5=4+1 or 9=4+4+1. We prefer
    ! 5=3+2 or 9=3+3+3, i.e. same number of divisions but evenly distributed.
    ndiv = (cub%desc%ny-1)/ny+1
    ny = (cub%desc%ny-1)/ndiv+1
    !
    write(mess,'(A,I0,A)')  'Buffer will store up to ',ny,' Y rows'
    call cubeio_message(ioseve%others,rname,mess)
  end subroutine cubeio_y_per_block
  !
  subroutine cubeio_plane_per_block(cub,blocksize,blockname,nplane,error)
    !---------------------------------------------------------------------
    ! Return the maximum number of planes which can be used in a
    ! cube_block_t for cube given as argument
    !---------------------------------------------------------------------
    class(cubeio_cube_t), intent(in)    :: cub
    real(kind=4),         intent(in)    :: blocksize  ! [bytes]
    character(len=*),     intent(in)    :: blockname
    integer(kind=data_k), intent(out)   :: nplane
    logical,              intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='MAX>ANY>BLOCK'
    real(kind=4) :: oneplane
    integer(kind=data_k) :: ndiv
    character(len=message_length) :: mess
    !
    ! The size of the block should fit in the following constraint:
    !  - must fit at least 1 entry (otherwise we can't do anything)
    !  - if memory is below the dedicated maximum size, increase the buffer
    !    to the maximum possible
    oneplane = cub%planesize() ! [Bytes] Weight of one plane
    nplane = min(floor(blocksize/oneplane),cub%desc%n3)
    if (nplane.le.0) then
      nplane = 1
      write(mess,'(5A)')  'Buffer (',cubetools_format_memsize(blocksize),  &
        ') is not large enough to store one plane (',  &
        cubetools_format_memsize(oneplane),')'
      call cubeio_message(seve%w,rname,mess)
      call cubeio_message(seve%w,rname,blockname//' should be increased for better efficiency')
    endif
    !
    ! Try to avoid very odd split, e.g. dividing 5=4+1 or 9=4+4+1. We prefer
    ! 5=3+2 or 9=3+3+3, i.e. same number of divisions but evenly distributed.
    ndiv = (cub%desc%n3-1)/nplane+1
    nplane = (cub%desc%n3-1)/ndiv+1
    !
    write(mess,'(A,I0,A)')  'Buffer will store up to ',nplane,' planes'
    call cubeio_message(ioseve%others,rname,mess)
  end subroutine cubeio_plane_per_block
  !
  function cubeio_plane_number(cub,iplane)
    !-------------------------------------------------------------------
    ! Convert a plane (1st x 2nd dimension) number from a 'user'
    ! (external programmer) request to the proper value internal value.
    ! As of today, the following behaviour is implemented:
    !  1) if the 'cube' has only 1 entry, requesting any positive
    !     entry value results in returning the unique entry. This
    !     is a way to vectorize at no cost the entry dimension.
    ! This only makes sense when reading entries (no sense writing
    ! different entries to the same one).
    !-------------------------------------------------------------------
    integer(kind=data_k) :: cubeio_plane_number  ! Function value on return
    class(cubeio_cube_t), intent(in) :: cub
    integer(kind=data_k), intent(in) :: iplane
    !
    if (iplane.le.0) then  ! Invalid
      cubeio_plane_number = iplane
    elseif (cub%desc%n3.eq.1) then
      cubeio_plane_number = 1
    else
      cubeio_plane_number = iplane
    endif
  end function cubeio_plane_number
    !
  function cubeio_chan_number(cub,ichan)
    !-------------------------------------------------------------------
    ! Convert a channel number from a 'user' (external programmer)
    ! request to the proper value internal value.
    ! As of today, the following behaviour is implemented:
    !  1) if the 'cube' has only 1 entry, requesting any positive
    !     entry value results in returning the unique entry. This
    !     is a way to vectorize at no cost the entry dimension.
    ! This only makes sense when reading entries (no sense writing
    ! different entries to the same one).
    !-------------------------------------------------------------------
    integer(kind=chan_k) :: cubeio_chan_number  ! Function value on return
    class(cubeio_cube_t), intent(in) :: cub
    integer(kind=chan_k), intent(in) :: ichan
    !
    if (ichan.le.0) then  ! Invalid
      cubeio_chan_number = ichan
    elseif (cub%desc%nc.eq.1) then
      cubeio_chan_number = 1
    else
      cubeio_chan_number = ichan
    endif
  end function cubeio_chan_number
  !
  function cubeio_xpix_number(cub,xpix)
    !-------------------------------------------------------------------
    ! Convert a X pixel number from a 'user' (external programmer)
    ! request to the proper value internal value.
    ! As of today, the following behaviour is implemented:
    !  1) if the 'cube' has only 1 entry, requesting any positive
    !     entry value results in returning the unique entry. This
    !     is a way to vectorize at no cost the entry dimension.
    ! This only makes sense when reading entries (no sense writing
    ! different entries to the same one).
    !-------------------------------------------------------------------
    integer(kind=pixe_k) :: cubeio_xpix_number  ! Function value on return
    class(cubeio_cube_t), intent(in) :: cub
    integer(kind=pixe_k), intent(in) :: xpix
    !
    if (xpix.le.0) then  ! Invalid
      cubeio_xpix_number = xpix
    elseif (cub%desc%nx.eq.1 .and. cub%desc%ny.eq.1) then
      ! Test both Nx and Ny. Only when both are 1 this behaviour applies.
      cubeio_xpix_number = 1
    else
      cubeio_xpix_number = xpix
    endif
  end function cubeio_xpix_number
  !
  function cubeio_ypix_number(cub,ypix)
    !-------------------------------------------------------------------
    ! Convert a Y pixel number from a 'user' (external programmer)
    ! request to the proper value internal value.
    ! As of today, the following behaviour is implemented:
    !  1) if the 'cube' has only 1 entry, requesting any positive
    !     entry value results in returning the unique entry. This
    !     is a way to vectorize at no cost the entry dimension.
    ! This only makes sense when reading entries (no sense writing
    ! different entries to the same one).
    !-------------------------------------------------------------------
    integer(kind=pixe_k) :: cubeio_ypix_number  ! Function value on return
    class(cubeio_cube_t), intent(in) :: cub
    integer(kind=pixe_k), intent(in) :: ypix
    !
    if (ypix.le.0) then  ! Invalid
      cubeio_ypix_number = ypix
    elseif (cub%desc%nx.eq.1 .and. cub%desc%ny.eq.1) then
      ! Test both Nx and Ny. Only when both are 1 this behaviour applies.
      cubeio_ypix_number = 1
    else
      cubeio_ypix_number = ypix
    endif
  end function cubeio_ypix_number
  !
  function cubeio_data_ready(cub)
    !-------------------------------------------------------------------
    ! Return .true. if the data has been prepared for access (memory or
    ! disk). Data is prepared when invoking one of the cubeio_iterate_*
    ! subroutines.
    !-------------------------------------------------------------------
    logical :: cubeio_data_ready
    class(cubeio_cube_t), intent(in) :: cub
    !
    cubeio_data_ready = cub%memo%ready.eq.cub%desc%buffered
  end function cubeio_data_ready
  !
  subroutine cubeio_cube_open(cub,error)
    !-------------------------------------------------------------------
    ! Open (or reopen) a 'cubeio_cube_t' instance for READ-ONLY access,
    ! if not yet opened.
    !-------------------------------------------------------------------
    class(cubeio_cube_t), intent(inout) :: cub    !
    logical,              intent(inout) :: error  !
    !
    call cub%file%open(error)
    if (error)  return
  end subroutine cubeio_cube_open
  !
  subroutine cubeio_cube_close(cub,error)
    !-------------------------------------------------------------------
    ! Close the file of a 'cubeio_cube_t' instance
    !-------------------------------------------------------------------
    class(cubeio_cube_t), intent(inout) :: cub    !
    logical,              intent(inout) :: error  !
    !
    call cub%file%close(error)
    if (error)  return
  end subroutine cubeio_cube_close
  !
  subroutine cubeio_cube_init(cub,error)
    !-------------------------------------------------------------------
    ! Initialize a cubeio_cube_t
    !-------------------------------------------------------------------
    class(cubeio_cube_t), intent(inout) :: cub
    logical,              intent(inout) :: error
    !
    cub%file => file_allocate_new(error)
    if (error)  return
  end subroutine cubeio_cube_init
  !
  subroutine cubeio_cube_free(cub,error)
    !-------------------------------------------------------------------
    ! Free the memory-consuming components of a 'cubeio_cube_t' instance.
    ! The cubeio_cube_t remains useable after this free. It is the
    ! responsibility of the caller to ensure the data remains available
    ! elsewhere (most likely on disk).
    ! Use cubeio_cube_final to free consistently all the object.
    !-------------------------------------------------------------------
    class(cubeio_cube_t), intent(inout) :: cub
    logical,              intent(inout) :: error
    !
    ! Beware!
    ! If this cube is memory-only, it should be completely discarded.
    ! If it has a file associated on disk, only the data in memory is deleted.
    call cub%file%free(error)
    if (error)  return
    call cub%memo%free(error)
    if (error)  return
    if (cub%desc%buffered.eq.code_buffer_memory) then
      call cubeio_desc_reset(cub%desc,error)
      if (error)  return
    endif
  end subroutine cubeio_cube_free
  !
  subroutine cubeio_cube_final(cub)
    !-------------------------------------------------------------------
    ! Finalize a 'cubeio_cube_t' instance
    !-------------------------------------------------------------------
    type(cubeio_cube_t), intent(inout) :: cub    !
    ! Local
    logical :: error
    !
    error = .false.
    call cub%file%free(error)
    if (error)  continue
    call cub%memo%free(error)
    if (error)  continue
    call cub%close(error)  ! Before cubeio_desc_final
    if (error)  continue
    call cubeio_desc_final(cub%desc) ! Explicit because above component rely on it
    deallocate(cub%file)
  end subroutine cubeio_cube_final

  subroutine cubeio_cube_feedback(cub,cubset)
    !-------------------------------------------------------------------
    !-------------------------------------------------------------------
    class(cubeio_cube_t), intent(in) :: cub
    type(cube_setup_t),   intent(in) :: cubset
    !
    ! Show only if requested
    if (.not.cubset%timing%io)  return
    call cub%time%feedback(cub%size())
  end subroutine cubeio_cube_feedback
end module cubeio_cube
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
