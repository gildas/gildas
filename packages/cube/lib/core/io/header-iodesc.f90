!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeio_header_iodesc
  use cubetools_parameters
  use cubetools_header_interface
  use cubeio_header_interface
  use cubeio_desc
  use cubeio_messaging
  !
  public :: cubeio_header_put_toiodesc
  public :: cubeio_iodesc_import
  public :: cubeio_iodesc_unrecognized
  private
  !
contains
  !
  subroutine cubeio_header_put_toiodesc(dno,order,iodesc,error)
    use cubedag_node_type
    !-------------------------------------------------------------------
    ! From type(cube_header_t) to type(cubeio_desc_t) with desired order
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), intent(inout) :: dno
    integer(kind=code_k),         intent(in)    :: order  ! code_order_*
    type(cubeio_desc_t),          intent(inout) :: iodesc
    logical,                      intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>PUT'
    !
    call cubeio_message(ioseve%trace,rname,'Welcome')
    !
    call dno%node%head2interf(error)
    if (error) return
    call cubeio_header_interface_transpose(dno%node%head,order,error)
    if (error) return
    call cubeio_iodesc_import(dno%node%head,iodesc,error)
    if (error) return
  end subroutine cubeio_header_put_toiodesc
  !
  !---------------------------------------------------------------------
  !
  subroutine cubeio_iodesc_import(in,iodesc,error)
    use gbl_format
    !-------------------------------------------------------------------
    ! From type(cube_header_interface_t) to type(cubeio_desc_t)
    !-------------------------------------------------------------------
    type(cube_header_interface_t), intent(in)    :: in
    type(cubeio_desc_t),           intent(inout) :: iodesc
    logical,                       intent(inout) :: error
    !
    integer(kind=ndim_k) :: ix,iy,ic,ndim
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='IODESC>IMPORT'
    !
    call cubeio_message(ioseve%trace,rname,'Welcome')
    !
    ! Insert fake dimensions if relevant:
    call cubeio_header_interface_derive_xyc(in,ndim,  &
                                            ix,iodesc%nx,  &
                                            iy,iodesc%ny,  &
                                            ic,iodesc%nc,error)
    if (error)  return
    !
    iodesc%iscplx = in%array_type.eq.fmt_c4
    if (ix.eq.1 .and. iy.eq.2) then
      iodesc%order = code_cube_imaset
      ! NB: ix/iy/ic might not be 1/2/3 if degenerate dimensions are
      !     interleaved.
      iodesc%i1 = ix
      iodesc%i2 = iy
      iodesc%i3 = ic
      iodesc%n1 = iodesc%nx
      iodesc%n2 = iodesc%ny
      iodesc%n3 = iodesc%nc
    elseif (ic.eq.1) then
      iodesc%order = code_cube_speset
      iodesc%i1 = ic
      iodesc%i2 = ix
      iodesc%i3 = iy
      iodesc%n1 = iodesc%nc
      iodesc%n2 = iodesc%nx
      iodesc%n3 = iodesc%ny
    else
      write(mess,'(4(a,i0))')  'Unrecognized order (IX=',ix,', IY=',iy,', IC=',ic,')'
      call cubeio_message(seve%w,rname,mess)
      call cubeio_iodesc_unrecognized(iodesc,error)
      if (error)  return
    endif
  end subroutine cubeio_iodesc_import
  !
  subroutine cubeio_iodesc_unrecognized(iodesc,error)
    !-------------------------------------------------------------------
    ! Set the descriptor as unrecognized order. Mostly unusable, except
    ! for basic operations like HEADER command
    !-------------------------------------------------------------------
    type(cubeio_desc_t), intent(inout) :: iodesc
    logical,             intent(inout) :: error
    !
    iodesc%order = code_cube_unkset
    iodesc%nx = 1
    iodesc%ny = 1
    iodesc%nc = 1
  end subroutine cubeio_iodesc_unrecognized
  !
end module cubeio_header_iodesc
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
