!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeio_header_hfits
  use phys_const
  use gkernel_interfaces
  use cubetools_parameters
  use cubetools_header_interface
  use cubetools_unit
  use cubedag_node_type
  use cubefitsio_header
  use cubefitsio_header_write
  use cubeio_header_interface
  use cubeio_messaging
  !
  integer(kind=code_k), parameter :: velref_convention_optical=0
  integer(kind=code_k), parameter :: velref_convention_radio=256
  !
  integer(kind=code_k), parameter :: velref_frame_lsrk=1
  integer(kind=code_k), parameter :: velref_frame_helio=2
  integer(kind=code_k), parameter :: velref_frame_obs=3
  !
  integer(kind=code_k), parameter :: code_unit_felo = code_unit_velo+1000
  !
  character(len=1), parameter :: telescop_separator='+'
  !
  public :: cubeio_header_get_and_derive_fromhfits
  public :: cubeio_header_put_tohfits
  public :: cubeio_header_truncate_and_put_tohfits
  private
  !
contains
  !
  subroutine cubeio_header_get_and_derive_fromhfits(hfits,dno,error)
    !-------------------------------------------------------------------
    ! From type(fitsio_header_t) to type(cube_header_interface_t)
    !-------------------------------------------------------------------
    type(fitsio_header_t),        intent(in)    :: hfits
    class(cubedag_node_object_t), intent(inout) :: dno
    logical,                      intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>GET>AND>DERIVE'
    !
    call cubeio_message(ioseve%trace,rname,'Welcome')
    !
    call cubeio_hfits_export(hfits,dno%node%head,dno%node%family,dno%node%flag,error)
    if (error) return
    call dno%node%interf2head(error)
    if (error) return
  end subroutine cubeio_header_get_and_derive_fromhfits
  !
  subroutine cubeio_header_put_tohfits(dno,order,hfits,error)
    !-------------------------------------------------------------------
    ! From type(cube_header_interface_t) to type(fitsio_header_t) with
    ! desired order.
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), intent(inout) :: dno
    integer(kind=code_k),         intent(in)    :: order  ! code_order_*
    type(fitsio_header_t),        intent(inout) :: hfits
    logical,                      intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>PUT'
    !
    call cubeio_message(ioseve%trace,rname,'Welcome')
    !
    call dno%node%head2interf(error)
    if (error) return
    call cubeio_header_interface_transpose(dno%node%head,order,error)  ! Inplace
    if (error) return
    call dno%node%head%compact_dim(error)
    if (error) return
    call cubeio_hfits_import(dno%node%head,dno%node%family,dno%node%flag,hfits,error)
    if (error) return
  end subroutine cubeio_header_put_tohfits
  !
  subroutine cubeio_header_truncate_and_put_tohfits(dno,order,dim3,hfits,error)
    !-------------------------------------------------------------------
    ! From type(cube_header_interface_t) to type(fitsio_header_t) with
    ! desired order.
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), intent(inout) :: dno
    integer(kind=code_k),         intent(in)    :: order  ! code_order_*
    integer(kind=data_k),         intent(in)    :: dim3
    type(fitsio_header_t),        intent(inout) :: hfits
    logical,                      intent(inout) :: error
    !
    character(len=*), parameter :: rname='HEADER>TRUNCATE>AND_PUT'
    !
    call cubeio_message(ioseve%trace,rname,'Welcome')
    !
    call dno%node%head2interf(error)
    if (error) return
    call cubeio_header_interface_transpose(dno%node%head,order,error)  ! Inplace
    if (error) return
    call cubeio_header_interface_truncate(dno%node%head,dim3,error)  ! Inplace
    if (error) return
    call dno%node%head%compact_dim(error)
    if (error) return
    call cubeio_hfits_import(dno%node%head,dno%node%family,dno%node%flag,hfits,error)
    if (error) return
  end subroutine cubeio_header_truncate_and_put_tohfits
  !
  !---------------------------------------------------------------------
  !
  subroutine cubeio_hfits_export(hfits,out,family,flags,error)
    use cubedag_flag
    !-------------------------------------------------------------------
    ! From type(fitsio_header_t) to type(cube_header_interface_t)
    !-------------------------------------------------------------------
    type(fitsio_header_t),         intent(in)    :: hfits
    type(cube_header_interface_t), intent(inout) :: out
    character(len=*),              intent(inout) :: family
    type(flag_list_t),             intent(inout) :: flags
    logical,                       intent(inout) :: error
    !
    logical :: found,classfits
    character(len=80) :: origin
    character(len=*), parameter :: rname='HFITS>EXPORT'
    !
    call cubeio_message(ioseve%trace,rname,'Welcome')
    !
    ! Nullify everything first, leave them nullified if not relevant
    call out%init(error)
    if (error)  return
    !
    out%array_type = hfits%mtype  ! Data type in memory
    out%axset_ndim = hfits%ndim
    out%axset_dim(1:hfits%ndim) = hfits%dim(1:hfits%ndim)
    !
    ! More general elements from the card dictionary
    call cubeio_hfits_export_spatial(hfits,out,error)
    if (error)  return
    call cubeio_hfits_export_convert(hfits,out,error)  ! After spatial
    if (error)  return
    call cubeio_hfits_export_array(hfits,out,error)
    if (error)  return
    call cubeio_hfits_export_spec(hfits,out,error)  ! After convert
    if (error)  return
    call cubeio_hfits_export_resolution(hfits,out,error)
    if (error)  return
    call cubeio_hfits_export_observatory(hfits,out,error)
    if (error)  return
    !
    ! ZZZ There should be a better way to recognize CLASS-FITS format
    origin = strg_unk
    call gfits_get_value(hfits%dict,'ORIGIN',found,origin,error)
    if (error)  return
    classfits = found .and. origin.eq.'CLASS-Grenoble'
    !
    if (classfits) then
      call cubeio_hfits_export_class(hfits,out,error)
      if (error) return
    endif
    !
    call cubeio_hfits_export_flags(hfits,family,flags,error)
    if (error)  return
  end subroutine cubeio_hfits_export
  !
  subroutine cubeio_hfits_export_class(hfits,out,error)
    !-------------------------------------------------------------------
    ! Collection of CLASS-specific patches to standardize to CUBE header
    !-------------------------------------------------------------------
    type(fitsio_header_t),         intent(in)    :: hfits
    type(cube_header_interface_t), intent(inout) :: out
    logical,                       intent(inout) :: error
    !
    ! Drop 4th degenerate dimension
    if (out%axset_ndim.eq.4 .and. out%axset_dim(4).eq.1)  out%axset_ndim = 3
    ! After array:
    call cubeio_hfits_export_class_unit(hfits,out,error)
    if (error)  return
    ! After resolution and observatory:
    call cubeio_hfits_export_class_resolution(out,error)
    if (error)  return
    ! After convert and class_resolution:
    call cubeio_hfits_export_class_recenter(out,error)
    if (error)  return
  end subroutine cubeio_hfits_export_class
  !
  subroutine cubeio_hfits_export_convert(hfits,out,error)
    use gfits_types
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    type(fitsio_header_t),         intent(in)    :: hfits
    type(cube_header_interface_t), intent(inout) :: out
    logical,                       intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='HFITS>EXPORT>CONVERT'
    integer(kind=4) :: iaxis,minus
    character(len=6) :: key
    character(len=12) :: ctype,ccode,cunit,suffix,pname
    character(len=unit_l) :: axis_unit
    integer(kind=code_k) :: axis_kind
    logical :: found
    character(len=mess_l) :: mess
    real(kind=8) :: factor,crota(maxdim)
    !
    ! Defaults
    out%axset_name(:) = strg_unk
    out%axset_unit(:) = strg_unk
    out%axset_convert(code_ref,:) = 0.d0
    out%axset_convert(code_val,:) = 0.d0
    out%axset_convert(code_inc,:) = 1.d0
    crota(:) = 0.d0
    pname = ''
    out%axset_ix = 0
    out%axset_iy = 0
    out%axset_ic = 0
    !
    ! ZZZ Need spatial_frame_code before loop
    !
    do iaxis=1,out%axset_ndim
      !
      write(key,'(A5,I1)')  'CTYPE',iaxis
      call gfits_get_value(hfits%dict,key,found,ctype,error)
      if (error)  return
      if (.not.found) then
        write(mess,'(A,I0,3A)')  'File has ',out%axset_ndim,' dimensions but ',  &
          trim(key),' is not defined'
        call hfits%message(seve%w,rname,mess)
        cycle
      endif
      !
      minus = index(ctype,'-',back=.true.)
      if (minus.eq.0) then
        suffix = ' '
        ccode = ctype
      else
        suffix = ctype(minus+1:)
        minus = index(ctype,'-',back=.false.)
        ccode = ctype(1:minus-1)
      endif
      !
      cunit = strg_unk
      select case (ccode)
      case ('RA')
        if (out%spatial_frame_code.eq.code_spaframe_unknown) then
          ! RA is ambiguously used for EQUATORIAL or ICRS. If missing default
          ! is FK5 (see subroutine cubeio_hfits_export_spatial for details)
          call hfits%message(seve%w,rname,'Coordinate system assumed EQUATORIAL')
          out%spatial_frame_code = code_spaframe_equatorial
        endif
        out%axset_ix = iaxis
        out%axset_name(iaxis) = 'RA'
        out%axset_kind(iaxis) = code_unit_fov
        cunit = 'deg'  ! Default if missing
        pname = suffix
      case ('DEC')
        if (out%spatial_frame_code.eq.code_spaframe_unknown) then
          ! DEC is ambiguously used for EQUATORIAL or ICRS
          call hfits%message(seve%w,rname,'Coordinate system assumed EQUATORIAL')
          out%spatial_frame_code = code_spaframe_equatorial
        endif
        out%axset_iy = iaxis
        out%axset_name(iaxis) = 'DEC'
        out%axset_kind(iaxis) = code_unit_fov
        cunit = 'deg'  ! Default if missing
        pname = suffix
      case ('LON','GLON')
        out%spatial_frame_code = code_spaframe_galactic
        out%axset_ix = iaxis
        out%axset_name(iaxis) = 'LII'
        out%axset_kind(iaxis) = code_unit_fov
        cunit = 'deg'  ! Default if missing
        pname = suffix
      case ('LAT','GLAT')
        out%spatial_frame_code = code_spaframe_galactic
        out%axset_iy = iaxis
        out%axset_name(iaxis) = 'BII'
        out%axset_kind(iaxis) = code_unit_fov
        cunit = 'deg'  ! Default if missing
        pname = suffix
      case ('VELOCITY','VELO','VRAD','VOPT')
        out%axset_ic = iaxis
        out%axset_name(iaxis) = 'VELOCITY'
        out%axset_kind(iaxis) = code_unit_velo
        cunit = 'm/s'  ! Default if missing
      case ('FELO')
        out%axset_ic = iaxis
        out%axset_name(iaxis) = 'VELOCITY'
        out%axset_kind(iaxis) = code_unit_felo
        cunit = 'm/s'  ! Default if missing
      case ('FREQUENCY','FREQ')
        out%axset_ic = iaxis
        out%axset_name(iaxis) = 'FREQUENCY'
        out%axset_kind(iaxis) = code_unit_freq
        cunit = 'Hz'  ! Default if missing
      case ('LAMBDA','WAVE','AWAV')
        out%axset_ic = iaxis
        out%axset_name(iaxis) = 'LAMBDA'
        out%axset_kind(iaxis) = code_unit_wave
        cunit = 'm'  ! Default if missing
      case ('STOKES')
        out%axset_name(iaxis) = 'STOKES'
        out%axset_kind(iaxis) = code_unit_unk
      case default
        ! Non spatial or unsupported spatial axis. Note that a projection
        ! can still be understood (axset_name split between ccode and
        ! suffix)
        ! ZZZ We might not want to split if projection is not understood
        !    OR
        ! Non spectral or unsupported spectral axis. See Greisen et al.
        ! 2006 Tables 1 & 2 for possibilities.
        out%axset_name(iaxis) = ccode
        out%axset_kind(iaxis) = code_unit_unk  ! Can not guess axis kind from name
                                             ! Will give a last try by looking at the unit (see below)
      end select
      !
      ! Unit and conversion factor to header_interface_t internal unit.
      write(key,'(A5,I1)')  'CUNIT',iaxis
      call gfits_get_value(hfits%dict,key,found,cunit,error)
      if (error)  return
      ! If not found, 'cunit' was not modified (which preserve preset default)
      if (cunit.ne.strg_unk) then
        ! Compute factor from FITS unit to header_interface_t internal unit.
        ! NB: this usually applies to known axes (e.g. RA-DEC-VELO, but unknown
        ! axes are converted too).
        call hfits_export_unit_factor(cunit,axis_unit,axis_kind,factor)
        out%axset_unit(iaxis) = axis_unit
        ! ZZZ Shouldn't we ensure that the unit found here is compatible with
        ! the kind resolved from axis name?
        ! out%axset_kind(iaxis) vs axis_kind
      else
        factor = 1.d0
        out%axset_unit(iaxis) = strg_unk
      endif
      !
      ! Kind? Last chance:
      if (out%axset_kind(iaxis).eq.code_unit_unk) then
        ! The axis kind could not be guessed from its name, apply guess from
        ! its unit
        out%axset_kind(iaxis) = axis_kind
      endif
      !
      ! Convert array
      write(key,'(A5,I1)')  'CRPIX',iaxis
      call gfits_get_value(hfits%dict,key,found,out%axset_convert(code_ref,iaxis),error)
      if (error)  return
      !
      write(key,'(A5,I1)')  'CRVAL',iaxis
      call gfits_get_value(hfits%dict,key,found,out%axset_convert(code_val,iaxis),error)
      if (error)  return
      out%axset_convert(code_val,iaxis) = out%axset_convert(code_val,iaxis)*factor
      !
      write(key,'(A5,I1)')  'CDELT',iaxis
      call gfits_get_value(hfits%dict,key,found,out%axset_convert(code_inc,iaxis),error)
      if (error)  return
      out%axset_convert(code_inc,iaxis) = out%axset_convert(code_inc,iaxis)*factor
      !
      write(key,'(A5,I1)')  'CROTA',iaxis
      call gfits_get_value(hfits%dict,key,found,crota(iaxis),error)
      if (error)  return
      crota(iaxis) = crota(iaxis)*factor  ! spatial_projection_pa also needs internal unit
      !
    enddo
    !
    call cubeio_hfits_export_pcmatrix(hfits,out,crota,error)
    if (error)  return
    call cubeio_hfits_export_cdmatrix(hfits,out,crota,error)
    if (error)  return
    !
    call cubeio_hfits_export_projection(hfits,pname,crota,out,error)
    if (error)  return
    !
    call cubeio_hfits_export_sfl_as_radio(hfits,out,error)
    if (error)  return
  end subroutine cubeio_hfits_export_convert
  !
  subroutine hfits_export_unit_factor(cunit,unit_name,unit_code,unit_factor)
    use gkernel_interfaces
    use cubetools_unit_setup ! ***JP: I'd rather avoid using this one everywhere
    !-------------------------------------------------------------------
    ! Compute the conversion factor from FITS unit to header_interface_t
    ! internal unit. Note that there is no warranty the FITS unit names
    ! match the CUBE names: this function is specific to FITS.
    !-------------------------------------------------------------------
    character(len=*),     intent(in)  :: cunit
    character(len=*),     intent(out) :: unit_name  ! Resolved unit name
    integer(kind=code_k), intent(out) :: unit_code  ! Resolved unit code
    real(kind=8),         intent(out) :: unit_factor
    ! Local
    logical :: error
    type(unit_user_t) :: user
    !
    error = .false.
    call user%get_from_name(cunit,error)
    if (error)  return ! ZZZ
    !
    if (user%kind.eq.unit_unk) then
      ! Found nothing known
      unit_factor = 1.d0
      unit_name = cunit  ! Leave as is
    else
      ! Some supported unit
      unit_factor = user%prog_per_user   ! Scaling factor from this unit to 'interface' internal unit
      unit_name = user%kind%prog_name()  ! Translated to CUBE internal name
    endif
    unit_code = user%kind%code ! Known or unknown
  end subroutine hfits_export_unit_factor
  !
  subroutine cubeio_hfits_export_pcmatrix(hfits,out,crota,error)
    use gkernel_interfaces
    use gfits_types
    !---------------------------------------------------------------------
    ! This code is precisely documented in the technical document
    ! "Support of FITS coordinates rotation and reflection in GILDAS"
    ! (Bardeau, 2024)
    !---------------------------------------------------------------------
    type(fitsio_header_t),         intent(in)    :: hfits
    type(cube_header_interface_t), intent(inout) :: out
    real(kind=8),                  intent(inout) :: crota(:)  ! [rad] Might be (re)defined here
    logical,                       intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='HFITS>EXPORT>PCMATRIX'
    real(kind=8) :: pc(2,2),det,inc1,inc2
    integer(kind=4) :: numpc
    !
    ! Read PC matrix (default as per Greisen & Calabretta paper I, section 2.1.2)
    pc(1,1) = 1.d0
    pc(1,2) = 0.d0
    pc(2,1) = 0.d0
    pc(2,2) = 1.d0
    call read_matrix(hfits,'PC',pc,numpc,error)
    if (error)  return
    if (numpc.eq.0) then
      return
    elseif (numpc.ne.4) then
      call hfits%message(seve%w,rname,'PC matrix is incomplete')
    endif
    !
    ! Increments
    inc1 = out%axset_convert(code_inc,1)
    inc2 = out%axset_convert(code_inc,2)
    !
    ! Check skewness
    det = pc(1,1)*pc(2,2) - pc(2,1)*pc(1,2)
    if (.not.nearly_equal(abs(det),1.d0,1.d-4)) then
      ! This is not a rotation (+1) nor a reflection (-1), but a more complex matrix
      call hfits%message(seve%e,rname,'PCi_j matrix is skewed')
      error = .true.
      return
    endif
    !
    ! Check for reflection
    if (det.lt.0) then
      ! One axis has been inverted. Assume this is the first one (X axis
      ! increase to the left)
      pc(1,1) = -pc(1,1)
      pc(2,1) = -pc(2,1)
      inc1 = -inc1
    endif
    !
    out%axset_convert(code_inc,1) = inc1
    out%axset_convert(code_inc,2) = inc2
    crota(1) = atan2(+(inc2/inc1)*pc(2,1),pc(1,1))  ! [rad] header_interface_t internal unit
    crota(2) = atan2(-(inc1/inc2)*pc(1,2),pc(2,2))  ! [rad]
  end subroutine cubeio_hfits_export_pcmatrix
  !
  subroutine cubeio_hfits_export_cdmatrix(hfits,out,crota,error)
    use gkernel_interfaces
    use gfits_types
    !---------------------------------------------------------------------
    ! This code is precisely documented in the technical document
    ! "Support of FITS coordinates rotation and reflection in GILDAS"
    ! (Bardeau, 2024)
    !---------------------------------------------------------------------
    type(fitsio_header_t),         intent(in)    :: hfits
    type(cube_header_interface_t), intent(inout) :: out
    real(kind=8),                  intent(inout) :: crota(:)  ! [rad] Might be (re)defined here
    logical,                       intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='HFITS>EXPORT>CDMATRIX'
    real(kind=8) :: cd(2,2),factor,inc1,inc2
    integer(kind=4) :: numcd
    real(kind=4) :: ratio1,ratio2  ! R*4 to avoid useless digits
    character(len=unit_l) :: axis_unit
    integer(kind=code_k) :: axis_kind
    !
    ! Read CD matrix (defaults as per Greisen & Calabretta paper I, section 2.1.2)
    cd(:,:) = 0.d0
    call read_matrix(hfits,'CD',cd,numcd,error)
    if (error)  return
    if (numcd.eq.0) then
      return
    elseif (numcd.ne.4) then
      call hfits%message(seve%w,rname,'PC matrix is incomplete')
    endif
    !
    ! Compute factor assuming the increments in the CD matrix are expressed
    ! in degrees/pixel
    call hfits_export_unit_factor('deg',axis_unit,axis_kind,factor)
    !
    ! Increment moduli
    inc1 = sqrt(cd(1,1)**2+cd(2,1)**2)*factor
    inc2 = sqrt(cd(1,2)**2+cd(2,2)**2)*factor
    !
    ! Check skewness
    ratio1 = cd(1,1)/cd(2,2)
    ratio2 = inc1/inc2
    if (.not.nearly_equal(abs(ratio1),ratio2,1e-4)) then
      ! This is not a rotation, but a more complex matrix
      call hfits%message(seve%e,rname,'CDi_j matrix is skewed')
      error = .true.
      return
    endif
    !
    ! Check for reflection
    if (ratio1.lt.0) then  ! i.e. cd(1,1) and cd(2,2) are opposite sign
      ! One axis has been inverted. Without the original CDELTi or a PC
      ! matrix, assume this is the first one (X axis increase to the left)
      cd(1,1) = -cd(1,1)
      cd(2,1) = -cd(2,1)
      inc1 = -inc1
    endif
    !
    out%axset_convert(code_inc,1) = inc1
    out%axset_convert(code_inc,2) = inc2
    crota(1) = atan2(+cd(2,1),cd(1,1))  ! [rad] header_interface_t internal unit
    crota(2) = atan2(-cd(1,2),cd(2,2))  ! [rad]
  end subroutine cubeio_hfits_export_cdmatrix
  !
  subroutine read_matrix(hfits,name,mat,num,error)
    !-------------------------------------------------------------------
    ! Read the named matrix in the mat(2,2) variable. For readability
    ! we store XXi_j into mat(i,j). The global matrix is:
    !           ( XX1_1  XX1_2 )
    !           ( XX2_1  XX2_2 )
    !-------------------------------------------------------------------
    type(fitsio_header_t), intent(in)    :: hfits
    character(len=*),      intent(in)    :: name
    real(kind=8),          intent(inout) :: mat(2,2)
    integer(kind=4),       intent(out)   :: num
    logical,               intent(inout) :: error
    !
    logical :: found
    !
    num = 0
    call gfits_get_value(hfits%dict,name//'1_1',found,mat(1,1),error)
    if (error)  return
    if (found)  num = num+1
    call gfits_get_value(hfits%dict,name//'1_2',found,mat(1,2),error)
    if (error)  return
    if (found)  num = num+1
    call gfits_get_value(hfits%dict,name//'2_1',found,mat(2,1),error)
    if (error)  return
    if (found)  num = num+1
    call gfits_get_value(hfits%dict,name//'2_2',found,mat(2,2),error)
    if (error)  return
    if (found)  num = num+1
  end subroutine read_matrix
  !
  subroutine cubeio_hfits_export_projection(hfits,pname,crota,out,error)
    use gbl_constant
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    type(fitsio_header_t),         intent(in)    :: hfits
    character(len=*),              intent(in)    :: pname
    real(kind=8),                  intent(in)    :: crota(:)
    type(cube_header_interface_t), intent(inout) :: out
    logical,                       intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='HFITS>EXPORT>PROJECTION'
    !
    ! Projection type
    select case (pname)
    case ('')
      ! Projection is not defined.
      if (out%axset_ix.ne.0 .or. out%axset_iy.ne.0) then
        ! Spatial axes are defined but not their projection. Quoting Greisen
        ! & Calabretta 2002: "CTYPEi values that are not in "4–3" form
        ! should be interpreted as linear axes."
        call hfits%message(seve%w,rname,'Projection assumed CARTESIAN')
        out%spatial_projection_code = p_cartesian
      else
        ! No spatial axes defined => no projection
        out%spatial_projection_code = p_none
      endif

      ! Projection is not defined. Quoting Greisen & Calabretta 2002: "CTYPEi
      ! values that are not in "4–3" form should be interpreted as linear axes."
    case ('TAN')
      out%spatial_projection_code = p_gnomonic
    case ('AIT','ATF')  !  Hammer-Aitoff (ATF code was formerly written by some GILDAS programs)
      out%spatial_projection_code = p_aitoff
    case ('SIN')
      ! Note that Gildas does not support extended SIN (Slant orthographic).
      ! See Calabretta & Greisen 2002, sections 5.1.5 and 6.1.1. We should
      ! reject the cases when PVi_j are defined and non-zero.
      out%spatial_projection_code = p_ortho  ! Orthographic or Dixon
    case ('ARC')
      out%spatial_projection_code = p_azimuthal  ! Schmidt or Azimuthal
    case ('STG')  ! Stereographic
      out%spatial_projection_code = p_stereo
    case ('GLS')
      out%spatial_projection_code = p_radio
    case ('CAR')  ! Cartesian
      out%spatial_projection_code = p_cartesian
    case ('SFL')
      out%spatial_projection_code = p_sfl
    case ('NCP')  ! North Celestial Pole
      ! Gildas offers native support. Note that according to Calabretta
      ! & Greisen 2002, NCP is obsolete and should be translated to
      ! (see section 6.1.2):
      !     SIN with PV2_1 = 0 and PV2_2 = 1/tan(d0)
      ! This defines an "extented" SIN projection (Slant orthographic, see
      ! section 5.1.5). However, Gildas supports only the (non-extended)
      ! orthographic projection with SIN with PV2_1 = PV2_2 = 0 (see section
      ! 6.1.1).
      out%spatial_projection_code = p_ncp
    case default
      call hfits%message(seve%w,rname,'Unrecognized projection '//pname)
      out%spatial_projection_code = p_none
    end select
    !
    ! Projection angle. Can be defined in CROTA2 but not CROTA1.
    if (out%axset_ix.ne.0 .and. crota(out%axset_ix).ne.0.d0) then
      out%spatial_projection_pa = crota(out%axset_ix)
    elseif (out%axset_iy.ne.0 .and. crota(out%axset_iy).ne.0.d0) then
      out%spatial_projection_pa = crota(out%axset_iy)
    else
      out%spatial_projection_pa = 0.d0
    endif
    !
    ! Projection center
    if (out%axset_ix.ne.0) then
      out%spatial_projection_l0 = out%axset_convert(code_val,out%axset_ix)
      out%axset_convert(code_val,out%axset_ix) = 0.d0
    endif
    if (out%axset_iy.ne.0) then
      out%spatial_projection_m0 = out%axset_convert(code_val,out%axset_iy)
      out%axset_convert(code_val,out%axset_iy) = 0.d0
    endif
    !
    if (out%spatial_projection_code.eq.p_aitoff .and.  &
        out%spatial_projection_m0.ne.0.d0) then
      ! GILDAS equations for Aitoff projection implement only reference
      ! on the Equator.
      call hfits%message(seve%w,rname,  &
        'AITOFF with non-zero declination is not supported: expect incorrect coordinates')
    endif
  end subroutine cubeio_hfits_export_projection
  !
  subroutine cubeio_hfits_export_sfl_as_radio(hfits,out,error)
    use gbl_constant
    !---------------------------------------------------------------------
    ! If possible, convert back the SFL projection to the usual radio
    ! projection. This patch is symetric to what is done at import time,
    ! but the import patch puts the reference on the Equator, which is a
    ! bit annoying.
    !---------------------------------------------------------------------
    type(fitsio_header_t),         intent(in)    :: hfits
    type(cube_header_interface_t), intent(inout) :: out
    logical,                       intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='HFITS>EXPORT>SFL>AS>RADIO'
    real(kind=8) :: dec
    logical :: found
    !
    if (out%spatial_projection_code.ne.p_sfl)  return
    if (out%axset_iy.eq.0)                     return
    if (out%spatial_projection_m0.ne.0.d0)     return
    !
    ! Get the source declination
    dec = 0.d0
    call gfits_get_value(hfits%dict,'DEC',found,dec,error)
    if (error)  return
    if (dec.eq.0.d0)  return
    dec = dec*rad_per_deg
    !
    ! Bring back reference point to the source declination
    call cubeio_message(seve%w,rname,'Projection kind converted from SFL to radio')
    out%spatial_projection_code = p_radio
    out%spatial_projection_m0 = dec
    out%axset_convert(code_ref,out%axset_iy) =     &
      out%axset_convert(code_ref,out%axset_iy) +  &
      dec/out%axset_convert(code_inc,out%axset_iy)
    !
  end subroutine cubeio_hfits_export_sfl_as_radio
  !
  subroutine cubeio_hfits_export_array(hfits,out,error)
    use gfits_types
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    type(fitsio_header_t),         intent(in)    :: hfits
    type(cube_header_interface_t), intent(inout) :: out
    logical,                       intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='HFITS>EXPORT>ARRAY'
    logical :: found
    character(len=12) :: tempscale
    !
    call gfits_get_value(hfits%dict,'DATAMIN',found,out%array_minval,error)
    if (error)  return
    !
    call gfits_get_value(hfits%dict,'DATAMAX',found,out%array_maxval,error)
    if (error)  return
    !
    call gfits_get_value(hfits%dict,'BUNIT',found,out%array_unit,error)
    if (error)  return
    !
    call gfits_get_value(hfits%dict,'NOISETHE',found,out%array_noise,error)
    if (error)  return
    !
    call gfits_get_value(hfits%dict,'NOISEMEA',found,out%array_rms,error)
    if (error)  return
    !
    tempscale = ''
    call gfits_get_value(hfits%dict,'TEMPSCAL',found,tempscale,error)
    if (error)  return
    if (tempscale.ne.'') then
      ! Try to format to the usual Gildas encoding e.g. "K (TA*)"
      out%array_unit = trim(out%array_unit)//' ('//trim(tempscale)//')'
    endif
  end subroutine cubeio_hfits_export_array
  !
  subroutine cubeio_hfits_export_spatial(hfits,out,error)
    use gbl_constant
    use gfits_types
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    type(fitsio_header_t),         intent(in)    :: hfits
    type(cube_header_interface_t), intent(inout) :: out
    logical,                       intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='HFITS>EXPORT>SPAFRAME'
    integer(kind=4) :: ier
    logical :: found
    character(len=80) :: value
    !
    call gfits_get_value(hfits%dict,'OBJECT',found,out%spatial_source,error)
    if (error)  return
    !
    ! Default equinox (NB: FITS says RADESYS defaults to FK5 if absent, i.e.
    ! there is a valid default system and equinox...)
    out%spatial_frame_code = code_spaframe_unknown
    out%spatial_frame_equinox = equinox_null
    !
    ! System and optionally equinox
    call gfits_get_value(hfits%dict,'RADESYS',found,value,error)
    if (error)  return
    if (.not.found) then  ! Try deprecated RADECSYS
      call gfits_get_value(hfits%dict,'RADECSYS',found,value,error)
      if (error)  return
    endif
    if (found) then
      ! From RADESYS
      if (value(1:8).eq.'FK4-NO-E') then
        out%spatial_frame_code = code_spaframe_equatorial
        out%spatial_frame_equinox = 1950.0
      elseif (value(1:3).eq.'FK4') then
        out%spatial_frame_code = code_spaframe_equatorial
        out%spatial_frame_equinox = 1950.0
      elseif (value(1:3).eq.'FK5') then
        out%spatial_frame_code = code_spaframe_equatorial
        out%spatial_frame_equinox = 2000.0
      elseif (value(1:4).eq.'ICRS') then
        out%spatial_frame_code = code_spaframe_icrs
        out%spatial_frame_equinox = equinox_null  ! Irrelevant for ICRS
      endif
    endif
    !
    ! EQUINOX (or EPOCH)
    call gfits_get_value(hfits%dict,'EQUINOX',found,value,error)
    if (error)  return
    if (.not.found) then
      call gfits_get_value(hfits%dict,'EPOCH',found,value,error)
      if (error)  return
    endif
    if (found) then
      read(value,*,iostat=ier) out%spatial_frame_equinox
      if (ier.ne.0) then
        if (value(1:1).eq.'J') then
          read(value(2:),*,iostat=ier) out%spatial_frame_equinox
        elseif (value(1:1).eq.'B') then
          read(value(2:),*,iostat=ier) out%spatial_frame_equinox
        endif
        if (ier.ne.0) then
          call hfits%message(seve%e,rname,'Undecipherable Equinox '//value)
          out%spatial_frame_equinox = equinox_null
        endif
      endif
      ! At this stage we are almost sure the frame is equatorial, but
      ! wait a bit more looking at the axis names.
    endif
  end subroutine cubeio_hfits_export_spatial
  !
  subroutine cubeio_hfits_export_spec(hfits,out,error)
    use gfits_types
    use cubetools_convert
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    type(fitsio_header_t),         intent(in)    :: hfits
    type(cube_header_interface_t), intent(inout) :: out
    logical,                       intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='HFITS>EXPORT>SPEC'
    integer(kind=4) :: velref
    logical :: found
    character(len=12) :: specsys
    real(kind=8) :: vres,altrpix
    character(len=8) :: veloname
    character(len=mess_l) :: mess
    !
    ! Velocity type
    out%spectral_convention = code_speconv_radio  ! Default if missing
    out%spectral_frame_code = code_speframe_lsrk  ! Default if missing
    ! Try VELREF which encodes both the convention and the frame
    call gfits_get_value(hfits%dict,'VELREF',found,velref,error)
    if (error)  return
    if (found) then
      if (velref.gt.velref_convention_radio) then
        out%spectral_convention = code_speconv_radio
        velref = velref-velref_convention_radio
      elseif (velref.gt.velref_convention_optical) then
        out%spectral_convention = code_speconv_optical
        velref = velref-velref_convention_optical
      endif
      select case (velref)
      case (velref_frame_lsrk)
        out%spectral_frame_code = code_speframe_lsrk
      case (velref_frame_helio)
        out%spectral_frame_code = code_speframe_helio
      case (velref_frame_obs)
        out%spectral_frame_code = code_speframe_obser
      case default
        write(mess,'(A,I0)') 'Invalid or unsupported VELREF ',velref
        call hfits%message(seve%w,rname,mess)
        found = .false.
      end select
    endif
    ! Else try SPECSYS which encodes the frame
    if (.not.found) then   ! 'found' might have change
      call gfits_get_value(hfits%dict,'SPECSYS',found,specsys,error)
      if (error)  return
      if (found) then
        select case (specsys)
        case ('LSRK')
          out%spectral_frame_code = code_speframe_lsrk
        case ('HEL','BARYCENT')
          out%spectral_frame_code = code_speframe_helio
        case ('TOPOCENT')
          out%spectral_frame_code = code_speframe_obser
        case default
          call hfits%message(seve%w,rname,'Invalid or unsupported SPECSYS '//trim(specsys))
          found = .false.
        end select
      endif
    endif
    ! Else keep defaults
    if (.not.found) then   ! 'found' might have change
      call hfits%message(seve%w,rname,  &
        'SPECSYS and VELREF missing, assuming radio convention and spectral frame LSRk')
    endif
    !
    ! LINE
    call gfits_get_value(hfits%dict,'LINE',found,out%spectral_line,error)
    if (error)  return
    if (.not.found) then
      call gfits_get_value(hfits%dict,'LINENAME',found,out%spectral_line,error)
      if (error)  return
    endif
    if (.not.found) then
      out%spectral_line = strg_unk
    endif
    !
    out%spectral_code = code_spectral_unknown  ! Default before looking for specific keywords
    !
    ! Velocity offset. Note: it is unclear whether we should search for FELO-*
    ! keyword in case of a felocity axis.
    select case (out%spectral_frame_code)
    case (code_speframe_lsrk)
      veloname = 'VELO-LSR'
    case (code_speframe_helio)
      veloname = 'VELO-HEL'
    case (code_speframe_obser)
      veloname = 'VELO-OBS'
    case default
      veloname = 'VELOCITY'
    end select
    out%spectral_systemic_code = code_systemic_velocity
    call gfits_get_value(hfits%dict,veloname,found,out%spectral_systemic_value,error)
    if (error)  return
    if (found) then
      out%spectral_code = code_spectral_frequency
      ! Up to now VELO-LSR has no known unit => assume m/s, convert to km/s
      out%spectral_systemic_value = out%spectral_systemic_value*1e-3
    elseif (out%axset_ic.gt.0 .and. out%axset_name(out%axset_ic).eq.'VELOCITY') then
      out%spectral_code = code_spectral_frequency
      out%spectral_systemic_value = out%axset_convert(code_val,out%axset_ic)  ! Already in 'interface' internal unit
    else
      ! No velocity axis nor velocity offset...
      call hfits%message(seve%w,rname,'Missing velocity description')
      out%spectral_systemic_value = 0.d0
    endif
    !
    ! Patch for felocity before conversions between velocity and frequency.
    call cubeio_hfits_export_felocity(out,error)
    if (error)  return
    !
    ! Rest frequency: CUBE assumes it is aligned with reference channel / CRVALi.
    ! Several possibilities, in order:
    !  1) from CRVALi if defined and if axis is FREQUENCY. This gives proper
    !     description of the velocity axis, but looses the informative RESTFRQ;
    !  2) from the RESTFREQ or RESTFRQ keyword if axis is VELOCITY;
    !  3) from ALTRVAL if axis is VELOCITY.
    found = .false.
    if (out%axset_ic.gt.0 .and.  &
        out%axset_name(out%axset_ic).eq.'FREQUENCY' .and.  &
        out%axset_convert(code_val,out%axset_ic).gt.0.d0) then
       out%spectral_signal_value = out%axset_convert(code_val,out%axset_ic)*1d6
       found = .true.
    endif
    if (.not.found) then
      call gfits_get_value(hfits%dict,'RESTFREQ',found,out%spectral_signal_value,error)
      if (error)  return
    endif
    if (.not.found) then
      call gfits_get_value(hfits%dict,'RESTFRQ',found,out%spectral_signal_value,error)
      if (error)  return
    endif
    if (.not.found .and. out%axset_ic.gt.0 .and.  &
        out%axset_name(out%axset_ic).eq.'VELOCITY') then
      ! Rest frequency not yet found, try to derive from ALTRVAL/ALTRPIX.
      ! Note: if RESTFREQ is already known (usual case), we should check the
      ! consistency between RESTFREQ, convert[*,faxi], and ALTRVAL/ALTRPIX
      call hfits%message(seve%w,rname,'Deriving rest frequency from ALTRVAL/ALTRPIX')
      call gfits_get_value(hfits%dict,'ALTRVAL',found,out%spectral_signal_value,error)
      if (error)  return
      call gfits_get_value(hfits%dict,'ALTRPIX',found,altrpix,error)
      if (error)  return
      if (found) then
        ! Frequency at ref. channel (in case it is different from ALTRPIX)
        out%spectral_signal_value = out%spectral_signal_value -  &
          (altrpix-out%axset_convert(code_ref,out%axset_ic)) *  &
          out%axset_convert(code_inc,out%axset_ic) * out%spectral_signal_value / clight_kms
        ! Rest frequency corresponds to velocity = 0 (LSR frame)
        out%spectral_signal_value = out%spectral_signal_value -  &
          out%axset_convert(code_val,out%axset_ic) *  &
          out%axset_convert(code_inc,out%axset_ic) * out%spectral_signal_value / clight_kms
      endif
    endif
    if (found) then
      out%spectral_code = code_spectral_frequency
      ! Up to now RESTFREQ has no known unit => assume Hz, convert to MHz
      out%spectral_signal_value = out%spectral_signal_value*1d-6
    endif
    !
    ! Value at reference channel
    if (out%axset_ic.gt.0 .and.  &
        out%axset_name(out%axset_ic).eq.'FREQUENCY' .and.  &
        out%axset_convert(code_val,out%axset_ic).le.0.d0) then
      ! Clearly undefined for this frequency axis (would not be so clear if it was
      ! a velocity axis). Take from rest frequency, if we found it:
      if (out%spectral_signal_value.gt.0.d0) then
        out%axset_convert(code_val,out%axset_ic) = out%spectral_signal_value  ! [MHz]
      endif
    endif
    !
    ! Image frequency
    call gfits_get_value(hfits%dict,'IMAGFREQ',found,out%spectral_image_value,error)
    if (error)  return
    out%spectral_image_value = out%spectral_image_value*1d-6  ! Assume Hz, convert to MHz
    !
    out%spectral_increment_value = 0.d0
    if (out%axset_ic.ne.0) then
      if (out%axset_name(out%axset_ic).eq.'VELOCITY') then
        vres = out%axset_convert(code_inc,out%axset_ic)
        out%spectral_increment_value = cubetools_convert_vres2fres(vres,out%spectral_signal_value)
      elseif (out%axset_name(out%axset_ic).eq.'FREQUENCY') then
        out%spectral_increment_value = out%axset_convert(code_inc,out%axset_ic)
      endif
    endif
  end subroutine cubeio_hfits_export_spec
  !
  subroutine cubeio_hfits_export_felocity(out,error)
    !-------------------------------------------------------------------
    ! Convert a felocity axis to a radio-convention velocity axis. See
    ! AIPS Memo 27 section II.
    !-------------------------------------------------------------------
    type(cube_header_interface_t), intent(inout) :: out
    logical,                       intent(inout) :: error
    !
    character(len=*), parameter :: rname='HFITS>EXPORT>FELOCITY'
    !
    if (out%axset_ic.eq.0)  return
    if (out%axset_kind(out%axset_ic).ne.code_unit_felo)  return
    !
    call cubeio_message(seve%w,rname,'Converting FELO axis to velocity in radio convention')
    !
    ! See more specifically the line "where AXDENU = ..." page 3 of the
    ! AIPS memo:
    out%axset_convert(code_inc,out%axset_ic) =  &
      out%axset_convert(code_inc,out%axset_ic)/(1.d0+out%spectral_systemic_value/clight_kms)
    !
    out%axset_kind(out%axset_ic) = code_unit_velo
    !
    out%spectral_convention = code_speconv_radio  ! The definition now fits in radio convention
  end subroutine cubeio_hfits_export_felocity
  !
  subroutine cubeio_hfits_export_resolution(hfits,out,error)
    use gfits_types
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    type(fitsio_header_t),         intent(in)    :: hfits
    type(cube_header_interface_t), intent(inout) :: out
    logical,                       intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='HFITS>EXPORT>RESOLUTION'
    logical :: found
    !
    out%spatial_beam_major = 0.
    call gfits_get_value(hfits%dict,'BMAJ',found,out%spatial_beam_major,error)
    if (error)  return
    if (found)  out%spatial_beam_major = out%spatial_beam_major*rad_per_deg
    !
    out%spatial_beam_minor = 0.
    call gfits_get_value(hfits%dict,'BMIN',found,out%spatial_beam_minor,error)
    if (error)  return
    if (found)  out%spatial_beam_minor = out%spatial_beam_minor*rad_per_deg
    !
    out%spatial_beam_pa = 0.
    call gfits_get_value(hfits%dict,'BPA', found,out%spatial_beam_pa,error)
    if (error)  return
    if (found)  out%spatial_beam_pa = out%spatial_beam_pa*rad_per_deg
  end subroutine cubeio_hfits_export_resolution
  !
  subroutine cubeio_hfits_export_observatory(hfits,out,error)
    use gfits_types
    use cubetools_string
    use cubetools_observatory_types
    use cubetools_obstel_prog_types
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    type(fitsio_header_t),         intent(in)    :: hfits
    type(cube_header_interface_t), intent(inout) :: out
    logical,                       intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='HFITS>EXPORT>OBSERVATORY'
    logical :: found
    character(len=24) :: telescope
    character(len=12), allocatable :: alltel(:)
    integer(kind=4) :: itel
    !
    call gfits_get_value(hfits%dict,'TELESCOP',found,telescope,error)
    if (error)  return
    if (found) then
      call cubetools_string_split(telescope,telescop_separator,alltel,error)
      if (error)  return
      call cubetools_observatory_reallocate(size(alltel),out%obs,error)
      if (error) return
      ! Split telescopes
      out%obs%ntel = size(alltel)
      do itel=1,out%obs%ntel
        call out%obs%tel(itel)%create_from_name(alltel(itel),error)
        if (error)  return
      enddo
      if (out%obs%ntel.gt.0) return
    endif
    !
    call cubetools_observatory_final(out%obs,error)
    if (error)  return
  end subroutine cubeio_hfits_export_observatory
  !
  subroutine cubeio_hfits_export_flags(hfits,family,flags,error)
    use cubetools_list
    use cubedag_flag
    use cubedag_allflags
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(fitsio_header_t), intent(in)    :: hfits
    character(len=*),      intent(inout) :: family
    type(flag_list_t),     intent(inout) :: flags
    logical,               intent(inout) :: error
    !
    integer(kind=4) :: nc
    integer(kind=4) :: icard
    logical :: found
    character(len=dag_flagl) :: name
    character(len=128) :: flagstr
    !
    ! Family
    call gfits_get_value(hfits%dict,'FAMILY',found,family,error)
    if (error)  return
    !
    ! --- 1: Collect flags from header. For simplicity, concatenate them
    ! in a single string and use the proper API to split and parse
    ! them.
    ! Because we want to preserve the order (FPROD, FACTI and FUSER might
    ! be interlaced), iterate the cards by hand, in order.
    nc = 0
    flagstr = ''
    do icard=1,hfits%dict%ncard
      if (hfits%dict%card(icard)%key(1:5).eq.'FPROD' .or.  &
          hfits%dict%card(icard)%key(1:5).eq.'FACTI' .or.  &
          hfits%dict%card(icard)%key(1:5).eq.'FUSER') then
        ! ZZZ we should also check if these keys are followed by a number
        call gfits_get_value(hfits%dict,hfits%dict%card(icard)%key,found,name,error)
        if (error)  return
        if (.not.found)  exit
        flagstr(nc+1:) = trim(name)//','
        nc = len_trim(flagstr)
      endif
    enddo
    if (nc.gt.0) then
      call flagstr_to_flags(flagstr(1:nc-1),error)
      return
    endif
    !
    ! --- 2: No FPROD/FACTI/FUSER found. Rely on XTENSION kind and
    ! name. Note: Primary HDU has no kind (always IMAGE) nor name.
    call gfits_get_value(hfits%dict,'XTENSION',found,name,error)
    if (error)  return
    if (found .and. name.eq.'IMAGE') then
      ! Use its EXTNAME as a user flag
      call gfits_get_value(hfits%dict,'EXTNAME',found,name,error)
      if (error)  return
      if (found) then
        flagstr(nc+1:) = trim(name)//','  ! Add the name as a user flag
        nc = len_trim(flagstr)
      endif
      ! This is a cube-like HDU: add 'cube' flag
      flagstr(nc+1:) = trim(flag_cube%get_name())//','  ! Add the 'cube' product flag
      nc = len_trim(flagstr)
      ! Create the flags
      call flagstr_to_flags(flagstr(1:nc-1),error)
      return
    endif
    !
    ! --- 3: Nothing set yet. Leave flags unset, let caller decide.
    continue
    !
  contains
    subroutine flagstr_to_flags(flagstr,error)
      !-----------------------------------------------------------------
      !-----------------------------------------------------------------
      character(len=*), intent(in)    :: flagstr
      logical,          intent(inout) :: error
      type(flag_t), allocatable :: allflags(:)
      !
      ! ZZZ We should have an API which directly converts from string
      ! to flag_list_t.
      call cubedag_string_toflaglist(flagstr,.true.,allflags,error,sep=',')
      if (error)  return
      call flags%create(allflags,error)
      if (error)  return
      deallocate(allflags)
    end subroutine flagstr_to_flags
  end subroutine cubeio_hfits_export_flags
  !
  subroutine cubeio_hfits_export_class_unit(hfits,out,error)
    use gkernel_interfaces
    use cubetools_brightness
    !-------------------------------------------------------------------
    ! If possible, refine the data unit (CLASS specific)
    !-------------------------------------------------------------------
    type(fitsio_header_t),         intent(in)    :: hfits
    type(cube_header_interface_t), intent(inout) :: out
    logical,                       intent(inout) :: error
    !
    real(kind=4) :: beeff,foeff
    logical :: found
    !
    if (out%array_unit.eq.'K') then
      call gfits_get_value(hfits%dict,'BEAMEFF',found,beeff,error)
      if (error)  return
      if (.not.found)  return
      call gfits_get_value(hfits%dict,'FORWEFF',found,foeff,error)
      if (error)  return
      if (.not.found)  return
      !
      ! CLASS convention:
      if (nearly_equal(beeff,foeff,1e-4)) then
        out%array_unit = brightness_get(code_unit_tas)
      else
        out%array_unit = brightness_get(code_unit_tmb)
      endif
    endif
  end subroutine cubeio_hfits_export_class_unit
  !
  subroutine cubeio_hfits_export_class_resolution(out,error)
    !-------------------------------------------------------------------
    ! If possible, set the resolution if missing (generic)
    !-------------------------------------------------------------------
    type(cube_header_interface_t), intent(inout) :: out
    logical,                       intent(inout) :: error
    !
    if (out%spatial_beam_major.gt.0.)  return  ! Already set
    !
    if (out%obs%ntel.le.0)  return  ! No telescope, can not guess
    !
    if (out%obs%ntel.gt.1)  return  ! 2 or more telescopes, can not guess
    !
    if (out%spectral_signal_value.le.0.d0)  return  ! No rest frequency, can not guess
    !
    if (out%obs%tel(1)%name.eq.'30M') then  ! Only some single-dish telescopes supported
      out%spatial_beam_major = 2.460d6/out%spectral_signal_value * rad_per_sec  ! [rad]
      out%spatial_beam_minor = out%spatial_beam_major
      out%spatial_beam_pa    = 0.
    endif
  end subroutine cubeio_hfits_export_class_resolution
  !
  subroutine cubeio_hfits_export_class_recenter(out,error)
    use gkernel_interfaces
    use gkernel_types
    !-------------------------------------------------------------------
    ! Recenter the projection center to the center of the unique pixel
    ! (CLASS specific).
    !
    ! CLASS-FITS format describe spatial coordinate as:
    !  - absolute coordinates are set into (CRVAL2,CRVAL3)
    !  - this position is set as reference pixel (CRPIX2,CRPIX3) = (0,0)
    !  - the spectrum offsets (lamof,betof) are set into (CDELT2,CDELT3)
    !    so that the (unique) pixel (1,1) is indeed at the right
    !    position.
    !-------------------------------------------------------------------
    type(cube_header_interface_t), intent(inout) :: out
    logical,                       intent(inout) :: error
    !
    type(projection_t) :: proj
    integer(kind=pixe_k) :: lref,mref
    real(kind=8) :: xinc,yinc,rxoff,ryoff,axoff,ayoff
    character(len=*), parameter :: rname='HFITS>EXPORT>RECENTER'
    !
    ! ZZZ Should add some sanity (1x1 pixels only, etc)
    !
    ! Pixel size? As indicated above, (CDELT2,CDELT3) are not the
    ! physical pixel size.
    if (out%spatial_beam_major.gt.0.) then
      xinc = out%spatial_beam_major/2.  ! [rad]
      yinc = out%spatial_beam_minor/2.  ! [rad]
      ! ZZZ What about the position angle?
    else
      call cubeio_message(seve%w,rname,'Pixel size assumed 1x1 arcsec')
      xinc = 1.d0 * rad_per_sec
      yinc = 1.d0 * rad_per_sec
    endif
    !
    ! Define input projection
    call gwcs_projec(out%spatial_projection_l0,    &
                     out%spatial_projection_m0,    &
                     out%spatial_projection_pa,    &
                     out%spatial_projection_code,  &
                     proj,error)
    if (error)  return
    !
    ! Center of central pixel of the grid:
    lref = (out%axset_dim(out%axset_ix)/2)+1  ! [pixel] Integer => center of pixel
    mref = (out%axset_dim(out%axset_iy)/2)+1  ! [pixel] Integer => center of pixel
    rxoff = (lref-out%axset_convert(code_ref,out%axset_ix)) * \
                  out%axset_convert(code_inc,out%axset_ix)  ! [rad] Relative coord
    ryoff = (mref-out%axset_convert(code_ref,out%axset_iy)) * \
                  out%axset_convert(code_inc,out%axset_iy)  ! [rad] Relative coord
    ! NB: we assume axset_convert(code_val,axset_ix/iy) is 0
    !
    ! Convert to absolute coordinates
    call rel_to_abs(proj,rxoff,ryoff,axoff,ayoff,1)
    !
  ! out%spatial_projection_code = unchanged
    out%spatial_projection_l0   = axoff
    out%spatial_projection_m0   = ayoff
  ! out%spatial_projection_pa   = unchanged (?)
    !
    out%axset_convert(code_ref,out%axset_ix) = lref
    out%axset_convert(code_val,out%axset_ix) = 0
    out%axset_convert(code_inc,out%axset_ix) = xinc
    !
    out%axset_convert(code_ref,out%axset_iy) = mref
    out%axset_convert(code_val,out%axset_iy) = 0
    out%axset_convert(code_inc,out%axset_iy) = yinc
  end subroutine cubeio_hfits_export_class_recenter
  !
  !---------------------------------------------------------------------
  !
  subroutine cubeio_hfits_import(in,family,flags,hfits,error)
    use gkernel_interfaces
    use cubetools_string
    use cubedag_flag
    !-------------------------------------------------------------------
    ! From type(cube_header_interface_t) to type(fitsio_header_t)
    !-------------------------------------------------------------------
    type(cube_header_interface_t), intent(in)    :: in
    character(len=*),              intent(in)    :: family
    type(flag_list_t),             intent(in)    :: flags
    type(fitsio_header_t),         intent(inout) :: hfits
    logical,                       intent(inout) :: error
    !
    integer(kind=ndim_k) :: ndim,iaxis
    real(kind=8) :: altrval,altrpix
    integer(kind=4) :: velrefcode,itel
    character(len=8) :: velkey
    character(len=12) :: specsyscode,altunit
    character(len=23) :: date,teles
    character(len=tele_l), allocatable :: obsname(:)
    character(len=*), parameter :: rname='HFITS>IMPORT'
    !
    call cubeio_message(ioseve%trace,rname,'Welcome')
    !
    ! By design CUBE does not insert fake dimensions before exporting to
    ! FITS format (shoud they be needed):
    call in%sanity_dim(error)
    if (error)  return
    !
    ! Data format
    hfits%mtype = in%array_type
    hfits%dtype = hfits%mtype  ! At write time no reason to write it in a different type
    ! Dimension section
    if (in%axset_ndim.le.maxdim) then
       ndim = in%axset_ndim
    else
       call hfits%message(seve%d,rname,'Larger number of dimensions than room to store it! => Truncating')
       ndim = maxdim
    endif
    hfits%ndim = ndim
    hfits%dim = 0
    hfits%dim(1:ndim) = in%axset_dim(1:ndim)
    !
    ! Other components
    hfits%dict%ncard = 0
    !
    ! Data
    call cubefitsio_header_addr4(hfits,'DATAMIN',in%array_minval,'',error)
    if (error)  return
    call cubefitsio_header_addr4(hfits,'DATAMAX',in%array_maxval,'',error)
    if (error)  return
    if (in%array_unit.eq.'') then
      call cubefitsio_header_addstr(hfits,'BUNIT','UNKNOWN','',error)
      if (error)  return
    else
      call cubefitsio_header_addstr(hfits,'BUNIT',in%array_unit,'',error)
      if (error)  return
    endif
    call cubefitsio_header_addr4(hfits,'NOISETHE',in%array_noise,'Theoretical noise',error)
    if (error)  return
    call cubefitsio_header_addr4(hfits,'NOISEMEA',in%array_rms,'Measured noise',error)
    if (error)  return
    !
    ! Axes
    do iaxis=1,ndim
      call cubeio_hfits_import_axis(in,iaxis,hfits,error)
      if (error)  return
    enddo
    !
    ! Rotation matrix
    call cubeio_hfits_import_pcmatrix(in,hfits,error)
    if (error)  return
    !
    ! Position section
    call cubefitsio_header_addstr(hfits,'OBJECT',in%spatial_source,'',error)
    if (error)  return
    select case (in%spatial_frame_code)
    case (code_spaframe_icrs)
      call cubefitsio_header_addstr(hfits,'RADESYS','ICRS','Coordinate system',error)
      if (error)  return
      call cubefitsio_header_addr8(hfits,'RA',in%spatial_projection_l0*deg_per_rad,'[deg] Right ascension',error)
      if (error)  return
      call cubefitsio_header_addr8(hfits,'DEC',in%spatial_projection_m0*deg_per_rad,'[deg] Declination',error)
      if (error)  return
    case (code_spaframe_equatorial)
      if (nearly_equal(in%spatial_frame_equinox,2000.,1e-6)) then
        call cubefitsio_header_addstr(hfits,'RADESYS','FK5','Coordinate system',error)
        if (error)  return
      elseif (nearly_equal(in%spatial_frame_equinox,1950.,1e-6)) then
        ! We are not able to distinguish FK4 vs FK4-NO-E. Leave unset.
      endif
      call cubefitsio_header_addr8(hfits,'RA',in%spatial_projection_l0*deg_per_rad,'[deg] Right ascension',error)
      if (error)  return
      call cubefitsio_header_addr8(hfits,'DEC',in%spatial_projection_m0*deg_per_rad,'[deg] Declination',error)
      if (error)  return
      call cubefitsio_header_addr4(hfits,'EQUINOX',in%spatial_frame_equinox,'',error)
      if (error)  return
    case (code_spaframe_galactic)
      call cubefitsio_header_addr8(hfits,'GLAT',in%spatial_projection_l0*deg_per_rad,'[deg] Galactic latitude',error)
      if (error)  return
      call cubefitsio_header_addr8(hfits,'GLON',in%spatial_projection_m0*deg_per_rad,'[deg] Galactic longitude',error)
      if (error)  return
    case default
      call cubefitsio_header_addcomment(hfits,'Unknown coordinate system',error)
      if (error)  return
    end select
    !
    ! Spectroscopic axis
    if (in%axset_ic.gt.0) then
      ! Spectroscopic axis is defined => try to declare ALTRPIX/ALTRVAL
      if (in%axset_name(in%axset_ic).eq.'FREQUENCY' .or.  &
          in%axset_name(in%axset_ic).eq.'VELOCITY') then
        altrpix = in%axset_convert(1,in%axset_ic)
        call cubefitsio_header_addr8(hfits,'ALTRPIX',altrpix,'',error)
        if (error)  return
        if (in%axset_name(in%axset_ic).eq.'FREQUENCY') then
          altrval = in%axset_convert(2,in%axset_ic)                 ! [MHz] Freq. at ref. pixel
          altrval = clight*(1.d0-altrval/in%spectral_signal_value)  ! [m/s] Velocity = c*(1-z)
          altunit = '[m/s]'
        else
          altrval = in%axset_convert(2,in%axset_ic)                     ! [km/s] Velocity at ref. pixel
          altrval = in%spectral_signal_value*(1.d0-altrval/clight_kms)  ! [MHz]
          altrval = altrval * 1d6                                       ! [Hz]
          altunit = '[Hz]'
        endif
        call cubefitsio_header_addr8(hfits,'ALTRVAL',altrval,altunit,error)
        if (error)  return
      endif
    endif
    ! Other components: export them "as is", should the spectroscopic axis
    ! be defined or not
    call cubefitsio_header_addstr(hfits,'LINE',in%spectral_line,'',error)
    if (error)  return
    call cubefitsio_header_addr8(hfits,'RESTFREQ',in%spectral_signal_value*1d6,'[Hz]',error)
    if (error)  return
    call cubefitsio_header_addr8(hfits,'IMAGFREQ',in%spectral_image_value*1d6,'[Hz]',error)
    if (error)  return
    select case (in%spectral_convention)
    case (code_speconv_radio)
      velrefcode = velref_convention_radio
    case (code_speconv_optical)
      velrefcode = velref_convention_optical
    case default
      call hfits%message(seve%w,rname,'Spectral convention not supported in FITS, assuming radio')
      velrefcode = velref_convention_radio
    end select
    select case (in%spectral_frame_code)
    case (code_speframe_lsrk)
      velkey = 'VELO-LSR'
      velrefcode = velrefcode+velref_frame_lsrk
      specsyscode = 'LSRK'
    case (code_speframe_helio)
      velkey = 'VELO-HEL'
      velrefcode = velrefcode+velref_frame_helio
      specsyscode = 'BARYCENT'
    case (code_speframe_obser)
      velkey = 'VELO-OBS'
      velrefcode = velrefcode+velref_frame_obs
      specsyscode = 'TOPOCENT'
    case default
      velkey = 'VELOCITY'
      velrefcode = 0
      specsyscode = ' '
    end select
    call cubefitsio_header_addr8(hfits,velkey,in%spectral_systemic_value*1d3,'[m/s]',error)
    if (error)  return
    if (velrefcode.gt.0) then
      call cubefitsio_header_addi4(hfits,'VELREF',velrefcode,'',error)
      if (error)  return
    endif
    if (specsyscode.ne.'') then
      call cubefitsio_header_addstr(hfits,'SPECSYS',specsyscode,'',error)
      if (error)  return
    endif
    !
    ! Spatial resolution
    if (in%spatial_beam_major.gt.0.0) then
      call cubefitsio_header_addr8(hfits,'BMAJ',in%spatial_beam_major*deg_per_rad,'[deg]',error)
      if (error)  return
      call cubefitsio_header_addr8(hfits,'BMIN',in%spatial_beam_minor*deg_per_rad,'[deg]',error)
      if (error)  return
      call cubefitsio_header_addr8(hfits,'BPA',in%spatial_beam_pa*deg_per_rad,'[deg]',error)
      if (error)  return
    endif
    !
    ! Observatory
    if (in%obs%ntel.gt.0) then
      allocate(obsname(in%obs%ntel))
      do itel=1,in%obs%ntel
        obsname(itel) = in%obs%tel(itel)%name
      enddo
      call cubetools_string_concat(in%obs%ntel,obsname,telescop_separator,teles,error)
      if (error)  return
      call cubefitsio_header_addstr(hfits,'TELESCOP',teles,'',error)
      if (error)  return
    endif
    !
    ! Family and flags
    call cubeio_hfits_import_flags(hfits,family,flags,error)
    if (error)  return
    !
    ! Misc
    call cubefitsio_header_addstr(hfits,'ORIGIN','GILDAS CUBE','',error)
    if (error)  return
    call sic_isodate(date)
    call cubefitsio_header_addstr(hfits,'DATE',date,'Date written',error)
    if (error)  return
  end subroutine cubeio_hfits_import
  !
  subroutine cubeio_hfits_import_axis(in,iaxis,hfits,error)
    use gbl_constant
    !-------------------------------------------------------------------
    ! Import one of the axes
    !-------------------------------------------------------------------
    type(cube_header_interface_t), intent(in)    :: in
    integer(kind=ndim_k),          intent(in)    :: iaxis
    type(fitsio_header_t),         intent(inout) :: hfits
    logical,                       intent(inout) :: error
    !
    character(len=*), parameter :: rname='HFITS>IMPORT>AXIS'
    character(len=6) :: ctype,crval,cdelt,crpix,cunit
    real(kind=8) :: val,inc,ref
    character(len=12) :: code
    logical :: projected
    integer(kind=4) :: l
    character(len=6) :: axisunit
    logical, parameter :: export_radio_as_sfl=.true.
    !
    write(ctype,'(A,I1)')  'CTYPE',iaxis
    write(crval,'(A,I1)')  'CRVAL',iaxis
    write(cdelt,'(A,I1)')  'CDELT',iaxis
    write(crpix,'(A,I1)')  'CRPIX',iaxis
    write(cunit,'(A,I1)')  'CUNIT',iaxis
    !
    if (in%spatial_projection_code.eq.code_unk .or.  &
        in%spatial_projection_code.eq.p_none) then
      projected = .false.
    else
      projected = iaxis.eq.in%axset_ix .or. iaxis.eq.in%axset_iy
    endif
    !
    ref = in%axset_convert(1,iaxis)
    val = in%axset_convert(2,iaxis)
    inc = in%axset_convert(3,iaxis)
    code = in%axset_name(iaxis)
    call sic_upper(code)  ! Case insensitive from cube_header_interface_t
    !
    if (code.eq.'L'.or.code.eq.'LII') then
      code = 'GLON'
    elseif (code.eq.'B'.or.code.eq.'BII') then
      code = 'GLAT'
    endif
    !
    if (projected) then
      ! Form is 4-3 e.g. XXXX-YYY
      l = len_trim(code)+1
      code(l:4) = '----'  ! Insert trailing - in the 4 part
      code(5:5) = '-'     ! Insert the - at the 5th position
      select case (in%spatial_projection_code)
      case (p_gnomonic)
        code(6:) = 'TAN'
      case (p_ortho)
        code(6:) = 'SIN'
      case (p_azimuthal)
        code(6:) = 'ARC'
      case (p_stereo)
        code(6:) = 'STG'
      case (p_aitoff)
        code(6:) = 'AIT'
      case (p_sfl)
        code(6:) = 'SFL'
      case (p_radio)
        ! See related IRAM Memo
        if (export_radio_as_sfl) then
          code(6:) = 'SFL'
        else
          code(6:) = 'GLS'
        endif
      case default
        code(6:) = '   '
      end select
      !
      ! Compute reference pixel so that VAL(REF) = 0
      ref = ref - val/inc
      if (iaxis.eq.in%axset_ix) then  ! X axis
        val = in%spatial_projection_l0
      else  ! Y axis
        if (in%spatial_projection_code.eq.p_radio .and. export_radio_as_sfl) then
          ! GLS is the same as SFL projection with reference declination
          ! 0 and appropriate shift of the reference pixel
          call cubeio_message(seve%w,rname,'Projection kind converted from radio to SFL')
          ref = ref-in%spatial_projection_m0/inc
          val = 0.d0
        elseif (in%spatial_projection_code.eq.p_aitoff) then
          ! Because GILDAS support for Aitoff projection implements equations
          ! ignoring the d0 reference value (i.e. it is forced on the Equator).
          val = 0.d0
        else
          val = in%spatial_projection_m0
        endif
      endif
      val = val*deg_per_rad
      inc = inc*deg_per_rad
      axisunit = 'deg'
    elseif (code.eq.'RA'   .or. code.eq.'L'    .or. code.eq.'DEC' .or.  &
            code.eq.'B'    .or. code.eq.'LII'  .or. code.eq.'BII' .or.  &
            code.eq.'GLAT' .or. code.eq.'GLON' .or. code.eq.'LAT' .or.  &
            code.eq.'LON'  .or. code.eq.'SLICE') then
       !***JP: I am adding SLICE here as a poor man work-around. The change of unit should be done
       !***JP: on the axiskind, not on the the code. This should make the code much more robust.
      val = val*deg_per_rad
      inc = inc*deg_per_rad
      axisunit = 'deg'
    elseif (code.eq.'FREQUENCY') then
      code = 'FREQ'
      val = val*1.0d6            ! MHz to Hz
      inc = inc*1.0d6
      axisunit = 'Hz'
    elseif (code.eq.'VELOCITY') then
      code = 'VRAD'              ! force VRAD instead of VELOCITY for CASA
      val = val*1.0d3            ! km/s to m/s
      inc = inc*1.0d3
      axisunit = 'm/s'
    else
      l = len_trim(code)+1
      code(l:12) = '            '    ! fills with blanks code.
      axisunit = ''
    endif
    !
    call cubefitsio_header_addstr(hfits,ctype,code,'',error)
    if (error)  return
    call cubefitsio_header_addr8(hfits,crval,val,'',error)
    if (error)  return
    call cubefitsio_header_addr8(hfits,cdelt,inc,'',error)
    if (error)  return
    call cubefitsio_header_addr8(hfits,crpix,ref,'',error)
    if (error)  return
    call cubefitsio_header_addstr(hfits,cunit,axisunit,'',error)
    if (error)  return
  end subroutine cubeio_hfits_import_axis
  !
  subroutine cubeio_hfits_import_pcmatrix(in,hfits,error)
    !-------------------------------------------------------------------
    ! Write the PC matrix describing the rotation, if any
    !-------------------------------------------------------------------
    type(cube_header_interface_t), intent(in)    :: in
    type(fitsio_header_t),         intent(inout) :: hfits
    logical,                       intent(inout) :: error
    !
    real(kind=8) :: cospa,sinpa,mat(2,2)
    integer(kind=1) :: ix,iy
    character(len=5) :: key
    logical, parameter :: export_as_cdmatrix=.false.
    character(len=2) :: matname
    character(len=*), parameter :: rname='HFITS>IMPORT>AXIS'
    !
    ! Do not write the matrix if no rotation
    if (in%spatial_projection_pa.eq.0.d0)  return
    !
    ix = in%axset_ix
    iy = in%axset_iy
    if (ix.eq.0 .or. iy.eq.0) then
      call cubeio_message(seve%w,rname,  &
        'X and/or Y spatial axes are not defined: can not export non-zero rotation angle')
      return
    endif
    !
    cospa = cos(in%spatial_projection_pa)
    sinpa = sin(in%spatial_projection_pa)
    !
    ! See e.g. https://fits.gsfc.nasa.gov/users_guide/users_guide/node57.html
    mat(1,1) =  cospa
    mat(1,2) = -sinpa * in%axset_convert(code_inc,iy)/in%axset_convert(code_inc,ix)
    mat(2,1) =  sinpa * in%axset_convert(code_inc,ix)/in%axset_convert(code_inc,iy)
    mat(2,2) =  cospa
    !
    if (export_as_cdmatrix) then  ! Only for debugging
      ! Add the increment information
      matname = 'CD'
      mat(1,1) = mat(1,1)*in%axset_convert(code_inc,1)*deg_per_rad
      mat(1,2) = mat(1,2)*in%axset_convert(code_inc,1)*deg_per_rad
      mat(2,1) = mat(2,1)*in%axset_convert(code_inc,2)*deg_per_rad
      mat(2,2) = mat(2,2)*in%axset_convert(code_inc,2)*deg_per_rad
    else
      matname = 'PC'
    endif
    !
    write(key,10) matname,ix,ix
    call cubefitsio_header_addr8(hfits,key,mat(1,1),'',error)
    if (error)  return
    write(key,10) matname,ix,iy
    call cubefitsio_header_addr8(hfits,key,mat(1,2),'',error)
    if (error)  return
    write(key,10) matname,iy,ix
    call cubefitsio_header_addr8(hfits,key,mat(2,1),'',error)
    if (error)  return
    write(key,10) matname,iy,iy
    call cubefitsio_header_addr8(hfits,key,mat(2,2),'',error)
    if (error)  return
  10 format(A2,I1,'_',I1)
  end subroutine cubeio_hfits_import_pcmatrix
  !
  subroutine cubeio_hfits_import_flags(hfits,family,flags,error)
    use cubetools_list
    use cubedag_flag
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(fitsio_header_t), intent(inout) :: hfits
    character(len=*),      intent(in)    :: family
    type(flag_list_t),     intent(in)    :: flags
    logical,               intent(inout) :: error
    !
    integer(kind=list_k) :: iflag,iprod,iacti,iuser,iunkn
    type(flag_t), pointer :: flag
    character(len=8) :: key
    character(len=32) :: comment
    character(len=*), parameter :: rname='HFITS>IMPORT>FLAGS'
    !
    call cubefitsio_header_addstr(hfits,'FAMILY',trim(family),'Cube family name',error)
    if (error)  return
    !
    iprod = 0
    iacti = 0
    iuser = 0
    iunkn = 0
    do iflag=1,flags%n
      flag => cubedag_flag_ptr(flags%list(iflag)%p,error)
      if (error)  return
      select case (flag%get_kind())
        case (code_flag_product)
          iprod = iprod+1
          write(key,'(A,I0)') 'FPROD',iprod
          write(comment,'(A,I0)') 'Product flag #',iprod
        case (code_flag_action)
          iacti = iacti+1
          write(key,'(A,I0)') 'FACTI',iacti
          write(comment,'(A,I0)') 'Action flag #',iacti
        case (code_flag_user)
          iuser = iuser+1
          write(key,'(A,I0)') 'FUSER',iuser
          write(comment,'(A,I0)') 'User flag #',iuser
        case default
          iunkn = iunkn+1
          write(key,'(A,I0)') 'FLAG',iunkn
          write(comment,'(A,I0)') 'Unknown flag #',iunkn
      end select
      call cubefitsio_header_addstr(hfits,key,flag%get_name(),comment,error)
      if (error)  return
    enddo
  end subroutine cubeio_hfits_import_flags
end module cubeio_header_hfits
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeio_fits
  use cubetools_parameters
  use cubetools_header_types
  use cubefitsio_header
  use cubeio_messaging
  use cubeio_header_hfits
  use cubeio_range

  public :: cubeio_create_hfits,cubeio_create_and_truncate_hfits
  public :: cubeio_fits_read_data
  public :: cubeio_fits_write_cube
  public :: cubeio_fits_write_data
  private

  interface cubeio_fits_read_data
    module procedure cubeio_fits_read_data_r4
    module procedure cubeio_fits_read_data_c4
  end interface cubeio_fits_read_data

  interface cubeio_fits_write_cube
    module procedure cubeio_fits_write_cube_r4
    module procedure cubeio_fits_write_cube_c4
  end interface cubeio_fits_write_cube

  interface cubeio_fits_write_data
    module procedure cubeio_fits_write_data_r4
    module procedure cubeio_fits_write_data_c4
  end interface cubeio_fits_write_data

contains

  subroutine cubeio_create_hfits(dno,oorder,hfits,error)
    use cubedag_node_type
    !---------------------------------------------------------------------
    ! Create or recreate the hfits by converting the header with requested
    ! order.
    !---------------------------------------------------------------------
    class(cubedag_node_object_t), intent(inout) :: dno
    integer(kind=code_k),         intent(in)    :: oorder
    type(fitsio_header_t),        intent(inout) :: hfits
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='CREATE>HFITS'
    !
    call cubeio_header_put_tohfits(dno,oorder,hfits,error)
    if (error)  return
  end subroutine cubeio_create_hfits
  !
  subroutine cubeio_create_and_truncate_hfits(dno,oorder,dim3,hfits,error)
    use cubedag_node_type
    !---------------------------------------------------------------------
    ! Same as cubeio_create_hgdf, but truncate the 3rd dimension to
    ! requested size.
    !---------------------------------------------------------------------
    class(cubedag_node_object_t), intent(inout) :: dno
    integer(kind=code_k),         intent(in)    :: oorder
    integer(kind=data_k),         intent(in)    :: dim3
    type(fitsio_header_t),        intent(inout) :: hfits
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='CREATE>AND>TRUNCATE>HFITS'
    !
    call cubeio_header_truncate_and_put_tohfits(dno,oorder,dim3,hfits,error)
    if (error)  return
  end subroutine cubeio_create_and_truncate_hfits

  subroutine cubeio_fits_read_data_r4(hfits,data,range,error)
    use cubefitsio_image_read
    !-------------------------------------------------------------------
    !-------------------------------------------------------------------
    type(fitsio_header_t), intent(in)    :: hfits
    real(kind=4),          intent(out)   :: data(:,:,:)
    type(cubeio_range_t),  intent(in)    :: range
    logical,               intent(inout) :: error
    !
    call cubefitsio_image_dataread(hfits,data,range%blc,range%trc,error)
    if (error)  return
  end subroutine cubeio_fits_read_data_r4

  subroutine cubeio_fits_read_data_c4(hfits,data,range,error)
    !-------------------------------------------------------------------
    !-------------------------------------------------------------------
    type(fitsio_header_t), intent(in)    :: hfits
    complex(kind=4),       intent(out)   :: data(:,:,:)
    type(cubeio_range_t),  intent(in)    :: range
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='FITS>READ>DATA>C4'
    !
    call cubeio_message(seve%e,rname,'Reading COMPLEX*4 data from FITS is not implemented')
    error = .true.
    return
  end subroutine cubeio_fits_read_data_c4

  subroutine cubeio_fits_write_cube_r4(hfits,filename,dochecksum,data,error)
    use cubefitsio_image_write
    use cubefitsio_image_utils
    !---------------------------------------------------------------------
    ! Write the whole cube to disk under FITS format
    !---------------------------------------------------------------------
    type(fitsio_header_t), intent(inout) :: hfits
    character(len=*),      intent(in)    :: filename
    logical,               intent(in)    :: dochecksum
    real(kind=4),          intent(in)    :: data(:,:,:)
    logical,               intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='FITS>WRITE>CUBE>R4'
    type(cubeio_range_t) :: range
    !
    call cubefitsio_image_create(hfits,filename,dochecksum,error)
    if (error)  return
    ! Location of data
    range%blc(:) = 0
    range%trc(:) = 0
    call cubeio_fits_write_data_r4(hfits,data,range,error)
    if (error)  continue
    call cubefitsio_image_close(hfits,error)
    if (error)  return
  end subroutine cubeio_fits_write_cube_r4

  subroutine cubeio_fits_write_cube_c4(hfits,filename,dochecksum,data,error)
    use cubefitsio_image_write
    use cubefitsio_image_utils
    !---------------------------------------------------------------------
    ! Write the whole cube to disk under FITS format
    !---------------------------------------------------------------------
    type(fitsio_header_t), intent(inout) :: hfits
    character(len=*),      intent(in)    :: filename
    logical,               intent(in)    :: dochecksum
    complex(kind=4),       intent(in)    :: data(:,:,:)
    logical,               intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='FITS>WRITE>CUBE>C4'
    type(cubeio_range_t) :: range
    !
    call cubefitsio_image_create(hfits,filename,dochecksum,error)
    if (error)  return
    ! Location of data
    range%blc(:) = 0
    range%trc(:) = 0
    call cubeio_fits_write_data_c4(hfits,data,range,error)
    if (error)  continue
    call cubefitsio_image_close(hfits,error)
    if (error)  return
  end subroutine cubeio_fits_write_cube_c4

  subroutine cubeio_fits_write_data_r4(hfits,data,range,error)
    use cubefitsio_image_write
    !-------------------------------------------------------------------
    !-------------------------------------------------------------------
    type(fitsio_header_t), intent(in)    :: hfits
    real(kind=4),          intent(in)    :: data(:,:,:)
    type(cubeio_range_t),  intent(in)    :: range
    logical,               intent(inout) :: error
    !
    call cubefitsio_image_datawrite(hfits,data,range%blc,range%trc,error)
    if (error)  return
  end subroutine cubeio_fits_write_data_r4

  subroutine cubeio_fits_write_data_c4(hfits,data,range,error)
    !-------------------------------------------------------------------
    !-------------------------------------------------------------------
    type(fitsio_header_t), intent(in)    :: hfits
    complex(kind=4),       intent(in)    :: data(:,:,:)
    type(cubeio_range_t),  intent(in)    :: range
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='FITS>WRITE>DATA>C4'
    !
    call cubeio_message(seve%e,rname,'Exporting COMPLEX*4 data to FITS is not implemented')
    error = .true.
    return
  end subroutine cubeio_fits_write_data_c4

end module cubeio_fits
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
