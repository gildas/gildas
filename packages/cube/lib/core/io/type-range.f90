module cubeio_range
  use cubetools_parameters
  !---------------------------------------------------------------------
  ! Type used to describe (and access) a subset of the data
  !---------------------------------------------------------------------

  type cubeio_range_t
    integer(kind=indx_k) :: blc(maxdim)
    integer(kind=indx_k) :: trc(maxdim)
  end type cubeio_range_t

  public :: cubeio_range_t
  private

end module cubeio_range
