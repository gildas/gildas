!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage CUBE cdf messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubecdf_messaging
  use gpack_def
  use gbl_message
  use cubetools_parameters
  !
  public :: seve,cdfseve
  public :: cubecdf_message_set_id,cubecdf_message,cubecdf_error
  public :: cubecdf_message_set_trace,cubecdf_message_get_trace
  public :: cubecdf_message_set_others,cubecdf_message_get_others
  private
  !
  ! Identifier used for message identification
  integer(kind=4) :: cubecdf_message_id = gpack_global_id  ! Default value for startup message
  !
  type :: cubecdf_messaging_debug_t
     integer(kind=code_k) :: trace = seve%t
     integer(kind=code_k) :: others = seve%d
  end type cubecdf_messaging_debug_t
  type(cubecdf_messaging_debug_t) :: cdfseve
  !
contains
  !
  subroutine cubecdf_message_set_id(id)
    !---------------------------------------------------------------------
    ! Alter library id into input id. Should be called by the library
    ! which wants to share its id with the current one.
    !---------------------------------------------------------------------
    integer(kind=4), intent(in) :: id
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='MESSAGE>SET>ID'
    !
    cubecdf_message_id = id
    write (mess,'(A,I3)') 'Now use id #',cubecdf_message_id
    call cubecdf_message(seve%d,rname,mess)
  end subroutine cubecdf_message_set_id
  !
  subroutine cubecdf_message(mkind,procname,message)
    use cubetools_cmessaging
    !---------------------------------------------------------------------
    ! Messaging facility for the current library. Calls the low-level
    ! (internal) messaging routine with its own identifier.
    !---------------------------------------------------------------------
    integer(kind=4),  intent(in) :: mkind     ! Message kind
    character(len=*), intent(in) :: procname  ! Name of calling procedure
    character(len=*), intent(in) :: message   ! Message string
    !
    call cubetools_cmessage(cubecdf_message_id,mkind,'CDF>'//procname,message)
  end subroutine cubecdf_message
  !
  function cubecdf_error(procname,status,error)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubecdf_error  ! Function value on return
    character(len=*), intent(in)    :: procname
    integer(kind=4),  intent(in)    :: status
    logical,          intent(inout) :: error
    ! Local
    character(len=message_length) :: mess
    !
    cubecdf_error = status.ne.0
    if (cubecdf_error) then
      error = .true.
      call ftgerr(status,mess)
      call cubecdf_message(seve%e,procname,mess)
    endif
    !
  end function cubecdf_error
  !
  subroutine cubecdf_message_set_trace(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       cdfseve%trace = seve%i
    else
       cdfseve%trace = seve%t
    endif
  end subroutine cubecdf_message_set_trace
  !
  subroutine cubecdf_message_set_others(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       cdfseve%others = seve%i
    else
       cdfseve%others = seve%d
    endif
  end subroutine cubecdf_message_set_others
  !
  function cubecdf_message_get_trace()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubecdf_message_get_trace
    !
    cubecdf_message_get_trace = cdfseve%trace.eq.seve%i
    !
  end function cubecdf_message_get_trace
  !
  function cubecdf_message_get_others()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubecdf_message_get_others
    !
    cubecdf_message_get_others = cdfseve%others.eq.seve%i
    !
  end function cubecdf_message_get_others
  !
end module cubecdf_messaging
!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
