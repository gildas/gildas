module cubecdf_image_write
  use cubecdf_header
  use cubecdf_messaging
  !---------------------------------------------------------------------
  ! Support module to write CDF 'images' (e.g. 2D images or 3D cubes)
  !---------------------------------------------------------------------

  public :: cubecdf_image_create,cubecdf_image_header_update,cubecdf_image_datawrite
  private

contains

  subroutine cubecdf_image_create(hcdf,filename,error)
    use gkernel_interfaces
    use cubetools_parameters
    !-------------------------------------------------------------------
    ! Create a new IMAGE-FITS file on disk.
    !-------------------------------------------------------------------
    type(cdf_header_t), intent(inout) :: hcdf
    character(len=*),   intent(in)    :: filename
    logical,            intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='IMAGE>CREATE'
    !
    if (gag_inquire(filename,len_trim(filename)).eq.0)  &
      call gag_filrm(filename(1:len_trim(filename)))
    !
    call hcdf%init(filename,error)
    if (error)  goto 10
    !
    ! Write header here
    !  ...
    !
    return
    !
  10 continue
    call hcdf%close(error)
    !
  end subroutine cubecdf_image_create

  subroutine cubecdf_image_header_update(hcdf,error)
    !-------------------------------------------------------------------
    ! Update the header in the current CDF file
    !-------------------------------------------------------------------
    type(cdf_header_t), intent(in)    :: hcdf
    logical,            intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='IMAGE>HEADER>UPDATE'
    !
    ! No header yet
    !
  end subroutine cubecdf_image_header_update

  subroutine cubecdf_image_datawrite(hcdf,data,iblc,itrc,error)
    use gkernel_interfaces
    use cubetools_parameters
    use cubecdf_image_utils
    !-------------------------------------------------------------------
    ! Write a contiguous piece of data to the output file
    !-------------------------------------------------------------------
    type(cdf_header_t),         intent(in)    :: hcdf
    real(kind=4),               intent(in)    :: data(*)
    integer(kind=index_length), intent(in)    :: iblc(maxdim)
    integer(kind=index_length), intent(in)    :: itrc(maxdim)
    logical,                    intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='IMAGE>DATAWRITE'
    integer(kind=size_length) :: nelements,buflen,ifirst,ilast
    integer(kind=4) :: ier
    !
    call cubecdf_image_subset(hcdf,iblc,itrc,nelements,error)
    if (error)  return
    !
    buflen = 256*1024  ! [words] Default 1 MB writing buffer
    ier = sic_getlog('CUBE_CDF_BUFLEN',buflen)
    !
    ! Note: stream-writing all at once does not seem to make much
    ! difference.
    ifirst = 1
    do while (ifirst.lt.nelements)
      ilast = min(ifirst+buflen-1,nelements)
      write(hcdf%unit,iostat=ier)  data(ifirst:ilast)
      if (ier.ne.0) then
        call cubecdf_message(seve%e,rname,'Write error')
        error = .true.
        exit
      endif
      ifirst = ifirst+buflen
    enddo
  end subroutine cubecdf_image_datawrite

end module cubecdf_image_write
