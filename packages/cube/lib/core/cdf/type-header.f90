!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubecdf_header
  use gkernel_interfaces
  use cubetools_parameters
  use cubecdf_messaging
  !
  type :: cdf_header_t
    integer(kind=4)    :: unit=0       ! Logical unit for associated CDF file
    character(len=512) :: id=strg_unk  ! Some identifier string (for feedback)
    ! Components which have an effect on the FITS reading or writing
!     integer(kind=code_k) :: action=code_null
    integer(kind=code_k) :: type=code_null  ! Type of data (e.g. R*4, C*4, etc)
    integer(kind=ndim_k) :: ndim
    integer(kind=data_k) :: dim(maxdim)
  contains
    procedure :: init    => cubecdf_header_init
!     procedure :: open    => cubecdf_header_open
    procedure :: close   => cubecdf_header_close
    procedure :: message => cubecdf_header_message
  end type cdf_header_t
  !
  public :: cdf_header_t
  private
  !
contains
  !
  subroutine cubecdf_header_init(hcdf,filename,error)
    use gbl_format
    !-------------------------------------------------------------------
    ! Create and open a new CDF file
    !-------------------------------------------------------------------
    class(cdf_header_t), intent(inout) :: hcdf
    character(len=*),    intent(in)    :: filename
    logical,             intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='INIT'
    integer(kind=4) :: ier
    !
    ier = sic_getlun(hcdf%unit)
    if (mod(ier,2).eq.0) then
      error = .true.
      return
    endif
    !
    hcdf%id = strg_unk
    !
    open(unit=hcdf%unit,      &
         file=filename,       &
         access='STREAM',     &
         form='UNFORMATTED',  &
         status='NEW',        &
         action='READWRITE',  &
         iostat=ier)
    if (ier.ne.0) then
      call cubecdf_message(seve%e,rname,'Open error file '//filename)
      error = .true.
      return
    endif
  end subroutine cubecdf_header_init
  !
!   subroutine cubecdf_header_open(hfits,filename,id,error)
!     !-------------------------------------------------------------------
!     ! Open an old FITS file
!     !-------------------------------------------------------------------
!     class(cdf_header_t), intent(inout) :: hfits
!     character(len=*),       intent(in)    :: filename
!     character(len=*),       intent(in)    :: id  ! Some identifier string
!     logical,                intent(inout) :: error
!     ! Local
!     character(len=*), parameter :: rname='INIT'
!     integer(kind=4) :: ier,status,blocksize
!     integer(kind=4), parameter :: readonly=0
!     !
!     if (gag_inquire(filename,len_trim(filename)).ne.0) then
!       call cubecdf_message(seve%e,rname,'No such file '//filename)
!       error = .true.
!       return
!     endif
!     !
!     ier = sic_getlun(hfits%unit)
!     if (mod(ier,2).eq.0) then
!       error = .true.
!       return
!     endif
!     !
!     hfits%action = code_read
!     hfits%id = id
!     status = 0
!     call ftopen(hfits%unit,filename,readonly,blocksize,status)
!     if (cubecdf_error(rname,status,error))  return
!   end subroutine cubecdf_header_open
  !
  subroutine cubecdf_header_close(hcdf,error)
    !-------------------------------------------------------------------
    ! Close a (new or old) FITS file
    !-------------------------------------------------------------------
    class(cdf_header_t), intent(inout) :: hcdf
    logical,                intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='CLOSE'
    integer(kind=4) :: ier
    !
    if (hcdf%unit.eq.0)  return  ! Already closed
    !
    ier = sic_flush(hcdf%unit)
    if (ier.ne.0) then
      call cubecdf_message(seve%e,rname,'Error flushing file')
      error = .true.
      return
    endif
    !
    close(unit=hcdf%unit,iostat=ier)
    if (ier.ne.0) then
      call cubecdf_message(seve%e,rname,'Error closing file')
      error = .true.
      return
    endif
    !
    call sic_frelun(hcdf%unit)
    hcdf%unit = 0
    hcdf%id = strg_unk
  end subroutine cubecdf_header_close
  !
  subroutine cubecdf_header_message(hcdf,seve,rname,mess)
    !-------------------------------------------------------------------
    ! Subroutine dedicated to messages customized for the current hcdf
    ! object
    !-------------------------------------------------------------------
    class(cdf_header_t), intent(in) :: hcdf
    integer(kind=4),     intent(in) :: seve
    character(len=*),    intent(in) :: rname
    character(len=*),    intent(in) :: mess
    !
    call cubecdf_message(seve,rname,  &
      'Object '//trim(hcdf%id)//' > '//mess)
  end subroutine cubecdf_header_message
end module cubecdf_header
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
