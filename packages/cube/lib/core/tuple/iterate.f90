module cubetuple_iterate
  use cubetools_parameters
  use cube_types
  use cubetuple_transpose

  public :: cubetuple_iterate_chan,cubetuple_iterate_pix,cubetuple_iterate_subcube
  private

contains

  subroutine cubetuple_iterate_chan(fchan,lchan,cub,error)
    use cubeio_chan
    !---------------------------------------------------------------------
    ! Prepare buffers for working (at least) on the requested channel
    ! range
    !---------------------------------------------------------------------
    integer(kind=chan_k), intent(in)    :: fchan
    integer(kind=chan_k), intent(in)    :: lchan
    type(cube_t),         intent(inout) :: cub
    logical,              intent(inout) :: error
    !
    call cubetuple_autotranspose_cube(cub,error)
    if (error)  return
    call cubeio_iterate_chan(cub%user,cub%prog,cub,cub%tuple%current,fchan,lchan,error)
    if (error)  return
  end subroutine cubetuple_iterate_chan

  subroutine cubetuple_iterate_pix(fypix,lypix,cub,error)
    use cubeio_pix
    !---------------------------------------------------------------------
    ! Prepare buffers for working (at least) on the requested Y pixel row
    ! range
    !---------------------------------------------------------------------
    integer(kind=pixe_k), intent(in)    :: fypix
    integer(kind=pixe_k), intent(in)    :: lypix
    type(cube_t),         intent(inout) :: cub
    logical,              intent(inout) :: error
    !
    call cubetuple_autotranspose_cube(cub,error)
    if (error)  return
    call cubeio_iterate_pix(cub%user,cub%prog,cub,cub%tuple%current,fypix,lypix,error)
    if (error)  return
  end subroutine cubetuple_iterate_pix

  subroutine cubetuple_iterate_subcube(first,last,cub,error)
    use cubeio_subcube
    !---------------------------------------------------------------------
    ! Prepare buffers for working (at least) on the requested plane
    ! range
    !---------------------------------------------------------------------
    integer(kind=data_k), intent(in)    :: first
    integer(kind=data_k), intent(in)    :: last
    type(cube_t),         intent(inout) :: cub
    logical,              intent(inout) :: error
    !
    call cubetuple_autotranspose_cube(cub,error)
    if (error)  return
    call cubeio_iterate_subcube(cub%user,cub%prog,cub,cub%tuple%current,first,last,error)
    if (error)  return
  end subroutine cubetuple_iterate_subcube

end module cubetuple_iterate
