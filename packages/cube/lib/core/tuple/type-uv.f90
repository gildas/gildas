!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetuple_uv
  use cubetools_parameters
  use cubedag_node_type
  use cubetuple_table
  !---------------------------------------------------------------------
  ! Support module for UV tables
  !---------------------------------------------------------------------
  !
  type, extends(table_t) :: uv_t
    ! ...
  contains
    ! ...
  end type uv_t
  !
  public :: uv_t
  public :: uv_allocate_new
  private
  !
contains
  !
  function uv_allocate_new(setup,error)
    use gkernel_interfaces
    use cubetools_setup_types
    !-------------------------------------------------------------------
    ! Allocate a new uv_t in memory and return a pointer to this
    ! allocation
    !-------------------------------------------------------------------
    type(uv_t), pointer :: uv_allocate_new
    type(cube_setup_t), intent(in), target :: setup
    logical,            intent(inout)      :: error
    !
    integer(kind=4) :: ier
    character(len=*), parameter :: rname='ALLOCATE>NEW'
    !
    allocate(uv_allocate_new,stat=ier)
    if (failed_allocate(rname,'object',ier,error)) return
    call uv_allocate_new%init(setup,error)
    if (error)  return
    !
    ! Override the node_t methods with cube_t specific ones
    uv_allocate_new%ltype    => uv_ltype
    uv_allocate_new%memsize  => uv_memsize
    uv_allocate_new%disksize => uv_disksize
    uv_allocate_new%datasize => uv_datasize
  end function uv_allocate_new
  !
  function uv_ltype(obj)
    use cubetools_axset_types
    character(len=2) :: uv_ltype
    class(cubedag_node_object_t), intent(in) :: obj
    select type (obj)
    type is (uv_t)
      uv_ltype = 'UV'
    class default
      uv_ltype = '??'
    end select
  end function uv_ltype

  function uv_memsize(obj)
    integer(kind=size_length) :: uv_memsize
    class(cubedag_node_object_t), intent(in) :: obj
    select type (obj)
    type is (uv_t)
      uv_memsize = obj%tuple%memsize()
    class default
      uv_memsize = 0
    end select
  end function uv_memsize

  function uv_disksize(obj)
    use cubedag_tuple
    integer(kind=size_length) :: uv_disksize
    class(cubedag_node_object_t), intent(in) :: obj
    select type (obj)
    type is (uv_t)
      uv_disksize = obj%node%tuple%disksizes()
    class default
      uv_disksize = 0
    end select
  end function uv_disksize

  function uv_datasize(obj)
    integer(kind=size_length) :: uv_datasize
    class(cubedag_node_object_t), intent(in) :: obj
    logical :: error
    select type (obj)
    type is (uv_t)
      error = .false.
      call obj%head%arr%datasize(uv_datasize,error)
    class default
      uv_datasize = 0
    end select
  end function uv_datasize
  !
end module cubetuple_uv
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
