!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetuple_iterator
  use cubetools_parameters
  use cubetools_axis_types
  use cubetuple_messaging
  !---------------------------------------------------------------------
  ! - This type describes how to iterate entry by entry a cube_t (or a
  !   format_t in general).
  ! - It is reevaluated at each command, depending on the size of the
  !   problem, the buffer sizes, the access mode, regions, etc.
  ! - In the general case it is different from one cube to another
  !
  ! We refer here to:
  !  - "this cube": the cube we want to access the data,
  !  - the "reference cube": usually an output cube which is iterated by
  !    the command. The other cubes (including "this one") are described
  !    respectively to the reference.
  !---------------------------------------------------------------------
  !
  integer(kind=code_k), parameter :: code_align_auto=1
  integer(kind=code_k), parameter :: code_align_channel=2
  !
  ! Duplicate of firstlaststride_t:
  type :: cubetuple_iterator_region_t
    integer(kind=indx_k) :: first
    integer(kind=indx_k) :: last
    integer(kind=indx_k) :: stride
    integer(kind=indx_k) :: n
  end type cubetuple_iterator_region_t
  !
  type :: cubetuple_iterator_axis_t
    type(axis_t),                      private :: axis
    type(cubetuple_iterator_region_t), private :: region
  end type cubetuple_iterator_axis_t
  !
  type :: cubetuple_iterator_axset_t
    logical,                                  private :: ready = .false.
    type(cubetuple_iterator_axis_t),          private :: l
    type(cubetuple_iterator_axis_t),          private :: m
    type(cubetuple_iterator_axis_t),          private :: c
    type(cubetuple_iterator_axis_t), pointer, private :: first
    type(cubetuple_iterator_axis_t), pointer, private :: second
    type(cubetuple_iterator_axis_t), pointer, private :: third
  end type cubetuple_iterator_axset_t
  !
  type :: cubetuple_iterator_t
    type(cubetuple_iterator_axset_t),          private :: this
    type(cubetuple_iterator_axset_t), pointer, private :: ref => null()
  contains
    procedure, private :: this_is_ready  => iterator_thisready  ! Unambiguous private name
    procedure, private :: ref_is_ready   => iterator_refready   ! Unambiguous private name
    procedure, public  :: ready          => iterator_thisready  ! Public name
    procedure, public  :: finish         => iterator_finish
    ! Setup procedures
    procedure, public :: get_axes        => iterator_get_axes
    procedure, public :: set_region_l    => iterator_set_region_l
    procedure, public :: set_region_m    => iterator_set_region_m
    procedure, public :: set_region_c    => iterator_set_region_c
    procedure, public :: set_reference   => iterator_set_reference
    ! Iteration procedures
    procedure, public :: compute_nplane_total      => iterator_compute_nplane_total
    procedure, public :: compute_nplane_per_buffer => iterator_compute_nplane_per_buffer
    ! Computation procedures
    procedure, public :: range           => iterator_range
    procedure, public :: fullrange       => iterator_fullrange
    procedure, public :: image_number    => iterator_image_number
    procedure, public :: image_size      => iterator_image_size
    procedure, public :: image_region    => iterator_image_region
    procedure, public :: spectrum_number => iterator_spectrum_number
    procedure, public :: spectrum_size   => iterator_spectrum_size
    procedure, public :: spectrum_region => iterator_spectrum_region
    procedure, public :: subcube_size    => iterator_subcube_size
    procedure, public :: subcube_region  => iterator_subcube_region
  end type cubetuple_iterator_t
  !
  private
  public :: cubetuple_iterator_t
  public :: code_align_auto,code_align_channel
  !
contains
  !
  function iterator_thisready(iter,caller,error)
    !-------------------------------------------------------------------
    ! Return true if the iterator is ready for use regarding 'this'
    ! cube (i.e. as long as the 'ref' cube is not needed).
    !-------------------------------------------------------------------
    logical :: iterator_thisready
    class(cubetuple_iterator_t), intent(in)    :: iter
    character(len=*),            intent(in)    :: caller
    logical,                     intent(inout) :: error
    !
    character(len=*), parameter :: rname='ITERATOR>THIS>READY'
    !
    iterator_thisready = iter%this%ready
    if (.not.iterator_thisready) then
      call cubetuple_message(seve%e,rname,  &
        'Internal error: this cube iterator is not ready for use by '//caller)
      error = .true.
      return
    endif
  end function iterator_thisready
  !
  function iterator_refready(iter,caller,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    logical :: iterator_refready
    class(cubetuple_iterator_t), intent(in)    :: iter
    character(len=*),            intent(in)    :: caller
    logical,                     intent(inout) :: error
    !
    character(len=*), parameter :: rname='ITERATOR>THIS>READY'
    !
    iterator_refready = .false.
    if (associated(iter%ref)) then
      iterator_refready = iter%ref%ready
    endif
    if (.not.iterator_refready) then
      call cubetuple_message(seve%e,rname,  &
        'Internal error: the reference cube iterator is not ready for use by '//caller)
      error = .true.
      return
    endif
  end function iterator_refready
  !
  subroutine iterator_finish(iter,error)
    !-------------------------------------------------------------------
    ! Finish (in the format_t sense) the iterator, i.e. it can not be
    ! used anymore until the next command is processed.
    !
    ! This is not a finalization routine.
    !-------------------------------------------------------------------
    class(cubetuple_iterator_t), intent(inout) :: iter
    logical,                     intent(inout) :: error
    !
    ! Forget the reference cube (since it changes from 1 command to
    ! another)
    iter%ref => null()
    !
    ! Should we also forget the 1/2/3 axes? Since this may change
    ! depending on access requested by command.
  end subroutine iterator_finish
  !
  !---------------------------------------------------------------------
  !
  subroutine iterator_get_axes(iter,head,order,align,error)
    use cubetools_header_types
    use cubetools_header_methods
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(cubetuple_iterator_t), target, intent(inout) :: iter
    type(cube_header_t),                 intent(in)    :: head
    integer(kind=code_k),                intent(in)    :: order
    integer(kind=code_k),                intent(in)    :: align
    logical,                             intent(inout) :: error
    !
    character(len=*), parameter :: rname='ITERATOR>GET>AXES'
    !
    call cubetools_header_get_axis_head_l(head,iter%this%l%axis,error)
    if (error)  return
    call cubetools_header_get_axis_head_m(head,iter%this%m%axis,error)
    if (error)  return
    call cubetools_header_get_axis_head_c(head,iter%this%c%axis,error)
    if (error)  return
    if (order.eq.code_cube_imaset) then
      iter%this%first  => iter%this%l
      iter%this%second => iter%this%m
      iter%this%third  => iter%this%c
    elseif (order.eq.code_cube_speset) then
      iter%this%first  => iter%this%c
      iter%this%second => iter%this%l
      iter%this%third  => iter%this%m
    else
      call cubetuple_message(seve%e,rname,'Unexpected order')
      error = .true.
      return
    endif
    select case (align)
    case (code_align_auto)
      ! Leave 3rd axis as is
      continue
    case (code_align_channel)
      ! Third axes ported to channel (or pixel) unit for exact alignment
      ! Note: there should be a cubetools_header_get_axis_head_channel for this purpose
      iter%this%third%axis%ref = 1
      iter%this%third%axis%val = 1
      iter%this%third%axis%inc = 1
    end select
    iter%this%ready = .true.
  end subroutine iterator_get_axes
  !
  subroutine iterator_set_region_l(iter,first,last,stride,error)
    !-------------------------------------------------------------------
    ! Set the L region, if any
    !-------------------------------------------------------------------
    class(cubetuple_iterator_t), intent(inout) :: iter
    integer(kind=indx_k),        intent(in)    :: first,last,stride
    logical,                     intent(inout) :: error
    !
    call iterator_set_region(iter%this%l,first,last,stride,error)
    if (error)  return
  end subroutine iterator_set_region_l
  !
  subroutine iterator_set_region_m(iter,first,last,stride,error)
    !-------------------------------------------------------------------
    ! Set the M region, if any
    !-------------------------------------------------------------------
    class(cubetuple_iterator_t), intent(inout) :: iter
    integer(kind=indx_k),        intent(in)    :: first,last,stride
    logical,                     intent(inout) :: error
    !
    call iterator_set_region(iter%this%m,first,last,stride,error)
    if (error)  return
  end subroutine iterator_set_region_m
  !
  subroutine iterator_set_region_c(iter,first,last,stride,error)
    !-------------------------------------------------------------------
    ! Set the C region, if any
    !-------------------------------------------------------------------
    class(cubetuple_iterator_t), intent(inout) :: iter
    integer(kind=indx_k),        intent(in)    :: first,last,stride
    logical,                     intent(inout) :: error
    !
    call iterator_set_region(iter%this%c,first,last,stride,error)
    if (error)  return
  end subroutine iterator_set_region_c
  !
  subroutine iterator_set_region(this,first,last,stride,error)
    !-------------------------------------------------------------------
    ! Set the given region, if any
    !-------------------------------------------------------------------
    type(cubetuple_iterator_axis_t), intent(inout) :: this
    integer(kind=indx_k),            intent(in)    :: first,last,stride
    logical,                         intent(inout) :: error
    !
    if (first.eq.code_indx_auto) then
      this%region%first = 1
    else
      this%region%first = first
    endif
    if (last.eq.code_indx_auto) then
      this%region%last = this%axis%n
    else
      this%region%last = last
    endif
    if (stride.eq.code_indx_auto) then
      this%region%stride = 1
    else
      this%region%stride = stride
    endif
    this%region%n = this%region%last-this%region%first+1
  end subroutine iterator_set_region
  !
  subroutine iterator_set_reference(thisiter,refiter,error)
    !-------------------------------------------------------------------
    ! Declare the reference cube axes.
    !-------------------------------------------------------------------
    class(cubetuple_iterator_t),         intent(inout) :: thisiter
    type(cubetuple_iterator_t),  target, intent(in)    :: refiter
    logical,                             intent(inout) :: error
    !
    ! For simplicity use a pointer. This typically makes an association
    ! of the kind:
    !   incube%iter%ref => oucube%iter%this
    ! which should be valid as long as long oucube is alive, i.e.
    ! during the command processing time.
    thisiter%ref => refiter%this

  end subroutine iterator_set_reference
  !
  !---------------------------------------------------------------------
  !
  function iterator_compute_nplane_total(iter,error)
    !-------------------------------------------------------------------
    ! Return the total number of reference planes to be iterated
    !-------------------------------------------------------------------
    integer(kind=indx_k) :: iterator_compute_nplane_total
    class(cubetuple_iterator_t), intent(in)    :: iter
    logical,                     intent(inout) :: error
    !
    character(len=*), parameter :: rname='ITERATOR>COMPUTE>NPLANE>TOTAL'
    !
    iterator_compute_nplane_total = 0
    if (.not.iter%ref_is_ready(rname,error))  return
    iterator_compute_nplane_total = iter%ref%third%axis%n
  end function iterator_compute_nplane_total
  !
  function iterator_compute_nplane_per_buffer(iter,bufsize,error)
    !-------------------------------------------------------------------
    ! For the given memory size and for this cube, return the maximum
    ! number of REFERENCE planes which can be processed.
    !-------------------------------------------------------------------
    integer(kind=indx_k) :: iterator_compute_nplane_per_buffer
    class(cubetuple_iterator_t), intent(in)    :: iter
    real(kind=4),                intent(in)    :: bufsize
    logical,                     intent(inout) :: error
    !
    integer(kind=data_k) :: planesize
    real(kind=4) :: ntpprp  ! Number of this plane per reference plane
    real(kind=4) :: nrppb   ! Number of reference plane per buffer
    character(len=*), parameter :: rname='ITERATOR>COMPUTE>NPLANE>PER>BUFFER'
    !
    iterator_compute_nplane_per_buffer = 0
    if (.not.iter%this_is_ready(rname,error))  return
    if (.not.iter%ref_is_ready(rname,error))  return
    !
    ntpprp = iter%ref%third%axis%inc/iter%this%third%axis%inc  ! Can be <, =, or > 1
    !
    ! Note: use the full axis, not the region, because the libio puts
    ! the whole block in buffer, not only the region.
    ! ZZZ Need this data type, do not assume R*4:
    planesize = iter%this%first%axis%n * iter%this%second%axis%n * 4 ! [Bytes] Size of one plane of this cube
    !
    nrppb = bufsize/planesize/ntpprp  ! Floating point
    !
    if (nrppb.lt.1.0) then
      ! Allow for at least one, even if this means consuming more memory
      iterator_compute_nplane_per_buffer = 1
    elseif (nrppb.gt.iter%this%third%axis%n) then
      iterator_compute_nplane_per_buffer = iter%this%third%axis%n
    else
      ! Use floor before bufsize should not be exceeded if possible
      iterator_compute_nplane_per_buffer = floor(nrppb)
    endif
  end function iterator_compute_nplane_per_buffer
  !
  subroutine iterator_range(iter,refrange,truncate,thisrange,error)
    use gkernel_interfaces
    !-------------------------------------------------------------------
    !  Convert the pixel range from the reference axis to the pixel
    ! range on this axis.
    !
    ! This API only allows for a 1-to-N or N-to-1 relationship, with N
    ! integer (for N non-integer, need a real use case and dedicated
    ! API).
    !-------------------------------------------------------------------
    class(cubetuple_iterator_t), intent(in)    :: iter
    integer(kind=indx_k),        intent(in)    :: refrange(2)
    logical,                     intent(in)    :: truncate
    integer(kind=indx_k),        intent(out)   :: thisrange(2)
    logical,                     intent(inout) :: error
    !
    real(kind=coor_k) :: lpix,rpix
    real(kind=coor_k) :: lcoor,rcoor
    character(len=mess_l) :: mess
    real(kind=coor_k), parameter :: tol=1d-5  ! Channel/pixel misaligment absolute tolerance
    character(len=*), parameter :: rname='ITERATOR>RANGE'
    !
    if (.not.iter%this_is_ready(rname,error))  return
    if (truncate .and. iter%this%third%axis%n.eq.1) then
      ! By design any reference axis range is compatible with a 2D
      ! plane and covers range 1-1
      thisrange(1) = 1
      thisrange(2) = 1
      return
    endif
    !
    if (.not.iter%ref_is_ready(rname,error))  return
    !
    ! Convert to pixel boundary in reference axis
    lpix = refrange(1)-0.5d0
    rpix = refrange(2)+0.5d0
    !
    ! Convert to physical
    call cubetools_axis_pixel2offset(iter%ref%third%axis,lpix,lcoor,error)
    if (error)  return
    call cubetools_axis_pixel2offset(iter%ref%third%axis,rpix,rcoor,error)
    if (error)  return
    !
    ! Convert to this axis
    call cubetools_axis_offset2pixel(iter%this%third%axis,lcoor,lpix,error)
    if (error)  return
    call cubetools_axis_offset2pixel(iter%this%third%axis,rcoor,rpix,error)
    if (error)  return
    !
    ! Convert to pixel center in this axis
    lpix = lpix+0.5d0
    rpix = rpix-0.5d0
    ! write(*,'(A,1pg14.7,1pg14.7)')  'Pixel range: ',lpix,rpix
    !
    ! Convert to pixel number
    thisrange(1) = nint(lpix)
    thisrange(2) = nint(rpix)
    !
    ! Sanity check: check that lpix and rpix are aligned on pixel center
    ! (within epsilon machine).
    ! Note: nearly_equal at 0 does not work since
    !       nearly_equal(0,epsilon,tolerance) with a relative tolerance
    !       is not what we want.
    ! if (.not.nearly_equal(real(thisrange(1),kind=coor_k),lpix,1d-7) .or.  &
    !     .not.nearly_equal(real(thisrange(2),kind=coor_k),rpix,1d-7)) then
    ! Use absolute tolerance instead:
    if (abs(thisrange(1)-lpix).gt.tol .or.  &
        abs(thisrange(2)-rpix).gt.tol) then
      call cubetuple_message(seve%e,rname,'Invalid pixel range')
      write(mess,'(3(a,i0),3(a,1pg14.7))')  &
        'From reference range ',refrange(1),  &
        ':',refrange(2),  &
        ' in axis n=',  iter%ref%third%axis%n, &
               ', ref=',iter%ref%third%axis%ref,  &
               ', val=',iter%ref%third%axis%val,  &
               ', inc=',iter%ref%third%axis%inc
      call cubetuple_message(seve%e,rname,mess)
      write(mess,'(2(a,1pg14.7),a,i0,3(a,1pg14.7))')  &
        'To this range ',lpix,  &
        ':',rpix,  &
        ' in axis n=',  iter%this%third%axis%n, &
               ', ref=',iter%this%third%axis%ref,  &
               ', val=',iter%this%third%axis%val,  &
               ', inc=',iter%this%third%axis%inc
      call cubetuple_message(seve%e,rname,mess)
      error = .true.
      return
    endif
    !
    if (truncate) then
      ! Depending on context truncation might be desired or not. For
      ! example when computing planes to be read from disk, we DO want
      ! truncation. On the other hand, subcubes might want to "load"
      ! non-existing planes for simplicity in the processing flow (planes
      ! to be ignored during the processing).
      if (thisrange(1).lt.1)                       thisrange(1) = 1
      if (thisrange(2).gt.iter%this%third%axis%n)  thisrange(2) = iter%this%third%axis%n
    endif
  end subroutine iterator_range
  !
  subroutine iterator_fullrange(iter,thisrange,error)
    !-------------------------------------------------------------------
    !  Return the full cube range WHATEVER its third axis compared to
    ! the reference (no sanity check). This is used by e.g. HISTO3D.
    !-------------------------------------------------------------------
    class(cubetuple_iterator_t), intent(in)    :: iter
    integer(kind=indx_k),        intent(out)   :: thisrange(2)
    logical,                     intent(inout) :: error
    !
    character(len=*), parameter :: rname='ITERATOR>FULLRANGE'
    !
    if (.not.iter%this_is_ready(rname,error))  return
    thisrange(1) = 1
    thisrange(2) = iter%this%third%axis%n
  end subroutine iterator_fullrange
  !
  !---------------------------------------------------------------------
  !
  subroutine iterator_image_number(iter,oimage,kimage,error)
    !-------------------------------------------------------------------
    !  Given the output plane number being processed, compute the
    ! corresponding absolute image number.
    !-------------------------------------------------------------------
    class(cubetuple_iterator_t), intent(in)    :: iter
    integer(kind=indx_k),        intent(in)    :: oimage
    integer(kind=indx_k),        intent(out)   :: kimage
    logical,                     intent(inout) :: error
    !
    character(len=*), parameter :: rname='ITERATOR>IMAGE>NUMBER'
    !
    if (.not.iter%this_is_ready(rname,error))  return
    if (.not.iter%ref_is_ready(rname,error))  return
    ! From channel number in reference cube to channel number in this cube
    call iterator_axis_number(iter%ref%c%axis,iter%this%c%axis,oimage,kimage,error)
    if (error)  return
  end subroutine iterator_image_number
  !
  subroutine iterator_image_size(iter,nx,ny,error)
    !-------------------------------------------------------------------
    ! Return the image size suited to iterate the cube.
    !-------------------------------------------------------------------
    class(cubetuple_iterator_t), intent(in)    :: iter
    integer(kind=indx_k),        intent(out)   :: nx,ny
    logical,                     intent(inout) :: error
    !
    character(len=*), parameter :: rname='ITERATOR>IMAGE>SIZE'
    !
    if (.not.iter%this_is_ready(rname,error))  return
    nx = iter%this%l%region%n
    ny = iter%this%m%region%n
  end subroutine iterator_image_size
  !
  subroutine iterator_image_region(iter,lrange,mrange,error)
    !-------------------------------------------------------------------
    !  Return the image region suited to iterate the cube. In practice,
    ! this means we select the range overlapping the reference (output)
    ! cube.
    !
    ! This API only allows for a one-to-one relationship for pixels,
    ! with a possible offset (e.g. with regions).
    !-------------------------------------------------------------------
    class(cubetuple_iterator_t), intent(in)    :: iter
    integer(kind=indx_k),        intent(out)   :: lrange(2),mrange(2)
    logical,                     intent(inout) :: error
    !
    character(len=*), parameter :: rname='ITERATOR>IMAGE>REGION'
    !
    if (.not.iter%this_is_ready(rname,error))  return
    ! L axis
    lrange(1) = iter%this%l%region%first
    lrange(2) = iter%this%l%region%last
    ! M axis
    mrange(1) = iter%this%m%region%first
    mrange(2) = iter%this%m%region%last
  end subroutine iterator_image_region
  !
  !---------------------------------------------------------------------
  !
  subroutine iterator_spectrum_number(iter,opixel,xpix,ypix,error)
    !-------------------------------------------------------------------
    !  Given the output pixel number being processed, compute the
    ! corresponding absolute pixel number.
    !-------------------------------------------------------------------
    class(cubetuple_iterator_t), intent(in)    :: iter
    integer(kind=indx_k),        intent(in)    :: opixel
    integer(kind=indx_k),        intent(out)   :: xpix,ypix
    logical,                     intent(inout) :: error
    !
    integer(kind=indx_k) :: oxpix,oypix
    character(len=*), parameter :: rname='ITERATOR>SPECTRUM>NUMBER'
    !
    if (.not.iter%this_is_ready(rname,error))  return
    if (.not.iter%ref_is_ready(rname,error))  return
    !
    ! From flat coordinate to 2D coordinates in output (reference) cube:
    oypix = (opixel-1)/iter%ref%l%axis%n+1        ! [pix]
    oxpix = opixel - iter%ref%l%axis%n*(oypix-1)  ! [pix]
    !
    ! From X pixel number in reference cube to X pixel number in this cube
    call iterator_axis_number(iter%ref%l%axis,iter%this%l%axis,oxpix,xpix,error)
    if (error)  return
    ! From Y pixel number in reference cube to Y pixel number in this cube
    call iterator_axis_number(iter%ref%m%axis,iter%this%m%axis,oypix,ypix,error)
    if (error)  return
  end subroutine iterator_spectrum_number
  !
  subroutine iterator_spectrum_size(iter,nc,error)
    !-------------------------------------------------------------------
    !  Return the spectrum size suited to iterate the cube.
    !-------------------------------------------------------------------
    class(cubetuple_iterator_t), intent(in)    :: iter
    integer(kind=indx_k),        intent(out)   :: nc
    logical,                     intent(inout) :: error
    !
    character(len=*), parameter :: rname='ITERATOR>SPECTRUM>SIZE'
    !
    if (.not.iter%this_is_ready(rname,error))  return
    nc = iter%this%c%region%n
  end subroutine iterator_spectrum_size
  !
  subroutine iterator_spectrum_region(iter,crange,error)
    !-------------------------------------------------------------------
    !  Return the spectrum region suited to iterate the cube. In
    ! practice, this means we select the range overlapping the reference
    ! (output) cube.
    !-------------------------------------------------------------------
    class(cubetuple_iterator_t), intent(in)    :: iter
    integer(kind=indx_k),        intent(out)   :: crange(2)
    logical,                     intent(inout) :: error
    !
    character(len=*), parameter :: rname='ITERATOR>SPECTRUM>REGION'
    !
    if (.not.iter%this_is_ready(rname,error))  return
    ! C axis
    crange(1) = iter%this%c%region%first
    crange(2) = iter%this%c%region%last
  end subroutine iterator_spectrum_region
  !
  !---------------------------------------------------------------------
  !
  subroutine iterator_subcube_size(iter,n1,n2,error)
    !-------------------------------------------------------------------
    !  Return the subcube size (2 first dimensions) suited to iterate
    ! the cube.
    !-------------------------------------------------------------------
    class(cubetuple_iterator_t), intent(in)    :: iter
    integer(kind=indx_k),        intent(out)   :: n1,n2
    logical,                     intent(inout) :: error
    !
    character(len=*), parameter :: rname='ITERATOR>SUBCUBE>SIZE'
    !
    if (.not.iter%this_is_ready(rname,error))  return
    n1 = iter%this%first%region%n
    n2 = iter%this%second%region%n
  end subroutine iterator_subcube_size
  !
  subroutine iterator_subcube_region(iter,range1,range2,error)
    !-------------------------------------------------------------------
    !  Return the subcube region (2 first dimensions) suited to iterate
    ! the cube. In practice, this means we select the range overlapping
    ! the reference (output) cube.
    !-------------------------------------------------------------------
    class(cubetuple_iterator_t), intent(in)    :: iter
    integer(kind=indx_k),        intent(out)   :: range1(2),range2(2)
    logical,                     intent(inout) :: error
    !
    character(len=*), parameter :: rname='ITERATOR>SUBCUBE>REGION'
    !
    if (.not.iter%this_is_ready(rname,error))  return
    ! First axis
    range1(1) = iter%this%first%region%first
    range1(2) = iter%this%first%region%last
    ! Second axis
    range2(1) = iter%this%second%region%first
    range2(2) = iter%this%second%region%last
  end subroutine iterator_subcube_region
  !
  !---------------------------------------------------------------------
  !
  subroutine iterator_axis_number(refaxis,thisaxis,refpix,thispix,error)
    use gkernel_interfaces
    !-------------------------------------------------------------------
    !  Convert the pixel number from the reference axis to the pixel
    ! number on this axis.
    !
    ! This API only allows for a one-to-one relationship, with a
    ! possible offset (e.g. with regions). For more complicated use
    ! cases (N-to-1 or 1-to-N, with N integer or floating point),
    ! dedicated APIs (with different iteration scheme) are to be
    ! implemented.
    !
    ! Note that we could add sanity check to avoid misuses.
    !-------------------------------------------------------------------
    type(axis_t),         intent(in)    :: refaxis
    type(axis_t),         intent(in)    :: thisaxis
    integer(kind=indx_k), intent(in)    :: refpix
    integer(kind=indx_k), intent(out)   :: thispix
    logical,              intent(inout) :: error
    !
    real(kind=coor_k) :: coor
    character(len=*), parameter :: rname='ITERATOR>AXIS>NUMBER'
    !
    if (thisaxis%n.eq.1) then
      ! By design any reference pixel is compatible with a 2D
      ! plane and covers pixel 1
      thispix = 1
      return
    endif
    !
    ! Sanity check
    if (.not.nearly_equal(refaxis%inc,thisaxis%inc,1d-6)) then  ! ZZZ Should have a 'compatibility' function
      ! The axes are not compatible. The formulas below are generic
      ! and return the (or a) correct pixel number, but this makes
      ! no sense in the general context of iterating the input and
      ! output cubes plane by plane. A better API depending on
      ! use-case is preferable.
      call cubetuple_message(seve%e,rname,  &
        'Internal error: the axes are not compatible (not the same increments)')
      error = .true.
      return
    endif
    !
    ! From pixel coordinate in reference axis to physical
    call cubetools_axis_pixel2offset(refaxis,refpix,coor,error)
    if (error)  return
    ! From physical coordinate to pixel in this cube
    call cubetools_axis_offset2pixel(thisaxis,coor,thispix,error)
    if (error)  return
  end subroutine iterator_axis_number
  !
end module cubetuple_iterator
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
