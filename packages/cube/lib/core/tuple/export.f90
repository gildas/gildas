module cubetuple_export
  use cubetools_parameters
  use cubetools_access_types
  use cubedag_tuple
  use cube_types
  use cubetuple_messaging
  use cubetuple_transpose

  public :: cubetuple_export_cube
  private

contains

  subroutine cubetuple_export_cube(cube,newname,tofits,error)
    use gkernel_interfaces
    use cubetools_dataformat_types
    use cubeio_highlevel
    !-------------------------------------------------------------------
    ! Export the current cube to disk
    !-------------------------------------------------------------------
    type(cube_t),     intent(inout) :: cube
    character(len=*), intent(in)    :: newname
    logical,          intent(in)    :: tofits
    logical,          intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='EXPORT>CUBE'
    character(len=file_l) :: oldname
    integer(kind=4) :: ier
    !
    ! If a transposition is pending, time has come to do it NOW before
    ! we dump the data!
    call cubetuple_autotranspose_cube(cube,error)
    if (error)  return
    !
    ! Which cube is to be exported?
    ! - As of today, the "current" one, i.e. the last which was used.
    ! - We could be a bit more clever, and check among all the cubes
    !   in the tuple, if there is one already written (i.e. save time!)
    !   This means asking for the tuple if there is a file already
    !   available (code_buffer_disk). BUT, is this file up-to-date?
    !   For now, we assume that the memory content is always newer or
    !   equal to the disk contents.
    !
    oldname = cube%tuple%current%file%hgdf%file
    !
    ! Rename the file in memory
    cube%tuple%current%file%hgdf%file = newname
    !
    ! Rename the file on disk (for e.g. files in disk mode)
    if (gag_inquire(oldname,len_trim(oldname)).eq.0) then
      ier = gag_filrename(oldname,newname)
      if (ier.ne.0)  error = .true.
    endif
    !
    ! Dump (for e.g. files in memory mode)
    call cube%prog%set_action(code_write,error)
    if (error)  return
    call cube%prog%set_access(cube%access(),error)  ! i.e. the current access, as explained above
    if (error)  return
    call cube%prog%set_order(cube%order(),error)  ! i.e. the current order, as explained above
    if (error)  return
    call cube%prog%set_filename(newname,error)
    if (error)  return
    if (tofits) then
      call cube%prog%set_filekind(code_dataformat_fits,error)
    else
      call cube%prog%set_filekind(code_dataformat_gdf,error)
    endif
    if (error)  return
    call cubeio_dump_cube(cube%user,cube%prog,cube,cube%tuple%current,error)
    if (error)  return
    !
    ! Update the tuple if relevant
    call cubedag_tuple_upsert(cube%node%tuple,  &
                              cube%order(),  &
                              code_buffer_disk,  &
                              cube%tuple%current%file%hgdf%file,1, &
                              error)
    if (error)  return
    !
    call cubetuple_message(seve%i,rname,'Exported cube to '//newname)
  end subroutine cubetuple_export_cube

end module cubetuple_export
