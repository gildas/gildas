!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cube_types  ! ZZZ to be renamed...
  use cubetools_parameters
  use cubedag_node_type
  use cubetuple_messaging
  use cubetuple_format
  use cubetuple_tuple
  !
  public :: cube_t
  public :: cube_allocate_new,cubetuple_cube_ptr,cubetuple_cube_ptr_from_format
  private
  !
  type, extends(format_t) :: cube_t
    ! No more
  contains
    procedure, public :: access        => cube_get_access
    procedure, public :: iscplx        => cube_get_iscplx
    procedure, public :: haskind       => cube_has_filekind
    procedure, public :: nbytes        => cube_get_nbytes
    procedure, public :: ndata         => cube_get_ndata
    procedure, public :: nentry        => cube_get_nentry
    procedure, public :: default_order => cube_default_order
    procedure, public :: free          => cube_free
  end type cube_t
  !
  ! Fortran compiler says these are ambiguous, but on the other hand
  ! I can not call cubetuple_cube_ptr_from_node with a format_t pointer...
  ! interface cubetuple_cube_ptr
  !   module procedure cubetuple_cube_ptr_from_node
  !   module procedure cubetuple_cube_ptr_from_format
  ! end interface cubetuple_cube_ptr
  !
contains
  !
  function cube_allocate_new(setup,error)
    use gkernel_interfaces
    use cubetools_setup_types
    !-------------------------------------------------------------------
    ! Allocate and initialize a new cube_t in memory and return a
    ! pointer to this allocation
    !-------------------------------------------------------------------
    type(cube_t), pointer :: cube_allocate_new
    type(cube_setup_t), intent(in), target :: setup
    logical,            intent(inout)      :: error
    !
    integer(kind=4) :: ier
    character(len=*), parameter :: rname='ALLOCATE>NEW'
    !
    allocate(cube_allocate_new,stat=ier)
    if (failed_allocate(rname,'object',ier,error)) return
    call cube_allocate_new%init(setup,error)
    if (error)  return
    !
    ! Override the node_t methods with cube_t specific ones
    cube_allocate_new%ltype    => cube_ltype
    cube_allocate_new%memsize  => cube_memsize
    cube_allocate_new%disksize => cube_disksize
    cube_allocate_new%datasize => cube_datasize
  end function cube_allocate_new
  !
  function cube_get_access(cub)
    !-------------------------------------------------------------------
    ! Return the CURRENT access mode.
    !-------------------------------------------------------------------
    integer(kind=code_k) :: cube_get_access
    class(cube_t), intent(in) :: cub
    cube_get_access = cub%tuple%access()
  end function cube_get_access
  !
  function cube_get_iscplx(cub)
    !-------------------------------------------------------------------
    ! Return .true. if the cube data is complex*4
    !-------------------------------------------------------------------
    logical :: cube_get_iscplx
    class(cube_t), intent(in) :: cub
    cube_get_iscplx = cub%tuple%iscplx()
  end function cube_get_iscplx
  !
  function cube_has_filekind(cub,code_filekind)
    !-------------------------------------------------------------------
    ! Return .true. if the cube provides the given kind description
    !-------------------------------------------------------------------
    logical :: cube_has_filekind
    class(cube_t),        intent(in) :: cub
    integer(kind=code_k), intent(in) :: code_filekind
    cube_has_filekind = cub%tuple%haskind(code_filekind)
  end function cube_has_filekind
  !
  function cube_get_nbytes(cub)
    !-------------------------------------------------------------------
    ! Return the number of bytes per data value
    !-------------------------------------------------------------------
    integer(kind=4) :: cube_get_nbytes
    class(cube_t), intent(in) :: cub
    cube_get_nbytes = cub%tuple%nbytes()
  end function cube_get_nbytes
  !
  function cube_get_ndata(cub)
    !-------------------------------------------------------------------
    ! Return the number of data values in the cube
    !-------------------------------------------------------------------
    integer(kind=data_k) :: cube_get_ndata
    class(cube_t), intent(in) :: cub
    cube_get_ndata = cub%tuple%ndata()
  end function cube_get_ndata
  !
  function cube_get_nentry(cub)
    !-------------------------------------------------------------------
    ! Return the number of entries (Nchan/NPix) for the CURRENT access
    ! mode.
    !-------------------------------------------------------------------
    integer(kind=entr_k) :: cube_get_nentry
    class(cube_t), intent(in) :: cub
    cube_get_nentry = cub%tuple%nentry()
  end function cube_get_nentry
  !
  subroutine cube_free(cub,error)
    use cubedag_tuple
    !---------------------------------------------------------------------
    ! Free the memory-consuming components of a 'cube_t' instance. The
    ! cube_t remains useable after this free. It is the responsibility of
    ! the caller to ensure the data remains available elsewhere (most
    ! likely on disk).
    ! Use cube_final to free consistently all the object.
    !---------------------------------------------------------------------
    class(cube_t), intent(inout) :: cub
    logical,       intent(inout) :: error
    !
    call cub%tuple%free(error)
    if (error)  return
    call cubedag_tuple_rmmemo(cub%node%tuple,error)
    if (error)  return
  end subroutine cube_free
  !
  function cubetuple_cube_ptr(dno,error)
    !-------------------------------------------------------------------
    ! Check if the input class is strictly a 'cube_t', and return a
    ! pointer to it if relevant.
    !-------------------------------------------------------------------
    type(cube_t), pointer :: cubetuple_cube_ptr  ! Function value on return
    class(cubedag_node_object_t), pointer       :: dno
    logical,                      intent(inout) :: error
    !
    character(len=*), parameter :: rname='CUBE>PTR'
    !
    select type(dno)
    type is (cube_t)
      cubetuple_cube_ptr => dno
    class default
      cubetuple_cube_ptr => null()
      call cubetuple_message(seve%e,rname,'Internal error: object is not a cube_t')
      error = .true.
      return
    end select
  end function cubetuple_cube_ptr
  !
  function cubetuple_cube_ptr_from_format(format,error)
    !-------------------------------------------------------------------
    ! Check if the input class is strictly a 'cube_t', and return a
    ! pointer to it if relevant.
    !-------------------------------------------------------------------
    type(cube_t), pointer :: cubetuple_cube_ptr_from_format  ! Function value on return
    class(format_t), pointer       :: format
    logical,         intent(inout) :: error
    !
    character(len=*), parameter :: rname='CUBE>PTR>FROM>FORMAT'
    !
    select type(format)
    type is (cube_t)
      cubetuple_cube_ptr_from_format => format
    class default
      cubetuple_cube_ptr_from_format => null()
      call cubetuple_message(seve%e,rname,'Internal error: object is not a cube_t')
      error = .true.
      return
    end select
  end function cubetuple_cube_ptr_from_format
  !
  function cube_ltype(obj)
    use cubetools_axset_types
    character(len=2) :: cube_ltype
    class(cubedag_node_object_t), intent(in) :: obj
    select type (obj)
    type is (cube_t)
      write(cube_ltype,'(I1,A1)')  cubetools_axset_count_genuine(obj%head%set),'D'
    class default
      cube_ltype = '??'
    end select
  end function cube_ltype

  function cube_memsize(obj)
    integer(kind=size_length) :: cube_memsize
    class(cubedag_node_object_t), intent(in) :: obj
    select type (obj)
    type is (cube_t)
      cube_memsize = obj%tuple%memsize()
    class default
      cube_memsize = 0
    end select
  end function cube_memsize

  function cube_disksize(obj)
    use cubedag_tuple
    integer(kind=size_length) :: cube_disksize
    class(cubedag_node_object_t), intent(in) :: obj
    select type (obj)
    type is (cube_t)
      cube_disksize = obj%node%tuple%disksizes()
    class default
      cube_disksize = 0
    end select
  end function cube_disksize

  function cube_datasize(obj)
    integer(kind=size_length) :: cube_datasize
    class(cubedag_node_object_t), intent(in) :: obj
    logical :: error
    select type (obj)
    type is (cube_t)
      error = .false.
      call obj%head%arr%datasize(cube_datasize,error)
    class default
      cube_datasize = 0
    end select
  end function cube_datasize

  subroutine cube_default_order(cube,error)
    use cubetools_header_types
    use cubeio_header_iodesc
    !-------------------------------------------------------------------
    ! If the cube has an intrinsic order which is not supported (e.g.
    ! spatial or spectral dimension all or partly missing), modify it
    ! for a supported order.
    !-------------------------------------------------------------------
    class(cube_t), intent(inout) :: cube
    logical,       intent(inout) :: error
    !
    integer(kind=code_k) :: done
    integer(kind=4) :: idime,ndime,iorder,norder
    integer(kind=4), parameter :: mdime=3
    integer(kind=4) :: cur_order(mdime)
    integer(kind=4) :: tmp_order(mdime)
    integer(kind=4) :: def_order(mdime,cubetuple_norder)
    integer(kind=code_k) :: def_code(cubetuple_norder)
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='DEFAULT>ORDER'
    !
    ! NB: changes are done in interface_t and then propagated to
    !     header_t and iodesc_t. Do not rely on header_t which is
    !     already a modified/patched product. It will be regenerated
    !     hereafter.
    !
    ! Note: axset_i* can go beyond 3!
    cur_order(:) = (/ cube%node%head%axset_ix,  &
                      cube%node%head%axset_iy,  &
                      cube%node%head%axset_ic /)
    !
    ! Image set order (first = default if all dimensions missing)
    def_code(1) = code_cube_imaset
    def_order(:,1) = (/ 1,2,3 /)
    ! Spectrum set order
    def_code(2) = code_cube_speset
    def_order(:,2) = (/ 2,3,1 /)
    !
    if (cube%node%head%axset_ndim.eq.2) then
      ndime = 2
      norder = 1  ! Compare with the first order (imaset) only
    elseif (cube%node%head%axset_ndim.eq.3) then
      ndime = 3
      norder = 2
    else
      ! Other dimensions: what do we want?
      ndime = 0
      norder = 0
    endif
    !
    done = code_null
    do iorder=1,norder
      tmp_order(:) = cur_order(:)
      do idime=1,mdime
        if (cur_order(idime).eq.0)  tmp_order(idime) = def_order(idime,iorder)
      enddo
      if (all(tmp_order.eq.def_order(:,iorder))) then
        ! Found a matching order
        if (cur_order(1).eq.0 .and. tmp_order(1).le.ndime) then  ! IX was missing
          cube%node%head%axset_ix = tmp_order(1)
          done = def_code(iorder)
        endif
        if (cur_order(2).eq.0 .and. tmp_order(2).le.ndime) then  ! IY was missing
          cube%node%head%axset_iy = tmp_order(2)
          done = def_code(iorder)
        endif
        if (cur_order(3).eq.0 .and. tmp_order(3).le.ndime) then  ! IC was missing
          cube%node%head%axset_ic = tmp_order(3)
          done = def_code(iorder)
        endif
        if (done.ne.code_null)  exit  ! There can be only 1 match as the orders are unambiguous
      endif
    enddo
    !
    write(mess,'(6(a,i0))')  'From IX=',cur_order(1),  &
                                ', IY=',cur_order(2),  &
                                ', IC=',cur_order(3),  &
                              ' to IX=',cube%node%head%axset_ix,  &
                                ', IY=',cube%node%head%axset_iy,  &
                                ', IC=',cube%node%head%axset_ic
    call cubetuple_message(seve%d,rname,mess)
    !
    if (done.eq.code_null) then
      call cubetuple_message(seve%d,rname,'No patch of the dimensions')
    else
      call cubetuple_message(seve%w,rname,  &
        'Assuming '//trim(access_status(done))//' for missing dimensions')
      ! interface_t has been updated
      !   => update header_t
      call cubetools_header_import_and_derive(cube%node%head,cube%head,error)
      if (error) return
      !   => update iodesc_t
      call cubeio_iodesc_import(cube%node%head,cube%tuple%current%desc,error)
      if (error) return
    endif
  end subroutine cube_default_order
end module cube_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
