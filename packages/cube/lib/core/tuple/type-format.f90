!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetuple_format
  use cubetools_parameters
  use cubetools_header_types
  use cubetools_processing_types
  use cubetools_setup_types
  use cubedag_node_type
  use cubeio_cube_define
  use cubetuple_messaging
  use cubetuple_iterator
  use cubetuple_tuple
  !---------------------------------------------------------------------
  ! Support module for any data kind which can be described with a
  ! cube_header_t, 0 or more DAPS, and some data with consistent shape.
  !---------------------------------------------------------------------
  !
  type, extends(cubedag_node_object_t) :: format_t
    type(cube_header_t)          :: head   ! [public]  User friendly cube description
  ! type(daps_t)                 :: daps   !
    type(cubetuple_t)            :: tuple  ! [private]
    type(cube_define_t)          :: prog   ! [private] program request
    type(cube_setup_t), pointer  :: user   ! [private] user    request
    type(cubetools_processing_t) :: proc   ! [private] technical components for processing
    type(cubetuple_iterator_t), public :: iter  ! Iterator utilities when processing a command
  contains
    procedure         :: init           => cubetuple_format_init
    procedure         :: list           => cubetuple_format_list
    procedure         :: close          => cubetuple_format_close
    procedure         :: finish         => cubetuple_format_finish
    procedure         :: order          => cubetuple_format_get_order
    procedure         :: attach_file    => cubetuple_format_attach_file
    procedure         :: dag_upsert     => cubetuple_format_dag_upsert
    procedure, public :: default_region => cubetuple_format_default_region
    final             :: cubetuple_format_final
  end type format_t
  !
  public :: format_t
  public :: cubetuple_format_ptr
  private
  !
contains
  !
  subroutine cubetuple_format_init(format,setup,error)
    !-------------------------------------------------------------------
    ! Initialize a format_t
    !-------------------------------------------------------------------
    class(format_t),     intent(inout)       :: format
    type(cube_setup_t),  intent(in), target  :: setup
    logical,             intent(inout)       :: error
    !
    format%user => setup
    call format%tuple%init(error)
    if (error)  return
    call format%head%init(error)
    if (error)  return
    call format%node%head_associate(format%head,error)
    if (error)  return
  end subroutine cubetuple_format_init
  !
  subroutine cubetuple_format_list(format,code_dataformat,error)
    use gfits_types
    use cubetools_dataformat_types
    use cubefitsio_header
    use cubeio_interface
    use cubetools_header_vo
    !-------------------------------------------------------------------
    ! List the cube header under requested format
    !-------------------------------------------------------------------
    class(format_t),      intent(inout) :: format
    integer(kind=code_k), intent(in)    :: code_dataformat
    logical,              intent(inout) :: error
    !
    type(fitsio_header_t) :: hfits
    !
    select case (code_dataformat)
    case (code_dataformat_cube)
      call format%head%list(error)
      if (error) return
    case (code_dataformat_interface)
      call format%node%head%list(error)  ! format%node%head is interface_t
      if (error) return
    case (code_dataformat_fits)
      if (format%tuple%haskind(code_dataformat_fits)) then
        ! Tuple will just list its current FITS header
        call format%tuple%list(code_dataformat,error)
        if (error)  return
      else
        ! Convert format%head to FITS format
        call cubetuple_message(seve%w,'LIST','Current header is not FITS')
        ! NB: Display in its current order (no internal transposition at conversion
        ! time)
        call cubeio_header_put(format,format%tuple%order(),hfits,error)
        if (error)  return
        call gfits_list_dict(hfits%dict,error)
        if (error)  return
      endif
    case (code_dataformat_vo)
      call cubetools_header_vo_list(format%head,error)
      if (error) return
    case default
      call format%tuple%list(code_dataformat,error)
      if (error)  return
    end select
  end subroutine cubetuple_format_list
  !
  subroutine cubetuple_format_close(format,error)
    !---------------------------------------------------------------------
    ! GIO-close a 'format_t' instance. This is worth calling this
    ! subroutine as GIO slots are a limited ressource
    !---------------------------------------------------------------------
    class(format_t), intent(inout) :: format
    logical,         intent(inout) :: error
    !
    call format%tuple%close(error)
    if (error)  return
  end subroutine cubetuple_format_close
  !
  subroutine cubetuple_format_finish(format,error)
    use cubeio_desc
    use cubeio_highlevel
    !---------------------------------------------------------------------
    ! Properly finish a cube, e.g. flush the remaining buffers from memory
    ! to disk if relevant.
    !---------------------------------------------------------------------
    class(format_t), intent(inout) :: format
    logical,         intent(inout) :: error
    !
    character(len=*), parameter :: rname='FORMAT>FINISH'
    !
    ! File extrema
    call format%proc%put_extrema(format%head,error)
    if (error)  return
    !
    if (format%tuple%trans%do.ne.code_trans_none) then
      call cubetuple_message(seve%d,rname,  &
        'Finishing the tuple but a transposition is pending')
      ! Two possibilities:
      ! - If the next command needs the direct access, there is no point
      !   performing this transposition.
      ! - If the next command needs the transposed access, we will re-
      !   prepare it from scratch. As of today leaving remnants breaks
      !   the transposition preparation.
      ! => forget the transposition preparation
      call cubeio_desc_reset(format%tuple%cube(format%tuple%trans%to)%desc,error)
      if (error)  continue
      call format%tuple%trans%reset(error)
      if (error)  continue
    endif
    !
    if (associated(format%tuple%current)) then
      ! It may not be associated e.g. in error recovery mode
      ! ZZZ Should we finish all cubes or only the current one?
      call cubeio_cube_finish(format%user,format,format%tuple%current,error)
      if (error)  return
    endif
    !
    ! The iterator is reset, so that it can not be used until next command
    call format%iter%finish(error)
    if (error)  return
    !
    ! The define type is reset, so that all requests are redefined by
    ! the next command
    call format%prog%reset(error)
    if (error)  continue
  end subroutine cubetuple_format_finish
  !
  function cubetuple_format_get_order(format)
    !-------------------------------------------------------------------
    ! Return the CURRENT cube order
    !-------------------------------------------------------------------
    integer(kind=code_k) :: cubetuple_format_get_order
    class(format_t), intent(in) :: format
    cubetuple_format_get_order = format%tuple%order()
  end function cubetuple_format_get_order
  !
  subroutine cubetuple_format_attach_file(format,file,error)
    use cubeio_file
    use cubeio_desc_setup
    !-------------------------------------------------------------------
    ! Attach the pre-loaded cubeio_file_t and compute the associated
    ! header_t
    !-------------------------------------------------------------------
    class(format_t),     intent(inout)       :: format
    type(cubeio_file_t), intent(in), pointer :: file
    logical,             intent(inout)       :: error
    !
    call format%tuple%attach_file(file,error)
    if (error)  return
    ! Propagate the header of this data type to interface_t, header_t, iodesc_t
    call cubeio_set_descriptor_intrinsic(format,format%tuple%current,error)
    if (error)  return
  end subroutine cubetuple_format_attach_file
  !
  subroutine cubetuple_format_dag_upsert(format,error)
    use cubedag_tuple
    !-------------------------------------------------------------------
    ! Upsert the format in DAG
    !-------------------------------------------------------------------
    class(format_t), intent(inout) :: format
    logical,         intent(inout) :: error
    ! Local
    integer(kind=code_k) :: code_where
    integer(kind=4) :: hdu
    character(len=file_l) :: where
    !
    if (.not.associated(format%tuple%current)) then
      ! This can happen in error recovery mode
      return
    endif
    !
    if (format%tuple%trans%do.ne.code_trans_none) then
      ! This can happen if a transposition was prepared at get_header
      ! time, but was not actually performed because no data was
      ! actually accessed (get_image/get_spectrum).
      ! => no upsertion in the DAG as the data is not ready
      return
    endif
    !
    if (format%tuple%current%desc%buffered.eq.code_buffer_memory) then
      code_where = code_buffer_memory
      where = '<memory>'
      hdu = 0
    else
      code_where = code_buffer_disk
      where = format%tuple%current%file%name
      hdu = format%tuple%current%file%hdu
    endif
    call cubedag_tuple_upsert(format%node%tuple,  &
                              format%order(),  &
                              code_where,  &
                              where,hdu,error)
    if (error)  return
    !
  end subroutine cubetuple_format_dag_upsert
  !
  subroutine cubetuple_format_default_region(format,error,align)
    !-------------------------------------------------------------------
    ! 1) Declare the original axes of this cube to its iterator, i.e.
    !    the leading ones and the iterated one.
    ! 2) Declare that the region to be applied on the cube is the default
    !    one. In practice this means that the data access will use 100%
    !    of the cube (not more, not less), i.e. 100% of the axes.
    ! See also cuberegion_prog_t%apply_to_cube() if a region is to be
    ! applied.
    !-------------------------------------------------------------------
    class(format_t),      intent(inout)        :: format
    logical,              intent(inout)        :: error
    integer(kind=code_k), intent(in), optional :: align
    !
    integer(kind=code_k) :: lalign,order
    integer(kind=indx_k), parameter :: auto=code_indx_auto
    character(len=*), parameter :: rname='FORMAT>DEFAULT>REGION'
    !
    if (present(align)) then
      lalign = align
    else
      lalign = code_align_auto
    endif
    !
    ! Rely on internal data order (cub is already the cube with desired
    ! order), not the way we access it (e.g. by subcube):
    order = format%order()
    !
    ! Fetch this cube axes
    call format%iter%get_axes(format%head,order,lalign,error)
    if (error)  return
    !
    call format%iter%set_region_l(auto,auto,auto,error)
    if (error)  return
    call format%iter%set_region_m(auto,auto,auto,error)
    if (error)  return
    call format%iter%set_region_c(auto,auto,auto,error)
    if (error)  return
  end subroutine cubetuple_format_default_region
  !
  subroutine cubetuple_format_final(format)
    use cubetools_header_types
    !---------------------------------------------------------------------
    ! Finalize a 'format_t' instance, i.e. free all its components before
    ! deleting the object itself
    !---------------------------------------------------------------------
    type(format_t), intent(inout) :: format
    !
    logical :: error
    !
    error = .false.
    call cubetools_header_final(format%head,error)
    if (error)  continue
    ! call cubetuple_final(format%tuple) => implicit at finalization time
  end subroutine cubetuple_format_final
  !
  function cubetuple_format_ptr(dno,error)
    !-------------------------------------------------------------------
    ! Check if the input class is a 'format_t', and return a
    ! pointer to it if relevant.
    !-------------------------------------------------------------------
    class(format_t), pointer :: cubetuple_format_ptr  ! Function value on return
    class(cubedag_node_object_t), pointer       :: dno
    logical,                      intent(inout) :: error
    !
    character(len=*), parameter :: rname='FORMAT>PTR'
    !
    select type(dno)
    class is (format_t)
      cubetuple_format_ptr => dno
    class default
      cubetuple_format_ptr => null()
      call cubetuple_message(seve%e,rname,'Internal error: object is not a format_t')
      error = .true.
      return
    end select
  end function cubetuple_format_ptr
  !
end module cubetuple_format
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
