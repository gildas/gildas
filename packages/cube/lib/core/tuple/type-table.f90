!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetuple_table
  use cubetuple_format
  !---------------------------------------------------------------------
  ! Support module for any kind of tables (UV, CC)
  !---------------------------------------------------------------------
  !
  type, extends(format_t) :: table_t
    ! ...
  contains
    ! ...
  end type table_t
  !
  public :: table_t
  private
  !
end module cubetuple_table
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
