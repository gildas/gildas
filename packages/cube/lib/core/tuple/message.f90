!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage CUBE TUPLE messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetuple_messaging
  use gpack_def
  use gbl_message
  use cubetools_parameters
  !
  public :: tupleseve
  public :: cubetuple_message_set_id,cubetuple_message
  public :: cubetuple_message_set_trans
  private
  !
  ! Identifier used for message identification
  integer(kind=4) :: cubetuple_message_id = gpack_global_id  ! Default value for startup message
  !
  type :: cubetuple_messaging_debug_t
    integer(kind=code_k) :: trans = seve%d
  end type cubetuple_messaging_debug_t
  type(cubetuple_messaging_debug_t) :: tupleseve
  !
contains
  !
  subroutine cubetuple_message_set_id(id)
    !---------------------------------------------------------------------
    ! Alter library id into input id. Should be called by the library
    ! which wants to share its id with the current one.
    !---------------------------------------------------------------------
    integer(kind=4), intent(in) :: id
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='MESSAGE>SET>ID'
    !
    cubetuple_message_id = id
    write (mess,'(A,I3)') 'Now use id #',cubetuple_message_id
    call cubetuple_message(seve%d,rname,mess)
  end subroutine cubetuple_message_set_id
  !
  subroutine cubetuple_message(mkind,procname,message)
    use cubetools_cmessaging
    !---------------------------------------------------------------------
    ! Messaging facility for the current library. Calls the low-level
    ! (internal) messaging routine with its own identifier.
    !---------------------------------------------------------------------
    integer(kind=4),  intent(in) :: mkind     ! Message kind
    character(len=*), intent(in) :: procname  ! Name of calling procedure
    character(len=*), intent(in) :: message   ! Message string
    !
    call cubetools_cmessage(cubetuple_message_id,mkind,'TUPLE>'//procname,message)
  end subroutine cubetuple_message
  !
  subroutine cubetuple_message_set_trans(on)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    logical, intent(in) :: on
    if (on) then
       tupleseve%trans = seve%i
    else
       tupleseve%trans = seve%d
    endif
  end subroutine cubetuple_message_set_trans
  !
end module cubetuple_messaging
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
