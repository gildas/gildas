!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_userstruct
  use gkernel_interfaces
  use gbl_format
  use cubesyntax_messaging
  use cubetools_parameters
  use cubetools_uservar
  use cubetools_userspace
  !
  public :: userstruct_t
  public :: cubetools_userstruct_ptr
  private
  !
  type, extends(userspace_t) :: userstruct_t
   contains
     procedure :: create        => cubetools_sicdef_struct_create
     procedure :: def           => cubetools_sicdef_struct
     procedure :: def_substruct => cubetools_sicdef_substruct
     procedure :: get_substruct => cubetools_sicget_substruct
     procedure :: create_member => cubetools_sicdef_create_member
     ! procedure :: has_changed  => method to check if structure has been changed
     !
     generic   :: set_member    => def_r4_0d,def_r4_1d,def_r4_2d,def_r4_3d,&
                                   def_r8_0d,def_r8_1d,def_r8_2d,def_r8_3d,&
                                   def_c4_0d,def_c4_1d,def_c4_2d,def_c4_3d,&
                                   def_i4_0d,def_i4_1d,def_i4_2d,def_i4_3d,&
                                   def_i8_0d,def_i8_1d,def_i8_2d,def_i8_3d,&
                                   def_st_0d,def_st_1d,&
                                   def_lg_0d
     !
     generic   :: get_member    => get_r4_0d,get_r4_1d,get_r4_2d,get_r4_3d,&
                                   get_r8_0d,get_r8_1d,get_r8_2d,get_r8_3d,&
                                   get_c4_0d,get_c4_1d,get_c4_2d,get_c4_3d,&
                                   get_i4_0d,get_i4_1d,get_i4_2d,get_i4_3d,&
                                   get_i8_0d,get_i8_1d,get_i8_2d,get_i8_3d,&
                                   get_st_0d,&
                                   get_lg_0d
     !
     procedure :: def_r4_0d => cubetools_sicdef_member_r4_0d
     procedure :: def_r4_1d => cubetools_sicdef_member_r4_1d
     procedure :: def_r4_2d => cubetools_sicdef_member_r4_2d
     procedure :: def_r4_3d => cubetools_sicdef_member_r4_3d
     ! 
     procedure :: def_r8_0d => cubetools_sicdef_member_r8_0d
     procedure :: def_r8_1d => cubetools_sicdef_member_r8_1d
     procedure :: def_r8_2d => cubetools_sicdef_member_r8_2d
     procedure :: def_r8_3d => cubetools_sicdef_member_r8_3d
     !
     procedure :: def_c4_0d => cubetools_sicdef_member_c4_0d
     procedure :: def_c4_1d => cubetools_sicdef_member_c4_1d
     procedure :: def_c4_2d => cubetools_sicdef_member_c4_2d
     procedure :: def_c4_3d => cubetools_sicdef_member_c4_3d
     !
     procedure :: def_i4_0d => cubetools_sicdef_member_i4_0d
     procedure :: def_i4_1d => cubetools_sicdef_member_i4_1d
     procedure :: def_i4_2d => cubetools_sicdef_member_i4_2d
     procedure :: def_i4_3d => cubetools_sicdef_member_i4_3d
     !
     procedure :: def_i8_0d => cubetools_sicdef_member_i8_0d
     procedure :: def_i8_1d => cubetools_sicdef_member_i8_1d
     procedure :: def_i8_2d => cubetools_sicdef_member_i8_2d
     procedure :: def_i8_3d => cubetools_sicdef_member_i8_3d
     !
     procedure :: def_st_0d => cubetools_sicdef_member_st_0d
     procedure :: def_st_1d => cubetools_sicdef_member_st_1d
     !
     procedure :: def_lg_0d => cubetools_sicdef_member_lg_0d
     !
     !----------------------------------------------------------------------
     !
     procedure :: get_r4_0d => cubetools_sicget_member_r4_0d
     procedure :: get_r4_1d => cubetools_sicget_member_r4_1d
     procedure :: get_r4_2d => cubetools_sicget_member_r4_2d
     procedure :: get_r4_3d => cubetools_sicget_member_r4_3d
     ! 
     procedure :: get_r8_0d => cubetools_sicget_member_r8_0d
     procedure :: get_r8_1d => cubetools_sicget_member_r8_1d
     procedure :: get_r8_2d => cubetools_sicget_member_r8_2d
     procedure :: get_r8_3d => cubetools_sicget_member_r8_3d
     !
     procedure :: get_c4_0d => cubetools_sicget_member_c4_0d
     procedure :: get_c4_1d => cubetools_sicget_member_c4_1d
     procedure :: get_c4_2d => cubetools_sicget_member_c4_2d
     procedure :: get_c4_3d => cubetools_sicget_member_c4_3d
     !
     procedure :: get_i4_0d => cubetools_sicget_member_i4_0d
     procedure :: get_i4_1d => cubetools_sicget_member_i4_1d
     procedure :: get_i4_2d => cubetools_sicget_member_i4_2d
     procedure :: get_i4_3d => cubetools_sicget_member_i4_3d
     !
     procedure :: get_i8_0d => cubetools_sicget_member_i8_0d
     procedure :: get_i8_1d => cubetools_sicget_member_i8_1d
     procedure :: get_i8_2d => cubetools_sicget_member_i8_2d
     procedure :: get_i8_3d => cubetools_sicget_member_i8_3d
     !
     procedure :: get_st_0d => cubetools_sicget_member_st_0d
     !
     procedure :: get_lg_0d => cubetools_sicget_member_lg_0d
  end type userstruct_t
  !
  ! Useful
  character(len=*), parameter :: link = '%'
  !
contains
  !
  function cubetools_userstruct_ptr(userspace,error)
    !-------------------------------------------------------------------
    ! Check if the input class is of type(userstruct_t), and return
    ! a pointer to it if relevant.
    !-------------------------------------------------------------------
    type(userstruct_t), pointer :: cubetools_userstruct_ptr  ! Function value on return
    class(userspace_t), target        :: userspace
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: rname='USERSTRUCT>PTR'
    !
    select type(userspace)
    type is (userstruct_t)
      cubetools_userstruct_ptr => userspace
    class default
      cubetools_userstruct_ptr => null()
      call cubesyntax_message(seve%e,rname,  &
        'Internal error: user space is not a userstruct_t type')
      error = .true.
      return
    end select
  end function cubetools_userstruct_ptr
  !
  subroutine cubetools_sicdef_struct_create(userstruct,name,scope,overwrite,error)
    !------------------------------------------------------------------------
    ! Define a sic from name, scope and overwrite status
    !------------------------------------------------------------------------
    class(userstruct_t),  intent(out)   :: userstruct
    character(len=*),     intent(in)    :: name
    logical,              intent(in)    :: scope
    integer(kind=code_k), intent(in)    :: overwrite
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICDEF>STRUCT'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    userstruct%name = name
    userstruct%scope = scope
    userstruct%overwrite = overwrite
    call userstruct%def(error)
    if (error) return
  end subroutine cubetools_sicdef_struct_create
  !
  subroutine cubetools_sicdef_struct(userstruct,error)
    !------------------------------------------------------------------------
    ! Define a sic structure
    !------------------------------------------------------------------------
    class(userstruct_t), intent(inout) :: userstruct
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICDEF>STRUCT'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%exists(error)
    if (error) return
    call sic_crestructure(userstruct%name,userstruct%scope,error)
    if (error) return
    call userstruct%fetch_desc(error)
    if (error) return
  end subroutine cubetools_sicdef_struct
  !
  subroutine cubetools_sicdef_substruct(userstruct,name,substruct,error)
    !------------------------------------------------------------------------
    ! Define a substructure to the current structure
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    type(userstruct_t),  intent(out)   :: substruct
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICDEF>STRUCT'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    substruct = userstruct
    substruct%overwrite = overwrite_user
    substruct%name = trim(userstruct%name)//link//name
    !
    call substruct%def(error)
    if (error) return
  end subroutine cubetools_sicdef_substruct
  !
  subroutine cubetools_sicdef_create_member(userstruct,name,uservar,error)
    !------------------------------------------------------------------------
    ! Properly initializes a uservar_t to be a member of a struct
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    type(uservar_t),     intent(out)   :: uservar
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICDEF>CREATE>MEMBER'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    uservar%name      = trim(userstruct%name)//link//name
    uservar%scope     = userstruct%scope
    uservar%overwrite = userstruct%overwrite
  end subroutine cubetools_sicdef_create_member
  !
  !------------------------------------------------------------------------
  !
  subroutine cubetools_sicdef_member_r4_0d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a r4 sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    real(kind=4),        intent(in)    :: data
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>R4>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_r4_0d
  !
  subroutine cubetools_sicdef_member_r4_1d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a r4 sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    real(kind=4),        intent(in)    :: data(:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>R4>1D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_r4_1d
  !
  subroutine cubetools_sicdef_member_r4_2d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a r4 sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    real(kind=4),        intent(in)    :: data(:,:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>R4>2D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_r4_2d
  !
  subroutine cubetools_sicdef_member_r4_3d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a r4 sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    real(kind=4),        intent(in)    :: data(:,:,:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>R4>3D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_r4_3d
  !
  !------------------------------------------------------------------------
  !
  subroutine cubetools_sicdef_member_r8_0d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a r8 sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    real(kind=8),        intent(in)    :: data
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>R8>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_r8_0d
  !
  subroutine cubetools_sicdef_member_r8_1d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a r8 sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    real(kind=8),        intent(in)    :: data(:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>R8>1D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_r8_1d
  !
  subroutine cubetools_sicdef_member_r8_2d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a r8 sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    real(kind=8),        intent(in)    :: data(:,:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>R8>2D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_r8_2d
  !
  subroutine cubetools_sicdef_member_r8_3d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a r8 sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    real(kind=8),        intent(in)    :: data(:,:,:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>R8>3D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_r8_3d
  !
  !------------------------------------------------------------------------
  !
  subroutine cubetools_sicdef_member_c4_0d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a c4 sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    complex(kind=4),     intent(in)    :: data
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>C4>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_c4_0d
  !
  subroutine cubetools_sicdef_member_c4_1d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a c4 sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    complex(kind=4),     intent(in)    :: data(:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>C4>1D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_c4_1d
  !
  subroutine cubetools_sicdef_member_c4_2d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a c4 sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    complex(kind=4),     intent(in)    :: data(:,:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>C4>2D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_c4_2d
  !
  subroutine cubetools_sicdef_member_c4_3d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a c4 sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    complex(kind=4),     intent(in)    :: data(:,:,:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>C4>3D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_c4_3d
  !
  !------------------------------------------------------------------------
  !
  subroutine cubetools_sicdef_member_i4_0d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a i4 sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    integer(kind=4),     intent(in)    :: data
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>I4>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_i4_0d
  !
  subroutine cubetools_sicdef_member_i4_1d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a i4 sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    integer(kind=4),     intent(in)    :: data(:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>I4>1D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_i4_1d
  !
  subroutine cubetools_sicdef_member_i4_2d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a i4 sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    integer(kind=4),     intent(in)    :: data(:,:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>I4>2D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_i4_2d
  !
  subroutine cubetools_sicdef_member_i4_3d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a i4 sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    integer(kind=4),     intent(in)    :: data(:,:,:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>I4>3D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_i4_3d
  !
  !------------------------------------------------------------------------
  !
  subroutine cubetools_sicdef_member_i8_0d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a i8 sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    integer(kind=8),     intent(in)    :: data
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>I8>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_i8_0d
  !
  subroutine cubetools_sicdef_member_i8_1d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a i8 sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    integer(kind=8),     intent(in)    :: data(:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>I8>1D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_i8_1d
  !
  subroutine cubetools_sicdef_member_i8_2d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a i8 sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    integer(kind=8),     intent(in)    :: data(:,:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>I8>2D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_i8_2d
  !
  subroutine cubetools_sicdef_member_i8_3d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a i8 sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    integer(kind=8),     intent(in)    :: data(:,:,:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>I8>3D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_i8_3d
  !
  !------------------------------------------------------------------------
  !
  subroutine cubetools_sicdef_member_st_0d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a string sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    character(len=*),    intent(in)    :: data
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>ST>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_st_0d
  !
  subroutine cubetools_sicdef_member_st_1d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a string sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    character(len=*),    intent(in)    :: data(:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>ST>1D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_st_1d
  !
  !------------------------------------------------------------------------
  !
  subroutine cubetools_sicdef_member_lg_0d(userstruct,name,data,error)
    !------------------------------------------------------------------------
    ! Define a logical sic variable as a member of a userstruct and fills
    ! it with data
    ! ------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    logical,             intent(in)    :: data
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICDEF>MEMBER>LG>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call userstruct%create_member(name,member,error)
    if (error) return
    call member%set(data,error)
    if (error) return
  end subroutine cubetools_sicdef_member_lg_0d
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_sicget_substruct(userstruct,name,substruct,error)
    !----------------------------------------------------------------------
    ! Gets a substruct of name
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    type(userstruct_t),  intent(out)   :: substruct
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>MEMBER>R4>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),substruct,error)
    if (error) return
  end subroutine cubetools_sicget_substruct
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_sicget_member_r4_0d(userstruct,name,data,error)
    !----------------------------------------------------------------------
    ! Gets the value of an R4 member of the structure
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    real(kind=4),        intent(out)   :: data
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICGET>MEMBER>R4>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),member,error)
    if (error) return
    call member%get(data,error)
    if (error) return
  end subroutine cubetools_sicget_member_r4_0d
  !
  subroutine cubetools_sicget_member_r4_1d(userstruct,name,data,error)
    !----------------------------------------------------------------------
    ! Gets the value of an R4 member of the structure
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    real(kind=4),        intent(out)   :: data(:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICGET>MEMBER>R4>1D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),member,error)
    if (error) return
    call member%get(data,error)
    if (error) return
  end subroutine cubetools_sicget_member_r4_1d
  !
  subroutine cubetools_sicget_member_r4_2d(userstruct,name,data,error)
    !----------------------------------------------------------------------
    ! Gets the value of an R4 member of the structure
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    real(kind=4),        intent(out)   :: data(:,:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICGET>MEMBER>R4>2D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),member,error)
    if (error) return
    call member%get(data,error)
    if (error) return
  end subroutine cubetools_sicget_member_r4_2d
  !
  subroutine cubetools_sicget_member_r4_3d(userstruct,name,data,error)
    !----------------------------------------------------------------------
    ! Gets the value of an R4 member of the structure
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    real(kind=4),        intent(out)   :: data(:,:,:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICGET>MEMBER>R4>3D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),member,error)
    if (error) return
    call member%get(data,error)
    if (error) return
  end subroutine cubetools_sicget_member_r4_3d
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_sicget_member_r8_0d(userstruct,name,data,error)
    !----------------------------------------------------------------------
    ! Gets the value of an R8 member of the structure
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    real(kind=8),        intent(out)   :: data
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICGET>MEMBER>R8>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),member,error)
    if (error) return
    call member%get(data,error)
    if (error) return
  end subroutine cubetools_sicget_member_r8_0d
  !
  subroutine cubetools_sicget_member_r8_1d(userstruct,name,data,error)
    !----------------------------------------------------------------------
    ! Gets the value of an R8 member of the structure
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    real(kind=8),        intent(out)   :: data(:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICGET>MEMBER>R8>1D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),member,error)
    if (error) return
    call member%get(data,error)
    if (error) return
  end subroutine cubetools_sicget_member_r8_1d
  !
  subroutine cubetools_sicget_member_r8_2d(userstruct,name,data,error)
    !----------------------------------------------------------------------
    ! Gets the value of an R8 member of the structure
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    real(kind=8),        intent(out)   :: data(:,:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICGET>MEMBER>R8>2D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),member,error)
    if (error) return
    call member%get(data,error)
    if (error) return
  end subroutine cubetools_sicget_member_r8_2d
  !
  subroutine cubetools_sicget_member_r8_3d(userstruct,name,data,error)
    !----------------------------------------------------------------------
    ! Gets the value of an R8 member of the structure
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    real(kind=8),        intent(out)   :: data(:,:,:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICGET>MEMBER>R8>3D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),member,error)
    if (error) return
    call member%get(data,error)
    if (error) return
  end subroutine cubetools_sicget_member_r8_3d
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_sicget_member_c4_0d(userstruct,name,data,error)
    !----------------------------------------------------------------------
    ! Gets the value of an C4 member of the structure
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    complex(kind=4),     intent(out)   :: data
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICGET>MEMBER>C4>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),member,error)
    if (error) return
    call member%get(data,error)
    if (error) return
  end subroutine cubetools_sicget_member_c4_0d
  !
  subroutine cubetools_sicget_member_c4_1d(userstruct,name,data,error)
    !----------------------------------------------------------------------
    ! Gets the value of an C4 member of the structure
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    complex(kind=4),     intent(out)   :: data(:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICGET>MEMBER>C4>1D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),member,error)
    if (error) return
    call member%get(data,error)
    if (error) return
  end subroutine cubetools_sicget_member_c4_1d
  !
  subroutine cubetools_sicget_member_c4_2d(userstruct,name,data,error)
    !----------------------------------------------------------------------
    ! Gets the value of an C4 member of the structure
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    complex(kind=4),     intent(out)   :: data(:,:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICGET>MEMBER>C4>2D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),member,error)
    if (error) return
    call member%get(data,error)
    if (error) return
  end subroutine cubetools_sicget_member_c4_2d
  !
  subroutine cubetools_sicget_member_c4_3d(userstruct,name,data,error)
    !----------------------------------------------------------------------
    ! Gets the value of an C4 member of the structure
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    complex(kind=4),     intent(out)   :: data(:,:,:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICGET>MEMBER>C4>3D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),member,error)
    if (error) return
    call member%get(data,error)
    if (error) return
  end subroutine cubetools_sicget_member_c4_3d
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_sicget_member_i4_0d(userstruct,name,data,error)
    !----------------------------------------------------------------------
    ! Gets the value of an I4 member of the structure
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    integer(kind=4),     intent(out)   :: data
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICGET>MEMBER>I4>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),member,error)
    if (error) return
    call member%get(data,error)
    if (error) return
  end subroutine cubetools_sicget_member_i4_0d
  !
  subroutine cubetools_sicget_member_i4_1d(userstruct,name,data,error)
    !----------------------------------------------------------------------
    ! Gets the value of an I4 member of the structure
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    integer(kind=4),     intent(out)   :: data(:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICGET>MEMBER>I4>1D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),member,error)
    if (error) return
    call member%get(data,error)
    if (error) return
  end subroutine cubetools_sicget_member_i4_1d
  !
  subroutine cubetools_sicget_member_i4_2d(userstruct,name,data,error)
    !----------------------------------------------------------------------
    ! Gets the value of an I4 member of the structure
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    integer(kind=4),     intent(out)   :: data(:,:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICGET>MEMBER>I4>2D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),member,error)
    if (error) return
    call member%get(data,error)
    if (error) return
  end subroutine cubetools_sicget_member_i4_2d
  !
  subroutine cubetools_sicget_member_i4_3d(userstruct,name,data,error)
    !----------------------------------------------------------------------
    ! Gets the value of an I4 member of the structure
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    integer(kind=4),     intent(out)   :: data(:,:,:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICGET>MEMBER>I4>3D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),member,error)
    if (error) return
    call member%get(data,error)
    if (error) return
  end subroutine cubetools_sicget_member_i4_3d
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_sicget_member_i8_0d(userstruct,name,data,error)
    !----------------------------------------------------------------------
    ! Gets the value of an I8 member of the structure
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    integer(kind=8),     intent(out)   :: data
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICGET>MEMBER>I8>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),member,error)
    if (error) return
    call member%get(data,error)
    if (error) return
  end subroutine cubetools_sicget_member_i8_0d
  !
  subroutine cubetools_sicget_member_i8_1d(userstruct,name,data,error)
    !----------------------------------------------------------------------
    ! Gets the value of an I8 member of the structure
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    integer(kind=8),     intent(out)   :: data(:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICGET>MEMBER>I8>1D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),member,error)
    if (error) return
    call member%get(data,error)
    if (error) return
  end subroutine cubetools_sicget_member_i8_1d
  !
  subroutine cubetools_sicget_member_i8_2d(userstruct,name,data,error)
    !----------------------------------------------------------------------
    ! Gets the value of an I8 member of the structure
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    integer(kind=8),     intent(out)   :: data(:,:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICGET>MEMBER>I8>2D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),member,error)
    if (error) return
    call member%get(data,error)
    if (error) return
  end subroutine cubetools_sicget_member_i8_2d
  !
  subroutine cubetools_sicget_member_i8_3d(userstruct,name,data,error)
    !----------------------------------------------------------------------
    ! Gets the value of an I8 member of the structure
    !------------------------------------------------------------------------
    class(userstruct_t), intent(in)    :: userstruct
    character(len=*),    intent(in)    :: name
    integer(kind=8),     intent(out)   :: data(:,:,:)
    logical,             intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICGET>MEMBER>I8>3D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),member,error)
    if (error) return
    call member%get(data,error)
    if (error) return
  end subroutine cubetools_sicget_member_i8_3d
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_sicget_member_st_0d(userstruct,name,data,error)
    !----------------------------------------------------------------------
    ! Gets the value of an string member of the structure
    !------------------------------------------------------------------------
    class(userstruct_t),   intent(in)    :: userstruct
    character(len=*),      intent(in)    :: name
    character(len=argu_l), intent(out)   :: data
    logical,               intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICGET>MEMBER>ST>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),member,error)
    if (error) return
    call member%get(data,error)
    if (error) return
  end subroutine cubetools_sicget_member_st_0d
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_sicget_member_lg_0d(userstruct,name,data,error)
    !----------------------------------------------------------------------
    ! Gets the value of an string member of the structure
    !------------------------------------------------------------------------
    class(userstruct_t),   intent(in)    :: userstruct
    character(len=*),      intent(in)    :: name
    logical,               intent(out)   :: data
    logical,               intent(inout) :: error
    !
    type(uservar_t) :: member
    character(len=*), parameter :: rname='SICGET>MEMBER>LG>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_userspace_get(trim(userstruct%name)//'%'//trim(name),member,error)
    if (error) return
    call member%get(data,error)
    if (error) return
  end subroutine cubetools_sicget_member_lg_0d
end module cubetools_userstruct
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
