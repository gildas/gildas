!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubesyntax_datatype_types
  use cubetools_parameters
  use cubetools_structure
  use cubesyntax_keyval_types
  !---------------------------------------------------------------------
  ! Support module for option
  !   /DATATYPE REAL|COMPLEX|...
  ! and related methods
  !---------------------------------------------------------------------
  !
  public :: datatype_comm_t,datatype_user_t,datatype_prog_t
  public :: cubesyntax_datatype
  private
  !
  integer(kind=4), parameter :: ndatatypes=6
  character(len=*), parameter :: datatypes(ndatatypes) =  &
    (/ '*      ','REAL   ','DOUBLE ','COMPLEX','INTEGER','LONG   ' /)
  !
  type :: datatype_comm_t
    type(keyval_keyword_comm_t) :: opt
  contains
    procedure, public :: register => datatype_comm_register
    procedure, public :: parse    => datatype_comm_parse
  end type datatype_comm_t
  !
  type :: datatype_user_t
    type(keyval_keyword_user_t) :: val
  contains
    procedure, public :: toprog => datatype_user_toprog
  end type datatype_user_t
  !
  type, extends(keyval_keyword_prog_t) :: datatype_prog_t
    integer(kind=code_k) :: code = code_null
  end type datatype_prog_t
  !
  interface cubesyntax_datatype
    module procedure datatype_name_to_code
    module procedure datatype_code_to_name
  end interface cubesyntax_datatype
  !
contains
  !
  subroutine datatype_comm_register(comm,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(datatype_comm_t), intent(inout) :: comm
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='DATATYPE>COMM>REGISTER'
    !
    call comm%opt%register(  &
        'DATATYPE', &
        'Select datatype to be used', &
        strg_star,  &
        datatypes,  &
        inflexible,  &
        error)
     if (error)  return
  end subroutine datatype_comm_register
  !
  subroutine datatype_comm_parse(comm,line,user,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(datatype_comm_t), intent(in)    :: comm
    character(len=*),       intent(in)    :: line
    type(datatype_user_t),  intent(inout) :: user
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='DATATYPE>COMM>PARSE'
    !
    call comm%opt%parse(line,user%val,error)
    if (error) return
  end subroutine datatype_comm_parse
  !
  subroutine datatype_user_toprog(user,comm,prog,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(datatype_user_t), intent(in)    :: user
    type(datatype_comm_t),  intent(in)    :: comm
    type(datatype_prog_t),  intent(inout) :: prog
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='DATATYPE>USER>TOPROG'
    !
    call user%val%toprog(comm%opt,prog,error)
    if (error)  return
    prog%do = prog%keyword.ne.strg_star
    prog%code = datatype_name_to_code(prog%keyword)
  end subroutine datatype_user_toprog
  !
  function datatype_name_to_code(name)
    use gbl_format
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    integer(kind=code_k) :: datatype_name_to_code
    character(len=*), intent(in)  :: name
    !
    ! Note: assume the keywords were previously parsed (upcased +
    ! disambiguised)
    select case (name)
    case ('REAL')
      datatype_name_to_code = fmt_r4
    case ('DOUBLE')
      datatype_name_to_code = fmt_r8
    case ('COMPLEX')
      datatype_name_to_code = fmt_c4
    case ('INTEGER')
      datatype_name_to_code = fmt_i4
    case ('LONG')
      datatype_name_to_code = fmt_i8
    case default
      datatype_name_to_code = code_null
    end select
  end function datatype_name_to_code
  !
  function datatype_code_to_name(code)
    use gbl_format
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    character(len=7) :: datatype_code_to_name
    integer(kind=code_k), intent(in) :: code
    !
    select case (code)
    case (fmt_r4)
      datatype_code_to_name = 'REAL'
    case (fmt_r8)
      datatype_code_to_name = 'DOUBLE'
    case (fmt_c4)
      datatype_code_to_name = 'COMPLEX'
    case (fmt_i4)
      datatype_code_to_name = 'INTEGER'
    case (fmt_i8)
      datatype_code_to_name = 'LONG'
    case default
      datatype_code_to_name = '???'
    end select
  end function datatype_code_to_name
  !
end module cubesyntax_datatype_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
