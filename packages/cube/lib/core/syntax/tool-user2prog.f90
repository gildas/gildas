!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_user2prog
  use cubetools_parameters
  use cubesyntax_messaging
  use gkernel_interfaces
  use cubetools_structure
  use cubetools_unit
  !
  public :: cubetools_user2prog_resolve_unk,cubetools_user2prog_resolve_star
  public :: cubetools_user2prog_resolve_all
  public :: cubetools_user2prog_resolve_code
  private
  !
  interface cubetools_user2prog_resolve_unk
     module procedure cubetools_user2prog_resolve_unk_r4
     module procedure cubetools_user2prog_resolve_unk_r8
     module procedure cubetools_user2prog_resolve_unk_i4
     module procedure cubetools_user2prog_resolve_unk_i8
     module procedure cubetools_user2prog_resolve_unk_string
  end interface cubetools_user2prog_resolve_unk
  !
  interface cubetools_user2prog_resolve_star
     module procedure cubetools_user2prog_resolve_star_r4
     module procedure cubetools_user2prog_resolve_star_r8
     module procedure cubetools_user2prog_resolve_star_i4
     module procedure cubetools_user2prog_resolve_star_i8
     module procedure cubetools_user2prog_resolve_star_string
  end interface cubetools_user2prog_resolve_star
  !
  interface cubetools_user2prog_resolve_all
     module procedure cubetools_user2prog_resolve_all_r4
     module procedure cubetools_user2prog_resolve_all_r8
     module procedure cubetools_user2prog_resolve_all_i4
     module procedure cubetools_user2prog_resolve_all_i8
     module procedure cubetools_user2prog_resolve_all_string
  end interface cubetools_user2prog_resolve_all
  !
contains
  !
  subroutine cubetools_user2prog_resolve_unk_r4(user,prog,error)
    !------------------------------------------------------------------------
    ! Transform a string to a r4 value
    !------------------------------------------------------------------------
    character(len=*),  intent(in)    :: user
    real(kind=4),      intent(out)   :: prog
    logical,           intent(inout) :: error
    !
    integer(kind=4) :: nc
    character(len=*), parameter :: rname='USER2PROG>RESOLVE>UNK>R4'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    select case(user)
    case (strg_unk)
       call cubesyntax_message(seve%e,rname,'String is unknown, maybe&
            & uninitialized?')
       error = .true.
       return
    case (strg_star)
       call cubesyntax_message(seve%e,rname,'* syntax not supported for argument')
       error = .true.
       return
    case (strg_equal)
       call cubesyntax_message(seve%e,rname,'= syntax not supported for argument')
       error = .true.
       return
    case default
       nc = len_trim(user)
       call sic_math_real(user,nc,prog,error)
       if (error) then
          call cubesyntax_message(seve%e,rname,'Cannot convert '//trim(user)//' to REAL*4' )
          return
       endif
    end select
  end subroutine cubetools_user2prog_resolve_unk_r4
  !
  subroutine cubetools_user2prog_resolve_unk_r8(user,prog,error)
    !------------------------------------------------------------------------
    ! Transform a string to a r8 value
    !------------------------------------------------------------------------
    character(len=*),  intent(in)    :: user
    real(kind=8),      intent(out)   :: prog
    logical,           intent(inout) :: error
    !
    integer(kind=4) :: nc
    character(len=*), parameter :: rname='USER2PROG>RESOLVE>UNK>R8'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    select case(user)
    case (strg_unk)
       call cubesyntax_message(seve%e,rname,'String is unknown, maybe&
            & uninitialized?')
       error = .true.
       return
    case (strg_star)
       call cubesyntax_message(seve%e,rname,'* syntax not supported for argument')
       error = .true.
       return
    case (strg_equal)
       call cubesyntax_message(seve%e,rname,'= syntax not supported for argument')
       error = .true.
       return
    case default
       nc = len_trim(user)
       call sic_math_dble(user,nc,prog,error)
       if (error) then
          call cubesyntax_message(seve%e,rname,'Cannot convert '//trim(user)//' to REAL*8' )
          return
       endif
    end select
  end subroutine cubetools_user2prog_resolve_unk_r8
  !
  subroutine cubetools_user2prog_resolve_unk_i4(user,prog,error)
    !------------------------------------------------------------------------
    ! Transform a string to a i4 value
    !------------------------------------------------------------------------
    character(len=*), intent(in)    :: user
    integer(kind=4),  intent(out)   :: prog
    logical,          intent(inout) :: error
    !
    integer(kind=4) :: nc
    character(len=*), parameter :: rname='USER2PROG>RESOLVE>UNK>I4'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    select case(user)
    case (strg_unk)
       call cubesyntax_message(seve%e,rname,'String is unknown, maybe&
            & uninitialized?')
       error = .true.
       return
    case (strg_star)
       call cubesyntax_message(seve%e,rname,'* syntax not supported for argument')
       error = .true.
       return
    case (strg_equal)
       call cubesyntax_message(seve%e,rname,'= syntax not supported for argument')
       error = .true.
       return
    case default
       nc = len_trim(user)
       call sic_math_inte(user,nc,prog,error)
       if (error) then
          call cubesyntax_message(seve%e,rname,'Cannot convert '//trim(user)//' to INTEGER*4' )
          return
       endif
    end select
  end subroutine cubetools_user2prog_resolve_unk_i4
  !
  subroutine cubetools_user2prog_resolve_unk_i8(user,prog,error)
    !------------------------------------------------------------------------
    ! Transform a string to a i8 value
    !------------------------------------------------------------------------
    character(len=*), intent(in)    :: user
    integer(kind=8),  intent(out)   :: prog
    logical,          intent(inout) :: error
    !
    integer(kind=4) :: nc
    character(len=*), parameter :: rname='USER2PROG>RESOLVE>UNK>I8'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    select case(user)
    case (strg_unk)
       call cubesyntax_message(seve%e,rname,'String is unknown, maybe&
            & uninitialized?')
       error = .true.
       return
    case (strg_star)
       call cubesyntax_message(seve%e,rname,'* syntax not supported for argument')
       error = .true.
       return
    case (strg_equal)
       call cubesyntax_message(seve%e,rname,'= syntax not supported for argument')
       error = .true.
       return
    case default
       nc = len_trim(user)
       call sic_math_long(user,nc,prog,error)
       if (error) then
          call cubesyntax_message(seve%e,rname,'Cannot convert '//trim(user)//' to INTEGER*8' )
          return
       endif
    end select
  end subroutine cubetools_user2prog_resolve_unk_i8
  !
  subroutine cubetools_user2prog_resolve_unk_string(user,default,prog,error)
    !------------------------------------------------------------------------
    ! tests if string returns the apropriate value
    !------------------------------------------------------------------------
    character(len=*), intent(in)    :: user
    character(len=*), intent(in)    :: default
    character(len=*), intent(out)   :: prog
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='USER2PROG>RESOLVE>UNK>STRING'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    select case(user)
    case (strg_unk)
       call cubesyntax_message(seve%e,rname,'String is unknown, maybe&
            & uninitialized?')
       error = .true.
       return
    case (strg_star)
       call cubesyntax_message(seve%e,rname,'* syntax not supported for argument')
       error = .true.
       return
    case (strg_equal)
       call cubesyntax_message(seve%e,rname,'= syntax not supported for argument')
       error = .true.
       return
    case default
       prog = user
    end select
  end subroutine cubetools_user2prog_resolve_unk_string
  !
  !--------------------------------------------------------------------------
  !
  subroutine cubetools_user2prog_resolve_star_r4(user,unit,default,prog,error)
    !------------------------------------------------------------------------
    ! Transform a string to a r4 value while testing for *
    !------------------------------------------------------------------------
    character(len=*),  intent(in)    :: user     ! [userunit]
    type(unit_user_t), intent(in)    :: unit
    real(kind=4),      intent(in)    :: default  ! [progunit]
    real(kind=4),      intent(out)   :: prog
    logical,           intent(inout) :: error
    !
    integer(kind=4) :: nc
    character(len=*), parameter :: rname='USER2PROG>RESOLVE>STAR>R4'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    select case(user)
    case (strg_unk)
       call cubesyntax_message(seve%e,rname,'String is unknown, maybe&
            & uninitialized?')
       error = .true.
       return
    case (strg_star)
       prog = default
    case (strg_equal)
       call cubesyntax_message(seve%e,rname,'= syntax not supported for argument')
       error = .true.
       return
    case default
       nc = len_trim(user)
       call sic_math_real(user,nc,prog,error)
       if (error) then
          call cubesyntax_message(seve%e,rname,'Cannot convert '//trim(user)//' to REAL*4' )
          return
       endif
       prog = prog*unit%prog_per_user
    end select
  end subroutine cubetools_user2prog_resolve_star_r4
  !
  subroutine cubetools_user2prog_resolve_star_r8(user,unit,default,prog,error)
    !------------------------------------------------------------------------
    ! Transform a string to a r8 value while testing for *
    !------------------------------------------------------------------------
    character(len=*),  intent(in)    :: user     ! [userunit]
    type(unit_user_t), intent(in)    :: unit
    real(kind=8),      intent(in)    :: default  ! [progunit]
    real(kind=8),      intent(out)   :: prog
    logical,           intent(inout) :: error
    !
    integer(kind=4) :: nc
    character(len=*), parameter :: rname='USER2PROG>RESOLVE>STAR>R8'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    select case(user)
    case (strg_unk)
       call cubesyntax_message(seve%e,rname,'String is unknown, maybe&
            & uninitialized?')
       error = .true.
       return
    case (strg_star)
       prog = default
    case (strg_equal)
       call cubesyntax_message(seve%e,rname,'= syntax not supported for argument')
       error = .true.
       return
    case default
       nc = len_trim(user)
       call sic_math_dble(user,nc,prog,error)
       if (error) then
          call cubesyntax_message(seve%e,rname,'Cannot convert '//trim(user)//' to REAL*8' )
          return
       endif
       prog = prog*unit%prog_per_user
    end select
  end subroutine cubetools_user2prog_resolve_star_r8
  !
  subroutine cubetools_user2prog_resolve_star_i4(user,default,prog,error)
    !------------------------------------------------------------------------
    ! Transform a string to a i4 value while testing for *
    !------------------------------------------------------------------------
    character(len=*), intent(in)    :: user
    integer(kind=4),  intent(in)    :: default
    integer(kind=4),  intent(out)   :: prog
    logical,          intent(inout) :: error
    !
    integer(kind=4) :: nc
    character(len=*), parameter :: rname='USER2PROG>RESOLVE>STAR>I4'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    select case(user)
    case (strg_unk)
       call cubesyntax_message(seve%e,rname,'String is unknown, maybe&
            & uninitialized?')
       error = .true.
       return
    case (strg_star)
       prog = default
    case (strg_equal)
       call cubesyntax_message(seve%e,rname,'= syntax not supported for argument')
       error = .true.
       return
    case default
       nc = len_trim(user)
       call sic_math_inte(user,nc,prog,error)
       if (error) then
          call cubesyntax_message(seve%e,rname,'Cannot convert '//trim(user)//' to INTEGER*4' )
          return
       endif
    end select
  end subroutine cubetools_user2prog_resolve_star_i4
  !
  subroutine cubetools_user2prog_resolve_star_i8(user,default,prog,error)
    !------------------------------------------------------------------------
    ! Transform a string to a i8 value while testing for *
    !------------------------------------------------------------------------
    character(len=*), intent(in)    :: user
    integer(kind=8),  intent(in)    :: default
    integer(kind=8),  intent(out)   :: prog
    logical,          intent(inout) :: error
    !
    integer(kind=4) :: nc
    character(len=*), parameter :: rname='USER2PROG>RESOLVE>STAR>I8'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    select case(user)
    case (strg_unk)
       call cubesyntax_message(seve%e,rname,'String is unknown, maybe&
            & uninitialized?')
       error = .true.
       return
    case (strg_star)
       prog = default
    case (strg_equal)
       call cubesyntax_message(seve%e,rname,'= syntax not supported for argument')
       error = .true.
       return
    case default
       nc = len_trim(user)
       call sic_math_long(user,nc,prog,error)
       if (error) then
          call cubesyntax_message(seve%e,rname,'Cannot convert '//trim(user)//' to INTEGER*8' )
          return
       endif
    end select
  end subroutine cubetools_user2prog_resolve_star_i8
  !
  subroutine cubetools_user2prog_resolve_star_string(user,default,prog,error)
    !------------------------------------------------------------------------
    ! tests if string is * and returns the apropriate value
    !------------------------------------------------------------------------
    character(len=*), intent(in)    :: user
    character(len=*), intent(in)    :: default
    character(len=*), intent(out)   :: prog
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='USER2PROG>RESOLVE>STAR>STRING'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    select case(user)
    case (strg_unk)
       call cubesyntax_message(seve%e,rname,'String is unknown, maybe&
            & uninitialized?')
       error = .true.
       return
    case (strg_star)
       prog = default
    case (strg_equal)
       call cubesyntax_message(seve%e,rname,'= syntax not supported for argument')
       error = .true.
       return
    case default
       prog = user
    end select
  end subroutine cubetools_user2prog_resolve_star_string
  !
  !--------------------------------------------------------------------------
  !
  subroutine cubetools_user2prog_resolve_all_r4(user,unit,default,previous,prog,error)
    !------------------------------------------------------------------------
    ! Transform a string to a r4 value while testing for = and *
    !------------------------------------------------------------------------
    character(len=*),  intent(in)    :: user      ! [userunit]
    type(unit_user_t), intent(in)    :: unit
    real(kind=4),      intent(in)    :: default   ! [progunit]
    real(kind=4),      intent(in)    :: previous  ! [progunit]
    real(kind=4),      intent(out)   :: prog
    logical,           intent(inout) :: error
    !
    integer(kind=4) :: nc
    character(len=*), parameter :: rname='USER2PROG>RESOLVE>ALL>R4'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    select case(user)
    case (strg_unk)
       call cubesyntax_message(seve%e,rname,'String is unknown, maybe&
            & uninitialized?')
       error = .true.
       return
    case (strg_star)
       prog = default
    case (strg_equal)
       prog = previous
    case default
       nc = len_trim(user)
       call sic_math_real(user,nc,prog,error)
       if (error) then
          call cubesyntax_message(seve%e,rname,'Cannot convert '//trim(user)//' to REAL*4' )
          return
       endif
       prog = prog*unit%prog_per_user
    end select
  end subroutine cubetools_user2prog_resolve_all_r4
  !
  subroutine cubetools_user2prog_resolve_all_r8(user,unit,default,previous,prog,error)
    !------------------------------------------------------------------------
    ! Transform a string to a r8 value while testing for = and *
    !------------------------------------------------------------------------
    character(len=*),  intent(in)    :: user      ! [userunit]
    type(unit_user_t), intent(in)    :: unit
    real(kind=8),      intent(in)    :: default   ! [progunit]
    real(kind=8),      intent(in)    :: previous  ! [progunit]
    real(kind=8),      intent(out)   :: prog
    logical,           intent(inout) :: error
    !
    integer(kind=4) :: nc
    character(len=*), parameter :: rname='USER2PROG>RESOLVE>ALL>R8'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    select case(user)
    case (strg_unk)
       call cubesyntax_message(seve%e,rname,'String is unknown, maybe&
            & uninitialized?')
       error = .true.
       return
    case (strg_star)
       prog = default
    case (strg_equal)
       prog = previous
    case default
       nc = len_trim(user)
       call sic_math_dble(user,nc,prog,error)
       if (error) then
          call cubesyntax_message(seve%e,rname,'Cannot convert '//trim(user)//' to REAL*8' )
          return
       endif
       prog = prog*unit%prog_per_user
    end select
  end subroutine cubetools_user2prog_resolve_all_r8
  !
  subroutine cubetools_user2prog_resolve_all_i4(user,default,previous,prog,error)
    !------------------------------------------------------------------------
    ! Transform a string to a i4 value while testing for = and *
    !------------------------------------------------------------------------
    character(len=*), intent(in)    :: user
    integer(kind=4),  intent(in)    :: default
    integer(kind=4),  intent(in)    :: previous
    integer(kind=4),  intent(out)   :: prog
    logical,          intent(inout) :: error
    !
    integer(kind=4) :: nc
    character(len=*), parameter :: rname='USER2PROG>RESOLVE>ALL>I4'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    select case(user)
    case (strg_unk)
       call cubesyntax_message(seve%e,rname,'String is unknown, maybe&
            & uninitialized?')
       error = .true.
       return
    case (strg_star)
       prog = default
    case (strg_equal)
       prog = previous
    case default
       nc = len_trim(user)
       call sic_math_inte(user,nc,prog,error)
       if (error) then
          call cubesyntax_message(seve%e,rname,'Cannot convert '//trim(user)//' to INTEGER*4' )
          return
       endif
    end select
  end subroutine cubetools_user2prog_resolve_all_i4
  !
  subroutine cubetools_user2prog_resolve_all_i8(user,default,previous,prog,error)
    !------------------------------------------------------------------------
    ! Transform a string to a i8 value while testing for = and *
    !------------------------------------------------------------------------
    character(len=*), intent(in)    :: user
    integer(kind=8),  intent(in)    :: default
    integer(kind=8),  intent(in)    :: previous
    integer(kind=8),  intent(out)   :: prog
    logical,          intent(inout) :: error
    !
    integer(kind=4) :: nc
    character(len=*), parameter :: rname='USER2PROG>RESOLVE>ALL>I8'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    select case(user)
    case (strg_unk)
       call cubesyntax_message(seve%e,rname,'String is unknown, maybe&
            & uninitialized?')
       error = .true.
       return
    case (strg_star)
       prog = default
    case (strg_equal)
       prog = previous
    case default
       nc = len_trim(user)
       call sic_math_long(user,nc,prog,error)
       if (error) then
          call cubesyntax_message(seve%e,rname,'Cannot convert '//trim(user)//' to INTEGER*8' )
          return
       endif
    end select
  end subroutine cubetools_user2prog_resolve_all_i8
  !
  subroutine cubetools_user2prog_resolve_all_string(user,default,previous,prog,error)
    !------------------------------------------------------------------------
    ! tests if string is * or = and returns the apropriate value
    !------------------------------------------------------------------------
    character(len=*), intent(in)    :: user
    character(len=*), intent(in)    :: default
    character(len=*), intent(in)    :: previous
    character(len=*), intent(out)   :: prog
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='USER2PROG>RESOLVE>ALL>STRING'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    select case(user)
    case (strg_unk)
       call cubesyntax_message(seve%e,rname,'String is unknown, maybe&
            & uninitialized?')
       error = .true.
       return
    case (strg_star)
       prog = default
    case (strg_equal)
       prog = previous
    case default
       prog = user
    end select
  end subroutine cubetools_user2prog_resolve_all_string
  !
  !--------------------------------------------------------------------------
  !
  subroutine cubetools_user2prog_resolve_code(keyarg,user,default,previous,prog,error)
    !------------------------------------------------------------------------
    ! Returns the correct code from a string
    !------------------------------------------------------------------------
    type(keywordlist_comm_t),  intent(in)    :: keyarg
    character(len=*),     intent(in)    :: user
    integer(kind=code_k), intent(in)    :: default
    integer(kind=code_k), intent(in)    :: previous
    integer(kind=code_k), intent(out)   :: prog
    logical,              intent(inout) :: error
    !
    character(len=argu_l) :: codestr
    character(len=*), parameter :: rname='USER2PROG>RESOLVE>CODE'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    select case(user)
    case (strg_star)
       prog = default
    case (strg_equal)
       prog = previous
    case default
       call cubetools_keywordlist_user2prog(keyarg,user,prog,codestr,error)
       if (error) return
    end select
  end subroutine cubetools_user2prog_resolve_code
end module cubetools_user2prog
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
