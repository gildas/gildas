!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage CUBE SYNTAX messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubesyntax_messaging
  use gpack_def
  use gbl_message
  use cubetools_parameters
  !
  public :: syntaxseve,seve,mess_l
  public :: cubesyntax_message_set_id,cubesyntax_message
  public :: cubesyntax_message_get_alloc,cubesyntax_message_set_alloc
  public :: cubesyntax_message_get_trace,cubesyntax_message_set_trace
  public :: cubesyntax_message_get_others,cubesyntax_message_set_others
  public :: cubesyntax_message_open_help_file,cubesyntax_message_close_help_file
  private
  !
  type :: cubesyntax_messaging_t
     integer(kind=code_k) :: alloc = seve%d
     integer(kind=code_k) :: trace = seve%t
     integer(kind=code_k) :: others = seve%d
     integer(kind=code_k) :: help = 999
  end type cubesyntax_messaging_t
  type(cubesyntax_messaging_t) :: syntaxseve
  !
  ! Identifier used for message identification
  integer(kind=4) :: cubesyntax_message_id = gpack_global_id ! Default value for startup message
  !
  integer(kind=4) :: help_lun = 0
  !
contains
  !
  subroutine cubesyntax_message_set_id(id)
    !---------------------------------------------------------------------
    ! Alter library id into input id. Should be called by the library
    ! which wants to share its id with the current one.
    !---------------------------------------------------------------------
    integer(kind=4), intent(in) :: id
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='MESSAGE>SET>ID'
    !
    cubesyntax_message_id = id
    write (mess,'(A,I0)') 'Now use id #',cubesyntax_message_id
    call cubesyntax_message(seve%d,rname,mess)
  end subroutine cubesyntax_message_set_id
  !
  subroutine cubesyntax_message(mkind,procname,message)
    use cubetools_cmessaging
    !---------------------------------------------------------------------
    ! Messaging facility for the current library. Calls the low-level
    ! (internal) messaging routine with its own identifier.
    !---------------------------------------------------------------------
    integer(kind=4),  intent(in) :: mkind     ! Message kind
    character(len=*), intent(in) :: procname  ! Name of calling procedure
    character(len=*), intent(in) :: message   ! Message string
    !
    if (mkind.eq.syntaxseve%help) then
      if (help_lun.ne.0) then
        write(help_lun,'(A)')  trim(message)
      else
        call cubetools_cmessage(cubesyntax_message_id,seve%r,'SYNTAX>'//procname,message)
      endif
    else
      call cubetools_cmessage(cubesyntax_message_id,mkind,'SYNTAX>'//procname,message)
    endif
  end subroutine cubesyntax_message
  !
  subroutine cubesyntax_message_set_alloc(on)
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       syntaxseve%alloc = seve%i
    else
       syntaxseve%alloc = seve%d
    endif
  end subroutine cubesyntax_message_set_alloc
  !
  subroutine cubesyntax_message_set_trace(on)
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       syntaxseve%trace = seve%i
    else
       syntaxseve%trace = seve%t
    endif
  end subroutine cubesyntax_message_set_trace
  !
  subroutine cubesyntax_message_set_others(on)
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       syntaxseve%others = seve%i
    else
       syntaxseve%others = seve%d
    endif
  end subroutine cubesyntax_message_set_others
  !
  function cubesyntax_message_get_alloc()
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    logical :: cubesyntax_message_get_alloc
    !
    cubesyntax_message_get_alloc = syntaxseve%alloc.eq.seve%i
  end function cubesyntax_message_get_alloc
  !
  function cubesyntax_message_get_trace()
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    logical :: cubesyntax_message_get_trace
    !
    cubesyntax_message_get_trace = syntaxseve%trace.eq.seve%i
  end function cubesyntax_message_get_trace
  !
  function cubesyntax_message_get_others()
    !---------------------------------------------------------------------
    ! 
    !---------------------------------------------------------------------
    logical :: cubesyntax_message_get_others
    !
    cubesyntax_message_get_others = syntaxseve%others.eq.seve%i
  end function cubesyntax_message_get_others
  !
  subroutine cubesyntax_message_open_help_file(file,error)
    use gkernel_interfaces
    !-------------------------------------------------------------------
    ! Open the file where syntaxseve%help messages will be redirected
    ! as long as the file is not closed.
    ! If the file is closed, these messages go to the terminal.
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: file
    logical,          intent(inout) :: error
    !
    integer(kind=4) :: ier
    character(len=*), parameter :: rname='MESSAGE>OPEN>HELP>FILE'
    !
    ier = sic_getlun(help_lun)
    if (ier.ne.1) then
      call cubesyntax_message(seve%e,rname,'No logical unit left')
      error = .true.
      return
    endif
    ier = sic_open(help_lun,file,'NEW',.false.)
    if (ier.ne.0) then
      call cubesyntax_message(seve%e,rname,'Cannot open file '//file)
      error = .true.
      call sic_frelun(help_lun)
      return
    endif
  end subroutine cubesyntax_message_open_help_file
  !
  subroutine cubesyntax_message_close_help_file(error)
    use gkernel_interfaces
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    integer(kind=4) :: ier
    character(len=*), parameter :: rname='MESSAGE>CLOSE>HELP>FILE'
    !
    ier = sic_close(help_lun)
    call sic_frelun(help_lun)
    help_lun = 0
  end subroutine cubesyntax_message_close_help_file
end module cubesyntax_messaging
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
