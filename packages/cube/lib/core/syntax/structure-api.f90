!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_structure
  use cubetools_package
  use cubetools_language
  use cubetools_standard_comm
  use cubetools_standard_opt
  use cubetools_obsolete_opt
  use cubetools_primitive_arg
  use cubetools_standard_arg
  use cubetools_keywordlist_types
  use cubetools_unit_arg
  use cubetools_structure_main
  !---------------------------------------------------------------------
  ! Provide the high level API (types, procedures) to programmers who
  ! design commands or options.
  !---------------------------------------------------------------------
  !
  ! Package
  public :: pack  ! ZZZ Should not be public, provide API to interact with it instead.
  public :: cubetools_register_package
  public :: cubetools_list_package_languages
  public :: cubetools_structure_help_iterate
  !
  ! Languages
  public :: lang_k
  public :: language_t
  public :: cubetools_register_language
  public :: cubetools_list_language_commands
  public :: cubetools_register_dict
  public :: cubetools_pack_has_language
  !
  ! Commands
  public :: comm_k
  public :: command_t
  public :: cubetools_register_command
  public :: cubetools_execute_command
  public :: cubetools_show_command
  !
  ! Options
  public :: opti_k
  public :: option_t,obsolete_opt_t
  public :: cubetools_register_option
  public :: cubetools_nopt
  !
  ! Arguments
  public :: narg_k
  public :: primitive_arg_t,standard_arg_t,keywordlist_comm_t,unit_arg_t
  public :: cubetools_register_primitive_arg
  public :: cubetools_keywordlist_user2prog
  public :: cubetools_getarg
  !
  ! Products
  public :: cubetools_register_primitive_prod
  !
  private
  !
end module cubetools_structure
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
