module cubetools_uservar ! should be renamed to uservar
  use gkernel_interfaces
  use gbl_format
  use cubesyntax_messaging
  use cubetools_parameters
  use cubetools_userspace
  !
  ! Formats
  character(len=*), parameter :: fmt1d = '(a,"[",i0,"]")'
  character(len=*), parameter :: fmt2d = '(a,"[",i0,",",i0,"]")'
  character(len=*), parameter :: fmt3d = '(a,"[",i0,",",i0,",",i0,"]")'
  !
  !
  type, extends(userspace_t) :: uservar_t
   contains
     procedure :: create    => cubetools_sicdef_var_create  
     !
     procedure :: get_dims  => cubetools_sicdef_getdims
     !
     generic   :: set       => def_r4_0d,def_r4_1d,def_r4_2d,def_r4_3d,&
                               def_r8_0d,def_r8_1d,def_r8_2d,def_r8_3d,&
                               def_c4_0d,def_c4_1d,def_c4_2d,def_c4_3d,&
                               def_i4_0d,def_i4_1d,def_i4_2d,def_i4_3d,&
                               def_i8_0d,def_i8_1d,def_i8_2d,def_i8_3d,&
                               def_st_0d,def_st_1d,&
                               def_lg_0d
     !
     generic   :: get       => get_r4_0d,get_r4_1d,get_r4_2d,get_r4_3d,&
                               get_r8_0d,get_r8_1d,get_r8_2d,get_r8_3d,&
                               get_c4_0d,get_c4_1d,get_c4_2d,get_c4_3d,&
                               get_i4_0d,get_i4_1d,get_i4_2d,get_i4_3d,&
                               get_i8_0d,get_i8_1d,get_i8_2d,get_i8_3d,&
                               get_st_0d,&
                               get_lg_0d
     !
     procedure :: def_r4_0d => cubetools_sicdef_r4_0d
     procedure :: def_r4_1d => cubetools_sicdef_r4_1d
     procedure :: def_r4_2d => cubetools_sicdef_r4_2d
     procedure :: def_r4_3d => cubetools_sicdef_r4_3d
     !
     procedure :: def_r8_0d => cubetools_sicdef_r8_0d
     procedure :: def_r8_1d => cubetools_sicdef_r8_1d
     procedure :: def_r8_2d => cubetools_sicdef_r8_2d
     procedure :: def_r8_3d => cubetools_sicdef_r8_3d
     !
     procedure :: def_c4_0d => cubetools_sicdef_c4_0d
     procedure :: def_c4_1d => cubetools_sicdef_c4_1d
     procedure :: def_c4_2d => cubetools_sicdef_c4_2d
     procedure :: def_c4_3d => cubetools_sicdef_c4_3d
     !
     procedure :: def_i4_0d => cubetools_sicdef_i4_0d
     procedure :: def_i4_1d => cubetools_sicdef_i4_1d
     procedure :: def_i4_2d => cubetools_sicdef_i4_2d
     procedure :: def_i4_3d => cubetools_sicdef_i4_3d
     !
     procedure :: def_i8_0d => cubetools_sicdef_i8_0d
     procedure :: def_i8_1d => cubetools_sicdef_i8_1d
     procedure :: def_i8_2d => cubetools_sicdef_i8_2d
     procedure :: def_i8_3d => cubetools_sicdef_i8_3d
     !
     procedure :: def_st_0d => cubetools_sicdef_st_0d
     procedure :: def_st_1d => cubetools_sicdef_st_1d
     !
     procedure :: def_lg_0d => cubetools_sicdef_lg_0d
     !
     !----------------------------------------------------------------------
     !
     procedure :: get_r4_0d => cubetools_sicget_r4_0d
     procedure :: get_r4_1d => cubetools_sicget_r4_1d
     procedure :: get_r4_2d => cubetools_sicget_r4_2d
     procedure :: get_r4_3d => cubetools_sicget_r4_3d
     !
     procedure :: get_r8_0d => cubetools_sicget_r8_0d
     procedure :: get_r8_1d => cubetools_sicget_r8_1d
     procedure :: get_r8_2d => cubetools_sicget_r8_2d
     procedure :: get_r8_3d => cubetools_sicget_r8_3d
     !
     procedure :: get_c4_0d => cubetools_sicget_c4_0d
     procedure :: get_c4_1d => cubetools_sicget_c4_1d
     procedure :: get_c4_2d => cubetools_sicget_c4_2d
     procedure :: get_c4_3d => cubetools_sicget_c4_3d
     !
     procedure :: get_i4_0d => cubetools_sicget_i4_0d
     procedure :: get_i4_1d => cubetools_sicget_i4_1d
     procedure :: get_i4_2d => cubetools_sicget_i4_2d
     procedure :: get_i4_3d => cubetools_sicget_i4_3d
     !
     procedure :: get_i8_0d => cubetools_sicget_i8_0d
     procedure :: get_i8_1d => cubetools_sicget_i8_1d
     procedure :: get_i8_2d => cubetools_sicget_i8_2d
     procedure :: get_i8_3d => cubetools_sicget_i8_3d
     !
     procedure :: get_st_0d => cubetools_sicget_st_0d
     !
     procedure :: get_lg_0d => cubetools_sicget_lg_0d
  end type uservar_t
  !
  public :: uservar_t, cubetools_userspace_get
  public :: cubetools_uservar_ptr
  private
  !
contains
  !
  function cubetools_uservar_ptr(userspace,error)
    !-------------------------------------------------------------------
    ! Check if the input class is of type(uservar_t), and return
    ! a pointer to it if relevant.
    !-------------------------------------------------------------------
    type(uservar_t),    pointer :: cubetools_uservar_ptr  ! Function value on return
    class(userspace_t), target        :: userspace
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: rname='USERVAR>PTR'
    !
    select type(userspace)
    type is (uservar_t)
      cubetools_uservar_ptr => userspace
    class default
      cubetools_uservar_ptr => null()
      call cubesyntax_message(seve%e,rname,  &
        'Internal error: user space is not a uservar_t type')
      error = .true.
      return
    end select
  end function cubetools_uservar_ptr
  !
  subroutine cubetools_sicdef_var_create(uservar,name,scope,overwrite,error)
    !------------------------------------------------------------------------
    ! Create a uservar instance from name and scope 
    !------------------------------------------------------------------------
    class(uservar_t),     intent(out)   :: uservar
    character(len=*),     intent(in)    :: name
    logical,              intent(in)    :: scope
    integer(kind=code_k), intent(in)    :: overwrite
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICDEF>VAR'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    uservar%name = name
    uservar%scope = scope
    uservar%overwrite = overwrite
    call uservar%exists(error)
    if (error) return
  end subroutine cubetools_sicdef_var_create
  !
  subroutine cubetools_sicdef_r4_0d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a r4 sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    real(kind=4),     intent(in)    :: data
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICDEF>R4>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    call sic_defvariable(fmt_r4,trim(uservar%name),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_fill(rname,trim(uservar%name),data,error)
    if (error) return
  end subroutine cubetools_sicdef_r4_0d
  !
  subroutine cubetools_sicdef_r4_1d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a r4 sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    real(kind=4),     intent(in)    :: data(:)
    logical,          intent(inout) :: error
    !
    character(len=varn_l) :: fullname
    integer(kind=size_length) :: dim
    character(len=*), parameter :: rname='SICDEF>R4>1D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    !
    dim = size(data)
    write(fullname,fmt1d) trim(uservar%name),dim
    !
    call sic_defvariable(fmt_r4,trim(fullname),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_fill(rname,trim(uservar%name),data,dim,error)
    if (error) return
  end subroutine cubetools_sicdef_r4_1d
  !
  subroutine cubetools_sicdef_r4_2d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a r4 sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    real(kind=4),     intent(in)    :: data(:,:)
    logical,          intent(inout) :: error
    !
    character(len=varn_l) :: fullname
    integer(kind=size_length) :: dims(2)
    character(len=*), parameter :: rname='SICDEF>R4>2D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    !
    dims = shape(data)
    write(fullname,fmt2d) trim(uservar%name),dims(1),dims(2)
    !
    call sic_defvariable(fmt_r4,trim(fullname),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_fillr4_1d(rname,uservar%name,data,dims(1)*dims(2),error)
    if (error) return
  end subroutine cubetools_sicdef_r4_2d
  !
  subroutine cubetools_sicdef_r4_3d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a r4 sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    real(kind=4),     intent(in)    :: data(:,:,:)
    logical,          intent(inout) :: error
    !
    character(len=varn_l) :: fullname
    integer(kind=size_length) :: dims(3)
    character(len=*), parameter :: rname='SICDEF>R4>3D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    !
    dims = shape(data)
    write(fullname,fmt3d) trim(uservar%name),dims(1),dims(2),dims(3)
    !
    call sic_defvariable(fmt_r4,trim(fullname),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_fillr4_1d(rname,uservar%name,data,dims(1)*dims(2)*dims(3),error)
    if (error) return
  end subroutine cubetools_sicdef_r4_3d
  !
  !------------------------------------------------------------------------
  !
  subroutine cubetools_sicdef_r8_0d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a r8 sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    real(kind=8),     intent(in)    :: data
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICDEF>R8>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    call sic_defvariable(fmt_r8,trim(uservar%name),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_fill(rname,trim(uservar%name),data,error)
    if (error) return
  end subroutine cubetools_sicdef_r8_0d
  !
  subroutine cubetools_sicdef_r8_1d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a r8 sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    real(kind=8),     intent(in)    :: data(:)
    logical,          intent(inout) :: error
    !
    character(len=varn_l) :: fullname
    integer(kind=size_length) :: dim
    character(len=*), parameter :: rname='SICDEF>R8>1D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    !
    dim = size(data)
    write(fullname,fmt1d) trim(uservar%name),dim
    !
    call sic_defvariable(fmt_r8,trim(fullname),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_fill(rname,trim(uservar%name),data,dim,error)
    if (error) return
  end subroutine cubetools_sicdef_r8_1d
  !
  subroutine cubetools_sicdef_r8_2d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a r8 sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    real(kind=8),     intent(in)    :: data(:,:)
    logical,          intent(inout) :: error
    !
    character(len=varn_l) :: fullname
    integer(kind=size_length) :: dims(2)
    character(len=*), parameter :: rname='SICDEF>R8>2D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    !
    dims = shape(data)
    write(fullname,fmt2d) trim(uservar%name),dims(1),dims(2)
    !
    call sic_defvariable(fmt_r8,trim(fullname),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_fillr8_1d(rname,uservar%name,data,dims(1)*dims(2),error)
    if (error) return
  end subroutine cubetools_sicdef_r8_2d
  !
  subroutine cubetools_sicdef_r8_3d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a r8 sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    real(kind=8),     intent(in)    :: data(:,:,:)
    logical,          intent(inout) :: error
    !
    character(len=varn_l) :: fullname
    integer(kind=size_length) :: dims(3)
    character(len=*), parameter :: rname='SICDEF>R8>3D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    !
    dims = shape(data)
    write(fullname,fmt3d) trim(uservar%name),dims(1),dims(2),dims(3)
    !
    call sic_defvariable(fmt_r8,trim(fullname),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_fillr8_1d(rname,uservar%name,data,dims(1)*dims(2)*dims(3),error)
    if (error) return
  end subroutine cubetools_sicdef_r8_3d
  !
  !------------------------------------------------------------------------
  !
  subroutine cubetools_sicdef_c4_0d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a c4 sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    complex(kind=4),  intent(in)    :: data
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICDEF>C4>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    call sic_defvariable(fmt_c4,trim(uservar%name),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_fill(rname,trim(uservar%name),data,error)
    if (error) return
  end subroutine cubetools_sicdef_c4_0d
  !
  subroutine cubetools_sicdef_c4_1d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a c4 sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    complex(kind=4),  intent(in)    :: data(:)
    logical,          intent(inout) :: error
    !
    character(len=varn_l) :: fullname
    integer(kind=size_length) :: dim
    character(len=*), parameter :: rname='SICDEF>C4>1D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    !
    dim = size(data)
    write(fullname,fmt1d) trim(uservar%name),dim
    !
    call sic_defvariable(fmt_c4,trim(fullname),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_fill(rname,trim(uservar%name),data,dim,error)
    if (error) return
  end subroutine cubetools_sicdef_c4_1d
  !
  subroutine cubetools_sicdef_c4_2d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a c4 sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    complex(kind=4),  intent(in)    :: data(:,:)
    logical,          intent(inout) :: error
    !
    character(len=varn_l) :: fullname
    integer(kind=size_length) :: dims(2)
    character(len=*), parameter :: rname='SICDEF>C4>2D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    !
    dims = shape(data)
    write(fullname,fmt2d) trim(uservar%name),dims(1),dims(2)
    !
    call sic_defvariable(fmt_c4,trim(fullname),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_fillc4_1d(rname,uservar%name,data,dims(1)*dims(2),error)
    if (error) return
  end subroutine cubetools_sicdef_c4_2d
  !
  subroutine cubetools_sicdef_c4_3d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a c4 sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    complex(kind=4),  intent(in)    :: data(:,:,:)
    logical,          intent(inout) :: error
    !
    character(len=varn_l) :: fullname
    integer(kind=size_length) :: dims(3)
    character(len=*), parameter :: rname='SICDEF>C4>3D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    !
    dims = shape(data)
    write(fullname,fmt3d) trim(uservar%name),dims(1),dims(2),dims(3)
    !
    call sic_defvariable(fmt_c4,trim(fullname),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_fillc4_1d(rname,uservar%name,data,dims(1)*dims(2)*dims(3),error)
    if (error) return
  end subroutine cubetools_sicdef_c4_3d
  !
  !------------------------------------------------------------------------
  !
  subroutine cubetools_sicdef_i4_0d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a i4 sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    integer(kind=4),  intent(in)    :: data
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICDEF>I4>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    call sic_defvariable(fmt_i4,trim(uservar%name),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_fill(rname,trim(uservar%name),data,error)
    if (error) return
  end subroutine cubetools_sicdef_i4_0d
  !
  subroutine cubetools_sicdef_i4_1d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a i4 sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    integer(kind=4),  intent(in)    :: data(:)
    logical,          intent(inout) :: error
    !
    character(len=varn_l) :: fullname
    integer(kind=size_length) :: dim
    character(len=*), parameter :: rname='SICDEF>I4>1D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    !
    dim = size(data)
    write(fullname,fmt1d) trim(uservar%name),dim
    !
    call sic_defvariable(fmt_i4,trim(fullname),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_fill(rname,trim(uservar%name),data,dim,error)
    if (error) return
  end subroutine cubetools_sicdef_i4_1d
  !
  subroutine cubetools_sicdef_i4_2d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a i4 sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    integer(kind=4),  intent(in)    :: data(:,:)
    logical,          intent(inout) :: error
    !
    character(len=varn_l) :: fullname
    integer(kind=size_length) :: dims(2)
    character(len=*), parameter :: rname='SICDEF>I4>2D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    !
    dims = shape(data)
    write(fullname,fmt2d) trim(uservar%name),dims(1),dims(2)
    !
    call sic_defvariable(fmt_i4,trim(fullname),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_filli4_1d(rname,uservar%name,data,dims(1)*dims(2),error)
    if (error) return
  end subroutine cubetools_sicdef_i4_2d
  !
  subroutine cubetools_sicdef_i4_3d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a i4 sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    integer(kind=4),  intent(in)    :: data(:,:,:)
    logical,          intent(inout) :: error
    !
    character(len=varn_l) :: fullname
    integer(kind=size_length) :: dims(3)
    character(len=*), parameter :: rname='SICDEF>I4>3D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    !
    dims = shape(data)
    write(fullname,fmt3d) trim(uservar%name),dims(1),dims(2),dims(3)
    !
    call sic_defvariable(fmt_i4,trim(fullname),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_filli4_1d(rname,uservar%name,data,dims(1)*dims(2)*dims(3),error)
    if (error) return
  end subroutine cubetools_sicdef_i4_3d
  !
  !------------------------------------------------------------------------
  !
  subroutine cubetools_sicdef_i8_0d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a i8 sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    integer(kind=8),  intent(in)    :: data
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICDEF>I8>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    call sic_defvariable(fmt_i8,trim(uservar%name),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_fill(rname,trim(uservar%name),data,error)
    if (error) return
  end subroutine cubetools_sicdef_i8_0d
  !
  subroutine cubetools_sicdef_i8_1d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a i8 sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    integer(kind=8),  intent(in)    :: data(:)
    logical,          intent(inout) :: error
    !
    character(len=varn_l) :: fullname
    integer(kind=size_length) :: dim
    character(len=*), parameter :: rname='SICDEF>I8>1D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    !
    dim = size(data)
    write(fullname,fmt1d) trim(uservar%name),dim
    !
    call sic_defvariable(fmt_i8,trim(fullname),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_fill(rname,trim(uservar%name),data,dim,error)
    if (error) return
  end subroutine cubetools_sicdef_i8_1d
  !
  subroutine cubetools_sicdef_i8_2d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a i8 sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    integer(kind=8),  intent(in)    :: data(:,:)
    logical,          intent(inout) :: error
    !
    character(len=varn_l) :: fullname
    integer(kind=size_length) :: dims(2)
    character(len=*), parameter :: rname='SICDEF>I8>2D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    !
    dims = shape(data)
    write(fullname,fmt2d) trim(uservar%name),dims(1),dims(2)
    !
    call sic_defvariable(fmt_i8,trim(fullname),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_filli8_1d(rname,uservar%name,data,dims(1)*dims(2),error)
    if (error) return
  end subroutine cubetools_sicdef_i8_2d
  !
  subroutine cubetools_sicdef_i8_3d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a i8 sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    integer(kind=8),  intent(in)    :: data(:,:,:)
    logical,          intent(inout) :: error
    !
    character(len=varn_l) :: fullname
    integer(kind=size_length) :: dims(3)
    character(len=*), parameter :: rname='SICDEF>I8>3D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    !
    dims = shape(data)
    write(fullname,fmt3d) trim(uservar%name),dims(1),dims(2),dims(3)
    !
    call sic_defvariable(fmt_i8,trim(fullname),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_filli8_1d(rname,uservar%name,data,dims(1)*dims(2)*dims(3),error)
    if (error) return
  end subroutine cubetools_sicdef_i8_3d
  !
  !------------------------------------------------------------------------
  !
  subroutine cubetools_sicdef_st_0d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a string sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    character(len=*), intent(in)    :: data
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICDEF>ST>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    call sic_defvariable(len(data),trim(uservar%name),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_fill(rname,trim(uservar%name),data,error)
    if (error) return
  end subroutine cubetools_sicdef_st_0d
  !
  subroutine cubetools_sicdef_st_1d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a string sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    character(len=*), intent(in)    :: data(:)
    logical,          intent(inout) :: error
    !
    character(len=16) :: dims
    integer(kind=size_length) nelem
    character(len=*), parameter :: rname='SICDEF>ST>1D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    nelem = size(data)
    write (dims,'(a,i0,a)') '[',nelem,']'
    call sic_defvariable(len(data),trim(uservar%name)//trim(dims),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_fillch_1d(rname,trim(uservar%name),data,nelem,error)
    if (error) return
  end subroutine cubetools_sicdef_st_1d
  !
  !------------------------------------------------------------------------
  !
  subroutine cubetools_sicdef_lg_0d(uservar,data,error)
    !------------------------------------------------------------------------
    ! Define a logical sic variable and fills it with data
    !------------------------------------------------------------------------
    class(uservar_t), intent(inout) :: uservar
    logical,          intent(in)    :: data
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICDEF>LG>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call uservar%exists(error)
    if (error) return
    call sic_defvariable(fmt_l,trim(uservar%name),uservar%scope,error)
    if (error) return
    call uservar%fetch_desc(error)
    if (error) return
    call sic_variable_fill(rname,trim(uservar%name),data,error)
    if (error) return
  end subroutine cubetools_sicdef_lg_0d
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_sicdef_getdims(uservar,ndim,dims,error)
    !----------------------------------------------------------------------
    ! Return dimensions of the SIC variable
    !----------------------------------------------------------------------
    class(uservar_t),                  intent(in)    :: uservar
    integer(kind=4),                   intent(out)   :: ndim
    integer(kind=entr_k), allocatable, intent(out)   :: dims(:)
    logical,                           intent(inout) :: error
    !
    integer(kind=4) :: ier
    character(len=*), parameter :: rname='SICDEF>GETDIMS'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    ndim = uservar%desc%ndim
    if (ndim.gt.0) then
       allocate(dims(ndim),stat=ier)
       if (failed_allocate(rname,'dims',ier,error)) return
       dims(:) = uservar%desc%dims(1:ndim)
    endif
  end subroutine cubetools_sicdef_getdims
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_sicget_r4_0d(uservar,data,error)
    !----------------------------------------------------------------------
    ! Gets R4 0d data from a SIC variable
    !----------------------------------------------------------------------
    class(uservar_t), intent(in)    :: uservar
    real(kind=4),     intent(inout) :: data
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>R4>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    if (uservar%desc%ndim.ne.0) then
       call cubesyntax_message(seve%e,rname,'SIC variable is not scalar')
       error = .true.
       return
    endif
    call sic_descriptor_getval(uservar%desc,int(1,kind=size_length),data,error)
    if (error)  return
  end subroutine cubetools_sicget_r4_0d
  !
  subroutine cubetools_sicget_r4_1d(uservar,data,error)
    !----------------------------------------------------------------------
    ! Gets R4 1d data from a SIC variable
    !----------------------------------------------------------------------
    class(uservar_t), intent(in)    :: uservar
    real(kind=4),     intent(inout) :: data(:)
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>R4>1D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    if (uservar%desc%ndim.ne.1) then
       call cubesyntax_message(seve%e,rname,'SIC variable is not 1D')
       error = .true.
       return
    endif
    call sic_descriptor_getval(uservar%desc,data,error)
    if (error)  return
  end subroutine cubetools_sicget_r4_1d
  !
  subroutine cubetools_sicget_r4_2d(uservar,data,error)
    !----------------------------------------------------------------------
    ! Gets R4 2d data from a SIC variable
    !----------------------------------------------------------------------
    class(uservar_t), intent(in)    :: uservar
    real(kind=4),     intent(inout) :: data(:,:)
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>R4>2D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    if (uservar%desc%ndim.ne.2) then
       call cubesyntax_message(seve%e,rname,'SIC variable is not 2D')
       error = .true.
       return
    endif
    call sic_descriptor_getval(uservar%desc,data,error)
    if (error)  return
  end subroutine cubetools_sicget_r4_2d
  !
  subroutine cubetools_sicget_r4_3d(uservar,data,error)
    !----------------------------------------------------------------------
    ! Gets R4 3d data from a SIC variable
    !----------------------------------------------------------------------
    class(uservar_t), intent(in)    :: uservar
    real(kind=4),     intent(inout) :: data(:,:,:)
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>R4>3D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    if (uservar%desc%ndim.ne.3) then
       call cubesyntax_message(seve%e,rname,'SIC variable is not 3D')
       error = .true.
       return
    endif
    call sic_descriptor_getval(uservar%desc,data,error)
    if (error)  return
  end subroutine cubetools_sicget_r4_3d
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_sicget_r8_0d(uservar,data,error)
    !----------------------------------------------------------------------
    ! Gets R8 0d data from a SIC variable
    !----------------------------------------------------------------------
    class(uservar_t), intent(in)    :: uservar
    real(kind=8),     intent(inout) :: data
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>R8>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    if (uservar%desc%ndim.ne.0) then
       call cubesyntax_message(seve%e,rname,'SIC variable is not scalar')
       error = .true.
       return
    endif
    call sic_descriptor_getval(uservar%desc,int(1,kind=size_length),data,error)
    if (error)  return
  end subroutine cubetools_sicget_r8_0d
  !
  subroutine cubetools_sicget_r8_1d(uservar,data,error)
    !----------------------------------------------------------------------
    ! Gets R8 1d data from a SIC variable
    !----------------------------------------------------------------------
    class(uservar_t), intent(in)    :: uservar
    real(kind=8),     intent(inout) :: data(:)
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>R8>1D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    if (uservar%desc%ndim.ne.1) then
       call cubesyntax_message(seve%e,rname,'SIC variable is not 1D')
       error = .true.
       return
    endif
    call sic_descriptor_getval(uservar%desc,data,error)
    if (error)  return
  end subroutine cubetools_sicget_r8_1d
  !
  subroutine cubetools_sicget_r8_2d(uservar,data,error)
    !----------------------------------------------------------------------
    ! Gets R8 2d data from a SIC variable
    !----------------------------------------------------------------------
    class(uservar_t), intent(in)    :: uservar
    real(kind=8),     intent(inout) :: data(:,:)
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>R8>2D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    if (uservar%desc%ndim.ne.2) then
       call cubesyntax_message(seve%e,rname,'SIC variable is not 2D')
       error = .true.
       return
    endif
    call sic_descriptor_getval(uservar%desc,data,error)
    if (error)  return
  end subroutine cubetools_sicget_r8_2d
  !
  subroutine cubetools_sicget_r8_3d(uservar,data,error)
    !----------------------------------------------------------------------
    ! Gets R8 3d data from a SIC variable
    !----------------------------------------------------------------------
    class(uservar_t), intent(in)    :: uservar
    real(kind=8),     intent(inout) :: data(:,:,:)
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>R8>3D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    if (uservar%desc%ndim.ne.3) then
       call cubesyntax_message(seve%e,rname,'SIC variable is not 3D')
       error = .true.
       return
    endif
    call sic_descriptor_getval(uservar%desc,data,error)
    if (error)  return
  end subroutine cubetools_sicget_r8_3d
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_sicget_c4_0d(uservar,data,error)
    !----------------------------------------------------------------------
    ! Gets C4 0d data from a SIC variable
    !----------------------------------------------------------------------
    class(uservar_t), intent(in)    :: uservar
    complex(kind=4),  intent(inout) :: data
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>C4>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    if (uservar%desc%ndim.ne.0) then
       call cubesyntax_message(seve%e,rname,'SIC variable is not scalar')
       error = .true.
       return
    endif
    call sic_descriptor_getval(uservar%desc,int(1,kind=size_length),data,error)
    if (error)  return
  end subroutine cubetools_sicget_c4_0d
  !
  subroutine cubetools_sicget_c4_1d(uservar,data,error)
    !----------------------------------------------------------------------
    ! Gets C4 1d data from a SIC variable
    !----------------------------------------------------------------------
    class(uservar_t), intent(in)    :: uservar
    complex(kind=4),  intent(inout) :: data(:)
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>C4>1D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    if (uservar%desc%ndim.ne.1) then
       call cubesyntax_message(seve%e,rname,'SIC variable is not 1D')
       error = .true.
       return
    endif
    call sic_descriptor_getval(uservar%desc,data,error)
    if (error)  return
  end subroutine cubetools_sicget_c4_1d
  !
  subroutine cubetools_sicget_c4_2d(uservar,data,error)
    !----------------------------------------------------------------------
    ! Gets C4 2d data from a SIC variable
    !----------------------------------------------------------------------
    class(uservar_t), intent(in)    :: uservar
    complex(kind=4),  intent(inout) :: data(:,:)
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>C4>2D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    if (uservar%desc%ndim.ne.2) then
       call cubesyntax_message(seve%e,rname,'SIC variable is not 2D')
       error = .true.
       return
    endif
    call sic_descriptor_getval(uservar%desc,data,error)
    if (error)  return
  end subroutine cubetools_sicget_c4_2d
  !
  subroutine cubetools_sicget_c4_3d(uservar,data,error)
    !----------------------------------------------------------------------
    ! Gets C4 3d data from a SIC variable
    !----------------------------------------------------------------------
    class(uservar_t), intent(in)    :: uservar
    complex(kind=4),  intent(inout) :: data(:,:,:)
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>C4>3D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    if (uservar%desc%ndim.ne.3) then
       call cubesyntax_message(seve%e,rname,'SIC variable is not 3D')
       error = .true.
       return
    endif
    call sic_descriptor_getval(uservar%desc,data,error)
    if (error)  return
  end subroutine cubetools_sicget_c4_3d
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_sicget_i4_0d(uservar,data,error)
    !----------------------------------------------------------------------
    ! Gets I4 0d data from a SIC variable
    !----------------------------------------------------------------------
    class(uservar_t), intent(in)    :: uservar
    integer(kind=4),  intent(inout) :: data
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>I4>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    if (uservar%desc%ndim.ne.0) then
       call cubesyntax_message(seve%e,rname,'SIC variable is not scalar')
       error = .true.
       return
    endif
    call sic_descriptor_getval(uservar%desc,int(1,kind=size_length),data,error)
    if (error)  return
  end subroutine cubetools_sicget_i4_0d
  !
  subroutine cubetools_sicget_i4_1d(uservar,data,error)
    !----------------------------------------------------------------------
    ! Gets I4 1d data from a SIC variable
    !----------------------------------------------------------------------
    class(uservar_t), intent(in)    :: uservar
    integer(kind=4),  intent(inout) :: data(:)
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>I4>1D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    if (uservar%desc%ndim.ne.1) then
       call cubesyntax_message(seve%e,rname,'SIC variable is not 1D')
       error = .true.
       return
    endif
    call sic_descriptor_getval(uservar%desc,data,error)
    if (error)  return
  end subroutine cubetools_sicget_i4_1d
  !
  subroutine cubetools_sicget_i4_2d(uservar,data,error)
    !----------------------------------------------------------------------
    ! Gets I4 2d data from a SIC variable
    !----------------------------------------------------------------------
    class(uservar_t), intent(in)    :: uservar
    integer(kind=4),  intent(inout) :: data(:,:)
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>I4>2D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    if (uservar%desc%ndim.ne.2) then
       call cubesyntax_message(seve%e,rname,'SIC variable is not 2D')
       error = .true.
       return
    endif
    call sic_descriptor_getval(uservar%desc,data,error)
    if (error)  return
  end subroutine cubetools_sicget_i4_2d
  !
  subroutine cubetools_sicget_i4_3d(uservar,data,error)
    !----------------------------------------------------------------------
    ! Gets I4 3d data from a SIC variable
    !----------------------------------------------------------------------
    class(uservar_t), intent(in)    :: uservar
    integer(kind=4),  intent(inout) :: data(:,:,:)
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>I4>3D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    if (uservar%desc%ndim.ne.3) then
       call cubesyntax_message(seve%e,rname,'SIC variable is not 3D')
       error = .true.
       return
    endif
    call sic_descriptor_getval(uservar%desc,data,error)
    if (error)  return
  end subroutine cubetools_sicget_i4_3d
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_sicget_i8_0d(uservar,data,error)
    !----------------------------------------------------------------------
    ! Gets I8 0d data from a SIC variable
    !----------------------------------------------------------------------
    class(uservar_t), intent(in)    :: uservar
    integer(kind=8),  intent(inout) :: data
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>I8>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    if (uservar%desc%ndim.ne.0) then
       call cubesyntax_message(seve%e,rname,'SIC variable is not scalar')
       error = .true.
       return
    endif
    call sic_descriptor_getval(uservar%desc,int(1,kind=size_length),data,error)
    if (error)  return
  end subroutine cubetools_sicget_i8_0d
  !
  subroutine cubetools_sicget_i8_1d(uservar,data,error)
    !----------------------------------------------------------------------
    ! Gets I8 1d data from a SIC variable
    !----------------------------------------------------------------------
    class(uservar_t), intent(in)    :: uservar
    integer(kind=8),  intent(inout) :: data(:)
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>I8>1D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    if (uservar%desc%ndim.ne.1) then
       call cubesyntax_message(seve%e,rname,'SIC variable is not 1D')
       error = .true.
       return
    endif
    call sic_descriptor_getval(uservar%desc,data,error)
    if (error)  return
  end subroutine cubetools_sicget_i8_1d
  !
  subroutine cubetools_sicget_i8_2d(uservar,data,error)
    !----------------------------------------------------------------------
    ! Gets I8 2d data from a SIC variable
    !----------------------------------------------------------------------
    class(uservar_t), intent(in)    :: uservar
    integer(kind=8),  intent(inout) :: data(:,:)
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>I8>2D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    if (uservar%desc%ndim.ne.2) then
       call cubesyntax_message(seve%e,rname,'SIC variable is not 2D')
       error = .true.
       return
    endif
    call sic_descriptor_getval(uservar%desc,data,error)
    if (error)  return
  end subroutine cubetools_sicget_i8_2d
  !
  subroutine cubetools_sicget_i8_3d(uservar,data,error)
    !----------------------------------------------------------------------
    ! Gets I8 3d data from a SIC variable
    !----------------------------------------------------------------------
    class(uservar_t), intent(in)    :: uservar
    integer(kind=8),  intent(inout) :: data(:,:,:)
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>I8>3D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    if (uservar%desc%ndim.ne.3) then
       call cubesyntax_message(seve%e,rname,'SIC variable is not 3D')
       error = .true.
       return
    endif
    call sic_descriptor_getval(uservar%desc,data,error)
    if (error)  return
  end subroutine cubetools_sicget_i8_3d
  !
  subroutine cubetools_sicget_st_0d(uservar,data,error)
    !----------------------------------------------------------------------
    ! Gets STring 0d data from a SIC variable
    !----------------------------------------------------------------------
    class(uservar_t), intent(in)    :: uservar
    character(len=*), intent(inout) :: data
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>ST>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    if (uservar%desc%ndim.ne.0) then
       call cubesyntax_message(seve%e,rname,'SIC variable is not scalar')
       error = .true.
       return
    endif
    call sic_descriptor_getval(uservar%desc,int(1,kind=size_length),data,error)
    if (error)  return
  end subroutine cubetools_sicget_st_0d
  !
  subroutine cubetools_sicget_lg_0d(uservar,data,error)
    !----------------------------------------------------------------------
    ! Gets Logical 0d data from a SIC variable
    !----------------------------------------------------------------------
    class(uservar_t), intent(in)    :: uservar
    logical,          intent(inout) :: data
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='SICGET>LG>0D'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    if (uservar%desc%ndim.ne.0) then
       call cubesyntax_message(seve%e,rname,'SIC variable is not scalar')
       error = .true.
       return
    endif
    call sic_descriptor_getval(uservar%desc,int(1,kind=size_length),data,error)
    if (error)  return
  end subroutine cubetools_sicget_lg_0d
end module cubetools_uservar
