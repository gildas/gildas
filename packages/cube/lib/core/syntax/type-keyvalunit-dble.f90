!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubesyntax_keyvalunit_dble_types
  use cubetools_parameters
  use cubetools_structure
  use cubetools_unit_types
  use cubesyntax_value_dble_types
  use cubesyntax_messaging
  !
  public :: keyvalunit_dble_comm_t,keyvalunit_dble_user_t,keyvalunit_dble_prog_t
  private
  !
  type keyvalunit_dble_comm_t
     type(option_t), pointer :: key
     type(value_dble_comm_t) :: value
     type(unit_comm_t)       :: unit
   contains
     procedure, public :: register      => cubesyntax_keyvalunit_dble_comm_register
     procedure, public :: parse         => cubesyntax_keyvalunit_dble_comm_parse
  end type keyvalunit_dble_comm_t
  !
  type keyvalunit_dble_user_t
     logical               :: do = .false.
     character(len=argu_l) :: val = strg_star
     character(len=unit_l) :: unit = strg_star
   contains
     procedure, public :: toprog => cubesyntax_keyvalunit_dble_user_toprog
     procedure, public :: list   => cubesyntax_keyvalunit_dble_user_list
  end type keyvalunit_dble_user_t
  !
  type keyvalunit_dble_prog_t
   contains
     procedure, public :: list   => cubesyntax_keyvalunit_dble_prog_list
  end type keyvalunit_dble_prog_t
  !
contains
  !
  subroutine cubesyntax_keyvalunit_dble_comm_register(comm,&
       keyname,keyabstract,valdefault,unitcode,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(keyvalunit_dble_comm_t), intent(inout) :: comm
    character(len=*),              intent(in)    :: keyname
    character(len=*),              intent(in)    :: keyabstract
    real(kind=dble_k),             intent(in)    :: valdefault
    integer(kind=code_k),          intent(in)    :: unitcode
    logical,                       intent(inout) :: error
    !
    character(len=*), parameter :: rname='KEYVALUNIT>DBLE>COMM>REGISTER'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         keyname,&
         'value [unit]',&
         keyabstract,&
         strg_id,&
         comm%key,&
         error)
    if (error) return
    call comm%value%register(&
         keyname,&
         valdefault,&
         error)
    if (error) return
    call comm%unit%register(&
         keyname,&
         unitcode,&
         error)
    if (error) return
  end subroutine cubesyntax_keyvalunit_dble_comm_register
  !
  subroutine cubesyntax_keyvalunit_dble_comm_parse(comm,line,user,error)
    use cubetools_structure
    !----------------------------------------------------------------------
    ! /KEY value [unit]
    !----------------------------------------------------------------------
    class(keyvalunit_dble_comm_t), intent(in)    :: comm
    character(len=*),              intent(in)    :: line
    type(keyvalunit_dble_user_t),  intent(out)   :: user ! => Initialized!
    logical,                       intent(inout) :: error
    !
    integer(kind=argu_k), parameter :: ival=1
    integer(kind=argu_k), parameter :: iunit=2
    character(len=*), parameter :: rname='KEYVALUNIT>DBLE>COMM>PARSE'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call comm%key%present(line,user%do,error)
    if (error) return
    if (user%do) then
       call cubetools_getarg(line,comm%key,ival,user%val,mandatory,error)
       if (error) return
       call cubetools_getarg(line,comm%key,iunit,user%unit,optional,error)
       if (error) return
    endif
  end subroutine cubesyntax_keyvalunit_dble_comm_parse
  !
  !------------------------------------------------------------------------
  !
  subroutine cubesyntax_keyvalunit_dble_user_toprog(user,comm,prog,error)
    use cubetools_unit
    use cubetools_user2prog
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(keyvalunit_dble_user_t), intent(in)    :: user
    type(keyvalunit_dble_comm_t),  intent(in)    :: comm
    real(kind=dble_k),             intent(inout) :: prog
    logical,                       intent(inout) :: error
    !
    type(unit_user_t) :: unit
    character(len=*), parameter :: rname='KEYVALUNIT>DBLE>USER>TOPROG'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call unit%get_from_name_for_code(user%unit,comm%unit%code,error)
    if (error) return
    call cubetools_user2prog_resolve_star(user%val,unit,comm%value%default,prog,error)
    if (error) return
  end subroutine cubesyntax_keyvalunit_dble_user_toprog
  !
  subroutine cubesyntax_keyvalunit_dble_user_list(user,error)
    !----------------------------------------------------------------------
    ! Mostly for debugging purpose
    !----------------------------------------------------------------------
    class(keyvalunit_dble_user_t), intent(in)    :: user 
    logical,                       intent(inout) :: error
    !
!    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='KEYVALUNIT>DBLE>USER>LIST'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
  end subroutine cubesyntax_keyvalunit_dble_user_list
  !
  !------------------------------------------------------------------------
  !
  subroutine cubesyntax_keyvalunit_dble_prog_list(prog,comm,value,error)
    use cubesyntax_keyvalunit_list_tool
    !-------------------------------------------------------------------
    ! List the information in a user friendly way
    !-------------------------------------------------------------------
    class(keyvalunit_dble_prog_t), intent(in)    :: prog
    type(keyvalunit_dble_comm_t),  intent(in)    :: comm
    real(kind=dble_k),             intent(in)    :: value
    logical,                       intent(inout) :: error
    !
    character(len=*), parameter :: rname='KEYVALUNIT>DBLE>PROG>LIST'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call keyvalunit%list(comm%key%name,value,comm%unit%code,error)
    if (error) return
  end subroutine cubesyntax_keyvalunit_dble_prog_list
end module cubesyntax_keyvalunit_dble_types
! 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
