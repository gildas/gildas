!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubesyntax_keyval_keyword_types
  use cubetools_parameters
  use cubetools_structure
  use cubesyntax_value_keywordlist_types
  use cubesyntax_messaging
  !
  public :: keyval_keyword_comm_t,keyval_keyword_user_t,keyval_keyword_prog_t
  private
  !
  type keyval_keyword_comm_t
     type(option_t), pointer        :: key
     type(value_keywordlist_comm_t) :: value
   contains
     procedure, public :: register => cubesyntax_keyval_keyword_comm_register
     procedure, public :: parse    => cubesyntax_keyval_keyword_comm_parse
  end type keyval_keyword_comm_t
  !
  type keyval_keyword_user_t
     logical               :: do = .false.
     character(len=argu_l) :: val = strg_star
   contains
     procedure, public :: toprog => cubesyntax_keyval_keyword_user_toprog
     procedure, public :: list   => cubesyntax_keyval_keyword_user_list
  end type keyval_keyword_user_t
  !
  type keyval_keyword_prog_t
     logical           :: do = .false. !
     character(len=32) :: keyword      ! Resolved keyword
     integer(kind=4)   :: ikeyword     ! Resolved position in value_keywordlist_comm_t
   contains
     procedure, public :: list   => cubesyntax_keyval_keyword_prog_list
  end type keyval_keyword_prog_t
  !
contains
  !
  subroutine cubesyntax_keyval_keyword_comm_register(comm,&
       keyname,keyabstract,valdefault,list,flexibl,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(keyval_keyword_comm_t), intent(inout) :: comm
    character(len=*),             intent(in)    :: keyname
    character(len=*),             intent(in)    :: keyabstract
    character(len=*),             intent(in)    :: valdefault
    character(len=*),             intent(in)    :: list(:)
    logical,                      intent(in)    :: flexibl
    logical,                      intent(inout) :: error
    !
    character(len=*), parameter :: rname='KEYVAL>KEYWORD>COMM>REGISTER'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         keyname,&
         'value [unit]',&
         keyabstract,&
         strg_id,&
         comm%key,&
         error)
    if (error) return
    call comm%value%register(&
         keyname,&
         valdefault,&
         list,&
         flexibl,&
         error)
    if (error) return
  end subroutine cubesyntax_keyval_keyword_comm_register
  !
  subroutine cubesyntax_keyval_keyword_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! /KEY value
    !----------------------------------------------------------------------
    class(keyval_keyword_comm_t), intent(in)    :: comm
    character(len=*),             intent(in)    :: line
    type(keyval_keyword_user_t),  intent(out)   :: user ! => Initialized!
    logical,                      intent(inout) :: error
    !
    integer(kind=argu_k), parameter :: ival=1
    character(len=*), parameter :: rname='KEYVAL>KEYWORD>COMM>PARSE'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call comm%key%present(line,user%do,error)
    if (error) return
    if (user%do) then
       call cubetools_getarg(line,comm%key,ival,user%val,mandatory,error)
       if (error) return
    endif
  end subroutine cubesyntax_keyval_keyword_comm_parse
  !
  !------------------------------------------------------------------------
  !
  subroutine cubesyntax_keyval_keyword_user_toprog(user,comm,prog,error)
    use cubetools_user2prog
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(keyval_keyword_user_t), intent(in)    :: user
    type(keyval_keyword_comm_t),  intent(in)    :: comm
    class(keyval_keyword_prog_t), intent(inout) :: prog
    logical,                      intent(inout) :: error
    !
    character(len=*), parameter :: rname='KEYVAL>KEYWORD>USER>TOPROG'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    prog%do = user%do
    if (user%do) then
      call cubetools_keywordlist_user2prog(comm%value%value,user%val, &
        prog%ikeyword,prog%keyword,error)
      if (error)  return
    else
      prog%keyword = comm%value%default
      prog%ikeyword = code_null
    endif
  end subroutine cubesyntax_keyval_keyword_user_toprog
  !
  subroutine cubesyntax_keyval_keyword_user_list(user,error)
    !----------------------------------------------------------------------
    ! Mostly for debugging purpose
    !----------------------------------------------------------------------
    class(keyval_keyword_user_t), intent(in)    :: user
    logical,                      intent(inout) :: error
    !
!    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='KEYVAL>KEYWORD>USER>LIST'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
  end subroutine cubesyntax_keyval_keyword_user_list
  !
  !------------------------------------------------------------------------
  !
  subroutine cubesyntax_keyval_keyword_prog_list(prog,comm,value,error)
    use cubesyntax_keyvalunit_list_tool
    !-------------------------------------------------------------------
    ! List the information in a user friendly way
    !-------------------------------------------------------------------
    class(keyval_keyword_prog_t), intent(in)    :: prog
    type(keyval_keyword_comm_t),  intent(in)    :: comm
    character(len=*),             intent(in)    :: value
    logical,                      intent(inout) :: error
    !
    character(len=*), parameter :: rname='KEYVAL>KEYWORD>PROG>LIST'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call keyvalunit%list(comm%key%name,value,error)
    if (error) return
  end subroutine cubesyntax_keyval_keyword_prog_list
end module cubesyntax_keyval_keyword_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
