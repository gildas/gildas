!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubesyntax_keyvalunit_list_tool
  use cubetools_parameters
  use cubetools_unit_types
  use cubetools_format
  use cubesyntax_messaging
  !
  public :: keyvalunit_list_t
  public :: keyvalunit
  private
  !
  type keyvalunit_list_t
   contains
     procedure, private :: list_inte => cubesyntax_keyvalunit_list_inte
!!$     procedure, private :: list_long => cubesyntax_keyvalunit_list_long
     procedure, private :: list_real => cubesyntax_keyvalunit_list_real
     procedure, private :: list_dble => cubesyntax_keyvalunit_list_dble
     procedure, private :: list_strg => cubesyntax_keyvalunit_list_strg
     generic,   public  :: list => list_inte,list_real,list_dble,list_strg
  end type keyvalunit_list_t
  type(keyvalunit_list_t) :: keyvalunit
  !
  integer(kind=4) :: list_l = 40
  !
  type(unit_user_t) :: unit
  character(len=mess_l) :: mess
  !
contains
  !
  subroutine cubesyntax_keyvalunit_list_inte(prog,name,value,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    class(keyvalunit_list_t), intent(in)    :: prog
    character(len=*),         intent(in)    :: name
    integer(kind=inte_k),     intent(in)    :: value
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='KEYVALUNIT>LIST>INTE'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    mess = '  '//cubetools_format_stdkey_boldval(name,value,'i0',list_l)
    call cubesyntax_message(seve%r,rname,mess)
  end subroutine cubesyntax_keyvalunit_list_inte
  !
  subroutine cubesyntax_keyvalunit_list_real(prog,name,value,code,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    class(keyvalunit_list_t), intent(in)    :: prog
    character(len=*),         intent(in)    :: name
    real(kind=real_k),        intent(in)    :: value
    integer(kind=code_k),     intent(in)    :: code
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='KEYVALUNIT>LIST>REAL'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call unit%get_from_code(code,error)
    if (error) return
    mess = '  '//cubetools_format_stdkey_boldval(&
         name,value*unit%user_per_prog,fsimple,list_l)
    mess = trim(mess)//'  '//trim(unit%name)
    call cubesyntax_message(seve%r,rname,mess)
  end subroutine cubesyntax_keyvalunit_list_real
  !
  subroutine cubesyntax_keyvalunit_list_dble(prog,name,value,code,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    class(keyvalunit_list_t), intent(in)    :: prog
    character(len=*),         intent(in)    :: name
    real(kind=dble_k),        intent(in)    :: value
    integer(kind=code_k),     intent(in)    :: code
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='KEYVALUNIT>LIST>DBLE'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call unit%get_from_code(code,error)
    if (error) return
    mess = '  '//cubetools_format_stdkey_boldval(&
         name,value*unit%user_per_prog,fdouble,list_l)
    mess = trim(mess)//'  '//trim(unit%name)
    call cubesyntax_message(seve%r,rname,mess)
  end subroutine cubesyntax_keyvalunit_list_dble
  !
  subroutine cubesyntax_keyvalunit_list_strg(prog,name,value,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    class(keyvalunit_list_t), intent(in)    :: prog
    character(len=*),         intent(in)    :: name
    character(len=*),         intent(in)    :: value
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='KEYVALUNIT>LIST>STRG'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    mess = '  '//cubetools_format_stdkey_boldval(name,value,list_l)
    call cubesyntax_message(seve%r,rname,mess)
  end subroutine cubesyntax_keyvalunit_list_strg
end module cubesyntax_keyvalunit_list_tool
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
