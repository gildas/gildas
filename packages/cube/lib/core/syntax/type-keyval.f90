!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubesyntax_keyval_types
  use cubesyntax_keyvalunit_list_tool
  use cubesyntax_keyvalunit_real_types
  use cubesyntax_keyvalunit_dble_types
  use cubesyntax_keyval_strg_types
  use cubesyntax_keyval_keyword_types
  !
  public :: keyval_strg_comm_t,keyval_strg_user_t,keyval_strg_prog_t
  public :: keyval_keyword_comm_t,keyval_keyword_user_t,keyval_keyword_prog_t
  !
  public :: keyvalunit_list_t
  public :: keyvalunit_real_comm_t,keyvalunit_real_user_t,keyvalunit_real_prog_t
  public :: keyvalunit_dble_comm_t,keyvalunit_dble_user_t,keyvalunit_dble_prog_t
  private
end module cubesyntax_keyval_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
