!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubesyntax_keyval_strg_types
  use cubetools_parameters
  use cubetools_structure
  use cubesyntax_value_strg_types
  use cubesyntax_messaging
  !
  public :: keyval_strg_comm_t,keyval_strg_user_t,keyval_strg_prog_t
  private
  !
  type keyval_strg_comm_t
     type(option_t), pointer :: key
     type(value_strg_comm_t) :: value
   contains
     procedure, public :: register => cubesyntax_keyval_strg_comm_register
     procedure, public :: parse    => cubesyntax_keyval_strg_comm_parse
  end type keyval_strg_comm_t
  !
  type keyval_strg_user_t
     logical               :: do = .false.
     character(len=argu_l) :: val = strg_star
   contains
     procedure, public :: toprog => cubesyntax_keyval_strg_user_toprog
     procedure, public :: list   => cubesyntax_keyval_strg_user_list
  end type keyval_strg_user_t
  !
  type keyval_strg_prog_t
   contains
     procedure, public :: list   => cubesyntax_keyval_strg_prog_list
  end type keyval_strg_prog_t
  !
contains
  !
  subroutine cubesyntax_keyval_strg_comm_register(comm,&
       keyname,keyabstract,valdefault,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(keyval_strg_comm_t), intent(inout) :: comm
    character(len=*),          intent(in)    :: keyname
    character(len=*),          intent(in)    :: keyabstract
    character(len=*),          intent(in)    :: valdefault
    logical,                   intent(inout) :: error
    !
    character(len=*), parameter :: rname='KEYVAL>CHAR>COMM>REGISTER'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         keyname,&
         'value',&
         keyabstract,&
         strg_id,&
         comm%key,&
         error)
    if (error) return
    call comm%value%register(&
         keyname,&
         valdefault,&
         error)
    if (error) return
  end subroutine cubesyntax_keyval_strg_comm_register
  !
  subroutine cubesyntax_keyval_strg_comm_parse(comm,line,user,error)
    use cubetools_structure
    !----------------------------------------------------------------------
    ! /KEY value
    !----------------------------------------------------------------------
    class(keyval_strg_comm_t), intent(in)    :: comm
    character(len=*),          intent(in)    :: line
    type(keyval_strg_user_t),  intent(out)   :: user ! => Initialized!
    logical,                   intent(inout) :: error
    !
    integer(kind=argu_k), parameter :: ival=1
    character(len=*), parameter :: rname='KEYVAL>CHAR>COMM>PARSE'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call comm%key%present(line,user%do,error)
    if (error) return
    if (user%do) then
       call cubetools_getarg(line,comm%key,ival,user%val,mandatory,error)
       if (error) return
    endif
  end subroutine cubesyntax_keyval_strg_comm_parse
  !
  !------------------------------------------------------------------------
  !
  subroutine cubesyntax_keyval_strg_user_toprog(user,comm,prog,error)
    use cubetools_user2prog
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(keyval_strg_user_t), intent(in)    :: user
    type(keyval_strg_comm_t),  intent(in)    :: comm
    character(len=*),          intent(inout) :: prog
    logical,                   intent(inout) :: error
    !
    character(len=*), parameter :: rname='KEYVAL>CHAR>USER>TOPROG'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_user2prog_resolve_star(user%val,comm%value%default,prog,error)
    if (error) return
  end subroutine cubesyntax_keyval_strg_user_toprog
  !
  subroutine cubesyntax_keyval_strg_user_list(user,error)
    !----------------------------------------------------------------------
    ! Mostly for debugging purpose
    !----------------------------------------------------------------------
    class(keyval_strg_user_t), intent(in)    :: user 
    logical,                   intent(inout) :: error
    !
!    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='KEYVAL>CHAR>USER>LIST'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
  end subroutine cubesyntax_keyval_strg_user_list
  !
  !------------------------------------------------------------------------
  !
  subroutine cubesyntax_keyval_strg_prog_list(prog,comm,value,error)
    use cubesyntax_keyvalunit_list_tool
    !-------------------------------------------------------------------
    ! List the information in a user friendly way
    !-------------------------------------------------------------------
    class(keyval_strg_prog_t), intent(in)    :: prog
    type(keyval_strg_comm_t),  intent(in)    :: comm
    character(len=*),          intent(in)    :: value
    logical,                   intent(inout) :: error
    !
    character(len=*), parameter :: rname='KEYVAL>CHAR>PROG>LIST'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call keyvalunit%list(comm%key%name,value,error)
    if (error) return
  end subroutine cubesyntax_keyval_strg_prog_list
end module cubesyntax_keyval_strg_types
! 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
