!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_standard_arg
  use cubetools_parameters
  use cubetools_primitive_arg
  use cubetools_structure_main
  !
  public :: standard_arg_t
  private
  !
  type, extends(primitive_arg_t) :: standard_arg_t
     ! No element to add
  contains
     procedure :: register => cubetools_register_standard_arg
  end type standard_arg_t
  !
contains
  !
  subroutine cubetools_register_standard_arg(template,name,abstract,help,mandat,error)
    !----------------------------------------------------------------------
    ! Register an argument of exact type standard_arg_t in the global 'pack'
    !----------------------------------------------------------------------
    class(standard_arg_t), intent(in)    :: template
    character(len=*),      intent(in)    :: name
    character(len=*),      intent(in)    :: abstract
    character(len=*),      intent(in)    :: help
    integer(kind=code_k),  intent(in)    :: mandat
    logical,               intent(inout) :: error
    !
    class(primitive_arg_t), pointer :: parg
    !
    call cubetools_register_primitive_arg(template,name,abstract,help,mandat,parg,error)
    if (error) return
  end subroutine cubetools_register_standard_arg

end module cubetools_standard_arg
