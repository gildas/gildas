!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_unit_types
  use cubetools_parameters
  use cubetools_unit
  use cubetools_structure
  use cubesyntax_messaging
  !
  public :: unit_comm_t,unit_user_t
  private
  !
  type unit_comm_t
     type(unit_arg_t), pointer :: arg
     integer(kind=code_k)      :: code
   contains
     procedure, public :: register => cubetools_unit_comm_register
     procedure, public :: parse    => cubetools_unit_comm_parse
  end type unit_comm_t
  !
contains
  !
  subroutine cubetools_unit_comm_register(comm,name,code,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(unit_comm_t),   intent(inout) :: comm
    character(len=*),     intent(in)    :: name
    integer(kind=code_k), intent(in)    :: code
    logical,              intent(inout) :: error
    !
    type(unit_arg_t) :: unitarg
    character(len=*), parameter :: rname='UNIT>COMM>REGISTER'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    comm%code = code
    call unitarg%register(&
         'UNIT',&
         trim(name)//' unit',&
         strg_id,&
         code_arg_optional,&
         code,&
         comm%arg,&
         error)
    if (error) return
  end subroutine cubetools_unit_comm_register
  !
  subroutine cubetools_unit_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! [unit]
    !----------------------------------------------------------------------
    class(unit_comm_t), intent(in)    :: comm
    character(len=*),   intent(in)    :: line
    type(unit_user_t),  intent(out)   :: user
    logical,            intent(inout) :: error
    !
!!$    logical :: do
!!$    character(len=unit_l) :: unitname
    character(len=*), parameter :: rname='UNIT>COMM>PARSE'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    !***JP: Here is what I would like to do
!!$ call comm%present(line,do,error)
!!$ if (error) return
!!$ if (do) then
!!$    call comm%get(line,unitname,optional,error)
!!$    if (error) return
!!$    call user%unit%get_from_name(unitname,error)
!!$    if (error) return
!!$ else
!!$    call user%unit%get_from_code(comm%code,error)
!!$    if (error) return
!!$ endif
    !***JP: As I don't know how to do this, I return an error for the moment
    call cubesyntax_message(seve%e,rname,'Do not know yet how to parse a unit argument')
    error = .true.
    return
  end subroutine cubetools_unit_comm_parse  
end module cubetools_unit_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
