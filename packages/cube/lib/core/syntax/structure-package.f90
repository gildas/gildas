!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_package
  use cubetools_parameters
  use cubesyntax_messaging
  use cubetools_terminal_tool
  use cubetools_list
  use cubetools_language
  !
  public :: lang_k
  public :: package_t
  private
  !
  integer(kind=4), parameter :: lang_k =  4 ! [integer] Languages
  integer(kind=4), parameter :: pack_l = 16
  integer(kind=4), parameter :: help_l = 80
  !
  type package_t
     character(len=pack_l) :: name = strg_unk
     character(len=help_l) :: help = strg_emp
     type(tools_list_t)    :: lang
   contains
     ! General
     procedure :: init             => cubetools_package_init
     procedure :: final            => cubetools_package_final
     procedure :: free             => cubetools_package_free
     !
     ! Registering
     procedure :: put              => cubetools_package_put
     !
     ! Execution
     procedure :: get_language_id  => cubetools_package_get_language_id
     !
     ! Print and Help
     procedure :: abstract         => cubetools_package_abstract
     procedure :: list             => cubetools_package_list
     procedure :: help_iterate     => cubetools_package_help_iterate
  end type package_t
  !
contains
  !
  !---General------------------------------------------------------------
  !
  subroutine cubetools_package_init(pack,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(package_t), intent(out)   :: pack
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='PACKAGE>INIT'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
  end subroutine cubetools_package_init
  !
  subroutine cubetools_package_final(pack,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(package_t), intent(inout) :: pack
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='PACKAGE>FINAL'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_package_free(pack,error)
    if (error) return
  end subroutine cubetools_package_final
  !
  subroutine cubetools_package_free(pack,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(package_t), intent(inout) :: pack
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='PACKAGE>FREE'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call pack%lang%free(error)
    if (error) return
  end subroutine cubetools_package_free
  !
  !---Registering--------------------------------------------------------
  !
  subroutine cubetools_package_put(pack,name,help,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(package_t),     intent(inout) :: pack
    character(len=*),     intent(in)    :: name
    character(len=*),     intent(in)    :: help
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='PACKAGE>PUT'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call pack%final(error)
    if (error) return
    call pack%init(error)
    if (error) return
    pack%name = name
    pack%help = help
    pack%lang%n = 0
  end subroutine cubetools_package_put
  !
  !---Execution----------------------------------------------------------
  !
  subroutine cubetools_package_get_language_id(pack,langname,langid,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(package_t),     intent(in)    :: pack
    character(len=*),     intent(in)    :: langname
    integer(kind=lang_k), intent(out)   :: langid
    logical,              intent(inout) :: error
    !
    integer(kind=lang_k) :: ilang
    type(language_t), pointer :: lang
    character(len=*), parameter :: rname='PACKAGE>GET>LANGUAGE>ID'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    do ilang = 1,pack%lang%n
      lang => cubetools_language_ptr(pack%lang%list(ilang)%p,error)
      if (error) return
      if (lang%name.eq.langname) then
        langid = ilang
        return
      endif
    enddo ! ilang
    !
    ! Not found
    langid = 0
    call cubesyntax_message(seve%e,rname,'Unknown language '//trim(langname)//' in package '//trim(pack%name))
    error = .true.
    return
  end subroutine cubetools_package_get_language_id
  !
  !---Print-and-Help-------------------------------------------------------
  !
  subroutine cubetools_package_help_iterate(pack,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(package_t), intent(in)    :: pack
    logical,          intent(inout) :: error
    !
    integer(kind=lang_k) :: ilang
    type(language_t), pointer :: lang
    character(len=*), parameter :: rname='PACKAGE>HELP>ITERATE'
    !
    call cubesyntax_message(syntaxseve%help,rname,'\chapter{'//trim(pack%name)//' On-Line Help}')
    call cubesyntax_message(syntaxseve%help,rname,'\label{sec:'//trim(pack%name)//':package}')
    call cubesyntax_message(syntaxseve%help,rname,'\section{List of '//trim(pack%name)//' languages}')
    call cubesyntax_message(syntaxseve%help,rname,'\label{sec:'//trim(pack%name)//':languages}')
    call cubesyntax_message(syntaxseve%help,rname,'\index{Language list}')
    !
    call cubesyntax_message(syntaxseve%help,rname,'\begin{PlusVerbatim}')
    call pack%list(error)
    if (error) return
    call cubesyntax_message(syntaxseve%help,rname,'\end{PlusVerbatim}')
    call cubesyntax_message(syntaxseve%help,rname,'\newpage{}')
    !
    do ilang=1,pack%lang%n
      lang => cubetools_language_ptr(pack%lang%list(ilang)%p,error)
      if (error) return
      call lang%help_iterate(error)
      if (error) return
    enddo
  end subroutine cubetools_package_help_iterate
  !
  subroutine cubetools_package_abstract(pack,error)
    use cubetools_format
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(package_t), intent(inout) :: pack
    logical,          intent(inout) :: error
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='PACKAGE>ABSTRACT'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    mess = '  '//cubetools_format_stdkey_boldval(pack%name,pack%help,terminal%width())
    call cubesyntax_message(seve%r,rname,mess)
  end subroutine cubetools_package_abstract
  !
  subroutine cubetools_package_list(pack,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(package_t), intent(in)    :: pack
    logical,          intent(inout) :: error
    !
    integer(kind=lang_k) :: ilang
    type(language_t), pointer :: lang
    character(len=*), parameter :: rname='PACKAGE>LIST'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    do ilang = 1,pack%lang%n
      lang => cubetools_language_ptr(pack%lang%list(ilang)%p,error)
      if (error) return
      call lang%abstract(error)
      if (error) return
    enddo ! ilang
    call cubesyntax_message(syntaxseve%help,rname,blankstr)
  end subroutine cubetools_package_list
  !
end module cubetools_package
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
