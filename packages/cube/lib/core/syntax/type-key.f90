!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubesyntax_key_types
  use cubetools_parameters
  use cubetools_structure
  use cubesyntax_messaging
  !
  public :: key_comm_t,key_user_t,key_prog_t
  private
  !
  type :: key_comm_t
     type(option_t), pointer :: key
   contains
     procedure, public :: register => key_comm_register
     procedure, public :: parse    => key_comm_parse
  end type key_comm_t
  !
  type :: key_user_t
     logical :: present = .false.
   contains
     procedure, public :: toprog => key_user_toprog
  end type key_user_t
  !
  type :: key_prog_t
     logical :: act = .false.
  end type key_prog_t
  !
contains
  !
  subroutine key_comm_register(comm,name,abstract,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(key_comm_t), intent(inout) :: comm
    character(len=*),  intent(in)    :: name
    character(len=*),  intent(in)    :: abstract
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='KEY>COMM>REGISTER'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         name,'',&
         abstract,strg_id,&
         comm%key,error)
    if (error) return
  end subroutine key_comm_register
  !
  subroutine key_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(key_comm_t), intent(in)    :: comm
    character(len=*),  intent(in)    :: line
    type(key_user_t),  intent(inout) :: user
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='KEY>COMM>PARSE'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call comm%key%present(line,user%present,error)
    if (error) return
  end subroutine key_comm_parse
  !
  !---------------------------------------------------------------------
  !
  subroutine key_user_toprog(user,comm,prog,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(key_user_t), intent(in)    :: user
    type(key_comm_t),  intent(in)    :: comm
    type(key_prog_t),  intent(inout) :: prog
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='KEY>USER>TOPROG'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    prog%act = user%present
  end subroutine key_user_toprog
end module cubesyntax_key_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
