!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubesyntax_value_real_types
  use cubetools_parameters
  use cubetools_structure
  use cubesyntax_messaging
  !
  public :: value_real_comm_t
  private
  !
  type value_real_comm_t
     type(standard_arg_t) :: value
     real(kind=real_k)    :: default
   contains
     procedure, public :: register => cubesyntax_value_real_comm_register
  end type value_real_comm_t
  !
contains
  !
  subroutine cubesyntax_value_real_comm_register(comm,name,default,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(value_real_comm_t), intent(inout) :: comm
    character(len=*),         intent(in)    :: name
    real(kind=real_k),        intent(in)    :: default
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='VALUE>REAL>COMM>REGISTER'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    comm%default = default
    call comm%value%register(&
         'VALUE',&
         trim(name)//' value',&
         'Default to ???',&
         code_arg_mandatory,&
         error)
    if (error) return
  end subroutine cubesyntax_value_real_comm_register
end module cubesyntax_value_real_types
! 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubesyntax_value_dble_types
  use cubetools_parameters
  use cubetools_structure
  use cubesyntax_messaging
  !
  public :: value_dble_comm_t
  private
  !
  type value_dble_comm_t
     type(standard_arg_t) :: value
     real(kind=dble_k)    :: default
   contains
     procedure, public :: register => cubesyntax_value_dble_comm_register
  end type value_dble_comm_t
  !
contains
  !
  subroutine cubesyntax_value_dble_comm_register(comm,name,default,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(value_dble_comm_t), intent(inout) :: comm
    character(len=*),         intent(in)    :: name
    real(kind=dble_k),        intent(in)    :: default
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='VALUE>DBLE>COMM>REGISTER'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    comm%default = default
    call comm%value%register(&
         'VALUE',&
         trim(name)//' value',&
         'Default to ???',&
         code_arg_mandatory,&
         error)
    if (error) return
  end subroutine cubesyntax_value_dble_comm_register
end module cubesyntax_value_dble_types
! 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubesyntax_value_strg_types
  use cubetools_parameters
  use cubetools_structure
  use cubesyntax_messaging
  !
  public :: value_strg_comm_t
  private
  !
  type value_strg_comm_t
     type(standard_arg_t)  :: value
     character(len=argu_l) :: default
   contains
     procedure, public :: register => cubesyntax_value_strg_comm_register
  end type value_strg_comm_t
  !
contains
  !
  subroutine cubesyntax_value_strg_comm_register(comm,name,default,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(value_strg_comm_t), intent(inout) :: comm
    character(len=*),         intent(in)    :: name
    character(len=*),         intent(in)    :: default
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='VALUE>STRG>COMM>REGISTER'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    comm%default = default
    call comm%value%register(&
         'VALUE',&
         trim(name)//' value',&
         'Default to '//trim(default),&
         code_arg_mandatory,&
         error)
    if (error) return
  end subroutine cubesyntax_value_strg_comm_register
end module cubesyntax_value_strg_types
! 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubesyntax_value_keywordlist_types
  use cubetools_parameters
  use cubetools_structure
  use cubesyntax_messaging
  !
  public :: value_keywordlist_comm_t
  private
  !
  type value_keywordlist_comm_t
     type(keywordlist_comm_t), pointer :: value
     character(len=argu_l)             :: default
   contains
     procedure, public :: register => cubesyntax_value_keywordlist_comm_register
  end type value_keywordlist_comm_t
  !
contains
  !
  subroutine cubesyntax_value_keywordlist_comm_register(comm,name,default, &
    list,flexibl,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(value_keywordlist_comm_t), intent(inout) :: comm
    character(len=*),                intent(in)    :: name
    character(len=*),                intent(in)    :: default
    character(len=*),                intent(in)    :: list(:)
    logical,                         intent(in)    :: flexibl
    logical,                         intent(inout) :: error
    !
    type(keywordlist_comm_t) :: tool  ! Registering tool
    character(len=*), parameter :: rname='VALUE>KEYWORDLIST>COMM>REGISTER'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    comm%default = default
    call tool%register(&
         'KEYWORD',&
         trim(name)//' value',&
         'Default to '//trim(default),&
         code_arg_mandatory,&
         list,&
         flexibl,&
         comm%value,&  ! Pointer to the registered keywordlist_comm_t
         error)
    if (error) return
  end subroutine cubesyntax_value_keywordlist_comm_register
end module cubesyntax_value_keywordlist_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
