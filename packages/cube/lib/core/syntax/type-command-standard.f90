!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_standard_comm
  use cubetools_command
  use cubetools_primitive_opt
  use cubetools_standard_opt
  use cubetools_structure_main
  !
  public :: command_t
  public :: cubetools_register_command,cubetools_command_ptr
  private
  !
contains
  !
  subroutine cubetools_register_command(name,syntax,abstract,help,run,popt,error,allowopt)
    !----------------------------------------------------------------------
    ! Register a command of exact type command_t in the global 'pack'
    !----------------------------------------------------------------------
    character(len=*),  intent(in)    :: name
    character(len=*),  intent(in)    :: syntax
    character(len=*),  intent(in)    :: abstract
    character(len=*),  intent(in)    :: help
    external                         :: run
    type(option_t),    pointer       :: popt  ! Return pointer to registered 'option'
    logical,           intent(inout) :: error
    logical, optional, intent(in)    :: allowopt
    !
    type(command_t) :: template
    type(option_t) :: otemplate
    class(primitive_opt_t), pointer :: actualopt
    !
    call cubetools_register_primitive_comm(template,otemplate,  &
      name,syntax,abstract,help,run,actualopt,error,allowopt)
    if (error)  return
    popt => cubetools_option_ptr(actualopt,error)
    if (error)  return
    popt%check          => cubetools_command_check_execute
    popt%build_syntax   => cubetools_command_build_syntax
    popt%print_abstract => cubetools_command_print_abstract
  end subroutine cubetools_register_command
  !
  subroutine cubetools_command_check_execute(opt,command,line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(primitive_opt_t), intent(in)    :: opt
    character(len=*),       intent(in)    :: command  ! Command name (for proper feedback)
    character(len=*),       intent(in)    :: line
    logical,                intent(inout) :: error
    !
    call cubetools_option_check_execute(opt,command,line,error)
    if (error)  return
  end subroutine cubetools_command_check_execute
  !
  subroutine cubetools_command_build_syntax(opt,syntax,error)
    use cubetools_format
    !-------------------------------------------------------------------
    !  Build the string
    !    COMMAND Arg1 ... ArgN
    !  according to opt%name and opt%syntax
    !-------------------------------------------------------------------
    class(primitive_opt_t), intent(in)    :: opt
    character(len=*),       intent(out)   :: syntax
    logical,                intent(inout) :: error
    !
    syntax = trim(cubetools_format_bold(opt%name))// &
             ' '// &
             opt%syntax
  end subroutine cubetools_command_build_syntax
  !
  subroutine cubetools_command_print_abstract(opt,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(primitive_opt_t), intent(in)    :: opt
    logical,                intent(inout) :: error
    !
    call cubetools_option_print_abstract(opt,error)
    if (error)  return
  end subroutine cubetools_command_print_abstract
end module cubetools_standard_comm
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
