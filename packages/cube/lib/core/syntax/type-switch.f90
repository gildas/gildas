!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_switch_types
  use cubetools_parameters
  use cubetools_structure
  use cubesyntax_messaging
  !
  public :: switch_comm_t,switch_user_t,switch_prog_t
  private
  !
  character(len=*), parameter :: onoff(2) = ['ON ','OFF']
  !
  type :: switch_comm_t
     type(option_t),      pointer :: key
     type(keywordlist_comm_t), pointer :: value
     character(len=argu_l) :: default = strg_unk ! Is the switch ON or OFF when the key is missing?
   contains
     procedure, public :: register => cubetools_switch_comm_register
     procedure, public :: parse    => cubetools_switch_comm_parse
  end type switch_comm_t
  !
  type :: switch_user_t
     logical :: act = .false. ! Act or list?
     character(len=argu_l) :: value = strg_unk
   contains
     procedure, public :: toprog => cubetools_switch_user_toprog
  end type switch_user_t
  !
  type :: switch_prog_t
     logical :: act = .false. ! Act or list?
     character(len=16) :: action = strg_unk
     logical :: enabled = .false.
   contains
     procedure, public :: init => cubetools_switch_prog_init
     procedure, public :: list => cubetools_switch_prog_list
  end type switch_prog_t
  !
contains
  !
  subroutine cubetools_switch_comm_register(comm,name,action,default,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(switch_comm_t), intent(inout) :: comm
    character(len=*),     intent(in)    :: name
    character(len=*),     intent(in)    :: action
    character(len=*),     intent(in)    :: default
    logical,              intent(inout) :: error
    !
    type(keywordlist_comm_t) :: keyarg
    character(len=*), parameter :: rname='SWITCH>KEY>REGISTER'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         name,'ON|OFF',&
         'Switch '//trim(action)// ' on or off',&
         'Default is '//trim(default)//'.',&
         comm%key,error)
    if (error) return
    call keyarg%register(&
         'ONOFF', &
         'Switch action ON or OFF',&
         strg_id,&
         code_arg_mandatory,&
         onoff,&
         .not.flexible,&
         comm%value,&
         error)
    if (error) return
    !
    comm%default = default
  end subroutine cubetools_switch_comm_register
  !
  subroutine cubetools_switch_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(switch_comm_t), intent(in)    :: comm
    character(len=*),     intent(in)    :: line
    type(switch_user_t),  intent(inout) :: user
    logical,              intent(inout) :: error
    !
    integer(kind=4), parameter :: iarg=1
    character(len=*), parameter :: rname='SWITCH>KEY>PARSE'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    call comm%key%present(line,user%act,error)
    if (error) return
    if (user%act) then
       call cubetools_getarg(line,comm%key,iarg,user%value,mandatory,error)
       if (error) return
    else
       user%value = comm%default
    endif
  end subroutine cubetools_switch_comm_parse
  !
  !---------------------------------------------------------------------
  !
  subroutine cubetools_switch_user_toprog(user,comm,prog,error)
    use cubetools_disambiguate
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(switch_user_t), intent(in)    :: user
    type(switch_comm_t),  intent(in)    :: comm
    type(switch_prog_t),  intent(inout) :: prog
    logical,              intent(inout) :: error
    !
    integer(kind=code_k) :: code
    character(len=argu_l) :: value
    character(len=*), parameter :: rname='SWITCH>USER>TOPROG'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    prog%act = user%act
    if (user%act) then
       ! *** JP Here I would like to have the name of switch from comm
       ! prog%action = comm%name
       call cubetools_disambiguate_strict(user%value,onoff,code,value,error)
       if (error) return
       prog%enabled = value.eq.'ON'
    else
       ! Do nothing
    endif
  end subroutine cubetools_switch_user_toprog
  !
  !---------------------------------------------------------------------
  !
  subroutine cubetools_switch_prog_init(prog,comm,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(switch_prog_t), intent(inout) :: prog
    type(switch_comm_t),  intent(in)    :: comm
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='SWITCH>PROG>INIT'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    prog%enabled = comm%default.eq.'ON'
  end subroutine cubetools_switch_prog_init
  !
  subroutine cubetools_switch_prog_list(prog,error)
    use cubetools_format
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(switch_prog_t), intent(in)    :: prog
    logical,              intent(inout) :: error
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='SWITCH>PROG>LIST'
    !
    call cubesyntax_message(syntaxseve%trace,rname,'Welcome')
    !
    write(mess,'(a,l)') '    '//trim(prog%action)//': ',prog%enabled
    call cubesyntax_message(seve%r,rname,mess)
  end subroutine cubetools_switch_prog_list
end module cubetools_switch_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
