!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubedag_dagcomm
  use cubetools_structure
  use cubetools_keywordlist_types
  !
  public :: cubedag_dag_register
  private
  !
  type :: dag_comm_t
     type(option_t),      pointer :: dag
     type(keywordlist_comm_t), pointer :: action_arg
  end type dag_comm_t
  type(dag_comm_t) :: comm
  !
contains
  !
  subroutine cubedag_dag_register(error)
    use cubetools_parameters
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    logical, intent(out) :: error
    ! Local
    type(keywordlist_comm_t)  :: keyarg
    type(standard_arg_t) :: stdarg
    integer, parameter :: naction = 2
    character(len=*), parameter :: actions(naction) =  &
         (/ 'PLIST  ','DIGRAPH' /)
    character(len=*), parameter :: rname='DAG>REGISTER'
    !
    call cubetools_register_command(&
         'DAG','action',&
         'For debugging purposes',&
         '* DIGRAPH: produce a .dot file for graphical representation of the DAG'//strg_cr//  &
         '    CUBE> dag digraph 1 toto.dot'//strg_cr//  &
         '    CUBE> system "dot -Tpdf -o toto.pdf toto.dot"'//strg_cr//  &
         '  Modes 2 and 3 result in more verbose nodes.'//strg_cr//  &
         '* PLIST: list the parents of a node given its ID'//strg_cr//  &
         '    CUBE> dag plist 3'//strg_cr,  &
         cubedag_dag_command,&
         comm%dag,error)
    if (error) return
    call keyarg%register( &
         'Action',  &
         'Action to be executed', &
         strg_id,&
         code_arg_mandatory, &
         actions,  &
         .not.flexible,  &
         comm%action_arg,  &
         error)
    if (error) return
    call stdarg%register( &
         'Argument 1',  &
         'First argument to action', &
         strg_id,&
         code_arg_mandatory,error)
    if (error) return
    call stdarg%register( &
         'Argument 2',  &
         'Second argument to action', &
         strg_id,&
         code_arg_optional,error)
    if (error) return
  end subroutine cubedag_dag_register
  !
  subroutine cubedag_dag_command(line,error)
    use cubedag_parameters
    use cubedag_node
    use cubedag_messaging
    use cubedag_repository
    use cubedag_list
    use cubedag_digraph
    !----------------------------------------------------------------------
    ! @ public
    ! DAG DIGRAPH|PLIST
    !----------------------------------------------------------------------
    character(len=*), intent(in)  :: line
    logical,          intent(out) :: error
    ! Local
    character(len=*), parameter :: rname='DAG'
    character(len=7) :: chain,key
    integer(kind=4) :: iaction
    character(len=filename_length) :: path
    integer(kind=iden_l) :: id
    class(cubedag_node_object_t), pointer :: object
    !
    call cubetools_getarg(line,comm%dag,1,chain,mandatory,error)
    if (error) return
    call cubetools_keywordlist_user2prog(comm%action_arg,chain,iaction,key,error)
    if (error)  return
    select case (key)
    case ('PLIST')
       call cubetools_getarg(line,comm%dag,2,id,mandatory,error)
       if (error)  return
       call cubedag_dag_get_object(id,object,error)
       if (error)  return
       call cubedag_parentwalker_reset(object,cubedag_walker_null,error)
       if (error)  return
       do while (cubedag_parentwalker_next(object))
          print *,'Found parent: ',object%node%id
       enddo
    case ('DIGRAPH')
       call cubetools_getarg(line,comm%dag,2,iaction,mandatory,error)
       if (error)  return
       call cubetools_getarg(line,comm%dag,3,path,mandatory,error)
       if (error)  return
       call cubedag_digraph_create(path,iaction,error)
       if (error)  return
    case default
       call cubedag_message(seve%e,rname,'Unknown keyword: '//trim(key))
       error = .true.
       return
    end select
    !
  end subroutine cubedag_dag_command
end module cubedag_dagcomm
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
