module cubedag_iterator
  !---------------------------------------------------------------------
  ! Support for "flat iterator" of the DAG. See also walker.f90 for
  ! graph (e.g. through parents or children) iteration.
  !---------------------------------------------------------------------
  use cubedag_parameters
  use cubedag_node_type
  use cubedag_dag

  integer(kind=entr_k) :: iterator

  public :: cubedag_iterator_init,cubedag_iterator_iterate

contains

  subroutine cubedag_iterator_init(error)
    logical, intent(inout) :: error
    iterator = 0
  end subroutine cubedag_iterator_init
  !
  function cubedag_iterator_iterate(node)
    !-------------------------------------------------------------------
    ! IX iterator: return .true. and pointer to next node as along as
    ! there are nodes to be iterated
    !-------------------------------------------------------------------
    logical :: cubedag_iterator_iterate
    class(cubedag_node_object_t), pointer :: node
    !
    logical :: error
    !
    iterator = iterator+1
    cubedag_iterator_iterate = iterator.le.ix%n
    !
    if (.not.cubedag_iterator_iterate) then
      node => null()
    else
      error = .false.
      node => cubedag_node_ptr(ix%list(iterator)%p,error)
      if (error)  return
    endif
    !
  end function cubedag_iterator_iterate

end module cubedag_iterator
