module cubedag_node
  use gkernel_interfaces
  use cubetools_list
  use cubedag_messaging
  use cubedag_parameters
  use cubedag_flag
  use cubedag_link_type
  use cubedag_node_type
  use cubedag_type
  !---------------------------------------------------------------------
  ! Support module for the cubedag_node_object_t methods
  !---------------------------------------------------------------------

  integer(kind=code_k) :: code_type_node=0

  public :: code_type_node
  public :: cubedag_node_allocate,cubedag_node_deallocate
  public :: cubedag_node_destroy,cubedag_node_remove
  public :: cubedag_node_links,cubedag_node_links_twins,cubedag_node_link_parents
  public :: cubedag_node_history
  public :: cubedag_node_set_origin,cubedag_node_set_family,  &
            cubedag_node_set_flags,cubedag_node_set_header, &
            cubedag_node_set_sicvar,cubedag_node_unset_sicvar
  public :: cubedag_node_get_header
  private

contains
  !
  subroutine cubedag_node_allocate(object,error)
    !-------------------------------------------------------------------
    ! Allocate a 'cubedag_node_object_t' in memory
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), pointer       :: object
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='NODE>ALLOCATE'
    integer(kind=4) :: ier
    !
    allocate(cubedag_node_object_t::object,stat=ier)
    if (failed_allocate(rname,'object',ier,error)) return
    !
    ! Set up the list-type method
    object%ltype    => cubedag_node_ltype
    object%memsize  => cubedag_node_memsize
    object%disksize => cubedag_node_disksize
    object%datasize => cubedag_node_datasize
  end subroutine cubedag_node_allocate
  !
  subroutine cubedag_node_final(object,error)
    !-------------------------------------------------------------------
    ! Free the contents of a 'cubedag_node_object_t'
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), pointer       :: object
    logical,                      intent(inout) :: error
    !
    call object%node%flag%free(error)
    if (error)  continue
    call object%node%parents%final(error)
    if (error)  continue
    call object%node%children%final(error)
    if (error)  continue
    call object%node%twins%final(error)
    if (error)  continue
  end subroutine cubedag_node_final
  !
  subroutine cubedag_node_deallocate(object,error)
    !-------------------------------------------------------------------
    ! Deallocate a 'cubedag_node_object_t' in memory
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), pointer       :: object
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='NODE>DEALLOCATE'
    !
    if (.not.associated(object)) then
      call cubedag_message(seve%e,rname,'Internal error: object is not allocated')
      error = .true.
      return
    endif
    !
    select type (object)
    type is (cubedag_node_object_t)
      continue
    class default
      call cubedag_message(seve%e,rname,'Internal error: object has wrong type')
      error = .true.
      return
    end select
    !
    deallocate(object)  ! NB: deallocation is polymorphic
  end subroutine cubedag_node_deallocate

  function cubedag_node_ltype(obj)
    character(len=2) :: cubedag_node_ltype
    class(cubedag_node_object_t), intent(in) :: obj
    cubedag_node_ltype = '<>'
  end function cubedag_node_ltype

  function cubedag_node_memsize(obj)
    integer(kind=size_length) :: cubedag_node_memsize
    class(cubedag_node_object_t), intent(in) :: obj
    cubedag_node_memsize = 0
  end function cubedag_node_memsize

  function cubedag_node_disksize(obj)
    integer(kind=size_length) :: cubedag_node_disksize
    class(cubedag_node_object_t), intent(in) :: obj
    cubedag_node_disksize = 0
  end function cubedag_node_disksize

  function cubedag_node_datasize(obj)
    integer(kind=size_length) :: cubedag_node_datasize
    class(cubedag_node_object_t), intent(in) :: obj
    cubedag_node_datasize = 0
  end function cubedag_node_datasize

  subroutine cubedag_node_destroy(object,error)
    !-------------------------------------------------------------------
    ! Brute force destruction of a node object. Do not worry about
    ! family links between nodes.
    ! The object is a pointer which is expected to be deallocated and
    ! nullified in return.
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), pointer       :: object
    logical,                      intent(inout) :: error
    !
    integer(kind=4) :: ivar
    character(len=*), parameter :: rname='NODE>DESTROY'
    !
    do ivar=1,object%node%nsicvar
      ! As of today, on the command OLDLOAD creates SIC variables
      ! pointing to the actual cube data. This is likely to disappear
      ! when OLDLOAD is rewritten.
      call cubedag_message(seve%w,rname,'Removing variable '//  &
        trim(object%node%sicvar(ivar))//' pointing to destroyed node')
      call sic_delvariable(object%node%sicvar(ivar),.false.,error)
      if (error)  error = .false.
    enddo
    !
    ! Clean the object contents (common part). The polymorphic part
    ! must be cleaned by the specific deallocation subroutine invoked
    ! below
    call cubedag_node_final(object,error)
    if (error)  continue
    !
    ! Deallocate the object in memory
    call cubedag_type_deallocate(object,error)
    if (error)  return
  end subroutine cubedag_node_destroy

  subroutine cubedag_node_remove(object,error)
    use cubedag_tuple
    !-------------------------------------------------------------------
    ! Proper removal of a node object, taking care of the family links
    ! between the other nodes
    ! The object is a pointer which is expected to be deallocated and
    ! nullified in return.
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), pointer       :: object
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='REMOVE'
    integer(kind=entr_k) :: irel
    class(cubedag_node_object_t), pointer :: relative
    class(tools_object_t), pointer :: tot
    !
    if (object%node%children%n.gt.0) then
      call cubedag_message(seve%e,rname,'Node has one or more children')
      error = .true.
      return
    endif
    !
    ! Clean twins
    do irel=1,object%node%twins%n
      relative => cubedag_node_ptr(object%node%twins%list(irel)%p,error)
      if (error)  return
      tot => object
      call relative%node%twins%unlink(tot,error)
      if (error)  return
    enddo
    ! Clean parents
    do irel=1,object%node%parents%n
      relative => cubedag_node_ptr(object%node%parents%list(irel)%p,error)
      if (error)  return
      tot => object
      call relative%node%children%unlink(tot,error)
      if (error)  return
    enddo
    !
    ! Clean files on disk, only for non-raw cubes!
    if (object%node%origin.ne.code_origin_imported) then
      call cubedag_tuple_rmfiles(object%node%tuple,error)
      if (error)  return
    endif
    !
    ! Now destroy it
    call cubedag_node_destroy(object,error)
    if (error)  return
  end subroutine cubedag_node_remove

  subroutine cubedag_node_links(parents,children,hid,error)
    !-------------------------------------------------------------------
    ! Insert all the links between parents, children, twins, and history
    ! ---
    ! Beware this subroutine can be called in an error recovery context
    !-------------------------------------------------------------------
    type(cubedag_link_t), intent(in)    :: parents
    type(cubedag_link_t), intent(in)    :: children
    integer(kind=entr_k), intent(in)    :: hid
    logical,              intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='NODE>LINKS'
    !
    if (error)  return  ! Error recovery: unclear
    !
    ! Sanity check for cubes opened in UPDATE mode
    ! ZZZ Obviously this should be moved in cube/adm
    ! do ip=1,np
    !   ient = cubedag_dag_entrynum(pids(ip),error)
    !   if (error)  return
    !   if (ix%cube(ient)%desc%action.eq.code_update) then
    !     if (ix%children(ient)%n.gt.0) then
    !       call cubedag_message(seve%w,rname,  &
    !         'Updated tuple has children which should be updated too')
    !     endif
    !   endif
    ! enddo
    !
    if (children%n.eq.0) then
      ! This can happen if a cube was opened in update mode, or
      ! for commands with input-only cubes
      continue
    else
      call cubedag_node_links_parents(children,parents,error)
      if (error)  return
      call cubedag_node_links_twins(children,error)
      if (error)  return
    endif
    !
    ! History
    call cubedag_node_history(children,hid,error)
    if (error)  return
  end subroutine cubedag_node_links
  !
  subroutine cubedag_node_links_parents(children,parents,error)
    !-------------------------------------------------------------------
    ! For each child in a list of children:
    !  - attach (overwrite) its list of parents,
    !  - set (append) a backpointer of this child to its parents.
    !-------------------------------------------------------------------
    type(cubedag_link_t), intent(in)    :: children
    type(cubedag_link_t), intent(in)    :: parents
    logical,              intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='NODE>LINKS>PARENTS'
    integer(kind=entr_k) :: ient
    class(cubedag_node_object_t), pointer :: dno
    !
    if (parents%n.eq.0) then
      ! Is this allowed, e.g. tuples created from scratch?
      call cubedag_message(seve%w,rname,'No parents for new tuple(s)')
      return
    endif
    !
    do ient=1,children%n
      dno => cubedag_node_ptr(children%list(ient)%p,error)
      if (error)  return
      call cubedag_node_link_parents(dno,parents,error)
      if (error)  exit
    enddo
  end subroutine cubedag_node_links_parents
  !
  subroutine cubedag_node_links_twins(children,error)
    !-------------------------------------------------------------------
    ! For each child in a list of children:
    !  - attach (overwrite) its list of twins.
    !-------------------------------------------------------------------
    type(cubedag_link_t), intent(in)    :: children
    logical,              intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='NODE>LINKS>TWINS'
    integer(kind=entr_k) :: ient,ic,it
    type(cubedag_link_t) :: twins
    class(cubedag_node_object_t), pointer :: dno
    !
    call twins%reallocate(children%n-1,error)
    if (error)  return
    twins%n = children%n-1
    do ient=1,children%n
      do ic=1,ient-1
        it = ic
        twins%list(it)%p => children%list(ic)%p
      enddo
      do ic=ient+1,children%n
        it = ic-1
        twins%list(it)%p => children%list(ic)%p
      enddo
      dno => cubedag_node_ptr(children%list(ient)%p,error)
      if (error)  return
      call cubedag_node_link_twins(dno,twins,error)
      if (error)  exit
    enddo
    call twins%final(error)
    if (error)  return
  end subroutine cubedag_node_links_twins
  !
  subroutine cubedag_node_link_parents(child,parents,error)
    !-------------------------------------------------------------------
    ! For the given 'child':
    !  - attach (overwrite) its list of parents,
    !  - set (append) a backpointer of this child to its parents.
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), pointer       :: child
    type(cubedag_link_t),         intent(in)    :: parents
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='NODE>LINK>PARENTS'
    !
    ! Insert parents
    if (parents%n.le.0) then
      call cubedag_message(seve%e,rname,'There should be at least 1 parent')
      error = .true.
      return
    endif
    !
    ! Fill the parents
    call parents%copy(child%node%parents,error)
    if (error)  return
    !
    ! Add the children backpointers
    call cubedag_node_add_children(child,parents,error)
    if (error)  return
  end subroutine cubedag_node_link_parents

  subroutine cubedag_node_link_twins(child,twins,error)
    !-------------------------------------------------------------------
    ! For the given 'child':
    !  - attach (overwrite) its list of twins.
    ! NO backpointer of between twins. This backpointers exists but
    ! we assume here the twins are created all at once in an efficient
    ! way (no need to add twins in several steps).
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), pointer       :: child
    type(cubedag_link_t),         intent(in)    :: twins
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='NODE>LINK>TWINS'
    !
    ! Fill the twins (can be zero-sized)
    call twins%copy(child%node%twins,error)
    if (error)  return
  end subroutine cubedag_node_link_twins
  !
  subroutine cubedag_node_add_children(child,parents,error)
    !-------------------------------------------------------------------
    ! For each parent, add a reference to this new child
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), pointer       :: child
    type(cubedag_link_t),         intent(in)    :: parents
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='NODE>ADD>CHILDREN'
    integer(kind=entr_k) :: ip
    class(cubedag_node_object_t), pointer :: dno
    !
    do ip=1,parents%n
      dno => cubedag_node_ptr(parents%list(ip)%p,error)
      if (error)  exit
      call cubedag_node_add_onechild(dno,child,error)
      if (error)  exit
    enddo
  end subroutine cubedag_node_add_children
  !
  subroutine cubedag_node_add_onechild(parent,child,error)
    !-------------------------------------------------------------------
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), intent(inout) :: parent
    class(cubedag_node_object_t), pointer       :: child
    logical,                      intent(inout) :: error
    ! Local
    integer(kind=entr_k) :: nc
    !
    ! Insert one more child to the parent
    ! ZZZ Should we protect against duplicate insertions? Can this happen
    !     under normal conditions?
    nc = parent%node%children%n + 1
    call parent%node%children%reallocate(nc,error)
    if (error)  return
    parent%node%children%list(nc)%p => child
    parent%node%children%n = nc
  end subroutine cubedag_node_add_onechild

  subroutine cubedag_node_history(link,hid,error)
    type(cubedag_link_t), intent(in)    :: link
    integer(kind=entr_k), intent(in)    :: hid
    logical,              intent(inout) :: error
    ! Local
    integer(kind=entr_k) :: iobj
    class(cubedag_node_object_t), pointer :: dno
    !
    do iobj=1,link%n
      dno => cubedag_node_ptr(link%list(iobj)%p,error)
      if (error)  return
      dno%node%history = hid
    enddo
    !
  end subroutine cubedag_node_history
  !
  !---------------------------------------------------------------------
  !
  subroutine cubedag_node_set_origin(object,origin,error)
    !-------------------------------------------------------------------
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), intent(inout) :: object
    integer(kind=code_k),         intent(in)    :: origin
    logical,                      intent(inout) :: error
    !
    object%node%origin = origin
  end subroutine cubedag_node_set_origin
  !
  subroutine cubedag_node_set_family(object,family,error)
    !-------------------------------------------------------------------
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), intent(inout) :: object
    character(len=*),             intent(in)    :: family
    logical,                      intent(inout) :: error
    !
    object%node%family = family
  end subroutine cubedag_node_set_family
  !
  subroutine cubedag_node_set_flags(object,flags,error)
    !-------------------------------------------------------------------
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), intent(inout) :: object
    type(flag_t),                 intent(in)    :: flags(:)
    logical,                      intent(inout) :: error
    !
    call object%node%flag%create(flags,error)
    if (error)  return
  end subroutine cubedag_node_set_flags
  !
  subroutine cubedag_node_set_header(object,head,error)
    use gkernel_interfaces
    use cubetools_header_types
    use cubetools_header_interface
    !-------------------------------------------------------------------
    ! The input cube_header_t is transfered to the DAG node header
    ! ZZZ This code should be made simpler when the cube_header_t
    !     becomes an extension of the cube_header_interface_t
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), intent(inout) :: object
    type(cube_header_t),          intent(in)    :: head
    logical,                      intent(inout) :: error
    !
    call cubetools_header_export(head,object%node%head,error)
    if (error)  return
  end subroutine cubedag_node_set_header
  !
  subroutine cubedag_node_get_header(object,head,error)
    use cubetools_header_types
    use cubetools_header_interface
    !-------------------------------------------------------------------
    ! The input DAG node header is transfered to the cube_header_t
    ! ZZZ This code should be made simpler when the cube_header_t
    !     becomes an extension of the cube_header_interface_t
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), intent(in)    :: object
    type(cube_header_t),          intent(inout) :: head
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='NODE>GET>HEADER'
    !
    call cubetools_header_import_and_derive(object%node%head,head,error)
    if (error)  return
  end subroutine cubedag_node_get_header
  !
  subroutine cubedag_node_set_sicvar(object,sicvar,error)
    !-------------------------------------------------------------------
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), intent(inout) :: object
    character(len=*),             intent(in)    :: sicvar
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='NODE>SET>SICVAR'
    integer(kind=4) :: nsicvar
    !
    nsicvar = object%node%nsicvar+1
    if (nsicvar.gt.dag_msicvar) then
      call cubedag_message(seve%e,rname,'Too many user variables pointing to the same object')
      error = .true.
      return
    endif
    object%node%nsicvar = nsicvar
    object%node%sicvar(nsicvar) = sicvar
  end subroutine cubedag_node_set_sicvar
  !
  subroutine cubedag_node_unset_sicvar(object,sicvar,error)
    !-------------------------------------------------------------------
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), intent(inout) :: object
    character(len=*),             intent(in)    :: sicvar
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='NODE>UNSET>SICVAR'
    integer(kind=4) :: isicvar,jsicvar
    !
    jsicvar = 0
    do isicvar=1,object%node%nsicvar
      if (object%node%sicvar(isicvar).eq.sicvar) then
        jsicvar = isicvar
        exit
      endif
    enddo
    !
    if (jsicvar.eq.0) then
      call cubedag_message(seve%w,rname,'Internal error: no such reference to variable '//sicvar)
      return
    endif
    !
    do isicvar=jsicvar+1,object%node%nsicvar
      object%node%sicvar(isicvar-1) = object%node%sicvar(isicvar)
    enddo
    object%node%nsicvar = object%node%nsicvar-1
  end subroutine cubedag_node_unset_sicvar
  !
end module cubedag_node
