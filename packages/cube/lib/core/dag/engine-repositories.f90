module cubedag_repositories
  use cubedag_parameters
  use cubedag_messaging
  use cubedag_dag
  use cubedag_repository
  use cubedag_history
  use cubedag_hrepository
  !---------------------------------------------------------------------
  ! Support module for both DAG and HISTORY repositories. There are
  ! actions where they must be treated at the same time for consistency,
  ! as they have cross links.
  !---------------------------------------------------------------------

  public :: cubedag_repositories_init
  public :: cubedag_repositories_read
  public :: cubedag_repositories_write
  public :: cubedag_repositories_free
  private

contains

  subroutine cubedag_repositories_init(error)
    !-------------------------------------------------------------------
    ! Initialize both the DAG and HISTORY repositories
    !-------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    call cubedag_repository_init('zzz',error)
    if (error)  return
    call cubedag_hrepository_init('zzz',error)
    if (error)  return
  end subroutine cubedag_repositories_init

  subroutine cubedag_repositories_write(dagname,histname,error)
    !-------------------------------------------------------------------
    ! Write the whole DAG and HISTORY repositories from memory to disk
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: dagname
    character(len=*), intent(in)    :: histname
    logical,          intent(inout) :: error
    !
    call cubedag_repository_write(dagname,error)
    if (error) return
    call cubedag_hrepository_write(histname,error)
    if (error) return
  end subroutine cubedag_repositories_write

  subroutine cubedag_repositories_read(dagname,histname,merge,error)
    !-------------------------------------------------------------------
    ! Read the DAG and HISTORY repositories from disk and load them in
    ! memory. They can be added to the current non-empty repositories.
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: dagname
    character(len=*), intent(in)    :: histname
    logical,          intent(in)    :: merge     ! Allow merging in non-empty DAG?
    logical,          intent(inout) :: error
    !
    integer(kind=iden_l) :: ixn,hxn,nshift,hshift
    character(len=*), parameter :: rname='REPOSITORIES>READ'
    !
    ixn = ix%n       ! Upper bound included
    hxn = hx%next-1  ! Upper bound excluded
    nshift = ixn-1   ! Because numbering starts at 0 (for root)
    hshift = hxn     ! Numbering starts at 1
    !
    if (.not.merge) then
      if (nshift.gt.0) then
        call cubedag_message(seve%e,rname,  &
          'Can not import '//trim(dagname)//' in a non-empty DAG')
        error = .true.
      endif
      if (hshift.gt.0) then
        call cubedag_message(seve%e,rname,  &
          'Can not import '//trim(histname)//' in a non-empty history')
        error = .true.
      endif
      if (error)  return
    endif
    !
    call cubedag_repository_read(dagname,nshift,hshift,error)
    if (error) return
    call cubedag_hrepository_read(histname,nshift,hshift,error)
    if (error) return
  end subroutine cubedag_repositories_read

  subroutine cubedag_repositories_free(error)
    !-------------------------------------------------------------------
    ! Free (empty) the DAG and HISTORY repositories from the memory.
    ! ZZZ It is unclear if the repository files should be destroyed too.
    ! ZZZ Both use cases (keeping or destroying these files) are valid.
    !-------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    ! Use brute-force destruction (works if both are destroyed simultaneously)
    call cubedag_dag_destroy(error)
    if (error)  continue
    call cubedag_history_destroy(error)
    if (error)  continue
  end subroutine cubedag_repositories_free

end module cubedag_repositories
