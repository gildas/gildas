!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubedag_library
  use cubedag_allflags
  use cubedag_node
  use cubedag_repositories
  !
  public :: cubedag_library_init,cubedag_library_exit
  private
  !
contains
  !
  subroutine cubedag_library_init(error)
    use cubedag_type
    !----------------------------------------------------------------------
    ! Initialization
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    ! Declare support for the basic Fortran type supported by the DAG
    call cubedag_type_register(&
         'NODE','node',&
         cubedag_node_allocate,  &
         cubedag_node_deallocate,  &
         code_type_node,error)
    if (error)  return
    !
    ! Must come after
    call cubedag_repositories_init(error)
    if (error)  return
    call cubedag_flag_init(error)
    if (error)  return
    call cubedag_allflags_init(error)
    if (error) return
  end subroutine cubedag_library_init
  !
  subroutine cubedag_library_exit(error)
    !----------------------------------------------------------------------
    ! Cleaning on exit
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    call cubedag_repositories_free(error)
    if (error)  continue
  end subroutine cubedag_library_exit
end module cubedag_library
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
