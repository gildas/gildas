module cubedag_node_type
  use cubedag_messaging
  use cubedag_parameters
  use cubedag_link_type
  use cubedag_nodedesc_type
  !---------------------------------------------------------------------
  ! Support module for the cubedag_node_object_t, i.e. the generic
  ! object type which is able to store any kind of object without
  ! knowing it (cubes, UV tables, etc).
  !
  ! A word on the dependencies:
  !    node -> desc -> link -> node-pointer -> node
  ! i.e. a circular dependency. These objects can not be split in
  ! several modules.
  !---------------------------------------------------------------------

  ! Placeholder for a 'node object' reference, i.e. for files indexed here. This
  ! type is extended by the external libraries using the DAG, to support their
  ! own object kinds (with their own Fortran types)
  type, extends(dag_object_t) :: cubedag_node_object_t
    type(cubedag_node_desc_t) :: node
    procedure(ltype_interface),   pointer :: ltype=>null()     ! Return string describing the polymorphic type (used e.g. in LIST)
    procedure(memsize_interface), pointer :: memsize=>null()   ! Return the memory footprint
    procedure(memsize_interface), pointer :: disksize=>null()  ! Return the disk footprint
    procedure(memsize_interface), pointer :: datasize=>null()  ! Return the data size
  ! procedure(), pointer :: free=>null()     !
  ! procedure(), pointer :: getsize=>null()  ! Useful for garbage collecting?
  contains
    procedure, public :: get_id   => cubedag_node_get_id
    procedure, public :: datatype => cubedag_node_get_datatype
  end type cubedag_node_object_t

  public :: cubedag_node_object_t
  public :: cubedag_node_ptr
  private

contains

  function ltype_interface(obj)
    !-------------------------------------------------------------------
    ! Dummy procedure providing its interface for the ltype method
    !-------------------------------------------------------------------
    character(len=2) :: ltype_interface
    class(cubedag_node_object_t), intent(in) :: obj
    ltype_interface = '??'
  end function ltype_interface

  function memsize_interface(obj)
    !-------------------------------------------------------------------
    ! Dummy procedure providing its interface for the memsize method
    !-------------------------------------------------------------------
    integer(kind=size_length) :: memsize_interface
    class(cubedag_node_object_t), intent(in) :: obj
    memsize_interface = 0
  end function memsize_interface

  function cubedag_node_get_id(obj)
    !-------------------------------------------------------------------
    ! Return the node identifier
    !-------------------------------------------------------------------
    integer(kind=iden_l) :: cubedag_node_get_id
    class(cubedag_node_object_t), intent(in) :: obj
    cubedag_node_get_id = obj%node%id
  end function cubedag_node_get_id

  function cubedag_node_get_datatype(obj)
    !-------------------------------------------------------------------
    ! Return the node data type. Not sure if this is generic on depends
    ! on the format_t.
    !-------------------------------------------------------------------
    integer(kind=code_k) :: cubedag_node_get_datatype
    class(cubedag_node_object_t), intent(in) :: obj
    cubedag_node_get_datatype = obj%node%head%array_type
  end function cubedag_node_get_datatype

  function cubedag_node_ptr(tot,error)
    use cubetools_list
    !-------------------------------------------------------------------
    ! Check if the input class is a 'cubedag_node_object_t', and return
    ! a pointer to it if relevant.
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), pointer :: cubedag_node_ptr  ! Function value on return
    class(tools_object_t),        pointer       :: tot
    logical,                      intent(inout) :: error
    !
    character(len=*), parameter :: rname='NODE>PTR'
    !
    select type(tot)
    class is (cubedag_node_object_t)
      cubedag_node_ptr => tot
    class default
      cubedag_node_ptr => null()
      call cubedag_message(seve%e,rname,'Internal error: object is not a cubedag_node_object_t')
      error = .true.
      return
    end select
  end function cubedag_node_ptr

end module cubedag_node_type
