module cubedag_repository
  use gkernel_interfaces
  use cubetools_parameters
  use cubedag_parameters
  use cubedag_flag
  use cubedag_node_type
  use cubedag_tuple
  use cubedag_dag
  use cubedag_node
  use cubedag_messaging
  use cubedag_type
  !
  integer(kind=4), parameter :: dag_version_current(2) = (/ 0,4 /)
  !
  integer(kind=4), parameter :: key_l=24  ! Note the T26 tab below
  character(len=*), parameter :: form_i4 ='(A,T26,I11,20(I11))'       ! Scalar or array I*4
  character(len=*), parameter :: form_a  ='(A,T26,A)'                 ! Scalar string

  public :: cubedag_repository_init,cubedag_repository_write,cubedag_repository_read
  private

contains

  subroutine cubedag_repository_init(path,error)
    !-------------------------------------------------------------------
    ! Initialize a new repository
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: path
    logical,          intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='REPOSITORY>INIT'
    !
    ! Create and init new one on disk
    ! ZZZ Not yet implemented
    !
    call cubedag_dag_resetcounter(error)
    if (error)  return
    !
    ! Create the root node in IX
    call cubedag_dag_root(error)
    if (error)  return
  end subroutine cubedag_repository_init

  !---------------------------------------------------------------------

  subroutine cubedag_repository_open(name,read,lun,error)
    character(len=*), intent(in)    :: name
    logical,          intent(in)    :: read
    integer(kind=4),  intent(out)   :: lun
    logical,          intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='REPOSITORY>OPEN'
    character(len=3) :: mode
    integer(kind=4) :: ier
    !
    if (read) then
      mode = 'OLD'
    else
      call cubedag_message(seve%i,rname,'Creating DAG repository in file '//name)
      mode = 'NEW'
    endif
    !
    ier = sic_getlun(lun)
    if (mod(ier,2).eq.0) then
      error = .true.
      return
    endif
    ier = sic_open(lun,name,mode,.false.)
    if (ier.ne.0) then
      call cubedag_message(seve%e,rname,'Error opening file '//name)
      call putios('E-SIC, ',ier)
      error = .true.
      return
    endif
  end subroutine cubedag_repository_open

  subroutine cubedag_repository_close(lun,error)
    integer(kind=4),  intent(in)   :: lun
    logical,          intent(inout) :: error
    ! Local
    integer(kind=4) :: ier
    !
    ier = sic_close(lun)
    call sic_frelun(lun)
  end subroutine cubedag_repository_close

  subroutine cubedag_repository_write(reponame,error)
    use cubetools_realpath
    character(len=*), intent(in)    :: reponame
    logical,          intent(inout) :: error
    ! Local
    integer(kind=entr_k) :: ient
    integer(kind=4) :: lun
    type(cubetools_realpath_t) :: dagpath
    !
    call cubedag_repository_open(reponame,.false.,lun,error)
    if (error)  return
    !
    call cubedag_write_version(lun,dag_version_current,error)
    if (error)  return
    !
    call dagpath%resolve(reponame)
    !
    do ient=2,ix%n  ! Skip root on purpose
      call cubedag_write_entry(lun,dagpath,ient,error)
      if (error)  return
    enddo
    !
    call cubedag_repository_close(lun,error)
    if (error)  return
  end subroutine cubedag_repository_write

  subroutine cubedag_write_version(lun,version,error)
    integer(kind=4), intent(in)    :: lun
    integer(kind=4), intent(in)    :: version(2)  ! Major + minor
    logical,         intent(inout) :: error
    !
    write(lun,form_i4) 'VERSION_MAJOR',version(1)
    write(lun,form_i4) 'VERSION_MINOR',version(2)
  end subroutine cubedag_write_version

  subroutine cubedag_write_entry(lun,dagpath,ient,error)
    use cubetools_realpath
    integer(kind=4),            intent(in)    :: lun
    type(cubetools_realpath_t), intent(inout) :: dagpath
    integer(kind=entr_k),       intent(in)    :: ient
    logical,                    intent(inout) :: error
    !
    class(cubedag_node_object_t), pointer :: dno
    !
    dno => cubedag_node_ptr(ix%list(ient)%p,error)
    if (error)  return
    call cubedag_write_entry_type(lun,dno,error)
    if (error)  return
    call dno%node%write(lun,dagpath,error)
    if (error)  return
  end subroutine cubedag_write_entry

  subroutine cubedag_write_entry_type(lun,object,error)
    integer(kind=4),             intent(in)    :: lun
    type(cubedag_node_object_t), intent(in)    :: object
    logical,                     intent(inout) :: error
    !
    write(lun,form_a)  'TYPE',trim(cubedag_type_tokey(object%node%type))
  end subroutine cubedag_write_entry_type

  !---------------------------------------------------------------------

  subroutine cubedag_repository_read(reponame,nshift,hshift,error)
    use cubetools_realpath
    character(len=*),     intent(in)    :: reponame
    integer(kind=iden_l), intent(in)    :: nshift  ! Node ID shift
    integer(kind=iden_l), intent(in)    :: hshift  ! History ID shift
    logical,              intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='REPOSITORY>READ'
    character(len=mess_l) :: mess
    integer(kind=4) :: lun,version(2)
    logical :: nomore
    integer(kind=entr_k) :: ient
    class(cubedag_node_object_t), pointer :: dno
    character(len=file_l) :: dagdir,dagfile
    type(cubetools_realpath_t) :: curdir
    !
    ! NB: in merge mode (nshift>0) we should detect duplicates!
    !
    call cubedag_repository_open(reponame,.true.,lun,error)
    if (error)  return
    !
    call cubedag_read_version(reponame,lun,version,error)
    if (error)  return
    !
    call sic_parse_name(reponame,ofile=dagfile,odir=dagdir)
    call curdir%resolve('.')
    !
    nomore = .false.
    do
      call cubedag_read_entry(reponame,version,lun,dagdir,curdir,  &
        nshift,hshift,nomore,error)
      if (error)  return
      if (nomore)  exit
    enddo
    !
    ! Post-read: resolve all the links (from IDs to pointers)
    do ient=1,ix%n
      dno => cubedag_node_ptr(ix%list(ient)%p,error)
      if (error)  return
      call cubedag_repo_resolve(dno,error)
      if (error)  return
    enddo
    !
    ! Feedback
    write(mess,'(A,I0,A)')  'Loaded a repository of ',ix%n,' objects'
    call cubedag_message(seve%i,rname,mess)
    !
    call cubedag_repository_close(lun,error)
    if (error)  return
    !
    call cubedag_dag_updatecounter(error)
    if (error)  return
  end subroutine cubedag_repository_read

  subroutine cubedag_read_version(reponame,lun,version,error)
    character(len=*), intent(in)    :: reponame
    integer(kind=4),  intent(in)    :: lun
    integer(kind=4),  intent(out)   :: version(2)  ! Major + minor
    logical,          intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='REPOSITORY>READ'
    character(len=key_l) :: key
    integer(kind=4) :: ier
    !
    read(lun,form_i4,iostat=ier) key,version(1)
    if (ier.gt.0) then
      call putios('E-REPOSITORY,  ',ier)
      error = .true.
      return
    endif
    if (key.ne.'VERSION_MAJOR') then
      call cubedag_message(seve%e,rname,'File '//trim(reponame)//' is malformatted')
      error = .true.
      return
    endif
    read(lun,form_i4,iostat=ier) key,version(2)
  end subroutine cubedag_read_version

  subroutine cubedag_read_entry(reponame,version,lun,dagdir,curdir,  &
    nshift,hshift,nomore,error)
    use cubetools_realpath
    character(len=*),           intent(in)    :: reponame
    integer(kind=4),            intent(in)    :: version(2)
    integer(kind=4),            intent(in)    :: lun
    character(len=*),           intent(in)    :: dagdir
    type(cubetools_realpath_t), intent(inout) :: curdir
    integer(kind=iden_l),       intent(in)    :: nshift  ! Node ID shift
    integer(kind=iden_l),       intent(in)    :: hshift  ! History ID shift
    logical,                    intent(inout) :: nomore
    logical,                    intent(inout) :: error
    ! Local
    class(cubedag_node_object_t), pointer :: object
    !
    call cubedag_read_entry_type(reponame,lun,object,nomore,error)
    if (error)  return
    if (nomore)  return
    call object%node%read(version,lun,dagdir,curdir,nshift,hshift,error)
    if (error)  return
    ! Insert in DAG
    call cubedag_dag_attach(object,error)
    if (error)  return
  end subroutine cubedag_read_entry

  subroutine cubedag_read_entry_type(reponame,lun,object,nomore,error)
    character(len=*),             intent(in)    :: reponame
    integer(kind=4),              intent(in)    :: lun
    class(cubedag_node_object_t), pointer       :: object
    logical,                      intent(inout) :: nomore
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='REPOSITORY>READ'
    character(len=key_l) :: key
    character(len=12) :: ktype
    integer(kind=4) :: ier
    integer(kind=code_k) :: type
    !
    read(lun,form_a,iostat=ier) key,ktype
    if (ier.lt.0) then
      ! EOF
      nomore = .true.
      return
    endif
    if (ier.gt.0) then
      call putios('E-REPOSITORY,  ',ier)
      error = .true.
      return
    endif
    if (key.ne.'TYPE') then
      call cubedag_message(seve%e,rname,'File '//trim(reponame)//' is malformatted')
      error = .true.
      return
    endif
    !
    call cubedag_type_tocode(ktype,type,error)
    if (error)  return
    call cubedag_dag_newnode(object,type,error)
    if (error)  return
  end subroutine cubedag_read_entry_type

  !---------------------------------------------------------------------

  subroutine cubedag_repo_resolve(obj,error)
    !-------------------------------------------------------------------
    ! Resolve all the cross-links (from IDs to pointer) for the given
    ! object.
    !-------------------------------------------------------------------
    type(cubedag_node_object_t), intent(inout) :: obj
    logical,                     intent(inout) :: error
    !
    call cubedag_link_resolve(obj%node%parents,error)
    if (error)  return
    call cubedag_link_resolve(obj%node%children,error)
    if (error)  return
    call cubedag_link_resolve(obj%node%twins,error)
    if (error)  return
    !
  end subroutine cubedag_repo_resolve

end module cubedag_repository
