module cubedag_dag
  use cubedag_messaging
  use cubedag_parameters
  use cubedag_link_type
  use cubedag_node_type
  use cubedag_node
  use cubedag_tuple
  use cubedag_type

  integer(kind=iden_l) :: id_counter
  integer(kind=iden_l) :: id_null=-1  ! 0 reserved for root
  class(cubedag_node_object_t), pointer :: root=>null()
  type(cubedag_link_t), target :: ix,cx

  public :: ix,cx
  public :: cubedag_dag_root,cubedag_dag_newnode,cubedag_dag_newbranch,  &
            cubedag_dag_attach,cubedag_dag_removenode
  public :: cubedag_dag_updatecounter,cubedag_dag_resetcounter
  public :: cubedag_dag_destroy,cubedag_index_entrynum
  public :: cubedag_dag_get_object,cubedag_dag_get_root
  public :: cubedag_dag_memsize,cubedag_dag_disksize
  public :: cubedag_dag_newid,cubedag_dag_nullid
  public :: cubedag_dag_contains,cubedag_dag_empty
  public :: cubedag_link_resolve
  private

contains

  subroutine cubedag_dag_root(error)
    !-------------------------------------------------------------------
    ! Create the root node, i.e. the one with no parent at all.
    !-------------------------------------------------------------------
    logical, intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='NODE>ROOT'
    !
    ! Sanity
    if (associated(root)) then
      call cubedag_message(seve%e,rname,'Root node already exists')
      error = .true.
      return
    endif
    if (ix%n.ne.0) then
      call cubedag_message(seve%e,rname,'Root node should be first in index!')
      error = .true.
      return
    endif
    !
    call cubedag_dag_newnode(root,code_type_node,error)
    if (error)  return
    root%node%family = '<root>'
    root%node%origin = code_origin_root
    root%node%history = 0
    !
    ! Insert in DAG
    call cubedag_dag_attach(root,error)
    if (error)  return
    !
    ! Also add a lightweight tag pointing to this root node
    ! ZZZ ?
  end subroutine cubedag_dag_root

  subroutine cubedag_dag_get_root(ptr)
    !-------------------------------------------------------------------
    ! Return a pointer to the root node
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), pointer :: ptr
    ptr => root
  end subroutine cubedag_dag_get_root

  subroutine cubedag_dag_newbranch(object,ftype,error)
    !-------------------------------------------------------------------
    ! Insert a new node (already allocated out of this subroutine) in
    ! the DAG, attached to the root.
    ! This materializes the start of a new branch in the DAG.
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), pointer       :: object
    integer(kind=code_k),         intent(in)    :: ftype
    logical,                      intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='DAG>NEWBRANCH'
    integer(kind=entr_k) :: np
    type(cubedag_link_t) :: parents
    !
    ! Sanity
    if (.not.associated(root)) then
      call cubedag_message(seve%e,rname,'Root node does not exist yet')
      error = .true.
      return
    endif
    !
    ! Minimal setup of the node_t part
    object%node%type = ftype
    call cubedag_dag_newid(object%node%id)
    call cubedag_tuple_reset(object%node%tuple)
    !
    ! Insert in DAG
    call cubedag_dag_attach(object,error)
    if (error)  return
    !
    ! Link this node to the root
    np = 1
    call parents%reallocate(np,error)
    if (error)  return
    parents%n = 1
    parents%list(1)%p => root
    call cubedag_node_link_parents(object,parents,error)
    if (error)  return
    call parents%final(error)
  end subroutine cubedag_dag_newbranch

  subroutine cubedag_dag_newnode(object,itype,error)
    !-------------------------------------------------------------------
    ! Create a new node standalone node (not attached to the DAG)
    ! This assumes connection and parents will be done later on
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), pointer       :: object
    integer(kind=code_k),         intent(in)    :: itype
    logical,                      intent(inout) :: error
    !
    ! Allocate the object in memory
    call cubedag_type_allocate(object,itype,error)
    if (error)  return
    !
    call cubedag_dag_newid(object%node%id)
    call cubedag_tuple_reset(object%node%tuple)
  end subroutine cubedag_dag_newnode

  subroutine cubedag_dag_attach(object,error)
    !-------------------------------------------------------------------
    ! Attach a node to the DAG
    !-------------------------------------------------------------------
    class(cubedag_node_object_t), pointer       :: object
    logical,                      intent(inout) :: error
    !
    call ix%reallocate(ix%n+1,error)
    if (error)  return
    !
    ! --- Insert in memory ---
    ix%n = ix%n+1
    object%node%ient = ix%n
    ix%list(ix%n)%p => object
    !
    ! --- Insert on disk ---
    ! ZZZ TO BE DONE
    ! ZZZ Insert in real time, or later?
  end subroutine cubedag_dag_attach

  subroutine cubedag_dag_destroy(error)
    !-------------------------------------------------------------------
    ! Brute-force destroy the whole DAG
    !-------------------------------------------------------------------
    logical, intent(inout) :: error
    ! Local
    integer(kind=entr_k) :: ient
    class(cubedag_node_object_t), pointer :: dno
    !
    ! Bruce force clean, do not worry about family links between nodes
    do ient=1,ix%n
      dno => cubedag_node_ptr(ix%list(ient)%p,error)
      if (error)  return
      call cubedag_node_destroy(dno,error)
      if (error)  continue
    enddo
    ix%n = 0
    root => null()
  end subroutine cubedag_dag_destroy

  subroutine cubedag_dag_removenode(id,error)
    !-------------------------------------------------------------------
    ! Properly remove a node from the DAG, taking care of its family
    ! links
    !-------------------------------------------------------------------
    integer(kind=iden_l), intent(in)    :: id
    logical,              intent(inout) :: error
    !
    ! Unreference from CX, before actual deletion from IX
    call cubedag_dag_removenode_from(cx,id,.false.,error)
    if (error)  return
    ! Unreference from IX
    call cubedag_dag_removenode_from(ix,id,.true.,error)
    if (error)  return
    !
  end subroutine cubedag_dag_removenode

  subroutine cubedag_dag_removenode_from(optx,id,rmnode,error)
    !-------------------------------------------------------------------
    ! Properly remove a node from one index, taking care of its family
    ! links
    !-------------------------------------------------------------------
    type(cubedag_link_t), intent(inout) :: optx
    integer(kind=iden_l), intent(in)    :: id
    logical,              intent(in)    :: rmnode
    logical,              intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='DAG>REMOVE>NODE'
    integer(kind=entr_k) :: ient,shift
    logical :: found
    class(cubedag_node_object_t), pointer :: dno
    !
    found = .false.
    shift = 0
    do ient=1,optx%n
      dno => cubedag_node_ptr(optx%list(ient)%p,error)
      if (error)  return
      if (dno%node%id.eq.id) then
        if (rmnode) then
          call cubedag_node_remove(dno,error)
          if (error)  return
        endif
        found = .true.
        shift = shift+1
        cycle
      endif
      if (found) then
        optx%list(ient-shift)%p => optx%list(ient)%p
        optx%flag(ient-shift)   =  optx%flag(ient)  ! Probably useless
      endif
    enddo
    optx%n = optx%n-shift
    !
    if (rmnode .and. .not.found)  &
      call cubedag_message(seve%e,rname,'ID not found in index')
    !
  end subroutine cubedag_dag_removenode_from

  !---------------------------------------------------------------------

  subroutine cubedag_dag_newid(id)
    !-------------------------------------------------------------------
    ! Generate a unique (unused yet) identifier for a new node
    !-------------------------------------------------------------------
    integer(kind=iden_l), intent(out) :: id
    id = id_counter
    id_counter = id_counter+1
  end subroutine cubedag_dag_newid

  subroutine cubedag_dag_nullid(id)
    !-------------------------------------------------------------------
    ! Set the id to the 'null' (unset yet) value
    !-------------------------------------------------------------------
    integer(kind=iden_l), intent(out) :: id
    id = id_null
  end subroutine cubedag_dag_nullid

  subroutine cubedag_dag_updatecounter(error)
    !-------------------------------------------------------------------
    ! Ensure the ID counter is ready to give a unique identifier
    !-------------------------------------------------------------------
    logical, intent(inout) :: error
    ! Local
    class(cubedag_node_object_t), pointer :: dno
    !
    ! Robust way:
    ! maxid = 0
    ! do ient=1,ix%n
    !   maxid = max(maxid,ix%list(ient)%p%node%id)
    ! enddo
    ! id_counter = maxid+1
    !
    ! Efficient way, assuming the identifiers are SORTED
    dno => cubedag_node_ptr(ix%list(ix%n)%p,error)
    if (error)  return
    id_counter = dno%node%id+1  ! Last ID + 1
    !
  end subroutine cubedag_dag_updatecounter

  subroutine cubedag_dag_resetcounter(error)
    !-------------------------------------------------------------------
    ! Reset the ID counter
    !-------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    id_counter = 0
  end subroutine cubedag_dag_resetcounter

  function cubedag_dag_entrynum(id,error)
    !-------------------------------------------------------------------
    ! Resolve the entry number corresponding to the given id in the DAG
    !-------------------------------------------------------------------
    integer(kind=entr_k) :: cubedag_dag_entrynum
    integer(kind=iden_l), intent(in)    :: id
    logical,              intent(inout) :: error
    ! Local
    integer(kind=iden_l) :: found_id
    character(len=*), parameter :: rname='DAG>ENTRYNUM'
    !
    call cubedag_index_entrynum(ix,id,found_id,cubedag_dag_entrynum,error)
    if (error)  return
    !
    if (found_id.ne.id) then
      call cubedag_message(seve%e,rname,'No such identifier in DAG')
      error = .true.
      return
    endif
  end function cubedag_dag_entrynum

  subroutine cubedag_index_entrynum(optx,id,found_id,found_num,error)
    !-------------------------------------------------------------------
    ! Resolve the entry number corresponding to the given id by looking
    ! in the given index. This resolution is based on two strong
    ! assumptions:
    !  1) the identifier is UNIQUE,
    !  2) the identifier list is SORTED
    ! No error is raised if ID is not found in the index. Return the
    ! nearest found instead (i.e. the requested ID is found if
    ! ID.eq.FOUND_ID). Let the caller decide if it is an error or not.
    !-------------------------------------------------------------------
    type(cubedag_link_t), intent(in)    :: optx
    integer(kind=iden_l), intent(in)    :: id
    integer(kind=iden_l), intent(out)   :: found_id
    integer(kind=entr_k), intent(out)   :: found_num
    logical,              intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='INDEX>ENTRYNUM'
    integer(kind=entr_k) :: inf,mid,sup
    class(cubedag_node_object_t), pointer :: dno
    !
    ! Sanity
    found_id = 0
    found_num = 0
    if (optx%n.le.0)  return
    !
    ! Dichotomic search
    dno => cubedag_node_ptr(optx%list(1)%p,error)
    if (error)  return
    if (dno%node%id.eq.id) then
      found_id = id
      found_num = 1
      return
    endif
    !
    inf = 1
    sup = optx%n
    do while (sup.gt.inf+1)
      mid = (inf+sup)/2  ! Integer division
      dno => cubedag_node_ptr(optx%list(mid)%p,error)
      if (error)  return
      if (dno%node%id.lt.id) then
        inf = mid
      else
        sup = mid
      endif
    enddo
    !
    dno => cubedag_node_ptr(optx%list(sup)%p,error)
    if (error)  return
    found_id = dno%node%id  ! Success if found_id.eq.id
    found_num = sup
  end subroutine cubedag_index_entrynum

  subroutine cubedag_dag_get_object(id,object,error)
    !-------------------------------------------------------------------
    ! This subroutine should not be used, except when reconstructing
    ! graph links (parents, children, etc) from a list of IDs to a list
    ! of nodes in memory.
    !-------------------------------------------------------------------
    integer(kind=iden_l),         intent(in)    :: id
    class(cubedag_node_object_t), pointer       :: object ! Associated on return
    logical,                      intent(inout) :: error  !
    ! Local
    integer(kind=entr_k) :: ient
    !
    ient = cubedag_dag_entrynum(id,error)
    if (error)  return
    object => cubedag_node_ptr(ix%list(ient)%p,error)
    if (error)  return
  end subroutine cubedag_dag_get_object

  subroutine cubedag_link_resolve(link,error)
    !-------------------------------------------------------------------
    ! Resolve the cross-links (from IDs to pointer) for the given list.
    ! This assumes link%flag(:) contains the ids
    !-------------------------------------------------------------------
    type(cubedag_link_t), intent(inout) :: link
    logical,              intent(inout) :: error
    ! Local
    integer(kind=entr_k) :: il
    integer(kind=iden_l) :: id
    class(cubedag_node_object_t), pointer :: targ
    !
    do il=1,link%n
      id = link%flag(il)
      call cubedag_dag_get_object(id,targ,error)
      if (error)  return
      link%list(il)%p => targ
    enddo
    !
  end subroutine cubedag_link_resolve

  function cubedag_dag_memsize()
    !-------------------------------------------------------------------
    ! Return the DAG size in memory
    !-------------------------------------------------------------------
    integer(kind=size_length) :: cubedag_dag_memsize  ! [bytes]
    ! Local
    integer(kind=entr_k) :: ient
    class(cubedag_node_object_t), pointer :: dno
    logical :: error
    !
    cubedag_dag_memsize = 0
    error = .false.
    do ient=1,ix%n
      dno => cubedag_node_ptr(ix%list(ient)%p,error)
      if (error)  return
      cubedag_dag_memsize = cubedag_dag_memsize + dno%memsize()
    enddo
  end function cubedag_dag_memsize

  function cubedag_dag_disksize()
    !-------------------------------------------------------------------
    ! Return the DAG size on disk
    !-------------------------------------------------------------------
    integer(kind=size_length) :: cubedag_dag_disksize  ! [bytes]
    ! Local
    integer(kind=entr_k) :: ient
    class(cubedag_node_object_t), pointer :: dno
    logical :: error
    !
    cubedag_dag_disksize = 0
    error = .false.
    do ient=1,ix%n
      dno => cubedag_node_ptr(ix%list(ient)%p,error)
      if (error)  return
      cubedag_dag_disksize = cubedag_dag_disksize + dno%disksize()
    enddo
  end function cubedag_dag_disksize

  function cubedag_dag_contains(file,hdu)
    !-------------------------------------------------------------------
    ! Return .true. if the DAG contains a node referencing the named
    ! file+hdu.
    !-------------------------------------------------------------------
    logical :: cubedag_dag_contains
    character(len=*), intent(in) :: file
    integer(kind=4),  intent(in) :: hdu
    ! Local
    integer(kind=entr_k) :: ient
    class(cubedag_node_object_t), pointer :: dno
    logical :: error
    !
    cubedag_dag_contains = .false.
    error = .false.
    do ient=1,ix%n
      dno => cubedag_node_ptr(ix%list(ient)%p,error)
      if (error)  return
      if (dno%node%tuple%contains(file,hdu)) then
        cubedag_dag_contains = .true.
        return
      endif
    enddo
  end function cubedag_dag_contains

  function cubedag_dag_empty()
    !-------------------------------------------------------------------
    ! Return .true. if the DAG is empty
    !-------------------------------------------------------------------
    logical :: cubedag_dag_empty
    cubedag_dag_empty = ix%n.le.1  ! Ignore the root node
  end function cubedag_dag_empty

end module cubedag_dag
