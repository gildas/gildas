!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage GILDAS DAG messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubedag_messaging
  use gpack_def
  use gbl_message
  use cubetools_parameters
  !
  public :: seve,mess_l
  public :: cubedag_message_set_id,cubedag_message
  private
  !
  ! Identifier used for message identification
  integer(kind=4) :: cubedag_message_id = gpack_global_id  ! Default value for startup message
  !
!   public :: dagseve
!   !
!   type :: dag_messaging_debug_t
!      integer(kind=code_k) :: trace = seve%t
!      integer(kind=code_k) :: others = seve%d
!   end type dag_messaging_debug_t
!   !
!   type(dag_messaging_debug_t) :: dagseve
  !
contains
  !
  subroutine cubedag_message_set_id(id)
    !---------------------------------------------------------------------
    ! Alter library id into input id. Should be called by the library
    ! which wants to share its id with the current one.
    !---------------------------------------------------------------------
    integer(kind=4), intent(in) :: id
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='MESSAGE>SET>ID'
    !
    cubedag_message_id = id
    write (mess,'(A,I0)') 'Now use id #',cubedag_message_id
    call cubedag_message(seve%d,rname,mess)
  end subroutine cubedag_message_set_id
  !
  subroutine cubedag_message(mkind,procname,message)
    use cubetools_cmessaging
    !---------------------------------------------------------------------
    ! Messaging facility for the current library. Calls the low-level
    ! (internal) messaging routine with its own identifier.
    !---------------------------------------------------------------------
    integer(kind=4),  intent(in) :: mkind     ! Message kind
    character(len=*), intent(in) :: procname  ! Name of calling procedure
    character(len=*), intent(in) :: message   ! Message string
    !
    call cubetools_cmessage(cubedag_message_id,mkind,'DAG>'//procname,message)
  end subroutine cubedag_message
  !
end module cubedag_messaging
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
