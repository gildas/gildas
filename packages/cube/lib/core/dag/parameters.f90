!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubedag_parameters
  use cubetools_parameters
  !
  integer, parameter :: iden_l=4               ! [integer] Node identifier length in DAG
  integer, parameter :: flag_l=128             ! [integer] Max string length of concatenated flags
  integer(kind=4), parameter :: dag_mflags=10  ! [integer] Maximum number of flags associated to a node
  integer(kind=4), parameter :: dag_msicvar=5  ! [integer] Maximum number of SIC variables associated to a node
  !
  integer(kind=code_k), parameter :: code_origin_unknown=1
  integer(kind=code_k), parameter :: code_origin_root=2
  integer(kind=code_k), parameter :: code_origin_imported=3
  integer(kind=code_k), parameter :: code_origin_created=4
  integer(kind=code_k), parameter :: code_origin_snapshot=5
  integer(kind=code_k), parameter :: code_origin_exported=6
  integer(kind=4), parameter :: dag_norigins=6
  character(len=*), parameter :: dag_origin_keys(dag_norigins) =  &
    (/ 'UNKNOWN ','ROOT    ','IMPORTED','CREATED ','SNAPSHOT','EXPORTED' /)
  character(len=*), parameter :: dag_origin_names(dag_norigins) =  &
    (/ 'unknown ','root    ','imported','created ','snapshot','exported' /)
  !
  ! Command history codes
  integer(kind=code_k), parameter ::  &
    code_history_notyetdefined=huge(1_code_k)  ! For entries being created (for transient use only)
end module cubedag_parameters
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
