module cubedag_find
  use gkernel_interfaces
  use cubesyntax_datatype_types
  use cubedag_parameters
  use cubedag_allflags
  use cubedag_link_type
  use cubedag_node_type
  use cubedag_messaging
  use cubedag_dag

  integer(kind=entr_k), parameter :: root_id=0
  integer(kind=entr_k), parameter :: minentr=1_entr_k
  integer(kind=entr_k), parameter :: maxentr=huge(1_entr_k)
  type :: find_prog_t
    !
    logical, private      :: lentr = .false.     ! Selection by entry range enabled?
    integer(kind=entr_k)  :: ientr(2) = [minentr,maxentr]  ! Desired entry range
    !
    logical, private      :: liden = .false.     ! Selection by identifier?
    integer(kind=iden_l)  :: iiden = -1          ! Desired identifier, if relevant
    !
    logical, private      :: lobse = .false.     ! Selection by cobse enabled?
    character(len=12)     :: cobse = strg_star   ! Desired observatory name, if relevant
    !
    logical, private      :: lsour = .false.     ! Selection by csour enabled?
    character(len=12)     :: csour = strg_star   ! Desired source name, if relevant
    !
    logical, private      :: lline = .false.     ! Selection by cline enabled?
    character(len=12)     :: cline = strg_star   ! Desired line name, if relevant
    !
    logical, private      :: lfami = .false.     ! Selection by cfami enabled?
    character(len=base_l) :: cfami = strg_star   ! Desired family  name, if relevant
    !
    logical, private      :: liflag  = .false.   ! Selection by iflag (flag_t) enabled?
    type(flag_t), allocatable :: iflags(:)       ! Desired flags, if relevant
    !
    logical, private      :: lcflag = .false.    ! Selection by ccflag (character string/pattern) enabled?
    character(len=base_l) :: ccflag = strg_star  ! Desired flag(s) name, if relevant
    !
    logical, private      :: lfreq = .false.     ! Selection by cfreq enabled?
    real(kind=coor_k)     :: rfreq = 0.d0        ! Desired frequency, if relevant
    !
    logical, private      :: lproj = .false.     ! Selection by iproj enabled?
    integer(kind=code_k)  :: iproj = code_null   ! Desired projection, if relevant
    !
    logical, private      :: lchil = .false.     ! Selection of children enabled?
    integer(kind=entr_k)  :: ichil = root_id     ! Id of the parent of the children
    !
    type(datatype_prog_t) :: datat               ! Selection by data type?
  contains
    procedure, public :: main    => cubedag_find_main
    procedure, public :: ix2cx   => cubedag_find_ix2cx
    procedure, public :: ix2optx => cubedag_find_ix2optx
    procedure, public :: cx2optx => cubedag_find_cx2optx
  end type find_prog_t

  public :: find_prog_t
  public :: minentr,maxentr
  private

contains
  !
  subroutine cubedag_find_main(prog,error)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    class(find_prog_t), intent(inout) :: prog
    logical,            intent(inout) :: error  ! Logical error flag
    ! Local
    character(len=*), parameter :: rname='FIND'
    !
    if (ix%n.le.1)  &  ! Ignore root
      call cubedag_message(seve%w,rname,'Input index is empty')
    !
    call prog%ix2cx(error)
    if (error)  return
    !
    call cubedag_find_cx_variables(error)
    if (error)  return
  end subroutine cubedag_find_main

  subroutine cubedag_find_lcriter(criter,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(find_prog_t), intent(inout) :: criter
    logical,           intent(inout) :: error
    !
    criter%lentr = criter%ientr(1).gt.minentr .or. criter%ientr(2).lt.maxentr
    criter%liden = criter%iiden.ge.0
    criter%lobse = criter%cobse.ne.strg_star
    criter%lsour = criter%csour.ne.strg_star
    criter%lline = criter%cline.ne.strg_star
    criter%lfami = criter%cfami.ne.strg_star
    if (allocated(criter%iflags)) then
      if (size(criter%iflags).eq.1 .and. criter%iflags(1).eq.flag_any) then
        criter%liflag = .false.
      else
        criter%liflag = .true.
      endif
    else
      criter%liflag = .false.
    endif
    criter%lcflag = criter%ccflag.ne.strg_star
    criter%lfreq = criter%rfreq.gt.0.d0
    criter%lproj = criter%iproj.ne.code_null
    criter%lchil = criter%ichil.ne.root_id
    criter%datat%do = criter%datat%code.ne.code_null
  end subroutine cubedag_find_lcriter

  subroutine cubedag_find_ix2cx(criter,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(find_prog_t), intent(inout) :: criter
    logical,            intent(inout) :: error
    ! Local
    character(len=*), parameter :: rname='FIND'
    character(len=message_length) :: mess
    !
    call cubedag_find_bycriter(criter,ix,cx,error)
    if (error)  return
    write(mess,'(I0,A)')  cx%n,' entries in Current indeX'
    call cubedag_message(seve%i,rname,mess)
  end subroutine cubedag_find_ix2cx

  subroutine cubedag_find_ix2optx(criter,out,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(find_prog_t),   intent(inout) :: criter
    type(cubedag_link_t), intent(inout) :: out
    logical,              intent(inout) :: error
    !
    call cubedag_find_bycriter(criter,ix,out,error)
    if (error)  return
  end subroutine cubedag_find_ix2optx

  subroutine cubedag_find_cx2optx(criter,out,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(find_prog_t),   intent(inout) :: criter
    type(cubedag_link_t), intent(inout) :: out
    logical,              intent(inout) :: error
    !
    call cubedag_find_bycriter(criter,cx,out,error)
    if (error)  return
  end subroutine cubedag_find_cx2optx
  !
  subroutine cubedag_find_bycriter(criter,in,out,error)
    use cubedag_allflags
    use cubedag_node
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    type(find_prog_t),    intent(inout) :: criter  !
    type(cubedag_link_t), intent(in)    :: in      !
    type(cubedag_link_t), intent(inout) :: out     !
    logical,              intent(inout) :: error   !
    ! Local
    character(len=*), parameter :: rname='FIND'
    integer(kind=entr_k) :: ient,nfound,nfoundall
    logical :: found
    integer(kind=4) :: iflag,iteles
    integer(kind=entr_k) :: list(in%n) ! Automatic array (assume in%n>0)
    class(cubedag_node_object_t), pointer :: obj
    character(len=256) :: nodeflag
    type(flag_t), pointer :: flag
    character(len=sour_l) :: mysource
    character(len=line_l) :: myline
    character(len=tele_l) :: myteles
    !
    call cubedag_find_lcriter(criter,error)
    if (error)  return
    !
    nfound = 0
    nfoundall = 0
    do ient=1,in%n
      obj => cubedag_node_ptr(in%list(ient)%p,error)
      if (error)  return
      !
      ! Do not find pure-node object, which are irrelevant (no data provided)
      if (obj%node%type.eq.code_type_node)  cycle
      !
      if (criter%liden) then
        if (obj%node%id.ne.criter%iiden) cycle
      endif
      !
      if (criter%lsour) then
        mysource = obj%node%head%spatial_source
        call sic_upper(mysource)
        if (.not.match_string(mysource,criter%csour)) cycle
      endif
      !
      if (criter%lline) then
        myline = obj%node%head%spectral_line
        call sic_upper(myline)
        if (.not.match_string(myline,criter%cline)) cycle
      endif
      !
      if (criter%lfami) then
        if (.not.match_string(obj%node%family,criter%cfami)) cycle
      endif
      !
      if (criter%lobse) then
        found = .false.
        do iteles=1,obj%node%head%obs%ntel
          myteles = obj%node%head%obs%tel(iteles)%name
          call sic_upper(myteles)
          if (match_string(myteles,criter%cobse)) then
            found = .true.
            exit
          endif
        enddo
        if (.not.found)  cycle
      endif
      !
      if (criter%liflag) then
        if (size(criter%iflags).ne.obj%node%flag%n)  cycle
        !
        found = .true.
        do iflag=1,obj%node%flag%n
          if (criter%iflags(iflag).eq.flag_any)  cycle
          flag => cubedag_flag_ptr(obj%node%flag%list(iflag)%p,error)
          if (error)  return
          if (criter%iflags(iflag).ne.flag) then
            found = .false.
            exit
          endif
        enddo
        if (.not.found)  cycle
      endif
      !
      if (criter%lcflag) then
        call obj%node%flag%repr(strflag=nodeflag,error=error)
        if (error)  return
        if (.not.match_string(nodeflag,criter%ccflag)) cycle
      endif
      !
      if (criter%lfreq) then
        if (.not.cubedag_find_byfreq(obj,criter%rfreq,error))  cycle
        if (error)  return
      endif
      !
      if (criter%lproj) then
        if (obj%node%head%spatial_projection_code.ne.criter%iproj)  cycle
      endif
      !
      if (criter%lchil) then
        if (.not.cubedag_find_bychild(obj,criter%ichil,error))  cycle
        if (error)  return
      endif
      !
      if (criter%datat%do) then
        if (obj%node%head%array_type.ne.criter%datat%code)  cycle
      endif
      !
      ! Must come last
      if (criter%lentr) then
        nfoundall = nfoundall+1
        if (nfoundall.lt.criter%ientr(1))  cycle
        if (nfoundall.gt.criter%ientr(2))  cycle
      endif
      !
      nfound = nfound+1
      list(nfound) = ient
    enddo
    !
    call cubedag_find_byentries(in,list,nfound,out,error)
    if (error)  return
    !
  end subroutine cubedag_find_bycriter
  !
  function cubedag_find_byfreq(obj,freq,error)
    use cubetools_header_types
    !-------------------------------------------------------------------
    ! Return .true. of the node covers the given frequency
    !-------------------------------------------------------------------
    logical :: cubedag_find_byfreq  ! Function value on return
    class(cubedag_node_object_t), intent(in)    :: obj
    real(kind=8),                 intent(in)    :: freq
    logical,                      intent(inout) :: error
    !
    type(cube_header_t) :: head
    !
    cubedag_find_byfreq = .false.
    !
    ! Convert interface to header for simplicity
    call head%init(error)
    if (error)  return
    call cubetools_header_import_and_derive(obj%node%head,head,error)
    if (error)  return
    cubedag_find_byfreq = head%spe%f%inside(freq)
    call cubetools_header_final(head,error)
    if (error)  return
  end function cubedag_find_byfreq
  !
  function cubedag_find_bychild(obj,parentid,error)
    use cubedag_walker
    !-------------------------------------------------------------------
    ! Return .true. of the node descends from the given parent
    !-------------------------------------------------------------------
    logical :: cubedag_find_bychild  ! Function value on return
    class(cubedag_node_object_t), pointer       :: obj
    integer(kind=entr_k),         intent(in)    :: parentid
    logical,                      intent(inout) :: error
    !
    class(cubedag_node_object_t), pointer :: parent
    !
    cubedag_find_bychild = .false.
    !
    call cubedag_parentwalker_reset(obj,cubedag_walker_null,error)
    if (error)  return
    do while (cubedag_parentwalker_next(parent))
      if (parent%node%id.eq.obj%node%id) then
        ! Skip object itself
        continue
      elseif (parent%node%id.eq.parentid) then
        cubedag_find_bychild = .true.
        return
      endif
    enddo
  end function cubedag_find_bychild
  !
  subroutine cubedag_find_byentries(in,entries,nentries,out,error)
    !-------------------------------------------------------------------
    ! FIND by list of entry numbers
    !-------------------------------------------------------------------
    type(cubedag_link_t), intent(in)    :: in          !
    integer(kind=entr_k), intent(in)    :: entries(:)  ! Entry numbers
    integer(kind=entr_k), intent(in)    :: nentries    !
    type(cubedag_link_t), intent(inout) :: out         !
    logical,              intent(inout) :: error       !
    ! Local
    integer(kind=entr_k) :: ient
    !
    call out%reallocate(nentries,error)
    if (error)  return
    !
    out%n = 0
    do ient=1,nentries
      out%n = out%n+1
      out%list(out%n)%p => in%list(entries(ient))%p
      out%flag(out%n)   =  in%flag(entries(ient))
    enddo
  end subroutine cubedag_find_byentries
  !
  subroutine cubedag_find_ix_variables(error)
    use gkernel_interfaces
    !-------------------------------------------------------------------
    ! Define the structure for the DAG index
    ! ZZZ Should be called by cubedag_dag_attach for each new node
    !-------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    call cubedag_find_variables(ix,'DAG',error)
    if (error)  return
  end subroutine cubedag_find_ix_variables
  !
  subroutine cubedag_find_cx_variables(error)
    use gkernel_interfaces
    !-------------------------------------------------------------------
    ! Define the structure for the CURRENT index
    !-------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    call cubedag_find_variables(cx,'IDX',error)
    if (error)  return
  end subroutine cubedag_find_cx_variables
  !
  subroutine cubedag_find_variables(optx,struct,error)
    use gkernel_interfaces
    use cubetools_userspace
    use cubetools_userstruct
    !-------------------------------------------------------------------
    ! Define the structure in the user domain describing the index
    !-------------------------------------------------------------------
    type(cubedag_link_t), intent(in)    :: optx
    character(len=*),     intent(in)    :: struct
    logical,              intent(inout) :: error
    !
    integer(kind=4) :: ier
    integer(kind=entr_k) :: ient
    class(cubedag_node_object_t), pointer :: obj
    character(len=*), parameter :: rname='FIND>VARIABLES'
    integer(kind=iden_l),  allocatable :: id(:)
    character(len=base_l), allocatable :: family(:)
    character(len=argu_l), allocatable :: flagstring(:)  ! Same as LOAD /ID => len=argu_l
    type(userstruct_t) :: idx,substruct
    !
    ! Allocate and set the temporary arrays gathering the relevant values
    if (allocated(id))  deallocate(id,family,flagstring)
    allocate(id(optx%n),family(optx%n),flagstring(optx%n),stat=ier)
    if (failed_allocate(rname,'IDX arrays',ier,error)) return
    do ient=1,optx%n
      obj => cubedag_node_ptr(optx%list(ient)%p,error)
      if (error)  return
      id(ient)     = obj%node%id
      family(ient) = obj%node%family
      call obj%node%flag%repr(strflag=flagstring(ient),error=error)
      if (error)  return
    enddo
    !
    ! The user structure is user-defined: no need for a permanent allocation
    ! of the arrays nor idx userstruct_t
    idx%name  = struct
    idx%scope = .true.  ! Global variable
    idx%overwrite = overwrite_user  ! Avoid overwriting e.g. PI
    !
    ! Main structure
    call idx%def(error)
    if (error)  return
    call idx%set_member('n',optx%n,error)
    if (error) return
    !
    ! Identifiers: same names as LOAD /ID
    call idx%def_substruct('id',substruct,error)
    if (error) return
    call substruct%set_member('id',id,error)
    if (error) return
    call substruct%set_member('family',family,error)
    if (error) return
    call substruct%set_member('flagstring',flagstring,error)
    if (error) return
    !
    ! Other sections: TBD
  end subroutine cubedag_find_variables
  !
end module cubedag_find
