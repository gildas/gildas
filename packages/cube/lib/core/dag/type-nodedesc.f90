module cubedag_nodedesc_type
  use gkernel_interfaces
  use cubetools_list
  use cubetools_header_types
  use cubetools_header_interface
  use cubedag_parameters
  use cubedag_flag
  use cubedag_tuple
  use cubedag_link_type

  type :: cubedag_node_desc_t
    integer(kind=iden_l)  :: id=0                        ! Identifier
    integer(kind=entr_k)  :: ient                        ! Entry number (backpointer to IX)
    integer(kind=code_k)  :: type=code_null              ! Fortran type identifier
    integer(kind=code_k)  :: origin=code_origin_unknown  ! Imported, created, etc
    type(flag_list_t)     :: flag                        ! Signal, noise, etc
    type(cubedag_tuple_t) :: tuple                       !
    integer(kind=iden_l)  :: history=code_history_notyetdefined  ! History identifier
    type(cubedag_link_t)  :: parents                     ! List of parents
    type(cubedag_link_t)  :: children                    ! List of children
    type(cubedag_link_t)  :: twins                       ! List of twins
    integer(kind=4)       :: nsicvar=0                   !
    character(len=varn_l) :: sicvar(dag_msicvar)=''      ! List of SIC variables pointing to the node
    character(len=base_l) :: family=''                   ! Family name
    ! Header
    type(cube_header_interface_t), public :: head
    type(cube_header_t), pointer, private :: header=>null()  ! See comments in node_head_associate
  contains
    procedure, public  :: cubeid         => node_cubeid
    procedure, public  :: write          => node_write
    procedure, public  :: read           => node_read
    procedure, private :: basic_write    => node_basic_write
    procedure, private :: basic_read     => node_basic_read
    procedure, private :: history_write  => node_history_write
    procedure, private :: history_read   => node_history_read
    procedure, private :: links_write    => node_links_write
    procedure, private :: links_read     => node_links_read
    procedure, private :: head_write     => node_head_write
    procedure, private :: head_read      => node_head_read
    procedure, public  :: head_associate => node_head_associate
    procedure, public  :: head2interf    => node_head2interf
    procedure, public  :: interf2head    => node_interf2head
  end type cubedag_node_desc_t

  integer(kind=4), parameter :: key_l=24  ! Note the T26 tab below
  character(len=*), parameter :: form_i4 ='(A,T26,I11,20(I11))'       ! Scalar or array I*4
  character(len=*), parameter :: form_i8 ='(A,T26,I20)'
  character(len=*), parameter :: form_a  ='(A,T26,A)'                 ! Scalar string

  public :: cubedag_node_desc_t
  private

contains

  function node_cubeid(node,error)
    !-------------------------------------------------------------------
    ! Return the CubeID (i.e. Family:Flags) as a string
    !-------------------------------------------------------------------
    character(len=128) :: node_cubeid
    class(cubedag_node_desc_t), intent(in)    :: node
    logical,                    intent(inout) :: error
    !
    call node%flag%repr(strflag=node_cubeid,error=error)
    node_cubeid = trim(node%family)//':'//node_cubeid
  end function node_cubeid

  subroutine node_write(node,lun,dagpath,error)
    use cubetools_realpath
    class(cubedag_node_desc_t), intent(in)    :: node
    integer(kind=4),            intent(in)    :: lun
    type(cubetools_realpath_t), intent(inout) :: dagpath
    logical,                    intent(inout) :: error
    !
    ! Order matters here as per the data format
    call node%basic_write(lun,error)
    if (error)  return
    call node%flag%write(lun,error)
    if (error)  return
    call node%tuple%write(lun,dagpath,error)
    if (error)  return
    call node%history_write(lun,error)
    if (error)  return
    call node%links_write(lun,error)
    if (error)  return
    call node%head_write(lun,error)
    if (error)  return
  end subroutine node_write

  subroutine node_read(node,version,lun,dagdir,curdir,nshift,hshift,error)
    use cubetools_realpath
    class(cubedag_node_desc_t), intent(inout) :: node
    integer(kind=4),            intent(in)    :: version(2)
    integer(kind=4),            intent(in)    :: lun
    character(len=*),           intent(in)    :: dagdir
    type(cubetools_realpath_t), intent(inout) :: curdir
    integer(kind=iden_l),       intent(in)    :: nshift  ! Node ID shift
    integer(kind=iden_l),       intent(in)    :: hshift  ! History ID shift
    logical,                    intent(inout) :: error
    !
    ! Order matters here as per the data format
    call node%basic_read(lun,nshift,error)
    if (error)  return
    call node%flag%read(lun,error)
    if (error)  return
    if (version(1).eq.0 .and. version(2).le.2) then
      ! Version 0.2 and earlier need special decoding of the tuple information
      call node%tuple%read02(lun,error)
      if (error)  return
    elseif (version(1).eq.0 .and. version(2).eq.3) then
      ! Version 0.3 has different convention on file paths
      call node%tuple%read03(lun,error)
      if (error)  return
    else
      call node%tuple%read(lun,dagdir,curdir,error)
      if (error)  return
    endif
    call node%history_read(lun,hshift,error)
    if (error)  return
    call node%links_read(lun,nshift,error)
    if (error)  return
    call node%head_read(lun,error)
    if (error)  return
  end subroutine node_read

  subroutine node_basic_write(node,lun,error)
    class(cubedag_node_desc_t), intent(in)    :: node
    integer(kind=4),            intent(in)    :: lun
    logical,                    intent(inout) :: error
    !
    write(lun,form_i8) 'ID',node%id
    write(lun,form_i4) 'ORIGIN',node%origin
    write(lun,form_a)  'FAMILY',trim(node%family)
  end subroutine node_basic_write

  subroutine node_basic_read(node,lun,nshift,error)
    class(cubedag_node_desc_t), intent(inout) :: node
    integer(kind=4),            intent(in)    :: lun
    integer(kind=iden_l),       intent(in)    :: nshift  ! Node ID shift
    logical,                    intent(inout) :: error
    ! Local
    character(len=key_l) :: key
    !
    read(lun,form_i8) key,node%id
    read(lun,form_i4) key,node%origin
    read(lun,form_a)  key,node%family
    !
    node%id = node%id + nshift
  end subroutine node_basic_read

  subroutine node_history_write(node,lun,error)
    class(cubedag_node_desc_t), intent(in)    :: node
    integer(kind=4),            intent(in)    :: lun
    logical,                    intent(inout) :: error
    !
    write(lun,form_i8) 'HISTORY',node%history
  end subroutine node_history_write

  subroutine node_history_read(node,lun,hshift,error)
    class(cubedag_node_desc_t), intent(inout) :: node
    integer(kind=4),            intent(in)    :: lun
    integer(kind=iden_l),       intent(in)    :: hshift  ! History ID shift
    logical,                    intent(inout) :: error
    ! Local
    character(len=key_l) :: key
    !
    read(lun,form_i8) key,node%history
    !
    node%history = node%history + hshift
  end subroutine node_history_read

  subroutine node_links_write(node,lun,error)
    class(cubedag_node_desc_t), intent(in)    :: node
    integer(kind=4),            intent(in)    :: lun
    logical,                    intent(inout) :: error
    !
    call node%parents%write(lun,'PARENTS',error)
    if (error)  return
    call node%children%write(lun,'CHILDREN',error)
    if (error)  return
    call node%twins%write(lun,'TWINS',error)
    if (error)  return
  end subroutine node_links_write

  subroutine node_links_read(node,lun,nshift,error)
    class(cubedag_node_desc_t), intent(inout) :: node
    integer(kind=4),            intent(in)    :: lun
    integer(kind=iden_l),       intent(in)    :: nshift  ! Node ID shift
    logical,                    intent(inout) :: error
    !
    call node%parents%read(lun,nshift,error)
    if (error)  return
    call node%children%read(lun,nshift,error)
    if (error)  return
    call node%twins%read(lun,nshift,error)
    if (error)  return
  end subroutine node_links_read

  subroutine node_head_write(node,lun,error)
    class(cubedag_node_desc_t), intent(in)    :: node
    integer(kind=4),            intent(in)    :: lun
    logical,                    intent(inout) :: error
    !
    call node%head%write(lun,error)
    if (error)  return
  end subroutine node_head_write

  subroutine node_head_read(node,lun,error)
    class(cubedag_node_desc_t), intent(inout) :: node
    integer(kind=4),            intent(in)    :: lun
    logical,                    intent(inout) :: error
    !
    call node%head%read(lun,error)
    if (error)  return
  end subroutine node_head_read

  subroutine node_head_associate(node,header,error)
    !-------------------------------------------------------------------
    ! Associate the target 'header' to the interface.
    ! ---
    ! For the time being, the cubedag_node_desc_t conveys a
    ! cube_header_t beside the cube_header_interface_t to allow easy
    ! synchronization between both. This should disappear when both
    ! types are merged.
    ! The cube_header_interface_t is named 'head' in anticipation of
    ! this improvement.
    !-------------------------------------------------------------------
    class(cubedag_node_desc_t),  intent(inout) :: node
    type(cube_header_t), target, intent(in)    :: header
    logical,                     intent(inout) :: error
    !
    node%header => header
  end subroutine node_head_associate

  subroutine node_head2interf(node,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(cubedag_node_desc_t), intent(inout) :: node
    logical,                    intent(inout) :: error
    !
    call cubetools_header_export(node%header,node%head,error)
    if (error)  return
  end subroutine node_head2interf

  subroutine node_interf2head(node,error)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(cubedag_node_desc_t), intent(inout) :: node
    logical,                    intent(inout) :: error
    !
    call cubetools_header_import_and_derive(node%head,node%header,error)
    if (error) return
  end subroutine node_interf2head

end module cubedag_nodedesc_type
