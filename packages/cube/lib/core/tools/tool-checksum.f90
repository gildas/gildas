module cubetools_checksum
  !
  integer(kind=4), parameter :: sha1_k=20        ! [bytes] Size of SHA1 binary digest
  integer(kind=4), parameter :: sha1_l=2*sha1_k  ! [char]  Length of SHA1 digest string
  character(len=sha1_l), parameter :: sha1_null=repeat("0",sha1_l)
  !
  ! C entry point interfaces
  interface
    function cubetools_sha1sum_init()
      integer(kind=4) :: cubetools_sha1sum_init
    end function cubetools_sha1sum_init
    !
    function cubetools_sha1sum_update(data,ldata)
      use iso_c_binding, only: c_ptr
      integer(kind=4) :: cubetools_sha1sum_update
      type(c_ptr), value, intent(in) :: data
      integer(kind=8),    intent(in) :: ldata
    end function cubetools_sha1sum_update
    !
    subroutine cubetools_sha1sum_final(checksum)
      integer(kind=1), intent(out) :: checksum(*)
    end subroutine cubetools_sha1sum_final
  end interface
  !
  public :: sha1_l,sha1_null
  public :: cubetools_sha1sum_init,cubetools_sha1sum_update,cubetools_sha1sum_final
  public :: cubetools_sha1sum_get
  private
  !
contains
  !
  function cubetools_sha1sum_get()
    !-------------------------------------------------------------------
    ! Get the current running checksum from the C SHA1 API.
    ! This terminates the computation, no more updates possible.
    !-------------------------------------------------------------------
    character(len=sha1_l) :: cubetools_sha1sum_get
    !
    integer(kind=1) :: checksum(sha1_k)
    !
    call cubetools_sha1sum_final(checksum)
    write(cubetools_sha1sum_get,'(*(z2.2))') checksum
    call sic_lower(cubetools_sha1sum_get)
  end function cubetools_sha1sum_get

end module cubetools_checksum
