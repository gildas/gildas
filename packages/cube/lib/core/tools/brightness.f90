!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_brightness
  use cubetools_parameters
  use cubetools_messaging
  !
  public :: applyeff
  public :: code_unit_tas,code_unit_tmb
  public :: code_unit_jyperbeam,code_unit_jyperpixel,code_unit_Mjypersr
  public :: code_unit_jy,code_unit_siflux
  !
  public :: brightness_get,flux_get
  public :: cubetools_brightness_valid_brightness_or_flux_unit
  public :: cubetools_brightness_valid_brightness_unit
  public :: cubetools_brightness_valid_flux_unit
  public :: cubetools_brightness_brightness2brightness
  public :: cubetools_brightness_brightness2flux
  public :: cubetools_brightness_flux2flux
  private
  !
  logical, parameter :: applyeff = .true.
  integer(kind=4), parameter :: unit_k = 4 ! Kind for integer unit codes
  integer(kind=4), parameter :: conv_k = 8 ! Kind for Conversion factors to avoid under or over flows
  real(kind=conv_k), parameter :: mega = 1d-6
  real(kind=conv_k), parameter :: jansky_per_siflux = 1d-26
  !
  ! Brightness units
  ! Codes (public)
  integer(kind=unit_k), parameter :: brightness_nunit = 5
  integer(kind=unit_k), parameter :: code_unit_tas        = 1
  integer(kind=unit_k), parameter :: code_unit_tmb        = 2
  integer(kind=unit_k), parameter :: code_unit_jyperbeam  = 3
  integer(kind=unit_k), parameter :: code_unit_jyperpixel = 4
  integer(kind=unit_k), parameter :: code_unit_Mjypersr   = 5
  ! Names (private, use cubetools_brightness functions to translate
  ! codes to/from names)
  integer(kind=unit_k), parameter :: brightness_munit = 7
  character(len=*), parameter :: brightness_units(brightness_munit) = [&
       'K (Ta*) ',  &
       'K (Tmb) ',  &
       'Jy/beam ',  &
       'Jy/pixel',  &
       'MJy/sr  ',  &
       ! Variants for external inputs
       'Jy/Beam ',  &  ! Variants for external inputs
       'Jy/Pixel']     ! Variants for external inputs
  ! Corresponding codes (private)
  integer(kind=code_k), parameter :: brightness_codes(brightness_munit) = [&
       code_unit_tas,         &
       code_unit_tmb,         &
       code_unit_jyperbeam,   &
       code_unit_jyperpixel,  &
       code_unit_Mjypersr,    &
       code_unit_jyperbeam,   &
       code_unit_jyperpixel]
  ! Translation routines
  interface brightness_get
    module procedure cubetools_brightness_name2code
    module procedure cubetools_brightness_code2name
  end interface brightness_get
  !
  ! Flux units
  ! Codes (public)
  integer(kind=unit_k), parameter :: flux_nunit = 3
  integer(kind=unit_k), parameter :: code_unit_siflux = 1
  integer(kind=unit_k), parameter :: code_unit_mjy    = 2
  integer(kind=unit_k), parameter :: code_unit_jy     = 3
  ! Names (private, use cubetools_flux functions to translate
  ! codes to/from names)
  integer(kind=unit_k), parameter :: flux_munit = 3
  character(len=*), parameter :: flux_units(flux_munit) = [&
       'W/m^2/Hz',  &
       'mJy     ',  &
       'Jy      ']
  ! Corresponding codes (private)
  integer(kind=code_k), parameter :: flux_codes(flux_munit) = [&
       code_unit_siflux,  &
       code_unit_mjy,     &
       code_unit_jy]
  ! Translation routines
  interface flux_get
    module procedure cubetools_flux_name2code
    module procedure cubetools_flux_code2name
  end interface flux_get
  !
contains
  !
  function cubetools_brightness_name2code(name)
    use cubetools_disambiguate
    !-------------------------------------------------------------------
    ! Translate a string to a brightness code. Search is case-sensitive
    ! and not disambiguised. Return code_unresolved if not recognized.
    !-------------------------------------------------------------------
    integer(kind=code_k) :: cubetools_brightness_name2code
    character(len=*), intent(in) :: name
    !
    integer(kind=4) :: ikey
    logical :: error
    !
    error = .false.
    cubetools_brightness_name2code = code_unresolved
    call cubetools_find_key(name,brightness_units,ikey,error)
    if (error)  return
    if (ikey.ne.code_unresolved)  &
      cubetools_brightness_name2code = brightness_codes(ikey)
  end function cubetools_brightness_name2code
  !
  function cubetools_brightness_code2name(code)
    !-------------------------------------------------------------------
    ! Translate a brightness code to its string unit. Return
    ! strg_unresolved if not a valid code.
    !-------------------------------------------------------------------
    character(len=8) :: cubetools_brightness_code2name
    integer(kind=code_k), intent(in) :: code
    !
    if (code.le.0 .or. code.gt.brightness_nunit) then
      cubetools_brightness_code2name = strg_unresolved
    else
      cubetools_brightness_code2name = brightness_units(code)
    endif
  end function cubetools_brightness_code2name
  !
  function cubetools_flux_name2code(name)
    use cubetools_disambiguate
    !-------------------------------------------------------------------
    ! Translate a string to a flux code. Search is case-sensitive
    ! and not disambiguised. Return code_unresolved if not recognized.
    !-------------------------------------------------------------------
    integer(kind=code_k) :: cubetools_flux_name2code
    character(len=*), intent(in) :: name
    !
    integer(kind=4) :: ikey
    logical :: error
    !
    error = .false.
    cubetools_flux_name2code = code_unresolved
    call cubetools_find_key(name,flux_units,ikey,error)
    if (error)  return
    if (ikey.ne.code_unresolved)  &
      cubetools_flux_name2code = flux_codes(ikey)
  end function cubetools_flux_name2code
  !
  function cubetools_flux_code2name(code)
    !-------------------------------------------------------------------
    ! Translate a flux code to its string unit. Return
    ! strg_unresolved if not a valid code.
    !-------------------------------------------------------------------
    character(len=8) :: cubetools_flux_code2name
    integer(kind=code_k), intent(in) :: code
    !
    if (code.le.0 .or. code.gt.flux_nunit) then
      cubetools_flux_code2name = strg_unresolved
    else
      cubetools_flux_code2name = flux_units(code)
    endif
  end function cubetools_flux_code2name
  !
  !---------------------------------------------------------------------
  !
  subroutine cubetools_brightness_valid_brightness_unit(unit,code,valid,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: unit
    integer(unit_k),  intent(out)   :: code
    logical,          intent(out)   :: valid
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='BRIGHTNESS>VALID>BRIGH>UNIT'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    valid = .true.
    code = cubetools_brightness_name2code(unit)
    if (code.eq.code_unresolved) then
       call cubetools_message(seve%w,rname,trim(unit)//' is not a recognized brightness unit')
       valid = .false.
    endif
  end subroutine cubetools_brightness_valid_brightness_unit
  !
  subroutine cubetools_brightness_valid_flux_unit(unit,code,valid,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    character(len=*), intent(in)    :: unit
    integer(unit_k),  intent(out)   :: code
    logical,          intent(out)   :: valid
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='BRIGHTNESS>VALID>FLUX>UNIT'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    valid = .true.
    code = cubetools_flux_name2code(unit)
    if (code.eq.code_unresolved) then
       call cubetools_message(seve%w,rname,trim(unit)//' is not a recognized flux unit')
       valid = .false.
    endif
  end subroutine cubetools_brightness_valid_flux_unit
  !
  subroutine cubetools_brightness_valid_brightness_or_flux_unit(unit,name,valid,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    character(len=*),      intent(in)    :: unit
    character(len=unit_l), intent(out)   :: name
    logical,               intent(out)   :: valid
    logical,               intent(inout) :: error
    !
    integer(unit_k) :: code
    character(len=*), parameter :: rname='BRIGHTNESS>VALID>BRIGHT>OR>FLUX>UNIT'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    valid = .true.
    !
    code = cubetools_flux_name2code(unit)
    if (code.ne.code_unresolved) then
       name = cubetools_flux_code2name(code)
       return
    endif
    !
    code = cubetools_brightness_name2code(unit)
    if (code.ne.code_unresolved) then
       name = cubetools_brightness_code2name(code)
       return
    endif
    !
    call cubetools_message(seve%w,rname,trim(unit)//' is not a recognized flux or brightness unit')
    valid = .false.
  end subroutine cubetools_brightness_valid_brightness_or_flux_unit
  !
  !------------------------------------------------------------------------
  !
  subroutine cubetools_brightness_brightness2brightness(doapplyeff,&
       feff,beff,freq,linc,minc,major,minor,unitin,unitou,factor,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    logical,              intent(in)    :: doapplyeff
    real(kind=sign_k),    intent(in)    :: feff
    real(kind=sign_k),    intent(in)    :: beff
    real(kind=coor_k),    intent(in)    :: freq
    real(kind=coor_k),    intent(in)    :: linc
    real(kind=coor_k),    intent(in)    :: minc
    real(kind=beam_k),    intent(in)    :: major
    real(kind=beam_k),    intent(in)    :: minor
    integer(kind=code_k), intent(in)    :: unitin
    integer(kind=code_k), intent(in)    :: unitou
    real(kind=sign_k),    intent(out)   :: factor
    logical,              intent(inout) :: error
    !
    character(len=mess_l) :: mess
    logical :: unknown_conversion
    real(kind=conv_k) :: unitou_per_Mjypersr,Mjypersr_per_unitin
    character(len=*), parameter :: rname='BRIGHTNESS>BRIGHTNESS2BRIGHTNESS>CONVERT'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (unitin.eq.unitou) then
       ! Nothing to be done!
       factor = 1.0
    else if ((unitin.eq.code_unit_tmb).and.(unitou.eq.code_unit_tas)) then
       if (doapplyeff) then
          ! From Tmb to Ta* => Only require Feff and Beff!
          if (badeff(feff,beff,error)) return
          factor = beff/feff
       else
          call cubetools_message(seve%e,rname,  &
            'Efficiencies required when converting '//  &
            trim(cubetools_brightness_code2name(code_unit_tmb))//  &
            ' to '//cubetools_brightness_code2name(code_unit_tas))
          error = .true.
          return
       endif
    else if ((unitin.eq.code_unit_tas).and.(unitou.eq.code_unit_tmb)) then
       if (doapplyeff) then
          ! From Ta* to Tmb => Only require Feff and Beff!
          if (badeff(feff,beff,error)) return
          factor = feff/beff
       else
          call cubetools_message(seve%e,rname,  &
            'Efficiencies required when converting '//  &
            trim(cubetools_brightness_code2name(code_unit_tas))//  &
            ' to '//cubetools_brightness_code2name(code_unit_tmb))
          error = .true.
          return
       endif
    else
       unknown_conversion = .false.
       ! Going from unitin to Mjypersr
       select case(unitin)
       case(code_unit_tmb,code_unit_tas)
          if (badfreq(freq,error)) return
          Mjypersr_per_unitin = Mjypersr_per_k(freq)
          if (unitin.eq.code_unit_tas) then
             if (doapplyeff) then
                if (badeff(feff,beff,error)) return
                Mjypersr_per_unitin = Mjypersr_per_unitin*beff/feff
             else
                call cubetools_message(seve%w,rname,'Converting from '// &
                  cubetools_brightness_code2name(code_unit_tas)//  &
                  ' without efficiencies')
             endif
          endif
       case(code_unit_Mjypersr)
          Mjypersr_per_unitin = 1.0
       case(code_unit_jyperpixel)
          if (badpixel(linc,minc,error)) return
          Mjypersr_per_unitin = Mjypersr_per_jyperpixel(linc,minc)
       case (code_unit_jyperbeam)
          if (badbeam(major,minor,error)) return
          Mjypersr_per_unitin = Mjypersr_per_jyperbeam(major,minor)
       case default
          unknown_conversion = .true.
       end select
       !
       ! Going from Mjypersr to unitou
       select case(unitou)
       case(code_unit_tmb,code_unit_tas)
          if (badfreq(freq,error)) return
          unitou_per_Mjypersr = 1.0/Mjypersr_per_k(freq)
          if (unitou.eq.code_unit_tas) then
             if (doapplyeff) then
                if (badeff(feff,beff,error)) return
                unitou_per_Mjypersr = unitou_per_Mjypersr*feff/beff
             else
                call cubetools_message(seve%w,rname,'Converting to '// &
                     trim(cubetools_brightness_code2name(code_unit_tas))//  &
                     ' without efficiencies')
             endif
          endif
       case(code_unit_Mjypersr)
          unitou_per_Mjypersr = 1.0
       case(code_unit_jyperpixel)
          if (badpixel(linc,minc,error)) return
          unitou_per_Mjypersr = 1.0/Mjypersr_per_jyperpixel(linc,minc)
       case (code_unit_jyperbeam)
          if (badbeam(major,minor,error)) return
          unitou_per_Mjypersr = 1.0/Mjypersr_per_jyperbeam(major,minor)
       case default
          unknown_conversion = .true.
       end select
       if (unknown_conversion) then
          write(mess,'(2(a,i0))') 'Unknown brightness conversion from ',unitin,' to ',unitou
          call cubetools_message(seve%e,rname,mess)
          error = .true.
          return
       endif
       factor = real(unitou_per_Mjypersr*Mjypersr_per_unitin,sign_k)
    endif
  end subroutine cubetools_brightness_brightness2brightness
  !
  subroutine cubetools_brightness_brightness2flux(&
       freq,linc,minc,major,minor,brightunit,fluxunit,factor,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    real(kind=coor_k),    intent(in)    :: freq
    real(kind=coor_k),    intent(in)    :: linc
    real(kind=coor_k),    intent(in)    :: minc
    real(kind=beam_k),    intent(in)    :: major
    real(kind=beam_k),    intent(in)    :: minor
    integer(kind=code_k), intent(in)    :: brightunit
    integer(kind=code_k), intent(in)    :: fluxunit
    real(kind=sign_k),    intent(out)   :: factor
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='BRIGHTNESS>BRIGHTNESS2FLUX'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    ! Sanity warning
    if (brightunit.eq.code_unit_tas) then
       call cubetools_message(seve%w,rname,'Computing flux on a Ta* cube assumes that the source is extended!')
       call cubetools_message(seve%w,rname,'Please consider to convert first to Tmb, depending on the source extent!')
    endif
    ! From brightness to flux in Jansky
    select case(brightunit)
    case(code_unit_tas,code_unit_tmb)
       if (badfreq(freq,error)) return
       if (badpixel(linc,minc,error)) return
       factor = Mjypersr_per_k(freq)/Mjypersr_per_jyperpixel(linc,minc)
    case(code_unit_jyperbeam)
       if (badbeam(major,minor,error)) return
       if (badpixel(linc,minc,error)) return
       factor = Mjypersr_per_jyperbeam(major,minor)/Mjypersr_per_jyperpixel(linc,minc)
    case(code_unit_jyperpixel)
       factor = 1.0
    case(code_unit_Mjypersr)
       if (badpixel(linc,minc,error)) return
       factor = 1.0/Mjypersr_per_jyperpixel(linc,minc)
    end select
    if (error) return
    ! From Jansky to SI flux
    if (fluxunit.eq.code_unit_siflux) factor = factor*jansky_per_siflux
  end subroutine cubetools_brightness_brightness2flux
  !
  subroutine cubetools_brightness_flux2flux(unitin,unitou,factor,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    integer(kind=code_k), intent(in)    :: unitin
    integer(kind=code_k), intent(in)    :: unitou
    real(kind=sign_k),    intent(out)   :: factor
    logical,              intent(inout) :: error
    !
    character(len=mess_l) :: mess
    real(kind=conv_k) :: in2jy,jy2ou
    logical :: unknown_convertion
    character(len=*), parameter :: rname='BRIGHTNESS>FLUX2FLUX'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (unitin.eq.unitou) then
       factor = 1.0
    else
       unknown_convertion = .false.
       select case (unitin)
       case(code_unit_mjy)
          in2jy = 1d-3
       case(code_unit_jy)
          in2jy = 1d0
       case(code_unit_siflux)
          in2jy = 1d0/jansky_per_siflux
       case default
          unknown_convertion = .true.
       end select
       !
       select case (unitou)
       case(code_unit_mjy)
          jy2ou = 1d3
       case(code_unit_jy)
          jy2ou = 1d0
       case(code_unit_siflux)
          jy2ou = jansky_per_siflux
       case default
          unknown_convertion = .true.
       end select
       !
       if (unknown_convertion) then
          write(mess,'(2(a,i0))') 'Unknown brightness conversion from ',unitin,' to ',unitou
          call cubetools_message(seve%e,rname,mess)
          error = .true.
          return
       else
          factor = real(in2jy*jy2ou,sign_k)
       endif
    endif
  end subroutine cubetools_brightness_flux2flux
  !
  !------------------------------------------------------------------------
  !
  function badfreq(freq,error)
    use ieee_arithmetic
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    logical                          :: badfreq ! intent(out)
    real(kind=coor_k), intent(in)    :: freq
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='BRIGHTNESS>BADFREQ'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (ieee_is_nan(freq)) then
       call cubetools_message(seve%e,rname,'Undefined frequency')
       badfreq = .true.
    else if (freq.le.0d0) then
       call cubetools_message(seve%e,rname,'Zero valued or negative frequency')
       badfreq = .true.
    else       
       badfreq = .false.
    endif
    if (badfreq) error = .true.
  end function badfreq
  !
  function badbeam(major,minor,error)
    use ieee_arithmetic
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    logical                          :: badbeam ! intent(out)
    real(kind=beam_k), intent(in)    :: major
    real(kind=beam_k), intent(in)    :: minor
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='BRIGHTNESS>BADBEAM'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (ieee_is_nan(major).or.(ieee_is_nan(minor))) then
       call cubetools_message(seve%e,rname,'Undefined beam')
       badbeam = .true.
    else if ((major.le.0.0).or.(minor.le.0.0)) then
       call cubetools_message(seve%e,rname,'Zero valued or negative beam')
       badbeam = .true.
    else       
       badbeam = .false.
    endif
    if (badbeam) error = .true.
  end function badbeam
  !
  function badpixel(linc,minc,error)
    use ieee_arithmetic
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    logical                          :: badpixel ! intent(out)
    real(kind=coor_k), intent(in)    :: linc
    real(kind=coor_k), intent(in)    :: minc
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='BRIGHTNESS>BADPIXEL'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (ieee_is_nan(linc).or.(ieee_is_nan(minc))) then
       call cubetools_message(seve%e,rname,'Undefined pixel')
       badpixel = .true.
    else if ((linc.eq.0.0).or.(minc.eq.0.0)) then
       call cubetools_message(seve%e,rname,'Zero valued pixel area')
       badpixel = .true.
    else       
       badpixel = .false.
    endif
    if (badpixel) error = .true.
  end function badpixel
  !
  function badeff(feff,beff,error)
    use ieee_arithmetic
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    logical                          :: badeff ! intent(out)
    real(kind=sign_k), intent(in)    :: feff
    real(kind=sign_k), intent(in)    :: beff
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='BRIGHTNESS>BADEFF'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    if (ieee_is_nan(feff).or.(ieee_is_nan(beff))) then
       call cubetools_message(seve%e,rname,'Undefined efficiencies')
       badeff = .true.
    else if ((feff.le.0.0).or.(beff.le.0.0)) then
       call cubetools_message(seve%e,rname,'Zero valued or negative efficiencies')
       badeff = .true.
    else if ((feff.ge.1.0).or.(beff.ge.1.0)) then
       call cubetools_message(seve%e,rname,'Efficiencies greater than unity')
       badeff = .true.
    else       
       badeff = .false.
    endif
    if (badeff) error = .true.
  end function badeff
  !
  !------------------------------------------------------------------------
  !  
  function beamarea(major,minor)
    use phys_const
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    real(kind=conv_k)             :: beamarea ! intent(out)
    real(kind=beam_k), intent(in) :: major
    real(kind=beam_k), intent(in) :: minor
    !
    character(len=*), parameter :: rname='BRIGHTNESS>BEAMAREA'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    beamarea = (pi*major*minor)/(4.0d0*log(2.0d0)) ! [st]
  end function beamarea
  !
  function Mjypersr_per_k(freq)
    use phys_const
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    real(kind=conv_k)             :: Mjypersr_per_k ! intent(out)
    real(kind=coor_k), intent(in) :: freq
    !
    real(kind=coor_k) :: lambda
    character(len=*), parameter :: rname='BRIGHTNESS>MJYPESR>PER>K'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    lambda = clight_mhz/freq
    Mjypersr_per_k = mega/jansky_per_siflux*2.0d0*kbolt/lambda**2
  end function Mjypersr_per_k
  !
  function Mjypersr_per_jyperpixel(linc,minc)
    use phys_const
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    real(kind=conv_k)             :: Mjypersr_per_jyperpixel ! intent(out)
    real(kind=coor_k), intent(in) :: linc
    real(kind=coor_k), intent(in) :: minc
    !
    character(len=*), parameter :: rname='BRIGHTNESS>MJYPERSR>PER>JYPERPIXEL'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    Mjypersr_per_jyperpixel = mega/abs(linc*minc)
  end function Mjypersr_per_jyperpixel
  !
  function Mjypersr_per_jyperbeam(major,minor)
    use phys_const
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    real(kind=conv_k)             :: Mjypersr_per_jyperbeam ! intent(out)
    real(kind=beam_k), intent(in) :: major
    real(kind=beam_k), intent(in) :: minor
    !
    character(len=*), parameter :: rname='BRIGHTNESS>MJYPERSR>PER>JYPERBEAM'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    Mjypersr_per_jyperbeam = mega/beamarea(major,minor)
  end function Mjypersr_per_jyperbeam
end module cubetools_brightness
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
