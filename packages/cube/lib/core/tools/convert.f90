!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_convert
  use cubetools_parameters
  use cubetools_messaging
  !
  public :: cubetools_convert_val2zero,cubetools_convert_fres2vres,cubetools_convert_vres2fres
  private
  !
contains
  !
  subroutine cubetools_convert_val2zero(inref,inval,ininc,ouref)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    real(kind=coor_k), intent(in)  :: inref
    real(kind=coor_k), intent(in)  :: inval
    real(kind=coor_k), intent(in)  :: ininc
    real(kind=coor_k), intent(out) :: ouref
    !
    character(len=*), parameter :: rname='CONVERT>VAL2ZERO'
    !
    call cubetools_message(toolseve%trace,rname,'Welcome')
    !
    ouref = inref-(inval/ininc)
  end subroutine cubetools_convert_val2zero
  !
  function cubetools_convert_fres2vres(fres,freq) result(vres)
    use phys_const
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    real(kind=coor_k)            :: vres
    real(kind=coor_k),intent(in) :: fres
    real(kind=coor_k),intent(in) :: freq
    !
    vres=-fres*clight_kms/freq
  end function cubetools_convert_fres2vres
  !
  function cubetools_convert_vres2fres(vres,freq) result(fres)
    use phys_const
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    real(kind=coor_k)            :: fres
    real(kind=coor_k),intent(in) :: vres
    real(kind=coor_k),intent(in) :: freq
    !
    fres=-freq*vres/clight_kms
  end function cubetools_convert_vres2fres
end module cubetools_convert
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
