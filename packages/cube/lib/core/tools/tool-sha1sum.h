#ifndef _CUBETOOLS_SHA1SUM_H_
#define _CUBETOOLS_SHA1SUM_H_

#include "gsys/cfc.h"

#define cubetools_sha1sum_init   CFC_EXPORT_NAME( cubetools_sha1sum_init)
#define cubetools_sha1sum_update CFC_EXPORT_NAME( cubetools_sha1sum_update)
#define cubetools_sha1sum_final  CFC_EXPORT_NAME( cubetools_sha1sum_final)

int32_t CFC_API cubetools_sha1sum_init();
int32_t CFC_API cubetools_sha1sum_update( unsigned char *data, int64_t *ldata);
void    CFC_API cubetools_sha1sum_final( void *outbytes);

#endif /* _CUBETOOLS_SHA1SUM_H_ */
