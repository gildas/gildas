!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_format
  use cubetools_parameters
  !
  public :: form_l,bold_l
  public :: nsimple,fsimple
  public :: ndouble,fdouble
  !
  public :: cubetools_format_memsize
  public :: cubetools_format_color,cubetools_format_bold,cubetools_format_right
  public :: cubetools_format_stdkey_stdval,cubetools_format_stdkey_boldval
  public :: cubetools_format_diff,cubetools_format_2values
  public :: cubetools_format_time
  public :: cubetools_format_range,cubetools_format_size_center
  public :: cubetools_format_offset,cubetools_format_radec
  private
  !
  character(len=*), parameter :: fmtrd = '1pg10.3'
  integer(kind=4),  parameter :: form_l = 32
  integer(kind=4),  parameter :: relative_l = 10
  integer(kind=4),  parameter :: bold_l = 8
  !
  integer(kind=4), parameter :: nsimple = 15
  integer(kind=4), parameter :: ndouble = 25
  character(len=8), parameter :: fsimple = '1PG14.7'  ! '1PG15.7'  in EXAMINE command
  character(len=8), parameter :: fdouble = '1PG23.16' ! '1PG25.16' in EXAMINE command
  !
  interface cubetools_format_memsize
     module procedure cubetools_format_memsize_integer8
     module procedure cubetools_format_memsize_real4
  end interface cubetools_format_memsize
  !
  interface cubetools_format_value2string
     module procedure cubetools_format_value2string_integer4
     module procedure cubetools_format_value2string_integer8
     module procedure cubetools_format_value2string_real4
     module procedure cubetools_format_value2string_real8
  end interface cubetools_format_value2string
  !
  interface cubetools_format_stdkey_stdval
     module procedure cubetools_format_stdkey_stdval_integer4
     module procedure cubetools_format_stdkey_stdval_integer8
     module procedure cubetools_format_stdkey_stdval_real4
     module procedure cubetools_format_stdkey_stdval_real8
     module procedure cubetools_format_stdkey_stdval_string
  end interface cubetools_format_stdkey_stdval
  !
  interface cubetools_format_stdkey_boldval
     module procedure cubetools_format_stdkey_boldval_integer4
     module procedure cubetools_format_stdkey_boldval_integer8
     module procedure cubetools_format_stdkey_boldval_real4
     module procedure cubetools_format_stdkey_boldval_real8
     module procedure cubetools_format_stdkey_boldval_string
  end interface cubetools_format_stdkey_boldval
  !
  interface cubetools_format_right
     module procedure cubetools_format_right_logical
     module procedure cubetools_format_right_integer4
     module procedure cubetools_format_right_integer8
     module procedure cubetools_format_right_real4
     module procedure cubetools_format_right_real8
     module procedure cubetools_format_right_string
  end interface cubetools_format_right
  !
  interface cubetools_format_diff
     module procedure cubetools_format_diff_integer4
     module procedure cubetools_format_diff_integer8
     module procedure cubetools_format_diff_real4
     module procedure cubetools_format_diff_real4_unit
     module procedure cubetools_format_diff_real8
     module procedure cubetools_format_diff_real8_unit
  end interface cubetools_format_diff
  !
  interface cubetools_format_2values
     module procedure cubetools_format_2values_logical
     module procedure cubetools_format_2values_integer8
     module procedure cubetools_format_2values_integer4
     module procedure cubetools_format_2values_real4
     module procedure cubetools_format_2values_real4_unit
     module procedure cubetools_format_2values_2units_real4
     module procedure cubetools_format_2values_real8
     module procedure cubetools_format_2values_real8_unit
     module procedure cubetools_format_2values_2units_real8
     module procedure cubetools_format_2values_string
  end interface cubetools_format_2values
  !
contains
  !
  function cubetools_format_memsize_real4(nbytes) result(result)
    !----------------------------------------------------------
    !
    !----------------------------------------------------------
    character(len=8)         :: result
    real(kind=4), intent(in) :: nbytes ! [Bytes]
    result = cubetools_format_memsize_integer8(int(nbytes,kind=8))
  end function cubetools_format_memsize_real4
  !
  function cubetools_format_memsize_integer8(nbytes) result(result)
    !----------------------------------------------------------
    !
    !----------------------------------------------------------
    character(len=8)            :: result
    integer(kind=8), intent(in) :: nbytes ! [Bytes]
    !
    integer(kind=4) :: iunit
    real(kind=8) :: size
    integer(kind=4), parameter :: munits=6
    character(len=3), parameter :: units(munits) =  &
      (/ 'B  ','kiB','MiB','GiB','TiB','PiB' /)
    character(len=*), parameter :: rname='FORMAT>MEMORY'
    !
    size = abs(nbytes)
    iunit = 1
    do while (iunit.le.munits)
      if (size.ge.1024.d0) then
        iunit = iunit+1
        size = size/1024.d0
        cycle
      endif
      if (nint(size*100).lt.1000) then
        write(result,'(F4.2,1X,A3)') size,units(iunit)
      elseif (nint(size*10).lt.1000) then
        write(result,'(F4.1,1X,A3)') size,units(iunit)
      else ! 100.d0 <= size < 1024.d0
        write(result,'(  I4,1X,A3)') nint(size),units(iunit)
      endif
      return
    enddo
    ! Fallback if too large
    write(result,'(a5,a3)') '>1024',units(munits)
  end function cubetools_format_memsize_integer8
  !
  !----------------------------------------------------------------------
  !
  function cubetools_format_color(string,color) result(result)
    use gbl_ansicodes
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    character(len=*), intent(in) :: string
    character(len=*), intent(in) :: color
    !
    integer(kind=4) :: length
    character(len=:), allocatable :: result
    !
    length = len_trim(color)+len_trim(string)+len_trim(c_clear)
    allocate(character(length)::result)
    write(result,'(3a)') trim(color),trim(string),c_clear
  end function cubetools_format_color
  !
  function cubetools_format_bold(string) result(result)
    use gbl_ansicodes
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    character(len=*), intent(in) :: string
    !
    integer(kind=4) :: length
    character(len=:), allocatable :: result
    !
    length = bold_l+len_trim(string)
    allocate(character(length)::result)
    write(result,'(3a)') c_bold,trim(string),c_clear
  end function cubetools_format_bold
  !
  !----------------------------------------------------------------------
  !
  function cubetools_format_value2string_integer4(value,format,result_l) result(result)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    integer(kind=4),  intent(in) :: value
    character(len=*), intent(in) :: format
    integer(kind=4),  intent(in) :: result_l
    !
    character(len=:), allocatable :: result
    !
    allocate(character(result_l)::result)
!!$ if (len_trim(value).le.result_l) then
    write(result,'('//format//')') value
!!$ else
!!$    ! *** JP: A warning would be welcome here
!!$ endif
  end function cubetools_format_value2string_integer4
  !
  function cubetools_format_value2string_integer8(value,format,result_l) result(result)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    integer(kind=8),  intent(in) :: value
    character(len=*), intent(in) :: format
    integer(kind=4),  intent(in) :: result_l
    !
    character(len=:), allocatable :: result
    !
    allocate(character(result_l)::result)
!!$ if (len_trim(value).le.result_l) then
    write(result,'('//format//')') value
!!$ else
!!$    ! *** JP: A warning would be welcome here
!!$ endif
  end function cubetools_format_value2string_integer8
  !
  function cubetools_format_value2string_real4(value,format,result_l) result(result)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    real(kind=4),     intent(in) :: value
    character(len=*), intent(in) :: format
    integer(kind=4),  intent(in) :: result_l
    !
    character(len=:), allocatable :: result
    !
    allocate(character(result_l)::result)
!!$ if (len_trim(value).le.result_l) then
    write(result,'('//format//')') value
!!$ else
!!$    ! *** JP: A warning would be welcome here
!!$ endif
  end function cubetools_format_value2string_real4
  !
  function cubetools_format_value2string_real8(value,format,result_l) result(result)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    real(kind=8),     intent(in) :: value
    character(len=*), intent(in) :: format
    integer(kind=4),  intent(in) :: result_l
    !
    character(len=:), allocatable :: result
    !
    allocate(character(result_l)::result)
!!$ if (len_trim(value).le.result_l) then
    write(result,'('//format//')') value
!!$ else
!!$    ! *** JP: A warning would be welcome here
!!$ endif
  end function cubetools_format_value2string_real8
  !
  !----------------------------------------------------------------------
  !
  function cubetools_format_stdkey_stdval_integer4(keyword,value,format,result_l) result(result)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    character(len=*), intent(in) :: keyword
    integer(kind=4),  intent(in) :: value
    character(len=*), intent(in) :: format
    integer(kind=4),  intent(in) :: result_l
    !
    character(len=:), allocatable :: result
    !
    result = cubetools_format_stdkey_stdval_string(&
         keyword,&
         cubetools_format_value2string(value,format,result_l),&
         result_l)
  end function cubetools_format_stdkey_stdval_integer4
  !
  function cubetools_format_stdkey_stdval_integer8(keyword,value,format,result_l) result(result)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    character(len=*), intent(in) :: keyword
    integer(kind=8),  intent(in) :: value
    character(len=*), intent(in) :: format
    integer(kind=4),  intent(in) :: result_l
    !
    character(len=:), allocatable :: result
    !
    result = cubetools_format_stdkey_stdval_string(&
         keyword,&
         cubetools_format_value2string(value,format,result_l),&
         result_l)
  end function cubetools_format_stdkey_stdval_integer8
  !
  function cubetools_format_stdkey_stdval_real4(keyword,value,format,result_l) result(result)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    character(len=*), intent(in) :: keyword
    real(kind=4),     intent(in) :: value
    character(len=*), intent(in) :: format
    integer(kind=4),  intent(in) :: result_l
    !
    character(len=:), allocatable :: result
    !
    result = cubetools_format_stdkey_stdval_string(&
         keyword,&
         cubetools_format_value2string(value,format,result_l),&
         result_l)
  end function cubetools_format_stdkey_stdval_real4
  !
  function cubetools_format_stdkey_stdval_real8(keyword,value,format,result_l) result(result)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    character(len=*), intent(in) :: keyword
    real(kind=8),     intent(in) :: value
    character(len=*), intent(in) :: format
    integer(kind=4),  intent(in) :: result_l
    !
    character(len=:), allocatable :: result
    !
    result = cubetools_format_stdkey_stdval_string(&
         keyword,&
         cubetools_format_value2string(value,format,result_l),&
         result_l)
  end function cubetools_format_stdkey_stdval_real8
  !
  function cubetools_format_stdkey_stdval_string(keyword,string,result_l) result(result)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    character(len=*), intent(in) :: keyword
    character(len=*), intent(in) :: string
    integer(kind=4),  intent(in) :: result_l
    !
    integer(kind=4) :: stg_l,key_l
    character(len=:), allocatable :: result
    !
    allocate(character(result_l)::result)
    stg_l = len_trim(string)
    key_l = len_trim(keyword)
    ! Trim must not be made here if result was made bold previously
    ! if ((key_l+1+stg_l).gt.result_l) stg_l = result_l-key_l-1
    result(:) = string(1:stg_l)
    result(:) = adjustr(result)
    result(1:key_l) = keyword(1:key_l)
!!$    print *,'debug '
!!$    print *,'keyword: ',keyword,'-',key_l,'   string ',string,'-',stg_l
!!$    print *,'result:  ',result,'-',result_l
!!$    print *,'debug '
  end function cubetools_format_stdkey_stdval_string
  !
  !----------------------------------------------------------------------
  !
  function cubetools_format_stdkey_boldval_integer4(keyword,value,format,result_l) result(result)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    character(len=*), intent(in) :: keyword
    integer(kind=4),  intent(in) :: value
    character(len=*), intent(in) :: format
    integer(kind=4),  intent(in) :: result_l
    !
    character(len=:), allocatable :: result
    !
!    allocate(character(result_l+bold_l)::result)
    result = cubetools_format_stdkey_boldval_string(&
         keyword,&
         cubetools_format_value2string(value,format,result_l),&
         result_l)
  end function cubetools_format_stdkey_boldval_integer4
  !
  function cubetools_format_stdkey_boldval_integer8(keyword,value,format,result_l) result(result)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    character(len=*), intent(in) :: keyword
    integer(kind=8),  intent(in) :: value
    character(len=*), intent(in) :: format
    integer(kind=4),  intent(in) :: result_l
    !
    character(len=:), allocatable :: result
    !
!    allocate(character(result_l+bold_l)::result)
    result = cubetools_format_stdkey_boldval_string(&
         keyword,&
         cubetools_format_value2string(value,format,result_l),&
         result_l)
  end function cubetools_format_stdkey_boldval_integer8
  !
  function cubetools_format_stdkey_boldval_real4(keyword,value,format,result_l) result(result)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    character(len=*), intent(in) :: keyword
    real(kind=4),     intent(in) :: value
    character(len=*), intent(in) :: format
    integer(kind=4),  intent(in) :: result_l
    !
    character(len=:), allocatable :: result
    !
!    allocate(character(result_l+bold_l)::result)
    result = cubetools_format_stdkey_boldval_string(&
         keyword,&
         cubetools_format_value2string(value,format,result_l),&
         result_l)
  end function cubetools_format_stdkey_boldval_real4
  !
  function cubetools_format_stdkey_boldval_real8(keyword,value,format,result_l) result(result)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    character(len=*), intent(in) :: keyword
    real(kind=8),     intent(in) :: value
    character(len=*), intent(in) :: format
    integer(kind=4),  intent(in) :: result_l
    !
    character(len=:), allocatable :: result
    !
!    allocate(character(result_l+bold_l)::result)
    result = cubetools_format_stdkey_boldval_string(&
         keyword,&
         cubetools_format_value2string(value,format,result_l),&
         result_l)
  end function cubetools_format_stdkey_boldval_real8
  !
  function cubetools_format_stdkey_boldval_string(keyword,string,result_l) result(result)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    character(len=*), intent(in) :: keyword
    character(len=*), intent(in) :: string
    integer(kind=4),  intent(in) :: result_l
    !
    integer(kind=4) :: stg_l,key_l
    character(len=:), allocatable :: result
    !
    !    allocate(character(result_l+bold_l)::result)
    stg_l = len_trim(string)
    key_l = len_trim(keyword)
    ! Trim must be made here before sending result to format_bold
    if ((key_l+1+stg_l).gt.result_l) stg_l = result_l-key_l-1
    result = cubetools_format_stdkey_stdval_string(&
         keyword,&
         cubetools_format_bold(string(1:stg_l)),&
         result_l+bold_l)
  end function cubetools_format_stdkey_boldval_string
  !
  !----------------------------------------------------------------------
  !
  function cubetools_format_right_real8(value,fmt,format_l) result(result)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    real(kind=8),     intent(in) :: value
    character(len=*), intent(in) :: fmt
    integer(kind=4),  intent(in) :: format_l
    !
    character(len=format_l) :: result
    !
    write(result,'('//fmt//')') value
    result(:) = adjustr(result)
  end function cubetools_format_right_real8
  !
  function cubetools_format_right_real4(value,fmt,format_l) result(result)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    real(kind=4),     intent(in) :: value
    character(len=*), intent(in) :: fmt
    integer(kind=4),  intent(in) :: format_l
    !
    character(len=format_l) :: result
    !
    write(result,'('//fmt//')') value
    result(:) = adjustr(result)
  end function cubetools_format_right_real4
  !
  function cubetools_format_right_integer4(value,fmt,format_l) result(result)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    integer(kind=4),  intent(in) :: value
    character(len=*), intent(in) :: fmt
    integer(kind=4),  intent(in) :: format_l
    !
    character(len=format_l) :: result
    !
    write(result,'('//fmt//')') value
    result(:) = adjustr(result)
  end function cubetools_format_right_integer4
  !
  function cubetools_format_right_integer8(value,fmt,format_l) result(result)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    integer(kind=8),  intent(in) :: value
    character(len=*), intent(in) :: fmt
    integer(kind=4),  intent(in) :: format_l
    !
    character(len=format_l) :: result
    !
    write(result,'('//fmt//')') value
    result(:) = adjustr(result)
  end function cubetools_format_right_integer8
  !
  function cubetools_format_right_string(value,fmt,format_l) result(result)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    character(len=*), intent(in) :: value
    character(len=*), intent(in) :: fmt
    integer(kind=4),  intent(in) :: format_l
    !
    character(len=format_l) :: result
    !
    write(result,'('//fmt//')') trim(value)
    result(:) = adjustr(result)
  end function cubetools_format_right_string
  !
  function cubetools_format_right_logical(value,fmt,format_l) result(result)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    logical,          intent(in) :: value
    character(len=*), intent(in) :: fmt
    integer(kind=4),  intent(in) :: format_l
    !
    character(len=format_l) :: result
    !
    write(result,'('//fmt//')') trim(logi2yesno(value))
    result(:) = adjustr(result)
  contains
    function logi2yesno(logi) result(yesno)
      logical, intent(in) :: logi
      character(len=3)    :: yesno
      !
      if (logi) then
         yesno = 'Yes'
      else
         yesno = 'No'
      endif
    end function logi2yesno
  end function cubetools_format_right_logical
  !
  !----------------------------------------------------------------------
  !
  function cubetools_format_unit(unit) result(result)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    character(len=*), intent(in) :: unit
    !
    character(len=unit_l) :: result
    !
    write(result,'(a)') unit
    result(:) = adjustr(result)
  end function cubetools_format_unit
  !
  !----------------------------------------------------------------------
  !
  function cubetools_format_diff_real8(value1,value2,fmt,format_l) result(result)
    use gbl_ansicodes
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    real(kind=8),     intent(in) :: value1
    real(kind=8),     intent(in) :: value2
    character(len=*), intent(in) :: fmt
    integer(kind=4),  intent(in) :: format_l
    !
    real(kind=8) :: reldiff
    integer(kind=4) :: length
    character(len=:), allocatable :: result
    !
    reldiff = 2d2*abs((value1-value2)/(value1+value2))
    length = bold_l+2*format_l+2*relative_l+5
    allocate(character(length)::result)
    write(result,'(a,a,1x,a,12x,a,"%",a)') c_bold,&
         cubetools_format_right(value1,fmt,format_l),&
         cubetools_format_right(value2,fmt,format_l),&
         cubetools_format_right(reldiff,fmtrd,relative_l),&
         c_clear
  end function cubetools_format_diff_real8
  !
  function cubetools_format_diff_real8_unit(value1,value2,fmt,format_l,unit) result(result)
    use gbl_ansicodes
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    real(kind=8),     intent(in) :: value1
    real(kind=8),     intent(in) :: value2
    character(len=*), intent(in) :: fmt
    integer(kind=4),  intent(in) :: format_l
    character(len=*), intent(in) :: unit
    !
    real(kind=8) :: reldiff
    integer(kind=4) :: length
    character(len=:), allocatable :: result
    !
    reldiff = 2d2*abs((value1-value2)/(value1+value2))
    length = bold_l+2*format_l+relative_l+unit_l+4
    allocate(character(length)::result)
    write(result,'(a,a,1x,a,a,a,"%",a)') c_bold,&
         cubetools_format_right(value1,fmt,format_l),&
         cubetools_format_right(value2,fmt,format_l),&
         cubetools_format_unit(unit),&
         cubetools_format_right(reldiff,fmtrd,relative_l),&
         c_clear
  end function cubetools_format_diff_real8_unit
  !
  function cubetools_format_2values_real8(value1,value2,fmt,format_l) result(result)
    use gbl_ansicodes
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    real(kind=8),     intent(in) :: value1
    real(kind=8),     intent(in) :: value2
    character(len=*), intent(in) :: fmt
    integer(kind=4),  intent(in) :: format_l
    !
    integer(kind=4) :: length
    character(len=:), allocatable :: result
    !
    length = bold_l+2*format_l+relative_l+4
    allocate(character(length)::result)
    write(result,'(a,a,1x,a,a)') c_bold,&
         cubetools_format_right(value1,fmt,format_l),&
         cubetools_format_right(value2,fmt,format_l),&
         c_clear
  end function cubetools_format_2values_real8
  !
  function cubetools_format_2values_real8_unit(value1,value2,fmt,format_l,unit) result(result)
    use gbl_ansicodes
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    real(kind=8),     intent(in) :: value1
    real(kind=8),     intent(in) :: value2
    character(len=*), intent(in) :: fmt
    integer(kind=4),  intent(in) :: format_l
    character(len=*), intent(in) :: unit
    !
    integer(kind=4) :: length
    character(len=:), allocatable :: result
    !
    length = bold_l+2*format_l+unit_l+relative_l+4
    allocate(character(length)::result)
    write(result,'(a,a,1x,a,a,a)') c_bold,&
         cubetools_format_right(value1,fmt,format_l),&
         cubetools_format_right(value2,fmt,format_l),&
         cubetools_format_unit(unit),&
         c_clear
  end function cubetools_format_2values_real8_unit
  !
  function cubetools_format_2values_2units_real8(value1,unit1,value2,unit2,fmt,format_l) result(result)
    use gbl_ansicodes
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    real(kind=8),     intent(in) :: value1
    character(len=*), intent(in) :: unit1
    real(kind=8),     intent(in) :: value2
    character(len=*), intent(in) :: unit2
    character(len=*), intent(in) :: fmt
    integer(kind=4),  intent(in) :: format_l
    !
    character(len=:), allocatable :: result
    integer(kind=4) :: length
    length = bold_l+2*format_l+2*unit_l+4
    !
    allocate(character(length)::result)
    write(result,'(6a)') c_bold,&
         cubetools_format_right(value1,fmt,format_l),cubetools_format_unit(unit1),&
         cubetools_format_right(value2,fmt,format_l),cubetools_format_unit(unit2),&
         c_clear
  end function cubetools_format_2values_2units_real8
  !
  !----------------------------------------------------------------------
  !
  function cubetools_format_diff_real4(value1,value2,fmt,format_l) result(result)
    use gbl_ansicodes
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    real(kind=4),     intent(in) :: value1
    real(kind=4),     intent(in) :: value2
    character(len=*), intent(in) :: fmt
    integer(kind=4),  intent(in) :: format_l
    !
    character(len=:), allocatable :: result
    integer(kind=4) :: length
    real(kind=4) :: reldiff
    length = bold_l+2*format_l+2*relative_l+5
    reldiff = 2d2*abs((value1-value2)/(value1+value2))
    !
    allocate(character(length)::result)
    write(result,'(a,a,1x,a,12x,a,"%",a)') c_bold,&
         cubetools_format_right(value1,fmt,format_l),&
         cubetools_format_right(value2,fmt,format_l),&
         cubetools_format_right(reldiff,fmtrd,relative_l),&
         c_clear
  end function cubetools_format_diff_real4
  !
  function cubetools_format_diff_real4_unit(value1,value2,fmt,format_l,unit) result(result)
    use gbl_ansicodes
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    real(kind=4),     intent(in) :: value1
    real(kind=4),     intent(in) :: value2
    character(len=*), intent(in) :: fmt
    integer(kind=4),  intent(in) :: format_l
    character(len=*), intent(in) :: unit
    !
    real(kind=4) :: reldiff
    integer(kind=4) :: length
    character(len=:), allocatable :: result
    !
    reldiff = 2d2*abs((value1-value2)/(value1+value2))
    length = bold_l+2*format_l+relative_l+unit_l+4
    allocate(character(length)::result)
    write(result,'(a,a,1x,a,a,a,"%",a)') c_bold,&
         cubetools_format_right(value1,fmt,format_l),&
         cubetools_format_right(value2,fmt,format_l),&
         cubetools_format_unit(unit),&
         cubetools_format_right(reldiff,fmtrd,relative_l),&
         c_clear
  end function cubetools_format_diff_real4_unit
  !
  function cubetools_format_2values_real4(value1,value2,fmt,format_l) result(result)
    use gbl_ansicodes
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    real(kind=4),     intent(in) :: value1
    real(kind=4),     intent(in) :: value2
    character(len=*), intent(in) :: fmt
    integer(kind=4),  intent(in) :: format_l
    !
    integer(kind=4) :: length
    character(len=:), allocatable :: result
    !
    length = bold_l+2*format_l+relative_l+4
    allocate(character(length)::result)
    write(result,'(a,a,1x,a,a)') c_bold,&
         cubetools_format_right(value1,fmt,format_l),&
         cubetools_format_right(value2,fmt,format_l),&
         c_clear
  end function cubetools_format_2values_real4
  !
  function cubetools_format_2values_real4_unit(value1,value2,fmt,format_l,unit) result(result)
    use gbl_ansicodes
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    real(kind=4),     intent(in) :: value1
    real(kind=4),     intent(in) :: value2
    character(len=*), intent(in) :: fmt
    integer(kind=4),  intent(in) :: format_l
    character(len=*), intent(in) :: unit
    !
    integer(kind=4) :: length
    character(len=:), allocatable :: result
    !
    length = bold_l+2*format_l+unit_l+relative_l+4
    allocate(character(length)::result)
    write(result,'(a,a,1x,a,a,a)') c_bold,&
         cubetools_format_right(value1,fmt,format_l),&
         cubetools_format_right(value2,fmt,format_l),&
         cubetools_format_unit(unit),&
         c_clear
  end function cubetools_format_2values_real4_unit
  !
  function cubetools_format_2values_2units_real4(value1,unit1,value2,unit2,fmt,format_l) result(result)
    use gbl_ansicodes
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    real(kind=4),     intent(in) :: value1
    character(len=*), intent(in) :: unit1
    real(kind=4),     intent(in) :: value2
    character(len=*), intent(in) :: unit2
    character(len=*), intent(in) :: fmt
    integer(kind=4),  intent(in) :: format_l
    !
    integer(kind=4) :: length
    character(len=:), allocatable :: result
    !
    length = bold_l+2*format_l+2*unit_l+4
    allocate(character(length)::result)
    write(result,'(6a)') c_bold,&
         cubetools_format_right(value1,fmt,format_l),cubetools_format_unit(unit1),&
         cubetools_format_right(value2,fmt,format_l),cubetools_format_unit(unit2),&
         c_clear
  end function cubetools_format_2values_2units_real4
  !
  !----------------------------------------------------------------------
  !
  function cubetools_format_diff_integer4(value1,value2,fmt,format_l) result(result)
    use gbl_ansicodes
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    integer(kind=4),  intent(in) :: value1
    integer(kind=4),  intent(in) :: value2
    character(len=*), intent(in) :: fmt
    integer(kind=4),  intent(in) :: format_l
    !
    real(kind=4) :: reldiff
    integer(kind=4) :: length
    character(len=:), allocatable :: result
    !
    reldiff = abs(2e2*(value1-value2)/(value1+value2))
    length = bold_l+2*format_l+2*relative_l+4
    allocate(character(length)::result)
    write(result,'(a,a,1x,a,12x,a,"%",a)') c_bold,&
         cubetools_format_right(value1,fmt,format_l),&
         cubetools_format_right(value2,fmt,format_l),&
         cubetools_format_right(reldiff,fmtrd,relative_l),&
         c_clear
  end function cubetools_format_diff_integer4
  !
  function cubetools_format_2values_integer4(value1,value2,fmt,format_l) result(result)
    use gbl_ansicodes
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    integer(kind=4),  intent(in) :: value1
    integer(kind=4),  intent(in) :: value2
    character(len=*), intent(in) :: fmt
    integer(kind=4),  intent(in) :: format_l
    !
    integer(kind=4) :: length
    character(len=:), allocatable :: result
    !
    length = bold_l+2*format_l+relative_l+4
    allocate(character(length)::result)
    write(result,'(a,a,1x,a,a)') c_bold,&
         cubetools_format_right(value1,fmt,format_l),&
         cubetools_format_right(value2,fmt,format_l),&
         c_clear
  end function cubetools_format_2values_integer4
  !
  !----------------------------------------------------------------------
  !
  function cubetools_format_diff_integer8(value1,value2,fmt,format_l) result(result)
    use gbl_ansicodes
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    integer(kind=8),  intent(in) :: value1
    integer(kind=8),  intent(in) :: value2
    character(len=*), intent(in) :: fmt
    integer(kind=4),  intent(in) :: format_l
    !
    real(kind=4) :: reldiff
    integer(kind=4) :: length
    character(len=:), allocatable :: result
    !
    reldiff = abs(2e2*(value1-value2)/(value1+value2))
    length = bold_l+2*format_l+2*relative_l+4
    allocate(character(length)::result)
    write(result,'(a,a,1x,a,12x,a,"%",a)') c_bold,&
         cubetools_format_right(value1,fmt,format_l),&
         cubetools_format_right(value2,fmt,format_l),&
         cubetools_format_right(reldiff,fmtrd,relative_l),&
         c_clear
  end function cubetools_format_diff_integer8
  !
  function cubetools_format_2values_integer8(value1,value2,fmt,format_l) result(result)
    use gbl_ansicodes
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    integer(kind=8),  intent(in) :: value1
    integer(kind=8),  intent(in) :: value2
    character(len=*), intent(in) :: fmt
    integer(kind=4),  intent(in) :: format_l
    !
    integer(kind=4) :: length
    character(len=:), allocatable :: result
    !
    length = bold_l+2*format_l+relative_l+4
    allocate(character(length)::result)
    write(result,'(a,a,1x,a,a)') c_bold,&
         cubetools_format_right(value1,fmt,format_l),&
         cubetools_format_right(value2,fmt,format_l),&
         c_clear
  end function cubetools_format_2values_integer8
  !
  !----------------------------------------------------------------------
  !
  function cubetools_format_2values_string(value1,value2,format_l) result(result)
    use gbl_ansicodes
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    character(len=*), intent(in) :: value1
    character(len=*), intent(in) :: value2
    integer(kind=4),  intent(in) :: format_l
    !
    integer(kind=4) :: length
    character(len=:), allocatable :: result
    !
    length = bold_l+2*format_l+relative_l+4
    allocate(character(length)::result)
    write(result,'(a,a,1x,a,a)') c_bold,&
         cubetools_format_right(value1,'a',format_l),&
         cubetools_format_right(value2,'a',format_l),&
         c_clear
  end function cubetools_format_2values_string
  !
  !----------------------------------------------------------------------
  !
  function cubetools_format_2values_logical(value1,value2,format_l) result(result)
    use gbl_ansicodes
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    logical,          intent(in) :: value1
    logical,          intent(in) :: value2
    integer(kind=4),  intent(in) :: format_l
    !
    integer(kind=4) :: length
    character(len=:), allocatable :: result
    !
    length = bold_l+2*format_l+relative_l+4
    allocate(character(length)::result)
    write(result,'(a,a,1x,a,a)') c_bold,&
         cubetools_format_right(value1,'a',format_l),&
         cubetools_format_right(value2,'a',format_l),&
         c_clear
  end function cubetools_format_2values_logical
  !
  !----------------------------------------------------------------------
  !
  function cubetools_format_time(duration) result(result)
    !----------------------------------------------------------
    !
    !----------------------------------------------------------
    character(len=10)        :: result
    real(kind=8), intent(in) :: duration ! [sec]
    !
    integer(kind=4) :: iunit,found
    integer(kind=4), parameter :: munits=7
    character(len=3), parameter :: units(munits) =  &
      (/ 'yr ','mth','wk ','day','hr ','min','sec' /)
    real(kind=8), parameter :: durations(munits) =  &
      (/ 365.25d0*24*60*60, 365.25d0/12*24*60*60, 7*24*60*60.d0,  &
         24*60*60.d0, 60*60.d0, 60.d0, 1.d0 /)
    character(len=*), parameter :: rname='FORMAT>TIME'
    !
    found = munits  ! In seconds if < 1sec
    do iunit=1,munits
      if (duration.gt.durations(iunit)) then
        found = iunit
        exit
      endif
    enddo
    write(result,'(F6.2,1X,A3)')  duration/durations(found),units(found)
  end function cubetools_format_time
  !
  !----------------------------------------------------------------------
  !
  subroutine cubetools_format_range(name,range,incr,unit,string,error)
    use gbl_ansicodes
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*),  intent(in)    :: name
    real(kind=coor_k), intent(in)    :: range(2)
    real(kind=coor_k), intent(in)    :: incr
    character(len=*),  intent(in)    :: unit
    character(len=*),  intent(inout) :: string
    logical,           intent(inout) :: error
    !
    write(string,'(11x,a,a,a,a,a,a,a,a,a,a,x,a)') 'range = [',c_bold,&
         cubetools_format_right(range(1),fsimple,nsimple),&
         c_clear,' ',c_bold,&
         cubetools_format_right(range(2),fsimple,nsimple),&
         c_clear,'] by ',&
         cubetools_format_right(incr,    fsimple,nsimple),&
         trim(unit)
    string(1:10) = name(1:len_trim(name))
  end subroutine cubetools_format_range
  !
  subroutine cubetools_format_offset(offset,unit,string,error)
    use gbl_ansicodes
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    real(kind=coor_k), intent(in)    :: offset(2)
    character(len=*),  intent(in)    :: unit
    character(len=*),  intent(inout) :: string
    logical,           intent(inout) :: error
    !
    write(string,'(a,a,a,a,a,a,a,a,a,x,a)') 'Offset = (',c_bold,&
         cubetools_format_right(offset(1),fsimple,nsimple),&
         c_clear,' ',c_bold,&
         cubetools_format_right(offset(2),fsimple,nsimple),&
         c_clear,')',&
         trim(unit)
  end subroutine cubetools_format_offset
  !
  subroutine cubetools_format_radec(absolute,string,error)
    use gkernel_interfaces, only: rad2sexa
    !----------------------------------------------------------------------
    ! absolute is assumed to be in internal angle unit, ie, radian
    !----------------------------------------------------------------------
    real(kind=coor_k), intent(in)    :: absolute(2) ! [rad]
    character(len=*),  intent(inout) :: string
    logical,           intent(inout) :: error
    !
    character(len=15) :: sexara,sexadec
    !
    call rad2sexa(absolute(1),24,sexara)
    call rad2sexa(absolute(2),360,sexadec)
    string = cubetools_format_stdkey_boldval('RA',sexara,22)
    string = trim(string)//'  '//cubetools_format_stdkey_boldval('Dec',sexadec,22)
  end subroutine cubetools_format_radec
  !
  function cubetools_format_size_center(xsize,ysize,xcenter,ycenter) result(result)
    use gbl_ansicodes
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    real(kind=coor_k), intent(in) :: xsize
    real(kind=coor_k), intent(in) :: ysize
    real(kind=coor_k), intent(in) :: xcenter
    real(kind=coor_k), intent(in) :: ycenter
    !
    character(len=mess_l) :: string
    character(len=:), allocatable :: result
    !
    write(string,'(17a)') 'Size and center  = [',c_bold,&
         cubetools_format_right(xsize,fsimple,nsimple),&
         c_clear,' ',c_bold,&
         cubetools_format_right(ysize,fsimple,nsimple),&
         c_clear,'] at (',c_bold,&
         cubetools_format_right(xcenter,fsimple,nsimple),&
         c_clear,' ',c_bold,&
         cubetools_format_right(ycenter,fsimple,nsimple),&
         c_clear,')'
    allocate(character(len_trim(string))::result)
    result = string
  end function cubetools_format_size_center
end module cubetools_format
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
