!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_useriter_types
  use cubetools_structure
  use cubetools_messaging
  !
  public :: useriter_t
  private
  !
  integer(kind=4), parameter :: izero=1
  integer(kind=4), parameter :: ifirst=2
  integer(kind=4), parameter :: ilast=3
  integer(kind=4), parameter :: iafterlast=4
  integer(kind=4), parameter :: iprevious=5
  integer(kind=4), parameter :: icurrent=6
  integer(kind=4), parameter :: inext=7
  integer(kind=4), parameter :: nkey = inext
  !
  character(len=*), parameter :: keys(nkey) = &
       ['ZERO     ','FIRST    ','LAST     ','AFTERLAST','PREVIOUS ','CURRENT  ','NEXT     ']
  !
  type useriter_opt_t
     integer(kind=4) :: n = nkey
     type(option_t)  :: opt(1:nkey)
   contains
     procedure, public :: register => cubetools_useriter_register
     procedure, public :: parse    => cubetools_useriter_parse
  end type useriter_opt_t
  !
contains
  !
  subroutine cubetools_useriter_opt_register(iter,preabs,postabs,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    class(useriter_opt_t), intent(inout) :: iter
    character(len=*),      intent(in)    :: preabs
    character(len=*),      intent(in)    :: postabs
    logical,               intent(inout) :: error
    !
    integer(kind=4) :: iopt
    character(len=*), parameter :: rname='USERITER>OPT>REGISTER'
    !
    call cubetools_message(edseve%trace,rname,'Welcome')
    !
    do iopt=1,opt%n
       call cubetools_register_option(&
            keys(iopt),'',&
            preabs//trim(keys(iopt))//postabs,&
            strg_id,&
            iter%opt(iopt),&
            error)
       if (error) return
    enddo ! iopt
  end subroutine cubetools_useriter_opt_register
  !
  subroutine cubetools_useriter_opt_parse(opt,error)
    !-------------------------------------------------------------------
    ! 
    !-------------------------------------------------------------------
    class(useriter_opt_t), intent(inout) :: opt
    logical,               intent(inout) :: error
    !
    integer(kind=4) :: iopt
    character(len=*), parameter :: rname='USERITER>OPT>PARSE'
    !
    call cubetools_message(edseve%trace,rname,'Welcome')
    !
    do iopt=1,opt%n
       call iter%opt(iopt)%present(line,user%opt(iopt)%do,error)
       if (error) return
    enddo ! iopt
  end subroutine cubetools_useriter_opt_parse
end module cubetools_useriter_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
