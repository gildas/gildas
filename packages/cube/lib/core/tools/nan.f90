!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_nan
  use ieee_arithmetic 
  ! --- ieee_values ----------------------------------------------------
  complex(kind=4), protected :: gc4nan
  real(kind=4),    protected :: gr4nan
  real(kind=8),    protected :: gr8nan
  !
contains
  !
  subroutine cubetools_nan_init()
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    gr4nan = ieee_value(0.,ieee_quiet_nan)
    gr8nan = ieee_value(0.d0,ieee_quiet_nan)
    gc4nan = cmplx(gr4nan,gr4nan)
  end subroutine cubetools_nan_init
end module cubetools_nan
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
