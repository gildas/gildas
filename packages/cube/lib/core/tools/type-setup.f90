!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubetools_setup_types
  use cubetools_parameters
  use cubetools_datasize
  !
  ! Support for SETUP\BLANKING
  type :: cube_setup_blanking_t
    integer(kind=4) :: rmode = code_patchblank_otf  ! Read mode
  end type cube_setup_blanking_t
  !
  ! Support for SETUP\BUFFER
  type :: cube_setup_buffer_t
    integer(kind=code_k) :: input  = code_buffer_memory
    integer(kind=code_k) :: output = code_buffer_memory
    real(kind=size_k)    :: limit  =  2.0*GiB  ! [Bytes]
    real(kind=size_k)    :: block  = 512.*MiB  ! [Bytes] Default block size (disk mode)
    real(kind=size_k)    :: task   = 256.*kiB  ! [Bytes] Default task size (1 plane of 250x250 pixels)
  end type cube_setup_buffer_t
  !
  ! Support for SETUP\OUTPUT (cubes customization)
  type :: cube_setup_output_t
    logical :: extrema = .true.  ! Compute and update the extrema section
    logical :: write = .true.    ! Write the file on disk?
  end type cube_setup_output_t
  !
  ! Support for SETUP\TIMING
  type :: cube_setup_timing_t
    logical :: io = .false.       ! IO timing feedback
    logical :: command = .false.  ! Command timing feedback
  end type cube_setup_timing_t
  !
  ! Support for SETUP\INDEX
  type :: cube_setup_index_t
    integer(kind=code_k) :: default=code_index_dag
  end type cube_setup_index_t
  !
  type :: cube_setup_t
    type(cube_setup_blanking_t) :: blanking
    type(cube_setup_buffer_t)   :: buff
    type(cube_setup_index_t)    :: index
    type(cube_setup_output_t)   :: output
    type(cube_setup_timing_t)   :: timing
  end type cube_setup_t
  !
end module cubetools_setup_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
