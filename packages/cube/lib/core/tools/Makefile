###########################################################################
#
# Makefile system for GILDAS softwares (2003-2024).
#
# Please be careful: element order often matters in makefiles.
#
###########################################################################

include $(gagadmdir)/Makefile.def

###########################################################################

ARRAY_OBJECTS = type-array.o

ARRAY_EXPORTS = cubetools_array_types.mod cubetools_strg_1d_types.mod	\
cubetools_inte_1d_types.mod cubetools_long_1d_types.mod			\
cubetools_real_1d_types.mod cubetools_dble_1d_types.mod			\
cubetools_cplx_1d_types.mod cubetools_logi_1d_types.mod			\
cubetools_inte_2d_types.mod cubetools_long_2d_types.mod			\
cubetools_real_2d_types.mod cubetools_dble_2d_types.mod			\
cubetools_cplx_2d_types.mod cubetools_real_3d_types.mod			\
cubetools_cplx_3d_types.mod

###########################################################################

TYPE_OBJECTS = type-list.o type-realpath.o type-setup.o

TYPE_EXPORTS = cubetools_list.mod cubetools_realpath.mod	\
cubetools_setup_types.mod

###########################################################################

TOOL_OBJECTS = tool-terminal.o

TOOL_EXPORTS = cubetools_terminal_tool.mod

###########################################################################

LIB_IDENTITY = cubetools

LIB_C_OBJECTS = tool-sha1sum.o

LIB_F_OBJECTS = $(ARRAY_OBJECTS) $(TOOL_OBJECTS) $(TYPE_OBJECTS) ascii.o	\
brightness.o cmessaging.o convert.o data-size.o disambiguate.o format.o		\
message.o nan.o parameters.o string.o tool-checksum.o

LIB_EXPORTS = $(ARRAY_EXPORTS) $(TOOL_EXPORTS) $(TYPE_EXPORTS)		\
cubetools_ascii.mod cubetools_brightness.mod cubetools_checksum.mod	\
cubetools_cmessaging.mod cubetools_convert.mod cubetools_datasize.mod	\
cubetools_disambiguate.mod cubetools_format.mod				\
cubetools_messaging.mod cubetools_nan.mod cubetools_parameters.mod	\
cubetools_string.mod

INTERFACES_DISABLE = yes

LIB_DEPENDS = $(GREG_LIBS)

ADD_LIBS += $(OPENSSL_LDFLAGS)

LOCAL_CFLAGS += $(OPENSSL_CFLAGS)

###########################################################################

include $(gagadmdir)/Makefile.lib

###########################################################################

include $(builddir)/Makefile.deps

###########################################################################
