!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage CUBE messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubeobsolete_messaging
  use gpack_def
  use gbl_message
  use cubetools_parameters
  !
  public :: obsoleteseve,seve,mess_l
  public :: cubeobsolete_message_set_id,cubeobsolete_message
  public :: cubeobsolete_message_set_alloc,cubeobsolete_message_get_alloc
  public :: cubeobsolete_message_set_trace,cubeobsolete_message_get_trace
  public :: cubeobsolete_message_set_others,cubeobsolete_message_get_others
  private
  !
  ! Identifier used for message identification
  integer(kind=4) :: cubeobsolete_message_id = gpack_global_id  ! Default value for startup message
  !
  type :: cubeobsolete_messaging_debug_t
     integer(kind=code_k) :: alloc = seve%d
     integer(kind=code_k) :: trace = seve%t
     integer(kind=code_k) :: others = seve%d
  end type cubeobsolete_messaging_debug_t
  !
  type(cubeobsolete_messaging_debug_t) :: obsoleteseve
  !
contains
  !
  subroutine cubeobsolete_message_set_id(id)
    !---------------------------------------------------------------------
    ! Alter library id into input id. Should be called by the library
    ! which wants to share its id with the current one.
    !---------------------------------------------------------------------
    integer(kind=4), intent(in) :: id
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='MESSAGE>SET>ID'
    !
    cubeobsolete_message_id = id
    write (mess,'(A,I0)') 'Now use id #',cubeobsolete_message_id
    call cubeobsolete_message(seve%d,rname,mess)
  end subroutine cubeobsolete_message_set_id
  !
  subroutine cubeobsolete_message(mkind,procname,message)
    use cubetools_cmessaging
    !---------------------------------------------------------------------
    ! Messaging facility for the current library. Calls the low-level
    ! (internal) messaging routine with its own identifier.
    !---------------------------------------------------------------------
    integer(kind=4),  intent(in) :: mkind     ! Message kind
    character(len=*), intent(in) :: procname  ! Name of calling procedure
    character(len=*), intent(in) :: message   ! Message string
    !
    call cubetools_cmessage(cubeobsolete_message_id,mkind,'OBSOLETE>'//procname,message)
  end subroutine cubeobsolete_message
  !
  subroutine cubeobsolete_message_set_alloc(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       obsoleteseve%alloc = seve%i
    else
       obsoleteseve%alloc = seve%d
    endif
  end subroutine cubeobsolete_message_set_alloc
  !
  subroutine cubeobsolete_message_set_trace(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       obsoleteseve%trace = seve%i
    else
       obsoleteseve%trace = seve%t
    endif
  end subroutine cubeobsolete_message_set_trace
  !
  subroutine cubeobsolete_message_set_others(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       obsoleteseve%others = seve%i
    else
       obsoleteseve%others = seve%d
    endif
  end subroutine cubeobsolete_message_set_others
  !
  function cubeobsolete_message_get_alloc()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubeobsolete_message_get_alloc
    !
    cubeobsolete_message_get_alloc = obsoleteseve%alloc.eq.seve%i
    !
  end function cubeobsolete_message_get_alloc
  !
  function cubeobsolete_message_get_trace()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubeobsolete_message_get_trace
    !
    cubeobsolete_message_get_trace = obsoleteseve%trace.eq.seve%i
    !
  end function cubeobsolete_message_get_trace
  !
  function cubeobsolete_message_get_others()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubeobsolete_message_get_others
    !
    cubeobsolete_message_get_others = obsoleteseve%others.eq.seve%i
    !
  end function cubeobsolete_message_get_others
end module cubeobsolete_messaging
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
