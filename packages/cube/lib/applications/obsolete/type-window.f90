!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_window_types
  use cubeadm_spectrum_types
  !
  public :: window_t
  private
  !
  type, extends(spectrum_t) :: window_t
   contains
     procedure, public :: isblank => cubemain_window_isblank
  end type window_t
  !
contains
  !
  function cubemain_window_isblank(wind,iw) result(isblank)
    use cubetools_parameters
    use cubetools_nan
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(window_t),      intent(in) :: wind
    integer(kind=wind_k), intent(in) :: iw
    logical                          :: isblank
    !
    isblank = ieee_is_nan(wind%y%val(2*iw-1)).or.ieee_is_nan(wind%y%val(2*iw))
  end function cubemain_window_isblank
end module cubemain_window_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
