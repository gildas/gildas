!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! *** JP *** This one is obsolescent. Please use type-ancillary-cube.f90 or
! *** JP *** one of its child, eg,  type-ancillary-mask.f90
!
module cubemain_auxiliary
  use cubetools_parameters
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeobsolete_messaging
  !
  public :: auxiliary_user_t
  public :: cubemain_auxiliary_register,cubemain_auxiliary_parse,cubemain_auxiliary_user2prog
  private
  !
  type auxiliary_user_t
     logical             :: do =.false.
     type(cubeid_user_t) :: id
  end type auxiliary_user_t
  !
contains
  !
  subroutine cubemain_auxiliary_register(name,abstract,help,option,  &
      argabs,flags,status,access,pcube,error)
    use cubedag_flag
    !----------------------------------------------------------------------
    ! Register a /SUPPLEMENT option according to a name, help and abstract
    ! provided by the command
    !----------------------------------------------------------------------
    ! Command description:
    character(len=*),        intent(in)    :: name
    character(len=*),        intent(in)    :: abstract
    character(len=*),        intent(in)    :: help
    type(option_t), pointer, intent(out)   :: option
    ! Cube argument description:
    character(len=*),        intent(in)    :: argabs
    type(flag_t),            intent(in)    :: flags(:)
    integer(kind=code_k),    intent(in)    :: status
    integer(kind=code_k),    intent(in)    :: access
    type(cubeid_arg_t),      pointer       :: pcube
    logical,                 intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    integer(kind=code_k) :: action
    character(len=*), parameter :: rname='AUXILIARY>REGISTER'
    !
    call cubeobsolete_message(obsoleteseve%trace,rname,'Welcome')
    !
    select case(name)
    case("INITIAL")
       action = code_read
    case("LIKE")
       action = code_read_head
    case default
       call cubeobsolete_message(seve%e,rname,"Don't know what to do if option is not /LIKE or /INITIAL")
       error = .true.
       return
    end select
    call cubetools_register_option(&
         name,'cube',&
         abstract,&
         help,&
         option,error)
    if (error) return
    call cubearg%register( &
         'CUBE', &
         argabs,  &
         strg_id,&
         status,  &
         flags, &
         action, &
         access, &
         pcube,error)
    if (error) return
  end subroutine cubemain_auxiliary_register
  !
  subroutine cubemain_auxiliary_parse(line,opt,user,error)
    !----------------------------------------------------------------------
    ! /SUPPLEMENT cub
    !----------------------------------------------------------------------
    character(len=*),       intent(in)    :: line
    type(option_t),         intent(in)    :: opt
    type(auxiliary_user_t), intent(out)   :: user
    logical,                intent(inout) :: error
    !
    character(len=*),parameter :: rname='AUXILIARY>PARSE'
    !
    call cubeobsolete_message(obsoleteseve%trace,rname,'Welcome')
    !
    call opt%present(line,user%do,error)
    if (error) return
    if (user%do) then
       call cubeadm_cubeid_parse(line,opt,user%id,error)
       if (error) return
    endif
  end subroutine cubemain_auxiliary_parse
  !
  subroutine cubemain_auxiliary_user2prog(cubearg,user,oucube,error)
    use cube_types
    use cubeadm_get
    !----------------------------------------------------------------------
    ! Resolve cubeid into a cube
    !----------------------------------------------------------------------
    type(cubeid_arg_t),     intent(in)    :: cubearg
    type(auxiliary_user_t), intent(in)    :: user
    type(cube_t), pointer,  intent(out)   :: oucube
    logical,                intent(inout) :: error
    !
    character(len=*),parameter :: rname='AUXILIARY>USER2PROG'
    !
    call cubeobsolete_message(obsoleteseve%trace,rname,'Welcome')
    !
    if (user%do) then
       call cubeadm_get_header(cubearg,user%id,oucube,error)
       if (error) return
    endif
  end subroutine cubemain_auxiliary_user2prog
end module cubemain_auxiliary
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
