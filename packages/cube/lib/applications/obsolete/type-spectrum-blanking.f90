!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_spectrum_blanking
  use cubetools_parameters
  use cubetools_nan
  use cubeobsolete_messaging
  use cubemain_spectrum_real
  !
  public :: cubemain_spectrum_blank
  public :: cubemain_spectrum_unblank
  public :: cubemain_spectrum_blank2zero
  public :: cubemain_spectrum_reblank
  public :: cubemain_spectrum_unmask
  private
  !
contains
  !
  function cubemain_spectrum_blank(spec)
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    logical                      :: cubemain_spectrum_blank
    type(spectrum_t), intent(in) :: spec
    !
    integer(kind=entr_k) :: ic
    !
    cubemain_spectrum_blank = .true.
    do ic = 1, spec%n
       if (.not.ieee_is_nan(spec%t(ic))) then
          cubemain_spectrum_blank = .false.
          return
       endif
    enddo
  end function cubemain_spectrum_blank
  !
  subroutine cubemain_spectrum_unblank(specin,specout,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(spectrum_t), intent(in)    :: specin
    type(spectrum_t), intent(inout) :: specout
    logical,          intent(inout) :: error
    !
    integer(kind=chan_k) :: ichan,jchan
    character(len=*), parameter :: rname='SPECTRUM>UNBLANK'
    !
    call cubeobsolete_message(obsoleteseve%trace,rname,'Welcome')
    !
    specout%ref = specin%ref
    specout%val = specin%val
    specout%inc = specin%inc
    specout%noi = specin%noi
    !
    jchan = 1
    do ichan=1,specin%n
       if (.not.ieee_is_nan(specin%t(ichan))) then
          specout%c(jchan) = specin%c(ichan)
          specout%v(jchan) = specin%v(ichan)
          specout%f(jchan) = specin%f(ichan)
          specout%w(jchan) = specin%w(ichan)
          specout%t(jchan) = specin%t(ichan)
          jchan = jchan+1
       endif
    enddo ! ichan
    do ichan=jchan,specout%m
       specout%c(jchan) = 0
       specout%v(jchan) = 0
       specout%f(jchan) = 0
       specout%w(jchan) = 0
       specout%t(jchan) = gr4nan
    enddo ! ichan
    specout%n = jchan-1
  end subroutine cubemain_spectrum_unblank
  !
  subroutine cubemain_spectrum_blank2zero(specin,specout,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(spectrum_t), intent(in)    :: specin
    type(spectrum_t), intent(inout) :: specout
    logical,          intent(inout) :: error
    !
    integer(kind=chan_k) :: ic
    character(len=*), parameter :: rname='SPECTRUM>BLANK2ZERO'
    !
    call cubeobsolete_message(obsoleteseve%trace,rname,'Welcome')
    !
    specout%noi = specin%noi
    specout%n = specin%n
    specout%c = specin%c
    specout%v = specin%v
    specout%f = specin%f
    !
    do ic=1,specin%n
       if (ieee_is_nan(specin%t(ic))) then
          specout%t(ic) = 0
       else
          specout%t(ic) = specin%t(ic)
       endif
    enddo ! ic
  end subroutine cubemain_spectrum_blank2zero
  !
  subroutine cubemain_spectrum_reblank(specin,specout,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(spectrum_t), intent(in)    :: specin
    type(spectrum_t), intent(inout) :: specout
    logical,          intent(inout) :: error
    !
    integer(kind=chan_k) :: ic
    character(len=*), parameter :: rname='SPECTRUM>REBLANK'
    !
    call cubeobsolete_message(obsoleteseve%trace,rname,'Welcome')
    !
    do ic=1,specin%n
       if (ieee_is_nan(specin%t(ic))) then
          specout%t(ic) = gr4nan
       endif
    enddo ! ic
  end subroutine cubemain_spectrum_reblank
  !
  subroutine cubemain_spectrum_unmask(specin,specout,error)
    !----------------------------------------------------------------------
    ! specout is a copy of specin channels that are not blanked or masked
    !----------------------------------------------------------------------
    type(spectrum_t), intent(in)    :: specin
    type(spectrum_t), intent(inout) :: specout
    logical,          intent(inout) :: error
    !
    integer(kind=chan_k) :: ichan,jchan
    character(len=*), parameter :: rname='SPECTRUM>UNMASK'
    !
    call cubeobsolete_message(obsoleteseve%trace,rname,'Welcome')
    !
    specout%noi = specin%noi
    !
    specout%c = 0
    specout%v = 0
    specout%f = 0
    specout%w = 0
    specout%t = gr4nan
    !
    jchan = 1
    do ichan=1,specin%n
       if ((.not.ieee_is_nan(specin%t(ichan))).and.(specin%w(ichan).le.0)) then
          specout%c(jchan) = specin%c(ichan)
          specout%v(jchan) = specin%v(ichan)
          specout%f(jchan) = specin%f(ichan)
          specout%w(jchan) = 1.0
          specout%t(jchan) = specin%t(ichan)
          jchan = jchan+1
       endif
    enddo ! ichan
    specout%n = jchan-1
  end subroutine cubemain_spectrum_unmask
end module cubemain_spectrum_blanking
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
