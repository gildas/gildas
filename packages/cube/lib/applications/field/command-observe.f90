!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubefield_observe
  use cube_types
  use cubetools_parameters
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubefield_messaging
  ! 
  public :: observe
  private
  !
  type :: observe_comm_t
     type(option_t),     pointer :: comm
     type(cubeid_arg_t), pointer :: voldens
     type(cubeid_arg_t), pointer :: vx
     type(cubeid_arg_t), pointer :: vy
     type(cubeid_arg_t), pointer :: vz
     type(cube_prod_t),  pointer :: coldens
     type(cube_prod_t),  pointer :: vcentroid
   contains
     procedure, public  :: register => cubefield_observe_register
     procedure, private :: parse    => cubefield_observe_parse
     procedure, private :: main     => cubefield_observe_main
  end type observe_comm_t
  type(observe_comm_t) :: observe
  !
  type observe_user_t
     type(cubeid_user_t)   :: cubeids
     real(kind=8)          :: distance    = 400 ! [pc]
     real(kind=8)          :: inclination =  30 ! [deg]
   contains
     procedure, private :: toprog => cubefield_observe_user_toprog
  end type observe_user_t
  !
  type observe_prog_t
     type(cube_t), pointer :: voldens
     type(cube_t), pointer :: vx
     type(cube_t), pointer :: vy
     type(cube_t), pointer :: vz
     type(cube_t), pointer :: coldens
     type(cube_t), pointer :: vcentroid
     integer(kind=chan_k)  :: nz
     real(kind=8)          :: factor
     real(kind=8)          :: distance
     real(kind=8)          :: cosi
     real(kind=8)          :: sini
   contains
     procedure, private :: header => cubefield_observe_prog_header
     procedure, private :: data   => cubefield_observe_prog_data
     procedure, private :: loop   => cubefield_observe_prog_loop
     procedure, private :: act    => cubefield_observe_prog_act
  end type observe_prog_t
  !
contains
  !
  subroutine cubefield_observe_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(observe_user_t) :: user
    character(len=*), parameter :: rname='OBSERVE>COMMAND'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call observe%parse(line,user,error)
    if (error) return
    call observe%main(user,error)
    if (error) continue
  end subroutine cubefield_observe_command
  !
  subroutine cubefield_observe_register(observe,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(observe_comm_t), intent(inout) :: observe
    logical,               intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    type(cube_prod_t) :: oucube
    character(len=*), parameter :: comm_abstract='Compute column density and centroid velocity'
    character(len=*), parameter :: comm_help=&
         'It uses the 3D volume density and velocity fields&
         &and it assumes the optically thin limit'
    character(len=*), parameter :: rname='OBSERVE>REGISTER'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'OBSERVE','voldens vx vy vz',&
         comm_abstract,&
         comm_help,&
         cubefield_observe_command,&
         observe%comm,error)
    if (error) return
    call cubearg%register(&
         'VOLDENS',&
         'Volume density',&
         strg_id,&
         code_arg_mandatory,&
         [flag_any],&
         code_read,&
         code_access_speset,&
         observe%voldens,&
         error)
    call cubearg%register(&
         'VX',&
         'x-axis velocity component',&
         strg_id,&
         code_arg_mandatory,&
         [flag_any],&
         code_read,&
         code_access_speset,&
         observe%vx,&
         error)
    call cubearg%register(&
         'VY',&
         'y-axis velocity component',&
         strg_id,&
         code_arg_mandatory,&
         [flag_any],&
         code_read,&
         code_access_speset,&
         observe%vy,&
         error)
    call cubearg%register(&
         'VZ',&
         'z-axis velocity component',&
         strg_id,&
         code_arg_mandatory,&
         [flag_any],&
         code_read,&
         code_access_speset,&
         observe%vz,&
         error)
    if (error) return
    !
    ! Products
    call oucube%register(&
         'COLDENS',&
         'Column density cube',&
         strg_id,&
         [flag_observed,flag_column,flag_density],&
         observe%coldens,&
         error)
    if (error) return
    call oucube%register(&
         'VCENTROID',&
         'Centroid velocity cube',&
         strg_id,&
         [flag_observed,flag_centroid,flag_velocity],&
         observe%vcentroid,&
         error)
    if (error) return
  end subroutine cubefield_observe_register
  !
  subroutine cubefield_observe_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! OBSERVE voldensid vxid vyid vzid
    !----------------------------------------------------------------------
    class(observe_comm_t), intent(in)    :: comm
    character(len=*),      intent(in)    :: line
    type(observe_user_t),  intent(out)   :: user
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='OBSERVE>PARSE'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,observe%comm,user%cubeids,error)
    if (error) return
  end subroutine cubefield_observe_parse
  !
  subroutine cubefield_observe_main(comm,user,error) 
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(observe_comm_t), intent(in)    :: comm
    type(observe_user_t),  intent(inout) :: user
    logical,               intent(inout) :: error
    !
    type(observe_prog_t) :: prog
    character(len=*), parameter :: rname='OBSERVE>MAIN'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubefield_observe_main
  !
  !------------------------------------------------------------------------
  !
  subroutine cubefield_observe_user_toprog(user,comm,prog,error)
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(observe_user_t), intent(in)    :: user
    type(observe_comm_t),  intent(in)    :: comm
    type(observe_prog_t),  intent(out)   :: prog
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='OBSERVE>USER>TOPROG'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%voldens,user%cubeids,prog%voldens,error)
    if (error) return
    call cubeadm_get_header(comm%vx,user%cubeids,prog%vx,error)
    if (error) return
    call cubeadm_get_header(comm%vy,user%cubeids,prog%vy,error)
    if (error) return
    call cubeadm_get_header(comm%vz,user%cubeids,prog%vz,error)
    if (error) return
    prog%distance = user%distance
  end subroutine cubefield_observe_user_toprog
  !
  !------------------------------------------------------------------------
  !
  subroutine cubefield_observe_prog_header(prog,comm,error)
    use phys_const
    use cubetools_axis_types
    use cubetools_header_methods
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(observe_prog_t), intent(inout) :: prog
    type(observe_comm_t),  intent(in)    :: comm
    logical,               intent(inout) :: error
    !
    type(axis_t) :: axis
    real(kind=8), parameter :: Av_per_pscm = 1d0/0.9d21
    real(kind=8), parameter :: cm_per_au = 14959787070000
    integer(kind=chan_k), parameter :: onechan=1
    character(len=*), parameter :: rname='OBSERVE>PROG>HEADER'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    ! *** JP: there should be a check that the input cubes are consistent
    call header_compress_c(prog%coldens,prog%voldens, &
         comm%coldens,'magn',onechan,error)
    if (error)  return
    call header_compress_c(prog%vcentroid,prog%vz,  &
         comm%vcentroid,'km/s',onechan,error)
    if (error)  return
    !
    call cubetools_header_get_axis_head_c(prog%voldens%head,axis,error)
    if (error) return
    prog%nz = axis%n
    prog%factor = Av_per_pscm*cm_per_au*prog%distance*sec_per_rad*abs(axis%inc)
    !
  contains
    !
    subroutine header_compress_c(ou,in,prod,unit,nw,error)
      use cubetools_axis_types
      use cubetools_header_methods
      use cubeadm_clone
      !----------------------------------------------------------------------
      !***JP: Would this one be useful elsewhere?
      !----------------------------------------------------------------------
      type(cube_t), pointer, intent(inout) :: ou
      type(cube_t), pointer, intent(inout) :: in
      type(cube_prod_t),     intent(in)    :: prod
      character(len=*),      intent(in)    :: unit
      integer(kind=chan_k),  intent(in)    :: nw
      logical,               intent(inout) :: error
      !
      type(axis_t) :: axis
      character(len=*), parameter :: rname='HEADER>COMPRESS>C'
      !
      call cubefield_message(fieldseve%trace,rname,'Welcome')
      !
      call cubeadm_clone_header(prod,in,ou,error)
      if (error) return
      call cubetools_header_put_array_unit(unit,ou%head,error)
      if (error) return
      call cubetools_header_get_axis_head_c(ou%head,axis,error)
      if (error) return
      axis%n = nw
      axis%ref = 1d0
      axis%val = 0d0
      call cubetools_header_update_axset_c(axis,ou%head,error)
      if (error) return
    end subroutine header_compress_c
  end subroutine cubefield_observe_prog_header
  !
  subroutine cubefield_observe_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(observe_prog_t), intent(inout) :: prog
    logical,               intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='OBSERVE>PROG>DATA'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo ! itertask
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubefield_observe_prog_data
  !   
  subroutine cubefield_observe_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(observe_prog_t),    intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(spectrum_t) :: voldens,vx,vy,vz
    type(spectrum_t) :: coldens,vcentroid
    character(len=*), parameter :: rname='OBSERVE>PROG>LOOP'
    !
    call voldens%associate('voldens',prog%voldens,iter,error)
    if (error) return
    call vx%associate('vx',prog%vx,iter,error)
    if (error) return
    call vy%associate('vy',prog%vy,iter,error)
    if (error) return
    call vz%associate('vz',prog%vz,iter,error)
    if (error) return
    call coldens%allocate('coldens',prog%coldens,iter,error)
    if (error) return
    call vcentroid%allocate('vcentroid',prog%vcentroid,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter%ie,voldens,vx,vy,vz,coldens,vcentroid,error)
      if (error) return
    enddo ! ie
  end subroutine cubefield_observe_prog_loop
  !   
  subroutine cubefield_observe_prog_act(prog,ie,voldens,vx,vy,vz,coldens,vcentroid,error)
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(observe_prog_t), intent(inout) :: prog
    integer(kind=entr_k),  intent(in)    :: ie
    type(spectrum_t),      intent(inout) :: voldens
    type(spectrum_t),      intent(inout) :: vx
    type(spectrum_t),      intent(inout) :: vy
    type(spectrum_t),      intent(inout) :: vz
    type(spectrum_t),      intent(inout) :: coldens
    type(spectrum_t),      intent(inout) :: vcentroid
    logical,               intent(inout) :: error
    !
    real(kind=sign_k) :: dens,cent
    integer(kind=chan_k) :: iz
    character(len=*), parameter :: rname='OBSERVE>PROG>ACT'
    !
    call voldens%get(ie,error)
    if (error) return
!!$ Not needed in this first implementation
!!$    call vx%get(prog%vx,ie,error)
!!$    if (error) return
!!$    call vy%get(prog%vy,ie,error)
!!$    if (error) return
    call vz%get(ie,error)
    if (error) return
    dens = 0
    cent = 0
    do iz=1,prog%nz
       dens = dens+voldens%y%val(iz)
       cent = cent+voldens%y%val(iz)*vz%y%val(iz)
    enddo ! iz
    cent = cent/dens
    coldens%y%val(1) = dens*prog%factor
    vcentroid%y%val(1) = cent
    call coldens%put(ie,error)
    if (error) return
    call vcentroid%put(ie,error)
    if (error) return
  end subroutine cubefield_observe_prog_act
end module cubefield_observe
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
