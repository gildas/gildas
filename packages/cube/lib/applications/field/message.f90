!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage CUBE messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubefield_messaging
  use gpack_def
  use gbl_message
  use cubetools_parameters
  !
  public :: fieldseve,seve,mess_l
  public :: cubefield_message_set_id,cubefield_message
  public :: cubefield_message_set_alloc,cubefield_message_get_alloc
  public :: cubefield_message_set_trace,cubefield_message_get_trace
  public :: cubefield_message_set_others,cubefield_message_get_others
  private
  !
  ! Identifier used for message identification
  integer(kind=4) :: cubefield_message_id = gpack_global_id  ! Default value for startup message
  !
  type :: cubefield_messaging_debug_t
     integer(kind=code_k) :: alloc = seve%d
     integer(kind=code_k) :: trace = seve%t
     integer(kind=code_k) :: others = seve%d
  end type cubefield_messaging_debug_t
  !
  type(cubefield_messaging_debug_t) :: fieldseve
  !
contains
  !
  subroutine cubefield_message_set_id(id)
    !---------------------------------------------------------------------
    ! Alter library id into input id. Should be called by the library
    ! which wants to share its id with the current one.
    !---------------------------------------------------------------------
    integer(kind=4), intent(in) :: id
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='MESSAGE>SET>ID'
    !
    cubefield_message_id = id
    write (mess,'(A,I0)') 'Now use id #',cubefield_message_id
    call cubefield_message(seve%d,rname,mess)
  end subroutine cubefield_message_set_id
  !
  subroutine cubefield_message(mkind,procname,message)
    use cubetools_cmessaging
    !---------------------------------------------------------------------
    ! Messaging facility for the current library. Calls the low-level
    ! (internal) messaging routine with its own identifier.
    !---------------------------------------------------------------------
    integer(kind=4),  intent(in) :: mkind     ! Message kind
    character(len=*), intent(in) :: procname  ! Name of calling procedure
    character(len=*), intent(in) :: message   ! Message string
    !
    call cubetools_cmessage(cubefield_message_id,mkind,'FIELD>'//procname,message)
  end subroutine cubefield_message
  !
  subroutine cubefield_message_set_alloc(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       fieldseve%alloc = seve%i
    else
       fieldseve%alloc = seve%d
    endif
  end subroutine cubefield_message_set_alloc
  !
  subroutine cubefield_message_set_trace(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       fieldseve%trace = seve%i
    else
       fieldseve%trace = seve%t
    endif
  end subroutine cubefield_message_set_trace
  !
  subroutine cubefield_message_set_others(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       fieldseve%others = seve%i
    else
       fieldseve%others = seve%d
    endif
  end subroutine cubefield_message_set_others
  !
  function cubefield_message_get_alloc()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubefield_message_get_alloc
    !
    cubefield_message_get_alloc = fieldseve%alloc.eq.seve%i
    !
  end function cubefield_message_get_alloc
  !
  function cubefield_message_get_trace()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubefield_message_get_trace
    !
    cubefield_message_get_trace = fieldseve%trace.eq.seve%i
    !
  end function cubefield_message_get_trace
  !
  function cubefield_message_get_others()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubefield_message_get_others
    !
    cubefield_message_get_others = fieldseve%others.eq.seve%i
    !
  end function cubefield_message_get_others
end module cubefield_messaging
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
