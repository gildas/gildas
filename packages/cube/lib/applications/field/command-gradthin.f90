!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubefield_gradthin
  use cubetools_parameters
  use cube_types
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubetopology_cuberegion_types
  use cubefield_messaging
  !
  public :: gradthin
  private
  !
  type :: gradthin_comm_t
     type(option_t), pointer :: comm
     type(cuberegion_comm_t) :: region
     type(cubeid_arg_t), pointer :: gradamp ! Gradient Amplitude
     type(cubeid_arg_t), pointer :: gradang ! Gradient Angle
     type(cube_prod_t),  pointer :: thinned
   contains
     procedure, public  :: register => cubefield_gradthin_comm_register
     procedure, private :: parse    => cubefield_gradthin_comm_parse
     procedure, private :: main     => cubefield_gradthin_comm_main
  end type gradthin_comm_t
  type(gradthin_comm_t) :: gradthin  
  !
  type gradthin_user_t
     type(cubeid_user_t)     :: cubeids
     type(cuberegion_user_t) :: region
   contains
     procedure, private :: toprog => cubefield_gradthin_user_toprog
  end type gradthin_user_t
  !
  type gradthin_prog_t
     type(cuberegion_prog_t) :: region
     type(cube_t), pointer   :: gradamp
     type(cube_t), pointer   :: gradang
     type(cube_t), pointer   :: thinned
   contains
     procedure, private :: header => cubefield_gradthin_prog_header
     procedure, private :: data   => cubefield_gradthin_prog_data
     procedure, private :: loop   => cubefield_gradthin_prog_loop
     procedure, private :: act    => cubefield_gradthin_prog_act
  end type gradthin_prog_t
  !
contains
  !
  subroutine cubefield_gradthin_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(gradthin_user_t) :: user
    character(len=*), parameter :: rname='GRADTHIN>COMMAND'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call gradthin%parse(line,user,error)
    if (error) return
    call gradthin%main(user,error)
    if (error) continue
  end subroutine cubefield_gradthin_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubefield_gradthin_comm_register(comm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(gradthin_comm_t), intent(inout) :: comm
    logical,                intent(inout) :: error
    !
    type(cubeid_arg_t) :: incube
    type(cube_prod_t) :: oucube
    character(len=*), parameter :: rname='GRADTHIN>COMM>REGISTER'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    ! Syntax
    call cubetools_register_command(&
         'GRADTHIN','[cubeid]',&
         'Output a mask of thin edges',&
         'Mark pixels that are a local maximum of the gradient amplitude along the gradient direction',&
         cubefield_gradthin_command,&
         comm%comm,&
         error)
    if (error) return
    call incube%register(&
         'GRADAMP',&
         'Cube of gradient amplitudes',&
         strg_id,&
         code_arg_optional,&
         [flag_gradient,flag_amplitude],&
         code_read,&
         code_access_imaset,&
         comm%gradamp,&
         error)
    if (error) return
    call incube%register(&
         'GRADANG',&
         'Cube of gradient angles',&
         strg_id,&
         code_arg_optional,&
         [flag_gradient,flag_angle],&
         code_read,&
         code_access_imaset,&
         comm%gradang,&
         error)
    if (error) return
    call comm%region%register(error)
    if (error) return
    !
    ! Products
    call oucube%register(&
         'THINNED',&
         'Cube of thinned edges',&
         strg_id,&
         [flag_thinned,flag_edge],&
         comm%thinned,&
         error)
    if (error)  return
  end subroutine cubefield_gradthin_comm_register
  !
  subroutine cubefield_gradthin_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! GRADTHIN gradampid gradangid
    !----------------------------------------------------------------------
    class(gradthin_comm_t), intent(in)    :: comm
    character(len=*),       intent(in)    :: line
    type(gradthin_user_t),  intent(out)   :: user
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='GRADTHIN>COMM>PARSE'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%region%parse(line,user%region,error)
    if (error) return
  end subroutine cubefield_gradthin_comm_parse
  !
  subroutine cubefield_gradthin_comm_main(comm,user,error)
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(gradthin_comm_t), intent(in)    :: comm
    type(gradthin_user_t),  intent(inout) :: user
    logical,                intent(inout) :: error
    !
    type(gradthin_prog_t) :: prog
    character(len=*), parameter :: rname='GRADTHIN>MAIN'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubefield_gradthin_comm_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubefield_gradthin_user_toprog(user,comm,prog,error)
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(gradthin_user_t), intent(in)    :: user
    type(gradthin_comm_t),  intent(in)    :: comm
    type(gradthin_prog_t),  intent(out)   :: prog
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='GRADTHIN>USER>TOPROG'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%gradamp,user%cubeids,prog%gradamp,error)
    if (error) return
    call user%region%toprog(prog%gradamp,prog%region,error)
    if (error) return
    call cubeadm_get_header(comm%gradang,user%cubeids,prog%gradang,error)
    if (error) return
    call user%region%toprog(prog%gradang,prog%region,error)
    if (error) return
    ! User feedback about the interpretation of his command line
    call prog%region%list(error)
    if (error) return
  end subroutine cubefield_gradthin_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubefield_gradthin_prog_header(prog,comm,error)
    use cubeadm_clone
    use cubetools_header_methods
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(gradthin_prog_t), intent(inout) :: prog
    type(gradthin_comm_t),  intent(in)    :: comm
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='GRADTHIN>PROG>HEADER'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call cubeadm_clone_header_with_region(comm%thinned,  &
      prog%gradamp,prog%region,prog%thinned,error)
    if (error) return
    ! Unit
    call cubetools_header_put_array_unit('Boolean',prog%thinned%head,error)
    if (error) return
  end subroutine cubefield_gradthin_prog_header
  !
  subroutine cubefield_gradthin_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(gradthin_prog_t), intent(inout) :: prog
    logical,                intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='GRADTHIN>PROG>DATA'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,prog%region,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubefield_gradthin_prog_data
  !   
  subroutine cubefield_gradthin_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(gradthin_prog_t),   intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(image_t) :: gradamp,gradang,thinned
    character(len=*), parameter :: rname='GRADTHIN>PROG>LOOP'
    !
    call gradamp%associate('gradamp',prog%gradamp,iter,error)
    if (error) return
    call gradang%associate('gradang',prog%gradang,iter,error)
    if (error) return
    call thinned%allocate('thinned',prog%thinned,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter%ie,gradamp,gradang,thinned,error)
      if (error) return
    enddo ! ie
  end subroutine cubefield_gradthin_prog_loop
  !   
  subroutine cubefield_gradthin_prog_act(prog,ie,gradamp,gradang,thinned,error)
    use phys_const
    use cubetools_nan
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(gradthin_prog_t), intent(inout) :: prog
    integer(kind=entr_k),   intent(in)    :: ie
    type(image_t), target,  intent(inout) :: gradamp
    type(image_t), target,  intent(inout) :: gradang
    type(image_t),          intent(inout) :: thinned
    logical,                intent(inout) :: error
    !
    integer(kind=pixe_k) :: ix,iy,idx,idy
    integer(kind=inte_k) :: iangle
    real(kind=sign_k), pointer :: ampli,angle,thin
    real(kind=dble_k), parameter :: pi4=pi/4
    real(kind=dble_k), parameter :: pi8=pi/8
    character(len=*), parameter :: rname='GRADTHIN>PROG>ACT'
    !
    idx = 1
    idy = 1
    call gradamp%get(ie,error)
    if (error) return
    call gradang%get(ie,error)
    if (error) return
    call thinned%set(gr4nan,error)
    if (error) return
    do iy=3,gradamp%ny-2
       do ix=3,gradamp%nx-2
          ampli => gradamp%val(ix,iy)
          angle => gradang%val(ix,iy)
          thin  => thinned%val(ix,iy)
          if (isnan(ampli).or.isnan(angle)) then
             thin = gr4nan
          else
             thin = ampli
             iangle = int(modulo(angle+pi8,pi)/pi4)
             select case (iangle)
             case (0)
                ! Mostly horizontal gradient
                if (ampli.lt.gradamp%val(ix-idx,iy) .or. ampli.lt.gradamp%val(ix+idx,iy)) then
                   thin = gr4nan
                endif
             case (1)
                ! Mostly oblique at +45 deg
                if (ampli.lt.gradamp%val(ix-idx,iy+idy) .or. ampli.lt.gradamp%val(ix+idx,iy-idy)) then
                   thin = gr4nan
                endif
             case (2)
                ! Mostly vertical gradient
                if (ampli.lt.gradamp%val(ix,iy-idy) .or. ampli.lt.gradamp%val(ix,iy+idy)) then
                   thin = gr4nan
                endif
             case (3)
                ! Mostly oblique at +135 deg
                if (ampli.lt.gradamp%val(ix-idx,iy-idy) .or. ampli.lt.gradamp%val(ix+idx,iy+idy)) then
                   thin = gr4nan
                endif
             case default
                error = .true.
                return
             end select
          endif
       enddo ! ix
    enddo ! iy
    call thinned%put(ie,error)
    if (error) return
  end subroutine cubefield_gradthin_prog_act
end module cubefield_gradthin
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
