!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubefield_divergence
  use cubetools_parameters
  use cube_types
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubefield_messaging
  ! 
  public :: divergence
  private
  !
  type :: divergence_comm_t
     type(option_t),     pointer :: comm
     type(cubeid_arg_t), pointer :: df_dx
     type(cubeid_arg_t), pointer :: df_dy
     type(cube_prod_t),  pointer :: divergence
   contains
     procedure, public  :: register => cubefield_divergence_register
     procedure, private :: parse    => cubefield_divergence_parse
     procedure, private :: main     => cubefield_divergence_main
  end type divergence_comm_t
  type(divergence_comm_t) :: divergence
  !
  type divergence_user_t
     type(cubeid_user_t) :: df_dx
     type(cubeid_user_t) :: df_dy
   contains
     procedure, private :: toprog => cubefield_divergence_user_toprog
  end type divergence_user_t
  !
  type divergence_prog_t
     type(cube_t), pointer :: df_dx
     type(cube_t), pointer :: df_dy
     type(cube_t), pointer :: divergence
     integer(kind=pixe_k)  :: nx = 0
     integer(kind=pixe_k)  :: idx = 0
     real(kind=coor_k)     :: dx = 0d0
     integer(kind=pixe_k)  :: ny = 0
     integer(kind=pixe_k)  :: idy = 0
     real(kind=coor_k)     :: dy = 0d0
   contains
     procedure, private :: header => cubefield_divergence_prog_header
     procedure, private :: data   => cubefield_divergence_prog_data
     procedure, private :: loop   => cubefield_divergence_prog_loop
     procedure, private :: act    => cubefield_divergence_prog_act
  end type divergence_prog_t
  !
contains
  !
  subroutine cubefield_divergence_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(divergence_user_t) :: user
    character(len=*), parameter :: rname='DIVERGENCE>COMMAND'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call divergence%parse(line,user,error)
    if (error) return
    call divergence%main(user,error)
    if (error) continue
  end subroutine cubefield_divergence_command
  !
  subroutine cubefield_divergence_register(divergence,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(divergence_comm_t), intent(inout) :: divergence
    logical,                  intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    type(cube_prod_t) :: oucube
    character(len=*), parameter :: comm_abstract='Compute the divergence of a 2D spatial gradient'
    character(len=*), parameter :: comm_help=strg_id
    character(len=*), parameter :: rname='DIVERGENCE>REGISTER'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'DIVERGENCE','[df_dx df_dy]',&
         comm_abstract,&
         comm_help,&
         cubefield_divergence_command,&
         divergence%comm,error)
    if (error) return
    call cubearg%register(&
         'DF_DX',&
         'Gradient x component',&
         strg_id,&
         code_arg_optional,&
         [flag_gradient,flag_dx],&
         code_read,&
         code_access_imaset,&
         divergence%df_dx,&
         error)
    call cubearg%register(&
         'DF_DY',&
         'Gradient y component',&
         strg_id,&
         code_arg_optional,&
         [flag_gradient,flag_dy],&
         code_read,&
         code_access_imaset,&
         divergence%df_dy,&
         error)
    if (error) return
    !
    ! Product
    call oucube%register(&
         'DIVERGENCE',&
         'Cube of divergence',&
         strg_id,&
         [flag_divergence],&
         divergence%divergence,&
         error)
    if (error) return
  end subroutine cubefield_divergence_register
  !
  subroutine cubefield_divergence_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! DIVERGENCE df_dx_id df_dy_id
    !----------------------------------------------------------------------
    class(divergence_comm_t), intent(in)    :: comm
    character(len=*),         intent(in)    :: line
    type(divergence_user_t),  intent(out)   :: user
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='DIVERGENCE>PARSE'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%df_dx,error)
    if (error) return
    call cubeadm_cubeid_parse(line,comm%comm,user%df_dy,error)
    if (error) return
  end subroutine cubefield_divergence_parse
  !
  subroutine cubefield_divergence_main(comm,user,error) 
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(divergence_comm_t), intent(in)    :: comm
    type(divergence_user_t),  intent(inout) :: user
    logical,                  intent(inout) :: error
    !
    type(divergence_prog_t) :: prog
    character(len=*), parameter :: rname='DIVERGENCE>MAIN'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubefield_divergence_main
  !
  !------------------------------------------------------------------------
  !
  subroutine cubefield_divergence_user_toprog(user,comm,prog,error)
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(divergence_user_t), intent(in)    :: user
    type(divergence_comm_t),  intent(in)    :: comm
    type(divergence_prog_t),  intent(out)   :: prog
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='DIVERGENCE>USER>TOPROG'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%df_dx,user%df_dx,prog%df_dx,error)
    if (error) return
    call cubeadm_get_header(comm%df_dy,user%df_dy,prog%df_dy,error)
    if (error) return
  end subroutine cubefield_divergence_user_toprog
  !
  !------------------------------------------------------------------------
  !
  subroutine cubefield_divergence_prog_header(prog,comm,error)
    use cubeadm_clone
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(divergence_prog_t), intent(inout) :: prog
    type(divergence_comm_t),  intent(in)    :: comm
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='DIVERGENCE>PROG>HEADER'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    ! *** JP: there should be a check that the input cubes are consistent
    call cubeadm_clone_header(comm%divergence,prog%df_dx,prog%divergence,error)
    if (error) return
    call cubefield_divergence_prog_header_axis(prog%divergence,&
         prog%divergence%head%set%il,prog%nx,prog%idx,prog%dx,error)
    if (error) return
    call cubefield_divergence_prog_header_axis(prog%divergence,&
         prog%divergence%head%set%im,prog%ny,prog%idy,prog%dy,error)
    if (error) return
  end subroutine cubefield_divergence_prog_header
  !
  subroutine cubefield_divergence_prog_header_axis(cube,iaxis,nx,idx,dx,error)
    use phys_const
    use cubetools_axis_types
    use cubetools_header_methods
    use cubeadm_clone
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(cube_t), pointer, intent(inout) :: cube
    integer(kind=ndim_k),  intent(in)    :: iaxis
    integer(kind=pixe_k),  intent(inout) :: nx
    integer(kind=pixe_k),  intent(inout) :: idx
    real(kind=coor_k),     intent(inout) :: dx
    logical,               intent(inout) :: error
    !
    type(axis_t) :: axis
    character(len=*), parameter :: rname='DIVERGENCE>PROG>HEADER>AXIS'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call cubetools_header_get_axis_head(iaxis,cube%head,axis,error)
    if (error) return
    nx = axis%n
    idx = 1 ! *** JP: should be customizable depending on the angular resolution
    dx = (2*idx)*axis%inc*sec_per_rad ! *** JP: should use the current angle unit
  end subroutine cubefield_divergence_prog_header_axis
  !
  subroutine cubefield_divergence_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(divergence_prog_t), intent(inout) :: prog
    logical,                  intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='DIVERGENCE>PROG>DATA'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo ! iter
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubefield_divergence_prog_data
  !   
  subroutine cubefield_divergence_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(divergence_prog_t), intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(image_t) :: df_dx,df_dy,divergence
    character(len=*), parameter :: rname='DIVERGENCE>PROG>LOOP'
    !
    call df_dx%associate('df_dx',prog%df_dx,iter,error)
    if (error) return
    call df_dy%associate('df_dy',prog%df_dy,iter,error)
    if (error) return
    call divergence%allocate('divergence',prog%divergence,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter%ie,df_dx,df_dy,divergence,error)
      if (error) return
    enddo
  end subroutine cubefield_divergence_prog_loop
  !   
  subroutine cubefield_divergence_prog_act(prog,ie,df_dx,df_dy,divergence,error)
    use cubetools_nan
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(divergence_prog_t), intent(inout) :: prog
    integer(kind=entr_k),     intent(in)    :: ie
    type(image_t),            intent(inout) :: df_dx
    type(image_t),            intent(inout) :: df_dy
    type(image_t),            intent(inout) :: divergence
    logical,                  intent(inout) :: error
    !
    real(kind=sign_k) :: ddf_dxdx,ddf_dydy
    integer(kind=pixe_k) :: ix,iy
    character(len=*), parameter :: rname='DIVERGENCE>PROG>ACT'
    !
    call df_dx%get(ie,error)
    if (error) return
    call df_dy%get(ie,error)
    if (error) return
    divergence%val(:,:) = gr4nan
    do iy=prog%idy+1,prog%ny-prog%idy
       do ix=prog%idx+1,prog%nx-prog%idx
          ddf_dxdx = (df_dx%val(ix+prog%idx,iy)-df_dx%val(ix-prog%idx,iy))/prog%dx
          ddf_dydy = (df_dy%val(ix,iy+prog%idy)-df_dy%val(ix,iy-prog%idy))/prog%dy
          divergence%val(ix,iy) = ddf_dxdx+ddf_dydy
       enddo ! ix
    enddo ! iy
    call divergence%put(ie,error)
    if (error) return
  end subroutine cubefield_divergence_prog_act
end module cubefield_divergence
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
