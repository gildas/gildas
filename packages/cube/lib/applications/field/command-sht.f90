!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubefield_sht
  use cubetools_parameters
  use cube_types
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
!!$  use cubetopology_cuberegion_types
  use cubefield_messaging
  !
  public :: sht
  private
  !
  type :: sht_comm_t
     type(option_t), pointer :: comm
!     type(cuberegion_comm_t) :: region
     type(cubeid_arg_t), pointer :: gradamp ! Gradient Amplitude
     type(cubeid_arg_t), pointer :: gradang ! Gradient Angle
     type(cube_prod_t),  pointer :: accu     
   contains
     procedure, public  :: register => cubefield_sht_comm_register
     procedure, private :: parse    => cubefield_sht_comm_parse
     procedure, private :: main     => cubefield_sht_comm_main
  end type sht_comm_t
  type(sht_comm_t) :: sht  
  !
  type sht_user_t
     type(cubeid_user_t)     :: cubeids
!     type(cuberegion_user_t) :: region
   contains
     procedure, private :: toprog => cubefield_sht_user_toprog
  end type sht_user_t
  !
  type sht_prog_t
!     type(cuberegion_prog_t) :: region
     type(cube_t), pointer   :: gradamp
     type(cube_t), pointer   :: gradang
     type(cube_t), pointer   :: accu
   contains
     procedure, private :: header => cubefield_sht_prog_header
     procedure, private :: data   => cubefield_sht_prog_data
     procedure, private :: loop   => cubefield_sht_prog_loop
     procedure, private :: act    => cubefield_sht_prog_act
  end type sht_prog_t
  !
contains
  !
  subroutine cubefield_sht_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(sht_user_t) :: user
    character(len=*), parameter :: rname='SHT>COMMAND'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call sht%parse(line,user,error)
    if (error) return
    call sht%main(user,error)
    if (error) continue
  end subroutine cubefield_sht_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubefield_sht_comm_register(comm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(sht_comm_t), intent(inout) :: comm
    logical,           intent(inout) :: error
    !
    type(cubeid_arg_t) :: incube
    type(cube_prod_t) :: accu
    character(len=*), parameter :: comm_abstract='Field command to input per image and output per image'
    character(len=*), parameter :: comm_help='Input and output cubes are real'
    character(len=*), parameter :: rname='SHT>COMM>REGISTER'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    ! Syntax
    call cubetools_register_command(&
         'SHT','[cubeid]',&
         'Standard Hough Transform',&
         'From images to line parameters',&
         cubefield_sht_command,&
         comm%comm,&
         error)
    if (error) return
    call incube%register(&
         'GRADAMP',&
         'Cube of gradient amplitudes',&
         strg_id,&
         code_arg_optional,&
         [flag_gradient,flag_amplitude],&
         code_read,&
         code_access_imaset,&
         comm%gradamp,&
         error)
    if (error) return
    call incube%register(&
         'GRADANG',&
         'Cube of gradient angles',&
         strg_id,&
         code_arg_optional,&
         [flag_gradient,flag_angle],&
         code_read,&
         code_access_imaset,&
         comm%gradang,&
         error)
    if (error) return
!!$    call comm%region%register(error)
!!$    if (error) return
    !
    ! Products
    call accu%register(&
         'ACCUMULATOR',&
         'Cube of line parameter accumulators',&
         strg_id,&
         [flag_sht,flag_accumulator],&
         comm%accu,&
         error)
    if (error)  return
  end subroutine cubefield_sht_comm_register
  !
  subroutine cubefield_sht_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! SHT cubeid
    !----------------------------------------------------------------------
    class(sht_comm_t), intent(in)    :: comm
    character(len=*),  intent(in)    :: line
    type(sht_user_t),  intent(out)   :: user
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='SHT>COMM>PARSE'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
!!$    call comm%region%parse(line,user%region,error)
!!$    if (error) return
  end subroutine cubefield_sht_comm_parse
  !
  subroutine cubefield_sht_comm_main(comm,user,error)
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(sht_comm_t), intent(in)    :: comm
    type(sht_user_t),  intent(inout) :: user
    logical,           intent(inout) :: error
    !
    type(sht_prog_t) :: prog
    character(len=*), parameter :: rname='SHT>MAIN'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubefield_sht_comm_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubefield_sht_user_toprog(user,comm,prog,error)
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(sht_user_t), intent(in)    :: user
    type(sht_comm_t),  intent(in)    :: comm
    type(sht_prog_t),  intent(out)   :: prog
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='SHT>USER>TOPROG'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%gradamp,user%cubeids,prog%gradamp,error)
    if (error) return
!!$    call user%region%toprog(prog%gradamp,prog%region,error)
!!$    if (error) return
    call cubeadm_get_header(comm%gradang,user%cubeids,prog%gradang,error)
    if (error) return
!!$    call user%region%toprog(prog%gradang,prog%region,error)
!!$    if (error) return
    ! User feedback about the interpretation of his command line
!!$    call prog%region%list(error)
!!$    if (error) return
  end subroutine cubefield_sht_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubefield_sht_prog_header(prog,comm,error)
    use phys_const
    use cubetools_unit
    use cubetools_axis_types
    use cubetools_header_methods
    use cubeadm_clone
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(sht_prog_t), intent(inout) :: prog
    type(sht_comm_t),  intent(in)    :: comm
    logical,           intent(inout) :: error
    !
    type(axis_t) :: xaxis,yaxis,axis
    character(len=*), parameter :: rname='SHT>PROG>HEADER'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call cubeadm_clone_header(comm%accu,prog%gradamp,prog%accu,error)
    if (error) return
!!$    call prog%region%header(prog%accu,error)
!!$    if (error) return
    ! Unit
    call cubetools_header_put_array_unit('Counts',prog%accu%head,error)
    if (error) return
    ! Axes
    call cubetools_header_get_axis_head_l(prog%accu%head,xaxis,error)
    if (error) return
    call cubetools_header_get_axis_head_m(prog%accu%head,yaxis,error)
    if (error) return
    call cubetools_axis_init(axis,error)
    ! theta
    if (error) return
    axis%name = 'theta'
    axis%unit = 'posangle'     !***JP: not the risht way to handle units
    axis%kind = code_unit_pang !***JP: not the risht way to handle units
    axis%genuine = .true.
    axis%regular = .true.
    axis%n   = 180
    axis%ref = 0.5
    axis%val = 0.0
    axis%inc = pi/axis%n
    call cubetools_header_update_axset_l(axis,prog%accu%head,error)
    if (error) return
    ! rho
    if (error) return
    axis%name = 'rho'
    axis%unit = 'pixe!'        !***JP: not the risht way to handle units
    axis%kind = code_unit_pixe !***JP: not the risht way to handle units
    axis%genuine = .true.
    axis%regular = .true.
    axis%inc = 5.0
    axis%n   = 2*ceiling(sqrt(real(xaxis%n-1,kind=dble_k)**2+real(yaxis%n-1,kind=dble_k)**2)/axis%inc)+1
    axis%ref = axis%n/2+1
    axis%val = 0
    call cubetools_header_update_axset_m(axis,prog%accu%head,error)
    if (error) return
  end subroutine cubefield_sht_prog_header
  !
  subroutine cubefield_sht_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(sht_prog_t), intent(inout) :: prog
    logical,           intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='SHT>PROG>DATA'
    !
    call cubefield_message(fieldseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubefield_sht_prog_data
  !   
  subroutine cubefield_sht_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(sht_prog_t),        intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(image_t) :: gradamp,gradang
    type(image_t) :: accu
    character(len=*), parameter :: rname='SHT>PROG>LOOP'
    !
    call gradamp%associate('gradamp',prog%gradamp,iter,error)
    if (error) return
    call gradang%associate('gradang',prog%gradang,iter,error)
    if (error) return
    call accu%allocate('accu',prog%accu,iter,error)
    if (error) return
    call accu%associate_xy(error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter%ie,gradamp,gradang,accu,error)
      if (error) return
    enddo ! ie
  end subroutine cubefield_sht_prog_loop
  !   
  subroutine cubefield_sht_prog_act(prog,ie,gradamp,gradang,accu,error)
    use phys_const
    use cubetools_nan
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(sht_prog_t),    intent(inout) :: prog
    integer(kind=entr_k), intent(in)    :: ie
    type(image_t),        intent(inout) :: gradamp
    type(image_t),        intent(inout) :: gradang
    type(image_t),        intent(inout) :: accu
    logical,              intent(inout) :: error
    !
    integer(kind=pixe_k) :: ix,iy,itheta,irho
    real(kind=coor_k) :: rho,theta
    character(len=*), parameter :: rname='SHT>PROG>ACT'
    !
    call gradamp%get(ie,error)
    if (error) return
    call gradang%get(ie,error)
    if (error) return
    call accu%set(0.0,error)
    if (error) return
    do iy=1,gradamp%ny
       do ix=1,gradamp%nx
          !***JP: The minus sign comes from the negative x-axis definition => A more general scheme is required here.
          theta = -gradang%val(ix,iy)
          if (isnan(gradamp%val(ix,iy)).or.isnan(theta)) cycle
          theta = modulo(theta,pi) ! The angle of a line is now inside [0,pi]
          rho = ix*cos(theta)+iy*sin(theta)
          ! Discretize
          call accu%x%offset2pixel(theta,itheta,error)
          if (error) return
          call accu%y%offset2pixel(rho,irho,error)
          if (error) return
          if (accu%contain(itheta,irho)) then
             accu%val(itheta,irho) = accu%val(itheta,irho)+1
          else
             call cubefield_message(seve%e,rname,'Internal error:')
             print *,ix,iy,theta,itheta,rho,irho
             error = .true.
             return
          endif
       enddo ! ix
    enddo ! iy
    call accu%put(ie,error)
    if (error) return
  end subroutine cubefield_sht_prog_act
end module cubefield_sht
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
