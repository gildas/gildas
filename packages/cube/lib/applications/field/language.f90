!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubefield_language
  use cubetools_structure
  use cubefield_messaging
  !
  use cubefield_divergence
  use cubefield_gradient
  use cubefield_gradthin
  use cubefield_hessian
  use cubefield_incline
  use cubefield_minmax
  use cubefield_observe
  use cubefield_sht
  !
  public :: cubefield_register_language
  private
  !
  integer(kind=lang_k) :: langid
  !
contains
  !
  subroutine cubefield_register_language(error)
    !----------------------------------------------------------------------
    ! Register the FIELD\ language
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    call cubetools_register_language('FIELD',&
         'J.Pety, S.Bardeau, V.deSouzaMagalhaes',&
         'Commands to interact with fields of vectors',&
         'gag_doc:hlp/cube-help-field.hlp',&
         cubefield_execute_command,langid,error)
    if (error) return
    !----------------------------------------------------------------------
    !
    call divergence%register(error)
    if (error) return
    call gradient%register(error)
    if (error) return
    call gradthin%register(error)
    if (error) return
    call hessian%register(error)
    if (error) return
    call incline%register(error)
    if (error) return
    call cubefield_minmax_register(error)
    if (error) return
    call observe%register(error)
    if (error) return
    call sht%register(error)
    if (error) return
    !
    call cubetools_register_dict(error)
    if (error) return
  end subroutine cubefield_register_language
  !
  subroutine cubefield_execute_command(line,comm,error)
    use cubeadm_opened
    use cubeadm_timing
    !----------------------------------------------------------------------
    ! Execute a command of the FIELD\ language
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    character(len=*), intent(in)    :: comm
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='EXECUTE>COMMAND'
    !
    error = .false.
    if (comm.eq.'FIELD?') then
       call cubetools_list_language_commands(langid,error)
       if (error) return
    else
       call cubeadm_timing_init()
       call cubetools_execute_command(line,langid,comm,error)
       if (error) continue ! To ensure error recovery in the call to cubeadm_finish_all
       call cubeadm_finish_all(comm,line,error)
       if (error) continue ! To ensure error recovery in timing
       call cubeadm_timing_final(comm)
    endif
  end subroutine cubefield_execute_command
end module cubefield_language
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
