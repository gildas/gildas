!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!***JP: Aborted try to use the one2two_real_comm_t.
!***JP: The problem is that we here need a /AT additional keyval!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubestatistics_percentile
  use cubetemplate_one2two_real_template
  use cubestatistics_messaging
  !
  public :: cubestatistics_percentile_register
  private
  !
  type(one2two_real_comm_t) :: percentile
  !
contains
  !
  subroutine cubestatistics_percentile_register(error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    character(len=*), parameter :: rname='PERCENTILE>REGISTER'
    !
    call cubestatistics_message(statisticsseve%trace,rname,'Welcome')
    !
    call percentile%register_syntax(&
         'PERCENTILE','cubeid','3D cube',[flag_any],cubestatistics_percentile_command,&
         'qmin',[flag_minimum,flag_percentile,flag_image],&
         'qmax',[flag_maximum,flag_percentile,flag_image],&
         error)
    if (error) return
    call percentile%register_act(cubestatistics_percentile_prog_act,error)
    if (error) return
  end subroutine cubestatistics_percentile_register
  !
  subroutine cubestatistics_percentile_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(one2two_real_user_t) :: user
    character(len=*), parameter :: rname='PERCENTILE>COMMAND'
    !
    call cubestatistics_message(statisticsseve%trace,rname,'Welcome')
    !
    call percentile%parse(line,user,error)
    if (error) return
    call percentile%main(user,error)
    if (error) continue
  end subroutine cubestatistics_percentile_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubestatistics_percentile_prog_act(prog,ie,spec,good,qmin,qmax,error)
    use cubetools_parameters
    use cubeadm_spectrum_types
    use cubemain_statistics_tool
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(one2two_real_prog_t), intent(inout) :: prog
    integer(kind=entr_k),       intent(in)    :: ie
    type(spectrum_t),           intent(inout) :: spec
    type(spectrum_t),           intent(inout) :: good
    type(spectrum_t),           intent(inout) :: qmin
    type(spectrum_t),           intent(inout) :: qmax
    logical,                    intent(inout) :: error
    !
    type(spectrum_t) :: line
    integer(kind=chan_k), parameter :: one=1
    character(len=*), parameter :: rname='PERCENTILE>PROG>ACT'
    !
    call spec%get(ie,error)
    if (error) return
    call line%point_to(spec,prog%region%iz%first,prog%region%iz%last,1.0,error)
    if (error) return
    call good%unblank(line,error)
    if (error) return
    !
    qmin%y%val(one) = statistics%percentile(good%y%val,good%n,prog%percentage)
    call qmin%put(ie,error)
    if (error) return
    !
    qmax%y%val(one) = statistics%percentile(good%y%val,good%n,prog%percentage)
    call qmax%put(ie,error)
    if (error) return
  end subroutine cubestatistics_percentile_prog_act
end module cubestatistics_percentile
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
