!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubestatistics_language
  use cubetools_structure
  use cubestatistics_messaging
  !
  use cubestatistics_mean
  use cubestatistics_median
  use cubestatistics_minmax
  use cubestatistics_percentile
  !
  public :: cubestatistics_register_language
  private
  !
  integer(kind=lang_k) :: langid
  !
contains
  !
  subroutine cubestatistics_register_language(error)
    !----------------------------------------------------------------------
    ! Register the STATISTICS\ language
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    call cubetools_register_language('STATISTICS',&
         'J.Pety, S.Bardeau',&
         'Simple statistical operations on cubes',&
         'gag_doc:hlp/cube-help-ima.hlp',&
         cubestatistics_execute_command,langid,error)
    if (error) return
    !
    call cubestatistics_mean_register(error)
    if (error) return
    call cubestatistics_median_register(error)
    if (error) return
    call cubestatistics_minmax_register(error)
    if (error) return
    call percentile%register(error)
    if (error) return
    !
    call cubetools_register_dict(error)
    if (error) return
  end subroutine cubestatistics_register_language
  !
  subroutine cubestatistics_execute_command(line,comm,error)
    use cubeadm_opened
    use cubeadm_timing
    !----------------------------------------------------------------------
    ! Execute a command of the STATISTICS\ language
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    character(len=*), intent(in)    :: comm
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='EXECUTE>COMMAND'
    !
    error = .false.
    if (comm.eq.'STATISTICS?') then
       call cubetools_list_language_commands(langid,error)
       if (error) return
    else
       call cubeadm_timing_init()
       call cubetools_execute_command(line,langid,comm,error)
       if (error) continue ! To ensure error recovery in the call to finalize_all
       call cubeadm_finish_all(comm,line,error)
       if (error) continue ! To ensure error recovery in timing
       call cubeadm_timing_final(comm)
    endif
  end subroutine cubestatistics_execute_command
end module cubestatistics_language
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
