!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubestatistics_median
  use cubetemplate_one2two_real_template
  use cubestatistics_messaging
  !
  public :: cubestatistics_median_register
  private
  !
  type(one2two_real_comm_t) :: median
  !
contains
  !
  subroutine cubestatistics_median_register(error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    character(len=*), parameter :: rname='MEDIAN>REGISTER'
    !
    call cubestatistics_message(statisticsseve%trace,rname,'Welcome')
    !
    call median%register_syntax(&
         'MEDIAN','cubeid','3D cube',[flag_any],cubestatistics_median_command,&
         'median',[flag_median,flag_image],&
         'mad',   [flag_mad   ,flag_image],&
         error)
    if (error) return
    call median%register_act(cubestatistics_median_prog_act,error)
    if (error) return
  end subroutine cubestatistics_median_register
  !
  subroutine cubestatistics_median_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(one2two_real_user_t) :: user
    character(len=*), parameter :: rname='MEDIAN>COMMAND'
    !
    call cubestatistics_message(statisticsseve%trace,rname,'Welcome')
    !
    call median%parse(line,user,error)
    if (error) return
    call median%main(user,error)
    if (error) continue
  end subroutine cubestatistics_median_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubestatistics_median_prog_act(prog,ie,spec,good,med,mad,error)
    use cubetools_parameters
    use cubeadm_spectrum_types
    use cubemain_statistics_tool
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(one2two_real_prog_t), intent(inout) :: prog
    integer(kind=entr_k),       intent(in)    :: ie
    type(spectrum_t),           intent(inout) :: spec
    type(spectrum_t),           intent(inout) :: good
    type(spectrum_t),           intent(inout) :: med
    type(spectrum_t),           intent(inout) :: mad
    logical,                    intent(inout) :: error
    !
    type(spectrum_t) :: line
    integer(kind=chan_k), parameter :: one=1
    character(len=*), parameter :: rname='MEDIAN>PROG>ACT'
    !
    call spec%get(ie,error)
    if (error) return
    call line%point_to(spec,prog%region%iz%first,prog%region%iz%last,1.0,error)
    if (error) return
    call good%unblank(line,error)
    if (error) return
    !
    med%y%val(one) = statistics%median(good%y%val,good%n)
    call med%put(ie,error)
    if (error) return
    !
    mad%y%val(one) = statistics%mad(good%y%val,good%n,med%y%val(one))
    call mad%put(ie,error)
    if (error) return
  end subroutine cubestatistics_median_prog_act
end module cubestatistics_median
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
