!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubestatistics_mean
  use cubetemplate_one2two_real_template
  use cubestatistics_messaging
  !
  public :: cubestatistics_mean_register
  private
  !
  type(one2two_real_comm_t) :: mean
  !
contains
  !
  subroutine cubestatistics_mean_register(error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    character(len=*), parameter :: rname='MEAN>REGISTER'
    !
    call cubestatistics_message(statisticsseve%trace,rname,'Welcome')
    !
    call mean%register_syntax(&
         'MEAN','cubeid','3D cube',[flag_any],cubestatistics_mean_command,&
         'mean',[flag_mean,flag_image],&
         'rms', [flag_rms ,flag_image],&
         error)
    if (error) return
    call mean%register_act(cubestatistics_mean_prog_act,error)
    if (error) return
  end subroutine cubestatistics_mean_register
  !
  subroutine cubestatistics_mean_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(one2two_real_user_t) :: user
    character(len=*), parameter :: rname='MEAN>COMMAND'
    !
    call cubestatistics_message(statisticsseve%trace,rname,'Welcome')
    !
    call mean%parse(line,user,error)
    if (error) return
    call mean%main(user,error)
    if (error) continue
  end subroutine cubestatistics_mean_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubestatistics_mean_prog_act(prog,ie,spec,good,mean,rms,error)
    use cubetools_parameters
    use cubeadm_spectrum_types
    use cubemain_statistics_tool
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(one2two_real_prog_t), intent(inout) :: prog
    integer(kind=entr_k),       intent(in)    :: ie
    type(spectrum_t),           intent(inout) :: spec
    type(spectrum_t),           intent(inout) :: good
    type(spectrum_t),           intent(inout) :: mean
    type(spectrum_t),           intent(inout) :: rms
    logical,                    intent(inout) :: error
    !
    type(spectrum_t) :: line
    integer(kind=chan_k), parameter :: one=1
    character(len=*), parameter :: rname='MEAN>PROG>ACT'
    !
    call spec%get(ie,error)
    if (error) return
    call line%point_to(spec,prog%region%iz%first,prog%region%iz%last,1.0,error)
    if (error) return
    call good%unblank(line,error)
    if (error) return
    !
    mean%y%val(one) = statistics%mean(good%y%val,good%n)
    call mean%put(ie,error)
    if (error) return
    !
    rms%y%val(one) = statistics%rms(good%y%val,good%n,mean%y%val(one))
    call rms%put(ie,error)
    if (error) return
  end subroutine cubestatistics_mean_prog_act
end module cubestatistics_mean
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
