!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage CUBESTATISTICS messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubestatistics_messaging
  use gpack_def
  use gbl_message
  use cubetools_parameters
  !
  public :: statisticsseve,seve,mess_l
  public :: cubestatistics_message_set_id,cubestatistics_message
  public :: cubestatistics_message_set_alloc,cubestatistics_message_get_alloc
  public :: cubestatistics_message_set_trace,cubestatistics_message_get_trace
  public :: cubestatistics_message_set_others,cubestatistics_message_get_others
  private
  !
  ! Identifier used for message identification
  integer(kind=4) :: cubestatistics_message_id = gpack_global_id  ! Default value for startup message
  !
  type :: cubestatistics_messaging_debug_t
     integer(kind=code_k) :: alloc = seve%d
     integer(kind=code_k) :: trace = seve%t
     integer(kind=code_k) :: others = seve%d
  end type cubestatistics_messaging_debug_t
  !
  type(cubestatistics_messaging_debug_t) :: statisticsseve
  !
contains
  !
  subroutine cubestatistics_message_set_id(id)
    !---------------------------------------------------------------------
    ! Alter library id into input id. Should be called by the library
    ! which wants to share its id with the current one.
    !---------------------------------------------------------------------
    integer(kind=4), intent(in) :: id
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='MESSAGE>SET>ID'
    !
    cubestatistics_message_id = id
    write (mess,'(A,I0)') 'Now use id #',cubestatistics_message_id
    call cubestatistics_message(seve%d,rname,mess)
  end subroutine cubestatistics_message_set_id
  !
  subroutine cubestatistics_message(mkind,procname,message)
    use cubetools_cmessaging
    !---------------------------------------------------------------------
    ! Messaging facility for the current library. Calls the low-level
    ! (internal) messaging routine with its own identifier.
    !---------------------------------------------------------------------
    integer(kind=4),  intent(in) :: mkind     ! Message kind
    character(len=*), intent(in) :: procname  ! Name of calling procedure
    character(len=*), intent(in) :: message   ! Message string
    !
    call cubetools_cmessage(cubestatistics_message_id,mkind,'IMA>'//procname,message)
  end subroutine cubestatistics_message
  !
  subroutine cubestatistics_message_set_alloc(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       statisticsseve%alloc = seve%i
    else
       statisticsseve%alloc = seve%d
    endif
  end subroutine cubestatistics_message_set_alloc
  !
  subroutine cubestatistics_message_set_trace(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       statisticsseve%trace = seve%i
    else
       statisticsseve%trace = seve%t
    endif
  end subroutine cubestatistics_message_set_trace
  !
  subroutine cubestatistics_message_set_others(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       statisticsseve%others = seve%i
    else
       statisticsseve%others = seve%d
    endif
  end subroutine cubestatistics_message_set_others
  !
  function cubestatistics_message_get_alloc()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubestatistics_message_get_alloc
    !
    cubestatistics_message_get_alloc = statisticsseve%alloc.eq.seve%i
    !
  end function cubestatistics_message_get_alloc
  !
  function cubestatistics_message_get_trace()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubestatistics_message_get_trace
    !
    cubestatistics_message_get_trace = statisticsseve%trace.eq.seve%i
    !
  end function cubestatistics_message_get_trace
  !
  function cubestatistics_message_get_others()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubestatistics_message_get_others
    !
    cubestatistics_message_get_others = statisticsseve%others.eq.seve%i
    !
  end function cubestatistics_message_get_others
end module cubestatistics_messaging
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
