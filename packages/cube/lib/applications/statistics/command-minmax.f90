!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubestatistics_minmax
  use cubetemplate_one2two_real_template
  use cubestatistics_messaging
  !
  public :: cubestatistics_minmax_register
  private
  !
  type(one2two_real_comm_t) :: minmax
  !
contains
  !
  subroutine cubestatistics_minmax_register(error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    character(len=*), parameter :: rname='MINMAX>REGISTER'
    !
    call cubestatistics_message(statisticsseve%trace,rname,'Welcome')
    !
    call minmax%register_syntax(&
         'MINMAX','cubeid','3D cube',[flag_any],cubestatistics_minmax_command,&
         'min',[flag_minimum,flag_image],&
         'max',[flag_maximum,flag_image],&
         error)
    if (error) return
    call minmax%register_act(cubestatistics_minmax_prog_act,error)
    if (error) return
  end subroutine cubestatistics_minmax_register
  !
  subroutine cubestatistics_minmax_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(one2two_real_user_t) :: user
    character(len=*), parameter :: rname='MINMAX>COMMAND'
    !
    call cubestatistics_message(statisticsseve%trace,rname,'Welcome')
    !
    call minmax%parse(line,user,error)
    if (error) return
    call minmax%main(user,error)
    if (error) continue
  end subroutine cubestatistics_minmax_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubestatistics_minmax_prog_act(prog,ie,spec,good,min,max,error)
    use cubetools_parameters
    use cubeadm_spectrum_types
    use cubemain_statistics_tool
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(one2two_real_prog_t), intent(inout) :: prog
    integer(kind=entr_k),       intent(in)    :: ie
    type(spectrum_t),           intent(inout) :: spec
    type(spectrum_t),           intent(inout) :: good
    type(spectrum_t),           intent(inout) :: min
    type(spectrum_t),           intent(inout) :: max
    logical,                    intent(inout) :: error
    !
    type(spectrum_t) :: line
    integer(kind=chan_k), parameter :: one=1
    character(len=*), parameter :: rname='MINMAX>PROG>ACT'
    !
    call spec%get(ie,error)
    if (error) return
    call line%point_to(spec,prog%region%iz%first,prog%region%iz%last,1.0,error)
    if (error) return
    call good%unblank(line,error)
    if (error) return
    call statistics%minmax(&
         good%y%val,good%n,&
         min%y%val(one),&
         max%y%val(one))
    call min%put(ie,error)
    if (error) return
    call max%put(ie,error)
    if (error) return
  end subroutine cubestatistics_minmax_prog_act
end module cubestatistics_minmax
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
