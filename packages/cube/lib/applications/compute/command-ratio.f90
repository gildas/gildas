!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubecompute_ratio
  use cubecompute_two2one_template
  use cubecompute_messaging
  !
  public :: cubecompute_ratio_register
  private
  !
  type(two2one_comm_t) :: ratio
  !
contains
  !
  subroutine cubecompute_ratio_register(error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    ! Register the command:
    !  - register command in SIC,
    !  - register help, arguments, etc in CUBE internal structures,
    !  - register the processing engine in the compute_two2one frame.
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    character(len=*), parameter :: rname='RATIO>REGISTER'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call ratio%register_syntax(&
         'RATIO','numerator','denominator',&
         [flag_ratio],cubecompute_ratio_command,error)
    if (error) return
    !
    call ratio%unit%register(cubecompute_ratio_prog_unit,error)
    if (error) return
    !
    call ratio%act%register_r4r4tor4(cubecompute_ratio_prog_act_r4r4,error)
    if (error) return
    call ratio%act%register_r4c4toc4(cubecompute_ratio_prog_act_r4c4,error)
    if (error) return
    call ratio%act%register_c4r4toc4(cubecompute_ratio_prog_act_c4r4,error)
    if (error) return
    call ratio%act%register_c4c4toc4(cubecompute_ratio_prog_act_c4c4,error)
    if (error) return
  end subroutine cubecompute_ratio_register
  !
  subroutine cubecompute_ratio_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(two2one_user_t) :: user
    character(len=*), parameter :: rname='RATIO>COMMAND'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call ratio%parse(line,user,error)
    if (error) return
    call ratio%main(user,error)
    if (error) continue
  end subroutine cubecompute_ratio_command
  !
  subroutine cubecompute_ratio_prog_unit(unit1,unit2,ouunit,error)
    !----------------------------------------------------------------------
    ! Set the proper unit of the output cube
    !----------------------------------------------------------------------
    character(len=*), intent(inout) :: unit1
    character(len=*), intent(inout) :: unit2
    character(len=*), intent(inout) :: ouunit
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: data_unit_null='---'  ! To be factorized
    character(len=*), parameter :: rname='RATIO>UNIT'
    !
    if (unit1.eq.unit2) then
       ouunit = data_unit_null
    else
       ouunit = trim(unit1)//'/'//trim(unit2)
    endif
  end subroutine cubecompute_ratio_prog_unit
  !
  !----------------------------------------------------------------------
  !
  subroutine cubecompute_ratio_prog_act_r4r4(ie,numerator,denominator,ratio,error)
    use cubetools_parameters
    use cubetools_nan
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(image_t),        intent(inout) :: numerator
    type(image_t),        intent(inout) :: denominator
    type(image_t),        intent(inout) :: ratio
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='RATIO>PROG>ACT>R4R4'
    integer(kind=pixe_k) :: ix,iy
    real(kind=4) :: nanval
    !
    nanval = gr4nan  ! Not a parameter because gr4nan is set at startup
    include 'action-ratio.inc'
  end subroutine cubecompute_ratio_prog_act_r4r4
  !
  subroutine cubecompute_ratio_prog_act_r4c4(ie,numerator,denominator,ratio,error)
    use cubetools_parameters
    use cubetools_nan
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(image_t),        intent(inout) :: numerator
    type(visi_t),         intent(inout) :: denominator
    type(visi_t),         intent(inout) :: ratio
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='RATIO>PROG>ACT>R4C4'
    integer(kind=pixe_k) :: ix,iy
    complex(kind=4) :: nanval
    !
    nanval = gc4nan  ! Not a parameter because gc4nan is set at startup
    include 'action-ratio.inc'
  end subroutine cubecompute_ratio_prog_act_r4c4
  !
  subroutine cubecompute_ratio_prog_act_c4r4(ie,numerator,denominator,ratio,error)
    use cubetools_parameters
    use cubetools_nan
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(visi_t),         intent(inout) :: numerator
    type(image_t),        intent(inout) :: denominator
    type(visi_t),         intent(inout) :: ratio
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='RATIO>PROG>ACT>C4R4'
    integer(kind=pixe_k) :: ix,iy
    complex(kind=4) :: nanval
    !
    nanval = gc4nan  ! Not a parameter because gc4nan is set at startup
    include 'action-ratio.inc'
  end subroutine cubecompute_ratio_prog_act_c4r4
  !
  subroutine cubecompute_ratio_prog_act_c4c4(ie,numerator,denominator,ratio,error)
    use cubetools_parameters
    use cubetools_nan
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(visi_t),         intent(inout) :: numerator
    type(visi_t),         intent(inout) :: denominator
    type(visi_t),         intent(inout) :: ratio
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='RATIO>PROG>ACT>C4C4'
    integer(kind=pixe_k) :: ix,iy
    complex(kind=4) :: nanval
    !
    nanval = gc4nan  ! Not a parameter because gc4nan is set at startup
    include 'action-ratio.inc'
  end subroutine cubecompute_ratio_prog_act_c4c4
end module cubecompute_ratio
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
