    !-------------------------------------------------------------------
    ! Action code for SUM command
    !-------------------------------------------------------------------
    !
    integer(kind=pixe_k) :: ix,iy
    !
    call summand1%get(ie,error)
    if (error) return
    call summand2%get(ie,error)
    if (error) return
    !
    do iy=1,sum%ny
       do ix=1,sum%nx
          sum%val(ix,iy) = summand1%val(ix,iy)+summand2%val(ix,iy)
       enddo ! ix
    enddo ! iy
    !
    call sum%put(ie,error)
    if (error) return
