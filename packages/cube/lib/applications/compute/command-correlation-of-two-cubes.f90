!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!***JP: This one is a tentative code that did not converge. I leave it here
!***JP: for the moment. To be deleted on 2024-09-01 if nothing happened!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubecompute_correlation
  use cube_types
  use cubetools_parameters
  use cubetools_structure
  use cubetopology_cuberegion_types
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubecompute_messaging
  use cubecompute_correlate_spectrum_tool
  !
  public :: correlation
  private
  !
  type :: correlation_comm_t
     type(option_t), pointer :: comm
     type(cuberegion_comm_t) :: region
     type(cubeid_arg_t), pointer :: cube1
     type(cubeid_arg_t), pointer :: cube2
     type(cube_prod_t),  pointer :: correlated
   contains
     procedure, public  :: register => cubecompute_correlation_comm_register
     procedure, private :: parse    => cubecompute_correlation_comm_parse
     procedure, private :: main     => cubecompute_correlation_comm_main
  end type correlation_comm_t
  type(correlation_comm_t) :: correlation
  !
  type correlation_user_t
     type(cubeid_user_t)     :: cubeids
     type(cuberegion_user_t) :: region
   contains
     procedure, private :: toprog => cubecompute_correlation_user_toprog
  end type correlation_user_t
  !
  type correlation_prog_t
     type(cuberegion_prog_t) :: region
     type(cube_t), pointer   :: cube1
     type(cube_t), pointer   :: cube2
     type(cube_t), pointer   :: correlated
     type(correlate_spectrum_prog_t) :: correl
   contains
     procedure, private :: header => cubecompute_correlation_prog_header
     procedure, private :: data   => cubecompute_correlation_prog_data
     procedure, private :: loop   => cubecompute_correlation_prog_loop
     procedure, private :: act    => cubecompute_correlation_prog_act
  end type correlation_prog_t
  !
contains
  !
  subroutine cubecompute_correlation_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(correlation_user_t) :: user
    character(len=*), parameter :: rname='CORRELATION>COMMAND'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call correlation%parse(line,user,error)
    if (error) return
    call correlation%main(user,error)
    if (error) continue
  end subroutine cubecompute_correlation_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubecompute_correlation_comm_register(comm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(correlation_comm_t), intent(inout) :: comm
    logical,                   intent(inout) :: error
    !
    type(cubeid_arg_t) :: incube
    type(cube_prod_t) :: correlated
    character(len=*), parameter :: comm_abstract='Template to access input/output data per spectrum'
    character(len=*), parameter :: comm_help='Input and output cubes are real'
    character(len=*), parameter :: rname='CORRELATION>COMM>REGISTER'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    ! Syntax
    call cubetools_register_command(&
        'CORRELATION','[cubeid1] [cubeid2]',&
         comm_abstract,&
         comm_help,&
         cubecompute_correlation_command,&
         comm%comm,&
         error)
    if (error) return
    call incube%register(&
         'CUBE1',&
         'First cube',&
         strg_id,&
         code_arg_optional,&
         [flag_any],&
         code_read,&
         code_access_speset,&
         comm%cube1,&
         error)
    call incube%register(&
         'CUBE2',&
         'Second cube',&
         strg_id,&
         code_arg_optional,&
         [flag_any],&
         code_read,&
         code_access_speset,&
         comm%cube2,&
         error)
    if (error) return
    call comm%region%register(error)
    if (error) return
    !
    ! Products
    call correlated%register(&
         'CORRELATION',&
         'Correlated cube',&
         strg_id,&
         [flag_correlation],&
         comm%correlated,&
         error)
    if (error)  return
  end subroutine cubecompute_correlation_comm_register
  !
  subroutine cubecompute_correlation_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(correlation_comm_t), intent(in)    :: comm
    character(len=*),          intent(in)    :: line
    type(correlation_user_t),  intent(out)   :: user
    logical,                   intent(inout) :: error
    !
    character(len=*), parameter :: rname='CORRELATION>COMM>PARSE'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%region%parse(line,user%region,error)
    if (error) return
  end subroutine cubecompute_correlation_comm_parse
  !
  subroutine cubecompute_correlation_comm_main(comm,user,error) 
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(correlation_comm_t), intent(in)    :: comm
    type(correlation_user_t),  intent(inout) :: user
    logical,                   intent(inout) :: error
    !
    type(correlation_prog_t) :: prog
    character(len=*), parameter :: rname='CORRELATION>COMM>MAIN'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubecompute_correlation_comm_main
  !
  !------------------------------------------------------------------------
  !
  subroutine cubecompute_correlation_user_toprog(user,comm,prog,error)
    use cubetools_consistency_methods
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(correlation_user_t), intent(in)    :: user
    type(correlation_comm_t),  intent(in)    :: comm    
    type(correlation_prog_t),  intent(out)   :: prog
    logical,                   intent(inout) :: error
    !
    logical :: conspb
    character(len=*), parameter :: rname='CORRELATION>USER>TOPROG'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%cube1,user%cubeids,prog%cube1,error)
    if (error) return
    call cubeadm_get_header(comm%cube2,user%cubeids,prog%cube2,error)
    if (error) return
    !
    ! ***JP: unclear whether the program should accept the case where the
    ! ***JP: subregions of two cubes are consistent
    conspb = .false.
    call cubetools_consistency_shape(&
         'Input cube #1',prog%cube1%head,&
         'Input cube #2',prog%cube2%head,&
         conspb,error)
    if (error) return
    if (cubetools_consistency_failed(rname,conspb,error)) return
    !    
    call user%region%toprog(prog%cube1,prog%region,error)
    if (error) return
    call user%region%toprog(prog%cube2,prog%region,error)
    if (error) return
    ! User feedback about the interpretation of his command line
    call prog%region%list(error)
    if (error) return
  end subroutine cubecompute_correlation_user_toprog
  !
  !------------------------------------------------------------------------
  !
  subroutine cubecompute_correlation_prog_header(prog,comm,error)
    use cubeadm_clone
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(correlation_prog_t), intent(inout) :: prog
    type(correlation_comm_t),  intent(in)    :: comm
    logical,                   intent(inout) :: error
    !
    character(len=*), parameter :: rname='CORRELATION>PROG>HEADER'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call cubeadm_clone_header(comm%correlated,prog%cube1,prog%correlated,error)
    if (error) return
    call prog%region%header(prog%correlated,error)
    if (error) return
  end subroutine cubecompute_correlation_prog_header
  !
  subroutine cubecompute_correlation_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(correlation_prog_t), intent(inout) :: prog
    logical,                   intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='CORRELATION>PROG>DATA'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo ! iter
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubecompute_correlation_prog_data
  !   
  subroutine cubecompute_correlation_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(correlation_prog_t), intent(inout) :: prog
    type(cubeadm_iterator_t),  intent(inout) :: iter
    logical,                   intent(inout) :: error
    !
    type(spectrum_t) :: spe1,spe2,correlated
    character(len=*), parameter :: rname='CORRELATION>PROG>LOOP'
    !
    call spe1%associate('spectrum #1',prog%cube1,iter,error)
    if (error) return
    call spe2%associate('spectrum #2',prog%cube2,iter,error)
    if (error) return
    call correlated%allocate('correlated spectrum',prog%correlated,iter,error)
    if (error) return
    call prog%correl%init(spe1,spe2,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter%ie,spe1,spe2,correlated,error)
      if (error) return
    enddo ! ie
  end subroutine cubecompute_correlation_prog_loop
  !   
  subroutine cubecompute_correlation_prog_act(prog,ie,spe1,spe2,correlated,error)
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(correlation_prog_t), intent(inout) :: prog
    integer(kind=entr_k),      intent(in)    :: ie
    type(spectrum_t),          intent(inout) :: spe1
    type(spectrum_t),          intent(inout) :: spe2
    type(spectrum_t),          intent(inout) :: correlated
    logical,                   intent(inout) :: error
    !
    character(len=*), parameter :: rname='CORRELATION>PROG>ACT'
    !
    call spe1%get(ie,error)
    if (error) return
    call spe2%get(ie,error)
    if (error) return
    call prog%correl%spectrum(spe1,spe2,correlated,error)
    if (error) return
    call correlated%put(ie,error)
    if (error) return
  end subroutine cubecompute_correlation_prog_act
end module cubecompute_correlation
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
