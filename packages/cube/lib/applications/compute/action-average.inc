    !-------------------------------------------------------------------
    ! Action code for AVERAGE command
    !-------------------------------------------------------------------
    !
    integer(kind=pixe_k) :: ix,iy
    !
    call operand1%get(ie,error)
    if (error) return
    call operand2%get(ie,error)
    if (error) return
    !
    do iy=1,average%ny
       do ix=1,average%nx
          average%val(ix,iy) = (operand1%val(ix,iy)+operand2%val(ix,iy))/2
       enddo ! ix
    enddo ! iy
    !
    call average%put(ie,error)
    if (error) return
