!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubecompute_difference
  use cubetools_parameters
  use cubecompute_two2one_template
  use cubecompute_messaging
  !
  public :: cubecompute_difference_register
  private
  !
  type(two2one_comm_t) :: difference
  !
contains
  !
  subroutine cubecompute_difference_register(error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    character(len=*), parameter :: rname='DIFFERENCE>REGISTER'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call difference%register_syntax(&
         'DIFFERENCE','subtrahend','minuend',&
         [flag_difference],cubecompute_difference_command,error)
    if (error) return
    !
    call difference%unit%register(cubecompute_difference_prog_unit,error)
    if (error) return
    !
    call difference%act%register_r4r4tor4(cubecompute_difference_prog_act_r4r4,error)
    if (error) return
    call difference%act%register_r4c4toc4(cubecompute_difference_prog_act_r4c4,error)
    if (error) return
    call difference%act%register_c4r4toc4(cubecompute_difference_prog_act_c4r4,error)
    if (error) return
    call difference%act%register_c4c4toc4(cubecompute_difference_prog_act_c4c4,error)
    if (error) return
  end subroutine cubecompute_difference_register
  !
  subroutine cubecompute_difference_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(two2one_user_t) :: user
    character(len=*), parameter :: rname='DIFFERENCE>COMMAND'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call difference%parse(line,user,error)
    if (error) return
    call difference%main(user,error)
    if (error) continue
  end subroutine cubecompute_difference_command
  !
  subroutine cubecompute_difference_prog_unit(unit1,unit2,ouunit,error)
    !----------------------------------------------------------------------
    ! Set the proper unit of the output cube
    !----------------------------------------------------------------------
    character(len=*), intent(inout) :: unit1
    character(len=*), intent(inout) :: unit2
    character(len=*), intent(inout) :: ouunit
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: data_unit_null='---'  ! To be factorized
    character(len=*), parameter :: rname='DIFFERENCE>UNIT'
    !
    if (unit1.ne.unit2) then
       call cubecompute_message(seve%w,rname,'Mixing units '//trim(unit1)//' and '//unit2)
    endif
    ouunit = unit1
  end subroutine cubecompute_difference_prog_unit
  !
  !----------------------------------------------------------------------
  !
  subroutine cubecompute_difference_prog_act_r4r4(ie,subtrahend,minuend,difference,error)
    use cubetools_parameters
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(image_t),        intent(inout) :: subtrahend
    type(image_t),        intent(inout) :: minuend
    type(image_t),        intent(inout) :: difference
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='DIFFERENCE>PROG>ACT>R4R4'
    !
    include 'action-difference.inc'
  end subroutine cubecompute_difference_prog_act_r4r4
  !
  subroutine cubecompute_difference_prog_act_r4c4(ie,subtrahend,minuend,difference,error)
    use cubetools_parameters
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(image_t),        intent(inout) :: subtrahend
    type(visi_t),         intent(inout) :: minuend
    type(visi_t),         intent(inout) :: difference
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='DIFFERENCE>PROG>ACT>R4C4'
    !
    include 'action-difference.inc'
  end subroutine cubecompute_difference_prog_act_r4c4
  !
  subroutine cubecompute_difference_prog_act_c4r4(ie,subtrahend,minuend,difference,error)
    use cubetools_parameters
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(visi_t),         intent(inout) :: subtrahend
    type(image_t),        intent(inout) :: minuend
    type(visi_t),         intent(inout) :: difference
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='DIFFERENCE>PROG>ACT>C4R4'
    !
    include 'action-difference.inc'
  end subroutine cubecompute_difference_prog_act_c4r4
  !
  subroutine cubecompute_difference_prog_act_c4c4(ie,subtrahend,minuend,difference,error)
    use cubetools_parameters
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(visi_t),         intent(inout) :: subtrahend
    type(visi_t),         intent(inout) :: minuend
    type(visi_t),         intent(inout) :: difference
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='DIFFERENCE>PROG>ACT>C4C4'
    !
    include 'action-difference.inc'
  end subroutine cubecompute_difference_prog_act_c4c4
end module cubecompute_difference
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
