!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubecompute_language
  use cubetools_structure
  use cubecompute_messaging
  !
  use cubecompute_average
  use cubecompute_difference
  use cubecompute_product
  use cubecompute_ratio
  use cubecompute_sum
  !
!  use cubecompute_correlation
  use cubecompute_convolution
  !
  use cubecompute_amplitude
  use cubecompute_complex
  use cubecompute_conjugate
  use cubecompute_fft
  use cubecompute_phase
  !
  use cubecompute_cumulant
  !
  public :: cubecompute_register_language
  private
  !
  integer(kind=lang_k) :: langid
  !
contains
  !
  subroutine cubecompute_register_language(error)
    !----------------------------------------------------------------------
    ! Register the COMPUTE\ language
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    call cubetools_register_language('COMPUTE',&
         'J.Pety, S.Bardeau, V.deSouzaMagalhaes',&
         'Basic mathematical operations on a cube',&
         'gag_doc:hlp/cube-help-ima.hlp',&
         cubecompute_execute_command,langid,error)
    if (error) return
    !
    call cubecompute_sum_register(error)
    if (error) return
    call cubecompute_difference_register(error)
    if (error) return
    call cubecompute_product_register(error)
    if (error) return
    call cubecompute_ratio_register(error)
    if (error) return
    call cubecompute_average_register(error)
    if (error) return
    !
    call convolution%register(error)
    if (error) return
    !
    call fft%register(error)
    if (error) return
    call cubecompute_amplitude_register(error)
    if (error) return
    call cubecompute_phase_register(error)
    if (error) return
    call cubecompute_complex_register(error)
    if (error) return
    call cubecompute_conjugate_register(error)
    if (error) return
    !
    call cumulant%register(error)
    if (error) return
    !
    call cubetools_register_dict(error)
    if (error) return
  end subroutine cubecompute_register_language
  !
  subroutine cubecompute_execute_command(line,comm,error)
    use cubeadm_opened
    use cubeadm_timing
    !----------------------------------------------------------------------
    ! Execute a command of the COMPUTE\ language
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    character(len=*), intent(in)    :: comm
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='EXECUTE>COMMAND'
    !
    error = .false.
    if (comm.eq.'COMPUTE?') then
       call cubetools_list_language_commands(langid,error)
       if (error) return
    else
       call cubeadm_timing_init()
       call cubetools_execute_command(line,langid,comm,error)
       if (error) continue ! To ensure error recovery in the call to finalize_all
       call cubeadm_finish_all(comm,line,error)
       if (error) continue ! To ensure error recovery in timing
       call cubeadm_timing_final(comm)
    endif
  end subroutine cubecompute_execute_command
end module cubecompute_language
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
