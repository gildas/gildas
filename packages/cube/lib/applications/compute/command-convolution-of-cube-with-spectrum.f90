!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubecompute_convolution
  use cube_types
  use cubetools_parameters
  use cubetools_structure
  use cubetopology_cuberegion_types
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubecompute_messaging
  use cubecompute_convolve_spectrum_tool
  !
  public :: convolution
  private
  !
  type :: convolution_comm_t
     type(option_t), pointer :: comm
     type(cuberegion_comm_t) :: region
     type(cubeid_arg_t), pointer :: cube1
     type(cubeid_arg_t), pointer :: cube2
     type(cube_prod_t),  pointer :: convolved
   contains
     procedure, public  :: register => cubecompute_convolution_comm_register
     procedure, private :: parse    => cubecompute_convolution_comm_parse
     procedure, private :: main     => cubecompute_convolution_comm_main
  end type convolution_comm_t
  type(convolution_comm_t) :: convolution
  !
  type convolution_user_t
     type(cubeid_user_t)     :: cubeids
     type(cuberegion_user_t) :: region
   contains
     procedure, private :: toprog => cubecompute_convolution_user_toprog
  end type convolution_user_t
  !
  type convolution_prog_t
     type(cuberegion_prog_t) :: region
     type(cube_t), pointer   :: cube1
     type(cube_t), pointer   :: cube2
     type(cube_t), pointer   :: convolved
     type(convolve_spectrum_prog_t) :: convolve
   contains
     procedure, private :: header => cubecompute_convolution_prog_header
     procedure, private :: data   => cubecompute_convolution_prog_data
     procedure, private :: loop   => cubecompute_convolution_prog_loop
     procedure, private :: act    => cubecompute_convolution_prog_act
  end type convolution_prog_t
  !
contains
  !
  subroutine cubecompute_convolution_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(convolution_user_t) :: user
    character(len=*), parameter :: rname='CONVOLUTION>COMMAND'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call convolution%parse(line,user,error)
    if (error) return
    call convolution%main(user,error)
    if (error) continue
  end subroutine cubecompute_convolution_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubecompute_convolution_comm_register(comm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(convolution_comm_t), intent(inout) :: comm
    logical,                   intent(inout) :: error
    !
    type(cubeid_arg_t) :: incube
    type(cube_prod_t) :: convolved
    character(len=*), parameter :: comm_abstract='Convolve the spectral axis of one cube with one spectrum'
    character(len=*), parameter :: comm_help='Input and output cubes are real'
    character(len=*), parameter :: rname='CONVOLUTION>COMM>REGISTER'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    ! Syntax
    call cubetools_register_command(&
        'CONVOLUTION','[cubeid1] [spectrumid2]',&
         comm_abstract,&
         comm_help,&
         cubecompute_convolution_command,&
         comm%comm,&
         error)
    if (error) return
    call incube%register(&
         'CUBE',&
         'Cube',&
         strg_id,&
         code_arg_optional,&
         [flag_any],&
         code_read,&
         code_access_speset,&
         comm%cube1,&
         error)
    call incube%register(&
         'SPECTRUM',&
         'Spectrum',&
         strg_id,&
         code_arg_optional,&
         [flag_spectrum],&
         code_read,&
         code_access_speset,&
         comm%cube2,&
         error)
    if (error) return
    call comm%region%register(error)
    if (error) return
    !
    ! Products
    call convolved%register(&
         'CONVOLUTION',&
         'Convolved cube',&
         strg_id,&
         [flag_convolution],&
         comm%convolved,&
         error)
    if (error)  return
  end subroutine cubecompute_convolution_comm_register
  !
  subroutine cubecompute_convolution_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(convolution_comm_t), intent(in)    :: comm
    character(len=*),          intent(in)    :: line
    type(convolution_user_t),  intent(out)   :: user
    logical,                   intent(inout) :: error
    !
    character(len=*), parameter :: rname='CONVOLUTION>COMM>PARSE'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%region%parse(line,user%region,error)
    if (error) return
  end subroutine cubecompute_convolution_comm_parse
  !
  subroutine cubecompute_convolution_comm_main(comm,user,error) 
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(convolution_comm_t), intent(in)    :: comm
    type(convolution_user_t),  intent(inout) :: user
    logical,                   intent(inout) :: error
    !
    type(convolution_prog_t) :: prog
    character(len=*), parameter :: rname='CONVOLUTION>COMM>MAIN'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubecompute_convolution_comm_main
  !
  !------------------------------------------------------------------------
  !
  subroutine cubecompute_convolution_user_toprog(user,comm,prog,error)
    use cubetools_shape_types
    use cubetools_header_methods
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(convolution_user_t), intent(in)    :: user
    type(convolution_comm_t),  intent(in)    :: comm    
    type(convolution_prog_t),  intent(out)   :: prog
    logical,                   intent(inout) :: error
    !
    type(shape_t) :: n
    character(len=*), parameter :: rname='CONVOLUTION>USER>TOPROG'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%cube1,user%cubeids,prog%cube1,error)
    if (error) return
    !
    call cubeadm_get_header(comm%cube2,user%cubeids,prog%cube2,error)
    if (error) return
    call cubetools_header_get_array_shape(prog%cube2%head,n,error)
    if (error) return
    if ((n%l.ne.1).or.(n%m.ne.1)) then
       call cubecompute_message(seve%e,rname,'Not a single spectrum') 
       print *,"Spatial dimensions: ",n%l,n%m
       error = .true.
       return
    endif
    !    
    call user%region%toprog(prog%cube1,prog%region,error)
    if (error) return
    ! User feedback about the interpretation of his command line
    call prog%region%list(error)
    if (error) return
  end subroutine cubecompute_convolution_user_toprog
  !
  !------------------------------------------------------------------------
  !
  subroutine cubecompute_convolution_prog_header(prog,comm,error)
    use cubeadm_clone
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(convolution_prog_t), intent(inout) :: prog
    type(convolution_comm_t),  intent(in)    :: comm
    logical,                   intent(inout) :: error
    !
    character(len=*), parameter :: rname='CONVOLUTION>PROG>HEADER'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call cubeadm_clone_header(comm%convolved,prog%cube1,prog%convolved,error)
    if (error) return
    call prog%region%header(prog%convolved,error)
    if (error) return
  end subroutine cubecompute_convolution_prog_header
  !
  subroutine cubecompute_convolution_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(convolution_prog_t), intent(inout) :: prog
    logical,                   intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='CONVOLUTION>PROG>DATA'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo ! iter
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubecompute_convolution_prog_data
  !   
  subroutine cubecompute_convolution_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(convolution_prog_t), intent(inout) :: prog
    type(cubeadm_iterator_t),  intent(inout) :: iter
    logical,                   intent(inout) :: error
    !
    integer(kind=entr_k) :: one=1
    type(spectrum_t) :: spe1,spe2,convolved
    character(len=*), parameter :: rname='CONVOLUTION>PROG>LOOP'
    !
    call spe1%associate('spectrum #1',prog%cube1,iter,error)
    if (error) return
    call spe2%associate('spectrum #2',prog%cube2,iter,error)
    if (error) return
    call convolved%allocate('convolved spectrum',prog%convolved,iter,error)
    if (error) return
    call prog%convolve%init(spe1,spe2,error)
    if (error) return
    !
    call spe2%get(one,error)
    if (error) return
    do while (iter%iterate_entry(error))
       call prog%act(iter%ie,spe1,spe2,convolved,error)
       if (error) return
    enddo ! ie
  end subroutine cubecompute_convolution_prog_loop
  !   
  subroutine cubecompute_convolution_prog_act(prog,ie,spe1,spe2,convolved,error)
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(convolution_prog_t), intent(inout) :: prog
    integer(kind=entr_k),      intent(in)    :: ie
    type(spectrum_t),          intent(inout) :: spe1
    type(spectrum_t),          intent(inout) :: spe2
    type(spectrum_t),          intent(inout) :: convolved
    logical,                   intent(inout) :: error
    !
    character(len=*), parameter :: rname='CONVOLUTION>PROG>ACT'
    !
    call spe1%get(ie,error)
    if (error) return
    call prog%convolve%spectrum(spe1,spe2,convolved,error)
    if (error) return
    call convolved%put(ie,error)
    if (error) return
  end subroutine cubecompute_convolution_prog_act
end module cubecompute_convolution
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
