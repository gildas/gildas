!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubecompute_convolve_spectrum_tool
  use cubetools_parameters
  use cubetools_array_types
  use cubecompute_messaging
  !
  public :: convolve_spectrum_prog_t
  private
  !
  type convolve_spectrum_prog_t
     integer(kind=chan_k) :: half  = 0 ! 
     integer(kind=chan_k) :: first = 0 ! First fully convolved output channel
     integer(kind=chan_k) :: last  = 0 ! Last  fully convolved output channel
   contains
     procedure, public :: init     => cubecompute_convolve_spectrum_prog_init
!     procedure, public :: spectrum => cubecompute_correlate_spectrum_prog_compute
     procedure, public :: spectrum => cubecompute_convolve_spectrum_prog_compute
  end type convolve_spectrum_prog_t
  !
contains
  !
  subroutine cubecompute_convolve_spectrum_prog_init(correl,spec,kern,error)
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(convolve_spectrum_prog_t), intent(inout) :: correl
    type(spectrum_t),                intent(in)    :: spec
    type(spectrum_t),                intent(in)    :: kern
    logical,                         intent(inout) :: error
    !
    character(len=*), parameter :: rname='CONVOLVE>SPECTRUM>PROG>INIT'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call check_size('first',spec,error)
    if (error) return
    call check_size('second',kern,error)
    if (error) return
    if (spec%n.lt.kern%n) then
       call cubecompute_message(seve%e,rname,'2nd spectrum larger than 1st spectrum => Swap their order')
       error = .true.
       return
    endif
    ! correlation
!!$    correl%half  = kern%n/2
!!$    correl%first =        1+correl%half
!!$    correl%last  = spec%n+1-correl%half
    !
    ! convolution
    correl%half  = kern%n/2
    correl%first =        correl%half
    correl%last  = spec%n-correl%half
    !
  contains
    !
    subroutine check_size(rank,spe,error)
      character(len=0), intent(in)    :: rank
      type(spectrum_t), intent(in)    :: spe
      logical,          intent(inout) :: error
      !
      if (spe%n.le.0) then
         call cubecompute_message(seve%e,rname,'Zero or negative sized '//trim(rank)//' spectrum')
         error = .true.
         return
      endif
    end subroutine check_size
  end subroutine cubecompute_convolve_spectrum_prog_init
  !
  subroutine cubecompute_convolve_spectrum_prog_compute(correl,spec,kern,out,error)
    use cubetools_nan
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    ! Convolution!
    !----------------------------------------------------------------------
    class(convolve_spectrum_prog_t),         intent(in)    :: correl
    type(spectrum_t),                target, intent(in)    :: spec
    type(spectrum_t),                target, intent(in)    :: kern
    type(spectrum_t),                target, intent(inout) :: out
    logical,                                 intent(inout) :: error
    !
    integer(kind=chan_k) :: isp,iou,ishift
    real(kind=sign_k), pointer :: yspec(:),ykern(:),yout
    character(len=*), parameter :: rname='CONVOLVE>SPECTRUM>PROG>COMPUTE'
    !
    yspec => spec%y%val(:)
    ykern => kern%y%val(:)
    !
    out%y%val(:) = 0
    do iou=1,out%n
       yout => out%y%val(iou)
       if (iou.lt.correl%first) then
          ishift = iou+correl%half+1
          do isp=1,iou+correl%half
!             print *,'>>>>>',iou,isp,ishift,ishift-isp
             yout = yout+ykern(ishift-isp)*yspec(isp)
          enddo ! ik
       else if ((correl%first.le.iou).and.(iou.le.correl%last)) then
          ishift = iou+correl%first+1
          do isp=iou-correl%half+1,iou+correl%half
!             print *,'=====',iou,isp,ishift,ishift-isp
             yout = yout+ykern(ishift-isp)*yspec(isp)
          enddo ! ik
       else
          ishift = iou+correl%last+1
          do isp=iou-correl%half+1,spec%n
!             print *,'<<<<<',iou,isp,ishift,ishift-isp
             yout = yout+ykern(ishift-isp)*yspec(isp)
          enddo ! ik
       endif
    enddo ! io
  end subroutine cubecompute_convolve_spectrum_prog_compute
  !
!!$  subroutine cubecompute_correlate_spectrum_prog_compute(correl,spec,kern,out,error)
!!$    use cubetools_nan
!!$    use cubeadm_spectrum_types
!!$    !----------------------------------------------------------------------
!!$    ! Correlation
!!$    !----------------------------------------------------------------------
!!$    class(correlate_spectrum_prog_t),         intent(in)    :: correl
!!$    type(spectrum_t),                 target, intent(in)    :: spec
!!$    type(spectrum_t),                 target, intent(in)    :: kern
!!$    type(spectrum_t),                 target, intent(inout) :: out
!!$    logical,                                  intent(inout) :: error
!!$    !
!!$    integer(kind=chan_k) :: isp,iou,ishift
!!$    real(kind=sign_k), pointer :: yspec(:),ykern(:),yout
!!$    character(len=*), parameter :: rname='CORRELATE>SPECTRUM>PROG>COMPUTE'
!!$    !
!!$    yspec => spec%y%val(:)
!!$    ykern => kern%y%val(:)
!!$    !
!!$    out%y%val(:) = 0
!!$    do iou=1,out%n
!!$       yout => out%y%val(iou)
!!$       if (iou.lt.correl%first) then
!!$          ishift = correl%half-(iou-1)
!!$          do isp=1,iou+(correl%half-1)
!!$             print *,'>>>>>',iou,isp,ishift,isp+ishift
!!$             yout = yout+ykern(isp+ishift)*yspec(isp)
!!$          enddo ! ik
!!$       else if ((correl%first.le.iou).and.(iou.le.correl%last)) then
!!$          ishift = iou-correl%first
!!$          do isp=iou-correl%half,iou+(correl%half-1)
!!$             print *,'=====',iou,isp,ishift,isp+ishift
!!$             yout = yout+ykern(isp+ishift)*yspec(isp)
!!$          enddo ! ik
!!$       else
!!$          ishift = correl%last-iou
!!$          do isp=iou-correl%half,spec%n
!!$             print *,'<<<<<',iou,isp,ishift,isp+ishift
!!$             yout = yout+ykern(isp+ishift)*yspec(isp)
!!$          enddo ! ik
!!$       endif
!!$    enddo ! io
!!$  end subroutine cubecompute_correlate_spectrum_prog_compute
end module cubecompute_convolve_spectrum_tool
! 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
