    !-------------------------------------------------------------------
    ! Action code for RATIO command
    !-------------------------------------------------------------------
    !
    call numerator%get(ie,error)
    if (error) return
    call denominator%get(ie,error)
    if (error) return
    !
    do iy=1,ratio%ny
       do ix=1,ratio%nx
          if (denominator%val(ix,iy).eq.0.0) then  ! Test for R*4 or C*4 value
             ratio%val(ix,iy) = nanval
          else
             ratio%val(ix,iy) = numerator%val(ix,iy)/denominator%val(ix,iy)
          endif
       enddo ! ix
    enddo ! iy
    !
    call ratio%put(ie,error)
    if (error) return
