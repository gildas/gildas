!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubecompute_average
  use cubecompute_two2one_template
  use cubecompute_messaging
  !
  public :: cubecompute_average_register
  private
  !
  type(two2one_comm_t) :: average
  !
contains
  !
  subroutine cubecompute_average_register(error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    character(len=*), parameter :: rname='AVERAGE>REGISTER'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call average%register_syntax(&
         'AVERAGE','operand1','operand2',&
         [flag_average],cubecompute_average_command,error)
    if (error) return
    !
    call average%unit%register(cubecompute_average_prog_unit,error)
    if (error) return
    !
    call average%act%register_r4r4tor4(cubecompute_average_prog_act_r4r4,error)
    if (error) return
    call average%act%register_r4c4toc4(cubecompute_average_prog_act_r4c4,error)
    if (error) return
    call average%act%register_c4r4toc4(cubecompute_average_prog_act_c4r4,error)
    if (error) return
    call average%act%register_c4c4toc4(cubecompute_average_prog_act_c4c4,error)
    if (error) return
  end subroutine cubecompute_average_register
  !
  subroutine cubecompute_average_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(two2one_user_t) :: user
    character(len=*), parameter :: rname='AVERAGE>COMMAND'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call average%parse(line,user,error)
    if (error) return
    call average%main(user,error)
    if (error) continue
  end subroutine cubecompute_average_command
  !
  subroutine cubecompute_average_prog_unit(unit1,unit2,ouunit,error)
    !----------------------------------------------------------------------
    ! Set the proper unit of the output cube
    !----------------------------------------------------------------------
    character(len=*), intent(inout) :: unit1
    character(len=*), intent(inout) :: unit2
    character(len=*), intent(inout) :: ouunit
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='AVERAGE>UNIT'
    !
    if (unit1.ne.unit2) then
       call cubecompute_message(seve%w,rname,'Mixing units '//trim(unit1)//' and '//unit2)
    endif
    ouunit = unit1
  end subroutine cubecompute_average_prog_unit
  !
  !----------------------------------------------------------------------
  !
  subroutine cubecompute_average_prog_act_r4r4(ie,operand1,operand2,average,error)
    use cubetools_parameters
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(image_t),        intent(inout) :: operand1
    type(image_t),        intent(inout) :: operand2
    type(image_t),        intent(inout) :: average
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='AVERAGE>PROG>ACT>R4R4'
    !
    include 'action-average.inc'
  end subroutine cubecompute_average_prog_act_r4r4
  !
  subroutine cubecompute_average_prog_act_r4c4(ie,operand1,operand2,average,error)
    use cubetools_parameters
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(image_t),        intent(inout) :: operand1
    type(visi_t),         intent(inout) :: operand2
    type(visi_t),         intent(inout) :: average
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='AVERAGE>PROG>ACT>R4C4'
    !
    include 'action-average.inc'
  end subroutine cubecompute_average_prog_act_r4c4
  !
  subroutine cubecompute_average_prog_act_c4r4(ie,operand1,operand2,average,error)
    use cubetools_parameters
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(visi_t),         intent(inout) :: operand1
    type(image_t),        intent(inout) :: operand2
    type(visi_t),         intent(inout) :: average
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='AVERAGE>PROG>ACT>C4R4'
    !
    include 'action-average.inc'
  end subroutine cubecompute_average_prog_act_c4r4
  !
  subroutine cubecompute_average_prog_act_c4c4(ie,operand1,operand2,average,error)
    use cubetools_parameters
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(visi_t),         intent(inout) :: operand1
    type(visi_t),         intent(inout) :: operand2
    type(visi_t),         intent(inout) :: average
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='AVERAGE>PROG>ACT>C4C4'
    !
    include 'action-average.inc'
  end subroutine cubecompute_average_prog_act_c4c4
end module cubecompute_average
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
