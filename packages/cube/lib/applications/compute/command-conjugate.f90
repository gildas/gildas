!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubecompute_conjugate
  use cubecompute_one2one_template
  use cubecompute_messaging
  !
  public :: cubecompute_conjugate_register
  private
  !
  type(one2one_comm_t) :: conjugate
  !
contains
  !
  subroutine cubecompute_conjugate_register(error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    character(len=*), parameter :: rname='CONJUGATE>REGISTER'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call conjugate%register_syntax(&
         'CONJUGATE','input',&
         [flag_conjugate],cubecompute_conjugate_command,error)
    if (error) return
    call conjugate%act%register_c4toc4(cubecompute_conjugate_prog_act,error)
    if (error) return
  end subroutine cubecompute_conjugate_register
  !
  subroutine cubecompute_conjugate_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(one2one_user_t) :: user
    character(len=*), parameter :: rname='CONJUGATE>COMMAND'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call conjugate%parse(line,user,error)
    if (error) return
    call conjugate%main(user,error)
    if (error) continue
  end subroutine cubecompute_conjugate_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubecompute_conjugate_prog_act(ie,input,conjugate,error)
    use cubetools_parameters
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(visi_t),         intent(inout) :: input
    type(visi_t),         intent(inout) :: conjugate
    logical,              intent(inout) :: error
    !
    integer(kind=pixe_k) :: ix,iy
    character(len=*), parameter :: rname='CONJUGATE>PROG>ACT'
    !
    call input%get(ie,error)
    if (error) return
    do iy=1,conjugate%ny
       do ix=1,conjugate%nx
          conjugate%val(ix,iy) = conjg(input%val(ix,iy))
       enddo ! ix
    enddo ! iy
    call conjugate%put(ie,error)
    if (error) return
  end subroutine cubecompute_conjugate_prog_act
end module cubecompute_conjugate
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
