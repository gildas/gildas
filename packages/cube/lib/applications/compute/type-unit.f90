!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubecompute_two2one_unit
  use cubecompute_messaging

  public :: two2one_unit_comm_t,two2one_unit_prog_t
  private
  !
  type two2one_unit_comm_t
    procedure(cubecompute_two2one_unit_set), pointer, nopass, public :: set => null()
  contains
    procedure, public :: register => cubecompute_two2one_unit_register
  end type two2one_unit_comm_t
  !
  type two2one_unit_prog_t
    procedure(cubecompute_two2one_unit_set), pointer, nopass, public :: set => null()
  end type two2one_unit_prog_t
  !
contains
  !
  subroutine cubecompute_two2one_unit_set(unit1,unit2,ouunit,error)
    !----------------------------------------------------------------------
    ! Hook which applies the proper unit to the output cube
    !----------------------------------------------------------------------
    character(len=*), intent(inout) :: unit1
    character(len=*), intent(inout) :: unit2
    character(len=*), intent(inout) :: ouunit
    logical,          intent(inout) :: error
  end subroutine cubecompute_two2one_unit_set
  !
  subroutine cubecompute_two2one_unit_register(comm,set,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(two2one_unit_comm_t),              intent(inout) :: comm
    procedure(cubecompute_two2one_unit_set)                :: set
    logical,                                 intent(inout) :: error
    !
    character(len=*), parameter :: rname='TWO2ONE>UNIT>REGISTER'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    comm%set => set
  end subroutine cubecompute_two2one_unit_register

end module cubecompute_two2one_unit
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
