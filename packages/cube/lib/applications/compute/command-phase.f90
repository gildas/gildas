!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubecompute_phase
  use cubecompute_one2one_template
  use cubecompute_messaging
  !
  public :: cubecompute_phase_register
  private
  !
  type(one2one_comm_t) :: phase
  !
contains
  !
  subroutine cubecompute_phase_register(error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    character(len=*), parameter :: rname='PHASE>REGISTER'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call phase%register_syntax(&
         'PHASE','input',&
         [flag_phase],cubecompute_phase_command,error)
    if (error) return
    call phase%act%register_c4tor4(cubecompute_phase_prog_act,error)
    if (error) return
  end subroutine cubecompute_phase_register
  !
  subroutine cubecompute_phase_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(one2one_user_t) :: user
    character(len=*), parameter :: rname='PHASE>COMMAND'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call phase%parse(line,user,error)
    if (error) return
    call phase%main(user,error)
    if (error) return
  end subroutine cubecompute_phase_command
  !
  subroutine cubecompute_phase_prog_act(ie,visi,image,error)
    use cubetools_parameters
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(visi_t),         intent(inout) :: visi
    type(image_t),        intent(inout) :: image
    logical,              intent(inout) :: error
    !
    integer(kind=pixe_k) :: ix,iy
    character(len=*), parameter :: rname='PHASE>PROG>ACT'
    !
    call visi%get(ie,error)
    if (error) return
    do iy=1,visi%ny
       do ix=1,visi%nx
          image%val(ix,iy) = atan2(aimag(visi%val(ix,iy)),real(visi%val(ix,iy)))
       enddo ! ix
    enddo ! iy
    call image%put(ie,error)
    if (error) return
  end subroutine cubecompute_phase_prog_act
end module cubecompute_phase
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
