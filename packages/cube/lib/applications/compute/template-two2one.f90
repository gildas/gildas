!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubecompute_two2one_template
  use gbl_format
  use cube_types
  use cubetools_parameters
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubetopology_cuberegion_types
  use cubecompute_messaging
  use cubecompute_two2one_act
  use cubecompute_two2one_unit
  !
  public :: two2one_comm_t,two2one_user_t,two2one_prog_t
  private
  !
  type two2one_comm_t
     type(option_t), pointer :: comm
     type(cuberegion_comm_t) :: region
     type(cubeid_arg_t), pointer :: incube1
     type(cubeid_arg_t), pointer :: incube2
     type(cube_prod_t),  pointer :: oucube
     type(two2one_unit_comm_t) :: unit
     type(two2one_act_comm_t) :: act
   contains
     procedure, public :: register_syntax => cubecompute_two2one_register_syntax
     procedure, public :: parse           => cubecompute_two2one_parse
     procedure, public :: main            => cubecompute_two2one_main
  end type two2one_comm_t
  !
  type two2one_user_t
     type(cubeid_user_t)     :: cubeids
     type(cuberegion_user_t) :: region
   contains
     procedure, private :: toprog => cubecompute_two2one_user_toprog
  end type two2one_user_t
  !
  type two2one_prog_t
     type(cuberegion_prog_t) :: region
     type(cube_t), pointer :: incube1
     type(cube_t), pointer :: incube2
     type(cube_t), pointer :: oucube
     type(two2one_unit_prog_t) :: unit
     type(two2one_act_prog_t) :: act
     ! Dynamically resolved from the current input cubes:
     integer(kind=code_k), private :: type  ! Type of the output cube
     procedure(cubecompute_two2one_prog_loop), pointer, private :: loop => null()
   contains
     procedure, private :: header => cubecompute_two2one_prog_header
     procedure, private :: data   => cubecompute_two2one_prog_data
     procedure, private :: select_loop => cubecompute_two2one_prog_select_loop
  end type two2one_prog_t
  !
contains
  !
  subroutine cubecompute_two2one_register_syntax(comm,opername,cube1name,cube2name,operflags,opercomm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(two2one_comm_t), intent(inout)    :: comm
    character(len=*),      intent(in)       :: opername
    character(len=*),      intent(in)       :: cube1name
    character(len=*),      intent(in)       :: cube2name
    type(flag_t),          intent(in)       :: operflags(:)
    external                                :: opercomm
    logical,               intent(inout)    :: error
    !
    type(cubeid_arg_t) :: incube
    type(cube_prod_t) :: oucube
    character(len=*), parameter :: rname='TWO2ONE>REGISTER>SYNTAX'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    ! Syntax
    call cubetools_register_command(&
         opername,trim(cube1name)//' '//trim(cube2name),&
         'Compute the '//trim(opername)//' of two cubes',&
         'Input and output cubes must be complex',&
         opercomm,&
         comm%comm,error)
    if (error) return
    call incube%register(&
         cube1name,&
         trim(cube1name)//' cube',&
         strg_id,&
         code_arg_mandatory,&
         [flag_any],&
         code_read,&
         code_access_imaset,&
         comm%incube1,&
         error)
    if (error) return
    call incube%register(&
         cube2name,&
         trim(cube2name)//' cube',&
         strg_id,&
         code_arg_mandatory,&
         [flag_any],&
         code_read,&
         code_access_imaset,&
         comm%incube2,&
         error)
    if (error) return
    !
    call comm%region%register(error)
    if (error) return
    !
    ! Product
    call oucube%register(&
         opername,&
         'Output cube of the operation',&
         strg_id,&
         operflags,&
         comm%oucube,&
         error)
    if (error)  return
  end subroutine cubecompute_two2one_register_syntax
  !
  subroutine cubecompute_two2one_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! TWO2ONE cube1name cube2name
    ! /SIZE sx [sy]
    ! /CENTER xcen ycen
    ! /RANGE zfirst zlast
    !----------------------------------------------------------------------
    class(two2one_comm_t), intent(in)    :: comm
    character(len=*),      intent(in)    :: line
    type(two2one_user_t),  intent(out)   :: user
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='TWO2ONE>PARSE'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%region%parse(line,user%region,error)
    if (error) return
  end subroutine cubecompute_two2one_parse
  !
  subroutine cubecompute_two2one_main(comm,user,error)
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(two2one_comm_t), intent(in)    :: comm
    type(two2one_user_t),  intent(inout) :: user
    logical,               intent(inout) :: error
    !
    type(two2one_prog_t) :: prog
    character(len=*), parameter :: rname='TWO2ONE>MAIN'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubecompute_two2one_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubecompute_two2one_user_toprog(user,comm,prog,error)
    use cubetools_consistency_methods
    use cubeadm_get
    !----------------------------------------------------------------------
    ! Only the dimension of the cubes are checked here on purpose for
    ! this very basic command
    !----------------------------------------------------------------------
    class(two2one_user_t), intent(in)    :: user
    type(two2one_comm_t),  intent(in)    :: comm
    type(two2one_prog_t),  intent(out)   :: prog
    logical,               intent(inout) :: error
    !
    logical :: conspb
    character(len=*), parameter :: rname='TWO2ONE>USER>TOPROG'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%incube1,user%cubeids,prog%incube1,error)
    if (error) return
    call cubeadm_get_header(comm%incube2,user%cubeids,prog%incube2,error)
    if (error) return
    !
    conspb = .false.
    call cubetools_consistency_shape(&
         'Input cube #1',prog%incube1%head,&
         'Input cube #2',prog%incube2%head,&
         conspb,error)
    if (error) return
    if (cubetools_consistency_failed(rname,conspb,error)) return
    !
    call user%region%toprog(prog%incube1,prog%region,error)
    if (error) return
    call prog%region%list(error)
    if (error) return
    !
    prog%unit%set     => comm%unit%set
    !
    prog%act%r4r4tor4 => comm%act%r4r4tor4
    prog%act%r4r4toc4 => comm%act%r4r4toc4
    prog%act%r4c4toc4 => comm%act%r4c4toc4
    prog%act%c4r4toc4 => comm%act%c4r4toc4
    prog%act%c4c4toc4 => comm%act%c4c4toc4
  end subroutine cubecompute_two2one_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubecompute_two2one_prog_header(prog,comm,error)
    use cubeadm_clone
    use cubetools_header_methods
    !----------------------------------------------------------------------
    ! For the moment, the header of the numerator is copied. On the longer
    ! term, only the common part of the header could/should be copied. The
    ! other ones should stay to unknown, as the user would be able to get
    ! the information by listing the headers of the parent cubes.
    !----------------------------------------------------------------------
    class(two2one_prog_t), intent(inout) :: prog
    type(two2one_comm_t),  intent(in)    :: comm
    logical,               intent(inout) :: error
    !
    character(len=unit_l) :: unit1,unit2,ouunit
    character(len=*), parameter :: rname='TWO2ONE>PROG>HEADER'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call prog%select_loop(error)
    if (error)  return
    !
    call cubeadm_clone_header_with_region(comm%oucube,  &
      prog%incube1,prog%region,prog%oucube,error)
    if (error) return
    if (prog%type.eq.fmt_c4) then
      call cubetools_header_make_array_cplx(prog%oucube%head,error)
      if (error) return
    elseif (prog%type.eq.fmt_r4) then
      call cubetools_header_make_array_real(prog%oucube%head,error)
      if (error) return
    endif
    !
    if (associated(prog%unit%set)) then
      call cubetools_header_get_array_unit(prog%incube1%head,unit1,error)
      if (error) return
      call cubetools_header_get_array_unit(prog%incube2%head,unit2,error)
      if (error) return
      call prog%unit%set(unit1,unit2,ouunit,error)
      if (error) return
      call cubetools_header_put_array_unit(ouunit,prog%oucube%head,error)
      if (error) return
    endif
  end subroutine cubecompute_two2one_prog_header
  !
  subroutine cubecompute_two2one_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(two2one_prog_t), intent(inout) :: prog
    logical,               intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='TWO2ONE>PROG>DATA'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,prog%region,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo ! iter
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubecompute_two2one_prog_data
  !
  subroutine cubecompute_two2one_prog_select_loop(prog,error)
    !----------------------------------------------------------------------
    ! Dynamic selection of the loop engine: for the current pair of input
    ! cubes, select the loop which has proper arguments (image_t vs
    ! visi_t).
    !----------------------------------------------------------------------
    class(two2one_prog_t), intent(inout) :: prog
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='TWO2ONE>PROG>LOOP'
    character(len=2) :: combo
    !
    if (prog%incube1%iscplx()) then
      combo(1:1) = 'C'
    else
      combo(1:1) = 'R'
    endif
    if (prog%incube2%iscplx()) then
      combo(2:2) = 'C'
    else
      combo(2:2) = 'R'
    endif
    !
    select case (combo)
    case ('RR')
      if (associated(prog%act%r4r4tor4)) then
        prog%type = fmt_r4
        prog%loop => cubecompute_two2one_prog_loop_r4r4tor4
      elseif (associated(prog%act%r4r4toc4)) then
        prog%type = fmt_c4
        prog%loop => cubecompute_two2one_prog_loop_r4r4toc4
      else
        call cubecompute_message(seve%e,rname,  &
          'This command does not offer transformation from two R*4 cubes')
        error = .true.
        return
      endif
    case ('RC')
      if (associated(prog%act%r4c4toc4)) then
        prog%type = fmt_c4
        prog%loop => cubecompute_two2one_prog_loop_r4c4toc4
      else
        call cubecompute_message(seve%e,rname,  &
          'This command does not offer transformation from a R*4 and a C*4 cube')
        error = .true.
        return
      endif
    case ('CR')
      if (associated(prog%act%c4r4toc4)) then
        prog%type = fmt_c4
        prog%loop => cubecompute_two2one_prog_loop_c4r4toc4
      else
        call cubecompute_message(seve%e,rname,  &
          'This command does not offer transformation from a C*4 and a R*4 cube')
        error = .true.
        return
      endif
    case ('CC')
      if (associated(prog%act%c4c4toc4)) then
        prog%type = fmt_c4
        prog%loop => cubecompute_two2one_prog_loop_c4c4toc4
      else
        call cubecompute_message(seve%e,rname,  &
          'This command does not offer transformation from two C*4 cubes')
        error = .true.
        return
      endif
    case default
      call cubecompute_message(seve%e,rname,'Unexpected cube data types')
      error = .true.
      return
    end select
  end subroutine cubecompute_two2one_prog_select_loop
  !
  subroutine cubecompute_two2one_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    !----------------------------------------------------------------------
    ! Provides the interface for the prog loop subroutine
    !----------------------------------------------------------------------
    class(two2one_prog_t),    intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
  end subroutine cubecompute_two2one_prog_loop
  !
  subroutine cubecompute_two2one_prog_loop_r4r4tor4(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(two2one_prog_t),    intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(image_t) :: inima1,inima2,ouima
    character(len=*), parameter :: rname='TWO2ONE>PROG>LOOP>R4R4TOR4'
    !
    call inima1%associate('input cube #1',prog%incube1,iter,error)
    if (error) return
    call inima2%associate('input cube #2',prog%incube2,iter,error)
    if (error) return
    call ouima%allocate('output cube',prog%oucube,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
       call prog%act%r4r4tor4(iter%ie,inima1,inima2,ouima,error)
    enddo ! ie
  end subroutine cubecompute_two2one_prog_loop_r4r4tor4
  !
  subroutine cubecompute_two2one_prog_loop_r4r4toc4(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(two2one_prog_t),    intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(image_t) :: inima1,inima2
    type(visi_t) :: ouima
    character(len=*), parameter :: rname='TWO2ONE>PROG>LOOP>R4R4TOC4'
    !
    call inima1%associate('input cube #1',prog%incube1,iter,error)
    if (error) return
    call inima2%associate('input cube #2',prog%incube2,iter,error)
    if (error) return
    call ouima%allocate('output cube',prog%oucube,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
       call prog%act%r4r4toc4(iter%ie,inima1,inima2,ouima,error)
    enddo ! ie
  end subroutine cubecompute_two2one_prog_loop_r4r4toc4
  !
  subroutine cubecompute_two2one_prog_loop_r4c4toc4(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(two2one_prog_t),    intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(image_t) :: inima1
    type(visi_t) :: inima2,ouima
    character(len=*), parameter :: rname='TWO2ONE>PROG>LOOP>R4C4TOC4'
    !
    call inima1%associate('input cube #1',prog%incube1,iter,error)
    if (error) return
    call inima2%associate('input cube #2',prog%incube2,iter,error)
    if (error) return
    call ouima%allocate('output cube',prog%oucube,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
       call prog%act%r4c4toc4(iter%ie,inima1,inima2,ouima,error)
    enddo ! ie
  end subroutine cubecompute_two2one_prog_loop_r4c4toc4
  !
  subroutine cubecompute_two2one_prog_loop_c4r4toc4(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(two2one_prog_t),    intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(visi_t) :: inima1,ouima
    type(image_t) :: inima2
    character(len=*), parameter :: rname='TWO2ONE>PROG>LOOP>C4R4TOC4'
    !
    call inima1%associate('input cube #1',prog%incube1,iter,error)
    if (error) return
    call inima2%associate('input cube #2',prog%incube2,iter,error)
    if (error) return
    call ouima%allocate('output cube',prog%oucube,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
       call prog%act%c4r4toc4(iter%ie,inima1,inima2,ouima,error)
    enddo ! ie
  end subroutine cubecompute_two2one_prog_loop_c4r4toc4
  !
  subroutine cubecompute_two2one_prog_loop_c4c4toc4(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(two2one_prog_t),    intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(visi_t) :: inima1,inima2,ouima
    character(len=*), parameter :: rname='TWO2ONE>PROG>LOOP>C4C4TOC4'
    !
    call inima1%associate('input cube #1',prog%incube1,iter,error)
    if (error) return
    call inima2%associate('input cube #2',prog%incube2,iter,error)
    if (error) return
    call ouima%allocate('output cube',prog%oucube,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
       call prog%act%c4c4toc4(iter%ie,inima1,inima2,ouima,error)
    enddo ! ie
  end subroutine cubecompute_two2one_prog_loop_c4c4toc4
end module cubecompute_two2one_template
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
