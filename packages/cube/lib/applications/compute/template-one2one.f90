!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubecompute_one2one_template
  use gbl_format
  use cube_types
  use cubetools_parameters
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubetopology_cuberegion_types
  use cubecompute_messaging
  use cubecompute_one2one_act
  !
  public :: one2one_comm_t,one2one_user_t,one2one_prog_t
  private
  !
  type one2one_comm_t
     type(option_t), pointer :: comm
     type(cuberegion_comm_t) :: region
     type(cubeid_arg_t), pointer :: incube
     type(cube_prod_t),  pointer :: oucube
     type(one2one_act_comm_t) :: act
   contains
     procedure, public :: register_syntax => cubecompute_one2one_register_syntax
     procedure, public :: parse           => cubecompute_one2one_parse
     procedure, public :: main            => cubecompute_one2one_main
  end type one2one_comm_t
  !
  type one2one_user_t
     type(cubeid_user_t)     :: cubeids
     type(cuberegion_user_t) :: region
   contains
     procedure, private :: toprog => cubecompute_one2one_user_toprog
  end type one2one_user_t
  !
  type one2one_prog_t
     type(cuberegion_prog_t) :: region
     type(cube_t), pointer :: incube
     type(cube_t), pointer :: oucube
     type(one2one_act_prog_t) :: act
     ! Dynamically resolved from the current input cube:
     integer(kind=code_k), private :: type  ! Type of the output cube
     procedure(cubecompute_one2one_prog_loop), pointer, private :: loop => null()
   contains
     procedure, private :: header => cubecompute_one2one_prog_header
     procedure, private :: data   => cubecompute_one2one_prog_data
     procedure, private :: select_loop => cubecompute_one2one_prog_select_loop
  end type one2one_prog_t
  !
contains
  !
  subroutine cubecompute_one2one_register_syntax(comm,opername,cubename,operflags,opercomm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(one2one_comm_t), intent(inout) :: comm
    character(len=*),      intent(in)    :: opername
    character(len=*),      intent(in)    :: cubename
    type(flag_t),          intent(in)    :: operflags(:)
    external                             :: opercomm
    logical,               intent(inout) :: error
    !
    type(cubeid_arg_t) :: incube
    type(cube_prod_t) :: oucube
    character(len=*), parameter :: rname='ONE2ONE>REGISTER>SYNTAX'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    ! Syntax
    call cubetools_register_command(&
         opername,trim(cubename),&
         'Compute the '//trim(opername)//' of a cube',&
         strg_id,&
         opercomm,&
         comm%comm,error)
    if (error) return
    call incube%register(&
         cubename,&
         trim(cubename)//' cube',&
         strg_id,&
         code_arg_optional,&
         [flag_any],&
         code_read,&
         code_access_imaset,&
         comm%incube,&
         error)
    if (error) return
    !
    call comm%region%register(error)
    if (error) return
    !
    ! Product
    call oucube%register(&
         opername,&
         'Output cube of the operation',&
         strg_id,&
         operflags,&
         comm%oucube,&
         error)
    if (error)  return
  end subroutine cubecompute_one2one_register_syntax
  !
  subroutine cubecompute_one2one_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! ONE2ONE cubename
    ! /SIZE sx [sy]
    ! /CENTER xcen ycen
    ! /RANGE zfirst zlast
    !----------------------------------------------------------------------
    class(one2one_comm_t), intent(in)    :: comm
    character(len=*),      intent(in)    :: line
    type(one2one_user_t),  intent(out)   :: user
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='ONE2ONE>PARSE'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%region%parse(line,user%region,error)
    if (error) return
  end subroutine cubecompute_one2one_parse
  !
  subroutine cubecompute_one2one_main(comm,user,error)
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(one2one_comm_t), intent(in)    :: comm
    type(one2one_user_t),  intent(inout) :: user
    logical,               intent(inout) :: error
    !
    type(one2one_prog_t) :: prog
    character(len=*), parameter :: rname='ONE2ONE>MAIN'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubecompute_one2one_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubecompute_one2one_user_toprog(user,comm,prog,error)
    use cubetools_consistency_methods
    use cubeadm_get
    !----------------------------------------------------------------------
    ! Only the dimension of the cubes are checked here on purpose for
    ! this very basic command
    !----------------------------------------------------------------------
    class(one2one_user_t), intent(in)    :: user
    type(one2one_comm_t),  intent(in)    :: comm
    type(one2one_prog_t),  intent(out)   :: prog
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='ONE2ONE>USER>TOPROG'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%incube,user%cubeids,prog%incube,error)
    if (error) return
    !
    call user%region%toprog(prog%incube,prog%region,error)
    if (error) return
    call prog%region%list(error)
    if (error) return
    !
    prog%act%r4tor4 => comm%act%r4tor4
    prog%act%c4tor4 => comm%act%c4tor4
    prog%act%c4toc4 => comm%act%c4toc4
  end subroutine cubecompute_one2one_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubecompute_one2one_prog_header(prog,comm,error)
    use cubeadm_clone
    use cubetools_header_methods
    !----------------------------------------------------------------------

    !----------------------------------------------------------------------
    class(one2one_prog_t), intent(inout) :: prog
    type(one2one_comm_t),  intent(in)    :: comm
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='ONE2ONE>PROG>HEADER'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call prog%select_loop(error)
    if (error)  return
    !
    call cubeadm_clone_header_with_region(comm%oucube,  &
      prog%incube,prog%region,prog%oucube,error)
    if (error) return
    if (prog%type.eq.fmt_r4) then
      call cubetools_header_make_array_real(prog%oucube%head,error)
      if (error) return
    elseif (prog%type.eq.fmt_c4) then
      call cubetools_header_make_array_cplx(prog%oucube%head,error)
      if (error) return
    endif
  end subroutine cubecompute_one2one_prog_header
  !
  subroutine cubecompute_one2one_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(one2one_prog_t), intent(inout) :: prog
    logical,               intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='ONE2ONE>PROG>DATA'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,prog%region,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo ! iter
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubecompute_one2one_prog_data
  !
  subroutine cubecompute_one2one_prog_select_loop(prog,error)
    !----------------------------------------------------------------------
    ! Dynamic selection of the loop engine: for the current input cube,
    ! select the loop which has proper argument (image_t vs visi_t).
    !----------------------------------------------------------------------
    class(one2one_prog_t), intent(inout) :: prog
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='ONE2ONE>PROG>LOOP'
    !
    if (prog%incube%iscplx()) then
      ! ZZZ We should check that only one of them is available
      if (associated(prog%act%c4toc4)) then
        prog%type = fmt_c4
        prog%loop => cubecompute_one2one_prog_loop_c4toc4
      elseif (associated(prog%act%c4tor4)) then
        prog%type = fmt_r4
        prog%loop => cubecompute_one2one_prog_loop_c4tor4
      else
        call cubecompute_message(seve%e,rname,  &
          'This command does not offer transformation from a C*4 cube')
        error = .true.
        return
      endif
    else
      if (associated(prog%act%r4tor4)) then
        prog%type = fmt_r4
        prog%loop => cubecompute_one2one_prog_loop_r4tor4
      else
        call cubecompute_message(seve%e,rname,  &
          'This command does not offer transformation from a R*4 cube')
        error = .true.
        return
      endif
    endif
  end subroutine cubecompute_one2one_prog_select_loop
  !
  subroutine cubecompute_one2one_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    !----------------------------------------------------------------------
    ! Provides the interface for the prog loop subroutine
    !----------------------------------------------------------------------
    class(one2one_prog_t),    intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
  end subroutine cubecompute_one2one_prog_loop
  !
  subroutine cubecompute_one2one_prog_loop_r4tor4(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(one2one_prog_t),    intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(image_t) :: inima,ouima
    character(len=*), parameter :: rname='ONE2ONE>PROG>LOOP>R4TOR4'
    !
    call inima%associate('input cube',prog%incube,iter,error)
    if (error) return
    call ouima%allocate('output cube',prog%oucube,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
       call prog%act%r4tor4(iter%ie,inima,ouima,error)
    enddo ! ie
  end subroutine cubecompute_one2one_prog_loop_r4tor4
  !
  subroutine cubecompute_one2one_prog_loop_c4tor4(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(one2one_prog_t),    intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(visi_t) :: inima
    type(image_t) :: ouima
    character(len=*), parameter :: rname='ONE2ONE>PROG>LOOP>C4TOR4'
    !
    call inima%associate('input cube',prog%incube,iter,error)
    if (error) return
    call ouima%allocate('output cube',prog%oucube,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
       call prog%act%c4tor4(iter%ie,inima,ouima,error)
    enddo ! ie
  end subroutine cubecompute_one2one_prog_loop_c4tor4
  !
  subroutine cubecompute_one2one_prog_loop_c4toc4(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(one2one_prog_t),    intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(visi_t) :: inima,ouima
    character(len=*), parameter :: rname='ONE2ONE>PROG>LOOP>C4TOC4'
    !
    call inima%associate('input cube',prog%incube,iter,error)
    if (error) return
    call ouima%allocate('output cube',prog%oucube,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
       call prog%act%c4toc4(iter%ie,inima,ouima,error)
    enddo ! ie
  end subroutine cubecompute_one2one_prog_loop_c4toc4
end module cubecompute_one2one_template
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
