!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubecompute_product
  use cubecompute_two2one_template
  use cubecompute_messaging
  !
  public :: cubecompute_product_register
  private
  !
  type(two2one_comm_t) :: product
  !
contains
  !
  subroutine cubecompute_product_register(error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    character(len=*), parameter :: rname='PRODUCT>REGISTER'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call product%register_syntax(&
         'PRODUCT','factor1','factor2',&
         [flag_product],cubecompute_product_command,error)
    if (error) return
    !
    call product%unit%register(cubecompute_product_prog_unit,error)
    if (error) return
    !
    call product%act%register_r4r4tor4(cubecompute_product_prog_act_r4r4,error)
    if (error) return
    call product%act%register_r4c4toc4(cubecompute_product_prog_act_r4c4,error)
    if (error) return
    call product%act%register_c4r4toc4(cubecompute_product_prog_act_c4r4,error)
    if (error) return
    call product%act%register_c4c4toc4(cubecompute_product_prog_act_c4c4,error)
    if (error) return
  end subroutine cubecompute_product_register
  !
  subroutine cubecompute_product_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(two2one_user_t) :: user
    character(len=*), parameter :: rname='PRODUCT>COMMAND'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call product%parse(line,user,error)
    if (error) return
    call product%main(user,error)
    if (error) continue
  end subroutine cubecompute_product_command
  !
  subroutine cubecompute_product_prog_unit(unit1,unit2,ouunit,error)
    !----------------------------------------------------------------------
    ! Set the proper unit of the output cube
    !----------------------------------------------------------------------
    character(len=*), intent(inout) :: unit1
    character(len=*), intent(inout) :: unit2
    character(len=*), intent(inout) :: ouunit
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: data_unit_null='---'  ! To be factorized
    character(len=*), parameter :: rname='PRODUCT>UNIT'
    !
    if (unit1.eq.data_unit_null .and. unit2.eq.data_unit_null) then
       ouunit = data_unit_null
    elseif (unit1.eq.data_unit_null) then
       ouunit = unit2
    elseif (unit2.eq.data_unit_null) then
       ouunit = unit1
    else
       ouunit = trim(unit1)//'*'//trim(unit2)
    endif
  end subroutine cubecompute_product_prog_unit
  !
  !----------------------------------------------------------------------
  !
  subroutine cubecompute_product_prog_act_r4r4(ie,factor1,factor2,product,error)
    use cubetools_parameters
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(image_t),        intent(inout) :: factor1
    type(image_t),        intent(inout) :: factor2
    type(image_t),        intent(inout) :: product
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='PRODUCT>PROG>ACT>R4R4'
    !
    include 'action-product.inc'
  end subroutine cubecompute_product_prog_act_r4r4
  !
  subroutine cubecompute_product_prog_act_r4c4(ie,factor1,factor2,product,error)
    use cubetools_parameters
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(image_t),        intent(inout) :: factor1
    type(visi_t),         intent(inout) :: factor2
    type(visi_t),         intent(inout) :: product
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='PRODUCT>PROG>ACT>R4C4'
    !
    include 'action-product.inc'
  end subroutine cubecompute_product_prog_act_r4c4
  !
  subroutine cubecompute_product_prog_act_c4r4(ie,factor1,factor2,product,error)
    use cubetools_parameters
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(visi_t),         intent(inout) :: factor1
    type(image_t),        intent(inout) :: factor2
    type(visi_t),         intent(inout) :: product
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='PRODUCT>PROG>ACT>C4R4'
    !
    include 'action-product.inc'
  end subroutine cubecompute_product_prog_act_c4r4
  !
  subroutine cubecompute_product_prog_act_c4c4(ie,factor1,factor2,product,error)
    use cubetools_parameters
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(visi_t),         intent(inout) :: factor1
    type(visi_t),         intent(inout) :: factor2
    type(visi_t),         intent(inout) :: product
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='PRODUCT>PROG>ACT>C4C4'
    !
    include 'action-product.inc'
  end subroutine cubecompute_product_prog_act_c4c4
end module cubecompute_product
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
