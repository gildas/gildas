    !-------------------------------------------------------------------
    ! Action code for PRODUCT command
    !-------------------------------------------------------------------
    !
    integer(kind=pixe_k) :: ix,iy
    !
    call factor1%get(ie,error)
    if (error) return
    call factor2%get(ie,error)
    if (error) return
    !
    do iy=1,product%ny
       do ix=1,product%nx
          product%val(ix,iy) = factor1%val(ix,iy)*factor2%val(ix,iy)
       enddo ! ix
    enddo ! iy
    !
    call product%put(ie,error)
    if (error) return
