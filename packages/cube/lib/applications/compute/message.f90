!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage CUBECOMPUTE messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubecompute_messaging
  use gpack_def
  use gbl_message
  use cubetools_parameters
  !
  public :: computeseve,seve,mess_l
  public :: cubecompute_message_set_id,cubecompute_message
  public :: cubecompute_message_set_alloc,cubecompute_message_get_alloc
  public :: cubecompute_message_set_trace,cubecompute_message_get_trace
  public :: cubecompute_message_set_others,cubecompute_message_get_others
  private
  !
  ! Identifier used for message identification
  integer(kind=4) :: cubecompute_message_id = gpack_global_id  ! Default value for startup message
  !
  type :: cubecompute_messaging_debug_t
     integer(kind=code_k) :: alloc = seve%d
     integer(kind=code_k) :: trace = seve%t
     integer(kind=code_k) :: others = seve%d
  end type cubecompute_messaging_debug_t
  !
  type(cubecompute_messaging_debug_t) :: computeseve
  !
contains
  !
  subroutine cubecompute_message_set_id(id)
    !---------------------------------------------------------------------
    ! Alter library id into input id. Should be called by the library
    ! which wants to share its id with the current one.
    !---------------------------------------------------------------------
    integer(kind=4), intent(in) :: id
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='MESSAGE>SET>ID'
    !
    cubecompute_message_id = id
    write (mess,'(A,I0)') 'Now use id #',cubecompute_message_id
    call cubecompute_message(seve%d,rname,mess)
  end subroutine cubecompute_message_set_id
  !
  subroutine cubecompute_message(mkind,procname,message)
    use cubetools_cmessaging
    !---------------------------------------------------------------------
    ! Messaging facility for the current library. Calls the low-level
    ! (internal) messaging routine with its own identifier.
    !---------------------------------------------------------------------
    integer(kind=4),  intent(in) :: mkind     ! Message kind
    character(len=*), intent(in) :: procname  ! Name of calling procedure
    character(len=*), intent(in) :: message   ! Message string
    !
    call cubetools_cmessage(cubecompute_message_id,mkind,'IMA>'//procname,message)
  end subroutine cubecompute_message
  !
  subroutine cubecompute_message_set_alloc(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       computeseve%alloc = seve%i
    else
       computeseve%alloc = seve%d
    endif
  end subroutine cubecompute_message_set_alloc
  !
  subroutine cubecompute_message_set_trace(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       computeseve%trace = seve%i
    else
       computeseve%trace = seve%t
    endif
  end subroutine cubecompute_message_set_trace
  !
  subroutine cubecompute_message_set_others(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       computeseve%others = seve%i
    else
       computeseve%others = seve%d
    endif
  end subroutine cubecompute_message_set_others
  !
  function cubecompute_message_get_alloc()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubecompute_message_get_alloc
    !
    cubecompute_message_get_alloc = computeseve%alloc.eq.seve%i
    !
  end function cubecompute_message_get_alloc
  !
  function cubecompute_message_get_trace()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubecompute_message_get_trace
    !
    cubecompute_message_get_trace = computeseve%trace.eq.seve%i
    !
  end function cubecompute_message_get_trace
  !
  function cubecompute_message_get_others()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubecompute_message_get_others
    !
    cubecompute_message_get_others = computeseve%others.eq.seve%i
    !
  end function cubecompute_message_get_others
end module cubecompute_messaging
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
