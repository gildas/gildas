!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubecompute_fft
  use cube_types
  use cubetools_parameters
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubetopology_cuberegion_types
  use cubecompute_messaging
  use cubecompute_fft_visi_tool
  !
  public :: fft
  private
  !
  type :: fft_comm_t
     type(option_t),     pointer :: comm
     type(fft_visi_opt_t)        :: direction
     type(cuberegion_comm_t)     :: region
     type(cubeid_arg_t), pointer :: incube
     type(cube_prod_t),  pointer :: oucube  ! Approx cube prod registered at start-up
   contains
     procedure, public  :: register => cubecompute_fft_register
     procedure, private :: parse    => cubecompute_fft_parse
     procedure, private :: ima      => cubecompute_fft_ima
  end type fft_comm_t
  type(fft_comm_t) :: fft
  !
  type fft_user_t
     type(fft_visi_user_t)   :: direction
     type(cuberegion_user_t) :: region
     type(cubeid_user_t)     :: cubeids
   contains
     procedure, private :: toprog => cubecompute_fft_user_toprog
  end type fft_user_t
  !
  type fft_prog_t
     integer(kind=code_k)    :: fftdirection
     type(cube_prod_t)       :: ouprod  ! Exact cube prod evaluated at run time
     type(cuberegion_prog_t) :: region
     type(cube_t), pointer   :: incube
     type(cube_t), pointer   :: oucube
     !
     procedure(cubecompute_fft_prog_image2visi_loop), private, pointer :: loop => null()
     procedure(cubecompute_fft_prog_image2visi_act),  private, pointer :: act  => null()
   contains
     procedure, private :: header => cubecompute_fft_prog_header
     procedure, private :: data   => cubecompute_fft_prog_data
  end type fft_prog_t
  !
contains
  !
  subroutine cubecompute_fft_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(fft_user_t) :: user
    character(len=*), parameter :: rname='FFT>COMMAND'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    call fft%parse(line,user,error)
    if (error) return
    call fft%ima(user,error)
    if (error) continue
  end subroutine cubecompute_fft_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubecompute_fft_register(comm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(fft_comm_t), intent(inout) :: comm
    logical,           intent(inout) :: error
    !
    type(cubeid_arg_t) :: incube
    type(cube_prod_t) :: oucube
    character(len=*), parameter :: rname='FFT>REGISTER'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    ! Syntax
    call cubetools_register_command(&
         'FFT','[cubeid]',&
         'Fourier transform a real or complex cube',&
         'The output will be either real or complex.',&
         cubecompute_fft_command,&
         comm%comm,error)
    if (error) return
    call incube%register(&
         'CUBE',&
         'Input data',&
         strg_id,&
         code_arg_optional,&
         [flag_any],&
         code_read,&
         code_access_imaset,&
         comm%incube,&
         error)
    if (error) return
    call comm%direction%register(error)
    if (error) return
    call comm%region%register(error)
    if (error) return
    !
    ! Products
    call oucube%register(  &
         'FFT',  &
         'FFT cube',  &
         strg_id,  &
         [flag_any,flag_fft],  &  ! flag_any resolved at run time
         fft%oucube,  &
         error)
    if (error)  return
  end subroutine cubecompute_fft_register
  !
  subroutine cubecompute_fft_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(fft_comm_t), intent(in)    :: comm
    character(len=*),  intent(in)    :: line
    type(fft_user_t),  intent(out)   :: user
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='FFT>PARSE'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%region%parse(line,user%region,error)
    if (error) return
    call comm%direction%parse(line,user%direction,error)
    if (error) return
  end subroutine cubecompute_fft_parse
  !
  subroutine cubecompute_fft_ima(comm,user,error)
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(fft_comm_t), intent(in)    :: comm
    type(fft_user_t),  intent(in)    :: user
    logical,           intent(inout) :: error
    !
    type(fft_prog_t) :: prog
    character(len=*), parameter :: rname='FFT>IMA'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubecompute_fft_ima
  !
  !----------------------------------------------------------------------
  !
  subroutine cubecompute_fft_user_toprog(user,comm,prog,error)
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(fft_user_t), intent(in)    :: user
    type(fft_comm_t),  intent(in)    :: comm
    type(fft_prog_t),  intent(out)   :: prog
    logical,           intent(inout) :: error
    !
    character(len=*), parameter :: rname='FFT>USER>TOPROG'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%incube,user%cubeids,prog%incube,error)
    if (error) return
    call user%direction%toprog(prog%incube,prog%fftdirection,error)
    if (error) return
    call user%region%toprog(prog%incube,prog%region,error)
    if (error) return
    ! User feedback about the interpretation of his command line
    call prog%region%list(error)
    if (error) return    
  end subroutine cubecompute_fft_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubecompute_fft_prog_header(prog,comm,error)
    use cubedag_allflags
    use cubeadm_clone
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(fft_prog_t), intent(inout) :: prog
    type(fft_comm_t),  intent(in)    :: comm
    logical,           intent(inout) :: error
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='FFT>PROG>HEADER'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    ! Sanity
    if (mod(prog%incube%head%spa%l%n,2).ne.0 .or.  &
        mod(prog%incube%head%spa%m%n,2).ne.0) then
      write(mess,'(a,i0,a,i0,a)')  &
        'Spatial axes must be even (shape is ',  &
        prog%incube%head%spa%l%n,'x',prog%incube%head%spa%m%n,')'
      call cubecompute_message(seve%e,rname,mess)
      error = .true.
      return
    endif
    !
    ! Create output cube with proper flags
    call comm%oucube%copy(prog%ouprod,error)
    if (error)  return
    if (prog%fftdirection.eq.code_fft_direct) then
      call prog%ouprod%flag_to_flag(flag_any,flag_direct,error)
      if (error)  return
    else
      call prog%ouprod%flag_to_flag(flag_any,flag_inverse,error)
      if (error)  return
    endif
    call cubeadm_clone_header(prog%ouprod,prog%incube,prog%oucube,error)
    if (error) return
    !
    call prog%region%header(prog%oucube,error)
    if (error) return
    ! *** JP I only code two cases making the assumption that visi is hermitian.
    ! *** JP To be generic, we should also considers the cases where visi is not!
    if (prog%incube%iscplx()) then
       prog%loop => cubecompute_fft_prog_visi2image_loop
       prog%act  => cubecompute_fft_prog_visi2image_act
       call cubecompute_fft_visi_prog_header_visi2image(prog%oucube%head,prog%oucube%head,error)
       if (error) return
    else
       prog%loop => cubecompute_fft_prog_image2visi_loop
       prog%act  => cubecompute_fft_prog_image2visi_act
       call cubecompute_fft_visi_prog_header_image2visi(prog%oucube%head,prog%oucube%head,error)
       if (error) return
    endif
  end subroutine cubecompute_fft_prog_header
  !
  subroutine cubecompute_fft_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(fft_prog_t), intent(inout) :: prog
    logical,           intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='FFT>PROG>DATA'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,error)
    if (error) return
    !
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) call prog%loop(iter,error)
       !$OMP END TASK
    enddo ! ie
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubecompute_fft_prog_data
  !
  !----------------------------------------------------------------------
  !
  subroutine cubecompute_fft_prog_image2visi_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(fft_prog_t),        intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(visi_t) :: visi
    type(image_t) :: image
    type(fft_visi_prog_t) :: fft
    character(len=*), parameter :: rname='FFT>PROG>IMAGE2VISI>LOOP'
    !
    call image%associate('image',prog%incube,iter,error)
    if (error) return
    call visi%allocate('visi',prog%oucube,iter,error)
    if (error) return
    call fft%init(prog%fftdirection,image,visi,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter%ie,fft,error)
      if (error) return
    enddo
  end subroutine cubecompute_fft_prog_image2visi_loop
  !
  subroutine cubecompute_fft_prog_image2visi_act(prog,ie,fft,error)
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(fft_prog_t),     intent(inout) :: prog
    integer(kind=entr_k),  intent(in)    :: ie
    type(fft_visi_prog_t), intent(inout) :: fft
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='FFT>PROG>IMAGE2VISI>ACT'
    !
    call fft%image%get(ie,error)
    if (error) return
    call fft%image2visi(error)
    if (error) return
    call fft%compute(error)
    if (error) return
    call fft%normalize(error)
    if (error) return
    call fft%visi%put(ie,error)
    if (error) return
  end subroutine cubecompute_fft_prog_image2visi_act
  !
  !----------------------------------------------------------------------
  !
  subroutine cubecompute_fft_prog_visi2image_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(fft_prog_t),        intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(visi_t) :: visi
    type(image_t) :: image
    type(fft_visi_prog_t) :: fft
    character(len=*), parameter :: rname='FFT>PROG>VISI2IMAGE>LOOP'
    !
    call visi%associate('visi',prog%incube,iter,error)
    if (error) return
    call image%allocate('image',prog%oucube,iter,error)
    if (error) return
    call fft%init(prog%fftdirection,image,visi,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter%ie,fft,error)
      if (error) return
    enddo
  end subroutine cubecompute_fft_prog_visi2image_loop
  !
  subroutine cubecompute_fft_prog_visi2image_act(prog,ie,fft,error)
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(fft_prog_t),     intent(inout) :: prog
    integer(kind=entr_k),  intent(in)    :: ie
    type(fft_visi_prog_t), intent(inout) :: fft
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='FFT>PROG>VISI2IMAGE>ACT'
    !
    call fft%visi%get(ie,error)
    if (error) return
    call fft%compute(error)
    if (error) return
    call fft%normalize(error)
    if (error) return
    call fft%visi2image(error)
    if (error) return
    call fft%image%put(ie,error)
    if (error) return
  end subroutine cubecompute_fft_prog_visi2image_act
end module cubecompute_fft
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
