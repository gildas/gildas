    !-------------------------------------------------------------------
    ! Action code for DIFFERENCE command
    !-------------------------------------------------------------------
    !
    integer(kind=pixe_k) :: ix,iy
    !
    call subtrahend%get(ie,error)
    if (error) return
    call minuend%get(ie,error)
    if (error) return
    !
    do iy=1,difference%ny
       do ix=1,difference%nx
          difference%val(ix,iy) = subtrahend%val(ix,iy)-minuend%val(ix,iy)
       enddo ! ix
    enddo ! iy
    !
    call difference%put(ie,error)
    if (error) return
