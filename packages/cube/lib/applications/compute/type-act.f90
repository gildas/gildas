!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubecompute_one2one_act
  use cubetools_parameters
  use cubecompute_messaging

  public :: one2one_act_comm_t,one2one_act_prog_t
  private
  !
  type one2one_act_comm_t
    procedure(cubecompute_one2one_act_r4tor4), pointer, nopass, public :: r4tor4 => null()
    procedure(cubecompute_one2one_act_c4tor4), pointer, nopass, public :: c4tor4 => null()
    procedure(cubecompute_one2one_act_c4toc4), pointer, nopass, public :: c4toc4 => null()
  contains
    procedure, public :: register_r4tor4 => cubecompute_one2one_act_register_r4tor4
    procedure, public :: register_c4tor4 => cubecompute_one2one_act_register_c4tor4
    procedure, public :: register_c4toc4 => cubecompute_one2one_act_register_c4toc4
  end type one2one_act_comm_t
  !
  type one2one_act_prog_t
    procedure(cubecompute_one2one_act_r4tor4), pointer, nopass, public :: r4tor4 => null()
    procedure(cubecompute_one2one_act_c4tor4), pointer, nopass, public :: c4tor4 => null()
    procedure(cubecompute_one2one_act_c4toc4), pointer, nopass, public :: c4toc4 => null()
  end type one2one_act_prog_t
  !
contains
  !
  subroutine cubecompute_one2one_act_r4tor4(ie,incube,oucube,error)
    use cubeadm_image_types
    !----------------------------------------------------------------------
    ! Provides the interface for the operation
    !  R*4 = R*4
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(image_t),        intent(inout) :: incube
    type(image_t),        intent(inout) :: oucube
    logical,              intent(inout) :: error
  end subroutine cubecompute_one2one_act_r4tor4
  !
  subroutine cubecompute_one2one_act_c4tor4(ie,incube,oucube,error)
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    ! Provides the interface for the operation
    !  C*4 = R*4
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(visi_t),         intent(inout) :: incube
    type(image_t),        intent(inout) :: oucube
    logical,              intent(inout) :: error
  end subroutine cubecompute_one2one_act_c4tor4
  !
  subroutine cubecompute_one2one_act_c4toc4(ie,incube,oucube,error)
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    ! Provides the interface for the operation
    !  C*4 = C*4
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(visi_t),         intent(inout) :: incube
    type(visi_t),         intent(inout) :: oucube
    logical,              intent(inout) :: error
  end subroutine cubecompute_one2one_act_c4toc4
  !
  subroutine cubecompute_one2one_act_register_r4tor4(comm,act,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(one2one_act_comm_t),                intent(inout) :: comm
    procedure(cubecompute_one2one_act_r4tor4)               :: act
    logical,                                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='ONE2ONE>ACT>REGISTER>R4TOR4'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    comm%r4tor4 => act
  end subroutine cubecompute_one2one_act_register_r4tor4
  !
  subroutine cubecompute_one2one_act_register_c4tor4(comm,act,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(one2one_act_comm_t),                intent(inout) :: comm
    procedure(cubecompute_one2one_act_c4tor4)               :: act
    logical,                                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='ONE2ONE>ACT>REGISTER>C4TOR4'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    comm%c4tor4 => act
  end subroutine cubecompute_one2one_act_register_c4tor4
  !
  subroutine cubecompute_one2one_act_register_c4toc4(comm,act,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(one2one_act_comm_t),                intent(inout) :: comm
    procedure(cubecompute_one2one_act_c4toc4)               :: act
    logical,                                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='ONE2ONE>ACT>REGISTER>C4TOC4'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    comm%c4toc4 => act
  end subroutine cubecompute_one2one_act_register_c4toc4

end module cubecompute_one2one_act
!
module cubecompute_two2one_act
  use cubetools_parameters
  use cubecompute_messaging

  public :: two2one_act_comm_t,two2one_act_prog_t
  private
  !
  type two2one_act_comm_t
    procedure(cubecompute_two2one_act_r4r4tor4), pointer, nopass, public :: r4r4tor4 => null()
    procedure(cubecompute_two2one_act_r4r4toc4), pointer, nopass, public :: r4r4toc4 => null()
    procedure(cubecompute_two2one_act_r4c4toc4), pointer, nopass, public :: r4c4toc4 => null()
    procedure(cubecompute_two2one_act_c4r4toc4), pointer, nopass, public :: c4r4toc4 => null()
    procedure(cubecompute_two2one_act_c4c4toc4), pointer, nopass, public :: c4c4toc4 => null()
  contains
    procedure, public :: register_r4r4tor4 => cubecompute_two2one_act_register_r4r4tor4
    procedure, public :: register_r4r4toc4 => cubecompute_two2one_act_register_r4r4toc4
    procedure, public :: register_r4c4toc4 => cubecompute_two2one_act_register_r4c4toc4
    procedure, public :: register_c4r4toc4 => cubecompute_two2one_act_register_c4r4toc4
    procedure, public :: register_c4c4toc4 => cubecompute_two2one_act_register_c4c4toc4
  end type two2one_act_comm_t
  !
  type two2one_act_prog_t
    procedure(cubecompute_two2one_act_r4r4tor4), pointer, nopass, public :: r4r4tor4 => null()
    procedure(cubecompute_two2one_act_r4r4toc4), pointer, nopass, public :: r4r4toc4 => null()
    procedure(cubecompute_two2one_act_r4c4toc4), pointer, nopass, public :: r4c4toc4 => null()
    procedure(cubecompute_two2one_act_c4r4toc4), pointer, nopass, public :: c4r4toc4 => null()
    procedure(cubecompute_two2one_act_c4c4toc4), pointer, nopass, public :: c4c4toc4 => null()
  end type two2one_act_prog_t
  !
contains
  !
  subroutine cubecompute_two2one_act_r4r4tor4(ie,incube1,incube2,oucube,error)
    use cubeadm_image_types
    !----------------------------------------------------------------------
    ! Provides the interface for the operation
    !  R*4 + R*4 = R*4
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(image_t),        intent(inout) :: incube1
    type(image_t),        intent(inout) :: incube2
    type(image_t),        intent(inout) :: oucube
    logical,              intent(inout) :: error
  end subroutine cubecompute_two2one_act_r4r4tor4
  !
  subroutine cubecompute_two2one_act_r4r4toc4(ie,incube1,incube2,oucube,error)
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    ! Provides the interface for the operation
    !  R*4 + R*4 = C*4
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(image_t),        intent(inout) :: incube1
    type(image_t),        intent(inout) :: incube2
    type(visi_t),         intent(inout) :: oucube
    logical,              intent(inout) :: error
  end subroutine cubecompute_two2one_act_r4r4toc4
  !
  subroutine cubecompute_two2one_act_r4c4toc4(ie,incube1,incube2,oucube,error)
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    ! Provides the interface for the operation
    !  R*4 + C*4 = C*4
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(image_t),        intent(inout) :: incube1
    type(visi_t),         intent(inout) :: incube2
    type(visi_t),         intent(inout) :: oucube
    logical,              intent(inout) :: error
  end subroutine cubecompute_two2one_act_r4c4toc4
  !
  subroutine cubecompute_two2one_act_c4r4toc4(ie,incube1,incube2,oucube,error)
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    ! Provides the interface for the operation
    !  C*4 + R*4 = C*4
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(visi_t),         intent(inout) :: incube1
    type(image_t),        intent(inout) :: incube2
    type(visi_t),         intent(inout) :: oucube
    logical,              intent(inout) :: error
  end subroutine cubecompute_two2one_act_c4r4toc4
  !
  subroutine cubecompute_two2one_act_c4c4toc4(ie,incube1,incube2,oucube,error)
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    ! Provides the interface for the operation
    !  C*4 + C*4 = C*4
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(visi_t),         intent(inout) :: incube1
    type(visi_t),         intent(inout) :: incube2
    type(visi_t),         intent(inout) :: oucube
    logical,              intent(inout) :: error
  end subroutine cubecompute_two2one_act_c4c4toc4
  !
  subroutine cubecompute_two2one_act_register_r4r4tor4(comm,act,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(two2one_act_comm_t),                  intent(inout) :: comm
    procedure(cubecompute_two2one_act_r4r4tor4)               :: act
    logical,                                    intent(inout) :: error
    !
    character(len=*), parameter :: rname='TWO2ONE>ACT>REGISTER>R4R4TOR4'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    comm%r4r4tor4 => act
  end subroutine cubecompute_two2one_act_register_r4r4tor4
  !
  subroutine cubecompute_two2one_act_register_r4r4toc4(comm,act,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(two2one_act_comm_t),                  intent(inout) :: comm
    procedure(cubecompute_two2one_act_r4r4toc4)               :: act
    logical,                                    intent(inout) :: error
    !
    character(len=*), parameter :: rname='TWO2ONE>ACT>REGISTER>R4R4TOC4'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    comm%r4r4toc4 => act
  end subroutine cubecompute_two2one_act_register_r4r4toc4
  !
  subroutine cubecompute_two2one_act_register_r4c4toc4(comm,act,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(two2one_act_comm_t),                  intent(inout) :: comm
    procedure(cubecompute_two2one_act_r4c4toc4)               :: act
    logical,                                    intent(inout) :: error
    !
    character(len=*), parameter :: rname='TWO2ONE>ACT>REGISTER>R4C4TOC4'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    comm%r4c4toc4 => act
  end subroutine cubecompute_two2one_act_register_r4c4toc4
  !
  subroutine cubecompute_two2one_act_register_c4r4toc4(comm,act,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(two2one_act_comm_t),                  intent(inout) :: comm
    procedure(cubecompute_two2one_act_c4r4toc4)               :: act
    logical,                                    intent(inout) :: error
    !
    character(len=*), parameter :: rname='TWO2ONE>ACT>REGISTER>C4R4TOC4'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    comm%c4r4toc4 => act
  end subroutine cubecompute_two2one_act_register_c4r4toc4
  !
  subroutine cubecompute_two2one_act_register_c4c4toc4(comm,act,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(two2one_act_comm_t),                  intent(inout) :: comm
    procedure(cubecompute_two2one_act_c4c4toc4)               :: act
    logical,                                    intent(inout) :: error
    !
    character(len=*), parameter :: rname='TWO2ONE>ACT>REGISTER>C4C4TOC4'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    comm%c4c4toc4 => act
  end subroutine cubecompute_two2one_act_register_c4c4toc4

end module cubecompute_two2one_act
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
