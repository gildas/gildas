!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubecompute_complex
  use cubecompute_two2one_template
  use cubecompute_messaging
  !
  public :: cubecompute_complex_register
  private
  !
  type(two2one_comm_t) :: complex
  !
contains
  !
  subroutine cubecompute_complex_register(error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    character(len=*), parameter :: rname='COMPLEX>REGISTER'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call complex%register_syntax(&
         'COMPLEX','real','imaginary',&
         [flag_complex],cubecompute_complex_command,error)
    if (error) return
    call complex%act%register_r4r4toc4(cubecompute_complex_prog_act,error)
    if (error) return
  end subroutine cubecompute_complex_register
  !
  subroutine cubecompute_complex_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(two2one_user_t) :: user
    character(len=*), parameter :: rname='COMPLEX>COMMAND'
    !
    call cubecompute_message(computeseve%trace,rname,'Welcome')
    !
    call complex%parse(line,user,error)
    if (error) return
    call complex%main(user,error)
    if (error) continue
  end subroutine cubecompute_complex_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubecompute_complex_prog_act(ie,real,imaginary,complex,error)
    use cubetools_parameters
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(image_t),        intent(inout) :: real
    type(image_t),        intent(inout) :: imaginary
    type(visi_t ),        intent(inout) :: complex
    logical,              intent(inout) :: error
    !
    integer(kind=pixe_k) :: ix,iy
    character(len=*), parameter :: rname='COMPLEX>PROG>ACT'
    !
    call real%get(ie,error)
    if (error) return
    call imaginary%get(ie,error)
    if (error) return
    do iy=1,complex%ny
       do ix=1,complex%nx
          complex%val(ix,iy) = cmplx(real%val(ix,iy),imaginary%val(ix,iy))
       enddo ! ix
    enddo ! iy
    call complex%put(ie,error)
    if (error) return
  end subroutine cubecompute_complex_prog_act
end module cubecompute_complex
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
