!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemask_invert
  use cubecompute_one2one_template
  use cubemask_messaging
  !
  public :: cubemask_invert_register
  private
  !
  type(one2one_comm_t) :: invert
  !
contains
  !
  subroutine cubemask_invert_register(error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    character(len=*), parameter :: rname='INVERT>REGISTER'
    !
    call cubemask_message(maskseve%trace,rname,'Welcome')
    !
    call invert%register_syntax(&
         'INVERT','input mask',&
         [flag_inverted,flag_mask],cubemask_invert_command,error)
    if (error) return
    call invert%act%register_r4tor4(cubemask_invert_prog_act,error)
    if (error) return
  end subroutine cubemask_invert_register
  !
  subroutine cubemask_invert_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(one2one_user_t) :: user
    character(len=*), parameter :: rname='INVERT>COMMAND'
    !
    call cubemask_message(maskseve%trace,rname,'Welcome')
    !
    call invert%parse(line,user,error)
    if (error) return
    call invert%main(user,error)
    if (error) return
  end subroutine cubemask_invert_command
  !
  subroutine cubemask_invert_prog_act(ie,in,ou,error)
    use cubetools_parameters
    use cubetools_nan
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !***JP: Shouldn't we use subcubes for one2one?
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(image_t),        intent(inout) :: in
    type(image_t),        intent(inout) :: ou
    logical,              intent(inout) :: error
    !
    integer(kind=pixe_k) :: ix,iy
    character(len=*), parameter :: rname='INVERT>PROG>ACT'
    !
    call in%get(ie,error)
    if (error) return
    do iy=1,in%ny
       do ix=1,in%nx
          if (isnan(in%val(ix,iy))) then
             ou%val(ix,iy) = 1
          else
             ou%val(ix,iy) = gr4nan
          endif
       enddo ! ix
    enddo ! iy
    call ou%put(ie,error)
    if (error) return
  end subroutine cubemask_invert_prog_act
end module cubemask_invert
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
