!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemask_velocity
  use cubetools_parameters
  use cube_types
  use cubetools_structure
  use cubetools_array_types
  use cubesyntax_keyval_strg_types
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubemain_ancillary_mask_types
  use cubetopology_cuberegion_types
  use cubemask_messaging
  !
  public :: velocity
  private
  !
  type :: velocity_comm_t
     type(option_t), pointer :: comm
     type(cuberegion_comm_t) :: region
     type(keyval_strg_comm_t) :: freqvar
     type(cubeid_arg_t), pointer :: cube
     type(ancillary_mask_comm_t) :: mask
     type(cube_prod_t),  pointer :: masked     
   contains
     procedure, public  :: register => cubemask_velocity_comm_register
     procedure, private :: parse    => cubemask_velocity_comm_parse
     procedure, private :: main     => cubemask_velocity_comm_main
  end type velocity_comm_t
  type(velocity_comm_t) :: velocity  
  !
  type velocity_user_t
     type(cuberegion_user_t)     :: region
     type(keyval_strg_user_t)    :: freqvar 
     type(cubeid_user_t)         :: cubeids
     type(ancillary_mask_user_t) :: mask
   contains
     procedure, private :: toprog => cubemask_velocity_user_toprog
  end type velocity_user_t
  !
  type velocity_prog_t
     type(cuberegion_prog_t)     :: region
     type(dble_1d_t)             :: frequency
     type(long_2d_t)             :: ocmin
     type(long_2d_t)             :: ocmax
     type(cube_t), pointer       :: cube
     type(ancillary_mask_prog_t) :: mask
     type(cube_t), pointer       :: masked
   contains
     procedure, private :: header => cubemask_velocity_prog_header
     procedure, private :: data   => cubemask_velocity_prog_data
     procedure, private :: loop   => cubemask_velocity_prog_loop
     procedure, private :: act    => cubemask_velocity_prog_act
  end type velocity_prog_t
  !
contains
  !
  subroutine cubemask_velocity_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(velocity_user_t) :: user
    character(len=*), parameter :: rname='VELOCITY>COMMAND'
    !
    call cubemask_message(maskseve%trace,rname,'Welcome')
    !
    call velocity%parse(line,user,error)
    if (error) return
    call velocity%main(user,error)
    if (error) continue
  end subroutine cubemask_velocity_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemask_velocity_comm_register(comm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(velocity_comm_t), intent(inout) :: comm
    logical,                intent(inout) :: error
    !
    type(cubeid_arg_t) :: incube
    type(cube_prod_t) :: masked
    character(len=*), parameter :: rname='VELOCITY>COMM>REGISTER'
    !
    call cubemask_message(maskseve%trace,rname,'Welcome')
    !
    ! Syntax
    call cubetools_register_command(&
         'VELOCITY','[cubeid]',&
         'Mask a spectral cube around each frequency following a mask velocity pattern',&
         'Input and output cubes must be real',&
         cubemask_velocity_command,&
         comm%comm,&
         error)
    if (error) return
    call incube%register(&
         'INPUT',&
         'Input cube',&
         strg_id,&
         code_arg_optional,&
         [flag_any],&
         code_read,&
         code_access_speset,&
         comm%cube,&
         error)
    if (error) return
    call comm%mask%register(&
         'Mask velocity pattern',&
         code_access_speset,&
         error)
    if (error) return
    call comm%freqvar%register(&
         'FREQVAR',&
         'Variable containing a list of frequencies. Default to lines%frequency defined by LINEDB\FIND',&
         'lines%frequency',&
         error)
    if (error) return
    call comm%region%register(error)
    if (error) return
    !
    ! Products
    call masked%register(&
         'MASKED',&
         'Masked cube',&
         strg_id,&
         [flag_masked],&
         comm%masked,&
         error)
    if (error)  return
  end subroutine cubemask_velocity_comm_register
  !
  subroutine cubemask_velocity_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! VELOCITY [cubeid] /AS [maskid] /FREQVAR [varname]
    !----------------------------------------------------------------------
    class(velocity_comm_t), intent(in)    :: comm
    character(len=*),       intent(in)    :: line
    type(velocity_user_t),  intent(out)   :: user
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='VELOCITY>COMM>PARSE'
    !
    call cubemask_message(maskseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%mask%parse(line,user%mask,error)
    if (error) return
    call comm%freqvar%parse(line,user%freqvar,error)
    if (error) return
    call comm%region%parse(line,user%region,error)
    if (error) return
  end subroutine cubemask_velocity_comm_parse
  !
  subroutine cubemask_velocity_comm_main(comm,user,error)
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(velocity_comm_t), intent(in)    :: comm
    type(velocity_user_t),  intent(inout) :: user
    logical,                intent(inout) :: error
    !
    type(velocity_prog_t) :: prog
    character(len=*), parameter :: rname='VELOCITY>MAIN'
    !
    call cubemask_message(maskseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubemask_velocity_comm_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemask_velocity_user_toprog(user,comm,prog,error)
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(velocity_user_t), intent(in)    :: user
    type(velocity_comm_t),  intent(in)    :: comm
    type(velocity_prog_t),  intent(out)   :: prog
    logical,                intent(inout) :: error
    !
    character(len=varn_l) :: freqvar
    character(len=*), parameter :: rname='VELOCITY>USER>TOPROG'
    !
    call cubemask_message(maskseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%cube,user%cubeids,prog%cube,error)
    if (error) return
    call user%mask%toprog(comm%mask,prog%mask,error)
    if (error) return
    !
    call user%freqvar%toprog(comm%freqvar,freqvar,error)
    if (error) return
    call get_dble_1d_from_varname(prog%frequency,freqvar,error)
    if (error) return
    !
    call prog%mask%check_consistency(prog%cube,error)
    if (error) return
    call user%region%toprog(prog%cube,prog%region,error)
    if (error) return
    ! User feedback about the interpretation of his command line
    call prog%region%list(error)
    if (error) return
    !
  contains
    !
    subroutine get_dble_1d_from_varname(array,varname,error)
      use gbl_constant
      use gkernel_types
      use gkernel_interfaces
      !----------------------------------------------------------------------
      !
      !----------------------------------------------------------------------
      type(dble_1d_t),  intent(inout) :: array
      character(len=*), intent(in)    :: varname
      logical,          intent(inout) :: error
      !
      logical :: found
      type(sic_descriptor_t) :: desc
      integer(kind=size_length) :: nelt
      !
      found = .true.
      call sic_descriptor(freqvar,desc,found)
      if (.not.found) then
         call cubemask_message(seve%e,rname,'SIC VARIABLE not found')
         error = .true.
         return
      endif
      if (desc%ndim.gt.1) then
         call cubemask_message(seve%e,rname,'SIC VARIABLE must be 0D or 1D')
         error = .true.
         return
      endif
      if ((desc%type.ne.fmt_r4).and.(desc%type.ne.fmt_r8)) then
         call cubemask_message(seve%e,rname,'SIC VARIABLE must be REAL or DOUBLE')
         error = .true.
         return
      endif
      !
      nelt = desc_nelem(desc)
      call array%reallocate(varname,nelt,error)
      if (error) return
      call sic_variable_get(rname,varname,array%val,nelt,error)
      if (error) return
    end subroutine get_dble_1d_from_varname
  end subroutine cubemask_velocity_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemask_velocity_prog_header(prog,comm,error)
    use cubeadm_clone
    use cubetools_axis_types
    use cubetools_header_methods
    !----------------------------------------------------------------------
    ! We could compress more when the frequencies are outside the range of
    ! the cube axis. This should be done after the prepare_masking because
    ! if a line is completely outside the input cube spectrum range, ocmin
    ! and ocmax will trigger empty do loops.
    !----------------------------------------------------------------------
    class(velocity_prog_t), intent(inout) :: prog
    type(velocity_comm_t),  intent(in)    :: comm
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='VELOCITY>PROG>HEADER'
    !
    call cubemask_message(maskseve%trace,rname,'Welcome')
    !
    call cubeadm_clone_header_with_region(comm%masked,  &
      prog%cube,prog%region,prog%masked,error)
    if (error) return
    !
    call compress(prog%frequency)
    call prepare_masking(&
         prog%cube,prog%mask%cube,prog%frequency,&
         prog%ocmin,prog%ocmax,&
         error)
    if (error) return
    !
  contains
    !
    subroutine compress(array)
      !----------------------------------------------------------------------
      !----------------------------------------------------------------------
      type(dble_1d_t), intent(inout) :: array
      !
      integer(kind=indx_k) :: i,o
      !
      if (array%hasblank()) then
         call cubemask_message(seve%e,rname,'Frequency array has NaN values')
         error = .true.
         return
      endif
      if (.not.array%issorted()) then
         !***JP: An error for the moment but the array could be sorted by the program!
         call cubemask_message(seve%e,rname,'Frequency array must be sorted')
         error = .true.
         return
      endif
      !
      o = 1
      do i=2,array%n
         if (array%val(i).ne.array%val(o)) then
            o = o+1
            if (o.ne.i) array%val(o) = array%val(i)
         endif
      enddo ! i
      array%n = o
    end subroutine compress
    !
    subroutine prepare_masking(cube,mask,frequency,ocmin,ocmax,error)
      use cubetools_header_types
      !----------------------------------------------------------------------
      !***JP: We could merge the ocmin and ocmax that overlaps for some
      !lines to speed things up.
      !----------------------------------------------------------------------
      type(cube_t),    intent(in)    :: cube
      type(cube_t),    intent(in)    :: mask
      type(dble_1d_t), intent(in)    :: frequency
      type(long_2d_t), intent(out)   :: ocmin
      type(long_2d_t), intent(out)   :: ocmax
      logical,         intent(inout) :: error
      !
      integer(kind=indx_k) :: if,nf
      integer(kind=chan_k) :: ic,nc,ocleft,ocright
      real(kind=coor_k) :: velo
      type(axis_t) :: maskvaxis,cubevaxis
      type(cube_header_t) :: head
      !
      call cubetools_header_get_axis_head_v(mask%head,maskvaxis,error)
      if (error) return
      !
      nf = prog%frequency%n
      call cubetools_header_get_nchan(mask%head,nc,error)
      if (error) return
      call prog%ocmin%reallocate('ocmin',nc,nf,error)
      if (error) return
      call prog%ocmax%reallocate('ocmax',nc,nf,error)
      if (error) return
      call cubetools_header_copy(cube%head,head,error)
      if (error) return
      do if = 1,nf
         call cubetools_header_modify_rest_frequency(frequency%val(if),head,error)
         if (error) return
         call cubetools_header_get_axis_head_v(head,cubevaxis,error)
         if (error) return
         do ic = 1,nc
            call maskvaxis%pixel2offset(ic-5d-1,velo,error)
            if (error) return
            call cubevaxis%offset2pixel(velo,ocleft,error)
            if (error) return
            call maskvaxis%pixel2offset(ic+5d-1,velo,error)
            if (error) return
            call cubevaxis%offset2pixel(velo,ocright,error)
            if (error) return
            ocmin%val(ic,if) = min(ocleft,ocright)
            ocmax%val(ic,if) = max(ocleft,ocright)
            ocmin%val(ic,if) = max(ocmin%val(ic,if),1)
            ocmax%val(ic,if) = min(ocmax%val(ic,if),cubevaxis%n)
         enddo ! ic
      enddo ! if
    end subroutine prepare_masking
  end subroutine cubemask_velocity_prog_header
  !
  subroutine cubemask_velocity_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(velocity_prog_t), intent(inout) :: prog
    logical,                intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='VELOCITY>PROG>DATA'
    !
    call cubemask_message(maskseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,prog%region,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubemask_velocity_prog_data
  !   
  subroutine cubemask_velocity_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(velocity_prog_t),   intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(logi_1d_t) :: good
    type(spectrum_t) :: spectrum,mask,masked
    character(len=*), parameter :: rname='VELOCITY>PROG>LOOP'
    !
    call spectrum%associate('spectrum',prog%cube,iter,error)
    if (error) return
    call good%reallocate('good',spectrum%n,error)
    if (error) return
    call mask%associate('mask',prog%mask%cube,iter,error)
    if (error) return
    call masked%allocate('masked',prog%masked,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter%ie,spectrum,good,mask,masked,error)
      if (error) return
    enddo ! ie
  end subroutine cubemask_velocity_prog_loop
  !   
  subroutine cubemask_velocity_prog_act(prog,ie,spectrum,good,mask,masked,error)
    use cubetools_nan
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(velocity_prog_t), target, intent(inout) :: prog
    integer(kind=entr_k),   intent(in)    :: ie
    type(spectrum_t),       intent(inout) :: spectrum
    type(logi_1d_t),        intent(inout) :: good
    type(spectrum_t),       intent(inout) :: mask
    type(spectrum_t),       intent(inout) :: masked
    logical,                intent(inout) :: error
    !
    logical :: maskstatus
    integer(kind=indx_k) :: if
    integer(kind=chan_k) :: ic,oc
    integer(kind=chan_k), pointer :: ocmin,ocmax
    character(len=*), parameter :: rname='VELOCITY>PROG>ACT'
    !
    call spectrum%get(ie,error)
    if (error) return
    call mask%get(ie,error)
    if (error) return
    call good%set(.false.,error)
    if (error) return
    do if=1,prog%frequency%n
       do ic=1,mask%y%n
          ocmin => prog%ocmin%val(ic,if)
          ocmax => prog%ocmax%val(ic,if)
          maskstatus = .not.isnan(mask%y%val(ic))
          do oc=ocmin,ocmax
             good%val(oc) = good%val(oc).or.maskstatus
          enddo ! oc
       enddo ! ic
    enddo ! if
    do oc=1,good%n
       if (good%val(oc)) then
          masked%y%val(oc) = spectrum%y%val(oc)
       else
          masked%y%val(oc) = gr4nan
       endif
    enddo ! oc
    call masked%put(ie,error)
    if (error) return
  end subroutine cubemask_velocity_prog_act
end module cubemask_velocity
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
