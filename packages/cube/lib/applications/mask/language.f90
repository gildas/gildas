!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemask_language
  use cubetools_structure
  use cubemask_messaging
  !
  use cubemask_cube
  use cubemask_erode
  use cubemask_invert
  use cubemask_poly2mask
  use cubemask_velocity
  !
  public :: cubemask_register_language
  private
  !
  integer(kind=lang_k) :: langid
  !
contains
  !
  subroutine cubemask_register_language(error)
    !----------------------------------------------------------------------
    ! Register the MASK\ language
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    call cubetools_register_language('MASK',&
         'J.Pety, S.Bardeau',&
         'Commands to interact with masks',&
         'gag_doc:hlp/cube-help-mask.hlp',&
         cubemask_execute_command,langid,error)
    if (error) return
    !----------------------------------------------------------------------
    !
    call cube%register(error)
    if (error) return
    call cubemask_erode_register(error)
    if (error) return
    call cubemask_invert_register(error)
    if (error) return
    call poly2mask%register(error)
    if (error) return
    call velocity%register(error)
    if (error) return
    !
    call cubetools_register_dict(error)
    if (error) return
  end subroutine cubemask_register_language
  !
  subroutine cubemask_execute_command(line,comm,error)
    use cubeadm_opened
    use cubeadm_timing
    !----------------------------------------------------------------------
    ! Execute a command of the MASK\ language
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    character(len=*), intent(in)    :: comm
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='EXECUTE>COMMAND'
    !
    error = .false.
    if (comm.eq.'MASK?') then
       call cubetools_list_language_commands(langid,error)
       if (error) return
    else
       call cubeadm_timing_init()
       call cubetools_execute_command(line,langid,comm,error)
       if (error) continue ! To ensure error recovery in the call to cubeadm_finish_all
       call cubeadm_finish_all(comm,line,error)
       if (error) continue ! To ensure error recovery in timing
       call cubeadm_timing_final(comm)
    endif
  end subroutine cubemask_execute_command
end module cubemask_language
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
