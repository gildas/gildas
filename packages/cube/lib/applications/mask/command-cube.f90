!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This version allows one to do CUBE cubeid /AS imageid
! We may want a version that does CUBE cubeid /AS imageid
! and another one that does CUBE cubeid /AS spectrumid
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemask_cube
  use cubetools_parameters
  use cube_types
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubemain_ancillary_mask_types
  use cubetopology_cuberegion_types
  use cubemask_messaging
  !
  public :: cube
  private
  !
  type :: cube_comm_t
     type(option_t), pointer :: comm
     type(cuberegion_comm_t) :: region
     type(cubeid_arg_t), pointer :: cube
     type(ancillary_mask_comm_t) :: mask
     type(cube_prod_t),  pointer :: masked     
   contains
     procedure, public  :: register => cubemask_cube_comm_register
     procedure, private :: parse    => cubemask_cube_comm_parse
     procedure, private :: main     => cubemask_cube_comm_main
  end type cube_comm_t
  type(cube_comm_t) :: cube
  !
  type cube_user_t
     type(cubeid_user_t)         :: cubeids
     type(ancillary_mask_user_t) :: mask
     type(cuberegion_user_t)     :: region
   contains
     procedure, private :: toprog => cubemask_cube_user_toprog
  end type cube_user_t
  !
  type cube_prog_t
     type(cuberegion_prog_t)     :: region
     type(cube_t), pointer       :: cube
     type(ancillary_mask_prog_t) :: mask
     type(cube_t), pointer       :: masked
   contains
     procedure, private :: header => cubemask_cube_prog_header
     procedure, private :: data   => cubemask_cube_prog_data
     procedure, private :: loop   => cubemask_cube_prog_loop
     procedure, private :: act    => cubemask_cube_prog_act
  end type cube_prog_t
  !
contains
  !
  subroutine cubemask_cube_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(cube_user_t) :: user
    character(len=*), parameter :: rname='CUBE>COMMAND'
    !
    call cubemask_message(maskseve%trace,rname,'Welcome')
    !
    call cube%parse(line,user,error)
    if (error) return
    call cube%main(user,error)
    if (error) continue
  end subroutine cubemask_cube_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemask_cube_comm_register(comm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(cube_comm_t), intent(inout) :: comm
    logical,            intent(inout) :: error
    !
    type(cubeid_arg_t) :: incube
    type(cube_prod_t) :: masked
    character(len=*), parameter :: rname='CUBE>COMM>REGISTER'
    !
    call cubemask_message(maskseve%trace,rname,'Welcome')
    !
    ! Syntax
    call cubetools_register_command(&
         'CUBE','[cubeid]',&
         'Mask a cube following a reference mask pattern',&
         'Input and output cubes must be real',&
         cubemask_cube_command,&
         comm%comm,&
         error)
    if (error) return
    call incube%register(&
         'INPUT',&
         'Input cube',&
         strg_id,&
         code_arg_optional,&
         [flag_any],&
         code_read,&
         code_access_imaset,&
         comm%cube,&
         error)
    if (error) return
    call comm%mask%register(&
         'Mask pattern',&
         code_access_imaset,&
         error)
    if (error) return
    call comm%region%register(error)
    if (error) return
    !
    ! Products
    call masked%register(&
         'MASKED',&
         'Masked cube',&
         strg_id,&
         [flag_masked],&
         comm%masked,&
         error)
    if (error)  return
  end subroutine cubemask_cube_comm_register
  !
  subroutine cubemask_cube_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! CUBE cubeid /AS maskid
    !----------------------------------------------------------------------
    class(cube_comm_t), intent(in)    :: comm
    character(len=*),   intent(in)    :: line
    type(cube_user_t),  intent(out)   :: user
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: rname='CUBE>COMM>PARSE'
    !
    call cubemask_message(maskseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%mask%parse(line,user%mask,error)
    if (error) return
    call comm%region%parse(line,user%region,error)
    if (error) return
  end subroutine cubemask_cube_comm_parse
  !
  subroutine cubemask_cube_comm_main(comm,user,error)
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(cube_comm_t), intent(in)    :: comm
    type(cube_user_t),  intent(inout) :: user
    logical,            intent(inout) :: error
    !
    type(cube_prog_t) :: prog
    character(len=*), parameter :: rname='CUBE>MAIN'
    !
    call cubemask_message(maskseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubemask_cube_comm_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemask_cube_user_toprog(user,comm,prog,error)
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(cube_user_t), intent(in)    :: user
    type(cube_comm_t),  intent(in)    :: comm
    type(cube_prog_t),  intent(out)   :: prog
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: rname='CUBE>USER>TOPROG'
    !
    call cubemask_message(maskseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%cube,user%cubeids,prog%cube,error)
    if (error) return
    call user%mask%toprog(comm%mask,prog%mask,error)
    if (error) return
    call prog%mask%check_consistency(prog%cube,error)
    if (error) return
    call user%region%toprog(prog%cube,prog%region,error)
    if (error) return
    ! User feedback about the interpretation of his command line
    call prog%region%list(error)
    if (error) return
  end subroutine cubemask_cube_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemask_cube_prog_header(prog,comm,error)
    use cubeadm_clone
    use cubetools_header_methods
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(cube_prog_t), intent(inout) :: prog
    type(cube_comm_t),  intent(in)    :: comm
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: rname='CUBE>PROG>HEADER'
    !
    call cubemask_message(maskseve%trace,rname,'Welcome')
    !
    call cubeadm_clone_header_with_region(comm%masked,  &
      prog%cube,prog%region,prog%masked,error)
    if (error) return
  end subroutine cubemask_cube_prog_header
  !
  subroutine cubemask_cube_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(cube_prog_t), intent(inout) :: prog
    logical,            intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='CUBE>PROG>DATA'
    !
    call cubemask_message(maskseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,prog%region,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubemask_cube_prog_data
  !   
  subroutine cubemask_cube_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(cube_prog_t),       intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(image_t) :: image,mask,masked
    character(len=*), parameter :: rname='CUBE>PROG>LOOP'
    !
    call image%associate('image',prog%cube,iter,error)
    if (error) return
    call mask%associate('mask',prog%mask%cube,iter,error)
    if (error) return
    call masked%allocate('masked',prog%masked,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter%ie,image,mask,masked,error)
      if (error) return
    enddo ! ie
  end subroutine cubemask_cube_prog_loop
  !   
  subroutine cubemask_cube_prog_act(prog,ie,image,mask,masked,error)
    use cubetools_nan
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(cube_prog_t),   intent(inout) :: prog
    integer(kind=entr_k), intent(in)    :: ie
    type(image_t),        intent(inout) :: image
    type(image_t),        intent(inout) :: mask
    type(image_t),        intent(inout) :: masked
    logical,              intent(inout) :: error
    !
    integer(kind=pixe_k) :: ix,iy
    character(len=*), parameter :: rname='CUBE>PROG>ACT'
    !
    call image%get(ie,error)
    if (error) return
    call mask%get(ie,error)
    if (error) return
    do iy=1,image%ny
       do ix=1,image%nx
          if (isnan(mask%val(ix,iy))) then
             masked%val(ix,iy) = gr4nan
          else
             masked%val(ix,iy) = image%val(ix,iy)
          endif
       enddo ! ix
    enddo ! iy
    call masked%put(ie,error)
    if (error) return
  end subroutine cubemask_cube_prog_act
end module cubemask_cube
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
