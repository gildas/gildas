!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemask_erode
  use cubecompute_one2one_template
  use cubemask_messaging
  !
  public :: cubemask_erode_register
  private
  !
  type(one2one_comm_t) :: erode
  !
contains
  !
  subroutine cubemask_erode_register(error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    character(len=*), parameter :: rname='ERODE>REGISTER'
    !
    call cubemask_message(maskseve%trace,rname,'Welcome')
    !
    call erode%register_syntax(&
         'ERODE','input cube',&
         [flag_eroded,flag_mask],cubemask_erode_command,error)
    if (error) return
    call erode%act%register_r4tor4(cubemask_erode_prog_act,error)
    if (error) return
  end subroutine cubemask_erode_register
  !
  subroutine cubemask_erode_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(one2one_user_t) :: user
    character(len=*), parameter :: rname='ERODE>COMMAND'
    !
    call cubemask_message(maskseve%trace,rname,'Welcome')
    !
    call erode%parse(line,user,error)
    if (error) return
    call erode%main(user,error)
    if (error) return
  end subroutine cubemask_erode_command
  !
  subroutine cubemask_erode_prog_act(ie,in,ou,error)
    use cubetools_parameters
    use cubetools_nan
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    integer(kind=entr_k), intent(in)    :: ie
    type(image_t),        intent(inout) :: in
    type(image_t),        intent(inout) :: ou
    logical,              intent(inout) :: error
    !
    logical :: erode
    integer(kind=pixe_k) :: mk,nk
    integer(kind=pixe_k) :: ix,iy
    integer(kind=pixe_k) :: jx,jy
    character(len=*), parameter :: rname='ERODE>PROG>ACT'
    !
    mk = 12
    nk = 2*mk+1
    !
    call in%get(ie,error)
    if (error) return
    do iy=1+mk,in%ny-mk
       do ix=1+mk,in%nx-mk
          erode = .false.
          convolution: do jy=iy-mk,iy+mk
             do jx =ix-mk,ix+mk
                if (isnan(in%val(jx,jy))) then
                   erode = .true.
                   exit convolution
                endif
             enddo ! jx
          enddo convolution ! jy
          if (erode) then
             ou%val(ix,iy) = gr4nan
          else
             ou%val(ix,iy) = in%val(ix,iy)
          endif
       enddo ! ix
    enddo ! iy
    !
    do iy=1,mk
       do ix=1,in%nx
          ou%val(ix,iy) = gr4nan
       enddo ! ix
    enddo ! iy
    do iy=in%ny-mk+1,in%ny
       do ix=1,in%nx
          ou%val(ix,iy) = gr4nan
       enddo ! ix
    enddo ! iy
    !
    do iy=1,in%ny
       do ix=1,mk
          ou%val(ix,iy) = gr4nan
       enddo ! ix
    enddo ! iy
    do iy=1,in%ny
       do ix=in%nx-mk+1,in%nx
          ou%val(ix,iy) = gr4nan
       enddo ! ix
    enddo ! iy
    !
    call ou%put(ie,error)
    if (error) return
  end subroutine cubemask_erode_prog_act
end module cubemask_erode
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
