!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage CUBE messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemask_messaging
  use gpack_def
  use gbl_message
  use cubetools_parameters
  !
  public :: maskseve,seve,mess_l
  public :: cubemask_message_set_id,cubemask_message
  public :: cubemask_message_set_alloc,cubemask_message_get_alloc
  public :: cubemask_message_set_trace,cubemask_message_get_trace
  public :: cubemask_message_set_others,cubemask_message_get_others
  private
  !
  ! Identifier used for message identification
  integer(kind=4) :: cubemask_message_id = gpack_global_id  ! Default value for startup message
  !
  type :: cubemask_messaging_debug_t
     integer(kind=code_k) :: alloc = seve%d
     integer(kind=code_k) :: trace = seve%t
     integer(kind=code_k) :: others = seve%d
  end type cubemask_messaging_debug_t
  !
  type(cubemask_messaging_debug_t) :: maskseve
  !
contains
  !
  subroutine cubemask_message_set_id(id)
    !---------------------------------------------------------------------
    ! Alter library id into input id. Should be called by the library
    ! which wants to share its id with the current one.
    !---------------------------------------------------------------------
    integer(kind=4), intent(in) :: id
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='MESSAGE>SET>ID'
    !
    cubemask_message_id = id
    write (mess,'(A,I0)') 'Now use id #',cubemask_message_id
    call cubemask_message(seve%d,rname,mess)
  end subroutine cubemask_message_set_id
  !
  subroutine cubemask_message(mkind,procname,message)
    use cubetools_cmessaging
    !---------------------------------------------------------------------
    ! Messaging facility for the current library. Calls the low-level
    ! (internal) messaging routine with its own identifier.
    !---------------------------------------------------------------------
    integer(kind=4),  intent(in) :: mkind     ! Message kind
    character(len=*), intent(in) :: procname  ! Name of calling procedure
    character(len=*), intent(in) :: message   ! Message string
    !
    call cubetools_cmessage(cubemask_message_id,mkind,'MASK>'//procname,message)
  end subroutine cubemask_message
  !
  subroutine cubemask_message_set_alloc(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       maskseve%alloc = seve%i
    else
       maskseve%alloc = seve%d
    endif
  end subroutine cubemask_message_set_alloc
  !
  subroutine cubemask_message_set_trace(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       maskseve%trace = seve%i
    else
       maskseve%trace = seve%t
    endif
  end subroutine cubemask_message_set_trace
  !
  subroutine cubemask_message_set_others(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       maskseve%others = seve%i
    else
       maskseve%others = seve%d
    endif
  end subroutine cubemask_message_set_others
  !
  function cubemask_message_get_alloc()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubemask_message_get_alloc
    !
    cubemask_message_get_alloc = maskseve%alloc.eq.seve%i
    !
  end function cubemask_message_get_alloc
  !
  function cubemask_message_get_trace()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubemask_message_get_trace
    !
    cubemask_message_get_trace = maskseve%trace.eq.seve%i
    !
  end function cubemask_message_get_trace
  !
  function cubemask_message_get_others()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubemask_message_get_others
    !
    cubemask_message_get_others = maskseve%others.eq.seve%i
    !
  end function cubemask_message_get_others
end module cubemask_messaging
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
