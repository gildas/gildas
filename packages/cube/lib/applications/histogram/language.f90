!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubehistogram_language
  use cubetools_structure
  use cubehistogram_messaging
  !
  use cubehistogram_histo1d
  use cubehistogram_histo2d
  use cubehistogram_histo3d
  use cubehistogram_mask2mask
  use cubehistogram_minmax
  use cubehistogram_mean
  use cubehistogram_ppv2xyv
  !
  public :: cubehistogram_register_language
  private
  !
  integer(kind=lang_k) :: langid
  !
contains
  !
  subroutine cubehistogram_register_language(error)
    !----------------------------------------------------------------------
    ! Register the HISTOGRAM\ language
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    call cubetools_register_language('HISTOGRAM',&
         'J.Pety, S.Bardeau, V.deSouzaMagalhaes',&
         'Compute histograms and operate on them',&
         'gag_doc:hlp/cube-help-ima.hlp',&
         cubehistogram_execute_command,langid,error)
    if (error) return
    !
    call histo1d%register(error)
    if (error) return
    call histo2d%register(error)
    if (error) return
    call histo3d%register(error)
    if (error) return
    call mask2mask%register(error)
    if (error) return    
    call minmax%register(error)
    if (error) return
    call cubehistogram_mean_register(error)
    if (error) return
    call ppv2xyv%register(error)
    if (error) return
    !
    call cubetools_register_dict(error)
    if (error) return
  end subroutine cubehistogram_register_language
  !
  subroutine cubehistogram_execute_command(line,comm,error)
    use cubeadm_opened
    use cubeadm_timing
    !----------------------------------------------------------------------
    ! Execute a command of the HISTOGRAM\ language
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    character(len=*), intent(in)    :: comm
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='EXECUTE>COMMAND'
    !
    error = .false.
    if (comm.eq.'HISTOGRAM?') then
       call cubetools_list_language_commands(langid,error)
       if (error) return
    else
       call cubeadm_timing_init()
       call cubetools_execute_command(line,langid,comm,error)
       if (error) continue ! To ensure error recovery in the call to finalize_all
       call cubeadm_finish_all(comm,line,error)
       if (error) continue ! To ensure error recovery in timing
       call cubeadm_timing_final(comm)
    endif
  end subroutine cubehistogram_execute_command
end module cubehistogram_language
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
