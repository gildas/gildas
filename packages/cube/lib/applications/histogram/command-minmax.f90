!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubehistogram_minmax
  use cube_types
  use cubetools_parameters
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubetopology_cuberegion_types
  use cubehistogram_messaging
  !
  public :: minmax
  private
  !
  type minmax_comm_t
     type(option_t),     pointer :: comm
     type(cubeid_arg_t), pointer :: cube
     type(cuberegion_comm_t)     :: region
     type(cube_prod_t),  pointer :: min
     type(cube_prod_t),  pointer :: max
   contains
     procedure, public  :: register => cubehistogram_minmax_register
     procedure, private :: parse    => cubehistogram_minmax_parse
     procedure, private :: main     => cubehistogram_minmax_main
  end type minmax_comm_t
  type(minmax_comm_t) :: minmax  
  !
  type minmax_user_t
     type(cubeid_user_t)     :: cubeids
     type(cuberegion_user_t) :: region
   contains
     procedure, private :: toprog => cubehistogram_minmax_user_toprog
  end type minmax_user_t
  !
  type minmax_prog_t
     type(cuberegion_prog_t) :: region
     type(cube_t), pointer :: cube
     type(cube_t), pointer :: min
     type(cube_t), pointer :: max
   contains
     procedure, private :: header => cubehistogram_minmax_prog_header
     procedure, private :: data   => cubehistogram_minmax_prog_data
     procedure, private :: loop   => cubehistogram_minmax_prog_loop
     procedure, private :: act    => cubehistogram_minmax_prog_act
  end type minmax_prog_t
  !
contains
  !
  subroutine cubehistogram_minmax_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(minmax_user_t) :: user
    character(len=*), parameter :: rname='MINMAX>COMMAND'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call minmax%parse(line,user,error)
    if (error) return
    call minmax%main(user,error)
    if (error) continue
  end subroutine cubehistogram_minmax_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubehistogram_minmax_register(comm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(minmax_comm_t), intent(inout) :: comm
    logical,              intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    type(cube_prod_t) :: oucube
    character(len=*), parameter :: comm_abstract = 'Compute the minimum and maximum spectra or images'
    character(len=*), parameter :: comm_help = &
         'Input and output cubes are real'
    character(len=*), parameter :: rname='MINMAX>REGISTER'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'MINMAX','[cube]',&
         comm_abstract,&
         comm_help,&
         cubehistogram_minmax_command,&
         comm%comm,error)
    if (error) return
    call cubearg%register(&
         'INPUT',&
         'Input cube',&
         strg_id,&
         code_arg_optional,&
         [flag_any],&
         code_read,&
         code_access_speset,&
         comm%cube,&
         error)
    if (error) return
    !
    call comm%region%register(error)
    if (error) return
    !
    ! Products
    call oucube%register(&
         'MIN',&
         'Cube of minimum values',&
         strg_id,&
         [flag_minimum,flag_image],&
         comm%min,&
         error)
    if (error) return
    call oucube%register(&
         'MAX',&
         'Cube of maximum values',&
         strg_id,&
         [flag_maximum,flag_image],&
         comm%max,&
         error)
    if (error) return
  end subroutine cubehistogram_minmax_register
  !
  subroutine cubehistogram_minmax_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! MINMAX cubname
    ! /SIZE sx [sy]
    ! /CENTER xcen ycen
    ! /RANGE zfirst zlast
    ! /SPECTRUM
    ! /IMAGE
    !----------------------------------------------------------------------
    class(minmax_comm_t), intent(in)    :: comm
    character(len=*),     intent(in)    :: line
    type(minmax_user_t),  intent(out)   :: user
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='MINMAX>PARSE'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%region%parse(line,user%region,error)
    if (error) return
  end subroutine cubehistogram_minmax_parse
  !
  subroutine cubehistogram_minmax_main(comm,user,error)
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(minmax_comm_t), intent(in)    :: comm
    type(minmax_user_t),  intent(inout) :: user
    logical,              intent(inout) :: error
    !
    type(minmax_prog_t) :: prog
    character(len=*), parameter :: rname='MINMAX>MAIN'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubehistogram_minmax_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubehistogram_minmax_user_toprog(user,comm,prog,error)
    use cubetools_consistency_methods
    use cubeadm_get
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(minmax_user_t), intent(in)    :: user
    type(minmax_comm_t),  intent(in)    :: comm
    type(minmax_prog_t),  intent(out)   :: prog
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='MINMAX>USER>TOPROG'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%cube,user%cubeids,prog%cube,error)
    if (error) return
    call user%region%toprog(prog%cube,prog%region,error)
    if (error) return
    call prog%region%list(error)
    if (error) return
  end subroutine cubehistogram_minmax_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubehistogram_minmax_prog_header(prog,comm,error)
    use cubeadm_clone
    use cubetools_header_methods
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(minmax_prog_t), intent(inout) :: prog
    type(minmax_comm_t),  intent(in)    :: comm
    logical,              intent(inout) :: error
    !
    integer(kind=chan_k), parameter :: one=1
    character(len=*), parameter :: rname='MINMAX>PROG>HEADER'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call cubeadm_clone_header(comm%min,prog%cube,prog%min,error)
    if (error) return
    call prog%region%header(prog%min,error)
    if (error) return
    call cubetools_header_put_nchan(one,prog%min%head,error)
    if (error) return
    !
    call cubeadm_clone_header(comm%max,prog%cube,prog%max,error)
    if (error) return
    call prog%region%header(prog%max,error)
    if (error) return
    call cubetools_header_put_nchan(one,prog%max%head,error)
    if (error) return
  end subroutine cubehistogram_minmax_prog_header
  !
  subroutine cubehistogram_minmax_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(minmax_prog_t), intent(inout) :: prog
    logical,              intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='MINMAX>PROG>DATA'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo ! iter
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubehistogram_minmax_prog_data
  !
  subroutine cubehistogram_minmax_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(minmax_prog_t),     intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(spectrum_t) :: spe,min,max
    character(len=*), parameter :: rname='MINMAX>PROG>LOOP'
    !
    call spe%associate('cube',prog%cube,iter,error)
    if (error) return
    call spe%associate_x(error)
    if (error) return
    call min%allocate('min',prog%min,iter,error)
    if (error) return
    call max%allocate('max',prog%max,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
       call prog%act(iter%ie,spe,min,max,error)
       if (error) return
    enddo ! ie
  end subroutine cubehistogram_minmax_prog_loop
  !   
  subroutine cubehistogram_minmax_prog_act(prog,ie,spectrum,spemin,spemax,error)
    use cubetools_nan
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(minmax_prog_t), intent(inout) :: prog
    integer(kind=entr_k), intent(in)    :: ie
    type(spectrum_t),     intent(inout) :: spectrum
    type(spectrum_t),     intent(inout) :: spemin
    type(spectrum_t),     intent(inout) :: spemax
    logical,              intent(inout) :: error
    !
    real(kind=sign_k) :: xval,yval
    real(kind=sign_k) :: mymin,mymax
    integer(kind=chan_k) :: if,il,iz
    integer(kind=chan_k), parameter :: one=1
    character(len=*), parameter :: rname='MINMAX>PROG>ACT'
    !
    call spectrum%get(ie,error)
    if (error) return
    if = prog%region%iz%first
    il = prog%region%iz%last
    mymin = gr4nan
    mymax = gr4nan
    do iz=if,il
       xval = spectrum%x%val(iz)
       yval = spectrum%y%val(iz)
       if (ieee_is_finite(yval)) then
          if (ieee_is_finite(mymin)) then
             mymin = min(mymin,xval)
             mymax = max(mymax,xval)
          else
             mymin = xval
             mymax = xval
          endif
       endif
    enddo ! iz
    spemin%y%val(one) = mymin
    spemax%y%val(one) = mymax
    call spemin%put(ie,error)
    if (error) return
    call spemax%put(ie,error)
    if (error) return
  end subroutine cubehistogram_minmax_prog_act
end module cubehistogram_minmax
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
