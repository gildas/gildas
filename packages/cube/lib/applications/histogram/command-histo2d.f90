!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubehistogram_histo2d
  use cube_types
  use cubetools_parameters
  use cubetools_structure
  use cubetools_switch_types
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubehistogram_messaging
  use cubehistogram_histoaxis_types
  !
  public :: histo2d
  private
  !
  type histo2d_t
     type(cube_t), pointer :: cube
     type(histoaxis_prog_t) :: x
     type(histoaxis_prog_t) :: y
     type(switch_prog_t) :: blank     ! Blank empty bins?
     type(switch_prog_t) :: normalize ! Normalize histogram?
  end type histo2d_t
  !
  type :: histo2d_comm_t
     type(option_t),     pointer :: comm
     type(cubeid_arg_t), pointer :: xcube
     type(cubeid_arg_t), pointer :: ycube
     type(histoaxis_opt_t)       :: x
     type(histoaxis_opt_t)       :: y
     type(switch_comm_t)         :: blank
     type(switch_comm_t)         :: normalize
     type(cube_prod_t),  pointer :: oucube
     type(cube_prod_t),  pointer :: pointer
   contains
     procedure, public  :: register => cubehistogram_histo2d_register
     procedure, private :: parse    => cubehistogram_histo2d_parse
     procedure, private :: main     => cubehistogram_histo2d_main
  end type histo2d_comm_t
  type(histo2d_comm_t) :: histo2d
  !
  type histo2d_user_t
     type(cubeid_user_t) :: cubeids
     type(histoaxis_user_t) :: x
     type(histoaxis_user_t) :: y
     type(switch_user_t) :: blank
     type(switch_user_t) :: normalize
   contains
     procedure, private :: toprog => cubehistogram_histo2d_user_toprog
  end type histo2d_user_t
  !
  type histo2d_prog_t
     type(cube_t), pointer :: xcube   ! Input  cube used as histo2d x-axis
     type(cube_t), pointer :: ycube   ! Input  cube used as histo2d y-axis
     type(cube_t), pointer :: pointer ! Output cube storing the position of the pixel in the 2D histogram
     type(histo2d_t) :: histo2d       ! Output 2D histogram
   contains
     procedure, private :: header => cubehistogram_histo2d_prog_header
     procedure, private :: data   => cubehistogram_histo2d_prog_data
     procedure, private :: loop   => cubehistogram_histo2d_prog_loop
     procedure, private :: act    => cubehistogram_histo2d_prog_act
  end type histo2d_prog_t
  !
contains
  !
  subroutine cubehistogram_histo2d_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(histo2d_user_t) :: user
    character(len=*), parameter :: rname='HISTO2D>COMMAND'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call histo2d%parse(line,user,error)
    if (error) return
    call histo2d%main(user,error)
    if (error) return
  end subroutine cubehistogram_histo2d_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubehistogram_histo2d_register(histo2d,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(histo2d_comm_t), intent(inout) :: histo2d
    logical,               intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    type(cube_prod_t) :: oucube
    character(len=*), parameter :: comm_abst = &
         'Compute the joint histogram of two cubes'
    character(len=*), parameter :: comm_help = &
         'Compute the joint histogram of two cubes'
    character(len=*), parameter :: rname='HISTO2D>REGISTER'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    ! Command
    call cubetools_register_command(&
         'HISTO2D','cube1 cube2',&
         comm_abst,&
         comm_help,&
         cubehistogram_histo2d_command,&
         histo2d%comm,error)
    if (error) return
    call cubearg%register(&
         'XCUBE',&
         'Input cube #1 used as X axis',  &
         strg_id,&
         code_arg_mandatory,&
         [flag_cube],&
         code_read,&
         code_access_imaset,&
         histo2d%xcube,&
         error)
    if (error) return
    call cubearg%register(&
         'YCUBE',&
         'Input cube #2 used as Y axis',&
         strg_id,&
         code_arg_mandatory,&
         [flag_cube],&
         code_read,&
         code_access_imaset,&
         histo2d%ycube,&
         error)
    if (error) return
    !
    call histo2d%x%register('X',error)
    if (error) return
    ! 
    call histo2d%y%register('Y',error)
    if (error) return
    ! 
    call histo2d%blank%register(&
         'BLANK','setting empty bins to NaN',&
         'ON',error)
    if (error) return
    ! 
    call histo2d%normalize%register(&
         'NORMALIZE','histogram normalization from counts to %',&
         'OFF',error)
    if (error) return
    !
    ! Products
    call oucube%register(&
         'HISTO2D',&
         'Joint histogram',&
         strg_id,&
         [flag_histo2d],&
         histo2d%oucube,&
         error)
    if (error) return
    call oucube%register(&
         'Pointer',&
         'Positions back pointer',&
         strg_id,&
         [flag_histo2d,flag_pointer],&
         histo2d%pointer,&
         error)
    if (error) return
  end subroutine cubehistogram_histo2d_register
  !
  subroutine cubehistogram_histo2d_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(histo2d_comm_t), intent(inout) :: comm
    character(len=*),      intent(in)    :: line
    type(histo2d_user_t),  intent(out)   :: user
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='HISTO2D>PARSE'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%x%parse(line,user%x,error)
    if (error) return
    call comm%y%parse(line,user%y,error)
    if (error) return
    call comm%blank%parse(line,user%blank,error)
    if (error) return
    call comm%normalize%parse(line,user%normalize,error)
    if (error) return
  end subroutine cubehistogram_histo2d_parse
  !
  subroutine cubehistogram_histo2d_main(comm,user,error)
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(histo2d_comm_t), intent(in)    :: comm
    type(histo2d_user_t),  intent(inout) :: user
    logical,               intent(inout) :: error
    !
    type(histo2d_prog_t) :: prog
    character(len=*), parameter :: rname='HISTO2D>MAIN'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubehistogram_histo2d_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubehistogram_histo2d_user_toprog(user,comm,prog,error)
    use cubetools_consistency_methods
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(histo2d_user_t), intent(in)    :: user
    type(histo2d_comm_t),  intent(in)    :: comm
    type(histo2d_prog_t),  intent(out)   :: prog
    logical,               intent(inout) :: error
    !
    logical :: prob
    character(len=*), parameter :: rname='HISTO2D>USER2PROG'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(histo2d%xcube,user%cubeids,prog%xcube,error)
    if (error) return
    call cubeadm_get_header(histo2d%ycube,user%cubeids,prog%ycube,error)
    if (error) return
    !
    prob = .false.
    call cubetools_consistency_shape('Input cube #1',prog%xcube%head,'Input cube #2',prog%ycube%head,prob,error)
    if (error) return
    if (cubetools_consistency_failed(rname,prob,error)) return
    !
    call user%x%toprog(comm%x,prog%xcube,prog%histo2d%x,error)
    if (error) return
    call user%y%toprog(comm%y,prog%ycube,prog%histo2d%y,error)
    if (error) return
    call prog%histo2d%x%list(error)
    if (error) return
    call prog%histo2d%y%list(error)
    if (error) return
    !
    call prog%histo2d%normalize%init(comm%normalize,error)
    if (error) return
    call user%normalize%toprog(comm%normalize,prog%histo2d%normalize,error)
    if (error) return
    call prog%histo2d%blank%init(comm%blank,error)
    if (error) return
    call user%blank%toprog(comm%blank,prog%histo2d%blank,error)
    if (error) return
  end subroutine cubehistogram_histo2d_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubehistogram_histo2d_prog_header(prog,comm,error)
    use cubetools_unit
    use cubetools_axis_types
    use cubetools_header_methods
    use cubeadm_clone
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(histo2d_prog_t), intent(inout) :: prog
    type(histo2d_comm_t),  intent(in)    :: comm
    logical,               intent(inout) :: error
    !
    type(axis_t) :: axis
    character(len=unit_l) :: unit
    character(len=*), parameter :: rname='HISTO2D>PROG>HEADER'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call cubeadm_clone_header(comm%pointer,prog%xcube,prog%pointer,error)
    if (error) return
    call cubeadm_clone_header(comm%oucube,prog%xcube,prog%histo2d%cube,error)
    if (error) return
    ! Unit
    if (prog%histo2d%normalize%enabled) then
       call cubetools_header_put_array_unit('%',prog%histo2d%cube%head,error)
       if (error) return
    else
       call cubetools_header_put_array_unit('Counts',prog%histo2d%cube%head,error)
       if (error) return
    endif
    ! X axis
    call cubetools_header_get_array_unit(prog%xcube%head,unit,error)
    if (error) return
    call cubetools_header_get_axis_head_l(prog%xcube%head,axis,error)
    if (error) return
    axis%name = 'X'
    if (prog%histo2d%x%dolog) then
       ! *** JP huge risk of overflow here...
       ! Try to decrease potential overflow by only adding 'log ' at start.
       axis%unit = 'log '//trim(unit)
    else
       axis%unit = unit
    endif
    axis%kind = code_unit_unk
    axis%genuine = .true.
    axis%regular = .true.
    axis%n   = prog%histo2d%x%n
    axis%ref = 1.0
    axis%val = prog%histo2d%x%min
    axis%inc = prog%histo2d%x%inc
    call cubetools_header_update_axset_l(axis,prog%histo2d%cube%head,error)
    if (error) return
    ! Y axis
    call cubetools_header_get_array_unit(prog%ycube%head,unit,error)
    if (error) return
    call cubetools_header_get_axis_head_m(prog%ycube%head,axis,error)
    if (error) return
    axis%name = 'Y'
    if (prog%histo2d%y%dolog) then
       ! *** JP huge risk of overflow here...
       ! Try to decrease potential overflow by only adding 'log ' at start.
       axis%unit = 'log '//trim(unit)
    else
       axis%unit = unit
    endif
    axis%kind = code_unit_unk
    axis%genuine = .true.
    axis%regular = .true.
    axis%n   = prog%histo2d%y%n
    axis%ref = 1.0
    axis%val = prog%histo2d%y%min
    axis%inc = prog%histo2d%y%inc
    call cubetools_header_update_axset_m(axis,prog%histo2d%cube%head,error)
    if (error) return
  end subroutine cubehistogram_histo2d_prog_header
  !
  subroutine cubehistogram_histo2d_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(histo2d_prog_t), intent(inout) :: prog
    logical,               intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='HISTO2D>PROG>DATA'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo ! ie
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubehistogram_histo2d_prog_data
  !
  subroutine cubehistogram_histo2d_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(histo2d_prog_t),    intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(image_t) :: xima,yima,pima,histo2d
    character(len=*), parameter :: rname='HISTO2D>PROG>LOOP'
    !
    ! The allocation of xima and yima allows me to take the logarithm of the inputs
    call xima%allocate('xima',prog%xcube,iter,error)
    if (error) return
    call yima%allocate('yima',prog%ycube,iter,error)
    if (error) return
    call pima%allocate('pointer image',prog%pointer,iter,error)
    if (error) return
    call histo2d%allocate('histo2d',prog%histo2d%cube,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter%ie,xima,yima,pima,histo2d,error)
      if (error) return
    enddo ! ie
  end subroutine cubehistogram_histo2d_prog_loop
  !
  subroutine cubehistogram_histo2d_prog_act(prog,ie,xima,yima,pima,histo2d,error)
    use cubetools_nan
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(histo2d_prog_t), intent(inout) :: prog
    integer(kind=entr_k),  intent(in)    :: ie
    type(image_t), target, intent(inout) :: xima
    type(image_t), target, intent(inout) :: yima
    type(image_t),         intent(inout) :: pima
    type(image_t),         intent(inout) :: histo2d
    logical,               intent(inout) :: error
    !
    integer(kind=pixe_k) :: ix,iy
    integer(kind=pixe_k) :: jx,jy
    integer(kind=data_k) :: nin,nou,nblank
    real(kind=sign_k), pointer :: xval,yval
    character(len=*), parameter :: rname='HISTO2D>ACT'
    !
    ! Get data
    call xima%get(ie,error)
    if (error) return
    call yima%get(ie,error)
    if (error) return
    ! Transform it
    if (prog%histo2d%x%dolog) then
       do iy=1,xima%ny
          do ix=1,xima%nx
             xval => xima%val(ix,iy)
             xval = log10(xval)
          enddo ! iy
       enddo ! ix
    endif
    if (prog%histo2d%y%dolog) then
       do iy=1,xima%ny
          do ix=1,xima%nx
             yval => yima%val(ix,iy)
             yval = log10(yval)
          enddo ! iy
       enddo ! ix
    endif
    ! Compute histogram
    nin = 0
    nou = 0
    nblank = 0
    histo2d%val = 0.0
    pima%val = 0
    do iy=1,xima%ny
       do ix=1,xima%nx
          xval => xima%val(ix,iy)
          yval => yima%val(ix,iy)
          if ((ieee_is_finite(xval)).and.(ieee_is_finite(yval))) then
             jx = nint((xval-prog%histo2d%x%min)/prog%histo2d%x%inc)
             jy = nint((yval-prog%histo2d%y%min)/prog%histo2d%y%inc)
             if ((1.le.jx).and.(jx.le.prog%histo2d%x%n).and.&
                  (1.le.jy).and.(jy.le.prog%histo2d%y%n)) then
                ! *** JP problem when many points?
                pima%val(ix,iy) = jx+(jy-1)*histo2d%nx
                histo2d%val(jx,jy) = histo2d%val(jx,jy)+1.0
                nin = nin+1
             else
                nou = nou+1
             endif
          else
             nblank = nblank+1
          endif
       enddo ! iy
    enddo ! ix
    ! Blank empty bins when asked
    if (prog%histo2d%blank%enabled) then
       do iy=1,prog%histo2d%y%n
          do ix=1,prog%histo2d%x%n
             if (histo2d%val(ix,iy).le.0.0) then
                histo2d%val(ix,iy) = gr4nan
             endif
          enddo ! iy
       enddo ! ix
    endif
    ! Normalize when asked
    if ((prog%histo2d%normalize%enabled).and.(nin.gt.0)) then
       do iy=1,prog%histo2d%y%n
          do ix=1,prog%histo2d%x%n
             histo2d%val(ix,iy) = 100d0*histo2d%val(ix,iy)/real(nin,kind=coor_k)
          enddo ! ix
       enddo ! iy
    endif
    ! Put result
    call pima%put(ie,error)
    if (error) return
    call histo2d%put(ie,error)
    if (error) return
  end subroutine cubehistogram_histo2d_prog_act
end module cubehistogram_histo2d
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
