!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubehistogram_ppv2xyv
  use cube_types 
  use cubetools_parameters
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubetopology_cuberegion_types
  use cubehistogram_messaging
  !
  public :: ppv2xyv
  private
  !
  type :: ppv2xyv_comm_t
     type(option_t), pointer :: comm
!     type(cuberegion_comm_t) :: region
     type(cubeid_arg_t), pointer :: fromto
     type(cubeid_arg_t), pointer :: histo2d
     type(cubeid_arg_t), pointer :: ppv
     type(cube_prod_t),  pointer :: xyv
   contains
     procedure, public  :: register => cubehistogram_ppv2xyv_comm_register
     procedure, private :: parse    => cubehistogram_ppv2xyv_comm_parse
     procedure, private :: main     => cubehistogram_ppv2xyv_comm_main
  end type ppv2xyv_comm_t
  type(ppv2xyv_comm_t) :: ppv2xyv  
  !
  type ppv2xyv_user_t
     type(cubeid_user_t) :: cubeids
!     type(cuberegion_user_t) :: region
   contains
     procedure, private :: toprog => cubehistogram_ppv2xyv_user_toprog
  end type ppv2xyv_user_t
  !
  type ppv2xyv_prog_t
!     type(cuberegion_prog_t) :: region
     type(cube_t), pointer :: fromto
     type(cube_t), pointer :: histo2d
     type(cube_t), pointer :: ppv
     type(cube_t), pointer :: xyv
   contains
     procedure, private :: header => cubehistogram_ppv2xyv_prog_header
     procedure, private :: data   => cubehistogram_ppv2xyv_prog_data
     procedure, private :: loop   => cubehistogram_ppv2xyv_prog_loop
     procedure, private :: act    => cubehistogram_ppv2xyv_prog_act
  end type ppv2xyv_prog_t
  !
contains
  !
  subroutine cubehistogram_ppv2xyv_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(ppv2xyv_user_t) :: user
    character(len=*), parameter :: rname='PPV2XYV>COMMAND'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call ppv2xyv%parse(line,user,error)
    if (error) return
    call ppv2xyv%main(user,error)
    if (error) continue
  end subroutine cubehistogram_ppv2xyv_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubehistogram_ppv2xyv_comm_register(comm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(ppv2xyv_comm_t), intent(inout) :: comm
    logical,                 intent(inout) :: error
    !
    type(cubeid_arg_t) :: incube
    type(cube_prod_t) :: oucube
    character(len=*), parameter :: rname='PPV2XYV>COMM>REGISTER'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    ! Syntax
    call cubetools_register_command(&
         'PPV2XYV','[ppvid fromtoid histo2did]',&
         'Stack spectra according to a 2D histogram',&
         strg_id,&
         cubehistogram_ppv2xyv_command,&
         comm%comm,&
         error)
    if (error) return
    call incube%register(&
         'PPV',&
         'Position-Position-Velocity cube',&
         strg_id,&
         code_arg_optional,&
         !         [flag_ppv,flag_cube],&
         [flag_cube],&
         code_read,&
         code_access_imaset,&
         comm%ppv,&
         error)
    if (error) return
    call incube%register(&
         'FROMTO',&
         'From PP to XY',&
         'Image explaining how to go from a PP image to a bin of XY histo2d',&
         code_arg_optional,&
         [flag_histo2d,flag_pointer],&
         code_read,&
         code_access_imaset,&
         comm%fromto,&
         error)
    if (error) return
    call incube%register(&
         'HISTO2D',&
         '2D histogram',&
         strg_id,&
         code_arg_optional,&
         [flag_histo2d],&
         code_read_head,&
         code_access_imaset,&
         comm%histo2d,&
         error)
    if (error) return
!    call comm%region%register(error)
!    if (error) return
    !
    ! Products
    call oucube%register(&
         'XYV',&
         'X-Y-Velocity cube',&
         strg_id,&
         [flag_xyv,flag_cube],&
         comm%xyv,&
         error)
    if (error)  return
  end subroutine cubehistogram_ppv2xyv_comm_register
  !
  subroutine cubehistogram_ppv2xyv_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! PPV2XYV fromtoid ppvid
    !----------------------------------------------------------------------
    class(ppv2xyv_comm_t), intent(in)    :: comm
    character(len=*),      intent(in)    :: line
    type(ppv2xyv_user_t),  intent(out)   :: user
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='PPV2XYV>COMM>PARSE'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
!!$    call comm%region%parse(line,user%region,error)
!!$    if (error) return
  end subroutine cubehistogram_ppv2xyv_comm_parse
  !
  subroutine cubehistogram_ppv2xyv_comm_main(comm,user,error)
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(ppv2xyv_comm_t), intent(in)    :: comm
    type(ppv2xyv_user_t),  intent(inout) :: user
    logical,               intent(inout) :: error
    !
    type(ppv2xyv_prog_t) :: prog
    character(len=*), parameter :: rname='PPV2XYV>MAIN'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubehistogram_ppv2xyv_comm_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubehistogram_ppv2xyv_user_toprog(user,comm,prog,error)
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(ppv2xyv_user_t), intent(in)    :: user
    type(ppv2xyv_comm_t),  intent(in)    :: comm
    type(ppv2xyv_prog_t),  intent(out)   :: prog
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='PPV2XYV>USER>TOPROG'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%ppv,user%cubeids,prog%ppv,error)
    if (error) return
    call cubeadm_get_header(comm%fromto,user%cubeids,prog%fromto,error)
    if (error) return
    call cubeadm_get_header(comm%histo2d,user%cubeids,prog%histo2d,error)
    if (error) return
!!$    call user%region%toprog(prog%fromto,prog%region,error)
!!$    if (error) return
!!$    ! User feedback about the interpretation of his command line
!!$    call prog%region%list(error)
!!$    if (error) return
  end subroutine cubehistogram_ppv2xyv_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubehistogram_ppv2xyv_prog_header(prog,comm,error)
    use cubeadm_clone
    use cubetools_header_methods
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(ppv2xyv_prog_t), intent(inout) :: prog
    type(ppv2xyv_comm_t),  intent(in)    :: comm
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='PPV2XYV>PROG>HEADER'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call cubeadm_clone_header(comm%xyv,prog%ppv,prog%xyv,error)
    if (error) return
    call cubetools_header_spatial_like(prog%histo2d%head,prog%xyv%head,error)
    if (error) return
!!$    call prog%region%header(prog%xyv,error)
!!$    if (error) return
  end subroutine cubehistogram_ppv2xyv_prog_header
  !
  subroutine cubehistogram_ppv2xyv_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(ppv2xyv_prog_t), intent(inout) :: prog
    logical,               intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='PPV2XYV>PROG>DATA'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubehistogram_ppv2xyv_prog_data
  !   
  subroutine cubehistogram_ppv2xyv_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubetools_array_types
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(ppv2xyv_prog_t),    intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(image_t) :: fromto
    type(image_t) :: pp,xy
    type(dble_2d_t) :: sum
    type(long_2d_t) :: wei
    character(len=*), parameter :: rname='PPV2XYV>PROG>LOOP'
    !
    call fromto%associate('fromto',prog%fromto,iter,error)
    if (error) return
    call pp%associate('pp',prog%ppv,iter,error)
    if (error) return
    call xy%allocate('xy',prog%xyv,iter,error)
    if (error) return
    call sum%reallocate('sum',xy%nx,xy%ny,error)
    if (error) return
    call wei%reallocate('weight',xy%nx,xy%ny,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter%ie,fromto,pp,xy,sum,wei,error)
      if (error) return
    enddo ! ie
  end subroutine cubehistogram_ppv2xyv_prog_loop
  !
  subroutine cubehistogram_ppv2xyv_prog_act(prog,ie,fromto,pp,xy,sum,wei,error)
    use cubetools_nan
    use cubetools_array_types
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(ppv2xyv_prog_t), intent(inout) :: prog
    integer(kind=entr_k),  intent(in)    :: ie
    type(image_t),         intent(inout) :: fromto
    type(image_t),         intent(inout) :: pp
    type(image_t),         intent(inout) :: xy
    type(dble_2d_t),       intent(inout) :: sum
    type(long_2d_t),       intent(inout) :: wei
    logical,               intent(inout) :: error
    !
    integer(kind=pixe_k) :: ix,iy
    integer(kind=pixe_k) :: jx,jy
    integer(kind=indx_k) :: index
    integer(kind=long_k), parameter :: zero=0
    character(len=*), parameter :: rname='PPV2XYV>PROG>ACT'
    !
    call fromto%get(ie,error)
    if (error) return
    call pp%get(ie,error)
    if (error) return
    ! Initialize
    call wei%set(zero,error)
    if (error) return
    call sum%set(0d0,error)
    if (error) return
    ! Sum the input pixels using the fromto mask
    do iy=1,fromto%ny
       do ix=1,fromto%nx
          if (ieee_is_nan(fromto%val(ix,iy))) cycle
          index = fromto%val(ix,iy)
          if (index.eq.0) cycle
          jx = 1+mod(index-1,xy%nx)
          jy = 1+(index-jx)/xy%nx
          ! The user can provide an inconsistent set of fromto and histo2d images!
          if ((1.le.jx).and.(jx.le.wei%nx).and.(1.le.jy).and.(jy.le.wei%ny)) then
             wei%val(jx,jy) = wei%val(jx,jy) + 1
             sum%val(jx,jy) = sum%val(jx,jy) + pp%val(ix,iy)
          else
             ! ***JP: Some shorter feedback to user? For instance counting
             ! ***JP: the number of points outside and stating it at the end?
             print *,jx,jy,wei%nx,wei%ny
          endif
       enddo ! ix
    enddo ! iy
    ! Compute xy
    do iy=1,xy%ny
       do ix=1,xy%nx
          if (wei%val(ix,iy).ne.zero) then
             xy%val(ix,iy) = sum%val(ix,iy)/wei%val(ix,iy)
          else
             xy%val(ix,iy) = gr4nan
          endif
       enddo ! ix
    enddo ! iy
    call xy%put(ie,error)
    if (error) return
  end subroutine cubehistogram_ppv2xyv_prog_act
end module cubehistogram_ppv2xyv
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
