!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage CUBEHISTOGRAM messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubehistogram_messaging
  use gpack_def
  use gbl_message
  use cubetools_parameters
  !
  public :: histogramseve,seve,mess_l
  public :: cubehistogram_message_set_id,cubehistogram_message
  public :: cubehistogram_message_set_alloc,cubehistogram_message_get_alloc
  public :: cubehistogram_message_set_trace,cubehistogram_message_get_trace
  public :: cubehistogram_message_set_others,cubehistogram_message_get_others
  private
  !
  ! Identifier used for message identification
  integer(kind=4) :: cubehistogram_message_id = gpack_global_id  ! Default value for startup message
  !
  type :: cubehistogram_messaging_debug_t
     integer(kind=code_k) :: alloc = seve%d
     integer(kind=code_k) :: trace = seve%t
     integer(kind=code_k) :: others = seve%d
  end type cubehistogram_messaging_debug_t
  !
  type(cubehistogram_messaging_debug_t) :: histogramseve
  !
contains
  !
  subroutine cubehistogram_message_set_id(id)
    !---------------------------------------------------------------------
    ! Alter library id into input id. Should be called by the library
    ! which wants to share its id with the current one.
    !---------------------------------------------------------------------
    integer(kind=4), intent(in) :: id
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='MESSAGE>SET>ID'
    !
    cubehistogram_message_id = id
    write (mess,'(A,I0)') 'Now use id #',cubehistogram_message_id
    call cubehistogram_message(seve%d,rname,mess)
  end subroutine cubehistogram_message_set_id
  !
  subroutine cubehistogram_message(mkind,procname,message)
    use cubetools_cmessaging
    !---------------------------------------------------------------------
    ! Messaging facility for the current library. Calls the low-level
    ! (internal) messaging routine with its own identifier.
    !---------------------------------------------------------------------
    integer(kind=4),  intent(in) :: mkind     ! Message kind
    character(len=*), intent(in) :: procname  ! Name of calling procedure
    character(len=*), intent(in) :: message   ! Message string
    !
    call cubetools_cmessage(cubehistogram_message_id,mkind,'IMA>'//procname,message)
  end subroutine cubehistogram_message
  !
  subroutine cubehistogram_message_set_alloc(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       histogramseve%alloc = seve%i
    else
       histogramseve%alloc = seve%d
    endif
  end subroutine cubehistogram_message_set_alloc
  !
  subroutine cubehistogram_message_set_trace(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       histogramseve%trace = seve%i
    else
       histogramseve%trace = seve%t
    endif
  end subroutine cubehistogram_message_set_trace
  !
  subroutine cubehistogram_message_set_others(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       histogramseve%others = seve%i
    else
       histogramseve%others = seve%d
    endif
  end subroutine cubehistogram_message_set_others
  !
  function cubehistogram_message_get_alloc()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubehistogram_message_get_alloc
    !
    cubehistogram_message_get_alloc = histogramseve%alloc.eq.seve%i
    !
  end function cubehistogram_message_get_alloc
  !
  function cubehistogram_message_get_trace()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubehistogram_message_get_trace
    !
    cubehistogram_message_get_trace = histogramseve%trace.eq.seve%i
    !
  end function cubehistogram_message_get_trace
  !
  function cubehistogram_message_get_others()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubehistogram_message_get_others
    !
    cubehistogram_message_get_others = histogramseve%others.eq.seve%i
    !
  end function cubehistogram_message_get_others
end module cubehistogram_messaging
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
