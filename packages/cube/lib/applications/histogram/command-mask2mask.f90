!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubehistogram_mask2mask
  use cube_types 
  use cubetools_parameters
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubetopology_cuberegion_types
  use cubehistogram_messaging
  !
  public :: mask2mask
  private
  !
  type range_t
     logical :: found = .false.
     integer(kind=indx_k) :: first  = 1
     integer(kind=indx_k) :: last   = 1
  end type range_t
  !
  type :: mask2mask_comm_t
     type(option_t), pointer :: comm
!     type(cuberegion_comm_t) :: region
     type(cubeid_arg_t), pointer :: pointer
     type(cubeid_arg_t), pointer :: inmask
     type(cube_prod_t),  pointer :: oumask     
   contains
     procedure, public  :: register => cubehistogram_mask2mask_comm_register
     procedure, private :: parse    => cubehistogram_mask2mask_comm_parse
     procedure, private :: main     => cubehistogram_mask2mask_comm_main
  end type mask2mask_comm_t
  type(mask2mask_comm_t) :: mask2mask  
  !
  type mask2mask_user_t
     type(cubeid_user_t)     :: cubeids
!     type(cuberegion_user_t) :: region
   contains
     procedure, private :: toprog => cubehistogram_mask2mask_user_toprog
  end type mask2mask_user_t
  !
  type mask2mask_prog_t
!     type(cuberegion_prog_t) :: region
     type(cube_t), pointer   :: pointer
     type(cube_t), pointer   :: inmask
     type(cube_t), pointer   :: oumask
   contains
     procedure, private :: header => cubehistogram_mask2mask_prog_header
     procedure, private :: data   => cubehistogram_mask2mask_prog_data
     procedure, private :: loop   => cubehistogram_mask2mask_prog_loop
     procedure, private :: act    => cubehistogram_mask2mask_prog_act
  end type mask2mask_prog_t
  !
contains
  !
  subroutine cubehistogram_mask2mask_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(mask2mask_user_t) :: user
    character(len=*), parameter :: rname='MASK2MASK>COMMAND'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call mask2mask%parse(line,user,error)
    if (error) return
    call mask2mask%main(user,error)
    if (error) continue
  end subroutine cubehistogram_mask2mask_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubehistogram_mask2mask_comm_register(comm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(mask2mask_comm_t), intent(inout) :: comm
    logical,                 intent(inout) :: error
    !
    type(cubeid_arg_t) :: incube
    type(cube_prod_t) :: oucube
    character(len=*), parameter :: comm_abstract='coucou'
    character(len=*), parameter :: comm_help='coucou'
    character(len=*), parameter :: rname='MASK2MASK>COMM>REGISTER'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    ! Syntax
    call cubetools_register_command(&
         'MASK2MASK','[pointerid maskid]',&
         comm_abstract,&
         comm_help,&
         cubehistogram_mask2mask_command,&
         comm%comm,&
         error)
    if (error) return
    call incube%register(&
         'POINTER',&
         'Pointer from original cubes to histogram',&
         strg_id,&
         code_arg_optional,&
         [flag_pointer],&
         code_read,&
         code_access_imaset,&
         comm%pointer,&
         error)
    if (error) return
    call incube%register(&
         'INPUT MASK',&
         'Mask of histogram',&
         strg_id,&
         code_arg_optional,&
         [flag_mask],&
         code_read,&
         code_access_imaset,&
         comm%inmask,&
         error)
    if (error) return
!    call comm%region%register(error)
!    if (error) return
    !
    ! Products
    call oucube%register(&
         'OUTPUT MASK',&
         'Mask of original cubes',&
         strg_id,&
         [flag_mask],&
         comm%oumask,&
         error)
    if (error)  return
  end subroutine cubehistogram_mask2mask_comm_register
  !
  subroutine cubehistogram_mask2mask_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! MASK2MASK cubname
    !----------------------------------------------------------------------
    class(mask2mask_comm_t), intent(in)    :: comm
    character(len=*),        intent(in)    :: line
    type(mask2mask_user_t),  intent(out)   :: user
    logical,                 intent(inout) :: error
    !
    character(len=*), parameter :: rname='MASK2MASK>COMM>PARSE'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
!!$    call comm%region%parse(line,user%region,error)
!!$    if (error) return
  end subroutine cubehistogram_mask2mask_comm_parse
  !
  subroutine cubehistogram_mask2mask_comm_main(comm,user,error)
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(mask2mask_comm_t), intent(in)    :: comm
    type(mask2mask_user_t),  intent(inout) :: user
    logical,                 intent(inout) :: error
    !
    type(mask2mask_prog_t) :: prog
    character(len=*), parameter :: rname='MASK2MASK>MAIN'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubehistogram_mask2mask_comm_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubehistogram_mask2mask_user_toprog(user,comm,prog,error)
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(mask2mask_user_t), intent(in)    :: user
    type(mask2mask_comm_t),  intent(in)    :: comm
    type(mask2mask_prog_t),  intent(out)   :: prog
    logical,                 intent(inout) :: error
    !
    character(len=*), parameter :: rname='MASK2MASK>USER>TOPROG'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%pointer,user%cubeids,prog%pointer,error)
    if (error) return
    call cubeadm_get_header(comm%inmask,user%cubeids,prog%inmask,error)
    if (error) return
!!$    call user%region%toprog(prog%pointer,prog%region,error)
!!$    if (error) return
!!$    ! User feedback about the interpretation of his command line
!!$    call prog%region%list(error)
!!$    if (error) return
  end subroutine cubehistogram_mask2mask_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubehistogram_mask2mask_prog_header(prog,comm,error)
    use cubeadm_clone
    use cubetools_header_methods
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(mask2mask_prog_t), intent(inout) :: prog
    type(mask2mask_comm_t),  intent(in)    :: comm
    logical,                 intent(inout) :: error
    !
    character(len=*), parameter :: rname='MASK2MASK>PROG>HEADER'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call cubeadm_clone_header(comm%oumask,prog%pointer,prog%oumask,error)
    if (error) return
!!$    call prog%region%header(prog%oumask,error)
!!$    if (error) return
  end subroutine cubehistogram_mask2mask_prog_header
  !
  subroutine cubehistogram_mask2mask_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(mask2mask_prog_t), intent(inout) :: prog
    logical,                 intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='MASK2MASK>PROG>DATA'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubehistogram_mask2mask_prog_data
  !   
  subroutine cubehistogram_mask2mask_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(mask2mask_prog_t),  intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(image_t) :: pimage
    type(image_t) :: inmask
    type(image_t) :: oumask
    character(len=*), parameter :: rname='MASK2MASK>PROG>LOOP'
    !
    call pimage%associate('pimage',prog%pointer,iter,error)
    if (error) return
    call inmask%associate('inimask',prog%inmask,iter,error)
    if (error) return
    call oumask%allocate('ouimask',prog%oumask,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter%ie,pimage,inmask,oumask,error)
      if (error) return
    enddo ! ie
  end subroutine cubehistogram_mask2mask_prog_loop
  !
  subroutine cubehistogram_mask2mask_prog_act(prog,ie,pimage,inmask,oumask,error)
    use cubetools_nan
    use cubetools_array_types
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(mask2mask_prog_t), intent(inout) :: prog
    integer(kind=entr_k),    intent(in)    :: ie
    type(image_t),           intent(inout) :: pimage
    type(image_t),           intent(inout) :: inmask
    type(image_t),           intent(inout) :: oumask
    logical,                 intent(inout) :: error
    !
    integer(kind=pixe_k) :: ix,iy
    integer(kind=indx_k) :: il,nl
    integer(kind=indx_k) :: index
    integer(kind=indx_k), parameter  :: ncol=3
    integer(kind=indx_k), parameter  :: ixcol=1
    integer(kind=indx_k), parameter  :: iycol=2
    integer(kind=indx_k), parameter  :: idcol=3
    type(long_2d_t) :: inlist
    type(range_t) :: range
    character(len=*), parameter :: rname='MASK2MASK>PROG>ACT'
    !
    call pimage%get(ie,error)
    if (error) return
    call inmask%get(ie,error)
    if (error) return
    !
    nl = 0
    do iy=1,pimage%ny
       do ix=1,pimage%nx
          if (pimage%val(ix,iy).gt.0) nl = nl+1
       enddo ! ix
    enddo ! iy
    !
    call inlist%reallocate('inlist',nl,ncol,error)
    if (error) return
    il = 0
    do iy=1,pimage%ny
       do ix=1,pimage%nx
          if (pimage%val(ix,iy).gt.0) then
             il = il+1
             inlist%val(il,1) = ix
             inlist%val(il,2) = iy
             inlist%val(il,3) = pimage%val(ix,iy) 
          endif
       enddo ! ix
    enddo ! iy
    call sort_list_along_column(inlist,idcol,error)
    if (error) return
    do il=1,inlist%nx
       print *,il,inlist%val(il,1),inlist%val(il,2),inlist%val(il,3)
    enddo ! il
    print *,''
    print *,''
    !
    call oumask%set(gr4nan,error)
    if (error) return
    ! Loop over histo2dmask
    do iy=1,inmask%ny
       do ix=1,inmask%nx
          if (inmask%val(ix,iy).gt.0) then
             index = ix+(iy-1)*inmask%nx
             call find(inlist,idcol,index,range,error)
             if (error) return
             if (range%found) then
                print *,'Found index',range%first,range%last,&
                     'for val',index,inlist%val(range%first,idcol),inlist%val(range%last,idcol),&
                     'before',inlist%val(range%first-1,idcol),'after',inlist%val(range%last+1,idcol)
                do il=range%first,range%last
                   oumask%val(inlist%val(il,ixcol),inlist%val(il,iycol)) = 1
                enddo ! il
             endif
          endif
       enddo ! ix
    enddo ! iy
    !
    call oumask%put(ie,error)
    if (error) return
    !
  contains
    !
    subroutine sort_list_along_column(list,icol,error)
      use gkernel_interfaces
      type(long_2d_t),      intent(inout) :: list
      integer(kind=indx_k), intent(in)    :: icol
      logical,              intent(inout) :: error
      !
      integer(kind=indx_k) :: iy
      type(long_1d_t) :: sortedidx
      !
      call sortedidx%reallocate('sortedidx',list%nx,error)
      if (error) return
      call gi8_trie_i8(list%val(:,icol),sortedidx%val,list%nx,error)
      if (error) return
      do iy=1,list%ny
         if (iy.ne.icol) then
            call sort(list%val(:,iy),sortedidx%val,list%nx,error)
            if (error) return
         endif
      enddo ! iy
    end subroutine sort_list_along_column
    !
    subroutine sort(x,key,nx,error)
      use gkernel_interfaces
      !---------------------------------------------------------------------
      ! Reorder an integer*8 array by increasing order using the sorted
      ! indexes computed by a G*_TRIE subroutine
      !---------------------------------------------------------------------
      integer(kind=indx_k), intent(in)    :: nx
      integer(kind=indx_k), intent(inout) :: x(nx)
      integer(kind=indx_k), intent(in)    :: key(nx)
      logical,              intent(inout) :: error
      !
      integer(kind=4) :: ier
      integer(kind=indx_k) :: ix
      integer(kind=indx_k), allocatable :: work(:)
      !
      allocate(work(nx),stat=ier)
      if (failed_allocate(rname,'work',ier,error)) return
      !
      if (nx.le.1) return
      do ix=1,nx
         work(ix) = x(key(ix))
      enddo ! ix
      do ix=1,nx
         x(ix) = work(ix)
      enddo ! ix
    end subroutine sort
    !
    subroutine find(list,icol,val,range,error)
      type(long_2d_t),      intent(in)    :: list
      integer(kind=indx_k), intent(in)    :: icol
      integer(kind=indx_k), intent(in)    :: val
      type(range_t),        intent(inout) :: range
      logical,              intent(inout) :: error
      !
      integer(kind=indx_k) :: imin,imid,imax
      !
      ! Sanity check
      if (list%val(1,icol).gt.list%val(list%nx,icol)) then
         print *,"Input column isn't sorted by increasing values"
         error = .true.
         return
      endif
      ! Initialize
      imin = range%first
      imax = list%nx
      ! Dichotomic search
      do while (imax.gt.imin+1)
         imid = floor((imin+imax)/2.)
!         print *,'Before index',imin,imid,imax,'val',list%val(imin,icol),list%val(imid,icol),list%val(imax,icol),val
         if (list%val(imid,icol).lt.val) then
            imin = imid
         else
            imax = imid
         endif
!         print *,'After  index',imin,imid,imax,'val',list%val(imin,icol),list%val(imid,icol),list%val(imax,icol),val
!         print *,''
      enddo
      if ((list%val(imin,icol).lt.val).and.(list%val(imax,icol).gt.val)) then
         range%first = imin
         range%last  = imax
         range%found = .false.
         return
      endif
      !
      if (list%val(imin,icol).eq.val) then
         range%first = imin
         imax = list%nx
      else if (list%val(imax,icol).eq.val) then
         range%first = imax
         imax = list%nx
      endif
      ! Dichotomic search
      do while (imax.gt.imin+1)
         imid = floor((imin+imax)/2.)
!         print *,'Before index',imin,imid,imax,'val',list%val(imin,icol),list%val(imid,icol),list%val(imax,icol),val
         if (list%val(imid,icol).le.val) then
            imin = imid
         else
            imax = imid
         endif
!         print *,'After  index',imin,imid,imax,'val',list%val(imin,icol),list%val(imid,icol),list%val(imax,icol),val
!         print *,''
      enddo
      range%last = imin
      range%found = .true.
    end subroutine find
  end subroutine cubehistogram_mask2mask_prog_act
end module cubehistogram_mask2mask
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
