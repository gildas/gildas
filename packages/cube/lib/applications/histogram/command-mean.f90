!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubehistogram_mean
  use cubetemplate_one2two_real_template
  use cubehistogram_messaging
  !
  public :: cubehistogram_mean_register
  private
  !
  type(one2two_real_comm_t) :: mean
  !
contains
  !
  subroutine cubehistogram_mean_register(error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    character(len=*), parameter :: rname='MEAN>REGISTER'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call mean%register_syntax(&
         'MEAN','histo3d','3D histogram',[flag_histo3d],cubehistogram_mean_command,&
         'mean',[flag_mean,flag_image],&
         'rms', [flag_rms ,flag_image],&
         error)
    if (error) return
    call mean%register_act(cubehistogram_mean_prog_act,error)
    if (error) return
  end subroutine cubehistogram_mean_register
  !
  subroutine cubehistogram_mean_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(one2two_real_user_t) :: user
    character(len=*), parameter :: rname='MEAN>COMMAND'
    !
    call cubehistogram_message(histogramseve%trace,rname,'Welcome')
    !
    call mean%parse(line,user,error)
    if (error) return
    call mean%main(user,error)
    if (error) continue
  end subroutine cubehistogram_mean_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubehistogram_mean_prog_act(prog,ie,spec,good,mean,rms,error)
    use cubetools_parameters
    use cubetools_nan
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(one2two_real_prog_t), intent(inout) :: prog
    integer(kind=entr_k),       intent(in)    :: ie
    type(spectrum_t),           intent(inout) :: spec
    type(spectrum_t),           intent(inout) :: good
    type(spectrum_t),           intent(inout) :: mean
    type(spectrum_t),           intent(inout) :: rms
    logical,                    intent(inout) :: error
    !
    real(kind=sign_k) :: xval,yval
    real(kind=sign_k) :: num,den
    integer(kind=chan_k) :: iz
    integer(kind=chan_k), parameter :: one=1
    character(len=*), parameter :: rname='MEAN>PROG>ACT'
    !
    call spec%get(ie,error)
    if (error) return
    num = 0
    den = 0
    do iz=prog%region%iz%first,prog%region%iz%last
       xval = spec%x%val(iz)
       yval = spec%y%val(iz)
       if (ieee_is_finite(yval)) then
          num = num+yval*xval
          den = den+yval
       endif
    enddo ! iz
    if (den.ne.0) then
       mean%y%val(one) = num/den
       rms%y%val(one) = num/den
    else
       mean%y%val(one) = gr4nan
       rms%y%val(one) = gr4nan
    endif
    call mean%put(ie,error)
    if (error) return
    call rms%put(ie,error)
    if (error) return
  end subroutine cubehistogram_mean_prog_act
end module cubehistogram_mean
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
