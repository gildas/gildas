!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_feather
  use cubetools_parameters
  use cube_types
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubetools_beam_types
  use cubemain_messaging
  !
  public :: feather
  private
  !
  type :: feather_comm_t
     type(option_t),     pointer :: comm
     type(cubeid_arg_t), pointer :: lores
     type(cubeid_arg_t), pointer :: hires
     type(option_t),     pointer :: factor
     type(cube_prod_t),  pointer :: feather
   contains
     procedure, public  :: register     => cubemain_feather_register
     procedure, private :: parse        => cubemain_feather_parse
     procedure, private :: parse_factor => cubemain_feather_parse_factor
     procedure, private :: main         => cubemain_feather_main
  end type feather_comm_t
  type(feather_comm_t) :: feather
  !
  type feather_user_t
     type(cubeid_user_t)   :: cubeids
     real(kind=sign_k)     :: lofac = 1.0  ! [---] Low resolution cube scalling factor
   contains
     procedure, private :: toprog => cubemain_feather_user_toprog
  end type feather_user_t
  type feather_prog_t
     type(cube_t), pointer :: lores                    ! Low resolution input cube
     type(cube_t), pointer :: hires                    ! High resolution input cube
     type(cube_t), pointer :: feather                  ! Output cube 
     complex(kind=sign_k), allocatable :: cweight(:,:) ! [---] Complex weight for the high resolution cube 
     real(kind=sign_k)     :: lofac                    ! [---] Low resolution cube scalling factor
     ! VVV Dim has to have kind=4 because of previous interfaces
     integer(kind=4)       :: dim(2)                   ! [pix,pix] Dimensions for the FFT
     integer(kind=pixe_k)  :: fftdim(2)                ! FFT dimensions
     type(beam_t)          :: lobeam,hibeam            ! Low and high resolution beams
   contains
     procedure, private :: header       => cubemain_feather_prog_header
     procedure, private :: data         => cubemain_feather_prog_data
     procedure, private :: loop         => cubemain_feather_prog_loop
     procedure, private :: act          => cubemain_feather_prog_act
     !
     procedure, private :: hires_weight => cubemain_feather_prog_hires_weight
  end type feather_prog_t
  !
contains
  !
  subroutine cubemain_feather_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(feather_user_t) :: user
    character(len=*), parameter :: rname='FEATHER>COMMAND'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call feather%parse(line,user,error)
    if (error) return
    call feather%main(user,error)
    if (error) continue
  end subroutine cubemain_feather_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_feather_register(feather,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(feather_comm_t), intent(inout) :: feather
    logical,               intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    type(cube_prod_t) :: oucube
    type(standard_arg_t) :: stdarg
    character(len=*), parameter :: comm_abstract = &
         'Combine two cubes of different spatial resolutions'
    character(len=*), parameter :: comm_help = &
         'The combination is done on the UV plane via the fourier&
         & transform of the input images. lores must have a coarser&
         & spatial resolution than hires. lores and hires must have&
         & the same spectral and spatial gridding, this can be&
         & achieved using CUBE\RESAMPLE and CUBE\REPROJECT'
    character(len=*), parameter :: rname='FEATHER>REGISTER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'FEATHER','lores hires',&
         comm_abstract,&
         comm_help,&
         cubemain_feather_command,&
         feather%comm,&
         error)
    if (error) return
    call cubearg%register(&
         'LORES',&
         'Lower resolution cube',&
         strg_id,&
         code_arg_mandatory,&
         [flag_cube],&
         code_read,&
         code_access_imaset,&
         feather%lores,&
         error)
    if (error) return
    call cubearg%register(&
         'HIRES',&
         'Higher resolution cube',&
         strg_id,&
         code_arg_mandatory,&
         [flag_cube],&
         code_read,&
         code_access_imaset,&
         feather%hires,&
         error)
    if (error) return
    !    
    call cubetools_register_option(&
         'FACTOR','lofac',&
         'Define a flux density scaling factor for LORES',&
         strg_id,&
         feather%factor,&
         error)
    if (error) return
    call stdarg%register(&
         'lofac',&
         'flux density scaling factor for LORES',&
         strg_id,&
         code_arg_mandatory,&
         error)
    if (error) return
    !
    ! Product
    call oucube%register(&
         'FEATHER',&
         'Feather cube',&
         strg_id,&
         [flag_feather,flag_cube],&
         feather%feather,&
         error)
    if (error) return
  end subroutine cubemain_feather_register
  !
  subroutine cubemain_feather_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! FEATHER lores hires
    ! [/FACTOR lofac]
    !----------------------------------------------------------------------
    class(feather_comm_t), intent(in)    :: comm
    character(len=*),      intent(in)    :: line
    type(feather_user_t),  intent(out)   :: user
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='FEATHER>PARSE'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%parse_factor(line,user,error)
    if (error) return
  end subroutine cubemain_feather_parse
  !
  subroutine cubemain_feather_parse_factor(comm,line,user,error)
    !----------------------------------------------------------------------
    ! FEATHER 
    ! /FACTOR lofac
    !----------------------------------------------------------------------
    class(feather_comm_t), intent(in)    :: comm
    character(len=*),      intent(in)    :: line
    type(feather_user_t),  intent(inout) :: user
    logical,               intent(inout) :: error
    !
    logical :: present
    character(len=*), parameter :: rname='FEATHER>PARSE>FACTOR'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call comm%factor%present(line,present,error)
    if (error) return
    if (present) then
       call cubetools_getarg(line,comm%factor,1,user%lofac,mandatory,error)
       if (error) return
    else
       user%lofac = 1.0
    endif
  end subroutine cubemain_feather_parse_factor
  !
  subroutine cubemain_feather_main(comm,user,error)
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(feather_comm_t), intent(in)    :: comm
    type(feather_user_t),  intent(in)    :: user
    logical,               intent(inout) :: error
    !
    type(feather_prog_t) :: prog
    character(len=*), parameter :: rname='FEATHER>MAIN'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubemain_feather_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_feather_user_toprog(user,comm,prog,error)
    use cubetools_shape_types
    use cubetools_header_methods
    use cubetools_consistency_methods
    use cubetools_brightness
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(feather_user_t), intent(in)    :: user
    type(feather_comm_t),  intent(in)    :: comm
    type(feather_prog_t),  intent(out)   :: prog
    logical,               intent(inout) :: error
    !
    type(shape_t) :: n
    logical :: conspb
    character(len=unit_l) :: lounit,hiunit
    real(kind=coor_k) :: loarea, hiarea
    character(len=*), parameter :: rname='FEATHER>USER>TOPROG'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%lores,user%cubeids,prog%lores,error)
    if (error) return
    call cubeadm_get_header(comm%hires,user%cubeids,prog%hires,error)
    if (error) return
    !
    ! Unit checks
    call cubetools_header_get_array_unit(prog%hires%head,hiunit,error)
    if (error) return
    call cubetools_header_get_array_unit(prog%lores%head,lounit,error)
    if (error) return
    call cubetools_header_get_spabeam(prog%lores%head,prog%lobeam,error)
    if (error) return
    call cubetools_header_get_spabeam(prog%hires%head,prog%hibeam,error)
    if (error) return
    if (trim(hiunit).ne.brightness_get(code_unit_jyperbeam)) then
       call cubemain_message(seve%e,rname,'High resolution image unit&
            & must be '//brightness_get(code_unit_jyperbeam))
       error = .true.
    endif
    if (trim(lounit).ne.brightness_get(code_unit_jyperbeam)) then
       call cubemain_message(seve%e,rname,'Low resolution image unit&
            & must be '//brightness_get(code_unit_jyperbeam))
       error = .true.
    endif
    loarea = prog%lobeam%major*prog%lobeam%minor
    hiarea = prog%hibeam%major*prog%hibeam%minor
    if (loarea.le.hiarea) then
       call cubemain_message(seve%e,rname,'Low resolution cube must have the largest beam')
       error = .true.
    endif
    !
    if (error) return
    conspb =.false.
    call cubetools_consistency_grid('Low resolution',prog%lores%head,&
         'High resolution',prog%hires%head,conspb,error)
    if (error) return
    if (cubetools_consistency_failed(rname,conspb,error)) return
    !
    prog%lofac = user%lofac*hiarea/loarea
    call cubetools_header_get_array_shape(prog%hires%head,n,error)
    if (error) return
    prog%dim(1) = int(n%l,4)
    prog%dim(2) = int(n%m,4)
  end subroutine cubemain_feather_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_feather_prog_header(prog,comm,error)
    use cubetools_header_methods
    use cubeadm_clone
    use cubemain_fft_utils
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(feather_prog_t), intent(inout) :: prog
    type(feather_comm_t),  intent(in)    :: comm
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='FEATHER>PROG>HEADER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_clone_header(comm%feather,prog%hires,prog%feather,error)
    if (error) return
    !  
    call cubetools_header_add_observatories(prog%lores%head,prog%feather%head,error)
    if (error) return
    !
    call prog%hires_weight(error)
    if (error) return
  end subroutine cubemain_feather_prog_header
  !
  subroutine cubemain_feather_prog_hires_weight(prog,error)
    use gkernel_interfaces
    use cubetools_shape_types
    use cubetools_header_methods
    use cubemain_fft_utils
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(feather_prog_t), intent(inout) :: prog
    logical,               intent(inout) :: error
    !
    type(shape_t) :: n
    integer(kind=4) :: ier
    integer(kind=pixe_k) :: ix,iy,icl,icm
    real(kind=coor_k) :: cl,cm,pl,pm,sigx,sigy,acoeff,bcoeff,ccoeff,pang
    real(kind=sign_k), allocatable :: beam(:,:),work(:)
    character(len=*), parameter :: rname='FEATHER>PROG>HIRES>WEIGHT'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubetools_header_get_array_shape(prog%lores%head,n,error)
    if (error) return
    allocate(prog%cweight(n%l,n%m),beam(n%l,n%m),work(2*max(n%l,n%m)),stat=ier)
    if (failed_allocate(rname,'work arrays',ier,error)) return
    !
    icl = n%l/2
    icm = n%m/2
    if (2*icl.ne.n%l) icl = icl+1
    if (2*icm.ne.n%m) icm = icm+1
    cl = prog%lores%head%spa%l%coord(icl)
    cm = prog%lores%head%spa%m%coord(icm)
    if (prog%lobeam%minor.gt.0) then
       sigx = prog%lobeam%major/(sqrt(8.*log(2.)))
       sigy = prog%lobeam%minor/(sqrt(8.*log(2.)))
       ! VVV is this correct, or is there a correction to be applied here?
       pang = prog%lobeam%pang
       !
       acoeff = (cos(pang)**2)/(2*sigx**2)+(sin(pang)**2)/(2*sigy**2)
       bcoeff = (sin(2*pang))/(4*sigx**2)+(sin(2*pang))/(4*sigy**2)
       ccoeff = (sin(pang)**2)/(2*sigx**2)+(cos(pang)**2)/(2*sigy**2)
       !
       do iy=1,n%m
          pm = prog%lores%head%spa%m%coord(iy)-cm
          do ix=1,n%l
             pl = prog%lores%head%spa%l%coord(ix)-cl
             beam(ix,iy) = exp(-(acoeff*pl**2+2*bcoeff*pl*pm+ccoeff*pm**2))
          enddo
       enddo
    else
       sigx = prog%lobeam%major/(sqrt(8.*log(2.)))
       do iy=1,n%m
          pm = prog%lores%head%spa%m%coord(iy)-cm
          do ix=1,n%l
             pl = prog%lores%head%spa%l%coord(ix)-cl
             beam(ix,iy) = exp(-(pl**2+pm**2)/(2*sigx**2))
          enddo
       enddo
    endif
    !
    prog%cweight(:,:) = cmplx(beam(:,:),0.0)
    call fourt(prog%cweight,prog%dim,2,code_dire,code_rdata,work)
    prog%cweight(:,:) = prog%cweight(:,:)/(n%l*n%m)
    prog%cweight(:,:) = 1.0-prog%cweight(:,:)
    !
    deallocate(beam,work)
  end subroutine cubemain_feather_prog_hires_weight
  !
  subroutine cubemain_feather_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(feather_prog_t), intent(inout) :: prog
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='FEATHER>PROG>DATA'
    type(cubeadm_iterator_t) :: iter
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo ! iter
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubemain_feather_prog_data
  !
  subroutine cubemain_feather_prog_loop(prog,iter,error)
    use gkernel_interfaces
    use cubetools_shape_types
    use cubetools_header_methods
    use cubeadm_taskloop
    use cubeadm_image_types
    use cubeadm_visi_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(feather_prog_t),    intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    integer(kind=4) :: ier
    real(kind=sign_k),allocatable :: work(:)
    type(visi_t) :: lofft,hifft
    type(image_t) :: lowres,highres,feathered
    type(shape_t) :: n
    character(len=*), parameter :: rname='FEATHER>PROG>LOOP'
    !
    call cubetools_header_get_array_shape(prog%feather%head,n,error)
    if (error) return
    !
    call highres%associate('highres',prog%hires,iter,error)
    if (error) return
    call lowres%associate('lowres',prog%lores,iter,error)
    if (error) return
    call feathered%allocate('feathered',prog%feather,iter,error)
    if (error) return
    call lofft%reallocate('Low res FFT',lowres%nx,lowres%ny,error)
    if (error) return
    call hifft%reallocate('High res FFT',highres%nx,highres%ny,error)
    if (error) return
    !
    allocate(work(2*max(n%l,n%m)),stat=ier)
    if (failed_allocate(rname,'work array',ier,error)) return
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter%ie,lowres,highres,feathered,work,lofft,hifft,error)
      if (error) return
    enddo ! ie
  end subroutine cubemain_feather_prog_loop
  !
  subroutine cubemain_feather_prog_act(prog,ie,lowres,highres,feathered,work,lofft,hifft,error)
    use cubeadm_image_types
    use cubeadm_visi_types
    use cubemain_fft_utils
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(feather_prog_t), intent(inout) :: prog
    integer(kind=entr_k),  intent(in)    :: ie
    type(image_t),         intent(inout) :: lowres
    type(image_t),         intent(inout) :: highres
    type(image_t),         intent(inout) :: feathered
    real(kind=sign_k),     intent(inout) :: work(:)
    type(visi_t),          intent(inout) :: lofft
    type(visi_t),          intent(inout) :: hifft
    logical,               intent(inout) :: error
    !
    integer(kind=pixe_k) :: ix,nx
    integer(kind=pixe_k) :: iy,ny
    character(len=*), parameter :: rname='FEATHER>PROG>ACT'
    !
    call highres%get(ie,error)
    if (error) return
    call lowres%get(ie,error)
    if (error) return
    !
    nx = highres%nx
    ny = highres%ny
    !
    call cubemain_fft_plunge(nx,ny,highres,nx,ny,hifft,error)
    if (error) return
    call cubemain_fft_plunge(nx,ny,lowres,nx,ny,lofft,error)
    if (error) return
    !
    call fourt(lofft%val,prog%dim,2,code_inve,code_rdata,work)
    call fourt(hifft%val,prog%dim,2,code_inve,code_rdata,work)
    !
    do iy=1,highres%ny
       do ix=1,highres%nx
          hifft%val(ix,iy) = hifft%val(ix,iy)*prog%cweight(ix,iy)+lofft%val(ix,iy)*prog%lofac
       enddo ! ix
    enddo ! iy
    !
    call fourt(hifft%val,prog%dim,2,code_dire,code_cdata,work)
    !
    call cubemain_fft_deplunge(nx,ny,hifft,nx,ny,feathered,error)
    if (error) return
    !
    call feathered%blank_like(highres,error)
    if (error) return
    call feathered%put(ie,error)
    if (error) return
  end subroutine cubemain_feather_prog_act
end module cubemain_feather
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
