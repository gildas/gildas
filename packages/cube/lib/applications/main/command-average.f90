!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_averagec
  use cubetools_parameters
  use cubetools_structure
  use cubeadm_identifier
  use cubeadm_cubeid_types
  use cubemain_messaging
  use cubemain_average_tool
  !
  public :: average
  private
  !
  type :: averagec_comm_t
     type(option_t),  pointer :: comm
     type(option_t),  pointer :: noise
     type(option_t),  pointer :: weight
     type(option_t),  pointer :: index
     type(identifier_opt_t)   :: family
     type(average_comm_t)     :: prod
   contains
     procedure, public  :: register      => cubemain_averagec_register
     procedure, private :: parse         => cubemain_averagec_parse
     procedure, private :: parse_weiopts => cubemain_averagec_parse_weiopts
     procedure, public  :: main          => cubemain_averagec_main
  end type averagec_comm_t
  type(averagec_comm_t) :: average
  !
  type, extends(average_user_t) :: averagec_user_t
    type(cubeid_user_t) :: cubeids
    type(cubeid_user_t) :: noiids
  end type averagec_user_t
  !
contains
  !
  subroutine cubemain_averagec_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(averagec_user_t) :: user
    character(len=*), parameter :: rname='AVERAGE>COMMAND'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call average%parse(line,user,error)
    if (error) return
    call average%main(user,error)
    if (error) continue
  end subroutine cubemain_averagec_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_averagec_register(average,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(averagec_comm_t), intent(inout) :: average
    logical,                intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    type(cubeid_arg_t), pointer :: dummy
    type(standard_arg_t) :: stdarg
    character(len=*), parameter :: comm_abstract = &
         'Average two cubes together'
    character(len=*), parameter :: comm_help = &
         'THIS COMMAND IS FOR DEVELOPPERS ONLY. USE STITCH INSTEAD.'&
         //strg_cr//strg_cr//&
         &'Average two cubes together with different weighting&
         & schemes. By default both cubes are assumed to have the&
         & same weight. This can be changed by using options /NOISE&
         & and /WEIGHT. These two options can be combined and the&
         & resulting weight will be w_i/(noise_i)**2. By default the&
         & resulting cube will have the same family name as cube1.&
         & This can be changed by using option /FAMILY.'
    character(len=*), parameter :: rname='AVERAGE>REGISTER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    ! Command
    call cubetools_register_command(&
         'AVERAGE','[cube1 cube2]',&
         comm_abstract,&
         comm_help,&
         cubemain_averagec_command,&
         average%comm,error)
    if (error) return
    call cubearg%register(&
         'CUBE1',&
         'Input cube #1',  &
         strg_id,&
         code_arg_optional,  &
         [flag_cube],&
         code_read,&
         code_access_imaset,&
         dummy,&  ! Pointer not saved, will use average%comm%arg%list(:)
         error)
    if (error) return
    call cubearg%register(&
         'CUBE2',&
         'Input cube #2',&
         strg_id,&
         code_arg_optional,&
         [flag_cube],&
         code_read,&
         code_access_imaset,&
         dummy,&  ! Pointer not saved, will use average%comm%arg%list(:)
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'WEIGHT','w1 w2',&
         'Define explicit real weights',&
         strg_id,&
         average%weight,error)
    if (error) return
    call stdarg%register(&
         'w1',&
         'Weight for Input cube #1',&
         strg_id,&
         code_arg_optional,&
         error)
    if (error) return
    call stdarg%register(&
         'w2',&
         'Weight for Input cube #2',&
         strg_id,&
         code_arg_optional,&
         error)
    if (error) return
    !
    call average%family%register(&
         'Define the new family name for products',&
         .not.changeflags,error)
    if (error) return
    !
    call cubetools_register_option(&
         'NOISE','noise1 noise2',&
         'Cubes are weighted by noise',&
         'The weight is computed from the noise images as: 1/(noise&
         &**2). Currently only single noise per spectrum is supported',&
         average%noise,error)
    if (error) return
    call cubearg%register(&
         'NOISE1',&
         'Noise for Input cube #1',  &
         strg_id,&
         code_arg_optional,  &
         [flag_noise],&
         code_read,&
         code_access_imaset,&
         dummy,&  ! Pointer not saved, will use average%noise%arg%list(:)
         error)
    if (error) return
    call cubearg%register(&
         'NOISE2',&
         'Noise for Input cube #2',&
         strg_id,&
         code_arg_optional,&
         [flag_noise],&
         code_read,&
         code_access_imaset,&
         dummy,&  ! Pointer not saved, will use average%noise%arg%list(:)
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'INDEX','',&
         'Average current index',&
         'Instead of giving two cubes explicitly to the command, use the &
         &current index (FIND command) as an input list (not limited to two &
         &cubes).' &
         //strg_cr//strg_cr//&
         'If /WEIGHT without argument (respectively /NOISE) is present, the &
         &current index is split in 2 subsets: one with the cubes with a &
         &"weight" flag (resp. "noise") assumed to be weight cubes (resp. noise &
         &cubes), the other one with the remaining cubes (assumed to be data &
         &cubes). The weights (resp. noise) and data cubes are then paired two &
         &by two according to their family name, and all the data cubes are &
         &then averaged with their proper weight (resp. noise).',&
         average%index,error)
    if (error) return
    !
    ! Products
    call average%prod%register(flag_average,error)
    if (error)  return
  end subroutine cubemain_averagec_register
  !
  subroutine cubemain_averagec_parse(average,line,user,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(averagec_comm_t), intent(in)    :: average
    character(len=*),       intent(in)    :: line
    type(averagec_user_t),  intent(out)   :: user
    logical,                intent(inout) :: error
    !
    logical :: docurrent
    character(len=*), parameter :: rname='AVERAGE>PARSE'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    ! Which index of cubes?
    call average%index%present(line,docurrent,error)
    if (error) return
    if (docurrent) then  ! From CX
       user%data_mode = code_user_index_cx
    else  ! From command line: build a custom index
       user%data_mode = code_user_index_custom
       if (average%comm%getnarg().ne.2) then
         call cubemain_message(seve%e,rname,  &
           'In non-/INDEX mode, command takes exactly 2 arguments')
         error = .true.
         return
       endif
       call cubeadm_cubeid_parse(line,average%comm,user%cubeids,error)
       if (error) return
       call user%datind%get_from_cubeid(average%comm,user%cubeids,error)
       if (error) return
    endif
    ! Weights or noise
    call average%parse_weiopts(line,user,error)
    if (error) return
    ! Misc
    call average%family%parse(line,user%family,error)
    if (error) return
  end subroutine cubemain_averagec_parse
  !
  subroutine cubemain_averagec_parse_weiopts(average,line,user,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(averagec_comm_t), intent(in)    :: average
    character(len=*),       intent(in)    :: line
    type(averagec_user_t),  intent(inout) :: user
    logical,                intent(inout) :: error
    !
    integer(kind=4) :: iw
    logical :: docx,doweight,donoise
    character(len=*), parameter :: rname='AVERAGE>PARSE>WEIOPTS'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call average%index%present(line,docx,error)
    if (error) return
    call average%weight%present(line,doweight,error)
    if (error) return
    call average%noise%present(line,donoise,error)
    if (error) return
    !
    if (docx) then
      ! List of cubes from current index
      if (doweight) then
        ! CX provides both data and weight cubes
        user%weight_mode = code_user_weight_datweiind
      elseif (donoise) then
        ! CX provides both data and noise cubes
        user%weight_mode = code_user_weight_datnoiind
      else
        ! CX provides only data cubes
        user%weight_mode = code_user_weight_none
      endif
    else
      ! List of cubes from command line
      if (doweight.and.donoise) then
         ! Combine weight values and noise cubes
         user%weight_mode = code_user_weight_weival_noiind
      elseif (doweight) then
         ! Weight values only
         user%weight_mode = code_user_weight_weival
      elseif (donoise) then
         ! Noise cubes only
         user%weight_mode = code_user_weight_noiind
      else
         ! No relative weight
         user%weight_mode = code_user_weight_none
      endif
    endif
    !
    ! Option arguments
    if (docx) then
      ! No extra arguments allowed
      if (average%weight%getnarg().ge.1) then
        call cubemain_message(seve%e,rname,'/WEIGHT takes no argument in /INDEX mode')
        error = .true.
        return
      endif
      if (average%noise%getnarg().ge.1) then
        call cubemain_message(seve%e,rname,'/NOISE takes no argument in /INDEX mode')
        error = .true.
        return
      endif
    else
      if (doweight) then
         do iw=1,2
            call cubetools_getarg(line,average%weight,iw,user%weival(iw),mandatory,error)
            if (error) return
            if (user%weival(iw).le.0) then
               call cubemain_message(seve%e,rname,'Weights must be positive')
               error = .true.
               return
            endif
         enddo
      endif
      if (donoise) then
         call cubeadm_cubeid_parse(line,average%noise,user%noiids,error)
         if (error) return
         call user%noiind%get_from_cubeid(average%noise,user%noiids,error)
         if (error) return
      endif
    endif
  end subroutine cubemain_averagec_parse_weiopts
  !
  subroutine cubemain_averagec_main(average,user,error)
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(averagec_comm_t), intent(inout) :: average  ! INOUT for the patch
    type(averagec_user_t),  intent(inout) :: user
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='AVERAGE>MAIN'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    ! ZZZ Miss specific parsing of /NOISE cubes on command line
    !
    call average%prod%main(user%average_user_t,error)
    if (error)  return
  end subroutine cubemain_averagec_main
end module cubemain_averagec
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
