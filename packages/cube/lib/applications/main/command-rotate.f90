!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_rotate
  use cubetools_structure
  use cubetools_spatial_types
  use cubemain_messaging
  use cubemain_reproject
  !
  public :: rotate
  private
  !
  type, extends(reproject_comm_t) :: rotate_comm_t
     ! No difference (only a subset actually used here)
  contains
     ! Important: overload these 2
     procedure, public  :: register => cubemain_rotate_register
     procedure, private :: parse    => cubemain_rotate_parse
  end type rotate_comm_t
  type(rotate_comm_t) :: rotate
  !
contains
  !
  subroutine cubemain_rotate_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(reproject_user_t) :: user
    character(len=*), parameter :: rname='ROTATE>COMMAND'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call rotate%parse(line,user,error)
    if (error) return
    call rotate%main(user,error)
    if (error) continue
  end subroutine cubemain_rotate_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_rotate_register(comm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(rotate_comm_t), intent(inout) :: comm
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: comm_abstract='Rotate a cube around a center'
    character(len=*), parameter :: comm_help=strg_id
    character(len=*), parameter :: rname='ROTATE>REGISTER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call comm%comm%register(&
         'ROTATE',&
         comm_abstract,&
         comm_help,&
         cubemain_rotate_command,&
         'Rotated', &
         flag_rotate,&
         error)
    if (error)  return
    call comm%pcenter%register(&
         'CENTER',&
         'Define the center about which to rotate',&
         error)
    if (error) return
    call comm%pangle%register(&
         'ANGLE',&
         'Define the new projection angle',&
         error)
    if (error) return
  end subroutine cubemain_rotate_register
  !
  subroutine cubemain_rotate_parse(comm,line,user,error)
    use cubeadm_cubeid_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(rotate_comm_t),   intent(in)    :: comm
    character(len=*),       intent(in)    :: line
    type(reproject_user_t), intent(out)   :: user
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='ROTATE>PARSE'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call user%init(error)
    if (error) return
    call comm%comm%parse(line,user%comm,error)
    if (error) return
    call comm%pcenter%parse(line,user%pcenter,error)
    if (error) return
    call comm%pangle%parse(line,user%pangle,error)
    if (error) return 
  end subroutine cubemain_rotate_parse
end module cubemain_rotate
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
