module cubemain_pang
  use cubetools_parameters
  use cubemain_messaging
  !
  type pang_user_t
     character(len=argu_l) :: val = strg_unk
     character(len=argu_l) :: unit = strg_unk
  end type pang_user_t
  !
  type pang_t
     real(kind=coor_k) :: val = 0d0
  end type pang_t
  !
contains
  !
  subroutine cubemain_pang_register(name,abstract,option,error)
    use cubetools_structure
    !----------------------------------------------------------------------
    ! Register a /PANG option according to a name and abstract
    ! provided by the command
    ! ----------------------------------------------------------------------
    character(len=*),        intent(in)    :: name
    character(len=*),        intent(in)    :: abstract
    type(option_t), pointer, intent(out)   :: option
    logical,                 intent(inout) :: error
    !
    type(standard_arg_t) :: stdarg
    !
    character(len=*), parameter :: rname='PANG>REGISTER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         name,'angle [unit]',&
         abstract,&
         strg_id,&
         option,error)
    if (error) return
    call stdarg%register( &
         'angle',&
         'Rotation angle', &
         '"*" or "=" mean previous value is kept',&
         code_arg_mandatory, &
         error)
    if (error) return
    call stdarg%register( &
         'unit',  &
         'Position unit if relative', &
         '"*" or "=" mean previous value is kept',&
         code_arg_optional, &
         error)
    if (error) return
  end subroutine cubemain_pang_register
  !
  subroutine cubemain_pang_parse(line,opt,user,dopang,error)
    use cubetools_structure
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*),        intent(in)    :: line   ! Input user line
    type(option_t),          intent(in)    :: opt    ! Command option
    type(pang_user_t),       intent(out)   :: user   ! Parsed user input
    logical,                 intent(out)   :: dopang ! Option was present
    logical,                 intent(inout) :: error
    !
    character(len=*), parameter :: rname='PANG>PARSE'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    user%val  = strg_star
    user%unit = strg_star
    !
    call opt%present(line,dopang,error)
    if (error) return
    if (dopang) then
       call cubetools_getarg(line,opt,1,user%val,mandatory,error)
       if (error) return
       call cubetools_getarg(line,opt,2,user%unit,.not.mandatory,error)
       if (error) return
    endif
  end subroutine cubemain_pang_parse
  !
  subroutine cubemain_pang_user2prog(cube,user,prog,error)
    use cubetools_unit
    use cube_types
    use cubetools_user2prog
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(cube_t),      intent(in)    :: cube
    type(pang_user_t), intent(in)    :: user
    type(pang_t),      intent(out)   :: prog
    logical,           intent(inout) :: error
    !
    type(unit_user_t) :: unit
    character(len=*), parameter :: rname='PANG>PARSE'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call unit%get_from_name_for_code(user%unit,code_unit_pang,error)
    if (error) return
    !
    call cubetools_user2prog_resolve_star(user%val,unit,cube%head%spa%pro%pa,prog%val,error)
    if (error) return
  end subroutine cubemain_pang_user2prog
end module cubemain_pang
