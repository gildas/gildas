!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This module is obsolescent. Please adapt your codes to use the more
! standard cubetopology_spaelli_types module.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_spaelli_types
  use cubetools_parameters
  use cubetools_structure
  use cubetools_unit_arg
  use cube_types
  use cubemain_messaging
  !
  public :: ellipse_opt_t,ellipse_user_t,ellipse_prog_t
  private
  !
  type ellipse_opt_t
     type(option_t),   pointer :: opt
     type(unit_arg_t), pointer :: unit
     type(unit_arg_t), pointer :: paunit
   contains
     procedure :: register => cubemain_spaelli_register
     procedure :: parse    => cubemain_spaelli_parse
  end type ellipse_opt_t
  !
  type ellipse_user_t
     character(len=argu_l) :: major  ! In user unit
     character(len=argu_l) :: minor  ! In user unit
     character(len=argu_l) :: unit   ! User unit for major and minor
     character(len=argu_l) :: pang   ! In user unit
     character(len=argu_l) :: paunit ! User unit for pang
     logical               :: do
   contains
     procedure :: toprog => cubemain_spaelli_user_toprog
  end type ellipse_user_t
  !
  type ellipse_prog_t
     real(kind=coor_k) :: major ! [rad]
     real(kind=coor_k) :: minor ! [rad] 
     real(kind=coor_k) :: pang  ! [rad]
     type(cube_t), pointer :: cube => null() ! Pointer to cube to which ellipse is part of
   contains
     procedure :: list   => cubemain_spaelli_prog_list
  end type ellipse_prog_t
  !
contains
  !
  subroutine cubemain_spaelli_register(option,name,abstract,error)
    use cubetools_unit
    !----------------------------------------------------------------------
    ! Register an ellipse kind option
    !----------------------------------------------------------------------
    class(ellipse_opt_t), intent(out)   :: option
    character(len=*),     intent(in)    :: name
    character(len=*),     intent(in)    :: abstract
    logical,              intent(inout) :: error
    !
    type(standard_arg_t) :: stdarg
    type(unit_arg_t) :: unitarg
    character(len=*), parameter :: rname='SPAELLI>REGISTER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         name,'major [minor [unit [pang [paunit]]]]',&
         abstract,&
         'If major or minor axis are *, the current beamsize is&
         & assumed.If minor axis is omited it is assumed to be equal&
         & to the major axis, i.e. a circle. If pang is&
         & omited an angle of 0 degrees from north is&
         & assumed.', option%opt,error)
    if (error) return
    call stdarg%register( &
         'major',  &
         'Major axis', &
         strg_id,&
         code_arg_mandatory, &
         error)
    if (error) return
    call stdarg%register( &
         'minor',  &
         'Minor axis', &
         strg_id,&
         code_arg_optional, &
         error)
    if (error) return
    call unitarg%register( &
         'unit', &
         'Axes unit',&
         strg_id,&
         code_arg_optional,&
         code_unit_fov,&
         option%unit,error)
    if (error) return
    call stdarg%register( &
         'pang',  &
         'Position angle', &
         strg_id,&
         code_arg_optional, &
         error)
    if (error) return
    call unitarg%register( &
         'paunit', &
         'pang unit',&
         strg_id,&
         code_arg_optional,&
         code_unit_pang,&
         option%paunit,error)
    if (error) return
  end subroutine cubemain_spaelli_register
  !
  subroutine cubemain_spaelli_parse(option,line,user,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(ellipse_opt_t), intent(in)    :: option
    character(len=*),     intent(in)    :: line
    type(ellipse_user_t), intent(out)   :: user
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPAELLI>PARSE'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    user%major  = strg_star
    user%minor  = strg_star
    user%unit   = strg_star
    user%pang   = strg_star
    user%paunit = strg_star
    !
    call option%opt%present(line,user%do,error)
    if (error) return
    !
    if (user%do) then
       call cubetools_getarg(line,option%opt,1,user%major,mandatory,error)
       if (error) return
       user%minor = user%major
       call cubetools_getarg(line,option%opt,2,user%minor,.not.mandatory,error)
       if (error) return
       call cubetools_getarg(line,option%opt,3,user%unit,.not.mandatory,error)
       if (error) return
       call cubetools_getarg(line,option%opt,4,user%pang,.not.mandatory,error)
       if (error) return
       call cubetools_getarg(line,option%opt,5,user%paunit,.not.mandatory,error)
       if (error) return
    endif
  end subroutine cubemain_spaelli_parse
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_spaelli_user_toprog(user,cube,prog,error)
    use cubetools_unit
    use cubetools_user2prog
    use cubetools_header_methods
    use cubetools_beam_types
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    class(ellipse_user_t), intent(in)    :: user
    type(cube_t), target,  intent(in)    :: cube
    type(ellipse_prog_t),  intent(out)   :: prog
    logical,               intent(inout) :: error
    !
    type(unit_user_t) :: fovunit,paunit
    real(kind=coor_k) :: defmajor,defminor,defangle
    type(beam_t) :: beam
    character(len=*), parameter :: rname='SPAELLI>USER>TOPROG'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    prog%cube => cube
    !
    call cubetools_header_get_spabeam(cube%head,beam,error)
    if (error) return
    defangle = 0d0
    defmajor = beam%major
    defminor = beam%minor
    call fovunit%get_from_name_for_code(user%unit,code_unit_fov,error)
    if (error) return
    call paunit%get_from_name_for_code(user%paunit,code_unit_pang,error)
    if (error) return
    call cubetools_user2prog_resolve_star(user%major,fovunit,defmajor,prog%major,error)
    if (error) return
    call cubetools_user2prog_resolve_star(user%minor,fovunit,defminor,prog%minor,error)
    if (error) return
    call cubetools_user2prog_resolve_star(user%pang,paunit,defangle,prog%pang,error)
    if (error) return
  end subroutine cubemain_spaelli_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_spaelli_prog_list(prog,error)
    use cubetools_unit
    use cubetools_format
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(ellipse_prog_t), intent(in)    :: prog
    logical,               intent(inout) :: error
    !
    character(len=mess_l) :: mess
    type(unit_user_t) :: axesunit,pangunit
    character(len=*), parameter :: rname='SPAELLI>PROG>LIST'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call axesunit%get_from_code(code_unit_fov,error)
    if (error) return
    call pangunit%get_from_code(code_unit_pang,error)
    if (error) return
    mess = cubetools_format_stdkey_boldval('Major',prog%major*axesunit%user_per_prog,'f8.3',22)
    mess = trim(mess)//'  '//cubetools_format_stdkey_boldval('Minor',prog%minor*axesunit%user_per_prog,'f8.3',22)
    mess = trim(mess)//'  '//cubetools_format_stdkey_boldval('PA',prog%pang*pangunit%user_per_prog,'f8.3',10)
    call cubemain_message(seve%r,rname,mess)
  end subroutine cubemain_spaelli_prog_list
end module cubemain_spaelli_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
