!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_baseline_median
  use cubetools_parameters
  use cubetools_structure
  use cubetools_unit_arg
  use cubemain_messaging
  use cubemain_baseline_cubes_types
  use cubemain_lineset_or_mask_types
  !
  public :: baseline_median_comm_t,baseline_median_user_t
  private
  !
  type baseline_median_comm_t
     type(option_t),   pointer :: key
     type(unit_arg_t), pointer :: unit ! ***JP: Provisional => unused for the moment
     type(lineset_or_mask_comm_t), pointer :: lineregion => null()
     type(baseline_cubes_comm_t),  pointer :: cubes => null()
   contains
     procedure, public :: register     => cubemain_baseline_median_comm_register
     procedure, public :: parse_key    => cubemain_baseline_median_comm_parse_key
     procedure, public :: parse_others => cubemain_baseline_median_comm_parse_others
     procedure, public :: main         => cubemain_baseline_median_comm_main
  end type baseline_median_comm_t
  !
  type baseline_median_user_t
     logical               :: present = .false.
     character(len=argu_l) :: width = strg_star
     character(len=argu_l) :: sampling = strg_star
     character(len=unit_l) :: unit = strg_star ! ***JP: Provisional => unused for the moment
     type(lineset_or_mask_user_t) :: lineregion
     type(baseline_cubes_user_t)  :: cubes
   contains
     procedure, public :: toprog => cubemain_baseline_median_user_toprog
     procedure, public :: list   => cubemain_baseline_median_user_list
  end type baseline_median_user_t
  !
  type baseline_median_prog_t
     integer(kind=chan_k) :: nmedian   = 0  ! [chan] Number of computed median values
     integer(kind=chan_k) :: nwidth    = 0  ! [chan] Width of the running window
     integer(kind=chan_k) :: nsampling = 0  ! [chan] Sampling of the computation
     type(lineset_or_mask_prog_t) :: lineregion !
     type(baseline_cubes_prog_t)  :: cubes      ! Input and output cubes
   contains
     procedure :: list   => cubemain_baseline_median_prog_list
     procedure :: header => cubemain_baseline_median_prog_header
     procedure :: data   => cubemain_baseline_median_prog_data
     procedure :: loop   => cubemain_baseline_median_prog_loop
     procedure :: act    => cubemain_baseline_median_prog_act
  end type baseline_median_prog_t
  !
contains
  !
  subroutine cubemain_baseline_median_comm_register(comm,error)
    use cubetools_unit
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(baseline_median_comm_t), intent(inout) :: comm
    logical,                       intent(inout) :: error
    !
!    type(unit_arg_t) :: unitarg ! ***JP: Provisional => unused for the moment
    type(standard_arg_t) :: stdarg
    character(len=*), parameter :: rname='BASELINE>MEDIAN>REGISTER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         'MEDIAN','[width [sampling]]',&
         'Use a median running filter to define the baseline',&
         'The median and associated median absolute deviation are computed&
         & in windows of the given width, sampled every sampling space.&
         & Intermediate values are then linearly interpolated so that the&
         & final cubes have the same number of channels as the input cube.&
         & Flat values are used (no extrapolation) For the first and last&
         & half windows (boundary conditions). When the input channels are&
         & blanked, the resulting channels are also blanked. Blank channels&
         & do not contribute to the surrounding windows.',&
         comm%key,&
         error)
    if (error) return
    call stdarg%register(&
         'width',&
         'Running filter width',&
         'In MHz. Default to 20 MHz.',&
         code_arg_optional,&
         error)
    if (error) return
    call stdarg%register(&
         'sampling',&
         'Running filter sampling',&
         'Default to width/2',&
         code_arg_optional,&
         error)
    if (error) return
  end subroutine cubemain_baseline_median_comm_register
  !
  subroutine cubemain_baseline_median_comm_parse_key(comm,line,user,error)
    use cubetools_structure
    !----------------------------------------------------------------------
    ! /MEDIAN [width [sampling]].
    !----------------------------------------------------------------------
    class(baseline_median_comm_t), intent(inout) :: comm
    character(len=*),              intent(in)    :: line
    type(baseline_median_user_t),  intent(out)   :: user
    logical,                       intent(inout) :: error
    !
    integer(kind=argu_k), parameter :: iwidth=1
    integer(kind=argu_k), parameter :: isampling=2
    character(len=*), parameter :: rname='BASELINE>MEDIAN>COMM>PARSE>KEY'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call comm%key%present(line,user%present,error)
    if (error) return
    if (user%present) then
       call cubetools_getarg(line,comm%key,iwidth,user%width,.not.mandatory,error)
       if (error) return
       call cubetools_getarg(line,comm%key,isampling,user%sampling,.not.mandatory,error)
       if (error) return
    endif
  end subroutine cubemain_baseline_median_comm_parse_key
  !
  subroutine cubemain_baseline_median_comm_parse_others(comm,cubes,lineregion,line,user,error)
    !----------------------------------------------------------------------
    ! Parse the others useful keys of the commands in this case
    !----------------------------------------------------------------------
    class(baseline_median_comm_t),        intent(inout) :: comm
    type(baseline_cubes_comm_t),  target, intent(inout) :: cubes
    type(lineset_or_mask_comm_t), target, intent(inout) :: lineregion
    character(len=*),                     intent(in)    :: line
    type(baseline_median_user_t),         intent(inout) :: user
    logical,                              intent(inout) :: error
    !
    character(len=*), parameter :: rname='BASELINE>MEDIAN>COMM>PARSE>OTHERS'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    comm%cubes => cubes
    call comm%cubes%parse(line,user%cubes,error)
    if (error) return
    !
    comm%lineregion => lineregion
    call comm%lineregion%parse(line,user%lineregion,error)
    if (error) return
  end subroutine cubemain_baseline_median_comm_parse_others
  !
  subroutine cubemain_baseline_median_comm_main(comm,user,error) 
    use cubeadm_timing
    !----------------------------------------------------------------------
    ! Parsing call is done in command_baseline.f90. Reminder calls are done
    ! here!
    !----------------------------------------------------------------------
    class(baseline_median_comm_t), intent(in)    :: comm
    type(baseline_median_user_t),  intent(inout) :: user
    logical,                       intent(inout) :: error
    !
    type(baseline_median_prog_t) :: prog
    character(len=*), parameter :: rname='BASELINE>MEDIAN>COMM>MAIN'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%list(error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubemain_baseline_median_comm_main
  !
  !------------------------------------------------------------------------
  !
  subroutine cubemain_baseline_median_user_toprog(user,comm,prog,error)
    use cubetools_unit
    use cubetools_user2prog
    use cubetools_axis_types
    use cubetools_header_methods
    !----------------------------------------------------------------------
    ! ***JP: Some work is needed to accept any unit
    !----------------------------------------------------------------------
    class(baseline_median_user_t), intent(inout) :: user
    type(baseline_median_comm_t),  intent(in)    :: comm
    type(baseline_median_prog_t),  intent(inout) :: prog
    logical,                       intent(inout) :: error
    !
    type(axis_t) :: axis
    type(unit_user_t) :: nounit
    real(kind=coor_k) :: default,value
    character(len=*), parameter :: rname='BASELINE>MEDIAN>USER>TOPROG'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    ! Get input and output cubes
    call user%cubes%toprog(comm%cubes,prog%cubes,error)
    if (error) return
    !
    ! 0 Preparatory work
    ! 0.1 Get frequency axis header
    call cubetools_header_get_axis_head_f(prog%cubes%cube%head,axis,error)
    if (error) return
    ! 0.2 Sanity check
    if (axis%inc.eq.0) then
       call cubemain_message(seve%e,rname,'Cube frequency increment is zero valued')
       error = .true.
       return
    endif
    ! 0.3 Unit
    call nounit%get_from_code(code_unit_unk,error)
    if (error) return
    !
    ! 1 Width
    ! 1.a Get user value and convert it to channels
    default = 20.d0 ! MHz
    call cubetools_user2prog_resolve_star(user%width,nounit,default,value,error)
    if (error) return
    prog%nwidth = nint(abs(value/axis%inc))
    ! 1.b Ensure that prog%nwidth will be odd because the median computation is simpler/faster
    if (mod(prog%nwidth,2).eq.0) then
       if (prog%nwidth.eq.axis%n) then
          prog%nwidth = prog%nwidth-1
       else
          prog%nwidth = prog%nwidth+1
       endif
    endif
    ! 1.c Ensure that we fall inside the spectral axis
    prog%nwidth = max(min(prog%nwidth,axis%n),1)
    !
    ! 2 Sampling
    ! 2.a Get user value and convert it to channels
    default = 0.5d0*value
    call cubetools_user2prog_resolve_star(user%sampling,nounit,default,value,error)
    if (error) return
    prog%nsampling = nint(abs(value/axis%inc))
    ! 2.b Ensure that we fall inside the spectral axis
    prog%nsampling = min(max(prog%nsampling,1),axis%n)
    !
    ! 3 Compute the associated number of median values
    prog%nmedian = floor(dble(axis%n)/dble(prog%nsampling))
    if (prog%nsampling*prog%nmedian.lt.axis%n) then
       prog%nmedian = prog%nmedian+1
    endif
    !
    ! 5 Define the regions where the lines are
    call user%lineregion%toprog(prog%cubes%cube,comm%lineregion,prog%lineregion,error)
    if (error) return
  end subroutine cubemain_baseline_median_user_toprog
  !
  subroutine cubemain_baseline_median_user_list(user,error)
    !----------------------------------------------------------------------
    ! Mostly for debugging purpose
    !----------------------------------------------------------------------
    class(baseline_median_user_t), intent(in)    :: user 
    logical,                       intent(inout) :: error
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='BASELINE>MEDIAN>USER>LIST'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    ! *** JP: To be written
    call cubemain_message(seve%r,rname,mess)
  end subroutine cubemain_baseline_median_user_list
  !
  !------------------------------------------------------------------------
  !
  subroutine cubemain_baseline_median_prog_list(prog,error)
    use cubetools_format
    !-------------------------------------------------------------------
    ! List the baseline_median information in a user friendly way
    !-------------------------------------------------------------------
    class(baseline_median_prog_t), intent(in)    :: prog
    logical,                       intent(inout) :: error
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='BASELINE>MEDIAN>PROG>LIST'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubemain_message(seve%r,rname,'')
    mess = cubetools_format_stdkey_boldval('Baselining method','Median Filtering',40)
    call cubemain_message(seve%r,rname,mess)
    write(mess,'(a,i0,a,i0,a)') 'Computing the median of ',prog%nwidth,' contiguous channels, every ',prog%nsampling,' channels'
    call cubemain_message(seve%r,rname,mess)
    !
    call prog%lineregion%list(error)
    if (error) return
    !
    call prog%cubes%list(error)
    if (error) return
  end subroutine cubemain_baseline_median_prog_list
  !
  subroutine cubemain_baseline_median_prog_header(prog,comm,error)
    !-------------------------------------------------------------------
    ! Update cube headers
    !-------------------------------------------------------------------
    class(baseline_median_prog_t), intent(inout) :: prog
    type(baseline_median_comm_t),  intent(in)    :: comm
    logical,                       intent(inout) :: error
    !
    character(len=*), parameter :: rname='BASELINE>MEDIAN>PROG>HEADER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call prog%cubes%header(comm%cubes,error)
    if (error) return
  end subroutine cubemain_baseline_median_prog_header
  !
  subroutine cubemain_baseline_median_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(baseline_median_prog_t), intent(inout) :: prog
    logical,                       intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='BASELINE>MEDIAN>PROG>DATA'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo ! iter
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubemain_baseline_median_prog_data
  !
  subroutine cubemain_baseline_median_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_spectrum_types
    use cubemain_interpolate_spectrum_tool
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(baseline_median_prog_t), intent(inout) :: prog
    type(cubeadm_iterator_t),      intent(inout) :: iter
    logical,                       intent(inout) :: error
    !
    type(spectrum_t) :: input,good,base,line
    type(spectrum_t) :: median
    type(interpolate_spectrum_prog_t) :: interp
    character(len=*), parameter :: rname='BASELINE>MEDIAN>PROG>LOOP'
    !
    ! 1 Prepare spectra
    call input%associate('input',prog%cubes%cube,iter,error)
    if (error) return
    call good%allocate('good',prog%cubes%cube,iter,error)
    if (error) return
    call line%allocate('line',prog%cubes%line,iter,error)
    if (error) return
    call base%allocate('base',prog%cubes%base,iter,error)
    if (error) return
    call median%allocate_y('median',prog%nmedian,error)
    if (error) return
    !
    ! 2. Prepare interpolation
    ! 2.1 Get header information for base
    call base%get_header(error)
    if (error) return
    ! 2.2 Update median header information
    !     Use left edge of first channel to compute new ref channel
    median%ref = 0.5d0-(0.5d0-base%ref)/prog%nsampling
    median%val = base%val
    median%inc = prog%nsampling*base%inc
    ! 2.3 Initialize interpolation
    call interp%init(median,base,error)
    if (error) return
    !
    ! Process lineregion method
    select case (prog%lineregion%method)
    case (code_line_none)
       do while (iter%iterate_entry(error))
          call prog%act(iter%ie,input,good,base,line,median,interp,error)
          if (error) return
       enddo ! ie
    case (code_line_set)
       call cubemain_message(seve%w,rname,'Not yet implemented')
       error = .true.
       return
       ! ***JP: This case is not trivial! The difficulty is that we assume a
       ! ***JP: given number of channels to compute the median. When there is
       ! ***JP: blanking or masking, this numbers varies from one position to
       ! ***JP: the other one. It becomes a major problem when all the channels
       ! ***JP: in the current median computation are blanked or masked.
       ! ***JP: In addition, this messes up the interpolation algorithm that
       ! ***JP: assumes a regular axis to be fast. When it's not the case,
       ! ***JP: there should be another mechanism to interpolate the solution.
!!$       do while (iter%iterate_entry(error))
!!$          call prog%act(iter%ie,input,good,base,line,median,interp,error)
!!$          if (error) return
!!$       enddo ! ie
    case (code_line_mask)
       call cubemain_message(seve%w,rname,'Not yet implemented')
       error = .true.
       return
       ! ***JP: This case is even less trivial (see above)!
!!$       call mask%associate('mask',prog%lineregion%mask%cube,iter,error)
!!$       if (error) return
!!$       do while (iter%iterate_entry(error))
!!$          call prog%act(iter%ie,input,good,base,line,median,interp,error)
!!$          if (error) return
!!$       enddo ! ie
    case default
       call cubemain_message(seve%e,rname,'Unknown line method code')
       error = .true.
       return
    end select
  end subroutine cubemain_baseline_median_prog_loop
  !
  subroutine cubemain_baseline_median_prog_act(prog,ie,input,good,base,line,median,interp,error)
    use cubeadm_spectrum_types
    use cubemain_interpolate_spectrum_tool
    use cubemain_statistics_tool
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(baseline_median_prog_t),     intent(inout) :: prog
    integer(kind=entr_k),              intent(in)    :: ie
    type(spectrum_t),                  intent(inout) :: input
    type(spectrum_t),                  intent(inout) :: good
    type(spectrum_t),                  intent(inout) :: base
    type(spectrum_t),                  intent(inout) :: line
    type(spectrum_t),                  intent(inout) :: median
    type(interpolate_spectrum_prog_t), intent(in)    :: interp
    logical,                           intent(inout) :: error
    !
    integer(chan_k) :: im,ic,first,last
    type(spectrum_t)  :: extracted
    character(len=*), parameter :: rname='BASELINE>MEDIAN>PROG>ACT'
    !
    call input%get(ie,error)
    if (error) return
    ! Compute the median
    first = 1
    do im=1,median%n
       ! The following ensures that
       !    1) we never get past the number of channels
       !    2) we always compute the mad on nwidth contiguous samples
       last  = min(first+prog%nwidth-1,input%n)
       first = last-prog%nwidth+1
       call extracted%point_to(input,first,last,1.0,error)
       if (error) return
       call good%unblank(extracted,error)
       if (error) return
       median%y%val(im) = statistics%median(good%y%val,good%n)
!!$       noise%y%val(im)  = statistics%mad(good%y%val,good%n,median)
       first = first+prog%nsampling
    enddo ! im
    ! Interpolate the median as the baseline
    call interp%spectrum(median,base,error)
    if (error) return
    ! Deduce the line by subtraction
    do ic=1,input%n
       ! ***JP: We still need to handle the original NaN.
       ! ***JP: The right behavior is unclear to me in this case.
       line%y%val(ic) = input%y%val(ic)-base%y%val(ic)
    enddo ! ic
    ! Write
    call base%put(ie,error)
    if (error) return
    call line%put(ie,error)
    if (error) return
  end subroutine cubemain_baseline_median_prog_act
end module cubemain_baseline_median
! 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
