!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_lineset_or_mask_types
  use cubetools_parameters
  use cubetopology_sperange_set_types
  use cubemain_messaging
  use cubemain_ancillary_mask_types
  !
  public :: lineset_or_mask_comm_t,lineset_or_mask_user_t,lineset_or_mask_prog_t
  public :: code_line_none,code_line_set,code_line_mask
  private
  !
  integer(kind=code_k), parameter :: code_line_none = 1
  integer(kind=code_k), parameter :: code_line_set  = 2
  integer(kind=code_k), parameter :: code_line_mask = 3
  !
  type :: lineset_or_mask_comm_t
     type(sperange_set_comm_t)   :: lineset
     type(ancillary_mask_comm_t) :: mask
   contains
     procedure, public  :: register => cubemain_lineset_or_mask_comm_register
     procedure, public :: parse    => cubemain_lineset_or_mask_comm_parse
  end type lineset_or_mask_comm_t
  !
  type lineset_or_mask_user_t
     type(sperange_set_user_t)   :: lineset
     type(ancillary_mask_user_t) :: mask
   contains
     procedure, public :: toprog => cubemain_lineset_or_mask_user_toprog
  end type lineset_or_mask_user_t
  !
  type lineset_or_mask_prog_t
     integer(kind=code_k)        :: method  ! Method to define line regions to be ignored during baselining
     type(sperange_set_prog_t)   :: lineset ! Set of line ranges
     type(ancillary_mask_prog_t) :: mask    ! Input mask of regions
   contains
     procedure, public :: list   => cubemain_lineset_or_mask_prog_list
  end type lineset_or_mask_prog_t
  !
contains
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_lineset_or_mask_comm_register(comm,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(lineset_or_mask_comm_t), intent(inout) :: comm
    logical,                       intent(inout) :: error
    !
    character(len=*), parameter :: rname='LINESET>OR>MASK>COMM>REGISTER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    ! Syntax
    call comm%lineset%register(&
         'LINE',&
         'Define global line regions to be ignored during baselining',&
         error)
    if (error) return
    call comm%mask%register(&
         'Use a mask to define line regions to be ignored during baselining',&
         code_access_speset,error)
    if (error) return
  end subroutine cubemain_lineset_or_mask_comm_register
  !
  subroutine cubemain_lineset_or_mask_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(lineset_or_mask_comm_t), intent(in)    :: comm
    character(len=*),              intent(in)    :: line
    type(lineset_or_mask_user_t),  intent(out)   :: user
    logical,                       intent(inout) :: error
    !
    character(len=*), parameter :: rname='LINESET>OR>MASK>COMM>PARSE'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call comm%lineset%parse(line,user%lineset,error)
    if (error) return
    call comm%mask%parse(line,user%mask,error)
    if (error) return
  end subroutine cubemain_lineset_or_mask_comm_parse
  !
  !------------------------------------------------------------------------
  !
  subroutine cubemain_lineset_or_mask_user_toprog(user,cube,comm,prog,error)
    use cube_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(lineset_or_mask_user_t), intent(in)    :: user
    type(cube_t),                  intent(in)    :: cube
    type(lineset_or_mask_comm_t),  intent(in)    :: comm    
    type(lineset_or_mask_prog_t),  intent(out)   :: prog
    logical,                       intent(inout) :: error
    !
    character(len=*), parameter :: rname='LINESET>OR>MASK>USER>TOPROG'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    ! Sanity check: Are the line and mask key combined?
    if (user%lineset%present.and.user%mask%present) then
       call cubemain_message(seve%e,rname,'The /LINE or /MASK key are exclusive from each other')
       call cubemain_message(seve%e,rname,'   => Choose only one of them')
       error = .true.
       return
    endif
    ! Process the line method
    if (user%lineset%present) then
       prog%method = code_line_set
       call user%lineset%toprog(cube,prog%lineset,error)
       if (error) return
   else if (user%mask%present) then
       prog%method = code_line_mask
       call user%mask%toprog(comm%mask,prog%mask,error)
       if (error) return
       call prog%mask%must_be_3d(error)
       if (error) return
      call prog%mask%check_consistency(cube,error)
       if (error) return
    else
       ! Default to no line window
       prog%method = code_line_none
    endif
  end subroutine cubemain_lineset_or_mask_user_toprog
  !
  !------------------------------------------------------------------------
  !
  subroutine cubemain_lineset_or_mask_prog_list(prog,error)
    use cubetools_format
    !----------------------------------------------------------------------
    ! List the lineset_or_mask_prog information in a user friendly way
    !----------------------------------------------------------------------
    class(lineset_or_mask_prog_t), intent(in)    :: prog
    logical,                       intent(inout) :: error
    !
    character(len=mess_l) :: mess
    character(len=*), parameter :: key='Line masking method'
    character(len=*), parameter :: rname='LINESET>OR>MASK>PROG>LIST'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    select case (prog%method)
    case (code_line_none)
       call cubemain_message(seve%r,rname,'')
       mess = cubetools_format_stdkey_boldval(key,'None',40)
       call cubemain_message(seve%r,rname,mess)
    case (code_line_set)
       call prog%lineset%list(key,error)
       if (error) return
    case (code_line_mask)
        call prog%mask%list(key,error)
       if (error) return
    case default
       call cubemain_message(seve%e,rname,'Unknown '//trim(key)//' code')
       error = .true.
       return
    end select
  end subroutine cubemain_lineset_or_mask_prog_list
end module cubemain_lineset_or_mask_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
