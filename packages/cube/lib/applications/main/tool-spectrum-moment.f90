!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_spectrum_moment_tool
  !----------------------------------------------------------------------
  ! Implementation of the computation of the moments of a line.  This
  ! modules assumes that all channels of the spectrum are completely valid,
  ! i.e., there is no provision for NaN. In other words, it is assumed
  ! that the spectrum is unblanked before calls to these routines.
  !----------------------------------------------------------------------
  use cubetools_parameters
  use cubetools_nan
  use cubemain_messaging
  use cubemain_spectrum_real
  !
  public :: spectrum_moment_t
  public :: spectrum_tpeak_t,spectrum_vpeak_t,spectrum_eqwd_t
  public :: spectrum_area_t,spectrum_cent_t,spectrum_fwhm_t
  private
  !
  type spectrum_moment_t
     real(kind=sign_k) :: sig = 0e0 ! Signal
     real(kind=sign_k) :: noi = 0e0 ! Noise
     real(kind=sign_k) :: snr = 0e0 ! Signal-to-Noise Ratio
   contains
     procedure, public :: nullify => cubemain_spectrum_moment_nullify
  end type spectrum_moment_t
  !
  type, extends(spectrum_moment_t) :: spectrum_tpeak_t
     integer(kind=chan_k) :: ic = 0 ! Associated channel
   contains
     procedure, public :: compute => cubemain_spectrum_moment_tpeak
  end type spectrum_tpeak_t
  !
  type, extends(spectrum_moment_t) :: spectrum_vpeak_t
   contains
     procedure, public :: compute => cubemain_spectrum_moment_vpeak
  end type spectrum_vpeak_t
  !
  type, extends(spectrum_moment_t) :: spectrum_area_t
   contains
     procedure, public :: compute => cubemain_spectrum_moment_area
  end type spectrum_area_t
  !
  type, extends(spectrum_moment_t) :: spectrum_cent_t
   contains
     procedure, public :: compute => cubemain_spectrum_moment_cent
  end type spectrum_cent_t
  !
  type, extends(spectrum_moment_t) :: spectrum_fwhm_t
   contains
     procedure, public :: compute => cubemain_spectrum_moment_fwhm
  end type spectrum_fwhm_t
  !
  type, extends(spectrum_moment_t) :: spectrum_eqwd_t
   contains
     procedure, public :: compute => cubemain_spectrum_moment_eqwd
  end type spectrum_eqwd_t
  !
contains
  !
  subroutine cubemain_spectrum_moment_nullify(moment)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spectrum_moment_t), intent(out) :: moment
    !
    moment%sig = gr4nan
    moment%noi = gr4nan
    moment%snr = gr4nan
  end subroutine cubemain_spectrum_moment_nullify
  !
  !------------------------------------------------------------------------
  !
  subroutine cubemain_spectrum_moment_tpeak(tpeak,line,error)
    !----------------------------------------------------------------------
    ! Find channel and value of line peak temperature
    !----------------------------------------------------------------------
    class(spectrum_tpeak_t), intent(out)   :: tpeak
    type(spectrum_t),        intent(in)    :: line
    logical,                 intent(inout) :: error
    !
    integer(kind=chan_k) :: ichan
    character(len=*), parameter :: rname='SPECTRUM>MOMENT>TPEAK'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    ! Sanity check
    if (line%n.lt.1) then
       call tpeak%nullify()
       tpeak%ic = code_null
       return
    endif
    !
    tpeak%ic = 1
    tpeak%sig = line%t(1)
    do ichan=2,line%n
       if (tpeak%sig.lt.line%t(ichan)) then
          tpeak%ic = ichan
          tpeak%sig = line%t(ichan)
       endif
    enddo
    if (line%noi.gt.0) then
       tpeak%noi = line%noi
       tpeak%snr = tpeak%sig/tpeak%noi
    else
       tpeak%noi = gr4nan
       tpeak%snr = gr4nan
    endif
  end subroutine cubemain_spectrum_moment_tpeak
  !
  subroutine cubemain_spectrum_moment_vpeak(vpeak,line,tpeak,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(spectrum_vpeak_t), intent(out)   :: vpeak
    type(spectrum_t),        intent(in)    :: line
    type(spectrum_tpeak_t),  intent(in)    :: tpeak
    logical,                 intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRUM>MOMENT>VPEAK'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    if (tpeak%sig.gt.0.0) then
       vpeak%sig = line%v(tpeak%ic)
       vpeak%noi = abs(line%inc)
       vpeak%snr = abs(line%inc)/vpeak%noi
    else
       vpeak%sig = gr4nan
       vpeak%noi = gr4nan
       vpeak%snr = gr4nan
    endif
  end subroutine cubemain_spectrum_moment_vpeak
  !
  !------------------------------------------------------------------------
  !
  subroutine cubemain_spectrum_moment_area(area,line,error)
    !----------------------------------------------------------------------
    ! Compute line area
    !----------------------------------------------------------------------
    class(spectrum_area_t), intent(out)   :: area
    type(spectrum_t),       intent(in)    :: line
    logical,                intent(inout) :: error
    !
    integer(kind=chan_k) :: ichan
    character(len=*), parameter :: rname='SPECTRUM>MOMENT>AREA'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    ! Sanity check
    if (line%n.lt.1) then
       call area%nullify()
       return
    endif
    !
    area%sig = 0
    do ichan=1,line%n
       area%sig = area%sig+line%t(ichan)
    enddo ! ichan
    area%sig = area%sig
    area%noi = sqrt(real(line%n))*line%noi
    if (area%noi.ne.0) then
       area%snr = area%sig/area%noi
    else
       area%snr = gr4nan
    endif
    area%sig = area%sig*abs(line%inc)
    area%noi = area%noi*abs(line%inc)
  end subroutine cubemain_spectrum_moment_area
  !
  subroutine cubemain_spectrum_moment_cent(cent,line,area,error)
    !----------------------------------------------------------------------
    ! Compute line velocity centroid
    !----------------------------------------------------------------------
    class(spectrum_cent_t), intent(out)   :: cent
    type(spectrum_t),       intent(in)    :: line
    type(spectrum_area_t),  intent(in)    :: area
    logical,                intent(inout) :: error
    !
    integer(kind=chan_k) :: ichan
    real(kind=sign_k) :: vmin,vmax,vmean,vsdev,vvar
    real(kind=sign_k) :: num,den,asnrsquared,diff_squared
    real (kind=sign_k), parameter :: x0 = 0.91
    real (kind=sign_k), parameter :: x0squared = x0**2
    character(len=*), parameter :: rname='SPECTRUM>MOMENT>CENT'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    ! Sanity check
    if (line%n.lt.1) then
       call cent%nullify()
       return
    endif
    !
    num = 0
    den = 0
    do ichan=1,line%n
       num = num+line%t(ichan)*line%v(ichan)
       den = den+line%t(ichan)
    enddo ! ichan
    if (den.ne.0) then
       cent%sig = num/den
       vmin = min(line%v(1),line%v(line%n))
       vmax = max(line%v(1),line%v(line%n))
       if ((vmin.le.cent%sig).and.(cent%sig.le.vmax)) then
          ! The centroid and its uncertainty can be computed
          vmean = 0.0
          do ichan=1,line%n
             vmean = vmean+line%v(ichan)
          enddo ! ichan
          vmean = vmean/real(line%n)
          vvar = 0.0
          do ichan=1,line%n
             vvar = vvar+(line%v(ichan)-vmean)**2
          enddo ! ichan
          vvar = vvar/real(line%n) ! Yes, this is not exactly the variance
          vsdev = sqrt(vvar)
          asnrsquared = area%snr**2
          diff_squared = asnrsquared-x0squared
          if (asnrsquared.ge.1.0) then
             num = sqrt(1+ (asnrsquared/diff_squared) * ((vmean-cent%sig)/vsdev)**2 )
             den = sqrt(diff_squared)
             cent%noi = x0*vsdev*num/den
             cent%snr = abs(line%inc)/cent%noi
          else
             cent%noi = gr4nan
             cent%snr = gr4nan
          endif
       else
          call cent%nullify()
       endif
    else
       call cent%nullify()
    endif
  end subroutine cubemain_spectrum_moment_cent
  !
  subroutine cubemain_spectrum_moment_fwhm(fwhm,line,cent,error)
    use phys_const
    !----------------------------------------------------------------------
    ! Compute line standard deviation
    !----------------------------------------------------------------------
    class(spectrum_fwhm_t), intent(out)   :: fwhm
    type(spectrum_t),       intent(in)    :: line
    type(spectrum_cent_t),  intent(in)    :: cent
    logical,                intent(inout) :: error
    !
    integer(kind=chan_k) :: ichan
    real(kind=sign_k) :: num,den
    character(len=*), parameter :: rname='SPECTRUM>MOMENT>FWHM'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    ! Sanity check
    if (line%n.lt.1) then
       call fwhm%nullify()
       return
    endif
    !
    num = 0
    den = 0
    do ichan=1,line%n
       num = num+line%t(ichan)*(line%v(ichan)-cent%sig)**2
       den = den+line%t(ichan)
    enddo ! ichan
    if (den.ne.0) then
       fwhm%sig = fwhm_per_sdev*sqrt(num/den)
       fwhm%noi = gr4nan
       fwhm%snr = gr4nan
    else
       call fwhm%nullify()
    endif
  end subroutine cubemain_spectrum_moment_fwhm
  !
  !------------------------------------------------------------------------
  !
  subroutine cubemain_spectrum_moment_eqwd(eqwd,tpeak,area,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(spectrum_eqwd_t), intent(out)   :: eqwd
    type(spectrum_tpeak_t), intent(in)    :: tpeak
    type(spectrum_area_t),  intent(in)    :: area
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRUM>MOMENT>EQWD'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    if ((area%sig.gt.0.0).and.(tpeak%sig.gt.0.0)) then
       eqwd%sig = area%sig/tpeak%sig
       eqwd%noi = gr4nan
       eqwd%snr = gr4nan
    else
       call eqwd%nullify()
    endif
  end subroutine cubemain_spectrum_moment_eqwd
end module cubemain_spectrum_moment_tool
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
