!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_replace
  use cubetools_parameters
  use cube_types
  use cubetools_structure
  use cubetools_keywordlist_types
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubemain_messaging
  !
  public :: replace
  private
  !
  character(len=*), parameter :: keyvalues(1) = &
       ['NaN'] ! We could also add +- inf
  integer(kind=code_k), parameter :: valisreal = -1
  integer(kind=code_k), parameter :: valisnan = 1
  !
  type :: replace_comm_t
     type(option_t),      pointer :: comm
     type(cubeid_arg_t),  pointer :: incube
     type(option_t),      pointer :: value
     type(keywordlist_comm_t), pointer :: ival_arg
     type(keywordlist_comm_t), pointer :: oval_arg
     type(cube_prod_t),   pointer :: replaced
   contains
     procedure, public  :: register     => cubemain_replace_register
     procedure, private :: parse        => cubemain_replace_parse
     procedure, private :: parse_values => cubemain_replace_parse_values
     procedure, private :: main         => cubemain_replace_main
  end type replace_comm_t
  type(replace_comm_t) :: replace
  !
  type replace_user_t
     type(cubeid_user_t)   :: cubeids
     character(len=argu_l) :: ival   ! Value to be replaced 
     character(len=argu_l) :: oval   ! Value to replace with
   contains
     procedure, private :: toprog => cubemain_replace_user_toprog
  end type replace_user_t
  !
  type replace_prog_t
     type(cube_t), pointer :: incube     ! Input cube
     type(cube_t), pointer :: replaced   ! Output cube
     integer(kind=code_k)  :: ivaliskey  ! Value to replace is NaN?
     real(kind=sign_k)     :: ival       ! Value to be replaced
     real(kind=sign_k)     :: oval       ! Value to replace with
     procedure(cubemain_replace_prog_act_nan), pointer :: act => null()
   contains
     procedure, private :: header => cubemain_replace_prog_header
     procedure, private :: data   => cubemain_replace_prog_data
     procedure, private :: loop   => cubemain_replace_prog_loop
  end type replace_prog_t
  !
contains
  !
  subroutine cubemain_replace_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(replace_user_t) :: user
    character(len=*), parameter :: rname='REPLACE>COMMAND'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call replace%parse(line,user,error)
    if (error) return
    call replace%main(user,error)
    if (error) continue
  end subroutine cubemain_replace_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_replace_register(replace,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(replace_comm_t), intent(inout) :: replace
    logical,               intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    type(cube_prod_t) :: oucube
    type(keywordlist_comm_t) :: keyarg
    character(len=*), parameter :: comm_abstract='Replace a value by another one in the data'
    character(len=*), parameter :: comm_help=strg_id
    character(len=*), parameter :: rname='REPLACE>REGISTER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'REPLACE','[cube]',&
         comm_abstract,&
         comm_help,&
         cubemain_replace_command,&
         replace%comm,error)
    if (error) return
    call cubearg%register(&
         'CUBE',&
         'Input CUBE',&
         strg_id,&
         code_arg_optional,&
         [flag_any],&
         code_read, &
         code_access_subset, &
         replace%incube, &
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'VALUE','ival oval',&
         'Define value to be replaced and its replacement',&
         strg_id,&
         replace%value,error)
    if (error) return
    call keyarg%register(&
         'ival',&
         'Value to be replaced',&
         strg_id,&
         code_arg_mandatory,&
         keyvalues,&
         flexible,&
         replace%ival_arg,&
         error)
    if (error) return
    call keyarg%register(&
         'oval',&
         'Value to replace with',&
         strg_id,&
         code_arg_mandatory,&
         keyvalues,&
         flexible,&
         replace%oval_arg,&
         error)
    if (error) return
    !
    ! Product
    call oucube%register(&
         'REPLACED',&
         'Cube with replaced values',&
         strg_id,&
         [flag_replace],&
         replace%replaced,&
         error,&
         flagmode=keep_prod)
    if (error) return
  end subroutine cubemain_replace_register
  !
  subroutine cubemain_replace_parse(replace,line,user,error)
    !----------------------------------------------------------------------
    ! REPLACE cubname
    !----------------------------------------------------------------------
    class(replace_comm_t), intent(in)    :: replace
    character(len=*),      intent(in)    :: line
    type(replace_user_t),  intent(out)   :: user
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='REPLACE>PARSE'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,replace%comm,user%cubeids,error)
    if (error) return
    call replace%parse_values(line,user,error)
    if (error) return
  end subroutine cubemain_replace_parse
  !
  subroutine cubemain_replace_parse_values(replace,line,user,error)
    !----------------------------------------------------------------------
    ! /VALUE ival oval
    !----------------------------------------------------------------------
    class(replace_comm_t), intent(in)    :: replace
    character(len=*),      intent(in)    :: line
    type(replace_user_t),  intent(inout) :: user
    logical,               intent(inout) :: error
    !
    logical :: present
    character(len=*), parameter :: rname='REPLACE>PARSE>VALUE'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call replace%value%present(line,present,error)
    if (error) return
    if (present) then
       call cubetools_getarg(line,replace%value,1,user%ival,mandatory,error)
       if (error) return
       call cubetools_getarg(line,replace%value,2,user%oval,mandatory,error)
       if (error) return       
    else
       call cubemain_message(seve%e,rname,'/VALUE key is mandatory')
       error = .true.
       return
    endif
  end subroutine cubemain_replace_parse_values
  !
  subroutine cubemain_replace_main(comm,user,error)
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(replace_comm_t), intent(in)    :: comm
    type(replace_user_t),  intent(in)    :: user
    logical,               intent(inout) :: error
    !
    type(replace_prog_t) :: prog
    character(len=*), parameter :: rname='REPLACE>MAIN'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubemain_replace_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_replace_user_toprog(user,comm,prog,error)
    use cubetools_unit
    use cubetools_nan
    use cubetools_user2prog
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(replace_user_t), intent(in)    :: user
    class(replace_comm_t), intent(in)    :: comm
    type(replace_prog_t),  intent(inout) :: prog
    logical,               intent(inout) :: error
    !
    integer(kind=code_k) :: ikey
    character(len=mess_l) :: mess
    character(len=argu_l) :: argu
    type(unit_user_t) :: nounit
    character(len=*), parameter :: rname='REPLACE>USER>TOPROG'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(comm%incube,user%cubeids,prog%incube,error)
    if (error) return
    !
    call nounit%get_from_code(code_unit_unk,error)
    if (error) return
    !
    if (user%ival.eq."=") then
       call cubemain_message(seve%e,rname,'= syntax not supported for value to be replaced')
       error =  .true.
       return
    endif
    if (user%oval.eq."=") then
       call cubemain_message(seve%e,rname,'= syntax not supported for replacing value')
       error =  .true.
       return
    endif
    !
    call cubetools_keywordlist_user2prog(replace%ival_arg,user%ival,ikey,argu,error)
    if (error) return
    if (argu.eq.strg_unresolved) then
       call cubetools_user2prog_resolve_star(user%ival,nounit,gr4nan,prog%ival,error)
       if (error) return
       if (user%ival.eq.strg_star) then
          prog%ivaliskey = valisnan
       else
          prog%ivaliskey = valisreal
       endif
    else
       select case(argu)
       case('NAN')
          prog%ival = gr4nan
          prog%ivaliskey = valisnan
       case default
          call cubemain_message(seve%e,rname,"Unknown key value: "//argu)
          error =  .true.
          return
       end select
    endif
    call cubetools_keywordlist_user2prog(replace%oval_arg,user%oval,ikey,argu,error)
    if (error) return
    if (argu.eq.strg_unresolved) then
       call cubetools_user2prog_resolve_star(user%oval,nounit,gr4nan,prog%oval,error)
       if (error) return
    else
       select case(argu)
       case('NAN')
          prog%oval = gr4nan
       case default
          call cubemain_message(seve%e,rname,"Unknown key value: "//argu)
          error =  .true.
          return
       end select
    endif
    ! User feedback
    write(mess,'(2(a,1pg14.7))') 'Replacing ',prog%ival,' with ',prog%oval
    call cubemain_message(seve%i,rname,mess)
    !
    if (prog%ivaliskey.eq.valisnan) then
       prog%act => cubemain_replace_prog_act_nan
    else
       prog%act => cubemain_replace_prog_act_real
    endif
  end subroutine cubemain_replace_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_replace_prog_header(prog,comm,error)
    use cubeadm_clone
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(replace_prog_t), intent(inout) :: prog
    type(replace_comm_t),  intent(in)    :: comm
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='REPLACE>PROG>HEADER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_clone_header(comm%replaced,prog%incube,prog%replaced,error)
    if (error) return
  end subroutine cubemain_replace_prog_header
  !
  subroutine cubemain_replace_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(replace_prog_t), intent(inout) :: prog
    logical,               intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: itertask
    character(len=*), parameter :: rname='REPLACE>PROG>DATA'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(itertask,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(itertask)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(itertask,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(itertask)
       if (.not.error) then
          call prog%loop(itertask,error)
       endif
       !$OMP END TASK
    enddo ! itertask
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubemain_replace_prog_data
  !
  subroutine cubemain_replace_prog_loop(prog,itertask,error)
    use cubeadm_taskloop
    !----------------------------------------------------------------------
    ! The subcube iterator will be shared by all input and output subcubes
    !----------------------------------------------------------------------
    class(replace_prog_t),    intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: itertask
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='REPLACE>PROG>LOOP>NAN'
    !
    do while (itertask%iterate_entry(error))
       call prog%act(itertask,error)
       if (error) return
    enddo  ! ientry
  end subroutine cubemain_replace_prog_loop
  !
  subroutine cubemain_replace_prog_act_nan(prog,itertask,error)
    use cubetools_nan
    use cubeadm_taskloop
    use cubeadm_subcube_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(replace_prog_t),    intent(inout) :: prog
    type(cubeadm_iterator_t), intent(in)    :: itertask
    logical,                  intent(inout) :: error
    !
    integer(kind=indx_k) :: ix,iy,iz
    type(subcube_t) :: input,replaced
    character(len=*), parameter :: rname='REPLACE>PROG>ACT>NAN'
    !
    ! Subcube2subcubes are initialized here as their size (3rd dim) may change from
    ! from one subcube2subcube to another.
    call input%associate('input',prog%incube,itertask,error)
    if (error) return
    call replaced%allocate('replaced',prog%replaced,itertask,error)
    if (error) return
    !
    call input%get(error)
    do iz=1,input%nz
       do iy=1,input%ny
          do ix=1,input%nx
             if (ieee_is_nan(input%val(ix,iy,iz))) then
                replaced%val(ix,iy,iz) = prog%oval
             else
                replaced%val(ix,iy,iz) = input%val(ix,iy,iz)
             endif
          enddo ! ix
       enddo ! iy
    enddo ! iz
    call replaced%put(error)
    if (error) return
  end subroutine cubemain_replace_prog_act_nan
  !
  subroutine cubemain_replace_prog_act_real(prog,itertask,error)
    use cubetools_nan
    use cubeadm_taskloop
    use cubeadm_subcube_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(replace_prog_t),    intent(inout) :: prog
    type(cubeadm_iterator_t), intent(in)    :: itertask
    logical,                  intent(inout) :: error
    !
    integer(kind=indx_k) :: ix,iy,iz
    type(subcube_t) :: input,replaced
    character(len=*), parameter :: rname='REPLACE>PROG>ACT>REAL'
    !
    ! Subcube2subcubes are initialized here as their size (3rd dim) may change from
    ! from one subcube2subcube to another.
    call input%associate('input',prog%incube,itertask,error)
    if (error) return
    call replaced%allocate('replaced',prog%replaced,itertask,error)
    if (error) return
    !
    call input%get(error)
    do iz=1,input%nz
       do iy=1,input%ny
          do ix=1,input%nx
             if (input%val(ix,iy,iz).eq.prog%ival) then
                replaced%val(ix,iy,iz) = prog%oval
             else
                replaced%val(ix,iy,iz) = input%val(ix,iy,iz)
             endif
          enddo ! ix
       enddo ! iy
    enddo ! iz
    call replaced%put(error)
    if (error) return
  end subroutine cubemain_replace_prog_act_real
end module cubemain_replace
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
