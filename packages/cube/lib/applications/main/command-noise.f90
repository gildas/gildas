!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_noise
  use cubetools_parameters
  use cube_types
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubemain_messaging
  use cubemain_range
  use cubemain_windowing
  !
  public :: noise
  private
  !
  type :: noise_comm_t
     type(option_t), pointer :: comm
     type(cubeid_arg_t), pointer :: sig
     type(cubeid_arg_t), pointer :: win
     type(cube_prod_t),  pointer :: noi
     type(option_t), pointer :: mad     
     type(option_t), pointer :: nchan
     type(range_opt_t)       :: range
   contains
     procedure, public  :: register => cubemain_noise_comm_register
     procedure, private :: parse    => cubemain_noise_comm_parse
     procedure, private :: main     => cubemain_noise_comm_main
  end type noise_comm_t
  type(noise_comm_t) :: noise
  !
  type noise_user_t
     type(cubeid_user_t)   :: cubeids           ! Signal and window cubeids
     logical               :: dorange = .false. ! Do range 
     type(range_array_t)   :: range             ! Range(s) to be ignored
     logical               :: domad = .false.   ! Do MAD noise
     character(len=argu_l) :: nchan             ! Number of channels to be used in mad mode
   contains
     procedure, private :: toprog => cubemain_noise_user_toprog
  end type noise_user_t
  !
  type noise_prog_t
     logical               :: domad = .false.
     logical               :: dowin = .false.
     logical               :: doloc = .false.
     integer(kind=chan_k)  :: nchan = 0  ! number of channels to be used
     integer(kind=chan_k)  :: nnois = 0  ! Number of noise estimates
     integer(kind=chan_k)  :: nhalf = 0  ! Half number of used channels
     type(window_array_t)  :: glowin     ! One set of windows for the full index
     type(cube_t), pointer :: locwin     ! Number and positions of windows depend on the spectrum
     type(cube_t), pointer :: sig
     type(cube_t), pointer :: noi
     procedure(cubemain_noise_prog_mad_loop), pointer :: loop => null()
   contains
     procedure, private :: header        => cubemain_noise_prog_header
     procedure, private :: data          => cubemain_noise_prog_data
     procedure, private :: mad_act       => cubemain_noise_prog_mad_act
     procedure, private :: glowin_act   => cubemain_noise_prog_glowin_act
     procedure, private :: locwin_act   => cubemain_noise_prog_locwin_act
     procedure, private :: compute_noise => cubemain_noise_prog_compute_noise
  end type noise_prog_t
  !
contains
  !
  subroutine cubemain_noise_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)  :: line
    logical,          intent(out) :: error
    !
    type(noise_user_t) :: user
    character(len=*), parameter :: rname='NOISE>COMMAND'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call noise%parse(line,user,error)
    if (error) return
    call noise%main(user,error)
    if (error) return
  end subroutine cubemain_noise_command
  !
  !------------------------------------------------------------------------
  !
  subroutine cubemain_noise_comm_register(comm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(noise_comm_t), intent(inout) :: comm
    logical,             intent(inout) :: error
    !
    type(cubeid_arg_t) :: incube
    type(cube_prod_t) :: oucube
    type(standard_arg_t) :: stdarg
    character(len=*), parameter :: comm_abstract=&
         'Compute the noise RMS of each spectrum of a cube'
    character(len=*), parameter :: comm_help=&
         'NOISE uses either a global window to compute the moments&
         & (/RANGE), or individual windows set for each spectrum&
         & (created by CUBE\WINDOW).'
    character(len=*), parameter :: rname='NOISE>COMM>REGISTER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    ! Syntax
    call cubetools_register_command(&
         'NOISE','[signalid [windowid]]',&
         comm_abstract,&
         comm_help,&
         cubemain_noise_command,&
         noise%comm,error)
    if (error) return
    call incube%register(&
         'SIGNAL',&
         'Signal cubeid',&
         strg_id,&
         code_arg_optional,&
         [flag_cube],&
         code_read,&
         code_access_speset,&
         comm%sig,&
         error)
    if (error) return
    call incube%register(&
         'WINDOW',&
         'Local spectral range(s) containing signal',&
         strg_id,&
         code_arg_optional,&
         [flag_window],&
         code_read,&
         code_access_speset,&
         comm%win,&         
         error)
    if (error) return
    !
    call noise%range%register(&
         'RANGE',&
         'Define global spectral range(s) containing signal',&
         range_is_multiple,error)
    if (error) return
    !
    call cubetools_register_option(&
         'MAD','',&
         'Compute Median Absolute Deviation noise instead of RMS',&
         strg_id,&
         noise%mad,error)
    if (error) return
    !
    call cubetools_register_option(&
         'NCHAN','nchan',&
         'Number of channels in each block to be used to compute noise',&
         strg_id,&
         noise%nchan,error)
    if (error) return
    call stdarg%register(&
         'nchan',&
         'number of channels',&
         strg_id,&
         code_arg_mandatory,&
         error)
    if (error) return
    !
    ! Product
    call oucube%register(&
         'NOISE',&
         'Cube of noise estimations',&
         strg_id,&
         [flag_noise],&
         comm%noi,&
         error)
    if (error) return
  end subroutine cubemain_noise_comm_register
  !
  subroutine cubemain_noise_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! NOISE signalid [windowid]
    ! /NCHAN nchan
    ! /RANGE vfirst vlast
    ! /MAD
    !----------------------------------------------------------------------
    class(noise_comm_t), intent(in)    :: comm
    character(len=*),    intent(in)    :: line
    type(noise_user_t),  intent(out)   :: user
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='NOISE>COMM>PARSE'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%mad%present(line,user%domad,error)
    if (error) return
    call cubemain_noise_comm_parse_nchan(line,comm%nchan,1,user%nchan,error)
    if (error) return
    call comm%range%parse(line,user%dorange,user%range,error)
    if (error) return
  end subroutine cubemain_noise_comm_parse
  !
  subroutine cubemain_noise_comm_parse_nchan(line,opt,iarg,nchan,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*),      intent(in)    :: line
    type(option_t),        intent(in)    :: opt
    integer(kind=4),       intent(in)    :: iarg
    character(len=argu_l), intent(out)   :: nchan
    logical,               intent(inout) :: error
    !
    logical :: present
    character(len=*), parameter :: rname='NOISE>COMM>PARSE>NCHAN'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call opt%present(line,present,error)
    if (error) return
    if (present) then
       call cubetools_getarg(line,opt,iarg,nchan,mandatory,error)
       if (error) return
    else
       nchan = strg_star
    endif
  end subroutine cubemain_noise_comm_parse_nchan
  !
  subroutine cubemain_noise_comm_main(comm,user,error)
    use cubeadm_timing
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(noise_comm_t), intent(in)    :: comm
    type(noise_user_t),  intent(inout) :: user
    logical,             intent(inout) :: error
    !
    type(noise_prog_t) :: prog
    character(len=*), parameter :: rname='NOISE>MAIN'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubemain_noise_comm_main
  !
  !------------------------------------------------------------------------
  !
  subroutine cubemain_noise_user_toprog(user,comm,prog,error)
    use cubetools_user2prog
    use cubetools_consistency_methods
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(noise_user_t), intent(inout) :: user
    class(noise_comm_t), intent(in)    :: comm
    type(noise_prog_t),  intent(inout) :: prog
    logical,             intent(inout) :: error
    !
    logical :: conspb
    integer(kind=chan_k) :: default
    character(len=*), parameter :: rname='NOISE>USER>TOPROG'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    default = 0
    call cubetools_user2prog_resolve_star(user%nchan,default,prog%nchan,error)
    if (error) return
    !
    call cubeadm_get_header(comm%sig,user%cubeids,prog%sig,error)
    if (error) return
    !
    prog%domad = user%domad
    if (user%domad) then
       ! No window searched
       prog%dowin = .false.
    else
       prog%dowin = .true.
       if (user%dorange) then
          ! Define a global window
          prog%doloc = .false.
          ! Ranges are truncated to avoid segfault
          user%range%val(:)%truncate = .true.
          call noise%range%user2prog(prog%sig,user%range,prog%glowin,error)
          if (error) return
       else
          prog%doloc = .true.
          conspb = .false.
          ! Search for a data cube with the definitions of the local windows
          call cubeadm_get_header(comm%win,user%cubeids,prog%locwin,error)
          if (error) return
          call cubetools_consistency_spatial('Input cube',prog%sig%head,'Window',prog%locwin%head,conspb,error)
          if (error) return
          if (cubetools_consistency_failed(rname,conspb,error)) return
       endif
    endif
    !
    if (prog%domad) then
       prog%loop => cubemain_noise_prog_mad_loop
    else if (prog%dowin) then
       if (prog%doloc) then
          prog%loop => cubemain_noise_prog_locwin_loop
       else
          prog%loop => cubemain_noise_prog_glowin_loop
       endif
    endif
  end subroutine cubemain_noise_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_noise_prog_header(prog,comm,error)
    use cubetools_header_methods
    use cubeadm_clone
    !---------------------------------------------------------------------- 
    ! To deal with edge effects, we cheat on the last frequency segment
    ! that we attribute beyond the original frequency range while we 
    ! always compute the noise inside this range. This should have no 
    ! consequence as the noise should be interpolated in frequency
    ! in later use.
    !----------------------------------------------------------------------
    class(noise_prog_t), intent(inout) :: prog
    type(noise_comm_t),  intent(in)    :: comm
    logical,             intent(inout) :: error
    !
    integer(kind=chan_k) :: in_nchan,nhalf
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='NOISE>PROG>HEADER'
    !
    if ((prog%domad).and.(prog%dowin)) then
       call cubemain_message(seve%e,rname,'Options /RANGE and /MAD are exclusives')
       error = .true.
       return
    else if (.not.((prog%domad).or.(prog%dowin))) then
       call cubemain_message(seve%e,rname,'Do not understand what you want')
       error = .true.
       return
    endif
    !
    call cubetools_header_get_nchan(prog%sig%head,in_nchan,error)
    if (error) return
    call cubeadm_clone_header(comm%noi,prog%sig,prog%noi,error)
    if (error) return
    if ((prog%nchan.lt.0).or.(prog%nchan.gt.in_nchan)) then
       call cubemain_message(seve%e,rname,'Number of asked channels outside cube range')
       error = .true.
       return
    else if (prog%nchan.eq.0) then
       ! Default value
       prog%nchan = in_nchan
       nhalf = prog%nchan
       prog%nnois = 1
    else
       ! User value
       ! Ensure that prog%nchan will be odd in output because the median definition 
       ! is simpler
       if (mod(prog%nchan,2).eq.0) then
          prog%nchan = prog%nchan-1
       endif
       nhalf = floor(0.5*prog%nchan)
       prog%nnois  = floor(dble(in_nchan)/dble(nhalf))
       if (nhalf*prog%nnois.lt.in_nchan) then
          prog%nnois = prog%nnois+1
       endif
    endif
    if ((prog%nchan.lt.5)) then
       call cubemain_message(seve%e,rname,'Minimum number of channels must be 5')
       error = .true.
       return
    else if (prog%nchan.gt.in_nchan) then
       call cubemain_message(seve%e,rname,'You asked to compute the noise rms on more samples than channels in the spectrum!')
       error = .true.
       return
    endif
    prog%nhalf = floor(0.5*prog%nchan)
    ! Update noise header accordingly
    call cubetools_header_put_nchan(prog%nnois,prog%noi%head,error)
    if (error) return
    call cubetools_header_multiply_spectral_spacing(nhalf,prog%noi%head,error)
    if (error) return
    call cubetools_header_rederive_spectral_axes(prog%noi%head,error)
    if (error) return
    ! User feedback
    write(mess,'(a,i0,a,i0,a)') 'Computing ',prog%nnois,' noise values, each using ',prog%nchan,' channels'
    call cubemain_message(seve%i,rname,mess)
  end subroutine cubemain_noise_prog_header
  !
  subroutine cubemain_noise_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(noise_prog_t), intent(inout) :: prog
    logical,             intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='NOISE>PROG>DATA'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,iter,error)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) call prog%loop(iter,error)
       !$OMP END TASK
    enddo ! iter
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubemain_noise_prog_data
  !
  !------------------------------------------------------------------------
  !
  subroutine cubemain_noise_prog_mad_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(noise_prog_t),      intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(spectrum_t) :: spe,good,noi
    character(len=*), parameter :: rname='NOISE>PROG>MAD>LOOP'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call spe%associate('spe',prog%sig,iter,error)
    if (error) return
    call good%allocate('good',prog%sig,error)
    if (error) return
    call noi%allocate('noi',prog%noi,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
       call prog%mad_act(iter%ie,spe,good,noi,error)
       if (error) return
    enddo
  end subroutine cubemain_noise_prog_mad_loop
  !
  subroutine cubemain_noise_prog_mad_act(prog,ie,spe,good,noi,error)
    use cubetools_nan
    use cubeadm_spectrum_types
    use cubemain_spectrum_blanking
    use cubemain_statistics_tool
    !----------------------------------------------------------------------
    ! Compute a running median absolute deviation filter
    !----------------------------------------------------------------------
    class(noise_prog_t),  intent(inout) :: prog
    integer(kind=entr_k), intent(in)    :: ie
    type(spectrum_t),     intent(inout) :: spe
    type(spectrum_t),     intent(inout) :: good
    type(spectrum_t),     intent(inout) :: noi
    logical,              intent(inout) :: error
    !  
    type(spectrum_t) :: line ! We just make the line%y%val array to point => No allocation
    integer(kind=chan_k) :: in,nn,nc
    integer(kind=chan_k) :: first,last,nhalf,nchan
    real(kind=sign_k) :: median
    character(len=*), parameter :: rname='NOISE>PROG>MAD>ACT'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    nchan = prog%nchan
    nhalf = floor(0.5*nchan)
    nn = noi%n
    nc = spe%n
    !
    call spe%get(ie,error)
    if (error) return
    first = 1
    do in=1,nn
       ! The following ensures that
       !    1) we never get past the number of channels
       !    2) we always compute the mad on nchan contiguous samples
       last  = min(first+nchan-1,nc)
       first = last-nchan+1
       call line%point_to(spe,first,last,1.0,error)
       if (error) return
       call good%unblank(line,error)
       if (error) return
       if (good%n.ge.1) then
          median = statistics%median(good%y%val,good%n)
          noi%y%val(in) = statistics%mad(good%y%val,good%n,median)
       else
          noi%y%val(in) = gr4nan
       endif
       first = first+nhalf
    enddo ! in
    call noi%put(ie,error)
    if (error) return   
  end subroutine cubemain_noise_prog_mad_act
  !
  !------------------------------------------------------------------------
  !
  subroutine cubemain_noise_prog_locwin_loop(prog,iter,error)
    use cubetools_parameters
    use cubetools_header_methods
    use cubeadm_taskloop
    use cubeadm_spectrum_types
    use cubemain_window_types
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(noise_prog_t),      intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    integer(kind=chan_k) :: nw
    type(window_t) :: win
    type(spectrum_t) :: spe,base,noi
    character(len=*), parameter :: rname='NOISE>LOCWIN>LOOP'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubetools_header_get_nchan(prog%locwin%head,nw,error)
    if (error) return
    if (nw.le.0) then
       call cubemain_message(seve%e,rname,'Negative or zero number of windows')
       error = .true.
       return
    endif
    !
    call win%associate('win',prog%locwin,iter,error)
    if (error) return
    call spe%allocate('spe',prog%sig,iter,error)
    if (error) return
    call base%allocate('base',prog%sig,error)
    if (error) return
    call noi%allocate('noi',prog%noi,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
       call prog%locwin_act(iter%ie,win,spe,base,noi,error)
       if (error) return
    enddo
  end subroutine cubemain_noise_prog_locwin_loop
  !
  subroutine cubemain_noise_prog_locwin_act(prog,ie,win,spe,base,noi,error)
    use cubetools_nan
    use cubetopology_tool
    use cubeadm_spectrum_types
    use cubemain_window_types
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(noise_prog_t),  intent(inout) :: prog
    integer(kind=entr_k), intent(in)    :: ie
    type(window_t),       intent(inout) :: win
    type(spectrum_t),     intent(inout) :: spe
    type(spectrum_t),     intent(inout) :: base
    type(spectrum_t),     intent(inout) :: noi
    logical,              intent(inout) :: error
    !
    integer(kind=wind_k) :: iw
    integer(kind=chan_k) :: ichan(2)
    real(kind=coor_k) :: vrange(2)
    character(len=*), parameter :: rname='NOISE>LOCAL'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call spe%get(ie,error)
    if (error) return
    call win%get(ie,error)
    if (error) return
    ! Blank signal
    do iw=1,win%n/2 ! *** JP: This is dangerous. It should be a method of the window_t
       if (win%isblank(iw)) cycle
       vrange(:) = win%y%val(2*iw-1:2*iw)
       call cubetopology_tool_vrange2crange(prog%sig,vrange,ichan,error)
       if (error) return
       spe%y%val(ichan(1):ichan(2)) = gr4nan
    enddo
    call prog%compute_noise(spe,base,noi,error)
    if (error) return
    call noi%put(ie,error)
    if (error) return
  end subroutine cubemain_noise_prog_locwin_act
  !
  !------------------------------------------------------------------------
  !
  subroutine cubemain_noise_prog_glowin_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(noise_prog_t),      intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(spectrum_t) :: spe,base,noi
    character(len=*), parameter :: rname='NOISE>WIN>GLOBAL>LOOP'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    if (prog%glowin%n.le.0) then
       call cubemain_message(seve%e,rname,'Negative or zero number of windows')
       error = .true.
       return
    endif
    !
    call spe%allocate('spe',prog%sig,iter,error)
    if (error) return
    call base%allocate('base',prog%sig,error)
    if (error) return
    call noi%allocate('noi',prog%noi,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
       call prog%glowin_act(iter%ie,spe,base,noi,error)
       if (error) return
    enddo
  end subroutine cubemain_noise_prog_glowin_loop
  !
  subroutine cubemain_noise_prog_glowin_act(prog,ie,spe,base,noi,error)
    use cubetools_nan
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(noise_prog_t), target, intent(inout) :: prog
    integer(kind=entr_k),        intent(in)    :: ie
    type(spectrum_t),            intent(inout) :: spe
    type(spectrum_t),            intent(inout) :: base
    type(spectrum_t),            intent(inout) :: noi
    logical,                     intent(inout) :: error
    !
    integer(kind=chan_k) :: iw
    type(window_array_t), pointer :: win
    character(len=*), parameter :: rname='NOISE>PROG>GLOWIN>ACT'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    win => prog%glowin
    !
    call spe%get(ie,error)
    if (error) return
    ! Blank signal
    do iw=1,win%n
       spe%y%val(win%val(iw)%o(1):win%val(iw)%o(2)) = gr4nan
    enddo
    call prog%compute_noise(spe,base,noi,error)
    if (error) return
    call noi%put(ie,error)
    if (error) return
  end subroutine cubemain_noise_prog_glowin_act
  !
  !------------------------------------------------------------------------
  !
  subroutine cubemain_noise_prog_compute_noise(prog,spe,base,noi,error)
    use cubeadm_spectrum_types
    use cubemain_spectrum_computation_tool
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(noise_prog_t), intent(inout) :: prog
    type(spectrum_t),    intent(inout) :: spe
    type(spectrum_t),    intent(inout) :: base
    type(spectrum_t),    intent(inout) :: noi
    logical,             intent(inout) :: error
    !
    type(spectrum_t) :: line
    integer(kind=chan_k) :: first,last,in
    character(len=*), parameter :: rname='NOISE>PROG>COMPUTE>NOISE'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    first = 1
    do in=1,prog%nnois
       last  = min(first+prog%nchan-1,spe%n)
       first = last-prog%nchan+1
       call line%point_to(spe,first,last,1.0,error)
       if (error) return
       call base%unblank(line,error)
       if (error) return
       call spectrum_compute%rms(base,error)
       if (error) return
       noi%y%val(in) = base%noi
       first = first+prog%nhalf
    enddo
  end subroutine cubemain_noise_prog_compute_noise
end module cubemain_noise
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
