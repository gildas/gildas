!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_average_tool
  use cubetools_parameters
  use cubedag_flag
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubeadm_identifier
  use cubeadm_index
  use cube_types
  use cubemain_messaging
  !
  public :: average_comm_t,average_user_t
  public :: code_user_index_cx,code_user_index_custom
  public :: code_user_weight_none,code_user_weight_weival,code_user_weight_noiind
  public :: code_user_weight_weival_noiind,code_user_weight_datnoiind,code_user_weight_datweiind
  private
  !
  ! For average_user_t
  integer(kind=code_k), parameter :: code_user_index_cx     = 1  ! Take cubes from current index
  integer(kind=code_k), parameter :: code_user_index_custom = 2  ! Take cubes from Fortran index_t
  integer(kind=code_k), parameter :: code_user_weight_none          = 0  ! No relative weight provided
  integer(kind=code_k), parameter :: code_user_weight_weival        = 1  ! Weight values provided
  integer(kind=code_k), parameter :: code_user_weight_noiind        = 2  ! Noise cube index provided
  integer(kind=code_k), parameter :: code_user_weight_weival_noiind = 3  ! Weight value and noise index combined
  integer(kind=code_k), parameter :: code_user_weight_datnoiind     = 4  ! The cube index (whereever it comes from) provides
                                                                         ! data and noise cubes to be implicitly split and paired
  integer(kind=code_k), parameter :: code_user_weight_datweiind     = 5  ! The cube index (whereever it comes from) provides
                                                                         ! data and weight cubes to be implicitly split and paired
  !
  ! For average_prog_t
  integer(kind=code_k), parameter :: code_prog_weight_none   = 10  ! No weight/noise cube provided
  integer(kind=code_k), parameter :: code_prog_weight_noiind = 11  ! A noise cube index is available
  integer(kind=code_k), parameter :: code_prog_weight_weiind = 12  ! A weight cube index is available
  !
  type :: average_comm_t
    type(cube_prod_t), pointer :: averaged
    type(cube_prod_t), pointer :: weightc
  contains
    procedure, public :: register => cubemain_average_register
    procedure, public :: main     => cubemain_average_main
  end type average_comm_t
  !
  integer(kind=4), parameter :: ione = 1
  integer(kind=4), parameter :: itwo = 2
  type average_user_t
    ! Data
    integer(kind=code_k) :: data_mode  ! Where data cubes are to be found?
    type(index_t)        :: datind     ! Data cubes index, if relevant
    ! Weight and/or noise for relative weighting
    integer(kind=code_k) :: weight_mode = code_user_weight_none  ! Where noise/weights are to be found?
    real(kind=sign_k)    :: weival(2)  ! Weight values, if relevant (/WEIGHT)
    type(index_t)        :: noiind     ! Noise cubes index, if relevant (/NOISE)
    ! Misc
    type(identifier_user_t) :: family
  contains
    procedure, private :: toprog => cubemain_average_user_toprog
  end type average_user_t
  !
  type average_prog_t
     type(identifier_prog_t)        :: family
     type(index_t)                  :: incubes
     type(cube_t), pointer          :: averaged
     type(cube_t), pointer          :: weight
     ! Weight and/or noise for relative weighting
     integer(kind=code_k)           :: weight_mode
     real(kind=sign_k), allocatable :: weival(:)  ! Weight values if relevant
     type(index_t)                  :: weicubes   ! Index of weight or noise cubes if relevant
   contains
     procedure, private :: header            => cubemain_average_prog_header
     procedure, private :: consistency       => cubemain_average_prog_consistency
     procedure, private :: data              => cubemain_average_prog_data
     procedure, private :: loop              => cubemain_average_prog_loop
     procedure, private :: act               => cubemain_average_prog_act
     procedure, private :: normalize_and_put => cubemain_average_prog_normalize_and_put
  end type average_prog_t
  !
contains
  !
  !---------------------------------------------------------------------
  !
  subroutine cubemain_average_register(average,ouflag,error)
    use cubedag_allflags
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(average_comm_t), intent(inout) :: average
    type(flag_t),          intent(in)    :: ouflag
    logical,               intent(inout) :: error
    !
    type(cube_prod_t) :: oucube
    character(len=*), parameter :: rname='AVERAGE>REGISTER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    ! Register products
    call oucube%register(&
         'AVERAGE',&
         'The average cube',&
         strg_id,&
         [ouflag,flag_cube],&
         average%averaged,&
         error)
    if (error) return
    call oucube%register(&
         'WEIGHT',&
         'The weight cube',&
         strg_id,&
         [ouflag,flag_weight],&
         average%weightc,&
         error)
    if (error) return
  end subroutine cubemain_average_register
  !
  subroutine cubemain_average_main(average,user,error)
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(average_comm_t), intent(in)    :: average
    type(average_user_t),  intent(inout) :: user
    logical,               intent(inout) :: error
    !
    type(average_prog_t) :: prog
    character(len=*), parameter :: rname='AVERAGE>MAIN'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call user%toprog(prog,error)
    if (error) return
    call prog%header(average,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubemain_average_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_average_user_toprog(user,prog,error)
    use gkernel_interfaces
    use cubedag_allflags
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(average_user_t), intent(in)    :: user
    type(average_prog_t),  intent(out)   :: prog
    logical,               intent(inout) :: error
    !
    integer(kind=4) :: icub,ier
    type(cube_t), pointer :: pcub
    type(index_t) :: allindex
    character(len=*), parameter :: rname='AVERAGE>USER>TOPROG'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    ! --- Collect the cubes to be averaged ---
    select case(user%data_mode)
    case(code_user_index_cx)
       call allindex%get_from_current(code_access_imaset,code_read,error)
       if (error)  return
    case(code_user_index_custom)
       ! This is the case where an index is provided by a command
       ! editing the user type.
       call user%datind%copy(allindex,error)
       if (error) return
       ! Here we guarantee that the cubes given by the command are
       ! indeed in the good access
       do icub=1,allindex%n
          pcub => allindex%get_cube(icub,error)
          if (error) return
          call cubeadm_access_header(pcub,code_access_imaset,code_read,error)
          if (error) return
       enddo
    case default
       call cubemain_message(seve%e,rname,'Unrecognized index code')
       error = .true.
       return
    end select
    !
    ! --- Split the cubes between signal and weigths/noises if relevant ---
    if (user%weight_mode.eq.code_user_weight_datweiind) then
      call cubemain_average_user_toprog_splitindex(allindex,  &
        flag_weight,prog%incubes,prog%weicubes,error)
      if (error)  return
    elseif (user%weight_mode.eq.code_user_weight_datnoiind) then
      call cubemain_average_user_toprog_splitindex(allindex,  &
        flag_noise,prog%incubes,prog%weicubes,error)
      if (error)  return
    else  ! No split, all are signal cubes
      call allindex%copy(prog%incubes,error)
      if (error) return
    endif
    !
    ! --- Fetch noise cubes from command line if relevant ---
    if (user%weight_mode.eq.code_user_weight_noiind .or.  &
        user%weight_mode.eq.code_user_weight_weival_noiind) then
      call user%noiind%copy(prog%weicubes,error)
      if (error) return
    endif
    !
    ! --- Fetch weight values from command line if relevant ---
    allocate(prog%weival(prog%incubes%n),stat=ier)
    if (failed_allocate(rname,'weight array',ier,error)) return
    if (user%weight_mode.eq.code_user_weight_weival .or.  &
        user%weight_mode.eq.code_user_weight_weival_noiind) then
      prog%weival(:) = user%weival(:)
    else
      prog%weival(:) = 1.0
    endif
    !
    ! --- Weight mode ---
    select case (user%weight_mode)
    case (code_user_weight_weival)
       prog%weight_mode = code_prog_weight_none
    case (code_user_weight_weival_noiind)
       prog%weight_mode = code_prog_weight_noiind
    case (code_user_weight_noiind)
       prog%weight_mode = code_prog_weight_noiind
    case (code_user_weight_datnoiind)
       prog%weight_mode = code_prog_weight_noiind
    case (code_user_weight_datweiind)
       prog%weight_mode = code_prog_weight_weiind
    case (code_user_weight_none)
       prog%weight_mode = code_prog_weight_none
    case default
       call cubemain_message(seve%e,rname,'Unexpected weight mode')
       error = .true.
       return
    end select
    !
    pcub => prog%incubes%get_cube(ione,error)
    if (error) return
    call user%family%toprog(pcub,prog%family,error)
    if (error) return
  end subroutine cubemain_average_user_toprog
  !
  subroutine cubemain_average_user_toprog_splitindex(in,flag,oud,ouw,error)
    use cubetools_header_methods
    use cubedag_flag
    !-------------------------------------------------------------------
    ! Try to split the input index into a cube index and a weight index,
    ! correctly ordered for proper pairing.
    !-------------------------------------------------------------------
    type(index_t), intent(in)    :: in
    type(flag_t),  intent(in)    :: flag
    type(index_t), intent(inout) :: oud
    type(index_t), intent(inout) :: ouw
    logical,       intent(inout) :: error
    !
    integer(kind=4) :: icub,ndata,nwei
    type(cube_t), pointer :: pcub
    type(index_t) :: tmpw
    character(len=mess_l) :: mess
    character(len=dag_flagl) :: flag_name
    logical :: chanerror
    integer(kind=chan_k) :: nchan
    character(len=*), parameter :: rname='AVERAGE>USER>TOPROG>SPLIT>INDEX'
    !
    flag_name = flag%get_name()
    !
    ! --- Split the cubes based on their flags ---
    ndata = 0
    nwei = 0
    do icub=1,in%n
      pcub => in%get_cube(icub,error)
      if (error) return
      if (pcub%node%flag%contains(flag)) then
        nwei = nwei+1
        call tmpw%put_cube(nwei,pcub,error)
        if (error)  return
      else
        ndata = ndata+1
        call oud%put_cube(ndata,pcub,error)
        if (error)  return
      endif
    enddo
    write(mess,'(a,i0,a,i0,3a)')  'Split index into two sub-indexes (',  &
      ndata,' data cubes and ',nwei,' ',trim(flag_name),' cubes)'
    call cubemain_message(seve%i,rname,mess)
    !
    ! --- Pair the cubes based on their family name ---
    call cubemain_average_user_toprog_pairindex(oud,tmpw,trim(flag_name),ouw,error)
    if (error)  return
    !
    ! --- Sanity number of channels ---
    chanerror = .false.
    do icub=1,ouw%n
      pcub => ouw%get_cube(icub,error)
      if (error) return
      call cubetools_header_get_nchan(pcub%head,nchan,error)
      if (error) return
      if (nchan.gt.1) then
        write(mess,'(a,a,i0,a)')  &
          trim(flag_name),' cube #',icub,' has more than one channel, not supported'
        call cubemain_message(seve%e,rname,mess)
        chanerror = .true.
      endif
    enddo
    if (chanerror)  error = .true.
    if (error)  return
  end subroutine cubemain_average_user_toprog_splitindex
  !
  subroutine cubemain_average_user_toprog_pairindex(oud,tmpw,what,ouw,error)
    !-------------------------------------------------------------------
    ! Re-order the weight index so that it matches the data index.
    !-------------------------------------------------------------------
    type(index_t),    intent(in)    :: oud   ! Data index
    type(index_t),    intent(in)    :: tmpw  ! Temporary weight index
    character(len=*), intent(in)    :: what  !
    type(index_t),    intent(inout) :: ouw   ! Weight index
    logical,          intent(inout) :: error
    !
    integer(kind=4) :: icub,jcub
    type(cube_t), pointer :: cub1,cub2
    character(len=mess_l) :: mess
    character(len=128) :: dataid,weiid
    character(len=*), parameter :: rname='AVERAGE>USER>TOPROG>PAIR>INDEX'
    !
    ! Number of cubes
    if (oud%n.ne.tmpw%n) then
      call cubemain_message(seve%e,rname,  &
        'Number of data and '//what//' cubes mismatch')
      error = .true.
      return
    endif
    !
    ! Perform the pairing between data and noise/weight cubes:
    ! tmpw is unordered, ouw will be ordered.
    !
    ! ZZZ: this is a N^2 loop: to be fixed with a dichotomic search in
    !      sorted indexes
main: do icub=1,oud%n
      cub1 => oud%get_cube(icub,error)
      if (error) return
      !
      ! 1) Data cubes must have unambiguous family names
      do jcub=icub+1,oud%n
        cub2 => oud%get_cube(jcub,error)
        if (error) return
        if (cub1%node%family.eq.cub2%node%family) then
          write(mess,'(3(a,i0))')  &
            'Cubes #',icub,' and #',jcub,' have same family name'
          call cubemain_message(seve%e,rname,mess)
          call cubemain_message(seve%e,rname,  &
            'Pairing data and '//what//' cubes is ambiguous')
          error = .true.
          return
        endif
      enddo
      !
      ! 2) Find its noise/weight cube
      do jcub=1,tmpw%n
        cub2 => tmpw%get_cube(jcub,error)
        if (error) return
        if (cub1%node%family.eq.cub2%node%family) then
          ! That's a match
          call ouw%put_cube(icub,cub2,error)
          if (error)  return
          ! Feedback
          dataid = cub1%node%cubeid(error)
          if (error)  return
          weiid = cub2%node%cubeid(error)
          if (error)  return
          write(mess,'(a,i0,4a)') '  #',icub,': ',trim(dataid),' + ',trim(weiid)
          call cubemain_message(seve%i,rname,mess)
          ! Done
          cycle main
        endif
      enddo
      !
      ! No match
      write(mess,'(a,i0,4a)')  'Cube #',icub,' has no associated ', &
        what,' cube with family name ',cub1%node%family
      call cubemain_message(seve%e,rname,mess)
      error = .true.
      return
    enddo main
  end subroutine cubemain_average_user_toprog_pairindex
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_average_prog_header(prog,comm,error)
    use cubetools_header_methods
    use cubeadm_clone
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(average_prog_t), intent(inout) :: prog
    type(average_comm_t),  intent(in)    :: comm
    logical,               intent(inout) :: error
    !
    type(cube_t), pointer :: pcub
    integer(kind=4) :: icub
    character(len=*), parameter :: rname='AVERAGE>PROG>HEADER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call prog%consistency(error)
    if (error) return
    pcub => prog%incubes%get_cube(ione,error)
    if (error) return
    call cubeadm_clone_header(comm%averaged,pcub,prog%averaged,error)
    if (error) return
    call cubeadm_clone_header(comm%weightc,pcub,prog%weight,error)
    if (error) return
    !
    call prog%family%apply(prog%averaged,error)
    if (error) return
    call prog%family%apply(prog%weight,error)
    if (error) return
    !
    do icub=2,prog%incubes%n
       pcub => prog%incubes%get_cube(icub,error)
       if (error) return
       call cubetools_header_add_observatories(pcub%head,prog%averaged%head,error)
       if (error) return
       call cubetools_header_add_observatories(pcub%head,prog%weight%head,error)
       if (error) return
    enddo
    call cubetools_header_put_array_unit('---', prog%weight%head,error)
    if (error) return
  end subroutine cubemain_average_prog_header
  !
  subroutine cubemain_average_prog_consistency(prog,error)
    use cubeadm_consistency
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(average_prog_t), intent(inout) :: prog
    logical,               intent(inout) :: error
    !
    type(consistency_t) :: cons
    character(len=16) :: cubnam, noinam
    type(cube_t), pointer :: pcub, pnoi
    integer(kind=4) :: icub
    character(len=*), parameter :: rname='AVERAGE>PROG>CONSISTENCY'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cons%grid(prog%incubes,error)
    if (error) return
    !
    if (prog%weight_mode.eq.code_prog_weight_noiind .or.  &
        prog%weight_mode.eq.code_prog_weight_weiind) then
       do icub=1,prog%incubes%n
          pcub => prog%incubes%get_cube(icub,error)
          if (error) return
          pnoi => prog%weicubes%get_cube(icub,error)
          if (error) return
          write(cubnam,'(a,i0)') 'Cube #',icub
          write(noinam,'(a,i0)') 'Noise #',icub
          call cons%image(cubnam,pcub,noinam,pnoi,error)
          if (error) return
       enddo
    endif
    !
    call cons%proceed(error)
    if (error) return
  end subroutine cubemain_average_prog_consistency
  !
  subroutine cubemain_average_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(average_prog_t), intent(inout) :: prog
    logical,               intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='AVERAGE>PROG>DATA'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubemain_average_prog_data
  !
  subroutine cubemain_average_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(average_prog_t),    intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(image_t) :: average,weight
    character(len=*), parameter :: rname='AVERAGE>NOISE>LOOP'
    !
    call average%allocate('average',prog%averaged,iter,error)
    if (error) return
    call weight%allocate('weight',prog%weight,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter,iter%ie,average,weight,error)
      if (error) return
    enddo ! ie
  end subroutine cubemain_average_prog_loop
  !
  subroutine cubemain_average_prog_act(prog,iter,ie,average,weight,error)
    use cubetools_nan
    use cubeadm_taskloop
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(average_prog_t),    intent(inout) :: prog
    type(cubeadm_iterator_t), intent(in)    :: iter
    integer(kind=entr_k),     intent(in)    :: ie
    type(image_t),            intent(inout) :: average
    type(image_t),            intent(inout) :: weight
    logical,                  intent(inout) :: error
    !
    integer(kind=pixe_k) :: ix,iy
    integer(kind=4) :: icub
    type(image_t) :: inima,noise,weiima
    type(cube_t), pointer :: pcub,pnoi
    character(len=*), parameter :: rname='AVERAGE>PROG>ACT>NOISE'
    !
    average%val(:,:) = 0
    weight%val(:,:) = 0
    do icub=1,prog%incubes%n
       ! --- Data ---
       pcub => prog%incubes%get_cube(icub,error)
       if (error) return
       call inima%associate('input',pcub,iter,error)
       if (error) return
       call inima%get(ie,error)
       if (error) return
       ! --- Weight ---
       if (icub.eq.1) then  ! Same for all cubes
         call weiima%allocate('weight image',pcub,error)
         if (error)  return
       endif
       ! Note: weival always used (might be 1)
       select case (prog%weight_mode)
       case (code_prog_weight_none)
         weiima%val(:,:) = prog%weival(icub)
       case (code_prog_weight_noiind)
         ! A noise cube is provided
         pnoi => prog%weicubes%get_cube(icub,error)
         if (error) return
         call noise%associate('noise',pnoi,iter,error)
         if (error) return
         call noise%get(ie,error)  ! Note: noise cube can have 1 or N planes, this works correctly
         if (error) return
         weiima%val(:,:) = prog%weival(icub)/noise%val(:,:)**2
       case (code_prog_weight_weiind)
         ! A weight cube is provided
         pnoi => prog%weicubes%get_cube(icub,error)
         if (error) return
         call noise%associate('noise',pnoi,iter,error)
         if (error) return
         call noise%get(ie,error)  ! Note: noise cube can have 1 or N planes, this works correctly
         if (error) return
         weiima%val(:,:) = prog%weival(icub)*noise%val(:,:)
       end select
       ! --- Combine ---
       do iy=1,inima%ny
          do ix=1,inima%nx
             if (.not.ieee_is_nan(inima%val(ix,iy)).and..not.ieee_is_nan(weiima%val(ix,iy))) then
                average%val(ix,iy) = average%val(ix,iy) + inima%val(ix,iy)*weiima%val(ix,iy)
                weight%val(ix,iy)  = weight%val(ix,iy)  + weiima%val(ix,iy)
             endif
          enddo ! ix
       enddo ! iy
    enddo ! icub
    call weiima%free()
    call prog%normalize_and_put(ie,average,weight,error)
    if (error) return
  end subroutine cubemain_average_prog_act
  !
  subroutine cubemain_average_prog_normalize_and_put(prog,ie,average,weight,error)
    use cubetools_nan
    use cubeadm_image_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(average_prog_t), intent(inout) :: prog
    integer(kind=entr_k),  intent(in)    :: ie
    type(image_t),         intent(inout) :: average
    type(image_t),         intent(inout) :: weight
    logical,               intent(inout) :: error
    !
    integer(kind=pixe_k) :: ix,iy
    character(len=*), parameter :: rname='AVERAGE>PROG>NORMALIZE>AND>PUT'
    !
    do iy=1,average%ny
       do ix=1,average%nx
          if (weight%val(ix,iy).eq.0) then
             average%val(ix,iy) = gr4nan
          else
             average%val(ix,iy) = average%val(ix,iy)/weight%val(ix,iy)
          endif
       enddo
    enddo
    call average%put(ie,error)
    if (error) return
    call weight%put(ie,error)
    if (error) return
  end subroutine cubemain_average_prog_normalize_and_put
  !
end module cubemain_average_tool
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
