!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_kernel
  use cubetools_parameters
  use cubemain_messaging
  !
  public :: kernel_t
  public :: cubemain_kernel_allocate,cubemain_kernel_free
  private
  !
  type kernel_t
     real(kind=coor_k) :: inc  = 0d0 ! [rad] 
     real(kind=coor_k) :: val  = 0d0 ! [rad] 
     real(kind=coor_k) :: ref  = 0d0 ! [rad] 
     real(kind=coor_k) :: zero = 0d0 ! [rad] i0 = ref - val/inc
     real(kind=coor_k) :: fac  = 0d0 ! [rad] 1/inc
     real(kind=coor_k) :: min  = 0d0 ! [rad] 
     real(kind=coor_k) :: max  = 0d0 ! [rad] 
     real(kind=sign_k),   pointer :: fn(:) => null()
     integer(kind=chan_k) :: n = 0
  end type kernel_t
  !
contains
  !
  subroutine cubemain_kernel_allocate(n,kern,error)
    use gkernel_interfaces
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    integer(kind=index_length), intent(in)    :: n
    type(kernel_t),             intent(inout) :: kern
    logical,                    intent(inout) :: error
    !
    integer(kind=4) :: ier
    character(len=*), parameter :: rname='KERNEL>ALLOCATE'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    ! Sanity check
    if (n.le.0) then
       call cubemain_message(seve%e,rname,'Negative or zero number of elements')
       error = .true.
       return
    endif
    ! Allocate
    allocate(kern%fn(n),stat=ier)
    if (failed_allocate(rname,'kernel',ier,error)) return
    ! Allocation success => kern%n may be updated
    kern%n = n
  end subroutine cubemain_kernel_allocate
  !
  subroutine cubemain_kernel_free(kern,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    type(kernel_t), intent(inout) :: kern
    logical,        intent(inout) :: error
    !
    character(len=*), parameter :: rname='KERNEL>FREE'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    kern%zero = 0
    kern%fac = 0
    kern%min = 0
    kern%max = 0
    kern%n = 0
    if (associated(kern%fn)) deallocate(kern%fn)
  end subroutine cubemain_kernel_free
end module cubemain_kernel
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
