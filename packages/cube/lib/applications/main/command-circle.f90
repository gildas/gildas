!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! CIRCLE is not a standard case: while it iterates the input cube with
! images, it creates an output cube (which has only 1 plane) with 1 spatial
! axis and 1 spectral axis (no 2nd spatial axis). This has consequences:
!  - header: the output header has IX=spatial axis and IY=spectral axis.
!    This way the io engines and subsequent commands like LOAD know how to
!    deal with this cube.
!  - data: the output "cube" (single plane) is fully filled after all the
!    input planes are iterated. This means fullset access: 1 allocation for
!    the whole output plane, fill it during the input plane iterations
!    (parallelization is allowed), write it once at the end.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_circle
  use cubetools_parameters
  use cube_types
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubeadm_image_types
  use cubemain_messaging
  use cubetopology_spapos_types
  use cubemain_spaelli_types
  !
  public :: circle
  private
  !
  type :: circle_comm_t
     type(option_t),     pointer :: comm
     type(cubeid_arg_t), pointer :: cube
     type(spapos_comm_t)         :: center
     type(ellipse_opt_t)         :: ellipse
     type(cube_prod_t),  pointer :: npix     ! # of pix in each radius
     type(cube_prod_t),  pointer :: summ     ! Sum
     type(cube_prod_t),  pointer :: aver     ! Average
     type(cube_prod_t),  pointer :: sigm     ! Sigma
     type(cube_prod_t),  pointer :: mini     ! Minimum
     type(cube_prod_t),  pointer :: maxi     ! Maximum
   contains
     procedure, public  :: register => cubemain_circle_register
     procedure, private :: parse    => cubemain_circle_parse
     procedure, private :: main     => cubemain_circle_main
  end type circle_comm_t
  type(circle_comm_t) :: circle
  !
  type circle_user_t
     type(cubeid_user_t)   :: cubeids
     type(spapos_user_t)   :: center   ! [Absolute|relative]
     type(ellipse_user_t)  :: ellipse
   contains
     procedure, private :: toprog => cubemain_circle_user_toprog
  end type circle_user_t
  !
  type circle_prog_t
     logical               :: doellipse
     type(ellipse_prog_t)  :: ellipse     ! ellipse
     type(spapos_prog_t)   :: center      ! ellipse center
     type(image_t)         :: idx         ! Output indices
     !
     type(cube_t), pointer :: cube        ! Input cube
     type(cube_t), pointer :: npix        ! # of pix in each radius
     type(cube_t), pointer :: summ        ! Sum
     type(cube_t), pointer :: aver        ! Average
     type(cube_t), pointer :: sigm        ! Sigma 
     type(cube_t), pointer :: mini        ! Minimum
     type(cube_t), pointer :: maxi        ! Maximum
   contains
     procedure, private :: header => cubemain_circle_prog_header
     procedure, private :: data   => cubemain_circle_prog_data
     procedure, private :: loop   => cubemain_circle_prog_loop
     procedure, private :: act    => cubemain_circle_prog_act
  end type circle_prog_t
  !
contains
  !
  subroutine cubemain_circle_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(circle_user_t) :: user
    character(len=*), parameter :: rname='CIRCLE>COMMAND'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call circle%parse(line,user,error)
    if (error) return
    call circle%main(user,error)
    if (error) continue
  end subroutine cubemain_circle_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_circle_register(circle,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(circle_comm_t), intent(inout) :: circle
    logical,              intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    type(cube_prod_t) :: oucube
    character(len=*), parameter :: comm_abstract = &
         'Average intensities of a cube along ellipses'
    character(len=*), parameter :: comm_help = &
         'By default the averaging is circular and centered at the&
         & projection center. This can be changed by using options&
         & /CENTER and /ELLIPSE. The axes given to option /ELLIPSE&
         & will be used to derive the aspect ratio over which to&
         & compute the azimuthal average.'
    character(len=*), parameter :: rname='CIRCLE>REGISTER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'CIRCLE','[cube]',&
         comm_abstract,&
         comm_help,&
         cubemain_circle_command,&
         circle%comm,error)
    if (error) return
    call cubearg%register( &
         'CUBE', &
         'Signal cube',  &
         strg_id,&
         code_arg_optional,  &
         [flag_cube], &
         code_read, &
         code_access_imaset, &
         circle%cube, &
         error)
    if (error) return
    !
    call circle%center%register('CENTER',&
         'Center of the ellipse',&
         error)
    if (error) return
    !
    call circle%ellipse%register(&
         'ELLIPSE',&
         'Define ellipse axes and position angle',&
         error)
    if (error) return
    !
    ! Products
    call oucube%register(&
         'NPIX',&
         'Cube of number of pixels',&
         strg_id,&
         [flag_circle,flag_npix],&
         circle%npix,&
         error,&
         access=code_access_fullset)
    if (error) return
    call oucube%register(&
         'SUM',&
         'Cube of sums',&
         strg_id,&
         [flag_circle,flag_sum],&
         circle%summ,&
         error,&
         access=code_access_fullset)
    if (error) return
    call oucube%register(&
         'AVERAGE',&
         'Cube of averages',&
         strg_id,&
         [flag_circle,flag_average],&
         circle%aver,&
         error,&
         access=code_access_fullset)
    if (error) return
    call oucube%register(&
         'SIGMA',&
         'Cube of noises',&
         strg_id,&
         [flag_circle,flag_noise],&
         circle%sigm,&
         error,&
         access=code_access_fullset)
    if (error) return
    call oucube%register(&
         'MINIMUM',&
         'Cube of minima',&
         strg_id,&
         [flag_circle,flag_minimum],&
         circle%mini,&
         error,&
         access=code_access_fullset)
    if (error) return
    call oucube%register(&
         'MAXIMUM',&
         'Cube of maxima',&
         strg_id,&
         [flag_circle,flag_maximum],&
         circle%maxi,&
         error,&
         access=code_access_fullset)
    if (error) return
  end subroutine cubemain_circle_register
  !
  subroutine cubemain_circle_parse(circle,line,user,error)
    !----------------------------------------------------------------------
    ! CIRCLE cubname
    ! /CENTER xcenter ycenter
    ! /ELLIPSE major [minor [pa]]
    !----------------------------------------------------------------------
    class(circle_comm_t), intent(in)    :: circle
    character(len=*),     intent(in)    :: line
    type(circle_user_t),  intent(out)   :: user
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='CIRCLE>PARSE'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,circle%comm,user%cubeids,error)
    if (error) return
    call circle%center%parse(line,user%center,error)
    if (error) return
    call circle%ellipse%parse(line,user%ellipse,error)
    if (error) return
  end subroutine cubemain_circle_parse
  !
  subroutine cubemain_circle_main(circle,user,error)
    use cubedag_parameters
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(circle_comm_t), intent(in)    :: circle
    type(circle_user_t),  intent(in)    :: user
    logical,              intent(inout) :: error
    !
    type(circle_prog_t) :: prog
    character(len=*), parameter :: rname='CIRCLE>MAIN'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call user%toprog(prog,error)
    if (error) return
    call prog%header(circle,error)
    if (error) return
    !
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubemain_circle_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_circle_user_toprog(user,prog,error)
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(circle_user_t), intent(in)    :: user
    type(circle_prog_t),  intent(out)   :: prog
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='CIRCLE>USER>TOPROG'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(circle%cube,user%cubeids,prog%cube,error)
    if (error) return
    !
    call user%center%toprog(prog%cube,prog%center,error)
    if (error) return
    prog%doellipse = user%ellipse%do
    if (prog%doellipse) then
       call user%ellipse%toprog(prog%cube,prog%ellipse,error)
       if (error) return
    endif
  end subroutine cubemain_circle_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_circle_prog_header(prog,comm,error)
    use cubetools_axis_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(circle_prog_t), intent(inout) :: prog
    type(circle_comm_t),  intent(in)    :: comm
    logical,              intent(inout) :: error
    !
    type(axis_t) :: ouaxis
    character(len=*), parameter :: rname='CIRCLE>PROG>HEADER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call compute_indices(prog%cube,prog%idx,ouaxis,error)
    if (error) return
    call header_one(ouaxis,comm%npix,prog%cube,prog%npix,error)
    if (error) return
    call header_one(ouaxis,comm%summ,prog%cube,prog%summ,error)
    if (error) return
    call header_one(ouaxis,comm%aver,prog%cube,prog%aver,error)
    if (error) return
    call header_one(ouaxis,comm%sigm,prog%cube,prog%sigm,error)
    if (error) return
    call header_one(ouaxis,comm%mini,prog%cube,prog%mini,error)
    if (error) return
    call header_one(ouaxis,comm%maxi,prog%cube,prog%maxi,error)
    if (error) return
    !
  contains
    !
    subroutine compute_indices(incube,ouidx,ouaxis,error)
      use cubetools_header_methods
      !----------------------------------------------------------------------
      !
      !----------------------------------------------------------------------
      type(cube_t),  intent(in)    :: incube
      type(image_t), intent(inout) :: ouidx
      type(axis_t),  intent(inout) :: ouaxis
      logical,       intent(inout) :: error
      !
      integer(kind=pixe_k) :: ix,iy,ir
      real(coor_k) :: xval,yval,cval,rval
      real(coor_k) :: excent,theta
      type(axis_t) :: xaxis,yaxis
      !
      call cubetools_header_get_axis_head_l(incube%head,xaxis,error)
      if (error) return
      xaxis%val = -prog%center%rela(1)
      !
      call cubetools_header_get_axis_head_m(incube%head,yaxis,error)
      if (error) return
      yaxis%val = -prog%center%rela(2)
      !
      if (prog%doellipse) then
         excent = sqrt(prog%ellipse%major**2-prog%ellipse%minor**2)/prog%ellipse%major
      else
         excent = 0.0
      endif
      !
      ! The region (or absence of region in this case) must be declared
      ! before trying to allocate an image mapping an input cube out of
      ! the main data loop:
      call prog%cube%default_region(error)
      if (error) return
      call ouidx%allocate('ouidx',prog%cube,error)
      if (error) return
      !
      call cubetools_axis_init(ouaxis,error)
      if (error) return
      ouaxis%ref = 1d0
      ouaxis%val = 0d0
      ouaxis%inc = sqrt(abs(xaxis%inc*yaxis%inc))
      ouaxis%n = 1
      do iy=1,ouidx%ny
         yval = (iy-yaxis%ref)*yaxis%inc+yaxis%val
         do ix=1,ouidx%nx
            xval = (ix-xaxis%ref)*xaxis%inc+xaxis%val
            rval = sqrt(xval**2+yval**2)
            if (rval.ne.0d0) then
               theta = atan2(yval/rval,xval/rval)
            else
               theta = 0d0
            endif
            theta = theta-prog%ellipse%pang
            cval = sqrt( rval**2 * (1-(excent*cos(theta))**2)/(1-excent**2) )
            ir = nint(cval/ouaxis%inc)+1 ! ir = 1 when cval = 0
            ouaxis%n = max(ouaxis%n,ir)
            ouidx%val(ix,iy) = ir
         enddo ! ix
      enddo ! iy
    end subroutine compute_indices
    !
    subroutine header_one(ouaxis,oucomm,incube,oucube,error)
      use cubetools_header_methods
      use cubeadm_clone
      !----------------------------------------------------------------------
      !
      !----------------------------------------------------------------------
      type(axis_t),          intent(in)    :: ouaxis
      type(cube_prod_t),     intent(in)    :: oucomm
      type(cube_t),          intent(in)    :: incube
      type(cube_t), pointer, intent(inout) :: oucube
      logical,               intent(inout) :: error
      !
      type(axis_t) :: axis
      !
      call cubeadm_clone_header(oucomm,incube,oucube,error)
      if (error) return
      ! Spectral axis becomes IY
      call cubetools_header_get_axis_head_c(oucube%head,axis,error)
      if (error) return
      call cubetools_header_update_axset_m(axis,oucube%head,error)
      if (error) return
      ! Nullify IC
      call cubetools_header_nullify_axset_c(oucube%head,error)
      if (error) return
      ! Recompute IX spatial axis
      call cubetools_header_get_axis_head_l(oucube%head,axis,error)
      if (error) return
      axis%n   = ouaxis%n
      axis%ref = ouaxis%ref
      axis%val = ouaxis%val
      axis%inc = ouaxis%inc
      call cubetools_header_update_axset_l(axis,oucube%head,error)
      if (error) return
    end subroutine header_one
  end subroutine cubemain_circle_prog_header
  !
  subroutine cubemain_circle_prog_data(prog,error)
    use cubeadm_opened
    use cubeadm_fullcube_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(circle_prog_t), intent(inout) :: prog
    logical,              intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    type(fullcube_t) :: npix,summ,aver,sigm,mini,maxi
    character(len=*), parameter :: rname='CIRCLE>PROG>DATA'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,error)
    if (error) return
    call npix%allocate('npix',prog%npix,error)
    if (error) return
    call summ%allocate('summ',prog%summ,error)
    if (error) return
    call aver%allocate('aver',prog%aver,error)
    if (error) return
    call sigm%allocate('sigm',prog%sigm,error)
    if (error) return
    call mini%allocate('mini',prog%mini,error)
    if (error) return
    call maxi%allocate('maxi',prog%maxi,error)
    if (error) return
    mini%val =  huge(mini%val)
    maxi%val = -huge(maxi%val)
    summ%val = 0e0
    npix%val = 0e0
    !
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,npix,summ,aver,sigm,mini,maxi,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,npix,summ,aver,sigm,mini,maxi,error)
       !$OMP END TASK
    enddo ! iter
    !$OMP END SINGLE
    !$OMP END PARALLEL
    !
    call npix%put(error)
    if (error) return
    call summ%put(error)
    if (error) return
    call aver%put(error)
    if (error) return
    call sigm%put(error)
    if (error) return
    call mini%put(error)
    if (error) return
    call maxi%put(error)
    if (error) return
  end subroutine cubemain_circle_prog_data
  !
  subroutine cubemain_circle_prog_loop(prog,iter,npix,summ,aver,sigm,mini,maxi,error)
    use cubeadm_taskloop
    use cubeadm_image_types
    use cubeadm_fullcube_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(circle_prog_t),     intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    type(fullcube_t),         intent(inout) :: npix
    type(fullcube_t),         intent(inout) :: summ
    type(fullcube_t),         intent(inout) :: aver
    type(fullcube_t),         intent(inout) :: sigm
    type(fullcube_t),         intent(inout) :: mini
    type(fullcube_t),         intent(inout) :: maxi
    logical,                  intent(inout) :: error
    !
    type(image_t) :: ima
    character(len=*), parameter :: rname='CIRCLE>PROG>LOOP'
    !
    call ima%associate('ima',prog%cube,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter%ie,ima,npix,summ,aver,sigm,mini,maxi,error)
      if (error) return
    enddo ! ie
  end subroutine cubemain_circle_prog_loop
  !
  subroutine cubemain_circle_prog_act(prog,ie,ima,npix,summ,aver,sigm,mini,maxi,error)
    use cubetools_nan
    use cubeadm_image_types
    use cubeadm_fullcube_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(circle_prog_t), intent(inout) :: prog
    integer(kind=entr_k), intent(in)    :: ie
    type(image_t),        intent(inout) :: ima
    type(fullcube_t),     intent(inout) :: npix
    type(fullcube_t),     intent(inout) :: summ
    type(fullcube_t),     intent(inout) :: aver
    type(fullcube_t),     intent(inout) :: sigm
    type(fullcube_t),     intent(inout) :: mini
    type(fullcube_t),     intent(inout) :: maxi
    logical,              intent(inout) :: error
    !
    integer(kind=pixe_k) :: ix,iy,ir,nr
    integer(kind=pixe_k), parameter :: one=1
    character(len=*), parameter :: rname='CIRCLE>PROG>ACT'
    !
    nr = npix%nx
    !
    call ima%get(ie,error)
    if (error) return
    !
    do iy=1,ima%ny
       do ix=1,ima%nx
          ir = prog%idx%val(ix,iy)
          if (.not.ieee_is_nan(ima%val(ix,iy))) then
             mini%val(ir,ie,one) = min(mini%val(ir,ie,one),ima%val(ix,iy))
             maxi%val(ir,ie,one) = max(maxi%val(ir,ie,one),ima%val(ix,iy))
             summ%val(ir,ie,one) = summ%val(ir,ie,one)+ima%val(ix,iy)
             npix%val(ir,ie,one) = npix%val(ir,ie,one)+1.0
          endif
       enddo ! ix
    enddo ! iy
    !
    do ir=1,nr
       if (npix%val(ir,ie,one).ge.1.0) then
          aver%val(ir,ie,one) = summ%val(ir,ie,one)/npix%val(ir,ie,one)
       else
          aver%val(ir,ie,one) = 0
          mini%val(ir,ie,one) = 0
          maxi%val(ir,ie,one) = 0
       endif
    enddo ! ir
    !
    sigm%val = 0d0
    do iy=1,ima%ny
       do ix=1,ima%nx
          ir = prog%idx%val(ix,iy)
          if (.not.ieee_is_nan(ima%val(ix,iy))) then
             sigm%val(ir,ie,one) = sigm%val(ir,ie,one)+(ima%val(ix,iy)-aver%val(ir,ie,one))**2
          endif
       enddo ! ix
    enddo ! iy
    !
    do ir=1,nr
       if (npix%val(ir,ie,one).gt.1.0) then
          sigm%val(ir,ie,one) = sqrt(sigm%val(ir,ie,one)/(npix%val(ir,ie,one)-1.0))
       else
          sigm%val(ir,ie,one) = 0
       endif
    enddo ! ir
  end subroutine cubemain_circle_prog_act
end module cubemain_circle
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
