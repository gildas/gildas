!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_fill
  use cubetools_parameters
  use cubetools_structure
  use cube_types
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubemain_messaging
  use cubetopology_sperange_types
  use cubetopology_sparange_types
  use cubetopology_spapos_types
  use cubetopology_spasize_types
  !
  public :: fill
  private
  !
  type :: fill_comm_t
     type(option_t),     pointer :: comm
     type(cubeid_arg_t), pointer :: cube
     type(sperange_opt_t)        :: range
     type(spapos_comm_t)         :: center
     type(spasize_opt_t)         :: size
     type(cube_prod_t),  pointer :: filled
   contains
     procedure, public  :: register => cubemain_fill_register
     procedure, private :: parse    => cubemain_fill_parse
     procedure, private :: main     => cubemain_fill_main
  end type fill_comm_t
  type(fill_comm_t) :: fill
  !
  type fill_user_t
     type(cubeid_user_t)  :: cubeids
     type(sperange_user_t):: range   ! Spectral range to be filled
     type(spapos_user_t)  :: center  ! Center of the region to be filled
     type(spasize_user_t) :: size    ! Size of the region to be filled
   contains
     procedure, private :: toprog => cubemain_fill_user_toprog
  end type fill_user_t
  !
  type fill_prog_t
     type(cube_t), pointer :: cube
     type(cube_t), pointer :: filled
     type(sperange_user_t) :: sperange
     integer(kind=indx_k)  :: imin(3)
     integer(kind=indx_k)  :: imax(3)
   contains
     procedure, private :: header => cubemain_fill_prog_header
     procedure, private :: data   => cubemain_fill_prog_data     
     procedure, private :: loop   => cubemain_fill_prog_loop
     procedure, private :: act    => cubemain_fill_prog_act
  end type fill_prog_t
  !
contains
  !
  subroutine cubemain_fill_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(fill_user_t) :: user
    character(len=*), parameter :: rname='FILL>COMMAND'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    call fill%parse(line,user,error)
    if (error) return
    call fill%main(user,error)
    if (error) return
  end subroutine cubemain_fill_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_fill_register(fill,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(fill_comm_t), intent(inout) :: fill
    logical,            intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    type(cube_prod_t) :: oucube
    !
    character(len=*), parameter :: comm_abstract = &
         'Fill a subcube'
    character(len=*), parameter :: comm_help = &
         'Fill a region with NaN'
    character(len=*), parameter :: rname='FILL>REGISTER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'FILL','[cube]',&
         comm_abstract,&
         comm_help,&
         cubemain_fill_command,&
         fill%comm,error)
    if (error) return
    call cubearg%register( &
         'DATA', &
         'Data cube',  &
         strg_id,&
         code_arg_optional,  &
         [flag_cube], &
         code_read, &
         code_access_subset, &
         fill%cube, &
         error)
    if (error) return
    !
    call fill%range%register('RANGE',&
         'Spectral range to be filled',&
         error)
    if (error) return
    !
    call fill%center%register('CENTER',&
         'Spatial center of the filled region',&
         error)
    if (error) return
    !
    call fill%size%register(&
         'Spatial size of the filled region',&
         error)
    if (error) return
    !
    ! Product
    call oucube%register(&
         'FILLED',&
         'Filled cube',&
         strg_id,&
         [flag_fill,flag_cube],&
         fill%filled,&
         error)
    if (error) return
  end subroutine cubemain_fill_register
  !
  subroutine cubemain_fill_parse(fill,line,user,error)
    !----------------------------------------------------------------------
    ! FILL cubname
    ! /RANGE vfirst vlast
    ! /CENTER xcen ycen
    ! /SIZE sx [sy]
    !----------------------------------------------------------------------
    class(fill_comm_t), intent(in)    :: fill
    character(len=*),   intent(in)    :: line
    type(fill_user_t),  intent(out)   :: user
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: rname='FILL>PARSE'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,fill%comm,user%cubeids,error)
    if (error) return
    call fill%range%parse(line,user%range,error)
    if (error) return
    call fill%center%parse(line,user%center,error)
    if (error) return
    call fill%size%parse(line,user%size,error)
    if (error) return
    if (user%center%present.and..not.user%size%do) then
       call cubemain_message(seve%e,rname,'A size must be specified when giving a new center')
       error = .true.
       return
    endif
    !
    ! *** JP: Is this an error?
    if (cubetools_nopt().eq.0) then
       call cubemain_message(seve%e,rname,'No options given, nothing to do')
       error = .true.
       return
    endif
  end subroutine cubemain_fill_parse
  !
  subroutine cubemain_fill_main(fill,user,error)
    use cubeadm_timing
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(fill_comm_t), intent(in)    :: fill
    type(fill_user_t),  intent(in)    :: user
    logical,            intent(inout) :: error
    !
    type(fill_prog_t) :: prog
    character(len=*), parameter :: rname='FILL>MAIN'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call user%toprog(prog,error)
    if (error) return
    call prog%header(fill,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubemain_fill_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_fill_user_toprog(user,prog,error)
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(fill_user_t), intent(in)    :: user
    type(fill_prog_t),  intent(out)   :: prog
    logical,            intent(inout) :: error
    !
    integer(kind=chan_k) :: stride
    integer(kind=ndim_k)  :: jx,jy,jc
    type(sperange_prog_t) :: crange
    type(sparange_prog_t) :: lrange,mrange
    type(spapos_prog_t) :: center
    integer(kind=ndim_k), parameter :: ix=1,iy=2
    character(len=*), parameter :: rname='FILL>USER>TOPROG'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(fill%cube,user%cubeids,prog%cube,error)
    if (error) return
    !
    select case (prog%cube%order())
    case (code_cube_imaset)
       jx = 1
       jy = 2
       jc = 3
    case (code_cube_speset)
       jx = 2
       jy = 3
       jc = 1
    case default
       call cubemain_message(seve%e,rname,'Order not supported')
       error = .true.
       return
    end select
    !
    call user%center%toprog(prog%cube,center,error)
    if (error) return
    call lrange%fromuser(code_sparange_truncated,prog%cube%head%set%il,prog%cube,center%rela(ix),&
         user%size%x,user%size%unit,prog%cube%head%spa%l%inc,&
         prog%cube%head%spa%l%kind,error)
    if (error) return
    call lrange%to_pixe_k(prog%imin(jx),prog%imax(jx),stride,error)
    if (error) return
    call mrange%fromuser(code_sparange_truncated,prog%cube%head%set%im,prog%cube,center%rela(iy),&
         user%size%y,user%size%unit,prog%cube%head%spa%m%inc,&
         prog%cube%head%spa%m%kind,error)
    if (error) return
    call mrange%to_pixe_k(prog%imin(jy),prog%imax(jy),stride,error)
    if (error) return
    !
    call user%range%toprog(prog%cube,code_sperange_truncated,crange,error)
    if (error) return
    call crange%to_chan_k(prog%imin(jc),prog%imax(jc),stride,error)
    if (error) return
  end subroutine cubemain_fill_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_fill_prog_header(prog,comm,error)
    use cubeadm_clone
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(fill_prog_t), intent(inout) :: prog
    type(fill_comm_t),  intent(in)    :: comm
    logical,            intent(inout) :: error
    !
    character(len=*), parameter :: rname='FILL>PROG>HEADER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_clone_header(comm%filled,prog%cube,prog%filled,error)
    if (error) return
  end subroutine cubemain_fill_prog_header
  !
  subroutine cubemain_fill_prog_data(prog,error)
    use cubeadm_opened
    use cubetools_header_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(fill_prog_t), intent(inout) :: prog
    logical,            intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: itertask
    character(len=*), parameter :: rname='FILL>PROG>DATA'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(itertask,error)
    if (error) return
    !
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(itertask)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(itertask,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(itertask)
       if (.not.error) then
          call prog%loop(itertask,error)
       endif
       !$OMP END TASK
    enddo ! itertask
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubemain_fill_prog_data
  !
  subroutine cubemain_fill_prog_loop(prog,itertask,error)
    use cubeadm_taskloop
    !-------------------------------------------------------------------
    !
    !-------------------------------------------------------------------
    class(fill_prog_t),       intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: itertask
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='FILL>PROG>LOOP'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    do while (itertask%iterate_entry(error))
      call prog%act(itertask,error)
      if (error) return
    enddo  ! ientry
  end subroutine cubemain_fill_prog_loop
  !
  subroutine cubemain_fill_prog_act(prog,iter,error)
    use cubetools_nan
    use cubeadm_taskloop
    use cubeadm_subcube_types
    !-------------------------------------------------------------------
    ! Fill one subcube
    !-------------------------------------------------------------------
    class(fill_prog_t),       intent(inout) :: prog
    type(cubeadm_iterator_t), intent(in)    :: iter
    logical,                  intent(inout) :: error
    !
    logical :: zfill,yzfill,xyzfill
    type(subcube_t) :: in,ou
    integer(kind=indx_k) :: ix,iy,iz,jz
    character(len=*), parameter :: rname='FILL>PROG>ACT'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call in%associate('in',prog%cube,iter,error)
    if (error) return
    call ou%allocate('ou',prog%filled,iter,error)
    if (error) return
    !
    call in%get(error)
    if (error) return
    do iz=1,in%nz
       jz = in%range3(1)+iz-1
       zfill = (prog%imin(3).le.jz).and.(jz.le.prog%imax(3))
       do iy=1,in%ny
          yzfill = zfill.and.(prog%imin(2).le.iy).and.(iy.le.prog%imax(2))
          do ix=1,in%nx
             xyzfill = yzfill.and.(prog%imin(1).le.ix).and.(ix.le.prog%imax(1))
             if (xyzfill) then
                ou%val(ix,iy,iz) = gr4nan
             else
                ou%val(ix,iy,iz) = in%val(ix,iy,iz)
             endif
          enddo ! ix
       enddo ! iy
    enddo ! iz
    call ou%put(error)
    if (error) return
  end subroutine cubemain_fill_prog_act
end module cubemain_fill
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
