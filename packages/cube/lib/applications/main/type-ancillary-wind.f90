!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_ancillary_wind_types
  use cubemain_messaging
  use cubeadm_ancillary_cube_types
  !
  public :: ancillary_wind_comm_t,ancillary_wind_user_t,ancillary_wind_prog_t
  private
  !
  type, extends(ancillary_cube_comm_t) :: ancillary_wind_comm_t
   contains
     procedure, public :: register => cubemain_ancillary_wind_comm_register
  end type ancillary_wind_comm_t
  !
  type, extends(ancillary_cube_user_t) :: ancillary_wind_user_t
     ! Empty for the moment
  end type ancillary_wind_user_t
  !
  type, extends(ancillary_cube_prog_t) :: ancillary_wind_prog_t
     ! Empty for the moment
  end type ancillary_wind_prog_t
  !
contains
  !
  subroutine cubemain_ancillary_wind_comm_register(comm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    ! Register an optional "/WIND [windid]" key
    !----------------------------------------------------------------------
    class(ancillary_wind_comm_t), intent(inout) :: comm
    logical,                      intent(inout) :: error
    !
    character(len=*), parameter :: rname='ANCILLARY>WIND>COMM>REGISTER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call comm%fully_register(&
         'WINDOWS','[windowid]',&
         'Fetch a cube of windows to help define initial parameter guesses',&
         strg_id,&
         'WINDOW',&
         'Window cube',&
         [flag_window],&
         code_arg_optional,&
         code_read,&
         code_access_speset,&
         error)
    if (error) return
  end subroutine cubemain_ancillary_wind_comm_register
end module cubemain_ancillary_wind_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
