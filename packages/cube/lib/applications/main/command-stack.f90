!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_stack
  use cubetools_parameters
  use cubetools_structure
  use cubetools_keywordlist_types
  use cube_types
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubemain_messaging
  use cubemain_range
  use cubemain_windowing
  use cubemain_ancillary_mask_types
  !
  public :: stack
  private
  !
  integer(kind=4), parameter :: nweights=2                      
  character(len=*), parameter :: weights(nweights) = ['Equal','Noise']
  !
  type :: stack_comm_t
     type(option_t), pointer      :: comm     
     type(range_opt_t)            :: range      
     type(option_t), pointer      :: image      
     type(option_t), pointer      :: spectrum
     type(option_t), pointer      :: sum        
     type(option_t), pointer      :: mean       
     type(option_t), pointer      :: weight     
     type(keywordlist_comm_t), pointer :: wei_arg
     type(cubeid_arg_t),  pointer :: cube
     type(cubeid_arg_t),  pointer :: noise
     type(ancillary_mask_comm_t)  :: mask       
     type(cube_prod_t),   pointer :: stacked
   contains
     procedure, public  :: register     => cubemain_stack_comm_register
     procedure, private :: parse        => cubemain_stack_comm_parse
     procedure, private :: parse_weight => cubemain_stack_comm_parse_weight
     procedure, private :: main         => cubemain_stack_comm_main
  end type stack_comm_t
  type(stack_comm_t) :: stack
  !
  type stack_user_t
     type(range_array_t)         :: range            ! Spectral range of interest
     type(cubeid_user_t)         :: cubeids          ! Input Cubes
     type(ancillary_mask_user_t) :: mask             ! Input Mask
     logical                     :: dospe = .true.   ! Stack spectra or images?
     logical                     :: domean = .false. ! Comute average
     logical                     :: dosum  = .false. ! Compute sum
     logical                     :: dowei  = .false. ! Use weighting
     character(len=argu_l)       :: wei              ! Weight scheme
   contains
     procedure, private :: toprog => cubemain_stack_user_toprog
  end type stack_user_t
  !
  type stack_prog_t
     type(window_array_t)        :: wind               ! Window to be spectrally stacked
     type(cube_t), pointer       :: cube               ! Input cube
     type(ancillary_mask_prog_t) :: mask               ! Input mask
     type(cube_t), pointer       :: noise              ! Input noise
     type(cube_t), pointer       :: stacked            ! Output product
     logical                     :: donoise = .false.  ! Weight by noise?
     logical                     :: domask = .false.   ! Use a mask?
     logical                     :: mask2d = .false.   ! Is the mask 2d?
     logical                     :: dospe = .true.     ! Stack spectra or images?
     logical                     :: domean = .false.   ! Compute average or sum?
  end type stack_prog_t
  !
contains
  !
  subroutine cubemain_stack_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(stack_user_t) :: user
    character(len=*), parameter :: rname='STACK>COMMAND'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call stack%parse(line,user,error)
    if (error) return
    call stack%main(user,error)
    if (error) continue
  end subroutine cubemain_stack_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_stack_comm_register(comm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(stack_comm_t), intent(inout) :: comm
    logical,             intent(inout) :: error
    !
    type(cubeid_arg_t) :: incube
    type(cube_prod_t) :: oucube
    type(keywordlist_comm_t) :: keyarg
    character(len=*), parameter :: comm_abstract='Stack spectra or images'
    character(len=*), parameter :: comm_help=&
         'Stacking spectra is the default.&
         & Output unit is determined by the type of stacking.'
    character(len=*), parameter :: rname='STACK>COMM>REGISTER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    ! Syntax
    call cubetools_register_command(&
         'STACK','[cubeid [noiseid]]',&
         comm_abstract,&
         comm_help,&
         cubemain_stack_command,&
         comm%comm,&
         error)
    if (error) return
    call incube%register(&
         'CUBE',&
         'Signal cube', &
         strg_id,&
         code_arg_optional, &
         [flag_cube],&
         code_read,&
         code_access_user,&
         comm%cube,&
         error)
    if (error) return
    call incube%register(&
         'NOISE',&
         'Noise image or cube',&
         strg_id,&
         code_arg_optional,&
         [flag_noise],&
         code_read,&
         code_access_user,&
         comm%noise,&
         error)
    if (error) return
    !
    call comm%mask%register(&
         'Use a mask to define selected pixels or channels during&
         & stacking',&
         code_access_user,&
         error)
    if (error) return
    !
    call comm%range%register(&
         'RANGE',&
         'Velocity range(s) of interest',&
         range_is_multiple,error)
    if (error) return
    !
    call cubetools_register_option(&
         'SUM','',&
         'Output the sum of spectra or images',&
         'Output unit is Jy for a spectral stack and <CubUnit>.km/s for a&
         & spatial stack',&
         comm%sum,error)
    if (error) return
    !
    call cubetools_register_option(&
         'MEAN','',&
         'Output the mean of spectra or images',&
         'Output unit is K when stacking spectra and <CubUnit>&
         & when stacking images',&
         comm%mean,error)
    if (error) return
    !
    call cubetools_register_option(&
         'WEIGHT','scheme',&
         'Weighting scheme used during stacking',&
         'Only valid for /SPECTRUM',&
         comm%weight,error)
    if (error) return
    call keyarg%register(&
         'scheme', &
         'Weighting scheme',&
         strg_id,&
         code_arg_mandatory,&
         weights,&
         .not.flexible,&
         comm%wei_arg,&
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'IMAGE','',&
         'Stack images into an output image',&
         'Default behaviour is /SUM',&
         comm%image,error)
    if (error) return
    !
    call cubetools_register_option(&
         'SPECTRUM','',&
         'Stack spectra into an output spectrum',&
         'Default behaviour is /SUM for Jy/beam spectra and /MEAN for&
         & K(Tmb) spectra',&
         comm%spectrum,error)
    if (error) return
    !
    ! Products
    call oucube%register(&
         'STACKED',&
         'Stacked spectrum or image',&
         strg_id,&
         [flag_stack,flag_image_or_spectrum],&  ! resolved as flag_spectrum or flag_image at run time
         comm%stacked,&
         error,&
         access=code_access_imaset_or_speset)
    if (error)  return
  end subroutine cubemain_stack_comm_register
  !
  subroutine cubemain_stack_comm_parse(comm,line,user,error)
    use gkernel_interfaces
    !----------------------------------------------------------------------
    ! STACK 
    !----------------------------------------------------------------------
    class(stack_comm_t), intent(inout) :: comm
    character(len=*),    intent(in)    :: line
    type(stack_user_t),  intent(out)   :: user
    logical,             intent(inout) :: error
    !
    logical :: dorange,domean,dosum,dospectrum,doimage
    character(len=*), parameter :: rname='STACK>COMM>PARSE'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%range%parse(line,dorange,user%range,error)
    if (error) return
    ! Ranges are truncated to avoid segfault
    user%range%val(:)%truncate = .true.
    call comm%mask%parse(line,user%mask,error)
    if (error) return
    !
    call comm%mean%present(line,domean,error)
    if (error) return
    call comm%sum%present(line,dosum,error)
    if (error) return
    call comm%spectrum%present(line,dospectrum,error)
    if (error) return
    call comm%image%present(line,doimage,error)
    if (error) return
    !
    if (domean.and.dosum)then
       call cubemain_message(seve%e,rname,'Options /MEAN and /SUM are incompatible')
       error = .true.
       return
    else if (dosum) then
       user%domean = .false.
       user%dosum  = .true.
    else if (domean) then
       user%domean = .true.
       user%dosum  = .false.
    else
       user%domean = .false.
       user%dosum  = .false.
    endif
    if (dospectrum.and.doimage) then
       call cubemain_message(seve%e,rname,'Options /IMAGE and /SPECTRUM are incompatible')
       error = .true.
       return
    elseif (dospectrum) then
       user%dospe = .true.
    elseif (doimage) then
       user%dospe = .false.
    else
       user%dospe = .true.
    endif
    !
    call comm%parse_weight(line,user,error)
    if (error) return
  end subroutine cubemain_stack_comm_parse
  !
  subroutine cubemain_stack_comm_parse_weight(comm,line,user,error)
    use cubetools_disambiguate
    !----------------------------------------------------------------------
    ! STACK cubname
    ! /WEIGHT equal|noise
    !----------------------------------------------------------------------
    class(stack_comm_t), intent(in)    :: comm
    character(len=*),    intent(in)    :: line
    type(stack_user_t),  intent(inout) :: user
    logical,             intent(inout) :: error
    !
    character(len=*), parameter :: rname='STACK>COMM>PARSE>WEIGHT'
    !
    call comm%weight%present(line,user%dowei,error)
    if (error) return
    if (user%dowei) then
       if (.not.user%dospe) then
          ! *** JP: This is arguable
          call cubemain_message(seve%w,rname,'Option /WEIGHT irrelevant when stacking images')
          return
       endif
       call cubetools_getarg(line,comm%weight,1,user%wei,mandatory,error)
       if (error) return
    endif
  end subroutine cubemain_stack_comm_parse_weight
  !
  subroutine cubemain_stack_comm_main(comm,user,error)
    use cubemain_stack_spatial
    use cubemain_stack_spectral
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(stack_comm_t), intent(in)    :: comm
    type(stack_user_t),  intent(in)    :: user
    logical,             intent(inout) :: error
    !
    type(stack_prog_t) :: prog
    character(len=*), parameter :: rname='STACK>COMM>MAIN'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    if (prog%dospe) then
       call cubemain_stack_spectral_noaperture(prog%domean,prog%wind,prog%cube,&
            prog%mask%do,prog%mask%cube,prog%donoise,prog%noise,&
            comm%stacked,prog%stacked,error)
       if (error) return
    else
       call cubemain_stack_spatial_do(prog%domean,prog%wind,prog%cube,&
            prog%mask%do,prog%mask%isspatial,prog%mask%cube,&
            comm%stacked,prog%stacked,error)
       if (error) return
    endif
  end subroutine cubemain_stack_comm_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_stack_user_toprog(user,comm,prog,error)
    use cubetools_shape_types
    use cubetools_header_methods
    use cubeadm_get
    use cubeadm_consistency
    use cubemain_stack_spectral
    !----------------------------------------------------------------------
    ! *** JP: Is there a confusion between the toprog and header methods?
    !----------------------------------------------------------------------
    class(stack_user_t), intent(in)    :: user
    type(stack_comm_t),  intent(in)    :: comm
    type(stack_prog_t),  intent(inout) :: prog
    logical,             intent(inout) :: error
    !
    type(shape_t) :: n
    type(consistency_t) :: cons
    integer(kind=code_k) :: ikey,access
    character(len=argu_l) :: key
    character(len=*), parameter :: rname='STACK>USER>TOPROG'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    prog%dospe = user%dospe
    if (prog%dospe) then
       ! Stack spectra => Image access for parallelization purpose
       access = code_access_imaset
    else
       ! Stack images => Spectrum access for parallelization purpose
       access = code_access_speset
    endif
    !
    ! Input cube and region of interest
    call cubeadm_get_header(comm%cube,user%cubeids,prog%cube,error,access=access)
    if (error) return
    call comm%range%user2prog(prog%cube,user%range,prog%wind,error)
    if (error) return
    !
    if (.not.user%domean.and..not.user%dosum) then
       ! *** JP: My interpretation is that when the unit is not a valid
       ! *** JP: brightness unit we want a mean spectrum or a mean image
       ! *** JP: by default.
!!$       if (prog%dospe) then
!!$          call cubemain_stack_spectral_domean(prog%cube,prog%domean,error)
!!$          if (error) return
!!$       else
!!$          prog%domean = .false.
!!$       endif
       call cubemain_stack_spectral_domean(prog%cube,prog%domean,error)
       if (error) return
       ! *** JP: My
    else if (user%domean.and.user%dosum) then
       call cubemain_message(seve%e,rname,'User%domean and user%dosum are both true')
       error = .true.
       return
    else if (user%domean) then
       prog%domean = .true.
    else if (user%dosum) then
       prog%domean = .false.
    else
       call cubemain_message(seve%e,rname,'Programming error in the interpretation of the user inputs')
       error = .true.
       return       
    endif
    !
    call user%mask%toprog(comm%mask,prog%mask,error,access=access)
    if (error) return
    call prog%mask%check_consistency(prog%cube,error)
    if (error) return       
    !
    ! Weight and associated noise
    if (user%dowei) then
       call cubetools_keywordlist_user2prog(comm%wei_arg,user%wei,ikey,key,error)
       if (error) return
       prog%donoise = key.eq.'NOISE'
    else
       prog%donoise = .false.
    endif
    if (prog%donoise.and.prog%dospe) then
       call cubeadm_get_header(comm%noise,user%cubeids,prog%noise,error,access=access)
       if (error) return
       call cubetools_header_get_array_shape(prog%noise%head,n,error)
       if (error) return
       if (n%c.ne.1) then
          call cubemain_message(seve%e,rname,'Multiple noise definitions not yet supported')
          error = .true.
          return
       endif 
       call cons%signal_noise('Input cube',prog%cube,'Noise',prog%noise,error)
       if (error) return
    endif
    call cons%proceed(error)
    if (error) return
    !
    prog%dospe = user%dospe
  end subroutine cubemain_stack_user_toprog
end module cubemain_stack
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
