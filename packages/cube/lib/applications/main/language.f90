!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_language
  use cubetools_parameters
  use cubetools_structure
  use cubemain_messaging
  use cubemain_aperture
  use cubemain_averagec
  use cubemain_baseline
  use cubemain_circle
  use cubemain_compare
  use cubemain_compress
  use cubemain_consistency
  use cubemain_convert
  use cubemain_detect
  use cubemain_extract
  use cubemain_extrema
  use cubemain_feather
  use cubemain_fill
  use cubemain_header
  use cubemain_luminosity
  use cubemain_modify
  use cubemain_moments
  use cubemain_noise
  use cubemain_polar
  use cubemain_replace
  use cubemain_reproject
  use cubemain_resample
  use cubemain_rotate
  use cubemain_segment
  use cubemain_shuffle
  use cubemain_slice
  use cubemain_smooth
  use cubemain_snr
  use cubemain_sort
  use cubemain_stack
  use cubemain_stitch
  use cubemain_window
  !
  public :: cubemain_register_language
  private
  !
  integer(kind=lang_k) :: langid
  !
contains
  !
  subroutine cubemain_register_language(error)
    !----------------------------------------------------------------------
    ! Register the CUBE\ language
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    call cubetools_register_language('CUBE',&
         'J.Pety, S.Bardeau, V.deSouzaMagalhaes',&
         'Commands that performs various computations on cubes',&
         'gag_doc:hlp/cube-help-cube.hlp',&
         cubemain_execute_command,langid,error)
    if (error) return
    !----------------------------------------------------------------------
    !
    call aperture%register(error)
    if (error) return
    if (cube_dev_mode()) then
      call average%register(error)
      if (error) return
    endif
    call baseline%register(error)
    if (error) return    
    call circle%register(error)
    if (error) return
    call compare%register(error)
    if (error) return
    call compress%register(error)
    if (error) return
    call consistency%register(error)
    if (error) return
    call convert%register(error)
    if (error) return
    call detect%register(error)
    if (error) return
    call extract%register(error)
    if (error) return
    call extrema%register(error)
    if (error) return
    call feather%register(error)
    if (error) return
    call fill%register(error)
    if (error) return
    call header%register(error)
    if (error) return
    call luminosity%register(error)
    if (error) return
    call modify%register(error)
    if (error) return
    call moments%register(error)
    if (error) return
    call noise%register(error)
    if (error) return
    call polar%register(error)
    if (error) return
    call replace%register(error)
    if (error) return 
    call reproject%register(error)
    if (error) return
    call resample%register(error)
    if (error) return
    call rotate%register(error)
    if (error) return
    call segment%register(error)
    if (error) return
    call shuffle%register(error)
    if (error) return
    call slice%register(error)
    if (error) return
    call smooth%register(error)
    if (error) return
    call snr%register(error)
    if (error) return
    call sort%register(error)
    if (error) return
    call stack%register(error)
    if (error) return
    call stitch%register(error)
    if (error) return
    call window%register(error)
    if (error) return    
    !----------------------------------------------------------------------
    !
    ! call cubetools_register_command('BLANK',&
    !      'Blank part of a cube',&
    !      'cubname',&
    !      6,error)
    ! if (error) return
    ! call cubetools_register_option('SNR',&
    !      'To Be Completed',&
    !      'val',&
    !      error)
    ! if (error) return
    ! call cubetools_register_option('PERCENT',&
    !      'To Be Completed',&
    !      '',&
    !      error)
    ! if (error) return
    ! call cubetools_register_option('LIKE',&
    !      'To Be Completed',&
    !      'refcube',&
    !      error)
    ! if (error) return
    ! call cubetools_register_option('INSIDE',&
    !      'To Be Completed',&
    !      '',&
    !      error)
    ! if (error) return
    ! call cubetools_register_option('OUTSIDE',&
    !      'To Be Completed',&
    !      '',&
    !      error)
    ! if (error) return
    ! call cubetools_register_option('VALUE',&
    !      'To Be Completed',&
    !      '',&
    !      error)
    ! if (error) return
    !
    !----------------------------------------------------------------------
    !
    ! call cubetools_register_command('CONVOLVE',&
    !      'Convolve the LM images of a cube by an az. sym. function',&
    !      'cubname',&
    !      3,error)
    ! if (error) return
    ! call cubetools_register_option('WITH',&
    !      'To Be Completed',&
    !      '',&
    !      error)
    ! if (error) return
    ! call cubetools_register_option('REGULAR',&
    !      'To Be Completed',&
    !      '',&
    !      error)
    ! if (error) return
    ! call cubetools_register_option('NONORM',&
    !      'To Be Completed',&
    !      '',&
    !      error)
    ! if (error) return
    !
    !----------------------------------------------------------------------
    !
    call cubetools_register_dict(error)
    if (error) return
    !
  contains
    subroutine cube_lang_list(error)
      logical, intent(inout) :: error
    end subroutine cube_lang_list
  end subroutine cubemain_register_language
  !
  subroutine cubemain_execute_command(line,comm,error)
    use cubeadm_opened
    use cubeadm_timing
    !----------------------------------------------------------------------
    ! Execute a command of the CUBE\ language
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    character(len=*), intent(in)    :: comm
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='EXECUTE>COMMAND'
    !
    error = .false.
    if (comm.eq.'CUBE?') then
       call cubetools_list_language_commands(langid,error)
       if (error) return
    else
       call cubeadm_timing_init()
       call cubetools_execute_command(line,langid,comm,error)
       if (error) continue ! To ensure error recovery in the call to cubeadm_finish_all
       call cubeadm_finish_all(comm,line,error)
       if (error) continue ! To ensure error recovery in timing
       call cubeadm_timing_final(comm)
    endif
  end subroutine cubemain_execute_command
end module cubemain_language
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
