!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_extrema
  use cubetools_structure
  use cube_types
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubemain_messaging
  !
  public :: extrema
  private
  !
  type :: extrema_comm_t
     type(option_t),     pointer :: comm
     type(cubeid_arg_t), pointer :: incube
     type(cube_prod_t),  pointer :: oucube
   contains
     procedure, public  :: register => cubemain_extrema_register
     procedure, private :: parse    => cubemain_extrema_parse
     procedure, private :: main     => cubemain_extrema_main     
  end type extrema_comm_t
  type(extrema_comm_t) :: extrema
  !
  type extrema_user_t
     type(cubeid_user_t)  :: cubeids
  end type extrema_user_t
  type extrema_prog_t
     type(cube_t), pointer :: incube  ! Input cube
     type(cube_t), pointer :: oucube  ! Output cube
  end type extrema_prog_t
  !
contains
  !
  subroutine cubemain_extrema_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(extrema_user_t) :: user
    character(len=*), parameter :: rname='EXTREMA>COMMAND'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    call extrema%parse(line,user,error)
    if (error) return
    call extrema%main(user,error)
    if (error) continue
  end subroutine cubemain_extrema_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_extrema_register(extrema,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(extrema_comm_t), intent(inout) :: extrema
    logical,               intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    type(cube_prod_t) :: oucube
    !
    character(len=*), parameter :: comm_abstract = &
         'Compute the extrema of a cube'
    character(len=*), parameter :: comm_help = &
         strg_id
    character(len=*), parameter :: rname='EXTREMA>REGISTER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'EXTREMA','[cube]',&
         comm_abstract,&
         comm_help,&
         cubemain_extrema_command,&
         extrema%comm,error)
    if (error) return
    call cubearg%register( &
         'CUBE', &
         'Signal cube',  &
         strg_id,&
         code_arg_optional,  &
         [flag_any], &
         code_read, &
         code_access_subset, &
         extrema%incube, &
         error)
    if (error) return
    !
    ! Product
    call oucube%register(&
         'EXTREMA',&
         'Cube with updated extrema',&
         strg_id,&
         [flag_extrema],&
         extrema%oucube,&
         error,&
         flagmode=keep_all)
    if (error) return
  end subroutine cubemain_extrema_register
  !
  subroutine cubemain_extrema_parse(extrema,line,user,error)
    !----------------------------------------------------------------------
    ! EXTREMA cubname
    !----------------------------------------------------------------------
    class(extrema_comm_t), intent(in)    :: extrema
    character(len=*),      intent(in)    :: line
    type(extrema_user_t),  intent(out)   :: user
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='EXTREMA>PARSE'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,extrema%comm,user%cubeids,error)
    if (error) return
  end subroutine cubemain_extrema_parse
  !
  subroutine cubemain_extrema_main(extrema,user,error)
    use cubeadm_get
    use cubeadm_clone
    use cubeadm_timing
    use cubeadm_copy_tool
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(extrema_comm_t), intent(in)    :: extrema
    type(extrema_user_t),  intent(in)    :: user
    logical,               intent(inout) :: error
    !
    type(extrema_prog_t) :: prog
    character(len=*), parameter :: rname='EXTREMA>MAIN'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(extrema%incube,user%cubeids,prog%incube,error)
    if (error) return
    call cubeadm_clone_header(extrema%oucube,prog%incube,prog%oucube,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call cubeadm_copy_data(prog%incube,prog%oucube,error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubemain_extrema_main
  !
end module cubemain_extrema
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
