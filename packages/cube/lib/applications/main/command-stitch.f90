!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_stitch
  use cubetools_structure
  use cubemain_messaging
  use cubemain_merging
  !
  type :: stitch_comm_t
     type(option_t), pointer :: comm
     type(merging_comm_t)    :: merge
   contains
     procedure, public  :: register => cubemain_stitch_register
     procedure, public  :: parse    => cubemain_stitch_parse
     procedure, public  :: main     => cubemain_stitch_main
  end type stitch_comm_t
  type(stitch_comm_t) :: stitch
  !
contains
  !
  subroutine cubemain_stitch_command(line,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(merging_user_t) :: user
    character(len=*), parameter :: rname='STITCH>COMMAND'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call stitch%parse(line,user,error)
    if (error) return
    !
    call stitch%main(user,error)
    if (error) return
  end subroutine cubemain_stitch_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_stitch_register(stitch,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(stitch_comm_t), intent(inout) :: stitch
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='STITCH>REGISTER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'STITCH','',&
         'Stitch cubes from the current index',&
         'Stitch the cubes in the current index. The stitching can be&
         & spectrally aligned by usign option /ALIGN. Several aspects&
         & of the output cube can be controlled: its axes&
         & (/LAXIS, /MAXIS, /FAXIS and /VAXIS), its projection&
         & (/PTYPE, /PCENTER and /PANGLE) as well as its&
         & reference frequency and line. A reference cube can be used to&
         & define the spectral and spatial characteristics of&
         & the stitched cube (/LIKE). If no options are given&
         & the axes will be chosen in a way to cover all the&
         & data in all cubes in the index. By default the&
         & family name of the output cube will be the same&
         & of the first cube in the current index, this can&
         & be changed with the usage of option /FAMILY.',&
         cubemain_stitch_command,&
         stitch%comm,&
         error)
    if (error) return
    !
    call stitch%merge%register('stitched',flag_stitch,error)
    if (error) return
  end subroutine cubemain_stitch_register
  !
  subroutine cubemain_stitch_parse(stitch,line,user,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(stitch_comm_t),  intent(in)    :: stitch
    character(len=*),      intent(in)    :: line
    class(merging_user_t), intent(out)   :: user
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='STITCH>PARSE'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call stitch%merge%parse(line,user,error)
    if (error) return
  end subroutine cubemain_stitch_parse
  !
  subroutine cubemain_stitch_main(stitch,user,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(stitch_comm_t),  intent(in)    :: stitch
    class(merging_user_t), intent(in)    :: user
    logical,               intent(inout) :: error
    !
    type(merging_prog_t) :: prog
    character(len=*), parameter :: rname='STITCH>MAIN'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call user%toprog(stitch%merge,prog,error)
    if (error) return
    call prog%main(stitch%merge,error)
    if (error) return
  end subroutine cubemain_stitch_main
end module cubemain_stitch
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
