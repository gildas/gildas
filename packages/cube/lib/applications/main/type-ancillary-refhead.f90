!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_ancillary_refhead_types
  use cubemain_messaging
  use cubeadm_ancillary_cube_types
  !
  public :: ancillary_refhead_comm_t,ancillary_refhead_user_t,ancillary_refhead_prog_t
  private
  !
  type, extends(ancillary_cube_comm_t) :: ancillary_refhead_comm_t
   contains
     procedure, public :: register => cubemain_ancillary_refhead_comm_register
  end type ancillary_refhead_comm_t
  !
  type, extends(ancillary_cube_user_t) :: ancillary_refhead_user_t
     ! Empty for the moment
  end type ancillary_refhead_user_t
  !
  type, extends(ancillary_cube_prog_t) :: ancillary_refhead_prog_t
     ! Empty for the moment
  end type ancillary_refhead_prog_t
  !
contains
  !
  subroutine cubemain_ancillary_refhead_comm_register(key,commname,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    ! Register an optional "/LIKE [referenceid]" key
    ! ***JP: For the moment I register the key name as /LIKE to be compatible
    ! ***JP: with all the commands. But I suspect that getting a reference
    ! ***JP: refhead can be useful in other cases. That's why the module is
    ! ***JP: called cubemain_ancillary_refhead_types instead of
    ! ***JP: cubemain_ancillary_like_types
    !----------------------------------------------------------------------
    class(ancillary_refhead_comm_t), intent(inout) :: key
    character(len=*),                intent(in)    :: commname
    logical,                         intent(inout) :: error
    !
    character(len=*), parameter :: rname='ANCILLARY>REFHEAD>COMM>REGISTER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call key%fully_register(&
         'LIKE','[referenceid]',&
         'Define a reference header for the '//trim(commname)//' command',&
         strg_id,&
         'LIKE',&
         'Reference header',&
         [flag_any],&
         code_arg_mandatory,&
         code_read_head,&
         code_access_any,&  ! For a header only, any access is acceptable. Is
                            ! there any use case where we would like to force
                            ! it e.g. speset?
         error)
    if (error) return
  end subroutine cubemain_ancillary_refhead_comm_register
end module cubemain_ancillary_refhead_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
