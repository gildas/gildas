!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage CUBE messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_messaging
  use gpack_def
  use gbl_message
  use cubetools_parameters
  !
  public :: mainseve,seve,mess_l
  public :: cubemain_message_set_id,cubemain_message
  public :: cubemain_message_set_alloc,cubemain_message_get_alloc
  public :: cubemain_message_set_trace,cubemain_message_get_trace
  public :: cubemain_message_set_others,cubemain_message_get_others
  private
  !
  ! Identifier used for message identification
  integer(kind=4) :: cubemain_message_id = gpack_global_id  ! Default value for startup message
  !
  type :: cubemain_messaging_debug_t
     integer(kind=code_k) :: alloc = seve%d
     integer(kind=code_k) :: trace = seve%t
     integer(kind=code_k) :: others = seve%d
  end type cubemain_messaging_debug_t
  !
  type(cubemain_messaging_debug_t) :: mainseve
  !
contains
  !
  subroutine cubemain_message_set_id(id)
    !---------------------------------------------------------------------
    ! Alter library id into input id. Should be called by the library
    ! which wants to share its id with the current one.
    !---------------------------------------------------------------------
    integer(kind=4), intent(in) :: id
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='MESSAGE>SET>ID'
    !
    cubemain_message_id = id
    write (mess,'(A,I0)') 'Now use id #',cubemain_message_id
    call cubemain_message(seve%d,rname,mess)
  end subroutine cubemain_message_set_id
  !
  subroutine cubemain_message(mkind,procname,message)
    use cubetools_cmessaging
    !---------------------------------------------------------------------
    ! Messaging facility for the current library. Calls the low-level
    ! (internal) messaging routine with its own identifier.
    !---------------------------------------------------------------------
    integer(kind=4),  intent(in) :: mkind     ! Message kind
    character(len=*), intent(in) :: procname  ! Name of calling procedure
    character(len=*), intent(in) :: message   ! Message string
    !
    call cubetools_cmessage(cubemain_message_id,mkind,'MAIN>'//procname,message)
  end subroutine cubemain_message
  !
  subroutine cubemain_message_set_alloc(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       mainseve%alloc = seve%i
    else
       mainseve%alloc = seve%d
    endif
  end subroutine cubemain_message_set_alloc
  !
  subroutine cubemain_message_set_trace(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       mainseve%trace = seve%i
    else
       mainseve%trace = seve%t
    endif
  end subroutine cubemain_message_set_trace
  !
  subroutine cubemain_message_set_others(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       mainseve%others = seve%i
    else
       mainseve%others = seve%d
    endif
  end subroutine cubemain_message_set_others
  !
  function cubemain_message_get_alloc()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubemain_message_get_alloc
    !
    cubemain_message_get_alloc = mainseve%alloc.eq.seve%i
    !
  end function cubemain_message_get_alloc
  !
  function cubemain_message_get_trace()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubemain_message_get_trace
    !
    cubemain_message_get_trace = mainseve%trace.eq.seve%i
    !
  end function cubemain_message_get_trace
  !
  function cubemain_message_get_others()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubemain_message_get_others
    !
    cubemain_message_get_others = mainseve%others.eq.seve%i
    !
  end function cubemain_message_get_others
end module cubemain_messaging
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
