!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_ancillary_mask_types
  use cubetools_parameters
  use cubemain_messaging
  use cubeadm_ancillary_cube_types
  !
  public :: ancillary_mask_comm_t,ancillary_mask_user_t,ancillary_mask_prog_t
  private
  !
  type, extends(ancillary_cube_comm_t) :: ancillary_mask_comm_t
   contains
     procedure, public :: register => cubemain_ancillary_mask_comm_register
  end type ancillary_mask_comm_t
  !
  type, extends(ancillary_cube_user_t) :: ancillary_mask_user_t
     ! Empty for the moment
  end type ancillary_mask_user_t
  !
  ! *** JP to be generic we would need to add the possibility of a spectral mask...
  type, extends(ancillary_cube_prog_t) :: ancillary_mask_prog_t
     logical :: isspatial = .false. ! Spatial or cube?
   contains
     procedure, public :: check_consistency => cubemain_ancillary_mask_prog_check_consistency
     procedure, public :: must_be_spatial   => cubemain_ancillary_mask_must_be_spatial
     procedure, public :: must_be_3d        => cubemain_ancillary_mask_must_be_3d
  end type ancillary_mask_prog_t
  !
contains
  !
  subroutine cubemain_ancillary_mask_comm_register(comm,abstract,access,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    ! Register an optional "/MASK [maskid]" key
    !----------------------------------------------------------------------
    class(ancillary_mask_comm_t), intent(inout) :: comm
    character(len=*),             intent(in)    :: abstract
    integer(kind=code_k),         intent(in)    :: access
    logical,                      intent(inout) :: error
    !
    character(len=*), parameter :: rname='ANCILLARY>MASK>COMM>REGISTER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call comm%fully_register(&
         'MASK','[maskid]',&
         abstract,strg_id,&
         'MASK',&
         'Mask can be 2D or 3D',&
         [flag_mask],&
         code_arg_optional,&
         code_read,&
         access,&
         error)
    if (error) return
  end subroutine cubemain_ancillary_mask_comm_register
  !
  !------------------------------------------------------------------------
  !
  subroutine cubemain_ancillary_mask_prog_check_consistency(mask,refcube,error)
    use cubetools_header_methods
    use cube_types
    use cubeadm_consistency
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(ancillary_mask_prog_t), intent(inout) :: mask
    type(cube_t),                 intent(in)    :: refcube
    logical,                      intent(inout) :: error
    !
    integer(kind=chan_k) :: nc
    type(consistency_t) :: cons
    character(len=*), parameter :: rname='ANCILLARY>MASK>PROG>CHECK>CONSISTENCY'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    if (.not.mask%do) return
    !
    call cubetools_header_get_nchan(mask%cube%head,nc,error)
    if (error) return
    mask%isspatial = nc.eq.1
    if (mask%isspatial) then
       call cons%spatial('Input cube',refcube,'Mask',mask%cube,error)
       if (error) return
    else
       call cons%grid('Input cube',refcube,'Mask',mask%cube,error)
       if (error) return
    endif    
  end subroutine cubemain_ancillary_mask_prog_check_consistency
  !
  subroutine cubemain_ancillary_mask_must_be_spatial(mask,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(ancillary_mask_prog_t), intent(inout) :: mask
    logical,                      intent(inout) :: error
    !
    character(len=*), parameter :: rname='ANCILLARY>MASK>MUST>BE>SPATIAL'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    if (.not.mask%isspatial) then
       call cubemain_message(seve%e,rname,'A spatial mask is required')
    endif
  end subroutine cubemain_ancillary_mask_must_be_spatial
  !
  subroutine cubemain_ancillary_mask_must_be_3d(mask,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(ancillary_mask_prog_t), intent(inout) :: mask
    logical,                      intent(inout) :: error
    !
    character(len=*), parameter :: rname='ANCILLARY>MASK>MUST>BE>3D'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    if (mask%isspatial) then
       call cubemain_message(seve%e,rname,'A 3D mask is required')
    endif
  end subroutine cubemain_ancillary_mask_must_be_3d
end module cubemain_ancillary_mask_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
