!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_compare
  use cubetools_parameters
  use cube_types
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubemain_messaging
  !
  public :: compare
  private
  !
  type :: compare_comm_t
     type(option_t),     pointer :: comm
     type(option_t),     pointer :: epsilon
     type(cubeid_arg_t), pointer :: one
     type(cubeid_arg_t), pointer :: two
     type(cube_prod_t),  pointer :: residuals
     type(cube_prod_t),  pointer :: relerr
     type(cube_prod_t),  pointer :: mask
   contains
     procedure, public  :: register      => cubemain_compare_register
     procedure, private :: parse         => cubemain_compare_parse
     procedure, private :: parse_epsilon => cubemain_compare_parse_epsilon
     procedure, private :: main          => cubemain_compare_main
  end type compare_comm_t
  type(compare_comm_t) :: compare
  !
  type compare_user_t
     real(kind=sign_k)   :: epsilon
     type(cubeid_user_t) :: cubeids
   contains
     procedure, private :: toprog => cubemain_compare_user_toprog
  end type compare_user_t
  !
  type compare_prog_t
     real(kind=sign_k)     :: epsilon ! Significance level for the relative error
     type(cube_t), pointer :: one
     type(cube_t), pointer :: two
     type(cube_t), pointer :: residuals
     type(cube_t), pointer :: relerr
     type(cube_t), pointer :: mask
   contains
     procedure, private :: header => cubemain_compare_prog_header
     procedure, private :: data   => cubemain_compare_prog_data
     procedure, private :: loop   => cubemain_compare_prog_loop
     procedure, private :: act    => cubemain_compare_prog_act
  end type compare_prog_t
  !
contains
  !
  subroutine cubemain_compare_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(compare_user_t) :: user
    character(len=*), parameter :: rname='COMPARE>COMMAND'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call compare%parse(line,user,error)
    if (error) return
    call compare%main(user,error)
    if (error) continue
  end subroutine cubemain_compare_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_compare_register(compare,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(compare_comm_t), intent(inout) :: compare
    logical,               intent(inout) :: error
    !
    type(cubeid_arg_t) :: incube
    type(cube_prod_t) :: oucube
    type(standard_arg_t) :: stdarg
    character(len=*), parameter :: comm_abstract='Check whether input cubes are identical'
    character(len=*), parameter :: comm_help='&
         &Compute the residuals (= cube1-cube2), the relative error &
         &(=2*(cube1-cube2)/(cube1+cube2)/), and a mask with the locations where the &
         &data are equal/different as 0 and 1, respectively. The equality is computed &
         &by comparing the relative error with epsilon. By default epsilon = 1e-6.'
    character(len=*), parameter :: rname='COMPARE>REGISTER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    ! Syntax
    call cubetools_register_command(&
         'COMPARE','cube1 cube2',&
         comm_abstract,&
         comm_help,&
         cubemain_compare_command,&
         compare%comm,&
         error)
    call incube%register(&
         'CUBE1',&
         'Input cube #1',&
         strg_id,&
         code_arg_mandatory,&
         [flag_any],& ! *** JP: Should it be flag_any?
         code_read,&
         code_access_subset,&
         compare%one,&
         error)
    call incube%register(&
         'CUBE2',&
         'Input cube #2',&
         strg_id,&
         code_arg_mandatory,&
         [flag_cube],& ! *** JP: Should it be flag_any?
         code_read,&
         code_access_subset,&
         compare%two,&
         error)
    !
    call cubetools_register_option(&
         'SIGNIFICANCE','epsilon',&
         'Define the relative error significance level',&
         'Default to 1e-6',&
         compare%epsilon,&
         error)
    call stdarg%register(&
         'EPSILON',&
         'Relative error significance level',&
         'Default to 1e-6',&
         code_arg_mandatory,&
         error)
    !
    ! Products
    call oucube%register(&
         'RESIDUALS',&
         'Cube of residuals',&
         strg_id,&
         [flag_compared,flag_residuals],&
         compare%residuals,&
         error)
    if (error) return
    call oucube%register(&
         'RELERR',&
         'Relative error cube',&
         strg_id,&
         [flag_compared,flag_relative,flag_error],&
         compare%relerr,&
         error)
    call oucube%register(&
         'MASK',&
         'Locations where the data are equal/different as 0 and 1, respectively',&
         strg_id,&
         [flag_compared,flag_mask],&
         compare%mask,&
         error)
    if (error) return
  end subroutine cubemain_compare_register
  !
  subroutine cubemain_compare_parse(compare,line,user,error)
    !----------------------------------------------------------------------
    ! COMPARE cubeid1 cubeid2 /SIGNIFICANCE epsilon
    !----------------------------------------------------------------------
    class(compare_comm_t), intent(in)    :: compare
    character(len=*),      intent(in)    :: line
    type(compare_user_t),  intent(out)   :: user
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='COMPARE>PARSE'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,compare%comm,user%cubeids,error)
    if (error) return
    call compare%parse_epsilon(line,user,error)
    if (error) return
  end subroutine cubemain_compare_parse
  !
  subroutine cubemain_compare_parse_epsilon(comm,line,user,error)
    !----------------------------------------------------------------------
    ! /SIGNIFICANCE epsilon
    !----------------------------------------------------------------------
    class(compare_comm_t), intent(in)    :: comm
    character(len=*),      intent(in)    :: line
    type(compare_user_t),  intent(inout) :: user
    logical,               intent(inout) :: error
    !
    logical :: present
    character(len=*), parameter :: rname='COMPARE>PARSE>EPSILON'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call comm%epsilon%present(line,present,error)
    if (error) return
    if (.not.present) then
       user%epsilon = 1e-6
    else
       call cubetools_getarg(line,compare%epsilon,1,user%epsilon,mandatory,error)
       if (error) return
       !
       if (user%epsilon.lt.0) then
          call cubemain_message(seve%e,rname,'Epsilon must be positive')
          error = .true.
          return
       endif
    endif
  end subroutine cubemain_compare_parse_epsilon
  !
  subroutine cubemain_compare_main(comm,user,error) 
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(compare_comm_t), intent(in)    :: comm
    type(compare_user_t),  intent(inout) :: user
    logical,               intent(inout) :: error
    !
    type(compare_prog_t) :: prog
    character(len=*), parameter :: rname='COMPARE>MAIN'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubemain_compare_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_compare_user_toprog(user,comm,prog,error)
    use cubetools_user2prog
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(compare_user_t), intent(in)    :: user
    type(compare_comm_t),  intent(in)    :: comm 
    type(compare_prog_t),  intent(out)   :: prog
    logical,                       intent(inout) :: error
    !
    real(kind=sign_k), parameter :: default=1.0
    character(len=*), parameter :: rname='COMPARE>USER>TOPROG'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    prog%epsilon = user%epsilon
    call cubeadm_get_header(comm%one,user%cubeids,prog%one,error)
    if (error) return
    call cubeadm_get_header(comm%two,user%cubeids,prog%two,error)
    if (error) return
  end subroutine cubemain_compare_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubemain_compare_prog_header(prog,comm,error)
    use cubetools_consistency_methods
    use cubeadm_clone
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(compare_prog_t), intent(inout) :: prog
    type(compare_comm_t),  intent(in)    :: comm
    logical,               intent(inout) :: error
    !
    logical :: conspb
    character(len=*), parameter :: rname='COMPARE>PROG>HEADER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    conspb = .false.
    call cubetools_consistency_grid(&
         'Input cube #1',prog%one%head,&
         'Input cube #2',prog%two%head,&
         conspb,error)
    if (error) return
    if (cubetools_consistency_failed(rname,conspb,error)) return
    !
    call cubeadm_clone_header(comm%residuals,prog%one,prog%residuals,error)
    if (error) return
    call clone_header_and_nullify_unit(comm%relerr,prog%one,prog%relerr,error)
    if (error) return
    call clone_header_and_nullify_unit(comm%mask,prog%one,prog%mask,error)
    if (error) return
    !
  contains
    !
    subroutine clone_header_and_nullify_unit(comm,in,ou,error)
      use cubetools_header_methods
      !--------------------------------------------------------------------
      !
      !--------------------------------------------------------------------
      type(cube_prod_t), pointer, intent(in)    :: comm
      type(cube_t),      pointer, intent(in)    :: in
      type(cube_t),      pointer, intent(inout) :: ou
      logical,                    intent(inout) :: error
      !
      call cubeadm_clone_header(comm,in,ou,error)
      if (error) return
      call cubetools_header_put_array_unit('---',ou%head,error)
      if (error) return
    end subroutine clone_header_and_nullify_unit
  end subroutine cubemain_compare_prog_header
  !
  subroutine cubemain_compare_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(compare_prog_t), intent(inout) :: prog
    logical,               intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: itertask
    character(len=*), parameter :: rname='COMPARE>PROG>DATA'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(itertask,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(itertask)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(itertask,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(itertask)
       if (.not.error) &
         call prog%loop(itertask,error)
       !$OMP END TASK
    enddo ! itertask
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubemain_compare_prog_data
  !   
  subroutine cubemain_compare_prog_loop(prog,itertask,error)
    use cubeadm_taskloop
    !----------------------------------------------------------------------
    ! The subcube iterator will be shared by all input and output subcubes
    !----------------------------------------------------------------------
    class(compare_prog_t),    intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: itertask
    logical,                  intent(inout) :: error
    !
    character(len=*), parameter :: rname='COMPARE>PROG>LOOP'
    !
    do while (itertask%iterate_entry(error))
       call prog%act(itertask,error)
       if (error) return
    enddo ! ientry
  end subroutine cubemain_compare_prog_loop
  !   
  subroutine cubemain_compare_prog_act(prog,itertask,error)
    use cubeadm_taskloop
    use cubeadm_subcube_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(compare_prog_t),    intent(inout) :: prog
    type(cubeadm_iterator_t), intent(in)    :: itertask
    logical,                  intent(inout) :: error
    !
    integer(kind=indx_k) :: ix,iy,iz
    type(subcube_t) :: one,two,residuals,relerr,mask
    character(len=*), parameter :: rname='COMPARE>PROG>ACT'
    !
    ! Compares are initialized here as their size (3rd dim) may change from
    ! from one call to another.
    call one%associate('one',prog%one,itertask,error)
    if (error) return
    call two%associate('two',prog%two,itertask,error)
    if (error) return
    call residuals%allocate('residuals',prog%residuals,itertask,error)
    if (error) return
    call relerr%allocate('relerr',prog%relerr,itertask,error)
    if (error) return
    call mask%allocate('mask',prog%mask,itertask,error)
    if (error) return
    !
    call one%get(error)
    if (error) return
    call two%get(error)
    if (error) return
    do iz=1,one%nz
       do iy=1,one%ny
          do ix=1,one%nx
             mask%val(ix,iy,iz) = compare_one(&
                  one%val(ix,iy,iz),&
                  two%val(ix,iy,iz),&
                  prog%epsilon,&
                  residuals%val(ix,iy,iz),&
                  relerr%val(ix,iy,iz))
          enddo ! ix
       enddo ! iy
    enddo ! iz
    call residuals%put(error)
    if (error) return
    call relerr%put(error)
    if (error) return
    call mask%put(error)
    if (error) return
    !
  contains
    !
    function compare_one(a,b,epsilon,error,relerr) result(diff)
      use cubetools_nan
      !--------------------------------------------------------------------
      ! See https://floating-point-gui.de/errors/comparison/
      !--------------------------------------------------------------------
      real(kind=sign_k), intent(in)  :: a
      real(kind=sign_k), intent(in)  :: b
      real(kind=sign_k), intent(in)  :: epsilon
      real(kind=sign_k), intent(out) :: error
      real(kind=sign_k), intent(out) :: relerr
      real(kind=sign_k)              :: diff ! intent(out)
      !
      real(kind=sign_k) :: mean
      !
      if ((isnan(a).and.isnan(b)).or.(a.eq.b)) then
         ! Strict equality. Take care of NaN and infinities
         diff = 0.0
         error = 0.0
         relerr = 0.0
         return
      endif
      !
      error = a-b
      mean = 0.5*sign( (abs(a)+abs(b)), (a+b) ) ! Take care of the case where a and b are near zero with opposite signs
      if ((a.eq.0.0).or.(b.eq.0.0).or.(abs(mean).lt.tiny(mean))) then
         ! tiny(a) returns the smallest floating point number
         ! representable without being a denormal number.
         !
         ! So a or b is zero or both are extremely close to it.
         ! => Relative error is less meaningful here.
         if (error.lt.epsilon*tiny(error)) then
            diff = 0.0
            error = 0.0
            relerr = 0.0
         else
            diff = 1.0
            relerr = gr4nan
         endif
      else
         ! Standard case
         relerr = error/mean
         if (relerr.le.epsilon) then
            diff = 0.0
         else
            diff = 1.0
         endif
      endif
    end function compare_one
  end subroutine cubemain_compare_prog_act
end module cubemain_compare
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
