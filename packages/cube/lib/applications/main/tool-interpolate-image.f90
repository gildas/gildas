!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_interpolate_image_tool
  use cubetools_parameters
  use cubetools_array_types
  use cubemain_messaging
  !
  public :: interpolate_image_prog_t
  private
  !
  type interpolate_image_prog_t
     type(long_2d_t) :: ixin ! [---] Output pixel number in input image
     type(long_2d_t) :: iyin ! [---] Output pixel number in input image
     type(dble_2d_t) :: fxin ! [---] Fraction of input pixel in output pixel
     type(dble_2d_t) :: fyin ! [---] Fraction of input pixel in output pixel
   contains
     procedure, public :: init  => cubemain_interpolate_image_prog_init
     procedure, public :: image => cubemain_interpolate_image_prog_image
     procedure, public :: pixel => cubemain_interpolate_image_prog_pixel
  end type interpolate_image_prog_t
  !
contains
  !
  subroutine cubemain_interpolate_image_prog_init(interp,hxin,hyin,xin,yin,error)
    use cubetools_axis_types
    !-------------------------------------------------------------------
    ! Initialize the bilinear interpolation
    !-------------------------------------------------------------------
    class(interpolate_image_prog_t), intent(inout) :: interp
    type(axis_t),                    intent(inout) :: hxin   ! X axis header of input image
    type(axis_t),                    intent(inout) :: hyin   ! Y axis header of input image
    type(dble_2d_t), target,         intent(in)    :: xin    ! Input image x coordinate of output image pixel (ixou,iyou)
    type(dble_2d_t), target,         intent(in)    :: yin    ! Input image y coordinate of output image pixel (ixou,iyou)
    logical,                         intent(inout) :: error
    !
    real(kind=coor_k), parameter :: one = 1.0
    real(kind=coor_k) :: ixin
    real(kind=coor_k) :: iyin
    integer(kind=pixe_k) :: ixou,iyou
    integer(kind=pixe_k), pointer :: nxou,nyou
    character(len=*), parameter :: rname='INTERPOLATE>IMAGE>PROG>INIT'
    !
    ! *** JP Here I should have a consistency check on xin and yin dimensions
    nxou => xin%nx
    nyou => xin%ny
    !
    call interp%ixin%reallocate('ixin',nxou,nyou,error)
    if (error) return
    call interp%fxin%reallocate('fxin',nxou,nyou,error)
    if (error) return
    ! Force the axis conversion formula to be adapted to the NINT function used below
    call hxin%set_ref_to(one)
    !
    call interp%iyin%reallocate('iyin',nxou,nyou,error)
    if (error) return
    call interp%fyin%reallocate('fyin',nxou,nyou,error)
    if (error) return
    ! Force the axis conversion formula to be adapted to the NINT function used below
    call hyin%set_ref_to(one)
    !
    do iyou=1,nyou
       do ixou=1,nxou
          ixin = (xin%val(ixou,iyou)-hxin%val)/hxin%inc+hxin%ref
          iyin = (yin%val(ixou,iyou)-hyin%val)/hyin%inc+hyin%ref
          interp%ixin%val(ixou,iyou) = int(ixin)
          interp%iyin%val(ixou,iyou) = int(iyin)
          interp%fxin%val(ixou,iyou) = ixin-dble(interp%ixin%val(ixou,iyou))
          interp%fyin%val(ixou,iyou) = iyin-dble(interp%iyin%val(ixou,iyou))
       enddo ! ixou
    enddo ! iyou
  end subroutine cubemain_interpolate_image_prog_init
  !
  subroutine cubemain_interpolate_image_prog_image(interp,in,ou,error)
    use cubetools_nan
    use cubeadm_image_types
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(interpolate_image_prog_t), target, intent(in)    :: interp
    type(image_t),                           intent(in)    :: in
    type(image_t),                           intent(inout) :: ou
    logical,                                 intent(inout) :: error
    !
    type(dble_2d_t), pointer :: fxin,fyin
    integer(kind=pixe_k), pointer :: ixin,iyin
    integer(kind=pixe_k) :: ixou,iyou
    character(len=*), parameter :: rname='INTERPOLATE>IMAGE>PROG>IMAGE'
    !
    fxin => interp%fxin
    fyin => interp%fyin
    !
    do iyou=1,ou%ny
       do ixou=1,ou%nx
          ixin => interp%ixin%val(ixou,iyou)
          iyin => interp%iyin%val(ixou,iyou)          
          if (&
               ((1.le.ixin).and.(ixin.lt.in%nx)).and.&
               ((1.le.iyin).and.(iyin.lt.in%ny))) then
             ! Inside input image
             ou%val(ixou,iyou) = interp%pixel(&
                  fxin%val(ixou,iyou),fyin%val(ixou,iyou),&
                  in%val(ixin  ,iyin  ),& ! blc
                  in%val(ixin+1,iyin  ),& ! brc
                  in%val(ixin+1,iyin+1),& ! trc
                  in%val(ixin  ,iyin+1))  ! tlc
          else
             ! Outside input image
             ou%val(ixou,iyou) = gr4nan
          endif
       enddo ! ixou
    enddo ! iyou
  end subroutine cubemain_interpolate_image_prog_image
  !
  function cubemain_interpolate_image_prog_pixel(interp,fx,fy,blc,brc,trc,tlc) result(interpolated)
    class(interpolate_image_prog_t), intent(in)    :: interp
    real(kind=dble_k),               intent(in)    :: fx,fy
    real(kind=real_k),               intent(in)    :: blc,brc,trc,tlc
    real(kind=real_k)                              :: interpolated
    !
    interpolated = &
         (1-fx)*(1-fy)*blc + fx*(1-fy)*brc + fx*fy*trc + (1-fx)*fy*tlc
  end function cubemain_interpolate_image_prog_pixel
end module cubemain_interpolate_image_tool
! 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
