!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_ancillary_init_types
  use cubemain_messaging
  use cubeadm_ancillary_cube_types
  !
  public :: ancillary_init_comm_t,ancillary_init_user_t,ancillary_init_prog_t
  private
  !
  type, extends(ancillary_cube_comm_t) :: ancillary_init_comm_t
   contains
     procedure, public :: register => cubemain_ancillary_init_comm_register
  end type ancillary_init_comm_t
  !
  type, extends(ancillary_cube_user_t) :: ancillary_init_user_t
     ! Empty for the moment
  end type ancillary_init_user_t
  !
  type, extends(ancillary_cube_prog_t) :: ancillary_init_prog_t
     ! Empty for the moment
  end type ancillary_init_prog_t
  !
contains
  !
  subroutine cubemain_ancillary_init_comm_register(comm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    ! Register an optional "/INIT [initid]" key
    !----------------------------------------------------------------------
    class(ancillary_init_comm_t), intent(inout) :: comm
    logical,                      intent(inout) :: error
    !
    character(len=*), parameter :: rname='ANCILLARY>INIT>COMM>REGISTER'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    call comm%fully_register(&
         'PARAMETERS','[parameterid]',&
         'Fetch a fit parameter cube to be used as an inital guesses',&
         'The input CUBE can be the output of the command MINIMIZE or&
         &can be created freely by the user',&
         'PARAMETERS',&
         'Parameter cube',&
         [flag_minimize],&
         code_arg_optional,&
         code_read,&
         code_access_speset,&
         error)
    if (error) return
  end subroutine cubemain_ancillary_init_comm_register
end module cubemain_ancillary_init_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
