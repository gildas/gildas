!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_statistics_tool
  use cubetools_parameters
  use cubetools_nan
  use cubemain_messaging
  !
  public :: statistics
  private
  !
  type statistics_t
   contains
     procedure, public  :: minmax => cubemain_statistics_minmax
     !
     procedure, public  :: mean => cubemain_statistics_mean
     procedure, public  :: rms  => cubemain_statistics_rms    
     !
     procedure, public  :: percentile => cubemain_statistics_percentile
     procedure, public  :: median     => cubemain_statistics_median
     procedure, public  :: mad        => cubemain_statistics_mad
     !
     procedure, private :: sort => cubemain_statistics_sort
  end type statistics_t
  type(statistics_t) :: statistics
  !
contains
  !
  subroutine cubemain_statistics_minmax(prog,vec,nvec,mymin,mymax)
    !---------------------------------------------------------------------
    ! Compute the min and max values of vec
    !
    ! It assumes that all array elements are valid, ie, ne.NaN.
    !---------------------------------------------------------------------
    class(statistics_t),  intent(in)    :: prog
    real(kind=real_k),    intent(in)    :: vec(:)
    integer(kind=entr_k), intent(in)    :: nvec
    real(kind=real_k),    intent(inout) :: mymin
    real(kind=real_k),    intent(inout) :: mymax
    !
    integer(kind=entr_k) :: ivec
    character(len=*), parameter :: rname='STATISTICS>MINMAX'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    if (nvec.le.0) then
       mymin = gr4nan
       mymax = gr4nan
    else
       mymin = vec(1)
       mymax = vec(1)
       do ivec=1,nvec
          mymin = min(mymin,vec(ivec))
          mymax = max(mymax,vec(ivec))
       enddo ! ivec
    endif
  end subroutine cubemain_statistics_minmax
  !
  !-----------------------------------------------------------------------
  !
  function cubemain_statistics_mean(prog,vec,nvec) result(mean)
    !----------------------------------------------------------------------
    ! Compute the mean.
    !
    ! It assumes that all array elements are valid, ie, ne.NaN.
    !----------------------------------------------------------------------
    class(statistics_t),  intent(in) :: prog
    real(kind=real_k),    intent(in) :: vec(:)
    integer(kind=entr_k), intent(in) :: nvec
    real(kind=real_k)                :: mean
    !
    integer(kind=entr_k) :: ivec
    real(kind=real_k) :: sum
    character(len=*), parameter :: rname='STATISTICS>MEAN'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    if (nvec.le.0) then
       mean = gr4nan
    else
       sum = 0.0
       do ivec=1,nvec
          sum = sum+vec(ivec)
       enddo ! ivec
       mean = sum/nvec
    endif
  end function cubemain_statistics_mean
  !
  function cubemain_statistics_rms(prog,vec,nvec,mean) result(rms)
    !----------------------------------------------------------------------
    ! Compute the Root Mean Square.
    !
    ! It assumes that all array elements are valid, ie, ne.NaN.
    !----------------------------------------------------------------------
    class(statistics_t),  intent(in) :: prog
    real(kind=real_k),    intent(in) :: vec(:)
    integer(kind=entr_k), intent(in) :: nvec
    real(kind=real_k),    intent(in) :: mean
    real(kind=real_k)                :: rms
    !
    integer(kind=entr_k) :: ivec
    real(kind=real_k) :: sum
    character(len=*), parameter :: rname='STATISTICS>RMS'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    if ((nvec.le.0).or.(ieee_is_nan(mean))) then
       rms = gr4nan
    else if (nvec.eq.1) then
       !***JP: Do we need a minimum number of points?
       rms = 0.0
    else
       ! nvec is greater than 1
       sum = 0.0
       do ivec=1,nvec
          sum = sum+(vec(ivec)-mean)**2
       enddo ! ivec
       rms = sum/(nvec-1)
    endif
  end function cubemain_statistics_rms
  !
  !-----------------------------------------------------------------------
  !
  function cubemain_statistics_percentile(prog,vec,nvec,percentage) result(percentile)
    !----------------------------------------------------------------------
    ! Compute the percentile value at percentage.
    ! The percentile at   0% is the minimum of the distribution.
    ! The percentile at 100% is the maximum of the distribution.
    ! In betwee, we compute the rank as ceiling(nve*(percentage/100.0))
    ! A percentage below 0% or above 100% delivers NaN.
    !
    ! It assumes that all array elements are valid, ie, ne.NaN.
    !----------------------------------------------------------------------
    class(statistics_t),  intent(in)    :: prog
    real(kind=real_k),    intent(inout) :: vec(:)
    integer(kind=entr_k), intent(in)    :: nvec
    real(kind=real_k),    intent(in)    :: percentage
    real(kind=real_k)                   :: percentile
    !
    logical :: error
    integer(kind=chan_k) :: irank
    character(len=*), parameter :: rname='STATISTICS>PERCENTILE'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    if (nvec.le.0) then
       percentile = gr4nan
    else
       irank = ceiling(nvec*(percentage/100.0))
       if (irank.eq.0) irank = 1
       error = .false.
       call statistics%sort(vec(1:nvec),irank,error)
       if (error) then
          percentile = gr4nan
       else
          percentile = vec(irank)
       endif
    endif
  end function cubemain_statistics_percentile
  !
  function cubemain_statistics_median(prog,vec,nvec) result(median)
    !----------------------------------------------------------------------
    ! Compute the median.
    !
    ! It assumes that all array elements are valid, ie, ne.NaN.
    !
    ! This is the correct definition for an odd number samples and a good
    ! enough compromise between performance and accuracy for an even number
    ! of samples.
    !----------------------------------------------------------------------
    class(statistics_t),  intent(in)    :: prog
    real(kind=real_k),    intent(inout) :: vec(:)
    integer(kind=entr_k), intent(in)    :: nvec
    real(kind=real_k)                   :: median
    !
    logical :: error
    integer(kind=chan_k) :: nmed
    character(len=*), parameter :: rname='STATISTICS>MEDIAN'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    if (nvec.le.0) then
       median = gr4nan
    else if (nvec.eq.1) then
       median = vec(1)
    else if (nvec.eq.2) then
       median = 0.5*(vec(1)+vec(2))
    else if (nvec.ge.3) then
       nmed = nvec/2+1
       error = .false.
       call statistics%sort(vec(1:nvec),nmed,error)
       if (error) then
          median = gr4nan
       else
          median = vec(nmed)
       endif
    endif
  end function cubemain_statistics_median
  !
  function cubemain_statistics_mad(prog,vec,nvec,median) result(mad)
    !----------------------------------------------------------------------
    ! Compute the Mean Absolute Deviation.
    !
    ! For normally distributed data K is taken to be 1/phi^-1(3/4) =~ 1.4826,
    ! where phi^-1 is the inverse of the cumulative distribution function for
    ! the standard normal distribution, i.e., the quantile function.
    ! See http://en.wikipedia.org/wiki/Median_absolute_deviation
    !
    ! It assumes that all array elements are valid, ie, ne.NaN.
    !----------------------------------------------------------------------
    class(statistics_t),  intent(in)    :: prog
    real(kind=real_k),    intent(inout) :: vec(:)
    integer(kind=entr_k), intent(in)    :: nvec
    real(kind=real_k),    intent(in)    :: median
    real(kind=real_k)                   :: mad
    !
    integer(kind=chan_k) :: ivec
    real(kind=real_k), parameter :: kfactor=1.4826
    character(len=*), parameter :: rname='STATISTICS>MAD'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    if ((nvec.le.0).or.(ieee_is_nan(median))) then
       mad = gr4nan
    else if (nvec.eq.1) then
       !***JP: Do we need a minimum number of points?
       mad = 0.0
    else
       do ivec=1,size(vec)
          vec(ivec) = abs(vec(ivec)-median)
       enddo ! ivec
       mad = kfactor*statistics%median(vec,nvec)
    endif
  end function cubemain_statistics_mad
  !
  !-----------------------------------------------------------------------
  !
  subroutine cubemain_statistics_sort(prog,vec,ith,error)
    !---------------------------------------------------------------------
    ! Sort the input vector to have the ith smallest value in location
    ! vec(ith), i.e., with all smaller elements moved to vec(1:ith-1) (in
    ! arbitrary order) and all larger elements in vec(ith+1:) (also in
    ! arbitrary order).
    !---------------------------------------------------------------------
    class(statistics_t),  intent(in)    :: prog
    real(kind=real_k),    intent(inout) :: vec(:)
    integer(kind=chan_k), intent(in)    :: ith
    logical,              intent(inout) :: error
    !
    integer(kind=chan_k) :: iup,jdown,iright,ileft,imed,nvec
    real(kind=real_k) :: pivot
    character(len=*), parameter :: rname='STATISTICS>SORT'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    nvec = size(vec)
    if ((ith.lt.1).or.(ith.gt.nvec)) then
       call cubemain_message(seve%e,rname,'Asked element out of array range')
       error = .true.
       return
    endif
    !
    ileft = 1
    iright = nvec
    do
       if (iright-ileft.le.1) then
          if (iright-ileft.eq.1) then
             ! Two elements remaining => Last potential swap!
             if (vec(ileft).gt.vec(iright)) call swap(vec(ileft),vec(iright))
          else
             ! One element remaining => Nothing to be done anymore
          endif
          return ! Finished!
       else 
          ! 1. Swap imedian and ileft+1
          ! 2. Ensure that vec(ileft) <= vec(ileft+1) <= vec(iright)
          ! 3. Use median value, i.e., vec(ileft+1), as pivot
          imed = (ileft+iright)/2
          call swap(vec(imed),vec(ileft+1))
          if (vec(ileft).gt.vec(iright))   call swap(vec(ileft),vec(iright))
          if (vec(ileft+1).gt.vec(iright)) call swap(vec(ileft+1),vec(iright))
          if (vec(ileft).gt.vec(ileft+1))  call swap(vec(ileft),vec(ileft+1))
          pivot = vec(ileft+1)
          ! Initialize partitioning loop
          iup = ileft+1
          jdown = iright
          do ! Partitioning loop
             do ! Scan up to find element greater than pivot
                iup = iup+1
                if (vec(iup).ge.pivot) exit
             enddo
             do ! Scan down to find element lower than pivot
                jdown = jdown-1
                if (vec(jdown).le.pivot) exit
             enddo
             if (jdown.lt.iup) exit ! Partitioning complete => Exit loop
             call swap(vec(iup),vec(jdown))
          enddo
          ! Analyze result
          vec(ileft+1) = vec(jdown)
          vec(jdown) = pivot
          if (jdown.ge.ith) iright = jdown-1
          if (jdown.le.ith) ileft  = iup
       endif
    enddo
    !
  contains
    !
    subroutine swap(in1,in2)
      !---------------------------------------------------------------------
      ! Swap two real
      !---------------------------------------------------------------------
      real(kind=real_k), intent(inout) :: in1
      real(kind=real_k), intent(inout) :: in2
      !
      real(kind=chan_k) :: tmp
      !
      tmp = in1
      in1 = in2
      in2 = tmp
    end subroutine swap
  end subroutine cubemain_statistics_sort
end module cubemain_statistics_tool
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
