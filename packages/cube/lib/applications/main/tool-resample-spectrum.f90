!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubemain_resample_spectrum_tool
  use cubetools_parameters
  use cubemain_messaging
  !
  public :: resample_spectrum_prog_t
  private
  !
  type resample_spectrum_prog_t
     logical :: contaminate = .true. ! NaN will contaminate the output
     logical :: equalweight = .true. ! Each channel weights equally
     integer(kind=chan_k) :: ochanmin
     integer(kind=chan_k) :: ochanmax
     real(kind=coor_k) :: inval0
     real(kind=coor_k) :: ouval0
     ! The following ones are only used for the undersampled case
     real(kind=coor_k) :: distmin
     real(kind=coor_k) :: distmax
     real(kind=coor_k) :: w1,w2,w3
     procedure(cubemain_resample_spectrum_prog_copy), pointer :: spectrum => null()
   contains
     procedure, public  :: init   => cubemain_resample_spectrum_prog_init
     procedure, private :: nanify => cubemain_resample_spectrum_prog_nanify
  end type resample_spectrum_prog_t
  !
contains
  !
  subroutine cubemain_resample_spectrum_prog_init(resample,in,ou,error)
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    ! Bilinear resampling based on CLASS algorithm. For information, CLASS
    ! offers a trilinear interpolation but it's bugged.
    !----------------------------------------------------------------------
    class(resample_spectrum_prog_t), intent(inout) :: resample
    type(spectrum_t),                intent(in)    :: in
    type(spectrum_t),                intent(in)    :: ou
    logical,                         intent(inout) :: error
    !
    real(kind=coor_k) :: xr1,xr2,is1,is2
    character(len=*), parameter :: rname='RESAMPLE>SPECTRUM>PROG>INIT'
    !
    call cubemain_message(mainseve%trace,rname,'Welcome')
    !
    ! For the moment 
    resample%contaminate = .true. ! NaN will contaminate the output
    resample%equalweight = .true. ! Each channel weights equally
    !
    ! ***JP: Identical code with tool_interpolate_spectrum.f90
    ! Special case: output axis = input axis
    if ((ou%n.eq.in%n).and.(ou%ref.eq.in%ref).and.(ou%val.eq.in%val).and.(ou%inc.eq.in%inc)) then
       ! ***JP: This code is too simple. We can have identical axes in other cases!
       resample%spectrum => cubemain_resample_spectrum_prog_copy
       ! Nothing to do anymore => return
       return
    endif
    ! Sanity check
    if ((in%inc.eq.0.d0).or.(ou%inc.eq.0.d0)) then
       call cubemain_message(seve%e,rname,'Zero valued input or output increment')
       error = .true.
       return
    endif
    ! ***JP: Identical code with tool_interpolate_spectrum.f90
    !
    xr1 = (  1.-in%ref)*in%inc+in%val
    xr2 = (in%n-in%ref)*in%inc+in%val
    is1 = (xr1-ou%val)/ou%inc+ou%ref  
    is2 = (xr2-ou%val)/ou%inc+ou%ref
    if (is1.lt.is2) then
       resample%ochanmin = nint(is1)
       resample%ochanmax = nint(is2)
    else
       resample%ochanmin = nint(is2)
       resample%ochanmax = nint(is1)
    endif
    if ((resample%ochanmin.gt.ou%n).or.(resample%ochanmax.lt.1)) then
      call cubemain_message(seve%e,rname, 'New spectral axis does not intersect the original one')
      error = .true.
      return
    endif
    resample%ochanmin = max(resample%ochanmin,1)
    resample%ochanmax = min(resample%ochanmax,ou%n)
    !
    ! The algorithm below works with the 0-th channels as references (simpler
    ! equations and faster computations). Compute the X value at these channels.
    resample%inval0 = (0.d0-in%ref)*in%inc+in%val
    resample%ouval0 = (0.d0-ou%ref)*ou%inc+ou%val
    !
    if (abs(ou%inc).le.abs(in%inc)) then
       resample%spectrum => cubemain_resample_spectrum_prog_bilinear_oversample
    else
       resample%spectrum => cubemain_resample_spectrum_prog_bilinear_undersample
       ! Additional computations for the undersampled case
       resample%distmax = (abs(ou%inc)+abs(in%inc))*0.5
       resample%distmin = (abs(ou%inc)-abs(in%inc))*0.5
       resample%w1 = -sign(1.d0,in%inc)*resample%distmin-resample%inval0 ! Take care of the resolution sign
       resample%w2 = +sign(1.d0,in%inc)*resample%distmax-resample%inval0 ! Idem
       resample%w3 = 1./(resample%distmax-resample%distmin)                      ! = 1/abs(in%inc)
    endif
  end subroutine cubemain_resample_spectrum_prog_init
  !
  subroutine cubemain_resample_spectrum_prog_copy(resample,in,ou,error)
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(resample_spectrum_prog_t), target, intent(in)    :: resample
    type(spectrum_t),                target, intent(in)    :: in
    type(spectrum_t),                target, intent(inout) :: ou
    logical,                                 intent(inout) :: error
    !
    character(len=*), parameter :: rname='RESAMPLE>SPECTRUM>PROG>COPY'
    !
    ou%y%val(:) = in%y%val(:)
    ou%w%val(:) = in%w%val(:)
  end subroutine cubemain_resample_spectrum_prog_copy
  !
  subroutine cubemain_resample_spectrum_prog_bilinear_undersample(resample,in,ou,error)
    use cubetools_nan
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(resample_spectrum_prog_t), target, intent(in)    :: resample
    type(spectrum_t),                target, intent(in)    :: in
    type(spectrum_t),                target, intent(inout) :: ou
    logical,                                 intent(inout) :: error
    !
    real(kind=sign_k), pointer :: inwval,inyval
    real(kind=sign_k), pointer :: ouwval,ouyval
    real(kind=sign_k) :: curwei,sumwei
    real(kind=coor_k) :: incenter,oucenter
    real(kind=coor_k) :: dist
    integer(kind=chan_k) :: ichan,inmin,nchan
    integer(kind=chan_k) :: ochan,inmax
    character(len=*), parameter :: rname='RESAMPLE>SPECTRUM>PROG>BILINEAR>UNDERSAMPLE'
    !
    ! Loop on output channels
    do ochan=resample%ochanmin,resample%ochanmax
       ouyval => ou%y%val(ochan)
       ouwval => ou%w%val(ochan)
       !
       oucenter = ou%inc*ochan+resample%ouval0
       !
       ! Local resample for input observation
       inmin = max(1   ,int((oucenter+resample%w1)/in%inc))
       inmax = min(in%n,int((oucenter+resample%w2)/in%inc))
       nchan  = 0. ! Number of channels that contribute
       sumwei = 0. ! Fraction sum
       ouwval = 0. ! Weight of (possibly resampled) channel from R
       ouyval = 0. ! Value of  (possibly resampled) channel from R
       do ichan=inmin,inmax
          inwval => in%w%val(ichan)
          inyval => in%y%val(ichan)
          incenter = in%inc*ichan+resample%inval0
          dist = abs(incenter-oucenter)
          if (dist.lt.resample%distmax) then
             ! This channel in R is at least partially overlapped by the
             ! one in S
             if (ieee_is_nan(inyval)) then
                ! Bad channels
                if (resample%contaminate) then
                   ouyval = gr4nan
                   ouwval = 0.0
                   goto 30 ! To next output channel
                endif
                curwei = 0.
             else if (dist.le.resample%distmin) then
                ! This channel in R is fully overlapped by the one in S. It
                ! has a full contribution:
                curwei = 1.
             else
                ! This channel in R is partially overlapped by the one in S
                ! It contributes, but less. resample%w3 normalizes dist-resample%distmin such as
                ! 0<curwei<1 . curwei is proportional to the overlapping.
                curwei = 1.-(dist-resample%distmin)*resample%w3
             endif
             nchan  = nchan+curwei
             sumwei = sumwei+inwval*curwei*curwei
             ouwval = ouwval+inwval*curwei
             ouyval = ouyval+inwval*curwei*inyval
          endif
       enddo ! ichan
       !
       if (ouwval.ne.0.0) then
          ! Value of resampled channel
          ouyval = ouyval/ouwval
          if (resample%equalweight) then
             ! Normalize the weight of the resampled channel
             ouwval = ouwval/nchan
          else
             ! Weight of resampled channel
             ouwval = ouwval*ouwval/sumwei
          endif
       else
          ouyval = gr4nan
       endif
       !
30     continue
    enddo ! ochan
    !
    call resample%nanify(ou,error)
    if (error)  return
  end subroutine cubemain_resample_spectrum_prog_bilinear_undersample
  !
  subroutine cubemain_resample_spectrum_prog_bilinear_oversample(resample,in,ou,error)
    use cubetools_nan
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(resample_spectrum_prog_t), target, intent(in)    :: resample
    type(spectrum_t),                target, intent(in)    :: in
    type(spectrum_t),                target, intent(inout) :: ou
    logical,                                 intent(inout) :: error
    !
    real(kind=sign_k), pointer :: inwval,inyval
    real(kind=sign_k), pointer :: ouwval,ouyval
    real(kind=sign_k) :: curwei,sumwei
    real(kind=coor_k) :: ichanf,oucenter
    integer(kind=chan_k) :: ichan,nchan
    integer(kind=chan_k) :: ochan
    character(len=*), parameter :: rname='RESAMPLE>SPECTRUM>PROG>BILINEAR>OVERSAMPLE'
    !
    ! Loop on output channels
    do ochan=resample%ochanmin,resample%ochanmax
       ouyval => ou%y%val(ochan)
       ouwval => ou%w%val(ochan)
       !
       oucenter = ou%inc*ochan+resample%ouval0    ! X position
       ichanf = (oucenter-resample%inval0)/in%inc ! Channel position in input spectrum
       !
       nchan  = 0.
       sumwei = 0.
       ouyval = 0.
       ouwval = 0.
       !
       ! Left channel contribution
       ichan = int(ichanf)
       inwval => in%w%val(ichan)
       inyval => in%y%val(ichan)
       if ((ichan.ge.1).and.(ichan.le.in%n)) then
          if (ieee_is_nan(inyval)) then
             ! Bad channels
             if (resample%contaminate) then
                ouyval = gr4nan
                ouwval = 0.0
                cycle ! To next output channel
             endif
             curwei = 0.
          else
             curwei = real(ichan+1)-ichanf
          endif
          nchan  = nchan+curwei
          sumwei = sumwei+inwval*curwei*curwei
          ouwval = ouwval+inwval*curwei
          ouyval = ouyval+inwval*curwei*inyval
       endif
       !
       ! Right channel contribution
       ichan = ichan+1
       inwval => in%w%val(ichan)
       inyval => in%y%val(ichan)
       if ((ichan.ge.1).and.(ichan.le.in%n)) then
          if (ieee_is_nan(inyval)) then
             ! Bad channels
             if (resample%contaminate) then
                ouyval = gr4nan
                ouwval = 0.0
                cycle ! Next output channel
             endif
             curwei = 0.
          else
             curwei = ichanf-real(ichan-1)
          endif
          nchan  = nchan+curwei
          sumwei = sumwei+inwval*curwei*curwei
          ouwval = ouwval+inwval*curwei
          ouyval = ouyval+inwval*curwei*inyval
       endif
       !
       if (ouwval.ne.0.0) then
          ! Value of resampled channel
          ouyval = ouyval/ouwval 
          if (resample%equalweight) then
             ! Normalize the weight of the resampled channel
             ouwval = ouwval/nchan
          else
             ! Weight of resampled channel
             ouwval = ouwval*ouwval/sumwei
          endif
       else
          ouyval = gr4nan
       endif
    enddo ! ochan
    !
    call resample%nanify(ou,error)
    if (error)  return
  end subroutine cubemain_resample_spectrum_prog_bilinear_oversample
  !
  subroutine cubemain_resample_spectrum_prog_nanify(resample,ou,error)
    use cubetools_nan
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    ! Set as NaN the channels before ochanmin and after ochanmax, if any
    !----------------------------------------------------------------------
    class(resample_spectrum_prog_t), intent(in)    :: resample
    type(spectrum_t),                intent(inout) :: ou
    logical,                         intent(inout) :: error
    !
    integer(kind=chan_k) :: ochan
    !
    do ochan=1,resample%ochanmin-1  ! No iteration if ochanmin.le.1
      ou%y%val(ochan) = gr4nan
      ou%w%val(ochan) = 0.0
    enddo
    do ochan=resample%ochanmax+1,ou%n  ! No iteration if ochanmax.ge.ou%n
      ou%y%val(ochan) = gr4nan
      ou%w%val(ochan) = 0.0
    enddo
  end subroutine cubemain_resample_spectrum_prog_nanify
end module cubemain_resample_spectrum_tool
! 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
