###########################################################################
#
# Makefile system for GILDAS softwares (2003-2025).
#
# Please be careful: element order often matters in makefiles.
#
###########################################################################

include $(gagadmdir)/Makefile.def

###########################################################################

ANCILLARY_OBJECTS = type-ancillary-refhead.o type-ancillary-init.o		\
type-ancillary-mask.o type-ancillary-wind.o

ANCILLARY_EXPORTS = cubemain_ancillary_init_types.mod			\
cubemain_ancillary_mask_types.mod cubemain_ancillary_wind_types.mod 

###########################################################################

TOOL_OBJECTS = tool-average.o tool-interpolate-image.o			\
tool-interpolate-spectrum.o tool-resample-spectrum.o			\
tool-spectrum-computation.o tool-spectrum-moment.o tool-statistics.o

TOOL_EXPORTS = cubemain_interpolate_image_tool.mod	\
cubemain_interpolate_spectrum_tool.mod cubemain_statistics_tool.mod

###########################################################################

# Baselining
BASELINE_OBJECTS = command-baseline.o command-baseline-median.o			\
command-baseline-chebyshev.o command-baseline-wavelet.o tool-chebyshev.o	\
tool-svd.o type-baseline-cubes.o type-lineset-or-mask.o

# Operations on data
DATA_OBJECTS = command-convert.o command-replace.o command-fill.o

# Operations on header
HEADER_OBJECTS = command-extrema.o command-header.o command-modify.o

# Selection of a subset
SELECTION_OBJECTS = command-extract.o command-slice.o

# Detection of signal and computation of luminosity and moments
SIGNAL_OBJECTS = command-detect.o command-noise.o command-segment.o	\
command-snr.o command-sort.o command-window.o command-luminosity.o	\
command-moments.o

# Playing with spatial coordinates
SPATIAL_OBJECTS = command-compress.o command-polar.o	\
command-reproject.o command-rotate.o command-smooth.o

# Playing with spectral coordinates
SPECTRAL_OBJECTS = command-resample.o command-shuffle.o

# Stacking spectra or image
STACKING_OBJECTS = command-aperture.o command-circle.o command-stack.o	\
engine-stack-spatial.o tool-stack-spectra.o #engine-stack-spectral.o

# Averaging, stitching cubes
SUM_OBJECTS = command-average.o command-stitch.o engine-merging.o

# Others
OTHER_OBJECTS = command-feather.o command-compare.o command-consistency.o	\
type-kernel.o type-pang.o type-spatial-ellipse.o

###########################################################################

LIB_IDENTITY = cubemain

LIB_F_OBJECTS = $(ANCILLARY_OBJECTS) $(TOOL_OBJECTS) $(BASELINE_OBJECTS)	\
$(DATA_OBJECTS) $(HEADER_OBJECTS) $(SELECTION_OBJECTS) $(SIGNAL_OBJECTS)	\
$(SPATIAL_OBJECTS) $(SPECTRAL_OBJECTS) $(STACKING_OBJECTS) $(SUM_OBJECTS)	\
$(OTHER_OBJECTS) language.o message.o

LIB_EXPORTS = $(ANCILLARY_EXPORTS) $(TOOL_EXPORTS) cubemain_language.mod	\
cubemain_messaging.mod

LIB_DEPENDS = -lcubeobsolete $(CUBE_INTERFACE_LIBS) $(CUBE_CORE_LIBS)	\
$(GREG_LIBS)

INTERFACES_DISABLE = yes

###########################################################################

include $(gagadmdir)/Makefile.lib

###########################################################################

include $(builddir)/Makefile.deps

###########################################################################
