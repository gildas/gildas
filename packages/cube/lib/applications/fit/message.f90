!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage CUBE messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubefit_messaging
  use gpack_def
  use gbl_message
  use cubetools_parameters
  !
  public :: fitseve,seve,mess_l
  public :: cubefit_message_set_id,cubefit_message
  public :: cubefit_message_set_alloc,cubefit_message_get_alloc
  public :: cubefit_message_set_trace,cubefit_message_get_trace
  public :: cubefit_message_set_others,cubefit_message_get_others
  private
  !
  ! Identifier used for message identification
  integer(kind=4) :: cubefit_message_id = gpack_global_id  ! Default value for startup message
  !
  type :: cubefit_messaging_debug_t
     integer(kind=code_k) :: alloc = seve%d
     integer(kind=code_k) :: trace = seve%t
     integer(kind=code_k) :: others = seve%d
  end type cubefit_messaging_debug_t
  !
  type(cubefit_messaging_debug_t) :: fitseve
  !
contains
  !
  subroutine cubefit_message_set_id(id)
    !---------------------------------------------------------------------
    ! Alter library id into input id. Should be called by the library
    ! which wants to share its id with the current one.
    !---------------------------------------------------------------------
    integer(kind=4), intent(in) :: id
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='MESSAGE>SET>ID'
    !
    cubefit_message_id = id
    write (mess,'(A,I0)') 'Now use id #',cubefit_message_id
    call cubefit_message(seve%d,rname,mess)
  end subroutine cubefit_message_set_id
  !
  subroutine cubefit_message(mkind,procname,message)
    use cubetools_cmessaging
    !---------------------------------------------------------------------
    ! Messaging facility for the current library. Calls the low-level
    ! (internal) messaging routine with its own identifier.
    !---------------------------------------------------------------------
    integer(kind=4),  intent(in) :: mkind     ! Message kind
    character(len=*), intent(in) :: procname  ! Name of calling procedure
    character(len=*), intent(in) :: message   ! Message string
    !
    call cubetools_cmessage(cubefit_message_id,mkind,'FIT>'//procname,message)
  end subroutine cubefit_message
  !
  subroutine cubefit_message_set_alloc(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       fitseve%alloc = seve%i
    else
       fitseve%alloc = seve%d
    endif
  end subroutine cubefit_message_set_alloc
  !
  subroutine cubefit_message_set_trace(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       fitseve%trace = seve%i
    else
       fitseve%trace = seve%t
    endif
  end subroutine cubefit_message_set_trace
  !
  subroutine cubefit_message_set_others(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       fitseve%others = seve%i
    else
       fitseve%others = seve%d
    endif
  end subroutine cubefit_message_set_others
  !
  function cubefit_message_get_alloc()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubefit_message_get_alloc
    !
    cubefit_message_get_alloc = fitseve%alloc.eq.seve%i
    !
  end function cubefit_message_get_alloc
  !
  function cubefit_message_get_trace()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubefit_message_get_trace
    !
    cubefit_message_get_trace = fitseve%trace.eq.seve%i
    !
  end function cubefit_message_get_trace
  !
  function cubefit_message_get_others()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubefit_message_get_others
    !
    cubefit_message_get_others = fitseve%others.eq.seve%i
    !
  end function cubefit_message_get_others
end module cubefit_messaging
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
