!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubefit_language
  use cubetools_structure
  use cubefit_messaging
  !
  use cubefit_command_parameters
  use cubefit_minimize
  use cubefit_residuals
  use cubefit_result
  !
  public :: cubefit_register_language
  private
  !
  integer(kind=lang_k) :: langid
  !
contains
  !
  subroutine cubefit_register_language(error)
    !----------------------------------------------------------------------
    ! Register the CUBE\ language
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    call cubetools_register_language('FIT',&
         'J.Pety, S.Bardeau, V.deSouzaMagalhaes',&
         'Commands for spectral fitting',&
         'gag_doc:hlp/cube-help-fit.hlp',&
         cubefit_execute_command,langid,error)
    if (error) return
    !----------------------------------------------------------------------
    !
    call minimize%register(error)
    if (error) return
    call parameters%register(error)
    if (error) return
    call residuals%register(error)
    if (error) return
    call result%register(error)
    if (error) return
    !
    call cubetools_register_dict(error)
    if (error) return
    !
  end subroutine cubefit_register_language
  !
  subroutine cubefit_execute_command(line,comm,error)
    use cubeadm_opened
    use cubeadm_timing
    !----------------------------------------------------------------------
    ! Execute a command of the CUBE\ language
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    character(len=*), intent(in)    :: comm
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='EXECUTE>COMMAND'
    !
    error = .false.
    if (comm.eq.'FIT?') then
       call cubetools_list_language_commands(langid,error)
       if (error) return
    else
       call cubeadm_timing_init()
       call cubetools_execute_command(line,langid,comm,error)
       if (error) continue ! To ensure error recovery in the call to cubeadm_finish_all
       call cubeadm_finish_all(comm,line,error)
       if (error) continue ! To ensure error recovery in timing
       call cubeadm_timing_final(comm)
    endif
  end subroutine cubefit_execute_command
end module cubefit_language
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
