module cubefit_spectral_parameters
  use cubetools_parameters
  use cubefit_parameters
  use cubefit_messaging
  !
  public spectral_pars_t
  !
  ! Status codes
  integer(kind=code_k), parameter :: code_imported   =  2
  !
  type spectral_pars_t
     integer(kind=npar_k)             :: n = 0
     real(kind=para_k),   allocatable :: pars(:)
     real(kind=para_k),   allocatable :: errs(:)
     real(kind=grad_k),   allocatable :: grad(:)
     integer(kind=npar_k)             :: leaders(mpara)
     integer(kind=flag_k),allocatable :: flag(:,:)
     integer(kind=func_k)             :: nfunc
     integer(kind=code_k)             :: method
   contains
     procedure                        :: init       => cubefit_spectral_parameters_init
     procedure                        :: check_line => cubefit_spectral_parameters_checkline
  end type spectral_pars_t
  !
contains
  !
  subroutine cubefit_spectral_parameters_init(par,npar,method,nfunc,error)
    use gkernel_interfaces
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    class(spectral_pars_t), intent(out)   :: par
    integer(kind=code_k),   intent(in)    :: method
    integer(kind=npar_k),   intent(in)    :: npar
    integer(kind=func_k),   intent(in)    :: nfunc
    logical,                intent(inout) :: error
    !
    integer(kind=4) :: ier
    character(len=*), parameter :: rname='PARAMETERS>INIT'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    if (nfunc.gt.mfunc) then
       call cubefit_message(seve%e,rname,"Number of functions is greater than maximum allowed")
       error = .true.
       return
    endif
    !
    par%n      = npar
    par%nfunc  = nfunc
    par%method = method
    !
    allocate(par%pars(par%n),par%grad(par%n),par%errs(par%n),par%flag(max(nfunc,1),mpara),stat=ier)
    if (failed_allocate(rname,'parameter arrays',ier,error)) return
  end subroutine cubefit_spectral_parameters_init
  !
  subroutine cubefit_spectral_parameters_checkline(par,ivar,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(spectral_pars_t), intent(inout) :: par
    integer(kind=npar_k),   intent(in)    :: ivar
    logical,                intent(inout) :: error
    !
    integer(kind=func_k) :: depvar,ifunc
    integer(kind=4) :: i
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='PARAMETERS>CHECK>LINE'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    if (par%nfunc.eq.0) then
       par%flag(1,ivar)=mod(par%flag(1,ivar),2)
       par%leaders(ivar) = 0
       return
    endif
    ! Find dependent
    if (par%flag(1,ivar).gt.4)then
       write(mess,101) 1,ivar
       call cubefit_message(seve%e,rname,mess)
       error = .true.
       return
    endif
    par%leaders(ivar)=0
    depvar=0
    if (par%flag(1,ivar).eq.2.or.par%flag(1,ivar).eq.4) par%leaders(ivar)=1
    if (par%flag(1,ivar).eq.3) depvar=1
    !
    i = 0
    do ifunc=2,par%nfunc
       if (par%flag(ifunc,ivar).gt.4) then
          write(mess,101) ifunc,ivar
          call cubefit_message(seve%e,rname,mess)
          error = .true.
          return
       endif
       if (par%flag(ifunc,ivar).eq.2.or.par%flag(ifunc,ivar).eq.4) then
          if (par%leaders(ivar).eq.0) then
             par%leaders(ivar)=ifunc
          else
             i=i+1
          endif
       endif
       if (par%flag(ifunc,ivar).eq.3) depvar=depvar+1
    enddo
    !
    if (i.ne.0) then
       write(mess,'(a,i0,a)') 'Several groups in for ',ivar,"-th parameter "
       call cubefit_message(seve%e,rname,mess)
       error = .true.
       return
    endif
    if (depvar.eq.0 .and. par%leaders(ivar).ne.0) then
       write(mess,'(2(a,i0),a)') 'Line ',par%leaders(ivar),' alone in a ',ivar,' parameter group'
       call cubefit_message(seve%w,rname,mess)
    endif
    if (depvar.ne.0 .and. par%leaders(ivar).eq.0) then
       write(mess,'(a,i0,a)') 'No independent ',ivar,'-th parameter'
       call cubefit_message(seve%e,rname,mess)
       error = .true.
       return
    endif
101 format("Flag for ",i0,"-th line and ",i0," parameter larger than 4")
  end subroutine cubefit_spectral_parameters_checkline
end module cubefit_spectral_parameters
