!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubefit_result
  use cube_types
  use cubetools_structure
  use cubetools_axis_types
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubetopology_sperange_types
  use cubefit_messaging
  use cubefit_hfs
  use cubefit_parameters
  use cubefit_selection
  !
  public :: result
  private
  !
  type :: result_comm_t
     type(option_t),     pointer :: comm
     type(cubeid_arg_t), pointer :: incube
     type(cubeid_arg_t), pointer :: fitcube
     type(sperange_opt_t)        :: range
     type(option_t),     pointer :: hfs
     type(selection_opt_t)       :: select
     type(cube_prod_t),  pointer :: oucube
   contains
     procedure, public  :: register => cubefit_result_register
     procedure, private :: parse    => cubefit_result_parse
     procedure, private :: main     => cubefit_result_main
  end type result_comm_t
  type(result_comm_t) :: result
  !
  type result_user_t
     type(cubeid_user_t)    :: cubeids
     type(sperange_user_t)  :: range
     type(hfs_user_t)       :: hfs
     type(selection_user_t) :: sele
   contains
     procedure, private :: toprog => cubefit_result_user_toprog
  end type result_user_t
  type result_prog_t
     type(sperange_prog_t)  :: range
     type(hfs_prog_t)       :: hfs
     logical                :: found
     type(selection_prog_t) :: sele
     type(axis_t)           :: axis
     type(cube_t), pointer  :: incube
     type(cube_t), pointer  :: fitcube
     type(cube_t), pointer  :: oucube
   contains
     procedure, private :: header => cubefit_result_prog_header
     procedure, private :: data   => cubefit_result_prog_data
     procedure, private :: loop   => cubefit_result_prog_loop
     procedure, private :: act    => cubefit_result_prog_act
  end type result_prog_t
  !
contains
  !
  subroutine cubefit_result_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*),       intent(in)    :: line
    logical,                intent(inout) :: error
    !
    type(result_user_t) :: user
    character(len=*), parameter :: rname='RESULT>COMMAND'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    call result%parse(line,user,error)
    if (error) return
    call result%main(user,error)
    if (error) return
  end subroutine cubefit_result_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubefit_result_register(result,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(result_comm_t), intent(inout) :: result
    logical,              intent(inout) :: error
    !
    type(cubeid_arg_t) :: cubearg
    type(cube_prod_t) :: oucube
    character(len=*), parameter :: comm_abstract = 'Produce a model cube from fit results'
    character(len=*), parameter :: comm_help = &
         strg_id
    character(len=*), parameter :: rname='RESULT>REGISTER'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'RESULT','[cube [fit]]',&
         comm_abstract,&
         comm_help,&
         cubefit_result_command,&
         result%comm,error)
    if (error) return
    call cubearg%register( &
         'CUBE', &
         'Signal cube',  &
         strg_id,&
         code_arg_optional,  &
         [flag_cube], &
         code_read_head, &
         code_access_speset, &
         result%incube, &
         error)
    if (error) return
    call cubearg%register( &
         'FIT', &
         'Cube containing fit results',  &
         strg_id,&
         code_arg_optional,  &
         [flag_fit,flag_minimize], &
         code_read, &
         code_access_speset, &
         result%fitcube, &
         error)
    if (error) return
    !
    call result%range%register(&
         'RANGE',&
         'Define velocity range for results',&
         error)
    if (error) return
    !
    call cubefit_hfs_register(result%hfs,error)
    if (error) return
    !
    call result%select%register(error)
    if (error) return
    !
    ! Product
    call oucube%register(&
         'RESULT',&
         'Cube of results',&
         strg_id,&
         [flag_fit,flag_result],&
         result%oucube,&
         error)
    if (error) return
  end subroutine cubefit_result_register
  !
  subroutine cubefit_result_parse(result,line,user,error)
    !----------------------------------------------------------------------
    ! RESULT cubname
    ! /RANGE vfirst vlast [unit]
    ! /HFS file.hfs
    ! /SELECT method [ifunc]
    !----------------------------------------------------------------------
    class(result_comm_t), intent(in)    :: result
    character(len=*),     intent(in)    :: line
    type(result_user_t),  intent(out)   :: user
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='RESULT>PARSE'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,result%comm,user%cubeids,error)
    if (error) return
    call result%range%parse(line,user%range,error)
    if (error) return
    call cubefit_hfs_parse(line,result%hfs,user%hfs,error)
    if (error) return
    call result%select%parse(line,user%sele,error)
    if (error) return
  end subroutine cubefit_result_parse
  !
  subroutine cubefit_result_main(result,user,error) 
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(result_comm_t), intent(in)    :: result
    type(result_user_t),  intent(in)    :: user
    logical,              intent(inout) :: error
    !
    type(result_prog_t) :: prog
    character(len=*), parameter :: rname='RESULT>MAIN'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    call user%toprog(prog,error)
    if (error) return
    call prog%header(result,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubefit_result_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubefit_result_user_toprog(user,prog,error)
    use cubetools_header_methods
    use cubetools_disambiguate
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(result_user_t), intent(in)    :: user
    type(result_prog_t),  intent(out)   :: prog
    logical,              intent(inout) :: error
    !
    integer(kind=chan_k) :: fchan,lchan,stride
    character(len=*), parameter :: rname='RESULT>USER>TOPROG'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    call cubeadm_get_header(result%incube,user%cubeids,&
         prog%incube,error)
    if (error) return
    call cubeadm_get_header(result%fitcube,user%cubeids,&
         prog%fitcube,error)
    if (error) return
    !
    call user%range%toprog(prog%incube,code_sperange_truncated,prog%range,error)
    if (error) return
    call prog%range%to_chan_k(fchan,lchan,stride,error)
    if (error) return
    call cubetools_header_get_axis_head_f(prog%incube%head,prog%axis,error)
    if (error) return
    prog%axis%n = lchan-fchan+1
    prog%axis%ref = prog%axis%ref-fchan+1
    !
    call cubefit_hfs_user2prog(user%hfs,prog%hfs,error)
    if (error) return
    call result%select%user2prog(user%sele,prog%sele,error)
    if (error) return
    !
  end subroutine cubefit_result_user_toprog
  !
  subroutine cubefit_result_prog_header(prog,comm,error)
    use cubetools_header_methods
    use cubeadm_clone
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(result_prog_t), intent(inout) :: prog
    type(result_comm_t),  intent(in)    :: comm
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='RESULT>HEADER'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    call cubeadm_clone_header(comm%oucube,prog%incube,prog%oucube,error)
    if (error) return
    !
    call cubetools_header_update_frequency_from_axis(prog%axis,prog%oucube%head,error)
    if (error) return
  end subroutine cubefit_result_prog_header
  !
  subroutine cubefit_result_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(result_prog_t), intent(inout) :: prog
    logical,              intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='RESULT>DATA'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    ! At the start nothing has been found
    prog%found = .false.
    call cubeadm_datainit_all(iter,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo ! ie
    !$OMP END SINGLE
    !$OMP END PARALLEL
    !
    if (.not.prog%found) then
       call cubefit_message(seve%w,rname,'No data found under current selection')
    endif
  end subroutine cubefit_result_prog_data
  !   
  subroutine cubefit_result_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubemain_spectrum_real
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(result_prog_t),     intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(spectrum_t) :: fitspec,ouspec
    character(len=*), parameter :: rname='RESULT>LOOP'
    !
    call fitspec%reassociate_and_init(prog%fitcube,iter,error)
    if (error) return
    call ouspec%reallocate_and_init(prog%oucube,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter%ie,fitspec,ouspec,error)
      if (error) return
    enddo
  end subroutine cubefit_result_prog_loop
  !   
  subroutine cubefit_result_prog_act(prog,ie,fitspec,ouspec,error)
    use cubetools_nan
    use cubemain_spectrum_real
    use cubefit_spectral_fit
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(result_prog_t), intent(inout) :: prog
    integer(kind=entr_k), intent(in)    :: ie
    type(spectrum_t),     intent(inout) :: fitspec
    type(spectrum_t),     intent(inout) :: ouspec
    logical,              intent(inout) :: error
    !
    logical :: doprof
    type(fit_spectral_t) :: fit
    character(len=*), parameter :: rname='RESULT>ACT'
    !
    call fitspec%get(prog%fitcube,ie,error)
    if (error) return
    doprof = .not.(ieee_is_nan(fitspec%t(icol_status)).or.nint(fitspec%t(icol_status)).eq.code_status_diverged)
    if (prog%sele%imeth.ne.0) then
       doprof = doprof.and.(prog%sele%imeth.eq.nint(fitspec%t(icol_method)))
    endif
    doprof = doprof.and.(prog%sele%ifunc.le.nint(fitspec%t(icol_nfunc)))   
    !
    if (doprof) then
       prog%found = .true.
       call fit%spec_import(prog%hfs,fitspec,error)
       if (error) return
       call fit%profile(prog%sele%ifunc,ouspec,error)
       if (error) return
    else
       ouspec%t(:) = gr4nan
    endif
    call ouspec%put(prog%oucube,ie,error)
    if (error) return
  end subroutine cubefit_result_prog_act
end module cubefit_result
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
