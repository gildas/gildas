module cubefit_function_spectral_absorption
  use fit_minuit
  !
  use cubefit_messaging
  use cubefit_spectral_parameters
  use cubefit_spectral_obs
  !
  real(kind=para_k), parameter ::  absorption_tau_min = 0.1
  real(kind=para_k), parameter ::  absorption_tau_max = 100.0
  !
  integer(kind=npar_k),parameter :: npara = 3
  integer(kind=npar_k),parameter :: icont   = 1
  integer(kind=npar_k),parameter :: itau    = 2
  integer(kind=npar_k),parameter :: ivelo   = 3
  integer(kind=npar_k),parameter :: ifwhm   = 4
  !
  public cubefit_function_spectral_absorption_init, cubefit_function_spectral_absorption_minimize
  public cubefit_function_spectral_absorption_extract, cubefit_function_spectral_absorption_residuals
  public cubefit_function_spectral_absorption_npar, cubefit_function_spectral_absorption_user2par
  public cubefit_function_spectral_absorption_par2spec, cubefit_function_spectral_absorption_spec2par
  public cubefit_function_spectral_absorption_iterate,cubefit_function_spectral_absorption_wind2par
  public cubefit_function_spectral_absorption_flags, cubefit_function_spectral_absorption_doprofile
  public cubefit_function_spectral_absorption_units
  private
  !
contains
    !
  subroutine cubefit_function_spectral_absorption_init(par,obs,minuit,error)
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    type(spectral_pars_t), intent(inout) :: par
    type(spectral_obs_t),  intent(in)    :: obs
    type(fit_minuit_t),    intent(inout) :: minuit
    logical,               intent(inout) :: error
    !
    real(kind=coor_k) :: vsup,vinf
    real(kind=sign_k) :: val,area,ymin,ymax,cont
    character(len=mess_l) :: mess
    integer(kind=4) :: ipara,ifunc
    integer(kind=chan_k) :: ichan
    character(len=*), parameter :: rname='SPECTRAL>ABSORPTION>INIT'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')    
    !
    if (.not.obs%hfs%loaded) then
       call cubefit_message(seve%e,rname,'No HFS structure loaded')
       error = .true.
       return
    endif
    !
    ! Starting values
    if (par%nfunc.eq.0) then
       par%leaders(:) = 0
       par%flag(:,:) = 0
       par%errs(:) = 0
       ymax=0.
       ymin=0.
       area=0.
       vinf=minval(obs%spec%v)
       vsup=maxval(obs%spec%v)
       cont = 0.5*(obs%spec%t(obs%ifirst)+obs%spec%t(obs%ilast))
       do ichan=obs%ifirst+1,obs%ilast-1
          if (obs%wfit(ichan).ne.0) then
             val = ( obs%spec%t(ichan) + &
                  obs%wfit(ichan-1)*obs%spec%t(ichan-1) +   &
                  obs%wfit(ichan+1)*obs%spec%t(ichan+1) )   &
                  / (1+obs%wfit(ichan-1)+obs%wfit(ichan+1))
             val = val - cont
             if (val.ge.ymax) then
                ymax = val
                vsup = obs%spec%v(ichan)
             endif
             if (val.le.ymin) then
                ymin = val
                vinf = obs%spec%v(ichan)
             endif
             area=area+val*abs((obs%spec%v(ichan+1)-obs%spec%v(ichan-1)))
          endif
       enddo
       area = area*0.5
       if (abs(ymin).lt.abs(ymax)) then
          par%pars(ivelo)=vsup
          par%pars(itau)=ymax
       else
          par%pars(ivelo)=vinf
          par%pars(itau)=ymin
       endif
       par%pars(ifwhm) = abs(area/par%pars(itau)/1.064467)/2.    ! Take care of satellites
       par%pars(icont) = cont
       if (par%pars(ifwhm).lt.2*obs%deltav) par%pars(ifwhm) = 2*obs%deltav
       minuit%nu=cubefit_function_spectral_absorption_npar(1)
    else
       minuit%nu=cubefit_function_spectral_absorption_npar(par%nfunc)
    endif
    !
    ipara=0
    write(mess,'(a)') 'Input Parameters           Tau      Position         Width'
    call cubefit_message(fitseve%others,rname,mess)
    do ifunc=1,max(par%nfunc,1)
       write (mess,1002) par%pars(ipara+itau),par%pars(ipara+ivelo),par%pars(ipara+ifwhm)
       call cubefit_message(fitseve%others,rname,mess)
       ipara=ipara+npara
    enddo
    !
    ! Set Up Parameters
    minuit%u(icont) = par%pars(icont)
    minuit%alim(icont) = absorption_tau_min*par%pars(icont)
    minuit%blim(icont) = absorption_tau_max*par%pars(icont)
    minuit%werr(icont) = obs%sigbase
    ipara=2
    do ifunc=1,max(par%nfunc,1)
       !
       ! Optical depth
       minuit%u(ipara)=par%pars(ipara)
       minuit%werr(ipara)=obs%sigbase
       if ( mod(par%flag(ifunc,itau),2).ne.0 .and. par%nfunc.ne.0) minuit%werr(ipara)=0.
       if (minuit%u(ipara).ne.0.) then
          minuit%alim(ipara)=min(0.d0,8.d0*minuit%u(ipara))
          minuit%blim(ipara)=max(0.d0,8.d0*minuit%u(ipara))
       else
          minuit%lcode(ipara)=1
       endif
       ipara=ipara+1
       !
       ! Velocity
       minuit%u(ipara)=par%pars(ipara)
       minuit%werr(ipara)=obs%deltav
       if ( mod(par%flag(ifunc,ivelo),2).ne.0 .and. par%nfunc.ne.0) minuit%werr(ipara)=0.
       minuit%alim(ipara)=minuit%u(ipara)-0.15*obs%spec%n*obs%deltav
       minuit%blim(ipara)=minuit%u(ipara)+0.15*obs%spec%n*obs%deltav
       ipara=ipara+1
       !
       ! Line Width
       minuit%u(ipara)=abs(par%pars(ipara))/1.665109
       minuit%werr(ipara)=2*obs%deltav
       if ( mod(par%flag(ifunc,ifwhm),2).ne.0 .and. par%nfunc.ne.0 ) minuit%werr(ipara)=0.
       minuit%alim(ipara)=0.2*obs%deltav
       minuit%blim(ipara)=0.5*obs%spec%n*obs%deltav
       ipara=ipara+1
    enddo
    !
1002 format((3(f14.3)))
  end subroutine cubefit_function_spectral_absorption_init
  !
  subroutine cubefit_function_spectral_absorption_minimize(npar,grad,chi2,pars,iflag,obs)
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    integer(kind=npar_k),   intent(in)    :: npar        ! Number of parameters
    real(kind=grad_k),      intent(out)   :: grad(npar)  ! Gradientes
    real(kind=chi2_k),      intent(out)   :: chi2        ! chi squared
    real(kind=para_k),      intent(in)    :: pars(npar)  ! Parameter values
    integer(kind=4),        intent(in)    :: iflag       ! Code operation
    type(spectral_obs_t),   intent(inout) :: obs         ! Observation
    !
    integer(kind=chan_k) :: ichan
    real(kind=coor_k) :: xvel
    real(kind=sign_k) :: pred
    real(kind=grad_k), allocatable :: parsgrad(:)
    real(kind=8) :: arg,aux
    real(kind=chi2_k) :: chi2loc
    real(kind=para_k) :: tau,velo,fwhm,acoeff,bcoeff,ccoeff
    integer(kind=func_k) :: ifunc,ihfs
    integer(kind=npar_k) :: ipara,it,iv,id,ntot
    character(len=*), parameter :: rname='SPECTRAL>ABSORPTION>MINIMIZE'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    if (iflag.eq.code_minuit_rms) then
       call obs%sigma(cubefit_function_spectral_absorption_profile,.false.)
       return
    endif
    !
    grad(:) = 0.
    chi2 = 0.
    ntot = cubefit_function_spectral_absorption_npar(max(obs%par%nfunc,1))
    !
    allocate(parsgrad(ntot))
    do ichan=obs%ifirst,obs%ilast
       !
       ! Skip over masked area or bad channels
       if (obs%wfit(ichan).gt.0) then
          xvel = obs%spec%v(ichan)
          pred = 0
          do ifunc = 1, max(obs%par%nfunc,1)
             acoeff = 0
             bcoeff = 0
             ccoeff = 0
             it = (ifunc-1)*3+2
             iv = it+1
             id = iv+1
             tau  = pars(it)
             velo = pars(iv)
             fwhm = pars(id)
             do ihfs=1, obs%hfs%n
                arg = (xvel-obs%hfs%vel(ihfs)-velo)/fwhm
                if (abs(arg).lt.4.) then
                   aux = obs%hfs%rel(ihfs)*exp(-arg**2)
                   acoeff = acoeff + aux
                   aux = aux*2.*arg/fwhm
                   bcoeff = bcoeff + aux
                   ccoeff = ccoeff + aux*arg
                endif
             enddo
             pred = pred+acoeff*tau
             parsgrad(it) = acoeff
             parsgrad(iv) = bcoeff*tau
             parsgrad(id) = ccoeff*tau
          enddo
          pred = exp(-pred)
          chi2loc = obs%spec%t(ichan) - pred*pars(icont)
          chi2 = chi2 + chi2loc**2
          pred = 2*pred*chi2loc
          grad(icont) = grad(icont) - pred
          do ipara =2, ntot
             grad(ipara) = grad(ipara)+pred*parsgrad(ipara)
          enddo
       endif
    enddo
  end subroutine cubefit_function_spectral_absorption_minimize
  !
  subroutine cubefit_function_spectral_absorption_extract(minuit,obs,par,error)
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    type(fit_minuit_t),    intent(inout) :: minuit
    type(spectral_obs_t),  intent(inout) :: obs
    type(spectral_pars_t), intent(inout) :: par
    logical,               intent(inout) :: error
    !
    integer(kind=func_k) :: ifunc
    integer(kind=npar_k) :: ipara
    character(len=*), parameter :: rname='SPECTRAL>ABSORPTION>EXTRACT'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    ! Update Parameters
    par%pars(icont) = minuit%u(icont)
    par%errs(icont) = minuit%werr(icont)
    ipara=0
    do ifunc=1,max(par%nfunc,1)
       par%pars(ipara+itau)  = minuit%u(ipara+itau)
       par%pars(ipara+ivelo) = minuit%u(ipara+ivelo)
       par%pars(ipara+ifwhm) = minuit%u(ipara+ifwhm)*1.665109
       par%errs(ipara+itau)  = minuit%werr(ipara+itau)
       par%errs(ipara+ivelo) = minuit%werr(ipara+ivelo)
       par%errs(ipara+ifwhm) = minuit%werr(ipara+ifwhm)*1.665109
       ipara=ipara+npara
    enddo
  end subroutine cubefit_function_spectral_absorption_extract
  !
  subroutine cubefit_function_spectral_absorption_residuals(obs,spec,error)
    use cubemain_spectrum_real
    !------------------------------------------------------------------------
    ! Compute the residuals of a absorption fit
    !------------------------------------------------------------------------
    type(spectral_obs_t), intent(inout) :: obs          
    type(spectrum_t),     intent(inout) :: spec         
    logical,              intent(inout) :: error
    !
    integer(kind=chan_k) :: ichan
    real(kind=coor_k) :: xvel
    real(kind=sign_k) :: pred
    character(len=*), parameter :: rname="SPECTRAL>ABSORPTION>RESIDUALS"
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    do ichan=1, spec%n
       xvel = obs%spec%v(ichan)
       pred = cubefit_function_spectral_absorption_profile(obs,xvel,0)
       spec%t(ichan) = obs%spec%t(ichan) - pred
    enddo
  end subroutine cubefit_function_spectral_absorption_residuals
  !
  subroutine cubefit_function_spectral_absorption_doprofile(ifunc,obs,spec,error)
    use cubemain_spectrum_real
    !------------------------------------------------------------------------
    ! Compute the doprofile of a absorption fit
    !------------------------------------------------------------------------
    integer(kind=func_k), intent(in)    :: ifunc
    type(spectral_obs_t), intent(inout) :: obs          
    type(spectrum_t),     intent(inout) :: spec         
    logical,              intent(inout) :: error
    !
    integer(kind=chan_k) :: ichan
    real(kind=coor_k) :: xvel
    real(kind=sign_k) :: pred
    character(len=*), parameter :: rname="SPECTRAL>ABSORPTION>DOPROFILE"
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    do ichan=1, spec%n
       xvel = obs%spec%v(ichan)
       pred = cubefit_function_spectral_absorption_profile(obs,xvel,ifunc)
       spec%t(ichan) = pred
    enddo
  end subroutine cubefit_function_spectral_absorption_doprofile
  !
  subroutine cubefit_function_spectral_absorption_user2par(flag,pars,par,error)
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    integer(kind=flag_k),  intent(in)    :: flag(:)
    real(kind=para_k),     intent(in)    :: pars(:)
    type(spectral_pars_t), intent(inout) :: par
    logical,               intent(inout) :: error
    !
    integer(kind=npar_k) :: ipara,jpara
    integer(kind=func_k) :: ifunc
    character(len=*), parameter :: rname='SPECTRAL>ABSORPTION>USER2PAR'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    par%leaders(:) = 0
    par%flag(:,:)  = 0
    par%errs(:)    = 0.
    !
    par%flag(1,icont) = flag(icont)
    par%pars(icont)   = pars(icont)
    jpara = 2
    do ifunc=1,par%nfunc
       do ipara=2,npara+1
          par%flag(ifunc,ipara) = flag(jpara)
          par%pars(jpara)       = pars(jpara)
          jpara=jpara+1
       enddo ! ipara
    enddo ! ifunc
    !
    call par%check_line(itau,error)
    if (error) return
    call par%check_line(ivelo,error)
    if (error) return
    call par%check_line(ifwhm,error)
    if (error) return
  end subroutine cubefit_function_spectral_absorption_user2par
  !
  subroutine cubefit_function_spectral_absorption_par2spec(par,spec,error)
    use cubemain_spectrum_real
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(spectral_pars_t), intent(in)    :: par
    type(spectrum_t),      intent(inout) :: spec
    logical,               intent(inout) :: error
    !
    integer(kind=func_k) :: ifunc
    integer(kind=chan_k) :: ichan
    integer(kind=npar_k) :: ipara
    character(len=*), parameter :: rname='SPECTRAL>ABSORPTION>PAR2SPEC'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    ichan = ndaps+1
    spec%t(ichan) = par%flag(1,icont)
    ichan = ichan+1
    spec%t(ichan) = par%pars(icont)
    ichan = ichan+1
    spec%t(ichan) = par%errs(icont)
    ipara = 0
    do ifunc=1,max(par%nfunc,1)
       ichan = ichan+1
       spec%t(ichan) = par%flag(ifunc,itau)
       ichan = ichan+1
       spec%t(ichan) = par%pars(ipara+itau)
       ichan = ichan+1
       spec%t(ichan) = par%errs(ipara+itau)
       ichan = ichan+1
       spec%t(ichan) = par%flag(ifunc,ivelo)
       ichan = ichan+1
       spec%t(ichan) = par%pars(ipara+ivelo)
       ichan = ichan+1
       spec%t(ichan) = par%errs(ipara+ivelo)
       ichan = ichan+1
       spec%t(ichan) = par%flag(ifunc,ifwhm)
       ichan = ichan+1
       spec%t(ichan) = par%pars(ipara+ifwhm)
       ichan = ichan+1
       spec%t(ichan) = par%errs(ipara+ifwhm)
       ipara = ipara + npara
    enddo
  end subroutine cubefit_function_spectral_absorption_par2spec
  !
  subroutine cubefit_function_spectral_absorption_spec2par(spec,par,error)
    use cubemain_spectrum_real
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(spectrum_t),      intent(in)    :: spec
    type(spectral_pars_t), intent(inout) :: par
    logical,               intent(inout) :: error
    !
    integer(kind=npar_k) :: ipara
    integer(kind=func_k) :: ifunc
    integer(kind=chan_k) :: ichan
    character(len=*), parameter :: rname='SPECTRAL>ABSORPTION>SPEC2PAR'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    ichan = ndaps+1
    par%flag(1,icont) = spec%t(ichan)  
    ichan = ichan+1
    par%pars(icont)   = spec%t(ichan)  
    ichan = ichan+1
    par%errs(icont)   = spec%t(ichan) 
    ipara = 0
    do ifunc=1,max(par%nfunc,1)
       ichan = ichan+1
       par%flag(ifunc,itau)  = spec%t(ichan)
       ichan = ichan+1
       par%pars(ipara+itau)   = spec%t(ichan)
       ichan = ichan+1
       par%errs(ipara+itau)   = spec%t(ichan)
       ichan = ichan+1
       par%flag(ifunc,ivelo) = spec%t(ichan)
       ichan = ichan+1
       par%pars(ipara+ivelo)  = spec%t(ichan)
       ichan = ichan+1
       par%errs(ipara+ivelo)  = spec%t(ichan)
       ichan = ichan+1
       par%flag(ifunc,ifwhm) = spec%t(ichan)
       ichan = ichan+1
       par%pars(ipara+ifwhm)  = spec%t(ichan)
       ichan = ichan+1
       par%errs(ipara+ifwhm)  = spec%t(ichan)
       ipara = ipara + npara
    enddo
  end subroutine cubefit_function_spectral_absorption_spec2par
  !
  subroutine cubefit_function_spectral_absorption_iterate(par,error)
    !----------------------------------------------------------------------
    ! This routine is empty as no transformation is needed when we are
    ! iterating an ABSORPTION fit
    !----------------------------------------------------------------------
    type(spectral_pars_t), intent(inout) :: par
    logical,               intent(inout) :: error
    !
    character(len=*), parameter :: rname='SPECTRAL>ABSORPTION>ITERATE'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
  end subroutine cubefit_function_spectral_absorption_iterate
  !
  subroutine cubefit_function_spectral_absorption_wind2par(obs,wind,par,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(spectral_obs_t),  intent(in)    :: obs
    integer(kind=chan_k),  intent(in)    :: wind(:)
    type(spectral_pars_t), intent(inout) :: par
    logical,               intent(inout) :: error
    !
    integer(kind=chan_k) :: first,last
    integer(kind=func_k) :: ifunc
    integer(kind=npar_k) :: ipara
    real(kind=para_k) :: area,velo,fwhm
    character(len=*), parameter :: rname='SPECTRAL>ABSORPTION>WIND2PAR'
    !
    par%errs(:) = 0
    par%leaders(:)  = 0
    par%pars(icont) = 1.0
    !
    ipara = 0
    do ifunc=1,par%nfunc
       first = wind(2*ifunc-1)
       last  = wind(2*ifunc)
       par%flag(ifunc,:)   = 0
       !
       call obs%est_gauss(first,last,area,velo,fwhm,error)
       if (error) return
       if (fwhm.lt.obs%deltav) fwhm = obs%deltav
       if (fwhm.gt.obs%deltav*obs%spec%n*0.5) fwhm = obs%deltav*obs%spec%n*0.5
       par%pars(ipara+itau)  = area 
       par%pars(ipara+ivelo) = velo 
       par%pars(ipara+ifwhm) = fwhm
       ipara = ipara + npara
    enddo
  end subroutine cubefit_function_spectral_absorption_wind2par
  !
  !----------------------------------------------------------------------
  !
  function cubefit_function_spectral_absorption_profile(obs,xvel,ifunc) result(absorption)
    !----------------------------------------------------------------------
    ! Compute the value of a hyperfine line or a sum of hyperfine
    ! lines at xvel
    !----------------------------------------------------------------------
    type(spectral_obs_t),   intent(in)    :: obs         ! Observation
    real(kind=coor_k),      intent(in)    :: xvel        ! Coordinate to compute value
    integer(kind=func_k),   intent(in)    :: ifunc       ! Which absorption is to be computed
    !
    real(kind=sign_k) :: absorption
    !
    integer(kind=func_k) :: jfunc
    !
    absorption=0.
    if (ifunc.eq.0) then
       do jfunc=1,max(obs%par%nfunc,1)
          absorption = absorption+absorption_single(jfunc)
       enddo
       absorption = exp(-absorption)
    else
       absorption = exp(-absorption_single(ifunc))
    endif
    ! multiply by continuum level
    absorption = absorption*obs%par%pars(icont)
  contains
    function absorption_single(iwhich)
      !----------------------------------------------------------------------
      !
      !----------------------------------------------------------------------
      integer(kind=func_k),   intent(in)    :: iwhich   ! Which absorption is to be computed
      real(kind=sign_k) :: absorption_single  
      !
      real(kind=para_k) :: tau, velo, fwhm, arg
      integer(kind=func_k) :: ihfs
      !
      tau =obs%par%pars((iwhich-1)*npara+itau)
      velo=obs%par%pars((iwhich-1)*npara+ivelo)
      fwhm=obs%par%pars((iwhich-1)*npara+ifwhm)/1.665109
      absorption_single = 0.
      if (tau.ne.0 .and. fwhm.ne.0) then
         do ihfs=1,obs%hfs%n
            arg = abs ((xvel-obs%hfs%vel(ihfs)-velo)/fwhm)
            if (arg.lt.4.) then
               absorption_single = absorption_single + obs%hfs%rel(ihfs)*exp(-(arg**2))
            endif
         enddo
      endif
      absorption_single = absorption_single*tau
    end function absorption_single
  end function cubefit_function_spectral_absorption_profile
  !
  subroutine cubefit_function_spectral_absorption_flags(ipara,funcflag,paraflag,error)
    use cubedag_allflags
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    integer(kind=npar_k), intent(in)    :: ipara
    type(flag_t),         intent(out)   :: funcflag
    type(flag_t),         intent(out)   :: paraflag
    logical,              intent(inout) :: error
    !
    integer(kind=func_k) :: ifunc
    integer(kind=npar_k) :: mypar
    character(len=*), parameter :: rname='SPECTRAL>ABSORPTION>FLAGS'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    if (ipara.eq.1) then
       paraflag = flag_continuum
       funcflag = flag_one
       return
    else
       mypar = ipara-1
       ifunc  = mypar/npara+1
       select case(mod(mypar,npara))
       case(1)
          paraflag = flag_tau
       case(2)
          paraflag = flag_velocity
       case(0)
          paraflag = flag_fwhm
          ifunc  = ifunc-1
       end select
       call cubefit_func2flag(ifunc,funcflag,error)
       if (error) return
    endif
  end subroutine cubefit_function_spectral_absorption_flags
  !
  subroutine cubefit_function_spectral_absorption_units(ipara,unit,error)
    use cubetools_unit_setup
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    integer(kind=npar_k), intent(in)    :: ipara
    character(len=*),     intent(out)   :: unit
    logical,              intent(inout) :: error
    !
    integer(kind=func_k) :: ifunc
    integer(kind=npar_k) :: mypar
    character(len=*), parameter :: rname='SPECTRAL>ABSORPTION>UNITS'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    if (ipara.eq.1) then
       unit = strg_id
    else
       mypar = ipara-1
       ifunc  = mypar/npara+1
       select case(mod(mypar,npara))
       case(1)
          unit = '---' ! no unit for opacity
       case(2)
          unit = unit_velo%prog_name()
       case(0)
          unit = unit_velo%prog_name()
       end select
    endif
  end subroutine cubefit_function_spectral_absorption_units
  !
  function cubefit_function_spectral_absorption_npar(nfunc) result(npar)
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    integer(kind=func_k), intent(in) :: nfunc
    !
    integer(kind=npar_k) :: npar
    !
    npar = npara*nfunc+1
  end function cubefit_function_spectral_absorption_npar
end module cubefit_function_spectral_absorption
