!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubefit_hfs
  use cubefit_messaging
  use cubefit_parameters
  !
  public :: hfs_user_t,hfs_prog_t
  public :: cubefit_hfs_register,cubefit_hfs_parse,cubefit_hfs_user2prog
  private
  !
  integer(kind=func_k),parameter :: mhyp = 40
  !
  type hfs_user_t
     character(len=file_l) :: file = strg_unk
     logical               :: do
  end type hfs_user_t
  !
  type hfs_prog_t
     integer(kind=func_k)          :: n      = 0
     character(len=file_l)         :: file
     real(kind=coor_k),allocatable :: vel(:)
     real(kind=sign_k),allocatable :: rel(:)
     logical                       :: loaded = .false.
  end type hfs_prog_t
  !
contains
  !
  subroutine cubefit_hfs_register(option,error)
    use cubetools_structure
    !----------------------------------------------------------------------
    ! Register a /HFS option 
    ! ----------------------------------------------------------------------
    type(option_t), pointer, intent(out)   :: option
    logical,                 intent(inout) :: error
    !
    type(standard_arg_t) :: stdarg
    !
    character(len=*), parameter :: rname='HFS>REGISTER'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    call cubetools_register_option(&
         'HFS','hfsfile',&
         'Provide the HFS file',&
         'Provide the file describing the HyperFine Structure (HFS)&
         & of the line to be fitted using the methods: HFS and&
         & ABSORPTION', &
         option,error)
    if (error) return
    call stdarg%register( &
         'hfsfile',  &
         'File containing the HFS structure', &
         strg_id,&
         code_arg_mandatory, error)
    if (error) return
  end subroutine cubefit_hfs_register
  !
  subroutine cubefit_hfs_parse(line,opt,user,error)
    use cubetools_structure
    !----------------------------------------------------------------------
    ! /HFS file
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    type(option_t),   intent(in)    :: opt
    type(hfs_user_t), intent(out)   :: user
    logical,          intent(inout) :: error
    !
    character(len=*), parameter     :: rname='HFS>USER2PROG'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    call opt%present(line,user%do,error)
    if (error) return
    if (user%do) then
       call cubetools_getarg(line,opt,1,user%file,mandatory,error)
       if (error) return
    endif
  end subroutine cubefit_hfs_parse
  !
  subroutine cubefit_hfs_user2prog(user,prog,error)
    use gkernel_interfaces
    use cubetools_user2prog
    use cubetools_unit
    use cubetools_ascii
    !----------------------------------------------------------------------
    !
    ! ----------------------------------------------------------------------
    type(hfs_user_t), intent(in)    :: user
    type(hfs_prog_t), intent(out)   :: prog
    logical,          intent(inout) :: error
    !
    integer(kind=4) :: ier,nl,pos,len
    integer(kind=func_k) :: ihfs
    type(unit_user_t) :: nounit
    type(ascii_file_t) :: file
    character(len=writ_l) :: line
    character(len=argu_l) :: str
    
    real(kind=coor_k), parameter :: defvelo   = 0d0
    real(kind=sign_k), parameter :: defrelint = 0.0
    character(len=*), parameter     :: rname='HFS>USER2PROG'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    if (user%do) then
       call sic_parse_file(user%file,' ','.hfs',prog%file)
       call file%open(prog%file,'readonly',error)
       if (error) return
       !
       ! Read number of components
       call file%read_next(line,error)
       if (error) return
       read (line,*,err=10,iostat=ier) prog%n
       if (prog%n.gt.mhyp) then
          call cubefit_message(seve%e,rname,'Too many HFS components')
          call file%close(error)
          error = .true.
          return
       endif
       call nounit%get_from_code(code_unit_unk,error)
       if (error) return
       allocate(prog%vel(prog%n),prog%rel(prog%n),stat=ier)
       if (failed_allocate(rname,'HFS information',ier,error)) return
       do ihfs=1,prog%n
          ! Read in local buffer
          call file%read_next(line,error)
          if (error) return
          line=trim(adjustl(line))
          if (ier.ne.0) goto 10
          nl = len_trim(line)
          !
          ! Class documentation promises that a fraction can be used for
          ! ratios. Use the math decoding function for this.
          pos = 1
          ! Get and decode 1st arg
          call sic_next(line(pos:nl),str,len,pos,dotab=.true.)
          call cubetools_user2prog_resolve_star(str,nounit,defvelo,prog%vel(ihfs),error)
          if (error) goto 10
          ! Get and decode 2nd arg
          call sic_next(line(pos:nl),str,len,pos,dotab=.true.)
          call cubetools_user2prog_resolve_star(str,nounit,defrelint,prog%rel(ihfs),error)
          if (error) goto 10
       enddo
       prog%loaded = .true.
       call file%close(error)
       if (error) return
       return
       !
10     call file%close(error)
       call cubefit_message(seve%e,rname,'Error reading HFS description file')
    endif
  end subroutine cubefit_hfs_user2prog
  !
!!$  subroutine cubefit_hfs_nh3(method,hfs,error)
!!$    use gkernel_interfaces
!!$    use cubefit_parameters
!!$    !----------------------------------------------------------------------
!!$    ! Fill the HFS structure with NH3 parameters
!!$    !----------------------------------------------------------------------
!!$    integer(kind=code_k), intent(in)    :: method
!!$    type(hfs_prog_t),     intent(inout) :: hfs
!!$    logical,              intent(inout) :: error
!!$    !
!!$    integer(kind=4) :: ier
!!$    character(len=*), parameter     :: rname='HFS>NH3'
!!$    !
!!$    call cubefit_message(fitseve%trace,rname,'Welcome')
!!$    !
!!$    if (hfs%loaded) then
!!$       call cubefit_message(seve%e,rname,'Cannot load values onto a previously loaded HFS')
!!$       error = .true.
!!$       return
!!$    endif
!!$    select case(method)
!!$    case(inh31)
!!$       hfs%n = 18
!!$       allocate(hfs%vel(hfs%n),hfs%rel(hfs%n),stat=ier)
!!$       if (failed_allocate(rname,'HFS information',ier,error)) return
!!$       hfs%vel( 1) =     19.851279
!!$       hfs%vel( 2) =     19.315905
!!$       hfs%vel( 3) =      7.886691
!!$       hfs%vel( 4) =      7.469667
!!$       hfs%vel( 5) =      7.351317
!!$       hfs%vel( 6) =      0.460409
!!$       hfs%vel( 7) =      0.322042
!!$       hfs%vel( 8) =     -0.075168
!!$       hfs%vel( 9) =     -0.213003
!!$       hfs%vel(10) =      0.311034
!!$       hfs%vel(11) =      0.192266
!!$       hfs%vel(12) =     -0.132382
!!$       hfs%vel(13) =     -0.250923
!!$       hfs%vel(14) =     -7.233485
!!$       hfs%vel(15) =     -7.372800
!!$       hfs%vel(16) =     -7.815255
!!$       hfs%vel(17) =    -19.411734
!!$       hfs%vel(18) =    -19.549987
!!$       hfs%rel( 1) =      0.074074
!!$       hfs%rel( 2) =      0.148148
!!$       hfs%rel( 3) =      0.092593
!!$       hfs%rel( 4) =      0.166667
!!$       hfs%rel( 5) =      0.018519
!!$       hfs%rel( 6) =      0.037037
!!$       hfs%rel( 7) =      0.018519
!!$       hfs%rel( 8) =      0.018519
!!$       hfs%rel( 9) =      0.092593
!!$       hfs%rel(10) =      0.033333
!!$       hfs%rel(11) =      0.300000
!!$       hfs%rel(12) =      0.466667
!!$       hfs%rel(13) =      0.033333
!!$       hfs%rel(14) =      0.092593
!!$       hfs%rel(15) =      0.018519
!!$       hfs%rel(16) =      0.166667
!!$       hfs%rel(17) =      0.074074
!!$       hfs%rel(18) =      0.148148
!!$    case(inh32)
!!$       hfs%n = 21
!!$       allocate(hfs%vel(hfs%n),hfs%rel(hfs%n),stat=ier)
!!$       if (failed_allocate(rname,'HFS information',ier,error)) return
!!$       hfs%vel( 1) =     26.526252
!!$       hfs%vel( 2) =     26.011126
!!$       hfs%vel( 3) =     25.950454
!!$       hfs%vel( 4) =     16.391711
!!$       hfs%vel( 5) =     16.379289
!!$       hfs%vel( 6) =     15.864175
!!$       hfs%vel( 7) =      0.562503
!!$       hfs%vel( 8) =      0.528408
!!$       hfs%vel( 9) =      0.523745
!!$       hfs%vel(10) =      0.013282
!!$       hfs%vel(11) =     -0.003791
!!$       hfs%vel(12) =     -0.013282
!!$       hfs%vel(13) =     -0.501831
!!$       hfs%vel(14) =     -0.531340
!!$       hfs%vel(15) =     -0.589080
!!$       hfs%vel(16) =    -15.854685
!!$       hfs%vel(17) =    -16.369798
!!$       hfs%vel(18) =    -16.382221
!!$       hfs%vel(19) =    -25.950454
!!$       hfs%vel(20) =    -26.011126
!!$       hfs%vel(21) =    -26.526252
!!$       hfs%rel( 1) =      0.004186
!!$       hfs%rel( 2) =      0.037674
!!$       hfs%rel( 3) =      0.020930
!!$       hfs%rel( 4) =      0.037209
!!$       hfs%rel( 5) =      0.026047
!!$       hfs%rel( 6) =      0.001860
!!$       hfs%rel( 7) =      0.020930
!!$       hfs%rel( 8) =      0.011628
!!$       hfs%rel( 9) =      0.010631
!!$       hfs%rel(10) =      0.267442
!!$       hfs%rel(11) =      0.499668
!!$       hfs%rel(12) =      0.146512
!!$       hfs%rel(13) =      0.011628
!!$       hfs%rel(14) =      0.010631
!!$       hfs%rel(15) =      0.020930
!!$       hfs%rel(16) =      0.001860
!!$       hfs%rel(17) =      0.026047
!!$       hfs%rel(18) =      0.037209
!!$       hfs%rel(19) =      0.020930
!!$       hfs%rel(20) =      0.037674
!!$       hfs%rel(21) =      0.004186
!!$    case(inh33)
!!$       hfs%n = 26
!!$       allocate(hfs%vel(hfs%n),hfs%rel(hfs%n),stat=ier)
!!$       if (failed_allocate(rname,'HFS information',ier,error)) return
!!$       hfs%vel( 1) =     29.195098
!!$       hfs%vel( 2) =     29.044147
!!$       hfs%vel( 3) =     28.941877
!!$       hfs%vel( 4) =     28.911408
!!$       hfs%vel( 5) =     21.234827
!!$       hfs%vel( 6) =     21.214619
!!$       hfs%vel( 7) =     21.136387
!!$       hfs%vel( 8) =     21.087456
!!$       hfs%vel( 9) =      1.005122
!!$       hfs%vel(10) =      0.806082
!!$       hfs%vel(11) =      0.778062
!!$       hfs%vel(12) =      0.628569
!!$       hfs%vel(13) =      0.016754
!!$       hfs%vel(14) =     -0.005589
!!$       hfs%vel(15) =     -0.013401
!!$       hfs%vel(16) =     -0.639734
!!$       hfs%vel(17) =     -0.744554
!!$       hfs%vel(18) =     -1.031924
!!$       hfs%vel(19) =    -21.125222
!!$       hfs%vel(20) =    -21.203441
!!$       hfs%vel(21) =    -21.223649
!!$       hfs%vel(22) =    -21.076291
!!$       hfs%vel(23) =    -28.908067
!!$       hfs%vel(24) =    -28.938523
!!$       hfs%vel(25) =    -29.040794
!!$       hfs%vel(26) =    -29.191744
!!$       hfs%rel( 1) =      0.012263
!!$       hfs%rel( 2) =      0.008409
!!$       hfs%rel( 3) =      0.003434
!!$       hfs%rel( 4) =      0.005494
!!$       hfs%rel( 5) =      0.006652
!!$       hfs%rel( 6) =      0.008852
!!$       hfs%rel( 7) =      0.004967
!!$       hfs%rel( 8) =      0.011589
!!$       hfs%rel( 9) =      0.019228
!!$       hfs%rel(10) =      0.010387
!!$       hfs%rel(11) =      0.010820
!!$       hfs%rel(12) =      0.009482
!!$       hfs%rel(13) =      0.293302
!!$       hfs%rel(14) =      0.459109
!!$       hfs%rel(15) =      0.177372
!!$       hfs%rel(16) =      0.009482
!!$       hfs%rel(17) =      0.010820
!!$       hfs%rel(18) =      0.019228
!!$       hfs%rel(19) =      0.004967
!!$       hfs%rel(20) =      0.008852
!!$       hfs%rel(21) =      0.006652
!!$       hfs%rel(22) =      0.011589
!!$       hfs%rel(23) =      0.005494
!!$       hfs%rel(24) =      0.003434
!!$       hfs%rel(25) =      0.008409
!!$       hfs%rel(26) =      0.012263
!!$    case default
!!$       call cubefit_message(seve%e,rname,'Unsupported method: '//methnames(method))
!!$       error = .true.
!!$       return
!!$    end select
!!$    hfs%loaded = .true.
!!$  end subroutine cubefit_hfs_nh3
end module cubefit_hfs
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
