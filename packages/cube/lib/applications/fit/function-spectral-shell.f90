module cubefit_function_spectral_shell
  use fit_minuit
  !
  use cubefit_messaging
  use cubefit_spectral_parameters
  use cubefit_spectral_obs
  !
  integer(kind=npar_k),parameter :: npara = 4
  integer(kind=npar_k),parameter :: iarea   = 1
  integer(kind=npar_k),parameter :: ifreq   = 2
  integer(kind=npar_k),parameter :: ifwzl   = 3
  integer(kind=npar_k),parameter :: ihorn   = 4
  !
  ! Contrary to other methods this ones uses the frequency axis
  !
  public cubefit_function_spectral_shell_init, cubefit_function_spectral_shell_minimize
  public cubefit_function_spectral_shell_extract, cubefit_function_spectral_shell_residuals
  public cubefit_function_spectral_shell_npar, cubefit_function_spectral_shell_user2par
  public cubefit_function_spectral_shell_par2spec, cubefit_function_spectral_shell_spec2par
  public cubefit_function_spectral_shell_iterate,cubefit_function_spectral_shell_wind2par
  public cubefit_function_spectral_shell_flags, cubefit_function_spectral_shell_doprofile
  public cubefit_function_spectral_shell_units
  private
  !
contains
  !
  subroutine cubefit_function_spectral_shell_init(par,obs,minuit,error)
    !------------------------------------------------------------------------
    ! 
    !------------------------------------------------------------------------
    type(spectral_pars_t), intent(inout) :: par
    type(spectral_obs_t),  intent(in)    :: obs
    type(fit_minuit_t),    intent(inout) :: minuit
    logical,               intent(inout) :: error
    !
    real(kind=coor_k) :: freq,fwzl
    real(kind=sign_k) :: val,area
    character(len=mess_l) :: mess
    integer(kind=4) :: ipara,ifunc
    integer(kind=chan_k) :: ichan
    character(len=*), parameter :: rname='SPECTRAL>SHELL>INIT'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')    
    !     
    ! Starting values
    if (par%nfunc.eq.0) then ! Automatic guess
       par%leaders(:) = 0
       par%flag(:,:) = 0
       par%errs(:) = 0
       area=0.
       freq=0.
       fwzl=0.
       do ichan=obs%ifirst+1,obs%ilast-1
          if (obs%wfit(ichan).ne.0 .and. abs(obs%spec%t(ichan)).gt.obs%sigbase) then
             val = obs%spec%t(ichan)
             area = area + val
             freq = freq + val * obs%spec%f(ichan)
             fwzl = fwzl + val * obs%spec%f(ichan)**2
          endif
       enddo
       if (area.ne.0) then
          freq = freq / area
          fwzl = fwzl / area
          par%pars(ifwzl) = obs%deltaf * sqrt (abs(fwzl-freq**2)*8.*alog(2.))
          par%pars(ifreq) = freq
          par%pars(iarea) = area * obs%deltaf
       else
          call cubefit_message(seve%e,rname,'Null area found, give explicit guesses')
          error =  .true.
          return
       endif
       minuit%nu=npara
    else ! Manipulate initial guesses from user
       minuit%nu=npara*par%nfunc
    endif
    minuit%nu=minuit%nu+npara
    !
    ! User feedback on par%parsameters used, useful for debug only...
    call cubefit_message(fitseve%others,rname,'Input Parameters:  Area Position Fwzl Horn')
    ipara = 0
    do ifunc=1,max(par%nfunc,1)
       write (mess,'(5x,4(5x,1pg11.4))') par%pars(ipara+iarea),par%pars(ipara+ifreq),par%pars(ipara+ifwzl),&
            par%pars(ipara+ihorn)
       call cubefit_message(fitseve%others,rname,mess)
       ipara=ipara+npara
    enddo
    !
    ! Set up Parameters
    ! Areas
    if (par%leaders(iarea).eq.0) then
       minuit%u(iarea)   = 1.0
       minuit%werr(iarea)= 0.0
    else
       ipara = (npara-1)*par%leaders(iarea)+iarea
       minuit%u(iarea)=par%pars(ipara)
       if (par%flag(par%leaders(iarea),iarea).eq.4) then
          minuit%werr(iarea)=0.
       else
          ! minuit%werr(K) = obs%sigbase*sqrt(abs(par%pars(k+2))*obs%deltaf)
          minuit%werr(iarea)=obs%sigbase*obs%deltaf*3.d0
          if (par%errs(ipara).ne.0) minuit%werr(iarea)=par%errs(ipara)
          if (minuit%u(iarea).ne.0.d0) then
             minuit%alim(iarea)=min(0.d0,8.d0*minuit%u(iarea))
             minuit%blim(iarea)=max(0.d0,8.d0*minuit%u(iarea))
          else
             minuit%lcode(iarea)=1
          endif
       endif
    endif
    !
    ! Frequencies
    if (par%leaders(ifreq).eq.0) then
       minuit%u(ifreq)=0.
       minuit%werr(ifreq)=0.
    else
       ipara = (npara-1)*par%leaders(ifreq)+ifreq
       minuit%u(ifreq)=par%pars(ipara)
       if (par%flag(par%leaders(ifreq),ifreq).eq.4) then
          minuit%werr(ifreq)=0.
       else
          minuit%werr(ifreq)=obs%deltaf
          if (par%errs(ipara).ne.0) minuit%werr(ifreq)=par%errs(ipara)
          minuit%alim(ifreq)=minuit%u(ifreq)-0.1*obs%spec%n*obs%deltaf
          minuit%blim(ifreq)=minuit%u(ifreq)+0.1*obs%spec%n*obs%deltaf
       endif
    endif
    !
    ! Line Widths
    if (par%leaders(ifwzl).eq.0) then
       minuit%u(ifwzl)=1.
       minuit%werr(ifwzl)=0.
    else
       ipara = (npara-1)*par%leaders(ifwzl)+ifwzl
       minuit%u(ifwzl)=abs(par%pars(ipara))
       if (par%flag(par%leaders(ifwzl),ifwzl).eq.4) then
          minuit%werr(ifwzl)=0.
       else
          minuit%werr(ifwzl)=obs%deltaf
          if (par%errs(ipara).ne.0) minuit%werr(ifwzl)=par%errs(ipara)
          minuit%alim(ifwzl)=obs%deltaf
          minuit%blim(ifwzl)=0.5*obs%spec%n*obs%deltaf
       endif
    endif
    !
    ! Horn to Center
    if (par%leaders(ihorn).eq.0) then
       minuit%u(ihorn)=1.
       minuit%werr(ihorn)=0.d0
    else
       ipara = (npara-1)*par%leaders(ihorn)+ihorn
       minuit%u(ihorn)=par%pars(ipara)
       if (par%flag(par%leaders(ihorn),ihorn).eq.4) then
          minuit%werr(ihorn)=0.d0
       else
          minuit%werr(ihorn)=0.05
          if (par%errs(ipara).ne.0) minuit%werr(ihorn)=par%errs(ipara)
          minuit%alim(ihorn)=-1.0
          minuit%blim(ihorn)=100.
       endif
    endif
    ! Set up parameters for Secondary Variables
    ipara=5
    do ifunc=1,max(par%nfunc,1)
       ! Area
       minuit%u(ipara)=par%pars(ipara-npara)
       if (par%flag(ifunc,iarea).eq.0 .or. par%nfunc.eq.0) then
          minuit%werr(ipara)=obs%sigbase*obs%deltaf*3.0
          if (par%errs(ipara-npara).ne.0) minuit%werr(ipara)=par%errs(ipara-npara)
          if (minuit%u(ipara).ne.0.) then
             minuit%alim(ipara)=min(0.d0,8.d0*minuit%u(ipara))
             minuit%blim(ipara)=max(0.d0,8.d0*minuit%u(ipara))
          else
             minuit%lcode(ipara)=1             ! No Boundaries if minuit%u(ipara)=0.
          endif
       else
          minuit%werr(ipara)=0.d0
          if (ifunc.eq.par%leaders(iarea)) minuit%u(ipara)=1.d0
       endif
       ipara=ipara+1
       !
       ! Velocity
       minuit%u(ipara)=par%pars(ipara-npara)
       if (par%flag(ifunc,ifreq).eq.0 .or. par%nfunc.eq.0) then
          minuit%werr(ipara)=obs%deltaf
          if (par%errs(ipara-npara).ne.0) minuit%werr(ipara)=par%errs(ipara-npara)
          minuit%alim(ipara)=minuit%u(ipara)-0.1*obs%spec%n*obs%deltaf
          minuit%blim(ipara)=minuit%u(ipara)+0.1*obs%spec%n*obs%deltaf
       else
          minuit%werr(ipara)=0.d0
          if (ifunc.eq.par%leaders(ifreq)) minuit%u(ipara)=0.
       endif
       ipara=ipara+1
       !
       ! Line Width
       minuit%u(ipara)=abs(par%pars(ipara-npara))
       if (par%flag(ifunc,ifwzl).eq.0 .or. par%nfunc.eq.0) then
          minuit%werr(ipara)=obs%deltaf
          if (par%errs(ipara-npara).ne.0) minuit%werr(ipara)=par%errs(ipara-npara)
          minuit%alim(ipara)=obs%deltaf
          minuit%blim(ipara)=0.5*obs%spec%n*obs%deltaf
       else
          minuit%werr(ipara)=0.d0
          if (ifunc.eq.par%leaders(ifwzl)) minuit%u(ipara)=1.
       endif
       ipara=ipara+1
       !
       ! Horn / Center ratio
       minuit%u(ipara)=par%pars(ipara-npara)
       if (par%flag(ifunc,ihorn).eq.0 .or. par%nfunc.eq.0) then
          minuit%werr(ipara)=0.05
          if (par%errs(ipara-npara).ne.0) minuit%werr(ipara)=par%errs(ipara-npara)
          minuit%alim(ipara)=-1.0
          minuit%blim(ipara)=100.
       else
          minuit%werr(ipara)=0.d0
          if (ifunc.eq.par%leaders(ihorn)) minuit%u(ipara)=1.
       endif
       ipara=ipara+1
    enddo
  end subroutine cubefit_function_spectral_shell_init
  !
  subroutine cubefit_function_spectral_shell_minimize(npar,grad,chi2,pars,iflag,obs)
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    integer(kind=npar_k),   intent(in)    :: npar        ! Number of parameters
    real(kind=grad_k),      intent(out)   :: grad(npar)  ! Gradientes
    real(kind=chi2_k),      intent(out)   :: chi2        ! chi squared
    real(kind=para_k),      intent(in)    :: pars(npar)  ! Parameter values
    integer(kind=4),        intent(in)    :: iflag       ! Code operation
    type(spectral_obs_t),   intent(inout) :: obs         ! Observation
    !
    integer(kind=chan_k) :: ichan,nfunc
    real(kind=coor_k) :: xfreq,eps
    real(kind=sign_k) :: sumpred,diff
    real(kind=para_k), allocatable :: repars(:,:)
    real(kind=para_k) :: areasca,freqoff,fwzlsca,hornsca
    real(kind=grad_k), allocatable :: regrad(:,:),scagrad(:)
    integer(kind=func_k) :: ifunc
    integer(kind=npar_k) :: ipara
    logical :: dograd
    character(len=*), parameter :: rname='SPECTRAL>SHELL>MINIMIZE'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    ! Final computations
    if (iflag.eq.code_minuit_rms) then
       call obs%sigma(cubefit_function_spectral_shell_profile,.true.)
       return
    endif
    !
    !
    ! The profile is written as the sum
    ! 	f(Ai,Ni,Vi,Hi)
    ! where Vi = VV.vi, Ai = AA.ai, Ni = NN+ni and Hi = HH*hi
    dograd = iflag.eq.code_minuit_gradient
    nfunc = max(obs%par%nfunc,1)
    allocate(repars(npara,nfunc),regrad(npara,nfunc),scagrad(npara))
    !
    areasca = pars(iarea)
    freqoff = pars(ifreq)
    fwzlsca = pars(ifwzl)
    hornsca = pars(ihorn)
    scagrad(iarea) = 0.0
    scagrad(ifreq) = 0.0
    scagrad(ifwzl) = 0.0
    scagrad(ihorn) = 0.0
    do ifunc=1,nfunc
       ipara = ifunc*npara
       repars(iarea,ifunc) = pars(ipara+iarea)*areasca
       regrad(iarea,ifunc) = 0.0
       repars(ifreq,ifunc) = pars(ipara+ifreq)+freqoff
       regrad(ifreq,ifunc) = 0.0
       repars(ifwzl,ifunc) = pars(ipara+ifwzl)*fwzlsca
       regrad(ifwzl,ifunc) = 0.0
       repars(ihorn,ifunc) = pars(ipara+ihorn)*hornsca
       regrad(ihorn,ifunc) = 0.0
    enddo
    !
    chi2 = 0.0
    eps = abs(obs%spec%f(obs%ifirst+1)-obs%spec%f(obs%ifirst))
    do ichan=obs%ifirst, obs%ilast
       if (obs%wfit(ichan).ne.0) then
          xfreq = obs%spec%f(ichan)
          sumpred = 0.0
          do ifunc=1,nfunc
             call cubefit_function_spectral_shell_one (xfreq,eps,repars(:,ifunc),dograd,sumpred,regrad(:,ifunc))
          enddo
          diff = sumpred - obs%spec%t(ichan)
          chi2 = chi2 + diff**2
          diff = 2.0*diff
          do ifunc = 1, nfunc
             do ipara = 1, npara
                regrad(ipara,ifunc) = diff*regrad(ipara,ifunc)
                regrad(ipara,ifunc) = regrad(ipara,ifunc) + regrad(ipara,ifunc)
             enddo
             scagrad(iarea) = scagrad(iarea) + regrad(iarea,ifunc)*repars(iarea,ifunc)
             scagrad(ifreq) = scagrad(ifreq) + regrad(ifreq,ifunc)
             scagrad(ifwzl) = scagrad(ifwzl) + regrad(ifwzl,ifunc)*repars(ifwzl,ifunc)
             scagrad(ihorn) = scagrad(ihorn) + regrad(ihorn,ifunc)*repars(ihorn,ifunc)
          enddo
       endif
    enddo
    !
    grad(iarea) = scagrad(iarea)
    grad(ifreq) = scagrad(ifreq)
    grad(ifwzl) = scagrad(ifwzl)
    grad(ihorn) = scagrad(ihorn)
    !
    do ifunc=1,nfunc
       ipara = ifunc*npara
       grad(ipara+iarea) = regrad(iarea,ifunc)*areasca
       grad(ipara+ifreq) = regrad(ifreq,ifunc)
       grad(ipara+ifwzl) = regrad(ifwzl,ifunc)*fwzlsca
       grad(ipara+ihorn) = regrad(ihorn,ifunc)*hornsca
    enddo
  end subroutine cubefit_function_spectral_shell_minimize
  !
  subroutine cubefit_function_spectral_shell_one(xfreq,eps,pars,dograd,pred,grad)
    use cubetools_nan
    !----------------------------------------------------------------------
    ! Computes the contribution of a Shell-like profile in the current
    ! channel to the square sum, and to the gradients relative to the
    ! parameters also if DOGRAD is .true.. Highly optimised ?
    !----------------------------------------------------------------------
    real(kind=coor_k), intent(in)    :: xfreq          ! Abscissa
    real(kind=coor_k), intent(in)    :: eps            ! Freq resolution
    real(kind=para_k), intent(in)    :: pars(npara) ! Input parameters
    logical,           intent(in)    :: dograd         ! Compute contribution to gradients
    real(kind=sign_k), intent(inout) :: pred           ! Output value of Shell function
    real(kind=grad_k), intent(out)   :: grad(npara) ! Gradients
    ! logical,      intent(out) :: error   ! Logical error flag -> deactivated, no way to signal error
    !
    real(kind=4) :: predloc,area,voff,fwzl,horn,arg,aarg,arg0,arg1,arg2,arg3,arg4
    integer(kind=npar_k) :: ipara
    character(len=*), parameter :: rname='SPECTRAL>SHELL>ONE'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    area = pars(iarea)
    voff = pars(ifreq)-xfreq
    fwzl = pars(ifwzl)
    horn = pars(ihorn)
    if (fwzl.eq.0.or.area.eq.0) then
       call cubefit_message(fitseve%others,rname,'zero valued area or width')
       ! Tell minuit this is a bad choice
       pred    = gr4nan
       grad(:) = pred
       return
    endif
    arg  = voff/fwzl
    arg0 = 1.-0.5*eps/fwzl
    arg1 = 1.+0.5*eps/fwzl
    aarg = abs(arg)
    if (aarg .lt. arg0) then
       arg2 = arg**2
       predloc = area*1.5/fwzl/(3.+horn)*(1.+horn*arg2)
       if (dograd) then
          arg3        =  1./(1.+horn*arg2)
          arg4        =  arg2*arg3
          grad(iarea) =  predloc/area
          grad(ifreq) = -predloc*arg3*2.*horn*arg/fwzl
          grad(ifwzl) = -predloc/fwzl*(1.+2.*horn*arg4)
          grad(ihorn) =  predloc*(-1./(3.+horn)+arg4)
       endif
    elseif (aarg .lt. arg1) then
       arg2 = arg0**2
       predloc   = area*1.5/fwzl/(3.+horn)*(1.+horn*arg2)*(aarg-arg1)/(arg0-arg1)
       if (dograd) then
          grad(iarea) =  predloc/area
          grad(ifreq) = -predloc/(aarg-arg1)/fwzl
          if (arg.le.0) grad(ifreq) = -grad(ifreq)
          grad(ifwzl) = -predloc/fwzl*(1.-1./(arg1-aarg)-2.*horn*arg0*(1.-arg0)/(1.+horn*arg2))
          grad(ihorn) =  predloc*(-1./(3.+horn)+arg2/(1.+horn*arg2))
       endif
    else
       predloc = 0.
       if (dograd) then
          do ipara=1,npara
             grad(ipara) = 0.
          enddo
       endif
    endif
    pred = pred + predloc
  end subroutine cubefit_function_spectral_shell_one
  !
  subroutine cubefit_function_spectral_shell_extract(minuit,obs,par,error)
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    type(fit_minuit_t),    intent(inout) :: minuit
    type(spectral_obs_t),  intent(inout) :: obs
    type(spectral_pars_t), intent(inout) :: par
    logical,               intent(inout) :: error
    !
    integer(kind=func_k) :: ifunc
    integer(kind=npar_k) :: ipara
    character(len=*), parameter :: rname='SPECTRAL>SHELL>EXTRACT'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    ! Update Parameters and errors
    ipara=0
    do ifunc=1,max(par%nfunc,1)
       par%pars(ipara+iarea)=minuit%u(ipara+iarea+npara)*minuit%u(iarea)
       if (ifunc.eq.par%leaders(iarea)) then
          par%errs(ipara+iarea) = minuit%werr(iarea)
       else
          par%errs(ipara+iarea) = minuit%werr(ipara+iarea+npara)
       endif
       par%pars(ipara+ifreq)=minuit%u(ipara+ifreq+npara)+minuit%u(ifreq)
       if (ifunc.eq.par%leaders(ifreq)) then
          par%errs(ipara+ifreq) = minuit%werr(ifreq)
       else
          par%errs(ipara+ifreq) = minuit%werr(ipara+ifreq+npara)
       endif
       par%pars(ipara+ifwzl)=minuit%u(ipara+ifwzl+npara)*minuit%u(ifwzl)
       if (ifunc.eq.par%leaders(ifwzl)) then
          par%errs(ipara+ifwzl) = minuit%werr(ifwzl)
       else
          par%errs(ipara+ifwzl) = minuit%werr(ipara+ifwzl+npara)
       endif
       par%pars(ipara+ihorn)=minuit%u(ipara+ihorn+npara)*minuit%u(ihorn)
       if (ifunc.eq.par%leaders(ihorn)) then
          par%errs(ipara+ihorn) = minuit%werr(ihorn)
       else
          par%errs(ipara+ihorn) = minuit%werr(ipara+ihorn+npara)
       endif
       ipara=ipara+npara
    enddo
  end subroutine cubefit_function_spectral_shell_extract
  !
  subroutine cubefit_function_spectral_shell_residuals(obs,spec,error)
    use cubemain_spectrum_real
    !------------------------------------------------------------------------
    ! Compute the residuals of a shell fit
    !------------------------------------------------------------------------
    type(spectral_obs_t), intent(inout) :: obs          
    type(spectrum_t),     intent(inout) :: spec         
    logical,              intent(inout) :: error
    !
    integer(kind=chan_k) :: ichan
    real(kind=coor_k) :: xfreq
    real(kind=sign_k) :: pred
    character(len=*), parameter :: rname="SPECTRAL>SHELL>RESIDUALS"
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    do ichan=1, spec%n
       xfreq = obs%spec%f(ichan)
       pred = cubefit_function_spectral_shell_profile(obs,xfreq,0)
       spec%t(ichan) = obs%spec%t(ichan) - pred
    enddo
  end subroutine cubefit_function_spectral_shell_residuals
  !
  subroutine cubefit_function_spectral_shell_doprofile(ifunc,obs,spec,error)
    use cubemain_spectrum_real
    !------------------------------------------------------------------------
    ! Compute the doprofile of a shell fit
    !------------------------------------------------------------------------
    integer(kind=func_k), intent(in)    :: ifunc
    type(spectral_obs_t), intent(inout) :: obs          
    type(spectrum_t),     intent(inout) :: spec         
    logical,              intent(inout) :: error
    !
    integer(kind=chan_k) :: ichan
    real(kind=coor_k) :: xfreq
    real(kind=sign_k) :: pred
    character(len=*), parameter :: rname="SPECTRAL>SHELL>DOPROFILE"
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    do ichan=1, spec%n
       xfreq = obs%spec%f(ichan)
       pred = cubefit_function_spectral_shell_profile(obs,xfreq,ifunc)
       spec%t(ichan) = pred
    enddo
  end subroutine cubefit_function_spectral_shell_doprofile
  !
  subroutine cubefit_function_spectral_shell_user2par(flag,pars,par,error)
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    integer(kind=flag_k),  intent(in)    :: flag(:)
    real(kind=para_k),     intent(in)    :: pars(:)
    type(spectral_pars_t), intent(inout) :: par
    logical,               intent(inout) :: error
    !
    integer(kind=npar_k) :: ipara,jpara
    integer(kind=func_k) :: ifunc
    integer(kind=4) :: ifatal
    character(len=mess_l) :: mess
    character(len=*), parameter :: rname='SPECTRAL>SHELL>USER2PAR'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    par%leaders(:) = 0
    par%flag(:,:)  = 0
    par%errs(:)    = 0.
    !
    jpara = 1
    do ifunc=1,par%nfunc
       do ipara=1,npara
          par%flag(ifunc,ipara) = flag(jpara)
          par%pars(jpara)       = pars(jpara)
          jpara=jpara+1
       enddo ! ipara
    enddo ! ifunc
    !
    ifatal = 0
    call par%check_line(iarea,error)
    if (error) ifatal=ifatal+1
    call par%check_line(ifreq,error)
    if (error) ifatal=ifatal+1
    call par%check_line(ifwzl,error)
    if (error) ifatal=ifatal+1
    call par%check_line(ihorn,error)
    if (error) ifatal=ifatal+1
    !
    if (par%leaders(iarea).ne.0 .and. par%leaders(ifwzl).ne.0 .and.&
         & par%leaders(iarea).ne.par%leaders(ifwzl)) ifatal=ifatal+1
    if (par%leaders(iarea).ne.0 .and. par%leaders(ihorn).ne.0 .and.&
         & par%leaders(iarea).ne.par%leaders(ihorn)) ifatal=ifatal+1
    if (par%leaders(iarea).ne.0 .and. par%leaders(ifreq).ne.0 .and.&
         & par%leaders(iarea).ne.par%leaders(ifreq)) ifatal=ifatal+1
    if (par%leaders(ifwzl).ne.0 .and. par%leaders(ifreq).ne.0 .and.&
         & par%leaders(ifwzl).ne.par%leaders(ifreq)) ifatal=ifatal+1
    if (par%leaders(ihorn).ne.0 .and. par%leaders(ifreq).ne.0 .and.&
         & par%leaders(ihorn).ne.par%leaders(ifreq)) ifatal=ifatal+1
    if (par%leaders(ihorn).ne.0 .and. par%leaders(ifwzl).ne.0 .and.&
         & par%leaders(ihorn).ne.par%leaders(ifwzl)) ifatal=ifatal+1
    if (ifatal.ne.0) then
       write(mess,'(i0,a)') ifatal,' Fatal Errors on Parameters'
       call cubefit_message(seve%e,rname,'Parameters flags are inconsistent')
       error = .true.
       return
    endif
  end subroutine cubefit_function_spectral_shell_user2par
  !
  subroutine cubefit_function_spectral_shell_par2spec(par,spec,error)
    use cubemain_spectrum_real
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(spectral_pars_t), intent(in)    :: par
    type(spectrum_t),      intent(inout) :: spec
    logical,               intent(inout) :: error
    !
    integer(kind=func_k) :: ifunc
    integer(kind=chan_k) :: ichan
    character(len=*), parameter :: rname='SPECTRAL>SHELL>PAR2SPEC'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    ichan = ndaps
    do ifunc=1,max(par%nfunc,1)
       ichan = ichan+1
       spec%t(ichan) = par%flag(ifunc,iarea) 
       ichan = ichan+1
       spec%t(ichan) = par%pars((ifunc-1)*npara+iarea)
       ichan = ichan+1
       spec%t(ichan) = par%errs((ifunc-1)*npara+iarea)
       ichan = ichan+1
       spec%t(ichan) = par%flag(ifunc,ifreq)
       ichan = ichan+1
       spec%t(ichan) = par%pars((ifunc-1)*npara+ifreq)
       ichan = ichan+1
       spec%t(ichan) = par%errs((ifunc-1)*npara+ifreq)
       ichan = ichan+1
       spec%t(ichan) = par%flag(ifunc,ifwzl)
       ichan = ichan+1
       spec%t(ichan) = par%pars((ifunc-1)*npara+ifwzl)
       ichan = ichan+1
       spec%t(ichan) = par%errs((ifunc-1)*npara+ifwzl)
       ichan = ichan+1
       spec%t(ichan) = par%flag(ifunc,ihorn)
       ichan = ichan+1
       spec%t(ichan) = par%pars((ifunc-1)*npara+ihorn)
       ichan = ichan+1
       spec%t(ichan) = par%errs((ifunc-1)*npara+ihorn)
    enddo
  end subroutine cubefit_function_spectral_shell_par2spec
  !
  subroutine cubefit_function_spectral_shell_spec2par(spec,par,error)
    use cubemain_spectrum_real
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(spectrum_t),      intent(in)    :: spec
    type(spectral_pars_t), intent(inout) :: par
    logical,               intent(inout) :: error
    !
    integer(kind=func_k) :: ifunc
    integer(kind=chan_k) :: ichan
    character(len=*), parameter :: rname='SPECTRAL>SHELL>SPEC2PAR'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    ichan = ndaps
    do ifunc=1,max(par%nfunc,1)
       ichan = ichan+1
       par%flag(ifunc,iarea)              = nint(spec%t(ichan),flag_k)
       ichan = ichan+1                     
       par%pars((ifunc-1)*npara+iarea) = spec%t(ichan) 
       ichan = ichan+1                     
       par%errs((ifunc-1)*npara+iarea) = spec%t(ichan) 
       ichan = ichan+1                     
       par%flag(ifunc,ifreq)              = nint(spec%t(ichan),flag_k)
       ichan = ichan+1                     
       par%pars((ifunc-1)*npara+ifreq) = spec%t(ichan) 
       ichan = ichan+1                     
       par%errs((ifunc-1)*npara+ifreq) = spec%t(ichan) 
       ichan = ichan+1                     
       par%flag(ifunc,ifwzl)              = nint(spec%t(ichan),flag_k)
       ichan = ichan+1                     
       par%pars((ifunc-1)*npara+ifwzl) = spec%t(ichan) 
       ichan = ichan+1                     
       par%errs((ifunc-1)*npara+ifwzl) = spec%t(ichan)
       ichan = ichan+1                     
       par%flag(ifunc,ihorn)              = nint(spec%t(ichan),flag_k)
       ichan = ichan+1                     
       par%pars((ifunc-1)*npara+ihorn) = spec%t(ichan) 
       ichan = ichan+1                     
       par%errs((ifunc-1)*npara+ihorn) = spec%t(ichan) 
    enddo
  end subroutine cubefit_function_spectral_shell_spec2par
  !
  subroutine cubefit_function_spectral_shell_iterate(par,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(spectral_pars_t), intent(inout) :: par
    logical,               intent(inout) :: error
    !
    integer(kind=func_k) :: ifunc
    integer(kind=npar_k) :: ipara
    character(len=*), parameter :: rname='SPECTRAL>SHELL>ITERATE'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    ipara = 0
    do ifunc=1,par%nfunc
       if (par%flag(ifunc,ipara+iarea).eq.3) &
            par%pars(ipara+iarea)=par%pars(ipara+iarea)/par%pars(npara*(par%leaders(iarea)-1)+iarea)
       if (par%flag(ifunc,ipara+ifreq).eq.3) &
            par%pars(ipara+ifreq)=par%pars(ipara+ifreq)/par%pars(npara*(par%leaders(ifreq)-1)+ifreq)
       if (par%flag(ifunc,ipara+ifwzl).eq.3) &
            par%pars(ipara+ifwzl)=par%pars(ipara+ifwzl)/par%pars(npara*(par%leaders(ifwzl)-1)+ifwzl)
       if (par%flag(ifunc,ipara+ihorn).eq.3) then
          if (par%pars(npara*(par%leaders(ihorn)-1)+ihorn).eq.0) then
             par%flag(ifunc,ipara+ihorn) = 1.0
          else
             par%pars(ipara+ifwzl)=par%pars(ipara+ifwzl)/par%pars(npara*(par%leaders(ihorn)-1)+ihorn)
          endif
       endif
       ipara = ipara+npara
    enddo
  end subroutine cubefit_function_spectral_shell_iterate
  !
  subroutine cubefit_function_spectral_shell_wind2par(obs,wind,par,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    type(spectral_obs_t),  intent(in)    :: obs
    integer(kind=chan_k),  intent(in)    :: wind(:)
    type(spectral_pars_t), intent(inout) :: par
    logical,               intent(inout) :: error
    !
    integer(kind=chan_k) :: first,last
    integer(kind=func_k) :: ifunc
    real(kind=para_k) :: area,freq,fwzl
    character(len=*), parameter :: rname='SPECTRAL>SHELL>WIND2PAR'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    par%errs(:)     = 0
    par%leaders(:)  = 0
    do ifunc=1,par%nfunc
       first = wind(2*ifunc-1)
       last  = wind(2*ifunc)
       par%flag(ifunc,:)   = 0
       !
       call obs%est_shell(first,last,area,freq,fwzl,error)
       if (error) return
       if (fwzl.lt.obs%deltaf) fwzl = obs%deltaf
       par%pars((ifunc-1)*npara+iarea) = area 
       par%pars((ifunc-1)*npara+ifreq) = freq 
       par%pars((ifunc-1)*npara+ifwzl) = fwzl
       par%pars((ifunc-1)*npara+ihorn) = 0
    enddo
  end subroutine cubefit_function_spectral_shell_wind2par
  !
  !----------------------------------------------------------------------
  !
  function cubefit_function_spectral_shell_profile(obs,xfreq,ifunc) result(shell)
    !----------------------------------------------------------------------
    ! Compute the value of a shell or a sum of shells at xfreq
    !----------------------------------------------------------------------
    type(spectral_obs_t),   intent(in)    :: obs      ! Observation
    real(kind=coor_k),      intent(in)    :: xfreq    ! Coordinate to compute value
    integer(kind=func_k),   intent(in)    :: ifunc    ! Which shell is to be computed
    !
    real(kind=sign_k) :: shell
    !
    integer(kind=func_k) :: ifirst,ilast,jfunc
    real(kind=para_k) :: locpars(npara)
    real(kind=para_k) :: nullgrad(npara)
    logical :: dograd
    !
    !
    dograd = .false.
    shell=0.
    if (ifunc.eq.0) then
       ifirst = 1
       ilast = max(obs%par%nfunc,1)
    else
       ifirst = ifunc
       ilast  = ifunc
    endif
    !
    do jfunc = ifirst,ilast
       locpars(:) = obs%par%pars((jfunc-1)*npara+1:jfunc*npara)
       if (locpars(iarea).ne.0 .and. locpars(ifwzl).ne.0) then
          call cubefit_function_spectral_shell_one(xfreq,obs%deltaf,locpars,dograd,shell,nullgrad)
       endif
    enddo
  end function cubefit_function_spectral_shell_profile
  !
  subroutine cubefit_function_spectral_shell_flags(ipara,funcflag,paraflag,error)
    use cubedag_allflags
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    integer(kind=npar_k), intent(in)    :: ipara
    type(flag_t),         intent(out)   :: funcflag
    type(flag_t),         intent(out)   :: paraflag
    logical,              intent(inout) :: error
    !
    integer(kind=func_k) :: ifunc
    character(len=*), parameter :: rname='SPECTRAL>SHELL>FLAGS'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    ifunc  = ipara/npara+1
    select case(mod(ipara,npara))
    case(1)
       paraflag = flag_area
    case(2)
       paraflag = flag_frequency
    case(3)
       paraflag = flag_fwzl
    case(0)
       paraflag = flag_horn
       ifunc  = ifunc-1
    end select
    call cubefit_func2flag(ifunc,funcflag,error)
    if (error) return
  end subroutine cubefit_function_spectral_shell_flags
  !
  subroutine cubefit_function_spectral_shell_units(ipara,unit,error)
    use cubetools_unit_setup
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    integer(kind=npar_k), intent(in)    :: ipara
    character(len=*),     intent(out)   :: unit
    logical,              intent(inout) :: error
    !
    integer(kind=func_k) :: ifunc
    character(len=*), parameter :: rname='SPECTRAL>SHELL>UNITS'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    ifunc  = ipara/npara+1
    select case(mod(ipara,npara))
    case(1)
       unit = strg_add//unit_freq%prog_name()
    case(2)
       unit = unit_freq%prog_name()
    case(3)
       unit = unit_freq%prog_name()
    case(0)
       unit = '---' ! No unit for horn ratio parameter
    end select
  end subroutine cubefit_function_spectral_shell_units
  !
  function cubefit_function_spectral_shell_npar(nfunc) result(npar)
    !------------------------------------------------------------------------
    !
    !------------------------------------------------------------------------
    integer(kind=func_k), intent(in) :: nfunc
    !
    integer(kind=npar_k) :: npar
    !
    npar = npara*nfunc
  end function cubefit_function_spectral_shell_npar
end module cubefit_function_spectral_shell
