!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubefit_parameters
  use cubetools_parameters
  use cubefit_messaging
  !
  ! Kind parameters
  integer(kind=4), parameter :: meth_l = 12
  integer(kind=4), parameter :: func_k = inte_k ! Number of fitted functions
  integer(kind=4), parameter :: npar_k = inte_k ! Number of fitted parameters
  integer(kind=4), parameter :: flag_k = inte_k ! Flag       values
  integer(kind=4), parameter :: para_k = dble_k ! Parameter  values
  integer(kind=4), parameter :: grad_k = dble_k ! Gradient   values
  integer(kind=4), parameter :: chi2_k = dble_k ! Chi square values
  !
  ! Miscelania
  character(len=*), parameter :: strg_add = '+' ! Track that unit is to be added
  !
  ! Convergence codes
  integer(kind=code_k), parameter :: code_status_diverged  = -1
  integer(kind=code_k), parameter :: code_status_unfitted  = 0  
  integer(kind=code_k), parameter :: code_status_converged =  1
  !
  ! Spectral data format
  integer(kind=chan_k), parameter :: ndaps =  5 ! Number of columns before the fit results in the output format
  integer(kind=func_k), parameter :: mfunc = 10 ! Maximum number of functions
  integer(kind=npar_k), parameter :: mpara =  4 ! Maximum number of parameters per function
  integer(kind=chan_k), parameter :: icol_status  = 1
  integer(kind=chan_k), parameter :: icol_method  = 2
  integer(kind=chan_k), parameter :: icol_nfunc   = 3
  integer(kind=chan_k), parameter :: icol_rmsbase = 4
  integer(kind=chan_k), parameter :: icol_rmsline = 5
  !
  ! Spectral Minimization methods
  integer(kind=4),  parameter :: nmeth = 4
  character(len=*), parameter :: methnames(nmeth)  = [&
       "GAUSSIAN  ","ABSORPTION","HFS       ","SHELL     "]
  !
  integer(kind=code_k), parameter :: code_method_all        = 0 ! *** JP: The role of this one is unclear to me. Shouldn't it be code_method_any?
  integer(kind=code_k), parameter :: code_method_gaussian   = 1
  integer(kind=code_k), parameter :: code_method_absorption = 2
  integer(kind=code_k), parameter :: code_method_hfs        = 3
  integer(kind=code_k), parameter :: code_method_shell      = 4
  !
  ! Minuit codes
  integer(kind=code_k), parameter :: code_minuit_value    = 1 ! Compute the function value
  integer(kind=code_k), parameter :: code_minuit_gradient = 2 ! Compute the function gradients
  integer(kind=code_k), parameter :: code_minuit_rms      = 3 ! Compute the RMS between the data and the fitted function
  !
contains
  !
  subroutine cubefit_func2flag(ifunc,flag,error)
    use cubedag_allflags
    !-------------------------------------------------------------------
    ! Return the proper flag for line #ifunc
    !-------------------------------------------------------------------
    integer(kind=func_k), intent(in)    :: ifunc
    type(flag_t),         intent(out)   :: flag
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='FUNC2FLAG'
    !
    select case (ifunc)
    case (1)
      flag = flag_one
    case (2)
      flag = flag_two
    case (3)
      flag = flag_three
    case (4)
      flag = flag_four
    case (5)
      flag = flag_five
    case (6)
      flag = flag_six
    case (7)
      flag = flag_seven
    case (8)
      flag = flag_eight
    case (9)
      flag = flag_nine
    case (10)
      flag = flag_ten
    case default
      call cubefit_message(seve%e,rname,'Line number not supported')
      error = .true.
      return
    end select
  end subroutine cubefit_func2flag
  !
  subroutine cubefit_flag2func(flag,ifunc,error)
    use cubedag_allflags
    !-------------------------------------------------------------------
    ! Return the proper line number for given flag
    !-------------------------------------------------------------------
    type(flag_t),         intent(in)    :: flag
    integer(kind=func_k), intent(out)   :: ifunc
    logical,              intent(inout) :: error
    !
    character(len=*), parameter :: rname='FLAG2FUNC'
    !
    ! *** JP: We should go to a SELECT CASE statement here
    if (flag.eq.flag_one) then
      ifunc = 1
    elseif (flag.eq.flag_two) then
      ifunc = 2
    elseif (flag.eq.flag_three) then
      ifunc = 3
    elseif (flag.eq.flag_four) then
      ifunc = 4
    elseif (flag.eq.flag_five) then
      ifunc = 5
    elseif (flag.eq.flag_six) then
      ifunc = 6
    elseif (flag.eq.flag_seven) then
      ifunc = 7
    elseif (flag.eq.flag_eight) then
      ifunc = 8
    elseif (flag.eq.flag_nine) then
      ifunc = 9
    elseif (flag.eq.flag_ten) then
      ifunc = 10
    else
      ifunc = 0
      call cubefit_message(seve%e,rname,'Flag is not a line flag')
      error = .true.
      return
    endif
  end subroutine cubefit_flag2func
end module cubefit_parameters
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
