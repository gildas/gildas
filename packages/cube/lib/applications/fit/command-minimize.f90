!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubefit_minimize
  use cube_types
  use cubetools_structure
  use cubetools_keywordlist_types
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubemain_windowing
  use cubemain_range
  use cubemain_ancillary_init_types
  use cubemain_ancillary_wind_types
  use cubemain_ancillary_mask_types
  use cubefit_messaging
  use cubefit_parameters
  use cubefit_hfs
  !
  public :: minimize
  private
  !
  type :: minimize_comm_t
     type(option_t),      pointer :: comm
     type(option_t),      pointer :: line
     type(keywordlist_comm_t), pointer :: meth_arg
     type(option_t),      pointer :: hfs
     type(cubeid_arg_t),  pointer :: cube
     type(range_opt_t)            :: glowin
     type(ancillary_wind_comm_t)  :: locwin
     type(ancillary_mask_comm_t)  :: mask
     type(ancillary_init_comm_t)  :: inpars
     type(cube_prod_t),   pointer :: oupars
   contains
     procedure, public  :: register   => cubefit_minimize_comm_register
     procedure, private :: parse      => cubefit_minimize_comm_parse
     procedure, private :: parse_line => cubefit_minimize_comm_parse_line
     procedure, private :: main       => cubefit_minimize_comm_main
  end type minimize_comm_t
  type(minimize_comm_t) :: minimize
  !
  type minimize_user_t
     type(cubeid_user_t)         :: cubeids
     logical                     :: doglowin = .false.
     type(range_array_t)         :: glowin
     type(ancillary_wind_user_t) :: locwin
     type(ancillary_mask_user_t) :: mask
     type(ancillary_init_user_t) :: inpars
     logical                     :: doline = .false.
     character(len=meth_l)       :: method
     integer(kind=func_k)        :: nfunc
     character(len=argu_l), allocatable :: lineinfo(:)
     type(hfs_user_t)            :: hfs
   contains
     procedure, private :: toprog => cubefit_minimize_user_toprog
  end type minimize_user_t
  !
  type minimize_prog_t
     logical :: doglowin
     type(hfs_prog_t)      :: hfs
     integer(kind=code_k)  :: method
     integer(kind=func_k)  :: nfunc
     integer(kind=npar_k)  :: npara
     integer(kind=chan_k)  :: nout
     real(kind=para_k),    allocatable :: pars(:)
     integer(kind=flag_k), allocatable :: flag(:)
     type(cube_t),       pointer :: cube
     type(window_array_t)        :: glowin
     type(ancillary_wind_prog_t) :: locwin
     type(ancillary_mask_prog_t) :: mask
     type(ancillary_init_prog_t) :: inpars
     type(cube_t),       pointer :: oupars
     procedure(cubefit_minimize_prog_loop_noguess), pointer :: loop => null()
   contains
     procedure, private :: header      => cubefit_minimize_prog_header
     procedure, private :: data        => cubefit_minimize_prog_data
     procedure, private :: act_noguess => cubefit_minimize_prog_act_noguess     
     procedure, private :: act_window  => cubefit_minimize_prog_act_window     
     procedure, private :: act_init    => cubefit_minimize_prog_act_init
  end type minimize_prog_t
  !
contains
  !
  subroutine cubefit_minimize_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(minimize_user_t) :: user
    character(len=*), parameter :: rname='MINIMIZE>COMMAND'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    call minimize%parse(line,user,error)
    if (error) return
    call minimize%main(user,error)
    if (error) return
  end subroutine cubefit_minimize_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubefit_minimize_comm_register(comm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(minimize_comm_t), intent(inout) :: comm
    logical,                intent(inout) :: error
    !
    type(cubeid_arg_t)   :: incube
    type(standard_arg_t) :: stdarg
    type(keywordlist_comm_t)  :: keyarg
    type(cube_prod_t)    :: oucube
    character(len=*), parameter :: comm_abstract='Fit functions to spectra'
    character(len=*), parameter :: comm_help=strg_id
    character(len=*), parameter :: rname='MINIMIZE>COMM>REGISTER'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    call cubetools_register_command(&
         'MINIMIZE','[cubeid [windowid]]',&
         comm_abstract,&
         comm_help,&
         cubefit_minimize_command,&
         comm%comm,error)
    if (error) return
    call incube%register(&
         'CUBE',&
         'Signal cube',&
         strg_id,&
         code_arg_optional,&
         [flag_cube],&
         code_read,&
         code_access_speset,&
         comm%cube,&
         error)
    if (error) return
    !
    call comm%glowin%register(&
         'RANGE',&
         'Define velocity range(s) for first guess estimates',&
         range_is_multiple,error)
    if (error) return
    !
    call comm%inpars%register(error)
    if (error) return
    !
    call comm%locwin%register(error)
    if (error) return
    !
    call comm%mask%register(&
         'Use a 2D mask to define the regions to be fitted',&
         code_access_speset,&
         error)
    if (error) return
    !
    call cubetools_register_option(&
         'LINE','method nfuncs [f11 p11 ... f1m p1m ... fn1 pn1 ... fnm pnm]',&
         'Set fitting method and optionally its first guesses',&
         'gag_doc:hlp/cube-help-fit-minimize-line.hlp',&
         comm%line,error)
    if (error) return
    call keyarg%register(&
         'method', &
         'Method for fitting',&
         strg_id,&
         code_arg_mandatory,&
         methnames,&
         .not.flexible,&
         comm%meth_arg,&
         error)
    if (error) return
    call stdarg%register(&
         'nfuncs', &
         'Number of lines to be fitted',&
         strg_id,&
         code_arg_mandatory, error)
    if (error) return
    call stdarg%register(&
         'f11', &
         'Flag for the first parameter of the first line',&
         strg_id,&
         code_arg_unlimited, error)
    if (error) return
    call stdarg%register(&
         'p11', &
         'First parameter of the first line',&
         strg_id,&
         code_arg_unlimited, error)
    if (error) return
    ! call stdarg%register(&
    !      'f1m', &
    !      'Flag for the m-eth parameter of the first line',&
    !      strg_id,&
    !      code_arg_unlimited, error)
    ! if (error) return
    ! call stdarg%register(&
    !      'p1m', &
    !      'm-eth parameter of the first line',&
    !      strg_id,&
    !      code_arg_unlimited, error)
    ! if (error) return
    ! call stdarg%register(&
    !      'fn1', &
    !      'Flag for the first parameter of the n-eth line',&
    !      strg_id,&
    !      code_arg_unlimited, error)
    ! if (error) return
    ! call stdarg%register(&
    !      'pn1', &
    !      'First parameter of the n-eth line',&
    !      strg_id,&
    !      code_arg_unlimited, error)
    ! if (error) return
    call stdarg%register(&
         'fnm', &
         'Flag for the m-eth parameter of the n-eth line',&
         strg_id,&
         code_arg_unlimited, error)
    if (error) return
    call stdarg%register(&
         'pnm', &
         'm-eth parameter of the n-eth line',&
         strg_id,&
         code_arg_unlimited, error)
    if (error) return
    !
    call cubefit_hfs_register(comm%hfs,error)
    if (error) return
    !
!!$    call cubetools_register_option(&
!!$         'METHOD','method',&
!!$         '',&
!!$         'The window cube can be created with commad WINDOW. The&
!!$         & number of lines to be fitted is assumed to be equal to the&
!!$         & number of windows in the window CUBE',&
!!$         comm%glowin,error)
!!$    if (error) return
!!$    call keyarg%register(&
!!$         'method', &
!!$         'Method for fitting',&
!!$         strg_id,&
!!$         code_arg_mandatory,&
!!$         methnames,&
!!$         .not.flexible,&
!!$         comm%meth_arg,&
!!$         error)
!!$    if (error) return
    !
    ! Products
    call oucube%register(&
         'FIT',&
         'Cube of fitted parameters',&
         strg_id,&
         [flag_fit,flag_minimize],&
         comm%oupars,&
         error)
    if (error) return
  end subroutine cubefit_minimize_comm_register
  !
  subroutine cubefit_minimize_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(minimize_comm_t), intent(in)    :: comm
    character(len=*),       intent(in)    :: line
    type(minimize_user_t),  intent(out)   :: user
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='MINIMIZE>COMM>PARSE'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    call cubeadm_cubeid_parse(line,comm%comm,user%cubeids,error)
    if (error) return
    call comm%glowin%parse(line,user%doglowin,user%glowin,error)
    if (error) return
    call comm%inpars%parse(line,user%inpars,error)
    if (error) return
    call comm%locwin%parse(line,user%locwin,error)
    if (error) return
    call comm%mask%parse(line,user%mask,error)
    if (error) return
    call comm%parse_line(line,user,error)
    if (error) return
    call cubefit_hfs_parse(line,comm%hfs,user%hfs,error)
    if (error) return
    !
    if (user%doline.and.user%inpars%present) then
       call cubefit_message(seve%e,rname,"Options /LINES and /INITIAL can not be given at the same time")
       error = .true.
       return
    else if (user%doline.and.user%locwin%present) then
       call cubefit_message(seve%e,rname,"Options /LINES and /WINDOW can not be given at the same time")
       error = .true.
       return
    else if (user%locwin%present.and.user%inpars%present) then
       call cubefit_message(seve%e,rname,"Options /WINDOW and /INITAL can not be given at the same time")
       error = .true.
       return
    endif
  end subroutine cubefit_minimize_comm_parse
  !
  subroutine cubefit_minimize_comm_parse_line(comm,line,user,error)
    use gkernel_interfaces
    !----------------------------------------------------------------------
    ! /LINES method [nfuncs f11 p11 ... f1m p1m ... fn1 pn1 ... fnm pnm]
    !----------------------------------------------------------------------
    class(minimize_comm_t), intent(in)    :: comm
    character(len=*),       intent(in)    :: line
    type(minimize_user_t),  intent(inout) :: user
    logical,                intent(inout) :: error
    !
    integer(kind=4) :: narg,iarg,ier
    character(len=*), parameter :: rname='MINIMIZE>COMM>PARSE>LINE'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    call comm%line%present(line,user%doline,error)
    if (error) return
    if (user%doline) then
       call cubetools_getarg(line,comm%line,1,user%method,mandatory,error)
       if (error) return
       narg = comm%line%getnarg()
       if (narg.gt.1) then
          call cubetools_getarg(line,comm%line,2,user%nfunc,mandatory,error)
          if (error) return
          allocate(user%lineinfo(narg-2),stat=ier)
          if (failed_allocate(rname,'user line guesses',ier,error)) return
          do iarg=3,narg
             call cubetools_getarg(line,comm%line,iarg,user%lineinfo(iarg-2),mandatory,error)
             if (error) return
          enddo
       else
          user%nfunc = 0
       endif
    endif
  end subroutine cubefit_minimize_comm_parse_line
  !
  subroutine cubefit_minimize_comm_main(comm,user,error) 
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(minimize_comm_t), intent(in)    :: comm
    type(minimize_user_t),  intent(in)    :: user
    logical,                intent(inout) :: error
    !
    type(minimize_prog_t) :: prog
    character(len=*), parameter :: rname='MINIMIZE>COMM>MAIN'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
    call prog%header(comm,error)
    if (error) return
    call cubeadm_timing_prepro2process()
    call prog%data(error)
    if (error) return
    call cubeadm_timing_process2postpro()
  end subroutine cubefit_minimize_comm_main
  !
  !----------------------------------------------------------------------
  !
  subroutine cubefit_minimize_user_toprog(user,comm,prog,error)
    use gkernel_interfaces
    use cubetools_user2prog
    use cubetools_unit
    use cubetools_header_methods
    use cubetools_consistency_methods
    use cubeadm_get
    use cubefit_spectral_fit
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(minimize_user_t), intent(in)    :: user
    type(minimize_comm_t),  intent(in)    :: comm
    type(minimize_prog_t),  intent(out)   :: prog
    logical,                intent(inout) :: error
    !
    type(unit_user_t) :: nounit
    logical :: conspb
    integer(kind=4) :: ier
    integer(kind=npar_k) :: ipara
    integer(kind=chan_k) :: nwindow
    character(len=mess_l) :: mess
    character(len=meth_l) :: methodstr
    character(len=*), parameter :: rname='MINIMIZE>USER>TOPROG'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    conspb =.false.
    call cubeadm_get_header(comm%cube,user%cubeids,prog%cube,error)
    if (error) return
    !
    call comm%glowin%user2prog(prog%cube,user%glowin,prog%glowin,error)
    if (error) return
    !
    call user%mask%toprog(comm%mask,prog%mask,error)
    if (error) return
    if (prog%mask%do) then
       call prog%mask%check_consistency(prog%cube,error)
       if (error) return
       call prog%mask%must_be_spatial(error)
       if (error) return
    endif
    !
    call user%inpars%toprog(comm%inpars,prog%inpars,error)
    if (error) return
    if (prog%inpars%do) then
       call cubetools_consistency_spatial('Input cube',prog%cube%head,'Init guesses',prog%inpars%cube%head,&
            conspb,error)
       if (error) return
       if (cubetools_consistency_failed(rname,conspb,error)) return
       call cubetools_header_get_nchan(prog%inpars%cube%head,prog%nout,error)
       if (error) return
       prog%method = code_abs
       prog%loop => cubefit_minimize_prog_loop_init
    else
       call user%locwin%toprog(comm%locwin,prog%locwin,error)
       if (error) return
       if (prog%locwin%do) then
          call cubetools_consistency_spatial('Input cube',prog%cube%head,'Window',prog%locwin%cube%head,conspb,error)
          if (error) return
          if (cubetools_consistency_failed(rname,conspb,error)) return
          call cubetools_keywordlist_user2prog(comm%meth_arg,user%method,prog%method,methodstr,error)
          if (error) return
          call cubetools_header_get_nchan(prog%locwin%cube%head,nwindow,error)
          if (error) return
          prog%nfunc = nwindow/2
          prog%loop => cubefit_minimize_prog_loop_window
       else if (user%doline) then
          call cubetools_keywordlist_user2prog(comm%meth_arg,user%method,prog%method,methodstr,error)
          if (error) return
          prog%nfunc = user%nfunc
          prog%loop => cubefit_minimize_prog_loop_noguess
       else
          prog%method = code_method_gaussian
          prog%nfunc  = 0
          prog%loop => cubefit_minimize_prog_loop_noguess
       endif
       !
       call cubefit_parameters_npars_nout(prog%method,prog%nfunc,prog%npara,prog%nout,error)
       if (error) return
       if (user%doline.and.prog%nfunc.gt.0) then
          if (size(user%lineinfo).ne.2*prog%npara) then
             write(mess,'(2(a,i0))') "Expected ",2*prog%npara," flags + parameters, got ",size(user%lineinfo)
             call cubefit_message(seve%e,rname,mess)
             error = .true.
             return
          endif
          allocate(prog%pars(prog%npara),prog%flag(prog%npara),stat=ier)
          if (failed_allocate(rname,'init guesses',ier,error)) return
          prog%flag(:) = 0
          prog%pars(:) = 0.
          call nounit%get_from_code(code_unit_unk,error)
          if (error) return
          do ipara=1,prog%npara
             call cubetools_user2prog_resolve_star(user%lineinfo(2*ipara-1),0,prog%flag(ipara),error)
             if (error) return
             call cubetools_user2prog_resolve_star(user%lineinfo(2*ipara),nounit,0d0,prog%pars(ipara),error)
             if (error) return
          enddo
       endif
    endif
    !
    if (user%hfs%do) then
       call cubefit_hfs_user2prog(user%hfs,prog%hfs,error)
       if (error) return
    else 
       if (prog%method.eq.code_method_hfs.or.prog%method.eq.code_method_absorption) then
          call cubefit_message(seve%e,rname,'Methods HFS and ABSORPTION require a HFS file')
          error = .true.
          return
       endif
    endif
  end subroutine cubefit_minimize_user_toprog
  !
  !----------------------------------------------------------------------
  !
  subroutine cubefit_minimize_prog_header(prog,comm,error)
    use cubetools_header_methods
    use cubetools_axis_types
    use cubeadm_clone
    use cubetools_unit
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(minimize_prog_t), intent(inout) :: prog
    type(minimize_comm_t),  intent(in)    :: comm
    logical,                intent(inout) :: error
    !
    type(axis_t) :: axis
    character(len=*), parameter :: rname='MINIMIZE>PROG>HEADER'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    call cubeadm_clone_header(comm%oupars,prog%cube,prog%oupars,error)
    if (error) return
    !
    call cubetools_header_get_axis_head_c(prog%oupars%head,axis,error)
    if (error) return
    axis%name = 'Spectral fit'
    axis%unit = 'NONE'
    axis%kind = code_unit_unk
    axis%ref = 1d0
    axis%val = 1d0
    axis%inc = 1d0
    axis%n   = prog%nout
    call cubetools_header_update_axset_c(axis,prog%oupars%head,error)
    if (error) return
    !
  end subroutine cubefit_minimize_prog_header
  !
  subroutine cubefit_minimize_prog_data(prog,error)
    use gkernel_interfaces
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(minimize_prog_t), intent(inout) :: prog
    logical,                intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='MINIMIZE>PROG>DATA'
    !
    call cubefit_message(fitseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,error)
    if (error) return
    !
    ! Kernel fitting routines raise a number of warnings when fit
    ! diverges, we don't want to see this messages polluting the
    ! terminal hence they are hidden here
    call exec_program('sic message cube s-w')
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo ! iter
    !$OMP END SINGLE
    !$OMP END PARALLEL
    !
    ! Reactivate cube warning messages after fits
    call exec_program('sic message cube s+w')
  end subroutine cubefit_minimize_prog_data
  !
  subroutine cubefit_minimize_prog_loop_noguess(prog,iter,error)
    use cubemain_spectrum_real
    use cubeadm_taskloop
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(minimize_prog_t),   intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(spectrum_t) :: inspec,ouspec,mask
    character(len=*), parameter :: rname='MINIMIZE>PROG>LOOP>NOGUESS'
    !
    call inspec%reassociate_and_init(prog%cube,iter,error)
    if (error) return
    if (prog%mask%do) then
       call mask%reassociate_and_init(prog%mask%cube,iter,error)
       if (error) return
    endif
    call ouspec%reallocate('fit',prog%oupars%head%arr%n%c,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
       call prog%act_noguess(iter%ie,inspec,mask,ouspec,error)
       if (error) return
    enddo
  end subroutine cubefit_minimize_prog_loop_noguess
  !
  subroutine cubefit_minimize_prog_act_noguess(prog,ie,inspec,mask,ouspec,error)
    use cubetools_nan
    use cubemain_spectrum_real
    use cubemain_spectrum_blanking
    use cubefit_spectral_fit
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(minimize_prog_t), intent(inout) :: prog
    integer(kind=entr_k),   intent(in)    :: ie
    type(spectrum_t),       intent(inout) :: inspec
    type(spectrum_t),       intent(inout) :: mask
    type(spectrum_t),       intent(inout) :: ouspec
    logical,                intent(inout) :: error
    !
    type(fit_spectral_t) :: fit
    logical :: dofit
    character(len=*), parameter :: rname='MINIMIZE>PROG>ACT>NOGUESS'
    !
    dofit = .true.
    if (prog%mask%do) then
       call mask%get(prog%mask%cube,ie,error)
       if (error) return
       dofit = mask%t(1).gt.0
    endif
    !
    if (dofit) then
       call inspec%get(prog%cube,ie,error)
       if (error) return
       if (cubemain_spectrum_blank(inspec)) then
          ouspec%t(:) = gr4nan
       else
          call fit%user_import(prog%hfs,prog%npara,prog%method,prog%nfunc,prog%flag,prog%pars,error)
          if (error) return
          call fit%fit(inspec,prog%glowin,error)
          if (error) return
          call fit%export(ouspec,error)
          if (error) return
       endif
    else
       ouspec%t(:) = gr4nan
    endif
    call ouspec%put(prog%oupars,ie,error)
    if (error) return
  end subroutine cubefit_minimize_prog_act_noguess
  !
  subroutine cubefit_minimize_prog_loop_window(prog,iter,error)
    use cubeadm_taskloop
    use cubemain_spectrum_real
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(minimize_prog_t),   intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    character(len=mess_l) :: mess
    type(spectrum_t) :: inspec,window,ouspec,mask
    character(len=*), parameter :: rname='MINIMIZE>PROG>LOOP>WINDOW'
    !
    call inspec%reassociate_and_init(prog%cube,iter,error)
    if (error) return
    if (prog%mask%do) then
       call mask%reassociate_and_init(prog%mask%cube,iter,error)
       if (error) return
    endif
    call window%reassociate_and_init(prog%locwin%cube,iter,error)
    if (error) return
    call ouspec%reallocate('fit',prog%oupars%head%arr%n%c,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      write(mess,'(a,i0)') 'Processing spectrum #',iter%ie
      call cubefit_message(fitseve%others,rname,mess)
      call prog%act_window(iter%ie,inspec,window,mask,ouspec,error)
      if (error) return
    enddo
  end subroutine cubefit_minimize_prog_loop_window
  !   
  subroutine cubefit_minimize_prog_act_window(prog,ie,inspec,window,mask,ouspec,error)
    use gkernel_interfaces
    use cubetools_nan
    use cubemain_spectrum_real
    use cubemain_spectrum_blanking
    use cubetopology_tool
    use cubefit_spectral_fit
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(minimize_prog_t), intent(inout) :: prog
    integer(kind=entr_k),   intent(in)    :: ie
    type(spectrum_t),       intent(inout) :: inspec
    type(spectrum_t),       intent(inout) :: window
    type(spectrum_t),       intent(inout) :: mask
    type(spectrum_t),       intent(inout) :: ouspec
    logical,                intent(inout) :: error
    !
    type(fit_spectral_t) :: fit
    logical :: dofit
    integer(kind=chan_k) :: iw
    integer(kind=4) :: ier
    integer(kind=chan_k),allocatable :: chanwin(:)
    real(kind=coor_k),allocatable :: r8win(:)
    character(len=*), parameter :: rname='MINIMIZE>PROG>ACT>WINDOW'
    !
    dofit = .true.
    if (prog%mask%do) then
       call mask%get(prog%mask%cube,ie,error)
       if (error) return
       dofit = mask%t(1).gt.0
    endif
    if (prog%doglowin.and.dofit) then
       call window%get(prog%locwin%cube,ie,error)
       if (error) return
       dofit = .not.ieee_is_nan(window%t(1))
    endif
    !
    if (dofit) then
       call inspec%get(prog%cube,ie,error)
       if (error) return
       if (cubemain_spectrum_blank(inspec)) then
          ouspec%t(:) = gr4nan
       else
          allocate(r8win(window%n),chanwin(window%n),stat=ier)
          if (failed_allocate(rname,'Double window',ier,error)) return
          r8win(:) = window%t(:)
          do iw=1,window%n/2
             call cubetopology_tool_vrange2crange(prog%cube,r8win(2*iw-1:2*iw),chanwin(2*iw-1:2*iw),error)
             if (error) return
          enddo
          call fit%wind_import(prog%hfs,prog%method,chanwin,error)
          if (error) return
          call fit%fit(inspec,prog%glowin,error)
          if (error) return
          call fit%export(ouspec,error)
          if (error) return
       endif
    else
       ouspec%t(:) = gr4nan
    endif
    call ouspec%put(prog%oupars,ie,error)
    if (error) return
  end subroutine cubefit_minimize_prog_act_window
  !
  subroutine cubefit_minimize_prog_loop_init(prog,iter,error)
    use cubeadm_taskloop
    use cubemain_spectrum_real
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(minimize_prog_t),   intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    character(len=mess_l) :: mess
    type(spectrum_t) :: inspec,init,ouspec,mask
    character(len=*), parameter :: rname='MINIMIZE>PROG>LOOP>INIT'
    !
    call inspec%reassociate_and_init(prog%cube,iter,error)
    if (error) return
    call init%reassociate_and_init(prog%inpars%cube,iter,error)
    if (error) return
    if (prog%mask%do) then
       call mask%reassociate_and_init(prog%mask%cube,iter,error)
       if (error) return
    endif
    call ouspec%reallocate('fit',prog%oupars%head%arr%n%c,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      write(mess,'(a,i0)') 'Processing spectrum #',iter%ie
      call cubefit_message(fitseve%others,rname,mess)
      call prog%act_init(iter%ie,inspec,init,mask,ouspec,error)
      if (error) return
    enddo
  end subroutine cubefit_minimize_prog_loop_init
  !   
  subroutine cubefit_minimize_prog_act_init(prog,ie,inspec,init,mask,ouspec,error)
    use cubetools_nan
    use cubemain_spectrum_real
    use cubemain_spectrum_blanking
    use cubetopology_tool
    use cubefit_spectral_fit
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(minimize_prog_t), intent(inout) :: prog
    integer(kind=entr_k),   intent(in)    :: ie
    type(spectrum_t),       intent(inout) :: inspec
    type(spectrum_t),       intent(inout) :: init
    type(spectrum_t),       intent(inout) :: mask
    type(spectrum_t),       intent(inout) :: ouspec
    logical,                intent(inout) :: error
    !
    type(fit_spectral_t) :: fit
    logical :: dofit
    character(len=*), parameter :: rname='MINIMIZE>PROG>ACT>INIT'
    !
    dofit = .true.
    if (prog%mask%do) then
       call mask%get(prog%mask%cube,ie,error)
       if (error) return
       dofit = mask%t(1).gt.0
    endif
    if (prog%inpars%do.and.dofit) then
       call init%get(prog%inpars%cube,ie,error)
       if (error) return
       dofit = .not.ieee_is_nan(init%t(1))
    endif
    !
    if (dofit) then
       call inspec%get(prog%cube,ie,error)
       if (error) return
       if (cubemain_spectrum_blank(inspec)) then
          ouspec%t(:) = gr4nan
       else
          call fit%spec_import(prog%hfs,init,error)
          if (error) return
          call fit%fit(inspec,prog%glowin,error)
          if (error) return
          call fit%export(ouspec,error)
          if (error) return
       endif
    else
       ouspec%t(:) = gr4nan
    endif
    call ouspec%put(prog%oupars,ie,error)
    if (error) return
  end subroutine cubefit_minimize_prog_act_init
endmodule cubefit_minimize
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
