!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubesimulate_language
  use cubetools_structure
  use cubesimulate_messaging
  !
  use cubesimulate_configuration
  use cubesimulate_coverage
  use cubesimulate_observatory
  use cubesimulate_tuning
  !
  public :: cubesimulate_register_language
  private
  !
  integer(kind=lang_k) :: langid
  !
contains
  !
  subroutine cubesimulate_register_language(error)
    !----------------------------------------------------------------------
    ! Register the SIMULATE\ language
    !----------------------------------------------------------------------
    logical, intent(inout) :: error
    !
    call cubetools_register_language('SIMULATE',&
         'J.Pety, S.Bardeau',&
         'Commands to simulate observations',&
         'gag_doc:hlp/cube-help-ima.hlp',& !***JP: Shall it be a different name
         cubesimulate_execute_command,langid,error)
    if (error) return
    !
    call configuration_comm%register(error)
    if (error) return
    call coverage%register(error)
    if (error) return
    call observatory_comm%register(error)
    if (error) return
    call tuning_comm%register(error)
    if (error) return
    !
    call cubetools_register_dict(error)
    if (error) return
  end subroutine cubesimulate_register_language
  !
  subroutine cubesimulate_execute_command(line,comm,error)
    use cubeadm_opened
    use cubeadm_timing
    !----------------------------------------------------------------------
    ! Execute a command of the SIMULATE\ language
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    character(len=*), intent(in)    :: comm
    logical,          intent(inout) :: error
    !
    character(len=*), parameter :: rname='EXECUTE>COMMAND'
    !
    error = .false.
    if (comm.eq.'SIMULATE?') then
       call cubetools_list_language_commands(langid,error)
       if (error) return
    else
       call cubeadm_timing_init()
       call cubetools_execute_command(line,langid,comm,error)
       if (error) continue ! To ensure error recovery in the call to finalize_all
       call cubeadm_finish_all(comm,line,error)
       if (error) continue ! To ensure error recovery in timing
       call cubeadm_timing_final(comm)
    endif
  end subroutine cubesimulate_execute_command
end module cubesimulate_language
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
