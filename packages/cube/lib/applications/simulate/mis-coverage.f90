!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mis_cover_sicdef_user(error)
  use gbl_message
  use gkernel_interfaces
  use maptools_parameters
  use mapmis_interfaces, except_this=>mis_cover_sicdef_user
  use mapmis_buffer ! *** JP: Could this be a problem in parallel mode?
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  logical, intent(inout) :: error
  !
  character(len=varname_length) :: name,prefix
  character(len=*), parameter :: rname='MIS>COVER>SICDEF>USER'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  name = 'cov'
  if (.not.sic_varexist(name)) then
     prefix = trim(name)//'%'
     call sic_defstructure(name,code_sicvar_global,error)
     call sic_def_logi (trim(prefix)//'fullysampled',usercov%fullysampled,code_readwrite,error)
     call sic_def_logi (trim(prefix)//'ideal',usercov%ideal,code_readwrite,error)
     call sic_def_charn(trim(prefix)//'config',usercov%config,0,0,code_readwrite,error)
     call sic_def_charn(trim(prefix)//'source',usercov%source,0,0,code_readwrite,error)
     call sic_def_charn(trim(prefix)//'line',usercov%line,0,0,code_readwrite,error)
     call sic_def_dble (trim(prefix)//'lat',usercov%lat,0,0,code_readwrite,error)
     call sic_def_dble (trim(prefix)//'hor',usercov%hor,0,0,code_readwrite,error)
     call sic_def_charn(trim(prefix)//'ra',usercov%ra,0,0,code_readwrite,error)
     call sic_def_charn(trim(prefix)//'dec',usercov%dec,0,0,code_readwrite,error)
     call sic_def_dble (trim(prefix)//'freq',usercov%freq,0,0,code_readwrite,error)
     call sic_def_inte (trim(prefix)//'nchan',usercov%nchan,0,0,code_readwrite,error)
     call sic_def_dble (trim(prefix)//'hour',usercov%hour,1,2,code_readwrite,error)
     call sic_def_dble (trim(prefix)//'dt',usercov%dt,0,0,code_readwrite,error)
     call sic_def_dble (trim(prefix)//'xoffset',usercov%xoffset,1,2,code_readwrite,error)
     call sic_def_dble (trim(prefix)//'xcenter',usercov%xcenter,1,2,code_readwrite,error)
     call sic_def_dble (trim(prefix)//'xsize',usercov%xsize,1,2,code_readwrite,error)
     call sic_def_real (trim(prefix)//'xsamp',usercov%xsamp,1,2,code_readwrite,error)
     call sic_def_dble (trim(prefix)//'uoffset',usercov%uoffset,1,2,code_readwrite,error)
     call sic_def_dble (trim(prefix)//'ucenter',usercov%ucenter,1,2,code_readwrite,error)
     call sic_def_dble (trim(prefix)//'usize',usercov%usize,1,2,code_readwrite,error)
     call sic_def_real (trim(prefix)//'usamp',usercov%usamp,1,2,code_readwrite,error)
  endif
  !
end subroutine mis_cover_sicdef_user
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mis_coverage_command(mapset,line,uvou,error)
  use gbl_message
  use gkernel_interfaces
  use maptools_setup_types
  use mapuv_types
  use mapmis_interfaces, except_this=>mis_coverage_command
  use mapmis_buffer
  !----------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !     COVERAGE
  !----------------------------------------------------------------------
  type(map_setup_t), intent(in)    :: mapset
  character(len=*),  intent(in)    :: line
  type(uv_t),        intent(inout) :: uvou  
  logical,           intent(inout) :: error
  !
  type(cov_t) :: progcov
  character(len=*), parameter :: rname='MIS>COVERAGE>COMMAND'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  progcov%uv%fullysampled = usercov%fullysampled
  if (progcov%uv%fullysampled) then
     call mis_cover_uv_fullysampled(mapset,usercov,progcov,uvou,error)
     if (error) return
  else
     call mis_cover_uv_undersampled(mapset,usercov,progcov,uvou,error)
     if (error) return
  endif
  !
end subroutine mis_coverage_command
!
subroutine mis_cover_uv_fullysampled(mapset,usercov,progcov,uvou,error)
  use gbl_message
  use maptools_setup_types
  use mapuv_types
  use mapmis_interfaces, except_this=>mis_cover_uv_fullysampled
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(map_setup_t), intent(in)    :: mapset
  type(siccov_t),    intent(in)    :: usercov
  type(cov_t),       intent(inout) :: progcov
  type(uv_t),        intent(inout) :: uvou
  logical,           intent(inout) :: error
  !
  character(len=*), parameter :: rname='MIS>COVER>UV>FULLYSAMPLED'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  ! *** JP this is a bold assumption for the moment...
  progcov%setup%kind = code_otf
  call mis_set_proj(usercov,progcov%proj,error)
  if (error) return
  call mis_set_primbeam(progcov%rec,progcov%conf,progcov%prim,error)
  if (error) return
  call mis_set_grid(usercov,progcov%prim,code_xy,progcov%xy,error)
  if (error) return
  call mis_set_grid(usercov,progcov%prim,code_uv,progcov%uv,error)
  if (error) return
  call mis_fill_uv_fullysampled(mapset,progcov,uvou,error)
  if (error) return
  !
end subroutine mis_cover_uv_fullysampled
!
subroutine mis_cover_uv_undersampled(mapset,usercov,progcov,uvou,error)
  use gbl_message
  use maptools_setup_types
  use mapuv_types
  use mapmis_interfaces, except_this=>mis_cover_uv_undersampled
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(map_setup_t), intent(in)    :: mapset
  type(siccov_t),    intent(in)    :: usercov
  type(cov_t),       intent(inout) :: progcov
  type(uv_t),        intent(inout) :: uvou
  logical,           intent(inout) :: error
  !
  type(cov_t) :: cov
  character(len=*), parameter :: rname='MIS>COVER>UV>UNDERSAMPLED'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  call mis_set_rec(usercov,progcov%rec,error)
  if (error) return
  call mis_set_obs(usercov,progcov%obs,error)
  if (error) return
  call mis_set_config(usercov,progcov%conf,error)
  if (error) return
  call mis_set_proj(usercov,progcov%proj,error)
  if (error) return
  call mis_set_primbeam(progcov%rec,progcov%conf,progcov%prim,error)
  if (error) return
  call mis_set_grid(usercov,progcov%prim,code_xy,progcov%xy,error)
  if (error) return
  call mis_set_hang(usercov,progcov%obs,progcov%proj,progcov%hang,error)
  if (error) return
  call mis_set_setup(usercov,progcov%conf,progcov%xy,progcov%setup,error)
  if (error) return
  call mis_set_sequence(mapset,progcov%xy%row,progcov%hang,progcov%setup,progcov%seq,error)
  if (error) return
  call mis_fill_uv_undersampled(mapset,progcov,uvou,error)
  if (error) return
  !
end subroutine mis_cover_uv_undersampled
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mis_set_primbeam(rec,conf,prim,error)
  use gbl_message
  use phys_const
  use mapmis_interfaces, except_this=>mis_set_primbeam
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(recev_t),  intent(in)    :: rec
  type(config_t), intent(in)    :: conf
  type(cgauss_t), intent(out)   :: prim
  logical,        intent(inout) :: error
  !
  character(len=message_length) :: mess
  character(len=*), parameter :: rname='MIS>SET>PRIMBEAM'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  prim%diam = conf%diam
  ! Assuming -12.5db edge taper in the illumination *** JP
  prim%fwhm  = 1.2*rec%wave/prim%diam
  prim%sigma = prim%fwhm/(2.0*sqrt(log(2.0)))
  prim%nyquist = 0.5*rec%wave/prim%diam
  !
  write(mess,'(a,t24,f9.2,a)') 'Observing frequency:  ',rec%freq/1000d0,' GHz'
  call mis_message(seve%r,rname,mess)
  write(mess,'(a,t24,f9.2,a)') 'Observing wavelength: ',rec%wave*1000d0,' mm'
  call mis_message(seve%r,rname,mess)
  write(mess,'(a,t24,f9.2,a)') 'Antenna diameter: ',prim%diam,' m'
  call mis_message(seve%r,rname,mess)
  write(mess,'(a,t24,f9.2,a)') 'Primary FWHM: ',prim%fwhm*sec_per_rad,'"'
  call mis_message(seve%r,rname,mess)
  write(mess,'(a,t24,f9.2,a)') 'Nyquist rate: ',prim%nyquist*sec_per_rad,'"'
  call mis_message(seve%r,rname,mess)
  call blankline()
  !
end subroutine mis_set_primbeam
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mis_set_proj(user,proj,error)
  use gbl_message
  use mapmis_interfaces, except_this=>mis_set_proj
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(siccov_t), intent(in)    :: user
  type(proj_t),   intent(inout) :: proj
  logical,        intent(inout) :: error
  !
  character(len=16) :: ra,dec
  character(len=*), parameter :: rname='MIS>SET>PROJ'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  ! Source name
  if (user%source.eq.'') then
     ! Default
     proj%name = 'HORSEHEAD'
  else
     ! User defined
     proj%name = user%source
  endif
  ! Coordinates
  if ((user%ra.eq.'').or.(user%dec.eq.'')) then
     ! Default: Horsehead
     ra  = '05:40:54.270'
     dec = '-02:28:00.00'
  else
     ! User defined
     ra  = user%ra
     dec = user%dec
  endif
  ! Conversion
  call sic_decode(ra,proj%ra,24,error)
  if (error) then
     call mis_message(seve%e,rname,'Input conversion error on source R.A. '//user%ra)
     return
  endif
  call sic_decode(dec,proj%dec,360,error)
  if (error) then
     call mis_message(seve%e,rname,'Input conversion error on source Dec '//user%dec)
     return
  endif
  !
end subroutine mis_set_proj
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mis_set_grid(user,prim,igrid,grid,error)
  use gbl_message
  use mapmis_interfaces, except_this=>mis_set_grid
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(siccov_t),  intent(in)    :: user
  type(cgauss_t),  intent(in)    :: prim
  integer(kind=4), intent(in)    :: igrid
  type(grid_t),    intent(inout) :: grid
  logical,         intent(inout) :: error
  !
  integer(kind=4), parameter :: ix = 1
  integer(kind=4), parameter :: iy = 2
  integer(kind=4) :: idir
  character(len=*), parameter :: rname='MIS>SET>GRID'
  !
  call mis_set_grid_geom(user,prim,igrid,grid,error)
  if (error) return
  call mis_set_grid_row_axes(ix,iy,code_para,code_perp,grid%geom,grid%row(code_lamb),error)
  if (error) return
  call mis_set_grid_row_axes(iy,ix,code_para,code_perp,grid%geom,grid%row(code_beta),error)
  if (error) return
  do idir=1,2
     grid%row(idir)%kind = idir
     call mis_set_grid_row_offsets(grid%row(idir),error)
     if (error) return
  enddo ! idir
  call mis_set_grid_print(code_para,code_perp,grid,error)
  if (error) return
  call blankline()
  !
end subroutine mis_set_grid
!
subroutine mis_set_grid_geom(user,prim,igrid,grid,error)
  use gbl_message
  use phys_const
  use mapmis_interfaces, except_this=>mis_set_grid_geom
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(siccov_t),  intent(in)    :: user
  type(cgauss_t),  intent(in)    :: prim
  integer(kind=4), intent(in)    :: igrid
  type(grid_t),    intent(inout) :: grid
  logical,         intent(inout) :: error
  !
  character(len=message_length) :: mess
  character(len=*), parameter :: rname='MIS>SET>GRID>GEOM'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  if (igrid.eq.code_xy) then
     ! Perpendicular sampling
     if (user%xsamp(1).eq.0.0) then
        ! Default
        grid%geom%inc(code_perp) = prim%nyquist
     else
        ! User defined
        if (user%xsamp(1).lt.0.1) then
           call mis_message(seve%e,rname,'Less than one raw in 10*Nyquist is forbidden!')
           error = .true.
           return
        endif
        grid%geom%inc(code_perp) = prim%nyquist/user%xsamp(1)
     endif
     ! Parallel sampling
     if (user%xsamp(2).eq.0.0) then
        ! Default: Less than 1% error (see pety et al. 2010, appendix C.4)
        grid%geom%inc(code_para) = prim%fwhm/5d0
     else
        ! User defined
        if (user%xsamp(2).lt.0.5) then
           call mis_message(seve%e,rname,'Less than 1 point every two beams is forbidden!')
           error = .true.
           return
        endif
        grid%geom%inc(code_para) = prim%fwhm/user%xsamp(2)
     endif
     ! Field of view size
     if (any(user%xsize.eq.0d0)) then
        grid%geom%size = 0d0
     else
        grid%geom%size = user%xsize*rad_per_sec
     endif
     ! Field of view center
     grid%geom%center = user%xcenter*rad_per_sec
     ! Number of dumps and lines
     ! Miscellaneous
     grid%factor = sec_per_rad
     grid%unit = '"'
  else if (igrid.eq.code_uv) then
     ! Perpendicular sampling
     if (user%usamp(1).eq.0.0) then
        ! Default
        grid%geom%inc(code_perp) = prim%diam/2d0
     else
        ! User defined
        if (user%usamp(1).lt.0.1) then
           call mis_message(seve%e,rname,'Less than one raw in 10*Nyquist is forbidden!')
           error = .true.
           return
        endif
        grid%geom%inc(code_perp) = prim%diam/2d0/user%usamp(1)
     endif
     ! Parallel sampling
     if (user%usamp(2).eq.0.0) then
        ! Default
        grid%geom%inc(code_para) = prim%diam/5d0
     else
        ! User defined
        if (user%usamp(2).lt.0.5) then
           call mis_message(seve%e,rname,'Less than 1 point every two beams is forbidden!')
           error = .true.
           return
        endif
        grid%geom%inc(code_para) = prim%diam/2d0/user%usamp(2)
     endif
     ! UV size & center
     grid%geom%size = user%usize
     grid%geom%center = user%ucenter
     ! Miscellaneous
     grid%factor = 1d0
     grid%unit = ' m'
  else
     write(mess,'(a,i0)') 'Unknown grid kind: ',igrid
     call mis_message(seve%e,rname,mess)
     error = .true.
     return
  endif
  grid%kind = igrid
  !
end subroutine mis_set_grid_geom
!
subroutine mis_set_grid_row_axes(ix,iy,ipara,iperp,geom,row,error)
  use gbl_message
  use maptools_interfaces
  use mapmis_interfaces, except_this=>mis_set_grid_row_axes
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)    :: ix
  integer(kind=4), intent(in)    :: iy
  integer(kind=4), intent(in)    :: ipara
  integer(kind=4), intent(in)    :: iperp
  type(geom_t),    intent(in)    :: geom
  type(row_t),     intent(inout) :: row
  logical,         intent(inout) :: error
  !
  integer(kind=4) :: npix(2),iaxis
  real(kind=8) :: inc(2),val(2),ref(2)
  character(len=*), parameter :: rname='MIS>SET>GRID>ROW>AXES'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  inc = geom%inc
  val(ipara) = geom%center(ix)
  val(iperp) = geom%center(iy)
  npix(ipara) = oddceiling(abs(geom%size(ix)/inc(ipara)))
  npix(iperp) = oddceiling(abs(geom%size(iy)/inc(iperp)))
  ref(ipara) = dble(int(npix(ipara)/2)+1)+geom%offset(ix)
  ref(iperp) = dble(int(npix(iperp)/2)+1)+geom%offset(iy)
  do iaxis=1,2
     call maptools_set_axis(npix(iaxis),inc(iaxis),val(iaxis),ref(iaxis),&
          row%axes(iaxis),error)
     if (error) return
  enddo ! iaxis
  row%nline = npix(iperp)
  row%ndlin = npix(ipara)
  row%ndcov = npix(ipara)*npix(iperp)
  !
end subroutine mis_set_grid_row_axes
!
subroutine mis_set_grid_row_offsets(row,error)
  use gbl_message
  use mapmis_interfaces, except_this=>mis_set_grid_row_offsets
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(row_t), intent(inout) :: row
  logical,     intent(inout) :: error
  !
  integer(kind=4) :: iline,ifirst,ilast
  integer(kind=4) :: ipara,iperp
  character(len=message_length) :: mess
  character(len=*), parameter :: rname='MIS>SET>GRID>ROW>OFFSETS'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  if (row%kind.eq.code_lamb) then
     ipara = 1
     iperp = 2
  else if (row%kind.eq.code_beta) then
     ipara = 2
     iperp = 1
  else
     write(mess,'(a,i0)') 'Unknown row kind: ',row%kind
     call mis_message(seve%e,rname,mess)
     error = .true.
     return
  endif
  call maptools_allocate_dble_xy(kind_otf(row%kind)//" offsets",row%ndcov,2,row%off,error)
  if (error) return
  ifirst = 1
  do iline=1,row%nline
     ilast = ifirst+row%ndlin-1
     row%off%val(ifirst:ilast,iperp) = row%axes(code_perp)%coord%val(iline)
     row%off%val(ifirst:ilast,ipara) = row%axes(code_para)%coord%val
     ifirst = ilast+1
  enddo ! iline
  !
end subroutine mis_set_grid_row_offsets
!
subroutine mis_set_grid_print(ipara,iperp,grid,error)
  use gbl_message
  use mapmis_interfaces, except_this=>mis_set_grid_print
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)    :: ipara
  integer(kind=4), intent(in)    :: iperp
  type(grid_t),    intent(in)    :: grid
  logical,         intent(inout) :: error
  !
  integer(kind=4) :: idir,iaxis
  real(kind=8) :: size(2)
  character(len=message_length) :: mess
  character(len=*), parameter :: rname='MIS>SET>GRID>PRINT'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  call mis_message(seve%r,rname,'XY plane geometry:')
  write(mess,'(a,f7.2,a)') '    Parallel      sampling rate: 1 dump every ',grid%geom%inc(ipara)*grid%factor,grid%unit
  call mis_message(seve%r,rname,mess)
  write(mess,'(a,f7.2,a)') '    Perpendicular sampling rate: 1 row  every ',grid%geom%inc(iperp)*grid%factor,grid%unit
  call mis_message(seve%r,rname,mess)
  write(mess,'(a,f7.2,a,a,f7.2,a)') '    Center: ',grid%geom%center(1)*grid%factor,grid%unit,' x ',&
       grid%geom%center(2)*grid%factor,grid%unit
  call mis_message(seve%r,rname,mess)
  write(mess,'(a,f7.2,a,a,f7.2,a)') '    Size:   ',grid%geom%size(1)*grid%factor,grid%unit,' x ',&
       grid%geom%size(2)*grid%factor,grid%unit
  call mis_message(seve%r,rname,mess)
  do idir=1,2
     do iaxis=1,2
        size(iaxis) = grid%row(idir)%axes(iaxis)%n*grid%row(idir)%axes(iaxis)%inc*grid%factor
     enddo ! iaxis
     write(mess,'(a,a,a,i0,a,i0,a,f7.2,a,a,f7.2,a,a)') '    ',kind_otf(grid%row(idir)%kind),&
          ' scanning: ',grid%row(idir)%nline,' rows x ',grid%row(idir)%ndlin,&
          ' dumps (= ',size(1),grid%unit,' x ',size(2),grid%unit,')'
     call mis_message(seve%r,rname,mess)
  enddo ! idir
  !
end subroutine mis_set_grid_print
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mis_set_hang(user,obs,proj,hang,error)
  use gbl_message
  use phys_const
  use mapmis_interfaces, except_this=>mis_set_hang
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(siccov_t), intent(in)    :: user
  type(obs_t),    intent(in)    :: obs
  type(proj_t),   intent(in)    :: proj
  type(minmax_t), intent(inout) :: hang
  logical,        intent(inout) :: error
  !
  real(kind=8) :: hour(2)
  character(len=message_length) :: mess
  character(len=*), parameter :: rname='MIS>SET>HANG'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  ! Sanity check
  if (user%hour(1).gt.user%hour(2)) then
     call mis_message(seve%e,rname,"Hour angle range must be ordered")
     error = .true.
     return
  endif
  ! Default
  call mis_set_hang_minmax(obs,proj,hang,error)
  if (error) return
  ! User defined
  hour = user%hour*rad_per_hang
  if (hour(1).ne.0.0) then
     if (hour(1).lt.hang%min) then
        write(mess,'(a,f7.2)') 'The source rises only at ',hang%min*hang_per_rad
        call mis_message(seve%e,rname,mess)
        error = .true.
        return
     else
        hang%min = hour(1)
     endif
  endif
  if (hour(2).ne.0.0) then
     if (hour(2).gt.hang%max) then
        write(mess,'(a,f7.2)') 'The source is already set. Setting time: ',hang%max*hang_per_rad
        call mis_message(seve%e,rname,mess)
        error = .true.
        return
     else
        hang%max = hour(2)
     endif
  endif
  !
end subroutine mis_set_hang
!
subroutine mis_set_hang_minmax(obs,proj,hang,error)
  use gbl_message
  use phys_const
  use mapmis_interfaces, except_this=>mis_set_hang_minmax
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  ! Formula on the projection plane for the visible part of the sky
  !   Case 1: pi/2-lat+hor < dec                 => circumpolar
  !   Case 2: lat-pi/2+hor < dec < pi/2-lat+hor  => h = acos(tan(dec)/tan(lat-pi/2))
  !   Case 3: dec < lat-pi/2+hor                 => invisible (always set)
  !---------------------------------------------------------------------
  type(obs_t),    intent(in)    :: obs
  type(proj_t),   intent(in)    :: proj
  type(minmax_t), intent(inout) :: hang
  logical,        intent(inout) :: error
  !
  real(kind=8) :: lat,dec,amin
  character(len=message_length) :: mess
  character(len=*), parameter :: rname='MIS>SET>HANG>MINMAX'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  if (obs%lat.lt.0.) then
     ! South hemisphere, e.g. ALMA
     dec = -proj%dec
     lat = -obs%lat
  else
     ! North hemisphere, e.g. NOEMA
     dec = proj%dec
     lat = obs%lat
  endif
  lat = lat-pi*0.5d0
  amin = (-sin(obs%hor)+sin(dec)*cos(lat))/(cos(dec)*sin(lat))
  if (amin.le.-1.0) then
     ! Circumpolar
     hang%min = -pi
  else if (amin.ge.1.0) then
     ! Invisible
     dec = proj%dec*deg_per_rad
     lat = obs%lat*deg_per_rad
     write(mess,'(a,f7.2,a,f7.2,a)') 'The source is invisible (Dec: ',dec,' Latitude: ',lat,')'
     call mis_message(seve%e,rname,mess)
     error = .true.
     return
  else
     hang%min = -acos(amin)
  endif
  hang%max = -hang%min
  !
end subroutine mis_set_hang_minmax
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mis_set_setup(user,conf,xy,setup,error)
  use gbl_message
  use mapmis_interfaces, except_this=>mis_set_setup
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(siccov_t), intent(in)    :: user
  type(config_t), intent(in)    :: conf
  type(grid_t),   intent(in)    :: xy
  type(setup_t),  intent(out)   :: setup
  logical,        intent(inout) :: error
  !
  character(len=message_length) :: mess
  character(len=*), parameter :: rname='MIS>SET>SETUP'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  setup%ideal = user%ideal
  call mis_set_setup_dump_minmax(conf,setup%integ,error)
  if (error) return
  call mis_set_setup_cali(setup%cali,error)
  if (error) return
  if (all(xy%geom%size.eq.0d0)) then
     call mis_set_setup_single(user,setup,error)
     if (error) return
  else
! *** JP There should be another alternative here
!!$     call mis_set_setup_mosaic(user,setup,error)
!!$     if (error) return
     call mis_set_setup_otf(user,xy%row,setup,error)
     if (error) return
  endif
  ! User feedback
  call mis_message(seve%r,rname,'Dump times:')
  write(mess,'(a,f7.2,a)') '   Minimum duration:  ',setup%integ%min,' sec'
  call mis_message(seve%r,rname,mess)
  write(mess,'(a,f7.2,a)') '   Maximum duration:  ',setup%integ%max,' sec'
  call mis_message(seve%r,rname,mess)
  write(mess,'(a,f7.2,a)') '   Actual  duration:  ',setup%dump%time,' sec'
  call mis_message(seve%r,rname,mess)
  write(mess,'(a,f7.2,a)') '   Overhead:          ',setup%dump%over,' sec'
  call mis_message(seve%r,rname,mess)
  write(mess,'(a,f7.2,a)') '   Total:             ',setup%dump%tot,' sec'
  call mis_message(seve%r,rname,mess)
  write(mess,'(a,f7.2,a)') '   Efficiency:        ',setup%dump%eff,' %'
  call mis_message(seve%r,rname,mess)
  if (setup%kind.eq.code_single) then
     call mis_message(seve%r,rname,'Single pointing subscan times:')
     write(mess,'(a,f7.2,a)') '   Duration:   ',setup%single%time,' sec'
     call mis_message(seve%r,rname,mess)
     write(mess,'(a,f7.2,a)') '   Overhead:   ',setup%single%over,' sec'
     call mis_message(seve%r,rname,mess)
     write(mess,'(a,f7.2,a)') '   Total:      ',setup%single%tot,' sec'
     call mis_message(seve%r,rname,mess)
     write(mess,'(a,f7.2,a)') '   Efficiency: ',setup%single%eff,' %'
     call mis_message(seve%r,rname,mess)
  else if (setup%kind.eq.code_single) then
     call mis_message(seve%e,rname,"Mosaic mode not yet implemented")
     error = .true.
     return
  else if (setup%kind.eq.code_otf) then
     call mis_message(seve%r,rname,'Lambda subscan times:')
     write(mess,'(a,f7.2,a)') '   Duration:   ',setup%lamb%time,' sec'
     call mis_message(seve%r,rname,mess)
     write(mess,'(a,f7.2,a)') '   Overhead:   ',setup%lamb%over,' sec'
     call mis_message(seve%r,rname,mess)
     write(mess,'(a,f7.2,a)') '   Total:      ',setup%lamb%tot,' sec'
     call mis_message(seve%r,rname,mess)
     write(mess,'(a,f7.2,a)') '   Efficiency: ',setup%lamb%eff,' %'
     call mis_message(seve%r,rname,mess)
     call mis_message(seve%r,rname,'Beta   subscan times:')
     write(mess,'(a,f7.2,a)') '   Duration:   ',setup%beta%time,' sec'
     call mis_message(seve%r,rname,mess)
     write(mess,'(a,f7.2,a)') '   Overhead:   ',setup%beta%over,' sec'
     call mis_message(seve%r,rname,mess)
     write(mess,'(a,f7.2,a)') '   Total:      ',setup%beta%tot,' sec'
     call mis_message(seve%r,rname,mess)
     write(mess,'(a,f7.2,a)') '   Efficiency: ',setup%beta%eff,' %'
     call mis_message(seve%r,rname,mess)
  endif
  call mis_message(seve%r,rname,'Calibration times:')
  write(mess,'(a,f7.2,a)') '   Maximum periodicity:  ',setup%cali%max,' sec'
  call mis_message(seve%r,rname,mess)
  write(mess,'(a,f7.2,a)') '   Duration:             ',setup%cali%time,' sec'
  call mis_message(seve%r,rname,mess)
  write(mess,'(a,f7.2,a)') '   Overhead:             ',setup%cali%over,' sec'
  call mis_message(seve%r,rname,mess)
  write(mess,'(a,f7.2,a)') '   Efficiency:           ',setup%cali%eff,' %'
  call mis_message(seve%r,rname,mess)
  call blankline()
  !
end subroutine mis_set_setup
!
subroutine mis_set_setup_dump_minmax(conf,integ,error)
  use gbl_message
  use phys_const
  use mapmis_interfaces, except_this=>mis_set_setup_dump_minmax
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(config_t), intent(in)    :: conf
  type(minmax_t), intent(out)   :: integ
  logical,        intent(inout) :: error
  !
  real(kind=8) :: dmax,dalias
  real(kind=8), parameter :: wearth = 7.27d-5 ! [rad/s]
  character(len=*), parameter :: rname='MIS>SET>SETUP>DUMP>MINMAX'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  integ%min = 2.0
  ! See pety et al. 2010, appendix B
  dalias = 0.5*conf%diam
  dmax = conf%size
  integ%max = 0.1*dalias/dmax/wearth
  if (integ%max.lt.integ%min) then
     call mis_message(seve%e,rname,'Recommanded dumping rate is too fast: The system will not follow')
     error = .true.
     return
  endif
  !
end subroutine mis_set_setup_dump_minmax
!
subroutine mis_set_setup_cali(cali,error)
  use gbl_message
  use mapmis_interfaces, except_this=>mis_set_setup_cali
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(time_t),   intent(out)   :: cali
  logical,        intent(inout) :: error
  !
  character(len=*), parameter :: rname='MIS>SET>SETUP>CALI'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  cali%max = 20.0*60.0
  cali%time = 5.0*60.0
  cali%over = 1.0*60.0
  call mis_set_setup_time(cali,error)
  if (error) return
  !
end subroutine mis_set_setup_cali
!
subroutine mis_set_setup_single(user,setup,error)
  use gbl_message
  use mapmis_interfaces, except_this=>mis_set_setup_single
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(siccov_t), intent(in)    :: user
  type(setup_t),  intent(inout) :: setup
  logical,        intent(inout) :: error
  !
  character(len=*), parameter :: rname='MIS>SET>SETUP>SINGLE'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  ! Observing kind
  setup%kind = code_single
  ! Single dump
  call mis_set_setup_dump(user,setup%integ,setup%integ%max,1d0,setup%dump,error)
  if (error) return
  ! Subscan
  setup%single%over = 5.0 ! second
  setup%single%time = setup%dump%tot
  call mis_set_setup_time(setup%single,error)
  if (error) return
  !
end subroutine mis_set_setup_single
!
subroutine mis_set_setup_mosaic(user,setup,error)
  use gbl_message
  use mapmis_interfaces, except_this=>mis_set_setup_mosaic
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(siccov_t), intent(in)    :: user
  type(setup_t),  intent(inout) :: setup
  logical,        intent(inout) :: error
  !
  character(len=*), parameter :: rname='MIS>SET>SETUP>MOSAIC'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  ! Observing kind
  setup%kind = code_mosaic
  ! Mosaic dump
  call mis_set_setup_dump(user,setup%integ,setup%integ%max,1d0,setup%dump,error)
  if (error) return
  ! Subscan
  setup%mosaic%over = 5.0 ! second
  setup%mosaic%time = setup%dump%tot
  call mis_set_setup_time(setup%mosaic,error)
  if (error) return
  !
end subroutine mis_set_setup_mosaic
!
subroutine mis_set_setup_otf(user,xyrow,setup,error)
  use gbl_message
  use mapmis_interfaces, except_this=>mis_set_setup_otf
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(siccov_t), intent(in)    :: user
  type(row_t),    intent(in)    :: xyrow(2)
  type(setup_t),  intent(inout) :: setup
  logical,        intent(inout) :: error
  !
  character(len=*), parameter :: rname='MIS>SET>SETUP>OTF'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  ! Observing kind
  setup%kind = code_otf
  ! OTF dump => continuous observation
  call mis_set_setup_dump(user,setup%integ,setup%integ%min,0d0,setup%dump,error)
  if (error) return
  ! Lambda and beta scanning subscans
  call mis_set_setup_otf_dir(xyrow(code_lamb),setup%dump,setup%lamb,error)
  if (error) return
  call mis_set_setup_otf_dir(xyrow(code_beta),setup%dump,setup%beta,error)
  if (error) return
  !
end subroutine mis_set_setup_otf
!
subroutine mis_set_setup_dump(user,integ,deftime,overhead,dump,error)
  use gbl_message
  use mapmis_interfaces, except_this=>mis_set_setup_dump
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(siccov_t), intent(in)    :: user
  type(minmax_t), intent(in)    :: integ
  real(kind=8),   intent(in)    :: deftime
  real(kind=8),   intent(in)    :: overhead
  type(time_t),   intent(out)   :: dump
  logical,        intent(inout) :: error
  !
  character(len=*), parameter :: rname='MIS>SET>SETUP>DUMP'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  dump%over = overhead
  if (user%dt.eq.0.0) then
     ! Default
     dump%time = deftime
  else
     ! User defined
     dump%time = user%dt
     if (dump%time.gt.integ%max) then
        call mis_message(seve%e,rname,'Dumping rate is too slow: There will be aliasing')
     else if (dump%time.lt.integ%min) then
        call mis_message(seve%e,rname,'Dumping rate is too fast: The system will not follow')
     endif
  endif
  call mis_set_setup_time(dump,error)
  if (error) return
  !
end subroutine mis_set_setup_dump
!
subroutine mis_set_setup_otf_dir(xyrow,dump,dir,error)
  use gbl_message
  use mapmis_interfaces, except_this=>mis_set_setup_otf_dir
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(row_t),  intent(in)    :: xyrow
  type(time_t), intent(in)    :: dump
  type(time_t), intent(out)   :: dir
  logical,      intent(inout) :: error
  !
  character(len=*), parameter :: rname='MIS>SET>SETUP>OTF>DIR'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  dir%time = xyrow%ndlin*dump%time ! [s]
  dir%over = 5.0 ! [s]
  call mis_set_setup_time(dir,error)
  if (error) return
  !
end subroutine mis_set_setup_otf_dir
!
subroutine mis_set_setup_time(time,error)
  use gbl_message
  use mapmis_interfaces, except_this=>mis_set_setup_time
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(time_t),   intent(inout) :: time
  logical,        intent(inout) :: error
  !
  time%tot = time%time + time%over
  time%eff = 100d0 * time%time / time%tot
  !
end subroutine mis_set_setup_time
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mis_set_sequence(mapset,xyrow,hang,setup,seq,error)
  use gbl_message
  use phys_const
  use maptools_setup_types
  use maptools_interfaces
  use mapmis_interfaces, except_this=>mis_set_sequence
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(map_setup_t), intent(in)    :: mapset
  type(row_t),       intent(inout) :: xyrow(2)
  type(minmax_t),    intent(in)    :: hang
  type(setup_t),     intent(in)    :: setup
  type(seq_t),       intent(inout) :: seq
  logical,           intent(inout) :: error
  !
  logical :: single,mosaic,otf
  integer(kind=4) :: idump,icur,nmax,subscankind
  real(kind=4) :: lambcov,betacov,subscancov
  real(kind=8) :: hcur
  real(kind=8) :: calitot,calimax,calicur
  real(kind=8) :: scanover,scantot
  real(kind=8) :: subscanover,subscantot,subscanmax,subscancur
  real(kind=8) :: lambover,lambtot,lambmax
  real(kind=8) :: betaover,betatot,betamax
  real(kind=8) :: dumptime,dumpover,dumptot
  real(kind=8), parameter :: rad_per_second = pi/(12d0*3600d0)
  character(len=message_length) :: mess
  character(len=*), parameter :: rname='MIS>SET>SEQUENCE'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  ! Kind
  single = setup%kind.eq.code_single
  mosaic = setup%kind.eq.code_mosaic
  otf    = setup%kind.eq.code_otf
  ! Unit conversion
  calitot = setup%cali%tot*rad_per_second
  calimax = setup%cali%max*rad_per_second
  if (single) then
     subscanover = setup%single%over*rad_per_second
     subscantot = setup%single%tot*rad_per_second
     subscanmax = subscantot + subscanover/1000d0
     scanover = 0d0
     scantot = floor((calimax-scanover)/subscantot)*subscantot
  else if (mosaic) then
     call mis_message(seve%e,rname,"Mosaic not yet implemented")
     error = .true.
     return
  else if (otf) then
     scantot = 0d0 ! This is because we can stop in the middle of a scan in otf mode
     scanover = 0d0
     !
     lambover = setup%lamb%over*rad_per_second
     lambtot = setup%lamb%tot*rad_per_second
     lambmax = lambtot + lambover/1000d0
     lambcov = 1.0/xyrow(code_lamb)%nline
     !
     betaover = setup%beta%over*rad_per_second
     betatot = setup%beta%tot*rad_per_second
     betamax = betatot + betaover/1000d0
     betacov = 1.0/xyrow(code_beta)%nline
     !
     subscanover = lambover
     subscantot = lambtot
     subscanmax = lambmax
     subscancov = lambcov
  else
     write(mess,'(a,i0)') 'Unknown observing kind: ',setup%kind
     call mis_message(seve%e,rname,mess)
     error = .true.
     return
  endif
  dumptime = setup%dump%time*rad_per_second
  dumpover = setup%dump%over*rad_per_second
  dumptot = setup%dump%tot*rad_per_second
  !
  ! Memory allocation
  if (setup%ideal) then
     nmax = floor((hang%max-hang%min)/dumptime)
  else
     nmax = floor((hang%max-hang%min)/dumptot)
  endif
  if (nmax.lt.1) then
     call mis_message(seve%e,rname,'Less than 1 possible dump')
     error = .true.
     return
  endif
  call maptools_reallocate_dble_1d("hcur",nmax,seq%hcur,error)
  if (error) return
  call maptools_reallocate_dble_1d("xoff",nmax,seq%xoff,error)
  if (error) return
  call maptools_reallocate_dble_1d("yoff",nmax,seq%yoff,error)
  if (error) return
  call mis_set_sequence_print(mapset,"Source  rise ",1,hang%min,error)
  if (error) return
  call mis_set_sequence_print(mapset,"Session start",1,hang%min,error)
  if (error) return
  ! Sequence
  if (otf) then
     seq%ncov = 0.0
     idump = 0
     subscankind = code_lamb
  endif
  icur = 0
  if (setup%ideal) then
     ! Start at source rise
     hcur = hang%min
     ! Is there enough time for another dump before the source setting?
     do while (hcur.le.(hang%max-dumptime))
        icur = icur + 1
        if (icur.le.nmax) then
           ! The sky position is computed at half the dump integration time
           hcur = hcur+0.5*dumptime
           seq%hcur%val(icur) = hcur
           hcur = hcur+0.5*dumptime
           if (otf) then
              if (idump.lt.xyrow(subscankind)%ndcov) then
                 ! New dump inside a coverage
                 idump = idump+1
                 if (mod(idump,xyrow(subscankind)%ndlin).eq.0) then
                    seq%ncov = seq%ncov + subscancov
                 endif
              else
                 ! New coverage in perpendicular direction
                 idump = 1
                 if (subscankind.eq.code_lamb) then
                    subscankind = code_beta
                    subscancov = betacov
                 else if (subscankind.eq.code_beta) then
                    subscankind = code_lamb
                    subscancov = lambcov
                 else
                    write(mess,'(a,i0)') 'Unknown mapping kind: ',subscankind
                    call mis_message(seve%e,rname,mess)
                    error = .true.
                    return
                 endif
              endif
              seq%xoff%val(icur) = xyrow(subscankind)%off%val(idump,1)
              seq%yoff%val(icur) = xyrow(subscankind)%off%val(idump,2)
           else
              seq%xoff%val(icur) = 0d0
              seq%yoff%val(icur) = 0d0
           endif
        else
           write(mess,'(a,i0,a,i0,a)') 'More hour angles (',icur,') than expected (',nmax,')'
           call mis_message(seve%e,rname,mess)
           error = .true.
           return
        endif
     enddo
  else
     ! Start with a calibration
     seq%ncali = 1
     seq%nscan = 0
     seq%nsubs = 0
     call mis_set_sequence_print(mapset,"   cali start",seq%ncali,hang%min,error)
     if (error) return
     hcur = hang%min+calitot
     ! Is there enough time for the next cycle before the source setting?
     do while (hcur.le.(hang%max-(calitot+scantot)))
        ! Reset calibration cycle and start with the next scan overhead
        calicur = scanover
        seq%nscan = seq%nscan+1
        call mis_set_sequence_print(mapset,"   scan start",seq%nscan,hcur,error)
        if (error) return
        hcur = hcur + scanover
        ! Is there enough time for the next subscan before doing another calibration?
        do while ((calicur.le.(calimax-subscantot)).and.(hcur.le.(hang%max-(calitot+subscantot))))
           seq%nsubs = seq%nsubs+1
           if (seq%nsubs.le.2) then
              call mis_set_sequence_print(mapset,"      subscan start",seq%nsubs,hcur,error)
              if (error) return
           endif
           ! Reset the subscan cycle and start with the next subscan overhead
           subscancur = subscanover
           hcur = hcur + subscanover
           ! Is there enough time for the next dump before subscan ends?
           do while (subscancur.le.(subscanmax-dumptot))
              icur = icur + 1
              if (seq%nsubs.le.2) then
                 call mis_set_sequence_print(mapset,"         dump start",icur,hcur,error)
                 if (error) return
              endif
              ! Start with the next dump overhead
              subscancur = subscancur+dumpover
              hcur = hcur+dumpover
              if (icur.le.nmax) then
                 ! The sky position is computed at half the dump integration time
                 subscancur = subscancur+dumptime
                 hcur = hcur + 0.5*dumptime
                 seq%hcur%val(icur) = hcur
                 hcur = hcur + 0.5*dumptime
                 if (otf) then
                    if (idump.lt.xyrow(subscankind)%ndcov) then
                       ! New dump inside a coverage
                       idump = idump+1
                       if (mod(idump,xyrow(subscankind)%ndlin).eq.0) then
                          seq%ncov = seq%ncov + subscancov
                       endif
                    else
                       ! New coverage in perpendicular direction
                       idump = 1
                       if (subscankind.eq.code_lamb) then
                          subscankind = code_beta
                          subscanover = betaover
                          subscantot = betatot
                          subscanmax = betamax
                          subscancov = betacov
                       else if (subscankind.eq.code_beta) then
                          subscankind = code_lamb
                          subscanover = lambover
                          subscantot = lambtot
                          subscanmax = lambmax
                          subscancov = lambcov
                       else
                          write(mess,'(a,i0)') 'Unknown mapping kind: ',subscankind
                          call mis_message(seve%e,rname,mess)
                          error = .true.
                          return
                       endif
                    endif
                    seq%xoff%val(icur) = xyrow(subscankind)%off%val(idump,1)
                    seq%yoff%val(icur) = xyrow(subscankind)%off%val(idump,2)
                 else
                    seq%xoff%val(icur) = 0d0
                    seq%yoff%val(icur) = 0d0
                 endif
              else
                 write(mess,'(a,i0,a,i0,a)') 'More hour angles (',icur,') than expected (',nmax,')'
                 call mis_message(seve%e,rname,mess)
                 error = .true.
                 return
              endif
           enddo ! subscan cycle
           if (otf) then
              if (mod(idump,xyrow(subscankind)%ndlin).ne.0) then
                 write(mess,'(a,i0,a,i0)') 'Subscan ends before the current line: ',&
                      mod(idump-1,xyrow(subscankind)%ndlin)+1,' < ',xyrow(subscankind)%ndlin
                 call mis_message(seve%w,rname,mess)
              endif
           endif
           calicur = calicur+subscancur
        enddo ! calibration cycle
        ! End with a calibration
        seq%ncali = seq%ncali+1
        call mis_set_sequence_print(mapset,"   cali start",seq%ncali,hcur,error)
        if (error) return
        hcur = hcur + calitot
     enddo ! Session cycle
  endif
  call mis_set_sequence_print(mapset,"Session end  ",1,hcur,error)
  if (error) return
  call mis_set_sequence_print(mapset,"Source  set  ",1,hang%max,error)
  if (error) return
  call blankline()
  seq%ndump = icur
  if (seq%ndump.le.1) then
     call mis_message(seve%r,rname,'Less than 1 observed dump')
  endif
  seq%eff = 100.0*float(icur)/float(nmax)
  ! User feedback
  if (setup%ideal) then
     call mis_message(seve%r,rname,'Ideal setup')
  else
     call mis_message(seve%r,rname,'Observing setup: '//kind_setup(setup%kind))
  endif
  if (otf) then
     write(mess,'(a,f7.2)') 'Number of coverages: ',seq%ncov
     call mis_message(seve%r,rname,mess)
  endif
  if (.not.setup%ideal) then
     write(mess,'(a,i0)') 'Number of calibrations: ',seq%ncali
     call mis_message(seve%r,rname,mess)
     write(mess,'(a,i0)') 'Number of scans: ',seq%nscan
     call mis_message(seve%r,rname,mess)
     write(mess,'(a,i0)') 'Number of subscans: ',seq%nsubs
     call mis_message(seve%r,rname,mess)
  endif
  write(mess,'(a,i0)') 'Number of dumps: ',seq%ndump
  call mis_message(seve%r,rname,mess)
  write(mess,'(a,f7.2,a)') 'Observing efficiency: ',seq%eff,'%'
  call mis_message(seve%r,rname,mess)
  call blankline()
  !
end subroutine mis_set_sequence
!
subroutine mis_set_sequence_print(mapset,kind,numb,hang,error)
  use gbl_message
  use phys_const
  use maptools_setup_types
  use mapmis_interfaces, except_this=>mis_set_sequence_print
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(map_setup_t), intent(in)    :: mapset
  character(len=*),  intent(in)    :: kind
  integer(kind=4),   intent(in)    :: numb
  real(kind=8),      intent(in)    :: hang
  logical,           intent(inout) :: error
  !
  integer(kind=4) :: hour,min
  real(kind=8) :: time,sec
  character(len=1) :: sign
  character(len=message_length) :: mess
  character(len=*), parameter :: rname='MIS>SET>SEQUENCE>PRINT'
  !
  if ((numb.le.2).or.(mapset%debug%cover.eq.seve%i)) then
     time = abs(hang*hang_per_rad)
     hour = floor(time)
     time = (time-hour)*60d0
     min = floor(time)
     sec = (time-min)*60d0
     if (hang.lt.0d0) then
        sign = '-'
     else
        sign = '+'
     endif
     write(mess,'(a,a,i0,a,a1,i0,a,i0,a,f5.2,a)') kind,' #',numb,' at ',sign,hour,'h',min,'m',sec,'s'
     call mis_message(seve%r,rname,mess)
  else if (numb.eq.3) then
     call mis_message(seve%r,rname,'   ...')
  endif
end subroutine mis_set_sequence_print
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mis_fill_uv_fullysampled(mapset,cov,uvout,error)
  use gbl_message
  use phys_const
  use maptools_setup_types
  use mapuv_types
  use maptools_interfaces
  use mapio_interfaces
  use mapmis_interfaces, except_this=>mis_fill_uv_fullysampled
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(map_setup_t), intent(in)    :: mapset
  type(cov_t),       intent(in)    :: cov
  type(uv_t),        intent(inout) :: uvout
  logical,           intent(inout) :: error
  !
  integer(kind=4) :: ndump,idump
  integer(kind=4) :: nbase,ibase
  integer(kind=4) :: nvisi,ivisi,nchan,ichan
  integer(kind=4) :: nant,iant,jant
  integer(kind=4) :: ixydir,iuvdir
  real(kind=4) :: date,time
  real(kind=8) :: xoff,yoff,uoff,voff
  character(len=message_length) :: mess
  type(uv_chan_t) :: chan
  type(uv_setup_t) :: uvset
  character(len=*), parameter :: rname='MIS>FILL>UV>FULLYSAMPLED'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  ndump = cov%xy%row(code_lamb)%ndcov+cov%xy%row(code_beta)%ndcov
  nbase = cov%uv%row(code_lamb)%ndcov+cov%uv%row(code_beta)%ndcov
  nvisi = ndump*nbase
  nchan = cov%rec%nchan
  nant = ceiling(sqrt(real(nbase)))
  !
  ! Allocate the header
  call mapio_uv_setup_filename(uvset,"coucou",error)
  if (error)  return
  call mapio_uv_setup_order(uvset,code_tuv,error)
  if (error)  return
  call mapio_uv_setup_nchan(uvset,nchan,error)
  if (error)  return
  call mapio_uv_setup_nvisi(uvset,nvisi,error)
  if (error)  return
  call mapio_uv_setup_add_daps(uvset,code_uvt_loff,error)
  if (error)  return
  call mapio_uv_setup_add_daps(uvset,code_uvt_moff,error)
  if (error)  return
  call mapio_uv_setup_add_daps(uvset,code_uvt_xoff,error)
  if (error)  return
  call mapio_uv_setup_add_daps(uvset,code_uvt_yoff,error)
  if (error)  return
  call mapio_create_uv(mapset,uvset,uvout,error)
  if (error) return
  !
  ivisi = 0
  time = 0.0 ! [ sec]
  date = 1.0 ! [days]
  do ixydir=1,2
     do idump=1,cov%xy%row(ixydir)%ndcov
        time = time+1.0
        if (time.eq.86401) then
           date = date + 1.0
           time = 0.0
        endif
        xoff = cov%xy%row(ixydir)%off%val(idump,1)
        yoff = cov%xy%row(ixydir)%off%val(idump,2)
        do iuvdir=1,2
           iant = 0
           jant = 1
           do ibase=1,cov%uv%row(iuvdir)%ndcov
              uoff = cov%uv%row(iuvdir)%off%val(ibase,1)
              voff = cov%uv%row(iuvdir)%off%val(ibase,2)
              ivisi = ivisi+1
              if (ivisi.gt.nvisi) then
                 call mis_message(seve%e,rname,'Too many visibilities')
                 error = .true.
                 return
              endif
              iant = iant+1
              if (iant.eq.nant+1) then
                 iant = 1
                 jant = jant+1
              endif
              uvout%daps%u(ivisi) = uoff
              uvout%daps%v(ivisi) = voff
              uvout%daps%l(ivisi) = 0.0
              uvout%daps%m(ivisi) = 0.0
              uvout%daps%x(ivisi) = xoff
              uvout%daps%y(ivisi) = yoff
              uvout%daps%scan(ivisi) = 0.0
              uvout%daps%date(ivisi) = date
              uvout%daps%time(ivisi) = time
              uvout%daps%anti(ivisi) = iant
              uvout%daps%antj(ivisi) = jant
           enddo ! ibase
        enddo ! iuvdir
     enddo ! idump
  enddo ! ixydir
  ! User feedback
  write(mess,'(a,i0,a,i0)') 'Number of visibilities: ',ivisi,' = ',nvisi
  call mis_message(seve%r,rname,mess)
  ! Data initialization
  call mapio_create_chan(uvout,chan,error)
  if (error) return
  chan%uv(:,1:2) = 0.0 ! Real and imaginary part
  chan%uv(:,3)   = 1.0 ! Weight
  do ichan=1,nchan
    call mapio_put_chan(mapset,uvout,ichan,chan,error)
    if (error) exit
  enddo
  call mapio_free_chan(chan,error)
  if (error) return
  ! Fill the header
  call mis_fill_uv_header(cov,ivisi,nchan,uvout%head,error)
  if (error) return
!!$  if (cov%setup%kind.eq.code_otf) then
!!$     uvout%head%type%wifi = .true.
!!$  else
!!$     uvout%head%type%wifi = .false.
!!$  endif
  !
end subroutine mis_fill_uv_fullysampled
!
subroutine mis_fill_uv_undersampled(mapset,cov,uvout,error)
  use gbl_message
  use phys_const
  use maptools_setup_types
  use mapuv_types
  use maptools_interfaces
  use mapio_interfaces
  use mapmis_interfaces, except_this=>mis_fill_uv_undersampled
  use mis_cover_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(map_setup_t), intent(in)    :: mapset
  type(cov_t),       intent(in)    :: cov
  type(uv_t),        intent(inout) :: uvout
  logical,           intent(inout) :: error
  !
  integer(kind=visi_length) :: ngood
  logical :: last
  integer(kind=4) :: ndump,idump
  integer(kind=4) :: nvisi,nchan,ichan
  integer(kind=4) :: nant,iant,jant,kant
  real(kind=4) :: xdist,ydist,zdist,uvmin2,uv2dump,date,time,eff
  real(kind=8) :: sindec,cosdec,az,el
  real(kind=8) :: sinhang,coshang,xoff,yoff
  type(real_xy_t) :: udump,vdump
  type(logi_1d_t) :: good
  type(uv_t) :: uvtot
  character(len=message_length) :: mess
  type(uv_chan_t) :: chan
  type(uv_setup_t) :: uvset
  character(len=*), parameter :: rname='MIS>FILL>UV>UNDERSAMPLED'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  nant = cov%conf%nant
  ndump = cov%seq%ndump
  nvisi = ndump*cov%conf%nbase
  nchan = cov%rec%nchan
  !
  ! Allocate the temporary header at the maximum possible size
  call mapio_uv_setup_buffering(uvset,code_buffer_none,error)  ! Create a header+DAPS without DATA support
  if (error)  return
  call mapio_uv_setup_order(uvset,code_tuv,error)
  if (error)  return
  call mapio_uv_setup_nchan(uvset,nchan,error)
  if (error)  return
  call mapio_uv_setup_nvisi(uvset,nvisi,error)
  if (error)  return
  call mapio_uv_setup_add_daps(uvset,code_uvt_loff,error)
  if (error)  return
  call mapio_uv_setup_add_daps(uvset,code_uvt_moff,error)
  if (error)  return
  call mapio_uv_setup_add_daps(uvset,code_uvt_xoff,error)
  if (error)  return
  call mapio_uv_setup_add_daps(uvset,code_uvt_yoff,error)
  if (error)  return
  call mapio_create_uv(mapset,uvset,uvtot,error)
  if (error) return
  ! Fill the header
  call mis_fill_uv_header(cov,nvisi,nchan,uvtot%head,error)
  if (error) return
  ! Fill the DAPS
  ngood = 0
  uvmin2 = cov%conf%diam**2
  sindec = sin(cov%proj%dec)
  cosdec = cos(cov%proj%dec)
  call maptools_allocate_logi_1d("shadowing",nant,good,error)
  if (error) return
  call maptools_allocate_real_xy("u",nant,nant,udump,error)
  if (error) return
  call maptools_allocate_real_xy("v",nant,nant,vdump,error)
  if (error) return
  do idump=1,ndump
     date = 1.0 ! [days] The day could change here
     time = cov%seq%hcur%val(idump)*hang_per_rad*3600.0 ! [sec]  There should be an additional offset to go to UT
     xoff = cov%seq%xoff%val(idump)
     yoff = cov%seq%yoff%val(idump)
     coshang = cos(cov%seq%hcur%val(idump))
     sinhang = sin(cov%seq%hcur%val(idump))
     ! az,el are just computed. Nothing done yet
     call mis_eq2az(cov%obs,coshang,sinhang,cosdec,sindec,az,el,error)
     ! Shadowing reset
     do iant=1,nant
        good%val(iant) = .true.
     enddo ! iant
     ! Shadowing check
     do iant=1,nant-1
        if (good%val(iant)) then
           do jant=iant+1,nant
              if (good%val(jant)) then
                 xdist = cov%conf%x%val(iant)-cov%conf%x%val(jant)
                 ydist = cov%conf%y%val(iant)-cov%conf%y%val(jant)
                 zdist = cov%conf%z%val(iant)-cov%conf%z%val(jant)
                 udump%val(iant,jant) = sinhang*xdist + coshang*ydist
                 vdump%val(iant,jant) = sindec * (-coshang*xdist+sinhang*ydist) + cosdec*zdist
                 uv2dump = udump%val(iant,jant)**2+vdump%val(iant,jant)**2
                 if (uv2dump.le.uvmin2) then
                    last = ((xdist*coshang-ydist*sinhang)*cosdec+zdist*sindec) .gt. 0.0
                    if (last) then
                       good%val(jant) = .false.
                       kant = jant
                    else
                       good%val(iant) = .false.
                       kant = iant
                    endif
                    write(mess,'(a,3i3,4f9.2)') 'Shadowed: ',iant,jant,kant,udump%val(iant,jant),vdump%val(iant,jant),uv2dump,uvmin2
                    call mis_message(mapset%debug%cover,rname,mess)
                 endif
              endif
           enddo ! jant
        endif
     enddo ! iant
     ! Fill the DAPS
     do iant=1,nant-1
        if (good%val(iant)) then
           do jant=iant+1,nant
              if (good%val(jant)) then
                 ngood = ngood+1
                 uvtot%daps%u(ngood) = udump%val(iant,jant)
                 uvtot%daps%v(ngood) = vdump%val(iant,jant)
                 uvtot%daps%l(ngood) = 0.0
                 uvtot%daps%m(ngood) = 0.0
                 uvtot%daps%x(ngood) = xoff
                 uvtot%daps%y(ngood) = yoff
                 uvtot%daps%scan(ngood) = 0.0
                 uvtot%daps%date(ngood) = date
                 uvtot%daps%time(ngood) = time
                 uvtot%daps%anti(ngood) = iant
                 uvtot%daps%antj(ngood) = jant
                 if (error) return
              endif
           enddo ! jant
        endif
     enddo ! iant
  enddo
  ! Set the useful number of visibilities
  call mapio_shrink_nvisi(uvtot,ngood,error)
  if (error) return
  call maptools_free_real_xy(vdump,error)
  if (error) return
  call maptools_free_real_xy(udump,error)
  if (error) return
  call maptools_free_logi_1d(good,error)
  if (error) return
  call blankline()
  ! User feedback
  call mis_message(seve%r,rname,'Number of visibilities')
  write(mess,'(a,i0)') '   * Total:    ',nvisi
  call mis_message(seve%r,rname,mess)
  write(mess,'(a,i0)') '   * Good:     ',ngood
  call mis_message(seve%r,rname,mess)
  write(mess,'(a,i0)') '   * Shadowed: ',nvisi-ngood
  call mis_message(seve%r,rname,mess)
  eff = 100.*float(ngood)/float(nvisi)
  write(mess,'(a,f7.2,a)') 'Shadowing efficiency ',eff,'%'
  call mis_message(seve%r,rname,mess)
  eff = eff*(cov%seq%eff/100.)
  write(mess,'(a,f7.2,a)') 'Total     efficiency ',eff,'%'
  call mis_message(seve%r,rname,mess)
  call blankline()
  !
  ! Allocation of output table at the right size
  call mapio_uv_setup_reset(uvset,error)
  if (error)  return
  call mapio_clone_uv(mapset,uvset,uvtot,uvout,error)
  if (error) return
  ! Transfer DAPS
  call mapio_copy_daps(uvtot,uvout,error)
  if (error) return
  ! Deallocation of intermediate table
  call mapio_free_uv(uvtot,error)
  if (error) return
  ! Data initialization
  call mapio_create_chan(uvout,chan,error)
  if (error) return
  chan%uv(:,1:2) = 0.0 ! Real and imaginary part
  chan%uv(:,3)   = 1.0 ! Weight
  do ichan=1,nchan
    call mapio_put_chan(mapset,uvout,ichan,chan,error)
    if (error) exit
  enddo
  call mapio_free_chan(chan,error)
  if (error) return
!!$  if (cov%setup%kind.eq.code_otf) then
!!$     uvout%head%type%wifi = .true.
!!$  else
!!$     uvout%head%type%wifi = .false.
!!$  endif
  !
end subroutine mis_fill_uv_undersampled
!
subroutine mis_fill_uv_header(cov,nvisi,nchan,hfile,error)
  use gbl_message
  use gbl_format
  use gbl_constant
  use phys_const
  use gildas_def
  use image_def
  use mapmis_interfaces, except_this=>mis_fill_uv_header
  use mis_cover_types
  !----------------------------------------------------------------------
  ! @ private
  ! Define the header of a transposed uv table
  !----------------------------------------------------------------------
  type(cov_t),     intent(in)    :: cov
  integer(kind=4), intent(in)    :: nvisi
  integer(kind=4), intent(in)    :: nchan
  type(gildas),    intent(inout) :: hfile
  logical,         intent(inout) :: error
  !
  real(kind=4) :: xoff,yoff
  character(len=*), parameter :: rname='MIS>FILL>UV>HEADER'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  hfile%char%unit = 'Jy'
  !
  hfile%char%syst = 'EQUATORIAL'
  hfile%char%name = cov%proj%name
  hfile%gil%ra = cov%proj%ra
  hfile%gil%dec = cov%proj%dec
  hfile%gil%epoc = 2000
  xoff=0.0
  yoff=0.0
  call equ_to_gal(hfile%gil%ra,hfile%gil%dec,xoff,yoff,hfile%gil%epoc, &
       hfile%gil%lii,hfile%gil%bii,xoff,yoff)
  !
  hfile%gil%ptyp = p_azimuthal
  hfile%gil%a0 = cov%proj%ra
  hfile%gil%d0 = cov%proj%dec
  hfile%gil%pang = 0
  hfile%gil%xaxi = 0
  hfile%gil%yaxi = 0
  !
  hfile%char%line = cov%rec%line
  hfile%gil%freq = cov%rec%freq
  hfile%gil%fima = 0.0
  hfile%gil%fres = 1.0
  hfile%gil%vres = -clight*hfile%gil%fres/hfile%gil%freq/1000d0
  hfile%gil%voff = 0.0
  hfile%gil%faxi = 2
  !
  hfile%gil%ref(1) = 0.0
  hfile%gil%val(1) = 0.0
  hfile%gil%inc(1) = 1.0
  hfile%gil%ref(2) = 0.5*(nchan+1)
  hfile%gil%val(2) = hfile%gil%freq
  hfile%gil%inc(2) = hfile%gil%fres
  !
  hfile%gil%bval = +1.23456e+34
  hfile%gil%eval = 0.0
  !
end subroutine mis_fill_uv_header
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mis_eq2az(obs,coshang,sinhang,cosdec,sindec,az,el,error)
  use gbl_message
  use mapmis_interfaces, except_this=>mis_eq2az
  use mis_cover_types
  !----------------------------------------------------------------------
  ! @ private
  ! From (hang,dec) to (az,el)
  !----------------------------------------------------------------------
  type(obs_t),  intent(in)    :: obs
  real(kind=8), intent(in)    :: coshang
  real(kind=8), intent(in)    :: sinhang
  real(kind=8), intent(in)    :: cosdec
  real(kind=8), intent(in)    :: sindec
  real(kind=8), intent(out)   :: az
  real(kind=8), intent(out)   :: el
  logical,      intent(inout) :: error
  !
  real(kind=8) :: avec(3),bvec(3)
  character(len=*), parameter :: rname='MIS>EQ2AZ'
  !
  call mis_message(seve%t,rname,'Welcome')
  !
  avec(1) = coshang*cosdec
  avec(2) = sinhang*cosdec
  avec(3) = sindec
  call mis_matrix_product(obs%mat,avec,bvec)
  call mis_vec2angle(bvec,az,el)
  !
end subroutine mis_eq2az
!
subroutine mis_matrix_product(mat,a,b)
  use mapmis_interfaces, except_this=>mis_matrix_product
  !---------------------------------------------------------------------
  ! @ private
  ! This routine computes b = mat#a
  !---------------------------------------------------------------------
  real(kind=8), intent(in)  :: mat(3,3)
  real(kind=8), intent(in)  :: a(3)
  real(kind=8), intent(out) :: b(3)
  !
  integer(kind=4) :: i,j
  !
  do i=1,3
     b(i) = 0.d0
     do j=1,3
        b(i) = b(i)+mat(i,j)*a(j)
     enddo
  enddo
end subroutine mis_matrix_product
!
subroutine mis_vec2angle(a,alon,alat)
  use phys_const
  use mapmis_interfaces, except_this=>mis_vec2angle
  !---------------------------------------------------------------------
  ! @ private
  ! This routine recovers the longitude and latitude angles from
  ! cosine direction vector a
  !---------------------------------------------------------------------
  real(kind=8), intent(in)  :: a(3) ! [---] Cosine direction vector
  real(kind=8), intent(out) :: alon ! [rad] Longitude
  real(kind=8), intent(out) :: alat ! [rad] Latitude
  !
  real(kind=8) :: aa
  !
  aa = a(1)*a(1)+a(2)*a(2)
  aa = dsqrt(aa)
  if (aa.ge.0.000001d0) then
     aa = a(3)/aa
     alat = datan(aa)
  else
     alat = pi/2d0
  endif
  if (a(2).ne.0.d0 .or. a(1).ne.0.d0) then
     alon=datan2(a(2),a(1))
  else
     alon = 0.d0
  endif
end subroutine mis_vec2angle
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
