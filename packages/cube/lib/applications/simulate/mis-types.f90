!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module mis_cover_types
  use gildas_def ! include definition of code_null
  use maptools_axis_types
  use maptools_array_types
  integer(kind=4), parameter :: code_uv = 1
  integer(kind=4), parameter :: code_xy = 2
  integer(kind=4), parameter :: code_single = 1
  integer(kind=4), parameter :: code_mosaic  = 2  
  integer(kind=4), parameter :: code_otf     = 3
  integer(kind=4), parameter :: code_lamb    = 1
  integer(kind=4), parameter :: code_beta    = 2
  integer(kind=4), parameter :: code_para    = 1
  integer(kind=4), parameter :: code_perp    = 2
  character(len=6), parameter :: kind_setup(0:3) = (/'NONE  ','SINGLE','MOSAIC','OTF   '/)
  character(len=6), parameter :: kind_otf(0:2)   = (/'NONE  ','LAMBDA','BETA  '/)
  type recev_t
     integer(kind=4) :: nchan       ! [---] Number of channels
  end type recev_t
  type cgauss_t
     real(kind=4) :: diam = 0.0    ! [  m] Antenna diameter
     real(kind=4) :: nyquist = 0.0 ! [rad] Associated Nyquist sampling
     real(kind=4) :: fwhm = 0.0    ! [rad] Full width at half maximum
     real(kind=4) :: sigma = 0.0   ! [rad] Half width at 1/e ! *** JP to be checked
  end type cgauss_t
  type proj_t
     character(len=12) :: name = '' ! [---] Source name
     real(kind=8) :: ra  = 0d0      ! [rad] Right Ascension
     real(kind=8) :: dec = 0d0      ! [rad] Declination
  end type proj_t
  !
  type row_t
     integer(kind=4) :: kind = code_null ! [-----] code_lambda or code_beta
     type(axis_1d_t) :: axes(2)          ! [-----] Axis description parallel & perpendicular to scanning direction
     integer(kind=4) :: nline = 0        ! [-----] Number of lines per coverage
     integer(kind=4) :: ndlin = 0        ! [-----] Number of dumps per line
     integer(kind=4) :: ndcov = 0        ! [-----] Number of dumps per coverage
     type(dble_xy_t) :: off              ! [rad|m] Offsets
  end type row_t
  type geom_t
     real(kind=8) :: offset(2) = 0d0 ! [pixel] Does the grid go through the center? Value: [0,0.5]
     real(kind=8) :: center(2) = 0d0 ! [rad|m] Center
     real(kind=8) :: size(2)   = 0d0 ! [rad|m] Size
     real(kind=8) :: inc(2)    = 0d0 ! [rad|m] Sampling
  end type geom_t
  type grid_t
     logical :: fullysampled = .false.   ! [---] Ideal or realistic sampling?
     integer(kind=4) :: kind = code_null ! [---] code_uv or code_xy
     real(kind=8) :: factor = 0d0        ! [---] conversion factor
     character(len=2) :: unit = ''       ! [---]
     type(geom_t) :: geom                ! [---] Geometry
     type(row_t) :: row(2)               ! [---] Scanning description (lambda & beta)
  end type grid_t
  !
  type minmax_t
     real(kind=8) :: min  = 0d0 ! [-] Minimum
     real(kind=8) :: max  = 0d0 ! [-] Maximum
  end type minmax_t
  type time_t
     real(kind=8) :: max  = 0d0 ! [s] Maximum periodicity
     real(kind=8) :: time = 0d0 ! [s] True duration
     real(kind=8) :: over = 0d0 ! [s] Overhead
     real(kind=8) :: tot  = 0d0 ! [s] Total (= time + over)
     real(kind=8) :: eff  = 0d0 ! [%] Efficiency (= 100 * time / tot)
  end type time_t
  type setup_t
     logical :: ideal = .true.
     integer(kind=4) :: kind = code_null
     type(minmax_t) :: integ
     type(time_t) :: dump   ! dump
     type(time_t) :: lamb   ! Lambda otf subscan
     type(time_t) :: beta   ! Beta otf subscan
     type(time_t) :: single ! Single pointing subscan
     type(time_t) :: mosaic ! Mosaic subscan
     type(time_t) :: cali   ! Calibration
  end type setup_t
  type seq_t
     type(dble_1d_t) :: hcur      ! [rad] Current hour angle
     type(dble_1d_t) :: xoff      ! [rad] Current associated x offset
     type(dble_1d_t) :: yoff      ! [rad] Current associated y offset
     integer(kind=4) :: ncali = 0 ! [---] Number of calibrations
     integer(kind=4) :: nscan = 0 ! [---] Number of scans
     integer(kind=4) :: nsubs = 0 ! [---] Number of subscans
!     integer(kind=4) :: nlamb = 0 ! [---] Number of lambda subscans
!     integer(kind=4) :: nbeta = 0 ! [---] Number of beta   subscans
!     integer(kind=4) :: nsing = 0 ! [---] Number of single subscans
     integer(kind=4) :: ndump = 0 ! [---] Number of dumps
     real(kind=4) :: ncov = 0.0   ! [---] Number of coverages
     real(kind=4) :: eff = 0.0    ! [%]
  end type seq_t
  !
!!$  type point_t
!!$     real(kind=8) :: x = 0.0 ! [rad]
!!$     real(kind=8) :: y = 0.0 ! [rad]
!!$     real(kind=8) :: u = 0.0 ! [  m]
!!$     real(kind=8) :: v = 0.0 ! [  m]
!!$  end type point_t
!!$  type noise_t
!!$     logical :: init = .false. ! [-------------] Do init?
!!$     real(kind=4) :: rms = 0.0 ! [Jy or Jy/Beam] ??? *** JP
!!$  end type noise_t
  type cov_t
     type(cgauss_t) :: prim
     type(proj_t)   :: proj
     type(grid_t)   :: xy
     type(grid_t)   :: uv
     type(minmax_t) :: hang
     type(seq_t)    :: seq
     type(setup_t)  :: setup
  end type cov_t
  ! User interface
  type siccov_t
     character(len=filename_length) :: config = '' ! Configuration file name
     character(len=12) :: line = ''    ! Line name
     character(len=12) :: source = ''  ! Source name
     character(len=16) :: ra  = ''     ! [  HH:MM:SS]
     character(len=16) :: dec = ''     ! [+DDD:MM:SS]
     logical :: ideal = .false.        ! Ideal or real time sequence?
     logical :: fullysampled = .false. ! Ideal or real uv coverage?     
     real(kind=4) :: usamp(2) = 0.0    ! [---] Relative sampling in uv plane
     real(kind=4) :: xsamp(2) = 0.0    ! [---] Relative sampling perp. and para. to scanning direction
     real(kind=8) :: usize(2) = 0d0    ! [  m]
     real(kind=8) :: xsize(2) = 0d0    ! [  "]
     real(kind=8) :: ucenter(2) = 0d0  ! [  m]
     real(kind=8) :: xcenter(2) = 0d0  ! [  "]
     real(kind=8) :: xoffset(2) = 0d0  ! [pix]
     real(kind=8) :: uoffset(2) = 0d0  ! [pix]
     real(kind=8) :: dt = 0d0          ! [sec] Integration time
     real(kind=8) :: lat = 0d0         ! [deg] Observatory latitude
     real(kind=8) :: hor = 0d0         ! [deg] Minimum elevation
     real(kind=8) :: hour(2) = 0d0     ! [hr ] Observed hour angle range
     real(kind=8) :: freq = 0d0        ! [GHz] Observing frequency
     integer(kind=4) :: nchan = 0      ! [---] Number of channels
  end type siccov_t
end module mis_cover_types
!
module mis_bright_types
  use gildas_def ! include definition of code_null
  use maptools_parameters
  use maptools_buffer_parameters
  !
  integer(kind=4), parameter :: msource = 4
  integer(kind=4), parameter :: source_length = 9
  character(len=source_length) :: known_source(msource)
  data known_source /'*','POINT','FLAT','FILE'/
  integer(kind=4), parameter :: code_source_default = 0
  integer(kind=4), parameter :: code_source_point   = 1
  integer(kind=4), parameter :: code_source_flat    = 2
  integer(kind=4), parameter :: code_source_file    = 3
  !
  type sou_t
     integer(kind=4) :: code = code_null
     real(kind=4) :: flux = 1.0 ! [Jy/Beam]
     real(kind=8) :: xy(2) = 0d0 ! [rad]
     real(kind=8), pointer :: xoff => null() ! [rad]
     real(kind=8), pointer :: yoff => null() ! [rad]
     integer(kind=4) :: ixy(2) = 0
     integer(kind=4), pointer :: ixoff => null()
     integer(kind=4), pointer :: iyoff => null()
  end type sou_t
  type buffer_t
     integer(kind=4) :: code = code_null
     logical :: uvkind = .true.
     logical :: xykind = .true.
  end type buffer_t
  type bright_t
     logical :: init = .false.
     logical :: beam = .false.
     logical :: clip = .false.
     logical :: conv = .false.
     logical :: shift = .false.
     type(buffer_t) :: buffer
     type(sou_t) :: sou
  end type bright_t
  ! User interface
  type sicbright_t
     logical :: init = .true.  ! [-] Initialize the buffer to zero or just add a new brightness?
     logical :: beam = .true.  ! [-] 
     logical :: clip = .true.  ! [-] 
     logical :: conv = .true.  ! [-] 
     logical :: shift = .true. ! [-] 
     character(len=2) :: match_axes = "*"                   ! [-------] *, xy, uv or ab
     real(kind=8) :: match_offset(2) = 0d0                  ! [  pixel]
     real(kind=4) :: source_intensity = 0.0                 ! [Jy/Beam] Source intensity
     real(kind=8) :: source_center(2) = 0d0                 ! [ arcsec] Source brightness
     character(len=12) :: source_kind = "*"                 ! [-------] Source kind
     character(len=buffer_kind_length) :: buffer_kind = "*" ! [-------] 
     logical :: buffer_uvkind = .true.                      ! [-------] 
     logical :: buffer_xykind = .true.                      ! [-------] 
  end type sicbright_t
end module mis_bright_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
