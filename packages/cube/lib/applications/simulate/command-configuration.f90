!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubesimulate_configuration
  use cubetools_parameters
  use cubetools_array_types
  use cubetools_structure
  use cubesyntax_keyval_types
  use cubesimulate_messaging
  !
  public :: configuration_comm_t,configuration_prog_t
  public :: configuration_comm,configuration_prog
  private
  !
  type :: configuration_comm_t
     type(option_t), pointer  :: comm
     type(keyval_strg_comm_t) :: file
   contains
     procedure, public  :: register => cubesimulate_configuration_comm_register
     procedure, private :: parse    => cubesimulate_configuration_comm_parse
     procedure, private :: main     => cubesimulate_configuration_comm_main
  end type configuration_comm_t
  type(configuration_comm_t), target :: configuration_comm
  !
  type configuration_user_t
     logical :: dolist = .false.
     type(keyval_strg_user_t) :: file
   contains
     procedure, private :: toprog => cubesimulate_configuration_user_toprog
  end type configuration_user_t
  !
  type configuration_prog_t
     character(len=file_l) :: file = 'gag_data:noema-12d.cfg'
     integer(kind=inte_k) :: nant  = 0 ! [-] Number of antenna
     integer(kind=inte_k) :: nbase = 0 ! [-] Number of baselines
     real(kind=real_k) :: size = 0.0   ! [m] Configuration size
     type(real_1d_t) :: x              ! [m] Antenna positions along X axis
     type(real_1d_t) :: y              ! [m] Antenna positions along Y axis
     type(real_1d_t) :: z              ! [m] Antenna positions along Z axis
   contains
     procedure, public  :: list   => cubesimulate_configuration_prog_list
     procedure, private :: header => cubesimulate_configuration_prog_header
  end type configuration_prog_t
  type(configuration_prog_t), target :: configuration_prog
  !
contains
  !
  subroutine cubesimulate_configuration_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(configuration_user_t) :: user
    character(len=*), parameter :: rname='CONFIGURATION>COMMAND'
    !
    call cubesimulate_message(simulateseve%trace,rname,'Welcome')
    !
    call configuration_comm%parse(line,user,error)
    if (error) return
    call configuration_comm%main(user,configuration_prog,error)
    if (error) continue
  end subroutine cubesimulate_configuration_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubesimulate_configuration_comm_register(comm,error)
    use cubetools_unit
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(configuration_comm_t), intent(inout) :: comm
    logical,                     intent(inout) :: error
    !
    character(len=*), parameter :: rname='CONFIGURATION>COMM>REGISTER'
    !
    call cubesimulate_message(simulateseve%trace,rname,'Welcome')
    !
    ! Syntax
    call cubetools_register_command(&
         'CONFIGURATION','',&
         'Setup the configuration',&
         strg_id,&
         cubesimulate_configuration_command,&
         comm%comm,&
         error)
    if (error) return
    call comm%file%register(&
         'FILENAME',&
         'Name of the file containing the configuration',&
         configuration_prog%file,&
         error)
    if (error) return
  end subroutine cubesimulate_configuration_comm_register
  !
  subroutine cubesimulate_configuration_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! CONFIGURATION
    !----------------------------------------------------------------------
    class(configuration_comm_t), intent(in)    :: comm
    character(len=*),            intent(in)    :: line
    type(configuration_user_t),  intent(out)   :: user
    logical,                     intent(inout) :: error
    !
    character(len=*), parameter :: rname='CONFIGURATION>COMM>PARSE'
    !
    call cubesimulate_message(simulateseve%trace,rname,'Welcome')
    !
    if (cubetools_nopt().eq.0) then
       user%dolist = .true.
    else
       user%dolist = .false.
       call comm%file%parse(line,user%file,error)
       if (error) return
    endif
  end subroutine cubesimulate_configuration_comm_parse
  !
  subroutine cubesimulate_configuration_comm_main(comm,user,prog,error) 
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(configuration_comm_t), intent(in)    :: comm
    type(configuration_user_t),  intent(inout) :: user
    type(configuration_prog_t),  intent(inout) :: prog
    logical,                     intent(inout) :: error
    !
    character(len=*), parameter :: rname='CONFIGURATION>COMM>MAIN'
    !
    call cubesimulate_message(simulateseve%trace,rname,'Welcome')
    !
    if (user%dolist) then
       call prog%header(comm,error)
       if (error) return
       call prog%list(comm,error)
       if (error) return
    else
       call user%toprog(comm,prog,error)
       if (error) return
       call prog%header(comm,error)
       if (error) return
    endif
  end subroutine cubesimulate_configuration_comm_main
  !
  !------------------------------------------------------------------------
  !
  subroutine cubesimulate_configuration_user_toprog(user,comm,prog,error)
    use gkernel_interfaces, only: sic_parsef
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(configuration_user_t), intent(in)    :: user
    type(configuration_comm_t),  intent(in)    :: comm    
    type(configuration_prog_t),  intent(out)   :: prog
    logical,                     intent(inout) :: error
    !
    character(len=file_l) :: filename
    character(len=*), parameter :: rname='CONFIGURATION>USER>TOPROG'
    !
    call cubesimulate_message(simulateseve%trace,rname,'Welcome')
    !
    call user%file%toprog(comm%file,prog%file,error)
    if (error) return
    ! Parse filename
    filename = prog%file
    call sic_parsef(filename,prog%file,' ','.cfg')    
  end subroutine cubesimulate_configuration_user_toprog
  !
  !------------------------------------------------------------------------
  !
  subroutine cubesimulate_configuration_prog_header(prog,comm,error)
    use phys_const
    use gkernel_interfaces, only: sic_getlun,sic_open,sic_frelun
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(configuration_prog_t), intent(inout) :: prog
    type(configuration_comm_t),  intent(in)    :: comm
    logical,                     intent(inout) :: error
    !
    logical :: ok
    integer(kind=inte_k) :: lun,ier,ip,iq
    integer(kind=indx_k) :: iant,nant
    real(kind=real_k) :: xcent,ycent,zcent,radius2
    character(len=message_length) :: mess,chain
    character(len=*), parameter :: rname='CONFIGURATION>PROG>HEADER'
    !
    call cubesimulate_message(simulateseve%trace,rname,'Welcome')
    !
    ! Open file
    ier = sic_getlun(lun)
    if (ier.ne.1) then
       !***JP: Is the message correct?
       call cubesimulate_message(seve%e,rname,'File already opened: '//trim(prog%file))
       error = .true.
       return
    endif
    call open_file(lun,prog%file,error)
    if (error) return
    ! Get number of antenna = Number of good lines
    nant = 0
    read(lun,'(a)',iostat=ier) chain ! The first line is either a comment or the configuration size
    ok = ier.eq.0
    do while (ok)
       read(lun,'(a)',iostat=ier) chain
       ok = ier.eq.0
       if (ok.and.(chain(1:1).ne.'!').and.(len_trim(chain).ne.0)) then
          nant = nant+1
       endif
    enddo
    close(unit=lun)
    if (nant.lt.1) then
       call cubesimulate_message(seve%e,rname,'Less than one antenna found in '//trim(prog%file)//'!')
       error = .true.
       return
    endif
    ! Allocate needed memory and initialize
    call prog%x%reallocate("config x position",nant,error)
    if (error) return
    call prog%y%reallocate("config y position",nant,error)
    if (error) return
    call prog%z%reallocate("config z position",nant,error)
    if (error) return
    call prog%x%set(0.0,error)
    if (error) return
    call prog%y%set(0.0,error)
    if (error) return
    call prog%z%set(0.0,error)
    if (error) return
    ! Read X,Y,Z coordinates from file
    call open_file(lun,prog%file,error)
    if (error) return
    iant = 0
    read(lun,'(a)',iostat=ier) chain ! The first line is either a comment or the configuration size
    ok = ier.eq.0
    do while (ok)
       read(lun,'(a)',iostat=ier) chain
       ok = ier.eq.0
       if (ok.and.(chain(1:1).ne.'!').and.(len_trim(chain).ne.0)) then
          iant = iant+1
          if (iant.gt.nant) then
             write(mess,'(a,i0,a,i0)') 'More lines to read (',iant,') than expected (',nant,')'
             call cubesimulate_message(seve%e,rname,mess)
             call cleanup(lun)
             error = .true.
             return
          else
             ! Parse line
             ip = 1
             do while(chain(ip:ip).eq.' ')
                ip = ip+1
             enddo
             iq = index(chain(ip:),' ')+ip
             read(chain(iq:),*,iostat=ier) prog%x%val(iant),prog%y%val(iant),prog%z%val(iant)
          endif
       endif
    enddo ! while (ok)
    call cleanup(lun)
    ! Compute number of baselines
    prog%nant = nant
    prog%nbase = prog%nant*(prog%nant-1)/2
    ! Compute center
    xcent = 0.
    ycent = 0.
    zcent = 0.
    do iant=1,prog%nant
       xcent = xcent+prog%x%val(iant)
       ycent = ycent+prog%y%val(iant)
       zcent = zcent+prog%z%val(iant)
    enddo ! iant
    xcent = xcent/prog%nant
    ycent = ycent/prog%nant
    zcent = zcent/prog%nant
    ! Compute radius and diameter
    radius2 = 0.0
    do iant=1,prog%nant
       radius2 = max(radius2, &
            (prog%x%val(iant)-xcent)**2 + &
            (prog%y%val(iant)-ycent)**2 + &
            (prog%z%val(iant)-zcent)**2)
    enddo ! iant
    prog%size = 2.0*sqrt(radius2)
    !
  contains
    !
    subroutine open_file(lun,filename,error)
      integer(kind=inte_k), intent(in)    :: lun
      character(len=*),     intent(in)    :: filename
      logical,              intent(inout) :: error
      !      
      ier = sic_open(lun,filename,'old',.true.)
      if (ier.ne.0) then
         call cubesimulate_message(seve%e,rname,'Cannot open configuration file: '//trim(filename))
         call cleanup(lun)
         error = .true.
         return
      endif
    end subroutine open_file
    !
    subroutine cleanup(lun)
      integer(kind=inte_k), intent(in)    :: lun
      !
      close(unit=lun)
      call sic_frelun(lun)
    end subroutine cleanup
  end subroutine cubesimulate_configuration_prog_header
  !
  subroutine cubesimulate_configuration_prog_list(prog,comm,error)
    use cubetools_unit
    use cubesyntax_keyvalunit_list_tool
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(configuration_prog_t), intent(inout) :: prog
    type(configuration_comm_t),  intent(in)    :: comm
    logical,                     intent(inout) :: error
    !
    type(keyval_strg_prog_t) :: keyval_strg
    character(len=*), parameter :: rname='CONFIGURATION>PROG>LIST'
    !
    call cubesimulate_message(simulateseve%trace,rname,'Welcome')
    !
    call cubesimulate_message(seve%r,rname,blankstr)
    call cubesimulate_message(seve%r,rname,'Configuration')
    !
    call keyval_strg%list(comm%file,prog%file,error)
    if (error) return
    call keyvalunit%list('Configuration radius',prog%size,code_unit_uv,error)
    if (error) return
    call keyvalunit%list('Number of antenna',prog%nant,error)
    if (error) return
    call keyvalunit%list('Number of baselines',prog%nbase,error)
    if (error) return
  end subroutine cubesimulate_configuration_prog_list
end module cubesimulate_configuration
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
