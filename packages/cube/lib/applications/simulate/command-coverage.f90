!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubesimulate_coverage
  use cube_types
  use cubetools_parameters
  use cubetools_structure
  use cubeadm_cubeid_types
  use cubeadm_cubeprod_types
  use cubesimulate_messaging
  use cubesimulate_configuration
  use cubesimulate_observatory
  use cubesimulate_tuning
  !
  public :: coverage
  private
  !
  type :: coverage_comm_t
     type(option_t),    pointer :: comm
     type(cube_prod_t), pointer :: cover
     type(tuning_comm_t),        pointer :: tuning => null()
     type(observatory_comm_t),   pointer :: observ => null()
     type(configuration_comm_t), pointer :: config => null()
   contains
     procedure, private :: init     => cubesimulate_coverage_comm_init
     procedure, public  :: register => cubesimulate_coverage_comm_register
     procedure, private :: parse    => cubesimulate_coverage_comm_parse
     procedure, private :: main     => cubesimulate_coverage_comm_main
  end type coverage_comm_t
  type(coverage_comm_t) :: coverage
  !
  type coverage_user_t
     type(cubeid_user_t) :: cubeids
   contains
     procedure, private :: toprog => cubesimulate_coverage_user_toprog
  end type coverage_user_t
  !
  type coverage_prog_t
     type(tuning_prog_t),        pointer :: tuning => null()
     type(observatory_prog_t),   pointer :: observ => null()
     type(configuration_prog_t), pointer :: config => null()
     type(cube_t),               pointer :: cover
   contains
     procedure, private :: init   => cubesimulate_coverage_prog_init
     procedure, private :: header => cubesimulate_coverage_prog_header
     procedure, private :: data   => cubesimulate_coverage_prog_data
     procedure, private :: loop   => cubesimulate_coverage_prog_loop
     procedure, private :: act    => cubesimulate_coverage_prog_act
  end type coverage_prog_t
  !
contains
  !
  subroutine cubesimulate_coverage_command(line,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    character(len=*), intent(in)    :: line
    logical,          intent(inout) :: error
    !
    type(coverage_user_t) :: user
    character(len=*), parameter :: rname='COVERAGE>COMMAND'
    !
    call cubesimulate_message(simulateseve%trace,rname,'Welcome')
    !
    call coverage%parse(line,user,error)
    if (error) return
    call coverage%main(user,error)
    if (error) continue
  end subroutine cubesimulate_coverage_command
  !
  !----------------------------------------------------------------------
  !
  subroutine cubesimulate_coverage_comm_init(comm,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(coverage_comm_t), intent(inout) :: comm
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='COVERAGE>COMM>INIT'
    !
    call cubesimulate_message(simulateseve%trace,rname,'Welcome')
    !
    comm%tuning => tuning_comm
    comm%observ => observatory_comm
    comm%config => configuration_comm
  end subroutine cubesimulate_coverage_comm_init
  !
  subroutine cubesimulate_coverage_comm_register(comm,error)
    use cubedag_allflags
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(coverage_comm_t), intent(inout) :: comm
    logical,                intent(inout) :: error
    !
    type(cube_prod_t) :: oucube
    character(len=*), parameter :: rname='COVERAGE>COMM>REGISTER'
    !
    call cubesimulate_message(simulateseve%trace,rname,'Welcome')
    !
    ! Initialize pointers
    call comm%init(error)
    if (error) return
    !
    ! Syntax
    call cubetools_register_command(&
         'COVERAGE','',&
         'Simulate the coverage of an observation',&
         strg_id,&
         cubesimulate_coverage_command,&
         comm%comm,&
         error)
    if (error) return
    !
    ! Products
    call oucube%register(&
         'COVERAGE',&
         'Observation coverage',&
         strg_id,&
         [flag_coverage],&
         comm%cover,&
         error)
    if (error)  return
  end subroutine cubesimulate_coverage_comm_register
  !
  subroutine cubesimulate_coverage_comm_parse(comm,line,user,error)
    !----------------------------------------------------------------------
    ! COVERAGE
    !----------------------------------------------------------------------
    class(coverage_comm_t), intent(in)    :: comm
    character(len=*),       intent(in)    :: line
    type(coverage_user_t),  intent(out)   :: user
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='COVERAGE>COMM>PARSE'
    !
    call cubesimulate_message(simulateseve%trace,rname,'Welcome')
    !
    ! Nothing to do right now.
  end subroutine cubesimulate_coverage_comm_parse
  !
  subroutine cubesimulate_coverage_comm_main(comm,user,error) 
    use cubeadm_timing
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(coverage_comm_t), intent(in)    :: comm
    type(coverage_user_t),  intent(inout) :: user
    logical,                intent(inout) :: error
    !
    type(coverage_prog_t) :: prog
    character(len=*), parameter :: rname='COVERAGE>COMM>MAIN'
    !
    call cubesimulate_message(simulateseve%trace,rname,'Welcome')
    !
    call user%toprog(comm,prog,error)
    if (error) return
!!$    call prog%header(comm,error)
!!$    if (error) return
!!$    call cubeadm_timing_prepro2process()
!!$    call prog%data(error)
!!$    if (error) return
!!$    call cubeadm_timing_process2postpro()
  end subroutine cubesimulate_coverage_comm_main
  !
  !------------------------------------------------------------------------
  !
  subroutine cubesimulate_coverage_user_toprog(user,comm,prog,error)
    use cubeadm_get
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(coverage_user_t), intent(in)    :: user
    type(coverage_comm_t),  intent(in)    :: comm    
    type(coverage_prog_t),  intent(out)   :: prog
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='COVERAGE>USER>TOPROG'
    !
    call cubesimulate_message(simulateseve%trace,rname,'Welcome')
    !
    ! Initialize pointers
    call prog%init(error)
    if (error) return
    !
    ! User feedback
    call prog%tuning%list(comm%tuning,error)
    if (error) return
    call prog%observ%list(comm%observ,error)
    if (error) return
    call prog%config%list(comm%config,error)
    if (error) return
  end subroutine cubesimulate_coverage_user_toprog
  !
  !------------------------------------------------------------------------
  !
  subroutine cubesimulate_coverage_prog_init(prog,error)
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(coverage_prog_t), intent(inout) :: prog
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='COVERAGE>PROG>INIT'
    !
    call cubesimulate_message(simulateseve%trace,rname,'Welcome')
    !
    prog%tuning => tuning_prog
    prog%observ => observatory_prog
    prog%config => configuration_prog
  end subroutine cubesimulate_coverage_prog_init
  !
  subroutine cubesimulate_coverage_prog_header(prog,comm,error)
    use cubedag_allflags
    use cubeadm_clone
    use cubedag_node
    !----------------------------------------------------------------------
    ! Create a cube header from scratch and initialize it
    !----------------------------------------------------------------------
    class(coverage_prog_t), intent(inout) :: prog
    type(coverage_comm_t),  intent(in)    :: comm
    logical,                intent(inout) :: error
    !
    character(len=*), parameter :: rname='COVERAGE>PROG>HEADER'
    !
    integer(kind=ndim_k) :: ndim
    integer(kind=data_k) :: dims(maxdim)
    !
    call cubesimulate_message(simulateseve%trace,rname,'Welcome')
    !
    ndim = 3
    dims(1:3) = [7,1,1]
    dims(4:maxdim) = 0
    call cubeadm_create_header(&
         [flag_uv,flag_coverage],code_access_speset,ndim,dims,prog%cover,error)
    if (error) return
    !***JP: Why the family name is not an input of the
    !***JP: cubeadm_create_header subroutine? This would avoid to use cubedag_node here!
    call cubedag_node_set_family(prog%cover,'coucou',error)
    if (error) return
    ! Prepare extrema processing. Assume no parallel put involved (1 pseudo-task)
!    call cover%proc%allocate_extrema(buffer%cube%head,1,error)
!    if (error)  return
  end subroutine cubesimulate_coverage_prog_header
  !
  subroutine cubesimulate_coverage_prog_data(prog,error)
    use cubeadm_opened
    !----------------------------------------------------------------------
    ! 
    !----------------------------------------------------------------------
    class(coverage_prog_t), intent(inout) :: prog
    logical,                intent(inout) :: error
    !
    type(cubeadm_iterator_t) :: iter
    character(len=*), parameter :: rname='COVERAGE>PROG>DATA'
    !
    call cubesimulate_message(simulateseve%trace,rname,'Welcome')
    !
    call cubeadm_datainit_all(iter,error)
    if (error) return
    !$OMP PARALLEL DEFAULT(none) SHARED(prog,error) FIRSTPRIVATE(iter)
    !$OMP SINGLE
    do while (cubeadm_dataiterate_all(iter,error))
       if (error) exit
       !$OMP TASK SHARED(prog,error) FIRSTPRIVATE(iter)
       if (.not.error) &
         call prog%loop(iter,error)
       !$OMP END TASK
    enddo
    !$OMP END SINGLE
    !$OMP END PARALLEL
  end subroutine cubesimulate_coverage_prog_data
  !   
  subroutine cubesimulate_coverage_prog_loop(prog,iter,error)
    use cubeadm_taskloop
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(coverage_prog_t),   intent(inout) :: prog
    type(cubeadm_iterator_t), intent(inout) :: iter
    logical,                  intent(inout) :: error
    !
    type(spectrum_t) :: cover
    character(len=*), parameter :: rname='COVERAGE>PROG>LOOP'
    !
    call cover%allocate('cover',prog%cover,iter,error)
    if (error) return
    !
    do while (iter%iterate_entry(error))
      call prog%act(iter%ie,cover,error)
      if (error) return
    enddo ! ie
  end subroutine cubesimulate_coverage_prog_loop
  !   
  subroutine cubesimulate_coverage_prog_act(prog,ie,cover,error)
    use cubeadm_spectrum_types
    !----------------------------------------------------------------------
    !
    !----------------------------------------------------------------------
    class(coverage_prog_t), intent(inout) :: prog
    integer(kind=entr_k),   intent(in)    :: ie
    type(spectrum_t),       intent(inout) :: cover
    logical,                intent(inout) :: error
    !
    integer(kind=chan_k) :: ic
    character(len=*), parameter :: rname='COVERAGE>PROG>ACT'
    !
    do ic=1,cover%n
       cover%y%val(ic) = 1.0
    enddo ! ic
    call cover%put(ie,error)
    if (error) return
  end subroutine cubesimulate_coverage_prog_act
end module cubesimulate_coverage
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
