!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage CUBESIMULATE messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module cubesimulate_messaging
  use gpack_def
  use gbl_message
  use cubetools_parameters
  !
  public :: simulateseve
  public :: cubesimulate_message_set_id,cubesimulate_message,cubesyntax_message
  public :: cubesimulate_message_set_alloc,cubesimulate_message_get_alloc
  public :: cubesimulate_message_set_trace,cubesimulate_message_get_trace
  public :: cubesimulate_message_set_others,cubesimulate_message_get_others
  private
  !
  ! Identifier used for message identification
  integer(kind=4) :: cubesimulate_message_id = gpack_global_id  ! Default value for startup message
  !
  type :: cubesimulate_messaging_debug_t
     integer(kind=code_k) :: alloc = seve%d
     integer(kind=code_k) :: trace = seve%t
     integer(kind=code_k) :: others = seve%d
  end type cubesimulate_messaging_debug_t
  !
  type(cubesimulate_messaging_debug_t) :: simulateseve
  !
contains
  !
  subroutine cubesimulate_message_set_id(id)
    !---------------------------------------------------------------------
    ! Alter library id into input id. Should be called by the library
    ! which wants to share its id with the current one.
    !---------------------------------------------------------------------
    integer(kind=4), intent(in) :: id
    !
    character(len=message_length) :: mess
    character(len=*), parameter :: rname='MESSAGE>SET>ID'
    !
    cubesimulate_message_id = id
    write (mess,'(A,I0)') 'Now use id #',cubesimulate_message_id
    call cubesimulate_message(seve%d,rname,mess)
  end subroutine cubesimulate_message_set_id
  !
  subroutine cubesimulate_message(mkind,procname,message)
    use cubetools_cmessaging
    !---------------------------------------------------------------------
    ! Messaging facility for the current library. Calls the low-level
    ! (internal) messaging routine with its own identifier.
    !---------------------------------------------------------------------
    integer(kind=4),  intent(in) :: mkind     ! Message kind
    character(len=*), intent(in) :: procname  ! Name of calling procedure
    character(len=*), intent(in) :: message   ! Message string
    !
    call cubetools_cmessage(cubesimulate_message_id,mkind,'IMA>'//procname,message)
  end subroutine cubesimulate_message
  !
  subroutine cubesyntax_message(mkind,procname,message)
    use cubetools_cmessaging
    !---------------------------------------------------------------------
    !***JP: Provisoire!
    ! Messaging facility for the current library. Calls the low-level
    ! (internal) messaging routine with its own identifier.
    !---------------------------------------------------------------------
    integer(kind=4),  intent(in) :: mkind     ! Message kind
    character(len=*), intent(in) :: procname  ! Name of calling procedure
    character(len=*), intent(in) :: message   ! Message string
    !
    call cubetools_cmessage(cubesimulate_message_id,mkind,'IMA>'//procname,message)
  end subroutine cubesyntax_message
  !
  subroutine cubesimulate_message_set_alloc(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       simulateseve%alloc = seve%i
    else
       simulateseve%alloc = seve%d
    endif
  end subroutine cubesimulate_message_set_alloc
  !
  subroutine cubesimulate_message_set_trace(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       simulateseve%trace = seve%i
    else
       simulateseve%trace = seve%t
    endif
  end subroutine cubesimulate_message_set_trace
  !
  subroutine cubesimulate_message_set_others(on)
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical, intent(in) :: on
    !
    if (on) then
       simulateseve%others = seve%i
    else
       simulateseve%others = seve%d
    endif
  end subroutine cubesimulate_message_set_others
  !
  function cubesimulate_message_get_alloc()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubesimulate_message_get_alloc
    !
    cubesimulate_message_get_alloc = simulateseve%alloc.eq.seve%i
    !
  end function cubesimulate_message_get_alloc
  !
  function cubesimulate_message_get_trace()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubesimulate_message_get_trace
    !
    cubesimulate_message_get_trace = simulateseve%trace.eq.seve%i
    !
  end function cubesimulate_message_get_trace
  !
  function cubesimulate_message_get_others()
    !---------------------------------------------------------------------
    !
    !---------------------------------------------------------------------
    logical :: cubesimulate_message_get_others
    !
    cubesimulate_message_get_others = simulateseve%others.eq.seve%i
    !
  end function cubesimulate_message_get_others
end module cubesimulate_messaging
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
