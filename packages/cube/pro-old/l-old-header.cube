!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! @ l-old-header hgdfheadvar
!
! Draw a header associated to plots of LMV cube
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Do we skip header?
if .not.(do%header) then
   return
endif
!
! Error in this procedure will not affect others => Shallower "on error" rule
on error return
!
set\panel 1 /def /position header
!
greg1\box n n n
!
! Define text variables
define character text1*40
define real xtext ytext stext
! X, Y position and step vary with page format
if (cubeset%panel%true%frame%landscape) then
   let xtext  0.5
   let ytext -1
   let stext -1
else
   let xtext  0.75
   let ytext -0.7
   let stext -0.75
endif
!
! Filename in normal character size
greg1\set expand 1.0
let text1 '&1%name'
greg1\draw text xtext ytext text1 6 0 /box 7
!
! All other information will be a bit smaller
greg1\set expand 0.8
!
! Source name
let text1 "\\iSource:\\i "'&1%source'
let ytext ytext+stext
greg1\draw text xtext ytext text1 6 0 /box 7
!
! Line name
let text1 "\\iLine:\\i "'&1%line'
let ytext ytext+stext
greg1\draw text xtext ytext text1 6 0 /box 7
!
! Rest frequency
let text1 "\\iFrequency:\\i "'1d-6*nint(&1%restfre*1000)'" GHz"
let ytext ytext+stext
greg1\draw text xtext ytext text1 6 0 /box 7
!
! Beam size
if (&1%major.ne.0) then
   define real rmajor rminor factor
   let rmajor &1%major/sec
   let rminor &1%minor/sec
   ! Ensure 3 significant digits
   if (rminor.gt.10.0) then
     let factor 10
   else if (rminor.gt.1.0) then
     let factor 100
   else if (rminor.gt.0.1) then
     let factor 1000
   else
     let factor 10000
   endif
   define integer imajor iminor ipa
   let imajor nint(rmajor*factor)
   let iminor nint(rminor*factor)
   let ipa nint(&1%pa*180.0/pi)
   let text1 "\\iBeam:\\i "'imajor/factor'" x "'iminor/factor'" PA "'ipa'"^"
   delete /var rmajor rminor factor imajor iminor ipa
else
   let text1 "\\iBeam:\\i (no beam defined)"
endif
let ytext ytext+stext
greg1\draw text xtext ytext text1 6 0 /box 7
!
! Contour levels
if (do%contour) then
   define character sic_precision*8
   let sic_precision 'sic%precision'
   sic precision single
   if (spacing.lt.0) then
      ! User defined contour levels
      let text1 "\\iLevels:\\i ("'&1%unit'")"
      let ytext ytext+stext
      greg1\draw text xtext ytext text1 6 0 /box 7
      if (.not.exist(greg%levels)) then
         ! Levels have not yet been defined
         message w header "LEVEL command should be used before this plot"
      else
         ! Levels have already been defined => Make a limited, relevant list
         greg1\set expand 0.64
         ! Define maximum number of levels per line
         define integer jmax jlevel
         if (cubeset%panel%true%frame%landscape) then
            let jmax 5
         else
            let jmax 10
         endif
         ! Initialization
         let jlevel 1
         let ytext ytext+stext
         let text1 " "
         ! Loop over levels
         for ilevel 1 to greg%nlevel
            ! Print only relevant levels
            if (greg%levels[ilevel].gt.&1%min.and.greg%levels[ilevel].lt.&1%max) then
               ! Added ilevel level to string
               let text1 'text1'" "'greg%levels[ilevel]'
               ! Print string when needed
               if ((jlevel.eq.jmax).or.(ilevel.eq.greg%nlevel)) then
                  greg1\draw text xtext ytext text1 6 0 /box 7
                  let jlevel 1
                  let ytext ytext+0.70*stext
                  let text1 " "
               else
                  let jlevel jlevel+1
               endif
            endif
         next ilevel
         let ytext ytext-0.70*stext
         greg1\set expand 0.8
      endif
   else
      ! Evenly spaced contour levels
      !
      ! Contour spacing in image unit
      let text1 '&1%unit'
      define double range expo mant round_spacing sigma
      if (true%spacing.gt.0.1) then
         ! Strong source
         let range log10(true%spacing)
         let expo int(range+20.)-21
         let mant range-expo+1
         let round_spacing 0.1*nint(10^mant)*10^expo
         let text1 "\\iLevel step:\\i "'round_spacing'" "'text1'
      else if (true%spacing.gt.0) then
         ! Faint source
         let range log10(true%spacing*1e3)
         let expo int(range+20.)-21
         let mant range-expo+1
         let round_spacing 0.1*nint(10^mant)*10^expo
         let text1 "\\iLevel step:\\i "'round_spacing'" m"'text1'
      endif
      ! Exponential or linear spacing
      if (true%space.eq."exp") then
         let text1 'text1'" ("'true%space'")"
      endif
      let ytext ytext+stext
      greg1\draw text xtext ytext text1 6 0 /box 7
      !
      let text1 " "
      ! Associated conversion if needed
      if (&1%unit.eq."Jy".or.&1%unit.eq."Jy/beam") then
         if ((&1%major.ne.0).and.(&1%minor.ne.0)) then
            @ c-old-jansky.cube &1 "nofeedback"
            let text1 '0.01*nint(true%spacing*kperjy*100.)'" K"
         endif
      endif
      ! And associated number of sigma
      if (&1%noise.ne.0.and.true%spacing.ne.0) then
         let sigma true%spacing/&1%noise
         let range log10(sigma)
         let expo int(range+20.)-21
         let mant range-expo+1
         let round_spacing 0.1*nint(10^mant)*10^expo
         if (text1.eq." ") then
            let text1 "            --  "'round_spacing'" \gs"
         else
            let text1 "  "'text1'"   --  "'round_spacing'" \gs"
         endif
      endif
      !
      if (text1.ne." ") then
         let ytext ytext+stext
         greg1\draw text xtext ytext text1 6 0 /box 7
      endif
   endif
   sic precision 'sic_precision'
endif
!
! Box marking
if (true%mark.ne."none") then
   let text1 "\\iBox marking:\\i "'true%mark'
   let ytext ytext+stext
   greg1\draw text xtext ytext text1 6 0 /box 7
endif
!
! Planes
if (true%step[1].ne.0) then
   let text1 "\\iPlanes:\\i ["'true%first[1]'" to "'true%last[1]'" by "'true%step[1]'"]"
   let ytext ytext+stext
   greg1\draw text xtext ytext text1 6 0 /box 7
endif
if (true%step[2].ne.0) then
   let text1 "\\iPlanes:\\i ["'true%first[2]'" to "'true%last[2]'" by "'true%step[2]'"]"
   let ytext ytext+stext
   greg1\draw text xtext ytext text1 6 0 /box 7
endif
!
! User and date
sic\sic date
if (cubeset%panel%true%frame%landscape) then
   greg1\draw text 0.0 2.0 "'sys_info'" 5 0 /box 2
   greg1\draw text 0.0 1.0 "'sys_date'" 5 0 /box 2
else
   greg1\draw text -1.0 0.0 "'sys_info'" 5 90 /box 6
   greg1\draw text -2.0 0.0 "'sys_date'" 5 90 /box 6
endif
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
