! A MIRA procedure for improving the receiver alignment.
! 2006-08-04, H.W.
!
!
! Use: SOLVE [ifb] /ALIGNMENT
!
! Breaks at intermediate results can be achieved by setting
!
! let doPause yes
!
! in your MIRA session (doPause is a global logical MIRA/SIC variable).
!

define integer   isub i1 i2 k ndir_az nDriftAz nDriftEl nrecord nsubscan
define double    max tPeak fwhm[2]
define double    elvAzDrift alignAzDrift alignAzError elvElDrift alignElDrift -
                 alignElError
define logical   init
define character tmpFe*4

let ff 4.*log(2.d0) /new double

var raw
var febe

define double aux1 /like data1%data%subscan
let aux1 data1%data%subscan
compute max max aux1
let nsubscan max
let aux1 data1%data%integnum
compute max max aux1
let nrecord max

define double dummy[nsubscan]

let init &1

let i1 &2
let i2 &3

if init then
   for i 1 to nsubscan
       if exist(pa'i') then
          del /var pa'i'
       endif
       define structure pa'i' /global
       define real pa'i'%x /like pcross%sub'i'%dat%y /global
       define real pa'i'%y /like pcross%sub'i'%dat%y /global
       define real pa'i'%fwhm /global
       let tPeak pcross%sub'i'%fun%par1%value/pcross%sub'i'%fun%par3%value
       let tPeak tPeak/sqrt(pi/ff)
       let pa'i'%y pcross%sub'i'%dat%y/tPeak
       let pa'i'%x pcross%sub'i'%dat%x
       let pa'i'%fwhm pcross%sub'i'%fun%par3%value
   next
else
   for i 1 to nsubscan
       if exist(pb'i') then
          del /var pb'i'
       endif
       define structure pb'i' /global
       define real pb'i'%x /like pcross%sub'i'%dat%x /global
       define real pb'i'%y /like pcross%sub'i'%dat%y /global
       define real pb'i'%fwhm /global
       let tPeak pcross%sub'i'%fun%par1%value/pcross%sub'i'%fun%par3%value
       let tPeak tPeak/sqrt(pi/ff)
       let pb'i'%y pcross%sub'i'%dat%y/tPeak
       let pb'i'%x pcross%sub'i'%dat%x
       let pb'i'%fwhm pcross%sub'i'%fun%par3%value
   next
endif

if init then
   return
endif

let nDriftAz 0
let alignAzDrift 0.
for i 1 to nsubscan

   if raw'i'%subref1%segmentXStart.ne.0 then 

      define real try /like pcross%sub'i'%dat%y
      let try pa'i'%y-pb'i'%y
      mfit try=exp(-ff*((pa'i'%x-&A)/pa'i'%fwhm)^2)-exp(-ff*((pb'i'%x-&B)/pb'i'%fwhm)^2) /epsilon 1e-12 /start 0,0 /step 1,1 /quiet
      let nDriftAz nDriftAz+1
      let dummy[nDriftAz] mfit%par[1]-mfit%par[2]
      let alignAzDrift alignAzDrift+dummy[nDriftAz]
      
      clear
      limits /var pcross%sub'i'%dat%x try
      g\set plot land
      box
      g\draw relocate user_xmin '1.1*user_ymax' /user
      g\label "Azimuth subscan "'i' /center 6
      label "Az displacement [\r`]" /x
      label "Power difference A100-B100" /y
      histo pa'i'%x try 
      pen /col 1
      conn pa'i'%x mfit%fit
      pen /def
      if doPause then
         pause
      endif
      clear
      del /var try

   endif

next

let alignAzDrift alignAzDrift/nDriftAz
define double aux2[nDriftAz]
let aux2 (dummy[1:nDriftAz]-alignAzDrift)^2
compute alignAzError mean aux2
let alignAzError sqrt(alignAzError/(nDriftAz-1.))

let nDriftEl 0

let alignElDrift 0.
for i 1 to nsubscan

   if raw'i'%subref1%segmentYStart.ne.0 then 

      define real try /like pcross%sub'i'%dat%y
      let try pa'i'%y-pb'i'%y
      mfit try=exp(-ff*((pa'i'%x-&A)/pa'i'%fwhm)^2)-exp(-ff*((pb'i'%x-&B)/pb'i'%fwhm)^2) /epsilon 1e-12 /start 0,0 /step 1,1 /quiet
      let nDriftEl nDriftEl+1
      let dummy[nDriftEl] mfit%par[1]-mfit%par[2]
      let alignElDrift alignElDrift+dummy[nDriftEl]

      clear
      limits /var pcross%sub'i'%dat%x try
      g\set plot land
      box
      g\draw relocate user_xmin '1.1*user_ymax' /user
      g\label "Elevation subscan "'i' /center 6
      label "El displacement [\r`]" /x
      label "Power difference A100-B100" /y
      histo pa'i'%x try 
      pen /co 1
      conn pa'i'%x mfit%fit
      pen /def
      if doPause then
         pause
      endif
      clear
      del /var try

   endif

next


let alignElDrift alignElDrift/nDriftEl
define double aux3[nDriftEl]
let aux3 (dummy[1:nDriftEl]-alignElDrift)^2
compute alignElError mean aux3
let alignElError sqrt(alignElError/(nDriftEl-1.))

let k 0
for i 1 to nrecord
    let isub data'i1'%data%subscan[i]
    if raw'isub'%subref1%segmentXStart.ne.0 then 
       let k k+1
       let aux1[k] data'i1'%data%elevatio[i]
    endif
next
compute elvAzDrift median aux1[1:k]

let k 0
for i 1 to nrecord
    let isub data'i1'%data%subscan[i]
    if raw'isub'%subref1%segmentYStart.ne.0 then 
       let k k+1
       let aux1[k] data'i1'%data%elevatio[i]
    endif
next
compute elvElDrift median aux1[1:k]

sic output .tmp

say 'scan%header%scannum' 'elvAzDrift' 'alignAzDrift' 'alignAzError' -
                          'elvElDrift' 'alignElDrift' 'alignElError'

sic output
sic append .tmp alignment.dat
!
g\set marker 4 3 .2
clear
!
g\set box landscape 
let bxmin box_xmin /new real
let bxmax box_xmax /new real
let bymin box_ymin /new real
let bymax box_ymax /new real
!
g\set expand 1.4
!
col x 2 /file alignment.dat
define real elv xtmp ytmp zxtmp zytmp zx zy /like x
!
clear
col x 2 /file alignment.dat
elv = x
col x 5
elv = (elv+x)/2.
elv = elv*pi/180.
!
col x 3 y 6 z 4 /file alignment.dat
zx = z
col z 7
zy = z
!
xtmp = x*cos(elv)-y*sin(elv)
ytmp = x*sin(elv)+y*cos(elv)
zxtmp = sqrt((zx*cos(elv))^2+(zy*sin(elv))^2)
zytmp = sqrt((zx*sin(elv))^2+(zy*cos(elv))^2)
!
let x xtmp
let y ytmp
!
g\set box land
!
let tmpFe 'febe1%header%febe'
!
if tmpFe.eq."HERA" then
   limits -4.5 4.5 -4.5 4.5 /blanking -1000. 0.
else
   limits -1.5 1.5 -1.5 1.5 /blanking -1000. 0.
endif
g\set box match
g\box
g\label "horizontal Nasmyth misalignment [\r`]" /x
g\draw relocate -3 0 /box 4
g\label "vertical Nasmyth misalignment [\r`]" 90 /center 5
let z zxtmp
points x y /blanking -1000. 0.
errorbar x
let z zytmp
points x y /blanking -1000. 0.
errorbar y
!
pen /col 2
points x[nxy] y[nxy] /blanking -1000. 0.
if nxy.gt.1 then
   pen /col 1
   points x['nxy-1'] y['nxy-1'] /blanking -1000. 0.
endif
pen /def
g\dra reloc 0 1 /box 8
g\label "green: last measured point, red: 2nd last point" /center 5

