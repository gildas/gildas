on error return
g\set plot land
g\set expand 1.0
clear 
!
if .not.exist(scan) then
    var scan
endif
if .not.exist(mon) then
   var mon
endif
if .not.exist(data) then
   var data
endif
if .not.exist(febe) then
   var febe
endif
if .not.exist(derot) then
   var derot
endif
!
let npts 1000 /new integer
!
define integer   intUtc iEnd iStart minutes
define real      heraMark heraMax heraMin utcMark
define double    heraAngle[npts] lst[npts] utc[npts]
define double    beta chi dlst dt frameAngle ha lambda lstStart pa phi utcStart
define character minutesString*2 obsDate*5
define logical   forbidden[npts] doMark
!
let frameAngle &1
if frameAngle.eq.-1000 then
   let frameAngle febe1%header%dewang
endif
let frameAngle nint(100*frameAngle)/100.
g\draw relocate -10 -1.5  /box 9
label 'scan%header%object' /center 6
g\draw relocate -10 -2.5  /box 9
label "HERA position angle = "'frameAngle' /center 6
g\draw relocate -10 -3.5  /box 9
label "Red derotator trajectory forbidden." /center 6
!
let dt 24/(npts-1)
let dlst 2*pi*(1.-1./365)/(npts-1)
let lstStart (data1%data%lst[1]/86400.)*2*pi
let phi scan%header%sitelat*PI/180.
let utcstart 24*(mon1%data%encoder_az_el[1,3]-int(mon1%data%encoder_az_el[1,3]))
!
for i 1 to npts
    let lst[i] lstStart+(i-1)*dlst
    let utc[i]   utcStart+(i-1)*dt
    let lambda data1%data%baslong[1]*pi/180.
    let beta   data1%data%baslat[1]*pi/180.
    let ha lst[i]-lambda
    let pa 180.*atan2(sin(HA),(tan(phi)*cos(beta)-sin(beta)*cos(HA)))/pi
    let chi (-mon1%data%encoder_az_el[1,2]+pa)/2.
    let heraAngle[i] chi-frameAngle/2.
    let lst[i] lst[i]*12./pi
next
compute heraMin min heraAngle
compute heraMax max heraAngle
let heraMin heraMin-30
let heraMax heraMax+30
g\tickspace 1 3 10 30 
limits * * heraMin heraMax /var lst heraAngle
if mod(1+int(user_xmin),3).eq.0 then
   let iStart int(user_xmin)+1
else if mod(1+int(user_xmin),3).eq.1 then
   let iStart int(user_xmin)+3
else if mod(1+int(user_xmin),3).eq.2 then
   let iStart int(user_xmin)+2
endif
for i iStart to int(user_xmax) by 3
    draw relocate 'i' 'user_ymax+2' /user
    if i.lt.24 then
       label 'i' /center 8
    else
       label 'i-24' /center 8
    endif
next
g\set ticksize .3
axis xu /tick in 1 3 /label none
limits * * = = /var utc heraAngle
conn utc heraAngle
axis xl /tick in 1 3 /label none
axis yl /label o
axis yr /label none
!
if mod(1+int(user_xmin),3).eq.0 then
   let iStart int(user_xmin)+1
else if mod(1+int(user_xmin),3).eq.1 then
   let iStart int(user_xmin)+3
else if mod(1+int(user_xmin),3).eq.2 then
   let iStart int(user_xmin)+2
endif
for i iStart to int(user_xmax) by 3
    draw relocate 'i' 'user_ymin-3' /user
    if i.lt.24 then
       label 'i' /center 2
    else
       label 'i-24' /center 2
    endif
next
!
g\pen /col 3
g\set orient 45
g\set marker 4 1 .3
g\draw marker utcStart derot%actframe[1] /user
g\set orient 0
let obsDate 'int(utcStart)'
let minutes nint((utcStart-int(utcStart))*60)
let minutesString 'minutes' /format i2.2
let obsDate 'obsDate'":"'minutesString' 
g\draw relocate use_curs[1] 'use_curs[2]+7' /user
g\label 'obsDate' /center 5
g\pen /def
g\label "UTC [hours]" /x
g\draw relocate 0 1.2 /box 8
g\label "LST [hours]" /center 5
g\draw relocate -2 0 /box 4
g\set orient 90
g\label "HERA Nasmyth Angle [deg]" /center 5
g\set orient 0
!
let iStart 0
let iEnd   0
!
let forbidden no
let forbidden yes /where abs(heraAngle).ge.83
if forbidden[1] then
   let iStart 1
endif
for i 2 to npts
    g\set orient 45
    g\set marker 4 1 .3
    let doMark no
    if .not.forbidden['i-1'].and.forbidden[i] then
       let iStart i
       let utcMark utc[i]
       let heraMark heraAngle[i]
       let doMark yes
    else if forbidden['i-1'].and..not.forbidden[i] then
       let iEnd i
       let utcMark utc[i]
       let heraMark heraAngle[i]
       let doMark yes
    endif
    if doMark then
       let intUtc int(utcMark)-24*int(utcMark/24)
       let obsDate 'intUtc' /format i2.2
       let minutes nint((utcMark-int(utcMark))*60)
       if minutes.eq.60 then
          let minutes 0
          let obsDate '1+intUtc' /format i2.2
       endif
       let minutesString 'minutes' /format i2.2
       let obsDate 'obsDate'":"'minutesString' 
       g\draw relocate utcMark heraMark /user
       g\draw relocate use_curs[1] 'use_curs[2]+15' /user
       g\pen /col 3 
       g\label 'obsDate' /center 2
       g\draw relocate use_curs[1] 'use_curs[2]-7' /user
       g\draw arrow    utcMark heraMark /user
       g\pen /def
    endif
next
g\set orient .0
g\pen /da 3
if user_ymax.gt.83 then
   g\draw relocate user_xmin +83 /user
   g\draw line     user_xmax +83 /user
endif
if user_ymin.lt.-83 then
   g\draw relocate user_xmin -83 /user
   g\draw line     user_xmax -83 /user
endif
g\pen /def
if iStart.ge.1.and.iStart.le.npts.and.iEnd.ge.1.and.iEnd.le.npts then
   say 'iStart' 'iEnd'
   if iStart.le.iEnd then
      g\pen /col 1 /weight 3
      g\conn utc[iStart:iEnd] heraAngle[iStart:iEnd]
      g\pen /def
   else
      g\conn utc[iEnd:iStart] heraAngle[iEnd:iStart]
      g\pen /col 1 /weight 3
      g\conn utc[1:'iEnd-1'] heraAngle[1:'iEnd-1']
      g\conn utc['iStart+1':npts] heraAngle['iStart+1':npts]
      g\pen /def
   endif
endif
!
g\tickspace 0 0 0 0
