!
on error return
!
clear 
g\set tick 0.1
g\set plot landscape
!
define integer ibe ichan ifb ipix jpix i1 i2 
define integer nbasebands nchan npix nrecords
define real bxmax bxmin bymax bymin uymax
define double aux chi chi0
define logical doHera
!
let ibe = &1
let jpix = &2
!
if ibe.gt.scan%header%nfebe then
   ifb = ibe
   ibe = ibe-scan%header%nfebe
else
   ifb = ibe
endif
!

!
if jpix.ne.0 then
   npix = 1
else
   npix = febe'ibe'%header%febefeed
endif
!
@ plotHeader "SATURATION" 'ibe'
!
let doHera = scan%data%fe[ibe].eq.50.or.scan%data%fe[ibe].eq.60
!
if doHera.and.npix.gt.1 then
   g\set expand 0.6
else
   g\set expand 0.8
endif
!
dy = (mira_ymax-mira_ymin)/sqrt(npix)
dx = dy
!
nbasebands = febe'ibe'%header%febeband
!
define real channels_per_band[nbasebands]
!
define real raux /like data'ibe'%data%integnum
raux = data'ibe'%data%integnum
!
compute aux max raux 
nrecords = int(aux)
!
for i 1 to nbasebands 
    channels_per_band[i] = array'ibe'%header'i'%usedchan
next
!
@ computeOffsets 'ibe' 0 'doHera'
!
compute aux max channels_per_band
nchan = int(aux)
!
define real rx[nchan,nbasebands] ry[npix,nchan,nbasebands]
!
let ry blankingRaw
!
i2 = 0
for i 1 to nbasebands 
    nchan = int(channels_per_band[i])
    i1 = array'ibe'%header'i'%dropchan+1
    i2 = i1+nchan-1
    define integer indx[nchan]
    let indx[j] j
    let rx[i] = (indx-array'ibe'%header'i'%crpx4f_2)*array'ibe'%header'i'%cd4f_21+array'ibe'%header'i'%crvl4f_2
    if npix.eq.1 then
       let ry[1:npix,1:nchan,i] gains'ifb'%phot[i,jpix,i1:i2]/gains'ifb'%psky[i,jpix,i1:i2] /where gains'ifb'%phot[i,jpix,i1:i2].ne.blankingRaw.and.gains'ifb'%psky[i,jpix,i1:i2].ne.blankingRaw 
    else
       let ry[1,1:nchan,i] gains'ifb'%phot[i,1:npix,i1:i2]/gains'ifb'%psky[i,1:npix,i1:i2] /where gains'ifb'%phot[i,1:npix,i1:i2].ne.blankingRaw.and.gains'ifb'%psky[i,1:npix,i1:i2].ne.blankingRaw 
    endif
    del /var indx
next
rx = rx/1.e9
!
ipix = 0
!
nchan = int(channels_per_band[1])
for i 1 to nbasebands
    nchan = int(channels_per_band[i])
    for j 1 to npix
        if i.eq.1.and.j.eq.1 then
           limits /var rx[i] ry[j,1:nchan,i] /blanking blankingRaw 0.
        else
           limits < > < > /var rx[i] ry[j,1:nchan,i] /blanking blankingRaw 0.
        endif
    next
next
!
!
if trc[2]-blc[2].ne.0.and.trc[1]-blc[1].ne.0 then
   limits blc[1] trc[1] blc[2] trc[2] /blanking blankingRaw 0.
else
   uymax = user_ymax+0.15*(user_ymax-user_ymin)
   limits = = = uymax /blanking blankingRaw 0.
endif
let blc[1:2] 0. 0.
let trc[1:2] 0. 0.
!
!
for i 1 to sqrt(npix)
    bxmin = 1.2*mira_ymin+(i-1)*dx
    bxmax = bxmin+dx
    for j 1 to sqrt(npix)
        ipix = ipix+1
        if npix.gt.1 then
           jpix = ipix
        endif
	bymin = mira_ymin+(j-1)*dy
	bymax = bymin+dy
        g\set box bxmin bxmax bymin bymax
        draw relocate 0.2 -0.5 /box 7 
        if doHera then
           label 'jpix' /center 6
           draw relocate 0.5 -0.5 /box 7
        endif
        if exist(offset%off) then
           if (offset%off%x[jpix].ge.0) then
              label "Sky offset: ( " /center 6
           else
              label "Sky offset: (" /center 6
           endif
           label 'offset%off%x[jpix]' /center 6
           if (offset%off%y[jpix].ge.0) then
              label "\r`, " /center 6
           else
              label "\r`," /center 6
           endif
           label 'offset%off%y[jpix]' /center 6
           label "\r`)" /center 6
        endif
        if doHera then
           draw relocate 0.5 -1.0 /box 7
           if (offset%pix%x[jpix].ge.0) then
              label "Pixel offset: ( " /center 6
           else 
              label "Pixel offset: (" /center 6
           endif
           label 'offset%pix%x[jpix]' /center 6
           if (offset%pix%y[jpix].ge.0) then
              label "\r`, " /center 6
           else 
              label "\r`," /center 6
           endif
           label 'offset%pix%y[jpix]' /center 6
           label "\r`)" /center 6
        endif
        if i.eq.1.and.j.eq.1 then
           box p o in 
           label "Sky Frequency [GHz]" /x
           draw relocate -1.5 0 /box 4
           label "count ratio hot/sky" 90 /center 5 
        else
           box n n
        endif 
        @ plotBasebands 'ibe' F
        for l 1 to nbasebands
            nchan = int(channels_per_band[l])
            histogram rx[l] ry[ipix,1:nchan,l] /blanking blankingRaw 0.
        next
	pen /da 3 /col 1
	draw relocate user_xmin 1.05 /user
	draw line 'user_xmin+0.4*(user_xmax-user_xmin)' 1.05 /user
	label " saturation limit " /center 6
	draw line user_xmax 1.05 /user
	pen /def
    next
next
!

