!
on error return
!
clear 
g\set tick 0.1
g\set plot landscape
!
define integer channels_per_band
define integer ibe ichan ipix i0 i1 i2 jpix
define integer nbasebands nchan npix nrecords
define real bxmax bxmin bymax bymin dx dy max sum y0
define double aux chi chi0 cpa spa
!
define logical displayOn doHera
!
let displayOn = &1
let ibe = &2
let jpix = &3
!
!
if jpix.ne.0 then
   npix = 1
else
   npix = febe'ibe'%header%febefeed
endif
!
let doHera = scan%data%fe[ibe].eq.50.or.scan%data%fe[ibe].eq.60
!
dy = (mira_ymax-mira_ymin)/sqrt(npix)
dx = dy
!
@ plotHeader "MAP" 'ibe'
!
if doHera.and.npix.gt.1 then
   g\set expand 0.6
else
   g\set expand 0.8
endif
!
nbasebands = febe'ibe'%header%febeband
!
define real raux /like data'ibe'%data%integnum
raux = data'ibe'%data%integnum
!
compute aux max raux 
nrecords = int(aux)
!
compute aux max raux
nrecords = int(aux)
!
@ computeOffsets 'ibe' 0 'doHera'
!								  
nchan = data'ibe'%header%channels
!
define real rx1[nchan] rx2[nchan] ry[npix,nchan,nrecords]
define double fstep f0 vstep v0
!
fstep = data'ibe'%header%cd4f_21*1.e-9
f0 = data'ibe'%header%crvl4f_2*1.e-9
vstep = data'ibe'%header%cd4r_21
v0 = data'ibe'%header%crvl4r_2
!
i0 = data'ibe'%header%crpx4r_2
!
let rx1[i] (i-i0)*fstep+f0
let rx2[i] (i-i0)*vstep+v0
!
if npix.eq.1 then
   ry[1,1:nchan,1:nrecords] = data'ibe'%data%otfdata[jpix,1:nchan,1:nrecords]
else
   ry[1:npix,1:nchan,1:nrecords] = data'ibe'%data%otfdata[1:npix,1:nchan,1:nrecords]
endif
!
ipix = 0
!
if scan%header%scantype.eq."POINTING" then
   say "not yet done"
   return
endif
!
if reduce'ibe'%calibration_done[3].eq.0.and.-
reduce'ibe'%calibration_done[4].eq.0 then
   say "E-PLOT: Only for data with off subtracted and spectral basebands concatenated."
   say "Please execute MIRA\CAL [\GAINS [\TCAL]] \OFF \CONCATENATE"
   return
endif
!
define real rymin rymax
compute rymin min ry /blanking blankingRed 0
compute rymax max ry /blanking blankingRed 0 
!
greg2\wedge bottom /scaling lin 'rymin' 'rymax'
!
g\draw relocate 0 -1.5 /box 2 
if reduce'ibe'%calibration_done[2].eq.0 then
   label "backend counts" /center 5
else if febe'ibe'%data%beameff[1,1].ne.febe'ibe'%data%etafss[1,1] then
   label "T\dm\db [K]" /center 5
else
   label "T\dA\u* [K]" /center 5
endif
!
limits 1 nchan 1 nrecords
if data'ibe'%header%cd4r_21.lt.0 then
   limits = = = = /rev x
endif
!
if displayOn then
   i1 = blc[2]
   if array'ibe'%data1%iswitch[i1,1].eq."OFF" then
      for k i1+1 to nrecords
          i0 = k 
          if array'ibe'%data1%iswitch[k,1].eq."ON" then
             break
          endif
      next
      i1 = i0
   endif
   blc[2] = i1
   i2 = trc[2]
   if array'ibe'%data1%iswitch[i2,1].eq."OFF" then
      for k i2-1 to 1 by -1 
          i0 = k
          if array'ibe'%data1%iswitch[k,1].eq."ON" then
             break
          endif
      next
      i2 = i0
   endif
   trc[2] = i2
endif
!
if trc[2]-blc[2].ne.0.and.trc[1]-blc[1].ne.0 then
   limits blc[1] trc[1] blc[2] trc[2]
else
   limits 1 nchan 1 nrecords
endif
if data'ibe'%header%cd4r_21.lt.0 then
   limits = = = = /rev x
endif
!
for i 1 to sqrt(npix)
    bxmin = 1.2*mira_ymin+(i-1)*dx
    bxmax = bxmin+dx
    for j 1 to sqrt(npix) 
        ipix = ipix+1
        if npix.gt.1 then
           jpix = ipix
        endif
        bymin = mira_ymin+(j-1)*dy
	bymax = bymin+dy
        g\set box bxmin bxmax bymin bymax
        greg2\rgdata ry[ipix,1:nchan,1:nrecords] /blanking blankingRed 0. /var
        greg2\plot /scaling lin 'rymin' 'rymax' /blanking blankingRed 0.
        draw relocate 0.5  -0.5 /box 7 
        if doHera then
           label 'jpix' /center 6
           draw relocate 0.5 -0.5 /box 7
        endif
	if exist(offset%on).and.displayOn then
	   offset%on%x[jpix] = nint(3.6e4*data'ibe'%data%longoff[i1])/10.
           offset%on%x[jpix] = offset%on%x[jpix]+offset%pix%x[jpix]
	   offset%on%y[jpix] = nint(3.6e4*data'ibe'%data%latoff[i1])/10.
           offset%on%y[jpix] = offset%on%y[jpix]+offset%pix%y[jpix]
           if (offset%on%x[jpix].ge.0) then
	      label "ON ( " /center 6
	   else
	      label "ON (" /center 6
	   endif
	   label 'offset%on%x[jpix]' /center 6
	   if (offset%on%y[jpix].ge.0) then
	      label "\r`, " /center 6
	   else
	      label "\r`," /center 6
	   endif
	   label 'offset%on%y[jpix]' /center 6
	   label "\r`) to " /center 6
	   offset%on%x[jpix] = nint(3.6d4*data'ibe'%data%longoff[i2])/10.
	   offset%on%y[jpix] = nint(3.6d4*data'ibe'%data%latoff[i2])/10.
           if (offset%on%x[jpix].ge.0) then
	      label "( " /center 6
	   else
	      label "(" /center 6
	   endif
	   label 'offset%on%x[jpix]' /center 6
	   if (offset%on%y[jpix].ge.0) then
	      label "\r`, " /center 6
	   else
	      label "\r`," /center 6
	   endif
	   label 'offset%on%y[jpix]' /center 6
	   label "\r`)" /center 6
        endif
	if exist(offset%off) then
           draw relocate -5.5 -0.5 /box 9 
	   if (offset%off%x[jpix].ge.0) then
	      label " REF ( " /center 6
	   else
	      label " REF (" /center 6
	   endif
	   label 'offset%off%x[jpix]' /center 6
	   if (offset%off%y[jpix].ge.0) then
	      label "\r`, " /center 6
	   else
	      label "\r`," /center 6
           endif
           label 'offset%off%y[jpix]' /center 6
           label "\r`)" /center 6
        endif
        if doHera then
	   if exist(offset%off) then
              draw relocate 0.5 -1.0 /box 7
	   else
              draw relocate 0.5 -0.5 /box 7
	   endif
           if (offset%pix%x[jpix].ge.0) then
              label "Pixel offset: ( " /center 6
           else 
              label "Pixel offset: (" /center 6
           endif
           label 'offset%pix%x[jpix]' /center 6
           if (offset%pix%y[jpix].ge.0) then
              label "\r`, " /center 6
           else 
              label "\r`," /center 6
           endif
           label 'offset%pix%y[jpix]' /center 6
           label "\r`)" /center 6
	endif
        if i.eq.1.and.j.eq.1 then
!
! velocity axis
!

	   if trc[2]-blc[2].ne.0 then
	      i1 = blc[1]
	      i2 = trc[1]
              limits rx2[i1] rx2[i2] blc[2] trc[2] -
	             /var rx2 ry[1,1:nchan,1]
              if data'ibe'%header%cd4r_21.lt.0 then
                 limits = = = = /rev x
              endif
	   else
              limits rx2[1] rx2[nchan] 1 nrecords /var rx2 ry[1,1:nchan,1]
              if data'ibe'%header%cd4r_21.lt.0 then
                 limits = = = = /rev x
              endif
	   endif
           axis yl /label o /tick in
	   axis yr /label n /tick in
           if .not.doHera.or.npix.eq.1 then
	      axis xu /label p /tick in
	   endif
!
! frequency axis
!
	   if trc[2]-blc[2].ne.0 then
	      i1 = blc[1]
	      i2 = trc[1]
              limits rx1[i1] rx1[i2] blc[2] trc[2] -
	             /var rx1 ry[1,1:nchan,1]
	   else
              limits rx1[nchan] rx1[1] 1 nrecords /var rx1 ry[1,1:nchan,1]
	   endif
	   axis xl /label p /tick in
           @ plotBasebands 'ibe' F
!
! channel number axis
!
	   if trc[2]-blc[2].ne.0.and.trc[1]-blc[1].ne.0 then
              limits blc[1] trc[1] blc[2] trc[2]
           else
              limits 1 nchan 1 nrecords
	   endif
           if data'ibe'%header%cd4r_21.lt.0 then
              limits = = = = /rev x
           endif
	   draw relocate 0 -1 /box 2
	   label "sky frequency [GHz]" /center 5
	   if .not.doHera.or.npix.eq.1 then
	      dra relocate 0 1.2 /box 8
	      label "V\dl\ds\dr [km s\u-\u1]" /center 5
	   endif
           g\set orient 90
           draw relocate -1.6 0 /box 4
           label "record number" /center 5 
           g\set orient 0
        else
           box n n n
        endif
    next
next
!
let blc[1:2] 0. 0.
let trc[1:2] 0. 0.
!
!
