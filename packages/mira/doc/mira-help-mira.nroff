.ec _
.ll 76
.ad b
.in 4

.ti -4
1 CAL 
.ti +4
MIRA_\CAL [ALL|ifb] [/GAINS [/TCAL]] [/PHASE]
       [/OFF [NONE] [AVER] [EQUAL] [TIME] [TOTAL vMin vMax] /MASK d1 [d2]]
       [/FEBECAL jfb]

CAL calibrates the raw data of the observations loaded with MIRA\SCAN.
By default, the first frontend-backend combination is calibrated, 
otherwise the frontend-backend combination number ifb, or all frontend-backend
combinations if ALL is specified instead. If no option is specified, all three 
calibration stages are performed (i.e. normalization by channel gains, 
conversion from backend counts to a temperature scale, subtraction of the 
off-signal with concatenation of autocorrelator basebands.

.br 	
CAL /GAINS normalizes the spectral band by the gainarray.

.br
CAL /TCAL
Converts from backend counts to temperature (forward beam 
brightness temperature if beam efficiciency set to forward efficiency,
otherwise main beam brightness temperature), using the TCal scale 
automatically determined when a "chopper wheel" calibration is loaded.

.br
CAL /PHASE
Applies the phase calibration for XPOL measurements (i.e. VESPA in 
polarimetry mode). The imaginary part of the cross correlation
between orthogonally polarized receivers thus becomes the Stokes V
parameter (circular polarization). In order to have a valid
phase calibration, a calibGrid measurement with an identical spectral
setup has to be previously calibrated.

.br 
CAL /OFF subtracts the atmospheric emission as measured on the OFF 
position, and concatenates spectral basebands. For frequency-switched
observations, the subtraction of the reference signal has to be done in
CLASS with command FOLD.

For on-offs with continuum backends, a baseline linearly varying with 
elapsed time is subtracted before further processing the data. The
resulting fluxes and errors can be displayed with VIEW /PHASE.

.br
For on-the-fly data in total power mode, CAL /OFF has the following options:

.br
CAL /OFF NONE does not subtract the reference signal, only concatenates
spectral basebands. Both the reference spectra and on-source spectra are
written to the CLASS file (see command MIRA\WRITE).

.br
CAL /OFF AVER uses as reference signal for off subtraction the unweighted
mean of all available reference measurements. This is the default.

.br
CAL /OFF EQUAL uses as reference signal an unweighted mean of the
off-measurements taken before respectively after the on-subscan.
                                                                                
.br
CAL /OFF TIME uses as reference signal a weighted mean of the
off-measurements taken before respectively after the on-subscan. The
first reference has a weight decreasing with the time elapsed between the
on-the-fly record and the first reference measurement (and the weight of the
second reference increases correspondingly).
                                                                            
.br
CAL /OFF TOTAL vMin vMax uses as reference signal a weighted mean value of
the off-measurements taken before respectively after the on-subscan. The
weighting is done in a way to ensure that the total power of the
on-the-fly record equals the total power of the reference signal. A spectral
line contributing significantly to the total power can be masked with
the arguments vMin and vMax.

.br 
CAL /MASK d1 [d2 [d3 d4 ...]] uses the OTF subscans themselves as atmospheric
reference, masking the source, which appears between dumps d1 and d2 (and d3
and d4 etc). If only one argument d1 is given, this is the number of dumps used 
as reference at the start and end of each OTF subscan.

.bf
CAL ifb /FEBECAL jfb calibrates the frontend-backend combination ifb with the
calibration measurement of frontend-backend combination jfb.



.ti -4
1 DESPIKE
.ti +4
MIRA_\DESPIKE [ifb] [/PIXEL [ipix]] [/ITERATE niter]
                    [/WINDOW vMin vMax] [/THRESHOLD value]

DESPIKE removes spikes from spectra, OTF data, and continuum drifts, 
respectively. If the frontend-backend identification number ifb is not 
specified, the first frontend-backend combination of the current scan is 
despiked. 

.br 	
DESPIKE /PIXEL [ipix]
.br
Allows to specify the pixel number ipix for array observations. Default is
pixel 1 (for HERA1 and HERA2: pixel 5, the central one).

.br 	
DESPIKE /ITERATE niter   
.br
Allows to remove up to niter spikes (default is niter = 10).

.br
DESPIKE /WINDOW vMin vMax
.br
Spectral channels with velocities between vMin and vMax are not considered in
the search for spikes. Used to avoid confusion between e.g. narrow spectral 
lines and spikes.

.br
DESPIKE /THRESHOLD value
.br
By default, the threshold for searching for spikes is 5 sigma (rms), i.e. 
records where the difference between the data value and the median of the 
spectrum or the time series is higher than 5 sigma are despiked (i.e. attributed
the blanking value). With option /THRESHOLD, the threshold can be set to another
value.



.ti -4
1 FILE 
.ti +4
MIRA_\FILE IN|OUT name [NEW] [/CLASS] [/MBFITS] [/SHOW]

Selects the input directory and output files.
.nf
FILE IN name         defines the input directory
FILE OUT name [NEW]  defined the output file;
                     initializes a file if NEW is given.

.br
FILE /CLASS Opens a CLASS type file for output (default). If the output
            filename has no extension, defaults to .30m

.br
FILE /MBITS Converts IMBFITS raw data to MBFITS raw data. 
            Not yet implemented.

.br
FILE /SHOW  Displays the names of the current input and output files
            (if opened, otherwise a warning message is issued).


.ti -4
1 FIND 
.ti +4
MIRA_\FIND [/BACKEND] [/FRONTEND] [/LINE] [/OBSERVED] [/PROCEDURE]
           [/SCAN] [/SOURCE] [/TELESCOPE] [/NEW] [/SWITCHMODE] [/SILENT]
	   [/WHAT] [/PROJECT] [/STAT]

FIND performs a search in the input directory to build a new index, 
according to selection criteria defined by one or more of the following 
options. The index list (as desribed in the help for command MIRA_\LIST). 
Each option accepts more than one argument.

NEW: FIND is not anymore a prerequisite for the MIRA_\SCAN command. In case of
ambiguous scan numbers, MIRA_\SCAN will implicitely take the last scan found
(i.e. the most recent one).

NOTE: if you use MIRA_\FIND together with a large data archive, you can 
accelerate the search by either not using FIND at all or by specifying at least one of the options /SCAN (will be fastest), /OBSERVED or /BACKEND.

.nf
/BACKEND name                 Searches for data from backend name.
/FRONTEND receiver            Searches for data from a given receiver.
/LINE     transition          Search for data of spectral line transition.
/OBSERVED startDate [endDate] Searches for data observed between startDate 
                              and endDate. If endDate is not specied, looks 
			      only for data observed the day of startDate.
			      Date format is YYYY-MM-DD (e.g. 2005-09-29).
/PROCEDURE procedure          Searches for data of a given observing 
                              procedure (e.g. pointing, focus, onOff, 
			      calibration,...).
/SCAN i1 [i2]                 Searches for data from scans i1 to i2 (or only
                              scan i1 if i2 not specified).
/SOURCE object                Searches for data from source object.
/TELESCOPE antenna            Searches for data from telescope antenna.
/NEW                          Searches for new data written to the input
                              directory and appends them to the existing
			      index list. If no new data are found, 
			      the current index list remains unchanged. For 
			      online data processing at the telescope.
/SWITCHMOD switch             Searches for observations done with switchmode
                              switch (e.g. wobbler, beamSwitching).
/SILENT                       Loads the observations corresponding to the search
                              criteria into the index list, without issuing the
                              list output.
/WHAT                         List the selection currently used.
/PROJECT xxx-yy               Selects only data from project xxx-yy.
/STAT                         Writes the total integration time of all 
                              observations in the current index list into 
			      output file .stat (mainly for observing pool
			      administration).


.ti -4
1 FLAG 
.ti +4
MIRA_\FLAG [all|ifb] [/BASEBAND] [/CHANNEL] [/PIXEL] [/RECORD] [/SUBSCAN]

FLAG allows to ignore selected data for further reduction by attributing them
the blanking value, for frontend-backend unit ifb (or for all units using the
corresponding keyword). If only one item is specified (e.g. baseband), the 
corresponding baseband(s) are flagged for all pixels and records, unless
options /PIXEL and /RECORD are specified, too. Multiple arguments are possible.
To reset the flags, the scan has to be read again.


.ti -4
1 LIST 
.ti +4
MIRA_\LIST [/OUTPUT listfile] [/LONG] [/PROJECT] [/REDUCE] [/FLUX [backend]] [/FORMAT] [/FLAG]

LIST writes the current index list to the screen:
.nf
column 1 :      scan number
column 2 :      source name
column 3 :      observing procedure
column 4 :      switch mode
column 5 :      backend name
column 6 :      observing date (format YYYY-MM-DD)
column 7 :      running index

.br
LIST /OUTPUT listfile   Writes the index list into output file listfile, 
                        instead of writing to the screen. No default 
			provided (LIST /OUTPUT has the same action as LIST).
			Default extension is .lis

.br
LIST /LONG              Provides a more complete listing. Each frontend-
                        backend combination is listed. Information which is 
			the same for all frontend-backend combinations is 
			only written once per scan (all columns except for 
			the spectral line name and the frontend name).
.nf
column 1:               scan number 
column 2:               source name
column 3:               spectral line name
column 4:               observing procedure
column 5:               switch mode
column 6:               frontend name
column 7:               backend name
column 8:               observing date
column 9:               running index 

.br
LIST /PROJECT           Includes the project number into the current index
                        list. LIST /LONG and LIST /PROJECT are mutually
			exclusive. The listing gets the following format:
.nf
column 1 :      scan number
column 2 :      source name
column 3 :      observing procedure
column 4 :      switch mode
column 5 :      backend name
column 6 :      observing date (format YYYY-MM-DD)
column 7 :      project number (format XXX-YY)
column 8 :      running index

.br
LIST /REDUCE [macro] [classFile] Instead of listing the data in the current
index, prepares a procedure for off-line data reduction (default: reduce.mira)
of all observations in the current index. The class output file defaults to
mira.30m

.br
LIST /FLUX writes an ASCII-table point_dd-mmm-yyyy.dat with pointing 
corrections and fluxes for all pointings in the index list. The flux unit is
antenna temperature [K] if the index list contains the corresponding calibration
measurements, otherwise backend counts. The executions halts after subscans and
scans if doPause = yes (then type c to continue or q to quit). If a backend
name is specified as argument, fluxes from on-offs with the corresponding
backend are written instead.

.br
LIST /FORMAT [shortByScan] [longByScan] [shortBySubscan] [longBySubscan]
Options for the output format of the pointing flux list: "short" means the 
essential information, "long" yields some extra information appended to extra
columns extending the output file, "ByScan" means that only solutions for 
subscan averages (of azimuth respectively elevation subscans) are written,
whereas "BySubscan" contains the solution for individual subscans.

.br
LIST /FLAG ifb isub1 isub2 isub3 ...
Only used together with option /FLUX. Flags frontend-backend unit ifb in
subscan(s) isub1, isub2, isub3 etc. , for all pointing scans in the current list.
If you only want to flag a single scan iscan, use FIND /SCAN iscan prior to
calling LIST /FLUX.


.ti -4
1 OVERRIDE
.ti +4
MIRA_\OVERRIDE TEM[PLOAD] | EFF[ICIENCY] | GAIN[IMAGE] value1 value2 |
               TAMB[_P_HUMID] value1 value2 [/FEBE ifb|all] [/RESET]

OVERRIDE allows to change parameters relevant for calibration. By default,
the parameters for frontend-backend combination 1 are changed (see below
for option /FEBE). Instead of value1 and value2, asterisks may be used to
leave the parameter unchanged (see below). The parameters changed are
displayed on the screen. If the scan loaded is a calibration (i.e. sky,
hot load, cold load), the calibration parameters are automatically
re-calculated, using the new values for the load temperatures and/or
efficiencies. The calibration parameters in the raw data are replaced by those
set with OVERRIDE until reset by OVERRIDE /RESET

.br
Keyword TEM[PLOAD]:     changes the temperatures of the cold load (value1) 
                        and of the hot load (value2).

Keyword EFF[ICIENCY]:   changes the forward efficiency (value1) and the
                        beam efficiency (value2). 

Keyword GAIN[IMAGE]:    changes the the suppression of the image band
                        (value1).

OVERRIDE /FEBE ifb|all  This is to specify the frontend-backend combination
                        (ifb) for which the calibration parameters are to
                        be changed. Keyword "all" changes the parameters
                        for all frontend-backend combinations in the
                        current scan.

OVERRIDE /RESET         Resets all calibration parameters to default (raw data
                        values). A new calibration has to be read in order to 
			activate the change.

EXAMPLES: 
OVERRIDE TEMPLOAD * 300 /FEBE all  Changes the hot load temperature for all 
                                   frontends to 300 K. All cold load 
                                   temperatures are left unchanged.

OVERRIDE EFFICIENCY 0.95           Changes the forward efficiency of
                                   frontend 1 to 0.95. All beam
                                   efficiencies are unchanged.

OVERRIDE GAINIMAGE 0.1 /FEBE 1     Changes the sideband suppression for the 
                                   receiver in frontend-backend combination
                                   number 1 to 0.1.

OVERRIDE TAMB_P_HUMID -7.5 712     Changes ambient outside temperature and 
                                   pressure to -7.5 deg C and 712 hPa.

.ti -4
1 SCAN
.ti +4
MIRA_\SCAN scan [/TRACKING trackingError] [/COMPRESS integTim] 
                [/TAU tau1 [tau2 ...]] [/PWV h2omm]
		[/SUBSCAN first last] [/BACKEND backend1 backend2 ...]
		[/DROP ignoreBackend1 ignoreBackend2 ...]

Loads a scan from the current index list. ALL frontend-backend combinations
used are loaded. A list of identification numbers for the frontend-backend
combinations is issued. For calibrations, MIRA\SCAN also computes the 
calibrations parameters, and writes them to the screen. The calibration 
parameters for VESPA basebands are individually computed; however, the 
screen output is for mean values across all basebands corresponding to one 
spectrum.

NOTE: (1) MIRA_\FIND is not anymore a prerequisite for command MIRA_\SCAN (see help
      for FIND).
      (2) After commands OVERRIDE (for calibrationts) or changing the value of
      logical flag timingCheck the scan needs to be read again in order to validate the 
      new settings.

.br
SCAN /TRACKING trackingError Backend dumps with azimuth- or elevation tracking
                             errors larger than trackingError (in degree)
                             are flagged (i.e. attributed the blanking value 
                             for raw data).

.br
SCAN /COMPRESS dumpTime      Allows to compress large data sets (e.g. with
                             too short a dump time). If the new dump time
			     is not an integer multiple of the uncompressed
			     dump time, command SCAN rounds the compression
			     factor to the next integer.

.br
SCAN /TAU tau1 tau2 ...      For calibration scans. Keeps the opacity fixed
                             at values tau1 tau2 ... for frontend-backend 
			     numbers 1,2,..., and calculates the
			     corresponding calibration.

.br
SCAN /PWV h2omm              For calibration scans. Keeps the atmospheric
                             water vapour fixed at value h2omm and calculates
			     the corresponding calibration for all connected 
                             frontend-backend units.

.br
SCAN /SUBSCAN first last     Only reads in subscans "first" to "last".

.br
SCAN /BACKEND backend1 backend2 ...      Only reads in backend1, backend2, etc.

.br
SCAN /DROP ignoreBackend1 ignoreBackend2 ... Ignores listed backend for reading.


.ti -4
1 SOLVE
.ti +4
MIRA_\SOLVE [ifb [jfb]] [/PIXEL ipix] [/BINNING nbin]

SOLVE retrieves the pointing respectively focus corrections or a solution
to a skydip from the observing procedure loaded with MIRA\SCAN. By default,
the first frontend connected to the continuum backend is used, otherwise
number ifb. The pointing and focus results are written to the output device,
and to output files miraResultsPointing.xml respectively miraResultsFocus.xml
for further use by the telescope's control system. If two receiver numbers ifb
and jfb are entered, the receiver alignment in the Nasmyth focus is fitted and
plotted (together with the other results in file alignment.dat created by MIRA).

.br
SOLVE /PIXEL ipix      For observations with HERA. Allows to specify 
                       another pixel than the reference pixel for pointing,
		       focus or skydip. For skydips, enter SOLVE /PIXEL 0
		       to plot all pixels.

.bf
SOLVE /BINNING nbin    For pointing measurements, allows to smooth the 
                       azimuth and elevations drifts (for weak pointing 
		       sources). nbin is the number of dumps averaged
		       (simple box window smoothing).



.ti -4
1 VARIABLE
.ti +4
MIRA_\VARIABLE section|* read|write

Generates a SIC copy of one of the following MIRA data sections 
(wildcard permitted, activates all sections), for read (default)
ow write access. For further documentation on MIRA's data structure,
please consult section 5 of the MIRA manual. The section MON consists of 
SIC structures MON%HEADER and MON%DATA. Each frontend-backend 
combination has its own GAINS and REDUCE section (e.g. GAINS1, REDUCE3,
etc.). The sections SCAN, FEBE, and DATA consist of header and data 
structures for each frontend-backend combination, e.g. SCAN1%HEADER,
SCAN1%DATA, FEBE2%HEADER, FEBE3%DATA, and so on. The DATA and HEADER 
structures of section ARRAY are written for each baseband, e.g. 
ARRAY1%HEADER1, ARRAY2%DATA3, etc.

.nf
VARIABLE PRIM     Retrieves the IMBFITS primary header.
VARIABLE SCAN     Same for the scan header and table sections.
VARIABLE FEBE     Information related to the frontend-backend combination.
VARIABLE ARRAY    Raw data headers and data sections.
VARIABLE DATA     For calibrated raw data, and monitor points interpolated
                  at the backend timestamps.
VARIABLE MON      Monitoring headers and table sections.
VARIABLE GAINS    Calibration parameter for each frontend-backend 
                  combination.
VARIABLE CHOICE   Search options for MIRA's command FIND.
VARIABLE REDUCE   MIRA internal flags for calibration history.
VARIABLE LIST     Entries of the current index list.



.ti -4
1 VIEW
.ti +4
MIRA_\VIEW [ifb] [/CAL] [/GAINS] [/PHASES [phaseId] [irec]] [/SIGNAL [idump]] 
                 [/MAP] [/PIXEL ipix] [/TRACES [trackLimits]] [/XPOL ipart]
		 [/ZOOM] [/DEROTATOR [frameAngle]] [/OTF]

VIEW is MIRA's command for plotting the data of frontend-backend 
combination number ifb (default is 1). Alternatively, VIEW ifb1 ifb2 
concatenates bands ifb1 and ifb2 for plotting (for option /SIGNAL only), and
VIEW ifb1 TO ifb2 concatenates all bands from ifb1 to ifb2.

The item to be plotted is specified
by one of the following options. Otherwise, the following defaults are 
provided: VIEW /CAL for chopper-wheel calibrations, VIEW /PHASES for all 
uncalibrated data, VIEW /SIGNAL for calibrated data (at least off 
subtracted) from a single position, respectively VIEW /MAP for OTF maps.

.br
VIEW /CAL     
.br
For calibrations only. Plots the counts from the loads (sky, ambient, 
cold, respectively) versus time (for continuum backends. For spectral line 
backends, plots the subscan average of the dumps from the corresponding
loads versus frequency. Calibration and weather parameters are also displayed
(for HERA data only if option /PIXEL is used).

.br
VIEW /GAINS   
.br
Plots the gainarray currently in use. For spectrometers only.

.br
VIEW /MAP 
.br
Plots a pseudo-map (data values versus velocity and record number) for 
calibrated OTF data.

.br
VIEW /PHASES [phaseId] [irec]
.br
Plots the uncalibrated data against time (for continuum backends) 
respectively frequency and record number (for spectrometers), using 
phase phaseId (according to the observing procedure viewed, it defaults to ON, 
SKY, or FHI). By default, the scan average of all phases iphase in shown, 
otherwise record number irec.

For on-offs with continuum backends, the mean flux and its estimated error are
displayed (for HERA data please select a pixel). For continuum data taken
with the beam switch, phase1-phase2 is plotted, where the leading phase1 is
given by phaseId. For one of phaseId = 1, 2, ..., nphases, only the total
power for the corresponding phase is shown.

.br
VIEW /PIXEL ipix
.br
By default, VIEW shows data for all HERA pixels. If you want to visualize
only one pixel (e.g. together with option /ZOOM, see below), please use this
option.

.br
VIEW /DEROTATOR [frameAngle]
.br
Plots HERA's derotator angle for the Nasmyth reference system, for the last
scan read in. If frameAngle is not specified, the current position angle of
HERa is used.

.br
VIEW /SIGNAL [idump] 
.br
Plots calibrated data, as time series (for continuum drifts) respectively
spectra (for spectrometers). For OTF maps, record number idump can be 
specified to plot a single spectrum.

.br
VIEW /TRACES [trackLimits]
.br
A tool to visualize the antenna speed and fast trace values. The plot 
distinguishes between the trace values, and their interpolation at the 
backend record time (used for further data processing). Optionally, 
parameter trackLimits [arc sec] can be used to narrow the plot limits for the
tracking errors.

.br
VIEW [ipart] /XPOL
.br
Allows to plot the amplitude and phase of the cross correlation of 
orthogonally polarized receivers, as measured by a calibGrid procedure
(only for VESPA data in XPOL mode). If the Stokes parameters of several 
spectral lines are simulteaneously measured, ipart sets the VESPA part
(defaults to 1). In order to retrieve the amplitude and phase, please 
perform first a calibration.

.br
VIEW /ZOOM
.br
Plots and calls the interactive cursor to define a region for a zoom. Use 
the cursor to define the lower left and upper right corner (order does not
matter).

.br
VIEW /OTF
.br
Displays a layout of points on the sky sampled by the OTF maps in the current
index list generated with FIND (latitude offset vs. longitude offset).



.ti -4
1 WRITE
.ti +4
MIRA_\WRITE [/FEBE ALL|ifb] [/PIXEL ALL|ipix] [/SUBSCANS isub]

Writes calibrated data to a CLASS file (respectively raw data to a
MBFITS file, not yet implemented, see help for MIRA\FILE). The number of 
the spectrum (or continuum drift) is successively increased. 

.br
WRITE  /FEBE ALL|ifb     
.br
Specifies the frontend-backend combination to be written (here: ifb). Default 
is 1. Argument ALL: writes CLASS spectra for all frontend-backend combinations 
in current index.

.br
WRITE /PIXEL ALL|ipix
.br
For HERA data: spectra from pixel ipix are written (default pixel 1). To
write out all pixels: WRITE /PIXEL ALL

.br
WRITE /SUBSCAN isub
.br
By default, all subscans are written to the CLASS output file (for OTF maps
one CLASS spectrum per records, for pointing one CLASS drift per subscan).
With the /SUBSCAN option, only a given subscan of an OTF map or a pointing 
is written to the output file (defaults to subscan 1 if isub is not specified).

                                                                                
.ti -4
1 ENDOFHELP

