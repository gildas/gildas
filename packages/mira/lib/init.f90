subroutine init
!
! called by load_mira
!
   !
   use gkernel_interfaces
   use mira
   !
   implicit none
   !
   logical error
   !
   clight = 2.99792458d+5
   pi = dacos(-1.d0)
   twopi = 2.*pi
   rad2deg = 1.8d+02/pi
   rad2asec = rad2deg*3600.
   sec2rad = pi/(12.*3.6d+3)
   sec2day = 1.d0/(24.*3.6d+3)
   overrideflag(1:7) = .false.
   !
   nmxoff = 5
   !
   blankingraw = -1.0e10
   blankingred = -1000.
   !
   calbychannel    = .false.
   ncalchan        =  1
   calcheck        = .false.
   timingcheck     = .true.
   traceExtrapolation = .true.
   classoption     = .true.
   dopause         = .false.
   writexml        = .false.
   searchtrec      = .false.
   ignoreTraceflag = .false.
   flagBSwSpikes   = .false.
   atmVersion      = '2009'
   atmType         = 'TROPICAL'
   !
   call sic_let_char('atm%profile%type',atmType,error)
   !
   badLevel        = 20.
   !
   ncsdata = '.'
   visdata = '.'
   !
   call sic_def_logi('calCheck',calcheck,.false.,error)
   call sic_def_logi('calByChannel',calbychannel,.false.,error)
   call sic_def_inte('ncalchan',ncalchan,0,0,.false.,error)
   call sic_def_logi('doPause',dopause,.false.,error)
   call sic_def_logi('writeXML',writexml,.false.,error)
   call sic_def_logi('searchTrec',searchtrec,.false.,error)
   call sic_def_logi('ignoreTraceflag',ignoreTraceflag,.false.,error)
   call sic_def_logi('flagBswSpikes',flagBSwSpikes,.false.,error)
   call sic_def_char('ncsData',ncsdata,.false.,error)
   call sic_def_char('visData',visdata,.false.,error)
   call sic_def_char('atmVersion',atmVersion,.false.,error)
   call sic_def_real('badLevel',badlevel,0,0,.false.,error)
   call sic_def_logi('timingCheck',timingCheck,.false.,error)
   call sic_def_logi('traceExtrapolation',traceExtrapolation,.false.,error)
!
end subroutine init

subroutine exit_mira

   use mira

   implicit none

   integer :: i, ibd, ier, ifb
   logical :: error
   !
   if (associated(scan)) call freescan(scan%data,error)
   if (associated(sc%data%longoff)) call freescan(sc%data,error)
   !
   if (allocated(fb)) then
      do ifb = 1, size(fb)
         call freefebe(fb(ifb)%data,error)
      enddo
      deallocate(fb, stat = ier)
   endif
   !
   if (allocated(monbuf)) then
      do ifb = 1, size(mon)
         call freemon(monbuf(ifb)%data,error)
      enddo
      deallocate(monbuf,stat=ier)
   endif
   !
   if (allocated(databuf)) then
      do ifb = 1, size(databuf)
         call freedata(databuf(ifb)%data,error)
      enddo
      deallocate(databuf,stat=ier)
   endif
   !
   if (allocated(arraybuf)) then
      do ifb = 1, size(arraybuf)
         do ibd = 1, size(arraybuf(ifb)%header)
            call freearray(arraybuf(ifb)%data(ibd),error)
         enddo
         deallocate(arraybuf(ifb)%header,arraybuf(ifb)%data,   &
                    stat =ier)
      enddo
      deallocate(arraybuf,stat=ier)
   endif
   !
   if (allocated(reduce)) deallocate(reduce,stat = ier)
   if (associated(cryo))    call freecryo(cryo,error)
   if (associated(crx%mjd)) call freecryo(crx, error)
   !
   if (allocated(raw)) then
      do i = 1, size(raw)
         call freeraw(raw(i), error)
      enddo
      deallocate(raw, stat = ier)
   endif
   !
   if (allocated(newpar)) then
      do i = 1, size(newpar)
         call freenewpar(newpar(i),error)
      enddo
      deallocate(newpar,stat=ier)
   endif
   !
   if (allocated(gn)) then
      do ifb = 1, size(gn)
         call freegains(gn(ifb),error)
      enddo
      deallocate(gn,stat=ier)
   endif
   !
   if (allocated(mylist)) then
      do i = 1, size(mylist)
         call freelist(mylist(i),error)
      enddo
      deallocate(mylist,stat=ier)
   endif
   !
   if (allocated(integtyp)) then
      do i = 1, size(integtyp)
         deallocate(integtyp(i)%switch,stat=ier)
      enddo
      deallocate(integtyp,stat=ier)
   endif
   !
   if (allocated(iflag)) deallocate(iflag,stat=ier)
!
end subroutine exit_mira
