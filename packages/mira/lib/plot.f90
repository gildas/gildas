subroutine plot(line,error)
   !
   use mira
   use gildas_def
   use gkernel_types
   use gkernel_interfaces
   !
   implicit none
   !
   integer                      :: i,j,ibe,idump,ifb,jfb,   &
                                   ipart,ipix,nc
   type(sic_descriptor_t)       :: desc
   character(len = *)           :: line
   character                    :: ch*72, name*8,   &
                                   string*50,   &
                                   tmpstring*20,   &
                                   command*200
   character                    :: defaultphase*4,   &
                                   phaseid*8,   &
                                   tmpfebe*32,   &
                                   tmpsbcalrec*4
   character(len =8), parameter :: nullstring = '        '
   real                         :: derotangle
   real                         :: tracklimits
   logical                      :: error
   logical, dimension(13)       :: option
   logical                      :: exist,   &
                                   ex_array, ex_data,   &
                                   ex_febe, ex_gains,   &
                                   ex_monitor,   &
                                   ex_prim, ex_raw,   &
                                   ex_reduce, ex_scan
   !
   command = line
   !
   do i = 1, 13
      option(i) = sic_present(i,0)
   enddo
   !
   if (sic_present(0,1).and.sic_present(0,2)) then
      call sic_i4(line,0,1,ifb,.false.,error)
      ch(1:2) = '  '
      call sic_ch(line,0,2,ch(1:2),nc,.false.,error)
      call sic_upper(ch(1:2))
      if (ch(1:2).eq.'TO') then
         if (.not.sic_present(0,3)) then
            call gagout('E-PLOT: please enter a range of FeBe units.')
            error = .true.
            return
         endif
         call sic_i4(line,0,3,jfb,.false.,error)
         if (jfb.lt.ifb) then
            call gagout('E-PLOT: please enter a range of FeBe units.')
            error = .true.
            return
         endif
      else
         call sic_i4(line,0,2,jfb,.false.,error)
      endif
      if (option(3).and.sic_present(3,1)) then
         call sic_i4(line,3,1,idump,.false.,error)
      else
         idump = 0
      endif
      do j = 1, 3
         if (.not.reduce(ifb)%calibration_done(j)) then
          call gagout('E-PLOT: concatenation mode only for calibrated spectra.')
          error = .true.
          return
         endif
      enddo
      do j = 1, 3
         if (.not.reduce(ifb)%calibration_done(j)) then
          call gagout('E-PLOT: concatenation mode only for calibrated spectra.')
          error = .true.
          return
         endif
      enddo
      if (ch(1:2).eq.'TO') then
         write(string,'(A28,I4,I3,A4,I3)') '@ gag_pro:p_plot_concat.mira', &
                                           idump,ifb,' TO ',jfb
      else
         write(string,'(A28,I4,I3,I3)') '@ gag_pro:p_plot_concat.mira', &
                                           idump,ifb,jfb
      endif
      call exec_program(string)
      if (option(7)) then
         call exec_string('g\draw relocate' ,error)
         call exec_string('let blc[1:2] use_curs', error)
         call exec_string('g\draw relocate', error)
         call exec_string('let trc[1:2] use_curs', error)
         call exec_string('let blc[3] min(blc[1],trc[1])', error)
         call exec_string('let trc[3] max(blc[1],trc[1])', error)
         call exec_string('let blc[4] min(blc[2],trc[2])', error)
         call exec_string('let trc[4] max(blc[2],trc[2])', error)
         call exec_string('let blc[1:2] blc[3:4]', error)
         call exec_string('let trc[1:2] trc[3:4]', error)
         call exec_string('g\draw relocate blc[1] blc[2] /user', error)
         call exec_string('g\draw line     trc[1] blc[2] /user', error)
         call exec_string('g\draw line     trc[1] trc[2] /user', error)
         call exec_string('g\draw line     blc[1] trc[2] /user', error)
         call exec_string('g\draw line     blc[1] blc[2] /user', error)
         call exec_string('sic wait 0.5', error)
         call exec_program(string)
      endif
      call sic_insert(command)
      return
   endif
   !
   if (option(10)) then
      call exec_program('@ gag_pro:p_plot_otf.mira')
      call sic_insert(command)
      return
   endif
   !
   if (option(11)) then
      if (sic_present(11,1)) then
         call sic_r4(line,11,1,derotangle,.false.,error)
      else
         derotangle = -1000.
      endif
      write(string,'(A27,F8.1)') '@ gag_pro:p_plot_derot.mira',   &
                                 derotangle
      call exec_program(string)
      call sic_insert(command)
      return
   endif
   !
   tmpstring = scan%header%scantype
   call sic_upper(tmpstring)
   !
   if (.not.sic_present(0,1)) then
      call gagout('W-PLOT: First backend found plotted.')
      ibe = 1
      ifb = ibe
   else if (index(tmpstring,'CAL').eq.0) then
      call sic_i4(line,0,1,ibe,.false.,error)
      if (ibe.gt.scan%header%nfebe.or.ibe.le.0) then
         call gagout('E-PLOT: Invalid backend specification.')
         error = .true.
         return
      endif
      ifb = ibe
      if (sic_present(0,2)) then
         call sic_i4(line,0,2,ibe,.false.,error)
         if (ibe.gt.scan%header%nfebe.or.ibe.le.0) then
            call gagout('E-PLOT: Invalid backend specification.')
            error = .true.
            return
         endif
         jfb = ibe
      else
         jfb = 0
      endif
   else
      call sic_i4(line,0,1,ibe,.false.,error)
      ifb = ibe
      if (ibe.gt.size(gains).or.ibe.le.0) then
         call gagout('E-PLOT: Invalid backend specification.')
         error = .true.
         return
      else if (ibe.gt.scan%header%nfebe) then
         ibe = ifb-scan%header%nfebe
      endif
   endif
   !
   tmpfebe = febe(ibe)%header%febe
   call sic_upper(tmpfebe)
   tmpsbcalrec = raw(1)%antenna(ibe)%sbcalrec
   call sic_upper(tmpsbcalrec)
   !
   call sic_descriptor('PRIM',desc,ex_prim)
   if (.not.ex_prim) call prim_to_sic(.true.,error)
   call sic_descriptor('SCAN',desc,ex_scan)
   if (.not.ex_scan) call scan_to_sic(.true.,error)
   call sic_descriptor('MON',desc,ex_monitor)
   if (.not.ex_monitor) call mon_to_sic(.true.,error)
   call sic_descriptor('RAW',desc,ex_raw)
   if (.not.ex_raw) call raw_to_sic(.true.,error)
   !
   ex_reduce = .false.
   ex_array  = .false.
   ex_data   = .false.
   ex_febe   = .false.
   ex_gains  = .false.
   !
   do i = 1, scan%header%nfebe
      name = nullstring
      write(name,'(A6,I0)') 'REDUCE',i
      call sic_descriptor(trim(name),desc,exist)
      if (exist) ex_reduce = .true.
      name = nullstring
      write(name,'(A5,I0)') 'ARRAY',i
      call sic_descriptor(trim(name),desc,exist)
      if (exist) ex_array = .true.
      name = nullstring
      write(name,'(A4,I0)') 'DATA',i
      call sic_descriptor(trim(name),desc,exist)
      if (exist) ex_data = .true.
      name = nullstring
      write(name,'(A4,I0)') 'FEBE',i
      call sic_descriptor(trim(name),desc,exist)
      if (exist) ex_febe = .true.
      name = nullstring
      write(name,'(A5,I0)') 'GAINS',i
      call sic_descriptor(trim(name),desc,exist)
      if (exist) ex_gains = .true.
   enddo
   !
   if (.not.ex_reduce) call reduce_to_sic(.true.,error)
   if (.not.ex_array)  call array_to_sic(.true.,error)
   if (.not.ex_data)   call data_to_sic(.true.,error)
   if (.not.ex_febe)   call febe_to_sic(.true.,error)
   !
   if (option(9)) then
      if (sic_present(9,1)) then
         call sic_r4(line,9,1,tracklimits,.false.,error)
      else
         tracklimits = 0.
      endif
      write(string,'(A29,E14.8)')   &
        '@ gag_pro:p_plot_traces.mira ', tracklimits
      call exec_program(string)
      call sic_insert(command)
      return
   endif
   !
   if (.not.ex_gains)  call gains_to_sic(.true.,error)
   !
   if (count(option(1:5)).eq.0.and..not.option(8).and.   &
     .not.option(12).and..not.option(13)) then
      !
      if (count(array(ibe)%data(1)%iswitch.eq.'ON' ).ne.0) then
         defaultphase = 'ON'
      else if (count(array(ibe)%data(1)%iswitch.eq.'SKY').ne.0) then
         defaultphase = 'SKY'
      else if (count(array(ibe)%data(1)%iswitch.eq.'FHI').ne.0) then
         defaultphase = 'FHI'
      else if (count(array(ibe)%data(1)%iswitch.eq.'OFF').ne.0) then
         defaultphase = 'OFF'
      else if (count(array(ibe)%data(1)%iswitch.eq.'FLO').ne.0) then
         defaultphase = 'FLO'
      else if (count(array(ibe)%data(1)%iswitch.eq.'LOAD').ne.0) then
         defaultphase = 'LOAD'
      else if (count(array(ibe)%data(1)%iswitch.eq.'HOT').ne.0) then
         defaultphase = 'HOT'
      else if (count(array(ibe)%data(1)%iswitch.eq.'COLD').ne.0) then
         defaultphase = 'COLD'
      endif
      !
      if (index(tmpstring,'CAL').ne.0) then
         if (index(tmpstring,'IMAGE').ne.0) then
            if (index(tmpfebe,'CONT').eq.0.and.   &
                index(tmpfebe,'BBC').eq.0.and.   &
                index(tmpfebe,'NBC').eq.0.and.   &
                index(tmpfebe,'ABBA').eq.0   &
              .and.index(tmpfebe,tmpsbcalrec).ne.0) then
               write(string,'(A28,I2)')   &
                 '@ gag_pro:p_plot_gains.mira ',ibe
            else if (index(tmpfebe,'CONT').ne.0.or.   &
                     index(tmpfebe,'BBC').ne.0  .or.   &
                     index(tmpfebe,'NBC').ne.0  .or.   &
                     index(tmpfebe,'ABBA').ne.0) then
               write(string,'(A26,I2)')   &
                 '@ gag_pro:p_plot_cal.mira ',ibe
            else
               call gagout('I-PLOT: image gain calibration not for '   &
                           //'this receiver.')
               error = .false.
               return
            endif
         else
            write(string,'(A26,I2)') '@ gag_pro:p_plot_cal.mira ',ibe
         endif
      else if (reduce(ibe)%calibration_done(3)) then
         if (index(tmpstring,'FLY').ne.0.or.   &
             index(tmpstring,'DIY').ne.0) then
            if (index(tmpfebe,'CONT').ne.0.or.   &
                index(tmpfebe,'BBC').ne.0  .or.   &
                index(tmpfebe,'NBC').ne.0  .or.   &
                index(tmpfebe,'ABBA').ne.0) then
               write(string,'(A29,I2,1X,A1)')   &
                 '@ gag_pro:p_plot_signal.mira ', ibe, '0'
            else
               write(string,'(A34,I2)')   &
                  '@ gag_pro:p_plot_map.mira .false. ',ibe
            endif
         else if (index(tmpstring,'ONOFF').ne.0.and.   &
                 (index(tmpfebe,'CONT').ne.0.or.   &
                  index(tmpfebe,'BBC').ne.0  .or.   &
                  index(tmpfebe,'NBC').ne.0  .or.   &
                  index(tmpfebe,'ABBA').ne.0)) then
            if (len_trim(defaultphase).eq.2) then
               write(string,'(A29,I2,1X,A2,1X,A1)')   &
                 '@ gag_pro:p_plot_phases.mira ',ibe,   &
                 defaultphase, '0'
            else if (len_trim(defaultphase).eq.3) then
               write(string,'(A29,I2,1X,A3,1X,A1)')   &
                 '@ gag_pro:p_plot_phases.mira ',ibe,   &
                 defaultphase, '0'
            else if (len_trim(defaultphase).eq.4) then
               write(string,'(A29,I2,1X,A4,1X,A1)')   &
                 '@ gag_pro:p_plot_phases.mira ',ibe,   &
                 defaultphase, '0'
            endif
         else
            write(string,'(A29,I2,1X,A1)')   &
              '@ gag_pro:p_plot_signal.mira ', ibe, '0'
         endif
      else
         if (len_trim(defaultphase).eq.2) then
            write(string,'(A29,I2,1X,A2,1X,A1)')   &
              '@ gag_pro:p_plot_phases.mira ',ibe, defaultphase, '0'
         else if (len_trim(defaultphase).eq.3) then
            write(string,'(A29,I2,1X,A3,1X,A1)')   &
              '@ gag_pro:p_plot_phases.mira ',ibe, defaultphase, '0'
         else if (len_trim(defaultphase).eq.4) then
            write(string,'(A29,I2,1X,A4,1X,A1)')   &
              '@ gag_pro:p_plot_phases.mira ',ibe, defaultphase, '0'
         endif
      endif
   !
   else if (option(1)) then
      !
      if ((index(tmpfebe,'CONT').ne.0..or.   &
           index(tmpfebe,'BBC').ne.0  .or.   &
           index(tmpfebe,'NBC').ne.0  .or.   &
           index(tmpfebe,'ABBA').ne.0).and.   &
           index(tmpstring,'CAL').eq.0) then
         call gagout('E-PLOT: Only for calibrations.')
         error = .true.
         return
      endif
      if (.not.associated(gains(ifb)%phot)) then
         call gagout('E-PLOT: No calibration available.')
         error = .true.
         return
      endif
      write(string,'(A26,I2)') '@ gag_pro:p_plot_cal.mira ',ibe
   !
   else if (option(12)) then
      !
      if ((index(tmpfebe,'CONT').ne.0..or.   &
           index(tmpfebe,'BBC').ne.0  .or.   &
           index(tmpfebe,'NBC').ne.0  .or.   &
           index(tmpfebe,'ABBA').ne.0).and.   &
           index(tmpstring,'CAL').eq.0) then
         call gagout('E-PLOT: Only for calibrations.')
         error = .true.
         return
      endif
      if (.not.associated(gains(ifb)%phot)) then
         call gagout('E-PLOT: No calibration available.')
         error = .true.
         return
      endif
      write(string,'(A26,I2)') '@ gag_pro:p_plot_trx.mira ',ibe
   !
   else if (option(2).or.option(13)) then
      !
      if (.not.associated(gains(ifb)%gainimage)) then
         call gagout('E-PLOT: No image gains available.')
         error = .true.
         return
      else  if (.not.associated(gains(ifb)%gainarray)) then
         call gagout('E-PLOT: No gains available.')
         error = .true.
         return
      endif
      if (index(tmpfebe,'CONT').ne.0.or.   &
          index(tmpfebe,'BBC').ne.0.or.  &
          index(tmpfebe,'NBC').ne.0.or.  &
          index(tmpfebe,'ABBA').ne.0) then
         call gagout('E-PLOT: only for spectrometers.')
         error = .true.
         return
      endif
      if (option(2)) then
         write(string,'(A28,I2)') '@ gag_pro:p_plot_gains.mira ',ibe
      else
         write(string,'(A33,I2)') '@ gag_pro:p_plot_saturation.mira ',ibe
      endif
   !
   else if (option(8)) then
      !
      if (.not.associated(gains(ifb)%phasearray)) then
         call gagout('E-PLOT: No phases available.')
         error = .true.
         return
      else if (.not.reduce(ibe)%calibration_done(1).or.   &
               .not.reduce(ibe)%calibration_done(2).or.   &
               .not.reduce(ibe)%calibration_done(3)) then
         call gagout('E-PLOT: Please calibrate the cal /phase '   &
           //'measurement (syntax: CAL all)')
         error = .true.
         return
      endif
      if (index(tmpfebe,'VESPA').eq.0) then
         call gagout('E-PLOT: only for VESPA.')
         error = .true.
         return
      endif
      ipart = ibe
      write(string,'(A27,I2)') '@ gag_pro:p_plot_xpol.mira ',ipart
   !
   else if (option(3)) then
      !
      if (sic_present(3,1)) then
         if (.not.reduce(ibe)%calibration_done(3)) then
            call gagout('E-PLOT: only for data with off subtracted.')
            call gagout('        Use VIEW /PHASES instead.')
            error = .true.
            return
         endif
         call sic_i4(line,3,1,idump,.false.,error)
         if (idump.gt.size(data(ibe)%data%otfdata,3)) then
            call gagout('E-PLOT: record number out of range.')
            error = .true.
            return
         endif
      else
         idump = 1
      endif
      write(string,'(A29,I2,1X,I4)')   &
        '@ gag_pro:p_plot_signal.mira ', ibe, idump
   !
   else if (option(4)) then
      !
      if (sic_present(4,1)) then
         call sic_ch(line,4,1,phaseid,nc,.false.,error)
         if (nc.gt.4) then
            defaultphase = phaseid(1:4)
         else
            defaultphase = phaseid(1:nc)
         endif
         call sic_upper(defaultphase)
      else if (count(array(ibe)%data(1)%iswitch.eq.'ON' ).ne.0) then
         defaultphase = 'ON'
      else if (count(array(ibe)%data(1)%iswitch.eq.'SKY').ne.0) then
         defaultphase = 'SKY'
      else if (count(array(ibe)%data(1)%iswitch.eq.'FHI').ne.0) then
         defaultphase = 'FHI'
      else if (count(array(ibe)%data(1)%iswitch.eq.'OFF').ne.0) then
         defaultphase = 'OFF'
      else if (count(array(ibe)%data(1)%iswitch.eq.'FLO').ne.0) then
         defaultphase = 'FLO'
      else if (count(array(ibe)%data(1)%iswitch.eq.'LOAD').ne.0) then
         defaultphase = 'LOAD'
      else if (count(array(ibe)%data(1)%iswitch.eq.'HOT').ne.0) then
         defaultphase = 'HOT'
      else if (count(array(ibe)%data(1)%iswitch.eq.'COLD').ne.0) then
         defaultphase = 'COLD'
      endif
      !
      idump = 0
      if (sic_present(4,2)) then
         call sic_i4(line,4,2,idump,.false.,error)
         if (idump.gt.size(data(ibe)%data%otfdata,3)) then
            call gagout('W-PLOT: record number out of range. '//   &
                        'Ignored (plot mean of all records).')
            idump = 0
         endif
      else if (index(tmpFebe,'CONT').eq.0) then
         call gagout('I-PLOT: plot average of all records of '//   &
                     'phase given.')
         idump = 0
      else if (index(tmpFebe,'BBC').eq.0) then
         call gagout('I-PLOT: plot average of all records of '//   &
                     'phase given.')
         idump = 0
      else if (index(tmpFebe,'NBC').eq.0) then
         call gagout('I-PLOT: plot average of all records of '//   &
                     'phase given.')
         idump = 0
      endif
      !
      if (len_trim(defaultphase).eq.1) then
         write(string,'(A29,I2,1X,A1,1X,I4)')   &
           '@ gag_pro:p_plot_phases.mira ',ibe,   &
           defaultphase,idump
      else if (len_trim(defaultphase).eq.2) then
         write(string,'(A29,I2,1X,A2,1X,I4)')   &
           '@ gag_pro:p_plot_phases.mira ',ibe,   &
           defaultphase,idump
      else if (len_trim(defaultphase).eq.3) then
         write(string,'(A29,I2,1X,A3,1X,I4)')   &
           '@ gag_pro:p_plot_phases.mira ',ibe,   &
           defaultphase,idump
      else if (len_trim(defaultphase).eq.4) then
         write(string,'(A29,I2,1X,A4,1X,I4)')   &
           '@ gag_pro:p_plot_phases.mira ',ibe,   &
           defaultphase,idump
      endif
   !
   else if (option(5)) then
      !
      if (index(tmpstring,'FLY').eq.0.and.   &
          index(tmpstring,'DIY').eq.0) then
         call gagout('E-PLOT: only for OTF maps. '//   &
           'Use VIEW /SIGNAL instead.')
         error = .true.
         return
      endif
      write(string,'(A34,I2)') '@ gag_pro:p_plot_map.mira .false. ',   &
                               ibe
   !
   endif
   !
   if (index(tmpfebe,'HERA').ne.0.or.index(tmpfebe,'MAMB').ne.0) then
      ipix = 0
   else
      ipix = 1
   endif
   !
   if (option(6).and.index(tmpfebe,'HERA').ne.0) then
      if (sic_present(6,1)) then
         call sic_i4(line,6,1,ipix,.false.,error)
         if (ipix.lt.1.or.ipix.gt.febe(ibe)%header%febefeed) then
            call gagout('W-PLOT: invalid pixel number.')
            ipix = 0
         endif
      else
         call gagout('W-PLOT: no pixel specified.')
         ipix = 0
      endif
   endif
   !
   nc = len_trim(string)
   write(string(nc+2:nc+2),'(I1)') ipix
   write(string(nc+4:nc+5),'(I2)') jfb
   !
   call exec_program(string)
   if (option(7)) then
      if (index(tmpfebe,'HERA').ne.0.and..not.option(6)) then
         call gagout('E-PLOT: For HERA data, use option /ZOOM '   &
           //'only together with option /PIXEL, please.')
         error = .true.
         return
      endif
      if (index(string,'map').ne.0) then
         nc = index(string,'.mira')
         string(nc+6:nc+12)  = '.true. '
      endif
      call exec_string('g\draw relocate', error)
      call exec_string('let blc[1:2] use_curs', error)
      call exec_string('g\draw relocate', error)
      call exec_string('let trc[1:2] use_curs', error)
      call exec_string('let blc[3] min(blc[1],trc[1])', error)
      call exec_string('let trc[3] max(blc[1],trc[1])', error)
      call exec_string('let blc[4] min(blc[2],trc[2])', error)
      call exec_string('let trc[4] max(blc[2],trc[2])', error)
      call exec_string('let blc[1:2] blc[3:4]', error)
      call exec_string('let trc[1:2] trc[3:4]', error)
      call exec_string('g\draw relocate blc[1] blc[2] /user', error)
      call exec_string('g\draw line     trc[1] blc[2] /user', error)
      call exec_string('g\draw line     trc[1] trc[2] /user', error)
      call exec_string('g\draw line     blc[1] trc[2] /user', error)
      call exec_string('g\draw line     blc[1] blc[2] /user', error)
      call exec_string('sic wait 0.5', error)
      call exec_program(string)
   endif
   !
   call sic_insert(command)
   !
   if (.not.ex_prim) call sic_delvariable('PRIM',.false.,error)
   if (.not.ex_scan) call sic_delvariable('SCAN',.false.,error)
   if (.not.ex_gains) call sic_delvariable('GAINS*',.false.,error)
   if (.not.ex_reduce) call sic_delvariable('REDUCE',.false.,error)
   if (.not.ex_array) call sic_delvariable('ARRAY*',.false.,error)
   if (.not.ex_data)  call sic_delvariable('DATA*',.false.,error)
   if (.not.ex_febe)  call sic_delvariable('FEBE*',.false.,error)
!
end subroutine plot
