subroutine show(line,error)
!
! Unused (command is not defined in subroutine load_mira).
!
   !
   use mira
   !
   integer            :: nc
   character(len = *) :: line
   character          :: ch*72
   logical            :: case_array, case_febe, case_general,   &
                         case_monitor, error
   !
   call sic_ch(line,0,1,ch,nc,.true.,error)
   case_general = (index(ch,'GE').ne.0.or.index(ch,'ge').ne.0)
   case_febe  =   (index(ch,'FE').ne.0.or.index(ch,'fe').ne.0)
   case_array =   (index(ch,'AR').ne.0.or.index(ch,'ar').ne.0)
   case_monitor = (index(ch,'MO').ne.0.or.index(ch,'mo').ne.0)
   !
   if (case_general) then
      call gagout("General parameters"//   &
                  "(constant troughout observations) :")
   else if (case_febe) then
      call gagout("Frontend/backend related parameters :")
   else if (case_array) then
      call gagout("Data and data associated parameters :")
   else if (case_monitor) then
      call gagout("Monitor parameters :")
   endif
!
end subroutine show
