subroutine findmira(line,error)
!
! Searches the input directory for imbfits raw data, and calls
! subroutine getattributes to assign values to MIRA structure
! list(iobs), where iobs is the number of the observation in the
! index list.
!
! Called by MIRA command "find" (see subroutine run_mira, in mira/lib/run.f90).
! See online help or user's ! guide for earch options.
! Input: command line from Mira (see "help find")
! Output: error flag.
!
   !
   use       mira
   use       gildas_def
   use       gkernel_types
   use       gkernel_interfaces
   !
   implicit  none
   !
   character(len = *)                    :: line
   logical                               :: error
   !
   character                             :: directory*256
   integer                               :: i, iday, ier, imonth,   &
                                            iyear, j, k0, k1, k2, lc,   &
                                            nfile, mfile,   &
                                            unit
   integer                               :: lastnum, narg, nobs
   integer(kind=8)                       :: lc0, lc1
   integer, dimension(:), allocatable    :: indx
   type(sic_descriptor_t)                :: desc
   real*8, dimension(:), allocatable     :: lc2
   character scanid*4
   character(len = 9)                    :: filename
   character(len = 12)                   :: tmpfile
   character(len = 20)                   :: lastdate
   character(len = 20)                   :: name
   character(len= 60)                    :: tmp
   character(len = 60), dimension(:), allocatable :: listcopy,   &
                                                     filelist
   character(len = 200)                  :: command
   character(len = 300)                  :: commandstring
   logical                               :: exist, ex_list, readonly
   logical, dimension(14)                :: option
   logical                               :: newfilename
   !
   !
   command = line
   !
   option = .false.
   do i = 1,14
      option(i) = sic_present(i,0)
   enddo
   !
   if (option(12)) then
      call gagout(' ')
      call gagout(' CURRENT SELECTION:')
      call gagout(' ')
      if (len_trim(choice%scan(1)).eq.0) then
         call gagout(' All scans selected.')
      else
         call gagout(' Scan selection: '//trim(choice%scan(1))//   &
           ' to '//trim(choice%scan(2)))
      endif
      if (len_trim(choice%date_obs(1)).eq.0) then
         call gagout(' All dates selected.')
      else
         call gagout(' Date selection: '//trim(choice%date_obs(1))//   &
           ' to '//trim(choice%date_obs(2)))
      endif
      if (allocated(choice%backend)) then
         call gagout(' Backend selection:')
         do j = 1, size(choice%backend)
            call gagout('    '//trim(choice%backend(j)))
         enddo
      else
         call gagout(' All backends selected.')
      endif
      if (allocated(choice%frontend)) then
         call gagout(' Frontend selection:')
         do j = 1, size(choice%frontend)
            call gagout('    '//trim(choice%frontend(j)))
         enddo
      else
         call gagout(' All frontends selected.')
      endif
      if (allocated(choice%transition)) then
         call gagout(' Line selection:')
         do j = 1, size(choice%transition)
            call gagout('    '//trim(choice%transition(j)))
         enddo
      else
         call gagout(' All lines selected.')
      endif
      if (allocated(choice%procedure)) then
         call gagout(' Procedure selection:')
         do j = 1, size(choice%procedure)
            call gagout('    '//trim(choice%procedure(j)))
         enddo
      else
         call gagout(' All procedures selected.')
      endif
      if (allocated(choice%object)) then
         call gagout(' Source selection:')
         do j = 1, size(choice%object)
            call gagout('    '//trim(choice%object(j)))
         enddo
      else
         call gagout(' All sources selected.')
      endif
      if (allocated(choice%telescope)) then
         call gagout(' Telescope selection:')
         do j = 1, size(choice%telescope)
            call gagout('    '//trim(choice%telescope(j)))
         enddo
      else
         call gagout(' All telescopes selected.')
      endif
      if (allocated(choice%switchmode)) then
         call gagout(' Switchmode selection:')
         do j = 1, size(choice%switchmode)
            call gagout('    '//trim(choice%switchmode(j)))
         enddo
      else
         call gagout(' All switchmodes selected.')
      endif
      if (allocated(choice%project)) then
         call gagout(' Project selection:')
         do j = 1, size(choice%project)
            call gagout('    '//trim(choice%project(j)))
         enddo
      else
         call gagout(' All projects selected.')
      endif
      call gagout(' ')
      return
   endif
   !
   if (allocated(choice%backend)) deallocate(choice%backend,stat=ier)
   if (allocated(choice%frontend))   &
     deallocate(choice%frontend,stat=ier)
   if (allocated(choice%transition))   &
     deallocate(choice%transition,stat=ier)
   if (allocated(choice%procedure))   &
     deallocate(choice%procedure,stat=ier)
   if (allocated(choice%project))   &
     deallocate(choice%project,stat=ier)
   if (allocated(choice%object))   deallocate(choice%object,stat=ier)
   if (allocated(choice%telescope))   &
     deallocate(choice%telescope,stat=ier)
   if (allocated(choice%switchmode))   &
     deallocate(choice%switchmode,stat=ier)
   if (option(1)) then
      narg = sic_narg(1)
      allocate(choice%backend(narg),stat=ier)
      do i = 1, narg
         call sic_ch(line,1,i,choice%backend(i),lc,.false.,error)
         call sic_upper(choice%backend(i))
      enddo
   endif
   if (option(2)) then
      narg = sic_narg(2)
      allocate(choice%frontend(narg),stat=ier)
      do i = 1, narg
         call sic_ch(line,2,i,choice%frontend(i),lc,.false.,error)
         call sic_upper(choice%frontend(i))
      enddo
   endif
   if (option(3)) then
      narg = sic_narg(3)
      allocate(choice%transition(narg),stat=ier)
      do i = 1, narg
         call sic_ch(line,3,i,choice%transition(i),lc,.false.,error)
         call sic_upper(choice%transition(i))
      enddo
   endif
   if (sic_present(4,1)) then
      call sic_ch(line,4,1,choice%date_obs(1),lc,.false.,error)
   else
      choice%date_obs(1) = '                       '
   endif
   if (sic_present(4,2)) then
      call sic_ch(line,4,2,choice%date_obs(2),lc,.false.,error)
   else
      choice%date_obs(2) = '                       '
   endif
   if (option(5)) then
      narg = sic_narg(5)
      allocate(choice%procedure(narg),stat=ier)
      do i = 1, narg
         call sic_ch(line,5,i,choice%procedure(i),lc,.false.,error)
         call sic_upper(choice%procedure(i))
      enddo
   endif
   if (sic_present(6,1)) then
      call sic_ch(line,6,1,choice%scan(1),lc,.false.,error)
   else
      choice%scan(1) = '    '
   endif
   if (sic_present(6,2)) then
      call sic_ch(line,6,2,choice%scan(2),lc,.false.,error)
   else
      choice%scan(2) = '    '
   endif
   if (option(7)) then
      narg = sic_narg(7)
      allocate(choice%object(narg),stat=ier)
      do i = 1, narg
         call sic_ch(line,7,i,choice%object(i),lc,.false.,error)
         call sic_upper(choice%object(i))
      enddo
   endif
   if (option(8)) then
      narg = sic_narg(8)
      allocate(choice%telescope(narg),stat=ier)
      do i = 1, narg
         call sic_ch(line,8,i,choice%telescope(i),lc,.false.,error)
         call sic_upper(choice%telescope(i))
      enddo
   endif
   if (option(10)) then
      narg = sic_narg(10)
      allocate(choice%switchmode(narg),stat=ier)
      do i = 1, narg
         call sic_ch(line,10,i,choice%switchmode(i),lc,.false.,error)
         call sic_upper(choice%switchmode(i))
      enddo
   endif
   if (option(13)) then
      narg = sic_narg(13)
      allocate(choice%project(narg),stat=ier)
      do i = 1, narg
         call sic_ch(line,13,i,choice%project(i),lc,.false.,error)
         call sic_upper(choice%project(i))
      enddo
   endif
   !
   if (option(4).and..not.sic_present(4,2)) choice%date_obs(2)   &
     = choice%date_obs(1)
   if (option(6).and..not.sic_present(6,2)) choice%scan(2)   &
     = choice%scan(1)
   !
   !
   directory = 'INPUT_FILE:'
   ier = sic_getlog(directory)
   !
   filename = '*.fits'
   do i = 0, 999
      tmpfile = '.mira.lis'
      write(tmpfile(10:12),'(I0.3)') i
      inquire(file=tmpfile,exist=exist)
      if (.not.exist) exit
   enddo
   commandstring = 'sys "ls -1 '//trim(directory)//'|wc -l >'//   &
                   tmpfile//'"'
   call exec_program(commandstring)
   call exec_program(' ')
   ier = sic_getlun(unit)
   open(unit,file=tmpfile,status='OLD')
   read(unit,*) mfile
   close(unit)
   ier = gag_frelun(unit)
   if (allocated(filelist)) deallocate(filelist,stat=ier)
   allocate(filelist(mfile),stat=ier)
   commandstring = 'sys "ls -1 '//trim(directory)//' >'//tmpfile//'"'
   call exec_program(commandstring)
   ier = sic_getlun(unit)
   open(unit,file=tmpfile,status='OLD')
   nfile = 0
   do i = 1, mfile
      read(unit,*) tmp
      if (index(tmp,'.fits').ne.0.and.index(tmp,'bad').eq.0) then
         nfile = nfile+1
         filelist(nfile) = trim(tmp)
      endif
   enddo
   close(unit)
   commandstring = 'SIC DELETE '//tmpfile
   call exec_program(commandstring)
   call exec_program(' ')
   ier = gag_frelun(unit)
   !
   if (nfile.eq.0) then
      call gagout('W-FIND, directory contains no data')
      return
   endif
   !
   if (allocated(lc2)) deallocate(lc2,stat=ier)
   if (allocated(indx)) deallocate(indx,stat=ier)
   if (allocated(listcopy)) deallocate(listcopy,stat=ier)
   allocate(indx(nfile),lc2(nfile),listcopy(nfile),stat=ier)
   !
   scanlist: do i = 1, nfile
      tmp = trim(filelist(i))
      !
      if (index(tmp,'vespa').ne.0.or.   &
          index(tmp,'wilma').ne.0.or.   &
          index(tmp,'100khz').ne.0.or.   &
          index(tmp,'1mhz').ne.0.or.   &
          index(tmp,'4mhz').ne.0.or.   &
          index(tmp,'fts').ne.0.or.   &
          index(tmp,'abba').ne.0.or.   &
          index(tmp,'bbc').ne.0.or.   &
          index(tmp,'nbc').ne.0.or.   &
          index(tmp,'holo').ne.0.or.   &
          index(tmp,'continuum').ne.0) then
         newfilename = .true.
      else
         newfilename = .false.
      endif
      !
      if (newfilename) then
         k1 = index(tmp,'-imb')
         do k0 = k1-1, 1, -1
            if (index(tmp(k0:k0),'s').ne.0) exit
         enddo
         read(tmp(k0+1:k1-1),'(I4)') lc1
         read(tmp(k0-8:k0-1),'(I8)') lc0
         lc = iachar(tmp(9:9))
         lc2(i) = dble(lc0-2.d7)+(lc1+lc*1.d-3)*1.d-6
         if (index(tmp,'100khz').ne.0) lc2(i) = lc2(i)+1.d-10
      else
         k1 = index(tmp,'.fits')
         read(tmp(12:k1-1),'(I4)') lc1
         read(tmp(1:4),'(I4)') iyear
         read(tmp(6:7),'(I2.2)') imonth
         read(tmp(9:10),'(I2.2)') iday
         lc0 = iday+100*imonth+10000*iyear
         lc2(i) = dble(lc0-2.d7)+lc1*1.d-6
      endif
   enddo scanlist
   call indexx(nfile,lc2,indx)
   do i = 1, nfile
      listcopy(i) = filelist(indx(i))
   enddo
   do i = 1, nfile
      filelist(i) = listcopy(i)
   enddo
   deallocate(lc2,indx,listcopy,stat=ier)
   !
   call sic_insert(command)
   !
   lastnum = 0
   if (option(9).and.associated(list)) then
      lastnum  = list(size(list))%indx
      lastdate = list(size(list))%dateobs
      write(lastdate,'(a11,i4.4)')   &
        list(size(list))%dateobs(1:10)//'-', list(size(list))%scan
   else
      if (allocated(mylist)) then
         do i = 1, size(mylist)
            call freelist(mylist(i),error)
         enddo
         deallocate(mylist,stat=ier)
      endif
   endif
   !
   !
   call getattributes(directory,option,nfile,filelist(1:nfile),   &
     lastnum,lastdate,nobs,error)
   !
   deallocate(filelist,stat=ier)
   !
   if (.not.allocated(mylist)) then
      if (sic_present(9,0)) then
         call gagout('I-FIND: No new fits data found.'//   &
           ' Current index unchanged.')
      else
         call gagout('I-FIND: No fits data found.')
      endif
      error=.true.
      return
   endif
   call sic_descriptor('list',desc,ex_list)
   if (ex_list) then
      readonly = desc%readonly
      call sic_delvariable('list',.false.,error)
      do i = 1, size(mylist)
         if (i.lt.10) then
            write(name,'(A4,I1)') 'list',i
         else if (i.lt.100) then
            write(name,'(A4,I2)') 'list',i
         else if (i.lt.1000) then
            write(name,'(A4,I3)') 'list',i
         else if (i.lt.10000) then
            write(name,'(A4,I4)') 'list',i
         endif
         call sic_descriptor(trim(name),desc,exist)
         if (exist) call sic_delvariable(trim(name),.false.,error)
      enddo
   endif
   !
   list => mylist(1:nobs)
   if (ex_list) call list_to_sic(readonly,error)
   !
   if (.not.option(11).and.size(list).ne.0)   &
     call exec_program('MIRA\LIST')
   !
   call sic_descriptor('CHOICE',desc,exist)
   if (exist) then
      call sic_descriptor('CHOICE%BACKEND1',desc,exist)
      readonly = desc%readonly
      call sic_delvariable('CHOICE',.false.,error)
      call choice_to_sic(readonly,error)
   endif
   !
   return
!
end subroutine findmira
!
!
subroutine getattributes(directory,option,nfile,filelist,   &
     lastnumfound,lastdatefound,   &
     nobs,error)
!
! Called by subroutine findmira. Opens fits files found for extracting
! information needed to attribute values to structure LIST.
!
!
   use mira
   use gkernel_interfaces
   !
   implicit none
   !
   integer                               :: idxstart, mobs, nfile
   real*4                                :: imbftsve
   character(len = 3)                    :: fitsioerrorcode
   character(len = 11)                   :: version
   character(len = 60), dimension(nfile) :: filelist
   character(len=256)                    :: directory
   logical                               :: error
   logical, dimension(14)                :: option
   logical                               :: newfilename
   !
   integer            :: blocksize, felem, hdutype, i, i1, i2,   &
                         idscan, ier, ife, j, jfe, j0, k, k0, k1, k2,   &
                         k3, l, m, ncfe, nelems, nhdu, nkeys, nobs,   &
                         npart, nrowfrontendtable, nrowbackendtable,   &
                         nidle, nspace, nrow, nsub, readwrite, unit
   integer            :: status = 0
   integer            :: idbandsave, idcol, lastnumfound
   integer            :: firstidx, lastidx
   integer            :: lun
   integer, dimension(:), allocatable :: idband, idpart, part
   integer, dimension(:), allocatable :: Oidband, Oidpart
   real*8             :: integtime,totaltime,scanend,scanstart,   &
                         startmjd,endmjd,t1,t2,sessionstart,dtime,   &
                         t2last
   real, dimension(:), allocatable:: reffreq ! this is a patch for Gabriel
   logical            :: cryoextension, derotextension
   logical, dimension(:), allocatable :: flagvals
   logical, dimension(1)              :: flg
   logical, dimension(:), allocatable :: banddone
   character(len=1)   :: nullstr
   character(len=2)   :: partstring
   character(len=4)   :: scanstring, tmpchoice1, tmpchoice2
   character(len=6)   :: idlestring
   character(len=6)   :: project, previousproject
   character(len=8)   :: backend,procedure,tmpprocedure
   character(len=10)  :: datestring,object
   character(len=12)  :: tmpfrontend,tmprecname
   character(len=20)  :: lastdatefound, mydate, switchmode,   &
                         tmplinename, tmpswitchmode
   character(len=20)  :: scanlabel, lastscanlabel
   character(len=23)  :: scanendstring
   character(len=4),dimension(:), allocatable   :: polar
   character(len=12),dimension(:), allocatable  :: frontend
   character(len=12),dimension(:), allocatable  :: rec1, rec2, rec3
   character(len=20),dimension(:), allocatable  :: linename
   character(len=13)  :: telescope
   character(len=23)  :: date_obs, dateObsBefore
   character(len=60)  :: tmp
   character(len=70)  :: fitsiolink =   &
                         'http://heasarc.gsfc.nasa.gov/docs/'   &
                         //'software/fitsio/c/f_user/node91.html'
   character(len=80)  :: comment,record
   character(len=256) :: filename
   character(len=256), dimension(100) :: idlewarning
   logical            :: anynull, docycle, foundfrontend,   &
                         foundproject, foundtransition
   logical            :: polmode

   !
   nobs = lastnumfound
   mobs = 10*nfile
   if (.not.allocated(mylist)) allocate(mylist(mobs),stat=ier)
   !
   if (option(4)) then
      tmpchoice1 = choice%date_obs(1)
      tmpchoice2 = choice%date_obs(2)
   endif
   date_obs(5:5) = '-'
   date_obs(8:8) = '-'
   !
   integtime = 0.d0
   totaltime = 0.d0
   dtime = 0.d0
   lastscanlabel = 'yyyymmddsXXXX'
   !
   if (option(14)) then
      ier = sic_getlun(lun)
      open(lun,file='.pool',status='unknown')
   endif
   !
   nidle = 0
   fileloop: do i = 1, nfile
      tmp = filelist(i)
      !
      if (index(tmp,'-imb').ne.0) then
         newfilename = .true.
         if (option(4)) then
            k1 = index(tmp,tmpchoice1)
            if (k1.eq.0) k1 = index(tmp,tmpchoice2)
            if (k1.eq.0) cycle
         else
            k1 = index(tmp,'-2')+1
         endif
         read(tmp(k1:k1+3),'(a4)') date_obs(1:4)
         read(tmp(k1+4:k1+5),'(a2)') date_obs(6:7)
         read(tmp(k1+6:k1+7),'(a2)') date_obs(9:10)
         dateObsBefore = date_obs
         !
         if (i.gt.1) lastscanlabel = scanlabel
         k2 = index(tmp,'-imb')-1
         scanlabel = tmp(k1:k2)
         !
         if (option(4)) then
            datestring = trim(choice%date_obs(1))
            if (date_obs(1:10) < datestring) cycle
            datestring = trim(choice%date_obs(2))
            if (date_obs(1:10) > datestring) cycle
         endif
         k2 = k1+9
         k3 = index(tmp,'-imb')-1
         read(tmp(k2:k3),'(i6)') idscan
         if (option(6)) then
            read(choice%scan(1),'(i4)') i1
            read(choice%scan(2),'(i4)') i2
            if (idscan < i1 .or. idscan > i2) cycle
         endif
         backend = tmp(9:k1-2)
         call sic_upper(backend)
         if (option(1)) then
            docycle = .true.
            do j = 1, size(choice%backend)
               if (index(backend,trim(choice%backend(j))).ne.0)   &
                 docycle = .false.
            enddo
            if (docycle) cycle
         endif
         if (index(tmp,'iram30m').ne.0) then
            telescope = 'IRAM-30M'
            if (option(8)) then
               docycle = .true.
               do j = 1, size(choice%telescope)
                  if (index(telescope,trim(choice%telescope(j)))   &
                    .ne.0) docycle = .false.
               enddo
               if (docycle) cycle
            endif
         endif
      else
         newfilename = .false.
      endif
      !
      ! if (index(backend,'ABBA').ne.0) then
      !    call gagout('I-FIND: MIRA does not handle bolometer data.')
      !    cycle
      ! endif
      !
      if (.not.newfilename) then
         j = index(tmp,'.fits')
         read(tmp(12:j-1),'(i4)') idscan
         scanstring = tmp(12:j-1)
         select_scans: if (option(6)) then
            read(choice%scan(1),'(i4)') k1
            read(choice%scan(2),'(i4)') k2
            if (idscan < k1 .or. idscan > k2) cycle
         endif select_scans
      endif
      !
      k1 = index(tmp,'-imb')
      if (k1.ne.0) then
         do k0 = k1-1, 1, -1
            if (index(tmp(k0:k0),'s').ne.0) exit
         enddo
      endif
      !
      if (newfilename.and.option(1)) then
         docycle = .true.
         do j = 1, size(choice%backend)
            if (index(backend,trim(choice%backend(j))).ne.0)   &
              docycle = .false.
         enddo
         if (docycle) cycle
      endif
      !
      filename = trim(directory)//filelist(i)
      !
      status = 0
      readwrite = 0
      call ftgiou(unit,status)
      call ftopen(unit,filename,readwrite,blocksize,status)
      if (status.ne.0) then
         if (.not.option(9)) then
            call gagout('W-FIND: file '//filename)
            call gagout('        Open fits file error.')
         endif
         status = 0
         call ftclos(unit,status)
         call ftfiou(unit,status)
         cycle
      endif
      !
      call ftthdu(unit,nhdu,status)
      if (nhdu.lt.6.or.status.ne.0) then
         call gagout('W-FIND: Less than 6 HDUs in file '//filename)
         status = 0
         call ftclos(unit,status)
         call ftfiou(unit,status)
         cycle
      endif
      !
      !
      call ftmahd(unit,1,hdutype,status)
      !
      if (status /= 0) then
         write(fitsioerrorcode,'(I3)') status
         call gagout('W-FIND: fitsio error code '//fitsioerrorcode//   &
           '. Please consult')
         call gagout('       '//fitsiolink)
         status = 0
         call ftclos(unit,status)
         call ftfiou(unit,status)
         cycle
      endif
      !
      if (i.gt.1) then
         previousproject = project
      else
         previousproject = '000-00'
      endif
      !
      call ftgkys(unit,'PROJID  ',record,comment,status)
      if (status.ne.0) then
         project = '000-00'
         status = 0
      else
         project = record(1:6)
      endif
      call sic_upper(project)
      !
      call ftgkys(unit,'IMBFTSVE',version,comment,status)
      read(version,'(f3.1)') imbftsve
      if (imbftsve.lt.1.2) then
         idxstart = 3
         call ftmahd(unit,2,hdutype,status)
         call ftgkyj(unit,'N_OBS   ',nsub,comment,status)
         call ftmahd(unit,1,hdutype,status)
      else
         idxstart = 4
         call ftgkyj(unit,'N_OBSP  ',nsub,comment,status)
      endif
      if (imbftsve.lt.2) then
         ipc = 1
      else
         ipc = 3
      endif
      if ((nhdu-idxstart)/nsub.lt.3.or.status.ne.0) then
         call gagout('W-FIND: input file '//trim(filename)   &
           //' is incomplete.')
         status = 0
         call ftclos(unit,status)
         call ftfiou(unit,status)
         cycle
      endif
      select_observed: if (.not.newfilename.and.option(4)) then
         call ftgkys(unit,'DATE-OBS',record,comment,status)
         date_obs = record(1:23)
         datestring = trim(choice%date_obs(1))
         if (date_obs(1:10) < datestring) then
            status = 0
            call ftclos(unit,status)
            call ftfiou(unit,status)
            cycle
         endif
         datestring = trim(choice%date_obs(2))
         if (date_obs(1:10) > datestring) then
            status = 0
            call ftclos(unit,status)
            call ftfiou(unit,status)
            cycle
         endif
      endif select_observed
      !
      if (option(9).and.lastnumfound.ne.0) then
         mydate = '                    '
         write(mydate,'(a11,i4.4)') date_obs(1:10)//'-',idscan
         if (trim(mydate) <= trim(lastdatefound)) then
            status = 0
            call ftclos(unit,status)
            call ftfiou(unit,status)
            cycle
         endif
      endif
      !
      if (.not.newfilename) then
         call ftgkys(unit,'TELESCOP',record,comment,status)
         telescope = record(1:13)
         call sic_upper(telescope)
         if (index(telescope,'PICO') /= 0 .or.   &
           index(telescope,'30M') /= 0) telescope = 'IRAM-30M'
         select_telescope: if (option(8)) then
            docycle = .true.
            do j = 1, size(choice%telescope)
               if (index(telescope,trim(choice%telescope(j))).ne.0)   &
                 docycle = .false.
            enddo
            if (docycle) then
               status = 0
               call ftclos(unit,status)
               call ftfiou(unit,status)
               cycle
            endif
         endif select_telescope
      endif
      !
      call ftgkys(unit,'OBJECT  ',record,comment,status)
      if (status.ne.0) then
         object = 'none'
         status = 0
      else
         object = adjustl(record)
      endif
      call sic_upper(object)
      call ftgkyd(unit,'MJD-OBS ',scanstart,comment,status)
      call ftgkys(unit,'DATE-OBS',date_obs,comment,status)
      select_object: if (option(7)) then
         docycle = .true.
         do j = 1, size(choice%object)
            if (index(object,trim(choice%object(j))).ne.0)   &
              docycle = .false.
         enddo
         if (docycle) then
            status = 0
            call ftclos(unit,status)
            call ftfiou(unit,status)
            cycle
         endif
      endif select_object
      !
      !
      if (imbftsve.ge.2.0) then
         call ftmahd(unit,idxstart+2,hdutype,status)
      else
         call ftmahd(unit,idxstart+1,hdutype,status)
      endif
      !
      if (status /= 0) then
         write(fitsioerrorcode,'(I3)') status
         call gagout('W-FIND: fitsio error code '//fitsioerrorcode//   &
           '. Please consult')
         call gagout('       '//fitsiolink)
         status = 0
         call ftclos(unit,status)
         call ftfiou(unit,status)
         cycle
      endif
      !
      call ftgkys(unit,'OBSTYPE ',record,comment,status)
      procedure = record(1:8)
      select_procedure: if (option(5)) then
         docycle = .true.
         tmpprocedure = procedure
         call sic_upper(tmpprocedure)
         do j = 1, size(choice%procedure)
            if (index(tmpprocedure,trim(choice%procedure(j))).ne.0)   &
              docycle = .false.
         enddo
         if (docycle) then
            status = 0
            call ftclos(unit,status)
            call ftfiou(unit,status)
            cycle
         endif
      endif select_procedure
      !
      !
      !
      nullstr = ' '
      !
      !
      if (.not.newfilename) then
         if (imbftsve.lt.1.2) then
            call ftmahd(unit,2,hdutype,status)
         else
            call ftmahd(unit,4,hdutype,status)
         endif
         !
         if (status /= 0) then
            write(fitsioerrorcode,'(I3)') status
            call gagout('W-FIND: fitsio error code '//fitsioerrorcode   &
              //'. Please consult')
            call gagout('       '//fitsiolink)
            status = 0
            call ftclos(unit,status)
            call ftfiou(unit,status)
            cycle
         endif
         !
         call ftgcvs(unit,1,2,1,1,nullstr,tmp,anynull,status)
         backend = tmp(1:8)
         call sic_upper(backend)
         select_backend: if (option(1)) then
            docycle = .true.
            do j = 1, size(choice%backend)
               if (index(backend,trim(choice%backend(j))).ne.0)   &
                 docycle = .false.
            enddo
            if (docycle) then
               status= 0
               call ftclos(unit,status)
               call ftfiou(unit,status)
               cycle
            endif
         endif select_backend
      endif
      !
      !
      if (imbftsve.lt.1.2) then
         call ftmahd(unit,2,hdutype,status)
      else
         call ftmahd(unit,4,hdutype,status)
      endif
      call ftgkyj(unit,'NAXIS2',nrowbackendtable,comment,status)
      if (nrowbackendtable.eq.0.and.backend.ne."ABBA") then
         call gagout(' E-FIND: scan table extension of file')
         call gagout(" "//trim(filename))
         call gagout(" is empty.")
         call gagout(' ')
         status = 0
         call ftclos(unit,status)
         call ftfiou(unit,status)
         cycle
      endif
      if (backend.eq."ABBA") goto 10
      if (allocated(rec1)) deallocate(rec1,stat=ier)
      if (allocated(rec2)) deallocate(rec2,stat=ier)
      if (allocated(rec3)) deallocate(rec3,stat=ier)
      if (allocated(polar)) deallocate(polar,stat=ier)
      if (allocated(idband)) deallocate(idband,stat=ier)
      if (allocated(Oidband)) deallocate(Oidband,stat=ier)
      if (allocated(idpart)) deallocate(idpart,stat=ier)
      if (allocated(Oidpart)) deallocate(Oidpart,stat=ier)
      if (allocated(flagvals)) deallocate(flagvals,stat=ier)
      allocate(idband(nrowbackendtable),flagvals(nrowbackendtable),   &
               rec1(nrowbackendtable),rec2(nrowbackendtable),   &
               rec3(nrowbackendtable),polar(nrowbackendtable),   &
               Oidpart(nrowbackendtable),Oidband(nrowbackendtable),  &
               idpart(nrowbackendtable),stat=ier)
      !
      if (imbftsve.ge.2.0) then
         call ftgcno(unit,.false.,'PART',idcol,status)
         call ftgcfj(unit,idcol,1,1,nrowbackendtable,Oidband,flagvals,   &
                     anynull,status)
         call ftgcno(unit,.false.,'BAND',idcol,status)
         call ftgcvs(unit,idcol,1,1,nrowbackendtable,nullstr,rec1,   &
                  anynull,status)
      !
      else
      !
         call ftgcno(unit,.false.,'BAND',idcol,status)
         call ftgcfj(unit,idcol,1,1,nrowbackendtable,Oidband,flagvals,   &
                     anynull,status)
         call ftgcno(unit,.false.,'REC1',idcol,status)
         call ftgcvs(unit,idcol,1,1,nrowbackendtable,nullstr,rec1,   &
                  anynull,status)
      !
         call ftgcno(unit,.false.,'REC2',idcol,status)
         call ftgcvs(unit,idcol,1,1,nrowbackendtable,nullstr,rec2,   &
                  anynull,status)
      !
      endif
      !
      !!print *,'Ori.idband ',Oidband
      call renBand(nrowbackendtable,Oidband,idband)
      !!print *,'New idband ',idband
      !
      call ftgcno(unit,.false.,'POLAR',idcol,status)
      call ftgcvs(unit,idcol,1,1,nrowbackendtable,nullstr,polar,   &
                  anynull,status)
      !
      polmode = .false.
      do j = 1, nrowbackendtable
         call sic_upper(polar(j))
!
! This is a patch for Gabriel.
!
         tmpfrontend = rec1(j)
         call sic_upper(tmpfrontend)
         if (tmpfrontend(1:1).eq.'E') call sic_upper(rec1(j))
         if (index(backend,'W').ne.0.and.tmpfrontend(1:1).eq.'E') then
            if (j.eq.1) then
               allocate(reffreq(nrowbackendtable),stat=ier)
               call ftgcno(unit,.false.,'REFFREQ',idcol,status)
               call ftgcfe(unit,idcol,1,1,nrowbackendtable,reffreq,flagvals,   &
                     anynull,status)
            endif
            if (reffreq(j).ge.-11555.and.reffreq(j).lt.-7840) then
               rec1(j) = tmpfrontend(1:2)//tmpfrontend(3:3)//'LO'
            else if (reffreq(j).ge.-7840.and.reffreq(j).le.-4125) then
               rec1(j) = tmpfrontend(1:2)//tmpfrontend(3:3)//'LI'
            else if (reffreq(j).ge.4125.and.reffreq(j).lt.7840) then
               rec1(j) = tmpfrontend(1:2)//tmpfrontend(3:3)//'UI'
            else if (reffreq(j).ge.7840.and.reffreq(j).le.11555) then
               rec1(j) = tmpfrontend(1:2)//tmpfrontend(3:3)//'UO'
            endif
            if (j.eq.nrowbackendtable) deallocate(reffreq)
         endif
!
! end of patch for Gabriel
!
         if (tmpfrontend(1:1).eq.'E') call sic_upper(rec2(j))
         if ((index(polar(j),'REAL').ne.0                            &
              .or.index(polar(j),'IMAG').ne.0                        &
             ).and.index(backend,'VESPA').ne.0) then
                polmode = .true.
                if (imbftsve.ge.2.0) then
                   tmpfrontend = rec1(j)
                   if (index(rec1(j),'V').ne.0) then
                      write(tmpfrontend(3:3),'(1HH)')
                   else if (index(rec1(j),'H').ne.0) then
                      write(tmpfrontend(3:3),'(1HV)')
                   endif
                   rec2(j) = tmpfrontend
                endif
         endif
         call sic_upper(rec1(j))
         call sic_upper(rec2(j))
      enddo
      !
      if (polmode) then
!
! An ugly feature: the RECEIVER and BAND columns in imbfits here mean
! REC1 and REC2 (i.e. the receivers that are cross-correlated).
!
         if (imbftsve.ge.2.0) then
            rec2(1:nrowbackendtable) = rec1(1:nrowbackendtable)
            call ftgcno(unit,.false.,'RECEIVER',idcol,status)
            call ftgcvs(unit,idcol,1,1,nrowbackendtable,nullstr,rec2,   &
                        anynull,status)
         endif

! end of feature        
         do j = 1, nrowbackendtable
            if (imbftsve.ge.2.0.and.index(rec1(j),'HERA').eq.0) then
               if (index(rec1(j),'V').ne.0.and.index(rec2(j),'H').ne.0)   &
               then
                  rec3(j) = rec1(j)//rec2(j)
               else if (index(rec2(j),'V').ne.0.and.index(rec1(j),'H')   &
                        .ne.0) then
                  rec3(j) = rec2(j)//rec1(j)
               else if (rec1(j).eq.rec2(j)) then
                  rec3(j) = rec1(j)//rec2(j)
               endif
            else
               if (index(rec1(j),'A').ne.0.and.index(rec2(j),'B').ne.0)   &
               then
                  rec3(j) = rec1(j)//rec2(j)
               else if (index(rec2(j),'A').ne.0.and.index(rec1(j),'B')   &
                        .ne.0) then
                  rec3(j) = rec2(j)//rec1(j)
               else if (index(rec1(j),'C').ne.0.and.index(rec2(j),'D')   &
                        .ne.0) then
                  rec3(j) = rec1(j)//rec2(j)
               else if (index(rec2(j),'C').ne.0.and.index(rec1(j),'D')   &
                        .ne.0) then
                  rec3(j) = rec2(j)//rec1(j)
               else if (rec1(j).eq.rec2(j)) then
                  rec3(j) = rec1(j)//rec2(j)
               endif
            endif
         enddo
      endif
      !
      if (polmode) then
         do j = 2, nrowbackendtable
            do l = 1, j-1
               if ((rec3(j).ne.rec3(l).or.polar(j).ne.polar(l))   &
                 .and.idband(j).eq.idband(l)) then
                  idbandsave = idband(j)
                  idband(j) = maxval(idband)+1
                  do k = j+1, nrowbackendtable
                     if (rec3(k).eq.rec3(j).and.polar(k).eq.polar(j)   &
                       .and.idband(k).eq.idbandsave)   &
                       idband(k) = idband(j)
                  enddo
               endif
            enddo
         enddo
      else
         do j = 2, nrowbackendtable
            do l = 1, j-1
               if (rec1(j).ne.rec1(l).and.idband(j).eq.idband(l)) then
                  idbandsave = idband(j)
                  idband(j) = maxval(idband)+1
                  do k = j+1, nrowbackendtable
                     if (rec1(k).eq.rec1(j)   &
                       .and.idband(k).eq.idbandsave)   &
                       idband(k) = idband(j)
                  enddo
               endif
            enddo
         enddo
      endif
      !
      if (imbftsve.ge.2.0) then
         call ftgcno(unit,.false.,'PART',idcol,status)
      else
         call ftgcno(unit,.false.,'BAND',idcol,status)
      endif
      call ftgcfj(unit,idcol,1,1,nrowbackendtable,Oidpart,flagvals,   &
                  anynull,status)
      !
      ncfe = maxval(idband)
      k1 = ncfe
      do k = 1, ncfe
         if (count(idband.eq.k).eq.0) k1 = k1-1
      enddo
      do k = 1, ncfe
         if (count(idband.eq.k).eq.0) then
            do j = 1, nrowbackendtable
               if (idband(j).gt.k) idband(j) = idband(j)-1
            enddo
         endif
      enddo
      ncfe = k1
      !
      !!print *,'Odpart ',Oidpart
      call renBand(nrowbackendtable,Oidpart,idpart)
      !!print *,'idpart ',idpart

      npart = maxval(idpart)
      k1 = npart
      do k = 1, npart
         if (count(idpart.eq.k).eq.0) k1 = k1-1
      enddo
      npart = k1
      !
      if (allocated(frontend)) deallocate(frontend,stat=ier)
      allocate (frontend(nrowbackendtable),stat=ier)
      if (allocated(linename)) deallocate(linename,stat=ier)
      allocate (linename(nrowbackendtable),stat=ier)
      if (allocated(part)) deallocate(part,stat=ier)
      allocate (part(nrowbackendtable),stat=ier)
      if (allocated(banddone)) deallocate(banddone,stat=ier)
      allocate (banddone(nrowbackendtable),stat=ier)
      !
      !
      call ftmahd(unit,3,hdutype,status)
      if (status /= 0) then
         write(fitsioerrorcode,'(I3)') status
         call gagout('W-FIND: fitsio error code '//fitsioerrorcode//   &
                     '. Please consult')
         call gagout('       '//fitsiolink)
         status = 0
         call ftclos(unit,status)
         call ftfiou(unit,status)
         cycle
      endif
      call ftgkyj(unit,'NAXIS2',nrowfrontendtable,comment,status)
      call ftgcno(unit,.false.,'RECNAME',idcol,status)
      !
      k1 = minval(idband)
      if (k1.ne.1) then
         do k = 1, nrowbackendtable
            idband(k) = idband(k)-k1+1
         enddo
      endif
      !
      banddone(1:ncfe) = .false.
      do jfe = 1, nrowbackendtable
         k = idband(jfe)
         if (banddone(k)) cycle
         do ife = 1, nrowfrontendtable
            call ftgcvs(unit,idcol,ife,1,1,nullstr,tmpfrontend,   &
                        anynull,status)
            call sic_upper(tmpfrontend)
            call ftgcvs(unit,idcol+1,ife,1,1,nullstr,tmplinename,   &
                        anynull,status)
            if (tmpfrontend.eq.rec1(jfe).and.tmpfrontend(1:1).ne.'E') then
               if (polmode) then
                  if (index(polar(jfe),'REAL').ne.0) then
                     tmpfrontend(1:1) = 'R'
                  else if (index(polar(jfe),'IMAG').ne.0) then
                     tmpfrontend(1:1) = 'I'
                  else if (index(polar(jfe),'AXIS').ne.0) then
                     if (index(rec1(jfe),'A').ne.0.or.   &
                         index(rec1(jfe),'C').ne.0) then
                        tmpfrontend(1:1) = 'V'
                     else if (index(rec1(jfe),'B').ne.0.or.   &
                              index(rec1(jfe),'D').ne.0) then
                        tmpfrontend(1:1) = 'H'
                     endif
                  endif
               endif
               frontend(k) = tmpfrontend
               part(k) = idpart(jfe)
               linename(k) = tmplinename
               banddone(idband(jfe)) = .true.
               exit
            else if (tmpfrontend(1:1).eq.'E') then
               tmprecname = rec1(jfe)
               call sic_upper(tmprecname)
               if (polmode) then
                  tmpfrontend = rec1(jfe)
                  if (index(polar(jfe),'REAL').ne.0) then
                     tmpfrontend(3:3) = 'R'
                  else if (index(polar(jfe),'IMAG').ne.0) then
                     tmpfrontend(3:3) = 'I'
                  endif
               endif
               if (tmprecname(1:2).eq.tmpfrontend(1:2)) then
                  if (polmode) then
                     frontend(k) = tmpfrontend
                  else
                     frontend(k) = rec1(jfe)
                  endif
                  part(k) = idpart(jfe)
                  linename(k) = tmplinename
                  banddone(idband(jfe)) = .true.
                  exit
               endif
            endif
         enddo
      enddo
      !
      deallocate(banddone,stat=ier)
      !
10    continue
      if (imbftsve.lt.1.2) then
         call ftmahd(unit,3,hdutype,status)
         call ftgkys(unit,'SWTCHMOD',switchmode,comment,status)
      else
         call ftmahd(unit,2,hdutype,status)
         call ftgkys(unit,'SWTCHMOD',switchmode,comment,status)
      endif
      if (backend.eq."ABBA") then
         ncfe = 1
         if (allocated(frontend)) deallocate(frontend,stat=ier)
         allocate (frontend(ncfe),stat=ier)
         frontend(1) = "MAMBO"
         if (allocated(linename)) deallocate(linename,stat=ier)
         allocate (linename(ncfe),stat=ier)
         linename = " "
         npart = 1
      endif
      !
      select_switchmode: if (option(10)) then
         tmpswitchmode = switchmode
         call sic_upper(tmpswitchmode)
         docycle = .true.
         do j = 1, size(choice%switchmode)
            if (index(tmpswitchmode,trim(choice%switchmode(j))).ne.0)   &
              docycle = .false.
         enddo
         if (docycle) then
            status = 0
            call ftclos(unit,status)
            call ftfiou(unit,status)
            cycle
         endif
      end if select_switchmode
      !
      select_frontend: if (option(2)) then
         do j = 1, size(choice%frontend)
            if (index(choice%frontend(j),'HER').ne.0)   &
              choice%frontend = 'HER     '
         enddo
         foundfrontend = .false.
         do ife = 1, ncfe
            tmpfrontend = frontend(ife)
            call sic_upper(tmpfrontend)
            do j = 1, size(choice%frontend)
               if (index(tmpfrontend,trim(choice%frontend(j))).ne.0)   &
                 foundfrontend = .true.
            enddo
         enddo
         if (.not.foundfrontend) then
            status = 0
            call ftclos(unit,status)
            call ftfiou(unit,status)
            cycle
         endif
      end if select_frontend
      !
      select_transition: if (option(3)) then
         foundtransition = .false.
         do ife = 1, ncfe
            do j = 1, size(choice%transition)
               if (index(linename(ife),trim(choice%transition(j)))   &
                 .ne.0) foundtransition = .true.
            enddo
         enddo
         if (.not.foundtransition) then
            status = 0
            call ftclos(unit,status)
            call ftfiou(unit,status)
            cycle
         endif
      end if select_transition
      !
      select_project: if (option(13)) then
         foundproject = .false.
         do j = 1, size(choice%project)
            if (index(project,trim(choice%project(j)))   &
              .ne.0) foundproject = .true.
         enddo
         if (.not.foundproject) then
            status = 0
            call ftclos(unit,status)
            call ftfiou(unit,status)
            cycle
         endif
      end if select_project
      !
      !
      nobs = nobs+1
      !
      !
      if (option(14).and.scanlabel.ne.lastscanlabel)   &
        then
         cryoextension = .false.
         derotextension = .false.
         call ftmnhd(unit,hdutype,'IMBF-hera-cryot',0,status)
         if (status.eq.0) then
            cryoextension = .true.
         else
            status = 0
         endif
         call ftmnhd(unit,hdutype,'IMBF-hera-derot',0,status)
         if (status.eq.0) then
            derotextension = .true.
         else
            status = 0
         endif
         !
         if (.not.cryoextension.and..not.derotextension) then
            firstidx = idxstart+3
            lastidx = firstidx+(nsub-1)*3
         else if (.not.derotextension) then
            firstidx = idxstart+4
            lastidx = firstidx+(nsub-1)*4
         else
            firstidx = idxstart+5
            lastidx = firstidx+(nsub-1)*5
         endif
         call ftmahd(unit,firstidx,hdutype,status)
         call ftgcno(unit,.false.,'MJD',idcol,status)
         call ftgcfd(unit,idcol,1,1,1,startmjd,flg,anynull,status)
         call ftmahd(unit,lastidx,hdutype,status)
         call ftgkyj(unit,'NAXIS2',nrow,comment,status)
         call ftgkys(unit,'DATE-END',scanendstring,comment,status)
         call ftgcfd(unit,idcol,nrow,1,1,endmjd,flg,anynull,status)
         call dateobstomjd(scanendstring,scanend,status)
         t1 = min(scanstart,startmjd)
         t2 = max(scanend,endmjd)
         write(lun,'(f12.6,1x,f12.6,1x,a20,1x,a6)')   &
           t1,t2,scanlabel,project
         integtime = integtime+(t2-t1)
         if (previousproject.ne.project.or.nobs.eq.1) then
            sessionstart = t1
            totaltime = totaltime+dtime
         else
            dtime = t2-sessionstart
            if (t1-t2last.gt.1./48.) then
               write(idlestring,'(f6.1)') (t1-t2last)*24.
               nidle = nidle+1
               idlewarning(nidle) = trim(idlestring)   &
                 //'h idle time before scan '//trim(scanlabel)//'.'
            endif
         endif
         t2last = t2
      !
      endif
      !
      ! if (.not.option(9).and..not.option(11))
      ! &   write(*,100) idScan, object, procedure, switchMode, backend,
      ! &                date_obs, nobs
      if (dateObsBefore(1:10).ne.date_obs(1:10)) date_obs  &
      = dateObsBefore(1:10)//'T23:59:59.999'
      mylist(nobs)%indx      = nobs
      mylist(nobs)%scan      = idscan
      mylist(nobs)%object    = object
      mylist(nobs)%telescope = telescope
      mylist(nobs)%procedure = procedure
      mylist(nobs)%switchmode= switchmode
      mylist(nobs)%dateobs   = date_obs
      mylist(nobs)%ncfe      = ncfe
      mylist(nobs)%project   = project
      allocate(mylist(nobs)%transition(nrowbackendtable),stat=ier)
      allocate(mylist(nobs)%frontend(nrowbackendtable),stat=ier)
      allocate(mylist(nobs)%backend(nrowbackendtable),stat=ier)
      mylist(nobs)%transition(1:ncfe) = linename(1:ncfe)
      mylist(nobs)%frontend(1:ncfe) = frontend(1:ncfe)
      !
      if (npart.gt.1) then
         do k = 1, ncfe
            write(partstring,'(I2)') part(k)
            mylist(nobs)%backend(k) = trim(backend(1:5))//'/'   &
                                    //trim(adjustl(partstring))
         enddo
      else
         do k = 1, ncfe
            mylist(nobs)%backend(k) = trim(backend)
         enddo
      endif
      !
      status = 0
      call ftclos(unit,status)
      call ftfiou(unit,status)
   !
   enddo fileloop
   !
   totaltime = totaltime+dtime
   !
   if (option(14)) then
      close(lun)
      ier = gag_frelun(lun)
      ier = sic_getlun(lun)
      open(lun,file='.stat',status='unknown')
      write(lun,'(f6.1)') int(240*integtime)/10.
      write(lun,'(f6.1)') int(240*totaltime)/10.
      do i = 1, nidle
         write(lun,*) trim(idlewarning(i))
      enddo
      close(lun)
      ier = gag_frelun(lun)
   endif
   !
   if (nobs.eq.lastnumfound) then
      if (sic_present(9,0)) then
         call gagout('I-FIND: No new fits data found.'//   &
                     ' Current index unchanged.')
      else
         call gagout('I-FIND: No fits data found.')
      endif
      error=.true.
   endif
   !
   deallocate(flagvals,idband,idpart,rec1,stat=ier)
   !
100 format(t1,i4,t6,a10,t17,a8,t31,a16,t50,a5,t58,a10,t70,i4)
   !
   return
!
end subroutine getattributes
!
!
!
subroutine indexx(n,arr,indx)
!
! Called by subroutines findmira, getfits, getfitsNew, mira_list, 
! solve_pointing_mira, syncDataNew.
!
! Returns an index list "indx" for sorting array "arr" in ascending order.
! (i.e., indx(1) yields the element number of arr with the smallest value,
!  indx(n) that of the element of arr with the largest value.
!
! Input:        n       integer         number of elements of vector arr
!               arr     double          n-element vector to be sorted
!               indx    integer         n-element vector with sort index
!
! Source code is from Numerical Recipes.
!

   integer n,indx(n),m,nstack
   real*8 arr(n)
   parameter (m=7,nstack=50)
   integer i,indxt,ir,itemp,j,jstack,k,l,istack(nstack)
   real*8 a
   do 11 j=1,n
      indx(j)=j
11 continue
   jstack=0
   l=1
   ir=n
1  if (ir-l.lt.m) then
      do 13 j=l+1,ir
         indxt=indx(j)
         a=arr(indxt)
         do 12 i=j-1,l,-1
            if (arr(indx(i)).le.a)goto 2
            indx(i+1)=indx(i)
12       continue
         i=l-1
2        indx(i+1)=indxt
13    continue
      if (jstack.eq.0)return
      ir=istack(jstack)
      l=istack(jstack-1)
      jstack=jstack-2
   else
      k=(l+ir)/2
      itemp=indx(k)
      indx(k)=indx(l+1)
      indx(l+1)=itemp
      if (arr(indx(l)).gt.arr(indx(ir))) then
         itemp=indx(l)
         indx(l)=indx(ir)
         indx(ir)=itemp
      endif
      if (arr(indx(l+1)).gt.arr(indx(ir))) then
         itemp=indx(l+1)
         indx(l+1)=indx(ir)
         indx(ir)=itemp
      endif
      if (arr(indx(l)).gt.arr(indx(l+1))) then
         itemp=indx(l)
         indx(l)=indx(l+1)
         indx(l+1)=itemp
      endif
      i=l+1
      j=ir
      indxt=indx(l+1)
      a=arr(indxt)
3     continue
      i=i+1
      if (arr(indx(i)).lt.a)goto 3
4     continue
      j=j-1
      if (arr(indx(j)).gt.a)goto 4
      if (j.lt.i)goto 5
      itemp=indx(i)
      indx(i)=indx(j)
      indx(j)=itemp
      goto 3
5     indx(l+1)=indx(j)
      indx(j)=indxt
      jstack=jstack+2
      if (jstack.gt.nstack)pause 'NSTACK too small in indexx'
      if (ir-i+1.ge.j-l) then
         istack(jstack)=ir
         istack(jstack-1)=i
         ir=j-1
      else
         istack(jstack)=j-1
         istack(jstack-1)=l
         l=i
      endif
   endif
   goto 1
end subroutine indexx
!
!
!
subroutine rank(n,indx,irank)
!
! Called by subroutines getfits and getfitsNew.
! Yields a ranking list, i.e. says which position a given element of
! vector "arr" has in a listing of elementes in ascending order.
!
! Input:        n       integer         number of elements of vector indx
!               indx    integer         index list for sorting in ascending order
!                                       (output from subroutine indexx)
!               irank   integer         n-element vector  
!
! Source code is from Numerical Recipes.
!
   integer n,indx(n),irank(n)
   integer j
   do 11 j=1,n
      irank(indx(j))=j
11 continue
   return
end subroutine rank

subroutine renBand(n,indx,outdx)
!
! If band numbers are non-consecutive
! rename them consecutively
!
    integer n, indx(n), outdx(n)
    integer j, jj, nnn
    logical found
!
    nnn=1
    do 10 j = 1, n
       found=.false.
       do 11 jj = 1,n
          if (indx(jj) .eq. j) then
             outdx(jj)=nnn
             found=.true.
          endif
 11    continue
    if (found) nnn=nnn+1
!
 10 continue 
    return
end subroutine renBand
