subroutine directory(line,error)
!
! Defines the directory containing the imbfits raw data.
! Checks whether the directory exists and whether there are
! data in it.
!
! Called by: load_mira (in load.f90, MIRA command "file in")
! Input: command line from Mira (see "help file")
! Output: error flag.
!
!
   use mira
   use gildas_def
   use gkernel_types
   use gkernel_interfaces
   !
   implicit none
   !
   integer                           :: ier, lc, nc
   type(sic_descriptor_t)            :: desc
   character(len = *)                :: line
   character                         :: ch*256,file*256,out*256
   character(len = 256), allocatable :: tmpstr(:)
   character(len = 256)              :: comm
   logical                           :: case_both, case_in,   &
                                        case_new, case_out,   &
                                        error, ex
   integer(kind=8)                   :: class_idx_size
   !
   !
   if (.not.sic_present(0,1).and.sic_present(3,0)) then
      file = 'INPUT_FILE:'
      call sic_parsef(file,ch,' ',' ')
      if (trim(ch).eq.'INPUT_FILE:') then
         call gagout('No input file opened.')
      else
         call gagout('Input file: '//trim(ch))
      endif
      file = 'OUTPUT_FILE:'
      call sic_parsef(file,ch,' ',' ')
      if (trim(ch).eq.'OUTPUT_FILE:') then
         call gagout('No output file opened.')
      else
         call gagout('Output file: '//trim(ch))
      endif
      error = .false.
      return
   endif
   call sic_ch (line,0,1,ch,nc,.true.,error)
   case_in   = .false.
   case_out  = .false.
   case_both = .false.
   case_in   = (index(ch,'IN').ne.0.or.index(ch,'in').ne.0)
   case_out  = (index(ch,'OUT').ne.0.or.index(ch,'out').ne.0)
   case_both = (index(ch,'BOTH').ne.0.or.index(ch,'both').ne.0)
   if (.not.case_in.and..not.case_out.and..not.case_both) then
      call gagout ('E-FILE, IN, OUT or BOTH, please.')
      error = .true.
      return
   endif
   if (.not.sic_present(0,2)) then
      call gagout ('E-FILE, no default provided for directory')
      error = .true.
      return
   endif
   if (error) return
   if (case_in) then
      call sic_ch (line,0,2,ch,nc,.false.,error)
      ier = sic_getlog(ch)
      ier = sic_setlog('INPUT_FILE:',ch)
      call gag_directory(char(0),ch,tmpstr,nc,error)
      if (nc.le.0) then
         call gagout('E-FILE, input file does not exist or is empty.')
         error = .true.
         return
      endif
   else if (case_out) then
      if (sic_present(0,3)) then
         call sic_ch (line,0,3,ch,nc,.true.,error)
         call sic_upper(ch)
         case_new = index(ch,'NEW').ne.0
      else
         case_new = .false.
      endif
      call sic_ch (line,0,2,ch,nc,.true.,error)
      ier = sic_getlog(ch)
      if (sic_present(2,0)) then
         classoption = .false.
         comm = 'sic find '//ch
         call sic_descriptor('DIR',desc,ex)
         if (ex) call sic_delvariable('DIR',.false.,error)
         call exec_program(comm)
         call sic_get_inte('DIR%NFILE',nc,error)
         if (nc.lt.0) then
            ex = .false.
         else
            ex = .true.
         endif
      else
         classoption = .true.
         if (index(ch,'.').eq.0) ch = trim(ch)//'.30m'
         inquire(file=ch,exist=ex)
      endif
      ier = sic_setlog('OUTPUT_FILE:',ch)
      if (case_new) then
         if (ex) then
            call gagout('E-FILE, file already exists.')
            error = .true.
            return
         endif
      else if (.not.ex) then
         call gagout('E-FILE, file does not exist, use:')
         call gagout('       FILE OUT myFile NEW')
         error = .true.
         return
      endif
      out = 'OUTPUT_FILE:'
      ier = sic_getlog(out)
      lc = lenc(out)
      out = out(1:lc-1)//char(0)
      if (classoption) then
         obsnumber = 1
         ier = sic_getlog('CLASS_IDX_SIZE',class_idx_size)
         if (ier.eq.0) then
            call class_fileout_open(out,case_new,.false.,class_idx_size,.true.,error)
         else
            error = .true.
         endif
      else
         call writembfits(error)
      endif
      if (error) return
   else if (case_both) then
      ier = sic_getlog(ch)
      ier = sic_setlog('INPUT_FILE:',ch)
      ier = sic_setlog('OUTPUT_FILE:',ch)
   endif
   if (error) return
end subroutine directory
