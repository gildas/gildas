SUBROUTINE DESPIKE(LINE,ERROR)
!
! Called by subroutine RUN_MIRA (source code is in mira/lib/run.f90).
! Used for despiking of data. See online help or user's guide for options.
!
! Input: command line from interactive MIRA session.
! Output: error flag.
!
  USE mira
  USE gildas_def
  use gkernel_types
  use gkernel_interfaces

  IMPLICIT NONE

  CHARACTER(LEN = *)                   :: line
  LOGICAL                              :: error
  INTEGER                              :: i, ibd, ier, ifb, ipix, iter, j, k,&
 &                                        l, nchan, ndump, niter, nphases,   &
 &                                        npix
  INTEGER                              :: spikesFound
  type(sic_descriptor_t)               :: desc
  INTEGER, DIMENSION(1)                :: spikeMaxLoc, spikeMinLoc
  REAL*4                               :: spikeMax, spikeMin
  REAL*4                               :: median, rms, threshold
  REAL*4, DIMENSION(:), ALLOCATABLE    :: tmpVec
  REAL*8                               :: bval
  REAL*8                               :: vChan
  REAL*8, DIMENSION(2)                 :: window
  CHARACTER(LEN=20)                    :: tmpString
  CHARACTER(LEN=20), PARAMETER :: nullString='                    '
  CHARACTER(LEN=23)                    :: name, tmpBackend
  CHARACTER(LEN=32)                    :: tmpFrontend
  LOGICAL                              :: exist, ex_reduce, noSky,           &
 &                                        readonly
  LOGICAL                              :: contMode, doWindow, specMode
  LOGICAL, DIMENSION(:), ALLOCATABLE   :: mask
  LOGICAL, DIMENSION(:,:), ALLOCATABLE :: mask2, mask3
  
  if (.not.SIC_PRESENT(0,1)) then
     call GAGOUT('W-DESPIKE: despikes first backend found.')
     ifb = 1
  else
     call SIC_I4(line,0,1,ifb,.false.,error)
     if (ifb.gt.scan%header%nfebe.or.ifb.le.0) then
        call GAGOUT('E-DESPIKE: Invalid backend specification.')
        error = .true.
        return
     endif
  endif

  tmpFrontend = febe(ifb)%header%febe
  call sic_upper(tmpFrontend)

  if (sic_present(1,0)) then
     call sic_i4(line,1,1,ipix,.false.,error)
  else if (index(tmpFrontend,'HERA').eq.0) then
     ipix = 1
  else
     ipix = 5
     if (febe(ifb)%header%febefeed.gt.1)                                   &
 &       call gagout('W-DESPIKE: no pixel specified. Defaults to 5.')
  endif

  if (sic_present(2,0)) then
     call sic_i4(line,2,1,niter,.false.,error)
  else
     niter = 10
     call gagout('W-DESPIKE: defaults to 10 iterations.')
  endif

  ndump = maxval(data(ifb)%data%integnum)
  nphases = febe(ifb)%header%nphases
  spikesFound = 0
  tmpBackend = scan%data%febe(ifb)
  call sic_upper(tmpBackend)

  if (index(tmpBackend,'CONT').ne.0) then
     contMode = .true.
     specMode = .false.
  else
     contMode = .false.
     specMode = .true.
  endif
  
  if (sic_present(3,0)) then
     if (contMode) then
        call gagout('W-DESPIKE: option WINDOW ignored '                        &
 &                  //'(only for spectral mode).')
     else if (.not.reduce(ifb)%calibration_done(3)) then
        call gagout('W-DESPIKE: option WINDOW ignored '                        &
 &                  //'(use only after CAL /OFF.')
     else if (.not.sic_present(3,1).or..not.sic_present(3,2)) then
        call gagout('W-DESPIKE: option WINDOW ignored (needs two arguments)')
     else
        call SIC_R8(LINE,3,1,WINDOW(1),.FALSE.,ERROR)
        call SIC_R8(LINE,3,1,WINDOW(1),.FALSE.,ERROR)
        doWindow = .true.
     endif
  endif

  if (sic_present(4,0)) then
     if (.not.sic_present(4,1)) then
        call gagout('W-DESPIKE: no threshold provided. Set to default value 5.')
        threshold = 5
     else
        call SIC_R4(LINE,4,1,THRESHOLD,.FALSE.,ERROR)
     endif
  else
     threshold = 5.0
  endif

  if (.not.reduce(ifb)%calibration_done(1).and.                                &
 &    .not.reduce(ifb)%calibration_done(3)) then
     bval = blankingRaw
  else
     bval = blankingRed
  endif

  if (.not.reduce(ifb)%calibration_done(3)) then

     iter_loop1: do iter = 1, niter 

        phase_loop1: do i = 1, nphases 

           if (specMode) then

              baseband_loop1: do ibd = 1, febe(ifb)%header%febeband

                 nchan = array(ifb)%header(ibd)%usedchan
                 if (allocated(mask)) deallocate(mask,stat=ier)
                 allocate(mask(nchan),stat=ier)
                 if (allocated(tmpVec)) deallocate(tmpVec,stat=ier)
                 allocate(tmpVec(nchan),stat=ier)

                 record_loop1: do k = 1, ndump 

                    l = 0

                    do j = 1, array(ifb)%header(ibd)%usedchan
                       if (array(ifb)%data(ibd)%data(ipix,j,k,i).ne.bval) then
                          l = l+1
                          tmpVec(l) = array(ifb)%data(ibd)%data(ipix,j,k,i)
                       endif
                    enddo

                    call COMP_R4_MEDIAN(tmpVec(1:l),l,0.,-1.,median)
                    call COMP_R4_RMS(tmpVec(1:l),l,0.,-1.,rms)

                    mask = array(ifb)%data(ibd)%data(ipix,1:nchan,k,i).ne.bval

                    spikeMax                                                 &
 &                  = MAXVAL(array(ifb)%data(ibd)%data(ipix,1:nchan,k,i),mask)
                    spikeMaxLoc                                              &
 &                  = MAXLOC(array(ifb)%data(ibd)%data(ipix,1:nchan,k,i),mask)

                    spikeMin                                                 &
 &                  = MINVAL(array(ifb)%data(ibd)%data(ipix,1:nchan,k,i),mask)
                    spikeMinLoc                                              &
 &                  = MINLOC(array(ifb)%data(ibd)%data(ipix,1:nchan,k,i),mask)

                    if (spikeMax-median.gt.threshold*rms) then
                       array(ifb)%data(ibd)%data(ipix,spikeMaxLoc(1),k,i) =bval
                       spikesFound = spikesFound+1
                    endif

                    if (median-spikeMin.gt.threshold*rms) then
                       array(ifb)%data(ibd)%data(ipix,spikeMinLoc(1),k,i) =bval 
                       spikesFound = spikesFound+1
                    endif

                 enddo record_loop1

              enddo baseband_loop1

           else if (contMode) then

                 if (allocated(mask)) deallocate(mask,stat=ier)
                 allocate(mask(ndump),stat=ier)
                 if (allocated(tmpVec)) deallocate(tmpVec,stat=ier)
                 allocate(tmpVec(ndump),stat=ier)

                 l = 0 

                 do k = 1, ndump 

                    if (array(ifb)%data(1)%data(ipix,1,k,i).ne.bval) then
                       l = l+1
                       tmpVec(l) = array(ifb)%data(1)%data(ipix,1,k,i)
                    endif
 
                 enddo

                 call COMP_R4_MEDIAN(tmpVec(1:l),l,0.,-1.,median)
                 call COMP_R4_RMS(tmpVec(1:l),l,0.,-1.,rms)

                 mask = array(ifb)%data(1)%data(ipix,1,1:ndump,i).ne.bval

                 spikeMax                                                 &
 &               = MAXVAL(array(ifb)%data(1)%data(ipix,1,1:ndump,i),mask)
                 spikeMaxLoc                                              &
 &               = MAXLOC(array(ifb)%data(1)%data(ipix,1,1:ndump,i),mask)

                 spikeMin                                                 &
 &               = MINVAL(array(ifb)%data(1)%data(ipix,1,1:ndump,i),mask)
                 spikeMinLoc                                              &
 &               = MINLOC(array(ifb)%data(1)%data(ipix,1,1:ndump,i),mask)

                 if (spikeMax-median.gt.threshold*rms) then 
                    array(ifb)%data(1)%data(ipix,1,spikeMaxLoc(1),i) =bval 
                    spikesFound = spikesFound+1
                 endif

                 if (median-spikeMin.gt.threshold*rms) then 
                    array(ifb)%data(1)%data(ipix,1,spikeMinLoc(1),i) =bval 
                    spikesFound = spikesFound+1
                 endif

           endif

        enddo phase_loop1
 
     enddo iter_loop1

  else 

     nchan = data(ifb)%header%channels
     ndump = size(data(ifb)%data%otfdata,3)
     if (allocated(mask)) deallocate(mask)
     if (allocated(tmpVec)) deallocate(tmpVec,stat=ier)
    
     if (specMode) then
        allocate(mask(nchan),stat=ier)
        allocate(tmpVec(nchan),stat=ier)
     else if (contMode) then
        allocate(mask(ndump),stat=ier)
        allocate(tmpVec(ndump),stat=ier)
     endif
     
     iter_loop3: do iter = 1, niter

        if (specMode) then

           k = 0 

           do j = 1, data(ifb)%header%channels
              if (data(ifb)%data%rdata(ipix,j).ne.bval) then
                 k = k+1
                 tmpVec(k) = data(ifb)%data%rdata(ipix,j)
              endif
           enddo

           call COMP_R4_MEDIAN(tmpVec(1:k),k,0.,-1.,median)
           call COMP_R4_RMS(tmpVec(1:k),k,0.,-1.,rms)

           if (doWindow) then
              do j = 1, nchan
                 vChan = (j-data(ifb)%header%crpx4r_2)                         &
&                        *data(ifb)%header%cd4r_21+data(ifb)%header%crvl4r_2
                 mask(j) = data(ifb)%data%rdata(ipix,j).ne.bval.and.           &
&                          (vChan.lt.window(1).or.vChan.gt.window(2))
              enddo
           else
              mask = data(ifb)%data%rdata(ipix,1:nchan).ne.bval
           endif

           spikeMax = MAXVAL(data(ifb)%data%rdata(ipix,1:nchan),mask)
           spikeMaxLoc(1:1) = MAXLOC(data(ifb)%data%rdata(ipix,1:nchan),mask)

           spikeMin = MINVAL(data(ifb)%data%rdata(ipix,1:nchan),mask)
           spikeMinLoc(1:1) = MINLOC(data(ifb)%data%rdata(ipix,1:nchan),mask)

           if (spikeMax-median.gt.threshold*rms) then
              data(ifb)%data%rdata(ipix,spikeMaxLoc(1)) = bval 
              spikesFound = spikesFound+1
           endif

           if (median-spikeMin.gt.threshold*rms) then
              data(ifb)%data%rdata(ipix,spikeMinLoc(1)) = bval 
              spikesFound = spikesFound+1
           endif


           record_loop3: do k = 1, ndump
        
              l = 0 

              do j = 1, data(ifb)%header%channels
                 if (data(ifb)%data%otfdata(ipix,j,k).ne.bval) then
                    l = l+1
                    tmpVec(l) = data(ifb)%data%otfdata(ipix,j,k)
                 endif
              enddo

              call COMP_R4_MEDIAN(tmpVec(1:l),l,0.,-1.,median)
              call COMP_R4_RMS(tmpVec(1:l),l,0.,-1.,rms)

              mask = data(ifb)%data%otfdata(ipix,1:nchan,k).ne.bval

              spikeMax = MAXVAL(data(ifb)%data%otfdata(ipix,1:nchan,k),mask)
              spikeMaxLoc =MAXLOC(data(ifb)%data%otfdata(ipix,1:nchan,k),mask)

              spikeMin = MINVAL(data(ifb)%data%otfdata(ipix,1:nchan,k),mask)
              spikeMinLoc =MINLOC(data(ifb)%data%otfdata(ipix,1:nchan,k),mask)

              if (spikeMax-median.gt.threshold*rms) then
                 data(ifb)%data%otfdata(ipix,spikeMaxLoc(1),k) = bval 
                 spikesFound = spikesFound+1
              endif

              if (median-spikeMin.gt.threshold*rms) then
                 data(ifb)%data%otfdata(ipix,spikeMinLoc(1),k) = bval 
                 spikesFound = spikesFound+1
              endif

           enddo record_loop3
        
        else if (contMode) then
           l = 0 

           do k = 1, ndump
        
              if (data(ifb)%data%otfdata(ipix,1,k).ne.bval) then
                 l = l+1
                 tmpVec(l) = data(ifb)%data%otfdata(ipix,1,k)
              endif
           enddo

           call COMP_R4_MEDIAN(tmpVec(1:l),l,0.,-1.,median)
           call COMP_R4_RMS(tmpVec(1:l),l,0.,-1.,rms)

           mask = data(ifb)%data%otfdata(ipix,1,1:ndump).ne.bval

           spikeMax = MAXVAL(data(ifb)%data%otfdata(ipix,1,1:ndump),mask)
           spikeMaxLoc =MAXLOC(data(ifb)%data%otfdata(ipix,1,1:ndump),mask)

           spikeMin = MINVAL(data(ifb)%data%otfdata(ipix,1,1:ndump),mask)
           spikeMinLoc =MINLOC(data(ifb)%data%otfdata(ipix,1,1:ndump),mask)

           if (spikeMax-median.gt.threshold*rms) then
              data(ifb)%data%otfdata(ipix,1,spikeMaxLoc(1)) = bval 
              spikesFound = spikesFound+1
           endif

           if (median-spikeMin.gt.threshold*rms) then 
              data(ifb)%data%otfdata(ipix,1,spikeMinLoc(1)) = bval 
              spikesFound = spikesFound+1
           endif

        endif

     enddo iter_loop3

     deallocate(mask,tmpVec,stat=ier)
     
  endif

  if (spikesFound.eq.0) then
     reduce(ifb)%despike_done = .false.
     call gagout('I-DESPIKE: no spikes found.')
  else
     reduce(ifb)%despike_done = .true.
     if (spikesFound.lt.10) then
        write(name,'(A6,I1,A8)') 'found ',spikesFound,' spikes.'
     else if (spikesFound.lt.100) then
        write(name,'(A6,I2,A8)') 'found ',spikesFound,' spikes.'
     else if (spikesFound.lt.1000) then
        write(name,'(A6,I3,A8)') 'found ',spikesFound,' spikes.'
     else
        write(name,'(A6,I4,A8)') 'found ',spikesFound,' spikes.'
     endif
     call gagout('I-DESPIKE: '//trim(name))
  endif
  tmpString = nullString
  write(tmpString,'(A6,I0)') 'REDUCE',ifb
  call sic_descriptor(trim(tmpString),desc,ex_reduce)
  if (ex_reduce) then
     call sic_descriptor(trim(tmpString)//'%DESPIKE_DONE',desc,exist)
     readonly = desc%readonly
     call sic_delvariable(trim(tmpString),.false.,error)
     call reduce_to_sic(readonly,error)
  endif 
        
  if (spikesFound.eq.0) return

  noSky = count(array(1)%data(1)%iswitch.eq.'SKY').eq.0
  tmpString = scan%header%scantype
  call sic_upper(tmpString)
                                                                                
  if (index(tmpString,'CAL').ne.0.and..not.noSky) then
!
     nphases = febe(ifb)%header%nphases
     npix = febe(ifb)%header%febefeed
     nchan = array(ifb)%header(1)%channels
     if (allocated(mask2)) deallocate(mask2,stat=ier)
     allocate(mask2(ndump,nphases),stat=ier)
     if (allocated(mask3)) deallocate(mask3,stat=ier)
     allocate(mask3(ndump,nphases),stat=ier)

     do i = 1, scan%header%nobs 
        do j = 1, nphases
           mask2(1:ndump,j) = data(ifb)%data%subscan.eq.i
        enddo

        if (index(raw(i)%antenna(ifb)%subScanType,'Sky').ne.0.or.              &
   &        index(raw(i)%subRef(ifb)%subScanType,'Sky').ne.0) then
           do j = 1, febe(ifb)%header%febeband
              do k = 1, npix
                 do l = 1, nchan 
                    mask3 = array(ifb)%data(j)%data(k,l,:,:).ne.bval
                    if (count(.not.mask3).eq.ndump*nphases.or.                 &
   &                    count(mask2.and.mask3).eq.0) then
                       gains(ifb)%psky(j,k,l) = blankingRaw
                    else
                       gains(ifb)%psky(j,k,l)                                  &
   &                   = sum(                                                  &
   &                         array(ifb)%data(j)%data(k,l,1:ndump,1:nphases),   &
   &                         mask2.and.mask3)/count(mask2.and.mask3)
                    endif
                 enddo
              enddo
           enddo

        else if (index(raw(i)%antenna(ifb)%subScanType,'Ambient').ne.0.or.     &
   &             index(raw(i)%subRef(ifb)%subScanType,'Ambient').ne.0) then
           do j = 1, febe(ifb)%header%febeband
              do k = 1, npix
                 do l = 1, nchan
                    mask3 = array(ifb)%data(j)%data(k,l,:,:).ne.bval
                    if (count(.not.mask3).eq.ndump*nphases.or.                 &
   &                    count(mask2.and.mask3).eq.0) then
                       gains(ifb)%phot(j,k,l) = blankingRaw
                    else
                       gains(ifb)%phot(j,k,l)                                  &
   &                   = sum(                                                  &
   &                         array(ifb)%data(j)%data(k,l,1:ndump,1:nphases),   &
   &                         mask2.and.mask3)/count(mask2.and.mask3)
                    endif
                 enddo
              enddo
           enddo

        else if (index(raw(i)%antenna(ifb)%subScanType,'Cold').ne.0.or.        &
   &             index(raw(i)%subRef(ifb)%subScanType,'Cold').ne.0) then
           do j = 1, febe(ifb)%header%febeband
              do k = 1, npix
                 do l = 1, nchan
                    mask3 = array(ifb)%data(j)%data(k,l,:,:).ne.bval
                    if (count(.not.mask3).eq.ndump*nphases.or.                 &
   &                    count(mask2.and.mask3).eq.0) then
                       gains(ifb)%pcold(j,k,l) = blankingRaw
                    else
                       gains(ifb)%pcold(j,k,l)                                 &
   &                   = sum(                                                  &
   &                         array(ifb)%data(j)%data(k,l,1:ndump,1:nphases),   &
   &                         mask2.and.mask3)/count(mask2.and.mask3)
                    endif
                 enddo
              enddo
           enddo

        else if (index(raw(i)%antenna(ifb)%subScanType,'Grid').ne.0.or.        &
   &             index(raw(i)%subRef(ifb)%subScanType,'Grid').ne.0) then
           do j = 1, febe(ifb)%header%febeband
              do k = 1, npix
                 do l = 1, nchan
                    mask3 = array(ifb)%data(j)%data(k,l,:,:).ne.bval
                    if (count(.not.mask3).eq.ndump*nphases) then
                       gains(ifb)%pgrid(j,k,l) = blankingRaw
                    else
                       gains(ifb)%pgrid(j,k,l)                                 &
   &                   = sum(                                                  &
   &                         array(ifb)%data(j)%data(k,l,1:ndump,1:nphases),   &
   &                         mask2.and.mask3)/count(mask2.and.mask3)
                    endif
                 enddo
              enddo
           enddo

        else 
           
           call gagout('W-SYNC: Subscan type unknown. Cannot attribute '       &
   &                   //' mean calibration counts.')

        endif

     enddo
     deallocate(mask2,mask3,stat=ier) 
     if (tmpFrontend(1:1).NE.'R'.and.tmpFrontend(1:1).ne.'I') then 
     where (gains(ifb)%phot.ne.blankingRaw.and.gains(ifb)%psky.ne.blankingRaw)
       gains(ifb)%gainarray = gains(ifb)%phot-gains(ifb)%psky
     else where
       gains(ifb)%gainarray = blankingRaw
     end where
     endif
!
     write(*,*)  ' '
     WRITE(*,10) 'idFe', 'freq', 'THotLoad', 'TColdLoad', 'idBe',       &
 &               'Pix', 'recTemp', 'sysTemp', 'calTemp',                &
 &               'tauZenith', 'pwv'
     WRITE(*,20) '[GHz]','---- [Kelvin] ----',                          &
 &                  '------ [Kelvin] -------','[Neper]','[mm]'
     WRITE(*,30) '----------------------------------------------'       &
 &               //'----------------------------------'

     do ifb = 1, scan%header%nfebe
        ndump = maxval(data(ifb)%data%integnum)
        call do_tcal(ifb,1,ndump,.false.,0.,error)
     enddo
     write(*,*)  ' '
  endif

 10   FORMAT(T1,A4,T6,A4,T13,A8,T22,A9,T32,A4,T38,A3,T43,A7,T51,A7,T59, &
 &           A7,T67,A9,T77,A3)
 20   FORMAT(T6,A5,T13,A18,T43,A23,T68,A7,T77,A4)
 30   FORMAT(T1,A80)
  
  return

end subroutine despike

