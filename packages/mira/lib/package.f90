!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine mira_pack_set(pack)
  use gpack_def
  use gkernel_interfaces
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(gpack_info_t), intent(out) :: pack
  !
  external :: class_pack_set
  external :: mira_pack_init,mira_pack_clean
  !
  pack%name='mira'
  pack%ext='.mira'
  pack%authors="H.Wiesemeyer, A.Sievers"
  pack%depend(1:3) = (/ locwrd(sic_pack_set), locwrd(greg_pack_set), locwrd(class_pack_set)/) ! List here dependencies
  pack%init=locwrd(mira_pack_init)
  pack%clean=locwrd(mira_pack_clean)
  !
end subroutine mira_pack_set
!
subroutine mira_pack_init(gpack_id,error)
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  integer :: gpack_id
  logical :: error
  !
  ! Activate SIC copies of structure ATM
  call atm_sicvariables(error)
  !
  ! Load local language and initialize MIRA
  call load_mira
  !
  ! Init error flag
  error = .false.
  !
end subroutine mira_pack_init
!
subroutine mira_pack_clean(error)
  !----------------------------------------------------------------------
  ! @ private
  ! Called at end of session. Might clean here for example global buffers
  ! allocated during the session
  !----------------------------------------------------------------------
  logical :: error
  !
  call exit_mira
  !
end subroutine mira_pack_clean
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
