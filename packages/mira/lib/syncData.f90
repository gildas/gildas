subroutine syncData(ifb,nfb,subscanRange,error)
!
! Called by subroutines getfits and getfitsNew.
! Interpolates antenna and subreflector data at backend timestamp
! (linear between preceeding and following antenna and subreflector dump).
! Results are written into Mira structures DATA(ifb) (where ifb is the frontend-
! backend number).
!
! syncData checks the consistency of the timing (e.g., that each backend time
! stamp between imbfits data header entries dateObs and dateEnd is framed by
! antenna and subreflector data points at either side).
!
!
  use mira
  use gildas_def

  implicit none

  integer :: ifb , nfb
  integer, dimension(2) :: subscanRange
  logical :: error

  integer :: i, ier, ilo, isub, iup, i1, i2, i3, i4, j, k, l, locate,       &
     &       nbd, nchan, newchan, nfebe, nphases, npix 
  integer :: ndumpAntenna, ndumpAntennaNew, ndumpAntennaFast,               &
     &       ndumpAntennaFastNew, ndumpBackend, ndumpBackendNew,            &
     &       ndumpCryo, ndumpFrontend, ndumpSubref, ndumpSubrefNew
  real*4  :: meanCounts
  real*8  :: dTime1, dTime2, gainMax1, gainMax2, slope, trackingCutoff
 
  real*8, dimension(nsubscan)       :: subScanEnd, subScanStart

  integer(kind=address_length),dimension(9) :: desc
  character(len=20)                    :: tmpScanType, tmpSwitchMode
  character(len=32)                    :: tmpFrontend
  logical                              :: doShift, exist, scanStart, trackFlag
  logical, dimension(:,:), allocatable :: mask,tmpMask
  logical, dimension(:), allocatable   :: maskFSW,tmpMaskFSW
  logical, dimension(:), allocatable   :: subScanFastMask, subScanMask,    &
      &                                   subScanSubrefMask


  ndumpBackend = size(dt(ifb)%data%mjd)
  ndumpAntenna = size(mnt(ifb)%data%lst,1)
  ndumpAntennaFast = size(mnt(ifb)%data%encoder_az_el,1)
  ndumpSubref = size(mnt(ifb)%data%focus_x_y_z,1)
  ndumpFrontend = size(mnt(ifb)%data%tamb_p_humid,1)
!
!
!
  nfebe = scan%header%nfebe
  nbd = febe(ifb)%header%febeband
  nchan = ar(ifb)%header(1)%channels 
  newchan = dt(ifb)%header%channels
  npix = febe(ifb)%header%febefeed
  nphases = febe(ifb)%header%nphases
!
! 
!
  trackingCutoff = raw(1)%antenna(ifb)%trackingError
  do isub = 1, nsubscan
     if (isub.gt.1) trackingCutoff                                         &
 &   = min(trackingCutoff,raw(isub)%antenna(ifb)%trackingError)
     subScanStart(isub) = raw(isub)%antenna(ifb)%subScanStart
     if (index(tmpScanType,'FOCUS').ne.0)                                  &
 &      subScanStart(isub) = max(subScanStart(isub),                       &
 &                               raw(isub)%subref(ifb)%subScanStart)
     subScanStart(isub) = max(subScanStart(isub),                          &
 &                            raw(isub)%backend(ifb)%subScanStart)
     subScanEnd(isub) = raw(isub)%antenna(ifb)%subScanEnd 
     if (index(tmpScanType,'FOCUS').ne.0)                                  &
 &      subScanEnd(isub) = min(subScanEnd(isub),raw(isub)%subref(ifb)%subScanEnd)
     subScanStart(isub) = raw(isub)%antenna(ifb)%subScanStart
     subScanEnd(isub) = raw(isub)%antenna(ifb)%subScanEnd 
  enddo 
!
  do i = 1, ndumpAntenna-1
     if (mnt(ifb)%data%mjd(i).eq.mnt(ifb)%data%mjd(i+1))                   &
 &       mnt(ifb)%data%subScan(i) = mnt(ifb)%data%subScan(i+1)
  enddo
!
  do i = 1, ndumpAntennaFast-1
     if (mnt(ifb)%data%encoder_az_el(i,3)                                  &
 &       .eq.mnt(ifb)%data%encoder_az_el(i+1,3)) &
 &       mnt(ifb)%data%subScanFast(i) = mnt(ifb)%data%subScanFast(i+1)
  enddo 
!
  do i = 1, ndumpSubref-1
     if (mnt(ifb)%data%focus_x_y_z(i,4).eq.mnt(ifb)%data%focus_x_y_z(i+1,4)) &
 &       mnt(ifb)%data%subScanSubref(i) = mnt(ifb)%data%subScanSubref(i+1)
  enddo
!
!
! now remove the second data point of a pair of identical data points, and 
! count dumps again (new number of dumps = New appended to variable name)
!
  ilo = 0
  do i = 1, ndumpAntenna
     isub = mnt(ifb)%data%subScan(i)
     i1 = isub-subscanRange(1)+1
     if (mnt(ifb)%data%mjd(i).lt.subScanStart(i1).or.                &
  &      mnt(ifb)%data%mjd(i).gt.subScanEnd(i1)) then
        cycle
     else if (i.eq.1) then
        ilo = 1
     else if (mnt(ifb)%data%mjd(i).ne.mnt(ifb)%data%mjd(i-1)) then
        ilo = ilo+1 
     endif
  enddo
  ndumpAntennaNew = ilo

  ilo = 0
  do i = 1, ndumpAntennaFast
     isub = mnt(ifb)%data%subScanFast(i)
     i1 = isub-subscanRange(1)+1
     if (mnt(ifb)%data%encoder_az_el(i,3).lt.subScanStart(i1).or.    &
  &      mnt(ifb)%data%encoder_az_el(i,3).gt.subScanEnd(i1)) then
        cycle
     else if (i.eq.1) then
        ilo = 1
     else if (mnt(ifb)%data%encoder_az_el(i,3)                         &
  &           .ne.mnt(ifb)%data%encoder_az_el(i-1,3)) then
        ilo = ilo+1
     endif
   enddo
   ndumpAntennaFastNew = ilo

  ilo = 0
  do i = 1, ndumpSubref
     isub = mnt(ifb)%data%subScanSubref(i)
     i1 = isub-subscanRange(1)+1
     if (mnt(ifb)%data%focus_x_y_z(i,4).lt.subScanStart(i1).or.      &
  &      mnt(ifb)%data%focus_x_y_z(i,4).gt.subScanEnd(i1)) then
        cycle
     else if (i.eq.1) then
        ilo = 1
     else if (mnt(ifb)%data%focus_x_y_z(i,4)                           &
&            .ne.mnt(ifb)%data%focus_x_y_z(i-1,4)) then
        ilo = ilo+1
     endif
  enddo
  if (ilo.ne.0) then
     ndumpSubrefNew = ilo
  else if (index(tmpScanType,'FOCUS').eq.0) then
     call gagout('W-SYNC: Subreflector data not synchronized.')
     ndumpSubrefNew = 1
  else
     call gagout('E-SYNC: Subreflector data not synchronized.')
     error = .true.
     return
  endif


  if (ifb.eq.1) allocate(monBuf(nfebe))
  monBuf(ifb)%header = mnt(ifb)%header
!
  allocate(monBuf(ifb)%data%subScan(ndumpAntennaNew),              &
     &     monBuf(ifb)%data%subScanFast(ndumpAntennaFastNew),      &
     &     monBuf(ifb)%data%subScanSubref(ndumpSubrefNew),         &
     &     monBuf(ifb)%data%mjd(ndumpAntennaNew),                  &
     &     monBuf(ifb)%data%lst(ndumpAntennaNew),                  &
     &     monBuf(ifb)%data%encoder_az_el(ndumpAntennaFastNew,3),  &
     &     monBuf(ifb)%data%tracking_az_el(ndumpAntennaFastNew,2), &
     &     monBuf(ifb)%data%longoff(ndumpAntennaNew),              &
     &     monBuf(ifb)%data%latoff(ndumpAntennaNew),               &
     &     monBuf(ifb)%data%baslong(ndumpAntennaNew),              &
     &     monBuf(ifb)%data%baslat(ndumpAntennaNew),               &
     &     monBuf(ifb)%data%parangle(ndumpAntennaNew),             &
     &     monBuf(ifb)%data%antenna_az_el(2),                      &
     &     monBuf(ifb)%data%tamb_p_humid(ndumpFrontend,3),         &
     &     monBuf(ifb)%data%wind_dir_vavg_vmax(ndumpFrontend,3),   &
     &     monBuf(ifb)%data%thotcold(ndumpFrontend,2),             &
     &     monBuf(ifb)%data%refractio(ndumpFrontend),              &
     &     monBuf(ifb)%data%focus_x_y_z(ndumpSubrefNew,4),         &
     &     monBuf(ifb)%data%dfocus_x_y_z(ndumpSubrefNew,3),        &
     &     monBuf(ifb)%data%phi_x_y_z(ndumpSubrefNew,3),           &
     &     monBuf(ifb)%data%dphi_x_y_z(ndumpSubrefNew,3),          &
     &     stat = ier)
  allocate(subScanSubrefMask(ndumpSubrefNew),                 &
     &     subScanMask(ndumpAntenna),                         &
     &     subScanFastMask(ndumpAntennaFastNew),stat = ier)
  monBuf(ifb)%data%antenna_az_el = mnt(ifb)%data%antenna_az_el
  monBuf(ifb)%data%tamb_p_humid = mnt(ifb)%data%tamb_p_humid
  monBuf(ifb)%data%wind_dir_vavg_vmax = mnt(ifb)%data%wind_dir_vavg_vmax
  monBuf(ifb)%data%thotcold  = mnt(ifb)%data%thotcold
  ilo = 0
  do i = 1, ndumpAntenna
     isub = mnt(ifb)%data%subScan(i)
     i1 = isub-subscanRange(1)+1
     if (mnt(ifb)%data%mjd(i).lt.subScanStart(i1).or.                &
  &      mnt(ifb)%data%mjd(i).gt.subScanEnd(i1)) then
        cycle
     else if (i.eq.1) then
        ilo = 1
     else if (mnt(ifb)%data%mjd(i).ne.mnt(ifb)%data%mjd(i-1)) then
        ilo = ilo+1 
     endif
     monBuf(ifb)%data%subScan(ilo) = mnt(ifb)%data%subScan(i)
     monBuf(ifb)%data%mjd(ilo) = mnt(ifb)%data%mjd(i)
     monBuf(ifb)%data%lst(ilo) = mnt(ifb)%data%lst(i)
     monBuf(ifb)%data%longoff(ilo) = mnt(ifb)%data%longoff(i)
     monBuf(ifb)%data%latoff(ilo) =  mnt(ifb)%data%latoff(i)
     monBuf(ifb)%data%baslong(ilo) = mnt(ifb)%data%baslong(i)
     monBuf(ifb)%data%baslat(ilo) = mnt(ifb)%data%baslat(i)
     monBuf(ifb)%data%parangle(ilo) = mnt(ifb)%data%parangle(i)
  enddo
   
  ilo = 0
  do i = 1, ndumpAntennaFast
     isub = mnt(ifb)%data%subScanFast(i)
     i1 = isub-subscanRange(1)+1
     if (mnt(ifb)%data%encoder_az_el(i,3).lt.subScanStart(i1).or.     & 
  &      mnt(ifb)%data%encoder_az_el(i,3).gt.subScanEnd(i1)) then
        cycle
     else if (i.eq.1) then
        ilo = 1
     else if (mnt(ifb)%data%encoder_az_el(i,3)                          &
  &           .ne.mnt(ifb)%data%encoder_az_el(i-1,3)) then
        ilo = ilo+1
     endif
     monBuf(ifb)%data%subScanFast(ilo) = mnt(ifb)%data%subScanFast(i)
     monBuf(ifb)%data%encoder_az_el(ilo,1:3)                            &
   & = mnt(ifb)%data%encoder_az_el(i,1:3)
     monBuf(ifb)%data%tracking_az_el(ilo,1:2)                           &
   & = mnt(ifb)%data%tracking_az_el(i,1:2)
  enddo

  ilo = 0
  do i = 1, ndumpSubref
     isub = mnt(ifb)%data%subScanSubref(i)
     i1 = isub-subscanRange(1)+1
     if (mnt(ifb)%data%focus_x_y_z(i,4).lt.subScanStart(i1).or.         &
  &      mnt(ifb)%data%focus_x_y_z(i,4).gt.subScanEnd(i1)) then
        cycle
     else if (i.eq.1) then
        ilo = 1
     else if (mnt(ifb)%data%focus_x_y_z(i,4)                              &
  &           .ne.mnt(ifb)%data%focus_x_y_z(i-1,4)) then
        ilo = ilo+1
     endif
     monBuf(ifb)%data%subScanSubref(ilo) = mnt(ifb)%data%subScanSubref(i)
     monBuf(ifb)%data%focus_x_y_z(ilo,1:4) = mnt(ifb)%data%focus_x_y_z(i,1:4)
     monBuf(ifb)%data%dfocus_x_y_z(ilo,1:3) = mnt(ifb)%data%dfocus_x_y_z(i,1:3)
     monBuf(ifb)%data%phi_x_y_z(ilo,1:3) = mnt(ifb)%data%phi_x_y_z(i,1:3)
     monBuf(ifb)%data%dphi_x_y_z(ilo,1:3) = mnt(ifb)%data%dphi_x_y_z(i,1:3)
  enddo
  ndumpAntenna = ndumpAntennaNew
  ndumpAntennaFast = ndumpAntennaFastNew
  ndumpSubref = ndumpSubrefNew
  ilo = 0
  k = 0
  scanStart = .true.
  do i = 1 , ndumpBackend
     isub = dt(ifb)%data%subScan(i)
     i1 = isub-subscanRange(1)+1
!     if (dt(ifb)%data%mjd(i).lt.subScanStart(isub).or.                &
!  &      dt(ifb)%data%mjd(i).gt.subScanEnd(isub)) then
!
! changed by HW as follows: (2007-06-22)
     if (dt(ifb)%data%mjd(i)-dt(ifb)%data%integtim(i)*sec2day.lt.    &
  &      subScanStart(i1).or.dt(ifb)%data%mjd(i).gt.subScanEnd(i1)) then
        k = k+nphases
        cycle
     else if (scanStart) then
        ilo = 1
        k = k+nphases
        scanStart = .false.
     else 
        doShift = .false.
        do j = 1, nphases 
           k = k+1
           if (ar(ifb)%data(1)%mjd(k).eq.ar(ifb)%data(1)%mjd(k-1)) &
  &          doShift = .true. 
        enddo
        if (.not.doShift) ilo = ilo+1
     endif
  enddo
  ndumpBackendNew = ilo

  if (ifb.eq.1) allocate(dataBuf(nfebe),stat=ier)

  dataBuf(ifb)%header = dt(ifb)%header
  allocate(dataBuf(ifb)%data%subscan(ndumpBackendNew),                &
  &        dataBuf(ifb)%data%mjd(ndumpBackendNew),                    &
  &        dataBuf(ifb)%data%integnum(ndumpBackendNew),               &
  &        dataBuf(ifb)%data%nints(ndumpBackendNew),                  &
  &        dataBuf(ifb)%data%lst(ndumpBackendNew),                    &
  &        dataBuf(ifb)%data%midtime(ndumpBackendNew),                &
  &        dataBuf(ifb)%data%integtim(ndumpBackendNew),               &
  &        dataBuf(ifb)%data%longoff(ndumpBackendNew),                &
  &        dataBuf(ifb)%data%latoff(ndumpBackendNew),                 &
  &        dataBuf(ifb)%data%azimuth(ndumpBackendNew),                &
  &        dataBuf(ifb)%data%elevatio(ndumpBackendNew),               &
  &        dataBuf(ifb)%data%cbaslong(ndumpBackendNew),               &
  &        dataBuf(ifb)%data%cbaslat(ndumpBackendNew),                &
  &        dataBuf(ifb)%data%baslong(ndumpBackendNew),                &
  &        dataBuf(ifb)%data%baslat(ndumpBackendNew),                 &
  &        dataBuf(ifb)%data%parangle(ndumpBackendNew),               &
  &        dataBuf(ifb)%data%pc_11(ndumpBackendNew),                  &
  &        dataBuf(ifb)%data%pc_12(ndumpBackendNew),                  &
  &        dataBuf(ifb)%data%pc_21(ndumpBackendNew),                  &
  &        dataBuf(ifb)%data%pc_22(ndumpBackendNew),                  &
  &        dataBuf(ifb)%data%mcrval1(ndumpBackendNew),                &
  &        dataBuf(ifb)%data%mcrval2(ndumpBackendNew),                &
  &        dataBuf(ifb)%data%mlonpole(ndumpBackendNew),               &
  &        dataBuf(ifb)%data%mlatpole(ndumpBackendNew),               &
  &        dataBuf(ifb)%data%dfocus_x_y_z(ndumpBackendNew,3),         &
  &        dataBuf(ifb)%data%focus_x_y_z(ndumpBackendNew,3),          &
  &        dataBuf(ifb)%data%dphi_x_y_z(ndumpBackendNew,3),           &
  &        dataBuf(ifb)%data%phi_x_y_z(ndumpBackendNew,3),            &
  &        dataBuf(ifb)%data%otfdata(npix,newchan,ndumpBackendNew),   &    
  &        dataBuf(ifb)%data%rdata(npix,newchan),                     &
  &        dataBuf(ifb)%data%cr4K1(ndumpBackendNew),                  &
  &        dataBuf(ifb)%data%cr4K2(ndumpBackendNew),                  &
  &        stat = ier)
  if (ifb.eq.1) allocate(arrayBuf(nfebe),stat=ier)
  allocate(arrayBuf(ifb)%data(nbd),arrayBuf(ifb)%header(nbd),stat=ier)
  arrayBuf(ifb)%header(1:nbd) = ar(ifb)%header(1:nbd)
  do i = 1, nbd
     allocate(arrayBuf(ifb)%data(i)%data(npix,nchan,ndumpBackendNew,nphases), &
  &           arrayBuf(ifb)%data(i)%iswitch(ndumpBackendNew,nphases),         &
  &           arrayBuf(ifb)%data(i)%mjd(ndumpBackendNew*nphases),             &
  &           arrayBuf(ifb)%data(i)%integtim(ndumpBackendNew*nphases),        &
  &           stat=ier)
  enddo
  ilo = 0
  k = 0 
  scanStart = .true.
  do i = 1, ndumpBackend
     isub = dt(ifb)%data%subScan(i)
     i1 = isub-subscanRange(1)+1
!     if (dt(ifb)%data%mjd(i).lt.subScanStart(isub).or.              &
!  &      dt(ifb)%data%mjd(i).gt.subScanEnd(isub)) then
!
! changed by HW as follows: (2007-06-22)
     if (dt(ifb)%data%mjd(i)-dt(ifb)%data%integtim(i)*sec2day.lt.    &
  &      subScanStart(i1).or.dt(ifb)%data%mjd(i).gt.subScanEnd(i1)) then
        k = k+nphases
        cycle
     else if (scanStart) then
        k = k+nphases
        ilo = 1
        scanStart = .false.
     else 
        doShift = .false.
        do j = 1, nphases
           k = k+1
           if (ar(ifb)%data(1)%mjd(k).eq.ar(ifb)%data(1)%mjd(k-1)) &
  &           doShift = .true.
        enddo 
        if (.not.doShift) ilo = ilo+1
     endif
     dataBuf(ifb)%data%subscan(ilo) = dt(ifb)%data%subscan(i)
     dataBuf(ifb)%data%mjd(ilo) = dt(ifb)%data%mjd(i)
     dataBuf(ifb)%data%integnum(ilo) = ilo
     dataBuf(ifb)%data%nints(ilo) = dt(ifb)%data%nints(i)
     dataBuf(ifb)%data%lst(ilo) = dt(ifb)%data%lst(i)
     dataBuf(ifb)%data%midtime(ilo) = dt(ifb)%data%midtime(i)
     dataBuf(ifb)%data%integtim(ilo) = dt(ifb)%data%integtim(i)
     dataBuf(ifb)%data%longoff(ilo) = dt(ifb)%data%longoff(i)
     dataBuf(ifb)%data%latoff(ilo) = dt(ifb)%data%latoff(i)
     dataBuf(ifb)%data%azimuth(ilo) = dt(ifb)%data%azimuth(i)
     dataBuf(ifb)%data%elevatio(ilo) = dt(ifb)%data%elevatio(i)
     dataBuf(ifb)%data%cbaslong(ilo) = dt(ifb)%data%cbaslong(i)
     dataBuf(ifb)%data%cbaslat(ilo) = dt(ifb)%data%cbaslat(i)
     dataBuf(ifb)%data%baslong(ilo) = dt(ifb)%data%baslong(i)
     dataBuf(ifb)%data%baslat(ilo) = dt(ifb)%data%baslat(i)
     dataBuf(ifb)%data%parangle(ilo) = dt(ifb)%data%parangle(i)
     dataBuf(ifb)%data%pc_11(ilo) = dt(ifb)%data%pc_11(i)
     dataBuf(ifb)%data%pc_12(ilo) = dt(ifb)%data%pc_12(i)
     dataBuf(ifb)%data%pc_21(ilo) = dt(ifb)%data%pc_21(i)
     dataBuf(ifb)%data%pc_22(ilo) = dt(ifb)%data%pc_22(i)
     dataBuf(ifb)%data%cr4K1(ilo) = dt(ifb)%data%cr4K1(i)
     dataBuf(ifb)%data%cr4K2(ilo) = dt(ifb)%data%cr4K2(i)
     dataBuf(ifb)%data%mcrval1(ilo) = dt(ifb)%data%mcrval1(i)
     dataBuf(ifb)%data%mcrval2(ilo) = dt(ifb)%data%mcrval2(i)
     dataBuf(ifb)%data%mlonpole(ilo) = dt(ifb)%data%mlonpole(i)
     dataBuf(ifb)%data%mlatpole(ilo) = dt(ifb)%data%mlatpole(i)
     dataBuf(ifb)%data%otfdata(npix,1:newchan,ilo) = &
  &                                  dt(ifb)%data%otfdata(npix,1:newchan,i)
     dataBuf(ifb)%data%rdata(npix,1:newchan) = dt(ifb)%data%rdata(npix,1:newchan) 

     i1 = (ilo-1)*nphases+1
     i2 = ilo*nphases
     i3 = (i-1)*nphases+1
     i4 = i*nphases
     do j = 1, nbd
        arrayBuf(ifb)%data(j)%data(1:npix,1:nchan,ilo,1:nphases)     &
  &     = ar(ifb)%data(j)%data(1:npix,1:nchan,i,1:nphases)
        arrayBuf(ifb)%data(j)%iswitch(ilo,1:nphases)     &
  &     = ar(ifb)%data(j)%iswitch(i,1:nphases)
        arrayBuf(ifb)%data(j)%mjd(i1:i2) = ar(ifb)%data(j)%mjd(i3:i4)
        arrayBuf(ifb)%data(j)%integtim(i1:i2) = ar(ifb)%data(j)%integtim(i3:i4)
     enddo
  enddo

  ndumpBackend = ndumpBackendNew
! 
! now interpolate monitor data at sampling of backend data
!
  do i = 1, ndumpBackend
     
     ilo = locate(monBuf(ifb)%data%mjd,dataBuf(ifb)%data%midtime(i),ndumpAntenna)

     if (ilo.eq.0) then
        ilo = 1
     else if (ilo.eq.ndumpAntenna) then
        ilo = ndumpAntenna-1
     endif

     iup = ilo+1

     if (monBuf(ifb)%data%subscan(ilo).ne.monBuf(ifb)%data%subscan(iup)) then 
        if (dataBuf(ifb)%data%subscan(i).eq.monBuf(ifb)%data%subscan(ilo)) then
           ilo = ilo-1
           iup = iup-1
        else
           ilo = ilo+1
           iup = iup+1
        endif
     endif

     dTime1 = monBuf(ifb)%data%mjd(iup)-monBuf(ifb)%data%mjd(ilo)
     dTime2 = dataBuf(ifb)%data%midtime(i)-monBuf(ifb)%data%mjd(ilo)

     slope = (monBuf(ifb)%data%lst(iup)-monBuf(ifb)%data%lst(ilo))/dTime1 
     dataBuf(ifb)%data%lst(i) = monBuf(ifb)%data%lst(ilo)+dTime2*slope
 
     slope = (monBuf(ifb)%data%longoff(iup)-monBuf(ifb)%data%longoff(ilo))/dTime1 
     dataBuf(ifb)%data%longoff(i) = monBuf(ifb)%data%longoff(ilo)+dTime2*slope

     slope = (monBuf(ifb)%data%latoff(iup)-monBuf(ifb)%data%latoff(ilo))/dTime1 
     dataBuf(ifb)%data%latoff(i) = monBuf(ifb)%data%latoff(ilo)+dTime2*slope

     slope = (monBuf(ifb)%data%baslong(iup)-monBuf(ifb)%data%baslong(ilo))/dTime1 
     dataBuf(ifb)%data%baslong(i) = monBuf(ifb)%data%baslong(ilo)+dTime2*slope

     slope = (monBuf(ifb)%data%baslat(iup)-monBuf(ifb)%data%baslat(ilo))/dTime1 
     dataBuf(ifb)%data%baslat(i) = monBuf(ifb)%data%baslat(ilo)+dTime2*slope

     slope = (monBuf(ifb)%data%parangle(iup)-monBuf(ifb)%data%parangle(ilo))/dTime1 
     dataBuf(ifb)%data%parangle(i) = monBuf(ifb)%data%parangle(ilo)+dTime2*slope

     ilo = locate(monBuf(ifb)%data%encoder_az_el(1:ndumpAntennaFast,3),          &
   &              dataBuf(ifb)%data%midtime(i),ndumpAntennaFast)

     if (ilo.eq.0) then
        ilo = 1
     else if (ilo.eq.ndumpAntennaFast) then
        ilo = ndumpAntennaFast-1
     endif

     iup = ilo+1

     if (monBuf(ifb)%data%subScanFast(ilo).ne.monBuf(ifb)%data%subScanFast(iup)) then
        if (dataBuf(ifb)%data%subscan(i).eq.monBuf(ifb)%data%subScanFast(ilo)) then
           ilo = ilo-1
           iup = iup-1
        else
           ilo = ilo+1
           iup = iup+1
        endif
     endif
 
     dTime1 = monBuf(ifb)%data%encoder_az_el(iup,3)                            &
 &            -monBuf(ifb)%data%encoder_az_el(ilo,3)
     dTime2 = dataBuf(ifb)%data%midtime(i)-monBuf(ifb)%data%encoder_az_el(ilo,3)

     slope = (monBuf(ifb)%data%encoder_az_el(iup,1)                            &
 &           -monBuf(ifb)%data%encoder_az_el(ilo,1))                           &
 &           /dTime1 
     dataBuf(ifb)%data%azimuth(i) = monBuf(ifb)%data%encoder_az_el(ilo,1)+dTime2*slope

     slope = (monBuf(ifb)%data%encoder_az_el(iup,2)                            &
 &            -monBuf(ifb)%data%encoder_az_el(ilo,2))                          &
 &          /dTime1 
     dataBuf(ifb)%data%elevatio(i) = monBuf(ifb)%data%encoder_az_el(ilo,2)     &
 &                                +dTime2*slope
     if (associated(cryo)) then
        ndumpCryo = size(cryo%mjd)
        if (ndumpCryo.gt.1) then
           ilo = locate(                                                     &
   &                    cryo%mjd(1:ndumpCryo),dataBuf(ifb)%data%midtime(i),     &
   &                    ndumpCryo)
           if (ilo.eq.0) ilo = ilo+1
           if (ilo.lt.ndumpCryo) then
               iup = ilo+1
           else
               iup = ilo
           endif
        else
           ilo = 1
           iup = 1
        endif
        if (ilo.eq.iup) then
           dataBuf(ifb)%data%cr4K1(i) = cryo%cr4K1(ilo)
           dataBuf(ifb)%data%cr4K2(i) = cryo%cr4K2(ilo)
        else
           dTime2 = dataBuf(ifb)%data%midtime(i)-cryo%mjd(ilo)
           dTime1 = cryo%mjd(iup)-cryo%mjd(ilo)
           slope = (cryo%cr4K1(iup)-cryo%cr4K1(ilo))/dTime1 
           dataBuf(ifb)%data%cr4K1(i) = cryo%cr4K1(ilo)+dTime2*slope
           slope = (cryo%cr4K2(iup)-cryo%cr4K2(ilo))/dTime1 
           dataBuf(ifb)%data%cr4K2(i) = cryo%cr4K2(ilo)+dTime2*slope
        endif
     endif

     trackFlag = .false.
     if (abs(monBuf(ifb)%data%antenna_az_el(1)-130.5).lt.1.0.and.                &
   &     mod(3.6d+2,monBuf(ifb)%data%antenna_az_el(2)).lt.1.0) then
        trackFlag = .false.  ! use data with crazy tracking errors if antenna in stow position
     else if (abs(monBuf(ifb)%data%tracking_az_el(ilo,1)).gt.trackingCutoff.or.  &
   &     abs(monBuf(ifb)%data%tracking_az_el(ilo,2)).gt.trackingCutoff.or.       &
   &     abs(monBuf(ifb)%data%tracking_az_el(iup,1)).gt.trackingCutoff.or.       &
   &     abs(monBuf(ifb)%data%tracking_az_el(iup,2)).gt.trackingCutoff) then
         do j = 1, nbd
            i1 = (i-1)*nphases+1
            i2 = i*nphases
            arrayBuf(ifb)%data(j)%data(1:npix,1:nchan,i1:i2,1:nphases) =blankingRaw
            trackFlag = .true.
         enddo
     endif
     if (trackFlag) call gagout('W-SYNC: Data flagged due to large tracking errors.')
     if (ndumpSubref.gt.1) then 
        ilo = locate(monBuf(ifb)%data%focus_x_y_z(1:ndumpSubref,4),              &
   &                 dataBuf(ifb)%data%midtime(i),ndumpSubref)

        if (ilo.eq.0) then
           ilo = 1
        else if (ilo.eq.ndumpSubref) then
           ilo = ndumpSubref-1
        endif

        iup = ilo+1

        if (monBuf(ifb)%data%subScanSubref(ilo)                                 &
   &        .ne.monBuf(ifb)%data%subScanSubref(iup)) then
           if (dataBuf(ifb)%data%subscan(i).eq.monBuf(ifb)%data%subScanSubref(ilo)) &
   &       then
              ilo = ilo-1
              iup = iup-1
           else
              ilo = ilo+1
              iup = iup+1
           endif
        endif

        dTime1 = monBuf(ifb)%data%focus_x_y_z(iup,4)                           &
   &             -monBuf(ifb)%data%focus_x_y_z(ilo,4)
        dTime2 = dataBuf(ifb)%data%midtime(i)-monBuf(ifb)%data%focus_x_y_z(ilo,4)

        slope = (monBuf(ifb)%data%focus_x_y_z(iup,1)                           &
   &             -monBuf(ifb)%data%focus_x_y_z(ilo,1))                         &
   &            /dTime1 
        dataBuf(ifb)%data%focus_x_y_z(i,1) = monBuf(ifb)%data%focus_x_y_z(ilo,1)  &
   &                                      +dTime2*slope

        slope = (monBuf(ifb)%data%focus_x_y_z(iup,2)                           &
   &             -monBuf(ifb)%data%focus_x_y_z(ilo,2))                         &
   &            /dTime1 
        dataBuf(ifb)%data%focus_x_y_z(i,2) = monBuf(ifb)%data%focus_x_y_z(ilo,2)  &
   &                                      +dTime2*slope

        slope = (monBuf(ifb)%data%focus_x_y_z(iup,3)                           &
   &             -monBuf(ifb)%data%focus_x_y_z(ilo,3))                         &
   &            /dTime1 
        dataBuf(ifb)%data%focus_x_y_z(i,3) = monBuf(ifb)%data%focus_x_y_z(ilo,3)  &
   &                                      +dTime2*slope

        dataBuf(ifb)%data%dfocus_x_y_z(i,1)=monBuf(ifb)%data%dfocus_x_y_z(ilo,1)
        dataBuf(ifb)%data%dfocus_x_y_z(i,2)=monBuf(ifb)%data%dfocus_x_y_z(ilo,2)
        dataBuf(ifb)%data%dfocus_x_y_z(i,3)=monBuf(ifb)%data%dfocus_x_y_z(ilo,3)

        slope = (monBuf(ifb)%data%phi_x_y_z(iup,3)                            &
   &             -monBuf(ifb)%data%phi_x_y_z(ilo,3))                          &
   &            /dTime1 
        dataBuf(ifb)%data%phi_x_y_z(i,3) = monBuf(ifb)%data%phi_x_y_z(ilo,3)     &
   &                                    +dTime2*slope
     else
        dataBuf(ifb)%data%focus_x_y_z(i,1:3)=monBuf(ifb)%data%focus_x_y_z(1,1:3)
        dataBuf(ifb)%data%dfocus_x_y_z(i,3) = monBuf(ifb)%data%dfocus_x_y_z(1,3)
        dataBuf(ifb)%data%phi_x_y_z(i,3) = monBuf(ifb)%data%phi_x_y_z(1,3)
     endif
  enddo

  tmpScanType = scan%header%scantype
  call sic_upper(tmpScanType)
  tmpSwitchMode = febe(ifb)%header%swtchmod
  call sic_upper(tmpSwitchMode)

  if (index(tmpScanType,'CAL').ne.0.and.index(tmpSwitchMode,'FREQ').eq.0) then

     gn(ifb)%psky(:,:,:) = blankingRaw
     gn(ifb)%phot(:,:,:) = blankingRaw
     gn(ifb)%pcold(:,:,:) = blankingRaw

     if (allocated(mask)) deallocate(mask,stat=ier)
     allocate(mask(ndumpBackend,nphases),stat=ier)
     if (allocated(tmpMask)) deallocate(tmpMask,stat=ier)
     allocate(tmpMask(ndumpBackend,nphases),stat=ier)

     do i = subscanRange(1), subscanRange(2) 
        i1 = i-subscanRange(1)+1
        do j = 1, nphases
          mask(1:ndumpBackend,j) = dataBuf(ifb)%data%subscan.eq.i
        enddo
        if (index(raw(i1)%antenna(ifb)%subScanType,'Sky').ne.0.or.              &
   &        index(raw(i1)%subRef(ifb)%subScanType,'Sky').ne.0) then
           do j = 1, nbd 
              do k = 1, npix
                 do l = 1, nchan 
                    tmpMask = mask.and.                                        &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1:nphases)&
   &                   .ne.blankingRaw
                    gn(ifb)%psky(j,k,l)                                        &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1:nphases),&
   &                      tmpMask)/count(tmpMask)
                 enddo
                 meanCounts = sum(gn(ifb)%psky(j,k,:))/nchan
                 where(gn(ifb)%psky(j,k,:).lt.meanCounts/badLevel.or.          &
   &                   gn(ifb)%psky(j,k,:).gt.meanCounts*badLevel)             &
   &                   gn(ifb)%psky(j,k,:) = blankingRaw
              enddo
           enddo

        else if (index(raw(i1)%antenna(ifb)%subScanType,'Ambient').ne.0.or.     &
   &             index(raw(i1)%subRef(ifb)%subScanType,'Ambient').ne.0) then
           do j = 1, nbd 
              do k = 1, npix
                 do l = 1, nchan
                    tmpMask = mask.and.                                        &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1:nphases)&
   &                   .ne.blankingRaw
                    gn(ifb)%phot(j,k,l)                                        &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1:nphases),&
   &                      tmpMask)/count(tmpMask)
                 enddo
                 meanCounts = sum(gn(ifb)%phot(j,k,:))/nchan
                 where(gn(ifb)%phot(j,k,:).lt.meanCounts/badLevel.or.          &
   &                   gn(ifb)%phot(j,k,:).gt.meanCounts*badLevel)             &
   &                   gn(ifb)%phot(j,k,:) = blankingRaw
              enddo
           enddo

        else if (index(raw(i1)%antenna(ifb)%subScanType,'Cold').ne.0.or.        &
   &             index(raw(i1)%subRef(ifb)%subScanType,'Cold').ne.0) then
           do j = 1, nbd 
              do k = 1, npix
                 do l = 1, nchan
                    tmpMask = mask.and.                                        &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1:nphases)&
   &                   .ne.blankingRaw
                    gn(ifb)%pcold(j,k,l)                                       &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1:nphases),&
   &                      tmpMask)/count(tmpMask)
                 enddo
                 meanCounts = sum(gn(ifb)%pcold(j,k,:))/nchan
                 where(gn(ifb)%pcold(j,k,:).lt.meanCounts/badLevel.or.         &
   &                   gn(ifb)%pcold(j,k,:).gt.meanCounts*badLevel)            &
   &                   gn(ifb)%pcold(j,k,:) = blankingRaw
              enddo
           enddo

        else if (index(raw(i1)%antenna(ifb)%subScanType,'Grid').ne.0.or.        &
   &             index(raw(i1)%subRef(ifb)%subScanType,'Grid').ne.0) then
           do j = 1, nbd 
              do k = 1, npix
                 do l = 1, nchan
                    tmpMask = mask.and.                                        &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1:nphases)&
   &                   .ne.blankingRaw
                    gn(ifb)%pgrid(j,k,l)                                       &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1:nphases),&
   &                      tmpMask)/count(tmpMask)
                 enddo
                 meanCounts = abs(sum(gn(ifb)%pgrid(j,k,:))/nchan)
                 where(abs(gn(ifb)%pgrid(j,k,:)).lt.meanCounts/badLevel.or.    &
   &                   abs(gn(ifb)%pgrid(j,k,:)).gt.meanCounts*badLevel)       &
   &                   gn(ifb)%pgrid(j,k,:) = blankingRaw
              enddo
           enddo

        else 
           
           call gagout('W-SYNC: Subscan type unknown. Cannot attribute '       &
   &                   //' mean calibration counts.')

        endif

     enddo
 
     tmpFrontend = febe(ifb)%header%febe
     call sic_upper(tmpFrontend)
     if (tmpFrontend(ipc:ipc).NE.'R'.and.tmpFrontend(ipc:ipc).ne.'I') then 
        where (gn(ifb)%phot.ne.blankingRaw.and.gn(ifb)%psky.ne.blankingRaw)
           gn(ifb)%gainarray = gn(ifb)%phot-gn(ifb)%psky
        else where
           gn(ifb)%gainarray = blankingRaw 
        end where
     endif

  else if (index(tmpScanType,'CAL').ne.0.and.index(tmpSwitchMode,'FREQ').ne.0) &
 &then

     gn(ifb)%psky(:,:,:) = blankingRaw
     gn(ifb)%phot(:,:,:) = blankingRaw
     gn(ifb)%pcold(:,:,:) = blankingRaw
     gn(ifb+nfb)%psky(:,:,:) = blankingRaw
     gn(ifb+nfb)%phot(:,:,:) = blankingRaw
     gn(ifb+nfb)%pcold(:,:,:) = blankingRaw

     if (allocated(maskFSW)) deallocate(maskFSW,stat=ier)
     allocate(maskFSW(ndumpBackend),stat=ier)
     if (allocated(tmpMaskFSW)) deallocate(tmpMaskFSW,stat=ier)
     allocate(tmpMaskFSW(ndumpBackend),stat=ier)

     do i = subscanRange(1), subscanRange(2) 
        i1 = i-subscanRange(1)+1
        maskFSW(1:ndumpBackend) = dataBuf(ifb)%data%subscan.eq.i

        if (index(raw(i1)%antenna(ifb)%subScanType,'Sky').ne.0.or.              &
   &        index(raw(i1)%subRef(ifb)%subScanType,'Sky').ne.0) then
           do j = 1, nbd 
              do k = 1, npix
                 do l = 1, nchan 
                    tmpMaskFSW = maskFSW.and.                                  &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1)        &
   &                   .ne.blankingRaw
                    gn(ifb)%psky(j,k,l)                                        &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1),        &
   &                      tmpMaskFSW)/count(tmpMaskFSW)
                    tmpMaskFSW = maskFSW.and.                                  &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,2)        &
   &                   .ne.blankingRaw
                    gn(ifb+nfb)%psky(j,k,l)                                    &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,2),        &
   &                      tmpMaskFSW)/count(tmpMaskFSW)
                 enddo
                 meanCounts = sum(gn(ifb)%psky(j,k,:))/nchan
                 where(gn(ifb)%psky(j,k,:).lt.meanCounts/badLevel.or.          &
   &                   gn(ifb)%psky(j,k,:).gt.meanCounts*badLevel)             &
   &                   gn(ifb)%psky(j,k,:) = blankingRaw
                 meanCounts = sum(gn(ifb+nfb)%psky(j,k,:))/nchan
                 where(gn(ifb+nfb)%psky(j,k,:).lt.meanCounts/badLevel.or.      &
   &                   gn(ifb+nfb)%psky(j,k,:).gt.meanCounts*badLevel)         &
   &                   gn(ifb+nfb)%psky(j,k,:) = blankingRaw
              enddo
           enddo

        else if (index(raw(i1)%antenna(ifb)%subScanType,'Ambient').ne.0.or.     &
   &             index(raw(i1)%subRef(ifb)%subScanType,'Ambient').ne.0) then
           do j = 1, nbd 
              do k = 1, npix
                 do l = 1, nchan
                    tmpMaskFSW = maskFSW.and.                                  &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1)        &
   &                   .ne.blankingRaw
                    gn(ifb)%phot(j,k,l)                                        &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1),        &
   &                      tmpMaskFSW)/count(tmpMaskFSW)
                    tmpMaskFSW = maskFSW.and.                                  &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,2)        &
   &                   .ne.blankingRaw
                    gn(ifb+nfb)%phot(j,k,l)                                    &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,2),        &
   &                      tmpMaskFSW)/count(tmpMaskFSW)
                 enddo
                 meanCounts = sum(gn(ifb)%phot(j,k,:))/nchan
                 where(gn(ifb)%phot(j,k,:).lt.meanCounts/badLevel.or.          &
   &                   gn(ifb)%phot(j,k,:).gt.meanCounts*badLevel)             &
   &                   gn(ifb)%phot(j,k,:) = blankingRaw
                 meanCounts = sum(gn(ifb+nfb)%phot(j,k,:))/nchan
                 where(gn(ifb+nfb)%phot(j,k,:).lt.meanCounts/badLevel.or.      &
   &                   gn(ifb+nfb)%phot(j,k,:).gt.meanCounts*badLevel)         &
   &                   gn(ifb+nfb)%phot(j,k,:) = blankingRaw
              enddo
           enddo

        else if (index(raw(i1)%antenna(ifb)%subScanType,'Cold').ne.0.or.        &
   &             index(raw(i1)%subRef(ifb)%subScanType,'Cold').ne.0) then
           do j = 1, nbd 
              do k = 1, npix
                 do l = 1, nchan
                    tmpMaskFSW = maskFSW.and.                                  &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1)        &
   &                   .ne.blankingRaw
                    gn(ifb)%pcold(j,k,l)                                       &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1),        &
   &                      tmpMaskFSW)/count(tmpMaskFSW)
                    tmpMaskFSW = maskFSW.and.                                  &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,2)        &
   &                   .ne.blankingRaw
                    gn(ifb+nfb)%pcold(j,k,l)                                   &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,2),        &
   &                      tmpMaskFSW)/count(tmpMaskFSW)
                 enddo
                 meanCounts = sum(gn(ifb)%pcold(j,k,:))/nchan
                 where(gn(ifb)%pcold(j,k,:).lt.meanCounts/badLevel.or.         &
   &                   gn(ifb)%pcold(j,k,:).gt.meanCounts*badLevel)            &
   &                   gn(ifb)%pcold(j,k,:) = blankingRaw
                 meanCounts = sum(gn(ifb+nfb)%pcold(j,k,:))/nchan
                 where(gn(ifb+nfb)%pcold(j,k,:).lt.meanCounts/badLevel.or.     &
   &                   gn(ifb+nfb)%pcold(j,k,:).gt.meanCounts*badLevel)        &
   &                   gn(ifb+nfb)%pcold(j,k,:) = blankingRaw
              enddo
           enddo

        else if (index(raw(i1)%antenna(ifb)%subScanType,'Grid').ne.0.or.        &
   &             index(raw(i1)%subRef(ifb)%subScanType,'Grid').ne.0) then
           do j = 1, nbd 
              do k = 1, npix
                 do l = 1, nchan
                    tmpMaskFSW = maskFSW.and.                                  &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1)        &
   &                   .ne.blankingRaw
                    gn(ifb)%pgrid(j,k,l)                                       &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1),        &
   &                      tmpMaskFSW)/count(tmpMaskFSW)
                    tmpMaskFSW = maskFSW.and.                                  &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,2)        &
   &                   .ne.blankingRaw
                    gn(ifb+nfb)%pgrid(j,k,l)                                   &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,2),        &
   &                      tmpMaskFSW)/count(tmpMaskFSW)
                 enddo
                 meanCounts = abs(sum(gn(ifb)%pgrid(j,k,:))/nchan)
                 where(abs(gn(ifb)%pgrid(j,k,:)).lt.meanCounts/badLevel.or.    &
   &                   abs(gn(ifb)%pgrid(j,k,:)).gt.meanCounts*badLevel)       &
   &                   gn(ifb)%pgrid(j,k,:) = blankingRaw
                 meanCounts = abs(sum(gn(ifb+nfb)%pgrid(j,k,:))/nchan)
                 where(abs(gn(ifb+nfb)%pgrid(j,k,:)).lt.meanCounts/badLevel.or.&
   &                   abs(gn(ifb+nfb)%pgrid(j,k,:)).gt.meanCounts*badLevel)   &
   &                   gn(ifb+nfb)%pgrid(j,k,:) = blankingRaw
              enddo
           enddo

        else 
           
           call gagout('W-SYNC: Subscan type unknown. Cannot attribute '       &
   &                   //' mean calibration counts.')

        endif

     enddo
 
     tmpFrontend = febe(ifb)%header%febe
     call sic_upper(tmpFrontend)
     if (tmpFrontend(ipc:ipc).NE.'R'.and.tmpFrontend(ipc:ipc).ne.'I') then 
        where (gn(ifb)%phot.ne.blankingRaw.and.gn(ifb)%psky.ne.blankingRaw)
           gn(ifb)%gainarray = gn(ifb)%phot-gn(ifb)%psky
        else where
           gn(ifb)%gainarray = blankingRaw 
        end where
     endif
     if (tmpFrontend(ipc:ipc).NE.'R'.and.tmpFrontend(ipc:ipc).ne.'I') then 
        
        where (gn(ifb+nfb)%phot.ne.blankingRaw.and.gn(ifb+nfb)%psky.ne.       &
  &            blankingRaw)
           gn(ifb+nfb)%gainarray = gn(ifb+nfb)%phot-gn(ifb+nfb)%psky
        else where
           gn(ifb+nfb)%gainarray = blankingRaw 
        end where
     endif
    
  endif

  if (index(tmpSwitchMode,'WOB').ne.0.and.index(tmpScantype,'POINT').eq.0) then
     do i = 1, ndumpBackend
        isub = dataBuf(ifb)%data%subscan(i)
        if (raw(isub)%antenna(ifb)%subScanXOff.lt.0) then
          dataBuf(ifb)%data%longoff(i) = dataBuf(ifb)%data%longoff(i)      &
 &                                 +0.5*scan%header%wobthrow
        else
          dataBuf(ifb)%data%longoff(i) = dataBuf(ifb)%data%longoff(i)      &
 &                                 -0.5*scan%header%wobthrow
        endif
     enddo
  endif

end subroutine syncData



FUNCTION locate(xx,x,n)
IMPLICIT NONE
INTEGER n
REAL*8, DIMENSION(n) :: xx
REAL*8 :: x
INTEGER :: LOCATE
INTEGER :: jl,jm,ju
LOGICAL :: ascnd

! ADDED BY HW: returns 1 or n if x below xx(1), respectively
!              above xx(n).
!
if (x.lt.xx(1)) then
   locate = 0
   return
else if (x.gt.xx(n)) then
   locate = n
   return
endif
!
!
ascnd = (xx(n) >= xx(1))
jl = 0
ju = n+1
do
     if (ju-jl <= 1) exit
     jm=(ju+jl)/2
     if (ascnd .eqv. (x >= xx(jm))) then
         jl=jm
     else
         ju=jm
     end if
end do
if (x == xx(1)) then
    locate=1
else if (x == xx(n)) then
    locate=n-1
else
    locate=jl
endif
END FUNCTION locate

  
subroutine miraPolfit(n,n2,y1,y2,error)

   use mira

   implicit none

   integer                  :: n, n2
   real*8, dimension(n)     :: y1,y2
   logical                  :: error

   integer                  :: i, j, k, l, m
   integer                  :: nmax
   real*8                   :: chisq,chisqr,free,delta1,delta2,xterm,xx,yterm
   real*8, dimension(2*n2-1)  :: sumx
   real*8, dimension(n)     :: w
   real*8, dimension(n2)    :: coef,sumy
   real*8, dimension(n2,n2) :: a

   nmax = 2*n2-1
   sumx = 0.
   sumy = 0.
   chisq = 0.

   w = 1.
   where (y1.eq.blankingRaw) w = 0.
   do i = 1, n 
      xterm = w(i)
      do j = 1, nmax
         sumx(j) = sumx(j)+xterm
         xterm = xterm*i*1.d-3
      enddo
      yterm = w(i)*y1(i)
      do j = 1, n2
         sumy(j) = sumy(j)+yterm
         yterm = yterm*i*1.d-3
      enddo
      chisq = chisq+w(i)*y1(i)*y1(i)
   enddo
   
   do j = 1, n2
      do k = 1, n2
         m = j+k-1
         a(j,k) = sumx(m)
      enddo
   enddo

   call determ(n2,a,delta1,error)
   if (error) return 
   if (delta1.eq.0) then
       chisqr = 0.
       coef(1:n2) = 0.
       return
   else
       do l = 1, n2
          do j = 1, n2
             do k = 1, n2
                m = j+k-1
                a(j,k) = sumx(m)
             enddo
             a(j,l) = sumy(j)
          enddo
          call determ(n2,a,delta2,error)
          coef(l) = delta2/delta1
       enddo
   endif

   do j = 1, n2
      chisq = chisq-2.*coef(j)*sumy(j)
      do k = 1, n2
         m = j+k-1
         chisq = chisq+coef(j)*coef(k)*sumx(m)
      enddo
   enddo
   free = n-n2
   chisqr = chisq/free
   
   do i = 1, n
      y2(i) = coef(1) 
      xx = 1.d0*i*1.d-3
      do j = 2, n2
         y2(i) = y2(i)+coef(j)*xx**(j-1)
      enddo
   enddo
 
   return

end subroutine miraPolfit

subroutine determ(n,array,delta,error)

   implicit none
   integer                :: n
   real*8                 :: delta
   real*8, dimension(n,n) :: array
   logical                :: error

   integer                :: i, imax, j, k
   real*8                 :: aamax, dum, sum
   real*8, dimension(n)   :: vv
   real*8, dimension(n,n) :: a
   real*8, parameter      :: tiny = 1.0d-20

   delta = 1.d0
   a     = array

   do i = 1, n
      aamax = maxval(abs(a(i,1:n)))
      if (aamax.eq.0) then
        call gagout('E-GET: singular matrix in subroutine DETERM')
        error = .true.
        return
      else
        vv(i) = 1./aamax
      endif
   enddo

   do j = 1, n
      do i = 1, j-1
         sum = a(i,j)
         do k = 1, i-1
            sum = sum-a(i,k)*a(k,j)
         enddo    
         a(i,j) = sum
      enddo
      aamax = 0.
      do i = j, n
         sum = a(i,j)
         do k = 1, j-1
            sum = sum-a(i,k)*a(k,j)
         enddo
         a(i,j) = sum
         dum = vv(i)*abs(sum)
         if (dum.ge.aamax) then
            imax = i
            aamax = dum
         endif
      enddo

      if (j.ne.imax) then
         do k = 1, n
            dum = a(imax,k)
            a(imax,k) = a(j,k)
            a(j,k) = dum
         enddo
         delta = -delta
         vv(imax) = vv(j)
      endif
      if (a(j,j).eq.0) a(j,j) = tiny
      if (j.ne.n) then
         dum = 1./a(j,j)
         do i = j+1, n
            a(i,j) = a(i,j)*dum
         enddo
      endif
   enddo

   do j = 1, n
      delta = delta*a(j,j)
   enddo

   return

end subroutine determ
