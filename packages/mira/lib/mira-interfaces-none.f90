module mira_interfaces_none
  interface
       subroutine calibrate(line,error)
    !
    ! Called by subroutine RUN, command CALIBRATE [ifb|all].
    !
    ! Input:
    ! LINE (character) : command line
    !
    ! Output:
    ! ERROR (logical)  : error return flag
    ! Calibration parameters in structure GAINS(1:NFB)%TAUZEN etc. where
    ! NFB is the number of frontend/backend combinations.
    ! For details see See module mira in mira_use.f90 and the MIRA manual,
    ! section 5.
    !
       use mira
       use gildas_def
       use gkernel_types
       !
    end subroutine calibrate
  end interface
  !
  interface
    subroutine apply_tcal(ifb,jfb,changefebecal,nrecords,error)
    !
    ! Called by subroutine CALIBRATE. Applies the calibration temperature
    ! to the raw data (after division by the channel gains in APPLY_GAINS).
    ! A separate calibration temperature is applied to each spectral
    ! baseband or to each spectral channel (if calByChannel = .true.,
    ! interactively set by the user by "let calByChannel yes"). The logical
    ! SIC variable calByChannel is read by MIRA in subroutine getFits.
    !
    ! Input:
    ! IFB (integer)           : frontend-backend unit to be calibrated
    ! JFB (integer)           : frontend-backedn unit containing the
    !                           calibration parameters to be applied here.
    ! changeFebeCal (logical) :
    ! NRECORDS (integer)      : number of records in scan
    !
    ! Output:
    ! ERROR (logical)         : error return flag
    !
    ! Input/output:
    !
    ! ARRAY(1:NFB)%DATA(1:NBD)%DATA (double), where NFB is the number of
    ! frontend-backend combinations, and NBD is the number of spectral
    ! basebands. For details, see module mira in mira_use.f90 and the MIRA
    ! manual, section 5.
    !
    ! On successful execution, the logical flag
    ! REDUCE(IFB)%CALIBRATION_DONE(2) is set to .true. (IFB as on input).
    !
       use mira
       use gildas_def
       use chopper_def
       !
    end subroutine apply_tcal
  end interface
  !
  interface
    subroutine apply_tcal_fsw(ifb,jfb,changefebecal,nrecords,error)
    !
    ! Same as subroutine APPLY_TCAL but for calibrations done in switch mode
    ! frequency. The calibration parameters for the phase 1 (lower frequency)
    ! are in structure gains(1:nfb), those for phase 2 (upper frequency) are
    ! in gains(nfb+1:2*nfb), where nfb is the number of frontend-backend
    ! units.
    !
       use mira
       use gildas_def
       use chopper_def
       !
    end subroutine apply_tcal_fsw
  end interface
  !
  interface
    subroutine apply_gains(ifb,jfb,changefebecal,nrecords,error)
    !
    ! Called by subroutine CALIBRATE. Applies the channel gains
    ! in GAINS(JFB)%GAINARRAY (= count rate on hot load - count rate on sky)
    ! to the raw data in ARRAY(IFB)%DATA%DATA. The gainarray is allocated in
    ! subroutine getFits, and written in subroutine syncData or syncDataNew.
    !
    ! Input:
    ! IFB (integer)           : frontend-backend unit to be calibrated
    ! JFB (integer)           : frontend-backedn unit containing the
    !                           calibration parameters to be applied here.
    ! changeFebeCal (logical) :
    ! NRECORDS (integer)      : number of records in scan
    !
    ! Output:
    ! ERROR (logical)         : error return flag
    !
    ! Input/output:
    ! ARRAY(1:NFB)%DATA(1:NBD)%DATA (double), where NFB is the number of
    ! frontend-backend combinations, and NBD is the number of spectral
    ! basebands. For details, see module mira in mira_use.f90 and the MIRA
    ! manual, section 5.
    !
    ! On successful execution, the logical flag
    ! REDUCE(IFB)%CALIBRATION_DONE(1) is set to .true. (IFB as on input).
    !
       use mira
       use gildas_def
       !
    end subroutine apply_gains
  end interface
  !
  interface
    subroutine apply_gains_fsw(ifb,jfb,changefebecal,nrecords,error)
    !
    ! Same as subroutine APPLY_GAINS_FSW, but for the calibration taken
    ! in switch mode frequency (i.e. channel gains for frequency switching
    ! phase 1 are applied to the raw data of frequency switching phase 1,
    ! respectively the gains of phase 2 to the raw data of phase 2).
    !
       use mira
       use gildas_def
       !
    end subroutine apply_gains_fsw
  end interface
  !
  interface
    subroutine subtract_off(ifb,nrecords,wmode,window,nwin,   &
         mask,nmask,error)
    !
    ! Called by subroutine CALIBRATE. Performs the subtraction ON-OFF
    ! for multiple-phase data, and concatenates adjacent spectral basebands
    ! to a single output spectrum (to be written to the outputfile for
    ! CLASS).
    !
    ! Also called by subroutine SOLVE for uncalibrated pointings and for
    ! focus measurements.
    !
    ! Input:
    ! ifb           integer         number of frontend/backend unit
    ! nrecords      integer         number of backend data records
    ! wmode         character       weight mode for total power subtraction
    !                               from on-the-fly scans.
    ! window        double          lower and upper boundary of spectral region
    !                               to be avoided for evaluation of total power
    !                               (only for weight mode TOTAL)
    ! nwin          integer         number of such windows
    ! mask          integer         If total power reference comes from the OTF map
    !                               itself: first and last dump number of an OTF
    !                               subscan framing the emission region
    ! nmask         integer         number of such masks
    !
    ! ARRAY(1:NFB)%DATA(1:NBD)%DATA (double), where NFB is the number of
    ! spectral frontend-backend units, and NBD is the number of spectral
    ! basebands.
    !
    ! Output:
    ! ERROR (logical)               : error return flag
    ! DATA(IFB)%DATA%RDATA (double) : the spectrum of frontend-backend unit
    !                                 IFB (observing modi TRACK and ONOFF).
    ! DATA(IFB)%DATA%OTFDATA (double) : same as above, but for the o
    !
    !
    ! On successful execution, the logical flag
    ! REDUCE(IFB)%CALIBRATION_DONE(3) is set to .true. (IFB as on input).
    !
    !
       use mira
       use gildas_def
       use linearregression
       !
    end subroutine subtract_off
  end interface
  !
  interface
    subroutine subtract_offE(ifb,nrecords,wmode,window,nwin,   &
         mask,nmask,error)
    !
    ! Called by subroutine CALIBRATE. Performs the subtraction ON-OFF
    ! for multiple-phase data, and concatenates adjacent spectral basebands
    ! to a single output spectrum (to be written to the outputfile for
    ! CLASS).
    !
    ! Also called by subroutine SOLVE for uncalibrated pointings and for
    ! focus measurements.
    !
    ! Input:
    ! ifb           integer         number of frontend/backend unit
    ! nrecords      integer         number of backend data records
    ! wmode         character       weight mode for total power subtraction
    !                               from on-the-fly scans.
    ! window        double          lower and upper boundary of spectral region
    !                               to be avoided for evaluation of total power
    !                               (only for weight mode TOTAL)
    ! nwin          integer         number of such windows
    ! mask          integer         If total power reference comes from the OTF map
    !                               itself: first and last dump number of an OTF
    !                               subscan framing the emission region
    ! nmask         integer         number of such masks
    !
    ! ARRAY(1:NFB)%DATA(1:NBD)%DATA (double), where NFB is the number of
    ! spectral frontend-backend units, and NBD is the number of spectral
    ! basebands.
    !
    ! Output:
    ! ERROR (logical)               : error return flag
    ! DATA(IFB)%DATA%RDATA (double) : the spectrum of frontend-backend unit
    !                                 IFB (observing modi TRACK and ONOFF).
    ! DATA(IFB)%DATA%OTFDATA (double) : same as above, but for the o
    !
    !
    ! On successful execution, the logical flag
    ! REDUCE(IFB)%CALIBRATION_DONE(3) is set to .true. (IFB as on input).
    !
       use mira
       use gildas_def
       use linearregression
       !
    end subroutine subtract_offE
  end interface
  !
  interface
    subroutine do_tcal(ifb,nfreq,nrecords,fixopacity,opac,   &
         fixpwv,pwv0,error)
    !
    ! Called by subroutine getmira (source code of call is in file mira/lib/get.f90)
    ! if reading a calibration scan, and by subroutine OVERRIDE (i.e., if Tcal needs
    ! to be re-evaluated after a manual change of calibration parameters, see
    ! HELP OVERRIDE or user's manual). Updates MIRA structure GAINS.
    !
    ! Input:
    ! ifb           integer         number of frontend/backend combination
    ! nfreq         integer         number of frequency points to be evaluated
    !                               simultaneously (so far always nfreq = 1 because
    !                               subroutine mira_chopper is called separately for
    !                               each frequency band or, if opted for, spectral
    !                               channel.
    ! nrecords      integer         number of backend dumps in this scan
    ! fixopacity    logical         .true. if Tsys and Tcal are to be evaluated for 
    !                               a given opacity (at a given frequency).
    ! opac          double          Value of that opacity.
    ! fixpwv        logical         .true. if pwv is kept fixed, and opacities,
    !                               Tsys and Tcal are evaluated for that pwv.
    !                               Used e.g. for channel-wise calibration with pwv 
    !                               obtained from a wide spectral band.
    ! pwv0          double          Value of that opacity.
    !
    ! Output: 
    ! error         logical         .false. upon successful completion.
    !
       use mira
       use gildas_def
       use chopper_def
       !
    end subroutine do_tcal
  end interface
  !
  interface
    subroutine do_tcal_fsw(ifb,nfreq,nrecords,fixopacity,opac,   &
         fixpwv,pwv0,error)
    !
    ! As for subroutine do_tcal, but for calibrations taken with frequency 
    ! switching (in order to calibrate the FLO and FHI phases separately).
    ! Experimental.
    !
       use mira
       use gildas_def
       use chopper_def
       !
    end subroutine do_tcal_fsw
  end interface
  !
  interface
    subroutine do_xpol(ifb,nrecords,error)
    !
    ! Called by subroutine getfits.f90
    ! For calibration scans with pako option "/grid yes" and
    ! VESPA in XPOL mode, calculates the differential phase
    ! between the horizontal and vertical polarization of the
    ! EMIR band in frontend-backend-unit ifb, and writes the 
    ! result into gains(ifb)%phasearray.
    !
    ! Input: 
    ! ifb           integer         number of frontend/backend combination
    ! nrecords      integer         number of dumps for this scan and backend.
    !
    ! Output:
    ! error         logical         .false. upon succesful completion
    !
       use mira
    end subroutine do_xpol
  end interface
  !
  interface
    subroutine apply_xpol(ifb,nrecords,error)
    !
    ! For polarization observations only (i.e., VESPA in XPOL mode).
    ! Applies the phase correction measured with a "cal /grid yes"
    ! measurement to the real and imaginary part of the complex
    ! cross correlation (such that they become, after calibration,
    ! Stokes U and V in the Nasmyth reference system). Update
    ! array(ifb)%data(j)%data with j=1 to number of basebands.
    !
    ! Input:
    ! ifb           integer         number of frontend/backend combination
    !                               to be phase-calibrated
    ! nrecords      integer         number of dumps in this backend and scan
    !
    ! Output:
    ! error         logical         .false. upon successful completion
    !
       use mira
    end subroutine apply_xpol
  end interface
  !
  interface
    function airmdry(elev)
    !
    ! Calculates the airmass as a function of elevation
    ! (input variable "elev"), taking into account the
    ! curvature of the atmosphere (therefore, the atmospheric
    ! scale height is needed, and the function needs to
    ! distinguish between the dry and wet atmosphere.
    !
    !
       use mira
    end function airmdry
  end interface
  !
  interface
    function airmwet(elev)
    !
    ! Calculates the airmass as a function of elevation
    ! (input variable "elev"), taking into account the
    ! curvature of the atmosphere (therefore, the
    ! atmospheric scale height is needed, and the function
    ! needs to distinguish between the dry and wet atmosphere.
    !
       use mira
    end function airmwet
  end interface
  !
  interface
    subroutine mira_chopper(search,fixOpacity,opac,fixPWV,pwv0,tel,nfreq,freqs, &
    &                       loads,recs,atms,tcals,tsyss,quiet,errors)
    !
    ! Called by MIRA subroutine do_tcal (in mira/lib/calibrate.f90).
    ! Replaces telcal's subroutine chopper. Calls gildas interface
    ! to ATM at packages/atm (which in turn calls legacy/atm/lib).
    !
    !
      use chopper_def
      type (chop_mode), intent(in)    :: search        ! search criteria
      type (telescope), intent(in)    :: tel           ! telescope data
      integer,          intent(in)    :: nfreq         ! number of frequency points
      type (dsb_value), intent(in)    :: freqs(nfreq)  ! signa/image frequencies
      type (chop_meas), intent(in)    :: loads(nfreq)  ! cal counts (sky,hot,cold)
      type (mixer),     intent(inout) :: recs(nfreq)   ! rx efficiencies and gains
      type (atm_prop),  intent(inout) :: atms(nfreq)   ! atmspheric data
      type (dsb_value), intent(out)   :: tcals(nfreq)  ! calibration temperatures
      type (dsb_value), intent(out)   :: tsyss(nfreq)  ! system temperatures
      logical,          intent(out)   :: errors(nfreq) ! error flags
      logical,          intent(in)    :: quiet         ! suppress verbose output
      logical,          intent(in)    :: fixOpacity    ! if fixOpacity: no iterations, but
                                                       ! get Tcal,Tsys for fixed opacity
      real(8),          intent(in)    :: opac          ! value of that opacity
      logical,          intent(in)    :: fixPWV        ! if fixPWD: no iterations, but
                                                       ! get Tcal,Tsys, and taus for fixed pwv 
      real(8),          intent(in)    :: pwv0          ! value of that pwv 
    end subroutine mira_chopper
  end interface
  !
  interface
    function mira_airmass(el,quiet,error)
      ! EXPORT
      !----------------------------------------------------------------------
      ! Compute airmass number corresponding to elevation EL
      ! Use a curved atmosphere, equivalent height 5.5 km
      !----------------------------------------------------------------------
      real(8), intent(in)  :: el      ! Elevation [radian]
      logical, intent(out) :: error   ! Error flag
      logical, intent(in)  :: quiet   ! suppresses error messages
      real(8)              :: mira_airmass ! Airmass
    end function mira_airmass
  end interface
  !
  interface
    subroutine mira_atm_dsb_transmission(tel,ier)
      ! EXPORT
      use chopper_def
      !----------------------------------------------------------------------
      !
      !----------------------------------------------------------------------
      type (telescope), intent(in)    :: tel  !
      integer,          intent(inout) :: ier  ! Error flag
    end subroutine mira_atm_dsb_transmission
  end interface
  !
  interface
          SUBROUTINE mnbrak(ax,bx,cx,fa,fb,fc,func)
    !
    ! Subroutine from Numerical Recipes for bracketing
    ! a minimum using the golde rule.
    !
    ! Input:
    !
          end subroutine mnbrak
  end interface
  !
  interface
          FUNCTION brent(ax,bx,cx,f,tol,xmin,error)
          end function brent
  end interface
  !
  interface
          FUNCTION chiSquaredAtm(pwv)
          use chopper_def
          real(8), intent(in) :: pwv
          integer :: ier
          real(8) :: chiSquaredAtm, temi
          END FUNCTION chiSquaredAtm
  end interface
  !
  interface
          FUNCTION chiSquaredTau(pwv)
          use chopper_def
          real(8), intent(in) :: pwv
          integer :: ier
          real(8) :: chiSquaredTau, tau
          END FUNCTION chiSquaredTau
  end interface
  !
  interface
    SUBROUTINE DESPIKE(LINE,ERROR)
    !
    ! Called by subroutine RUN_MIRA (source code is in mira/lib/run.f90).
    ! Used for despiking of data. See online help or user's guide for options.
    !
    ! Input: command line from interactive MIRA session.
    ! Output: error flag.
    !
      USE mira
      USE gildas_def
      use gkernel_types
    end subroutine despike
  end interface
  !
  interface
    subroutine directory(line,error)
    !
    ! Defines the directory containing the imbfits raw data.
    ! Checks whether the directory exists and whether there are
    ! data in it.
    !
    ! Called by: load_mira (in load.f90, MIRA command "file in")
    ! Input: command line from Mira (see "help file")
    ! Output: error flag.
    !
    !
       use mira
       use gildas_def
       use gkernel_types
       !
    end subroutine directory
  end interface
  !
  interface
    subroutine findmira(line,error)
    !
    ! Searches the input directory for imbfits raw data, and calls
    ! subroutine getattributes to assign values to MIRA structure
    ! list(iobs), where iobs is the number of the observation in the
    ! index list.
    !
    ! Called by MIRA command "find" (see subroutine run_mira, in mira/lib/run.f90).
    ! See online help or user's ! guide for earch options.
    ! Input: command line from Mira (see "help find")
    ! Output: error flag.
    !
       !
       use       mira
       use       gildas_def
       use       gkernel_types
       !
    end subroutine findmira
  end interface
  !
  interface
    subroutine getattributes(directory,option,nfile,filelist,   &
         lastnumfound,lastdatefound,   &
         nobs,error)
    !
    ! Called by subroutine findmira. Opens fits files found for extracting
    ! information needed to attribute values to structure LIST.
    !
    !
       use mira
       !
    end subroutine getattributes
  end interface
  !
  interface
    subroutine indexx(n,arr,indx)
    !
    ! Called by subroutines findmira, getfits, getfitsNew, mira_list, 
    ! solve_pointing_mira, syncDataNew.
    !
    ! Returns an index list "indx" for sorting array "arr" in ascending order.
    ! (i.e., indx(1) yields the element number of arr with the smallest value,
    !  indx(n) that of the element of arr with the largest value.
    !
    ! Input:        n       integer         number of elements of vector arr
    !               arr     double          n-element vector to be sorted
    !               indx    integer         n-element vector with sort index
    !
    ! Source code is from Numerical Recipes.
    !
       integer n,indx(n),m,nstack
       real*8 arr(n)
    end subroutine indexx
  end interface
  !
  interface
    subroutine rank(n,indx,irank)
    !
    ! Called by subroutines getfits and getfitsNew.
    ! Yields a ranking list, i.e. says which position a given element of
    ! vector "arr" has in a listing of elementes in ascending order.
    !
    ! Input:        n       integer         number of elements of vector indx
    !               indx    integer         index list for sorting in ascending order
    !                                       (output from subroutine indexx)
    !               irank   integer         n-element vector  
    !
    ! Source code is from Numerical Recipes.
    !
       integer n,indx(n),irank(n)
    end subroutine rank
  end interface
  !
  interface
    subroutine renBand(n,indx,outdx)
    !
    ! If band numbers are non-consecutive
    ! rename them consecutively
    !
        integer n, indx(n), outdx(n)
    end subroutine renBand
  end interface
  !
  interface
    subroutine flagData(line,error)
    !
    ! Used for attributing the blanking value of raw data (blankingRaw), 
    ! respectively calibrated data (blankingRed) for a given spectral
    ! channel, receiver pixel, record, or spectral baseband.
    !
    ! Called by run_mira (interactive command: FLAG, see online help or 
    ! users' manual for options).
    !
    ! Input:        line    character       command line in interactive MIRA session
    ! Output:       error   logical         error flag
    !
    !
     USE mira
    end subroutine flagData
  end interface
  !
  interface
    subroutine getmira(line,error)
    !
    ! Searches the input directory for imbfits raw data.
    !
    ! Called by: load_mira (in load.f90, MIRA command "SCAN", accepts several search options).)
    ! Input: command line from Mira. For help about options: "MIRA\help find")
    ! Output: error flag.
    !
       !
       use mira
       use gildas_def
       use gkernel_types
       !
    end subroutine getmira
  end interface
  !
  interface
    subroutine getfitsNew(filename,tmpbackend,stepcompress,   &
         cryoextension,derotextension,trackingcutoff,         &
         currentindex,firstifb,ncf,nfb,subscanRange,nosky,error)
    !
    ! Reads the imbfits file (version >= 2.0) into MIRA's data structures (define in mira_use.f90).
    ! Called by subroutine getmira. See "MIRA\help get" for possible options.
    !
    ! Input: character(len=200) :: filename       ! with full path
    !        character(len=23)  :: tmpbackend     ! backend name (converted to upper case) as appearing in imbfits
    !        real*8             :: stepcompress   ! Elementary integration time for data compression.
    !        logical            :: cryoextension  ! Flag indicating whether imbfits has a binary extension for the
    !                                             ! HERA cryostat (one per subscan).
    !        logical            :: derotextension ! Idem for HERA derotator extension (one per subscan).
    !        real*8             :: trackingcutoff ! Backend data framed by antenna data with
    !                                             ! tracking errors > trackingcutoff [degree] are assigned the 
    !                                             ! blanking value (in subroutine syncData.f90, respectively 
    !                                             ! syncDataNew.f90). 
    ! 
    ! Overwrite hot load from receiver table if available 2011-04-14 AS
    !
       use mira
       use gbl_constant
       use fit_definitions
       use pcross_definitions
       use focus_definitions
       !
    end subroutine getfitsNew
  end interface
  !
  interface
    subroutine dateobstomjd(timestring,mjd,ier)
       !
    end subroutine dateobstomjd
  end interface
  !
  interface
    subroutine sla_cldj (iy, im, id, djm, j)
       !+
       !     - - - - -
       !      C L D J
       !     - - - - -
       !
       !  Gregorian Calendar to Modified Julian Date
       !
       !  Given:
       !     IY,IM,ID     int    year, month, day in Gregorian calendar
       !
       !  Returned:
       !     DJM          dp     modified Julian Date (JD-2400000.5) for 0 hrs
       !     J            int    status:
       !                           0 = OK
       !                           1 = bad year   (MJD not computed)
       !                           2 = bad month  (MJD not computed)
       !                           3 = bad day    (MJD computed)
       !
       !  The year must be -4699 (i.e. 4700BC) or later.
       !
       !  The algorithm is derived from that of Hatcher 1984
       !  (QJRAS 25, 53-55).
       !
       !  P.T.Wallace   Starlink   11 March 1998
       !
       !  Copyright (C) 1998 Rutherford Appleton Laboratory
       !-
    end subroutine sla_cldj
  end interface
  !
  interface
    subroutine medianinteger(x,n,out)
       !
       integer i, n
       integer indx(n),x(n)
       real*4 out
    end subroutine medianinteger
  end interface
  !
  interface
    subroutine mediandouble(x,n,out)
       !
       integer i, n
       integer indx(n)
       real*8  out, x(n)
    end subroutine mediandouble
  end interface
  !
  interface
    subroutine computeoffset(nfb,noff,nrecords,sysoff,scantype,   &
         switchmode,nosky,error)
       use mira
    end subroutine computeoffset
  end interface
  !
  interface
    subroutine convertCoord(xoff,yoff,sysoff,isign,error)
       use      mira
    end subroutine convertCoord
  end interface
  !
  interface
    subroutine freescan(var,error)
       use mira
    end subroutine freescan
  end interface
  !
  interface
    subroutine freefebe(var,error)
       use mira
    end subroutine freefebe
  end interface
  !
  interface
    subroutine freedata(var,error)
       use mira
    end subroutine freedata
  end interface
  !
  interface
    subroutine freearray(var,error)
       use mira
    end subroutine freearray
  end interface
  !
  interface
    subroutine freemon(var,error)
       use mira
    end subroutine freemon
  end interface
  !
  interface
    subroutine freegains(var,error)
       use mira
    end subroutine freegains
  end interface
  !
  interface
    subroutine freeraw(var,error)
       use mira
    end subroutine freeraw
  end interface
  !
  interface
    subroutine freecryo(var,error)
       use mira
    end subroutine freecryo
  end interface
  !
  interface
    subroutine freederot(var,error)
       use mira
    end subroutine freederot
  end interface
  !
  interface
    subroutine freelist(var,error)
       use mira
    end subroutine freelist
  end interface
  !
  interface
    subroutine getfits(filename,tmpbackend,stepcompress,   &
         cryoextension,derotextension,trackingcutoff,   &
         currentindex,firstifb,ncf,nfb,subscanRange,nosky,error)
    !
    ! Reads the imbfits file (version < 2.0) into MIRA's data structures (define in
    ! mira_use.f90). Called by subroutine getmira. See "MIRA\help get" for possible options.
    !
       !
       use mira
       use gbl_constant
       use fit_definitions
       use pcross_definitions
       use focus_definitions
       !
    end subroutine getfits
  end interface
  !
  interface
    subroutine init
    !
    ! called by load_mira
    !
       !
       use mira
       !
    end subroutine init
  end interface
  !
  interface
    subroutine exit_mira
       use mira
    end subroutine exit_mira
  end interface
  !
  interface
    subroutine mira_list(line,error)
    !
    ! Called by MIRA subroutine run_mira (source code in mira/lib/run.f90), command LIST,
    ! and SUBROUTINE FINDMIRA (see source code in find.f90).
    ! See interactive HELP or user's guide for options.
    !
    ! Input: command line from interactive Mira session
    ! Output: error flag.
    !
       use mira
       use pcross_definitions
       !
    end subroutine mira_list
  end interface
  !
  interface
    subroutine fluxfill(lun,freq,iyear,imon,iday,month,   &
         format,receiver,tau,tsys,trec,ifb,doflag,flagFebe,flagSubscan,nFlagSubscan)
    !
    ! Called by mira_list. Creates an output ASCII file with flux densities from
    ! monitoring sessions.
    !
       use mira
       use pcross_definitions
    end subroutine fluxfill
  end interface
  !
  interface
    subroutine fluxBackend(lun,freq,iyear,imon,iday,month,ifb,nbd,nchan, &
                           tau,pwv,tsys,trec,fluxCorr)
    !
    ! Same as fluxfill but for on-offs of continuum souces observed with a
    ! spectrometer and wobbler switch mode. Needs to be debugged ?
    !
       use mira
       use pcross_definitions
    end subroutine fluxBackend
  end interface
  !
  interface
    subroutine load_mira
    !
    ! Called by driver routine mira/main/mira.f90 
    !
       !
       use mira
       !
    end subroutine load_mira
  end interface
  !
  interface
    subroutine plot(line,error)
       !
       use mira
       use gildas_def
       use gkernel_types
       !
    end subroutine plot
  end interface
  !
  interface
    subroutine run_mira(line,comm,error)
       !
       use mira
       !
    end subroutine run_mira
  end interface
  !
  interface
    subroutine show(line,error)
    !
    ! Unused (command is not defined in subroutine load_mira).
    !
       !
       use mira
       !
       integer            :: nc
       character(len = *) :: line
       character          :: ch*72
       logical            :: case_array, case_febe, case_general,   &
       !
    end subroutine show
  end interface
  !
  interface
    subroutine solve(line,error)
    !
    ! Called by subroutine run_mira (MIRA command "solve"). 
    ! Calls the appropriate subroutines and plotting procedures
    ! for solving cross-scan pointings, focus measurements (axial or lateral ones)
    ! and skydips.
    !
    ! Input: command line from interactive MIRA session or ODP script.
    ! Output: error flag.
    !
       use mira
       !
    end subroutine solve
  end interface
  !
  interface
    subroutine solve_pointing_mira(ifb,ipix,nbin,ibase,line)
    !
    ! Called by subroutine SOLVE.
    ! solve_pointing_mira prepares (subscan or coadded Az- and
    ! El-subscan, after removal of linear baselines) data for
    ! subsequent call of telcal's subroutine fit_1d, writes
    ! the results into the corresponding MIRA and SIC structures,
    ! and calls subroutines protoResultsPointing to write an XML
    ! file with the results.
    !
       use mira
       use fit
       use gildas_def
       use gkernel_types
       use pcross_definitions
       use fit_definitions
       use linearregression
       !
    end subroutine solve_pointing_mira
  end interface
  !
  interface
    subroutine pointing_to_sic(dat,fun,profile,error)
    !
    ! Creates a SIC copy of Mira structure PNT (pointing results for
    ! co-added azimuth and elevation data). Results from individual
    ! pointing subscans are in telcal/sic structure PCROSS.
    !
       use gildas_def
       use gkernel_types
       use fit_definitions
       !
    end subroutine pointing_to_sic
  end interface
  !
  interface
    subroutine solve_xyzfocus(ifb,ipix,idir)
    !
    ! Called by subroutine SOLVE.
    ! solve_xyzfocus prepares the data of a focus scan
    ! subsequent call of telcal's subroutine fit_1d, writes
    ! the results into the corresponding MIRA and SIC structures,
    ! and calls subroutines protoResultsFocus to write an XML
    ! file with the results.
    !
       use mira
       use gildas_def
       use gkernel_types
       use fit_definitions
       use focus_definitions
       !
    end subroutine solve_xyzfocus
  end interface
  !
  interface
    subroutine solve_tip(ifb,ipix)
    !
    ! Called from run_mira (MIRA command SOLVE if current scan is a 
    ! skytip). This subroutine is merely an interface to SIC routine
    ! where the skydip is solved (in mira/pro/p_plot_tip.mira).
    !
    !
       integer           :: ifb, ipix
    end subroutine solve_tip
  end interface
  !
  interface
    subroutine linregress(basedata,basepar,bval)
       !
       use linearregression
       !
    end subroutine linregress
  end interface
  !
  interface
    subroutine syncData(ifb,nfb,subscanRange,error)
    !
    ! Called by subroutines getfits and getfitsNew.
    ! Interpolates antenna and subreflector data at backend timestamp
    ! (linear between preceeding and following antenna and subreflector dump).
    ! Results are written into Mira structures DATA(ifb) (where ifb is the frontend-
    ! backend number).
    !
    ! syncData checks the consistency of the timing (e.g., that each backend time
    ! stamp between imbfits data header entries dateObs and dateEnd is framed by
    ! antenna and subreflector data points at either side).
    !
    !
      use mira
      use gildas_def
    end subroutine syncData
  end interface
  !
  interface
    FUNCTION locate(xx,x,n)
    END FUNCTION locate
  end interface
  !
  interface
    subroutine miraPolfit(n,n2,y1,y2,error)
       use mira
    end subroutine miraPolfit
  end interface
  !
  interface
    subroutine determ(n,array,delta,error)
    end subroutine determ
  end interface
  !
  interface
    subroutine syncDataNew(ifb,nfb,subscanRange,error)
    !
    ! Called by subroutines getfits and getfitsNew.
    ! Interpolates antenna and subreflector data at backend timestamp
    ! (linear between preceeding and following antenna and subreflector dump).
    ! Results are written into Mira structures DATA(ifb) (where ifb is the frontend-
    ! backend number).
    !
    ! Unlike subroutine syncData, does not apply any timing check.
    !
      use mira
      use gildas_def
    end subroutine syncDataNew
  end interface
  !
  interface
    subroutine mira_variable(line,error)
    !
    ! Called by MIRA command VARIABLE (see online help and users' manual
    ! for options). Calling subroutine mira_to_sic.
    !
    ! Input:  line      character      command line from interactive MIRA session
    ! Output: error    logical        error flag
    !
       use mira
       !
    end subroutine mira_variable
  end interface
  !
  interface
    subroutine mira_to_sic
    !
    ! Contains the following entries:
    ! prim_to_sic, scan_to_sic, febe_to_sic, data_to_sic, array_to_sic,
    ! mon_to_sic, gains_to_sic, choice_to_sic, reduce_to_sic, raw_to_sic,
    ! cryo_to_sic, derot_to_sic, list_to_sic.
    !
    ! Called by the following subroutines:
    !
    ! subroutine            comments
    ! calibrate             if SIC structure GAINS or REDUCE already declared
    ! despike               if SIC structure REDUCE already declared
    ! findmira              if SIC structures CHOICE and LIST already declared
    ! getmira               for update of already declared SIC structures
    !
    ! Note: the subroutine pointing_to_sic (source code in mira/lib/solve.f90) for declaring
    ! and attributing the SIC structure PNT for coadded pointing subscans is called
    ! by solve_pointing_mira.
    !
       use mira
       use gildas_def
       use gkernel_types
       !
    end subroutine mira_to_sic
  end interface
  !
  interface
    subroutine mira_write(line,error)
    !
    ! Called by subroutine run_mira (interactively by MIRA command WRITE).
    ! Prepares things for call to subroutine writeClass.
    !
    ! Input: MIRA command line.
    ! Output: error flag (if .not.error then data of the current scan are
    !         written into the output class file.
    !
       use mira
    !
    end subroutine mira_write
  end interface
  !
  interface
    subroutine writeClass(ifb,ipix,subscan,irec,init,error)
    !
    ! Called by subroutine mira_write.
    ! 
    ! Input:
    ! ifb       integer     input   Number of frontend-backend combination to be
    !                               written to CLASS output file.
    ! ipix      integer     input   Number of pixel (of HERA, otherwise ipix = 1)
    !                               to be written to CLASS output file.
    ! init      logical     input   Set to .true. if all CLASS header entries are 
    !                               to be determined. For the records of an OTF map
    !                               whose CLASS spectra have many identical header
    !                               entries.
    ! error     logical     output  error flag
    !
    !
       use mira
       use fit
       use class_api
       use class_RT
       use class_setup
       use gildas_def
       use gbl_constant
       use pcross_definitions
       use fit_definitions
       use gbl_format
    end subroutine writeClass
  end interface
  !
  interface
    subroutine writeMbfits(error)
    !
    ! Yet to be done. Was thought to convert IMBFITS (IRAM 30m) to MBFITS (MPIfR
    ! Apex).
    ! 
    !
       use mira
    !
    end subroutine writeMbfits
  end interface
  !
  interface
    SUBROUTINE protoResultsFocus(ifb,ipix,fun,error)
    !
    ! Writes result from a focus scans (evaluated by MIRA with command
    ! SOLVE) into an XML file to be used by the NCS for monitoring and feedback
    ! of the focus correction.
    !
       Use mira
       Use modulePakoXML
       Use moduleResultsToNCS
       Use fit_definitions
       Use focus_definitions
       Use gbl_format
    END SUBROUTINE protoResultsFocus
  end interface
  !
  interface
    SUBROUTINE protoResultsPointing(ifb,ipix,error)
    !
    ! Writes results from pointing cross scans (evaluated by MIRA with command
    ! SOLVE) into an XML file to be used by the NCS for monitoring and feedback
    ! of pointing corrections.
    !
       Use mira
       Use modulePakoXML
       Use moduleResultsToNCS
       Use fit_definitions
       USe pcross_definitions
       Use fit 
       Use gbl_format
    END SUBROUTINE protoResultsPointing
  end interface
  !
  interface
    SUBROUTINE protoResultsCalibration(error)
       Use mira
       Use modulePakoXML
       Use moduleResultsToNCS
       Use gbl_format
    END SUBROUTINE protoResultsCalibration
  end interface
  !
  interface
    SUBROUTINE OVERRIDE(LINE,ERROR)
      USE mira
    END SUBROUTINE OVERRIDE
  end interface
  !
  interface
    subroutine freeNewpar(var,error)
      use mira
    end subroutine freeNewpar
  end interface
  !
  interface
    subroutine do_trx(ifb,error)
      USE mira
    end subroutine do_trx
  end interface
  !
end module mira_interfaces_none
