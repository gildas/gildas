subroutine mira_variable(line,error)
!
! Called by MIRA command VARIABLE (see online help and users' manual
! for options). Calling subroutine mira_to_sic.
!
! Input:  line      character      command line from interactive MIRA session
! Output: error    logical        error flag
!
   use mira
   !
   implicit none
   !
   integer               :: ifb, lc
   character(len = *)    :: line
   character(len = 80)   :: section
   character(len = 5)    :: access
   logical error, readonly, sic_present
   !
   call sic_ch(line,0,1,section,lc,.false.,error)
   !
   if (.not.sic_present(0,1)) then
      call gagout('E-VARIABLE, need a section name or *.')
      error = .true.
      return
   endif
   !
   if (sic_present(0,2)) then
      call sic_ch(line,0,2,access,lc,.false.,error)
      if (index(access,'W').ne.0.or.index(access,'w').ne.0) then
         readonly = .false.
      else
         readonly = .true.
      endif
   else
      readonly = .true.
   endif
   !
   if (trim(section).eq.'*') then
      call prim_to_sic(readonly,error)
      if (error) return
      call scan_to_sic(readonly,error)
      if (error) return
      call febe_to_sic(readonly,error)
      if (error) return
      call array_to_sic(readonly,error)
      if (error) return
      call data_to_sic(readonly,error)
      if (error) return
      call mon_to_sic(readonly,error)
      if (error) return
      call gains_to_sic(readonly,error)
      if (error) return
      call choice_to_sic(readonly,error)
      if (error) return
      call reduce_to_sic(readonly,error)
      if (error) return
      call raw_to_sic(readonly,error)
      if (error) return
      call cryo_to_sic(readonly,error)
      if (error) return
      call derot_to_sic(readonly,error)
      if (error) return
      call list_to_sic(readonly,error)
      if (error) return
   else if (index(section,'PR').ne.0.or.   &
            index(section,'pr').ne.0.or.   &
            index(section,'Pr').ne.0) then
      call prim_to_sic(readonly,error)
      if (error) return
   else if (index(section,'SC').ne.0.or.   &
            index(section,'sc').ne.0.or.   &
            index(section,'Sc').ne.0) then
      call scan_to_sic(readonly,error)
      if (error) return
   else if (index(section,'FE').ne.0.or.   &
            index(section,'fe').ne.0.or.   &
            index(section,'Fe').ne.0) then
      call febe_to_sic(readonly,error)
      if (error) return
   else if (index(section,'AR').ne.0.or.   &
            index(section,'ar').ne.0.or.   &
            index(section,'Ar').ne.0) then
      call array_to_sic(readonly,error)
      if (error) return
   else if (index(section,'DA').ne.0.or.   &
            index(section,'da').ne.0.or.   &
            index(section,'Da').ne.0) then
      call data_to_sic(readonly,error)
      if (error) return
   else if (index(section,'MO').ne.0.or.   &
            index(section,'mo').ne.0.or.   &
            index(section,'Mo').ne.0) then
      call mon_to_sic(readonly,error)
      if (error) return
   else if (index(section,'GA').ne.0.or.   &
            index(section,'ga').ne.0.or.   &
            index(section,'Ga').ne.0) then
      call gains_to_sic(readonly,error)
      if (error) return
   else if (index(section,'CH').ne.0.or.   &
            index(section,'ch').ne.0.or.   &
            index(section,'Ch').ne.0) then
      call choice_to_sic(readonly,error)
      if (error) return
   else if (index(section,'RE').ne.0.or.   &
            index(section,'re').ne.0.or.   &
            index(section,'Re').ne.0) then
      call reduce_to_sic(readonly,error)
      if (error) return
   else if (index(section,'RA').ne.0.or.   &
            index(section,'ra').ne.0.or.   &
            index(section,'Ra').ne.0) then
      call raw_to_sic(readonly,error)
      if (error) return
   else if (index(section,'CR').ne.0.or.   &
            index(section,'cr').ne.0.or.   &
            index(section,'Cr').ne.0) then
      call cryo_to_sic(readonly,error)
      if (error) return
   else if (index(section,'DE').ne.0.or.   &
            index(section,'de').ne.0.or.   &
            index(section,'De').ne.0) then
      call derot_to_sic(readonly,error)
      if (error) return
   else if (index(section,'LI').ne.0.or.   &
            index(section,'li').ne.0.or.   &
            index(section,'Li').ne.0) then
      call list_to_sic(readonly,error)
      if (error) return
   else
      call gagout('E-VARIABLE, no such section.')
      error = .true.
   endif
   !
   return
end subroutine mira_variable
!
subroutine mira_to_sic
!
! Contains the following entries:
! prim_to_sic, scan_to_sic, febe_to_sic, data_to_sic, array_to_sic,
! mon_to_sic, gains_to_sic, choice_to_sic, reduce_to_sic, raw_to_sic,
! cryo_to_sic, derot_to_sic, list_to_sic.
!
! Called by the following subroutines:
!
! subroutine            comments
! calibrate             if SIC structure GAINS or REDUCE already declared
! despike               if SIC structure REDUCE already declared
! findmira              if SIC structures CHOICE and LIST already declared
! getmira               for update of already declared SIC structures
!
! Note: the subroutine pointing_to_sic (source code in mira/lib/solve.f90) for declaring
! and attributing the SIC structure PNT for coadded pointing subscans is called
! by solve_pointing_mira.
!
   use mira
   use gildas_def
   use gkernel_types
   use gkernel_interfaces
   !
   implicit none
   !
   integer                                    :: ifb
   integer, save                              :: found
   integer                                    :: i,ier,j,k,l,nbd,   &
                                                 nfast,nfb,   &
                                                 noffsets, npix,   &
                                                 nphases,nrecord,   &
                                                 nslow, nsubref
   type(sic_descriptor_t)                     :: desc
   integer, dimension(18)                     :: idpol
   integer, dimension(100)                    :: idbe, idfe
   integer(kind=index_length), dimension(4)   :: dim=0, dim1=0,  &
                                                 dim2=0, dim3=0
   character                                  :: name1*20,name2*20
   character(len =32)                         :: tmpbackend
   character(len=32), dimension(:), allocatable :: tmpfebe
   character(len=20), parameter :: nullstring='                    '
   logical error, exist, readonly
   !
   entry prim_to_sic(readonly,error)
   !
   if (.not.associated(prim)) then
      call gagout('E-VAR: no PRIMARY section in MIRA.')
      error = .true.
      return
   endif
   !
   call sic_descriptor('PRIM',desc,exist)
   if (exist) call sic_delvariable('PRIM',.false.,error)
   !
   call sic_defstructure('PRIM',.true.,error)
   call sic_defstructure('PRIM%HEADER',.true.,error)
   call sic_defstructure('PRIM%DATA',.true.,error)
   !
   call sic_def_inte('PRIM%HEADER%NAXIS',prim%header%naxis,0,0,   &
                     readonly,error)
   call sic_def_logi('PRIM%HEADER%SIMPLE',prim%header%simple,   &
                     readonly,error)
   call sic_def_inte('PRIM%HEADER%BITPIX',prim%header%bitpix,0,0,   &
                     readonly,error)
   call sic_def_logi('PRIM%HEADER%EXTEND',prim%header%extend,   &
                     readonly,error)
   call sic_def_char('PRIM%HEADER%TELESCOP',prim%header%telescop,   &
                     readonly,error)
   call sic_def_char('PRIM%HEADER%ORIGIN',prim%header%origin,   &
                     readonly,error)
   call sic_def_char('PRIM%HEADER%CREATOR',prim%header%creator,   &
                     readonly,error)
   call sic_def_char('PRIM%HEADER%MBFTSVER',prim%header%mbftsver,   &
                     readonly,error)
   !
   return
   !
   entry scan_to_sic(readonly,error)
   !
   if (.not.associated(scan)) then
      call gagout('E-VAR: no SCAN section in MIRA.')
      error = .true.
      return
   endif
   !
   call sic_descriptor('blankingRaw',desc,exist)
   if (.not.exist)   &
     call sic_def_real('blankingRaw',blankingraw,0,0,readonly,error)
   call sic_descriptor('blankingRed',desc,exist)
   if (.not.exist)   &
     call sic_def_real('blankingRed',blankingred,0,0,readonly,error)
   !
   call sic_descriptor('SCAN',desc,exist)
   if (exist) call sic_delvariable('SCAN',.false.,error)
   !
   call sic_defstructure('SCAN',.true.,error)
   call sic_defstructure('SCAN%HEADER',.true.,error)
   call sic_defstructure('SCAN%DATA',.true.,error)
   !
   call sic_def_char('SCAN%HEADER%TELESCOP',scan%header%telescop,   &
                     readonly,error)
   call sic_def_dble('SCAN%HEADER%SITELONG',scan%header%sitelong,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%SITELAT',scan%header%sitelat,   &
                     0,0,readonly,error)
   call sic_def_real('SCAN%HEADER%SITEELEV',scan%header%siteelev,   &
                     0,0,readonly,error)
   call sic_def_char('SCAN%HEADER%PROJID',scan%header%projid,   &
                     readonly,error)
   call sic_def_char('SCAN%HEADER%OBSID',scan%header%obsid,   &
                     readonly,error)
   call sic_def_inte('SCAN%HEADER%SCANNUM',scan%header%scannum,   &
                     0,0,readonly,error)
   call sic_def_char('SCAN%HEADER%DATE_OBS',scan%header%date_obs,   &
                     readonly,error)
   call sic_def_dble('SCAN%HEADER%MJD',scan%header%mjd,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%LST',scan%header%lst,   &
                     0,0,readonly,error)
   call sic_def_inte('SCAN%HEADER%NOBS',scan%header%nobs,   &
                     0,0,readonly,error)
   call sic_def_inte('SCAN%HEADER%NOBSTOTAL',scan%header%nobstotal,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%UT1UTC',scan%header%ut1utc,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%TAIUTC',scan%header%taiutc,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%ETUTC',scan%header%etutc,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%GPSTAI',scan%header%gpstai,   &
                     0,0,readonly,error)
   call sic_def_char('SCAN%HEADER%CTYPE1',scan%header%ctype1,   &
                     readonly,error)
   call sic_def_char('SCAN%HEADER%CTYPE2',scan%header%ctype2,   &
                     readonly,error)
   call sic_def_char('SCAN%HEADER%RADECSYS',scan%header%radecsys,   &
                     readonly,error)
   call sic_def_real('SCAN%HEADER%EQUINOX',scan%header%equinox,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%CRVAL1',scan%header%crval1,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%CRVAL2',scan%header%crval2,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%LONPOLE',scan%header%lonpole,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%LATPOLE',scan%header%latpole,   &
                     0,0,readonly,error)
   call sic_def_char('SCAN%HEADER%OBJECT',scan%header%object,   &
                     readonly,error)
   call sic_def_dble('SCAN%HEADER%LONGOBJ',scan%header%longobj,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%LATOBJ',scan%header%latobj,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%LONGOFF',scan%header%longoff,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%LATOFF',scan%header%latoff,   &
                     0,0,readonly,error)
   call sic_def_char('SCAN%HEADER%CALCODE',scan%header%calcode,   &
                     readonly,error)
   call sic_def_logi('SCAN%HEADER%MOVEFRAM',scan%header%movefram,   &
                     readonly,error)
   call sic_def_dble('SCAN%HEADER%PERIDATE',scan%header%peridate,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%PERIDIST',scan%header%peridist,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%LONGASC',scan%header%longasc,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%OMEGA',scan%header%omega,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%INCLINAT',scan%header%inclinat,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%ECCENTR',scan%header%eccentr,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%ORBEPOCH',scan%header%orbepoch,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%ORBEQUNX',scan%header%orbeqnox,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%DISTANCE',scan%header%distance,   &
                     0,0,readonly,error)
   call sic_def_char('SCAN%HEADER%SCANTYPE',scan%header%scantype,   &
                     readonly,error)
   call sic_def_char('SCAN%HEADER%SCANMODE',scan%header%scanmode,   &
                     readonly,error)
   call sic_def_char('SCAN%HEADER%SCANGEOM',scan%header%scangeom,   &
                     readonly,error)
   call sic_def_char('SCAN%HEADER%SCANDIR',scan%header%scandir,   &
                     readonly,error)
   call sic_def_inte('SCAN%HEADER%SCANLINE',scan%header%scanline,   &
                     0,0,readonly,error)
   call sic_def_inte('SCAN%HEADER%SCANRPTS',scan%header%scanrpts,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%SCANLEN',scan%header%scanlen,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%SCANXVEL',scan%header%scanxvel,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%SCANTIME',scan%header%scantime,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%SCANXSPC',scan%header%scanxspc,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%SCANYSPC',scan%header%scanyspc,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%SCANSKEW',scan%header%scanskew,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%SCANPAR1',scan%header%scanpar1,   &
                     0,0,readonly,error)
   call sic_def_dble('SCAN%HEADER%SCANPAR2',scan%header%scanpar2,   &
                     0,0,readonly,error)
   call sic_def_char('SCAN%HEADER%CROCYCLE',scan%header%crocycle,   &
                     readonly,error)
   call sic_def_logi('SCAN%HEADER%ZIGZAG',scan%header%zigzag,   &
                     readonly,error)
   call sic_def_inte('SCAN%HEADER%TRANDIST',scan%header%trandist,   &
                     0,0,readonly,error)
   call sic_def_inte('SCAN%HEADER%TRANFREQ',scan%header%tranfreq,   &
                     0,0,readonly,error)
   call sic_def_inte('SCAN%HEADER%TRANFOCU',scan%header%tranfocu,   &
                     0,0,readonly,error)
   call sic_def_logi('SCAN%HEADER%WOBUSED',scan%header%wobused,   &
                     readonly,error)
   call sic_def_dble('SCAN%HEADER%WOBTHROW',scan%header%wobthrow,   &
                     0,0,readonly,error)
   call sic_def_char('SCAN%HEADER%WOBDIR',scan%header%wobdir,   &
                     readonly,error)
   call sic_def_real('SCAN%HEADER%WOBCYCLE',scan%header%wobcycle,   &
                     0,0,readonly,error)
   call sic_def_char('SCAN%HEADER%WOBMODE',scan%header%wobmode,   &
                     readonly,error)
   call sic_def_inte('SCAN%HEADER%NFEBE',scan%header%nfebe,   &
                     0,0,readonly,error)
   call sic_def_real('SCAN%HEADER%IA',scan%header%ia,   &
                     0,0,readonly,error)
   call sic_def_real('SCAN%HEADER%CA',scan%header%ca,   &
                     0,0,readonly,error)
   call sic_def_real('SCAN%HEADER%NPAE',scan%header%npae,   &
                     0,0,readonly,error)
   call sic_def_real('SCAN%HEADER%AN',scan%header%an,   &
                     0,0,readonly,error)
   call sic_def_real('SCAN%HEADER%AW',scan%header%aw,   &
                     0,0,readonly,error)
   call sic_def_real('SCAN%HEADER%IE',scan%header%ie,   &
                     0,0,readonly,error)
   call sic_def_real('SCAN%HEADER%ECEC',scan%header%ecec,   &
                     0,0,readonly,error)
   call sic_def_real('SCAN%HEADER%ZFLX',scan%header%zflx,   &
                     0,0,readonly,error)
   !
   nfb = scan%header%nfebe
   if (allocated(tmpfebe)) deallocate(tmpfebe)
   allocate(tmpfebe(nfb),stat=ier)
   do i = 1, nfb
      tmpfebe(i) = scan%data%febe(i)
      call sic_upper(tmpfebe(i))
   enddo
   !
   where (index(tmpfebe,'CONT').ne.0)
      idbe(1:nfb) = 10
   else where (index(tmpfebe,'BBC').ne.0)
      idbe(1:nfb) = 11
   else where (index(tmpfebe,'NBC').ne.0)
      idbe(1:nfb) = 12
   else where (index(tmpfebe,'100K').ne.0)
      idbe(1:nfb) = 20
   else where (index(tmpfebe,'1MHZ').ne.0)
      idbe(1:nfb) = 30
   else where (index(tmpfebe,'VESPA').ne.0)
      idbe(1:nfb) = 40
   else where (index(tmpfebe,'WILMA').ne.0)
      idbe(1:nfb) = 60
   else where (index(tmpfebe,'4MHZ').ne.0)
      idbe(1:nfb) = 70
   else where (index(tmpfebe,'FTS').ne.0)
      idbe(1:nfb) = 80
   else where (index(tmpfebe,'ABBA').ne.0)
      idbe(1:nfb) = 90
   else where
      idbe(1:nfb) = 0
   end where
   !
!! temporary disabled
!   do i = 1, nfb
!      tmpbackend = tmpfebe(i)
!      j = index(tmpbackend,'/')
!      if (j.ne.0) then
!         read(tmpbackend(j+1:j+2),'(I2)') k
!         idbe(i) = idbe(i)+k
!      endif
!   enddo
   !
   where (index(tmpfebe,'100').eq.2)
      idfe(1:nfb) = 10
   else where (index(tmpfebe,'150').ne.0)
      idfe(1:nfb) = 20
   else where (index(tmpfebe,'230').ne.0)
      idfe(1:nfb) = 30
   else where (index(tmpfebe,'270').ne.0)
      idfe(1:nfb) = 40
   else where (index(tmpfebe,'HERA1').ne.0)
      idfe(1:nfb) = 50
   else where (index(tmpfebe,'HERA2').ne.0)
      idfe(1:nfb) = 60
   else where (index(tmpfebe,'MAMBO').ne.0)
      idfe(1:nfb) = 70
   else where (index(tmpfebe,'90').ne.0)
      idfe(1:nfb) = 80
   else where (index(tmpfebe,'E0').eq.1)
      idfe(1:nfb) = 1000
   else where (index(tmpfebe,'E1').eq.1)
      idfe(1:nfb) = 2000
   else where (index(tmpfebe,'E2').eq.1)
      idfe(1:nfb) = 3000
   else where (index(tmpfebe,'E3').eq.1)
      idfe(1:nfb) = 4000
   else where
      idfe(1:nfb) = 0
   end where
   !
   where (index(tmpfebe,'A').eq.1)
      idfe(1:nfb) = idfe(1:nfb)+1
   else where (index(tmpfebe,'B').eq.1)
      idfe(1:nfb) = idfe(1:nfb)+2
   else where (index(tmpfebe,'C').eq.1)
      idfe(1:nfb) = idfe(1:nfb)+3
   else where (index(tmpfebe,'D').eq.1)
      idfe(1:nfb) = idfe(1:nfb)+4
   else where (index(tmpfebe,'H').eq.1.and.index(tmpfebe,'HER').eq.0)
      idfe(1:nfb) = idfe(1:nfb)+5
   else where (index(tmpfebe,'V').eq.1)
      idfe(1:nfb) = idfe(1:nfb)+6
   else where (index(tmpfebe,'R').eq.1)
      idfe(1:nfb) = idfe(1:nfb)+7
   else where (index(tmpfebe,'I').eq.1)
      idfe(1:nfb) = idfe(1:nfb)+8
   end where
   !
   ! EMIR cases
   !
   where (index(tmpfebe,'H').eq.3)
      idfe(1:nfb) = idfe(1:nfb)+100
   else where (index(tmpfebe,'V').eq.3)
      idfe(1:nfb) = idfe(1:nfb)+200
   else where (index(tmpfebe,'R').eq.3.and.index(tmpfebe,'HER').eq.0)
      idfe(1:nfb) = idfe(1:nfb)+300
   else where (index(tmpfebe,'I').eq.3)
      idfe(1:nfb) = idfe(1:nfb)+400
   end where
   where (index(tmpfebe,'L').eq.4)
      idfe(1:nfb) = idfe(1:nfb)+10
   else where (index(tmpfebe,'U').eq.4)
      idfe(1:nfb) = idfe(1:nfb)+20
   end where
   where (index(tmpfebe,'LI').eq.4.or.index(tmpfebe,'UI').eq.4)
      idfe(1:nfb) = idfe(1:nfb)+1
   else where (index(tmpfebe,'LO').eq.4.or.index(tmpfebe,'UO').eq.4)
      idfe(1:nfb) = idfe(1:nfb)+2
   end where
   !
   call sic_def_inte('SCAN%DATA%BE',idbe(1:nfb),1,nfb,readonly,error)
   call sic_def_inte('SCAN%DATA%FE',idfe(1:nfb),1,nfb,readonly,error)
   !
   call sic_descriptor('SCAN%DATA%LONGOFF',desc,exist)
   if (exist) call sic_delvariable('SCAN%DATA%LONGOFF',.false.,error)
   call sic_descriptor('SCAN%DATA%LATOFF',desc,exist)
   if (exist) call sic_delvariable('SCAN%DATA%LATOFF',.false.,error)
   !
   if (associated(scan%data%longoff)) then
      noffsets = size(scan%data%longoff,1)
      call sic_def_dble('SCAN%DATA%LONGOFF',scan%data%longoff,1,   &
                        noffsets,readonly,error)
      call sic_def_dble('SCAN%DATA%LATOFF',scan%data%latoff,1,   &
                        noffsets,readonly,error)
   endif
   !
   return
   !
   entry febe_to_sic(readonly,error)
   !
   if (.not.associated(febe)) then
      call gagout('E-VAR: no FEBE section in MIRA.')
      error = .true.
      return
   endif
   !
   npix = febe(1)%header%nusefeed
   call sic_descriptor('xHera',desc,exist)
   if (exist) call sic_delvariable('xHera',.false.,error)
   call sic_def_dble('xHera',xhera,1,npix,.true.,error)
   call sic_descriptor('yHera',desc,exist)
   if (exist) call sic_delvariable('yHera',.false.,error)
   call sic_def_dble('yHera',yhera,1,npix,.true.,error)
   !
   nfb = scan%header%nfebe
   !
   febe_loop1: do i = 1, nfb
      name1 = nullstring
      write(name1,'(A4,I0)') 'FEBE',i
      call sic_descriptor(trim(name1),desc,exist)
      if (exist) call sic_delvariable(trim(name1),.false.,error)
      call sic_defstructure(trim(name1),.true.,error)
      !
      name2 = nullstring
      write(name2,'(A)') trim(name1)//'%HEADER'
      call sic_defstructure(trim(name2),.true.,error)
      call sic_def_char(trim(name2)//'%FEBE',   &
                        febe(i)%header%febe,readonly,error)
      call sic_def_inte(trim(name2)//'%SCANNUM',   &
                        febe(i)%header%scannum,0,0,readonly,error)
      call sic_def_char(trim(name2)//'%DATE_OBS',   &
                        febe(i)%header%date_obs,readonly,error)
      call sic_def_char(trim(name2)//'%DEWRTMOD',   &
                        febe(i)%header%dewrtmod,readonly,error)
      call sic_def_real(trim(name2)//'%DEWANG',   &
                        febe(i)%header%dewang,0,0,readonly,error)
      call sic_def_char(trim(name2)//'%DEWCABIN',   &
                        febe(i)%header%dewcabin,readonly,error)
      call sic_def_inte(trim(name2)//'%FEBEBAND',   &
                        febe(i)%header%febeband,0,0,readonly,error)
      call sic_def_inte(trim(name2)//'%FEBEFEED',   &
                        febe(i)%header%febefeed,0,0,readonly,error)
      call sic_def_inte(trim(name2)//'%NUSEFEED',   &
                        febe(i)%header%nusefeed,0,0,readonly,error)
      call sic_def_char(trim(name2)//'%SWTCHMOD',   &
                        febe(i)%header%swtchmod,readonly,error)
      call sic_def_char(trim(name2)//'%VELOCONV',   &
                        febe(i)%header%veloconv,readonly,error)
      call sic_def_inte(trim(name2)//'%NPHASES',   &
                        febe(i)%header%nphases,0,0,readonly,error)
      call sic_def_real(trim(name2)//'%FRQOFF1',   &
                        febe(i)%header%frqoff1,0,0,readonly,error)
      call sic_def_real(trim(name2)//'%FRQOFF2',   &
                        febe(i)%header%frqoff2,0,0,readonly,error)
      call sic_def_real(trim(name2)//'%FRQTHROW',   &
                        febe(i)%header%frqthrow,0,0,readonly,error)
      call sic_def_real(trim(name2)//'%IARX',   &
                        febe(i)%header%iarx,0,0,readonly,error)
      call sic_def_real(trim(name2)//'%CARX',   &
                        febe(i)%header%carx,0,0,readonly,error)
      call sic_def_real(trim(name2)//'%IERX',   &
                        febe(i)%header%ierx,0,0,readonly,error)
      call sic_def_real(trim(name2)//'%ECECRX',   &
                        febe(i)%header%ececrx,0,0,readonly,error)
      call sic_def_real(trim(name2)//'%ZFLXRX',   &
                        febe(i)%header%zflxrx,0,0,readonly,error)
      call sic_def_real(trim(name2)//'%IFCENTER',   &
                        febe(i)%header%ifcenter,0,0,readonly,error)
      !
      name2 = nullstring
      write(name2,'(A)') trim(name1)//'%DATA'
      call sic_defstructure(trim(name2),.true.,error)
      !
      nbd = febe(i)%header%febeband
      npix = febe(i)%header%nusefeed
      dim(1) = npix
      dim(2) = nbd
      !
      call sic_def_inte(trim(name2)//'%USEFEED',   &
                        febe(i)%data%usefeed,1,npix,readonly,error)
      call sic_def_char(trim(name2)//'%FEEDTYPE','H',readonly,   &
                        error)
      call sic_def_dble(trim(name2)//'%FEEDOFFX',   &
                        febe(i)%data%feedoffx,1,npix,readonly,error)
      call sic_def_dble(trim(name2)//'%FEEDOFFY',   &
                        febe(i)%data%feedoffy,1,npix,readonly,error)
      call sic_def_inte(trim(name2)//'%REFFEED',   &
                        febe(i)%data%reffeed,0,0,readonly,error)
      !         DO J = 1, NPIX
      !            IF (INDEX(FEBE(I)%DATA%POLTY(J),'x').NE.0.OR.
      !     &          INDEX(FEBE(I)%DATA%POLTY(J),'X').NE.0) THEN
      !                IDPOL(J) = 1
      !            ELSE IF (INDEX(FEBE(I)%DATA%POLTY(J),'y').NE.0.OR.
      !     &          INDEX(FEBE(I)%DATA%POLTY(J),'Y').NE.0) THEN
      !                IDPOL(J) = 2
      !            ELSE IF (INDEX(FEBE(I)%DATA%POLTY(J),'l').NE.0.OR.
      !     &          INDEX(FEBE(I)%DATA%POLTY(J),'L').NE.0) THEN
      !                IDPOL(J) = 3
      !            ELSE IF (INDEX(FEBE(I)%DATA%POLTY(J),'r').NE.0.OR.
      !     &          INDEX(FEBE(I)%DATA%POLTY(J),'R').NE.0) THEN
      !                IDPOL(J) = 4
      !            ENDIF
      !         ENDDO
      name2 = nullstring
      write(name2,'(A)') trim(name1)//'%DATA'
      call sic_def_char(trim(name2)//'%POLTY',febe(i)%data%polty(1),   &
                        readonly,error)
      call sic_def_real(trim(name2)//'%POLA',   &
                        febe(i)%data%pola,1,npix,readonly,error)
      call sic_def_real(trim(name2)//'%APEREFF',   &
                        febe(i)%data%apereff,2,dim,readonly,error)
      call sic_def_real(trim(name2)//'%BEAMEFF',   &
                        febe(i)%data%beameff,2,dim,readonly,error)
      call sic_def_real(trim(name2)//'%ETAFSS',   &
                        febe(i)%data%etafss,2,dim,readonly,error)
      call sic_def_real(trim(name2)//'%HPBW',   &
                        febe(i)%data%hpbw,2,dim,readonly,error)
      call sic_def_real(trim(name2)//'%ANTGAIN',   &
                        febe(i)%data%antgain,2,dim,readonly,error)
      call sic_def_real(trim(name2)//'%BOLCALFC',   &
                        febe(i)%data%bolcalfc,1,npix,readonly,error)
      call sic_def_real(trim(name2)//'%FLATFIEL',   &
                        febe(i)%data%flatfiel,2,dim,readonly,error)
      call sic_def_real(trim(name2)//'%GAINIMAG',   &
                        febe(i)%data%gainimag,2,dim,readonly,error)
      call sic_def_real(trim(name2)//'%GAINELE1',   &
                        febe(i)%data%gainele1,1,npix,readonly,error)
      call sic_def_real(trim(name2)//'%GAINELE2',   &
                        febe(i)%data%gainele2,1,npix,readonly,error)
   !
   enddo febe_loop1
   !
   return
   !
   entry data_to_sic(readonly,error)
   !
   if (.not.associated(data)) then
      call gagout('E-VAR: no DATA section in MIRA.')
      error = .true.
      return
   endif
   !
   nfb = scan%header%nfebe
   !
   febe_loop2: do i = 1, nfb
      name1 = nullstring
      write(name1,'(A4,I0)') 'DATA',i
      call sic_descriptor(trim(name1),desc,exist)
      if (exist) then
         call sic_delvariable(trim(name1),.false.,error)
      endif
      call sic_defstructure(trim(name1),.true.,error)
      name2 = nullstring
      write(name2,'(A)') trim(name1)//'%HEADER'
      call sic_defstructure(trim(name2),.true.,error)
      !
      call sic_def_char(trim(name2)//'%FEBE',   &
                        data(i)%header%febe,   &
                        readonly,error)
      call sic_def_inte(trim(name2)//'%SCANNUM',   &
                        data(i)%header%scannum,   &
                        0,0,readonly,error)
      call sic_def_inte(trim(name2)//'%OBSNUM',   &
                        data(i)%header%obsnum,   &
                        0,0,readonly,error)
      call sic_def_char(trim(name2)//'%DATE_OBS',   &
                        data(i)%header%date_obs,   &
                        readonly,error)
      call sic_def_dble(trim(name2)//'%LST',   &
                        data(i)%header%lst,   &
                        0,0,readonly,error)
      call sic_def_char(trim(name2)//'%OBSTYPE',   &
                        data(i)%header%obstype,   &
                        readonly,error)
      call sic_def_logi(trim(name2)//'%DPBLOCK',   &
                        data(i)%header%dpblock,   &
                        readonly,error)
      call sic_def_char(trim(name2)//'%OBSTATUS',   &
                        data(i)%header%obstatus,   &
                        readonly,error)
      call sic_def_inte(trim(name2)//'%CHANNELS',   &
                        data(i)%header%channels,   &
                        0,0,readonly,error)
      call sic_def_real(trim(name2)//'%CRPX4F_2',   &
                        data(i)%header%crpx4f_2,   &
                        0,0,readonly,error)
      call sic_def_real(trim(name2)//'%CRPX4R_2',   &
                        data(i)%header%crpx4r_2,   &
                        0,0,readonly,error)
      call sic_def_dble(trim(name2)//'%CRVL4F_2',   &
                        data(i)%header%crvl4f_2,   &
                        0,0,readonly,error)
      call sic_def_dble(trim(name2)//'%CRVL4R_2',   &
                        data(i)%header%crvl4r_2,   &
                        0,0,readonly,error)
      call sic_def_dble(trim(name2)//'%CD4F_21',   &
                        data(i)%header%cd4f_21,   &
                        0,0,readonly,error)
      call sic_def_dble(trim(name2)//'%CD4R_21',   &
                        data(i)%header%cd4r_21,   &
                        0,0,readonly,error)
      name2 = nullstring
      write(name2,'(A)') trim(name1)//'%DATA'
      call sic_defstructure(trim(name2),.true.,error)
      !
      nrecord = maxval(data(i)%data%integnum)
      dim(1)   = nrecord
      call sic_def_dble(trim(name2)//'%LONGOFF',data(i)%data%longoff,   &
                        1,dim,readonly,error)
      call sic_def_dble(trim(name2)//'%LATOFF', data(i)%data%latoff,   &
                        1,dim,readonly,error)
      call sic_def_inte(trim(name2)//'%INTEGNUM',   &
                        data(i)%data%integnum,   &
                        1,dim,readonly,error)
      call sic_def_inte(trim(name2)//'%SUBSCAN',   &
                        data(i)%data%subscan,   &
                        1,dim,readonly,error)
      call sic_def_inte(trim(name2)//'%NINTS',   &
                        data(i)%data%nints,   &
                        1,dim,readonly,error)
      call sic_def_dble(trim(name2)//'%MJD',   &
                        data(i)%data%mjd,   &
                        1,dim,readonly,error)
      call sic_def_dble(trim(name2)//'%LST',   &
                        data(i)%data%lst,   &
                        1,dim,readonly,error)
      call sic_def_dble(trim(name2)//'%MIDTIME',   &
                        data(i)%data%midtime,   &
                        1,dim,readonly,error)
      call sic_def_dble(trim(name2)//'%INTEGTIM',   &
                        data(i)%data%integtim,   &
                        1,dim,readonly,error)
      dim(1) = febe(i)%header%febefeed
      dim(2) = data(i)%header%channels
      dim(3) = nrecord
      !
      call sic_def_real(trim(name2)//'%RDATA',   &
                        data(i)%data%rdata,   &
                        2,dim,readonly,error)
      call sic_def_real(trim(name2)//'%OTFDATA',   &
                        data(i)%data%otfdata,   &
                        3,dim,readonly,error)
      !
      name1 = nullstring
      write(name1,'(A4,I0)') 'DATA',i
      write(name2,'(A)') trim(name1)//'%DATA'
      dim(1) = nrecord
      dim(2) = 3
      !
      call sic_def_dble(trim(name2)//'%AZIMUTH',data(i)%data%azimuth,   &
                        1,dim,readonly,error)
      call sic_def_dble(trim(name2)//'%ELEVATIO',   &
                        data(i)%data%elevatio,1,dim,readonly,error)
      call sic_def_dble(trim(name2)//'%CBASLONG',   &
                        data(i)%data%cbaslong,1,dim,readonly,error)
      call sic_def_dble(trim(name2)//'%CBASLAT',data(i)%data%cbaslat,   &
                        1,dim,readonly,error)
      call sic_def_dble(trim(name2)//'%BASLONG',data(i)%data%baslong,   &
                        1,dim,readonly,error)
      call sic_def_dble(trim(name2)//'%BASLAT',data(i)%data%baslat,   &
                        1,dim,readonly,error)
      call sic_def_dble(trim(name2)//'%PARANGLE',   &
                        data(i)%data%parangle,1,dim,readonly,error)
      call sic_def_dble(trim(name2)//'%PC_11',data(i)%data%pc_11,   &
                        1,dim,readonly,error)
      call sic_def_dble(trim(name2)//'%PC_12',data(i)%data%pc_12,   &
                        1,dim,readonly,error)
      call sic_def_dble(trim(name2)//'%PC_21',data(i)%data%pc_21,   &
                        1,dim,readonly,error)
      call sic_def_dble(trim(name2)//'%PC_22',data(i)%data%pc_22,   &
                        1,dim,readonly,error)
      call sic_def_real(trim(name2)//'%CR4K1',data(i)%data%cr4k1,   &
                        1,dim,readonly,error)
      call sic_def_real(trim(name2)//'%CR4K2',data(i)%data%cr4k2,   &
                        1,dim,readonly,error)
      call sic_def_dble(trim(name2)//'%MCRVAL1',data(i)%data%mcrval1,   &
                        1,dim,readonly,error)
      call sic_def_dble(trim(name2)//'%MCRVAL2',data(i)%data%mcrval2,   &
                        1,dim,readonly,error)
      call sic_def_dble(trim(name2)//'%MLONPOLE',   &
                        data(i)%data%mlonpole,1,dim,readonly,error)
      call sic_def_dble(trim(name2)//'%MLATPOLE',   &
                        data(i)%data%mlatpole,1,dim,readonly,error)
      call sic_def_dble(trim(name2)//'%FOCUS_X_Y_Z',   &
                        data(i)%data%focus_x_y_z,2,dim,readonly,   &
                        error)
      call sic_def_dble(trim(name2)//'%DFOCUS_X_Y_Z',   &
                        data(i)%data%dfocus_x_y_z,2,dim,readonly,   &
                        error)
      call sic_def_dble(trim(name2)//'%PHI_X_Y_Z',   &
                        data(i)%data%phi_x_y_z,2,dim,readonly,   &
                        error)
   !
   enddo febe_loop2
   !
   return
   !
   entry array_to_sic(readonly,error)
   !
   if (.not.associated(array)) then
      call gagout('E-VAR: no ARRAY section in MIRA.')
      error = .true.
      return
   endif
   !
   if (allocated(integtyp)) then
      do i = 1, size(integtyp)
         deallocate(integtyp(i)%switch,stat=ier)
      enddo
      deallocate(integtyp,stat=ier)
   endif
   allocate(integtyp(scan%header%nfebe),stat=ier)
   !
   febe_loop3: do i = 1, scan%header%nfebe
      !
      nrecord = size(array(i)%data(1)%data,3)
      nphases = size(array(i)%data(1)%data,4)
      allocate(integtyp(i)%switch(nrecord,nphases),stat=ier)
      !
      name1 = nullstring
      write(name1,'(A5,I0)') 'ARRAY',i
      call sic_descriptor(trim(name1),desc,exist)
      if (exist) call sic_delvariable(trim(name1),.false.,error)
      call sic_defstructure(trim(name1),.true.,error)
      nbd = febe(i)%header%febeband
      baseband_loop: do j = 1, nbd
         !
         name2 = nullstring
         if (i.lt.10) then
            write(name2,'(A6,A7,I0)') trim(name1),'%HEADER',j
         else
            write(name2,'(A7,A7,I0)') trim(name1),'%HEADER',j
         endif
         call sic_descriptor(trim(name2),desc,exist)
         if (exist) call sic_delvariable(trim(name2),.false.,error)
         call sic_defstructure(trim(name2),.true.,error)
         !
         call sic_def_char(trim(name2)//'%FEBE',   &
                           array(i)%header(j)%febe,   &
                           readonly,error)
         call sic_def_inte(trim(name2)//'%BASEBAND',   &
                           array(i)%header(j)%baseband,   &
                           0,0,readonly,error)
         call sic_def_inte(trim(name2)//'%SCANNUM',   &
                           array(i)%header(j)%scannum,   &
                           0,0,readonly,error)
         call sic_def_inte(trim(name2)//'%OBSNUM',   &
                           array(i)%header(j)%obsnum,   &
                           0,0,readonly,error)
         call sic_def_char(trim(name2)//'%DATE_OBS',   &
                           array(i)%header(j)%date_obs,   &
                           readonly,error)
         call sic_def_inte(trim(name2)//'%CHANNELS',   &
                           array(i)%header(j)%channels,   &
                           0,0,readonly,error)
         call sic_def_inte(trim(name2)//'%USEDCHAN',   &
                           array(i)%header(j)%usedchan,   &
                           0,0,readonly,error)
         call sic_def_inte(trim(name2)//'%DROPCHAN',   &
                           array(i)%header(j)%dropchan,   &
                           0,0,readonly,error)
         call sic_def_dble(trim(name2)//'%FREQRES',   &
                           array(i)%header(j)%freqres,   &
                           0,0,readonly,error)
         call sic_def_dble(trim(name2)//'%BANDWID',   &
                           array(i)%header(j)%bandwid,   &
                           0,0,readonly,error)
         call sic_def_char(trim(name2)//'%MOLECULE',   &
                           array(i)%header(j)%molecule,   &
                           readonly,error)
         call sic_def_char(trim(name2)//'%TRANSITI',   &
                           array(i)%header(j)%transiti,   &
                           readonly,error)
         call sic_def_dble(trim(name2)//'%RESTFREQ',   &
                           array(i)%header(j)%restfreq,   &
                           0,0,readonly,error)
         call sic_def_char(trim(name2)//'%SIDEBAND',   &
                           array(i)%header(j)%sideband,   &
                           readonly,error)
         call sic_def_dble(trim(name2)//'%SBSEP',   &
                           array(i)%header(j)%sbsep,   &
                           0,0,readonly,error)
         call sic_def_char(trim(name2)//'%CTYP4_1',   &
                           array(i)%header(j)%ctyp4_1,   &
                           readonly,error)
         call sic_def_inte(trim(name2)//'%CRPX4_1',   &
                           array(i)%header(j)%crpx4_1,   &
                           0,0,readonly,error)
         call sic_def_inte(trim(name2)//'%CRVL4_1',   &
                           array(i)%header(j)%crvl4_1,   &
                           0,0,readonly,error)
         call sic_def_inte(trim(name2)//'%CD3A_11',   &
                           array(i)%header(j)%cd3a_11,   &
                           0,0,readonly,error)
         call sic_def_char(trim(name2)//'%WCSNM4F',   &
                           array(i)%header(j)%wcsnm4f,   &
                           readonly,error)
         call sic_def_char(trim(name2)//'%CTYP4F_2',   &
                           array(i)%header(j)%ctyp4f_2,   &
                           readonly,error)
         call sic_def_real(trim(name2)//'%CRPX4F_2',   &
                           array(i)%header(j)%crpx4f_2,   &
                           0,0,readonly,error)
         call sic_def_dble(trim(name2)//'%CRVL4F_2',   &
                           array(i)%header(j)%crvl4f_2,   &
                           0,0,readonly,error)
         call sic_def_dble(trim(name2)//'%CD4F_21',   &
                           array(i)%header(j)%cd4f_21,   &
                           0,0,readonly,error)
         call sic_def_char(trim(name2)//'%CUNI4F_2',   &
                           array(i)%header(j)%cuni4f_2,   &
                           readonly,error)
         call sic_def_char(trim(name2)//'%SPEC4F_2',   &
                           array(i)%header(j)%spec4f_2,   &
                           readonly,error)
         call sic_def_char(trim(name2)//'%SOBS4F_2',   &
                           array(i)%header(j)%sobs4f_2,   &
                           readonly,error)
         call sic_def_char(trim(name2)//'%WCSNM4R',   &
                           array(i)%header(j)%wcsnm4r,   &
                           readonly,error)
         call sic_def_char(trim(name2)//'%CTYP4R_2',   &
                           array(i)%header(j)%ctyp4r_2,   &
                           readonly,error)
         call sic_def_real(trim(name2)//'%CRPX4R_2',   &
                           array(i)%header(j)%crpx4r_2,   &
                           0,0,readonly,error)
         call sic_def_dble(trim(name2)//'%CRVL4R_2',   &
                           array(i)%header(j)%crvl4r_2,   &
                           0,0,readonly,error)
         call sic_def_dble(trim(name2)//'%CD4R_21',   &
                           array(i)%header(j)%cd4r_21,   &
                           0,0,readonly,error)
         call sic_def_char(trim(name2)//'%CUNI4R_2',   &
                           array(i)%header(j)%cuni4r_2,   &
                           readonly,error)
         call sic_def_char(trim(name2)//'%SPEC4R_2',   &
                           array(i)%header(j)%spec4r_2,   &
                           readonly,error)
         call sic_def_char(trim(name2)//'%SOBS4R_2',   &
                           array(i)%header(j)%sobs4r_2,   &
                           readonly,error)
         call sic_def_real(trim(name2)//'%VSOU4R_2',   &
                           array(i)%header(j)%vsou4r_2,   &
                           0,0,readonly,error)
         call sic_def_real(trim(name2)//'%VSYS4R_2',   &
                           array(i)%header(j)%vsys4r_2,   &
                           0,0,readonly,error)
         !
         name2 = nullstring
         if (i.lt.10) then
            write(name2,'(A6,A5,I0)') trim(name1),'%DATA',j
         else
            write(name2,'(A7,A5,I0)') trim(name1),'%DATA',j
         endif
         call sic_descriptor(trim(name2),desc,exist)
         if (exist) call sic_delvariable(trim(name2),.false.,error)
         call sic_defstructure(trim(name2),.true.,error)
         !
         nrecord = size(array(i)%data(j)%data,3)
         nphases = size(array(i)%data(j)%data,4)
         !
         integtyp(i)%switch = array(i)%data(j)%iswitch
         !
         dim(1) = nrecord
         dim(2) = nphases
         dim(3) = 0
         dim(4) = 0
         call sic_def_charn(trim(name2)//'%ISWITCH',   &
                            integtyp(i)%switch,2,   &
                            dim,readonly,error)
         !
         dim(1) = size(array(i)%data(j)%data,1)
         dim(2) = size(array(i)%data(j)%data,2)
         dim(3) = size(array(i)%data(j)%data,3)
         dim(4) = size(array(i)%data(j)%data,4)
         call sic_def_dble(trim(name2)//'%DATA',   &
                           array(i)%data(j)%data,   &
                           4,dim,readonly,error)
         !
         dim(1) = nrecord*nphases
         dim(2:4) = 0
         call sic_def_dble(trim(name2)//'%MJD',   &
                           array(i)%data(j)%mjd,   &
                           1,dim,readonly,error)
         call sic_def_dble(trim(name2)//'%INTEGTIM',   &
                           array(i)%data(j)%integtim,   &
                           1,dim,readonly,error)
         !
      !
      enddo baseband_loop
   !
   enddo febe_loop3
   !
   return
   !
   entry mon_to_sic(readonly,error)
   !
   if (.not.associated(mon)) then
      call gagout('E-VAR: no MONITOR section in MIRA.')
      error = .true.
      return
   endif
   !
   nfb = scan%header%nfebe
   !
   call sic_descriptor('ANTENNA',desc,exist)
   if (exist) call sic_delvariable('ANTENNA',.false.,error)
   call sic_defstructure('ANTENNA',.true.,error)
   nslow = size(antdata%slowtrace,1)
   dim1(1) = nslow
   dim1(2) = 3
   call sic_def_dble('ANTENNA%SLOWTRACE',antdata%slowtrace,   &
                     2,dim1,readonly,error)
   nfast = size(antdata%tracking_az_el,1)
   dim1(1) = nfast
   dim1(2) = 3
   dim2(1) = nfast
   dim2(2) = 2
   call sic_def_dble('ANTENNA%ENCODER_AZ_EL',antdata%encoder_az_el,   &
                     2,dim1,readonly,error)
   call sic_def_dble('ANTENNA%TRACKING_AZ_EL',antdata%tracking_az_el,   &
                     2,dim2,readonly,error)
   !
   febe_loop4: do i = 1, nfb
      name1 = nullstring
      write(name1,'(A3,I0)') 'MON',i
      call sic_descriptor(trim(name1),desc,exist)
      if (exist) call sic_delvariable(trim(name1),.false.,error)
      call sic_defstructure(trim(name1),.true.,error)
      name2 = nullstring
      write(name2,'(A)') trim(name1)//'%HEADER'
      call sic_defstructure(name2,.true.,error)
      !
      nrecord = size(mon(i)%data%mjd)
      nfast = size(mon(i)%data%tracking_az_el,1)
      nsubref = size(mon(i)%data%focus_x_y_z,1)
      dim1(1) = nfast
      dim1(2) = 2
      dim2(1) = nfast
      dim2(2) = 3
      dim3(1) = nsubref
      dim3(2) = 4
      !
      call sic_def_inte(trim(name2)//'%SCANNUM',   &
                        mon(i)%header%scannum,0,0,readonly,error)
      call sic_def_inte(trim(name2)//'%OBSNUM',mon(i)%header%obsnum,   &
                        0,0,readonly,error)
      call sic_def_dble(trim(name2)//'%IAOBS_CAOBS_IEOBS',   &
                        mon(i)%header%iaobs_caobs_ieobs,   &
                        1,3,readonly,error)
      call sic_def_dble(trim(name2)//'%FOCOBS_X_Y_Z',   &
                        mon(i)%header%focobs_x_y_z,   &
                        1,3,readonly,error)
      call sic_def_char(trim(name2)//'%DATE_OBS',   &
                        mon(i)%header%date_obs,readonly,error)

      name2 = nullstring
      write(name2,'(A)') trim(name1)//'%DATA'
      call sic_defstructure(trim(name2),.true.,error)

      call sic_def_dble(trim(name2)//'%MJD',mon(i)%data%mjd,   &
                        1,nrecord,readonly,error)
      call sic_def_inte(trim(name2)//'%SUBSCAN',mon(i)%data%subscan,   &
                        1,nrecord,readonly,error)
      call sic_def_inte(trim(name2)//'%SUBSCANFAST',   &
                        mon(i)%data%subscanfast,1,nfast,readonly,   &
                        error)
      call sic_def_inte(trim(name2)//'%SUBSCANSUBREF',   &
                        mon(i)%data%subscansubref,1,nsubref,readonly,   &
                        error)
      call sic_def_dble(trim(name2)//'%LST',mon(i)%data%lst,   &
                        1,nrecord,readonly,error)
      call sic_def_dble(trim(name2)//'%ENCODER_AZ_EL',   &
                        mon(i)%data%encoder_az_el,2,dim2,readonly,   &
                        error)
      call sic_def_dble(trim(name2)//'%TRACKING_AZ_EL',   &
                        mon(i)%data%tracking_az_el,2,dim1,readonly,   &
                        error)
      call sic_def_dble(trim(name2)//'%DFOCUS_X_Y_Z',   &
                        mon(i)%data%dfocus_x_y_z,2,dim3,readonly,   &
                        error)
      call sic_def_dble(trim(name2)//'%FOCUS_X_Y_Z',   &
                        mon(i)%data%focus_x_y_z,2,dim3,readonly,   &
                        error)
      call sic_def_dble(trim(name2)//'%PHI_X_Y_Z',   &
                        mon(i)%data%phi_x_y_z,2,dim3,readonly,error)
      call sic_def_dble(trim(name2)//'%PARANGLE',   &
                        mon(i)%data%parangle,1,nrecord,readonly,   &
                        error)
      call sic_def_dble(trim(name2)//'%BASLONG',mon(i)%data%baslong,   &
                        1,nrecord,readonly,error)
      call sic_def_dble(trim(name2)//'%BASLAT',mon(i)%data%baslat,   &
                        1,nrecord,readonly,error)
      call sic_def_dble(trim(name2)//'%LONGOFF',mon(i)%data%longoff,   &
                        1,nrecord,readonly,error)
      call sic_def_dble(trim(name2)//'%LATOFF',mon(i)%data%latoff,   &
                        1,nrecord,readonly,error)
      call sic_def_dble(trim(name2)//'%ANTENNA_AZ_EL',   &
                        mon(i)%data%antenna_az_el,1,2,readonly,error)
      dim1(1) = size(mon(i)%data%tamb_p_humid,1)
      dim1(2) = 3
      call sic_def_dble(trim(name2)//'%TAMB_P_HUMID',   &
                        mon(i)%data%tamb_p_humid,2,dim1,readonly,   &
                        error)
      call sic_def_dble(trim(name2)//'%WIND_DIR_VAVG_VMAX',   &
                        mon(i)%data%wind_dir_vavg_vmax,2,dim1,   &
                        readonly,error)
      dim1(2) = 2
      call sic_def_dble(trim(name2)//'%REFRACTIO',   &
                        mon(i)%data%refractio,1,dim1,readonly,   &
                        error)
      dim1(1) = size(mon(i)%data%thotcold,1)
      dim1(2) = 2
      call sic_def_dble(trim(name2)//'%THOTCOLD',   &
                        mon(i)%data%thotcold,2,dim1,readonly,error)
   !
   !
   enddo febe_loop4
   !
   !
   return
   !
   entry gains_to_sic(readonly,error)
   !
   if (.not.associated(gains)) then
      call gagout('E-VAR: no CALIBRATION section in MIRA.')
      error = .true.
      return
   endif
   !
   nfb = size(gains)
   do i = 1, nfb
      name1 = nullstring
      write(name1,'(A5,I0)') 'GAINS',i
      call sic_descriptor(trim(name1),desc,exist)
      if (exist) call sic_delvariable(trim(name1),.false.,error)
      if (.not.associated(gains(i)%gainarray)) then
         call gagout('W-VARIABLE: No calibration available for '   &
                     //scan%data%febe(i))
      else
         dim2(1) = size(gains(i)%gainarray,1)
         dim2(2) = size(gains(i)%gainarray,2)
         dim2(3) = size(gains(i)%gainarray,3)
         dim3(1:2) = dim2(1:2)
         dim3(3) = size(gains(i)%trx,3)
         call sic_defstructure(trim(name1),.true.,error)
         call sic_def_char(trim(name1)//'%FEBE',gains(i)%febe,   &
                           readonly,error)
         call sic_def_dble(trim(name1)//'%GAINARRAY',   &
                           gains(i)%gainarray,   &
                           3,dim2,readonly,error)
         call sic_def_dble(trim(name1)//'%GAINIMAGE',   &
                           gains(i)%gainimage,   &
                           3,dim2,readonly,error)
         if (associated(gains(i)%phasearray))   &
           call sic_def_dble(trim(name1)//'%PHASEARRAY',   &
                             gains(i)%phasearray,   &
                             3,dim2,readonly,error)
         call sic_def_dble(trim(name1)//'%CRVL4F_2',   &
                           gains(i)%crvl4f_2,1,dim2,readonly,error)
         call sic_def_inte(trim(name1)//'%CHANNELS',   &
                           gains(i)%channels,1,dim2,readonly,error)
         call sic_def_dble(trim(name1)//'%CD4F_21',   &
                           gains(i)%cd4f_21,1,dim2,readonly,error)
         call sic_def_dble(trim(name1)//'%PSKY',gains(i)%psky,   &
                           3,dim2,readonly,error)
         call sic_def_dble(trim(name1)//'%PHOT',gains(i)%phot,   &
                           3,dim2,readonly,error)
         call sic_def_dble(trim(name1)//'%PCOLD',gains(i)%pcold,   &
                           3,dim2,readonly,error)
         if (associated(gains(i)%pgrid))   &
           call sic_def_dble(trim(name1)//'%PGRID',gains(i)%pgrid,   &
                           3,dim2,readonly,error)
         call sic_def_dble(trim(name1)//'%TAUZEN',gains(i)%tauzen,   &
                           3,dim3,readonly,error)
         call sic_def_dble(trim(name1)//'%TAUZENDRY',   &
                           gains(i)%tauzendry,3,dim3,readonly,error)
         call sic_def_dble(trim(name1)//'%TAUZENWET',   &
                           gains(i)%tauzenwet,3,dim3,readonly,error)
         call sic_def_dble(trim(name1)//'%TAUZENIMAGE',  &
                           gains(i)%tauzenimage,3,dim3,readonly,error)
         call sic_def_dble(trim(name1)//'%TAUZENIMAGEDRY',   &
                           gains(i)%tauzenimagedry,3,dim3,readonly,error)
         call sic_def_dble(trim(name1)//'%TAUZENIMAGEWET',   &
                           gains(i)%tauzenimagewet,3,dim3,readonly,error)
         call sic_def_dble(trim(name1)//'%TATMS',gains(i)%tatms,   &
                           3,dim3,readonly,error)
         call sic_def_dble(trim(name1)//'%TATMI',gains(i)%tatmi,   &
                           3,dim3,readonly,error)
         call sic_def_dble(trim(name1)//'%TEMIS',gains(i)%temis,   &
                           3,dim3,readonly,error)
         call sic_def_dble(trim(name1)//'%TEMII',gains(i)%temii,   &
                           3,dim3,readonly,error)
         call sic_def_dble(trim(name1)//'%H2OMM',gains(i)%h2omm,   &
                           3,dim3,readonly,error)
         call sic_def_dble(trim(name1)//'%LCALOF',gains(i)%lcalof,   &
                           0,0,readonly,error)
         call sic_def_dble(trim(name1)//'%BCALOF',gains(i)%bcalof,   &
                           0,0,readonly,error)
         call sic_def_dble(trim(name1)//'%TCAL',gains(i)%tcal,   &
                           3,dim3,readonly,error)
         call sic_def_dble(trim(name1)//'%TSYS',gains(i)%tsys,   &
                           3,dim3,readonly,error)
         call sic_def_dble(trim(name1)//'%TRX',gains(i)%trx,   &
                           3,dim3,readonly,error)
      endif
   enddo
   !
   return
   !
   entry choice_to_sic(readonly,error)
   !
   call sic_descriptor('CHOICE',desc,exist)
   if (exist) call sic_delvariable('CHOICE',.false.,error)
   call sic_defstructure('CHOICE',.true.,error)
   if (allocated(choice%backend)) then
      do i = 1, size(choice%backend)
         name1 = nullstring
         write(name1,'(A14,I0)') 'CHOICE%BACKEND',i
         call sic_def_char(trim(name1),choice%backend(i),   &
                           .true.,error)
      enddo
   endif
   if (allocated(choice%frontend)) then
      do i = 1, size(choice%frontend)
         name1 = nullstring
         write(name1,'(A15,I0)') 'CHOICE%FRONTEND',i
         call sic_def_char(trim(name1),choice%frontend(i),   &
                           .true.,error)
      enddo
   endif
   if (allocated(choice%transition)) then
      do i = 1, size(choice%transition)
         name1 = nullstring
         write(name1,'(A17,I0)') 'CHOICE%TRANSITION',i
         call sic_def_char(trim(name1),choice%transition(i),   &
                           .true.,error)
      enddo
   endif
   call sic_def_char('CHOICE%DATE_OBS1',choice%date_obs(1),   &
                     .true.,error)
   call sic_def_char('CHOICE%DATE_OBS2',choice%date_obs(2),   &
                     .true.,error)
   if (allocated(choice%procedure)) then
      do i = 1, size(choice%procedure)
         name1 = nullstring
         write(name1,'(A16,I0)') 'CHOICE%PROCEDURE',i
         call sic_def_char(trim(name1),choice%procedure(i),   &
                           .true.,error)
      enddo
   endif
   if (allocated(choice%switchmode)) then
      do i = 1, size(choice%switchmode)
         name1 = nullstring
         write(name1,'(A17,I0)') 'CHOICE%SWITCHMODE',i
         call sic_def_char(trim(name1),choice%switchmode(i),   &
                           .true.,error)
      enddo
   endif
   call sic_def_char('CHOICE%SCAN1',choice%scan(1),   &
                     .true.,error)
   call sic_def_char('CHOICE%SCAN2',choice%scan(2),   &
                     .true.,error)
   if (allocated(choice%object)) then
      do i = 1, size(choice%object)
         name1 = nullstring
         write(name1,'(A13,I0)') 'CHOICE%OBJECT',i
         call sic_def_char(trim(name1),choice%object(i),.true.,error)
      enddo
   endif
   if (allocated(choice%telescope)) then
      do i = 1, size(choice%telescope)
         name1 = nullstring
         write(name1,'(A16,I0)') 'CHOICE%TELESCOPE',i
         call sic_def_char(trim(name1),choice%telescope(i),   &
                           .true.,error)
      enddo
   endif
   if (allocated(choice%project)) then
      do i = 1, size(choice%project)
         name1 = nullstring
         write(name1,'(A16,I0)') 'CHOICE%PROJECT',i
         call sic_def_char(trim(name1),choice%project(i),   &
                           .true.,error)
      enddo
   endif
   !
   return
   !
   entry reduce_to_sic(readonly,error)
   !
   if (allocated(iflag)) deallocate(iflag,stat=ier)
   allocate(iflag(5,scan%header%nfebe),stat=ier)
   do i = 1, scan%header%nfebe
      name1 = 'NULLSTRING'
      write(name1,'(A6,I0)') 'REDUCE',i
      call sic_descriptor(trim(name1),desc,exist)
      if (exist) call sic_delvariable(trim(name1),.false.,error)
      call sic_defstructure(trim(name1),.true.,error)
      where (reduce(i)%calibration_done(1:5))
         iflag(1:5,i) = 1
      elsewhere
         iflag(1:5,i) = 0
      end where
      call sic_def_inte(trim(name1)//'%CALIBRATION_DONE',   &
                        iflag(1:5,i),1,5,readonly,error)
      call sic_def_logi(trim(name1)//'%BASE_DONE',   &
                        reduce(i)%base_done,readonly,error)
      call sic_def_logi(trim(name1)//'%DESPIKE_DONE',   &
                        reduce(i)%despike_done,readonly,error)
   enddo
   !
   return
   !
   entry raw_to_sic(readonly,error)
   !
   nfb = scan%header%nfebe
   subscanloop: do i = 1, size(raw)
      name1 = nullstring
      write(name1,'(A3,I0)') 'RAW',i
      call sic_descriptor(trim(name1),desc,exist)
      if (exist) call sic_delvariable(trim(name1),.false.,error)
      call sic_defstructure(trim(name1),.true.,error)
      febeloop: do j = 1, nfb
         name2 = nullstring
         if (i.lt.10) then
            write(name2,'(A4,A8,I0)') trim(name1),'%ANTENNA',j
         else if (i.lt.100) then
            write(name2,'(A5,A8,I0)') trim(name1),'%ANTENNA',j
         else
            write(name2,'(A6,A8,I0)') trim(name1),'%ANTENNA',j
         endif
         call sic_defstructure(trim(name2),.true.,error)
         call sic_def_char(trim(name2)//'%SYSTEMOFF',   &
                           raw(i)%antenna(j)%systemoff,.true.,error)
         call sic_def_dble(trim(name2)//'%SUBSCANSTART',   &
                           raw(i)%antenna(j)%subscanstart,0,0,   &
                           readonly,error)
         call sic_def_dble(trim(name2)//'%SUBSCANEND',   &
                           raw(i)%antenna(j)%subscanend,0,0,   &
                           readonly,error)
         call sic_def_dble(trim(name2)//'%dateObs',   &
                           raw(i)%antenna(j)%dateObs,0,0,   &
                           readonly,error)
         call sic_def_dble(trim(name2)//'%dateEnd',   &
                           raw(i)%antenna(j)%dateEnd,0,0,   &
                           readonly,error)
         call sic_def_dble(trim(name2)//'%SEGMENTXSTART',   &
                           raw(i)%antenna(j)%segmentxstart,0,0,   &
                           readonly,error)
         call sic_def_dble(trim(name2)//'%SEGMENTXEND',   &
                           raw(i)%antenna(j)%segmentxend,0,0,   &
                           readonly,error)
         call sic_def_dble(trim(name2)//'%SEGMENTYSTART',   &
                           raw(i)%antenna(j)%segmentystart,0,0,   &
                           readonly,error)
         call sic_def_dble(trim(name2)//'%SEGMENTYEND',   &
                           raw(i)%antenna(j)%segmentyend,0,0,   &
                           readonly,error)
         call sic_def_dble(trim(name2)//'%TRACKINGERROR',   &
                           raw(i)%antenna(j)%trackingerror,0,0,   &
                           readonly,error)
         call sic_def_dble(trim(name2)//'%DOPPLERCORR',   &
                           raw(i)%antenna(j)%dopplercorr,0,0,   &
                           readonly,error)
         call sic_def_dble(trim(name2)//'%OBSVELRF',   &
                           raw(i)%antenna(j)%obsvelrf,0,0,   &
                           readonly,error)
         call sic_def_char(trim(name2)//'%SBCALREC',   &
                           raw(i)%antenna(j)%sbcalrec,   &
                           readonly,error)
         name2 = nullstring
         if (i.lt.10) then
            write(name2,'(A4,A7,I0)') trim(name1),'%SUBREF',j
         else if (i.lt.100) then
            write(name2,'(A5,A7,I0)') trim(name1),'%SUBREF',j
         else
            write(name2,'(A6,A7,I0)') trim(name1),'%SUBREF',j
         endif
         call sic_defstructure(trim(name2),.true.,error)
         call sic_def_char(trim(name2)//'%FOCUSTRANSL',   &
                           raw(i)%subref(j)%focustransl,   &
                           readonly,error)
         call sic_def_dble(trim(name2)//'%SUBSCANSTART',   &
                           raw(i)%subref(j)%subscanstart,0,0,   &
                           readonly,error)
         call sic_def_dble(trim(name2)//'%SUBSCANEND',   &
                           raw(i)%subref(j)%subscanend,0,0,   &
                           readonly,error)
         call sic_def_dble(trim(name2)//'%SEGMENTXSTART',   &
                           raw(i)%subref(j)%segmentxstart,0,0,   &
                           readonly,error)
         call sic_def_dble(trim(name2)//'%SEGMENTXEND',   &
                           raw(i)%subref(j)%segmentxend,0,0,   &
                           readonly,error)
         call sic_def_dble(trim(name2)//'%SEGMENTYSTART',   &
                           raw(i)%subref(j)%segmentystart,0,0,   &
                           readonly,error)
         call sic_def_dble(trim(name2)//'%SEGMENTYEND',   &
                           raw(i)%subref(j)%segmentyend,0,0,   &
                           readonly,error)

         name2 = nullstring
         if (i.lt.10) then
            write(name2,'(A4,A8,I0)') trim(name1),'%BACKEND',j
         else if (i.lt.100) then
            write(name2,'(A5,A8,I0)') trim(name1),'%BACKEND',j
         else
            write(name2,'(A6,A8,I0)') trim(name1),'%BACKEND',j
         endif
         call sic_defstructure(trim(name2),.true.,error)
         call sic_def_dble(trim(name2)//'%SUBSCANSTART',   &
                           raw(i)%backend(j)%subscanstart,0,0,   &
                           readonly,error)
         call sic_def_dble(trim(name2)//'%SUBSCANEND',   &
                           raw(i)%backend(j)%subscanend,0,0,   &
                           readonly,error)
      enddo febeloop
   enddo subscanloop
   !
   return
   !
   entry cryo_to_sic(readonly,error)
   !
   if (associated(cryo)) then
      !
      call sic_descriptor('CRYO',desc,exist)
      if (exist) call sic_delvariable('CRYO',.false.,error)
      call sic_defstructure('CRYO',.true.,error)
      dim1(1) = size(cryo%cr4k1)
      call sic_def_real('CRYO%CR4K1',cryo%cr4k1,1,dim1,   &
                        readonly,error)
      call sic_def_real('CRYO%CR4K2',cryo%cr4k2,1,dim1,   &
                        readonly,error)
      call sic_def_dble('CRYO%MJD',cryo%mjd,1,dim1,readonly,error)
   !
   endif
   !
   return
   !
   entry derot_to_sic(readonly,error)
   !
   if (associated(derot)) then
      !
      call sic_descriptor('DEROT',desc,exist)
      if (exist) call sic_delvariable('DEROT',.false.,error)
      call sic_defstructure('DEROT',.true.,error)
      dim1(1) = size(derot%actframe)
      call sic_def_real('DEROT%ACTFRAME',derot%actframe,1,dim1,   &
                        readonly,error)
      call sic_def_dble('DEROT%MJD',derot%mjd,1,dim1,readonly,error)
   !
   endif
   !
   return
   !
   !
   entry list_to_sic(readonly,error)
   !
   if (.not.associated(list)) then
      call gagout('W-VARIABLE: Cannot create SIC structure LIST.')
      return
   endif
   call sic_descriptor('LIST',desc,exist)
   if (exist) call sic_delvariable('LIST',.false.,error)
   call sic_defstructure('LIST',.true.,error)
   found = size(list)
   call sic_def_inte('LIST%FOUND',found,0,0,readonly,error)
   !
   do i = 1, size(list)
      write(name1,'(A4,I0)') 'LIST',i
      call sic_descriptor(trim(name1),desc,exist)
      if (exist) call sic_delvariable(trim(name1),.false.,error)
      call sic_defstructure(trim(name1),.true.,error)
      call sic_def_inte(trim(name1)//'%INDX',list(i)%indx,   &
                        0,0,readonly,error)
      call sic_def_inte(trim(name1)//'%SCAN',list(i)%scan,   &
                        0,0,readonly,error)
      call sic_def_char(trim(name1)//'%OBJECT',   &
                        list(i)%object,.true.,error)
      call sic_def_char(trim(name1)//'%TELESCOPE',   &
                        list(i)%telescope,.true.,error)
      call sic_def_char(trim(name1)//'%PROCEDURE',   &
                        list(i)%procedure,.true.,error)
      call sic_def_char(trim(name1)//'%SWITCHMODE',   &
                        list(i)%switchmode,.true.,error)
      call sic_def_char(trim(name1)//'%BACKEND',   &
                        list(i)%backend(1),.true.,error)
      call sic_def_char(trim(name1)//'%dateOBS',   &
                        list(i)%dateobs,.true.,error)
      call sic_def_inte(trim(name1)//'%NCFE',list(i)%ncfe,   &
                        0,0,readonly,error)
   enddo
   !
   return
!
end subroutine mira_to_sic
