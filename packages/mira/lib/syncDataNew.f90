subroutine syncDataNew(ifb,nfb,subscanRange,error)
!
! Called by subroutines getfits and getfitsNew.
! Interpolates antenna and subreflector data at backend timestamp
! (linear between preceeding and following antenna and subreflector dump).
! Results are written into Mira structures DATA(ifb) (where ifb is the frontend-
! backend number).
!
! Unlike subroutine syncData, does not apply any timing check.
!

  use mira
  use gildas_def

  implicit none

  integer :: ifb, nfb
  integer, dimension(2) :: subscanRange 
  logical :: error

  integer :: i, ibd, ier, ilo, isub, iup, i1, i2, i3, i4, j, k, l,          &
     &       locate, nbd, nchan, newchan, nfebe, nphases, npix 
  integer :: ndumpAntenna, ndumpAntennaNew, ndumpAntennaFast,               &
     &       ndumpAntennaFastNew, ndumpBackend, ndumpBackendNew,            &
     &       ndumpCryo, ndumpFrontend, ndumpSubref, ndumpSubrefNew
  real*4  :: meanCounts
  real*8  :: dTime1, dTime2, gainMax1, gainMax2, slope, trackingCutoff
 
  real*8, dimension(nsubscan)       :: subScanEnd, subScanStart

  integer(kind=address_length),dimension(9) :: desc
  integer, dimension(:), allocatable   :: indxFast, indxSlow, indxSubref
  character(len=20)                    :: tmpScanType, tmpSwitchMode
  character(len=32)                    :: tmpFrontend
  logical                              :: doShift, exist, scanStart, trackFlag
  logical, dimension(:,:), allocatable :: mask,tmpMask
  logical, dimension(:), allocatable   :: maskFSW,tmpMaskFSW
  logical, dimension(:), allocatable   :: subScanFastMask, subScanMask,    &
      &                                   subScanSubrefMask

!
!
!
  trackingCutoff = raw(1)%antenna(ifb)%trackingError
  ndumpBackend = size(dt(ifb)%data%mjd)
  ndumpAntenna = size(mnt(ifb)%data%lst,1)
  ndumpAntennaFast = size(mnt(ifb)%data%encoder_az_el,1)
  ndumpSubref = size(mnt(ifb)%data%focus_x_y_z,1)
  ndumpFrontend = size(mnt(ifb)%data%tamb_p_humid,1)
!
  nfebe = scan%header%nfebe
  nbd = febe(ifb)%header%febeband
  nchan = ar(ifb)%header(1)%channels 
  newchan = dt(ifb)%header%channels
  npix = febe(ifb)%header%febefeed
  nphases = febe(ifb)%header%nphases
!
!
!
  do isub = 1, nsubscan
     subScanStart(isub) = raw(isub)%antenna(ifb)%subScanStart
     subScanEnd(isub)   = raw(isub)%antenna(ifb)%subScanEnd
  enddo
!
  do i = 1, ndumpAntenna
     do isub = subscanRange(1), subscanRange(2)
        i1 = isub-subscanRange(1)+1
        if (mnt(ifb)%data%mjd(i).ge.subScanStart(i1).and. &
            mnt(ifb)%data%mjd(i).le.subScanEnd(i1)) then
            mnt(ifb)%data%subScan(i) = isub
            exit
        else
            cycle
        endif
     enddo
  enddo
!
  do i = 1, ndumpAntennaFast
     do isub = subscanRange(1), subscanRange(2)
        i1 = isub-subscanRange(1)+1
        if (mnt(ifb)%data%encoder_az_el(i,3).ge.subScanStart(i1).and.    &
 &          mnt(ifb)%data%encoder_az_el(i,3).le.subScanEnd(i1)) then
           mnt(ifb)%data%subScanFast(i) = isub
           exit
        else
           cycle
        endif
     enddo
  enddo
!
  do i = 1, ndumpSubref
     do isub = 1, nsubscan
        i1 = isub-subscanRange(1)+1
        if (mnt(ifb)%data%focus_x_y_z(i,4).ge.subScanStart(i1).and.    &
 &          mnt(ifb)%data%focus_x_y_z(i,4).le.subScanEnd(i1)) then
           mnt(ifb)%data%subScanSubref(i) = isub
           exit
        else
           cycle
        endif
     enddo
  enddo
!
  tmpScanType = scan%header%scantype
  call sic_upper(tmpScanType)
  dt(ifb)%data%subScan(1:ndumpBackend) = 0
  do i = 1, ndumpBackend
     do isub = subscanRange(1), subscanRange(2)
        i1 = isub-subscanRange(1)+1 
        if (dt(ifb)%data%mjd(i)-dt(ifb)%data%integtim(i)*sec2day.ge.    &
 &         subScanStart(i1).and.dt(ifb)%data%mjd(i).le.subScanEnd(i1)) then
           dt(ifb)%data%subScan(i) = isub
           if (nphases.eq.1.and.tmpscantype.eq.'ONTHEFLYMAP') then
              do j=1, nbd
                 if (raw(i1)%antenna(j)%subscantype.eq.'track') then
                    ar(ifb)%data(j)%iswitch(i,1) = 'OFF'
                 else if (raw(i1)%antenna(j)%subscantype &
 &                        .eq.'onTheFly') then
                    ar(ifb)%data(j)%iswitch(i,1) = 'ON'
                 else
                    ar(ifb)%data(j)%iswitch(i,1) = 'NONE'
                 endif
              enddo
           endif
           exit
        else
           cycle
        endif
!
! The above IF condition may have to be relaxed. Here I only accept backend
! dumps during which no subScanStart or subScanEnd timestamps occur.
!

     enddo
  enddo
  do i = 2, ndumpBackend
     if (dt(ifb)%data%mjd(i).eq.dt(ifb)%data%mjd(i-1)) &
 &      dt(ifb)%data%subscan(i) = 0
  enddo
!
  if (allocated(indxSlow)) deallocate(indxSlow,stat=ier)
  allocate(indxSlow(ndumpAntenna),stat=ier)
  call indexx(ndumpAntenna,mnt(ifb)%data%mjd,indxSlow)
!
  ndumpAntennaNew = 1 
  do i = 1, ndumpAntenna-1
     if (mnt(ifb)%data%mjd(indxSlow(i+1)).ne.                     &
 &       mnt(ifb)%data%mjd(indxSlow(i)))                          &
 &      ndumpAntennaNew = ndumpAntennaNew+1
  enddo
!
  if (allocated(indxFast)) deallocate(indxFast,stat=ier)
  allocate(indxFast(ndumpAntennaFast),stat=ier)
  call indexx(ndumpAntennaFast,mnt(ifb)%data%encoder_az_el(:,3),indxFast)
!
  ndumpAntennaFastNew = 1 
  do i = 1, ndumpAntennaFast-1
     if (mnt(ifb)%data%encoder_az_el(indxFast(i+1),3).ne.          &
         mnt(ifb)%data%encoder_az_el(indxFast(i),3))               &
 &      ndumpAntennaFastNew = ndumpAntennaFastNew+1
  enddo
!
  if (allocated(indxSubref)) deallocate(indxSubref,stat=ier)
  allocate(indxSubref(ndumpSubref),stat=ier)
  call indexx(ndumpSubref,mnt(ifb)%data%focus_x_y_z(:,4),indxSubref)
!
  ndumpSubrefNew = 1
  do i = 1, ndumpSubref-1
     if (mnt(ifb)%data%focus_x_y_z(indxSubref(i+1),4).ne.           &
 &       mnt(ifb)%data%focus_x_y_z(indxSubref(i),4))                &
 &      ndumpSubrefNew = ndumpSubrefNew+1
  enddo
!
  ndumpBackendNew = 0
  do i = 1, ndumpBackend
     if (dt(ifb)%data%subscan(i).ne.0)                   &
 &      ndumpBackendNew = ndumpBackendNew+1
  enddo
!
!
!
  if (ifb.eq.1) allocate(monBuf(nfebe))
  monBuf(ifb)%header = mnt(ifb)%header
!
  allocate(monBuf(ifb)%data%subScan(ndumpAntennaNew),              &
     &     monBuf(ifb)%data%subScanFast(ndumpAntennaFastNew),      &
     &     monBuf(ifb)%data%subScanSubref(ndumpSubrefNew),         &
     &     monBuf(ifb)%data%mjd(ndumpAntennaNew),                  &
     &     monBuf(ifb)%data%lst(ndumpAntennaNew),                  &
     &     monBuf(ifb)%data%encoder_az_el(ndumpAntennaFastNew,3),  &
     &     monBuf(ifb)%data%tracking_az_el(ndumpAntennaFastNew,2), &
     &     monBuf(ifb)%data%longoff(ndumpAntennaNew),              &
     &     monBuf(ifb)%data%latoff(ndumpAntennaNew),               &
     &     monBuf(ifb)%data%baslong(ndumpAntennaNew),              &
     &     monBuf(ifb)%data%baslat(ndumpAntennaNew),               &
     &     monBuf(ifb)%data%parangle(ndumpAntennaNew),             &
     &     monBuf(ifb)%data%antenna_az_el(2),                      &
     &     monBuf(ifb)%data%tamb_p_humid(ndumpFrontend,3),         &
     &     monBuf(ifb)%data%wind_dir_vavg_vmax(ndumpFrontend,3),   &
     &     monBuf(ifb)%data%thotcold(ndumpFrontend,2),             &
     &     monBuf(ifb)%data%refractio(ndumpFrontend),              &
     &     monBuf(ifb)%data%focus_x_y_z(ndumpSubrefNew,4),         &
     &     monBuf(ifb)%data%dfocus_x_y_z(ndumpSubrefNew,3),        &
     &     monBuf(ifb)%data%phi_x_y_z(ndumpSubrefNew,3),           &
     &     monBuf(ifb)%data%dphi_x_y_z(ndumpSubrefNew,3),          &
     &     stat = ier)
  allocate(subScanSubrefMask(ndumpSubrefNew),                 &
     &     subScanMask(ndumpAntenna),                         &
     &     subScanFastMask(ndumpAntennaFastNew),stat = ier)
  monBuf(ifb)%data%antenna_az_el = mnt(ifb)%data%antenna_az_el
  monBuf(ifb)%data%tamb_p_humid = mnt(ifb)%data%tamb_p_humid
  monBuf(ifb)%data%wind_dir_vavg_vmax = mnt(ifb)%data%wind_dir_vavg_vmax
  monBuf(ifb)%data%thotcold  = mnt(ifb)%data%thotcold
!
  ilo = 1
  i1  = indxSlow(1)
  monBuf(ifb)%data%subScan(1) = mnt(ifb)%data%subScan(i1)
  monBuf(ifb)%data%mjd(1) = mnt(ifb)%data%mjd(i1)
  monBuf(ifb)%data%lst(1) = mnt(ifb)%data%lst(i1)
  monBuf(ifb)%data%longoff(1) = mnt(ifb)%data%longoff(i1)
  monBuf(ifb)%data%latoff(1) =  mnt(ifb)%data%latoff(i1)
  monBuf(ifb)%data%baslong(1) = mnt(ifb)%data%baslong(i1)
  monBuf(ifb)%data%baslat(1) = mnt(ifb)%data%baslat(i1)
  monBuf(ifb)%data%parangle(1) = mnt(ifb)%data%parangle(i1)
  do i = 1, ndumpAntenna-1
     i1 = indxSlow(i)
     i2 = indxSlow(i+1)

     if (mnt(ifb)%data%mjd(i2).ne.mnt(ifb)%data%mjd(i1)) then
        ilo = ilo+1
        monBuf(ifb)%data%subScan(ilo) = mnt(ifb)%data%subScan(i2)
        monBuf(ifb)%data%mjd(ilo) = mnt(ifb)%data%mjd(i2)
        monBuf(ifb)%data%lst(ilo) = mnt(ifb)%data%lst(i2)
        monBuf(ifb)%data%longoff(ilo) = mnt(ifb)%data%longoff(i2)
        monBuf(ifb)%data%latoff(ilo) =  mnt(ifb)%data%latoff(i2)
        monBuf(ifb)%data%baslong(ilo) = mnt(ifb)%data%baslong(i2)
        monBuf(ifb)%data%baslat(ilo) = mnt(ifb)%data%baslat(i2)
        monBuf(ifb)%data%parangle(ilo) = mnt(ifb)%data%parangle(i2)
     endif
  enddo
!
  ilo = 1
  i1 = indxFast(1)
  monBuf(ifb)%data%subScanFast(1) = mnt(ifb)%data%subScanFast(i1)
  monBuf(ifb)%data%encoder_az_el(1,1:3) = mnt(ifb)%data%encoder_az_el(i1,1:3)
  monBuf(ifb)%data%tracking_az_el(1,1:2) = mnt(ifb)%data%tracking_az_el(i1,1:2)
  do i = 1, ndumpAntennaFast-1
     i1 = indxFast(i)
     i2 = indxFast(i+1)
     if (mnt(ifb)%data%encoder_az_el(i2,3).ne.                          &
 &       mnt(ifb)%data%encoder_az_el(i1,3)) then
        ilo = ilo+1
        monBuf(ifb)%data%subScanFast(ilo) = mnt(ifb)%data%subScanFast(i2)
        monBuf(ifb)%data%encoder_az_el(ilo,1:3)                          &
 &      = mnt(ifb)%data%encoder_az_el(i2,1:3)
        monBuf(ifb)%data%tracking_az_el(ilo,1:2)                         &
 &      = mnt(ifb)%data%tracking_az_el(i2,1:2)
     endif
  enddo
!
  i1 = indxSubref(1)
  monBuf(ifb)%data%subScanSubref(1)   = mnt(ifb)%data%subScanSubref(i1)
  monBuf(ifb)%data%focus_x_y_z(1,1:4) = mnt(ifb)%data%focus_x_y_z(i1,1:4)
  monBuf(ifb)%data%dfocus_x_y_z(1,1:3)= mnt(ifb)%data%dfocus_x_y_z(i1,1:3)
  monBuf(ifb)%data%phi_x_y_z(1,1:3)   = mnt(ifb)%data%phi_x_y_z(i1,1:3)
  monBuf(ifb)%data%dphi_x_y_z(1,1:3)  = mnt(ifb)%data%dphi_x_y_z(i1,1:3)
  ilo = 1
  do i = 1, ndumpSubref-1
     i1 = indxSubref(i)
     i2 = indxSubref(i+1)
     if (mnt(ifb)%data%focus_x_y_z(i2,4).ne.mnt(ifb)%data%focus_x_y_z(i1,4)) then
        ilo = ilo+1
        monBuf(ifb)%data%subScanSubref(ilo)   = mnt(ifb)%data%subScanSubref(i2)
        monBuf(ifb)%data%focus_x_y_z(ilo,1:4) = mnt(ifb)%data%focus_x_y_z(i2,1:4)
        monBuf(ifb)%data%dfocus_x_y_z(ilo,1:3)= mnt(ifb)%data%dfocus_x_y_z(i2,1:3)
        monBuf(ifb)%data%phi_x_y_z(ilo,1:3)   = mnt(ifb)%data%phi_x_y_z(i2,1:3)
        monBuf(ifb)%data%dphi_x_y_z(ilo,1:3)  = mnt(ifb)%data%dphi_x_y_z(i2,1:3)
     endif
  enddo
!
  ndumpAntenna = ndumpAntennaNew
  ndumpAntennaFast = ndumpAntennaFastNew
  ndumpSubref = ndumpSubrefNew
!
!
!
  if (ifb.eq.1) allocate(dataBuf(nfebe),stat=ier)
  dataBuf(ifb)%header = dt(ifb)%header
  allocate(dataBuf(ifb)%data%subscan(ndumpBackendNew),                &
  &        dataBuf(ifb)%data%mjd(ndumpBackendNew),                    &
  &        dataBuf(ifb)%data%integnum(ndumpBackendNew),               &
  &        dataBuf(ifb)%data%nints(ndumpBackendNew),                  &
  &        dataBuf(ifb)%data%lst(ndumpBackendNew),                    &
  &        dataBuf(ifb)%data%midtime(ndumpBackendNew),                &
  &        dataBuf(ifb)%data%integtim(ndumpBackendNew),               &
  &        dataBuf(ifb)%data%longoff(ndumpBackendNew),                &
  &        dataBuf(ifb)%data%latoff(ndumpBackendNew),                 &
  &        dataBuf(ifb)%data%azimuth(ndumpBackendNew),                &
  &        dataBuf(ifb)%data%elevatio(ndumpBackendNew),               &
  &        dataBuf(ifb)%data%cbaslong(ndumpBackendNew),               &
  &        dataBuf(ifb)%data%cbaslat(ndumpBackendNew),                &
  &        dataBuf(ifb)%data%baslong(ndumpBackendNew),                &
  &        dataBuf(ifb)%data%baslat(ndumpBackendNew),                 &
  &        dataBuf(ifb)%data%parangle(ndumpBackendNew),               &
  &        dataBuf(ifb)%data%pc_11(ndumpBackendNew),                  &
  &        dataBuf(ifb)%data%pc_12(ndumpBackendNew),                  &
  &        dataBuf(ifb)%data%pc_21(ndumpBackendNew),                  &
  &        dataBuf(ifb)%data%pc_22(ndumpBackendNew),                  &
  &        dataBuf(ifb)%data%mcrval1(ndumpBackendNew),                &
  &        dataBuf(ifb)%data%mcrval2(ndumpBackendNew),                &
  &        dataBuf(ifb)%data%mlonpole(ndumpBackendNew),               &
  &        dataBuf(ifb)%data%mlatpole(ndumpBackendNew),               &
  &        dataBuf(ifb)%data%dfocus_x_y_z(ndumpBackendNew,3),         &
  &        dataBuf(ifb)%data%focus_x_y_z(ndumpBackendNew,3),          &
  &        dataBuf(ifb)%data%dphi_x_y_z(ndumpBackendNew,3),           &
  &        dataBuf(ifb)%data%phi_x_y_z(ndumpBackendNew,3),            &
  &        dataBuf(ifb)%data%otfdata(npix,newchan,ndumpBackendNew),   &    
  &        dataBuf(ifb)%data%rdata(npix,newchan),                     &
  &        dataBuf(ifb)%data%cr4K1(ndumpBackendNew),                  &
  &        dataBuf(ifb)%data%cr4K2(ndumpBackendNew),                  &
  &        stat = ier)
  if (ifb.eq.1) allocate(arrayBuf(nfebe),stat=ier)
  allocate(arrayBuf(ifb)%data(nbd),arrayBuf(ifb)%header(nbd),stat=ier)
  arrayBuf(ifb)%header(1:nbd) = ar(ifb)%header(1:nbd)
  do i = 1, nbd
     allocate(arrayBuf(ifb)%data(i)%data(npix,nchan,ndumpBackendNew,nphases), &
  &           arrayBuf(ifb)%data(i)%iswitch(ndumpBackendNew,nphases),         &
  &           arrayBuf(ifb)%data(i)%mjd(ndumpBackendNew*nphases),             &
  &           arrayBuf(ifb)%data(i)%integtim(ndumpBackendNew*nphases),        &
  &           stat=ier)
  enddo
  !
  ilo = 0
  do i = 1, ndumpBackend
     if (dt(ifb)%data%subscan(i).ne.0) then
        ilo = ilo+1
        dataBuf(ifb)%data%subscan(ilo) = dt(ifb)%data%subscan(i)
        dataBuf(ifb)%data%mjd(ilo) = dt(ifb)%data%mjd(i)
        dataBuf(ifb)%data%integnum(ilo) = ilo
        dataBuf(ifb)%data%nints(ilo) = dt(ifb)%data%nints(i)
        dataBuf(ifb)%data%lst(ilo) = dt(ifb)%data%lst(i)
        dataBuf(ifb)%data%midtime(ilo) = dt(ifb)%data%midtime(i)
        dataBuf(ifb)%data%integtim(ilo) = dt(ifb)%data%integtim(i)
        dataBuf(ifb)%data%longoff(ilo) = dt(ifb)%data%longoff(i)
        dataBuf(ifb)%data%latoff(ilo) = dt(ifb)%data%latoff(i)
        dataBuf(ifb)%data%azimuth(ilo) = dt(ifb)%data%azimuth(i)
        dataBuf(ifb)%data%elevatio(ilo) = dt(ifb)%data%elevatio(i)
        dataBuf(ifb)%data%cbaslong(ilo) = dt(ifb)%data%cbaslong(i)
        dataBuf(ifb)%data%cbaslat(ilo) = dt(ifb)%data%cbaslat(i)
        dataBuf(ifb)%data%baslong(ilo) = dt(ifb)%data%baslong(i)
        dataBuf(ifb)%data%baslat(ilo) = dt(ifb)%data%baslat(i)
        dataBuf(ifb)%data%parangle(ilo) = dt(ifb)%data%parangle(i)
        dataBuf(ifb)%data%pc_11(ilo) = dt(ifb)%data%pc_11(i)
        dataBuf(ifb)%data%pc_12(ilo) = dt(ifb)%data%pc_12(i)
        dataBuf(ifb)%data%pc_21(ilo) = dt(ifb)%data%pc_21(i)
        dataBuf(ifb)%data%pc_22(ilo) = dt(ifb)%data%pc_22(i)
        dataBuf(ifb)%data%cr4K1(ilo) = dt(ifb)%data%cr4K1(i)
        dataBuf(ifb)%data%cr4K2(ilo) = dt(ifb)%data%cr4K2(i)
        dataBuf(ifb)%data%mcrval1(ilo) = dt(ifb)%data%mcrval1(i)
        dataBuf(ifb)%data%mcrval2(ilo) = dt(ifb)%data%mcrval2(i)
        dataBuf(ifb)%data%mlonpole(ilo) = dt(ifb)%data%mlonpole(i)
        dataBuf(ifb)%data%mlatpole(ilo) = dt(ifb)%data%mlatpole(i)
        dataBuf(ifb)%data%otfdata(npix,1:newchan,ilo)                 &
 &      = dt(ifb)%data%otfdata(npix,1:newchan,i)
        dataBuf(ifb)%data%rdata(npix,1:newchan)                       &
 &      = dt(ifb)%data%rdata(npix,1:newchan) 
        i1 = (ilo-1)*nphases+1
        i2 = ilo*nphases
        i3 = (i-1)*nphases+1
        i4 = i*nphases
        do j = 1, nbd
           arrayBuf(ifb)%data(j)%data(1:npix,1:nchan,ilo,1:nphases)    &
 &         = ar(ifb)%data(j)%data(1:npix,1:nchan,i,1:nphases)
           arrayBuf(ifb)%data(j)%iswitch(ilo,1:nphases)     &
 &         = ar(ifb)%data(j)%iswitch(i,1:nphases)
           arrayBuf(ifb)%data(j)%mjd(i1:i2) = ar(ifb)%data(j)%mjd(i3:i4)
           arrayBuf(ifb)%data(j)%integtim(i1:i2) = ar(ifb)%data(j)%integtim(i3:i4)
        enddo
     endif
  enddo

  ndumpBackend = ndumpBackendNew
! 
! now interpolate monitor data at sampling of backend data
!
  do i = 1, ndumpBackend
     
     ilo = locate(monBuf(ifb)%data%mjd,dataBuf(ifb)%data%midtime(i),ndumpAntenna)

     extrapolateSlowTrace: if (.not.traceExtrapolation.and. &
                               (ilo.eq.0.or.ilo.eq.ndumpAntenna)) then
        dataBuf(ifb)%data%lst(i)      = 0.d0
        dataBuf(ifb)%data%longoff(i)  = 0.d0
        dataBuf(ifb)%data%latoff(i)   = 0.d0
        dataBuf(ifb)%data%baslong(i)  = 0.d0
        dataBuf(ifb)%data%baslat(i)   = 0.d0
        dataBuf(ifb)%data%parangle(i) = 0.d0
        do ibd=1,nbd
           arrayBuf(ifb)%data(ibd)%data(1:npix,1:nchan,i,1:nphases) = blankingRaw
        enddo
     else
        if (ilo.eq.0) then
           ilo = 1
        else if (ilo.eq.ndumpAntenna) then
           ilo = ndumpAntenna-1
        endif
        iup = ilo+1

        dTime1 = monBuf(ifb)%data%mjd(iup)-monBuf(ifb)%data%mjd(ilo)
        dTime2 = dataBuf(ifb)%data%midtime(i)-monBuf(ifb)%data%mjd(ilo)

        slope = (monBuf(ifb)%data%lst(iup)-monBuf(ifb)%data%lst(ilo))/dTime1 
        dataBuf(ifb)%data%lst(i) = monBuf(ifb)%data%lst(ilo)+dTime2*slope
 
        slope = (monBuf(ifb)%data%longoff(iup)-monBuf(ifb)%data%longoff(ilo))/dTime1 
        dataBuf(ifb)%data%longoff(i) = monBuf(ifb)%data%longoff(ilo)+dTime2*slope

        slope = (monBuf(ifb)%data%latoff(iup)-monBuf(ifb)%data%latoff(ilo))/dTime1 
        dataBuf(ifb)%data%latoff(i) = monBuf(ifb)%data%latoff(ilo)+dTime2*slope

        slope = (monBuf(ifb)%data%baslong(iup)-monBuf(ifb)%data%baslong(ilo))/dTime1 
        dataBuf(ifb)%data%baslong(i) = monBuf(ifb)%data%baslong(ilo)+dTime2*slope

        slope = (monBuf(ifb)%data%baslat(iup)-monBuf(ifb)%data%baslat(ilo))/dTime1 
        dataBuf(ifb)%data%baslat(i) = monBuf(ifb)%data%baslat(ilo)+dTime2*slope

        slope = (monBuf(ifb)%data%parangle(iup)-monBuf(ifb)%data%parangle(ilo))/dTime1 
        dataBuf(ifb)%data%parangle(i) = monBuf(ifb)%data%parangle(ilo)+dTime2*slope

     endif extrapolateSlowTrace

     ilo = locate(monBuf(ifb)%data%encoder_az_el(1:ndumpAntennaFast,3),          &
   &              dataBuf(ifb)%data%midtime(i),ndumpAntennaFast)

     extrapolateFastTrace: if (.not.traceExtrapolation.and. &
                               (ilo.eq.0.or.ilo.eq.ndumpAntennaFast)) then

        dataBuf(ifb)%data%azimuth(i)  = 0.d0
        dataBuf(ifb)%data%elevatio(i) = 0.d0
        do ibd=1,nbd
           arrayBuf(ifb)%data(ibd)%data(1:npix,1:nchan,i,1:nphases) = blankingRaw
        enddo

     else

        if (ilo.eq.0) then
           ilo = 1
        else if (ilo.eq.ndumpAntennaFast) then
           ilo = ndumpAntennaFast-1
        endif

        dTime1 = monBuf(ifb)%data%encoder_az_el(iup,3)                            &
 &               -monBuf(ifb)%data%encoder_az_el(ilo,3)
        dTime2 = dataBuf(ifb)%data%midtime(i)-monBuf(ifb)%data%encoder_az_el(ilo,3)

        slope = (monBuf(ifb)%data%encoder_az_el(iup,1)                            &
 &              -monBuf(ifb)%data%encoder_az_el(ilo,1))                           &
 &              /dTime1 
        dataBuf(ifb)%data%azimuth(i) = monBuf(ifb)%data%encoder_az_el(ilo,1)+dTime2*slope

        slope = (monBuf(ifb)%data%encoder_az_el(iup,2)                            &
 &               -monBuf(ifb)%data%encoder_az_el(ilo,2))                          &
 &             /dTime1 
        dataBuf(ifb)%data%elevatio(i) = monBuf(ifb)%data%encoder_az_el(ilo,2)     &
 &                                   +dTime2*slope


     endif extrapolateFastTrace

     if (associated(cryo)) then
        ndumpCryo = size(cryo%mjd)
        if (ndumpCryo.gt.1) then
           ilo = locate(                                                     &
   &                    cryo%mjd(1:ndumpCryo),dataBuf(ifb)%data%midtime(i),     &
   &                    ndumpCryo)
           if (ilo.eq.0) ilo = 1
           if (ilo.lt.ndumpCryo) then
               iup = ilo+1
           else
               iup = ilo
           endif
        else
           ilo = 1
           iup = 1
        endif
        if (ilo.eq.iup) then
           dataBuf(ifb)%data%cr4K1(i) = cryo%cr4K1(ilo)
           dataBuf(ifb)%data%cr4K2(i) = cryo%cr4K2(ilo)
        else
           dTime2 = dataBuf(ifb)%data%midtime(i)-cryo%mjd(ilo)
           dTime1 = cryo%mjd(iup)-cryo%mjd(ilo)
           slope = (cryo%cr4K1(iup)-cryo%cr4K1(ilo))/dTime1 
           dataBuf(ifb)%data%cr4K1(i) = cryo%cr4K1(ilo)+dTime2*slope
           slope = (cryo%cr4K2(iup)-cryo%cr4K2(ilo))/dTime1 
           dataBuf(ifb)%data%cr4K2(i) = cryo%cr4K2(ilo)+dTime2*slope
        endif
     endif

     trackFlag = .false.
     if (abs(monBuf(ifb)%data%antenna_az_el(1)-130.5).lt.1.0.and.                &
   &     mod(3.6d+2,monBuf(ifb)%data%antenna_az_el(2)).lt.1.0) then
        trackFlag = .false.  ! use data with crazy tracking errors if antenna in stow position
     else if (abs(monBuf(ifb)%data%tracking_az_el(ilo,1)).gt.trackingCutoff.or.  &
   &     abs(monBuf(ifb)%data%tracking_az_el(ilo,2)).gt.trackingCutoff.or.       &
   &     abs(monBuf(ifb)%data%tracking_az_el(iup,1)).gt.trackingCutoff.or.       &
   &     abs(monBuf(ifb)%data%tracking_az_el(iup,2)).gt.trackingCutoff) then
         do j = 1, nbd
            i1 = (i-1)*nphases+1
            i2 = i*nphases
            arrayBuf(ifb)%data(j)%data(1:npix,1:nchan,i1:i2,1:nphases) =blankingRaw
            trackFlag = .true.
         enddo
     endif
     if (trackFlag) call gagout('W-SYNC: Data flagged due to large tracking errors.')
     if (ndumpSubref.gt.1) then 
        ilo = locate(monBuf(ifb)%data%focus_x_y_z(1:ndumpSubref,4),              &
   &                 dataBuf(ifb)%data%midtime(i),ndumpSubref)

        if (ilo.eq.0) then
           ilo = 1
        else if (ilo.eq.ndumpSubref) then
           ilo = ndumpSubref-1
        endif

        iup = ilo+1

        if (monBuf(ifb)%data%subScanSubref(ilo)                                 &
   &        .ne.monBuf(ifb)%data%subScanSubref(iup)) then
           if (dataBuf(ifb)%data%subscan(i).eq.monBuf(ifb)%data%subScanSubref(ilo)) &
   &       then
              ilo = ilo-1
              iup = iup-1
           else
              ilo = ilo+1
              iup = iup+1
           endif
        endif

        dTime1 = monBuf(ifb)%data%focus_x_y_z(iup,4)                           &
   &             -monBuf(ifb)%data%focus_x_y_z(ilo,4)
        dTime2 = dataBuf(ifb)%data%midtime(i)-monBuf(ifb)%data%focus_x_y_z(ilo,4)

        slope = (monBuf(ifb)%data%focus_x_y_z(iup,1)                           &
   &             -monBuf(ifb)%data%focus_x_y_z(ilo,1))                         &
   &            /dTime1 
        dataBuf(ifb)%data%focus_x_y_z(i,1) = monBuf(ifb)%data%focus_x_y_z(ilo,1)  &
   &                                      +dTime2*slope

        slope = (monBuf(ifb)%data%focus_x_y_z(iup,2)                           &
   &             -monBuf(ifb)%data%focus_x_y_z(ilo,2))                         &
   &            /dTime1 
        dataBuf(ifb)%data%focus_x_y_z(i,2) = monBuf(ifb)%data%focus_x_y_z(ilo,2)  &
   &                                      +dTime2*slope

        slope = (monBuf(ifb)%data%focus_x_y_z(iup,3)                           &
   &             -monBuf(ifb)%data%focus_x_y_z(ilo,3))                         &
   &            /dTime1 
        dataBuf(ifb)%data%focus_x_y_z(i,3) = monBuf(ifb)%data%focus_x_y_z(ilo,3)  &
   &                                      +dTime2*slope

        dataBuf(ifb)%data%dfocus_x_y_z(i,1)=monBuf(ifb)%data%dfocus_x_y_z(ilo,1)
        dataBuf(ifb)%data%dfocus_x_y_z(i,2)=monBuf(ifb)%data%dfocus_x_y_z(ilo,2)
        dataBuf(ifb)%data%dfocus_x_y_z(i,3)=monBuf(ifb)%data%dfocus_x_y_z(ilo,3)

        slope = (monBuf(ifb)%data%phi_x_y_z(iup,3)                            &
   &             -monBuf(ifb)%data%phi_x_y_z(ilo,3))                          &
   &            /dTime1 
        dataBuf(ifb)%data%phi_x_y_z(i,3) = monBuf(ifb)%data%phi_x_y_z(ilo,3)     &
   &                                    +dTime2*slope
     else
        dataBuf(ifb)%data%focus_x_y_z(i,1:3)=monBuf(ifb)%data%focus_x_y_z(1,1:3)
        dataBuf(ifb)%data%dfocus_x_y_z(i,3) = monBuf(ifb)%data%dfocus_x_y_z(1,3)
        dataBuf(ifb)%data%phi_x_y_z(i,3) = monBuf(ifb)%data%phi_x_y_z(1,3)
     endif
  enddo

  tmpSwitchMode = febe(ifb)%header%swtchmod
  call sic_upper(tmpSwitchMode)

  if (index(tmpScanType,'CAL').ne.0.and.index(tmpSwitchMode,'FREQ').eq.0) then

     gn(ifb)%psky(:,:,:) = blankingRaw
     gn(ifb)%phot(:,:,:) = blankingRaw
     gn(ifb)%pcold(:,:,:) = blankingRaw

     if (allocated(mask)) deallocate(mask,stat=ier)
     allocate(mask(ndumpBackend,nphases),stat=ier)
     if (allocated(tmpMask)) deallocate(tmpMask,stat=ier)
     allocate(tmpMask(ndumpBackend,nphases),stat=ier)

     do i = subscanRange(1), subscanRange(2)
        i1 = i-subscanRange(1)+1
        do j = 1, nphases
          mask(1:ndumpBackend,j) = dataBuf(ifb)%data%subscan.eq.i
        enddo
        if (index(raw(i1)%antenna(ifb)%subScanType,'Sky').ne.0.or.              &
   &        index(raw(i1)%subRef(ifb)%subScanType,'Sky').ne.0) then
           do j = 1, nbd 
              do k = 1, npix
                 do l = 1, nchan 
                    tmpMask = mask.and.                                        &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1:nphases)&
   &                   .ne.blankingRaw
                    gn(ifb)%psky(j,k,l)                                        &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1:nphases),&
   &                      tmpMask)/count(tmpMask)
                 enddo
                 meanCounts = sum(gn(ifb)%psky(j,k,:))/nchan
                 where(gn(ifb)%psky(j,k,:).lt.meanCounts/badLevel.or.          &
   &                   gn(ifb)%psky(j,k,:).gt.meanCounts*badLevel)             &
   &                   gn(ifb)%psky(j,k,:) = blankingRaw
              enddo
           enddo

        else if (index(raw(i1)%antenna(ifb)%subScanType,'Ambient').ne.0.or.     &
   &             index(raw(i1)%subRef(ifb)%subScanType,'Ambient').ne.0) then
           do j = 1, nbd 
              do k = 1, npix
                 do l = 1, nchan
                    tmpMask = mask.and.                                        &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1:nphases)&
   &                   .ne.blankingRaw
                    gn(ifb)%phot(j,k,l)                                        &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1:nphases),&
   &                      tmpMask)/count(tmpMask)
                 enddo
                 meanCounts = sum(gn(ifb)%phot(j,k,:))/nchan
                 where(gn(ifb)%phot(j,k,:).lt.meanCounts/badLevel.or.          &
   &                   gn(ifb)%phot(j,k,:).gt.meanCounts*badLevel)             &
   &                   gn(ifb)%phot(j,k,:) = blankingRaw
              enddo
           enddo

        else if (index(raw(i1)%antenna(ifb)%subScanType,'Cold').ne.0.or.        &
   &             index(raw(i1)%subRef(ifb)%subScanType,'Cold').ne.0) then
           do j = 1, nbd 
              do k = 1, npix
                 do l = 1, nchan
                    tmpMask = mask.and.                                        &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1:nphases)&
   &                   .ne.blankingRaw
                    gn(ifb)%pcold(j,k,l)                                       &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1:nphases),&
   &                      tmpMask)/count(tmpMask)
                 enddo
                 meanCounts = sum(gn(ifb)%pcold(j,k,:))/nchan
                 where(gn(ifb)%pcold(j,k,:).lt.meanCounts/badLevel.or.         &
   &                   gn(ifb)%pcold(j,k,:).gt.meanCounts*badLevel)            &
   &                   gn(ifb)%pcold(j,k,:) = blankingRaw
              enddo
           enddo

        else if (index(raw(i1)%antenna(ifb)%subScanType,'Grid').ne.0.or.        &
   &             index(raw(i1)%subRef(ifb)%subScanType,'Grid').ne.0) then
           do j = 1, nbd 
              do k = 1, npix
                 do l = 1, nchan
                    tmpMask = mask.and.                                        &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1:nphases)&
   &                   .ne.blankingRaw
                    gn(ifb)%pgrid(j,k,l)                                       &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1:nphases),&
   &                      tmpMask)/count(tmpMask)
                 enddo
                 meanCounts = abs(sum(gn(ifb)%pgrid(j,k,:))/nchan)
                 where(abs(gn(ifb)%pgrid(j,k,:)).lt.meanCounts/badLevel.or.    &
   &                   abs(gn(ifb)%pgrid(j,k,:)).gt.meanCounts*badLevel)       &
   &                   gn(ifb)%pgrid(j,k,:) = blankingRaw
              enddo
           enddo

        else 
           
           call gagout('W-SYNC: Subscan type unknown. Cannot attribute '       &
   &                   //' mean calibration counts.')

        endif

     enddo

 
     tmpFrontend = febe(ifb)%header%febe
     call sic_upper(tmpFrontend)
     if (tmpFrontend(ipc:ipc).NE.'R'.and.tmpFrontend(ipc:ipc).ne.'I') then 
        where (gn(ifb)%phot.ne.blankingRaw.and.gn(ifb)%psky.ne.blankingRaw)
           gn(ifb)%gainarray = gn(ifb)%phot-gn(ifb)%psky
        else where
           gn(ifb)%gainarray = blankingRaw 
        end where
     endif

  else if (index(tmpScanType,'CAL').ne.0.and.index(tmpSwitchMode,'FREQ').ne.0) &
 &then

     gn(ifb)%psky(:,:,:) = blankingRaw
     gn(ifb)%phot(:,:,:) = blankingRaw
     gn(ifb)%pcold(:,:,:) = blankingRaw
     gn(ifb+nfb)%psky(:,:,:) = blankingRaw
     gn(ifb+nfb)%phot(:,:,:) = blankingRaw
     gn(ifb+nfb)%pcold(:,:,:) = blankingRaw

     if (allocated(maskFSW)) deallocate(maskFSW,stat=ier)
     allocate(maskFSW(ndumpBackend),stat=ier)
     if (allocated(tmpMaskFSW)) deallocate(tmpMaskFSW,stat=ier)
     allocate(tmpMaskFSW(ndumpBackend),stat=ier)

     do i = subscanRange(1), subscanRange(2)
        i1 = i-subscanRange(1)+1
        maskFSW(1:ndumpBackend) = dataBuf(ifb)%data%subscan.eq.i

        if (index(raw(i1)%antenna(ifb)%subScanType,'Sky').ne.0.or.              &
   &        index(raw(i1)%subRef(ifb)%subScanType,'Sky').ne.0) then
           do j = 1, nbd 
              do k = 1, npix
                 do l = 1, nchan 
                    tmpMaskFSW = maskFSW.and.                                  &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1)        &
   &                   .ne.blankingRaw
                    gn(ifb)%psky(j,k,l)                                        &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1),        &
   &                      tmpMaskFSW)/count(tmpMaskFSW)
                    tmpMaskFSW = maskFSW.and.                                  &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,2)        &
   &                   .ne.blankingRaw
                    gn(ifb+nfb)%psky(j,k,l)                                    &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,2),        &
   &                      tmpMaskFSW)/count(tmpMaskFSW)
                 enddo
                 meanCounts = sum(gn(ifb)%psky(j,k,:))/nchan
                 where(gn(ifb)%psky(j,k,:).lt.meanCounts/badLevel.or.          &
   &                   gn(ifb)%psky(j,k,:).gt.meanCounts*badLevel)             &
   &                   gn(ifb)%psky(j,k,:) = blankingRaw
                 meanCounts = sum(gn(ifb+nfb)%psky(j,k,:))/nchan
                 where(gn(ifb+nfb)%psky(j,k,:).lt.meanCounts/badLevel.or.      &
   &                   gn(ifb+nfb)%psky(j,k,:).gt.meanCounts*badLevel)         &
   &                   gn(ifb+nfb)%psky(j,k,:) = blankingRaw
              enddo
           enddo

        else if (index(raw(i1)%antenna(ifb)%subScanType,'Ambient').ne.0.or.     &
   &             index(raw(i1)%subRef(ifb)%subScanType,'Ambient').ne.0) then
           do j = 1, nbd 
              do k = 1, npix
                 do l = 1, nchan
                    tmpMaskFSW = maskFSW.and.                                  &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1)        &
   &                   .ne.blankingRaw
                    gn(ifb)%phot(j,k,l)                                        &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1),        &
   &                      tmpMaskFSW)/count(tmpMaskFSW)
                    tmpMaskFSW = maskFSW.and.                                  &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,2)        &
   &                   .ne.blankingRaw
                    gn(ifb+nfb)%phot(j,k,l)                                    &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,2),        &
   &                      tmpMaskFSW)/count(tmpMaskFSW)
                 enddo
                 meanCounts = sum(gn(ifb)%phot(j,k,:))/nchan
                 where(gn(ifb)%phot(j,k,:).lt.meanCounts/badLevel.or.          &
   &                   gn(ifb)%phot(j,k,:).gt.meanCounts*badLevel)             &
   &                   gn(ifb)%phot(j,k,:) = blankingRaw
                 meanCounts = sum(gn(ifb+nfb)%phot(j,k,:))/nchan
                 where(gn(ifb+nfb)%phot(j,k,:).lt.meanCounts/badLevel.or.      &
   &                   gn(ifb+nfb)%phot(j,k,:).gt.meanCounts*badLevel)         &
   &                   gn(ifb+nfb)%phot(j,k,:) = blankingRaw
              enddo
           enddo

        else if (index(raw(i1)%antenna(ifb)%subScanType,'Cold').ne.0.or.        &
   &             index(raw(i1)%subRef(ifb)%subScanType,'Cold').ne.0) then
           do j = 1, nbd 
              do k = 1, npix
                 do l = 1, nchan
                    tmpMaskFSW = maskFSW.and.                                  &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1)        &
   &                   .ne.blankingRaw
                    gn(ifb)%pcold(j,k,l)                                       &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1),        &
   &                      tmpMaskFSW)/count(tmpMaskFSW)
                    tmpMaskFSW = maskFSW.and.                                  &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,2)        &
   &                   .ne.blankingRaw
                    gn(ifb+nfb)%pcold(j,k,l)                                   &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,2),        &
   &                      tmpMaskFSW)/count(tmpMaskFSW)
                 enddo
                 meanCounts = sum(gn(ifb)%pcold(j,k,:))/nchan
                 where(gn(ifb)%pcold(j,k,:).lt.meanCounts/badLevel.or.         &
   &                   gn(ifb)%pcold(j,k,:).gt.meanCounts*badLevel)            &
   &                   gn(ifb)%pcold(j,k,:) = blankingRaw
                 meanCounts = sum(gn(ifb+nfb)%pcold(j,k,:))/nchan
                 where(gn(ifb+nfb)%pcold(j,k,:).lt.meanCounts/badLevel.or.     &
   &                   gn(ifb+nfb)%pcold(j,k,:).gt.meanCounts*badLevel)        &
   &                   gn(ifb+nfb)%pcold(j,k,:) = blankingRaw
              enddo
           enddo

        else if (index(raw(i1)%antenna(ifb)%subScanType,'Grid').ne.0.or.        &
   &             index(raw(i1)%subRef(ifb)%subScanType,'Grid').ne.0) then
           do j = 1, nbd 
              do k = 1, npix
                 do l = 1, nchan
                    tmpMaskFSW = maskFSW.and.                                  &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1)        &
   &                   .ne.blankingRaw
                    gn(ifb)%pgrid(j,k,l)                                       &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,1),        &
   &                      tmpMaskFSW)/count(tmpMaskFSW)
                    tmpMaskFSW = maskFSW.and.                                  &
   &                   arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,2)        &
   &                   .ne.blankingRaw
                    gn(ifb+nfb)%pgrid(j,k,l)                                   &
   &                = sum(                                                     &
   &                  arrayBuf(ifb)%data(j)%data(k,l,1:ndumpBackend,2),        &
   &                      tmpMaskFSW)/count(tmpMaskFSW)
                 enddo
                 meanCounts = abs(sum(gn(ifb)%pgrid(j,k,:))/nchan)
                 where(abs(gn(ifb)%pgrid(j,k,:)).lt.meanCounts/badLevel.or.    &
   &                   abs(gn(ifb)%pgrid(j,k,:)).gt.meanCounts*badLevel)       &
   &                   gn(ifb)%pgrid(j,k,:) = blankingRaw
                 meanCounts = abs(sum(gn(ifb+nfb)%pgrid(j,k,:))/nchan)
                 where(abs(gn(ifb+nfb)%pgrid(j,k,:)).lt.meanCounts/badLevel.or.&
   &                   abs(gn(ifb+nfb)%pgrid(j,k,:)).gt.meanCounts*badLevel)   &
   &                   gn(ifb+nfb)%pgrid(j,k,:) = blankingRaw
              enddo
           enddo

        else 
           
           call gagout('W-SYNC: Subscan type unknown. Cannot attribute '       &
   &                   //' mean calibration counts.')

        endif

     enddo
 
     tmpFrontend = febe(ifb)%header%febe
     call sic_upper(tmpFrontend)
     if (tmpFrontend(ipc:ipc).NE.'R'.and.tmpFrontend(ipc:ipc).ne.'I') then 
        where (gn(ifb)%phot.ne.blankingRaw.and.gn(ifb)%psky.ne.blankingRaw)
           gn(ifb)%gainarray = gn(ifb)%phot-gn(ifb)%psky
        else where
           gn(ifb)%gainarray = blankingRaw 
        end where
     endif
     if (tmpFrontend(ipc:ipc).NE.'R'.and.tmpFrontend(ipc:ipc).ne.'I') then 
        
        where (gn(ifb+nfb)%phot.ne.blankingRaw.and.gn(ifb+nfb)%psky.ne.       &
  &            blankingRaw)
           gn(ifb+nfb)%gainarray = gn(ifb+nfb)%phot-gn(ifb+nfb)%psky
        else where
           gn(ifb+nfb)%gainarray = blankingRaw 
        end where
     endif
    
  endif

  if (index(tmpSwitchMode,'WOB').ne.0.and.index(tmpScantype,'POINT').eq.0) then
     do i = 1, ndumpBackend
        isub = dataBuf(ifb)%data%subscan(i)
        if (raw(isub)%antenna(ifb)%subScanXOff.lt.0) then
          dataBuf(ifb)%data%longoff(i) = dataBuf(ifb)%data%longoff(i)      &
 &                                 +0.5*scan%header%wobthrow
        else
          dataBuf(ifb)%data%longoff(i) = dataBuf(ifb)%data%longoff(i)      &
 &                                 -0.5*scan%header%wobthrow
        endif
     enddo
  endif

  deallocate(indxSlow,indxFast,indxSubref,stat=ier)

end subroutine syncDataNew
