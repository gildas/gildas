subroutine run_mira(line,comm,error)
   !
   use mira
   !
   implicit none
   !
   character*(*) line, comm
   logical error, sic_present
   !
   if (comm.eq.'SCAN') then
      call getmira(line,error)
   else if (comm.eq.'SHOW') then
      call show(line,error)
   else if (comm.eq.'CAL') then
      call calibrate(line,error)
   else if (comm.eq.'DESPIKE') then
      call despike(line,error)
   else if (comm.eq.'FILE') then
      call directory(line,error)
   else if (comm.eq.'FIND') then
      call findmira(line,error)
   else if (comm.eq.'FLAG') then
      call flagdata(line,error)
   else if (comm.eq.'VIEW') then
      call plot(line,error)
   else if (comm.eq.'LIST') then
      call mira_list(line,error)
   else if (comm.eq.'WRITE') then
      call mira_write(line,error)
   else if (comm.eq.'VARIABLE') then
      call mira_variable(line,error)
   else if (comm.eq.'SOLVE') then
      call solve(line,error)
   else if (comm.eq.'OVERRIDE') then
      call override(line,error)
   else
      call gagout ('E-MIRA, unsupported command')
   endif
!
end subroutine run_mira
