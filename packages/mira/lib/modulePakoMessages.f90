!     
!     $Id$
!    
! <DOCUMENTATION name="modulePakoUtilities">
!
! Utilitiy to handle I W E F "messages".
!
! </DOCUMENTATION>
!
! <DEV> 
! TBD: - more documentation
! <\DEV> 
!
Module modulePakoMessages
  !
  Implicit None
  Save
  Private
  !
  ! *** Subroutines
  !
  Public :: message
  Public :: message_init
  Public :: message_level
  !
  Integer :: iPstdio = 0 , iPfile = 0, iUnit = 0
  !
  Integer, Parameter :: lSeverityCode    = 1
  Integer, Parameter :: nDimSeverityCode = 4
  Character(len=lSeverityCode), Dimension(nDimSeverityCode) :: &
       & sC = (/ 'I','W','E','F' /)
!!!
!!!
Contains
!!!
!!!
  Subroutine message (iPriority, iSeverity, package, messageText)
    !
    !   handle message 
    !
    Integer, Intent(in)          :: iPriority   ! "priority"
    Integer, Intent(in)          :: iSeverity   ! "severity" I W E F
    Character(len=*), Intent(in) :: package     ! name of sw package
    Character(len=*), Intent(in) :: messageText ! text of message
    !
    Integer, Parameter   :: lOut = 160
    Character(len=lOut)  :: messageTextOut
    !
    Integer :: iS, iP, nl
    !
    iS = Min(Max(iSeverity,1),nDimSeverityCode)
    nl = Min(Len_trim(messageText),lOut)
    !
    messageTextOut = messageText(1:nl)
    nl             = Len(messageTextOut)
!!$    Call Sic_noir(messageTextOut,nl)
    !
    If (iPstdio.Le.iPriority) Then
       Write(6,100) sC(iS),package,messageTextOut(1:nl)
    Endif
!!$    If (iPfile.Le.iPriority) Then
!!$       Write(iUnit,101) iPriority,sC(iS),package,messageTextOut(1:nl)
!!$    Endif
    !
100 Format(a,'-',a,',  ',a)
101 Format(i2,'-',a,'-',a,',  ',a)
    !
  End Subroutine message
!!!
!!!
  Subroutine message_level(setPstdio,setPfile)
    !
    !   set minimum "level" for messages to write to std I/O and file
    !
    Integer, Intent(in)           :: setPstdio ! set minumum priority for std. I/O
    Integer, Intent(in), Optional :: setPfile  ! set minimum priority for file
    !
    Integer :: iP
    !
!D    Write (6,*) '   --> message_level: '
    !
    iP = Min(Max(setPstdio,0),9)
    iPstdio = iP
    !
    If (Present(setPfile)) iPfile = setPfile
    !
!D    Write (6,*) '       iPstdio: ', iPstdio
!D    Write (6,*) '       iPfile:  ', iPfile
    !
  End Subroutine message_level
!!!
!!!
  Subroutine message_init(messageFile,iniPstdo,iniPfile)
    !
    ! open file for messages
    !
    Character(len=*)   :: messageFile  !  name of message file
    Integer            :: iniPstdo     !  init minumum priority for std. I/O
    Integer            :: iniPfile     !  init minimum priority for file
    !
    Integer            :: sic_getlun
    !
    Character(len=256) :: messageFileOLD
    Integer            :: ier
    Integer            :: rename
    !
!D    Write (6,*) '   --> message_init: ', messageFile(1:Len_trim(messageFile))
    !
!!$    Call sic_lower(messageFile)
    !
    messageFileOLD = messageFile(1:Len_trim(messageFile))//"~"
    !
    ier = Rename(messageFile, messageFileOLD)
    If (ier.Eq.0) Then
       Write (6,*)  &
            &           'I-message, renamed previous message file: "',  &
            &           messageFile(1:Len_trim(messageFile)), &
            &           '"'
       Write (6,*)  &
            &           'I-message, to: "',  &
            &           messageFileOLD(1:Len_trim(messageFileOLD)), &
            &           '"'
    End If
    !
!!$    ier = sic_getlun(iUnit)
    ier = 1
    iUnit = 55
    If (ier.Eq.1) Then
       Open (unit=iUnit, file=messageFile,  recl=132, & 
            &           status='unknown')
       Write (6,*) 'I-message, opened  message file "', messageFile(1:Len_trim(messageFile)), '"'
    Else
       Write (6,*) 'F-message, error opening message file "', messageFile(1:Len_trim(messageFile)), '"'
    End If
    !
    iPfile  = iniPfile
    iPstdio = iniPstdo
    !
  End Subroutine message_init
!!!
!!!
End Module modulePakoMessages



