   subroutine calibrate(line,error)
!
! Called by subroutine RUN, command CALIBRATE [ifb|all].
!
! Input:
! LINE (character) : command line
!
! Output:
! ERROR (logical)  : error return flag
! Calibration parameters in structure GAINS(1:NFB)%TAUZEN etc. where
! NFB is the number of frontend/backend combinations.
! For details see See module mira in mira_use.f90 and the MIRA manual,
! section 5.
!
   use mira
   use gildas_def
   use gkernel_types
   use gkernel_interfaces
   !
   implicit none
   !
   integer               :: i, ier, ifb, j, jfb, nc, nchan, nrecords,   &
                            nwin
   integer               :: nmask
   integer, dimension(:,:), allocatable :: mask
   integer               :: firstifb, lastifb
   type(sic_descriptor_t) :: desc
   real*8, dimension(:,:), allocatable :: window
   character(len = *)    :: line
   character(len=128)    :: string
   character(len=5)      :: wmode
   character(len=20)     :: name, tmpmode, tmpstring
   character(len=20),parameter :: nullstring = '                    '
   character(len=32)     :: tmpfrontend
   character(len=80)     :: option
   character             :: ch*3
   logical               :: changefebecal, error, exist, ex_gains,   &
                            ex_reduce, readonly
   !
   nmask = 0
   !
   tmpstring = scan%header%scantype
   call sic_upper(tmpstring)
   call sic_get_logi('calCheck',calcheck,error)
   changefebecal = .false.
   !
   if (index(tmpstring,'CAL').ne.0.and.index(tmpstring,'GRID').eq.0)   &
     then
      call gagout('E-CAL: you cannot calibrate a calibration.')
      error = .true.
      return
   endif
   !
   if (.not.sic_present(0,1)) then
      firstifb = 1
      lastifb = 1
   else
      call sic_ch(line,0,1,ch,nc,.true.,error)
      call sic_upper(ch)
      if (index(ch,'ALL').ne.0) then
         firstifb = 1
         lastifb = scan%header%nfebe
      else
         call sic_i4(line,0,1,ifb,.false.,error)
         if (ifb.gt.scan%header%nfebe.or.ifb.le.0) then
            call gagout('E-CAL: Invalid backend specification.')
            error = .true.
            return
         endif
         firstifb = ifb
         lastifb = ifb
      endif
   endif
   !
   if (sic_present(6,0)) then
      if (firstifb.ne.lastifb) then
         call gagout('E-CAL: Please specify the number of the '//   &
                     'frontend-backend combination to be calibrated.')
         error = .true.
         return
      endif
      if (sic_present(6,1)) then
         if (calcheck) then
            call gagout('E-CAL: let calCheck .true. and option '//   &
                        '/FEBECAL are mutually exclusive.')
            error = .true.
            return
         endif
         call sic_i4(line,6,1,jfb,.false.,error)
         if (jfb.gt.scan%header%nfebe.and.jfb.gt.size(gains)) then
            call gagout('E-CAL: impossible febe combination number')
            error = .true.
            return
         endif
         changefebecal = .true.
      else
         call gagout('E-CAL: for this option, you have to specify '//   &
                     'a frontend-backend combination (see HELP).')
         error = .true.
         return
      endif
   endif
   !
   febe_loop: do ifb = firstifb, lastifb
      !
      call gagout('I-CAL: Calibrating '//trim(febe(ifb)%header%febe))
      tmpfrontend = trim(febe(ifb)%header%febe)
      call sic_upper(tmpfrontend)
      !
      nrecords = maxval(data(ifb)%data%integnum)
      !
      if (index(tmpstring,'POIN').ne.0) then
         reduce(ifb)%pointing_done = .false.
      else if (index(tmpstring,'FOCUS').ne.0) then
         reduce(ifb)%focus_done = .false.
      endif
      !
      if (sic_present(1,0)) then
         if (reduce(ifb)%calibration_done(1)) then
            call gagout('I-CALIBRATE, gains already calibrated.')
            return
         else if (.not.associated(gains)) then
            call gagout('E-CAL: No calibration available.')
            call gagout('       First read in a calibration scan.')
            error = .true.
            return
         else
            if (size(gains).eq.2*scan%header%nfebe) then
               call apply_gains_fsw(ifb,jfb,changefebecal,nrecords,error)
            else
               call apply_gains(ifb,jfb,changefebecal,nrecords,error)
            endif
            if (error) return
         endif
      endif
      !
      if (sic_present(2,0)) then
         if (reduce(ifb)%calibration_done(2)) then
            call gagout('I-CALIBRATE, Tcal scale already applied.')
            return
         else
            if (.not.reduce(ifb)%calibration_done(1)) then
               call gagout('E-CALIBRATE, first calibrate gains.')
               error = .true.
               return
            else if (.not.associated(gains)) then
               call gagout('E-CAL: No calibration available.')
               call gagout('       First read in a calibration scan.')
               error = .true.
               return
            else
               if (size(gains).eq.2*scan%header%nfebe) then
                  call apply_tcal_fsw(   &
                    ifb,jfb,changefebecal,nrecords,error   &
                    )
               else
                  call apply_tcal(ifb,jfb,changefebecal,nrecords,error)
               endif
               if (error) return
               if (reduce(ifb)%calibration_done(3))   &
                 call gagout('I-CAL: Now call again CAL /OFF, to '//   &
                             'subtract off and concatenate basebands.')
            endif
         endif
      endif
      !
      if (sic_present(3,0)) then
         if (sic_present(5,0).and.sic_present(5,1).and.sic_present(5,2))   &
           then
            i = 1
            do
               if (sic_present(5,i).and.sic_present(5,i+1)) then
                  nmask = (i+1)/2
                  i = i+2
               else
                  exit
               endif
            enddo
            allocate(mask(2,nmask),stat=ier)
            do i = 1, nmask
               call sic_i4(line,5,2*i-1,mask(1,i),.false.,error)
               call sic_i4(line,5,2*i,mask(2,i),.false.,error)
               if (mask(1,i).gt.nrecords) then
                  mask(1,i) = 1
                  write(string(35:36),'(I2)') i
                  string(1:34)   = 'W-CAL: lower bound of source mask '
                  string(37:59)  = 'out of range, set to 1.'
                  call gagout(string)
               endif
               if (mask(2,i).gt.nrecords) then
                  mask(2,i) = nrecords
                  write(string(35:36),'(I2)') i
                  string(1:34)   = 'W-CAL: upper bound of source mask '
                  string(37:69)  = 'out of range, set to last record.'
                  call gagout(string)
               endif
            enddo
         else if (sic_present(5,0).and.sic_present(5,1).and. &
                  .not.sic_present(5,2)) then
            nmask = maxval(data(ifb)%data%subscan)
            allocate(mask(2,nmask),stat=ier)
            call sic_i4(line,5,1,j,.false.,error)
            do i = 1, nmask
               mask(1,i) &
               =minloc(data(ifb)%data%integnum,1,data(ifb)%data%subscan.eq.i)+j
               mask(2,i) &
               =maxloc(data(ifb)%data%integnum,1,data(ifb)%data%subscan.eq.i)-j
            enddo
         else
            nmask = 0
         endif
         if (sic_present(3,1)) then
            call sic_ch(line,3,1,wmode,nc,.false.,error)
            call sic_upper(wmode)
         else
            if (index(tmpstring,'FLYMAP').ne.0) then
                call gagout('I-CAL: defaults to off weight mode TIME.')
                wmode = 'TIME'
            else
                wmode = 'AVER'
            endif
! Mira versions < 2.0:
!           wmode = 'AVER'
         endif
         nwin = 2
         if (index(wmode,'TO').ne.0) then
            i = 2
            do
               if (sic_present(3,i)) then
                  nwin = i-1
                  i = i+1
               else
                  exit
               endif
            enddo
         endif
         nwin = int(nwin/2.)
         allocate(window(2,nwin),stat=ier)
         if (index(wmode,'TO').eq.0) then
            if (febe(ifb)%header%febe(1:1).eq.'E') then
               call subtract_offE(ifb,nrecords,wmode,window,nwin,   &
                                  mask,nmask,error)
            else
               call subtract_off(ifb,nrecords,wmode,window,nwin,   &
                                 mask,nmask,error)
            endif
         else
            do i = 1, nwin
               call sic_r8(line,3,2*i,window(1,i),.false.,error)
               call sic_r8(line,3,2*i+1,window(2,i),.false.,error)
            enddo
            if (febe(ifb)%header%febe(1:1).eq.'E') then
               call subtract_offE(ifb,nrecords,'NONE',window,nwin,   &
                 mask,nmask,error)
               call subtract_offE(ifb,nrecords,'AVER',window,nwin,   &
                 mask,nmask,error)
            else
               call subtract_off(ifb,nrecords,'NONE',window,nwin,   &
                 mask,nmask,error)
               call subtract_off(ifb,nrecords,'AVER',window,nwin,   &
                 mask,nmask,error)
            endif
         endif
         deallocate(window,stat=ier)
         if (error) return
      endif
      !
      if (sic_present(4,0)) then
         if (tmpfrontend(ipc:ipc).ne.'I') then
            continue
         else if (.not.reduce(ifb)%calibration_done(1).or.   &
                  .not.reduce(ifb)%calibration_done(2)) then
            call gagout('E-CAL: please calibrate for gains and TCal')
            error = .true.
            return
         else if (index(tmpstring,'GRID').ne.0) then
            call do_xpol(ifb,nrecords,error)
         else if (.not.associated(gains(ifb)%phasearray)) then
            call gagout('W-CAL: No valid phase calibration.')
            reduce(ifb)%calibration_done(5) = .false.
         else if (sum(gains(ifb)%phasearray).eq.0.and.   &
                  sum(gains(ifb-1)%phasearray).eq.0) then
            call gagout('W-CAL: First calibrate a calibGrid '//   &
                        ' observation.')
            reduce(ifb)%calibration_done(5) = .false.
         else if (.not.reduce(ifb)%calibration_done(5)) then
            call apply_xpol(ifb,nrecords,error)
            if (reduce(ifb-1)%calibration_done(3)) then
              if (febe(ifb-1)%header%febe(1:1).eq.'E') then
                 call subtract_offE(ifb-1,nrecords,wmode,window,nwin,   &
                                    mask,nmask,error)
              else
                 call subtract_off(ifb-1,nrecords,wmode,window,nwin,   &
                                   mask,nmask,error)
              endif
            endif
            if (reduce(ifb)%calibration_done(3)) then
              if (febe(ifb)%header%febe(1:1).eq.'E') then
                 call subtract_offE(ifb,nrecords,wmode,window,nwin,   &
                                    mask,nmask,error)
              else
                 call subtract_off(ifb,nrecords,wmode,window,nwin,   &
                                   mask,nmask,error)
              endif
            endif
         endif
      endif
      !
      if (allocated(mask)) deallocate(mask,stat=ier)
      !
      if (.not.sic_present(1,0).and..not.sic_present(2,0).and.   &
        .not.sic_present(3,0).and..not.sic_present(4,0)) then
         if (reduce(ifb)%calibration_done(1)) then
            call gagout('I-CALIBRATE, gains already calibrated.')
         else if (.not.associated(gains)) then
            call gagout('E-CAL: No calibration available.')
            call gagout('       First read in a calibration scan.')
            error = .true.
            return
         else
            if (size(gains).eq.2*scan%header%nfebe) then
               call apply_gains_fsw(ifb,jfb,changefebecal,nrecords,error)
            else
               call apply_gains(ifb,jfb,changefebecal,nrecords,error)
            endif
            if (error) return
         endif
         if (reduce(ifb)%calibration_done(2)) then
            call gagout('I-CALIBRATE, Tcal scale already applied.')
         else if (.not.associated(gains)) then
            call gagout('E-CAL: No calibration available.')
            call gagout('       First read in a calibration scan.')
            error = .true.
         else
            if (size(gains).eq.2*scan%header%nfebe) then
               call apply_tcal_fsw(ifb,jfb,changefebecal,nrecords,error)
            else
               call apply_tcal(ifb,jfb,changefebecal,nrecords,error)
            endif
            if (error) return
         endif
         if (tmpfrontend(ipc:ipc).ne.'I') then
            continue
         else if (reduce(ifb)%calibration_done(5)) then
            call gagout('I-CALIBRATE, phases already calibrated.')
         else if (.not.reduce(ifb)%calibration_done(1).or.   &
           .not.reduce(ifb)%calibration_done(2)) then
            call gagout('E-CAL: please calibrate for gains and TCal')
            error = .true.
            return
         else if (index(tmpstring,'GRID').ne.0) then
            call do_xpol(ifb,nrecords,error)
         else if (.not.associated(gains(ifb)%phasearray)) then
            call gagout('W-CAL: No valid phase calibration.')
            reduce(ifb)%calibration_done(5) = .false.
         else if (sum(gains(ifb)%phasearray).eq.0.and.   &
           sum(gains(ifb-1)%phasearray).eq.0) then
            call gagout('W-CAL: First calibrate a calibGrid '//   &
                        ' observation.')
            reduce(ifb)%calibration_done(5) = .false.
         else if (.not.reduce(ifb)%calibration_done(5)) then
            call apply_xpol(ifb,nrecords,error)
            if (reduce(ifb-1)%calibration_done(3)) then
              if (febe(ifb-1)%header%febe(1:1).eq.'E') then
                 call subtract_offE(ifb-1,nrecords,wmode,window,nwin,   &
                                    mask,nmask,error)
              else
                 call subtract_off(ifb-1,nrecords,wmode,window,nwin,   &
                                   mask,nmask,error)
              endif
            endif
         endif
         wmode = 'AVER'
         if (index(tmpstring,'FLYMAP').ne.0) then
            wmode = 'TIME'
            call gagout('I-CAL: defaults to off weight mode TIME.')
         endif
         nwin = 1
         allocate(window(2,nwin),stat=ier)
         if (febe(ifb)%header%febe(1:1).eq.'E') then
            call subtract_offE(ifb,nrecords,wmode,window,nwin,   &
                               mask,nmask,error)
         else
            call subtract_off(ifb,nrecords,wmode,window,nwin,   &
                              mask,nmask,error)
         endif
         deallocate(window,stat=ier)
         if (error) return
      endif
   !
   !
   !
   enddo febe_loop
   ex_reduce = .false.
   do ifb = firstifb, lastifb
      name = nullstring
      write(name,'(A6,I0)') 'REDUCE',ifb
      call sic_descriptor(trim(name),desc,exist)
      if (exist) then
         ex_reduce = .true.
         call sic_descriptor(trim(name)//'%DESPIKE_DONE',desc,exist)
         readonly = desc%readonly
         call sic_delvariable(trim(name),.false.,error)
      endif
   enddo
   if (ex_reduce) call reduce_to_sic(readonly,error)
   !
   if (index(tmpstring,'GRID').ne.0) then
      ex_gains = .false.
      do ifb = firstifb, lastifb
         name = nullstring
         write(name,'(A5,I0)') 'GAINS',ifb
         call sic_descriptor(trim(name),desc,ex_reduce)
         if (exist) then
            ex_gains = .true.
            call sic_descriptor(trim(name)//'%PHASEARRAY',desc,exist)
            readonly = desc%readonly
            call sic_delvariable(trim(name),.false.,error)
         endif
      enddo
      if (ex_gains) call gains_to_sic(readonly,error)
   endif
   !
   call gagout(' ')
   if (reduce(firstifb)%calibration_done(1))   &
     call gagout('   --> normalized by gains')
   if (reduce(firstifb)%calibration_done(2))   &
     call gagout('   --> temperature scale applied')
   do ifb = firstifb, lastifb
      if (reduce(ifb)%calibration_done(5)) then
         call gagout('   --> phases calibrated')
         exit
      endif
   enddo
   if (reduce(firstifb)%calibration_done(3))   &
     call gagout('   --> off subtracted, spectra synthesized')
   call gagout(' ')
   !
   return
end subroutine calibrate
!
!
!
!
subroutine apply_tcal(ifb,jfb,changefebecal,nrecords,error)
!
! Called by subroutine CALIBRATE. Applies the calibration temperature
! to the raw data (after division by the channel gains in APPLY_GAINS).
! A separate calibration temperature is applied to each spectral
! baseband or to each spectral channel (if calByChannel = .true.,
! interactively set by the user by "let calByChannel yes"). The logical
! SIC variable calByChannel is read by MIRA in subroutine getFits.
!
! Input:
! IFB (integer)           : frontend-backend unit to be calibrated
! JFB (integer)           : frontend-backedn unit containing the
!                           calibration parameters to be applied here.
! changeFebeCal (logical) :
! NRECORDS (integer)      : number of records in scan
!
! Output:
! ERROR (logical)         : error return flag
!
! Input/output:
!
! ARRAY(1:NFB)%DATA(1:NBD)%DATA (double), where NFB is the number of
! frontend-backend combinations, and NBD is the number of spectral
! basebands. For details, see module mira in mira_use.f90 and the MIRA
! manual, section 5.
!
! On successful execution, the logical flag
! REDUCE(IFB)%CALIBRATION_DONE(2) is set to .true. (IFB as on input).
!
   use mira
   use gildas_def
   use chopper_def
   !
   implicit none
   !
   integer           :: ifb, jfb, nrecords
   logical           :: changefebecal, error
   !
   integer           :: ichan, ichan1, ichan2, ibd, ier, iphas, ipix, &
                        irec, jchan, j1, j2, k, kfb, nbd, ncc, nphases
   real*8            :: airmdry, airmwet, bval, calfac, imagegain
   real*8, dimension(:), allocatable :: airmassdry, airmasswet
   character(len=32) :: tmpbackend
   character(len=20) :: tmpscantype
   character(len=128):: string
   logical           :: foundcal, nocal
   external          :: airmdry, airmwet
   !
   tmpscantype = scan%header%scantype
   call sic_upper(tmpscantype)
   tmpbackend = febe(ifb)%header%febe
   call sic_upper(tmpbackend)
   nphases = febe(ifb)%header%nphases
   if (reduce(ifb)%calibration_done(1)) then
      bval = blankingred
   else
      bval = blankingraw
   endif
   !
   if (calcheck) then
      foundcal = .false.
      do kfb = 1, size(gains)
         if (gains(kfb)%febe(1:9).eq.febe(ifb)%header%febe(1:9)) then
            nbd = 0
            do ibd = 1, febe(ifb)%header%febeband
               if (gains(kfb)%crvl4f_2(ibd)   &
                 .eq.array(ifb)%header(ibd)%crvl4f_2   &
                 .and.gains(kfb)%channels(ibd)   &
                 .eq.array(ifb)%header(ibd)%channels   &
                 .and.gains(kfb)%cd4f_21(ibd)   &
                 .eq.array(ifb)%header(ibd)%cd4f_21) nbd = nbd+1
            enddo
            if (nbd.eq.febe(ifb)%header%febeband) then
               foundcal = .true.
               exit
            endif
         endif
      enddo
      !
      if (.not.foundcal) then
         write(string,'(A61,I2)') 'W-CAL: No calibration found for '   &
           //'frontend/backend combination ', ifb
         call gagout(string)
         return
      else
         jfb = kfb
      endif
   else if (.not.changefebecal) then
      jfb = ifb
   endif
   !
   if (jfb.ne.ifb) then
      write(string,'(A43,I2,A24,I2)')   &
        'W-CAL: Tcal of frontend/backend combination',   &
        jfb, ' applied to combination ', ifb
      call gagout(string)
   endif
   !
   nocal = .false.
   !
   if ((index(tmpbackend,'CONT').ne.0 .or.   &
        index(tmpbackend,'NBC').ne.0  .or.   &
        index(tmpbackend,'BBC').ne.0  .or.   &
        index(tmpbackend,'ABBA').ne.0).and.  &
        index(tmpscantype,'TIP').ne.0) then
      do ipix = 1,febe(ifb)%header%febefeed
         if (gains(jfb)%tcal(1,ipix,1).eq.-1.) then
            nocal = .true.
         else
            where(array(ifb)%data(1)%data(ipix,1,1:nrecords,1)   &
              .ne.bval)   &
              array(ifb)%data(1)%data(ipix,1,1:nrecords,1)   &
              = array(ifb)%data(1)%data(ipix,1,1:nrecords,1)   &
              *gains(jfb)%tcal(1,ipix,1)
         endif
      enddo
      reduce(ifb)%calibration_done(2) = .not.nocal
      return
   endif
   !
   if (allocated(airmassdry)) deallocate(airmassdry,stat=ier)
   allocate(airmassdry(nrecords),stat=ier)
   if (allocated(airmasswet)) deallocate(airmasswet,stat=ier)
   allocate(airmasswet(nrecords),stat=ier)
   do irec = 1, nrecords
      airmassdry(irec) = airmdry(data(ifb)%data%elevatio(irec))
      airmasswet(irec) = airmwet(data(ifb)%data%elevatio(irec))
   enddo
   !
   nocal = .false.
   !
   do ibd = 1, febe(ifb)%header%febeband
      if (calbychannel) then
         nocal = .true.
         do ipix = 1,febe(ifb)%header%febefeed
            j1 = array(ifb)%header(ibd)%dropchan+1
            j2 = array(ifb)%header(ibd)%dropchan   &
              +array(ifb)%header(ibd)%usedchan
            imagegain = 0.
            k = 0
            do ichan = j1, j2, ncalchan
               k = k+1
               ichan1 = ichan
               ichan2 = min(ichan+ncalchan-1,j2)
               ncc = ichan2-ichan1+1
               if (gains(ifb)%gainimage(ibd,ipix,ichan).ne.0.d0) then
                  imagegain = sum(gains(ifb)%gainimage(ibd,ipix,ichan1:ichan2))/ncc
               else
                  imagegain = febe(ifb)%data%gainimag(ipix,ibd)
               endif
               if (gains(jfb)%tcal(ibd,ipix,k).eq.-1.) then
                  array(ifb)%data(ibd)%data(ipix,ichan1:ichan2,1:nrecords,1:nphases)   &
                    = bval
               else
                  nocal = .false.
               endif
               do iphas = 1,nphases
                  do jchan=ichan1,ichan2
                     where(array(ifb)%data(ibd)%data(ipix,jchan,1:nrecords,                 &
                           iphas).ne.bval)                                                  &
                     array(ifb)%data(ibd)%data(ipix,jchan,1:nrecords,iphas)                 &
                     = array(ifb)%data(ibd)%data(ipix,jchan,1:nrecords,iphas)               &
                       *gains(jfb)%tcal(ibd,ipix,k)                                         &
                       *exp(gains(jfb)%tauzenwet(ibd,ipix,k)*airmasswet(1:nrecords)         &
                            +gains(jfb)%tauzendry(ibd,ipix,k)*airmassdry(1:nrecords))       &
                       /(1.d0+imagegain                                                     &
                         *exp(gains(jfb)%tauzenwet(ibd,ipix,k)*airmasswet(1:nrecords)       &
                              +gains(jfb)%tauzendry(ibd,ipix,k)*airmassdry(1:nrecords)      &
                              -gains(jfb)%tauzenimagewet(ibd,ipix,k)*airmasswet(1:nrecords) &
                              -gains(jfb)%tauzenimagedry(ibd,ipix,k)*airmassdry(1:nrecords) &
                          )                                                                 &
                        )
                  enddo
               enddo
            enddo
         enddo
      else
         do ipix = 1,febe(ifb)%header%febefeed
            if (gains(jfb)%tcal(ibd,ipix,1).eq.-1.) then
               nocal = .true.
            else
               do ichan = 1,array(ifb)%header(ibd)%channels
                  do iphas = 1,nphases
                     if (index(tmpScanType,'CALIBGRID').ne.0) then
                        where(array(ifb)%data(ibd)%data(ipix,ichan,1:nrecords,   &
                          iphas).ne.bval)   &
                          array(ifb)%data(ibd)%data(ipix,ichan,1:nrecords,iphas)   &
                          = array(ifb)%data(ibd)%data(ipix,ichan,1:nrecords,iphas)   &
                          *gains(jfb)%tcal(ibd,ipix,1)
                     else
                        where(array(ifb)%data(ibd)%data(ipix,ichan,1:nrecords,   &
                          iphas).ne.bval)   &
                          array(ifb)%data(ibd)%data(ipix,ichan,1:nrecords,iphas)   &
                          = array(ifb)%data(ibd)%data(ipix,ichan,1:nrecords,iphas)   &
                          *gains(jfb)%tcal(ibd,ipix,1)   &
                          *exp(gains(jfb)%tauzenwet(ibd,ipix,1)*airmasswet(1:nrecords))   &
                          *exp(gains(jfb)%tauzendry(ibd,ipix,1)*airmassdry(1:nrecords))
                     endif
                  enddo
               enddo
            endif
         enddo
      endif
   enddo
   !
   reduce(ifb)%calibration_done(2) = .not.nocal
   !
   !
   return
!
end subroutine apply_tcal
!
!
!
subroutine apply_tcal_fsw(ifb,jfb,changefebecal,nrecords,error)
!
! Same as subroutine APPLY_TCAL but for calibrations done in switch mode
! frequency. The calibration parameters for the phase 1 (lower frequency)
! are in structure gains(1:nfb), those for phase 2 (upper frequency) are
! in gains(nfb+1:2*nfb), where nfb is the number of frontend-backend
! units.
!
   use mira
   use gildas_def
   use chopper_def
   !
   implicit none
   !
   integer           :: ifb, jfb, nrecords
   logical           :: changefebecal, error
   !
   integer           :: ichan, ichan1, ichan2, ibd, ier, iphas, ipix, irec, &
                        jchan, j1, j2, k, kfb, nbd, ncc, nphases
   real*8            :: airmdry, airmwet, bval, calfac, imagegain
   real*8, dimension(:), allocatable :: airmassdry, airmasswet
   character(len=32) :: tmpbackend
   character(len=20) :: tmpscantype
   character(len=128):: string
   logical           :: foundcal, nocal
   external          :: airmdry, airmwet
   !
   tmpscantype = scan%header%scantype
   call sic_upper(tmpscantype)
   tmpbackend = febe(ifb)%header%febe
   call sic_upper(tmpbackend)
   nphases = febe(ifb)%header%nphases
   if (reduce(ifb)%calibration_done(1)) then
      bval = blankingred
   else
      bval = blankingraw
   endif
   !
   if (calcheck) then
      foundcal = .false.
      do kfb = 1, size(gains)
         if (gains(kfb)%febe(1:9).eq.febe(ifb)%header%febe(1:9)) then
            nbd = 0
            do ibd = 1, febe(ifb)%header%febeband
               if (gains(kfb)%crvl4f_2(ibd)   &
                 .eq.array(ifb)%header(ibd)%crvl4f_2   &
                 .and.gains(kfb)%channels(ibd)   &
                 .eq.array(ifb)%header(ibd)%channels   &
                 .and.gains(kfb)%cd4f_21(ibd)   &
                 .eq.array(ifb)%header(ibd)%cd4f_21) nbd = nbd+1
            enddo
            if (nbd.eq.febe(ifb)%header%febeband) then
               foundcal = .true.
               exit
            endif
         endif
      enddo
      !
      if (.not.foundcal) then
         write(string,'(A61,I2)') 'W-CAL: No calibration found for '   &
           //'frontend/backend combination ', ifb
         call gagout(string)
         return
      else
         jfb = kfb
      endif
   else if (.not.changefebecal) then
      jfb = ifb
   endif
   !
   if (jfb.ne.ifb) then
      write(string,'(A43,I2,A24,I2)')   &
        'W-CAL: Tcal of frontend/backend combination',   &
        jfb, ' applied to combination ', ifb
      call gagout(string)
   endif
   !
   nocal = .false.
   !
   !
   if (allocated(airmassdry)) deallocate(airmassdry,stat=ier)
   allocate(airmassdry(nrecords),stat=ier)
   if (allocated(airmasswet)) deallocate(airmasswet,stat=ier)
   allocate(airmasswet(nrecords),stat=ier)
   do irec = 1, nrecords
      airmassdry(irec) = airmdry(data(ifb)%data%elevatio(irec))
      airmasswet(irec) = airmwet(data(ifb)%data%elevatio(irec))
   enddo
   !
   nocal = .false.
   !
   do ibd = 1, febe(ifb)%header%febeband
      if (calbychannel) then
         nocal = .true.
         do ipix = 1,febe(ifb)%header%febefeed
            j1 = array(ifb)%header(ibd)%dropchan+1
            j2 = array(ifb)%header(ibd)%dropchan   &
              +array(ifb)%header(ibd)%usedchan
            imagegain = 0.
            k = 0
            do ichan = j1, j2, ncalchan
               k = k+1
               ichan1 = ichan
               ichan2 = min(ichan+ncalchan-1,j2)
               ncc = ichan2-ichan1+1
               if (gains(ifb)%gainimage(ibd,ipix,ichan).ne.0) then
                  imagegain = sum(gains(ifb)%gainimage(ibd,ipix,ichan1:ichan2))/ncc
               else
                  imagegain = febe(ifb)%data%gainimag(ipix,ibd)
               endif
               if (gains(jfb)%tcal(ibd,ipix,k).eq.-1.) then
                  array(ifb)%data(ibd)%data(ipix,ichan1:ichan2,1:nrecords,1:nphases)   &
                    = bval
               else
                  nocal = .false.
               endif
               do iphas = 1,nphases
                  kfb = jfb+(iphas-1)*scan%header%nfebe
                  do jchan = ichan1, ichan2
                     where(array(ifb)%data(ibd)%data(ipix,jchan,1:nrecords,                   &
                       iphas).ne.bval)                                                        &
                       array(ifb)%data(ibd)%data(ipix,jchan,1:nrecords,iphas)                 &
                       = array(ifb)%data(ibd)%data(ipix,jchan,1:nrecords,iphas)               &
                         *gains(kfb)%tcal(ibd,ipix,k)                                         &
                         *exp(gains(kfb)%tauzenwet(ibd,ipix,k)*airmasswet(1:nrecords)         &
                              +gains(kfb)%tauzendry(ibd,ipix,k)*airmassdry(1:nrecords))       &
                         /(1.d0+imagegain                                                     &
                           *exp(gains(kfb)%tauzenwet(ibd,ipix,k)*airmasswet(1:nrecords)       &
                                +gains(kfb)%tauzendry(ibd,ipix,k)*airmassdry(1:nrecords)      &
                                -gains(kfb)%tauzenimagewet(ibd,ipix,k)*airmasswet(1:nrecords) &
                                -gains(kfb)%tauzenimagedry(ibd,ipix,k)*airmassdry(1:nrecords) &
                            )                                                                 &
                          )
                  enddo
               enddo
            enddo
         enddo
      else
         do ipix = 1,febe(ifb)%header%febefeed
            if (gains(jfb)%tcal(ibd,ipix,1).eq.-1.) then
               nocal = .true.
            else
               do ichan = 1,array(ifb)%header(ibd)%channels
                  do iphas = 1,nphases
                     kfb = jfb+(iphas-1)*scan%header%nfebe
                     where(array(ifb)%data(ibd)%data(ipix,ichan,1:nrecords,   &
                       iphas).ne.bval)   &
                       array(ifb)%data(ibd)%data(ipix,ichan,1:nrecords,iphas)   &
                       = array(ifb)%data(ibd)%data(ipix,ichan,1:nrecords,iphas)   &
                       *gains(kfb)%tcal(ibd,ipix,1)   &
                       *exp(gains(kfb)%tauzenwet(ibd,ipix,1)*airmasswet(1:nrecords))   &
                       *exp(gains(kfb)%tauzendry(ibd,ipix,1)*airmassdry(1:nrecords))
                  enddo
                  kfb = jfb+scan%header%nfebe
                  if (gains(kfb)%gainarray(ibd,ipix,ichan).ne.blankingred)   &
                    gains(kfb)%gainarray(ibd,ipix,ichan)   &
                    = gains(kfb)%gainarray(ibd,ipix,ichan)   &
                    *gains(kfb)%tcal(ibd,ipix,1)   &
                    *exp(gains(kfb)%tauzenwet(ibd,ipix,1)*airmasswet(1))   &
                    *exp(gains(kfb)%tauzendry(ibd,ipix,1)*airmassdry(1))
               enddo
            endif
         enddo
      endif
   enddo
   !
   reduce(ifb)%calibration_done(2) = .not.nocal
   !
   !
   return
!
end subroutine apply_tcal_fsw
!
!
!
subroutine apply_gains(ifb,jfb,changefebecal,nrecords,error)
!
! Called by subroutine CALIBRATE. Applies the channel gains
! in GAINS(JFB)%GAINARRAY (= count rate on hot load - count rate on sky)
! to the raw data in ARRAY(IFB)%DATA%DATA. The gainarray is allocated in
! subroutine getFits, and written in subroutine syncData or syncDataNew.
!
! Input:
! IFB (integer)           : frontend-backend unit to be calibrated
! JFB (integer)           : frontend-backedn unit containing the
!                           calibration parameters to be applied here.
! changeFebeCal (logical) :
! NRECORDS (integer)      : number of records in scan
!
! Output:
! ERROR (logical)         : error return flag
!
! Input/output:
! ARRAY(1:NFB)%DATA(1:NBD)%DATA (double), where NFB is the number of
! frontend-backend combinations, and NBD is the number of spectral
! basebands. For details, see module mira in mira_use.f90 and the MIRA
! manual, section 5.
!
! On successful execution, the logical flag
! REDUCE(IFB)%CALIBRATION_DONE(1) is set to .true. (IFB as on input).
!
   use mira
   use gildas_def
   !
   implicit none
   !
   integer            :: i, ibd, ifb, j, jfb,   &
                         k, kfb, l, nbd,   &
                         nphases, nrecords
   logical            :: error, exist,   &
                         foundgains,   &
                         changefebecal
   real*4             :: meangain, rdump
   real*8             :: bval
   character(len=1)   :: tmpfrontend
   character(len=128) :: string
   !
   !
   if (calcheck) then
      foundgains = .false.
      do kfb = 1, size(gains)
         if (gains(kfb)%febe(1:9).eq.febe(ifb)%header%febe(1:9)) then
            nbd = 0
            do ibd = 1, febe(ifb)%header%febeband
               if (gains(kfb)%crvl4f_2(ibd)   &
                 .eq.array(ifb)%header(ibd)%crvl4f_2   &
                 .and.gains(kfb)%channels(ibd)   &
                 .eq.array(ifb)%header(ibd)%channels   &
                 .and.gains(kfb)%cd4f_21(ibd)   &
                 .eq.array(ifb)%header(ibd)%cd4f_21) nbd = nbd+1
            enddo
            if (nbd.eq.febe(ifb)%header%febeband) then
               foundgains = .true.
               exit
            endif
         endif
      enddo
      !
      if (.not.foundgains) then
         write(string,'(A61,I2)') 'W-CAL: No calibration found for '   &
           //'frontend/backend combination ', ifb
         call gagout(string)
         return
      else
         jfb = kfb
      endif
   else if (.not.changefebecal) then
      jfb = ifb
   endif
   !
   !
   if (jfb.ne.ifb) then
      write(string,'(A61,I2,A16,I2)')   &
        'W-CAL: applying channel gains of'   &
        //'frontend/backend combination ', jfb,   &
        ' to combination ',ifb
      write(string,'(A52,I2,A24,I2)')   &
        'W-CAL: channel gains of frontend/backend combination',   &
        jfb, ' applied to combination ', ifb
      call gagout(string)
   endif
   !
   !
   if (.not.reduce(ifb)%calibration_done(3)) then
      bval = blankingraw
   else
      bval = blankingred
   endif
   !
   tmpfrontend = febe(ifb)%header%febe(ipc:ipc)
   call sic_upper(tmpfrontend)
   nphases = febe(ifb)%header%nphases
   !
   !
   !      DO I = 1, FEBE(IFB)%HEADER%FEBEBAND
   !         DO J = 1, FEBE(IFB)%HEADER%FEBEFEED
   !            DO K = 1, ARRAY(IFB)%HEADER(I)%CHANNELS
   !               GAINS(JFB)%PSKY(I,J,K)
   !     &         = SUM(ARRAY(IFB)%DATA(I)%DATA(J,K,:,1:NPHASES),
   !     &               ARRAY(IFB)%DATA(I)%ISWITCH(:,:).EQ.'OFF')
   !     &           /COUNT(ARRAY(IFB)%DATA(I)%ISWITCH(:,:).EQ.'OFF')
   !               GAINS(JFB)%GAINARRAY(I,J,K) = GAINS(JFB)%PHOT(I,J,K)
   !     &               -GAINS(JFB)%PSKY(I,J,K)
   !            ENDDO
   !         ENDDO
   !      ENDDO
   !      CALL DO_TCAL(JFB,1,NRECORDS,fixOpacity,opac,fixPWV,h2o,error)
   ! this is a test
   !      print*, "all gains and TCal updated"
   !
   ! end of test
   !
   do i = 1, febe(ifb)%header%febeband
      do j = 1, febe(ifb)%header%febefeed
         meangain = sum(gains(jfb)%gainarray(i,j,:),   &
           gains(jfb)%gainarray(i,j,:).ne.blankingraw)   &
           /count(gains(jfb)%gainarray(i,j,:).ne.blankingraw)
         meangain = abs(meangain)
         do k = 1, array(ifb)%header(i)%channels
            rdump = gains(jfb)%gainarray(i,j,k)
            if (tmpfrontend.eq.'R'.or.tmpfrontend.eq.'I') then
               where (array(ifb)%data(i)%data(j,k,1:nrecords,1:nphases)   &
                    .ne.bval.and.rdump.ne.0)
                  array(ifb)%data(i)%data(j,k,1:nrecords,1:nphases)   &
                    = array(ifb)%data(i)%data(j,k,1:nrecords,1:nphases)   &
                    /rdump
               else where
                  array(ifb)%data(i)%data(j,k,1:nrecords,1:nphases)   &
                    = blankingred
               end where
            else if (rdump.eq.0) then
               array(ifb)%data(i)%data(j,k,1:nrecords,1:nphases)   &
                 = blankingred
            else
               where   &
                    (array(ifb)%data(i)%data(j,k,1:nrecords,1:nphases)   &
                    .ne.bval.and.abs(rdump).gt.meangain/badlevel   &
                    .and.abs(rdump).lt.meangain*badlevel)
                  array(ifb)%data(i)%data(j,k,1:nrecords,1:nphases)   &
                    = array(ifb)%data(i)%data(j,k,1:nrecords,1:nphases)   &
                    /rdump
               else where
                  array(ifb)%data(i)%data(j,k,1:nrecords,1:nphases)   &
                    = blankingred
               end where
            endif
         enddo
      enddo
   enddo
   !
   reduce(ifb)%calibration_done(1) = .true.
   !
   return
end subroutine apply_gains
!
!
!
subroutine apply_gains_fsw(ifb,jfb,changefebecal,nrecords,error)
!
! Same as subroutine APPLY_GAINS_FSW, but for the calibration taken
! in switch mode frequency (i.e. channel gains for frequency switching
! phase 1 are applied to the raw data of frequency switching phase 1,
! respectively the gains of phase 2 to the raw data of phase 2).
!
   use mira
   use gildas_def
   !
   implicit none
   !
   integer            :: i, ibd, ifb, j, jfb,   &
                         k, kfb, l, nbd,   &
                         nphases, nrecords
   logical            :: error, exist,   &
                         foundgains,   &
                         changefebecal
   real*4             :: meangain, rdump
   real*8             :: bval
   character(len=1)   :: tmpfrontend
   character(len=128) :: string
   !
   !
   if (calcheck) then
      foundgains = .false.
      do kfb = 1, size(gains)
         if (gains(kfb)%febe(1:9).eq.febe(ifb)%header%febe(1:9)) then
            nbd = 0
            do ibd = 1, febe(ifb)%header%febeband
               if (gains(kfb)%crvl4f_2(ibd)   &
                 .eq.array(ifb)%header(ibd)%crvl4f_2   &
                 .and.gains(kfb)%channels(ibd)   &
                 .eq.array(ifb)%header(ibd)%channels   &
                 .and.gains(kfb)%cd4f_21(ibd)   &
                 .eq.array(ifb)%header(ibd)%cd4f_21) nbd = nbd+1
            enddo
            if (nbd.eq.febe(ifb)%header%febeband) then
               foundgains = .true.
               exit
            endif
         endif
      enddo
      !
      if (.not.foundgains) then
         write(string,'(A61,I2)') 'W-CAL: No calibration found for '   &
           //'frontend/backend combination ', ifb
         call gagout(string)
         return
      else
         jfb = kfb
      endif
   else if (.not.changefebecal) then
      jfb = ifb
   endif
   !
   !
   if (jfb.ne.ifb) then
      write(string,'(A61,I2,A16,I2)')   &
        'W-CAL: applying channel gains of'   &
        //'frontend/backend combination ', jfb,   &
        ' to combination ',ifb
      write(string,'(A52,I2,A24,I2)')   &
        'W-CAL: channel gains of frontend/backend combination',   &
        jfb, ' applied to combination ', ifb
      call gagout(string)
   endif
   !
   !
   if (.not.reduce(ifb)%calibration_done(3)) then
      bval = blankingraw
   else
      bval = blankingred
   endif
   !
   tmpfrontend = febe(ifb)%header%febe(ipc:ipc)
   call sic_upper(tmpfrontend)
   nphases = febe(ifb)%header%nphases
   !
   !
   do i = 1, febe(ifb)%header%febeband
      do j = 1, febe(ifb)%header%febefeed
         do k = 1, 2
            kfb = jfb          !+(K-1)*SCAN%HEADER%NFEBE  ! changed by HW
            meangain =sum(gains(kfb)%gainarray(i,j,:),   &
              gains(kfb)%gainarray(i,j,:).ne.blankingraw)   &
              /count(gains(kfb)%gainarray(i,j,:).ne.blankingraw)
            meangain = abs(meangain)
            do l = 1, array(ifb)%header(i)%channels
               rdump = gains(kfb)%gainarray(i,j,l)
               if (tmpfrontend.eq.'R'.or.tmpfrontend.eq.'I') then
                  where (array(ifb)%data(i)%data(j,l,1:nrecords,k)   &
                       .ne.bval.and.rdump.ne.0)
                     array(ifb)%data(i)%data(j,l,1:nrecords,k)   &
                       = array(ifb)%data(i)%data(j,l,1:nrecords,k)   &
                       /rdump
                  else where
                     array(ifb)%data(i)%data(j,l,1:nrecords,k)   &
                       = blankingred
                  end where
               else if (rdump.eq.0) then
                  array(ifb)%data(i)%data(j,l,1:nrecords,k)   &
                    = blankingred
               else
                  where   &
                       (array(ifb)%data(i)%data(j,l,1:nrecords,k)   &
                       .ne.bval.and.abs(rdump).gt.meangain/badlevel   &
                       .and.abs(rdump).lt.meangain*badlevel)
                     array(ifb)%data(i)%data(j,l,1:nrecords,k)   &
                       = array(ifb)%data(i)%data(j,l,1:nrecords,k)   &
                       /rdump
                  else where
                     array(ifb)%data(i)%data(j,l,1:nrecords,k)   &
                       = blankingred
                  end where
               endif
            enddo
         enddo
      enddo
   enddo
   !
   reduce(ifb)%calibration_done(1) = .true.
   !
   return
end subroutine apply_gains_fsw
!
!
!
!
subroutine subtract_off(ifb,nrecords,wmode,window,nwin,   &
     mask,nmask,error)
!
! Called by subroutine CALIBRATE. Performs the subtraction ON-OFF
! for multiple-phase data, and concatenates adjacent spectral basebands
! to a single output spectrum (to be written to the outputfile for
! CLASS).
!
! Also called by subroutine SOLVE for uncalibrated pointings and for
! focus measurements.
!
! Input:
! ifb           integer         number of frontend/backend unit
! nrecords      integer         number of backend data records
! wmode         character       weight mode for total power subtraction
!                               from on-the-fly scans.
! window        double          lower and upper boundary of spectral region
!                               to be avoided for evaluation of total power
!                               (only for weight mode TOTAL)
! nwin          integer         number of such windows
! mask          integer         If total power reference comes from the OTF map
!                               itself: first and last dump number of an OTF
!                               subscan framing the emission region
! nmask         integer         number of such masks
!
! ARRAY(1:NFB)%DATA(1:NBD)%DATA (double), where NFB is the number of
! spectral frontend-backend units, and NBD is the number of spectral
! basebands.
!
! Output:
! ERROR (logical)               : error return flag
! DATA(IFB)%DATA%RDATA (double) : the spectrum of frontend-backend unit
!                                 IFB (observing modi TRACK and ONOFF).
! DATA(IFB)%DATA%OTFDATA (double) : same as above, but for the o
!
!
! On successful execution, the logical flag
! REDUCE(IFB)%CALIBRATION_DONE(3) is set to .true. (IFB as on input).
!
!
   use mira
   use gildas_def
   use linearregression
   !
   implicit none
   !
   integer                                    :: ichan,ichan1,ichan2,   &
                                                 i, ier, ifb, imask, ipix,   &
                                                 isign, iphas, irec,   &
                                                 irev, isub, i1, i2,   &
                                                 j, k, nchan, nfb,   &
                                                 noffdumps,   &
                                                 nondumps, nphases,   &
                                                 npix, nrecords,   &
                                                 nref,nsub,   &
                                                 noverlap, nusedchan,   &
                                                 nwin, nmask
   real*8                                     :: bval
   real*8                                     :: ref1, ref2, tp1, tp2
   real*8, dimension(2,nwin)                  :: window
   integer, dimension(2,nmask)                :: mask
   integer, dimension(:,:), allocatable       :: j1, j2
   integer, dimension(:), allocatable         :: refsubscan1,   &
                                                 refsubscan2
   integer, dimension(:,:,:,:), allocatable   :: signarray
   real*4                                     :: rmed
   real*4, dimension(:,:), allocatable        :: rdata
   real*4, dimension(:,:,:), allocatable      :: otfdata
   real*8                                     :: tref1, tref2, v
   real*8, dimension(:), allocatable          :: fx
   real*8, dimension(nrecords)                :: tp
   real*8, dimension(:,:), allocatable, save  :: w1, w2
   character(len=3)                           :: tmpsideband
   character(len=5)                           :: wmode, tmpfrontend
   character(len=32)                          :: tmpbackend
   character(len=20)                          :: tmpscantype,   &
                                                 tmpswitchmode
   character(len=65)                          :: string
   logical                                    :: error, exist
   logical, dimension(:), allocatable         :: doneflag,linemask,   &
                                                 overlapflag
   logical, dimension(nrecords)               :: onmask, offmask,   &
                                                 subscanmask
   type(base), dimension(:), allocatable      :: basedata
   type(lg)                                   :: basepar
!
!  The following patch is for the automatic despiking of beam switched
!  pointings.
!
   integer              :: idc
   real, parameter      :: deltaCrit = 0.002
   real*8               :: dmd, dmn, dmx
   real*8, dimension(4) :: delta
!
   nfb = scan%header%nfebe
   if (.not.reduce(ifb)%calibration_done(1).and.   &
     .not.reduce(ifb)%calibration_done(3)) then
      bval = blankingraw
   else
      bval = blankingred
   endif
   !
   npix = febe(ifb)%header%febefeed
   !
   tmpscantype = scan%header%scantype
   call sic_upper(tmpscantype)
   tmpswitchmode = febe(ifb)%header%swtchmod
   call sic_upper(tmpswitchmode)
   tmpbackend = febe(ifb)%header%febe
   call sic_upper(tmpbackend)
   !
   nphases = febe(ifb)%header%nphases
   !
   if ((index(tmpbackend,'CONT').ne.0 .or.  &
       index(tmpbackend,'NBC').ne.0 .or.  &
       index(tmpbackend,'BBC').ne.0) .and. &
       index(tmpscantype,'ONOFF') .ne.0) then

      allocate(basedata(nrecords*nphases),stat=ier)
      do k = 1, febe(ifb)%header%febeband
         do ipix = 1, npix
            i1 = 0
            basedata%x = array(ifb)%data(k)%mjd   &
              -array(ifb)%data(k)%mjd(1)
            do irec = 1, nrecords
               do iphas = 1, nphases
                  i1 = i1+1
                  basedata(i1)%y   &
                    = array(ifb)%data(k)%data(ipix,1,irec,iphas)
               enddo
            enddo
            basepar%n = nrecords*nphases
            call linregress(basedata,basepar,bval)
            i1 = 0
            do irec = 1, nrecords
               do iphas = 1, nphases
                  i1 = i1+1
                  if (array(ifb)%data(k)%data(ipix,1,irec,iphas)   &
                    .ne.bval)   &
                    array(ifb)%data(k)%data(ipix,1,irec,iphas)   &
                    = array(ifb)%data(k)%data(ipix,1,irec,iphas)   &
                    -basepar%a-basepar%b*basedata(i1)%x
               enddo
            enddo
         enddo
      enddo
      deallocate(basedata,stat=ier)
   else if ((index(tmpbackend,'CONT').ne.0 .or.   &
             index(tmpbackend,'NBC').ne.0 .or.    &
             index(tmpbackend,'BBC').ne.0) .and.  &
             index(tmpscantype,'TIP').ne.0) then
      reduce(ifb)%calibration_done(3) = .false.
      return
   endif
   !
   nusedchan = 0
   do k = 1, febe(ifb)%header%febeband
      nusedchan = nusedchan+array(ifb)%header(k)%usedchan
   enddo
   allocate(rdata(npix,nusedchan),   &
            otfdata(npix,nusedchan,nrecords),stat=ier)
   allocate(doneflag(nusedchan),overlapflag(nusedchan),stat=ier)
   !
   !
   !
   if ((index(tmpscantype,'FLYMAP').ne.0.or.   &
     index(tmpscantype,'DIY').ne.0).and.nphases.eq.1) then
      !
      nsub = maxval(data(ifb)%data%subscan)
      if (allocated(refsubscan1)) deallocate(refsubscan1,stat=ier)
      if (allocated(refsubscan2)) deallocate(refsubscan2,stat=ier)
      allocate(refsubscan1(nsub),refsubscan2(nsub),stat=ier)
      refsubscan1(1:nsub) = 0
      refsubscan2(1:nsub) = 0
      !
      !
      !
      !
      nref = 0
      do i = 1, nsub
         subscanmask = (data(ifb)%data%subscan.eq.i)
         if (timingcheck) then
            if (count((   &
              index(array(ifb)%data(1)%iswitch(1:nrecords,1),   &
              'OFF').ne.0   &
              .or.   &
              index(array(ifb)%data(1)%iswitch(1:nrecords,1),   &
              'LOAD').ne.0   &
              ).and.subscanmask).ne.0) then
               nref = nref+1
               do j = i, nsub
                  refsubscan1(j) = i
               enddo
            endif
         else
            if (count(array(ifb)%data(1)%iswitch(1:nrecords,1)  &
                .eq.'OFF'.and.subscanmask).gt.                  &
                count(array(ifb)%data(1)%iswitch(1:nrecords,1)  &
                .eq.'ON'.and.subscanmask).or.                   &
                count(array(ifb)%data(1)%iswitch(1:nrecords,1)  &
                .eq.'LOAD'.and.subscanmask).gt.                 &
                count(array(ifb)%data(1)%iswitch(1:nrecords,1)  &
                .eq.'SKY'.and.subscanmask)) then
               nref = nref+1
               do j = i, nsub
                  refsubscan1(j) = i
               enddo
            endif
         endif
      enddo
      !
      if (nref.ne.0) then
         do i = nsub,1,-1
            subscanmask = (data(ifb)%data%subscan.eq.i)
            if (timingcheck) then
               if (count((   &
                 index(array(ifb)%data(1)%iswitch(1:nrecords,1),   &
                 'OFF').ne.0   &
                 .or.   &
                 index(array(ifb)%data(1)%iswitch(1:nrecords,1),   &
                 'LOAD').ne.0   &
                 ).and.subscanmask).ne.0) then
                  do j = 1, i
                     refsubscan2(j) = i
                  enddo
               endif
            else
               if (count(array(ifb)%data(1)%iswitch(1:nrecords,1)  &
                   .eq.'OFF'.and.subscanmask).gt.                  &
                   count(array(ifb)%data(1)%iswitch(1:nrecords,1)  &
                   .eq.'ON'.and.subscanmask).or.                   &
                   count(array(ifb)%data(1)%iswitch(1:nrecords,1)  &
                   .eq.'LOAD'.and.subscanmask).gt.                 &
                   count(array(ifb)%data(1)%iswitch(1:nrecords,1)  &
                   .eq.'SKY'.and.subscanmask)) then
                  do j = 1, i
                     refsubscan2(j) = i
                  enddo
               endif
            endif
         enddo
      endif
      !
      if (nmask.ne.0) then
         offmask(1:nrecords) = .true.
         onmask(1:nrecords) = .false.
         array(ifb)%data(1)%iswitch(1:nrecords,1) = "OFF"
         do i = 1, nmask
            i1 = mask(1,i)
            i2 = mask(2,i)
            onmask(i1:i2) = .true.
            offmask(i1:i2) = .false.
            array(ifb)%data(1)%iswitch(i1:i2,1) = "ON"
         enddo
         if (allocated(j1)) deallocate(j1,stat=ier)
         if (allocated(j2)) deallocate(j2,stat=ier)
         allocate(j1(2,nmask),j2(2,nmask),stat=ier)
         j1(1:2,1:nmask) = 0
         j2(1:2,1:nmask) = 0
         !
         if (mask(1,1).gt.1) then
            j1(1,1) = 1
            j1(2,1) = mask(1,1)-1
         endif
         if (mask(2,nmask).lt.nrecords) then
            j2(2,nmask) = nrecords
            j2(1,nmask) = mask(2,nmask)+1
         endif
         do i = 1, nmask
            if (i.gt.1) j1(1:2,i) = j2(1:2,i-1)
            if (i.lt.nmask) then
               j2(1,i) = mask(2,i)+1
               j2(2,i) = mask(1,i+1)-1
            endif
         enddo
      !
      !
      else
      !
      !
         if (allocated(j1)) deallocate(j1,stat=ier)
         if (allocated(j2)) deallocate(j2,stat=ier)
         allocate(j1(2,nsub),j2(2,nsub),stat=ier)
         j1(1:2,1:nsub) = 0
         j2(1:2,1:nsub) = 0
         do i = 1, nsub
            j1(1,i) = minloc(data(ifb)%data%integnum,1,   &
              data(ifb)%data%subscan.eq.   &
              refsubscan1(i))
            j1(2,i) = maxloc(data(ifb)%data%integnum,1,   &
              data(ifb)%data%subscan.eq.   &
              refsubscan1(i))
            j2(1,i) = minloc(data(ifb)%data%integnum,1,   &
              data(ifb)%data%subscan.eq.   &
              refsubscan2(i))
            j2(2,i) = maxloc(data(ifb)%data%integnum,1,   &
              data(ifb)%data%subscan.eq.   &
              refsubscan2(i))
         enddo
         onmask(1:nrecords)   &
           = index(array(ifb)%data(1)%iswitch(1:nrecords,1),'SKY').ne.0   &
           .or.   &
           index(array(ifb)%data(1)%iswitch(1:nrecords,1),'ON').ne.0
         offmask(1:nrecords)   &
           = index(array(ifb)%data(1)%iswitch(1:nrecords,1),'OFF').ne.0   &
           .or.   &
           index(array(ifb)%data(1)%iswitch(1:nrecords,1),'LOAD').ne.0
      endif
      !
      otfdata(1:npix,1:nusedchan,1:nrecords) = blankingred
   !
   !
   !
   endif
   !
   !
   !
   ichan2 = 0
   baseband_loop: do k = 1, febe(ifb)%header%febeband
      nchan = array(ifb)%header(k)%usedchan
      ichan1 = ichan2+1
      ichan2 = ichan2+nchan
      rdata(1:npix,ichan1:ichan2) = 0
      !
      ! correct here for phases accidentally lost in the imbFits file
      !
      if (nphases.gt.1) then
         do j = 1, nrecords
            do i = 1, nphases-1
               if (array(ifb)%data(k)%iswitch(j,i+1).eq.   &
                 array(ifb)%data(k)%iswitch(j,i)) then
                  array(ifb)%data(k)%data(1:npix,1:nchan,j,i:i+1) =0.
                  write(string,'(A40,I3,A17,I5)')   &
                    'W-CALIBRATE: a phase is lacking in febe ',ifb,   &
                    ' , record number ',j
                  call gagout(string)
               endif
            enddo
         enddo
      endif
      !
      ! end of correction
      !
      if ((index(tmpscantype,'FLYMAP').ne.0.or.   &
        index(tmpscantype,'DIY').ne.0).and.nphases.eq.1) then
         !
         if (index(wmode,'TO').eq.0) then
            if (allocated(w1)) deallocate(w1,stat=ier)
            allocate(w1(npix,nrecords),stat=ier)
            w1(1:npix,1:nrecords) = 0
            if (allocated(w2)) deallocate(w2,stat=ier)
            allocate(w2(npix,nrecords),stat=ier)
            w2(1:npix,1:nrecords) = 0
            tp(1:nrecords) = 0
         endif
         !
         if (index(wmode,'EQ').ne.0) then
            w1(1:npix,1:nrecords) = 0.5
            w2(1:npix,1:nrecords) = 0.5
         else if (index(wmode,'TI').ne.0.and.nmask.eq.0) then
            do isub = 1, nsub
               i1 = j1(1,isub)
               i2 = j1(2,isub)
               call medianinteger(data(ifb)%data%integnum(i1:i2),   &
                 i2-i1+1,rmed)
               tref1 = data(ifb)%data%midtime(int(rmed))
               i1 = j2(1,isub)
               i2 = j2(2,isub)
               call medianinteger(data(ifb)%data%integnum(i1:i2),   &
                 i2-i1+1,rmed)
               tref2 = data(ifb)%data%midtime(int(rmed))
               forall (ipix=1:npix,irec=1:nrecords,   &
                    data(ifb)%data%subscan(irec).eq.isub   &
                    .and.onmask(irec))
                  w1(ipix,irec)   &
                    = (tref2-data(ifb)%data%midtime(irec))   &
                    /(tref2-tref1)
               end forall
               where(w1(1:npix,1:nrecords).ne.0)   &
                 w2(1:npix,1:nrecords) = 1.-w1(1:npix,1:nrecords)
            enddo
         else if (index(wmode,'TI').ne.0.and.nmask.ne.0) then
            do imask = 1, nmask
               i1 = j1(1,imask)
               i2 = j1(2,imask)
               call medianinteger(data(ifb)%data%integnum(i1:i2),   &
                 i2-i1+1,rmed)
               tref1 = data(ifb)%data%midtime(int(rmed))
               i1 = j2(1,imask)
               i2 = j2(2,imask)
               call medianinteger(data(ifb)%data%integnum(i1:i2),   &
                 i2-i1+1,rmed)
               tref2 = data(ifb)%data%midtime(int(rmed))
               forall (ipix=1:npix,irec=mask(1,imask):mask(2,imask))
                  w1(ipix,irec)   &
                    = (tref2-data(ifb)%data%midtime(irec))   &
                    /(tref2-tref1)
               end forall
               where(w1(1:npix,1:nrecords).ne.0)   &
                 w2(1:npix,1:nrecords) = 1.-w1(1:npix,1:nrecords)
            enddo
         endif
         !
         !
         !
         do i = ichan1, ichan2
            if (array(ifb)%header(k)%cd4f_21.ge.0) then
               irev = i-ichan1+1+array(ifb)%header(k)%dropchan
            else
               irev = nchan-i+ichan1+array(ifb)%header(k)%dropchan
            endif
            do ipix = 1, npix
               if ((index(wmode,'AV').ne.0.or.nref.eq.1).and.          &
                    index(wmode,'NO').eq.0) then
                  tp1 = sum(                                           &
                    array(ifb)%data(k)%data(ipix,irev,1:nrecords,1),   &
                    offmask)/count(offmask)
                  where(onmask) otfdata(ipix,i,1:nrecords)             &
                    = array(ifb)%data(k)%data(ipix,irev,1:nrecords,1)  &
                    -tp1
               else if (index(wmode,'NO').ne.0) then
                  otfdata(ipix,i,1:nrecords)                           &
                    =  array(ifb)%data(k)%data(ipix,irev,1:nrecords,1)
               else if (nmask.eq.0) then
                  do isub = 1, nsub
                     ref1 = 0
                     ref2 = 0
                     i1 = j1(1,isub)
                     i2 = j1(2,isub)
                     if (refsubscan1(isub).ne.0.and.i1.ne.i2)   &
                       ref1 = sum(   &
                       array(ifb)%data(k)%data(ipix,irev,i1:i2,1),   &
                       array(ifb)%data(k)%data(ipix,irev,i1:i2,1)   &
                       .ne.bval   &
                       )/count(   &
                       array(ifb)%data(k)%data(ipix,irev,i1:i2,1)   &
                       .ne.bval)
                     i1 = j2(1,isub)
                     i2 = j2(2,isub)
                     if (refsubscan2(isub).ne.0.and.i1.ne.i2)   &
                       ref2 = sum(   &
                       array(ifb)%data(k)%data(ipix,irev,i1:i2,1),   &
                       array(ifb)%data(k)%data(ipix,irev,i1:i2,1)   &
                       .ne.bval   &
                       )/count(   &
                       array(ifb)%data(k)%data(ipix,irev,i1:i2,1)   &
                       .ne.bval)
                     if (ref1.eq.0) then
                        where(data(ifb)%data%subscan.eq.isub.and.   &
                          array(ifb)%data(k)%data(ipix,irev,   &
                          1:nrecords,1).ne.bval.and.onmask)   &
                          otfdata(ipix,i,1:nrecords) =   &
                          array(ifb)%data(k)%data(ipix,irev,1:nrecords,1)   &
                          -ref2
                     else if (ref2.eq.0) then
                        where(data(ifb)%data%subscan.eq.isub.and.   &
                          array(ifb)%data(k)%data(ipix,irev,   &
                          1:nrecords,1).ne.bval.and.onmask)   &
                          otfdata(ipix,i,1:nrecords) =   &
                          array(ifb)%data(k)%data(ipix,irev,1:nrecords,1)   &
                          -ref1
                     else
                        where(data(ifb)%data%subscan.eq.isub.and.   &
                          array(ifb)%data(k)%data(ipix,irev,   &
                          1:nrecords,1).ne.bval.and.onmask)   &
                          otfdata(ipix,i,1:nrecords) =   &
                          array(ifb)%data(k)%data(ipix,irev,1:nrecords,1)   &
                          -w1(ipix,1:nrecords)*ref1   &
                          -w2(ipix,1:nrecords)*ref2
                     endif
                  enddo
               else
                  do imask = 1, nmask
                     ref1 = 0
                     ref2 = 0
                     i1 = j1(1,imask)
                     i2 = j1(2,imask)
                     if (i1.ne.i2)   &
                       ref1 = sum(   &
                       array(ifb)%data(k)%data(ipix,irev,i1:i2,1),   &
                       array(ifb)%data(k)%data(ipix,irev,i1:i2,1)   &
                       .ne.bval   &
                       )/count(   &
                       array(ifb)%data(k)%data(ipix,irev,i1:i2,1)   &
                       .ne.bval)
                     i1 = j2(1,imask)
                     i2 = j2(2,imask)
                     if (i1.ne.i2)   &
                       ref2 = sum(   &
                       array(ifb)%data(k)%data(ipix,irev,i1:i2,1),   &
                       array(ifb)%data(k)%data(ipix,irev,i1:i2,1)   &
                       .ne.bval   &
                       )/count(   &
                       array(ifb)%data(k)%data(ipix,irev,i1:i2,1)   &
                       .ne.bval)
                     if (ref1.eq.0) then
                        where(data(ifb)%data%integnum.ge.mask(1,imask).and.  &
                              data(ifb)%data%integnum.le.mask(2,imask).and.   &
                          array(ifb)%data(k)%data(ipix,irev,   &
                          1:nrecords,1).ne.bval)         &
                          otfdata(ipix,i,1:nrecords) =   &
                          array(ifb)%data(k)%data(ipix,irev,1:nrecords,1)   &
                          -ref2
                     else if (ref2.eq.0) then
                        where(data(ifb)%data%integnum.ge.mask(1,imask).and.  &
                              data(ifb)%data%integnum.le.mask(2,imask).and.   &
                          array(ifb)%data(k)%data(ipix,irev,   &
                          1:nrecords,1).ne.bval)   &
                          otfdata(ipix,i,1:nrecords) =   &
                          array(ifb)%data(k)%data(ipix,irev,1:nrecords,1)   &
                          -ref1
                     else
                        where(data(ifb)%data%integnum.ge.mask(1,imask).and.  &
                              data(ifb)%data%integnum.le.mask(2,imask).and.   &
                          array(ifb)%data(k)%data(ipix,irev,   &
                          1:nrecords,1).ne.bval)   &
                          otfdata(ipix,i,1:nrecords) =   &
                          array(ifb)%data(k)%data(ipix,irev,1:nrecords,1)   &
                          -w1(ipix,1:nrecords)*ref1   &
                          -w2(ipix,1:nrecords)*ref2
                     endif
                  enddo
               endif
            enddo
         enddo
      !
      else if (index(tmpbackend,'CONT').ne.0   .or.   &
               index(tmpbackend,'NBC').ne.0    .or.   &
               index(tmpbackend,'BBC').ne.0    .or.   &
               index(tmpscantype,'POINT').ne.0 .or.   &
               index(tmpscantype,'TIP').ne.0   .or.   &
               index(tmpscantype,'FOCUS').ne.0) then
         !
         !            IF (INDEX(tmpScanType,'POINT').NE.0.AND.
         !     &          INDEX(tmpSwitchMode,'WOB').NE.0) THEN
         !               FORALL (IPIX=1:NPIX,ICHAN=1:NCHAN,IREC=1:NRECORDS,
         !     &                IPHAS=1:NPHASES,0.5*SCAN%HEADER%WOBTHROW
         !     &                +DATA(IFB)%DATA%LONGOFF(IREC)
         !     &                .LT.FEBE(IFB)%DATA%HPBW(IPIX,1)
         !     &               )
         !                     ARRAY(IFB)%DATA(K)%DATA(IPIX,ICHAN,IREC,IPHAS)
         !     &               = BVAL
         !               END FORALL
         !            ENDIF
         !
         if (allocated(signarray)) deallocate(signarray,stat=ier)
         allocate(signarray(npix,nchan,nrecords,nphases),stat=ier)
         signarray = 1
         forall (ipix=1:npix,ichan=1:nchan,irec=1:nrecords,   &
              iphas=1:nphases,   &
              index(array(ifb)%data(k)%iswitch(irec,iphas),'LOAD')   &
              .ne.0.or.   &
              index(array(ifb)%data(k)%iswitch(irec,iphas),'OFF')   &
              .ne.0)
            signarray(ipix,ichan,irec,iphas) = -1
         end forall
         if (array(ifb)%header(k)%cd4f_21.ge.0) then
            if (febe(ifb)%header%nphases.eq.1) then
               otfdata(1:npix,ichan1:ichan2,1:nrecords)   &
                 = array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,1)
               where(   &
                 array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,1)   &
                 .eq.bval)   &
                 otfdata(1:npix,ichan1:ichan2,1:nrecords)   &
                 = blankingred
            else if (febe(ifb)%header%nphases.eq.2) then
               otfdata(1:npix,ichan1:ichan2,1:nrecords)   &
                 = signarray(1:npix,1:nchan,1:nrecords,1)*   &
                 array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,1)   &
                 +signarray(1:npix,1:nchan,1:nrecords,2)*   &
                 array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,2)
               where(   &
                 array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,1)   &
                 .eq.bval.or.   &
                 array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,2)   &
                 .eq.bval)   &
                 otfdata(1:npix,ichan1:ichan2,1:nrecords)   &
                 = blankingred
            else if (febe(ifb)%header%nphases.eq.4) then
               otfdata(1:npix,ichan1:ichan2,1:nrecords)   &
                 = signarray(1:npix,1:nchan,1:nrecords,1)*   &
                 array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,1)   &
                 +signarray(1:npix,1:nchan,1:nrecords,2)*   &
                 array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,2)   &
                 +signarray(1:npix,1:nchan,1:nrecords,3)*   &
                 array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,3)   &
                 +signarray(1:npix,1:nchan,1:nrecords,4)*   &
                 array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,4)
               where(   &
                 array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,1)   &
                 .eq.bval.or.   &
                 array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,2)   &
                 .eq.bval.or.   &
                 array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,3)   &
                 .eq.bval.or.   &
                 array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,4)   &
                 .eq.bval)   &
                 otfdata(1:npix,ichan1:ichan2,1:nrecords) = blankingred
            endif
         else
            if (febe(ifb)%header%nphases.eq.1) then
               do i = ichan1, ichan2
                  irev = nchan-i+ichan1+array(ifb)%header(k)%dropchan
                  otfdata(1:npix,i,1:nrecords)   &
                    = array(ifb)%data(k)%data(1:npix,irev,1:nrecords,1)
                  where(   &
                    array(ifb)%data(k)%data(1:npix,irev,1:nrecords,1)   &
                    .eq.bval)   &
                    otfdata(1:npix,i,1:nrecords) = blankingred
               enddo
            else if (febe(ifb)%header%nphases.eq.2) then
               do i = ichan1, ichan2
                  irev = nchan-i+ichan1+array(ifb)%header(k)%dropchan
                  otfdata(1:npix,i,1:nrecords)   &
                    = signarray(1:npix,irev,1:nrecords,1)*   &
                    array(ifb)%data(k)%data(1:npix,irev,1:nrecords,1)   &
                    +signarray(1:npix,irev,1:nrecords,2)*   &
                    array(ifb)%data(k)%data(1:npix,irev,1:nrecords,2)
                  where(   &
                    array(ifb)%data(k)%data(1:npix,irev,1:nrecords,1)   &
                    .eq.bval.or.   &
                    array(ifb)%data(k)%data(1:npix,irev,1:nrecords,2)   &
                    .eq.bval)   &
                    otfdata(1:npix,i,1:nrecords) = blankingred
               enddo
            else if (febe(ifb)%header%nphases.eq.4) then
               do i = ichan1, ichan2
                  irev = nchan-i+ichan1+array(ifb)%header(k)%dropchan
                  otfdata(1:npix,i,1:nrecords)   &
                    = signarray(1:npix,irev,1:nrecords,1)*   &
                    array(ifb)%data(k)%data(1:npix,irev,1:nrecords,1)   &
                    +signarray(1:npix,irev,1:nrecords,2)*   &
                    array(ifb)%data(k)%data(1:npix,irev,1:nrecords,2)   &
                    +signarray(1:npix,irev,1:nrecords,3)*   &
                    array(ifb)%data(k)%data(1:npix,irev,1:nrecords,3)   &
                    +signarray(1:npix,irev,1:nrecords,4)*   &
                    array(ifb)%data(k)%data(1:npix,irev,1:nrecords,4)
                  where(   &
                    array(ifb)%data(k)%data(1:npix,irev,1:nrecords,1)   &
                    .eq.bval.or.   &
                    array(ifb)%data(k)%data(1:npix,irev,1:nrecords,2)   &
                    .eq.bval.or.   &
                    array(ifb)%data(k)%data(1:npix,irev,1:nrecords,3)   &
                    .eq.bval.or.   &
                    array(ifb)%data(k)%data(1:npix,irev,1:nrecords,4)   &
                    .eq.bval)   &
                    otfdata(1:npix,i,1:nrecords) = blankingred
               enddo
            endif
         endif
      else if (index(tmpswitchmode,'WOB').ne.0.or.   &
        index(tmpswitchmode,'BEAM').ne.0) then
         !
         !
         do j = 1, nrecords
            if (index(array(ifb)%data(k)%iswitch(j,1),'ON').ne.0   &
              .or.index(array(ifb)%data(k)%iswitch(j,1),'SKY')   &
              .ne.0) then
               isign = 1
            else if (index(wmode,'NO').eq.0) then
               isign = -1
            else
               isign = 0
            endif
            do i = ichan1, ichan2
               if (array(ifb)%header(k)%cd4f_21.ge.0) then
                  irev = i-ichan1+1+array(ifb)%header(k)%dropchan
               else
                  irev = nchan-i+ichan1+array(ifb)%header(k)%dropchan
               endif
               rdata(1:npix,i)   &
                 = rdata(1:npix,i)   &
                 +isign*(   &
                 array(ifb)%data(k)%data(1:npix,irev,j,1)   &
                 -array(ifb)%data(k)%data(1:npix,irev,j,2)   &
                 )
               where(   &
                 array(ifb)%data(k)%data(1:npix,irev,j,1)   &
                 .eq.bval.or.   &
                 array(ifb)%data(k)%data(1:npix,irev,j,2)   &
                 .eq.bval   &
                 ) rdata(1:npix,i) = blankingred
            enddo
         enddo
         where(rdata(1:npix,ichan1:ichan2).ne.blankingred)   &
           rdata(1:npix,ichan1:ichan2)   &
           = rdata(1:npix,ichan1:ichan2)/nrecords
      !
      !
      else if (index(tmpswitchmode,'FREQ').ne.0) then
         !
         if (index(wmode,'NO').ne.0) then
            call gagout('E-CAL: switch mode FREQ only valid together with')
            call gagout('       subtraction FHI-FLO.')
            error = .true.
            return
         endif
         !
         if (index(tmpscantype,'FLYMAP').eq.0.and.   &
           index(tmpscantype,'DIY').eq.0) then
            do j = 1, nrecords
               do i = ichan1, ichan2
                  if (array(ifb)%header(k)%cd4f_21.ge.0) then
                     irev = i-ichan1+1+array(ifb)%header(k)%dropchan
                  else
                     irev = nchan-i+ichan1   &
                       +array(ifb)%header(k)%dropchan
                  endif
                  rdata(1:npix,i)   &
                    = rdata(1:npix,i)   &
                    -array(ifb)%data(k)%data(1:npix,irev,j,1)   &
                    +array(ifb)%data(k)%data(1:npix,irev,j,2)
                  where(   &
                    array(ifb)%data(k)%data(1:npix,irev,j,1)   &
                    .eq.bval.or.   &
                    array(ifb)%data(k)%data(1:npix,irev,j,2)   &
                    .eq.bval   &
                    ) rdata(1:npix,i) = blankingred
               enddo
            enddo
            where(rdata(1:npix,ichan1:ichan2).ne.blankingred)   &
              rdata(1:npix,ichan1:ichan2)   &
              = rdata(1:npix,ichan1:ichan2)/nrecords
            if (size(gains).eq.2*nfb) then
               do i = ichan1, ichan2
                  if (array(ifb)%header(k)%cd4f_21.ge.0) then
                     irev = i-ichan1+1+array(ifb)%header(k)%dropchan
                  else
                     irev = nchan-i+ichan1   &
                       +array(ifb)%header(k)%dropchan
                  endif
                  where (gains(ifb+nfb)%gainarray(k,1:npix,irev)   &
                    .ne.blankingred   &
                    .and.rdata(1:npix,i).ne.blankingred)   &
                    rdata(1:npix,i)=rdata(1:npix,i)   &
                    -gains(ifb+nfb)%gainarray(k,1:npix,irev)
               enddo
            endif
         else
            do j = 1, nrecords
               do i = ichan1, ichan2
                  if (array(ifb)%header(k)%cd4f_21.ge.0) then
                     irev = i-ichan1+1+array(ifb)%header(k)%dropchan
                  else
                     irev = nchan-i+ichan1   &
                       +array(ifb)%header(k)%dropchan
                  endif
                  otfdata(1:npix,i,j)   &
                    = -array(ifb)%data(k)%data(1:npix,irev,j,1)   &
                    +array(ifb)%data(k)%data(1:npix,irev,j,2)
                  where(   &
                    array(ifb)%data(k)%data(1:npix,irev,j,1)   &
                    .eq.bval.or.   &
                    array(ifb)%data(k)%data(1:npix,irev,j,2)   &
                    .eq.bval   &
                    ) otfdata(1:npix,i,j) = blankingred
               enddo
            enddo
         endif
      !
      else if ((index(tmpscantype,'ONOFF').ne.0.or.   &
        index(tmpscantype,'TRACK').ne.0)   &
        .and.   &
        index(tmpswitchmode,'TOT').ne.0) then
         onmask(1:nrecords) = .false.
         offmask(1:nrecords) = .false.
         do j = 1, nrecords
            if (index(array(ifb)%data(k)%iswitch(j,1),'ON').ne.0   &
              .or.index(array(ifb)%data(k)%iswitch(j,1),'SKY')   &
              .ne.0) then
               onmask(j) = .true.
            else
               offmask(j) = .true.
            endif
         enddo
         nondumps = count(onmask)
         noffdumps = count(offmask)
         if (nondumps.eq.0) then
            call gagout('E-CAL: no data from ON position')
            error = .true.
            return
         else if (noffdumps.eq.0.and.index(tmpscantype,'ONOFF').ne.0)     &
           then
            call gagout('W-CAL: no data from OFF position')
         endif
         if (array(ifb)%header(k)%cd4f_21.ge.0) then
            do i = ichan1, ichan2
               irev = i-ichan1+1+array(ifb)%header(k)%dropchan
               do ipix = 1, npix
                  if (count(                                              &
                    array(ifb)%data(k)%data(ipix,irev,1:nrecords,1)       &
                    .eq.bval).eq.0) then
                     rdata(ipix,i) = sum(                                 &
                       array(ifb)%data(k)%data(ipix,irev,1:nrecords,1),   &
                       1,onmask)/nondumps
                     if (noffdumps.ne.0.and.index(wmode,'NO').eq.0)       &
                       rdata(ipix,i) = rdata(ipix,i)-sum(                 &
                       array(ifb)%data(k)%data(ipix,irev,1:nrecords,1),   &
                       1,offmask)/noffdumps
                  else
                     rdata(ipix,i) = bval
                  endif
               enddo
            enddo
         else
            do i = ichan1, ichan2
               irev = nchan-i+ichan1+array(ifb)%header(k)%dropchan
               do ipix = 1, npix
                  if (count(                                              &
                    array(ifb)%data(k)%data(ipix,irev,1:nrecords,1)       &
                    .eq.bval).eq.0) then
                     rdata(ipix,i) = sum(                                 &
                       array(ifb)%data(k)%data(ipix,irev,1:nrecords,1),   &
                       1,onmask)/nondumps
                     if (noffdumps.ne.0.and.index(wmode,'NO').eq.0)       &
                       rdata(ipix,i) = rdata(ipix,i)-sum(                 &
                       array(ifb)%data(k)%data(ipix,irev,1:nrecords,1),   &
                       1,offmask)/noffdumps
                  endif
               enddo
            enddo
         endif
      else if (index(tmpscantype,'GRID').ne.0) then
         if (index(wmode,'NO').ne.0) then
            call gagout('E-CAL: scantype calibGrid not valid without '//&
                        'off subtraction.')
            error = .true.
            return
         endif
         onmask(1:nrecords) = .false.
         offmask(1:nrecords) = .false.
         do j = 1, nrecords
            if (index(array(ifb)%data(k)%iswitch(j,1),'HOT').ne.0)   &
              then
               onmask(j) = .true.
            else if (index(array(ifb)%data(k)%iswitch(j,1),'GRID')   &
              .ne.0) then
               offmask(j) = .true.
            endif
         enddo
         nondumps = count(onmask)
         noffdumps = count(offmask)
         if (nondumps.eq.0) then
            call gagout('E-CAL: no data from ON position')
            error = .true.
            return
         else if (noffdumps.eq.0) then
            call gagout('W-CAL: no data from OFF position')
         endif
         if (array(ifb)%header(k)%cd4f_21.ge.0) then
            do i = ichan1, ichan2
               irev = i-ichan1+1+array(ifb)%header(k)%dropchan
               do ipix = 1, npix
                  if (count(   &
                    array(ifb)%data(k)%data(ipix,irev,1:nrecords,1)   &
                    .eq.bval).eq.0) then
                     rdata(ipix,i) = sum(   &
                       array(ifb)%data(k)%data(ipix,irev,1:nrecords,1),   &
                       1,onmask)/nondumps
                     if (noffdumps.ne.0)   &
                       rdata(ipix,i) = rdata(ipix,i)-sum(   &
                       array(ifb)%data(k)%data(ipix,irev,1:nrecords,1),   &
                       1,offmask)/noffdumps
                  endif
               enddo
            enddo
         else
            do i = ichan1, ichan2
               irev = nchan-i+ichan1+array(ifb)%header(k)%dropchan
               do ipix = 1, npix
                  if (count(   &
                    array(ifb)%data(k)%data(ipix,irev,1:nrecords,1)   &
                    .eq.bval).eq.0) then
                     rdata(ipix,i) = sum(   &
                       array(ifb)%data(k)%data(ipix,irev,1:nrecords,1),   &
                       1,onmask)/nondumps
                     if (noffdumps.ne.0)   &
                       rdata(ipix,i)   &
                       = rdata(ipix,i)-sum(   &
                       array(ifb)%data(k)%data(ipix,irev,1:nrecords,1),   &
                       1,offmask)/noffdumps
                  endif
               enddo
            enddo
         endif
      else
         call gagout('E-CAL: No such switch mode known.')
         error = .true.
         return
      endif
      if (nphases.gt.2) then
         where (otfdata.ne.blankingred) otfdata = otfdata*2/nphases
      endif
   enddo baseband_loop
   !
   ! The following is an automatic flagging of phase samples with too short an
   ! integration time (leading to spikes) in EMIR beam switch pointing data.
   !
   !
!   print *,'flagBSwSpikes,nphases,tmpSwitchMode,tmpBackend,tmpScanType'
!   print *,flagBSwSpikes,nphases,tmpSwitchMode,tmpBackend,tmpScanType
   if (flagBSwSpikes.and.nphases.eq.4 .and.  &
       index(tmpSwitchMode,'BEAM').ne.0) then
      if (index(tmpBackend,'CONT').ne.0.or.    &
          index(tmpBackend,'NBC').ne.0.or.    &
          index(tmpBackend,'BBC').ne.0.and.    &
          (index(tmpScanType,'POIN').ne.0.or.   &
           index(tmpScanType,'FOCU').ne.0)) then  

          i1 = 0
          do i = 1, nrecords
             delta(1) = array(ifb)%data(1)%integtim(i1+1)
             delta(2) = array(ifb)%data(1)%integtim(i1+2)
             delta(3) = array(ifb)%data(1)%integtim(i1+3)
             delta(4) = array(ifb)%data(1)%integtim(i1+4)
             dmx = maxval(delta)
             dmn = minval(delta)
             i1 = i1+nphases
             do j = 1, 4
                if (delta(j).ne.dmx.and.delta(j).ne.dmn) then
                   dmd = delta(j)
                   exit
                endif
             enddo
             idc = 0
             do j = 1, 4
                if (abs(delta(j)-dmd).gt.deltaCrit) idc = idc+1
             enddo
             if (idc.gt.0) then
                if (i.eq.1) then
                   otfdata(1:npix,1,i) = otfdata(1:npix,1,2)
                else if (i.eq.nrecords) then
                   otfdata(1:npix,1,i) = otfdata(1:npix,1,nrecords-1)
                else
                   otfdata(1:npix,1,i) = 0.5*(otfdata(1:npix,1,i-1)   &
                                                +otfdata(1:npix,1,i+1))
                endif
             endif
          enddo
          print *,'BeamSwitch Spikes found ',idc
      endif
   endif
   !
   if (index(tmpbackend,'100K').eq.0) then
      !
      allocate(fx(nusedchan),stat=ier)
      !
      ichan2 = 0
      overlapflag(1:nusedchan) = .false.
      do k = 1, febe(ifb)%header%febeband
         nchan = array(ifb)%header(k)%usedchan
         ichan1 = ichan2+1
         ichan2 = ichan2+nchan
         do i = ichan1, ichan2
            if (array(ifb)%header(k)%cd4f_21.ge.0) then
               irev = i-ichan1+1+array(ifb)%header(k)%dropchan
            else
               irev = nchan-i+ichan1+array(ifb)%header(k)%dropchan
            endif
            fx(i) = array(ifb)%header(k)%crvl4f_2   &
              +(irev-array(ifb)%header(k)%crpx4f_2)   &
              *array(ifb)%header(k)%cd4f_21
            if (i.gt.1) then
               if (fx(i)-fx(i-1).le.0) then
                  j = abs((fx(i)-fx(i-1))   &
                    /array(ifb)%header(k)%cd4f_21)+1
                  overlapflag(i-j:i+j-1) = .true.
               else if (fx(i)-fx(i-1).gt.   &
                 abs(array(ifb)%header(k)%cd4f_21)) then
                  call gagout('E-CALIBRATE: '//   &
                              'Data lacking for spectral channels.')
                  error = .true.
!                  return
               endif
            endif
         enddo
      enddo
      !
      j = 0
      doneflag(1:nusedchan) = .false.
      do i = 1, nusedchan
         if (.not.doneflag(i)) then
            if (.not.overlapflag(i)) then
               j = j+1
               data(ifb)%data%rdata(1:npix,j) = rdata(1:npix,i)
               data(ifb)%data%otfdata(1:npix,j,1:nrecords)   &
                 = otfdata(1:npix,i,1:nrecords)
            else
               do k = i, nusedchan
                  if (.not.overlapflag(k)) exit
               enddo
               noverlap = k-i
               do k = i, i+noverlap/2-1
                  j = j+1
                  where (rdata(1:npix,k).ne.blankingred.and.   &
                       rdata(1:npix,k+noverlap/2).ne.blankingred)
                     data(ifb)%data%rdata(1:npix,j)   &
                       = 0.5*(rdata(1:npix,k)   &
                       +rdata(1:npix,k+noverlap/2))
                  else where (rdata(1:npix,k).eq.blankingred.and.   &
                       rdata(1:npix,k+noverlap/2).ne.   &
                       blankingred)
                     data(ifb)%data%rdata(1:npix,j)   &
                       = rdata(1:npix,k+noverlap/2)
                  else where (rdata(1:npix,k).ne.blankingred.and.   &
                       rdata(1:npix,k+noverlap/2).eq.   &
                       blankingred)
                     data(ifb)%data%rdata(1:npix,j) = rdata(1:npix,k)
                  else where
                     data(ifb)%data%rdata(1:npix,j) = blankingred
                  end where
                  where (otfdata(1:npix,k,1:nrecords).ne.blankingred   &
                       .and.otfdata(1:npix,k+noverlap/2,1:nrecords)   &
                       .ne.blankingred)
                     data(ifb)%data%otfdata(1:npix,j,1:nrecords)   &
                       = 0.5*(otfdata(1:npix,k,1:nrecords)   &
                       +otfdata(1:npix,k+noverlap/2,1:nrecords))
                  else where (otfdata(1:npix,k,1:nrecords).eq.   &
                       blankingred.and.   &
                       otfdata(1:npix,k+noverlap/2,1:nrecords)   &
                       .ne.blankingred)
                     data(ifb)%data%otfdata(1:npix,j,1:nrecords)   &
                       = otfdata(1:npix,k+noverlap/2,1:nrecords)
                  else where (otfdata(1:npix,k,1:nrecords).ne.   &
                       blankingred.and.   &
                       otfdata(1:npix,k+noverlap/2,1:nrecords)   &
                       .eq.blankingred)
                     data(ifb)%data%otfdata(1:npix,j,1:nrecords)   &
                       = otfdata(1:npix,k,1:nrecords)
                  else where
                     data(ifb)%data%otfdata(1:npix,j,1:nrecords)   &
                       = blankingred
                  end where
               enddo
               doneflag(i:i+noverlap-1) = .true.
            endif
         endif
      enddo
      !
      nchan = data(ifb)%header%channels
      if (fx(nusedchan).ne.fx(1)) then
         data(ifb)%header%crpx4f_2   &
           = 1.0+(array(ifb)%header(1)%restfreq-fx(1))   &
           *(nchan-1.)/(fx(nusedchan)-fx(1))
      else
         data(ifb)%header%crpx4f_2 = 1
      endif
   !
   else
   !
      allocate(fx(2),stat=ier)
      nchan = data(ifb)%header%channels
      fx(1) = array(ifb)%header(1)%crvl4f_2
      fx(2) = fx(1)+(nchan-1.)*array(ifb)%header(1)%cd4f_21
      data(ifb)%header%crpx4f_2   &
        = 1.0+(array(ifb)%header(1)%restfreq-fx(1))   &
        *(nchan-1.)/(fx(2)-fx(1))
      data(ifb)%header%crpx4f_2   &
        = nint(1.e3*data(ifb)%header%crpx4f_2)/1.d3
      if (nchan.eq.253) then
         !
         data(ifb)%data%rdata(1:npix,1:125) = rdata(1:npix,1:125)
         data(ifb)%data%otfdata(1:npix,1:125,1:nrecords)   &
           = otfdata(1:npix,1:125,1:nrecords)
         !
         where (rdata(1:npix,126:128).ne.blankingred.and.   &
              rdata(1:npix,129:131).ne.blankingred)
            data(ifb)%data%rdata(1:npix,126:128)   &
              = 0.5*(rdata(1:npix,126:128)+rdata(1:npix,129:131))
         else where (rdata(1:npix,126:128).eq.blankingred   &
              .and.rdata(1:npix,129:131).ne.blankingred)
            data(ifb)%data%rdata(1:npix,126:128)   &
              = rdata(1:npix,129:131)
         else where (rdata(1:npix,126:128).ne.blankingred   &
              .and.rdata(1:npix,129:131).eq.blankingred)
            data(ifb)%data%rdata(1:npix,126:128)   &
              = rdata(1:npix,126:128)
         else where
            data(ifb)%data%rdata(1:npix,126:128) = blankingred
         end where
         !
         where (otfdata(1:npix,126:128,1:nrecords).ne.blankingred   &
              .and.   &
              otfdata(1:npix,129:131,1:nrecords).ne.blankingred)
            data(ifb)%data%otfdata(1:npix,126:128,1:nrecords)   &
              = 0.5*(otfdata(1:npix,126:128,1:nrecords)   &
              +otfdata(1:npix,129:131,1:nrecords))
         else where (otfdata(1:npix,126:128,1:nrecords)   &
              .eq.blankingred.and.   &
              otfdata(1:npix,129:131,1:nrecords).ne.blankingred)
            data(ifb)%data%otfdata(1:npix,126:128,1:nrecords)   &
              = otfdata(1:npix,129:131,1:nrecords)
         else where (otfdata(1:npix,126:128,1:nrecords)   &
              .ne.blankingred.and.   &
              otfdata(1:npix,129:131,1:nrecords).eq.blankingred)
            data(ifb)%data%otfdata(1:npix,126:128,1:nrecords)   &
              = otfdata(1:npix,126:128,1:nrecords)
         else where
            data(ifb)%data%otfdata(1:npix,126:128,1:nrecords)   &
              = blankingred
         end where
         !
         data(ifb)%data%rdata(1:npix,129:253) = rdata(1:npix,132:256)
         data(ifb)%data%otfdata(1:npix,129:253,1:nrecords)   &
           = otfdata(1:npix,132:256,1:nrecords)
      !
      else if (nchan.eq.128) then
         !
         data(ifb)%data%rdata(1:npix,1:nchan) = rdata(1:npix,1:nchan)
         data(ifb)%data%otfdata(1:npix,1:nchan,1:nrecords)   &
           = otfdata(1:npix,1:nchan,1:nrecords)
      !
      endif
   !
   endif
   !
   data(ifb)%header%crpx4r_2 = data(ifb)%header%crpx4f_2
   data(ifb)%header%crvl4f_2 = array(ifb)%header(1)%restfreq
   data(ifb)%header%crvl4r_2 = array(ifb)%header(1)%vsou4r_2
   tmpsideband = array(ifb)%header(1)%sideband
   call sic_upper(tmpsideband)
   tmpfrontend = febe(ifb)%header%febe
   call sic_upper(tmpfrontend)
   if (tmpsideband.eq.'LSB') then
      data(ifb)%header%cd4f_21 =  abs(array(ifb)%header(1)%cd4f_21)
      data(ifb)%header%cd4r_21 = -abs(array(ifb)%header(1)%cd4r_21)
   else
      data(ifb)%header%cd4f_21 = -abs(array(ifb)%header(1)%cd4f_21)
      data(ifb)%header%cd4r_21 =  abs(array(ifb)%header(1)%cd4r_21)
   endif
   !
   if (allocated(signarray)) deallocate(signarray,stat=ier)
   deallocate(doneflag,fx,otfdata,overlapflag,rdata,stat=ier)
   !
   if ((index(tmpscantype,'FLYMAP').ne.0.or.   &
     index(tmpscantype,'DIY').ne.0).and.nphases.eq.1   &
     .and.index(wmode,'NO').ne.0) then
      allocate(linemask(nchan),stat=ier)
      linemask(1:nchan) = .false.
      do i = 1, nchan
         v =(i-data(ifb)%header%crpx4r_2)*data(ifb)%header%cd4r_21   &
           +data(ifb)%header%crvl4r_2
         do j = 1, nwin
            if (v.ge.window(1,j).and.v.le.window(2,j))   &
              linemask(i) = .true.
         enddo
      enddo
      if (nmask.eq.0) then
         do isub = 1, nsub
            do ipix = 1, npix
               i1 = j1(1,isub)
               i2 = j1(2,isub)
               tp1 = sum(data(ifb)%data%otfdata(ipix,1:nchan,i1:i2),   &
                 data(ifb)%data%otfdata(ipix,1:nchan,i1:i2)   &
                 .ne.blankingred   &
                 )/count(   &
                 data(ifb)%data%otfdata(ipix,1:nchan,i1:i2)   &
                 .ne.blankingred   &
                 )
               i1 = j2(1,isub)
               i2 = j2(2,isub)
               tp2 = sum(data(ifb)%data%otfdata(ipix,1:nchan,i1:i2),   &
                 data(ifb)%data%otfdata(ipix,1:nchan,i1:i2)   &
                 .ne.blankingred   &
                 )/count(   &
                 data(ifb)%data%otfdata(ipix,1:nchan,i1:i2)   &
                 .ne.blankingred   &
                 )
               forall (i=1:nrecords,onmask(i).and.   &
                    data(ifb)%data%subscan(i).eq.isub)
                  tp(i) = sum(data(ifb)%data%otfdata(ipix,1:nchan,i),   &
                    data(ifb)%data%otfdata(ipix,1:nchan,i)   &
                    .ne.blankingred.and.   &
                    .not.linemask(1:nchan)   &
                    )/count(   &
                    data(ifb)%data%otfdata(ipix,1:nchan,i)   &
                    .ne.blankingred.and.   &
                    .not.linemask(1:nchan)   &
                    )
               end forall
               forall (i=1:nrecords,onmask(i).and.   &
                    data(ifb)%data%subscan(i).eq.isub.and.tp1.ne.tp2)
                  w1(ipix,i) = (tp(i)-tp2)/(tp1-tp2)
               end forall
               where (w1(ipix,1:nrecords).ne.0)   &
                 w2(ipix,1:nrecords) = 1.-w1(ipix,1:nrecords)
            enddo
         enddo
      else
         do imask = 1, nmask
            do ipix = 1, npix
               i1 = j1(1,imask)
               i2 = j1(2,imask)
               tp1 = sum(data(ifb)%data%otfdata(ipix,1:nchan,i1:i2),   &
                 data(ifb)%data%otfdata(ipix,1:nchan,i1:i2)   &
                 .ne.blankingred   &
                 )/count(   &
                 data(ifb)%data%otfdata(ipix,1:nchan,i1:i2)   &
                 .ne.blankingred   &
                 )
               i1 = j2(1,imask)
               i2 = j2(2,imask)
               tp2 = sum(data(ifb)%data%otfdata(ipix,1:nchan,i1:i2),   &
                 data(ifb)%data%otfdata(ipix,1:nchan,i1:i2)   &
                 .ne.blankingred   &
                 )/count(   &
                 data(ifb)%data%otfdata(ipix,1:nchan,i1:i2)   &
                 .ne.blankingred   &
                 )
               forall (i=mask(1,imask):mask(2,imask))
                  tp(i) = sum(data(ifb)%data%otfdata(ipix,1:nchan,i),   &
                    data(ifb)%data%otfdata(ipix,1:nchan,i)   &
                    .ne.blankingred.and.   &
                    .not.linemask(1:nchan)   &
                    )/count(   &
                    data(ifb)%data%otfdata(ipix,1:nchan,i)   &
                    .ne.blankingred.and.   &
                    .not.linemask(1:nchan)   &
                    )
               end forall
               forall (i=mask(1,imask):mask(2,imask),tp1.ne.tp2)
                  w1(ipix,i) = (tp(i)-tp2)/(tp1-tp2)
               end forall
               where (w1(ipix,1:nrecords).ne.0)   &
                 w2(ipix,1:nrecords) = 1.-w1(ipix,1:nrecords)
            enddo
         enddo
      endif
      deallocate(linemask,stat=ier)
   endif
   !
   if ((index(tmpscantype,'FLYMAP').ne.0.or.   &
     index(tmpscantype,'DIY').ne.0).and.nphases.eq.1   &
     .and.index(wmode,'NO').eq.0) then
      !
      do ichan = 1, nchan
         do ipix = 1, npix
            where(.not.onmask)   &
              data(ifb)%data%otfdata(ipix,ichan,1:nrecords)   &
              = blankingred
         enddo
      enddo
   !
   endif
   !
   !
   if (index(wmode,'NO').ne.0) then
      reduce(ifb)%calibration_done(3) = .false.
   else
      reduce(ifb)%calibration_done(3) = .true.
   endif
   !
   !
   return
end subroutine subtract_off
!
!
!
subroutine subtract_offE(ifb,nrecords,wmode,window,nwin,   &
     mask,nmask,error)
!
! Called by subroutine CALIBRATE. Performs the subtraction ON-OFF
! for multiple-phase data, and concatenates adjacent spectral basebands
! to a single output spectrum (to be written to the outputfile for
! CLASS).
!
! Also called by subroutine SOLVE for uncalibrated pointings and for
! focus measurements.
!
! Input:
! ifb           integer         number of frontend/backend unit
! nrecords      integer         number of backend data records
! wmode         character       weight mode for total power subtraction
!                               from on-the-fly scans.
! window        double          lower and upper boundary of spectral region
!                               to be avoided for evaluation of total power
!                               (only for weight mode TOTAL)
! nwin          integer         number of such windows
! mask          integer         If total power reference comes from the OTF map
!                               itself: first and last dump number of an OTF
!                               subscan framing the emission region
! nmask         integer         number of such masks
!
! ARRAY(1:NFB)%DATA(1:NBD)%DATA (double), where NFB is the number of
! spectral frontend-backend units, and NBD is the number of spectral
! basebands.
!
! Output:
! ERROR (logical)               : error return flag
! DATA(IFB)%DATA%RDATA (double) : the spectrum of frontend-backend unit
!                                 IFB (observing modi TRACK and ONOFF).
! DATA(IFB)%DATA%OTFDATA (double) : same as above, but for the o
!
!
! On successful execution, the logical flag
! REDUCE(IFB)%CALIBRATION_DONE(3) is set to .true. (IFB as on input).
!
   use mira
   use gildas_def
   use linearregression
   !
   implicit none
   !
   integer                                    :: ichan, i, ier, ifb, imask, &
                                                 ipix, isign, iphas, irec,  &
                                                 isub, i1, i2, j, k, l, m,  &
                                                 nbd, nchan, nfb, noffdumps,&
                                                 nondumps, nphases,         &
                                                 npix, nrecords,            &
                                                 nref,nsub,                 &
                                                 noverlap, nusedchan,       &
                                                 nwin, nmask
   real*8                                     :: bval
   real*8                                     :: fmin,fmax,f1,f2,f3
   real*8                                     :: ref1, ref2, tp1, tp2
   real*8, dimension(2,nwin)                  :: window
   integer, dimension(2,nmask)                :: mask
   integer, dimension(:,:), allocatable       :: j1, j2
   integer, dimension(:), allocatable         :: refsubscan1,   &
                                                 refsubscan2, histo
   integer, dimension(:,:,:,:), allocatable   :: signarray
   real*4                                     :: rmed
   real*4, dimension(:,:,:), allocatable      :: rdata
   real*4, dimension(:,:,:,:), allocatable    :: otfdata
   real*8                                     :: tref1, tref2, v
   real*8, dimension(nrecords)                :: tp
   real*8, dimension(:,:), allocatable, save  :: w1, w2
   character(len=3)                           :: tmpsideband
   character(len=5)                           :: wmode, tmpfrontend
   character(len=32)                          :: tmpbackend
   character(len=20)                          :: tmpscantype,   &
                                                 tmpswitchmode
   character(len=65)                          :: string
   logical                                    :: error, exist
   logical, dimension(:), allocatable         :: doneflag,linemask,   &
                                                 overlapflag
   logical, dimension(nrecords)               :: onmask, offmask,   &
                                                 subscanmask
   type(base), dimension(:), allocatable      :: basedata
   type(lg)                                   :: basepar
!
!  The following patch is for the automatic despiking of beam switched
!  EMIR pointings.
!
   integer              :: idc, idccum
   real, parameter      :: deltaCrit = 0.002
   real*8               :: dmd, dmn, dmx
   real*8, dimension(4) :: delta
!
   character(len=8) :: date
   character(len=10) :: time
   character(len=5) :: zone 
   integer, dimension(8) :: values
!
   nfb = scan%header%nfebe
   !
   if (.not.reduce(ifb)%calibration_done(1).and.   &
     .not.reduce(ifb)%calibration_done(3)) then
      bval = blankingraw
   else
      bval = blankingred
   endif
   !
   npix = febe(ifb)%header%febefeed
   !
   tmpscantype = scan%header%scantype
   call sic_upper(tmpscantype)
   tmpswitchmode = febe(ifb)%header%swtchmod
   call sic_upper(tmpswitchmode)
   tmpbackend = febe(ifb)%header%febe
   call sic_upper(tmpbackend)
   !
   nphases = febe(ifb)%header%nphases
   !
   if ((index(tmpbackend,'CONT')  .ne.0 .or.  &
       index(tmpbackend,'NBC')   .ne.0 .or.  &
       index(tmpbackend,'NBC')   .ne.0) .and. &      
       index(tmpscantype,'ONOFF') .ne. 0) then
      allocate(basedata(nrecords*nphases),stat=ier)
      do k = 1, febe(ifb)%header%febeband
         do ipix = 1, npix
            i1 = 0
            basedata%x = array(ifb)%data(k)%mjd   &
              -array(ifb)%data(k)%mjd(1)
            do irec = 1, nrecords
               do iphas = 1, nphases
                  i1 = i1+1
                  basedata(i1)%y   &
                    = array(ifb)%data(k)%data(ipix,1,irec,iphas)
               enddo
            enddo
            basepar%n = nrecords*nphases
            call linregress(basedata,basepar,bval)
            i1 = 0
            do irec = 1, nrecords
               do iphas = 1, nphases
                  i1 = i1+1
                  if (array(ifb)%data(k)%data(ipix,1,irec,iphas)   &
                    .ne.bval)   &
                    array(ifb)%data(k)%data(ipix,1,irec,iphas)   &
                    = array(ifb)%data(k)%data(ipix,1,irec,iphas)   &
                    -basepar%a-basepar%b*basedata(i1)%x
               enddo
            enddo
         enddo
      enddo
      deallocate(basedata,stat=ier)
   else if ((index(tmpbackend,'CONT').ne.0 .or.   &
            index(tmpbackend,'NBC').ne.0   .or.   &
            index(tmpbackend,'BBC').ne.0)  .and.  &
            index(tmpscantype,'TIP').ne.0) then
      reduce(ifb)%calibration_done(3) = .false.
      return
   endif
   !
   nchan = array(ifb)%header(1)%channels
   nbd   = febe(ifb)%header%febeband
   allocate(rdata(nbd,npix,nchan),   &
            otfdata(nbd,npix,nchan,nrecords),stat=ier)
   !
   !
   !
   if ((index(tmpscantype,'FLYMAP').ne.0.or.   &
     index(tmpscantype,'DIY').ne.0).and.nphases.eq.1) then
      !
      nsub = maxval(data(ifb)%data%subscan)
      if (allocated(refsubscan1)) deallocate(refsubscan1,stat=ier)
      if (allocated(refsubscan2)) deallocate(refsubscan2,stat=ier)
      allocate(refsubscan1(nsub),refsubscan2(nsub),stat=ier)
      refsubscan1(1:nsub) = 0
      refsubscan2(1:nsub) = 0
      !
      !
      !
      nref = 0
      do i = 1, nsub
         subscanmask = (data(ifb)%data%subscan.eq.i)
         if (timingcheck) then
            if (count((   &
              index(array(ifb)%data(1)%iswitch(1:nrecords,1),   &
              'OFF').ne.0   &
              .or.   &
              index(array(ifb)%data(1)%iswitch(1:nrecords,1),   &
              'LOAD').ne.0   &
              ).and.subscanmask).ne.0) then
               nref = nref+1
               do j = i, nsub
                  refsubscan1(j) = i
               enddo
            endif
         else
            if (count(array(ifb)%data(1)%iswitch(1:nrecords,1)  &
                .eq.'OFF'.and.subscanmask).gt.                  &
                count(array(ifb)%data(1)%iswitch(1:nrecords,1)  &
                .eq.'ON'.and.subscanmask).or.                   &
                count(array(ifb)%data(1)%iswitch(1:nrecords,1)  &
                .eq.'LOAD'.and.subscanmask).gt.                 &
                count(array(ifb)%data(1)%iswitch(1:nrecords,1)  &
                .eq.'SKY'.and.subscanmask)) then
               nref = nref+1
               do j = i, nsub
                  refsubscan1(j) = i
               enddo
            endif
         endif
      enddo
      !
      if (nref.ne.0) then
         do i = nsub,1,-1
            subscanmask = (data(ifb)%data%subscan.eq.i)
            if (timingcheck) then
               if (count((   &
                 index(array(ifb)%data(1)%iswitch(1:nrecords,1),   &
                 'OFF').ne.0   &
                 .or.   &
                 index(array(ifb)%data(1)%iswitch(1:nrecords,1),   &
                 'LOAD').ne.0   &
                 ).and.subscanmask).ne.0) then
                  do j = 1, i
                     refsubscan2(j) = i
                  enddo
               endif
            else
               if (count(array(ifb)%data(1)%iswitch(1:nrecords,1)  &
                   .eq.'OFF'.and.subscanmask).gt.                  &
                   count(array(ifb)%data(1)%iswitch(1:nrecords,1)  &
                   .eq.'ON'.and.subscanmask).or.                   &
                   count(array(ifb)%data(1)%iswitch(1:nrecords,1)  &
                   .eq.'LOAD'.and.subscanmask).gt.                 &
                   count(array(ifb)%data(1)%iswitch(1:nrecords,1)  &
                   .eq.'SKY'.and.subscanmask)) then
                     do j = 1, i
                        refsubscan2(j) = i
                     enddo
               endif
            endif
         enddo
      endif
      !
      if (nmask.ne.0) then
         offmask(1:nrecords) = .true.
         onmask(1:nrecords) = .false.
         array(ifb)%data(1)%iswitch(1:nrecords,1) = "OFF"
         do i = 1, nmask
            i1 = mask(1,i)
            i2 = mask(2,i)
            onmask(i1:i2) = .true.
            offmask(i1:i2) = .false.
            array(ifb)%data(1)%iswitch(i1:i2,1) = "ON"
         enddo
         if (allocated(j1)) deallocate(j1,stat=ier)
         if (allocated(j2)) deallocate(j2,stat=ier)
         allocate(j1(2,nmask),j2(2,nmask),stat=ier)
         j1(1:2,1:nmask) = 0
         j2(1:2,1:nmask) = 0
         !
         if (mask(1,1).gt.1) then
            j1(1,1) = 1
            j1(2,1) = mask(1,1)-1
         endif
         if (mask(2,nmask).lt.nrecords) then
            j2(2,nmask) = nrecords
            j2(1,nmask) = mask(2,nmask)+1
         endif
         do i = 1, nmask
            if (i.gt.1) j1(1:2,i) = j2(1:2,i-1)
            if (i.lt.nmask) then
               j2(1,i) = mask(2,i)+1
               j2(2,i) = mask(1,i+1)-1
            endif
         enddo
      !
      !
      else
      !
      !
         if (allocated(j1)) deallocate(j1,stat=ier)
         if (allocated(j2)) deallocate(j2,stat=ier)
         allocate(j1(2,nsub),j2(2,nsub),stat=ier)
         j1(1:2,1:nsub) = 0
         j2(1:2,1:nsub) = 0
         do i = 1, nsub
            j1(1,i) = minloc(data(ifb)%data%integnum,1,   &
              data(ifb)%data%subscan.eq.   &
              refsubscan1(i))
            j1(2,i) = maxloc(data(ifb)%data%integnum,1,   &
              data(ifb)%data%subscan.eq.   &
              refsubscan1(i))
            j2(1,i) = minloc(data(ifb)%data%integnum,1,   &
              data(ifb)%data%subscan.eq.   &
              refsubscan2(i))
            j2(2,i) = maxloc(data(ifb)%data%integnum,1,   &
              data(ifb)%data%subscan.eq.   &
              refsubscan2(i))
         enddo
         onmask(1:nrecords)   &
           = index(array(ifb)%data(1)%iswitch(1:nrecords,1),'SKY').ne.0   &
           .or.   &
           index(array(ifb)%data(1)%iswitch(1:nrecords,1),'ON').ne.0
         offmask(1:nrecords)   &
           = index(array(ifb)%data(1)%iswitch(1:nrecords,1),'OFF').ne.0   &
           .or.   &
           index(array(ifb)%data(1)%iswitch(1:nrecords,1),'LOAD').ne.0
      endif
      !
      nbd = febe(ifb)%header%febeband
      nchan = array(ifb)%header(1)%channels
      otfdata(1:nbd,1:npix,1:nchan,1:nrecords) = blankingred
   !
   !
   !
   endif
   !
   !
   !call DATE_AND_TIME(date, time, zone, values)
   !print*, "line 2685: ", (values(j),j=1,8)
   !
   baseband_loop: do k = 1, febe(ifb)%header%febeband
      nchan = array(ifb)%header(k)%channels
      rdata(k,1:npix,1:nchan) = 0
      !
      ! correct here for phases accidentally lost in the imbFits file
      !
      if (nphases.gt.1.and.k.eq.1) then
         do j = 1, nrecords
            do i = 1, nphases-1
               if (array(ifb)%data(1)%iswitch(j,i+1).eq.   &
                 array(ifb)%data(1)%iswitch(j,i)) then
                  do l = 1, nbd
                     array(ifb)%data(l)%data(1:npix,1:nchan,j,i:i+1) =0.
                  enddo
                  write(string,'(A40,I3,A17,I5)')   &
                    'W-CALIBRATE: a phase is lacking in febe ',ifb,   &
                    ' , record number ',j
                  call gagout(string)
               endif
            enddo
         enddo
      endif
      !
      ! end of correction
      !
      if ((index(tmpscantype,'FLYMAP').ne.0.or.   &
        index(tmpscantype,'DIY').ne.0).and.nphases.eq.1) then
         !
         if (index(wmode,'TO').eq.0) then
            if (k.eq.1) then
               if (allocated(w1)) deallocate(w1,stat=ier)
               allocate(w1(npix,nrecords),stat=ier)
               if (allocated(w2)) deallocate(w2,stat=ier)
               allocate(w2(npix,nrecords),stat=ier)
               w1(1:npix,1:nrecords) = 0
               w2(1:npix,1:nrecords) = 0
            endif
            tp(1:nrecords) = 0
         endif
         !
         if (index(wmode,'EQ').ne.0.and.k.eq.1) then
            w1(1:npix,1:nrecords) = 0.5
            w2(1:npix,1:nrecords) = 0.5
         else if (index(wmode,'TI').ne.0.and.nmask.eq.0.and.k.eq.1) then
            do isub = 1, nsub
               i1 = j1(1,isub)
               i2 = j1(2,isub)
               call medianinteger(data(ifb)%data%integnum(i1:i2),   &
                 i2-i1+1,rmed)
               tref1 = data(ifb)%data%midtime(int(rmed))
               i1 = j2(1,isub)
               i2 = j2(2,isub)
               call medianinteger(data(ifb)%data%integnum(i1:i2),   &
                 i2-i1+1,rmed)
               tref2 = data(ifb)%data%midtime(int(rmed))
               forall (ipix=1:npix,irec=1:nrecords,   &
                    data(ifb)%data%subscan(irec).eq.isub   &
                    .and.onmask(irec))
                  w1(ipix,irec)   &
                    = (tref2-data(ifb)%data%midtime(irec))   &
                    /(tref2-tref1)
               end forall
               where(w1(1:npix,1:nrecords).ne.0)   &
                 w2(1:npix,1:nrecords) = 1.-w1(1:npix,1:nrecords)
            enddo

         else if (index(wmode,'TI').ne.0.and.nmask.ne.0.and.k.eq.1) then
            do imask = 1, nmask
               i1 = j1(1,imask)
               i2 = j1(2,imask)
               call medianinteger(data(ifb)%data%integnum(i1:i2),   &
                 i2-i1+1,rmed)
               tref1 = data(ifb)%data%midtime(int(rmed))
               i1 = j2(1,imask)
               i2 = j2(2,imask)
               call medianinteger(data(ifb)%data%integnum(i1:i2),   &
                 i2-i1+1,rmed)
               tref2 = data(ifb)%data%midtime(int(rmed))
               forall (ipix=1:npix,irec=mask(1,imask):mask(2,imask))
                  w1(ipix,irec)   &
                    = (tref2-data(ifb)%data%midtime(irec))   &
                    /(tref2-tref1)
               end forall
               where(w1(1:npix,1:nrecords).ne.0)   &
                 w2(1:npix,1:nrecords) = 1.-w1(1:npix,1:nrecords)
            enddo
         endif
         !
         !
         !
         do i = 1, array(ifb)%header(k)%channels
            do ipix = 1, npix
               if ((index(wmode,'AV').ne.0.or.nref.eq.1).and.          &
                    index(wmode,'NO').eq.0) then
                  tp1 = sum(                                           &
                    array(ifb)%data(k)%data(ipix,i,1:nrecords,1),   &
                    offmask)/count(offmask)
                  where(onmask) otfdata(k,ipix,i,1:nrecords)             &
                    = array(ifb)%data(k)%data(ipix,i,1:nrecords,1)  &
                    -tp1
               else if (index(wmode,'NO').ne.0) then
                  otfdata(k,ipix,i,1:nrecords)                           &
                    =  array(ifb)%data(k)%data(ipix,i,1:nrecords,1)
               else if (nmask.eq.0) then
                  do isub = 1, nsub
                     ref1 = 0
                     ref2 = 0
                     i1 = j1(1,isub)
                     i2 = j1(2,isub)
                     if (refsubscan1(isub).ne.0.and.i1.ne.i2)   &
                       ref1 = sum(   &
                       array(ifb)%data(k)%data(ipix,i,i1:i2,1),   &
                       array(ifb)%data(k)%data(ipix,i,i1:i2,1)   &
                       .ne.bval   &
                       )/count(   &
                       array(ifb)%data(k)%data(ipix,i,i1:i2,1)   &
                       .ne.bval)
                     i1 = j2(1,isub)
                     i2 = j2(2,isub)
                     if (refsubscan2(isub).ne.0.and.i1.ne.i2)   &
                       ref2 = sum(   &
                       array(ifb)%data(k)%data(ipix,i,i1:i2,1),   &
                       array(ifb)%data(k)%data(ipix,i,i1:i2,1)   &
                       .ne.bval   &
                       )/count(   &
                       array(ifb)%data(k)%data(ipix,i,i1:i2,1)   &
                       .ne.bval)
                     if (ref1.eq.0) then
                        where(data(ifb)%data%subscan.eq.isub.and.   &
                          array(ifb)%data(k)%data(ipix,i,   &
                          1:nrecords,1).ne.bval.and.onmask)   &
                          otfdata(k,ipix,i,1:nrecords) =   &
                          array(ifb)%data(k)%data(ipix,i,1:nrecords,1)   &
                          -ref2
                     else if (ref2.eq.0) then
                        where(data(ifb)%data%subscan.eq.isub.and.   &
                          array(ifb)%data(k)%data(ipix,i,   &
                          1:nrecords,1).ne.bval.and.onmask)   &
                          otfdata(k,ipix,i,1:nrecords) =   &
                          array(ifb)%data(k)%data(ipix,i,1:nrecords,1)   &
                          -ref1
                     else
                        where(data(ifb)%data%subscan.eq.isub.and.    &
                        array(ifb)%data(k)%data(ipix,i,1:nrecords,1) &
                        .ne.bval.and.onmask)                         &
                         otfdata(k,ipix,i,1:nrecords) =              &
                         array(ifb)%data(k)%data(ipix,i,1:nrecords,1)&
                         -w1(ipix,1:nrecords)*ref1                   &
                         -w2(ipix,1:nrecords)*ref2
                     endif
                  enddo
               else
                  do imask = 1, nmask
                     ref1 = 0
                     ref2 = 0
                     i1 = j1(1,imask)
                     i2 = j1(2,imask)
                     if (i1.ne.i2)   &
                       ref1 = sum(   &
                       array(ifb)%data(k)%data(ipix,i,i1:i2,1),   &
                       array(ifb)%data(k)%data(ipix,i,i1:i2,1)   &
                       .ne.bval   &
                       )/count(   &
                       array(ifb)%data(k)%data(ipix,i,i1:i2,1)   &
                       .ne.bval)
                     i1 = j2(1,imask)
                     i2 = j2(2,imask)
                     if (i1.ne.i2)   &
                       ref2 = sum(   &
                       array(ifb)%data(k)%data(ipix,i,i1:i2,1),   &
                       array(ifb)%data(k)%data(ipix,i,i1:i2,1)   &
                       .ne.bval   &
                       )/count(   &
                       array(ifb)%data(k)%data(ipix,i,i1:i2,1)   &
                       .ne.bval)
                     if (ref1.eq.0) then
                        where(data(ifb)%data%integnum.ge.mask(1,imask).and.  &
                              data(ifb)%data%integnum.le.mask(2,imask).and.   &
                          array(ifb)%data(k)%data(ipix,i,   &
                          1:nrecords,1).ne.bval)         &
                          otfdata(k,ipix,i,1:nrecords) =   &
                          array(ifb)%data(k)%data(ipix,i,1:nrecords,1)   &
                          -ref2
                     else if (ref2.eq.0) then
                        where(data(ifb)%data%integnum.ge.mask(1,imask).and.  &
                              data(ifb)%data%integnum.le.mask(2,imask).and.   &
                          array(ifb)%data(k)%data(ipix,i,   &
                          1:nrecords,1).ne.bval)   &
                          otfdata(k,ipix,i,1:nrecords) =   &
                          array(ifb)%data(k)%data(ipix,i,1:nrecords,1)   &
                          -ref1
                     else
                        where(data(ifb)%data%integnum.ge.mask(1,imask).and.  &
                              data(ifb)%data%integnum.le.mask(2,imask).and.   &
                          array(ifb)%data(k)%data(ipix,i,   &
                          1:nrecords,1).ne.bval)   &
                          otfdata(k,ipix,i,1:nrecords) =   &
                          array(ifb)%data(k)%data(ipix,i,1:nrecords,1)   &
                          -w1(ipix,1:nrecords)*ref1   &
                          -w2(ipix,1:nrecords)*ref2
                     endif
                  enddo
               endif
            enddo
         enddo
      !
!!      else if (index(tmpbackend,'CONT').ne.0 .or.   &
      else if (index(tmpscantype,'POINT').ne.0 .or.   &
               index(tmpscantype,'TIP').ne.0   .or.   &
               index(tmpscantype,'FOCUS').ne.0) then
         !
         !            IF (INDEX(tmpScanType,'POINT').NE.0.AND.
         !     &          INDEX(tmpSwitchMode,'WOB').NE.0) THEN
         !               FORALL (IPIX=1:NPIX,ICHAN=1:NCHAN,IREC=1:NRECORDS,
         !     &                IPHAS=1:NPHASES,0.5*SCAN%HEADER%WOBTHROW
         !     &                +DATA(IFB)%DATA%LONGOFF(IREC)
         !     &                .LT.FEBE(IFB)%DATA%HPBW(IPIX,1)
         !     &               )
         !                     ARRAY(IFB)%DATA(K)%DATA(IPIX,ICHAN,IREC,IPHAS)
         !     &               = BVAL
         !               END FORALL
         !            ENDIF
         !
         if (allocated(signarray)) deallocate(signarray,stat=ier)
         allocate(signarray(npix,nchan,nrecords,nphases),stat=ier)
         signarray = 1
         forall (ipix=1:npix,ichan=1:nchan,irec=1:nrecords,   &
              iphas=1:nphases,   &
              index(array(ifb)%data(k)%iswitch(irec,iphas),'LOAD')   &
              .ne.0.or.   &
              index(array(ifb)%data(k)%iswitch(irec,iphas),'OFF')   &
              .ne.0)
            signarray(ipix,ichan,irec,iphas) = -1
         end forall
         if (febe(ifb)%header%nphases.eq.1) then
            otfdata(k,1:npix,1:nchan,1:nrecords)   &
            = array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,1)
            where(   &
              array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,1)   &
              .eq.bval)   &
              otfdata(k,1:npix,1:nchan,1:nrecords)   &
              = blankingred
         else if (febe(ifb)%header%nphases.eq.2) then
            otfdata(k,1:npix,1:nchan,1:nrecords)   &
              = signarray(1:npix,1:nchan,1:nrecords,1)*   &
              array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,1)   &
              +signarray(1:npix,1:nchan,1:nrecords,2)*   &
              array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,2)
            where(   &
              array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,1)   &
              .eq.bval.or.   &
              array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,2)   &
              .eq.bval)   &
              otfdata(k,1:npix,1:nchan,1:nrecords)   &
              = blankingred
         else if (febe(ifb)%header%nphases.eq.4) then
            otfdata(k,1:npix,1:nchan,1:nrecords)   &
              = signarray(1:npix,1:nchan,1:nrecords,1)*   &
              array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,1)   &
              +signarray(1:npix,1:nchan,1:nrecords,2)*   &
              array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,2)   &
              +signarray(1:npix,1:nchan,1:nrecords,3)*   &
              array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,3)   &
              +signarray(1:npix,1:nchan,1:nrecords,4)*   &
              array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,4)
            where(   &
              array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,1).eq.bval.or. &
              array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,2).eq.bval.or. &
              array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,3).eq.bval.or. &
              array(ifb)%data(k)%data(1:npix,1:nchan,1:nrecords,4).eq.bval)    &
                 otfdata(k,1:npix,1:nchan,1:nrecords) = blankingred
         endif
      else if (index(tmpswitchmode,'WOB').ne.0.or.   &
        index(tmpswitchmode,'BEAM').ne.0) then
         !
         !
         do j = 1, nrecords
            if (index(array(ifb)%data(k)%iswitch(j,1),'ON').ne.0   &
              .or.index(array(ifb)%data(k)%iswitch(j,1),'SKY')   &
              .ne.0) then
               isign = 1
            else if (index(wmode,'NO').eq.0) then
               isign = -1
            else
               isign = 0
            endif
            do i = 1, array(ifb)%header(k)%channels
               rdata(k,1:npix,i)   &
                 = rdata(k,1:npix,i)   &
                 +isign*(   &
                 array(ifb)%data(k)%data(1:npix,i,j,1)   &
                 -array(ifb)%data(k)%data(1:npix,i,j,2)   &
                 )
               where(   &
                 array(ifb)%data(k)%data(1:npix,i,j,1)   &
                 .eq.bval.or.   &
                 array(ifb)%data(k)%data(1:npix,i,j,2)   &
                 .eq.bval   &
                 ) rdata(k,1:npix,i) = blankingred
            enddo
         enddo
         where(rdata(k,1:npix,:).ne.blankingred)   &
            rdata(k,1:npix,:) = rdata(k,1:npix,:)/nrecords
      !
      !
      else if (index(tmpswitchmode,'FREQ').ne.0) then
         !
         if (index(wmode,'NO').ne.0) then
            call gagout('E-CAL: switch mode FREQ only valid together with')
            call gagout('       subtraction FHI-FLO.')
            error = .true.
            return
         endif
         !
         if (index(tmpscantype,'FLYMAP').eq.0.and.   &
           index(tmpscantype,'DIY').eq.0) then
            do j = 1, nrecords
               do i = 1, array(ifb)%header(k)%channels
                  rdata(k,1:npix,i)   &
                    = rdata(k,1:npix,i)   &
                    -array(ifb)%data(k)%data(1:npix,i,j,1)   &
                    +array(ifb)%data(k)%data(1:npix,i,j,2)
                  where(   &
                    array(ifb)%data(k)%data(1:npix,i,j,1)   &
                    .eq.bval.or.   &
                    array(ifb)%data(k)%data(1:npix,i,j,2)   &
                    .eq.bval   &
                    ) rdata(k,1:npix,i) = blankingred
               enddo
            enddo
            where(rdata(k,1:npix,:).ne.blankingred)   &
                 rdata(k,1:npix,:) = rdata(k,1:npix,:)/nrecords
            if (size(gains).eq.2*nfb) then
               do i = 1, array(ifb)%header(k)%channels
                  where (gains(ifb+nfb)%gainarray(k,1:npix,i)   &
                    .ne.blankingred   &
                    .and.rdata(k,1:npix,i).ne.blankingred)   &
                    rdata(k,1:npix,i)=rdata(k,1:npix,i)   &
                    -gains(ifb+nfb)%gainarray(k,1:npix,i)
               enddo
            endif
         else
            do j = 1, nrecords
               do i = 1, array(ifb)%header(k)%channels
                  otfdata(k,1:npix,i,j)   &
                    = -array(ifb)%data(k)%data(1:npix,i,j,1)   &
                    +array(ifb)%data(k)%data(1:npix,i,j,2)
                  where(   &
                    array(ifb)%data(k)%data(1:npix,i,j,1)   &
                    .eq.bval.or.   &
                    array(ifb)%data(k)%data(1:npix,i,j,2)   &
                    .eq.bval   &
                    ) otfdata(k,1:npix,i,j) = blankingred
               enddo
            enddo
         endif
      !
      else if ((index(tmpscantype,'ONOFF').ne.0.or.   &
        index(tmpscantype,'TRACK').ne.0)   &
        .and.   &
        index(tmpswitchmode,'TOT').ne.0) then
         onmask(1:nrecords) = .false.
         offmask(1:nrecords) = .false.
         do j = 1, nrecords
            if (index(array(ifb)%data(k)%iswitch(j,1),'ON').ne.0   &
              .or.index(array(ifb)%data(k)%iswitch(j,1),'SKY')   &
              .ne.0) then
               onmask(j) = .true.
            else
               offmask(j) = .true.
            endif
         enddo
         nondumps = count(onmask)
         noffdumps = count(offmask)
         if (nondumps.eq.0) then
            call gagout('E-CAL: no data from ON position')
            error = .true.
            return
         else if (noffdumps.eq.0.and.index(tmpscantype,'ONOFF').ne.0)     &
           then
            call gagout('W-CAL: no data from OFF position')
         endif
         do i = 1, array(ifb)%header(k)%channels
            do ipix = 1, npix
               if (count(                                              &
                 array(ifb)%data(k)%data(ipix,i,1:nrecords,1)       &
                 .eq.bval).eq.0) then
                  rdata(k,ipix,i) = sum(                                 &
                    array(ifb)%data(k)%data(ipix,i,1:nrecords,1),   &
                    1,onmask)/nondumps
                  if (noffdumps.ne.0.and.index(wmode,'NO').eq.0)       &
                    rdata(k,ipix,i) = rdata(k,ipix,i)-sum(                 &
                    array(ifb)%data(k)%data(ipix,i,1:nrecords,1),   &
                    1,offmask)/noffdumps
               else
                  rdata(k,ipix,i) = bval
               endif
            enddo
         enddo
      else if (index(tmpscantype,'GRID').ne.0) then
         if (index(wmode,'NO').ne.0) then
            call gagout('E-CAL: scantype calibGrid not valid without '//&
                        'off subtraction.')
            error = .true.
            return
         endif
         onmask(1:nrecords) = .false.
         offmask(1:nrecords) = .false.
         do j = 1, nrecords
            if (index(array(ifb)%data(k)%iswitch(j,1),'HOT').ne.0)   &
              then
               onmask(j) = .true.
            else if (index(array(ifb)%data(k)%iswitch(j,1),'GRID')   &
              .ne.0) then
               offmask(j) = .true.
            endif
         enddo
         nondumps = count(onmask)
         noffdumps = count(offmask)
         if (nondumps.eq.0) then
            call gagout('E-CAL: no data from ON position')
            error = .true.
            return
         else if (noffdumps.eq.0) then
            call gagout('W-CAL: no data from OFF position')
         endif
         do i = 1, array(ifb)%header(k)%channels
            do ipix = 1, npix
               if (count(   &
                   array(ifb)%data(k)%data(ipix,i,1:nrecords,1)   &
                   .eq.bval).eq.0) then
                  rdata(k,ipix,i) = sum(   &
                      array(ifb)%data(k)%data(ipix,i,1:nrecords,1),   &
                      1,onmask)/nondumps
                  if (noffdumps.ne.0)   &
                     rdata(k,ipix,i) = rdata(k,ipix,i)-sum(   &
                     array(ifb)%data(k)%data(ipix,i,1:nrecords,1),   &
                     1,offmask)/noffdumps
               endif
            enddo
         enddo
      else
         call gagout('E-CAL: No such switch mode known.')
         error = .true.
         return
      endif
      if (nphases.gt.2) then
         where (otfdata.ne.blankingred) otfdata = otfdata*2/nphases
      endif
   enddo baseband_loop
   !
   !call DATE_AND_TIME(date, time, zone, values)
   !print*, "line 3134: ", (values(j),j=1,8)
   !
   ! The following is an automatic flagging of phase samples with too short an
   ! integration time (leading to spikes) in EMIR beam switch pointing data.
   !
   !
!!   print *,flagBSwSpikes,nphases,tmpSwitchMode,tmpScanType
   if (flagBSwSpikes.and.nphases.eq.4) then
      if (index(tmpSwitchMode,'BEAM').ne.0.and.&
          (index(tmpScanType,'POIN').ne.0.or.  &
           index(tmpScanType,'FOCU').ne.0.))    then
          i1 = 0
          idccum = 0
          do i = 1, nrecords
             delta(1) = array(ifb)%data(1)%integtim(i1+1)
             delta(2) = array(ifb)%data(1)%integtim(i1+2)
             delta(3) = array(ifb)%data(1)%integtim(i1+3)
             delta(4) = array(ifb)%data(1)%integtim(i1+4)
             dmx = maxval(delta)
             dmn = minval(delta)
             i1 = i1+nphases
             do j = 1, 4
                if (delta(j).ne.dmx.and.delta(j).ne.dmn) then
                   dmd = delta(j)
                   exit
                endif
             enddo
             idc = 0
             do j = 1, 4
                if (abs(delta(j)-dmd).gt.deltaCrit) idc = idc+1
             enddo
             if (idc.gt.0) then
                if (i.eq.1) then
                   otfdata(1,1:npix,1,i) = otfdata(1,1:npix,1,2)
                else if (i.eq.nrecords) then
                   otfdata(1,1:npix,1,i) = otfdata(1,1:npix,1,nrecords-1)
                else
                   otfdata(1,1:npix,1,i) = 0.5*(otfdata(1,1:npix,1,i-1)   &
                                                +otfdata(1,1:npix,1,i+1))
                endif
                idccum=idccum+idc
             endif
          enddo          
          print *,'BeamSwitch Spikes found :',idccum
      endif
   endif
   !
   fmin =  1.d32
   fmax = -1.d32
   !
   do k = 1, febe(ifb)%header%febeband
      nchan = array(ifb)%header(k)%usedchan
      f1 = array(ifb)%header(k)%crvl4f_2           &
           +(1+array(ifb)%header(k)%dropchan       &
             -array(ifb)%header(k)%crpx4f_2)       &
            *array(ifb)%header(k)%cd4f_21
      f2 = array(ifb)%header(k)%crvl4f_2           &
           +(nchan+array(ifb)%header(k)%dropchan   &
             -array(ifb)%header(k)%crpx4f_2)       &
            *array(ifb)%header(k)%cd4f_21
      f3 = min(f1,f2)
      fmin = min(fmin,f3)
      f3 = max(f1,f2)
      fmax = max(fmax,f3)
   enddo
   nchan = (fmax-fmin)/abs(array(ifb)%header(1)%cd4f_21)+1
   allocate(histo(data(ifb)%header%channels),stat=ier)
   histo(:) = 0
   !
   data(ifb)%data%rdata   = blankingRed
   data(ifb)%data%otfdata = blankingRed
   do k = 1, febe(ifb)%header%febeband
      nchan = array(ifb)%header(k)%usedchan
      do l = array(ifb)%header(k)%dropchan+1,  &
             array(ifb)%header(k)%dropchan+nchan
         f1 = (l-array(ifb)%header(k)%crpx4f_2)   &
              *array(ifb)%header(k)%cd4f_21       &
              +array(ifb)%header(k)%crvl4f_2 
         i = 1+(f1-fmin)/abs(array(ifb)%header(1)%cd4f_21)
         histo(i) = histo(i)+1
         if (histo(i).eq.1) then
            where (rdata(k,1:npix,l).ne.blankingRed)        &
               data(ifb)%data%rdata(1:npix,i) = rdata(k,1:npix,l)
            do m = 1, nrecords
               where (otfdata(k,1:npix,l,m).ne.blankingRed) &
                  data(ifb)%data%otfdata(1:npix,i,m)   &
                  = otfdata(k,1:npix,l,m)
            enddo
         else
            where (rdata(k,1:npix,l).ne.blankingRed)        &
               data(ifb)%data%rdata(1:npix,i) =             &
               data(ifb)%data%rdata(1:npix,i)+rdata(k,1:npix,l)
            do m = 1, nrecords
               where (otfdata(k,1:npix,l,m).ne.blankingRed) &
                  data(ifb)%data%otfdata(1:npix,i,m)   &
                  = data(ifb)%data%otfdata(1:npix,i,m) &
                    +otfdata(k,1:npix,l,m)
            enddo
         endif
      enddo
   enddo
   !
   !call DATE_AND_TIME(date, time, zone, values)
   !print*, "line 3237: ", (values(j),j=1,8)
   !
   do k = 1, npix
      where(histo(:).ne.0)
         data(ifb)%data%rdata(k,:) = data(ifb)%data%rdata(k,:)/histo(:)
      else where
         data(ifb)%data%rdata(k,:) = blankingRed
      end where
      do l = 1, nrecords
         where (histo(:).ne.0)
            data(ifb)%data%otfdata(k,:,l)=data(ifb)%data%otfdata(k,:,l)/histo(:)
         else where
            data(ifb)%data%otfdata(k,:,l)=blankingRed
         end where
      enddo
   enddo
   !
   nchan = data(ifb)%header%channels
   if (fmin.ne.fmax) then
      data(ifb)%header%crpx4f_2   &
      = 1.0+(array(ifb)%header(1)%restfreq-fmin)   &
           *(nchan-1.)/(fmax-fmin)
   else
      data(ifb)%header%crpx4f_2 = 1
   endif
   !
   !
   !
   !
   data(ifb)%header%crpx4r_2 = data(ifb)%header%crpx4f_2
   data(ifb)%header%crvl4f_2 = array(ifb)%header(1)%restfreq
   data(ifb)%header%crvl4r_2 = array(ifb)%header(1)%vsou4r_2
   tmpsideband = array(ifb)%header(1)%sideband
   call sic_upper(tmpsideband)
   tmpfrontend = febe(ifb)%header%febe
   call sic_upper(tmpfrontend)
   data(ifb)%header%cd4r_21 = -abs(array(ifb)%header(1)%cd4r_21)
   data(ifb)%header%cd4f_21 =  abs(array(ifb)%header(1)%cd4f_21)
   !
   if (allocated(signarray)) deallocate(signarray,stat=ier)
   deallocate(histo,otfdata,rdata,stat=ier)
   !
   if ((index(tmpscantype,'FLYMAP').ne.0.or.   &
     index(tmpscantype,'DIY').ne.0).and.nphases.eq.1   &
     .and.index(wmode,'NO').ne.0) then
      allocate(linemask(nchan),stat=ier)
      linemask(1:nchan) = .false.
      do i = 1, nchan
         v =(i-data(ifb)%header%crpx4r_2)*data(ifb)%header%cd4r_21   &
           +data(ifb)%header%crvl4r_2
         do j = 1, nwin
            if (v.ge.window(1,j).and.v.le.window(2,j))   &
              linemask(i) = .true.
         enddo
      enddo
      if (nmask.eq.0) then
         do isub = 1, nsub
            do ipix = 1, npix
               i1 = j1(1,isub)
               i2 = j1(2,isub)
               tp1 = sum(data(ifb)%data%otfdata(ipix,1:nchan,i1:i2),   &
                 data(ifb)%data%otfdata(ipix,1:nchan,i1:i2)   &
                 .ne.blankingred   &
                 )/count(   &
                 data(ifb)%data%otfdata(ipix,1:nchan,i1:i2)   &
                 .ne.blankingred   &
                 )
               i1 = j2(1,isub)
               i2 = j2(2,isub)
               tp2 = sum(data(ifb)%data%otfdata(ipix,1:nchan,i1:i2),   &
                 data(ifb)%data%otfdata(ipix,1:nchan,i1:i2)   &
                 .ne.blankingred   &
                 )/count(   &
                 data(ifb)%data%otfdata(ipix,1:nchan,i1:i2)   &
                 .ne.blankingred   &
                 )
               forall (i=1:nrecords,onmask(i).and.   &
                    data(ifb)%data%subscan(i).eq.isub)
                  tp(i) = sum(data(ifb)%data%otfdata(ipix,1:nchan,i),   &
                    data(ifb)%data%otfdata(ipix,1:nchan,i)   &
                    .ne.blankingred.and.   &
                    .not.linemask(1:nchan)   &
                    )/count(   &
                    data(ifb)%data%otfdata(ipix,1:nchan,i)   &
                    .ne.blankingred.and.   &
                    .not.linemask(1:nchan)   &
                    )
               end forall
               forall (i=1:nrecords,onmask(i).and.   &
                    data(ifb)%data%subscan(i).eq.isub.and.tp1.ne.tp2)
                  w1(ipix,i) = (tp(i)-tp2)/(tp1-tp2)
               end forall
               where (w1(ipix,1:nrecords).ne.0)   &
                 w2(ipix,1:nrecords) = 1.-w1(ipix,1:nrecords)
            enddo
         enddo
      else
         do imask = 1, nmask
            do ipix = 1, npix
               i1 = j1(1,imask)
               i2 = j1(2,imask)
               tp1 = sum(data(ifb)%data%otfdata(ipix,1:nchan,i1:i2),   &
                 data(ifb)%data%otfdata(ipix,1:nchan,i1:i2)   &
                 .ne.blankingred   &
                 )/count(   &
                 data(ifb)%data%otfdata(ipix,1:nchan,i1:i2)   &
                 .ne.blankingred   &
                 )
               i1 = j2(1,imask)
               i2 = j2(2,imask)
               tp2 = sum(data(ifb)%data%otfdata(ipix,1:nchan,i1:i2),   &
                 data(ifb)%data%otfdata(ipix,1:nchan,i1:i2)   &
                 .ne.blankingred   &
                 )/count(   &
                 data(ifb)%data%otfdata(ipix,1:nchan,i1:i2)   &
                 .ne.blankingred   &
                 )
               forall (i=mask(1,imask):mask(2,imask))
                  tp(i) = sum(data(ifb)%data%otfdata(ipix,1:nchan,i),   &
                    data(ifb)%data%otfdata(ipix,1:nchan,i)   &
                    .ne.blankingred.and.   &
                    .not.linemask(1:nchan)   &
                    )/count(   &
                    data(ifb)%data%otfdata(ipix,1:nchan,i)   &
                    .ne.blankingred.and.   &
                    .not.linemask(1:nchan)   &
                    )
               end forall
               forall (i=mask(1,imask):mask(2,imask),tp1.ne.tp2)
                  w1(ipix,i) = (tp(i)-tp2)/(tp1-tp2)
               end forall
               where (w1(ipix,1:nrecords).ne.0)   &
                 w2(ipix,1:nrecords) = 1.-w1(ipix,1:nrecords)
            enddo
         enddo
      endif
      deallocate(linemask,stat=ier)
   endif
   !
   !
   if ((index(tmpscantype,'FLYMAP').ne.0.or.   &
     index(tmpscantype,'DIY').ne.0).and.nphases.eq.1   &
     .and.index(wmode,'NO').eq.0) then
      !
      do ichan = 1, nchan
         do ipix = 1, npix
            where(.not.onmask)   &
              data(ifb)%data%otfdata(ipix,ichan,1:nrecords)   &
              = blankingred
         enddo
      enddo
   !
   endif
   !
   !
   if (index(wmode,'NO').ne.0) then
      reduce(ifb)%calibration_done(3) = .false.
   else
      reduce(ifb)%calibration_done(3) = .true.
   endif
   !
   !
   !call DATE_AND_TIME(date, time, zone, values)
   !print*, "line 3406: ", (values(j),j=1,8)
   !
   !
   return
end subroutine subtract_offE
!
!
!
subroutine do_tcal(ifb,nfreq,nrecords,fixopacity,opac,   &
     fixpwv,pwv0,error)
!
! Called by subroutine getmira (source code of call is in file mira/lib/get.f90)
! if reading a calibration scan, and by subroutine OVERRIDE (i.e., if Tcal needs
! to be re-evaluated after a manual change of calibration parameters, see
! HELP OVERRIDE or user's manual). Updates MIRA structure GAINS.
!
! Input:
! ifb           integer         number of frontend/backend combination
! nfreq         integer         number of frequency points to be evaluated
!                               simultaneously (so far always nfreq = 1 because
!                               subroutine mira_chopper is called separately for
!                               each frequency band or, if opted for, spectral
!                               channel.
! nrecords      integer         number of backend dumps in this scan
! fixopacity    logical         .true. if Tsys and Tcal are to be evaluated for 
!                               a given opacity (at a given frequency).
! opac          double          Value of that opacity.
! fixpwv        logical         .true. if pwv is kept fixed, and opacities,
!                               Tsys and Tcal are evaluated for that pwv.
!                               Used e.g. for channel-wise calibration with pwv 
!                               obtained from a wide spectral band.
! pwv0          double          Value of that opacity.
!
! Output: 
! error         logical         .false. upon successful completion.
!
   use mira
   use gildas_def
   use chopper_def
   !
   implicit none
   !
   integer           :: ibd, ichan, ichan1, ichan2, ier, ifb, ifb1, ifb2,&
                        iphas, ipix, ipc1, ipc2, irec, j, jfb, k, k1, k2,&
                        nbd, nchan, ncc, nxchan, nphases, nrecords
   real*4            :: imbftsve, restfreq
   real*8            :: airmdry, airmwet, pwv, tauzenith, tcal, trec,    &
                        tsys, opac, pwv0, deltaintfreq
   character(len=32) :: frontend
   character(len=60) :: string
   logical           :: error, fixopacity, fixpwv
   !
   type(chop_mode)   :: search
   type(telescope)   :: tel
   integer           :: nfreq
   type(dsb_value)   :: freqs(nfreq)
   type(chop_meas)   :: loads(nfreq)
   type(mixer)       :: recs(nfreq)
   type(atm_prop)    :: atms(nfreq)
   type(dsb_value)   :: tcals(nfreq)
   type(dsb_value)   :: tsyss(nfreq)
   logical           :: quiet, weatherdata, errors(nfreq)
   !
   logical, dimension(:),   allocatable :: mask
   logical, dimension(:,:), allocatable :: calmask
   !
   external          :: airmdry, airmwet
   !
   if (scan%data%febe(ifb).ne.gains(ifb)%febe) then
      call gagout('E-CAL: calibration unavailable for this Fe/Be '//   &
                  'combination. First read in a valid calibration.')
      error = .true.
      return
   endif
   frontend = febe(ifb)%header%febe
   search%water = .not.fixopacity.and..not.fixpwv
   search%atm   =.true.
   call sic_get_logi('searchTrec',search%trec,error)
   search%tcal  =.true.
   search%tsys  =.true.
   !
   if (ipc.eq.1) then
      ipc1 = 6 
      ipc2 = 12
   else
      ipc1 = 7
      ipc2 = 13
   endif
   !
   tel%alti = scan%header%siteelev/1.d3
   tel%lati = scan%header%sitelat/rad2deg
   tel%elev = mon(ifb)%data%encoder_az_el(1,2)/rad2deg
   tel%hwat = 2.0d0
   tel%tout = mon(ifb)%data%tamb_p_humid(1,1)+273.15
   ! The following assumes hot load temp. = cabin temp.
   ! The cabin temperature is only needed for (1.-Feff)*TCab,
   ! but TCab needs to take into account the spill-over.
   !
   tel%tcab = mon(ifb)%data%thotcold(1,1)*0.8+tel%tout*0.2
   tel%pres = mon(ifb)%data%tamb_p_humid(1,2)
   !
   loads(1)%cold%temp = mon(ifb)%data%thotcold(1,2)
   loads(1)%cold%ceff  = 1.d0
   loads(1)%hot%temp  = mon(ifb)%data%thotcold(1,1)
   loads(1)%hot%ceff  = 1.d0
   !
   nbd = febe(ifb)%header%febeband
   read(pr%header%mbftsver,'(f3.1)') imbftsve 
   if (array(ifb)%header(1)%sbsep.eq.0) then
      if (index(scan%header%telescop,'30m').eq.0.and.   &
        index(scan%header%telescop,'30M').eq.0) then
         call gagout('W-CAL, no sideband separation in data,')
         call gagout('       TCALS(1)%S set to 1.')
         weatherdata = .false.
      else if (index(febe(ifb)%header%febe,'100').eq.2.and.imbftsve.lt.2.0) then
         do ibd = 1, febe(ifb)%header%febeband
            array(ifb)%header(ibd)%sbsep = 2.*1.498*1e9
         enddo
      else if (imbftsve.lt.2.0) then
         do ibd = 1, febe(ifb)%header%febeband
            array(ifb)%header(ibd)%sbsep = 2.*4.002*1e9
         enddo
      endif
   endif
   !
   if (mon(ifb)%data%tamb_p_humid(1,2).eq.0) then
      weatherdata = .false.
   else
      weatherdata = .true.
   endif
   !
   !
   baseband_loop: do ibd = 1, febe(ifb)%header%febeband
      ! to be changed as soon as frequency scale defined
      ! (right now one frequency per band)
      deltaintfreq = array(ifb)%header(ibd)%crvl4f_2   &
                    -array(ifb)%header(ibd)%restfreq
      if (.not.calbychannel) then 
         freqs(1:nfreq)%s   &
           = array(ifb)%header(ibd)%crvl4f_2   &
           *raw(1)%antenna(ifb)%dopplercorr
         if (array(ifb)%header(ibd)%cd4f_21.gt.0) then
            freqs(1:nfreq)%s = (freqs(1:nfreq)%s   &
              +0.5*array(ifb)%header(ibd)%bandwid)*1.d-9
         else
            freqs(1:nfreq)%s = (freqs(1:nfreq)%s   &
              -0.5*array(ifb)%header(ibd)%bandwid)*1.d-9
         endif
         if (index(array(ifb)%header(1)%sideband,'LSB').ne.0) then
            freqs(1:nfreq)%i = freqs(1:nfreq)%s   &
              +(array(ifb)%header(ibd)%sbsep   &
               -2*deltaintfreq)*1.d-9
         else
            freqs(1:nfreq)%i = freqs(1:nfreq)%s   &
              -(array(ifb)%header(ibd)%sbsep   &
               +2*deltaintfreq)*1.d-9
         endif
      endif
      nchan = array(ifb)%header(ibd)%usedchan
      allocate(mask(nchan),stat=ier)
      k1 = array(ifb)%header(ibd)%dropchan+1
      k2 = array(ifb)%header(ibd)%dropchan+nchan
      nphases = febe(ifb)%header%nphases
      pixel_loop: do ipix = 1, febe(ifb)%header%febefeed
         recs%beff = febe(ifb)%data%beameff(ipix,ibd)
         recs%feff = febe(ifb)%data%etafss(ipix,ibd)
         recs%fout = 0.d0
         recs%fcab = 0.d0 !1.D0-RECS%FEFF
         atms(1)%airmass = 0.5*(   &
           airmdry(mon(ifb)%data%encoder_az_el(1,2))   &
           +airmwet(mon(ifb)%data%encoder_az_el(1,2))   &
           )
         if (.not.calbychannel)   &
           recs%sbgr = febe(ifb)%data%gainimag(ipix,ibd)
         k = 0
         channelloop: do ichan = k1, k2, ncalchan
            mask = .false.
            k = k+1
            if (calbychannel) then
               ichan1 = ichan
               ichan2 = min(ichan+ncalchan-1,k2)
            else
               ichan1 = k1 
               ichan2 = k2
            endif
            ncc = ichan2-ichan1+1
            if (gains(ifb)%gainimage(ibd,ipix,ichan).ne.0.d0) then
               recs%sbgr   = gains(ifb)%gainimage(ibd,ipix,ichan)
            else
               recs%sbgr   = febe(ifb)%data%gainimag(ipix,ibd)
            endif
!
            mask(1:ncc) = gains(ifb)%pcold(ibd,ipix,ichan1:ichan2).ne.blankingRaw
            loads(1)%cold%count = sum(                                        & 
                                   gains(ifb)%pcold(ibd,ipix,ichan1:ichan2),  &
                                   mask(1:ncc)                                &
                                  )/count(mask(1:ncc))
!
            mask(1:ncc) = gains(ifb)%phot(ibd,ipix,ichan1:ichan2).ne.blankingRaw
            loads(1)%hot%count = sum(                                         &
                                   gains(ifb)%phot(ibd,ipix,ichan1:ichan2),   &
                                   mask(1:ncc)                                &
                                  )/count(mask(1:ncc))
!
            mask(1:ncc) = gains(ifb)%psky(ibd,ipix,ichan1:ichan2).ne.blankingRaw
            loads(1)%sky_count = sum(                                         &
                                  gains(ifb)%psky(ibd,ipix,ichan1:ichan2),    &
                                  mask(1:ncc)                                 &
                                 )/count(mask(1:ncc))
!
            if (calbychannel) then
               freqs(1:nfreq)%s                                                  &
               = array(ifb)%header(ibd)%crvl4f_2*raw(1)%antenna(ifb)%dopplercorr
               freqs(1:nfreq)%s = (freqs(1:nfreq)%s                              &
                                +(ichan-k1)*array(ifb)%header(ibd)%cd4f_21)*1.d-9
               if (index(array(ifb)%header(1)%sideband,'LSB').ne.0) then
                  freqs(1:nfreq)%i = freqs(1:nfreq)%s                            &
                             +(array(ifb)%header(ibd)%sbsep-2*deltaintfreq)*1.d-9
               else
                  freqs(1:nfreq)%i = freqs(1:nfreq)%s                            &
                             -(array(ifb)%header(ibd)%sbsep+2*deltaintfreq)*1.d-9
               endif
               if (ichan.gt.k1) quiet = .true.
               gains(ifb)%trx(ibd,ipix,k)                                        &
               = (loads(1)%hot%count*mon(ifb)%data%thotcold(1,2)                 &
                  -loads(1)%cold%count*mon(ifb)%data%thotcold(1,1))              &
                 /(loads(1)%cold%count-loads(1)%hot%count)
            else
	       quiet = .false.
            endif
            recs%temp = gains(ifb)%trx(ibd,ipix,k)
            existWeather: if (weatherdata) then
               crossCorr: if (frontend(ipc:ipc).eq.'R'.or.frontend(ipc:ipc).eq.'I')   &
               then
                  do jfb = 1, scan%header%nfebe
                     if (   &
                       (febe(jfb)%header%febe(ipc:ipc).eq.'H'.or.   &
                       febe(jfb)%header%febe(ipc:ipc).eq.'h')   &
                       .and.   &
                       febe(jfb)%header%febe(ipc1:ipc2)   &
                       .eq.frontend(ipc1:ipc2)   &
                       ) then
                        ifb1 = jfb
                        exit
                     endif
                  enddo
                  do jfb = 1, scan%header%nfebe
                     if (   &
                       (febe(jfb)%header%febe(ipc:ipc).eq.'V'.or.   &
                       febe(jfb)%header%febe(ipc:ipc).eq.'v')   &
                       .and.   &
                       febe(jfb)%header%febe(ipc1:ipc2)   &
                       .eq.frontend(ipc1:ipc2)   &
                       ) then
                        ifb2 = jfb
                        exit
                     endif
                  enddo
                  gains(ifb)%trx(ibd,ipix,k)                       &
                  = sqrt(gains(ifb1)%trx(ibd,ipix,k)               &
                         *gains(ifb2)%trx(ibd,ipix,k))
                  tcals(1)%s = sqrt(gains(ifb1)%tcal(ibd,ipix,k)   &
                                    *gains(ifb2)%tcal(ibd,ipix,k))
                  tsyss(1)%s = sqrt(gains(ifb1)%tsys(ibd,ipix,k)   &
                                    *gains(ifb2)%tsys(ibd,ipix,k))
                  recs(1)%temp = 0.5*(gains(ifb1)%trx(ibd,ipix,k)  &
                                      +gains(ifb2)%trx(ibd,ipix,k))
                  atms(1)%taus%tot                                 &
                    = 0.5*(gains(ifb1)%tauzen(ibd,ipix,k)          &
                           +gains(ifb2)%tauzen(ibd,ipix,k))
                  atms(1)%taui%tot   &
                    = 0.5*(gains(ifb1)%tauzenimage(ibd,ipix,k)     &
                           +gains(ifb2)%tauzenimage(ibd,ipix,k)    &
                    )
                  atms(1)%taus%wat                                 &
                    = 0.5*(gains(ifb1)%tauzenwet(ibd,ipix,k)       &
                           +gains(ifb2)%tauzenwet(ibd,ipix,k))
                  atms(1)%taus%oth                                 &
                    = 0.5*(gains(ifb1)%tauzendry(ibd,ipix,k)       &
                    +gains(ifb2)%tauzendry(ibd,ipix,k))
                  atms(1)%taui%tot                                 &
                    = 0.5*(gains(ifb1)%tauzenimage(ibd,ipix,k)     &
                    +gains(ifb2)%tauzenimage(ibd,ipix,k))
                  atms(1)%taui%wat                                 &
                    = 0.5*(gains(ifb1)%tauzenimagewet(ibd,ipix,k)  &
                    +gains(ifb2)%tauzenimagewet(ibd,ipix,k))
                  atms(1)%taui%oth                                 &
                    = 0.5*(gains(ifb1)%tauzenimagedry(ibd,ipix,k)  &
                    +gains(ifb2)%tauzenimagedry(ibd,ipix,k))
                  atms(1)%temp%s                                   &
                    = 0.5*(gains(ifb1)%tatms(ibd,ipix,k)           &
                    +gains(ifb2)%tatms(ibd,ipix,k))
                  atms(1)%temp%i                                   &
                    = 0.5*(gains(ifb1)%tatmi(ibd,ipix,k)           &
                    +gains(ifb2)%tatmi(ibd,ipix,k))
                  atms(1)%h2omm                                    &
                    = 0.5*(gains(ifb1)%h2omm(ibd,ipix,k)           &
                    +gains(ifb2)%h2omm(ibd,ipix,k))
               else
                  !
                  ! The following is for code testing only.
                  !
                  !      print*, loads%hot%count,loads%cold%count, loads%sky_count
                  !      print*, freqs(1)%s,freqs(1)%i
                  !      print*, recs%sbgr,recs%beff,recs%feff,recs%fout,recs%fcab
                  !      print*, atms(1)%airmass
                  !      print*,tel%alti,tel%lati,tel%elev,tel%hwat,tel%tout
                  call mira_chopper(search,   &
                    fixopacity,opac,fixpwv,pwv0,   &
                    tel,nfreq,freqs,loads,recs,atms,   &
                    tcals,tsyss,quiet,errors)
                  if (count(errors).ne.0) tcals(1)%s = 0.
               endif crossCorr
            else
               call gagout('W-CAL: no weather data, Tcal set to 1 K')
               tcals(1)%s = 1.
            endif existWeather
            if (int(tcals(1)%s).eq.0) then
               write(string,'(A40,I0,A11,I0,A1)')   &
                 'W-CAL: Problem with calibration in FEBE ',   &
                 ifb, ', baseband ', ibd, '.'
               call gagout(string)
               gains(ifb)%tcal(ibd,ipix,k) = -1.
               gains(ifb)%tsys(ibd,ipix,k) = -1.
               call gagout('       Tcal set to -1.')
            else
               gains(ifb)%tcal(ibd,ipix,k) = tcals(1)%s
               gains(ifb)%tsys(ibd,ipix,k) = tsyss(1)%s
               if (search%trec)   &
                 gains(ifb)%trx(ibd,ipix,k) = recs(1)%temp
            endif
            gains(ifb)%tauzen(ibd,ipix,k) = atms(1)%taus%tot
            gains(ifb)%tauzendry(ibd,ipix,k) = atms(1)%taus%oth
            gains(ifb)%tauzenwet(ibd,ipix,k) = atms(1)%taus%wat
            gains(ifb)%tauzenimage(ibd,ipix,k) = atms(1)%taui%tot
            gains(ifb)%tauzenimagedry(ibd,ipix,k) = atms(1)%taui%oth
            gains(ifb)%tauzenimagewet(ibd,ipix,k) = atms(1)%taui%wat
            gains(ifb)%tatms(ibd,ipix,k) = atms(1)%temp%s
            gains(ifb)%tatmi(ibd,ipix,k) = atms(1)%temp%i
            gains(ifb)%temis(ibd,ipix,k) = atms(1)%temi%s
            gains(ifb)%temii(ibd,ipix,k) = atms(1)%temi%i
            gains(ifb)%h2omm(ibd,ipix,k) = atms(1)%h2omm

            if (.not.calbychannel.and.ichan.eq.k1) exit
         enddo channelloop
      !
      !
      enddo pixel_loop
      deallocate(mask,stat=ier)
   enddo baseband_loop
   !
   k1 = index(febe(ifb)%header%febe,' ')+1
   k2 = index(febe(ifb)%header%febe,'/')-1
   if (k2.eq.-1) k2 = len_trim(febe(ifb)%header%febe)
   nxchan = size(gains(ifb)%trx,3)
   allocate(calmask(nbd,nxchan),stat=ier)
   do ipix = 1, febe(ifb)%header%febefeed
      calmask = .false.
      calmask = gains(ifb)%trx(1:nbd,ipix,:).ne.-1.
      trec = sum(gains(ifb)%trx(1:nbd,ipix,:),calmask)/count(calmask)
      calmask = .false.
      calmask = gains(ifb)%tsys(1:nbd,ipix,:).ne.-1.
      tsys = sum(gains(ifb)%tsys(1:nbd,ipix,:),calmask)/count(calmask)
      calmask = .false.
      calmask = gains(ifb)%tcal(1:nbd,ipix,:).ne.-1.
      tcal = sum(gains(ifb)%tcal(1:nbd,ipix,:),calmask)/count(calmask)
      tauzenith = sum(gains(ifb)%tauzen(1:nbd,ipix,:),calmask)/count(calmask)
      pwv = sum(gains(ifb)%h2omm(1:nbd,ipix,:),calmask)/count(calmask)
      restfreq = nint(array(ifb)%header(1)%restfreq*1.d-8)/10.
      if (ipix.eq.1) then
         write(*,10) febe(ifb)%header%febe(1:5),   &
           restfreq,   &
           mon(ifb)%data%thotcold(1,1),   &
           mon(ifb)%data%thotcold(1,2),   &
           febe(ifb)%header%febe(k1:k2),   &
           ipix, trec, tsys, tcal, tauzenith, pwv
      else
         write(*,20) restfreq,ipix, trec, tsys, tcal, tauzenith, pwv
      endif
   enddo
   deallocate(calmask)
   !
   !
10 format(t1,a5,t7,f5.1,t13,f8.3,t20,f9.3,t32,a5,t37,i3,t43,f7.2,t51,   &
          f7.2,t59,f7.2,t69,f5.2,t77,f4.1)
20 format(t7,f5.1,t37,i3,t43,f7.2,t51,f7.2,t59,   &
          f7.2,t69,f5.2,t77,f4.1)
   !
   return
end subroutine do_tcal
!
!
!
subroutine do_tcal_fsw(ifb,nfreq,nrecords,fixopacity,opac,   &
     fixpwv,pwv0,error)
!
! As for subroutine do_tcal, but for calibrations taken with frequency 
! switching (in order to calibrate the FLO and FHI phases separately).
! Experimental.
!
   use mira
   use gildas_def
   use chopper_def
   !
   implicit none
   !
   integer           :: ibd, ichan, ichan1, ichan2, ier, ifb,      &
                        ifb1, ifb2, iphas, ipc1, ipc2, ipix, irec, &
                        j, jfb, k, kfb, kfb1, kfb2, k1, k2,        &
                        nbd, nchan, ncc, nfb, nphases, nrecords,   &
                        nxchan
   real*4            :: restfreq
   real*8            :: airmdry, airmwet, pwv, tauzenith, tcal, trec,   &
                        tsys, opac, pwv0, deltaintfreq
   character(len=32) :: frontend
   character(len=60) :: string
   logical           :: error, fixopacity, fixpwv
   !
   type(chop_mode)   :: search
   type(telescope)   :: tel
   integer           :: nfreq
   type(dsb_value)   :: freqs(nfreq)
   type(chop_meas)   :: loads(nfreq)
   type(mixer)       :: recs(nfreq)
   type(atm_prop)    :: atms(nfreq)
   type(dsb_value)   :: tcals(nfreq)
   type(dsb_value)   :: tsyss(nfreq)
   logical           :: quiet, weatherdata, errors(nfreq)
   !
   logical, dimension(:),   allocatable :: mask
   logical, dimension(:,:), allocatable :: calmask
   !
   external          :: airmdry, airmwet
   !
   if (scan%data%febe(ifb).ne.gains(ifb)%febe) then
      call gagout('E-CAL: calibration unavailable for this Fe/Be '//   &
                  'combination. First read in a valid calibration.')
      error = .true.
      return
   endif
   frontend = febe(ifb)%header%febe
   search%water = .not.fixopacity.and..not.fixpwv
   search%atm   =.true.
   call sic_get_logi('searchTrec',search%trec,error)
   search%tcal  =.true.
   search%tsys  =.true.
   !
   tel%alti = scan%header%siteelev/1.d3
   tel%lati = scan%header%sitelat/rad2deg
   tel%elev = mon(ifb)%data%encoder_az_el(1,2)/rad2deg
   tel%hwat = 2.0d0
   tel%tout = mon(ifb)%data%tamb_p_humid(1,1)+273.15
   ! The following assumes hot load temp. = cabin temp.
   ! The cabin temperature is only needed for (1.-Feff)*TCab,
   ! but TCab needs to take into account the spill-over.
   !
   tel%tcab = mon(ifb)%data%thotcold(1,1)*0.8+tel%tout*0.2
   tel%pres = mon(ifb)%data%tamb_p_humid(1,2)
   !
   loads%cold%temp = mon(ifb)%data%thotcold(1,2)
   ! to be replaced by interpolation between load temperature measurements
   loads%cold%ceff  = 1.d0
   loads%hot%temp  = mon(ifb)%data%thotcold(1,1)
   loads%hot%ceff  = 1.d0
   !
   if (array(ifb)%header(ibd)%sbsep.eq.0) then
      if (index(scan%header%telescop,'30m').eq.0.and.   &
          index(scan%header%telescop,'30M').eq.0) then
         call gagout('W-CAL, no sideband separation in data,')
         call gagout('       TCALS(1)%S set to 1.')
         weatherdata = .false.
      else if (index(febe(ifb)%header%febe,'100').eq.2) then
         array(ifb)%header(ibd)%sbsep = 2.*1.498*1e9
      else
         array(ifb)%header(ibd)%sbsep = 2.*4.002*1e9
      endif
   endif
   !
   if (mon(ifb)%data%tamb_p_humid(1,2).eq.0) then
      weatherdata = .false.
   else
      weatherdata = .true.
   endif
   !
   !
   baseband_loop: do ibd = 1, febe(ifb)%header%febeband
      ! to be changed as soon as frequency scale defined
      ! (right now one frequency per band)
      deltaintfreq = array(ifb)%header(ibd)%crvl4f_2   &
                     -array(ifb)%header(ibd)%restfreq
      if (.not.calbychannel) then
         freqs(1:nfreq)%s   &
           = array(ifb)%header(ibd)%crvl4f_2   &
           *raw(1)%antenna(ifb)%dopplercorr
         if (array(ifb)%header(ibd)%cd4f_21.gt.0) then
            freqs(1:nfreq)%s = (freqs(1:nfreq)%s   &
              +0.5*array(ifb)%header(ibd)%bandwid)*1.d-9
         else
            freqs(1:nfreq)%s = (freqs(1:nfreq)%s   &
              -0.5*array(ifb)%header(ibd)%bandwid)*1.d-9
         endif
         if (index(array(ifb)%header(1)%sideband,'LSB').ne.0) then
            freqs(1:nfreq)%i = freqs(1:nfreq)%s   &
              +(array(ifb)%header(ibd)%sbsep   &
              -2*deltaintfreq)*1.d-9
         else
            freqs(1:nfreq)%i = freqs(1:nfreq)%s   &
              -(array(ifb)%header(ibd)%sbsep   &
              +2*deltaintfreq)*1.d-9
         endif
      endif
      nchan = array(ifb)%header(ibd)%usedchan
      allocate(mask(nchan),stat=ier)
      k1 = array(ifb)%header(ibd)%dropchan+1
      k2 = array(ifb)%header(ibd)%dropchan+nchan
      nphases = febe(ifb)%header%nphases
      pixel_loop: do ipix = 1, febe(ifb)%header%febefeed
         phase_loop: do iphas = 1, 2
            kfb = ifb+(iphas-1)*scan%header%nfebe
            recs%beff = febe(ifb)%data%beameff(ipix,ibd)
            recs%feff = febe(ifb)%data%etafss(ipix,ibd)
            recs%fout = 0.d0
            recs%fcab = 0.d0  !1.D0-RECS%FEFF
            atms%airmass = 0.5*(   &
              airmdry(mon(ifb)%data%encoder_az_el(1,2))   &
              +airmwet(mon(ifb)%data%encoder_az_el(1,2))   &
              )
            if (.not.calbychannel)   &
              recs%sbgr = febe(ifb)%data%gainimag(ipix,ibd)
            k = 0
            channelloop: do ichan = k1, k2, ncalchan
               k = k+1
               if (calbychannel) then
                  ichan1 = ichan
                  ichan2 = min(ichan+ncalchan-1,k2)
               else
                  ichan1 = k1 
                  ichan2 = k2
               endif
               ncc = ichan2-ichan1+1
               recs%temp = gains(kfb)%trx(ibd,ipix,k)
               if (gains(kfb)%gainimage(ibd,ipix,ichan).ne.0.d0) then
                  recs%sbgr =gains(kfb)%gainimage(ibd,ipix,ichan)
               else
                  recs%sbgr =febe(kfb)%data%gainimag(ipix,ibd)
               endif
!
               mask(ichan1:ichan2)                                             &
               = gains(kfb)%pcold(ibd,ipix,ichan1:ichan2).ne.blankingRaw
               loads%cold%count = sum(                                         & 
                                      gains(kfb)%pcold(ibd,ipix,ichan1:ichan2),&
                                      mask(ichan1:ichan2)                      &
                                     )/count(mask(ichan1:ichan2))
!
               mask(ichan1:ichan2)                                             &
               = gains(kfb)%phot(ibd,ipix,ichan1:ichan2).ne.blankingRaw
               loads%hot%count = sum(                                          &
                                      gains(kfb)%phot(ibd,ipix,ichan1:ichan2), &
                                      mask(ichan1:ichan2)                      &
                                     )/count(mask(ichan1:ichan2))
!
               mask(ichan1:ichan2)                                             &
               = gains(kfb)%psky(ibd,ipix,ichan1:ichan2).ne.blankingRaw
               loads%sky_count = sum(                                          &
                                     gains(kfb)%psky(ibd,ipix,ichan1:ichan2),  &
                                     mask(ichan1:ichan2)                       &
                                    )/count(mask(ichan1:ichan2))
!
               if (calbychannel) then
                  freqs(1:nfreq)%s   &
                       = array(ifb)%header(ibd)%crvl4f_2   &
                       *raw(1)%antenna(ifb)%dopplercorr
                  freqs(1:nfreq)%s   &
                       = (freqs(1:nfreq)%s   &
                       +(ichan-k1)*array(ifb)%header(ibd)%cd4f_21   &
                       )*1.d-9
                  if (index(array(ifb)%header(1)%sideband,'LSB')   &
                       .ne.0) then
                        freqs(1:nfreq)%i = freqs(1:nfreq)%s   &
                          +(array(ifb)%header(ibd)%sbsep   &
                          -2*deltaintfreq)*1.d-9
                  else
                        freqs(1:nfreq)%i = freqs(1:nfreq)%s   &
                          -(array(ifb)%header(ibd)%sbsep   &
                          +2*deltaintfreq)*1.d-9
                  endif
                  if (ichan.gt.k1) quiet = .true.
               else
                  quiet = .false.
               endif
               existWeather: if (weatherdata) then
                  crossCorr: if (frontend(ipc:ipc).eq.'R'.or.frontend(ipc:ipc).eq.'I')   &
                  then
                     do jfb = 1, scan%header%nfebe
                        if (   &
                          (febe(jfb)%header%febe(ipc:ipc).eq.'H'.or.   &
                          febe(jfb)%header%febe(ipc:ipc).eq.'h')   &
                          .and.   &
                          febe(jfb)%header%febe(ipc1:ipc2)   &
                          .eq.frontend(ipc1:ipc2)   &
                          ) then
                           ifb1 = jfb
                           exit
                        endif
                     enddo
                     do jfb = 1, scan%header%nfebe
                        if (   &
                          (febe(jfb)%header%febe(ipc:ipc).eq.'V'.or.   &
                          febe(jfb)%header%febe(ipc:ipc).eq.'v')   &
                          .and.   &
                          febe(jfb)%header%febe(ipc1:ipc2)   &
                          .eq.frontend(ipc1:ipc2)   &
                          ) then
                           ifb2 = jfb
                           exit
                        endif
                     enddo
                     kfb1 = ifb1+(iphas-1)*scan%header%nfebe
                     kfb2 = ifb2+(iphas-1)*scan%header%nfebe
                     gains(ifb)%trx(ibd,ipix,k)                       &
                     = sqrt(gains(kfb1)%trx(ibd,ipix,k)               &
                            *gains(kfb2)%trx(ibd,ipix,k))
                     tcals(1)%s = sqrt(gains(kfb1)%tcal(ibd,ipix,k)   &
                                       *gains(kfb2)%tcal(ibd,ipix,k))
                     tsyss(1)%s = sqrt(gains(kfb1)%tsys(ibd,ipix,k)   &
                                       *gains(kfb2)%tsys(ibd,ipix,k))
                     recs(1)%temp =0.5*(gains(kfb1)%trx(ibd,ipix,k)   &
                                        +gains(kfb2)%trx(ibd,ipix,k))
                     atms(1)%taus%tot                                 &
                       = 0.5*(gains(kfb1)%tauzen(ibd,ipix,k)          &
                              +gains(kfb2)%tauzen(ibd,ipix,k))
                     atms(1)%taui%tot                                 &
                       = 0.5*(gains(kfb1)%tauzenimage(ibd,ipix,k)     &
                              +gains(kfb2)%tauzenimage(ibd,ipix,k))
                     atms(1)%taus%wat                                 &
                       = 0.5*(gains(kfb1)%tauzenwet(ibd,ipix,k)       &
                              +gains(kfb2)%tauzenwet(ibd,ipix,k))
                     atms(1)%taus%oth                                 &
                       = 0.5*(gains(kfb1)%tauzendry(ibd,ipix,k)       &
                              +gains(kfb2)%tauzendry(ibd,ipix,k))
                     atms(1)%taui%tot                                 &
                       = 0.5*(gains(kfb1)%tauzenimage(ibd,ipix,k)     &
                              +gains(kfb2)%tauzenimage(ibd,ipix,k))
                     atms(1)%taui%wat                                 &
                       = 0.5*(gains(kfb1)%tauzenimagewet(ibd,ipix,k)  &
                              +gains(kfb2)%tauzenimagewet(ibd,ipix,k))
                     atms(1)%taui%oth                                 &
                       = 0.5*(gains(kfb1)%tauzenimagedry(ibd,ipix,k)  &
                              +gains(kfb2)%tauzenimagedry(ibd,ipix,k))
                     atms(1)%temp%s                                   &
                       = 0.5*(gains(kfb1)%tatms(ibd,ipix,k)           &
                              +gains(kfb2)%tatms(ibd,ipix,k))
                     atms(1)%temp%i   &
                       = 0.5*(gains(kfb1)%tatmi(ibd,ipix,k)           &
                              +gains(kfb2)%tatmi(ibd,ipix,k))
                     atms(1)%h2omm                                    &
                       = 0.5*(gains(kfb1)%h2omm(ibd,ipix,k)           &
                              +gains(kfb2)%h2omm(ibd,ipix,k))
                  else
                     call mira_chopper(search,   &
                       fixopacity,opac,fixpwv,pwv0,   &
                       tel,nfreq,freqs,loads,recs,   &
                       atms,tcals,tsyss,quiet,errors)
                     if (count(errors).ne.0) tcals(1)%s = 0.
                  endif crossCorr
               else
                  call gagout(   &
                    'W-CAL: no weather data, Tcal set to 1 K'   &
                    )
                  tcals(1)%s = 1.
               endif existWeather
               if (int(tcals(1)%s).eq.0) then
                  write(string,'(A40,I0,A11,I0,A1)')   &
                    'W-CAL: Problem with calibration in FEBE ',   &
                    ifb, ', baseband ', ibd, '.'
                  call gagout(string)
                  gains(kfb)%tcal(ibd,ipix,k) = -1.
                  gains(kfb)%tsys(ibd,ipix,k) = -1.
                  call gagout('       Tcal set to -1.')
               else
                  gains(kfb)%tcal(ibd,ipix,k) = tcals(1)%s
                  gains(kfb)%tsys(ibd,ipix,k) = tsyss(1)%s
                  if (search%trec) gains(kfb)%trx(ibd,ipix,k)   &
                    = recs(1)%temp
               endif
               gains(kfb)%tauzen(ibd,ipix,k)         = atms(1)%taus%tot
               gains(kfb)%tauzendry(ibd,ipix,k)      = atms(1)%taus%oth
               gains(kfb)%tauzenwet(ibd,ipix,k)      = atms(1)%taus%wat
               gains(kfb)%tauzenimage(ibd,ipix,k)    = atms(1)%taui%tot
               gains(kfb)%tauzenimagedry(ibd,ipix,k) = atms(1)%taui%oth
               gains(kfb)%tauzenimagewet(ibd,ipix,k) = atms(1)%taui%wat
               gains(kfb)%tatms(ibd,ipix,k)          = atms(1)%temp%s
               gains(kfb)%tatmi(ibd,ipix,k)          = atms(1)%temp%i
               gains(kfb)%temis(ibd,ipix,k)          = atms(1)%temi%s
               gains(kfb)%temii(ibd,ipix,k)          = atms(1)%temi%i
               gains(kfb)%h2omm(ibd,ipix,k)          = atms(1)%h2omm
               if (.not.calbychannel.and.ichan.eq.k1) exit
            enddo channelloop
            !
         enddo phase_loop
      enddo pixel_loop
      deallocate(mask,stat=ier)
   enddo baseband_loop
   !
   nbd = febe(ifb)%header%febeband
   k1 = index(febe(ifb)%header%febe,' ')+1
   k2 = index(febe(ifb)%header%febe,'/')-1
   if (k2.eq.-1) k2 = len_trim(febe(ifb)%header%febe)
   nxchan = size(gains(ifb)%trx,3)
   allocate(calmask(nbd,nxchan),stat=ier)
   do ipix = 1, febe(ifb)%header%febefeed
      calmask = .false.
      calmask = gains(ifb)%trx(1:nbd,ipix,:).ne.-1.
      do iphas = 1, nphases
         kfb = ifb+(iphas-1)*scan%header%nfebe
         calmask = .false.
         calmask = gains(kfb)%trx(1:nbd,ipix,:).ne.-1.
         trec = sum(gains(kfb)%trx(1:nbd,ipix,:),calmask)   &
                /count(calmask)
         calmask = .false.
         calmask = gains(kfb)%tsys(1:nbd,ipix,:).ne.-1.
         tsys = sum(gains(kfb)%tsys(1:nbd,ipix,:),calmask)   &
                /count(calmask)
         calmask = .false.
         calmask = gains(kfb)%tcal(1:nbd,ipix,:).ne.-1.
         tcal = sum(gains(kfb)%tcal(1:nbd,ipix,:),calmask)   &
                /count(calmask)
         tauzenith = sum(gains(kfb)%tauzen(1:nbd,ipix,:),calmask) &
                /count(calmask)
         pwv = sum(gains(kfb)%h2omm(1:nbd,ipix,:),calmask)    &
                /count(calmask)
         restfreq = nint(array(ifb)%header(1)%restfreq*1.d-8)/10.
         if (ipix.eq.1.and.iphas.eq.1) then
            write(*,10) febe(ifb)%header%febe(1:5),   &
              restfreq,   &
              mon(ifb)%data%thotcold(1,1),   &
              mon(ifb)%data%thotcold(1,2),   &
              febe(ifb)%header%febe(k1:k2),   &
              ipix, iphas,trec,tsys,tcal,tauzenith,pwv
         else
            write(*,20) restfreq,ipix,iphas,trec,tsys,tcal,tauzenith,   &
              pwv
         endif
      enddo
   enddo
   deallocate(calmask)
   !
   !
10 format(t1,a5,t7,f5.1,t13,f8.3,t20,f9.3,t32,a5,t37,i3,t41,i1,t43,   &
          f7.2,t51,f7.2,t59,f7.2,t69,f5.2,t77,f4.1)
20 format(t7,f5.1,t37,i3,t41,i1,t43,f7.2,t51,f7.2,t59,   &
          f7.2,t69,f5.2,t77,f4.1)
   !
   return
end subroutine do_tcal_fsw
!
!
subroutine do_xpol(ifb,nrecords,error)
!
! Called by subroutine getfits.f90
! For calibration scans with pako option "/grid yes" and
! VESPA in XPOL mode, calculates the differential phase
! between the horizontal and vertical polarization of the
! EMIR band in frontend-backend-unit ifb, and writes the 
! result into gains(ifb)%phasearray.
!
! Input: 
! ifb           integer         number of frontend/backend combination
! nrecords      integer         number of dumps for this scan and backend.
!
! Output:
! error         logical         .false. upon succesful completion
!
   use mira
   implicit none
   integer                                :: ifb, nrecords
   logical                                :: error
   integer                                :: i, ier, j, k, k1, k2,   &
                                             mxchan, nbd,nchan,   &
                                             nphases, npix, nspec
   real                                   :: imbftsve
   real*8                                 :: bval, gn1, gn2, gn3,   &
                                             tau, utest1
   real*8                                 :: ccimag, ccreal
   logical, dimension(:,:,:), allocatable :: onmask, offmask
   type(calibration), dimension(:),   &
                allocatable, target, save :: xpol

   read(pr%header%mbftsver,'(f3.1)') imbftsve 
   if (.not.reduce(ifb)%calibration_done(1).and.   &
     .not.reduce(ifb)%calibration_done(3)) then
      bval = blankingraw
   else
      bval = blankingred
   endif
   nspec = scan%header%nfebe/4
   nbd = febe(ifb)%header%febeband
   mxchan = 0
   do i = 1, nbd
      mxchan = max(mxchan,array(ifb)%header(i)%channels)
   enddo
   npix = febe(ifb)%header%febefeed
   if (ifb.eq.4) then
      if (allocated(xpol)) deallocate(xpol,stat=ier)
      allocate(xpol(4*nspec),stat=ier)
   endif
   allocate(xpol(ifb-3)%phasearray(nbd,npix,mxchan),   &
            xpol(ifb-2)%phasearray(nbd,npix,mxchan),   &
            xpol(ifb-1)%phasearray(nbd,npix,mxchan),   &
            xpol(ifb)%phasearray(nbd,npix,mxchan),stat=ier)
   nphases = febe(ifb)%header%nphases
   do i = 1, nbd
      nchan = array(ifb)%header(i)%channels
      allocate(onmask(nchan,nrecords,nphases),   &
               offmask(nchan,nrecords,nphases),stat=ier)
      do j = 1, npix
         do k = 1, nchan
            onmask(k,:,:) = (   &
              index(array(ifb)%data(i)%iswitch(:,:),'HOT')   &
              .ne.0.or.   &
              index(array(ifb)%data(i)%iswitch(:,:),'hot')   &
              .ne.0   &
              ).and.array(ifb)%data(i)%data(j,k,:,:).ne.bval
            offmask(k,:,:) = (   &
              index(array(ifb)%data(i)%iswitch(:,:),'GRID')   &
              .ne.0.or.   &
              index(array(ifb)%data(i)%iswitch(:,:),'grid')   &
              .ne.0   &
              ).and.array(ifb)%data(i)%data(j,k,:,:).ne.bval
            ccreal = sum(array(ifb-1)%data(i)%data(j,k,:,:),   &
              onmask(k,:,:))/count(onmask(k,:,:))   &
              -sum(array(ifb-1)%data(i)%data(j,k,:,:),   &
              offmask(k,:,:))/count(offmask(k,:,:))
            ccimag = sum(array(ifb)%data(i)%data(j,k,:,:),   &
              onmask(k,:,:))/count(onmask(k,:,:))   &
              -sum(array(ifb)%data(i)%data(j,k,:,:),   &
              offmask(k,:,:))/count(offmask(k,:,:))
            xpol(ifb-1)%phasearray(i,j,k)=sqrt(ccreal**2+ccimag**2)
            XPOL(IFB)%PHASEARRAY(I,J,K)  = ATAN2(ccImag,ccReal)
            ! to be changed for rotated G3 as follows:
            !xpol(ifb)%phasearray(i,j,k)  = atan2(-ccimag,ccreal)
         enddo
         if (array(ifb)%header(i)%dropchan.gt.0) then
            k1 = 1
            k2 = array(ifb)%header(i)%dropchan
            onmask(k1:k2,:,:) = .false.
            offmask(k1:k2,:,:) = .false.
         endif
         if (array(ifb)%header(i)%usedchan.ne.   &
           array(ifb)%header(i)%channels) then
            k1 = array(ifb)%header(i)%dropchan   &
              +array(ifb)%header(i)%usedchan+1
            k2 = nchan
            onmask(k1:k2,:,:) = .false.
            offmask(k1:k2,:,:) = .false.
         endif
         gn1 = sqrt(   &
           sum(array(ifb-3)%data(i)%data(j,:,:,:),onmask)   &
           /count(onmask)   &
           -sum(array(ifb-3)%data(i)%data(j,:,:,:),offmask)   &
           /count(offmask)   &
           )
         gn2 = sqrt(   &
           sum(array(ifb-2)%data(i)%data(j,:,:,:),onmask)   &
           /count(onmask)   &
           -sum(array(ifb-2)%data(i)%data(j,:,:,:),offmask)   &
           /count(offmask)   &
           )
         k1 = array(ifb)%header(i)%dropchan+1
         k2 = array(ifb)%header(i)%dropchan   &
           +array(ifb)%header(i)%usedchan
         gn3 = sum(xpol(ifb-1)%phasearray(i,j,k1:k2))   &
           /array(ifb)%header(i)%usedchan
         tau = pi-atan2(gn2,gn1)
         utest1 = 0.5*(gn1**2-gn2**2)*tan(2.*tau)
         if (imbftsve.ge.2.0) then
            xpol(ifb-3)%phasearray(i,j,:) = -sign(1.d0,utest1/gn3)
         else
            xpol(ifb-3)%phasearray(i,j,:) = sign(1.d0,utest1/gn3)
         endif
         gn3 = gn3*xpol(ifb-3)%phasearray(i,j,1)
         xpol(ifb-2)%phasearray(i,j,:) = utest1/gn3
      enddo
      deallocate(onmask,offmask,stat=ier)
   enddo
   !
   do i = ifb-3, ifb
      gains(i)%phasearray = xpol(i)%phasearray
   enddo
   return
end subroutine do_xpol
!
!
!
subroutine apply_xpol(ifb,nrecords,error)
!
! For polarization observations only (i.e., VESPA in XPOL mode).
! Applies the phase correction measured with a "cal /grid yes"
! measurement to the real and imaginary part of the complex
! cross correlation (such that they become, after calibration,
! Stokes U and V in the Nasmyth reference system). Update
! array(ifb)%data(j)%data with j=1 to number of basebands.
!
! Input:
! ifb           integer         number of frontend/backend combination
!                               to be phase-calibrated
! nrecords      integer         number of dumps in this backend and scan
!
! Output:
! error         logical         .false. upon successful completion
!
   use mira
   implicit none
   integer :: ifb, nrecords
   logical :: error
   integer                               :: i, ier, j, k, l, m,   &
                                            nphases
   real*8, dimension(:,:,:), allocatable :: datatmp
   character (len = 1)                   :: tmpFeBe

   tmpFeBe = febe(ifb)%header%febe
   call sic_upper(tmpFeBe)
   nphases = febe(ifb)%header%nphases
   allocate(datatmp(nrecords,nphases,2),stat=ier)
   do i = 1, febe(ifb)%header%febeband
      do j = 1, febe(ifb)%header%febefeed
         do k = 1, array(ifb)%header(i)%channels
!
! The following if clause is the correction of correlation flux loss
! due to phase noise (for ABCD receivers only).
!
            if (tmpFeBe.ne.'E') then
               datatmp(:,:,1) = array(ifb-1)%data(i)%data(j,k,:,:)   &
                 *gains(ifb-2)%phasearray(i,j,k)
               datatmp(:,:,2) = array(ifb)%data(i)%data(j,k,:,:)   &
                 *gains(ifb-2)%phasearray(i,j,k)
            else
               datatmp(:,:,1) = array(ifb-1)%data(i)%data(j,k,:,:)
               datatmp(:,:,2) = array(ifb)%data(i)%data(j,k,:,:)
            endif
            do l = 1, nrecords
               do m = 1, nphases
                  if (array(ifb)%data(i)%data(j,k,l,m)   &
                    .ne.blankingraw.and.   &
                    array(ifb-1)%data(i)%data(j,k,l,m)   &
                    .ne.blankingraw) then
                     !
                     ! The sign of Stokes U depends on which zero crossing phase has been
                     ! used for the imaginary part, and is determined in subroutine do_xpol.
                     !
                     !
                     ARRAY(IFB-1)%DATA(I)%DATA(J,K,L,M)                  &
                     =(dataTmp(L,M,1)*COS(GAINS(IFB)%PHASEARRAY(I,J,K))  &
                       +dataTmp(L,M,2)*SIN(GAINS(IFB)%PHASEARRAY(I,J,K)) &
                      )*GAINS(IFB-3)%PHASEARRAY(I,J,K)
                     !
                     ! to be changed for rotated G3 as follows:
                     !
                     !array(ifb-1)%data(i)%data(j,k,l,m)   &
                     !  =(datatmp(l,m,1)*cos(gains(ifb)%phasearray(i,j,k))   &
                     !  -datatmp(l,m,2)*sin(gains(ifb)%phasearray(i,j,k))   &
                     !  )*gains(ifb-3)%phasearray(i,j,k)
                     !
                     ! The following expression yields the sign of Stokes V
                     ! according to IAU convention.
                     !
                     ARRAY(IFB)%DATA(I)%DATA(J,K,L,M) &
                     =dataTmp(L,M,1)*SIN(GAINS(IFB)%PHASEARRAY(I,J,K)) &
                      -dataTmp(L,M,2)*COS(GAINS(IFB)%PHASEARRAY(I,J,K))
                     ! 
                     ! to be changed for rotated G3 as follows:
                     !
                     !array(ifb)%data(i)%data(j,k,l,m)   &
                     !  =datatmp(l,m,1)*sin(gains(ifb)%phasearray(i,j,k))   &
                     !  +datatmp(l,m,2)*cos(gains(ifb)%phasearray(i,j,k))
                  else
                     array(ifb)%data(i)%data(j,k,l,m)   &
                       = blankingraw
                     array(ifb-1)%data(i)%data(j,k,l,m)   &
                       = blankingraw
                  endif
               enddo
            enddo
         enddo
      enddo
   enddo
   deallocate(datatmp,stat=ier)
   reduce(ifb-3)%calibration_done(5)   = .true.
   reduce(ifb-2)%calibration_done(5) = .true.
   reduce(ifb-1)%calibration_done(5)   = .true.
   reduce(ifb)%calibration_done(5) = .true.
   return
end subroutine apply_xpol
!
function airmdry(elev)
!
! Calculates the airmass as a function of elevation
! (input variable "elev"), taking into account the
! curvature of the atmosphere (therefore, the atmospheric
! scale height is needed, and the function needs to
! distinguish between the dry and wet atmosphere.
!
!
   use mira
   implicit none
   real*8            :: airmdry, elev
   real*8, parameter :: aearth = 6378.135,   &
                        bearth = 6356.750,   &
                        h0     = 8.4
   real*8            :: am1, am2, gamma, rearth
   gamma = pi/2.+elev/rad2deg
   rearth = aearth*bearth/sqrt(aearth**2-(aearth**2-bearth**2)   &
     *(cos(scan%header%sitelat/rad2deg))**2)   &
     +scan%header%siteelev*1.d-3
   am1 = rearth*cos(gamma)   &
     +sqrt((rearth+h0)**2-rearth**2*(sin(gamma))**2)
   am2 = rearth*cos(gamma)   &
     -sqrt((rearth+h0)**2-rearth**2*(sin(gamma))**2)
   airmdry = max(am1,am2)/h0
   return
end function airmdry
!
function airmwet(elev)
!
! Calculates the airmass as a function of elevation
! (input variable "elev"), taking into account the
! curvature of the atmosphere (therefore, the
! atmospheric scale height is needed, and the function
! needs to distinguish between the dry and wet atmosphere.
!
   use mira
   implicit none
   real*8            :: airmwet, elev
   real*8, parameter :: aearth = 6378.135,   &
                        bearth = 6356.750,   &
                        h0     = 2.0
   real*8            :: am1, am2, gamma, rearth
   gamma = pi/2.+elev/rad2deg
   rearth = aearth*bearth/sqrt(aearth**2-(aearth**2-bearth**2)  &
     *(cos(scan%header%sitelat/rad2deg))**2)   &
     +scan%header%siteelev*1.d-3
   am1 = rearth*cos(gamma)   &
     +sqrt((rearth+h0)**2-rearth**2*(sin(gamma))**2)
   am2 = rearth*cos(gamma)   &
     -sqrt((rearth+h0)**2-rearth**2*(sin(gamma))**2)
   airmwet = max(am1,am2)/h0
   return
end function airmwet
