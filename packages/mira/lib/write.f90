subroutine mira_write(line,error)
!
! Called by subroutine run_mira (interactively by MIRA command WRITE).
! Prepares things for call to subroutine writeClass.
!
! Input: MIRA command line.
! Output: error flag (if .not.error then data of the current scan are
!         written into the output class file.
!
   use mira
!
   implicit none
!
   integer               :: ifb, ipix, irec, isub, nc, ndump
   integer               :: firstIFB, lastIFB, firstPix, lastPix
   integer               :: firstSubscan, lastSubscan, subscan
   character(len = *)    :: line
   character             :: ch*3, scanId*4, tmpScanType*20, tmpFeBe*32
   logical               :: error, init, sic_present, writeError
!
!
   if (.not.associated(scan)) then
      call GAGOUT('E-WRITE: no observation in current index.')
      error = .true.
      return
   endif
!
   tmpScanType = scan%header%scantype

   write(scanId,'(i4.0)') scan%header%scannum
   call sic_upper(tmpScanType)
!
   if (sic_present(1,0)) then
      call sic_ch(line,1,1,ch,nc,.true.,error)
      call sic_upper(ch)
      if (index(ch,'ALL').ne.0) then
         firstIFB = 1
         lastIFB = scan%header%nfebe
      else
         call sic_i4(line,1,1,ifb,.true.,error)
         firstIFB = ifb
         lastIFB = ifb
      endif
      if (lastIFB.gt.scan%header%nfebe) then
        call GAGOUT('E-WRITE: FEBE index exceeds number of FE/BE combinations')
        error = .true.
        return 
      endif
   else 
      firstIFB = 1
      lastIFB = 1
      call GAGOUT('I-WRITE: first found FE/BE combination written.')
   endif
!
   if (sic_present(2,0)) then
      call sic_ch(line,2,1,ch,nc,.true.,error)
      call sic_upper(ch)
      if (index(ch,'ALL').ne.0) then
         firstPix = 1
         lastPix = maxval(febe(1:scan%header%nfebe)%header%febefeed)
      else
         call sic_i4(line,2,1,ipix,.true.,error)
         firstPix = ipix
         lastPix = ipix
      endif
   else 
      firstPix = 1
      lastPix = 1
      tmpFeBe = febe(1)%header%febe
      call sic_upper(tmpFeBe)
      if (index(tmpFeBe,'HERA').ne.0)                                    &
 &    call GAGOUT('I-WRITE: only pixel 1 written.')
   endif
!
   subscan = 0
   if (sic_present(3,0)) then
      if (.not.sic_present(3,1)) then
         call GAGOUT('I-WRITE: no subscan number specified, defaults to 1.')
         subscan = 1
      else
         call sic_i4(line,3,1,subscan,.true.,error)
      endif
      firstSubscan = subscan
      lastSubscan = subscan
   else if (index(tmpScanType,'POIN').ne.0) then
      firstSubscan = 1
      lastSubscan = maxval(data(1)%data%subscan)
   else
      firstSubscan = 0
      lastSubscan = 0
   endif
!
   if (classOption) then
      do ifb = firstIFB, lastIFB
         if (.not.reduce(ifb)%calibration_done(3).and.                     &
 &           index(tmpScanType,'GRID').eq.0) then
            call gagout('I-WRITE: total power data (no off subtracted).')
         endif
      enddo
!      do ifb = firstIFB, lastIFB
!         if (.not.reduce(ifb)%calibration_done(4)) then
!            call gagout('E-WRITE: Basebands of one backend part need to '//&
! &                      ' be concatenated.')
!            error = .true.
!            return
!         endif
!      enddo
      writeError = .false.
      flyCondition: if (index(tmpScanType,'FLY').eq.0.and.                 &
 &                      index(tmpScanType,'DIY').eq.0) then

         irec = 0
         init = .true.
         do ifb = firstIFB, lastIFB
            do isub = firstSubscan, lastSubscan 
               do ipix = firstPix, lastPix
                  call writeClass(ifb,ipix,isub,irec,init,error)
                  if (error) writeError = .true.
               enddo
            enddo
            if (.not.writeError) then
               call gagout('I-WRITE: CLASS spectrum '//         &
 &                         trim(febe(ifb)%header%febe)//        &
 &                         ' (scan '//trim(scanId)//', '//      &
 &                            scan%header%date_obs(1:10)//      &
 &                         ') written.')
            else
               call gagout('W-WRITE: CLASS spectrum '//         &
 &                         trim(febe(ifb)%header%febe)//        &
 &                         ' output error.')
            endif
         enddo

      else

         ndump = size(data(firstIFB)%data%subscan)
         if (subscan.eq.0) then
            do ifb = firstIFB, lastIFB
               do ipix = firstPix, lastPix
                  init = .true.
                  do irec = 1, ndump
                     if (.not.reduce(ifb)%calibration_done(3).or.              &
 &                       index(array(ifb)%data(1)%iswitch(irec,1),'ON').ne.0   &
 &                       .or.                                                  &
 &                       index(array(ifb)%data(1)%iswitch(irec,1),'SKY').ne.0  &
 &                       .or.                                                  &
 &                       index(array(ifb)%data(1)%iswitch(irec,1),'FLO').ne.0  &
 &                       .or.                                                  &
 &                       index(array(ifb)%data(1)%iswitch(irec,1),'FHI').ne.0) &
 &                   then
                        call writeClass(ifb,ipix,subscan,irec,init,error)
                        init = .false.
                        if (error) writeError = .true.
                     endif
                  enddo
               enddo
               if (.not.writeError) then
                  call gagout('I-WRITE: CLASS spectrum '//         &
 &                            trim(febe(ifb)%header%febe)//        &
 &                            ' (scan '//trim(scanId)//', '//      &
 &                            scan%header%date_obs(1:10)//         &
 &                            ') written.')
               else
                  call gagout('W-WRITE: CLASS spectrum '//         &
 &                            trim(febe(ifb)%header%febe)          &
 &                            //' output error.')
               endif
            enddo
         else
            do ifb = firstIFB, lastIFB
               do ipix = firstPix, lastPix
                  init = .true.
                  do irec = 1, ndump
                     if (data(ifb)%data%subscan(irec).eq.subscan              &
 &                       .and.(                                               &
 &                             .not.reduce(ifb)%calibration_done(3).or.       &
 &                             index(array(ifb)%data(1)%iswitch(irec,1),'ON') &
 &                             .ne.0.or.                                      &
 &                             index(array(ifb)%data(1)%iswitch(irec,1),'SKY')&
 &                             .ne.0.or.                                      &
 &                             index(array(ifb)%data(1)%iswitch(irec,1),'FHI')&
 &                             .ne.0.or.                                      &
 &                             index(array(ifb)%data(1)%iswitch(irec,1),'FLO')&
 &                             .ne.0)                                         &
 &                   ) then
                        call writeClass(ifb,ipix,subscan,irec,init,error)
                        init = .false.
                        if (error) writeError = .true.
                     endif
                  enddo
               enddo
               if (.not.writeError) then
                  call gagout('I-WRITE: CLASS spectrum '//         &
 &                            trim(febe(ifb)%header%febe)//        &
 &                            ' (scan '//trim(scanId)//', '//      &
 &                            scan%header%date_obs(1:10)//         &
 &                            ') written.')
               else
                  call gagout('W-WRITE: CLASS spectrum '//         &
 &                            trim(febe(ifb)%header%febe)          &
 &                            //' output error.')
               endif
            enddo
         endif

      endif flyCondition

   else 
      call writeMbfits(error)
   endif
!
   return
end subroutine mira_write
!
!
!
subroutine writeClass(ifb,ipix,subscan,irec,init,error)
!
! Called by subroutine mira_write.
! 
! Input:
! ifb       integer     input   Number of frontend-backend combination to be
!                               written to CLASS output file.
! ipix      integer     input   Number of pixel (of HERA, otherwise ipix = 1)
!                               to be written to CLASS output file.
! init      logical     input   Set to .true. if all CLASS header entries are 
!                               to be determined. For the records of an OTF map
!                               whose CLASS spectra have many identical header
!                               entries.
! error     logical     output  error flag
!
!
   use mira
   use fit
   use class_api
   use class_RT
   use class_setup
   use gildas_def
   use gbl_constant
   use telcal_interfaces
   use pcross_definitions
   use fit_definitions
   use gbl_format

   implicit none

   integer                             :: i, i1, i2, ier, ifb, ipix, irec, j, &
 &                                        mxchan, nbd, nchan, ndata,    &
 &                                        ndump, needed,    &
 &                                        subscan
   real(4)                             :: calCounts, imbftsve
!!   real(4), allocatable                :: tmp(:)
   real(8)                             :: dlon, dlat, gcd
   real(8)                             :: xtmp, ytmp
   character(len=10)                   :: tmpSystemOff
   character(len=12)                   :: date
   character(len=14)                   :: tmpDewrtMod
   character(len=20)                   :: tmpScanType, tmpSwitchMode
   character(len=32)                   :: tmpFeBe
   character(len=3)                    :: beId 
   character(len=5)                    :: feId
   character(len=3), dimension(12)     :: month = (/'JAN','FEB','MAR','APR', &
 &                                                  'MAY','JUN','JUL','AUG', &
 &                                                  'SEP','OCT','NOV','DEC'/)
   logical                             :: error, init, user_function
   logical, dimension(:), allocatable  :: phaseMask, pswMask, subScanMask
   logical, dimension(:,:), allocatable:: calMask 
!
   external user_function
!
   include 'gbl_memory.inc'
!
!!None are used
!!   integer(kind=address_length)        :: gag_pointer, imem, ipotf, iprec
!
!
!
   call iobs(15,r%head%gen%num,error)
!
   r%head%gen%num = 0
   r%head%gen%ver = 0
   ndata = size(data(ifb)%data%mjd) 
!
   read(prim%header%mbftsver,'(f3.1)') imbftsve
!
   allocate(subScanMask(ndata),stat=ier)
   if(ier.ne.0)print*," STAT allocate subScanMask :",ier
   subScanMask = .true.
   if (subscan.ne.0) subScanMask = (data(ifb)%data%subscan.eq.subscan)
!
   initCondition1: if (init) then

      tmpScanType = scan%header%scantype
      call sic_upper(tmpScanType)
      tmpSwitchMode = febe(ifb)%header%swtchmod
      call sic_upper(tmpSwitchMode)
      tmpFeBe = febe(ifb)%header%febe
      call sic_upper(tmpFeBe)
!
! write general section
!
      r%head%presec(class_sec_gen_id) = .true.
      r%head%gen%teles = scan%header%telescop(1:12)
!
      if (index(scan%header%telescop,'IRAM').ne.0) then
!
         feID = 'none'
         beID = 'tbd'
!
         if (index(tmpFeBe,'E').eq.1) then
            i1 = index(tmpFeBe,' ')
            feId = tmpFeBe(1:i1)
         else if (index(tmpFeBe,'HE').eq.0) then
            feId = tmpFeBe(1:4)
         else if (index(tmpFeBe,'HERA1').ne.0) then
            write(feId,'(A3,I1)') '1H0',ipix
         else if (index(tmpFeBe,'HERA2').ne.0) then
            write(feId,'(A3,I1)') '2H0',ipix
         endif
!
         i1 = index(tmpFeBe,'/')
         if (i1.eq.0) then
            j = 1
         else
            i2 = len_trim(tmpFeBe)
            read(tmpFeBe(i1+1:i2),'(i3)') j
         endif      
!
         if (index(tmpFeBe,'4MHZ').ne.0) then
            write(beId,'(A2,I1)') '4M', j 
         else if (index(tmpFeBe,'1MHZ').ne.0) then
            write(beId,'(A2,I1)') '1M', j 
         else if (index(tmpFeBe,'100K').ne.0) then
            write(beId,'(A2,I1)') '1K', j 
         else if (index(tmpFeBe,'VESPA').ne.0) then
            write(beId,'(A1,I2.2)') 'V', j 
         else if (index(tmpFeBe,'FTS').ne.0) then
            write(beId,'(A1,I2.2)') 'F', j 
         else if (index(tmpFeBe,'WILMA').ne.0) then
            write(beId,'(A1,I2.2)') 'W', j 
!! Check the below, all C? Try N B  and C
         else if (index(tmpFeBe,'BBC').ne.0) then
            write(beId,'(A1,I2.2)') 'B', j 
         else if (index(tmpFeBe,'NBC').ne.0) then
            write(beId,'(A1,I2.2)') 'N', j 
         else if (index(tmpFeBe,'CONT').ne.0) then
            write(beId,'(A1,I2.2)') 'C', j 
         endif
!
         if (index(feId,'none').eq.0.and.index(beId,'tbd').eq.0) then 
            if (imbftsve.lt.2.0) then
               r%head%gen%teles = '30M-'//beId//'-'//trim(feId)
            else
               r%head%gen%teles = '30M'//trim(feId)//'-'//beId
            endif
         endif
!
      endif
!
      write(date(7:11),'(5a)') '-'//scan%header%date_obs(1:4)
      read(scan%header%date_obs(6:7),'(i2)') i1 
      write(date(3:6),'(4a)') '-'//month(i1)
      write(date(1:2),'(2a)') scan%header%date_obs(9:10)
      call gag_fromdate(date,r%head%gen%dobs,error)
      call sic_date(date)
      call gag_fromdate(date,r%head%gen%dred,error)
      if (index(scan%header%ctype1,'RA').ne.0.and. &
  &       index(scan%header%ctype2,'DEC').ne.0) then
         r%head%pos%system = type_eq
      else if (index(scan%header%ctype1,'GLON').ne.0.and. &
  &            index(scan%header%ctype2,'GLAT').ne.0) then
         r%head%pos%system = type_ga
      else if (index(scan%header%ctype1,'ALON').ne.0.and. &
  &            index(scan%header%ctype2,'ALAT').ne.0) then
         r%head%pos%system = type_ho
      else
         r%head%pos%system = type_un
      endif
!
      if (index(tmpFeBe,'CONT').eq.0.and.  &
          index(tmpFeBe,'BBC').eq.0 .and. &
          index(tmpFeBe,'NBC').eq.0 .and. &
          index(tmpFeBe,'DIG').eq.0) then
         r%head%gen%kind = 0
      else if (index(tmpScanType,'POINT').ne.0) then
         r%head%gen%kind = kind_cont
      else if (index(tmpScanType,'ONTHEFLYMAP').ne.0) then
         r%head%gen%kind = kind_cont
      else if (index(tmpScanType,'DIP').ne.0) then
         r%head%gen%kind = kind_sky
      else if (index(tmpScanType,'FOCUS').ne.0) then
         r%head%gen%kind = kind_focus
      else if (index(tmpSwitchMode,'TOTAL').eq.0) THEN
         r%head%gen%kind = kind_onoff
      endif
!     print *,'kind ', r%head%gen%kind,tmpScanType
      r%head%gen%qual = 0
      r%head%gen%scan = scan%header%scannum
      if (subscan.eq.0.and.index(tmpScanType,'FLY').eq.0.and.    &
 &                         index(tmpScanType,'DIY').eq.0) then
         r%head%gen%subscan = data(ifb)%data%subscan(1)
! instead of r%head%gen%subscan = 1, changed 20100618 hwi
      else if (subscan.eq.0.and.irec.ne.0) then
!         r%head%gen%subscan = data(ifb)%data%subscan(1)
         r%head%gen%subscan = data(ifb)%data%subscan(irec)
      else
         r%head%gen%subscan = subscan
      endif
      r%head%gen%ut = (array(ifb)%data(1)%mjd(1)                 &
  &                    -int(array(ifb)%data(1)%mjd(1)))          &
  &                   *twopi  
      r%head%gen%st = data(ifb)%data%lst(1)*sec2rad
      r%head%gen%az = data(ifb)%data%azimuth(1)/rad2deg
      r%head%gen%el = data(ifb)%data%elevatio(1)/rad2deg

   else

      if (subscan.eq.0.and.index(tmpScanType,'FLY').eq.0.and.    &
  &                        index(tmpScanType,'DIY').eq.0) then
         r%head%gen%subscan = 1
      else if (subscan.eq.0.and.irec.ne.0) then
         r%head%gen%subscan = data(ifb)%data%subscan(irec)
      else
         r%head%gen%subscan = subscan
      endif
      r%head%gen%ut = (array(ifb)%data(1)%mjd(irec)              &
  &                    -int(array(ifb)%data(1)%mjd(irec)))       &
  &                   *twopi  
      r%head%gen%st = data(ifb)%data%lst(irec)*sec2rad
      r%head%gen%az = data(ifb)%data%azimuth(irec)/rad2deg
      r%head%gen%el = data(ifb)%data%elevatio(irec)/rad2deg

   endif initCondition1
!

   initCondition2: if (init) then

      nbd = febe(ifb)%header%febeband
      if (reduce(ifb)%calibration_done(2)) then
         if (calByChannel) then
            mxchan = size(gains(ifb)%tsys,3)
            allocate (calMask(nbd,mxchan),stat=ier)
            if (ier.ne.0) print *,"allocate calMask ",ier
            calMask = gains(ifb)%tsys(1:nbd,ipix,1:mxchan).ne.-1.
            r%head%gen%tsys = sum(gains(ifb)%tsys(1:nbd,ipix,1:mxchan),calMask)&
 &                             /count(calMask)
            r%head%gen%tau = sum(gains(ifb)%tauzen(1:nbd,ipix,1:mxchan),calMask)&
 &                             /count(calMask)
            deallocate(calMask,stat=ier)
            if (ier.ne.0) print *,"deallocate calMask ",ier
         else
            r%head%gen%tsys = sum(gains(ifb)%tsys(1:nbd,ipix,1))/nbd
            r%head%gen%tau = sum(gains(ifb)%tauzen(1:nbd,ipix,1))/nbd
         endif
      else
         r%head%gen%tau = -1.
         r%head%gen%tsys = -1.
      endif
!
      if (index(tmpFeBe,'CONT').ne.0) then
         r%head%gen%time = data(ifb)%data%integtim(1)
      else if (index(tmpFeBe,'BBC').ne.0) then
         r%head%gen%time = data(ifb)%data%integtim(1)
      else if (index(tmpFeBe,'NBC').ne.0) then
         r%head%gen%time = data(ifb)%data%integtim(1)
      else if (irec.eq.0) then
         r%head%gen%time = sum(data(ifb)%data%integtim,subScanMask) 
      else
         r%head%gen%time = data(ifb)%data%integtim(1)
      endif
!  
      r%head%gen%xunit = 0
   else

      r%head%gen%time = data(ifb)%data%integtim(irec)

   endif initCondition2

   call wgen(set,r,error)
!
! write position section
!

   initCondition3: if (init) then

      r%head%presec(class_sec_pos_id) = .true.
      if (index(tmpScanType,'GRID').ne.0) then
         r%head%pos%sourc = 'CAL_PHASE'
      else
         r%head%pos%sourc = scan%header%object(1:12)
         call sic_upper(r%head%pos%sourc)
      endif
      r%head%pos%equinox = scan%header%equinox
      r%head%pos%lam = scan%header%longobj/rad2deg
      r%head%pos%bet = scan%header%latobj/rad2deg
      if (irec.ne.0) then
         r%head%pos%lamof = data(ifb)%data%longoff(irec)/rad2deg
         r%head%pos%betof =  data(ifb)%data%latoff(irec)/rad2deg
      else if (subscan.eq.0) then
         r%head%pos%lamof = scan%header%longoff/rad2deg
         r%head%pos%betof = scan%header%latoff/rad2deg
      else
         if (index(tmpSwitchMode,'totalPower').ne.0) then
            do i = 1, ndata
               if (data(ifb)%data%subscan(i).eq.subscan) then
                  r%head%pos%lamof = data(ifb)%data%longoff(i)/rad2deg
                  r%head%pos%betof = data(ifb)%data%latoff(i)/rad2deg
                  exit
               endif
            enddo
         else
            r%head%pos%lamof = scan%header%longoff/rad2deg
            r%head%pos%betof = scan%header%latoff/rad2deg
         endif
      endif
      r%head%pos%proj = p_radio ! Radio projection (Sanson-Flamsteed),
                                ! other ones not yet supported.
   else

      r%head%pos%lamof = data(ifb)%data%longoff(irec)/rad2deg
      r%head%pos%betof =  data(ifb)%data%latoff(irec)/rad2deg

   endif initCondition3

   if (index(tmpFeBe,'HERA').ne.0) then
      tmpSystemOff = raw(1)%antenna(ifb)%systemoff(1:5)
      call sic_upper(tmpSystemOff)
      tmpDewrtMod = febe(ifb)%header%dewrtmod
      call sic_upper(tmpDewrtMod)
      if (index(tmpDewrtMod,'SKY').ne.0) then
         tmpDewrtMod = 'PROJECTION'
      else if (index(tmpDewrtMod,'FRAME').ne.0) then
         tmpDewrtMod = 'NASMYTH'
      else if (index(tmpDewrtMod,'HORIZ').ne.0) then
         tmpDewrtMod = 'HORIZONTALTRUE'
      endif
      if (trim(tmpSystemOff).ne.trim(tmpDewrtMod)) then
         tmpSystemOff = raw(1)%antenna(ifb)%systemoff(1:5)
         xtmp = dble(r%head%pos%lamof)
         ytmp = dble(r%head%pos%betof)
         call convertCoord(xtmp,ytmp,tmpDewrtMod,-1,error)
         r%head%pos%lamof = xtmp
         r%head%pos%betof = ytmp
      endif
      r%head%pos%lamof = r%head%pos%lamof+febe(ifb)%data%feedoffx(ipix)/rad2deg
      r%head%pos%betof = r%head%pos%betof+febe(ifb)%data%feedoffy(ipix)/rad2deg
   else
      tmpSystemOff = raw(1)%antenna(ifb)%systemoff(1:10)
!      tmpSystemOff = raw(1)%antenna(ifb)%systemoff(1:5)
! Here need to be at least 10 character long AS 2010.10.18
      call sic_upper(tmpSystemOff)
      if (index(tmpSystemOff,'PROJECTION').eq.0.and.                     &
     &    index(tmpSwitchMode,'WOB').eq.0) then
!!         tmpSystemOff = raw(1)%antenna(ifb)%systemoff(1:5)
         tmpSystemOff='PROJECTION'
         xtmp = dble(r%head%pos%lamof)
         ytmp = dble(r%head%pos%betof)
         call convertCoord(xtmp,ytmp,tmpSystemOff,-1,error)
         r%head%pos%lamof = xtmp
         r%head%pos%betof = ytmp
      endif
   endif

   call wpos(set,r,error)

!
! write spectroscopic section
!
   nchan = data(ifb)%header%channels
   initCondition4: if (init) then

      r%head%presec(class_sec_spe_id) = .true.
      r%head%spe%line = array(ifb)%header(1)%transiti(1:12)
      r%head%spe%nchan = nchan 
      r%head%spe%rchan = data(ifb)%header%crpx4f_2
!      print*,'rchan (orig), ifb ',r%head%spe%rchan, ifb
      r%head%spe%restf = array(ifb)%header(1)%restfreq*1.d-6
!      print *,'restf restfreqO ',r%head%spe%restf,array(ifb)%header(1)%restfreqO*1.d-6
      r%head%spe%fres = abs(array(ifb)%header(1)%cd4f_21*1.d-6)
      r%head%spe%voff = array(ifb)%header(1)%crvl4r_2
      r%head%spe%vres = -abs(array(ifb)%header(1)%cd4r_21)
!      r%head%spe%doppler = raw(1)%antenna(ifb)%obsVelRf/clight
! changed on 2008-10-29, hw
!      r%head%spe%doppler = -raw(1)%antenna(ifb)%obsVelRf/clight
! changed again on 2009-06-17, hw
! valid for all CLASS data from June 17 onwards, written with gag_may09 or
! gag_dev at the 30m telescope
!
      r%head%spe%doppler = -(array(ifb)%header(1)%vsou4r_2                  &
  &                          +raw(1)%antenna(ifb)%obsVelRf)/clight

      if (index(array(ifb)%header(1)%sideband,'LSB').ne.0.or.               &
  &       index(array(ifb)%header(1)%sideband,'lsb').ne.0) then
!          r%head%spe%image = r%head%spe%restf+1.d-6                         &
!  &              /raw(1)%antenna(ifb)%dopplerCorr*array(ifb)%header(1)%sbsep
! Using the radio doppler formula
          r%head%spe%image =  r%head%spe%restf + 1.d-6*array(ifb)%header(1)%sbsep/  &
  &                                             (1 + r%head%spe%doppler) 
      else
!          r%head%spe%image = r%head%spe%restf-1.d-6                         &
!  &              /raw(1)%antenna(ifb)%dopplerCorr*array(ifb)%header(1)%sbsep

          r%head%spe%image =  r%head%spe%restf - 1.d-6*array(ifb)%header(1)%sbsep/  &
   &                                            (1 + r%head%spe%doppler)  
       endif

      r%head%spe%bad = blankingRed
      if (index(array(ifb)%header(1)%spec4r_2,'LSR').ne.0.or.               &
  &       index(array(ifb)%header(1)%spec4r_2,'lsr').ne.0) then
        r%head%spe%vtype = vel_lsr
      else if (index(array(ifb)%header(1)%spec4r_2,'HEL').ne.0.or.          &
  &            index(array(ifb)%header(1)%spec4r_2,'hel').ne.0) then
        r%head%spe%vtype = vel_hel
      else if (index(array(ifb)%header(1)%spec4r_2,'OBS').ne.0.or.          &
  &            index(array(ifb)%header(1)%spec4r_2,'obs').ne.0) then
        r%head%spe%vtype = vel_obs
      else if (index(array(ifb)%header(1)%spec4r_2,'EAR').ne.0.or.          &
  &            index(array(ifb)%header(1)%spec4r_2,'ear').ne.0) then
        r%head%spe%vtype = vel_ear
      else
         r%head%spe%vtype = 0
      endif
      r%head%spe%voff = array(ifb)%header(1)%vsou4r_2
! From J. Pety (11.08.2011) But this correction already has been done! Taken out 15.02.2015 AS
! - not + A.Sievers (5 Feb 2015) 
!      r%head%spe%rchan = r%head%spe%rchan +  &
!           (r%head%spe%restf-array(ifb)%header(1)%restfreqO*1.d-6)/r%head%spe%fres   &
!          *(r%head%spe%doppler)/(1.0 - r%head%spe%doppler**2) 


      r%head%spe%vres = -clight*r%head%spe%fres/r%head%spe%restf
!
! write switch section
!
      r%head%presec(class_sec_swi_id) = .true.
      r%head%swi%nphas = febe(ifb)%header%nphases
      r%head%swi%decal = 0.d0
      if (index(tmpSwitchMode,'FREQ').ne.0) then
          r%head%swi%swmod = mod_freq
          do i = 1, febe(ifb)%header%nphases, 2
             r%head%swi%decal(i) = febe(ifb)%header%frqoff1*1.d-6
             r%head%swi%poids(i) = -1. 
          enddo 
          do i = 2, febe(ifb)%header%nphases, 2
             r%head%swi%decal(i) = febe(ifb)%header%frqoff2*1.d-6
             r%head%swi%poids(i) = 1. 
          enddo
      else if (index(tmpSwitchMode,'WOB').ne.0.or.index(tmpScanType,'ONOFF') &
 &             .ne.0) then
          r%head%swi%swmod = mod_pos
      endif
      ndump = size(array(ifb)%data(1)%mjd)
      allocate(phaseMask(ndump),stat=ier)
      if (ier.ne.0) print *,"allocating phaseMask ",ier
      do i = 1, febe(ifb)%header%nphases
         phaseMask(1:ndump) = .false.
         do j = i, ndump, febe(ifb)%header%nphases
            phaseMask(j) = .true.
         enddo
         r%head%swi%duree(i) = sum(                                    &
 &                array(ifb)%data(1)%integtim,phaseMask)               &
 &                /count(phaseMask)
      enddo
      deallocate(phaseMask,stat=ier)
      if (ier.ne.0) print *,"deallocating phaseMask ",ier
      r%head%swi%ldecal(1:febe(ifb)%header%nphases) = 0. 
      r%head%swi%bdecal(1:febe(ifb)%header%nphases) = 0. 
!

   endif initCondition4

   call wswi(set,r,error)
!
! write calibration section 
!
   initCondition5: if (init) then

      if (count(reduce(ifb)%calibration_done(1:2)).eq.2) then
!.and.         &  reduce(ifb)%calibration_done(4)) then
         r%head%presec(class_sec_cal_id) = .true.
         r%head%cal%beeff = febe(ifb)%data%beameff(ipix,1)
         r%head%cal%foeff = febe(ifb)%data%etafss(ipix,1)
         r%head%cal%gaini = febe(ifb)%data%gainimag(ipix,1)
         r%head%cal%pamb = mon(ifb)%data%tamb_p_humid(1,2)
         r%head%cal%tamb = mon(ifb)%data%tamb_p_humid(1,1)+273.15
         r%head%cal%tchop = mon(ifb)%data%thotcold(1,1) 
         r%head%cal%tcold = mon(ifb)%data%thotcold(1,2)
         if (calByChannel) then
            mxchan = size(gains(ifb)%trx,3)
            allocate (calMask(nbd,mxchan),stat=ier)
            if (ier.ne.0) print*,"Can not allocate calMask  ",ier

            calMask = gains(ifb)%trx(1:nbd,ipix,1:mxchan).ne.-1.
            r%head%cal%trec = sum(gains(ifb)%trx(1:nbd,ipix,1:mxchan),calMask) &
 &                            /count(calMask)
            calMask = gains(ifb)%tcal(1:nbd,ipix,1:mxchan).ne.-1.
            r%head%cal%atfac = sum(gains(ifb)%tcal(1:nbd,ipix,1:mxchan),calMask)&
 &                             /count(calMask)
            r%head%cal%taus  = sum(gains(ifb)%tauzen(1:nbd,ipix,1:mxchan),calMask)&
 &                             /count(calMask)
            r%head%cal%taui  = sum(gains(ifb)%tauzenImage(1:nbd,ipix,1:mxchan),calMask)&
 &                             /count(calMask)
            r%head%cal%h2omm = sum(gains(ifb)%h2omm(1:nbd,ipix,1:mxchan),calMask)&
 &                             /count(calMask)
            r%head%cal%tatms = sum(gains(ifb)%tatms(1:nbd,ipix,1:mxchan),calMask)&
 &                             /count(calMask)
            r%head%cal%tatmi = sum(gains(ifb)%tatmi(1:nbd,ipix,1:mxchan),calMask)&
 &                             /count(calMask)
            deallocate(calMask,stat=ier)
            if (ier.ne.0)print*,"Deallocating calMask ",ier
         else
            r%head%cal%trec  = sum(gains(ifb)%trx(1:nbd,ipix,1))/nbd
            r%head%cal%h2omm = sum(gains(ifb)%h2omm(1:nbd,ipix,1))/nbd
            r%head%cal%atfac = sum(gains(ifb)%tcal(1:nbd,ipix,1))/nbd
            r%head%cal%tatms = sum(gains(ifb)%tatms(1:nbd,ipix,1))/nbd
            r%head%cal%tatmi = sum(gains(ifb)%tatmi(1:nbd,ipix,1))/nbd
            r%head%cal%taus  = sum(gains(ifb)%tauzen(1:nbd,ipix,1))/nbd
            r%head%cal%taui  = sum(gains(ifb)%tauzenImage(1:nbd,ipix,1))/nbd
         endif
         if (reduce(ifb)%calibration_done(5)) then
            r%head%cal%cmode = 10  ! xpol phase calibration done
         else
            r%head%cal%cmode = 1
         endif
         r%head%cal%alti = scan%header%siteelev
         nbd = size(gains(ifb)%psky,1)
         nchan = size(gains(ifb)%psky,3)
         calCounts = sum(gains(ifb)%psky(1:nbd,ipix,1:nchan))                  &
 &                   /size(gains(ifb)%psky(1:nbd,ipix,1:nchan))
         r%head%cal%count(1) = calCounts
         calCounts = sum(gains(ifb)%phot(1:nbd,ipix,1:nchan))                  &
 &                   /size(gains(ifb)%phot(1:nbd,ipix,1:nchan))
         r%head%cal%count(2) = calCounts
         calCounts = sum(gains(ifb)%pcold(1:nbd,ipix,1:nchan))                 &
 &                /size(gains(ifb)%pcold(1:nbd,ipix,1:nchan))
         r%head%cal%count(3) = calCounts
         r%head%cal%lcalof = gains(ifb)%lcalof
         r%head%cal%bcalof = gains(ifb)%bcalof
         r%head%cal%geolong = scan%header%sitelong/rad2deg
         r%head%cal%geolat = scan%header%sitelat/rad2deg

         call wcal(set,r,error)
      else 
         r%head%presec(class_sec_cal_id) = .false.
      endif

   else if (count(reduce(ifb)%calibration_done(1:2)).eq.2) then
!           .and.reduce(ifb)%calibration_done(4)) then

      call wcal(set,r,error)

   endif initCondition5
!
!
!

   initCondition6: if (init) then

      if (subscan.gt.0) then
         ndump = count(subScanMask)
      else
         ndump = ndata
      endif
!
! continuum drift section 
!
      if (index(tmpFeBe,'CONT').ne.0.or. &
          index(tmpFeBe,'BBC').ne.0..or. &
          index(tmpFeBe,'NBC').ne.0..or. &
          index(tmpFeBe,'DIG').ne.0.)  &
      then
!
         r%head%presec(class_sec_dri_id) = .true.
         r%head%presec(class_sec_spe_id) = .false.
         r%head%dri%freq = r%head%spe%restf
         r%head%dri%width = data(ifb)%header%channels                          &
 &                          *array(ifb)%header(1)%cd4f_21*1.d-6
!
         r%head%dri%npoin = ndump
!
         if (maxval(data(ifb)%data%longoff,subScanMask).ne.0.or.               &
 &           minval(data(ifb)%data%longoff,subScanMask).ne.0) then
            i = min(                                                           &
 &                  minloc(data(ifb)%data%longoff,1,subScanMask),              &
 &                  maxloc(data(ifb)%data%longoff,1,subScanMask)               &
 &                 )
         else
            i = min(                                                           &
 &                  minloc(data(ifb)%data%latoff,1,subScanMask),               &
 &                  maxloc(data(ifb)%data%latoff,1,subScanMask)                &
 &                 )
         endif 
!
         r%head%dri%rpoin = 1.0           
         r%head%dri%tref = twopi*(data(ifb)%data%mjd(i)                        &
 &                                -int(data(ifb)%data%mjd(i)))
         if (data(ifb)%data%longoff(i).ne.data(ifb)%data%longoff(i+1).and.     &
 &           data(ifb)%data%latoff(i).eq.0.) then
            r%head%dri%aref = data(ifb)%data%longoff(i)/rad2deg
         else if (data(ifb)%data%latoff(i).ne.data(ifb)%data%latoff(i+1).and.  &
 &                data(ifb)%data%longoff(i).eq.0) then
            r%head%dri%aref = data(ifb)%data%latoff(i)/rad2deg
         else 
!
! Need to compute length of scan from spherical trigonometry
!
            gcd = 2.*asin(                                                     &
 &                        sin(                                                 &
 &                            data(ifb)%data%longoff(i)                        &
 &                            /(2.*cos(data(ifb)%data%elevatio(i)/rad2deg)     &
 &                              *rad2deg)                                      &
 &                           )                                                 &
 &                        *cos(data(ifb)%data%elevatio(i)/rad2deg)             &
 &                       )  
!
! gcd = longitudinal offset, measured on great circle instead of parallel 
! of latitude
!
            r%head%dri%aref = acos(                                            &
 &                                 cos(gcd)                                    &
 &                                 *cos(data(ifb)%data%latoff(i)/rad2deg)      &
 &                                 )
         endif 
         r%head%dri%apos = atan2(                                              &
 &                               data(ifb)%data%longoff(i+1)                   &
 &                               -data(ifb)%data%longoff(i),                   &
 &                               data(ifb)%data%latoff(i+1)                    &
 &                               -data(ifb)%data%latoff(i)                     &
 &                              )
         r%head%dri%tres = (data(ifb)%data%mjd(i+1)                            &
 &                          -data(ifb)%data%mjd(i))*twopi
         r%head%dri%ares = sqrt(                                               &
 &                              (                                              &
 &                               data(ifb)%data%longoff(i+1)                   &
 &                               -data(ifb)%data%longoff(i)                    &
 &                              )**2                                           &
 &                             +(                                              &
 &                               data(ifb)%data%latoff(i+1)                    &
 &                               -data(ifb)%data%latoff(i)                     &
 &                              )**2                                           &
 &                          )/rad2deg
         if (r%head%dri%apos.gt.3.*pi/4..or.r%head%dri%apos.lt.-pi/4.)         &
 &       r%head%dri%ares = -r%head%dri%ares
  
!
! flat trigonometry permitted here, small values
!
         r%head%dri%bad = blankingRed
         r%head%dri%ctype = r%head%pos%system
         r%head%dri%cimag = r%head%spe%image
         r%head%dri%colla = mon(ifb)%header%iaobs_caobs_ieobs(2)/rad2deg
         r%head%dri%colle = mon(ifb)%header%iaobs_caobs_ieobs(3)/rad2deg
!
         call wcont(set,r,error)
!
      else
!
         r%head%presec(class_sec_dri_id) = .false.
         call wspec(set,r,error)
!
      endif

   else if (index(tmpFeBe,'CONT').ne.0.or. &
            index(tmpFeBe,'BBC').ne.0.or. &
            index(tmpFeBe,'NBC').ne.0.or. &
            index(tmpFeBe,'DIG').ne.0) &
   then

         call wcont(set,r,error)

   else

         call wspec(set,r,error)

   endif initCondition6
!
! write beam-switching section
!

   initCondition7: if (init) then

      if (index(tmpSwitchMode,'PSW').ne.0) then
!
         r%head%presec(class_sec_bea_id) = .true.
         i = min(                                                              &
 &               minloc(data(ifb)%data%azimuth,1,subScanMask),                 &
 &               maxloc(data(ifb)%data%azimuth,1,subScanMask)                  &
 &              )
         r%head%bea%cazim = data(ifb)%data%azimuth(i)/rad2deg
         r%head%bea%celev = data(ifb)%data%elevatio(i)/rad2deg
         allocate(pswMask(ndump),stat=ier)
         pswMask = (data(ifb)%data%latoff.ne.0.or.                             &
 &                  data(ifb)%data%longoff.ne.0)
         dlon = sum(data(ifb)%data%longoff,pswMask)/count(pswMask)
         dlat = sum(data(ifb)%data%latoff,pswMask)/count(pswMask) 
         gcd = 2.*asin(                                                        &
 &                     sin(                                                    &
 &                         data(ifb)%data%longoff(i)                           &
 &                         /(2.*cos(scan%header%latobj/rad2deg)*rad2deg)       &
 &                     )*cos(scan%header%latobj/rad2deg)                       &
 &                    )  
         r%head%bea%space = acos(                                              &
 &                               cos(gcd)                                      &
 &                               *cos(data(ifb)%data%latoff(i)/rad2deg)        &
 &                              ) 
         r%head%bea%bpos = atan2(dlon,dlat)
         r%head%bea%btype = type_ho
!
         call wbeam(set,r,error)
!
      else if (index(tmpSwitchMode,'WOB').ne.0) then
         i = min(                                                              &
 &               minloc(data(ifb)%data%azimuth,1,subScanMask),                 &
 &               maxloc(data(ifb)%data%azimuth,1,subScanMask)                  &
 &              )
         r%head%bea%cazim = data(ifb)%data%azimuth(i)
         r%head%bea%celev = data(ifb)%data%elevatio(i)
         r%head%bea%space = scan%header%wobthrow/rad2deg
         if (index(scan%header%wobdir,'LAT').ne.0.or.                          &
 &           index(scan%header%wobdir,'lat').ne.0) then
            r%head%bea%bpos = pi/2.
         else if (index(scan%header%wobdir,'LON').ne.0.or.                     &
 &           index(scan%header%wobdir,'lon').ne.0) then
            r%head%bea%bpos = 0.
         endif
         r%head%bea%btype = r%head%pos%system
!
         call wbeam(set,r,error)
!
      else
!
         r%head%presec(class_sec_bea_id) = .false.
!
      endif

   else if (index(tmpSwitchMode,'WOB').ne.0.or.                                &
 &          index(tmpSwitchMode,'PSW').ne.0) then

      call wbeam(set,r,error)
   
   endif initCondition7

!
! write continuum section (gaussian pointing fit)
!

  initCondition8: if (init) then

     if (index(tmpScanType,'POIN').ne.0.and.reduce(ifb)%pointing_done) then
         r%head%presec(class_sec_poi_id) = .true.
         r%head%poi%nline = 1
         r%head%poi%sigba = -1 ! not yet determined
         r%head%poi%sigra = pcross(subscan)%fun%rms
         r%head%poi%nfit(1:3) = pcross(subscan)%fun%par(1:3)%value
         r%head%poi%nerr(1:3) = pcross(subscan)%fun%par(1:3)%error
         call wgaus(set,r,error)
     else
         r%head%presec(class_sec_poi_id) = .false.
     endif

  else if (index(tmpScanType,'POIN').ne.0.and.reduce(ifb)%pointing_done) then

     call wgaus(set,r,error) 

  endif initCondition8

!
! write data section descriptor
!
  initCondition9: if (init) then

     if (index(tmpScanType,'FLY').ne.0.or.index(tmpScanType,'DIY').ne.0) then
        r%head%presec(class_sec_desc_id) = .true.
        r%head%des%ndump = ndump
        r%head%des%ldpar = 20 
        r%head%des%ldatl = r%head%spe%nchan 
        r%head%des%ldump = r%head%spe%nchan+20 
!
     else
        r%head%des%ndump = 1
        r%head%des%ldump = 0
        r%head%des%ldpar = 0
        r%head%des%ldatl = 0
        r%head%presec(class_sec_desc_id) = .false.
     endif
!
!
!
!
     if (r%head%presec(class_sec_dri_id)) then
        needed = ndump
     else
        needed = data(ifb)%header%channels
     endif
!
     call reallocate_obs(r,needed,error)
!
     if (r%head%presec(class_sec_dri_id)) then
        r%head%presec(class_sec_xcoo_id) = .true.
        j = 0
        do i=1, ndata
           if (subScanMask(i)) then
              j = j+1
              if (raw(subscan)%antenna(ifb)%segmentXEnd.ne.                    &
 &                raw(subscan)%antenna(ifb)%segmentXStart                      &
 &                .and.                                                        &
 &                raw(subscan)%antenna(ifb)%segmentYEnd.ne.                    &
 &                raw(subscan)%antenna(ifb)%segmentYStart                      &
 &            ) then
                 gcd = 2.*asin(                                                &
 &                             sin(                                            &
 &                                 data(ifb)%data%longoff(i)/rad2deg           &
 &                                 /(2.*cos(scan%header%latobj/rad2deg))       &
 &                             )*cos(scan%header%latobj/rad2deg)               &
 &                        )  
                 r%datav(j) = acos(                                            &
 &                                 cos(gcd)                                    &
 &                                 *cos(data(ifb)%data%latoff(i)/rad2deg)      &
 &                                )*rad2deg
              else if (raw(subscan)%antenna(ifb)%segmentXEnd.ne.               &
 &                     raw(subscan)%antenna(ifb)%segmentXStart) then
                 r%datav(j) = data(ifb)%data%longoff(i)/rad2deg
              else if(raw(subscan)%antenna(ifb)%segmentYEnd.ne.                &
 &                    raw(subscan)%antenna(ifb)%segmentYStart) then 
                 r%datav(j) = data(ifb)%data%latoff(i)/rad2deg
              endif
              r%data1(j) = data(ifb)%data%otfdata(ipix,1,i)
!
           endif
        enddo
        call wxcoo(set,r,error)
     else
!
        nchan = r%head%spe%nchan
        if (r%head%presec(class_sec_desc_id)) then
           r%data1(1:nchan) = data(ifb)%data%otfdata(ipix,1:nchan,irec)
        else
           r%data1(1:nchan) = data(ifb)%data%rdata(ipix,1:nchan)
        endif
     endif
!
     call wdata(r,needed,r%data1,error)

  else if (r%head%presec(class_sec_dri_id)) then

     call wxcoo(set,r,error)

  else
     if (r%head%presec(class_sec_desc_id)) then
        r%data1(1:nchan) = data(ifb)%data%otfdata(ipix,1:nchan,irec)
     else
        r%data1(1:nchan) = data(ifb)%data%rdata(ipix,1:nchan)
     endif
     call wdata(r,needed,r%data1,error)

  endif initCondition9

!
!
!
  call class_close(error)
!
end subroutine writeClass
!
!
!
subroutine writeMbfits(error)
!
! Yet to be done. Was thought to convert IMBFITS (IRAM 30m) to MBFITS (MPIfR
! Apex).
! 
!
   use mira
!
   implicit none
 
   character(len=80) :: output 
   logical           :: error
  
   output = 'OUTPUT_FILE:' 
   call GAG_MKDIR(output,error)   
   call GAGOUT('I-WRITE: empty directory created. Not yet ready')
   call GAGOUT('         for mbfits file writing.')
 
   return
 
end subroutine writeMbfits
