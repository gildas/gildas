!     
!     $Id$
!     
! TBD:  error handling
! TBD:  for the lists: use linked lists instead of arrays
!
Module moduleResultsToNCS
  !     
  Use modulePakoXML
  !
  Implicit None
  Save
  Private
  !
  ! *** Subroutines:
  !
  Public   ::  resultsToNCSwriteProlog
  Public   ::  resultsToNCSsetOdpHeader
  Public   ::  resultsToNCSwriteOdpHeader
  Public   ::  resultsToNCSsetMeasurementHeader
  Public   ::  resultsToNCSsetScanId
  Public   ::  resultsToNCSwriteMeasurementHeader
  Public   ::  resultsToNCSsetReceiver
  Public   ::  resultsToNCSwriteReceivers
  Public   ::  resultsToNCSsetBackend
  Public   ::  resultsToNCSwriteBackends
  Public   ::  resultsToNCSsetFocusResults
  Public   ::  resultsToNCSwriteFocusResults
  Public   ::  resultsToNCSsetPointingResults
  Public   ::  resultsToNCSwritePointingResults
  Public   ::  resultsToNCSsetCalibrationResults
  Public   ::  resultsToNCSwriteCalibrationResults
  Public   ::  resultsToNCSwriteMeasurementEnd
  Public   ::  resultsToNCSwriteEnd
  !
  ! *** Derived Types:
  !
  Public   ::  resultT
  !
  ! *** values representing "not set":
  !
  Character(len=8), Parameter  :: cNone = 'none'
  Integer,          Parameter  :: iNone = 0
  Real,             Parameter  :: rNone = 0.0
  Real*8,           Parameter  :: dNone = 0.0D0
  ! TBD: lNone = .False.
  !
  !
  ! *** for ODP Header in XML file:
  !
  Type     ::  odpHeaderT
     Character(len=128) ::  telescope   = cNone
     Character(len=128) ::  observatory = cNone
     Character(len=128) ::  odpSoftware = cNone
     ! ...
  End Type odpHeaderT
  Type(odpHeaderT)      ::  odpHeader, odpHeaderDefault
  !
  ! *** for "measurement Header in XML file:
  !     
  ! first: list of scanID or scan numbers
  !
  Type :: scanIdT       
     Character(len=24)  :: scanId              = cNone
     Integer            :: scanN               = iNone
  End Type scanIdT
  Integer, Parameter    :: nDimScans           = 99
  Integer               :: nScans              = 0
  Type(scanIdT), Dimension(nDimScans) :: scans
  Type(scanIdT)                       :: scanDefault
  !
  Type     ::  measurementHeaderT
     Character(len=128) :: measurement         =  cNone
     Character(len=128) :: sourceName          =  cNone
     Character(len=128) :: timeStamp           =  cNone
     Real               :: azimuth             =  rNone
     Real               :: elevation           =  rNone
     Real               :: refraction          =  rNone
     Real               :: pressureAmbient     =  rNone
     Real               :: temperatureAmbient  =  rNone
     Real               :: humidityAmbient     =  rNone
     Logical            :: lAzimuth            = .False.
     Logical            :: lElevation          = .False.
     Logical            :: lRefraction         = .False.
     Logical            :: lPressureAmbient    = .False.
     Logical            :: lTemperatureAmbient = .False.
     Logical            :: lHumidityAmbient    = .False.
     Character(len=128) :: switchingMode       =  cNone
     Character(len=128) :: observingMode       =  cNone
     Character(len=128) :: method              =  cNone
     Character(len=128) :: focusDirection      =  cNone
     Real               :: focusLength         =  rNone
     Real               :: focusCurrent        =  rNone
     Logical            :: lFocusLength        = .False.
     Logical            :: lFocusCurrent       = .False.
     Real               :: pointingLength      =  rNone
     Real               :: pointingP1          =  rNone
     Real               :: pointingP2          =  rNone
     Real               :: pointingP7          =  rNone
     Logical            :: lPointingLength     = .False.
     Logical            :: lPointingP1         = .False.
     Logical            :: lPointingP2         = .False.
     Logical            :: lPointingP7         = .False.
     ! ...
  End Type measurementHeaderT
  Type(measurementHeaderT) ::  mHead
  Type(measurementHeaderT) ::  mHeadDefault
  !
  ! *** list of receivers 
  !      consistent with paKo --> some extra variables
  ! 
  Type :: receiverFrequency
     Character(len=12)       :: name           = cNone
     Character(len=12)       :: kind           = cNone
     Character(len=12)       :: unit           = cNone
     Character(len=12)       :: doppler        = cNone         
     Real*8                  :: value          = dNone
  End Type receiverFrequency
  !
  Type :: gainRatio
     Character(len=12)       :: name           = cNone
     Character(len=12)       :: kind           = cNone
     Character(len=12)       :: unit           = cNone
     Real*4                  :: value          = dNone
     Real*4                  :: db             = dNone
  End Type gainRatio
  !
  Type :: varReceiverT
     !
     Character(len=13)       :: name           = cNone
     Logical                 :: isConnected    = .False.
     Logical                 :: gunnOn         = .False.
     Type(receiverFrequency) :: frequency
     Type(receiverFrequency) :: frequencyImage
     Type(receiverFrequency) :: offset
     Type(receiverFrequency) :: centerIF       
     !
     Character(len=100)      :: lineName       = cNone         
     !
     Character(len=12)       :: sideBand       = cNone         
     Character(len=12)       :: doppler        = cNone         
     Character(len=12)       :: width          = cNone
     !
     Real*4                  :: effForward     = rNone
     Real*4                  :: effBeam        = rNone
     Character(len=12)       :: scale          = cNone
     Type(gainRatio)         :: gainImage
     Real*4                  :: tempAmbient    = rNone
     Real*4                  :: tempCold       = rNone
     Real*4                  :: xOffsetNasmyth = rNone
     Real*4                  :: yOffsetNasmyth = rNone
     Real*4                  :: lcalof         = rNone
     Real*4                  :: bcalof         = rNone
     Real*4                  :: h2omm          = rNone
     Real*4                  :: trx            = rNone
     Real*4                  :: tsys           = rNone
     Real*4                  :: tcal           = rNone
     Real*4                  :: tatms          = rNone
     Real*4                  :: tatmi          = rNone
     Real*4                  :: tauzen         = rNone
     Real*4                  :: tauzenImage    = rNone
     Real*4                  :: pcold          = rNone
     Real*4                  :: phot           = rNone
     Real*4                  :: psky           = rNone
     !
     Integer                 :: iPixel         = iNone
     !
  End Type varReceiverT
  !
  Integer, Parameter         :: nDimReceivers  = 18
  Integer                    :: nRec           = 0
  Type(varReceiverT), Dimension(nDimReceivers) :: receivers
  Type(varReceiverT)                           :: receiverDefault
  !
  ! *** list of backends
  !      consistent with paKo --> some extra variables
  !
  Type :: varBackendT
     !
     Character(len=12)       :: name           = cNone
     Logical                 :: wasConnected   = .False.
     Logical                 :: isConnected    = .False.
     Integer                 :: nPart          = iNone
     !
     Real*4                  :: resolution     = rNone
     Real*4                  :: bandWidth      = rNone
     Real*4                  :: fShift         = rNone
     !
     Character(len=13)       :: receiverName   = cNone         
     !
     Integer                 :: nChannels      = iNone
     !
  End Type varBackendT
  !
  Integer, Parameter         :: nDimBackends   = 99
  Integer                    :: nBack          = 0
  Type(varBackendT), Dimension(nDimBackends)   :: backends
  Type(varBackendT)                            :: backendDefault
  !
  ! *** list of pointing results
  !      
  ! most results have a value and an error:
  !
  Type :: resultT
     Real*4                  :: value          = rNone
     Real*4                  :: error          = rNone
  End Type resultT
  !
  Type :: focusResultsT
     Character(len=12)       :: backendName    = cNone
     Integer                 :: nPart          = iNone
     Type(resultT)           :: focus
     Type(resultT)           :: offset       
     Real*4                  :: rmsResidual    = rNone
  End Type focusResultsT
  !
  Integer, Parameter         :: nDimResults      = 99
  Integer                    :: nFocusResults = 0
  Type(focusResultsT), Dimension(nDimResults) :: FocusResults
  Type(focusResultsT)                         :: FocusResultDefault
  !
  Type :: pointingResultsT
     Character(len=12)       :: backendName    = cNone
     Integer                 :: nPart          = iNone
     Character(len=12)       :: direction      = cNone
     Type(resultT)           :: correction   
     Type(resultT)           :: peak         
     Type(resultT)           :: integral     
     Type(resultT)           :: offset       
     Type(resultT)           :: width        
     Real*4                  :: rmsResidual    = rNone
  End Type pointingResultsT
  !
!!$  Integer, Parameter         :: nDimResults      = 99
  Integer                    :: nPointingResults = 0
  Type(pointingResultsT), Dimension(nDimResults) :: PointingResults
  Type(pointingResultsT)                         :: PointingResultDefault
  !
Contains
!!!
!!!
  Subroutine resultsToNCSwriteProlog(                             &
       &                                       error,             &
       &                                       errorC             &
       &                            )
    !
    Logical,          Intent(OUT), Optional :: error              ! errorFlag
    Character(len=*), Intent(OUT), Optional :: errorC             ! errorCode
    !
    Logical            :: errorXML
    Character(len=256) :: c1
    !
    If (Present(error))  error  = .False.
    If (Present(errorC)) errorC = ""
    !
    c1 = ".xml file generated by moduleResultsToNCS.f90 (author: Hans Ungerechts)"
    !
    Call pakoXMLwriteStartXML("1.0",                              &
         &                    "VOTABLE", "VOTable.dtd", "1.0",    &
         &                    comment=c1(1:Len_trim(c1)),         & 
         &                    error=errorXML)
    !
    If (Present(error) .And. errorXML) error=errorXML
    Return
    !
  End Subroutine resultsToNCSwriteProlog
!!!
!!!
  Subroutine resultsToNCSsetOdpHeader(                            &
       &                                       telescope        , &
       &                                       observatory      , &
       &                                       odpSoftware      , &
       &                                       default          , &
       &                                       reset            , &
       &                                       error            , &
       &                                       errorC             &
       &                             )
    !
    Character(len=*), Intent(IN), Optional  :: telescope
    Character(len=*), Intent(IN), Optional  :: observatory
    Character(len=*), Intent(IN), Optional  :: odpSoftware
    !
    Logical         , Intent(IN), Optional  :: default            ! set default values
    Logical         , Intent(IN), Optional  :: reset              ! re-set values to undefined
    !
    Logical,          Intent(OUT), Optional :: error              ! errorFlag
    Character(len=*), Intent(OUT), Optional :: errorC             ! errorCode
    !
    If (Present(error))  error  = .False.
    If (Present(errorC)) errorC = ""
    !
    If (Present(default).And.default) Then
       odpHeader = odpHeaderDefault
    End If
    !
    If (Present(reset).And.reset) Then
       odpHeader = odpHeaderDefault
    End If
    !
    If (Present(telescope))   odpHeader%telescope   = telescope
    If (Present(observatory)) odpHeader%observatory = observatory
    If (Present(odpSoftware)) odpHeader%odpSoftware = odpSoftware
    !
    Return
    !
  End Subroutine resultsToNCSsetOdpHeader
!!!
!!!
  Subroutine resultsToNCSwriteOdpHeader(                          &
       &                                       error,             &
       &                                       errorC             &
       &                               )
    !
    Logical,          Intent(OUT), Optional :: error              ! errorFlag
    Character(len=*), Intent(OUT), Optional :: errorC             ! errorCode
    !
    Logical            :: errorXML
    !
    If (Present(error))  error  = .False.
    If (Present(errorC)) errorC = ""
    !
    Call pakoXMLwriteStartElement(                                &
         &              element = 'RESOURCE',                     &
         &              name    = 'odpHeader',                    &
         &              error   = errorXML)
    !
    If (odpHeader%telescope.Ne.cNone) Then
       Call  pakoXMLwriteElement(                                 &
            &              element = 'PARAM',                     &
            &              name    = 'telescope',                 &
            &              value   = odpHeader%telescope,         &
            &              dataType = 'char',                     &
            &              error   = errorXML)
    End If
    !
    If (odpHeader%observatory.Ne.cNone) Then
       Call  pakoXMLwriteElement(                                 &
            &              element = 'PARAM',                     &
            &              name    = 'observatory',               &
            &              value   = odpHeader%observatory,       &
            &              dataType = 'char',                     &
            &              error   = errorXML)
    End If
    !
    If (odpHeader%odpSoftware.Ne.cNone) Then
       Call  pakoXMLwriteElement(                                 &
            &              element = 'PARAM',                     &
            &              name    = 'odpSoftware',               &
            &              value   = odpHeader%odpSoftware,       &
            &              dataType = 'char',                     &
            &              error   = errorXML)
    End If
    !
    Call pakoXMLwriteEndElement(                                  &
         &              element = 'RESOURCE',                     &
         &              name    = 'odpHeader',                    &
         &              error   = errorXML)          
    !
    If (Present(error) .And. errorXML) error=errorXML
    Return
    !
  End Subroutine resultsToNCSwriteOdpHeader
!!!
!!!
  Subroutine resultsToNCSsetMeasurementHeader(                     &
       &                                       measurement       , &
       &                                       sourceName        , &
       &                                       timeStamp         , &
       &                                       azimuth           , &
       &                                       elevation         , &
       &                                       refraction        , &
       &                                       pressureAmbient   , &
       &                                       temperatureAmbient, &
       &                                       humidityAmbient   , &
       &                                       switchingMode     , &
       &                                       observingMode     , &
       &                                       method            , &
       &                                       focusDirection    , &
       &                                       focusLength       , &
       &                                       focusCurrent      , &
       &                                       pointingLength    , &
       &                                       pointingP1        , &
       &                                       pointingP2        , &
       &                                       pointingP7        , &
       &                                       default           , &
       &                                       reset             , &
       &                                       error             , &
       &                                       errorC              &                        
       &                                     ) 
    !
    Character(len=*), Intent(IN), Optional  :: measurement        
    Character(len=*), Intent(IN), Optional  :: sourceName         
    Character(len=*), Intent(IN), Optional  :: timeStamp          
    Real            , Intent(IN), Optional  :: azimuth            
    Real            , Intent(IN), Optional  :: elevation          
    Real            , Intent(IN), Optional  :: refraction         
    Real            , Intent(IN), Optional  :: pressureAmbient    
    Real            , Intent(IN), Optional  :: temperatureAmbient 
    Real            , Intent(IN), Optional  :: humidityAmbient    
    Character(len=*), Intent(IN), Optional  :: switchingMode      
    Character(len=*), Intent(IN), Optional  :: observingMode      
    Character(len=*), Intent(IN), Optional  :: method             
    Character(len=*), Intent(IN), Optional  :: focusDirection
    Real            , Intent(IN), Optional  :: focusLength     
    Real            , Intent(IN), Optional  :: focusCurrent     
    Real            , Intent(IN), Optional  :: pointingLength     
    Real            , Intent(IN), Optional  :: pointingP1         
    Real            , Intent(IN), Optional  :: pointingP2         
    Real            , Intent(IN), Optional  :: pointingP7         
    !
    Logical         , Intent(IN), Optional  :: default
    Logical         , Intent(IN), Optional  :: reset
    !
    Logical,          Intent(OUT), Optional :: error              ! errorFlag
    Character(len=*), Intent(OUT), Optional :: errorC             ! errorCode
    !
    If (Present(error))  error  = .False.
    If (Present(errorC)) errorC = ""
    !
    If (Present(default).And.default) Then
       mHead = mHeadDefault
    End If
    !
    If (Present(reset).And.reset) Then
       mHead = mHeadDefault
    End If
    !
    If (Present(measurement )) mHead%measurement = measurement        
    If (Present(sourceName  )) mHead%sourceName  = sourceName         
    If (Present(timeStamp   )) mHead%timeStamp   = timeStamp          
    If (Present(azimuth     )) mHead%azimuth     = azimuth            
    !
    If (Present(azimuth           )) Then
       mHead%azimuth            = azimuth            
       mHead%lAzimuth           = .True.
    End If
    !
    If (Present(elevation         )) Then
       mHead%elevation          = elevation          
       mHead%lElevation         = .True.
    End If
    !
    If (Present(refraction        )) Then
       mHead%refraction         = refraction         
       mHead%lRefraction        = .True.
    End If
    !
    If (Present(pressureAmbient   )) Then
       mHead%pressureAmbient    = pressureAmbient    
       mHead%lPressureAmbient   = .True.
    End If
    !
    If (Present(temperatureAmbient)) Then
       mHead%temperatureAmbient = temperatureAmbient 
       mHead%lTemperatureAmbient= .True.
    End If
    !
    If (Present(humidityAmbient   )) Then
       mHead%humidityAmbient    = humidityAmbient    
       mHead%lHumidityAmbient   = .True.
    End If
    !
    If (Present(switchingMode)) mHead%switchingMode = trim(switchingMode)
    If (Present(observingMode)) mHead%observingMode = observingMode      
    If (Present(method       )) mHead%method        = method             
    !
    If (Present(focusDirection)) mHead%focusDirection = focusDirection      
!
    If (Present(focusLength    )) Then
       mHead%focusLength     = focusLength     
       mHead%lFocusLength    = .True.
    End If
    !
    If (Present(focusCurrent    )) Then
       mHead%focusCurrent     = focusCurrent     
       mHead%lFocusCurrent    = .True.
    End If
    !
    If (Present(pointingLength    )) Then
       mHead%pointingLength     = pointingLength     
       mHead%lPointingLength    = .True.
    End If
    !
    If (Present(pointingP1        )) Then
       mHead%pointingP1         = pointingP1         
       mHead%lPointingP1        = .True.
    End If
    !
    If (Present(pointingP2        )) Then
       mHead%pointingP2         = pointingP2         
       mHead%lPointingP2        = .True.
    End If
    !
    If (Present(pointingP7        )) Then
       mHead%pointingP7         = pointingP7         
       mHead%lPointingP7        = .True.
    End If
    !
    Return
    !
  End Subroutine resultsToNCSsetMeasurementHeader
!!!
!!!
  Subroutine resultsToNCSsetScanId(                               &
       &                                       scanId           , &
       &                                       scanN            , &
       &                                       default          , &
       &                                       reset            , &
       &                                       error            , &
       &                                       errorC             &                        
       &                          )
    !
    Character(len=*), Intent(IN), Optional  :: scanId
    Integer         , Intent(IN), Optional  :: scanN
    !
    Logical         , Intent(IN), Optional  :: default
    Logical         , Intent(IN), Optional  :: reset
    !
    Logical,          Intent(OUT), Optional :: error              ! errorFlag
    Character(len=*), Intent(OUT), Optional :: errorC             ! errorCode
    !
    If (Present(error))  error  = .False.
    If (Present(errorC)) errorC = ""
    !
    If (Present(default).And.default) Then
       nScans = 0
       scans(:) = scanDefault
    End If
    !
    If (Present(reset).And.reset) Then
       nScans = 0 
       scans(:) = scanDefault
    End If
    !
    If ((Present(scanId).Or.Present(scanN)) .And. nScans.Lt.nDimScans) Then
       nScans = nScans+1
    End If
    !
    ! TBD: could do a consistency check to ensure that ther is no
    !      "mixed" scan list, i.e., shoud be all Ids OR numbers!
    !
    If (Present(scanId))      scans(nScans)%scanId = scanId
    If (Present(scanN))       scans(nScans)%scanN  = scanN
    !
    Return
    !
  End Subroutine resultsToNCSsetScanId
!!!
!!!
  Subroutine resultsToNCSwriteMeasurementHeader(                  &
       &                                       error,             &
       &                                       errorC             &
       &                                       )
    !
    Logical,          Intent(OUT), Optional :: error              ! errorFlag
    Character(len=*), Intent(OUT), Optional :: errorC             ! errorCode
    !
    Logical            :: errorXML
    Integer            :: ii
    Character(len=64)  :: valueC
    !
    If (Present(error))  error  = .False.
    If (Present(errorC)) errorC = ""
    !
    Call pakoXMLwriteStartElement(                                &
         &              element = 'RESOURCE',                     &
         &              name    = 'measurement',                  &
         &              space   = 'before',                       &
         &              error   = errorXML)
    !
    Call pakoXMLwriteStartElement(                                &
         &              element = 'RESOURCE',                     &
         &              name    = 'measurementHeader',            &
         &              space   = 'before',                       &
         &              error   = errorXML)
    !
    If (mHead%measurement.Ne.cNone) Then
       Call  pakoXMLwriteElement(                                 &
            &              element = 'PARAM',                     &
            &              name    = 'measurement',               &
            &              value   = mHead%measurement,           &
            &              dataType = 'char',                     &
            &              error   = errorXML)
    End If
    !
    If (mHead%sourceName.Ne.cNone) Then
       Call  pakoXMLwriteElement(                                 &
            &              element = 'PARAM',                     &
            &              name    = 'sourceName',                &
            &              value   = mHead%sourceName,            &
            &              dataType = 'char',                     &
            &              error   = errorXML)
    End If
    !
    ! TBD: timeStamp could be generated internally
    !
    If (mHead%timeStamp.Ne.cNone) Then
       Call  pakoXMLwriteElement(                                 &
            &              element = 'PARAM',                     &
            &              name    = 'timeStamp',                 &
            &              value   = mHead%timeStamp,             &
            &              dataType = 'char',                     &
            &              error   = errorXML)
    End If
    !
    If (mHead%lAzimuth) Then
       Write (valueC,*) mHead%azimuth
       Call  pakoXMLwriteElement(                                 &
            &              element  = 'PARAM',                    &
            &              name     = 'azimuth',                  &
            &              value    =  valueC,                    &
            &              dataType = 'double',                   &
            &              unit     = 'deg',                      &
            &              error    =  errorXML)
    End If
    !
    If (mHead%lElevation) Then
       Write (valueC,*) mHead%elevation
       Call  pakoXMLwriteElement(                                 &
            &              element  = 'PARAM',                    &
            &              name     = 'elevation',                &
            &              value    =  valueC,                    &
            &              dataType = 'double',                   &
            &              unit     = 'deg',                      &
            &              error    =  errorXML)
    End If
    !
    If (mHead%lRefraction) Then
       Write (valueC,*) mHead%refraction
       Call  pakoXMLwriteElement(                                 &
            &              element  = 'PARAM',                    &
            &              name     = 'refraction',               &
            &              value    =  valueC,                    &
            &              dataType = 'double',                   &
            &              unit     = 'arcsec',                   &
            &              error    =  errorXML)
    End If
    !
    If (mHead%lTemperatureAmbient) Then
       Write (valueC,*) mHead%temperatureAmbient
       Call  pakoXMLwriteElement(                                 &
            &              element  = 'PARAM',                    &
            &              name     = 'temperatureAmbient',       &
            &              value    =  valueC,                    &
            &              dataType = 'double',                   &
            &              unit     = 'K',                        &
            &              error    =  errorXML)
    End If
    !
    If (mHead%lPressureAmbient) Then
       Write (valueC,*) mHead%pressureAmbient
       Call  pakoXMLwriteElement(                                 &
            &              element  = 'PARAM',                    &
            &              name     = 'pressureAmbient',          &
            &              value    =  valueC,                    &
            &              dataType = 'double',                   &
            &              unit     = '10+2Pa',                   &
            &              error    =  errorXML)
    End If
    !
    If (mHead%lHumidityAmbient) Then
       Write (valueC,*) mHead%humidityAmbient
       Call  pakoXMLwriteElement(                                 &
            &              element  = 'PARAM',                    &
            &              name     = 'humidityAmbient',          &
            &              value    =  valueC,                    &
            &              dataType = 'double',                   &
            &              unit     = '%',                        &
            &              error    =  errorXML)
    End If
    !
    If (mHead%switchingMode.Ne.cNone) Then
       Call  pakoXMLwriteElement(                                 &
            &              element = 'PARAM',                     &
            &              name    = 'switchingMode',             &
            &              value   = mHead%switchingMode,         &
            &              dataType = 'char',                     &
            &              error   = errorXML)
    End If
    !
    If (mHead%observingMode.Ne.cNone) Then
       Call  pakoXMLwriteElement(                                 &
            &              element = 'PARAM',                     &
            &              name    = 'observingMode',             &
            &              value   = mHead%observingMode,         &
            &              dataType = 'char',                     &
            &              error   = errorXML)
    End If
    !
    If (mHead%method.Ne.cNone) Then
       Call  pakoXMLwriteElement(                                 &
            &              element = 'PARAM',                     &
            &              name    = 'method',                    &
            &              value   = mHead%method,                &
            &              dataType = 'char',                     &
            &              error   = errorXML)
    End If
    ! ***
    !
    ! TBD: parameters for specific mesurements, depending on 
    !      "measurement" and "observingMode"
    !
    valueC = mHead%measurement
    Call pakoXMLupper(valueC)
    !
    If (valueC.Eq.'FOCUS') Then                                   ! parameters for FOCUS
       !
       If (mHead%focusDirection.Ne.cNone) Then
          Call  pakoXMLwriteElement(                              &
               &              element = 'PARAM',                  &
               &              name    = 'focusDirection',         &
               &              value   = mHead%focusDirection,     &
               &              dataType = 'char',                  &
               &              error   = errorXML)
       End If
       !
       If (mHead%lFocusLength) Then
          Write (valueC,*) mHead%focusLength
          Call  pakoXMLwriteElement(                              &
               &              element  = 'PARAM',                 &
               &              name     = 'focusLength',           &
               &              value    =  valueC,                 &
               &              dataType = 'double',                &
               &              unit     = 'mm',                    &
               &              error    =  errorXML)
       End If
       !
       If (mHead%lFocusCurrent) Then
          Write (valueC,*) mHead%focusCurrent
          Call  pakoXMLwriteElement(                              &
               &              element  = 'PARAM',                 &
               &              name     = 'focusCurrent',          &
               &              value    =  valueC,                 &
               &              dataType = 'double',                &
               &              unit     = 'mm',                    &
               &              error    =  errorXML)
       End If
       !
    End If                                                        ! end of Focus
    ! ***
    !
    If (valueC.Eq.'POINTING') Then                                ! parameters for POINTING
       !          
       If (mHead%lPointingLength) Then
          Write (valueC,*) mHead%pointingLength
          Call  pakoXMLwriteElement(                              &
               &              element  = 'PARAM',                 &
               &              name     = 'pointingLength',        &
               &              value    =  valueC,                 &
               &              dataType = 'double',                &
               &              unit     = 'arcsec',                &
               &              error    =  errorXML)
       End If
       !
       If (mHead%lPointingP1) Then
          Write (valueC,*) mHead%pointingP1
          Call  pakoXMLwriteElement(                              &
               &              element  = 'PARAM',                 &
               &              name     = 'pointingP1',            &
               &              value    =  valueC,                 &
               &              dataType = 'double',                &
               &              unit     = 'deg',                &
               &              error    =  errorXML)
       End If
       !
       If (mHead%lPointingP2) Then
          Write (valueC,*) mHead%pointingP2
          Call  pakoXMLwriteElement(                              &
               &              element  = 'PARAM',                 &
               &              name     = 'pointingP2',            &
               &              value    =  valueC,                 &
               &              dataType = 'double',                &
               &              unit     = 'deg',                &
               &              error    =  errorXML)
       End If
       !
       If (mHead%lPointingP7) Then
          Write (valueC,*) mHead%pointingP7
          Call  pakoXMLwriteElement(                              &
               &              element  = 'PARAM',                 &
               &              name     = 'pointingP7',            &
               &              value    =  valueC,                 &
               &              dataType = 'double',                &
               &              unit     = 'deg',                &
               &              error    =  errorXML)
       End If
       !
    End If                                                        ! end of Pointing
    ! ***
    !
    Call pakoXMLwriteStartElement("RESOURCE","scanList",          &
         &                         error=errorXML)
       !
    Call pakoXMLwriteStartElement("TABLE","scanList",             &
         &                         error=errorXML)
    !
    If (scans(1)%scanId .Ne. cNone) then
       Call pakoXMLwriteElement("FIELD","scanId",                 &
            &                         dataType="char",            &
            &                         error=errorXML)
    End If
    !
    If (scans(1)%scanN .Ne. iNone) Then
       Call pakoXMLwriteElement("FIELD","scanNumber",             &
            &                         dataType="int",             &
            &                         error=errorXML)
    End If
    !
    Call pakoXMLwriteStartElement("DATA",                         &
         &                         error=errorXML)
    !
    Call pakoXMLwriteStartElement("TABLEDATA",                    &
         &                         error=errorXML)
    ! **
    Do ii = 1,nScans,1
       !
       Call pakoXMLwriteStartElement("TR",                        &
            &                         error=errorXML)
       !
       If (scans(ii)%scanId .Ne. cNone) then
          valueC = scans(ii)%scanId
          Call pakoXMLcase(valueC,error=errorXML)
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
       End If
       !
       If (scans(ii)%scanN .Ne. iNone) then
          Write (valueC,*)                                        &
               &                  scans(ii)%scanN
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
       End If
       !
       Call pakoXMLwriteEndElement("TR",                          &
            &                         error=errorXML)
       !
    End Do
    ! **
    Call pakoXMLwriteEndElement  ("TABLEDATA",                    &
         &                         error=errorXML)
    !
    Call pakoXMLwriteEndElement  ("DATA",                         &
         &                         error=errorXML)
    !
    Call pakoXMLwriteEndElement  ("TABLE","scanList",             &
         &                         error=errorXML)
    !
    Call pakoXMLwriteEndElement  ("RESOURCE","scanList",          &
         &                         space="after",                 &
         &                         error=errorXML)
    !
    Call pakoXMLwriteEndElement(                                  &
         &              element = 'RESOURCE',                     &
         &              name    = 'measurementHeader',            &
         &              error   = errorXML)
    !
    If (Present(error) .And. errorXML) error=errorXML
    Return
    !
  End Subroutine resultsToNCSwriteMeasurementHeader
!!!
!!!
  Subroutine resultsToNCSwriteMeasurementEnd(                     &
       &                                       error,             &
       &                                       errorC             &
       &                                    )
    !
    Logical,          Intent(OUT), Optional :: error              ! errorFlag
    Character(len=*), Intent(OUT), Optional :: errorC             ! errorCode
    !
    Logical            :: errorXML
    !
    If (Present(error))  error  = .False.
    If (Present(errorC)) errorC = ""
    !
    Call pakoXMLwriteEndElement(                                  &
         &              element = 'RESOURCE',                     &
         &              name    = 'measurement',                  &
         &              space   = 'before',                       &
         &              error   = errorXML)          
    !
    If (Present(error) .And. errorXML) error=errorXML
    Return
    !
  End Subroutine resultsToNCSwriteMeasurementEnd
!!!
!!!
  Subroutine resultsToNCSsetReceiver(                             &
       &                                       name             , &
       &                                       frequency        , &
       &                                       frequencyImage   , &
       &                                       offset           , &
       &                                       centerIF         , &
       &                                       lineName         , &
       &                                       sideBand         , &
       &                                       doppler          , &
       &                                       width            , &
       &                                       effForward       , &
       &                                       effBeam          , &
       &                                       scale            , &
       &                                       gainImage        , &
       &                                       tempAmbient      , &
       &                                       tempCold         , &
       &                                       xOffsetNasmyth   , &
       &                                       yOffsetNasmyth   , &
       &                                       default          , &
       &                                       reset            , &
       &                                       error            , &
       &                                       errorC             &
       &                            )
    !
    Character(len=*), Intent(IN), Optional  :: name
    Real*8          , Intent(IN), Optional  :: frequency      
    Real*8          , Intent(IN), Optional  :: frequencyImage     
    Real*8          , Intent(IN), Optional  :: offset        
    Real*8          , Intent(IN), Optional  :: centerIF
    Character(len=*), Intent(IN), Optional  :: lineName
    Character(len=*), Intent(IN), Optional  :: sideBand
    Character(len=*), Intent(IN), Optional  :: doppler
    Character(len=*), Intent(IN), Optional  :: width
    Real            , Intent(IN), Optional  :: effForward
    Real            , Intent(IN), Optional  :: effBeam
    Character(len=*), Intent(IN), Optional  :: scale
    Real            , Intent(IN), Optional  :: gainImage
    Real            , Intent(IN), Optional  :: tempAmbient
    Real            , Intent(IN), Optional  :: tempCold
    !
    Real            , Intent(IN), Optional  :: xOffsetNasmyth
    Real            , Intent(IN), Optional  :: yOffsetNasmyth
    !
    Logical         , Intent(IN), Optional  :: default
    Logical         , Intent(IN), Optional  :: reset
    !
    Logical,          Intent(OUT), Optional :: error              ! errorFlag
    Character(len=*), Intent(OUT), Optional :: errorC             ! errorCode
    !
    If (Present(error))  error  = .False.
    If (Present(errorC)) errorC = ""
    !
    If (Present(default).And.default) Then
       nRec = 0
       receivers(:) = receiverDefault
    End If
    !
    If (Present(reset).And.reset) Then
       nRec = 0
       receivers(:) = receiverDefault
    End If
    !
    If (Present(name) .And. nRec.Lt.nDimReceivers) Then
       nRec = nRec+1
       receivers(nRec)%name        = name
       receivers(nRec)%isConnected = .True.
    End If
    !
    If (Present(frequency))      receivers(nRec)%frequency%value      = frequency      
    If (Present(frequencyImage)) receivers(nRec)%frequencyImage%value = frequencyImage
    If (Present(offset))         receivers(nRec)%offset%value         = offset         
    If (Present(centerIF))       receivers(nRec)%centerIF%value       = centerIF       
    If (Present(lineName))       receivers(nRec)%lineName             = lineName       
    If (Present(sideBand))       receivers(nRec)%sideBand             = sideBand       
    If (Present(doppler))        receivers(nRec)%doppler              = doppler       
    If (Present(width))          receivers(nRec)%width                = width          
    If (Present(effForward))     receivers(nRec)%effForward           = effForward     
    If (Present(effBeam))        receivers(nRec)%effBeam              = effBeam        
    If (Present(scale))          receivers(nRec)%scale                = scale          
    If (Present(gainImage))      receivers(nRec)%gainImage%value      = gainImage      
    If (Present(tempAmbient))    receivers(nRec)%tempAmbient          = tempAmbient    
    If (Present(tempCold))       receivers(nRec)%tempCold             = tempCold       
    If (Present(xOffsetNasmyth)) receivers(nRec)%xOffsetNasmyth       = xOffsetNasmyth
    If (Present(yOffsetNasmyth)) receivers(nRec)%yOffsetNasmyth       = yOffsetNasmyth
    !
    Return
    !
  End Subroutine resultsToNCSsetReceiver
!!!
!!!
  Subroutine resultsToNCSsetCalibrationResults(                   &
       &                                       name             , &
       &                                       frequency        , &
       &                                       frequencyImage   , &
       &                                       lineName         , &
       &                                       sideBand         , &
       &                                       effForward       , &
       &                                       effBeam          , &
       &                                       gainImage        , &
       &                                       tempAmbient      , &
       &                                       tempCold         , &
       &                                       xOffsetNasmyth   , &
       &                                       yOffsetNasmyth   , &
       &                                       lcalof           , &
       &                                       bcalof           , &
       &                                       h2omm            , &
       &                                       trx              , &
       &                                       tsys             , &
       &                                       tcal             , &
       &                                       tatms            , &
       &                                       tatmi            , &
       &                                       tauzen           , &
       &                                       tauzenImage      , &
       &                                       pcold            , &
       &                                       phot             , &
       &                                       psky             , &
       &                                       default          , &
       &                                       reset            , &
       &                                       error            , &
       &                                       errorC             &             
       &                            )
    !
    Character(len=*), Intent(IN), Optional  :: name
    Real*8          , Intent(IN), Optional  :: frequency      
    Real*8          , Intent(IN), Optional  :: frequencyImage     
    Character(len=*), Intent(IN), Optional  :: lineName
    Character(len=*), Intent(IN), Optional  :: sideBand
    Real            , Intent(IN), Optional  :: effForward
    Real            , Intent(IN), Optional  :: effBeam
    Real            , Intent(IN), Optional  :: gainImage
    Real            , Intent(IN), Optional  :: tempAmbient
    Real            , Intent(IN), Optional  :: tempCold
    Real            , Intent(IN), Optional  :: xOffsetNasmyth
    Real            , Intent(IN), Optional  :: yOffsetNasmyth
    !
    Real            , Intent(IN), Optional  :: lcalof
    Real            , Intent(IN), Optional  :: bcalof
    Real            , Intent(IN), Optional  :: h2omm
    Real            , Intent(IN), Optional  :: trx
    Real            , Intent(IN), Optional  :: tsys
    Real            , Intent(IN), Optional  :: tcal
    Real            , Intent(IN), Optional  :: tatms
    Real            , Intent(IN), Optional  :: tatmi
    Real            , Intent(IN), Optional  :: tauzen
    Real            , Intent(IN), Optional  :: tauzenImage
    Real            , Intent(IN), Optional  :: pcold
    Real            , Intent(IN), Optional  :: phot
    Real            , Intent(IN), Optional  :: psky
    !
    Logical         , Intent(IN), Optional  :: default
    Logical         , Intent(IN), Optional  :: reset
    !
    Logical,          Intent(OUT), Optional :: error              ! errorFlag
    Character(len=*), Intent(OUT), Optional :: errorC             ! errorCode
    !
    If (Present(error))  error  = .False.
    If (Present(errorC)) errorC = ""
    !
    If (Present(default).And.default) Then
       nRec = 0
       receivers(:) = receiverDefault
    End If
    !
    If (Present(reset).And.reset) Then
       nRec = 0
       receivers(:) = receiverDefault
    End If
    !
    If (Present(name) .And. nRec.Lt.nDimReceivers) Then
       nRec = nRec+1
       receivers(nRec)%name        = name
       receivers(nRec)%isConnected = .True.
    End If
    !
    If (Present(frequency))      receivers(nRec)%frequency%value      = frequency      
    If (Present(frequencyImage)) receivers(nRec)%frequencyImage%value = frequencyImage
    If (Present(lineName))       receivers(nRec)%lineName             = lineName       
    If (Present(sideBand))       receivers(nRec)%sideBand             = sideBand       
    If (Present(effForward))     receivers(nRec)%effForward           = effForward     
    If (Present(effBeam))        receivers(nRec)%effBeam              = effBeam        
    If (Present(gainImage))      receivers(nRec)%gainImage%value      = gainImage      
    If (Present(tempAmbient))    receivers(nRec)%tempAmbient          = tempAmbient    
    If (Present(tempCold))       receivers(nRec)%tempCold             = tempCold       
    If (Present(xOffsetNasmyth)) receivers(nRec)%xOffsetNasmyth       = xOffsetNasmyth
    If (Present(yOffsetNasmyth)) receivers(nRec)%yOffsetNasmyth       = yOffsetNasmyth
    If (Present(lcalof))         receivers(nRec)%lcalof          = lcalof
    If (Present(bcalof))         receivers(nRec)%bcalof          = bcalof
    If (Present(h2omm))          receivers(nRec)%h2omm           = h2omm 
    If (Present(trx))            receivers(nRec)%trx             = trx
    If (Present(tsys))           receivers(nRec)%tsys            = tsys
    If (Present(tcal))           receivers(nRec)%tcal            = tcal
    If (Present(tatms))          receivers(nRec)%tatms           = tatms
    If (Present(tatmi))          receivers(nRec)%tatmi           = tatmi
    If (Present(tauzen))         receivers(nRec)%tauzen          = tauzen
    If (Present(tauzenImage))    receivers(nRec)%tauzenImage     = tauzenImage
    If (Present(pcold))          receivers(nRec)%pcold           = pcold
    If (Present(phot))           receivers(nRec)%phot            = phot 
    If (Present(psky))           receivers(nRec)%psky            = psky 

    !
    Return
    !
  End Subroutine resultsToNCSsetCalibrationResults
!!!
!!!
  Subroutine resultsToNCSwriteReceivers(                          &
       &                                       error,             &
       &                                       errorC             &
       &                               )
    !
    Logical,          Intent(OUT), Optional :: error              ! errorFlag
    Character(len=*), Intent(OUT), Optional :: errorC             ! errorCode
    !
    Logical            :: errorXML
    Integer            :: ii
    Character(len=64)  :: valueC
    Character(len=64)  :: valueComment
    !
    If (Present(error))  error  = .False.
    If (Present(errorC)) errorC = ""
    !
    Call pakoXMLwriteStartElement(                                &
         &              element = 'RESOURCE',                     &
         &              name    = 'receivers',                    &
         &              space   = 'before',                       &
         &              error   = errorXML)
    !
    Call pakoXMLwriteStartElement(                                &
         &              element = 'TABLE',                        &
         &              name    = 'receivers',                    &
         &              error   = errorXML)
    !
    Call pakoXMLwriteElement("FIELD","receiverName",              &
         &                         dataType="char",               &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","lineName",                  &
         &                         dataType="char",               &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","frequency",                 &
         &                         dataType="double",             &
         &                         unit="GHz",                    &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","sideBand",                  &
         &                         dataType="char",               &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","doppler",                   &
         &                         dataType="char",               &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","width",                     &
         &                         dataType="char",               &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","gainImage",                 &
         &                         dataType="float",              &
         &                         unit="--",                     &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","tempCold",                  &
         &                         dataType="float",              &
         &                         unit="K",                      &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","tempAmbient",               &
         &                         dataType="float",              &
         &                         unit="K",                      &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","effForward",                &
         &                         dataType="float",              &
         &                         unit="--",                     &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","effBeam",                   &
         &                         dataType="float",              &
         &                         unit="--",                     &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","scale",                     &
         &                         dataType="char",               &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","offset",                    &
         &                         dataType="double",             &
         &                         unit="GHz",                    &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","centerIF",                  &
         &                         dataType="double",             &
         &                         unit="GHz",                    &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","frequencyImage",            &
         &                         dataType="double",             &
         &                         unit="GHz",                    &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","xOffsetNasmyth",            &
         &                         dataType="double",             &
         &                         unit="arcsec",                 &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","yOffsetNasmyth",            &
         &                         dataType="double",             &
         &                         unit="arcsec",                 &
         &                         error=errorXML)                          
    !                                                                       
    ! **                                                                    
    Call pakoXMLwriteStartElement("DATA",                         &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteStartElement("TABLEDATA",                    &
         &                         error=errorXML)
    !
    ! **
    Do ii = 1,nDimReceivers,1
       If (receivers(ii)%isConnected) Then
          Call pakoXMLwriteStartElement("TR",                     &
               &                         error=errorXML)
          !
          valueC = receivers(ii)%name
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          valueC = receivers(ii)%lineName
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Write (valueC,'(F20.6)')                                &
               &                   receivers(ii)%frequency%value
          valueComment = 'GHz'
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         comment=valueComment,    &
               &                         error=errorXML)
          !
          valueC = receivers(ii)%sideBand
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          valueC = receivers(ii)%doppler
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          valueC = receivers(ii)%width
          Call pakoXMLcase(valueC,error=errorXML)
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Write (valueC,'(F20.6)')                                &
               &                   receivers(ii)%gainImage%value
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Write (valueC,'(F20.6)')                                &
               &                   receivers(ii)%tempCold
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Write (valueC,'(F20.6)')                                &
               &                   receivers(ii)%tempAmbient
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Write (valueC,'(F20.6)')                                &
               &                   receivers(ii)%effForward
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Write (valueC,'(F20.6)')                                &
               &                   receivers(ii)%effBeam
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          valueC = receivers(ii)%scale
          Call pakoXMLcase(valueC,error=errorXML)
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Write (valueC,'(F20.6)')                                &
               &                   receivers(ii)%offset%value
          valueComment = 'GHz'
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         comment=valueComment,    &
               &                         error=errorXML)
          !
          Write (valueC,'(F20.6)')                                &
               &                   receivers(ii)%centerIF%value
          valueComment = 'GHz'
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         comment=valueComment,    &
               &                         error=errorXML)
          !
          Write (valueC,'(F20.6)')                                &
               &        receivers(ii)%frequencyImage%value
          valueComment = 'GHz'
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         comment=valueComment,    &
               &                         error=errorXML)
          !
          Write (valueC,'(F20.6)')                                &
               &                   receivers(ii)%xOffsetNasmyth
          valueComment = 'arcsec'
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         comment=valueComment,    &
               &                         error=errorXML)
          !
          Write (valueC,'(F20.6)')                                &
               &                   receivers(ii)%yOffsetNasmyth
          valueComment = 'arcsec'
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         comment=valueComment,    &
               &                         error=errorXML)
          !
          Call pakoXMLwriteEndElement("TR",                       &
               &                         error=errorXML)
       End If
    End Do
    !
    Call pakoXMLwriteEndElement  ("TABLEDATA",                    &
         &                         error=errorXML)
    !
    Call pakoXMLwriteEndElement  ("DATA",                         &
         &                         error=errorXML)
    !
    Call pakoXMLwriteEndElement  ("TABLE","receivers",            &
         &                         error=errorXML)
    !
    Call pakoXMLwriteEndElement  ("RESOURCE","receivers",         &
         &                         space="after",                 &
         &                         error=errorXML)
    !
    If (Present(error) .And. errorXML) error=errorXML
    Return
    !
  End Subroutine resultsToNCSwriteReceivers
!!!
!!!
  Subroutine resultsToNCSwriteCalibrationResults(                 &
       &                                       error,             &
       &                                       errorC             &
       &                                 )
    !
    Logical,          Intent(OUT), Optional :: error              ! errorFlag
    Character(len=*), Intent(OUT), Optional :: errorC             ! errorCode
    !
    Logical            :: errorXML
    Integer            :: ii
    Character(len=64)  :: valueC
    Character(len=64)  :: valueComment
    !
    If (Present(error))  error  = .False.
    If (Present(errorC)) errorC = ""
    !
    Call pakoXMLwriteStartElement(                                &
         &              element = 'RESOURCE',                     &
         &              name    = 'calibration',                  &
         &              space   = 'before',                       &
         &              error   = errorXML)
    !
    Call pakoXMLwriteStartElement(                                &
         &              element = 'TABLE',                        &
         &              name    = 'calibration',                  &
         &              error   = errorXML)
    !
    Call pakoXMLwriteElement("FIELD","receiverName",              &
         &                         dataType="char",               &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","lineName",                  &
         &                         dataType="char",               &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","frequency",                 &
         &                         dataType="double",             &
         &                         unit="GHz",                    &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","sideBand",                  &
         &                         dataType="char",               &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","gainImage",                 &
         &                         dataType="float",              &
         &                         unit="--",                     &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","tempCold",                  &
         &                         dataType="float",              &
         &                         unit="K",                      &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","tempAmbient",               &
         &                         dataType="float",              &
         &                         unit="K",                      &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","effForward",                &
         &                         dataType="float",              &
         &                         unit="--",                     &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","effBeam",                   &
         &                         dataType="float",              &
         &                         unit="--",                     &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","frequencyImage",            &
         &                         dataType="double",             &
         &                         unit="GHz",                    &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","xOffsetNasmyth",            &
         &                         dataType="double",             &
         &                         unit="arcsec",                 &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","yOffsetNasmyth",            &
         &                         dataType="double",             &
         &                         unit="arcsec",                 &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","lcalof",                    &
         &                         dataType="double",             &
         &                         unit="arcsec",                 &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","bcalof",                    &
         &                         dataType="double",             &
         &                         unit="arcsec",                 &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","h2omm",                     &
         &                         dataType="double",             &
         &                         unit="mm",                     &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","trx",                       &
         &                         dataType="double",             &
         &                         unit="K",                      &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","tsys",                      &
         &                         dataType="double",             &
         &                         unit="K",                      &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","tcal",                      &
         &                         dataType="double",             &
         &                         unit="K",                      &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","tatms",                     &
         &                         dataType="double",             &
         &                         unit="K",                      &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","tatmi",                     &
         &                         dataType="double",             &
         &                         unit="K",                      &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","tauzen",                    &
         &                         dataType="double",             &
         &                         unit="Neper",                  &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","tauzenImage",               &
         &                         dataType="double",             &
         &                         unit="Neper",                  &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","pcold",                     &
         &                         dataType="double",             &
         &                         unit="counts",                 &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","phot",                      &
         &                         dataType="double",             &
         &                         unit="counts",                 &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteElement("FIELD","psky",                      &
         &                         dataType="double",             &
         &                         unit="counts",                 &
         &                         error=errorXML)                          
    !                                                                       
    ! **                                                                    
    Call pakoXMLwriteStartElement("DATA",                         &
         &                         error=errorXML)                          
    !                                                                       
    Call pakoXMLwriteStartElement("TABLEDATA",                    &
         &                         error=errorXML)
    !
    ! **
    Do ii = 1,nDimReceivers,1
       If (receivers(ii)%isConnected) Then
          Call pakoXMLwriteStartElement("TR",                     &
               &                         error=errorXML)
          !
          valueC = receivers(ii)%name
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          valueC = receivers(ii)%lineName
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Write (valueC,'(F20.6)')                                &
               &                   receivers(ii)%frequency%value
          valueComment = 'GHz'
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         comment=valueComment,    &
               &                         error=errorXML)
          !
          valueC = receivers(ii)%sideBand
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Write (valueC,'(F20.6)')                                &
               &                   receivers(ii)%gainImage%value
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Write (valueC,'(F20.6)')                                &
               &                   receivers(ii)%tempCold
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Write (valueC,'(F20.6)')                                &
               &                   receivers(ii)%tempAmbient
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Write (valueC,'(F20.6)')                                &
               &                   receivers(ii)%effForward
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Write (valueC,'(F20.6)')                                &
               &                   receivers(ii)%effBeam
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Write (valueC,'(F20.6)')                                &
               &        receivers(ii)%frequencyImage%value
          valueComment = 'GHz'
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         comment=valueComment,    &
               &                         error=errorXML)
          !
          Write (valueC,'(F20.6)')                                &
               &                   receivers(ii)%xOffsetNasmyth
          valueComment = 'arcsec'
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         comment=valueComment,    &
               &                         error=errorXML)
          !
          Write (valueC,'(F20.6)')                                &
               &                   receivers(ii)%yOffsetNasmyth
          valueComment = 'arcsec'
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         comment=valueComment,    &
               &                         error=errorXML)
          !
          Write(valueC,'(F20.6)')                                 &
               &                  receivers(ii)%lcalof   
          valueComment = 'arcsec'
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         comment=valueComment,    &
               &                         error=errorXML)
          !
          Write(valueC,'(F20.6)')                                 &
               &                  receivers(ii)%bcalof   
          valueComment = 'arcsec'
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         comment=valueComment,    &
               &                         error=errorXML)
          !
          Write(valueC,'(F20.6)')                                 &
               &                  receivers(ii)%h2omm
          valueComment = 'mm'
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         comment=valueComment,    &
               &                         error=errorXML)
          !
          Write(valueC,'(F20.6)')                                 &
               &                  receivers(ii)%trx
          valueComment = 'K'
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         comment=valueComment,    &
               &                         error=errorXML)
          !
          Write(valueC,'(F20.6)')                                 &
               &                  receivers(ii)%tsys
          valueComment = 'K'
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         comment=valueComment,    &
               &                         error=errorXML)
          !
          Write(valueC,'(F20.6)')                                 &
               &                  receivers(ii)%tcal
          valueComment = 'K'
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         comment=valueComment,    &
               &                         error=errorXML)
          !
          Write(valueC,'(F20.6)')                                 &
               &                  receivers(ii)%tatms
          valueComment = 'K'
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         comment=valueComment,    &
               &                         error=errorXML)
          !
          Write(valueC,'(F20.6)')                                 &
               &                  receivers(ii)%tatmi
          valueComment = 'K'
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         comment=valueComment,    &
               &                         error=errorXML)
          !
          Write(valueC,'(F20.6)')                                 &
               &                  receivers(ii)%tauzen
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Write(valueC,'(F20.6)')                                 &
               &                  receivers(ii)%tauzenImage
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Write(valueC,'(F20.6)')                                 &
               &                  receivers(ii)%pcold
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Write(valueC,'(F20.6)')                                 &
               &                  receivers(ii)%phot
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Write(valueC,'(F20.6)')                                 &
               &                  receivers(ii)%psky
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Call pakoXMLwriteEndElement("TR",                       &
               &                         error=errorXML)
       End If
    End Do
    !
    Call pakoXMLwriteEndElement  ("TABLEDATA",                    &
         &                         error=errorXML)
    !
    Call pakoXMLwriteEndElement  ("DATA",                         &
         &                         error=errorXML)
    !
    Call pakoXMLwriteEndElement  ("TABLE","calibration",          &
         &                         error=errorXML)
    !
    Call pakoXMLwriteEndElement  ("RESOURCE","calibration",       &
         &                         space="after",                 &
         &                         error=errorXML)
    !
    If (Present(error) .And. errorXML) error=errorXML
    Return
    !
  End Subroutine resultsToNCSwriteCalibrationResults
!!!
!!!
  Subroutine resultsToNCSsetBackend(                              &
       &                                       name             , &
       &                                       nPart            , &
       &                                       resolution       , &
       &                                       bandwidth        , &
       &                                       fShift           , &
       &                                       receiver         , &
       &                                       nChannels        , &
       &                                       default          , &
       &                                       reset            , &
       &                                       error            , &
       &                                       errorC             &
       &                           )
    !
    Character(len=*), Intent(IN), Optional  :: name
    Integer         , Intent(IN), Optional  :: nPart
    Real            , Intent(IN), Optional  :: resolution
    Real            , Intent(IN), Optional  :: bandwidth          
    Real            , Intent(IN), Optional  :: fShift        
    Character(len=*), Intent(IN), Optional  :: receiver
    Integer         , Intent(IN), Optional  :: nChannels
    !
    Logical         , Intent(IN), Optional  :: default
    Logical         , Intent(IN), Optional  :: reset
    !
    Logical,          Intent(OUT), Optional :: error              ! errorFlag
    Character(len=*), Intent(OUT), Optional :: errorC             ! errorCode
    !
    If (Present(error))  error  = .False.
    If (Present(errorC)) errorC = ""
    !
    If (Present(default).And.default) Then
       nBack = 0
       backends(:) = backendDefault
    End If
    !
    If (Present(reset).And.reset) Then
       nBack = 0 
       backends(:) = backendDefault
    End If
    !
    If (Present(name) .And. nBack.Lt.nDimBackends) Then
       nBack = nBack+1
       backends(nBack)%name        = name
       backends(nBack)%isConnected = .True.
    End If
    !
    If (Present(nPart))      backends(nBack)%nPart        = nPart
    If (Present(resolution)) backends(nBack)%resolution   = resolution
    If (Present(bandwidth))  backends(nBack)%bandwidth    = bandwidth
    If (Present(fShift))     backends(nBack)%fShift       = fShift
    If (Present(receiver))   backends(nBack)%receiverName = receiver
    If (Present(nChannels))  backends(nBack)%nChannels    = nChannels
    !
    Return
    !
  End Subroutine resultsToNCSsetBackend
!!!
!!!
  Subroutine resultsToNCSwriteBackends(                           &
       &                                       error,             &
       &                                       errorC             &
       &                              )
    !
    Logical,          Intent(OUT), Optional :: error              ! errorFlag
    Character(len=*), Intent(OUT), Optional :: errorC             ! errorCode
    !
    Logical            :: errorXML
    Integer            :: ii
    Character(len=64)  :: valueC
    Character(len=64)  :: valueComment
    !
    If (Present(error))  error  = .False.
    If (Present(errorC)) errorC = ""
    !
    Call pakoXMLwriteStartElement("RESOURCE","backends",          &
         &                         space ="before",               &
         &                         error=errorXML)
    !
    Call pakoXMLwriteStartElement("TABLE","backends",             &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","backendName",               &
         &                         dataType="char",               &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","nPart",                     &
         &                         dataType="int",                &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","resolution",                &
         &                         dataType="double",             &
         &                         unit="GHz",                    &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","bandwidth",                 &
         &                         dataType="double",             &
         &                         unit="GHz",                    &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","fShift",                    &
         &                         dataType="double",             &
         &                         unit="GHz",                    &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","receiver",                  &
         &                         dataType="char",               &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","nChannels",                 &
         &                         dataType="int",                &
         &                         error=errorXML)
    !
    Call pakoXMLwriteStartElement("DATA",                         &
         &                         error=errorXML)
    !
    Call pakoXMLwriteStartElement("TABLEDATA",                    &
         &                         error=errorXML)
    !
    ! **
    Do ii = 1,nDimBackends,1
       If (backends(ii)%isConnected) Then
          Call pakoXMLwriteStartElement("TR",                     &
               &                         error=errorXML)
          !
          valueC = backends(ii)%name
          Call pakoXMLcase(valueC,error=errorXML)
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Write (valueC,*)                                        &
               &                  backends(ii)%nPart
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Write (valueC,'(ES20.6)')                               &
               &                  backends(ii)%resolution/1.0D3
          Write (valueComment,*) backends(ii)%resolution,'MHz' 
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         comment=valueComment,    &
               &                         error=errorXML)
          !
          Write (valueC,'(ES20.6)')                               &
               &                  backends(ii)%bandwidth/1.0D3
          Write (valueComment,*) backends(ii)%bandwidth,'MHz' 
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         comment=valueComment,    &
               &                         error=errorXML)
          !
          Write (valueC,'(ES20.6)')                               &
               &                  backends(ii)%fShift/1.0D3
          Write (valueComment,*) backends(ii)%fShift,'MHz' 
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         comment=valueComment,    &
               &                         error=errorXML)
          !
          valueC = backends(ii)%receiverName
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Write (valueC,*)                                        &
               &                  backends(ii)%nChannels
          Call pakoXMLwriteElement("TD",                          &
               &                         content=valueC,          &
               &                         error=errorXML)
          !
          Call pakoXMLwriteEndElement("TR",                       &
               &                         error=errorXML)
       End If
    End Do
    ! ***
    !
    Call pakoXMLwriteEndElement  ("TABLEDATA",                    &
         &                         error=errorXML)
    !
    Call pakoXMLwriteEndElement  ("DATA",                         &
         &                         error=errorXML)
    !
    Call pakoXMLwriteEndElement  ("TABLE","backends",             &
         &                         error=errorXML)
    !
    Call pakoXMLwriteEndElement  ("RESOURCE","backends",          &
         &                         space="after",                 &
         &                         error=errorXML)
    !
    If (Present(error) .And. errorXML) error=errorXML
    Return
    !
  End Subroutine resultsToNCSwriteBackends
!!!
!!!
  Subroutine resultsToNCSsetFocusResults(                         &
       &                                       backendName      , &
       &                                       nPart            , &
       &                                       focus            , &
       &                                       offset           , &
       &                                       rmsResidual      , &
       &                                       default          , &
       &                                       reset            , &
       &                                       error            , &
       &                                       errorC             &                        
       &                                   )
    !
    Character(len=*), Intent(IN), Optional  :: backendName  
    Integer         , Intent(IN), Optional  :: nPart        
    Type(resultT)   , Intent(IN), Optional  :: focus
    Type(resultT)   , Intent(IN), Optional  :: offset       
    Real*4          , Intent(IN), Optional  :: rmsResidual  
    !
    Logical         , Intent(IN), Optional  :: default
    Logical         , Intent(IN), Optional  :: reset
    !
    Logical         , Intent(OUT), Optional :: error              ! errorFlag
    Character(len=*), Intent(OUT), Optional :: errorC             ! errorCode
    !
    Integer :: ii
    !
    If (Present(error))  error  = .False.
    If (Present(errorC)) errorC = ""
    !
    If (Present(default).And.default) Then
       nFocusResults = 0
       FocusResults(:) = FocusResultDefault
    End If
    !
    If (Present(reset).And.reset) Then
       nFocusResults = 0
       FocusResults(:) = FocusResultDefault
    End If
    !
    If (Present(backendName) .And. nBack.Lt.nDimBackends) Then
       nFocusResults = nFocusResults+1
       ii               = nFocusResults
       focusResults(ii)%backendName = backendName
    End If
    !
    If (Present(nPart))       focusResults(ii)%nPart        = nPart
    If (Present(focus))       focusResults(ii)%focus        = focus
    If (Present(offset))      focusResults(ii)%offset       = offset
    If (Present(rmsResidual)) focusResults(ii)%rmsResidual  = rmsResidual
    !
!!$    write (6,*) '  focusResults(ii)%nPart    ',  focusResults(ii)%nPart
!!$    write (6,*) '  focusResults(ii)%focus    ',  focusResults(ii)%focus
!!$    write (6,*) '  focusResults(ii)%offset   ',  focusResults(ii)%offset 
!!$    write (6,*) '  focusResults(ii)%rmsResidual   ', focusResults(ii)%rmsResidual
    !
    Return
    !
  End Subroutine resultsToNCSsetFocusResults
!!!
!!!
  Subroutine resultsToNCSwriteFocusResults(                       &
       &                                       error,             &
       &                                       errorC             &
       &                                     )
    !
    Logical,          Intent(OUT), Optional :: error              ! errorFlag
    Character(len=*), Intent(OUT), Optional :: errorC             ! errorCode
    !
    Logical            :: errorXML
    Integer            :: ii
    Character(len=64)  :: valueC
    Character(len=64)  :: valueComment
    !
    If (Present(error))  error  = .False.
    If (Present(errorC)) errorC = ""
    !
    Call pakoXMLwriteStartElement("RESOURCE","results",           &
         &                         space ="before",               &
         &                         error=errorXML)
    !
    Call pakoXMLwriteStartElement("TABLE","results",              &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","backendName",               &
         &                         dataType="char",               &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","nPart",                     &
         &                         dataType="int",                &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","focus",                &
         &                         dataType="double",             &
         &                         unit="mm",                 &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","focusError",           &
         &                         dataType="double",             &
         &                         unit="mm",                 &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","offset",                    &
         &                         dataType="double",             &
         &                         unit="mm",                 &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","offsetError",               &
         &                         dataType="double",             &
         &                         unit="mm",                 &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","rmsResidual",               &
         &                         dataType="double",             &
         &                         unit="K",                      &
         &                         error=errorXML)
    !
    Call pakoXMLwriteStartElement("DATA",                         &
         &                         error=errorXML)
    !
    Call pakoXMLwriteStartElement("TABLEDATA",                    &
         &                         error=errorXML)
    !
    ! **
    Do ii = 1,nFocusResults,1
       !
       Call pakoXMLwriteStartElement("TR",                        &
            &                         error=errorXML)
       !
       valueC = focusResults(ii)%backendName
       Call pakoXMLcase(valueC,error=errorXML)
       Call pakoXMLwriteElement("TD",                             &
            &                         content=valueC,             &
            &                         error=errorXML)
       !
       Write (valueC,*)                                           &
            &                  focusResults(ii)%nPart
       Call pakoXMLwriteElement("TD",                             &
            &                         content=valueC,             &
            &                         error=errorXML)
       !
       Write (valueC,'(ES20.6)')                                  &
            &                  focusResults(ii)%focus%value
       Call pakoXMLwriteElement("TD",                             &
            &                         content=valueC,             &
            &                         error=errorXML)
       !
       Write (valueC,'(ES20.6)')                                  &
            &                  focusResults(ii)%focus%error
       Call pakoXMLwriteElement("TD",                             &
            &                         content=valueC,             &
            &                         error=errorXML)
       !
       Write (valueC,'(ES20.6)')                                  &
            &                  focusResults(ii)%offset%value
       Call pakoXMLwriteElement("TD",                             &
            &                         content=valueC,             &
            &                         error=errorXML)
       !
       Write (valueC,'(ES20.6)')                                  &
            &                  focusResults(ii)%offset%error
       Call pakoXMLwriteElement("TD",                             &
            &                         content=valueC,             &
            &                         error=errorXML)
       !
       Write (valueC,'(ES20.6)')                                  &
            &                  focusResults(ii)%rmsResidual
       Call pakoXMLwriteElement("TD",                             &
            &                         content=valueC,             &
            &                         error=errorXML)
       !
       Call pakoXMLwriteEndElement("TR",                          &
            &                         error=errorXML)
    End Do
    ! **
    !
    Call pakoXMLwriteEndElement  ("TABLEDATA",                    &
         &                         error=errorXML)
    !
    Call pakoXMLwriteEndElement  ("DATA",                         &
         &                         error=errorXML)
    !
    Call pakoXMLwriteEndElement  ("TABLE","results",              &
         &                         error=errorXML)
    !
    Call pakoXMLwriteEndElement  ("RESOURCE","results",           &
         &                         space="after",                 &
         &                         error=errorXML)
    !
    If (Present(error) .And. errorXML) error=errorXML
    Return
    !
  End Subroutine resultsToNCSwriteFocusResults
!!!
!!!
  Subroutine resultsToNCSsetPointingResults(                      &
       &                                       backendName      , &
       &                                       nPart            , &
       &                                       direction        , & 
       &                                       correction       , &
       &                                       peak             , &
       &                                       integral         , &
       &                                       offset           , &
       &                                       width            , &
       &                                       rmsResidual      , &
       &                                       default          , &
       &                                       reset            , &
       &                                       error            , &
       &                                       errorC             &                        
       &                                   )
    !
    Character(len=*), Intent(IN), Optional  :: backendName  
    Integer         , Intent(IN), Optional  :: nPart        
    Character(len=*), Intent(IN), Optional  :: direction    
    Type(resultT)   , Intent(IN), Optional  :: correction   
    Type(resultT)   , Intent(IN), Optional  :: peak         
    Type(resultT)   , Intent(IN), Optional  :: integral     
    Type(resultT)   , Intent(IN), Optional  :: offset       
    Type(resultT)   , Intent(IN), Optional  :: width        
    Real*4          , Intent(IN), Optional  :: rmsResidual  
    !
    Logical         , Intent(IN), Optional  :: default
    Logical         , Intent(IN), Optional  :: reset
    !
    Logical         , Intent(OUT), Optional :: error              ! errorFlag
    Character(len=*), Intent(OUT), Optional :: errorC             ! errorCode
    !
    Integer :: ii
    !
    If (Present(error))  error  = .False.
    If (Present(errorC)) errorC = ""
    !
    If (Present(default).And.default) Then
       nPointingResults = 0
       PointingResults(:) = PointingResultDefault
    End If
    !
    If (Present(reset).And.reset) Then
       nPointingResults = 0
       PointingResults(:) = PointingResultDefault
    End If
    !
    If (Present(backendName) .And. nBack.Lt.nDimBackends) Then
       nPointingResults = nPointingResults+1
       ii               = nPointingResults
       pointingResults(ii)%backendName = backendName
    End If
    !
    If (Present(nPart))       pointingResults(ii)%nPart        = nPart
    If (Present(direction))   pointingResults(ii)%direction    = direction
    If (Present(correction))  pointingResults(ii)%correction   = correction
    If (Present(peak))        pointingResults(ii)%peak         = peak
    If (Present(integral))    pointingResults(ii)%integral     = integral
    If (Present(offset))      pointingResults(ii)%offset       = offset
    If (Present(width))       pointingResults(ii)%width        = width
    If (Present(rmsResidual)) pointingResults(ii)%rmsResidual  = rmsResidual
    !
    Return
    !
  End Subroutine resultsToNCSsetPointingResults
!!!
!!!
  Subroutine resultsToNCSwritePointingResults(                    &
       &                                       error,             &
       &                                       errorC             &
       &                                     )
    !
    Logical,          Intent(OUT), Optional :: error              ! errorFlag
    Character(len=*), Intent(OUT), Optional :: errorC             ! errorCode
    !
    Logical            :: errorXML
    Integer            :: ii
    Character(len=64)  :: valueC
    Character(len=64)  :: valueComment
    !
    If (Present(error))  error  = .False.
    If (Present(errorC)) errorC = ""
    !
    Call pakoXMLwriteStartElement("RESOURCE","results",           &
         &                         space ="before",               &
         &                         error=errorXML)
    !
    Call pakoXMLwriteStartElement("TABLE","results",              &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","backendName",               &
         &                         dataType="char",               &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","nPart",                     &
         &                         dataType="int",                &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","direction",                 &
         &                         dataType="char",               &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","correction",                &
         &                         dataType="double",             &
         &                         unit="arcsec",                 &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","correctionError",           &
         &                         dataType="double",             &
         &                         unit="arcsec",                 &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","peak",                      &
         &                         dataType="double",             &
         &                         unit="K",                      &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","peakError",                 &
         &                         dataType="double",             &
         &                         unit="K",                      &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","integral",                  &
         &                         dataType="double",             &
         &                         unit="K.arcsec",               &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","integralError",             &
         &                         dataType="double",             &
         &                         unit="K.arcsec",               &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","offset",                    &
         &                         dataType="double",             &
         &                         unit="arcsec",                 &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","offsetError",               &
         &                         dataType="double",             &
         &                         unit="arcsec",                 &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","width",                     &
         &                         dataType="double",             &
         &                         unit="arcsec",                 &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","widthError",                &
         &                         dataType="double",             &
         &                         unit="arcsec",                 &
         &                         error=errorXML)
    !
    Call pakoXMLwriteElement("FIELD","rmsResidual",               &
         &                         dataType="double",             &
         &                         unit="K",                      &
         &                         error=errorXML)
    !
    Call pakoXMLwriteStartElement("DATA",                         &
         &                         error=errorXML)
    !
    Call pakoXMLwriteStartElement("TABLEDATA",                    &
         &                         error=errorXML)
    !
    ! **
    Do ii = 1,nPointingResults,1
       !
       Call pakoXMLwriteStartElement("TR",                        &
            &                         error=errorXML)
       !
       valueC = pointingResults(ii)%backendName
       Call pakoXMLcase(valueC,error=errorXML)
       Call pakoXMLwriteElement("TD",                             &
            &                         content=valueC,             &
            &                         error=errorXML)
       !
       Write (valueC,*)                                           &
            &                  pointingResults(ii)%nPart
       Call pakoXMLwriteElement("TD",                             &
            &                         content=valueC,             &
            &                         error=errorXML)
       !
       valueC = pointingResults(ii)%direction
       Call pakoXMLcase(valueC,error=errorXML)
       Call pakoXMLwriteElement("TD",                             &
            &                         content=valueC,             &
            &                         error=errorXML)
       !
       Write (valueC,'(ES20.6)')                                  &
            &         pointingResults(ii)%correction%value
       Call pakoXMLwriteElement("TD",                             &
            &                         content=valueC,             &
            &                         error=errorXML)
       !
       Write (valueC,'(ES20.6)')                                  &
            &         pointingResults(ii)%correction%error
       Call pakoXMLwriteElement("TD",                             &
            &                         content=valueC,             &
            &                         error=errorXML)
       !
       Write (valueC,'(ES20.6)')                                  &
            &                  pointingResults(ii)%peak%value
       Call pakoXMLwriteElement("TD",                             &
            &                         content=valueC,             &
            &                         error=errorXML)
       !
       Write (valueC,'(ES20.6)')                                  &
            &                  pointingResults(ii)%peak%error
       Call pakoXMLwriteElement("TD",                             &
            &                         content=valueC,             &
            &                         error=errorXML)
       !
       Write (valueC,'(ES20.6)')                                  &
            &                  pointingResults(ii)%integral%value
       Call pakoXMLwriteElement("TD",                             &
            &                         content=valueC,             &
            &                         error=errorXML)
       !
       Write (valueC,'(ES20.6)')                                  &
            &                  pointingResults(ii)%integral%error
       Call pakoXMLwriteElement("TD",                             &
            &                         content=valueC,             &
            &                         error=errorXML)
       !
       Write (valueC,'(ES20.6)')                                  &
            &                  pointingResults(ii)%offset%value
       Call pakoXMLwriteElement("TD",                             &
            &                         content=valueC,             &
            &                         error=errorXML)
       !
       Write (valueC,'(ES20.6)')                                  &
            &                  pointingResults(ii)%offset%error
       Call pakoXMLwriteElement("TD",                             &
            &                         content=valueC,             &
            &                         error=errorXML)
       !
       Write (valueC,'(ES20.6)')                                  &
            &                  pointingResults(ii)%width%value
       Call pakoXMLwriteElement("TD",                             &
            &                         content=valueC,             &
            &                         error=errorXML)
       !
       Write (valueC,'(ES20.6)')                                  &
            &                  pointingResults(ii)%width%error
       Call pakoXMLwriteElement("TD",                             &
            &                         content=valueC,             &
            &                         error=errorXML)
       !
       Write (valueC,'(ES20.6)')                                  &
            &                  pointingResults(ii)%rmsResidual
       Call pakoXMLwriteElement("TD",                             &
            &                         content=valueC,             &
            &                         error=errorXML)
       !
       Call pakoXMLwriteEndElement("TR",                          &
            &                         error=errorXML)
    End Do
    ! **
    !
    Call pakoXMLwriteEndElement  ("TABLEDATA",                    &
         &                         error=errorXML)
    !
    Call pakoXMLwriteEndElement  ("DATA",                         &
         &                         error=errorXML)
    !
    Call pakoXMLwriteEndElement  ("TABLE","results",              &
         &                         error=errorXML)
    !
    Call pakoXMLwriteEndElement  ("RESOURCE","results",           &
         &                         space="after",                 &
         &                         error=errorXML)
    !
    If (Present(error) .And. errorXML) error=errorXML
    Return
    !
  End Subroutine resultsToNCSwritePointingResults
!!!
!!!
  Subroutine resultsToNCSwriteEnd(                                &
       &                                       error,             &
       &                                       errorC             &
       &                         )
    !
    Logical,          Intent(OUT), Optional :: error              ! errorFlag
    Character(len=*), Intent(OUT), Optional :: errorC             ! errorCode
    !
    Logical            :: errorXML
    !
    If (Present(error))  error  = .False.
    If (Present(errorC)) errorC = ""
    !
    Call pakoXMLwriteEndElement(                                  &
         &              element = 'VOTABLE',                      &
         &              space   = 'before',                       &
         &              error   = errorXML)          
    !
    If (Present(error) .And. errorXML) error=errorXML
    Return
    !
  End Subroutine resultsToNCSwriteEnd
!!!
!!!
End Module moduleResultsToNCS
!!!


