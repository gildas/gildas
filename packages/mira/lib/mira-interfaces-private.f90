module mira_interfaces_private
  interface
    subroutine mira_pack_set(pack)
      use gpack_def
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(gpack_info_t), intent(out) :: pack
    end subroutine mira_pack_set
  end interface
  !
  interface
    subroutine mira_pack_init(gpack_id,error)
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      integer :: gpack_id
      logical :: error
    end subroutine mira_pack_init
  end interface
  !
  interface
    subroutine mira_pack_clean(error)
      !----------------------------------------------------------------------
      ! @ private
      ! Called at end of session. Might clean here for example global buffers
      ! allocated during the session
      !----------------------------------------------------------------------
      logical :: error
    end subroutine mira_pack_clean
  end interface
  !
end module mira_interfaces_private
