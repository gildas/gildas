SUBROUTINE protoResultsPointing(ifb,ipix,error)
!
! Writes results from pointing cross scans (evaluated by MIRA with command
! SOLVE) into an XML file to be used by the NCS for monitoring and feedback
! of pointing corrections.
!
   Use mira
   Use modulePakoXML
   Use moduleResultsToNCS
   Use telcal_interfaces
   Use fit_definitions
   USe pcross_definitions
   Use fit 
   Use gbl_format
   Use gkernel_interfaces

   Implicit None

   Integer            :: idPart, ier, ifb, iobs, ipix, k1, k2, lpix=0,      &
             &           LF, lun, nLam, nBet, scanN
   Real*4             :: bandwidth, peakError, peakValue, pointingLength,   &
                         imbftsve
   Real*8             :: correction, frequency, frequencyImage, offset,     &
             &           pointingOffset, foffx, foffy
   Character(len=256) :: vis_data
   Character(len=200) :: filename
   Character(len=128) :: site
   Character(len=20)  :: switchingMode, tmpString
   Character(len=32)  :: lastFebe=' '
   Character(len=15)  :: lastScanId=' ',scanId
   Character(len=13)  :: backendName, frontendName
   Character(len=7)   :: scale
   Character(len=7)   :: doppler
   Character(len=4)   :: year
   Character(len=2)   :: day, month 
   Logical            :: error, ex, foundCal, lError, resetBe, resetRx

   Save lastFebe, lastScanId, lpix

   Call Message(2,1,'protoResults','runs')
   Call Message(2,1,'protoResultsPointing',' starts writing')

   year  = scan%header%date_obs(1:4)
   month = scan%header%date_obs(6:7)
   day   = scan%header%date_obs(9:10)

   if (scan%header%scannum.lt.10) then
      write(filename,'(a17,a4,a2,a2,a1,i1,a4)') 'iram30m-pointing-',     &
 &                              year,month,day,'s',scan%header%scannum,'.xml'
   else if (scan%header%scannum.lt.100) then
      write(filename,'(a17,a4,a2,a2,a1,i2,a4)') 'iram30m-pointing-',     &
 &                              year,month,day,'s',scan%header%scannum,'.xml'
   else if (scan%header%scannum.lt.1000) then
      write(filename,'(a17,a4,a2,a2,a1,i3,a4)') 'iram30m-pointing-',     &
 &                              year,month,day,'s',scan%header%scannum,'.xml'
   else
      write(filename,'(a17,a4,a2,a2,a1,i4,a4)') 'iram30m-pointing-',     &
 &                              year,month,day,'s',scan%header%scannum,'.xml'
   endif
   call sic_get_char('visData',vis_data,ier,error)
   if (vis_data.eq.'.') then
      filename = './'//trim(filename)
   else
      inquire(file=trim(vis_data)//'/mira/results',exist=ex)
      if (.not.ex) then
        call gagout(' ')
        call gagout('W-SOLVE: subdirectory for XML file does not exist.')
        call gagout('         Reset to default (working directory).')
        call gagout(' ')
      else
         filename = trim(vis_data)//'/mira/results/'//trim(filename)
      endif
   endif

   ier = sic_getlun(lun)
   Open (unit=lun,file=filename, recl=facunf*512,     &
        &   status='unknown')
   Call pakoXMLsetOutputUnit(iUnit = lun, error = lError)
   Call pakoXMLsetIndent(iIndent = 2, error = lError)
   Call resultsToNCSwriteProlog(error = lError)

   pointingLength = real(abs(pcross(1)%dat%x(pcross(1)%dat%n)          &
        &           -pcross(1)%dat%x(1)))

   tmpString = febe(ifb)%header%swtchmod
   call sic_upper(tmpString)
   if (index(tmpString,'WOB').ne.0) then
      switchingMode = 'wobblerSwitching'
   else if (index(tmpString,'TOT').ne.0) then
      switchingMode = 'totalPower'
   else if (index(tmpString,'BEAM').ne.0) then
      switchingMode = 'beamSwitching'
   endif

   if (index(scan%header%telescop,"30M").ne.0.or.                      &
   &   index(scan%header%telescop,"30m").ne.0)                         &
   &                site = "Pico Veleta"
   Call resultsToNCSsetOdpHeader(                                      &
        &              telescope   = scan%header%telescop,             &
        &              observatory = site,                             &
        &              odpSoftware = prim%header%creator,              &
        &              reset       = .True.,                           &
        &              error       = lError                            &
        &                       ) 
   Call resultsToNCSwriteOdpHeader(error = lError) 

   Call resultsToNCSsetMeasurementHeader(                              &
                       measurement        = "Pointing",                &
        &              sourceName         = trim(scan%header%object),  & 
        &              timeStamp          = scan%header%date_obs,      &
        &              azimuth            =                            &
	&                       real(mon(ifb)%data%encoder_az_el(1,1)),&
        &              elevation          =                            &
	&                       real(mon(ifb)%data%encoder_az_el(1,2)),&
        &              refraction         =                            &
	&                       real(mon(ifb)%data%refractio(1)),      &
        &              pressureAmbient    =                            &
	&                       real(mon(ifb)%data%tamb_p_humid(1,2)), &
        &              temperatureAmbient = 273.16+                    &
	&                       real(mon(ifb)%data%tamb_p_humid(1,1)), &
        &              humidityAmbient    =                            &
	&                       real(mon(ifb)%data%tamb_p_humid(1,3)), &
        &              switchingMode      = switchingMode,             &
        &              observingMode      = "pointing",                &
        &              method             = "Gaussian",                &
        &              pointingLength     =  pointingLength,           &
        &              pointingP1         =                            &
        &                   real(mon(ifb)%header%iaobs_caobs_ieobs(1)),&
        &              pointingP2         =                            &
        &                   real(mon(ifb)%header%iaobs_caobs_ieobs(2)),& 
        &              pointingP7         =                            &
        &                   real(mon(ifb)%header%iaobs_caobs_ieobs(3)),&
        &              reset              = .True.,                    &
        &              error              = lError                     &
	&                        )
!
! information about calibration scan for this pointing (not yet needed,
! since XML file does not contain calibration information)
!
!   foundCal = .false.
!   do iobs = list%found,1,-1 
!      if (index(list%attribute(iobs,5),'CAL').ne.0) then
!          foundCal = .true.
!          scanId(1:4) = list%attribute(iobs,1)
!          scanN = iachar(scanId(4:4))-48               &
!        &         +10*(iachar(scanId(3:3))-48          &
!        &              +10*(iachar(scanId(2:2))-48     &
!        &                   +10*(iachar(scanId(1:1))-48&
!        &                       )                      &
!        &                  )                           &
!        &             )
!          write(scanId,'(a10,a5)') list%attribute(iobs,8),'.' &
!        &       //list%attribute(iobs,1)
!          Call resultsToNCSsetScanId(                         &
!        &                     scanId = scanId,                &
!        &                     scanN  = scanN,                 &
!        &                     reset  = .True.,                &
!        &                     error  = lError                 &
!        &              ) 
!      endif
!      if (scanN.eq.scan%header%scannum-1) exit 
!   enddo
!
   write(scanId,'(a11,i4.4)') scan%header%date_obs(1:10)//'.', &
         &                    scan%header%scannum 

   resetRx = .true.
   resetBe = .true.

   if (scanId.eq.lastScanId.and.(febe(ifb)%header%febe.ne.lastFebe       &
        & .or.lpix.ne.ipix)) then
      resetRx = .false.
      resetBe = .false.
   endif

   lpix = ipix
   lastFebe = febe(ifb)%header%febe
   lastScanId = scanId

   Call resultsToNCSsetScanId(                       &
        &              scanId = scanId,              &
        &              scanN  = scan%header%scannum, &
!        &              reset  = .not.foundCal,       &
        &              reset  = .true.,              &
        &              error  = lError               &
        &              ) 
   Call resultsToNCSwriteMeasurementHEader(error = lError)
  
   frequency = array(ifb)%header(1)%restfreq*1.d-9
   offset = ((data(ifb)%header%channels/2.+0.5-data(ifb)%header%crpx4f_2)    &
 &           *data(ifb)%header%cd4f_21)*1.d-9

   if (index(array(ifb)%header(1)%sideband,'LSB').ne.0) then
      frequencyImage = frequency+array(ifb)%header(1)%sbsep*1.d-9
   else
      frequencyImage = frequency-array(ifb)%header(1)%sbsep*1.d-9
   endif

   if (scan%header%movefram) then
      doppler = 'fixed'
   else
      doppler = 'Doppler'
   endif

   read(pr%header%mbftsver,'(f3.1)') imbftsve

   if (reduce(ifb)%calibration_done(1)) then
      scale = 'antenna'
   else
      scale = 'backend'
   endif
   if (index(febe(ifb)%header%febe(1:4),'HERA').ne.0) then
      frontendName = febe(ifb)%header%febe(1:5)//" Pixel "
      write(frontendName(13:13),'(i1)') ipix
      LF = 13
   else if (imbftsve.lt.2) then
      frontendName(1:4) = febe(ifb)%header%febe(1:4)
      LF = 4
   else
      frontendName(1:5) = febe(ifb)%header%febe(1:5)
      LF = 5
   endif
   foffx = febe(ifb)%data%feedoffx(ipix)*3600.
   foffy = febe(ifb)%data%feedoffy(ipix)*3600.

   Call resultsToNCSsetReceiver(                                       &
        &       name           = frontendName(1:LF),                   &
        &       frequency      = frequency,                            &   
        &       lineName       = array(ifb)%header(1)%transiti,        &   
        &       sideBand       = array(ifb)%header(1)%sideband,        &
        &       doppler        = Doppler,                              &
        &       width          = febe(ifb)%header%widenar,             &
        &       effForward     = febe(ifb)%data%etafss(ipix,1),        &
        &       effBeam        = febe(ifb)%data%beameff(ipix,1),       &
        &       scale          = scale,                                &
        &       gainImage      = febe(ifb)%data%gainimag(ipix,1),      &
        &       tempAmbient    = real(mon(ifb)%data%thotcold(1,1)),    &
        &       tempCold       = real(mon(ifb)%data%thotcold(1,2)),    &
        &       offset         = offset,                               &
        &       centerIF       = febe(ifb)%header%ifcenter*1.d-9,      &   
        &       frequencyImage = frequencyImage,                       &
        &       xOffsetNasmyth = real(foffx),                          &
        &       yOffsetNasmyth = real(foffy),                          &
        &       reset          = resetRx,                              &
        &       error          = Error                                 &
        &                      )

   Call resultsToNCSwriteReceivers(error = lError)
   if (index(febe(ifb)%header%febe,'VESPA').ne.0) then
      backendName = 'VESPA'
   else if (index(febe(ifb)%header%febe,'FTS').ne.0) then
      backendName = 'FTS'
   else if (index(febe(ifb)%header%febe,'CONT').ne.0) then
      backendName = 'continuum' 
   else if (index(febe(ifb)%header%febe,'BBC').ne.0) then
      backendName = 'BBC' 
   else if (index(febe(ifb)%header%febe,'NBC').ne.0) then
      backendName = 'NBC' 
   else if (index(febe(ifb)%header%febe,'4MHZ').ne.0) then
      backendName = '4MHZ' 
   else if (index(febe(ifb)%header%febe,'1MHZ').ne.0) then
      backendName = '1MHZ' 
   else if (index(febe(ifb)%header%febe,'100KHZ').ne.0) then
      backendName = '100KHZ' 
   else if (index(febe(ifb)%header%febe,'WILMA').ne.0) then
      backendName = 'WILMA'
   endif

   k1 = index(febe(ifb)%header%febe,'/')+1
   if (k1.eq.1) then
      idPart = 1
   else
      k2 = len_trim(febe(ifb)%header%febe)
      read(febe(ifb)%header%febe(k1:k2),'(i3)') idPart
   endif   

   if (index(backendName,'continuum').eq.0) then
      bandwidth = real(array(ifb)%header(1)%cd4f_21                    &
        &              *data(ifb)%header%channels)*1.e-6
   else
      bandwidth = 1000. !need to distinguish here bb mode and nb mode
   endif

   Call resultsToNCSsetBackend(                                              &
        &       name  = backendName,                                         &
        &       nPart = idPart,                                              &
        &       resolution = abs(real(array(ifb)%header(1)%freqres))*1.e-6,  &
        &       bandwidth  = bandwidth,                                      &
        &       fShift = real(offset),                                       &
        &       receiver = frontendName(1:LF),                               &
        &       nChannels = data(ifb)%header%channels,                       &
        &       reset     = resetBe,                                         &
        &       error = lError                                               &
        &       )

   Call resultsToNCSwriteBackends(error = lError) 

   peakValue = real(2*fun(1)%par(1)%value                              &
        &      /(fun(1)%par(3)%value*sqrt(pi/log(2.d0))))       
   peakError = real(2.*sqrt(log(2.d0)/pi)/fun(1)%par(3)%value          &
        &          *sqrt(fun(1)%par(1)%error**2                        &
        &                +(fun(1)%par(1)%value*fun(1)%par(3)%error     &
        &                  /fun(1)%par(3)%value)**2                    &
        &                )                                             &
        &          )     

  
   correction = mon(ifb)%header%iaobs_caobs_ieobs(2)*3.6d3+fun(1)%par(2)%value 
   pointingOffset = fun(1)%par(2)%value
   Call resultsToNCSsetPointingResults(                                &
        &       backendName = backendName,                             &
        &       nPart       = idPart,                                  &
        &       direction   = "azimuth",                               &
        &       correction  = resultT(correction,fun(1)%par(2)%error), &
        &       peak        = resultT(peakValue,peakError),            &
        &       integral    = resultT(fun(1)%par(1)%value,             &
        &                             fun(1)%par(1)%error),            &
        &       offset      = resultT(pointingOffset,                  &
        &                             fun(1)%par(2)%error),            &
        &       width       = resultT(fun(1)%par(3)%value,             &
        &                             fun(1)%par(3)%error),            &
        &       rmsResidual = real(fun(1)%rms),                        &
        &       reset       = .False.,                                 &
        &       error       = lError                                   &
        &       )

   peakValue = real(2*fun(2)%par(1)%value                              &
        &      /(fun(2)%par(3)%value*sqrt(pi/log(2.d0))))       
   peakError = real(2.*sqrt(log(2.d0)/pi)/fun(2)%par(3)%value          &
        &          *sqrt(fun(2)%par(1)%error**2                        &
        &                +(fun(2)%par(1)%value*fun(2)%par(3)%error     &
        &                  /fun(2)%par(3)%value)**2                    &
        &                )                                             &
        &          )     

   correction = mon(ifb)%header%iaobs_caobs_ieobs(3)*3.6d+3+fun(2)%par(2)%value 
   pointingOffset = fun(2)%par(2)%value
   Call resultsToNCSsetPointingResults(                                &
        &       backendName = backendName,                             &
        &       nPart       = idPart,                                  &
        &       direction   = "elevation",                             &
        &       correction  = resultT(correction,                      &
        &                             fun(2)%par(2)%error),            &
        &       peak        = resultT(peakValue,peakError),            &
        &       integral    = resultT(fun(2)%par(1)%value,             &
        &                             fun(2)%par(1)%error),            &
        &       offset      = resultT(pointingOffset,                  &
        &                             fun(2)%par(2)%error),            &
        &       width       = resultT(fun(2)%par(3)%value,             &
        &                             fun(2)%par(3)%error),            &
        &       rmsResidual = real(fun(2)%rms),                        &
        &       reset       = .False.,                                 &
        &       error       = lError                                   &
        &       )

   Call resultsToNCSwritePointingResults(error = lError)

   Call resultsToNCSwriteMeasurementEnd(error = lError)

   Call resultsToNCSwriteEnd(error = lError)

   Close(Unit=lun)
   ier = gag_frelun(lun)

   Call Message(2,1,'protoResultsPointing',' finished writing')

   return

END SUBROUTINE protoResultsPointing
