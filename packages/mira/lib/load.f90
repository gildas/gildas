subroutine load_mira
!
! Called by driver routine mira/main/mira.f90 
!

   !
   use mira
   !
   implicit none
   !
   integer mcom
   parameter(mcom=90)
   character vers*50
   parameter(vers='2.6 03-SEP-2015 H.Wiesemeyer, A.Sievers')
   character*12 ccom(mcom)
   external gr_error,run_mira

   data ccom /   &
     ' FILE','/CLASS','/MBFITS','/SHOW',   &
     ' FLAG','/BASEBAND','/CHANNEL','/PIXEL','/RECORD',   &
     '/SUBSCAN',   &
     ' LIST','/OUTPUT','/LONG','/PROJECT','/REDUCE','/FLUX',   &
     '/FORMAT','/FLAG',   &
     ' FIND','/BACKEND','/FRONTEND','/LINE','/OBSERVED',   &
     '/PROCEDURE','/SCAN','/SOURCE','/TELESCOPE','/NEW',   &
     '/SWITCHMODE','/SILENT','/WHAT','/PROJECT','/STAT',   &
     ' CAL','/GAINS','/TCAL','/OFF','/PHASE','/MASK',   &
     '/FEBECAL', &
     ' SCAN','/TRACKING','/COMPRESS','/TAU','/PWV','/SUBSCAN', &
     '/BACKEND','/DROP', &
     ' SOLVE','/PIXEL','/BINNING','/BASE',   &
     ' VIEW','/CAL', '/GAINS', '/SIGNAL', '/PHASES', '/MAP',   &
     '/PIXEL','/ZOOM','/XPOL','/TRACES','/OTF',   &
     '/DEROTATOR','/TREC','/SATURATION', &
     ' SUPPORT','/CURSOR',   &
     ' BASE','/ORDER','/OFF','/INTERPOLATE',   &
     ' DECONVOLVE',   &
     ' DESPIKE','/PIXEL','/ITERATE','/WINDOW','/THRESHOLD',   &
     ' GRID','/KERNEL','/CHANNEL','/PRECESS',   &
     ' VARIABLE',   &
     ' WRITE','/FEBE','/PIXEL','/SUBSCAN',   &
     ' OVERRIDE','/FEBE','/RESET'   &
     /
   call sic_begin('MIRA','GAG_HELP_MIRA',mcom,ccom,vers,   &
                  run_mira,gr_error) 
   call init

end subroutine load_mira
