subroutine flagData(line,error)
!
! Used for attributing the blanking value of raw data (blankingRaw), 
! respectively calibrated data (blankingRed) for a given spectral
! channel, receiver pixel, record, or spectral baseband.
!
! Called by run_mira (interactive command: FLAG, see online help or 
! users' manual for options).
!
! Input:        line    character       command line in interactive MIRA session
! Output:       error   logical         error flag
!
!

 USE mira
 IMPLICIT none

 CHARACTER(LEN=*)                   :: LINE
 LOGICAL                            :: ERROR

 integer                            :: i, ier, ifb, ifb1, ifb2, ii, j, jj, k,  &
&                                      kk, l, ll, nbb, nc, nchan, npix, nrec,  &
&                                      nsub, sic_narg
 integer, dimension(:), allocatable :: ibb, ichan, ipix, irec, isub
 real                               :: bval
 character(len=3)                   :: string
 logical                            :: flagBaseband,flagChannel,flagPixel,     &
&                                      flagRecord,flagSubscan, sic_present

 if (.not.sic_present(0,1)) then
    call gagout('E-FLAG: please specify a frontend-backend combination or ALL.')
    error = .true.
    return
 else
    call sic_ch(line,0,1,string,nc,.false.,error)
    call sic_upper(string)
    if (index(string(1:1),'A').ne.0) then
       ifb1 = 1
       ifb2 = scan%header%nfebe
    else
       call sic_i4(line,0,1,ifb,.false.,error)
       ifb1 = ifb
       ifb2 = ifb
    endif
 endif

 febeLoop: do ifb = ifb1, ifb2

    do i = 1, 5
       if (sic_present(i,0).and..not.sic_present(i,1)) then
          call gagout('E-FLAG: please specify a number.')
          error = .true.
          return
       endif
    enddo
 
    flagBaseband = sic_present(1,1)

    if (reduce(ifb)%calibration_done(3).and.flagBaseband) then
       call gagout('E-FLAG: individual basebands cannot be flagged at this '&
&                  //'stage, since the basebands are already concatenated.')
       error = .true.
       return
    endif
    
    flagChannel  = sic_present(2,1)
    flagPixel    = sic_present(3,1)
    flagRecord   = sic_present(4,1)
    flagSubscan  = sic_present(5,1)

    nbb      = 0
    nchan    = 0
    npix     = 0
    nrec     = 0
    nsub     = 0

    if (flagBaseband) then
       nbb = sic_narg(1)
       allocate(ibb(nbb),stat=ier)
       do i = 1, nbb
          call sic_i4(line,1,i,ibb(i),.false.,error)
          if (ibb(i).gt.febe(ifb)%header%febeband) then
             call gagout('E-FLAG: invalid baseband number.')
             error = .true.
             return
          endif
       enddo
    else if (.not.reduce(ifb)%calibration_done(3)) then
       nbb = febe(ifb)%header%febeband
       allocate(ibb(nbb),stat=ier)
       do i = 1, nbb
          ibb(i) = i
       enddo
    endif

    if (flagChannel) then
       nchan = sic_narg(2)
       allocate(ichan(nchan),stat=ier)
       do i = 1, nchan
          call sic_i4(line,2,i,ichan(i),.false.,error)
          if (reduce(ifb)%calibration_done(3)) then
             if (ichan(i).gt.data(ifb)%header%channels) then
                call gagout('E-FLAG: invalid channel number.')
                error = .true.
                return
             endif
          else
             if (ichan(i).gt.array(ifb)%header(1)%channels) then
                call gagout('E-FLAG: invalid channel number.')
                error = .true.
                return
             endif
          endif
       enddo
    else
       nchan = array(ifb)%header(1)%channels
       allocate(ichan(nchan),stat=ier)
       do i = 1, nchan
          ichan(i) = i
       enddo
    endif

    if (flagPixel) then
       npix = sic_narg(3)
       allocate(ipix(npix),stat=ier)
       do i = 1, npix
          call sic_i4(line,3,i,ipix(i),.false.,error)
          if (ipix(i).gt.febe(ifb)%header%febefeed) then
             call gagout('E-FLAG: invalid pixel number.')
             error = .true.
             return
          endif
       enddo
    else
       npix = febe(ifb)%header%febefeed
       allocate(ipix(npix),stat=ier)
       do i = 1, npix
          ipix(i) = i
       enddo
    endif

    if (flagRecord) then
       nrec = sic_narg(4)
       allocate(irec(nrec),stat=ier)
       do i = 1, nrec
          call sic_i4(line,4,i,irec(i),.false.,error)
          if (irec(i).gt.maxval(data(ifb)%data%integnum)) then
             call gagout('E-FLAG: invalid record number.')
             error = .true.
             return
          endif
       enddo
    else if (.not.flagSubscan) then
       nrec = maxval(data(ifb)%data%integnum)
       allocate(irec(nrec),stat=ier)
       do i = 1, nrec
          irec(i) = i
       enddo
    endif

    if (flagSubscan) then

       if (flagRecord) then
         call gagout('W-FLAG: options /RECORD and /SUBSCAN are '//     &
&                    'mutually exclusive. Ignoring option /RECORD.')
         if (allocated(irec)) deallocate(irec)
       endif

    endif

    nsub = sic_narg(5)
    allocate(isub(nsub),stat=ier)
    do i = 1, nsub
       call sic_i4(line,5,i,isub(i),.false.,error)
       if (isub(i).gt.scan%header%nobs) then
          call gagout('E-FLAG: invalid subscan number.')
          error = .true.
          return
       endif
    enddo

    flagRecord = .true.
    nrec = 0
    do i = 1, nsub
       ii = isub(i)
       nrec = nrec+count(data(ifb)%data%subscan(:).eq.ii)
    enddo
    allocate(irec(nrec),stat=ier)
    k = 0
    do i = 1, nsub
       ii = isub(i)
       do j = 1, maxval(data(ifb)%data%integnum)
          if (ii.eq.data(ifb)%data%subscan(j)) then
             k = k+1
             irec(k) = data(ifb)%data%integnum(j)
          endif
       enddo
    enddo

    flagSubscan = .false.
    if (reduce(ifb)%calibration_done(3)) then   

       bval = blankingRed

       do i = 1, nchan
          ii = ichan(i)
          do j = 1, npix
             jj = ipix(j)
             data(ifb)%data%rdata(jj,ii) = bval
             do k = 1, nrec
                kk = irec(k)
                data(ifb)%data%otfdata(jj,ii,kk) = bval
             enddo
          enddo
       enddo

    else

       if (.not.reduce(ifb)%calibration_done(1)) then
          bval = blankingRaw
       else
          bval = blankingRed
       endif

       do i = 1, nbb
          ii = ibb(i)
          do j = 1, nchan
             jj = ichan(j)
             do k = 1, npix
                kk = ipix(k)
                do l = 1, nrec
                   ll = irec(l)
                   array(ifb)%data(ii)%data(kk,jj,ll,:) = bval
                enddo
             enddo
          enddo
       enddo
       
    endif

 enddo febeLoop

 if (allocated(ibb))   deallocate(ibb,stat=ier)
 if (allocated(ichan)) deallocate(ichan,stat=ier)
 if (allocated(ipix))  deallocate(ipix,stat=ier)
 if (allocated(irec))  deallocate(irec,stat=ier)

end subroutine flagData


