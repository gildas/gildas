subroutine classic_init(error)
  use classic_vars
  !---------------------------------------------------------------------
  ! @ public
  !  Initialize the Classic library. This should be done only once at
  ! startup. The library will refuse opening a Classic file as long
  ! as this has not been done.
  !---------------------------------------------------------------------
  logical, intent(inout) :: error  ! Logical error flag
  !
  call classic_code
  !
  classic_initialized = .true.
  !
end subroutine classic_init
!
subroutine classic_code
  use gkernel_interfaces
  use classic_vars
  !---------------------------------------------------------------------
  ! @ private
  !   Set the identification codes for the current system.
  !---------------------------------------------------------------------
  character(len=4) :: csyst,ccode_file_v2
  character(len=4) :: ccode_file_v1_multiple,ccode_file_v1_single
  !
  ! Get hardware code
  call gdf_getcod(csyst)
  if (csyst.eq.'VAX_') then
    ccode_file_v1_multiple = '1   '
    ccode_file_v1_single = '9   '
    ccode_file_v2 = '2   '
  elseif (csyst.eq.'IEEE') then
    ccode_file_v1_multiple = '1A  '
    ccode_file_v1_single = '9A  '
    ccode_file_v2 = '2A   '
  elseif (csyst.eq.'EEEI') then
    ccode_file_v1_multiple = '1B  '
    ccode_file_v1_single = '9B  '
    ccode_file_v2 = '2B  '
  endif
  call chtoby(ccode_file_v1_single,code_file_v1_single,4)
  call chtoby(ccode_file_v1_multiple,code_file_v1_multiple,4)
  call chtoby(ccode_file_v2,code_file_v2,4)
  call chtoby('2   ',code_entry,4)
end subroutine classic_code
!
subroutine classic_convcod(filecode,lversion,lsingle,lpdp11,convcode,error)
  use gbl_convert
  use gbl_message
  use gkernel_interfaces
  use classic_interfaces, except_this=>classic_convcod
  use classic_vars
  !---------------------------------------------------------------------
  ! @ private
  !   Return the conversion code from File Data Type to System Data
  ! Type, and the extra informations which can be deduced from the File
  ! code.
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)    :: filecode  ! File code
  integer(kind=4), intent(out)   :: lversion  ! File version
  logical,         intent(out)   :: lsingle   ! Single or multiple?
  logical,         intent(out)   :: lpdp11    ! Old PDP 11 system?
  integer(kind=4), intent(out)   :: convcode  ! Conversion code
  logical,         intent(inout) :: error     ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='CONVERT'
  integer(kind=4) :: sever,scode  ! System code
  character(len=4) :: csyst,cfile
  character(len=20) :: conversion
  character(len=1), parameter :: cv1m='1'  ! Version 1, multiple
  character(len=1), parameter :: cv1s='9'  ! Version 1, single
  character(len=1), parameter :: cv2='2'   ! Version 2, any kind
  !
  scode = code_file_v2
  convcode = unknown
  lpdp11 = .false.
  lsingle = .false.
  !
  if (filecode.eq.scode) then
    ! File is Native + Version2
    lversion = 2
    convcode = 0
  else
    ! Must parse the code
    call bytoch(scode,csyst,4)
    call bytoch(filecode,cfile,4)
    !
    if (cfile(1:1).eq.cv2) then
      ! File is V2
      lversion = 2
    elseif (cfile(1:1).eq.cv1m) then
      ! File is V1 multiple
      lversion = 1
      lsingle = .false.
    elseif (cfile(1:1).eq.cv1s) then
      ! File is V1 single
      lversion = 1
      lsingle = .true.
    else
      ! Nothing known
      call classic_message(seve%e,rname,'Non-standard file')
      error = .true.
      return
    endif
    !
    ! Then find the File coding system
    cfile(1:1) = '1'  ! Switch the first byte
    if (cfile.eq.'1   ') then
      cfile = 'VAX_'
    elseif (cfile.eq.'1A  ') then
      cfile = 'IEEE'
    elseif (cfile.eq.'1B  ') then
      cfile = 'EEEI'
    elseif (lversion.eq.1 .and. cfile(1:2).eq.'1 ') then
      ! Old PDP 11 format
      cfile = 'VAX_'
      lpdp11 = .true.
    else
      cfile = '?...'
    endif
    !
    csyst(1:1) = '1'  ! Switch the first byte
    if (csyst.eq.'1   ') then
      csyst = 'VAX_'
    elseif (csyst.eq.'1A  ') then
      csyst = 'IEEE'
    elseif (csyst.eq.'1B  ') then
      csyst = 'EEEI'
    endif
    !
    call gdf_convcod(cfile,csyst,convcode)
  endif
  !
  call gdf_conversion(convcode,conversion)
  if (index(conversion,'Native').gt.0) then
    ! Native: only debug
    sever = seve%d
  else
    ! Non-native: info
    sever = seve%i
  endif
  call classic_message(sever,'CONVERT','File is '//conversion)
  !
end subroutine classic_convcod
!
subroutine classic_conv(conv,error)
  use gildas_def
  use gbl_convert
  use gbl_message
  use classic_interfaces, except_this=>classic_conv
  use classic_types
  !---------------------------------------------------------------------
  ! @ private
  !  Set up the file conversion routines: read/write file-data-type
  ! to/from the system-data-type conversion routines.
  !---------------------------------------------------------------------
  type(classic_fileconv_t), intent(inout) :: conv   !
  logical,                  intent(inout) :: error  ! Logical error flag
  ! Global
  external :: r4tor4,r8tor8,i4toi4,i8toi8
  external :: ier4va,eir4va,eir4ie,ier4ei,var4ie,var4ei
  external :: ier8va,eir8va,eir8ie,ier8ei,var8ie,var8ei
  external ::        eii4va,eii4ie,iei4ei       ,vai4ei
  external ::               eii8ie,iei8ei
  external :: w4toch,chtow4
  ! Local
  character(len=*), parameter :: rname='CONV'
  !
  if (conv%code.eq.0) then  ! No conversion
     conv%read%r4 => r4tor4
     conv%read%r8 => r8tor8
     conv%read%i4 => i4toi4
     conv%read%i8 => i8toi8
     conv%read%cc => w4toch
     conv%writ%r4 => r4tor4
     conv%writ%r8 => r8tor8
     conv%writ%i4 => i4toi4
     conv%writ%i8 => i8toi8
     conv%writ%cc => chtow4
  elseif (conv%code.eq.vax_to_ieee) then
     conv%read%r4 => var4ie
     conv%read%r8 => var8ie
     conv%read%i4 => i4toi4
     conv%read%i8 => i8toi8  ! Unsupported
     conv%read%cc => w4toch
     conv%writ%r4 => ier4va
     conv%writ%r8 => ier8va
     conv%writ%i4 => i4toi4
     conv%writ%i8 => i8toi8  ! Unsupported
     conv%writ%cc => chtow4
  elseif (conv%code.eq.ieee_to_vax) then
     conv%read%r4 => ier4va
     conv%read%r8 => ier8va
     conv%read%i4 => i4toi4
     conv%read%i8 => i8toi8  ! Unsupported
     conv%read%cc => w4toch
     conv%writ%r4 => var4ie
     conv%writ%r8 => var8ie
     conv%writ%i4 => i4toi4
     conv%writ%i8 => i8toi8  ! Unsupported
     conv%writ%cc => chtow4
  elseif (conv%code.eq.vax_to_eeei) then
     conv%read%r4 => var4ei
     conv%read%r8 => var8ei
     conv%read%i4 => vai4ei
     conv%read%i8 => i8toi8  ! Unsupported
     conv%read%cc => w4toch
     conv%writ%r4 => eir4va
     conv%writ%r8 => eir8va
     conv%writ%i4 => eii4va
     conv%writ%i8 => i8toi8  ! Unsupported
     conv%writ%cc => chtow4
  elseif (conv%code.eq.ieee_to_eeei) then
     conv%read%r4 => ier4ei
     conv%read%r8 => ier8ei
     conv%read%i4 => iei4ei
     conv%read%i8 => iei8ei
     conv%read%cc => w4toch
     conv%writ%r4 => eir4ie
     conv%writ%r8 => eir8ie
     conv%writ%i4 => eii4ie
     conv%writ%i8 => eii8ie
     conv%writ%cc => chtow4
  elseif (conv%code.eq.eeei_to_vax) then
     conv%read%r4 => eir4va
     conv%read%r8 => eir8va
     conv%read%i4 => eii4va
     conv%read%i8 => i8toi8  ! Unsupported
     conv%read%cc => w4toch
     conv%writ%r4 => var4ei
     conv%writ%r8 => var8ei
     conv%writ%i4 => vai4ei
     conv%writ%i8 => i8toi8  ! Unsupported
     conv%writ%cc => chtow4
  elseif (conv%code.eq.eeei_to_ieee) then
     conv%read%r4 => eir4ie
     conv%read%r8 => eir8ie
     conv%read%i4 => eii4ie
     conv%read%i8 => eii8ie
     conv%read%cc => w4toch
     conv%writ%r4 => ier4ei
     conv%writ%r8 => ier8ei
     conv%writ%i4 => iei4ei
     conv%writ%i8 => iei8ei
     conv%writ%cc => chtow4
  else
     call classic_message(seve%e,rname,'Unsupported conversion')
     error = .true.
     return
  endif
  !
end subroutine classic_conv
