module classic_params
  !---------------------------------------------------------------------
  ! @ public
  !  Generic Data Format public parameters
  !---------------------------------------------------------------------
  integer(kind=4), parameter :: entry_length=8  ! Entry number or number of entries
  integer(kind=4), parameter :: data_length=8   ! Length of data arrays (data, sections, ...)
  !
  integer(kind=4), parameter :: classic_kind_demo=-1  ! For testing purpose only!
  integer(kind=4), parameter :: classic_kind_unknown=0
  integer(kind=4), parameter :: classic_kind_class=1
  integer(kind=4), parameter :: classic_kind_clic=2
  integer(kind=4), parameter :: classic_kind_mrtcal=3
  !
end module classic_params
!
module classic_types
  use gildas_def
  !---------------------------------------------------------------------
  ! @ public
  ! Support file for the Classic Data Format
  !---------------------------------------------------------------------
  !
  integer(kind=4), parameter :: classic_reclen_v1=128  ! V1 record length, in words
  integer(kind=4), parameter :: classic_vind_v1=1      ! V1 Index Version
  integer(kind=4), parameter :: classic_lind_v1=32     ! V1 Index Length, in words
  integer(kind=4), parameter :: classic_vobs_v1=1      ! V1 Observation Version
  !
  ! --- The reading/writing buffer type --------------------------------
  type classic_recordbuf_t
    ! Object descriptor
    integer(kind=8) :: rstart  ! The record where the object starts
    integer(kind=4) :: wstart  ! In this record, the word where the object starts
    integer(kind=8) :: nrec    ! Number of records used by the object
    ! Reading buffer
    integer(kind=4) :: lun     ! Associated file unit
    integer(kind=8) :: roff    ! Offset record from reference
    integer(kind=4) :: len     ! Size of the 'data' buffer (i.e. size of associated record)
    integer(kind=4), allocatable :: data(:)  ! The buffer
  end type classic_recordbuf_t
  !
  ! --- The File Conversion type ---------------------------------------
  type :: subrconv_t
     ! Routines for system-data-type <-> file-data-type conversions
     procedure(), nopass, pointer :: i4 => null()  ! for integer*4 values
     procedure(), nopass, pointer :: i8 => null()  ! for integer*8 values
     procedure(), nopass, pointer :: r4 => null()  ! for real*4 values
     procedure(), nopass, pointer :: r8 => null()  ! for real*8 values
     procedure(), nopass, pointer :: cc => null()  ! for character*(*) values
  end type subrconv_t
  !
  type :: classic_fileconv_t
     integer(kind=4) :: code   ! Conversion code
     type(subrconv_t) :: read  ! Reading routines (from file to memory)
     type(subrconv_t) :: writ  ! Writing routines (from memory to file)
  end type classic_fileconv_t
  !
  ! --- The File Descriptor type --------------------------------------------
  integer(kind=4), parameter :: mex_v1=251
  type filedesc_v1_t
     sequence
     integer(kind=4) :: code         ! 1     File code
     integer(kind=4) :: next         ! 2     Next free record
     integer(kind=4) :: lex          ! 3     Extension length (number of entries)
     integer(kind=4) :: nex          ! 4     Number of extensions
     integer(kind=4) :: xnext        ! 5     Next available entry number
     integer(kind=4) :: aex(mex_v1)  ! 6:256 Extension addresses
  end type filedesc_v1_t
  !
  integer(kind=4) :: filedescv2_nw1=14  ! Number of words, in 1st part
  type classic_filedesc_t
     sequence  ! Not really needed since we always access these elements one by one
     integer(kind=4)              :: code     ! 1             File code
     integer(kind=4)              :: reclen   ! 2             Record length
     integer(kind=4)              :: kind     ! 3             File kind
     integer(kind=4)              :: vind     ! 4             Index version
     integer(kind=4)              :: lind     ! 5             Index length
     integer(kind=4)              :: flags    ! 6 (bit #1)    Single or multiple?
                                              ! 6 (bits 2:32) Provision (0-filled)
     integer(kind=8)              :: xnext    ! 7:8           Next available entry number
     integer(kind=8)              :: nextrec  ! 9:10          Next record which contains free space
     integer(kind=4)              :: nextword ! 11            Next available word in this record
     integer(kind=4)              :: lex1     ! 12            Length of first extension (number of entries)
     integer(kind=4)              :: nex      ! 13            Number of extensions
     integer(kind=4)              :: gex      ! 14            Extension growth rule
     integer(kind=8), allocatable :: aex(:)   ! 15:reclen     Extension addresses
     ! Not in data:
     integer(kind=4)              :: version  ! 1 or 2  (duplicate of file code)
     logical                      :: single   ! Obs numbering mode (duplicate of 'flags', bit #1)
     integer(kind=4)              :: mex      ! Size of aex(:) and lexN(:)
     integer(kind=4)              :: pad1     ! Memory padding
     integer(kind=8), allocatable :: lexN(:)  ! Extension lengths (cumulative, number of entries)
  end type classic_filedesc_t
  !
  ! --- The Entry Descriptor type -------------------------------------
  integer(kind=4), parameter :: classic_maxsec=64  ! Maximum number of sections
    ! the Classic library can handle (used for arrays holding the section
    ! descriptions in memory, not on disk, i.e. could be anything else reasonable)
  !
  ! Version 1 (private)
  integer(kind=4), parameter :: entrydescv1_nw1=9  ! Number of words, in 1st part
  integer(kind=4), parameter :: entrydescv1_nw2=3  ! Number of words for 1 section in 2nd part
  type entrydesc_v1_t
     ! note that the "dummies" are needed to match the file format...
     sequence
     integer(kind=4) :: code    ! 1: code observation icode
     integer(kind=4) :: nbloc   ! 2: number of records
     integer(kind=4) :: nword   ! 3: number of words
     integer(kind=4) :: dumm4   ! 4: unused
     integer(kind=4) :: adata   ! 5: data address
     integer(kind=4) :: ldata   ! 6: data length
     integer(kind=4) :: dumm7   ! 7: unused
     integer(kind=4) :: nsec    ! 8: number of sections
     integer(kind=4) :: xnum    ! 9: entry number
     ! Out of the 'sequence' block:
     integer(kind=4) :: msec    ! Not in data: maximum number of sections the
                                ! Observation Index can hold
     integer(kind=4) :: seciden(classic_maxsec)  ! Section Numbers (on disk: 1 to ed%nsec)
     integer(kind=4) :: secleng(classic_maxsec)  ! Section Lengths (on disk: 1 to ed%nsec)
     integer(kind=4) :: secaddr(classic_maxsec)  ! Section Addresses (on disk: 1 to ed%nsec)
  end type entrydesc_v1_t
  !
  ! Version 2 (public)
  integer(kind=4), parameter :: entrydescv2_nw1=11  ! Number of words, in 1st part
  integer(kind=4), parameter :: entrydescv2_nw2=5   ! Number of words for 1 section in 2nd part
  type classic_entrydesc_t
     sequence
     integer(kind=4) :: code     !  1   : code observation icode
     integer(kind=4) :: version  !  2   : observation version
     integer(kind=4) :: nsec     !  3   : number of sections
     integer(kind=4) :: pad1     !  -   : memory padding (not in data)
     integer(kind=8) :: nword    !  4- 5: number of words
     integer(kind=8) :: adata    !  6- 7: data address
     integer(kind=8) :: ldata    !  8- 9: data length
     integer(kind=8) :: xnum     ! 10-11: entry number
     ! Out of the 'sequence' block:
     integer(kind=4) :: msec     ! Not in data: maximum number of sections the
                                 ! Observation Index can hold
     integer(kind=4) :: pad2     ! Memory padding for 8 bytes alignment
     integer(kind=4) :: seciden(classic_maxsec)  ! Section Numbers (on disk: 1 to ed%nsec)
     integer(kind=8) :: secleng(classic_maxsec)  ! Section Lengths (on disk: 1 to ed%nsec)
     integer(kind=8) :: secaddr(classic_maxsec)  ! Section Addresses (on disk: 1 to ed%nsec)
  end type classic_entrydesc_t
  !
  ! --- Type which describe the CLASSIC files ----------------------------
  type classic_file_t
     character(len=256)       :: spec=''    ! File name
     integer(kind=4)          :: nspec=0    ! Filename length
     integer(kind=4)          :: lun=0      ! Logical unit
     logical                  :: readwrite  ! Read-only or Read-Write?
     type(classic_filedesc_t) :: desc       ! File descriptor
     logical                  :: update     ! Is file opened for Update ?
     type(classic_fileconv_t) :: conv       ! Conversion routines
  end type classic_file_t
  !
  ! --- Index Version 2 ------------------------------------------------
  ! This is a dummy kind (indexes are the property of the application),
  ! which is used to read (mostly for debugging purpose) the 2 first
  ! words in a V2 index.
  type indx_basic_v2_t  ! private
    integer(kind=8) :: rec   ! Starting record of the entry
    integer(kind=4) :: word  ! In this record, starting word of the entry
  end type indx_basic_v2_t
  !
end module classic_types
!
module classic_vars
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  !
  ! Initialization flag
  logical :: classic_initialized=.false.
  !
  ! Identification codes (set once at startup for the current system)
  integer(kind=4) :: code_file_v1_single    ! Classic file Version 1 identifier, kind single
  integer(kind=4) :: code_file_v1_multiple  ! Classic file Version 1 identifier, kind multiple
  integer(kind=4) :: code_file_v2           ! Classic file Version 2 identifier, any kind
  integer(kind=4) :: code_entry             ! Entry identifier
  !
end module classic_vars
!
module toc_types
  use gildas_def
  use classic_params
  !---------------------------------------------------------------------
  ! Support module for Table-Of-Contents of Classic indexes
  !---------------------------------------------------------------------
  !
  integer(kind=4), parameter :: toc_ftype_i4_1d  =1  ! 1D data
  integer(kind=4), parameter :: toc_ftype_i8_1d  =2  !
  integer(kind=4), parameter :: toc_ftype_r4_1d  =3  !
  integer(kind=4), parameter :: toc_ftype_r8_1d  =4  !
  integer(kind=4), parameter :: toc_ftype_c8_1d  =5  !
  integer(kind=4), parameter :: toc_ftype_c12_1d =6  !
  integer(kind=4), parameter :: toc_ftype_i4_2d  =7  ! 2D data
  integer(kind=4), parameter :: toc_ftype_c8_2d  =8  !
  integer(kind=4), parameter :: toc_ftype_c128_1d=9  !
  !
  type :: pointer_typei4_t
    integer(kind=4)            :: ndim
    integer(kind=4),   pointer :: data1(:)
    integer(kind=4),   pointer :: data2(:,:)
  end type pointer_typei4_t
  type :: pointer_typei8_t
    integer(kind=8),   pointer :: data1(:)
  end type pointer_typei8_t
  type :: pointer_typer4_t
    real(kind=4),      pointer :: data1(:)
  end type pointer_typer4_t
  type :: pointer_typer8_t
    real(kind=8),      pointer :: data1(:)
  end type pointer_typer8_t
  type :: pointer_typec8_t
    integer(kind=4)            :: ndim
    character(len=8),  pointer :: data1(:)
    character(len=8),  pointer :: data2(:,:)
  end type pointer_typec8_t
  ! This deferred-length string array pointer should replace all explicit-length
  ! string array pointers. But gfortran<=11 has a bug (values, string lengths
  ! are not seen correctly) when such pointers are included in derived type. To
  ! be implemented in the future, when the bug is fixed.
  ! type :: pointer_typec0_t
  !   character(len=:), pointer :: data1(:)
  ! end type pointer_typec0_t
  type :: pointer_typec12_t
    character(len=12), pointer :: data1(:)
  end type pointer_typec12_t
  type :: pointer_typec128_t
    character(len=128), pointer :: data1(:)
  end type pointer_typec128_t
  !
  type toc_pointers_t
    ! The working pointers, stored as array of pointers for each kind of data.
    type(pointer_typei4_t),   allocatable :: i4(:)
    type(pointer_typei8_t),   allocatable :: i8(:)
    type(pointer_typer4_t),   allocatable :: r4(:)
    type(pointer_typer8_t),   allocatable :: r8(:)
    type(pointer_typec8_t),   allocatable :: c8(:)
    type(pointer_typec12_t),  allocatable :: c12(:)
    type(pointer_typec128_t), allocatable :: c128(:)
    ! Counters for the current selection. Counts how many array of
    ! each type is in use for this selection
    integer(kind=4) :: ni4
    integer(kind=4) :: ni8
    integer(kind=4) :: nr4
    integer(kind=4) :: nr8
    integer(kind=4) :: nc8
    integer(kind=4) :: nc12
    integer(kind=4) :: nc128
  end type toc_pointers_t
  !
  type generic_pointer_t
    type(pointer_typei4_t),   pointer :: i4
    type(pointer_typei8_t),   pointer :: i8
    type(pointer_typer4_t),   pointer :: r4
    type(pointer_typer8_t),   pointer :: r8
    type(pointer_typec8_t),   pointer :: c8
    type(pointer_typec12_t),  pointer :: c12
    type(pointer_typec128_t), pointer :: c128
  end type generic_pointer_t
  !
  type toc_selection_t
    integer(kind=4)                         :: nkey      ! Number of keys used for this selection
    integer(kind=entry_length)              :: nequ      ! Number of equiv classes (toc_nsour)
    integer(kind=entry_length), allocatable :: cnt(:)    ! (nequ) Number of elements in each equiv class (toc_msour)
    integer(kind=entry_length), allocatable :: bak(:)    ! (nequ) Back pointer to 1st element found in each class
    character(len=128),         allocatable :: nam(:,:)  ! (nequ,nkey) Name of each equiv class (toc_sour)
  end type toc_selection_t
  !
  ! Descriptor of one field of the index
  type :: toc_descriptor_t
    ! To be filled once for each key by the application (public):
    character(len=16)       :: keyword       ! e.g. SOUR
    character(len=16)       :: sic_var_name  ! e.g. sour
    character(len=12)       :: human_name    ! e.g. source
    character(len=80)       :: message       ! e.g. Number of sources...
    integer(kind=4)         :: ftype         ! Fortran type (see codes 'toc_ftype_*')
    integer(kind=4)         :: ptype         ! Custom program type (e.g. a gag_date needs special decoding)
    ! To be reassociated to the index arrays by the application at each call (public):
    type(generic_pointer_t) :: ptr           ! Convenient shortcut to 'ptrs' (if only one)
    ! Internal support pointers (private)
    type(toc_pointers_t)    :: ptrs          ! Pointer to data
    ! Internal working buffer for the current selection (private)
    type(toc_selection_t)   :: select        ! Selection
  end type toc_descriptor_t
  !
  type toc_t
    ! Support for each key (single equivalence classes) (public)
    logical :: initialized=.false.
    integer(kind=4)                     :: nkey     ! Number of keys
    type(toc_descriptor_t), allocatable :: keys(:)  ! Keys description
    ! Support for all keys (combined equivalence classes) (private)
    type(toc_descriptor_t) :: all
  end type toc_t
  !
end module toc_types
