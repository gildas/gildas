module classic_interfaces
  !---------------------------------------------------------------------
  ! @ private
  ! CLASSIC interfaces, all kinds (private or public). Do not use
  !  directly out of the library.
  !---------------------------------------------------------------------
  !
  use classic_interfaces_public
  use classic_interfaces_private
  !
end module classic_interfaces
!
module classic_api
  !---------------------------------------------------------------------
  ! @ public
  !   CLASSIC public interfaces, parameters, and types. Can be used by
  ! external programs
  !---------------------------------------------------------------------
  use classic_params
  use classic_types
  use classic_interfaces_public
end module classic_api
