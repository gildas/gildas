!-----------------------------------------------------------------------
! Support file for Table-Of-Contents of Classic indexes
!-----------------------------------------------------------------------
subroutine toc_init_pointers(toc,error)
  use gkernel_interfaces
  use toc_types
  !---------------------------------------------------------------------
  ! @ public
  !  Initialize the pointers arrays
  !---------------------------------------------------------------------
  type(toc_t), intent(inout), target :: toc    ! Table-Of-Content description
  logical,     intent(inout)         :: error  !
  ! Local
  character(len=*), parameter :: rname='TOC'
  integer(kind=4) :: ier,ikey
  type(toc_descriptor_t), pointer :: key
  !
  ! Allocate the array of pointers. Dimension them so that they can hold (point
  ! to) all the arrays for all keywords
  toc%all%ptrs%ni4 = 0
  toc%all%ptrs%ni8 = 0
  toc%all%ptrs%nr4 = 0
  toc%all%ptrs%nr8 = 0
  toc%all%ptrs%nc8 = 0
  toc%all%ptrs%nc12 = 0
  toc%all%ptrs%nc128 = 0
  toc%all%ptr%i4 => null()  ! Shorcuts not relevant here
  toc%all%ptr%i8 => null()
  toc%all%ptr%r4 => null()
  toc%all%ptr%r8 => null()
  toc%all%ptr%c8 => null()
  toc%all%ptr%c12 => null()
  toc%all%ptr%c128 => null()
  !
  do ikey=1,toc%nkey
    key => toc%keys(ikey)
    !
    key%ptrs%ni4 = 0
    key%ptrs%ni8 = 0
    key%ptrs%nr4 = 0
    key%ptrs%nr8 = 0
    key%ptrs%nc8 = 0
    key%ptrs%nc12 = 0
    key%ptrs%nc128 = 0
    key%ptr%i4 => null()  ! Reset shorcuts
    key%ptr%i8 => null()
    key%ptr%r4 => null()
    key%ptr%r8 => null()
    key%ptr%c8 => null()
    key%ptr%c12 => null()
    key%ptr%c128 => null()
    !
    select case (key%ftype)
    case(toc_ftype_i4_1d)
      allocate(key%ptrs%i4(1))      ! Each key has a single data array
      key%ptrs%ni4 = 1              ! For consistency
      key%ptr%i4 => key%ptrs%i4(1)  ! Define a convenient shorcut to this unique array
      toc%all%ptrs%ni4 = toc%all%ptrs%ni4+1
    case(toc_ftype_i8_1d)
      allocate(key%ptrs%i8(1))
      key%ptrs%ni8 = 1
      key%ptr%i8 => key%ptrs%i8(1)
      toc%all%ptrs%ni8 = toc%all%ptrs%ni8+1
    case(toc_ftype_r4_1d)
      allocate(key%ptrs%r4(1))
      key%ptrs%nr4 = 1
      key%ptr%r4 => key%ptrs%r4(1)
      toc%all%ptrs%nr4 = toc%all%ptrs%nr4+1
    case(toc_ftype_r8_1d)
      allocate(key%ptrs%r8(1))
      key%ptrs%nr8 = 1
      key%ptr%r8 => key%ptrs%r8(1)
      toc%all%ptrs%nr8 = toc%all%ptrs%nr8+1
    case(toc_ftype_c8_1d)
      allocate(key%ptrs%c8(1))
      key%ptrs%nc8 = 1
      key%ptr%c8 => key%ptrs%c8(1)
      toc%all%ptrs%nc8 = toc%all%ptrs%nc8+1
    case(toc_ftype_c12_1d)
      allocate(key%ptrs%c12(1))
      key%ptrs%nc12 = 1
      key%ptr%c12 => key%ptrs%c12(1)
      toc%all%ptrs%nc12 = toc%all%ptrs%nc12+1
    case(toc_ftype_i4_2d)
      allocate(key%ptrs%i4(1))
      key%ptrs%ni4 = 1
      key%ptr%i4 => key%ptrs%i4(1)
      toc%all%ptrs%ni4 = toc%all%ptrs%ni4+1
    case(toc_ftype_c8_2d)
      allocate(key%ptrs%c8(1))
      key%ptrs%nc8 = 1
      key%ptr%c8 => key%ptrs%c8(1)
      toc%all%ptrs%nc8 = toc%all%ptrs%nc8+1
    case(toc_ftype_c128_1d)
      allocate(key%ptrs%c128(1))
      key%ptrs%nc128 = 1
      key%ptr%c128 => key%ptrs%c128(1)
      toc%all%ptrs%nc128 = toc%all%ptrs%nc128+1
    end select
  enddo
  !
  if (toc%all%ptrs%ni4.gt.0)   allocate(toc%all%ptrs%i4(toc%all%ptrs%ni4),stat=ier)
  if (toc%all%ptrs%ni8.gt.0)   allocate(toc%all%ptrs%i8(toc%all%ptrs%ni8),stat=ier)
  if (toc%all%ptrs%nr4.gt.0)   allocate(toc%all%ptrs%r4(toc%all%ptrs%nr4),stat=ier)
  if (toc%all%ptrs%nr8.gt.0)   allocate(toc%all%ptrs%r8(toc%all%ptrs%nr8),stat=ier)
  if (toc%all%ptrs%nc8.gt.0)   allocate(toc%all%ptrs%c8(toc%all%ptrs%nc8),stat=ier)
  if (toc%all%ptrs%nc12.gt.0)  allocate(toc%all%ptrs%c12(toc%all%ptrs%nc12),stat=ier)
  if (toc%all%ptrs%nc128.gt.0) allocate(toc%all%ptrs%c128(toc%all%ptrs%nc128),stat=ier)
  if (failed_allocate(rname,'pointers array',ier,error)) return
  !
  toc%all%keyword      = 'SETUP'
  toc%all%sic_var_name = 'setup'
  toc%all%human_name   = 'SETUP'  ! Not relevant
  toc%all%message      = 'Number of setups.......'
  ! toc%all%ftype =  Fortran type not relevant here
  ! toc%all%ptype =  custom program type not relevant here
  ! toc%all%ptr%* => shortcuts not relevant here
  ! toc%all%select =  computed in 'toc_main'
  !
end subroutine toc_init_pointers
!
subroutine toc_clean(toc,error)
  use gkernel_interfaces
  use toc_types
  !---------------------------------------------------------------------
  ! @ public
  !  Clean all the TOC structure
  !---------------------------------------------------------------------
  type(toc_t), intent(inout) :: toc    ! Table-Of-Content description
  logical,     intent(inout) :: error  !
  ! Local
  integer(kind=4) :: ikey
  !
  toc%initialized = .false.
  !
  call toc_clean_key(toc%all)
  !
  if (.not.allocated(toc%keys))  return
  !
  do ikey=1,ubound(toc%keys,1)
    call toc_clean_key(toc%keys(ikey))
  enddo
  !
  deallocate(toc%keys)
  toc%nkey = 0
  !
contains
  subroutine toc_clean_key(key)
    use toc_types
    type(toc_descriptor_t), intent(inout) :: key
    if (allocated(key%ptrs%i4))   deallocate(key%ptrs%i4)
    if (allocated(key%ptrs%i8))   deallocate(key%ptrs%i8)
    if (allocated(key%ptrs%r4))   deallocate(key%ptrs%r4)
    if (allocated(key%ptrs%r8))   deallocate(key%ptrs%r8)
    if (allocated(key%ptrs%c8))   deallocate(key%ptrs%c8)
    if (allocated(key%ptrs%c12))  deallocate(key%ptrs%c12)
    if (allocated(key%ptrs%c128)) deallocate(key%ptrs%c128)
    key%ptrs%ni4 = 0
    key%ptrs%ni8 = 0
    key%ptrs%nr4 = 0
    key%ptrs%nr8 = 0
    key%ptrs%nc8 = 0
    key%ptrs%nc12 = 0
    key%ptrs%nc128 = 0
    if (allocated(key%select%nam))  deallocate(key%select%nam)
    if (allocated(key%select%bak))  deallocate(key%select%bak)
    if (allocated(key%select%cnt))  deallocate(key%select%cnt)
  end subroutine toc_clean_key
  !
end subroutine toc_clean
!
subroutine toc_getkeys(line,iopt,toc,keys,error)
  use gbl_message
  use gkernel_interfaces
  use classic_interfaces, except_this=>toc_getkeys
  use toc_types
  !---------------------------------------------------------------------
  ! @ public
  !  Get arguments of option /TOC from command line. 'keys' is unchanged
  ! if no argument is present.
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: line     ! Command line
  integer(kind=4),  intent(in)    :: iopt     ! Option number
  type(toc_t),      intent(in)    :: toc      ! Table-Of-Content description
  character(len=*), intent(inout) :: keys(:)  ! List of keys retrieved
  logical,          intent(inout) :: error    ! Error flag
  ! Local
  character(len=*), parameter :: rname='TOC'
  integer(kind=4) :: narg,iloop,iarg,larg,num
  character(len=20) :: arg
  character(len=16) :: intern_keys(toc%nkey)
  character(len=12) :: human_keys(toc%nkey),key
  !
  narg = sic_narg(iopt)
  if (narg.gt.ubound(keys,1)) then
     call classic_message(seve%e,rname,'Too many keywords for option /TOC')
     error = .true.
     return
  endif
  !
  ! Fill the list of (internal and human-understandable) known keywords
  intern_keys = (/ (toc%keys(iloop)%keyword,    iloop=1,toc%nkey) /)
  human_keys =  (/ (toc%keys(iloop)%human_name, iloop=1,toc%nkey) /)
  !
  if (narg.le.0)  return
  !
  keys(:) = ''
  do iarg=1,narg
    call sic_ch(line,iopt,iarg,arg,larg,.true.,error)
    if (error) return
    call sic_upper(arg)
    !
    call sic_ambigs(rname,arg,key,num,human_keys,toc%nkey,error)
    if (error) return
    !
    keys(iarg) = intern_keys(num)
  enddo
  !
end subroutine toc_getkeys
!
subroutine toc_main(rname,toc,nentry,keywords,sname,olun,p_format,error)
  use gbl_message
  use gkernel_interfaces
  use classic_interfaces, except_this=>toc_main
  use classic_params
  use toc_types
  !---------------------------------------------------------------------
  ! @ public
  !  Main Table-Of-Contents engine
  !---------------------------------------------------------------------
  character(len=*),           intent(in)    :: rname        ! Calling routine name
  type(toc_t),                intent(inout) :: toc          !
  integer(kind=entry_length), intent(in)    :: nentry       ! Size of index (can be 0)
  character(len=*),           intent(in)    :: keywords(:)  ! Selection
  character(len=*),           intent(in)    :: sname        ! Sic structure name
  integer(kind=4),            intent(in)    :: olun         ! Output logical unit
  interface
    subroutine p_format(key,ival,output)  ! Formatting subroutine
    use classic_params
    use toc_types
    type(toc_descriptor_t),     intent(in)  :: key
    integer(kind=entry_length), intent(in)  :: ival
    character(len=*),           intent(out) :: output
    end subroutine p_format
  end interface
  logical,                    intent(inout) :: error        ! Logical error flag
  ! Local
  integer(kind=4) :: ikey
  character(len=16) :: one(1)
  type(toc_descriptor_t), pointer :: key
  !
  call toc_structure_parent(rname,sname,error)
  if (error)  return
  !
  ! Sanity check
  if (nentry.lt.1) then
    toc%all%select%nequ = 0
    call toc_structure_key(rname,sname,toc%all,.true.,error)  ! Fill a minimal structure
    call classic_message(seve%w,rname,'Index is empty')
    return
  endif
  !
  call toc_select_all(rname,toc,nentry,keywords,p_format,error)
  if (error)  return
  !
  do ikey=1,toc%all%select%nkey
     one(1) = keywords(ikey)  ! 1 key used for this (sub)selection
     call toc_select_one(rname,toc,one,key,p_format,error)
     if (error)  return
     !
     call toc_feedback(key)
     !
     call toc_structure_key(rname,sname,key,.false.,error)
     if (error)  return
  enddo
  !
  call toc_feedback(toc%all)
  !
  call toc_structure_key(rname,sname,toc%all,.true.,error)
  if (error)  return
  !
contains
  subroutine toc_feedback(key)
    use toc_types
    type(toc_descriptor_t), intent(in) :: key
    character(len=128) :: chain
    integer(kind=4) :: iequ,ikey,lchain,maxlen(key%select%nkey)
    character(len=12) :: aform(key%select%nkey)
    write(chain,'(a,i4)') trim(key%message),key%select%nequ
    call toc_output(chain)
    maxlen(:) = 12  ! At least 12 for cosmetic alignment
    do iequ=1,key%select%nequ
      do ikey=1,key%select%nkey
        maxlen(ikey) = max(maxlen(ikey),len_trim(key%select%nam(iequ,ikey)))
      enddo
    enddo
    do ikey=1,key%select%nkey
      write (aform(ikey),'(a2,i0,a1)')  '(a',maxlen(ikey),')'
    enddo
    do iequ=1,key%select%nequ
      write (chain,'(3x)')
      lchain = 4
      do ikey=1,key%select%nkey
        write (chain(lchain:),aform(ikey)) key%select%nam(iequ,ikey)
        lchain = lchain+maxlen(ikey)+1
      enddo
      write (chain(lchain:),'(1x,i10," (",f5.1,"%)")')  &
        key%select%cnt(iequ),key%select%cnt(iequ)*100./nentry
      call toc_output(chain)
    enddo
  end subroutine toc_feedback
  !
  subroutine toc_output(chain)
    character(len=*), intent(in) :: chain
    if (olun.eq.6) then
      call classic_message(seve%r,rname,chain)
    else
      write(olun,'(A)') trim(chain)
    endif
  end subroutine toc_output
end subroutine toc_main
!
subroutine toc_select_all(rname,toc,nentry,keywords,p_format,error)
  use gkernel_interfaces
  use classic_interfaces, except_this=>toc_select_all
  use toc_types
  !---------------------------------------------------------------------
  ! @ public
  !  Compute the equivalence classes for all (combined) keywords
  !---------------------------------------------------------------------
  character(len=*),           intent(in)    :: rname
  type(toc_t),                intent(inout) :: toc
  integer(kind=entry_length), intent(in)    :: nentry       ! Size of index
  character(len=*),           intent(in)    :: keywords(:)  ! Selection
  interface
    subroutine p_format(key,ival,output)  ! Formatting subroutine
    use classic_params
    use toc_types
    type(toc_descriptor_t),     intent(in)  :: key
    integer(kind=entry_length), intent(in)  :: ival
    character(len=*),           intent(out) :: output
    end subroutine p_format
  end interface
  logical,                    intent(inout) :: error
  ! Local
  integer(kind=entry_length), allocatable :: list(:,:)
  integer(kind=4) :: ier
  integer(kind=entry_length) :: iobs
  integer(kind=4) :: keywords_selected(toc%nkey)
  !
  ! Allocate a list for at most 'nentry' classes
  allocate(list(2,nentry),stat=ier)
  if (failed_allocate(rname,'list index',ier,error))  return
  do iobs=1,nentry
    list(1,iobs) = iobs
    list(2,iobs) = 1
  enddo
  !
  ! Get numerical ids of all keywords
  toc%all%select%nkey = toc_select_keywords(keywords,toc,keywords_selected,error)
  if (error)  return
  !
  ! Compute the all (combined) keys equivalence classes
  call toc_select_do(rname,toc,toc%all,keywords_selected,list,nentry,p_format,  &
    error)
  ! if (error)  continue
  !
  deallocate(list)
  !
end subroutine toc_select_all
!
subroutine toc_select_one(rname,toc,keywords,key,p_format,error)
  use classic_interfaces, except_this=>toc_select_one
  use toc_types
  !---------------------------------------------------------------------
  ! @ private
  !  Compute the equivalence classes for one keyword
  !---------------------------------------------------------------------
  character(len=*),       intent(in)            :: rname        ! Calling routine name
  type(toc_t),            intent(inout), target :: toc          !
  character(len=*),       intent(in)            :: keywords(1)  !
  type(toc_descriptor_t), pointer               :: key          ! Associated on return
  interface
    subroutine p_format(key,ival,output)  ! Formatting subroutine
    use classic_params
    use toc_types
    type(toc_descriptor_t),     intent(in)  :: key
    integer(kind=entry_length), intent(in)  :: ival
    character(len=*),           intent(out) :: output
    end subroutine p_format
  end interface
  logical,                intent(inout)         :: error        ! Logical error flag
  ! Local
  integer(kind=4) :: one_count,one_selected(1)
  integer(kind=entry_length) :: list(2,toc%all%select%nequ)  ! Automatic array
  !
  ! Get numerical id of this keyword
  one_count = toc_select_keywords(keywords,toc,one_selected,error)
  if (error)  return
  key => toc%keys(one_selected(1))
  key%select%nkey = one_count  ! Should be 1
  !
  ! Restart with the compressed list (we know we will find at most as many
  ! classes as in the "combined" classes)
  list(1,1:toc%all%select%nequ) = toc%all%select%bak(:)
  list(2,1:toc%all%select%nequ) = toc%all%select%cnt(:)
  !
  ! Compute per-key equivalence classes
  call toc_select_do(rname,toc,key,one_selected,list,toc%all%select%nequ,  &
    p_format,error)
  if (error)  return
  !
end subroutine toc_select_one
!
function toc_select_keywords(keywords,toc,selected,error)
  use gbl_message
  use toc_types
  !---------------------------------------------------------------------
  ! @ private
  !  Identify the selected keywords and set up the internal pointers
  ! accordingly
  !---------------------------------------------------------------------
  integer(kind=4) :: toc_select_keywords  ! Function value on return
  character(len=*), intent(in)    :: keywords(:)  ! Keyword names
  type(toc_t),      intent(inout) :: toc          ! Table-Of-Content description
  integer(kind=4),  intent(out)   :: selected(:)  ! Keyword identifiers
  logical,          intent(inout) :: error        ! Logical error flag
  ! Local
  character(len=*), parameter :: rname='TOC>SELECT>KEYWORDS'
  integer(kind=4) :: iselect, ikeyword
  logical :: found
  !
  toc_select_keywords = 0
  do iselect=1,ubound(keywords,1)
     if (keywords(iselect).eq.'') exit  ! All done
     found = .false.
     do ikeyword=1,toc%nkey
        if (toc%keys(ikeyword)%keyword.eq.keywords(iselect)) then
           toc_select_keywords = toc_select_keywords+1
           selected(toc_select_keywords) = ikeyword
           found = .true.
           exit
        endif
     enddo
     if (.not.found) then
       call classic_message(seve%e,rname,'Internal error: keyword '//  &
         trim(keywords(iselect))//' not found in internal key list')
       error = .true.
       return
     endif
  enddo
  !
  toc%all%ptrs%ni4 = 0
  toc%all%ptrs%ni8 = 0
  toc%all%ptrs%nr4 = 0
  toc%all%ptrs%nr8 = 0
  toc%all%ptrs%nc8 = 0
  toc%all%ptrs%nc12 = 0
  toc%all%ptrs%nc128 = 0
  !
  do ikeyword=1,toc_select_keywords
     iselect = selected(ikeyword)
     select case(toc%keys(iselect)%ftype)
     case(toc_ftype_i4_1d)
        toc%all%ptrs%ni4 = toc%all%ptrs%ni4+1
        toc%all%ptrs%i4(toc%all%ptrs%ni4)%ndim = 1
        toc%all%ptrs%i4(toc%all%ptrs%ni4)%data1 => toc%keys(iselect)%ptrs%i4(1)%data1
        toc%all%ptrs%i4(toc%all%ptrs%ni4)%data2 => null()  ! For safety
     case(toc_ftype_i8_1d)
        toc%all%ptrs%ni8 = toc%all%ptrs%ni8+1
        toc%all%ptrs%i8(toc%all%ptrs%ni8)%data1 => toc%keys(iselect)%ptrs%i8(1)%data1
     case(toc_ftype_r4_1d)
        toc%all%ptrs%nr4 = toc%all%ptrs%nr4+1
        toc%all%ptrs%r4(toc%all%ptrs%nr4)%data1 => toc%keys(iselect)%ptrs%r4(1)%data1
     case(toc_ftype_r8_1d)
        toc%all%ptrs%nr8 = toc%all%ptrs%nr8+1
        toc%all%ptrs%r8(toc%all%ptrs%nr8)%data1 => toc%keys(iselect)%ptrs%r8(1)%data1
     case(toc_ftype_c8_1d)
        toc%all%ptrs%nc8 = toc%all%ptrs%nc8+1
        toc%all%ptrs%c8(toc%all%ptrs%nc8)%ndim = 1
        toc%all%ptrs%c8(toc%all%ptrs%nc8)%data1 => toc%keys(iselect)%ptrs%c8(1)%data1
        toc%all%ptrs%c8(toc%all%ptrs%nc8)%data2 => null()  ! For safety
     case(toc_ftype_c12_1d)
        toc%all%ptrs%nc12 = toc%all%ptrs%nc12+1
        toc%all%ptrs%c12(toc%all%ptrs%nc12)%data1 => toc%keys(iselect)%ptrs%c12(1)%data1
     case(toc_ftype_i4_2d)
        toc%all%ptrs%ni4 = toc%all%ptrs%ni4+1
        toc%all%ptrs%i4(toc%all%ptrs%ni4)%ndim = 2
        toc%all%ptrs%i4(toc%all%ptrs%ni4)%data1 => null()  ! For safety
        toc%all%ptrs%i4(toc%all%ptrs%ni4)%data2 => toc%keys(iselect)%ptrs%i4(1)%data2
     case(toc_ftype_c8_2d)
        toc%all%ptrs%nc8 = toc%all%ptrs%nc8+1
        toc%all%ptrs%c8(toc%all%ptrs%nc8)%ndim = 2
        toc%all%ptrs%c8(toc%all%ptrs%nc8)%data1 => null()  ! For safety
        toc%all%ptrs%c8(toc%all%ptrs%nc8)%data2 => toc%keys(iselect)%ptrs%c8(1)%data2
     case(toc_ftype_c128_1d)
        toc%all%ptrs%nc128 = toc%all%ptrs%nc128+1
        toc%all%ptrs%c128(toc%all%ptrs%nc128)%data1 => toc%keys(iselect)%ptrs%c128(1)%data1
     end select
  enddo
  !
end function toc_select_keywords
!
subroutine toc_select_do(rname,toc,key,ids,list,nelem,p_format,error)
  use gkernel_interfaces
  use toc_types
  use classic_interfaces, except_this=>toc_select_do
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*),           intent(in)    :: rname
  type(toc_t),                intent(in)    :: toc
  type(toc_descriptor_t),     intent(inout) :: key
  integer(kind=4),            intent(in)    :: ids(key%select%nkey)  ! Key ids for this selection
  integer(kind=entry_length), intent(in)    :: nelem
  integer(kind=entry_length), intent(inout) :: list(2,nelem)
  interface
    subroutine p_format(key,ival,output)  ! Formatting subroutine
    use classic_params
    use toc_types
    type(toc_descriptor_t),     intent(in)  :: key
    integer(kind=entry_length), intent(in)  :: ival
    character(len=*),           intent(out) :: output
    end subroutine p_format
  end interface
  logical,                    intent(inout) :: error
  integer(kind=4) :: ier,iequ,ikey
  !
  ! Compute the equivalence classes
  call toc_eclass(toc%all%ptrs,toc_generic_eq,list,nelem,key%select%nequ)
  !
  if (allocated(key%select%cnt))  &
    deallocate(key%select%nam,key%select%cnt,key%select%bak)
  allocate(key%select%nam(key%select%nequ,key%select%nkey),  &
           key%select%bak(key%select%nequ),                  &
           key%select%cnt(key%select%nequ),stat=ier)
  if (failed_allocate(rname,'toc '//key%keyword,ier,error)) return
  !
  ! Save results in the cnt/bak/nam arrays
  do iequ=1,key%select%nequ
    key%select%bak(iequ) = list(1,iequ)
    key%select%cnt(iequ) = list(2,iequ)
    do ikey=1,key%select%nkey
      call p_format(toc%keys(ids(ikey)),key%select%bak(iequ),  &
        key%select%nam(iequ,ikey))
    enddo
  enddo
  !
contains
  subroutine toc_eclass(ptrs,equiv,list,nelt,nlist)
    use classic_params
    use toc_types
    !---------------------------------------------------------------------
    ! Compute equivalence classes
    !---------------------------------------------------------------------
    type(toc_pointers_t),       intent(in)    :: ptrs          ! Pointers to data arrays
    logical,                    external      :: equiv         ! Equality routine
    integer(kind=entry_length), intent(in)    :: nelt          !
    integer(kind=entry_length), intent(out)   :: nlist         !
    integer(kind=entry_length), intent(inout) :: list(2,nelt)  !
    ! Local
    integer(kind=entry_length) :: ielt,ilist
    logical :: listed
    !
    ! Loop over first element of all pairs
    nlist = 1
    do ielt=2,nelt
      listed = .false.
      do ilist=1,nlist
          if (equiv(ptrs,list(1,ielt),list(1,ilist))) then
            listed = .true.
            list(2,ilist) = list(2,ilist)+list(2,ielt)
            exit
          endif
      enddo
      if (.not.listed) then
          nlist = nlist+1
          list(:,nlist) = list(:,ielt)
      endif
    enddo
    !
  end subroutine toc_eclass
end subroutine toc_select_do
!
function toc_generic_eq(ptrs,m,l)
  use classic_params
  use toc_types
  !---------------------------------------------------------------------
  ! @ public
  ! TOC support routine
  !---------------------------------------------------------------------
  logical :: toc_generic_eq  ! Function value on return
  type(toc_pointers_t),       intent(in) :: ptrs  ! Pointers to data arrays
  integer(kind=entry_length), intent(in) :: m     ! Observation number
  integer(kind=entry_length), intent(in) :: l     ! Observation number
  ! Local
  integer(kind=4) :: ikey
  !
  toc_generic_eq = .true.
  do ikey=1,ptrs%ni4
     if (ptrs%i4(ikey)%ndim.eq.1) then
        if (ptrs%i4(ikey)%data1(m).ne.ptrs%i4(ikey)%data1(l)) then
            toc_generic_eq = .false.
            return
        endif
     else
        if (any(ptrs%i4(ikey)%data2(:,m).ne.ptrs%i4(ikey)%data2(:,l))) then
           toc_generic_eq = .false.
           return
        endif
     endif
  enddo
  do ikey=1,ptrs%ni8
     if (ptrs%i8(ikey)%data1(m).ne.ptrs%i8(ikey)%data1(l)) then
        toc_generic_eq = .false.
        return
     endif
  enddo
  do ikey=1,ptrs%nr4
     if (ptrs%r4(ikey)%data1(m).ne.ptrs%r4(ikey)%data1(l)) then
        toc_generic_eq = .false.
        return
     endif
  enddo
  do ikey=1,ptrs%nr8
     if (ptrs%r8(ikey)%data1(m).ne.ptrs%r8(ikey)%data1(l)) then
        toc_generic_eq = .false.
        return
     endif
  enddo
  do ikey=1,ptrs%nc8
     if (ptrs%c8(ikey)%ndim.eq.1) then
        if (ptrs%c8(ikey)%data1(m).ne.ptrs%c8(ikey)%data1(l)) then
           toc_generic_eq = .false.
           return
        endif
     else
        ! WARNING: this compares elements 1 by 1 (no implicit sort, cost
        ! would be prohibitive and very repetitive). If you want that
        ! order does not matter, you should sort your arrays earlier.
        if (any(ptrs%c8(ikey)%data2(:,m).ne.ptrs%c8(ikey)%data2(:,l))) then
           toc_generic_eq = .false.
           return
        endif
     endif
  enddo
  do ikey=1,ptrs%nc12
     if (ptrs%c12(ikey)%data1(m).ne.ptrs%c12(ikey)%data1(l)) then
        toc_generic_eq = .false.
        return
     endif
  enddo
  do ikey=1,ptrs%nc128
     if (ptrs%c128(ikey)%data1(m).ne.ptrs%c128(ikey)%data1(l)) then
        toc_generic_eq = .false.
        return
     endif
  enddo
end function toc_generic_eq
!
subroutine toc_format(key,ival,output)
  use toc_types
  !---------------------------------------------------------------------
  ! @ public
  ! TOC support routine: generic formatting routine
  !---------------------------------------------------------------------
  type(toc_descriptor_t),     intent(in)  :: key     !
  integer(kind=entry_length), intent(in)  :: ival    !
  character(len=*),           intent(out) :: output  !
  ! Local
  integer(kind=4) :: l,nc,i
  !
  select case(key%ftype)
  case(toc_ftype_i4_1d)
    write(output,'(i12)') key%ptrs%i4(1)%data1(ival)
  case(toc_ftype_i8_1d)
    write(output,'(i12)') key%ptrs%i8(1)%data1(ival)
  case(toc_ftype_r4_1d)
    write(output,'(f8.3)') key%ptrs%r4(1)%data1(ival)
  case(toc_ftype_r8_1d)
    write(output,'(f8.3)') key%ptrs%r8(1)%data1(ival)
  case(toc_ftype_c8_1d)
    output = key%ptrs%c8(1)%data1(ival)
  case(toc_ftype_c12_1d)
    output = key%ptrs%c12(1)%data1(ival)
  case(toc_ftype_i4_2d)
    nc = 0
    do i=1,ubound(key%ptrs%i4(1)%data2,1)
      write(output(nc+1:),'(i0,a)') key%ptrs%i4(1)%data2(i,ival),','
      nc = len_trim(output)
    enddo
    output(nc:nc) = ' '  ! Drop last ,
  case(toc_ftype_c8_2d)
    l = len(key%ptrs%c8(1)%data2(1,1))
    nc = 1
    do i=1,ubound(key%ptrs%c8(1)%data2,1)
      output(nc:) = key%ptrs%c8(1)%data2(i,ival)
      nc = nc+l+1
    enddo
  case(toc_ftype_c128_1d)
    output = key%ptrs%c128(1)%data1(ival)
  end select
  !
end subroutine toc_format
!
subroutine toc_structure_parent(rname,name,error)
  use gbl_message
  use gkernel_types
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! @ private
  !  Define the Table-Of-Content Sic structure (parent structure part)
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: rname  ! Calling routine name
  character(len=*), intent(in)    :: name   ! Structure name e.g. MYTOC
  logical,          intent(inout) :: error  ! Logical error flag
  ! Local
  type(sic_descriptor_t) :: desc
  logical :: found
  !
  ! Check if structure exist, create if needed
  found = .false.  ! Not verbose
  call sic_descriptor(name,desc,found)
  if (found) then
    ! Delete it
    if (desc%type.ne.0) then
      call classic_message(seve%e,rname,'Output variable must be a structure')
      error = .true.
!   elseif (desc%status.ne.user_defined) then
!     call classic_message(seve%e,rname,'Output variable must be user-defined')
!     error = .true.
    endif
    if (error)  return
    call sic_delvariable(name,.true.,error)  ! User request
    if (error)  return
  endif
  ! Create a user-defined structure  ! ZZZ not global?
  call sic_crestructure(name,.true.,error)
  if (error) then
    call classic_message(seve%e,rname,'Can not define '//trim(name)//' structure')
    return
  endif
  !
end subroutine toc_structure_parent
!
subroutine toc_structure_key(rname,sname,key,twodims,error)
  use gildas_def
  use gbl_format
  use gbl_message
  use gkernel_types
  use gkernel_interfaces
  use toc_types
  !---------------------------------------------------------------------
  ! @ private
  !  Define the Table-Of-Content Sic structure (one key part)
  !---------------------------------------------------------------------
  character(len=*),       intent(in)    :: rname    ! Calling routine name
  character(len=*),       intent(in)    :: sname    ! Structure name e.g. MYTOC
  type(toc_descriptor_t), intent(in)    :: key      !
  logical,                intent(in)    :: twodims  ! Two dimensions for MYTOC%MSOUR?
  logical,                intent(inout) :: error    ! Logical error flag
  ! Local
  character(len=varname_length) :: varname
  logical :: found
  type(sic_descriptor_t) :: desc
  integer(kind=address_length) :: ipnt
  integer(kind=4) :: memory(2),ikey,lname
  character(len=12) :: dims
  integer(kind=entry_length) :: iequ
  !
  ! MYTOC%NSOUR
  write(varname,'(A,A,A)')  trim(sname),'%N',key%sic_var_name
  call sic_defvariable(fmt_i8,varname,.true.,error)
  if (error)  return
  found = .false.  ! Not verbose
  call sic_descriptor(varname,desc,found)
  ipnt = gag_pointer(desc%addr,memory)
  call i8toi8(key%select%nequ,memory(ipnt),1)
  !
  ! This key is empty: stop here
  if (key%select%nequ.le.0)  return
  !
  ! MYTOC%MSOUR[NEQU]
  write(varname,'(A,A,A)')  trim(sname),'%M',key%sic_var_name
  write(dims,'(A,I0,A)')  '[',key%select%nequ,']'
  call sic_defvariable(fmt_i8,trim(varname)//dims,.true.,error)
  if (error)  return
  found = .false.  ! Not verbose
  call sic_descriptor(varname,desc,found)
  ipnt = gag_pointer(desc%addr,memory)
  do iequ=1,key%select%nequ
    call i8toi8(key%select%cnt(iequ),memory(ipnt),1)
    ipnt = ipnt+2
  enddo
  !
  ! MYTOC%SOUR[NEQU]
  write(varname,'(A,A,A)')  trim(sname),'%',key%sic_var_name
  if (twodims) then
    ! Special patch for MYTOC%MSETUP, even if NKEY is 1
    write(dims,'(A,I0,A,I0,A)')  '[',key%select%nequ,',',key%select%nkey,']'
  else
    write(dims,'(A,I0,A)')  '[',key%select%nequ,']'
  endif
  lname = len(key%select%nam)
  call sic_defvariable(lname,trim(varname)//dims,.true.,error)
  if (error)  return
  found = .false.  ! Not verbose
  call sic_descriptor(varname,desc,found)
  do ikey=1,key%select%nkey
    do iequ=1,key%select%nequ
      call ctodes(key%select%nam(iequ,ikey),desc%type,desc%addr)
      desc%addr = desc%addr+lname
    enddo
  enddo
  !
end subroutine toc_structure_key
