program classic_dump
  use gbl_message
  use gkernel_interfaces
  use classic_api
  !---------------------------------------------------------------------
  ! Dump the contents of any kind of Classic file
  !---------------------------------------------------------------------
  logical :: error
  type(classic_file_t) :: file
  integer(kind=4), parameter :: file_lun=50  ! Arbitrary value
  integer(kind=entry_length) :: ientry
  type(classic_recordbuf_t) :: rbufind,rbufobs
  integer(kind=4), allocatable :: bufind(:)  ! Buffer for byte-swapping one index
  integer(kind=4), allocatable :: bufsec(:)  ! Buffer for byte-swapping one section
  integer(kind=8) :: ind_rstart
  integer(kind=4) :: ind_wstart
  type(classic_entrydesc_t) :: edesc
  character(len=message_length) :: mess
  integer(kind=4) :: isec,nc,arg_count,iarg,verbosity
  character(len=24) :: opt_arg
  !
  error = .false.
  !
  ! Command line arguments
  arg_count = sic_get_arg_count()
  if (arg_count.le.0) then
    call classic_dump_usage
    stop
  endif
  !
  verbosity = 0  ! See usage for verbosity levels
  iarg = 1
  do while (iarg.le.arg_count-1)
    call sic_get_arg(iarg,opt_arg)
    iarg = iarg+1
    if (opt_arg.eq.'-v') then
      call sic_get_arg(iarg,opt_arg)
      iarg = iarg+1
      read(opt_arg,'(I1)')  verbosity
    elseif (opt_arg.eq.'-h') then
      call classic_dump_usage
      stop
    else
      write(*,'(A)')  'Option '//trim(opt_arg)//' not recognized'
      stop
    endif
  enddo
  !
  ! Last argument is the file name
  call sic_get_arg(arg_count,file%spec)
  !
  ! Initialize the Classic library
  call classic_init(error)
  if (error)  stop
  file%lun = file_lun
  file%nspec = len_trim(file%spec)
  file%update = .false.
  call classic_file_open(file,.false.,error)
  if (error)  stop
  !
  call classic_filedesc_open(file,error)
  if (error)  stop
  write(*,*) ''
  write(*,'(A)') '----------------------------------------------------------'
  write(*,'(A)')  'File Descriptor:'
  call classic_filedesc_dump(file%desc,'')
  write(*,*) ''
  !
  if (verbosity.eq.0)  goto 10
  !
  call reallocate_recordbuf(rbufind,file%desc%reclen,error)
  if (error)  stop
  call reallocate_recordbuf(rbufobs,file%desc%reclen,error)
  if (error)  stop
  call reallocate_buffer(bufind,int(file%desc%lind,kind=8),error)
  if (error)  stop
  !
  write(*,'(A)') '------------------------------------------------------------------------'
  write(*,'(A)') 'Entries:'
  write(*,'(A)') '     #  Record   Word  Xnum Ver      Len Adata    Ldata Nsec  Id,Add,Len'
  do ientry=1,file%desc%xnext-1  ! Read all entries
    !
    ! Read the Entry Index
    call classic_entryindex_read(file,ientry,bufind,rbufind,error)
    if (error)  stop
    call classic_dump_buf2ind(bufind,ind_rstart,ind_wstart,file%conv)
    if (error)  stop
    !
    if (verbosity.le.2) then
      write(mess,'(I6,I8,I7)')  &
        ientry,ind_rstart,ind_wstart
      write(*,'(A)')  trim(mess)
    else
      !
      ! Set the working buffer
      call classic_recordbuf_open(file,ind_rstart,ind_wstart,rbufobs,error)
      if (error)  stop
      ! Read the Entry Descriptor
      call classic_entrydesc_read(file,rbufobs,edesc,error)
      if (error)  stop
      !
      write(mess,'(I6,I8,I7,I6,I4,I9,I6,I9,I5)')  &
        ientry,ind_rstart,ind_wstart,edesc%xnum,edesc%version,  &
        edesc%nword,edesc%adata,edesc%ldata,edesc%nsec
      nc = len_trim(mess)
      ! Compact description of the sections
      do isec=1,edesc%nsec
        write(mess(nc+3:),'(I0,A,I0,A,I0)')  &
          edesc%seciden(isec),',',edesc%secaddr(isec),',',edesc%secleng(isec)
        nc = len_trim(mess)
      enddo
      write(*,'(A)')  mess(1:nc)
      !
    endif
    !
    if (verbosity.eq.2 .or. verbosity.eq.4) then
      ! Verbose index:
      write(*,'(A)')  'Index data:'
      call hex_type(int(file%desc%lind*4,kind=size_length),bufind)
    endif
    !
    if (verbosity.eq.4) then
      ! Verbose sections:
      if (edesc%nsec.gt.0) then
        do isec=1,edesc%nsec
          write(*,'(A,I0,A,I0,A,I0,A)')        &
            'Section #',edesc%seciden(isec),   &
            ', Address ',edesc%secaddr(isec),  &
            ', Length ',edesc%secleng(isec),   &
            ', Data:'
          !
          call reallocate_buffer(bufsec,edesc%secleng(isec),error)
          if (error)  stop
          call classic_entry_section_read(edesc%seciden(isec),edesc%secleng(isec),  &
            bufsec,edesc,rbufobs,error)
          if (error)  stop
          call hex_type(int(edesc%secleng(isec)*4,kind=size_length),bufsec)
        enddo
      endif
    endif
    !
    if (verbosity.eq.2 .or. verbosity.eq.4)  write(*,*)
    !
  enddo
  !
  ! Cleaning
  call deallocate_recordbuf(rbufind,error)
  if (error)  stop
  call deallocate_recordbuf(rbufobs,error)
  if (error)  stop
  if (allocated(bufind)) deallocate(bufind)
  if (allocated(bufsec)) deallocate(bufsec)
  !
10 continue
  call classic_file_close(file,error)
  if (error)  stop
  !
contains
  subroutine classic_dump_usage
    write(*,'(A)')  'Usage:'
    write(*,'(A)')  '  classic-dump [-v level] file'
    write(*,'(A)')  ''
    write(*,'(A)')  '  Dump the contents of any kind of Classic file.'
    write(*,'(A)')  ''
    write(*,'(A)')  '  -v 0 (default) dump the file summary (file descriptor),'
    write(*,'(A)')  '  -v 1 list all entries in the file index,'
    write(*,'(A)')  '  -v 2 same + dump the content of each entry index,'
    write(*,'(A)')  '  -v 3 list all entries in the file index + their entry descriptor,'
    write(*,'(A)')  '  -v 4 same + dump the content of each entry index and its sections'
    write(*,'(A)')  ''
    write(*,'(A)')  '  Remember that on little-endian architectures (e.g. IEEE), bytes'
    write(*,'(A)')  '  are read from right to left. You can use e.g. Python to decode '
    write(*,'(A)')  '  those bytes. For floating points use e.g. ''<f'' for little-endian'
    write(*,'(A)')  '  order or ''=f'' for native order:'
    write(*,'(A)')  '  >>> import struct'
    write(*,'(A)')  '  >>> struct.unpack(''<d'',"53ED599657EC813F".decode(''hex''))'
    write(*,'(A)')  '  (0.008751568103653773,)'
    write(*,'(A)')  '  >>> struct.unpack(''<i'',"40EEFFFF".decode(''hex''))'
    write(*,'(A)')  '  (-4544,)'
    write(*,'(A)')  '  >>> struct.unpack(''<f'',"71D2C040".decode(''hex''))'
    write(*,'(A)')  '  (6.025688648223877,)'
    write(*,'(A)')  '  >>> struct.unpack(''<12s'',"33304D4532484C492D463031".decode(''hex''))'
    write(*,'(A)')  '  (''30ME2HLI-F01'',)'
    write(*,'(A)')  ''
  end subroutine classic_dump_usage
  !
  subroutine classic_dump_buf2ind(buf,rec,word,conv)
    use gildas_def
    use gkernel_interfaces
    use classic_api
    !---------------------------------------------------------------------
    ! Copy the Entry Index into the given buffer, swap bytes if needed
    !---------------------------------------------------------------------
    integer(kind=4),          intent(in)  :: buf(*)
    integer(kind=8),          intent(out) :: rec
    integer(kind=4),          intent(out) :: word
    type(classic_fileconv_t), intent(in)  :: conv
    ! Local
    integer(kind=4) :: addr4
    !
    if (file%desc%version.eq.1) then
      call conv%read%i4(buf(1),addr4,1)  !   1: record
      rec = addr4
      word = 1
    else
      call conv%read%i8(buf(1),rec, 1)  ! 1-2: record
      call conv%read%i4(buf(3),word,1)  !   3: word
    endif
  end subroutine classic_dump_buf2ind
  !
  subroutine reallocate_buffer(buf,l,error)
    integer(kind=4), allocatable   :: buf(:)
    integer(kind=8), intent(in)    :: l
    logical,         intent(inout) :: error
    ! Local
    integer(kind=4) :: ier
    !
    if (allocated(buf)) then
      if (size(buf).ge.l) then
        return
      else
        deallocate(buf)
      endif
    endif
    allocate(buf(l),stat=ier)
    if (failed_allocate('DUMP','buffer',ier,error))  return
    !
  end subroutine reallocate_buffer
  !
end program classic_dump
