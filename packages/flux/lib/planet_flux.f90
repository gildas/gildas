subroutine planet_flux(name,date,fnu,flux,error)
  !---------------------------------------------------------------------
  !	FLUX Command :
  !		character*(*) name 	input
  !		character*(*) date 	input
  !		real*8 	FNU		input
  !		real*8	flux		output
  !		logical error 		in/output
  !---------------------------------------------------------------------
  character(len=12) :: name         !
  character(len=12) :: date         !
  real*8 :: fnu                     !
  real*8 :: flux                    !
  logical :: error                  !
  ! Global
  ! INCLUDE 'astro.inc'
  ! Local
  real*8 :: jnow,d_ut1,d_tdt
  logical :: found
  integer :: m_body, i_planet, ndate(7), i, lobs
  parameter (m_body = 9)
  character(len=12) :: body (m_body)
  character(len=4) :: observatory
  real*8 :: helio_dist, distance, tbright, tmb, beam, fbeam, asize
  real*8 :: lola(2), eq(2), ho(2), sunel, vr
  real*8 :: lola_e(2), eqoff(2), vroff(2), size(3), eqtopo(2)
  logical :: visible
  ! Data
  data body/'MOON', 'SUN', 'MERCURY', 'VENUS',   &
     &    'MARS', 'JUPITER', 'SATURN', 'URANUS', 'NEPTUNE'/
  !------------------------------------------------------------------------
  ! Code :
  !
  found = .false.
  !
  !Initialisation des params header de flux ra,dec, etc...
  ! Pas besoin, nuisible ici car Astro_init fait le travail
  !	   CALL FLUX_INIT(ERROR)
  !
  ! Compute real time
  call cdaten(date,ndate,error)
  if (error) return
  ! set the time field to 0 h, 0 min, 0 sec, 0 millisec
  do i=4, 7
    ndate(i) = 0
  enddo
  !
  call datejj (ndate,jnow)     ! transform it to julian date
  !
  ! new subroutine calls from astro
  ! Get D_UT1 and D_TDT from Astro
  d_ut1 = 0.0
  d_tdt = 0.0
  call do_astro_time(jnow, d_ut1, d_tdt, error)
  !
  ! Loop on possible objects
  call sic_get_char('OBSERVATORY',observatory,lobs,error)
  call do_tele_beam(beam,fnu,observatory)
  !
  do i_planet = 1, m_body
    if (name .eq. body(i_planet)) then
      found = .true.
      call do_astro_planet(i_planet,.false.,lola,eq,eqtopo,   &
     &        ho,sunel,vr,   &
     &        distance,helio_dist,size,tbright,fnu,flux,   &
     &        visible,lola_e,eqoff,vroff,beam,tmb,fbeam,asize,error)
    endif
  enddo
  !
  flux = fbeam
end subroutine planet_flux
