!-------------------------------------------------------------------
! FLUX.INC
!
      INTEGER MSOURCES                   !  Max nbre sources
      PARAMETER(MSOURCES=100)
      INTEGER MREF                       !  Max reference sources
      PARAMETER (MREF=10)
      INTEGER NQUA
      PARAMETER (NQUA=5)
!
      INTEGER N_REF                      !  Nbre reference sources
      INTEGER NSOURCES                   !  Nbre sources
      INTEGER N_QUA                      !  Nbre quasar reference
!
      REAL*8 VFREQEQ                     ! eq. frequency (depends of the gain:gais)
      REAL*8 FREQUENCY                   ! obs frequency (USB or LSB band)
      REAL*4 GAIS                        ! gain I/S
      REAL*8 EFFI,DEFFI                  ! efficacity (JY/K) and error
      INTEGER NANTENNA                   ! Antenna number
!
      REAL*8 RFLUX(MREF), SFLUX(MSOURCES), DSFLUX(MSOURCES), QFLUX(NQUA)
      LOGICAL FREE(MSOURCES)
!
      CHARACTER*12 RSOURCE(MREF), SSOURCE(MSOURCES), QSOURCE(NQUA)
!
      CHARACTER*12 UDATE                 ! date of observation: 'dd-mmm-yyyy'
      CHARACTER*80 FICH                  ! Input file from CLASS or CLIC (fit results)
!
      COMMON /COM_IFLUX/ VFREQEQ, FREQUENCY,                            &
     &EFFI, DEFFI, SFLUX, DSFLUX, QFLUX, RFLUX,                         &
     &GAIS, NANTENNA, N_REF, NSOURCES, N_QUA, FREE
      COMMON /COM_CFLUX/ FICH, UDATE, RSOURCE, SSOURCE, QSOURCE
!
      SAVE /COM_IFLUX/, /COM_CFLUX/
