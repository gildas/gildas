subroutine flux_file(line,error)
  !---------------------------------------------------------------------
  !     Command FLUX\FILE Name
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'flux.inc'
  ! Local
  character(len=80) :: namf
  integer :: nc
  !
  call sic_ch (line,0,1,namf,nc,.true.,error)
  if (error) return
  call sic_parsef(namf,fich,' ','.FIT')
  !
  ! Mise a Zero du compteur de Source de reference N_REF et N_QUA a chaque appel
  ! d'un nouveau fichier d'entree.
  !
  n_ref = 0
  n_qua = 0
end subroutine flux_file
