subroutine flux_set(line,error)
  !---------------------------------------------------------------------
  ! FLUX   Support for command
  !     ASTRO\SET FLUX Quasar [Flux|*]
  !        (only one value by SET command)
  !
  !  Quasar   CHARACTER*12	Quasar name
  !  Flux     REAL   		Flux (of the source)
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'flux.inc'
  ! Local
  character(len=12) :: name,argum2
  real*8 :: flux
  integer :: i,j,nc
  !------------------------------------------------------------------------
  ! Code:
  !
  call sic_ch (line,0,2,name,nc,.true.,error)
  argum2 = '*'
  call sic_ch (line,0,3,argum2,nc,.false.,error)
  !
  ! Set the flux for the speficied quasar
  if (argum2.ne.'*') then
    call sic_r8 (line,0,3,flux,.false.,error)
    do i=1,n_qua
      if (qsource(i).eq.name) then
        qflux(i) = flux
        return
      endif
    enddo
    n_qua = n_qua+1
    qsource(n_qua) = name
    qflux(n_qua) = flux
  else
    !
    ! If flux is *, remove quasar from list
    do i=1,n_qua
      if (qsource(i).eq.name) then
        do j=i+1,n_qua
          qsource(j-1) = qsource(j)
          qflux(j-1) = qflux(j)
        enddo
        n_qua = n_qua-1
        return
      endif
    enddo
  endif
end subroutine flux_set
