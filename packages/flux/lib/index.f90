subroutine flux_index (line,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  !  Command  FLUX\INDEX FileIn Source [/OUTPUT FileOut [NEW]]
  !     Compute spectral index for specified source from data in
  !     input file FileIn.
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'flux.inc'
  ! Local
  integer :: msou,nsou
  parameter (msou=100)
  character(len=80) :: namf,filin,filou,filf
  character(len=12) :: sousou,nwou,sou(msou)
  real*8 :: freqin,freqout,fluxin,fluxou,flux,index,dindx
  logical :: output
  integer :: i,nc,nf,ier
  !
  call sic_ch (line,0,1,namf,nc,.true.,error)
  if (error) return
  call sic_parsef(namf,filin,' ','.MES')
  !
  ! Quelle source ?
  sousou = '*'
  call sic_ch (line,0,2,sousou,nc,.false.,error)
  if (error) return
  !
  if (sic_present(1,0)) then
    call sic_ch (line,1,1,filf,nc,.true.,error)
    if (error) return
    call sic_parsef(filf,filou,' ','.IND')
    nwou = 'OLD'
  else
    nwou = 'NON'
  endif
  call sic_ch (line,1,2,nwou,nc,.false.,error)
  if (error) return
  !
  ! Ecriture du fichier de sortie:
  output = nwou.ne.'NON'
  if (output) then
    nf = lenc(filou)
    if (nwou.eq.'NEW') then
      ier = sic_open(2,filou(1:nf),'NEW',.false.)
    else
      ier = sic_open(2,filou(1:nf),'APPEND',.false.)
    endif
    if (ier.ne.0) then
      call gagout('E-INDEX,  Cannot open output file')
      call putios('E-INDEX,  ',ier)
      error = .true.
      return
    endif
    !
    if (nwou.eq.'NEW') then
      write(2,10) '! Spectral Index (Flux=C*freq.^Index):'
      write(2,10) '! Source  Flux(Low freq.) Frequencies  Index '
    endif
  endif
  !
  ! Single source case
  if (sousou.ne.'*') then
    nsou = 1
    sou(1) = sousou
  else
    call get_names(filin,sou,nsou,msou)
  endif
  write(6,100) 'I-FLUX  Flux = C*(freq.)^Index '
  write(6,102)
  102   format(1x,'   Source      Frequencies    Flux(1)   Flux(2) ',   &
     &    '  Flux(86)     Index')
  !
  ! Calcul de l'index spectral
  do i=1,nsou
    call index_flux(filin,sou(i),index,dindx,   &
     &      freqin,freqout,fluxin,fluxou,error)
    if (.not.error) then
      if (output) write(2,20)   &
     &        sou(i), fluxin, freqin, freqout, index, dindx
      flux = fluxin*(86d0/freqin)**index
      write(6,104) sou(i), freqin, freqout, fluxin, fluxou,   &
     &        flux, index, dindx
    endif
  enddo
  if (output) close(unit=2)
  !
  100   format(1x,a)
  104   format(2x,a,1x,f6.2,1x,f6.2,2x,f8.2,2x,f8.2,3x,   &
     &    f8.2,2x,f6.2,' +- ',f6.2)
  10    format(a)
  20    format(2x,a,2x,f8.2,2x,f7.2,2x,f7.2,2x,f6.2,' +/- ',f6.2)
end subroutine flux_index
!
subroutine index_flux(file,source,indx,dindx,   &
     &    freqin,freqout,fluxin,fluxou,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! ASTRO This subroutine -reads the input file from FLUX\print
  !			-reads the frequencies limits
  !			-calculate the index from the fluxes and frequencies
  !
  ! Arguments:	Input: character*(80) file	name of input file
  !		Input: character*(12)  source   source name
  !		Output: real*8 indx		between freqin and freqout
  !		Output: real*8 dindx		error on indx
  !		Output: real*8 freqin 		low freq.
  !		Output: real*8 freqout		high freq.
  !		Output: real*8 fluxin 		flux at low freq. (freqin)
  !		Output: real*8 fluxou 		flux at high freq. (freqou)
  !		Input/Output: logical error
  !---------------------------------------------------------------------
  character(len=*) :: file          !
  character(len=*) :: source        !
  real*8 :: indx                    !
  real*8 :: dindx                   !
  real*8 :: freqin                  !
  real*8 :: freqout                 !
  real*8 :: fluxin                  !
  real*8 :: fluxou                  !
  logical :: error                  !
  ! Local
  character(len=80) :: ligne
  character(len=12) :: sour
  real*8 :: dflux1,dflux2, freq1,freq2, flux1,flux2
  integer :: nf,ier,k
  logical :: ok
  !
  ! Program
  error = .false.
  !
  freq1 = 0.0
  freq2 = 0.0
  flux2 = 0.0
  flux1 = 0.0
  !
  nf = lenc(file)
  ier = sic_open (1,file(1:nf),'OLD',.true.)
  if (ier.ne.0) then
    call gagout('E-INDEX,  Cannot open input file')
    call putios('E-INDEX,  ',ier)
    error = .true.
    return
  endif
  !
  ! Skip the header and Locate the source
  freq1 = 0.d0
  freq2 = 0.d0
  ok = .true.
  do while (ok)
    read(1,'(A)',end=10) ligne
    k = index(ligne(1:4),'!')
    if (k.ne.0) then
      k = index(ligne,'frequency')
      if (k.ne.0) read (ligne(k+10:),*) freq1
    else
      k = index(ligne,source)
      if (k.ne.0) then
        read(ligne(k+16:),*) flux1, dflux1
        ok = .false.
      endif
    endif
  enddo
  !
  ! Skip other sources to get second frequency
  ok = .true.
  do while (ok)
    read(1,'(A)',end=10) ligne
    k = index(ligne(1:4),'!')
    if (k.ne.0) then
      k = index(ligne,'frequency')
      if (k.ne.0) read (ligne(k+10:),*) freq2
    else
      k = index(ligne,source)
      if (k.ne.0) then
        read(ligne(k+16:),*) flux2, dflux2
        ok = .false.
      endif
    endif
  enddo
  !
  10    continue
  close(unit=1)
  !
  ! Calcul de l'index spectral: f= freq**index
  !
  if (ok) then
    if (freq1.eq.0d0) then
      write(6,*) 'E-FLUX,  No such sources',source
    else
      write(6,*) 'E-FLUX,  Only frequency ',freq1,   &
     &        ' in the input file'
    endif
    error=.true.
    return
  endif
  !
  indx = dlog(flux1/flux2)/dlog(freq1/freq2)
  dindx = (dflux1/flux1 + dflux2/flux2) / abs(dlog(freq1/freq2))
  if (freq1.lt.freq2) then
    freqin = freq1
    freqout = freq2
    fluxin = flux1
    fluxou = flux2
  else
    freqin = freq2
    freqout = freq1
    fluxin = flux2
    fluxou = flux1
  endif
end subroutine index_flux
!
subroutine get_names(file,sou,nsou,msou)
  use gkernel_interfaces
  character(len=*) :: file          !
  integer :: msou                   !
  character(len=*) :: sou(msou)     !
  integer :: nsou                   !
  ! Local
  integer :: i,nf,nl,ier
  character(len=132) :: line
  character(len=12) :: source
  logical :: error,ok,found
  !
  nsou = 0
  nf = lenc(file)
  ier = sic_open (1,file(1:nf),'OLD',.true.)
  if (ier.ne.0) then
    call gagout('E-INDEX,  Cannot open input file')
    call putios('E-INDEX,  ',ier)
    error = .true.
    return
  endif
  !
  ok = .true.
  do while(ok)
    read(1,'(A)',end=10,iostat=ier) line
    if (index(line,'!').eq.0) then
      nl = lenc(line)
      call sic_blanc(line,nl)
      nl = index(line,' ')
      if (nl.gt.1) then
        source = line(1:nl)
        found = .false.
        do i=1,nsou
          if (source.eq.sou(i)) then
            found = .true.
          endif
        enddo
        if (.not.found) then
          if (nsou.eq.msou) then
            call gagout('W-FLUX,  Too many sources')
            goto 10
          endif
          nsou = nsou+1
          sou(nsou) = source
        endif
      endif
    endif
  enddo
  10    close(unit=1)
  !
end subroutine get_names
