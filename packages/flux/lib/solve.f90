subroutine flux_solve(line,error)
  !---------------------------------------------------------------------
  ! commande FLUX\SOLVE NAME1 NAME2...
  !
  ! NAMEx est une PLANETE, sans argument prend TOUTES les planetes du
  ! fichier d'appel. Avec 1 argument: prend uniquement cette planete
  ! comme source de reference (plus les quasars definis par "set name flux")
  ! meme si il y a d'autres planetes dans le fichier d'entree.
  !
  ! Apres lecture de la commande et des arguments
  ! 1/ Appel read_flux
  ! 2/ Appel solve_flux
  ! pour calculer les efficacites et les flux
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  integer :: sic_narg
  include 'flux.inc'
  ! Local
  integer :: m_source,n_source,i,ref,j,pla,nc
  parameter (m_source=100)
  real*8 :: sarea(m_source),sdarea(m_source),flux(m_source)
  real*8 :: dflux(m_source),eff,deff
  real*8 :: fre_int
  real*4 :: gaga
  character(len=12) :: source(m_source), nam(m_source)
  !
  ! Lecture de la commande:
  pla = sic_narg(0)
  do i=1,pla
    call sic_ch (line,0,i,nam(i),nc,.true.,error)
    if (error) return
  enddo
  !
  ! Conserver la frequence pour un eventuel 2ieme passage:
  fre_int = frequency
  call read_flux(fich,nantenna,fre_int,udate,n_source,source,   &
     &    sarea,sdarea,flux,gaga,error)
  if (error) return
  !
  ! FRE_INT contient maintenant la frequence equivalente:
  vfreqeq = fre_int
  fre_int = frequency
  gais = gaga
  !
  ref = 0
  if (pla.eq.0) then
    do i=1, n_source
      if (flux(i).lt.0) then
        ref = ref+1
        rsource(ref) = source(i)
        rflux(ref) = -flux(i)
        flux(i) = -flux(i)
      endif
    enddo
  else
    do j =1, pla
      do i=1, n_source
        if (flux(i).lt.0.and.source(i).eq.nam(j)) then
          ref = ref+1
          rsource(ref) = source(i)
          rflux(ref) = -flux(i)
          flux(i) = -flux(i)
        endif
      enddo
    enddo
  endif
  !
  do i=1, n_qua
    ref= ref+1
    rsource(ref) = qsource(i)
    rflux(ref) = qflux(i)
  enddo
  !
  if (ref.eq.0.0) then
    write(6,*) 'E-FLUX No reference source...'
    error = .true.
    return
  else
    write(6,*) 'W-FLUX Using ',ref,' reference source(s):'
    do i=1, ref
      write(6,10) rsource(i), rflux(i)
    enddo
  endif
  !
  n_ref = ref
  write(6,*) ' '
  write(6,*) 'W-FLUX Gain I/S and corrected frequency: '
  write(6,5)  gais, vfreqeq
  !
  ! Call to Solve_flux...
  call solve_flux(n_source,source,sarea,sdarea,flux,dflux,eff,   &
     &    deff,n_ref,rsource,rflux,free,error)
  if (error) return
  !
  ! Recuperation des variables:
  effi = eff
  deffi = deff
  !
  ! Ecriture des resultats pour les quasars dans le common:
  nsources=n_source
  do i=1, n_source
    ssource(i) = source(i)
    sflux(i) = flux(i)
    dsflux(i) = dflux(i)
  enddo
  !
  ! Test sur les references:
  write(6,*) ' '
  write(6,*) 'W-FLUX Source  Flux(Jy)  +-error on flux '
  !
  do i=1, n_source
    if (free(i)) then
      write(6,20) ssource(i), sflux(i), dsflux(i)
    else
      write(6,30) ssource(i), sflux(i), dsflux(i), 'Fixed'
    endif
  enddo
  !
  5     format(5x,f4.2,4x,f7.2)
  10    format(5x,a,2x,f7.2,2x,a)
  20    format(5x,a,2x,f8.2,2x,f7.2)
  30    format(5x,a,2x,f8.2,2x,f7.2,5x,a)
end subroutine flux_solve
!
subroutine solve_flux(n_source,source,sarea,sdarea,flux,ddflux,   &
     &    eff,deff,n_ref,refe,r_flux,free,error)
  !---------------------------------------------------------------------
  ! Subroutine to compute mean of the Antennas efficacity (Jy/K) and
  ! the fluxes from n_sources sources containing n_ref referenced sources
  ! named refe with the known flux r_flux.
  !
  ! Input/Output
  !	Input integer	N_SOURCE		number of sources
  !	Input character*12 source(n_source)	name of sources
  !	Input real*8 sarea(n_source)		mean of integrated area
  !(sarea/warea)
  !	Input real*8 sdarea(n_source)		error on area
  !	Output real*8 flux(n_source)		flux of sources
  !	Output real*8 ddflux(n_source)		error on flux of sources
  !	Output real*8 EFF			efficacity of the antenna
  !	Output real*8 DEFF			error on efficacity
  !	Input  integer N_REF			number of reference sources
  !	Input  character*12 refe(n_ref)		name of reference sources
  !	Input  real*8	r_flux(n_ref)		flux of reference sources
  !       Output logical  free(n_sources)            Source fixee ou Non
  !	Input logical error
  !---------------------------------------------------------------------
  integer :: n_source               !
  character(len=12) :: source(*)    !
  real*8 :: sarea(*)                !
  real*8 :: sdarea(*)               !
  real*8 :: flux(*)                 !
  real*8 :: ddflux(*)               !
  real*8 :: eff                     !
  real*8 :: deff                    !
  integer :: n_ref                  !
  character(len=12) :: refe(n_ref)  !
  real*8 :: r_flux(n_ref)           !
  logical :: free(*)                !
  logical :: error                  !
  ! Local
  integer :: et_zut
  integer :: i,j
  real*8 :: factor, dfactor
  !
  !Init des variables
  !
  eff = 0
  deff = 0
  !
  factor = 0
  dfactor = 0
  !
  !Beginning of loops on flux and area
  !
  et_zut = 0
  !
  ! Look for reference sources
  do i=1, n_source
    free(i) = .true.
    do j=1, n_ref
      if (source(i).eq.refe(j)) then
        et_zut = et_zut+1
        factor = r_flux(j)/sarea(i)
        dfactor = factor*sdarea(i)/sarea(i)
        eff = eff + factor
        deff = deff + dfactor
        flux(i) = r_flux(j)
        ddflux(i) = 0.0
        free(i) = .false.
      endif
    enddo
  enddo
  !
  if (et_zut.eq.0) then
    error = .true.
    write(6,*) 'E-FLUX,  No valid reference source found'
    return
  endif
  !
  eff = eff/et_zut
  deff = deff/et_zut
  write(6,*) 'W-FLUX,  Efficiency(Jy/K) +-error on efficiency'
  write(6,10,err=999) eff, deff
  !
  do i=1, n_source
    flux(i) = sarea(i)*eff
    ddflux(i) = sdarea(i)*eff+deff*sarea(i)
  enddo
  !
  999   return
  !
  10    format(5x,f7.2,2x,f5.2)
end subroutine solve_flux
