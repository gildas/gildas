module flux_interfaces_none
  interface
    subroutine flux_file(line,error)
      !---------------------------------------------------------------------
      !     Command FLUX\FILE Name
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      logical :: error                  !
    end subroutine flux_file
  end interface
  !
  interface
    subroutine flux_index (line,error)
      !---------------------------------------------------------------------
      !  Command  FLUX\INDEX FileIn Source [/OUTPUT FileOut [NEW]]
      !     Compute spectral index for specified source from data in
      !     input file FileIn.
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      logical :: error                  !
    end subroutine flux_index
  end interface
  !
  interface
    subroutine index_flux(file,source,indx,dindx,   &
         &    freqin,freqout,fluxin,fluxou,error)
      !---------------------------------------------------------------------
      ! ASTRO This subroutine -reads the input file from FLUX\print
      !			-reads the frequencies limits
      !			-calculate the index from the fluxes and frequencies
      !
      ! Arguments:	Input: character*(80) file	name of input file
      !		Input: character*(12)  source   source name
      !		Output: real*8 indx		between freqin and freqout
      !		Output: real*8 dindx		error on indx
      !		Output: real*8 freqin 		low freq.
      !		Output: real*8 freqout		high freq.
      !		Output: real*8 fluxin 		flux at low freq. (freqin)
      !		Output: real*8 fluxou 		flux at high freq. (freqou)
      !		Input/Output: logical error
      !---------------------------------------------------------------------
      character(len=*) :: file          !
      character(len=*) :: source        !
      real*8 :: indx                    !
      real*8 :: dindx                   !
      real*8 :: freqin                  !
      real*8 :: freqout                 !
      real*8 :: fluxin                  !
      real*8 :: fluxou                  !
      logical :: error                  !
    end subroutine index_flux
  end interface
  !
  interface
    subroutine get_names(file,sou,nsou,msou)
      character(len=*) :: file          !
      integer :: msou                   !
      character(len=*) :: sou(msou)     !
      integer :: nsou                   !
    end subroutine get_names
  end interface
  !
  interface
    subroutine flux_print (line,error)
      !---------------------------------------------------------------------
      !     Command
      !     FLUX\PRINT FileOut [NEW]
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      logical :: error                  !
    end subroutine flux_print
  end interface
  !
  interface
    subroutine flux_read (line,error)
      !---------------------------------------------------------------------
      !     Command
      !     FLUX\READ Antenna Frequency
      !     with frequency in GHz
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      logical :: error                  !
    end subroutine flux_read
  end interface
  !
  interface
    subroutine read_flux(file,antenna,freqobs,date,n_source,   &
         &    source,sarea,sdarea,flux,gainis,error)
      !---------------------------------------------------------------------
      ! ASTRO This subroutine -reads the input file from CLASS or CLIC (print FLUX)
      !			-reads the selected antenna and frequency
      !			-corrects from pointing errors
      !			-corrects frequency from gains USB/LSB measurements
      !			-calculates the flux from planets (call to subroutine
      !	flux_planet at the corrected frequency)
      !
      ! Arguments:
      !   Input: character *(80) FILE	name of file from Class or Clic
      !   Input: integer ANTENNA	Antenna number
      !   Input: R*8 FREQOBS		Frequence selected to compute
      !                               fluxes (generaly 86 or 110 GHz).
      !   Output: Character*12 DATE 	Observing date
      !   Output: integer N_SOURCE 	Number of sources
      !                               (corrected from U/L gain).
      !   Output: character*12 SOURCE(N_SOURCE)	Name of source
      !   Output: real SAREA(N_SOURCE)		Mean area corrected
      !                                               from pointing (Sarea/Warea)
      !   Output: real SDAREA(N_SOURCE)		Errors on Area
      !   Output: real FLUX(N_SOURCE)		Flux from planets only
      !   Output: real GAINIS  		Gain I/S
      !   Input/Output: logical ERROR
      !---------------------------------------------------------------------
      character(len=*) :: file          !
      integer :: antenna                !
      real*8 :: freqobs                 !
      character(len=*) :: date          !
      integer :: n_source               !
      character(len=*) :: source(*)     !
      real*8 :: sarea(*)                !
      real*8 :: sdarea(*)               !
      real*8 :: flux(*)                 !
      real*4 :: gainis                  !
      logical :: error                  !
    end subroutine read_flux
  end interface
  !
  interface
    subroutine flux_set(line,error)
      !---------------------------------------------------------------------
      ! FLUX   Support for command
      !     ASTRO\SET FLUX Quasar [Flux|*]
      !        (only one value by SET command)
      !
      !  Quasar   CHARACTER*12	Quasar name
      !  Flux     REAL   		Flux (of the source)
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      logical :: error                  !
    end subroutine flux_set
  end interface
  !
  interface
    subroutine flux_solve(line,error)
      !---------------------------------------------------------------------
      ! commande FLUX\SOLVE NAME1 NAME2...
      !
      ! NAMEx est une PLANETE, sans argument prend TOUTES les planetes du
      ! fichier d'appel. Avec 1 argument: prend uniquement cette planete
      ! comme source de reference (plus les quasars definis par "set name flux")
      ! meme si il y a d'autres planetes dans le fichier d'entree.
      !
      ! Apres lecture de la commande et des arguments
      ! 1/ Appel read_flux
      ! 2/ Appel solve_flux
      ! pour calculer les efficacites et les flux
      !---------------------------------------------------------------------
      character(len=*) :: line          !
      logical :: error                  !
    end subroutine flux_solve
  end interface
  !
  interface
    subroutine solve_flux(n_source,source,sarea,sdarea,flux,ddflux,   &
         &    eff,deff,n_ref,refe,r_flux,free,error)
      !---------------------------------------------------------------------
      ! Subroutine to compute mean of the Antennas efficacity (Jy/K) and
      ! the fluxes from n_sources sources containing n_ref referenced sources
      ! named refe with the known flux r_flux.
      !
      ! Input/Output
      !	Input integer	N_SOURCE		number of sources
      !	Input character*12 source(n_source)	name of sources
      !	Input real*8 sarea(n_source)		mean of integrated area
      !(sarea/warea)
      !	Input real*8 sdarea(n_source)		error on area
      !	Output real*8 flux(n_source)		flux of sources
      !	Output real*8 ddflux(n_source)		error on flux of sources
      !	Output real*8 EFF			efficacity of the antenna
      !	Output real*8 DEFF			error on efficacity
      !	Input  integer N_REF			number of reference sources
      !	Input  character*12 refe(n_ref)		name of reference sources
      !	Input  real*8	r_flux(n_ref)		flux of reference sources
      !       Output logical  free(n_sources)            Source fixee ou Non
      !	Input logical error
      !---------------------------------------------------------------------
      integer :: n_source               !
      character(len=12) :: source(*)    !
      real*8 :: sarea(*)                !
      real*8 :: sdarea(*)               !
      real*8 :: flux(*)                 !
      real*8 :: ddflux(*)               !
      real*8 :: eff                     !
      real*8 :: deff                    !
      integer :: n_ref                  !
      character(len=12) :: refe(n_ref)  !
      real*8 :: r_flux(n_ref)           !
      logical :: free(*)                !
      logical :: error                  !
    end subroutine solve_flux
  end interface
  !
  interface
    subroutine planet_flux(name,date,fnu,flux,error)
      !---------------------------------------------------------------------
      !	FLUX Command :
      !		character*(*) name 	input
      !		character*(*) date 	input
      !		real*8 	FNU		input
      !		real*8	flux		output
      !		logical error 		in/output
      !---------------------------------------------------------------------
      character(len=12) :: name         !
      character(len=12) :: date         !
      real*8 :: fnu                     !
      real*8 :: flux                    !
      logical :: error                  !
    end subroutine planet_flux
  end interface
  !
  interface
    subroutine run_flux (line,comm,error)
      character(len=*) :: line          !
      character(len=*) :: comm          !
      logical :: error                  !
    end subroutine run_flux
  end interface
  !
  interface
    subroutine load_flux
      ! Global
    end subroutine load_flux
  end interface
  !
  interface
    subroutine flux_pack_set(pack)
      use gpack_def
      !----------------------------------------------------------------------
      ! Public package definition routine.
      ! It defines package methods and dependencies to other packages.
      !----------------------------------------------------------------------
      type(gpack_info_t), intent(out) :: pack ! Package info structure
    end subroutine flux_pack_set
  end interface
  !
  interface
    subroutine flux_pack_init(gpack_id,error)
      use sic_def
      !----------------------------------------------------------------------
      ! Private routine to set the FLUX environment.
      !----------------------------------------------------------------------
      integer :: gpack_id
      logical :: error
    end subroutine flux_pack_init
  end interface
  !
  interface
    subroutine flux_pack_clean(error)
      !----------------------------------------------------------------------
      ! Private routine to clean the FLUX environment
      ! (e.g. Remove temporary buffers, unregister languages...)
      !----------------------------------------------------------------------
      logical :: error
    end subroutine flux_pack_clean
  end interface
  !
end module flux_interfaces_none
