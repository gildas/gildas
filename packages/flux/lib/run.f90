subroutine run_flux (line,comm,error)
  character(len=*) :: line          !
  character(len=*) :: comm          !
  logical :: error                  !
  !
  if (comm.eq.'FILE') then
    call flux_file (line,error)
  elseif (comm.eq.'READ') then
    call flux_read (line,error)
  elseif (comm.eq.'SOLVE') then
    call flux_solve (line,error)
  elseif (comm.eq.'PRINT') then
    call flux_print (line,error)
  elseif (comm.eq.'INDEX') then
    call flux_index (line,error)
  else
    write(6,*) 'E-FLUX,  No code for ',comm
  endif
end subroutine run_flux
!
subroutine load_flux
  ! Global
  external :: run_flux
  logical, external :: gr_error
  ! Local
  integer :: mflux
  parameter (mflux=6)
  character(len=12) :: cflux(mflux)
  logical :: error
  data cflux/   &
     &    ' FILE', ' INDEX', '/OUTPUT',   &
     &    ' PRINT', ' READ', ' SOLVE'/
  !
  error = gr_error()
  call sic_begin('FLUX','GAG_HELP_FLUX',mflux,cflux,   &
     &    '1.1-00  20-FEB-1995  A.Dutrey',run_flux,gr_error)
end subroutine load_flux
