!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage the FLUX package
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine flux_pack_set(pack)
  use gpack_def
  use gkernel_interfaces
  !----------------------------------------------------------------------
  ! Public package definition routine.
  ! It defines package methods and dependencies to other packages.
  !----------------------------------------------------------------------
  type(gpack_info_t), intent(out) :: pack ! Package info structure
  !
  external :: astro_pack_set
  external :: flux_pack_init
  external :: flux_pack_clean
  !
  pack%name='flux'
  pack%ext='.flux'
  pack%depend(1:1) = (/ locwrd(astro_pack_set) /)
  pack%init=locwrd(flux_pack_init)
  pack%clean=locwrd(flux_pack_clean)
  !
end subroutine flux_pack_set
!
subroutine flux_pack_init(gpack_id,error)
  use sic_def
  !----------------------------------------------------------------------
  ! Private routine to set the FLUX environment.
  !----------------------------------------------------------------------
  integer :: gpack_id
  logical :: error
  !
  ! Set libraries id's
  !call flux_message_set_id(gpack_id)
  !
  ! Local language
  call load_flux
  !
end subroutine flux_pack_init
!
subroutine flux_pack_clean(error)
  !----------------------------------------------------------------------
  ! Private routine to clean the FLUX environment
  ! (e.g. Remove temporary buffers, unregister languages...)
  !----------------------------------------------------------------------
  logical :: error
  !
end subroutine flux_pack_clean
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
