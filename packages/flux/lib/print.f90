subroutine flux_print (line,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  !     Command
  !     FLUX\PRINT FileOut [NEW]
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'flux.inc'
  ! Local
  character(len=72) :: remark,file,filf
  character(len=4) :: nwou
  integer :: nc,nl,nf,i,ier
  !
  call sic_ch (line,0,1,file,nc,.true.,error)
  if (error) return
  call sic_parsef(file,filf,' ','.MES')
  nwou = 'OLD'
  call sic_ch (line,0,2,nwou,nc,.false.,error)
  if (error) return
  !
  if (nwou.eq.'NEW') then
    nf= lenc(filf)
    ier = sic_open(2,filf(1:nf),'NEW',.false.)
  else
    nf= lenc(filf)
    ier = sic_open(2,filf(1:nf),'APPEND',.false.)
  endif
  if (ier.ne.0) then
    call gagout('E-FLUX-PRINT,  Cannot open output file')
    call putios('E-FLUX_PRINT,  ',ier)
    error = .true.
    return
  endif
  !
  ! Ecriture du fichier:
  !
  write(2,*) '! Flux measurements from the ', udate
  write(2,10) ' ! Gain I/S is ', gais
  write(2,10) ' ! Equivalent frequency:', vfreqeq
  !
  ! Info complementaire libre:
  !
  write(6,*) 'Weather Conditions: '
  write(2,*) '! Weather Conditions: '
  nl = 1
  do while(nl.ne.0)
    read(5,'(A)',iostat=ier) remark
    nl = lenc(remark)
    if (nl.ne.0) write(2,*) '!   '//remark(1:nl)
  enddo
  write(6,*) 'Calibration Quality:'
  write(2,*) '! Calibration Quality:'
  nl = 1
  do while(nl.ne.0)
    read(5,'(A)',iostat=ier) remark
    nl = lenc(remark)
    if (nl.ne.0) write(2,*) '!   '//remark(1:nl)
  enddo
  write(6,*) ' Other Remarks '
  write(2,*) '! Other Remarks '
  nl = 1
  do while(nl.ne.0)
    read(5,'(A)',iostat=ier) remark
    nl = lenc(remark)
    if (nl.ne.0) write(2,*) '!   '//remark(1:nl)
  enddo
  write(2,*) '!'
  write(2,*) '! Antenna Number and Efficiency(Jy/K) +- error(Jy/K)'
  write(2,20) ' ! BUR',nantenna, effi, deffi
  write(2,*) '!'
  write(2,*) '! Reference	Flux(Jy)   '
  do i=1, n_ref
    write(2,30) ' !', rsource(i), rflux(i)
  enddo
  write(2,*) '!'
  write(2,*) '! Source   Flux(Jy) +-error on flux  '
  do i=1, nsources
    if (free(i)) then
      write(2,40) ssource(i), sflux(i), dsflux(i)
    else
      write(2,50) ssource(i), sflux(i), dsflux(i), 'Fixed'
    endif
  enddo
  close(unit=2)
  !
  10    format(a,2x,f7.2)
  20    format(a,i2,3x,f5.2,2x,f5.2)
  30    format(a,2x,a,5x,f8.2)
  40    format(3x,a,3x,f8.2,2x,f7.2)
  50    format(3x,a,3x,f8.2,2x,f7.2,5x,a)
end subroutine flux_print
