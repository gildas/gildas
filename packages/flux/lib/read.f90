subroutine flux_read (line,error)
  !---------------------------------------------------------------------
  !     Command
  !     FLUX\READ Antenna Frequency
  !     with frequency in GHz
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'flux.inc'
  !
  call sic_i4 (line,0,1,nantenna,.true.,error)
  if (error) return
  call sic_r8 (line,0,2,frequency,.true.,error)
end subroutine flux_read
!
subroutine read_flux(file,antenna,freqobs,date,n_source,   &
     &    source,sarea,sdarea,flux,gainis,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! ASTRO This subroutine -reads the input file from CLASS or CLIC (print FLUX)
  !			-reads the selected antenna and frequency
  !			-corrects from pointing errors
  !			-corrects frequency from gains USB/LSB measurements
  !			-calculates the flux from planets (call to subroutine
  !	flux_planet at the corrected frequency)
  !
  ! Arguments:
  !   Input: character *(80) FILE	name of file from Class or Clic
  !   Input: integer ANTENNA	Antenna number
  !   Input: R*8 FREQOBS		Frequence selected to compute
  !                               fluxes (generaly 86 or 110 GHz).
  !   Output: Character*12 DATE 	Observing date
  !   Output: integer N_SOURCE 	Number of sources
  !                               (corrected from U/L gain).
  !   Output: character*12 SOURCE(N_SOURCE)	Name of source
  !   Output: real SAREA(N_SOURCE)		Mean area corrected
  !                                               from pointing (Sarea/Warea)
  !   Output: real SDAREA(N_SOURCE)		Errors on Area
  !   Output: real FLUX(N_SOURCE)		Flux from planets only
  !   Output: real GAINIS  		Gain I/S
  !   Input/Output: logical ERROR
  !---------------------------------------------------------------------
  character(len=*) :: file          !
  integer :: antenna                !
  real*8 :: freqobs                 !
  character(len=*) :: date          !
  integer :: n_source               !
  character(len=*) :: source(*)     !
  real*8 :: sarea(*)                !
  real*8 :: sdarea(*)               !
  real*8 :: flux(*)                 !
  real*4 :: gainis                  !
  logical :: error                  !
  ! Local
  integer :: m_source
  parameter (m_source=100)
  character(len=12) :: sour,sdat
  integer :: i,ier
  real*8 :: area,darea, warea(m_source), nu(m_source)
  real :: gaini,snui,inui
  real :: gainj,snuj,inuj
  real :: az,el,time,daz,ddaz,waz,dwaz,areaj,daj
  real :: del,ddel,wel,dwel,areai,dai
  integer :: jnum,jscan,jcode,jant
  integer :: inum,iscan,icode,iant
  integer :: nf
  logical :: found, existe
  real*8 :: freqeq, dnui
  !
  ! Programme
  error = .false.
  !
  ier = 0
  nf = lenc(file)
  ier = sic_open (1,file(1:nf),'OLD',.true.)
  if (ier.ne.0) then
    call gagout('E-READ_FLUX,  Cannot open input file')
    call putios('E-READ_FLUX,  ',ier)
    error = .true.
    return
  endif
  read (1,100)
  read (1,100)
  read (1,100)
  read (1,100)
  !
  n_source = 0
  !
  existe = .true.
  dnui = 10.0
  iant = 0
  !
  date = ' '
  do while (.true.)
    ! Test sur la frequence et l'antenne:
    do while (antenna.ne.iant .or. abs(dnui).gt.1.0)
      read(1,*,end=10) inum,iscan,icode,az,el,time,daz,ddaz,iant,   &
     &        waz,dwaz,areai,dai,gaini,snui,inui,sour,sdat
      if (antenna.eq.iant) dnui = freqobs-snui
    enddo
    existe = .false.
    jant = 0
    jscan = 0
    if (date.ne.sdat) then
      call gagout('W-READ_FLUX,  Date changed, now '//sdat)
      date = sdat
    endif
    !
    ! Do NOT Assume consecutive Az & El data, but start with Az (or El)
    ! and now Search the associated El (resp Az) Scan
    do while (iant.ne.jant .or. iscan.ne.jscan)
      read(1,*,end=10) jnum,jscan,jcode,az,el,time,del,ddel,   &
     &        jant,wel,dwel,areaj,daj,gainj,snuj,inuj,sour
    enddo
    !
    ! Correct for pointing
    waz = waz/(2.*sqrt(log(2.)))   ! FWHM_TO_1/E
    wel = wel/(2.*sqrt(log(2.)))   ! FWHM_TO_1/E
    area = areai*exp((del/wel)**2)+areaj*exp((daz/waz)**2)
    area = 0.5*area
    darea =  (dai*exp((del/wel)**2))**2 +   &
     &      (daj*exp((daz/waz)**2))**2
    darea = 0.5*sqrt(darea)
    inui = (snui+gaini*inui)/(1+gaini)
    !
    found = .false.
    i = 1
    do while (.not.found .and. i.le.n_source)
      if (sour.eq.source(i)) then
        sarea(i) = sarea(i)+area
        sdarea(i) = sdarea(i)+darea**2
        warea(i) = warea(i)+1.0
        nu(i) = inui
        found = .true.
      endif
      i = i+1
    enddo
    if (.not.found) then
      n_source = n_source+1
      source(n_source) = sour
      sdarea(n_source) = darea**2  ! /SIGMA**2
      sarea(n_source) = area   ! /SIGMA**2
      warea(n_source) = 1      ! /SIGMA**2
      nu(n_source) = inui
    endif
    gainis = gaini
    ! Next data
    dnui = 10.0
    iant = 0
  enddo
  !
  ! Moyenne de l'aire + CAS DES PLANETES:
  10    continue
  close(unit=1)
  !
  if (existe) then
    error=.true.
    write(6,*) 'E-FLUX,  Wrong antenna ',antenna,   &
     &      ' or frequency ',freqobs
    goto 999
  endif
  !
  write(6,*) 'W-FLUX Sources in the file:'
  do i=1, n_source
    freqeq = nu(i)
    if (source(i).eq.'MERCURY' .or. source(i).eq.'VENUS'   .or.  &
        source(i).eq.'MARS'    .or. source(i).eq.'JUPITER' .or.  &
        source(i).eq.'SATURN'  .or. source(i).eq.'URANUS'  .or.  &
        source(i).eq.'NEPTUNE') then
      call planet_flux(source(i),date,freqeq,flux(i),error)
      if (error) goto 999
    else
      flux(i) = 0.0
    endif
    flux(i) = -flux(i)
    sarea(i) = sarea(i)/warea(i)
    sdarea(i) = sqrt(sdarea(i)/warea(i))
    write(6,110) source(i), 'weight:', warea(i)
  enddo
  !
  !True frequency of observation (gain corrected):
  freqobs = freqeq
  !
  100   format(a)
  110   format(5x,a,2x,a,1x,f2.0)
  !
  return
  999   error=.true.
end subroutine read_flux
