!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_reallocate_complex_cxyuv(kind,nc,nx,ny,nu,nv,array,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_reallocate_complex_cxyuv
  use wifisyn_array_types
  !-------------------------------------------------------------------
  ! @ public
  !-------------------------------------------------------------------
  character(len=*),   intent(in)    :: kind
  integer,            intent(in)    :: nc
  integer,            intent(in)    :: nx
  integer,            intent(in)    :: ny
  integer,            intent(in)    :: nu
  integer,            intent(in)    :: nv
  type(complex_5d_t), intent(inout) :: array
  logical,            intent(inout) :: error
  !
  logical :: allocate
  integer :: ier
  character(len=80) :: mess
  character(len=*), parameter :: rname='REALLOCATE/COMPLEX/CXYUV'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  allocate = .true.
  if (associated(array%val)) then
     if ((array%nc.eq.nc).and.&
          (array%nx.eq.nx).and.(array%ny.eq.ny).and.&
          (array%nu.eq.nu).and.(array%nv.eq.nv)) then
        write(mess,'(a,a,i0,a,i0,a,i0,a,i0,a,i0)') &
             kind,' already associated at the right size: ',nc,' x ',nx,' x ',ny,' x ',nu,' x ',nv
        call wifisyn_message(seve%i,rname,mess)
        allocate = .false.
     else
        write(mess,'(a,a,a)') 'Pointer ',kind,' already associated with a different size => Free it first'
        call wifisyn_message(seve%e,rname,mess)
        deallocate(array%val)
     endif
  endif
  if (allocate) then
     allocate(array%val(nc,nx,ny,nu,nv),stat=ier)
     if (failed_allocate(rname,kind,ier,error)) return
     write(mess,'(a,a,a,i0,a,i0,a,i0,a,i0,a,i0)') 'Allocated ',kind,' array of size: ',nc,' x ',nx,' x ',ny,' x ',nu,' x ',nv
     call wifisyn_message(seve%i,rname,mess)
  endif
  !
  ! Allocation success => Initialize
  array%nc = nc
  array%nx = nx
  array%ny = ny
  array%nu = nu
  array%nv = nv
  array%val = (0.0,0.0)
  !
end subroutine wifisyn_reallocate_complex_cxyuv
!
subroutine wifisyn_reallocate_complex_xyuv(kind,nx,ny,nu,nv,array,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_reallocate_complex_xyuv
  use wifisyn_array_types
  !-------------------------------------------------------------------
  ! @ public
  !-------------------------------------------------------------------
  character(len=*),   intent(in)    :: kind
  integer,            intent(in)    :: nx
  integer,            intent(in)    :: ny
  integer,            intent(in)    :: nu
  integer,            intent(in)    :: nv
  type(complex_4d_t), intent(inout) :: array
  logical,            intent(inout) :: error
  !
  logical :: allocate
  integer :: ier
  character(len=80) :: mess
  character(len=*), parameter :: rname='REALLOCATE/COMPLEX/XYUV'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  allocate = .true.
  if (associated(array%val)) then
     if ((array%nx.eq.nx).and.(array%ny.eq.ny).and.&
          (array%nu.eq.nu).and.(array%nv.eq.nv)) then
        write(mess,'(a,a,i0,a,i0,a,i0,a,i0)') &
             kind,' already associated at the right size: ',nx,' x ',ny,' x ',nu,' x ',nv
        call wifisyn_message(seve%i,rname,mess)
        allocate = .false.
     else
        write(mess,'(a,a,a)') 'Pointer ',kind,' already associated with a different size => Free it first'
        call wifisyn_message(seve%e,rname,mess)
        deallocate(array%val)
     endif
  endif
  if (allocate) then
     allocate(array%val(nx,ny,nu,nv),stat=ier)
     if (failed_allocate(rname,kind,ier,error)) return
     write(mess,'(a,a,a,i0,a,i0,a,i0,a,i0)') 'Allocated ',kind,' array of size: ',nx,' x ',ny,' x ',nu,' x ',nv
     call wifisyn_message(seve%i,rname,mess)
  endif
  !
  ! Allocation success => Initialize
  array%nx = nx
  array%ny = ny
  array%nu = nu
  array%nv = nv
  array%val = (0.0,0.0)
  !
end subroutine wifisyn_reallocate_complex_xyuv
!
subroutine wifisyn_free_complex_4d(array,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_free_complex_4d
  use wifisyn_array_types
  !-------------------------------------------------------------------
  ! @ public
  !-------------------------------------------------------------------
  type(complex_4d_t), intent(inout) :: array
  logical,            intent(inout) :: error
  !
  character(len=*), parameter :: rname='FREE/COMPLEX/4D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  array%nx = 0
  array%ny = 0
  array%nu = 0
  array%nv = 0
  if (associated(array%val)) deallocate(array%val)
  nullify(array%val)
  !
end subroutine wifisyn_free_complex_4d
!
subroutine wifisyn_free_complex_5d(array,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_free_complex_5d
  use wifisyn_array_types
  !-------------------------------------------------------------------
  ! @ public
  !-------------------------------------------------------------------
  type(complex_5d_t), intent(inout) :: array
  logical,            intent(inout) :: error
  !
  character(len=*), parameter :: rname='FREE/COMPLEX/5D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  array%nc = 0
  array%nx = 0
  array%ny = 0
  array%nu = 0
  array%nv = 0
  if (associated(array%val)) deallocate(array%val)
  nullify(array%val)
  !
end subroutine wifisyn_free_complex_5d
!
subroutine wifisyn_reallocate_complex_xyc(kind,nx,ny,nc,array,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_reallocate_complex_xyc
  use wifisyn_array_types
  !-------------------------------------------------------------------
  ! @ public
  !-------------------------------------------------------------------
  character(len=*),   intent(in)    :: kind
  integer,            intent(in)    :: nx
  integer,            intent(in)    :: ny
  integer,            intent(in)    :: nc
  type(complex_3d_t), intent(inout) :: array
  logical,            intent(inout) :: error
  !
  logical :: allocate
  integer :: ier
  character(len=80) :: mess
  character(len=*), parameter :: rname='REALLOCATE/COMPLEX/XYC'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  allocate = .true.
  if (associated(array%val)) then
     if ((array%nc.eq.nc).and.(array%nx.eq.nx).and.(array%ny.eq.ny)) then
        write(mess,'(a,a,i0,a,i0,a,i0)') kind,' already associated at the right size: ',nx,' x ',ny,' x ',nc
        call wifisyn_message(seve%i,rname,mess)
        allocate = .false.
     else
        write(mess,'(a,a,a)') 'Pointer ',kind,' already associated with a different size => Free it first'
        call wifisyn_message(seve%e,rname,mess)
        deallocate(array%val)
     endif
  endif
  if (allocate) then
     allocate(array%val(nx,ny,nc),stat=ier)
     if (failed_allocate(rname,kind,ier,error)) return
     write(mess,'(a,a,a,i0,a,i0,a,i0)') 'Allocated ',kind,' array of size: ',nx,' x ',ny,' x ',nc
     call wifisyn_message(seve%i,rname,mess)
  endif
  !
  ! Allocation success => Initialize
  array%nx = nx
  array%ny = ny
  array%nc = nc
  array%val = (0.0,0.0)
  !
end subroutine wifisyn_reallocate_complex_xyc
!
subroutine wifisyn_free_complex_3d(array,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_free_complex_3d
  use wifisyn_array_types
  !-------------------------------------------------------------------
  ! @ public
  !-------------------------------------------------------------------
  type(complex_3d_t), intent(inout) :: array
  logical,            intent(inout) :: error
  !
  character(len=*), parameter :: rname='FREE/COMPLEX/3D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  array%nx = 0
  array%ny = 0
  array%nc = 0
  if (associated(array%val)) deallocate(array%val)
  nullify(array%val)
  !
end subroutine wifisyn_free_complex_3d
!
subroutine wifisyn_allocate_complex_2d(kind,nx,ny,array,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_allocate_complex_2d
  use wifisyn_array_types
  !-------------------------------------------------------------------
  ! @ public
  !-------------------------------------------------------------------
  character(len=*),   intent(in)    :: kind
  integer,            intent(in)    :: nx
  integer,            intent(in)    :: ny
  type(complex_2d_t), intent(inout) :: array
  logical,            intent(inout) :: error
  !
  integer :: ier
  character(len=80) :: mess
  character(len=*), parameter :: rname='ALLOCATE/COMPLEX/2D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (.not.associated(array%val)) then
     allocate(array%val(nx,ny),stat=ier)
     if (failed_allocate(rname,kind,ier,error)) return
     write(mess,'(a,a,a,i0,a,i0)') 'Allocated ',kind,' array of size: ',nx,' x ',ny
     call wifisyn_message(seve%i,rname,mess)
  else
     write(mess,'(a,a,a)') 'Pointer ',kind,' already associated'
     call wifisyn_message(seve%e,rname,mess)
     error = .true.
     return
  endif
  !
  ! Allocation success => File array%n...
  array%nx = nx
  array%ny = ny
  !
end subroutine wifisyn_allocate_complex_2d
!
subroutine wifisyn_free_complex_2d(array,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_free_complex_2d
  use wifisyn_array_types
  !-------------------------------------------------------------------
  ! @ public
  !-------------------------------------------------------------------
  type(complex_2d_t), intent(inout) :: array
  logical,            intent(inout) :: error
  !
  character(len=*), parameter :: rname='FREE/COMPLEX/2D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  array%nx = 0
  array%ny = 0
  if (associated(array%val)) deallocate(array%val)
  nullify(array%val)
  !
end subroutine wifisyn_free_complex_2d
!
subroutine wifisyn_allocate_complex_1d(kind,n,array,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_allocate_complex_1d
  use wifisyn_array_types
  !-------------------------------------------------------------------
  ! @ public
  !-------------------------------------------------------------------
  character(len=*),   intent(in)    :: kind
  integer,            intent(in)    :: n
  type(complex_1d_t), intent(inout) :: array
  logical,            intent(inout) :: error
  !
  integer :: ier
  character(len=80) :: mess
  character(len=*), parameter :: rname='ALLOCATE/COMPLEX/1D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (.not.associated(array%val)) then
     allocate(array%val(n),stat=ier)
     if (failed_allocate(rname,kind,ier,error)) return
     write(mess,'(a,a,a,i0)') 'Allocated ',kind,' array of size: ',n
     call wifisyn_message(seve%i,rname,mess)
  else
     write(mess,'(a,a,a)') 'Pointer ',kind,' already associated'
     call wifisyn_message(seve%e,rname,mess)
     error = .true.
     return
  endif
  !
  ! Allocation success => File array%n...
  array%n = n
  !
end subroutine wifisyn_allocate_complex_1d
!
subroutine wifisyn_free_complex_1d(array,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_free_complex_1d
  use wifisyn_array_types
  !-------------------------------------------------------------------
  ! @ public
  !-------------------------------------------------------------------
  type(complex_1d_t), intent(inout) :: array
  logical,            intent(inout) :: error
  !
  character(len=*), parameter :: rname='FREE/COMPLEX/1D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  array%n = 0
  if (associated(array%val)) deallocate(array%val)
  nullify(array%val)
  !
end subroutine wifisyn_free_complex_1d
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_allocate_dble_2d(kind,nx,ny,array,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_allocate_dble_2d
  use wifisyn_array_types
  !-------------------------------------------------------------------
  ! @ public
  !-------------------------------------------------------------------
  character(len=*), intent(in)    :: kind
  integer,          intent(in)    :: nx
  integer,          intent(in)    :: ny
  type(dble_2d_t),  intent(inout) :: array
  logical,          intent(inout) :: error
  !
  integer :: ier
  character(len=80) :: mess
  character(len=*), parameter :: rname='ALLOCATE/DBLE/2D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (.not.associated(array%val)) then
     allocate(array%val(nx,ny),stat=ier)
     if (failed_allocate(rname,kind,ier,error)) return
     write(mess,'(a,a,a,i0,a,i0)') 'Allocated ',kind,' array of size: ',nx,' x ',ny
     call wifisyn_message(seve%i,rname,mess)
  else
     write(mess,'(a,a,a)') 'Pointer ',kind,' already associated'
     call wifisyn_message(seve%e,rname,mess)
     error = .true.
     return
  endif
  !
  ! Allocation success => File array%n...
  array%nx = nx
  array%ny = ny
  !
end subroutine wifisyn_allocate_dble_2d
!
subroutine wifisyn_reallocate_dble_1d(kind,n,array,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_reallocate_dble_1d
  use wifisyn_array_types
  !-------------------------------------------------------------------
  ! @ public
  !-------------------------------------------------------------------
  character(len=*), intent(in)    :: kind
  integer,          intent(in)    :: n
  type(dble_1d_t),  intent(inout) :: array
  logical,          intent(inout) :: error
  !
  logical :: allocate
  integer :: ier
  character(len=160) :: mess
  character(len=*), parameter :: rname='REALLOCATE/DBLE/1D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  allocate = .true.
  if (associated(array%val)) then
     if (array%n.eq.n) then
        write(mess,'(a,a,i0)') kind,' already associated at the right size: ',n
        call wifisyn_message(seve%i,rname,mess)
        allocate = .false.
     else
        write(mess,'(a,a,a)') 'Pointer ',kind,' already associated with a different size => Free it first'
        call wifisyn_message(seve%e,rname,mess)
        deallocate(array%val)
        array%n = 0
     endif
  endif
  if (allocate) then
     allocate(array%val(n),stat=ier)
     if (failed_allocate(rname,kind,ier,error)) return
     write(mess,'(a,a,a,i0)') 'Allocated ',kind,' array of size: ',n
     call wifisyn_message(seve%i,rname,mess)
  endif
  !
  ! Allocation success => File array%n...
  array%n = n
  !
end subroutine wifisyn_reallocate_dble_1d
!
subroutine wifisyn_free_dble_1d(array,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_free_dble_1d
  use wifisyn_array_types
  !-------------------------------------------------------------------
  ! @ public
  !-------------------------------------------------------------------
  type(dble_1d_t), intent(inout) :: array
  logical,         intent(inout) :: error
  !
  character(len=*), parameter :: rname='FREE/DBLE/1D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  array%n = 0
  if (associated(array%val)) deallocate(array%val)
  nullify(array%val)
  !
end subroutine wifisyn_free_dble_1d
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_allocate_real_2d(kind,nx,ny,array,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_allocate_real_2d
  use wifisyn_array_types
  !-------------------------------------------------------------------
  ! @ public
  !-------------------------------------------------------------------
  character(len=*), intent(in)    :: kind
  integer,          intent(in)    :: nx
  integer,          intent(in)    :: ny
  type(real_2d_t),  intent(inout) :: array
  logical,          intent(inout) :: error
  !
  integer :: ier
  character(len=80) :: mess
  character(len=*), parameter :: rname='ALLOCATE/REAL/2D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (.not.associated(array%val)) then
     allocate(array%val(nx,ny),stat=ier)
     if (failed_allocate(rname,kind,ier,error)) return
     write(mess,'(a,a,a,i0,a,i0)') 'Allocated ',kind,' array of size: ',nx,' x ',ny
     call wifisyn_message(seve%i,rname,mess)
  else
     write(mess,'(a,a,a)') 'Pointer ',kind,' already associated'
     call wifisyn_message(seve%e,rname,mess)
     error = .true.
     return
  endif
  !
  ! Allocation success => File array%n...
  array%nx = nx
  array%ny = ny
  !
end subroutine wifisyn_allocate_real_2d
!
subroutine wifisyn_free_real_2d(array,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_free_real_2d
  use wifisyn_array_types
  !-------------------------------------------------------------------
  ! @ public
  !-------------------------------------------------------------------
  type(real_2d_t), intent(inout) :: array
  logical,         intent(inout) :: error
  !
  character(len=*), parameter :: rname='FREE/REAL/2D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  array%nx = 0
  array%ny = 0
  if (associated(array%val)) deallocate(array%val)
  nullify(array%val)
  !
end subroutine wifisyn_free_real_2d
!
subroutine wifisyn_reallocate_real_1d(kind,n,array,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_reallocate_real_1d
  use wifisyn_array_types
  !-------------------------------------------------------------------
  ! @ public
  !-------------------------------------------------------------------
  character(len=*), intent(in)    :: kind
  integer,          intent(in)    :: n
  type(real_1d_t),  intent(inout) :: array
  logical,          intent(inout) :: error
  !
  logical :: allocate
  integer :: ier
  character(len=160) :: mess
  character(len=*), parameter :: rname='REALLOCATE/REAL/1D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  allocate = .true.
  if (associated(array%val)) then
     if (array%n.eq.n) then
        write(mess,'(a,a,i0)') kind,' already associated at the right size: ',n
        call wifisyn_message(seve%i,rname,mess)
        allocate = .false.
     else
        write(mess,'(a,a,a)') 'Pointer ',kind,' already associated with a different size => Free it first'
        call wifisyn_message(seve%e,rname,mess)
        deallocate(array%val)
        array%n = 0
     endif
  endif
  if (allocate) then
     allocate(array%val(n),stat=ier)
     if (failed_allocate(rname,kind,ier,error)) return
     write(mess,'(a,a,a,i0)') 'Allocated ',kind,' array of size: ',n
     call wifisyn_message(seve%i,rname,mess)
  endif
  !
  ! Allocation success => File array%n...
  array%n = n
  !
end subroutine wifisyn_reallocate_real_1d
!
subroutine wifisyn_free_real_1d(array,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_free_real_1d
  use wifisyn_array_types
  !-------------------------------------------------------------------
  ! @ public
  !-------------------------------------------------------------------
  type(real_1d_t), intent(inout) :: array
  logical,         intent(inout) :: error
  !
  character(len=*), parameter :: rname='FREE/REAL/1D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  array%n = 0
  if (associated(array%val)) deallocate(array%val)
  nullify(array%val)
  !
end subroutine wifisyn_free_real_1d
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_allocate_inte_1d(kind,n,array,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_allocate_inte_1d
  use wifisyn_array_types
  !-------------------------------------------------------------------
  ! @ public
  !-------------------------------------------------------------------
  character(len=*), intent(in)    :: kind
  integer,          intent(in)    :: n
  type(inte_1d_t),  intent(inout) :: array
  logical,          intent(inout) :: error
  !
  integer :: ier
  character(len=80) :: mess
  character(len=*), parameter :: rname='ALLOCATE/INTE/1D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (.not.associated(array%val)) then
     allocate(array%val(n),stat=ier)
     if (failed_allocate(rname,kind,ier,error)) return
     write(mess,'(a,a,a,i0)') 'Allocated ',kind,' array of size: ',n
     call wifisyn_message(seve%i,rname,mess)
  else
     write(mess,'(a,a,a)') 'Pointer ',kind,' already associated'
     call wifisyn_message(seve%e,rname,mess)
     error = .true.
     return
  endif
  !
  ! Allocation success => File array%n...
  array%n = n
  !
end subroutine wifisyn_allocate_inte_1d
!
subroutine wifisyn_free_inte_1d(array,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_free_inte_1d
  use wifisyn_array_types
  !-------------------------------------------------------------------
  ! @ public
  !-------------------------------------------------------------------
  type(inte_1d_t), intent(inout) :: array
  logical,         intent(inout) :: error
  !
  character(len=*), parameter :: rname='FREE/INTE/1D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  array%n = 0
  if (associated(array%val)) deallocate(array%val)
  nullify(array%val)
  !
end subroutine wifisyn_free_inte_1d
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_allocate_logi_1d(kind,n,array,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_allocate_logi_1d
  use wifisyn_array_types
  !-------------------------------------------------------------------
  ! @ public
  !-------------------------------------------------------------------
  character(len=*), intent(in)    :: kind
  integer,          intent(in)    :: n
  type(logi_1d_t),  intent(inout) :: array
  logical,          intent(inout) :: error
  !
  integer :: ier
  character(len=80) :: mess
  character(len=*), parameter :: rname='ALLOCATE/LOGI/1D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (.not.associated(array%val)) then
     allocate(array%val(n),stat=ier)
     if (failed_allocate(rname,kind,ier,error)) return
     write(mess,'(a,a,a,i0)') 'Allocated ',kind,' array of size: ',n
     call wifisyn_message(seve%i,rname,mess)
  else
     write(mess,'(a,a,a)') 'Pointer ',kind,' already associated'
     call wifisyn_message(seve%e,rname,mess)
     error = .true.
     return
  endif
  !
  ! Allocation success => File array%n...
  array%n = n
  !
end subroutine wifisyn_allocate_logi_1d
!
subroutine wifisyn_free_logi_1d(array,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_free_logi_1d
  use wifisyn_array_types
  !-------------------------------------------------------------------
  ! @ public
  !-------------------------------------------------------------------
  type(logi_1d_t), intent(inout) :: array
  logical,         intent(inout) :: error
  !
  character(len=*), parameter :: rname='FREE/LOGI/1D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  array%n = 0
  if (associated(array%val)) deallocate(array%val)
  nullify(array%val)
  !
end subroutine wifisyn_free_logi_1d
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
