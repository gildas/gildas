!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_uvsym_command(line,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_uvsym_command
  use wifisyn_cube_buffer
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  logical,          intent(inout) :: error
  !
  character(len=*), parameter :: rname='UVSYM/COMMAND'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
!  call wifisyn_uvsym_check_wifi(wifi,error)
!  if (error) return
  call wifisyn_uvsym_check_visi(visi,error)
  if (error) return
  !
end subroutine wifisyn_uvsym_command
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_uvsym_check_wifi(wifi,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_uvsym_check_wifi
  use wifisyn_cube_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(wifi_t), intent(inout) :: wifi
  logical,      intent(inout) :: error
  !
  integer :: ic,nc
  integer :: ix,jx,nx
  integer :: iy,jy,ny
  integer :: iu,ju,nu
  integer :: iv,jv,nv,mv
  complex :: complx1,complx2
  character(len=*), parameter :: rname='CHECK/WIFI'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  nc = wifi%cub%nc
  nx = wifi%cub%nx
  ny = wifi%cub%ny
  nu = wifi%cub%nu
  nv = wifi%cub%nv
  mv = nv/2
  if (wifi%uvkind) then
     if (wifi%xykind) then
        call wifisyn_message(seve%i,rname,'Checking hermitian symmetry of wifi XYUV space')
        do iv=mv+1,nv
           jv = nv+2-iv
           do iu=2,nu
              ju = nu+2-iu
              do iy=1,ny
                 jy = ny+2-iy
                 do ix=1,nx
                    jx = nx+2-ix
                    do ic=1,nc
                       complx1 = wifi%cub%val(ic,ix,iy,iu,iv)
                       complx2 = conjg(wifi%cub%val(ic,jx,jy,ju,jv))
                       wifi%cub%val(ic,ix,iy,iu,iv) = complx1-complx2
                       wifi%cub%val(ic,jx,jy,ju,jv) = complx1-complx2
                    enddo ! ic
                 enddo ! ix
              enddo ! iy
           enddo ! iu
        enddo ! iv
     else
        call wifisyn_message(seve%i,rname,'Checking hermitian symmetry of wifi UV space')
        do iv=mv+1,nv
           jv = nv+2-iv
           do iu=2,nu
              ju = nu+2-iu
              do iy=1,ny
                 do ix=1,nx
                    do ic=1,nc
                       complx1 = wifi%cub%val(ic,ix,iy,iu,iv)
                       complx2 = conjg(wifi%cub%val(ic,ix,iy,ju,jv))
                       wifi%cub%val(ic,ix,iy,iu,iv) = complx1-complx2
                       wifi%cub%val(ic,ix,iy,ju,jv) = complx1-complx2
                    enddo ! ic
                 enddo ! ix
              enddo ! iy
           enddo ! iu
        enddo ! iv
     endif
  else
     call wifisyn_message(seve%i,rname,'Checking real character of wifi UV space')
  endif
  !
end subroutine wifisyn_uvsym_check_wifi
!
subroutine wifisyn_uvsym_check_visi(visi,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_uvsym_check_visi
  use wifisyn_cube_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(visi_t), intent(inout) :: visi
  logical,      intent(inout) :: error
  !
  integer :: ic,nc
  integer :: iu,ju,nu
  integer :: iv,jv,nv,mv
  complex :: complx1,complx2
  character(len=*), parameter :: rname='CHECK/VISI'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  nc = visi%cub%nc
  nu = visi%cub%nx
  nv = visi%cub%ny
  mv = nv/2
  if (visi%uvkind) then
     call wifisyn_message(seve%i,rname,'Checking hermitian symmetry of visi cube')
     do iv=mv+1,nv
        jv = nv+2-iv
        do iu=2,nu
           ju = nu+2-iu
           do ic=1,nc
              complx1 = visi%cub%val(iu,iv,ic)
              complx2 = conjg(visi%cub%val(ju,jv,ic))
              visi%cub%val(iu,iv,ic) = complx1-complx2
              visi%cub%val(ju,jv,ic) = complx1-complx2
           enddo ! ic
       enddo ! iu
     enddo ! iv
  else
     call wifisyn_message(seve%i,rname,'Checking real character of visi cube')
  endif
  !
end subroutine wifisyn_uvsym_check_visi
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
