!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_convolution_definition(pixinc,kern,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_convolution_definition
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  ! Determine default parameters for the convolution functions when
  ! they are not specified. Default convolving type is a spheroidal
  ! function. Any other unspecified values (= 0.0) will be set to
  ! some value.
  !----------------------------------------------------------------------
  real(kind=8),      intent(in)    :: pixinc
  type(kernel_1d_t), intent(inout) :: kern
  logical,           intent(inout) :: error
  !
  character(len=*), parameter :: rname='GRIDDING'
  character(len=256) :: mess
  character(len=12) :: names(5)
  integer :: nparms(5)
  data nparms /1, 3, 2, 4, 2/
  data names /'Pillbox','Exponential','Sinc','Exp*Sinc','Spheroidal'/
  !
  select case (kern%type)
  case (code_pillbox)
     if (kern%parm(1).le.0.0) kern%parm(1) = 0.5
  case (code_exponential)
     if (kern%parm(1).le.0.0) kern%parm(1) = 3.0
     if (kern%parm(2).le.0.0) kern%parm(2) = 1.00
     if (kern%parm(3).le.0.0) kern%parm(3) = 2.00
  case (code_sinc)
     if (kern%parm(1).le.0.0) kern%parm(1) = 3.0
     if (kern%parm(2).le.0.0) kern%parm(2) = 1.14
  case (code_exp_sinc)
     if (kern%parm(1).le.0.0) kern%parm(1) = 3.0
     if (kern%parm(2).le.0.0) kern%parm(2) = 1.55
     if (kern%parm(3).le.0.0) kern%parm(3) = 2.52
     if (kern%parm(4).le.0.0) kern%parm(4) = 2.00
  case (code_spheroidal)
     if (kern%parm(1).le.0.0) kern%parm(1) = 3.0
     if (kern%parm(2).le.0.0) kern%parm(2) = 1.0
  case default
     write(mess,'(a,i0)') 'Unknown kernel type: ',kern%type
     call wifisyn_message(seve%e,rname,mess)
     error = .true.
     return
  end select
  ! From here on, kern%type has a known value => we can use it safely
  kern%nparm = nparms(kern%type)
  kern%name = names(kern%type)
  kern%sup = abs(kern%parm(1)*pixinc)
  !
end subroutine wifisyn_convolution_definition
!
subroutine wifisyn_convolution_computation(kern,error)
  use gbl_message
  use phys_const
  use wifisyn_interfaces, except_this=>wifisyn_convolution_computation
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  ! Compute the convolving functions and stores them in the supplied
  ! buffer. Values are tabulated every 1/100th of grid pixel.
  ! PARM(1) = radius of support in grid pixel unit
  !----------------------------------------------------------------------
  type(kernel_1d_t), intent(inout) :: kern
  logical,           intent(inout) :: error
  !
  integer :: i,im,ialf,ier,nhalf,nfn
  real(kind=4) :: p1,p2,u,umax,absu,eta,psi
  character(len=80) :: mess
  character(len=*), parameter :: rname = 'GRIDDING'
  !
  ! Number of pixels in kernel
  ! The following ensures that kern%zero = kern%fn%n/2+1  (this is mandatory)
  kern%fac = 100
  kern%zero = ceiling(kern%fac*max(kern%parm(1),1.0))
  kern%fn%n = 2*(kern%zero-1)
  umax = kern%parm(1)
  !
  ! Allocate buffer
  if (kern%fn%n.gt.4096) then
     write(mess,*) 'Large work buffer size: ',kern%fn%n,' > 4096'
     call wifisyn_message(seve%w,rname,mess)
  endif
  call wifisyn_reallocate_real_1d('Kernel function',kern%fn%n,kern%fn,error)
  if (error) return
  !
  ! Compute kernel
  select case (kern%type)
  case (code_pillbox)
     do i = 1,kern%fn%n
        u = (i-kern%zero) / kern%fac
        absu = abs (u)
        if (absu.lt.umax) then
           kern%fn%val(i) = 1.0
        elseif (absu.eq.umax) then
           kern%fn%val(i) = 0.5
        else
           kern%fn%val(i) = 0.0
        endif
     enddo
  case (code_exponential)
     p1 = 1.0 / kern%parm(2)
     do i = 1,kern%fn%n
        u = (i-kern%zero) / kern%fac
        absu = abs (u)
        if (absu.gt.umax) then
           kern%fn%val(i) = 0.0
        else
           kern%fn%val(i) = exp (-((p1*absu) ** kern%parm(3)))
        endif
     enddo
  case (code_sinc)
     p1 = pi / kern%parm(2)
     do i = 1,kern%fn%n
        u = (i-kern%zero) / kern%fac
        absu = abs (u)
        if (absu.gt.umax) then
           kern%fn%val(i) = 0.0
        elseif (absu.eq.0.0) then
           kern%fn%val(i) = 1.0
        else
           kern%fn%val(i) = sin (p1*absu) / (p1*absu)
        endif
     enddo
  case (code_exp_sinc)
     p1 = pi / kern%parm(2)
     p2 = 1.0 / kern%parm(3)
     do i = 1,kern%fn%n
        u = (i-kern%zero) / kern%fac
        absu = abs (u)
        if (absu.gt.umax) then
           kern%fn%val(i) = 0.0
        elseif (absu.lt.0.01) then
           kern%fn%val(i) = 1.0
        else
           kern%fn%val(i) = sin(u*p1) / (u*p1) * exp (-((absu * p2) ** kern%parm(4)))
        endif
     enddo
  case (code_spheroidal)
     kern%fn%val = 0.0
     ialf = 2.0 * kern%parm(2) + 1.1
     if ((ialf.lt.1).or.(5.lt.ialf)) then
        write(mess,'(a,i0,a)') 'Spheroidal kernel: ialf (',ialf,') outside authorized range [1-5]'
        call wifisyn_message(seve%w,rname,mess)
        ialf = max (1, min (5, ialf))
        write(mess,'(a,i0)') 'Spheroidal kernel: ialf is set to ',ialf
        call wifisyn_message(seve%w,rname,mess)
     endif
     im = 2.0 * kern%parm(1) + 0.1
     if ((im.lt.4).or.(8.lt.im)) then
        write(mess,'(a,i0,a)') 'Spheroidal kernel: im (',im,') outside authorized range [4-8]'
        call wifisyn_message(seve%w,rname,mess)
        im = max (4, min (8, im))
        write(mess,'(a,i0)') 'Spheroidal kernel: im is set to ',im
        call wifisyn_message(seve%w,rname,mess)
     endif
     ! The following assumes that kern%zero = kern%fn%val/2+1
     nfn = kern%fn%n
     nhalf = nfn/2
     do i = 1,nhalf+1
        eta = float ((nhalf+1)-i) / float (nhalf)
        call sphfn (ialf, im, 0, eta, psi, ier)
        kern%fn%val(i) = psi
     enddo
     do i = nhalf+2,nfn
        kern%fn%val(i) = kern%fn%val(nfn+2-i)
     enddo
  case default
     kern%type = code_exp_sinc
     kern%parm(1) = 3.0
     kern%parm(2) = 1.55
     kern%parm(3) = 2.52
     kern%parm(4) = 2.00
     p1 = pi / kern%parm(2)
     p2 = 1.0 / kern%parm(3)
     do i = 1,kern%fn%n
        u = (i-kern%zero) / kern%fac
        absu = abs (u)
        if (absu.gt.umax) then
           kern%fn%val(i) = 0.0
        elseif (absu.lt.0.01) then
           kern%fn%val(i) = 1.0
        else
           kern%fn%val(i) = sin(u*p1) / (u*p1) * exp (-((absu * p2) ** kern%parm(4)))
        endif
     enddo
  end select
  !
end subroutine wifisyn_convolution_computation
!
subroutine sphfn(ialf,im,iflag,eta,psi,ier)
  use gbl_message
  use wifisyn_interfaces, except_this=>sphfn
  !---------------------------------------------------------------------
  ! @ private
  ! SPHFN is a subroutine to evaluate rational approximations to selected
  ! zero-order spheroidal functions, psi(c,eta), which are, in a
  ! sense defined in VLA Scientific Memorandum No. 132, optimal for
  ! gridding interferometer data.  The approximations are taken from
  ! VLA Computer Memorandum No. 156.  The parameter c is related to the
  ! support width, m, of the convoluting function according to c=
  ! pi*m/2.  The parameter alpha determines a weight function in the
  ! definition of the criterion by which the function is optimal.
  ! SPHFN incorporates approximations to 25 of the spheroidal functions,
  ! corresponding to 5 choices of m (4, 5, 6, 7, or 8 cells)
  ! and 5 choices of the weighting exponent (0, 1/2, 1, 3/2, or 2).
  ! 
  ! Input:
  !   IALF    I*4   Selects the weighting exponent, alpha.  IALF =
  !                 1, 2, 3, 4, and 5 correspond, respectively, to
  !                 alpha = 0, 1/2, 1, 3/2, and 2.
  !   IM      I*4   Selects the support width m, (=IM) and, correspond-
  !                 ingly, the parameter c of the spheroidal function.
  !                 Only the choices 4, 5, 6, 7, and 8 are allowed.
  !   IFLAG   I*4   Chooses whether the spheroidal function itself, or
  !                 its Fourier transform, is to be approximated.  The
  !                 latter is appropriate for gridding, and the former
  !                 for the u-v plane convolution.  The two differ on-
  !                 by a factor (1-eta**2)**alpha.  IFLAG less than or
  !                 equal to zero chooses the function appropriate for
  !                 gridding, and IFLAG positive chooses its F.T.
  !   ETA     R*4   Eta, as the argument of the spheroidal function, is
  !                 a variable which ranges from 0 at the center of the
  !                 convoluting function to 1 at its edge (also from 0
  !                 at the center of the gridding correction function
  !                 to unity at the edge of the map).
  ! 
  ! Output:
  !   PSI      R*4  The function value which, on entry to the subrou-
  !                 tine, was to have been computed.
  !   IER      I*4  An error flag whose meaning is as follows:
  !                    IER = 0  =>  No evident problem.
  !                          1  =>  IALF is outside the allowed range.
  !                          2  =>  IM is outside of the allowed range.
  !                          3  =>  ETA is larger than 1 in absolute
  !                                    value.
  !                         12  =>  IALF and IM are out of bounds.
  !                         13  =>  IALF and ETA are both illegal.
  !                         23  =>  IM and ETA are both illegal.
  !                        123  =>  IALF, IM, and ETA all are illegal.
  !---------------------------------------------------------------------
  integer(kind=4), intent(in)  :: ialf
  integer(kind=4), intent(in)  :: im
  integer(kind=4), intent(in)  :: iflag
  real(kind=4),    intent(in)  :: eta
  real(kind=4),    intent(out) :: psi
  integer(kind=4), intent(out) :: ier
  !
  integer :: j
  real(kind=4) :: alpha(5),eta2,x
  real(kind=4) :: p4(5,5), q4(2,5), p5(7,5), q5(5), p6l(5,5), q6l(2,5), &
       p6u(5,5), q6u(2,5), p7l(5,5), q7l(2,5), p7u(5,5), &
       q7u(2,5), p8l(6,5), q8l(2,5), p8u(6,5), q8u(2,5)
  character(len=80) :: mess
  character(len=*), parameter :: rname = 'SPHEROIDAL'
  !
  data alpha / 0., .5, 1., 1.5, 2. /
  data p4 /   &
       &    1.584774e-2, -1.269612e-1,  2.333851e-1, -1.636744e-1,   &
       &    5.014648e-2,  3.101855e-2, -1.641253e-1,  2.385500e-1,   &
       &   -1.417069e-1,  3.773226e-2,  5.007900e-2, -1.971357e-1,   &
       &    2.363775e-1, -1.215569e-1,  2.853104e-2,  7.201260e-2,   &
       &   -2.251580e-1,  2.293715e-1, -1.038359e-1,  2.174211e-2,   &
       &    9.585932e-2, -2.481381e-1,  2.194469e-1, -8.862132e-2,   &
       &    1.672243e-2 /
  data q4 /   &
       &    4.845581e-1,  7.457381e-2,  4.514531e-1,  6.458640e-2,   &
       &    4.228767e-1,  5.655715e-2,  3.978515e-1,  4.997164e-2,   &
       &    3.756999e-1,  4.448800e-2 /
  data p5 /   &
       &    3.722238e-3, -4.991683e-2,  1.658905e-1, -2.387240e-1,   &
       &    1.877469e-1, -8.159855e-2,  3.051959e-2,  8.182649e-3,   &
       &   -7.325459e-2,  1.945697e-1, -2.396387e-1,  1.667832e-1,   &
       &   -6.620786e-2,  2.224041e-2,  1.466325e-2, -9.858686e-2,   &
       &    2.180684e-1, -2.347118e-1,  1.464354e-1, -5.350728e-2,   &
       &    1.624782e-2,  2.314317e-2, -1.246383e-1,  2.362036e-1,   &
       &   -2.257366e-1,  1.275895e-1, -4.317874e-2,  1.193168e-2,   &
       &    3.346886e-2, -1.503778e-1,  2.492826e-1, -2.142055e-1,   &
       &    1.106482e-1, -3.486024e-2,  8.821107e-3 /
  data q5 /   &
       &    2.418820e-1,  2.291233e-1,  2.177793e-1,  2.075784e-1,   &
       &    1.983358e-1 /
  data p6l /   &
       &    5.613913e-2, -3.019847e-1,  6.256387e-1, -6.324887e-1,   &
       &    3.303194e-1,  6.843713e-2, -3.342119e-1,  6.302307e-1,   &
       &   -5.829747e-1,  2.765700e-1,  8.203343e-2, -3.644705e-1,   &
       &    6.278660e-1, -5.335581e-1,  2.312756e-1,  9.675562e-2,   &
       &   -3.922489e-1,  6.197133e-1, -4.857470e-1,  1.934013e-1,   &
       &    1.124069e-1, -4.172349e-1,  6.069622e-1, -4.405326e-1,   &
       &    1.618978e-1 /
  data q6l /   &
       &    9.077644e-1,  2.535284e-1,  8.626056e-1,  2.291400e-1,   &
       &    8.212018e-1,  2.078043e-1,  7.831755e-1,  1.890848e-1,   &
       &    7.481828e-1,  1.726085e-1 /
  data p6u /   &
       &    8.531865e-4, -1.616105e-2,  6.888533e-2, -1.109391e-1,   &
       &    7.747182e-2,  2.060760e-3, -2.558954e-2,  8.595213e-2,   &
       &   -1.170228e-1,  7.094106e-2,  4.028559e-3, -3.697768e-2,   &
       &    1.021332e-1, -1.201436e-1,  6.412774e-2,  6.887946e-3,   &
       &   -4.994202e-2,  1.168451e-1, -1.207733e-1,  5.744210e-2,   &
       &    1.071895e-2, -6.404749e-2,  1.297386e-1, -1.194208e-1,   &
       &    5.112822e-2 /
  data q6u /   &
       &    1.101270e+0,  3.858544e-1,  1.025431e+0,  3.337648e-1,   &
       &    9.599102e-1,  2.918724e-1,  9.025276e-1,  2.575336e-1,   &
       &    8.517470e-1,  2.289667e-1 /
  data p7l /   &
       &    2.460495e-2, -1.640964e-1,  4.340110e-1, -5.705516e-1,   &
       &    4.418614e-1,  3.070261e-2, -1.879546e-1,  4.565902e-1,   &
       &   -5.544891e-1,  3.892790e-1,  3.770526e-2, -2.121608e-1,   &
       &    4.746423e-1, -5.338058e-1,  3.417026e-1,  4.559398e-2,   &
       &   -2.362670e-1,  4.881998e-1, -5.098448e-1,  2.991635e-1,   &
       &    5.432500e-2, -2.598752e-1,  4.974791e-1, -4.837861e-1,   &
       &    2.614838e-1 /
  data q7l /   &
       &    1.124957e+0,  3.784976e-1,  1.075420e+0,  3.466086e-1,   &
       &    1.029374e+0,  3.181219e-1,  9.865496e-1,  2.926441e-1,   &
       &    9.466891e-1,  2.698218e-1 /
  data p7u /   &
       &    1.924318e-4, -5.044864e-3,  2.979803e-2, -6.660688e-2,   &
       &    6.792268e-2,  5.030909e-4, -8.639332e-3,  4.018472e-2,   &
       &   -7.595456e-2,  6.696215e-2,  1.059406e-3, -1.343605e-2,   &
       &    5.135360e-2, -8.386588e-2,  6.484517e-2,  1.941904e-3,   &
       &   -1.943727e-2,  6.288221e-2, -9.021607e-2,  6.193000e-2,   &
       &    3.224785e-3, -2.657664e-2,  7.438627e-2, -9.500554e-2,   &
       &    5.850884e-2 /
  data q7u /   &
       &    1.450730e+0,  6.578685e-1,  1.353872e+0,  5.724332e-1,   &
       &    1.269924e+0,  5.032139e-1,  1.196177e+0,  4.460948e-1,   &
       &    1.130719e+0,  3.982785e-1 /
  data p8l /   &
       &    1.378030e-2, -1.097846e-1,  3.625283e-1, -6.522477e-1,   &
       &    6.684458e-1, -4.703556e-1,  1.721632e-2, -1.274981e-1,   &
       &    3.917226e-1, -6.562264e-1,  6.305859e-1, -4.067119e-1,   &
       &    2.121871e-2, -1.461891e-1,  4.185427e-1, -6.543539e-1,   &
       &    5.904660e-1, -3.507098e-1,  2.580565e-2, -1.656048e-1,   &
       &    4.426283e-1, -6.473472e-1,  5.494752e-1, -3.018936e-1,   &
       &    3.098251e-2, -1.854823e-1,  4.637398e-1, -6.359482e-1,   &
       &    5.086794e-1, -2.595588e-1 /
  data q8l /   &
       &    1.076975e+0,  3.394154e-1,  1.036132e+0,  3.145673e-1,   &
       &    9.978025e-1,  2.920529e-1,  9.617584e-1,  2.715949e-1,   &
       &    9.278774e-1,  2.530051e-1 /
  data p8u /   &
       &    4.290460e-5, -1.508077e-3,  1.233763e-2, -4.091270e-2,   &
       &    6.547454e-2, -5.664203e-2,  1.201008e-4, -2.778372e-3,   &
       &    1.797999e-2, -5.055048e-2,  7.125083e-2, -5.469912e-2,   &
       &    2.698511e-4, -4.628815e-3,  2.470890e-2, -6.017759e-2,   &
       &    7.566434e-2, -5.202678e-2,  5.259595e-4, -7.144198e-3,   &
       &    3.238633e-2, -6.946769e-2,  7.873067e-2, -4.889490e-2,   &
       &    9.255826e-4, -1.038126e-2,  4.083176e-2, -7.815954e-2,   &
       &    8.054087e-2, -4.552077e-2 /
  data q8u /   &
       &    1.379457e+0,  5.786953e-1,  1.300303e+0,  5.135748e-1,   &
       &    1.230436e+0,  4.593779e-1,  1.168075e+0,  4.135871e-1,   &
       &    1.111893e+0,  3.744076e-1 /
  !
  ! Code
  ier = 0
  if (ialf.lt.1 .or. ialf.gt.5) ier = 1
  if (im.lt.4 .or. im.gt.8) ier = 2+10*ier
  if (abs(eta).gt.1.) ier = 3+10*ier
  if (ier.ne.0) then
     write(mess,*) 'Error #',ier
     call wifisyn_message(seve%f,rname,mess)
     return
  endif
  eta2 = eta**2
  j = ialf
  !
  ! Support width = 4 cells:
  if (im.eq.4) then
     x = eta2-1.
     psi = (p4(1,j)+x*(p4(2,j)+x*(p4(3,j)+x*(p4(4,j)+x*p4(5,j)))))   &
          &      / (1.+x*(q4(1,j)+x*q4(2,j)))
     !
     ! Support width = 5 cells:
  elseif (im.eq.5) then
     x = eta2-1.
     psi = (p5(1,j)+x*(p5(2,j)+x*(p5(3,j)+x*(p5(4,j)+x*(p5(5,j)   &
          &      +x*(p5(6,j)+x*p5(7,j)))))))   &
          &      / (1.+x*q5(j))
     !
     ! Support width = 6 cells:
  elseif (im.eq.6) then
     if (abs(eta).le..75) then
        x = eta2-.5625
        psi = (p6l(1,j)+x*(p6l(2,j)+x*(p6l(3,j)+x*(p6l(4,j)   &
             &        +x*p6l(5,j))))) / (1.+x*(q6l(1,j)+x*q6l(2,j)))
     else
        x = eta2-1.
        psi = (p6u(1,j)+x*(p6u(2,j)+x*(p6u(3,j)+x*(p6u(4,j)   &
             &        +x*p6u(5,j))))) / (1.+x*(q6u(1,j)+x*q6u(2,j)))
     endif
     !
     ! Support width = 7 cells:
  elseif (im.eq.7) then
     if (abs(eta).le..775) then
        x = eta2-.600625
        psi = (p7l(1,j)+x*(p7l(2,j)+x*(p7l(3,j)+x*(p7l(4,j)   &
             &        +x*p7l(5,j))))) / (1.+x*(q7l(1,j)+x*q7l(2,j)))
     else
        x = eta2-1.
        psi = (p7u(1,j)+x*(p7u(2,j)+x*(p7u(3,j)+x*(p7u(4,j)   &
             &        +x*p7u(5,j))))) / (1.+x*(q7u(1,j)+x*q7u(2,j)))
     endif
     !
     ! Support width = 8 cells:
  elseif (im.eq.8) then
     if (abs(eta).le..775) then
        x = eta2-.600625
        psi = (p8l(1,j)+x*(p8l(2,j)+x*(p8l(3,j)+x*(p8l(4,j)   &
             &        +x*(p8l(5,j)+x*p8l(6,j)))))) / (1.+x*(q8l(1,j)+x*q8l(2,j)))
     else
        x = eta2-1.
        psi = (p8u(1,j)+x*(p8u(2,j)+x*(p8u(3,j)+x*(p8u(4,j)   &
             &        +x*(p8u(5,j)+x*p8u(6,j)))))) / (1.+x*(q8u(1,j)+x*q8u(2,j)))
     endif
  endif
  !
  ! Normal return:
  if (iflag.gt.0 .or. ialf.eq.1 .or. eta.eq.0.) return
  if (abs(eta).eq.1.) then
     psi = 0.0
  else
     psi = (1.-eta2)**alpha(ialf)*psi
  endif
end subroutine sphfn
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
