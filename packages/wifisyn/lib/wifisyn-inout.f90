!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_write_visi(do,map,visi,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_write_visi
  use wifisyn_readwrite_types
  use wifisyn_cube_types
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(readwrite_t), intent(in)    :: do
  type(map_t),       intent(in)    :: map
  type(visi_t),      intent(inout) :: visi
  logical,           intent(inout) :: error
  ! Local
  integer :: iy,ix,ic
  type(gildas) :: visiri
  character(len=filename_length) :: newname
  character(len=*), parameter :: rname='WRITE/VISI'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call sic_parse_file(do%name,'',do%type,newname)
  !
  ! *** JP visi is inout here because of the next call
  !        it should be called wifisyn_visi_header(map,visi,visiri,error)...
  call wifisyn_visi_header(map,visi,error)
  if (error) return
  call gdf_copy_header(visi%amp,visiri,error)
  if (error)  return
  call wifisyn_cube_reallocate_r3d(map%win%cur(1)%nchan,'visi real/imag',visiri,error)
  if (error) return
  !
  do ic=1,visi%cub%nc
     do iy=1,visi%cub%ny
        do ix=1,visi%cub%nx
           visiri%r3d(ix,iy,ic) = real(visi%cub%val(ix,iy,ic))
        enddo ! ix
     enddo ! iy
  enddo ! ic
  call wifisyn_cube_minmax_r3d(code_linscale,map%win%cur(1),visiri,error)
  if (error) return
  visiri%file = trim(do%name)//'-real'//do%type
  call gdf_write_image(visiri,visiri%r3d,error)
  if (gildas_error(visiri,rname,error)) return
  !
  do ic=1,visi%cub%nc
     do iy=1,visi%cub%ny
        do ix=1,visi%cub%nx
           visiri%r3d(ix,iy,ic) = aimag(visi%cub%val(ix,iy,ic))
        enddo ! ix
     enddo ! iy
  enddo ! ic
  call wifisyn_cube_minmax_r3d(code_linscale,map%win%cur(1),visiri,error)
  if (error) return
  visiri%file = trim(do%name)//'-imag'//do%type
  call gdf_write_image(visiri,visiri%r3d,error)
  if (gildas_error(visiri,rname,error)) return
  ! Clean up
  call wifisyn_cube_free_r3d(visiri,error)
  if (error) return
  !
end subroutine wifisyn_write_visi
!
subroutine wifisyn_write_wifi(do,map,wifi,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_write_wifi
  use wifisyn_readwrite_types
  use wifisyn_cube_types
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(readwrite_t), intent(in)    :: do
  type(map_t),       intent(in)    :: map
  type(wifi_t),      intent(inout) :: wifi
  logical,           intent(inout) :: error
  ! Local
  integer :: ix,iy,iu,iv,ichan
  type(gildas) :: wifiri
  character(len=filename_length) :: newname
  character(len=*), parameter :: rname='WRITE/WIFI'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call sic_parse_file(do%name,'',do%type,newname)
  !
  ! *** JP wifi is inout here because of the next call
  !        it should be called wifisyn_wifi_header(map,wifi,wifiri,error)...
  call wifisyn_wifi_header(map,wifi,error)
  if (error) return
  call gdf_copy_header(wifi%amp,wifiri,error)
  if (error)  return
  call wifisyn_cube_reallocate_r4d('wifi real/imag',wifiri,error)
  if (error) return
  !
  ichan = 1
  do iv=1,wifi%cub%nv
     do iu=1,wifi%cub%nu
        do iy=1,wifi%cub%ny
           do ix=1,wifi%cub%nx
              wifiri%r4d(ix,iy,iu,iv) = real(wifi%cub%val(ichan,ix,iy,iu,iv))
           enddo ! ix
        enddo ! iy
     enddo ! iu
  enddo ! iv
  call wifisyn_cube_minmax_r4d(code_linscale,wifiri,error)
  if (error) return
  wifiri%file = trim(do%name)//'-real'//do%type
  call gdf_write_image(wifiri,wifiri%r4d,error)
  if (gildas_error(wifiri,rname,error)) return
  !
  ichan = 1
  do iv=1,wifi%cub%nv
     do iu=1,wifi%cub%nu
        do iy=1,wifi%cub%ny
           do ix=1,wifi%cub%nx
              wifiri%r4d(ix,iy,iu,iv) = aimag(wifi%cub%val(ichan,ix,iy,iu,iv))
           enddo ! ix
        enddo ! iy
     enddo ! iu
  enddo ! iv
  call wifisyn_cube_minmax_r4d(code_linscale,wifiri,error)
  if (error) return
  wifiri%file = trim(do%name)//'-imag'//do%type
  call gdf_write_image(wifiri,wifiri%r4d,error)
  if (gildas_error(wifiri,rname,error)) return
  ! Clean up
  call wifisyn_cube_free_r4d(wifiri,error)
  if (error) return
  !
end subroutine wifisyn_write_wifi
!
subroutine wifisyn_read_wifi(do,wifi,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_read_wifi
  use wifisyn_readwrite_types
  use wifisyn_cube_types
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  ! *** JP1 The progmap structure should be set as well to ensure consistency
  ! *** JP2 There is no consistency check between real and imaginary part...
  ! *** JP3 The xykind and uvkind should be set according to the file units
  !----------------------------------------------------------------------
  type(readwrite_t), intent(in)    :: do
  type(wifi_t),      intent(inout) :: wifi
  logical,           intent(inout) :: error
  ! Local
  integer :: ix,nx
  integer :: iy,ny
  integer :: iu,nu
  integer :: iv,nv,ichan
  type(gildas) :: wreal,wimag
  character(len=filename_length) :: newname
  character(len=*), parameter :: rname='READ/WIFI'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call sic_parse_file(do%name,'','',newname)
  ! Read real part
  newname = trim(do%name)//'-real'
  call sic_parsef(newname,wreal%file,' ',do%type)
  call wifisyn_message(seve%i,rname,'Reading: '//wreal%file)
  wreal%blc = 0
  wreal%trc = 0
  call gdf_read_header(wreal,error)
  if (gildas_error(wreal,rname,error)) return
  call wifisyn_cube_reallocate_r4d('wifi real',wreal,error)
  if (error) return
  call gdf_read_data(wreal%file,wreal%r4d,error)
  if (gildas_error(wreal,rname,error)) return
  ! Read imaginary part
  newname = trim(do%name)//'-imag'
  call sic_parsef(newname,wimag%file,' ',do%type)
  call wifisyn_message(seve%i,rname,'Reading: '//wimag%file)
  wimag%blc = 0
  wimag%trc = 0
  call gdf_read_header(wimag,error)
  if (gildas_error(wimag,rname,error)) return
  call wifisyn_cube_reallocate_r4d('wifi imag',wimag,error)
  if (error) return
  call gdf_read_data(wimag%file,wimag%r4d,error)
  if (gildas_error(wimag,rname,error)) return
  ! Transfer
  ichan = 1
  nx = wreal%gil%dim(1)
  ny = wreal%gil%dim(2)
  nu = wreal%gil%dim(3)
  nv = wreal%gil%dim(4)
  call wifisyn_reallocate_complex_cxyuv("wifi%cub",ichan,nx,ny,nu,nv,wifi%cub,error)
  if (error) return
  do iv=1,nv
     do iu=1,nu
        do iy=1,ny
           do ix=1,nx
              wifi%cub%val(ichan,ix,iy,iu,iv) = &
                   cmplx(wreal%r4d(ix,iy,iu,iv),wimag%r4d(ix,iy,iu,iv))
           enddo ! ix
        enddo ! iy
     enddo ! iu
  enddo ! iv
  ! Clean up
  call wifisyn_cube_free_r4d(wreal,error)
  if (error) return
  call wifisyn_cube_free_r4d(wimag,error)
  if (error) return
  !
end subroutine wifisyn_read_wifi
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
