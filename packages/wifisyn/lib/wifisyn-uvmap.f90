!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_uvmap_command(line,error)
  use wifisyn_interfaces, except_this=>wifisyn_uvmap_command
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  logical,          intent(inout) :: error
  !
  call wifisyn_uvmap_inter(error)
  if (error) return
!  call wifisyn_uvmap_pipe(error)
!  if (error) return
  !
end subroutine wifisyn_uvmap_command
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_uvmap_inter(error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_uvmap_inter
  use wifisyn_uv_buffer
  use wifisyn_cube_buffer
  use wifisyn_sic_buffer
  use wifisyn_transform_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  logical, intent(inout) :: error
  !
  type(uv_t) :: uvtmp
  type(window_t) :: win
  character(len=*), parameter :: rname='UVMAP/INTER'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  call wifisyn_message(seve%i,rname,'Interactive mode')
  !
  call wifisyn_setup_main(usermap,uvr,progmap,uvtmp,error)
  if (error) return
  win = progmap%win%cur(1)
  call wifisyn_uvgrid_main(code_shift,win,uvr,uvtmp,progmap,wifi,error)
  if (error) return
  call wifisyn_fft_wifi(code_inverse,code_space_uv,win,wifi,error)
  if (error) return
  call wifisyn_apodize_wifi_uv(code_inverse,progmap,wifi,error)
  if (error) return
  call wifisyn_fft_wifi(code_direct,code_space_uv,win,wifi,error)
  if (error) return
  call wifisyn_shift_wifi_uv(code_direct,progmap,wifi,error)
  if (error) return
  call wifisyn_fft_wifi(code_direct,code_space_xy,win,wifi,error)
  if (error) return
!!$  call wifisyn_apodize_wifi_xy(code_inverse,progmap,wifi,error)
!!$  if (error) return
  call wifisyn_wifi2visi_main(progmap,wifi,visi,error)
  if (error) return
  call wifisyn_fft_visi(code_inverse,win,visi,error)
  if (error) return
  call wifisyn_free_uv(uvtmp,error)
  if (error) return
  !
end subroutine wifisyn_uvmap_inter
!
subroutine wifisyn_uvmap_pipe(error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_uvmap_pipe
  use wifisyn_uv_buffer
  use wifisyn_cube_buffer
  use wifisyn_sic_buffer
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  logical, intent(inout) :: error
  !
  character(len=*), parameter :: rname='UVMAP/PIPE'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  call wifisyn_message(seve%i,rname,'Pipeline mode')
  !
  !
end subroutine wifisyn_uvmap_pipe
!
subroutine wifisyn_uvmap_cube_header(map,huv,itype,cube,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_uvmap_cube_header
  use wifisyn_buffer_parameters
  use wifisyn_uv_types
  use wifisyn_gridding_types
  !-----------------------------------------------------
  ! @ private
  ! Set beam or dirty header
  ! Note: Contrary to MAPPING, we here copy the projection information also
  !       in the beam header.
  !-----------------------------------------------------
  type(map_t),     intent(in)    :: map
  type(uv_head_t), intent(in)    :: huv
  integer,         intent(in)    :: itype ! cube type (beam or dirty)
  type (gildas),   intent(inout) :: cube
  logical,         intent(inout) :: error
  !
  character(len=80) :: mess
  character(len=*), parameter :: rname='UVMAP/CUBE/HEADER'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call gdf_copy_header(huv%file,cube,error)
  if (error)  return
  !
  if (itype.eq.code_buffer_beam) then
     ! 3rd axis
     cube%char%code(3) = ''
     cube%gil%faxi = 0
     cube%gil%ndim = 2
     cube%gil%dim(3) = 1
     cube%gil%ref(3) = 0
     cube%gil%val(3) = 0
     cube%gil%inc(3) = 0
     ! Noise section
     cube%gil%nois_words = 0
     ! Cube unit
     cube%char%unit = 'none'
  else if (itype.eq.code_buffer_dirty) then
     ! 3rd axis
     cube%char%code(3) = 'VELOCITY'
     cube%gil%faxi = 3
     cube%gil%ndim = 3
     cube%gil%dim(3) = map%win%cur(map%win%n)%last-map%win%cur(1)%first+1
     ! The input table is a tuv one
     cube%gil%ref(3) = huv%file%gil%ref(2) - map%win%cur(1)%first +1
     cube%gil%val(3) = huv%file%gil%voff
     cube%gil%inc(3) = huv%file%gil%vres
     ! Noise section
     cube%gil%nois_words = 2
     cube%gil%noise = map%noise%mod ! not fully clear *** JP
     ! Cube unit
     cube%char%unit = 'Jy/beam'
  else
     write(mess,'(a,i0)') 'Data type should be dirty or beam: ',itype
     call wifisyn_message(seve%e,rname,mess)
     error = .true.
     return
  endif
  ! 1st axis
  cube%char%code(1) = 'RA'
  cube%gil%xaxi = 1
  cube%gil%dim(1) = map%axes(1)%a%sky%n
  cube%gil%ref(1) = map%axes(1)%a%sky%ref
  cube%gil%val(1) = map%axes(1)%a%sky%val
  cube%gil%inc(1) = map%axes(1)%a%sky%inc
  ! 2nd axis
  cube%char%code(2) = 'DEC'
  cube%gil%yaxi = 2
  cube%gil%dim(2) = map%axes(2)%a%sky%n
  cube%gil%ref(2) = map%axes(2)%a%sky%ref
  cube%gil%val(2) = map%axes(2)%a%sky%val
  cube%gil%inc(2) = map%axes(2)%a%sky%inc
  ! 4th axis
  cube%char%code(4) = ''
  cube%gil%dim(4) = 1
  cube%gil%ref(4) = 0
  cube%gil%val(4) = 0
  cube%gil%inc(4) = 0
  ! Projection information (assume equatorial system)
  cube%gil%proj_words = 9
  cube%gil%ra  = huv%file%gil%ra
  cube%gil%dec = huv%file%gil%dec
  cube%gil%lii = huv%file%gil%lii
  cube%gil%bii = huv%file%gil%bii
  cube%gil%ptyp = p_azimuthal ! Azimuthal (Sin)
  cube%gil%pang = map%shift%pang
  cube%gil%a0 = map%shift%a0
  cube%gil%d0 = map%shift%d0
  cube%char%syst = 'EQUATORIAL'
  ! Initialized extrema section
  cube%gil%extr_words = 10
  cube%gil%rmin = +1e38
  cube%gil%rmax = -1e38
  ! No beam yet
  cube%gil%reso_words = 0
  ! Others
  cube%char%type = 'GILDAS_IMAGE'
  cube%loca%size = cube%gil%dim(1)*cube%gil%dim(2)*cube%gil%dim(3)*cube%gil%dim(4)
  !
end subroutine wifisyn_uvmap_cube_header
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
