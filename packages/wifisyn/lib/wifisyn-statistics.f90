!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_statistics_command(line,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_statistics_command
  !----------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !     STATISTICS
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  logical,          intent(inout) :: error
  !
  character(len=*), parameter :: rname='STATISTICS/COMMAND'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call wifisyn_statistics_main(error)
  if (error) return
  !
end subroutine wifisyn_statistics_command
!
subroutine wifisyn_statistics_main(error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_statistics_main
  use wifisyn_cube_buffer
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  logical, intent(inout) :: error
  !
  character(len=*), parameter :: rname='STATISTICS/MAIN'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call wifisyn_statistics_wifi(wifi,error)
  if (error) return
  !
end subroutine wifisyn_statistics_main
!
subroutine wifisyn_statistics_wifi(wifi,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_statistics_wifi
  use wifisyn_cube_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(wifi_t), intent(inout) :: wifi
  logical,      intent(inout) :: error
  !
  integer :: ic
  integer :: ix,nx
  integer :: iy,ny
  integer :: iu,nu
  integer :: iv,nv
  real(kind=8) :: variance
  complex(kind=4) :: mean,diff
  character(len=*), parameter :: rname='STATISTICS/WIFI'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  nx = wifi%cub%nx
  ny = wifi%cub%ny
  nu = wifi%cub%nu
  nv = wifi%cub%nv
  call wifisyn_reallocate_complex_xyuv('wifi mean',nx,ny,nu,nv,wifi%mean,error)
  if (error) return
  call wifisyn_reallocate_complex_xyuv('wifi rms',nx,ny,nu,nv,wifi%rms,error)
  if (error) return
  !
  do iv=1,nv
     do iu=1,nu
        do iy=1,ny
           do ix=1,nx
              mean = sum(wifi%cub%val(:,ix,iy,iu,iv))/wifi%cub%nc
              variance = (0.0,0.0)
              do ic=1,wifi%cub%nc
                 diff = wifi%cub%val(ic,ix,iy,iu,iv)-mean
                 variance = variance+dble(abs(diff))**2
              enddo ! ic
              variance = variance/(wifi%cub%nc-1)
              wifi%rms%val(ix,iy,iu,iv) = sqrt(variance)
              wifi%mean%val(ix,iy,iu,iv) = mean
           enddo ! ix
        enddo ! iy
     enddo ! iu
  enddo ! iv
  !
end subroutine wifisyn_statistics_wifi
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
