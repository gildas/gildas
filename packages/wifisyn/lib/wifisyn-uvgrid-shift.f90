!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_uvgrid_shift(win,map,visitab,wifi,error)
  use gbl_message
  use phys_const
  use gkernel_interfaces
  use gkernel_types
  use wifisyn_interfaces, except_this=>wifisyn_uvgrid_shift
  use wifisyn_uv_types
  use wifisyn_cube_types
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(window_t),         intent(in)    :: win
  type(map_t),    target, intent(in)    :: map
  type(uv_t),             intent(in)    :: visitab
  type(wifi_t),           intent(inout) :: wifi
  logical,                intent(inout) :: error
  !
  integer :: ichan
  integer :: ndata,idata
  integer :: nuc,muc,iuc,juc,iucmin,iucmax,nuk,iuk
  integer :: nvc,mvc,ivc,jvc,ivcmin,ivcmax,nvk,ivk
  integer :: nxc,mxc,ixc,jxc,ixcmin,ixcmax,nxk,ixk
  integer :: nyc,myc,iyc,jyc,iycmin,iycmax,nyk,iyk
  real(kind=4) :: resxyuv,resyuv,resuv,resv,w
  type(real_1d_t), pointer :: ukern
  type(real_1d_t), pointer :: vkern
  type(real_1d_t), pointer :: xkern
  type(real_1d_t), pointer :: ykern
  type(dble_1d_t), pointer :: ucoord
  type(dble_1d_t), pointer :: vcoord
  type(dble_1d_t), pointer :: xcoord
  type(dble_1d_t), pointer :: ycoord
  real(kind=8) :: u,du,usup,ufac,uzero
  real(kind=8) :: v,dv,vsup,vfac,vzero
  real(kind=8) :: x,dx,xsup,xfac,xzero,xc
  real(kind=8) :: y,dy,ysup,yfac,yzero,yc,ycv
  real(kind=8) :: uinc,uref,uval
  real(kind=8) :: vinc,vref,vval
  real(kind=8) :: xinc,xref,xval
  real(kind=8) :: yinc,yref,yval
  real(kind=8) :: mtophase,tophase,pha
  complex(kind=8) :: exppha
  type(time_t) :: time
  character(len=*), parameter :: rname='UVGRID/SHIFT'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! Simplifies notation
  ndata = visitab%head%type%ndata
  !
  nuc    = map%axes(1)%u%uvp%n
  uinc   = map%axes(1)%u%uvp%inc
  uref   = map%axes(1)%u%uvp%ref
  uval   = map%axes(1)%u%uvp%val
  ucoord => map%axes(1)%u%uvp%coord
  nuk    = map%kern(1)%u%fn%n
  uzero  = map%kern(1)%u%zero
  ufac   = map%kern(1)%u%fac/uinc
  usup   = map%kern(1)%u%sup
  ukern  => map%kern(1)%u%fn
  !
  nvc    = map%axes(2)%u%uvp%n
  vinc   = map%axes(2)%u%uvp%inc
  vref   = map%axes(2)%u%uvp%ref
  vval   = map%axes(2)%u%uvp%val
  vcoord => map%axes(2)%u%uvp%coord
  nvk    = map%kern(2)%u%fn%n
  vzero  = map%kern(2)%u%zero
  vfac   = map%kern(2)%u%fac/vinc
  vsup   = map%kern(2)%u%sup
  vkern  => map%kern(2)%u%fn
  !
  nxc    = map%axes(1)%x%sky%n
  xinc   = map%axes(1)%x%sky%inc
  xref   = map%axes(1)%x%sky%ref
  xval   = map%axes(1)%x%sky%val
  xcoord => map%axes(1)%x%sky%coord
  nxk    = map%kern(1)%x%fn%n
  xzero  = map%kern(1)%x%zero
  xfac   = map%kern(1)%x%fac/xinc
  xsup   = map%kern(1)%x%sup
  xkern  => map%kern(1)%x%fn
  !
  nyc    = map%axes(2)%x%sky%n
  yinc   = map%axes(2)%x%sky%inc
  yref   = map%axes(2)%x%sky%ref
  yval   = map%axes(2)%x%sky%val
  ycoord => map%axes(2)%x%sky%coord
  nyk    = map%kern(2)%x%fn%n
  yzero  = map%kern(2)%x%zero
  yfac   = map%kern(2)%x%fac/yinc
  ysup   = map%kern(2)%x%sup
  ykern  => map%kern(2)%x%fn
  !
  tophase  = f_to_k*map%rchan%freq
  mtophase = -tophase
  !
  ! Sanity checks
  if ((uval.ne.0d0).or.(vval.ne.0d0).or.(xval.ne.0d0).or.(yval.ne.0d0)) then
     call wifisyn_message(seve%e,rname,'u, v, x and y values must be zero valued')
     error = .true.
     return
  endif
  if ((uinc.ge.0d0).or.(xinc.ge.0d0)) then
     call wifisyn_message(seve%e,rname,'u and x increment value must be negative')
     error = .true.
     return
  endif
  if ((vinc.le.0d0).or.(yinc.le.0d0)) then
     call wifisyn_message(seve%e,rname,'v and y increment value must be positive')
     error = .true.
     return
  endif
  !
  ! Indices of zero frequency
  muc = nuc/2+1
  mvc = nvc/2+1
  !
  ! Initialize
  wifi%cub%val = (0.0,0.0)
  wifi%wei%r4d = 1.0
  !
  ! Loop on observed visibilities
  call gtime_init(time,ndata,error)
  if (error) return
  do idata=1,ndata
     call gtime_current(time)
     u = visitab%head%u(idata)
     v = visitab%head%v(idata)
     x = visitab%head%x(idata)
     y = visitab%head%y(idata)
     w = map%weight%mod%val(idata) ! weight and taper included
     if (v.gt.0) then
        call wifisyn_message(seve%e,rname,'v must be lower than or equal to 0')
        error = .true.
        return
     endif
     !
     ! Find wifi%cub X and Y planes which will be affected by the current data point
     ixcmax = ceiling((x-xsup)/xinc+xref) ! xinc < 0
     ixcmin = floor((x+xsup)/xinc+xref)   ! xinc < 0
     iycmin = floor((y-ysup)/yinc+yref)   ! yinc > 0
     iycmax = ceiling((y+ysup)/yinc+yref) ! yinc > 0
     !
     if (.not.(ixcmin.lt.1.or.ixcmax.gt.nxc.or.iycmin.lt.1.or.iycmax.gt.nyc)) then
        ! Find wifi%cub cells which will be affected by the current data point
        iucmax = nint((u-usup)/uinc+uref)          ! uinc < 0
        iucmin = nint((u+usup)/uinc+uref)            ! uinc < 0
        ivcmin = floor((v-vsup)/vinc+vref)            ! vinc > 0
        ivcmax = min(mvc,ceiling((v+vsup)/vinc+vref)) ! vinc > 0
        !
        if (.not.(iucmin.lt.1.or.iucmax.gt.nuc.or.ivcmin.lt.1.or.ivcmax.gt.nvc)) then
           do ivc=ivcmin,ivcmax
              dv = v-vcoord%val(ivc)
              ivk = nint(dv*vfac+vzero)
              if ((1.le.ivk).and.(ivk.le.nvk)) then
                 resv = vkern%val(ivk)*w
                 do iuc=iucmin,iucmax
                    du = u-ucoord%val(iuc)
                    iuk = nint(du*ufac+uzero)
                    if ((1.le.iuk).and.(iuk.le.nuk)) then
                       resuv = ukern%val(iuk)*resv
                       do iyc=iycmin,iycmax
                          yc = ycoord%val(iyc)
                          dy = y-yc
                          iyk = nint(dy*yfac+yzero)
                          if ((1.le.iyk).and.(iyk.le.nyk)) then
                             ycv = yc*v
                             resyuv = ykern%val(iyk)*resuv
                             do ixc=ixcmin,ixcmax
                                xc = xcoord%val(ixc)
                                dx = x-xc
                                ixk = nint(dx*xfac+xzero)
                                if ((1.le.ixk).and.(ixk.le.nxk)) then
                                   pha = mtophase*(xc*u+ycv)
                                   resxyuv = xkern%val(ixk)*resyuv
                                   exppha = cmplx(cos(pha),sin(pha))*resxyuv
                                   do ichan=win%first,win%last
                                      wifi%cub%val(ichan,ixc,iyc,iuc,ivc) = wifi%cub%val(ichan,ixc,iyc,iuc,ivc) + &
                                           exppha * cmplx(visitab%data(1,ichan,idata),visitab%data(2,ichan,idata))
                                   enddo ! ichan
                                else
!                                   print *,"x",idata,nxk,ixk
                                endif ! ((1.le.ixk).and.(ixk.le.nxk))
                             enddo ! ixc
                          else
!                             print *,"y",idata,nyk,iyk
                          endif ! ((1.le.iyk).and.(iyk.le.nyk))
                       enddo ! iyc
                    else
!                       print *,"u",idata,nuk,iuk
                    endif ! ((1.le.iuk).and.(iuk.le.nuk))
                 enddo ! iuc
              else
!                 print *,"v",nvk,ivk
              endif ! ((1.le.ivk).and.(ivk.le.nvk))
           enddo ! ivc
        endif ! .not.(iucmin.lt.1.or.iucmax.gt.nuc.or.ivcmin.lt.1.or.ivcmax.gt.nvc)
        !
        ! Uses hermitian symmetry to convolve the data points around v = 0
        ! The loop order (imposed by the index order in wifi%cub) doubles the x and y
        ! computations for this region of the uv plane...
        if (v.ge.-vsup) then
           u = -u
           v = -v
           iucmax = nint((u-usup)/uinc+uref)             ! uinc < 0
           iucmin = nint((u+usup)/uinc+uref)             ! uinc < 0
           ivcmin = floor((v-vsup)/vinc+vref)            ! vinc > 0
           ivcmax = min(mvc,ceiling((v+vsup)/vinc+vref)) ! vinc > 0
           !
           if (.not.(iucmin.lt.1.or.iucmax.gt.nuc.or.ivcmin.lt.1.or.ivcmax.gt.nvc)) then
              do ivc=ivcmin,ivcmax
                 dv = v-vcoord%val(ivc)
                 ivk = nint(dv*vfac+vzero)
                 if ((1.le.ivk).and.(ivk.le.nvk)) then
                    resv = vkern%val(ivk)*w
                    do iuc=iucmin,iucmax
                       du = u-ucoord%val(iuc)
                       iuk = nint(du*ufac+uzero)
                       if ((1.le.iuk).and.(iuk.le.nuk)) then
                          resuv = ukern%val(iuk)*resv
                          do iyc=iycmin,iycmax
                             yc = ycoord%val(iyc)
                             dy = y-yc
                             iyk = nint(dy*yfac+yzero)
                             if ((1.le.iyk).and.(iyk.le.nyk)) then
                                ycv = yc*v
                                resyuv = ykern%val(iyk)*resuv
                                do ixc=ixcmin,ixcmax
                                   xc = xcoord%val(ixc)
                                   dx = x-xc
                                   ixk = nint(dx*xfac+xzero)
                                   if ((1.le.ixk).and.(ixk.le.nxk)) then
                                      pha = mtophase*(xc*u+ycv)
                                      resxyuv = xkern%val(ixk)*resyuv
                                      exppha = cmplx(cos(pha),sin(pha))*resxyuv
                                      do ichan=win%first,win%last
                                         wifi%cub%val(ichan,ixc,iyc,iuc,ivc) = wifi%cub%val(ichan,ixc,iyc,iuc,ivc) + &
                                              exppha * cmplx(visitab%data(1,ichan,idata),-visitab%data(2,ichan,idata))
                                      enddo ! ichan
                                   endif ! ((1.le.ixk).and.(ixk.le.nxk))
                                enddo ! ixc
                             endif ! ((1.le.iyk).and.(iyk.le.nyk))
                          enddo ! iyc
                       endif ! ((1.le.iuk).and.(iuk.le.nuk))
                    enddo ! iuc
                 endif ! ((1.le.ivk).and.(ivk.le.nvk))
              enddo ! ivc
           endif ! .not.(iucmin.lt.1.or.iucmax.gt.nuc.or.ivcmin.lt.1.or.ivcmax.gt.nvc)
        endif ! (v.ge.-vsup)
     else
!        print *,"x,y outside grid: ",idata,ixcmin,ixcmax,iycmin,iycmax ! Should be expanded *** JP
     endif ! .not.(ixcmin.lt.1.or.ixcmax.gt.nxc.or.iycmin.lt.1.or.iycmax.gt.nyc)
  enddo ! idata
  !
  ! Apply mirror symmetry around zero frequency
  do ivc=mvc+1,nvc
     jvc = nvc+2-ivc
     do iuc=2,nuc
        juc = nuc+2-iuc
        do iyc=1,nyc
           do ixc=1,nxc
              do ichan=win%first,win%last
                 wifi%cub%val(ichan,ixc,iyc,iuc,ivc) = conjg(wifi%cub%val(ichan,ixc,iyc,juc,jvc))
              enddo ! ichan
           enddo ! ixc
        enddo ! iyc
     enddo ! iuc
  enddo ! ivc
  !
  ! Zero frequencies must have zero imaginary part
  do iyc=1,nyc
     do ixc=1,nxc
        if (aimag(wifi%cub%val(1,ixc,iyc,muc,mvc)).ne.0.0) then
!           print *,'Zero frequency imaginary part is not zero: ',ixc,iyc,wifi%cub%val(1,ixc,iyc,muc,mvc)
        endif
     enddo
  enddo
  ! First raw and columns must be empty (Is that really true ??? *** JP)
  do ivc=1,nvc
     do iyc=1,nyc
        do ixc=1,nxc
           if (wifi%cub%val(1,ixc,iyc,1,ivc).ne.(0.0,0.0)) then
              print *,'Invalid beam ',iyc,ixc,ivc,nvc,wifi%cub%val(1,ixc,iyc,1,ivc)
              error = .true.
              return
           endif
        enddo ! ixc
     enddo ! iyc
  enddo ! ivc
  do iuc=1,nuc
     do iyc=1,nyc
        do ixc=1,nxc
           if (wifi%cub%val(1,ixc,iyc,iuc,1).ne.(0.0,0.0)) then
              print *,'Invalid beam ',iyc,ixc,iuc,nuc,wifi%cub%val(1,ixc,iyc,iuc,1)
              error = .true.
              return
           endif
        enddo ! ixc
     enddo ! iyc
  enddo ! ivc
  !
end subroutine wifisyn_uvgrid_shift
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_uvgrid_shift_unshift(win,map,visitab,wifi,error)
  use gbl_message
  use phys_const
  use gkernel_interfaces
  use gkernel_types
  use wifisyn_interfaces, except_this=>wifisyn_uvgrid_shift_unshift
  use wifisyn_uv_types
  use wifisyn_cube_types
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(window_t),         intent(in)    :: win
  type(map_t),    target, intent(in)    :: map
  type(uv_t),             intent(in)    :: visitab
  type(wifi_t),           intent(inout) :: wifi
  logical,                intent(inout) :: error
  !
  integer :: ichan
  integer :: ndata,idata
  integer :: nuc,muc,iuc,juc,iucmin,iucmax,nuk,iuk
  integer :: nvc,mvc,ivc,jvc,ivcmin,ivcmax,nvk,ivk
  integer :: nxc,mxc,ixc,jxc,ixcmin,ixcmax,nxk,ixk
  integer :: nyc,myc,iyc,jyc,iycmin,iycmax,nyk,iyk
  real(kind=4) :: resrea,resima,resxyuv,resyuv,resuv
  type(real_1d_t), pointer :: ukern
  type(real_1d_t), pointer :: vkern
  type(real_1d_t), pointer :: xkern
  type(real_1d_t), pointer :: ykern
  type(dble_1d_t), pointer :: ucoord
  type(dble_1d_t), pointer :: vcoord
  type(dble_1d_t), pointer :: xcoord
  type(dble_1d_t), pointer :: ycoord
  real(kind=8) :: u,du,usup,ufac,uzero
  real(kind=8) :: v,dv,vsup,vfac,vzero
  real(kind=8) :: x,dx,xsup,xfac,xzero,xc
  real(kind=8) :: y,dy,ysup,yfac,yzero,yc,ycdv
  real(kind=8) :: uinc,uref,uval
  real(kind=8) :: vinc,vref,vval
  real(kind=8) :: xinc,xref,xval
  real(kind=8) :: yinc,yref,yval
  real(kind=8) :: tophase,pha
  complex(kind=8) :: exppha
  type(time_t) :: time
  character(len=*), parameter :: rname='UVGRID/SHIFT/UNSHIFT'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! Simplifies notation
  ndata = visitab%head%type%ndata
  !
  nuc    = map%axes(1)%u%uvp%n
  uinc   = map%axes(1)%u%uvp%inc
  uref   = map%axes(1)%u%uvp%ref
  uval   = map%axes(1)%u%uvp%val
  ucoord => map%axes(1)%u%uvp%coord
  nuk    = map%kern(1)%u%fn%n
  uzero  = map%kern(1)%u%zero
  ufac   = map%kern(1)%u%fac/uinc
  usup   = map%kern(1)%u%sup
  ukern  => map%kern(1)%u%fn
  !
  nvc    = map%axes(2)%u%uvp%n
  vinc   = map%axes(2)%u%uvp%inc
  vref   = map%axes(2)%u%uvp%ref
  vval   = map%axes(2)%u%uvp%val
  vcoord => map%axes(2)%u%uvp%coord
  nvk    = map%kern(2)%u%fn%n
  vzero  = map%kern(2)%u%zero
  vfac   = map%kern(2)%u%fac/vinc
  vsup   = map%kern(2)%u%sup
  vkern  => map%kern(2)%u%fn
  !
  nxc    = map%axes(1)%x%sky%n
  xinc   = map%axes(1)%x%sky%inc
  xref   = map%axes(1)%x%sky%ref
  xval   = map%axes(1)%x%sky%val
  xcoord => map%axes(1)%x%sky%coord
  nxk    = map%kern(1)%x%fn%n
  xzero  = map%kern(1)%x%zero
  xfac   = map%kern(1)%x%fac/xinc
  xsup   = map%kern(1)%x%sup
  xkern  => map%kern(1)%x%fn
  !
  nyc    = map%axes(2)%x%sky%n
  yinc   = map%axes(2)%x%sky%inc
  yref   = map%axes(2)%x%sky%ref
  yval   = map%axes(2)%x%sky%val
  ycoord => map%axes(2)%x%sky%coord
  nyk    = map%kern(2)%x%fn%n
  yzero  = map%kern(2)%x%zero
  yfac   = map%kern(2)%x%fac/yinc
  ysup   = map%kern(2)%x%sup
  ykern  => map%kern(2)%x%fn
  !
  tophase = f_to_k*map%rchan%freq
  !
  ! Sanity checks
  if ((uval.ne.0d0).or.(vval.ne.0d0).or.(xval.ne.0d0).or.(yval.ne.0d0)) then
     call wifisyn_message(seve%e,rname,'u, v, x and y values must be zero valued')
     error = .true.
     return
  endif
  if ((uinc.ge.0d0).or.(xinc.ge.0d0)) then
     call wifisyn_message(seve%e,rname,'u and x increment value must be negative')
     error = .true.
     return
  endif
  if ((vinc.le.0d0).or.(yinc.le.0d0)) then
     call wifisyn_message(seve%e,rname,'v and y increment value must be positive')
     error = .true.
     return
  endif
  !
  ! Indices of zero frequency
  muc = nuc/2+1
  mvc = nvc/2+1
  !
  ! Initialize
  wifi%cub%val = (0.0,0.0)
  wifi%wei%r4d = 1.0
  !
  ! Loop on observed visibilities
  call gtime_init(time,ndata,error)
  if (error) return
  do idata=1,ndata
     call gtime_current(time)
     u = visitab%head%u(idata)
     v = visitab%head%v(idata)
     x = visitab%head%x(idata)
     y = visitab%head%y(idata)
     resrea = map%weight%mod%val(idata) ! weight and taper included
     if (v.gt.0) then
        u = -u
        v = -v
        resima = -resrea
     else
        resima = resrea
     endif
     !
     ! Find wifi%cub X and Y planes which will be affected by the current data point
     ixcmax = ceiling((x-xsup)/xinc+xref) ! xinc < 0
     ixcmin = floor((x+xsup)/xinc+xref)   ! xinc < 0
     iycmin = floor((y-ysup)/yinc+yref)   ! yinc > 0
     iycmax = ceiling((y+ysup)/yinc+yref) ! yinc > 0
     !
     if (.not.(ixcmin.lt.1.or.ixcmax.gt.nxc.or.iycmin.lt.1.or.iycmax.gt.nyc)) then
        ! Find wifi%cub cells which will be affected by the current data point
        iucmax = nint((u-usup)/uinc+uref)          ! uinc < 0
        iucmin = nint((u+usup)/uinc+uref)            ! uinc < 0
        ivcmin = floor((v-vsup)/vinc+vref)            ! vinc > 0
        ivcmax = min(mvc,ceiling((v+vsup)/vinc+vref)) ! vinc > 0
        !
        if (.not.(iucmin.lt.1.or.iucmax.gt.nuc.or.ivcmin.lt.1.or.ivcmax.gt.nvc)) then
           do ivc=ivcmin,ivcmax
              dv = v-vcoord%val(ivc)
              ivk = nint(dv*vfac+vzero)
              if ((1.le.ivk).and.(ivk.le.nvk)) then
                 do iuc=iucmin,iucmax
                    du = u-ucoord%val(iuc)
                    iuk = nint(du*ufac+uzero)
                    if ((1.le.iuk).and.(iuk.le.nuk)) then
                       resuv = ukern%val(iuk)*vkern%val(ivk)
                       do iyc=iycmin,iycmax
                          yc = ycoord%val(iyc)
                          dy = y-yc
                          iyk = nint(dy*yfac+yzero)
                          if ((1.le.iyk).and.(iyk.le.nyk)) then
                             ycdv = yc*dv
                             resyuv = ykern%val(iyk)*resuv
                             do ixc=ixcmin,ixcmax
                                xc = xcoord%val(ixc)
                                dx = x-xc
                                ixk = nint(dx*xfac+xzero)
                                if ((1.le.ixk).and.(ixk.le.nxk)) then
                                   pha = tophase*(xc*du+ycdv)
                                   resxyuv = xkern%val(ixk)*resyuv
                                   exppha = cmplx(cos(pha),sin(pha))*resxyuv
                                   do ichan=win%first,win%last
                                      wifi%cub%val(ichan,ixc,iyc,iuc,ivc) = &
                                           wifi%cub%val(ichan,ixc,iyc,iuc,ivc) + exppha * &
                                           cmplx(visitab%data(1,ichan,idata)*resrea,visitab%data(2,ichan,idata)*resima)
                                   enddo ! ichan
                                else
!                                   print *,"x",idata,nxk,ixk
                                endif ! ((1.le.ixk).and.(ixk.le.nxk))
                             enddo ! ixc
                          else
!                             print *,"y",idata,nyk,iyk
                          endif ! ((1.le.iyk).and.(iyk.le.nyk))
                       enddo ! iyc
                    else
!                       print *,"u",idata,nuk,iuk
                    endif ! ((1.le.iuk).and.(iuk.le.nuk))
                 enddo ! iuc
              else
!                 print *,"v",nvk,ivk
              endif ! ((1.le.ivk).and.(ivk.le.nvk))
           enddo ! ivc
        endif ! .not.(iucmin.lt.1.or.iucmax.gt.nuc.or.ivcmin.lt.1.or.ivcmax.gt.nvc)
        !
        ! Uses hermitian symmetry to convolve the data points around v = 0
        ! The loop order (imposed by the index order in wifi%cub) doubles the x and y
        ! computations for this region of the uv plane...
        if (v.ge.-vsup) then
           u = -u
           v = -v
           resima = -resima
           iucmax = nint((u-usup)/uinc+uref)          ! uinc < 0
           iucmin = nint((u+usup)/uinc+uref)            ! uinc < 0
           ivcmin = floor((v-vsup)/vinc+vref)            ! vinc > 0
           ivcmax = min(mvc,ceiling((v+vsup)/vinc+vref)) ! vinc > 0
           !
           if (.not.(iucmin.lt.1.or.iucmax.gt.nuc.or.ivcmin.lt.1.or.ivcmax.gt.nvc)) then
              do ivc=ivcmin,ivcmax
                 dv = v-vcoord%val(ivc)
                 ivk = nint(dv*vfac+vzero)
                 if ((1.le.ivk).and.(ivk.le.nvk)) then
                    do iuc=iucmin,iucmax
                       du = u-ucoord%val(iuc)
                       iuk = nint(du*ufac+uzero)
                       if ((1.le.iuk).and.(iuk.le.nuk)) then
                          resuv = ukern%val(iuk)*vkern%val(ivk)
                          do iyc=iycmin,iycmax
                             yc = ycoord%val(iyc)
                             dy = y-yc
                             iyk = nint(dy*yfac+yzero)
                             if ((1.le.iyk).and.(iyk.le.nyk)) then
                                ycdv = yc*dv
                                resyuv = ykern%val(iyk)*resuv
                                do ixc=ixcmin,ixcmax
                                   xc = xcoord%val(ixc)
                                   dx = x-xc
                                   ixk = nint(dx*xfac+xzero)
                                   if ((1.le.ixk).and.(ixk.le.nxk)) then
                                      pha = tophase*(xc*du+ycdv)
                                      resxyuv = xkern%val(ixk)*resyuv
                                      exppha = cmplx(cos(pha),sin(pha))*resxyuv
                                      do ichan=win%first,win%last
                                         wifi%cub%val(ichan,ixc,iyc,iuc,ivc) = &
                                              wifi%cub%val(ichan,ixc,iyc,iuc,ivc) + exppha * &
                                              cmplx(visitab%data(1,ichan,idata)*resrea,visitab%data(2,ichan,idata)*resima)
                                      enddo ! ichan
                                   endif ! ((1.le.ixk).and.(ixk.le.nxk))
                                enddo ! ixc
                             endif ! ((1.le.iyk).and.(iyk.le.nyk))
                          enddo ! iyc
                       endif ! ((1.le.iuk).and.(iuk.le.nuk))
                    enddo ! iuc
                 endif ! ((1.le.ivk).and.(ivk.le.nvk))
              enddo ! ivc
           endif ! .not.(iucmin.lt.1.or.iucmax.gt.nuc.or.ivcmin.lt.1.or.ivcmax.gt.nvc)
        endif ! (v.ge.-vsup)
     else
!        print *,"x,y outside grid: ",idata,ixcmin,ixcmax,iycmin,iycmax ! Should be expanded *** JP
     endif ! .not.(ixcmin.lt.1.or.ixcmax.gt.nxc.or.iycmin.lt.1.or.iycmax.gt.nyc)
  enddo ! idata
  !
  ! Apply mirror symmetry around zero frequency
  do ivc=mvc+1,nvc
     jvc = nvc+2-ivc
     do iuc=2,nuc
        juc = nuc+2-iuc
        do iyc=1,nyc
           do ixc=1,nxc
              do ichan=win%first,win%last
                 wifi%cub%val(ichan,ixc,iyc,iuc,ivc) = conjg(wifi%cub%val(ichan,ixc,iyc,juc,jvc))
              enddo ! ichan
           enddo ! ixc
        enddo ! iyc
     enddo ! iuc
  enddo ! ivc
  !
  ! Zero frequencies must have zero imaginary part
  do iyc=1,nyc
     do ixc=1,nxc
        if (aimag(wifi%cub%val(1,ixc,iyc,muc,mvc)).ne.0.0) then
!           print *,'Zero frequency imaginary part is not zero: ',ixc,iyc,wifi%cub%val(1,ixc,iyc,muc,mvc)
        endif
     enddo
  enddo
  ! First raw and columns must be empty (Is that really true ??? *** JP)
  do ivc=1,nvc
     do iyc=1,nyc
        do ixc=1,nxc
           if (wifi%cub%val(1,ixc,iyc,1,ivc).ne.(0.0,0.0)) then
              print *,'Invalid beam ',iyc,ixc,ivc,nvc,wifi%cub%val(1,ixc,iyc,1,ivc)
              error = .true.
              return
           endif
        enddo ! ixc
     enddo ! iyc
  enddo ! ivc
  do iuc=1,nuc
     do iyc=1,nyc
        do ixc=1,nxc
           if (wifi%cub%val(1,ixc,iyc,iuc,1).ne.(0.0,0.0)) then
              print *,'Invalid beam ',iyc,ixc,iuc,nuc,wifi%cub%val(1,ixc,iyc,iuc,1)
              error = .true.
              return
           endif
        enddo ! ixc
     enddo ! iyc
  enddo ! ivc
  !
end subroutine wifisyn_uvgrid_shift_unshift
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
