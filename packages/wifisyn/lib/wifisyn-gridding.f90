!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_gridding_nullify_axis_1d(axis)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_gridding_nullify_axis_1d
  use wifisyn_gridding_types
  !----------------------------------------------------------
  ! @ private
  !----------------------------------------------------------
  type(axis_1d_t), target, intent(inout) :: axis
  !
  character(len=*), parameter :: rname='GRIDDING/NULLIFY/AXIS/1D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  axis%inc => axis%conv(1)
  axis%val => axis%conv(2)
  axis%ref => axis%conv(3)
  !
end subroutine wifisyn_gridding_nullify_axis_1d
!
subroutine wifisyn_gridding_nullify_axis_pair(kind,axis)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_gridding_nullify_axis_pair
  use wifisyn_gridding_types
  !----------------------------------------------------------
  ! @ private
  !----------------------------------------------------------
  integer,           intent(in)    :: kind
  type(axis_pair_t), intent(inout) :: axis
  !
  character(len=*), parameter :: rname='GRIDDING/NULLIFY/AXIS/PAIR'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  axis%kind = kind
  !
  call wifisyn_gridding_nullify_axis_1d(axis%sky)
  call wifisyn_gridding_nullify_axis_1d(axis%uvp)
  !
end subroutine wifisyn_gridding_nullify_axis_pair
!
subroutine wifisyn_gridding_nullify_axes(axes)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_gridding_nullify_axes
  use wifisyn_gridding_types
  !----------------------------------------------------------
  ! @ private
  !----------------------------------------------------------
  type(axis_t), intent(inout) :: axes(2)
  !
  character(len=*), parameter :: rname='GRIDDING/NULLIFY/AXES'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  axes(1)%sign = -1 ! RA
  axes(2)%sign = +1 ! Dec
  call wifisyn_gridding_nullify_axis_pair(code_axis_u,axes(1)%u)
  call wifisyn_gridding_nullify_axis_pair(code_axis_v,axes(2)%u)
  call wifisyn_gridding_nullify_axis_pair(code_axis_x,axes(1)%x)
  call wifisyn_gridding_nullify_axis_pair(code_axis_y,axes(2)%x)
  call wifisyn_gridding_nullify_axis_pair(code_axis_a,axes(1)%a)
  call wifisyn_gridding_nullify_axis_pair(code_axis_b,axes(2)%a)
  call wifisyn_gridding_nullify_axis_pair(code_axis_d,axes(1)%d)
  call wifisyn_gridding_nullify_axis_pair(code_axis_e,axes(2)%d)
  !
end subroutine wifisyn_gridding_nullify_axes
!
subroutine wifisyn_gridding_nullify_map(map)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_gridding_nullify_map
  use wifisyn_gridding_types
  !----------------------------------------------------------
  ! @ private
  !----------------------------------------------------------
  type(map_t), intent(inout) :: map
  !
  character(len=*), parameter :: rname='GRIDDING/NULLIFY/MAP'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call wifisyn_gridding_nullify_axes(map%axes)
  !
  map%kern(1)%u%kind = code_axis_u
  map%kern(2)%u%kind = code_axis_v
  map%kern(1)%x%kind = code_axis_x
  map%kern(2)%x%kind = code_axis_y
  !
end subroutine wifisyn_gridding_nullify_map
!
subroutine wifisyn_free_map(map,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_free_map
  use wifisyn_gridding_types
  !----------------------------------------------------------
  ! @ private
  !----------------------------------------------------------
  type(map_t), intent(inout) :: map
  logical,     intent(inout) :: error
  !
  integer :: iaxis
  character(len=*), parameter :: rname='FREE/MAP'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (associated(map%shift%vsign%val)) deallocate(map%shift%vsign%val)
  !
  if (associated(map%sort%idx%val)) deallocate(map%sort%idx%val)
  if (associated(map%sort%iu%val)) deallocate(map%sort%iu%val)
  if (associated(map%sort%iv%val)) deallocate(map%sort%iv%val)
  if (associated(map%sort%ix%val)) deallocate(map%sort%ix%val)
  if (associated(map%sort%iy%val)) deallocate(map%sort%iy%val)
  !
  if (associated(map%weight%nat%val)) deallocate(map%weight%nat%val)
  if (associated(map%weight%mod%val)) deallocate(map%weight%mod%val)
  !
  do iaxis=1,2
     call wifisyn_free_dble_1d(map%axes(iaxis)%u%sky%coord,error)
     call wifisyn_free_dble_1d(map%axes(iaxis)%u%uvp%coord,error)
     call wifisyn_free_dble_1d(map%axes(iaxis)%x%sky%coord,error)
     call wifisyn_free_dble_1d(map%axes(iaxis)%x%uvp%coord,error)
     call wifisyn_free_dble_1d(map%axes(iaxis)%a%sky%coord,error)
     call wifisyn_free_dble_1d(map%axes(iaxis)%a%uvp%coord,error)
     call wifisyn_free_dble_1d(map%axes(iaxis)%d%sky%coord,error)
     call wifisyn_free_dble_1d(map%axes(iaxis)%d%uvp%coord,error)
     !
     call wifisyn_free_real_1d(map%kern(iaxis)%u%fn,error)
     call wifisyn_free_real_1d(map%kern(iaxis)%u%ft,error)
     call wifisyn_free_real_1d(map%kern(iaxis)%x%fn,error)
     call wifisyn_free_real_1d(map%kern(iaxis)%x%ft,error)
  enddo ! iaxis
  !
end subroutine wifisyn_free_map
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Interface
!
subroutine wifisyn_gridding_sicdef_user(error)
  use gildas_def
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_gridding_sicdef_user
  use wifisyn_sic_buffer
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  logical, intent(inout) :: error
  !
  character(len=*), parameter :: rname='GRIDDING/SICDEF/USER'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (.not.sic_varexist('map')) then
     call sic_defstructure('map',.true.,error)
     call sic_def_dble ('map%angle',usermap%pang,0,0,.false.,error)
     call sic_def_charn('map%dec',usermap%dec,1,1,.false.,error)
     call sic_def_charn('map%ra',usermap%ra,1,1,.false.,error)
     call sic_def_logi ('map%shift',usermap%shift,.false.,error)
     call sic_def_real ('map%support',usermap%support,1,2,.false.,error)
     call sic_def_inte ('map%ctype',usermap%ctype,0,0,.false.,error)
     call sic_def_real ('map%cell',usermap%cell,1,2,.false.,error)
     call sic_def_inte ('map%size',usermap%size,1,2,.false.,error)
     call sic_defstructure('map%field%',.true.,error)
     call sic_def_real ('map%field%size',usermap%field_size,1,2,.false.,error)
     call sic_def_real ('map%field%center',usermap%field_center,1,2,.false.,error)
!     call sic_def_real ('map%diam',usermap%diam,0,0,.false.,error)
!     call sic_def_charn('map%like',usermap%like,1,1,.false.,error)
!     call sic_def_real ('map%reso',usermap%reso,1,2,.false.,error)
     call sic_def_inte ('map%wchan',usermap%wchan,0,0,.false.,error)
     call sic_def_inte ('map%chan',usermap%chan,1,2,.false.,error)
     call sic_def_charn('map%weighting',usermap%mode,1,1,.false.,error)
     call sic_defstructure('map%robust%',.true.,error)
     call sic_def_real ('map%robust%cell',usermap%robust_size,1,2,.false.,error)
     call sic_def_real ('map%robust%thre',usermap%robust_thre,1,1,.false.,error)
     call sic_defstructure('map%taper%',.true.,error)
     call sic_def_real ('map%taper%sigma',usermap%taper_sigma,1,2,.false.,error)
     call sic_def_real ('map%taper%angle',usermap%taper_pang,0,0,.false.,error)
     call sic_def_real ('map%taper%expo',usermap%taper_expo,0,0,.false.,error)
  endif
  !
end subroutine wifisyn_gridding_sicdef_user
!
subroutine wifisyn_gridding_sicdef_prog(prog_request,error)
  use gildas_def
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_gridding_sicdef_prog
  use wifisyn_sic_buffer
  !----------------------------------------------------------------------
  ! @ private
  ! *** JP Can not yet define readwrite or readonly
  !----------------------------------------------------------------------
  logical, intent(in)    :: prog_request
  logical, intent(inout) :: error
  !
  logical :: global = .true.
  character(len=varname_length) :: name,prefix
  character(len=*), parameter :: rname='SICDEF/PROG'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  name = progmap%sic%name
  if (prog_request) then
     call wifisyn_message(seve%i,rname,'Program request')
     if (progmap%sic%defined) then
        if (progmap%sic%readonly) then
           call wifisyn_message(seve%i,rname,'Redefining '//name//' arrays readonly')
        else
           call wifisyn_message(seve%i,rname,'Redefining '//name//' arrays readwrite')
        endif
     else
        call wifisyn_message(seve%i,rname,name//' not defined => Nothing to be done')
        return
     endif
  else
     call wifisyn_message(seve%i,rname,'User request')
     if (progmap%sic%defined) then
        call wifisyn_message(seve%i,rname,name//' already defined')
        return
     else
        call wifisyn_message(seve%i,rname,'Defining '//name)
        progmap%sic%defined = .true. ! Ensure array redefinition at next call
     endif
  endif
  !
  prefix = trim(name)//'%'
  if (.not.sic_varexist(name)) then
     call sic_defstructure(name,global,error)
     call sic_defstructure(trim(prefix)//'shift',global,error)
     call sic_defstructure(trim(prefix)//'sort',global,error)
     call sic_defstructure(trim(prefix)//'taper',global,error)
     call sic_defstructure(trim(prefix)//'robust',global,error)
     call sic_defstructure(trim(prefix)//'weight',global,error)
     call sic_defstructure(trim(prefix)//'noise',global,error)
     call sic_defstructure(trim(prefix)//'beam',global,error)
     call sic_defstructure(trim(prefix)//'buffer',global,error)
  endif
  ! Some array redefinitions here
  call wifisyn_gridding_sicdef_prog_kern(trim(prefix)//'kern',progmap%kern,error)
  call wifisyn_gridding_sicdef_prog_axes(trim(prefix)//'axes',progmap%axes,error)
  !
end subroutine wifisyn_gridding_sicdef_prog
!
subroutine wifisyn_gridding_sicdef_prog_axes(name,axes,error)
  use gbl_message
  use gildas_def
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_gridding_sicdef_prog_axes
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*), intent(in)    :: name
  type(axis_t),     intent(in)    :: axes(2)
  logical,          intent(inout) :: error
  !
  character(len=varname_length) :: prefix
  character(len=*), parameter :: rname='SICDEF/PROG/AXES'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (.not.sic_varexist(name)) then
     call sic_defstructure(name,.true.,error)
  endif
  prefix = trim(name)//'%'
  call wifisyn_gridding_sicdef_prog_axis_pair(trim(prefix)//'u',axes(1)%u,error)
  call wifisyn_gridding_sicdef_prog_axis_pair(trim(prefix)//'v',axes(2)%u,error)
  call wifisyn_gridding_sicdef_prog_axis_pair(trim(prefix)//'x',axes(1)%x,error)
  call wifisyn_gridding_sicdef_prog_axis_pair(trim(prefix)//'y',axes(2)%x,error)
  call wifisyn_gridding_sicdef_prog_axis_pair(trim(prefix)//'a',axes(1)%a,error)
  call wifisyn_gridding_sicdef_prog_axis_pair(trim(prefix)//'b',axes(2)%a,error)
  call wifisyn_gridding_sicdef_prog_axis_pair(trim(prefix)//'d',axes(1)%d,error)
  call wifisyn_gridding_sicdef_prog_axis_pair(trim(prefix)//'e',axes(2)%d,error)
  !
end subroutine wifisyn_gridding_sicdef_prog_axes
!
subroutine wifisyn_gridding_sicdef_prog_axis_pair(name,axis,error)
  use gildas_def
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_gridding_sicdef_prog_axis_pair
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*),  intent(in)    :: name
  type(axis_pair_t), intent(in)    :: axis
  logical,           intent(inout) :: error
  !
  character(len=varname_length) :: prefix
  character(len=*), parameter :: rname='SICDEF/PROG/AXIS/PAIR'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  prefix = trim(name)//'%'
  if (.not.sic_varexist(name)) then
     call sic_defstructure(name,.true.,error)
     call sic_def_inte(trim(prefix)//'kind',axis%kind,0,0,.false.,error)
  endif
  call wifisyn_gridding_sicdef_prog_axis_1d(trim(prefix)//'sky',axis%sky,error)
  call wifisyn_gridding_sicdef_prog_axis_1d(trim(prefix)//'uvp',axis%uvp,error)
  !
end subroutine wifisyn_gridding_sicdef_prog_axis_pair
!
subroutine wifisyn_gridding_sicdef_prog_axis_1d(name,axis,error)
  use gildas_def
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_gridding_sicdef_prog_axis_1d
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*), intent(in)    :: name
  type(axis_1d_t),  intent(in)    :: axis
  logical,          intent(inout) :: error
  !
  character(len=varname_length) :: prefix
  character(len=*), parameter :: rname='SICDEF/PROG/AXIS/1D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  prefix = trim(name)//'%'
  if (.not.sic_varexist(name)) then
     call sic_defstructure(name,.true.,error)
     call sic_def_inte(trim(prefix)//'n',axis%n,0,0,.false.,error)
     call sic_def_dble(trim(prefix)//'inc',axis%inc,0,0,.false.,error)
     call sic_def_dble(trim(prefix)//'val',axis%val,0,0,.false.,error)
     call sic_def_dble(trim(prefix)//'ref',axis%ref,0,0,.false.,error)
  endif
  call wifisyn_sicdef_dble_1d(trim(prefix)//'coord',axis%coord%val,axis%coord%n,.false.,error)
  !
end subroutine wifisyn_gridding_sicdef_prog_axis_1d
!
subroutine wifisyn_gridding_sicdef_prog_kern(name,kern,error)
  use gildas_def
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_gridding_sicdef_prog_kern
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*), intent(in)    :: name
  type(kernel_t),   intent(in)    :: kern(2)
  logical,          intent(inout) :: error
  !
  character(len=varname_length) :: prefix
  character(len=*), parameter :: rname='SICDEF/PROG/KERN'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (.not.sic_varexist(name)) then
     call sic_defstructure(name,.true.,error)
  endif
  prefix = trim(name)//'%'
  call wifisyn_gridding_sicdef_prog_kernel_1d(trim(prefix)//'u',kern(1)%u,error)
  call wifisyn_gridding_sicdef_prog_kernel_1d(trim(prefix)//'v',kern(2)%u,error)
  call wifisyn_gridding_sicdef_prog_kernel_1d(trim(prefix)//'x',kern(1)%x,error)
  call wifisyn_gridding_sicdef_prog_kernel_1d(trim(prefix)//'y',kern(2)%x,error)
  !
end subroutine wifisyn_gridding_sicdef_prog_kern
!
subroutine wifisyn_gridding_sicdef_prog_kernel_1d(name,kern,error)
  use gildas_def
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_gridding_sicdef_prog_kernel_1d
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*),  intent(in)    :: name
  type(kernel_1d_t), intent(in)    :: kern
  logical,           intent(inout) :: error
  !
  character(len=varname_length) :: prefix
  character(len=*), parameter :: rname='SICDEF/PROG/KERN/1D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  prefix = trim(name)//'%'
  if (.not.sic_varexist(name)) then
     call sic_defstructure(name,.true.,error)
     call sic_def_inte (trim(prefix)//'kind',kern%kind,0,0,.false.,error)
     call sic_def_inte (trim(prefix)//'type',kern%type,0,0,.false.,error)
     call sic_def_charn(trim(prefix)//'name',kern%name,1,1,.false.,error)
     call sic_def_inte (trim(prefix)//'nparm',kern%nparm,0,0,.false.,error)
     call sic_def_real (trim(prefix)//'parm',kern%parm,1,10,.false.,error)
     call sic_def_real (trim(prefix)//'zero',kern%zero,0,0,.false.,error)
     call sic_def_real (trim(prefix)//'fac',kern%fac,0,0,.false.,error)
     call sic_def_real (trim(prefix)//'sup',kern%sup,0,0,.false.,error)
     call sic_def_inte (trim(prefix)//'nfn',kern%fn%n,0,0,.false.,error)
     call sic_def_inte (trim(prefix)//'nft',kern%ft%n,0,0,.false.,error)
  endif
  call wifisyn_sicdef_real_1d(trim(prefix)//'fn',kern%fn%val,kern%fn%n,.false.,error)
  call wifisyn_sicdef_real_1d(trim(prefix)//'ft',kern%ft%val,kern%ft%n,.false.,error)
  !
end subroutine wifisyn_gridding_sicdef_prog_kernel_1d
!
subroutine wifisyn_sicdef_dble_1d(name,val,nval,readonly,error)
  use gildas_def
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_sicdef_dble_1d
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*), intent(in)    :: name
  integer,          intent(in)    :: nval
  real(kind=8),     intent(in)    :: val(nval)
  logical,          intent(in)    :: readonly
  logical,          intent(inout) :: error
  !
  character(len=*), parameter :: rname='SICDEF/DBLE/1D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (sic_varexist(name)) then
     call sic_delvariable(name,.false.,error)
     if (error) return
  endif
  call sic_def_dble(name,val,1,nval,readonly,error)
!!$  if (associated(val)) then
!!$     call sic_def_dble(name,val,1,nval,readonly,error)
!!$  else
!!$     call wifisyn_message(seve%e,rname,"Unassociated variable: "//trim(name))
!!$     error = .true.
!!$     return
!!$  endif
  !
end subroutine wifisyn_sicdef_dble_1d
!
subroutine wifisyn_sicdef_real_1d(name,val,nval,readonly,error)
  use gildas_def
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_sicdef_real_1d
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*), intent(in)    :: name
  integer,          intent(in)    :: nval
  real(kind=4),     intent(in)    :: val(nval)
  logical,          intent(in)    :: readonly
  logical,          intent(inout) :: error
  !
  character(len=*), parameter :: rname='SICDEF/REAL/1D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (sic_varexist(name)) then
     call sic_delvariable(name,.false.,error)
     if (error) return
  endif
  call sic_def_real(name,val,1,nval,readonly,error)
!!$  if (associated(val)) then
!!$     call sic_def_real(name,val,1,nval,readonly,error)
!!$  else
!!$     call wifisyn_message(seve%e,rname,"Unassociated variable: "//trim(name))
!!$     error = .true.
!!$     return
!!$  endif
  !
end subroutine wifisyn_sicdef_real_1d
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_gridding_set_huv(huv,map,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_gridding_set_huv
  use wifisyn_uv_types
  use wifisyn_gridding_types
  !----------------------------------------------------------
  ! @ private
  !----------------------------------------------------------
  type(uv_head_t), intent(in)    :: huv
  type(map_t),     intent(inout) :: map
  logical,         intent(inout) :: error
  !
  character(len=*), parameter :: rname='GRIDDING/SET/HUV'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call gdf_copy_header(huv%file,map%huv,error)
  if (error)  return
  !
end subroutine wifisyn_gridding_set_huv
!
subroutine wifisyn_gridding_set_tabdim(huv,n,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_gridding_set_tabdim
  use wifisyn_uv_types
  use wifisyn_gridding_types
  !----------------------------------------------------------
  ! @ private
  !----------------------------------------------------------
  type(uv_head_t), intent(in)    :: huv
  type(tabdim_t),  intent(out)   :: n
  logical,         intent(inout) :: error
  !
  character(len=*), parameter :: rname='GRIDDING/SET/TABDIM'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  n%chan = huv%type%nchan
  n%data = huv%type%ndata
  !
end subroutine wifisyn_gridding_set_tabdim
!
subroutine wifisyn_gridding_set_winlist(user,nchan,winlist,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_gridding_set_winlist
  use wifisyn_uv_types
  use wifisyn_gridding_types
  !----------------------------------------------------------
  ! @ private
  !----------------------------------------------------------
  type(sicmap_t),  intent(in)    :: user
  integer,         intent(in)    :: nchan
  type(winlist_t), intent(inout) :: winlist
  logical,         intent(inout) :: error
  !
  integer :: iwin,first,last
  type(window_t) :: win
  character(len=*), parameter :: rname='GRIDDING/SET/WINLIST'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! Channel range
  winlist%n = 1
  iwin = 1
  if (user%chan(1).eq.0) then
     ! Default
     first = 1
  else
     ! User value
     first = max(1,min(user%chan(1),nchan))
  endif
  if (user%chan(2).eq.0) then
     ! Default
     last = nchan
  else
     ! User value
     last = max(1,min(user%chan(2),nchan))
  endif
  win%first = min(first,last)
  win%last  = max(first,last)
  win%nchan = win%last-win%first+1
  !
  ! Weight column
  if (user%wchan.eq.0) then
     ! Default
     win%wchan = (win%first+win%last)/2
  else
     ! User value
     win%wchan = max(0,min(user%wchan,nchan))
  endif
  !
  winlist%cur(iwin) = win
  !
end subroutine wifisyn_gridding_set_winlist
!
subroutine wifisyn_gridding_set_rchan(huv,rchan,error)
  use gbl_message
  use phys_const
  use wifisyn_interfaces, except_this=>wifisyn_gridding_set_rchan
  use wifisyn_uv_types
  use wifisyn_gridding_types
  !----------------------------------------------------------
  ! @ private
  !----------------------------------------------------------
  type(uv_head_t), intent(in)    :: huv
  type(channel_t), intent(out)   :: rchan
  logical,         intent(inout) :: error
  !
  character(len=*), parameter :: rname='GRIDDING/SET/RCHAN'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! Rest frequency and not the center frequency of the current
  ! channel interval as in MAPPING
  rchan%freq = huv%file%gil%freq
  rchan%wave = clight_mhz / rchan%freq
  !
end subroutine wifisyn_gridding_set_rchan
!
subroutine wifisyn_gridding_set_shift(user,hin,rchan,shift,error)
  use gbl_message
  use gbl_constant
  use phys_const
  use image_def
  use gkernel_interfaces
  use gkernel_types
  use wifisyn_interfaces, except_this=>wifisyn_gridding_set_shift
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(sicmap_t),  intent(in)  :: user
  type(gildas),    intent(in)  :: hin
  type(channel_t), intent(in)  :: rchan
  type(shift_t),   intent(out) :: shift
  logical,         intent(out) :: error
  ! Local
  character(len=*), parameter :: rname='GRIDDING/SETSHIFT'
  type(projection_t) :: proj
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (user%shift) then
     ! User value or reference file value
     if (hin%gil%ptyp.ne.p_azimuthal) then
        call wifisyn_message(seve%e,rname,'Projection type of UV hin is not '//projnam(p_azimuthal))
        error = .true.
        return
     endif
     ! Decode RA and DEC and position angle
     call sic_decode(user%ra,shift%a0,24,error)
     if (error) then
        call wifisyn_message(seve%e,rname,'Input conversion error on phase center '//user%ra)
        return
     endif
     call sic_decode(user%dec,shift%d0,360,error)
     if (error) then
        call wifisyn_message(seve%e,rname,'Input conversion error on phase center '//user%dec)
        return
     endif
     shift%pang = user%pang*rad_per_deg
     ! Check phase center agreement to 0.01" and angle to 1"
     if ((abs(shift%a0-hin%gil%a0).lt.1d-2*rad_per_sec).and. &
         (abs(shift%d0-hin%gil%d0).lt.1d-2*rad_per_sec).and. &
         (abs(shift%pang-hin%gil%pang).lt.rad_per_sec)) then
        shift%a0 = hin%gil%a0
        shift%d0 = hin%gil%d0
        shift%pang = hin%gil%pang
        shift%shift = .false.
     else
        call gwcs_projec(hin%gil%a0,hin%gil%d0,-hin%gil%pang,p_azimuthal,proj,error)
        if (error)  return
        call abs_to_rel(proj,shift%a0,shift%d0,shift%xoff,shift%yoff,1)
        shift%rang = shift%pang-hin%gil%pang
        if (abs(shift%rang).lt.rad_per_sec) shift%rang = 0.d0
     endif
     shift%cs(1) =  cos(shift%rang)
     shift%cs(2) = -sin(shift%rang)
     ! Note that the new phase center is counter-rotated because rotations
     ! are applied before phase shift
     shift%xy(1) = - rchan%freq * f_to_k * ( shift%xoff*shift%cs(1) - shift%yoff*shift%cs(2) )
     shift%xy(2) = - rchan%freq * f_to_k * ( shift%yoff*shift%cs(1) + shift%xoff*shift%cs(2) )
  else
     ! Default from hin header
     shift%a0 = hin%gil%a0
     shift%d0 = hin%gil%d0
     shift%pang = hin%gil%pang
  endif
  !
end subroutine wifisyn_gridding_set_shift
!
subroutine wifisyn_gridding_set_topology(huv,topo,error)
  use gbl_message
  use phys_const
  use wifisyn_interfaces, except_this=>wifisyn_gridding_set_topology
  use wifisyn_uv_types
  use wifisyn_gridding_types
  !----------------------------------------------------------
  ! @ private
  !----------------------------------------------------------
  type(uv_head_t),  intent(in)    :: huv
  type(topology_t), intent(out)   :: topo(2)
  logical,          intent(inout) :: error
  !
  character(len=*), parameter :: rname='GRIDDING/SET/TOPOLOGY'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! u and x axes
  topo(1)%u%min = huv%type%umin
  topo(1)%u%max = huv%type%umax
  topo(1)%x%min = huv%type%xmin
  topo(1)%x%max = huv%type%xmax
  topo(1)%uv%min = huv%type%uvmin
  topo(1)%uv%max = huv%type%uvmax
  ! v and y axes
  topo(2)%x%min = huv%type%ymin
  topo(2)%x%max = huv%type%ymax
  topo(2)%u%min = huv%type%vmin
  topo(2)%u%max = huv%type%vmax
  topo(2)%uv%min = huv%type%uvmin
  topo(2)%uv%max = huv%type%uvmax
  !
end subroutine wifisyn_gridding_set_topology
!
subroutine wifisyn_gridding_set_1d_scale(hin,rchan,topo,scale,error)
  use gbl_message
  use phys_const
  use image_def
  use wifisyn_interfaces, except_this=>wifisyn_gridding_set_1d_scale
  use wifisyn_gridding_types
  !----------------------------------------------------------
  ! @ private
  !----------------------------------------------------------
  type(gildas),     intent(in)    :: hin
  type(channel_t),  intent(in)    :: rchan
  type(topology_t), intent(in)    :: topo
  type(scale_t),    intent(out)   :: scale
  logical,          intent(inout) :: error
  !
  real(kind=8) :: fwhm
  character(len=80) :: mess
  character(len=*), parameter :: rname='GRIDDING/SET/1D/SCALE'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  scale%synt%uvp = topo%uv%max
  scale%synt%sky = rchan%wave/scale%synt%uvp
  !
  scale%prim%uvp = 15d0 ! Assume bure antenna *** JP
  scale%prim%sky = rchan%wave/scale%prim%uvp
  !
  fwhm = hin%gil%majo
  if (fwhm.ne.0d0) then
     scale%fwhm%sky = fwhm
  else
     ! Assuming -12.5db edge taper in the illumination *** JP
     scale%fwhm%sky = 1.2 * scale%prim%sky
  endif
  scale%fwhm%uvp = rchan%wave/scale%fwhm%sky
  !
  scale%alias%sky = 2.0 * scale%fwhm%sky
  scale%alias%uvp = rchan%wave/scale%alias%sky
  !
  ! There is a subtlety here: We want a field of view centered on zero
  ! because we FFT. However, nothing says that the observed field of view
  ! is centered on zero. Thus scale%field%sky = topo%x%max-topo%x%min does
  ! not work. However, with the following we potentially waste large amount
  ! of memory and we potentially make larger rounding errors. So there
  ! should be an automatic shift to set the phase center at some position
  ! resulting from a weighted average, probably at shift time. *** JP
  scale%field%sky = 2.0 * topo%x%max
  scale%field%uvp = rchan%wave/scale%field%sky
  !
  scale%image%sky = scale%field%sky + scale%alias%sky
  scale%image%uvp = rchan%wave/scale%image%sky
  !
  call wifisyn_message(seve%i,rname,'Scales:')
  write(mess,'(a,f7.2,a,f7.2,a)') '   Synthesize beam: ',scale%synt%sky*sec_per_rad,'" and ',scale%synt%uvp,' m'
  call wifisyn_message(seve%i,rname,mess)
  write(mess,'(a,f7.2,a,f7.2,a)') '   Primary beam:    ',scale%prim%sky*sec_per_rad,'" and ',scale%prim%uvp,' m'
  call wifisyn_message(seve%i,rname,mess)
  write(mess,'(a,f7.2,a,f7.2,a)') '   FWHM beam:       ',scale%fwhm%sky*sec_per_rad,'" and ',scale%fwhm%uvp,' m'
  call wifisyn_message(seve%i,rname,mess)
  write(mess,'(a,f7.2,a,f7.2,a)') '   Anti-aliasing:   ',scale%alias%sky*sec_per_rad,'" and ',scale%alias%uvp,' m'
  call wifisyn_message(seve%i,rname,mess)
  write(mess,'(a,f7.2,a,f7.2,a)') '   Field:           ',scale%field%sky*sec_per_rad,'" and ',scale%field%uvp,' m'
  call wifisyn_message(seve%i,rname,mess)
  write(mess,'(a,f7.2,a,f7.2,a)') '   Image:           ',scale%image%sky*sec_per_rad,'" and ',scale%image%uvp,' m'
  call wifisyn_message(seve%i,rname,mess)
  !
end subroutine wifisyn_gridding_set_1d_scale
!
subroutine wifisyn_gridding_set_1d_axis(rchan,scale,axis,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_gridding_set_1d_axis
  use wifisyn_gridding_types
  !----------------------------------------------------------
  ! @ private
  !----------------------------------------------------------
  type(channel_t), intent(in)    :: rchan
  type(scale_t),   intent(in)    :: scale
  type(axis_t),    intent(inout) :: axis
  logical,         intent(inout) :: error
  !
  logical :: dopoweroftwo
  integer :: asncell,upncell,nus,nup,nuf,nud,dup_per_dus
  real(kind=8) :: dmax,dsynt,dprim,dimage,dalias,sign
  real(kind=8) :: Sus,dus,ussamp
  real(kind=8) :: Sup,dup,upsamp
  real(kind=8) :: Suf,duf,ufsamp
  real(kind=8) :: Sud,dud
  character(len=80) :: mess
  character(len=*), parameter :: rname='GRIDDING/SET/1D/AXIS'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
ussamp = 2.0
upsamp = 2.0
ufsamp = 2.0
  !
! The number of cells of the spheroidal functions for padding in the next formula
upncell = 6
asncell = 6
  !
dopoweroftwo = .true.
  !
  dsynt = scale%synt%uvp
  dprim = scale%prim%uvp
  dimage = scale%image%uvp
  dalias = scale%alias%uvp
  !
  dmax = dsynt+dprim
  !
  ! Ensure square final pixels
  Suf = ufsamp * dmax
  if (dopoweroftwo) then
     nuf = poweroftwo(Suf/dimage+asncell)
  else
     nuf = evenceiling(Suf/dimage+asncell)
  endif
  duf = Suf/dble(nuf)
  ! Ensure that dup/dus is an integer
  dus = duf
  dup_per_dus = floor(dalias/dus)
!  dup_per_dus = 1
  dup = dus*dble(dup_per_dus)
  ! Deduce image sizes
  if (dopoweroftwo) then
     nus = poweroftwo(abs(ussamp*dprim/dus))
     nup = poweroftwo(abs(upsamp*dmax/dup)+upncell)
  else
     nus = evenceiling(abs(ussamp*dprim/dus))
     nup = evenceiling(abs(upsamp*dmax/dup)+upncell)
  endif
  Sus = nus*dus
  Sup = nup*dup
  ! Number of dirty beam computed
!  nud = 3
!  dud = dus*4
  nud = nus
  dud = dus
  Sud = dud*nud
  ! Ensure correct sign
  sign = dble(axis%sign)
  dup = sign*dup
  dus = sign*dus
  duf = sign*duf
  dud = sign*dud
  ! User feedback
  call wifisyn_message(seve%i,rname,'Space size:')
  write(mess,'(a,f7.2,a,f5.2,a,f5.2,a,f7.2,a)') '   U gridding: Dup = ',Sup,' m >= ',upsamp,'*dsynt (',upsamp,' *',dsynt,' m)'
  call wifisyn_message(seve%i,rname,mess)
  write(mess,'(a,f7.2,a,f5.2,a,f5.2,a,f7.2,a)') '   X gridding: Dus = ',Sus,' m >= ',ussamp,'*dprim (',ussamp,' *',dprim,' m)'
  call wifisyn_message(seve%i,rname,mess)
  write(mess,'(a,f7.2,a,f5.2,a,f5.2,a,f7.2,a)') '   Final:      Duf = ',Suf,' m  = ',ufsamp,'*dsynt (',ufsamp,' *',dsynt,' m)'
  call wifisyn_message(seve%i,rname,mess)
  call wifisyn_message(seve%i,rname,'Pixel size:')
  write(mess,'(a,f7.2,a,f7.2,a)') '   U gridding: dup = ',dup,' m <= dalias (',dalias,' m)'
  call wifisyn_message(seve%i,rname,mess)
  write(mess,'(a,f7.2,a,f7.2,a)') '   X gridding: dus = ',dus,' m <= dimage (',dimage,' m)'
  call wifisyn_message(seve%i,rname,mess)
  write(mess,'(a,f7.2,a,f7.2,a)') '   Final:      duf = ',duf,' m <= dimage (',dimage,' m)'
  call wifisyn_message(seve%i,rname,mess)
  call wifisyn_message(seve%i,rname,'Space / pixel size:')
  write(mess,'(a,i0)') '   U gridding: Nup = Dup/dup = ',nup
  call wifisyn_message(seve%i,rname,mess)
  write(mess,'(a,i0)') '   X gridding: Nus = Dus/dus = ',nus
  call wifisyn_message(seve%i,rname,mess)
  write(mess,'(a,i0)') '   Final:      Nuf = Duf/duf = ',nuf
  call wifisyn_message(seve%i,rname,mess)
  call wifisyn_message(seve%i,rname,'Pixel ratios:')
  write(mess,'(a,i0,a)') '   duf = ',1,' * dus'
  call wifisyn_message(seve%i,rname,mess)
  write(mess,'(a,i0,a)') '   dup = ',dup_per_dus,' * dus'
  call wifisyn_message(seve%i,rname,mess)
  ! Complete axes
  axis%dup_per_duf = dup_per_dus
  call wifisyn_gridding_set_1d_axis_pair(rchan,nup,dup,axis%u,error)
  if (error) return
  call wifisyn_gridding_set_1d_axis_pair(rchan,nus,dus,axis%x,error)
  if (error) return
  call wifisyn_gridding_set_1d_axis_pair(rchan,nuf,duf,axis%a,error)
  if (error) return
  call wifisyn_gridding_set_1d_axis_pair(rchan,nud,dud,axis%d,error)
  if (error) return
  !
end subroutine wifisyn_gridding_set_1d_axis
!
subroutine wifisyn_gridding_set_1d_axis_pair(rchan,npix,incuvp,axis,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_gridding_set_1d_axis_pair
  use wifisyn_gridding_types
  !----------------------------------------------------------
  ! @ private
  !----------------------------------------------------------
  type(channel_t),   intent(in)    :: rchan
  integer,           intent(in)    :: npix
  real(kind=8),      intent(in)    :: incuvp
  type(axis_pair_t), intent(inout) :: axis
  logical,           intent(inout) :: error
  !
  character(len=*), parameter :: rname='GRIDDING/SET/1D/AXIS/PAIR'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  axis%uvp%n = npix
  axis%uvp%inc = incuvp
  axis%uvp%val = 0d0
  axis%uvp%ref = npix/2+1
  call wifisyn_gridding_set_1d_coord(axis%uvp,error)
  if (error) return
  !
  axis%sky%n = npix
  axis%sky%inc = rchan%wave/(npix*incuvp)
  axis%sky%val = 0d0
  axis%sky%ref = npix/2+1
  call wifisyn_gridding_set_1d_coord(axis%sky,error)
  if (error) return
  !
end subroutine wifisyn_gridding_set_1d_axis_pair
!
subroutine wifisyn_gridding_set_1d_coord(axis,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_gridding_set_1d_coord
  use wifisyn_gridding_types
  !------------------------------------------------------------------------
  ! @ private
  ! Allocate and compute coordinate array from conversion formula
  !------------------------------------------------------------------------
  type(axis_1d_t), intent(inout) :: axis
  logical,         intent(inout) :: error
  !
  integer :: ig
  character(len=*), parameter :: rname='GRIDDING/SET/1D/COORD'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call wifisyn_reallocate_dble_1d('axis coordinate buffer',axis%n,axis%coord,error)
  do ig=1,axis%coord%n
     axis%coord%val(ig) = (dble(ig)-axis%ref)*axis%inc+axis%val
  enddo
  !
end subroutine wifisyn_gridding_set_1d_coord
!
subroutine wifisyn_gridding_set_1d_kernel(user,axis,kern,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_gridding_set_1d_kernel
  use wifisyn_gridding_types
  !----------------------------------------------------------
  ! @ private
  !----------------------------------------------------------
  type(sicmap_t),    intent(in)    :: user
  type(axis_pair_t), intent(in)    :: axis
  type(kernel_1d_t), intent(inout) :: kern
  logical,           intent(inout) :: error
  !
  real(kind=8) :: pixinc
  integer :: ik,nfft,default_type
  character(len=80) :: mess
  character(len=*), parameter :: rname='GRIDDING/SET/1D/KERNEL'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! Initialize
  if ((kern%kind.eq.code_axis_u).or.(kern%kind.eq.code_axis_x)) then
     ik = 1
  else if ((kern%kind.eq.code_axis_v).or.(kern%kind.eq.code_axis_y)) then
     ik = 2
  else
     write(mess,'(a,i0)') 'Unknown kernel kind: ',kern%kind
     call wifisyn_message(seve%t,rname,mess)
     error = .true.
     return
  endif
  if ((kern%kind.eq.code_axis_u).or.(kern%kind.eq.code_axis_v)) then
     nfft = axis%sky%n
     pixinc = axis%uvp%inc
  else if ((kern%kind.eq.code_axis_x).or.(kern%kind.eq.code_axis_y)) then
     nfft = axis%uvp%n
     pixinc = axis%sky%inc
  endif
  default_type = code_spheroidal
  !
  ! Support size [pixel]
  if (user%support(ik).eq.0) then
     ! Default value (0 means that the definition is defered to wifisyn_convolution_definition)
     kern%parm(1) = 0
  else
     ! User value
     kern%parm(1) = user%support(ik)
  endif
  ! Kernel type
  if (user%ctype.eq.0) then
     ! Default value
     kern%type = default_type
  else
     ! User value
     kern%type = user%ctype
  endif
  !
  ! Definition of the kernel parameters
  call wifisyn_convolution_definition(pixinc,kern,error)
  if (error) return
  !
  ! Allocate and compute kernel function
  call wifisyn_convolution_computation(kern,error)
  if (error) return
  !
  ! Allocate and compute kernel inverse Fourier transform
  call wifisyn_ift_kern(nfft,kern,error)
  if (error) return
  !
end subroutine wifisyn_gridding_set_1d_kernel
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_gridding_set_taper(user,taper,error)
  use gbl_message
  use phys_const
  use wifisyn_interfaces, except_this=>wifisyn_gridding_set_taper
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(sicmap_t), intent(in)    :: user
  type(taper_t),  intent(inout) :: taper
  logical,        intent(inout) :: error
  !
  character(len=*), parameter :: rname='GRIDDING/SETTAPER'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if ((user%taper_sigma(1).eq.0.).and.(user%taper_sigma(2).eq.0.)) then
     taper%taper = .false.
  else
     taper%taper = .true.
  endif
  ! The absolute value enforce a single definition for taper%angle
  taper%major = abs(user%taper_sigma(1))
  taper%minor = abs(user%taper_sigma(2))
  taper%angle = user%taper_pang*rad_per_deg
  taper%cang = cos(taper%angle)
  taper%sang = sin(taper%angle)
  if (user%taper_expo.ne.0.) then
     ! User defined
     taper%expon = user%taper_expo/2.0
  else
     ! Default: Gaussian
     taper%expon = 1
  endif
  !
end subroutine wifisyn_gridding_set_taper
!
subroutine wifisyn_gridding_set_buffer(map,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_gridding_set_buffer
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(map_t), intent(inout) :: map
  logical,     intent(inout) :: error
  !
  integer :: ndata,nchan,smallnx,smallny,largenx,largeny
  character(len=*), parameter :: rname='GRIDDING/SET/NCHAN'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! Aliases
  ndata = map%n%data
  nchan = map%n%chan
  smallnx = map%axes(1)%x%sky%n
  smallny = map%axes(2)%x%sky%n
  largenx = map%axes(1)%a%sky%n
  largeny = map%axes(2)%a%sky%n
  ! Memory size of one channel
  map%buffer%chansize = 2*1.5*ndata                     ! UV data (raw and modified, including weights)
  map%buffer%chansize = map%buffer%chansize+smallnx*smallny ! Gridding size
  map%buffer%chansize = map%buffer%chansize+largenx*largeny ! FFT size
  map%buffer%chansize = 8*map%buffer%chansize               ! All these are complex numbers
  ! Maximum allowed buffer size and associated number of channels
  map%buffer%maxsize = map%buffer%ram*map%buffer%percent/100.0
  map%buffer%ndata = ndata
  map%buffer%nchan = nint(map%buffer%maxsize/map%buffer%chansize)
  map%buffer%nchan = min(max(1,map%buffer%nchan),nchan) ! Don't allocate more than really needed...
  map%buffer%nblock = ceiling(float(map%buffer%nchan)/float(nchan))
  map%buffer%truesize = map%buffer%nchan*map%buffer%chansize
  !
end subroutine wifisyn_gridding_set_buffer
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_gridding_print(map,error)
  use gbl_message
  use phys_const
  use wifisyn_interfaces, except_this=>wifisyn_gridding_print
  use wifisyn_gridding_types
  !----------------------------------------------------------
  ! @ private
  !----------------------------------------------------------
  type(map_t), target, intent(inout) :: map   ! Gridding parameters
  logical,             intent(inout) :: error ! Return status
  !
  type(winlist_t), pointer :: winlist
  type(tabdim_t),  pointer :: n
  type(minmax_t),  pointer :: uv
  type(minmax_t),  pointer :: xoff,yoff
  type(channel_t), pointer :: rchan
  type(shift_t),   pointer :: shift
  type(taper_t),   pointer :: taper
  type(beam_t),    pointer :: beam
  integer :: nu,nv,nx,ny,na,nb,nd,ne,unit
  real(kind=4) :: klambda_per_meter,truesize
  real(kind=4) :: ucell,vcell,xcell,ycell,acell,bcell,dcell,ecell
  character(len=12) :: cha0,chd0,chunit
  character(len=80) :: mess
  character(len=*), parameter :: rname='GRIDDING/PRINT'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  n  => map%n
  uv => map%topo(1)%uv
  xoff => map%topo(1)%x
  yoff => map%topo(2)%x
  rchan => map%rchan
  winlist => map%win
  shift => map%shift
  taper => map%taper
  beam  => map%beam
  !
  nu    = map%axes(1)%u%uvp%n
  nv    = map%axes(2)%u%uvp%n
  ucell = map%axes(1)%u%uvp%inc
  vcell = map%axes(2)%u%uvp%inc
  !
  nx    = map%axes(1)%x%sky%n
  ny    = map%axes(2)%x%sky%n
  xcell = map%axes(1)%x%sky%inc*sec_per_rad
  ycell = map%axes(2)%x%sky%inc*sec_per_rad
  !
  na    = map%axes(1)%a%sky%n
  nb    = map%axes(2)%a%sky%n
  acell = map%axes(1)%a%sky%inc*sec_per_rad
  bcell = map%axes(2)%a%sky%inc*sec_per_rad
  !
  nd    = map%axes(1)%d%sky%n
  ne    = map%axes(2)%d%sky%n
  dcell = map%axes(1)%d%sky%inc*sec_per_rad
  ecell = map%axes(2)%d%sky%inc*sec_per_rad
  !
  klambda_per_meter = 1e-3/rchan%wave
  !
  call sexag(cha0,shift%a0,24)
  call sexag(chd0,shift%d0,360)
  !
  ! User feedback
  !
  call wifisyn_message(seve%r,rname,'')
  call wifisyn_message(seve%r,rname,'Gridding axes')
  write(mess,'(a,i5,3es15.3)') '   U axis definition: ',nu,map%axes(1)%u%uvp%conv
  call wifisyn_message(seve%r,rname,mess)
  write(mess,'(a,i5,3es15.3)') '   V axis definition: ',nv,map%axes(2)%u%uvp%conv
  call wifisyn_message(seve%r,rname,mess)
  write(mess,'(a,i5,3es15.3)') '   X axis definition: ',nx,map%axes(1)%x%sky%conv
  call wifisyn_message(seve%r,rname,mess)
  write(mess,'(a,i5,3es15.3)') '   Y axis definition: ',ny,map%axes(2)%x%sky%conv
  call wifisyn_message(seve%r,rname,mess)
  write(mess,'(a,i5,3es15.3)') '   A axis definition: ',na,map%axes(1)%a%sky%conv
  call wifisyn_message(seve%r,rname,mess)
  write(mess,'(a,i5,3es15.3)') '   B axis definition: ',nb,map%axes(2)%a%sky%conv
  call wifisyn_message(seve%r,rname,mess)
  call wifisyn_message(seve%r,rname,'')
  !
  write(mess,'(a,i0,a,i0,a)') 'Table size: ',n%data," visibilities x ",n%chan," channels"
  call wifisyn_message(seve%r,rname,mess)
  write(mess,'(a,i0,a,i0,a,i0)') 'First and last imaged channels: ',winlist%cur(1)%first," and ",winlist%cur(1)%last
  call wifisyn_message(seve%r,rname,mess)
  write(mess,'(a,i0)') 'Weight channel:                 ',winlist%cur(1)%wchan
  call wifisyn_message(seve%r,rname,mess)
  call wifisyn_message(seve%r,rname,'')
  !
  write(mess,'(a,f11.3,a,f7.2,a)') 'Frequency: ',rchan%freq,' MHz (= ',rchan%wave*1d3,' mm)'
  call wifisyn_message(seve%r,rname,mess)
  write(mess,'(a,f9.2,a,f9.2,a)') 'Baselines: ',uv%min,' to ',uv%max,' meters'
  call wifisyn_message(seve%r,rname,mess)
  write(mess,'(a,f9.2,a,f9.2,a)') 'Baselines: ',uv%min*klambda_per_meter, ' to ',uv%max*klambda_per_meter, ' klambda'
  call wifisyn_message(seve%r,rname,mess)
  call wifisyn_message(seve%r,rname,'Offsets:')
  write(mess,'(a,f9.2,a,f9.2,a)') '   X-axis: ',xoff%min*sec_per_rad,' to ',xoff%max*sec_per_rad,'"'
  call wifisyn_message(seve%r,rname,mess)
  write(mess,'(a,f9.2,a,f9.2,a)') '   Y-axis: ',yoff%min*sec_per_rad,' to ',yoff%max*sec_per_rad,'"'
  call wifisyn_message(seve%r,rname,mess)
  call wifisyn_message(seve%r,rname,'')
  !
  write(mess,'(a,a,a,a,a,f7.2,a)') 'Grid position: R.A. = ',cha0,', Dec. = ',chd0,' at ',shift%pang*deg_per_rad,' degree'
  call wifisyn_message(seve%r,rname,mess)
  write(mess,'(a,i9,a,i9,a,i0,a)') 'Cube size:        ',na,'  x ',nb,' pixels x ',n%chan,' channels'
  call wifisyn_message(seve%r,rname,mess)
  ! Warn for big images
  if ((nx.gt.4096).or.(ny.ge.4096)) then
     write(mess,*) 'More than 4096 pixels in X or Y'
     call wifisyn_message(seve%e,rname,mess)
     error = .true.
     return
  elseif ((nx.gt.1024).or.(ny.gt.1024)) then
     write(mess,*) 'More than 1024 pixels in X or Y'
     call wifisyn_message(seve%w,rname,mess)
  endif
  write(mess,'(a,f9.2,a,f9.2,a)') 'Pixel size:       ',acell,'" x ',bcell,'"'
  call wifisyn_message(seve%r,rname,mess)
  write(mess,'(a,f9.2,a,f9.2,a)') 'Image size:       ',abs(na*acell),'" x ',abs(nb*bcell),'"'
  call wifisyn_message(seve%r,rname,mess)
  !
  call wifisyn_message(seve%r,rname,'')
  write(mess,'(a,f9.2,a,f9.2,a)') 'Beam spacing:     ',dcell,'" x ',ecell,'"'
  call wifisyn_message(seve%r,rname,mess)
  write(mess,'(a,i9,a,i9)') 'Beam number:       ',nd,' x ',ne
  call wifisyn_message(seve%r,rname,mess)
  call wifisyn_message(seve%r,rname,'')
  !
  if (beam%major.eq.beam%minor) then
     write(mess,'(a,f9.1,a)') 'Synthesized beam: ',(beam%major*sec_per_rad),'"'
  else
     write(mess,'(a,f9.1,a,f9.1,a,f9.1,a)') &
          'Synthesized beam: ',(beam%major*sec_per_rad),'" x ',(beam%minor*sec_per_rad),'" at ',(beam%angle*deg_per_rad),'deg'
  endif
  call wifisyn_message(seve%r,rname,mess)
  call wifisyn_message(seve%r,rname,'')
  !
  if (taper%taper) then
     call wifisyn_message(seve%r,rname,"Tapering weight:")
     write(mess,'(a,f9.2)') '  Major:',taper%major
     call wifisyn_message(seve%r,rname,mess)
     write(mess,'(a,f9.2)') '  Minor:',taper%minor
     call wifisyn_message(seve%r,rname,mess)
     write(mess,'(a,f9.2)') '  Angle:',taper%angle*deg_per_rad
     call wifisyn_message(seve%r,rname,mess)
     write(mess,'(a,f9.2)') '  Exponent:',2*taper%expon
     call wifisyn_message(seve%r,rname,mess)
! with exp{-[(u',')]**','}'
  else
     call wifisyn_message(seve%r,rname,"No tapering")
  endif
  call wifisyn_message(seve%r,rname,'')
  !
  ! Buffer size
  truesize = map%buffer%truesize
  unit = 0
  do while (truesize.gt.0.5)
     truesize = truesize/1024
     unit = unit+1
  enddo
  truesize = truesize*1024
  select case (unit)
  case (1)
     chunit = 'byte'
  case (2)
     chunit = 'kbyte'
  case (3)
     chunit = 'Mbyte'
  case (4)
     chunit = 'Gbyte'
  case (5)
     chunit = 'Tbyte'
  case default
     call wifisyn_message(seve%e,rname,'Unknown buffer size unit')
     error = .true.
     return
  end select
  write(mess,'(a,f9.2,a,i0,a,i0,a)') 'Buffer size: ',truesize,' '//trim(chunit)//' (= ',map%buffer%nchan,'channels => ', &
       map%buffer%nblock,' blocks)'
  call wifisyn_message(seve%r,rname,mess)
  !
end subroutine wifisyn_gridding_print
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
