!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_weighting_extract_sort(wchan,tabin,sort,weight,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_weighting_extract_sort
  use wifisyn_uv_types
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  ! Extract the used weights from the input data
  !----------------------------------------------------------------------
  integer,        intent(in)    :: wchan
  type(uv_t),     intent(in)    :: tabin
  type(sort_t),   intent(in)    :: sort
  type(weight_t), intent(out)   :: weight
  logical,        intent(inout) :: error
  !
  integer :: ndata,idata,isort
  integer :: nchan
  character(len=80) :: mess
  character(len=*), parameter :: rname='WEIGHTING/EXTRACT/SORT'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! Sanity check
  nchan = tabin%head%type%nchan
  ndata = tabin%head%type%ndata
  if (inconsistent_size(rname,'input table',ndata,'sorting index',sort%idx%n,error)) return
  ! Allocate needed memory
  call wifisyn_reallocate_real_1d('natural weight',ndata,weight%nat,error)
  if (error) return
  call wifisyn_reallocate_real_1d('modified weight',ndata,weight%mod,error)
  if (error) return
  !
  ! Extract the weighting column
  weight%nflagged = 0
  if ((wchan.ge.1).and.(wchan.le.nchan)) then
     do idata=1,ndata
        if (tabin%data(idata,3,wchan).gt.0.0) then
           isort = sort%idx%val(idata)
           weight%nat%val(idata) = tabin%data(isort,3,wchan)
        else
           weight%nflagged = weight%nflagged+1
           weight%nat%val(idata) = 0.0
        endif
     enddo
     weight%chan = wchan
  else
     write(mess,*) 'Unknown weighting channel: ',wchan,' => Setting weights to 1'
     call wifisyn_message(seve%w,rname,mess)
     do idata=1,weight%nat%n
        weight%nat%val(idata) = 1.0
     enddo
     weight%chan = 0
  endif
  if (weight%nflagged.ne.0) then
     write(mess,'(a,i0)') 'Number of flagged visibilities: ',weight%nflagged
     call wifisyn_message(seve%w,rname,mess)
  endif
  !
  ! This ensures that we keep the natural weight information even
  ! after tapering and/or robust weighting
  do idata=1,weight%nat%n
     weight%mod%val(idata) = weight%nat%val(idata)
  enddo ! idata
  !
end subroutine wifisyn_weighting_extract_sort
!
!!$subroutine wifisyn_weighting_robust(htab,robust,weight,error)
!!$  use gbl_message
!!$  use wifisyn_interfaces
!!$  use wifisyn_uv_types
!!$  use wifisyn_gridding_types
!!$  use wifisyn_sort_types
!!$  !----------------------------------------------------------------------
!!$  ! @ private
!!$  ! real wm             ! on input: % of uniformity
!!$  !----------------------------------------------------------------------
!!$  type(uv_head_t), intent(in)    :: htab
!!$  type(robust_t),  intent(in)    :: robust
!!$  type(real_1d_t), intent(inout) :: weight
!!$  logical,         intent(inout) :: error
!!$  !
!!$  integer :: args(m_tot_key)
!!$  real(kind=8) :: uref,uval,uinc
!!$  real(kind=8) :: vref,vval,vinc
!!$  character(len=*), parameter :: rname='WEIGHTING/ROBUST'
!!$  !
!!$  call wifisyn_message(seve%t,rname,'Welcome')
!!$  !
!!$  if (.not.robust%robust) return
!!$  !
!!$  ndata = htab%type%ndata
!!$  call wifisyn_allocate_inte_1d('wei',ndata,tmp%wei,error)
!!$  if (error) return
!!$  call wifisyn_allocate_inte_1d('idx',ndata,tmp%idx,error)
!!$  if (error) return
!!$  call wifisyn_allocate_inte_1d('iu',ndata,tmp%iu,error)
!!$  if (error) return
!!$  call wifisyn_allocate_inte_1d('iv',ndata,tmp%iv,error)
!!$  if (error) return
!!$  !
!!$  uref = robust%u%ref
!!$  uval = robust%u%val
!!$  uinc = robust%u%inc
!!$  !
!!$  vref = robust%v%ref
!!$  vval = robust%v%val
!!$  vinc = robust%v%inc
!!$  !
!!$  do idata=1,ndata
!!$     tmp%iu%val(idata) = nint(uref+(htab%u(idata)-uval)/uinc)
!!$     tmp%iv%val(idata) = nint(vref+(htab%v(idata)-vval)/vinc)
!!$  enddo
!!$  !
!!$  args = code_n
!!$  args(1) = code_u
!!$  args(2) = code_v
!!$  call wifisyn_sort_index(args,tmp,error)
!!$  if (error) return
!!$  !
!!$  do idata=1,ndata
!!$     jdata = tmp%idx%val(idata)
!!$     u%val(idata) = htab%u(jdata)
!!$     v%val(idata) = htab%v(jdata)
!!$     iu%val(idata) = tmp%iu%val(jdata)
!!$     iv%val(idata) = tmp%iv%val(jdata)
!!$  enddo
!!$  !
!!$  imin = 0
!!$  imax = 0
!!$  weig = 0
!!$  jmin = 1
!!$  do idata=2,ndata
!!$     if ((iu%val(idata-1).ne.iu%val(idata)).or.(iv%val(idata-1).ne.iv%val(idata))) then
!!$        jmax = idata-1
!!$        wsum = 0
!!$        do jdata=jmin,jmax
!!$           if (wei%val(jdata).gt.0) then
!!$              wsum = wsum+wei(jdata)
!!$           endif
!!$        enddo
!!$        weig(iu%val(jmax),iv%val(jmax)) = wsum
!!$        imin(iu%val(jmax),iv%val(jmax)) = jmin
!!$        imax(iu%val(jmax),iv%val(jmax)) = jmax
!!$        jmin = idata
!!$     endif
!!$  enddo
!!$  !
!!$  do idata=1,ndata
!!$     iu = iu%val(idata)
!!$     iv = iv%val(idata)
!!$     wsum = 0
!!$     do jv=max(1,iv-1),min(iv+1,nv)
!!$        do ju=max(1,iu-1),min(iu+1,nu)
!!$           wsum = wsum+weig(ju,jv)
!!$        enddo
!!$     enddo
!!$     wei%val(idata) = wsum
!!$  enddo
!!$  !
!!$  ! Normalization
!!$  wsum = 0.0
!!$  wmax = 0.0
!!$  wmin = 1.e36
!!$  do idata=1,ndata
!!$     widat = wei%val(idata)
!!$     wmax = max(widat,wmax)
!!$     if (widat.gt.0.0) wmin = min(widat,wmin)
!!$     wsum = wsum+widat
!!$  enddo
!!$  wmean = wsum/ndata
!!$  write(6,*) 'Weights ',wm,wmax,wmin,sqrt(wmin*wmax)*wm
!!$  wthres = wm*sqrt(wmin*wmax)
!!$  do idata=1,ndata
!!$     widat = wei%val(idata)
!!$     if (widat.gt.wthres) then
!!$        wei%val(idata) = visi(iw,i)/widat*wmean
!!$     elseif (widat.gt.0.0) then
!!$        wei%val(idata) = visi(iw,i)
!!$     endif
!!$  enddo
!!$  !
!!$  ! Clean
!!$  !
!!$end subroutine wifisyn_weighting_robust
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_weighting_taper(htab,taper,modwei,error)
  use gbl_message
  use phys_const
  use wifisyn_interfaces, except_this=>wifisyn_weighting_taper
  use wifisyn_uv_types
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  ! Taper the weights of the visibility points
  !----------------------------------------------------------------------
  type(uv_head_t), intent(in)    :: htab
  type(taper_t),   intent(in)    :: taper
  type(real_1d_t), intent(inout) :: modwei
  logical,         intent(inout) :: error
  !
  integer :: ndata,idata
  real :: u,cu,su
  real :: v,cv,sv
  real :: targ,tval
  character(len=*), parameter :: rname='WEIGHTING/TAPERING'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (.not.taper%taper) return
  !
  ! Sanity check
  ndata = htab%type%ndata
  if (inconsistent_size(rname,'input table',ndata,'modified weight',modwei%n,error)) return
  !
  if (taper%major.ne.0) then
    cu = taper%cang/taper%major
    sv = taper%sang/taper%major
  else
    cu = 0.0
    sv = 0.0
  endif
  if (taper%minor.ne.0) then
    cv = taper%cang/taper%minor
    su = taper%sang/taper%minor
  else
    cv = 0.0
    su = 0.0
  endif
  !
  do idata=1,ndata
    u = htab%u(idata)
    v = htab%v(idata)
    targ = (u*cu + v*sv)**2 + (-u*su + v*cv)**2
    if (taper%expon.ne.1) targ = targ**taper%expon
    if (targ.gt.64.0) then
       tval = 0.0
    else
       tval = exp(-targ)
    endif
    modwei%val(idata) = modwei%val(idata)*tval
  enddo ! idata
  !
end subroutine wifisyn_weighting_taper
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_weighting_noise(weight,noise,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_weighting_noise
  use wifisyn_uv_types
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  ! Compute and print natural and weighted/tapered noise levels
  ! The formula used to compute the weighted/tapered noise comes from
  ! Thompson et al. Interferometry and Synthesis in Radio Astronomy
  ! (section "Response to the noise")
  !----------------------------------------------------------------------
  type(weight_t),  intent(in)    :: weight
  type(noise_t),   intent(out)   :: noise
  logical,         intent(inout) :: error
  !
  real(kind=8) :: wsum,wnum,wden
  integer :: ndata,idata
  character(len=80) :: mess
  character(len=*), parameter :: rname='WEIGHTING/NOISE'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! First, compute and print natural weighting noise
  ndata = weight%nat%n
  wsum = 0
  do idata=1,ndata
     if (weight%nat%val(idata).gt.0) then
        wsum = wsum + weight%nat%val(idata)
     endif
  enddo
  if (wsum.eq.0.0) then
     write(mess,'(a,i0,a)') 'Channel ',weight%chan,' is zero weighted'
     call wifisyn_message(seve%e,rname,mess)
     error = .true.
     return
  endif
  call wifisyn_weighting_print_noise(rname,'Natural ',wsum,noise%nat)
  !
  ! Second, compute and print modified weighting noise
  wnum = 0 ! Numerator
  wden = 0 ! Denominator
  do idata=1,ndata
     if (weight%nat%val(idata).gt.0) then
        wnum = wnum + weight%mod%val(idata)
        wden = wden + weight%mod%val(idata)**2/weight%nat%val(idata)
     endif
  enddo
  if ((wnum.eq.0.0).or.(wden.eq.0.0)) then
     call wifisyn_message(seve%e,rname,'Modified weight sum is zero valued')
     error = .true.
     return
  endif
  wsum = wnum**2/wden
  call wifisyn_weighting_print_noise(rname,'Expected',wsum,noise%mod)
  !
end subroutine wifisyn_weighting_noise
!
subroutine wifisyn_weighting_print_noise(cname,wmode,wsum,noise)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_weighting_print_noise
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*), intent(in)  :: cname
  character(len=*), intent(in)  :: wmode
  real(kind=8),     intent(in)  :: wsum
  real(kind=4),     intent(out) :: noise
  !
  real(kind=4) :: factor
  character(len=128) :: mess
  character(len=16) :: unit
  character(len=*), parameter :: rname='WEIGHTING/PRINT'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  noise = 1e-3/sqrt(wsum) ! [jy/beam]
  !
  if (noise.gt.0.05) then
    factor = 1
    unit = ' Jy/beam'
  elseif (noise.lt.0.05e-6) then
    factor = 1e6
    unit = ' microJy/beam'
  else
    factor = 1e3
    unit = ' mJy/beam'
  endif
  !
  write(mess,'(a,a,f9.3,a)') wmode,' rms noise is ',factor*noise,unit
  call wifisyn_message(seve%r,cname,mess)
  !
end subroutine wifisyn_weighting_print_noise
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
