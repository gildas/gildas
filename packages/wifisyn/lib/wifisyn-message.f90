!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage WIFISYN messages
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_message_set_id(id)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_message_set_id
  use wifisyn_message_private
  !---------------------------------------------------------------------
  ! @ private
  ! Alter library id into input id. Should be called by the library
  ! which wants to share its id with the current one.
  !---------------------------------------------------------------------
  integer, intent(in) :: id
  !
  character(len=message_length) :: mess
  !
  wifisyn_message_id = id
  !
  write (mess,'(A,I3)') 'Now use id #',wifisyn_message_id
  call wifisyn_message(seve%d,'wifisyn_message_set_id',mess)
  !
end subroutine wifisyn_message_set_id
!
subroutine wifisyn_message(mkind,procname,mymessage)
  use wifisyn_interfaces, except_this=>wifisyn_message
  use wifisyn_message_private
  !---------------------------------------------------------------------
  ! @ private
  ! Messaging facility for the current library. Calls the low-level
  ! (internal) messaging routine with its own identifier.
  !---------------------------------------------------------------------
  integer,          intent(in) :: mkind    ! Message kind
  character(len=*), intent(in) :: procname ! Name of calling procedure
  character(len=*), intent(in) :: mymessage  ! Message string
  !
  call gmessage_write(wifisyn_message_id,mkind,procname,mymessage)
  !
end subroutine wifisyn_message
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
