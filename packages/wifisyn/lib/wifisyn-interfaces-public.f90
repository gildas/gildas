module wifisyn_interfaces_public
  interface
    subroutine wifisyn_reallocate_complex_cxyuv(kind,nc,nx,ny,nu,nv,array,error)
      use gbl_message
      use wifisyn_array_types
      !-------------------------------------------------------------------
      ! @ public
      !-------------------------------------------------------------------
      character(len=*),   intent(in)    :: kind
      integer,            intent(in)    :: nc
      integer,            intent(in)    :: nx
      integer,            intent(in)    :: ny
      integer,            intent(in)    :: nu
      integer,            intent(in)    :: nv
      type(complex_5d_t), intent(inout) :: array
      logical,            intent(inout) :: error
    end subroutine wifisyn_reallocate_complex_cxyuv
  end interface
  !
  interface
    subroutine wifisyn_reallocate_complex_xyuv(kind,nx,ny,nu,nv,array,error)
      use gbl_message
      use wifisyn_array_types
      !-------------------------------------------------------------------
      ! @ public
      !-------------------------------------------------------------------
      character(len=*),   intent(in)    :: kind
      integer,            intent(in)    :: nx
      integer,            intent(in)    :: ny
      integer,            intent(in)    :: nu
      integer,            intent(in)    :: nv
      type(complex_4d_t), intent(inout) :: array
      logical,            intent(inout) :: error
    end subroutine wifisyn_reallocate_complex_xyuv
  end interface
  !
  interface
    subroutine wifisyn_free_complex_4d(array,error)
      use gbl_message
      use wifisyn_array_types
      !-------------------------------------------------------------------
      ! @ public
      !-------------------------------------------------------------------
      type(complex_4d_t), intent(inout) :: array
      logical,            intent(inout) :: error
    end subroutine wifisyn_free_complex_4d
  end interface
  !
  interface
    subroutine wifisyn_free_complex_5d(array,error)
      use gbl_message
      use wifisyn_array_types
      !-------------------------------------------------------------------
      ! @ public
      !-------------------------------------------------------------------
      type(complex_5d_t), intent(inout) :: array
      logical,            intent(inout) :: error
    end subroutine wifisyn_free_complex_5d
  end interface
  !
  interface
    subroutine wifisyn_reallocate_complex_xyc(kind,nx,ny,nc,array,error)
      use gbl_message
      use wifisyn_array_types
      !-------------------------------------------------------------------
      ! @ public
      !-------------------------------------------------------------------
      character(len=*),   intent(in)    :: kind
      integer,            intent(in)    :: nx
      integer,            intent(in)    :: ny
      integer,            intent(in)    :: nc
      type(complex_3d_t), intent(inout) :: array
      logical,            intent(inout) :: error
    end subroutine wifisyn_reallocate_complex_xyc
  end interface
  !
  interface
    subroutine wifisyn_free_complex_3d(array,error)
      use gbl_message
      use wifisyn_array_types
      !-------------------------------------------------------------------
      ! @ public
      !-------------------------------------------------------------------
      type(complex_3d_t), intent(inout) :: array
      logical,            intent(inout) :: error
    end subroutine wifisyn_free_complex_3d
  end interface
  !
  interface
    subroutine wifisyn_allocate_complex_2d(kind,nx,ny,array,error)
      use gbl_message
      use wifisyn_array_types
      !-------------------------------------------------------------------
      ! @ public
      !-------------------------------------------------------------------
      character(len=*),   intent(in)    :: kind
      integer,            intent(in)    :: nx
      integer,            intent(in)    :: ny
      type(complex_2d_t), intent(inout) :: array
      logical,            intent(inout) :: error
    end subroutine wifisyn_allocate_complex_2d
  end interface
  !
  interface
    subroutine wifisyn_free_complex_2d(array,error)
      use gbl_message
      use wifisyn_array_types
      !-------------------------------------------------------------------
      ! @ public
      !-------------------------------------------------------------------
      type(complex_2d_t), intent(inout) :: array
      logical,            intent(inout) :: error
    end subroutine wifisyn_free_complex_2d
  end interface
  !
  interface
    subroutine wifisyn_allocate_complex_1d(kind,n,array,error)
      use gbl_message
      use wifisyn_array_types
      !-------------------------------------------------------------------
      ! @ public
      !-------------------------------------------------------------------
      character(len=*),   intent(in)    :: kind
      integer,            intent(in)    :: n
      type(complex_1d_t), intent(inout) :: array
      logical,            intent(inout) :: error
    end subroutine wifisyn_allocate_complex_1d
  end interface
  !
  interface
    subroutine wifisyn_free_complex_1d(array,error)
      use gbl_message
      use wifisyn_array_types
      !-------------------------------------------------------------------
      ! @ public
      !-------------------------------------------------------------------
      type(complex_1d_t), intent(inout) :: array
      logical,            intent(inout) :: error
    end subroutine wifisyn_free_complex_1d
  end interface
  !
  interface
    subroutine wifisyn_allocate_dble_2d(kind,nx,ny,array,error)
      use gbl_message
      use wifisyn_array_types
      !-------------------------------------------------------------------
      ! @ public
      !-------------------------------------------------------------------
      character(len=*), intent(in)    :: kind
      integer,          intent(in)    :: nx
      integer,          intent(in)    :: ny
      type(dble_2d_t),  intent(inout) :: array
      logical,          intent(inout) :: error
    end subroutine wifisyn_allocate_dble_2d
  end interface
  !
  interface
    subroutine wifisyn_reallocate_dble_1d(kind,n,array,error)
      use gbl_message
      use wifisyn_array_types
      !-------------------------------------------------------------------
      ! @ public
      !-------------------------------------------------------------------
      character(len=*), intent(in)    :: kind
      integer,          intent(in)    :: n
      type(dble_1d_t),  intent(inout) :: array
      logical,          intent(inout) :: error
    end subroutine wifisyn_reallocate_dble_1d
  end interface
  !
  interface
    subroutine wifisyn_free_dble_1d(array,error)
      use gbl_message
      use wifisyn_array_types
      !-------------------------------------------------------------------
      ! @ public
      !-------------------------------------------------------------------
      type(dble_1d_t), intent(inout) :: array
      logical,         intent(inout) :: error
    end subroutine wifisyn_free_dble_1d
  end interface
  !
  interface
    subroutine wifisyn_allocate_real_2d(kind,nx,ny,array,error)
      use gbl_message
      use wifisyn_array_types
      !-------------------------------------------------------------------
      ! @ public
      !-------------------------------------------------------------------
      character(len=*), intent(in)    :: kind
      integer,          intent(in)    :: nx
      integer,          intent(in)    :: ny
      type(real_2d_t),  intent(inout) :: array
      logical,          intent(inout) :: error
    end subroutine wifisyn_allocate_real_2d
  end interface
  !
  interface
    subroutine wifisyn_free_real_2d(array,error)
      use gbl_message
      use wifisyn_array_types
      !-------------------------------------------------------------------
      ! @ public
      !-------------------------------------------------------------------
      type(real_2d_t), intent(inout) :: array
      logical,         intent(inout) :: error
    end subroutine wifisyn_free_real_2d
  end interface
  !
  interface
    subroutine wifisyn_reallocate_real_1d(kind,n,array,error)
      use gbl_message
      use wifisyn_array_types
      !-------------------------------------------------------------------
      ! @ public
      !-------------------------------------------------------------------
      character(len=*), intent(in)    :: kind
      integer,          intent(in)    :: n
      type(real_1d_t),  intent(inout) :: array
      logical,          intent(inout) :: error
    end subroutine wifisyn_reallocate_real_1d
  end interface
  !
  interface
    subroutine wifisyn_free_real_1d(array,error)
      use gbl_message
      use wifisyn_array_types
      !-------------------------------------------------------------------
      ! @ public
      !-------------------------------------------------------------------
      type(real_1d_t), intent(inout) :: array
      logical,         intent(inout) :: error
    end subroutine wifisyn_free_real_1d
  end interface
  !
  interface
    subroutine wifisyn_allocate_inte_1d(kind,n,array,error)
      use gbl_message
      use wifisyn_array_types
      !-------------------------------------------------------------------
      ! @ public
      !-------------------------------------------------------------------
      character(len=*), intent(in)    :: kind
      integer,          intent(in)    :: n
      type(inte_1d_t),  intent(inout) :: array
      logical,          intent(inout) :: error
    end subroutine wifisyn_allocate_inte_1d
  end interface
  !
  interface
    subroutine wifisyn_free_inte_1d(array,error)
      use gbl_message
      use wifisyn_array_types
      !-------------------------------------------------------------------
      ! @ public
      !-------------------------------------------------------------------
      type(inte_1d_t), intent(inout) :: array
      logical,         intent(inout) :: error
    end subroutine wifisyn_free_inte_1d
  end interface
  !
  interface
    subroutine wifisyn_allocate_logi_1d(kind,n,array,error)
      use gbl_message
      use wifisyn_array_types
      !-------------------------------------------------------------------
      ! @ public
      !-------------------------------------------------------------------
      character(len=*), intent(in)    :: kind
      integer,          intent(in)    :: n
      type(logi_1d_t),  intent(inout) :: array
      logical,          intent(inout) :: error
    end subroutine wifisyn_allocate_logi_1d
  end interface
  !
  interface
    subroutine wifisyn_free_logi_1d(array,error)
      use gbl_message
      use wifisyn_array_types
      !-------------------------------------------------------------------
      ! @ public
      !-------------------------------------------------------------------
      type(logi_1d_t), intent(inout) :: array
      logical,         intent(inout) :: error
    end subroutine wifisyn_free_logi_1d
  end interface
  !
  interface
    subroutine wifisyn_axis_offset2pixel(axis,doff,ioff,error)
      use gbl_message
      use wifisyn_primitive_types
      !----------------------------------------------------------------------
      ! @ public
      ! Convert an offset into the corresponding pixel number.
      ! Make sure that the returned pixel number is inside the axis range.
      !----------------------------------------------------------------------
      type(axis_1d_t), intent(in)    :: axis
      real(kind=8),    intent(in)    :: doff
      integer,         intent(out)   :: ioff
      logical,         intent(inout) :: error
    end subroutine wifisyn_axis_offset2pixel
  end interface
  !
  interface
    subroutine wifisyn_axis_pixel2offset(axis,ioff,doff,error)
      use gbl_message
      use wifisyn_primitive_types
      !----------------------------------------------------------------------
      ! @ public
      !----------------------------------------------------------------------
      type(axis_1d_t), intent(in)    :: axis
      integer,         intent(in)    :: ioff
      real(kind=8),    intent(out)   :: doff
      logical,         intent(inout) :: error
    end subroutine wifisyn_axis_pixel2offset
  end interface
  !
  interface
    subroutine wifisyn_axis_ddx2idx(axis,ddx,idx,error)
      use gbl_message
      use wifisyn_primitive_types
      !----------------------------------------------------------------------
      ! @ public
      !----------------------------------------------------------------------
      type(axis_1d_t), intent(in)    :: axis
      real(kind=8),    intent(in)    :: ddx
      integer,         intent(out)   :: idx
      logical,         intent(inout) :: error
    end subroutine wifisyn_axis_ddx2idx
  end interface
  !
  interface
    subroutine wifisyn_axis_idx2ddx(axis,idx,ddx,error)
      use gbl_message
      use wifisyn_primitive_types
      !----------------------------------------------------------------------
      ! @ public
      !----------------------------------------------------------------------
      type(axis_1d_t), intent(in)    :: axis
      integer,         intent(in)    :: idx
      real(kind=8),    intent(out)   :: ddx
      logical,         intent(inout) :: error
    end subroutine wifisyn_axis_idx2ddx
  end interface
  !
  interface
    function inconsistent_size(rname,array1,n1,array2,n2,error)
      use gbl_message
      !-------------------------------------------------------------------
      ! @ public
      ! Check
      !-------------------------------------------------------------------
      logical                         :: inconsistent_size
      character(len=*), intent(in)    :: rname
      character(len=*), intent(in)    :: array1
      integer,          intent(in)    :: n1
      character(len=*), intent(in)    :: array2
      integer,          intent(in)    :: n2
      logical,          intent(inout) :: error
    end function inconsistent_size
  end interface
  !
  interface
    function no_data(rname,htab,error)
      use gbl_message
      use wifisyn_uv_types
      !-------------------------------------------------------------------
      ! @ public
      ! Check
      !-------------------------------------------------------------------
      logical                         :: no_data
      character(len=*), intent(in)    :: rname
      type(uv_head_t),  intent(in)    :: htab
      logical,          intent(inout) :: error
    end function no_data
  end interface
  !
  interface
    function oddceiling(dval)
      !----------------------------------------------------------------------
      ! @ public
      !----------------------------------------------------------------------
      integer                  :: oddceiling
      real(kind=8), intent(in) :: dval
    end function oddceiling
  end interface
  !
  interface
    function evenceiling(dval)
      !----------------------------------------------------------------------
      ! @ public
      !----------------------------------------------------------------------
      integer                  :: evenceiling
      real(kind=8), intent(in) :: dval
    end function evenceiling
  end interface
  !
  interface
    function poweroftwo(dval)
      !----------------------------------------------------------------------
      ! @ public
      !----------------------------------------------------------------------
      integer                  :: poweroftwo
      real(kind=8), intent(in) :: dval
    end function poweroftwo
  end interface
  !
end module wifisyn_interfaces_public
