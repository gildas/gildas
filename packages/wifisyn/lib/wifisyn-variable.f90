!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_variable_command(line,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_variable_command
  use wifisyn_parameters
  !----------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !    VARIABLE
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  logical,          intent(inout) :: error
  !
  character(len=*), parameter :: rname='VARIABLE'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call wifisyn_gridding_sicdef_prog(code_request_user,error)
  if (error) return
  !
end subroutine wifisyn_variable_command
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
