!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module wifisyn_parameters
  ! integer, parameter :: code_null = 0  ! Provided by the module 'gio_params'
  logical, parameter :: code_request_prog = .true.
  logical, parameter :: code_request_user = .false.
  logical, parameter :: code_readonly  = .true.
  logical, parameter :: code_readwrite = .false.
  logical, parameter :: code_sicvar_global = .true.
  logical, parameter :: code_sicvar_local = .false.
end module wifisyn_parameters
!
module wifisyn_buffer_parameters
  integer, parameter :: mbuffer=13
  integer, parameter :: buffer_kind_length = 12
  integer, parameter :: buffer_type_length = 12
  character(len=buffer_kind_length) :: known_buffer_kind(mbuffer)
  character(len=buffer_type_length) :: known_buffer_type(mbuffer)
  !
  integer, parameter :: code_buffer_default =  1
  integer, parameter :: code_buffer_uv      =  2
  integer, parameter :: code_buffer_visi    =  3
  integer, parameter :: code_buffer_wifi    =  4
  integer, parameter :: code_buffer_beam    =  5
  integer, parameter :: code_buffer_dirty   =  6
  integer, parameter :: code_buffer_clean   =  7
  integer, parameter :: code_buffer_resid   =  8
  integer, parameter :: code_buffer_mask    =  9
  integer, parameter :: code_buffer_support = 10
  integer, parameter :: code_buffer_cct     = 11
  integer, parameter :: code_buffer_model   = 12
  integer, parameter :: code_buffer_weight  = 13
  !
  data known_buffer_kind &
       /'*', &
        'UV', &
        'VISICUBE', &
        'WIFICUBE', &
        'BEAM', &
        'DIRTY', &
        'CLEAN', &
        'RESIDUAL', &
        'MASK', &
        'SUPPORT', &
        'CCT', &
        'MODEL', &
        'WEIGHT'/
  data known_buffer_type &
       /'.none', &
        '.tuv', &
        '.visicub', &
        '.wificub', &
        '.beam', &
        '.lmv', &
        '.lmv-clean', &
        '.lmv-res', &
        '.msk', &
        '.pol', &
        '.cct', &
        '.uvt', &
        '.wei'/
  !
end module wifisyn_buffer_parameters
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
