!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_uvswap_command(error)
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_uvswap_command
  use wifisyn_uv_buffer
  !-------------------------------------------------------------------
  ! @ private
  !-------------------------------------------------------------------
  logical, intent(out) :: error
  !
  type(uv_head_t) :: tmp
  !
  ! Swap the headers
  tmp%type = uvt%head%type
  uvt%head%type = uvr%head%type
  uvr%head%type = tmp%type
  call gdf_copy_header(uvt%head%file,tmp%file,error)
  if (error)  return
  call gdf_copy_header(uvr%head%file,uvt%head%file,error)
  if (error)  return
  call gdf_copy_header(tmp%file,uvr%head%file,error)
  if (error)  return
  !
  ! Swap the pointers
  if (associated(uvr%buff%uvp1,ruv1)) then
     uvr%buff%uvp1 => tuv1
     uvr%buff%uvp2 => tuv2
     uvr%buff%uvp3 => tuv3
     uvr%head%u    => tuv1(:,1)
     uvr%head%v    => tuv1(:,2)
     uvr%head%scan => tuv1(:,3)
     uvr%head%date => tuv1(:,4)
     uvr%head%time => tuv1(:,5)
     uvr%head%anti => tuv1(:,6)
     uvr%head%antj => tuv1(:,7)
     uvr%head%x    => tuv2(:,1)
     uvr%head%y    => tuv2(:,2)
     uvr%data      => tuv3
     !
     uvt%buff%uvp1 => ruv1
     uvt%buff%uvp2 => ruv2
     uvt%buff%uvp3 => ruv3
     uvt%head%u    => ruv1(:,1)
     uvt%head%v    => ruv1(:,2)
     uvt%head%scan => ruv1(:,3)
     uvt%head%date => ruv1(:,4)
     uvt%head%time => ruv1(:,5)
     uvt%head%anti => ruv1(:,6)
     uvt%head%antj => ruv1(:,7)
     uvt%head%x    => ruv2(:,1)
     uvt%head%y    => ruv2(:,2)
     uvt%data      => ruv3
  else
     uvt%buff%uvp1 => tuv1
     uvt%buff%uvp2 => tuv2
     uvt%buff%uvp3 => tuv3
     uvt%head%u    => tuv1(:,1)
     uvt%head%v    => tuv1(:,2)
     uvt%head%scan => tuv1(:,3)
     uvt%head%date => tuv1(:,4)
     uvt%head%time => tuv1(:,5)
     uvt%head%anti => tuv1(:,6)
     uvt%head%antj => tuv1(:,7)
     uvt%head%x    => tuv2(:,1)
     uvt%head%y    => tuv2(:,2)
     uvt%data      => tuv3
     !
     uvr%buff%uvp1 => ruv1
     uvr%buff%uvp2 => ruv2
     uvr%buff%uvp3 => ruv3
     uvr%head%u    => ruv1(:,1)
     uvr%head%v    => ruv1(:,2)
     uvr%head%scan => ruv1(:,3)
     uvr%head%date => ruv1(:,4)
     uvr%head%time => ruv1(:,5)
     uvr%head%anti => ruv1(:,6)
     uvr%head%antj => ruv1(:,7)
     uvr%head%x    => ruv2(:,1)
     uvr%head%y    => ruv2(:,2)
     uvr%data      => ruv3
  endif
  !
  call wifisyn_uvrt_sicdef(uvr,'uvr',error)
  if (error) return
  call wifisyn_uvrt_sicdef(uvt,'uvt',error)
  if (error) return
  !
end subroutine wifisyn_uvswap_command
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_uvrt_sicdef(uvbuffer,uvname,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_uvrt_sicdef
  use gkernel_interfaces
  use wifisyn_uv_buffer
  !-------------------------------------------------------------------
  ! @ private
  !-------------------------------------------------------------------
  type(uv_t),       intent(inout) :: uvbuffer
  character(len=3), intent(in)    :: uvname
  logical,          intent(out)   :: error
  !
  integer :: ndata
  integer(kind=index_length) :: dim(4) = 0
  character(len=*), parameter  :: rname='UVRT/SICDEF'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (sic_varexist(uvname)) then
     call wifisyn_message(seve%d,rname,uvname//' SIC structure already exists')
     call sic_delvariable(uvname,.false.,error)
     if (error) return
  endif
  !
  call sic_defstructure(uvname,.true.,error)
  !
  call sic_defstructure(uvname//'%head',.true.,error)
  call sic_def_header(uvname//'%head',uvbuffer%head%file,.true.,error)
  if (error) return
  !
  call sic_def_logi(uvname//'%wifi',uvbuffer%head%type%wifi,.false.,error)
  call sic_def_logi(uvname//'%sorted',uvbuffer%head%type%sorted,.false.,error)
  call sic_def_inte(uvname//'%ndata',uvbuffer%head%type%ndata,0,0,.false.,error)
  call sic_def_inte(uvname//'%nchan',uvbuffer%head%type%nchan,0,0,.false.,error)
  call sic_def_inte(uvname//'%range',uvbuffer%head%type%range,1,2,.false.,error)
  !
  ndata = uvbuffer%head%type%ndata
  call sic_def_real(uvname//'%u',uvbuffer%head%u,1,ndata,.false.,error)
  call sic_def_real(uvname//'%v',uvbuffer%head%v,1,ndata,.false.,error)
  call sic_def_real(uvname//'%x',uvbuffer%head%x,1,ndata,.false.,error)
  call sic_def_real(uvname//'%y',uvbuffer%head%y,1,ndata,.false.,error)
  call sic_def_real(uvname//'%scan',uvbuffer%head%scan,1,ndata,.false.,error)
  call sic_def_real(uvname//'%date',uvbuffer%head%date,1,ndata,.false.,error)
  call sic_def_real(uvname//'%time',uvbuffer%head%time,1,ndata,.false.,error)
  call sic_def_real(uvname//'%anti',uvbuffer%head%anti,1,ndata,.false.,error)
  call sic_def_real(uvname//'%antj',uvbuffer%head%antj,1,ndata,.false.,error)
  !
  dim(1) = ndata
  dim(2) = 3
  dim(3) = uvbuffer%head%type%nchan
  call sic_def_real(uvname//'%data',uvbuffer%data,3,dim,.false.,error)
  !
  if (error) then
     call wifisyn_message(seve%e,rname,uvname//' SIC structure not (fully) defined')
  endif
  !
end subroutine wifisyn_uvrt_sicdef
!
subroutine wifisyn_free_uv(uv,error)
  use gbl_message
  use gio_params
  use wifisyn_interfaces, except_this=>wifisyn_free_uv
  use wifisyn_uv_buffer
  !-------------------------------------------------------------------
  ! @ private
  ! Free a UV type
  ! Must not touch the Header, which may have been previously defined
  !-------------------------------------------------------------------
  type(uv_t), intent(inout) :: uv
  logical,    intent(inout) :: error
  !
  character(len=*), parameter :: rname='FREE/UV'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! In all cases, nullification of pointer only
  nullify(uv%head%u,uv%head%v,uv%head%scan,uv%head%date,uv%head%time,uv%head%anti,uv%head%antj,uv%data)
  !
  if (associated(uv%buff%uvp1,ruv1)) then
     ! UVR buffer => Just a nullification + Removal of the SIC structure
     nullify(uv%buff%uvp1,uv%buff%uvp2,uv%buff%uvp3)
     call sic_delvariable('uvr',.false.,error)
     if (error) return
  else if (associated(uv%buff%uvp1,tuv1)) then
     ! UVT buffer => Just a nullification + Removal of the SIC structure
     nullify(uv%buff%uvp1,uv%buff%uvp2,uv%buff%uvp3)
     call sic_delvariable('uvt',.false.,error)
     if (error) return
  else
     ! Other case => deallocation
     if (associated(uv%buff%uvp1)) deallocate(uv%buff%uvp1)
     if (associated(uv%buff%uvp2)) deallocate(uv%buff%uvp2)
     if (associated(uv%buff%uvp3)) deallocate(uv%buff%uvp3)
     nullify(uv%buff%uvp1,uv%buff%uvp2,uv%buff%uvp3)
  endif
  !
  ! In all cases, only reset parameter linked to allocated arrays
  uv%head%type%ndata = 0
  uv%head%type%nchan = 0
  uv%head%type%range = 0
  uv%head%type%order = code_null
  !
end subroutine wifisyn_free_uv
!
subroutine wifisyn_free_uvrt(error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_free_uvrt
  use wifisyn_uv_buffer
  !-------------------------------------------------------------------
  ! @ private
  ! Free the UVRT buffers
  !-------------------------------------------------------------------
  logical,    intent(inout) :: error
  !
  character(len=*), parameter :: rname='FREE/UVRT'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! Nullification + deallocation of R buffer
  nullify(uvr%head%u,uvr%head%v,uvr%head%scan,uvr%head%date,uvr%head%time,uvr%head%anti,uvr%head%antj,uvr%data)
  if (allocated(ruv1)) deallocate(ruv1)
  if (allocated(ruv2)) deallocate(ruv2)
  if (allocated(ruv3)) deallocate(ruv3)
  ! Nullification + deallocation of T buffer
  nullify(uvt%head%u,uvt%head%v,uvt%head%scan,uvt%head%date,uvt%head%time,uvt%head%anti,uvt%head%antj,uvt%data)
  if (allocated(tuv1)) deallocate(tuv1)
  if (allocated(tuv2)) deallocate(tuv2)
  if (allocated(tuv3)) deallocate(tuv3)
  !
end subroutine wifisyn_free_uvrt
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_reallocate_uvrt(ndata,nchan,order,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_reallocate_uvrt
  use wifisyn_uv_buffer
  !-------------------------------------------------------------------
  ! @ private
  ! (Re)allocate pointers and targets for the UVR and UVT buffers
  ! The headers are not modified at all
  ! These buffers are always in the TUV order
  !-------------------------------------------------------------------
  integer,    intent(in)    :: ndata
  integer,    intent(in)    :: nchan
  integer,    intent(in)    :: order ! uvt or tuv
  logical,    intent(out)   :: error
  !
  integer :: ier
  character(len=80) :: mess
  character(len=*), parameter :: rname='REALLOCATE/UVRT'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! Sanity check
  if (ndata.le.0) then
     call wifisyn_message(seve%e,rname,'Negative number of visibilities!')
     error = .true.
     return
  endif
  if (nchan.le.0) then
     call wifisyn_message(seve%e,rname,'Negative number of channels!')
     error = .true.
     return
  endif
  if (order.eq.code_uvt) then
     call wifisyn_message(seve%e,rname,'RUV or TUV buffers must have TUV order')
     error = .true.
     return
  endif
  !
  ! Allocation or reallocation?
  if (allocated(ruv1)) then
     ! Reallocation
     if ((uvr%head%type%ndata.ne.ndata).or.(uvr%head%type%nchan.ne.nchan)) then
        ! Different size => reallocation
        write(mess,'(a,i0,a,i0)') 'Allocating UV arrays to ',ndata,' x ',nchan
        call wifisyn_message(seve%i,rname,mess)
     else
        ! Same size => Nothing to be done!
        write(mess,'(a,i0,a,i0)') 'UV arrays already allocated with size: ',ndata,' x ',nchan
        call wifisyn_message(seve%i,rname,mess)
        return
     endif
  else
     ! Allocation
     write(mess,'(a,i0,a,i0)') 'Creating UV arrays of size: ',ndata,' x ',nchan
     call wifisyn_message(seve%i,rname,mess)
  endif
  !
  ! Free memory when needed
  if (allocated(ruv1)) then
     call wifisyn_free_uv(uvr,error)
     if (error) return
     deallocate(ruv1,ruv2,ruv3)
  endif
  if (allocated(tuv1)) then
     call wifisyn_free_uv(uvt,error)
     if (error) return
     deallocate(tuv1,tuv2,tuv3)
  endif
  !
  ! Reallocate memory of the right size
  allocate(ruv1(ndata,7),ruv2(ndata,2),ruv3(ndata,3,nchan),stat=ier)
  if (failed_allocate(rname,'RUV arrays',ier,error)) return
  allocate(tuv1(ndata,7),tuv2(ndata,2),tuv3(ndata,3,nchan),stat=ier)
  if (failed_allocate(rname,'TUV arrays',ier,error)) return
  !
  ! Associate UVR pointers to RUV buffers
  uvr%buff%uvp1 => ruv1
  uvr%buff%uvp2 => ruv2
  uvr%buff%uvp3 => ruv3
  !
  uvr%head%u    => ruv1(:,1)
  uvr%head%v    => ruv1(:,2)
  uvr%head%scan => ruv1(:,3)
  uvr%head%date => ruv1(:,4)
  uvr%head%time => ruv1(:,5)
  uvr%head%anti => ruv1(:,6)
  uvr%head%antj => ruv1(:,7)
  !
  uvr%head%x    => ruv2(:,1)
  uvr%head%y    => ruv2(:,2)
  !
  uvr%data      => ruv3
  !
  ! Operation success => uvr%head%type%ndata and uvr%head%type%nchan may be updated
  uvr%head%type%ndata = ndata
  uvr%head%type%nchan = nchan
  uvr%head%type%order = code_tuv
  call wifisyn_uvrt_sicdef(uvr,'uvr',error)
  if (error) return
  !
  ! Associate UVT pointers to RUV buffers
  uvt%buff%uvp1 => tuv1
  uvt%buff%uvp2 => tuv2
  uvt%buff%uvp3 => tuv3
  !
  uvt%head%u    => tuv1(:,1)
  uvt%head%v    => tuv1(:,2)
  uvt%head%scan => tuv1(:,3)
  uvt%head%date => tuv1(:,4)
  uvt%head%time => tuv1(:,5)
  uvt%head%anti => tuv1(:,6)
  uvt%head%antj => tuv1(:,7)
  !
  uvt%head%x    => tuv2(:,1)
  uvt%head%y    => tuv2(:,2)
  !
  uvt%data      => tuv3
  !
  ! Operation success => uvt%head%type%ndata and uvt%head%type%nchan may be updated
  uvt%head%type%ndata = ndata
  uvt%head%type%nchan = nchan
  uvt%head%type%order = code_tuv
  call wifisyn_uvrt_sicdef(uvt,'uvt',error)
  if (error) return
  !
end subroutine wifisyn_reallocate_uvrt
!
subroutine wifisyn_reallocate_uv_header(uv,ndata,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_reallocate_uv_header
  use wifisyn_uv_buffer
  !-------------------------------------------------------------------
  ! @ private
  ! (Re)allocate uv header arrays
  ! The headers are not modified at all
  !-------------------------------------------------------------------
  type(uv_t), intent(inout) :: uv
  integer,    intent(in)    :: ndata
  logical,    intent(out)   :: error
  !
  integer :: ier
  character(len=100) :: mess
  character(len=*), parameter :: rname='REALLOCATE/UV/HEADER'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! Sanity checks
  if (ndata.le.0) then
     call wifisyn_message(seve%e,rname,'Negative number of visibilities!')
     error = .true.
     return
  endif
  !
  ! Is UV associated to an internal buffer?
  if (associated(uv%buff%uvp1,ruv1).or.associated(uv%buff%uvp1,tuv1)) then
     call wifisyn_message(seve%e,rname,'R or T UV header buffers cannot be allocated separately')
     error = .true.
     return
  else
     ! Standalone uv data
     ! Allocation or reallocation?
     if (associated(uv%head%u)) then
        ! Reallocation
        if (uv%head%type%ndata.ne.ndata) then
           ! Different size => reallocation
           write(mess,'(a,i0)') 'Allocating UV header arrays to ',ndata
           call wifisyn_message(seve%i,rname,mess)
        else
           ! Same size => Nothing to be done!
           write(mess,'(a,i0)') 'UV header arrays already allocated with size: ',ndata
           call wifisyn_message(seve%i,rname,mess)
           return
        endif
     else
        ! Allocation
        write(mess,'(a,i0,a,i0)') 'Creating UV header arrays of size: ',ndata
        call wifisyn_message(seve%i,rname,mess)
     endif
     !
     ! Reallocate memory of the right size
     allocate(uv%buff%uvp1(ndata,7),uv%buff%uvp2(ndata,2),stat=ier)
     if (failed_allocate(rname,'UV header arrays',ier,error)) return
     !
     ! Associate pointers and arrays
     uv%head%u    => uv%buff%uvp1(:,1)
     uv%head%v    => uv%buff%uvp1(:,2)
     uv%head%scan => uv%buff%uvp1(:,3)
     uv%head%date => uv%buff%uvp1(:,4)
     uv%head%time => uv%buff%uvp1(:,5)
     uv%head%anti => uv%buff%uvp1(:,6)
     uv%head%antj => uv%buff%uvp1(:,7)
     !
     uv%head%x    => uv%buff%uvp2(:,1)
     uv%head%y    => uv%buff%uvp2(:,2)
     !
     ! Operation success => uv%head%type%ndata may be updated
     uv%head%type%ndata = ndata
     !
  endif
  !
end subroutine wifisyn_reallocate_uv_header
!
subroutine wifisyn_reallocate_uv_data(uv,ndata,nchan,order,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_reallocate_uv_data
  use wifisyn_uv_buffer
  !-------------------------------------------------------------------
  ! @ private
  ! (Re)allocate uv data arrays
  ! Do nothing when the array sizes did not changed
  !-------------------------------------------------------------------
  type(uv_t), intent(inout) :: uv
  integer,    intent(in)    :: ndata
  integer,    intent(in)    :: nchan
  integer,    intent(in)    :: order ! uvt or tuv
  logical,    intent(out)   :: error
  !
  integer :: ier
  character(len=20) :: chsize
  character(len=80) :: mess
  character(len=*), parameter :: rname='REALLOCATE/UV/DATA'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! Sanity checks
  if (ndata.le.0) then
     call wifisyn_message(seve%e,rname,'Negative number of visibilities!')
     error = .true.
     return
  endif
  if (nchan.le.0) then
     call wifisyn_message(seve%e,rname,'Negative number of channels!')
     error = .true.
     return
  endif
  if (nchan.ne.uv%head%type%nchan) then
     write(mess,'(a,i0,a,i0)') 'Different sizes for the UV header and data arrays: ',uv%head%type%nchan,' and ',nchan
     call wifisyn_message(seve%e,rname,mess)
     call wifisyn_message(seve%e,rname,'Allocate UV header arrays first')
     error = .true.
     return
  endif
  !
  ! Is UV associated to an internal buffer?
  if (associated(uv%buff%uvp1,ruv1).or.associated(uv%buff%uvp1,tuv1)) then
     call wifisyn_message(seve%e,rname,'R or T UV data buffers cannot be allocated separately')
     error = .true.
     return
  else
     ! Standalone uv data
     ! Allocation or reallocation?
     if (associated(uv%head%u)) then
        ! Check table order
        if (order.eq.code_tuv) then
           write(chsize,'(i0,a,i0)') ndata,' x 3 x ',nchan
        else if (order.eq.code_uvt) then
           write(chsize,'(a,i0,a,i0)') '3 x ',nchan,' x ',ndata
        else
           write(mess,'(a,i0)') 'Unknown uv table order code: ',order
           call wifisyn_message(seve%e,rname,mess)
           error = .true.
           return
        endif
        ! Reallocation
        if ((uv%head%type%ndata.ne.ndata).or.(uv%head%type%nchan.ne.nchan).or.(uv%head%type%order.ne.order)) then
           ! Different size => reallocation
           write(mess,'(a,a)') 'Allocating UV data arrays to ',trim(chsize)
           call wifisyn_message(seve%i,rname,mess)
        else
           ! Same size => Nothing to be done!
           write(mess,'(a,a)') 'UV data arrays already allocated with size: ',trim(chsize)
           call wifisyn_message(seve%i,rname,mess)
           return
        endif
     else
        ! Allocation
        write(mess,'(a,a)') 'Creating UV data arrays of size: ',trim(chsize)
        call wifisyn_message(seve%i,rname,mess)
     endif
     !
     ! Reallocate memory of the right size
     if (order.eq.code_tuv) then
        allocate(uv%buff%uvp3(ndata,3,nchan),stat=ier)
        if (failed_allocate(rname,'UV data arrays',ier,error)) return
     else if (order.eq.code_uvt) then
        allocate(uv%buff%uvp3(3,nchan,ndata),stat=ier)
        if (failed_allocate(rname,'UV data arrays',ier,error)) return
     endif
     !
     ! Associate pointers and arrays
     uv%data => uv%buff%uvp3
     !
     ! Operation success => uv%head%type%nchan may be updated
     uv%head%type%ndata = ndata
     uv%head%type%order = order
     !
  endif
  !
end subroutine wifisyn_reallocate_uv_data
!
subroutine wifisyn_reallocate_uv(uv,ndata,nchan,order,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_reallocate_uv
  use wifisyn_uv_buffer
  !-------------------------------------------------------------------
  ! @ private
  ! (Re)allocate uv header and data arrays
  !-------------------------------------------------------------------
  type(uv_t), intent(inout) :: uv
  integer,    intent(in)    :: ndata
  integer,    intent(in)    :: nchan
  integer,    intent(in)    :: order ! tuv or uvt
  logical,    intent(out)   :: error
  !
  character(len=*), parameter :: rname='REALLOCATE/UV'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! Is UV associated to an internal buffer?
  if (associated(uv%buff%uvp1,ruv1)) then
     ! UV is R buffer
     if ((uv%head%type%ndata.ne.ndata).or.(uv%head%type%nchan.ne.nchan).or.(uv%head%type%order.ne.order)) then
        call wifisyn_reallocate_uvrt(ndata,nchan,order,error)
        if (error) return
     else
        call wifisyn_message(seve%i,rname,'Data buffer already allocated to R')
     endif
  else if (associated(uv%buff%uvp1,tuv1)) then
     ! UV is T buffer
     if ((uv%head%type%ndata.ne.ndata).or.(uv%head%type%nchan.ne.nchan).or.(uv%head%type%order.ne.order)) then
        call wifisyn_reallocate_uvrt(ndata,nchan,order,error)
        if (error) return
     else
        call wifisyn_message(seve%i,rname,'Data buffer already allocated to R')
     endif
  else
     call wifisyn_reallocate_uv_header(uv,ndata,error)
     if (error) return
     call wifisyn_reallocate_uv_data(uv,ndata,nchan,order,error)
     if (error) return
  endif
  !
end subroutine wifisyn_reallocate_uv
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_read_uv_header(do,uv,ndata,nchan,nc,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_read_uv_header
  use wifisyn_readwrite_types
  use wifisyn_uv_types
  !------------------------------------------------------------------
  ! @ private
  ! Read input UV header
  !------------------------------------------------------------------
  type(readwrite_t), intent(in)    :: do
  type(uv_t),        intent(inout) :: uv
  integer,           intent(out)   :: ndata
  integer,           intent(out)   :: nchan
  integer,           intent(out)   :: nc(2)
  logical,           intent(inout) :: error
  !
  character(len=*), parameter :: rname = 'READ/UV/HEADER'
  character(len=100) :: mess
  !
  ! Read header to get the file information
  call sic_parsef(do%name,uv%head%file%file,' ',do%type)
  uv%head%file%blc = 0
  uv%head%file%trc = 0
  call gdf_read_header(uv%head%file,error)
  if (error) return
  !
  ! Some checks
  !
  if (uv%head%file%char%code(1).ne."RANDOM") then
     call wifisyn_message(seve%e,rname,'UV data must be transposed')
     error = .true.
     return
  endif
  !
  if ((modulo(uv%head%file%gil%dim(2)-7,3)).eq.0) then
     ! Single field table
     uv%head%type%wifi = .false.
     nchan = (uv%head%file%gil%dim(2)-7)/3
  else if ((modulo(uv%head%file%gil%dim(2)-9,3)).eq.0) then
     ! Wide field table
     uv%head%type%wifi = .true.
     nchan = (uv%head%file%gil%dim(2)-9)/3
  else
     write(mess,'(a,i0)') 'Wrong number of channels: ',uv%head%file%gil%dim(2)
     call wifisyn_message(seve%e,rname,mess)
     error = .true.
     return
  endif
  ! Check channel range
  nc(1) = min(max(1,do%first),nchan)
  if (do%last.le.0) then
     nc(2) = nchan
  else
     nc(2) = max(1,min(nchan,do%last))
  endif
  ndata = uv%head%file%gil%dim(1)
  !
end subroutine wifisyn_read_uv_header
!
subroutine wifisyn_read_uv_data(uv,ndata,nchan,nc,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_read_uv_data
  use wifisyn_uv_types
  !-------------------------------------------------------------------
  ! @ private
  ! Read uv data
  !-------------------------------------------------------------------
  type(uv_t), intent(inout) :: uv
  integer,    intent(in)    :: ndata
  integer,    intent(in)    :: nchan
  integer,    intent(in)    :: nc(2)
  logical,    intent(out)   :: error
  ! Local
  integer :: mc(2)
  character(len=100) :: mess
  character(len=*), parameter :: rname='READ/UV/DATA'
  !
  ! Check channel range
  mc(1) = min(max(1,nc(1)),nchan)
  if (nc(2).le.0) then
     mc(2) = nchan
  else
     mc(2) = max(1,min(nchan,nc(2)))
  endif
  write(mess,'(a,i0,a,i0)') 'Reading channels: ',mc(1),' to ',mc(2)
  call wifisyn_message(seve%i,rname,mess)
  !
  call wifisyn_reallocate_uv(uv,ndata,mc(2)-mc(1)+1,code_tuv,error)
  if (error) return
  !
  uv%head%file%blc = 0
  uv%head%file%trc = 0
  uv%head%file%blc(2) = 1
  uv%head%file%trc(2) = 7
  call gdf_read_data(uv%head%file,uv%buff%uvp1,error)
  if (gildas_error(uv%head%file,rname,error)) return
  !
  uv%head%file%blc(2) = 5+3*mc(1)
  uv%head%file%trc(2) = 7+3*mc(2)
  call gdf_read_data(uv%head%file,uv%buff%uvp3,error)
  if (gildas_error(uv%head%file,rname,error)) return
  uv%head%type%range = mc
  !
  if (uv%head%type%wifi) then
     ! Wide-Field table => Pointing positions read from table
     uv%head%file%blc(2) = uv%head%file%gil%dim(2)-1
     uv%head%file%trc(2) = uv%head%file%gil%dim(2)
     call gdf_read_data(uv%head%file,uv%buff%uvp2,error)
     if (gildas_error(uv%head%file,rname,error)) return
  else
     ! Single-Field table => Pointing positions are all 0...
     uv%buff%uvp2 = 0
  endif
  !
end subroutine wifisyn_read_uv_data
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_write_uv_header(do,uv,error)
  use gbl_message
  use gildas_def
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_write_uv_header
  use wifisyn_readwrite_types
  use wifisyn_uv_types
  !------------------------------------------------------------------
  ! @ private
  ! Write UV header
  !------------------------------------------------------------------
  type(readwrite_t), intent(in)    :: do
  type(uv_t),        intent(inout) :: uv
  logical,           intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname = 'WRITE/UV/HEADER'
  !
  call sic_parse_file(do%name,' ',do%type,uv%head%file%file)
  uv%head%file%blc = 0
  uv%head%file%trc = 0
  call gdf_create_image(uv%head%file,error)
  if (gildas_error(uv%head%file,rname,error)) return
  uv%head%file%blc(2) = 1
  uv%head%file%trc(2) = 7
  call gdf_write_data(uv%head%file,uv%buff%uvp1,error)
  if (gildas_error(uv%head%file,rname,error)) return
  if (uv%head%type%wifi) then
     ! Wide-Field table => Pointing positions read from table
     uv%head%file%blc(2) = uv%head%file%gil%dim(2)-1
     uv%head%file%trc(2) = uv%head%file%gil%dim(2)
     call gdf_write_data(uv%head%file,uv%buff%uvp2,error)
     if (gildas_error(uv%head%file,rname,error)) return
  endif
  call gdf_update_header(uv%head%file,error)
  if (gildas_error(uv%head%file,rname,error)) return
  call gdf_close_image(uv%head%file,error)
  if (gildas_error(uv%head%file,rname,error)) return
  !
end subroutine wifisyn_write_uv_header
!
subroutine wifisyn_write_uv_data(do,uv,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_write_uv_data
  use wifisyn_readwrite_types
  use wifisyn_uv_types
  !-------------------------------------------------------------------
  ! @ private
  ! Write uv data
  !-------------------------------------------------------------------
  type(readwrite_t), intent(in)    :: do
  type(uv_t),        intent(inout) :: uv
  logical,           intent(inout) :: error
  ! Local
  integer :: nchan,mc(2)
  character(len=100) :: mess
  character(len=*), parameter :: rname='WRITE/UV/DATA'
  !
  ! Check channel range
  nchan = uv%head%type%nchan
  mc(1) = min(max(1,do%first),nchan)
  if (do%last.le.0) then
     mc(2) = nchan
  else
     mc(2) = max(1,min(nchan,do%last))
  endif
  write(mess,'(a,i0,a,i0)') 'Writing channels: ',mc(1),' to ',mc(2)
  call wifisyn_message(seve%i,rname,mess)
  !
  ! Some protection should happen here in case of non-existent file *** JP
  call gdf_open_image(uv%head%file,error)
  if (gildas_error(uv%head%file,rname,error)) return
  uv%head%file%blc = 0
  uv%head%file%trc = 0
  uv%head%file%blc(2) = 5+3*mc(1)
  uv%head%file%trc(2) = 7+3*mc(2)
  call gdf_write_data(uv%head%file,uv%buff%uvp3,error)
  if (gildas_error(uv%head%file,rname,error)) return
  uv%head%type%range = mc
  call gdf_close_image(uv%head%file,error)
  if (gildas_error(uv%head%file,rname,error)) return
  !
end subroutine wifisyn_write_uv_data
!
subroutine wifisyn_write_uv(do,uv,error)
  use gbl_message
  use gildas_def
  use wifisyn_interfaces, except_this=>wifisyn_write_uv
  use wifisyn_readwrite_types
  use wifisyn_uv_types
  !------------------------------------------------------------------
  ! @ private
  ! Read input UV header
  !------------------------------------------------------------------
  type(readwrite_t), intent(in)    :: do
  type(uv_t),        intent(inout) :: uv
  logical,           intent(inout) :: error
  !
  character(len=*), parameter :: rname = 'WRITE/UV'
  !
  call wifisyn_write_uv_header(do,uv,error)
  if (error) return
  call wifisyn_write_uv_data(do,uv,error)
  if (error) return
  !
end subroutine wifisyn_write_uv
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_copy_uv_header(in,out,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_copy_uv_header
  use wifisyn_uv_types
  !------------------------------------------------------------------
  ! @ private
  ! Copy header information
  !------------------------------------------------------------------
  type(uv_t), intent(in)    :: in
  type(uv_t), intent(inout) :: out
  logical,    intent(inout) :: error
  !
  integer(kind=4) :: idata
  character(len=*), parameter :: rname='COPY/UV/HEADER'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call gdf_copy_header(in%head%file,out%head%file,error)
  if (error)  return
  out%head%type = in%head%type
  if (associated(out%buff%uvp1).and.associated(in%buff%uvp1)) then
     do idata=1,in%head%type%ndata
        out%buff%uvp1(idata,:) = in%buff%uvp1(idata,:)
     enddo ! idata
  else
     call wifisyn_message(seve%d,rname,'Unassociated UVP1')
     error = .true.
     return
  endif
  if (associated(out%buff%uvp2).and.associated(in%buff%uvp2)) then
     do idata=1,in%head%type%ndata
        out%buff%uvp2(idata,:) = in%buff%uvp2(idata,:)
     enddo ! idata
  else
     call wifisyn_message(seve%d,rname,'Unassociated UVP2')
     error = .true.
     return
  endif
  !
end subroutine wifisyn_copy_uv_header
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
