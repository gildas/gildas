!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_read_command(line,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_read_command
  use wifisyn_readwrite_types
  !----------------------------------------------------------------------
  ! @ private
  ! Support routine for command READ
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  logical,          intent(inout) :: error
  !
  type(readwrite_t) :: do
  character(len=*), parameter :: rname='READ/COMMAND'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call wifisyn_parse_readwrite(line,do,error)
  if (error) return
  call wifisyn_read_main(do,error)
  if (error) return
  !
end subroutine wifisyn_read_command
!
subroutine wifisyn_read_main(do,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_read_main
  use wifisyn_readwrite_types
  use wifisyn_uv_buffer
  use wifisyn_cube_buffer
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(readwrite_t), intent(inout) :: do
  logical,           intent(inout) :: error
  !
  character(len=*), parameter :: rname = 'READ/MAIN'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! Dispatch according to buffer
  if (do%buffer.eq.code_buffer_uv) then
     call wifisyn_read_uv(do,uvr,error)
  else if (do%buffer.eq.code_buffer_beam) then
     call wifisyn_cube_read(do,dirty%beam,error)
     if (error) return
  else if (do%buffer.eq.code_buffer_dirty) then
     call wifisyn_cube_read(do,dirty%image,error)
     if (error) return
  else if (do%buffer.eq.code_buffer_wifi) then
     call wifisyn_read_wifi(do,wifi,error)
     if (error) return
  else
     call wifisyn_message(seve%e,'READ','Reading of '//do%kind//' not yet implemented')
     error = .true.
     return
  endif
  !
end subroutine wifisyn_read_main
!
subroutine wifisyn_read_uv(do,uv,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_read_uv
  use wifisyn_readwrite_types
  use wifisyn_uv_types
  !------------------------------------------------------------------
  ! @ private
  ! Read input UV header and data
  !------------------------------------------------------------------
  type(readwrite_t), intent(in)    :: do
  type(uv_t),        intent(inout) :: uv
  logical,           intent(inout) :: error
  !
  integer :: nvisi,nchan,nc(2)
  character(len=*), parameter :: rname='READ/UV'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! *** JP Is the use of nchan and nc consistent?
  call wifisyn_read_uv_header(do,uv,nvisi,nchan,nc,error)
  if (error) return
  call wifisyn_read_uv_data(uv,nvisi,nchan,nc,error)
  if (error) return
  call gdf_close_image(uv%head%file,error) ! Free image slot
  if (error) return
  !
end subroutine wifisyn_read_uv
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
