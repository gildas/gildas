!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_axis_offset2pixel(axis,doff,ioff,error)
  use gbl_message
  use wifisyn_primitive_types
  use wifisyn_interfaces, except_this=>wifisyn_axis_offset2pixel
  !----------------------------------------------------------------------
  ! @ public
  ! Convert an offset into the corresponding pixel number.
  ! Make sure that the returned pixel number is inside the axis range.
  !----------------------------------------------------------------------
  type(axis_1d_t), intent(in)    :: axis
  real(kind=8),    intent(in)    :: doff
  integer,         intent(out)   :: ioff
  logical,         intent(inout) :: error
  !
  character(len=*), parameter :: rname='AXIS/OFFSET2PIXEL'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (axis%inc.ne.0.0) then
     ioff = nint(axis%ref+(doff-axis%val)/axis%inc)
     ioff = min(axis%n,max(1,ioff))
  else
     call wifisyn_message(seve%e,rname,'Increment is zero valued')
     error = .true.
     return
  endif
  !
end subroutine wifisyn_axis_offset2pixel
!
subroutine wifisyn_axis_pixel2offset(axis,ioff,doff,error)
  use gbl_message
  use wifisyn_primitive_types
  use wifisyn_interfaces, except_this=>wifisyn_axis_pixel2offset
  !----------------------------------------------------------------------
  ! @ public
  !----------------------------------------------------------------------
  type(axis_1d_t), intent(in)    :: axis
  integer,         intent(in)    :: ioff
  real(kind=8),    intent(out)   :: doff
  logical,         intent(inout) :: error
  !
  character(len=*), parameter :: rname='AXIS/PIXEL2OFFSET'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  doff = (ioff-axis%ref)*axis%inc+axis%val
  !
end subroutine wifisyn_axis_pixel2offset
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_axis_ddx2idx(axis,ddx,idx,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_axis_ddx2idx
  use wifisyn_primitive_types
  !----------------------------------------------------------------------
  ! @ public
  !----------------------------------------------------------------------
  type(axis_1d_t), intent(in)    :: axis
  real(kind=8),    intent(in)    :: ddx
  integer,         intent(out)   :: idx
  logical,         intent(inout) :: error
  !
  character(len=*), parameter :: rname='WIFISYN/AXIS/DDX2IDX'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (axis%inc.ne.0.0) then
     idx = nint(ddx/axis%inc)
  else
     call wifisyn_message(seve%e,rname,'Increment is zero valued')
     error = .true.
     return
  endif
  !
end subroutine wifisyn_axis_ddx2idx
!
subroutine wifisyn_axis_idx2ddx(axis,idx,ddx,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_axis_idx2ddx
  use wifisyn_primitive_types
  !----------------------------------------------------------------------
  ! @ public
  !----------------------------------------------------------------------
  type(axis_1d_t), intent(in)    :: axis
  integer,         intent(in)    :: idx
  real(kind=8),    intent(out)   :: ddx
  logical,         intent(inout) :: error
  !
  character(len=*), parameter :: rname='WIFISYN/AXIS/IDX2DDX'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ddx = idx*axis%inc
  !
end subroutine wifisyn_axis_idx2ddx
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
