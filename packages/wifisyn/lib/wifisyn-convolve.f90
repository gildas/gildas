!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_convolve_wifi_uvkern(map,wifi,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_convolve_wifi_uvkern
  use wifisyn_cube_types
  use wifisyn_transform_types
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(map_t), target, intent(in)    :: map
  type(wifi_t),        intent(inout) :: wifi
  logical,             intent(inout) :: error
  !
  integer :: ix,iy,iu,iv,nu,nv
  real(kind=4) :: norm,vkernftval,uvkernftval
  type(kernel_1d_t), pointer :: ukern,vkern
  character(len=*), parameter :: rname='WIFISYN/CONVOLVE/WIFI/UVKERN'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call wifisyn_fft_wifi(code_inverse,code_space_uv,map%win%cur(1),wifi,error)
  if (error) return
  ukern => map%kern(1)%u
  vkern => map%kern(2)%u
  nu = ukern%ft%n
  nv = vkern%ft%n
  norm = 1.0/(wifi%cub%nv*wifi%cub%nu*ukern%ft%val(nv/2+1)*vkern%ft%val(nu/2+1))
  do iv=1,wifi%cub%nv
     vkernftval = vkern%ft%val(iv)*norm
     do iu=1,wifi%cub%nu
        uvkernftval = vkernftval*ukern%ft%val(iu)
        do iy=1,wifi%cub%ny
           do ix=1,wifi%cub%nx
              wifi%cub%val(:,ix,iy,iu,iv) = wifi%cub%val(:,ix,iy,iu,iv)*uvkernftval
           enddo ! ix
        enddo ! iy
     enddo ! iu
  enddo ! iv
  call wifisyn_fft_wifi(code_direct,code_space_uv,map%win%cur(1),wifi,error)
  if (error) return
  !
end subroutine wifisyn_convolve_wifi_uvkern
!
subroutine wifisyn_deconvolve_wifi_uvkern(map,wifi,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_deconvolve_wifi_uvkern
  use wifisyn_cube_types
  use wifisyn_transform_types
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(map_t), target, intent(in)    :: map
  type(wifi_t),        intent(inout) :: wifi
  logical,             intent(inout) :: error
  !
  integer :: ix,iy,iu,iv,nu,nv
  real(kind=4) :: norm,vkernftval,uvinvkernftval
  type(kernel_1d_t), pointer :: ukern,vkern
  character(len=*), parameter :: rname='WIFISYN/DECONVOLVE/WIFI/UVKERN'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call wifisyn_fft_wifi(code_inverse,code_space_uv,map%win%cur(1),wifi,error)
  if (error) return
  ukern => map%kern(1)%u
  vkern => map%kern(2)%u
  nu = ukern%ft%n
  nv = vkern%ft%n
  norm = (ukern%ft%val(nv/2+1)*vkern%ft%val(nu/2+1))/(wifi%cub%nv*wifi%cub%nu)
  do iv=1,wifi%cub%nv
     vkernftval = vkern%ft%val(iv)*norm
     do iu=1,wifi%cub%nu
        uvinvkernftval = 1.0/(vkernftval*ukern%ft%val(iu))
        do iy=1,wifi%cub%ny
           do ix=1,wifi%cub%nx
              wifi%cub%val(:,ix,iy,iu,iv) = wifi%cub%val(:,ix,iy,iu,iv)*uvinvkernftval
           enddo ! ix
        enddo ! iy
     enddo ! iu
  enddo ! iv
  call wifisyn_fft_wifi(code_direct,code_space_uv,map%win%cur(1),wifi,error)
  if (error) return
  !
end subroutine wifisyn_deconvolve_wifi_uvkern
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
