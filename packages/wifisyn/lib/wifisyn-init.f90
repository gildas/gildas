!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_init(error)
  use wifisyn_interfaces, except_this=>wifisyn_init
  use wifisyn_uv_types
  !----------------------------------------------------------------------
  ! @ private
  ! WIFISYN Initialization
  !----------------------------------------------------------------------
  logical, intent(inout) :: error
  !
  call wifisyn_reallocate_uvrt(1,1,code_tuv,error)
  if (error) return
  call wifisyn_gridding_sicdef_user(error)
  if (error) return
  !
end subroutine wifisyn_init
!
subroutine wifisyn_exit(error)
  use wifisyn_interfaces, except_this=>wifisyn_exit
  use wifisyn_uv_buffer
  !----------------------------------------------------------------------
  ! @ private
  ! WIFISYN Initialization
  !----------------------------------------------------------------------
  logical, intent(inout) :: error
  !
  call wifisyn_free_uvrt(error)
  if (error) return
  !
end subroutine wifisyn_exit
!
subroutine wifisyn_load
  use wifisyn_interfaces, except_this=>wifisyn_load
  !----------------------------------------------------------------------
  ! @ private
  ! WIFISYN language definition
  !----------------------------------------------------------------------
  external gr_error ! *** JP: Why???
  !
  ! wifisyn vocabulary
  integer, parameter ::  mwifisyn = 37
  character(len=12) :: vocab(mwifisyn)
  character(len=40) version
  data vocab /&
       ' APODIZE', '/DIRECT', '/INVERSE', '/UV', '/XY', &
       ' COMPLEX', '/LOGSCALE', '/NORMALIZE', &
       ' FFT', '/DIRECT', '/INVERSE', '/UV', '/XY', &
       ' LOAD', '/AMPLITUDE', '/NORMALIZE', &
       ' READ', '/CHANNELS', &
       ' SETUP', &
       ' SHIFT', '/DIRECT', '/INVERSE', '/UV', '/VISI', '/WIFI', &
       ' STATISTICS', &
       ' UVBEAM', &
       ' UVGRID', '/SHIFT', &
       ' UVMAP', &
       ' UVSORT', &
       ' UVSWAP', &
       ' UVSYMMETRY', &
       ' VARIABLE', &
       ' WIFI2VISI', &
       ' WRITE', '/CHANNELS'/
  !
  version = '0.2 J.Pety'
  call sic_begin('WIFISYN','GAG_HELP_WIFISYN',mwifisyn,vocab,version,&
       wifisyn_run,gr_error)
  !
end subroutine wifisyn_load
!
subroutine wifisyn_run(line,command,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_run
  !----------------------------------------------------------------------
  ! @ private
  ! WIFISYN Main routine: Call appropriate subroutine according to COMMAND
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  character(len=*), intent(in)    :: command
  logical,          intent(out)   :: error
  !
  error = .false.
  select case (command)
  case ('APODIZE')
     call wifisyn_apodize_command(line,error)
  case ('COMPLEX')
     call wifisyn_complex_command(line,error)
  case ('FFT')
     call wifisyn_fft_command(line,error)
  case ('LOAD')
     call wifisyn_load_command(line,error)
  case ('READ')
     call wifisyn_read_command(line,error)
  case ('SETUP')
     call wifisyn_setup_command(line,error)
  case ('SHIFT')
     call wifisyn_shift_command(line,error)
  case ('STATISTICS')
     call wifisyn_statistics_command(line,error)
  case ('UVBEAM')
     call wifisyn_uvbeam_command(line,error)
  case ('UVGRID')
     call wifisyn_uvgrid_command(line,error)
  case ('UVMAP')
     call wifisyn_uvmap_command(line,error)
  case ('UVSORT')
     call wifisyn_uvsort_command(line,error)
  case ('UVSWAP')
     call wifisyn_uvswap_command(error)
  case ('UVSYMMETRY')
     call wifisyn_uvsym_command(line,error)
  case ('VARIABLE')
     call wifisyn_variable_command(line,error)
  case ('WIFI2VISI')
     call wifisyn_wifi2visi_command(line,error)
  case ('WRITE')
     call wifisyn_write_command(line,error)
  case default
     call wifisyn_message(seve%i,'WIFISYN',command//' not yet implemented')
  end select
  !
end subroutine wifisyn_run
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
