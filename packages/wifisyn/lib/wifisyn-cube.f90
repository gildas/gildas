!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_cube_reallocate_r4d(kind,cube,error)
  use gbl_message
  use image_def
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_cube_reallocate_r4d
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*), intent(in)    :: kind
  type(gildas),     intent(inout) :: cube
  logical,          intent(inout) :: error
  !
  integer :: ier,nx,ny,nu,nv
  character(len=80) :: mess
  character(len=*), parameter :: rname='CUBE/REALLOCATE/R4D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! Free when needed
  call wifisyn_cube_free_r4d(cube,error)
  ! Allocate
  nx = cube%gil%dim(1)
  ny = cube%gil%dim(2)
  nu = cube%gil%dim(3)
  nv = cube%gil%dim(4)
  allocate(cube%r4d(nx,ny,nu,nv),stat=ier)
  if (failed_allocate(rname,kind,ier,error)) return
  ! User feedback
  write(mess,'(a,a,a,i0,a,i0,a,i0,a,i0)') 'Allocated ',kind,' array of size: ',nx,' x ',ny,' x ',nu,' x ',nv
  call wifisyn_message(seve%i,rname,mess)
  cube%gil%ndim = 4
  ! Initialize
  cube%r4d = cube%gil%bval
  !
end subroutine wifisyn_cube_reallocate_r4d
!
subroutine wifisyn_cube_free_r4d(cube,error)
  use gbl_message
  use image_def
  use wifisyn_interfaces, except_this=>wifisyn_cube_free_r4d
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(gildas), intent(inout) :: cube
  logical,      intent(inout) :: error
  !
  character(len=*), parameter :: rname='CUBE/FREE/R4D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (associated(cube%r4d)) deallocate(cube%r4d)
  nullify(cube%r4d)
  !
end subroutine wifisyn_cube_free_r4d
!
subroutine wifisyn_cube_reallocate_r3d(nc,kind,cube,error)
  use gbl_message
  use image_def
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_cube_reallocate_r3d
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  integer,          intent(in)    :: nc
  character(len=*), intent(in)    :: kind
  type(gildas),     intent(inout) :: cube
  logical,          intent(inout) :: error
  !
  integer :: ier,nx,ny
  character(len=80) :: mess
  character(len=*), parameter :: rname='CUBE/REALLOCATE/R3D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! Free when needed
  call wifisyn_cube_free_r3d(cube,error)
  ! Allocate
  nx = cube%gil%dim(1)
  ny = cube%gil%dim(2)
  if (nc.gt.cube%gil%dim(3)) then
     write(mess,'(a,i0,a,i0,a)') 'Trying to allocate more channels (',nc,') than expected in file (',cube%gil%dim(3),')'
     call wifisyn_message(seve%e,rname,mess)
     error = .true.
     return
  endif
  allocate(cube%r3d(nx,ny,nc),stat=ier)
  if (failed_allocate(rname,kind,ier,error)) return
  ! User feedback
  write(mess,'(a,a,a,i0,a,i0,a,i0)') 'Allocated ',kind,' array of size: ',nx,' x ',ny,' x ',nc
  call wifisyn_message(seve%i,rname,mess)
  ! Header update (do *not* update the 3rd header dimensions here...)
  cube%gil%ndim = 3
  cube%gil%dim(4) = 1
  ! Initialize
  cube%r3d = cube%gil%bval
  !
end subroutine wifisyn_cube_reallocate_r3d
!
subroutine wifisyn_cube_free_r3d(cube,error)
  use gbl_message
  use image_def
  use wifisyn_interfaces, except_this=>wifisyn_cube_free_r3d
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(gildas), intent(inout) :: cube
  logical,      intent(inout) :: error
  !
  character(len=*), parameter :: rname='CUBE/FREE/R3D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (associated(cube%r3d)) deallocate(cube%r3d)
  nullify(cube%r3d)
  !
end subroutine wifisyn_cube_free_r3d
!
subroutine wifisyn_cube_reallocate_r2d(kind,cube,error)
  use gbl_message
  use image_def
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_cube_reallocate_r2d
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*), intent(in)    :: kind
  type(gildas),     intent(inout) :: cube
  logical,          intent(inout) :: error
  !
  integer :: ier,nx,ny
  character(len=80) :: mess
  character(len=*), parameter :: rname='CUBE/REALLOCATE/R2D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! Free when needed
  call wifisyn_cube_free_r2d(cube,error)
  ! Allocate
  nx = cube%gil%dim(1)
  ny = cube%gil%dim(2)
  allocate(cube%r2d(nx,ny),stat=ier)
  if (failed_allocate(rname,kind,ier,error)) return
  ! User feedback
  write(mess,'(a,a,a,i0,a,i0)') 'Allocated ',kind,' array of size: ',nx,' x ',ny
  call wifisyn_message(seve%i,rname,mess)
  ! Header update
  cube%gil%ndim = 2
  cube%gil%dim(3) = 1
  cube%gil%dim(4) = 1
  ! Initialize
  cube%r2d = cube%gil%bval
  !
end subroutine wifisyn_cube_reallocate_r2d
!
subroutine wifisyn_cube_free_r2d(cube,error)
  use gbl_message
  use image_def
  use wifisyn_interfaces, except_this=>wifisyn_cube_free_r2d
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(gildas), intent(inout) :: cube
  logical,      intent(inout) :: error
  !
  character(len=*), parameter :: rname='CUBE/FREE/R2D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (associated(cube%r2d)) deallocate(cube%r2d)
  nullify(cube%r2d)
  !
end subroutine wifisyn_cube_free_r2d
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_cube_minmax_r4d(logscale,cube,error)
  use gbl_message
  use image_def
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_cube_minmax_r4d
  use wifisyn_gridding_types
  !--------------------------------------------------------------------
  ! @ private
  ! Update the minimum and maximum value of a cube and
  ! their locations. We here assume that the initializations
  ! were done when the cube was initialized...
  !--------------------------------------------------------------------
  logical,        intent(in)    :: logscale
  type(gildas),   intent(inout) :: cube
  logical,        intent(inout) :: error
  !
  real(kind=4) :: cubval
  integer(kind=index_length) :: ix,iy,iu,iv
  character(len=*), parameter :: rname='CUBE/MINMAX/R4D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (cube%gil%ndim.ne.4) then
     call wifisyn_message(seve%e,rname,'Cube rank is not 4')
     error = .true.
     return
  endif
  !
  do iv=1,cube%gil%dim(4)
     do iu=1,cube%gil%dim(3)
        do iy=1,cube%gil%dim(2)
           do ix=1,cube%gil%dim(1)
              cubval = cube%r4d(ix,iy,iu,iv)
              if (logscale.and.(cubval.eq.0.0)) cycle
              if (cubval.gt.cube%gil%rmax) then
                 cube%gil%rmax = cubval
                 cube%gil%maxloc(1) = ix
                 cube%gil%maxloc(2) = iy
                 cube%gil%maxloc(3) = iu
                 cube%gil%maxloc(4) = iv
              endif
              if (cubval.lt.cube%gil%rmin) then
                 cube%gil%rmin = cubval
                 cube%gil%minloc(1) = ix
                 cube%gil%minloc(2) = iy
                 cube%gil%minloc(3) = iu
                 cube%gil%minloc(4) = iv
              endif
           enddo ! ix
        enddo ! iy
     enddo ! iu
  enddo ! iv
  if (logscale.and.(cube%gil%rmin.eq.cube%gil%rmax)) cube%gil%rmin = cube%gil%rmax/10.0
  !
  print *,"max:",cube%gil%rmax,cube%gil%maxloc(1:4)
  print *,"min:",cube%gil%rmin,cube%gil%minloc(1:4)
  !
end subroutine wifisyn_cube_minmax_r4d
!
subroutine wifisyn_cube_minmax_r3d(logscale,win,cube,error)
  use gbl_message
  use image_def
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_cube_minmax_r3d
  use wifisyn_gridding_types
  !--------------------------------------------------------------------
  ! @ private
  ! Update the minimum and maximum value of a cube and
  ! their locations. We here assume that the initializations
  ! were done when the cube was initialized...
  !--------------------------------------------------------------------
  logical,        intent(in)    :: logscale
  type(window_t), intent(in)    :: win
  type(gildas),   intent(inout) :: cube
  logical,        intent(inout) :: error
  !
  real(kind=4) :: cubval
  integer(kind=index_length) :: ix,iy,ic
  character(len=*), parameter :: rname='CUBE/MINMAX/R3D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (cube%gil%ndim.ne.3) then
     call wifisyn_message(seve%e,rname,'Cube rank is not 3')
     error = .true.
     return
  endif
  !
  do ic=win%first,win%last
     do iy=1,cube%gil%dim(2)
        do ix=1,cube%gil%dim(1)
           cubval = cube%r3d(ix,iy,ic)
           if (logscale.and.(cubval.eq.0.0)) cycle
           if (cubval.gt.cube%gil%rmax) then
              cube%gil%rmax = cubval
              cube%gil%maxloc(1) = ix
              cube%gil%maxloc(2) = iy
              cube%gil%maxloc(3) = ic
           endif
           if (cubval.lt.cube%gil%rmin) then
              cube%gil%rmin = cubval
              cube%gil%minloc(1) = ix
              cube%gil%minloc(2) = iy
              cube%gil%minloc(3) = ic
           endif
        enddo ! ix
     enddo ! iy
  enddo ! ic
  if (logscale.and.(cube%gil%rmin.eq.cube%gil%rmax)) cube%gil%rmin = cube%gil%rmax/10.0
  !
  print *,"max:",cube%gil%rmax,cube%gil%maxloc(1:3)
  print *,"min:",cube%gil%rmin,cube%gil%minloc(1:3)
  !
end subroutine wifisyn_cube_minmax_r3d
!
subroutine wifisyn_cube_minmax_r2d(logscale,cube,error)
  use gbl_message
  use image_def
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_cube_minmax_r2d
  use wifisyn_gridding_types
  !--------------------------------------------------------------------
  ! @ private
  ! Update the minimum and maximum value of a cube and
  ! their locations. We here assume that the initializations
  ! were done when the cube was initialized...
  !--------------------------------------------------------------------
  logical,        intent(in)    :: logscale
  type(gildas),   intent(inout) :: cube
  logical,        intent(inout) :: error
  !
  real(kind=4) :: cubval
  integer(kind=index_length) :: ix,iy
  character(len=*), parameter :: rname='CUBE/MINMAX/R2D'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (cube%gil%ndim.ne.2) then
     call wifisyn_message(seve%e,rname,'Cube rank is not 2')
     error = .true.
     return
  endif
  do iy=1,cube%gil%dim(2)
     do ix=1,cube%gil%dim(1)
        cubval = cube%r2d(ix,iy)
        if (logscale.and.(cubval.eq.0.0)) cycle
        if (cubval.gt.cube%gil%rmax) then
           cube%gil%rmax = cubval
           cube%gil%maxloc(1) = ix
           cube%gil%maxloc(2) = iy
        endif
        if (cubval.lt.cube%gil%rmin) then
           cube%gil%rmin = cubval
           cube%gil%minloc(1) = ix
           cube%gil%minloc(2) = iy
        endif
     enddo ! ix
  enddo ! iy
  !
end subroutine wifisyn_cube_minmax_r2d
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_cube_open(name,ibuffer,cube,error)
  use gbl_message
  use image_def
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_cube_open
  use wifisyn_buffer_parameters
  !----------------------------------------------------------------------
  ! @ private
  ! *** JP should probably here use the readwrite_t
  !----------------------------------------------------------------------
  character(len=*), intent(in)    :: name
  integer,          intent(in)    :: ibuffer
  type(gildas),     intent(inout) :: cube
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CUBE/OPEN'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if ((ibuffer.lt.1).or.(mbuffer.lt.ibuffer)) then
     call wifisyn_message(seve%e,rname,'Unknown buffer')
     error = .true.
     return
  endif
  call sic_parse_file(name,' ',known_buffer_type(ibuffer),cube%file)
  call gdf_create_image(cube,error)
  if (gildas_error(cube,rname,error)) return
  !
end subroutine wifisyn_cube_open
!
subroutine wifisyn_cube_read(do,cube,error)
  use gbl_message
  use image_def
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_cube_read
  use wifisyn_readwrite_types
  use wifisyn_cube_types
  !------------------------------------------------------------------
  ! @ private
  ! Read input cube header and data
  !------------------------------------------------------------------
  type(readwrite_t), intent(in)    :: do
  type(gildas),      intent(inout) :: cube
  logical,           intent(inout) :: error
  ! Local
  integer :: nchan
  character(len=*), parameter :: rname = 'CUBE/READ'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call sic_parse_file(do%name,' ',do%type,cube%file)
  cube%blc = 0
  cube%trc = 0
  call gdf_read_header(cube,error)
  if (gildas_error(cube,rname,error)) return
  if (cube%gil%ndim.eq.3) then
     nchan = max(1,do%last-do%first)
     call wifisyn_cube_reallocate_r3d(nchan,do%kind,cube,error)
     if (error) return
     call gdf_read_data(cube,cube%r3d,error)
     if (gildas_error(cube,rname,error)) return
  else if (cube%gil%ndim.eq.4) then
     call wifisyn_cube_reallocate_r4d(do%kind,cube,error)
     if (error) return
     call gdf_read_data(cube,cube%r4d,error)
     if (gildas_error(cube,rname,error)) return
  endif
  call gdf_close_image(cube,error) ! Free image slot
  if (gildas_error(cube,rname,error)) return
  !
end subroutine wifisyn_cube_read
!
subroutine wifisyn_cube_write_r4d(i3,i4,cube,error)
  use gbl_message
  use gildas_def
  use image_def
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_cube_write_r4d
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  integer,              intent(in)    :: i3
  integer,              intent(in)    :: i4
  type(gildas), target, intent(inout) :: cube
  logical,              intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CUBE/WRITE/R4D'
  !
  real, pointer :: cube_r2d(:,:) => null()
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  cube%blc(:) = 0
  cube%blc(3) = i3
  cube%blc(4) = i4
  cube%trc(:) = 0
  cube%trc(3) = i3
  cube%trc(4) = i4
  cube_r2d => cube%r4d(:,:,i3,i4)
  call gdf_write_data(cube,cube_r2d,error)
  if (gildas_error(cube,rname,error)) return
  !
end subroutine wifisyn_cube_write_r4d
!
subroutine wifisyn_cube_write(win,cube,error)
  use gbl_message
  use gildas_def
  use image_def
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_cube_write
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(window_t), intent(in)    :: win
  type(gildas),   intent(inout) :: cube
  logical,        intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CUBE/WRITE'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  cube%blc(:) = 0
  cube%blc(3) = win%first
  cube%trc(:) = 0
  cube%trc(3) = win%last
  call gdf_write_data(cube,cube%r3d,error)
  if (gildas_error(cube,rname,error)) return
  !
end subroutine wifisyn_cube_write
!
subroutine wifisyn_cube_close(cube,error)
  use gbl_message
  use image_def
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_cube_close
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(gildas),     intent(inout) :: cube
  logical,          intent(inout) :: error
  ! Local
  character(len=*), parameter :: rname='CUBE/CLOSE'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call gdf_update_header(cube,error)
  if (gildas_error(cube,rname,error)) return
  call gdf_close_image(cube,error)
  if (gildas_error(cube,rname,error)) return
  !
end subroutine wifisyn_cube_close
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_cube_sicdef(readonly,name,cube,error)
  use gbl_message
  use image_def
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_cube_sicdef
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  logical,          intent(in)    :: readonly
  character(len=*), intent(in)    :: name ! sic name
  type(gildas),     intent(inout) :: cube ! buffer
  logical,          intent(inout) :: error
  !
  integer :: ndim
  character(len=80) :: mess
  character(len=*), parameter :: rname='CUBE/SICDEF'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  call wifisyn_message(seve%i,rname,'Defining '//trim(name)//' SIC structure')
  !
  if (sic_varexist(name)) then
     call wifisyn_message(seve%d,rname,trim(name)//' SIC structure already exists')
     call wifisyn_cube_sicdel_header(name,error)
     if (error) return
     call sic_delvariable(name,.false.,error)
     if (error) return
  endif
  ndim = cube%gil%ndim
  if (ndim.eq.2) then
     call sic_def_real(name,cube%r2d,ndim,cube%gil%dim,readonly,error)
     if (error) return
  else if (ndim.eq.3) then
     call sic_def_real(name,cube%r3d,ndim,cube%gil%dim,readonly,error)
     if (error) return
  else if (ndim.eq.4) then
     call sic_def_real(name,cube%r4d,ndim,cube%gil%dim,readonly,error)
     if (error) return
  else
     write(mess,*) ndim,' dimensions in ',trim(name),' while expecting at least 2'
     call wifisyn_message(seve%e,rname,mess)
     error = readonly
     return
  endif
  call sic_def_header(name,cube,readonly,error)
  if (error) return
  !
end subroutine wifisyn_cube_sicdef
!
subroutine wifisyn_cube_sicdel_header(name,error)
  use gbl_message
  use gildas_def
  use image_def
  use wifisyn_interfaces, except_this=>wifisyn_cube_sicdel_header
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  character(len=*), intent(in)    :: name
  logical,          intent(inout) :: error
  !
  integer :: nn
  character(len=varname_length) :: pre
  character(len=*), parameter :: rname='CUBE/SICDEL/HEADER'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  nn = len_trim(name)+1
  pre = name
  pre(nn:nn) = '%'
  !
  ! GENERAL section
  call sic_delvariable(pre(1:nn)//'GENE',.false.,error)
  call sic_delvariable(pre(1:nn)//'NDIM',.false.,error)
  call sic_delvariable(pre(1:nn)//'DIM',.false.,error)
  call sic_delvariable(pre(1:nn)//'CONVERT',.false.,error)
  ! Blanking section
  call sic_delvariable(pre(1:nn)//'BLAN',.false.,error)
  call sic_delvariable(pre(1:nn)//'BLANK',.false.,error)
  ! Extrema
  call sic_delvariable(pre(1:nn)//'EXTREMA',.false.,error)
  call sic_delvariable(pre(1:nn)//'MIN',.false.,error)
  call sic_delvariable(pre(1:nn)//'MAX',.false.,error)
  call sic_delvariable(pre(1:nn)//'MAXLOC',.false.,error)
  call sic_delvariable(pre(1:nn)//'MINLOC',.false.,error)
  ! Units and cooreadonlyinate system
  call sic_delvariable(pre(1:nn)//'DESC',.false.,error)
  call sic_delvariable(pre(1:nn)//'UNIT',.false.,error)
  call sic_delvariable(pre(1:nn)//'UNIT1',.false.,error)
  call sic_delvariable(pre(1:nn)//'UNIT2',.false.,error)
  call sic_delvariable(pre(1:nn)//'UNIT3',.false.,error)
  call sic_delvariable(pre(1:nn)//'UNIT4',.false.,error)
  call sic_delvariable(pre(1:nn)//'SYSTEM',.false.,error)
  !
  ! Astronomical position
  call sic_delvariable(pre(1:nn)//'POSI',.false.,error)
  call sic_delvariable(pre(1:nn)//'SOURCE',.false.,error)
  call sic_delvariable(pre(1:nn)//'RA',.false.,error)
  call sic_delvariable(pre(1:nn)//'DEC',.false.,error)
  call sic_delvariable(pre(1:nn)//'LII',.false.,error)
  call sic_delvariable(pre(1:nn)//'BII',.false.,error)
  !
  call sic_delvariable(pre(1:nn)//'EPOCH',.false.,error)
  !
  ! Projection
  call sic_delvariable(pre(1:nn)//'PROJ',.false.,error)
  call sic_delvariable(pre(1:nn)//'PTYPE',.false.,error)
  call sic_delvariable(pre(1:nn)//'A0',.false.,error)
  call sic_delvariable(pre(1:nn)//'D0',.false.,error)
  call sic_delvariable(pre(1:nn)//'ANGLE',.false.,error)
  call sic_delvariable(pre(1:nn)//'X_AXIS',.false.,error)
  call sic_delvariable(pre(1:nn)//'Y_AXIS',.false.,error)
  !
  ! Spectroscopy
  call sic_delvariable(pre(1:nn)//'SPEC',.false.,error)
  call sic_delvariable(pre(1:nn)//'LINE',.false.,error)
  call sic_delvariable(pre(1:nn)//'FREQRES',.false.,error)
  call sic_delvariable(pre(1:nn)//'IMAGFRE',.false.,error)
  call sic_delvariable(pre(1:nn)//'RESTFRE',.false.,error)
  call sic_delvariable(pre(1:nn)//'VELRES',.false.,error)
  call sic_delvariable(pre(1:nn)//'VELOFF',.false.,error)
  call sic_delvariable(pre(1:nn)//'F_AXIS',.false.,error)
  !
  ! Beam size
  call sic_delvariable(pre(1:nn)//'BEAM',.false.,error)
  call sic_delvariable(pre(1:nn)//'MAJOR',.false.,error)
  call sic_delvariable(pre(1:nn)//'MINOR',.false.,error)
  call sic_delvariable(pre(1:nn)//'PA',.false.,error)
  !
  ! Noise
  call sic_delvariable(pre(1:nn)//'SIGMA',.false.,error)
  call sic_delvariable(pre(1:nn)//'NOISE',.false.,error)
  call sic_delvariable(pre(1:nn)//'RMS',.false.,error)
  !
  call sic_delvariable(pre(1:nn)//'PROPER',.false.,error)
  call sic_delvariable(pre(1:nn)//'MU',.false.,error)
  call sic_delvariable(pre(1:nn)//'PARALLAX',.false.,error)

  !
end subroutine wifisyn_cube_sicdel_header
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
