!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Derived types for wifisyn
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module wifisyn_array_types
  type complex_5d_t
     integer :: nc = 0
     integer :: nx = 0
     integer :: ny = 0
     integer :: nu = 0
     integer :: nv = 0
     complex, pointer :: val(:,:,:,:,:) => null()
  end type complex_5d_t
  type complex_4d_t
     integer :: nx = 0
     integer :: ny = 0
     integer :: nu = 0
     integer :: nv = 0
     complex, pointer :: val(:,:,:,:) => null()
  end type complex_4d_t
  type complex_3d_t
     integer :: nc = 0
     integer :: nx = 0
     integer :: ny = 0
     complex, pointer :: val(:,:,:) => null()
  end type complex_3d_t
  type complex_2d_t
     integer :: nx = 0
     integer :: ny = 0
     complex, pointer :: val(:,:) => null()
  end type complex_2d_t
  type complex_1d_t
     integer :: n = 0
     complex, pointer :: val(:) => null()
  end type complex_1d_t
  type dble_2d_t
     integer :: nx = 0
     integer :: ny = 0
     real(kind=8), pointer :: val(:,:) => null()
  end type dble_2d_t
  type dble_1d_t
     integer :: n = 0
     real(kind=8), pointer :: val(:) => null()
  end type dble_1d_t
  type real_2d_t
     integer :: nx = 0
     integer :: ny = 0
     real(kind=4), pointer :: val(:,:) => null()
  end type real_2d_t
  type real_1d_t
     integer :: n = 0
     real(kind=4), pointer :: val(:) => null()
  end type real_1d_t
  type inte_1d_t
     integer :: n = 0
     integer, pointer :: val(:) => null()
  end type inte_1d_t
  type logi_1d_t
     integer :: n = 0
     logical, pointer :: val(:) => null()
  end type logi_1d_t
end module wifisyn_array_types
!
module wifisyn_uv_types
  use image_def
  use wifisyn_parameters
  use wifisyn_array_types
  !
  integer, parameter :: code_uvt  = 1
  integer, parameter :: code_tuv  = 2
  !
  type uv_head_type_t
     integer :: order = code_null  ! UVT or TUV order?
     logical :: wifi = .false.     ! Is it a wide-field uv table?
     logical :: sorted = .false.   ! Is it already sorted?
     integer :: ndata = 0          ! Number of read visibilities
     integer :: nchan = 0          ! Number of read channels
     integer :: range(2) = 0       ! First and last read channels
     real(kind=4) :: xmin  = +1e38 ! [rad]
     real(kind=4) :: xmax  = -1e38 ! [rad]
     real(kind=4) :: ymin  = +1e38 ! [rad]
     real(kind=4) :: ymax  = -1e38 ! [rad]
     real(kind=4) :: umin  = +1e38 ! [  m]
     real(kind=4) :: umax  = -1e38 ! [  m]
     real(kind=4) :: vmin  = +1e38 ! [  m]
     real(kind=4) :: vmax  = -1e38 ! [  m]
     real(kind=4) :: uvmax =   0.0 ! [  m]
     real(kind=4) :: uvmin = +1e38 ! [  m]
  end type uv_head_type_t
  !
  type uv_head_t
     type(gildas)         :: file ! File header
     type(uv_head_type_t) :: type ! Type header
     real, pointer :: u(:)    => null() ! [  m] U
     real, pointer :: v(:)    => null() ! [  m] V
     real, pointer :: x(:)    => null() ! [rad] X pointing offset
     real, pointer :: y(:)    => null() ! [rad] Y pointing offset
     real, pointer :: scan(:) => null() ! [---] Scan
     real, pointer :: date(:) => null() ! [gag] Observing date
     real, pointer :: time(:) => null() ! [sec] Time
     real, pointer :: anti(:) => null() ! [---] First antenna number
     real, pointer :: antj(:) => null() ! [---] Second antenna number
  end type uv_head_t
  !
  type uv_buffer_t
     real, pointer    :: uvp1(:,:)   => null() ! Columns   1 to 7   => dimensions: ndata x 7
     real, pointer    :: uvp2(:,:)   => null() ! Columns n-1 to n   => dimensions: ndata x 2
     real, pointer    :: uvp3(:,:,:) => null() ! Columns   8 to n-2 => dimensions: ndata x 3 x nchan for tuv order
                                               !                    => dimensions: 3 x nchan x ndata for uvt order
  end type uv_buffer_t
  !
  type uv_t
     type(uv_buffer_t) :: buff
     type(uv_head_t)   :: head
     real, pointer     :: data(:,:,:) => null() ! ndata x (Real [Jy], Imaginary [Jy], Weight [???]) x nchan
  end type uv_t
end module wifisyn_uv_types
!
module wifisyn_cube_types
  use image_def
  use wifisyn_array_types
  use wifisyn_buffer_parameters
  !
  integer, parameter :: moper = 4
  integer, parameter :: oper_length = 9
  character(len=oper_length) :: known_oper(moper)
  data known_oper /'AMPLITUDE','PHASE','REAL','IMAGINARY'/
  integer, parameter :: code_ampl = 1
  integer, parameter :: code_phas = 2
  integer, parameter :: code_real = 3
  integer, parameter :: code_imag = 4
  !
  integer, parameter :: mcube = 2
  character(len=oper_length) :: known_cube(mcube)
  data known_cube /'VISICUBE','WIFICUBE'/
  integer, parameter :: code_cube_visi = 1
  integer, parameter :: code_cube_wifi = 2
  !
  integer, parameter :: mdire = 2
  character(len=oper_length) :: known_dire(mcube)
  data known_dire /'FROM','TO'/
  integer, parameter :: code_real2complex = 1
  integer, parameter :: code_complex2real = 2
  !
  integer, parameter :: mscale = 2
  character(len=oper_length) :: known_scale(mscale)
  data known_scale /'LOGSCALE','LINSCALE'/
  logical, parameter :: code_logscale = .true.
  logical, parameter :: code_linscale = .false.
  !
  type dirty_t
     type(gildas) :: image ! 3D
     type(gildas) :: beam  ! 2D or 4D
  end type dirty_t
  type visi_t
     logical            :: uvkind = .true.
     type(complex_3d_t) :: cub  ! 3D complex gridded visibilities
     type(gildas)       :: amp  ! 3D derived amplitude or phase or real or imaginary part
     type(gildas)       :: wei  ! 2D weight
  end type visi_t
  type wifi_t
     logical            :: xykind = .true.
     logical            :: uvkind = .true.
     type(complex_5d_t) :: cub  ! 5D complex gridded visibilities
     type(complex_4d_t) :: rms
     type(complex_4d_t) :: mean
     type(gildas)       :: amp  ! 4D derived amplitude or phase or real or imaginary part
     type(gildas)       :: wei  ! 4D weight
  end type wifi_t
  !
end module wifisyn_cube_types
!
module wifisyn_primitive_types
  use wifisyn_array_types
  ! Axis
  type axis_1d_t
     integer      :: n = 0                  !
     real(kind=8) :: conv(3) = 0d0          !
     real(kind=8), pointer :: inc => null() ! [m or rad]
     real(kind=8), pointer :: val => null() ! [m or rad]
     real(kind=8), pointer :: ref => null() ! [   pixel]
     type(dble_1d_t) :: coord               ! [m or rad]
  end type axis_1d_t
end module wifisyn_primitive_types
!
module wifisyn_gridding_types
  use wifisyn_parameters
  use wifisyn_primitive_types
  use wifisyn_uv_types
  use wifisyn_array_types
  !
  integer, parameter :: code_axis_u = 1 !
  integer, parameter :: code_axis_v = 2 !
  integer, parameter :: code_axis_x = 3 !
  integer, parameter :: code_axis_y = 4 !
  integer, parameter :: code_axis_a = 5 !
  integer, parameter :: code_axis_b = 6 !
  integer, parameter :: code_axis_d = 7 !
  integer, parameter :: code_axis_e = 8 !
  !
  integer, parameter :: code_pillbox     = 1
  integer, parameter :: code_exponential = 2
  integer, parameter :: code_sinc        = 3
  integer, parameter :: code_exp_sinc    = 4
  integer, parameter :: code_spheroidal  = 5
  !
  logical, parameter :: code_shift = .true.
  !
  ! Table dimension
  type tabdim_t
     integer :: data = 0
     integer :: chan = 0
  end type tabdim_t
  ! Freq/Velo windows
  type window_t
     integer :: first = 0 ! First channel
     integer :: last  = 0 ! Last channel
     integer :: nchan = 0 ! Number of channels
     integer :: wchan = 0 ! Weight channel
  end type window_t
  type winlist_t
     integer :: n = 1
     type(window_t) :: cur(1)
  end type winlist_t
  ! Reference channel parameters
  type channel_t
     real(kind=8) :: freq ! [MHz]
     real(kind=8) :: wave ! [  m]
  end type channel_t
  ! Topology parameters
  type minmax_t
     real(kind=8) :: min  = 0d0 ! [-] Minimum
     real(kind=8) :: max  = 0d0 ! [-] Maximum
  end type minmax_t
  type topology_t
     type(minmax_t) :: uv ! [  m] Min and max of uv distance
     type(minmax_t) :: u  ! [  m] Min and max of u or v
     type(minmax_t) :: x  ! [rad] Min and max of x or y
  end type topology_t
  ! Scale parameters
  type scale_pairs_t
     real(kind=8) :: sky = 0.0 ! [rad]
     real(kind=8) :: uvp = 0.0 ! [  m]
  end type scale_pairs_t
  type scale_t
     type(scale_pairs_t) :: synt  ! Synthesized beam
     type(scale_pairs_t) :: prim  ! Primary beam
     type(scale_pairs_t) :: fwhm  ! Primary FWHM
     type(scale_pairs_t) :: alias ! Anti-aliasing scale
     type(scale_pairs_t) :: field ! Total field of view
     type(scale_pairs_t) :: image ! Final image
  end type scale_t
  ! Axis parameters
  type axis_pair_t
     integer         :: kind = code_null ! [---] (e.g. u,v,x,y,a,b)
     type(axis_1d_t) :: sky ! Axis coordinates in sky plane
     type(axis_1d_t) :: uvp ! Axis coordinates in uv  plane
  end type axis_pair_t
  type axis_t
     integer :: sign = +1
     integer :: dup_per_duf
     type(axis_pair_t) :: u ! Gridding along the U axis
     type(axis_pair_t) :: x ! Gridding along the X axis
     type(axis_pair_t) :: a ! Dirty image
     type(axis_pair_t) :: d ! Dirty beam position
  end type axis_t
  ! Convolution kernel
  type kernel_1d_t
     integer      :: kind = code_null   ! [---] (e.g. u,v,x,y)
     integer      :: type = code_null   ! [---] (e.g. spheroidal)
     character(len=12) :: name = "null" ! [---] (e.g. "Spheroidal")
     integer      :: nparm = 0          ! [---] Number of kernel param
     real(kind=4) :: parm(10) = 0.0     ! [pix] Kernel param (zero to avoid problems...)
     real(kind=4) :: zero = 0.0         ! [pix]
     real(kind=4) :: fac  = 0.0         ! [---] lens (multiplicative factor)
     real(kind=4) :: sup  = 0.0         ! [physical unit]
     type(real_1d_t) :: fn              ! [---] Function values
     type(real_1d_t) :: ft              ! [---] Fourier transform
  end type kernel_1d_t
  type kernel_t
     type(kernel_1d_t) :: u
     type(kernel_1d_t) :: x
  end type kernel_t
  ! Projection/phase center
  type shift_t
     logical :: shift = .false.          ! [---] Shift and/or Rotate grid?
     real(kind=8) :: a0 = 0d0            ! [rad] Projection center (lambda)
     real(kind=8) :: d0 = 0d0            ! [rad] Projection center (beta)
     real(kind=8) :: pang = 0d0          ! [rad] Grid position angle
     real(kind=8) :: xoff = 0d0          ! [rad] xoff from old to new center
     real(kind=8) :: yoff = 0d0          ! [rad] yoff from old to new center
     real(kind=8) :: rang = 0d0          ! [rad] Rotation angle from old to new pang
     real(kind=8) :: xy(2) = 0d0         ! [---] Phase rotation
     real(kind=8) :: cs(2) = (/1d0,0d0/) ! [---] cos(rang), sin(range)
     type(logi_1d_t) :: vsign            ! [---] Was sign of v not flipped?
  end type shift_t
  ! Sorting
  type sort_t
     type(inte_1d_t) :: idx
     type(inte_1d_t) :: iu
     type(inte_1d_t) :: iv
     type(inte_1d_t) :: ix
     type(inte_1d_t) :: iy
  end type sort_t
  ! Tapering
  type taper_t
     logical :: taper = .false.  ! Tapering?
     real(kind=4) :: expon = 1.0 ! [---] Gaussian = 1
     real(kind=4) :: major = 0.0 ! [m  ]
     real(kind=4) :: minor = 0.0 ! [m  ]
     real(kind=4) :: angle = 0.0 ! [rad]
     real(kind=4) :: cang  = 0.0 ! [---]
     real(kind=4) :: sang  = 0.0 ! [---]
  end type taper_t
  ! Robust weighting
  type robust_t
     logical :: robust = .false. ! Robust weighting?
     type(axis_1d_t) :: u
     type(axis_1d_t) :: v
     real(kind=4) :: thres ! [???]
  end type robust_t
  ! Weighting
  type weight_t
     integer :: chan = 0     ! Weighting channel
     integer :: nflagged = 0 ! Number of flagged visibilities
     type(real_1d_t) :: nat  ! Natural weights
     type(real_1d_t) :: mod  ! Modified (tapered and/or robust weighted) weights
  end type weight_t
  ! Noise
  type noise_t
     real(kind=4) :: nat = 0 ! [Jy/beam] Natural noise
     real(kind=4) :: mod = 0 ! [Jy/beam] Modified (tapered and/or robust weighted) noise
  end type noise_t
  ! Resolution
  type beam_t
     real(kind=4) :: major = 0.0 ! [rad]
     real(kind=4) :: minor = 0.0 ! [rad]
     real(kind=4) :: angle = 0.0 ! [rad]
  end type beam_t
  ! Buffer characteristics
  type buffer_t
     real(kind=4) :: ram = 4.294967296E+09 ! RAM size in byte
     real(kind=4) :: percent = 20
     real(kind=4) :: maxsize = 0
     real(kind=4) :: truesize = 0
     real(kind=4) :: chansize = 0
     integer      :: ndata = 0     ! Associated number of data
     integer      :: nchan = 0     ! Associated number of channels
     integer      :: nblock = 0    ! Associated number of blocks
  end type buffer_t
  type sicvar_t
     logical :: defined = .false.
     logical :: readonly = .true.
     character(len=varname_length) :: name = 'SETUP'
  end type sicvar_t
  ! Global gridding type
  type map_t
     type(gildas)     :: huv
     type(tabdim_t)   :: n
     type(channel_t)  :: rchan
     type(winlist_t)  :: win
     type(topology_t) :: topo(2)
     type(scale_t)    :: scale(2)
     type(axis_t)     :: axes(2)
     type(kernel_t)   :: kern(2)
     type(shift_t)    :: shift
     type(sort_t)     :: sort
     type(taper_t)    :: taper
     type(robust_t)   :: robust
     type(weight_t)   :: weight
     type(noise_t)    :: noise
     type(beam_t)     :: beam
     type(buffer_t)   :: buffer
     type(sicvar_t)   :: sic
  end type map_t
  ! User interface
  type sicmap_t
     ! Channels
     integer :: chan(2) = 0          ! First and last gridded channels
     integer :: wchan = 0            ! Weight channel
     ! Projection/phase center
     logical :: shift = .false.     ! [ ] Shift and/or Rotate grid?
     character(len=16) :: ra  = ""  ! [  HH:MM:SS]
     character(len=16) :: dec = ""  ! [+DDD:MM:SS]
!     real(kind=8) :: lii = 0d0     ! [deg]
!     real(kind=8) :: bii = 0d0     ! [deg]
     real(kind=8) :: pang = 0d0     ! [deg] Grid position angle
     ! Grid
     integer      :: size(2) = 0    ! [pix] Image size
     real(kind=4) :: cell(2) = 0.0  ! ["  ] Image cell
     ! Convolution
     integer      :: ctype = 0        ! Convolution kernel type
     real(kind=4) :: support(2) = 0.0 ! [m] Support size
     ! Weighting and tapering
     character(len=8) :: mode = 'NATURAL' ! [] Weighting mode (NATURAL or ROBUST)
     real(kind=4) :: robust_size(2) = 0.0 ! Robust
     real(kind=4) :: robust_thre = 0.0    ! Robust
     real(kind=4) :: taper_sigma(2) = 0.0 ! [m,m]
     real(kind=4) :: taper_pang = 0.0     ! [deg]
     real(kind=4) :: taper_expo = 0.0     ! [---]
     ! Field
     real(kind=4) :: field_size(2) = 0.0   ! ["]
     real(kind=4) :: field_center(2) = 0.0 ! ["]
  end type sicmap_t
  !
end module wifisyn_gridding_types
!
module wifisyn_load_types
  use gildas_def
  use gio_params
  use wifisyn_parameters
  use wifisyn_buffer_parameters
  type load_t
     logical :: logscale = .false.  ! Logarithmic or linear scale
     logical :: amplitude = .false. ! (Amplitude + Phase) or (Real + Imaginary)
     logical :: normalize = .false. ! Normalize amplitude to 1?
     integer :: buffer = code_null  ! Buffer ID
     character(len=buffer_type_length) :: type = '' ! Buffer extension
     character(len=buffer_kind_length) :: kind = '' ! Buffer name
     character(len=varname_length)     :: name = '' ! Variable name
  end type load_t
end module wifisyn_load_types
!
module wifisyn_readwrite_types
  use gildas_def
  use gio_params
  use wifisyn_parameters
  use wifisyn_buffer_parameters
  !
  type readwrite_t
     character(len=filename_length)    :: name = '' ! File name
     character(len=buffer_type_length) :: type = '' ! File extension
     character(len=buffer_kind_length) :: kind = '' ! Buffer name
     integer :: buffer = code_null ! Buffer ID
     integer :: first = code_null  ! First channel
     integer :: last = code_null   ! Last  channel
  end type readwrite_t
end module wifisyn_readwrite_types
!
module wifisyn_transform_types
  use gio_params
  use wifisyn_parameters
  use wifisyn_array_types
  !
  integer, parameter :: mspace = 2
  character(len=2) :: known_space(mspace)
  data known_space /'UV','XY'/
  integer, parameter :: code_space_uv = 1
  integer, parameter :: code_space_xy = 2
  !
  integer, parameter :: unit_length = 6
  integer, parameter :: munit = 2
  character(len=unit_length) :: known_unit(munit)
  data known_unit /'METER','SECOND'/
  integer, parameter :: code_unit_meter  = 1
  integer, parameter :: code_unit_second = 2
  !
  integer, parameter :: code_type_real    = 0 ! To be compatible with fourt
  integer, parameter :: code_type_complex = 1 ! To be compatible with fourt
  integer, parameter :: code_direct  = +1 ! To be compatible with fourt
  integer, parameter :: code_inverse = -1 ! To be compatible with fourt
  !
  type action_t
     integer :: cube  = code_null
     integer :: dire  = code_null
     integer :: space = code_null
  end type action_t
  !
  type user_shift_t
     integer :: unit  = code_null
     real(kind=8) :: dx = 0d0 ! X component of shift vector
     real(kind=8) :: dy = 0d0 ! Y component of shift vector
  end type user_shift_t
  !
  type fft_t
     logical,            pointer :: datakind
     type(complex_5d_t), pointer :: wifi
     type(complex_3d_t), pointer :: visi
     type(complex_2d_t) :: plane
     type(complex_1d_t) :: work
  end type fft_t
  !
  type myshift_t
     logical :: phase = .false.   ! [-----] Shift phase or grid?
     integer :: dire  = code_null !
     real(kind=8) :: freq = 0d0   !
     real(kind=8) :: dx = 0d0     ! [rad|m] X component of shift vector
     real(kind=8) :: dy = 0d0     ! [rad|m] Y component of shift vector
     integer :: idx = 0           ! [pixel] X component of shift vector
     integer :: idy = 0           ! [pixel] Y component of shift vector
     type(dble_1d_t), pointer :: ucoord => null() ! [m|rad] X conjugate coordinate array
     type(dble_1d_t), pointer :: vcoord => null() ! [m|rad] Y conjugate coordinate array
  end type myshift_t
  !
end module wifisyn_transform_types
!
module wifisyn_sort_types
  !---------------------------------------------------------------------
  !
  !---------------------------------------------------------------------
  type inte_pointer_t
     ! Shouldn't we use a array_inte_t here? *** JP
     integer(4), pointer :: type(:)
  end type inte_pointer_t
  !
  type sorting_t
     type(inte_pointer_t) :: ipointer
  end type sorting_t
  !
  integer, parameter :: m_inte_key = 4
  integer, parameter :: m_tot_key = m_inte_key
  !
  character(len=6) :: known_keys(m_tot_key)
  data known_keys /'U', 'V', 'X', 'Y'/
  !
end module wifisyn_sort_types
!
module wifisyn_uvbeam_types
  type source_t
     ! This has something to do with a clean component...
     real(kind=8) :: xoff = 0.0 ! [rad]
     real(kind=8) :: yoff = 0.0 ! [rad]
     real(kind=4) :: flux = 0.0 ! [Jy]
  end type source_t
end module wifisyn_uvbeam_types
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

