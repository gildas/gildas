!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_uvbeam_command(line,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_uvbeam_command
  !----------------------------------------------------------------------
  ! @ private
  ! Support routine for command
  !     UVBEAM
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  logical,          intent(inout) :: error
  !
  character(len=*), parameter :: rname='UVBEAM/COMMAND'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call wifisyn_uvbeam_inter(error)
  if (error) return
  !
end subroutine wifisyn_uvbeam_command
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_uvbeam_inter(error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_uvbeam_inter
  use wifisyn_uv_buffer
  use wifisyn_cube_buffer
  use wifisyn_sic_buffer
  use wifisyn_transform_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  logical, intent(inout) :: error
  !
  type(uv_t) :: uvtmp
  character(len=*), parameter :: rname='UVBEAM/INTER'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call wifisyn_setup_main(usermap,uvr,progmap,uvtmp,error)
  if (error) return
  call wifisyn_uvbeam_main(progmap%win%cur(1),uvr,uvtmp,progmap,wifi,dirty,error)
  if (error) return
  call wifisyn_free_uv(uvtmp,error)
  if (error) return
  !
end subroutine wifisyn_uvbeam_inter
!
subroutine wifisyn_uvbeam_main(win,uvin,uvout,map,wifi,dirty,error)
  use gbl_message
  use gkernel_interfaces
  use gkernel_types
  use wifisyn_interfaces, except_this=>wifisyn_uvbeam_main
  use wifisyn_buffer_parameters
  use wifisyn_gridding_types
  use wifisyn_uv_types
  use wifisyn_cube_types
  use wifisyn_uvbeam_types
  use wifisyn_transform_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(window_t),      intent(in)    :: win
  type(uv_t),          intent(in)    :: uvin
  type(uv_t),          intent(inout) :: uvout
  type(map_t), target, intent(inout) :: map
  type(wifi_t),        intent(inout) :: wifi
  type(dirty_t),       intent(inout) :: dirty
  logical,             intent(inout) :: error
  !
  character(len=filename_length) :: name
  type(time_t) :: time
  type(visi_t) :: visi
  type(source_t) :: sou
  integer :: ndata,nchan
  integer :: ix,nx
  integer :: iy,ny
  type(dble_1d_t), pointer :: xcoord
  type(dble_1d_t), pointer :: ycoord
  character(len=*), parameter :: rname='UVBEAM/MAIN'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ndata = map%n%data
  nchan = map%n%chan
  ! Get header ready
  call wifisyn_uvgrid_sort_and_weight(uvin,uvout,map,error)
  if (error) return
  ! Get data ready
  call wifisyn_reallocate_uv_data(uvout,ndata,nchan,code_uvt,error)
  if (error) return
  call wifisyn_uvgrid_shift_sort_transpose_data(win,map%shift,map%sort,uvin,uvout,error)
  if (error) return
  ! Get buffer ready
  call wifisyn_reallocate_wifi(map,wifi,error)
  if (error) return
  call wifisyn_reallocate_dirty_beam(map,dirty,error)
  if (error) return
  !
  nx = map%axes(1)%d%sky%n
  ny = map%axes(2)%d%sky%n
  xcoord => map%axes(1)%d%sky%coord
  ycoord => map%axes(2)%d%sky%coord
  !
name = 'test'
  !
  sou%flux = 1.0
  dirty%beam%r4d = 0.0
  call wifisyn_cube_open(name,code_buffer_beam,dirty%beam,error)
  if (error) return
  call gtime_init(time,nx*ny,error)
  if (error) return
  do iy=1,ny
     sou%yoff = ycoord%val(iy)
     do ix=1,nx
        call gtime_current(time)
        sou%xoff = xcoord%val(ix)
        call wifisyn_uvbeam_fill_uv(map%rchan,map%scale(1)%fwhm,sou,uvout,error)
        if (error) return
        call wifisyn_uvgrid_convolve(win,map,uvout,wifi,error)
        if (error) return
        call wifisyn_fft_wifi(code_direct,code_space_xy,win,wifi,error)
        if (error) return
        call wifisyn_wifi2visi_main(map,wifi,visi,error)
        if (error) return
        call wifisyn_fft_visi(code_inverse,win,visi,error)
        if (error) return
        dirty%beam%r4d(:,:,ix,iy) = real(visi%cub%val(:,:,1))
        call wifisyn_cube_write_r4d(ix,iy,dirty%beam,error)
        if (error) return
     enddo ! ix
  enddo ! iy
  call wifisyn_cube_close(dirty%beam,error)
  if (error) return
  !
  call wifisyn_cube_minmax_r4d(code_linscale,dirty%beam,error)
  if (error) return
  if (dirty%beam%gil%rmax.ne.0.0) then
     dirty%beam%r4d = dirty%beam%r4d/dirty%beam%gil%rmax
     dirty%beam%gil%rmin = dirty%beam%gil%rmin/dirty%beam%gil%rmax
     dirty%beam%gil%rmax = 1.0
  endif
  call wifisyn_cube_sicdef(code_readonly,'xyuv',dirty%beam,error)
  if (error) return
  !
end subroutine wifisyn_uvbeam_main
!
subroutine wifisyn_reallocate_dirty_beam(map,dirty,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_reallocate_dirty_beam
  use wifisyn_gridding_types
  use wifisyn_cube_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(map_t),   intent(in)    :: map
  type(dirty_t), intent(inout) :: dirty
  logical,       intent(inout) :: error
  !
  character(len=*), parameter :: rname='REALLOCATE/DIRTY/BEAM'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call wifisyn_wifi_dirty_header(map,dirty,error)
  if (error) return
  call wifisyn_cube_reallocate_r4d("wifi dirty%beam",dirty%beam,error)
  if (error) return
  !
end subroutine wifisyn_reallocate_dirty_beam
!
subroutine wifisyn_wifi_dirty_header(map,dirty,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_wifi_dirty_header
  use wifisyn_gridding_types
  use wifisyn_cube_types
  !----------------------------------------------------------------------
  ! @ private
  ! Should be merged with wifi_header *** JP
  !----------------------------------------------------------------------
  type(map_t),   intent(in)    :: map
  type(dirty_t), intent(inout) :: dirty
  logical,       intent(inout) :: error
  !
  character(len=*), parameter :: rname='WIFI/DIRTY/HEADER'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call wifisyn_common_header(map,dirty%beam,error)
  if (error) return
  dirty%beam%gil%xaxi = 0
  dirty%beam%gil%yaxi = 0
  ! 1st axis
  dirty%beam%char%code(1) = 'RA'
  dirty%beam%gil%dim(1) = map%axes(1)%a%sky%n
  dirty%beam%gil%ref(1) = map%axes(1)%a%sky%ref
  dirty%beam%gil%val(1) = map%axes(1)%a%sky%val
  dirty%beam%gil%inc(1) = map%axes(1)%a%sky%inc
  ! 2nd axis
  dirty%beam%char%code(2) = 'DEC'
  dirty%beam%gil%dim(2) = map%axes(2)%a%sky%n
  dirty%beam%gil%ref(2) = map%axes(2)%a%sky%ref
  dirty%beam%gil%val(2) = map%axes(2)%a%sky%val
  dirty%beam%gil%inc(2) = map%axes(2)%a%sky%inc
  ! 3rd axis
  dirty%beam%char%code(3) = 'RA'
  dirty%beam%gil%dim(3) = map%axes(1)%d%sky%n
  dirty%beam%gil%ref(3) = map%axes(1)%d%sky%ref
  dirty%beam%gil%val(3) = map%axes(1)%d%sky%val
  dirty%beam%gil%inc(3) = map%axes(1)%d%sky%inc
  ! 4th axis
  dirty%beam%char%code(4) = 'DEC'
  dirty%beam%gil%dim(4) = map%axes(2)%d%sky%n
  dirty%beam%gil%ref(4) = map%axes(2)%d%sky%ref
  dirty%beam%gil%val(4) = map%axes(2)%d%sky%val
  dirty%beam%gil%inc(4) = map%axes(2)%d%sky%inc
  !
end subroutine wifisyn_wifi_dirty_header
!
subroutine wifisyn_uvbeam_fill_uv(rchan,fwhm,sou,uv,error)
  use gbl_message
  use phys_const
  use gkernel_interfaces
  use gkernel_types
  use wifisyn_interfaces, except_this=>wifisyn_uvbeam_fill_uv
  use wifisyn_uv_types
  use wifisyn_gridding_types
  use wifisyn_uvbeam_types
  !----------------------------------------------------------------------
  ! @ private
  ! Note: This looks like the computation of clean component visibilities...
  !----------------------------------------------------------------------
  type(channel_t),     intent(in)    :: rchan
  type(scale_pairs_t), intent(in)    :: fwhm
  type(source_t),      intent(in)    :: sou
  type(uv_t),          intent(inout) :: uv
  logical,             intent(inout) :: error
  !
  type(time_t) :: time
  integer :: idata,ndata
  real(kind=8) :: pha,tophase
  real(kind=8) :: arg,invsigma2,primpower
  character(len=*), parameter :: rname='UVBEAM/FILL/UV'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! Sanity check: Is the UV header set? *** JP
  !
  tophase = f_to_k*rchan%freq
  invsigma2 = ( (2d0*sqrt(log(2d0)))/fwhm%sky )**2
  ndata = uv%head%type%ndata
  call gtime_init(time,ndata,error)
  if (error) return
  uv%data = 0.0
  do idata=1,ndata
     call gtime_current(time)
    ! 1. The primary beam
     arg = ((sou%xoff-uv%head%x(idata))**2 + (sou%yoff-uv%head%y(idata))**2) * invsigma2
     if (arg.lt.14d0) then
        primpower = exp(-arg) ! >= 10^-6
        ! 2. The phase term exp(+i2pi(u*x+v*y))
        pha = tophase*(uv%head%u(idata)*sou%xoff+uv%head%v(idata)*sou%yoff)
        ! 3. The visibility of a source of flux 1.0.
        uv%data(1,1,idata) = primpower*cos(pha) ! Real part
        uv%data(2,1,idata) = primpower*sin(pha) ! Imag part
     endif
  enddo ! idata
  !
end subroutine wifisyn_uvbeam_fill_uv
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
