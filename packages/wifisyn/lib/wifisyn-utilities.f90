!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
function inconsistent_size(rname,array1,n1,array2,n2,error)
  use gbl_message
  use wifisyn_interfaces, only: wifisyn_message
  !-------------------------------------------------------------------
  ! @ public
  ! Check
  !-------------------------------------------------------------------
  logical                         :: inconsistent_size
  character(len=*), intent(in)    :: rname
  character(len=*), intent(in)    :: array1
  integer,          intent(in)    :: n1
  character(len=*), intent(in)    :: array2
  integer,          intent(in)    :: n2
  logical,          intent(inout) :: error
  !
  character(len=160) :: mess
  !
  if (n1.ne.n2) then
     write(mess,'(a,a,a,i0,a,a,a,i0,a)') 'Inconsistent size between ',array1,' (',n1,') and ',array2,' (',n2,')'
     call wifisyn_message(seve%e,rname,mess)
     error = .true.
  endif
  inconsistent_size = error
end function inconsistent_size
!
function no_data(rname,htab,error)
  use gbl_message
  use wifisyn_interfaces, only: wifisyn_message
  use wifisyn_uv_types
  !-------------------------------------------------------------------
  ! @ public
  ! Check
  !-------------------------------------------------------------------
  logical                         :: no_data
  character(len=*), intent(in)    :: rname
  type(uv_head_t),  intent(in)    :: htab
  logical,          intent(inout) :: error
  !
  if (htab%type%ndata.le.0) then
     call wifisyn_message(seve%e,rname,'No data (Negative number of visibilities!)')
     error = .true.
  endif
  if (htab%type%nchan.le.0) then
     call wifisyn_message(seve%e,rname,'No data (Negative number of channels!)')
     error = .true.
  endif
  no_data = error
end function no_data
!
function oddceiling(dval)
  !----------------------------------------------------------------------
  ! @ public
  !----------------------------------------------------------------------
  integer                  :: oddceiling
  real(kind=8), intent(in) :: dval
  !
  integer :: ival
  !
  ival = ceiling(dval)
  if (mod(ival,2).eq.0) then
     ! ival is even
     oddceiling = ival+1
  else
     ! ival is odd
     oddceiling = ival
  endif
  !
end function oddceiling
!
function evenceiling(dval)
  !----------------------------------------------------------------------
  ! @ public
  !----------------------------------------------------------------------
  integer                  :: evenceiling
  real(kind=8), intent(in) :: dval
  !
  integer :: ival
  !
  ival = ceiling(dval)
  if (mod(ival,2).eq.0) then
     ! ival is even
     evenceiling = ival
  else
     ! ival is odd
     evenceiling = ival+1
  endif
  !
end function evenceiling
!
function poweroftwo(dval)
  !----------------------------------------------------------------------
  ! @ public
  !----------------------------------------------------------------------
  integer                  :: poweroftwo
  real(kind=8), intent(in) :: dval
  !
  poweroftwo = 2**ceiling(log(abs(dval))/log(2d0))
  !
end function poweroftwo
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
