!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_setup_command(line,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_setup_command
  use wifisyn_uv_buffer
  use wifisyn_sic_buffer
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  logical,          intent(inout) :: error
  !
  type(uv_t) :: uvtmp
  character(len=*), parameter :: rname='SETUP/COMMAND'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call wifisyn_setup_main(usermap,uvr,progmap,uvtmp,error)
  if (error) return
  call wifisyn_free_uv(uvtmp,error)
  if (error) return
  !
end subroutine wifisyn_setup_command
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_setup_main(user,uvin,map,uvout,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_setup_main
  use wifisyn_uv_types
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(sicmap_t), intent(in)    :: user
  type(uv_t),     intent(in)    :: uvin
  type(map_t),    intent(inout) :: map
  type(uv_t),     intent(inout) :: uvout
  logical,        intent(inout) :: error
  !
  character(len=*), parameter :: rname='SETUP/MAIN'
  !
  call wifisyn_setup_uvtmp(uvin,uvout,error)
  if (error) return
  call wifisyn_setup_map_and_shift_header(user,map,uvout%head,error)
  if (error) return
  !
end subroutine wifisyn_setup_main
!
subroutine wifisyn_setup_uvtmp(uvin,uvout,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_setup_uvtmp
  use wifisyn_uv_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(uv_t), intent(in)    :: uvin
  type(uv_t), intent(inout) :: uvout
  logical,    intent(inout) :: error
  !
  character(len=*), parameter :: rname='SETUP/UVTMP'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call wifisyn_reallocate_uv_header(uvout,uvin%head%type%ndata,error)
  if (error) return
  call wifisyn_copy_uv_header(uvin,uvout,error)
  if (error) return
  !
end subroutine wifisyn_setup_uvtmp
!
subroutine wifisyn_setup_map_and_shift_header(user,map,huv,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_setup_map_and_shift_header
  use wifisyn_uv_types
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(sicmap_t),  intent(in)    :: user
  type(map_t),     intent(inout) :: map
  type(uv_head_t), intent(inout) :: huv
  logical,         intent(inout) :: error
  !
  integer :: iaxis
  character(len=*), parameter :: rname='SETUP/MAP+SHIFT/HEAD'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call wifisyn_gridding_nullify_map(map)
  call wifisyn_gridding_set_huv(huv,map,error)
  if (error) return
  call wifisyn_gridding_set_tabdim(huv,map%n,error)
  if (error) return
  call wifisyn_gridding_set_rchan(huv,map%rchan,error)
  if (error) return
  call wifisyn_gridding_set_winlist(user,map%n%chan,map%win,error)
  if (error) return
  call wifisyn_gridding_set_shift(user,huv%file,map%rchan,map%shift,error)
  if (error) return
  call wifisyn_shift_uv_head(map%shift,huv,error)
  if (error) return
  call wifisyn_gridding_set_topology(huv,map%topo,error)
  if (error) return
  do iaxis=1,2
     call wifisyn_gridding_set_1d_scale(huv%file,map%rchan,map%topo(iaxis),map%scale(iaxis),error)
     if (error) return
     call wifisyn_gridding_set_1d_axis(map%rchan,map%scale(iaxis),map%axes(iaxis),error)
     if (error) return
     call wifisyn_gridding_set_1d_kernel(user,map%axes(iaxis)%u,map%kern(iaxis)%u,error)
     if (error) return
     call wifisyn_gridding_set_1d_kernel(user,map%axes(iaxis)%x,map%kern(iaxis)%x,error)
     if (error) return
  enddo ! iaxis
  call wifisyn_gridding_set_taper(user,map%taper,error)
  if (error) return
  call wifisyn_gridding_set_buffer(map,error)
  if (error) return
  call wifisyn_gridding_print(map,error)
  if (error) return
  call wifisyn_gridding_sicdef_prog(code_request_prog,error)
  if (error) return
  !
end subroutine wifisyn_setup_map_and_shift_header
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
