!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Routines to manage the WIFISYN package
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_pack_set(pack)
  use gpack_def
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_pack_set
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(gpack_info_t), intent(out) :: pack
  !
  pack%name='wifisyn'
  pack%ext='.wifi'
  pack%depend(1:1) = (/ locwrd(greg_pack_set) /)
  pack%init=locwrd(wifisyn_pack_init)
  pack%on_exit=locwrd(wifisyn_pack_on_exit)
  pack%authors="J.Pety"
  !
end subroutine wifisyn_pack_set
!
subroutine wifisyn_pack_init(gpack_id,error)
  use sic_def
  use wifisyn_interfaces, except_this=>wifisyn_pack_init
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  integer :: gpack_id
  logical :: error
  !
  ! Init WIFISYN package
  error = .false.
  call wifisyn_init(error)
  if (error) return
  !
  ! Local language
  call wifisyn_load
  !
  ! One time initialization
  call wifisyn_message_set_id(gpack_id)  ! Set library id
  !
  ! Language priorities
  call exec_program('SIC'//backslash//'SIC PRIORITY 1 WIFISYN')
  !
  ! Specific initializations
  !
end subroutine wifisyn_pack_init
!
subroutine wifisyn_pack_on_exit(error)
  use wifisyn_interfaces, except_this=>wifisyn_pack_on_exit
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  logical :: error
  !
  call wifisyn_exit(error)
  if (error) return
  !
end subroutine wifisyn_pack_on_exit
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
