!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_apodize_command(line,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_apodize_command
  use wifisyn_transform_types
  !----------------------------------------------------------------------
  ! Support routine for command
  !     APODIZE WIFI|VISI
  !       1 /DIRECT
  !       2 /INVERSE
  !       3 /UV
  !       4 /XY
  ! @ private
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  logical,          intent(inout) :: error
  !
  type(action_t) :: do
  character(len=*), parameter :: rname='APODIZE/COMMAND'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call wifisyn_parse_transform(line,do,error)
  if (error) return
  call wifisyn_apodize_main(do,error)
  if (error) return
  !
end subroutine wifisyn_apodize_command
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_apodize_main(do,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_apodize_main
  use wifisyn_transform_types
  use wifisyn_sic_buffer
  use wifisyn_cube_buffer
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(action_t), intent(in)    :: do
  logical,        intent(inout) :: error
  !
  character(len=*), parameter :: rname='APODIZE/MAIN'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (do%cube.eq.code_cube_visi) then
     call wifisyn_message(seve%e,rname,'Not yet implemented')
     error = .true.
     return
  else if (do%cube.eq.code_cube_wifi) then
     if (do%space.eq.code_space_uv) then
        call wifisyn_apodize_wifi_uv(do%dire,progmap,wifi,error)
        if (error) return
     else if (do%space.eq.code_space_xy) then
        call wifisyn_apodize_wifi_xy(do%dire,progmap,wifi,error)
        if (error) return
     else
        call wifisyn_message(seve%e,rname,'Unknown plane')
        error = .true.
        return
     endif
  else
     call wifisyn_message(seve%e,rname,'Unknown cube type')
     error = .true.
     return
  endif
  !
end subroutine wifisyn_apodize_main
!
subroutine wifisyn_apodize_wifi_uv(idir,map,wifi,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_apodize_wifi_uv
  use wifisyn_transform_types
  use wifisyn_cube_types
  use wifisyn_gridding_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer,             intent(in)    :: idir
  type(map_t), target, intent(in)    :: map
  type(wifi_t),        intent(inout) :: wifi
  logical,             intent(inout) :: error
  !
  integer :: ix,iy,iu,iv
  real(kind=4) :: vkernftval,uvkernftval
  type(kernel_1d_t), pointer :: ukern,vkern
  character(len=*), parameter :: rname='APODIZE/WIFI/UV'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (wifi%uvkind) then
     call wifisyn_message(seve%e,rname,'WIFICUB UV space must be of type SKY => Use first FFT /INV')
     error = .true.
     return
  endif
  ukern => map%kern(1)%u
  vkern => map%kern(2)%u
  if (idir.eq.code_direct) then
     do iv=1,wifi%cub%nv
        vkernftval = vkern%ft%val(iv)
        do iu=1,wifi%cub%nu
           uvkernftval = vkernftval*ukern%ft%val(iu)
           do iy=1,wifi%cub%ny
              do ix=1,wifi%cub%nx
                 wifi%cub%val(:,ix,iy,iu,iv) = wifi%cub%val(:,ix,iy,iu,iv)*uvkernftval
              enddo ! ix
           enddo ! iy
        enddo ! iu
     enddo ! iv
  else if (idir.eq.code_inverse) then
     do iv=1,wifi%cub%nv
        vkernftval = vkern%ft%val(iv)
        do iu=1,wifi%cub%nu
           uvkernftval = vkernftval*ukern%ft%val(iu)
           do iy=1,wifi%cub%ny
              do ix=1,wifi%cub%nx
                 wifi%cub%val(:,ix,iy,iu,iv) = wifi%cub%val(:,ix,iy,iu,iv)/uvkernftval
              enddo ! ix
           enddo ! iy
        enddo ! iu
     enddo ! iv
  else
     call wifisyn_message(seve%e,rname,'Unknown direction')
     error = .true.
     return
  endif
  !
end subroutine wifisyn_apodize_wifi_uv
!
subroutine wifisyn_apodize_wifi_xy(idir,map,wifi,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_apodize_wifi_xy
  use wifisyn_transform_types
  use wifisyn_cube_types
  use wifisyn_gridding_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  integer,             intent(in)    :: idir
  type(map_t), target, intent(in)    :: map
  type(wifi_t),        intent(inout) :: wifi
  logical,             intent(inout) :: error
  !
  integer :: ix,iy,iu,iv
  type(kernel_1d_t), pointer :: xkern,ykern
  character(len=*), parameter :: rname='APODIZE/WIFI/XY'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (wifi%xykind) then
     call wifisyn_message(seve%e,rname,'WIFICUB XY space must be of type UVP => Use first FFT /DIR')
     error = .true.
     return
  endif
  xkern => map%kern(1)%x
  ykern => map%kern(2)%x
  if (idir.eq.code_direct) then
     do iv=1,wifi%cub%nv
        do iu=1,wifi%cub%nu
           do iy=1,wifi%cub%ny
              do ix=1,wifi%cub%nx
                 wifi%cub%val(:,ix,iy,iu,iv) = wifi%cub%val(:,ix,iy,iu,iv)*(xkern%ft%val(ix)*ykern%ft%val(iy))
              enddo ! ix
           enddo ! iy
        enddo ! iu
     enddo ! iv
  else if (idir.eq.code_inverse) then
     do iv=1,wifi%cub%nv
        do iu=1,wifi%cub%nu
           do iy=1,wifi%cub%ny
              do ix=1,wifi%cub%nx
                 wifi%cub%val(:,ix,iy,iu,iv) = wifi%cub%val(:,ix,iy,iu,iv)/(xkern%ft%val(ix)*ykern%ft%val(iy))
              enddo ! ix
           enddo ! iy
        enddo ! iu
     enddo ! iv
  else
     call wifisyn_message(seve%e,rname,'Unknown direction')
     error = .true.
     return
  endif
  !
end subroutine wifisyn_apodize_wifi_xy
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

