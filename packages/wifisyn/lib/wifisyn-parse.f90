!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_parse_unit_kind(line,iopt,iarg,iunit,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_parse_unit_kind
  use wifisyn_transform_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  integer,          intent(in)    :: iopt
  integer,          intent(in)    :: iarg
  integer,          intent(out)   :: iunit
  logical,          intent(inout) :: error
  !
  integer :: larg
  character(len=unit_length) :: arg,unit
  character(len=*), parameter :: rname='PARSE/UNIT/KIND'
  !
  call sic_ke(line,iopt,iarg,arg,larg,.true.,error)
  if (error) return
  call sic_ambigs(rname,arg,unit,iunit,known_unit,munit,error)
  if (error) return
  !
end subroutine wifisyn_parse_unit_kind
!
subroutine wifisyn_parse_cube_kind(line,icube,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_parse_cube_kind
  use wifisyn_cube_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  integer,          intent(out)   :: icube
  logical,          intent(inout) :: error
  !
  integer :: larg
  character(len=oper_length) :: arg,cube
  character(len=*), parameter :: rname='PARSE/CUBE/KIND'
  !
  call sic_ke(line,0,1,arg,larg,.true.,error)
  if (error) return
  call sic_ambigs(rname,arg,cube,icube,known_cube,mcube,error)
  if (error) return
  !
end subroutine wifisyn_parse_cube_kind
!
subroutine wifisyn_parse_dire_kind(line,idire,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_parse_dire_kind
  use wifisyn_cube_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  integer,          intent(out)   :: idire
  logical,          intent(inout) :: error
  !
  integer :: larg
  character(len=oper_length) :: arg,dire
  character(len=*), parameter :: rname='PARSE/CUBE/DIRE'
  !
  call sic_ke(line,0,2,arg,larg,.true.,error)
  if (error) return
  call sic_ambigs(rname,arg,dire,idire,known_dire,mdire,error)
  if (error) return
  !
end subroutine wifisyn_parse_dire_kind
!
subroutine wifisyn_parse_oper_kind(line,ioper,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_parse_oper_kind
  use wifisyn_cube_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  integer,          intent(out)   :: ioper
  logical,          intent(inout) :: error
  !
  integer :: larg
  character(len=oper_length) :: arg,oper
  character(len=*), parameter :: rname='PARSE/CUBE/OPER'
  !
  call sic_ke(line,0,3,arg,larg,.true.,error)
  if (error) return
  call sic_ambigs(rname,arg,oper,ioper,known_oper,moper,error)
  if (error) return
  !
end subroutine wifisyn_parse_oper_kind
!
subroutine wifisyn_parse_cube_amplitude(line,iopt,amplitude,logscale,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_parse_cube_amplitude
  use wifisyn_cube_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  integer,          intent(in)    :: iopt
  logical,          intent(out)   :: amplitude
  logical,          intent(out)   :: logscale
  logical,          intent(inout) :: error
  !
  integer :: larg,iscale
  character(len=oper_length) :: arg,scale
  character(len=*), parameter :: rname='PARSE/CUBE/AMPLITUDE'
  !
  amplitude = sic_present(iopt,0)
  if (amplitude) then
     call sic_ke(line,iopt,1,arg,larg,.true.,error)
     if (error) return
     call sic_ambigs(rname,arg,scale,iscale,known_scale,mscale,error)
     if (error) return
  else
     logscale = .false.
  endif
  !
end subroutine wifisyn_parse_cube_amplitude
!
subroutine wifisyn_parse_buffer(line,iopt,iarg,id,kind,type,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_parse_buffer
  use wifisyn_buffer_parameters
  !----------------------------------------------------------------------
  ! @ private
  ! *** JP Buffer parsing should be merged with wifisyn_parse_cube_kind
  !----------------------------------------------------------------------
  character(len=*),                  intent(inout) :: line
  integer,                           intent(in)    :: iopt
  integer,                           intent(in)    :: iarg
  integer,                           intent(out)   :: id
  character(len=buffer_kind_length), intent(out)   :: kind
  character(len=buffer_type_length), intent(out)   :: type
  logical,                           intent(inout) :: error
  !
  integer :: larg
  character(len=buffer_kind_length) :: arg
  character(len=*), parameter :: rname='PARSE/BUFFER'
  !
  call sic_ke(line,iopt,iarg,arg,larg,.true.,error)
  if (error) return
  call sic_ambigs(rname,arg,kind,id,known_buffer_kind,mbuffer,error)
  if (error) return
  type = known_buffer_type(id)
  !
end subroutine wifisyn_parse_buffer
!
subroutine wifisyn_parse_filename(line,iopt,iarg,filename,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_parse_filename
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  integer,          intent(in)    :: iopt
  integer,          intent(in)    :: iarg
  character(len=*), intent(inout) :: filename
  logical,          intent(inout) :: error
  !
  integer :: nfilename
  character(len=*), parameter :: rname='PARSE/FILENAME'
  !
  call sic_ch(line,iopt,iarg,filename,nfilename,.true.,error)
  if (error) return
  !
end subroutine wifisyn_parse_filename
!
subroutine wifisyn_parse_varname(line,iopt,iarg,vardefault,varname,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_parse_varname
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  integer,          intent(in)    :: iopt
  integer,          intent(in)    :: iarg
  character(len=*), intent(in)    :: vardefault
  character(len=*), intent(inout) :: varname
  logical,          intent(inout) :: error
  !
  integer :: nvarname
  character(len=*), parameter :: rname='PARSE/VARNAME'
  !
  if (sic_present(iopt,0)) then
     ! User defined
     call sic_ch(line,iopt,iarg,varname,nvarname,.true.,error)
     if (error) return
  else
     ! Default
     varname = vardefault
  endif
  !
end subroutine wifisyn_parse_varname
!
subroutine wifisyn_parse_channels(line,iopt,first,last,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_parse_channels
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  integer,          intent(in)    :: iopt
  integer,          intent(out)   :: first
  integer,          intent(out)   :: last
  logical,          intent(inout) :: error
  !
  integer, parameter :: ifirst = 1
  integer, parameter :: ilast = 2
  !
  integer :: ichan
  character(len=*), parameter :: rname='PARSE/CHANNELS'
  !
  if (sic_present(iopt,0)) then
     call sic_i4(line,iopt,ifirst,first,.true.,error)
     if (error) return
     call sic_i4(line,iopt,ilast,last,.true.,error)
     if (error) return
     if (first.gt.last) then
        ichan = first
        first = last
        last = ichan
     endif
  else
     first = 0
     last = 0
  endif
  !
end subroutine wifisyn_parse_channels
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_parse_readwrite(line,do,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_parse_readwrite
  use wifisyn_readwrite_types
  !----------------------------------------------------------------------
  ! @ private
  ! Parsing routine for commands
  !    READ|WRITE buffer filename
  !      1. [/CHANNELS first last]
  !----------------------------------------------------------------------
  character(len=*),  intent(inout) :: line
  type(readwrite_t), intent(inout) :: do
  logical,           intent(inout) :: error
  !
  integer, parameter :: icommand = 0
  integer, parameter :: iplane = 1
  !
  integer, parameter :: ibuffer = 1
  integer, parameter :: iname = 2
  !
  character(len=*), parameter :: rname='PARSE/READWRITE'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call wifisyn_parse_buffer(line,icommand,ibuffer,do%buffer,do%kind,do%type,error)
  if (error) return
  call wifisyn_parse_filename(line,icommand,iname,do%name,error)
  if (error) return
  call wifisyn_parse_channels(line,iplane,do%first,do%last,error)
  if (error) return
  !
end subroutine wifisyn_parse_readwrite
!
subroutine wifisyn_parse_transform(line,do,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_parse_transform
  use wifisyn_cube_types
  use wifisyn_transform_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  type(action_t),   intent(inout) :: do
  logical,          intent(inout) :: error
  !
  logical :: uv,xy,direct,inverse
  integer :: narg
  character(len=*), parameter :: rname='PARSE/TRANSFORM'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! Sanity check
  narg = sic_narg(0)
  if (narg.ne.1) then
     call wifisyn_message(seve%e,rname,'WIFI or VISI argument needed')
     error = .true.
     return
  endif
  ! Parsing
  call wifisyn_parse_cube_kind(line,do%cube,error)
  if (error) return
  ! Direct or inverse
  direct  = sic_present(1,0)
  inverse = sic_present(2,0)
  if (direct.and.inverse) then
     call wifisyn_message(seve%e,rname,'DIRECT and INVERSE options are exclusive')
     error = .true.
     return
  else if (.not.(direct.or.inverse)) then
     call wifisyn_message(seve%e,rname,'DIRECT or INVERSE option must be set')
     error = .true.
     return
  endif
  if (direct) then
     do%dire = code_direct
  else
     do%dire = code_inverse
  endif
  ! UV or XY planes
  if (do%cube.eq.code_cube_visi) then
     call wifisyn_message(seve%i,rname,'VISICUBE has no XY planes => Default to UV plane')
     uv = .true.
  else if (do%cube.eq.code_cube_wifi) then
     uv = sic_present(3,0)
     xy = sic_present(4,0)
     if (uv.and.xy) then
        call wifisyn_message(seve%e,rname,'UV and XY options are exclusive')
        error = .true.
        return
     else if (.not.(uv.or.xy)) then
        call wifisyn_message(seve%e,rname,'UV or XY option must be set for a WIFICUBE')
        error = .true.
        return
     endif
  else
     call wifisyn_message(seve%e,rname,'Unknown cube kind')
     error = .true.
     return
  endif
  if (uv) then
     do%space = code_space_uv
  else
     do%space = code_space_xy
  endif
  !
end subroutine wifisyn_parse_transform
!
subroutine wifisyn_parse_transform_direction(do,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_parse_transform_direction
  use wifisyn_transform_types
  !----------------------------------------------------------------------
  ! @ private
  ! Parsing routine for commands
  !    TRANSFORM
  !       1 /DIRECT
  !       2 /INVERSE
  !----------------------------------------------------------------------
  type(action_t),   intent(inout) :: do
  logical,          intent(inout) :: error
  !
  integer, parameter :: idirect = 1
  integer, parameter :: iinverse = 2
  logical :: direct,inverse
  character(len=*), parameter :: rname='PARSE/TRANSFORM/DIRECTION'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  direct  = sic_present(idirect,0)
  inverse = sic_present(iinverse,0)
  if (direct.and.inverse) then
     call wifisyn_message(seve%e,rname,'DIRECT and INVERSE options are exclusive')
     error = .true.
     return
  else if (.not.(direct.or.inverse)) then
     call wifisyn_message(seve%e,rname,'DIRECT or INVERSE option must be set')
     error = .true.
     return
  endif
  if (direct) then
     do%dire = code_direct
  else
     do%dire = code_inverse
  endif
  !
end subroutine wifisyn_parse_transform_direction
!
subroutine wifisyn_parse_transform_buffer(do,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_parse_transform_buffer
  use wifisyn_buffer_parameters
  use wifisyn_transform_types
  !----------------------------------------------------------------------
  ! @ private
  ! Parsing routine for commands
  !    TRANSFORM
  !       3 /UV
  !       4 /VISI
  !       5 /WIFI
  !----------------------------------------------------------------------
  type(action_t), intent(inout) :: do
  logical,        intent(inout) :: error
  !
  integer, parameter :: iuv = 3
  integer, parameter :: ivisi = 4
  integer, parameter :: iwifi = 5
  logical :: uv,visi,wifi
  character(len=*), parameter :: rname='PARSE/TRANSFORM/BUFFER'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  uv = sic_present(iuv,0)
  visi = sic_present(ivisi,0)
  wifi = sic_present(iwifi,0)
  if (.not.(uv.neqv.visi.neqv.wifi).or.(uv.and.visi.and.wifi)) then
     call wifisyn_message(seve%e,rname,'One and only one of /UV or /VISI or /WIFI must be set')
     error = .true.
     return
  else if (uv) then
     do%cube = code_buffer_uv
  else if (visi) then
     do%cube = code_buffer_visi
  else if (wifi) then
     do%cube = code_buffer_wifi
  endif
  !
end subroutine wifisyn_parse_transform_buffer
!
subroutine wifisyn_parse_transform_space(line,iopt,iarg,do,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_parse_transform_space
  use wifisyn_buffer_parameters
  use wifisyn_transform_types
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  integer,          intent(in)    :: iopt
  integer,          intent(in)    :: iarg
  type(action_t),   intent(inout) :: do
  logical,          intent(inout) :: error
  !
  integer :: larg
  character(len=2) :: arg,space
  character(len=*), parameter :: rname='PARSE/TRANSFORM/SPACE'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  if (do%cube.ne.code_buffer_wifi) return
  call sic_ke(line,iopt,iarg,arg,larg,.true.,error)
  if (error) return
  call sic_ambigs(rname,arg,space,do%space,known_space,mspace,error)
  if (error) return
  !
end subroutine wifisyn_parse_transform_space
!
subroutine wifisyn_parse_transform_bis(line,do,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_parse_transform_bis
  use wifisyn_cube_types
  use wifisyn_transform_types
  !----------------------------------------------------------------------
  ! @ private
  ! Parsing routine for commands
  !    TRANSFORM
  !       1 /DIRECT
  !       2 /INVERSE
  !       3 /UV
  !       4 /VISI
  !       5 /WIFI UV|XY
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  type(action_t),   intent(inout) :: do
  logical,          intent(inout) :: error
  !
  integer, parameter :: iwifi = 5
  integer, parameter :: ispace = 1
  character(len=*), parameter :: rname='PARSE/TRANSFORM'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call wifisyn_parse_transform_direction(do,error)
  if (error) return
  call wifisyn_parse_transform_buffer(do,error)
  if (error) return
  call wifisyn_parse_transform_space(line,iwifi,ispace,do,error)
  if (error) return
  !
end subroutine wifisyn_parse_transform_bis
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
