!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
module wifisyn_sic_buffer
  use wifisyn_gridding_types
  !----------------------------------------------------------------------
  ! Interface buffers
  !----------------------------------------------------------------------
  type(sicmap_t), save :: usermap
  type(map_t),    save :: progmap
  !
end module wifisyn_sic_buffer
!
module wifisyn_uv_buffer
  use image_def
  use wifisyn_uv_types
  !
  type(uv_t), save :: uvr ! Current  uv table
  type(uv_t), save :: uvt ! Previous uv table
  !
  real, allocatable, target :: ruv1(:,:),   tuv1(:,:)   !   1 to 7   => dimensions: ndata x 7
  real, allocatable, target :: ruv2(:,:),   tuv2(:,:)   ! n-1 to n   => dimensions: ndata x 2
  real, allocatable, target :: ruv3(:,:,:), tuv3(:,:,:) !   8 to n-2 => dimensions: ndata x 3 x nchan
  !
end module wifisyn_uv_buffer
!
module wifisyn_cube_buffer
  use wifisyn_cube_types
  !
  type(dirty_t), save :: dirty
  type(wifi_t),  save :: wifi
  type(visi_t),  save :: visi
  type(visi_t),  save :: visibis
  !
end module wifisyn_cube_buffer
!
module wifisyn_sort_buffer
  use wifisyn_sort_types
  !---------------------------------------------------------------------
  ! @ private
  !---------------------------------------------------------------------
  type(sorting_t), target :: keys(m_tot_key)
  type(inte_pointer_t)    :: inte_keys(m_inte_key)
  integer :: n_inte_key
  !
end module wifisyn_sort_buffer
!
module wifisyn_message_private
  use gpack_def
  !
  ! Identifier used for message identification
  integer :: wifisyn_message_id = gpack_global_id  ! Default value for startup message
  !
end module wifisyn_message_private
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
