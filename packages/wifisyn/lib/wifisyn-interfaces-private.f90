module wifisyn_interfaces_private
  interface
    subroutine wifisyn_apodize_command(line,error)
      use gbl_message
      use wifisyn_transform_types
      !----------------------------------------------------------------------
      ! Support routine for command
      !     APODIZE WIFI|VISI
      !       1 /DIRECT
      !       2 /INVERSE
      !       3 /UV
      !       4 /XY
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical,          intent(inout) :: error
    end subroutine wifisyn_apodize_command
  end interface
  !
  interface
    subroutine wifisyn_apodize_main(do,error)
      use gbl_message
      use wifisyn_transform_types
      use wifisyn_sic_buffer
      use wifisyn_cube_buffer
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(action_t), intent(in)    :: do
      logical,        intent(inout) :: error
    end subroutine wifisyn_apodize_main
  end interface
  !
  interface
    subroutine wifisyn_apodize_wifi_uv(idir,map,wifi,error)
      use gbl_message
      use wifisyn_transform_types
      use wifisyn_cube_types
      use wifisyn_gridding_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer,             intent(in)    :: idir
      type(map_t), target, intent(in)    :: map
      type(wifi_t),        intent(inout) :: wifi
      logical,             intent(inout) :: error
    end subroutine wifisyn_apodize_wifi_uv
  end interface
  !
  interface
    subroutine wifisyn_apodize_wifi_xy(idir,map,wifi,error)
      use gbl_message
      use wifisyn_transform_types
      use wifisyn_cube_types
      use wifisyn_gridding_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer,             intent(in)    :: idir
      type(map_t), target, intent(in)    :: map
      type(wifi_t),        intent(inout) :: wifi
      logical,             intent(inout) :: error
    end subroutine wifisyn_apodize_wifi_xy
  end interface
  !
  interface
    subroutine wifisyn_complex_command(line,error)
      use gbl_message
      use wifisyn_sic_buffer
      use wifisyn_cube_buffer
      !----------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !    COMPLEX VISICUBE|WIFICUBE TO|FROM AMPLITUDE|PHASE|REAL|IMAGINARY
      !      1 /LOGSCALE
      !      2 /NORMALIZE
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical,          intent(inout) :: error
    end subroutine wifisyn_complex_command
  end interface
  !
  interface
    subroutine wifisyn_wifi_complex2real(logscale,ioper,ichan,map,wifi,error)
      use gbl_message
      use phys_const
      use wifisyn_gridding_types
      use wifisyn_cube_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      logical,      intent(in)    :: logscale
      integer,      intent(in)    :: ioper
      integer,      intent(in)    :: ichan
      type(map_t),  intent(in)    :: map
      type(wifi_t), intent(inout) :: wifi
      logical,      intent(inout) :: error
    end subroutine wifisyn_wifi_complex2real
  end interface
  !
  interface
    subroutine wifisyn_visi_complex2real(logscale,ioper,win,map,visi,error)
      use gbl_message
      use phys_const
      use wifisyn_gridding_types
      use wifisyn_cube_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      logical,        intent(in)    :: logscale
      integer,        intent(in)    :: ioper
      type(window_t), intent(in)    :: win
      type(map_t),    intent(in)    :: map
      type(visi_t),   intent(inout) :: visi
      logical,        intent(inout) :: error
    end subroutine wifisyn_visi_complex2real
  end interface
  !
  interface
    subroutine wifisyn_wifi_header(map,wifi,error)
      use gbl_message
      use wifisyn_gridding_types
      use wifisyn_cube_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(map_t),  intent(in)    :: map
      type(wifi_t), intent(inout) :: wifi
      logical,      intent(inout) :: error
    end subroutine wifisyn_wifi_header
  end interface
  !
  interface
    subroutine wifisyn_visi_header(map,visi,error)
      use gbl_message
      use phys_const
      use wifisyn_gridding_types
      use wifisyn_cube_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(map_t),  intent(in)    :: map
      type(visi_t), intent(inout) :: visi
      logical,      intent(inout) :: error
    end subroutine wifisyn_visi_header
  end interface
  !
  interface
    subroutine wifisyn_common_header(map,cube,error)
      use gbl_message
      use image_def
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(map_t),  intent(in)    :: map
      type(gildas), intent(inout) :: cube
      logical,      intent(inout) :: error
    end subroutine wifisyn_common_header
  end interface
  !
  interface
    subroutine wifisyn_wifi_real2complex(ioper,ichan,wifi,error)
      use gbl_message
      use phys_const
      use wifisyn_gridding_types
      use wifisyn_cube_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      integer,      intent(in)    :: ioper
      integer,      intent(in)    :: ichan
      type(wifi_t), intent(inout) :: wifi
      logical,      intent(inout) :: error
    end subroutine wifisyn_wifi_real2complex
  end interface
  !
  interface
    subroutine wifisyn_visi_real2complex(ioper,win,visi,error)
      use gbl_message
      use phys_const
      use wifisyn_gridding_types
      use wifisyn_cube_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      integer,        intent(in)    :: ioper
      type(window_t), intent(in)    :: win
      type(visi_t),   intent(inout) :: visi
      logical,        intent(inout) :: error
    end subroutine wifisyn_visi_real2complex
  end interface
  !
  interface
    subroutine wifisyn_convolution_definition(pixinc,kern,error)
      use gbl_message
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      ! Determine default parameters for the convolution functions when
      ! they are not specified. Default convolving type is a spheroidal
      ! function. Any other unspecified values (= 0.0) will be set to
      ! some value.
      !----------------------------------------------------------------------
      real(kind=8),      intent(in)    :: pixinc
      type(kernel_1d_t), intent(inout) :: kern
      logical,           intent(inout) :: error
    end subroutine wifisyn_convolution_definition
  end interface
  !
  interface
    subroutine wifisyn_convolution_computation(kern,error)
      use gbl_message
      use phys_const
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      ! Compute the convolving functions and stores them in the supplied
      ! buffer. Values are tabulated every 1/100th of grid pixel.
      ! PARM(1) = radius of support in grid pixel unit
      !----------------------------------------------------------------------
      type(kernel_1d_t), intent(inout) :: kern
      logical,           intent(inout) :: error
    end subroutine wifisyn_convolution_computation
  end interface
  !
  interface
    subroutine sphfn(ialf,im,iflag,eta,psi,ier)
      use gbl_message
      !---------------------------------------------------------------------
      ! @ private
      ! SPHFN is a subroutine to evaluate rational approximations to selected
      ! zero-order spheroidal functions, psi(c,eta), which are, in a
      ! sense defined in VLA Scientific Memorandum No. 132, optimal for
      ! gridding interferometer data.  The approximations are taken from
      ! VLA Computer Memorandum No. 156.  The parameter c is related to the
      ! support width, m, of the convoluting function according to c=
      ! pi*m/2.  The parameter alpha determines a weight function in the
      ! definition of the criterion by which the function is optimal.
      ! SPHFN incorporates approximations to 25 of the spheroidal functions,
      ! corresponding to 5 choices of m (4, 5, 6, 7, or 8 cells)
      ! and 5 choices of the weighting exponent (0, 1/2, 1, 3/2, or 2).
      ! 
      ! Input:
      !   IALF    I*4   Selects the weighting exponent, alpha.  IALF =
      !                 1, 2, 3, 4, and 5 correspond, respectively, to
      !                 alpha = 0, 1/2, 1, 3/2, and 2.
      !   IM      I*4   Selects the support width m, (=IM) and, correspond-
      !                 ingly, the parameter c of the spheroidal function.
      !                 Only the choices 4, 5, 6, 7, and 8 are allowed.
      !   IFLAG   I*4   Chooses whether the spheroidal function itself, or
      !                 its Fourier transform, is to be approximated.  The
      !                 latter is appropriate for gridding, and the former
      !                 for the u-v plane convolution.  The two differ on-
      !                 by a factor (1-eta**2)**alpha.  IFLAG less than or
      !                 equal to zero chooses the function appropriate for
      !                 gridding, and IFLAG positive chooses its F.T.
      !   ETA     R*4   Eta, as the argument of the spheroidal function, is
      !                 a variable which ranges from 0 at the center of the
      !                 convoluting function to 1 at its edge (also from 0
      !                 at the center of the gridding correction function
      !                 to unity at the edge of the map).
      ! 
      ! Output:
      !   PSI      R*4  The function value which, on entry to the subrou-
      !                 tine, was to have been computed.
      !   IER      I*4  An error flag whose meaning is as follows:
      !                    IER = 0  =>  No evident problem.
      !                          1  =>  IALF is outside the allowed range.
      !                          2  =>  IM is outside of the allowed range.
      !                          3  =>  ETA is larger than 1 in absolute
      !                                    value.
      !                         12  =>  IALF and IM are out of bounds.
      !                         13  =>  IALF and ETA are both illegal.
      !                         23  =>  IM and ETA are both illegal.
      !                        123  =>  IALF, IM, and ETA all are illegal.
      !---------------------------------------------------------------------
      integer(kind=4), intent(in)  :: ialf
      integer(kind=4), intent(in)  :: im
      integer(kind=4), intent(in)  :: iflag
      real(kind=4),    intent(in)  :: eta
      real(kind=4),    intent(out) :: psi
      integer(kind=4), intent(out) :: ier
    end subroutine sphfn
  end interface
  !
  interface
    subroutine wifisyn_convolve_wifi_uvkern(map,wifi,error)
      use gbl_message
      use wifisyn_cube_types
      use wifisyn_transform_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(map_t), target, intent(in)    :: map
      type(wifi_t),        intent(inout) :: wifi
      logical,             intent(inout) :: error
    end subroutine wifisyn_convolve_wifi_uvkern
  end interface
  !
  interface
    subroutine wifisyn_deconvolve_wifi_uvkern(map,wifi,error)
      use gbl_message
      use wifisyn_cube_types
      use wifisyn_transform_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(map_t), target, intent(in)    :: map
      type(wifi_t),        intent(inout) :: wifi
      logical,             intent(inout) :: error
    end subroutine wifisyn_deconvolve_wifi_uvkern
  end interface
  !
  interface
    subroutine wifisyn_cube_reallocate_r4d(kind,cube,error)
      use gbl_message
      use image_def
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(in)    :: kind
      type(gildas),     intent(inout) :: cube
      logical,          intent(inout) :: error
    end subroutine wifisyn_cube_reallocate_r4d
  end interface
  !
  interface
    subroutine wifisyn_cube_free_r4d(cube,error)
      use gbl_message
      use image_def
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(gildas), intent(inout) :: cube
      logical,      intent(inout) :: error
    end subroutine wifisyn_cube_free_r4d
  end interface
  !
  interface
    subroutine wifisyn_cube_reallocate_r3d(nc,kind,cube,error)
      use gbl_message
      use image_def
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      integer,          intent(in)    :: nc
      character(len=*), intent(in)    :: kind
      type(gildas),     intent(inout) :: cube
      logical,          intent(inout) :: error
    end subroutine wifisyn_cube_reallocate_r3d
  end interface
  !
  interface
    subroutine wifisyn_cube_free_r3d(cube,error)
      use gbl_message
      use image_def
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(gildas), intent(inout) :: cube
      logical,      intent(inout) :: error
    end subroutine wifisyn_cube_free_r3d
  end interface
  !
  interface
    subroutine wifisyn_cube_reallocate_r2d(kind,cube,error)
      use gbl_message
      use image_def
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(in)    :: kind
      type(gildas),     intent(inout) :: cube
      logical,          intent(inout) :: error
    end subroutine wifisyn_cube_reallocate_r2d
  end interface
  !
  interface
    subroutine wifisyn_cube_free_r2d(cube,error)
      use gbl_message
      use image_def
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(gildas), intent(inout) :: cube
      logical,      intent(inout) :: error
    end subroutine wifisyn_cube_free_r2d
  end interface
  !
  interface
    subroutine wifisyn_cube_minmax_r4d(logscale,cube,error)
      use gbl_message
      use image_def
      use wifisyn_gridding_types
      !--------------------------------------------------------------------
      ! @ private
      ! Update the minimum and maximum value of a cube and
      ! their locations. We here assume that the initializations
      ! were done when the cube was initialized...
      !--------------------------------------------------------------------
      logical,        intent(in)    :: logscale
      type(gildas),   intent(inout) :: cube
      logical,        intent(inout) :: error
    end subroutine wifisyn_cube_minmax_r4d
  end interface
  !
  interface
    subroutine wifisyn_cube_minmax_r3d(logscale,win,cube,error)
      use gbl_message
      use image_def
      use wifisyn_gridding_types
      !--------------------------------------------------------------------
      ! @ private
      ! Update the minimum and maximum value of a cube and
      ! their locations. We here assume that the initializations
      ! were done when the cube was initialized...
      !--------------------------------------------------------------------
      logical,        intent(in)    :: logscale
      type(window_t), intent(in)    :: win
      type(gildas),   intent(inout) :: cube
      logical,        intent(inout) :: error
    end subroutine wifisyn_cube_minmax_r3d
  end interface
  !
  interface
    subroutine wifisyn_cube_minmax_r2d(logscale,cube,error)
      use gbl_message
      use image_def
      use wifisyn_gridding_types
      !--------------------------------------------------------------------
      ! @ private
      ! Update the minimum and maximum value of a cube and
      ! their locations. We here assume that the initializations
      ! were done when the cube was initialized...
      !--------------------------------------------------------------------
      logical,        intent(in)    :: logscale
      type(gildas),   intent(inout) :: cube
      logical,        intent(inout) :: error
    end subroutine wifisyn_cube_minmax_r2d
  end interface
  !
  interface
    subroutine wifisyn_cube_open(name,ibuffer,cube,error)
      use gbl_message
      use image_def
      use wifisyn_buffer_parameters
      !----------------------------------------------------------------------
      ! @ private
      ! *** JP should probably here use the readwrite_t
      !----------------------------------------------------------------------
      character(len=*), intent(in)    :: name
      integer,          intent(in)    :: ibuffer
      type(gildas),     intent(inout) :: cube
      logical,          intent(inout) :: error
    end subroutine wifisyn_cube_open
  end interface
  !
  interface
    subroutine wifisyn_cube_read(do,cube,error)
      use gbl_message
      use image_def
      use wifisyn_readwrite_types
      use wifisyn_cube_types
      !------------------------------------------------------------------
      ! @ private
      ! Read input cube header and data
      !------------------------------------------------------------------
      type(readwrite_t), intent(in)    :: do
      type(gildas),      intent(inout) :: cube
      logical,           intent(inout) :: error
    end subroutine wifisyn_cube_read
  end interface
  !
  interface
    subroutine wifisyn_cube_write_r4d(i3,i4,cube,error)
      use gbl_message
      use gildas_def
      use image_def
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      integer,              intent(in)    :: i3
      integer,              intent(in)    :: i4
      type(gildas), target, intent(inout) :: cube
      logical,              intent(inout) :: error
    end subroutine wifisyn_cube_write_r4d
  end interface
  !
  interface
    subroutine wifisyn_cube_write(win,cube,error)
      use gbl_message
      use gildas_def
      use image_def
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(window_t), intent(in)    :: win
      type(gildas),   intent(inout) :: cube
      logical,        intent(inout) :: error
    end subroutine wifisyn_cube_write
  end interface
  !
  interface
    subroutine wifisyn_cube_close(cube,error)
      use gbl_message
      use image_def
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(gildas),     intent(inout) :: cube
      logical,          intent(inout) :: error
    end subroutine wifisyn_cube_close
  end interface
  !
  interface
    subroutine wifisyn_cube_sicdef(readonly,name,cube,error)
      use gbl_message
      use image_def
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      logical,          intent(in)    :: readonly
      character(len=*), intent(in)    :: name ! sic name
      type(gildas),     intent(inout) :: cube ! buffer
      logical,          intent(inout) :: error
    end subroutine wifisyn_cube_sicdef
  end interface
  !
  interface
    subroutine wifisyn_cube_sicdel_header(name,error)
      use gbl_message
      use gildas_def
      use image_def
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: name
      logical,          intent(inout) :: error
    end subroutine wifisyn_cube_sicdel_header
  end interface
  !
  interface
    subroutine wifisyn_allocate_fft_wifi(ispace,wifi,fft,error)
      use gbl_message
      use wifisyn_cube_types
      use wifisyn_transform_types
      use wifisyn_gridding_types
      !-------------------------------------------------------------------
      ! @ private
      !-------------------------------------------------------------------
      integer,              intent(in)    :: ispace
      type(wifi_t), target, intent(in)    :: wifi
      type(fft_t),          intent(out)   :: fft
      logical,              intent(inout) :: error
    end subroutine wifisyn_allocate_fft_wifi
  end interface
  !
  interface
    subroutine wifisyn_allocate_fft_visi(visi,fft,error)
      use gbl_message
      use wifisyn_cube_types
      use wifisyn_transform_types
      use wifisyn_gridding_types
      !-------------------------------------------------------------------
      ! @ private
      !-------------------------------------------------------------------
      type(visi_t), target, intent(in)    :: visi
      type(fft_t),          intent(out)   :: fft
      logical,              intent(inout) :: error
    end subroutine wifisyn_allocate_fft_visi
  end interface
  !
  interface
    subroutine wifisyn_free_fft(fft,error)
      use gbl_message
      use wifisyn_transform_types
      !-------------------------------------------------------------------
      ! @ private
      !-------------------------------------------------------------------
      type(fft_t), intent(inout) :: fft
      logical,     intent(inout) :: error
    end subroutine wifisyn_free_fft
  end interface
  !
  interface
    subroutine wifisyn_fft_command(line,error)
      use gbl_message
      use wifisyn_sic_buffer
      use wifisyn_cube_buffer
      use wifisyn_transform_types
      !----------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !     FFT WIFI|VISI
      !       1 /DIRECT
      !       2 /INVERSE
      !       3 /UV
      !       4 /XY
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical,          intent(inout) :: error
    end subroutine wifisyn_fft_command
  end interface
  !
  interface
    subroutine wifisyn_fft_wifi(idir,ispace,win,wifi,error)
      use gbl_message
      use wifisyn_gridding_types
      use wifisyn_cube_types
      use wifisyn_transform_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer,        intent(in)    :: idir
      integer,        intent(in)    :: ispace
      type(window_t), intent(in)    :: win
      type(wifi_t),   intent(inout) :: wifi
      logical,        intent(inout) :: error
    end subroutine wifisyn_fft_wifi
  end interface
  !
  interface
    subroutine wifisyn_fft_wifi_xy(idir,win,fft,error)
      use gbl_message
      use gkernel_types
      use wifisyn_transform_types
      use wifisyn_gridding_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer,        intent(in)    :: idir
      type(window_t), intent(in)    :: win
      type(fft_t),    intent(inout) :: fft
      logical,        intent(inout) :: error
    end subroutine wifisyn_fft_wifi_xy
  end interface
  !
  interface
    subroutine wifisyn_fft_xyuv2xy(ic,iu,iv,fft)
      use gbl_message
      use wifisyn_transform_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer,     intent(in)    :: ic
      integer,     intent(in)    :: iu
      integer,     intent(in)    :: iv
      type(fft_t), intent(inout) :: fft
    end subroutine wifisyn_fft_xyuv2xy
  end interface
  !
  interface
    subroutine wifisyn_fft_xy2xyuv(ic,iu,iv,fft)
      use gbl_message
      use image_def
      use wifisyn_transform_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer,     intent(in)    :: ic
      integer,     intent(in)    :: iu
      integer,     intent(in)    :: iv
      type(fft_t), intent(inout) :: fft
    end subroutine wifisyn_fft_xy2xyuv
  end interface
  !
  interface
    subroutine wifisyn_fft_wifi_uv(idir,win,fft,error)
      use gbl_message
      use gkernel_types
      use wifisyn_transform_types
      use wifisyn_gridding_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer,        intent(in)    :: idir
      type(window_t), intent(in)    :: win
      type(fft_t),    intent(inout) :: fft
      logical,        intent(inout) :: error
    end subroutine wifisyn_fft_wifi_uv
  end interface
  !
  interface
    subroutine wifisyn_fft_xyuv2uv(ic,ix,iy,fft)
      use gbl_message
      use wifisyn_transform_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer,     intent(in)    :: ic
      integer,     intent(in)    :: ix
      integer,     intent(in)    :: iy
      type(fft_t), intent(inout) :: fft
    end subroutine wifisyn_fft_xyuv2uv
  end interface
  !
  interface
    subroutine wifisyn_fft_uv2xyuv(ic,ix,iy,fft)
      use gbl_message
      use image_def
      use wifisyn_transform_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer,     intent(in)    :: ic
      integer,     intent(in)    :: ix
      integer,     intent(in)    :: iy
      type(fft_t), intent(inout) :: fft
    end subroutine wifisyn_fft_uv2xyuv
  end interface
  !
  interface
    subroutine wifisyn_fft_visi(idir,win,visi,error)
      use gbl_message
      use wifisyn_gridding_types
      use wifisyn_cube_types
      use wifisyn_transform_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer,        intent(in)    :: idir
      type(window_t), intent(in)    :: win
      type(visi_t),   intent(inout) :: visi
      logical,        intent(inout) :: error
    end subroutine wifisyn_fft_visi
  end interface
  !
  interface
    subroutine wifisyn_fft_visi_uv(idir,win,fft,error)
      use gbl_message
      use gkernel_types
      use wifisyn_transform_types
      use wifisyn_gridding_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer,        intent(in)    :: idir
      type(window_t), intent(in)    :: win
      type(fft_t),    intent(inout) :: fft
      logical,        intent(inout) :: error
    end subroutine wifisyn_fft_visi_uv
  end interface
  !
  interface
    subroutine wifisyn_fft_visi2plane(ic,fft)
      use gbl_message
      use wifisyn_transform_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer,     intent(in)    :: ic
      type(fft_t), intent(inout) :: fft
    end subroutine wifisyn_fft_visi2plane
  end interface
  !
  interface
    subroutine wifisyn_fft_plane2visi(ic,fft)
      use gbl_message
      use image_def
      use wifisyn_transform_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer,     intent(in)    :: ic
      type(fft_t), intent(inout) :: fft
    end subroutine wifisyn_fft_plane2visi
  end interface
  !
  interface
    subroutine wifisyn_gridding_nullify_axis_1d(axis)
      use gbl_message
      use wifisyn_gridding_types
      !----------------------------------------------------------
      ! @ private
      !----------------------------------------------------------
      type(axis_1d_t), target, intent(inout) :: axis
    end subroutine wifisyn_gridding_nullify_axis_1d
  end interface
  !
  interface
    subroutine wifisyn_gridding_nullify_axis_pair(kind,axis)
      use gbl_message
      use wifisyn_gridding_types
      !----------------------------------------------------------
      ! @ private
      !----------------------------------------------------------
      integer,           intent(in)    :: kind
      type(axis_pair_t), intent(inout) :: axis
    end subroutine wifisyn_gridding_nullify_axis_pair
  end interface
  !
  interface
    subroutine wifisyn_gridding_nullify_axes(axes)
      use gbl_message
      use wifisyn_gridding_types
      !----------------------------------------------------------
      ! @ private
      !----------------------------------------------------------
      type(axis_t), intent(inout) :: axes(2)
    end subroutine wifisyn_gridding_nullify_axes
  end interface
  !
  interface
    subroutine wifisyn_gridding_nullify_map(map)
      use gbl_message
      use wifisyn_gridding_types
      !----------------------------------------------------------
      ! @ private
      !----------------------------------------------------------
      type(map_t), intent(inout) :: map
    end subroutine wifisyn_gridding_nullify_map
  end interface
  !
  interface
    subroutine wifisyn_free_map(map,error)
      use gbl_message
      use wifisyn_gridding_types
      !----------------------------------------------------------
      ! @ private
      !----------------------------------------------------------
      type(map_t), intent(inout) :: map
      logical,     intent(inout) :: error
    end subroutine wifisyn_free_map
  end interface
  !
  interface
    subroutine wifisyn_gridding_sicdef_user(error)
      use gildas_def
      use gbl_message
      use wifisyn_sic_buffer
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine wifisyn_gridding_sicdef_user
  end interface
  !
  interface
    subroutine wifisyn_gridding_sicdef_prog(prog_request,error)
      use gildas_def
      use gbl_message
      use wifisyn_sic_buffer
      !----------------------------------------------------------------------
      ! @ private
      ! *** JP Can not yet define readwrite or readonly
      !----------------------------------------------------------------------
      logical, intent(in)    :: prog_request
      logical, intent(inout) :: error
    end subroutine wifisyn_gridding_sicdef_prog
  end interface
  !
  interface
    subroutine wifisyn_gridding_sicdef_prog_axes(name,axes,error)
      use gbl_message
      use gildas_def
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(in)    :: name
      type(axis_t),     intent(in)    :: axes(2)
      logical,          intent(inout) :: error
    end subroutine wifisyn_gridding_sicdef_prog_axes
  end interface
  !
  interface
    subroutine wifisyn_gridding_sicdef_prog_axis_pair(name,axis,error)
      use gildas_def
      use gbl_message
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*),  intent(in)    :: name
      type(axis_pair_t), intent(in)    :: axis
      logical,           intent(inout) :: error
    end subroutine wifisyn_gridding_sicdef_prog_axis_pair
  end interface
  !
  interface
    subroutine wifisyn_gridding_sicdef_prog_axis_1d(name,axis,error)
      use gildas_def
      use gbl_message
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(in)    :: name
      type(axis_1d_t),  intent(in)    :: axis
      logical,          intent(inout) :: error
    end subroutine wifisyn_gridding_sicdef_prog_axis_1d
  end interface
  !
  interface
    subroutine wifisyn_gridding_sicdef_prog_kern(name,kern,error)
      use gildas_def
      use gbl_message
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(in)    :: name
      type(kernel_t),   intent(in)    :: kern(2)
      logical,          intent(inout) :: error
    end subroutine wifisyn_gridding_sicdef_prog_kern
  end interface
  !
  interface
    subroutine wifisyn_gridding_sicdef_prog_kernel_1d(name,kern,error)
      use gildas_def
      use gbl_message
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*),  intent(in)    :: name
      type(kernel_1d_t), intent(in)    :: kern
      logical,           intent(inout) :: error
    end subroutine wifisyn_gridding_sicdef_prog_kernel_1d
  end interface
  !
  interface
    subroutine wifisyn_sicdef_dble_1d(name,val,nval,readonly,error)
      use gildas_def
      use gbl_message
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(in)    :: name
      integer,          intent(in)    :: nval
      real(kind=8),     intent(in)    :: val(nval)
      logical,          intent(in)    :: readonly
      logical,          intent(inout) :: error
    end subroutine wifisyn_sicdef_dble_1d
  end interface
  !
  interface
    subroutine wifisyn_sicdef_real_1d(name,val,nval,readonly,error)
      use gildas_def
      use gbl_message
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(in)    :: name
      integer,          intent(in)    :: nval
      real(kind=4),     intent(in)    :: val(nval)
      logical,          intent(in)    :: readonly
      logical,          intent(inout) :: error
    end subroutine wifisyn_sicdef_real_1d
  end interface
  !
  interface
    subroutine wifisyn_gridding_set_huv(huv,map,error)
      use gbl_message
      use wifisyn_uv_types
      use wifisyn_gridding_types
      !----------------------------------------------------------
      ! @ private
      !----------------------------------------------------------
      type(uv_head_t), intent(in)    :: huv
      type(map_t),     intent(inout) :: map
      logical,         intent(inout) :: error
    end subroutine wifisyn_gridding_set_huv
  end interface
  !
  interface
    subroutine wifisyn_gridding_set_tabdim(huv,n,error)
      use gbl_message
      use wifisyn_uv_types
      use wifisyn_gridding_types
      !----------------------------------------------------------
      ! @ private
      !----------------------------------------------------------
      type(uv_head_t), intent(in)    :: huv
      type(tabdim_t),  intent(out)   :: n
      logical,         intent(inout) :: error
    end subroutine wifisyn_gridding_set_tabdim
  end interface
  !
  interface
    subroutine wifisyn_gridding_set_winlist(user,nchan,winlist,error)
      use gbl_message
      use wifisyn_uv_types
      use wifisyn_gridding_types
      !----------------------------------------------------------
      ! @ private
      !----------------------------------------------------------
      type(sicmap_t),  intent(in)    :: user
      integer,         intent(in)    :: nchan
      type(winlist_t), intent(inout) :: winlist
      logical,         intent(inout) :: error
    end subroutine wifisyn_gridding_set_winlist
  end interface
  !
  interface
    subroutine wifisyn_gridding_set_rchan(huv,rchan,error)
      use gbl_message
      use phys_const
      use wifisyn_uv_types
      use wifisyn_gridding_types
      !----------------------------------------------------------
      ! @ private
      !----------------------------------------------------------
      type(uv_head_t), intent(in)    :: huv
      type(channel_t), intent(out)   :: rchan
      logical,         intent(inout) :: error
    end subroutine wifisyn_gridding_set_rchan
  end interface
  !
  interface
    subroutine wifisyn_gridding_set_shift(user,hin,rchan,shift,error)
      use gbl_message
      use gbl_constant
      use phys_const
      use image_def
      use gkernel_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(sicmap_t),  intent(in)  :: user
      type(gildas),    intent(in)  :: hin
      type(channel_t), intent(in)  :: rchan
      type(shift_t),   intent(out) :: shift
      logical,         intent(out) :: error
    end subroutine wifisyn_gridding_set_shift
  end interface
  !
  interface
    subroutine wifisyn_gridding_set_topology(huv,topo,error)
      use gbl_message
      use phys_const
      use wifisyn_uv_types
      use wifisyn_gridding_types
      !----------------------------------------------------------
      ! @ private
      !----------------------------------------------------------
      type(uv_head_t),  intent(in)    :: huv
      type(topology_t), intent(out)   :: topo(2)
      logical,          intent(inout) :: error
    end subroutine wifisyn_gridding_set_topology
  end interface
  !
  interface
    subroutine wifisyn_gridding_set_1d_scale(hin,rchan,topo,scale,error)
      use gbl_message
      use phys_const
      use image_def
      use wifisyn_gridding_types
      !----------------------------------------------------------
      ! @ private
      !----------------------------------------------------------
      type(gildas),     intent(in)    :: hin
      type(channel_t),  intent(in)    :: rchan
      type(topology_t), intent(in)    :: topo
      type(scale_t),    intent(out)   :: scale
      logical,          intent(inout) :: error
    end subroutine wifisyn_gridding_set_1d_scale
  end interface
  !
  interface
    subroutine wifisyn_gridding_set_1d_axis(rchan,scale,axis,error)
      use gbl_message
      use wifisyn_gridding_types
      !----------------------------------------------------------
      ! @ private
      !----------------------------------------------------------
      type(channel_t), intent(in)    :: rchan
      type(scale_t),   intent(in)    :: scale
      type(axis_t),    intent(inout) :: axis
      logical,         intent(inout) :: error
    end subroutine wifisyn_gridding_set_1d_axis
  end interface
  !
  interface
    subroutine wifisyn_gridding_set_1d_axis_pair(rchan,npix,incuvp,axis,error)
      use gbl_message
      use wifisyn_gridding_types
      !----------------------------------------------------------
      ! @ private
      !----------------------------------------------------------
      type(channel_t),   intent(in)    :: rchan
      integer,           intent(in)    :: npix
      real(kind=8),      intent(in)    :: incuvp
      type(axis_pair_t), intent(inout) :: axis
      logical,           intent(inout) :: error
    end subroutine wifisyn_gridding_set_1d_axis_pair
  end interface
  !
  interface
    subroutine wifisyn_gridding_set_1d_coord(axis,error)
      use gbl_message
      use wifisyn_gridding_types
      !------------------------------------------------------------------------
      ! @ private
      ! Allocate and compute coordinate array from conversion formula
      !------------------------------------------------------------------------
      type(axis_1d_t), intent(inout) :: axis
      logical,         intent(inout) :: error
    end subroutine wifisyn_gridding_set_1d_coord
  end interface
  !
  interface
    subroutine wifisyn_gridding_set_1d_kernel(user,axis,kern,error)
      use gbl_message
      use wifisyn_gridding_types
      !----------------------------------------------------------
      ! @ private
      !----------------------------------------------------------
      type(sicmap_t),    intent(in)    :: user
      type(axis_pair_t), intent(in)    :: axis
      type(kernel_1d_t), intent(inout) :: kern
      logical,           intent(inout) :: error
    end subroutine wifisyn_gridding_set_1d_kernel
  end interface
  !
  interface
    subroutine wifisyn_gridding_set_taper(user,taper,error)
      use gbl_message
      use phys_const
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(sicmap_t), intent(in)    :: user
      type(taper_t),  intent(inout) :: taper
      logical,        intent(inout) :: error
    end subroutine wifisyn_gridding_set_taper
  end interface
  !
  interface
    subroutine wifisyn_gridding_set_buffer(map,error)
      use gbl_message
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(map_t), intent(inout) :: map
      logical,     intent(inout) :: error
    end subroutine wifisyn_gridding_set_buffer
  end interface
  !
  interface
    subroutine wifisyn_gridding_print(map,error)
      use gbl_message
      use phys_const
      use wifisyn_gridding_types
      !----------------------------------------------------------
      ! @ private
      !----------------------------------------------------------
      type(map_t), target, intent(inout) :: map   ! Gridding parameters
      logical,             intent(inout) :: error ! Return status
    end subroutine wifisyn_gridding_print
  end interface
  !
  interface
    subroutine wifisyn_ift_kern(nift,kern,error)
      use gbl_message
      use phys_const
      use wifisyn_gridding_types
      !---------------------------------------------------------------------
      ! @ private
      ! Compute the inverse Fourier transform of the gridding function
      ! on the full image support (i.e. much larger number of pixels than the
      ! initial definition). The result is centered on kern%ft%n/2+1.
      ! I guess that kern%ft%n must be even (not checked...).
      !---------------------------------------------------------------------
      integer,           intent(in)    :: nift
      type(kernel_1d_t), intent(inout) :: kern
      logical,           intent(inout) :: error
    end subroutine wifisyn_ift_kern
  end interface
  !
  interface
    subroutine wifisyn_ift_shift_and_plunge(ic,cube,image)
      use gbl_message
      use wifisyn_array_types
      !---------------------------------------------------------------------
      ! @ private
      ! Extract a Fourier plane from the FFT cube
      !---------------------------------------------------------------------
      integer,            intent(in)    :: ic
      type(complex_3d_t), intent(in)    :: cube
      type(complex_2d_t), intent(inout) :: image
    end subroutine wifisyn_ift_shift_and_plunge
  end interface
  !
  interface
    subroutine wifisyn_ift_shift_and_real(ic,image,cube)
      use gbl_message
      use image_def
      use wifisyn_array_types
      !---------------------------------------------------------------------
      ! @ private
      ! Extract real image from an IFT image with appropriate recentering
      !---------------------------------------------------------------------
      integer,            intent(in)    :: ic
      type(complex_2d_t), intent(in)    :: image
      type(gildas),       intent(inout) :: cube
    end subroutine wifisyn_ift_shift_and_real
  end interface
  !
  interface
    subroutine wifisyn_ift_compute_grid_correction(beam,ukern,vkern,corr,error)
      use gbl_message
      use image_def
      use wifisyn_array_types
      use wifisyn_gridding_types
      !---------------------------------------------------------------------
      ! @ private
      ! Compute grid correction array, with normalisation of beam to 1.0
      ! at maximum pixel
      !---------------------------------------------------------------------
      type(gildas),      intent(in)    :: beam  ! Beam before normalization
      type(kernel_1d_t), intent(in)    :: ukern ! U gridding kernel
      type(kernel_1d_t), intent(in)    :: vkern ! V gridding kernel
      type(real_2d_t),   intent(inout) :: corr  ! Beam-normalized 2D grid correction
      logical,           intent(inout) :: error
    end subroutine wifisyn_ift_compute_grid_correction
  end interface
  !
  interface
    subroutine wifisyn_ift_apply_grid_correction(win,corr,cube,error)
      use gbl_message
      use image_def
      use wifisyn_array_types
      use wifisyn_gridding_types
      !---------------------------------------------------------------------
      ! @ private
      ! Apply gridding kernel correction to map
      !---------------------------------------------------------------------
      type(window_t),  intent(in)    :: win
      type(real_2d_t), intent(in)    :: corr
      type(gildas),    intent(inout) :: cube
      logical,         intent(inout) :: error
    end subroutine wifisyn_ift_apply_grid_correction
  end interface
  !
  interface
    subroutine wifisyn_init(error)
      use wifisyn_uv_types
      !----------------------------------------------------------------------
      ! @ private
      ! WIFISYN Initialization
      !----------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine wifisyn_init
  end interface
  !
  interface
    subroutine wifisyn_exit(error)
      use wifisyn_uv_buffer
      !----------------------------------------------------------------------
      ! @ private
      ! WIFISYN Initialization
      !----------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine wifisyn_exit
  end interface
  !
  interface
    subroutine wifisyn_load
      !----------------------------------------------------------------------
      ! @ private
      ! WIFISYN language definition
      !----------------------------------------------------------------------
    end subroutine wifisyn_load
  end interface
  !
  interface
    subroutine wifisyn_run(line,command,error)
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      ! WIFISYN Main routine: Call appropriate subroutine according to COMMAND
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      character(len=*), intent(in)    :: command
      logical,          intent(out)   :: error
    end subroutine wifisyn_run
  end interface
  !
  interface
    subroutine wifisyn_write_visi(do,map,visi,error)
      use gbl_message
      use wifisyn_readwrite_types
      use wifisyn_cube_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(readwrite_t), intent(in)    :: do
      type(map_t),       intent(in)    :: map
      type(visi_t),      intent(inout) :: visi
      logical,           intent(inout) :: error
    end subroutine wifisyn_write_visi
  end interface
  !
  interface
    subroutine wifisyn_write_wifi(do,map,wifi,error)
      use gbl_message
      use wifisyn_readwrite_types
      use wifisyn_cube_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(readwrite_t), intent(in)    :: do
      type(map_t),       intent(in)    :: map
      type(wifi_t),      intent(inout) :: wifi
      logical,           intent(inout) :: error
    end subroutine wifisyn_write_wifi
  end interface
  !
  interface
    subroutine wifisyn_read_wifi(do,wifi,error)
      use gbl_message
      use wifisyn_readwrite_types
      use wifisyn_cube_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      ! *** JP1 The progmap structure should be set as well to ensure consistency
      ! *** JP2 There is no consistency check between real and imaginary part...
      ! *** JP3 The xykind and uvkind should be set according to the file units
      !----------------------------------------------------------------------
      type(readwrite_t), intent(in)    :: do
      type(wifi_t),      intent(inout) :: wifi
      logical,           intent(inout) :: error
    end subroutine wifisyn_read_wifi
  end interface
  !
  interface
    subroutine wifisyn_load_command(line,error)
      use gbl_message
      use wifisyn_load_types
      !----------------------------------------------------------------------
      ! @ private
      ! Support routine for command LOAD
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical,          intent(inout) :: error
    end subroutine wifisyn_load_command
  end interface
  !
  interface
    subroutine wifisyn_load_parse(line,do,error)
      use gbl_message
      use wifisyn_load_types
      !----------------------------------------------------------------------
      ! @ private
      ! Parsing routine for command
      !    LOAD buffer [varname]
      !      1. [/AMPLITUDE [LOGSCALE | LINSCALE]]
      !      2. [/NORMALIZE]
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      type(load_t),     intent(inout) :: do
      logical,          intent(inout) :: error
    end subroutine wifisyn_load_parse
  end interface
  !
  interface
    subroutine wifisyn_load_main(do,error)
      use gbl_message
      use wifisyn_load_types
      use wifisyn_cube_buffer
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(load_t), intent(in)    :: do
      logical,      intent(inout) :: error
    end subroutine wifisyn_load_main
  end interface
  !
  interface
    subroutine wifisyn_message_set_id(id)
      use gbl_message
      use wifisyn_message_private
      !---------------------------------------------------------------------
      ! @ private
      ! Alter library id into input id. Should be called by the library
      ! which wants to share its id with the current one.
      !---------------------------------------------------------------------
      integer, intent(in) :: id
    end subroutine wifisyn_message_set_id
  end interface
  !
  interface
    subroutine wifisyn_message(mkind,procname,mymessage)
      use wifisyn_message_private
      !---------------------------------------------------------------------
      ! @ private
      ! Messaging facility for the current library. Calls the low-level
      ! (internal) messaging routine with its own identifier.
      !---------------------------------------------------------------------
      integer,          intent(in) :: mkind    ! Message kind
      character(len=*), intent(in) :: procname ! Name of calling procedure
      character(len=*), intent(in) :: mymessage  ! Message string
    end subroutine wifisyn_message
  end interface
  !
  interface
    subroutine wifisyn_pack_set(pack)
      use gpack_def
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(gpack_info_t), intent(out) :: pack
    end subroutine wifisyn_pack_set
  end interface
  !
  interface
    subroutine wifisyn_pack_init(gpack_id,error)
      use sic_def
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      integer :: gpack_id
      logical :: error
    end subroutine wifisyn_pack_init
  end interface
  !
  interface
    subroutine wifisyn_pack_on_exit(error)
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      logical :: error
    end subroutine wifisyn_pack_on_exit
  end interface
  !
  interface
    subroutine wifisyn_parse_unit_kind(line,iopt,iarg,iunit,error)
      use gbl_message
      use wifisyn_transform_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      integer,          intent(in)    :: iopt
      integer,          intent(in)    :: iarg
      integer,          intent(out)   :: iunit
      logical,          intent(inout) :: error
    end subroutine wifisyn_parse_unit_kind
  end interface
  !
  interface
    subroutine wifisyn_parse_cube_kind(line,icube,error)
      use gbl_message
      use wifisyn_cube_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      integer,          intent(out)   :: icube
      logical,          intent(inout) :: error
    end subroutine wifisyn_parse_cube_kind
  end interface
  !
  interface
    subroutine wifisyn_parse_dire_kind(line,idire,error)
      use gbl_message
      use wifisyn_cube_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      integer,          intent(out)   :: idire
      logical,          intent(inout) :: error
    end subroutine wifisyn_parse_dire_kind
  end interface
  !
  interface
    subroutine wifisyn_parse_oper_kind(line,ioper,error)
      use gbl_message
      use wifisyn_cube_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      integer,          intent(out)   :: ioper
      logical,          intent(inout) :: error
    end subroutine wifisyn_parse_oper_kind
  end interface
  !
  interface
    subroutine wifisyn_parse_cube_amplitude(line,iopt,amplitude,logscale,error)
      use gbl_message
      use wifisyn_cube_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      integer,          intent(in)    :: iopt
      logical,          intent(out)   :: amplitude
      logical,          intent(out)   :: logscale
      logical,          intent(inout) :: error
    end subroutine wifisyn_parse_cube_amplitude
  end interface
  !
  interface
    subroutine wifisyn_parse_buffer(line,iopt,iarg,id,kind,type,error)
      use gbl_message
      use wifisyn_buffer_parameters
      !----------------------------------------------------------------------
      ! @ private
      ! *** JP Buffer parsing should be merged with wifisyn_parse_cube_kind
      !----------------------------------------------------------------------
      character(len=*),                  intent(inout) :: line
      integer,                           intent(in)    :: iopt
      integer,                           intent(in)    :: iarg
      integer,                           intent(out)   :: id
      character(len=buffer_kind_length), intent(out)   :: kind
      character(len=buffer_type_length), intent(out)   :: type
      logical,                           intent(inout) :: error
    end subroutine wifisyn_parse_buffer
  end interface
  !
  interface
    subroutine wifisyn_parse_filename(line,iopt,iarg,filename,error)
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      integer,          intent(in)    :: iopt
      integer,          intent(in)    :: iarg
      character(len=*), intent(inout) :: filename
      logical,          intent(inout) :: error
    end subroutine wifisyn_parse_filename
  end interface
  !
  interface
    subroutine wifisyn_parse_varname(line,iopt,iarg,vardefault,varname,error)
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      integer,          intent(in)    :: iopt
      integer,          intent(in)    :: iarg
      character(len=*), intent(in)    :: vardefault
      character(len=*), intent(inout) :: varname
      logical,          intent(inout) :: error
    end subroutine wifisyn_parse_varname
  end interface
  !
  interface
    subroutine wifisyn_parse_channels(line,iopt,first,last,error)
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      integer,          intent(in)    :: iopt
      integer,          intent(out)   :: first
      integer,          intent(out)   :: last
      logical,          intent(inout) :: error
    end subroutine wifisyn_parse_channels
  end interface
  !
  interface
    subroutine wifisyn_parse_readwrite(line,do,error)
      use gbl_message
      use wifisyn_readwrite_types
      !----------------------------------------------------------------------
      ! @ private
      ! Parsing routine for commands
      !    READ|WRITE buffer filename
      !      1. [/CHANNELS first last]
      !----------------------------------------------------------------------
      character(len=*),  intent(inout) :: line
      type(readwrite_t), intent(inout) :: do
      logical,           intent(inout) :: error
    end subroutine wifisyn_parse_readwrite
  end interface
  !
  interface
    subroutine wifisyn_parse_transform(line,do,error)
      use gbl_message
      use wifisyn_cube_types
      use wifisyn_transform_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      type(action_t),   intent(inout) :: do
      logical,          intent(inout) :: error
    end subroutine wifisyn_parse_transform
  end interface
  !
  interface
    subroutine wifisyn_parse_transform_direction(do,error)
      use gbl_message
      use wifisyn_transform_types
      !----------------------------------------------------------------------
      ! @ private
      ! Parsing routine for commands
      !    TRANSFORM
      !       1 /DIRECT
      !       2 /INVERSE
      !----------------------------------------------------------------------
      type(action_t),   intent(inout) :: do
      logical,          intent(inout) :: error
    end subroutine wifisyn_parse_transform_direction
  end interface
  !
  interface
    subroutine wifisyn_parse_transform_buffer(do,error)
      use gbl_message
      use wifisyn_buffer_parameters
      use wifisyn_transform_types
      !----------------------------------------------------------------------
      ! @ private
      ! Parsing routine for commands
      !    TRANSFORM
      !       3 /UV
      !       4 /VISI
      !       5 /WIFI
      !----------------------------------------------------------------------
      type(action_t), intent(inout) :: do
      logical,        intent(inout) :: error
    end subroutine wifisyn_parse_transform_buffer
  end interface
  !
  interface
    subroutine wifisyn_parse_transform_space(line,iopt,iarg,do,error)
      use gbl_message
      use wifisyn_buffer_parameters
      use wifisyn_transform_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      integer,          intent(in)    :: iopt
      integer,          intent(in)    :: iarg
      type(action_t),   intent(inout) :: do
      logical,          intent(inout) :: error
    end subroutine wifisyn_parse_transform_space
  end interface
  !
  interface
    subroutine wifisyn_parse_transform_bis(line,do,error)
      use gbl_message
      use wifisyn_cube_types
      use wifisyn_transform_types
      !----------------------------------------------------------------------
      ! @ private
      ! Parsing routine for commands
      !    TRANSFORM
      !       1 /DIRECT
      !       2 /INVERSE
      !       3 /UV
      !       4 /VISI
      !       5 /WIFI UV|XY
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      type(action_t),   intent(inout) :: do
      logical,          intent(inout) :: error
    end subroutine wifisyn_parse_transform_bis
  end interface
  !
  interface
    subroutine wifisyn_read_command(line,error)
      use gbl_message
      use wifisyn_readwrite_types
      !----------------------------------------------------------------------
      ! @ private
      ! Support routine for command READ
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical,          intent(inout) :: error
    end subroutine wifisyn_read_command
  end interface
  !
  interface
    subroutine wifisyn_read_main(do,error)
      use gbl_message
      use wifisyn_readwrite_types
      use wifisyn_uv_buffer
      use wifisyn_cube_buffer
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(readwrite_t), intent(inout) :: do
      logical,           intent(inout) :: error
    end subroutine wifisyn_read_main
  end interface
  !
  interface
    subroutine wifisyn_read_uv(do,uv,error)
      use gbl_message
      use wifisyn_readwrite_types
      use wifisyn_uv_types
      !------------------------------------------------------------------
      ! @ private
      ! Read input UV header and data
      !------------------------------------------------------------------
      type(readwrite_t), intent(in)    :: do
      type(uv_t),        intent(inout) :: uv
      logical,           intent(inout) :: error
    end subroutine wifisyn_read_uv
  end interface
  !
  interface
    subroutine wifisyn_setup_command(line,error)
      use gbl_message
      use wifisyn_uv_buffer
      use wifisyn_sic_buffer
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical,          intent(inout) :: error
    end subroutine wifisyn_setup_command
  end interface
  !
  interface
    subroutine wifisyn_setup_main(user,uvin,map,uvout,error)
      use gbl_message
      use wifisyn_uv_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(sicmap_t), intent(in)    :: user
      type(uv_t),     intent(in)    :: uvin
      type(map_t),    intent(inout) :: map
      type(uv_t),     intent(inout) :: uvout
      logical,        intent(inout) :: error
    end subroutine wifisyn_setup_main
  end interface
  !
  interface
    subroutine wifisyn_setup_uvtmp(uvin,uvout,error)
      use gbl_message
      use wifisyn_uv_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(uv_t), intent(in)    :: uvin
      type(uv_t), intent(inout) :: uvout
      logical,    intent(inout) :: error
    end subroutine wifisyn_setup_uvtmp
  end interface
  !
  interface
    subroutine wifisyn_setup_map_and_shift_header(user,map,huv,error)
      use gbl_message
      use wifisyn_uv_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(sicmap_t),  intent(in)    :: user
      type(map_t),     intent(inout) :: map
      type(uv_head_t), intent(inout) :: huv
      logical,         intent(inout) :: error
    end subroutine wifisyn_setup_map_and_shift_header
  end interface
  !
  interface
    subroutine wifisyn_shift_command(line,error)
      use gbl_message
      use wifisyn_transform_types
      !----------------------------------------------------------------------
      ! @ private
      ! Entry point for command SHIFT
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical,          intent(inout) :: error
    end subroutine wifisyn_shift_command
  end interface
  !
  interface
    subroutine wifisyn_shift_parse(line,user_trans,user_shift,error)
      use gbl_message
      use wifisyn_buffer_parameters
      use wifisyn_transform_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*),   intent(inout) :: line
      type(action_t),     intent(in)    :: user_trans
      type(user_shift_t), intent(out)   :: user_shift
      logical,            intent(inout) :: error
    end subroutine wifisyn_shift_parse
  end interface
  !
  interface
    subroutine wifisyn_shift_main(user_trans,user_shift,error)
      use gbl_message
      use wifisyn_transform_types
      use wifisyn_sic_buffer
      use wifisyn_cube_buffer
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(action_t),     intent(in)    :: user_trans
      type(user_shift_t), intent(in)    :: user_shift
      logical,            intent(inout) :: error
    end subroutine wifisyn_shift_main
  end interface
  !
  interface
    subroutine wifisyn_shift_wifi_uv(idir,map,wifi,error)
      use gbl_message
      use phys_const
      use gkernel_types
      use wifisyn_transform_types
      use wifisyn_cube_types
      use wifisyn_gridding_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer,             intent(in)    :: idir
      type(map_t), target, intent(in)    :: map
      type(wifi_t),        intent(inout) :: wifi
      logical,             intent(inout) :: error
    end subroutine wifisyn_shift_wifi_uv
  end interface
  !
  interface
    subroutine wifisyn_shift_visi(idire,user,map,visi,error)
      use gbl_message
      use wifisyn_transform_types
      use wifisyn_cube_types
      use wifisyn_gridding_types
      !---------------------------------------------------------------------
      ! @ private
      !  *** JP Should we change a0 and d0???
      !---------------------------------------------------------------------
      integer,            intent(in)    :: idire
      type(user_shift_t), intent(in)    :: user
      type(map_t),        intent(in)    :: map
      type(visi_t),       intent(inout) :: visi
      logical,            intent(inout) :: error
    end subroutine wifisyn_shift_visi
  end interface
  !
  interface
    subroutine wifisyn_shift_visi_set(map,visi,idire,user,shift,error)
      use gbl_message
      use phys_const
      use wifisyn_cube_types
      use wifisyn_transform_types
      use wifisyn_gridding_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(map_t), target, intent(in)    :: map
      type(visi_t),        intent(in)    :: visi
      integer,             intent(in)    :: idire
      type(user_shift_t),  intent(in)    :: user
      type(myshift_t),     intent(out)   :: shift
      logical,             intent(inout) :: error
    end subroutine wifisyn_shift_visi_set
  end interface
  !
  interface
    subroutine wifisyn_shift_visi_phase(shift,visi,error)
      use gbl_message
      use phys_const
      use gkernel_types
      use wifisyn_transform_types
      use wifisyn_cube_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(myshift_t), intent(in)    :: shift
      type(visi_t),    intent(inout) :: visi
      logical,         intent(inout) :: error
    end subroutine wifisyn_shift_visi_phase
  end interface
  !
  interface
    subroutine wifisyn_shift_visi_grid(shift,visi,error)
      use gbl_message
      use wifisyn_transform_types
      use wifisyn_cube_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(myshift_t), intent(in)    :: shift
      type(visi_t),    intent(inout) :: visi
      logical,         intent(inout) :: error
    end subroutine wifisyn_shift_visi_grid
  end interface
  !
  interface
    subroutine wifisyn_shift_uv_head(shift,htab,error)
      use gbl_message
      use wifisyn_uv_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      ! The output table has its U and V shifted and rotated
      ! All V values are flip to be negative
      ! UVMIN and UVMAX are computed
      !----------------------------------------------------------------------
      type(shift_t),   intent(inout) :: shift
      type(uv_head_t), intent(inout) :: htab
      logical,         intent(inout) :: error
    end subroutine wifisyn_shift_uv_head
  end interface
  !
  interface
    subroutine wifisyn_shift_uv_data(shift,tabin,tabout,error)
      use gbl_message
      use wifisyn_uv_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      ! The output UV table is a rotated, phase shifted copy of
      ! the input one
      !----------------------------------------------------------------------
      type(shift_t), intent(inout) :: shift
      type(uv_t),    intent(in)    :: tabin
      type(uv_t),    intent(inout) :: tabout
      logical,       intent(inout) :: error
    end subroutine wifisyn_shift_uv_data
  end interface
  !
  interface
    subroutine wifisyn_uvsort_command(line,error)
      use wifisyn_sort_types
      !----------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !	UVSORT [U|V|X|Y]
      !----------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      logical,          intent(inout) :: error
    end subroutine wifisyn_uvsort_command
  end interface
  !
  interface
    subroutine wifisyn_sort_getargs(line,args,error)
      use gbl_message
      use gio_params
      use wifisyn_gridding_types
      use wifisyn_sort_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      character(len=*), intent(in)    :: line
      integer,          intent(out)   :: args(m_tot_key)
      logical,          intent(inout) :: error
    end subroutine wifisyn_sort_getargs
  end interface
  !
  interface
    subroutine wifisyn_set_index(ndata,sort,error)
      use gbl_message
      use wifisyn_gridding_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer,      intent(in)    :: ndata
      type(sort_t), intent(out)   :: sort
      logical,      intent(inout) :: error ! Error status
    end subroutine wifisyn_set_index
  end interface
  !
  interface
    subroutine wifisyn_sort_index(args,sort,error)
      use gbl_message
      use gio_params
      use wifisyn_gridding_types
      use wifisyn_sort_buffer
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      integer,              intent(in)    :: args(m_tot_key)
      type(sort_t), target, intent(inout) :: sort
      logical,              intent(inout) :: error
    end subroutine wifisyn_sort_index
  end interface
  !
  interface
    function wifisyn_eq(m,l)
      use wifisyn_sort_buffer
      !---------------------------------------------------------------------
      ! @ private
      ! This routine is optimized because it is called many times
      ! Do not modify
      !---------------------------------------------------------------------
      logical             :: wifisyn_eq
      integer, intent(in) :: m ! Data #m
      integer, intent(in) :: l ! Data #l
    end function wifisyn_eq
  end interface
  !
  interface
    function wifisyn_ge(m,l)
      use wifisyn_sort_buffer
      !---------------------------------------------------------------------
      ! @ private
      ! This routine is optimized because it is called many times
      ! Do not modify
      !---------------------------------------------------------------------
      logical             :: wifisyn_ge
      integer, intent(in) :: m ! Data #m
      integer, intent(in) :: l ! Data #l
    end function wifisyn_ge
  end interface
  !
  interface
    function wifisyn_gt(m,l)
      use wifisyn_sort_buffer
      !---------------------------------------------------------------------
      ! @ private
      ! This routine is optimized because it is called many times
      ! Do not modify
      !---------------------------------------------------------------------
      logical             :: wifisyn_gt
      integer, intent(in) :: m ! Data #m
      integer, intent(in) :: l ! Data #l
    end function wifisyn_gt
  end interface
  !
  interface
    subroutine wifisyn_sort_head(sort,table,error)
      use gbl_message
      use wifisyn_gridding_types
      use wifisyn_uv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(sort_t), intent(in)    :: sort
      type(uv_t),   intent(inout) :: table
      logical,      intent(inout) :: error
    end subroutine wifisyn_sort_head
  end interface
  !
  interface
    subroutine wifisyn_sort_data(sort,tabin,tabout,error)
      use gbl_message
      use wifisyn_gridding_types
      use wifisyn_uv_types
      !---------------------------------------------------------------------
      ! @ private
      !---------------------------------------------------------------------
      type(sort_t), intent(in)    :: sort
      type(uv_t),   intent(in)    :: tabin
      type(uv_t),   intent(inout) :: tabout
      logical,      intent(inout) :: error
    end subroutine wifisyn_sort_data
  end interface
  !
  interface
    subroutine wifisyn_statistics_command(line,error)
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !     STATISTICS
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical,          intent(inout) :: error
    end subroutine wifisyn_statistics_command
  end interface
  !
  interface
    subroutine wifisyn_statistics_main(error)
      use gbl_message
      use wifisyn_cube_buffer
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine wifisyn_statistics_main
  end interface
  !
  interface
    subroutine wifisyn_statistics_wifi(wifi,error)
      use gbl_message
      use wifisyn_cube_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(wifi_t), intent(inout) :: wifi
      logical,      intent(inout) :: error
    end subroutine wifisyn_statistics_wifi
  end interface
  !
  interface
    subroutine wifisyn_uvbeam_command(line,error)
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !     UVBEAM
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical,          intent(inout) :: error
    end subroutine wifisyn_uvbeam_command
  end interface
  !
  interface
    subroutine wifisyn_uvbeam_inter(error)
      use gbl_message
      use wifisyn_uv_buffer
      use wifisyn_cube_buffer
      use wifisyn_sic_buffer
      use wifisyn_transform_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine wifisyn_uvbeam_inter
  end interface
  !
  interface
    subroutine wifisyn_uvbeam_main(win,uvin,uvout,map,wifi,dirty,error)
      use gbl_message
      use gkernel_types
      use wifisyn_buffer_parameters
      use wifisyn_gridding_types
      use wifisyn_uv_types
      use wifisyn_cube_types
      use wifisyn_uvbeam_types
      use wifisyn_transform_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(window_t),      intent(in)    :: win
      type(uv_t),          intent(in)    :: uvin
      type(uv_t),          intent(inout) :: uvout
      type(map_t), target, intent(inout) :: map
      type(wifi_t),        intent(inout) :: wifi
      type(dirty_t),       intent(inout) :: dirty
      logical,             intent(inout) :: error
    end subroutine wifisyn_uvbeam_main
  end interface
  !
  interface
    subroutine wifisyn_reallocate_dirty_beam(map,dirty,error)
      use gbl_message
      use wifisyn_gridding_types
      use wifisyn_cube_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(map_t),   intent(in)    :: map
      type(dirty_t), intent(inout) :: dirty
      logical,       intent(inout) :: error
    end subroutine wifisyn_reallocate_dirty_beam
  end interface
  !
  interface
    subroutine wifisyn_wifi_dirty_header(map,dirty,error)
      use gbl_message
      use wifisyn_gridding_types
      use wifisyn_cube_types
      !----------------------------------------------------------------------
      ! @ private
      ! Should be merged with wifi_header *** JP
      !----------------------------------------------------------------------
      type(map_t),   intent(in)    :: map
      type(dirty_t), intent(inout) :: dirty
      logical,       intent(inout) :: error
    end subroutine wifisyn_wifi_dirty_header
  end interface
  !
  interface
    subroutine wifisyn_uvbeam_fill_uv(rchan,fwhm,sou,uv,error)
      use gbl_message
      use phys_const
      use gkernel_types
      use wifisyn_uv_types
      use wifisyn_gridding_types
      use wifisyn_uvbeam_types
      !----------------------------------------------------------------------
      ! @ private
      ! Note: This looks like the computation of clean component visibilities...
      !----------------------------------------------------------------------
      type(channel_t),     intent(in)    :: rchan
      type(scale_pairs_t), intent(in)    :: fwhm
      type(source_t),      intent(in)    :: sou
      type(uv_t),          intent(inout) :: uv
      logical,             intent(inout) :: error
    end subroutine wifisyn_uvbeam_fill_uv
  end interface
  !
  interface
    subroutine wifisyn_uvgrid_command(line,error)
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !    UVGRID
      !       1. /SHIFT
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical,          intent(inout) :: error
    end subroutine wifisyn_uvgrid_command
  end interface
  !
  interface
    subroutine wifisyn_uvgrid_inter(doshift,error)
      use gbl_message
      use wifisyn_uv_buffer
      use wifisyn_cube_buffer
      use wifisyn_sic_buffer
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      logical, intent(in)    :: doshift
      logical, intent(inout) :: error
    end subroutine wifisyn_uvgrid_inter
  end interface
  !
  interface
    subroutine wifisyn_uvgrid_main(doshift,win,uvin,uvout,map,wifi,error)
      use gbl_message
      use wifisyn_gridding_types
      use wifisyn_uv_types
      use wifisyn_cube_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      logical,        intent(in)    :: doshift
      type(window_t), intent(in)    :: win
      type(uv_t),     intent(in)    :: uvin
      type(uv_t),     intent(inout) :: uvout
      type(map_t),    intent(inout) :: map
      type(wifi_t),   intent(inout) :: wifi
      logical,        intent(inout) :: error
    end subroutine wifisyn_uvgrid_main
  end interface
  !
  interface
    subroutine wifisyn_reallocate_wifi(map,wifi,error)
      use gbl_message
      use wifisyn_gridding_types
      use wifisyn_cube_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(map_t),  intent(in)    :: map
      type(wifi_t), intent(inout) :: wifi
      logical,      intent(inout) :: error
    end subroutine wifisyn_reallocate_wifi
  end interface
  !
  interface
    subroutine wifisyn_wifi_weight_header(map,wifi,error)
      use gbl_message
      use wifisyn_gridding_types
      use wifisyn_cube_types
      !----------------------------------------------------------------------
      ! @ private
      ! Should be merged with wifi_header *** JP
      !----------------------------------------------------------------------
      type(map_t),  intent(in)    :: map
      type(wifi_t), intent(inout) :: wifi
      logical,      intent(inout) :: error
    end subroutine wifisyn_wifi_weight_header
  end interface
  !
  interface
    subroutine wifisyn_uvgrid_sort_and_weight(uvin,uvout,map,error)
      use gbl_message
      use wifisyn_uv_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(uv_t),  intent(in)    :: uvin
      type(uv_t),  intent(inout) :: uvout
      type(map_t), intent(inout) :: map
      logical,     intent(inout) :: error
    end subroutine wifisyn_uvgrid_sort_and_weight
  end interface
  !
  interface
    subroutine wifisyn_uvgrid_shift_sort_transpose_data(win,shift,sort,tabin,tabout,error)
      use gbl_message
      use wifisyn_uv_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      ! The output visibilities are a rotated, phase shifted, transposed, sorted
      ! copy of the input ones. This is the sorting and the transposition
      ! which makes it different from wifisyn_shift_uv_data. All the operations
      ! are grouped here to avoid copying 3 times the same data... Note that
      ! only the visibility are changed, the header data (U,V,X,Y,...)
      ! stays untouched. Weights are not carried over.
      !----------------------------------------------------------------------
      type(window_t),   intent(in)    :: win
      type(shift_t),    intent(inout) :: shift
      type(sort_t),     intent(inout) :: sort
      type(uv_t),       intent(in)    :: tabin
      type(uv_t),       intent(inout) :: tabout
      logical,          intent(inout) :: error
    end subroutine wifisyn_uvgrid_shift_sort_transpose_data
  end interface
  !
  interface
    subroutine wifisyn_uvmap_command(line,error)
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical,          intent(inout) :: error
    end subroutine wifisyn_uvmap_command
  end interface
  !
  interface
    subroutine wifisyn_uvmap_inter(error)
      use gbl_message
      use wifisyn_uv_buffer
      use wifisyn_cube_buffer
      use wifisyn_sic_buffer
      use wifisyn_transform_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine wifisyn_uvmap_inter
  end interface
  !
  interface
    subroutine wifisyn_uvmap_pipe(error)
      use gbl_message
      use wifisyn_uv_buffer
      use wifisyn_cube_buffer
      use wifisyn_sic_buffer
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      logical, intent(inout) :: error
    end subroutine wifisyn_uvmap_pipe
  end interface
  !
  interface
    subroutine wifisyn_uvmap_cube_header(map,huv,itype,cube,error)
      use gbl_message
      use wifisyn_buffer_parameters
      use wifisyn_uv_types
      use wifisyn_gridding_types
      !-----------------------------------------------------
      ! @ private
      ! Set beam or dirty header
      ! Note: Contrary to MAPPING, we here copy the projection information also
      !       in the beam header.
      !-----------------------------------------------------
      type(map_t),     intent(in)    :: map
      type(uv_head_t), intent(in)    :: huv
      integer,         intent(in)    :: itype ! cube type (beam or dirty)
      type (gildas),   intent(inout) :: cube
      logical,         intent(inout) :: error
    end subroutine wifisyn_uvmap_cube_header
  end interface
  !
  interface
    subroutine wifisyn_uvsym_command(line,error)
      use gbl_message
      use wifisyn_cube_buffer
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical,          intent(inout) :: error
    end subroutine wifisyn_uvsym_command
  end interface
  !
  interface
    subroutine wifisyn_uvsym_check_wifi(wifi,error)
      use gbl_message
      use wifisyn_cube_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(wifi_t), intent(inout) :: wifi
      logical,      intent(inout) :: error
    end subroutine wifisyn_uvsym_check_wifi
  end interface
  !
  interface
    subroutine wifisyn_uvsym_check_visi(visi,error)
      use gbl_message
      use wifisyn_cube_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(visi_t), intent(inout) :: visi
      logical,      intent(inout) :: error
    end subroutine wifisyn_uvsym_check_visi
  end interface
  !
  interface
    subroutine wifisyn_uvswap_command(error)
      use wifisyn_uv_buffer
      !-------------------------------------------------------------------
      ! @ private
      !-------------------------------------------------------------------
      logical, intent(out) :: error
    end subroutine wifisyn_uvswap_command
  end interface
  !
  interface
    subroutine wifisyn_uvrt_sicdef(uvbuffer,uvname,error)
      use gbl_message
      use wifisyn_uv_buffer
      !-------------------------------------------------------------------
      ! @ private
      !-------------------------------------------------------------------
      type(uv_t),       intent(inout) :: uvbuffer
      character(len=3), intent(in)    :: uvname
      logical,          intent(out)   :: error
    end subroutine wifisyn_uvrt_sicdef
  end interface
  !
  interface
    subroutine wifisyn_free_uv(uv,error)
      use gbl_message
      use gio_params
      use wifisyn_uv_buffer
      !-------------------------------------------------------------------
      ! @ private
      ! Free a UV type
      ! Must not touch the Header, which may have been previously defined
      !-------------------------------------------------------------------
      type(uv_t), intent(inout) :: uv
      logical,    intent(inout) :: error
    end subroutine wifisyn_free_uv
  end interface
  !
  interface
    subroutine wifisyn_free_uvrt(error)
      use gbl_message
      use wifisyn_uv_buffer
      !-------------------------------------------------------------------
      ! @ private
      ! Free the UVRT buffers
      !-------------------------------------------------------------------
      logical,    intent(inout) :: error
    end subroutine wifisyn_free_uvrt
  end interface
  !
  interface
    subroutine wifisyn_reallocate_uvrt(ndata,nchan,order,error)
      use gbl_message
      use wifisyn_uv_buffer
      !-------------------------------------------------------------------
      ! @ private
      ! (Re)allocate pointers and targets for the UVR and UVT buffers
      ! The headers are not modified at all
      ! These buffers are always in the TUV order
      !-------------------------------------------------------------------
      integer,    intent(in)    :: ndata
      integer,    intent(in)    :: nchan
      integer,    intent(in)    :: order ! uvt or tuv
      logical,    intent(out)   :: error
    end subroutine wifisyn_reallocate_uvrt
  end interface
  !
  interface
    subroutine wifisyn_reallocate_uv_header(uv,ndata,error)
      use gbl_message
      use wifisyn_uv_buffer
      !-------------------------------------------------------------------
      ! @ private
      ! (Re)allocate uv header arrays
      ! The headers are not modified at all
      !-------------------------------------------------------------------
      type(uv_t), intent(inout) :: uv
      integer,    intent(in)    :: ndata
      logical,    intent(out)   :: error
    end subroutine wifisyn_reallocate_uv_header
  end interface
  !
  interface
    subroutine wifisyn_reallocate_uv_data(uv,ndata,nchan,order,error)
      use gbl_message
      use wifisyn_uv_buffer
      !-------------------------------------------------------------------
      ! @ private
      ! (Re)allocate uv data arrays
      ! Do nothing when the array sizes did not changed
      !-------------------------------------------------------------------
      type(uv_t), intent(inout) :: uv
      integer,    intent(in)    :: ndata
      integer,    intent(in)    :: nchan
      integer,    intent(in)    :: order ! uvt or tuv
      logical,    intent(out)   :: error
    end subroutine wifisyn_reallocate_uv_data
  end interface
  !
  interface
    subroutine wifisyn_reallocate_uv(uv,ndata,nchan,order,error)
      use gbl_message
      use wifisyn_uv_buffer
      !-------------------------------------------------------------------
      ! @ private
      ! (Re)allocate uv header and data arrays
      !-------------------------------------------------------------------
      type(uv_t), intent(inout) :: uv
      integer,    intent(in)    :: ndata
      integer,    intent(in)    :: nchan
      integer,    intent(in)    :: order ! tuv or uvt
      logical,    intent(out)   :: error
    end subroutine wifisyn_reallocate_uv
  end interface
  !
  interface
    subroutine wifisyn_read_uv_header(do,uv,ndata,nchan,nc,error)
      use gbl_message
      use wifisyn_readwrite_types
      use wifisyn_uv_types
      !------------------------------------------------------------------
      ! @ private
      ! Read input UV header
      !------------------------------------------------------------------
      type(readwrite_t), intent(in)    :: do
      type(uv_t),        intent(inout) :: uv
      integer,           intent(out)   :: ndata
      integer,           intent(out)   :: nchan
      integer,           intent(out)   :: nc(2)
      logical,           intent(inout) :: error
    end subroutine wifisyn_read_uv_header
  end interface
  !
  interface
    subroutine wifisyn_read_uv_data(uv,ndata,nchan,nc,error)
      use gbl_message
      use wifisyn_uv_types
      !-------------------------------------------------------------------
      ! @ private
      ! Read uv data
      !-------------------------------------------------------------------
      type(uv_t), intent(inout) :: uv
      integer,    intent(in)    :: ndata
      integer,    intent(in)    :: nchan
      integer,    intent(in)    :: nc(2)
      logical,    intent(out)   :: error
    end subroutine wifisyn_read_uv_data
  end interface
  !
  interface
    subroutine wifisyn_write_uv_header(do,uv,error)
      use gbl_message
      use gildas_def
      use wifisyn_readwrite_types
      use wifisyn_uv_types
      !------------------------------------------------------------------
      ! @ private
      ! Write UV header
      !------------------------------------------------------------------
      type(readwrite_t), intent(in)    :: do
      type(uv_t),        intent(inout) :: uv
      logical,           intent(inout) :: error
    end subroutine wifisyn_write_uv_header
  end interface
  !
  interface
    subroutine wifisyn_write_uv_data(do,uv,error)
      use gbl_message
      use wifisyn_readwrite_types
      use wifisyn_uv_types
      !-------------------------------------------------------------------
      ! @ private
      ! Write uv data
      !-------------------------------------------------------------------
      type(readwrite_t), intent(in)    :: do
      type(uv_t),        intent(inout) :: uv
      logical,           intent(inout) :: error
    end subroutine wifisyn_write_uv_data
  end interface
  !
  interface
    subroutine wifisyn_write_uv(do,uv,error)
      use gbl_message
      use gildas_def
      use wifisyn_readwrite_types
      use wifisyn_uv_types
      !------------------------------------------------------------------
      ! @ private
      ! Read input UV header
      !------------------------------------------------------------------
      type(readwrite_t), intent(in)    :: do
      type(uv_t),        intent(inout) :: uv
      logical,           intent(inout) :: error
    end subroutine wifisyn_write_uv
  end interface
  !
  interface
    subroutine wifisyn_copy_uv_header(in,out,error)
      use gbl_message
      use wifisyn_uv_types
      !------------------------------------------------------------------
      ! @ private
      ! Copy header information
      !------------------------------------------------------------------
      type(uv_t), intent(in)    :: in
      type(uv_t), intent(inout) :: out
      logical,    intent(inout) :: error
    end subroutine wifisyn_copy_uv_header
  end interface
  !
  interface
    subroutine wifisyn_variable_command(line,error)
      use gbl_message
      use wifisyn_parameters
      !----------------------------------------------------------------------
      ! @ private
      ! Support routine for command
      !    VARIABLE
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical,          intent(inout) :: error
    end subroutine wifisyn_variable_command
  end interface
  !
  interface
    subroutine wifisyn_weighting_extract_sort(wchan,tabin,sort,weight,error)
      use gbl_message
      use wifisyn_uv_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      ! Extract the used weights from the input data
      !----------------------------------------------------------------------
      integer,        intent(in)    :: wchan
      type(uv_t),     intent(in)    :: tabin
      type(sort_t),   intent(in)    :: sort
      type(weight_t), intent(out)   :: weight
      logical,        intent(inout) :: error
    end subroutine wifisyn_weighting_extract_sort
  end interface
  !
  interface
    subroutine wifisyn_weighting_taper(htab,taper,modwei,error)
      use gbl_message
      use phys_const
      use wifisyn_uv_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      ! Taper the weights of the visibility points
      !----------------------------------------------------------------------
      type(uv_head_t), intent(in)    :: htab
      type(taper_t),   intent(in)    :: taper
      type(real_1d_t), intent(inout) :: modwei
      logical,         intent(inout) :: error
    end subroutine wifisyn_weighting_taper
  end interface
  !
  interface
    subroutine wifisyn_weighting_noise(weight,noise,error)
      use gbl_message
      use wifisyn_uv_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      ! Compute and print natural and weighted/tapered noise levels
      ! The formula used to compute the weighted/tapered noise comes from
      ! Thompson et al. Interferometry and Synthesis in Radio Astronomy
      ! (section "Response to the noise")
      !----------------------------------------------------------------------
      type(weight_t),  intent(in)    :: weight
      type(noise_t),   intent(out)   :: noise
      logical,         intent(inout) :: error
    end subroutine wifisyn_weighting_noise
  end interface
  !
  interface
    subroutine wifisyn_weighting_print_noise(cname,wmode,wsum,noise)
      use gbl_message
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(in)  :: cname
      character(len=*), intent(in)  :: wmode
      real(kind=8),     intent(in)  :: wsum
      real(kind=4),     intent(out) :: noise
    end subroutine wifisyn_weighting_print_noise
  end interface
  !
  interface
    subroutine wifisyn_wifi2visi_command(line,error)
      use gbl_message
      use wifisyn_sic_buffer
      use wifisyn_cube_buffer
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical,          intent(inout) :: error
    end subroutine wifisyn_wifi2visi_command
  end interface
  !
  interface
    subroutine wifisyn_wifi2visi_main(map,wifi,visi,error)
      use gbl_message
      use wifisyn_cube_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(map_t),     intent(in)    :: map
      type(wifi_t),    intent(in)    :: wifi
      type(visi_t),    intent(inout) :: visi
      logical,         intent(inout) :: error
    end subroutine wifisyn_wifi2visi_main
  end interface
  !
  interface
    subroutine wifisyn_visi_wei_header(map,visi,error)
      use gbl_message
      use wifisyn_gridding_types
      use wifisyn_cube_types
      !----------------------------------------------------------------------
      ! @ private
      ! This one should be merged with wifisyn_wifi_header *** JP
      !----------------------------------------------------------------------
      type(map_t),     intent(in)    :: map
      type(visi_t),    intent(inout) :: visi
      logical,         intent(inout) :: error
    end subroutine wifisyn_visi_wei_header
  end interface
  !
  interface
    subroutine wifisyn_wifi2visi_average(axes,wifi,visi,error)
      use gbl_message
      use wifisyn_cube_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(axis_t), intent(in)    :: axes(2)
      type(wifi_t), intent(in)    :: wifi
      type(visi_t), intent(inout) :: visi
      logical,      intent(inout) :: error
    end subroutine wifisyn_wifi2visi_average
  end interface
  !
  interface
    subroutine wifisyn_wifi2visi_average_and_symmetry(axes,wifi,visi,error)
      use gbl_message
      use wifisyn_cube_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(axis_t), intent(in)    :: axes(2)
      type(wifi_t), intent(in)    :: wifi
      type(visi_t), intent(inout) :: visi
      logical,      intent(inout) :: error
    end subroutine wifisyn_wifi2visi_average_and_symmetry
  end interface
  !
  interface
    subroutine wifisyn_wifi2visi_shift_and_sum(axes,wifi,visi,error)
      use gbl_message
      use wifisyn_cube_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(axis_t), intent(in)    :: axes(2)
      type(wifi_t), intent(in)    :: wifi
      type(visi_t), intent(inout) :: visi
      logical,      intent(inout) :: error
    end subroutine wifisyn_wifi2visi_shift_and_sum
  end interface
  !
  interface
    subroutine wifisyn_wifi2visi_sum(axes,wifi,visi,error)
      use gbl_message
      use wifisyn_cube_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      ! *** JP This requires the same us and uf axes to actually work...
      !----------------------------------------------------------------------
      type(axis_t), intent(in)    :: axes(2)
      type(wifi_t), intent(in)    :: wifi
      type(visi_t), intent(inout) :: visi
      logical,      intent(inout) :: error
    end subroutine wifisyn_wifi2visi_sum
  end interface
  !
  interface
    subroutine wifisyn_write_command(line,error)
      use gbl_message
      use wifisyn_readwrite_types
      !----------------------------------------------------------------------
      ! @ private
      ! Support routine for command WRITE
      !----------------------------------------------------------------------
      character(len=*), intent(inout) :: line
      logical,          intent(inout) :: error
    end subroutine wifisyn_write_command
  end interface
  !
  interface
    subroutine wifisyn_write_main(do,error)
      use gbl_message
      use wifisyn_readwrite_types
      use wifisyn_sic_buffer
      use wifisyn_uv_buffer
      use wifisyn_cube_buffer
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(readwrite_t), intent(inout) :: do
      logical,           intent(inout) :: error
    end subroutine wifisyn_write_main
  end interface
  !
  interface
    subroutine wifisyn_uvgrid_convolve(win,map,visitab,wifi,error)
      use gbl_message
      use phys_const
      use gkernel_types
      use wifisyn_uv_types
      use wifisyn_cube_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(window_t),         intent(in)    :: win
      type(map_t),    target, intent(in)    :: map
      type(uv_t),             intent(in)    :: visitab
      type(wifi_t),           intent(inout) :: wifi
      logical,                intent(inout) :: error
    end subroutine wifisyn_uvgrid_convolve
  end interface
  !
  interface
    subroutine wifisyn_uvgrid_shift(win,map,visitab,wifi,error)
      use gbl_message
      use phys_const
      use gkernel_types
      use wifisyn_uv_types
      use wifisyn_cube_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(window_t),         intent(in)    :: win
      type(map_t),    target, intent(in)    :: map
      type(uv_t),             intent(in)    :: visitab
      type(wifi_t),           intent(inout) :: wifi
      logical,                intent(inout) :: error
    end subroutine wifisyn_uvgrid_shift
  end interface
  !
  interface
    subroutine wifisyn_uvgrid_shift_unshift(win,map,visitab,wifi,error)
      use gbl_message
      use phys_const
      use gkernel_types
      use wifisyn_uv_types
      use wifisyn_cube_types
      use wifisyn_gridding_types
      !----------------------------------------------------------------------
      ! @ private
      !----------------------------------------------------------------------
      type(window_t),         intent(in)    :: win
      type(map_t),    target, intent(in)    :: map
      type(uv_t),             intent(in)    :: visitab
      type(wifi_t),           intent(inout) :: wifi
      logical,                intent(inout) :: error
    end subroutine wifisyn_uvgrid_shift_unshift
  end interface
  !
end module wifisyn_interfaces_private
