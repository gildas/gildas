!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_write_command(line,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_write_command
  use wifisyn_readwrite_types
  !----------------------------------------------------------------------
  ! @ private
  ! Support routine for command WRITE
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  logical,          intent(inout) :: error
  !
  type(readwrite_t) :: do
  character(len=*), parameter :: rname='WRITE/COMMAND'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call wifisyn_parse_readwrite(line,do,error)
  if (error) return
  call wifisyn_write_main(do,error)
  if (error) return
  !
end subroutine wifisyn_write_command
!
subroutine wifisyn_write_main(do,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_write_main
  use wifisyn_readwrite_types
  use wifisyn_sic_buffer
  use wifisyn_uv_buffer
  use wifisyn_cube_buffer
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(readwrite_t), intent(inout) :: do
  logical,           intent(inout) :: error
  !
  character(len=*), parameter :: rname = 'WRITE/MAIN'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! Dispatch according to type
  if (do%buffer.eq.code_buffer_beam) then
!     call wifisyn_cube_write(do,beam,error)
     if (error) return
  else if (do%buffer.eq.code_buffer_dirty) then
!     call wifisyn_cube_write(do,dirty,error)
     if (error) return
  else if (do%buffer.eq.code_buffer_visi) then
     call wifisyn_write_visi(do,progmap,visi,error)
     if (error) return
  else if (do%buffer.eq.code_buffer_wifi) then
     call wifisyn_write_wifi(do,progmap,wifi,error)
     if (error) return
  else if (do%buffer.eq.code_buffer_uv) then
     call wifisyn_write_uv(do,uvr,error)
     if (error) return
  else
     call wifisyn_message(seve%e,'WRITE','Writing of '//do%kind//' not yet implemented')
     error = .true.
     return
  endif
  !
end subroutine wifisyn_write_main
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
