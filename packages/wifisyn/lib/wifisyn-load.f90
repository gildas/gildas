!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
subroutine wifisyn_load_command(line,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_load_command
  use wifisyn_load_types
  !----------------------------------------------------------------------
  ! @ private
  ! Support routine for command LOAD
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  logical,          intent(inout) :: error
  !
  type(load_t) :: do
  character(len=*), parameter :: rname='LOAD/COMMAND'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  call wifisyn_load_parse(line,do,error)
  if (error) return
  call wifisyn_load_main(do,error)
  if (error) return
  !
end subroutine wifisyn_load_command
!
subroutine wifisyn_load_parse(line,do,error)
  use gbl_message
  use gkernel_interfaces
  use wifisyn_interfaces, except_this=>wifisyn_load_parse
  use wifisyn_load_types
  !----------------------------------------------------------------------
  ! @ private
  ! Parsing routine for command
  !    LOAD buffer [varname]
  !      1. [/AMPLITUDE [LOGSCALE | LINSCALE]]
  !      2. [/NORMALIZE]
  !----------------------------------------------------------------------
  character(len=*), intent(inout) :: line
  type(load_t),     intent(inout) :: do
  logical,          intent(inout) :: error
  !
  integer, parameter :: iload = 0
  integer, parameter :: iamplitude = 1
  integer, parameter :: inormalize = 2
  !
  integer, parameter :: ibuffer = 1
  integer, parameter :: ivarname = 2
  !
  character(len=*), parameter :: rname='LOAD/PARSE'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  do%normalize = sic_present(inormalize,0)
  call wifisyn_parse_buffer(line,iload,ibuffer,do%buffer,do%kind,do%type,error)
  if (error) return
  call wifisyn_parse_varname(line,iload,ivarname,do%kind,do%name,error)
  if (error) return
  call wifisyn_parse_cube_amplitude(line,iamplitude,do%amplitude,do%logscale,error)
  if (error) return
  !
end subroutine wifisyn_load_parse
!
subroutine wifisyn_load_main(do,error)
  use gbl_message
  use wifisyn_interfaces, except_this=>wifisyn_load_main
  use wifisyn_load_types
  use wifisyn_cube_buffer
  !----------------------------------------------------------------------
  ! @ private
  !----------------------------------------------------------------------
  type(load_t), intent(in)    :: do
  logical,      intent(inout) :: error
  !
  character(len=*), parameter :: rname='LOAD/MAIN'
  !
  call wifisyn_message(seve%t,rname,'Welcome')
  !
  ! Dispatch according to type
  if (do%buffer.eq.code_buffer_visi) then
     call wifisyn_cube_sicdef(code_readonly,do%name,visi%amp,error)
     if (error) return
  else if (do%buffer.eq.code_buffer_weight) then
     call wifisyn_cube_sicdef(code_readonly,do%name,visi%wei,error)
     if (error) return
  else if (do%buffer.eq.code_buffer_beam) then
     call wifisyn_cube_sicdef(code_readonly,do%name,dirty%beam,error)
     if (error) return
  else if (do%buffer.eq.code_buffer_dirty) then
     call wifisyn_cube_sicdef(code_readonly,do%name,dirty%image,error)
     if (error) return
  else
     call wifisyn_message(seve%e,rname,'Loading of '//do%kind//' not yet implemented')
     error = .true.
     return
  endif
  !
end subroutine wifisyn_load_main
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
