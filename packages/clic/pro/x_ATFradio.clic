!---*-f90-*--------------------------------------------------------------------
begin procedure get_fits
!
! Get FITS data
!
  define char name*80
  file both dummy
  file out 'rad_filename'
  for i rad_scan[1] to rad_scan[2]
    if i.lt.10 then
       let name "radiodata:TEST000"'i'".FITS"
    else if i.lt.100 then
       let name "radiodata:TEST00"'i'".FITS"
    else if i.lt.1000 then
       let name "radiodata:TEST0"'i'".FITS"
    else
       let name "radiodata:TEST"'i'".FITS"
    end if
    if file(name) then
      say "file " 'name' " exists"
      read 'name'
    else
      say "file " 'name' " not found"
    endif
  next
end procedure   get_fits
!-----------------------------------------------------------
begin procedure concat
  symbol &1 "&2 &3 &4 &5 &6 &7 &8 &9"
end procedure concat
!-----------------------------------------------------------
begin procedure reduce_scan
!
! Data reduction
!
   define logical store
   let store &1
   say OK
!   file both dummy
   say OK
   file in  'rad_filename'
   say OK
   set line 'rad_line'
   symb s 'rad_cproc'
   find /scan rad_scan[1] rad_scan[2] /proc 's'
   say OK
   delete /symbol s
   if found.eq.0 then
      return
   endif 
   get f
   if .not.exist(procedure) then
      var general
   endif
   if .not.exist(source) then
      var position
   endif
   if (rad_aver_time) then
      set averag time 'rad_time'
   else
      set averag none
   endif
   on error return
   if (procedure.eq."POIN") then
!
! FIVE is now at the ATF a Total power focus....
!
!!   if (procedure.eq."POIN").or.(procedure.eq."FIVE") then
     if rad_fix_width.or.rad_fix_zero then
        symbol sym_fix "/fix"
     else
        symbol sym_fix " "
     endif
     if (rad_fix_width) then
        symbol sym_fixw "width "'rad_width'
        say "Zero Level Fixed to 0"
     else
        symbol sym_fixw " "
     endif
     if (rad_fix_zero) then
       symbol sym_fix0 "zero 0.0"
        say "Zero Level Fixed to 0"
     else
       symbol sym_fix0 " "
     endif
     if store then
        say 'file(rad_point_filename)'
        if file(rad_point_filename) then
           symbol sym_point "/output "'rad_point_filename'
        else
           symbol sym_point "/output "'rad_point_filename'" new"
        endif
        say "Result stored in "'rad_point_filename'
        symbol sym_point
     else
        symbol sym_point " "
     endif
     solve point /total /plot 'sym_fix' 'sym_fix0' 'sym_fixw' 'sym_point'
     delete /symbol sym_fix sym_fix0 sym_fixw sym_point
     say "Pointing corrections used:"
     list /short /var source coll_az[1] coll_el[1]
     if n_group.gt.1 then
        say "** Data should not be combined! **"
     endif
     
   else if (procedure.eq."FOCU").or.(procedure.eq."FIVE") then
     !
     ! this is a focus measurement...
     !
     @ reduce_TPfocus
   else if procedure.eq."ONOF" then
      !
      ! this is a beam map ...
      !
      if nswitch[1].gt.0 then
         define char map_file*80
         let map_file "map-"'rad_scan[1]'
         if rad_scan[2].ne.rad_scan[1] then
            let map_file 'map_file'"-"'rad_scan[2]'
         end if
         file out 'map_file' new
         clic\compress .096
         file in 'map_file'
         find 
         list /short
         grid /plot
         file   both 'rad_filename'
         sic delete 'map_file'".hpb"
      else
         list /short
         grid 200 -9.2 /plot 
      end if
   endif
end procedure  reduce_scan
!-----------------------------------------------------------
begin procedure reduce_TPfocus
   if (.not.exist(frequency)) then
      variable rf
   endif
   define integer nsub
   define logical fx fy fz
   define real plate w
   let plate 33.9 ! " / mm
   let fz procedure.eq."FIVE"
   if .not.fz then
      let fx scan_type.eq.11
      let fy scan_type.eq.12
   endif
!   let fz .true.
!   let fy .false.
!   let fx .false.

!
! Data reduction
!
   if (rad_fix_zero) then
      symbol sym_fix0 "/fix zero 0.0"
      say "Zero Level Fixed to 0"
   else
      symbol sym_fix0 " "
   endif
! with nutator 
   if nswitch[1].gt.0 then
      solve focus /total /plot 'sym_fix0'
      if (fx) then
         say rad_scan[1] rad_scan[2]  azimuth*180|pi elevation*180|pi -
         f_zfocus[1] e_zfocus[1] ambiant_t 'time_observed' 'date_observed' -
         "! FOCUS" 'source' -
         /format i6 i6 f7.1 f5.1 f7.3 f6.3 f6.1 a13 a12 a7 a12 
      else if (fy) then
         say rad_scan[1] rad_scan[2]  azimuth*180|pi elevation*180|pi -
         f_xfocus[1] e_xfocus[1] ambiant_t 'time_observed' 'date_observed' -
         "! XFOCUS" 'source' -
         /format i6 i6 f7.1 f5.1 f7.3 f6.3 f6.1 a13 a12 a7 a12 
      else if (fz) then
         say rad_scan[1] rad_scan[2]  azimuth*180|pi elevation*180|pi -
         f_yfocus[1] e_yfocus[1] ambiant_t 'time_observed' 'date_observed' -
         "! YFOCUS" 'source' -
         /format i6 i6 f7.1 f5.1 f7.3 f6.3 f6.1 a13 a12 a7 a12 
      endif
   else
      ! without nutator: solve each subscan as a pointing
      let itype scan_type /new integer
      let itype 12
      let nsub found
!
      define integer sub[nsub] 
      define real xm[nsub] ym[nsub] em[nsub] 
      def real xx[1000] yy[1000] ff[5]
      define character axis*1
!
      let sub cx_num
      let ff[i] (i-3)*0.5 ! check
      for i 1 to nsub
         fin /num sub[i]
         if (fy) then
            solve pointing /tot /plot /fix az 0
            let xm[i] -f_el[1]|plate
            let ym[i] f_peak[1]
            let em[i] e_peak[1]
            let axis "Y"
         else if (fx) then
            solve pointing /tot /plot /fix el 0
            let xm[i] f_az[1]|plate
            let ym[i] f_peak[1]
            let em[i] e_peak[1]
            let axis "X"
         else if (fz) then
            solve pointing /tot /plot /fix az 0
            let xm[i] ff[i]
            let ym[i] f_peak[1]
            let em[i] e_peak[1]
            let axis "Z"
         end if
      next
      exa xm
      exa ym
      clea plo
      pen 0
      lim * *  0 * /var xm ym
      box
      label 'axis'"-Focus" /x
      label "Intensity" /y

      point xm ym
      err y  xm ym em 
      let em 1.|em**2
      if fz then
         mfit ym=&a|(1.+4.*(Xm-&b)**2|&c**2) /start 10,0.0,2 
      else
         mfit ym=&a|(1.+4.*(Xm-&b)**2|&c**2) /start 10,0.0,15 
      endif
      let xx[i] -10+0.02*i
      let yy mfit%par[1]|(1.+4.*(xx-mfit%par[2])**2|mfit%par[3]**2)
      pen 1
      conn xx yy
      if (fz) then
         !
         ! in Z
         !
         say "-------------------"
         say "Z focus " mfit%par[2] " mm" /format a f6.2 a
         say "-------------------"
         say rad_scan[1] rad_scan[2]  azimuth*180|pi elevation*180|pi -
         mfit%par[2] 0 ambiant_t 'time_observed' 'date_observed' -
         "! FOCUS" 'source' -
         /format i6 i6 f7.1 f5.1 f7.3 f6.3 f6.1 a13 a12 a7 a12 
      else if (fx) then
         !
         ! in X
         !
         say "-------------------"
         say "X focus " mfit%par[2] " mm" /format a f6.2 a
         say "Pointing: " mfit%par[2]*plate " 0 ''" /format a f6.1 a
         say "-------------------"
         say rad_scan[1] rad_scan[2]  azimuth*180|pi elevation*180|pi -
         mfit%par[2] 0 ambiant_t 'time_observed' 'date_observed' -
         "! XFOCUS" 'source' -
         /format i6 i6 f7.1 f5.1 f7.3 f6.3 f6.1 a13 a12 a7 a12 
      else if (fy) then
         !
         ! in Y
         !
         say "-------------------"
         say "Y focus : " mfit%par[2] " mm" /format a f6.2 a
         say "Pointing: 0 " -mfit%par[2]*plate "''" /format a f6.1 a
         say "-------------------"
         say rad_scan[1] rad_scan[2]  azimuth*180|pi elevation*180|pi -
         mfit%par[2] 0 ambiant_t 'time_observed' 'date_observed' -
         "! YFOCUS(TP)" 'source' -
          /format i6 i6 f7.1 f5.1 f7.3 f6.3 f6.1 a13 a12 a7 a12 
      end if
   end if
   delete /symbol sym_fix0
end procedure  reduce_TPfocus
!-----------------------------------------------------------
begin procedure bad_scan
!
! Data reduction
!
   file both dummy
   file   both 'rad_filename'
   set line 'rad_line'
   symb s 'rad_cproc'
   find /scan rad_scan[1] rad_scan[2] /proc 's'
   delete /symbol s
!   find /scan rad_scan[1] rad_scan[2] /proc 'rad_cproc'
   if found.eq.0 then
      return
   endif
   store quality bad 
end procedure  bad_scan
!-----------------------------------------------------------
!begin procedure get_file
!end procedure  get_file
!-----------------------------------------------------------
begin procedure get_scan
  let increment &1 /new integer
  let ok no /new logical
  set level 8
  file both dummy
  file   both 'rad_filename'
  set line 'rad_line'
  find
  if (found.le.0) then
    return
  endif
  get cx_num[1]
  let s1 scan /new integer
  get cx_num[found]
  let s2 scan /new integer 
  symb s 'rad_cproc'
  let rad_Scan max(rad_Scan[1],s1)
  let rad_Scan min(rad_Scan[1],s2)
  let rad_Scan 'rad_Scan[1]+increment'
  find /line dummy
  for /while .not.ok.and.rad_scan[1].gt.s1.and.rad_scan[1].lt.s2
      find /scan rad_scan[1] rad_scan[2] /proc 's'
      let ok found.gt.0 
      if (.not.ok) then
         let rad_Scan 'rad_Scan[1]+increment'
      endif
  next
  if (found.gt.0) then
       list /short
  else
       say "No "'rad_cproc'" found"
  endif
  set level 1
  delete /symbol s
end   procedure get_scan
!-----------------------------------------------------------
if .not.exist(rad_filename) then
  define char rad_filename*80 rad_point_filename*80 rad_cproc*80 /global
  define char rad_line*12 /global
  define integer rad_scan[2] /global
  define logical rad_fix_width rad_aver_time rad_point_file rad_fix_zero -
  /global
  define real rad_width rad_time /global
  let rad_filename "radio-2004-mm-dd"
  let rad_scan 0 
  let rad_line F265P2 ! F095P1
  let rad_fix_width no
  let rad_fix_zero no
  let rad_aver_time no
  let rad_point_file no
  let rad_width 80.
  let rad_time 1.
  let rad_cproc "POIN FIVE FOCU"
endif
if .not.file("dummy.hpb") then
  file out dummy new
endif
set quality average
var general
var position
var scanning
var atmos

gui\panel "Calibration package for ATF Radiometry (R.Lucas)" -
    x_ATFradio.hlp
let rad_Scan 'rad_Scan[1]'  'rad_Scan[2]' /PROMPT "First and Last Scan Numbers?" 
let rad_cproc 'rad_cproc' /PROMPT "Procedures?" -
/CHOICE "POIN FIVE" "FOCU" "POIN FIVE FOCU ONOF"
LET rad_FILENAME 'rad_FILENAME'   /PROMPT "CLIC File name ?" /FILE "*.hpb"
LET rad_fix_zero 'rad_fix_zero'  /PROMPT "Fix Zero level?" 
LET rad_fix_width 'rad_fix_width'  /PROMPT "Fix Width for Pointings?" 
LET rad_width 'rad_width'  /PROMPT "Width (arc. sec.)?" 
LET rad_aver_time 'rad_aver_time'  /PROMPT "Time averaging?" 
LET rad_time 'rad_time'  /PROMPT "Averaging time (sec.)?" 
LET rad_line 'rad_line'  /PROMPT "Line name?" 
!let rad_point_file 'rad_point_file' /prompt "Output To Point File?"
let rad_point_filename 'rad_point_filename' /prompt "Point File Name?" -
  /file "*.int"

!
!GUI\BUTTON "let rad_Scan 'rad_Scan[1]+1'"  NEXT 
!GUI\BUTTON "let rad_Scan 'rad_Scan[1]-1'"  PREVIOUS 
GUI\BUTTON "@ get_scan +1"  NEXT 
GUI\BUTTON "@ get_scan -1"  PREVIOUS
GUI\BUTTON "@ get_fits"  FITS 
GUI\BUTTON "@ reduce_scan NO "  REDUCE 	
GUI\BUTTON "@ reduce_scan YES"  STORE 	
GUI\BUTTON "@ bad_scan"  BAD 	
GUI\GO     " "         





