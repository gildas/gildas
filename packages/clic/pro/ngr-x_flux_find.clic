!------------------------------------------------------------------------
!
! NGR-X_FLUX_FIND.CLIC
!
!------------------------------------------------------------------------
!
begin procedure choosebox
   define character chain*4
   let chain &1 /format i4.4
   change dir <greg<box'chain'
end procedure choosebox

if (.not.before_polyfix) set drop 0.1 0.1

if .not.file("tmpipe") then
   sic mkdir tmpipe
   sys "chmod ugo+rwx tmpipe "
endif

if .not.exist(theconf) def char theconf*20 /global
let theconf ""
if (exist(tnconf).and.exist(array)) then
   if (tnconf.gt.1) then
      for ti 1 to tnconf
         if (array['ti',1].eq.telescope.and.array['ti',2].eq."yessou") then
            let theconf "-conf"'ti'
         endif
      next ti
   endif
endif

if .not.exist(listcounter) define integer listcounter /global
let listcounter 0
!
let invalid_list .false.
symbol thislist 'scanlist'
!
for i 0 'thislist' 
   let listcounter listcounter+1
next
!
if (listcounter.eq.1) then
   !
   ! Nothing in scanlist: use default 
   ! 
   let scanlist 'scanflux[1]' 'scanflux[2]'
   let listcounter 2
else
   !
   ! There must be an even number of scan in scanlist to define
   ! valid ranges
   !
   let listcounter listcounter-1
   if (mod(listcounter,2).ne.0) then
      say "Not a valid scan list !"
      let invalid_list .true.
      return
   endif
endif
!
! Just for presentation
!
define integer i2
if exist(sf) delete /var sf
define integer sf[listcounter] /global
!
let sf 'thislist'
let scanlist 'thislist'
for i1 1 to listcounter by 2
   let i2 i1+1
   if (i2.eq.2) then
      let scanlist 'sf[i1]'" "'sf[i2]' 
   else
      let scanlist 'scanlist'"   "'sf[i1]'" "'sf[i2]' 
   endif
next i1
!
! 
! 
find /source * /type 'caltype' /proc flux corr /scan 'thislist'   
find append /proc flux corr /type object /source MWC349 /scan 'thislist'  
find append /proc flux corr /type object /source LKHA101 /scan 'thislist' 
!find append /proc flux corr /type object /source CRL618 /scan 'thislist'
if (polarization_track) find append /source * /type 'caltype' /proc flux corr /scan 'thislist' /pol 1   
if (polarization_track) find append /proc flux corr /type object /source MWC349 /scan 'thislist' /pol 1   
if (polarization_track) find append /proc flux corr /type object /source LKHA101 /scan 'thislist' /pol 1   
!
! End
! 
!delete /symbol thislist

if exist(solve_check) then
   ! solve_check is created with ngr-x_flux_default
   ! therefore, this script won't flag is called from ngr-x_flux_default
   ! this script will flag if called from ngr-x_flux_solve
   del /var solve_check
   return
endif 


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
if exist(do_pipe) then
if (do_pipe) then

   mask shadow /ant all
   ! it discards all scans which scaled amplitudes differ from the maximun 
   ! in more than a cerain limit (currently trying a 20%), by flagging them 
   ! for the specific antenna, and recovered later with recover.clic .
   !
   say " "
   say "FLUX CALIBRATION: find good scans to be used just within the pipeline..."
   !
   ! for the pipeline I will just concentrate in the correlations closer to the 
   ! maximum corr-amplitude in less than a certain limit (limfind)
   !
   if .not.exist(dh_aflag) var data_header
   if .not.exist(source) var position
   !
   !
   find /proc corr flux /typ p /scan 'thislist'  
   find append /proc flux corr /type object /source MWC349 /scan 'thislist'  
   find append /proc flux corr /type object /source LKHA101 /scan 'thislist'  
   !find append /proc flux corr /type object /source CRL618 /scan 'thislist' 
   if (polarization_track) find append /proc corr flux /typ p /scan 'thislist' /pol 1  
   if (polarization_track) find append /proc flux corr /type object /source MWC349 /scan 'thislist' /pol 1    
   if (polarization_track) find append /proc flux corr /type object /source LKHA101 /scan 'thislist' /pol 1    
   lis /sou
   if exist(nsou) del /var nsou csou  
   define integer nsou /like n_source /global
   define character csou*12 /like c_source /global
   !
   let nsou 'n_source'
   !
   !                              to define csou ordered from list end to start
   !                              in order to be able to associate to colors in plots
   define logical repeated
   define character prevsou*12 
   define integer ns
   let prevsou "none"
   let ns 0
   for i found to 1 by -1
      get cx_num['i'] !/header
      if (source.ne.prevsou) then
         if (ns.eq.0) then
            let ns 1
            let csou[1] 'source'
         else
            let repeated no
            for j 1 to ns
               if (source.eq.csou['j']) let repeated yes
            next j
            if (.not.repeated) then
               let ns ns+1
               let csou[ns] 'source'
            endif
         endif
         let prevsou 'source'
      endif
   next i
   exam csou

   define logical execute_find allfiles
   let execute_find yes
   if (nsidebands.eq.2) then
      let allfiles file("tmpipe/'short_file'_badflux'theconf'.clic").and.file("tmpipe/'short_file'_fluxplot_limfind'theconf'.clic")
      let allfiles allfiles.and.file("tmpipe/'short_file'_badflux_o'theconf'.clic")
      let allfiles allfiles.and.file("tmpipe/'short_file'_fluxplot_limfind_o'theconf'.clic") 
      if (allfiles) let execute_find no
   else
      if file("tmpipe/'short_file'_badflux'theconf'.clic").and.file("tmpipe/'short_file'_fluxplot_limfind'theconf'.clic") let execute_find no
      set band 'cal_band[receiver]'
   endif
   if (nconf.gt.1) then  ! more than one configuraiton with data on source ! we delete each time previous FIND files and execute it again
      let execute_find yes
      sic delete "tmpipe/"'short_file'"_badflux"'theconf'".clic tmpipe/"'short_file'"_fluxplot_limfind"'theconf'".clic"
      sic delete "tmpipe/"'short_file'"_recover"'theconf'".clic tmpipe/"'short_file'"_recoverbb"'theconf'".clic"
      if (nsidebands.eq.2) sic delete "tmpipe/"'short_file'"_badflux_o"'theconf'".clic tmpipe/"'short_file'"_fluxplot_limfind_o"'theconf'".clic"
      if (nsidebands.eq.2) sic delete "tmpipe/"'short_file'"_recover_o"'theconf'".clic tmpipe/"'short_file'"_recoverbb_o"'theconf'".clic"
   endif
   exam execute_find
   del /var allfiles

   define double wei[nant]
   define real max[nant]  ! desv[nant] med[nant]
   define real dhaflag dhbflag
   define integer flag 
   define logical aflag bflag
   if exist(limfind) del /var limfind prev_flux
   if (.not.file("tmpipe/'short_file'_badflux'theconf'.clic")).or.(.not.file("tmpipe/'short_file'_fluxplot_limfind'theconf'.clic")) then      ! initialize both
      sic out tmpipe/'short_file'_fluxplot_limfind'theconf'.clic
         say "if .not.exist(limfind) define real limfind["'nant'","'nsou'"] prev_flux["'nsou'"] /global"
      sic out
      sic out tmpipe/'short_file'_badflux'theconf'.clic
         say " "
      sic out
   endif
   if (.not.file("tmpipe/'short_file'_badflux_o'theconf'.clic")).or.(.not.file("tmpipe/'short_file'_fluxplot_limfind_o'theconf'.clic")) then  ! initialize both
      sic out tmpipe/'short_file'_fluxplot_limfind_o'theconf'.clic
         say "if .not.exist(limfind) define real limfind["'nant'","'nsou'"] prev_flux["'nsou'"] /global"
      sic out
      sic out tmpipe/'short_file'_badflux_o'theconf'.clic
         say " "
      sic out
   endif
   !type "tmpipe/"'short_file'"_fluxplot_limfind'theconf'.clic"
   !type "tmpipe/"'short_file'"_fluxplot_limfind_o'theconf'.clic"


   if (do_atm) then
      set phase relative atm
   else
      set phase relative noatm
   endif
   !
   
   set spw line
   set polar both
   !
   set ant all
   !set amp abs
   set amp scaled
   set y ampl

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !if file("tmpipe/'short_file'_badflux'theconf'.clic").and.file("tmpipe/'short_file'_fluxplot_limfind'theconf'.clic").and.file("tmpipe/'short_file'_badflux_o'theconf'.clic").and.file("tmpipe/'short_file'_fluxplot_limfind_o'theconf'.clic") then
      !set level 8 
      !say "RELOADING limfind variable....."
      !@ "tmpipe/"'short_file'"_fluxplot_limfind'theconf'.clic"   !! >> reload LIMFIND and PREV_FLUX
      !@ "tmpipe/"'short_file'"_badflux'theconf'.clic"            !! >> perform store flag /memory
      !set level 6
      ! this is now made in solve program for each sideband independently
   !else

   if (execute_find) then

      for iisb 1 to nsidebands

         if (nsidebands.eq.2) then
            if (iisb.eq.1) then
               say " FOR TUNING SIDEBAND....." 
               set band 'tuning_sideband'
            else if (iisb.eq.2) then
               say " FOR THE OTHER SIDEBAND....." 
               set band 'other_sideband'
            endif
         else
            set band 'cal_band[receiver]'
         endif

         if .not.exist(limfind) define real limfind[nant,nsou] prev_flux[nsou] /global
         let limfind 0

         !sic delete tmpipe/'short_file'_badflux'theconf'.clic !tbrem !already removed in x_flux
         !sic delete tmpipe/'short_file'_recover'theconf'.clic !tbrem !already removed in x_flux

         for i 1 to nsou                        ! loop for all sources

            !exam csou['i']

            find /proc corr flux /sou 'csou[i]' /scan 'thislist' 
            if (polarization_track) find append /proc corr flux /sou 'csou[i]' /scan 'thislist' /pol 1
            store flag redu /ant all /memory /reset
            get first !/header
            let prev_flux['i'] flux
            for jj 2 to found
               get cx_num['jj'] !/header
               let prev_flux['i'] min(prev_flux['i'],flux)  !compute the min flux stored in all correlations (just in case we have 1Jy)
            next jj
            !get cx_num['found'] !/header
            !let prev_flux['i'] flux
            sic out dummy.dat
               say "let prev_flux["'i'"] "'prev_flux[i]'
            sic out
            if (nsidebands.eq.2) then
               if (iisb.eq.1) then
                  sic append dummy.dat tmpipe/'short_file'_fluxplot_limfind'theconf'.clic
               else if (iisb.eq.2) then
                  sic append dummy.dat tmpipe/'short_file'_fluxplot_limfind_o'theconf'.clic
               endif
            else
               sic append dummy.dat tmpipe/'short_file'_fluxplot_limfind'theconf'.clic
            endif
            !exam flux
            !lis 
            set x scan
            !if exist(atbure) then     !FOR VERIFICATIONS
            !   if atbure then
            !      plot /id col
            !   else
            !      plot /nodraw
            !   endif
            !else
            plot /nodraw 
            !plot /id col
            !endif
            !pause aaaaaaa
            if exist(xdata) del /var xdata
            lis /flag   !needed to update the dh_a/bflag information
         
            let flag 0           ! discard sources for which all scans are flagged
            for ii 1 to found
               let aflag yes
               let bflag yes
               get cx_num['ii']  ! reading with get instead of get/head due  to difficulties to !!!
               ! read flag dh variables                                 !!!!!!!
               compute dhaflag sum dh_aflag
               compute dhbflag sum dh_bflag
               if (csou[i].eq."MWC349".or.csou[i].eq."LKHA101") then   ! to avoid penalizing MWC349 data when having Halpha line
                  let dhbflag abs(dhaflag)
               else
                  let dhbflag abs(dhbflag)+abs(dhaflag)
               endif
               if dhbflag.ne.0 let flag flag+1
            next ii
 
!!!if flag.lt.found then  ! at least a scan is not flagged (other than shadow, satu, etc.) !today we can have too much shadowed data, but we still need to put limits

            if exist(x_data) then ! it means that all the data are not flagged
               define integer xdata /like x_data
               let xdata x_data
               
               define double wdata /like w_data[,1]
               if exist(atbure) then
                  if atbure then
                     say 'csou[i]'
                  endif
               endif
               for aa 1 to nant
                  compute max['aa'] max y_data[,'aa'] /blanking 1.23456e34 1.23456e30
                  !let wdata 0
                  let wdata  1e-3/w_data[,'aa'] /where w_data[,'aa'].ne.0  !commented until the pb with weights is solved !!!!!!!!!!!!!!!!!! ACC !!!!!!!!!!  PATCH !!!! ATTENTION
                  compute wei['aa'] median wdata 
                  
                  let limfind['aa','i'] min(max['aa']*0.8,max['aa']-2.5*wei['aa'])
                  sic out dummy.dat new
                     say "let limfind["'aa'","'i'"] "'limfind[aa,i]'
                  sic out
                  !type dummy.dat
                  if (nsidebands.eq.2) then
                     if (iisb.eq.1) then
                        sic append dummy.dat tmpipe/'short_file'_fluxplot_limfind'theconf'.clic
                     else if (iisb.eq.2) then
                        sic append dummy.dat tmpipe/'short_file'_fluxplot_limfind_o'theconf'.clic
                     endif
                  else
                     sic append dummy.dat tmpipe/'short_file'_fluxplot_limfind'theconf'.clic
                  endif
                  say 'aa' "  max/wei=" 'max[aa]' 'wei[aa]' "  lim=" 'limfind[aa,i]' " (" 'max[aa]*0.8' 'max[aa]-3*wei[aa]' ")  " 'csou[i]'  /format i0 a f8.4 f8.4 a f8.4 a f8.4 f8.4 a a
                  !          if exist(atbure) then   !FOR VERIFICATIONS
                  !             if atbure then
                  !                @ choosebox 'aa'    
                  !                pen /dash 3
                  !                draw relocate 'x_data_min[aa]' 'limfind[aa,i]' /user
                  !                draw line 'x_data_max[aa]' 'limfind[aa,i]' /user /clip
                  !                pen /def
                  !             endif
                  !          endif
                  
                  for mm 1 to n_data['aa']
                     !if y_data['mm','aa'].lt.limfind['aa','i'].or.y_data['mm','aa'].gt.1e30 then
                     if ((y_data['mm','aa'].lt.limfind['aa','i']).and.(xdata['mm','aa'].ne.0)) then
                        if exist(atbure) then
                           if atbure then
                              say "scan "'xdata[mm,aa]'" of "'csou[i]'" removed for ant(log)"'aa'
                           endif
                        endif
                        sic delete tmpipe/dummy.dat*
                        sic out tmpipe/dummy.dat new
                           say "find /sou "'csou[i]'" /scan "'xdata[mm,aa]'" "'xdata[mm,aa]'" /proc corr flux"
                           if (polarization_track) say "find append /sou "'csou[i]'" /scan "'xdata[mm,aa]'" "'xdata[mm,aa]'" /proc corr flux /pol 1"
                           say "if (found.ne.0) store flag redu /ant "'aa'" /memory"
                        sic out
                        set level 8 
                        !find /sou 'csou[i]' /scan 'xdata[mm,aa]' 'xdata[mm,aa]' /proc corr flux
                        !store flag redu /ant 'aa' /memory
                        set level 6
                        sic delete tmpipe/dummy2.dat*
                        sic out tmpipe/dummy2.dat new
                           say "find /sou "'csou[i]'" /scan "'xdata[mm,aa]'" "'xdata[mm,aa]'" /proc corr flux" 
                           if (polarization_track) say "find append /sou "'csou[i]'" /scan "'xdata[mm,aa]'" "'xdata[mm,aa]'" /proc corr flux /pol 1" 
                           say "if (found.ne.0) store flag redu /ant "'aa'" /memory /reset"
                        sic out 
                        if (nsidebands.eq.2) then
                           if (iisb.eq.1) then
                              sic append tmpipe/dummy.dat tmpipe/'short_file'_badflux'theconf'.clic
                              sic append tmpipe/dummy2.dat tmpipe/'short_file'_recover'theconf'.clic
                           else if (iisb.eq.2) then
                              sic append tmpipe/dummy.dat tmpipe/'short_file'_badflux_o'theconf'.clic
                              sic append tmpipe/dummy2.dat tmpipe/'short_file'_recover_o'theconf'.clic
                           endif
                        else
                           sic append tmpipe/dummy.dat tmpipe/'short_file'_badflux'theconf'.clic
                           sic append tmpipe/dummy2.dat tmpipe/'short_file'_recover'theconf'.clic
                        endif
                        sic delete tmpipe/dummy2.dat* tmpipe/dummy.dat*
                        !else
                        !   if exist(atbure) then
                        !      if atbure then
                        !         say "scan "'xdata[mm,aa]'" of "'csou[i]'" NOT removed for ant(log)"'aa'
                        !      endif
                        !   endif
                     endif
                  next mm
                  !say 'aa'"      max "'max[aa]'"      lim"'limfind[aa,i]'"    "'csou[i]'
                  !exam max['aa']
                  !exam limfind['aa','i']
               next aa
            
               del /var wdata
            endif 
!!!endif
            !pause bbbb

         next i                        ! loop for all sources
         sic delete tmpipe/dummy.dat 
         sic delete tmpipe/dummy2.dat

!pause aaaaaaaaaaaa
      next iisb

   endif
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


   !sic delete tmpipe/'short_file'_badflux.clic 
   sys "chmod ugo+rw tmpipe/"'short_file'"_badflux"'theconf'".clic"  ! keep it to use it again if needed
   sys "chmod ugo+rw tmpipe/"'short_file'"_recover"'theconf'".clic"
   sys "chmod ugo+rw tmpipe/"'short_file'"_fluxplot_limfind"'theconf'".clic"
   sys "chmod ugo+rw tmpipe/"'short_file'"_badflux_o"'theconf'".clic"  ! keep it to use it again if needed
   sys "chmod ugo+rw tmpipe/"'short_file'"_recover_o"'theconf'".clic"
   sys "chmod ugo+rw tmpipe/"'short_file'"_fluxplot_limfind_o"'theconf'".clic"
   sys "ls -lrt tmpipe/"'short_file'"_recover.clic tmpipe/"'short_file'"_recover_o"'theconf'".clic"
   

   !set level 8             
   !find /proc corr flux /typ p /scan 'thislist'  
   !find append /proc flux corr /type object /source MWC349 /scan 'thislist' 
   !find append /proc flux corr /type object /source LKHA101 /scan 'thislist'   
   !!find append /proc flux corr /type object /source CRL618 /scan 'thislist'  
   !set level 6
   
   !say ""
   !say " Selected scans for flux calibration..."
   !list
   !say ""

   mask shadow /ant all /reset

endif
endif





delete /symbol thislist

return

