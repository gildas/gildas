!---------------------------------------------------------------------------------
!
! NGR-X_CALIB_MONITOR.CLIC
!
!---------------------------------------------------------------------------------
!===========================================================================
var size off
var size on
var wvr off
var wvr on
var data_header off
var data_header on
var conf off
var conf on

if (.not.before_polyfix) set drop 0.1 0.1

if .not.exist(dobs) var general
if .not.file("tmpipe") then
   sic mkdir tmpipe
   sys "chmod ugo+rwx tmpipe "
endif
!
set spw cont
if (old_file.ne.file_name) then
   say "Please use SELECT before"
   return
endif

if ((do_calibrate_lr).and.(n_line_bands.gt.r_nbb)) then
   say " "
   say " SWITCHING to LR calibration mode. Loading the track... "
   say " "
   !set level 0
   set mode lowres
   find
   on error continue
   for i 1 to found
      get 'cx_num[i]'
   next i
   if (sic%interactive) on error pause
   !set level 6
else
   if (do_calibrate_lr) then
      say " Working in LR calibration mode... "
   else
      say " Not working in LR calibration mode... "
   endif
endif
exam do_calibrate_lr

if (found.ne.0) get f
if .not.exist(theconf) def char theconf*20 /global
let theconf ""
if (exist(tnconf).and.exist(array)) then
   if tnconf.gt.1 then
      for ti 1 to tnconf
         if (array['ti',1].eq.telescope.and.array['ti',2].eq."yessou") then
            let theconf "-conf"'ti'
         endif
      next ti
   endif
endif

! 
! Local variables
!
define character disabled*40 outline*300
define real meanph
!
! Loop on receivers
!
if do_atm then
   for kr 1 to 4
      if (irec[kr].gt.0) then
         !
         ! Find all scans down to quality 8 
         ! 
         set quality 8
         set receiver irec[kr]
         find /proc corr flux /sou * /type *
         get f !/header 

         !
         set level 6
         set level 0
         ! MONITOR COMMAND is not needed from Jan07, the installation of the new receivers 
         !   Run MONITOR 0 to set reference for atmospheric phase correction based on total power
         if dobs.lt.-6448 then !before 01-jan-2007
            monitor 0 
         else    
            if exist(val_corr) del /var val_corr val_nant val_scans val_sets val_rflag val_mxx valno_cha val_num_ant val_corr_line val_ants_line
            define logical val_corr[maxant,found] val_corr_line[maxant] val_ants_line[maxant]
            define integer val_nant[found] val_sets val_num_ant
            define integer val_scans[2,found]
            define real val_rflag[maxant] val_mxx
            define character valno_cha*40
            let val_sets 1
            for i 1 to found
               get cx_num[i]
               let val_corr_line wvrnch.ne.0.and.dh_wvrstat.eq.8
!
! The following two lines can serve as template to flag the radiometers on certain antennas for defined periods 
               let val_ants_line .not.(phys_ant.eq.7.and.dobs.ge.-3398.and.dobs.le.-3367) ! A7 WVR instable 09-May-2015 to 09-Jun-2015
               let val_corr_line = val_corr_line.and.val_ants_line
               !
               let val_rflag 0
               if i.gt.1 then
                  let val_rflag = 1 /where val_corr_line.ne.val_corr[,val_sets]
                  compute val_mxx sum val_rflag
                  if val_mxx.lt.0.5 then
                     let val_scans[2,val_sets] = scan
                  else
                     let val_sets = val_sets+1
                     let val_corr[,val_sets] = val_corr_line
                     let val_nant[val_sets] = nant
                     let val_scans[,val_sets] = scan
                  endif
               else
                  let val_corr[,val_sets] = val_corr_line
                  let val_nant[val_sets] = nant
                  let val_scans[,val_sets] = scan
               endif
            next i
               !
            for i 1 to val_sets
               let val_num_ant 0
               let valno_cha " "
               for j 1 to val_nant[i]
                  if .not.val_corr[j,i] then
                     let valno_cha = 'valno_cha'" "'j'
                  else
                     let val_num_ant = val_num_ant+1 
                  endif
               next j
               find /proc corr flux /sou * /type * /scan val_scans[1,i] val_scans[2,i] 
               if (found.ne.0) then
                  if ((val_num_ant.ge.1).and.(val_num_ant.lt.val_nant[i])) then
                     symbol valnocha 'valno_cha'
                     mark data /ant 'valnocha'
                     if (kr.eq.1) then 
                        mask shadow saturati /ant all 
                        if (do_self_all) then
                           say "store correction self "
                           store correction self
                        else
                           say "store correction auto /fraction 0.05 "
                           store correction auto /fraction 0.05
                        endif
                        mask shadow saturati /ant all /reset  
                     else
                        say "store correction auto /fraction 0.05 "
                        store correction auto /fraction 0.05 /receiver irec[1]
                     endif
                     say "store correction bad /ant "'valnocha'
                     store correction bad /ant 'valnocha'
                     mark data /ant 'valnocha' /reset
                     del /symbol valnocha
                  else
                     if  (val_num_ant.eq.0) then
                        say "store correction bad /ant all "
                        store correction bad /ant all
                     else
                        if (kr.eq.1) then 
                           mask shadow saturati /ant all 
                           if (do_self_all) then
                              say "store correction self "
                              store correction self
                           else
                              say "store correction auto /fraction 0.05 "
                              store correction auto /fraction 0.05
                           endif
                           mask shadow saturati /ant all /reset  
                        else
                           say "store correction auto /fraction 0.05 "
                           store correction auto /fraction 0.05 /receiver irec[1]
                        endif
                     endif
                  endif
               endif
            next i
         endif
         !    
         !
         ! Print out message
         ! 
         find /proc corr flux /sou * /type *
         let outline = "list /short /var "
         for i 1 to maxant 
            let outline = 'outline'" ok_mon["'i'"]" 
         next
         let outline = 'outline'" /out ""tmpipe/mon.txt"" "
         sic delete tmpipe/mon.clic tmpipe/mon.clic~
         sic output "tmpipe/mon.clic"
            say 'outline'
         sic output
         @ tmpipe/mon.clic
         sic delete tmpipe/mon.txt tmpipe/mon.txt~
         sic delete tmpipe/mon.clic tmpipe/mon.clic~
         say " "
         say "Real-time atmospheric phase correction"
         for i 1 to n_group
            find /scan s_group[1,i] s_group[2,i] /proc corr flux /sou * /type *
            get first
            let disabled " "
            for j 1 to nant
               if (.not.ok_mon[j]) then
                  let disabled 'disabled'" "'j'
               endif
            next j                   
            if (disabled.ne." ") then
               say Scans 's_group[1,i]'" to "'s_group[2,i]' -
               ": phase correction disabled (ant "'disabled'")"
            endif
         next i
         say " "
         !
         ! Check which phase correction was used at the PdBI
         ! 
         if (.not.exist(phselect)) variable line on
         find /proc corr /sou * /type *
         get first !/header
         define real tmpph /like phselect
         let tmpph phselect
         for i 1 to nant
            compute meanph mean tmpph[i,1:n_line_bands]
            if (.not.before_polyfix.and.dobs.le.-2395) let meanph 10  !to patch a bug in RDI before 05.feb.2018 (-2395) such that phselect=0 for LR spws
            if (meanph.lt.1) then
               say "Ant. "'i'": no real-time phase correction used"
               let do_atm no
            else if (meanph.lt.2) then
               say "Ant. "'i'": real-time phase correction based on Total Power monitor" 
            else 
               say "Ant. "'i'": real-time phase correction based on 22 GHz WVR" 
               let wvr_type yes
            endif
         next i
         delete /var tmpph
         list /short /var phselect /out "tmpipe/phselect.txt" !needed to generate n_group
         sic delete tmpipe/phselect.txt
         if (n_group.gt.1) then
            say "Warning: phase correction setup changed during the observations"
         endif
      endif
   next kr
   !
   ! MB 13-Mar-2008 --v
   @ ngr-wvr_interf
   ! MB 13-Mar-2008 --^
   ! MB 14-Feb-2008 --v
   if file("tmpipe/'short_file''theconf'.22i") then
      sys "wc tmpipe/"'short_file''theconf'".22i > tmpipe/tmp.tmp"
      define integer kk
      accept kk /column "tmpipe/tmp.tmp"
      sic delete tmpipe/tmp.tmp
      if kk.gt.1 then
         say " "
         say " INFO: found scans with perturbed WVRs."
         let kk = kk-1
         define integer scan_a[kk] scan_b[kk]
         accept scan_a scan_b /column "tmpipe/"'short_file''theconf'".22i" /line 2 'kk+1'
         for i 1 to kk
            say "Scans " 'scan_a[i]' " to " 'scan_b[i]' " : phase correction disabled (all antennas)"  /form a i4 a i4 a
            find /proc corr vlbi /scan 'scan_a[i]' 'scan_b[i]'
            if (found.ne.0) then
               say "store corr bad /ant all "
               store corr bad /ant all
            endif
         next i
         say " "
      endif
   endif
   ! MB 14-Feb-2008 --^

   
   ! After PHCORR, toggle WVR correction good/bad as defined by user (VA, 28/3/22)
   !! check_nlines is defined in pipeline.clic to check number of lines that are 
   !! not comments.
   if exist(user_inputs_file) then
      if (user_inputs_file) then 

         let check_nlines 0   
         let user_inputs_message ""
         
         if file("tmpipe/'short_file'_toggle_wvr_correction.clic") -
              sic delete tmpipe/'short_file'"_toggle_wvr_correction.clic"

         sic grep "wvr.correction" 'user_inputs_filename'
         
         if (GREP%N.ne.0) then 
            say ""                      
            say "wvr.correction..."                    
            for l 1 to GREP%N
              if (GREP%LINES[l][1:1].ne."!") then 
                 let check_nlines check_nlines+1

                 if (check_nlines.eq.1) then
                    sic out tmpipe/'short_file'"_toggle_wvr_correction.clic" NEW
                 else
                    sic out tmpipe/'short_file'"_toggle_wvr_correction.clic" APPEND
                 endif
                 if (GREP%LINES[l][1:5].eq."store") then 
                    say "if ('found'.gt.0) "'GREP%LINES[l]'
                 else 
                    say 'GREP%LINES[l]'
                 endif 
                 sic out
              endif
            next l
            
            if (check_nlines.ne.0) then               
               @ tmpipe/'short_file'"_toggle_wvr_correction.clic"
               say "==> Toggle WVR correction as defined by user:"
               if ('found'.ne.0) then          
                  let user_toggle_wvr yes
                  say ""
                  say ""
                  type "tmpipe/"'short_file'"_toggle_wvr_correction.clic"
                  say ""
                  say ""

                  !! Write used commands to file for printing in report
                  sic out "tmpipe/"'short_file'"-pipeline_user_inputs_used.txt" APPEND
                    say "  - Toggle WVR correction (wvr.correction)"
                    for q 1 to check_nlines
                      accept user_inputs_message -
                             /column tmpipe/'short_file'_toggle_wvr_correction.clic -
                             /line 'q' /format a150
                      say "    "'user_inputs_message'
                    next q
                    say " "                
                  sic out
               else
                  say "==> Scan range not found -- user WVR settings not applied."
               endif              
               sic delete tmpipe/'short_file'"_toggle_wvr_correction.clic"
               sic delete tmpipe/'short_file'"_toggle_wvr_correction.clic~"
            endif
         endif
      endif
   endif
else if (.not.do_atm) then
   say " "
   say " Atmospheric phase correction is disabled. "
   say " To enable it: 'let do_atm yes' "
   say " "
   find /proc corr flux
   if (found.ne.0) then
      say "store corr bad /ant all "
      store corr bad /ant all   
   endif
endif



sys "touch tmpipe/monitor-"'short_file''theconf'".done"
sys "chmod ugo+rw tmpipe/monitor-"'short_file''theconf'".done*"
say " MONITOR TOUCH IS DONE ......................................................"

sys "ls -lrt tmpipe/monitor-"'short_file''theconf'".done"



!
!-----------------------------------------------------------------------------
! begin procedure polarization
!
!
!!  Estimate of the polarization degree -- experimental
!
if file("tmpipe/polarization-'short_file''theconf'.done") then
   say "polarization assessment already done ......"
   return
   !if exist(pol_source) then
   !   exam pol_source[1]
   !   if (pol_source[1].ne."none") then
   !   return
   !endif
endif
 say " "
 say " Polarization Assessment on the phase calibrators ..."
 say " "
 @ngr-x_calib_pol
 if (nph.gt.0) then                  !! phc from select
   symbol allcali 'phcal'
 else
   symbol allcali *
 endif
 find /proc corr flux /typ 'caltype' /sou 'allcali'
 lis /source
 !let do_avpol no
 if (isaod) then
   say " "
   exam pol_source
   exam pol_sig
   say " "
 endif
 for i 1 to n_source
   for j 1 to npol_source 
     if (c_source['i'].eq.pol_source['j']) then
       if (pol_sig['j'].gt.7) then 
          let do_avpol yes
          say ""
          say "Switching to averaged-polar mode for amplitude calibration..."
          say ""
       endif
     endif
   next j
 next i



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! DECIDE ON POLARIZATION FOR ALL RECOMMENDED CALIBRATORS
symbol allcali 'phcal'
find /proc corr /typ ph /sou 'allcali'
delete /symbol allcali
lis /sou 
define integer dim[5]
compute dim dim pol_sig
define char cavpol*1
for c 1 to dim[1]
   if pol_sig['c'].gt.7 then
      let cavpol n
      for i 1 to n_source   !recommended phase calibrator
         if pol_source['c'].eq.c_source['i'] then ! it's a possible recommended phase calibrator (phcal)
            let do_avpol yes
            let cavpol y
            if (pol_sig[c].ne.1000) then ! 1000=fake value, when deduced from the archive
               say "  " 'pol_source[c]' " is found to be polarized at " 'pol_sig[c]' "xsigma" /format a a a f4.1 a
            else
               say "  " 'pol_source[c]' " is deduced to be polarized from the NOEMA database "  /format a a a 
            endif
            say "  Averaged polarization mode is selected for the amplitude calibration "
            say "  You can change it with ""let do_avpol no"" before the amplitude calibration" 
            say ""
         endif
      next i
      if cavpol.eq."n" then  ! no phase calibrator
         say "  " 'pol_source[c]' " is found to be polarized with a degree =" 'pol_sig[c]' "xrms" /format a a a f4.1 a
         say ""
      endif
   else if pol_sig['c'].eq.-1 then
      for i 1 to n_source   !recommended phase calibrator
         if pol_source['c'].eq.c_source['i'] then ! only reported for recommended phase calibrators (phcal) 
            say "  No polarization clearly detected for " 'pol_source[c]' 
         endif
      next i
   endif
next c
say " "
!if .not.(do_avpol) then
! in that case message is more discrete and given at the beginning...
!endif
del /var dim cavpol
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
if file("'short_file'-pipeline_user_inputs.clic") then
   sic grep "let do_avpol " 'short_file'-pipeline_user_inputs.clic
   if (grep%n.ne.0) then
      sic delete dummy.clic
      for q 1 to grep%n
         if (grep%lines[q][1:1].ne."!") then 
            sic out dummy.clic append
            say 'grep%lines[q]'
            sic out
         endif
      next q
      if file("dummy.clic") then 
         @dummy.clic
         say " "
         say " MONITOR: Restoring user or pipeline wish for do_avpol ...."
         say " "
         type dummy.clic
         sic delete dummy.clic
      endif
   endif
endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 @ngr-x_calib_save
!
! end procedure polarization
!-----------------------------------------------------------------------------
!
!
! Back to defaults
! 
set quality 'min_qual'
set level 4
set spw line
set phase cont 10
if (.not.before_polyfix) set drop 0.002 0
!

