!------------------------------------------------------------------------
!
! NGR-XSHOW_CABLE.CLIC
!
!------------------------------------------------------------------------
!
!
if (.not.isaod) return
if .not.file("tmpipe") then
   sic mkdir tmpipe
   sys "chmod ugo+rwx tmpipe "
endif
!

if exist(do_pipe) then
   if do_pipe then
      on error continue
   endif
endif

set /def
set scan 'first_scan[1]+1' 'last_scan[ngains]'
set tele 'min_teles'

if .not.exist(theconf) def char theconf*20 /global
let theconf ""
if (exist(tnconf).and.exist(array)) then
   if (tnconf.gt.1) then
      for ti 1 to tnconf
         if (array['ti',1].eq.telescope.and.array['ti',2].eq."yessou") then
            let theconf "-conf"'ti'
         endif
      next ti
   endif
endif

!
define integer dims[5] il i3
define real s t utm
define doub clo1_ref
define char chain*4
define logical compute_noise
!
pen /def
g\set exp 1.
set aspect 2 auto
define integer ir
set aver scan
set antenna all
set x time 
set y cable_phase /lim * * 
!
! Loop on receivers
!
if .not.exist(scan) var general on
if .not.exist(flo1_ref) var rf on
if .not.exist(nant) var conf
if .not.exist(n_dumps) var descriptor

set polar both

if file("tmpipe/flag_duetocable-'short_file''theconf'.clic") sic delete "tmpipe/flag_duetocable-"'short_file''theconf'".clic"

for kr 1 to 4

   let ir irec[kr]

   if (irec[kr].gt.0) then

      set receiver irec[kr]
      if (before_polyfix) then
         if (cal_band[ir].eq."DSB") then
            set band lsb
            say "Check the data, the NGRs are SSB receivers"
            if (before_polyfix) pause
         else
            set band 'cal_band[ir]'
         endif
      endif
      
      for icable 1 to 2
      
         !find /proc gain corr flux poin        ! ... everything????????????
         find /proc corr flux poin        ! ... everything????????????
         set ante 1
         mask shadow redu data time saturati lock tracking tsys doppler /ant all
         mask /spectral /ant all
         mask data redu /base all     ! if some second keep flagged, mfit won't work
                                   ! isn't there a 'mfit /blank' ?

         set y scan
         set x time
         set aver scan 
         plot /nodraw
         if icable.eq.1 then
            define real xtime /like x_data[1]
            let xtime x_data[1]
            define integer slist /like y_data[1]
            let slist y_data[1]
         endif
         
         set aver scan !none
         
         set data 'found*60' 'nant'
         set ant all
         if icable.eq.1 then
            set sub line
            set y cable_phase /lim * *
            set x time
            plot /nodraw 
         else if icable.eq.2 then
            set sub cont
            set aver scan  ! time averaged in the scan for the 2nd REFERENCE
            set x time
            if (dobs.lt.-3274) then ! 10-sep-2015, although new variables existed from summer
               set y t12  /lim * *
               plot /nodraw
               let y_data y_data*360 
               plot same /nodraw 
            else
               set y cable_altern  /lim * *
               plot /nodraw
            endif
         endif
         
         if .not.exist(phase_jump) define real phase_jump
         let phase_jump 30                              ! phase jump from which lines are plotted !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1

         define real du /like y_data
         let du y_data
         compute dims dim y_data
         !define real dx['dims[1]-1'] dy['dims[1]-1']   
         define real dx['dims[1]'] dy['dims[1]-1'] 
         define real dm['dims[1]-1'] dn['dims[1]-1'] 
         ! let dx x_data[1:'dims[1]-1',1]  
         let dx x_data[1:'dims[1]',1] 
         
         if icable.eq.1 then
            set y cable_phase /lim * *
            !find /proc gain corr flux poin
            find /proc corr flux poin
            set sub line
            set aver scan
            plot /phys /id col
         else if icable.eq.2 then
            set sub cont
            set aver scan !time 30
            !find /proc gain corr flux poin 
            find /proc corr flux poin 
            if (dobs.lt.-3274) then ! 10-sep-2015, although new variables existed from summer
               set y t12 /lim * *
               plot /nodraw
               let y_data y_data*360 /where y_data.lt.1e34
               plot same /phys /id col
            else
               set y cable_altern /lim * *
               plot /phys /id col
            endif
         endif
         
         g\set mar 4 0 0.2
         g\set exp .6
         
!!!!!! MARK CHANGE IN CABLE_PHASE > phase_jump  (between consecutive scans?) !!!!!!
         for i 1 to nant
            let dn[j] j
            let dy abs(du[2:'dims[1]',i]-du[1:'dims[1]-1',i]) /where du[1:'dims[1]-1',i].lt.1000
            let dy 0 /where dy.lt.phase_jump*(flo1_ref/8)/flo1.or.dy.gt.1000     ! = phase_jump @ synth freq
            
            let dm 1
            let dm 0 /where dy.eq.0
            compute s sum dm
            
            let il 0
            let i3 0
            
            let dn dn*dm                               ! 1 0 0 0 5 0 0 8 0 0 ... keep number where dy.ne.0
            let dn dims[1] /where dn.lt.0.1            ! 1 ffound ffound ffound ffound 5 ffound .... 
            
            for k 1 to nint(s)

               compute t min dn                           ! t = min matrix position (1, then 5, then 8, ...)
               let utm (dx['nint(t)']+dx['nint(t)+1'])|2
               
               find /scan slist['nint(t)']
               get f !/head
               let clo1_ref flo1_ref
               find /scan slist['nint(t)+1']
               get f !/head
               
               !            if (slist['nint(t)+1'].eq.(slist['nint(t)']+1)) then               !only if consecutive scans 
               let chain 'i' /format i4.4
               chan dire <greg<box'chain'
      	       if (flo1_ref.ne.clo1_ref) then             ! the phase jump is due to LO1ref change 
                  pen 0 /col 11 /dash 2
                  if (il.eq.0) then
 	             draw tex utm-(user_xmax-user_xmin)*.015 user_ymax-(user_ymax-user_ymin)*0.2 "LO1_REF" 5 90 /user
                  endif
                  let il 1
               else                                       ! the phase jump is NOT due to LO1ref change
                  pen 0 /col 6 /dash 2
                  if (i3.eq.0) then
  	             draw tex utm-(user_xmax-user_xmin)*.015 user_ymin+(user_ymax-user_ymin)*0.2 'nint(phase_jump)'"\Uo" 5 90 /user
                  endif
                  let i3 1
               endif
               draw rel utm user_ymin /user
               draw lin utm user_ymax /user
               chan dire <greg  
               !            endif
               
               let dn dims[1] /where dn.le.t+0.1         ! 1 ff ff ff ff 5 ff ff... >  ff ff ff ff ff 5 ff ff...  
               
            next k 
            
            let chain 'i' /format i4.4
            chan dire <greg<box'chain'
            clear seg 3-4
            chan dire <greg 

         next i 
         pen /col 0 /dash 1
         g\set mar 4 0 0.1
         g\set exp 1.
 
         
         set aver scan
         del /var dm dn dx dy du
         chan dire <greg
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         
         
!!!!!!!!!!!! CHECK PRESENCE OF SCANS WITH NOISIER CABLE-PHASE CORRECTIONS !!!!!!!!!!!!!!!!!!
         !find /proc gain corr flux poin 
         find /proc corr flux poin 
         if exist(cxnum) del /var cxnum ndata 
         if exist(ffound) del /var ffound
         if exist(rrms) del /var rrms drift dd1 dd2 dd eff_drift eff_rrms iss
         if exist(eloss1) del /var dloss1 dloss5 eloss1 eloss5
         if exist(effloss) del /var effloss
         define int cxnum /like cx_num
         let cxnum cx_num
         let ffound found /new integer
         let ndata n_data[1] /new integer
         define real rrms['ffound','nant'] drift['ffound','nant'] dd1 dd2 dd eff_drift eff_rrms
         define integer iss['ffound'] 
         define logical dloss1['nant'] dloss5['nant'] eloss1['nant'] eloss5['nant'] effloss
         
         let rrms 1.23456e34
         let drift 1.23456e34
         
         set aver none
         sic delete tmpipe/seecable-'short_file''theconf'.clic
         
         sic out tmpipe/seecable-'short_file''theconf'.clic
            say "set aver non"
            say "!"
         sic out
         sic out tmpipe/dummy1.dat
            say "set y cable_phase /lim * *"
            say "plot /phys" 
            say "pause "
            say "! "
         sic out
         sic out tmpipe/dummy2.dat
            if (dobs.lt.-3274) then ! 10-sep-2015, although new variables existed from summer
               say "set y t12 "
               say "plot      "
               say "let y_data y_data*360*360000/(flo1_ref/8) ! Band4 equivalent "
               say "plot same /phys"  
            else
               say "set y cable_altern /lim * * "
               say "plot /phys     "
            endif 
            say "pen /col 1 "
            say "DRAW TEXT -1 0 ""Band 4 equivalent cable-phase corrections"" 5   0.000  /BOX 8 "  
            say "pen /def "
            say "pause "
            say "! "
         sic out
         for is 1 to ffound
         
            find /num 'cxnum[is]' 
            get f !/header
            if (n_dumps.gt.30) let compute_noise .true.                 !!!!!!!!!!! only analyze scans longer than 30 seconds  
            if (n_dumps.gt.20.and.confa) let compute_noise .true.       !!!!!!!!!!! only analyze scans longer than 30 seconds  
            if (compute_noise) then               !!!!!!!!!!! only analyze scans longer than 30 seconds  
               if icable.eq.1 then 
                  clic\set y cable_phase /lim * *
                  plot /nodraw !acc /nodraw   !comment to visualize
               else
                  if (dobs.lt.-3274) then ! 10-sep-2015, although new variables existed from summer
                     clic\set y t12
                     plot /nodraw   
                     if icable.eq.2 let y_data y_data*360 /where y_data.lt.1e34
                  else
                     clic\set y cable_altern /lim * *
                     plot /nodraw !acc /nodraw   !uncomment to visualize   
                  endif
               endif
               !
               if exist(ya) del /var ya xa
               define real ya /like y_data[1]  
               define real xa /like x_data[1] 
               let xa  x_data[1]                  
               for ia 1 to nant  

                  let ya y_data['ia']
                  if (.not.confa) then
                     mfit ya = &a + &b*xa + &c*xa**2 + &d*xa**3 + &e*xa**4 + &f*xa**5 + &g*xa**6 + &h*xa**7 + &i*xa**8 /quiet
                  else if (confa) then
                     mfit ya = &a + &b*xa + &c*xa**2 + &d*xa**3 + &e*xa**4 + &f*xa**5 + &g*xa**6 + &h*xa**7 /quiet
                  endif
!!$                  if (ia.le.9) then                   !uncomment to visualize   
!!$                     change direc  <GREG<BOX000'ia'   !uncomment to visualize 
!!$                  else                                !uncomment to visualize 
!!$                     change direc  <GREG<BOX00'ia'    !uncomment to visualize 
!!$                  endif                               !uncomment to visualize 
!!$                  pen /col 1                          !uncomment to visualize 
!!$                  curve xa mfit%fit                   !uncomment to visualize 
!!$                  pen /def                            !uncomment to visualize 
!!$                  change direc  <GREG                 !uncomment to visualize 
             
                  compute rrms['is','ia'] rms mfit%res  /blanking 1.23456e34 1.23456e30 
                  compute dd1 max mfit%res              /blanking 1.23456e34 1.23456e30 
                  compute dd2 min mfit%res              /blanking 1.23456e34 1.23456e30 
                  let dd dd1-dd2
                  let drift['is','ia'] (dd1-dd2)*flo1/(flo1_ref/8)
                  let rrms['is','ia'] rrms['is','ia']*flo1/(flo1_ref/8)
                  if icable.eq.2.and.receiver.ge.2 then       !t12=alternate=A, and B=observed
                     let drift['is','ia'] drift['is','ia']*115000/flo1
                     let rrms['is','ia'] rrms['is','ia']*115000/flo1
                  else if icable.eq.2.and.receiver.eq.1 then  !t12=alternate=B, and A=observed
                     let drift['is','ia'] drift['is','ia']*360000/flo1
                     let rrms['is','ia'] rrms['is','ia']*360000/flo1
                  endif
                  !
                  ! check if double cable phase - strange pattern
                  let dd1 (abs(dd1)+abs(dd2))/2
                  let mfit%res 0 /where (abs(mfit%res)).gt.(dd1/2)
                  let mfit%res 1 /where mfit%res.ne.0
                  let dd2 0
                  compute dd2 sum mfit%res /blanking 1.23456e34 1.23456e30  !number of seconds with small residuals (close to fitting by max/2)
                  if ((dd2.lt.(n_dumps/3)).and.(dd.gt.0.05)) let rrms['is','ia'] 100  ! if too few seconds with small residuals  = bad cable phases, jumps between two values
                  ! the limit of 0.05 between max-min is set to avoid detecting periodic parasites 
                  !
                  let eff_drift exp(-1*(drift['is','ia']*pi/180)^2)
                  let eff_rrms exp(-1*(rrms['is','ia']*pi/180)^2)
                  !exam drift['is','ia']
                  !exam rrms['is','ia']
                  exam eff_drift
                  exam eff_rrms
                  let effloss (eff_drift.lt.0.98).or.(eff_rrms.lt.0.98)
                  exam effloss
                  !pause aaaaaaaaaaaaaa
                  !if ((iss['is'].eq.0).and.(effloss)) then ! this was aimed to avoid repeating the plot for every antennas !
                  if ((effloss)) then  ! plot for all antennas, even if repeated
                     let iss['is'] 'scan'
                     let chain 'ia' /format i4.4
                     chan dire <greg<box'chain'
                     g\set char .35
                     if (eff_rrms.lt.0.6) then !when rrms=100               !---------------------------------------------------------------------
                        pen /col 1 /dash 5
                        if .not.eloss5['ia'] draw tex xtime['is']-(user_xmax-user_xmin)*.015 user_ymax-(user_ymax-user_ymin)*0.4 "BAD: double values" 5 90 /user
                        pen /col 5 /dash 5
                        let eloss5['ia'] yes
                        if icable.eq.1 then
                        !store flag data /ant 'ia'  ! flags to be applied !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                           sic out tmpipe/dummy.dat
                              say "find /scan "'scan'
                              say "store flag data /ant "'ia'
                           sic out
                           sic append tmpipe/dummy.dat tmpipe/flag_duetocable-'short_file''theconf'.clic
                           !type tmpipe/flag_duetocable-'short_file'.clic
                        endif
                        sic out tmpipe/dummy.dat
                           if (eff_rrms.lt.0.95)  say "find /scan "'scan'"    ! Double values - ref"'icable'
                           if (eff_drift.lt.0.95) say "find /scan "'scan'"    ! Double values - ref"'icable'
                        sic out
                        sic append tmpipe/dummy.dat tmpipe/seecable-'short_file''theconf'.clic
                        sic append tmpipe/dummy'icable'.dat tmpipe/seecable-'short_file''theconf'.clic
                     else if ((eff_drift.lt.0.95).or.(eff_rrms.lt.0.95)) then !---------------------------------------------------------------------
                        !   pause bbbbbbbbbbbbbbbbbbbbbbbbbbb
                        if (eff_drift.lt.0.95) then 
                           pen /col 1 /dash 3 
                           if .not.dloss5['ia'] draw tex xtime['is']-(user_xmax-user_xmin)*.015 user_ymax-(user_ymax-user_ymin)*0.4 ">5% eff_loss (rms)" 5 90 /user
                           let dloss5['ia'] yes
                        endif
                        if (eff_rrms.lt.0.95) then 
                           pen /col 3 /dash 4
                           if .not.eloss5['ia'] draw tex xtime['is']-(user_xmax-user_xmin)*.015 user_ymax-(user_ymax-user_ymin)*0.4 ">5% eff_loss (drift)" 5 90 /user
                           let eloss5['ia'] yes
                        endif
                        if icable.eq.1 then
                        !store flag data /ant 'ia'  ! flags to be applied !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                           sic out tmpipe/dummy.dat
                              say "find /scan "'scan'
                              say "store flag data /ant "'ia'
                           sic out
                           sic append tmpipe/dummy.dat tmpipe/flag_duetocable-'short_file''theconf'.clic
                           !type tmpipe/flag_duetocable-'short_file''theconf'.clic
                        endif
                        sic out tmpipe/dummy.dat
                           if (eff_rrms.lt.0.95)  say "find /scan "'scan'"    ! eloss 5% - ref"'icable'
                           if (eff_drift.lt.0.95) say "find /scan "'scan'"    ! dloss 5% - ref"'icable'
                        sic out
                        sic append tmpipe/dummy.dat tmpipe/seecable-'short_file''theconf'.clic
                        sic append tmpipe/dummy'icable'.dat tmpipe/seecable-'short_file''theconf'.clic
                     else                                                     !---------------------------------------------------------------------
                        if (eff_drift.lt.0.98) then 
                           pen /col 2 /dash 3 
                           if .not.dloss1['ia'] draw tex xtime['is']-(user_xmax-user_xmin)*.015 user_ymax-(user_ymax-user_ymin)*0.4 ">2% eff_loss (drift)" 5 90 /user
                           let dloss1['ia'] yes
                        endif
                        if (eff_rrms.lt.0.98) then 
                           pen /col 4 /dash 4
                           if .not.eloss1['ia'] draw tex xtime['is']-(user_xmax-user_xmin)*.015 user_ymax-(user_ymax-user_ymin)*0.4 ">2% eff_loss (rms)" 5 90 /user
                           let eloss1['ia'] yes
                        endif
                        sic out tmpipe/dummy.dat
                           if (eff_rrms.lt.0.98)  say "find /scan "'scan'"    ! eloss 2% - ref"'icable'
                           if (eff_drift.lt.0.98) say "find /scan "'scan'"    ! dloss 2% - ref"'icable'
                        sic out
                        sic append tmpipe/dummy.dat tmpipe/seecable-'short_file''theconf'.clic
                        sic append tmpipe/dummy'icable'.dat tmpipe/seecable-'short_file''theconf'.clic
                     endif                                                     !---------------------------------------------------------------------
                     g\set char .6
                     draw rel xtime['is'] user_ymin /user
                     draw lin xtime['is'] user_ymax /user
                     pen /def
                     chan dire <greg
                  !else
                  !   pause bbbbbbbbbbbbbbbbbbbbbbbbbb
                  endif
                  !pause 1111111111111111111111111
               next ia
            endif
         next is

         if file("tmpipe/flag_duetocable-'short_file''theconf'.clic") sys "chmod ugo+rw tmpipe/flag_duetocable-"'short_file''theconf'".clic"
         if file("tmpipe/seecable-"'short_file''theconf'".clic") sys "chmod ugo+rw tmpipe/seecable-"'short_file''theconf'".clic"
         sys "rm -rf tmpipe/dummy*"

         set aver scan
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


         if icable.eq.1 then
            pen /col 1
            if page_x.lt.page_y then
               GREG1\DRAW TEXT -0.71 .8 "CABLE PHASE for the used IF" 8 0.000  /BOX 8
            else
               GREG1\DRAW TEXT -0.71 -.5 "CABLE PHASE for the used IF" 8 0.000  /BOX 8
            endif
         else if icable.eq.2 then
            pen /col 1
            if page_x.lt.page_y then
               GREG1\DRAW TEXT -0.71 .8 "CABLE PHASE for the unused IF" 8 0.000  /BOX 8
            else
               GREG1\DRAW TEXT -0.71 -.5 "CABLE PHASE for the unused IF" 8 0.000  /BOX 8
            endif
         endif

         GREG1\SET CHARACTER 0.6
         pen /def
         
         stop
         sic delete "tmpipe/show-cable-"'short_file'"-rec"'ir'"-if"'icable'".ps"
         hard tmpipe/show-cable-'short_file'-rec'ir'-if'icable'.ps /dev ps color
         sys "chmod ugo+rw tmpipe/show-cable-"'short_file'"-rec*.ps"
         
         if exist(atbure) then
            if atbure then
               if file("/SOG/observer/Monitor/cable/pipe/.transferred") then
                  sys "rm -rf /SOG/observer/Monitor/cable/pipe/*.ps.gz "
                  sic delete "/SOG/observer/Monitor/cable/pipe/.transferred"
               endif
               sys "cp -rf tmpipe/show-cable-"'short_file'"-rec"'ir'"-if"'icable'".ps /SOG/observer/Monitor/cable/pipe/show-"'short_file'"-cable-rec"'ir'"-if"'icable''theconf'".ps"
               sys "gzip -f /SOG/observer/Monitor/cable/pipe/*ps"
               sys "chmod ugo+rw /SOG/observer/Monitor/cable/pipe/show-"'short_file'"-cable-rec"'ir'"-if"'icable''theconf'".ps.gz"
            endif
         endif
         
!!!!!!!!!!!!!!!!!!!!!!!!!
         if exist(atbure) then
            if atbure then
               !find /proc gain corr flux poin 
               find /proc corr flux poin 
               clic\set y cable_phase /lim * *
               plot /nodraw 
               let y_data rrms
               plot same /id col
               sic delete "tmpipe/show-rrms-"'short_file'"-rec"'ir'"-if"'icable'".ps"
               hardcopy tmpipe/show-rrms-'short_file'-rec'ir'"-if"'icable'.ps /dev eps color /fit
               !
               let y_data drift
               plot same /id col
               sic delete "tmpipe/show-drift-"'short_file'"-rec"'ir'"-if"'icable'".ps"
               hardcopy tmpipe/show-drift-'short_file'-rec'ir'"-if"'icable'.ps /dev eps color /fit
               sys "chmod ugo+rw tmpipe/show-rrms*.ps"
               sys "chmod ugo+rw tmpipe/show-drift*.ps"
            endif
         endif
!!!!!!!!!!!!!!!!!!!!!!!!!
         
         mask /spectral /ant all /reset
         mask shadow redu data time saturati lock tracking tsys doppler /ant all /reset
         mask data redu /base all /reset
         
      next icable

      !if exist(atbure) then
      !if atbure then
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1 diff cables (like diffcable.clic script @ Bure) - START
      define integer fnum lnum

      set x number

      find
      let fnum cx_num[1]
      let lnum cx_num['found']
      find /proc gain
      if found.ne.0 let fnum cx_num['found']+1

      find /number 'fnum' 'lnum'
      get f 

      set y cable_phase /lim -180 180
      plot /nodraw
      let y_data y_data*flo1/(flo1_ref/8) /where y_data.lt.1e34
      plot same /phys

      define real ycable /like y_data
      define real icab_mean mmin[2] mmax[2]
      define real it12 /like y_data
      define real it12_mean min max 
      let ycable y_data
      compute icab_mean mean ycable /blanking 1.23456e34 1.23456e30
      compute mmin[1] min ycable /blanking 1.23456e34 1.23456e30
      compute mmax[1] max ycable /blanking 1.23456e34 1.23456e30

      if (dobs.lt.-3274) then ! 10-sep-2015, although new variables existed from summer
         set y t12 /lim -180 180
         plot /nodraw
         let y_data y_data*360*flo1/(flo1_ref/8) /where y_data.lt.1e34
      else
         set y cable_altern
         plot /nodraw
         let y_data y_data*flo1/(flo1_ref/8) /where y_data.lt.1e34
      endif
      let it12 y_data
      compute it12_mean mean it12 /blanking 1.23456e34 1.23456e30
      compute mmin[2] min it12 /blanking 1.23456e34 1.23456e30
      compute mmax[2] max it12 /blanking 1.23456e34 1.23456e30
      
      compute min min mmin 
      compute max max mmax 
      
      set y cable_phase /lim min max
      plot /nodraw
      let y_data ycable
      plot same /phys
      
      let y_data it12
      pen /col 1
      plot same /nofr
      
      if exist(cxnum) delete /variable cxnum 
      if exist(ffound) delete /variable ffound
      define integer cxnum /like cx_num
      define integer ffound rrece
      let cxnum cx_num
      define log rchange
      let rchange no
      let ffound found
      get f !/header
      let rrece 'receiver'
      for j 1 to n_boxes 
         if j.lt.10 then
            change dir <GREG<BOX000'j' 
         else
            change dir <GREG<BOX00'j' 
         endif
         clear seg 3-4
         change dir <GREG
      next j
      for i 2 to ffound
         get cxnum['i'] !/header
         if receiver.ne.rrece then
            pen /col 3 /dash 3
            if .not.rchange then
               let rchange yes
               DRAW TEXT 3 -.5 "Change of receiver" 7 0.000  /BOX 7
            endif
            for j 1 to n_boxes               ! j = ant
               if j.lt.10 then
                  change dir <GREG<BOX000'j' 
               else
                  change dir <GREG<BOX00'j' 
               endif
               draw rel 'number' 'user_ymin' /user
               draw lin 'number' 'user_ymax' /user
               change dir <GREG
            next j
            pen /def
            let rrece 'receiver'
         endif
      next i

      pen /def

      if (page_x.gt.page_y) then
         DRAW TEXT 2 -0.2 "Used correction" 0 0.000  /BOX 7
         pen /col 1
         DRAW TEXT -2 -0.2 "Unused correction" 0 0.000  /BOX 8
         g\set char .5
         DRAW TEXT 0 0.1 "Sky-scaled phases" 0 0.000  /BOX 9
      else
         DRAW TEXT 0.5 0.6 "Used correction" 0 0.000  /BOX 7
         pen /col 1
         DRAW TEXT -2.5 0.6 "Unused correction" 0 0.000  /BOX 8
         g\set char .5
         DRAW TEXT -0.68 1.33 "Sky-scaled phases" 0 0.000  /BOX 9
      endif

      find /typ o /number 'fnum' 'lnum'
      if found.eq.0 find /number 'fnum' 'lnum'
      get f !/header
      pen /col 0 /weigh 2
      g\set char .5
      define char msg*100 aref*1
      let aref "A"
      if receiver.gt.1 let aref "B"
      let msg "Main Band: "'receiver'" - at "'nint(frequency/1000)'" GHz - Ref "'aref' 
      if (page_x.gt.page_y) then
         DRAW TEXT -7.3 -.25 'msg' 6 0.000  /BOX 9 
      else
         DRAW TEXT -7 .8 'msg' 6 0.000  /BOX 9 
      endif
      pen /def
      g\set char .6
      
      stop
      sic delete "tmpipe/show-diffcable-"'short_file'"-rec"'ir'".ps"
      hardcopy "tmpipe/show-diffcable-"'short_file'"-rec"'ir'".ps" /dev eps color /fit /over
      sys "chmod ugo+rw tmpipe/show-diffcable-"'short_file'"-rec"'ir'".ps"
      
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1 diff cables (like diffcable.clic script @ Bure) - END
      !endif
      !endif
      
   endif
   
next kr

set aver scan
set sub line
!
