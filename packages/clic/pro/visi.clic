! rn / May, 2002
! jp   Jun 06, 2002 erased the first "sic out say.clic" and added a few 
!                   conditions on the number of found scans.
! rn / Aug 22, 2002 plotting simplified using "chan dire <greg<box0?"
!                   plotting only the amplitude of source visibilities 
! rn / Dec 07, 2002 different treatment for mapping and detection projects
! rn / Jan 14, 2003 error recovery + accept source phase variations up to 
!                   'rpm|2' from zero mean level
! rn / Mar 24, 2003 pointing (red) and focus errors (blue)
! rn / Jan 20, 2004 postscript front page + activate mapping + detection
! rn / Jan 12, 2005 take care of uncalibrated data - filename
! rn / Jan 14, 2005 tracking plot
! rn / Jan 15, 2005 histograms ...

begin procedure choosebox
  define char chain*4
  let chain &1 /format i4.4
  chan dire <greg<box'chain'
end procedure choosebox

!------------------------------------

begin procedure verify_amp
define integer m in
define real me mr 

if (exist(y_data)) then
  define integer dims[5]    
  compute dims dim y_data   
  let in dims[1]            
  define real adata /like y_data[1] ! New code
  for i 1 to nbas
    let m 2*i-1
    let adata 0
    let adata 1-y_data[,m] /where y_data[,m].lt.1e34
    compute me sum adata
    let adata adata^2
    compute mr sum adata
    let ra[i,&1] sqrt(mr|in+(me|in)^2)
  next
else
   let ra[,&1] 999
endif
end procedure verify_amp

!------------------------------------

begin procedure verify_det
define integer m 
define real in me mr

if (exist(y_data)) then
  define integer dims[5]    
  compute dims dim y_data   
  let in dims[1]            
  define real adata /like y_data[1] ! New code
  for i 1 to nbas
    let m 2*i
    let adata 0
    let adata y_data[,m] /where y_data[,m].lt.1e34
    compute me sum adata
    let me me|2  ! Assume source variations are half as much
    let adata adata^2
    compute mr sum adata
    let rp[i,&1] sqrt(mr|in-(me|in)^2)
  next
else
  let ra[,&1] 999
endif
end procedure verify_det

!------------------------------------

begin procedure verify_map
define integer m 
define real ba fac c
define real in me mr
let c 299792458E-6
let fac sqrt(2*log(2))*3600|pi*c|frequency

if (exist(y_data))
  define integer dims[5]    
  compute dims dim y_data   
  let in dims[1]            
  define real adata /like y_data[1] ! New code
  for i 1 to nbas
    let m 2*i
    let adata 0
    let adata y_data[,m] /where y_data[,m].lt.1e34
    compute me sum adata
    let adata adata^2
    compute mr sum adata
    let ba sqrt(dh_uvm[1,i]^2+dh_uvm[2,i]^2)
    let rp[i,&1] sqrt(mr|in-(me|in)^2)|ba*fac
  next
else
 let rp[,&1] 999
endif
end procedure verify_map

!------------------------------------

begin procedure verify_focus
define integer p
define logical f

let p &2-60
if (p.lt.1) then
  let p p+10000
endif
find /proc focu corr /type p /scan 'p' &1
let p found
let f .true.
for /while ((p.ne.found-60.and.p.ne.0).and.f)
   get cx_num[p]
   if (proc.eq.15) then  ! focus
      find /proc corr /type p /scan 'scan' &3
      get f
      let fcoff[1:nant,1] cor_foc[1:nant]
      let f .false.
   endif
   let p p-1
next

find /proc focu corr /type p /scan &1 &3
let p 1
let f .true.
for /while (p.ne.found.and.f)
   get cx_num[p]
   if (proc.eq.15) then  ! focus
      find /proc corr /type p /scan 'scan' &3
      get f
      let fcoff[1:nant,2] cor_foc[1:nant]
      let f .false.
   endif
   let p p+1
next
end procedure verify_focus

!------------------------------------

begin procedure verify_pointing
define integer p
define logical f

let p &2-60
if (p.lt.1) then
  let p p+10000
endif
find /proc poin corr /type p /scan 'p' &1
let p found
let f .true.
for /while ((p.ne.found-60.and.p.ne.0).and.f)
    get cx_num[p]
    if (proc.eq.16) then  ! pointing
       find /proc corr /type p /scan 'scan' &3
       get f
       let azoff[1:nant,1] coll_az[1:nant]
       let eloff[1:nant,1] coll_el[1:nant]
       let f .false.
    endif
    let p p-1
next

find /proc poin corr /type p /scan &1 &3
let p 1
let f .true.
for /while (p.ne.found.and.f)
  get cx_num[p]
  if (proc.eq.16) then  ! pointing
     find /proc corr /type p /scan 'scan' &3
     get f
     let azoff[1:nant,2] coll_az[1:nant]
     let eloff[1:nant,2] coll_el[1:nant]
     let f .false.
  endif
  let p p+1
next
end procedure verify_pointing

!------------------------------------
!
! start here ...
!
sys "rm -f visi* say.* pha-amp-his.dat ampli*.dat phase*.dat point*.dat focus*.dat"
sys "rm -f poi-foc-his.dat table* track*.dat *amp-*.gz *pha-*.gz"
on error continue
if .not.exist(source) then
   var gen
   var pos
   var config
   var rf
endif
if .not.exist(dh_uvm) then
   var data_header
endif
if .not.exist(coll_az) then
   var scanning
endif

! + introduced by jmw, necessary if visib is called "from scratch" 02/08/04
if (.not.exist(phselect)) then
   var line
endif
! -

if ("No&1ne".eq."None") then
  say "Syntax:   visi 22-apr-2002-lb1d"
  return
endif 

! define boundary conditions...
!
define real rpm ram mtk mpt mft see
define char project_type*3 file*80 sfile*80
define logical fex

let project_type "det"                ! map= mapping, det= detection

let see 1.5                      ! max seeing
let rpm 40                       ! max phase rms
let ram 1-exp(-(rpm*pi|180)^2|2) ! max amplitude loss
let mtk 10                       ! max tracking rms (fraction of beam)
let mpt 30                       ! max pointing correction (fraction of beam) = 20% amp loss
let mft 30                       ! max focus correction 20% amp loss = focus off by 30% lambda 

if ("No&2ne".ne."None") then
  let rpm &2
endif

if ("No&3ne".ne."None") then
  let ram &3|100
endif

if ("No&4ne".ne."None") then
  let mpt &4
endif

if ("No&5ne".ne."None") then
  let mft &5
endif

if ("No&6ne".ne."None") then
  let mtk &6
endif

if ("No&7ne".ne."None") then
  let see &7
endif

if ("No&8ne".eq."NoMappingne") then
  let project_type "map"
else
  let project_type "det"
endif

if .not.exist(vd)
  def char vd*80 vf*40 /global
endif
sic user

if ("&1".ne."not yet defined") then
  ! A filename is defined: redefine vd (directory name) and vf (file name)
  sys "echo let vd -                         >  tmpipe/delete.me"  ! Print a Sic line, continued
  sys "dirname '&1' | sed 's/\(.*\)/""\1""/' >> tmpipe/delete.me"  ! Print dirname, surrounded by "". May be "." if no directory supplied
  sys "echo let vf -                         >> tmpipe/delete.me"  ! Print a Sic line, continued
  sys "basename '&1' | sed s/\.hpb//         >> tmpipe/delete.me"  ! Print filename, extension stripped
  @ "tmpipe/delete.me"
  sic delete tmpipe/delete.me
endif

sic dir 'vd'
let fex file("'vf'")
if .not.fex then
  let fex file("'vf'.hpb")
  if fex then
    let file 'vf'.hpb
    let sfile 'vf'
  endif
else
  let file 'vf'
  let sfile 'vf'
endif

if fex then
   sic out table.clic
   if (project_type.eq."det") then
     say "! phase rms  = " rpm " (deg)    - yellow"  /format a15 f4.1 a18
     say "! ampli loss = " ram*100 " (%)      - yellow" /format a15 f4.1 a18
   else
     say "! seeing     = " see " (arcs)   - yellow"  /format a15 f4.1 a18
     say "! ampli loss = " ram*100 " (%)      - yellow" /format a15 f4.1 a18
   endif
   say "! point err  = " mpt " (% FOV)  - magenta"  /format a15 f4.1 a19
   say "! focus err  = " mft " (% wave) - cyan"     /format a15 f4.1 a16
   say "! track rms  = " mtk " (% FOV)  - blue"     /format a15 f4.1 a16
   say " "
   say "set def"
   say "set receiver 1"
   say " "
   say "file both ""!"'file'""""
   say "find"
   say "store flag redu /reset /base all"
   say "store flag redu /reset /ant  all"
   say "!"
   say "! lower quality data: pha + amp + poi + foc + trk check"
   sic out
   sys "rm -f phase.dat ampli.dat"
 else
   say "No such file..."
   return
endif

define character array*12[10] name*12[100] pant*12 
define character iar*20 caln1*12 caln2*12 fant*12
define integer bas[15] i3 i4 i5 i6 i7 i8 i9 fs ls na ns
define real ba speed fac in ptdif[6] fcdif[6] hx[48] hy[48]
define real am1 am2 am3 am4 ut1 ut2 md me ha m1 m2 m3
define real maz[6,2] mel[6,2] lscan a[2] b[2] hm hs
if (.not.exist(rp)) then
  define real rp[15,2] ra[15,2] /global 
  define real azoff[6,2] eloff[6,2] fcoff[6,2] /global
endif
define logical f pt ft at tel dexist[10]  ! max of 10 configs
define integer j m n calm1[20] calm2[20] fla[5,6]
define integer oldscan af al last pti fti if1

let bas 12 13 23 14 24 34 15 25 35 45 16 26 36 46 56
let fla[1] 12 13 14 15 16
let fla[2] 12 23 24 25 26 
let fla[3] 13 23 34 35 36
let fla[4] 14 24 34 45 46 
let fla[5] 15 25 35 45 56
let fla[6] 16 26 36 46 56   

! just 3mm receiver for the time being ...
!
set def
set rece 1
set level 9

if (exist(short_file)) then
  if (short_file.ne.sfile) then
    file in 'vf'
  endif
else
  file in 'file'
endif

find /type p /scan * * /proc corr

!+ temporary section rn, jmw 12/07/04
if (.not.exist(do_atm)) then
   define logical do_atm /global
   let do_atm yes
endif
if (.not.exist(do_wvr)) then
   define logical do_wvr /global
   get f
   define real deleteph /like phselect
   define real deletema 
   let deleteph phselect
   compute deletema max deleteph
   if (deletema.gt.1.5) then
     let do_wvr yes
   else
     let do_wvr no
   endif
   del /var deletema deleteph
endif
if do_atm then
  set phase rel atm nowvr notot
else
   set phase rel noatm nowvr notot
endif
!-

set ampl scal rel
set rf on
set aver scan
set band ave
set baseline all

if (found.ne.0) then 
   get cx_num[1]
   on error return
   let fs scan
   get cx_num[found]
   let ls scan
   find /proc corr /type o /scan 'fs' 'ls'
   if (found.eq.0) then
      say "No source observed..."
      return
   endif
else
   say "No pointing observed..."
   return
endif
on error continue

! check number of array configurations
!
find /type p /scan * * /proc corr
get f
let na 1
let array[na] 'telescope'
for i 2 to found
  get n
  let tel .false.
  for nb 1 to na
    if (array[nb].eq.telescope) then
      let tel .true.
    endif
  next
  if (.not.tel) then
    let na na+1
    let array[na] 'telescope'
  endif
next 
pen 0 /col 0

let m1 mtk
let m2 mpt
let m3 mft
let mtk mtk*48500|frequency
let mpt mpt*48500|frequency
let mft mft*2997.92458|frequency

! for every array configuration do...
!
let iar ""
for ia 1 to na
  find /proc corr /type o /tele 'array[ia]' /scan 'fs' 'ls'
  if (found.ne.0) then
    let iar 'iar'" "'ia'
  endif
next
sym iarray 'iar'
!
g\set plot land

for ia 'iarray'
  say "Treating array "'ia'
  set tele 'array[ia]'
  find /proc corr /type o 
  list /source            ! Builds the C_SOURCE and N_SOURCE variables 
  let ns n_source
  for i 1 to ns
    let name[i] 'c_source[i]'
  next
  
  find /proc corr /sou 'name[1]'
  if (found.ne.0) then
     set y amp pha
     plot /reset
  endif
  let dexist[ia] .true.
  if (.not.exist(y_data)) then
    let dexist[ia] .false.
  endif 

  if (dexist[ia]) then
    for l 1 to ns
      let calm2[1] fs
      for h 1 to 50
        find /proc corr /sou 'name[l]' /scan 'calm2[1]' 'ls'
        if (found.ne.0) then
          get cx_num[1]
          let oldscan scan
          find /proc corr /type p /scan 'calm2[1]' 'scan'
          if (found.ne.0) then
            
            get cx_num[found]
            let caln1 'source'
            let calm1[2] scan
            let j found-1

            let f .true.
            for /while ((j.ne.found-4.and.j.ne.0).and.f)
              get cx_num[j]
              if (source.eq.caln1) then
                let calm1[1] scan
              else
                let f .false.
              endif
              let j j-1
            next
            find /scan 'calm1[1]' 'calm1[2]' /sou 'caln1' /proc corr
            plot
            !
            ! phase + amplitude
            !
 	    @ verify_'project_type' 1
 	    @ verify_amp 1
            !
          endif
	   
! check the foregoing + following pointing and focus correction

	  @ verify_pointing 'calm1[1]' 'calm2[1]' 'ls'
          @ verify_focus 'calm1[1]' 'calm2[1]' 'ls'

          !
          find /proc corr /type p /scan 'oldscan' 'ls'
          if (found.ne.0) then
            get cx_num[1]
            let calm2[1] scan
            let caln2 'source'
            let f .true.
            let j 2
            for /while ((j.ne.found+1.and.j.ne.6).and.f)
              get cx_num[j]
              if (source.eq.caln2.and.scan.eq.calm2[1]+j-1) then
                 let calm2[2] scan
              else
                 let f .false.
              endif
              let j j+1
            next
            find /scan 'calm2[1]' 'calm2[2]' /sou 'caln2' /proc corr
            plot
            !
            ! phase + amplitude
            !
	    @ verify_'project_type' 2
      	    @ verify_amp 2
            !
          endif

!!!!! what do we do here when calm1 or calm2 has not been defined because index
!!!!! was empty ???

          let f .false.
	  let at .false.
	  let ft .false.
          let pt .false.
          let am1 0
          let am2 0
          let am3 0
          let am4 0
	  sic out dummy.dat
	  if (project_type.eq."det") then
            for i 1 to nbas
              if (rp[i,1].gt.rpm.or.rp[i,2].gt.rpm.or.ra[i,1].gt.ram.or.ra[i,2].gt.ram) then
                let f .true.
	        let at .true.
              endif
              let am1 max(rp[i,1],am1)
              let am2 max(rp[i,2],am2)
              let am3 max(ra[i,1],am3)
              let am4 max(ra[i,2],am4)
              say rp[i,1] ra[i,1] /format f10.3 f10.3
              say rp[i,2] ra[i,2] /format f10.3 f10.3  
            next
          else
            for i 1 to nbas
              if (rp[i,1].gt.see.or.rp[i,2].gt.see.or.ra[i,1].gt.ram.or.ra[i,2].gt.ram) then
                let f .true.
	        let at .true.
              endif
              let am1 max(rp[i,1],am1)
              let am2 max(rp[i,2],am2)
              let am3 max(ra[i,1],am3)
              let am4 max(ra[i,2],am4)
              say rp[i,1] ra[i,1] /format f10.3 f10.3
              say rp[i,2] ra[i,2] /format f10.3 f10.3 
           next
	  endif
          sic out
          sys "cat dummy.dat >> pha-amp-his.dat"

	  let ptdif sqrt((azoff[2]-azoff[1])^2+(eloff[2]-eloff[1])^2)
	  let fcdif abs(fcoff[2]-fcoff[1])
	  let pant ""
	  let fant ""
	  let pti 0
	  let fti 0
	  sic out dummy.dat
          for i 1 to nant
	    if (ptdif[i].ge.mpt) then
	      let f .true.
	      let pt .true.
	      let pant 'pant'" "'i'
	      let pti pti+1
	    else
	      let pant 'pant'" 0"
	    endif
	    if (fcdif[i].ge.mft) then
 	      let f .true.
              let ft .true.	
	      let fant 'fant'" "'i'
	      let fti fti+1
	    else
	      let fant 'fant'" 0"
            endif	
            say ptdif[i] fcdif[i] /format f10.3 f10.3
	  next
	  sic out
	  sys "cat dummy.dat >> poi-foc-his.dat"

 	  if (f) then
            sic out say.clic
            if (at) then
	      if (project_type.eq."det") then
                say "find /scan "'calm1[2]'" "'calm2[1]'" /type o    !   pha1rms pha2rms amp1los amp2los"
                for i 1 to nbas
                  if (rp[i,1].gt.rpm.or.rp[i,2].gt.rpm.or.ra[i,1].gt.ram.or.ra[i,2].gt.ram) then
                    say "store flag redu /base " bas[i] "        ! " rp[i,1] rp[i,2] ra[i,1] ra[i,2] /format a22 i2 a10 f9.2 f8.2 f8.2 f8.2
                  endif
                next
              else
                say "find /scan "'calm1[2]'" "'calm2[1]'" /type o    !   seeing1 seeing2 amp1los amp2los"
                for i 1 to nbas
                  if (rp[i,1].gt.see.or.rp[i,2].gt.see.or.ra[i,1].gt.ram.or.ra[i,2].gt.ram) then
                    say "store flag redu /base " bas[i] "        ! " rp[i,1] rp[i,2] ra[i,1] ra[i,2] /format a22 i2 a10 f9.2 f8.2 f8.2 f8.2
                  endif
                next
              endif
            endif
	    if (pt) then
              say "find /scan "'calm1[2]'" "'calm2[1]'" /type o    !   poi1_az poi2_az poi1_el poi2_el"
	      for i 1 to nant
	        if (ptdif[i].gt.mpt) then
                  say "store flag redu /ant  " i "        ! " azoff[i,1] azoff[i,2] eloff[i,1] eloff[i,2] /format a22 i2 a10 f9.2 f8.2 f8.2 f8.2 
	        endif
	      next
            endif
	    if (ft) then
              say "find /scan "'calm1[2]'" "'calm2[1]'" /type o    !    focus1  focus2"
	      for i 1 to nant
	        if (fcdif[i].gt.mft) then
                  say "store flag redu /ant  " i "        ! " fcoff[i,1] fcoff[i,2] /format a22 i2 a10 f9.2 f8.2 
	        endif
	      next
	    endif
            sic out
            sys "cat say.clic >> table.clic"
          endif

          find /scan 'calm1[2]'
          get f
          let ut1 utobs*12|pi
          find /scan 'calm2[1]'
          get f
          let ut2 utobs*12|pi
          for i 1 to nbas
            if (project_type.eq."det") then
              if (rp[i,1].gt.rpm.or.rp[i,2].gt.rpm) then
                sic out say.clic
                say 'ut1'" "'ut2'" "'i'" ! "'bas[i]'" phase"
                sic out
                sys "cat say.clic >> phase-"'ia'".dat"
              endif
            else
              if (rp[i,1].gt.see.or.rp[i,2].gt.see) then
                sic out say.clic
                say 'ut1'" "'ut2'" "'i'" ! "'bas[i]'" phase"
                sic out
                sys "cat say.clic >> phase-"'ia'".dat"
	      endif
            endif
            if (ra[i,1].gt.ram.or.ra[i,2].gt.ram) then
              sic out say.clic
              say 'ut1'" "'ut2'" "'i'" ! "'bas[i]'" amplitude"
              sic out
              sys "cat say.clic >> ampli-"'ia'".dat"
            endif
          next
	  if (pt) then
            sic out say.clic
            say 'ut1'" "'ut2'" "'pant'" ! pointing"
            sic out
            sys "cat say.clic >> point-"'ia'".dat"
          endif
  	  if (ft) then
 	   sic out say.clic
            say 'ut1'" "'ut2'" "'fant'" ! focus"
            sic out
            sys "cat say.clic >> focus-"'ia'".dat"
          endif
        endif
      next
    next
  endif
next

!!!!! what do we do here when calm1 or calm2 has not been defined because index
!!!!! was empty ???

sys "rm -f "'sfile'"-amp-visi-*.ps*"
sys "rm -f "'sfile'"-pha-visi-*.ps*"
sys "rm -f "'sfile'"-poi-visi-*.ps*"
sys "rm -f "'sfile'"-foc-visi-*.ps*"
sys "rm -f "'sfile'"-trk-visi-*.ps*"
sys "rm -f "'sfile'"-his-visi-*.ps*"


for ia 'iarray'
  set tele 'array[ia]'

  if (dexist[ia]) then
    pen 0 /col 0
    find /proc corr /sou *
    set x time
    set y amp /lim 0 2
    plot /id col /reset
    let ut1  24
    let ut2 -24
    for i 1 to n_boxes
      @ choosebox 'i'
      let ut1 min(ut1,user_xmin)
      let ut2 max(ut2,user_xmax)
    next 
    chan dire <greg
    set x time /lim ut1 ut2
!   clear alpha  ! Ambiguous with CLONE
    plot /id col

    if file("ampli-"'ia'".dat") then
      pen 0 /col 5
      col x 1 y 2 z 3 /file ampli-'ia'.dat
      for i1 1 to nxy
        let me z[i1]
        if (x[i1].lt.x_data_min[me].or.x[i1].gt.x_data_max[me]) then
          let x[i1] x[i1]+int((x_data_max[me]-x[i1])/24)*24
        endif
        if (y[i1].lt.x_data_min[me].or.y[i1].gt.x_data_max[me]) then
          let y[i1] y[i1]+int((x_data_max[me]-y[i1])/24)*24
        endif
      next

      for i 1 to n_boxes
        @ choosebox 'i' 

        for i1 1 to nxy
          if (i.eq.z[i1]) then
            draw rel x[i1] 0 /user
            draw lin x[i1] 2 /user
            draw rel y[i1] 0 /user
            draw lin y[i1] 2 /user
            for i2 0 to 40
              let a[1] x[i1]
              let b[1] 1.9-i2*0.1
              let a[2] y[i1]
              let b[2] b[1]+(a[2]-a[1])|(x_data_max[i]-x_data_min[i])*(box_xmax-box_xmin)|(box_ymax-box_ymin)
              conn a b
            next
          endif
        next
      next
    endif
    chan dire <greg
    pen 0 /col 0
    hard 'sfile'-amp-visi-'ia'.ps /dev ps color

    find /proc corr /type p
    set phase jum
    set y phase /lim -180 180
!   clear alpha
    plot /id col

    if file("phase-"'ia'".dat") then
      pen 0 /col 5
      col x 1 y 2 z 3 /file phase-'ia'.dat
      for i1 1 to nxy
        let me z[i1]
        if (x[i1].lt.x_data_min[me].or.x[i1].gt.x_data_max[me]) then
          let x[i1] x[i1]+int((x_data_max[me]-x[i1])/24)*24
        endif
        if (y[i1].lt.x_data_min[me].or.y[i1].gt.x_data_max[me]) then
          let y[i1] y[i1]+int((x_data_max[me]-y[i1])/24)*24
        endif
      next

      for i 1 to n_boxes
        @ choosebox 'i' 

        for i1 1 to nxy
          if (i.eq.z[i1]) then
            draw rel x[i1] -170 /user
            draw lin x[i1] +170 /user
            draw rel y[i1] -170 /user
            draw lin y[i1] +170 /user
              for i2 0 to 40
              let a[1] x[i1]
              let b[1] 175-i2*20
              let a[2] y[i1]
              let b[2] b[1]+(a[2]-a[1])|(x_data_max[i]-x_data_min[i])*(box_xmax-box_xmin)|(box_ymax-box_ymin)*180
              conn a b
            next
          endif
        next
      next
    endif
    chan dire <greg
    pen 0 /col 0
    hard 'sfile'-pha-visi-'ia'.ps /dev ps color
  endif
next

for ia 'iarray'
  set tele 'array[ia]'

  if (dexist[ia]) then
    find /proc corr /sou *
    set x time
    set y amp /lim 0 2
    plot /id col /reset
    let ut1  24
    let ut2 -24
    for i 1 to n_boxes
      @ choosebox 'i'
      let ut1 min(ut1,user_xmin)
      let ut2 max(ut2,user_xmax)
    next 
    chan dire <greg
    set x time /lim ut1 ut2
!   clear alpha  ! Ambiguous with CLONE

    plot /id col
    if file("point-"'ia'".dat") then
    
      pen 0 /col 6
      col x 1 y 2 /file point-'ia'.dat
    
      for i 1 to n_boxes
        @ choosebox 'i' 

        for i1 1 to nxy
          for j2 1+2 to nant+2
            col z 'j2' /file point-'ia'.dat 
   	    if (z[i1].ne.0) then
  	      let i4 z[i1]
  	      for j1 1 to nant-1
                let i3 fla[j1,i4]
                if (i3.eq.bas[i]) then
                  if (x[i1].lt.x_data_min[i].or.x[i1].gt.x_data_max[i]) then
                    let x[i1] x[i1]+int((x_data_max[i]-x[i1])/24)*24
                  endif
                  if (y[i1].lt.x_data_min[i].or.y[i1].gt.x_data_max[i]) then
                    let y[i1] y[i1]+int((x_data_max[i]-y[i1])/24)*24
                  endif
                  draw rel x[i1] 0 /user
                  draw lin x[i1] 2 /user
                  draw rel y[i1] 0 /user
                  draw lin y[i1] 2 /user
                  for i2 0 to 40
                    let a[1] x[i1]
                    let b[1] 1.9-i2*0.1
                    let a[2] y[i1]
                    let b[2] b[1]+(a[2]-a[1])|(x_data_max[i]-x_data_min[i])*(box_xmax-box_xmin)|(box_ymax-box_ymin)
                    conn a b
                  next
                endif
              next
            endif
          next
        next
      next
      endif
      chan dire <greg
      pen 0 /col 0
      hard 'sfile'-poi-visi-'ia'.ps /dev ps color

      plot /id col ! (rn 27/05) 
      if file("focus-"'ia'".dat") then
    
!     clear alpha  ! Ambiguous with CLONE
      plot /id col
      pen 0 /col 4
      col x 1 y 2 /file focus-'ia'.dat
    
      for i 1 to n_boxes
        @ choosebox 'i' 

        for i1 1 to nxy
          for j2 1+2 to nant+2
            col z 'j2' /file focus-'ia'.dat
  	    if (z[i1].ne.0) then
  	      let i4 z[i1]
  	      for j1 1 to nant-1
                let i3 fla[j1,i4]
                if (i3.eq.bas[i]) then
                  if (x[i1].lt.x_data_min[i].or.x[i1].gt.x_data_max[i]) then
                    let x[i1] x[i1]+int((x_data_max[i]-x[i1])/24)*24
                  endif
                  if (y[i1].lt.x_data_min[i].or.y[i1].gt.x_data_max[i]) then
                    let y[i1] y[i1]+int((x_data_max[i]-y[i1])/24)*24
                  endif
                  draw rel x[i1] 0 /user
                  draw lin x[i1] 2 /user
                  draw rel y[i1] 0 /user
                  draw lin y[i1] 2 /user
                  for i2 0 to 40
                    let a[1] x[i1]
                    let b[1] 1.9-i2*0.1
                    let a[2] y[i1]
                    let b[2] b[1]+(a[2]-a[1])|(x_data_max[i]-x_data_min[i])*(box_xmax-box_xmin)|(box_ymax-box_ymin)
                    conn a b
                  next
                endif
              next
            endif
          next
        next
      next
    endif
    chan dire <greg
    pen 0 /col 0
    hard 'sfile'-foc-visi-'ia'.ps /dev ps color
  endif
next

for ia 'iarray'
  if (dexist[ia]) then
    find /scan 'fs' 'ls' /proc corr /tele 'array[ia]' /type o 
    set ant all
    set x scan

! tracking az + el
!
    set y az_err el_err
    plot
    if exist(y_data) then
      define real tscan dscan ascan /like y_data
      define real iscan /like y_data[1]
      let iscan[i] i
      let y_data -1000 /where y_data.gt.1e34
      let tscan x_data /where y_data.gt.mtk
      let dscan y_data /where y_data.gt.mtk
      for i 1 to nant
        let tscan['2*i-1'] x_data['2*i-1'] /where sqrt(dscan['2*i-1']^2+dscan['2*i']^2).gt.mtk
        let tscan['2*i'] tscan['2*i-1']
        let ascan[i] sqrt(dscan['2*i-1']^2+dscan['2*i']^2)
      next
      sic out say.clic
      say "!"
      say "! tracking errors in az and el"
      sic out
      sys "cat say.clic >> table.clic"

      set baseline all
      set x time /lim ut1 ut2
      set y amp /lim 0 2
!     clear alpha  ! Ambiguous with CLONE
      pen 0 /col 0
      find /sou * /proc corr /tele 'array[ia]'
      plot /id col
      compute me max tscan
      if (me.gt.0) then

        compute me max iscan
        for i 1 to nant
          let i8 1
          let i6 0
          for /while (i8.ne.nint(me+1))
            if (tscan[i8,'2*i'].ne.0.and.i6.eq.0) then
              let i6 tscan[i8,'2*i']
	      let if1 i8
            endif
            if (tscan[i8,'2*i'].ne.0.and.i6.ne.0) then
              let i7 tscan[i8,'2*i']
            endif
            if ((tscan[i8,'2*i'].eq.0.or.i8.eq.nint(me)).and.i6.ne.0) then

              sic out say.clic
              say "find /scan "'i6'" "'i7'" /proc corr /type o"
              say "store flag redu /ant "'i'
              sic out
              sys "cat say.clic >> table.clic"

  	      find /scan 'i6'
              get f
              let ut1 utobs*12|pi
  	      find /scan 'i7'
              get f
              let ut2 utobs*12|pi
              sic out say.clic
              say 'ut1'" "'ut2'" "'i'" ! pointing"
              sic out
              sys "cat say.clic >> track-"'ia'".dat"

              let i6 0
           endif
            let i8 i8+1
          next
        next

        pen 0 /col 3
        col x 1 y 2 z 3 /file track-'ia'.dat
        for i 1 to n_boxes
          @ choosebox 'i' 
  
          for i1 1 to nxy
     	  if (z[i1].ne.0) then
   	     let i4 z[i1]
   	     for j1 1 to nant-1
                let i3 fla[j1,i4]
                if (i3.eq.bas[i]) then
                  if (x[i1].lt.x_data_min[i].or.x[i1].gt.x_data_max[i]) then
                    let x[i1] x[i1]+int((x_data_max[i]-x[i1])/24)*24
                   endif
                  if (y[i1].lt.x_data_min[i].or.y[i1].gt.x_data_max[i]) then
                    let y[i1] y[i1]+int((x_data_max[i]-y[i1])/24)*24
                  endif
                  draw rel x[i1] 0 /user
                  draw lin x[i1] 2 /user
                  draw rel y[i1] 0 /user
                  draw lin y[i1] 2 /user
                  for i2 0 to 40
                    let a[1] x[i1]
                    let b[1] 1.9-i2*0.1
                    let a[2] y[i1]
                    let b[2] b[1]+(a[2]-a[1])|(x_data_max[i]-x_data_min[i])*(box_xmax-box_xmin)|(box_ymax-box_ymin)
                    conn a b
                  next
                endif
              next
            endif 
          next
        next
      endif
      del /var tscan iscan dscan
    endif
    chan dire <greg
    pen 0 /col 0
    hard 'sfile'-trk-visi-'ia'.ps /dev ps color           

    sic out say.clic
    say "!"
    say "! data after last calibration ("'array[ia]'")"
    find /type p /proc corr /tele 'array[ia]'
    get f
    let af scan-1
    if (af.eq.0) then
      let af 9999
    endif
    get cx_num[found]
    let al scan
    find /scan 'al' 'af' /type o /proc corr /tele 'array[ia]'  ! inverted "al" and "af"
    if (found.ne.0) then
      find /scan 'af' 'al' /type p /proc corr /tele 'array[ia]'
      set x scan
      set y amp
      set baseline all
      plot
      let x_data 0 /where y_data.gt.1e34
      for k 1 to nbas
        if (al.gt.af) then
          compute lscan max x_data[k]
        else
          let x_data[k] x_data[k]+10000 /where x_data[k].lt.al
          compute lscan max x_data[k]
          if (lscan.gt.9999) then
  	  let lscan lscan-9999
          endif 
        endif
        let last nint(lscan)+1
        say "find /scan "'last'" * /type o /proc corr /tel "'array[ia]'
        say "store flag redu /base "'bas[k]'
      next
    endif 
    sic out
    sys "cat say.clic >> table.clic"
  endif

  set ant 1
  set y az_err
  find /proc corr
  plot 
  for i 1 to 6
    cle seg
  next 

! amp + pha histo
!
  g\set exp .75
  col x 1 y 2 /file pha-amp-his.dat
  define real am[nxy]
  let am nint((1-y)*480|12)
  let am 12 /where am.lt.12
  let am 48 /where am.gt.48
  let hx[i] i/(480|12)*100
  let hy 0
  let hm 0
  for i 1 to nxy
    let hy['am[i]'] hy['am[i]']+1
  next
  for i 1 to 48
    if (hx[i].le.(1-ram)*100) then
      let hm hm+hy[i] 
    endif
  next
  compute hs sum hy
  let hm hm|hs*100
  lim 30 * 0 * /var hx hy
  g\set box 1 9.5 13 17
  histo hx hy /fill 5 /base 0
  histo hx hy
  
  box 
  draw text 0 0.4 "Flagged "'nint(hm)'"%" 6 0 /box 7
  label "Amplitude Scaled (%)" /x
  draw rel (1-ram)*100 user_ymin /user
  draw lin (1-ram)*100 user_ymax /user
  for i2 -20 to 20
    let a[1] 30
    let b[1] user_ymin+i2*(user_ymax-user_ymin)/20
    let a[2] (1-ram)*100
    let b[2] b[1]+(a[2]-a[1])|(user_xmax-user_xmin)*(user_ymax-user_ymin)
    conn a b
  next 

  define real ph[nxy]
  if (project_type.eq."det") then
    let ph nint(x|(110|48))
    let ph  1 /where ph.lt.1
    let ph 48 /where ph.gt.48
    let hx[i] i*(110|48)
    let hy 0
    let hm 0
    for i 1 to nxy
      let hy['ph[i]'] hy['ph[i]']+1
    next
    for i 1 to 48
      if (hx[i].ge.rpm) then
        let hm hm+hy[i] 
      endif
    next
    compute hs sum hy
    let hm hm|hs*100
    lim * * 0 * /var hx hy
    g\set box 11.25 19.75 13 17
    draw text 0 0.4 "Flagged "'nint(hm)'"% - Detection" 6 0 /box 7
    label "Phase RMS (deg)" /x
    histo hx hy /fill 5 /base 0
    histo hx hy
    box 
    draw rel rpm user_ymin /user
    draw lin rpm user_ymax /user
    for i2 -20 to 20
      let a[1] rpm
      let b[1] user_ymin+i2*(user_ymax-user_ymin)/20
      let a[2] user_xmax
      let b[2] b[1]+(a[2]-a[1])|(user_xmax-user_xmin)*(user_ymax-user_ymin)
      conn a b
    next
  else
    let ph nint(x|(3|48))
    let ph  1 /where ph.lt.1
    let ph 48 /where ph.gt.48
    let hx[i] i*(3|48)
   let hy 0
    for i 1 to nxy
      let hy['ph[i]'] hy['ph[i]']+1
    next
    for i 1 to 48
      if (hx[i].ge.see) then
        let hm hm+hy[i] 
      endif
    next
    compute hs sum hy
    let hm hm|hs*100
    lim * * 0 * /var hx hy
    g\set box 11.25 19.75 13 17
    draw text 0 0.4 "Flagged "'nint(hm)'"% - Mapping" 6 0 /box 7
    label "Seeing (arcs)" /x
    histo hx hy /fill 5 /base 0
    histo hx hy
    box
    draw rel see user_ymin /user
    draw lin see user_ymax /user
    for i2 -20 to 20
      let a[1] see
      let b[1] user_ymin+i2*(user_ymax-user_ymin)/20
      let a[2] user_xmax
      let b[2] b[1]+(a[2]-a[1])|(user_xmax-user_xmin)*(user_ymax-user_ymin)
      conn a b
    next
  endif

! poi + foc histo
!
  col x 1 y 2 /file poi-foc-his.dat
  define real po[nxy]
  let po nint(x*2)
  let po  1 /where po.lt.1
  let po 48 /where po.gt.48
  let hx[i] i/2
  let hy 0
  let hm 0
  for i 1 to nxy
    let hy['po[i]'] hy['po[i]']+1
  next
  for i 1 to 48
    if (hx[i].gt.mpt) then
      let hm hm+hy[i] 
    endif
  next
  compute hs sum hy
  let hm hm|hs*100
  lim 0 * 0 * /var hx hy
  g\set box 21.5 30 13 17
  histo hx hy /fill 6 /base 0
  histo hx hy
  draw text 0 0.6 "Flagged "'nint(hm)'"% " 6 0 /box 7
  draw text 0 0.6 "(% FOV)" 4 0 /box 9
  label "Pointing Corr. (arcs)" /x
  draw rel mpt user_ymin /user
  draw lin mpt user_ymax /user
  for i2 -20 to 20
    let a[1] mpt
    let b[1] user_ymin+i2*(user_ymax-user_ymin)/20
    let a[2] user_xmax
    let b[2] b[1]+(a[2]-a[1])|(user_xmax-user_xmin)*(user_ymax-user_ymin)
    conn a b
  next 
  axis xl /label p
  axis yl /label o
  axis yr
  lim = user_xmax|48500*frequency = = 
  axis xu
  g\set box 21.5 30 13 16.8
  axis xu /label p only

  define real fo[nxy]
  let fo nint(y*30)
  let fo  1 /where fo.lt.1
  let fo 48 /where fo.gt.48
  let hx[i] i/30
  let hy 0
  let hm 0
  for i 1 to nxy
    let hy['fo[i]'] hy['fo[i]']+1
  next
  for i 1 to 48
    if (hx[i].gt.mft) then
      let hm hm+hy[i] 
    endif
  next
  compute hs sum hy
  let hm hm|hs*100
  lim 0 * 0 * /var hx hy
  g\set box 1 9.5 7 11
  histo hx hy /fill 4 /base 0
  histo hx hy
  draw text 0 0.6 "Flagged "'nint(hm)'"% " 6 0 /box 7
  draw text 0 0.6 "(% \Gl)" 4 0 /box 9
  label "Focus Corr. (mm)" /x
  draw rel mft user_ymin /user
  draw lin mft user_ymax /user
  for i2 -20 to 20
    let a[1] mft
    let b[1] user_ymin+i2*(user_ymax-user_ymin)/20
    let a[2] user_xmax
    let b[2] b[1]+(a[2]-a[1])|(user_xmax-user_xmin)*(user_ymax-user_ymin)
    conn a b
  next 
  axis xl /label p
  axis yl /label o
  axis yr
  g\set box 1 9.5 7 11
  lim = user_xmax|2997.92458*frequency = = 
  axis xu
  g\set box 1 9.5 7 10.8
  axis xu /label p only
  g\set exp 1.
  pen 0 /col 0
  hard 'sfile'-his-visi-'ia'.ps /dev ps color
next


sic out say.clic
say " " 
say "set receiver *" 
sic out
sys "cat say.clic >> table.clic"

sys "mv table.clic "'sfile'"-visi.clic"
sic out visi_a.tex
say "\documentclass[11pt]{article} \usepackage{graphicx}"
say "\oddsidemargin 0pt \evensidemargin 0pt \topmargin 0pt"  
say "\marginparwidth 50pt \textheight 650pt \textwidth 460pt"  
say "\pagestyle{headings} \sloppy"
say "\title {Project "'project'"\\"
say " Observed on "'date_observed'"}"
say "\author{\Large Automatic Visibility Quality Assessment Report\\"
say "\normalsize by CLIC @\,x\_visi}"
say "\begin{document} \maketitle"
say "\vskip 2cm"
say "The procedure analyzes the 3mm data only. To assess the"
say "quality of the 1mm data, use the 3mm input parameters"
say "appropriately scaled to the 1mm observing frequency. Also, do not"
say "apply blindly the output of the automatic quality assessment procedure"
say "to the data, but do it with granu salis as it may otherwise result in"
say "a big loss of data. \def\la{\normalsize}"
say "\mbox{}\vskip 1cm \begin{center} \begin{tabular}{lr}"
if (project_type.eq."det") then
  say " {\la Project type:}   & \hskip 1cm DETECTION \\"
  say " & \\"
  say " {\la Phase loss:}     & \hskip 1cm $\leq "'rpm'"^\circ$ RMS \\"
else
  say " {\la Project type:}   & \hskip 1cm MAPPING \\"
  say " & \\"
  say " {\la Seeing limit:}   & \hskip 1cm $\leq "'nint(see*10)/10'"''$ \\"
endif
say " {\la  Amplitude loss:} & \hskip 1cm $\leq " 'nint(ram*1000)/10'"$\% \\"
say " {\la  Pointing error:} & \hskip 1cm $\leq "'m2'"$\% (FOV) $\simeq "'nint(mpt*10)/10'"''$ \\"
say " {\la  Focus error:}    & \hskip 1cm $\leq "'m3'"$\% ($\lambda$) $\simeq "'nint(mft*10)/10'"$ mm \\"
say " {\la  Tracking error:} & \hskip 1cm $\leq "'m1'"$\% (FOV) $\simeq "'nint(mtk*10)/10'"''$ \\"
say " & \\ & \\"
say " {\la Configuration(s):} "
for ia 'iarray'
  set tel 'array[ia]'
  find 
  get cx_num[1]
  let fs cx_num[1]
  let ls cx_num[found]
  if (dexist[ia]) then
    say "& \multicolumn{1}{l}{\hskip 2cm "'ia'".\hskip 5mm "'configuration'" }\\"
  else
    say "& \multicolumn{1}{l}{\hskip 2cm "'ia'".\hskip 5mm "'configuration'" -- NO CALIBRATED DATA }\\"
  endif
next
say "\end{tabular} \end{center}"


for ia 'iarray'
  if (dexist[ia]) then
    find /tel 'array[ia]'
    get f
    say "\section*{\normalsize "'ia'".1\ Amplitude Assessment \hfill "'configuration'"}"
    say "\includegraphics[width=16cm]{"'sfile'"-amp-visi-"'ia'"}"
    say "\section*{\normalsize "'ia'".2\ Phase Assessment \hfill "'configuration'"}"
    say "\includegraphics[width=16cm]{"'sfile'"-pha-visi-"'ia'"}"
    say "\section*{\normalsize "'ia'".3\ Pointing Assessment \hfill "'configuration'"}"
    say "\includegraphics[width=16cm]{"'sfile'"-poi-visi-"'ia'"}"
    say "\section*{\normalsize "'ia'".4\ Focus Assessment \hfill "'configuration'"}"
    say "\includegraphics[width=16cm]{"'sfile'"-foc-visi-"'ia'"}"
    say "\section*{\normalsize "'ia'".5\ Tracking Assessment \hfill "'configuration'"}"
    say "\includegraphics[width=16cm]{"'sfile'"-trk-visi-"'ia'"}"
    say "\section*{\normalsize "'ia'".6\ Statistical Assessment \hfill "'configuration'"}"
    say "\includegraphics[width=16cm]{"'sfile'"-his-visi-"'ia'"}"
  endif
next
say "\begin{verbatim}"
say "Content of file: "'sfile'"-visi.clic"
say "-------------------------------------------"
say " "
sic out visi_b.tex
say "\end{verbatim}" 
say "\end{document}"
sic out
sys "cat "'sfile'"-visi.clic >> visi_a.tex; cat visi_b.tex >> visi_a.tex" 
sys "latex visi_a.tex; dvips visi_a -o "'sfile'"-visi.ps -pp1:"
sys "gzip -f "'sfile'"-*visi.ps"
sys "rm -f visi_?.* say.* *-???-visi-?.ps ampli*.dat phase*.dat point*.dat focus*.dat table* *amp-*.gz *pha-*.gz"
sys "rm -f dummy.dat* pha-amp-his.dat* poi-foc-his.dat* track*.dat" 
sic user
if ("'sys_info'".eq."observer") then
  sys "test -r /project/SECURED-FILES/transferred && rm -f /project/SECURED-FILES/*"
  sys "cp "'sfile'"-visi.* /project/SECURED-FILES/"
! sys "gunzip -f "'sfile'"-*visi.ps.gz"
endif
del /sym iarray 
