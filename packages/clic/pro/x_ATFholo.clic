!---*-f90-*--------------------------------------------------------------------
begin procedure  x_ATFholo_create
  let hfile_name "test"'hfscan'
  on error quit
  directory "HOLODATA:TEST*"'hfscan'".FITS"
  if hfscan.lt.10 then
     directory "HOLODATA:TEST000"'hfscan'".FITS"
  else if hfscan.lt.100 then
     directory "HOLODATA:TEST00"'hfscan'".FITS"
  else if hfscan.lt.1000 then
     directory "HOLODATA:TEST0"'hfscan'".FITS"
  else
     directory "HOLODATA:TEST"'hfscan'".FITS"
  endif
  !  if dir_nfile.eq.0 then
  !     directory "TEST*"'hfscan'".FITS"
  !  endif
  if hfinit then 
    sic delete 'hfile_name'.hpb
    file out 'hfile_name' NEW
  else
    file out 'hfile_name'
  endif
  for i 1 to dir_nfile
    read 'dir_file[i]'
  next
  let hOLD_FILE "Perime"
end procedure   x_ATFholo_create
!------------------------------------------------------------------------------
begin procedure  x_ATFholo_calibrate
!
! Solve Amplitude Phase for ATH Holography.
!
  IF (hOLD_FILE.NE.hFILE_NAME) THEN
    SAY "Please use ""SELECT"" option before"
    RETURN
  ENDIF
  set plot points
  set baseline all
  set rf off
  set scan first_scan last_scan
  set receiver hrec
  set phase internal
  FIND /PROC CORR /TYPE P ! /OFF 0 0
  if found.le.0 then
    find /proc corr   /type *
  endif
  SET X TIME
  SET BAND USB
  set aver time 5.
! avoid the first points which are not on source
  set record 151 to 1000000
  !
  set sub all 
  !
  set y amplitude phase
  set phase absolute
  plot /id color
  set step 'hcalstep'
  if found/5.gt.4 then
    solve amplitude phase /plot
    say "Calibration: "
    say "Command was SOLVE AMPLITUDE PHASE /PLOT"
  else
    solve amplitude phase /pol '(found/5-1)' /plot
    say "Calibration: "
    say "Command was SOLVE AMPLITUDE PHASE /poly "'(found/5-1)'" /PLOT"
  endif
  STOP 
  !
  FIND /PROCED CORR HOLO   /TYPE *
  SAY "... storing calibration curves ..."
  set level 6
  STORE AMPLITUDE PHASE /BAND USB
  set level 4
end procedure  x_ATFholo_calibrate
!---------------------------------------------------------------------------
begin procedure x_ATFholo_select
!
! Open the file
!
if (HFILE_NAME.ne."not yet defined") then
  on error FILE BO "!"'HFILE_NAME'
  FILE BO 'HFILE_NAME'
  on error
  if hold_file.ne.HFILE_NAME then
    if exist(RF_USB) then
      DELETE /VARIABLE RF_USB RF_LSB
    endif
    if exist(amp_err) then
      DELETE /VARIABLE AMP_ERR 
    endif
    if exist(pha_err) then
      DELETE /VARIABLE PHA_ERR
    endif
  endif
  LET hOLD_FILE 'HFILE_NAME'
else
  SAY "E-HOLO_SELECT, Please define the file name (Press FILE)"
  return
endif
SET TIME *
!
! 1.2 Define additional parameters
!
SIC PREC DOUBLE
!
! Reset CLIC defaults
!
SET DEFAULT
set flux all /reset
set quality 'hmin_qual'
set level 6
set receiver hrec
set phase internal
SET PHAS BASE ABS
SET RF   BASE OFF
SET AMPL BASE ABS
SET PHAS CONT
SET PLOT BAR
!
IF (.NOT.EXIST(SCAN)) THEN
   VARIABLE GENERAL ON
ENDIF
IF (.NOT.EXIST(NBAS)) THEN
   VARIABLE CONFIG ON
ENDIF
IF (.NOT.EXIST(IN_FILE)) THEN
   VARIABLE READ_WRITE ON
ENDIF
IF (.NOT.EXIST(N_LINE_BANDS)) THEN
   VAR LINE ON
ENDIF
IF (.NOT.EXIST(FLUX)) THEN
   VAR POSITION ON
ENDIF
IF (.NOT.EXIST(ISB)) THEN
   VAR RF ON
ENDIF
IF (.NOT.EXIST(GAIN_IMAGE)) THEN
   VAR ATM ON
ENDIF
IF (.NOT.EXIST(dh_offlam)) THEN
   VAR data_header ON
ENDIF
IF (EXIST(CAL_BAND)) THEN
  del /var CAL_BAND BAND_SOURCE  b_flux
endif
DEFINE CHARACTER CAL_BAND*3 BAND_SOURCE*12 /GLOBAL
define double  b_flux /global
!
SET AVER SCAN
!
! 2 --- Define the subbands
!
SET SCAN hSCAN[1] hSCAN[2]
FIND
GET FIRST
SET BASELINE ALL
set sub all
if .not.exist(rf_usb) then
  DEFINE REAL RF_USB[2,NBAS] RF_LSB[2,NBAS] /GLOBAL
endif
if .not.exist(amp_err) then
  DEFINE REAL AMP_ERR[NBAS] /GLOBAL
endif
if .not.exist(PHA_ERR) then
  define real PHA_ERR[NBAS] /GLOBAL
endif
!
! 4 --- Find the bandpass calibrator
! 
say " "
FIND /SOURCE * /TYPE  P /PROC CORR 
if (found.eq.0) then
  FIND /SOURCE * /TYPE * /PROC CORR 
endif  
LIST /SOURCE
if n_source.le.0 then
   say "No calibrators"
   return
endif
LET B_FLUX 0
FOR I 1 TO N_SOURCE
   FIND /SOURCE 'C_SOURCE[I]' /PROC CORR
   GET FIRST
   SAY "Source " 'I' 'SOURCE' " Flux " 'FLUX' " Jy" -
                       /format a i2 a12 a f6.2 a
   IF (flux.GT.B_FLUX) THEN  
     LET BAND_SOURCE 'C_SOURCE[I]'
     LET B_FLUX   flux
   ENDIF
NEXT
SAY " "
SAY "Found Bandpass Calibrator "'BAND_SOURCE'
!
DEFINE REAL A
FIND /SOURCE 'band_source' /PROC CORR 
get f
let a 10.
for i 1 to nant
   let a min(a,gain_image[i])
next
IF (A.LT.0.2) THEN
   IF (ISB.EQ.-1) THEN   ! Signal == LSB
      LET CAL_BAND LSB
   ELSE
      LET CAL_BAND USB
   ENDIF
ELSE
   LET CAL_BAND DSB
ENDIF
SAY " "
SAY 'CAL_BAND'" tuning "
!
! Find first and last scan for Phase calibration
!
if .not.exist(first_scan) then
   define integer first_scan last_scan /global
endif
find /proc holo
!
get cx_num[1]
let i1 scan /new integer
get cx_num[found]
let i2 scan /new integer
! if scan 1 in range:
if i2-i1.gt.9000 then
   fin /proc holo  /scan 5000 10000
   get cx_num[1]
   let i2 scan
   fin /proc holo  /scan 1 5000 
   get cx_num[found]
   let i1 scan
endif
fin /proc corr   /scan mod(i1-20-1,10000)+1 i1
if found.ne.0 then
  get cx_num[found]
  let first_scan scan
else
  let first_scan i1
endif
fin /proc corr   /scan i2 mod(i2+20-1,10000)+1
if found.ne.0 then
  get cx_num[1]
  let last_scan scan
else
  let last_scan i2
endif
say "Scan range: "'first_scan'" to "'last_scan'
!
! Decide on the subb range : all or only the narrows (masers)
!

if .not.exist(sub_chain) then
  def character sub_chain*80 /global
endif
if .not.exist(n_sub_bands) then
  variables continuum
endif
def real  mwidth 
if hmode.eq.1 then
  let mwidth 330.
else
  let mwidth 20.
endif 
fin /proc corr   /scan 'first_scan' 'last_scan'
get first
let sub_chain "ALL" 
for i 1 to n_sub_bands
  if cfwid[i].le.mwidth then
    if sub_chain.eq."ALL" then
      let sub_chain "C0"'i'
    else
      let sub_chain 'sub_chain'" and C0"'i'
    endif
  endif
next
Say "Subbands used : "'sub_chain'
!
!
find /proc holo    /scan 'first_scan' 'last_scan'
!
! Now check which antenna moved in that scan range:
!
get first
set antenna all
set x number
set y beta
!
def integer nmob j
def real bmin[nant] bmax[nant] imob[nant] smin[nant] smax[nant]
!
set lev 6
set aver time 10
set antenna all
plot 
let nmob 0
for i 1 to n_boxes
  def real wy /like y_data[i]
  let wy y_data[i]*w_data[i]
  compute bmin[i] min wy
  compute bmax[i] max wy
  del /var wy
  if bmin[i].ne.bmax[i] then
    let nmob nmob+1
    let imob[nmob] i
  endif
next
if nmob.eq.0 then
  say "No antenna was scanned."
  return
else if nmob.ne.1 then
  say 'nmob'" antennas were scanned.:"
  for i 1 to nmob
    let j imob[i]
    say "Antenna "'phys_ant[j]'" (Logical "'j'")"
  next
else
  let j imob[1]
  say "Antenna "'phys_ant[j]'" (Logical "'j'")  was scanned."
  let hcant 'j' 
endif
!
! identify holographies by antennas
!
if exist(n_holo) then
  del /var n_holo scan_holo
endif
define integer n_holo[6] scan_holo[2,100,6] /global
define integer snew sold nh
define real yold ynew
!
let n_holo 0
let scan_holo 0
for ka 1 to n_boxes
  let yold 0
  let sold 0
  let nh 0
  for i 1 to n_data[ka]
    let ynew y_data[i,ka]
    let snew x_data[i,ka]
    if (ynew.lt.0).and.(ynew.lt.yold) then
      let nh nh+1 
      let scan_holo[1,nh,ka] snew
    else if (ynew.gt.yold).and.(nh.gt.0) then
      let scan_holo[2,nh,ka] snew
    endif
    let yold ynew
    let sold snew
  next
  let n_holo[ka] nh
  if nh.gt.0 then
    say " " 
    say "Holographies on Antenna " 'ka' ":"
    say "No " "From scan" "to scan"  /format a10 a10 a10 
    say " " 
    for i 1 to n_holo[ka] 
       say i scan_holo[1,i,ka] scan_holo[2,i,ka] /format i10 i10 i10 
    next
  else
    say "antenna " 'ka' " was not scanned."
  endif
next
return
end procedure x_ATFholo_select



begin procedure concat
  symbol &1 "&2 &3 &4 &5 &6 &7 &8 &9"
end procedure concat

!------------------------------------------------------------------------------
! x_holo_solve.clic 
! called by x_ATFholo.clic
! RL 13-may-1997
! SOLVE and PLOT Holographic map, after calibration
!------------------------------------------------------------------------------
begin procedure x_ATFholo_solve
  define integer thesize[4]
  let thesize 64 128 256 512
  set scan hscan[1] hscan[2]
  g\set plot landscape
  find /proc  holo /scan first_scan last_scan
  ! no rf cal needed...
  set rf off
  set phase relative baseline noatm
  set amplitude baseline relative
  ! not really needed...
  set antenna 1
  set band 'hBand'
  set sub ALL
  set record 1 to 1000000
  !
  if hastig then
    symbol ast "AS"
  else
    symbol ast " "
  endif
  if hFeed then
    symbol nofeed " "
  else
    symbol nofeed "NOFEED"
  endif
  if hApodize then
    symbol apodi "APODIZE"
  else
    symbol apodi " "
  endif
  if hFresnel then
    symbol fr "FRESNEL"
  else
    symbol fr " "
  endif
  @ concat s1 NP 'thesize[hsize]' 'AST' 'FR' 'NOFEED' 'apodi'
  symbol s1
  if hFixFocus then
    symbol ffoc "NOFOCUS"
  else
    symbol ffoc ""
  endif
  if hFixXYFocus then
    symbol fxyfoc "NOXYFOCUS"
  else
    symbol fxyfoc ""
  endif
  if hNMask.gt.0 then
    symbol maskp "MASK "'hNMask'" "'hMask'
  else
    symbol maskp " "
  endif
  if telescope.eq."VTX-ALMATI" then
!    valid for Vertex antenna
! reference plane is now mid-plane of dish, not az axis. 
! 314.7-2.18-1.875/2.
! was     symbol close "DEFOCUS 0.091 DISTANCE 314.7" 
     symbol close "DEFOCUS 0.091 DISTANCE 311.6" 
  else if telescope.eq."AEC-ALMATI" then
!    valid for AEC antenna in 2004
!     symbol close "DEFOCUS 0.083 DISTANCE 302" 
!    valid for AEC antenna in 2005
     symbol close "DEFOCUS 0.093 DISTANCE 298.9" 
  endif
  @ concat s2  'ffoc' 'fxyfoc' 'close' 
  symbol s2
  symbol poffset "POINTING "'hPointOffset[1]'" "'hPointOffset[2]'
  @ concat s3  FUDGE 'hFudge' 'poffset' CHEAT '0.01*nint(100*hcheat[1])' '0.01*nint(100*hcheat[2])'
  symbol s3
  @ concat s4 "/OFFSET" 'hFocusOffset[1]|1000' 'hFocusOffset[2]|1000' 'hFocusOffset[3]|1000' 
  symbol s4
  @ concat s5  "/PLOT" ERR 'herange[1]' 'herange[2]' 'herange[3]' 
  symbol s5
  @ concat s6  AMP '0.01*nint(100*harange[1])' '0.01*nint(100*harange[2])' '0.01*nint(100*harange[3])'
  symbol s6
  sic verify on
   solve holo 's1' 'maskp' 's2' 's3' 's4' 's5' 's6'

!  solve holo np thesize[hsize] 'AST' 'FR' 'NOFEED' 'close' -
!  FUDGE 'hFudge' 'poffset' CHEAT 'hcheat[1]' 'hcheat[2]' -
!    'ffoc' 'fxyfoc' 'maskp' 'apodi' - 
!    'foffset' /PLOT 'syplot'
  sic verify off  
  delete /symbol ast fr nofeed ffoc fxyfoc maskp poffset apodi close s1 s2 s3 s4 s5 s6
end procedure x_ATFholo_solve
!------------------------------------------------------------------------------
!------------------------------------------------------------------------------
! x_ATFholo_panels.clic 
! called by x_ATFholo.clic
! SOLVE and PLOT Holographic map, after calibration:
! SOLVE for Panels screw settings.
!------------------------------------------------------------------------------
begin   procedure x_ATFholo_panels
define integer thesize[4] themodes[4]
  let themodes 1 3 4 5
  let thesize 64 128 256 512
  set scan hscan[1] hscan[2]
  g\set plot landscape
  find /proc  holo /scan first_scan last_scan
  set rf off
  set phase relative baseline noatm
  set amplitude baseline relative
  set record 1 to 1000000
!
  symb sss 'hcant'
  set antenna 1
  set band 'hBand'
  set sub ALL
!!
  symbol ring "FREE RING "'hrings'
  symbol sect "FREE SECT "'hsects'
!  symbol onemore -
!  "NP "'thesize[hsize]'" MODE "'themodes[hpmode]'" ITER "'hniter'" "'hgain' 
  @ concat s1 NP 'thesize[hsize]' MODE 'themodes[hpmode]' ITER 'hniter' 'hgain' 
  symbol s1
  @ concat s15  'ring' 'sect' 
  symbol s15
  if hbases.eq."ALL" then
    symbol base " "
  else
    symbol base "BASE "'hbases'
  endif
  if hastig then
    symbol ast "AS"
  else
    symbol ast " "
  endif
  if hFresnel then
    symbol fr "FRESNEL"
  else
    symbol fr " "
  endif
  if hFeed then
    symbol nofeed " "
  else
    symbol nofeed "NOFEED"
  endif
  if hApodize then
    symbol apodi "APODIZE"
  else
    symbol apodi " "
  endif
  @ concat s2 'base' 'AST' 'FR' 'NOFEED' 'apodi'
  if hFixFocus then
    symbol ffoc "NOFOCUS"
  else
    symbol ffoc ""
  endif
  if hFixXYFocus then
    symbol fxyfoc "NOXYFOCUS"
  else
    symbol fxyfoc ""
  endif
  if hNMask.gt.0 then
    symbol maskp "MASK "'hNMask'" "'hMask'
  else
    symbol maskp " "
  endif
  if telescope.eq."VTX-ALMATI" then
!    valid for Vertex antenna
! reference plane is now mid-plane of dish, not az axis. 
! 314.7-2.18-1.875/2.
! was     symbol close "DEFOCUS 0.091 DISTANCE 314.7" 
     symbol close "DEFOCUS 0.091 DISTANCE 311.6" 
  else if telescope.eq."AEC-ALMATI" then
!    valid for AEC antenna in 2004
!     symbol close "DEFOCUS 0.083 DISTANCE 302" 
!    valid for AEC antenna in 2005
     symbol close "DEFOCUS 0.093 DISTANCE 298.9" 
  endif
  @ concat s2  's2' 'ffoc' 'fxyfoc' 'close' 
  symbol s2
  symbol poffset "POINTING "'hPointOffset[1]'" "'hPointOffset[2]'
  @ concat s3  FUDGE 'hFudge' 'poffset' CHEAT '0.01*nint(100*hcheat[1])' '0.01*nint(100*hcheat[2])'
  symbol s3
  @ concat s4 "/OFFSET" 'hFocusOffset[1]|1000' 'hFocusOffset[2]|1000' 'hFocusOffset[3]|1000' 
  symbol s4
  @ concat s5  "/PLOT" ERR 'herange[1]' 'herange[2]' 'herange[3]' NUMBER 
  symbol s5
  @ concat s6  AMP '0.01*nint(100*harange[1])' '0.01*nint(100*harange[2])' '0.01*nint(100*harange[3])'
  symbol s6
  sic verify on
  solve holo 's1' 'maskp' 's2' 's3' 's4' 's5' 's6'
  sic verify off  
  delete /symbol ring sect base fxyfoc ffoc ast fr nofeed poffset s1 s15 s2 s3 s4 s5 s6 -
  apodi maskp close
end   procedure x_ATFholo_panels
!------------------------------------------------------------------------------
begin procedure x_ATFholo_pointing
  set data 1000000 1
  set x lambda beta /lim -hprange +hprange -hprange +hprange
  set y t01 /lim 0 =
  set aver none
  set antenna 1
  set plot poin
  fin /procedure holo
  plot
  fin /procedure corr
  plot /nofra 1
end procedure x_ATFholo_pointing
!------------------------------------------------------------------------------
begin procedure x_ATFholo_scanning
  set data 1000000 1
  set aver none
  set x lambda /lim -hprange +hprange 
  set y beta /lim -hprange +hprange 
  fin /proc holo
  g\set mark 4 0 .01
  set aspect  1 exact
  plot
  fin /proc corr
  plo /nofra 1
  g\set mar 4 0 .1
end procedure x_ATFholo_scanning
!------------------------------------------------------------------------------
begin procedure x_ATFholo_beam
  def real bmin bmax
  def character nn*80 hatype*6
  let nn 'date_observed' /lower
  if telescope.eq."VTX-ALMATI" then
     let hatype Vertex
  else if telescope.eq."AEC-ALMATI" then
     let hatype AEC
  endif
  if scan.lt.10 then
    let nn 'nn'"-"'hatype'"-000"'scan' 
  else if scan.lt.100 then
    let nn 'nn'"-"'hatype'"-00"'scan' 
  else if scan.lt.1000 then
    let nn 'nn'"-"'hatype'"-0"'scan' 
  else
    let nn 'nn'"-"'hatype'"-"'scan' 
  endif
  exa  nn
  vector\transpose 'nn'.beam 'nn'.b2 231
  vector\header 'nn'.b2 
  define image b 'nn'.b2 read
  define image  ba dummy.map real /like b
  let ba% b%
  let ba[1] sqrt(b[1]^2+b[2]^2)
  greg2\rgdata ba /var
  exa hbrange
  clear whole
  greg\set box sq
  greg\lim -hbrange hbrange -hbrange hbrange s
  lut rainbow
  sic\compute bmin min ba[1]
  sic\compute bmax max ba[1]
  sic\exa bmin
  sic\exa bmax
  let ba ba|bmax
  greg2\plot ba[1] /scal log 0.001 1
  greg\box /unit s 
  greg2\wedge
  vector\header 'nn'.b2
end procedure x_ATFholo_beam
!------------------------------------------------------------------------------
begin procedure x_ATFholo_all
  !
  ! Fast plotting of holography.
  !
  @ x_holo_select
  @ x_ATFholo_calibrate
  @ x_ATFholo_solve
end   procedure x_ATFholo_all
!------------------------------------------------------------------------------
begin help x_ATFholo.hlp
1 Description
-------------------------------------------------------------------------------
Calibration package for reducing ATF holographies          R. Lucas 2003-05-08
-------------------------------------------------------------------------------

This is the X-window procedure to reduce holographies of antennas at
ALMA test facility.  The DEVICE is switched to IMAGE WHITE



Active Buttons:
---------------
GO	Perform in sequence: SELECT, RF, CALIBRATE, SOLVE.

CREATE  Create a file of ATF holography data. The file testnnn.hpb is
        created from the fits file TESTnnnn.FITS, assumed to reside in
        /users/almamgr/HOLODATA/ ; nnn is the scan number.


SELECT	Select observations; Must be done after FILE is changed (or created by
	pressing CREATE). Note all scans with
	quality worse than MIN_QUAL are ignored. Use STORE QUALITY to change
	the quality on scans.

CALIBRATE   Amplitude and phase calibration (baseline based).

SOLVE 	Solve the holography and plot.

PANELS	Solve for panel screw settings. Additional input parameters
	are available. 

POINTING Plot the signal total power as a function of angular
        displacement, to check pointing.

BEAM    Plot a beam map. 


Inputs:
-------
2 HSCAN
"Scan number ?" 
        The scan number as defined by TICS.

2 HFILE_NAME
"File name ?"   
	The name of the file (.hpb) containing the data, created by CREATE.
	CREATE also provides the default. Press FILE for browser.

2 HSIZE  
"Map size in pixels"
	Size of the plotted map in pixels: 64, 128, 256 (default) or 512.

2 HFRESNEL
"Fresnel Approximation"
        Use NO to include the first terms neglected in the standard 
        approach (see the memo "ALMA Holography System Basics") 

2 HFEED
" Do Feed Correction?"
        Use NO to take the feed correction off (for tests). The
        default is YES.

2 HNMASK
"Number of Masked Panels"
        Number of panels masked in the best fit paraboloid
        determination. 
        

2 HMASK
"Masked Panels"
        Panels to be masked in the best fit paraboloid determination. See 
        the memo "ALMA Holography System Basics" for the
        numbering scheme.

2 HAPODIZE
"Apodize map?"
        Use apodization of the beam to reduce oscillations near the
        edge due to the finite map size.

1 ENDOFHELP
end help x_ATFholo.hlp
!------------------------------------------------------------------------------
begin help x_ATFholo_pointing.hlp
1 Description

  Pointing and scanning related parameters:

    The action taken when pressing GO (or POINTING in the main window) is to 
    produce a plot of the signal total power as a function of angular
    displacement, in Az and El, to check pointing.

    The pointing offsets and the fudge offset are used when processing
    the map.


2 HPOINTINGOFFSET
"Pointing Offsets (arc sec.)?"
  - These pointing corrections (in arc sec.) are added to the antenna
    positions in the file, inorder to recenter the main beam in the
    map and compensate for bad centering on the transmitter. 

2 HFUDGE
"Fudge offset (arc sec.)?" 
  - The 'Fudge'  parameter is added to the increasing Azimuth scans and
    subtracted to the decreasing scans. This was a workaround for
    timing errors.

2 HPRANGE
"Plot Range (arc. sec.)?"
  - The first parameter is the Az and El range in that plot.

1 ENDOFHELP
end help x_ATFholo_pointing.hlp
!------------------------------------------------------------------------------
begin help x_ATFholo_beam.hlp
1 Description

   - Action on pressing GO: plot the beam map.
   
2 HBRANGE
"Size of map (arc sec.)"
   - parameter: size  of map in arc sec.

1 ENDOFHELP
end help x_ATFholo_beam.hlp
!------------------------------------------------------------------------------
begin help x_ATFholo_focus.hlp
1 Description

   Options and parameters relative to  the "best paraboloid" 
   fit determination:

2 HFOCUSOFFSET
"Focus Offsets (mm)?"
   - Focus offsets in X, Y, Z (mm) subtracted to the phases before
     fitting for the best paraboloid (the fit search box in only 2mm in side),
     so offsets have to be introduced first for larger focus errors).

2 HFIXXYFOCUS
"Fix X and Y Focus?" 

   - Select to perform  the fit only in Z

2 HFIXFOCUS
"Fix all Focus Coordinates?"

  - Select to perform  the fit only in axis direction and phase offset.

1 ENDOFHELP
end help x_ATFholo_focus.hlp
!------------------------------------------------------------------------------
begin help x_ATFholo_panels.hlp
1 Description
-------------------------------------------------------------------------------
	Determination of panel screw settings for ATF antenna(s)
	using holography.
-------------------------------------------------------------------------------

Additional input parameters:
----------------------------
2 HPMODE	
"Modes "
	Specify the authorised panel motions, to be commanded by the screw 
	settings. Five modes  are allowed, since there are five screws 
	on each panel (except 3 for ring 1). Available options are:

	Piston		TRANSLATION of the whole panel, in the normal 
			direction (1 mode).
	+Tilt		Add the panel ROTATION, along axes tangential to the
			of the antenna surface (1+2 modes).
	+Torsion 	The same, plus panel TORSION: opposite panel corners
			move in the same direction, while adjacent panel 
			corners move in opposite directions; the center
			does not move (3+1 modes)
	+Boss		The same, plus the panel DEFORMATION where the panel
			center moves relative to its corners (4+1 modes).

2 HNITER
"Num. Iterations ?" 
	Number of iterations when solving for panel modes. 
	The iterative procedure is used to allow for the finite resolution 
	of the surface measurement, which is not negligible as compared to 
	the panel size.
	At each iteration the effect of panel displacements are subtracted
	from the observed beam map, the residual beam map is used to compute 
	a residual surface map, which is used to compute improved panel 
	displacements. The default is 5 iterations, which is normally 
	enough for a good convergence. At each iteration, both the surface map
	and the map of panel displacements are plotted.

2 HGAIN
"Interative Gain ?"
	The gain of each iteration: this allows to subtract only a fraction of 
	the panel displacements from the observed beam map, to improve 
	convergence. This is not normally needed, so the default is 1.

2 HRINGS
"Rings avoided for paraboloid fit ?" 
	This string should contain the panel rings (numbered 1 to 8
	from center to edge of the antenna surface, separated by spaces), 
	which should be left out when fitting the reference paraboloid 
	to the computed surface map. 

	This allows to use only the "good" part of the surface to fit 
	the paraboloid, using it as a reference for the rest of the surface
	(i.e. the "bad" part, the deformations of which one aims to correct).
2 HSECTS
"Sectors avoided for paraboloid fit ?"
	This string should contain the panel sectors (numbered 1 to 12
	anti-clockwise from 03:00 hours on the plotted map, separated by 
        spaces), which should be left out when fitting the reference 
        paraboloid to the computed surface map. 

	This allows to use only the "good" part of the surface to fit 
	the paraboloid, using it as a reference for the rest of the surface
	(i.e. the "bad" part, the deformations of which one aims to correct).
1 ENDOFHELP


end help x_ATFholo_panels.hlp
!------------------------------------------------------------------------------
!
! @ x_ATFHholo
!
! Automatic calibration procedure for Holographies at ATF
! No arguments
!
! Argument &1 may be "hide" to have no widgets
!
let widget .true. /new logical
if ("&1widget".eq."hidewidget") then
   let widget .false. 
endif
!
clear whole
g\set plot landscape
if (widget) then
   device image white
endif
lut rainbow3
if (.not.exist(hMode)) then
  define integer mant hREC hscan[2] hmode hpmode hsize hniter -
         hfscan hNMask /global
  define real hgain hFudge hFocusOffset[3] hPointOffset[2] hprange hbrange -
  herange[3] harange[3] hcheat[2] -
         hCalStep /global
  define character ATFFits*80 hFile_name*80 hOld_file*80 hMin_qual*10 /global
  define character hcant*1 hrings*12 hsects*12 hbases*15 hBand*3 -
         hMask*80 /global
  define logical hAstig hFinit hFresnel hFeed hFixFocus hFixXYFocus hapodize -
         /global
  !
  ! First call defaults:
  !
  let hFinit      yes
  let hRings       " "
  let hSects       " "
  let hMode       3
  let hRec        1 
  let mant	  5
  let hsize       3
  let ATFFits   "ATFTestFile"
  let hold_file   "ATFTestFile"
  let hscan       0 10000
  let hmin_qual   "average"
  let hNiter 5
  let hGain 1.0
  let hPmode 2
  let hBases "ALL"
  let hCant  "A"
  let hAstig NO
  let hFresnel YES
  let hFeed YES
  let hBand USB ! the right sign for phases
  let hFudge 0
  let hfScan  0
  let hFocusOffset 0 0 0
  let hPointOffset 0 0
  let hFixFocus NO
  let hFixXYFocus NO 
  let hNMask 0
  let hMask " "
  let hapodize NO
  let hprange 1000
  let hbrange 2500
  let herange -125 125 50
!  let hArange -0.05 0.05 0.02
  let hArange -15 0 3
  let hcheat 0 0
  let hcalstep 0.1
ENDIF
!
let hrec 1
let hcant 1
let hmode 3
!!symbol stop "SIC\PAUSE"
symbol breaks " "
!
if (widget) then
   gui\panel "Calibration package for ATF Holographies (R.Lucas)" -
   x_ATFholo.hlp
   let hfScan 'hfScan' /PROMPT "Scan Number?" 
   !let ATFFits      'ATFFits'   	 /PROMPT "Fits File name ?" -
   !    /FILE "/users/almamgr/HOLODATA/*.FITS"
   ! let hFinit      'hFinit'         /PROMPT "Init CLIC file?"
   LET hFILE_NAME	'hold_file'   	 /PROMPT "CLIC File name ?" /FILE "*.hpb"
   LET hCalStep 'hCalStep' /PROMPT "Calibration Curve time interval (h)"
   ! LET hMIN_QUAL   'hmin_qual'           /PROMPT "Min. Data quality ?" -
   !  /CHOICE "UNKNOWN" "EXCELLENT" "GOOD" "FAIR" "AVERAGE" "POOR" "BAD" "AWFUL" -
   ! "WORST" "DELETED"
   let hSize  'hSize' /PROMPT "Map size in pixels"  /index 64 128 256 512
   let hFresnel 'hFresnel' /PROMPT "Fresnel Approximation?" 
   let hFeed 'hFeed' /PROMPT "Do Feed Correction?" 
   !let hBand 'hBand' /PROMPT "Sign of phases?" /CHOICE "USB" "LSB"
   let hNMask 'hNMask' /PROMPT "NUmber of Masked Panels?" 
   let hMask 'hMask' /PROMPT "Masked Panels?" 
   let hApodize  'hApodize' /PROMPT "Apodize Map?" 
   !
   ! this button creates the hpb file:
   !
   GUI\BUTTON "@ x_ATFholo_create"  CREATE 
   ! Selection criteria
   !
   !
   GUI\BUTTON "@ x_ATFholo_select"  SELECT 	! Selection criteria
   GUI\BUTTON "@ x_ATFholo_calibrate" CALIBRATE  	! Calibration
   GUI\BUTTON "@ x_ATFholo_solve"   SOLVE  	! Solve and Plot
   !
   !
   let hERange 'hErange[1]' 'hErange[2]' 'hErange[3]'  -
   /PROMPT "Plot range (Min Max Step) in micrometers?" 
   !let hARange 'hArange[1]' 'hArange[2]' 'hArange[3]'  -
   !            /PROMPT "Plot amplitude range (Min Max Step)?" 
   let hARange '0.01*nint(100*harange[1])' '0.01*nint(100*harange[2])' '0.01*nint(100*harange[3])'  -
   /PROMPT "Plot amplitude range (Min Max Step)?" 
   
   GUI\BUTTON "@ x_ATFholo_solve " "(SOLVE) " "Focus offsets" - 
   "x_ATFholo_focus.hlp" "More input for focus"
   let hFocusOffset 'hFocusOffset[1]' 'hFocusOffset[2]' 'hFocusOffset[3]'  -
   /PROMPT "Focus Offsets (mm)?" 
   let hFixXYFocus 'hFixXYFocus' /PROMPT "Fix X and Y Focus?" 
   let hFixFocus 'hFixFocus' /PROMPT "Fix All Focus Coords?" 
   !
   !
   ! Check Pointing
   GUI\BUTTON "@ x_ATFholo_pointing" POINTING "Tracking, pointing ..." - 
   "x_ATFholo_pointing.hlp" "More input for pointing"
   let hprange 'hprange'   /prompt "Plot Range (arc. sec.)?" 
   let hPointOffset 'hPointOffset[1]' 'hPointOffset[2]' -
   /PROMPT "Pointing Offsets (arc sec.)?" 
   let hFudge 'hFudge' /PROMPT "Fudge offset (arc sec.)?" 
   !
   ! cheat with center pixels
   GUI\BUTTON "@ x_ATFholo_solve " "(SOLVE) " "Cheat with center pixels..." - 
   "x_ATFholo_focus.hlp" "More input"
   let hcheat 'hcheat[1]' 'hcheat[2]'   /prompt "Amplitude, Phase?" 
   !
   GUI\BUTTON "@ x_ATFholo_scanning" SCANNING "Scanning ..." - 
   "x_ATFholo_pointing.hlp" "More input for scanning"
   let hprange 'hprange'   /prompt "Plot Range (arc. sec.)?" 
   !
   !
   GUI\BUTTON "@ x_ATFholo_panels"  PANELS "Panels ..." -
   "x_ATFholo_panels.hlp" "Input for Panels"
   let hpmode 'hpmode' /Prompt "Modes "   /INDEX -
   "Piston" "+Tilts" "+Torsion" "+Boss(ALL)"
   let hniter 'hniter' /prompt "Num. Iterations ?"
   let hgain  'hgain'  /prompt "Interative Gain ?"
   let hrings 'hrings'   /prompt "Rings avoided for paraboloid fit ?" 
   let hsects 'hsects'   /prompt "Sectors avoided for paraboloid fit ?" 
   let hastig 'hastig'   /prompt "Subtract astigmatism ?" 
   !
   !
   GUI\BUTTON "@ x_ATFholo_beam"  BEAM "Beam map ..." -
   "x_ATFholo_beam.hlp" "Input for Beam Map"
   
   let hbrange 'hbrange' /Prompt "Range (arc sec.) " 
   GUI\GO     "@ x_ATFholo_all"     ALL 
endif   
!



