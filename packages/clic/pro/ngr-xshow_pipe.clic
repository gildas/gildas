!------------------------------------------------------------------------
!
! XSHOW_PIPE.CLIC
!
!------------------------------------------------------------------------
!
symbol old 'stop'
symbol stop "sic\continue"
!
if exist(prev_file_name) then
   let prev_file_name 'file_name'  !to avoid that later calib_select launch x_show.clic
endif
!
if exist(err) then
   delete /var err
endif
define logical err /global
define logical create_show_1
define logical create_show_2
define logical create_show_3
define logical create_show_4
define logical create_show_5
let err yes
@ ngr-x_calib_select ! no ??     ! x_calib_select but no x_calib_verify
!
set phase noatm
!
let do_array1 no
let do_array2 no
let do_array3 no
if .not.exist(theconf) def char theconf*20 /global
let theconf ""
!
for cc 1 to tnconf
   let do_array'cc' yes 
   if (tnconf.gt.1) let theconf "-conf"'cc'
   if exist(short_track) delete /var short_track
   if (tnconf.gt.1) then
      if (array['cc',2].eq."yessou") then 
         @ ngr-x_calib_select ! no? 
      else if (array['cc',2].eq."nosou") then ! if no data on source (show is created by the pipe)
         let min_teles 'array[cc,1]'
         set telescope 'array[cc,1]'
         !
         if exist(first_scan) then
            del /var first_scan last_scan
         endif
         define integer first_scan[1] last_scan[1] /global
         let first_scan 0
         let last_scan 10000
         let ngains 1
         set scan * * 
         !
         find /proc corr flux
         if (found.ne.0) then
            get cx_num['found'] !/header
            let irec 'receiver' 0 0 0
            set receiver 'receiver'
         endif
         !
         if (found.le.3) then                  ! not create show if too short_track but
            define logical short_track    ! tnconf>1, so message remain in show 1st page
         endif
         !
      endif
   endif
   !

   if (tnconf.gt.1) let theconf "-conf"'cc'

   sic cpu
   exam sic%cpu%raw%
   exam sic%cpu%cumul%
   if (.not.exist(short_track)) then
      if (.not.err) then
         
         let create_show_'cc' yes
         if (file("show-'short_file''theconf'.ps")) let create_show_'cc' no
         
         if (create_show_'cc') then
            
            @ ngr-xshow_elaz port     ! elevation-azimuth
            
            !for c 1 to tnconf 
            if (do_array'cc'.and.array['cc',2].eq."yessou") then
               
               sic cpu
               exam sic%cpu%raw%
               exam sic%cpu%cumul%
               
               @ xshow_meteo        	 ! meteo
               @ xshow_tracking          ! tracking
               @ xshow_point             ! pointing-focussing
               @ ngr-xshow_incli         ! inclinometer data
               
               sic cpu
               exam sic%cpu%raw%
               exam sic%cpu%cumul%
               
               @ ngr-xshow_ifpb          ! ampli on bandpass vs time
               if (before_polyfix.or.(n_line_bands.eq.r_nbb)) @ ngr-xshow_ifpb vsfreq       ! ampli on bandpass vs freq
               @ ngr-xshow_tweak         ! tweak levels on time
               @ ngr-xshow_ifpower       ! if transmission power levels
               @ ngr-xshow_cali          ! calibrations on time
               
               sic cpu
               exam sic%cpu%raw%
               exam sic%cpu%cumul%
               
               @ ngr-xshow_trec          ! receiver temperatures 
               @ ngr-xshow_trect         ! receiver temperatures on time 
               @ ngr-xshow_total         ! total power
               @ ngr-xshow_tsys          ! tsys
               @ ngr-xshow_water         ! water
               
               sic cpu
               exam sic%cpu%raw%
               exam sic%cpu%cumul%
               
               @ xshow_wvr               ! 22 GHz monitors
               @ ngr-xshow_time          ! monitoring the time, utc, ntp, pps 
               @ ngr-xshow_cable         ! cable phase
               @ ngr-xshow_rfif          ! RF phases at the start and end of track
               
               !! Measure phase delays along the track	 
               !! Check if user has defined inputs to be used in delay computation
               if exist(user_inputs_file) then
                  if (user_inputs_file) then 
                     if file("'delay_inputs_filename'") then                                             
                        sic grep "delay_if" 'delay_inputs_filename'
                        if (GREP%N.ne.0) then 
                           if .not.exist(delay_if) define character*3 delay_if /global
                        endif
                        sic grep "refant" 'delay_inputs_filename'
                        if (GREP%N.ne.0) then 
                           if .not.exist(refant) define integer refant /global
                        endif

                        @ 'delay_inputs_filename'
                        let user_delaycorr yes       ! even if no correction is done
                                                     ! it should be clear that inputs were present
                        say ""
                        say "==> Delay inputs read from user-defined file:"
                        say ""
                        type 'delay_inputs_filename'
                        say ""
                        say ""
                     endif         
         
                     ! 'refant' is not needed as an input, but it is kept if existing
                     if exist(delay_if) then   
                        if .not.exist(which_if) let which_if 0 /new integer
                        if (delay_if[1:2].eq."IF") let which_if 'delay_if[3:3]'
                        if (delay_if[1:3].eq."1+2") let which_if 5
                        if (delay_if[1:3].eq."3+4") let which_if 6

                        if (which_if.ne.0) then 
                           @ ngr-xshow_delay if 'which_if'  ! measure phase delays along the track
                                                            ! with user inputs for IF choice  
                        else
                           @ngr-xshow_delay                   
                        endif
                     else
                        @ ngr-xshow_delay         ! delay inputs present but not IF choice
                     endif
                  else
                     @ ngr-xshow_delay         ! normal delay -- no user inputs for delays
                  endif
               else
                  @ ngr-xshow_delay         ! normal delay -- 'user_inputs_file' not defined
               endif


               !**** Copy delay plot before correction to monitoring directory ****
               if .not.exist(mon_directory) define char mon_directory*200 
               let mon_directory "/SOG/observer/Monitor/delays/"    
               if .not.exist(delay_file_tocopy) define char delay_file_tocopy*200
               if .not.exist(delay_file_suffix) define char delay_file_suffix*50
               let delay_file_suffix "uncorrected"
               
               if exist(atbure) then
                  if ((atbure).and.(do_pipe)) then
                     if file("tmpipe/'short_file'-delay_warn'theconf'.ps") then
                        let delay_file_tocopy "tmpipe/"'short_file'"-delay_warn"'theconf'".ps"
                     else			 
                        let delay_file_tocopy "tmpipe/"'short_file'"-delay"'theconf'".ps"
                     endif

                     sic copy 'delay_file_tocopy' "tmpipe/"'short_file'"-delay_"'delay_file_suffix''theconf'".ps"
                     sic copy  'delay_file_tocopy' 'mon_directory''short_file'"-delay_"'delay_file_suffix''theconf'".ps"
                     sys "gzip -f "'mon_directory''short_file'"-delay_"'delay_file_suffix''theconf'".ps"
                     
                     sic copy "tmpipe/delay-"'short_file''theconf'".txt" "tmpipe/delay-"'short_file'"_"'delay_file_suffix''theconf'".txt"
                     sic copy "tmpipe/delay-"'short_file''theconf'".txt" 'mon_directory'"delay-"'short_file'"_"'delay_file_suffix''theconf'".txt"
                     sys "gzip -f "'mon_directory'"delay-"'short_file'"_"'delay_file_suffix''theconf'".txt"
                     sys "chmod ugo+rw "'mon_directory'"* "
                  endif
               endif
               
               sic cpu
               exam sic%cpu%raw%
               exam sic%cpu%cumul%
               
            endif
            !next c            
            @ ngr-xshow_print         ! print report   --  output :  "show-'short_file'.ps"
            if ((tnconf.gt.1).and.(file("show-'short_file'.ps"))) sic copy show-'short_file'.ps show-'short_file''theconf'.ps        
         else
            say " " 
            say "The show file already exist for "'theconf'
            say " " 
         endif         
      endif
   endif
   !
   if exist(short_track) delete /variable short_track
   let do_array'cc' no
next cc
   
sic cpu
exam sic%cpu%raw%
exam sic%cpu%cumul%
!
set def
symbol stop 'old'
