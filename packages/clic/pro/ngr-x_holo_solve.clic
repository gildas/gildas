!------------------------------------------------------------------------------
! ngr-x_holo_solve.clic 
! called by ngr-x_holo.clic
! RL 13-may-1997
! SOLVE and PLOT Holographic map, after calibration
!------------------------------------------------------------------------------
!
! Local variables
!
if .not.exist(nant) then
   var conf
endif
define character psf*50
define character plotname*47
!
define integer thesize[2] ant icol
if .not.exist(physant) then
  define integer physant /global
endif
if .not.exist(ngrpol) then
  define character ngrpol*1 polysb*3 /global
endif
let polysb 'cal_band'
let thesize 128 256 
set scan hscan[1] hscan[2]
if hcant.eq."A"
  SAY "Please select an antenna"
  return
endif
set antenna 'hcant'
let ant 'hcant'
if (ant.gt.nant) then
   say ""
   say " Logical antenna "'ant'" is not in the array "
   say ""
   return
endif
if (scan_holo[1,1,'ant'].eq.0).and.(scan_holo[2,1,'ant'].eq.0) then
   say ""
   say " Logical Antenna "'ant'" did not move "
   say " No holography can be solved  "
   say ""
   return
endif

let physant phys_ant['hcant']

find /proc holo /scan scan_holo[1,1,'ant'] scan_holo[2,1,'ant'] 
   !say 'ant' 'scan_holo[1,1,ant]' 'scan_holo[2,1,ant]'
if found.eq.0 then
  say " no holo scans found "
  return
endif
if (found.ge.32) let hsize 2

if .not.exist(whichpol) then
  sym which both
  def char whichpol*1 /global
  let whichpol "b"
endif
say " "
if (whichpol.eq."b") sym which /inquire "Which polar [h,v,both = default]? "
if (whichpol.eq."h") sym which /inquire "Which polar [h = default,v,both]? "
if (whichpol.eq."v") sym which /inquire "Which polar [h,v = default,both]? "
let ngrpol 'which' 
let ngrpol 'ngrpol' /lower
if .not.(ngrpol.eq."h".or.ngrpol.eq."v".or.ngrpol.eq."b") then
  say "Please select a polar"
  return
endif
let whichpol 'ngrpol'
set polar 'ngrpol'
set band 'cal_band'

  if ngrpol.eq."h" then
    set spw c01 and c02
  else if ngrpol.eq."v" then
    set spw c03 and c04
  endif

if (dobs.gt.-2538) then ! PolyFiX

  if .not.exist(sideband) then
    sym sideb 'cal_band'
    def char sideband*3 /global
    let sideband 'cal_band'
  endif
  say " "
  if (sideband.eq."lsb") sym sideb /inquire "Which side band [lsb = default, usb]? "
  if (sideband.eq."usb") sym sideb /inquire "Which side band [lsb, usb = default]? "
  let polysb 'sideb' 
  let polysb 'polysb' /lower
  if .not.(polysb.eq."lsb".or.polysb.eq."usb") then
    say "Please select a sideband"
    return
  endif
  let sideband 'polysb'
  set polar ''

  set band 'polysb'
!
  say " "
  say "ONLY one sideband considered at this stage: You have chosen "'polysb'"!"
  say " "
  if (polysb.eq."lsb") then
    set spw c001 and c002 and c005 and c006
    if ngrpol.eq."h" then
      set spw c001 and c002 
    else if ngrpol.eq."v" then
      set spw c005 and c006
    endif
  else if (polysb.eq."usb") then
    set spw c003 and c004 and c007 and c008
    if ngrpol.eq."h" then
      set spw c003 and c004 
    else if ngrpol.eq."v" then
      set spw c007 and c008
    endif
  endif
endif
show sub
stop

if hmode.eq.1 then
  set rf on baseline
else
  set rf off
endif
set phase relative baseline noatm
!!if (hamprel) then
  set amplitude baseline relative
!!else
!!  set amplitude baseline absolute
!!endif
!
if hbases.eq."ALL" then

 symbol base " "
 if exist(pha_err_h) then
  def inte hbascount hnb
  def real hant
  def char hsel*100 bas*2
  let hsel ""
  let bas "no"
  let hbascount 0
  let hant 'hcant'
  let hnb 0
  for i 2 to nant
   for j 1 to i-1
    let hbascount hbascount+1
    if i.eq.hant.or.j.eq.hant then
     if (pha_err_h[hbascount,1]+pha_err_v[hbascount,1])|2.le.hpharms then
      let bas 'j' /format i2.2 
      let hsel 'hsel'" "'bas'
      let bas 'i' /format i2.2 
      let hsel 'hsel'"-"'bas'
      let hnb hnb+1
     endif
    endif
   next
  next
  if (bas.eq."no") then
   say " "
   say " No baseline available for an RMS of " 'hpharms' " deg <<<<" /format a f5.1 a
   return
  endif
  if hnb.lt.nant-1 symbol base "BASE "'hsel'
 endif
else
  symbol base "BASE "'hbases'
endif
if hastig then
  symbol ast "AS"
else
  symbol ast " "
endif
let icol hcolor-1
solve holo np thesize[hsize] 'AST' 'BASE' /plot ERR -500 500 100 NUMBER COLOR 'icol'
show sub
delete /symbol base
!

let psf  'short_file'"-"'ngrpol'"-"'polysb'"-solve-an"'physant'".ps" 
if (dobs.le.-2538) let psf 'short_file'"-"'ngrpol'"-solve-an"'physant'".ps" 
sic delete 'psf'
hardcopy 'psf' /dev ps !eps color /fitpage
!
!------------------------------------------------------------------------------
