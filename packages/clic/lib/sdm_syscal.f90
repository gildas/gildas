subroutine write_sdm_syscal(error)
  !---------------------------------------------------------------------
  ! Write the syscal SDM table.
  !
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_Syscal
  ! Dummy
  logical error
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'sdm_identifiers.inc'
  character*20 version
  ! Local
  type(SysCalRow) :: scRow
  type(SysCalOpt) :: scOpt
  type(SysCalKey) :: scKey
  integer sig, ima, nfreq, ia, isb, ier, ic, ilc, ip, ispp, numReceptors
  integer ireturn
  character sdmTable*16
  parameter (sdmTable = 'calAtmosphere')
  !---------------------------------------------------------------------
  include 'clic_version.inc'
  call sdmMessage(1,1,sdmTable ,'starting...')
  !
  !
  scKey%timeInterval = ArrayTimeInterval(time_Interval(1)   &
    , time_Interval(2))
  scKey%FeedId = feed_Id
  if (r_isb.eq.1) then
    sig = 1
    ima = 2
  else
    sig = 2
    ima = 1
  endif
  !     line
  !     loop on correlator units
  do ic = 1, r_lband
    scRow%numReceptor = r_npol_rec
    scRow%numChan = r_lnch(ic)
    call allocSysCalOpt(scRow, scOpt, error)
    if (error) return
    !     loop on side bands
    do isb = 1, r_lnsb
      do ia = 1, r_nant
        sckey%spectralWindowId = spectralWindow_Id(isb, ic, 2)
        scKey%AntennaId = antenna_Id(ia)
        call addSysCalRow(sckey, scrow, error)
        !
        do ispp = 1,  r_lnch(ic)
          do ip=1, r_npol_rec
            if (isb.eq.sig) then
              scopt%tSysSpectrum(ispp,ip) = r_tsyss(ip,ia)
            elseif (isb.eq.ima) then
              scopt%tSysSpectrum(ispp,ip) = r_tsysi(ip,ia)
            endif
          enddo
        enddo
        !
        call addSysCalTsysSpectrum(sckey, scopt, error)
        if (error) goto 99
        !
      enddo
    enddo
  enddo
  
  !     continuum
  scRow%numReceptor = r_npol_rec
  scRow%numChan = 1
  call allocSysCalOpt(scRow, scOpt, error)
  if (error) return
  
  !     loop on side bands
  do isb = 1, r_lnsb
    !     loop on correlator units
    do ic = 1, r_lband
      do ia=1, r_nant
        sckey%spectralWindowId = spectralWindow_Id(isb, ic, 1)
        scKey%AntennaId = antenna_Id(ia)
        call addSysCalRow(sckey, scrow, error)
        !
        do ip=1, r_npol_rec
          scopt%tRxSpectrum(1,ip) = r_trec(ip,ia)
          if (isb.eq.sig) then
            scopt%tSysSpectrum(1,ip) = r_tsyss(ip,ia)
          elseif (isb.eq.ima) then
            scopt%tSysSpectrum(1,ip) = r_tsysi(ip,ia)
          endif
        enddo
          !
        call addSysCalTsysSpectrum(sckey, scopt, error)
        if (error) goto 99
          !
      enddo
    enddo
  enddo
  call sdmMessage(1,1,sdmTable ,'Written.')
99 return
end subroutine write_sdm_syscal
!---------------------------------------------------------------------
