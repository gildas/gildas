subroutine mesname (file)
  use gkernel_interfaces
  character(len=*) :: file          !
  ! Local
  character(len=48) :: name
  character(len=32) :: mess
  integer :: i
  !
  call sic_date(mess)
  call sic_lower(mess)
  i = index(mess,':')
  do while (i.gt.0)
    mess(i:)=mess(i+1:)
    i = index(mess,':')
  enddo
  i = index(mess,'-')
  do while (i.gt.0)
    mess(i:)=mess(i+1:)
    i = index(mess,'-')
  enddo
  name = 'clic-'//mess(1:9)//'-'//mess(11:16)
  call sic_parsef (name,file,'GAG_LOG:','.dat')
end subroutine mesname
!
subroutine messios (prio,seve,name,ier)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Output system message corresponding to IO error code IER, and
  ! using the MESSAGE routine, using the portable GAG_IOSTAT routine
  !---------------------------------------------------------------------
  integer :: prio                   !
  integer :: seve                   !
  character(len=*) :: name          !
  integer :: ier                    !
  ! Local
  character(len=60) :: msg
  !
  if (ier.eq.0) return
  call gag_iostat(msg,ier)
  call message(prio,seve,name,msg)
end subroutine messios
!
function get_file_type ( fspec, ftype, ftype_len, new)
  use gkernel_interfaces
  integer :: get_file_type          !
  character(len=*) :: fspec         ! file spec to parse
  character(len=*) :: ftype         ! type field
  integer*4 :: ftype_len            ! length of type string
  logical :: new                    !
  ! Local
  integer :: i,j,l
  !
  i = index(fspec,'.')
  if (i.ne.0) then
    l = lenc(fspec)
    do j=l,i,-1
      if (fspec(j:j).eq.'.') then
        ftype = fspec(j:)
        ftype_len = lenc(ftype)
        call sic_upper(ftype)
        get_file_type = 1
        return
      endif
    enddo
  else
    get_file_type = 0
  endif
end function get_file_type
!
function get_file_name ( fspec, fname, fname_len)
  use gkernel_interfaces
  integer :: get_file_name          !
  character(len=*) :: fspec         ! file spec to parse
  character(len=*) :: fname         ! type field
  integer*4 :: fname_len            ! length of type string
  ! Local
  integer :: i
  character(len=1) :: cdir,insep,ousep
  !
  call gag_separ(insep,ousep,cdir)
  fname = fspec
  fname_len = lenc(fname)
  i = index(fname,cdir)
  do while (i.gt.0)
    fname(1:) = fname(i+1:fname_len)
    fname_len = fname_len - i
    i = index(fname,cdir)
  enddo
  i = index(fname,'.')
  if (i.le.0) return
  fname_len = i-1
  if (i.gt.1) then
    fname = fname(1:i-1)
  elseif (i.eq.1) then
    fname = ' '
  endif
  call sic_upper(fname)
  get_file_name = 1
end function get_file_name
!
function no_number(f)
  !---------------------------------------------------------------------
  !  Return codes of fpclassifyf:
  !
  !  FP_PLUS_NORM      0	/* Positive normalized */
  !  FP_MINUS_NORM     1	/* Negative normalized */
  !  FP_PLUS_ZERO      2	/* Positive zero */
  !  FP_MINUS_ZERO     3	/* Negative zero */
  !  FP_PLUS_INF	     4	/* Positive infinity */
  !  FP_MINUS_INF      5	/* Negative infinity */
  !  FP_PLUS_DENORM    6	/* Positive denormalized */
  !  FP_MINUS_DENORM   7	/* Negative denormalized */
  !  FP_SNAN	     8	/* Signalling NaN */
  !  FP_QNAN	     9	/* Quiet NaN */
  ! shanged to use gag_isreal
  !---------------------------------------------------------------------
  logical :: no_number              !
  real :: f                         !
  ! Global
  integer :: gag_isreal
  !-----------------------------------------------------------------------
  no_number = gag_isreal(f).ne.0   ! .FALSE.
  return
end function no_number
!
subroutine clic_directory(line,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! INPUT
  !     line       char    command line
  !     error      LOGICAL error parameter
  ! OUTPUT
  !     define  DIR_NFILE and DIR_FILE variables
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Local
  integer :: nc, dir_nfile, ld, ls, id, lch, ier
  character(len=256) :: filestring, sirstring, ch
  character(len=256), allocatable :: dir_file(:)
  save dir_nfile, dir_file
  !-----------------------------------------------------------------------
  filestring = '*.hpb'
  call sic_ch(line,0,1,filestring,nc,.false.,error)
  if (error) return
  id = index(filestring,':')
  if (id.gt.2) then
    ch = filestring(1:id)
    ier = sic_getlog(ch)
    lch = lenc(ch)
    filestring = ch(1:lch)//filestring(id+1:)
  endif
  call gag_directory(' ',filestring,dir_file,dir_nfile,error)
  if (error) return
  call sic_delvariable('DIR_NFILE',.false.,error)
  call sic_delvariable('DIR_FILE',.false.,error)
  if (dir_nfile.gt.0) then
    error = .false.
    call sic_def_inte('DIR_NFILE',dir_nfile,0,1,.true.,error)
    call sic_def_charn   &
      ('DIR_FILE',dir_file,1,dir_nfile,.true.,error)
  else
    call message(8,3,'CLIC_DIRECTORY','No file found')
  endif
  return
end subroutine clic_directory
