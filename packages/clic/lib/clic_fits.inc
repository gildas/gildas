!----------------------------------------------------------------------
! FITS01	Common for fits file
      INTEGER MAXIS                      ! Maximum number of axes
      PARAMETER (MAXIS=7)
      INTEGER MCOLS                      ! Maximum number of columns in a 3D table
      PARAMETER (MCOLS=200)
!
!
      REAL*8 AXVAL(MAXIS)
      REAL*8 AXREF(MAXIS)
      REAL*8 AXINC(MAXIS)
      REAL*8 ROTA(MAXIS)
      REAL*8 SCALING(MCOLS)
      REAL*4 BSCAL                       !Tape scaling factor
      REAL*4 BZERO                       !Map offset
      REAL*4 DATAMIN                     !Minimum map value
      REAL*4 DATAMAX                     !Maximum map value
      INTEGER NBIT                       !Num. of bits / value in current scan
      INTEGER SNBIT                      !Num. of bits / value for FITS writing
      INTEGER IBAD                       !Integer blanking value
      INTEGER NAXIS                      !Number of axes present in data header
      INTEGER AXIS(MAXIS)                !Axes dimensions
      INTEGER MAIN_AXIS                  !Non dummy axis
      INTEGER NDATA                      !Num. of data pts (in one row if ap.
      INTEGER MDAT                       !Max. number of data points in any row
      INTEGER NROWS                      !Number of rows in 3D extension
      INTEGER LROW                       !Length of row in 3D extens.(in bytes)
      INTEGER NCOLS                      !Number of columns in table
      INTEGER COLADDR(MCOLS+1)           !Addr. of col. i (in bytes within row)
      INTEGER COLLENG(MCOLS)             !Length (in bytes) of column i
      INTEGER COLFMT(MCOLS)              !Format of col. i (standard SIC codes)
      INTEGER NITEM(MCOLS)               !Repeat count for column i
      INTEGER PHYADDR(MCOLS)             !Addr. of data correspond. to col. i
      INTEGER I_NAXIS
      INTEGER I_AXIS(MAXIS)
      INTEGER I_CRVAL(MAXIS)
      INTEGER I_CDELT(MAXIS)
      INTEGER I_CRPIX(MAXIS)
      INTEGER I_CROTA(MAXIS)
      INTEGER I_AXTYP(MAXIS)
      INTEGER I_SCAN
      INTEGER I_LINE
      INTEGER I_OBJECT
      INTEGER I_TELESC
      INTEGER I_MATRIX
!
      CHARACTER*20 AXTYPE(MAXIS)
      CHARACTER*20 COLTYPE(MCOLS)        ! Type of information in column i
      CHARACTER*20 COLFORM(MCOLS)        ! Format data in col.i (FITS syntax)
      CHARACTER KIND*12                  ! Typ of FITS formt (basic, or 3D table)
!
      COMMON /CFITS01/                                                  &
! Double precision
     &AXVAL,AXREF,AXINC,ROTA,SCALING,                                   &
! Reals
     &BSCAL,BZERO,DATAMIN,DATAMAX,                                      &
! Integers
     &NBIT,SNBIT,IBAD,NAXIS,AXIS,MAIN_AXIS,NROWS                        &
     &,MDAT,NDATA,LROW,NCOLS,COLADDR,COLLENG,COLFMT,NITEM               &
     &,PHYADDR                                                          &
!
     &,I_NAXIS,I_AXIS,I_CRVAL,I_CDELT,I_CRPIX,I_CROTA,I_AXTYP           &
     &,I_MATRIX,I_SCAN,I_LINE,I_OBJECT,I_TELESC
! Bytes
! Logicals
!     &,BLANKED
! Character strings
      COMMON /CFITSC1/ AXTYPE,KIND,COLTYPE,COLFORM
