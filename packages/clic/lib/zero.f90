subroutine zero_index
  use clic_index
  !--------------------------------------------------------------
  ! Zeroes the current index
  !--------------------------------------------------------------
  !
  cxnext = 1
  knext = 0
  nindex = 0
end subroutine zero_index
