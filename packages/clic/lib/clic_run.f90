subroutine run_clic(line,comm,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! CLIC CLIC
  !	Main routine
  ! 	Call appropriate subroutine according to COMM
  ! Arguments :
  !	LINE	C*(*)	Command line		Input/Output
  !	COMM	C*(*)	Command name		Input
  !	ERROR	L	Logical error flag	Output
  ! (2-Jan-1985)
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  character(len=*) :: comm          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  ! Local
  integer :: nch
  integer :: msolve,nsolve,mvoc1
  parameter (msolve=22, mvoc1=11)
  character(len=12) :: vsolve(msolve),argum,keywor,voc1(mvoc1)
  character(len=60) :: mess
  !
  data vsolve /'BASELINES','AMPLITUDE','PHASE','FLUX',   &
    'FLAG','FOCUS','POINTING','GAIN','RF_PASSBAND','SKYDIP',   &
    'FIVE','DELAYS','HOLOGRAPHY','PSEUDO','TOTAL',   &
    'CORRECTION',   &
    'MONITOR','QUALITY','WVR','SPIDX','STOKES','DTERM'/
  data voc1/'FILE','INDEX','DATA','CONTINUUM','LINE',  &
    'VIRTUAL','HEADER','SIZE','BUFFER','RF','SBA'/
  !
  call message(0,1,'CLIC',line(1:lenc(line)))
  !
  if (comm.eq.'FILE') then
    call file(line,error)
  elseif (comm.eq.'BLANK') then
    call clic_blank(line,error)
  elseif (comm.eq.'CURSOR') then
    call clic_cursor(line,error)
  elseif (comm.eq.'COMPRESS') then
    call clic_compress(line,error)
  elseif (comm.eq.'COPY') then
    call clic_copy(line,error)
  elseif (comm.eq.'CLONE') then
    call clic_clone(line,error)
  elseif (comm.eq.'DIRECTORY') then
    call clic_directory(line,error)
  elseif (comm.eq.'DROP') then
    call clic_drop(line,error)
  elseif (comm.eq.'DUMP') then
    argum = ' '
    call clic_kw(line,0,1,argum,nch,voc1,mvoc1,.false.,error,.true.)
    if (error) return
    if (argum .eq. 'FILE') then
      call filedump(error)
    elseif (argum .eq. 'INDEX') then
      call idump(error)
    elseif (argum .eq. 'DATA') then
      call dhdump(error)
    elseif (argum .eq. 'CONTINUUM') then
      call dcdump(line,error)
    elseif (argum .eq. 'LINE') then
      call dldump(line,error)
    elseif (argum .eq. 'VIRTUAL') then
      call vdump(error)
    elseif (argum .eq. 'SIZE') then
      call sdump(error)
    elseif (argum .eq. 'BUFFER') then
      call bdump(error)
    elseif (argum .eq. 'RF') then
      call rfdump(error)
    elseif (argum .eq. 'SBA') then
      call sbadump(error)
    else
      call rdump(line,error)
    endif
  elseif (comm.eq.'FIND') then
    call find(line,error)
  elseif (comm.eq.'FITS') then
    call clic_fits(line,error)
  elseif (comm.eq.'FLAG') then
    call clic_flag(line,error)
  elseif (comm.eq.'FREE') then
    call free_memory()
  elseif (comm.eq.'GET') then
    call clic_get(line,error)
  elseif (comm.eq.'GRID_TOTAL') then
    call clic_grid_tp(line,error)
  elseif (comm.eq.'HEADER') then
    call clic_header(line,error)
  elseif (comm.eq.'IGNORE') then
    call ignore(line,error)
  elseif (comm.eq.'LIST') then
    call liste(line,error)
  elseif (comm.eq.'MARK') then
    call clic_mark(line,error)
  elseif (comm.eq.'MASK') then
    call clic_mask(line,error)
  elseif (comm.eq.'MINMAX') then
    call clic_minmax(error)
  elseif (comm.eq.'MODIFY') then
    call clic_modify(line,error)
  elseif (comm.eq.'MONITOR') then
    call clic_monitor(line,error)
  elseif (comm.eq.'NEW_DATA') then
    call newdata(line,error)
  elseif (comm.eq.'PLOT') then
    call clic_plot(line,error)
  elseif (comm.eq.'PRINT') then
    call clic_print(line,error)
  elseif (comm.eq.'POPUP') then
    call clic_popup(line,error)
  elseif (comm.eq.'RESIDUALS') then
    call sic_ke(line,0,1,argum,nch,.true.,error)
    if (error) return
    if (argum(1:2) .eq. 'BA') then
      call resid_baseline(line,error)
    else
      write(6,'(1x,a)') 'W-RESI,  ',argum,   &
        ' not available'
    endif
  elseif (comm.eq.'SET') then
    call clic_set(line,error)
  elseif (comm.eq.'SHOW') then
    call clic_show(line,error)
  elseif (comm.eq.'SOLVE') then
    if (sic_present(12,0)) then
      call solve_tp(line,error)
      return
    endif
    keywor = ' '
    call clic_kw(line,0,1,keywor,nsolve,vsolve,msolve,.true.,error,.true.)
    if (error) return
    if (keywor .eq. 'BASELINES') then
      call solve_baseline (line,error)
    elseif (keywor .eq. 'DELAYS') then
      call solve_delay (line,error)
    elseif (keywor.eq.'AMPLITUDE') then
      if (do_amplitude_antenna) then
        call solve_cal_ant (line,error)
      else
        call solve_cal (line,error)
      endif
    elseif (keywor.eq.'PHASE') then
      if (do_phase_antenna) then
        call solve_cal_ant (line,error)
      else
        call solve_cal (line,error)
      endif
    elseif (keywor.eq.'FLUX') then
      call solve_flux (line,error)
    elseif (keywor.eq.'FIVE') then
      call solve_five (line,error)
    elseif (keywor .eq. 'FOCUS') then
      call solve_focus (line,error)
    elseif (keywor .eq. 'POINTING') then
      call solve_point (line,error)
    elseif (keywor .eq. 'HOLOGRAPHY') then
      call solve_holo (line,error)
    elseif (keywor .eq. 'GAIN') then
      call solve_gain (line,error)
    elseif (keywor .eq. 'CORRECTION') then
      call solve_corr (line,error)
    elseif (keywor .eq. 'RF_PASSBAND') then
      if (.not.do_pass_antenna) then
        call solve_pass (line,error)
      else
        call solve_pass_ant (line,error)
      endif
    elseif (keywor .eq. 'SKYDIP') then
      call solve_skydip (line,error)
    elseif (keywor .eq. 'PSEUDO') then
      call solve_pseudo (line,error)
    elseif (keywor .eq. 'TOTAL') then
      call solve_pseudo (line,error)
    elseif (keywor .eq. 'MONITOR') then
      call solve_monitor (line,error)
    elseif (keywor .eq. 'WVR') then
      call solve_wvr (line,error)
    elseif (keywor .eq. 'SPIDX') then
      call solve_spidx (line,error)
    endif
  elseif (comm.eq.'STORE') then
    keywor = ' '
    call clic_kw(line,0,1,keywor,nsolve,vsolve,msolve,.true.,error,.true.)
    if (error) return
    if (keywor.eq.'RF_PASSBAND') then
      call store_pass(line,error)
    elseif ((keywor.eq.'AMPLITUDE') .or.   &
      (keywor.eq.'PHASE')) then
      call store_cal (line,error)
    elseif (keywor.eq.'FLAG') then
      call store_flag (line,error)
    elseif (keywor.eq.'FLUX') then
      call store_flux (line,error)
    elseif (keywor.eq.'SPIDX') then
      call store_spidx (line,error)
    elseif (keywor.eq.'MONITOR') then
      call store_monitor (line,error)
    elseif (keywor.eq.'WVR') then
      call store_wvr (line,error)
    elseif (keywor.eq.'QUALITY') then
      call store_quality (line,error)
    elseif (keywor.eq.'CORRECTION') then
      call store_correction (line,error)
    elseif (keywor.eq.'STOKES') then
      call store_stokes (line,error)
    elseif (keywor.eq.'DTERM') then
      call store_dterm (line,error)
    else
      write(6,'(1x,a)') 'W-CLIC, ',argum,   &
        ' may not be stored'
    endif
  elseif (comm.eq.'ATMOSPHERE') then
    call clic_atmos(line,error)
  elseif (comm.eq.'TAG') then
    call tagout(line,error)
  elseif (comm.eq.'TABLE') then
    if (table_prototype) then
      call sg_table(line,error)
    else
      call clic_table(line,error)
    endif
  elseif (comm.eq.'SG_TABLE') then
    call sg_table(line,error)
  elseif (comm.eq.'VARIABLES') then
    call clic_variable(line,error)
  elseif (comm.eq.'CHECK') then
    call clic_check(line,error)
  elseif (comm.eq.'WVR') then
    call clic_wvr(line,error)
  elseif (comm.eq.'ZPASSBAND') then
    call sg_zpassband(line,error)
  else
    mess = comm//' Not yet implement'
    call message(6,1,'CLIC',mess)
  endif
  !
  return
end subroutine run_clic
