subroutine clic_check (line,error)
  character(len=*) :: line          !
  logical :: error                  !
  ! Local
  real :: step
  !
  step = 5.0
  call sic_r4(line,0,1,step,.false.,error)
  if (error) goto 99
  call clic_check_contin (step,error)
  return
99 error = .true.
end subroutine clic_check
!
subroutine clic_check_contin (step,error)
  use classic_api
  use clic_rdata
  real :: step                      !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  !------------------------------------------------------------------------
  ! Code:
  call sub_continuum (r_nsb, r_nband, r_nbas, r_lntch,   &
    datac, datal, passc, passl, step,error)
  if (error) goto 999
  error = .false.
  return
999 error = .true.
  return
end subroutine clic_check_contin
!
subroutine sub_continuum (qsb, qband, qbas, qntch,   &
    data_c, data_l, pass_c, pass_l, step,error)
  use gildas_def
  use classic_api
  use clic_rdata
  !---------------------------------------------------------------------
  ! Check continuum correlator stability on current scan
  !
  ! Call Tree
  !	CLIC_CHECK
  !---------------------------------------------------------------------
  integer :: qsb                     !
  integer :: qband                   !
  integer :: qbas                    !
  integer :: qntch                   !
  complex :: data_c(qsb,qband,qbas)  !
  complex :: data_l(qntch,qsb,qbas)  !
  complex :: pass_c(qsb,qband,qbas)  !
  complex :: pass_l(qntch,qsb,qbas)  !
  real :: step                       !
  logical :: error                   !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_par.inc'
  include 'clic_display.inc'
  ! Local
  logical :: down_channel
  integer :: ib, is, isb 
  real :: afac,rmss
  complex :: zz(10),mean
  logical :: bad(mnbas,10)
  character(len=60) :: chain
  !------------------------------------------------------------------------
  ! Code:
  call set_scaling(error)
  do ib=1, mnbas
    do is = 1, r_nband
      bad(ib, is) = down_channel (ib, is)
    enddo
    do isb = 1,2
      mean = 0
      rmss = 0
      do is = 1, r_nband
        if (.not.bad(ib,is)) then
          zz(is) = data_c(isb,is,ib)
          if (do_pass) then
            zz(is) = zz(is)*pass_c(isb,is,ib)
          endif
          call scaling (is,isb,ib,zz(is),afac,error)
          if (.not.error) then
            mean = mean+zz(is)
            rmss = rmss+real(zz(is))**2+aimag(zz(is))**2
          endif
        endif
      enddo
      !
      mean = mean/r_nband
      rmss = rmss/r_nband-real(mean)**2-aimag(mean)**2
      rmss = sqrt(rmss)*step
      !
      do is = 1,r_nband
        if (.not.bad(ib,is) .and.   &
          abs(zz(is)-mean).gt.rmss) then
          write (chain,100) is,cband(isb),cbas(ib)
          call message(6,2,'CONTINUUM',chain)
        endif
      enddo
    enddo
  enddo
100 format('Bad subband C',i2.2,1x,a,' Baseline ',a)
  return
999 error = .true.
  return
end subroutine sub_continuum
