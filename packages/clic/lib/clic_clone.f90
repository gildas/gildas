subroutine clic_clone(line,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api
  !---------------------------------------------------------------------
  ! Command COPY = MODIFY NOTHING
  !     well not so simple
  ! COPY DATA|HEADERS [ANT | NOANT] [BASE | NOBASE]
  ! 1:
  !     DATA     data and headers are copied
  !     HEADERS  headers only
  ! 2 and/or 3:
  !     ANT      Antenna calibration section created
  !     BASE     Baseline calibration section created
  !     NOANT    Antenna calibration sections  NOT created
  !     NOBASE   Baseline calibration sections NOT created
  ! otherwise sections are copied if and only if present.
  ! default is "ANT"
  ! Bure copy Job does "NOANT NOBASE"
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_par.inc'
  include 'clic_number.inc'
  include 'clic_dheader.inc'
  include 'clic_proc_par.inc'
  include 'gbl_memory.inc'
  include 'gbl_pi.inc'
  ! Local
  logical :: end, check
  integer(kind=address_length) :: data_in, ip_data, kh
  integer(kind=address_length) :: addrnewdata, ipnewdata
  integer(kind=data_length) :: ndata, mnewdata, nnewdata
  integer :: narg, i, t_nant
  integer :: ia, ja, isb, l, ig, im, ir, ipol
  integer :: t_nbas, t_ndump, t_ldpar, t_ldump, t_ldatl, t_ldatc, j
  real*8 :: theta, r, rmin
  character(len=80) :: chain
  character(len=7) :: done
  character(len=12) :: argum, argum2
  logical :: do_write_saved, acopy, askip
  integer :: mvoc1, mvoc2
  parameter (mvoc1=4, mvoc2=5)
  character(len=12) :: voc1(mvoc1), voc2(mvoc2)
  save addrnewdata, mnewdata
  data voc1/'HEADERS','NOHEADERS','DATA','NODATA'/
  data voc2/'ANTENNA','NOANTENNA','BASELINE','NOBASELINE','NOCHECK'/
  data mnewdata/0/
  !------------------------------------------------------------------------
  ! Code:
  do_write_saved = do_write_data
  ! input needed:  number of antennas and their positions
  !     ARGUM = 'HEADERS'
  !     CALL CLIC_KW(LINE,0,1,ARGUM,NARG,VOC1,MVOC1,.TRUE.,ERROR)
  t_nant = mnant
  call sic_i4(line,0,1,t_nant,.false.,error)
  if (error) return
  do_write_data = .true.
  acopy = .true.
  askip = .false.
  check = .true.
  !
  call check_input_file(error)
  if (error) goto 99
  call check_index(error)
  if (error) goto 99
  call check_output_file(error)
  if (error) goto 99
  call check_different_file(error)
  if (error) goto 99
  call show_general('MODEL',.false.,line,error)
  if (error) goto 99
  !
  ! Loop on current index
100 call get_first(.true.,error)
  if (error) goto 99
  end = .false.
  do while (.not.end)
    ! Get storage and data
    call get_data (ndata,data_in,error)
    if (error) goto 99
    ip_data = gag_pointer(data_in,memory)
    !         print *, 'GET_DATA , ndata , ip_data ', ndata ,ip_data
    ! this is the only reason to acess tha data itself.
    ! Only needed for data older than 11 feb 1997.
    ! Sould be tested.
    ! For time consistency
    r_ut = dh_utc*2*pi/86400.
    r_dobs = dh_obs
    !
    ! Reserve space for ANTENNA based sections only by default.
    r_bpcdeg = 20
    r_abpcdeg = 20
    r_icdeg = 3
    r_aicdeg = 3
    r_presec(bpcal_sec) = .false.
    r_presec(ical_sec) =  .false.
    r_presec(abpcal_sec) =  .true.
    r_presec(aical_sec) =  .true.
    !
    ! Fill in header variables r_xxx:
    t_nbas = max(t_nant*(t_nant-1)/2,1)
    do i=1, t_nbas
      r_iant(i) = antbas(1,i)
      r_jant(i) = antbas(2,i)
    enddo
    t_ldpar = 21 + 27*t_nant + 11*t_nbas
    if (r_lmode.eq.2) then
      t_ldatc = r_nband*t_nant
      t_ldatl = r_lntch*t_nant
    elseif (r_lmode.eq.1) then
      t_ldatc = 2*r_nband*r_nsb*t_nbas
      t_ldatl = 2*r_lntch*r_lnsb*t_nbas
    endif
    !         print *, 'r_ndump, r_ldpar, r_ldump, r_ldatc, r_ldatl'
    !         print *, r_ndump, r_ldpar, r_ldump, r_ldatc, r_ldatl
    t_ldump = t_ldpar+t_ldatc
    !         print *, 'r_ndump, t_ldpar, t_ldump, t_ldatc, t_ldatl'
    !         print *, r_ndump, t_ldpar, t_ldump, t_ldatc, t_ldatl
    !
    ! new coordinates for all antennas
    ! minimum distance is 24 meters.
    !
    do i = 1, t_nant
      rmin = 0.
      do while (rmin.lt.24)
        r_istat(i) = 100+i
        r = min(s_noise(50.),150.)
        theta = s_random(2.*pis)
        r_ant(1,i) = -r*cos(theta)*sin(44.63389d0)
        r_ant(2,i) =  r*sin(theta)
        r_ant(3,i) =  r*cos(theta)*cos(44.63389d0)
        if (i.gt.1) then
          rmin = 9999.
          do j = 1, i-1
            r = sqrt((r_ant(1,i)-r_ant(1,j))**2   &
              + (r_ant(2,i)-r_ant(2,j))**2   &
              + (r_ant(3,i)-r_ant(3,j))**2)
            rmin = min(rmin,r)
          enddo
        else
          rmin = 9999.
        endif
      enddo
    enddo
    do i=1, t_nbas
      do l=1, 3
        r_bas(l,i) = r_ant(l,r_jant(i)) - r_ant(l,r_iant(i))
      enddo
    enddo
    ! new antennas:
    do i = r_nant+1, t_nant
      j = mod(i-1,r_nant)+1
      r_kant(i) = i            !r_kant(j)
      r_kent(i) = i            !r_kent(j)
      r_axes(i) = r_axes(j)
      r_phlo1(i) = r_phlo1(j)
      r_phlo3(i) = r_phlo3(j)
      do l=1, r_lband
        r_lilevu(l,i) = r_lilevu(l,j)
        r_lilevl(l,i) = r_lilevl(l,j)
        r_ldeloff(i,l) = r_ldeloff(j,l)
      enddo
      r_mobil(i) = r_mobil(j)
      r_collaz(i) = r_collaz(j)
      r_collel(i) = r_collel(j)
      r_corfoc(i) = r_corfoc(j)
      do ipol =1, r_npol_rec
        r_h2omm(ipol,i) = r_h2omm(ipol,j)  !! R_CORFOC(J)
        r_tatms(ipol,i) = r_tatms( ipol,j)
        r_tatmi(ipol,i) = r_tatmi(ipol,j)
        r_taus(ipol,i) = r_taus(ipol,j)
        r_taui(ipol,i) = r_taui(ipol,j)
        r_beef(ipol,i) = r_beef(ipol,j)
        r_feff(ipol,i) = r_feff(ipol,j)
        r_tchop(ipol,i) = r_tchop(ipol,j)
        r_tcold(ipol,i) = r_tcold(ipol,j)
        r_trec(ipol,i) = r_trec(ipol,j)
        r_gim(ipol,i) = r_gim(ipol,j)
        r_csky(ipol,i) = r_csky(ipol,j)
        r_cchop(ipol,i) = r_cchop(ipol,j)
        r_ccold(ipol,i) = r_ccold(ipol,j)
        r_tsyss(ipol,i) = r_tsyss(ipol,j)
        r_tsysi(ipol,i) = r_tsysi(ipol,j)
        r_ceff(ipol,i) = r_ceff(ipol,j)
        r_cmode(ipol,i) = r_cmode(ipol,j)
        r_jykel(ipol,i) = r_jykel(ipol,j)
      enddo
      r_tcabin(i) = r_tcabin(j)
      do l=1, 3
        r_tdewar(l,i) = r_tdewar(l,j)
      enddo
      r_h2o_mon(i) =  r_h2o_mon(j)
      r_tatms_mon(i) =  r_tatms_mon(j)
      r_tatmi_mon(i) =  r_tatmi_mon(j)
      r_taus_mon(i) =  r_taus_mon(j)
      r_taui_mon(i) =  r_taui_mon(j)
      r_feff_mon(i) =  r_feff_mon(j)
      r_tchop_mon(i) =  r_tchop_mon(j)
      r_tcold_mon(i) =  r_tcold_mon(j)
      r_trec_mon(i) =  r_trec_mon(j)
      r_gim_mon(i) =  r_gim_mon(j)
      r_csky_mon(i) =  r_csky_mon(j)
      r_cchop_mon(i) =  r_cchop_mon(j)
      r_ccold_mon(i) =  r_ccold_mon(j)
      r_tsys_mon(i) =  r_tsys_mon(j)
      r_cmode_mon(i) =  r_cmode_mon(j)
      r_path_mon(i) =  r_path_mon(j)
      r_tem_mon(i) =  r_tem_mon(j)
      r_dpath_mon(i) =  r_dpath_mon(j)
      r_ok_mon(i) =  r_ok_mon(j)
      !-- ignore ???
      !            do l=0, MBBPCDEG
      !               R_AICPHA(1,i,l) = R_AICPHA(1,j,l)
      !               R_AICAMP(1,i,l) = R_AICAMP(1,j,l)
      !               R_AICPHA(2,i,l) = R_AICPHA(2,j,l)
      !               R_AICAMP(2,i,l) = R_AICAMP(2,j,l)
      !            enddo
      !            do m = 1, 2
      !               r_abpcsba(m,i) =  r_abpcsba(m,j) !
      !               do l=0, mbpccdeg
      !                  do n = 1, r_lband
      !                     r_abpclamp(m,i,n,l) = r_abpclamp(m,j,n,l)
      !                     r_abpclpha(m,i,n,l) = r_abpclpha(m,j,n,l)
      !                  enddo
      !                  r_abpfamp(m,i,l) = r_abpfamp(m,j,l)
      !                  r_abpfpha(m,i,l) = r_abpfpha(m,j,l)
      !               enddo
      !            enddo
      !--
      !            R_DMAFLAG(i) = R_DMAFLAG(j)
      !            R_DMATFAC(1,i) = R_DMATFAC(1,j)
      !            R_DMATFAC(2,i) = R_DMATFAC(2,j)
      !            R_DMDELAY(i) = R_DMDELAY(j)
      !            R_WVRNCH(i) = 0
    enddo
    !
    ! Get necessary memory for data section (pointer IPNEWDATA)
    nnewdata = r_ndump * t_ldump +   &
      max(1,r_ndatl) * (t_ldump+t_ldatl)
    if (mnewdata.lt.nnewdata) then
      if (mnewdata.gt.0) call free_vm(mnewdata,addrnewdata)
      mnewdata = nnewdata
      !            print *, 'sic_getvm, mnewdata ', mnewdata
      if (sic_getvm(mnewdata,addrnewdata).ne.1) goto 99
    endif
    ipnewdata = gag_pointer(addrnewdata,memory)
    !
    ! In subroutine loop on records to fill in data section
    !         print *, 'call fill_data,  t_nbas ', t_nbas
    call fill_data(ndata, memory(ip_data),   &
      t_nant, t_nbas, t_ldpar, t_ldatc, t_ldump,   &
      nnewdata, memory(ipnewdata))
    !
    r_nant = t_nant
    r_nbas = t_nbas
    r_ldpar = t_ldpar
    r_ldatc = t_ldatc
    r_ldatl = t_ldatl
    r_ldump = t_ldump
    ndata = nnewdata
    call ipb_write(write_mode,check,error)
    if (.not.error) then
      call wdata (nnewdata,memory(ipnewdata),.true.,error)
      if (error) goto 99
      call ipb_close(error)
      if (error) goto 99
      write(chain,'(a,i6,a,i6,a)') 'Observation ',r_num,   &
        ' '//done(write_mode)//' (',r_ndump,' records)'
      call message(4,1,'CLIC_MODEL',chain(1:50))
    endif
200 if (sic_ctrlc()) then
      error = .true.
      goto 99
    endif
    call get_next(end,error)
    if (error) goto 99
  enddo
99 do_write_data = do_write_saved
  return
end subroutine clic_clone
!*<FF>
subroutine fill_data(ndata, data,   &
    t_nant, t_nbas, t_ldpar, t_ldatc, t_ldump,   &
    nnewdata, newdata)
  use gildas_def
  use gkernel_interfaces
  use classic_api
  !---------------------------------------------------------------------
  ! create a data section with nant antennas (nant > r_nant)
  !---------------------------------------------------------------------
  integer(kind=data_length) :: ndata                  !
  real :: data(ndata)               !
  integer :: t_nant                 !
  integer :: t_nbas                 !
  integer :: t_ldpar                !
  integer :: t_ldatc                !
  integer :: t_ldump                !
  integer(kind=data_length) :: nnewdata               !
  real :: newdata(nnewdata)         !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_number.inc'
  include 'clic_dheader.inc'
  !      INCLUDE 'clic_clic.inc'
  !      INCLUDE 'clic_proc_par.inc'
  !      INCLUDE 'gbl_memory.inc'
  !      INCLUDE 'gbl_pi.inc'
  ! Local
  real :: ampli, noise, tsys(2)
  integer :: s_nant, s_nbas, s_ldpar, s_ldatc, s_ldump, i, j
  integer :: kin, kout, is, l, ig, im, ir, ia, ja, ib, t_ldatl, ic, isb
  real*8 :: sind, cosd, sinh, cosh
  !
  ! Loop on records to process the data headers and continuum data
  s_nant = r_nant
  s_nbas = r_nbas
  s_ldpar = r_ldpar
  s_ldatc = r_ldatc
  s_ldump = r_ldump
  kin = 1
  kout = 1
  if (r_isb.eq.1) then
    ig = 1                     ! upper sideband
    im = 2
  else
    ig = 2                     ! lower sideband
    im = 1
  endif
  do ir=1, r_ndump+r_ndatl
    r_nant = s_nant
    r_nbas = s_nbas
    call decode_header (data(kin))
    !         print *,'dh_dump, dh_integ, dh_utc ', dh_dump, dh_integ, dh_utc
    do i = r_nant+1, t_nant
      j = mod(i-1,r_nant)+1
      dh_aflag(i) = dh_aflag(j)
      do l=1, mnbb
        dh_total(l,i) = dh_total(l,j)
        dh_atfac(l,1,i) = dh_atfac(l,1,j)
        dh_atfac(l,2,i) = dh_atfac(l,2,j)
      enddo
      dh_delcon(i) = dh_delcon(j)
      do l=1,mnbb ! number of bband
         dh_dellin(l,i) = dh_dellin(l,j)
         dh_delay(l,i) = dh_delay(l,j)
      enddo
      dh_delayc(i) = dh_delayc(j)
      dh_phasec(i) = dh_phasec(j)
      dh_phase(i) = dh_phase(j)
      dh_ratec(i) = dh_ratec(j)
      dh_cable(i) = dh_cable(j)
      dh_gamme(i) = dh_gamme(j)
      do l=1, 5
        dh_test1(l,i) = dh_test1(l,j)
      enddo
      dh_offfoc(i) = dh_offfoc(j)
      dh_offlam(i) = dh_offlam(j)
      dh_offbet(i) = dh_offbet(j)
      do l=1, mwvrch
        dh_wvr(l,i) = dh_wvr(l,j)
      enddo
      dh_wvrstat(i) = dh_wvrstat(j)
    enddo
    ! baselines ???
    do ib = 1, t_nbas
      ia = r_iant(ib)
      ja = r_jant(ib)
      ! some baselines will get e.g. basant(i,i) -> ???
      ! should rather recompute dh_uvb and ignore the rest...
      !            jb = basant(mod(ia-1,nant_in)+1, mod(ja-1,nant_in)+1)
      !            do l=1, 2
      !               dh_rmsamp(l,ib) = dh_rmsamp(l,jb)
      !               dh_rmspha(l,ib) = dh_rmsamp(l,jb)
      !               dh_uvm(l,ib) = dh_uvm(l,jb)
      !            enddo
      !            dh_BFLAG(ib) = dh_BFLAG(jb)
      sind = dh_svec(3)
      cosd = sqrt(1.d0-sind**2)
      sinh = -dh_svec(2) / cosd
      cosh = dh_svec(1) / cosd
      dh_uvm(1,ib) = (r_bas(1,ib)*sinh + r_bas(2,ib)*cosh)
      dh_uvm(2,ib) = (-r_bas(1,ib)*cosh + r_bas(2,ib)*sinh)*sind
    enddo
    r_nant = t_nant
    r_nbas = t_nbas
    !         print *,'dh_dump, dh_integ, dh_utc ', dh_dump, dh_integ, dh_utc
    call encode_header (newdata(kout))
    kin = kin + r_ldpar
    kout = kout + t_ldpar
    r_nant = s_nant
    r_nbas = s_nbas
    !
    ! Continuum data: Auto Correlation
    if (r_lmode.eq.2) then
      do i = 1, t_nant
        j = mod(i-1,r_nant)+1
        do is = 1, r_nband
          !                  print *, kin + is-1 +(j-1)*r_nband,
          !     &                 kout + is-1 + (i-1)*r_nband
          newdata(kout + is-1 + (i-1)*r_nband)   &
            = data (kin + is-1 +(j-1)*r_nband)
        enddo
      enddo
    !
    ! Continuum data: Cross Correlation
    elseif (r_lmode.eq.1) then
      do ib = 1, t_nbas
        ia = r_iant(i)
        ja = r_jant(i)
        ampli = r_flux/sqrt(r_jykel(1,ia)*r_jykel(1,ja))
        tsys(im) = r_tsysi(1,ia)*r_tsysi(1,ja)
        tsys(ig) = r_tsyss(1,ia)*r_tsyss(1,ja)
        do isb = 1, 2
          do is = 1, r_nband
            noise = sqrt(tsys(isb)   &
              /(2*r_cfwid(is)*dh_integ*1e6))
            ! rangau ...
            newdata(kout) = ampli+s_noise(noise)
            newdata(kout+1) = s_noise(noise)
            kout = kout + 2
          enddo
        enddo
      enddo
    endif
    kin = kin + r_ldatc
    !
    if (ir.gt.r_ndump) then
      ! Spectral data: Auto Correlation
      if (r_lmode.eq.2) then
        do i = 1, t_nant
          j = mod(i-1,r_nant)+1
          do is = 1, r_lntch
            newdata(kout + is-1 + (i-1)*r_lntch)   &
              = data (kin + is-1 +(j-1)*r_lntch)
          enddo
        enddo
        kout = kout + r_lntch*t_nant
      !
      ! Spectral data: Cross Correlation
      elseif (r_lmode.eq.1) then
        do i = 1, t_nbas
          ia = r_iant(i)
          ja = r_jant(i)
          ampli = r_flux/sqrt(r_jykel(1,ia)*r_jykel(1,ja))
          tsys(im) = r_tsysi(1,ia)*r_tsysi(1,ja)
          tsys(ig) = r_tsyss(1,ia)*r_tsyss(1,ja)
          do isb = 1, 2
            do is = 1, r_lband
              do ic = r_lich(is)+1, r_lich(is)+r_lnch(is)
                noise = sqrt(tsys(isb)   &
                  /(2*r_lfres(is)*dh_integ*1e6))
                !     rangau ...
                newdata(kout) = ampli+s_noise(noise)
                newdata(kout+1) = s_noise(noise)
                kout = kout + 2
              enddo
            enddo
          enddo
        enddo
      endif
      kin = kin + r_ldatl
    endif
  enddo
  !      print *, 'kin, ndata, kout, nnewdata'
  !      print *, kin, ndata, kout, nnewdata
  return
end subroutine fill_data
