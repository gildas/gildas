subroutine clic_mark(line,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! CLIC	Command CLIC\MARK f1 f2 ... [/ANTENNA] [/BASELINE] [/RESET]
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_flags.inc'
  ! Local
  integer :: i, j, ak(mnant), bk(mnbas)
  logical :: sa(2*mbands,mnant), sb(2*mbands,mnbas)
  logical :: reset
  character(len=1024) :: out
  !------------------------------------------------------------------------
  ! Code:
  error = .false.
  if (sic_narg(0).eq.0) then
    call list_flags(mnant,mnbas,ant_kill,bas_kill,san_kill,sba_kill,out)
    call message(6,1,'CLIC_MARK','Marked '//out(1:lenc(out)))
    return
  endif
  call get_flags(line,1,1,2,3,-1,-1,ak,bk,sa,sb,reset,error)
  if (error) return
  do i=1, mnant
    if (ak(i).ne.0) then
      if (reset) then
        ant_kill(i) = iand(ant_kill(i),not(ak(i)))
      else
        ant_kill(i) = ior(ant_kill(i),ak(i))
      endif
    endif
    do j=1, 2*mbands
      if (sa(j,i)) then
        if (reset) then
          san_kill(j,i) = .false.
        else
          san_kill(j,i) = .true.
        endif
      endif
    enddo
  enddo
  do i=1, mnbas
    if (bk(i).ne.0) then
      if (reset) then
        bas_kill(i) = iand(bas_kill(i),not(bk(i)))
      else
        bas_kill(i) = ior(bas_kill(i),bk(i))
      endif
    endif
    do j=1, 2*mbands
      if (sb(j,i)) then
        if (reset) then
          sba_kill(j,i) = .true.
        else
          sba_kill(j,i) = .false.
        endif
      endif
    enddo
  enddo
  call list_flags(mnant,mnbas,ant_kill,bas_kill,san_kill,sba_kill,out)
  call message(6,1,'CLIC_MARK','Marked '//out(1:lenc(out)))
  return
end subroutine clic_mark
!
subroutine list_flags(nant,nbas,afl,bfl,saf,sbf,out)
  use gkernel_interfaces
  use gildas_def
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_flags.inc'
  integer :: nant                   !
  integer :: nbas                   !
  integer :: afl(nant)              !
  integer :: bfl(nbas)              !
  logical :: saf(2*mbands,nant)              !
  logical :: sbf(2*mbands,nbas)              !
  character(len=*) :: out           !
  ! Local
  integer :: k, n, i, j, l, nout, lout
  logical :: done
  !------------------------------------------------------------------------
  ! Code:
  lout = len(out)
  nout = 0
  out = ' '
  do i=1, nant
    if (afl(i).ne.0) then
      if (nout.gt.0) then
        if (nout+2.gt.lout) goto 99
        out(nout+1:) = ', '
        nout = nout+2
      endif
      if (nout+6.gt.lout) goto 99
      write (out(nout+1:),'(a,i0,a)') 'Ant ',i,':'
      nout = nout + 6
      do k=1, maf
        n = ishft(1,k-1)
        if (iand(afl(i),n).eq.n) then
           l = lenc(af(k))
           if (nout+l+1.gt.lout) goto 99
           out(nout+1:) = ' '//af(k)(1:l)
           nout = nout + l+1
        endif
      enddo
    endif
    done = .false.
    do j=1, 2*mbands
      if (saf(j,i)) then
        if (.not.done) then
          if (nout.gt.0) then
            if (nout+2.gt.lout) goto 99
            out(nout+1:) = ', '
            nout = nout+2
          endif
          if (nout+6.gt.lout) goto 99
          write (out(nout+1:),'(a,i0,a)') 'Ant ',i,':'
          nout = nout + 6
          done = .true.
        endif
        l = lenc(sf(j))
        if (nout+l+1.gt.lout) goto 99
        out(nout+1:) = ' '//sf(j)(1:l)
        nout = nout + l+1
      endif
    enddo
  enddo
  do i=1, nbas
    if (bfl(i).ne.0) then
      if (nout.gt.0) then
        if (nout+2.gt.lout) goto 99
        out(nout+1:) = ', '
        nout = nout+2
      endif
      if (nout+10.gt.lout) goto 99
      out(nout+1:) = 'Bas '//cbas(i)//':'
      nout = nout + 10
      do k=1, mbf
        n = ishft(1,k-1)
        if (iand(bfl(i),n).eq.n) then
          l = lenc(bf(k))
          if (nout+l+1.gt.lout) goto 99
          out(nout+1:) = ' '//bf(k)(1:l)
          nout = nout + l+1
        endif
      enddo
      if (nout+1.gt.lout) goto 99
      out(nout+1:) = ' '
      nout = nout+1
    endif
    done = .false.
    do j=1, 2*mbands
      if (sbf(j,i)) then
        if (.not.done) then
          if (nout.gt.0) then
            if (nout+2.gt.lout) goto 99
            out(nout+1:) = ', '
            nout = nout+2
          endif
          if (nout+10.gt.lout) goto 99
          out(nout+1:) = 'Bas '//cbas(i)//':'
          nout = nout + 10
          done = .true.
        endif
        l = lenc(sf(j))
        if (nout+l+1.gt.lout) goto 99
        out(nout+1:) = ' '//sf(j)(1:l)
        nout = nout + l+1
        if (nout+1.gt.lout) goto 99
        out(nout+1:) = ' '
        nout = nout+1
      endif
    enddo
  enddo
  if (nout.eq.0) out = 'no flags'
  return
99 out(lout-3:lout) = '...'
  return
end subroutine list_flags
