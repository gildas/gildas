subroutine cut_header (ndrop,newfreq,positions,error)
  use gildas_def
  use clic_xypar
  use classic_api
  !---------------------------------------------------------------------
  ! Prepare header for shortened records
  ! Call Tree
  !	CLIC_TABLE
  !---------------------------------------------------------------------
  integer, intent(in) :: ndrop(2)               !
  real(8), intent(in) :: newfreq                 !
  logical, intent(out) :: error                  !
  logical, intent(in) :: positions
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  ! Local
  integer :: isb, isub, is, js, i1, i2, nch, nd1, nd2,ismin,ismax, i
  real*8 :: fimin, fimax, fi1, fin
  real*8 :: fmin, fmax, f1, fn
  real*4 :: fres
  !------------------------------------------------------------------------
  ! Code:
  if (.not.r_lfour.and. .not.allow_pol) then
    call message(6,3,'CUT_HEADER',   &
      'Operation not possible on raw data')
    error = .true.
    return
  endif
  ! Copy header first
  call r_to_y(positions)
  yima%gil%bval = blank4
  i1 = isubb_out(1)
  i2 = isubb_out(lsubb_out)
  if (cont_select) then
    yima%gil%nchan = 1
    yima%gil%dim(1) = 7+3
    if (positions) then
       yima%gil%dim(1) = 7+3+2
    endif
    yima%gil%ref(1) = 1.0
    yima%char%line = r_line
    yima%gil%voff = r_veloc
    isb = isideb_out
    i1 = isubb_out(1)
    fimin = 1e30
    fimax = 0
    do js = 1, lsubb_out
      is = isubb_out(js)
      if (is.le.mbands) then
        fi1 = r_cfcen(is)-r_cfwid(is)/2.
        fin = r_cfcen(is)+r_cfwid(is)/2.
        if (fi1.lt.fimin) then
          ismin = is
          fimin = fi1
        endif
        if (fin.lt.fimin) then
          ismin = is
          fimin = fin
        endif
        if (fi1.gt.fimax) then
          ismax = is
          fimax = fi1
        endif
        if (fin.gt.fimin) then
          ismax = is
          fimax = fin
        endif
      else
        is = is-mbands
        nch = r_lnch(is)
        nd1 = max(ndrop(1),nint(fdrop(1)*nch))
        nd2 = max(ndrop(2),nint(fdrop(2)*nch))
        fi1 = r_lfcen(is)+r_lfres(is)*(1+nd1-r_lcench(is))
        fin = r_lfcen(is)+r_lfres(is)*(nch-nd2-r_lcench(is))
        if (fi1.lt.fimin) then
          ismin = is
          fimin = fi1
        endif
        if (fin.lt.fimin) then
          ismin = is
          fimin = fin
        endif
        if (fi1.gt.fimax) then
          ismax = is
          fimax = fi1
        endif
        if (fin.gt.fimin) then
          ismax = is
          fimax = fin
        endif
      endif
    enddo
    yima%gil%fres = fimax-fimin
    if (freq_out.ne.0) then
      yima%gil%val(1) = freq_out
    elseif (isb.eq.1) then
      yima%gil%val(1) = (r_flo1 + r_flo2(ismin)   &
        + r_band2(ismin) * (r_flo2bis(ismin)   &
        + r_band2bis(ismin)* fimin)   &
        + r_flo1 + r_flo2(ismax)   &
        + r_band2(ismax) * (r_flo2bis(ismax)   &
        + r_band2bis(ismax)*fimax))/2
    elseif (isb.eq.2) then
      yima%gil%val(1) = (r_flo1 - (r_flo2(ismin)   &
        + r_band2(ismin) * (r_flo2bis(ismin)   &
        + r_band2bis(ismin)* fimin))   &
        + r_flo1 - (r_flo2(ismax)   &
        + r_band2(ismax) * (r_flo2bis(ismax)   &
        + r_band2bis(ismax)*fimax)))/2
    else
      yima%gil%val(1) = r_flo1
    endif
    yima%gil%vres = yima%gil%fres/yima%gil%val(1)*299792.458d0
    yima%gil%freq = yima%gil%val(1)/(1.d0+r_doppl)
  ! Line out
  else
    if (isideb_out.gt.2) then
      if (newfreq.ne.0) then
        ! find the good sideband if declared LINE DSB
        isb = nint(3-(newfreq-r_flo1)/r_fif1)/2
        isb = min(2,max(1,isb))
      else
        if (i1.gt.mbands) then
          isub = i1 - mbands
        else
          isub = i1
        endif
        isb = (3-r_sb(isub))/2
      endif
    else
      isb = isideb_out
    endif
    if (i1.le.mbands) then         ! not meaningful with new corr.
      yima%char%line = r_cnam(isb)
      yima%gil%fres = r_crfres(isb)
      yima%gil%freq = r_crfoff(isb)
      yima%gil%nchan = i2-i1+1
      yima%gil%dim(1) = 7+3*yima%gil%nchan 
      if (positions) then
         yima%gil%dim(1) = yima%gil%dim(1)+2
      endif
      yima%gil%ref(1) = r_crch(isb) - (i1-1)
      yima%gil%vres = r_cvres(isb)
      yima%gil%voff = r_cvoff(isb)
      if (r_flo2(i1).ne.r_flo2(i2).or.   &
        r_flo2bis(i1).ne.r_flo2bis(i2)) then
        print *,"E-CUT_HEADER Quarters don't match"
        return
      endif
      if (isb.eq.1) then
        yima%gil%val(1) = r_flo1 + r_fif1
      else
        yima%gil%val(1) = r_flo1 - r_fif1
      endif
    else
      isub = i1-mbands
      yima%gil%fres = 0.
      fmin = 1e30
      fmax = -1e30
      do js = 1, lsubb_out
        is = isubb_out(js)-mbands
        if (is.le.r_lband) then
          fres = r_lrfres(isb,is)/(1+r_doppl)
          if (abs(fres).gt.abs(yima%gil%fres)) then
            yima%gil%fres = abs(fres)
            isub = is
          endif
          nch = r_lnch(is)
          nd1 = max(ndrop(1),nint(fdrop(1)*nch))
          nd2 = max(ndrop(2),nint(fdrop(2)*nch))
          f1 = r_lrfoff(isb,is)+fres   &
            *(1.+nd1-r_lrch(isb,is))
          fn = r_lrfoff(isb,is)  + fres   &
            *(r_lnch(is)-nd2-r_lrch(isb,is))
          fmin = min(fmin,f1,fn)
          fmax = max(fmax,f1,fn)
        endif
      enddo
      yima%gil%nchan = int((fmax-fmin)/yima%gil%fres)+1
      yima%gil%dim(1) = 7 + 3*yima%gil%nchan 
      if (positions) then
         yima%gil%dim(1) = yima%gil%dim(1)+2
      endif
      yima%gil%freq = r_lrfoff(isb,isub)
      yima%gil%voff = r_lvoff(isb,isub)
      yima%gil%vres = -abs(r_lvres(isb,isub))
      yima%gil%ref(1) = (yima%gil%freq-fmin)/yima%gil%fres +1.
      yima%char%line = r_lnam(isb,isub)
      if (isb.eq.1) then
        yima%gil%val(1) = r_flo1 + r_flo2(isub) &
          + r_band2(isub) * (r_flo2bis(isub)   &
          + r_band2bis(isub) * (r_lfcen(isub) +   &
          (r_lrch(isb,isub)-r_lcench(isub))*r_lfres(isub)))
      else
        yima%gil%val(1) = r_flo1 - (r_flo2(isub) &
          + r_band2(isub) * (r_flo2bis(isub)   &
          + r_band2bis(isub) * (r_lfcen(isub) +   &
          (r_lrch(isb,isub)-r_lcench(isub))*r_lfres(isub))))
      endif
      yima%gil%inc(1) = yima%gil%fres*(1+r_doppl)
    endif
  endif
  !
  ! This description is in this version of the header
  yima%gil%version_uv = code_version_uvt_freq
  return
end subroutine cut_header
!
subroutine r_to_y(positions)
  use gildas_def
  use clic_xypar
  use classic_api
  !---------------------------------------------------------------------
  ! Updates table header parameters (Y) from CLIC Header parameters (R)
  ! For positions (mosaic or OTF), use the reference coordinate.
  !---------------------------------------------------------------------
  logical :: positions
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  ! Local
  integer :: obs1=0
  integer :: obs2=0
  logical :: error
  !-----------------------------------------------------------------------
  yima%char%name = r_sourc
  yima%gil%epoc = r_epoch
  !
  ! Normal data
  if (positions) then
    yima%gil%dec = r_bet 
    yima%gil%ra = r_lam 
  else
    yima%gil%dec = r_bet + r_betof
    yima%gil%ra = r_lam + r_lamof/cos(yima%gil%dec)
  endif
  yima%gil%a0 = yima%gil%ra
  yima%gil%d0 = yima%gil%dec
  !
  ! Patch for 1996/1997 data
  if (obs1.eq.0) then
    call cdate('20-jun-1996',obs1,error)
    call cdate('05-may-1997',obs2,error)
  endif
  if ((r_dobs.gt.obs1).and.(r_dobs.lt.obs2)) then
    yima%gil%a0 = r_lam
    yima%gil%d0 = r_bet
  endif
  !
  yima%gil%ptyp = r_proj
  yima%gil%freq = r_restf
  !
  return
end subroutine r_to_y
