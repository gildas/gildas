subroutine rix(ix,error)
  use classic_api
  use clic_file
  use clic_title
  !---------------------------------------------------------------------
  ! RW	Internal routine
  !	read an entry in the input file index
  !---------------------------------------------------------------------
  integer(kind=entry_length) :: ix          !
  logical         :: error                  !
  !
  ! Read title from file to buffer 
  call classic_entryindex_read(i,ix,tbuf,ibufi,error)
  if (error) return
  !
  ! Convert buffer to latest version index
  call read_index(i,error)
  !
end subroutine rix
!
subroutine rox(ix,error)
  use classic_api
  use clic_file
  use clic_title
  !---------------------------------------------------------------------
  ! RW	Internal routine
  !	read an entry in the output file index
  !---------------------------------------------------------------------
  integer(kind=entry_length) :: ix          !
  logical         :: error                  !
  !
  ! Read title from file to buffer 
  call classic_entryindex_read(o,ix,tbuf,obufi,error)
  if (error) return
  !
  ! Convert buffer to latest version index
  call read_index(o,error)
  !
end subroutine rox
! 
subroutine wox(error)
  use classic_api
  use clic_file
  use clic_title
  use clic_index
  !---------------------------------------------------------------------
  ! RW	Internal routine
  !	write an entry in the output file index
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Local
  integer(kind=entry_length) :: ix
  !
  if (o%lun.le.0) then
    error = .true.
    call message(8,4,'WOX','Output file not opened')
    return
  endif
  ix = o%desc%xnext
  if (ix.gt.m_ox) then
    error = .true.
    call message(8,4,'WOX','Output file index is full')
    return
  endif
  ! 
  ! Write title into index buffer tbuf
  call write_index(o,error)
  if (error) return
  !
  ! Write buffer to file
  call classic_entryindex_write(o,ix,tbuf,obufi,error)
  if (error) return
  !
  ! Update output index
  ox_num(ix)  = title%num
  ox_ver(ix)  = title%ver
  ox_bloc(ix) = title%bloc
  ox_word(ix) = title%word
  ox_scan(ix) = title%scan
  ox_ut(ix)   = title%ut
  ox_rece(ix) = title%recei
  !
  ! Also update input index if file both
  if (i%lun.eq.o%lun) then
    ix_num(ix)   = title%num
    ix_ver(ix)   = title%ver
    ix_bloc(ix)  = title%bloc
    ix_word(ix)  = title%word
    ix_kind(ix)  = title%kind
    ix_qual(ix)  = title%qual
    ! CLIC only
    ix_scan(ix)  = title%scan
    ix_itype(ix) = title%itype
    ix_proc(ix)  = title%proc
    ix_rece(ix)  = title%recei
    !
    call classic_recordbuf_nullify(ibufi)
    i%desc%xnext = ix+1
    !
  endif
  !  
  ! Increment file entry number
  o%desc%xnext=ix+1
  !
end subroutine wox
!
subroutine mox(ix,error)
  use classic_api
  use clic_file
  use clic_title
  use clic_index
  !---------------------------------------------------------------------
  ! RW 	Internal routine
  !	modifies an entry in the output file index
  !---------------------------------------------------------------------
  integer(kind=entry_length) :: ix  !
  logical :: error                  !
  ! Local
  character(len=80) :: mess
  character(len=10) :: nombre
  integer :: ii
  !
  if (o%lun.le.0) then
    error = .true.
    call message(8,4,'MOX','Output file not opened')
    return
  endif
  if (ix.le.0 .or. ix.gt.o%desc%xnext .or. ix.gt.m_ox) then
    error = .true.
    write(nombre,'(I6)') ix
    mess = 'Wrong index address '//nombre
    call message(8,3,'MOX',mess)
    return
  endif
  !
  ! Write title into index buffer tbuf
  call write_index(o,error)
  if (error) return
  !
  ! Write buffer to file
  call classic_entryindex_write(o,ix,tbuf,obufi,error)
  if (error) return
  ! 
  ! Update output index
  ox_num(ix)  = title%num
  ox_ver(ix)  = title%ver
  ox_bloc(ix) = title%bloc
  ox_word(ix) = title%word
  ox_scan(ix) = title%scan
  ox_ut(ix)   = title%ut
  ox_rece(ix) = title%recei
  !
  ! Also update input index if file both
  if (i%lun.eq.o%lun) then
    ix_num(ix)   = title%num
    ix_ver(ix)   = title%ver
    ix_bloc(ix)  = title%bloc
    ix_word(ix)  = title%word
    ix_kind(ix)  = title%kind
    ix_qual(ix)  = title%qual
    ! CLIC only
    ix_scan(ix)  = title%scan
    ix_proc(ix)  = title%proc
    ix_rece(ix)  = title%recei
    ix_itype(ix) = title%itype
    !
    call classic_recordbuf_nullify(ibufi)
    !
    ! Update current index if observation in it
    do ii=1, cxnext-1
      if (cx_num(ii).eq.title%num) then
        cx_ver(ii)  = title%ver
        cx_ind(ii)  = ix
        cx_bloc(ii) = ix_bloc(ix)
        cx_word(ii) = ix_word(ix)
      endif
    enddo
  endif
  !
end subroutine mox
!
subroutine cox(error)
  use classic_api
  use clic_file
  !---------------------------------------------------------------------
  ! RW	Internal routine
  !	Close an observation in the output file index
  !---------------------------------------------------------------------
  logical :: error                  !
  !
  if (o%lun.le.0) then
    error = .true.
    call message(8,4,'COX','Output file not opened')
    return
  endif
  !
  ! Write file descriptor
  call classic_filedesc_write(o,error)
  !
  ! Update input file descriptor if file both
  if (i%lun.eq.o%lun) then
     i%desc = o%desc
  endif
  !
end subroutine cox
!
subroutine rdx(ix,error)
  use classic_api
  use gbl_constant
  use clic_file
  use clic_title
  !---------------------------------------------------------------------
  ! RW	Internal routine
  !	read an entry in the external data file index
  !     results in lbloc, ....
  !---------------------------------------------------------------------
  integer(kind=entry_length) :: ix                     !
  logical :: error                  !
  ! Local
  character(len=10) :: nombre
  character(len=80) :: mess
  !
  if (ix.le.0 .or. ix.gt.d%desc%xnext) then
    error = .true.
    write(nombre,'(I6)') ix
    mess = 'Wrong index address '//nombre
    call message(8,3,'RDX',mess)
    return
  endif
  !
  if (d%lun.le.0) then
    error = .true.
    call message(8,4,'RDX','Input file not opened')
    return
  endif
  !
  ! Read title from file to buffer 
  call classic_entryindex_read(d,ix,tbuf,dbufi,error)
  if (error) return
  !
  ! Convert buffer to latest version index
  !
  call read_index(d,error)
  !
end subroutine rdx
