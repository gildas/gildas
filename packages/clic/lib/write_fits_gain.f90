subroutine write_fits_gain(unit, nsubant, subant, error)
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  ! Write the optional GAIN calibration fits table.
  ! assumes NO_POL=1
  !---------------------------------------------------------------------
  integer :: unit                   !
  integer :: nsubant                !
  integer :: subant(nsubant)        ! selected antennas in table
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  ! Local
  integer :: naxis, naxes(4), mf, status, nrows, tfields, varidat
  integer :: i, j, r_npol, r_nfeed, r_nside, mdigit2, mdigit1, isub
  parameter(r_npol = 1, r_nfeed = 1, r_nside=2, mdigit2=13,   &
    mdigit1=8)
  parameter (mf = 3)
  character(len=16) :: ttype(mf),tform(mf),tunit(mf), extname
  data ttype/'ANTENNID',   'AMPLGAIN',   'PHASGAIN'/
  data tform/'1J',         '*E',         '*E'/
  data tunit/' ',          'K/Jy',       'radian'/
  !---------------------------------------------------------------------
  !
  !  Append/create a new empty HDU onto the end of the file and move to it.
  status = 0
  call ftcrhd(unit,status)
  if (status .gt. 0) goto 99
  !
  !  Define parameters for the binary table (see the above data statements)
  nrows = r_nant
  tfields = mf
  extname = 'GAIN-ALMATI'
  varidat = 0
  ! columns in datapar
  do i = 2, 3
    call f_tform(tform(i),r_nside*(r_aicdeg+1),'E')
  enddo
  !      CALL F_TFORM(TFORM(4),2*2,'E')
  call ftphbn(unit,nrows,tfields,ttype,tform,tunit,   &
    extname,varidat,status)
  if (status .gt. 0) goto 99
  do i=2, 3
    naxis = 2
    naxes(1) = r_nside
    naxes(2) = r_aicdeg+1
    call ftptdm(unit, i, naxis, naxes, status)
  enddo
  !      NAXES(1) = 2
  !      NAXES(2) = r_nside
  !      CALL FTPTDM(UNIT, I, NAXIS, NAXES, STATUS)
  ! Other header keywords
  call ftpkys(unit,'TABLEREV','v1.0 2001-07-03',   &
    extname//' release',status)
  if (status .gt. 0) goto 99
  !      CALL FTPKYJ(UNIT,'NO_ANT',R_NANT,'Number of Antennas',STATUS)
  call ftpkyj(unit,'NO_ANT',nsubant,'Number of Antennas',status)
  if (status .gt. 0) goto 99
  call write_fits_dobs(unit,error)
  if (error) return
  call ftpkyj(unit,'NO_BAND',r_lband,'Number of base bands',status)
  if (status .gt. 0) goto 99
  call ftpkyj(unit,'NO_SIDE',r_nside,'Number of side bands',status)
  if (status .gt. 0) goto 99
  call ftpkyj(unit,'NO_GAIN',r_aicdeg+1,'Number of gain values',   &
    status)
  if (status .gt. 0) goto 99
  !
  status = 0
  !
  ! Fill in the columns.
  !
  !      DO I=1, R_NANT
  do isub =1, nsubant
    i = subant(isub)
    ! ANNAME
    call ftpclj(unit,1,i,1,1,r_kant(i),status)
    if (status .gt. 0) goto 99
    ! AMPLGAIN
    do j=0, r_aicdeg
      call ftpcle(unit,2,i,1+2*j,2,r_aicamp(1,1,i,j),status)
      if (status .gt. 0) goto 99
    enddo
    ! PHASGAIN
    do j=0, r_aicdeg
      call ftpcle(unit,3,i,1+2*j,2,r_aicpha(1,1,i,j),status)
      if (status .gt. 0) goto 99
    enddo
  enddo
  return
  !
99 call printerror('WRITE_FITS_GAIN',     status)
  error = .true.
end subroutine write_fits_gain
