subroutine solve_focus(line,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api
  !---------------------------------------------------------------------
  ! CLIC
  ! CLIC
  !	Support routine for command
  !	SOLVE FOCUS [/PLOT] [/OUTPUT File [NEW]] [/COMPRESS time_max]
  !	[/Store /Reset /Offset /Search] Not relevant here
  !       [/PRINT]
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_point.inc'
  include 'gbl_memory.inc'
  ! Local
  logical :: plot, output, end, flu
  real :: lo1, time_max, bsize, beam
  character(len=132) :: filnam,snam
  integer :: p_lun, q_lun, ier
  integer :: icount, naddr, nant, icount_previous
  integer(kind=address_length) :: data_in, ip_data, iaddr
  integer(kind=data_length)    :: ldata_in
  data p_lun, q_lun/0,0/
  data flu/.false./
  data bsize /0./
  !------------------------------------------------------------------------
  ! Code:
  iproc = 1
  !
  ! Get beam size
  beam = -1.0                  ! fix to frequency dep. value
  call sic_r4(line,0,2,beam,.false.,error)
  if (error) goto 999
  !
  ! /COMPRESS TIME_MAX option
  time_max = 5.0
  call sic_r4(line,6,1,time_max,.false.,error)
  if (error) goto 999
  call check_input_file(error)
  if (error) goto 999
  call check_index(error)
  if (error) goto 999
  plot = sic_present(1,0)
  !
  ! /PRINT option
  if (sic_present(10,0)) then
    ier = sic_getlun(q_lun)
    filnam = 'focus'
    snam = filnam
    call sic_parsef(snam,filnam,'INTER_OBS:','.obs')
    ier = sic_open (q_lun,filnam(1:lenc(filnam)),'NEW',.false.)
    if (ier.ne.0) then
      call message(6,3,'SOLVE_FOCUS',   &
        'Cannot open INTER_OBS:focus.obs')
      call messios(6,3,'SOLVE_FOCUS',ier)
      goto 999
    endif
  else
    q_lun = 0
  endif
  !
  ! /OUTPUT option
  if (sic_present(3,0)) then
    call output_file('SOLVE_FOCUS',line,3,p_lun,'.lis',   &
      '!   Obs.#   Scan Code Azimuth   Elevation   Time  '//   &
      '     Position  ..        Width      ..     Intensity'//   &
      '     Sigma    Source', error)
    if (error) goto 999
    output = .true.
  else
    output = .false.
  endif
  if (plot) call gr_exec1('TICKSPACE 0 0 0 0')
  !
  ! Get  a first one
  call get_first(.true.,error)
  if (error) goto 999
  end = .false.
  call check_focus (error)
  do while (error .and. .not.end)
    call get_next(end,error)
    if (error) goto 999
    if (.not.end) call check_focus(error)
  enddo
  if (error) goto 999
  icount = 0
  call get_data (ldata_in,data_in,error)
  if (error) goto 999
  ip_data = gag_pointer(data_in,memory)
  call load_point (memory(ip_data),   &
    icount,iaddr,naddr,error,lo1,nant,time_max)
    
  if (error) goto 999
  call load_focw  (beam,bsize,lo1,nant)
  !
  ! Get next ones alike
  call get_next(end,error)
  if (error) goto 999
  do while (.not.end)
    call check_focus(error)
    if (error) then
      if (icount.gt.1) then
        call sub_point (plot,output,p_lun,iaddr,naddr,q_lun,   &
          error,bsize,flu,end)
        call free_vm(naddr,iaddr)
        icount = 0
        if (error) goto 998
      endif
    elseif (icount.eq.0) then
      call get_data (ldata_in,data_in,error)
      if (error) goto 999
      ip_data = gag_pointer(data_in,memory)
      call load_point (memory(ip_data),   &
        icount,iaddr,naddr,error,lo1,nant,time_max)
      if (error) goto 998
    else
      call get_data (ldata_in,data_in,error)
      if (error) goto 999
      ip_data = gag_pointer(data_in,memory)
      icount_previous = icount
      call load_point (memory(ip_data),   &
        icount,iaddr,naddr,error,lo1,nant,time_max)
      call load_focw  (beam,bsize,lo1,nant)
      if (icount.eq.0.and.icount_previous.gt.1) then
        call sub_point (plot,output,p_lun,iaddr,naddr,q_lun,   &
          error,bsize,flu,end)
        call free_vm(naddr,iaddr)
        if (error) goto 998
        call load_point (memory(ip_data),   &
          icount,iaddr,naddr,error,lo1,nant,time_max)
        if (error) goto 998
      endif
    endif
    call get_next(end,error)
    if (error) goto 999
    if (sic_ctrlc()) goto 999
  enddo
  !
  if (icount.gt.1) then
    call sub_point (plot,output,p_lun,iaddr,naddr,q_lun,   &
      error,bsize,flu,end)
    call free_vm(naddr,iaddr)
  endif
  !
  goto 998
  !
999 error = .true.
998 if (output) then
    close(unit=p_lun)
    call sic_frelun(p_lun)
  endif
  if (q_lun.ne.0) then
    close (unit=q_lun)
    call sic_frelun(q_lun)
  endif
end subroutine solve_focus
!
subroutine check_focus(error)
  use classic_api  
  !---------------------------------------------------------------------
  ! CLIC
  !	Check if data is a pointing scan
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  ! Code:
  if (.not. r_presec(scanning_sec) .or. r_scaty.ne.1) then
    call message(6,3,'CHECK_FOCUS',   &
      'Scan is not a focus measurement')
    error = .true.
    return
  endif
end subroutine check_focus
!
subroutine output_file(caller,line,iopt,lun,ext,header,error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Open output file for logging
  !---------------------------------------------------------------------
  character(len=*) :: caller        !
  character(len=*) :: line          !
  integer :: iopt                   !
  integer :: lun                    !
  character(len=*) :: ext           !
  character(len=*) :: header        !
  logical :: error                  !
  ! Local
  character(len=6) :: ch
  character(len=80) :: snam,filnam
  integer :: ier, nch
  !
  integer :: mvoc1, nkey
  parameter (mvoc1=2)
  character(len=6) :: kw, voc1(2)
  data voc1/'NEW','OLD'/
  !------------------------------------------------------------------------
  ! Code:
  call sic_ch(line,iopt,1,snam,nch,.true.,error)
  if (error) return
  ch = 'OLD'
  call sic_ke(line,iopt,2,ch,nch,.false.,error)
  if (error) return
  call sic_ambigs('SOLVE FOCUS',ch,kw,nkey,voc1,mvoc1,error)
  if (error) return
  call sic_parsef(snam,filnam,' ',ext)
  ier = sic_getlun(lun)
  if (kw.eq.'NEW') then
    ier = sic_open (lun,filnam,'NEW',.false.)
    if (ier.eq.0) write(lun,'(a)') header
  elseif (kw.eq.'OLD') then
    ier = sic_open (lun,filnam,'APPEND',.false.)
  endif
  if (ier.ne.0) then
    call message(6,3,caller,'Cannot open file '//filnam)
    call messios(6,3,caller,ier)
    call sic_frelun(lun)
    error = .true.
    return
  endif
end subroutine output_file
!
!
subroutine load_focw (beam, bsize, lo1, nant)
  use gildas_def
  use classic_api  
  real :: beam                      !
  real :: bsize                     !
  real :: lo1                       !
  integer :: nant                   !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_par.inc'
  include 'clic_display.inc'
  ! Local
  real*4 :: freq
  !
  if (beam.lt.0.0) then
    if (n_band.eq.0 .or. i_band(1).eq.3) then
      freq = lo1               ! Sideband average
    elseif (i_band(1).eq.1) then
      freq = lo1+r_fif1      ! Upper sideband
    else
      freq = lo1-r_fif1      ! Lower sideband
    endif
    !         BSIZE = 2.8*115.0E3/FREQ       ! In mm
    bsize = 3.0*115.0e3/freq   ! In mm
    if (nant.lt.3) bsize = bsize*sqrt(2.0)
  elseif (beam.eq.0.0) then
    bsize = 0.0
  else
    bsize = beam
    if (nant.lt.3) bsize = bsize*sqrt(2.0)
  endif
end subroutine load_focw
