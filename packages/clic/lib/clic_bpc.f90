module clic_bpc
      character*12  :: bp_source
      integer(kind=4),allocatable :: bp_nant(:)   ! number of antennas available
      integer(kind=4),allocatable :: bp_ant(:,:)  ! logical antenna numbers
      integer(kind=4),allocatable :: bp_phys(:,:) ! physical antenna numbers
!      complex(kind=4),allocatable :: sba(:,:,:)   ! side-band averages (cont)
! continuum
      logical, allocatable :: fbp_camp(:,:,:,:)
      logical, allocatable :: fbp_cpha(:,:,:,:)
      integer(kind=4)      :: bpc_band            ! number of line subbands
      real(kind=4),allocatable :: bp_camp(:,:,:,:,:)
      real(kind=4),allocatable :: bp_cpha(:,:,:,:,:)
! line (channel)
      logical, allocatable :: fbp_lamp(:,:,:,:,:) ! true if fitted
      logical, allocatable :: fbp_lpha(:,:,:,:,:)
      integer(kind=4),allocatable :: bpc_deg(:,:,:,:,:) ! degree for chan. pol
      real(kind=4),allocatable :: bp_lamp(:,:,:,:,:,:)
      real(kind=4),allocatable :: bp_lpha(:,:,:,:,:,:)
! frequency
      logical, allocatable :: fbp_famp(:,:,:,:)
      logical, allocatable :: fbp_fpha(:,:,:,:)
      integer(kind=4),allocatable :: bpf_deg(:,:,:,:)    ! degree for fre. pol
!  if freq. interval (min, max)
      real(kind=4),allocatable :: bp_flim(:,:,:,:,:)
      real(kind=4),allocatable :: bp_famp(:,:,:,:,:)
      real(kind=4),allocatable :: bp_fpha(:,:,:,:,:)
! spectrum in memory
      complex(kind=4), allocatable :: specc(:), specl(:)
      real(kind=4), allocatable    :: bp_samp(:,:,:,:), bp_spha(:,:,:,:)
      real(kind=4), allocatable    :: bp_fsamp(:,:,:),bp_fspha(:,:,:)
      integer, allocatable :: nbp_samp(:,:), nbp_spha(:,:)
      logical,allocatable :: fbp_samp(:,:,:)
      logical,allocatable :: fbp_spha(:,:,:)
! cross switch bp
      real(kind=4), allocatable    :: bp_samp_cross(:,:,:,:)
      real(kind=4), allocatable    :: bp_spha_cross(:,:,:,:)
      real(kind=4), allocatable    :: bp_fsamp_cross(:,:,:)
      real(kind=4), allocatable    :: bp_fspha_cross(:,:,:)
      complex(kind=4), allocatable :: cpassc(:,:), cpassl(:,:)
      integer, allocatable :: nbp_samp_cross(:,:), nbp_spha_cross(:,:)
      logical,allocatable :: fbp_samp_cross(:,:,:)
      logical,allocatable :: fbp_spha_cross(:,:,:)
end module
