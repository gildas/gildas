subroutine solve_five(line,error)
  use gkernel_interfaces
  use gildas_def
  use classic_api
  !---------------------------------------------------------------------
  ! CLIC
  !	Support routine for command
  !	SOLVE FIVE [/PLOT] [/OUTPUT File [NEW]]
  !	[/Store /Reset /Offset /Closure /Search] Not relevant here
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_point.inc'
  include 'clic_display.inc'
  include 'gbl_memory.inc'
  ! Local
  logical :: plot, output, end, flux, tpoint
  real :: beam, lo1, bsize, time_max
  integer :: i, p_lun, nant
  integer :: old_scan, old_nrec, ia, q_lun
  integer(kind=address_length) :: ip_data, data_in
  integer(kind=data_length)    :: ldata_in
  real :: wpos(5,mnant), vpos(5,mnant), woffset, offset
  !------------------------------------------------------------------------
  flux = .false.
  q_lun = 0
  ! Code:
  iproc = 2
  time_max = 4.0
  call check_input_file(error)
  if (error) goto 999
  call check_index(error)
  if (error) goto 999
  plot = sic_present(1,0)
  !
  ! /OUTPUT FLUX|OLD|NEW, /PRINT
  call file_point(line,output,p_lun,flux,q_lun,tpoint,error)
  !
  if (i_band(1).eq.4) then
    call message(6,2,'SOLVE_FIVE',   &
      'Sideband Ratio invalid in this context')
    error = .true.
    return
  endif
  !
  ! Get beam size
  beam = -1.0                  ! fix to frequency dep. value
  call sic_r4(line,0,2,beam,.false.,error)
  if (error) goto 999
  if (plot) call gr_exec1('TICKSPACE 0 0 0 0')
  !
  ! Loop on current index
100 call get_first(.true.,error)
  if (error) goto 999
  lo1 = r_flo1
  call load_beam (beam, bsize,lo1,r_nant)
  end = .false.
  old_scan = r_scan
  old_nrec = r_nrec
  do ia=1, mnant
    do i=1, 5
      wpos(i,ia) = 0
      vpos(i,ia) = 0
    enddo
  enddo
  woffset = 0
  offset = 0
  do while (.not.end)
    error = .false.
    nfix = 0
    nmob = 0
    nant = r_nant
    do i=1,r_nant
      if (r_mobil(i)) then
        nmob = nmob+1
        imob(nmob) = i
      else
        nfix = nfix+1
        ifix(nfix) = i
      endif
    enddo
    ! Two antenna case
    if (r_nant.eq.2 .and. nfix.ne.1) then
      call message(6,3,'SUB_FIVE','One antenna must be fixed')
      error = .true.
      return
    endif
    !
    call get_data (ldata_in,data_in,error)
    if (error) goto 999
    ip_data = gag_pointer(data_in,memory)
    call load_five (memory(ip_data),r_nant,   &
      wpos,vpos,offset,woffset,error)
    if (error) goto 999
    !
    ! Next observation
    call get_next(end, error)
    if (error) goto 999
    if (end .or. r_scan.ne.old_scan .or. r_nrec.ne.old_nrec) then
      call sub_five(plot,q_lun,p_lun,output,flux,   &
        wpos,vpos,offset,woffset,bsize,error,end)
      if (error) goto 998
      old_nrec = r_nrec
      old_scan = r_scan
      do ia=1, mnant
        do i=1, 5
          wpos(i,ia) = 0
          vpos(i,ia) = 0
        enddo
      enddo
      woffset = 0
      offset = 0
    endif
  enddo
  goto 998
  !
999 error = .true.
998 if (output) then
    close(unit=p_lun)
    call sic_frelun(p_lun)
  endif
2000 format(   &
    '!   Obs.#   Scan Code Azimuth   Elevation   Time  ',   &
    '     Position  ..        Width      ..     Intensity',   &
    '     Sigma    Source')
end subroutine solve_five
!
subroutine load_five (data,nant,wpos,vpos,offset,woffset,   &
    error)
  use gildas_def
  use classic_api
  use clic_rdata
  !---------------------------------------------------------------------
  ! CLIC
  !	Support routine for SOLVE FIVE
  !	Measure antenna pointing position from a specialised
  !	routine in the real-time acquisition system
  ! Arguments:
  !	JADDR	I	Array address
  !	NADDR	I	Array length
  !	LO1	R*4 	LO1 frequency
  !	NANT	I	Number of antennas
  !---------------------------------------------------------------------
  real :: data(*)                   !
  integer :: nant                   !
  real :: wpos(5,nant)              !
  real :: vpos(5,nant)              !
  real :: offset                    !
  real :: woffset                   !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  include 'clic_point.inc'
  include 'clic_display.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(kind=data_length) :: h_offset, c_offset, l_offset
  integer :: i, ipos, ipol, kb, kif
  integer :: iant, ix, iy, jw1, jw2
  integer :: iv, im, k, kc, kl, kr
  real :: vt(4,3), wt(4,3)
  real :: offmin, wisi, posx, posy, visi
  parameter (offmin=5.)
  !
  ! Check it is OK
  ipol = 1 ! Dummy
  kb   = 1 ! Dummy
  kif  = 1 ! Dummy
  if (.not. r_presec(scanning_sec) .or. r_scaty.ne.6) then
    call message(6,3,'SUB_FIVE',   &
      'Scan is not a Five-Point measurement')
    error = .true.
    return
  endif
  iy = 1
  !
  if (do_pass) then
    call set_pass (r_nsb,r_nband,r_nbas+r_ntri,   &
      r_lntch, passc, passl, error)
    if (error) goto 999
  endif
  !
  ! Read visibilities and positions
  !
  ! Average record
  call spectral_dump(kr,0,0)
  k = h_offset(kr)+1
  call decode_header (data(k))
  call set_scaling(error)
  if (error) return
  do i = 1, nmob
    iant = imob(i)
    call spectral_dump(kr,iant,0)
    kl = 1 + l_offset(kr)
    kc = 1 + c_offset(kr)
    do ix=76, 77
      do iv=1,4
        do im=1,3
          vt(iv,im) = 0
          wt(iv,im) = 0
        enddo
      enddo
      call arecord (   &
        r_nsb, r_nband, r_nbas, r_lntch,   &
        data(kc), data(kl), passc, passl,   &
        iant, i_band(1), ipol, kb, kif,     &
        l_subb(1), i_subb(1,1),jw1,jw2,ix,iy,   &
        vt,wt,.false.,error)
      if (error) goto 999
      wisi =  wt(2,2)
      if (wt(2,2).gt.0) then
        if (ix.eq.76) then
          posx = vt(1,2)/wt(1,2)
          visi = vt(2,2)/wt(2,2)
        else
          posy = vt(1,2)/wt(1,2)
        endif
      else
        visi = blank4
      endif
    enddo
    !
    if (posx.lt.-offmin) then
      ipos = 1
    elseif (posx.gt.offmin) then
      ipos = 2
    elseif (posy.lt.-offmin) then
      ipos = 4
    elseif (posy.gt.offmin) then
      ipos = 5
    else
      ipos = 3
    endif
    wpos(ipos,i) = wpos(ipos,i) + wisi
    vpos(ipos,i) = vpos(ipos,i) + visi*wisi
    if (ipos.lt.3) then
      offset = offset+abs(posx)*wisi
      woffset = woffset+wisi
    elseif (ipos.gt.3) then
      offset = offset+abs(posy)*wisi
      woffset = woffset+wisi
    endif
  enddo
  return
999 error = .true.
  return
end subroutine load_five
!
subroutine sub_five(plot,qlun,iout,out,flux,   &
    wpos,vpos,offset,woffset,bsize,error,end)
  use gkernel_interfaces
  use classic_api  
  include 'clic_parameter.inc'
  logical :: plot                   !
  integer :: qlun                   !
  integer :: iout                   !
  logical :: out                    !
  logical :: flux                   !
  real :: wpos(5,mnant)             !
  real :: vpos(5,mnant)             !
  real :: offset                    !
  real :: woffset                   !
  real :: bsize                     !
  logical :: error                  !
  logical :: end                    !
  ! Global
  include 'clic_par.inc'
  include 'clic_point.inc'
  include 'clic_gauss.inc'
  include 'gbl_pi.inc'
  ! Local
  real :: dpos, aoff, boff, daoff, dboff, i0, di0
  real :: qaz, qel
  integer :: ia, j, iant, tant, n
  logical :: ok, clear
  character(len=80) :: chain
  character(len=12) :: answer
  real :: posi(3),gain(3),weig(3)
  integer :: dummy
  !
  if (woffset.gt.0) then
    offset = offset/woffset
  else
    call message(8,3,'SUB_FIVE','Incomplete five point scan')
  endif
  if (qlun.ne.0) then
    call check_scan(qlun,r_scan,'Pointing')
  endif
  clear = .true.
  do ia=1, nmob
    iant = imob(ia)
    tant = r_kant(iant)
    dpos = 0
    ok = .true.
    do j=1,5
      if (wpos(j,ia).gt.0) then
        vpos(j,ia) = vpos(j,ia)/wpos(j,ia)
        dpos = dpos+wpos(j,ia)
      else
        ok = .false.
      endif
    enddo
    if (ok) then
      dpos = dpos / 5
      dpos = 1e-3/sqrt(dpos)
      call gaufiv (vpos(1,ia),dpos,aoff,boff,i0,   &
        daoff,dboff,di0,bsize,offset)
      if (plot) then
        do icode=2, 3
          write (chain,1004) r_scan, r_nrec, r_sourc,   &
            r_az*180.d0/pi,   &
            r_el*180.d0/pi,'" 8 /BOX 8'
          np=3
          gain(1) = vpos(3*icode-5,ia)
          gain(2) = vpos(3*icode-4,ia)
          gain(3) = vpos(3,ia)
          posi(1) = -offset
          posi(2) = +offset
          posi(3) = 0
          weig(1) = wpos(3*icode-5,ia)
          weig(2) = wpos(3*icode-4,ia)
          weig(3) = wpos(3,ia)
          call plot_point(nmob,ia,tant,icode,clear,   &
            'DRAW TEXT 0 0.2 "'//chain(1:lenc(chain)),   &
            np,gain,posi,weig,'LIMITS * * 0 *')
          clear = .false.
          par(1) = i0*1.064467*bsize
          if (icode.eq.2) then
            par(2) = aoff
          else
            par(2) = boff
          endif
          par(3) = bsize
          call pro_point(error)
          write (chain,1015) 'DRAW TEXT 0.2 -0.2 "',   &
            par(2),'" 3 /BOX 7'
          call gr_exec1(chain(1:34))
        enddo
      endif
    else
      write(chain,'(a,i0)')   &
        'Incomplete five point scan for antenna ',ia
      call message(8,2,'SUB_POINT',chain(1:lenc(chain)))
      error = .true.
      return
    endif
    if (out) then
      call out_point(iout,flux,iant,'AZ',i0,di0,aoff,daoff,   &
        bsize,0.0,error)
      call out_point(iout,flux,iant,'EL',i0,di0,boff,dboff,   &
        bsize,0.0,error)
      if (error) return
    endif
    !
    ! However do nothing if the sigma is larger that 5 arc seconds (really bad).
    ! Note that R_COLL* are based on logical antennas
    ! tant is the corresponding physical antenna number
    if (daoff.lt.5.) then
      qaz = aoff + r_collaz(iant)
    else
      qaz = r_collaz(iant)
    endif
    if (dboff.lt.5.) then
      qel = boff + r_collel(iant)
    else
      qel = r_collel(iant)
    endif
    write(chain,1017)  tant, aoff, daoff, qaz, boff, dboff, qel
    call message(6,1,'SUB_POINT',chain(1:lenc(chain)))
    if (qlun.ne.0) then
      write(qlun,1007) qaz,qel,tant
    endif
  enddo
  !
  if (plot) then
    if (.not.end .and. sic_inter_state(dummy)) then
      answer = ' '
      write(6,*) '[Hardcopy - clear Alpha - Clear Plot - Quit]'
      call sic_wprn('I-PLOT,  Type H,A,Q,P or Return ',answer,n)
      call sic_blanc(answer,n)
      if (index(answer,'H').ne.0) call gr_exec('HARDCOPY /PRINT')
      if (index(answer,'A').ne.0) call gtclal
      if (index(answer,'Q').ne.0) error = .true.
      if (index(answer,'P').ne.0) call gtclear
    endif
  endif
  return
1004 format (i4,' R',i1,1x,a,2f8.2,a)
1007 format ('CORRECTIONS ',f6.1,1x,f6.1,'/ANTENNA ',i0)
1015 format (a,f5.1,a)
1017 format ('A',i0,' AZ:',f6.1,'(',f4.1,') Corr.',f6.1,   &
    ';   EL:',f6.1,'(',f4.1,') Corr.',f6.1)
end subroutine sub_five
!
subroutine gaufiv (z,dz,aoff,boff,i0,daoff,dboff,di0,bsize,offset)
  !---------------------------------------------------------------------
  !	Find the gaussian of known width going through five points
  !			5
  !		1	3	2
  !			4
  !	Z	R*4(5)	The measurements
  !	DZ	R*4	The typical error on Z
  !	OFFSET	R*4	Point separation
  !       bsize   R*4     Beam size
  !	AOFF	R*4	First axis offset		Output
  !	BOFF	R*4	First axis offset		Output
  !	I0	R*4	Peak intensity                  Output
  !	DAOFF	R*4	Error on AOFF                   Output
  !	DBAOFF	R*4	Error on BOFF                   Output
  !       DI0
  !---------------------------------------------------------------------
  real :: z(5)                      !
  real :: dz                        !
  real :: aoff                      !
  real :: boff                      !
  real :: i0                        !
  real :: daoff                     !
  real :: dboff                     !
  real :: di0                       !
  real :: bsize                     !
  real :: offset                    !
  ! Local
  real :: a1, a2, a3
  !
  a2 = bsize**2/4/log(2.)
  a1 = 4*offset/a2
  aoff = log(z(2)/z(1))/a1
  boff = log(z(5)/z(4))/a1
  daoff = dz/a1*sqrt(1/z(2)**2+1/z(1)**2)
  dboff = dz/a1*sqrt(1/z(4)**2+1/z(5)**2)
  a3 = (aoff**2+boff**2)/a2
  i0 = z(3)*exp(a3)
  di0 = (dz/z(3))**2+4/a2**2*((aoff*daoff)**2+(boff*dboff)**2)
  di0 = i0*sqrt(di0)
end subroutine gaufiv
!
subroutine check_subb(n,first,error)
  use gildas_def
  use classic_api  
  integer :: n                      ! number of subband groups to be checke
  logical :: first                  !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_display.inc'
  ! Local
  integer :: lch, lch2, i, j, ich, save_lband
  real*8 ::  save_lfcen(mrlband)
  real*4 ::  save_lfres(mrlband)
  character(len=80) :: ch, ch2
  save save_lband, save_lfcen, save_lfres
  !------------------------------------------------------------------------
  ! Code:
  if (n.eq.1 .and. n_subb.ne.1) then
    call message (6,2,'CHECK_SUBB',   &
      'Using only first set of subbands')
  endif
  if (n.gt.n_subb) then
    call message (6,2,'CHECK_SUBB',   &
      'Using only one subband')
    n = n_subb
  endif
  !
  if (first) then
    save_lband = r_lband
    do i= 1, r_lband
      save_lfcen(i) = r_lfcen(i)
      save_lfres(i) = r_lfres(i)
    enddo
  endif
  ch = ' '
  lch = 0
  lch2 = 0
  ch2 = ' '
  do i=1, n
    do j=1, l_subb(i)
      ich = i_subb(j,i)
      if ((ich.le.mbands .and. ich.gt.r_nband) .or.   &
        (ich.gt.mbands .and. ich-mbands.gt.r_lband)) then
        ch(lch+1:) = csub(ich)//' '
        lch = lch + 4
      endif
      if (ich.gt.mbands .and. (r_lfcen(ich-mbands).ne.save_lfcen(ich-mbands)   &
        .or. save_lfres(ich-mbands).ne.r_lfres(ich-mbands))) then
        ch2(lch2+1:) = csub(ich)//' '
        lch2 = lch2 + 4
      endif
    enddo
  enddo
  if (lch.gt.0) then
    call message(6,3, 'CHECK_SUBB', 'Subbands '//ch(1:lch)   &
      //'not available in data set')
    error = .true.
  elseif (.not.first .and. lch2.gt.0) then
    call message(6,3, 'CHECK_SUBB', 'Setup of subbands '   &
      //ch2(1:lch2)//'modified in data set')
    error = .true.
  endif
  return
end subroutine check_subb
!
!
subroutine out_point(iout,flux,iant,coo,i0,di0,off,doff,b,db,   &
    error)
  use classic_api  
  !---------------------------------------------------------------------
  ! Output a line in plot result files
  !---------------------------------------------------------------------
  integer :: iout                   !
  logical :: flux                   !
  integer :: iant                   !
  character(len=*) :: coo           !
  real :: i0                        !
  real :: di0                       !
  real :: off                       !
  real :: doff                      !
  real :: b                         !
  real :: db                        !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'gbl_pi.inc'
  ! Local
  integer :: icoo
  real :: coll
  character(len=40) :: chain
  !
  if (coo.eq.'AZ') then
    icoo = 1
    coll = r_collaz(iant)
  elseif (coo.eq.'EL') then
    icoo = 0
    coll = r_collaz(iant)
  endif
  if (.not.flux) then
    write(iout,2001) r_num, r_scan, icoo,   &
      r_az*180d0/pi, r_el*180d0/pi, r_ut*12d0/pi,   &
      r_kant(iant), r_istat(iant),   &
      off+coll, doff,   &
      b, db,   &
      i0, r_sourc, off
  else
    !
    ! For flux: compute error on intensity...
    ! NGRX: Use the image gain for polarization 1 !!!
    call datec (r_dobs,chain,error)
    write(iout,2011) r_num, r_scan, icoo,   &
      r_az*180d0/pi, r_el*180d0/pi, r_ut*12d0/pi,   &
      off, doff,   &           ! Position & error
      r_kant(iant),   &
      b, db,   &               ! Width & error
      i0, di0,   &
      r_gim(1,iant),   &
      r_restf*1e-3,   &
      (r_restf-2*r_isb*r_fif1)*1e-3,   &
      r_sourc,chain(1:11)
  endif
  return
  !
2001 format(1x,i8,i6,i4,3(2x,f8.3),1x,2i4,   &
    2(f9.2,' ',f8.2,'  '),1pg10.3,2x,'''',a,''' ',f9.2)
2011 format(1x,i8,i6,i3,3(1x,f8.3),1x,   &
    f9.2,1x,f8.2,1x,i2,1x,f9.2,1x,f8.2,1x,   &
    1pg10.3,1x,1pg10.3,1x,0pf6.2,2(1x,f8.3),1x,'''',a,'''',   &
    1x,'''',a,'''')
end subroutine out_point
!
subroutine file_point(line,output,p_lun,flux,q_lun,tpoint,error)
  use gkernel_interfaces
  use classic_api  
  !---------------------------------------------------------------------
  ! Open an output file for the PLOT session
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: output                 !
  integer :: p_lun                  !
  logical :: flux                   !
  integer :: q_lun                  !
  logical :: tpoint                 !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'gbl_pi.inc'
  ! Local
  logical :: new
  character(len=6) :: ch
  character(len=80) :: snam,filnam
  integer ::  ier
  integer :: nch, date(7)
  !
  integer :: mvoc1, i, nkey
  parameter (mvoc1=4)
  character(len=6) :: kw, voc1(mvoc1)
  data voc1/'OLD','NEW','FLUX','TPOINT'/
  !-----------------------------------------------------------------------
  output = .false.
  flux = .false.
  tpoint = .false.
  q_lun = 0
  p_lun = 0
  output = sic_present(3,0)
  if (output) then
    call sic_ch(line,3,1,snam,nch,.true.,error)
    if (error) goto 999
    flux = .false.
    new = .false.
    tpoint = .false.
    i = 2
    do while (i.le.sic_narg(3))
      call sic_ke(line,3,i,ch,nch,.true.,error)
      if (error) goto 999
      call sic_ambigs('SOLVE_FIVE',ch,kw,nkey,voc1,mvoc1,error)
      if (error) goto 999
      flux = flux.or.(kw.eq.'FLUX')
      new = new.or.(kw.eq.'NEW')
      tpoint = tpoint.or.(kw.eq.'TPOINT')
      i = i+1
    enddo
    if (tpoint) then
      call sic_parsef(snam,filnam,' ','.tpoint')
    else
      call sic_parsef(snam,filnam,' ','.int')
    endif
    ier = sic_getlun(p_lun)
    if (new) then
      ier = sic_open (p_lun,filnam,'NEW',.false.)
      if (ier.eq.0) then
        if (flux) then
          call user_info(snam)
          write(p_lun,2001) '! '
          write(p_lun,2003) '! CLIC'//char(92),   &
            'SOLVE POINT /OUT '//filnam(1:lenc(filnam)),   &
            ' FLUX NEW '
          write(p_lun,2001) '! '//snam(1:70)
          write(p_lun,2010)
        elseif (tpoint) then
          call jjdate (r_dobs+2460549.5d0+r_ut/pi/2,date)
          write(p_lun,2011) 'ALMA Antenna Test Facility: ',date
2011      format(a,i4.2,'-',i2.2,'-',i2.2,   &
            'T',i2.2,':',i2.2,':',i2.2,'.',i3.3,/,   &
            ': ALTAZ',/,   &
            ': ALLSKY',/,   &
            '34 04 29.80')
        else
          write(p_lun,2000)
        endif
      endif
    else
      ier = sic_open (p_lun,filnam,'APPEND',.false.)
    endif
    if (ier.ne.0) then
      call message(6,3,'SOLVE_POINT','Cannot open file '//filnam)
      call messios(6,3,'SOLVE_POINT',ier)
      error = .true.
      call sic_frelun(p_lun)
      return
    endif
  endif
  !
  ! PRINT option
  if (sic_present(10,0)) then
    if (output) then
      call message(6,3,'SOLVE POINT',   &
        'Incompatible option /PRINT and option /OUTPUT')
      goto 999
    endif
    ier = sic_getlun(q_lun)
    if (r_scaty.eq.1) then
      filnam = 'focus'
    else
      filnam = 'pointing'
    endif
    snam = filnam
    call sic_parsef(snam,filnam,'INTER_OBS:','.obs')
    ier = sic_open (q_lun,filnam(1:lenc(filnam)),'NEW',   &
      .false.)
    if (ier.ne.0) then
      call message(6,3,'SOLVE POINT',   &
        'Cannot open INTER_OBS:pointing.obs')
      call messios(6,3,'SOLVE POINT',ier)
      goto 999
    endif
  endif
  return
999 error = .true.
  return
2000 format(   &
     '!   Obs.#  Scan Code  Azimuth     Elevation   Time ', &
     '     Sta#     Position  ..       deltaPos  Width   ..   ', &
     '  Intensity',&
     '    Wind      Direct      Humidi     Temper     Press    ',&
     '  Refc1     Refc2       Refc3      RefCor     Source')

2010 format(   &
    '!   Obs.#  Scan Code Azimuth   Elevation  Time  ',   &
    '     Position  ..  A#     Width      ..     Intensity',   &
    '     ..  Gain   Fsignal  Fimage Source  Date')
2001 format(a)
2003 format(a,a,a)
end subroutine file_point
