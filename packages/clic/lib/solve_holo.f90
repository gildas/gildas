subroutine load_holo(iant,nvis,ivis,vis,all_bases,   &
    nbasu,basu,fudge,pcorr,acheat,pcheat,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api
  use clic_xypar
  use clic_rdata
  !---------------------------------------------------------------------
  ! CLIC
  !	Support routine for SOLVE HOLOGRAPHY
  ! Arguments:
  !	IANT	I	antenna used
  !       TIME_MAX R*4    Max. integration time (compression)
  !---------------------------------------------------------------------
  integer :: iant                   !
  integer :: nvis                   !
  integer :: ivis                   !
  real :: vis(5,nvis)               !
  logical :: all_bases              !
  integer :: nbasu                  !
  integer :: basu(nbasu)            !
  real :: fudge                     !
  real :: pcorr(2)                  !
  real :: acheat                    !
  real :: pcheat                    !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_point.inc'
  include 'gbl_memory.inc'
  include 'gbl_pi.inc'
  include 'clic_number.inc'
  include 'clic_proc_par.inc'
  ! Local
  logical :: caldm
  logical :: useit
  integer :: mdump, j, i_loop, i, ipol, kb, kif
  integer(kind=address_length) :: data_in, ipk
  integer(kind=data_length)    :: ldata_in, h_offset, c_offset, l_offset
  integer :: iband, ix, iy, jw1, jw2, ib, ir, iv, im, ja, k
  real :: vt(4,3), wt(4,3), dlam, sign, p_tolerance
  real :: radvis
  integer(kind=address_length) :: ipc, ipl, iph
  logical :: no_number
  !       REAL*8 ksi, MTH_BESSJ1
  !
  !------------------------------------------------------------------------
  !
  ipol = 1 ! Not used for the time being, but for consistent call to brecord
  kb = 1   ! imde
  kif = 1  ! idem
  error = .false.
  if (r_proc.eq.p_holog) then
    p_tolerance=30             ! arc seconds
  else
    p_tolerance=5
  endif
  !
  ! Compress (to time_max)
  call get_data (ldata_in,data_in,error)
  if (error) goto 999
  !
  source = r_sourc
  az = r_az
  el = r_el
  ut = r_ut
  mdump = r_ndump
  !
  ! Select subbands and channels to be averaged
  iband = i_band(1)
  jw1 = iw1(1)
  jw2 = iw2(1)
  call check_subb(1,.true.,error)
  if (error) return
  iy = 1                       ! Amplitude
  ix = 76                      ! Lambda
  !
  if (do_pass) then
    call set_pass (r_nsb,r_nband,r_nbas+r_ntri,   &
      r_lntch, passc, passl, error)
    if (error) return
  endif
  call set_corr(error)
  !
  ! Loop on records
  ipk = gag_pointer(data_in,memory)
  iph = ipk + h_offset(r_ndump)
  call decode_header (memory(iph))
  iph = ipk + h_offset(r_ndump/2)
  dlam = dh_offlam(iant)
  call decode_header (memory(iph))
  dlam =  dlam-dh_offlam(iant)
  sign = 1
  if (dlam.ne.0) sign = dlam/abs(dlam)
  do i_loop = 1, n_loop
    do ir = rec_1(i_loop), min(r_ndump,rec_2(i_loop)),   &
        rec_3(i_loop)
      !
      !c      DO IR = 1, R_NDUMP
      !
      iph = ipk + h_offset(ir)
      call decode_header (memory(iph))
      if (ivis.lt.nvis .and.   &
        (max(abs(dh_rmspe(1,iant)),   &
        abs(dh_rmspe(2,iant))).lt. p_tolerance))   &
        then

        ivis = ivis + 1
        call set_scaling(error)
        if (error) return
        vis(1,ivis) = (dh_offlam(iant)+sign*fudge+pcorr(1))   &
          *pi/180/3600
        vis(2,ivis) = (dh_offbet(iant)+pcorr(2))*pi/180/3600
        radvis = sqrt(dh_offlam(iant)**2 + dh_offbet(iant)**2)
        ipl = ipk + l_offset(ir)
        ipc = ipk + c_offset(ir)
        vis(3,ivis) = 0
        vis(4,ivis) = 0
        vis(5,ivis) = 0
        do ja = 1, r_nant
          useit = .false.
          if (ja.ne.iant) then
            ib = basant(min(ja,iant),max(ja,iant))
            if (.not.all_bases) then
              do j=1, nbasu
                useit = useit.or.(basu(j).eq.ib)
              enddo
            else
              useit = .true.
            endif
            useit = useit .and.   &
              .not.r_mobil(ja) .and. r_mobil(iant)
          endif
          ! specific case of SD holography: r_nant = r_nbas = 1
          if (r_nant.eq.1) then
            useit = .true.
            ib = 1
          endif
          if (useit) then
            do iv=1,4
              do im=1,3
                vt(iv,im) = 0
                wt(iv,im) = 0
              enddo
            enddo
            call brecord (   &
              r_nsb, r_nband, r_nbas+r_ntri, r_lntch,   &
              memory(ipc), memory(ipl), passc, passl,   &
              ib, iband, ipol, kb, kif, l_subb(1), i_subb(1,1),   &
              jw1,jw2,ix,iy, vt,wt,error)
            if (error) return
            if (no_number(wt(3,2))) then
              wt(3,2) = 0.
              vt(3,2) = 0.
              vt(4,2) = 0.
              print *, 'Bad data for ivis ',ivis
            endif
            if (ja.gt.iant) then
              vis(3,ivis) = vis(3,ivis)+wt(3,2)
              vis(4,ivis) = vis(4,ivis)+vt(3,2)
              vis(5,ivis) = vis(5,ivis)-vt(4,2)
            else
              vis(3,ivis) = vis(3,ivis)+wt(3,2)
              vis(4,ivis) = vis(4,ivis)+vt(3,2)
              vis(5,ivis) = vis(5,ivis)+vt(4,2)
            endif
          endif
        enddo
        if (vis(3,ivis).gt.0) then
          vis(4,ivis) = vis(4,ivis)/vis(3,ivis)
          vis(5,ivis) = vis(5,ivis)/vis(3,ivis)
        else
          vis(4,ivis) = 0.
          vis(5,ivis) = 0.
        endif
        !cc               vis(4,ivis) = vis(4,ivis)+acheat*cos(pcheat)*dh_test0(2)
        !cc               vis(5,ivis) = vis(5,ivis)+acheat*sin(pcheat)*dh_test0(2)
        if (radvis.lt.acheat) then
          print *, radvis, vis(5,ivis), ' set to zero'
          vis(5,ivis) = 0
        endif
        !print *, ja, ivis, (vis(k,ivis), k=1, 5)
1000    format(2i6,5(1pg12.5))
      endif
    !**
    !         ksi = 3.1459*sqrt(dh_offlam(iant)**2+dh_offbet(iant)**2)/41
    !         vis(4,ivis) = MTH_BESSJ1(ksi) / ksi *cos((ksi/100)**2)
    !         vis(5,ivis) =  MTH_BESSJ1(ksi) / ksi *sin((ksi/100**2)
    !**
    enddo
  enddo
  if (error) goto 999
  return
  !
999 error = .true.
  return
end subroutine load_holo
!
subroutine solve_holo(line,error)
  use gildas_def
  use gkernel_interfaces
  use clic_xypar
  use clic_title
  use clic_index
  !---------------------------------------------------------------------
  ! CLIC
  ! SOLVE HOLO [MODES nmode] [NP np] [MASK npm pm1 ... pmn] [ITER niter gain]
  ! /PLOT [NUMBER] [AMP min max step] [PHA min max step]
  ! defaults
  !         MODES 0
  !         NP    64
  !         MASK  0
  !         ITER  0
  !         NONUMBER
  !         AMP -15  0    3  (decibels)
  !         PHA -pi pi pi|8  (radians)
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_display.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  include 'clic_panels.inc'
  include 'clic_proc_par.inc'
  ! Local

  logical :: plot, end, number, plot_phases, all_bases, rigging, first, tempBias
  logical :: err, test_data, fresnel, feed_correction, apodize, docheat
  logical :: caldm, beamreference, fixSpacing, fixedAst
  integer :: nbasu, basu(mnbas), mvar, l, lt, ier
  integer, parameter :: maxvar = 8
  integer :: iant, nx, ny, lc, i, nxmap, nymap, lsc1, j, if, nn
  real :: el1,el2, taper
  real :: elevation, riggingElevation(2), biasTemperature, temperature           !  
  real :: dx, dy, xmprevious, ymprevious 
  real :: xmin, xmax, ymin, ymax, x1, x2, y1,y2, rx, ry, rmsu, rmsast
  real :: rmsw, amp_scale(3), pha_scale(3), z_scale(3)
  real :: taperx, tapery, x0, y0, sigu, sigw, eff(8)
  real :: ymmin, ymmax, xmmin, xmmax, xm, ym, jykel(3)
  real*8 :: boff(maxvar), bmes(maxvar), ames(5)
  real*8 :: kw, sig, bstart(3), btry(maxvar), sigmin
  real*8 :: bstep, beam, astamp, astdir
  complex :: cheat
  real :: acheat, pcheat
  real*4 :: holo_focus(4), holo_rms(2), center, phasrms, holo_astigm(4)
  save holo_focus, holo_rms, holo_astigm
  integer :: m_work, nmode, nh1, nh2, nvis, mvis
  integer :: ix,iy,iz,      ix1, ix2, iy1, iy2, iz1, iz2  ! Were REAL by mistake
  integer :: dtest(9), lmap, ivis, niter, narg, k
  real :: rlam, xref, xval, xinc, beamsize, gain, fudge
  real :: s_ring(mring),z_ring(mring),rms_ring(mring)
  save rms_ring
  integer(kind=address_length) :: ipx, ipy, ipw, ipwork, ipwork2
  integer(kind=address_length) :: i_work, addrvis, ipvis, ipp
  character(len=256) :: chain, h1, h2, result_name
  character(len=40) :: test_file
  integer :: npm, nfring, ifring(mring), nfsec, ifsec(16),lprint
  integer :: lch, lfich, pen_col
  character(len=5) :: pm(200)
  character(len=12) :: asourc,ateles
  !
  ! screw info for CalDM
  integer :: nscrew, nc,  nfilename
  character(len=12), allocatable :: screwname(:)
  real, allocatable :: screwmotion(:), screwmotionerror(:)
  real :: threshold(2)
  character(len=40) :: chaintype
  character(len=256) :: filename
  integer :: luid
  character(len=256) :: beammapuid, surfacemapuid, surfacemapfile
  character(len=256) :: beammapfile
  !
  integer :: mvoc1, mvoc2, mvoc3, nk, nd, ia, ib, kant, jant, mvoctype
  parameter (mvoc1=28, mvoc2=2, mvoc3=5, mvoctype=9)
  character(len=12) :: kw1, voc1(mvoc1), kw2, voc2(mvoc2), voc3(mvoc3), arg
  character(len=12) :: saved_teles, argbeam, argtype, voctype(mvoctype)
  real :: radius, f, distance, df, pcorr(2), thres, zm, scale, rowSpacing
  real :: astangle
  data df/0./, distance /1e10/ ! in meters ...
  data voc1/'MODE','ASTIGMATISM','NPOINTS','MASK','BASELINES',   &
    'ITERATIONS','FREE','DISTANCE','NOFEED', 'FOCUS','FRESNEL',   &
    'DEFOCUS','DIAMETER', 'TEST','FUDGE','NOFOCUS','NOXYFOCUS',   &
    'POINTING','APODIZE', 'CHEAT', 'TAPER','CALDM','BEAM','TYPE', &
    'RIGGING','FIXSPACING','TEMP_BIAS','REFERENCE'/
  data voc2/'RINGS','SECTORS'/
  data voc3/'AMPLITUDE','PHASE','ERRORS','NUMBER','COLOR_PANEL'/
  data voctype/'ASDM','VERTEX','AEC','MELCO12_1','MELCO12_2', &
       'MELCO12_3','MELCO12_4','BURE', 'IRAM30'/
  data m_work/0/
  data boff/8*0/, bstep/0.001/, bstart/3*0/
  save dtest, addrvis, mvis
  save m_work, i_work
  save taperx, tapery, x0, y0, eff, sigu, sigw, jykel
  !
  !------------------------------------------------------------------------
  ! Code:
  ! nmode is the number of modes used for each panel. (1,3,4, or 5)
  ! the default is 0: no calculation of panels.
  !   3 (no panel deformation).
  ! SOLVE HOLO MODES nmode NP np MASK npm pm1 ... pmn ITER niter gain
  ! FREE ring1 ring2 ...
  i = 2
  niter = 5
  gain = 1.0
  nmode = 0
  ant_type = -1
  !      NXMAP = 64
  !      NYMAP = 64
  nxmap = 256
  nymap = 256
  npm = 0
  nfring = 0
  nfsec = 0
  all_bases = .true.
  mvar = 6
  nbasu = 0
  test_data = .false.
  fresnel = .false.
  feed_correction = .true.
  beamreference = .false.
  pcorr(1) = 0
  pcorr(2) = 0
  cheat = 0
  docheat = .false.
  taper = 0
  acheat = 0
  pcheat = 0
  caldm = .false.
  rigging = .false.
  tempBias = .false.
  if (cxnext.gt.1) call get_first(.true.,error)
  if (error) goto 999
  distance = r_trandist
  df = r_tranfocu
  fudge = 0
  apodize = .false.
  argtype = 'ASDM'
  plot_phases = .false.
  fixSpacing = .false.
  rowSpacing = 0.
  fixedAst = .false.
  iant = -i_base(1)
  do while (i.le.sic_narg(0))
    call sic_ke(line,0,i,arg,narg,.true.,error)
    if (error) return
    call sic_ambigs('SOLVE HOLO',arg,kw1,nk,voc1,mvoc1,error)
    if (error) return
    i = i+1
    if (kw1.eq.'MODE') then
      nmode = 0
      call sic_i4(line,0,i,nmode,.false.,error)
      i = i+1
      write(6,*) nmode, ' modes.'
    elseif (kw1.eq.'ASTIGMATISM') then
      arg = ""
      call sic_ke(line,0,i,arg,narg,.false.,error)
      if (error) return
      if (arg.eq."ANGLE") then
        i =  i+1
        call sic_r4(line,0,i,astangle,.true.,error)
        if (error) return
        write (6,*) "Astigmatism angle forced to ",astangle," degrees."
        astangle = 180-astangle
        fixedAst = .true.
        i = i+1
        mvar = 7
     else
        mvar = 8
        ! mvar = 7
     endif
    elseif (kw1.eq.'NOFOCUS') then
      mvar = 3
    elseif (kw1.eq.'NOXYFOCUS') then
      mvar = 4
    elseif (kw1.eq.'NPOINTS') then
      nxmap = 64
      call sic_i4(line,0,i,nxmap,.false.,error)
      i = i+1
      nymap = nxmap
    !            WRITE(6,*) 'NPOINTS ',NXMAP,' x ',NYMAP
    elseif (kw1.eq.'MASK') then
      npm = 0
      call sic_i4(line,0,i,npm,.false.,error)
      i = i+1
      if (npm.gt.0) then
        do j=1, npm
          call sic_ch(line,0,i,pm(j),narg,.false.,error)
          i = i+1
          if (lenc(pm(j)).eq.4) pm(j) = ' '//pm(j)
        enddo
      endif
      write(6,*) 'Mask ',npm,' panels :',(pm(j),' ',j=1,npm)
    elseif (kw1.eq.'POINTING') then
      call sic_r4(line,0,i,pcorr(1),.false.,error)
      i = i+1
      call sic_r4(line,0,i,pcorr(2),.false.,error)
      i = i+1
      write(6,*) 'Applying pointing corrections :',pcorr,'"'
    elseif (kw1.eq.'CHEAT') then
      acheat = 0
      call sic_r4(line,0,i,acheat,.false.,error)
      docheat = .false.        ! acheat.ne.0
      i = i+1
      call sic_r4(line,0,i,pcheat,.false.,error)
      i = i+1
      cheat = acheat*exp(cmplx(0.,pcheat))
      cheat = 0
      write(6,*) 'Cheat by  :',acheat, pcheat, cheat
    elseif (kw1.eq.'BASELINES') then
      all_bases = .false.
      nbasu = 0
      err = .false.
      do while (.not.err .and. i.le.sic_narg(0))
        call sic_ke(line,0,i,arg,narg,.true.,err)
        if (.not.err) then
          err = .true.
          do j=1, mnbas
            if (cbas(j).eq.arg(1:5)) then
              nbasu = nbasu+1
              basu(nbasu) = j
              err = .false.
              i = i+1
            endif
          enddo
        endif
      enddo
      write(6,*) 'Using ',nbasu,' baselines :',   &
        (cbas(basu(j))//' ',j=1,nbasu)
    elseif (kw1.eq.'REFERENCE') then
      ! alma specific...
      all_bases = .false.
      nbasu = 0
      err = .false.
      do while (.not.err .and. i.le.sic_narg(0))
        call sic_ke(line,0,i,arg,narg,.true.,err)
        print *, 'REFERENCE ', arg
        if (.not.err) then
          err = .true.
          ! is this a valid antenna?
          kant = 0
          do k = 1, r_nant
            if (r_antennaName(k).eq.arg(1:4)) then
              kant = k
            endif
          enddo
          print *, kant
          if (kant.gt.0) then
            do j=1, mnbas
              print *, iant, kant, j, r_iant(j), r_jant(j)
              if (((r_iant(j).eq.iant).and.(r_jant(j).eq.kant)) &
                 .or.((r_iant(j).eq.kant).and.(r_jant(j).eq.iant))) then
                nbasu = nbasu+1
                basu(nbasu) = j
                err = .false.
                i = i+1
              endif
            enddo
          endif
        endif
      enddo
      write(6,*) 'Using ',nbasu,' baselines :',   &
        (cbas(basu(j))//' ',j=1,nbasu)
    elseif (kw1.eq.'ITERATIONS') then
      niter = 0
      call sic_i4(line,0,i,niter,.false.,error)
      i = i+1
      gain = 0.5
      call sic_r4(line,0,i,gain,.false.,error)
      i = i+1
      write(6,*) niter, ' Iter. Gain= ',gain
    elseif (kw1.eq.'DISTANCE') then
      distance = 1e15
      call sic_r4(line,0,i,distance,.false.,error)
      i = i+1
      write(6,*) ' Source distance(m)= ',distance
    elseif (kw1.eq.'FUDGE') then
      fudge = 1e15
      call sic_r4(line,0,i,fudge,.false.,error)
      i = i+1
      write(6,*) ' Fudge(")= ',fudge
    elseif (kw1.eq.'DIAMETER') then
      df = 0.0
      call sic_r4(line,0,i,diameter,.false.,error)
      i = i+1
      write(6,*) ' Diameter(m)= ',diameter
    elseif (kw1.eq.'TAPER') then
      taper = 12.0
      call sic_r4(line,0,i,taper,.false.,error)
      i = i+1
      write(6,*) ' Illumination taper(dB)= ',taper
    elseif (kw1.eq.'FOCUS') then
      df = 0.0
      call sic_r4(line,0,i,f,.false.,error)
      i = i+1
      write(6,*) ' Focus distance(m)= ',f
    elseif (kw1.eq.'DEFOCUS') then
      df = 0.0
      call sic_r4(line,0,i,df,.false.,error)
      i = i+1
      write(6,*) ' Focus offset(m)= ',df
    elseif (kw1.eq.'TYPE') then
      argtype = 'ASDM'
      call sic_ke(line,0,i,kw2,lc,.true.,error)
      i = i+1
      call sic_ambigs('SOLVE HOLO TYPE',kw2,argtype,nk,voctype,mvoctype,   &
        error)
      write(6,*) ' Antenna Type forced to ', argtype
    elseif (kw1.eq.'FREE') then
      call sic_ke(line,0,i,arg,narg,.true.,error)
      i = i+1
      call sic_ambigs('SOLVE HOLO FREE',arg,kw2,nk,voc2,mvoc2,   &
        error)
      if (error) return
      if (kw2.eq.'RINGS') then
        nfring = 0
        error = .false.
        do while (.not.error .and. i.le.sic_narg(0))
          call sic_i4(line,0,i,if,.false.,error)
          if (.not.error .and. if.ge.1 .and. if.le.mring) then
            i = i+1
            nfring = nfring + 1
            ifring(nfring) = if
          else
            error = .true.
          endif
        enddo
        error = .false.
        if (nfring.gt.0) then
          write(6,*) 'Rings ',(ifring(if),if=1,nfring),   &
            ' are not used for paraboloid fit'
        endif
      elseif (kw2.eq.'SECTORS') then
        nfsec = 0
        error = .false.
        do while (.not.error .and. i.le.sic_narg(0))
          call sic_i4(line,0,i,if,.false.,error)
          if (.not.error .and. if.ge.1 .and. if.le.16) then
            i = i+1
            nfsec = nfsec + 1
            ifsec(nfsec) = if
          else
            error = .true.
          endif
        enddo
        error = .false.
        if (nfsec.gt.0) then
          write(6,*) 'Sectors ',(ifsec(if),if=1,nfsec),   &
            ' are not used for paraboloid fit'
        endif
      endif
    elseif (kw1.eq.'TEST') then
      test_file = 'testFile'
      call sic_ch(line,0,i,test_file,lc,.false.,error)
      test_data = .true.
      i = i+1
    elseif (kw1.eq.'APODIZE') then
      apodize = .true.
    elseif (kw1.eq.'FRESNEL') then
      fresnel = .true.
    elseif (kw1.eq.'NOFEED') then
      feed_correction  = .false.
    elseif (kw1.eq.'BEAM') then
      argbeam = 'SIGNAL'
      call sic_ch(line,0,i,argbeam,lc,.false.,error)
      beamreference = argbeam(1:lc).eq.'REFERENCE'
      i = i+1
      !
    elseif (kw1.eq.'RIGGING') then
      riggingElevation(1) = 20.
      riggingElevation(2) = 90.
      rigging = .true.
      if (i.le.sic_narg(0)) then
        call sic_r4(line,0,i,riggingElevation(1),.true.,error)
        if (.not. error) then
          i = i+1
          riggingElevation(2) = riggingElevation(1)
          if (i.le.sic_narg(0)) then
            call sic_r4(line,0,i,riggingElevation(2),.true.,error)
            if (.not. error) then
              i = i+1
            endif
          endif
        endif
      endif
      !
    elseif (kw1.eq.'TEMP_BIAS') then
      biasTemperature = 273. 
      tempBias  = .true.
      if (i.le.sic_narg(0)) then
        call sic_r4(line,0,i,biasTemperature,.true.,error)
        if (.not. error) then
          i = i+1
        endif
      endif
      !
    elseif (kw1.eq.'CALDM') then
#if defined(SDM)
      caldm  = .true.
#else
      call message(6,2,'SOLVE_HOLO',   &
        'CalDM output (for ALMA) is not available')
#endif
    elseif (kw1.eq.'FIXSPACING') then
      fixSpacing = .true.
      rowSpacing = 0.
      if (i.le.sic_narg(0)) then
        call sic_r4(line,0,i,rowSpacing,.false.,error)
        if (.not. error) then
          i = i+1
        endif
      endif
      !
      ! (for compatibility)
    elseif (i.eq.2) then
      nmode = 0
      call sic_i4(line,0,2,nmode,.false.,error)
    elseif (i.eq.3) then
      nxmap = 64
      call sic_i4(line,0,3,nxmap,.false.,error)
      nymap = nxmap
    endif
    if (error) goto 999
  enddo
  if (beamreference) then
    apodize = .false.
  endif
  if (caldm) then
    print *, 'Result written in CalDM Format'
  endif
  if (apodize) then
    print *, 'Apodization of measured beam map'
  else
    print *, 'No Apodization of measured beam map'
  endif
  !
  if (distance.gt.1e6) then
    fresnel = .true.
  endif
  if (fresnel) then
    print *, 'Using Fresnel approximation'
  else
    print *, 'Including  Non-Fresnel terms'
  endif
  if (rigging) then
    write(6,*) 'Correction for gravitational deformation will be applied.'
    if (riggingElevation(2) .eq. riggingElevation(1)) then
      write(6,*) 'Single rigging angle set to ', riggingElevation(1),&
           ' degrees.' 
    else
      write(6,*) 'Rigging angle range set to ', riggingElevation(1), &
           riggingElevation(2),' degrees.' 
    endif
    riggingElevation = riggingElevation*pi/180.
  else
    write(6,*) 'No correction for gravitational deformation.'
  endif
  if (tempBias) then
    write(6,*) 'Correction for thermal deformation will be applied.'
    write(6,*) 'Bias temperature set to ', biasTemperature, 'kelvins' 
  else
    write(6,*) 'No correction for thermal deformation.'
  endif
  ! Get threshold for screw listing
  threshold = 0.
  call sic_r4(line,17,1,threshold(1),.false.,error)
  if (error) goto 999
  call sic_r4(line,17,2,threshold(2),.false.,error)
  if (error) goto 999
  write (6,*) 'Threshold for screws set to ',threshold(1),'microns'
  write (6,*) 'Threshold for screws set to ',threshold(2),'sigmas'
  ! Get plot scales
  plot = sic_present(1,0)
  number = .false.
  pen_col = 7
  amp_scale(1) = -15
  amp_scale(2) =   0
  amp_scale(3) = 3
  pha_scale(1) = -pi
  pha_scale(2) = +pi
  pha_scale(3) = pi/8
  ! in micrometers ...
  z_scale(1) = -500
  z_scale(2) = +500
  z_scale(3) = 50
  if (plot) then
    i = 1
    do while (i.le.sic_narg(1))
      call sic_ke(line,1,i,arg,narg,.true.,error)
      if (error) goto 999
      i = i+1
      call sic_ambigs('SOLVE HOLO /PLOT',arg,kw1,nk,voc3,mvoc3,   &
        error)
      if (error) return
      if (kw1.eq.'NUMBER') then
        number = .true.
      elseif (kw1.eq.'AMPLITUDE') then
        do k=1,3
          call sic_r4(line,1,i,amp_scale(k),.false.,error)
          i = i+1
        enddo
      elseif (kw1.eq.'PHASE') then
        plot_phases = .true.
        do k=1,3
          call sic_r4(line,1,i,pha_scale(k),.false.,error)
          i = i+1
        enddo
      elseif (kw1.eq.'ERRORS') then
        plot_phases = .false.
        do k=1,3
          call sic_r4(line,1,i,z_scale(k),.false.,error)
          i = i+1
        enddo
      elseif (kw1.eq.'COLOR_PANEL') then
        call sic_i4(line,1,i,pen_col,.false.,error)
        i = i+1 
      endif
    enddo
  endif
  !      LMAP = NXMAP*NYMAP
  !
  ! Now get the input data
  if (.not.test_data) then
    call check_input_file(error)
    if (error) goto 999
    call check_index(error)
    if (error) goto 999
    call get_first(.true.,error)
    if (error) goto 999
    !
    ! Patches for old data -- we need to insert an antenna Type in CLIC...
    if (r_teles.eq.'VTX-ALMATI') then
      call set_panels(type_vx12m)
    elseif (r_teles.eq.'AEC-ALMATI') then
      call set_panels(type_aec12m)
    endif
    write(6,*) 'I-SOLVE_HOLO, Using Antenna ', r_kant(iant)
    !
    !     Here select the type from r_antennatype. However this is not yet
    !     in the ASDM. Use r_kant, decoded from the antenna name, instead.
    if (argtype.eq.'ASDM') then
      if (r_kant(iant).le.100) then
        if ((r_kant(iant).eq.4).and.(r_dobs.le.-5445)) then  ! 31.09.2009 A4
          call set_panels(type_bure)
	else if ((r_kant(iant).eq.5).and.(r_dobs.le.-4787)) then ! 20.07.2011 A5 
	  call set_panels(type_bure)
	else if ((r_kant(iant).eq.6).and.(r_dobs.le.-4744)) then ! 01.09.2011 A6
	  call set_panels(type_bure)
	else if ((r_kant(iant).eq.2).and.(r_dobs.le.-4733)) then ! 12.09.2011 A2
	  call set_panels(type_bure)
	else if ((r_kant(iant).eq.1).and.(r_dobs.le.-4723)) then ! 22.09.2011 A1
	  call set_panels(type_bure)
	else if ((r_kant(iant).eq.3).and.(r_dobs.le.-4719)) then ! 26.09.2011 A3
	  call set_panels(type_bure)
        else
          call set_panels(type_bure2)
        endif
      elseif ((r_kant(iant).eq.101) .or.( r_antennaType(iant).eq.'VERTEX_12') &
           .or. (r_antennaType(iant).eq.'VERTEX_12_ATF')) then
        call set_panels(type_vx12m)
      elseif ((r_kant(iant).eq.102) .or. (r_antennaType(iant).eq.'AEM_12') &
           .or. (r_antennaType(iant).eq.'AEM_12_ATF')) then
        call set_panels(type_aec12m)
        ! dont'n know yet the antenna name for Melco tests...
      elseif ((r_kant(iant).eq.103) .or. (r_antennaType(iant).eq.'MITSUBISHI_12_A') &
           .or. (r_antennaType(iant).eq.'MITSUBISHI_12_B')) then
        if (r_antennaName(iant)(4:4) .eq.'1') then
          call set_panels(type_melco12m_1)
        elseif (r_antennaName(iant)(4:4) .eq.'2') then
          call set_panels(type_melco12m_2)
        elseif (r_antennaName(iant)(4:4) .eq.'3') then
          call set_panels(type_melco12m_3)
        elseif (r_antennaName(iant)(4:4) .eq.'4') then
          call set_panels(type_melco12m_4)
        endif
      endif
      !
      !     type explicit from command line:
      !
    elseif (argtype.eq.'BURE') then
       if ((r_kant(iant).eq.4).and.(r_dobs.le.-5445)) then  ! 31.09.2009  A4
          call set_panels(type_bure)
       else if ((r_kant(iant).eq.5).and.(r_dobs.le.-4787)) then ! 20.07.2011 A5 
	  call set_panels(type_bure)
       else if ((r_kant(iant).eq.6).and.(r_dobs.le.-4744)) then ! 01.09.2011 A6
	  call set_panels(type_bure)
       else if ((r_kant(iant).eq.2).and.(r_dobs.le.-4733)) then ! 12.09.2011 A2
	  call set_panels(type_bure)
       else if ((r_kant(iant).eq.1).and.(r_dobs.le.-4723)) then ! 22.09.2011 A1
	  call set_panels(type_bure)
       else if ((r_kant(iant).eq.3).and.(r_dobs.le.-4719)) then ! 26.09.2011 A3
	  call set_panels(type_bure)
       else
          call set_panels(type_bure2)
       endif 
    elseif (argtype.eq.'VERTEX') then
      call set_panels(type_vx12m)
    elseif (argtype.eq.'AEC') then
      call set_panels(type_aec12m)
    elseif (argtype.eq.'MELCO12_1') then
      call set_panels(type_melco12m_1)
    elseif (argtype.eq.'MELCO12_2') then
      call set_panels(type_melco12m_2)
    elseif (argtype.eq.'MELCO12_3') then
      call set_panels(type_melco12m_3)
    elseif (argtype.eq.'MELCO12_4') then
      call set_panels(type_melco12m_4)
    elseif (argtype.eq.'IRAM30') then
      call set_panels(type_iram30m)
    endif
    r_antennatype(iant) = chaintype(ant_type)
    lt = lenc(r_antennatype(iant))
    write(6,*) 'I-SOLVE_HOLO, '//r_antennatype(iant)(1:lt)
    !
    ! Screws have been defined - now forget type_bure2 for simplicity
    !
    if (ant_type.eq.type_bure2) ant_type=type_bure
    !
    !     count screws and (de)allocate
    !
    nscrew = 0
    do i = 1, nring
      nscrew = nscrew + mmode(i)*npan(i)
    enddo
    if (allocated(screwname)) then
      deallocate(screwname, screwmotion, screwmotionerror)
    endif
    allocate( screwname(nscrew), screwmotion(nscrew),   &
      screwmotionerror(nscrew))
    print *, nscrew, ' screws on antenna.'
    !
    !     Offsets
    do i=1, 3
      bstart(i) = 0
    enddo
    if (sic_present(5,0)) then
      do i=1,3
        call sic_r8(line,5,i,bstart(i),.true.,error)
      enddo
      if (error) goto 999
    endif
    !
    ! 1) - Loop to get limits
    ! count rows first
    nn = 0
    call get_first(.true.,error)
    if (error) goto 999
    end = .false.
    do while (.not.end)
      if (r_proc.eq.p_holog) then
        nn = nn + 1
      endif
      call get_next(end,error)
      if (error) goto 999
    enddo
    ! drop last row if odd number
    if (nn/2*2.lt.nn) then
      nn = nn-1
    endif
    !
    ymin = 1e6
    ymax = -1e6
    xmin = 1e6
    xmax = -1e6
    ymmin = 1e6
    ymmax = -1e6
    xmmin = 1e6
    xmmax = -1e6
    nvis = 0
    ivis = 0
    xmprevious = 0
    ymprevious = 0
    dx = 1e6
    dy = 1e6
    call get_first(.true.,error)
    if (error) goto 999
    end = .false.
#ifdef SDM
    if (caldm) then
      call sdm_holotimes(.true., error)
      if (error) goto 999
    endif
#endif
    nn = 1
    first = .true.
    do while (.not.end)
      !         do i=1, nn
      call check_holo(iant,y1,y2, ym, x1,x2, xm, error)
      if (error) goto 999
      !
      ! get time paramters for calDM result
#ifdef SDM
      if (caldm) then
        call sdm_holotimes(.false., error)
        if (error) goto 999
      endif
#endif
      ymin = min(y1,y2,ymin)
      ymax  = max(y1,y2,ymax)
      ymmin = min(ym,ymmin)
      ymmax = max(ym,ymmax)
      if (.not.first .and. (abs(ym-ymprevious) .gt.2.00)) then
         dy = min(dy,abs(ym-ymprevious))
         nn = nn+1
      endif
      ymprevious = ym
      !
      xmin = min(x1,x2,xmin)
      xmax = max(x1,x2,xmax)
      xmmin = min(xm,xmmin)
      xmmax = max(xm,xmmax)
      if (.not.first .and. (abs(xm-xmprevious) .gt.2.00)) then
        dx = min(dx,abs(xm-xmprevious))
        nn = nn+1
      endif
      xmprevious = xm
      !
      nvis = nvis + r_ndump
      !!print *, 'nn, xm, xMMIN, xmmax,  ym, YMMIN, ymmax ', &
      !!     nn, xm, xMMIN, xmmax,  ym, YMMIN, ymmax
      call get_next(end,error)
      if (error) goto 999
      first = .false.
    enddo
    !! print *, 'nn, dx, dy ', nn, dx, dy
    ! Get memory for visibility table (4 columns: lam, beta, real, imag, weight)
    !
    ! this is the voltage beam size
    beamsize = 41*(115000./r_flo1)*sqrt(2.)*(15./diameter)   &
      *pi/180/3600
    !
    ! scanning grid
    if (mvis.lt.nvis*5) then
      if (mvis.gt.0) call free_vm(mvis,addrvis)
      mvis= 5*nvis
      if (sic_getvm(mvis,addrvis).ne.1) goto 999
    endif
    ipvis = gag_pointer(addrvis,memory)
    ! a general algorithm is not easy. The scanning errors may make the minimum DYs
    ! not realiable for some prototype antennas.
    !         print *, 'nn, xmmin, xmmax,  ymmin, ymmax'
    !         print *, nn, xmmin, xmmax,  ymmin, ymmax
    ! this is the preferred row spacing (lambda/2/D that is twice oversampled).
    ! 
    if (fixSpacing) then
      if (rowSpacing .le.0) then
        rowSpacing = 299792458./r_flo1/1e6 / 3/ diameter  /pi*180*3600 
      endif
      dx = rowSpacing
      dy = rowSpacing
!      nx = 2*nint(max(abs(xmmax-xmmin),abs(ymmax-ymmin))/dx/2)
      nx = 2*nint(max(abs(xmax-xmin),abs(ymax-ymin))/dx/2)
      rx = nx/2 + 0.5
      ny = nx
      ry = rx 
      ! PUT A PIXEL ON CENTER (?)
      ry = ny/2+1
      rx = nx/2+1
      print *, nx, ' rows, spacing set to ', dx
    elseif ((ymmax-ymmin).gt.(xmmax-xmmin)) then
      if (ant_type.ne.type_bure) then
        ny = nn
        dy = (ymmax-ymmin)/(nn-1)
        ry = ny/2 + 0.5
        rx = ry
        rowSpacing = dy
      else
        rowSpacing = dy
        dy = dy/2
        ny = 8*nint(abs(ymmax-ymmin)/dy/8)
        ry = ny/2+1
        rx = ry
      endif
      dx = dy
      nx = ny
      print *, 'scanning in Az, ',ny, ' rows, spacing ',rowSpacing
    else
      if (ant_type.ne.type_bure) then 
        nx = nn
        dx = (xmmax-xmmin)/(nn-1)
        rx = nx/2 + 0.5
        ry = rx
        rowSpacing = dx
      else
        rowSpacing = dx
        dx = dx/2
        nx = 8*nint(abs(xmmax-xmmin)/dx/8)
        rx = nx/2+1
        ry = rx
      endif
      dy = dx
      ny = nx
      print *, 'scanning in El, ',nx, ' rows, spacing ',rowSpacing
    endif
    !         DY = 41.*87750./R_FLO1*(15./DIAMETER)
    !         DY = 35.4 !41.*87750./R_FLO1*(15./DIAMETER)
    !         NY = 8*NINT(ABS(YMAX-YMIN)/DY/8)
    !
    ! Axes in radians
    dy = dy *pi/180/3600
    dx = dx *pi/180/3600
    !     *!      RX = 1.-XMIN/DX
    !
    write (chain,'(a,i3,a,i3,a,f5.1,a,2f5.1)')   &
      'Beam map size ',nx,' by ',ny,   &
      ' step ',dx*180*3600/pi,' " ref ',rx,ry
    lc = lenc(chain)
    call message(6,1,'SOLVE_HOLO',chain(1:lc))
    lmap = nxmap*nymap
    write (chain,'(i3,a,i3)') nxmap,' by ',nymap
    call message(6,1,'SOLVE_HOLO','Output antenna map '   &
      //chain(1:10)//' pixels.')
    !
    call open_map(nx,ny,dx,dy,rx,ry,i_band(1),   &
      r_kant(iant),nxmap,nymap,distance,error)
    if (error) goto 999
    ipx = gag_pointer(xima%loca%addr,memory) 
    ipy = gag_pointer(yima%loca%addr,memory)
    !
    ! 2) - Get a the visibility table : pointer ipvis
    !
    call zero_holo(memory(ipvis),5*nvis)
    call get_first(.true.,error)
    if (error) goto 999
    end = .false.
    do while (.not.end)
      call load_holo(iant,nvis,ivis,memory(ipvis),all_bases,   &
        nbasu,basu,fudge,pcorr,acheat,pcheat,error)
      if (error) goto 999
      call get_next(end,error)
      if (error) goto 999
    enddo
    nvis = ivis
    write (chain,'(i6)') nvis
    call message(6,1,'SOLVE_HOLO','Using '//chain(1:6)//   &
      ' visibilities.')
    !     need to override again here.
    r_antennatype(iant) = chaintype(ant_type)
    !
    ! 2bis) - Grid the visibility table into the visibility (beam) map (in X)
    ! from ipvis to ipx
    !
    nd = 5
    ! mask relative weight threshold negative: do not mask
    thres = -1.
    !print *, 'nvis, beamsize,nd,thres'
    !print *, nvis, beamsize,nd,thres
    call sub_grid(nvis,memory(ipvis),beamsize,nd,thres,error)
    !$$$         call do_test1(MEMORY(IPX),NX,NY)
    !
    ! In the reference beam case, we're done.
    if (beamreference) then
      call close_map(error)
      return
    endif
    !
    if (distance.gt.0) then
      !         if (distance.gt.1000) then
      !     2.18 = a + 2c - f
      !     a = 0.803 between sec. focus and elev axis
      !     2c = 6.177 distbetween foci
      !     f = 4.8
      zm = (diameter/2)**2/4/focus
      if (ant_type.ge.type_melco12m_1 .and. ant_type.le.type_melco12m_4) then
        scale = 1.+(2.01+zm/2.)/distance
      else
        scale = 1. + (2.18+zm/2.)/distance
      endif
      dx = dx*scale
      dy = dy*scale
      xima%gil%inc(2) = xima%gil%inc(2)*scale
      xima%gil%inc(3) = xima%gil%inc(3)*scale
      yima%gil%inc(1) = yima%gil%inc(1)/scale
      yima%gil%inc(2) = yima%gil%inc(2)/scale
    endif
  !
  ! Otherwise use some test data
  else
    print *, 'Using gridded test data from '//test_file(1:lc)
    call load_x_image(test_file(1:lc),'.beam',error)
    if (error) return
    ipx = gag_pointer(xima%loca%addr,memory)
    nx = xima%gil%dim(2)
    ny = xima%gil%dim(3)
    dx = xima%gil%inc(2)
    dy = xima%gil%inc(3)
    lmap=nxmap*nymap
    call create_aperture_map(nxmap,nymap,error)
    ipy = gag_pointer(yima%loca%addr,memory)
    if (argtype.eq.'IRAM30') then
       write(6,*) 'I-SOLVE_HOLO, Antenna type is IRAM 30m antenna'
       call set_panels(type_iram30m)
    else
       write(6,*) 'I-SOLVE_HOLO, Assuming Vertex 12m Antenna '
       call set_panels(type_vx12m) 
    endif
  endif
  !
  ! 3) - Compute amplitude and phases FFT in Y image
  ! result in ipy, weight in ipw
  ! - extend map by zeroes
  if (m_work.lt.9*2*lmap) then
    if (m_work.gt.0) then
      call free_vm(m_work,i_work)
      m_work = 0
    endif
    if (sic_getvm(9*2*lmap,i_work).ne.1) goto 999
    m_work = 8*2*lmap
  endif
  ipwork = gag_pointer(i_work,memory)
  ipwork2 = ipwork + 8*2*lmap
  ipw = ipy+2*lmap 
  ipp = ipy+3*lmap 
  if (apodize) then
    call do_apodize(memory(ipx),nx,ny)
  endif
  !
  center = 2.18                ! distance between vertex and elevation axis
  kw = xima%gil%freq*2.d0*pi/299792458.d-6
  !print *, xima%gil%freq, kw
  !      if (distance.gt.0) then
  !         call reference_feed(NX,NY,MEMORY(IPX), KW, center)
  !      endif
  call  field_extend(memory(ipx),nx,ny,memory(ipwork),nxmap,nymap)
  if (distance.eq.0) then
    fresnel = .true.
  endif
  !print *, distance, nxmap,nymap,xima%gil%inc(2), yima%gil%inc(1)
 
  call field_to_aperture(nxmap,nymap,   &
    memory(ipwork), xima%gil%inc(2),   &
    memory(ipy), yima%gil%inc(1),   &
    memory(ipw),   &
    distance, fresnel, df, cheat)
  !
  ! compute phases (amplitude in plane 3, phase in plane 4 of Y image)
  call phases(memory(ipy),nxmap,nymap,memory(ipw),npm,pm,error)
  if (error) return
  !
  ! 3.5) At this point we correct the phases for the feed (ALMA Vertex).
  if ((ant_type.eq.type_vx12m .or. ant_type.eq.type_aec12m .or.   &
    (ant_type.ge.type_melco12m_1 .and. ant_type.le.type_melco12m_4)) &
    .and. feed_correction) then
    ipp = ipy+3*lmap
    call feedcorrection(   &
         nxmap,nymap,memory(ipp),error)
    if (error) return
    !
    ! We also compute rms with a 12dB taper.
    taper = 12.0
  elseif (ant_type.eq.type_iram30m.and.feed_correction) then
    ipp = ipy+3*lmap
    call feedcorrection30m(   &
         nxmap,nymap,memory(ipp),error)
    if (error) return
  endif
  !
  ! 4) - Fit a gaussian beam in the amplitudes.
  ! 5 parameters : intensity, edge taper in x and y , offsets in x and y,
  !
  ! Note that order is mandatory for later use
  !
  ! Fit illumination pattern: Amplitude is in IPY+2*LMAP,
  ! fitted illumination goes into IPY
  call fit_illum (memory(ipy), nxmap, nymap,   &
    memory(ipwork), memory(ipwork2), ames, lmap, npm, pm, docheat)
  radius = diameter/2.
  taperx = -ames(1)*radius**2 * 2
  tapery = -ames(2)*radius**2 * 2
  x0 = -ames(3)/2/ames(1)
  y0 = -ames(4)/2/ames(2)
  !
  ! 5) - Fit a paraboloid shape in the phases
  ! 6 parameters : source phase, source position (2 pars.),
  ! Focus coordinates (3 pars.)
  ! Phase data in IPY+3*LMAP, weight (amplitude) in IPY+2*LMAP
  !
  sigmin = 1e10
  do i=1, 8
    boff(i) = 0.d0
    bmes(i) = 0.d0
  enddo
  !      BOFF(7) = 0
  if (mvar.gt.3) then
    iz1 = -1
    iz2 = 1
    print *, '... Fitting Z focus '
  else
    iz1 = 0
    iz2 = 0
  endif
  if (mvar.gt.4) then
    ix1 = -1
    ix2 = 1
    iy1 = -1
    iy2 = 1
    print *, '... Fitting X and Y focus '
  else
    ix1 = 0
    ix2 = 0
    iy1 = 0
    iy2 = 0
  endif
  !
  !$$$      print *, 'ix1, ix2, iy1, iy2, iz1, iz2'
  !$$$      print *, ix1, ix2, iy1, iy2, iz1, iz2
  do ix=ix1, ix2
    boff(5) = (bstart(1)+ix*bstep)*kw
    do iy=iy1, iy2
      boff(6) = (bstart(2)+iy*bstep)*kw
      do iz=iz1, iz2
        boff(4) = (bstart(3)+iz*bstep)*kw
        do ia = -1, 1
          boff(2) = ia * bstep/radius*kw
          do ib = -1, 1
            boff(3) = ib * bstep/radius*kw
            call fit_holo (memory(ipy),nxmap,nymap,kw,   &
                 memory(ipwork), memory(ipwork2), boff, btry,   &
                 sig, lmap,nfring,ifring,nfsec,ifsec, mvar, df,astangle)
            if (sig.lt.sigmin) then
              sigmin = sig
              do i=1, mvar
                bmes(i) = btry(i)
              enddo
            endif
          enddo
        enddo
      enddo
    enddo
  enddo
  if (mvar.lt.4) then
    bmes(4) = bstart(3)*kw
  endif
  if (mvar.lt.5) then
    bmes(5) = bstart(1)*kw
    bmes(6) = bstart(2)*kw
  endif
  if (mvar.lt.7) then
    bmes(7) = 0
    bmes(8) = 0
  endif
  if (mvar.eq.7) then
    bmes(8) = sin(2*astangle*pi/180)*bmes(7)
    bmes(7) = cos(2*astangle*pi/180)*bmes(7)
  endif
  ! Phase residuals go in IPY+LMAP
  elevation = r_el
  temperature = r_tamb
  call res_holo(memory(ipy),nxmap,nymap,bmes,ames,kw,rmsu,rmsw,rmsast,   &
    sigu,sigw,eff,rms_ring,s_ring,z_ring, mvar, df, taper, &
    rigging, elevation, riggingElevation, tempBias, biasTemperature, &
    temperature, error)
 if (error) return
  !
  !     Corrected aperture map
  if (.true.) then
    !      if (.false.) then
    call corr_aperture(nxmap,nymap,   &
      memory(ipy+2*lmap),   &  ! amplitudes
      memory(ipy+1*lmap),   &  ! phases
      memory(ipwork+2*lmap))   ! complex result
  !
  ! Replace by fitted amplitudes and zero phase
  else
    call corr_aperture_perfect(nxmap,nymap,   &
      memory(ipy),   &         !  fitted amplitudes
      memory(ipy+1*lmap),   &  ! phases (unused)
      memory(ipwork+2*lmap))   ! complex result
  endif
  !
  ! Recompute the beam (recentered and refocussed
  !$$$      call APERTURE_TO_FIELD(NXMAP,NYMAP,
  !$$$     &        MEMORY(IPWORK), xima%gil%inc(2), ! output field map
  !$$$     &        MEMORY(IPWork+2*lmap), yima%gil%inc(1), !  input aperture
  !$$$     &        MEMORY(IPWork+4*lmap), ! work space
  !$$$     &        DISTANCE, FRESNEL, DF)
  !ccc      CALL  FIELD_EXTRACT(MEMORY(IPX),NX,NY,MEMORY(IPWORK),NXMAP,NYMAP)
  !
  ! print results
  call user_info(h1)
  nh1 = lenc(h1)+1
  if (r_presec(alma_sec)) then
    l = lenc(r_execblock)
    write(6,'(a,a)') 'Exec Block: ',r_execblock(1:l)
  endif
  if (lenc(r_config).gt.0) then
    if (r_presec(alma_sec)) then
      h1 = h1(1:nh1-1)//' - '//r_antennaname(iant)
      nh1 = lenc(h1)
      h1 = h1(1:nh1)//' - '//r_antennatype(iant)
    else
      write (h1(nh1:),'(a,i3,a,a)') ' - Ant ',r_kant(iant),   &
        ' - ',r_config(1:lenc(r_config))
    endif
  else
    write (6,'(a,i0)') ' - Ant. ',r_kant(iant)
  endif
  nh1 = lenc(h1)
  call rix(cx_ind(1),error)
  el1=r_el*180d0/pi
  lsc1 = title%scan
  call rix(cx_ind(cxnext-1),error)
  el2=r_el*180d0
  chain=r_cdobs(1:11)//' '//r_ctobs(1:5)//'UT'
  nh2 = lenc(chain)
  call bytoch(title%sourc,asourc,12)
  call bytoch(title%teles,ateles,12)
  write(h2,1003) asourc, ateles, lsc1, title%scan, chain(1:nh2), el1
1003 format(a,1x,a,' scans ',i4,' to ',i4,' ',a,' El:',f6.2)
  !      write(*,*) lsc1,lscan,el1,el2
  nh2 = lenc(h2)
  write(6,1004) h1(1:nh1), h2(1:nh2)
1004 format(/,1x,a,/,1x,a)
  !      BEAM=2.*1.895494/KW/(NX*DX/2.*PI/180/3600)
  beam=2.*1.895494/kw/(nx*dx/2.)
  write(6,1005) 'Wavelength is ',2*pi/kw*1000.,' mm,'
1005 format(1x,6(a,f7.3))
  write(6,1005) 'Resolution is ',beam,' m.'
  write(6,1005) 'Edge taper ',taperx, 'x', tapery,   &
    ' dB, centered at X= ',x0,' Y=',y0
  call sic_delvariable ('TAPER_X',.false.,error)
  call sic_delvariable ('TAPER_Y',.false.,error)
  call sic_delvariable ('OFFSET_X',.false.,error)
  call sic_delvariable ('OFFSET_Y',.false.,error)
  call sic_def_real ('TAPER_X',taperx,1,1,.false., error)
  call sic_def_real ('TAPER_Y',tapery,0,0,.false., error)
  call sic_def_real ('OFFSET_X',x0,0,0,.false., error)
  call sic_def_real ('OFFSET_Y',y0,0,0,.false., error)
  write(6,1005) 'Unweighted, phase rms ',sigu,' radians.'
  write(6,1005) 'Weighted, phase rms ',sigw,' radians.'
  write (6,1006) 'Transmitter distance ',distance,' meters.'
  call sic_delvariable ('RMS_PHA_U',.false.,error)
  call sic_delvariable ('RMS_PHA_C',.false.,error)
  call sic_def_real ('RMS_PHA_U',sigu,0,0,.false., error)
  call sic_def_real ('RMS_PHA_W',sigw,0,0,.false., error)
  write (6,1005) 'Focal distance ',focus,' meters.'
  write (6,1005) 'Focal displacement ',df*1000.,   &
    ' millimeters.'
  ! convert bmes to lengths in meters
  do i=1,8
    bmes(i) = bmes(i)/kw
  enddo
  do i=1, 4
    holo_focus(i) = bmes(3+i)*1000.
  enddo
  holo_rms(1) = rmsw
  holo_rms(2) = rmsu
  write(6,1005) 'Path and position offsets: ',bmes(1)*1000.,' mm ',   &
    bmes(2)*180*3600/pi,'" ', bmes(3)*180*3600/pi,'" '
1006 format(1x,a,8(1pg11.4))
  write(6,1005) 'Focus position: X= ',bmes(5)*1000.,   &
    ' Y= ',bmes(6)*1000.,' Z= ',bmes(4)*1000, ' mm.'
  write(6,1005) 'Astigmatism = (',  bmes(7)*1000, ', ',bmes(8)*1000, ') mm.'
  astdir = atan2(bmes(8), bmes(7))*180/pi/2.
  astdir = 180.-mod(astdir+3600d0, 180d0)
  holo_astigm(1) = bmes(7)*1000.
  holo_astigm(2) = bmes(8)*1000.
  holo_astigm(3) = rmsast
  holo_astigm(4) = astdir
    
  write(6,1005) 'Astigmatism rms = ', rmsast, 'mum, direction ',&
       astdir, 'degrees.'
  call sic_delvariable ('HOLO_FOCUS',.false.,error)
  call sic_def_real ('HOLO_FOCUS',holo_focus,1,4,.false.,error)
  call sic_delvariable ('HOLO_ASTIGM',.false.,error)
  call sic_def_real ('HOLO_ASTIGM',holo_astigm,1,4,.false.,error)
  call sic_delvariable ('HOLO_RMS',.false.,error)
  call sic_def_real ('HOLO_RMS',holo_rms,1,2,.false.,error)
  call sic_delvariable ('HOLO_RING',.false.,error)
  call sic_def_real ('HOLO_RING',rms_ring,1,nring,.false.,error)
  error = .false.
  !      WRITE(6,1005) 'Unweighted, normal rms is ',RMSU*1E6,' microns.'
  !      WRITE(6,1005) 'Weighted, normal rms is ',RMSW*1E6,' microns.'
  write(6,1005) 'Unweighted, normal rms is ',rmsu,' microns.'
  write(6,1005) 'Weighted, normal rms is ',rmsw,' microns.'
  write(6,1005) 'Illumination efficiency is ', eff(3)
  write(6,1005) 'Feed Taper efficiency is ', eff(4)
  write(6,1005) 'Ruze factor at ',kw/2/pi*0.299792458,   &
    ' GHz is ', eff(5)
  write(6,1005) 'Ruze factor at 230 GHz is ',eff(6)
  write(6,1005) 'Ruze factor at 345 GHz is ',eff(8)
  !
  write(6,1005) 'Aperture efficiency at ',kw/2/pi*0.299792458,   &
    ' GHz is ', eff(1)
  write(6,1005) 'Aperture efficiency at 230 GHz is ', eff(2)
  write(6,1005) 'Aperture efficiency at 345 GHz is ', eff(7)
  call sic_delvariable ('ETA',.false.,error)
  call sic_delvariable ('ETA_230',.false.,error)
  call sic_delvariable ('ETA_345',.false.,error)
  call sic_delvariable ('ETA_I',.false.,error)
  call sic_delvariable ('ETA_S',.false.,error)
  call sic_delvariable ('RUZE',.false.,error)
  call sic_delvariable ('RUZE_230',.false.,error)
  call sic_delvariable ('RUZE_345',.false.,error)
  call sic_def_real('RUZE',eff(5),0,0,.false.,error)
  call sic_def_real('RUZE_230',eff(6),0,0,.false.,error)
  call sic_def_real('RUZE_345',eff(8),0,0,.false.,error)
  call sic_def_real('ETA',eff(1),0,0,.false.,error)
  call sic_def_real('ETA_I',eff(3),0,0,.false.,error)
  call sic_def_real('ETA_S',eff(4),0,0,.false.,error)
  call sic_def_real('ETA_230',eff(2),0,0,.false.,error)
  call sic_def_real('ETA_345',eff(7),0,0,.false.,error)
  write(6,1005) 'S/T = ', 4/eff(1)*2*1.38e3/pi/225,' Jy/K'
  write(6,1005) 'S/T(230) = ', 4/eff(2)*2*1.38e3/pi/225,' Jy/K'
  write(6,1005) 'S/T(345) = ', 4/eff(7)*2*1.38e3/pi/225,' Jy/K'
  jykel(1) = 4/eff(1)*2*1.38e3/pi/225
  jykel(2) = 4/eff(2)*2*1.38e3/pi/225
  jykel(3) = 4/eff(7)*2*1.38e3/pi/225
  call sic_delvariable ('JYKEL',.false.,error)
  call sic_delvariable ('JYKEL_230',.false.,error)
  call sic_delvariable ('JYKEL_345',.false.,error)
  call sic_def_real('JYKEL',jykel(1),0,0,.false.,error)
  call sic_def_real('JYKEL_230',jykel(2),0,0,.false.,error)
  call sic_def_real('JYKEL_345',jykel(3),0,0,.false.,error)
  write(6,1008) 'Rms / ring  (1-',nring,'): ',   &
    (rms_ring(i),i=1,nring)
1008 format(1x,a,i1,a,8(1pg11.4))
  write(6,1008) 'Pos / ring  (1-',nring,'): ',   &
    (z_ring(i),i=1,nring)
  write(6,1008) 'Weight/ring (1-',nring,'): ',   &
    (s_ring(i)*100.,i=1,nring)
  !
  ! Fit panels if nmode.gt.0
  !
  if (nmode.gt.0) then
    ier = sic_getlun(lprint)
    call datec (r_dobs,chain(1:11),error)
    call sic_lower(chain(1:11))
    if (r_presec(alma_sec)) then
      call sic_get_char('SHORT_FILE',filename,nfilename,error)
      chain = filename(1:nfilename)//'.panels'
    !     only for old data ...
    elseif (r_teles.eq.'VTX-ALMATI') then
      chain(12:25) = '-Vertex.panels'
    elseif (r_teles.eq.'AEC-ALMATI') then
      chain(12:22) = '-AEC.panels'
    else
      write(chain(12:26),'(a3,i0,a2,i0,a7)')   &
        '-an',r_kant(iant),'-r',r_nrec,'.panels'
    endif
    lch = lenc(chain)
    ier = sic_open(lprint,chain(1:lch),'NEW',.false.)
    !
    ! Copy map phase to initialize residuals
    call r4tor4(memory(ipy+lmap),memory(ipwork),lmap)
    if (plot) then
      call zero_holo(memory(ipwork+lmap),nxmap*nxmap)
      call plot_holo(r_kant(iant),kw,   &
        sigu,sigw,rmsu,rmsw,eff,rms_ring,s_ring,z_ring,   &
        bmes,rmsast,taperx,tapery,x0,y0,h1,h2,basu,nbasu, &
        distance, rigging, riggingElevation, &
        tempBias, biasTemperature, temperature,fixedAst)
    !            CALL PLOT_MAPS(MEMORY(IPY+2*LMAP),MEMORY(IPY+4*LMAP),
    !     $           NXMAP,AMP_SCALE,Z_SCALE)
    endif
    rlam = 2*pi/kw
    xref = yima%gil%ref(1)
    xval = yima%gil%val(1)
    xinc = yima%gil%inc(1)
    elevation = r_el
    call solve_panels(niter,gain,  &
      memory(ipy),                 &  ! fitted amplitude
      memory(ipwork),              &  ! input observed map phase
      memory(ipwork+lmap),         &  ! output phase of panel fit
      memory(ipwork+6*lmap),       &  ! output residuals
      nxmap,rlam,xref,xval,xinc,   &
      nmode,r_kant(iant),h1, h2,   &
      memory(ipwork+2*lmap),       &  ! complex work space
      memory(ipwork+4*lmap),       &  ! complex work space
      nx,ny,plot,z_scale,lprint,   &
      memory(ipwork+2*lmap),       &  ! real work space only for plot
      memory(ipwork+4*lmap),       &  ! real work space only for plot
      npm, pm,   &

      nscrew, screwname, screwmotion, screwmotionerror, phasrms, &
      threshold,error)
    close(lprint)
    call sic_frelun(lprint)
    if (error) return
    !
    write (chain, '(a,f5.2,a)') 'Residual after panel fit: ',phasrms*1e6,&
         '\gmm'
    l = lenc(chain)
    if (plot) then
      call plot_ctrs(memory(ipwork+2*lmap),'Panel fit',   &
        memory(ipwork+4*lmap), chain(1:l),   &
        nxmap,z_scale,z_scale,beam,number,r_kant(iant),pen_col)
    endif
  else
    if (plot) then
      !            if (docheat) then
      !               call amp_residuals(MEMORY(IPY),nxmap,nymap)
      !            endif
      call plot_holo(r_kant(iant),kw,sigu,sigw,rmsu,   &
           rmsw,eff,rms_ring,s_ring,z_ring,bmes,rmsast,taperx,tapery,   &
           x0,y0,h1,h2,basu,nbasu, distance, &
           rigging, riggingElevation, & 
           tempBias, biasTemperature, temperature,fixedAst)
      if (plot_phases) then
        call plot_maps(memory(ipy+2*lmap),memory(ipy+lmap),   &
          nxmap,amp_scale,pha_scale)
        call plot_ctrs(memory(ipy+2*lmap),'Amplitude',   &
          memory(ipy+lmap),'Phase (focussed)',   &
          nxmap,amp_scale,pha_scale,beam,number,r_kant(iant),pen_col)
      else
        call plot_maps(memory(ipy+2*lmap),memory(ipy+4*lmap),   &
          nxmap,amp_scale,z_scale)
        call plot_ctrs(memory(ipy+2*lmap),'Amplitude',   &
          memory(ipy+4*lmap),'Normal errors',   &
          nxmap,amp_scale,z_scale,beam,number,r_kant(iant),pen_col)
      endif
    !            if (docheat) then
    !               call amp_restore(MEMORY(IPY),nxmap,nymap)
    !            endif
    endif
  endif
  !
  ! Close files and return
  call close_map(error)
  ! ============================
#ifdef SDM
  ! Result in Cal DM format
  if (.not.caldm .or. nmode.le.0) return
  print *, 'Write CalDM result ... '
  call sic_get_char('SHORT_FILE',chain,nc,error)
  chain =chain(1:nc)//'-Result'
  !c      chain = 'HolographyResult'
  call sic_parsef(chain,result_name,' ','.sdm')
  lfich = lenc(result_name)
  ! does not work on directories...
  !      call gag_delete(result_name(1:lfich))
  call system('rm -fR '//result_name(1:lfich)//char(0))
  call sdm_init(result_name(1:lfich)//char(0),error)
  if (error) return
  call system('mkdir '//result_name(1:lfich))
  call system('mkdir '//result_name(1:lfich)//'/ASDMBinary')
  ! reset all identifiers
  call init_sdm
  !
  !     may be some more arguments needed...
  call sdm_getuid(beammapuid)
  call sdm_getuid(surfacemapuid)
  call write_sdm_calholography(nmode, rmsu, rmsw, bmes, nscrew ,   &
    screwname, screwmotion, screwmotionerror, surfacemapuid,   &
    beammapuid, error)
  if (error) return
  call message(2,1,'SOLVE_HOLO','Write the CalDM result file')
  surfacemapfile = surfacemapuid
  call uidtofilename(surfacemapfile)
  luid = lenc(surfacemapfile)
  call gildas_fits_sub(xima%file,result_name(1:lfich)//'/ASDMBinary/'//   &
    surfacemapfile(1:luid),'STANDARD',32,error)
  beammapfile = beammapuid
  call uidtofilename(beammapfile)
  luid = lenc(beammapfile)
  call gildas_fits_sub(yima%file,result_name(1:lfich)//'/ASDMBinary/'//  &
    beammapfile(1:luid),'STANDARD',32,error)
  call sdm_write()
  call sdm_close()
  ! ============================
#endif
  return
  !
999 error = .true.
  return
end subroutine solve_holo
!
subroutine fit_illum(v, nx, ny, a,b, ames, nxy, npm,pm, docheat)
  use gildas_def
  use clic_xypar
  integer, parameter :: mvar=5      !
  integer :: nx                     !
  integer :: ny                     !
  real*4 :: v(nx,ny,4)              ! the amplitude is in V(3)
  integer :: nxy                    !
  real*8 :: a(nxy,*)                !
  real*8 :: b(*)                    !
  real*8 :: ames(mvar)              !
  integer :: npm                    !
  character(len=5) :: pm(200)       !
  logical :: docheat                !
  ! Global
  include 'gbl_pi.inc'
  ! Local
  real*8 :: sig
  integer :: nd
  !
  integer :: i, j, k, nra
  real*8 :: x, y, x2, y2, r2
  real :: xx, yy
  real :: vmax, xxmax, yymax
  logical :: mask
  external :: mask                ! not an intrinsic
  !
  print *, '... fitting illumination pattern'
  k = 0
  vmax = -1000.
  do j=1, ny
    y = yima%gil%val(2)+(dble(j)-yima%gil%ref(2))*yima%gil%inc(2)
    y2 = y*y
    yy = y
    do i=1, nx
      x = yima%gil%val(1)+(dble(i)-yima%gil%ref(1))*yima%gil%inc(1)
      x2 = x*x
      xx = x
      !
      ! Mask beyond edges and 4-pod., holes, optionally some panels
      if (.not.mask(xx,yy,0.,npm,pm)) then
        k = k+1
        a(k,1) = x2
        a(k,2) = y2
        a(k,3) = x
        a(k,4) = y
        a(k,5) = 1.0d0
        ! Fit power pattern:
        !               B(K) = 2.0D0*DBLE(V(I,J,3))
        ! Fit amplitude pattern:
        b(k) = dble(v(i,j,3))
        if (b(k) .gt. vmax) then
          vmax = b(k)
          xxmax = xx
          yymax = yy
        endif
      endif
    enddo
  enddo
  nd = k
  nra = nx*ny
  !! print *, 'vmax, xxmax, yymax ', vmax, xxmax, yymax 
  !
  ! Fit linear system
  call mth_fitlin ('FIT_ILLUM',nd,mvar,a,b,nra,sig)
  do i=1, mvar
    ames(i) = b(i)
  enddo
  ! Put fitted illumination in V(1)
  do j=1, ny
    y = yima%gil%val(2)+(dble(j)-yima%gil%ref(2))*yima%gil%inc(2)
    y2 = y*y
    do i=1, nx
      x = yima%gil%val(1)+(dble(i)-yima%gil%ref(1))*yima%gil%inc(1)
      x2 = x*x
      r2 = x2+y2
      v(i,j,1) = (x2*b(1)+y2*b(2)+x*b(3)+y*b(4)+b(5))
    !cccccccc
    !            if (docheat) then
    !               v(i,j,3) = (10**(0.1*V(I,j,3))-10**(0.1*V(I,J,1)))
    !            endif
    enddo
  enddo
end subroutine fit_illum
!
subroutine amp_residuals(v, nx, ny)
  use gildas_def
  integer :: nx                     !
  integer :: ny                     !
  real*4 :: v(nx,ny,4)              !
  ! Local
  integer :: mvar
  integer :: npm
  ! the amplitude is in v(3), and the amplitude is in v(1)
  integer :: i, j
  do j=1, ny
    do i=1, nx
      v(i,j,3) = (10**(0.1*v(i,j,3))-10**(0.1*v(i,j,1)))
    enddo
  enddo
end subroutine amp_residuals
subroutine amp_restore(v, nx, ny)
  use gildas_def
  integer :: nx                     !
  integer :: ny                     !
  real*4 :: v(nx,ny,4)              !
  ! Local
  integer :: mvar
  integer :: npm
  ! the amplitude is in v(3), and the amplitude is in v(1)
  integer :: i, j
  do j=1, ny
    do i=1, nx
      v(i,j,3) = 10*log10(v(i,j,3)+10**(0.1*v(i,j,1)))
    enddo
  enddo
end subroutine amp_restore
!
subroutine fit_holo(v,nx,ny,kw,a,b,boff,bmes,sig,nxy,   &
    nfr,ifr,nfs,ifs,mvar,defocus,astangle)
  use gildas_def
  use clic_xypar
  integer :: nx                     !
  integer :: ny                     !
  real*4 :: v(nx,ny,4)              !
  real*8 :: kw                      !
  integer :: nxy                    !
  real*8 :: a(nxy,*)                !
  real*8 :: b(*)
  integer, parameter :: maxvar = 8!
  real*8 :: boff(maxvar)                 !
  real*8 :: bmes(maxvar)                 !
  real*8 :: sig                     !
  integer :: nfr                    !
  integer :: ifr(nfr)               !
  integer :: nfs                    !
  integer :: ifs(nfs)               !
  integer :: mvar                   !
  real*4 :: defocus                 !
  real*4 :: astangle
  ! Global
  include 'gbl_pi.inc'
  include 'clic_panels.inc'
  ! Local
  integer :: iring, ipan
  integer :: l, nd
  !
  integer :: i, j, k, m, nra, nsector
  real :: x, y, x2, y2, r2, cx, cz, w, xx, yy, def
  logical :: free
  character(len=5) :: chain,cpanel
  !
  cz = 1./2./focus**2
  cx = 1./4./focus**3
  def = defocus/focus
  !
  k = 0
  do j=1, ny
    y = yima%gil%val(2)+(dble(j)-yima%gil%ref(2))*yima%gil%inc(2)
    y2 = y*y
    do i=1, nx
      x = yima%gil%val(1)+(dble(i)-yima%gil%ref(1))*yima%gil%inc(1)
      if (abs(v(i,j,4)-yima%gil%bval).gt.yima%gil%eval) then
        call xypanel(x,y,iring,ipan,xx,yy)
        free = .false.
        do m=1, nfr
          free = free .or. iring.eq.ifr(m)
        enddo
        chain = cpanel(iring,ipan)
        read (chain,'(i2)') nsector
        do m=1, nfs
          free = free .or. nsector.eq.ifs(m)
        enddo
        if (.not.free) then
          x2 = x*x
          r2 = x2+y2
          k = k+1
          a(k,1) = 1.d0
          a(k,2) = x
          a(k,3) = y
          ! Z now in 4.
          ! include defocus
          a(k,4) = 1-(1-r2/4/focus**2+def)   &
            /sqrt(r2/focus**2+(1.-r2/4/focus**2+def)**2)
          a(k,5) = x/focus*(1./(1.+def)   &
            -1./sqrt(r2/focus**2+(1.-r2/4/focus**2+def)**2))
          a(k,6) = y/focus*(1./(1.+def)   &
            -1./sqrt(r2/focus**2+(1.-r2/4/focus**2+def)**2))
          ! old formulation :
          !                  A(K,5) = X/FOCUS*(1.-1./(1.+R2/4/FOCUS**2))
          !                  A(K,6) = Y/FOCUS*(1.-1./(1.+R2/4/FOCUS**2))
          !                  A(K,4) = 1-(1-R2/4/FOCUS**2)/(1.+R2/4/FOCUS**2)
          ! end old formulation
          !
          if (mvar.eq.7) a(k,7) = ((x2-y2)*cos(2*astangle*pi/180) &
     &                             + 2*x*y*sin(2*astangle*pi/180))*cz
          if (mvar.gt.7) a(k,7) = (x2-y2)*cz
          if (mvar.gt.7) a(k,8) = 2*x*y*cz
          ! plane 4 is the observed phase 
          b(k) = dble(v(i,j,4))
          do l=1, mvar
          !do l=1, mvar
            b(k) = b(k)-boff(l)*a(k,l)
          enddo
          ! Set in [-PI,PI[ range
          b(k) = mod(b(k)+21d0*pi,2d0*pi)-pi
          !     CALL RANGE_PI8(B(K))
          ! do not weigh by amplitude.
          ! This gives too much weight to the central part to fit the focus
          ! and can produce long range phase errors on the edge of the dish.
          !                  W = 0.1D0*V(I,J,3)
          !                  W = 10.D0**W
          !                  B(K) = B(K)*W
          !                  DO L=1,6
          !                     A(K,L) = A(K,L)*W
          !                  ENDDO
        endif
      endif
    enddo
  enddo
  nd = k
  nra = nx*ny
  !
  ! Solve linear system
  call mth_fitlin ('FIT_HOLO',nd,mvar,a,b,nra,sig)
  do i=1, mvar
    bmes(i) = b(i)+boff(i)
  enddo
end subroutine fit_holo
!
subroutine range_pi8(b)
  real*8 :: b                       !
  ! Global
  include 'gbl_pi.inc'
  ! Local
  real*8 :: twopi
  parameter (twopi=2.d0*pi)
  !
  do while (b.le.-pi)
    b = b+twopi
  enddo
  do while (b.gt.pi)
    b = b-twopi
  enddo
end subroutine range_pi8
!
subroutine res_holo(v,nx,ny,bmes,ames,kw,   &
     rmsu,rmsw,rmsast,&
     sigu,sigw,eff,rms_ring,s_ring,z_ring,mvar,defocus,taper, &
     rigging, elevation, riggingElevation, tempBias, biasTemperature, &
     temperature, error)
  use gildas_def
  use clic_xypar
  !---------------------------------------------------------------------
  ! Compute in V(2) the residuals from the fit in V(4)
  !---------------------------------------------------------------------
  include 'clic_panels.inc'
  integer :: nx                     !
  integer :: ny                     !
  integer, parameter :: Maxvar=8
  real :: v(nx,ny,5)                !
  real*8 :: bmes(maxvar)                 !
  real*8 :: ames(5)                 !
  real*8 :: kw                      !
  real :: rmsu                      !
  real :: rmsw  
  real :: rmsast                     !
  real :: sigu                      !
  real :: sigw                      !
  real :: eff(8)                    !
  real :: rms_ring(mring)           !
  real :: s_ring(mring)             !
  real :: z_ring(mring)             !
  integer :: mvar                   !
  real :: defocus                   !
  real :: taper                     !
  real :: xref                      !
  real :: xval                      !
  real :: xinc                      !
  real :: rlam                     !
  ! Global
  include 'gbl_pi.inc'
  ! Local
  real :: xx, yy
  real :: a(maxvar)
  real*8 :: k230, k345, df
  real :: x, y, x2, y2, r2, f, cx, cz, w, sw, su, def
  real :: meansu, meansw, meanw, meanu, d
  integer :: i, j, l, npanel, iring, ipanel
  complex :: e, e230, e345
  real :: e1, e2, twopi, pf, spf, radius
  character(len=5) :: pch
  !
  real :: elevation, riggingElevation(2) 
  real :: temperature, biasTemperature
  logical :: error, rigging, tempBias
  real, allocatable :: phrig(:,:), phTemp(:,:)
  !  
  ! wave vector at 230 GHz:
  parameter (k230=2d0*pi*230d9/299792458d0)
  ! wave vector at 345 GHz:
  parameter (k345=2d0*pi*345d9/299792458d0)
  !
  if (tempBias) then
    allocate (phtemp(nx, ny))
    rlam = 2*pi/kw
    xref = yima%gil%ref(1)
    xval = yima%gil%val(1)
    xinc = yima%gil%inc(1)
    print *, 'temperature ', temperature,   ' biasTemperature ', biasTemperature
    call temperature_mode(phTemp, nx, xref, xval, xinc, rlam,   &
         temperature, biasTemperature, error)
    if (error) return
  endif
  if (rigging) then
    allocate(phrig(nx,ny))
    rlam = 2*pi/kw
    xref = yima%gil%ref(1)
    xval = yima%gil%val(1)
    xinc = yima%gil%inc(1)
    ! print *, 'elevation ', elevation, 'riggingElevation ', riggingElevation
    call rigging_mode(phrig,nx,xref,xval,xinc,rlam,   &
         Elevation, riggingElevation, error)
    if (error) return
  endif
  print *, '... computing phase residuals'
  if (taper.gt.0.0) then
    print *, '    taper ',taper,' used to compute rms.'
  else
    print *, '    amplitude weighting used to compute rms.'
  endif
  radius = diameter/2.
  cz = 1./2./focus**2
  cx = 1./4./focus**3
  def = defocus/focus
  twopi = 2.d0*pi
  rmsu = 0
  rmsw = 0
  rmsast = 0
  meansu = 0
  meansw = 0
  sw = 0
  su = 0
  sigu = 0
  sigw = 0
  do i=1, nring
    rms_ring(i) = 0
    s_ring(i) = 0
    z_ring(i) = 0
  enddo
  e = 0
  e1 = 0
  e2 = 0
  e230 = 0
  e345 = 0
  spf = 0
  do j=1, ny
    y = yima%gil%val(2)+(j-yima%gil%ref(2))*yima%gil%inc(2)
    y2 = y*y
    do i=1, ny
      x = yima%gil%val(1)+(i-yima%gil%ref(1))*yima%gil%inc(1)
      x2 = x*x
      r2 = x2+y2
      pf = ames(1)*x2+ames(2)*y2+ames(3)*x+ames(4)*y
      pf = 10.0**(0.1*pf)
      if (r2.lt.(radius)**2) spf = spf + pf
      if (abs(v(i,j,4)-yima%gil%bval).gt.yima%gil%eval) then
        a(1) = 1
        a(2) = x
        a(3) = y
        ! defocus
        a(4) = 1-(1-r2/4/focus**2+def)   &
          /sqrt(r2/focus**2+(1.-r2/4/focus**2+def)**2)
        a(5) = x/focus*(1./(1.+def)   &
          -1./sqrt(r2/focus**2+(1.-r2/4/focus**2+def)**2))
        a(6) = y/focus*(1./(1.+def)   &
          -1./sqrt(r2/focus**2+(1.-r2/4/focus**2+def)**2))
        ! old formulation :
        !               A(4) = 1-(1-R2/4/FOCUS**2)/(1.+R2/4/FOCUS**2)
        !               A(5) = X/FOCUS*(1.-1./(1.+R2/4/FOCUS**2))
        !               A(6) = Y/FOCUS*(1.-1./(1.+R2/4/FOCUS**2))
        ! end old formulation
        a(7) = (x2-y2)*cz
        a(8) = 2*x*y*cz
        v(i,j,2) = v(i,j,4)
        !do l=1,mvar
        do l=1, 8
          v(i,j,2) = v(i,j,2)-bmes(l)*a(l)
        enddo
!!!         ! TEST
!!!         v(i,j,2) = 0
!!!         do l=7,8
!!!           v(i,j,2) = v(i,j,2)+bmes(l)*a(l)
!!!         enddo
!!!         ! END TEST
        ! add the phase due to the rigging
        if (rigging) v(i,j,2) = v(i,j,2) + Phrig(i,j)
        ! add the thermal bias phases
        if (tempBias) v(i,j,2) = v(i,j,2) + PhTemp(i,j)
        !! Replace the phase by the bias as a test
!!         if (rigging) v(i,j,2) = Phrig(i,j)
        ! if (tempBias) v(i,j,2) = PhTemp(i,j)
        !!
        ! Set in [-PI,PI[ range
        v(i,j,2) = mod(v(i,j,2)+21d0*pi,2d0*pi)-pi
        ! 
        ! in micrometers ...
        v(i,j,5) = v(i,j,2)/kw/2*sqrt(1+r2*cz/2)*1e6
        if (taper.le.0.0) then
          !--     Bure option: weight by amplitude. Use normal rms
          w = 10.0**(0.1*v(i,j,3))
          d = v(i,j,5)
        else
          !--     weight by targetted voltage illumination pattern. Use projected rms
          w = 1.-(1.-10**(-0.05*taper))*r2/radius**2
          d = v(i,j,2)/kw/2*1e6
        endif
        ! fitted illumination power pattern
        rmsw = rmsw + w*d**2
        rmsu = rmsu + d**2
        rmsast = rmsast + w*((bmes(7)*a(7)+bmes(8)*a(8))/kw/2*1e6)**2
        sigw = sigw + w*v(i,j,2)**2
        sigu = sigu + v(i,j,2)**2
        meansw = meansw + w*d
        meansu = meansu + d
        meanw = meanw + w*v(i,j,2)
        meanu = meanu + v(i,j,2)
        sw = sw + w
        su = su + 1
        e = e + w*exp(cmplx(0.0,v(i,j,2)))
        e230 = e230 + w*exp(cmplx(0.0d0,v(i,j,2)*k230/kw))
        e345 = e345 + w*exp(cmplx(0.0d0,v(i,j,2)*k345/kw))
        e1 = e1 + w
        e2 = e2 + w**2
        call xypanel(x,y,iring,ipanel,xx,yy)
        rms_ring(iring) = rms_ring(iring) + w*d**2
        z_ring(iring) = z_ring(iring) + w*d
        s_ring(iring) = s_ring(iring) + w
      else
        v(i,j,2) = yima%gil%bval
        v(i,j,5) = yima%gil%bval
      endif
    enddo
  enddo
  sigu = sqrt(sigu/su)
  sigw = sqrt(sigw/sw)
  !      RMSU = SQRT(RMSU/SU)
  !      RMSW = SQRT(RMSW/SW)
  ! Mean was not properly subtracted before...
  rmsu = sqrt(rmsu/su-(meansu/su)**2)
  rmsw = sqrt(rmsw/sw-(meansw/sw)**2)
  rmsast = sqrt(rmsast/sw)
  do i=1, nring
    if (s_ring(i).ne.0) then
      ! now computed in microns
      z_ring(i) = z_ring(i)/s_ring(i)
      ! Mean of ring was not properly subtracted before...
      rms_ring(i) = sqrt(rms_ring(i)/s_ring(i)-z_ring(i)**2)
    !            RMS_RING(I) = SQRT(RMS_RING(I)/S_RING(I))*1E6
    !            Z_RING(I) = Z_RING(I)/S_RING(I)*1E6
    endif
    s_ring(i) = s_ring(i)/sw
  enddo
  spf = spf * yima%gil%inc(1)*yima%gil%inc(2)
  e = e * yima%gil%inc(1)*yima%gil%inc(2)
  e230 = e230 *yima%gil%inc(1)*yima%gil%inc(2)
  e345 = e345 *yima%gil%inc(1)*yima%gil%inc(2)
  e1 = e1 *yima%gil%inc(1)*yima%gil%inc(2)
  e2 = e2 *yima%gil%inc(1)*yima%gil%inc(2)
  ! eff(1) is the aperture efficiency (excluding ohmic losses)
  eff(1) = abs(e)**2/e2/(pi*radius**2)
  ! eff(2) is the same at 230 GHz
  eff(2) = abs(e230)**2/e2/(pi*radius**2)
  ! eff(2) is the same at 345 GHz
  eff(7) = abs(e345)**2/e2/(pi*radius**2)
  ! eff(3) is the illumination efficiency (aperture efficiency with no
  ! surface errors)
  eff(3) = e1**2/e2/(pi*radius**2)
  ! eff(1 or 2)/eff(3) should be the Ruze factor.
  ! eff(4) is the feed spillover efficiency
  eff(4) = 0
  eff(5) = 0
  eff(6) = 0
  eff(8) = 0
  if (ames(2).ne.0 .and. ames(1).ne.0) then
    eff(4) = spf   &
      *10**(0.1*(ames(3)**2/2/ames(1)+ames(4)**2/2/ames(2)))   &
      /(pi/(0.1*alog(10.)*sqrt(abs(ames(1)*ames(2)))))
  endif
  if (eff(3).gt.0) then
    eff(5) = eff(1)/eff(3)
    eff(6) = eff(2)/eff(3)
    eff(8) = eff(7)/eff(3)
  endif
  return
end subroutine res_holo
!
subroutine zero_holo(v,n)
  integer :: n                      !
  real :: v(n)                      !
  ! Local
  integer :: i
  do i=1, n
    v(i) = 0
  enddo
end subroutine zero_holo
!
character*40 function chaintype(anttype)
  integer anttype
  if (anttype.eq.1) then
    chaintype = 'IRAM/Bure 15-m Antenna'
  elseif (anttype.eq.2) then
    chaintype = 'IRAM/NOEMA 15-m Antenna'
  elseif (anttype.eq.3) then
    chaintype = 'ALMA/Vertex 12-m Antenna'
  elseif (anttype.eq.4) then
    chaintype = 'ALMA/AEC 12-m Antenna'
  elseif (anttype.eq.5) then
    chaintype = 'ALMA/MELCO 12-m Antenna'
  elseif (anttype.eq.10) then
    chaintype = 'IRAM/PicoVeleta 30-m Antenna'
  endif
  return
end function chaintype
