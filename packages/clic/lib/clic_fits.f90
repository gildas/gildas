subroutine clic_fits(line,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api
  !---------------------------------------------------------------------
  !  Writes the available line subbands as a FITS spectrum.
  !  FITS file_name
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'gbl_pi.inc'
  include 'clic_xy_code.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(kind=address_length) :: data_p, ipx,ipy,ipw,ipp
  integer(kind=address_length) :: kpp
  integer :: isb,  i, nd, is, m_p, nch, isub
  real :: weight
  logical :: plot
  character(len=132) :: name,fich,argum
  character(len=12) :: telescope
  !
  data m_p /0/
  data plot/.false./
  !
  save m_p, data_p
  !------------------------------------------------------------------------
  ! Code:
  !
  ! Verify if any input file
  call check_input_file(error)
  if (error) return
  !
  call check_index(error)
  if (error) return
  !
  error = .false.
  call sic_ch(line,0,1,argum,nch,.true.,error)
  if (error) return
  !
  ! Force some defaults:
  call get_first(.false.,error)
  if (error) return
  !
  ! Channel as X
  n_x = 1
  i_x(1) = xy_chann
  i_x(1) = xy_chann
  sm_x1(1) = '*'
  sm_x2(1) = '*'
  !
  ! Real and Imaginary as Y, or AUTO
  if (i_y(1).eq.xy_auto) then
    n_base = r_nant
    do i=1, n_base
      i_base(i) = -i
    enddo
    n_y = 1
    sm_y1(1) = '*'
    sm_y2(1) = '*'
  else
    n_base = r_nbas
    do i=1, n_base
      i_base(i) = i
    enddo
    n_y = 2
    i_y(1) = xy_real
    i_y(2) = xy_imag
    sm_y1(1) = '*'
    sm_y2(1) = '*'
    sm_y1(2) = '*'
    sm_y2(2) = '*'
  endif
  ! 1 subbands only
  !      N_SUBB = 1
  !      L_SUBB(1) = 1
  do i=1, n_subb
    l_subb(i) = 1
  enddo
  !
  ! One band only
  n_band = 1
  i_band(1) = min(i_band(1),2)
  isb = i_band(k_band(1))
  !      IS = I_SUBB(1,K_SUBB(1))-10
  !      TELESCOPE = 'PdB-'//R_PROJECT//'-'//CSUB(IS+10)
  change_display = .true.
  !
  call set_display(error)
  if (error) return
  !      CALL SHOW_DISPLAY('X   ',.FALSE.,ERROR)
  !      CALL SHOW_DISPLAY('Y   ',.FALSE.,ERROR)
  !      CALL SHOW_DISPLAY('SUBB',.FALSE.,ERROR)
  !      CALL SHOW_DISPLAY('BAND',.FALSE.,ERROR)
  !
  if (change_display) then
    call read_spec('ALL',.true.,error) ! all scans in index
    plotted = .false.
    if (error) return
    change_display = .false.
  else
    plotted = .true.
  endif
  !      PLOT = SIC_PRESENT(1,0)
  !
  if (plot) then
    clear = .true.
    if (.not. plotted) call sub_plot('A',.false.,.false.,0,error)
    if (error) return
    plotted = .true.
  endif
  !
  ! Get auxiliary storage
  nd = n_data(1)
  do i=2, n_boxes
     nd = max(nd,n_data(i))
  enddo
  if (nd*3*n_subb.gt.m_p) then
    if (m_p .gt.0) then
      call free_vm(m_p,data_p)
      m_p = 0
    endif
    error = (sic_getvm(nd*3*n_subb,data_p).ne.1)
    if (error) return
    m_p = nd*3*n_subb
  endif
  ipx = gag_pointer(data_x,memory)
  ipy = gag_pointer(data_y,memory)
  ipw = gag_pointer(data_w,memory)
  ipp = gag_pointer(data_p,memory)
  !
  ! Average data from all baselines
  call sum_base(m_data, m_boxes, memory(ipx),  memory(ipy),   &
    memory(ipw), nd, n_subb, memory(ipp), weight)
  !
  ! Modify integration time to be written in fits file
  ! (normalize to the number of baselines)
  r_time = r_time*r_nbas
  !
  ! Write ouput fits file (one file per subbands group)
  ! Compute r_tsys from r_time and weight
  do isub = 1, n_subb
    if (i_subb(1,isub).gt.mbands) then
       is = i_subb(1,isub)-mbands
    else
       call message(8,4,'FITS','Command valid only for line subband')
       error = .true.
       return
    endif
    telescope = 'PdB-'//r_project//'-'//csub(is+mbands)
    r_tsys = sqrt(abs(r_lfres(is))*r_time/weight)
    r_teles = telescope
    name=argum(1:lenc(argum))//'-'//csub(is+mbands)
    call sic_parsef(name,fich,' ','.fits')
    call gfits_open(fich,'OUT',error)
    if (error) return
    kpp = ipp+(isub-1)*nd
    call clic_tofits(nd, memory(kpp), is, isb, .true. ,error)
  enddo
  return
end subroutine clic_fits
!
subroutine sum_base(md, mb , x_data, y_data, w_data,   &
    nd, ns, p_data, weight)
  use gildas_def
  use classic_api
  !---------------------------------------------------------------------
  ! Average real and imaginary data in all boxes (baselines)
  !---------------------------------------------------------------------
  integer :: md                     !
  integer :: mb                     !
  real :: x_data(md,mb)             !
  real :: y_data(md,mb)             !
  real :: w_data(md,mb)             !
  integer :: nd                     !
  integer :: ns                     !
  real :: p_data(nd,ns, 3)          !
  real :: weight                    !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_xy_code.inc'
  ! Local
  integer :: iba, iy, ichan
  integer :: ib, i, j
  !------------------------------------------------------------------
  do i=1, nd
    do j=1, ns
      p_data(i,j,1) = 0
      p_data(i,j,2) = 0
      p_data(i,j,3) = 0
    enddo
  enddo
  ! Loop on boxes
  do ib=1, n_boxes
    iba = i_base(k_base(ib))
    iy = i_y(k_y(ib))
    j = k_subb(ib)
    do i=1, n_data(ib)
      ichan = nint(x_data(i,ib))
      if (iy.eq.xy_real .or. iy.eq.xy_auto) then
        p_data(ichan,j,1) = p_data(ichan,j,1)   &
          +y_data(i,ib)*w_data(i,ib)
        p_data(ichan,j,3) = p_data(ichan,j,3)+w_data(i,ib)
      elseif (iy.eq.xy_imag) then
        p_data(ichan,j,2) = p_data(ichan,j,2)+   &
          y_data(i,ib)*w_data(i,ib)
      endif
    enddo
  enddo
  weight = 0
  do i=1, nd
    do j=1, ns
      if (p_data(i,j,3).ne.0) then
        p_data(i,j,1) = p_data(i,j,1)/p_data(i,j,3)
        p_data(i,j,2) = p_data(i,j,2)/p_data(i,j,3)
        weight = max(weight,p_data(i,j,3))
      endif
    enddo
  enddo
  return
end subroutine sum_base
!
subroutine clic_tofits(nd,ydata,is,isb, check,error)
  use gkernel_interfaces
  use gbl_constant
  use gildas_def
  use classic_api
  !---------------------------------------------------------------------
  ! FITS	Internal routine.
  !	Write current spectrum on tape.
  ! Arguments :
  !	ERROR		L	Logical error flag	Output
  !
  ! Current limitations:
  !	- Horizontal offsets incorrectly handled.
  !---------------------------------------------------------------------
  integer :: nd                     !
  real :: ydata(nd)                 !
  integer :: is                     !
  integer :: isb                    !
  logical :: check                  !
  logical :: error                  !
  ! Global
  include 'gbl_pi.inc'
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_fits.inc'
  include 'clic_display.inc'
  include 'clic_constant.inc'
  ! Local
  integer(kind=1) :: buffer(2880)
  integer(kind=size_length) :: idata, kdata
  character(len=80) :: line
  character(len=20) :: chain
  real :: rmin,rmax
  real(8) :: hobs
  integer :: idate
  integer :: i, id, iy, im, ni2, ni4, l
  real(8) :: ra,dec,off1,off2,frq,frqres,rchan, fimage
  character(len=4) :: pcode(0:7)
  !
  ! Data
  ! Table de correspondance a terminer...
  data pcode /'    ','-TAN','-SIN','-ARC','-STG',   &
    'lamb','-AIT','-GLS'/
  !
  !		     123456789012345678901234567890
  call gfits_put('SIMPLE  =                    T         /',   &
    check,error)
  write (line,10) 'BITPIX  =           ',snbit
  call gfits_put(line,check,error)
  if (error) return
  !
  ! Defines 4 different axis
  !
  call gfits_put('NAXIS   =                    4         /',   &
    check,error)
  ! Data type dependent information
  ndata  = r_lnch(is)
  write (line,10) 'NAXIS1  =           ',r_lnch(is)
  call gfits_put(line,check,error)
  call gfits_put('NAXIS2  =                    1         /',   &
    check,error)
  call gfits_put('NAXIS3  =                    1         /',   &
    check,error)
  call gfits_put('NAXIS4  =                    1         /',   &
    check,error)
  if (error) return
  !
  ! Compute extrema
  i = ndata
  call minmax (rmin,rmax,ydata,i,blank4)
  if (snbit.eq.16) then
    bscal = (rmax - rmin) / 65534
    bzero  = rmin + bscal * 32768
    write (line,10) 'BLANK   =           ',32767,'Blanking value'
    call gfits_put(line,check,error)
  elseif (snbit.eq.32) then
    bscal = (rmax - rmin) / 4294967294.
    bscal = bscal * (1-epsr4)  ! Allow some room for roundoff
    bzero  = rmin + bscal * 2147483648.
    write (line,10) 'BLANK   =           ',2147483647,   &
      'Blanking value'
    call gfits_put(line,check,error)
  else
    write (6,'(1X,A,I6)')   &
      'F-FITS,  Invalid format. Internal logic error ',snbit
    error = .true.
    return
  endif
  write (line,20) 'BSCALE  = ',bscal
  call gfits_put(line,check,error)
  write (line,20) 'BZERO   = ',bzero
  call gfits_put(line,check,error)
  write (line,20) 'DATAMIN = ',rmin
  call gfits_put(line,check,error)
  write (line,20) 'DATAMAX = ',rmax
  call gfits_put(line,check,error)
  call gfits_put('BUNIT   = ''K       ''                   /',   &
    check,error)
  if (error) return
  !
  ! First axis: Frequency
  !
  call gfits_put('CTYPE1  = ''FREQ    ''                   /',   &
    check,error)
  write (line,20) 'CRVAL1  = ',0d0,'Offset frequency'
  call gfits_put(line,check,error)
  frqres = r_lrfres(isb,is)    ! RFRES
  rchan  = r_lrch(isb,is)
  write (line,20) 'CDELT1  = ',frqres*1.d6,'Frequency resolution'
  call gfits_put(line,check,error)
  write (line,20) 'CRPIX1  = ',rchan
  call gfits_put(line,check,error)
  !
  off1 =  r_lamof
  off2 =  r_betof
  if (error) return
  !
  ! Axis 2 : Right Ascension or Lii
  !
  if (r_typec.eq.type_ga) then
    line = 'CTYPE2  = ''GLON'//pcode(r_proj)//   &
      '''                   /'
    call gfits_put(line,check,error)
  elseif (r_typec.eq.type_eq) then
    line = 'CTYPE2  = ''RA--'//pcode(r_proj)//   &
      '''                   /'
    call gfits_put(line,check,error)
    write (line,20) 'EQUINOX = ',r_epoch
    call gfits_put(line,check,error)
  else
    call gfits_put('CTYPE2  = ''ANGLE   ''                   /',   &
      check,error)
  endif
  if (error) return
  !
  ! For projected data, the reference point has to be the projection centre.
  write (line,20) 'CRVAL2  = ',r_lam*180.d0/pi
  call gfits_put(line,check,error)
  write (line,20) 'CDELT2  = ',dble(off1)*180.d0/pi
  call gfits_put(line,check,error)
  write (line,20) 'CRPIX2  = ',0.0
  call gfits_put(line,check,error)
  if (error) return
  !
  ! Axis 3 : Declination, or galactic latitude
  !
  if (r_typec.eq.type_ga) then
    line = 'CTYPE3  = ''GLAT'//pcode(r_proj)//   &
      '''                   /'
    call gfits_put(line,check,error)
  elseif (r_typec.eq.type_eq) then
    line = 'CTYPE3  = ''DEC-'//pcode(r_proj)//   &
      '''                   /'
    call gfits_put(line,check,error)
  else
    call gfits_put('CTYPE3  = ''ANGLE   ''                   /',   &
      check,error)
  endif
  write (line,20) 'CRVAL3  = ',r_bet*180.d0/pi
  call gfits_put(line,check,error)
  write (line,20) 'CDELT3  = ',dble(off2)*180.d0/pi
  call gfits_put(line,check,error)
  write (line,20) 'CRPIX3  = ',0.0
  call gfits_put(line,check,error)
  if (error) return
  !
  ! Axis 4 : Polarisation
  !
  call gfits_put('CTYPE4  = ''STOKES  ''                   /',   &
    check,error)
  call gfits_put('CRVAL4  =      1.0000000000000         /',   &
    check,error)
  call gfits_put('CDELT4  =      0.0000000000000         /',   &
    check,error)
  call gfits_put('CRPIX4  =      0.0000000000000         /',   &
    check,error)
  if (error) return
  !
  ! Miscellaneous
  write (line,30) 'TELESCOP= ',r_teles
  call gfits_put(line,check,error)
  write (line,30) 'OBJECT  = ',r_sourc
  call gfits_put(line,check,error)
  if (r_typec.eq.type_ga) then
    !
    ! Change to 2000.0 one day? Do the same for Equatorial coordinates?
    !
    !         CALL GAL_TO_EQU(R_LAM,R_BET,0.0,0.0,RA,DEC,XXX,YYY,1950.0,ERROR)
    !
    call gal_to_eq1950(r_lam,r_bet,ra,dec,1)
    write (line,20) 'RA      = ',180.d0*ra/pi,'Right Ascension'
    call gfits_put(line,check,error)
    write (line,20) 'DEC     = ',180.d0*dec/pi,'Declination'
    call gfits_put(line,check,error)
    write (line,20) 'EPOCH   = ',1950.0    ! Old syntax.
    call gfits_put(line,check,error)
    write (line,20) 'EQUINOX = ',1950.0
    call gfits_put(line,check,error)
  endif
  if (error) return
  !
  ! Spectral line information
  !
  write (line,30) 'LINE    = ',r_lnam(isb,is),'Line name'
  call gfits_put(line,check,error)
  frq = r_lrfoff(isb,is)
  write (line,20) 'RESTFREQ= ',frq*1d6,'Rest frequency'
  call gfits_put(line,check,error)
  ! Check here for velocity referential. Add something for definition
  ! (radio or optical): VHEL_RAD, or VLSR_OPT.
  if (r_typev.eq.v_lsr) then
    write (line,20) 'VELO-LSR= ',r_lvoff(isb,is)*1d3,   &
      'Velocity of reference channel'
  elseif (r_typev.eq.v_helio) then
    write (line,20) 'VELO-HEL= ',r_lvoff(isb,is)*1d3,   &
      'Velocity of reference channel'
  !
  ! Note in CLIC for an unknown reason VEL_OBS(3) means relative to EARTH
  elseif (r_typev.eq.v_earth) then
    write (line,20) 'VELO-EAR= ',r_lvoff(isb,is)*1d3,   &
      'Velocity of reference channel'
  else
    write (line,20) 'VELO    = ',r_lvoff(isb,is)*1d3,   &
      'Velocity of reference channel'
  endif
  call gfits_put(line,check,error)
  write (line,20) 'DELTAV  = ',r_lvres(isb,is)*1d3,   &
    'Velocity spacing of channels'
  call gfits_put(line,check,error)
  fimage = r_lrfoff(isb,is)   &
    !
    !error!     $- 2*(1+R_DOPPL)*R_ISB
    ! other error     $+(2*ISB-3)*2/(1+R_DOPPL)*R_ISB
    +(2*isb-3)*2/(1+r_doppl)   &
    *(r_flo1_ref-r_lfcen(is)   &
    -(r_lrch(isb,is)-r_lcench(is))*r_lfres(is))
  write (line,20) 'IMAGFREQ= ',fimage*1d6,   &
    'Image frequency'
  call gfits_put(line,check,error)
  if (error) return
  !
  ! Integration observation
  write (line,20) 'TSYS    = ',r_tsys,'System temperature'
  call gfits_put(line,check,error)
  write (line,20) 'OBSTIME = ',r_time,'Integration time'
  call gfits_put(line,check,error)
  write (line,20) 'SCAN-NUM= ',float(r_scan),'Scan number'
  call gfits_put(line,check,error)
  write (line,20) 'TAU-ATM = ',r_tau,'Atmospheric opacity'
  call gfits_put(line,check,error)
  if (error) return
  !
  ! Calibration parameters
  !      IF (PRESEC(CAL_SEC)) THEN
  !         WRITE (LINE,20) 'GAINIMAG= ',RGAINI,
  !     $   'Image sideband gain ratio'
  !         CALL GFITS_PUT(LINE,CHECK,error)
  !         WRITE (LINE,20) 'BEAMEFF = ',RBEEFF,'Beam efficiency'
  !         CALL GFITS_PUT(LINE,CHECK,error)
  !         WRITE (LINE,20) 'FORWEFF = ',RFOEFF,
  !     $   'Image sideband gain ratio'
  !         CALL GFITS_PUT(LINE,CHECK,error)
  !      ENDIF
  !
  ! Miscellaneous
  call gfits_put('ORIGIN  = ''CLIC-Grenoble  ''           /',   &
    check,error)
  call sic_date(line)
  call cdate(line,idate,error)
  call jdat(idate,id,im,iy)
  if (iy.lt.1999) then
    iy = mod(iy,100)
    write (line,12) 'DATE    = ''',id,im,iy,'Date written'
  else
    write (line,13) 'DATE    = ''',iy,im,id,   &
      '00:00:00.000','Date written'
  endif
  call gfits_put(line,check,error)
  if (error) return
  do while (r_ut.lt.0)
    r_ut   =  r_ut+2*pi
    r_dobs = r_dobs-1
  enddo
  do while (r_st.lt.0)
    r_st = r_st+2*pi
  enddo
  ! Write new ISO DATE FORMAT
  call jdat(r_dobs,id,im,iy)
  call sexag(chain,r_ut,24)
  call sic_black(chain,l)
  call gfits_put('TIMESYS = ''UTC             ''           /',   &
    check,error)
  if (error) return
  ! RL: FITS recommends to use the old date format until Jan 1st, 1999.
  ! this is because data may be propagated faster than software.
  if (iy.lt.1999) then
    iy = mod(iy,100)
    write (line,12) 'DATE-OBS= ''',id,im,iy,'Date observed'
  else
    write (line,13) 'DATE-OBS= ''',iy,im,id,   &
      chain(1:12),'Date observed'
  endif
  call gfits_put(line,check,error)
  if (error) return
  !
  call jdat(r_dred,id,im,iy)
  hobs=0d0
  call sexag(chain,hobs,24)
  call sic_black(chain,l)
  if (iy.lt.1999) then
    iy = mod(iy,100)
    write (line,12) 'DATE-RED= ''',id,im,iy,'Date reduced'
  else
    write (line,13) 'DATE-RED= ''',iy,im,id,   &
      '00:00:00.000','Date reduced'
  endif
  call gfits_put(line,check,error)
  if (error) return
  !
  write (line,20) 'ELEVATIO= ',180.d0*r_el/pi,' Telescope elevation'
  call gfits_put(line,check,error)
  write (line,20) 'AZIMUTH = ',180.d0*r_az/pi,' Telescope azimuth'
  call gfits_put(line,check,error)
  call sexag(chain,r_ut,24)
  write (line,31) 'UT      = ',chain, ' Universal time at start'
  call gfits_put(line,check,error)
  call sexag(chain,r_st,24)
  write (line,31) 'LST     = ',chain, ' Sideral time at start'
  call gfits_put(line,check,error)
  if (error) return
  ! History
  !      LINE = 'HISTORY SCAN-LIST '
  !      N = 20
  !      DO I =1,RNSEQ
  !         WRITE(LINE(N:),29) RSTART(I),REND(I)
  !         N = N+20
  !         IF (MOD(I,3).EQ.0) THEN
  !            CALL GFITS_PUT(LINE,CHECK,error)
  !!            N = 20
  !         ENDIF
  !      ENDDO
  !      IF (MOD(RNSEQ,3).NE.0) THEN
  !         CALL GFITS_PUT(LINE,CHECK,error)
  !      ENDIF
  !
  ! Finish Header
  call gfits_put('END                         ',check,error)
  call gfits_flush_header(error)
  if (error) return
  !
  ! Write data
  idata = 0
  kdata = ndata
  ni2 = 1440
  ni4 = 720
  !      DO WHILE (I.LT.ND)
  do while (idata.lt.kdata)
    buffer(:) = 0  ! Bytes which are left unused will be zero, as required by FITS
    if (snbit.eq.16) then
      call real_to_int2 (buffer,ni2,ydata,kdata,idata,   &
        bscal,bzero,bval,abs(bval)*epsr4)
    elseif (snbit.eq.32) then
      call real_to_int4 (buffer,ni4,ydata,kdata,idata,   &
        bscal,bzero,bval,abs(bval)*epsr4)
    else
      write (6,'(1X,A,I6)')   &
        'F-FITS,  Invalid format. Internal logic error',snbit
      error = .true.
      return
    endif
    call gfits_putbuf(buffer,2880,error)
  enddo
  call gfits_close(error)
  return
  !
10 format(a,i10,'         / ',a)
11 format(a,i10,';',i6)
12 format(a,i2,'/',i2,'/',i2,'''                   / ',a)
13 format(a,i4.4,'-',i2.2,'-',i2.2,'T',a12,'''    / ',a)
20 format(a,e20.13,'         / ',a)
21 format(a,1pg11.4,a)
29 format(i8,'-',i8,3x)
30 format(a,'''',a12,'''','               / ',a)
31 format(a,'''',a13,'''','              / ',a)
50 format(a,i1,a,e20.13,'         / ',a,i1)
end subroutine clic_tofits
!
subroutine minmax (rmin,rmax,a,na,rbad)
  real*4 :: rmin                    !
  real*4 :: rmax                    !
  real*4 :: a(*)                    !
  integer :: na                     !
  real*4 :: rbad                    !
  ! Local
  real*4 :: adiff
  integer :: j,jstart
  !
  if (na.lt.1) then
    rmin = 0.
    rmax = 1.
    return
  endif
  do 10 j = 1,na
    if(a(j).ne.rbad) then
      rmin = a(j)
      rmax = a(j)
      goto 15
    endif
10 continue
15 jstart = j+1
  do 20 j = jstart,na
    if (a(j).eq.rbad) goto 20
    rmin = amin1 (rmin,a(j))
    rmax = amax1 (rmax,a(j))
20 continue
  adiff = rmax - rmin
  if (adiff.eq.0.) adiff = max(abs(rmin),0.1)
  rmin = rmin - .05*adiff
  rmax = rmax + .05*adiff
  return
end subroutine minmax
