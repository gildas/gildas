subroutine write_sdm_focus(error)
  !---------------------------------------------------------------------
  !     Write the Focus SDM table.
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_focus
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type (FocusRow) :: fRow
  type (FocusOpt) :: fOpt
  type (FocusKey) :: fKey
  !$$$      integer sdm_addFocusRow, ireturn
  integer ia
  logical doIt
  character, parameter ::  sdmTable*8='Focus'
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting ...')
  call allocFocusRow(fRow, error)
  if (error) return
  call allocFocusOpt(fRow, fOpt, error)
  if (error) return
  !
  ! Use only the first point of not a focus measurement
  if (.not. r_presec(scanning_sec) .or. r_scaty.ne.1) then
    doIt = dh_dump.eq.1
  else
    doIt = .true.
  endif
  if (.not. doIt) return
  ! open-ended interval
  fKey%timeInterval = ArrayTimeInterval(time_Interval(1),-1)
  !!fKey%feedId = feed_Id
  ! now do it.
  !      call sdmMessage(1,1,sdmTable ,'starting...')
  do ia = 1, r_nant
    fRow%focusTracking = .true.
    ! x and y not tracked in Bure data...
!!!     fRow%xFocusPosition = 0.
!!!     fRow%yFocusPosition = 0.
!!!     fRow%zFocusPosition = r_corfoc(ia)/1000.
    fOpt%measuredFocusPosition = (/ 0., 0., r_corfoc(ia)/1000. /)
!!!     fRow%xFocusOffset = 0.
!!!     fRow%yFocusOffset = 0.
!!!     fRow%zFocusOffset = dh_offfoc(ia)/1000.
    fRow%FocusOffset = (/ 0., 0., dh_offfoc(ia)/1000. /)
    fKey%antennaId = antenna_Id(ia)
    ! not meaningful.
    fRow%focusModelId = focusmodel_id(ia)
    call addFocusRow(fKey, fRow, error)
    if (error) return
    call addFocusMeasuredFocusPosition(fKey, fOpt, error)
    if (error) return    
  !
  enddo
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
  !
99 error = .true.
end subroutine write_sdm_focus
!---------------------------------------------------------------------
subroutine get_sdm_focus(first,error)
  !---------------------------------------------------------------------
  !     Read the Focus SDM table.
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_focus
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error, first, present
  ! Local
  type (FocusRow) :: fRow
  type (FocusKey) :: fKey
  type (FocusOpt) :: fOpt
  !$$$      integer sdm_addFocusRow, ireturn
  integer ia
  logical doIt
  character, parameter ::  sdmTable*8='Focus'
  real :: saved_corfoc(mnant), saved_offfoc(mnant)
  save saved_corfoc, saved_offfoc
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting ...')
  !
  call allocFocusRow(fRow, error)
  if (error) return
  call allocFocusOpt(fRow, fOpt, error)
  if (error) return

  fKey%timeInterval = ArrayTimeInterval(time_Interval(1)   &
    ,time_Interval(2)-1)
  !!fKey%feedId = feed_Id
  ! now do it.

  do ia = 1, r_nant
    fKey%antennaId = antenna_Id(ia)
    call getFocusRow(fKey, fRow, error)
    if (error) then
      r_corfoc(ia)  = saved_corfoc(ia)
      dh_offfoc(ia) = saved_offfoc(ia) 
      
      error = .false.
      call sdmMessage(8,3,sdmTable, 'Missing Focus table row.')
    else
      ! x and y not tracked in Bure data...
      call getFocusMeasuredFocusPosition(fKey, fOpt, present, error)
      if (error) return
      if (present) then
        r_corfoc(ia)  = fOpt%MeasuredFocusPosition(3)*1000.d0
        dh_offfoc(ia) = fRow%FocusOffset(3)*1000.d0
      else
        r_corfoc(ia)  = 0
        dh_offfoc(ia) = 0 
      endif
      saved_corfoc(ia) = r_corfoc(ia)
      saved_offfoc(ia) = dh_offfoc(ia)
    endif
  !         print *, r_scan, ia, r_corfoc(ia) , dh_offfoc(ia)
  enddo
  call sdmMessage(1,1,sdmTable ,'Read.')
  return
  !
!99 error = .true.
end subroutine get_sdm_focus
!---------------------------------------------------------------------
