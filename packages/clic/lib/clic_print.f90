subroutine clic_print(line,error)
  use gkernel_interfaces
  use gildas_def
  use clic_file
  use clic_title
  use clic_index
  !---------------------------------------------------------------------
  ! Command PRINT [ BASELINE | DELAY | FLUX ]
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_par.inc'
  include 'clic_stations.inc'
  include 'clic_display.inc'
  ! Local
  character(len=11) :: ch1
  character(len=80) :: name,file
  character(len=12) :: arg,ateles
  character(len=1) :: arm(4)
  integer :: j, k, iarm, jarm, k_ant, lun, ier, iarm1
  integer :: iarm2, jarm1, jarm2, nch, idf, mvoc1, narg
  parameter (mvoc1=3)
  logical :: f_ant(mnant), more, antenna
  real*8 :: pos(3,mnant)
  character(len=12) :: voc1(mvoc1)
  !
  data arm/'N','W','E','E'/
  data voc1/'BASELINE','FLUX','DELAY'/
  !------------------------------------------------------------------------
  ! Code:
  arg = '*'
  call clic_kw(line,0,1,arg,nch,voc1,mvoc1,.true.,error,.true.)
  if (error) return
  !
  ! Baseline
  if (arg.eq.'BASELINE') then
    !
    ! output
    call datec(title%dobs,ch1,error)
    ! May be datec should produce lower case ?
    call sic_lower(ch1)
    ier = sic_getlun(lun)
    if (ier.ne.1) then
      call gagout('W-CLIC_PRINT,  Error getting logical unit')
      call show_luns
      error = .true.
      return
    endif
    if (sic_present(1,0)) then
       call sic_ch (line,1,1,file,narg,.true.,error)
       if (error) return
    else
      name = ch1(1:11)//'-base'
      file = name
      call sic_parsef (name,file,'INTER_OBS:','.obs')
    endif
    ier = sic_open (lun,file,'NEW',.false.)
    if (ier.ne.0) then
      call message(8,4, 'CLIC_PRINT',   &
        'Open error file '//file(1:lenc(file)))
      call messios(8,4, 'CLIC_INPUT',ier)
      goto 999
    endif
    rewind(lun)
    !
    call user_info(name)
    write (lun,1002) '! '//name(1:78)
    write (lun,1002) '! Results of baseline fit:'
    call rix(cx_ind(1),error)
    call bytoch(title%teles,ateles,12)
    write(lun,1003) ateles, title%scan, ch1(1:11)
    call rix(cx_ind(cxnext-1),error)
    call datec(title%dobs,ch1,error)
    write(lun,1004) title%scan, ch1(1:11)
    write(lun,1002) '!'
    !
    antenna = .false.
    do k=1, r_nant
      antenna = antenna .or. f_afit(k)
      f_ant(k) = .false.
    enddo
    if (.not.antenna) then
      do k=1, mnbas
        if (f_bfit(k)) then
          iarm1 = r_istat(r_iant(k))/100
          jarm1 = r_istat(r_iant(k))-iarm1*100+(r_istat(r_iant(k))/400)*100
          iarm2 = r_istat(r_jant(k))/100
          jarm2 = r_istat(r_jant(k))-iarm2*100+(r_istat(r_jant(k))/400)*100
          write(lun,1010) cbas(k),   &
            arm(iarm1), jarm1, arm(iarm2), jarm2, b_rms(k)
          write(lun,1011,err=999)   &
            (b_fit(j,k), b_err(j,k)/1000., j=1,3)
          write(lun,1002) '!'
        endif
      enddo
      k_ant = 0
      more = .true.
      do while (more)
        more = .false.
        do k=1, mnbas
          if (f_bfit(k)) then
            if (k_ant.eq.0) then
              do j=1,3
                pos(j,r_iant(k)) = 0
              enddo
              k_ant = k_ant+1
              f_ant(r_iant(k)) = .true.
              more = .true.
            endif
            if (f_ant(r_iant(k))   &
              .and. .not. f_ant(r_jant(k))) then
              do j=1,3
                pos(j,r_jant(k)) =   &
                  b_fit(j,k)+pos(j,r_iant(k))   &
                  - stat99(j,r_istat(r_jant(k)))   &
                  + stat99(j,r_istat(r_iant(k)))
              enddo
              k_ant = k_ant+1
              f_ant(r_jant(k)) = .true.
              more = .true.
            elseif (f_ant(r_jant(k))   &
              .and. .not.f_ant(r_iant(k))) then
              do j=1,3
                pos(j,r_iant(k)) =   &
                  - b_fit(j,k)+ pos(j,r_jant(k))   &
                  + stat99(j,r_istat(r_jant(k)))   &
                  - stat99(j,r_istat(r_iant(k)))
              enddo
              k_ant = k_ant+1
              f_ant(r_iant(k)) = .true.
              more = .true.
            endif
          endif
        enddo
      enddo
    elseif (antenna) then
      do k=1, r_nant
        if (f_afit(k)) then
          f_ant(k) = .true.
          do j = 1, 3
            pos(j,k) = a_fit(j,k)-stat99(j,r_istat(k))
          enddo
          iarm = r_istat(k)/100
          jarm = r_istat(k)-iarm*100+(r_istat(k)/400)*100
          write(lun,1012) k, arm(iarm), jarm, a_rms(k)
          write(lun,1011,err=999)   &
            (a_fit(j,k), a_err(j,k)/1000., j=1,3)
          write(lun,1002) '!'
        else
          do j = 1, 3
            pos(j,k) = 0
          enddo
        endif
      enddo
    endif
    !
    write (lun,1002) '!'
    write (lun,1002) '! OBS procedure for updating '   &
      //'antenna positions offsets'
    write (lun,1002) '!'
    do k=1, r_nant
      iarm = r_istat(k)/100
      jarm = r_istat(k)-iarm*100+(r_istat(k)/400)*100
      if (f_ant(k)) then
        write(lun,1000) char(92),   &
          (pos(j,k)-pos(j,1),j=1,3),r_kant(k),   &
          arm(iarm), jarm
      else
        write(lun,1001) r_kant(k),arm(iarm), jarm
      endif
    enddo
    !
    write(lun,1002) '!'
    write(lun,1002) '! CLIC command for updating antenna positions'
    write(lun,1002) '!'
    write(lun,1002) '! MODIFY ANTENNA -'
    do k=1,r_nant
      write(lun,1008) k,(pos(j,k)-pos(j,1),j=1,3)
    enddo
    write(lun,1002) '! /OFFSET 99'
    inquire (unit=lun, name=name)
    close (unit=lun)
    call sic_frelun (lun)
    call message(6,1,'CLIC_PRINT','Created '//name(1:lenc(name)))
  !
  ! Flux ------------------------------------------------------------------
  elseif (arg.eq.'FLUX') then
    if (n_flux.le.0) then
      call gagout('E-PRINT,  No list of fluxes')
    else
      ier = sic_getlun (lun)
      if (ier.ne.1) then
        call gagout('W-CLIC_PRINT,  Error getting logical unit')
        call show_luns
        error = .true.
        return
      endif
      name = 'clic-fluxes.clic'
      ier = sic_open(lun,name,'NEW',.false.)
      if (ier.ne.0) then
        call message(8,4, 'CLIC_PRINT',   &
          'Open error file CLIC-FLUXES.CLIC')
        call messios(8,4, 'CLIC_PRINT',ier)
        goto 999
      endif
      !
      call user_info(name)
      write (lun,1002) '! '//name(1:78)
      write (lun,1002)   &
        '! CLIC procedure for updating source fluxes'
      call rix(cx_ind(1),error)
      call datec(title%dobs,ch1,error)
      call bytoch(title%teles,ateles,12)
      write(lun,1003) ateles, title%scan, ch1(1:11)
      call rix(cx_ind(cxnext-1),error)
      call datec(title%dobs,ch1,error)
      write(lun,1004) title%scan, ch1(1:11)
      do k=1, n_flux
        idf = date_flux(k)
        if (idf.le.99999) then
          call datec(idf,ch1,error)
        else
          ch1 = ' '
        endif
        if (f_flux(k)) then
          write(lun,1005) ' SET FLUX', c_flux(k),   &
            flux(k), '! Jy FIXED '
        else
          write(lun,1005) '!SET FLUX',c_flux(k),   &
            flux(k), '! Jy FREE  '//ch1(1:11)//' ',   &
            freq_flux(k)/1d3,' GHz'
        endif
      enddo
      inquire (unit=lun, name=name)
      close (unit=lun)
      call sic_frelun (lun)
      call message(6,1,'CLIC_PRINT','Created '   &
        //name(1:lenc(name)))
    endif
  !
  ! DELAY --------------------------------------------------------
  !
  elseif (arg.eq.'DELAY') then
    call print_delay (.true.,error)
  endif
  return
  !
  ! Error case
999 error = .true.
  call sic_frelun(lun)
  return
  !
1000 format('SET',a,'XYZ ',3(f10.6,1x),'/ANTENNA ',i0,   &
    '  ! on ',a1,i3.3)
1001 format('! (no data for antenna ',i0,' on ',a1,i3.3,')')
1002 format(a)
1003 format('! ',a,' from scan ',i5,' (',a,')')
1004 format('! ',12x,'   to scan ',i5,' (',a,')')
1005 format(a9,2x,a12,2x,f10.4,2x,a,f5.1,a)
1008 format('! ',i2,3(1x,f11.6),' -')
1010 format('! Baseline ',a5,' ',a1,i3.3,'-',a1,i3.3,   &
    '   rms = ',1pg11.4)
1012 format('! Antenna  ',i3,' on ',a1,i3.3,'   rms = ',1pg11.4)
1011 format('! X= ',f11.6,' +/- ',f8.6,/   &
    '! Y= ',f11.6,' +/- ',f8.6,/   &
    '! Z= ',f11.6,' +/- ',f8.6)
end subroutine clic_print
!
subroutine show_luns
  use gkernel_interfaces
  ! Local
  integer :: i,ier
  character(len=80) :: name
  character(len=1) :: pol(2)
  data pol/'H','V'/
  !------------------------------------------------------------------------
  ! Code:
  do i=100,119
    inquire (unit=i, name=name, iostat=ier)
    if (ier.ne.0) then
      write(6,*) 'Unit ',i,':'
      call messios(8,4, 'clic_print',ier)
    else
      write(6,*) 'Unit ',i,'   File ',name(1:lenc(name))
    endif
  enddo
  return
end subroutine show_luns
!
subroutine print_delay (date,error)
  use gildas_def
  use gkernel_interfaces
  use clic_file,except=>i
  use clic_title
  use clic_index
  logical :: date                   !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_stations.inc'
  ! Local
  character(len=11) :: ch1
  character(len=1) :: arm(4),pol(2)
  character(len=200) :: name,file,ch
  integer :: ier,lun,i,j,k,l,iarm,jarm,iif,ibb,ndelay(mnbb),jbb
  character(len=12) :: ateles
  real :: ref_delay(mnbb)
  !
  data arm/'N','W','E','E'/
  data pol/'H','V'/
  !
  ier = sic_getlun(lun)
  if (ier.ne.1) then
    call gagout('W-CLIC_PRINT,  Error getting logical unit')
    call show_luns
    error = .true.
    return
  endif
  if (date) then
    call datec(title%dobs,ch1,error)
    name = ch1(1:11)//'-delay'
  else
    name = 'delay'
    ch1 = ' '
  endif
  file = name
  call sic_parsef (name,file,'INTER_OBS:','.obs')
  ier = sic_open (lun,file,'NEW',.false.)
  if (ier.ne.0) then
    call message(8,4, 'CLIC_PRINT',   &
      'Open error file '//file(1:lenc(file)))
    call messios(8,4, 'CLIC_INPUT',ier)
    call sic_frelun (lun)
    error = .true.
    return
  endif
  rewind(lun)
  !
  call check_scan(lun,r_scan,'Delay')
  call user_info(name)
  write (lun,1002) '! '//name(1:78)
  write (lun,1002) '! Results of Delay fit:'
  call rix(cx_ind(1),error)
  call bytoch(title%teles,ateles,12)
  write(lun,1003) ateles, title%scan, ch1(1:11)
  call rix(cx_ind(cxnext-1),error)
  call datec(title%dobs,ch1,error)
  write(lun,1004) title%scan, ch1(1:11)
  write(lun,1002) '!'
  !
  ref_delay = 0
  ndelay = 0
  do k=1, r_nant
    do ibb=1,mnbb
      if (f_bbdelayfit(k,ibb)) then
        iarm = r_istat(k)/100
        jarm = r_istat(k)-iarm*100+(r_istat(k)/400)*100
        write(lun,1013) k, r_kant(k), ibb,arm(iarm), jarm,   &
          bbdelayoff(r_kant(k),ibb),bbdelayrms(k,ibb)
1013    format('! Ant. ',i0,' (a',i0,') bb.',i0,' on ',a1,i3.3,' delay = ',   &
          1pg13.6, '   rms = ',1pg13.6)
        write(lun,1002) '!'
      endif
    enddo
    do iif=1,mnif
      if (f_ifdelayfit(k,iif)) then
        iarm = r_istat(k)/100
        jarm = r_istat(k)-iarm*100+(r_istat(k)/400)*100
        write(lun,1014) k, r_kant(k), iif,arm(iarm), jarm,   &
          ifdelayoff(r_kant(k),iif),ifdelayrms(k,iif)
1014    format('! Ant. ',i0,' (a',i0,') if.',i0,' on ',a1,i3.3,' delay = ',   &
          1pg13.6, '   rms = ',1pg13.6)
        write(lun,1002) '!'
      endif
    enddo
  enddo
  !
  write (lun,1002) '!'
  write (lun,1002) '! OBS procedure for updating delays'
  write (lun,1002) '!'
  do i=1, r_nant
    k = r_kant(i)
    do ibb=1,mnbb
      if (f_bbdelayoff(k,ibb)) then
        iarm = r_istat(i)/100
        jarm = r_istat(i)-iarm*100+(r_istat(k)/400)*100
        if (f_crossed) then
          write(lun,1009) char(92), bbdelayoff(k,ibb), k, r_nrec,   &
            r_bbname(ibb),arm(iarm), jarm
        elseif (.not.f_reference) then
          if (new_receivers) then
            write(lun,1007) char(92), bbdelayoff(k,ibb), k, r_nrec,   &
              r_bbname(ibb),arm(iarm), jarm
          else
            write(lun,1006) char(92), bbdelayoff(k,1), k, r_nrec,   & !! CHECK
              arm(iarm), jarm
          endif
        elseif (.not.f_delaybase) then          
          ref_delay(ibb) = ref_delay(ibb)+bbdelayoff(k,ibb)
          ndelay(ibb) = ndelay(ibb)+1
        else
          jbb = ibb
        endif
      endif
1006  format('SET',a,'DELAY ',f10.4,' /ANTENNA ',i0,' /RECEIVER ',   &
        i1,' ! on ',a1,i2.2)
1007  format('SET',a,'DELAY ',f10.4,' /ANTENNA ',i0,' /RECEIVER ',   &
        i1,' /BASEBAND ',a,' ! on ',a1,i2.2)
1009  format('SET',a,'DELAY ',f10.4,' /ANTENNA ',i0,' /RECEIVER ',   &
        i1,' /BASEBAND ',a,'/CROSSED   ! on ',a1,i2.2)
1010  format('SET',a,'DELAY ',f10.4,' /RECEIVER ',   &
        i1,' /BASEBAND ',a,'/REFERENCE ')
    enddo
    do iif=1,mnif
      if (f_ifdelayoff(k,iif)) then
        iarm = r_istat(i)/100
        jarm = r_istat(i)-iarm*100+(r_istat(k)/400)*100
        if (new_receivers) then
          write(lun,1008) char(92), ifdelayoff(k,iif), k, r_nrec,   &
            r_ifname(iif),arm(iarm), jarm
        else
          write(lun,1006) char(92), ifdelayoff(k,1), k, r_nrec,   & !! CHECK
            arm(iarm), jarm
        endif
      endif
1008  format('SET',a,'DELAY ',f10.4,' /ANTENNA ',i0,' /RECEIVER ',   &
        i1,' /IF ',a,' ! on ',a1,i2.2)
    enddo

  enddo
  if (f_reference.and.f_delaybase) then
    do i=1, r_nbas
      ref_delay(jbb) = ref_delay(jbb)+bbdelayfitbase(i,jbb)
      ndelay(jbb) = ndelay(jbb)+1
    enddo
  endif
  do ibb=1,mnbb
    if (ndelay(ibb).gt.0) then
      ref_delay(ibb) = ref_delay(ibb)/ndelay(ibb)
      write(lun,1010) char(92), ref_delay(ibb), r_nrec,   &
          r_bbname(ibb)
    endif
  enddo
  !
  write(lun,1002) '!'
  write(lun,1002) '! CLIC command for updating delays'
  write(lun,1002) '!'
  do ibb=1, mnbb
    ch = '! MODIFY DELAY '
    l = lenc(ch)
    do k=1,r_nant
      if (.not.f_reference) then
        if (k.gt.1 .and. f_bbdelayfit(k,ibb)) then
          write(ch(l+1:),'(1x,i2,1x,f10.4)') k,bbdelayoff(r_kant(k),ibb)
          l = lenc(ch)
        endif
      else
        if (ndelay(ibb).gt.0) then
          write(ch(l+1:),'(1x,i2,1x,f10.4)') k,ref_delay(ibb)
          l = lenc(ch)
        endif 
      endif
    enddo
    ch(l+1:) = ' /BBAND '//char(ibb+ichar('0'))
    l = lenc(ch)
    ch(l+1:) = ' /OFF'
    l = lenc(ch)
    if (f_crossed.or.f_reference) then
      ch(l+1:) = ' /SWITCH CROSSED'
      l = lenc(ch)
    else
      ! Generic case is direct
      ch(l+1:) = ' /SWITCH DIRECT'
      l = lenc(ch)
    endif
    if (l.gt.44)  write(lun,1002) ch(1:l)
  enddo
  do iif=1, mnif
    ch = '! MODIFY DELAY '
    l = lenc(ch)
    do k=1,r_nant
      if (k.gt.1 .and. f_ifdelayfit(k,iif)) then
        write(ch(l+1:),'(1x,i2,1x,f10.4)') k,ifdelayoff(r_kant(k),iif)
        l = lenc(ch)
      endif
    enddo      
    ch(l+1:) = ' /IF '//char(iif+ichar('0'))
    l = lenc(ch)
    ch(l+1:) = ' /OFF'
    l = lenc(ch)
    if (l.gt.25)  write(lun,1002) ch(1:l)
  enddo
  inquire (unit=lun, name=name)
  close (unit=lun)
  call sic_frelun (lun)
  call message(6,1,'CLIC_PRINT','Created '//name(1:lenc(name)))
  return
  !
1001 format('! (no data for antenna ',i0,' on ',a1,i2.2,')')
1002 format(a)
1003 format('! ',a,' from scan ',i5,' (',a,')')
1004 format('! ',12x,'   to scan ',i5,' (',a,')')
1005 format(a9,2x,a12,2x,f10.4,2x,a,f5.1,a)
end subroutine print_delay
