subroutine write_sdm_antenna(error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Write the Antenna SDM table.
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_antenna
  use sdm_enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'clic_stations.inc'
  include 'gbl_memory.inc'
  include 'gbl_pi.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type(AntennaRow) :: aRow
  type(AntennaKey) :: aKey
  integer :: ia, i, k, is
  character*12, parameter ::  sdmTable='Antenna'
  real*8 :: lonlat(2), altitude, s_5(2), x_3(3), x_2(3), s_2(2),   &
    trfm_23(3,3), psi, the, phi
  real*8, parameter :: radius=6367.435d3   ! the Earth radius
  logical :: first
  integer*8 antennaTime
  save first, antennaTime
  data first /.true./
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  ! 
  call allocAntennaRow(arow, error)
  if (error) return
  !     Make sure we set the time only once.
  ! in principle only once a scan would be allowed.
  if (first) then
    antennaTime = time_interval(1)
    first = .false.
  endif
  !
  aRow%time = antennaTime
  !
  ! TODO: Check the type and name of antennas used by Control
  if (r_teles.eq.'VTX-ALMATI') then
    aRow%antennatype = AntennaType_GROUND_BASED    !!'ALMA/Vertex Prototype'
    aRow%antennamake = AntennaMake_VERTEX_12_ATF   !!'ALMA/Vertex Prototype'
    aRow%dishDiameter = 12.0
    CALL SIC_SEXA ('-107:37:10.02',10,LONLAT(1),ERROR)
    CALL SIC_SEXA ('34:04:29.8',8,LONLAT(2),ERROR)
    ALTITUDE = 2.13542*1000.
    r_ant(1,1) = 0.
    r_ant(2,1) = 0.
    r_ant(3,1) = 0.
  elseif (r_teles.eq.'AEC-ALMATI') then
    aRow%antennatype = AntennaType_GROUND_BASED    !!'ALMA/AEC Prototype'
    aRow%antennamake = AntennaMake_AEM_12_ATF  !!'ALMA/Vertex Prototype'
    aRow%dishDiameter = 12.0
    CALL SIC_SEXA ('-107:37:10.02',10,LONLAT(1),ERROR)
    CALL SIC_SEXA ('34:04:29.8',8,LONLAT(2),ERROR)
    ALTITUDE = 2.13542*1000.
    r_ant(1,1) = -35.          ! displaced towards East by 35. meters.
    r_ant(2,1) = 0.
    r_ant(3,1) = 0.
  elseif (index(r_teles,'OSF').gt.0) then
    aRow%antennatype = AntennaType_GROUND_BASED    !!'ALMA/AEC Prototype'
    aRow%antennamake = AntennaMake_VERTEX_12       !!'ALMA/Vertex Production'
    aRow%dishDiameter = 12.0

  else
    aRow%antennatype = AntennaType_GROUND_BASED    !!'IRAM/Bure 15m'
    aRow%antennamake = AntennaMake_IRAM_15
    aRow%dishDiameter = 15.0
    CALL SIC_SEXA ('05:54:28.5',10,LONLAT(1),ERROR)
    CALL SIC_SEXA ('44:38:02.0',10,LONLAT(2),ERROR)
    ALTITUDE = 2.560*1000.
  endif
  lonlat = lonlat*pi/180d0
  do ia = 1, r_nant
    if (r_teles.eq.'VTX-ALMATI') then
      aRow%name = 'ALMA01'
    elseif (r_teles.eq.'AEC-ALMATI') then
      aRow%name = 'ALMA02'
    elseif (index(r_teles,'OSF').gt.0) then
      aRow%name = r_antennaName(ia)
      aRow%position = 0
      aRow%offset = 0
    else
      write (aRow%name,'(a4,i2.2,a1)') 'BURE', r_kant(ia), char(0)
      !     These coordinates are in the Bure system, rotate them to horizontal.
      !
      !     First subtract the station coordinates
      is = r_istat(ia)
      !
      ! Euler angles from Hour-angle to Horizontal:
      psi = pi/2               ! 90 degrees
      the = pi/2-lonlat(2)     ! 90 degrees minus latitude
      phi = pi/2               ! 90 degrees
      call eulmat (psi, the, phi,trfm_23)
      do k=1, 3
        x_3(k) = r_ant(k,ia)  - stat99(k,is)
      enddo
      call matvec (x_3, trfm_23, x_2)
      !c            print *, is, x_2
      aRow%position(1) = x_2(1)
      aRow%position(2) = x_2(2)
      aRow%position(3) = x_2(3)
      !!aRow%yposition = x_2(2)
      !!aRow%zposition = x_2(3)
    endif
    !!  aRow%flagRow = dh_aflag(ia).ne.0
    !     **TBD** not sure : check if axes offset is in x, y or z...
    !   aRow%xoffset = r_axes(ia)
    !   aRow%yoffset = 0
    !   aRow%zoffset = 0
    aRow%offset(1) = r_axes(ia)
    aRow%offset(2) = 0d0
    aRow%offset(3) = 0d0
    aRow%stationId = Station_Id(ia)
    call addAntennaRow(aKey, aRow, error)
    if (error) return
    antenna_id(ia) = aKey%antennaId
  enddo
  call sdmMessage(1,1,sdmTable ,'Written.')
  return
  !
99 error = .true.
end subroutine write_sdm_antenna
!---------------------------------------------------------------------
subroutine get_sdm_antenna(error)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! Write the Antenna SDM table.
  !
  !---------------------------------------------------------------------
  use gildas_def
  use sdm_antenna
  use sdm_enumerations
  ! Global variables:
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_constant.inc'
  include 'gbl_memory.inc'
  include 'gbl_pi.inc'
  include 'sdm_identifiers.inc'
  ! Dummy
  logical error
  ! Local
  type(AntennaRow) :: aRow
  type(AntennaKey) :: aKey
  integer :: ia, ja, ib, is, k, ic, isb, ilc, l1, l2
  character*1 :: arm(3)
  character*12, parameter ::  sdmTable='Antenna'
  real*8 :: lonlat(2), altitude, xObs, yObs, zObs, xAnt, yAnt, zAnt    ! SI units...
  real*8, parameter :: radius=6367.435d3   ! the Earth radius
  DATA ARM/'N','W','E'/
  !---------------------------------------------------------------------
  call sdmMessage(1,1,sdmTable ,'starting...')
  call allocAntennaRow(arow, error)
 
! first pass to get the station coordinates in r_antgeo ...
  do ia = 1, r_nant
    aKey%antennaId = Antenna_id(ia)
    call getAntennaRow(aKey, aRow, error)
    if (error) return
    Station_Id(ia) = aRow%stationId
  enddo
  !     station table try reading this before 
  !     as in Bure data the station coords are in the antenna coords.
  call get_sdm_station(error)
  if (error) goto 99
  !
  do ia = 1, r_nant
    aKey%antennaId = Antenna_id(ia)
    call getAntennaRow(aKey, aRow, error)
    if (error) return
    !Station_Id(ia) = aRow%stationId
    ! HOLO PATCH
    ! This will become aRow%make when available...
    !cc now only ALMA03 is Vertex!
    if (aRow%antennatype.eq.AntennaType_GROUND_BASED) then
      !c            if ((aRow%name.eq.'ALMA01').or.(aRow%name.eq.'ALMA03')) then
      if ((aRow%name.eq.'ALMA03')) then
        aRow%antennamake = AntennaMake_VERTEX_12_ATF   !!'ALMA/Vertex Prototype'
        !               aRow%type = 'ALMA/Vertex Prototype'
        aRow%dishDiameter = 12.
      elseif ((aRow%name.eq.'ALMA01').or.(aRow%name.eq.'ALMA02')   &
        .or.(aRow%name.eq.'ALMA04')) then
        aRow%antennamake = AntennaMake_AEM_12_ATF  !!'ALMA/Vertex Prototype'
        !               aRow%type = 'ALMA/AEC Prototype'
        aRow%dishDiameter = 12.
      endif
    endif
    ! HOLO PATCH
    ! should not be needed any more ....
    ! Use the VTX-ALMATI and AEC-ALMATI telescope names for compatibility
    !$$$         if (aRow%type.eq.'ALMA/Vertex Prototype' .and.
    !$$$     &        aRow%dishDiameter.eq.12.0) then
    !$$$            call sic_sexa ('-107:37:10.02',10,lonlat(1),error)
    !$$$            call sic_sexa ('34:04:29.8',8,lonlat(2),error)
    !$$$            altitude = 2.13542*1000.
    !$$$            do k=1, 2
    !$$$               lonlat(k) = pi*lonlat(k)/180.d0
    !$$$            enddo
    !$$$         elseif (aRow%type.eq.'ALMA/AEC Prototype' .and.
    !$$$     &           aRow%dishDiameter.eq.12.0) then
    !$$$            call sic_sexa ('-107:37:10.02',10,lonlat(1),error)
    !$$$            call sic_sexa ('34:04:29.8',8,lonlat(2),error)
    !$$$            altitude = 2.13542*1000.
    !$$$            do k=1, 2
    !$$$               lonlat(k) = pi*lonlat(k)/180.d0
    !$$$            enddo
    !$$$         elseif (aRow%type.eq.'IRAM/Bure 15m' .and. aRow%dishDiameter.eq
    !$$$     &           .15.0) then
    !$$$            call sic_sexa ('05:54:28.5',10,lonlat(1),error)
    !$$$            call sic_sexa ('44:38:02.0',10,lonlat(2),error)
    !$$$            altitude = 2.560*1000.
    !$$$         else
    !$$$            r_teles = 'Unsupported'
    !$$$c     default this to Bure...
    !$$$            call sic_sexa ('05:54:28.5',10,lonlat(1),error)
    !$$$            call sic_sexa ('44:38:02.0',10,lonlat(2),error)
    !$$$            altitude = 2.560*1000.
    !$$$         endif
    ! Patch in the ATF reference point ...

    call sic_sexa ('-107:37:10.02',10,lonlat(1),error)
    call sic_sexa ('34:04:29.8',8,lonlat(2),error)
    altitude = 2.13542*1000.
    do k=1, 2
      lonlat(k) = pi*lonlat(k)/180.d0
    enddo
    !     These coordinates are in the Bure-style  system, rotate them to
    !     geocentric...
    xObs = (radius+altitude)*cos(lonlat(1))*cos(lonlat(2))
    yObs = (radius+altitude)*sin(lonlat(1))*cos(lonlat(2))
    zObs = (radius+altitude)*sin(lonlat(2))
    
!!$    xAnt = aRow%xposition - xObs
!!$    yAnt = aRow%yposition - yObs
!!$    zAnt = aRow%zposition - zObs
    xAnt = r_antgeo(1,ia) - xObs
    yAnt = r_antgeo(2,ia) - yObs
    zAnt = r_antgeo(3,ia) - zObs
    r_ant(1,ia) = xAnt*cos(lonlat(1)) + yAnt *sin(lonlat(1))
    r_ant(2,ia) =-xAnt*sin(lonlat(1)) + yAnt *cos(lonlat(1))
    r_ant(3,ia) = zAnt
    !! deleted if (aRow%flagRow) dh_aflag(ia) = -1
    !     **TBD** not sure : check if axes offset is in x, y or z...
    !!r_axes(ia) = aRow%xoffset
    r_axes(ia) = aRow%offset(1)
    l1 = lenc(aRow%name)
    r_antennaName(ia) = aRow%name(1:l1)
    if (index(aRow%name(1:l1), 'BURE').gt.0) then
      k = index(aRow%name(1:l1), 'BURE')
      read (aRow%name(k+4:k+5),'(i2.2)') r_kant(ia)
    !
    ! ALMA antennas start an number 101 in CLIC physical numbers.
    elseif (index(aRow%name(1:l1), 'ALMA').gt.0) then
      k = index(aRow%name(1:l1), 'ALMA')
      read (aRow%name(k+4:k+5),'(i2.2)') r_kant(ia)
      r_kant(ia) = r_kant(ia) + 100
    else
      r_kant(ia) = 999
    endif
    !         l2 = lenc(aRow%type)
    !         r_antennaType(ia) = aRow%type(1:l2)
    r_antennaType(ia) = AntennaMake_toString(aRow%antennaMake)
    r_dishDiameter(ia) = aRow%dishDiameter
  enddo
  ib = 0
  do ja = 2, r_nant
    do ia = 1, ja - 1
      ib = ib+1
      r_iant(ib) = ia
      r_jant(ib) = ja
      do k=1, 3
        r_bas(k,ib) = r_ant(k, ja) -r_ant(k, ia)
      enddo
    enddo
  enddo
  call sdmMessage(1,1, sdmTable ,'Read.')
  return
  !
99 error = .true.
end subroutine get_sdm_antenna
!---------------------------------------------------------------------
