subroutine check_input_file(error)
  use clic_file
  use clic_index
  !---------------------------------------------------------------------
  ! Verify if any input file
  !---------------------------------------------------------------------
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  !------------------------------------------------------------------------
  ! Code:
  !
  if (i%lun.eq.0) then
    call message(8,3,'CHECK','No input file connected')
    error = .true.
    return
  endif
  return
  !
entry check_output_file(error)
  !
  ! Verify if any output file
  if (o%lun.eq.0) then
    call message(8,3,'CHECK','No output file connected')
    error = .true.
    return
  endif
  return
  !
entry check_equal_file(error)
  !
  ! Verify if both files are equal.
  if (o%lun.ne.i%lun) then
    call message(8,3,'CHECK','Output file must equal Input file')
    error = .true.
    return
  endif
  return
  !
entry check_different_file(error)
  !
  ! Verify if both files are different.
  if (o%lun.eq.i%lun) then
    call message(8,3,'CHECK','Input and Output file must differ')
    error = .true.
    return
  endif
  return
  !
entry check_index(error)
  !
  ! Test  on current index
  if (cxnext.le.1) then
    call message(8,3,'CHECK','Current index is empty')
    error = .true.
    return
  endif
  if (find_needed) then
    call message(8,2,'CHECK_INDEX',   &
      'Index does not match current search criteria')
  endif
  return
end subroutine check_input_file
