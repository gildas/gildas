subroutine solve_delay(line,error)
  use gildas_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! CLIC
  !     Interface routine for SOLVE DELAY/SEARCH/PRINT
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(kind=address_length) :: ipx,ipy
  !------------------------------------------------------------------------
  ! Code:
  call check_index(error)
  if (error) return
  ipx = gag_pointer(data_x,memory)
  ipy = gag_pointer(data_y,memory)
  !
  ! just in case m_data has changed
  call get_first(.true.,error)
  !
  call sub_solve_delay(m_data,m_boxes,memory(ipx),   &
    memory(ipy),line,error)
  return
end subroutine solve_delay
!
subroutine sub_solve_delay(md,mb,x_data,y_data,line,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api  
  use gio_params
  !---------------------------------------------------------------------
  !     CLIC
  !     Interface routine for SOLVE DELAY/SEARCH
  !     Solves for DELAY components errors.
  !     SOLVE DELAY [DIFF] /OFFSET base1 dx dy dz ...
  !     Arguments
  !     MD       I     Number of data
  !     MB       I     Number of boxes
  !     X_DATA   R*4   X coordinates of data in boxes
  !     Y_DATA   R*4   Y coordinates of data in boxes
  !     LINE     C*(*) Command line
  !     ERROR    L     Logical error flag
  !---------------------------------------------------------------------
  integer :: md                     !
  integer :: mb                     !
  real :: x_data(md,mb)             !
  real :: y_data(md,mb)             !
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'gbl_pi.inc'
  include 'clic_stations.inc'
  include 'clic_xy_code.inc'
  include 'gbl_memory.inc'
  ! Local
  integer, parameter :: mtry=5000
  integer(kind=index_length) :: dim(4)
  integer :: nd, ix, iy, i, l, l1, l2, l3, ib, ntry, i1, iband
  logical :: plot, print, bb_listed, doit
  real :: search_box, db, bmes(2), bmin(2), ddb, sig, sigmin
  real :: savedb, search_interval, delay(mnant,mnbb)
  character(len=132) :: chain
  integer :: m_base, ibase, iant, jant, ii, ibb, iif
  integer :: nbb(mnif),i_bb(mnif,mnbb)
  integer(kind=address_length) :: data_base, ipd
  real*8 :: err8
  data m_base/0./
  !------------------------------------------------------------------------
  save m_base, data_base
  !     Code:
  error = .false.
  plotted = .true.
  !
  !     Offset values:
  ntry = 1
  search_box = 20.             ! in ns
  search_interval = 0.01
  call sic_r4(line,7,1,search_box,.false.,error)
  if (error) goto 999
  call sic_r4(line,7,2,search_interval,.false.,error)
  if (error) goto 999
  ddb = search_interval
  ntry = min(mtry, int(search_box/ddb + 1.))
  !
  if (change_display) then
    call read_spec('ALL',.true.,error) ! all scans in index,unsorted/X
    if (error) return
    change_display = .false.
    plotted = .false.
  endif
  !
  ! Needs selection by either basebands of IF. 
  if (r_kind.ge.5.and..not.(bb_select.or.if_select)) then
    call message(6,3,'SOLVE_DELAY',   &
      'Needs selection by IF or baseband')
    return
  endif
  !
  plot = sic_present(1,0)
  print = sic_present(10,0)
  if (plot .and. .not.plotted) then
    clear = .true.
    call sub_plot('All',.false.,.false.,0,error)
    if (error) return
  endif
  !
  ! Polarisation case
  f_crossed = .false.
  f_reference = .false.
  ! Check on first switch
  if (all((r_polswitch(1:r_nant,1).eq.1))) then
    f_crossed = .true.
    call message(6,2,'SOLVE_DELAY',   &
      'Solving for all switches crossed delays')
  else
    if (any((r_polswitch(1:r_nant,1).eq.1))) then
      f_reference = .true.
      call message(6,2,'SOLVE_DELAY',   &
        'Solving for V-H delay')
    endif
  endif
  f_delaybase = .false.
  !
  ! If HV or VH it is reference delay
  ! and we want to use all baselines
  if (i_stokes(1).eq.code_stokes_hv.or. &
      i_stokes(1).eq.code_stokes_vh) then
    f_reference = .true.
    f_crossed = .false.
    f_delaybase = .true.
  endif
  !
  do i=1, mnant
    do ibb=1, mnbb
      f_bbdelayoff(i,ibb) = .false.
      f_bbdelayfit(i,ibb) = .false.
    enddo
    do iif=1, mnif
      f_ifdelayoff(i,ibb) = .false.
      f_ifdelayfit(i,iif) = .false.
    enddo
  enddo
  !
  ! For IF, find which bband connected to which IF
  if (if_select) then
    nbb = 0
    do i =1, r_lband
      iif = r_if(i)
      ibb = r_bb(i)
      bb_listed = .false.
      if (nbb(iif).gt.0) then
        do ii=1, nbb(iif)
          if (i_bb(iif,ii).eq.ibb) bb_listed = .true.
        enddo
      endif
      if (.not.bb_listed) then
        nbb(iif) = nbb(iif)+1
        i_bb(iif,nbb(iif)) = ibb
        print *,"Found a new bband for IF ",iif," : ",ibb        
      endif
    enddo
    do i = 1, r_nant
      do iif=1, mnif
        if (nbb(iif).gt.0) then
          do ibb = 1, nbb(iif)
            l = i_if(iif,ibb)
            delay(i,iif) = delay(i,iif) + dh_delay(l,i)
          enddo
          delay(i,iif) = delay(i,iif)/nbb(iif)
        endif
      enddo
    enddo
  !
  ! Bband case is simpler
  elseif (bb_select) then
    do i = 1, r_nant
      do ibb=1, mnbb
        delay(i,ibb) = dh_delay(ibb,i)
      enddo
    enddo
  endif
  !
  !     Loop on : baselines, bands, subbs (La Triple Boucle)
  do ib = 1, n_boxes
    iy = i_y(k_y(ib))
    ix = i_x(k_x(ib))
    if (i_band(k_band(1)).eq.1) then
      iband = 1
    else
      iband = -1
    endif
    if (i_band(k_band(1)).eq.3) then
      call message(6,3,'SOLVE_DELAY',   &
        'Cannot work in DSB mode')
    endif
    doit = .true.
    ibase = i_base(k_base(ib))
    if (f_reference) then
      if (ibase.lt.0) then
        call message(6,3,'SOLVE_DELAY','Cannot solve for H-V in antenna mode')
        if (f_delaybase) then
          error = .true.
          return
        endif
        doit = .false.
        if (r_polswitch(-ibase,1).ne.r_polswitch(ref_ant,1)) then
          doit = .true.
        endif
      else
        doit = .false.
        if (r_polswitch(r_iant(ibase),1).ne.r_polswitch(r_jant(ibase),1)) then
          doit = .true.
        endif
        if (f_delaybase) doit = .true.
      endif
    endif
    if (ix.eq.xy_i_fre1 .and. iy.eq.xy_phase.and.doit) then
      nd = n_data(ib)
      !
      !     Get auxiliary storage
      if (nd*16.gt.m_base) then
        if (m_base .gt.0) then
          call free_vm(m_base,data_base)
          m_base = 0
        endif
        error = (sic_getvm(nd*16,data_base).ne.1)
        if (error) return
        m_base = nd*16
      endif
      ipd = gag_pointer(data_base,memory)
      !
      sigmin = 1e20
      db = -search_box/2.
      do i1 = 1, ntry
        db = db+ddb
        call solve_del_sub (nd,x_data(1,ib),   &
          y_data(1,ib), iband, db, bmes,   &
          sig, memory(ipd+2*nd), memory(ipd),   &
          error)
        if (sig.lt.sigmin   &
          .and. abs(bmes(1)).lt.search_box/2) then
          bmin(1) = bmes(1)
          bmin(2) = bmes(2)
          sigmin = sig
          savedb = db
        endif
      enddo
      !
      !     Display results
      l = lenc(y_label(ib))
      l1 = lenc(header_1(ib))
      l2 = lenc(header_2(ib))
      l3 = lenc(header_3(ib))
      write(chain,1000) ib, header_1(ib)(1:l1),   &
        header_2(ib)(1:l2), header_3(ib)(1:l3), sigmin
      call message(6,1,'SOLVE_DELAY',chain(1:lenc(chain)))
      write(chain,1001) (bmin(i), i=1,2)
      call message(6,1,'SOLVE_DELAY',chain(1:lenc(chain)))
      ibase = i_base(k_base(ib))
      if (bb_select) then
        ibb = i_baseband(1,k_baseband(ib))
      elseif (if_select) then
        ibb = i_if(1,k_if(ib))
      endif
      call solve_del_sub (nd,x_data(1,ib),   &
        y_data(1,ib), iband, savedb, bmin,   &
        sigmin, memory(ipd+2*nd), memory(ipd),   &
        error)
      call show_del_errors(memory(ipd+2*nd))
      if (plot) then
        call plot_delay(ib,iband,bmin,error)
        if (error) return
      endif
      !
      !     Save fitted delay if by antenna ...
      if (ibase.lt.0) then
        iant = -ibase
      elseif ((r_jant(ibase).le.r_nant).and.(r_iant(ibase).eq.1))   &
        then
        iant = r_jant(ibase)
      else
        iant = 0
      endif
      if (iant.ne.0) then
        if (bb_select) then
          f_bbdelayfit(iant,ibb) = .true.
          jant = r_kant(iant)
          f_bbdelayoff(jant,ibb) = .true.
          bbdelayoff(jant,ibb) = -bmin(1)
          call r4tor4(memory(ipd+2*nd), err8, 2)
          bbdelayrms(iant,ibb) = sqrt(err8)
          write(chain,'(a,i0,a,f9.3,a,f7.3,a)')   &
            'Delay offset for Phys.Ant. ',jant,' : ',   &
            bbdelayoff(jant,ibb), ' +- ', bbdelayrms(iant,ibb),' ns'
          call message(6,1,'SOLVE_DELAY',chain(1:lenc(chain)))
          bbdelayfit(iant,ibb) = delay(iant,ibb)-bmin(1)
          write(chain,'(a,i0,a,f9.3,a,f7.3,a)')   &
            'Absolute delay for Log.Ant. ',iant,' :',   &
            bbdelayfit(iant,ibb), ' +- ', bbdelayrms(iant,ibb),' ns'
          call message(6,1,'SOLVE_DELAY',chain(1:lenc(chain)))
        elseif(if_select) then
          f_ifdelayfit(iant,ibb) = .true.
          jant = r_kant(iant)
          f_ifdelayoff(jant,ibb) = .true.
          ifdelayoff(jant,ibb) = -bmin(1)
          call r4tor4(memory(ipd+2*nd), err8, 2)
          ifdelayrms(iant,ibb) = sqrt(err8)
          write(chain,'(a,i0,a,f9.3,a,f7.3,a)')   &
            'Delay offset for Phys.Ant. ',jant,' : ',   &
            ifdelayoff(jant,ibb), ' +- ', ifdelayrms(iant,ibb),' ns'
          call message(6,1,'SOLVE_DELAY',chain(1:lenc(chain)))
          ifdelayfit(iant,ibb) = delay(iant,ibb)-bmin(1)
          write(chain,'(a,i0,a,f9.3,a,f7.3,a)')   &
            'Absolute delay for Log.Ant. ',iant,' :',   &
            ifdelayfit(iant,ibb), ' +- ', ifdelayrms(iant,ibb),' ns'
          call message(6,1,'SOLVE_DELAY',chain(1:lenc(chain)))
        endif
      endif
      !
      ! Also per baseline if needed
      if (ibase.gt.0) then
        bbdelayfitbase(ibase,ibb) = -bmin(1)
      endif
    else
      if (doit) call message(6,3,'SOLVE_DELAY',   &
        'Use SET X IF1 and SET Y PHASE')
    endif
  enddo
  call sic_delvariable ('DELAY',.false.,error)
  call sic_delvariable ('DELAY_ERROR',.false.,error)
  error = .false.
  dim(1) = mnant
  if (bb_select) then
    dim(2) = mnbb
    call sic_def_real ('DELAY',bbdelayoff,2,dim,   &
        .false.,error) 
    call sic_def_real ('DELAY_ERROR',bbdelayrms,2,dim,   &
        .false.,error) 
  elseif(if_select) then
    dim(2) = mnif
    call sic_def_real ('DELAY',ifdelayoff,2,dim,   &
        .false.,error) 
    call sic_def_real ('DELAY_ERROR',ifdelayrms,2,dim,   &
        .false.,error) 
  endif
  if (f_delaybase) then
    call sic_delvariable('DELAY_BASE',.false.,error)
    dim(1) = mnbas
    dim(2) = mnbb
    call sic_def_real('DELAY_BASE',bbdelayfitbase,2,dim,.false.,error)
  endif
  if (print) then
    call print_delay(.false.,error)
    if (.not.error) call message(6,1,'SOLVE_DELAY',   &
      'Procedure delay.obs created')
  else
    call message(6,1,'SOLVE_DELAY',   &
      'Command PRINT DELAY will create a procedure '//   &
      ' for updating delays in OBS.')
  endif
  return
  !
999 error = .true.
  return
1000 format(i2,1x,a,' Ch. ',a,' Band ',a,' rms ',f12.3)
1001 format('delay= ',f12.3,' ns.  phase= ',f12.3)
end subroutine sub_solve_delay
!
subroutine plot_delay(ib,iband,bmin,error)
  use gildas_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! CLIC
  !     Plot the result from delay determination
  !     IB      INTEGER    Box number
  !     BMIN(2) REAL       Limits for plot
  !---------------------------------------------------------------------
  integer :: ib                     !
  integer :: iband                  !
  real :: bmin(2)                   !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_clic.inc'
  include 'gbl_pi.inc'
  ! Local
  integer :: k, l, kmin, kmax, old_pen
  real :: xmin, xmax, ymin, ymax, y1, y2, xx(2), yy(2), tour
  character(len=132) :: chain
  logical :: err
  !------------------------------------------------------------------------
  ! Code:
  err = .false.
  if (degrees) then
    tour = 360.
  else
    tour = 2.0*pis
  endif
  write(chain,'(I4.4)') ib
  call gr_execl('CHANGE DIRECTORY BOX'//chain)
  error = gr_error()
  if (error) return
  call sic_get_real ('USER_XMIN',xmin,error)
  call sic_get_real ('USER_XMAX',xmax,error)
  call sic_get_real ('USER_YMIN',ymin,error)
  call sic_get_real ('USER_YMAX',ymax,error)
  if (iband.eq.-1) then
    y1 = xmin * bmin(1)*tour/1000. + bmin(2)
    y2 = xmax * bmin(1)*tour/1000. + bmin(2)
  else
    y1 = -xmin * bmin(1)*tour/1000. + bmin(2)
    y2 = -xmax * bmin(1)*tour/1000. + bmin(2)
  endif
  old_pen = gr_spen(1)
  call gr_segm ('DELAY',err)
  kmin = (max(y1,y2)-ymin) / tour
  kmax = (ymax-min(y1,y2)) / tour
  xx(1) = xmin
  xx(2) = xmax
  do k = -kmin, kmax
    yy(1) = y1 + k*tour
    yy(2) = y2 + k*tour
    call gr4_connect (2,xx,yy,blank4,d_blank4)
  enddo
  call gr_segm_close(error)
  call gr_execl('CHANGE DIRECTORY')
  k = gr_spen(old_pen)
  return
end subroutine plot_delay
!
subroutine show_del_errors(a)
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! CLIC
  !     Print result from delay determination
  !     A       REAL*8    Square of the errors
  !---------------------------------------------------------------------
  real*8 :: a                       !
  ! Local
  character(len=132) :: chain
  real :: err
  !------------------------------------------------------------------------
  ! Code:
  err = sqrt(a)
  write(chain,1000) err
  call message(5,1,'SHOW_DEL_ERRORS',chain(1:lenc(chain)))
  return
1000 format('Rms error= ',f12.3,' ns.')
end subroutine show_del_errors
!
subroutine solve_del_sub (nd,   &
    x_if, y_pha, iband, boff, bmes, sig, a, b, error)
  use gildas_def
  !---------------------------------------------------------------------
  !     ND        INTEGER    Number of points
  !     X_IF(ND)  REAL       I_F frequency of points
  !     Y_PHA(ND) REAL       Phases of points
  !     BOFF      REAL       Original delay offset
  !     BMES(2)   REAL       Returned delay (?) or phase range (?)
  !     SIG       REAL       Rms error on phase (?) or delay (?)
  !     A(ND,MVAR)   REAL*8  Work space for minimization routine
  !                          on return, covariance matrix ?
  !     B(ND)        REAL*8  Ibid
  !     ERROR     LOGICAL    Error flag
  !---------------------------------------------------------------------
  integer :: nd                     !
  real :: x_if(nd)                  !
  real :: y_pha(nd)                 !
  integer :: iband                  !
  real :: boff                      !
  real :: bmes(2)                   !
  real :: sig                       !
  real*8 :: a(nd,*)                 !
  real*8 :: b(nd)                   !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'gbl_pi.inc'
  ! Local
  integer :: i,k,ndd,nvar
  real*8 :: sigma
  !------------------------------------------------------------------------
  !      SAVE SIGMA
  ! Code:
  nvar = 2
  i = 0
  do k=1, nd
    if (abs(y_pha(k)-blank4).gt.d_blank4) then
      i = i + 1
      ! Careful: convention for delays is (first ant - second ant.)
      if (iband.eq.1) then
        a(i,1) = -x_if(k)*pis*2/1000.  ! in rad/ns (X_IF in MHz)
      else
        a(i,1) =  x_if(k)*pis*2/1000.  ! in rad/ns (X_IF in MHz)
      endif
      a(i,2) = 1
      if (.not.degrees) then
        b(i) = y_pha(k)
      else
        b(i) = y_pha(k)/180.*pis
      endif
      b(i) = b(i)-boff*a(i,1)
      do while (b(i).le.-pis)
        b(i) = b(i)+2*pis
      enddo
      do while (b(i).ge.pis)
        b(i) = b(i)-2*pis
      enddo
    endif
  enddo
  ndd = i
  if (ndd.lt.nvar) then
    call message(8,4,'SOLVE_DELA_SUB','Too few data points')
    error = .true.
    return
  endif
  call mth_fitlin ('SOLVE_DELAY',ndd,nvar,a,b,nd,sigma)
  ! display results
  if (.not.degrees) then
    sig = sigma
  else
    sig = sigma *180./pis
  endif
  bmes(1) = b(1)+boff
  b(2) = mod(b(2)+21.*pi,2*pi)-pi
  if (.not.degrees) then
    bmes(2) = b(2)
  else
    bmes(2) = b(2) *180./pis
  endif
end subroutine solve_del_sub
