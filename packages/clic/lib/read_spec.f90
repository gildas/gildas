subroutine read_spec(arg,init,error)
  use gildas_def
  use gkernel_interfaces
  character(len=*) :: arg           !
  logical :: init                   !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(kind=address_length) :: ipx,ipy,ipz,ipw,ipi,ipu
  integer(kind=address_length) :: ips
  !------------------------------------------------------------------------
  ! Code:
  !
  ! Verify that input file and index do exist
  call check_input_file(error)
  if (error) return
  !
  call check_index(error)
  if (error) return
  !
  ! Make sure m_data and m_boxes are up to date
  ! (if set base all or set sub all)
  if (arg(1:1).eq.'A') then
    call get_first(.true.,error)
  endif
  !
  ipx = gag_pointer(data_x,memory)
  ipy = gag_pointer(data_y,memory)
  ipz = gag_pointer(data_z,memory)
  ipw = gag_pointer(data_w,memory)
  ipi = gag_pointer(data_i,memory)
  ipu = gag_pointer(data_u,memory)
  ips = gag_pointer(data_s,memory)
  call sub_read_spec(   &
    m_data,m_boxes,memory(ipx),memory(ipy),   &
    memory(ipw), memory(ipi), memory(ipu), memory(ipz),   &
    memory(ips), arg, init, error)
end subroutine read_spec
!
subroutine sub_read_spec(md,mb,x_data,y_data,w_data,   &
    i_data,u_data,z_data,a_data,arg,init,error)
  use gildas_def
  use gkernel_interfaces
  use gio_params
  use classic_api
  use clic_rdata
  !---------------------------------------------------------------------
  ! CLIC
  !	Fill in the display data buffers from current index.
  !	arg : 'A' all scans in current index are averaged
  !	      'N' next scan in current index is averaged
  !	data is the spectrum og selected subbands, bands, baselines.
  !  MD      INTEGER        Number of points per box
  !  MB      INTEGER        Number of boxes
  !  X_DATA  REAL(MB,MD)    X coordinates
  !  Y_DATA  REAL(MB,MD)    Y values
  !  W_DATA  REAL(MB,MD)    Weight values (for error bars)
  !  I_DATA  REAL(MB,MD)    Identification number
  !  U_DATA  REAL(MB,MD)    UT time of point
  !  Z_DATA  COMPLEX(MB,MD) Visibility of point used to compute X and Y
  !  A_DATA  REAL(MB,MD)    "Amplitude" of point
  !  ARG     C*(*)
  !  INIT    L              Initialize the plot buffers.
  !  ERROR   L              Logical error flag
  !---------------------------------------------------------------------
  integer :: md                     !
  integer :: mb                     !
  real :: x_data(md,mb)             !
  real :: y_data(md,mb)             !
  real :: w_data(md,mb)             !
  integer :: i_data(md,mb)          !
  integer :: u_data(md,mb)          !
  complex :: z_data(md,mb)          !
  real :: a_data(md,mb)             !
  character(len=*) :: arg           !
  logical :: init                   !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_clic.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_proc_par.inc'
  include 'clic_xy_code.inc'
  include 'gbl_memory.inc'
  ! Local
  integer(data_length) :: h_offset, c_offset, l_offset
  logical :: down_baseline, down_antenna, down_channel
  logical :: auto_data, corr_data
  logical :: end, flag
  logical :: stokes_required, needed
  integer :: i, kk, ib, ix, iy, ibase, iband, ich1, ich2
  integer :: isub, nx, ny, ignored(0:2)
  integer :: nnn, nfl, jw1, jw2, jw3, iant, itri
  integer :: kb, lb, jsub, kr
  logical :: first, do_gainc, do_gainl, auto, corr, zero_average, skip
  real*4 :: w, offw
  integer :: lsave, l, scan_save, ier
  character(len=80) :: ch
  !
  real :: total_time, uvr
  integer(kind=address_length) :: ksave, ipk, ipkc, ipkl
  integer(kind=address_length) :: k, data_in, kin
  integer(kind=data_length)    :: ldata_in
  save ksave, lsave
  data lsave/0/
  !------------------------------------------------------------------------
  ! Code:
  !
  ! Initialize
  if (init .or. lsave.eq.0) then
    first = .true.
    offw = 1
    scan_save = 0
    do i = 1, n_boxes
      n_data(i) = 0
    enddo
    zero_average = .true.
    do i=0,2
      ignored(i) = 0
    enddo
  else
    ipk = gag_pointer(ksave,memory)
    do ib=1, n_boxes
      call r4tor4(memory(ipk),z_data(1,ib),n_data(ib)*2)
      ipk = ipk + n_data(ib)*2
      call r4tor4(memory(ipk),a_data(1,ib),n_data(ib))
      ipk = ipk + n_data(ib)
      call r4tor4(memory(ipk),w_data(1,ib),n_data(ib))
      ipk = ipk + n_data(ib)
    enddo
  endif
  !
  skip = .false.
  end = .false.
  ! PLOT NEXT
  if (arg(1:1).eq.'N') then
    call get_next(end,error)
    if (error) return
    if (end) then
      call message(8,3,'READ_SPEC','End of current index')
      error = .true.
      return
    endif
  ! PLOT number
  elseif (arg(1:1).ne.'A' .and. arg(1:1).ne.'F') then
    read(arg(1:lenc(arg)),*,err=999) nnn
    call get_sub(nnn,0,error)
    if (error) return
  ! PLOT FIRST or PLOT ALL
  else
    if (do_spec .or. r_proc.eq.p_onoff) then
      call get_first(.true.,error)
    else
      call get_first(.false.,error)
    endif
    if (error) return
    if (arg(1:1).eq.'F') end = .false.
  endif
  !
  stokes_required = .false.
  if (n_stokes.gt.1.or.i_stokes(1).ne.code_stokes_none) then
    stokes_required = .true.
    call message(6,1,'READ_SPEC','Stokes parameter selection')
  endif
  do while (.not.end)
    call check_subb(n_subb,first,error)
    if (error) return
    !
    ! Get number and X coordinates from header information
    if (first) then
      !
      ! Define correlator configuration if first scan
      l = 0
      if (do_spec .and. .not.r_lfour) then
        call vel_scale
      endif
      nfl = 0
      do_gainc = .false.
      do_gainl = .false.
      do ib = 1, n_boxes
        ix = i_x(k_x(ib))
        iy = i_y(k_y(ib))
        corr = corr_data(ix,iy)
        kb = k_subb(ib)
        lb = l_subb(kb)
        ich1 = i_subb(1,kb)
        ich2 = i_subb(lb,kb)
        ibase = i_base(k_base(ib))
        iband = i_band(k_band(ib))
        do_gainc = do_gainc .or.   &
          (corr .and. i_base(1).lt.0 .and. ich1.le.mbands)
        do_gainl = do_gainl .or.   &
          (corr .and. i_base(1).lt.0 .and. ich1.gt.mbands)
        k = 0
        do jsub=1, lb
          isub = i_subb(jsub,kb)
          call xvalue(ix,ibase,iband,isub,x_data(k+1,ib),   &
            nx,r_lfour)
          k = k + nx
          if (nx.ge.2 .and. isub.ne.ich2) then
            k = k + 2          ! 2 values to be blanked
          ! for nice histograms
          endif
        enddo
        n_data(ib) = k
        if (n_data(ib).gt.m_data) then
          goto 990
        endif
        do i=1, n_data(ib)
          z_data(i,ib) = 0
          y_data(i,ib) = 0
          w_data(i,ib) = 0
          a_data(i,ib) = 0
          i_data(i,ib) = 0
          u_data(i,ib) = 0
        enddo
        l= l + n_data(ib)*4
      enddo
      first = .false.
      total_time = 0.
      !
      ! Get memory for PLOT/APPEND
      if (l.gt.lsave) then
        if (lsave.gt.0) call free_vm(lsave, ksave)
        lsave = l
        ier=sic_getvm(lsave,ksave)
        if (ier.ne.1) then
          error = .true.
          call message(8,3,'READ_SPEC',   &
            'Memory allocation failure')
          return
        endif
      endif
    endif
    !
    ! Check for configuration
    do ib = 1, n_boxes
      ix = i_x(k_x(ib))
      iy = i_y(k_y(ib))
      ibase = i_base(k_base(ib))
      iband = i_band(k_band(ib))
      kb = k_subb(ib)
      lb = l_subb(kb)
      ich1 = i_subb(1,kb)
      ich2 = i_subb(lb,kb)
      ibase = i_base(k_base(ib))
      itri = ibase-mnbas
      iant = -ibase
      if (ibase.le.mnbas .and. ibase.gt.r_nbas) then
        call message(6,3,'READ_SPEC',   &
          'Baseline '//cbas(ibase)//' not available in data')
        error = .true.
        return
      elseif (itri.gt.0 .and. itri.gt.r_ntri) then
        call message(6,3,'READ_SPEC',   &
          'Triangle '//ctri(itri)//' not available in data')
        error = .true.
        return
      endif
      if (iant.gt.r_nant) then
        write(ch,'(A,I2,A)') 'Antenna ',-ibase,   &
          ' not available in data'
        call message(6,3,'READ_SPEC',ch(1:lenc(ch)))
        error = .true.
        return
      endif
    !
    enddo
    !
    ! Read data
    call get_data (ldata_in,data_in,error)
    if (error) return
    call check_cal(skip)
    if (skip) then
      ignored(r_lmode) = ignored(r_lmode) + 1
      skip = .false.
      goto 180
    endif
    if (do_pass) then
      call set_pass (r_nsb,r_nband,r_nbas+r_ntri,   &
        r_lntch, passc, passl, error)
      if (error) return
    endif
    if (do_spidx) then
      call set_spidx(r_nsb,r_nband,r_nbas+r_ntri,   &
        r_lntch, spidc, spidl, r_spidx, error)
    endif
    if (do_pass) then
      if (do_spidx) then
        where (passc.ne.blankc)
          factc = passc * spidc
        elsewhere
          factc =  blankc
        end where
        where (passl.ne.blankc)
          factl = passl * spidl
        elsewhere
          factl = blankc
        end where
      else
        factc = passc
        factl = passl
      endif
    elseif (do_spidx) then
      factc = spidc
      factl = spidl
    endif
    call set_corr(error)
    !
    if (r_proc.eq.p_onoff) then
      ! For ONOFF, the OFF is done before (now)
      if (scan_save.ne.r_scan) then
        offw = -1
      else
        offw = -offw
      endif
      scan_save = r_scan
    else
      ! Others MUST be positive
      offw = 1
    endif
    !  skip temporal data
    kin = gag_pointer(data_in,memory)
    call spectral_dump(kr,0,0)
    ipk = kin + h_offset(kr)
    call decode_header (memory(ipk))
    !         IPKC = KIN + C_OFFSET(KR)
    !         IPKL = KIN + L_OFFSET(KR)
    flag = .false.
    call set_scaling(error)
    !
    w = dh_integ*offw
    if (w.ge.0) total_time = total_time+dh_integ
    if (r_lmode.eq.1) then     ! correlation only ...
      if (do_gainc) call gain_cont(   &
        r_nsb, r_nband, r_nbas, r_nant,   &
        data_in, factc, gainc, wgainc,error)
      if (error) return
      if (do_gainl) call gain_line(   &
        r_lntch, r_lnsb, r_nbas, r_nant,   &
        data_in, factl, gainl, wgainl,error)
      if (error) return
      if (k_average.eq.1) then
        call line_average (r_lntch,r_nbas,r_lnsb,data_in,   &
          spidl,zero_average,error)
      else
        call cont_average (r_nband,r_nbas,data_in,   &
         spidc,zero_average,error)
      endif 
      if (error) return
    endif
    do ib = 1, n_boxes
      ibase = i_base(k_base(ib))
      !
      ! Test for masks
      if (ibase.gt.0) then
        if (down_baseline(ibase)) then
          flag = .true.
          goto 140
        endif
        call spectral_dump(kr,0,ibase)
        ipkc = kin + c_offset(kr)
        ipkl = kin + l_offset(kr)
      else
        if (down_antenna (-ibase)) then
          flag = .true.
          goto 140
        endif
        call spectral_dump(kr,-ibase,0)
        ipkc = kin + c_offset(kr)
        ipkl = kin + l_offset(kr)
      endif
      iband = i_band(k_band(ib))
      if (error) return
      kb = k_subb(ib)
      lb = l_subb(kb)
      ich1 = i_subb(1,kb)
      ich2 = i_subb(lb,kb)
      jw1 = iw1(k_subb(ib))
      jw2 = iw2(k_subb(ib))
      ix = i_x(k_x(ib))
      iy = i_y(k_y(ib))
      corr = corr_data(ix,iy)
      auto = auto_data(ix,iy)
      if (auto.and.r_lmode.eq.1) goto 140
      if (corr.and.r_lmode.eq.2) goto 140
      !
      ! Eliminate data out of UV Range:
      uvr = sqrt(dh_uvm(1,ibase)**2+dh_uvm(2,ibase)**2)
      if ((uvr.lt.uv_range(1)).or.(uvr.gt.uv_range(2)))   &
        goto 140
      kk = 1
      do jsub=1, lb
        isub = i_subb(jsub,kb)
        if (isub.gt.mbands) then
          if (r_band4(isub-mbands).eq.2) then
            jw3 = ngibbs
          else
            jw3 = 0
          endif
        endif
        if (ibase.gt.0) then
          flag = flag.or.down_channel(ibase,isub)
          needed = .true.
          if (stokes_required) then
            if (i_stokes(k_stokes(ib)).ne. &
                r_code_stokes(ibase,mod(i_subb(1,kb),mbands))) &
               needed = .false.
            endif
        endif
        if (iy.le.xy_imag .and. ibase.gt.0) then
          ! "Complex" like data by baseline
          if (needed) then
            call zvalue (   &
              r_nsb, r_nband, r_nbas+r_ntri, r_lntch,   &
              memory(ipkc), memory(ipkl), factc, factl,   &
              ibase,iband,isub, jw1, jw2, jw3, w,   &
              z_data(kk,ib),a_data(kk,ib),   &
              w_data(kk,ib),ny,error)
            if (error) return
           endif
        elseif (iy.eq.xy_auto .and. ibase.lt.0) then
          ! Autocorrelation data
          call avalue (   &
            r_nband, r_nant, r_lntch,   &
            memory(ipkc), memory(ipkl),   &
            -ibase,iband,isub, jw1, jw2, jw3, w,   &
            z_data(kk,ib),a_data(kk,ib),w_data(kk,ib),ny)
        elseif (iy.le.xy_imag .and. ibase.lt.0   &
          .and. r_lmode.eq.1) then
          ! "Complex" like data by antenna
          call gvalue (   &
            r_nsb, r_nband, r_nant, r_lntch,   &
            gainc, gainl, wgainc, wgainl,   &
            -ibase,iband,isub, jw1, jw2, jw3, w,   &
            z_data(kk,ib),a_data(kk,ib),w_data(kk,ib),ny,error)
          if (error) return
        endif
        kk = kk + ny
        if (ny.ge.2 .and. isub.ne.ich2) then
          w_data(kk,ib) = 0
          x_data(kk,ib) = 2*x_data(kk-1,ib)   &
            -x_data(kk-2,ib)
          kk = kk + 1
          w_data(kk,ib) = 0
          x_data(kk,ib) = 2*x_data(kk+1,ib)   &
            -x_data(kk+2,ib)
          kk = kk + 1
        endif
      enddo
140   continue
    enddo
150 continue
    !
    ! Go on for more scans
    if (flag) then
      call message(4,2,'READ_SPEC','Scan Flagged')
    endif
    if (arg(1:1).ne.'A') goto 200
180 continue
    if (sic_ctrlc()) then
      error = .true.
      goto 200
    endif
    call get_next(end,error)
    if (error) return
  enddo
  r_time = total_time
  !
  ! Get Y values from Z, W, and A data
  ! but first save Z, A, and W arrays for further PLOT/APPENDs
200 ipk = gag_pointer(ksave,memory)
  do ib = 1, n_boxes
    call r4tor4(z_data(1,ib),memory(ipk),n_data(ib)*2)
    ipk = ipk + n_data(ib)*2
    call r4tor4(a_data(1,ib),memory(ipk),n_data(ib))
    ipk = ipk + n_data(ib)
    call r4tor4(w_data(1,ib),memory(ipk),n_data(ib))
    ipk = ipk + n_data(ib)
    iy = i_y(k_y(ib))
    call yvalue(iy,z_data(1,ib),a_data(1,ib),   &
      w_data(1,ib), n_data(ib), y_data(1,ib))
  enddo
  if (ignored(1).ne.0) then
    write(ch,'(i4,a)') ignored(1),' ignored correlations'
    call message(4,2,'READ_SPEC',ch(1:lenc(ch)))
  endif
  if (ignored(2).ne.0) then
    write(ch,'(i4,a)') ignored(2),' ignored autocorrelations'
    call message(4,2,'READ_SPEC',ch(1:lenc(ch)))
  endif
  !
  ! Optionnally bin data
  if (do_bin) then
    call bin(error)
    if (error) return
  endif
  !
  ! Process phases
  call reset_phases
  !
  ! Redimension variables
  call resetvar(error)
  !
  return
  !
990 call message(8,4,'READ_SPEC','Too many data points')
999 error = .true.
  return
end subroutine sub_read_spec
!
subroutine yvalue(iy,zz,aa,ww,nn,yy)
  use gildas_def
  use gkernel_interfaces
  !---------------------------------------------------------------------
  ! CLIC
  !     Get spectral values
  !     IY      INTEGER    Type of Y value desired
  !     ZZ(NN)  COMPLEX    Visibilities of the NN points
  !     AA(NN)  REAL       Amplitude (??) of the NN points
  !     WW(NN)  REAL       Weight of the NN points
  !     NN      INTEGER    Number of points
  !     YY(NN)  REAL       Returned Y values
  !---------------------------------------------------------------------
  integer :: iy                     !
  integer :: nn                     !
  complex :: zz(nn)                 !
  real :: aa(*)                     !
  real :: ww(nn)                    !
  real :: yy(nn)                    !
  ! Global
  real :: faz
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_xy_code.inc'
  ! Local
  integer :: i, lch
  character(len=80) :: ch
  !------------------------------------------------------------------------
  ! Code:
  do i=1, nn
    if (ww(i).ne.0 .and. abs(zz(i)).gt.1e10) then
      write (ch,*) zz(i)
      lch = lenc(ch)
      call message(8,2,'YVALUE',   &
        'High amplitude data point '//ch(1:lch))
      ww(i) = 0
    endif
  enddo
  if (iy.eq.xy_ampli) then
    do i=1, nn
      if (ww(i).ne.0) then
        yy(i) = abs(zz(i))/ww(i)
        if (j_average.ne.1) then
          yy(i) = aa(i)/ww(i)
        endif
      else
        yy(i) = blank4
      endif
    enddo
  elseif (iy.eq.xy_auto) then
    do i=1, nn
      if (ww(i).ne.0) then
        yy(i) = real(zz(i))/ww(i)
        !  now for ONOFF: real = on (weight ww), imag = off (weight aa)
        if (aa(i).ne.0) then
          yy(i) = yy(i) - aimag(zz(i))/aa(i)
          ww(i) = ww(i) /2.
        endif
      else
        yy(i) = blank4
      endif
    enddo
  elseif (iy.eq.xy_phase) then
    do i=1, nn
      if (ww(i).gt.0) then
        yy(i) = faz(zz(i))
        ww(i) = abs(zz(i))**2/ww(i)    ! weight is amp**2
      else
        yy(i) = blank4
      endif
    enddo
    call cvphase(nn,yy,ww,degrees,.false.)
  elseif (iy.eq.xy_real) then
    do i=1, nn
      if (ww(i).gt.0) then
        yy(i) = real(zz(i)) / ww(i)
        if (j_average.ne.1) then
          yy(i) = aa(i)/ww(i) * cos(faz(zz(i)))
        endif
      else
        yy(i) = blank4
      endif
    enddo
  elseif (iy.eq.xy_imag) then
    do i=1, nn
      if (ww(i).ne.0) then
        yy(i) = aimag(zz(i)) / ww(i)
        if (j_average.ne.1) then
          yy(i) = aa(i)/ww(i) * sin(faz(zz(i)))
        endif
      else
        yy(i) = blank4
      endif
    enddo
  endif
  !
  ! Also return the normalized complex values
  do i=1, nn
    if (ww(i).ne.0) then
      zz(i) = zz(i) / ww(i)
    else
      zz(i) = cmplx(blank4,blank4)
    endif
  enddo
  return
end subroutine yvalue
!
function faz(z)
  !---------------------------------------------------------------------
  ! 	Compute the phase of a complex number Z
  !---------------------------------------------------------------------
  real :: faz                       !
  complex :: z                      !
  ! Global
  include 'clic_parameter.inc'
  !------------------------------------------------------------------------
  ! Code:
  if (z.ne.0 .and. z.ne.blankc) then
    faz = atan2(aimag(z),real(z))
  else
    faz = blank4
  endif
  return
end function faz
!
function expim(p)
  !---------------------------------------------------------------------
  !     Returns the complex number of phase P and modulus 1
  !---------------------------------------------------------------------
  complex :: expim                  !
  real :: p                         !
  !------------------------------------------------------------------------
  ! Code:
  expim = cmplx(cos(p),sin(p))
  return
end function expim
!
subroutine xvalue(ix,ibase,iband,isub,x,nx,four)
  use classic_api  
  !---------------------------------------------------------------------
  ! CLIC
  !	Get spectral abscissa of subband ISUB of continuum or line
  !     IX     INTEGER    Type of X value
  !     IBASE  INTEGER    Baseline number
  !     IBAND  INTEGER    Sideband (U or L)
  !     ISUB   INTEGER    Subbband of correlator
  !     X      REAL(*)    Returned X values
  !     NX     INTEGER    Number of X values
  !     FOUR   LOGICAL    Indicate "lag data"
  !---------------------------------------------------------------------
  integer :: ix                     !
  integer :: ibase                  !
  integer :: iband                  !
  integer :: isub                   !
  real*4 :: x(*)                    !
  integer :: nx                     !
  logical :: four                   !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_xy_code.inc'
  include 'clic_clic.inc'
  ! Local
  integer :: ibb, is, i, i0
  !------------------------------------------------------------------------
  ! Code  :
  ibb = iband
  is = mbands
  if (isub.gt.mbands) is = isub-mbands
  if (iband.gt.2) ibb = r_lsband(1,is)
  if (isub.le.mbands) then
    nx = 1
    !
    ! Channel number
    if (ix.eq.xy_chann) then
      x(1) = isub
    ! Velocity
    elseif (ix.eq.xy_veloc) then
      x(1) = r_cvoff(ibb) + (isub-r_crch(ibb))*r_cvres(ibb)
    ! IF frequency
    elseif (ix.eq.xy_i_fre) then
      x(1) = r_cfcen(isub)
    ! Sky Frequency
    elseif (ix.eq.xy_sky_f) then
      x(1) = r_crfoff(ibb)  + (isub-r_crch(ibb))*r_crfres(ibb)
    endif
  !
  ! Line
  else
    is = isub-mbands
    nx = r_lnch(is)
    !
    ! Channel number
    if (ix.eq.xy_chann) then
      if (four) then
        do i=1, nx
          !			x(i) = r_lich(is) + i
          x(i) = i
        enddo
      ! (old berkeley data)
      elseif (ibase.gt.0) then
        do i=1, nx
          x(i) = i+(iband-1)*nx+(is-1)*2*nx+(ibase-1)*256
        enddo
      elseif (ibase.lt.0) then
        if (ibase.eq.-1 .and. iband.eq.1) then
          i0 = 0
        elseif (ibase.eq.-1 .and. iband.eq.2) then
          i0 = 256
        elseif (ibase.eq.-2 .and. iband.eq.1) then
          i0 = nx
        elseif (ibase.eq.-2 .and. iband.eq.2) then
          i0 = 512
        elseif (ibase.eq.-3 .and. iband.eq.1) then
          i0 = 256+nx
        elseif (ibase.eq.-3 .and. iband.eq.2) then
          i0 = 512+nx
        endif
        do i=1, nx
          x(i) = i + (is-1)*2*nx + i0
        enddo
      endif
    !
    ! Velocity
    elseif (ix.eq.xy_veloc) then
      do i=1, nx
        x(i) = r_lvoff(ibb,is)+(i-r_lrch(ibb,is))*r_lvres(ibb,is)
      enddo
    ! IF frequency
    elseif (ix.eq.xy_i_fre) then
      do i=1, nx
        x(i) = r_lfcen(is)+(i-r_lcench(is)) * r_lfres(is)
      enddo
    ! IF frequency 1
    elseif (ix.eq.xy_i_fre1) then
      do i=1, nx
        x(i) = r_flo2(is) + r_band2(is) * (r_flo2bis(is)   &
          + r_band2bis(is)*   &
          (r_lfcen(is)+(i-r_lcench(is)) * r_lfres(is)))
      enddo
    ! IF frequency 2
    elseif (ix.eq.xy_i_fre2) then
      do i=1, nx
        x(i) = r_flo2bis(is)   &
          + r_band2bis(is)*   &
          (r_lfcen(is)+(i-r_lcench(is)) * r_lfres(is))
      enddo
    ! IF frequency 3
    elseif (ix.eq.xy_i_fre3) then
      do i=1, nx
        x(i) = (r_lfcen(is)+(i-r_lcench(is)) * r_lfres(is))
      enddo
    ! Sky Frequency
    elseif (ix.eq.xy_sky_f) then
      do i=1, nx
        x(i) = r_lrfoff(ibb,is)+(i-r_lrch(ibb,is))   &
          *r_lrfres(ibb,is)
      enddo
    endif
  endif
end subroutine xvalue
!
subroutine zvalue (qsb, qband, qbt, qntch,   &
    datac, datal, passc, passl,   &
    ibase,iband,isub, jw1, jw2, jw3, w,zz,aa,ww,ny,error)
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  !     QSB     INTEGER   Number of sidebands (2)
  !     QBAND   INTEGER   Number of temporal data
  !     QBT     INTEGER   Number of baseline+triangles
  !     QNTCH   INTEGER   Number of channels (in total per baseline)
  !     DATAC(QBAND,QSB,QBT)  COMPLEX  Temporal data
  !     DATAL(QNTCH,QSB,QBT)  COMPLEX  Spectral data
  !     PASSC(QBAND,QSB,QBT)  COMPLEX  PassBand of temporal data
  !     PASSL(QNTCH,QSB,QBT)  COMPLEX  PassBand of spectral data
  !     IBASE   INTEGER   Baseline number
  !     IBAND   INTEGER   Subband number
  !     ISUB    INTEGER   Sideband number
  !     NY      INTEGER   Number of Y values (output)
  !     JW1,JW2 INTEGER   Lowest and Highest channel in window subband
  !     W       REAL      Signed integration time
  !     AA(*)   REAL      Positive weights
  !     WW(*)   REAL      Total weights
  !     ZZ(*)   COMPLEX   Complex output
  ! Call Tree
  !	...	SUB_READ_SPEC	AVALUE
  !	...	SUB_READ_SPEC
  !---------------------------------------------------------------------
  integer :: qsb                    !
  integer :: qband                  !
  integer :: qbt                    !
  integer :: qntch                  !
  complex :: datac(qband,qsb,qbt)   !
  complex :: datal(qntch,qsb,qbt)   !
  complex :: passc(qband,qsb,qbt)   !
  complex :: passl(qntch,qsb,qbt)   !
  integer :: ibase                  !
  integer :: iband                  !
  integer :: isub                   !
  integer :: jw1                    !
  integer :: jw2                    !
  integer :: jw3                    !
  real*4 :: w                       !
  complex :: zz(*)                  !
  real*4 :: aa(*)                   !
  real*4 :: ww(*)                   !
  integer :: ny                     !
  logical :: error                  !
  ! Global
  real :: tsys_b
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_clic.inc'
  include 'clic_par.inc'
  include 'clic_sba.inc'
  include 'clic_proc_par.inc'
  ! Local
  integer :: is, i, k, ii, ii1, ii2, ig, im, jok(mch), ia, ja
  integer :: ibnd, ibdata
  complex :: z, zzz(2), av(2)
  real :: fw, afac, www(2)
 logical :: down_channel, present_sb
  !------------------------------------------------------------------------
  ! Code:
  if (ibase.le.mnbas) then
    ia = r_iant(ibase)
    ja = r_jant(ibase)
  else
    ia = 1
    ja = 2
  endif
  if (qsb.eq.1) then ! Check available sideband
    ibnd = 1
  else
    ibnd = 0
  endif
  !
  call get_cont_average(av,ibase)
  !
  if (iband.le.2) then
    ii1 = iband
    ii2 = iband
  else
    ii1 = 1
    ii2 = 2
  endif
  if (isub.le.mbands) then
    ny = 1
    is = isub
    afac = 1
    present_sb = .true.
    if (.not.down_channel(ibase,is)) then
      do ii = ii1, ii2
        if (qsb.eq.1.and.r_lsband(1,isub).ne.ii) then
          zzz(ii) = 0
          www(ii) = 0
        else
          if (ibnd.ne.0) then
            ibdata = ibnd
          else
            ibdata = ii
          endif
          zzz(ii) = datac(is,ibdata,ibase)
          if (do_pass.or.do_spidx) then
            zzz(ii) = zzz(ii)*passc(is,ii,ibase)
          endif
          call scaling (is,ii,ibase,zzz(ii),afac,error)
          if (error) then
            call message(8,4,'ZVALUE',   &
              'Invalid calibration factor for '//cband(ii))
            error = .true.
            return
          endif
          www(ii) = 2*w*r_cfwid(is)/tsys_b(is,ii,ibase)
        endif
      enddo
      call mixband(iband,r_sb(is),zzz,www,av,z,fw)
      fw = fw / afac**2
      zz(1) = zz(1) + fw * z
      if (j_average.ne.1) then
        aa(1) = aa(1) + fw * abs(z)
      endif
      ww(1) = ww(1) + fw
    endif
  else
    afac = 1
    is = isub-mbands
    ny = r_lnch(is)
    if (.not.down_channel(ibase,isub)) then
      k = r_lich(is)
      call jlimits(ny,jw1,jw2,jw3,jok)
      call jmask(ny,nmask(is),cmask(:,:,is),jok)
      do i  = 1, ny
        k = k + 1
        do ii = ii1, ii2
          if (qsb.eq.1.and.r_lsband(1,is).ne.ii) then
            zzz(ii) = 0
            www(ii) = 0
          else
            if (ibnd.ne.0) then
              ibdata = ibnd
            else
              ibdata = ii
            endif
            zzz(ii) = datal(k,ibdata,ibase)
            if (do_pass.or.do_spidx) then
              if (passl(k,ii,ibase).ne.blankc) then
                zzz(ii) = zzz(ii) * passl(k,ii,ibase)
              else
                jok(i) = 0
              endif
            endif
            call scaling (is,ii,ibase,zzz(ii),afac,error)
            if (error) then
              call message(8,4,'ZVALUE',   &
                'Invalid calibration factor for '//cband(ii))
              error = .true.
              return
            endif
            www(ii) = 2*w*abs(r_lfres(is))/tsys_b(is,ii,ibase)
          endif
        enddo
        call mixband(iband,r_sb(is),zzz,www,av,z,fw)
        fw = fw / afac**2 * jok(i)
        zz(i) = zz(i) + fw * z
        if (j_average.ne.1) then
          aa(i) = aa(i) + fw * abs(z)
        endif
        if (fw.gt.0) ww(i) = ww(i) + fw
      enddo
    endif
  endif
  return
end subroutine zvalue
!
subroutine avalue (qband, qant, qntch, datac, datal,   &
    iant, iband, isub, jw1, jw2, jw3, w, zz, aa, ww, ny)
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  ! CLIC
  !     Get antenna based values for display
  ! Input:
  !     QBAND   INTEGER   Number of temporal data
  !     QANT    INTEGER   Number of antennas
  !     QNTCH   INTEGER   Number of channels (in total per baseline)
  !     DATAC(QBAND,QANT)   REAL     Temporal data
  !     DATAL(QNTCH,QANT)   REAL     Spectral data
  !     IANT    INTEGER   Antenna number
  !     IBAND   INTEGER   Subband number
  !     ISUB    INTEGER   Sideband number
  !     JW1,JW2 INTEGER   Lowest and Highest channel in window subband
  !     W       REAL      Signed integration time
  ! Output:
  !     ZZ(*)   COMPLEX   Complex output
  !     AA(*)   REAL      Positive weights
  !     WW(*)   REAL      Total weights
  !     NY      INTEGER   Number of Y values (output)
  ! Call Tree
  !	...	SUB_READ_SPEC	AVALUE
  !	...	SUB_READ_SPEC
  !---------------------------------------------------------------------
  integer :: qband                  !
  integer :: qant                   !
  integer :: qntch                  !
  real :: datac(qband,qant)         !
  real :: datal(qntch,qant)         !
  integer :: iant                   !
  integer :: iband                  !
  integer :: isub                   !
  integer :: jw1                    !
  integer :: jw2                    !
  integer :: jw3                    !
  real*4 :: w                       !
  complex :: zz(*)                  !
  real*4 :: aa(*)                   !
  real*4 :: ww(*)                   !
  integer :: ny                     !
  ! Global
  real :: tsys_a
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_clic.inc'
  include 'clic_par.inc'
  include 'clic_proc_par.inc'
  ! Local
  integer :: is, i, k, ig, im, jok(mch)
  complex :: z
  real :: fw, afac, a
  logical :: error
  !------------------------------------------------------------------------
  ! Code:
  !
  if (isub.le.mbands) then
    ny = 1
    is = isub
    a = datac(is,iant)
    call ascaling(iant,is,a,afac,error)
    fw = 2*w*r_cfwid(is)/tsys_a(is,iband,iant)/afac**2
    if (r_proc.ne.p_onoff) then
      zz(1) = zz(1) + fw * a
      ww(1) = ww(1) + fw
    elseif (fw.gt.0) then
      zz(1) = zz(1) + fw * a
      ww(1) = ww(1) + fw
    elseif (fw.lt.0) then
      zz(1) = zz(1) + cmplx(0.,-1.) * fw * a
      aa(1) = aa(1) + abs(fw)
    endif
  else
    is = isub-mbands
    ny = r_lnch(is)
    k = r_lich(is)
    call jlimits(ny,jw1,jw2,jw3, jok)
    call jmask(r_lnch(is),nmask(is),cmask(:,:,is),jok)
    do i = 1, ny
      k = k + 1
      a = datal(k,iant)
      call ascaling(iant,is,a,afac,error)
      fw = 2*w*abs(r_lfres(is))/tsys_a(is,iband,iant)*jok(i)   &
        /afac**2
      if (r_proc.ne.p_onoff) then
        zz(i) = zz(i) + fw * a
        if (fw.gt.0) ww(i) = ww(i) + fw
      elseif (fw.gt.0) then
        zz(i) = zz(i) + fw * a
        ww(i) = ww(i) + fw
      elseif (fw.lt.0) then
        zz(i) = zz(i) + cmplx(0.,-1.) * fw * a
        aa(i) = aa(i) + abs(fw)
      endif
    enddo
  endif
  return
end subroutine avalue
!
subroutine zswap (qntch, qsb, qbas, wk1, wk2, wk4, datal)
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  ! CLC
  !     Swap Berkeley correlator unfouriered data into place
  !     (negative delay channels in LSB, Positive in USB)
  !     QNTCH    INTEGER   Number of channels
  !     QSB      INTEGER   Number of sidebands
  !     QBAS     INTEGER   Number of baselines
  !     DATAL(QNTCH,QSB,QBAS)  COMPLEX   Berkeley correlator data
  !     WK1(QNTCH,2,1,QBAS)    COMPLEX
  !     WK2(QNTCH/2,2,2,QBAS)  COMPLEX   Work spaces
  !     WK4(QNTCH/4,2,4,QBAS)  COMPLEX
  ! Call Tree
  !	...	SUB_READ_SPEC
  !---------------------------------------------------------------------
  integer :: qntch                  !
  integer :: qsb                    !
  integer :: qbas                   !
  complex :: wk1(qntch,2,1,qbas)    !
  complex :: wk2(qntch/2,2,2,qbas)  !
  complex :: wk4(qntch/4,2,4,qbas)  !
  complex :: datal(qntch,qsb,qbas)  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  ! Local
  integer :: isub, isb
  integer :: i, ibas, k
  !------------------------------------------------------------------------
  ! Code:
  do ibas = 1, r_nbas
    do isb = 1, 2
      do isub = 1, r_lband
        k = r_lich(isub)+1
        if (r_lband.eq.1) then
          do i=1, r_lnch(isub)
            datal(k,isb,ibas) = wk1(i,isb,isub,ibas)
            k = k + 1
          enddo
        elseif (r_lband.eq.2) then
          do i=1, r_lnch(isub)
            datal(k,isb,ibas) = wk2(i,isb,isub,ibas)
            k = k + 1
          enddo
        elseif (r_lband.eq.4) then
          do i=1, r_lnch(isub)
            datal(k,isb,ibas) = wk4(i,isb,isub,ibas)
            k = k + 1
          enddo
        endif
      enddo
    enddo
  enddo
  return
end subroutine zswap
!
subroutine cont_average (qband,qbas,data_in,spidc,zero,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api  
  !---------------------------------------------------------------------
  ! CLIC
  !      Get the average of continuum channels in each subband
  !      This is used for the scaling of further bandpass calibrations
  !      These averages are stored in ZSBA(isb,ib) (weights in WSBA(isb,ib))
  !      (negative ib for antenna based quantities).
  ! Arguments
  !      INTEGER QBAND	! Number of bands
  !      INTEGER QBAS	! Number of baselines
  !      COMPLEX CONT(QBAND,2,QBAS)	! Usb/Lsb, Band, Base
  !      logical zero     ! initialize average
  !---------------------------------------------------------------------
  integer :: qband                         ! Number of bands
  integer :: qbas                          ! Number of baselines
  integer(kind=address_length) :: data_in  !
  complex :: spidc(qband,2,qbas)
  logical :: zero                          !
  logical :: error                         !
  ! Global
  real :: tsys_b
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_sba.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'gbl_memory.inc'
  ! Local
  ! COMPLEX CONT(QBAND,2,QBAS)   ! Usb/Lsb, Band, Base
  real :: w, wr, afac
  integer :: isb, iba, ich, kr, c_offset,ipola,inbc,iu
  integer :: isb1, isb2, ibnd, ibdata
  logical :: down_channel, down_baseline
  complex :: zz, zzr
  integer(kind=address_length) :: ipkc, kin
  !------------------------------------------------------------------------
  ! Code:
  kin = gag_pointer(data_in,memory)
  if (zero) then
     do isb = 1, 2
        do iba = 1, r_nbas
           zsba(isb,iba) = 0
           wsba(isb,iba) = 0
           do iu = 1, mnbb
              zrsba(iu,isb,iba) = 0
              wrsba(iu,isb,iba) = 0
           enddo
        enddo
     enddo
     zero = .false.
  endif
  !
  ! Compute baseline based quantities, both Absolute and Relative.
  do iba = 1, r_nbas
    if (.not. down_baseline(iba)) then
      call spectral_dump(kr,0,iba)
      ipkc = kin + c_offset(kr)
      !
      do ich = 1, r_nband
        inbc = r_bb(ich)
        !
        ! Number of sidebands.
        if (r_nsb.eq.1) then
           isb1 = r_sband(1,ich)
           isb2 = isb1
           ibnd = 1
        else 
           isb1 = 1
           isb2 = 2
           ibnd = 0
        endif
        do isb=isb1, isb2
          if (ibnd.ne.0) then
            ibdata = ibnd
          else
            ibdata = isb
          endif
          !
          if (.not.down_channel(iba,ich)) then
            call retrieve_datac(qband,r_nsb,qbas,   &
              memory(ipkc),ich,ibdata,iba,zz)
            w = dh_integ*r_cfwid(ich)/tsys_b(ich,isb,iba)
            zzr = zz
            if (do_spidx) zzr = zzr * spidc(ich,isb,iba)
            call scaling (ich,isb,iba,zzr,afac,error)
            if (error) then
              wr = 0
              error = .false.
            else
              wr = w/afac**2
            endif
            ! Absolute values are for all data (TO BE CHANGED)
            ! Relative values are per correlator input
            zsba(isb,iba) = zsba(isb,iba) + zz*w
            zrsba(inbc,isb,iba) = zrsba(inbc,isb,iba) + zzr*wr
            wsba(isb,iba) = wsba(isb,iba) + w
            wrsba(inbc,isb,iba) = wrsba(inbc,isb,iba) + wr
          endif
        enddo
      enddo
    endif
  enddo
  return
end subroutine cont_average
!<FF>
subroutine line_average (qntch,qbas,qsb,data_in,spidl,zero,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api
  !---------------------------------------------------------------------
  ! CLIC
  !      Get the average of line channels in each subband.
  !      Averaging done according to j_average (SET AVERAGING METHOD)
  !      This is used for the scaling of further bandpass calibrations
  !      These averages are stored in ZSBA(isb,ib) (weights in WSBA(isb,ib))
  !      (negative ib for antenna based quantities).
  ! Arguments
  !      INTEGER QNTCH  ! Number of channels
  !      INTEGER QBAS   ! Number of baselines
  !      INTEGER QSB    ! Number of sidebands
  !      COMPLEX CONT(QNTCH,QSB,QBAS)     ! Channels, Usb/Lsb, Base
  !      logical zero     ! initialize average
  !---------------------------------------------------------------------
  integer :: qntch                         ! Number of channels
  integer :: qbas                          ! Number of baselines
  integer :: qsb                           ! Number of sidebands
  integer(kind=address_length) :: data_in  !
  complex :: spidl(qntch,qsb,qbas)         !
  logical :: zero                          !
  logical :: error                         !
  ! Global
  real :: tsys_b
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_sba.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_display.inc'
  include 'gbl_memory.inc'
  ! Local
  real :: w, wr, afac,faz
  integer :: isb, ib, ich, kr, l_offset,ipola,ibb,iu,iif,ifbb(mnbb)
  integer :: i,ic,ny,k,jok(mch),ip
  integer :: isb1, isb2, ibnd, ibdata
  logical :: down_channel, down_baseline
  complex :: zz, zzr, ifsba(mnif,2,mnbas),expim
  real :: aa(2,mnbas),pha,rpha,wifsba(mnif,2,mnbas)
  real :: ifraa(mnif,2,mnbas)
  integer(kind=address_length) :: ipkl, kin
  !------------------------------------------------------------------------
  ! Code:
  kin = gag_pointer(data_in,memory)
  if (zero) then
    do isb = 1, 2
      do ib = 1, r_nbas
        zsba(isb,ib) = 0
        wsba(isb,ib) = 0
        aa(isb,ib) = 0
        do iu = 1, mnbb
          zrsba(iu,isb,ib) = 0
          wrsba(iu,isb,ib) = 0
        enddo
        do iu = 1, mnif
          ifsba(iu,isb,ib) = 0
          wifsba(iu,isb,ib) = 0
          ifraa(iu,isb,ib) = 0
        enddo
       enddo
     enddo
     zero = .false.
  endif
  !
  ! Compute baseline based quantities, both Absolute and Relative.
  !
  !
  do ic=1, r_lband
    ! Number of sidebands.
    if (r_lnsb.eq.1) then
      isb1 = r_lsband(1,ic)
      isb2 = isb1
      ibnd = 1
    else 
      isb1 = 1
      isb2 = 2
      ibnd = 0
    endif
    do isb=isb1, isb2
      if (ibnd.ne.0) then
        ibdata = ibnd
      else
        ibdata = isb
      endif
      ibb = r_bb(ic)
      iif = r_if(ic)
      ifbb(ibb) = iif
      ny = r_lnch(ic)
      k = r_lich(ic)
      call jlimits(ny,0,0,ngibbs,jok)
      call jmask(r_lnch(ic),nmask(ic),cmask(:,:,ic),jok)
      do i = 1, ny
        k = k + 1
        do ib = 1, r_nbas
          if (down_baseline(ib) .or.   &
            down_channel(ib,ic+mbands) .or. jok(i).eq.0) then
            zz = 0
            w = 0
          else
            call spectral_dump(kr,0,ib)
            ipkl = kin + l_offset(kr)
            afac = 1
            call retrieve_datal(qntch,qsb,qbas,   &
              memory(ipkl),k,ibdata,ib,zz)
            zzr = zz
            if (do_spidx) zzr = zzr * spidl(k,isb,ib)
            w = 2*dh_integ*abs(r_lfres(ic))/tsys_b(ic,isb,ib)
            call scaling (ic,isb,ib,zzr,afac,error)
            if (.not.error)  then
              wr = w /afac**2
            else
              w = 0
            endif
            ! Absolute values are for all data (TO BE CHANGED)
            ! Relative values are per correlator input
            zsba(isb,ib) = zsba(isb,ib) + zz*w
            wsba(isb,ib) = wsba(isb,ib) + w
            aa(isb,ib) = aa(isb,ib) + abs(zz)*w
            zrsba(ibb,isb,ib) = zrsba(ibb,isb,ib) + zzr*wr
            wrsba(ibb,isb,ib) = wrsba(ibb,isb,ib) + wr
            ifsba(iif,isb,ib) = ifsba(iif,isb,ib) + zzr*wr
            wifsba(iif,isb,ib) = wifsba(iif,isb,ib) + wr
            ifraa(iif,isb,ib) = ifraa(iif,isb,ib)+abs(zzr)*wr
          endif
        enddo
      enddo
    enddo
  enddo
  !
  ! Method scalar
  if (j_average.ne.1) then
    do ib = 1, r_nbas
      do isb = 1,2
        if (zsba(isb,ib).ne.0) then
          pha = faz(zsba(isb,ib))
          zsba(isb,ib) =   aa(isb,ib) *  &  
             cmplx(cos(pha),sin(pha))
        endif
        do i=1, r_nbb
          ibb = r_mapbb(i)
          iif = ifbb(ibb)
          if (ifsba(iif,isb,ib).ne.0) then
            rpha = faz(ifsba(iif,isb,ib))
            zrsba(ibb,isb,ib) = ifraa(iif,isb,ib) * &
               cmplx(cos(rpha),sin(rpha))
            wrsba(ibb,isb,ib) = wifsba(iif,isb,ib)
          endif
        enddo
      enddo
    enddo
  !
  ! Replace phase, keep amplitude
  else
    do i = 1, r_nbb
       ibb = r_mapbb(i)
       iif = ifbb(ibb)
       do ib = 1, r_nbas
         do isb =1,2
           zrsba(ibb,isb,ib) = ifsba(iif,isb,ib)
           wrsba(ibb,isb,ib) = wifsba(iif,isb,ib)
         enddo
       enddo
    enddo
  endif
  return
end subroutine line_average
!
!      FUNCTION LOG_BASE(IP)
!------------------------------------------------------------------------
!------------------------------------------------------------------------
!      use gildas_def
! Global variables:
!      INCLUDE 'clic_parameter.inc'
!      INCLUDE 'clic_par.inc'
!      INCLUDE 'clic_display.inc'
!      INCLUDE 'clic_number.inc'
! Dummy variables:
!      integer ip
! Local variables:
!------------------------------------------------------------------------
! Code:
! Logical antenna:
!      if (ip.lt.0) then
!         do i=1, r_nant
!            if (r_kant(i).eq.-ip) then
!               log_base = -i
!               return
!!            endif
!         enddo
! Logical baseline:
!      elseif (ip.le.r_nbas) then
!         ip1 = antbas(1,ip)                ! physical antennas
!         ip2 = antbas(2,ip)
!         do i=1, r_nant
!            if (r_kant(i).eq.ip1) then
!               i1 = i
!            endif
!            if (r_kant(i).eq.ip2) then
!               i2 = i
!            endif
!         enddo
!         log_base = basant(min(i1,i2),max(i1,i2))
!* Logical triangle:
!      else
!         ip1 = anttri(1,ip-r_nbas)                ! physical antennas
!         ip2 = anttri(2,ip-r_nbas)
!         ip3 = anttri(3,ip-r_nbas)
!###
!
subroutine get_cont_average (av,ibase)
  use classic_api  
  !---------------------------------------------------------------------
  ! Get the continuum average phases used to average sidebands.
  !---------------------------------------------------------------------
  complex :: av(2)                  !
  integer :: ibase                  !
  ! Global
  real :: faz
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  include 'clic_sba.inc'
  ! Local
  integer :: i, ia, ja, ir
  !--------------------------------------------------------------------
  if (ibase.le.0 .or. ibase.gt.mnbas) then
    write(6,*) 'GET_CONT_AVERAGE_B, ibase =', ibase
    return
  endif
  ia = r_iant(ibase)
  ja = r_jant(ibase)
  ir = r_nrec
  ! Use side band averages from the RF Passband section, or, by default,
  ! the ones in memory.
  do i=1, 2
    ! Phase calibration ON: Each side band should have zero phase
    if (do_phase .or. do_phase_ext) then
      av(i) = 1.
    !
    ! Pass band calibration ON: Take the corresponding averages.
    elseif (do_pass) then
      ! Use the passband in memory:
      if (do_pass_memory .and. sba(i,ibase,ir).ne.0) then
        av(i) = abs(sba(i,ibase,ir))/sba(i,ibase,ir)
      ! or from the file (baseline based):
      elseif (.not. do_pass_antenna .and. r_bpc.ne.0   &
        .and. r_bpcsba(1,i,ibase).ne.0) then
        av(i) = 1./r_bpcsba(1,i,ibase)
      ! or from the file (antenna based):
      elseif (r_abpc.ne.0 .and. r_abpcsba(1,i,ja).ne.0) then
        av(i) = r_abpcsba(1,i,ia)/r_abpcsba(1,i,ja)
      endif
    ! Passband calibration OFF:
    ! Use preferentially info in the file:
    elseif (r_bpc.ne.0 .and. r_bpcsba(1,i,ibase).ne.0) then
      av(i) = 1./r_bpcsba(1,i,ibase)
    elseif (r_abpc.ne.0 .and. r_abpcsba(1,i,ja).ne.0) then
      av(i) = r_abpcsba(1,i,ia)/r_abpcsba(1,i,ja)
    ! otherwise use the rf passband in memory:
    elseif  (abs(sba(i,ibase,ir)).ne.0) then
      av(i) = abs(sba(i,ibase,ir))/sba(i,ibase,ir)
    ! or finally the side band averages of the current observation:
    elseif (zsba(i,ibase).ne.0) then
      av(i) = abs(zsba(i,ibase))/zsba(i,ibase)
    else
      av(i) = 1.0
    endif
  enddo
  return
end subroutine get_cont_average
!
real function tsys_b(isub,iband,ibase)
  use classic_api  
  !---------------------------------------------------------------------
  !     Compute TSYS**2 for a given subband, band, baseline, using the
  !     polarization information
  !---------------------------------------------------------------------
  integer :: isub                   !
  integer :: iband                  !
  integer :: ibase                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  ! Local
  integer :: isig, inbc, jnbc, ia, ja
  !------------------------------------------------------------------------
  ia = r_iant(ibase)
  ja = r_jant(ibase)
  isig = (3-r_sb(isub))/2
  inbc = r_bb(isub)
  jnbc = inbc
! To be checked
!  jnbc = nbc_entry(isub,r_iunit(isub),r_flo2(isub), r_flo2bis(isub),   &
!    r_lpolmode(isub), r_lpolentry(ja,isub))
  if (iband.eq.isig) then
    tsys_b = r_tsyss(inbc,ia)*r_tsyss(jnbc,ja)
  else
    tsys_b = r_tsysi(inbc,ia)*r_tsysi(jnbc,ja)
  endif
  if (tsys_b.le.0) tsys_b = 300**2
end function tsys_b
!
real function tsys_a(isub,iband,iant)
  use classic_api  
  !---------------------------------------------------------------------
  !     Compute TSYS**2 for a given subband, band, antenna, using the
  !     polarization information
  !---------------------------------------------------------------------
  integer :: isub                   !
  integer :: iband                  !
  integer :: iant                   !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  ! Local
  integer :: isig, inbc
  !------------------------------------------------------------------------
  isig = (3-r_sb(isub))/2
  inbc = r_bb(isub)
  if (iband.eq.isig) then
    tsys_a = r_tsyss(inbc,iant)**2
  else
    tsys_a = r_tsysi(inbc,iant)**2
  endif
  if (tsys_a.le.0) tsys_a = 300**2
end function tsys_a
