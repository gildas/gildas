!------------------------------------------------------------------------
! CLIC 	display.inc
!	common variables for graphic display
!------------------------------------------------------------------------
!
      INTEGER MBOX, N_BOXES
#if defined(CLIC64)
      PARAMETER (MBOX=120+8*(MNBAS/4+1))
#else
      PARAMETER (MBOX=240)               ! ie 2*8*(5*6)/2
#endif
      INTEGER N_X                        ! number of diff. x axes
      INTEGER I_X(MBOX)                  ! x_axes
      INTEGER K_X(MBOX)                  ! x_axis pointer for each box
      INTEGER N_Y                        ! number of diff. y axes
      INTEGER I_Y(MBOX)                  ! y_axes
      INTEGER K_Y(MBOX)                  ! y_axis pointer for each box
      INTEGER N_BASE                     ! number of baselines
      INTEGER I_BASE(MBOX)               ! baselines to plot
      INTEGER K_BASE(MBOX)               ! baseline pointer for each box
      INTEGER N_SUBB                     ! number of subband groups
      INTEGER L_SUBB(MBOX)               ! number of subbands in given group
      INTEGER I_SUBB(MBANDS,MBOX)        ! subbs to plot in given group
      INTEGER K_SUBB(MBOX)               ! subb group pointer for each box
      INTEGER N_BAND                     ! number of bands
      INTEGER I_BAND(MBOX)               ! bands to plot
      INTEGER K_BAND(MBOX)               ! band pointer for each box
      INTEGER K_POLAR(MBOX)              ! polar for each box
      INTEGER N_BASEBAND                 ! number of basebands
      INTEGER L_BASEBAND(MBOX)           ! number of basebands groups
      INTEGER I_BASEBAND(MNBB,MBOX)      ! basebands to plot in given group
      INTEGER K_BASEBAND(MBOX)           ! baseband pointer for each box
      INTEGER N_IF                       ! number of if
      INTEGER L_IF(MBOX)                 ! number of if groups
      INTEGER I_IF(MNIF,MBOX)            ! if to plot in given group
      INTEGER K_IF(MBOX)                 ! if pointer for each box
      INTEGER N_STOKES
      INTEGER I_STOKES(MBOX)             ! there can be only one
      INTEGER K_STOKES(MBOX)
! All subbands mode, All baselines mode
      LOGICAL ALL_BASE, ALL_SUBB, EACH_SUBB, EACH_POLAR, ALL_BBAND
      INTEGER POL_SUBB, QUAR_SUBB, NBC_SUBB, WIDEX_SUBB
! Selection criterion
      LOGICAL POL_SELECT, BB_SELECT, SUBB_SELECT, IF_SELECT

! Physical baseline-antennas
      LOGICAL PHYS_BASE
      LOGICAL DO_PHYSICAL
!
! Windows in Line subbands
      CHARACTER*1 SW1(MBOX), SW2(MBOX)
      INTEGER IW1(MBOX),IW2(MBOX)
      REAL FDROP(2)                      ! frac. dropped by def.at both ends
! Channels dropped at center (GIBBS)
      INTEGER NGIBBS
      INTEGER MMASK                      ! Maximum number of mask 
      PARAMETER (MMASK=10)               ! per spw
      INTEGER NMASK(MBANDS)              ! Number of mask in spw
      INTEGER CMASK(2,MMASK,MBANDS)      ! Mask limit
      INTEGER IMASK                      ! 0: blank, 1: extrapolate
! Box
      LOGICAL USER_BOX,MANY_PAGES
      INTEGER USER_BOX_NX, USER_BOX_NY, USER_NBOXES
      CHARACTER*80 C_SETBOX(MBOX)        ! SET BOX command
! Limits
      REAL*4 GU1_X(MBOX), GU2_X(MBOX)    ! x axis limits for var. x
      REAL*4 GU1_Y(MBOX), GU2_Y(MBOX)    ! y axis limits for var. y
      REAL*4 GB1_X(MBOX), GB2_X(MBOX)    ! x axis limits for each box
      REAL*4 GB1_Y(MBOX), GB2_Y(MBOX)    ! y axis limits for each box
      CHARACTER*1 SM_X1(MBOX), SM_X2(MBOX)
      CHARACTER*1 SM_Y1(MBOX), SM_Y2(MBOX)
      CHARACTER*120 C_LIMITS(MBOX)       ! LIMITS command
! Labels
      CHARACTER*40 X_LABEL(MBOX)         ! label x
      CHARACTER*40 Y_LABEL(MBOX)         ! label y
      CHARACTER*40 HEADER_1(MBOX)        ! left top header
      CHARACTER*50 HEADER_2(MBOX)        ! center top header
      CHARACTER*40 HEADER_3(MBOX)        ! right top header
! Default aspect ratio
      REAL*4 ARATIO
! Averaging
      INTEGER I_AVERAGE                  ! 0: no ; 	1: Scan ; 	2: time
      INTEGER J_AVERAGE                  ! 0: 'scalar' 	1: 'Vector'
      INTEGER K_AVERAGE                  ! 0: CONT      1: LINE
      INTEGER S_AVERAGE                  ! many scan average
      REAL*4 T_AVERAGE                   ! time window
! UV Range
      REAL UV_RANGE(2)                   ! min, max radius
! Binning
      LOGICAL DO_BIN
      REAL BIN_POS, BIN_SIZE
! Comments ( for mnant  antennas )
      CHARACTER CBAS(MNBAS)*8, CTRI(MNTRI)*10, CBAND(5)*8  ! CLAB(174)*20,
      CHARACTER CPOL(3)*10
      INTEGER MCSUB
      PARAMETER (MCSUB=136*2)
      CHARACTER CSUB(MCSUB)*4
      CHARACTER QUALITY(0:9)*12
!
      CHARACTER*4 PLOT_TYPE              ! 'POINTS','LINE','HIST','BARS'
      CHARACTER*4 PLOT_MODE              ! 'TIME','SPEC'
! Phases
      LOGICAL DEGREES, CONTINUOUS
      INTEGER N_CONT
! Record loops
      INTEGER M_LOOP
      PARAMETER (M_LOOP=100)
      INTEGER REC_1(M_LOOP), REC_2(M_LOOP), REC_3(M_LOOP), N_LOOP
! Data
      INTEGER N_DATA(MBOX)               ! number of points
      INTEGER M_BOXES                    ! running maximum nb of boxes
      INTEGER M_DATA                     ! running maximum nb of boxes
      INTEGER(KIND=ADDRESS_LENGTH) DATA_X! X coordinate of plot
      INTEGER(KIND=ADDRESS_LENGTH) DATA_Y! Y Value of plot
      INTEGER(KIND=ADDRESS_LENGTH) DATA_W! Weight of plotted point
      INTEGER(KIND=ADDRESS_LENGTH) DATA_Z! Complex visibility of plotted point
      INTEGER(KIND=ADDRESS_LENGTH) DATA_U! Observation number of point
      INTEGER(KIND=ADDRESS_LENGTH) DATA_R! Record number of point
      INTEGER(KIND=ADDRESS_LENGTH) DATA_I! Source Identifier of point
      INTEGER(KIND=ADDRESS_LENGTH) DATA_S! Scratch space
!
!BUG	logical frame			! frame must be drawn
      LOGICAL F_SRES (MBOX)              ! a spline was subtracted
      LOGICAL F_BRES (MBOX)              ! something subtracted
      LOGICAL SORTED                     ! data sorted in each box
      REAL BVAL,EVAL                     ! blanking values
! Identifiers
      INTEGER M_IDENT
      INTEGER N_IDENT
      PARAMETER (M_IDENT=50)             ! different identifiers
      CHARACTER*12 CH_IDENT(M_IDENT)     ! identifiers
! Day offset for UT times
      INTEGER DOBS_0
! Clear Display
      LOGICAL CLEAR
! Plot done
      LOGICAL PLOTTED
! Change Display
      LOGICAL CHANGE_DISPLAY
! Blanking values for plots
      REAL*8 BLANK,D_BLANK
      PARAMETER (BLANK=1.23456D34,D_BLANK=1.23456D30)
!------------------------------------------------------------------------
! Common
      COMMON /CDISPLAY/ SW1, SW2, C_SETBOX, SM_X1, SM_X2, SM_Y1,        &
     &SM_Y2, C_LIMITS, X_LABEL, Y_LABEL, HEADER_1, HEADER_2,            &
     &HEADER_3, CBAS, CBAND, CPOL, CSUB, PLOT_TYPE, PLOT_MODE,          &
     &CH_IDENT, CTRI, QUALITY
      COMMON / DISPLAY/ DATA_X, DATA_Y, DATA_Z, DATA_W, DATA_I,         &
     &DATA_R, DATA_U, DATA_S,                                           &
     &N_BOXES, N_X, I_X, K_X, N_Y, I_Y, K_Y,                            &
     &N_BASE, I_BASE, K_BASE,                                           &
     &N_SUBB, L_SUBB, I_SUBB, K_SUBB,                                   &
     &N_BAND, I_BAND, K_BAND, K_POLAR,                                  &
     &N_BASEBAND,L_BASEBAND,I_BASEBAND,K_BASEBAND,                      &
     &N_IF,L_IF,I_IF,K_IF,                                              &
     &N_STOKES,I_STOKES,K_STOKES,                                       &
     &IW1, IW2, FDROP, NGIBBS,                                          &
     &NMASK,CMASK,IMASK,                                                &
     &GB1_X, GB2_X, GB1_Y, GB2_Y,                                       &
     &GU1_X, GU2_X, GU1_Y, GU2_Y,                                       &
     &REC_1, REC_2, REC_3, N_LOOP,                                      &
     &ARATIO, I_AVERAGE, T_AVERAGE, K_AVERAGE, S_AVERAGE,               &
     &J_AVERAGE, N_CONT, DOBS_0, UV_RANGE,                              &
     &N_IDENT,                                                          &
     &M_DATA,                                                           &
     &N_DATA, M_BOXES,                                                  &
     &F_SRES, F_BRES,                                                   &
     &SORTED,                                                           &
     &BVAL, EVAL,                                                       &
     &CLEAR, CHANGE_DISPLAY, PLOTTED,                                   &
     &DEGREES, CONTINUOUS,                                              &
     &DO_BIN, BIN_POS, BIN_SIZE, PHYS_BASE, ALL_BASE, ALL_SUBB,         &
     &EACH_SUBB, POL_SUBB, QUAR_SUBB, NBC_SUBB, WIDEX_SUBB,             &
     &EACH_POLAR, DO_PHYSICAL, POL_SELECT, BB_SELECT, SUBB_SELECT,      &
     &IF_SELECT, USER_BOX, USER_BOX_NX, USER_BOX_NY, USER_NBOXES,       &
     &MANY_PAGES, ALL_BBAND
!
      SAVE /CDISPLAY/, /DISPLAY/
!------------------------------------------------------------------------
