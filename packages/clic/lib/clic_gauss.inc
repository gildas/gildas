!---------------------------------------------------------------------
      INTEGER MXLINE,MXCAN
      PARAMETER (MXLINE=5,MXCAN=2048)
!
! Ce common contient les valeurs initiales et les codes de dependence
      REAL*4 SPAR(3*MXLINE),DELTAV
      INTEGER KT0,KT1,KT2,KT3,KT4,KT5,                                  &
     &KV0,KV1,KV2,KV3,KV4,KV5,                                          &
     &KD0,KD1,KD2,KD3,KD4,KD5,                                          &
     &NLINE
      COMMON /CGUESS/ NLINE,SPAR,DELTAV,                                &
     &KT0,KT1,KT2,KT3,KT4,KT5,                                          &
     &KV0,KV1,KV2,KV3,KV4,KV5,                                          &
     &KD0,KD1,KD2,KD3,KD4,KD5
!
! Poids
      REAL WFIT
      COMMON /CPOIDS/ WFIT(MXCAN)
!
! Ce common contient les resultats et les erreurs
      INTEGER*4 NGLINE
      REAL*4 PAR(3*MXLINE),ERR(3*MXLINE),SIGBAS,SIGRAI
      COMMON /CRGAUS/ NGLINE,SIGBAS,SIGRAI,PAR,ERR
!	common /crgaus/ rnline,rsigba,rsigra,rnfit
      SAVE /CPOIDS/, /CRGAUS/
!----------------------------------------------------------------------
