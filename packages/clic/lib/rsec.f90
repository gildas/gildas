subroutine rsec(ksec,lsec,sec,error)
  use classic_api
  use clic_file
  !---------------------------------------------------------------------
  !  Read a section in the input observation
  !---------------------------------------------------------------------
  integer(kind=4),intent(in)    :: ksec   ! Section number           
  integer(kind=8),intent(inout) :: lsec   ! Length. In: buffer /Out: section    
  integer(kind=4),intent(out)   :: sec(*) ! Section buffer              
  logical,        intent(out)   :: error  ! Error flag                
  ! Global
  include 'clic_parameter.inc'
  !------------------------------------------------------------------------
  error = .false.
  call classic_entry_section_read(ksec,lsec,sec,e,ibuff,error)
  !
  ! Translate section id to human readable name in case of error
  if (error) then
    if (ksec.le.0 .or. -ksec.le.mnsec) then
      call message(1,2,'RSEC','Absent section '//section(-ksec))
    endif
  endif
  !
end subroutine rsec
!
subroutine wsec(ksec,lsec,sec,error)
  use classic_api
  use clic_file
  !---------------------------------------------------------------------
  !  Write a section in the current output observation
  !---------------------------------------------------------------------
  integer(kind=4),intent(in)    :: ksec   ! Section number           
  integer(kind=8),intent(in)    :: lsec   ! Length. In: buffer /Out: section    
  integer(kind=4),intent(in)    :: sec(*) ! Section buffer              
  logical,        intent(out)   :: error  ! Error flag                
  !
  ! Global
  include 'clic_parameter.inc'
  !------------------------------------------------------------------------
  error = .false.
  !
  if (obuff%lun.ne.o%lun) then
    error = .true.
    call message(8,4,'WSEC',   &
      'Observation not opened for write or modify')
    return
  endif
  !
  ! Call appropriate CLASSIC routine
  if (modify) then
    call classic_entry_section_update(ksec,lsec,sec,e,obuff,error)
  else
    call classic_entry_section_add(ksec,lsec,sec,e,obuff,error)
  endif
  !
  ! Translate section id to human readable name in case of error
  if (error) then
    if (ksec.le.0 .or. -ksec.le.mnsec) then
      call message(1,2,'WSEC','Error writing section '//section(-ksec))
    endif
  endif
  !
end subroutine wsec
!
function fsec(ksec)
  use clic_file
  !---------------------------------------------------------------------
  !  Finds out if a section is present in a given observation
  !---------------------------------------------------------------------
  logical             :: fsec     ! Section present
  integer, intent(in) :: ksec     ! Section number
  ! Local
  integer :: j
  !------------------------------------------------------------------------
  do j=1,e%nsec
    if (ksec.eq.e%seciden(j)) then
      fsec = .true.
      return
    endif
  enddo
  fsec = .false.
end function fsec
!
function lensec(ksec)
  use clic_file
  !---------------------------------------------------------------------
  !  Returns the length of a section, 0 if not present
  !---------------------------------------------------------------------
  integer(kind=8)     :: lensec                 ! Section length 
  integer,intent(in)  :: ksec                   ! Section number
  ! Local
  integer :: j
  !------------------------------------------------------------------------
  do j=1,e%nsec
    if (ksec.eq.e%seciden(j)) then
      lensec = e%secleng(j)
      return
    endif
  enddo
  lensec = 0
end function lensec
