subroutine solve_pass_ant(line,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api  
  use clic_bpc
  !---------------------------------------------------------------------
  !     +	' SOLVE',	'/PLOT',	'/WEIGHT',	'/OUTPUT',
  !     +	'/RESET',	'/OFFSET',	'/COMPRESS',	'/SEARCH',
  !     +	'/BREAK',	'/POLYNOMIAL'
  !	Computes a passband based on the passband calibrator
  !	assumed to be in the current index.
  !---------------------------------------------------------------------
  character(len=*) :: line          !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'gbl_pi.inc'
  include 'clic_sba.inc'
  include 'clic_xy_code.inc'
  include 'gbl_memory.inc'
  include 'clic_number.inc'
  ! Local
  integer :: isb, iba, lb, kb, ib, i, j, nd, ndeg,ipol, inbc, ip
  integer :: ic, ic1, ndeg1, ndeg2, iy, ix, ir, nc
  integer(kind=address_length) :: ipx,ipy,ipw
  integer(kind=address_length) :: data_pass, iwx, iwy, iww, iwi, iwk
  integer :: ignore(mnant),nignore,iarg,narg
  logical :: plot, reset, ok, skip(mbox),ignored, spectrum, crossed 
  character(len=132) :: chain
  integer :: m_pass, old_pen
  data m_pass /0/
  !
  save m_pass, data_pass
  !------------------------------------------------------------------------
  ! Code:
  error = .false.
  old_pen = -1
  !
  spectrum = .false.
  if (sic_present(14,0)) spectrum = .true.
  crossed = .false.
  if (sic_present(15,0)) crossed = .true.
  !
  ! Selecting correlator input
  !
  if (.not.new_receivers) then
    inbc = 1
  else
    if (.not.bb_select.or.n_baseband.ne.1.or.l_baseband(1).ne.1) then
      call show_display('BBAND',.false.,error)
      write(chain,'(A)')   &
        'Select a single baseband using SET BBAND command'
      call message(6,3,'SOLVE_RF',chain)
      error = .true.
      return  
    else
      inbc = i_baseband(1,1)
       write(chain,'(A,I1)')   &
            'Calibrating baseband ',inbc
       call message(6,1,'SOLVE_RF',chain)
    endif
  endif
  !
  ! Options
  reset = sic_present(4,0)
  if (sic_present(2,0) .or. sic_present(3,0) .or.   &
    sic_present(5,0) .or. sic_present(6,0) .or. sic_present(7,0) .or.   &
    sic_present(8,0) .or. sic_present(9,0)) then
    call message(6,3,'SOLVE_RF','Option invalid in this context')
    error = .true.
    return
  endif
  !
  ! Ignore option
  nignore = 0
  nc = 0
  if (sic_present(13,0)) then 
    narg = sic_narg(13)
    if (narg.lt.1) then
      call message(6,3,'SOLVE_RF','/IGNORE option requires argument')
      error = .true.
      return
    endif
    do i = 1, narg
      call sic_i4(line,13,i,iarg,.false.,error)
      if (iarg.gt.mnant.or.iarg.le.0) then
        call message(6,3,'SOLVE_RF','Invalid antenna number')
        error = .true.
        return
      else
        nignore = nignore + 1
        ignore(nignore) = iarg
        write(chain(nc+1:nc+3),'(i2,a)') iarg,' '
        nc = nc+3
      endif
    enddo    
    if (nignore.gt.0) then
       call message(8,1,'SOLVE_RF','Ignoring antenna '//chain(1:nc))
    endif
  endif
  !
  if (i_base(1).gt.mnbas) then
    call message(8,4,'SOLVE_RF','Triangle mode not supported')
    error = .true.
    return
  endif
  ! Get degrees as arguments
  ndeg1 = -1
  call sic_i4(line,0,2,ndeg1,.false.,error)
  if (error) goto 999
  if (ndeg1.gt.mbpcdeg) then
    write(chain,'(i3)') mbpcdeg
    call message(6,2,'SOLVE_RF','Degree reset to '//chain(1:3))
  endif
  ndeg1 = min(ndeg1,mbpcdeg)
  ndeg2 = ndeg1
  call sic_i4(line,0,3,ndeg2,.false.,error)
  if (error) goto 999
  if (ndeg2.gt.mbpcdeg) then
    write(chain,'(i3)') mbpcdeg
    call message(6,2,'SOLVE_RF','Degree reset to '//chain(1:3))
  endif
  ndeg2 = min(ndeg2,mbpcdeg)
  if (ndeg1.lt.0) ndeg1 = 0
  if (ndeg2.lt.0) ndeg2 = 1
  ndeg = max(ndeg1,ndeg2)
  ! force some defaults:
  call get_first(.false.,error)
  if (error) goto 999
  !
  ! all available baselines:
  if (n_base.ne.r_nbas) then
    n_base = r_nbas
    change_display = .true.
  endif
  do i=1, n_base
    if (i_base(i).ne.i) then
      i_base(i) = i
      change_display = .true.
    endif
  enddo
  !
  ! 1 subband group, 1 band at a time:
  if (n_subb.gt.1 .or. n_band.gt.1) then
    call message(4,2,'SOLVE_CAL',   &
      'Only ONE band and ONE group of subbands at a time')
    goto 999
  endif
  ic1 = i_subb(1,k_subb(1))
  isb = i_band(1)
  if (isb.gt.2) then
    call message(4,2,'SOLVE_PASS_ANT',   &
      'Band should Upper, or Lower')
    goto 999
  endif
  !
  call set_display(error)
  if (error) return
  ! Check plot mode: loop on boxes
  ok = .false.
  do ib = 1, n_boxes
    isb = i_band(k_band(ib))
    iba = i_base(k_base(ib))
    kb = k_subb(ib)
    iy = i_y(k_y(ib))
    ix = i_x(k_x(ib))
    lb = l_subb(kb)
    if (isb.gt.2) then
      call message(4,2,'SOLVE_PASS','Band should Upper or Lower')
      skip(ib) = .true.
    elseif (iy.gt.xy_phase) then
      call message(4,2,'SOLVE_PASS',   &
        'Y axis should be Amplitude or Phase')
      skip(ib) = .true.
    elseif (ix.ne.xy_i_fre .and. do_pass_freq) then
      call message(4,2,'SOLVE_PASS',   &
        'X axis should be I_Freq. for frequency-based mode')
      skip(ib) = .true.
    elseif (ix.lt.xy_chann .or. ix.gt.xy_sky_f) then
      call message(4,2,'SOLVE_PASS',   &
        'X axis should be frequency-like for channel-based mode')
      skip(ib) = .true.
    elseif ((lb.ne.1) .and. (i_subb(1,kb).gt.mbands)   &
      .and. (.not. do_pass_freq)) then
      call message(8,4,'SOLVE_PASS',   &
        'Process line subbands separately in channel mode')
      skip(ib) = .true.
    else
      ok = .true.
      skip(ib)=.false.
    endif
  enddo
50 if (.not.ok) then
    error = .true.
    return
  endif

  !
  ! Reset option
  if (reset) then
    do ir=1, mnrec
      do iba = -mnant, mnbas
        do ip = 1, mnbb            ! basebands
          do isb=1, 2
            if (do_pass_freq) then
              bpf_deg(isb,ip,iba,ir) = 10  ! to keep space in header
              fbp_famp(isb,ip,iba,ir) = .false.
              fbp_fpha(isb,ip,iba,ir) = .false.
              do i=1, bpf_deg(isb,ip,iba,ir)
                bp_famp(isb,ip,iba,i,ir)=0
                bp_fpha(isb,ip,iba,i,ir)=0
              enddo
            else
              bpc_band = 0
              fbp_camp(isb,ip,iba,ir) = .false.
              fbp_cpha(isb,ip,iba,ir) = .false.
              do ic=1, mrlband
                bpc_deg(isb,ip,iba,ic,ir) = 4  ! keep space in header
                fbp_lamp(isb,ip,iba,ic,ir) = .false.
                fbp_lpha(isb,ip,iba,ic,ir) = .false.
                do i=1, bpc_deg(isb,ip,iba,ic,ir)
                  bp_lamp(isb,ip,iba,ic,i,ir)=0
                  bp_lpha(isb,ip,iba,ic,i,ir)=0
                enddo
              enddo
            endif
            do ipol=1,3
              ph_fac(isb,ipol,iba,ir) = 0
            enddo
          enddo
        enddo
      enddo
    enddo
    bp_nant(:) = 0
    bp_ant(:,:) = 0
    bp_phys(:,:) = 0
    bp_source = 'UNKNOWN'
    change_display = .true.
  endif
  if (do_pass) then
    call message(8,2,'SOLVE_RF','SET RF_PASSBAND reset to OFF')
    do_pass = .false.
    change_display = .true.
  endif
  !
  if (change_display) then
    call read_spec('ALL',.true.,error) ! all scans in index
    plotted = .false.
    if (error) return
    change_display = .false.
    if (n_base.ne.r_nbas) then
      call message(8,3,'SOLVE_PASS',   &
        'Number of antennas has changed')
      goto 999
    endif
  else
    plotted = .true.
  endif
  plot = sic_present(1,0)
  if (plot) then
    clear = .true.
    if (.not. plotted) call sub_plot('A',.false.,.false.,0,error)
    if (error) return
    plotted = .true.
    old_pen = gr_spen(1)
    call gr_segm('FIT',error)
  endif
  !
  ! Remember antennas.
  if (nignore.gt.0) then
    bp_nant(inbc) = 0
    bp_ant(1:mnant,inbc) = 0
    bp_phys(1:mnant,inbc) = 0
    do i=1, r_nant
      ignored = .false.
      do j = 1, nignore
        if (i.eq.ignore(j)) ignored = .true.
      enddo
      if (ignored) cycle
      bp_nant(inbc) = bp_nant(inbc) + 1
      bp_ant(bp_nant(inbc),inbc) = i
      bp_phys(bp_nant(inbc),inbc) = r_kant(i)
    enddo
  else
    bp_nant(inbc) = r_nant
    do i=1, r_nant
      bp_ant(i,inbc) = i
      bp_phys(i,inbc) = r_kant(i)
    enddo
  endif
  ! Check for less than 3 antennas:
  if (bp_nant(inbc).le.2) then
    call message(6,3,'SOLVE_PASS_ANT',   &
      'Antenna mode cannot be used for 2 antennas only')
    error = .true.
    return
  endif
  !
  ! save the side band averages in complex array sba
  call save_averages(sba,error)
  if (error) goto 999
  !
  ipx = gag_pointer(data_x,memory)
  ipy = gag_pointer(data_y,memory)
  ipw = gag_pointer(data_w,memory)
  !
  ! Frequency-dependent passband:
  ! do a polynomial fit, as a function of IF frequency ...
  if (do_pass_freq.or.spectrum) then
    !
    ! Get auxiliary storage
    nd = n_data((basant(bp_ant(1,inbc),bp_ant(2,inbc))-1)*n_y+1)
    if (nd.eq.0) then
      call message(6,3,'SOLVE_PASS','No valid data point in reference box')
      error = .true.
      return
    endif
    if (nd*(2*r_nbas+3)*2.gt.m_pass) then
      if (m_pass .gt.0) then
        call free_vm(m_pass,data_pass)
      endif
      m_pass = nd*(2*r_nbas+3)*2
      error = sic_getvm(m_pass,data_pass).ne.1
      if (error) goto 999
    endif
    iwx = gag_pointer(data_pass,memory)
    iww = iwx + 2*nd
    iwy = iww + 2*nd*r_nbas
    iwk = iwy + 2*nd*r_nbas
    iwi = iwk + 2*nd           ! This one is INTEGER only ...
    if (spectrum) then
      if (crossed) then
        call solve_pass_ant_spectrum_cross(xy_ampli, m_data, m_boxes, &
          memory(ipx), memory(ipy), memory(ipw),   &
          nd, r_nbas, bp_nant(inbc), &
          memory(iwx), memory(iwy), memory(iww),   &
          memory(iwi), memory(iwk), plot, error)
        if (error) goto 999
        call solve_pass_ant_spectrum_cross(xy_phase, m_data, m_boxes, &
          memory(ipx), memory(ipy), memory(ipw),   &
          nd, r_nbas, bp_nant(inbc), &
          memory(iwx), memory(iwy), memory(iww),   &
          memory(iwi), memory(iwk), plot, error)
        if (error) goto 999

      else 
        call solve_pass_ant_spectrum(xy_ampli, m_data, m_boxes, &
          memory(ipx), memory(ipy), memory(ipw),   &
          nd, r_nbas, bp_nant(inbc), &
          memory(iwx), memory(iwy), memory(iww),   &
          memory(iwi), memory(iwk), plot, error)
        if (error) goto 999
        call solve_pass_ant_spectrum(xy_phase, m_data, m_boxes, &
          memory(ipx), memory(ipy), memory(ipw),   &
          nd, r_nbas, bp_nant(inbc), &
          memory(iwx), memory(iwy), memory(iww),   &
          memory(iwi), memory(iwk), plot, error)
        if (error) goto 999
        !
        ! Compute corresponding passband
        call set_pass_spectrum_ant(isb,inbc,r_nband,r_lnsb,r_nbas+r_ntri,&
                             r_lntch,specc,specl,error)
      endif
    else
      ! Do fit and plot: amplitudes
      call fit_poly_fr_ant(xy_ampli, m_data, m_boxes,   &
        memory(ipx), memory(ipy), memory(ipw),   &
        nd, r_nbas, bp_nant(inbc),   &
        memory(iwx), memory(iwy), memory(iww),   &
        memory(iwi), memory(iwk), plot, ndeg1, error)
      if (error) goto 999
      ! Do fit and plot: phases
      call fit_poly_fr_ant(xy_phase, m_data, m_boxes,   &
        memory(ipx), memory(ipy), memory(ipw),   &
        nd, r_nbas, bp_nant(inbc),   &
        memory(iwx), memory(iwy), memory(iww),   &
        memory(iwi), memory(iwk), plot, ndeg2, error)
      if (error) goto 999
    endif
  !
  ! Channel-based Continuum :
  elseif (ic1.le.mbands) then
    if (ix.ne.xy_chann) then
      call message(4,2,'solve_pass',   &
        'X axis be should Channel Number, '   &
        //'for Continuum Subbands')
      goto 998
    endif
    call channel_cont_ant(xy_ampli, m_data, m_boxes,   &
      memory(ipx), memory(ipy), memory(ipw),error)
    if (error) goto 999
    call channel_cont_ant(xy_phase, m_data, m_boxes,   &
      memory(ipx), memory(ipy), memory(ipw),error)
    if (error) goto 999
  !
  ! Channel-based line : do polynomial fit
  else
    bpc_band = r_lband
    !
    ! Get auxiliary storage
    nd = n_data((basant(bp_ant(1,inbc),bp_ant(2,inbc))-1)*n_y+1)
    if (nd*(2*r_nbas+3)*2.gt.m_pass) then
      if (m_pass .gt.0) then
        call free_vm(m_pass,data_pass)
      endif
      m_pass = nd*(2*r_nbas+3)*2
      error = sic_getvm(m_pass,data_pass).ne.1
      if (error) goto 999
    endif
    iwx = gag_pointer(data_pass,memory)
    iww = iwx + 2*nd
    iwy = iww + 2*nd*r_nbas
    iwk = iwy + 2*nd*r_nbas
    iwi = iwk + 2*nd
    ! Do fit and plot: amplitudes
    call fit_poly_ch_ant(xy_ampli, m_data, m_boxes,   &
      memory(ipx), memory(ipy), memory(ipw),   &
      nd, r_nbas, r_nant,   &
      memory(iwx), memory(iwy), memory(iww),   &
      memory(iwi), memory(iwk), plot, ndeg1, error)
    if (error) goto 999
    ! Do fit and plot: phases
    call fit_poly_ch_ant(xy_phase, m_data, m_boxes,   &
      memory(ipx), memory(ipy), memory(ipw),   &
      nd, r_nbas, r_nant,   &
      memory(iwx), memory(iwy), memory(iww),   &
      memory(iwi), memory(iwk), plot, ndeg2, error)
    if (error) goto 999
  endif
998 continue
  if (old_pen.ge.0) then
    call gr_segm_close(error)
    i = gr_spen(old_pen)
  endif
  bp_source = r_sourc
  return
999 error = .true.
  goto 998
end subroutine solve_pass_ant
!
subroutine save_averages(sbav,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api  
  include 'clic_parameter.inc'
  complex :: sbav(2,-mnant:mnbas,mnrec)  !
  logical :: error                       !
  ! Global
  include 'clic_par.inc'
  include 'clic_clic.inc'
  include 'clic_sba.inc'
  include 'gbl_pi.inc'
  include 'clic_display.inc'
  ! Local
  integer :: ir, isb, iba, ia, isub, inbc, isb1, isb2
  character(len=80) :: chain
  complex :: zbas(mnbas), zant(mnant)
  real :: wbas(mnbas), want(mnant), faz
  !-----------------------------------------------------------------------
  ir = r_nrec
  !
  ! Selecting correlator input
  !
  if (.not.new_receivers) then
    inbc = 1
  else
    inbc = i_baseband(1,1)
  endif
  if (r_lnsb.eq.1) then
    isub = i_subb(1,1)
    if (isub.gt.mbands) isub=isub-mbands
    isb1 = r_lsband(1,isub)
    isb2 = isb1
   else 
     isb1 = 1
     isb2 = 2 
  endif
  !
  ! Number of sidebands.
  do isb=isb1, isb2
    ! absolute
    do iba = 1, r_nbas
      if (wsba(isb,iba).gt.0) then
        zbas(iba) = zsba(isb,iba)/wsba(isb,iba)
      else
        zbas(iba) = 0
      endif
      wbas(iba) = wsba(isb,iba)
    enddo
    call antvisi (zbas,wbas,zant,want,error)
    if (error) return
    do ia = 1, r_nant
      zsba(isb,-ia) = zant(ia)*want(ia)
      wsba(isb,-ia) = want(ia)
    enddo
    ! relative (used later to reference the passband curves).
    do iba = 1, r_nbas
      if (wrsba(inbc,isb,iba).gt.0) then
        zbas(iba) = zrsba(inbc,isb,iba)/wrsba(inbc,isb,iba)
      !
      ! this is wrong, zrsba should NOT be changed.
      !*!               ZRSBA(ISB,IBA) = ZBAS(IBA)
      else
        zbas(iba) = 0
      endif
      wbas(iba) = wrsba(inbc,isb,iba)
    enddo
    call antgain (zbas,wbas,zant,want)
    do ia = 1, r_nant
      zrsba(inbc,isb,-ia) = zant(ia)*want(ia)
      wrsba(inbc,isb,-ia) = want(ia)
    enddo
  enddo
  ! Keep the ABSOLUTE side band averages in SBAV for
  ! use with memory-based passband, and storing in file.
  do iba = -r_nant, r_nbas
    !
    do isb=1,2
      if (wsba(isb,iba).gt.0) then
        sbav(isb,iba,ir) =   &
          zsba(isb,iba)/wsba(isb,iba)
      else
        sbav(isb,iba,ir) = 0
      endif
    enddo
    ! Use the center frequency of quarter (600 MHz) if
    ! only HOR or VER have been selected, else center IF1
    if (do_polar.eq.1.or.do_polar.eq.2) then
      isub = i_subb(1,1)
      ph_fac(1,do_polar,iba,ir) = r_flo1 + (r_flo2(isub)   &
        + r_band2(isub) * (r_flo2bis(isub)+r_band2bis(isub)*600))
      ph_fac(2,do_polar,iba,ir) = r_flo1 - (r_flo2(isub)   &
        + r_band2(isub) * (r_flo2bis(isub)+r_band2bis(isub)*600))
      ph_fac(3,do_polar,iba,ir) = r_flo1
    else
      ph_fac(1,do_polar,iba,ir) = r_flo1+r_fif1
      ph_fac(2,do_polar,iba,ir) = r_flo1-r_fif1
      ph_fac(3,do_polar,iba,ir) = r_flo1
    endif
    if (sbav(2,iba,ir).ne.0 .and. iba.lt.0) then
      write(chain,'(1X,A,I0,A,F8.4,A,F7.2,A)')   &
        'Ant  ',-iba,   &
        ' Ratio (U/L) = ',   &
        abs(sbav(1,iba,ir)/sbav(2,iba,ir)),   &
        ' Phase diff. = ',   &
        faz(sbav(1,iba,ir)/sbav(2,iba,ir))*180./pi,   &
        ' deg.'
      call message(6,1,'SAVE_AV',chain(1:lenc(chain)))
    endif
  enddo
  return
end subroutine save_averages
!
subroutine channel_cont_ant(ly, md, mb, x_data, y_data, w_data,   &
    error)
  use gildas_def
  use classic_api  
  use clic_bpc
  !---------------------------------------------------------------------
  ! CLIC: passband calibration.
  !     store values as channel-by channel passband for continuum
  !     Antenna-based version: LY = 1 (Amp) or 2 (Phases)
  !---------------------------------------------------------------------
  integer :: ly                     !
  integer :: md                     !
  integer :: mb                     !
  real :: x_data(md, mb)            !
  real :: y_data(md, mb)            !
  real :: w_data(md, mb)            !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'gbl_pi.inc'
  include 'clic_sba.inc'
  include 'clic_xy_code.inc'
  ! Local
  integer :: inbc
  integer :: isb, iba, i, ic, iy, lc, ib,  ia, ir
  real :: ynorm, bd(mnbas), faz, ad(mnant), wbd(mnbas), wad(mnant)
  !------------------------------------------------------------------------
  ! Code:
  ! Just store data, normalize amplitudes and phases
  !
  !
  ! Selecting correlator input
  !
  if (.not.new_receivers) then
    inbc = 1
  else
    inbc = i_baseband(1,1)
  endif
  !
  ! Loop on channels
  ir = r_nrec
  do lc = 1, r_nband
    !
    ! Loop on boxes
    do ib = 1, n_boxes
      isb = i_band(k_band(ib))
      iba = i_base(k_base(ib))
      iy = i_y(k_y(ib))
      do i=1, n_data(ib)
        ic = nint(x_data(i,ib))    ! channel number
        if (iy.eq.ly .and. ic.eq.lc) then
          wbd(iba) = w_data(i, ib)
          if (wbd(iba).ne.0) then
            if (iy.eq.xy_ampli) then
              bd(iba) = log(y_data(i,ib))
              wbd(iba) = wbd(iba) *y_data(i, ib)**2
            elseif (iy.eq.xy_phase .and. degrees) then
              bd(iba) = pi/180.*y_data(i,ib)
            elseif (iy.eq.xy_phase .and. .not.degrees) then
              bd(iba) = y_data(i,ib)
            endif
          else
            bd(ib) = 0
          endif
        endif
      enddo
    enddo
    !
    ! save the values
    if (ly.eq.xy_ampli) then
      call ampli_ant(r_nant,r_nbas,bd,wbd,ad,wad,error)
      if (error) goto 999
      do ia=1, r_nant
        iba = -ia
        !
        ! Factor 2 is because bandpass calibration is defined as direct
        ! product of antenna-based calibrations, not as sqrt of the
        ! product (definition of ZRSBA).
        ynorm =   &
          log(abs(zrsba(inbc,isb,iba)/wrsba(inbc,isb,iba)))/2.
        bp_camp(isb,inbc,iba,lc,ir) = ad(ia)-ynorm
        fbp_camp(isb,inbc,iba,ir) = .true.
      enddo
    elseif (ly.eq.xy_phase) then
      call phase_ant(r_nant,r_nbas,bd,wbd,ad,wad,error)
      if (error) goto 999
      do ia=1, r_nant
        iba = -ia
        ynorm = faz(zrsba(inbc,isb,iba))
        bp_cpha(isb,inbc,iba,lc,ir) = ad(ia)-ynorm
        fbp_cpha(isb,inbc,iba,ir) = .true.
      enddo
    endif
  enddo
  return
999 error = .true.
  return
end subroutine channel_cont_ant
!
subroutine phase_ant(nant,nbas,bd,wbd,ad,wad,error)
  use classic_api  
  integer :: nant                   !
  integer :: nbas                   !
  real :: bd(nbas)                  !
  real :: wbd(nbas)                 !
  real :: ad(nant)                  !
  real :: wad(nant)                 !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_par.inc'
  include 'gbl_pi.inc'
  include 'clic_number.inc'
  ! Local
  real :: wb, yi, ww
  real*8 :: wk2(mnant, mnant), wk3(mnbas), norm
  integer :: ib, ia, ir, nantm1, ja, i, zant
  !
  norm = 1e10
  nantm1 = nant - 1
  ir = 1
  do ia=1, r_nant
    ad(ia) = 0
  enddo
  do while (norm.gt.1e-10)
    do ia=1, nant
      do ja=1, nant
        wk2(ia,ja) = 0
      enddo
      wk3(ia) = 0
    enddo
    do ib = 1, r_nbas
      wb = wbd(ib)
      if (wb.gt.0) then
        ia = r_iant(ib)
        ja = r_jant(ib)
        yi = sin(bd(ib) - (ad(ja)-ad(ia)))
        ia = zant(ia,ir)
        ja = zant(ja,ir)
        if (ia.ne.0) then
          wk2(ia,ia) = wk2(ia,ia) + wb
          wk3(ia) = wk3(ia) - wb*yi
        endif
        if (ja.ne.0) then
          wk2(ja,ja) = wk2(ja,ja) + wb
          wk3(ja) = wk3(ja) + wb*yi
        endif
        if (ia.ne.0 .and. ja.ne.0) then
          wk2(ia,ja) = wk2(ia,ja) - wb
          wk2(ja,ia) = wk2(ja,ia) - wb
        endif
      endif
    enddo
    call mth_dpotrf ('PHASE_ANT','U',nantm1,wk2,mnant,error)
    if (error) goto 990
    call mth_dpotrs ('PHASE_ANT',   &
      'U',nantm1,1,wk2,mnant,wk3,nantm1,error)
    if (error) goto 990
    ! Add the result to ad:
    norm = 0
    do ia=1,nant
      i = zant(ia,ir)
      if (i.ne.0) then
        ww = wk3(i)
        ad(ia) = ad(ia) + ww
        norm = norm + ww**2
      endif
    enddo
  enddo
  return
990 error = .true.
  return
end subroutine phase_ant
!
subroutine ampli_ant(nant,nbas,bd,wbd,ad,wad,error)
  use classic_api  
  integer :: nant                   !
  integer :: nbas                   !
  real :: bd(nbas)                  !
  real :: wbd(nbas)                 !
  real :: ad(nant)                  !
  real :: wad(nant)                 !
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_par.inc'
  include 'gbl_pi.inc'
  include 'clic_number.inc'
  ! Local
  real :: wb, yi, ww
  real*8 :: wk2(mnant, mnant), wk3(mnbas)
  integer :: ib, ia, ja
  !
  do ia=1, nant
    do ja=1, nant
      wk2(ia,ja) = 0
    enddo
    wk3(ia) = 0
    ad(ia) = 0
  enddo
  do ib = 1, nbas
    wb = wbd(ib)
    if (wb.gt.0) then
      ia = r_iant(ib)
      ja = r_jant(ib)
      yi = bd(ib) - (ad(ja)+ad(ia))
      wk3(ia) = wk3(ia) + wb*yi
      wk3(ja) = wk3(ja) + wb*yi
      wk2(ia,ia) = wk2(ia,ia) + wb
      wk2(ja,ja) = wk2(ja,ja) + wb
      wk2(ia,ja) = wk2(ia,ja) + wb
      wk2(ja,ia) = wk2(ja,ia) + wb
    endif
  enddo
  call mth_dpotrf ('AMPLI_ANT','U',nant,wk2,mnant,error)
  if (error) goto 990
  call mth_dpotrs ('AMPLI_ANT',   &
    'U',nant,1,wk2,mnant,wk3,r_nant,error)
  if (error) goto 990
  ! Add the result to ad:
  do ia=1,nant
    ww = wk3(ia)
    ad(ia) = ad(ia) + ww
  enddo
  return
990 error = .true.
  return
end subroutine ampli_ant
!
subroutine fit_poly_fr_ant(ly,md,mb,x_data,y_data,w_data,   &
    nd, dbas,nant,xx,yy,ww,ind,wk,plot,ndeg,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api  
  use clic_bpc
  !---------------------------------------------------------------------
  ! CLIC routine: Passband calibration
  !     Fit polynomial as a function of I_F frequency
  !     Antenna-based version
  !---------------------------------------------------------------------
  integer :: ly                     !
  integer :: md                     !
  integer :: mb                     !
  real :: x_data(md,mb)             !
  real :: y_data(md,mb)             !
  real :: w_data(md,mb)             !
  integer :: nd                     !
  integer :: dbas                   !
  integer :: nant                   !
  real*8 :: xx(nd)                  !
  real*8 :: yy(nd,dbas)             !
  real*8 :: ww(nd,dbas)             !
  integer :: ind(nd)                !
  real*8 :: wk(nd)                  !
  logical :: plot                   !
  integer :: ndeg                   !
  logical :: error                  !
  ! Global
  logical :: gr8_random
  include 'gbl_memory.inc'
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'gbl_pi.inc'
  include 'clic_sba.inc'
  include 'clic_xy_code.inc'
  ! Local
  logical :: my_do_rf
  integer :: ib
  integer :: isb, iba, i, l, l1, l2, l3, iy, ix, ia, ja
  integer :: id, j, ir, mad, mad2, inbc, ibas, nbas
  parameter (mad=mnant*(mbpcdeg+1),mad2=mad**2)
  real*8 ::  c(mad), wk1(mbpcdeg+1)
  real*8 ::  aa(0:mbpcdeg), x0, x1, x2, yy1, yy2, ss(mnbas)
  integer :: iant(mnbas),jant(mnbas), ibsl(mnbas)
  !! Original code
  !!      PARAMETER (MAD=MNANT*(MBPCDEG+1),MAD2=MAD**2)
  !!      REAL*8  WK2(MAD*MAD)
  !!      REAL*8  WK3(MAD)
  !!      REAL*8  WSS(MNBAS)
  !! New code uses virtual memory
  integer :: mnad,size
  integer(kind=address_length) :: ipwk2   ! Size 2 * MAD**2
  integer(kind=address_length) :: ipwk3   ! Size 2 * MAD
  integer(kind=address_length) :: ipwss   ! Size 2 * MNBAS
  integer(kind=address_length) :: addr
  integer :: ier, subtract
  !
  real :: wnorm, faz, pha, ynorm(mbox), turn 
  real*8 :: xcurs,ycurs
  character(len=132) :: chain
  !
  real*4 :: rms_err(mbox)
  save rms_err
  !------------------------------------------------------------------------
  ! Code:
  call sic_delvariable ('RMS_ERR',.false.,error)
  call sic_def_real ('RMS_ERR',rms_err,1,n_boxes,.false.,error)
  ir = r_nrec
  !
  ! Correlator input
  ! Selecting correlator input
  !
  if (.not.new_receivers) then
    inbc = 1
  else
    inbc = i_baseband(1,1)
  endif
  iba = 0
  !
  ! Loop on boxes to load data from plot buffers.
  wnorm = 0.0
  do ib = 1, n_boxes
    iy = i_y(k_y(ib))
    if (iy.eq.ly) then
      ix = i_x(k_x(ib))
      isb = i_band(k_band(ib))
      ibas = i_base(k_base(ib))
      !
      ! Deal with ignored antennas
      ia = r_iant(ibas)
      ja = r_jant(ibas)
      if (my_do_rf(ia,inbc).and.my_do_rf(ja,inbc)) then
        iba = iba+1
        ibsl(ibas) = iba
        subtract = 0
        do i = 1, ia
          if (.not.my_do_rf(i,inbc)) subtract = subtract + 1
        enddo 
        ia = ia-subtract
        subtract = 0
        do i = 1, ja
          if (.not.my_do_rf(i,inbc)) subtract = subtract + 1
        enddo 
        ja = ja-subtract
        iant(iba) = ia
        jant(iba) = ja
      else
        cycle
      endif
      !
      nd = n_data(ib)
      if (zrsba(inbc,isb,ibas).ne.0) then
        if (iy.eq.xy_ampli) then
          ynorm(ib) =   &
            log(abs(zrsba(inbc,isb,ibas))/wrsba(inbc,isb,ibas))
        elseif (iy.eq.xy_phase) then
          ynorm(ib) = faz(zrsba(inbc,isb,ibas))
        endif
      else
        ynorm(ib) = -2.
      endif
      do i=1, nd
        xx(i) = x_data(i,ib)
        if (w_data(i,ib).gt.0) then
          if (iy.eq.xy_ampli) then
            yy(i,iba) = log(dble(y_data(i,ib)))
          elseif (iy.eq.xy_phase .and. degrees) then
            yy(i,iba) = y_data(i,ib)*pi/180d0
          else
            yy(i,iba) = y_data(i,ib)
          endif
          if (iy.eq.xy_ampli) then
            ww(i,iba) = w_data(i,ib)*y_data(i,ib)**2
            if (ww(i,iba).lt.2e-6) then
              ww(i,iba) = ww(i,iba)/100.
            endif
            yy(i,iba) = yy(i,iba)-ynorm(ib)
          else
            ww(i,iba) = w_data(i,ib)
            yy(i,iba) = yy(i,iba)-ynorm(ib)
          endif
          wnorm = wnorm + ww(i,iba)
        else
          ww(i,iba) = 0
          if (iy.eq.xy_ampli) then
            yy(i,iba) = -3.5
          elseif (iy.eq.xy_phase) then
            yy(i,iba) = 0
          endif
        endif
      enddo
    endif
  enddo
  nbas = iba
  ! no amplitude or no phase
  if (wnorm.le.0) return
  !
  ! Sort if required:
  if (gr8_random(xx,nd)) then
    call gr8_trie(xx,ind,nd,error)
    if (error) goto 999
    do iba=1,nbas
      call gr8_sort(yy(1,iba),wk,ind,nd)
      call gr8_sort(ww(1,iba),wk,ind,nd)
    enddo
  endif
  !
  mnad = (ndeg+1)*nant
  size = 2 * (nbas + mnad + mnad*mnad)
  ier = sic_getvm(size,addr)
  if (ier.ne.1) then
    call message(8,3,'SOLVE RF','Memory allocation failure')
    error = .true.
    goto 999
  endif
  ipwss = gag_pointer(addr,memory)
  ipwk3 = ipwss + 2*nbas
  ipwk2 = ipwk3 + 2*mnad
  call  polyant(ly, nd, nbas, iant(1:nbas), jant(1:nbas), ref_ant,   &
    ndeg+1, nant, xx, yy, ww,   &
    wk1, memory(ipwk2), memory(ipwk3),   &
    ss, memory(ipwss), c, error)
  call free_vm (size,addr)
  if (error) goto 999
  !
  do ia = 1, nant
    iba = -bp_ant(ia,inbc)
    bp_flim(isb,inbc,iba,1,ir) = xx(1)
    bp_flim(isb,inbc,iba,2,ir) = xx(nd)
    if (ly.eq.xy_ampli) then
      do i = 0, ndeg
        bp_famp(isb,inbc,iba,i,ir)=c(ia+i*nant)
      enddo
      if (ndeg.lt.bpf_deg(isb,inbc,iba,ir)) then
        do i=ndeg+1, bpf_deg(isb,inbc,iba,ir)
          bp_famp(isb,inbc,iba,i,ir) = 0
        enddo
      endif
      fbp_famp(isb,inbc,iba,ir) = .true.
    elseif (ly.eq.xy_phase) then
      do i=0, ndeg
        pha = c(ia+i*nant)
        bp_fpha(isb,inbc,iba,i,ir) = pha
      enddo
      if (ndeg.lt.bpf_deg(isb,inbc,iba,ir)) then
        do i=ndeg+1, bpf_deg(isb,inbc,iba,ir)
          bp_fpha(isb,inbc,iba,i,ir) = 0
        enddo
      endif
      fbp_fpha(isb,inbc,iba,ir) = .true.
    endif
    bpf_deg(isb,inbc,iba,ir) = ndeg
  enddo
  !!      RSS = S(NDEG) ??
  ! display results
  do ib=1, n_boxes
    iy = i_y(k_y(ib))
    if (iy.eq.ly) then
      i = i_base(k_base(ib))
      ia = r_iant(i)
      ja = r_jant(i)
      if (my_do_rf(ia,inbc).and.my_do_rf(ja,inbc)) then
        iba = ibsl(i_base(k_base(ib)))
        l = lenc(y_label(ib))
        l1 = max(3,lenc(header_1(ib)))   ! saves space on listing
        l2 = lenc(header_2(ib))
        l3 = lenc(header_3(ib))
        if (iy.eq.xy_phase .and. degrees) then
          ss(iba) = ss(iba)*180d0/pi
          rms_err(ib) = ss(iba)
        else
          rms_err(ib) = ss(iba)*100.0
        endif
        !
        write(chain,1000) y_label(ib)(1:3), header_1(ib)(1:l1),   &
          header_2(ib)(1:l2), header_3(ib)(1:l3), ss(iba)
1000    format(a,'. ',a,1x,' ',a,' ',a,1x,   &
          'rms: ',1pg11.4)
        call message(6,1,'SOLVE_RF',chain(1:lenc(chain)))
      endif
    endif
  enddo
  !
  if (plot) then
    x0 = xx(1)
    x1 = xx(nd)
    do ib=1, n_boxes
      iy = i_y(k_y(ib))
      if (iy.eq.ly) then
        iba = i_base(k_base(ib))
        isb = i_band(k_band(ib))
        ia = r_iant(iba)
        ja = r_jant(iba)
        if (my_do_rf(ia,inbc).and.my_do_rf(ja,inbc)) then
           do i=1, nd
            x2 = min(1d0,max(-1d0,(2d0*i-1d0-nd)/(nd-1d0)))
            if (ly.eq.xy_ampli) then
              do id=0, ndeg
                aa(id) = bp_famp(isb,inbc,-ia,id,ir)
              enddo
            elseif (ly.eq.xy_phase) then
              do id=0, ndeg
                aa(id) = bp_fpha(isb,inbc,-ia,id,ir)
              enddo
            endif
            call mth_getpol('FIT_POLYC',   &
              ndeg+1, aa, x2, yy1, error)
            if (error) goto 999
            if (ly.eq.xy_ampli) then
              do id=0, ndeg
                aa(id) = bp_famp(isb,inbc,-ja,id,ir)
              enddo
            elseif (ly.eq.xy_phase) then
              do id=0, ndeg
                aa(id) = bp_fpha(isb,inbc,-ja,id,ir)
              enddo
            endif
            call mth_getpol('FIT_POLYC',   &
              ndeg+1, aa, x2, yy2, error)
            if (error) goto 999
            xx(i) = (x0*(1-x2)+x1*(1+x2))/2
            if (ly.eq.xy_ampli) then
              yy(i,1) = exp(yy1+yy2+ynorm(ib))
            elseif (ly.eq.xy_phase) then
              yy(i,1) =-yy1+yy2+ynorm(ib)
              if (degrees) then
                yy(i,1) = yy(i,1)*180d0/pi
              endif
            endif
          enddo
          write(chain,'(I4.4)') ib
          call gr_execl('CHANGE DIRECTORY BOX'//chain)
          error = gr_error()
          if (error) goto 999
          if (ly.eq.xy_phase) then
            turn = 2d0*pi
            if (degrees) turn = 360d0
            call phi_plot(gb1_y(ib),gb2_y(ib),   &
              nd,xx,yy, turn)
          else
            call gr8_connect (nd,xx,yy,0.0d0,-1.0d0)
          endif
          call gr_execl('CHANGE DIRECTORY')
        else
          write(chain,'(I4.4)') ib
          call gr_execl('CHANGE DIRECTORY BOX'//chain)
          xcurs = 0.5*(gb2_x(ib)+gb1_x(ib))
          ycurs = 0.5*(gb2_y(ib)+gb1_y(ib))
          call relocate(xcurs,ycurs)
!          call putlabel(7,"IGNORED",5)
          call gr_execl('CHANGE DIRECTORY')
        endif
      endif
    enddo
  endif
  return
  !
999 error = .true.
  return
end subroutine fit_poly_fr_ant
!
subroutine phi_plot(g1,g2,nd,xx,yy,turn)
  real :: g1                        !
  real :: g2                        !
  integer :: nd                     !
  real*8 :: xx(nd)                  !
  real*8 :: yy(nd)                  !
  real :: turn                      !
  include 'clic_parameter.inc'
  ! Local
  integer :: k, i
  real*8 :: ymin, ymax
  real :: gmin, gmax
  !
  ymax = -1e30
  ymin = 1e30
  do i=1, nd
    if (yy(i).ne.blank8) then
      ymax = max(ymax,yy(i))
      ymin = min(ymin,yy(i))
    endif
  enddo
  call gr8_connect (nd,xx,yy,blank8,1.0d0)
  gmax = max(g1,g2)
  gmin = min(g1,g2)
  k = int((gmin-ymax)/turn+11.)-10
  do i=1, nd
    if (yy(i).ne.blank8) yy(i) = yy(i)+(k-1)*turn
  enddo
  do while (ymin+k*turn.lt.gmax)
    do i=1, nd
      if (yy(i).ne.blank8) yy(i) = yy(i)+turn
    enddo
    call gr8_connect (nd,xx,yy,blank8,1.0d0)
    k = k+1
  enddo
  do i=1, nd
    yy(i) = yy(i)-k*turn
  enddo
  return
end subroutine phi_plot
!
!
subroutine fit_poly_ch_ant(ly,md,mb,x_data,y_data,w_data,   &
    nd, nbas,nant,xx,yy,ww,ind,wk,plot,ndeg,error)
  use gildas_def
  use gkernel_interfaces
  use classic_api  
  use clic_bpc
  !---------------------------------------------------------------------
  ! CLIC routine: Passband calibration
  !     Fit polynomial as a function frequency-like variable in an individual
  !     line subband.
  !     Antenna-based version
  !---------------------------------------------------------------------
  integer :: ly                     !
  integer :: md                     !
  integer :: mb                     !
  real :: x_data(md,mb)             !
  real :: y_data(md,mb)             !
  real :: w_data(md,mb)             !
  integer :: nd                     !
  integer :: nbas                   !
  integer :: nant                   !
  real*8 :: xx(nd)                  !
  real*8 :: yy(nd,nbas)             !
  real*8 :: ww(nd,nbas)             !
  integer :: ind(nd)                !
  real*8 :: wk(nd)                  !
  logical :: plot                   !
  integer :: ndeg                   !
  logical :: error                  !
  ! Global
  logical :: gr8_random
  include 'gbl_memory.inc'
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_par.inc'
  include 'clic_display.inc'
  include 'gbl_pi.inc'
  include 'clic_sba.inc'
  include 'clic_xy_code.inc'
  ! Local
  logical :: my_do_rf
  integer :: ib, ia, ja
  integer :: isb, iba, i, l, l1, l2, l3, ic, iy, ix
  integer :: mad, mad2, id, ir, inbc, ibas
  parameter (mad=mnant*(mbpcdeg+1),mad2=mad**2)
  real*8 ::  c(mad), wk1(mbpcdeg+1)
  real*8 ::  yy1, yy2, ss(mnbas), aa(0:mbpcdeg), x0, x1, x2
  integer :: iant(mnbas),jant(mnbas), ibsl(mnbas)
  !! Original code
  !!      PARAMETER (MAD=MNANT*(MBPCDEG+1),MAD2=MAD**2)
  !!      REAL*8  WK2(MAD*MAD)
  !!      REAL*8  WK3(MAD)
  !!      REAL*8  WSS(MNBAS)
  !! New code uses virtual memory
  integer :: mnad,size
  integer(kind=address_length) :: ipwk2   ! Size 2 * MAD**2
  integer(kind=address_length) :: ipwk3   ! Size 2 * MAD
  integer(kind=address_length) :: ipwss   ! Size 2 * MNBAS
  integer(kind=address_length) :: addr
  integer :: ier, subtract
  real :: wnorm, faz, pha, xr, xv, xd, ynorm(mbox), turn
  !
  character(len=132) :: chain
  !------------------------------------------------------------------------
  ! Code:
  ir = r_nrec
  !
  ! Selecting correlator input
  !
  if (.not.new_receivers) then
    inbc = 1
  else
    inbc = i_baseband(1,1)
  endif
  iba = 0
  wnorm = 0.0
  !
  ! Loop on boxes to load data from plot buffers.
  do ib = 1, n_boxes
    iy = i_y(k_y(ib))
    if (iy.eq.ly) then
      ix = i_x(k_x(ib))
      isb = i_band(k_band(ib))
      ic = i_subb(1,k_subb(ib))-mbands
      ibas = i_base(k_base(ib))
      !
      ! Deal with ignored antennas
      ia = r_iant(ibas)
      ja = r_jant(ibas)
      if (my_do_rf(ia,inbc).and.my_do_rf(ja,inbc)) then
        iba = iba+1
        ibsl(ibas) = iba
        subtract = 0
        do i = 1, ia
          if (.not.my_do_rf(i,inbc)) subtract = subtract + 1
        enddo
        ia = ia-subtract
        subtract = 0
        do i = 1, ja
          if (.not.my_do_rf(i,inbc)) subtract = subtract + 1
        enddo
        ja = ja-subtract
        iant(iba) = ia
        jant(iba) = ja
      else
        cycle
      endif
      !
      nd = n_data(ib)
      if (iy.eq.xy_ampli) then
        ynorm(ib) =   &
          log(abs(zrsba(inbc,isb,ibas))/wrsba(inbc,isb,ibas))
      elseif (iy.eq.xy_phase) then
        ynorm(ib) = faz(zrsba(inbc,isb,ibas))
      endif
      call getxvall(ix,isb,ic,xr,xv,xd)
      do i=1, nd
        xx(i) = nint((x_data(i,ib)-xv)/xd+xr)+r_lich(ic)
        if (iy.eq.xy_ampli) then
          yy(i,iba) = log(dble(y_data(i,ib)))
        elseif (iy.eq.xy_phase .and. degrees) then
          yy(i,iba) = y_data(i,ib)*pi/180d0
        else
          yy(i,iba) = y_data(i,ib)
        endif
        if (w_data(i,ib).le.0) then
          ww(i,iba) = 0
          yy(i,iba) = 0
        elseif (iy.eq.xy_ampli) then
          ww(i,iba) = w_data(i,ib)*y_data(i,ib)**2
          yy(i,iba) = yy(i,iba)-ynorm(ib)
        else
          ww(i,iba) = w_data(i,ib)
          yy(i,iba) = yy(i,iba)-ynorm(ib)
        endif
        wnorm = wnorm + ww(i,iba)
      enddo
    !            DO I=1, ND
    !               WW(I,IBA) = WW(I,IBA)/WNORM
    !            ENDDO
    endif
  enddo
  nbas = iba
  nant = bp_nant(inbc)
  !
  if (wnorm.le.0) return
  !
  ! Sort if required:
  if (gr8_random(xx,nd)) then
    call gr8_trie(xx,ind,nd,error)
    if (error) goto 999
    do iba=1,nbas
      call gr8_sort(yy(1,iba),wk,ind,nd)
      call gr8_sort(ww(1,iba),wk,ind,nd)
    enddo
  endif
  !
  mnad = (ndeg+1)*nant
  size = 2 * (nbas + mnad + mnad*mnad)
  ier = sic_getvm(size,addr)
  if (ier.ne.1) then
    call message(8,3,'SOLVE RF','Memory allocation failure')
    error = .true.
    goto 999
  endif
  ipwss = gag_pointer(addr,memory)
  ipwk3 = ipwss + 2 * nbas
  ipwk2 = ipwk3 + 2 * mnad
  call  polyant(ly, nd, nbas, iant, jant, ref_ant,   &
    ndeg+1, nant, xx, yy, ww,   &
    wk1, memory(ipwk2), memory(ipwk3),   &
    ss, memory(ipwss),  c, error)
  call free_vm(size,addr)
  if (error) goto 999
  !
  do ia = 1, nant
    iba = -bp_ant(ia,inbc)
    if (ly.eq.xy_ampli) then
      do i = 0, ndeg
        bp_lamp(isb,inbc,iba,ic,i,ir)=c(ia+i*nant)
      enddo
      if (ndeg.lt.bpc_deg(isb,inbc,iba,ic,ir)) then
        do i=ndeg+1, bpc_deg(isb,inbc,iba,ic,ir)
          bp_lamp(isb,inbc,iba,ic,i,ir) = 0
        enddo
      endif
      fbp_lamp(isb,inbc,iba,ic,ir) = .true.
    elseif (ly.eq.xy_phase) then
      do i=0, ndeg
        pha = c(ia+i*nant)
        bp_lpha(isb,inbc,iba,ic,i,ir) = pha
      enddo
      if (ndeg.lt.bpc_deg(isb,inbc,iba,ic,ir)) then
        do i=ndeg+1, bpc_deg(isb,inbc,iba,ic,ir)
          bp_lpha(isb,inbc,iba,ic,i,ir) = 0
        enddo
      endif
      bpc_deg(isb,inbc,iba,ic,ir) = ndeg
      fbp_lpha(isb,inbc,iba,ic,ir) = .true.
    endif
  enddo
  ! display results
  do ib=1, n_boxes
    iy = i_y(k_y(ib))
    if (iy.eq.ly) then
      ic = i_subb(1,k_subb(ib))-mbands
      iba = ibsl(i_base(k_base(ib)))
      l = lenc(y_label(ib))
      l1 = max(3,lenc(header_1(ib)))
      l2 = lenc(header_2(ib))
      l3 = lenc(header_3(ib))
      if (iy.eq.xy_phase .and. degrees) then
        ss(iba) = ss(iba)*180d0/pi
      endif
      !
      write(chain,1000) y_label(ib)(1:3), header_1(ib)(1:l1),   &
        header_2(ib)(1:l2), header_3(ib)(1:l3), ss(iba)
1000  format(a,'. ',a,1x,' ',a,' ',a,1x,   &
        'rms: ',1pg11.4)
      call message(6,1,'SOLVE_RF',chain(1:lenc(chain)))
    endif
  enddo
  !
  if (plot) then
    x0 = xx(1)
    x1 = xx(nd)
    do ib=1, n_boxes
      iy = i_y(k_y(ib))
      if (iy.eq.ly) then
        iba = i_base(k_base(ib))
        isb = i_band(k_band(ib))
        ic = i_subb(1,k_subb(ib))-mbands
        ia = r_iant(iba)
        ja = r_jant(iba)
        if (my_do_rf(ia,inbc).and.my_do_rf(ja,inbc)) then
          call getxvall(ix,isb,ic,xr,xv,xd)
          do i=1, nd
            x2 = min(1d0,max(-1d0,(2d0*i-1d0-nd)/(nd-1d0)))
            if (ly.eq.xy_ampli) then
              do id=0, ndeg
                aa(id) = bp_lamp(isb,inbc,-ia,ic,id,ir)
              enddo
            elseif (ly.eq.xy_phase) then
              do id=0, ndeg
                aa(id) = bp_lpha(isb,inbc,-ia,ic,id,ir)
              enddo
            endif
            call mth_getpol('FIT_POLYC',   &
              ndeg+1, aa, x2, yy1, error)
            if (error) goto 999
            if (ly.eq.xy_ampli) then
              do id=0, ndeg
                aa(id) = bp_lamp(isb,inbc,-ja,ic,id,ir)
              enddo
            elseif (ly.eq.xy_phase) then
              do id=0, ndeg
                aa(id) = bp_lpha(isb,inbc,-ja,ic,id,ir)
              enddo
            endif
            call mth_getpol('FIT_POLYC',   &
              ndeg+1, aa, x2, yy2, error)
            if (error) goto 999
            xx(i) = (x0*(1-x2)+x1*(1+x2))/2
            xx(i) = (xx(i)-r_lich(ic)-xr)*xd+xv
            if (ly.eq.xy_ampli) then
              yy(i,1) = exp(yy1+yy2+ynorm(ib))
            elseif (ly.eq.xy_phase) then
              yy(i,1) =-yy1+yy2+ynorm(ib)
              if (degrees) then
                yy(i,1) = yy(i,1)*180d0/pi
              endif
            endif
          enddo
          write(chain,'(I4.4)') ib
          call gr_execl('CHANGE DIRECTORY BOX'//chain)
          error = gr_error()
          if (error) goto 999
          if (ly.eq.xy_phase) then
            turn = 2d0*pi
            if (degrees) turn = 360d0
            call phi_plot(gb1_y(ib),gb2_y(ib),   &
              nd,xx,yy, turn)
          else
            call gr8_connect (nd,xx,yy,0.0d0,-1.0d0)
          endif
          call gr_execl('CHANGE DIRECTORY')
        endif
      endif
    enddo
  endif
  return
999 error = .true.
  return
end subroutine fit_poly_ch_ant
!
subroutine antvisi (z,w,zant,want,error)
  use gildas_def
  use classic_api  
  !---------------------------------------------------------------------
  ! CLIC
  !     Derive antenna "gains" from baseline visibilities
  ! Arguments:
  !     Z(MNBAS)    COMPLEX   Visibility
  !     W(MNBAS)    REAL      Weight
  !     ZANT(MNANT) COMPLEX   Complex antenna gain
  !     WANT(MNANT) REAL      Weight
  ! Call Tree
  !	...	GAIN_CONT
  !	...	GAIN_LINE
  !	...	ARECORD
  !	...	CONT_AVERAGE
  !	...	SUB_SOLVE_PASS
  !---------------------------------------------------------------------
  include 'clic_parameter.inc'
  complex :: z(mnbas)               !
  real*4 :: w(mnbas)                !
  complex :: zant(mnant)            !
  real*4 :: want(mnant)             !
  logical :: error                  !
  ! Global
  include 'gbl_pi.inc'
  include 'clic_constant.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  ! Local
  real :: pha(mnant), amp(mnant), faz
  integer :: ib, ia
  real :: bd(mnbas), wad(mnant)
  !------------------------------------------------------------------------
  ! Code:
  !
  ! Solve for phases
  do ib=1, r_nbas
    if (w(ib).ne.0) then
      bd(ib) = faz(z(ib))
    else
      bd(ib) = 0.
    endif
  enddo
  call phase_ant(r_nant,r_nbas,bd,w,pha,wad,error)
  if (error) goto 999
  !
  ! Solve for amplitudes
  do ib=1, r_nbas
    if (w(ib).ne.0) then
      bd(ib) = log(abs(z(ib)))
    else
      bd(ib) = -3.
    endif
  enddo
  if (r_nbas.gt.1) then
    call ampli_ant(r_nant,r_nbas,bd,w,amp,wad,error)
    if (error) goto 999
  else
    amp(1) = bd(1)/2.
    amp(2) = bd(1)/2.
  endif
  do ia=1, r_nant
    zant(ia) = exp(cmplx(amp(ia),pha(ia)))
    want(ia) = 1.0             !?????!?????!
  enddo
  return
999 error = .true.
end subroutine antvisi
!
function my_do_rf(iant,inbc)
  use clic_bpc
  !
  logical :: my_do_rf
  integer :: iant
  integer :: inbc 
  !
  include 'clic_parameter.inc'
  !
  integer :: i
  !
  my_do_rf = .false.
  if (iant.le.0.or.iant.gt.mnant) return
  do i=1, bp_nant(inbc)
    if (bp_ant(i,inbc).eq.iant) then
      my_do_rf = .true.
      return
    endif
  enddo
  return
end function my_do_rf
!
subroutine solve_pass_ant_spectrum(ly,md,mb,x_data,y_data,w_data,   &
    nd, dbas,nant,xx,yy,ww,ind,wk,plot,error)
  use gildas_def
  use gkernel_interfaces
  use clic_bpc
  !---------------------------------------------------------------------
  ! CLIC routine: Passband calibration
  !     Derive antenna gain, that will be interpolated later
  !---------------------------------------------------------------------
  integer :: ly                     !
  integer :: md                     !
  integer :: mb                     !
  real :: x_data(md,mb)             !
  real :: y_data(md,mb)             !
  real :: w_data(md,mb)             !
  integer :: nd                     !
  integer :: dbas                   !
  integer :: nant                   !
  real*8 :: xx(nd)                  !
  real*8 :: yy(dbas,nd)             !
  real*8 :: ww(dbas,nd)             !
  integer :: ind(nd)                !
  real*8 :: wk(nd)                  !
  logical :: plot
  logical :: error                  !
  ! Global
  logical :: gr8_random
  include 'gbl_memory.inc'
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'gbl_pi.inc'
  include 'clic_sba.inc'
  include 'clic_xy_code.inc'
  ! Local
  logical :: my_do_rf, counting
  integer :: ib
  integer :: isb, iba, i, l, l1, l2, l3, iy, ix, ia, ja
  integer :: id, j, ir, mad, mad2, inbc, ibas, nbas, mynd
  parameter (mad=mnant*(mbpcdeg+1),mad2=mad**2)
  real*8 ::  c(mad), wk1(mbpcdeg+1), xxx(m_data)
  real*8 ::  aa(0:mbpcdeg), yy1, yy2, ss(mnbas),yyy(m_data)
  integer :: iant(mnbas),jant(mnbas), ibsl(mnbas)
  !
  integer :: ier, subtract, nhole, ifirst(mmask),ilast(mmask)
  real :: wnorm, faz, pha, ynorm(mbox), turn, ad(mnant), wad(mnant) 
  real :: yp,ym, delta
  real*8 :: xcurs,ycurs
  character(len=132) :: chain
  !
  real*4 :: rms_err(mbox)
  save rms_err
  !------------------------------------------------------------------------
  !
  ! Correlator input
  ! Selecting correlator input
  !
  if (.not.new_receivers) then
    inbc = 1
  else
    inbc = i_baseband(1,1)
  endif
  iba = 0
  !
  ! Loop on boxes to load data from plot buffers.
  wnorm = 0.0
  do ib = 1, n_boxes
    iy = i_y(k_y(ib))
    if (iy.eq.ly) then
      ix = i_x(k_x(ib))
      isb = i_band(k_band(ib))
      ibas = i_base(k_base(ib))
      !
      ! Deal with ignored antennas
      ia = r_iant(ibas)
      ja = r_jant(ibas)
      if (my_do_rf(ia,inbc).and.my_do_rf(ja,inbc)) then
        iba = iba+1
        ibsl(ibas) = iba
        subtract = 0
        do i = 1, ia
          if (.not.my_do_rf(i,inbc)) subtract = subtract + 1
        enddo
        ia = ia-subtract
        subtract = 0
        do i = 1, ja
          if (.not.my_do_rf(i,inbc)) subtract = subtract + 1
        enddo
        ja = ja-subtract
        iant(iba) = ia
        jant(iba) = ja
      else
        cycle
      endif
      !
      nd = n_data(ib)
      if (zrsba(inbc,isb,ibas).ne.0) then
        if (iy.eq.xy_ampli) then
          ynorm(ib) =   &
            log(abs(zrsba(inbc,isb,ibas))/wrsba(inbc,isb,ibas))
        elseif (iy.eq.xy_phase) then
          ynorm(ib) = faz(zrsba(inbc,isb,ibas))
        endif
      else
        ynorm(ib) = -2.
      endif
      do i=1, nd
        xx(i) = x_data(i,ib)
        if (w_data(i,ib).gt.0) then
          if (iy.eq.xy_ampli) then
            yy(iba,i) = log(dble(y_data(i,ib)))
          elseif (iy.eq.xy_phase .and. degrees) then
            yy(iba,i) = y_data(i,ib)*pi/180d0
          else
            yy(iba,i) = y_data(i,ib)
          endif
          if (iy.eq.xy_ampli) then
            ww(iba,i) = w_data(i,ib)*y_data(i,ib)**2
            if (ww(iba,i).lt.2e-6) then
              ww(iba,i) = ww(iba,i)/100.
            endif
            yy(iba,i) = yy(iba,i)-ynorm(ib)
          else
            ww(iba,i) = w_data(i,ib)
            yy(iba,i) = yy(iba,i)-ynorm(ib)
          endif
          wnorm = wnorm + ww(i,iba)
        else
          ww(iba,i) = 0
          if (iy.eq.xy_ampli) then
            yy(iba,i) = -3.5
          elseif (iy.eq.xy_phase) then
            yy(iba,i) = 0
          endif
        endif
      enddo
    endif
  enddo
  nbas = iba
  ! no amplitude or no phase
  if (wnorm.le.0) return
  !
  ! Sort if required:
!! if needed, do it before transposition
!!  if (gr8_random(xx,nd)) then
!!    call gr8_trie(xx,ind,nd,error)
!!    if (error) goto 999
!!    do iba=1,nbas
!!      call gr8_sort(yy(1,iba),wk,ind,nd)
!!      call gr8_sort(ww(1,iba),wk,ind,nd)
!!    enddo
!!  endif
  !
  do i = 1, nd
    ad = 0 
    wad = 0
    if (ly.eq.xy_ampli) then
      if (all(ww(:,i).gt.0)) then
        call ampli_ant(nant,nbas,real(yy(:,i),kind=4),real(ww(:,i),kind=4),&
                        ad,wad,error)
      else
      endif
      if (error) goto 999
      do ia =1,  nant
        iba = -bp_ant(ia,inbc)
        bp_samp(isb,inbc,iba,i) = ad(ia)
        fbp_samp(isb,inbc,iba) = .true.
      enddo
    elseif (ly.eq.xy_phase) then
      if (all(ww(:,i).gt.0)) then
        call phase_ant(nant,nbas,real(yy(:,i),kind=4),real(ww(:,i),kind=4), &
                      ad,wad,error)
      else
      endif
      if (error) goto 999
      do ia =1,  nant
        iba = -bp_ant(ia,inbc)
        bp_spha(isb,inbc,iba,i) = ad(ia)
        fbp_spha(isb,inbc,iba) = .true.
      enddo
    endif
  enddo
  ! Set number of points and frequencies
  ! Compute "holes" in spectrum and interpolate
  nhole = 0
  counting = .false.
  if (ly.eq.xy_ampli) then
    !
    ! Count holes
    do i=1,nd
      bp_fsamp(isb,inbc,i) = xx(i)
      if (.not.counting) then
        if (bp_samp(isb,inbc,-1,i).eq.0) then
          nhole = nhole + 1
          ifirst(nhole) = i
          counting  = .true.
        endif
      else
        if (bp_samp(isb,inbc,-1,i).ne.0) then
          counting  = .false.
          ilast(nhole) = i-1
        endif
      endif
    enddo
    if (counting) ilast(nhole)=nd
    !
    ! Fill holes
    do i=1, nhole
      do ia =1, nant
        iba = -bp_ant(ia,inbc)
        ym = 0
        yp = 0
        if (ifirst(i).gt.1) then
          ym=bp_samp(isb,inbc,iba,ifirst(i)-1)
        endif
        if (ilast(i).lt.nd) then
          yp=bp_samp(isb,inbc,iba,ilast(i)+1)
        endif
        if (ilast(i).ne.(ifirst(i)-1)) then
          delta = (yp-ym)/(ilast(i)-ifirst(i)+2)
        endif
        if (yp.ne.0.and.ym.ne.0) then
          do j=ifirst(i),ilast(i)
            if (imask.eq.1) then
              bp_samp(isb,inbc,iba,j) = bp_samp(isb,inbc,iba,ifirst(i)-1) &
                                     +(j-ifirst(i)+1)*delta
            else
              bp_samp(isb,inbc,iba,j) = blank4
            endif
          enddo
        else
        endif
      enddo
    enddo
    !
    ! Add an extra extrapolated point
    mynd = nd+1
    bp_fsamp(isb,inbc,mynd) = 2*bp_fsamp(isb,inbc,mynd-1)&
                               -bp_fsamp(isb,inbc,mynd-2)
    do ia =1, nant
      iba = -bp_ant(ia,inbc)
      if (bp_samp(isb,inbc,iba,mynd-1).ne.blank4.and. &
          bp_samp(isb,inbc,iba,mynd-2).ne.blank4) then
        bp_samp(isb,inbc,iba,mynd) = 2*bp_samp(isb,inbc,iba,mynd-1) &
                                    - bp_samp(isb,inbc,iba,mynd-2)
      else
        bp_samp(isb,inbc,iba,mynd) = blank4
      endif
    enddo
    !
    ! And set number of points
    nbp_samp(isb,inbc) = mynd
  elseif(ly.eq.xy_phase) then
    !
    ! Count holes
    do i=1,nd
      bp_fspha(isb,inbc,i) = xx(i)
      if (.not.counting) then
        if (bp_spha(isb,inbc,-2,i).eq.0) then
          nhole = nhole + 1
          ifirst(nhole) = i
          counting  = .true.
        endif
      else
        if (bp_spha(isb,inbc,-2,i).ne.0) then
          counting  = .false.
          ilast(nhole) = i-1
        endif
      endif
    enddo
    if (counting) ilast(nhole)=nd
    !
    ! Fill holes
    do i=1, nhole
      do ia =1, nant
        iba = -bp_ant(ia,inbc)
        ym = 0
        yp = 0
        if (ifirst(i).gt.1) then
          ym=bp_spha(isb,inbc,iba,ifirst(i)-1)
        endif
        if (ilast(i).lt.nd) then
          yp=bp_spha(isb,inbc,iba,ilast(i)+1)
        endif
        if (ilast(i).ne.(ifirst(i)-1)) then
          delta = (yp-ym)/(ilast(i)-ifirst(i)+2)
        endif
        if (yp.ne.0.and.ym.ne.0) then
          do j=ifirst(i),ilast(i)
            if (imask.eq.1) then
              bp_spha(isb,inbc,iba,j) = bp_spha(isb,inbc,iba,ifirst(i)-1) &
                                     +(j-ifirst(i)+1)*delta
            else
              bp_spha(isb,inbc,iba,j) = blank4
            endif
          enddo
        endif
      enddo
    enddo
    !
    ! Add an extra extrapolated point
    mynd = nd+1
    bp_fspha(isb,inbc,mynd) = 2*bp_fspha(isb,inbc,mynd-1) &
                               -bp_fspha(isb,inbc,mynd-2)
    do ia =1, nant
      iba = -bp_ant(ia,inbc)
      if (bp_spha(isb,inbc,iba,mynd-1).ne.blank4.and. &
          bp_spha(isb,inbc,iba,mynd-2).ne.blank4) then
        bp_spha(isb,inbc,iba,mynd) = 2*bp_spha(isb,inbc,iba,mynd-1) &
                                    - bp_spha(isb,inbc,iba,mynd-2)
      else
        bp_spha(isb,inbc,iba,mynd) = blank4
      endif
    enddo
    !
    ! And set number of points
    nbp_spha(isb,inbc) = mynd
  endif
  !
  if (plot) then
    do ib=1, n_boxes
      iy = i_y(k_y(ib))
      yyy = 0
      if (iy.eq.ly) then
        iba = i_base(k_base(ib))
        isb = i_band(k_band(ib))
        ia = r_iant(iba)
        ja = r_jant(iba)
        if (my_do_rf(ia,inbc).and.my_do_rf(ja,inbc)) then
           do i=1, mynd
            if (ly.eq.xy_ampli) then
              yy1 = dble(bp_samp(isb,inbc,-ia,i))
              yy2 = dble(bp_samp(isb,inbc,-ja,i))
              if (yy1.eq.dble(blank4).or.yy2.eq.dble(blank4)) then
                yyy(i) = blank8
              else
                yyy(i) = exp(yy1+yy2+ynorm(ib))
              endif
              xxx(i) = bp_fsamp(isb,inbc,i)
            elseif (ly.eq.xy_phase) then
              yy1 = dble(bp_spha(isb,inbc,-ia,i))
              yy2 = dble(bp_spha(isb,inbc,-ja,i))
              if (yy1.eq.dble(blank4).or.yy2.eq.dble(blank4)) then
                yyy(i) = blank8
              else
                yyy(i) =-yy1+yy2+ynorm(ib)
              endif
              if (degrees.and.yyy(i).ne.blank8) then
                yyy(i) = yyy(i)*180d0/pi
              endif
              xxx(i) = bp_fspha(isb,inbc,i)
            endif
          enddo
          write(chain,'(I4.4)') ib
          call gr_execl('CHANGE DIRECTORY BOX'//chain)
          error = gr_error()
          if (error) goto 999
          if (ly.eq.xy_phase) then
            turn = 2d0*pi
            if (degrees) turn = 360d0
            call phi_plot(gb1_y(ib),gb2_y(ib),   &
              mynd,xxx,yyy, turn)
          else
            call gr8_connect (mynd,xxx,yyy,blank8,1.0d0)
          endif
          call gr_execl('CHANGE DIRECTORY')
        else
          write(chain,'(I4.4)') ib
          call gr_execl('CHANGE DIRECTORY BOX'//chain)
          xcurs = 0.5*(gb2_x(ib)+gb1_x(ib))
          ycurs = 0.5*(gb2_y(ib)+gb1_y(ib))
          call relocate(xcurs,ycurs)
!          call putlabel(7,"IGNORED",5)
          call gr_execl('CHANGE DIRECTORY')
        endif
      endif
    enddo
  endif
  !
  bp_spectrum_memory = .false.  
  return
  !
999 error = .true.
  return
end subroutine solve_pass_ant_spectrum
!
subroutine solve_pass_ant_spectrum_cross(ly,md,mb,x_data,y_data,w_data,   &
    nd, dbas,nant,xx,yy,ww,ind,wk,plot,error)
  use gildas_def
  use gkernel_interfaces
  use clic_bpc
  !---------------------------------------------------------------------
  ! CLIC routine: Passband calibration
  !     Derive antenna gain, that will be interpolated later
  !     Store in crossed solution 
  !---------------------------------------------------------------------
  integer :: ly                     !
  integer :: md                     !
  integer :: mb                     !
  real :: x_data(md,mb)             !
  real :: y_data(md,mb)             !
  real :: w_data(md,mb)             !
  integer :: nd                     !
  integer :: dbas                   !
  integer :: nant                   !
  real*8 :: xx(nd)                  !
  real*8 :: yy(dbas,nd)             !
  real*8 :: ww(dbas,nd)             !
  integer :: ind(nd)                !
  real*8 :: wk(nd)                  !
  logical :: plot
  logical :: error                  !
  ! Global
  logical :: gr8_random
  include 'gbl_memory.inc'
  include 'clic_parameter.inc'
  include 'clic_clic.inc'
  include 'clic_display.inc'
  include 'clic_par.inc'
  include 'gbl_pi.inc'
  include 'clic_sba.inc'
  include 'clic_xy_code.inc'
  ! Local
  logical :: my_do_rf, counting
  integer :: ib
  integer :: isb, iba, i, l, l1, l2, l3, iy, ix, ia, ja
  integer :: id, j, ir, mad, mad2, inbc, ibas, nbas, mynd
  parameter (mad=mnant*(mbpcdeg+1),mad2=mad**2)
  real*8 ::  c(mad), wk1(mbpcdeg+1), xxx(m_data)
  real*8 ::  aa(0:mbpcdeg), yy1, yy2, ss(mnbas),yyy(m_data)
  integer :: iant(mnbas),jant(mnbas), ibsl(mnbas)
  !
  integer :: ier, subtract, nhole, ifirst(mmask),ilast(mmask)
  real :: wnorm, faz, pha, ynorm(mbox), turn, ad(mnant), wad(mnant) 
  real :: yp,ym, delta
  real*8 :: xcurs,ycurs
  character(len=132) :: chain
  character(len=*), parameter :: rname='INIT_CROSS'
  !
  real*4 :: rms_err(mbox)
  save rms_err
  !------------------------------------------------------------------------
  !
  ! Solution for crossed are not allocated by default
  if (.not.allocated(bp_samp_cross)) then
    allocate(bp_samp_cross(2,mnbb,-mnant:mnbas,mch),stat=ier)
    if (failed_allocate(rname,'Buffers 40',ier,error))  return
    allocate(bp_spha_cross(2,mnbb,-mnant:mnbas,mch),stat=ier)
    if (failed_allocate(rname,'Buffers 41',ier,error))  return
    allocate(bp_fsamp_cross(2,mnbb,mch),stat=ier)
    if (failed_allocate(rname,'Buffers 42',ier,error))  return
    allocate(bp_fspha_cross(2,mnbb,mch),stat=ier)
    if (failed_allocate(rname,'Buffers 43',ier,error))  return
    allocate(fbp_samp_cross(2,mnbb,-mnant:mnbas),stat=ier)
    if (failed_allocate(rname,'Buffers 44',ier,error))  return
    allocate(fbp_spha_cross(2,mnbb,-mnant:mnbas),stat=ier)
    if (failed_allocate(rname,'Buffers 45',ier,error))  return
    allocate(nbp_samp_cross(2,mnbb),stat=ier)
    if (failed_allocate(rname,'Buffers 46',ier,error))  return
    allocate(nbp_spha_cross(2,mnbb),stat=ier)
    if (failed_allocate(rname,'Buffers 47',ier,error))  return
  endif
  ! Correlator input
  ! Selecting correlator input
  !
  if (.not.new_receivers) then
    inbc = 1
  else
    inbc = i_baseband(1,1)
  endif
  iba = 0
  !
  ! Loop on boxes to load data from plot buffers.
  wnorm = 0.0
  do ib = 1, n_boxes
    iy = i_y(k_y(ib))
    if (iy.eq.ly) then
      ix = i_x(k_x(ib))
      isb = i_band(k_band(ib))
      ibas = i_base(k_base(ib))
      !
      ! Deal with ignored antennas
      ia = r_iant(ibas)
      ja = r_jant(ibas)
      if (my_do_rf(ia,inbc).and.my_do_rf(ja,inbc)) then
        iba = iba+1
        ibsl(ibas) = iba
        subtract = 0
        do i = 1, ia
          if (.not.my_do_rf(i,inbc)) subtract = subtract + 1
        enddo
        ia = ia-subtract
        subtract = 0
        do i = 1, ja
          if (.not.my_do_rf(i,inbc)) subtract = subtract + 1
        enddo
        ja = ja-subtract
        iant(iba) = ia
        jant(iba) = ja
      else
        cycle
      endif
      !
      nd = n_data(ib)
      wnorm = 0.0
      if (zrsba(inbc,isb,ibas).ne.0) then
        if (iy.eq.xy_ampli) then
          ynorm(ib) =   &
            log(abs(zrsba(inbc,isb,ibas))/wrsba(inbc,isb,ibas))
        elseif (iy.eq.xy_phase) then
          ynorm(ib) = faz(zrsba(inbc,isb,ibas))
        endif
      else
        ynorm(ib) = -2.
      endif
      do i=1, nd
        xx(i) = x_data(i,ib)
        if (w_data(i,ib).gt.0) then
          if (iy.eq.xy_ampli) then
            yy(iba,i) = log(dble(y_data(i,ib)))
          elseif (iy.eq.xy_phase .and. degrees) then
            yy(iba,i) = y_data(i,ib)*pi/180d0
          else
            yy(iba,i) = y_data(i,ib)
          endif
          if (iy.eq.xy_ampli) then
            ww(iba,i) = w_data(i,ib)*y_data(i,ib)**2
            if (ww(iba,i).lt.2e-6) then
              ww(iba,i) = ww(iba,i)/100.
            endif
            yy(iba,i) = yy(iba,i)-ynorm(ib)
          else
            ww(iba,i) = w_data(i,ib)
            yy(iba,i) = yy(iba,i)-ynorm(ib)
          endif
          wnorm = wnorm + ww(i,iba)
        else
          ww(iba,i) = 0
          if (iy.eq.xy_ampli) then
            yy(iba,i) = -3.5
          elseif (iy.eq.xy_phase) then
            yy(iba,i) = 0
          endif
        endif
      enddo
      wnorm = wnorm/nd
    endif
  enddo
  nbas = iba
  ! no amplitude or no phase
  if (wnorm.le.0) return
  !
  ! Sort if required:
!! if needed, do it before transposition
!!  if (gr8_random(xx,nd)) then
!!    call gr8_trie(xx,ind,nd,error)
!!    if (error) goto 999
!!    do iba=1,nbas
!!      call gr8_sort(yy(1,iba),wk,ind,nd)
!!      call gr8_sort(ww(1,iba),wk,ind,nd)
!!    enddo
!!  endif
  !
  do i = 1, nd
    ad = 0 
    wad = 0
    if (ly.eq.xy_ampli) then
      if (all(ww(:,i).gt.0)) then
        call ampli_ant(nant,nbas,real(yy(:,i),kind=4),real(ww(:,i),kind=4),&
                        ad,wad,error)
      else
      endif
      if (error) goto 999
      do ia =1,  nant
        iba = -bp_ant(ia,inbc)
        bp_samp_cross(isb,inbc,iba,i) = ad(ia)
        fbp_samp_cross(isb,inbc,iba) = .true.
      enddo
    elseif (ly.eq.xy_phase) then
      if (all(ww(:,i).gt.0)) then
        call phase_ant(nant,nbas,real(yy(:,i),kind=4),real(ww(:,i),kind=4), &
                      ad,wad,error)
      else
      endif
      if (error) goto 999
      do ia =1,  nant
        iba = -bp_ant(ia,inbc)
        bp_spha_cross(isb,inbc,iba,i) = ad(ia)
        fbp_spha_cross(isb,inbc,iba) = .true.
      enddo
    endif
  enddo
  ! Set number of points and frequencies
  ! Compute "holes" in spectrum and interpolate
  nhole = 0
  counting = .false.
  if (ly.eq.xy_ampli) then
    !
    ! Count holes
    do i=1,nd
      bp_fsamp_cross(isb,inbc,i) = xx(i)
      if (.not.counting) then
        if (bp_samp_cross(isb,inbc,-1,i).eq.0) then
          nhole = nhole + 1
          ifirst(nhole) = i
          counting  = .true.
        endif
      else
        if (bp_samp_cross(isb,inbc,-1,i).ne.0) then
          counting  = .false.
          ilast(nhole) = i-1
        endif
      endif
    enddo
    if (counting) ilast(nhole)=nd
    !
    ! Fill holes
    do i=1, nhole
      do ia =1, nant
        iba = -bp_ant(ia,inbc)
        ym = 0
        yp = 0
        if (ifirst(i).gt.1) then
          ym=bp_samp_cross(isb,inbc,iba,ifirst(i)-1)
        endif
        if (ilast(i).lt.nd) then
          yp=bp_samp_cross(isb,inbc,iba,ilast(i)+1)
        endif
        if (ilast(i).ne.(ifirst(i)-1)) then
          delta = (yp-ym)/(ilast(i)-ifirst(i)+2)
        endif
        if (yp.ne.0.and.ym.ne.0) then
          do j=ifirst(i),ilast(i)
            if (imask.eq.1) then
              bp_samp_cross(isb,inbc,iba,j) = &
                                     bp_samp_cross(isb,inbc,iba,ifirst(i)-1) &
                                     +(j-ifirst(i)+1)*delta
            else
              bp_samp_cross(isb,inbc,iba,j) = blank4
            endif
          enddo
        else
        endif
      enddo
    enddo
    !
    ! Add an extra extrapolated point
    mynd = nd+1
    bp_fsamp_cross(isb,inbc,mynd) = 2*bp_fsamp_cross(isb,inbc,mynd-1)&
                               -bp_fsamp_cross(isb,inbc,mynd-2)
    do ia =1, nant
      iba = -bp_ant(ia,inbc)
      if (bp_samp_cross(isb,inbc,iba,mynd-1).ne.blank4.and. &
          bp_samp_cross(isb,inbc,iba,mynd-2).ne.blank4) then
            bp_samp_cross(isb,inbc,iba,mynd) = &
                                 2*bp_samp_cross(isb,inbc,iba,mynd-1) &
                                 - bp_samp_cross(isb,inbc,iba,mynd-2)
      else
        bp_samp_cross(isb,inbc,iba,mynd) = blank4
      endif
    enddo
    !
    ! And set number of points
    nbp_samp_cross(isb,inbc) = mynd
  elseif(ly.eq.xy_phase) then
    !
    ! Count holes
    do i=1,nd
      bp_fspha_cross(isb,inbc,i) = xx(i)
      if (.not.counting) then
        if (bp_spha_cross(isb,inbc,-2,i).eq.0) then
          nhole = nhole + 1
          ifirst(nhole) = i
          counting  = .true.
        endif
      else
        if (bp_spha_cross(isb,inbc,-2,i).ne.0) then
          counting  = .false.
          ilast(nhole) = i-1
        endif
      endif
    enddo
    if (counting) ilast(nhole)=nd
    !
    ! Fill holes
    do i=1, nhole
      do ia =1, nant
        iba = -bp_ant(ia,inbc)
        ym = 0
        yp = 0
        if (ifirst(i).gt.1) then
          ym=bp_spha_cross(isb,inbc,iba,ifirst(i)-1)
        endif
        if (ilast(i).lt.nd) then
          yp=bp_spha_cross(isb,inbc,iba,ilast(i)+1)
        endif
        if (ilast(i).ne.(ifirst(i)-1)) then
          delta = (yp-ym)/(ilast(i)-ifirst(i)+2)
        endif
        if (yp.ne.0.and.ym.ne.0) then
          do j=ifirst(i),ilast(i)
            if (imask.eq.1) then
              bp_spha_cross(isb,inbc,iba,j) = &
                                   bp_spha_cross(isb,inbc,iba,ifirst(i)-1) &
                                   +(j-ifirst(i)+1)*delta
            else
              bp_spha_cross(isb,inbc,iba,j) = blank4
            endif
          enddo
        endif
      enddo
    enddo
    !
    ! Add an extra extrapolated point
    mynd = nd+1
    bp_fspha_cross(isb,inbc,mynd) = 2*bp_fspha_cross(isb,inbc,mynd-1) &
                                     -bp_fspha_cross(isb,inbc,mynd-2)
    do ia =1, nant
      iba = -bp_ant(ia,inbc)
      if (bp_spha_cross(isb,inbc,iba,mynd-1).ne.blank4.and. &
          bp_spha_cross(isb,inbc,iba,mynd-2).ne.blank4) then
            bp_spha_cross(isb,inbc,iba,mynd) = &
                                    2*bp_spha_cross(isb,inbc,iba,mynd-1) &
                                    - bp_spha_cross(isb,inbc,iba,mynd-2)
      else
        bp_spha_cross(isb,inbc,iba,mynd) = blank4
      endif
    enddo
    !
    ! And set number of points
    nbp_spha_cross(isb,inbc) = mynd
  endif
  !
  if (plot) then
    do ib=1, n_boxes
      iy = i_y(k_y(ib))
      yyy = 0
      if (iy.eq.ly) then
        iba = i_base(k_base(ib))
        isb = i_band(k_band(ib))
        ia = r_iant(iba)
        ja = r_jant(iba)
        if (my_do_rf(ia,inbc).and.my_do_rf(ja,inbc)) then
           do i=1, mynd
            if (ly.eq.xy_ampli) then
              yy1 = dble(bp_samp_cross(isb,inbc,-ia,i))
              yy2 = dble(bp_samp_cross(isb,inbc,-ja,i))
              if (yy1.eq.dble(blank4).or.yy2.eq.dble(blank4)) then
                yyy(i) = blank8
              else
                yyy(i) = exp(yy1+yy2+ynorm(ib))
              endif
              xxx(i) = bp_fsamp_cross(isb,inbc,i)
            elseif (ly.eq.xy_phase) then
              yy1 = dble(bp_spha_cross(isb,inbc,-ia,i))
              yy2 = dble(bp_spha_cross(isb,inbc,-ja,i))
              if (yy1.eq.dble(blank4).or.yy2.eq.dble(blank4)) then
                yyy(i) = blank8
              else
                yyy(i) =-yy1+yy2+ynorm(ib)
              endif
              if (degrees.and.yyy(i).ne.blank8) then
                yyy(i) = yyy(i)*180d0/pi
              endif
              xxx(i) = bp_fspha_cross(isb,inbc,i)
            endif
          enddo
          write(chain,'(I4.4)') ib
          call gr_execl('CHANGE DIRECTORY BOX'//chain)
          error = gr_error()
          if (error) goto 999
          if (ly.eq.xy_phase) then
            turn = 2d0*pi
            if (degrees) turn = 360d0
            call phi_plot(gb1_y(ib),gb2_y(ib),   &
              mynd,xxx,yyy, turn)
          else
            call gr8_connect (mynd,xxx,yyy,blank8,1.0d0)
          endif
          call gr_execl('CHANGE DIRECTORY')
        else
          write(chain,'(I4.4)') ib
          call gr_execl('CHANGE DIRECTORY BOX'//chain)
          xcurs = 0.5*(gb2_x(ib)+gb1_x(ib))
          ycurs = 0.5*(gb2_y(ib)+gb1_y(ib))
          call relocate(xcurs,ycurs)
!          call putlabel(7,"IGNORED",5)
          call gr_execl('CHANGE DIRECTORY')
        endif
      endif
    enddo
  endif
  !
  bp_spectrum_memory = .false.  
  return
  !
999 error = .true.
  return
end subroutine solve_pass_ant_spectrum_cross

