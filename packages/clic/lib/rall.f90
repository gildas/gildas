subroutine rgen (error)
  use gkernel_interfaces
  use gildas_def
  use classic_api
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_constant.inc'
  include 'clic_work.inc'
  include 'clic_par.inc'
  include 'clic_display.inc'
  ! Local
  real*4 :: rwork (mdata)
  integer*4 :: iwork (mdata)
  equivalence (rwork,iwork)
  equivalence (iwork,data_buffer)
  integer(kind=8) :: len0
  integer :: ia
  character(len=4) :: procname
  !
  character(len=12) :: ch
  character(len=1) :: carm(0:4)
  data carm/'?', 'N','W','E','E'/
  !-----------------------------------------------------------------------
  ! Code:
  ! -2 General
  len0 = mgen
  call rsec(gen_sec,len0,iwork,error)
  call crsec(gen_sec,len0,error)
  !
  ! for easy variables:
  call datec(r_dobs,r_cdobs,error)
  call datec(r_dred,r_cdred,error)
  r_cproc = procname(r_proc)
  call sexag(ch,r_ut,24)
  r_ctobs = ch(1:12)
  r_cqual = quality(r_qual)
  return
  !
  ! -3 Position
entry rpos(error)
  len0 = mpos
  call rsec(pos_sec,len0,iwork,error)
  call crsec(pos_sec,len0,error)
  return
  !
  ! -4 Spectroscopy: never used
  !
  !      ENTRY RSPEC(ERROR)
  !      LEN0 = MSPEC
  !      CALL RSEC(SPEC_SEC,LEN0,IWORK,ERROR)
  !      CALL CRSEC(SPEC_SEC,LEN0,ERROR)
  !      RETURN
  !
  ! Interferometer Configuration
  !
entry rinterc(error)
  len0 = minterc
  call rsec(interc_sec,len0,iwork,error)
  call crsec(interc_sec,len0,error)
  if (r_kant(1).le.100) then
    if (r_nant.gt.0 .and. r_nant.le.12) then
      write (r_config,'(12(a1,i3.3))') (carm(r_istat(ia)/100),   &
        mod(r_istat(ia),100)+100*(r_istat(ia)/400),ia=1,r_nant)
    else
      r_config = '*Wrong config*'
    endif
  else
    r_config = 'ALMA'
  endif
  return
  !
  ! RF Frequency setup
  !
entry rrfset(error)
  len0 = mrfset
  call rsec(rfset_sec,len0,iwork,error)
  call crsec(rfset_sec,len0,error)
  return
  !
  ! Continuum setup
  !
entry rcontset(error)
  len0 = mcontset
  call rsec(contset_sec,len0,iwork,error)
  call crsec(contset_sec,len0,error)
  return
  !
  ! Line setup
entry rlineset(error)
  len0 = mlineset
  call rsec(lineset_sec,len0,iwork,error)
  call crsec(lineset_sec,len0,error)
  return
  !
  ! Scanning
entry rscanning(error)
  len0 = mscanning
  call rsec(scanning_sec,len0,iwork,error)
  call crsec(scanning_sec,len0,error)
  return
  !
  ! Pointing corrections
  !
  ! Atmospheric parameters
entry ratparm(error)
  len0 = matparm
  call rsec(atparm_sec,len0,iwork,error)
  call crsec(atparm_sec,len0,error)
  return
  !
  ! Atmospheric monitor
entry ratmon(error)
  len0 = matmon
  call rsec(atmon_sec,len0,iwork,error)
  call crsec(atmon_sec,len0,error)
  return
  !
  ! Baseline Passband Calibration
  !
entry rbpcal(error)
  len0 = mbpc
  call rsec(bpcal_sec,len0,iwork,error)
  call crsec(bpcal_sec,len0,error)
  return
  !
  ! Antenna  Passband Calibration
  !
entry rabpcal(error)
  len0 = mabpc
  call rsec(abpcal_sec,len0,iwork,error)
  call crsec(abpcal_sec,len0,error)
  return
  !
  ! Instrumental Calibration
  !
entry rical(error)
  len0 = mic
  call rsec(ical_sec,len0,iwork,error)
  call crsec(ical_sec,len0,error)
  return
  !
  ! Antenna Instrumental Calibration
  !
entry raical(error)
  len0 = maic
  call rsec(aical_sec,len0,iwork,error)
  call crsec(aical_sec,len0,error)
  return
  !
  ! Data Section descriptor
  !
entry rdescr (error)
  len0 = mdescr
  call rsec(descr_sec,len0,iwork,error)
  call crsec(descr_sec,len0,error)
  return
  !
  ! Data Modifier
  !
entry rdmodif (error)
  len0 = mdmodif
  call rsec(modify_sec,len0,iwork,error)
  call crsec(modify_sec,len0,error)
  return
  !
entry rdfile (error)
  len0 = mdfile
  call rsec(file_sec,len0,iwork,error)
  call crsec(file_sec,len0,error)
  return
  !
  ! Water Vapor radiometer
entry rwvr (error)
  len0 = mwvr
  call rsec(wvr_sec,len0,iwork,error)
  call crsec(wvr_sec,len0,error)
  return
  !
  ! SD Holo
entry ralma (error)
  len0 = malma
  call rsec(alma_sec,len0,iwork,error)
  call crsec(alma_sec,len0,error)
  return
  !
  ! NOEMA monitoring section
entry rmoni (error)
  len0 = mmonitor
  call rsec(monitor_sec,len0,iwork,error)
  call crsec(monitor_sec,len0,error)
  return
  !
  ! NOEMA status monitoring 
entry rstatus (error)
  len0 = mstatus
  call rsec(status_sec,len0,iwork,error)
  call crsec(status_sec,len0,error)
  return
  !
  ! VLBI 
entry rvlbi (error)
  len0 = mvlbi
  call rsec(vlbi_sec,len0,iwork,error)
  call crsec(vlbi_sec,len0,error)
  return
end subroutine rgen
!
subroutine wgen (error)
  use clic_file
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_constant.inc'
  include 'clic_work.inc'
  include 'clic_par.inc'
  ! Local
  integer*4 :: iwork (mdata)
  equivalence (iwork,data_buffer)
  integer(kind=8) :: len0, len1, len2, lensec, len3, newlen, t1, t2
  !------------------------------------------------------------------------
  ! Code:
  !
  len0 = mgen
  call cwsec(gen_sec,len0,error)
  call wsec(gen_sec,len0,iwork,error)
  return
  !
entry wpos(error)
  len0 = mpos
  call cwsec(pos_sec,len0,error)
  call wsec(pos_sec,len0,iwork,error)
  return
  !
  ! never used
  !      ENTRY WSPEC(ERROR)
  !      LEN0 = MSPEC
  !      CALL CWSEC(SPEC_SEC,LEN0,ERROR)
  !      RETURN
  !
entry winterc(error)
  len0 = minterc
  call cwsec(interc_sec,len0,error)
  call wsec(interc_sec,len0,iwork,error)
  return
  !
entry wrfset(error)
  len0 = mrfset
  call cwsec(rfset_sec,len0,error)
  call wsec(rfset_sec,len0,iwork,error)
  return
  !
entry wcontset(error)
  len0 = mcontset
  call cwsec(contset_sec,len0,error)
  call wsec(contset_sec,len0,iwork,error)
  return
  !
entry wlineset(error)
  len0 = mlineset
  call cwsec(lineset_sec,len0,error)
  call wsec(lineset_sec,len0,iwork,error)
  return
  !
entry wscanning(error)
  len0 = mscanning
  call cwsec(scanning_sec,len0,error)
  call wsec(scanning_sec,len0,iwork,error)
  return
  !
entry watparm(error)
  len0 = matparm
  call cwsec(atparm_sec,len0,error)
  call wsec(atparm_sec,len0,iwork,error)
  return
  !
entry watmon(error)
  call cwsec(atmon_sec,len0,error)
  call wsec(atmon_sec,len0,iwork,error)
  return
  !
entry wbpcal(error)
  if (modify) then
    len0 = lensec(bpcal_sec)
    if (e%version.le.3) then
      t1 = mnbcinput+r_nband_widex
      if (r_nband_widex.eq.0) then
        t2 = 10
      else
        t2 = 12
      endif
    else
      t1 = r_nbb
      t2 = r_nband
    endif
    len1 = 5+r_nbas*t1*(4*t2+4) ! may not be compressed
    len2 = 4*r_nbas*t1*r_lband    ! times R_BPCDEG+1
    len3 = 4*r_nbas*t1            ! times R_BPFDEG+1
    ! give priority to r_bpfdeg if cuts are needed.
    if (len0.lt.len1+(r_bpcdeg+1)*len2+(r_bpfdeg+1)*len3) then
      ! message un peu trop troublant ...
      !            CALL MESSAGE(6,2,'WBPCAL',
      !     $      'Not enough space, limiting degree (channel-based RF)')
      r_bpcdeg =  max(0,(len0-len1-len3*(r_bpfdeg+1))/len2 - 1)
      newlen =  len1+len2*(r_bpcdeg+1)+len3*(r_bpfdeg+1)
      if (len0.lt.newlen) then
        r_bpfdeg = (len0-len1-len2)/len3 -1
        r_bpcdeg = 0
      endif
      len0 =  len1+len2*(r_bpcdeg+1)+len3*(r_bpfdeg+1)
    endif
  endif
  call cwsec(bpcal_sec,len0,error)
  call wsec(bpcal_sec,len0,iwork,error)
  return
  !
entry wabpcal(error)
  if (modify) then
    len0 = lensec(abpcal_sec)
    !         LEN1 = 5+R_NANT*(4*MCCH+4)      ! may not be compressed
    !         LEN2 = 4*R_NANT*R_LBAND         ! times r_abpcdeg+1
    !         LEN3 = 4*R_NANT                 ! times r_abpfdeg+1
    !
    if (e%version.le.3) then
      t1 = mnbcinput+r_nband_widex
      if (r_nband_widex.eq.0) then
        t2 = 10
      else
        t2 = 12
      endif
    else
      t1 = r_nbb
      t2 = r_nband
    endif
    len1 = 3+2*t1+r_nant*t1*(4*t2+4)   ! may not be compressed
    len2 = 4*r_nant*t1*r_lband  ! times r_abpcdeg+1
    len3 = 4*r_nant*t1  ! times r_abpfdeg+1
    ! give priority to r_abpfdeg if cuts are needed.
    if (len0.lt.len1+(r_abpcdeg+1)*len2+(r_abpfdeg+1)*len3) then
      ! message un peu trop troublant ...
      !            CALL MESSAGE(6,2,'WABPCAL',
      !     $      'Not enough space, limiting degree (channel-based RF)')
      r_abpcdeg =  max(0,(len0-len1-len3*(r_abpfdeg+1))/len2 - 1)
      newlen =  len1+len2*(r_abpcdeg+1)+len3*(r_abpfdeg+1)
      if (len0.lt.newlen) then
        r_abpfdeg = (len0-len1-len2)/len3 -1
        r_abpcdeg = 0
      endif
      len0 =  len1+len2*(r_abpcdeg+1)+len3*(r_abpfdeg+1)
    endif
  endif
  call cwsec(abpcal_sec,len0,error)
  call wsec(abpcal_sec,len0,iwork,error)
  return
  !
entry wical(error)
  len0 = mic
  call cwsec(ical_sec,len0,error)
  call wsec(ical_sec,len0,iwork,error)
  return
  !
entry waical(error)
  len0 = maic
  call cwsec(aical_sec,len0,error)
  call wsec(aical_sec,len0,iwork,error)
  return
  !
entry wdescr(error)
  len0 = mdescr
  call cwsec(descr_sec,len0,error)
  call wsec(descr_sec,len0,iwork,error)
  return
  !
entry wdmodif (error)
  len0 = mdmodif
  call cwsec(modify_sec,len0,error)
  call wsec(modify_sec,len0,iwork,error)
  return
  !
entry wdfile (error)
  len0 = mdfile
  call cwsec(file_sec,len0,error)
  call wsec(file_sec,len0,iwork,error)
  return
  !
  ! Water Vapor radiometer
entry wwvr (error)
  len0 = mwvr
  call cwsec(wvr_sec,len0,error)
  call wsec(wvr_sec,len0,iwork,error)
  return
  !
  ! SD Holo
entry walma (error)
  len0 = malma
  call cwsec(alma_sec,len0,error)
  call wsec(alma_sec,len0,iwork,error)
  return
  ! 
  ! NOEMA monitoring seciton
entry wmoni (error)
  len0 = mmonitor
  call cwsec(monitor_sec,len0,error)
  call wsec(monitor_sec,len0,iwork,error)
  return
  ! 
  ! NOEMA status monitoring 
entry wstatus (error)
  len0 = mstatus
  call cwsec(status_sec,len0,error)
  call wsec(status_sec,len0,iwork,error)
  return
  ! 
  ! VLBI 
entry wvlbi (error)
  len0 = mvlbi
  call cwsec(vlbi_sec,len0,error)
  call wsec(vlbi_sec,len0,iwork,error)
end subroutine wgen
!
subroutine udescr (error)
  use clic_file
  logical :: error                  !
  ! Global
  include 'clic_parameter.inc'
  ! Local
  logical :: msave
  integer :: len0
  !------------------------------------------------------------------------
  ! Code:
  msave = modify
  modify = .true.
  len0 = mdescr
  call cwsec(descr_sec,len0,error)
  modify = msave
end subroutine udescr
!
subroutine rcom(com,ncom,error)
  !---------------------------------------------------------------------
  ! RW	Internal routine
  !	Read a comment section
  ! Arguments :
  !	COM	I*4 (*)	Array to receive the comment	Output
  !	NCOM	I	Number of words read		Output
  !			Size of COM			Input
  !	ERROR	L	Logical error flag		Output
  !---------------------------------------------------------------------
  integer*4 :: com(*)               !
  integer :: ncom                   !
  logical :: error                  !
  !
  call rsec (-1,ncom,com,error)
  return
  !
entry wcom(com,ncom,error)
  !----------------------------------------------------------------------
  ! RW	Internal routine
  !	write a comment section
  ! Arguments :
  !	COM	I*4 (*)	Array to receive the comment	Input
  !	NCOM	I	Number of words to write	Input
  !	ERROR	L	Logical error flag		Output
  !----------------------------------------------------------------------
  call wsec (-1,ncom,com,error)
end subroutine rcom
!
subroutine rdata (ndata,data,error)
  use clic_file
  !---------------------------------------------------------------------
  ! RW	Internal routine
  !	read the data section
  !---------------------------------------------------------------------
  integer(kind=data_length)  :: ndata    ! Data length (number of "words")
  integer                    :: data(*)  ! Data array
  logical                    :: error    ! Logical error flag
  ! Global
  include 'clic_parameter.inc'
  include 'clic_work.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  ! Local
  integer(kind=data_length) :: l1, l2, h_offset, k
  !
  integer :: ii, isb, ic, econve, ipol 
  !
  ! read data in external file if needed:
  if (r_presec(file_sec)) then
    call rdata_ext(ndata,data,error)
    econve = d%conv%code
  else
    if (e%ldata.gt.ndata) then
      call message(8,4,'RDATA','Data buffer too small')
      error = .true.
      return
    else
      ndata  = e%ldata
    endif
    call classic_entry_data_read(data,ndata,e,ibuff,error)
    econve = i%conv%code
    !
    ! For use in rrecord
    de%version = e%version
  endif
  if (ndata.eq.0) then
    call message(8,4,'RDATA','No data found')
    error = .true.
    return
  endif
  if (error) return
  !*
  if (r_ndump*r_ldump.eq.ndata) then
    if (r_ldump.eq.(r_ldpar+r_ldatc+r_ldatl)) then
      call message(4,2,'RDATA','Old correlator data')
      l1 = r_ldatc+r_ldatl
      l2 = 0
    elseif (r_ldump.eq.(r_ldpar+r_ldatc)) then
      call message(4,2,'RDATA','No spectral data')
      l1 = r_ldatc
      l2 = r_ldatc+r_ldatl
    endif
  elseif (r_ndump*r_ldump +max(1,r_ndatl)*(r_ldump+r_ldatl).eq.ndata)  then
    l1 = r_ldatc
    l2 = r_ldatc+r_ldatl
  else
    write(6,*)   &
      'r_ldpar, r_ldatc, r_ndump, r_ldatl, r_ndatl, ndata'
    write(6,*)   &
      r_ldpar, r_ldatc, r_ndump, r_ldatl, r_ndatl, ndata
    call message(8,4,'RDATA','Inconsistent data section')
    error = .true.
    return
  endif
  if (r_ndump.ne.0) then
    do ii=1,r_ndump
      k = 1+h_offset(ii)
      call convert_dh (data(k),r_ldpar,econve,r_nant,r_nbas,   &
        r_lband,r_npol_rec)
      if (r_presec(file_sec)) call modify_dh (data(k))
      k = k+r_ldpar
      call convert_data (l1,data(k),econve)
      if (r_presec(file_sec)) then
        call modify_datac (data(k))
      endif
    enddo
  endif
  ! extra dump in new data format (with line data)
  if (l2.gt.0) then
    k = 1+h_offset(r_ndump+1)
    call convert_dh (data(k),r_ldpar,econve,r_nant,r_nbas,   &
      r_lband,r_npol_rec)
    if (r_presec(file_sec)) call modify_dh (data(k))
    k = k+r_ldpar
    call convert_data (l2,data(k),econve)
    if (r_presec(file_sec)) then
      call modify_datac (data(k))
      call modify_datal (data(k+r_ldatc))
    endif
    ! extra dump with phase-corrected data:
    if (r_ndatl.gt.1) then
      k = 1+h_offset(r_ndump+2)
      call convert_dh (data(k),r_ldpar,econve,r_nant,r_nbas,   &
        r_lband,r_npol_rec)
      if (r_presec(file_sec)) call modify_dh (data(k))
      k = k+r_ldpar
      call convert_data (l2,data(k),econve)
      if (r_presec(file_sec)) then
        call modify_datac (data(k))
        call modify_datal (data(k+r_ldatc))
      endif
    endif
  endif
  !
  ! If data was read from input file, we initialize the data modifier section
  ! here, in case we do set write nodata later.
  if (.not.r_presec(file_sec)) then
    k = 1+h_offset(r_ndump+1)
    call decode_header(data(k))
    do ii=1, r_nant
      r_dmaflag(ii) = dh_aflag(ii)
      do ipol = 1, r_nbb
        r_dmatfac(ipol,1,ii) = dh_atfac(ipol,1,ii)
        r_dmatfac(ipol,2,ii) = dh_atfac(ipol,2,ii)
      enddo
      do ipol = 1, r_nbb
        r_dmdelay(ipol,ii) = dh_delay(ipol,ii)
      enddo
    enddo
    do ii=1, r_nbas
      r_dmbflag(ii) = dh_bflag(ii)
      do isb=1, 2
        do ic = 1, mcch
          r_dmcamp(isb,ii,ic) = 1.0
          r_dmcpha(isb,ii,ic) = 0.0
        enddo
        do ic = 1, mrlband
          r_dmlamp(isb,ii,ic) = 1.0
          r_dmlpha(isb,ii,ic) = 0.0
          r_dmldph(isb,ii,ic) = 0.0
        enddo
      enddo
    enddo
  endif
  return
end subroutine rdata
!
subroutine wdata (ndata,data,copy_data,error)
  use gkernel_interfaces
  use clic_file
  !---------------------------------------------------------------------
  ! RW	Internal routine
  !	write the data section
  !---------------------------------------------------------------------
  integer(kind=data_length):: ndata      ! Data length (number of "words")
  integer                  :: data(*)    ! Data array
  logical                  :: copy_data  ! Use do_write_data to write data ?
  logical                  :: error      ! Logical error flag
  ! Global
  include 'clic_parameter.inc'
  include 'clic_work.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  ! Local
  integer(kind=data_length) :: h_offset, k, l1, l2
  integer            :: ii, jconve
  integer            :: r_xnum_i4
  logical            :: write_data
  character(len=256) :: chain,chain2
  !----------------------------------------------------------------------
  ! according to SET WRITE DATA status
  if (copy_data) then
    write_data = do_write_data
  ! according to input observation status
  else
    write_data = .not.r_presec(file_sec)
  endif
  if (.not.write_data) then
    !
    ! we write the modifier section, if data or flags have been modified:
    ! in fact we write it always, if only to reserve space.
    ! it should have been initialized in rdata
    call wdmodif(error)
    if (error) return
    !
    ! if no data file section we prepare the data file section:
    if (.not. r_presec(file_sec)) then
      r_presec(file_sec) = .true.
      call i8toi4_fini(r_xnum,r_xnum_i4,1,error)
      if (error) then
        call message(8,3,'WDATA','hpb file can only address I4 R_DXNUM')
        return
      endif
      r_dxnum = r_xnum_i4
      chain = i%spec
      chain2 = ' '
      call sic_parsef(chain,chain2,' ',' ')
      r_dfile = chain
    endif
    call wdfile (error)
    if (write_mode.eq.'NEW ') e%ldata = ndata
    return
  endif
  !
  ! Actually write the data section in output file
  if (r_presec(file_sec).and.modify) then
    call message(8,4,'WDATA',   &
      'Cannot write the data section in a header-only file')
    error = .true.
    return
  endif
  !
  ! In that case, the data in memory is up-to-date, the modifier
  ! section is not written
  r_presec(modify_sec) = .false.
  r_presec(file_sec) = .false.
  de%version = e%version
  !
  if (obuff%lun.ne.o%lun) then
    error = .true.
    call message(8,4,'WDATA','Observation not open for write'   &
      //' nor modify')
    return
  endif
  !
  if (r_ndump*r_ldump.eq.ndata) then
    l1 = r_ldatc+r_ldatl
    l2 = 0
  elseif (r_ndump*r_ldump +max(1,r_ndatl)*(r_ldump+r_ldatl).eq.ndata) then
    l1 = r_ldatc
    l2 = r_ldatc+r_ldatl
  else
    write(6,*) 'r_ldpar, r_ldatc, r_ndump, r_ldatl, r_ndatl, ndata'
    write(6,*)  r_ldpar, r_ldatc, r_ndump, r_ldatl, r_ndatl, ndata
    call message(8,4,'WDATA','Inconsistent data section')
    error = .true.
    return
  endif
  jconve = -o%conv%code
  if (r_ndump.gt.0) then
    do ii=1,r_ndump
      k = 1+h_offset(ii)
      call convert_dh (data(k),r_ldpar,jconve,r_nant,r_nbas,   &
        r_lband,r_npol_rec)
      k = k+r_ldpar
      call convert_data (l1,data(k),jconve)
      k = k+l1
    enddo
  endif
  !
  ! extra dump in new data format (with line data)
  if (l2.gt.0) then
    k = 1+h_offset(r_ndump+1)
    call convert_dh (data(k),r_ldpar,jconve,r_nant,r_nbas,   &
      r_lband,r_npol_rec)
    k = k+r_ldpar
    call convert_data (l2,data(k),jconve)
  endif
  !
  ! extra dump with phase-corrected data:
  if (r_ndatl.gt.1) then
    k = 1+h_offset(r_ndump+2)
    call convert_dh (data(k),r_ldpar,jconve,r_nant,r_nbas,   &
      r_lband,r_npol_rec)
    k = k+r_ldpar
    call convert_data (l2,data(k),jconve)
  endif
  !
  ! Now writes the data
  if (modify) then
    call classic_entry_data_update(data,ndata,e,obuff,error)
  else
    call classic_entry_data_add(data,ndata,e,obuff,error)
  endif
end subroutine wdata
!
subroutine rdata_ext (ndata,data,error)
  use gkernel_interfaces
  use gbl_format
  ! Classic want real buffer
  use classic_api,droit_au_but=>classic_entry_data_read,&
                  a_jamais_les_premiers=>classic_entry_data_readsub 
  use classic_types
  use clic_file
  use clic_title
  use clic_index
  use clic_virtual
  !---------------------------------------------------------------------
  ! RW	Internal routine
  !	read the data section in external file
  !---------------------------------------------------------------------
  integer(kind=data_length)  :: ndata   ! Data length (number of "words")
  integer                    :: data(*) ! Data array
  logical                    :: error   ! Logical error flag
  ! Global
  include 'clic_parameter.inc'
  include 'clic_work.inc'
  include 'clic_par.inc'
  include 'clic_clic.inc'
  include 'gbl_pi.inc'
  include 'gbl_memory.inc'
  ! Local
  character(len=80)          :: dname, filnam, mess, chain, uname, lname
  character(len=filename_length) :: toto
  integer                    :: nfil, ier, ln, ls, ll
  integer                    :: lun_0, j, ix ! r_dxnum kind=4 for the time being
  integer(kind=8)            :: ddata, ddatal 
  integer(kind=data_length)  :: first, last, ndata1, ndata2, ldl, k
  integer(kind=entry_length) :: ii
  integer(kind=address_length) :: ipv
  logical                    :: ok
  integer                    :: mlogs,mexts,lntch
  parameter(mlogs=10,mexts=4)
  character(len=12) :: logs(mlogs)
  character(len=9)  :: exts(mexts)
  character(len=1)  :: backslash
  save lun_0
  !
  data lun_0 /0/
  data logs/'ipb_data:','ipb_data1:','ipb_data2:',   &
    'ipb_data3:','ipb_data4:','ipb_data5:',   &
    'ipb_data6:','ipb_data7:','ipb_data8:','ipb_data9:'/
  data exts/'.ipb','.IPB','.ipb-data','.IPB;1'/
  !----------------------------------------------------------------------
  !
  backslash=char(92)
  !
  ! Get LUN
  if (lun_0.eq.0) then
    ier = sic_getlun(lun_0)
    if (ier.ne.1) then
      error = .true.
      call message(8,4,'RDATA_EXT','No logical unit left')
      return
    endif
  endif
  !
  ! No data file section
  if (.not.r_presec(file_sec)) then
    call message(8,4,'RDATA_EXT','No data file section')
    error = .true.
    return
  endif
  ln = lenc(r_dfile)
  dname = r_dfile(1:ln)
  !
  ! Patch Windows
  if (dname(2:3).eq.':'//backslash) then
    dname = r_dfile(4:ln)
    ln = ln-3
  endif
  !
  uname = dname
  call sic_upper(uname)
  if (ln.eq.8.or.ln.eq.16) then
    ls = 8
    ll = 16
  else
    ls = 14
    ll = 20
  endif
  if (ln.eq.16.or.ln.eq.20) then
    call encrypt(dname,uname,error)
  elseif (ln.eq.8.or.ln.eq.14) then
    call decrypt(uname,dname,error)
  endif
  !
  lname = uname
  call sic_lower(lname)
  !
  ! Scan directories to find data file
  ok = .false.
  ii = 1
  do while (.not.ok .and. ii.le.mlogs)
    j = 1
    do while (.not.ok .and. j.le.mexts)
      if (exts(j)(2:4).eq.'IPB') then
        call sic_parse_file('!'//uname(1:ls)//exts(j),logs(ii),exts(j),filnam)
      else
        call sic_parse_file(dname,logs(ii),exts(j),filnam)
      endif
      nfil = lenc(filnam)
      inquire (file=filnam(1:nfil),exist=ok)
      !
      ! On Linux Systems, the CDROM may sometimes be mounted in such
      ! a way as to give lower case filenames, although they were uppercase before
      if (.not.ok .and. j.eq.1) then   ! '.ipb' case
        call sic_parsef(lname,filnam,logs(ii),exts(j))
        nfil = lenc(filnam)
        inquire (file=filnam(1:nfil),exist=ok)
      endif
      j = j+1
    enddo
    ii = ii+1
  enddo
  !
  if (.not.ok) then
    error = .true.
    mess = 'Files '//dname(1:ll)//' or '//uname(1:ls)//' not found'
    call message(8,4,'RDATA_EXT',mess)
    call message(8,4,'RDATA_EXT','No matching IPB file found in:')
    do ii=1,mlogs
      toto = logs(ii)
      call sic_resolve_log(toto)
      if (toto.ne.logs(ii)) then
        write(chain,*) logs(ii),trim(toto)
        call message(8,4,'RDATA_EXT',chain(1:lenc(chain)))
      else
        write(chain,*) logs(ii),"Undefined"
        call message(8,4,'RDATA_EXT',chain(1:lenc(chain)))
      endif
    enddo
    return
  endif
  !
  ! Close file if necessary
  if (d%lun.ne.0 .and. d%spec(1:d%nspec).ne.filnam(1:nfil)) then
    close (unit=d%lun)
    d%lun = 0
  endif
  !
  ! Open file if necessary
  if (d%lun.eq.0) then
    d%lun = lun_0
    ! There may be conflict with files opened in Input/Output under a different
    ! name...
    d%spec = filnam(1:nfil)
    d%nspec = nfil
    open (unit=d%lun,file=d%spec(1:d%nspec),access='DIRECT',   &
      status='OLD',form='UNFORMATTED',recl=classic_reclen_v1*facunf,   &
      iostat=ier)
    if (ier.ne.0) then
      error = .true.
      mess = 'Open error file '//filnam(1:nfil)
      call message(8,4,'RDATA_EXT',mess)
      call messios(8,4,'RDATA_EXT',ier)
      return
    endif
    !
    ! Now check the data structure
    call classic_filedesc_open(d,error)
    if (error) goto 11
    !
    ! Reallocate buffers in case reclen changed
    call reallocate_recordbuf(dbuff,d%desc%reclen,error)
    call reallocate_recordbuf(dbufi,d%desc%reclen,error)
    call classic_recordbuf_nullify(dbufi)  
    !
    ! read the index : Read all blocks even first one
    do ii = 1, d%desc%xnext-1
      call rdx (ii,error)
      if (error) return
      dx_bloc(ii) = title%bloc
      dx_word(ii) = title%word
      dx_dobs(ii) = title%dobs
      dx_scan(ii) = title%scan
      dx_rece(ii) = title%recei
      dx_ut(ii)   = title%ut
    enddo
  endif
  !
  ! Read the observation header
  ix = r_dxnum
  !
  ! Check first consistency
  error = .false.
  if (dx_scan(ix).ne.r_scan) then
    error = .true.
    write (chain,*) 'Scan error: ',dx_scan(ix), r_scan
    call message(8,2,'RDATA_EXT',chain(1:lenc(chain)))
  endif
  if (dx_rece(ix).ne.r_nrec) then
    error = .true.
    write (chain,*) 'Receiver error: ',dx_rece(ix), r_nrec
    call message(8,2,'RDATA_EXT',chain(1:lenc(chain)))
  endif
  if (error) then
    ix = 0
    do ii = 1, d%desc%xnext-1
      if ((dx_scan(ii).eq.r_scan).and.(dx_rece(ii).eq.r_nrec)) then
        if (abs(mod(dx_ut(ii)-r_ut+pi,2*pi)-pi).le.pi/43200.) then
          ix = ii
        endif
      endif
    enddo
    if (ix.eq.0) then
      call message(8,4,'RDATA_EXT',   &
        'Fatal ordering error in data file '   &
        //filnam(1:nfil))
      error = .true.
      return
    else
      r_dxnum = ix
      error = .false.
    endif
  endif
  !
  ! Initialize Observation buffer
  call classic_recordbuf_open(d,dx_bloc(ix),dx_word(ix),dbuff,error)
  !
  ! Read entry descriptor
  call classic_entrydesc_read(d,dbuff,de,error)
  if (error) return
  !
  ! Check observation version
  if (de%version.gt.version_last) then
    call message(8,3,'RDATA_EXT','Observation version not supported')
    call message(8,3,'RDATA_EXT','Update your CLIC version')
    error = .true.
    return
  endif
  !
  ! Now reads data into data buffer
  if (.not.lowres.or.r_lmode.ne.1) then
    ndata  = de%ldata
    call classic_entry_data_read(data,ndata,de,dbuff,error)
  else
    ! lowres mode: read only LR data (+time dumps)
    r_lband = lband_original
    r_ldatl = ldatl_original
    r_lntch = lntch_original
    lntch = 0
    do j=1, r_nbb
      lntch = lntch+r_lnch(j)
    enddo
    ldl = 2*lntch*r_lnsb
    !
    ! First time dumps
    ndata = (r_ndump+1)*r_ldump
    first = 1
    last = ndata
    k = 1
    call classic_entry_data_readsub(data(k),ndata1,first,last,de,dbuff,error)
    if (ndata1.ne.ndata) then
      write(mess,'(a,i0,a,i0)') 'lowres mode: ndata1=',ndata1,' ndata=',ndata
      call message(8,4,'RDATA_EXT',mess)
      error = .true.
      return
    endif
    k = k+ndata
    !
    ! Now uncorrected data
    do j=1, r_nbas
      first = (r_ndump+1)*r_ldump+(j-1)*2*r_lntch*r_lnsb+1
      last = first+ldl-1
      call classic_entry_data_readsub(data(k),ndata1,first,last,de,dbuff,error)
      k = k+ldl
    enddo
    !
    ! Read corrected data if present
    if (r_ndatl.gt.1) then
      first = (r_ndump+1)*r_ldump+r_ldatl+1
      last = first+r_ldump-1
      call classic_entry_data_readsub(data(k),ndata2,first,last,de,dbuff,error)
      k = k+r_ldump
      do j=1, r_nbas
        first = (r_ndump+2)*r_ldump+r_ldatl+(j-1)*2*r_lntch*r_lnsb+1
        last = first+ldl-1
        call classic_entry_data_readsub(data(k),ndata1,first,last,de,dbuff,error)
        k = k+ldl
      enddo
    endif
    !
    ! And forget (temporarily) about HR
    r_lband = r_nbb
    r_lntch = lntch
    r_ldatl = ldl*r_nbas
    ndata = k-1
    !
    ! Update header in memory if needed
    if (got_header(r_xnum)) then
      ipv = gag_pointer(v_header(r_xnum),memory)
      call r4tor4 (r_xnum,memory(ipv),v_header_length(r_xnum))
    endif
  endif
  if (error) return
  !
  ! Store data length in input entrydescriptor
  e%ldata = ndata
  return
  !
11 error = .true.
  mess = 'Read error file '//filnam(1:nfil)
  call message(8,4,'RDATA_EXT',mess)
  call messios(8,4,'RDATA_EXT',ier)
  return
end subroutine rdata_ext
!
subroutine modify_datac(data)
  use gkernel_interfaces
  use classic_api
  !---------------------------------------------------------------------
  ! Apply the modifying factors in the data modifier section,
  ! to the continuum data.
  !---------------------------------------------------------------------
  real :: data(*)                   !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  ! Local
  integer :: k, ia, ib, isb, isub 
  integer :: ipol
  logical :: no_number
  real :: ampli
  real*8 :: phase
  !-----------------------------------------------------------------------
  if (.not.r_presec(modify_sec)) return
  k = 1
  ! Correlation
  if (r_lmode.eq.1) then
    do ib=1, r_nbas
      do isb = 1, r_nsb
        do isub = 1, r_nband
          ipol = r_lpolentry(1,isub)
          if (.not.(no_number(data(k)).or.no_number(data(k+1)))) then
            phase = r_dmcpha(isb,ib,isub)
            ampli = r_dmcamp(isb,ib,isub)
            data(k) = data(k) * ampli
            data(k+1) = data(k+1) * ampli
            call rotate(data(k), phase)
          endif
          k = k + 2
        enddo
      enddo
    enddo
  ! Autocorrelation
  else
    do ia=1, r_nant
      do isub = 1, r_nband
        ipol = r_lpolentry(1,isub)
        if (.not.(no_number(data(k)))) then
          ampli = r_dmcamp(1,ia,isub)
          data(k) = data(k) * ampli
        endif
        k = k + 1
      enddo
    enddo
  endif
  return
end subroutine modify_datac
!
subroutine modify_datal(data)
  use gkernel_interfaces
  use classic_api
  !---------------------------------------------------------------------
  ! Apply the modifying factors in the data modifier section,
  ! to the line data.
  !---------------------------------------------------------------------
  real :: data(*)                   !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  ! Local
  integer :: k, ia, ib, isb, isub, kc, i
  integer :: ipol
  logical :: no_number
  real :: ampli
  real*8 :: phase
  !-----------------------------------------------------------------------
  if (.not.r_presec(modify_sec)) return
  k = 1
  ! Correlation
  if (r_lmode.eq.1) then
    do ib=1, r_nbas
      do isb = 1, r_lnsb
        kc = k
        do isub = 1, r_lband
          k = kc + 2*r_lich(isub)
          do i=1, r_lnch(isub)
            if (.not.(no_number(data(k)).or.no_number(data(k+1))))   &
              then
              phase = r_dmlpha(isb,ib,isub)   &
                + (i-r_lcench(isub))   &
                *r_dmldph(isb,ib,isub)
              ampli = r_dmlamp(isb,ib,isub)
              data(k) = data(k) * ampli
              data(k+1) = data(k+1) * ampli
              call rotate(data(k), phase)
            endif
            k = k + 2
          enddo
        enddo
        k = kc + 2*r_lntch
      enddo
    enddo
  ! Autocorrelation
  else
    do ia=1, r_nant
      kc = k
      do isub = 1, r_lband
        k = kc + r_lich(isub)
        do i=1, r_lnch(isub)
          if (.not.(no_number(data(k))))  then
            ampli = r_dmlamp(1,ia,isub)
            data(k) = data(k) * ampli
            k = k + 1
          endif
        enddo
      enddo
      k = kc+r_lntch
    enddo
  endif
  return
end subroutine modify_datal
!
subroutine modify_dh(data)
  use classic_api
  use clic_file, except=>i
  !---------------------------------------------------------------------
  ! Modify the data header according to the current modifier section.
  ! We only change the antenna and baseline flags.
  !---------------------------------------------------------------------
  integer :: data(*)                !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'clic_dheader.inc'
  include 'clic_clic.inc'
  ! Local
  integer :: k,temp_dim,temp_dim2,i,j,ii,dmsaflag(mnant),dmsbflag(mnbas)
  integer :: l,m
  real    :: dmdelay(m_pol_rec,mnant)
  !-----------------------------------------------------------------------
  if (.not.r_presec(modify_sec)) return
  !
  if (de%version.le.2) then
    k = 22
  else
    k = 32
  endif
  do i =1, r_nant
    data(k) = ior(data(k),r_dmaflag(i))
    k = k+1
  enddo
  !     Change of atmospheric calibration on 13-feb-2007
  if (de%version.le.3) then
    if (r_dobs.lt.(-6406)) then
      temp_dim = r_npol_rec 
    else
      temp_dim = 2 + r_nband_widex
    endif
  else
    temp_dim = r_nbb
  endif
  k = k + temp_dim*r_nant
  call r4tor4(r_dmatfac,data(k),2*r_nant)  ! FAUX
  !      K = K + 7*R_NANT
  if (de%version.le.3) then
    if (new_receivers) then
      temp_dim = r_lband
    else
      temp_dim = 1
    endif
    if (r_nband_widex.ne.0) then
      temp_dim2 = 2
    else
      temp_dim2 = 1
    endif
    if (de%version.le.2) then
      k = k+(4+temp_dim2*r_npol_rec+2*temp_dim)*r_nant
    else
      k = k+(3+temp_dim2*r_npol_rec+2*temp_dim)*r_nant
    endif 
  else
    k = k+(3+3*r_nbb)*r_nant
  endif
  if (de%version.le.3) then
    do i=1, r_nant
      do j=1, r_nbb
        m = r_mappol(i,j)
        dmdelay(m,i) = r_dmdelay(j,i)
      enddo
      call r4tor4(dmdelay(1,i),data(k),r_npol_rec)
      k = k + r_npol_rec
    enddo
  else 
    do i=1, r_nant
      call r4tor4(r_dmdelay(1,i),data(k),r_nbb)
      k = k + r_nbb
    enddo
  endif
  !      K = K + 14*R_NANT + 4*R_NBAS
  if (de%version.le.2) then
    k = k+13*r_nant+4*r_nbas*r_npol_rec
  else
    k = k+27*r_nant+4*r_nbas*r_npol_rec
  endif
  do i =1, r_nbas
    data(k) = ior(data(k),r_dmbflag(i))
    k = k+1
  enddo
  if (de%version.le.2) then
    k = k+6*r_nbas+ (1+mwvrch)*r_nant
  else
    k = k+2*r_nbas+ (1+mwvrch)*r_nant
  endif 
  if (de%version.le.3) then
    dmsaflag = 0
    do j= 1, r_nant
      do ii = 1, 16
         if (r_dmsaflag(ii,j))  dmsaflag(j)= ibset(dmsaflag(j),ii-1)
         if (r_dmsaflag(ii+mbands,j))  dmsaflag(j)= ibset(dmsaflag(j),ii+15)
      enddo
    enddo
    dmsbflag = 0
    do j = 1, r_nbas
      do ii = 1, 16
         if (r_dmsbflag(ii,j)) dmsbflag(j)= ibset(dmsbflag(j),ii-1)
         if (r_dmsbflag(ii+mbands,j)) dmsbflag(j)= ibset(dmsbflag(j),ii+15)
      enddo
    enddo
    if (r_nband_widex.ne.0) then
      k = k+r_nant*r_npol_rec*(r_lband+1)
      do i =1, r_nant
        data(k) = ior(data(k),dmsaflag(i))
        k = k+1
      enddo
      do i =1, r_nbas
        data(k) = ior(data(k),dmsbflag(i))
        k = k+1
      enddo
    endif
  else
     k = k +r_nant*r_nif 
     do i=1, r_nant
       l=0  
       do j=1, r_nword
         do m=1, 32
           l=l+1
           if (r_dmsaflag(l,i)) data(k)=ibset(data(k),m-1)
         enddo
         k = k+1
       enddo
     enddo
     do i=1, r_nant
       l=0  
       do j=1, r_nword
         do m=1, 32
           l=l+1
           if (r_dmsaflag(l+mbands,i)) data(k)=ibset(data(k),m-1)
         enddo
         k = k+1
       enddo
     enddo
     do i=1, r_nbas
       l=0  
       do j=1, r_nword
         do m=1, 32
           l=l+1
           if (r_dmsbflag(l,i)) data(k)=ibset(data(k),m-1)
         enddo
         k = k+1
       enddo
     enddo
     do i=1, r_nbas
       l=0  
       do j=1, r_nword
         do m=1, 32
           l=l+1
           if (r_dmsbflag(l+mbands,i)) data(k)=ibset(data(k),m-1)
         enddo
         k = k+1
       enddo
     enddo
  endif
  return
end subroutine modify_dh
!
subroutine update_observation(data_in,ndata_in,data_out,ndata_out,error)
  use gkernel_interfaces
  use gildas_def
  use classic_api
  use clic_file
  !
  ! Routine to convert "data" to latest observation version
  ! Allows to convert only data headers for the time being
  !
  integer(kind=address_length) :: data_in   ! Data (i/o)
  integer(kind=data_length)    :: ndata_in  ! Data length (i/o)
  integer(kind=address_length) :: data_out  ! Data (i/o)
  integer(kind=data_length)    :: ndata_out ! Data length (i/o)
  logical                      :: error     ! Error flag
  !
  ! Global
  include 'clic_parameter.inc'
  include 'clic_par.inc'
  include 'gbl_memory.inc'
  !
  ! Local
  integer(kind=data_length) :: h_offset, c_offset, l_offset, k, ldpar, ldump
  integer(kind=address_length) :: kin, kou, ipk, opk
  integer :: idump, version_in, version_out
  !
  version_in = e%version
  version_out = version_current
  !
  ! Out sizes
  ldpar = m_dh      ! To be optimized and checked in RDI
  ldump = r_ldatc+ldpar
  if (r_ndatl.ge.1) then
    ndata_out = (r_ndump+r_ndatl)*ldump+r_ndatl*r_ldatl
  else
    ndata_out = (r_ndump+1)*ldump+r_ldatl
  endif
  !
  ! Get some memory 
  call get_memory(ndata_out,data_out,error)
  if (error) then
    call message(6,3,'CONVERT','Problems with getting memory')
    return
  endif
  !
  ! Loop on dumps 
  kin = gag_pointer(data_in,memory)
  kou = gag_pointer(data_out,memory) 
  opk = kou
  do idump = 1, r_ndump+max(1,r_ndatl)
    !
    ! Modify data header
    ipk = kin + h_offset(idump)
    de%version = version_in           ! File version
    call decode_header(memory(ipk))
    de%version = version_out          ! Memory version
    call encode_header(memory(opk))
    opk = opk + ldpar
    ! Copy continuum data
    ipk = kin + c_offset(idump)
    call w4tow4(memory(ipk),memory(opk),r_ldatc)
    opk = opk + r_ldatc
    if (idump.gt.r_ndump) then
       ! 
       ! Copy line data if applicable
       ipk = kin +l_offset(idump)
       call w4tow4(memory(ipk),memory(opk),r_ldatl) 
       opk = opk + r_ldatl
    endif
  enddo
  !
  ! Update data descriptor
  e%version = version_out
  r_ldpar = ldpar 
  r_ldump = r_ldpar+r_ldatc
end subroutine update_observation
